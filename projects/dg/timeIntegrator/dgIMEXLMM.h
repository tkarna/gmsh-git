#ifndef _DG_IMEXLMM_H_
#define _DG_IMEXLMM_H_

class dgConservationLaw;
class dgDofManager;
class dgNewton;
class dgAssemblerExplicit;

#include "dgDofContainer.h"

/** IMEX LMM iterator **/
typedef enum { IMEX_BDF2=2, IMEX_BDF3=3, IMEX_BDF4=4, IMEX_BDF5=5} IMEX_LMM_TYPE;

class dgIMEXLMM
{
 private:
  double _dt;
  int _k;
  dgConservationLaw *_claw;
  dgDofManager *_dofIm;
  dgAssemblerExplicit *_assembler;
  bool ownAssembler;
  dgNewton *_newton;
  bool ownNewton;
  bool initialized;
  std::vector<dgDofContainer*> _previousF;
  std::vector<dgDofContainer*> _previousG;
  std::vector<dgDofContainer*> _previousX;
  dgDofContainer _buf;
  dgDofContainer _X;
  std::vector<double> _a, _bIm, _bEx;
  inline void setCoef(IMEX_LMM_TYPE type);
 public:
  double starter(dgDofContainer &solution, double dt, double t, int order);
  dgIMEXLMM(dgConservationLaw &claw, dgDofManager &dofIm, IMEX_LMM_TYPE type, dgDofManager *dofEx=NULL);
  ~dgIMEXLMM();
  int iterate(dgDofContainer &solution, double dt, double t);
  inline dgNewton &getNewton() {return *_newton;}
  inline dgAssemblerExplicit * getAssembler() {return _assembler;}
};

#endif
