#ifndef _DG_JACOBIAN_OPERATORS_H_
#define _DG_JACOBIAN_OPERATORS_H_

class dgConservationLaw;
class dgDofManager;

#include "dgDofContainer.h"
#include "dgBlockMatrix.h"

class dgJacobianOperators
{
  dgDofManager *_dof;
  dgBlockMatrix _jac;
  dgDofContainer _F;
  dgConservationLaw &_law;
 public:
  dgJacobianOperators(dgConservationLaw &claw, const dgGroupCollection &groups);
  void computeTermAndJacobian(const dgDofContainer &U0, double t, bool needJacobian);
  void jacobianMultiply(const dgDofContainer &in, dgDofContainer &out){_jac.mult(in, out);}
  void printJacobian(){_jac.print();}
  dgBlockMatrix *getJacobian(){return &_jac;}
  dgDofContainer *getTerm(){return &_F;}
  ~dgJacobianOperators();
};

#endif
