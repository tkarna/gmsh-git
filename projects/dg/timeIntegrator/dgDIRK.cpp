#include "dgDIRK.h"
#include "dgNewton.h"
#include "dgDofContainer.h"
#include "dgDofManager.h"
#include "dgConservationLaw.h"
#include "GmshConfig.h"
#include<sstream>
#ifdef HAVE_MPI
#include "mpi.h"
#endif

static void copyButcherInVector (int n, const double *a, const double *b, const double *c, std::vector<double> &va, std::vector<double> &vb, std::vector<double> &vc)
{
  va.resize(n * n);
  vb.resize(n);
  vc.resize(n);
  for (int i = 0; i < n; i ++)
  {
    vb[i] = b[i];
    vc[i] = c[i];
    for (int j = 0; j < n; ++j)
      va[i * n + j] = a[i * n + j];
  }
}

void dgDIRK::setCoef(int order) {
  switch (order) {
    case 1 :
      {
        _a.push_back(1);
        _b.push_back(1);
        _c.push_back(1);
        break;
      }
    case 2 :
      {
        double Gamma = (2 - sqrt(2.))/2;
        double a[2*2] = {Gamma, 0,  1-Gamma, Gamma};
        double b[2] = {1-Gamma, Gamma};
        double c[2] = {Gamma, 1};
        copyButcherInVector(2, a, b, c, _a, _b, _c);
        break;
      }
    case 3 :
      {
        double a[4*4] = {
          1./2, 0.  , 0.  , 0.  ,
          1./6, 1./2, 0.  , 0.  ,
          -1./2, 1./2, 1./2, 0.  ,
          3./2,-3./2, 1./2, 1./2};
        double b[4] = {3./2,-3./2, 1./2, 1./2};
        double c[4] = {1./2, 2./3, 1./2, 1.  };
        copyButcherInVector(4, a, b, c, _a, _b, _c);
        break;
      }
    default : Msg::Fatal("diagonal implicit runge-kutta not implemented for order %i", order);
  }
}

dgDIRK::dgDIRK (dgConservationLaw &claw, dgDofManager &dof, std::vector<double> a, std::vector<double> b, std::vector<double> c) :
    _X(dof.getGroups(), claw.getNbFields()),
    _a(a),
    _b(b),
    _c(c)
{
  _newton = new dgNewton(dof, claw);
  ownNewton = true;
  _du.resize(_b.size(), _X);
}

dgDIRK::dgDIRK (dgConservationLaw &claw, dgDofManager &dof, int order) :
    _X(dof.getGroups(),claw.getNbFields())
{
  _newton = new dgNewton(dof, claw);
  ownNewton = true;
  setCoef(order);
  _du.resize(_b.size(), _X); 
}

dgDIRK::dgDIRK (dgNewton *newton, int order) :
    _X(newton->getDof().getGroups(), newton->getLaw().getNbFields())
{
  _newton = newton;
  ownNewton = false;
  setCoef(order);
  _du.resize(_b.size(), _X);
}

int dgDIRK::subiterate(dgDofContainer &solution, double dt, double t, int step)
{
  int nsteps = _b.size();
  function::getDT()->set(dt);
  _X.copy(solution);
  for (int j = 0; j < step; j ++)
    _X.axpy (_du[j], _a[step*nsteps+j] / _a[j*nsteps+j]);
  function::getSubDT()->set(dt * _c[step]);
  _du[step].copy (_newton->solve (&_X, 1. / (dt * _a[step*nsteps+step]), NULL, t + dt * _c[step], &solution));
  if (step == nsteps - 1)
    for (int i = 0; i < nsteps; i++)
      solution.axpy (_du[i], _b[i] / _a[i * nsteps + i]);
  return _newton->hasConverged();
}

int dgDIRK::iterate (dgDofContainer &solution, double dt, double t)
{
  if (dt <= 0)
    Msg::Fatal("Time step must be greater than zero");
  int nsteps = _b.size();
  int maxIterNewton = 0;
  function::getDT()->set(dt);
  for (int i = 0; i < nsteps; i++) {
    int converged = subiterate(solution, dt, t, i);
    maxIterNewton = std::max(converged, maxIterNewton);
  }
  return maxIterNewton;
}
dgDIRK::~dgDIRK()
{
  if(_newton && ownNewton)
    delete _newton;
}

