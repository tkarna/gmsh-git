#ifndef _DG_IMEX_DIMSIM_H_
#define _DG_IMEX_DIMSIM_H_

class dgConservationLaw;
class dgDofManager;
class dgNewton;
class dgAssemblerExplicit;

#include "dgDofContainer.h"

typedef enum {IMEX_DIMSIM_2 = 1, IMEX_DIMSIM_3a = 2, IMEX_DIMSIM_3b = 3, IMEX_DIMSIM_3c = 4, IMEX_DIMSIM_3d = 5, IMEX_DIMSIM_5a = 6} IMEX_DIMSIM_TYPE;

/** Diagonally implicit-explicit multistage integration method **/
class dgIMEXDIMSIM
{
 private:
  double _dt;
  dgConservationLaw *_claw;
  dgDofManager *_dofIm;
  dgAssemblerExplicit *_assembler;
  bool ownAssembler;
  dgNewton *_newton;
  bool ownNewton;
  bool initialized;
  std::vector<dgDofContainer> _KIm;
  std::vector<dgDofContainer> _KEx;
  std::vector<dgDofContainer> _L;
  std::vector<dgDofContainer> _LOld;
  dgDofContainer _buf;
  dgDofContainer _X;
  std::vector<double> _aIm, _bIm, _aEx, _bEx, _c, _u, _v, _betaEx, _betaIm, _gamma, _wEx, _wIm, _d;
  inline void setCoef(IMEX_DIMSIM_TYPE type);
 public:
  double starter(dgDofContainer &solution, double sdt, double dt, double t, int order);
  dgIMEXDIMSIM(dgConservationLaw &claw, dgDofManager &dofIm, IMEX_DIMSIM_TYPE type, dgDofManager *dofEx=NULL);
  int iterate(dgDofContainer &solution, double dt, double t);
  inline dgNewton &getNewton() {return *_newton;}
  inline dgAssemblerExplicit * getAssembler() {return _assembler;}
};

#endif
