#include "dgIMEXTSRK.h"
#include "dgIMEXRK.h"
#include "dgNewton.h"
#include "dgAssemblerExplicit.h"
#include "dgDofContainer.h"
#include "dgDofManager.h"
#include "dgConservationLaw.h"
#include "GmshConfig.h"
#include "dgMassAssembler.h"
#include<sstream>
#ifdef HAVE_MPI
#include "mpi.h"
#endif

static void copyButcherInVector1d (int n, const double *v,std::vector<double> &vv)
{
  vv.resize(n);
  for (int i = 0; i < n; i ++)
  {
    vv[i] = v[i];
  }
}

static void copyButcherInVector2d (int n, const double *m,std::vector<double> &vm)
{
  vm.resize(n*n);
  for (int i = 0; i < n; i ++)
    for (int j = 0; j < n; j++)
      vm[i*n+j] = m[n*i+j];
}

dgIMEXTSRK::dgIMEXTSRK(dgConservationLaw &claw, dgDofManager &dofIm, IMEX_TSRK_TYPE type, dgDofManager *dofEx) :
  _claw(&claw),
  _dofIm(&dofIm),
  _buf(dofIm.getGroups(),claw.getNbFields()),
  _X(dofIm.getGroups(),claw.getNbFields()),
  _L(dofIm.getGroups(),claw.getNbFields())
{
  _dt = -1;
  _claw->setImexMode(dgConservationLaw::IMEX_IMPLICIT);
  _newton = new dgNewton(dofIm, claw);
  ownNewton = true;
  _assembler = new dgAssemblerExplicit(dofEx, claw);
  ownAssembler = true;
  initialized = false;
  setCoef(type);
  _KIm.resize(_c.size(), _X);
  _KEx.resize(_c.size(), _X);
  _KImOld.resize(_c.size(), _X);
  _KExOld.resize(_c.size(), _X);
}

void dgIMEXTSRK::setCoef(IMEX_TSRK_TYPE type) {
  switch (type) {
   case IMEX_TSRK_23 :
      {
        double c[2] = {0.,1.};
        double aIm[2*2] = {0.416666666666667, 0, 0.815364469611720, 0.416666666666667};
        double bIm[2*2] = {0.5,0.083333333333333,-0.083333333333333,-0.148697802945053};

        double aEx[2*2] = {0.,0.,1.825912381819746,0.};
        double bEx[2*2] = {0.5, 0.5, -0.5, -0.325912381819746};
        double u[2] = {1.,0.};
        _theta = 0.;
        double vEx[2] = {0.81536446961172, 0.416666666666667};
        double vIm[2] = {0.81536446961172, 0.416666666666667};
        double w[2] = {-0.083333333333333, -0.148697802945053};

        //copyButcherInVector1d(2,v,_v);
        copyButcherInVector1d(2,vEx,_vEx);
        copyButcherInVector1d(2,vIm,_vIm);
        copyButcherInVector1d(2,w,_w);
        copyButcherInVector1d(2,u,_u);
        copyButcherInVector1d(2,c,_c);
        copyButcherInVector2d(2, aEx,_aEx);
        copyButcherInVector2d(2, aIm,_aIm);
        copyButcherInVector2d(2, bEx,_bEx);
        copyButcherInVector2d(2, bIm,_bIm);
        break;
      }
   case IMEX_TSRK_34 :
      {
        double c[3] = {0, 0.5, 1};
        double aIm[3*3] = {0.235880190910947, 0., 0., 1.018461419211348, 0.235880190910947, 0., 0.290136315264242, 0.389812569689545, 0.235880190910947};
        double aEx[3*3] = {0., 0., 0., 1.215905100969091, 0., 0., -1.012523027539376, 1.143656957186445, 0.};
        _theta = 0.278367184188801;
        double bIm[3*3] = {0.039528854287143, 0.158115417148572, -0.196351336623804,
	                   0.060537659749704, 0.393311975375446, -0.679684141283538,
	                   0.115608054942414, -0.091275974184587, 0.338206027566240};
        double bEx[3*3] = {0.039528854287143, 0.158115417148572, 0.039528854287143,
	                   0.296417850660651, -0.314328597357395, -0.169487250308440,
	                   0.069404240178355, 0.283215661018536, 0.794613353344841};
        double u[3] = {0.237173125722858, 0.528507103963907, 0.278367184188801};
	double vEx[3] = {aIm[6], aIm[7], aIm[8]};
        double vIm[3] = {aIm[6], aIm[7], aIm[8]};
	double w[3] = {bIm[6], bIm[7], bIm[8]};
        copyButcherInVector1d(3,vIm,_vIm);
        copyButcherInVector1d(3,vEx,_vEx);
        copyButcherInVector1d(3,w,_w);
        copyButcherInVector1d(3,u,_u);
        copyButcherInVector1d(3,c,_c);
        copyButcherInVector2d(3, aEx,_aEx);
        copyButcherInVector2d(3, aIm,_aIm);
        copyButcherInVector2d(3, bEx,_bEx);
        copyButcherInVector2d(3, bIm,_bIm);
        break;
      }
   case IMEX_TSRK_ev4 :
      {
        double c[3] = {-0.1932019056112625, -0.5868942450696052,1.0875233281146581};
        //double c[3] = {0, 0.5868942450696052,1.0875233281146581};	
        double aIm[3*3] = {0.5 , 0 , 0 ,
                           0.555158209211296 , 0.5   , 0,
                           -0.278970902909974 , 2.326822807480972, 0.5};
        double bIm[3*3] =   {0.011385950463339, 0.046591031460398, -0.294123172715654,
			     -0.481293188802622, 0.309247981970040, -0.418047327148037,
			     -2.386222820797578, 0.990174110957607, 0.087160936498261};
        double aEx[3*3] = {0     , 0       , 0,
                           0.130476793083096 , 0       , 0,
                           1.649241112842109 , 1.814778592781876  , 0};
        double bEx[3*3] = {0.399362466364539, -0.166335960500608, 0.030827303344152,
                           0.517023762612737, -0.18175387306707, -0.000681007398086,
                           -5.849608690342908, 3.223595203158622, 0.400957912789590};
        double u[3] = {0.4570557148193444,1.0519599203002830,0.1514408031146307};
        _theta = 0;
        double vEx[3] = {-0.702404745643173, 2.118523168461118, 0.39319598421807};
        double vIm[3] = {-0.702404745643173, 2.118523168461118, 0.39319598421807};
        double w[3] = {-2.075547697702156, 0.840494705444326, 0.425738585221815};
        
        //copyButcherInVector1d(3,v,_v);
        copyButcherInVector1d(3,vIm,_vIm);
        copyButcherInVector1d(3,vEx,_vEx);
        copyButcherInVector1d(3,w,_w);
        copyButcherInVector1d(3,u,_u);
        copyButcherInVector1d(3,c,_c);
        copyButcherInVector2d(3, aEx,_aEx);
        copyButcherInVector2d(3, aIm,_aIm);
        copyButcherInVector2d(3, bEx,_bEx);
        copyButcherInVector2d(3, bIm,_bIm);
        break;
      }
   case IMEX_TSRK_56 :
      {
        double c[5] = {-0.4045545270596140, -0.2648814932055048, 0.0573006049881161, 0.3537009742246656, 0.4888151814702037};
        double aIm[5*5] = {0.5,               0,                 0,                  0,                 0,
                           -0.219716941152438, 0.5,               0,                  0,                 0,
                           0.346773919739397,  -0.878445809486042, 0.5,                0,                 0,
                           0.066017872696560,  -1.149762715421605, -0.082640514076908,  0.5,               0,
                           -1.694204053596701, 0.166183201140192,  -0.742293615298083,  0.200483541400373,  0.5};
        double bIm[5*5] =   {-1.426514003802300, 3.345688000742844, -5.218030001825067, 8.753403451191515, -6.358886236134719,
	                     -1.852130371557033, 4.344962599595140, -6.766817742906721, 11.100814575120509, -7.371858146755118,
                             -4.263885437633374, 9.736820104626624, -13.734655585071252, 18.290030946666331, -9.939290614227902,
                             -10.196227552596067, 22.710411541189387, -29.477813698981240,  33.884656643594731,-15.900914934000989,
                             -16.693676181973967, 36.829591934210178, -46.440243468544779, 51.343753993776154, -22.980757155738928};
        double aEx[5*5] = {0,                  0,                  0,                 0,                 0,
                           -0.411067755933170, 0,                  0,                 0,                 0, 
                           -2.184767292491065, 0.988337848532673,  0,                 0,                 0,
                           -1.933520824847556, -0.039861058310592, 0.757541697266156, 0,                 0,
                           -0.605970379043349, -1.561665570720364, 1.243988457457954, 0.094790995264783, 0};
        double bEx[5*5] = {-1.28995988436797,  3.01509152040882, -4.635404693403601, 7.439327688499761, -4.933393420964737,
                           -1.071271446612666,  2.488920905157963, -3.731039866088971, 5.533368094228661, -3.073655958407476,
                           -0.24158168198390,  0.585136843092235, -0.975677382623482, 1.297866462015352, 0.588032728071968,
                           -0.670424286810472,  1.660524137719979, -2.857197273619436, 4.082694772448352, -0.646030521442559,
                           -1.516563433968520,   3.677389064377236,  -5.986500374361925,   8.415453055292184,  -3.272083618923554};
        double u[5] = {0.0002157372318872, 0.0001354655498456, 0.0000469196256648, 0.0000256681792066, 0.0000230139042425};
        _theta = 0;
        double v[5] = {1.383107620382207, -1.209559812526554, 0.112810664731615, -1.919857152723093, 2.645957623294895};
        double w[5] = {-4.706540203232213,  10.880797962453455, -15.264606465662681,  18.345730361159166, -9.267840597876798};

        //copyButcherInVector1d(3,v,_v);
        copyButcherInVector1d(5,v,_vIm);
        copyButcherInVector1d(5,v,_vEx);
        copyButcherInVector1d(5,w,_w);
        copyButcherInVector1d(5,u,_u);
        copyButcherInVector1d(5,c,_c);
        copyButcherInVector2d(5, aEx,_aEx);
        copyButcherInVector2d(5, aIm,_aIm);
        copyButcherInVector2d(5, bEx,_bEx);
        copyButcherInVector2d(5, bIm,_bIm);
        break;
        }
        //
  default : Msg::Fatal("IMEX Two-Step Runge-Kutta not implemented for the method %i", type);
  }
}

int dgIMEXTSRK::iterate (dgDofContainer &solution, double dt, double t)
{
  if (dt <= 0)
    Msg::Fatal("Time step must be greater than zero");
  if (abs(_dt-dt)/dt > 1e-12)
    _newton->forceReassembleMatrix();

  int nsteps = _c.size();
  int maxIterNewton = 0;
  //int timeDir = 1;
  _dt = dt;
  function::getDT()->set(dt);

  for (int istep = 0; istep < nsteps; istep++) {
    function::getSubDT()->set(dt*_c[istep]);
    // IMPLICIT //
    _X.copy(solution);
    _X.scale(1.-_u[istep]);
    _X.axpy(_L, _u[istep]);
    for (int j = 0; j < istep; j++){
      if (_aIm[istep*nsteps+j]!=0) _X.axpy (_KIm[j], dt*_aIm[istep*nsteps+j]);
      if (_aEx[istep*nsteps+j]!=0) _X.axpy (_KEx[j], dt*_aEx[istep*nsteps+j]);
    }
    for (int j = 0; j < nsteps; j++){
      if (_bIm[istep*nsteps+j]!=0) _X.axpy (_KImOld[j], dt*_bIm[istep*nsteps+j]);
      if (_bEx[istep*nsteps+j]!=0) _X.axpy (_KExOld[j], dt*_bEx[istep*nsteps+j]);
    }

    _claw->setImexMode(dgConservationLaw::IMEX_IMPLICIT);
    //if(_c[istep]<0)
    //  timeDir = -1;
    //else
    //  timeDir = 1;
    //_claw->setTimeDirection(timeDir);
    if(_aIm[istep*nsteps+istep]!=0){
      _KIm[istep].copy (_newton->solve (&_X, 1.0/(dt*_aIm[istep*nsteps+istep]), NULL, t+dt*_c[istep]));
      _KIm[istep].scale (1.0/(dt*_aIm[istep*nsteps+istep]));
    }
    maxIterNewton = std::max(_newton->hasConverged(), maxIterNewton);

    if (_aIm[istep*nsteps+istep]!=0) _X.axpy (_KIm[istep], dt*_aIm[istep*nsteps+istep]);
    // EXPLICIT //
    
    _claw->setImexMode(dgConservationLaw::IMEX_ALL);
    _assembler->solve (_X, 1, NULL, t+dt*_c[istep], _KEx[istep]);
    _claw->setImexMode(dgConservationLaw::IMEX_IMPLICIT);
    _assembler->solve (_X, 1, NULL, t+dt*_c[istep], _buf);
    _KEx[istep].axpy(_buf, -1);

    //bug test
    //_assembler->solve (_X, 1, NULL, t+dt*_c[istep], _KIm[istep]);
    //_KEx[istep].axpy(_KIm[istep],-1.0);
  }

  _buf.setAll(0.0);
  if(1-_theta != 0) _buf.axpy(solution, 1.0-_theta);
  if(_theta !=0) _buf.axpy(_L, _theta);
  for(int j=0; j<nsteps; j++){
    if(_vIm[j]!=0) _buf.axpy(_KIm[j],dt*_vIm[j]);
    if(_vEx[j]!=0) _buf.axpy(_KEx[j],dt*_vEx[j]);
  }
  for(int j=0; j<nsteps; j++){
    if(_w[j]!=0){
      _buf.axpy(_KImOld[j],dt*_w[j]);
      _buf.axpy(_KExOld[j],dt*_w[j]);
    }
  }

  _L.copy(solution);
  solution.copy(_buf);
  for(int j=0; j<nsteps; j++){
    _KImOld[j].copy(_KIm[j]);
    _KExOld[j].copy(_KEx[j]);
  }

  return maxIterNewton;
}

double dgIMEXTSRK::starter(dgDofContainer &solution, double dt, double t, int order)
{
  dgIMEXRK stimeIter(*_claw,*_dofIm,order);
  double sdt;//sub steps
  double tt;
  int substeps;

  substeps = 2;
  initialized = true;

  for(int i=0;i<(int)_c.size();i++){
    _X.copy(solution);
    sdt=(1.0+_c[i])*dt/(double)substeps;
    tt=t;
    for(int j=0;j<substeps;j++){
      stimeIter.iterate(_X,sdt,tt);
      tt=tt+sdt;
    }
      //stimeIter.iterate(_X,t+(-cmin+_c[i])*dt-tt,tt);
    _claw->setImexMode(dgConservationLaw::IMEX_ALL);
    _assembler->solve (_X, 1, NULL, t+(1.0+_c[i])*dt, _KExOld[i]);
    _claw->setImexMode(dgConservationLaw::IMEX_IMPLICIT);
    _assembler->solve (_X, 1, NULL, t+(1.0+_c[i])*dt, _KImOld[i]);
    _KExOld[i].axpy(_KImOld[i], -1);

  }

  substeps = 2;
  sdt=dt/(double)substeps;
  tt=t;
  for(int j=0;j<substeps;j++){
    stimeIter.iterate(solution,sdt,tt);
    tt=tt+sdt;
  }
  //stimeIter.iterate(solution,t+dt-tt,tt);
  t=t+dt;
  //stimeIter.iterate(solution,-cmin*dt,t);
  //t = t-cmin*dt;
  _L.copy(solution);

  sdt=dt/(double)substeps;
  tt=t;
  for(int j=0;j<substeps;j++){
    stimeIter.iterate(solution,sdt,tt);
    tt=tt+sdt;
  }
  //stimeIter.iterate(solution,t+dt-tt,tt);
  t=t+dt;
  return t; // return current time
}
