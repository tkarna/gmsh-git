#include "dgNewton.h"
#include "dgDofContainer.h"
#include "dgDofManager.h"
#include "dgConservationLaw.h"
#include "dgGroupOfElements.h"
#include "GmshConfig.h"
#include<sstream>
#ifdef HAVE_MPI
#include "mpi.h"
#endif

dgNewton::dgNewton (dgDofManager &dof, const dgConservationLaw &law) :
  _dof (dof),
  _law (law),
  _currentSolution (_dof.getGroups(), _law.getNbFields()),
  _dU (_currentSolution),
  _F (_currentSolution),
  _residual (_currentSolution)
{
  _maxit = 30;
  _atol = 1e-14;
  _rtol = 1e-10;
  _verb = 1;
  _law.fixStrongBC(_dof);
  _dof.fillSparsityPattern(!_dof.isContinuous(), &_law);
  _assembleLHSMatrix = true;
  _preProc = NULL;
  _returnZeroIfNotConverged = false;
}

//with c++11 : can merge both constructors !
dgNewton::dgNewton (dgDofManager &dof, const dgConservationLaw &law, bool fillSparsityPattern) :
  _dof (dof),
  _law (law),
  _currentSolution (_dof.getGroups(), _law.getNbFields()),
  _dU (_currentSolution),
  _F (_currentSolution),
  _residual (_currentSolution)
{
  _maxit = 30;
  _atol = 1e-14;
  _rtol = 1e-10;
  _verb = 1;
  _law.fixStrongBC(_dof);
  if(fillSparsityPattern)
    _dof.fillSparsityPattern(!_dof.isContinuous(), &_law);
  _assembleLHSMatrix = true;
  _preProc = NULL;
  _returnZeroIfNotConverged = false;
}

void dgNewton::assembleSystem (double alpha, const dgDofContainer *R, double t, dgDofContainer * variable)
{
  const dgGroupCollection &groups = _dof.getGroups();
  _F.setAll(0.0);
  _law.fixStrongBC(_dof, &_currentSolution);
  _dof.zeroRHS();
  std::vector<dgFullMatrix<double> > MG(groups.getNbElementGroups());
  _residual.setAll(0.);
  if (_assembleLHSMatrix)
    _dof.zeroMatrix();
  _currentSolution.scatterBegin(!_dof.isContinuous());
  for (int iGroup = 0; iGroup < groups.getNbElementGroups(); iGroup++) {
    const dgGroupOfElements &group = *groups.getElementGroup(iGroup);
    _law.computeElementMass(group, t, &_dU, &_residual, alpha, _assembleLHSMatrix ? &_dof: NULL);
    _law.computeElement(group, t, _currentSolution, _F,  _assembleLHSMatrix ? &_dof : NULL, variable);
  }
  _currentSolution.scatterEnd();
  for (int iGroup = 0; iGroup < groups.getNbFaceGroups (); iGroup++) {
    dgGroupOfFaces &faces = *groups.getFaceGroup(iGroup);
    if(_dof.isContinuous() && (faces.nConnection() != 1 )) continue;
    _law.computeFaceMass(faces, t, &_dU, &_residual, alpha, _assembleLHSMatrix ? &_dof : NULL);
    _law.computeFace(faces, t, _currentSolution, _F, _assembleLHSMatrix ? &_dof : NULL, variable);
  }
  if (_law.isConstantJac()) _assembleLHSMatrix=false;
  
  _dof.setMatrixFreeParameters(_currentSolution, _F, alpha);
  _residual.axpy(_F);
  if (R)
    _residual.axpy(*R);
  fullMatrix<double> rhs;
  for (int iGroup = 0; iGroup < groups.getNbElementGroups (); iGroup++){
    dgGroupOfElements *group = groups.getElementGroup(iGroup);
    dgFullMatrix<double> &GG = _residual.getGroupProxy (iGroup);
    for (size_t iElement = 0 ; iElement < group->getNbElements(); iElement++) {
      GG.getBlockProxy(iElement, rhs);
      _dof.assembleRHSVector(iGroup, iElement, rhs);
    }
  }
}


const dgDofContainer &dgNewton::solve (const dgDofContainer *U0, double alpha, const dgDofContainer *R, double t, dgDofContainer *variable) {
  // todo function::getDT()->set(dt);
  const dgGroupCollection &groups = _dof.getGroups();
  int nbFields = _law.getNbFields();
  _dU.setAll (0);

  firstNewtonResidual = 0.0;
  bool hasConverged = false;

  if (U0)
    _currentSolution.copy (*U0);
  else
    _currentSolution.setAll (0.);

//dgDofContainer newtonStep(&groups, _law.getNbFields());

  int ii;
  for (ii = 0; ii < _maxit; ii++) {

    if(_preProc) _preProc->call();

    assembleSystem (alpha, R, t, variable);

/*    char str[100], str2[100], str3[100];
    sprintf(str, "output/debugSol%d.msh", ii);
    sprintf(str2, "output/debugDu%d.msh", ii);
    sprintf(str3, "output/debugV%d.msh", ii);
    _currentSolution.exportMsh(str, ii, ii, "sol");
    _dU.exportMsh(str2, ii, ii, "Du");
    newtonStep.exportMsh(str3, ii, ii, "step");*/

    // Compute Newton Residual and check convergence
    double newtonResidual = 0;
    newtonResidual = _dof.getNormInfRHS();
    if (newtonResidual != newtonResidual) {
      if (_verb > 0)
        Msg::Info("Newton failed");
      _converged = 0;
      if(_returnZeroIfNotConverged)
        _dU.setAll(0.);
      return _dU;
    }
    double conv = 0.0;
    if (ii == 0)
      firstNewtonResidual = newtonResidual;
    if (ii > 0 && firstNewtonResidual == 0.0 )
      conv = log(1. / newtonResidual) / log(10);
    else if (ii > 0. && newtonResidual != 0.0 )
      conv = log(firstNewtonResidual / newtonResidual) / log(10);
    if (_verb > 1)
      Msg::Info("Newton residual (%i): %g (%.2f)", ii, newtonResidual, conv);
      //todo : this check ii>0 are for boundary condition but we have to find another solution
    if (ii > 0 && ((firstNewtonResidual != 0.0 && newtonResidual / firstNewtonResidual < _rtol) || (newtonResidual < _atol))) {
      hasConverged = true;
      break;
    }
    _dof.solve();
    // store for the next newton iteration
    // new sol = solGroup:  U(n+1)^{k+1}      = U(n+1)^{k}      + [U(n+1)^{k+1} - U(n+1)^{k}]
    // and  deltaG:         U(n+1)^{k+1}-U(n) = U(n+1)^{k}-U(n) + [U(n+1)^{k+1} - U(n+1)^{k}]
    fullMatrix<double> v;
    for (int iGroup = 0; iGroup < groups.getNbElementGroups(); iGroup++){
      dgGroupOfElements *group = groups.getElementGroup(iGroup);
      fullMatrix<double> & solgroup = _currentSolution.getGroupProxy(group);
      fullMatrix<double> & deltaG = _dU.getGroupProxy(group);
//  fullMatrix<double> & step = newtonStep.getGroupProxy(group);
      int nbNodes = group->getNbNodes();
      v.resize (nbNodes * nbFields, 1);
      for (size_t iElement = 0 ; iElement < group->getNbElements(); iElement++) {
        _dof.getSolution(iGroup, iElement, v);
        for ( int i = 0; i < nbNodes; i++) {
          for ( int j = 0; j < nbFields; j++) {
            solgroup (i, iElement*nbFields + j) += v (j*nbNodes+i, 0);
            deltaG   (i, iElement*nbFields + j) += v (j*nbNodes+i, 0);
//    step(i, iElement*nbFields + j) = v(j*nbNodes+i, 0);
          }
        }
      }
    }
    if ( _law.isLinear() ) {
      hasConverged = true;
      break;
    }
  }
  
  if (!hasConverged && _verb>0) {
    Msg::Info("Newton not converged after %i iters !!",_maxit);
    _converged = 0;
    if(_returnZeroIfNotConverged)
      _dU.setAll(0.);
  }
  else {
    _converged = ii + 1;
  }
  
  return _dU;
}
