#ifndef _DG_ROK_H_
#define _DG_ROK_H_

class dgConservationLaw;
class dgDofManager;
class dgArnoldiIteration;
class dgJacobianOperators;

#include "dgDofContainer.h"

typedef enum { DG_ROK_4A = 1, DG_ROK_4B = 2, DG_ROK_4P = 3 } DG_ROK_TYPE;

/** Rosenbrock-Krylov implicit method. */
class dgROK
{
  dgArnoldiIteration *_arnoldi;
  bool _ownArnoldi;
  dgJacobianOperators *_jacobian;
  bool _ownJacobian;
  std::vector<double> _a, _g, _b, _c;
  double _gamma;
  dgDofContainer _F, _Y;
  fullVector<double> _phi;
  fullVector<double> _lambdaSum;
  fullVector<double> _rhs;
  fullVector<int> _ipiv;
  fullMatrix<double> _Imhgh;
  std::vector<fullVector<double> > _lambda;
  std::vector<dgDofContainer> _K;
  inline void setCoef(DG_ROK_TYPE order);
 public:
  dgROK(dgConservationLaw &claw, dgDofManager &dof, DG_ROK_TYPE order, int basisSize);
	dgROK(dgConservationLaw &claw, dgGroupCollection &groups, DG_ROK_TYPE order, int basisSize);
  dgROK(dgJacobianOperators &jac, DG_ROK_TYPE order, int basisSize);
  ~dgROK();
  void finalize(dgDofContainer &solution);
  void iterate(dgDofContainer &solution, double dt, double t);
  void subiterate(dgDofContainer &solution, double dt, double t, int step);
  inline dgArnoldiIteration &getArnoldi() { return *_arnoldi; }
  int getNsteps() { return _b.size(); }
};

class dgERK_test
{
	dgJacobianOperators *_jacobian;
	std::vector<double> _a, _b, _c;
	dgDofContainer _Y;
	std::vector<dgDofContainer> _K;
public:
	dgERK_test(dgConservationLaw &claw, const dgGroupCollection &groups);
	~dgERK_test();
	void finalize(dgDofContainer &solution);
	void iterate(dgDofContainer &solution, double dt, double t);
	void subiterate(dgDofContainer &solution, double dt, double t, int step);
};

#endif
