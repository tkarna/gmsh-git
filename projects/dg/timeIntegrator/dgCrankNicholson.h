#ifndef _DG_CRANK_NICHOLSON_H_
#define _DG_CRANK_NICHOLSON_H_

class dgConservationLaw;
class dgDofManager;
class dgDofContainer;

#include "dgNewton.h"

/** Crank-Nicholson semi-implicit temporal iterator
 * \f{eqnarray*}
 *    M \frac{u^{t+1} - u^{t}}{\Delta t} + \frac{1}{2} F(u^{t+1}) + \frac{1}{2} F(u^{t}) = 0
 * \} */
class dgCrankNicholson
{
  dgDofContainer *_solution, _R;
  dgGroupCollection _groups;
  dgNewton _newton;
  const dgConservationLaw &_law;
  double _t;
  bool _computeExplicitTerm;  // If false, uses old F as explicit term  !! It doesn't work yet with false.
 public:
  dgCrankNicholson(dgDofContainer &solution, dgConservationLaw &claw, dgDofManager &dof, double t);
  void iterate(double t);
  inline void setComputeExplicitTerm (bool flag) {_computeExplicitTerm = flag;}
  inline dgNewton &getNewton() {return _newton;}
};

#endif
