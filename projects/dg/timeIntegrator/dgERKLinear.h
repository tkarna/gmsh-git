#ifndef _DG_ERK_LINEAR_H_
#define _DG_ERK_LINEAR_H_

class dgConservationLaw;
class dgDofManager;
//class dgLimiter;
//class dgTransformNodalValue;
//class dgAssemblerExplicit;

#include "dgERK.h"
#include "dgBlockMatrix.h"

class dgERKLinear : public dgERK {
  bool _builtMatrix;
  dgDofManager *_dof;
  dgBlockMatrix _mat,_matInvM;
  dgConservationLaw &_law;
 public:
  dgERKLinear(dgConservationLaw &claw, dgGroupCollection *groups, DG_ERK_TYPE order);
  ~dgERKLinear();
  void assembleSystem(double t);
  int subiterate(dgDofContainer &solution, double dt, double t);
};

#endif
