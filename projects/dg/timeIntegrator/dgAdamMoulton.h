#ifndef _DG_ADAM_MOULTON_H_
#define _DG_ADAM_MOULTON_H_

class dgConservationLaw;

#include "dgNewton.h"
#include "dgDofContainer.h"

/**Adams moulton implicit temporal iterator (order n use n-1 previous residual) */
class dgAdamsMoulton 
{
  double _dt, _t;
  const dgConservationLaw *_claw;
  dgDofManager *_dof;
  std::vector<dgDofContainer*> _previousF;
  dgDofContainer *_solution;
  std::vector<double> _coeff;
  dgNewton _newton;
  void _init (int nsteps, double *coeff);
 public:
  dgAdamsMoulton (int order, dgDofContainer &solution, const dgConservationLaw *claw, dgDofManager *dof, double dt, double t);
  ~dgAdamsMoulton();
  void iterate ();
  inline dgNewton &getNewton() {return _newton;}
};

#endif
