#ifndef _DG_TRANSFORM_NODAL_VALUE_H_
#define _DG_TRANSFORM_NODAL_VALUE_H_

#include "fullMatrix.h"
#include <vector>
class dgDofContainer;
class dgGroupCollection;
class dgConservationLawFunction;

/**Parent class for transformations of nodal value */
class dgTransformNodalValue{
protected:
  dgConservationLawFunction *_claw;
public:
  dgTransformNodalValue (dgConservationLawFunction *claw) : _claw(claw) {}
  /**apply a transformation of the solution */
  virtual int apply ( dgDofContainer *sol)=0;
  /**apply an inverse transformation of the solution */
  virtual int apply_Inverse ( dgDofContainer *sol)=0;
  virtual ~dgTransformNodalValue(){};
};

/**Transformation of nodal value with the power law of superconductors */
class dgSupraTransformNodalValue : public dgTransformNodalValue{
public :
  dgSupraTransformNodalValue (dgConservationLawFunction *claw) : dgTransformNodalValue (claw) {}
  virtual int apply ( dgDofContainer *solution);
  virtual int apply_Inverse ( dgDofContainer *solution);
};
#endif
