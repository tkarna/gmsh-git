#include "dgAssemblerExplicit.h"
#include "dgDofContainer.h"
#include "dgDofManager.h"
#include "dgConservationLaw.h"
#include "dgGroupOfElements.h"
#include "GmshConfig.h"
#include<sstream>
#ifdef HAVE_MPI
#include "mpi.h"
#endif


/** solve the linear system :  dU = M^(-1) F (U^n,t) */

dgAssemblerExplicit::dgAssemblerExplicit(dgDofManager *dof, const dgConservationLaw &law) :
    _dof (dof),
    _law (&law)
{
  _assembled = _diagonalMassFactor = false;
  _scatterField = NULL;
  if (_dof) {
    _law->fixStrongBC(*_dof);
    _dof->fillSparsityPattern(false, _law);
  }
}

void dgAssemblerExplicit::solve (dgDofContainer &U0, double alpha, dgDofContainer *R, double t, dgDofContainer &dU, bool recomputeMassMatrix, dgDofContainer *variable)
{
  dgDofContainer F(dU);
  const dgGroupCollection &groups = *dU.getGroups();
  int nbFields = _law->getNbFields();
  bool haveMassFactor = false;
  bool constantMass = true;
  for (int iGroup = 0; iGroup < groups.getNbElementGroups(); iGroup++) {
    const dgGroupOfElements &group = *groups.getElementGroup(iGroup);
    dgConservationLaw::massFactorType mftype = _law->haveMassFactor(group);
    haveMassFactor |= (mftype != dgConservationLaw::MASS_FACTOR_NO);
    constantMass &= (mftype != dgConservationLaw::MASS_FACTOR_TIME_DEPENDENT);
  }
  recomputeMassMatrix |= (haveMassFactor && !_diagonalMassFactor && !constantMass);

  //init iteration
  if (R)
    F.copy(*R);
  else
    F.setAll(0.0);
  std::vector<dgFullMatrix<double> > MG(groups.getNbElementGroups());
  std::vector<dgFullMatrix<double> > IMG(groups.getNbElementGroups());
  (_scatterField ? *_scatterField : U0).scatterBegin(!(_dof && _dof->isContinuous()));
  if(_dof) {
    _law->fixStrongBC(*_dof, &U0, alpha);
    _dof->zeroRHS();
    if (!_assembled || recomputeMassMatrix)
      _dof->zeroMatrix();
  }
  //assemble LHS for volume term
  fullMatrix<double> massE;
  for (int iGroup = 0; iGroup < groups.getNbElementGroups(); iGroup++) {
    dgGroupOfElements &group = *groups.getElementGroup(iGroup);
    if ((!_assembled || recomputeMassMatrix) && _dof) {
      _law->computeElementMass(group, t, NULL, NULL, 1.0, _dof);
      _law->computeElement(group, t, U0, F, NULL);
    }
    else if (haveMassFactor && !_dof) {
      Msg::Error("using a massFactor without a dof manager is no more supported.");
    }
    else {
      _law->computeElement(group, t, U0, F, NULL);
    }
  }
  _assembled = true;
  (_scatterField ? *_scatterField : U0).scatterEnd();
  //assemble LHS for interface term
  for (int iGroup = 0; iGroup < groups.getNbFaceGroups (); iGroup++) {
    dgGroupOfFaces &faces = *groups.getFaceGroup(iGroup);
    if(_dof && _dof->isContinuous() && faces.nConnection() != 1) continue;
    _law->computeFaceMass(faces, t, NULL, NULL, 1.0, _dof);
    _law->computeFace(faces, t, U0, F, NULL);
  }
  if (_dof) {
    fullMatrix<double> rhs; 
    for (int iGroup = 0; iGroup < groups.getNbElementGroups (); iGroup++) {
      dgGroupOfElements *group = groups.getElementGroup(iGroup);
      for (size_t iElement = 0 ; iElement < group->getNbElements(); iElement++) {
        F.getGroupProxy(iGroup).getBlockProxy(iElement, rhs);
        _dof->assembleRHSVector(iGroup, iElement, rhs);
      }
    }
    _dof->solve();
    fullMatrix<double> v;
    dU.setAll(0.);
    for (int iGroup = 0; iGroup < groups.getNbElementGroups(); iGroup++) {
      dgGroupOfElements *group = groups.getElementGroup(iGroup);
      fullMatrix<double> & deltaG = dU.getGroupProxy(group);
      int nbNodes = group->getNbNodes();
      for (size_t iElement = 0 ; iElement < group->getNbElements(); iElement++) {
        v.resize (nbNodes*nbFields, 1);
        _dof->getSolution(iGroup, iElement, v);
        for ( int i = 0; i < nbNodes; i++) {
          for ( int j = 0; j < nbFields; j++) {
            deltaG (i, iElement*nbFields + j) += v (j*nbNodes+i, 0) / alpha;
          }
        }
      }
    }
  }
  else {
    for (int iGroup = 0; iGroup < groups.getNbElementGroups(); ++iGroup) {
      dU.getGroupProxy(iGroup).gemmBlock(groups.getElementGroup(iGroup)->getInverseMassMatrix(), F.getGroupProxy(iGroup), 1./alpha, 0);
    }
  }
}
