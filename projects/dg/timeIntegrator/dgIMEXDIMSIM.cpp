#include "dgIMEXDIMSIM.h"
#include "dgNewton.h"
#include "dgAssemblerExplicit.h"
#include "dgIMEXRK.h"
#include "dgDofContainer.h"
#include "dgDofManager.h"
#include "dgConservationLaw.h"
#include "dgFunctionIntegrator.h"
#include "GmshConfig.h"
#include<sstream>
#ifdef HAVE_MPI
#include "mpi.h"
#endif

static void copyButcherInVector1d (int n, const double *v,std::vector<double> &vv)
{
  vv.resize(n);
  for (int i = 0; i < n; i ++)
  {
    vv[i] = v[i];
  }
}

static void copyButcherInVector2d (int n, const double *m,std::vector<double> &vm)
{
  vm.resize(n*n);
  for (int i = 0; i < n; i ++)
    for (int j = 0; j < n; j++)
      vm[i*n+j] = m[n*i+j];
}

dgIMEXDIMSIM::dgIMEXDIMSIM(dgConservationLaw &claw, dgDofManager &dofIm, IMEX_DIMSIM_TYPE type, dgDofManager *dofEx) :
  _claw(&claw),
  _dofIm(&dofIm),
  _buf(dofIm.getGroups(),claw.getNbFields()),
  _X(dofIm.getGroups(),claw.getNbFields())
{
  _newton = new dgNewton(dofIm, claw);
  ownNewton = true;
  _assembler = new dgAssemblerExplicit(dofEx, claw);
  ownAssembler = true;
  initialized = false;
  setCoef(type);
  _KIm.resize(_c.size(), _X);
  _KEx.resize(_c.size(), _X);
  _L.resize(_c.size(),_X);
  _LOld.resize(_c.size(),_X);
}


void dgIMEXDIMSIM::setCoef(IMEX_DIMSIM_TYPE type) {
  switch (type) {
   case IMEX_DIMSIM_2:
      {
        double c[2] = {0.0, 1.0};
        double aIm[2*2] = {(2.0-sqrt(2.0))/2.0, 0, (2.0*sqrt(2.0)+6.0)/7.0, (2.0-sqrt(2.0))/2.0};
        double bIm[2*2] = {(73.0-34.0*sqrt(2.0))/28.0, (4.0*sqrt(2.0)-5.0)/4.0, (87.0-48.0*sqrt(2.0))/28.0, (-45.0+34.0*sqrt(2.0))/28.0};
        double aEx[2*2] = {0, 0, 3.0/2.0, 0};
        double bEx[2*2] = {sqrt(2.0)/2.0, (3.0-sqrt(2.0))/4.0, (sqrt(2.0)-1.0)/2.0, (3.0-sqrt(2.0))/4.0};
        double u[2*2] = {1, 0, 0, 1};
        double v[2*2] = {(3.0-sqrt(2.0))/2.0, (sqrt(2.0)-1.0)/2.0, (3.0-sqrt(2.0))/2.0, (sqrt(2.0)-1.0)/2.0};
        double gamma[2] = {(3.0-sqrt(2.0))/2.0, (sqrt(2.0)-1.0)/2.0};
        double betaEx[2] = {sqrt(2.0)/2.0, (3.0-sqrt(2.0))/4.0};
        double betaIm[2] = {(73.0-34.0*sqrt(2.0))/28.0, (2.0*sqrt(2.0)-1.0)/4.0};
        double wEx[2*2] = {0.0, 0.0, -0.5, 0.5};
        double wIm[2*2] = {(sqrt(2.0)-2.0)/2.0, 0.0, (3.0*sqrt(2.0)-12.0)/14.0, (sqrt(2.0)-1.0)/2.0};
        double d[2*2] = {1.0, 0.0, -1.0, 1.0};        
 
        copyButcherInVector1d(2,betaIm,_betaIm);
        copyButcherInVector1d(2,betaEx,_betaEx);
        copyButcherInVector1d(2,gamma,_gamma);
        copyButcherInVector1d(2,c,_c);
        copyButcherInVector2d(2, aEx,_aEx);
        copyButcherInVector2d(2, aIm,_aIm);
        copyButcherInVector2d(2, bEx,_bEx);
        copyButcherInVector2d(2, bIm,_bIm);
        copyButcherInVector2d(2, u,_u);
        copyButcherInVector2d(2, v,_v); 
        copyButcherInVector2d(2, wEx,_wEx);
        copyButcherInVector2d(2, wIm,_wIm);
        copyButcherInVector2d(2, d,_d);
 
        break;
      }
    case IMEX_DIMSIM_3a : //improved version of 3c (A-stable)
      {
        double c[3] = {0.0, 0.5, 1.0};
        double aIm[3*3] = {0.5, 0, 0, 0.20083502714510933754,0.5, 0, -1.3099840889964108325, 1.0168524885302504495, 0.5};
        double bIm[3*3] = {1.0164009489460498207, 0.63222990353105426061,-0.40805747588276356515, 0.72473428227938315401,1.4655632368643875939, -0.65055916969453956936, -0.33378487291753396219, 4.3494540357884722759, -1.4819641858104374146};
        double aEx[3*3] = {0, 0, 0, 0.7731420380418415, 0, 0, -0.5747218038549333, 1.402340197639324, 0};
        double bEx[3*3] = {0.568615416356845492, 0.349254080830621047, 0.226439028444830250,
                           0.776948749690178825, -0.317412585836045620, 0.411630323736322084,
                           0.33294188538418816, 1.22294134041525971, -0.23919309395154178};
        double u[3*3] = {1, 0, 0, 0, 1, 0, 0, 0, 1};
        double v[3*3] = {0.91042836060001214905, 0.35856464805517548161, -0.26899300865518763066,0.91042836060001214905, 0.35856464805517548161, -0.26899300865518763066,0.91042836060001214905, 0.35856464805517548161, -0.26899300865518763066};
        double gamma[3] = {0.91042836060001214905, 0.35856464805517548161, -0.26899300865518763066};
        double betaEx[3] = {0.568615416356845492, 0.349254080830621047, 0.226439028444830250};
        double betaIm[3] = {1.0164009489460498207, 0.63222990353105426061,0.091942524117236434848};
        double wEx[3*3] = {0, 0, 0, -0.2731420380418415, 0.125, 0.020833333333333333333, 0.17238160621560930, -0.201170098819662000, -0.008625858038248833};
        double wIm[3*3] = {-0.5, 0, 0, -0.20083502714510933754, -0.125, -0.041666666666666666667, 0.79313160046616038293, -0.50842624426512522477, -0.21043989439961463952};
        double d[3*3] = {1.0, 0.0, 0.0, -3.0/2.0, 2.0, -0.5, 1.0, -2.0, 1.0};

        copyButcherInVector1d(3, betaIm,_betaIm);
        copyButcherInVector1d(3, betaEx,_betaEx);
        copyButcherInVector1d(3, gamma,_gamma);
        copyButcherInVector1d(3, c,_c);
        copyButcherInVector2d(3, aEx,_aEx);
        copyButcherInVector2d(3, aIm,_aIm);
        copyButcherInVector2d(3, bEx,_bEx);
        copyButcherInVector2d(3, bIm,_bIm);
        copyButcherInVector2d(3, u,_u);
        copyButcherInVector2d(3, v,_v);
        copyButcherInVector2d(3, wEx,_wEx);
        copyButcherInVector2d(3, wIm,_wIm);
        copyButcherInVector2d(3, d,_d);

        break;
      }
    case IMEX_DIMSIM_3b : //improved version of 3d (L-stable)
      {
        double c[3] = {0.0, 0.5, 1.0};
        double aIm[3*3] = {0.43586652150845899942, 0., 0., 0.2505148808977192824, 0.43586652150845899942,0., -1.2115942877770055395, 1.00127459988118551736, 0.43586652150845899942};
        double bIm[3*3] = {0.8337907282501253221, 0.64599891214631404058, -0.3158270855129703300, 
                           0.6062575400749996560, 1.2869318100050243722, -0.4797416760942742778,
                           -0.3084167694897705268, 3.80342155052420925464, -1.1207225382551496729};
        double aEx[3*3] = {0., 0., 0.,0.753076872681821, 0., 0., -0.4897243738259477, 1.28728279647947, 0.};
        double bEx[3*3] = {0.75532493259223481151, 0.2436301241397702157, 0.2451102978132463951,
                           0.9636582659255681448, -0.4230365425268964510, 0.45036675846475872843, 
                           0.6347088027794314782, 0.7721451802448468824, 0.0396529488674507618};
        double u[3*3] = {1, 0, 0, 0, 1, 0, 0, 0, 1};
        double v[3*3] = {0.5520909620403618809, 0.7348566598712938908, -0.28694762191165577169,
                         0.5520909620403618809, 0.7348566598712938908, -0.28694762191165577169,
                         0.5520909620403618809, 0.7348566598712938908, -0.28694762191165577169};
        double gamma[3] = {0.5520909620403618809, 0.7348566598712938908, -0.28694762191165577169};
        double betaEx[3] = {0.75532493259223481151, 0.2436301241397702157, 0.2451102978132463951};
        double betaIm[3] = {0.8337907282501253221, 0.64599891214631404058, 0.1200394359954886695};
        double wEx[3*3] = {0.0, 0.0, 0.0, -0.253076872681821, 0.125, 0.02083333333333333218, 0.2024415773464777, -0.143641398239735, 0.005756317106732917};
        double wIm[3*3] = {-0.43586652150845899942, 0.0, 0.0, -0.1863814024061782818, -0.09293326075422949971, -0.033649981855224041594, 0.7744531663873610, -0.43650382144905176, -0.176425919072711023};
        double d[3*3] = {1.0, 0.0, 0.0, -3.0/2.0, 2.0, -0.5, 1.0, -2.0, 1.0};

        copyButcherInVector1d(3, betaIm,_betaIm);
        copyButcherInVector1d(3, betaEx,_betaEx);
        copyButcherInVector1d(3, gamma,_gamma);
        copyButcherInVector1d(3, c,_c);
        copyButcherInVector2d(3, aEx,_aEx);
        copyButcherInVector2d(3, aIm,_aIm);
        copyButcherInVector2d(3, bEx,_bEx);
        copyButcherInVector2d(3, bIm,_bIm);
        copyButcherInVector2d(3, u,_u);
        copyButcherInVector2d(3, v,_v);
        copyButcherInVector2d(3, wEx,_wEx);
        copyButcherInVector2d(3, wIm,_wIm);
        copyButcherInVector2d(3, d,_d);

        break;
      }
   case IMEX_DIMSIM_3c :
      {
        double c[3] = {0.0, 0.5, 1.0};
        double aIm[3*3] = {0.5, 0, 0, 0.20083502714510933754,0.5, 0, -1.3099840889964108325, 1.0168524885302504495, 0.5};
        double bIm[3*3] = {1.0164009489460498207, 0.63222990353105426061,-0.40805747588276356515, 0.72473428227938315401,1.4655632368643875939, -0.65055916969453956936, -0.33378487291753396219, 4.3494540357884722759, -1.4819641858104374146};
        double aEx[3*3] = {0, 0, 0, 0.87413482629106, 0, 0, -0.83771344595655, 1.42486604891578, 0};
        double bEx[3*3] = {0.67557077299161957388, 0.34319478432324784844, 0.22643902844483025018, 0.88390410632495290721, -0.32347188234341881822, 0.31063753548710358351, 0.4173713907425062405, 1.2844595977372545151, -0.0437790056792930832};
        double u[3*3] = {1, 0, 0, 0, 1, 0, 0, 0, 1};
        double v[3*3] = {0.91042836060001214905, 0.35856464805517548161, -0.26899300865518763066,0.91042836060001214905, 0.35856464805517548161, -0.26899300865518763066,0.91042836060001214905, 0.35856464805517548161, -0.26899300865518763066};
        double gamma[3] = {0.91042836060001214905, 0.35856464805517548161, -0.26899300865518763066};
        double betaEx[3] = {0.67557077299161957388, 0.34319478432324784844, 0.22643902844483025018};
        double betaIm[3] = {1.0164009489460498207, 0.63222990353105426061,0.091942524117236434848};
        double wEx[3*3] = {0, 0, 0, -0.37413482629106, 0.125, 0.020833333333333333333, 0.41284739704077, -0.21243302445789, -0.011441589447805833333};
        double wIm[3*3] = {-0.5, 0, 0, -0.20083502714510933754, -0.125, -0.041666666666666666667, 0.79313160046616038293, -0.50842624426512522477, -0.21043989439961463952};
        double d[3*3] = {1.0, 0.0, 0.0, -3.0/2.0, 2.0, -0.5, 1.0, -2.0, 1.0};

        copyButcherInVector1d(3,betaIm,_betaIm);
        copyButcherInVector1d(3,betaEx,_betaEx);
        copyButcherInVector1d(3,gamma,_gamma);
        copyButcherInVector1d(3,c,_c);
        copyButcherInVector2d(3, aEx,_aEx);
        copyButcherInVector2d(3, aIm,_aIm);
        copyButcherInVector2d(3, bEx,_bEx);
        copyButcherInVector2d(3, bIm,_bIm);
        copyButcherInVector2d(3, u,_u);
        copyButcherInVector2d(3, v,_v);
        copyButcherInVector2d(3, wEx,_wEx);
        copyButcherInVector2d(3, wIm,_wIm);
        copyButcherInVector2d(3, d,_d);

        break;
      }
   case IMEX_DIMSIM_3d :
      {
        double c[3] = {0.0, 0.5, 1.0};
        double aIm[3*3] = {0.43586652150845899942, 0, 0, 0.25051488089771928169, 0.43586652150845899942, 0, -1.2115942877770056339, 1.0012745998811855584, 0.43586652150845899942};
	double bIm[3*3] = {0.83379072825012537509, 0.64599891214631396702, -0.31582708551297030201, 0.60625754007499970901, 1.2869318100050242986, -0.47974167609427424920, -0.30841676948977051489, 3.8034215505242093042, -1.1207225382551496737};
        double aEx[3*3] = {0., 0., 0., 0.7609384479121202, 0., 0., -0.4430150815943605, 1.2601935514993, 0.};
        double bEx[3*3] = {0.74769894318026885936, 0.2514033185662122451, 0.2451102978132463951, 0.9560322765136021927, -0.4152633481004544215, 0.4425051832344595284,0.6541720583476355260, 0.69865063973077891179, 0.0742113915763735618};
        double u[3*3] = {1, 0, 0, 0, 1, 0, 0, 0, 1};
        double v[3*3] = {0.55209096204036338484, 0.73485665987129198860, -0.28694762191165537345, 0.55209096204036338484, 0.73485665987129198860, -0.28694762191165537345, 0.55209096204036338484, 0.73485665987129198860, -0.28694762191165537345};
        double gamma[3] = {0.55209096204036338484, 0.73485665987129198860, -0.28694762191165537345};
        double betaEx[3] = {0.74769894318026885936, 0.2514033185662122451, 0.2451102978132463951};
        double betaIm[3] = {0.83379072825012537509, 0.64599891214631396702,0.12003943599548869740};
        double wEx[3*3] = {0.0, 0.0, 0.0, -0.2609384479121202, 0.125, 1.0/48.0, 0.1828215300950605, -0.13009677574965, 0.00914247272925416667};
        double wIm[3*3] = {-0.43586652150845899942, 0, 0, -0.18638140240617828111, -0.092933260754229499708, -0.033649981855224041594, 0.77445316638736107612, -0.43650382144905177861, -0.17642591907271102784};
        double d[3*3] = {1.0, 0.0, 0.0, -3.0/2.0, 2.0, -0.5, 1.0, -2.0, 1.0};

        copyButcherInVector1d(3,betaIm,_betaIm);
        copyButcherInVector1d(3,betaEx,_betaEx);
        copyButcherInVector1d(3,gamma,_gamma);
        copyButcherInVector1d(3,c,_c);
        copyButcherInVector2d(3, aEx,_aEx);
        copyButcherInVector2d(3, aIm,_aIm);
        copyButcherInVector2d(3, bEx,_bEx);
        copyButcherInVector2d(3, bIm,_bIm);
        copyButcherInVector2d(3, u,_u);
        copyButcherInVector2d(3, v,_v);
        copyButcherInVector2d(3, wEx,_wEx);
        copyButcherInVector2d(3, wIm,_wIm);
        copyButcherInVector2d(3, d,_d);

        break;
      }
    case IMEX_DIMSIM_5a : //(L-stable) maximized combined stability region
      {
        double c[5] = {0.0, 0.25, 0.5, 0.75, 1.0};
        double aIm[5*5] = {0.278053841136452, 0, 0, 0, 0, 0.220452276182580, 0.278053841136452, 0, 0, 0, 2.294819895736366, -0.602366708071285, 0.278053841136452, 0, 0, 5.054620901153854, -1.529876218309763, 0.097119141498823, 0.278053841136452, 0, 9.345167780108133, -1.412133513099773, -1.883401998517870, 0.782533955446870, 0.278053841136452};
        double bIm[5*5] = {6.044855283302179, -2.020000467205476, 0.032934533641225, 0.593578985923315, -0.226664851205853, 5.853954219943505, -1.072092372634326, -1.839270544389963, 2.410922952843391, -0.899263047489796, 6.004175007913425, -2.014097375842605, 0.610845429880394, -0.963490004887004, -0.405182760273902, 6.002703177071046, -2.556003283230891, 3.151551366098853, -5.493514217893924, 0.448102618067392, 4.481882795290198, 2.672564354868939, -1.413660973235832, -8.058154793746990, 0.909905877341711};
        double aEx[5*5] = {0., 0., 0., 0., 0, 0.379452184521517, 0., 0., 0., 0., -0.722500230838067, 0.933891204165547, 0., 0., 0., -0.287769806426527, 1.488893099112468, 0.228633938432498, 0., 0., 10.352287505519904, 0.196082846459819, 0.841884463295958, -0.149700985535253, 0. };
        double bEx[5*5] = {-1.806941830867166, 2.072752117806156, 0.129037505613424, 0.166465251500658, 0.117403740739418, -1.719789053089388, 1.630391006695045, 1.037370838946757, -0.796729192943787, 0.398021000662346, -1.993610812810491, 3.086652583078336, -2.143207869375380, 2.849821737600573, -0.832329827028028, -1.354629622142124, 0.323182135748447, 2.176579285026333, 0.332003779925258, -1.478176024883865, 5.110845566999797, -29.531786629370579, 55.279065703472341, -43.560375677708841, 3.140413991659348};
        double u[5*5] = {1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0,0, 1, 0, 0, 0, 0, 0, 1};
        double v[5*5] = {-0.079385465132435, 0.554317572910577, -1.569589549144155, 2.332074592443682, -0.237417151077669, -0.079385465132435, 0.554317572910577, -1.569589549144155, 2.332074592443682, -0.237417151077669, -0.079385465132435, 0.554317572910577, -1.569589549144155, 2.332074592443682, -0.237417151077669, -0.079385465132435, 0.554317572910577, -1.569589549144155, 2.332074592443682, -0.237417151077669, -0.079385465132435, 0.554317572910577, -1.569589549144155, 2.332074592443682, -0.237417151077669};
        double gamma[5] = {-0.079385465132435, 0.554317572910577, -1.569589549144155, 2.332074592443682, -0.237417151077669};
        double betaEx[5] = {0};
        double betaIm[5] = {0};
        double wEx[5*5] = { 0, 0, 0, 0, 0, -0.129452184521517, 0.031250000000000, 0.002604166666667, 0.000162760416667, 0.000008138020833, 0.288609026672520, -0.108472801041387, -0.008350766796840, 0.000172158322486, 0.000108416145155, -0.679757231118439, -0.205290243994366, -0.004794651651327, 0.004543060920384, 0.001139805319982, -10.240553829740428, 0.142312795888506, 0.097406921984592, 0.034142625147459, 0.008082608328314};
        double wIm[5*5] = { -0.278053841136452, 0, 0, 0, 0, -0.248506117319032, -0.038263460284113, -0.006085015868847, -0.000561338127960, -0.000037118138206,-1.470507028801533, 0.136564756449595, 0.004900562818504, -0.001619958388074, -0.000365640421568, -3.149917665479366, 0.406619102975690, 0.027778596315200, -0.004406329750951, -0.001692120959916, -6.110220065073812, 0.929780069812273, 0.087106493228110, -0.016782586272280, -0.008434321001423};
        double d[5*5] = {1.0, 0, 0, 0, 0, -25.0/12.0, 4.0, -3.0, 4.0/3.0, -1.0/4.0, 35.0/12.0, -26.0/3.0, 19.0/2.0, -14.0/3.0, 11.0/12.0, -5.0/2.0, 9.0, -12.0, 7.0, -3.0/2.0, 1.0,-4.0, 6.0, -4.0, 1.0};

        copyButcherInVector1d(5, betaIm,_betaIm);
        copyButcherInVector1d(5, betaEx,_betaEx);
        copyButcherInVector1d(5, gamma,_gamma);
        copyButcherInVector1d(5, c,_c);
        copyButcherInVector2d(5, aEx,_aEx);
        copyButcherInVector2d(5, aIm,_aIm);
        copyButcherInVector2d(5, bEx,_bEx);
        copyButcherInVector2d(5, bIm,_bIm);
        copyButcherInVector2d(5, u,_u);
        copyButcherInVector2d(5, v,_v);
        copyButcherInVector2d(5, wEx,_wEx);
        copyButcherInVector2d(5, wIm,_wIm);
        copyButcherInVector2d(5, d,_d);

        break;
      }
  default : Msg::Fatal("Diganally IMEX multistage integration methods not implemented for the input type");
  }
}


int dgIMEXDIMSIM::iterate (dgDofContainer &solution, double dt, double t)
{
  if (dt <= 0)
    Msg::Fatal("Time step must be greater than zero");
  int nsteps = _c.size();
  int maxIterNewton = 0;
  _dt = dt;
  function::getDT()->set(dt);

  for (int istep = 0; istep < nsteps; istep++) {
    function::getSubDT()->set(dt*_c[istep]);
    // IMPLICIT //
    _X.setAll(0.0);
    for (int j =0; j < nsteps; j++){
      if(_u[istep*nsteps+j]!=0) _X.axpy(_LOld[j],_u[istep*nsteps+j]);
    }
    for (int j = 0; j < istep; j++){
      if (_aIm[istep*nsteps+j]!=0) _X.axpy (_KIm[j], dt*_aIm[istep*nsteps+j]);
      if (_aEx[istep*nsteps+j]!=0) _X.axpy (_KEx[j], dt*_aEx[istep*nsteps+j]);
    }
    _claw->setImexMode(dgConservationLaw::IMEX_IMPLICIT);
    if(_aIm[istep*nsteps+istep]!=0){

      _KIm[istep].copy (_newton->solve (&_X, 1.0/(dt*_aIm[istep*nsteps+istep]), NULL, t+dt*_c[istep]));
      _KIm[istep].scale (1.0/(dt*_aIm[istep*nsteps+istep]));

    }
    maxIterNewton = std::max(_newton->hasConverged(), maxIterNewton);

    if (_aIm[istep*nsteps+istep]!=0) _X.axpy (_KIm[istep], dt*_aIm[istep*nsteps+istep]);
    // EXPLICIT //
    _claw->setImexMode(dgConservationLaw::IMEX_ALL);
    _assembler->solve (_X, 1, NULL, t+dt*_c[istep], _KEx[istep]);
    _claw->setImexMode(dgConservationLaw::IMEX_IMPLICIT);
    _assembler->solve (_X, 1, NULL, t+dt*_c[istep], _buf);
    _KEx[istep].axpy(_buf, -1);
    if(istep == nsteps-1) solution.copy(_X);
  }
  
  // update _L
  for (int istep = 0; istep < nsteps; istep++){
    _buf.setAll(0);

    for(int j=0; j<nsteps; j++){
      if(_v[istep*nsteps+j]!=0) _buf.axpy(_LOld[j], _v[istep*nsteps+j]);
    }
    for(int j=0; j<nsteps; j++){
      if(_bIm[istep*nsteps+j]!=0) _buf.axpy(_KIm[j], dt*_bIm[istep*nsteps+j]);
      if(_bEx[istep*nsteps+j]!=0) _buf.axpy(_KEx[j], dt*_bEx[istep*nsteps+j]);
    }

    _L[istep].copy(_buf); 
  }
  // compute solution 
  //_buf.setAll(0);
  //for(int istep = 0; istep < nsteps; istep++){
  //  if(_betaIm[istep]!=0) _buf.axpy(_KIm[istep], dt*_betaIm[istep]);
  //  if(_betaEx[istep]!=0) _buf.axpy(_KEx[istep], dt*_betaEx[istep]);
  //  if(_gamma[istep]!=0) _buf.axpy(_LOld[istep], _gamma[istep]);
  //}

  //solution.copy(_buf);
  //Msg::Exit(0); 
  // update _LOld
  for(int j=0; j<nsteps; j++){
    _LOld[j].copy(_L[j]);
    _LOld[j].copy(_L[j]);
  }
  return maxIterNewton;
}


double dgIMEXDIMSIM::starter(dgDofContainer &solution, double sdt, double dt, double t, int order)
{	
  dgIMEXRK stimeIter(*_claw,*_dofIm,order);
  std::vector<dgDofContainer> SDersX;
  std::vector<dgDofContainer> SDersY;  
  int nsteps;
  double sc;
 
  initialized = true;
  nsteps = _c.size();
  SDersX.resize(nsteps, _X);
  SDersY.resize(nsteps, _X);

  _X.copy(solution);
  _claw->setImexMode(dgConservationLaw::IMEX_ALL);
  _assembler->solve (_X, 1, NULL, t, _KEx[0]);
  _claw->setImexMode(dgConservationLaw::IMEX_IMPLICIT);
  _assembler->solve (_X, 1, NULL, t, _KIm[0]);
  _KEx[0].axpy(_KIm[0], -1);
  
  for(int i=0; i<nsteps; i++){
    SDersX[i].setAll(0);
    SDersY[i].setAll(0);
    if(_d[i*nsteps+0]!=0) SDersX[i].axpy(_KEx[0], sdt*_d[i*nsteps+0]);
    if(_d[i*nsteps+0]!=0) SDersY[i].axpy(_KIm[0], sdt*_d[i*nsteps+0]);
  }
  
  for(int i=1; i<nsteps; i++){   
    stimeIter.iterate(_X,sdt,t);
    t = t + sdt;
    _claw->setImexMode(dgConservationLaw::IMEX_ALL);
    _assembler->solve (_X, 1, NULL, t, _KEx[i]);
    _claw->setImexMode(dgConservationLaw::IMEX_IMPLICIT);
    _assembler->solve (_X, 1, NULL, t, _KIm[i]);
    _KEx[i].axpy(_KIm[i], -1);

    for(int j=0; j<nsteps; j++){
      if(_d[j*nsteps+i]!=0) SDersX[j].axpy(_KEx[i], sdt*_d[j*nsteps+i]);
      if(_d[j*nsteps+i]!=0) SDersY[j].axpy(_KIm[i], sdt*_d[j*nsteps+i]);
    }
  }

  for(int i=0; i<nsteps; i++){
    sc = pow(dt/sdt,i+1);
    SDersX[i].scale(sc);
    SDersY[i].scale(sc); 
  }
  
  for(int i=0; i<nsteps; i++){
    _LOld[i].copy(solution);  
    for(int j=0; j<nsteps; j++){
      if(_wEx[i*nsteps+j]!=0) _LOld[i].axpy(SDersX[j], _wEx[i*nsteps+j]);
      if(_wIm[i*nsteps+j]!=0) _LOld[i].axpy(SDersY[j], _wIm[i*nsteps+j]);
    }

//    dgFunctionIntegrator integrator(&_dofIm->getGroups(), _LOld[i].getFunction());
//    fullMatrix<double> bufVal(_claw->getNbFields(),1);
//    integrator.compute(bufVal);
//    bufVal.print();
  }

//	fullMatrix<double> sol(1,1);
//	_LO1d[0].getAtIntegrationPoints()

  return t; // return current time
}
