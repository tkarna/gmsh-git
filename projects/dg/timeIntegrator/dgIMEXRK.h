#ifndef _DG_IMEX_RK_H_
#define _DG_IMEX_RK_H_

class dgConservationLaw;
class dgDofManager;
class dgAssemblerExplicit;
class dgNewton;

#include "dgDofContainer.h"

class dgIMEXRK
{
 private:
  double _dt;
  dgConservationLaw *_claw;
  dgAssemblerExplicit *_assembler;
  bool ownAssembler;
  dgNewton *_newton;
  bool ownNewton;
  std::vector<dgDofContainer> _KIm;
  std::vector<dgDofContainer> _KEx;
  dgDofContainer _buf;
  dgDofContainer _X;
  std::vector<double> _aIm, _bIm,_aEx, _bEx, _c;
  inline void setCoef(int order);
 public:
  dgIMEXRK(dgConservationLaw &claw, dgDofManager &dofIm, int order, dgDofManager *dofEx=NULL);
  int iterate(dgDofContainer &solution, double dt, double t);
  inline dgNewton &getNewton() {return *_newton;}
  inline dgAssemblerExplicit * getAssembler() {return _assembler;}
};

#endif
