#include "dgIMEXRK.h"
#include "dgNewton.h"
#include "dgAssemblerExplicit.h"
#include "dgDofContainer.h"
#include "dgDofManager.h"
#include "dgConservationLaw.h"
#include "GmshConfig.h"
#include<sstream>
#ifdef HAVE_MPI
#include "mpi.h"
#endif

static void copyButcherInVector (int n, const double *a, const double *b, const double *c, std::vector<double> &va, std::vector<double> &vb, std::vector<double> &vc)
{
  va.resize(n * n);
  vb.resize(n);
  vc.resize(n);
  for (int i = 0; i < n; i ++)
  {
    vb[i] = b[i];
    vc[i] = c[i];
    for (int j = 0; j < n; ++j)
      va[i * n + j] = a[i * n + j];
  }
}

dgIMEXRK::dgIMEXRK (dgConservationLaw &claw, dgDofManager &dofIm, int order, dgDofManager *dofEx) :
  _claw(&claw),
  _buf(dofIm.getGroups(),claw.getNbFields()),
  _X(dofIm.getGroups(),claw.getNbFields())
{
  _dt = -1;
  _claw->setImexMode(dgConservationLaw::IMEX_IMPLICIT);
  _newton = new dgNewton(dofIm, claw);
  ownNewton = true;
  _assembler = new dgAssemblerExplicit(dofEx, claw);
  ownAssembler = true;
  setCoef(order);
  _KIm.resize(_c.size(), _X); 
  _KEx.resize(_c.size(), _X); 
}

void dgIMEXRK::setCoef(int order) {
  switch (order) {
    case 2 : //ARS(2,2,2)
      {
        double Gamma = (2 - sqrt(2.))/2;
	double delta = 1 - 1.0/(2*Gamma);
        double aIm[3*3] = {0 , 0       , 0, 
			   0 , Gamma   , 0,
			   0 , 1-Gamma , Gamma};
        double bIm[3] =   {0 , 1-Gamma , Gamma};
	double aEx[3*3] = {0     , 0       , 0, 
			   Gamma , 0       , 0,
			   delta , 1-delta , 0};
        double bEx[3] =   {delta , 1-delta , 0};
        double c[3] =     {0 , Gamma   , 1};
        copyButcherInVector(3, aIm, bIm, c, _aIm, _bIm, _c);
        copyButcherInVector(3, aEx, bEx, c, _aEx, _bEx, _c);
        break;
      }
    case 22 :
      {
        double aIm[2*2] = {0 , 0 ,
                           0 , 1 };
        double bIm[2] =   {0 , 1};
        double aEx[2*2] = {0 , 0 ,
                           1 , 0 };
        double bEx[2] =   {0 , 1};
        double c[2] =     {0 , 1};
        copyButcherInVector(2, aIm, bIm, c, _aIm, _bIm, _c);
        copyButcherInVector(2, aEx, bEx, c, _aEx, _bEx, _c);
        break;
      }
    case 3 : //ARS(3,4,3)
    {
      double Gamma = 0.435866521508459;
      double b1 = -3./2*Gamma*Gamma + 4*Gamma - 1./4;
      double b2 = 3./2*Gamma*Gamma - 5*Gamma + 5./4;
      double aIm[4*4] = {0 , 0           , 0     , 0,
			 0 , Gamma       , 0     , 0,
			 0 , (1-Gamma)/2 , Gamma , 0,
			 0 , b1          , b2    , Gamma };
      double bIm[4]   = {0 , b1          , b2    , Gamma };
      double a42 = 0.5529291479;
      double a43 = a42;
      double a41 = 1 - a42 - a43;
      double a31 = (  1 - 9./2*Gamma + 3./2*Gamma*Gamma )*a42 + ( 11./4 - 21./2*Gamma + 15./4*Gamma*Gamma )*a43 - 7./2 + 13*Gamma - 9./2*Gamma*Gamma;
      double a32 = ( -1 + 9./2*Gamma - 3./2*Gamma*Gamma )*a42 + ( -11./4 + 21./2*Gamma - 15./4*Gamma*Gamma )*a43 + 4 - 25./2*Gamma + 9./2*Gamma*Gamma;
      double aEx[4*4] = { 0     , 0   , 0   , 0,
			  Gamma , 0   , 0   , 0,
			  a31   , a32 , 0   , 0,
			  a41   , a42 , a43 , 0 };
      double bEx[4]   = { 0     , b1  , b2  , Gamma };
      double c[4] = { 0 , Gamma , (1+Gamma)/2 , 1};
      copyButcherInVector(4, aIm, bIm, c, _aIm, _bIm, _c);
      copyButcherInVector(4, aEx, bEx, c, _aEx, _bEx, _c);
      break;
    }
    case 4 : //ARK4(3)6L[2]SA or KC(5,6,4)
    {
      double Gamma = 0.25;
      double aIm[6*6] = {0                          , 0                     , 0                     , 0                 , 0            , 0,
                         1./4.                      , Gamma                 , 0                     , 0                 , 0            , 0,
                         8611./62500.               , -1743./31250.         , Gamma                 , 0                 , 0            , 0,
                         5012029./34652500.         , -654441./2922500.     , 174375./388108.       , Gamma             , 0            , 0,
                         15267082809./155376265600. , -71443401./120774400. , 730878875./902184768. , 2285395./8070912. , Gamma        , 0,
                         82889./524892.             , 0                     , 15625./83664.         , 69875./102672.    , -2260./8211. , Gamma};
      double bIm[6]   = {82889./524892., 0, 15625./83664., 69875./102672., -2260./8211., Gamma};
      double aEx[6*6] = {0, 0, 0, 0, 0, 0,
                         0.500000000000000, 0, 0, 0, 0, 0,
                         0.221776000000000, 0.110224000000000, 0, 0, 0, 0,
                         -0.048846595153119, -0.177720652326401, 0.846567247479520, 0, 0, 0,
                         -0.155416858424915, -0.356705009822199, 1.058725879868443, 0.303395988378672, 0, 0,
                         0.201424350672676, 0.008742057842904, 0.159939957071681, 0.403829060522078, 0.226064573890661, 0};
      double bEx[6]   = {82889./524892., 0, 15625./83664., 69875./102672., -2260./8211., Gamma};
      double c[6] = {0, 1./2., 83./250., 31./50., 17./20., 1. };
      copyButcherInVector(6, aIm, bIm, c, _aIm, _bIm, _c);
      copyButcherInVector(6, aEx, bEx, c, _aEx, _bEx, _c);
      break;
    }
    case 5 : //ARK5(4)8L[2]SA or KC(7,8,5)
    {
      double Gamma = 41./200.;
      double aIm[8*8] = {0, 0, 0, 0, 0, 0, 0, 0,    
                         41./200., Gamma, 0, 0, 0, 0,0, 0,
                         41./200., -567603406766./11931857230679., Gamma, 0, 0, 0, 0, 0,
                         683785636431./9252920307686., 0, -110385047103./1367015193373., Gamma, 0, 0, 0, 0,
                         3016520224154./10081342136671., 0, 30586259806659./12414158314087. , -22760509404356./11113319521817., Gamma        , 0, 0, 0,
                         218866479029./1489978393911., 0, 638256894668./5436446318841., -1179710474555/5321154724896, -60928119172./8023461067671., Gamma, 0, 0,
                         1020004230633./5715676835656., 0, 25762820946817./25263940353407.,-2161375909145./9755907335909., -211217309593./5846859502534., -4269925059573./7827059040749., Gamma, 0,
                         -872700587467./9133579230613., 0, 0, 22348218063261./9555858737531., -1143369518992./8141816002931., -39379526789629./19018526304540., 32727382324388./42900044865799., Gamma};
      double bIm[8]   = {-872700587467./9133579230613., 0, 0, 22348218063261./9555858737531., -1143369518992./8141816002931., -39379526789629./19018526304540., 32727382324388./42900044865799., Gamma};
      double aEx[8*8] = {0, 0, 0, 0, 0, 0, 0, 0,
                         41./100., 0, 0, 0, 0, 0, 0, 0,
                         367902744464./2072280473677., 677623207551./8224143866563., 0, 0, 0, 0, 0, 0,
                         1268023523408./10340822734521., 0, 1029933939417./13636558850479., 0, 0, 0, 0, 0,
                         14463281900351./6315353703477., 0, 66114435211212./5879490589093.,-54053170152839./4284798021562., 0, 0, 0, 0,
                         14090043504691./34967701212078., 0, 15191511035443./11219624916014., -18461159152457./12425892160975.,  -281667163811./9011619295870., 0, 0, 0,
                         19230459214898./13134317526959., 0, 21275331358303./2942455364971., -38145345988419./4862620318723., -1./8., -1./8., 0, 0, 
                         -19977161125411./11928030595625., 0, -40795976796054./6384907823539., 177454434618887./12078138498510.,  782672205425./8267701900261., -69563011059811./9646580694205., 7356628210526./4942186776405., 0};

      double bEx[8]   = {-872700587467./9133579230613., 0, 0, 22348218063261./9555858737531., -1143369518992./8141816002931., -39379526789629./19018526304540., 32727382324388./42900044865799., Gamma};
      double c[8] = {0, 41./100., 2935347310677./11292855782101., 1426016391358./7196633302097., 92./100., 24./100., 3./5., 1};
      copyButcherInVector(8, aIm, bIm, c, _aIm, _bIm, _c);
      copyButcherInVector(8, aEx, bEx, c, _aEx, _bEx, _c);
      break;
    }
  default : Msg::Fatal("IMEX Runge-Kutta not implemented for order %i", order);
  }
}

int dgIMEXRK::iterate (dgDofContainer &solution, double dt, double t)
{
  if (dt <= 0)
    Msg::Fatal("Time step must be greater than zero (currently %e)",dt);
  if (abs(_dt-dt)/dt > 1e-12)
    _newton->forceReassembleMatrix();
  _dt = dt;
  int nsteps = _c.size();
  int maxIterNewton = 0;
  function::getDT()->set(dt);

  _X.copy(solution);

  function::getSubDT()->set(dt*_c[0]);
  _claw->setImexMode(dgConservationLaw::IMEX_ALL);
  _assembler->solve (_X, 1, NULL, t, _KEx[0]);
  _claw->setImexMode(dgConservationLaw::IMEX_IMPLICIT);
  _assembler->solve (_X, 1, NULL, t, _KIm[0]);
  _KEx[0].axpy(_KIm[0], -1);

  for (int istep = 1; istep < nsteps; istep++) {
    function::getSubDT()->set(dt*_c[istep]);
    // IMPLICIT //
    _X.copy(solution);
    for (int j = 0; j < istep; j ++){
      if (_aIm[istep*nsteps+j]!=0) _X.axpy (_KIm[j], dt*_aIm[istep*nsteps+j]);
      if (_aEx[istep*nsteps+j]!=0) _X.axpy (_KEx[j], dt*_aEx[istep*nsteps+j]);
    }
    _KIm[istep].copy (_newton->solve (&_X, 1.0/(dt*_aIm[istep*nsteps+istep]), NULL, t + dt * _c[istep]));
    _KIm[istep].scale (1.0/(dt*_aIm[istep*nsteps+istep]));

    maxIterNewton = std::max(_newton->hasConverged(), maxIterNewton);
//    if(istep == 1){
//      solution.copy(_X);
//      return maxIterNewton;
//    }
    if (_aIm[istep*nsteps+istep]!=0) _X.axpy (_KIm[istep], dt*_aIm[istep*nsteps+istep]);
    // EXPLICIT //
    _claw->setImexMode(dgConservationLaw::IMEX_ALL);
    _assembler->solve (_X, 1, NULL, t + dt*_c[istep], _KEx[istep]);
    _claw->setImexMode(dgConservationLaw::IMEX_IMPLICIT);
    _assembler->solve (_X, 1, NULL, t + dt*_c[istep], _buf);
    _KEx[istep].axpy(_buf, -1);
  }
  for (int istep = 0; istep < nsteps; istep++){
    if (_bIm[istep]!=0) solution.axpy (_KIm[istep], dt*_bIm[istep]);
    if (_bEx[istep]!=0) solution.axpy (_KEx[istep], dt*_bEx[istep]);
  }
  return maxIterNewton;
}
