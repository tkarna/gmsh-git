#include "dgIMEXLMM.h"
#include "dgNewton.h"
#include "dgAssemblerExplicit.h"
#include "dgIMEXRK.h"
#include "dgDofContainer.h"
#include "dgDofManager.h"
#include "dgConservationLaw.h"
#include "GmshConfig.h"
#include<sstream>
#ifdef HAVE_MPI
#include "mpi.h"
#endif

static void copyButcherInVector1d (int n, const double *v,std::vector<double> &vv)
{
  vv.resize(n);
  for (int i = 0; i < n; i ++)
  {
    vv[i] = v[i];
  }
}

dgIMEXLMM::dgIMEXLMM(dgConservationLaw &claw, dgDofManager &dofIm, IMEX_LMM_TYPE type, dgDofManager *dofEx) :
  _claw(&claw),
  _dofIm(&dofIm),
  _buf(dofIm.getGroups(),claw.getNbFields()),
  _X(dofIm.getGroups(),claw.getNbFields())
{
  _dt = -1;
  _claw->setImexMode(dgConservationLaw::IMEX_IMPLICIT);
  _newton = new dgNewton(dofIm, claw);
  ownNewton = true;
  _assembler = new dgAssemblerExplicit(dofEx, claw);
  ownAssembler = true;
  initialized = false;
  setCoef(type);
  _previousX.resize (_k);
  _previousF.resize (_k);
  _previousG.resize (_k);
  for (int i = 0; i < _k; i++){
    _previousF[i] = new dgDofContainer (_X);
    _previousG[i] = new dgDofContainer (_X);
    _previousX[i] = new dgDofContainer (_X);
  }
 }


 dgIMEXLMM::~dgIMEXLMM()
 {
   for (size_t i = 0; i < _previousF.size(); i++){
	 delete _previousX[i];
     delete _previousF[i];
     delete _previousG[i];
   }
 }

void dgIMEXLMM::setCoef(IMEX_LMM_TYPE type) {
  switch (type) {
    case IMEX_BDF2 :
      {
        double a[2] = {4.0/3.0, -1.0/3.0};
        double bIm[3] = {2.0/3.0, 0, 0};
        double bEx[2] = {4.0/3.0, -2.0/3.0};
        _k = 2;
        copyButcherInVector1d(2,a,_a);
        copyButcherInVector1d(2,bEx,_bEx);
        copyButcherInVector1d(3,bIm,_bIm);
        break;        
      }
    case IMEX_BDF3 :
      {
        double a[3] = {18.0/11.0, -9.0/11.0, 2.0/11.0};
        double bIm[4] = {6.0/11.0, 0, 0, 0};
        double bEx[3] = {18.0/11.0, -18.0/11.0, 6.0/11.0};
        _k = 3;
        copyButcherInVector1d(3,a,_a);
        copyButcherInVector1d(3,bEx,_bEx);
        copyButcherInVector1d(4,bIm,_bIm);
        break;
      }
    case IMEX_BDF4 :
      {
        double a[4] = {48.0/25.0, -36.0/25.0, 16.0/25.0, -3.0/25.0};
        double bIm[5] = {12.0/25.0, 0, 0, 0, 0};
        double bEx[4] = {48.0/25.0, -72.0/25.0, 48.0/25.0, -12.0/25.0};
        _k = 4;
        copyButcherInVector1d(4,a,_a);
        copyButcherInVector1d(4,bEx,_bEx);
        copyButcherInVector1d(5,bIm,_bIm);
        break;
      }
    case IMEX_BDF5 :
      {
        double a[5] = {300.0/137.0, -300.0/137.0, 200.0/137.0, -75.0/137.0, 12.0/137.0};
        double bIm[6] = {60.0/137.0, 0, 0, 0, 0, 0};
        double bEx[5] = {300.0/137.0, -600.0/137.0, 600.0/137.0, -300.0/137.0, 60.0/137.0};
        _k = 5;
        copyButcherInVector1d(5,a,_a);
        copyButcherInVector1d(5,bEx,_bEx);
        copyButcherInVector1d(6,bIm,_bIm);
        break;
      }
  default : Msg::Fatal("IMEX LMM not implemented for the input type");
  }
}

int dgIMEXLMM::iterate (dgDofContainer &solution, double dt, double t)
{
  if (dt <= 0)
    Msg::Fatal("Time step must be greater than zero");
  if (abs(_dt-dt)/dt > 1e-12)
    _newton->forceReassembleMatrix();

  int maxIterNewton = 0;
  //int timeDir = 1;
  _dt = dt;
  function::getDT()->set(dt);

  /* compute RHS */
  _X.setAll(0.0);
  for (int j=0; j<_k; j++){
    if (_a[j]!=0) _X.axpy (*_previousX[j], _a[j]);
    if (_bEx[j]!=0) _X.axpy (*_previousF[j], dt*_bEx[j]);
    if (_bIm[j+1]!=0) _X.axpy (*_previousG[j], dt*_bIm[j+1]);
  }

  /* shift previous info*/
  dgDofContainer *Xtmp = _previousX[_k-1];
  dgDofContainer *Ftmp = _previousF[_k-1];
  dgDofContainer *Gtmp = _previousG[_k-1];
  for (int j=_k-1; j>0; j--){
	_previousX[j] = _previousX[j-1];
	_previousF[j] = _previousF[j-1];
	_previousG[j] = _previousG[j-1];
  }
  _previousX[0] = Xtmp;
  _previousF[0] = Ftmp;
  _previousG[0] = Gtmp;

  /* solve for g(x) at t+h*/
  _claw->setImexMode(dgConservationLaw::IMEX_IMPLICIT);
  if(_bIm[0]!=0){
	_previousG[0]->copy (_newton->solve (&_X, 1.0/(dt*_bIm[0]), NULL, t+dt));
	_previousG[0]->scale (1.0/(dt*_bIm[0]));
  }
  maxIterNewton = std::max(_newton->hasConverged(), maxIterNewton);

  /* update solution*/
  if (_bIm[0]!=0) _X.axpy (*_previousG[0], dt*_bIm[0]);
  solution.copy(_X);
  _previousX[0]->copy(_X);

  /* compute f(x) at t+h */
  _claw->setImexMode(dgConservationLaw::IMEX_ALL);
  _assembler->solve (_X, 1, NULL, t+dt, *_previousF[0]);
  _claw->setImexMode(dgConservationLaw::IMEX_IMPLICIT);
  _assembler->solve (_X, 1, NULL, t+dt, _buf);
  _previousF[0]->axpy(_buf, -1);

  return maxIterNewton;
}

double dgIMEXLMM::starter(dgDofContainer &solution, double dt, double t, int order)
{
  dgIMEXRK stimeIter(*_claw,*_dofIm,order);
  double sdt;//sub steps
  double tt;
  int substeps;

  substeps = 2;
  initialized = true;
  _X.copy(solution);
  //stimeIter.iterate(_X,t+(-cmin+_c[i])*dt-tt,tt);
  _claw->setImexMode(dgConservationLaw::IMEX_ALL);
  _assembler->solve (_X, 1, NULL, t, *_previousF[_k-1]);
  _claw->setImexMode(dgConservationLaw::IMEX_IMPLICIT);
  _assembler->solve (_X, 1, NULL, t, *_previousG[_k-1]);
  _previousF[_k-1]->axpy(*_previousG[_k-1], -1);
  _previousX[_k-1]->copy(_X);

  for(int i=1; i<_k; i++){
    sdt = dt/(double)substeps;
    tt = t;
    for(int j=0; j<substeps; j++){
      stimeIter.iterate(_X, sdt, tt);
      tt = tt+sdt;
    }
      //stimeIter.iterate(_X,t+(-cmin+_c[i])*dt-tt,tt);
    _claw->setImexMode(dgConservationLaw::IMEX_ALL);
    _assembler->solve (_X, 1, NULL, tt, *_previousF[_k-i-1]);
    _claw->setImexMode(dgConservationLaw::IMEX_IMPLICIT);
    _assembler->solve (_X, 1, NULL, tt, *_previousG[_k-i-1]);
    _previousF[_k-i-1]->axpy(*_previousG[_k-i-1], -1);
    _previousX[_k-i-1]->copy(_X);
  }

  t = t+(_k-1)*dt;
  return t; // return current time
}
