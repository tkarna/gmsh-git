#ifndef _DG_ASSEMBLER_EXPLICIT_H_
#define _DG_ASSEMBLER_EXPLICIT_H_

class dgConservationLaw;
class dgDofManager;

#include "dgDofContainer.h"

/** explicit
  * return dU 
  *   Alpha M dU - F(U0, t) - R = 0
  * F being provided by a conservationLaw */
class dgAssemblerExplicit
{
  dgDofManager *_dof;
  const dgConservationLaw *_law;
  dgDofContainer *_scatterField;
  bool _assembled, _diagonalMassFactor;
  public:
  void setScatterField(dgDofContainer *sf) {_scatterField = sf;}
  virtual void solve (dgDofContainer &U0, double alpha, dgDofContainer *R, double t, dgDofContainer &dU, bool recomputeMassMatrix = false, dgDofContainer *variable = NULL);
  dgAssemblerExplicit(dgDofManager *dof, const dgConservationLaw &law);
  void setDiagonalMassFactor(bool b) {_diagonalMassFactor = b;}
  inline const dgConservationLaw &getLaw() {if(!_law) Msg::Error("assembler does not have a law\n");return *_law;}
  dgAssemblerExplicit(){_dof =NULL; _law = NULL;}
  virtual ~dgAssemblerExplicit(){}
};

#endif
