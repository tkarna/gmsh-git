#ifndef _DG_DIRK_H_
#define _DG_DIRK_H_

class dgConservationLaw;
class dgDofManager;
class dgNewton;

#include "dgDofContainer.h"

/**Diagonal implicit runge-kutta temporal iterator (order 1 = implicit euler)*/
class dgDIRK
{
 protected:
  dgNewton *_newton;
  bool ownNewton;
  std::vector<dgDofContainer> _du;
  dgDofContainer _X;
  std::vector<double> _a, _b, _c;
  inline void setCoef(int order);
 public:
  dgDIRK(dgConservationLaw &claw, dgDofManager &dof, int order = 1);
  dgDIRK(dgConservationLaw &claw, dgDofManager &dof, std::vector<double> a, std::vector<double> b, std::vector<double> c);
  dgDIRK(dgNewton *newton, int order = 1);
  ~dgDIRK();
  int iterate(dgDofContainer &solution, double dt, double t);
  int subiterate(dgDofContainer &solution, double dt, double t, int step);
  inline dgNewton &getNewton() {return *_newton;}
  int getNsteps(){return _b.size();};
};

#endif
