#include <algorithm>
#include "dgMultirateERK.h"
#include <vector>
#include <string>
#include "dgConservationLaw.h"
#include "dgDofContainer.h"
#include "dgGroupOfElements.h"
#include "MElement.h"
#include "dgTimer.h"
#include <sstream>
#include <stdio.h>
#include <float.h>
#include "dgTerm.h"
#ifdef HAVE_MPI
#include "mpi.h"
#else
#include <string.h>
#endif

static void getSchlegelVectors(int maxTag, int n, std::vector<int> &startK, std::vector<int> &stopK, std::vector<int> &startU, std::vector<int> &stopU, bool first = true)
{
  if ( n == 0)
    return;
  int end = 3 * n - 2;
  int begin = std::max(3 * n - 4, 0);
  if (first){
    end = end -1;
    startK.push_back(0);
    stopK.push_back(end);
    startU.push_back(0);
    stopU.push_back(end);
  }

  getSchlegelVectors(maxTag, n - 1, startK, stopK, startU, stopU, false);

  startK.push_back(begin );
  stopK.push_back(end);
  startK.push_back(0);
  stopK.push_back(end);
  startU.push_back(std::max(begin - 1, 0));
  stopU.push_back(std::min(maxTag, end + 1));
  startU.push_back(0);
  stopU.push_back(std::min(maxTag, end + 1));

  getSchlegelVectors(maxTag, n - 1, startK,  stopK, startU, stopU, false);

  startK.push_back(begin);
  stopK.push_back(end);
  startU.push_back(std::max(0, begin - 1));
  stopU.push_back(std::min(maxTag, end + 1));
}

static void constructTables(int n,  const double *a,  const double *b,  const double *c, std::vector<double> &va,  std::vector<double> &vb, std::vector<double> &vc)
{
  va.resize(n * n);
  vb.resize(n);
  vc.resize(n);
  for (int i = 0; i < n; i ++)
  {
    vb[i] = b[i];
    vc[i] = c[i];
    for (int j = 0; j < n; ++j)
      va[i * n + j] = a[i * n + j];
  }
}

static void constructBufferTablesConstantinescu(int n,  const double *a, const double *b,  const double *c,  std::vector<double> &va, std::vector<double> &vb,  std::vector<double> &vc)
{
  va.resize(2*n * 2*n);
  vb.resize(2*n);
  vc.resize(2*n);
  for (int i = 0; i < n; i ++)
  {
    vb[i] = 0.5 * b[i];
    vb[n+i] = 0.5 * b[i];
    vc[i] = c[i];
    vc[n+i] = c[i];
    for (int j = 0; j < n; ++j){
      va[i * 2 * n + j] = a[i * n + j];
      va[(2 * n + 1) *n + i * 2 * n + j] = a[i * n + j];
    }
  }

}

void dgMultirateERK::setAllTag(int tag, dgDofContainer &x, double val){
  std::set<dgGroupOfElements *> &ve = _groupsOfElementsByTag[tag];
  for (std::set<dgGroupOfElements *>::iterator group = ve.begin(); group != ve.end(); group++){
    x.getGroupProxy(*group).setAll(val);
  }
}

void dgMultirateERK::axpyTag(int tag, dgDofContainer &x, const dgDofContainer &y, double val){
  std::set<dgGroupOfElements *> &ve = _groupsOfElementsByTag[tag];
  for (std::set<dgGroupOfElements *>::iterator group = ve.begin(); group != ve.end(); group++){
    x.getGroupProxy(*group).add(y.getGroupProxy(*group), val);
  }
}

void dgMultirateERK::resetTag(int tag, dgDofContainer &x){
  std::set<dgGroupOfElements *> &ve = _groupsOfElementsByTag[tag];
  for (std::set<dgGroupOfElements *>::iterator group = ve.begin(); group != ve.end(); group++){
    x.getGroupProxy(*group).setAll(0.);
  }
}

void dgMultirateERK::computeClassicalContrainedPartition(meshPartitionOptions &opts,  int algo){
  int ncon = 2;
  opts.setNumConstraints(ncon);
  opts.algorithm = algo;
  std::vector<int> loads(ncon);
  for (int iGroup = 0; iGroup < _gc->getNbElementGroups(); iGroup++){
    loads.clear();
    loads.resize(ncon);
    dgGroupOfElements *ge = _gc->getElementGroup(iGroup);
    loads[0] = 1;
    loads[1] = 1;
    for (size_t iElem = 0; iElem < ge->getNbElements(); iElem++){
      MElement *el = ge->getElement(iElem);
      int elNum = el->getNum();
      opts.vWeightMap[elNum] = loads;
      opts.eWeightMap[elNum] = 1;
    }
  }
}
void dgMultirateERK::computeSingleContrainedPartition(meshPartitionOptions &opts,  int algo){
  int ncon = 2;
  opts.setNumConstraints(ncon);
  opts.algorithm = algo;
  std::vector<int> loads(ncon);
  for (int iGroup = 0; iGroup < _gc->getNbElementGroups(); iGroup++){
    loads.clear();
    loads.resize(ncon);
    dgGroupOfElements *ge = _gc->getElementGroup(iGroup);
    int sCon;
    int mrTag = ge->getMultirateTag();
    if(_mrType > 0 && mrTag % 3 == 2){
      sCon = mrTag/3 + 1; 
    }
    else{
      sCon = mrTag/3;
    }
    loads[0]=1;
    loads[1] = (int) _nStages*pow(2, _maxExponent - sCon);
    for (size_t iElem = 0; iElem < ge->getNbElements(); iElem++){
      MElement *el = ge->getElement(iElem);
      int elNum = el->getNum();
      opts.vWeightMap[elNum] = loads;
      opts.eWeightMap[elNum] = (int) _nStages*pow(2, _maxExponent - sCon);
    }
  }
}

void dgMultirateERK::computeMultiConstrainedPartition(meshPartitionOptions &opts,  int algo, int nbconstraints){
  if(nbconstraints > 15){
    Msg::Fatal("The maximum number of constraints is 15");
  }
  int maxCon=_compuLoads.size()+1;
  int ncon=std::min(nbconstraints+1, maxCon);
  double pload = 0.0;
  std::vector< std::pair<double, int> > index_loads (_compuLoads.size());
  for(size_t ii = 0; ii < _compuLoads.size(); ii++){
    index_loads[ii]=std::make_pair(_compuLoads[ii], ii);
  }
  std::sort(index_loads.begin(), index_loads.end());
  std::reverse(index_loads.begin(), index_loads.end());
  for(int ii = 0; ii < ncon-1; ii++){
    pload += index_loads[ii].first;
  }
  Msg::Info("%d constraints for partitioning,  optimization for %f percent of the total workload", ncon, pload*100);
  opts.setNumConstraints(ncon);
  opts.algorithm = algo;
  std::vector<int> maxLoads(maxCon);
  std::vector<int> loads(ncon);
  for (int iGroup = 0; iGroup < _gc->getNbElementGroups(); iGroup++){
    maxLoads.clear();
    maxLoads.resize(maxCon);
    loads.clear();
    loads.resize(ncon);
    dgGroupOfElements *ge = _gc->getElementGroup(iGroup);
    int sCon;
    int mrTag = ge->getMultirateTag();
    if(_mrType > 0 && mrTag % 3 == 2){
      sCon = mrTag/3 + 1; 
    }
    else{
      sCon = mrTag/3;
    }
    maxLoads[maxCon-1]=1;
    loads[ncon-1]=1;
    for(int i = sCon; i < _maxExponent + 1; i ++){
      maxLoads[i] = 1;
    }
    if(_mrType > 0 && sCon > 0){
      maxLoads[_maxExponent + sCon] = 1;
    }
    for(int k = 0; k < (ncon-1); k++){
      //loads[ncon-2-k]=maxLoads[index_loads[k].second];
      loads[k]=maxLoads[index_loads[k].second];
    }
    for (size_t iElem = 0; iElem < ge->getNbElements(); iElem++){
      MElement *el = ge->getElement(iElem);
      int elNum = el->getNum();
      opts.vWeightMap[elNum] = loads;
      opts.eWeightMap[elNum] = (int) _nStages*pow(2, _maxExponent - sCon);
      //opts.eWeightMap[elNum] = 1;
    }
  }
}


void dgMultirateERK::computeInputForK(int level, const dgDofContainer &solution, double dt){
  int maxTag = _stopCritU[level] + 1;
  int minTag = _startCritU[level];
  for (int iTag = minTag; iTag < maxTag; iTag++){
    if(level > 0){
      _kLevel[iTag] += 1;
    }
    resetTag(iTag, *_currentInput);
    axpyTag(iTag, *_currentInput, solution, 1.);
    if (iTag % 3 == 0){
      double dtTag = dt/pow(2.0, (_maxExponent - iTag/3));
      _kLevel[iTag] = _kLevel[iTag] % _nStages;
      int iK = _kLevel[iTag];
      for (int i = 0; i < iK; i++){
        if (_aBulk[iK * _nStages + i] != 0.0){
          axpyTag(iTag, *_currentInput, *_K[i], _aBulk[iK * _nStages + i] * dtTag);
        }
      }
    }
    else if (iTag % 3 == 1 && _mrType > 0){
      double dtTag = dt/pow(2.0, (_maxExponent  - 1 - iTag/3));
      _kLevel[iTag] = _kLevel[iTag]  % _nStagesBuffer;
      int iK = _kLevel[iTag];
      for (int i =0; i < iK; i++){
        if (_aInnerBuffer[iK * _nStagesBuffer + i] != 0.0){
          axpyTag(iTag, *_currentInput,  *_K[i], _aInnerBuffer[iK * _nStagesBuffer + i] * dtTag);
          if(_mrType == 2){
            axpyTag(iTag, *_currentInput,  *_KInt[i], _aInnerBuffer[iK * _nStagesBuffer + i] * dtTag);
          }
        }
      }
    }
    else if (iTag % 3 == 2){
      double dtTag = dt/pow(2.0, (_maxExponent - 1 - iTag/3));
      _kLevel[iTag] = _kLevel[iTag] % _nStagesBuffer;
      int iK = _kLevel[iTag];
      for (int i =0; i < iK; i++){
        if (_aOuterBuffer[iK * _nStagesBuffer + i] != 0.0){
          axpyTag(iTag, *_currentInput,  *_K[i], _aOuterBuffer[iK * _nStagesBuffer + i] * dtTag);
          if(_mrType == 2){
            axpyTag(iTag, *_currentInput,  *_KInt[i], _aOuterBuffer[iK * _nStagesBuffer + i] * dtTag);
          }
        }
      }
    }
  }
}

void dgMultirateERK::updateSolution(int level, dgDofContainer &solution, double dt){
  int maxTag = _stopCritK[level] + 1;
  int minTag = _startCritK[level];
  if (_mrType > 0 && minTag % 3 == 2)
    minTag += -1;
  for (int iTag = minTag; iTag < maxTag; iTag++){
    if (iTag % 3 == 0 && _kLevel[iTag] == _nStages - 1){
      double dtTag = dt/pow(2.0, (_maxExponent - iTag/3));
      for (int iK = 0; iK < _nStages; iK++){
        if(_bBulk[iK] != 0.0){
          axpyTag(iTag, solution, *_K[iK], _bBulk[iK] * dtTag);
        }
      }
      _tLevel[iTag] += dtTag;
    }
    else if (iTag % 3 == 1 && _kLevel[iTag] == _nStagesBuffer - 1 && _mrType > 0){
      double dtTag = dt/pow(2.0, (_maxExponent  - 1 - iTag/3));
      for (int iK = 0; iK < _nStagesBuffer; iK++){
        if(_bInnerBuffer[iK] != 0.0){
          axpyTag(iTag, solution, *_K[iK], _bInnerBuffer[iK] * dtTag);
          if(_mrType == 2){
            axpyTag(iTag, solution, *_KInt[iK],_bInnerBuffer[iK] * dtTag);
            axpyTag(iTag+1, solution, *_KInt[iK],_bInnerBuffer[iK] * dtTag);
          }
        }
      }
      _tLevel[iTag] += dtTag;
    }
    else if (iTag % 3 == 2 && _kLevel[iTag] == _nStagesBuffer - 1){
      double dtTag = dt/pow(2.0, (_maxExponent - 1 - iTag/3));
      for (int iK = 0; iK < _nStagesBuffer; iK++){
        if(_bOuterBuffer[iK] != 0.0){
          axpyTag(iTag, solution, *_K[iK], _bOuterBuffer[iK] * dtTag);
        }
      }
      _tLevel[iTag] += dtTag;
    }
  }
}
void dgMultirateERK::computeK(int level, double dt){
  static dgTimer &timerCompute = dgTimer::root().child("compute");
  timerCompute.start();
  static dgTimer &timerInit = timerCompute.child("init");
  timerInit.start();
  int maxTag = _stopCritK[level] + 1;
  int minTag = _startCritK[level];
  _currentInput->scatterBeginMRTag(_stopCritU[level], _startCritU[level]);

  for (int iTag = minTag; iTag < maxTag; iTag++){
    resetTag(iTag, *_residual);
    if(_mrType == 2 && iTag % 3 != 0){
      resetTag(iTag, *_residualInt);
    }
  }
  if(_mrType == 2 && minTag % 3 == 2){
    resetTag(minTag-1, *_residualInt);
  }
  if(_mrType == 2 && maxTag % 3 == 2){
    resetTag(maxTag, *_residualInt);
  }

  timerInit.stop();
  static dgTimer &timerElem = timerCompute.child("elem");
  timerElem.start();
  static dgTimer &timerElem2 = timerElem.child("true elem");
  for (int iTag = minTag; iTag < maxTag; iTag++){
    std::set<dgGroupOfElements *> &ve = _groupsOfElementsByTag[iTag];
    double dtTag=0, c=0;
    if(iTag % 3 == 0){
      c = _cBulk[_kLevel[iTag]];
      dtTag = dt/pow(2.0,  (_maxExponent - iTag/3));
    }
    else if(iTag % 3 == 1 && _mrType > 0){
      c = _cInnerBuffer[_kLevel[iTag]];
      dtTag = dt/pow(2.0,  (_maxExponent - 1 - iTag/3));
    }
    else if(iTag % 3 == 2){
      c = _cOuterBuffer[_kLevel[iTag]];
      dtTag = dt/pow(2.0,  (_maxExponent - 1 - iTag/3));
    }
    for (std::set<dgGroupOfElements*>::iterator eGroup = ve.begin(); eGroup != ve.end(); eGroup++){
      timerElem2.start();
      _law.computeElement(**eGroup, _tLevel[iTag] + c*dtTag, *_currentInput, *_residual, NULL);
      timerElem2.stop();
      nb_elements += (*eGroup)->getNbElements(); 
    }
  }
  timerElem.stop();
  static dgTimer &timerFace = timerCompute.child("faces");
  static dgTimer &timerFace1 = timerFace.child("intra-processor faces");
  static dgTimer &timerFace2 = timerFace.child("inter-processor faces");
  timerFace.start();
  for (int iTag = minTag; iTag < maxTag; iTag++){
    double dtTag=0, c=0;
    if(iTag % 3 == 0){
      c = _cBulk[_kLevel[iTag]];
      dtTag = dt/pow(2.0,  (_maxExponent - iTag/3));
    }
    else if(iTag % 3 == 1 && _mrType > 0){
      c = _cInnerBuffer[_kLevel[iTag]];
      dtTag = dt/pow(2.0,  (_maxExponent - 1 - iTag/3));
    }
    else if(iTag % 3 == 2){
      c = _cOuterBuffer[_kLevel[iTag]];
      dtTag = dt/pow(2.0,  (_maxExponent - 1 - iTag/3));
    }
    std::set<dgGroupOfFaces*> &vf = _groupsOfFacesByTag[iTag];
    for (std::set<dgGroupOfFaces*>::iterator fGroup = vf.begin(); fGroup != vf.end(); fGroup++){
      dgGroupOfFaces *faces = *fGroup;
      bool alreadyComputed = false;
      bool oneGhost = false;
      bool schlegelInterface = false;
      for (int iConn = 0; iConn < faces->nConnection(); iConn++){
        const dgGroupOfElements &ge = faces->elementGroup(iConn);
        oneGhost |= (ge.getGhostPartition() >= 0);
        alreadyComputed |= (ge.getGhostPartition() < 0 && ge.getMultirateTag() < iTag && iTag != minTag);
        schlegelInterface |= (iTag % 3 == 1 && _mrType == 2 && ge.getMultirateTag() % 3 == 2 && iTag < ge.getMultirateTag());
        schlegelInterface |= (iTag % 3 == 2 && _mrType == 2 && ge.getMultirateTag() % 3 == 1 && iTag > ge.getMultirateTag());
      }
      if (alreadyComputed)
        continue;
      if (!oneGhost){
         if(schlegelInterface){
            timerFace1.start();
           _law.computeFace(*faces, _tLevel[iTag] + c*dtTag, *_currentInput, *_residualInt, NULL);
            timerFace1.stop();
         }
         else{
            timerFace1.start();
           _law.computeFace(*faces, _tLevel[iTag] + c*dtTag, *_currentInput, *_residual, NULL);
            timerFace1.stop();
         }
         nb_faces1 += faces->size();
      }
    }
  }
  static dgTimer &timerScatter = timerFace.child("scatter");
  timerScatter.start();
  _currentInput->scatterEndMRTag(_stopCritU[level], _startCritU[level]);
  timerScatter.stop();
  for (int iTag = minTag; iTag < maxTag; iTag++){
    double dtTag=0.0, c=0.0;
    if(iTag % 3 == 0){
      c = _cBulk[_kLevel[iTag]];
      dtTag = dt/pow(2.0,  (_maxExponent - iTag/3));
    }
    else if(iTag % 3 == 1 && _mrType > 0){
      c = _cInnerBuffer[_kLevel[iTag]];
      dtTag = dt/pow(2.0,  (_maxExponent - 1 - iTag/3));
    }
    else if(iTag % 3 == 2){
      c = _cOuterBuffer[_kLevel[iTag]];
      dtTag = dt/pow(2.0,  (_maxExponent - 1 - iTag/3));
    }
    std::set<dgGroupOfFaces*> &vf = _groupsOfFacesByTag[iTag];
    for (std::set<dgGroupOfFaces*>::iterator fGroup = vf.begin(); fGroup != vf.end(); fGroup++){
      dgGroupOfFaces *faces = *fGroup;
      bool oneGhost = false;
      bool schlegelInterface = false;
      for (int iConn = 0; iConn < faces->nConnection(); iConn++){
        const dgGroupOfElements &ge = faces->elementGroup(iConn);
        oneGhost |= (ge.getGhostPartition() >= 0);
        schlegelInterface |= (iTag % 3 == 1 && _mrType == 2 && ge.getMultirateTag() % 3 == 2 && iTag < ge.getMultirateTag());
        schlegelInterface |= (iTag % 3 == 2 && _mrType == 2 && ge.getMultirateTag() % 3 == 1 && iTag > ge.getMultirateTag());
      }
      if (oneGhost){
         if(schlegelInterface){
            timerFace2.start();
           _law.computeFace(*faces, _tLevel[iTag] + c*dtTag, *_currentInput, *_residualInt, NULL);
            timerFace2.stop();
         }
         else{
            timerFace2.start();
           _law.computeFace(*faces,  _tLevel[iTag] + c*dtTag,  *_currentInput,  *_residual, NULL);
            timerFace2.stop();
         }
         nb_faces2 += faces->size();
      }
    }
  }
  if(_mrType == 2 && minTag % 3 == 2){
    double dtTag=0.0, c=0.0;
    c = _cInnerBuffer[_kLevel[minTag-1]];
    dtTag = dt/pow(2.0,  (_maxExponent - 1 - (minTag-1)/3));
    std::set<dgGroupOfFaces*> &vf = _groupsOfFacesByTag[minTag-1];
    for (std::set<dgGroupOfFaces*>::iterator fGroup = vf.begin(); fGroup != vf.end(); fGroup++){
      dgGroupOfFaces *faces = *fGroup;
      bool oneGhost = false;
      bool schlegelInterface = false;
      for (int iConn = 0; iConn < faces->nConnection(); iConn++){
        const dgGroupOfElements &ge = faces->elementGroup(iConn);
        oneGhost |= (ge.getGhostPartition() >= 0);
        schlegelInterface |= (ge.getMultirateTag() % 3 == 1 && minTag ==  (ge.getMultirateTag()+1));
      }
      if (oneGhost){
         if(schlegelInterface){
            timerFace2.start();
           _law.computeFace(*faces, _tLevel[minTag-1] + c*dtTag, *_currentInput, *_residualInt, NULL);
            timerFace2.stop();
           nb_faces2 += faces->size();
         }
      }
    }
  }
  if(_mrType == 2 && maxTag % 3 == 2){
    double dtTag=0.0, c=0.0;
    c = _cInnerBuffer[_kLevel[maxTag]];
    dtTag = dt/pow(2.0,  (_maxExponent - 1 - maxTag/3));
    std::set<dgGroupOfFaces*> &vf = _groupsOfFacesByTag[maxTag];
    for (std::set<dgGroupOfFaces*>::iterator fGroup = vf.begin(); fGroup != vf.end(); fGroup++){
      dgGroupOfFaces *faces = *fGroup;
      bool oneGhost = false;
      bool schlegelInterface = false;
      for (int iConn = 0; iConn < faces->nConnection(); iConn++){
        const dgGroupOfElements &ge = faces->elementGroup(iConn);
        oneGhost |= (ge.getGhostPartition() >= 0);
        schlegelInterface |= (ge.getMultirateTag() % 3 == 1 && maxTag == (ge.getMultirateTag()+1));
      }
      if (oneGhost){
         if(schlegelInterface){
            timerFace2.start();
           _law.computeFace(*faces, _tLevel[maxTag] + c*dtTag, *_currentInput, *_residualInt, NULL);
            timerFace2.stop();
           nb_faces2 += faces->size();
         }
      }
    }
  }
  timerFace.stop();
  static dgTimer &timerEnd = timerCompute.child("end");
  timerEnd.start();
  fullMatrix<double> iMassEl,  KEl, KElInt,   residuEl, residuElInt;
  for (int iTag = minTag; iTag < maxTag; iTag++){
    std::set<dgGroupOfElements *> &ve = _groupsOfElementsByTag[iTag];
    for (std::set<dgGroupOfElements*>::iterator eGroup = ve.begin(); eGroup != ve.end(); eGroup++){
      for (size_t iElem = 0; iElem < (*eGroup)->getNbElements(); iElem++){
        _residual->getGroupProxy(*eGroup).getBlockProxy(iElem, residuEl);
        _K[_kLevel[iTag]]->getGroupProxy(*eGroup).getBlockProxy(iElem, KEl);
        (*eGroup)->getInverseMassMatrix().getBlockProxy(iElem,  iMassEl);
        iMassEl.mult(residuEl, KEl);
        if(iTag % 3 > 0 && _mrType == 2){
          _residualInt->getGroupProxy(*eGroup).getBlockProxy(iElem, residuElInt);
          _KInt[_kLevel[iTag]]->getGroupProxy(*eGroup).getBlockProxy(iElem, KElInt);
          iMassEl.mult(residuElInt, KElInt);
        }
      }
    }
  }
  if( (maxTag-1) % 3 == 1 && _mrType == 2){
    std::set<dgGroupOfElements *> &ve = _groupsOfElementsByTag[maxTag];
    for (std::set<dgGroupOfElements*>::iterator eGroup = ve.begin(); eGroup != ve.end(); eGroup++){
      for (size_t iElem = 0; iElem < (*eGroup)->getNbElements(); iElem++){
        _residualInt->getGroupProxy(*eGroup).getBlockProxy(iElem, residuElInt);
        _KInt[_kLevel[maxTag]]->getGroupProxy(*eGroup).getBlockProxy(iElem, KElInt);
        (*eGroup)->getInverseMassMatrix().getBlockProxy(iElem,  iMassEl);
        iMassEl.mult(residuElInt, KElInt);
      }
    }
  }
  timerEnd.stop();
  timerCompute.stop();
}

void dgMultirateERK::iterate(dgDofContainer &solution,  double dt,  double t){
  initialize();
  nb_elements = 0;
  nb_faces1 = 0;
  nb_faces2 = 0;
  static dgTimer &timerUpdate = dgTimer::root().child("update");
  static dgTimer &timerInput = dgTimer::root().child("input");
  for (int iTag = 0; iTag < (_maxExponent * 3 + 1); iTag++){
    _kLevel[iTag] = 0;
    _tLevel[iTag] = t;
  }
  for (int iLevel = 0; iLevel < _nbLevels; iLevel++){
    timerInput.start();
    computeInputForK(iLevel, solution, dt);
    timerInput.stop();
    computeK(iLevel, dt);
    timerUpdate.start();
    updateSolution(iLevel, solution, dt);
    timerUpdate.stop();
  }
}

void dgMultirateERK::initialize(){
  if (!_init){
    _K = new dgDofContainer*[_nStagesBuffer];
    if(_mrType==2){
      _KInt = new dgDofContainer*[_nStagesBuffer];
    }
    for (int iK = 0; iK < _nStagesBuffer; iK++){
      _K[iK] = new dgDofContainer(*_gc, _law.getNbFields());
      _K[iK]->setAll(0.0);
      if(_mrType==2){
        _KInt[iK] = new dgDofContainer(*_gc, _law.getNbFields());
        _KInt[iK]->setAll(0.0);
      }
    }
    _currentInput = new dgDofContainer(*_gc, _law.getNbFields());
    _currentInput->setAll(0.0);
    _residual = new dgDofContainer(*_gc, _law.getNbFields());
    _residual->setAll(0.0);
    if(_mrType==2){
      _residualInt = new dgDofContainer(*_gc, _law.getNbFields());
      _residualInt->setAll(0.0);
    }
    _groupsOfElementsByTag.resize(_maxExponent * 3 + 1);
    for (int iGroupE = 0; iGroupE < _gc->getNbElementGroups(); iGroupE++){
      dgGroupOfElements *group = _gc->getElementGroup(iGroupE);
      _groupsOfElementsByTag[group->getMultirateTag()].insert(group);
    }
    _groupsOfFacesByTag.resize(_maxExponent * 3 + 1);
    for (int iGroupF = 0; iGroupF < _gc->getNbFaceGroups(); iGroupF++){
      dgGroupOfFaces *faces = _gc->getFaceGroup(iGroupF);
      for (int iConn = 0; iConn < faces->nConnection(); iConn++){
        const dgGroupOfElements *group = &faces->elementGroup(iConn);
        if (group->getGhostPartition() == -1)
          _groupsOfFacesByTag[group->getMultirateTag()].insert(faces);
      }
    }
    _kLevel.resize(_maxExponent * 3 + 1);
    _tLevel.resize(_maxExponent * 3 + 1);
    if (_mrType == 0){
      _nbLevels = _nStages * (int) pow(2,  _maxExponent);
      _stopCritU.resize(_nbLevels);
      _startCritU.resize(_nbLevels);
      _stopCritK.resize(_nbLevels);
      _startCritK.resize(_nbLevels);
      for (int i = 0; i < _nbLevels; i++){
        _startCritU[i] = 0;
        _startCritK[i] = 0;
      }
      for (int i = 0; i < _nStages; i++){
        _stopCritU[(_nbLevels -1 + i) % _nbLevels] = 3 * _maxExponent;
        _stopCritK[(_nbLevels -1 + i) % _nbLevels] = 3 * _maxExponent;
      }
      int startInd = _nStages -1;
      for (int i = 0; i < _maxExponent; i++){
        int crit = 3 * (i + 1);
        if(i > 0)
          startInd +=  _nStages * (int) pow(2, i - 1);
        for(int j = 0; j < (int) pow(2, _maxExponent - 1 - i); j++){
          int ind = startInd;
          if(j > 0)
            ind =  startInd + (_nStages * (int) pow(2, i + 1)) * j;
          for (int k = 0; k < _nStages; k++){
            _stopCritU[ind + k] = crit;
            _stopCritK[ind + k] = crit - 1;
          }
        }
      }
    }
    else if (_mrType > 0){
      _nbLevels = _nStages;
      for (int i = 0; i < _maxExponent; i++)
        _nbLevels = 2 * _nbLevels + (_nStagesBuffer-2*_nStages);

      _stopCritU.reserve(_nbLevels);
      _startCritU.reserve(_nbLevels);
      _stopCritK.reserve(_nbLevels);
      _startCritK.reserve(_nbLevels);

      getSchlegelVectors(_maxExponent*3, _maxExponent + 1, _startCritK, _stopCritK, _startCritU, _stopCritU);
    }
    _init = true;
  }
  else
    return;
}

dgMultirateERK::dgMultirateERK(dgConservationLaw &claw,  dgMultirateERK *copy):
 _law(claw), _gc(copy->_gc), _rk_type(copy->_rk_type)
{
  _init = false;
  setButcherTables(_rk_type);
  _dtMin = copy->_dtMin;
  _dtMax = copy->_dtMax;
  _dtRef = copy->_dtRef;
  _speedUp = copy->_speedUp;
  _maxExponent = copy->_maxExponent;

}

dgMultirateERK::dgMultirateERK(dgGroupCollection *gc, dgConservationLaw &claw, ERK_TYPE rk_type) : 
 _law(claw), _gc(gc), _rk_type(rk_type)
{
  _init = false;
  setButcherTables(_rk_type);
}

dgMultirateERK::~dgMultirateERK(){ 
  if(_init){
    for (int i = 0; i < _nStagesBuffer; i++){
      delete _K[i];
      if(_mrType==2){
        delete _KInt[i];
      }
    }
    delete []_K;
    delete _residual;
    delete _currentInput;
    if(_mrType==2){
      delete []_KInt;
      delete _residualInt;
    }
  }
}

void dgMultirateERK::setButcherTables(ERK_TYPE rk_type) {
  switch (rk_type) {
    case ERK_22_C :
      {
        _mrType = 0;
        _nStages = 2;
        _nStagesBuffer = 4;
        _innerBufferSize = 0;
        _outerBufferSize = _nStages;
        _maxExponent=0;
        const double a[] = {0., 0., 1., 0.};
        const double b[] = {0.5, 0.5};
        const double c[] = {0., 1.};
        constructTables(_nStages, a, b, c, _aBulk, _bBulk, _cBulk);
        constructBufferTablesConstantinescu(_nStages, a, b, c, _aOuterBuffer, _bOuterBuffer, _cOuterBuffer);

      }
      break;
    case ERK_33_C :
      {
        _mrType = 0;
        _nStages = 3;
        _nStagesBuffer = 6;
        _innerBufferSize = 0;
        _outerBufferSize = _nStages;
        _maxExponent=0;
        const double a[] = 
        {0., 0., 0., 
         .5, 0., 0., 
         -1., 2., 0.};
        const double b[] = {1./6, 2./3, 1./6};
        const double c[] = {0., .5, 1.};
        constructTables(_nStages, a, b, c, _aBulk, _bBulk, _cBulk);
        constructBufferTablesConstantinescu(_nStages, a, b, c, _aOuterBuffer, _bOuterBuffer, _cOuterBuffer);

      }
      break;
    case ERK_43_C :
      {
        _mrType = 0;
        _nStages = 4;
        _nStagesBuffer = 8;
        _innerBufferSize = 0;
        _outerBufferSize = _nStages;
        _maxExponent=0;
        const double a[] = 
        {0., 0., 0., 0., 
         .5, 0., 0., 0., 
         -1./6, 2./3, 0., 0., 
         1./3, -1./3., 1., 0.};
        const double b[] = {1./6, 1./3, 1./3, 1./6};
        const double c[] = {0., .5, .5, 1.};
        constructTables(_nStages, a, b, c, _aBulk, _bBulk, _cBulk);
        constructBufferTablesConstantinescu(_nStages, a, b, c, _aOuterBuffer, _bOuterBuffer, _cOuterBuffer);

      }
      break;
    case ERK_44_C :
      {
        _mrType = 0;
        _nStages = 4;
        _nStagesBuffer = 8;
        _innerBufferSize = 0;
        _outerBufferSize = _nStages;
        _maxExponent=0;
        const double a[] = 
        {0., 0., 0., 0., 
         .5, 0., 0., 0., 
         0., .5, 0., 0., 
         0., 0., 1., 0.};
        const double b[] = {1./6, 1./3, 1./3, 1./6};
        const double c[] = {0., .5, .5, 1.};
        constructTables(_nStages, a, b, c, _aBulk, _bBulk, _cBulk);
        constructBufferTablesConstantinescu(_nStages, a, b, c, _aOuterBuffer, _bOuterBuffer, _cOuterBuffer);

      }
      break;
    case ERK_22_S :
      {
        _mrType = 1;
        _nStages = 2;
        _nStagesBuffer = 5;
        _innerBufferSize = 1;
        _outerBufferSize = 1;
        _maxExponent=0;
        const double a[] = {0., 0., 1., 0.};
        const double b[] = {0.5, 0.5};
        const double c[] = {0., 1.};
        constructTables(_nStages, a, b, c, _aBulk, _bBulk, _cBulk);
        const double aInner[]=
        { 0.0    , 0.0    , 0.0    , 0.0    , 0.0    , 
          1.0/2.0, 0.0    , 0.0    , 0.0    , 0.0    ,
          1.0/4.0, 1.0/4.0, 0.0    , 0.0    , 0.0    , 
          1.0/4.0, 1.0/4.0, 1.0/2.0, 0.0    , 0.0    , 
          1.0/4.0, 1.0/4.0, 1.0/4.0, 1.0/4.0, 0.0     };
        const double bInner[]={1.0/4.0, 1.0/4.0, 1.0/4.0, 1.0/4.0, 0.0};
        const double cInner[]={0, 1.0/2.0, 1.0/2.0, 1.0, 1.0};
        constructTables(_nStagesBuffer, aInner, bInner, cInner, _aInnerBuffer, _bInnerBuffer, _cInnerBuffer);
        const double aOuter[]=
        { 0.0    , 0.0    , 0.0    , 0.0    , 0.0    , 
          1.0/2.0, 0.0    , 0.0    , 0.0    , 0.0    ,
          1.0/2.0, 0.0    , 0.0    , 0.0    , 0.0    , 
          1.0    , 0.0    , 0.0    , 0.0    , 0.0    , 
          1.0    , 0.0    , 0.0    , 0.0    , 0.0     };
        const double bOuter[]={1.0/2.0, 0.0, 0.0, 0.0, 1.0/2.0};
        const double cOuter[]={0, 1.0/2.0, 1.0/2.0, 1.0, 1.0};
        constructTables(_nStagesBuffer, aOuter, bOuter, cOuter, _aOuterBuffer, _bOuterBuffer, _cOuterBuffer);
      }
      break;
    case ERK_22_S_C :
      {
        _mrType = 2;
        _nStages = 2;
        _nStagesBuffer = 5;
        _innerBufferSize = 1;
        _outerBufferSize = 1;
        _maxExponent=0;
        const double a[] = {0., 0., 1., 0.};
        const double b[] = {0.5, 0.5};
        const double c[] = {0., 1.};
        constructTables(_nStages, a, b, c, _aBulk, _bBulk, _cBulk);
        const double aInner[]=
        { 0.0    , 0.0    , 0.0    , 0.0    , 0.0    , 
          1.0/2.0, 0.0    , 0.0    , 0.0    , 0.0    ,
          1.0/4.0, 1.0/4.0, 0.0    , 0.0    , 0.0    , 
          1.0/4.0, 1.0/4.0, 1.0/2.0, 0.0    , 0.0    , 
          1.0/4.0, 1.0/4.0, 1.0/4.0, 1.0/4.0, 0.0     };
        const double bInner[]={1.0/4.0, 1.0/4.0, 1.0/4.0, 1.0/4.0, 0.0};
        const double cInner[]={0, 1.0/2.0, 1.0/2.0, 1.0, 1.0};
        constructTables(_nStagesBuffer, aInner, bInner, cInner, _aInnerBuffer, _bInnerBuffer, _cInnerBuffer);
        const double aOuter[]=
        { 0.0    , 0.0    , 0.0    , 0.0    , 0.0    , 
          1.0/2.0, 0.0    , 0.0    , 0.0    , 0.0    ,
          1.0/2.0, 0.0    , 0.0    , 0.0    , 0.0    , 
          1.0    , 0.0    , 0.0    , 0.0    , 0.0    , 
          1.0    , 0.0    , 0.0    , 0.0    , 0.0     };
        const double bOuter[]={1.0/2.0, 0.0, 0.0, 0.0, 1.0/2.0};
        const double cOuter[]={0, 1.0/2.0, 1.0/2.0, 1.0, 1.0};
        constructTables(_nStagesBuffer, aOuter, bOuter, cOuter, _aOuterBuffer, _bOuterBuffer, _cOuterBuffer);
      }
      break;
    case ERK_43_S :
      {
        _mrType = 1;
        _nStages = 4;
        _nStagesBuffer = 10;
        _innerBufferSize = 1;
        _outerBufferSize = 1;
        _maxExponent=0;
        const double a[] = 
        {0., 0., 0., 0., 
         .5, 0., 0., 0., 
         -1./6, 2./3, 0., 0., 
         1./3, -1./3., 1., 0.};
        const double b[] = {1./6, 1./3, 1./3, 1./6};
        const double c[] = {0., .5, .5, 1.};
        constructTables(_nStages, a, b, c, _aBulk, _bBulk, _cBulk);
        const double aInner[]=
        { 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         ,
          1.0/4.0   , 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         ,
         -1.0/12.0  , 1.0/3.0   , 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         ,
          1.0/6.0   ,-1.0/6.0   , 1.0/2.0   , 0         , 0         , 0         , 0         , 0         , 0         , 0         ,
          1.0/12.0  , 1.0/6.0   , 1.0/6.0   , 1.0/12.0  , 0         , 0         , 0         , 0         , 0         , 0         ,
          1.0/12.0  , 1.0/6.0   , 1.0/6.0   , 1.0/12.0  , 0         , 0         , 0         , 0         , 0         , 0         ,
          1.0/12.0  , 1.0/6.0   , 1.0/6.0   , 1.0/12.0  , 0         , 1.0/4.0   , 0         , 0         , 0         , 0         ,
          1.0/12.0  , 1.0/6.0   , 1.0/6.0   , 1.0/12.0  , 0         ,-1.0/12.0  , 1.0/3.0   , 0         , 0         , 0         ,
          1.0/12.0  , 1.0/6.0   , 1.0/6.0   , 1.0/12.0  , 0         , 1.0/6.0   ,-1.0/6.0   , 1.0/2.0   , 0         , 0         ,
          1.0/12.0  , 1.0/6.0   , 1.0/6.0   , 1.0/12.0  , 0         , 1.0/12.0  , 1.0/6.0   , 1.0/6.0   , 1.0/12.0  , 0          };
        const double bInner[]={1.0/12.0, 1.0/6.0, 1.0/6.0,1.0/12.0,0,1.0/12.0, 1.0/6.0, 1.0/6.0,1.0/12.0,0};
        const double cInner[]={0, 1.0/4.0, 1.0/4.0, 1.0/2.0, 1.0/2.0, 1.0/2.0, 3.0/4.0, 3.0/4.0, 1, 1 };
        constructTables(_nStagesBuffer, aInner, bInner, cInner, _aInnerBuffer, _bInnerBuffer, _cInnerBuffer);
        const double aOuter[]=
        { 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         ,
          1.0/4.0   , 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         ,
          1.0/4.0   , 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         ,
          1.0/2.0   , 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         ,
          1.0/2.0   , 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         ,
         -1.0/6.0   , 0         , 0         , 0         , 2.0/3.0   , 0         , 0         , 0         , 0         , 0         ,
          1.0/12.0  , 0         , 0         , 0         , 1.0/6.0   , 1.0/2.0   , 0         , 0         , 0         , 0         ,
          1.0/12.0  , 0         , 0         , 0         , 1.0/6.0   , 1.0/2.0   , 0         , 0         , 0         , 0         ,
          1.0/3.0   , 0         , 0         , 0         ,-1.0/3.0   , 1         , 0         , 0         , 0         , 0         ,
          1.0/3.0   , 0         , 0         , 0         ,-1.0/3.0   , 1         , 0         , 0         , 0         , 0          };
        const double bOuter[]={1.0/6.0, 0 , 0 , 0 , 1.0/3.0,1.0/3.0, 0 , 0 , 0 , 1.0/6.0};
        const double cOuter[]={0, 1.0/4.0, 1.0/4.0, 1.0/2.0, 1.0/2.0, 1.0/2.0, 3.0/4.0, 3.0/4.0, 1, 1 };
        constructTables(_nStagesBuffer, aOuter, bOuter, cOuter, _aOuterBuffer, _bOuterBuffer, _cOuterBuffer);
      }
      break;
    case ERK_43_S_C :
      {
        _mrType = 2;
        _nStages = 4;
        _nStagesBuffer = 10;
        _innerBufferSize = 1;
        _outerBufferSize = 1;
        _maxExponent=0;
        const double a[] = 
        {0., 0., 0., 0., 
         .5, 0., 0., 0., 
         -1./6, 2./3, 0., 0., 
         1./3, -1./3., 1., 0.};
        const double b[] = {1./6, 1./3, 1./3, 1./6};
        const double c[] = {0., .5, .5, 1.};
        constructTables(_nStages, a, b, c, _aBulk, _bBulk, _cBulk);
        const double aInner[]=
        { 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         ,
          1.0/4.0   , 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         ,
         -1.0/12.0  , 1.0/3.0   , 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         ,
          1.0/6.0   ,-1.0/6.0   , 1.0/2.0   , 0         , 0         , 0         , 0         , 0         , 0         , 0         ,
          1.0/12.0  , 1.0/6.0   , 1.0/6.0   , 1.0/12.0  , 0         , 0         , 0         , 0         , 0         , 0         ,
          1.0/12.0  , 1.0/6.0   , 1.0/6.0   , 1.0/12.0  , 0         , 0         , 0         , 0         , 0         , 0         ,
          1.0/12.0  , 1.0/6.0   , 1.0/6.0   , 1.0/12.0  , 0         , 1.0/4.0   , 0         , 0         , 0         , 0         ,
          1.0/12.0  , 1.0/6.0   , 1.0/6.0   , 1.0/12.0  , 0         ,-1.0/12.0  , 1.0/3.0   , 0         , 0         , 0         ,
          1.0/12.0  , 1.0/6.0   , 1.0/6.0   , 1.0/12.0  , 0         , 1.0/6.0   ,-1.0/6.0   , 1.0/2.0   , 0         , 0         ,
          1.0/12.0  , 1.0/6.0   , 1.0/6.0   , 1.0/12.0  , 0         , 1.0/12.0  , 1.0/6.0   , 1.0/6.0   , 1.0/12.0  , 0          };
        const double bInner[]={1.0/12.0, 1.0/6.0, 1.0/6.0,1.0/12.0,0,1.0/12.0, 1.0/6.0, 1.0/6.0,1.0/12.0,0};
        const double cInner[]={0, 1.0/4.0, 1.0/4.0, 1.0/2.0, 1.0/2.0, 1.0/2.0, 3.0/4.0, 3.0/4.0, 1, 1 };
        constructTables(_nStagesBuffer, aInner, bInner, cInner, _aInnerBuffer, _bInnerBuffer, _cInnerBuffer);
        const double aOuter[]=
        { 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         ,
          1.0/4.0   , 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         ,
          1.0/4.0   , 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         ,
          1.0/2.0   , 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         ,
          1.0/2.0   , 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         , 0         ,
         -1.0/6.0   , 0         , 0         , 0         , 2.0/3.0   , 0         , 0         , 0         , 0         , 0         ,
          1.0/12.0  , 0         , 0         , 0         , 1.0/6.0   , 1.0/2.0   , 0         , 0         , 0         , 0         ,
          1.0/12.0  , 0         , 0         , 0         , 1.0/6.0   , 1.0/2.0   , 0         , 0         , 0         , 0         ,
          1.0/3.0   , 0         , 0         , 0         ,-1.0/3.0   , 1         , 0         , 0         , 0         , 0         ,
          1.0/3.0   , 0         , 0         , 0         ,-1.0/3.0   , 1         , 0         , 0         , 0         , 0          };
        const double bOuter[]={1.0/6.0, 0 , 0 , 0 , 1.0/3.0,1.0/3.0, 0 , 0 , 0 , 1.0/6.0};
        const double cOuter[]={0, 1.0/4.0, 1.0/4.0, 1.0/2.0, 1.0/2.0, 1.0/2.0, 3.0/4.0, 3.0/4.0, 1, 1 };
        constructTables(_nStagesBuffer, aOuter, bOuter, cOuter, _aOuterBuffer, _bOuterBuffer, _cOuterBuffer);
      }
      break;
    default :
      Msg::Error("Unknown multirate Runge-Kutta type: %i \n", rk_type);

  }
}

double dgMultirateERK:: splitGroupsForMultirate(int maxLevels, dgDofContainer *solution, std::vector<dgDofContainer*> dofs, double fact){
  if(fact > 1.0)
    Msg::Fatal("Factor for tme step is too high and the scheme may be unstable. Choose a factor smaller or equal to 1.0");

  dgDofContainer tag(*_gc, 1);
  dgDofContainer isolated(*_gc, 1);
  std::vector<std::vector<double> > localDt;
  double dtMin = DBL_MAX;
  double dtMax = 0;
  int maxExponent;
  localDt.resize(_gc->getNbElementGroups());
// Compute the stable time steps of each element in the mesh,  the min and the max time steps and finally the reference time step for multirate

  for (int i = 0; i < _gc->getNbElementGroups(); i++){
    _law.computeElementaryTimeSteps(*_gc->getElementGroup(i), solution, localDt[i]);
    for (size_t j = 0; j < localDt[i].size(); j++){
      dtMin = std::min(dtMin, localDt[i][j]);
      dtMax = std::max(dtMax, localDt[i][j]);
    }
  }
  
#ifdef HAVE_MPI
  if (!_gc->getIsDefinedOnOneProcessor()){
    double dtMinG, dtMaxG;
    MPI_Allreduce((void *)&dtMin,  &dtMinG,  1,  MPI_DOUBLE,  MPI_MIN,  MPI_COMM_WORLD);
    dtMin = dtMinG;
    MPI_Allreduce((void *)&dtMax,  &dtMaxG,  1,  MPI_DOUBLE,  MPI_MAX,  MPI_COMM_WORLD);
    dtMax = dtMaxG;
  }
#endif

  _dtMin = dtMin;
  _dtMax = dtMax;

  dtMin = dtMin * fact;
  while(dtMin <= 0.5*_dtMin)
    dtMin=2*dtMin;
  maxExponent = floor(log(dtMax/dtMin)/log(2.));
  double dtRef = dtMin * pow(2, maxExponent);
  if(maxExponent > maxLevels - 1){
    dtRef *= 1.0/pow(2.0, maxExponent - maxLevels + 1);
    maxExponent = maxLevels -1;
  }

  fullMatrix<double> elementProxy, elementProxyI, elementProxyJ, elementProxyK, elementProxyL, newElementProxy;
  for (int iGroup = 0; iGroup < _gc->getNbElementGroups(); iGroup++){
    for (size_t iElem = 0; iElem < _gc->getElementGroup(iGroup)->getNbElements(); iElem++){
      tag.getGroupProxy(iGroup).getBlockProxy(iElem, elementProxy);
      elementProxy(0, 0) = std::min(maxExponent*1.,  maxExponent + floor(log(localDt[iGroup][iElem]/dtRef)/log(2.0))) * (_outerBufferSize+1);
      for (int i = 0; i < elementProxy.size1(); ++i)
        elementProxy(i, 0) = elementProxy(0, 0);
    }
  }
  if (!_gc->getIsDefinedOnOneProcessor()){
    tag.scatter(true);
  }

  if (_outerBufferSize > 0){
    for (int iLoop = 0; iLoop < (_outerBufferSize + 1) * maxExponent; iLoop++){
      for (int iFaceGroup = 0; iFaceGroup < _gc->getNbFaceGroups(); iFaceGroup++){
        dgGroupOfFaces *faces = _gc->getFaceGroup(iFaceGroup);
        for (size_t iFace = 0; iFace < faces->size(); iFace++){
          for (int iConn = 0; iConn < faces->nConnection(); iConn++){
            tag.getGroupProxy(&faces->elementGroup(iConn)).getBlockProxy(faces->elementId(iFace, iConn), elementProxyI);
            double &tagI = elementProxyI(0, 0);
            for (int jConn = 0; jConn < iConn; jConn++){
              tag.getGroupProxy(&faces->elementGroup(jConn)).getBlockProxy(faces->elementId(iFace, jConn), elementProxyJ);
              double &tagJ = elementProxyJ(0, 0);
              tagI = std::min(tagI, tagJ+1);
              tagJ = std::min(tagJ, tagI+1);
            }
          }
        }
      }
      if (!_gc->getIsDefinedOnOneProcessor()){
        tag.scatter(true);
      }
    }
  }
  if (_outerBufferSize > 1){
    for (int iLoop = 0; iLoop < (_outerBufferSize + 1) * maxExponent; iLoop++){
      isolated.setAll(0.0);
      for (int iFaceGroup = 0; iFaceGroup < _gc->getNbFaceGroups(); iFaceGroup++){
        dgGroupOfFaces *faces = _gc->getFaceGroup(iFaceGroup);
        for (size_t iFace = 0; iFace < faces->size(); iFace++){
          for (int iConn = 0; iConn < faces->nConnection(); iConn++){
            isolated.getGroupProxy(&faces->elementGroup(iConn)).getBlockProxy(faces->elementId(iFace, iConn), elementProxyK);
            tag.getGroupProxy(&faces->elementGroup(iConn)).getBlockProxy(faces->elementId(iFace, iConn), elementProxyI);
            double &tagI = elementProxyI(0, 0);
            double &isoK = elementProxyK(0, 0);
            for (int jConn = 0; jConn < iConn; jConn++){
              isolated.getGroupProxy(&faces->elementGroup(jConn)).getBlockProxy(faces->elementId(iFace, jConn), elementProxyL);
              tag.getGroupProxy(&faces->elementGroup(jConn)).getBlockProxy(faces->elementId(iFace, jConn), elementProxyJ);
              double &tagJ = elementProxyJ(0, 0);
              double &isoL = elementProxyL(0, 0);
              if((int)tagI % (_outerBufferSize+1) != 0)
                isoK +=  (double) (tagJ > tagI) ;
              
              if((int)tagJ % (_outerBufferSize+1) != 0)
                isoL += (double) (tagI > tagJ) ;
            }
          }
        }
      }
      for (int iGroup = 0; iGroup < _gc->getNbElementGroups(); iGroup++){
        dgGroupOfElements *group = _gc->getElementGroup(iGroup);
        for (size_t iElem = 0; iElem < group->getNbElements(); iElem++){
          isolated.getGroupProxy(group).getBlockProxy(iElem, elementProxyK);
          if(elementProxyK(0, 0) < 1.0){
            tag.getGroupProxy(group).getBlockProxy(iElem,  elementProxyJ);
            double &nTag = elementProxyJ(0, 0); 
            if((int)nTag % (_outerBufferSize+1) != 0){
              nTag += -1;
            }
          }
        }
      }
      if (!_gc->getIsDefinedOnOneProcessor()){
        tag.scatter(true);
      }
    }
  }

  if (_innerBufferSize > 0){
    for (int iFaceGroup = 0; iFaceGroup < _gc->getNbFaceGroups(); iFaceGroup++){
      dgGroupOfFaces *faces = _gc->getFaceGroup(iFaceGroup);
      for (size_t iFace = 0; iFace < faces->size(); iFace++){
        for (int iConn = 0; iConn < faces->nConnection(); iConn++){
          tag.getGroupProxy(&faces->elementGroup(iConn)).getBlockProxy(faces->elementId(iFace, iConn), elementProxyI);
          double &tagI = elementProxyI(0, 0);
          for (int jConn = 0; jConn < iConn; jConn++){
            tag.getGroupProxy(&faces->elementGroup(jConn)).getBlockProxy(faces->elementId(iFace, jConn), elementProxyJ);
            double &tagJ = elementProxyJ(0, 0);
            if (fabs(fmod(tagI, _outerBufferSize+1))<1e-4 && tagJ>tagI+0.9999 ){
              tagI+=0.5;
            }
            if (fabs(fmod(tagJ, _outerBufferSize+1))<1e-4 && tagI>tagJ+0.9999 ){
              tagJ+=0.5;
            }
          }
        }
      }
    }
  }

  if (!_gc->getIsDefinedOnOneProcessor()){
    tag.scatter(false);
  }

  std::map<std::pair<dgGroupOfElements*,  int>, std::vector<MElement*> > newGroupsMap;
  std::map<int, std::pair<int, int> > dataMap;
  int nbGroupElements = 0;
  for (int iGroup = 0; iGroup < _gc->getNbElementGroups() + _gc->getNbGhostGroups(); iGroup++){
    dgGroupOfElements *group = _gc->getElementGroup(iGroup);
    int nbNodes = group->getNbNodes();
    for (size_t iElem = 0; iElem < group->getNbElements(); iElem++){
      tag.getGroupProxy(group).getBlockProxy(iElem, newElementProxy);
      int newTag;
      int iTag = (int) round(2 * newElementProxy(0, 0));
      int level = iTag/(2*(_outerBufferSize+1));
      int buf = iTag%(2*(_outerBufferSize+1));
      if (buf == 0){
        newTag = 3 * level;
      }
      else if (buf == 1){
        newTag = 3 * level +1;
      }
      else{
        newTag = 3 * level +2;
      }
      newGroupsMap[std::pair<dgGroupOfElements*, int>(group, newTag)].push_back(group->getElement(iElem));
      if (group->getGhostPartition() == -1){
        dataMap[group->getElement(iElem)->getNum()].first = nbGroupElements + iElem * nbNodes;
      }
    }
    if (group->getGhostPartition() == -1)
      nbGroupElements += group->getNbElements() * nbNodes;
  }

  std::vector<dgGroupOfElements*> newGroups;
  std::vector<dgGroupOfElements*> newGhostGroups;
  int maxTag = 0;
  for (std::map<std::pair<dgGroupOfElements*, int>, std::vector<MElement*> >::iterator it = newGroupsMap.begin(); it != newGroupsMap.end(); it++){
    dgGroupOfElements *oldGroup = it->first.first;
    dgGroupOfElements *newGroup = new dgGroupOfElements(_gc, it->second, oldGroup->getOrder());
    newGroup->copyPrivateDataFrom(oldGroup);
    int iTag = it->first.second;
    maxTag = std::max(maxTag, iTag);
    newGroup->setMultirateTag(iTag);
    if (newGroup->getGhostPartition() == -1){
      newGroups.push_back(newGroup);
    }
    else{
      newGhostGroups.push_back(newGroup);
    }
  }
  #ifdef HAVE_MPI
  MPI_Allreduce(&maxTag,   &maxTag,   1,   MPI_INT,   MPI_MAX,   MPI_COMM_WORLD);
  #endif
  _maxExponent = (maxTag+1)/3;
  _dtRef = dtRef/pow(2, maxExponent-_maxExponent);
  

  int nbNewGroupElements = 0;
  for (size_t iGroup = 0; iGroup < newGroups.size(); iGroup++){
    dgGroupOfElements *group = newGroups[iGroup];
    int nbNodes = group->getNbNodes();
    for (size_t iElem = 0; iElem < group->getNbElements(); iElem++)
      dataMap[group->getElement(iElem)->getNum()].second = nbNewGroupElements+iElem*nbNodes;
    nbNewGroupElements += group->getNbElements() * nbNodes;
  }

  _gc->setGroups(newGroups, newGhostGroups);

  for (size_t iDof = 0; iDof < dofs.size(); iDof++){
    dofs[iDof]->changeGroups(_gc, dataMap);
#ifdef HAVE_MPI
    if (!_gc->getIsDefinedOnOneProcessor()){
      dofs[iDof]->scatter(true);
    }
#endif
  }
  unsigned long workSingleRate = 0;
  unsigned long workMultiRate = 0;
  for (int iGroup = 0; iGroup < _gc->getNbElementGroups(); iGroup++){
    dgGroupOfElements *group = _gc->getElementGroup(iGroup);
    int tag = group->getMultirateTag();
    if(_mrType>0)
      tag += 1;
    int nbElements = group->getNbElements();
    workSingleRate += nbElements;
    workMultiRate += nbElements * (unsigned long) pow(2, _maxExponent - tag/3);
  }
#ifdef HAVE_MPI
  if (!_gc->getIsDefinedOnOneProcessor()){
    int workSingleRateG, workMultiRateG;
    MPI_Allreduce((void *) &workSingleRate, &workSingleRateG, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce((void *) &workMultiRate, &workMultiRateG, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
    workSingleRate = workSingleRateG;
    workMultiRate = workMultiRateG;
  }
#endif
  _speedUp = (double) (_dtRef*workSingleRate)/(_dtMin*workMultiRate);
  computePartitionInfo();
  return _dtRef;
}



void dgMultirateERK::printMultirateInfo(int rank){
  if (rank > (Msg::GetCommSize()-1))
    Msg::Fatal("There is no partition with rank %d", rank);

  if (Msg::GetCommRank() == rank){
    int NbElements = 0; 
    int BulkElements = 0;
    int IBufferElements = 0;
    int OBufferElements = 0;
    printf("\n*** Multirate Information For Partion %d *** \n \n", rank);
    printf("Maximum Multirate Exponent = %d \n", _maxExponent);
    for (int iGroup = 0; iGroup < _gc->getNbElementGroups(); iGroup++){
      dgGroupOfElements *group = _gc->getElementGroup(iGroup);
      int nbElementsInGroup = group->getNbElements();
      NbElements+=nbElementsInGroup;
      if (group->getMultirateTag() % 3 == 0)
        BulkElements+=nbElementsInGroup;
      else if (group->getMultirateTag() % 3 == 1)
        IBufferElements+=nbElementsInGroup;
      else if (group->getMultirateTag() % 3 == 2)
        OBufferElements+=nbElementsInGroup;
      if (Msg::GetCommRank() == rank)
        printf("TAG: %3d  (%d elements) \n", group->getMultirateTag(), nbElementsInGroup);
    }
    printf("\nBulk         : %2.2f%% \n", (double)BulkElements/NbElements*100.);  
    printf("Inner Buffer : %2.2f%% \n", (double)IBufferElements/NbElements*100.); 
    printf("Outer Buffer : %2.2f%% \n\n", (double)OBufferElements/NbElements*100.);
  }
}

void dgMultirateERK::printButcherTables(){
  if (Msg::GetCommRank() == 0){
    printf("\n ************************** \n");
    printf(" ***     Bulk Table     *** \n");
    printf(" ************************** \n \n");
    for (int i = 0; i < _nStages; i++){
      for (int j = 0;j < _nStages; j++){
        if (j == 0) printf(" % .4f|", _cBulk[i]);
        printf(" % .4f", _aBulk[_nStages * i + j]);
      }
      printf("\n");
    }
      printf("---------");
    for (int j = 0;j < _nStages; j++){
      printf("--------");
    }
    printf("\n");
    for (int j = 0;j < _nStages; j++){
      if (j == 0) printf("\t|");
      printf(" % .4f", _bBulk[j]);
    }
    printf("\n \n");
  
    printf("\n ************************** \n");
    printf(" *** Outer Buffer Table *** \n");
    printf(" ************************** \n \n");
    int nOB = _cOuterBuffer.size(); 
    for (int i = 0; i < nOB; i++){
      for (int j = 0; j < nOB; j++){
        if (j == 0) printf(" % .4f|", _cOuterBuffer[i]);
        printf(" % .4f", _aOuterBuffer[nOB * i + j]);
      }
      printf("\n");
    }
      printf("---------");
    for (int j = 0; j < nOB; j++){
      printf("--------");
    }
    printf("\n");
    for (int j = 0; j < nOB; j++){
      if (j == 0) printf("\t|");
      printf(" % .4f", _bOuterBuffer[j]);
    }
    printf("\n \n");
    
    if (_cInnerBuffer.size() > 0){
      int nIB = _cInnerBuffer.size();
      printf("\n ************************** \n");
      printf(" *** Inner Buffer Table *** \n");
      printf(" ************************** \n \n");
      for (int i = 0; i < nIB; i++){
        for (int j = 0; j < nIB; j++){
          if (j == 0) printf(" % .4f|", _cInnerBuffer[i]);
          printf(" % .4f", _aInnerBuffer[nIB * i + j]);
        }
        printf("\n");
      }
        printf("---------");
      for (int j = 0; j < nIB; j++){
        printf("--------");
      }
      printf("\n");
      for (int j = 0; j < nIB; j++){
        if (j == 0) printf("\t|");
        printf(" % .4f", _bInnerBuffer[j]);
      }
      printf("\n \n");
    }
  }
}

void dgMultirateERK::computePartitionInfo(){
  int cSize;
  if(_mrType > 0){
    cSize = 2 * _maxExponent + 1;
  }
  else{
    cSize = _maxExponent + 1;
  }
  _imbalances.resize(cSize);
  _idealLoads.resize(cSize);
  _maximLoads.resize(cSize);
  _compuLoads.resize(cSize);
  _effecLoads.resize(cSize);

  for (int i = 0; i < cSize; i++){
    _effecLoads[i]=0.0;
    _imbalances[i] = 0.0;
    _idealLoads[i] = 0.0;
    _compuLoads[i] = 0.0;
    _maximLoads[i] = 0.0;
  }
  double totalWork = 0.0;
  _globalImbalance = 0.0;
  _bestImbalance = 0.0;
  for (int iGroup = 0; iGroup < _gc->getNbElementGroups(); iGroup++){
    dgGroupOfElements *group = _gc->getElementGroup(iGroup);
    int tag = group->getMultirateTag();
    if(_mrType > 0)
      tag += 1;
    int sCon = tag/3;
    totalWork += (double) group->getNbElements() * pow(2,_maxExponent - sCon);
    for (size_t iElem = 0; iElem < group->getNbElements(); iElem++){
      //MElement *el = group->getElement(iElem);
      //int iPart;
      //if(Msg::GetCommSize() > 1)
      //  iPart = el->getPartition() - 1;
      //else
      //  iPart = 0;
      for (int iExp = sCon; iExp < (_maxExponent+1); iExp++){
        _effecLoads[iExp] += 1.0;
      }
      if(_mrType > 0 && sCon > 0){
        _effecLoads[_maxExponent + sCon] += 1.0;
      }
    }
  }
  if(_mrType > 0)
    totalWork *= 2;
#ifdef HAVE_MPI
  MPI_Allreduce((void *)&totalWork,   &totalWork,   1,   MPI_DOUBLE,   MPI_SUM,   MPI_COMM_WORLD);
  MPI_Allreduce(&(_effecLoads[0]),   &(_idealLoads[0]), cSize,   MPI_DOUBLE,   MPI_SUM,   MPI_COMM_WORLD);
#endif
  for (int iExp = 0; iExp < cSize; iExp++){
    _idealLoads[iExp] /= Msg::GetCommSize();
  }
#ifdef HAVE_MPI
  MPI_Allreduce(&(_effecLoads[0]),   &(_maximLoads[0]),   cSize,   MPI_DOUBLE,   MPI_MAX,   MPI_COMM_WORLD);
#endif
  for (int iExp = 0; iExp < cSize; iExp++){
    _imbalances[iExp] = _maximLoads[iExp]/_idealLoads[iExp];
    double frac;
    if(iExp < _maxExponent + 1){
      frac = std::max(1.0,  pow(2,  _maxExponent - iExp - 1));
      if(_mrType > 0 && iExp == 0){
        frac += pow(2, _maxExponent);
      }
    }
    else{
      frac = pow(2, cSize - iExp - 1);
    }
    _compuLoads[iExp] = _idealLoads[iExp]*(Msg::GetCommSize())*frac/totalWork;
    _globalImbalance += _compuLoads[iExp]*_imbalances[iExp];
    _bestImbalance +=  _compuLoads[iExp]*(ceil(_idealLoads[iExp])/_idealLoads[iExp]);
  }
}

void dgMultirateERK::updatePartitionInfo(int nbPart){
  if(Msg::GetCommSize() > 1){
    Msg::Error("You can only update the partitions info in sequential \n");
  }
  int cSize;
  if(_mrType > 0){
    cSize = 2 * _maxExponent + 1;
  }
  else{
    cSize = _maxExponent + 1;
  }
  _imbalances.resize(cSize);
  _idealLoads.resize(cSize);
  _maximLoads.resize(cSize);
  _compuLoads.resize(cSize);
  _effecLoads.resize(cSize * nbPart);

  for (int i = 0; i < nbPart*cSize; i++){
    _effecLoads[i]=0.0;
  }
  for (int i = 0; i < cSize; i++){
    _imbalances[i] = 0.0;
    _idealLoads[i] = 0.0;
    _compuLoads[i] = 0.0;
    _maximLoads[i] = 0.0;
  }
  double totalWork = 0.0;
  _globalImbalance = 0.0;
  _bestImbalance = 0.0;
  for (int iGroup = 0; iGroup < _gc->getNbElementGroups(); iGroup++){
    dgGroupOfElements *group = _gc->getElementGroup(iGroup);
    int tag = group->getMultirateTag();
    if(_mrType > 0)
      tag += 1;
    int sCon = tag/3;
    totalWork += (double) group->getNbElements() * pow(2,_maxExponent - sCon);
    for (size_t iElem = 0; iElem < group->getNbElements(); iElem++){
      MElement *el = group->getElement(iElem);
      int iPart;
      if(nbPart > 1)
        iPart = el->getPartition() - 1;
      else
        iPart = 0;
      for (int iExp = sCon; iExp < (_maxExponent+1); iExp++){
        _effecLoads[iPart * cSize + iExp] += 1.0;
        _idealLoads[iExp] += 1.0/nbPart;
        _compuLoads[iExp] += 1.0;
      }
      if(_mrType > 0 && sCon > 0){
        _effecLoads[iPart*cSize + _maxExponent + sCon] += 1.0;
        _idealLoads[_maxExponent + sCon] += 1.0/nbPart;
        _compuLoads[_maxExponent + sCon] += 1.0;
      }
    }
  }
  if(_mrType > 0)
    totalWork *= 2;

  for (int iPart = 0; iPart < nbPart; iPart++){
    for (int iExp = 0; iExp < cSize; iExp++){
      _maximLoads[iExp] = std::max(_maximLoads[iExp], _effecLoads[iPart * cSize + iExp]);
    }
  }
  for (int iExp = 0; iExp < cSize; iExp++){
    _imbalances[iExp] = _maximLoads[iExp]/_idealLoads[iExp];
    double frac;
    if(iExp < _maxExponent + 1){
      frac = std::max(1.0,  pow(2,  _maxExponent - iExp - 1));
      if(_mrType > 0 && iExp == 0){
        frac += pow(2, _maxExponent);
      }
    }
    else{
      frac = pow(2, cSize - iExp - 1);
    }
    _compuLoads[iExp] *= frac/totalWork;
    _globalImbalance += _compuLoads[iExp]*_imbalances[iExp];
    _bestImbalance +=  _compuLoads[iExp]*(ceil(_idealLoads[iExp])/_idealLoads[iExp]);
  }

}
void dgMultirateERK::printPartitionInfo(){
  if(Msg::GetCommRank()==0){
    int cSize;
    if(_mrType > 0){
      cSize = 2 * _maxExponent + 1;
    }
    else{
      cSize = _maxExponent + 1;
    }
    std::vector < std::pair<double, int> > index_loads(cSize);
    for(size_t ii = 0; ii < _compuLoads.size(); ii++){
      index_loads[ii]=std::make_pair(_compuLoads[ii], ii);
    }
    std::sort(index_loads.begin(), index_loads.end());
    std::reverse(index_loads.begin(), index_loads.end());
    printf("Ideal       Loads = [");
    for (int iExp = 0; iExp < cSize; iExp++){
      printf(" %02.2f", _idealLoads[index_loads[iExp].second]);
    }
    printf("] \n");
    printf("Maximum     Loads = [");
    for (int iExp = 0; iExp < cSize; iExp++){
      printf(" %02.2f", _maximLoads[index_loads[iExp].second]);
    }
    printf("] \n");
    printf("Ideal Imbalances  = [");
    for (int iExp = 0; iExp < cSize; iExp++){
      printf(" %02.4f", (ceil(_idealLoads[index_loads[iExp].second])/_idealLoads[index_loads[iExp].second]));
    }
    printf("] \n");
    printf("Imbalances        = [");
    for (int iExp = 0; iExp < cSize; iExp++){
      printf(" %02.4f", _imbalances[index_loads[iExp].second]);
    }
    printf("] \n");
    printf("Constraint Weight = [");
    for (int iExp = 0; iExp < cSize; iExp++){
      printf(" %02.4f", _compuLoads[index_loads[iExp].second]);
    }
    printf("] \n");
    printf("GLOBAL IMBALANCE=%f \n", _globalImbalance);
    printf("BEST POSSIBLE IMBALANCE=%f \n", _bestImbalance);
  }
}
