#include "dgSteady.h"
#include "dgNewton.h"
#include "dgDofContainer.h"
#include "dgDofManager.h"
#include "dgConservationLaw.h"
#include "GmshConfig.h"
#include<sstream>
#ifdef HAVE_MPI
#include "mpi.h"
#endif

dgSteady::dgSteady (dgConservationLaw &claw, dgDofManager &dof) :
    _newton(dof, claw)
{
}

void dgSteady::solve(dgDofContainer &solution)
{
  solution.copy(_newton.solve(NULL, 0., NULL, 0., &solution));
}
