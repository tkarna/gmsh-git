#include "dgAdamMoulton.h"
#include "dgDofContainer.h"
#include "dgDofManager.h"
#include "dgConservationLaw.h"
#include "GmshConfig.h"
#include<sstream>
#ifdef HAVE_MPI
#include "mpi.h"
#endif

static long int fact (int i) {
  long int r = 1;
  for (int j = 1; j <= i; j++)
    r *= j;
  return r;
}


//from http://en.wikipedia.org/wiki/Linear_multistep_method
static double adamsMoultonCoeff (int order, int j)
{
  std::vector<long int> pol(order);
  pol[0] = 1;
  for (int i = 0; i<order; i++) {
    if (i==j) continue;
    for (int k = i; k>=0; k--) {
      if (k!=order-1)
        pol[k+1] += pol[k]; // u
      pol[k]*=(i-1);      // + i-1
    }
  }
  long int n = 0, d = fact (order);
  for (int i = 0; i<order; i++)
    n += pol[i]*d/(i+1);
  d *= fact(j)*fact(order-j-1);
  return ((double)n)/ ((j%2)?-d:d);
}

/* just in case ...
  static double adamsBashforthCoeff (int order, int j) {
  std::vector<long int> pol(order+1);
  pol[0] = 1;
  for (int i = 0; i<order; i++) {
    if (i==j) continue;
    for (int k = i; k>=0; k--) {
      if (k!=order)
        pol[k+1] += pol[k]; // u
      pol[k]*=i;      // + i
    }
  }
  long int n = 0, d = fact (order+1);
  for (int i = 0; i<order+1; i++)
    n += pol[i]*d/(i+1);
  d*=fact(j)*fact(order-j-1);
  return  ((double)n)/ ((j%2)?-d:d);
}*/


dgAdamsMoulton::dgAdamsMoulton (int order, dgDofContainer &solution, const dgConservationLaw *claw, dgDofManager *dof, double dt, double t) :
    _newton (*dof, *claw)
{
  _solution = &solution;
  _claw = claw;
  _dof = dof;
  _dt = dt;
  _t = t;
  _previousF.resize (order-1);
  _coeff.resize (order);
  for (int i = 0; i < order-1; i++)
    _previousF[i] = new dgDofContainer (solution);
  for (int i = 0; i < order; i++)
    _coeff[i] = adamsMoultonCoeff (order, i);
}


dgAdamsMoulton::~dgAdamsMoulton()
{
  for (size_t i = 0; i < _previousF.size(); i++)
    delete _previousF[i];
}


void dgAdamsMoulton::iterate() 
{
  dgDofContainer R (*_solution);
  R.setAll(0);
  for (size_t i = 0; i < _previousF.size(); i++)
    R.axpy(*_previousF[i], _coeff[i+1]*_dt/_coeff[0]);
  _solution->axpy (_newton.solve(_solution, 1./(_coeff[0]*_dt), &R, _t, _solution));
  dgDofContainer *F0 = _previousF[_previousF.size()-1];
  for (int i = _previousF.size() - 1; i > 0; i--)
    _previousF[i] = _previousF[i-1];
  _previousF[0] = F0;
  F0->copy (_newton.getF());
  _t += _dt;
}
