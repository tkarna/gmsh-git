#include "dgROK.h"
#include "dgJacobianOperators.h"
#include "dgArnoldiIteration.h"
#include "dgDofContainer.h"
#include "dgDofManager.h"
#include "dgConservationLaw.h"
#include "GmshConfig.h"
#include<sstream>
#ifdef HAVE_MPI
#include "mpi.h"
#endif

static void copyButcherInVector1d (int n, const double *v,std::vector<double> &vv)
{
  vv.resize(n);
  for (int i = 0; i < n; i ++)
  {
    vv[i] = v[i];
  }
}

static void copyButcherInVector2d (int n, const double *m,std::vector<double> &vm)
{
  vm.resize(n*n);
  for (int i = 0; i < n; i ++)
    for (int j = 0; j < n; j++)
      vm[i*n+j] = m[n*i+j];
}

/* ROK */

void dgROK::setCoef(DG_ROK_TYPE order) {
  switch (order) {
    case DG_ROK_4A:
    {
      double a[4*4] = {
        0.0, 0.0, 0.0, 0.0,
        1.0, 0.0, 0.0, 0.0,
        0.1084530016931939175868117432550153454393009345950250308, 0.3915469983068060824131882567449846545606990654049749692, 0.0, 0.0,
        0.4345304775600447762471370176270279643577768155816826992, 0.1448434925200149254157123392090093214525922718605608997, (-0.0793739700800597016628493568360372858103690874422435989), 0.0
      };
      double g[4*4] = {
        0.0, 0.0, 0.0, 0.0,
        (-1.91153192976055097824558940682133229601582735199337138313), 0.0, 0.0, 0.0,
        0.3288182406115352215636148889409996289068084425348308862, 0.0, 0.0, 0.0,
        0.0330364423979581129062589491015367687146658980867715225, (-0.2437515237610823531197130316973934919183493340255052118), (-0.1706260299199402983371506431639627141896309125577564011), 0.0
      };
      double b[4] = {
        1.0/6.0, 1.0/6.0, 0.0, 2.0/3.0
      };
      double c[4] = {
        0.0, 1.0, 0.5, 0.5
      };
      double gamma = 0.572816062482134855408001384976768340931514124329888624090;
      
      copyButcherInVector2d(4, a, _a);
      copyButcherInVector1d(4, b, _b);
      copyButcherInVector1d(4, c, _c);
      copyButcherInVector2d(4, g, _g);
      _gamma = gamma;
      break;
    }
    case DG_ROK_4B:
    {
      double a[6*6] = {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        1.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.530633333333333, -0.030633333333333, 0.0, 0.0, 0.0, 0.0,
        0.894444444444444, 0.055555555555556, 0.05, 0.0, 0.0, 0.0,
        0.738333333333333, -0.121666666666667, 0.333333333333333, 0.05, 0.0, 0.0,
        -0.096929102825711, -0.121666666666667, 1.045582889789120, 0.173012879703258, 0.0, 0.0
      };
      double g[6*6] = {
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        -22.824608269858540, 0.0, 0.0, 0.0, 0.0, 0.0,
        -69.343635255712726, -0.030633333333333, 0.0, 0.0, 0.0, 0.0,
        404.7106882480958, 0.055555555555556, 0.05, 0.0, 0.0, 0.0,
        -0.571666666666667, -0.121666666666667, 0.333333333333333, 0.05, 0.0, 0.0,
        0.263595769492377, -0.121666666666667, -0.378916223122453, -0.073012879703258, 0.0, 0.0
      };
      double b[6] = {
        0.166666666666667, -0.243333333333333, 0.666666666666667, 0.100000000000000, 0.0, 0.31
      };
      double c[6] = {
        0.0, 1.0, 0.5, 1.0, 1.0, 1.0
      };
      double gamma = 0.31;
      
      copyButcherInVector2d(6, a, _a);
      copyButcherInVector1d(6, b, _b);
      copyButcherInVector1d(6, c, _c);
      copyButcherInVector2d(6, g, _g);
      _gamma = gamma;
      break;
    }
    case DG_ROK_4P:
    {
      double a[5*5] = {
        0.0, 0.0, 0.0, 0.0, 0.0,
        0.757900000000000, 0.0, 0.0, 0.0, 0.0,
        0.170400000000000, 0.821100000000000, 0.0, 0.0, 0.0,
        1.196218621274069, 0.297700000000000, -1.433618621274069, 0.0, 0.0,
        -0.010650410785863, 0.142100000000000, -0.129349589214137, 0.392800000000000, 0.0
      };
      double g[5*5] = {
        0.0, 0.0, 0.0, 0.0, 0.0,
        -0.757900000000000, 0.0, 0.0, 0.0, 0.0,
        -0.295086678808293, 0.178900000000000, 0.0, 0.0, 0.0,
        -1.836333117783808, -0.247700000000000, 1.681409044712106, 0.0, 0.0,
        -0.197089800872483, -0.684644029868020, 0.166330242942910, 0.0, 0.0
      };
      double b[5] = {
        0.056000000000000, 0.116601238130482, 0.160300000000000, -0.031109354304222, 0.698208116173739
      };
      double c[5] = {
        0.0, 0.757900000000000, 0.991500000000000, 0.060300000000000, 0.394900000000000
      };
      double gamma = 0.572816062482134855408001384976768340931514124329888624090;
      
      copyButcherInVector2d(5, a, _a);
      copyButcherInVector1d(5, b, _b);
      copyButcherInVector1d(5, c, _c);
      copyButcherInVector2d(5, g, _g);
      _gamma = gamma;
      break;
    }
    default:
      Msg::Fatal("Unrecognized Rosenbrock-Krylov method id %d", order);
  }
}

dgROK::dgROK(dgConservationLaw &claw, dgDofManager &dof, DG_ROK_TYPE order, int basisSize = 10) :
  _F(dof.getGroups(), claw.getNbFields()),
  _Y(dof.getGroups(), claw.getNbFields()),
  _phi(basisSize),
  _lambdaSum(basisSize),
  _rhs(basisSize),
  _Imhgh(basisSize, basisSize)
{
  _jacobian = new dgJacobianOperators(claw, dof.getGroups());
  _ownJacobian = true;
  _arnoldi = new dgArnoldiIteration(_jacobian, basisSize);
  _ownArnoldi = true;
  setCoef(order);
  _lambda.resize(_b.size(), _phi);
  _K.resize(_b.size(), _Y);
}

dgROK::dgROK(dgConservationLaw &claw, dgGroupCollection &groups, DG_ROK_TYPE order, int basisSize = 10) :
  _F(groups, claw.getNbFields()),
  _Y(groups, claw.getNbFields()),
  _phi(basisSize),
  _lambdaSum(basisSize),
  _rhs(basisSize),
  _Imhgh(basisSize, basisSize)
{
  _jacobian = new dgJacobianOperators(claw, groups);
  _ownJacobian = true;
  _arnoldi = new dgArnoldiIteration(_jacobian, basisSize);
  _ownArnoldi = true;
  setCoef(order);
  _lambda.resize(_b.size(), _phi);
  _K.resize(_b.size(), _Y);
}

dgROK::dgROK(dgJacobianOperators &jac, DG_ROK_TYPE order, int basisSize = 10) :
  _jacobian(&jac),
  _ownJacobian(false),
  _F(*(jac.getTerm())),
  _Y(*(jac.getTerm())),
  _phi(basisSize),
  _lambdaSum(basisSize),
  _rhs(basisSize),
  _Imhgh(basisSize, basisSize)
{
  _arnoldi = new dgArnoldiIteration(_jacobian, basisSize);
  _ownArnoldi = true;
  setCoef(order);
  _lambda.resize(_b.size(), _phi);
  _K.resize(_b.size(), _Y);
}

dgROK::~dgROK()
{
  if (_arnoldi && _ownArnoldi) delete _arnoldi;
  if (_jacobian && _ownJacobian) delete _jacobian;
}

void dgROK::iterate(dgDofContainer &solution, double dt, double t) {
  if (dt <= 0)
    Msg::Fatal("Time step must be greater than zero");
  int nsteps = _b.size();
  function::getDT()->set(dt);
  
  // Compute the Krylov subspace.
  _arnoldi->init(solution, t);
  
  _arnoldi->computeBasis();
  
  //_arnoldi->validate(solution, t);
  
  // Compute Imhgh = I - dt * gamma * H and LU factor
  _Imhgh = _arnoldi->getHMatrix();
  _Imhgh.scale(-dt*_gamma);
  for (int i = 0; i < _Imhgh.size1(); ++i) {
    _Imhgh(i, i) += 1.0;
  }
  
  // Perform LU factorization.
  _Imhgh.luFactor(_ipiv);
//  _Imhgh.invertInPlace();
  
  for (int i = 0; i < nsteps; ++i) {
    subiterate(solution, dt, t, i);
  }
  finalize(solution);
}

void dgROK::subiterate(dgDofContainer &solution, double dt, double t, int step) {
  int nsteps = _b.size();
  _Y.copy(solution);
  _lambdaSum.setAll(0.0);
  
  for (int j = 0; j < step; ++j) {
    // sum_j(a_ij * k_j)
    _Y.axpy(_K[j], _a[step*nsteps+j]);
    // sum_j(g_ij * lambda_j)
    _lambdaSum.axpy(_lambda[j], _g[step*nsteps+j]);
  }
  
  // f = F(t + c[i] * dt, y + sum_j(a_ij * k_j))
  function::getSubDT()->set(_c[step]*dt);
  _jacobian->computeTermAndJacobian(_Y, t + _c[step]*dt, false);
  _F.copy(*(_jacobian->getTerm()));
  
  // phi = (V^T)*f + w
  _phi = _arnoldi->getWArray();
  _arnoldi->matvecVtranspose(_F, _phi);
  
  // rhs = dt * (phi + H * sum_j(g_ij * lambda_j))
  _rhs = _phi;
  (_arnoldi->getHMatrix()).multAddy(_lambdaSum, _rhs);
  _rhs.scale(dt);
  
  // Solve for lambda_i = _Imhgh^-1 * rhs
  _Imhgh.luSubstitute(_rhs, _ipiv, _lambda[step]);
//  _Imhgh.mult(_rhs, _lambda[step]);
//  _Imhgh.luSolve(_rhs, _lambda[step]);
  
  // Recover K_i = V * lambda_i - dt * (V * phi - f)
  _K[step].copy(_F);
  _K[step].scale(-1.0);
  _arnoldi->matvecV(_phi, _K[step]);
  _K[step].scale(-dt);
  _arnoldi->matvecV(_lambda[step], _K[step]);
}

void dgROK::finalize(dgDofContainer &solution) {
  int nsteps = _b.size();
  
  for (int i = 0; i < nsteps; ++i) {
    solution.axpy(_K[i], _b[i]);
  }
}

dgERK_test::dgERK_test(dgConservationLaw &claw, const dgGroupCollection &groups) :
  _Y(groups, claw.getNbFields())
{
  _jacobian = new dgJacobianOperators(claw, groups);
  _K.resize(4, _Y);
	
	const double a[4 * 4] = {
		0., 0., 0., 0.,
		.5, 0., 0., 0.,
		0., .5, 0., 0.,
		0., 0., 1., 0.
	};
	const double b[4] = {1./6, 1./3, 1./3, 1./6};
	const double c[4] = {0, 0.5, 0.5, 1};
	copyButcherInVector2d(4, a, _a);
	copyButcherInVector1d(4, b, _b);
	copyButcherInVector1d(4, c, _c);
}

dgERK_test::~dgERK_test() {
	if(_jacobian) delete _jacobian;
}

void dgERK_test::finalize(dgDofContainer &solution) {
	int nsteps = _b.size();
	for (int i = 0; i < nsteps; ++i) {
		solution.axpy(_K[i], _b[i]);
	}
}

void dgERK_test::iterate(dgDofContainer &solution, double dt, double t) {
	int nsteps = _b.size();
  function::getDT()->set(dt);
  for (int  i = 0; i < nsteps; i++) {
    subiterate(solution, dt, t, i);
  }
  finalize(solution);
}

void dgERK_test::subiterate(dgDofContainer &solution, double dt, double t, int step) {
	int nsteps = _b.size();
  _Y.copy(solution);
  
  for (int j = 0; j < step; ++j) {
    // sum_j(a_ij * k_j)
    _Y.axpy(_K[j], _a[step*nsteps+j]);
  }
  
  // f = F(t + c[i] * dt, y + sum_j(a_ij * k_j))
  function::getSubDT()->set(_c[step]*dt);
  _jacobian->computeTermAndJacobian(_Y, t + _c[step]*dt, false);
  
  // K_i = f
  _K[step].copy(*(_jacobian->getTerm()));
	_K[step].scale(dt);
}
