#ifndef _DG_ARNOLDI_ITERATION_H_
#define _DG_ARNOLDI_ITERATION_H_

class dgConservationLaw;
class dgDofManager;
//class dgJacobianOperators;

#include "dgDofContainer.h"
#include "dgJacobianOperators.h"

class dgArnoldiIteration
{
  int _M;
  dgJacobianOperators *_J;
  bool _ownJacobianOperators;
  std::vector<dgDofContainer> _V;
  fullVector<double> _W;
  fullMatrix<double> _H;
  dgDofContainer _dFdt;
  dgDofContainer _U;
  double _t;
  bool _fullJacobian;
 public:
  dgArnoldiIteration(dgConservationLaw &claw, const dgGroupCollection &groups, int basisSize);
  dgArnoldiIteration(dgJacobianOperators *J, int basisSize);
  ~dgArnoldiIteration();
  void init(const dgDofContainer &U0, double t);
  void computeBasis();
  fullMatrix<double> &getHMatrix() { return _H; }
  std::vector<dgDofContainer> &getVMatrix() { return _V; }
  fullVector<double> &getWArray() { return _W; }
  dgDofContainer &getVi(int index) { return _V.at(index); }
  inline double getWi(int index) { return _W(index); }
  void matvecV(fullVector<double> &x, dgDofContainer &y, bool zeroY = false);
  void matvecVtranspose(dgDofContainer &x, fullVector<double> &y, bool zeroY = false);
  void validate(dgDofContainer &U0, double t);
  inline int getM() { return _M; }
 private:
  double ddot(dgDofContainer &x, const dgDofContainer &y);
  void jacobianVectorProduct(const dgDofContainer &in, dgDofContainer &out);
};

#endif
