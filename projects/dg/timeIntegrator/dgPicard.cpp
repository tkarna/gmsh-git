#include "dgPicard.h"
#include "dgNewton.h"
#include "dgDofContainer.h"
#include "dgDofManager.h"
#include "dgConservationLaw.h"
#include "GmshConfig.h"
#include<sstream>
#ifdef HAVE_MPI
#include "mpi.h"
#endif

dgPicard::dgPicard(dgDofManager &dof, const dgConservationLaw &law) : dgNewton(dof, law, false), _linearizedSolution(dof.getGroups(), law.getNbFields()), _sparsityPatternFilled(false) {}

void dgPicard::assembleSystem (double alpha, const dgDofContainer *R, double t, dgDofContainer *variable) {
  _linearizedSolution.copy(_currentSolution);
  dgNewton::assembleSystem(alpha, R, t, variable);
}

const dgDofContainer & dgPicard::solve (const dgDofContainer *U0, double alpha, const dgDofContainer *R, double t, dgDofContainer *variable)
{
  if(!_sparsityPatternFilled) {
    _dof.fillSparsityPattern(!_dof.isContinuous(), &_law);
    _sparsityPatternFilled = true;
  }
  return dgNewton::solve(U0, alpha, R, t, variable);
}
