#include "dgERK.h"
#include "dgDofContainer.h"
#include "dgDofManager.h"
#include "dgConservationLaw.h"
#include "dgGroupOfElements.h"
#include "dgLimiter.h"
#include "dgTransformNodalValue.h"
#include "dgAssemblerExplicit.h"
#include "GmshConfig.h"
#include<sstream>
#ifdef HAVE_MPI
#include "mpi.h"
#endif

static void copyButcherInVector (int n, const double *a, const double *b, const double *c, std::vector<double> &va, std::vector<double> &vb, std::vector<double> &vc)
{
  va.resize(n * n);
  vb.resize(n);
  vc.resize(n);
  for (int i = 0; i < n; i ++)
  {
    vb[i] = b[i];
    vc[i] = c[i];
    for (int j = 0; j < n; ++j)
      va[i * n + j] = a[i * n + j];
  }
}

void dgERK::setCoef(DG_ERK_TYPE order) {
  switch (order) {
    case DG_ERK_EULER :
      {
        const double a[] = {0};
        const double b[] = {1};
        const double c[] = {0};
        copyButcherInVector(1, a, b, c, _a, _b, _c);
      }
      break;
    case DG_ERK_22 :
      {
        const double a[] = {0, 0, 1, 0};
        const double b[] = {0.5, 0.5};
        const double c[] = {0, 1};
        copyButcherInVector(2, a, b, c, _a, _b, _c);
      }
      break;
    case DG_ERK_33 :
      {
        const double a[] = 
        {0., 0., 0.,
          .5, 0., 0.,
          -1., 2., 0.};
        const double b[] = {1./6, 2./3, 1./6};
        const double c[] = {0, .5, 1.};
        copyButcherInVector(3, a, b, c, _a, _b, _c);
      }
      break;
    case DG_ERK_44 :
      {
        const double a[4 * 4] = 
        {0., 0., 0., 0.,
          .5, 0., 0., 0.,
          0., .5, 0., 0.,
          0., 0., 1., 0.};
        const double b[4] = {1./6, 1./3, 1./3, 1./6};
        const double c[4] = {0, 0.5, 0.5, 1};
        copyButcherInVector(4, a, b, c, _a, _b, _c);
      }
      break;
    case DG_ERK_44_3_8 :
      {
        const double a[4 * 4] =
        {0, 0, 0, 0,
          1.0/3.0, 0, 0 ,0,
          -1.0/3.0, 1.0, 0, 0,
          1, -1, 1, 0};
        const double b[4]={1./8, 3./8, 3./8, 1./8};
        const double c[4]={0, 1./3, 2./3, 1};
        copyButcherInVector(4, a, b, c, _a, _b, _c);
      }
      break;
    case DG_ERK_65_FEHLBERG :
      {
        const double a[6 * 6] =
        {0,0,0,0,0,0,
          1.0/4,0,0,0,0,0,
          3.0/32,9.0/32,0,0,0,0,
          1932.0/2197,-7200.0/2197,7296.0/2197,0,0,0,
          439.0/216,-8,3680.0/513,-845.0/4104,0,0,
          8.0/27,2,-3544.0/2565,-1859.0/4104,-11.0/40,0};
        const double b[6]={16.0/235,6656.0/12825,28561.0/56430,-9.0/50,2.0/55};
        double c[6];
        for (int i = 0; i < 6; ++i) {
          c[i] = 0;
          for(int j = 0; j < 6; ++j) {
            c[i] += a[i * 6 + j];
          }
        }
        copyButcherInVector(6, a, b, c, _a, _b, _c);
      }
      break;
    case DG_ERK_43_SCHLEGEL :
      {
        double a[4 * 4]={
          0, 0, 0, 0,
          1.0/2.0, 0, 0 ,0,
          -1.0/6.0, 2.0/3.0, 0, 0,
          1.0/3.0, -1.0/3.0, 1, 0
        };
        double b[4]={1.0/6.0, 1.0/3.0, 1.0/3.0, 1.0/6.0};
        double c[4]={0, 1.0/2.0, 1.0/2.0, 1};
        copyButcherInVector(4, a, b, c, _a, _b, _c);
      }
      break;
    default :
      Msg::Error("unknown runge kutta type : %i\n", order);
  }
}

dgERK::dgERK(dgConservationLaw &claw, dgDofManager *dof, DG_ERK_TYPE order) :
  _ownAssembler(true)
{
  _assembler = new dgAssemblerExplicit(dof, claw);
  _limiter = NULL;
  _transformNodalValue = NULL;
  setCoef(order);
  _istep = 0;
}

dgERK::dgERK (dgConservationLaw &claw, dgDofManager *dof, std::vector<double> a, std::vector<double> b, std::vector<double> c) :
  _ownAssembler(true), _a(a), _b(b), _c(c)
{
  _assembler = new dgAssemblerExplicit(dof, claw);
  _limiter = NULL;
  _transformNodalValue = NULL;
  _istep = 0;
}

dgERK::dgERK(dgAssemblerExplicit *assembler, DG_ERK_TYPE order):
  _assembler(assembler), _ownAssembler(false)
{
  _limiter = NULL;
  _transformNodalValue = NULL;
  setCoef(order);
  _istep = 0;
}

dgERK::~dgERK() {
  if(_ownAssembler && _assembler) delete _assembler;
}


double dgERK::computeInvSpectralRadius(dgDofContainer *solution) {
  double sr = 1.e22;
  const dgGroupCollection *groups = solution->getGroups();
  for (int i = 0; i < groups->getNbElementGroups(); i++) {
    std::vector<double> DTS;
    _assembler->getLaw().computeElementaryTimeSteps(*(groups->getElementGroup(i)), solution, DTS);
    for (size_t k = 0; k < DTS.size(); k++) sr = std::min(sr, DTS[k]);
  }
  #ifdef HAVE_MPI
  double sr_min;
  MPI_Allreduce((void *)&sr, &sr_min, 1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
  return sr_min;
  #else
  return sr;
  #endif
}
int dgERK::subiterate(dgDofContainer &solution, double dt, double t) {
  int nsteps = _b.size();
  //initialize
  if(_istep == 0) {
    _X = new dgDofContainer(solution);
    _X->copy(solution);
    if (dt <= 0)
      Msg::Fatal("Time step must be greater than zero");
    function::getDT()->set(dt);
    _du.resize(nsteps, *_X);
    if (_transformNodalValue) _transformNodalValue->apply(&solution);
  }

  //subiterate
  solution.copy(*_X);
  for (int j = 0; j < _istep; j ++){
    if (_a[_istep*nsteps+j] != 0.){
      solution.axpy(_du[j], _a[_istep*nsteps+j]);
    }
  }
  if (_limiter) _limiter->apply(_X);
  if (_transformNodalValue) _transformNodalValue->apply_Inverse(_X);
  function::getSubDT()->set(dt*_c[_istep]);
  _assembler->solve(solution, 1./dt, NULL, t + dt*_c[_istep], _du[_istep], false, &solution);
  _istep += 1;
  return _istep;
}

void dgERK::finalize(dgDofContainer &solution) {
  int nsteps = _b.size();
  for (int i = 0; i < nsteps; i++)
    _X->axpy (_du[i],  _b[i]);
  solution.copy(*_X);
  if (_limiter) _limiter->apply(&solution);
  if (_transformNodalValue) _transformNodalValue->apply_Inverse(&solution);
  if (_X)
    delete _X;
  _istep = 0;
}



void dgERK::iterate (dgDofContainer &solution, double dt, double t)
{
  int nsteps = _b.size();
  for (int  i = 0; i < nsteps; i++) {
    subiterate(solution, dt, t);
  }
  finalize(solution);
}

