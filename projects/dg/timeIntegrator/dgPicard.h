#ifndef _DG_PICARD_H_
#define _DG_PICARD_H_

class dgConservationLaw;
class dgDofManager;
//CHECK
//class dgNewton;

#include "dgNewton.h"
#include "dgDofContainer.h"

class dgPicard : public dgNewton
{
  dgDofContainer _linearizedSolution;
  bool _sparsityPatternFilled;
 public:
  dgPicard(dgDofManager &dof, const dgConservationLaw &law);
  virtual void assembleSystem (double alpha, const dgDofContainer *R, double t, dgDofContainer *variable = NULL);
  virtual const dgDofContainer &solve (const dgDofContainer *U0, double alpha, const dgDofContainer *R, double t, dgDofContainer *variable = NULL);
  inline const function * getLinearizedSolution()         { return _linearizedSolution.getFunction();         }
  inline const function * getLinearizedSolutionGradient() { return _linearizedSolution.getFunctionGradient(); }
};

#endif
