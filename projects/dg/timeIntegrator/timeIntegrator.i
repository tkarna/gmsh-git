%module timeIntegrator

%import "dofContainer.i";

%{
#undef HAVE_DLOPEN
#include "dgNewton.h"
#include "dgJacobianOperators.h"
#include "dgArnoldiIteration.h" 
#include "dgPicard.h"
#include "dgAssemblerExplicit.h"
#include "dgSteady.h"
#include "dgERK.h"
#include "dgERKLinear.h"
#include "dgROK.h"
#include "dgDIRK.h"
#include "dgIMEXRK.h"
#include "dgIMEXTSRK.h"
#include "dgIMEXDIMSIM.h"
#include "dgIMEXLMM.h"
#include "dgAdamMoulton.h"
#include "dgCrankNicholson.h"
#include "dgMultirateERK.h"
%}

%include "dgNewton.h"
%include "dgJacobianOperators.h"
%include "dgArnoldiIteration.h" 
%include "dgPicard.h"
%include "dgAssemblerExplicit.h"
%include "dgSteady.h"
%include "dgERK.h"
%include "dgERKLinear.h"
%include "dgROK.h"
%include "dgDIRK.h"
%include "dgIMEXRK.h"
%include "dgIMEXTSRK.h"
%include "dgIMEXDIMSIM.h"
%include "dgIMEXLMM.h"
%include "dgAdamMoulton.h"
%include "dgCrankNicholson.h"
%include "dgMultirateERK.h"
