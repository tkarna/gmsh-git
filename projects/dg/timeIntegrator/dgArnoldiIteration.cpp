#include "dgArnoldiIteration.h"
#include "dgDofContainer.h"
#include "dgConservationLaw.h"
#include "dgGroupOfElements.h"
#include "GmshConfig.h"
#include<sstream>
#ifdef HAVE_MPI
#include "mpi.h"
#endif

dgArnoldiIteration::dgArnoldiIteration(dgConservationLaw &claw, const dgGroupCollection &groups, int basisSize) :
  _M (basisSize),
  _W (basisSize),
  _H (basisSize, basisSize),
  _dFdt(groups, claw.getNbFields()),
  _U(groups, claw.getNbFields()),
  _t(0.0),
  _fullJacobian(true)
{
  _J = new dgJacobianOperators(claw, groups);
  _ownJacobianOperators = true;
}

dgArnoldiIteration::dgArnoldiIteration(dgJacobianOperators *J, int basisSize) :
  _M (basisSize),
  _J (J),
  _ownJacobianOperators (false),
  _W (basisSize),
  _H (basisSize, basisSize),
  _dFdt(*(J->getTerm())),
  _U(*(J->getTerm())),
  _t(0.0),
  _fullJacobian(true)
{
}

dgArnoldiIteration::~dgArnoldiIteration() {
  if(_J && _ownJacobianOperators) delete _J;
}

void dgArnoldiIteration::init(const dgDofContainer &U0, double t) {
  double beta = 0.0;
  double delta = 1.0e-7 + 1.0e-7*U0.norm();
  
  // Setup finite difference approximation of Jv:
  if (!_fullJacobian) {
    _U.copy(U0);
    _t = t;
  }
  
  // Get function evaluation for dFdt finite difference.
  _J->computeTermAndJacobian(U0, t+delta, false);
  _dFdt.copy(*(_J->getTerm()));
  
  // Get Jacobian and function for current time step.
  _J->computeTermAndJacobian(U0, t, _fullJacobian);
  
  // Compute dFdt.
  _dFdt.axpy(*(_J->getTerm()), -1.0);
  _dFdt.scale(1.0/delta);
  
  // Initialize Arnoldi.
  beta = _J->getTerm()->norm();
  beta = sqrt(beta*beta + 1.0);
  _V.resize(_M, *(_J->getTerm()));
  _V[0].copy(*(_J->getTerm()));
  _V[0].scale(1.0/beta);
  _W(0) = 1.0/beta;
}

double dgArnoldiIteration::ddot(dgDofContainer &x, const dgDofContainer &y) {
  int i;
  double ret = 0.0;
  fullVector<double> prod;
  
  x.dotProduct(y, prod);
  for (i = 0; i < prod.size(); ++i)
    ret += prod(i);
  
  return ret;
}

void dgArnoldiIteration::computeBasis() {
  int i, j;
  double beta, tau, xi, rho;
  dgDofContainer Zeta(_V[0]);
  
  for (i = 0; i < _M; ++i) {
    jacobianVectorProduct(_V[i], Zeta);
    Zeta.axpy(_dFdt, _W(i));
    xi = 0.0;
    tau = Zeta.norm();
    
    for (j = 0; j <= i; ++j) {
      _H(j, i) = ddot(Zeta, _V[j]) + xi * _W(j);
      Zeta.axpy(_V[j], -_H(j, i));
      xi = xi - _H(j, i) * _W(j);
    }
    
    beta = Zeta.norm();
    beta = sqrt(beta*beta + xi*xi);
    if (beta / tau <= 0.25) {
      for (j = 0; j < i; ++j) {
        rho = ddot(Zeta, _V[j]) + xi * _W(j);
        Zeta.axpy(_V[j], -rho);
        xi = xi - rho * _W(j);
        _H(j, i) += rho;
      }
      beta = Zeta.norm();
      beta = sqrt(beta*beta + xi*xi);
    }
    
    if (i < _M-1) {
      _H(i+1, i) = beta;
      _V[i+1].copy(Zeta);
      _V[i+1].scale(1.0/beta);
      _W(i+1) = xi/beta;
    }
  }
}

void dgArnoldiIteration::matvecV(fullVector<double> &x, dgDofContainer &y, bool zeroY) {
  int i;
  
  if (zeroY) y.setAll(0.0);
  
  for (i = 0; i < x.size(); ++i) {
    y.axpy(_V[i], x(i));
  }
}

void dgArnoldiIteration::matvecVtranspose(dgDofContainer &x, fullVector<double> &y, bool zeroY) {
  unsigned int i;
  
  if (zeroY) y.setAll(0.0);
  
  for(i = 0; i < _V.size(); ++i) {
    y(i) = ddot(_V[i], x);
  }
}

void dgArnoldiIteration::validate(dgDofContainer &U0, double t)
{
  // V^T * V = I
  int orthonormal_err_count = 0;
  std::vector<fullVector<double> > I_mxm;
  I_mxm.resize(_M, _W);
  for (int i = 0; i < _M; ++i) {
    matvecVtranspose(_V[i], I_mxm[i], true);
  }
  for (int i = 0; i < _M; ++i) {
    I_mxm[i](i) -= 1.0;
    for (int j = 0; j < _M; ++j) {
      I_mxm[i](j) += _W(i) * _W(j);
      if (I_mxm[i](j) > 1.0e-10)
        orthonormal_err_count++;
//        printf("I_mxm[%d][%d] = %18.12e\n", i, j, I_mxm[i](j));
    }
  }
  if (orthonormal_err_count > 0)
    printf("Arnoldi error: V is not orthonormal.\n");
 
  // Compute the Jacobian in case we haven't already.
  if (!_fullJacobian)
    _J->computeTermAndJacobian(U0, t, true);
  
  // V^T * J * V = H
  int vjv_err_count = 0;
  dgDofContainer jv(_V[0]);
  std::vector<fullVector<double> > vtjv;
  vtjv.resize(_M, _W);
  for(int i = 0; i < _M; ++i) {
    _J->jacobianMultiply(_V[i], jv);
    jv.axpy(_dFdt, _W(i));
    matvecVtranspose(jv, vtjv[i], true);
  }
//  fullVector<double> vtdfdt(_M);
//  matvecVtranspose(_dFdt, vtdfdt, true);
  for (int i = 0; i < _M; ++i) {
    for (int j = 0; j < _M; ++j) {
//      vtjv[i](j) += vtdfdt(i) * _W(j);
      double E = vtjv[i](j) - _H(i, j);
      if (E > 1.0e-10) {
        vjv_err_count++;
//        printf("vtjv[%d][%d] = %18.12e   ", i, j, vtjv[i](j));
//        printf("E[%d][%d] = %18.12e\n", i, j, E);
      }
    }
  }
  if (vjv_err_count > 0)
    printf("Arnoldi error: V'JV != H\n");
  
  // check Jacobian vector products.
  dgDofContainer u(_V[0]);
  dgDofContainer Ju(_V[0]);
  dgDofContainer F(_V[0]);
  dgDofContainer dF(_V[0]);
  double delta = 1.0e-7 + 1.0e-7*u.norm();
  
  // directly find Ju
  u.copy(_V[0]);
  _J->jacobianMultiply(u, Ju);
  printf("||Ju|| = %18.12e\n", Ju.norm());
  
  // compute finite difference approximation for Ju
  printf("t = %f\n", t);
  printf("||U|| = %18.12e\n", U0.norm());
  _J->computeTermAndJacobian(U0, t, false);
  F.copy(*(_J->getTerm()));
  printf("||F(U, t)|| = %18.12e\n", F.norm());
    
  dF.copy(U0);
  dF.axpy(u, delta);
  printf("delta = %18.12e\n", delta);
  printf("||U + deltaV|| = %18.12e\n", dF.norm());
  _J->computeTermAndJacobian(dF, t, false);
  dF.copy(*(_J->getTerm()));
  printf("||F(U + deltaV, t)|| = %18.12e\n", dF.norm());
  dF.axpy(F, -1.0);
  printf("||dF - F|| = %18.12e\n", dF.norm());
  dF.scale(1.0/delta);
  printf("||(1/delta)(dF - F)|| = %18.12e\n", dF.norm());
  
  dF.axpy(Ju, -1.0);
  printf("||Ju - (1/delta)(dF - F)|| = %18.12e\n", dF.norm());
  
}

void dgArnoldiIteration::jacobianVectorProduct(const dgDofContainer &in, dgDofContainer &out)
{
  if(_fullJacobian) {
    _J->jacobianMultiply(in, out);
  } else {
    double delta = 1.0e-7 + 1.0e-7 * in.norm();
    dgDofContainer F(in);
    _J->computeTermAndJacobian(_U, _t, false);
    F.copy(*(_J->getTerm()));
    
    out.copy(_U);
    out.axpy(in, delta);
    _J->computeTermAndJacobian(out, _t, false);
    out.copy(*(_J->getTerm()));
    out.axpy(F, -1.0);
    out.scale(1.0/delta);
  }
}
