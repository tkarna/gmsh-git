#ifndef _DG_MULTIRATE_ERK_H_
#define _DG_MULTIRATE_ERK_H_

class dgGroupCollection;
class dgGroupOfElements;
class dgDofContainer;
class dgGroupOfFaces;
class dgConservationLaw;

#include <vector>
#include <set>
#include "meshPartitionOptions.h"


/** The MultirateERK class provides explicit Runge-Kutta time-integration methods that allow to use different time setps on the elements */

/**
Multirate Methods of Constantinescu and Sandu (Journal of Scientific Computing, 2007)
 * ERK_22_C  : 2 stages,  second order in time,  conservative
 * ERK_33_C  : 3 stages,  second order in time,  conservative
 * ERK_43_C  : 4 stages,  second order in time,  conservative
 * ERK_44_C  : 4 stages,  second order in time,  conservative
Multirate methods of Schlegel et. al. (Journal of Computational and Applied Mathematics,  2009)
 * ERK_22_S  : 2 stages,  second order in time,  nonconservative
 * ERK_22_S_C: 2 stages,  second order in time,  conservative
 * ERK_43_S  : 4 stages,  third  order in time,  nonconservative
 * ERK_43_S_C: 4 stages,  third  order in time,  conservative
*/
typedef enum {ERK_22_C = 1, ERK_33_C = 3, ERK_43_C = 4,  ERK_44_C = 5,  ERK_22_S = 6, ERK_22_S_C = 7, ERK_43_S = 8, ERK_43_S_C = 9} ERK_TYPE;
class dgMultirateERK{
  private:
    /** Conservation law on which we will perform the multirate algorithm */
    const dgConservationLaw &_law;
    /** Group collection containing the different groups of elemnts */
    dgGroupCollection *_gc;
    /** Defines the multirate method */
    ERK_TYPE _rk_type;
    /** Multirate type: 0 for Constantinescu,  1 for Schlegel Non Conservative,  2 for Schlegel Conservative */ 
    int _mrType;
    /** Number of stages in the classical RK base method */
    int _nStages;
    /** Number of stages of the buffer method */
    int _nStagesBuffer;
    /** Total number of stages of one "large" iteration of the multirate method */
    int _nbLevels;
    /** Size of the inner buffer,  i.e. number of connected DG elements */
    int _innerBufferSize;
    /** Size of the outer buffer,  i.e. number of connected DG elements */
    int _outerBufferSize;
    /** Maximum multirate exponent,  i.e. the largest multirate time step is 2^(_maxExponent) * Dt_min */
    int _maxExponent;

    int nb_elements, nb_faces1, nb_faces2;
    /** Theoretical speedup obtained with the multirate method compared to teh equivalent singlerate method */
    double _speedUp;
    /** Minimum time step of the problem  */
    double _dtMin;
    /** Maximum time step of the problem  */
    double _dtMax;
    /** Refernce time step used by the multirate configuration, i.e. maximum of all the time steps used  */
    double _dtRef;
    /** Boolean variable which specifies if everything is initialized before iterating  */
    bool _init;

    /** Objects needed to perform the multirate algorithm */
    std::vector<double> _aBulk, _bBulk, _cBulk, _aInnerBuffer, _bInnerBuffer, _cInnerBuffer, _aOuterBuffer, _bOuterBuffer, _cOuterBuffer, _tLevel;
    std::vector<int> _kLevel, _stopCritU, _startCritU, _stopCritK, _startCritK;
    dgDofContainer **_K, **_KInt, *_currentInput, *_residual, *_residualInt;
  
    /** Information about the multi-constrained partitioning for multirate  */
    std::vector<double> _imbalances, _idealLoads, _maximLoads, _compuLoads, _effecLoads;
    double _globalImbalance;
    double _bestImbalance;

    /** Create Butcher Tableaus (bulk and buffer) of the considered multirate method  */
    void setButcherTables(ERK_TYPE rk_type);

    /** Set all the variables in a dgDofContainer that corresponds to a multirate tag to a value */
    void setAllTag(int tag, dgDofContainer &x, double val);
    /** Perform an axpy operation on the subset of the variables of a dgDofContainer that corresponds to a multirate tag */
    void axpyTag(int tag, dgDofContainer &x, const dgDofContainer &y, double val);
    /** Set all the variables that corresponds to a multirate tag in a dgDofContainer to zero*/
    void resetTag(int tag, dgDofContainer &x);
    
    /** Vector containing sets of groups of elements with the same multirate tag  */
    std::vector<std::set<dgGroupOfElements*> > _groupsOfElementsByTag;
    /** Vector containing sets of groups of faces with the same multirate tag  */
    std::vector<std::set<dgGroupOfFaces*> > _groupsOfFacesByTag;


    /** Computes the current solution at a certain level of the multirate algoritm which uses a dt time step */
    void computeInputForK(int level, const dgDofContainer &solution, double dt);
    /** Computes the flux (steday-state residual) at certain level of the multirate algorithm */
    void computeK(int level, double dt);
    /** Update the solution to the next time step */
    void updateSolution(int level, dgDofContainer &solution, double dt);

    /** Computes some information about the partitioning */
    void computePartitionInfo();

  public:
    /** Constructor */
    dgMultirateERK(dgGroupCollection *gc, dgConservationLaw &claw, ERK_TYPE rk_type);
    /** Copy constructor for a different conservation law */
    dgMultirateERK(dgConservationLaw &claw, dgMultirateERK *copy);
    /** Destructor */
    ~dgMultirateERK();

    /** Organize the elements in multirate groups (bulk,  inner buffer,  outer buffer) depending on their local time step and the multirate method */
    double splitGroupsForMultirate(int maxLevels, dgDofContainer *solution, std::vector<dgDofContainer*> dofs, double fact=1.);

    /** Initialization of all the variables before iterating */
    void initialize();
    /** Iterates the multirate algorithm with a time step "dt" from the solution at "t" */
    void iterate(dgDofContainer &solution, double dt, double t);
    /** Computes the partitions options for a classcial mesh partitioning with MeTiS algorithm "algo" */
    void computeClassicalContrainedPartition(meshPartitionOptions &opts,  int algo = 5);
    /** Computes the partitions options for a single-constrained mesh partitioning with MeTiS algorithm "algo" */
    void computeSingleContrainedPartition(meshPartitionOptions &opts,  int algo = 5);
    /** Computes the partitions options for a multi-constrained mesh partitioning with MeTiS algorithm "algo" */
    void computeMultiConstrainedPartition(meshPartitionOptions &opts,  int algo = 5, int nbconstraints=15);
    /** Print the Butcher Tableaus of the multirate method */
    void printButcherTables();
    /** Print some information about the multirate setup of partition "rank"*/
    void printMultirateInfo(int rank = 0);
    /** Print some information about the partitionning */
    void printPartitionInfo();
    /** Update the information about the partitionning */
    void updatePartitionInfo(int nbPart = 1);

    /** Returns the theoretical speedup of the multirate setup */
    inline double speedUp() const {return _speedUp;};
    /** Returns the minimum time step of the problem */
    inline double dtMin() const {return _dtMin;};
    /** Returns the maximum time step of the problem */
    inline double dtMax() const {return _dtMax;};
    /** Returns the reference time step of the multirate setup */
    inline double dtRef() const {return _dtRef;};
    /** Returns the global imbalance of the partitioned problem */
    inline double globalImbalance() const {return _globalImbalance;};
    /** Returns the theoretical best possible global imbalance of the partitioned problem */
    inline double bestImbalance() const {return _bestImbalance;};
    /** Returns the total of volume term operations during one large time step of the multirate algorithm */
    inline int nbVolumeTerms() const {return nb_elements;};
    /** Returns the total of intra-processors face term operations during one large time step of the multirate algorithm */
    inline int nbInterfaceTerms1() const {return nb_faces1;};
    /** Returns the total of inter-processors face term operations during one large time step of the multirate algorithm */
    inline int nbInterfaceTerms2() const {return nb_faces2;};
};

#endif
