#ifndef _DG_ERK_H_
#define _DG_ERK_H_

class dgConservationLaw;
class dgDofManager;
class dgLimiter;
class dgTransformNodalValue;
class dgAssemblerExplicit;

#include "dgDofContainer.h"

typedef enum {DG_ERK_EULER = 1, DG_ERK_22 = 2, DG_ERK_33 = 3, DG_ERK_44 = 4, DG_ERK_44_3_8 = 1000, DG_ERK_65_FEHLBERG = 1001, DG_ERK_43_SCHLEGEL = 10002} DG_ERK_TYPE;

class dgERK
{
 protected:
  dgAssemblerExplicit *_assembler;
  bool _ownAssembler;
  std::vector<dgDofContainer> _du;
  std::vector<double> _a, _b, _c;
  dgLimiter *_limiter;
  dgTransformNodalValue *_transformNodalValue;
  inline void setCoef(DG_ERK_TYPE order);
  dgDofContainer *_X;
  int _istep;
 public:
  /**if a limiter is set, it is applied after each RK stage */
  void setLimiter(dgLimiter *limiter) { _limiter = limiter; }
  /**if the Nodal values is transformed in first step of RK */
  void setTransformNodalValue(dgTransformNodalValue *TransformNodalValue) { _transformNodalValue = TransformNodalValue; }
  void iterate (dgDofContainer &solution, double dt, double t);
  virtual int subiterate(dgDofContainer &solution, double dt, double t);
  void finalize(dgDofContainer &solution);
  /** explicit runge-kutta temporal iterator : type can be DG_ERK_EULER=1 for a forward euler, DG_ERK_22=2, DG_ERK_33=3 and DG_ERK_44=4 for diagonal ERK,
   * DG_ERK_44_3_8 for  a fourth order runge-kutta 3/8 (see Hairer & Wanner book,
   * DG_ERK_43_SHLEGEL for a four stage third order runge-kutta (from Schlegel et al. Journal of Computational and Applied Mathematics, 2009),
   * DG_ERK_65_FEHLBERG for a six stage fifth order Runge-Kutta-Fehlberg.
   * The dofManager argument can be NULL (None in python), in this case a simple dg formulation with one single conservation law is assumed and no system is assembled
   * (this is faster than providing any dgDofManager).
   **/
  dgERK(dgConservationLaw &claw, dgDofManager *dof, std::vector<double> a, std::vector<double> b, std::vector<double> c);
  dgERK(dgConservationLaw &claw, dgDofManager *dof, DG_ERK_TYPE order);
  dgERK(dgAssemblerExplicit * assembler, DG_ERK_TYPE order);
  ~dgERK();
  /**Returns the inverse of the spectral radius of L(U).
   * Useful for computing stable explicit time step. */
  double computeInvSpectralRadius(dgDofContainer *solution);
  /** Returns the number of step in the rk method */
  int getNsteps(){return _b.size();};
  inline dgAssemblerExplicit * getAssembler() {return _assembler;}
};

#endif
