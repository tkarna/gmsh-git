#include "dgTransformNodalValue.h" 
#include "dgGroupOfElements.h"   
#include "dgDofContainer.h"
#include "function.h"

//----------------------------------------------------------------------------------   
static double n_supra = 20;

int dgSupraTransformNodalValue::apply ( dgDofContainer *solution)
{    
  
  
  fullVector<double> &sol = (*solution).getVector(); 
  for (int i=0;i< sol.size();i++){
   
     if (sol(i)<0) sol(i)=-pow(- sol(i), 1/n_supra);
       else sol(i)=pow( sol(i), 1/n_supra);
     }
  
  return 1;        
}

int dgSupraTransformNodalValue::apply_Inverse ( dgDofContainer *solution)
{    
  

  fullVector<double> &sol = (*solution).getVector(); 
  for (int i=0;i< sol.size();i++){
   
     if (sol(i)<0) sol(i)=-pow(- sol(i), n_supra);
       else sol(i)=pow( sol(i), n_supra);
     }
  
  return 1;
}
