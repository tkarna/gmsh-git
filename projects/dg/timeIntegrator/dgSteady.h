#ifndef _DG_STEADY_H_
#define _DG_STEADY_H_

class dgConservationLaw;
class dgDofManager;
class dgDofContainer;

#include "dgNewton.h"

/** documentation about this function can be found at https://braque.mema.ucl.ac.be/svn/private/papers/lambrechts/fastDG/newton.tex */
class dgSteady
{
  dgNewton _newton;
 public:
  dgSteady (dgConservationLaw &claw, dgDofManager &dof);
  void solve (dgDofContainer &solution);
  inline dgNewton &getNewton() {return _newton;}
};

#endif
