#ifndef _DG_NEWTON_H_
#define _DG_NEWTON_H_

class dgConservationLaw;
class dgDofManager;

#include "dgDofContainer.h"
#include "process.h"

/** Newton-Raphson method
  * Solve non-linear system of the shape
  *   Alpha M dU - F(U0 + dU, t) - R = 0
  * F being provided by a conservationLaw */
class dgNewton
{
 protected:
  dgDofManager &_dof;
  bool _assembleLHSMatrix;  // If false, uses old LHS matrix skipping it's assembly
  const dgConservationLaw &_law;
  dgDofContainer _currentSolution, _dU, _F, _residual;
  bool _returnZeroIfNotConverged;
  int _maxit, _verb;
  double _atol, _rtol;
  double firstNewtonResidual;
  int _converged; // false or 0: not converged ; num = nb iters to converge
  process *_preProc;
  dgNewton (dgDofManager &dof, const dgConservationLaw &law, bool fillSparsityPattern);
 public:
  virtual void assembleSystem (double alpha, const dgDofContainer *R, double t, dgDofContainer *variable = NULL);
  dgNewton (dgDofManager &dof, const dgConservationLaw &law);
  virtual const dgDofContainer &solve (const dgDofContainer *U0, double alpha, const dgDofContainer *R, double t, dgDofContainer *variable = NULL);

  inline void forceReassembleMatrix () {_assembleLHSMatrix = true;}
  inline void setMaxIt (double maxit)       {_maxit = maxit;}
  inline void setAssembleMatrix (bool flag) {_assembleLHSMatrix = flag;}
  inline void setAtol (double atol)         {_atol = atol;}
  inline void setRtol (double rtol)         {_rtol = rtol;}
  inline void setVerb (int verb)            {_verb = verb;}
  inline void setPreProc(process *preProc) { _preProc = preProc; }
  inline void returnZeroIfNotConverged(bool yn) { _returnZeroIfNotConverged = yn; }
  
  inline double getFirstResidual()    {return firstNewtonResidual;}
  inline const dgDofContainer &getF() {return _F;}
  inline int hasConverged() {return _converged;}
  inline dgDofManager &getDof() {return _dof;}
  inline const dgConservationLaw &getLaw() { return _law; }
  inline dgDofContainer* getCurrentSolution() { return &_currentSolution; }
  inline dgDofContainer* getCurrentRHS() { return &_dU; }
  virtual ~dgNewton(){};
};

#endif
