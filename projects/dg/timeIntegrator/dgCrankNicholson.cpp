#include "dgCrankNicholson.h"
#include "dgNewton.h"
#include "dgDofContainer.h"
#include "dgDofManager.h"
#include "dgConservationLaw.h"
#include "dgGroupOfElements.h"
#include "GmshConfig.h"
#include<sstream>
#ifdef HAVE_MPI
#include "mpi.h"
#endif

dgCrankNicholson::dgCrankNicholson (dgDofContainer &solution, dgConservationLaw &claw, dgDofManager &dof, double t) :
    _solution(&solution),
    _R(dof.getGroups(), claw.getNbFields()),
    _groups(dof.getGroups()),
    _newton(dof, claw),
      _law(claw),
    _t(t),
    _computeExplicitTerm(true)
{
  _law.computeAllTerms(_t, *_solution, _R);
}


void dgCrankNicholson::iterate (double t)
{
  double dt = t-_t;
  if (_computeExplicitTerm)
    _law.computeAllTerms(_t, *_solution, _R);
  _solution->axpy(_newton.solve(_solution, 2./dt, &_R, t, _solution),1.);
  _t = t;
  if (!_computeExplicitTerm)
    _R = _newton.getF();
}
