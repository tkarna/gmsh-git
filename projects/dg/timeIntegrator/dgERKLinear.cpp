#include "dgERKLinear.h"
#include "dgConservationLaw.h"
#include "dgTransformNodalValue.h"
#include "dgLimiter.h"
#include "GmshConfig.h"
#include<sstream>
#ifdef HAVE_MPI
#include "mpi.h"
#endif

dgERKLinear::dgERKLinear(dgConservationLaw &claw, dgGroupCollection *groups, DG_ERK_TYPE order) :
  dgERK(claw,NULL,order),
  _mat (claw.getNbFields(), groups),
  _matInvM (claw.getNbFields(), groups),
  _law(claw)
{
  _builtMatrix=false;
  _dof=dgDofManager::newDGBlock (groups,claw.getNbFields(),&_mat);
}

dgERKLinear::~dgERKLinear(){
  delete _dof;
}

void dgERKLinear::assembleSystem(double t){
  dgDofContainer currentSolution(_dof->getGroups(), _law.getNbFields());
  currentSolution.copy (*_X);
  dgDofContainer F(currentSolution);

  const dgGroupCollection &groups = _dof->getGroups();
  int nbFields = _law.getNbFields();
  
  //init iteration
  F.setAll(0.0);
  _law.fixStrongBC(*_dof, &currentSolution);
  
  currentSolution.scatterBegin(!_dof->isContinuous());
  _dof->zeroMatrix();
  //assemble LHS for volume term
  for (int iGroup = 0; iGroup < groups.getNbElementGroups(); iGroup++) {
    dgGroupOfElements &group = *groups.getElementGroup(iGroup);
    int nbNodes = group.getNbNodes();
    fullMatrix<double> JE(nbFields * nbNodes, nbFields * nbNodes);
    _law.computeElement(group, t, currentSolution, F, _dof, NULL);
  }
  currentSolution.scatterEnd();
  //assemble LHS for interface term
  for (int iGroup = 0; iGroup < groups.getNbFaceGroups (); iGroup++) {
    dgGroupOfFaces *group = groups.getFaceGroup(iGroup);
    const int nbConnections = group->nConnection();
    if(_dof->isContinuous() && nbConnections != 1) continue;
    _law.computeFace(*group, t, currentSolution, F, _dof, NULL);
  }

//  _matInvM=_mat;
//  _matInvM.setIdentity();
//  _matInvM.invMmult();
//  _matInvM.reduce(false,1e-10);

  _mat.invMmult();
    //_mat.setIdentity();
  _mat.reduce(false,1e-10);    

  
}

int dgERKLinear::subiterate(dgDofContainer &solution, double dt, double t) {
  int nsteps = _b.size();
  dgDofContainer buf(solution);
  buf.setAll(0);
  //initialize
  if(_istep==0){
    _X= new dgDofContainer(solution);
    if (dt <= 0)
      Msg::Fatal("Time step must be greater than zero");
    function::getDT()->set(dt);
    _du.resize(nsteps, *_X);
    if (_transformNodalValue) _transformNodalValue->apply(&solution);
    if (!_builtMatrix){
      assembleSystem (t);
      _builtMatrix=true;
    }
  }

  //subiterate
  _X->copy(solution);
  for (int j = 0; j < _istep; j ++)
    if (_a[_istep*nsteps+j] != 0.)
      _X->axpy(_du[j], _a[_istep*nsteps+j]);
  if (_limiter) _limiter->apply(_X);
  if (_transformNodalValue) _transformNodalValue->apply_Inverse(_X);
  function::getSubDT()->set(dt*_c[_istep]);
  _mat.mult(*_X,_du[_istep]);//buf);
  //_matInvM.mult(buf,_du[_istep]);
  _du[_istep].scale(-dt);
  return _istep;
}
