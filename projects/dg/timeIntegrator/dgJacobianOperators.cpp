#include "dgJacobianOperators.h"
#include "dgDofContainer.h"
#include "dgDofManager.h"
#include "dgConservationLaw.h"
#include "dgGroupOfElements.h"
#include "GmshConfig.h"
#include<sstream>
#ifdef HAVE_MPI
#include "mpi.h"
#endif

dgJacobianOperators::dgJacobianOperators(dgConservationLaw &claw, const dgGroupCollection &groups):
  _jac (claw.getNbFields(), &groups),
  _F (groups, claw.getNbFields()),
  _law(claw)
{
  _dof=dgDofManager::newDGBlock (&groups,claw.getNbFields(), &_jac);
}

dgJacobianOperators::~dgJacobianOperators(){
  delete _dof;
}

void dgJacobianOperators::computeTermAndJacobian(const dgDofContainer &U0, double t, bool needJacobian){
  dgDofContainer currentSolution(_dof->getGroups(), _law.getNbFields());
  currentSolution.copy (U0); 
  const dgGroupCollection &groups = _dof->getGroups();
  
  //init iteration
  _F.setAll(0.0);
  _law.fixStrongBC(*_dof, &currentSolution);

  if (needJacobian)
    _dof->zeroMatrix(); 
  currentSolution.scatterBegin(!_dof->isContinuous());
  //assemble LHS for volume term
  for (int iGroup = 0; iGroup < groups.getNbElementGroups(); iGroup++) {
    dgGroupOfElements &group = *groups.getElementGroup(iGroup);
    _law.computeElement(group, t, currentSolution, _F,  needJacobian ? _dof : NULL, NULL);
  }
  currentSolution.scatterEnd();
  //assemble LHS for interface term
  for (int iGroup = 0; iGroup < groups.getNbFaceGroups (); iGroup++) {
    dgGroupOfFaces *group = groups.getFaceGroup(iGroup);
    const int nbConnections = group->nConnection();
    if(_dof->isContinuous() && nbConnections != 1) continue;
    _law.computeFace(*group, t, currentSolution, _F, needJacobian ? _dof : NULL, NULL);
  }
  if (needJacobian) _jac.invMmult();
  _F.multiplyByInvMassMatrix();
}

