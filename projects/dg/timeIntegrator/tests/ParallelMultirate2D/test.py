from dgpy import *
from input import *
import os,sys


CCode = """
#include "fullMatrix.h"
#include "function.h"
extern "C" {
void wind (dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< sol.size1(); i++) {
    sol.set(i,0,sin(xyz(i,1)/1e6)/1e6);
    sol.set(i,1,0);
  }
}
void coriolis (dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< sol.size1(); i++) {
    sol.set(i,0,1e-4+2e-11*xyz(i,1));
  }
}
void extractEta (dataCacheMap *, fullMatrix<double> &eta, fullMatrix<double> &fullSol) {
  for (size_t i = 0; i< fullSol.size1(); i++) {
    eta.set(i,0,fullSol(i,0));
  }
}
}
"""

tmpLib = "./tmp.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CCode, tmpLib);
Msg.Barrier()


os.system("rm -r output")
os.mkdir(outputDir)

model = GModel()

m = GModel()
m.load("stommel.geo")
GmshSetOption('Mesh', 'CharacteristicLengthFactor',    clscale)
m.mesh(dimension)
m.save("stommel.msh")

model.load ('stommel.msh')

groups = dgGroupCollection(model, dimension, order)
claw = dgConservationLawShallowWater2d()
solution_ref = dgDofContainer(groups, claw.getNbFields())
solution = dgDofContainer(groups, claw.getNbFields())

error=0
nbp=[1,2,4]
for rkn in range (0,len(RK_TYPES)):
  diff = []
  for i in range(0, len(nbp)):
    solution.setAll(0.0)
    solution_ref.setAll(0.0)
    os.system('rundgpy PartitionStommel.py %d %d'%(rkn, nbp[i]))
    os.system('mpirun -np %d rundgpy ScalingMultirateStommel.py %d'%(nbp[i], rkn))
    solution_ref.readMsh(outputDir+"solution_export_%d_%d/solution_export_%d_%d.idx"%(rkn, 1, rkn, 1))
    solution.readMsh(outputDir+"solution_export_%d_%d/solution_export_%d_%d.idx"%(rkn, nbp[i], rkn, nbp[i]))
    solution.axpy(solution_ref, -1)
    diff.append(solution.norm())
    if solution.norm() > 0.00000000000000001:
      error = 1
  print (RK_TYPES[rkn])
  print ('-----------------------------------------------------------------------------------------------------')
  print (diff)
  print ('-----------------------------------------------------------------------------------------------------')

if(error == 0):
  print ("exit with succes :-)")
else:
  print ("exit with failure :-(")
  
sys.exit(error)









