from dgpy import *
from input import *
import sys

tmpLib = "./tmp.dylib"

model = GModel()
model.load (outputDir+'stommel_%d_partitioned%d.msh'%(int(sys.argv[1]), Msg.GetCommSize()))

groups = dgGroupCollection(model, dimension, order)
claw = dgConservationLawShallowWater2d()
solution = dgDofContainer(groups, claw.getNbFields())

XYZ = groups.getFunctionCoordinates();
f0 = functionC(tmpLib,"coriolis",1,[XYZ, functionSolution.get()])
claw.setCoriolisFactor(f0)
f1 = functionConstant(0)
claw.setQuadraticDissipation(f1)
claw.setIsLinear(1)
f2 = functionConstant([1e-6])
claw.setLinearDissipation(f2)
f3 = functionC(tmpLib,"wind",2,[XYZ])
claw.setSource(f3)
f4 = functionConstant([1000])
claw.setBathymetry(f4)
f5 = functionConstant([0,0,0])
claw.setBathymetryGradient(f5)

bndWall = claw.newBoundaryWall()
claw.addBoundaryCondition('Wall', bndWall)

rk = dgMultirateERK(groups, claw, RK_TYPES[int(sys.argv[1])])
dt=rk.splitGroupsForMultirate(mL,   solution, [solution])
t=0
Msg.Barrier()
for i in range (1,nbSteps+1) :
  norm = rk.iterate (solution, dt, t)
  t = t +dt
Msg.Barrier()
solutionName=outputDir+"solution_export_%d_%d"%(int(sys.argv[1]),  Msg.GetCommSize())
Msg.Barrier()
solutionExporter = dgIdxExporter(solution,  solutionName)
Msg.Barrier()
solutionExporter.exportIdx()
Msg.Barrier()

Msg.Exit(0)

