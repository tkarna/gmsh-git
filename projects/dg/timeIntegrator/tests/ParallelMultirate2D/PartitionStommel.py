from dgpy import *
from input import *
import sys

model = GModel()
model.load ('stommel.msh')

pOpt = meshPartitionOptions()
pOpt.setNumOfPartitions(int(sys.argv[2]))

tmpLib = "./tmp.dylib"

groups = dgGroupCollection(model, dimension, order)
claw = dgConservationLawShallowWater2d()
solution = dgDofContainer(groups, claw.getNbFields())

XYZ = groups.getFunctionCoordinates();
f0 = functionC(tmpLib,"coriolis",1,[XYZ, functionSolution.get()])
claw.setCoriolisFactor(f0)
f1 = functionConstant(0)
claw.setQuadraticDissipation(f1)
claw.setIsLinear(1)
f2 = functionConstant([1e-6])
claw.setLinearDissipation(f2)
f3 = functionC(tmpLib,"wind",2,[XYZ])
claw.setSource(f3)
f4 = functionConstant([1000])
claw.setBathymetry(f4)
f5 = functionConstant([0,0,0])
claw.setBathymetryGradient(f5)

bndWall = claw.newBoundaryWall()
claw.addBoundaryCondition('Wall', bndWall)

rk = dgMultirateERK(groups, claw, RK_TYPES[int(sys.argv[1])])
dt=rk.splitGroupsForMultirate(mL,   solution, [solution])
rk.printMultirateInfo()

rk.computeMultiConstrainedPartition(pOpt,  algo)
PartitionMesh(model, pOpt)
rk.updatePartitionInfo(int(sys.argv[2]))
rk.printPartitionInfo()
model.save(outputDir+'stommel_%d_partitioned%d.msh'%(int(sys.argv[1]), int(sys.argv[2])))
Msg.Exit(0)

