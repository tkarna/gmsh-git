from dgpy import *

order = 1
dimension = 2
clscale=0.25

mL = 10
RK_TYPES = [ERK_22_C,  ERK_33_C,  ERK_43_C,  ERK_44_C, ERK_22_S, ERK_22_S_C,  ERK_43_S, ERK_43_S_C]
algo=5
nbSteps=10
outputDir="output/"

