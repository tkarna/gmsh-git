from dgpy import *
from termcolor import colored
import time
import sys, os
import math


if(Msg.GetCommRank() == 0):
  functionC.buildLibraryFromFile ("TURBOFAN.cc",  "lib_turbofan.so");
Msg.Barrier()

tlib = "./lib_turbofan.so";

order = 1
dimension = 3
clscale = 2

os.system("gmsh -3 turbofan.geo")
model = GModel()
#m = GModel()
#m.load("turbofan.geo")
#m.setOrderN(order, False, False)
#GmshSetOption('Mesh', 'CharacteristicLengthFactor',    clscale)
#m.mesh(dimension)
#OptimizeMesh(m)
#m.save("turbofan.msh")
model.load ('turbofan.msh')

RK_TYPES = [ERK_22_C, ERK_33_C, ERK_43_C, ERK_44_C, ERK_43_S, ERK_43_S_C]
cOrders = [2, 2, 2, 2, 3, 3]
n = 5
mL=10
error = 0
for k in range (0, len(RK_TYPES)):

  groups = dgGroupCollection(model, dimension, order)

  claw=dgConservationLawWave(dimension)
  cValue = 1.
  rhoValue = 1.
  sigmaValue = 0.
  coef = functionConstant([cValue, rhoValue, sigmaValue])
  claw.setPhysCoef('', coef)

  solution = dgDofContainer(groups, claw.getNbFields())
  solution_ref = dgDofContainer(groups, claw.getNbFields())  

  solution.setAll(0.0)
  solution_ref.setAll(0.0)
  
  rk_ref = dgERK(claw,  None,  DG_ERK_44_3_8)

  RKTYPE = RK_TYPES[k]
  cOrder = cOrders[k]
  rk = dgMultirateERK(groups, claw, RKTYPE)
  dt = rk.splitGroupsForMultirate(mL,   solution, [solution, solution_ref])
  
  rk.printMultirateInfo()
  
  coeff = functionConstant([10.,16.,18.063,-7.7882e-08,0.,-15.04266]) # No flow, omega = 10, m = 16, alpha = 18.063, MOverN = -7.7882e-08, mu = (0,-15.0427)
  xyz = function.getCoordinates()
  time = function.getTime()
  ISAmp = functionC(tlib, "inletStreamAmp", 8, [xyz, coeff])
  amp = functionPrecomputed(groups, 2*order+1, 8)
  for i in range(groups.getNbFaceGroups()) :
    group = groups.getFaceGroup(i)
    if (group.physicalTag() == 'Fan') :
      amp.compute(ISAmp,group)
  IS = functionC(tlib, "inletStream", 4, [time, amp, coeff])
  InletBoundary = claw.newBoundaryOpenOutflowWithInflow("Fan", IS)
  OutletBoundary = claw.newBoundaryOpenOutflow("Intake")
  InteriorBoundary1 = claw.newBoundaryWall("Spinner")
  InteriorBoundary2 = claw.newBoundaryWall("Duct")
  
  claw.addBoundaryCondition('Fan',InletBoundary)
  claw.addBoundaryCondition('Intake',OutletBoundary)
  claw.addBoundaryCondition('Spinner',InteriorBoundary1)
  claw.addBoundaryCondition('Duct',InteriorBoundary2)
  dt = 2 * dt
  Ti = 0
  Tf = dt
  dt_ref = dt / pow(2, n + 1)
  nbSteps_ref = int(math.ceil((Tf - Ti) / dt_ref))

  t = Ti
  for i in range (0, nbSteps_ref):
    if(i == nbSteps_ref - 1):
      dt_ref = Tf - t
    rk_ref.iterate(solution_ref, dt_ref, t)
    t = t + dt_ref

  conv = []
  for j in range(0, n):
    solution.setAll(0.0)
    t = Ti
    dtN = dt / pow(2, j)
    nbSteps = int(math.ceil((Tf - Ti) / dtN))
    for i in range(0, nbSteps):
      if(i == nbSteps - 1):
        dtN = Tf - t
      rk.iterate(solution, dtN, t)
      t = t + dtN
    solution.axpy(solution_ref, -1)
    conv.append(solution.norm())

  old = conv[0]
  for c in range(0, n):
    if(conv[0] / conv[c] < (2**(cOrder*c)) * 0.9):
      error = 1
      print(colored("%.2f - %.2e (%.2f %%)", "red") %(math.log(old/conv[c],  2), conv[c], conv[0] / (conv[c] * 2**(cOrder*c))*100))
    else:
      print(colored("%.2f - %.2e (%.2f %%)", "green") %(math.log(old/conv[c],  2), conv[c], conv[0] / (conv[c] * 2**(cOrder*c))*100))
    old = conv[c]
  print ('')
   

if(error == 0):
  print ("exit with succes :-)")
else:
  print ("exit with failure :-(")
  
sys.exit(error)
Msg.Exit(0)
