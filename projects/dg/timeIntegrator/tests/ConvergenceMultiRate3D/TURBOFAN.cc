
/*
 * =====================================================================================
 *
 *       Filename:  TURBOFAN.cc
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  22.12.2010 10:25:38
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  bseny (BS), bruno.seny@uclouvain.be
 *        Company:  UCL
 *
 * =====================================================================================
 */

#include "fullMatrix.h"
#include "function.h"
#include "dgGroupOfElements.h"
#include "polynomialBasis.h"
#include <cmath>
#include <complex>

extern "C" {


  void inletStreamAmp(dataCacheMap *dCM, fullMatrix<double> &f, fullMatrix<double> &xyz, fullMatrix<double> &Coeff)
  {

    const double omega = Coeff(0, 0);
    const int m = (int)Coeff(0, 1);
    const double alpha = Coeff(0, 2), MOverN = Coeff(0, 3);
    const std::complex<double> mu = std::complex<double>(Coeff(0, 4), Coeff(0, 5));

    typedef fullMatrix<std::complex<double> > fullMatComp;

    for (size_t i = 0; i < f.size1(); i++) {
      double &x = xyz(i, 0);
      double &y = xyz(i, 1);
      double &z = xyz(i, 2);
      double theta = atan2(z, y);
      double r = sqrt(y*y+z*z);
      std::complex<double> pAmp, UAxAmp;
      if (alpha == 0.) {                                            // Pure spin (radially uniform) if alpha = 0
        pAmp = pow(M_E, std::complex<double>(0.,m*theta));
        UAxAmp = pAmp;
      }
      else {
        pAmp = (jn(m,alpha*r)+MOverN*yn(m,alpha*r))*pow(M_E, std::complex<double>(0.,m*theta));
        UAxAmp = mu*pAmp/omega;
      }
      f(i, 0) = pAmp.real();
      f(i, 1) = pAmp.imag();
      f(i, 2) = UAxAmp.real();
      f(i, 3) = UAxAmp.imag();
      if(r == 0.){
        f(i, 4) = 0.;
        f(i, 5) = 0.;
        f(i, 6) = 0.;
        f(i, 7) = 0.;
      }
      else {
        std::complex<double> UTheta = m/(r*omega)*pAmp;
        f(i, 4) = -sin(theta)*UTheta.real();
        f(i, 5) = -sin(theta)*UTheta.imag();
        f(i, 6) = cos(theta)*UTheta.real();
        f(i, 7) = cos(theta)*UTheta.imag();
      }
    }

  }


  void inletStream(dataCacheMap *dCM, fullMatrix<double> &f, fullMatrix<double> &time, fullMatrix<double> &Amp, fullMatrix<double> &Coeff)
  {

    const double omega = Coeff(0, 0);

    typedef fullMatrix<std::complex<double> > fullMatComp;

    const double cosMOT = cos(-omega*time(0, 0)), sinMOT = sin(-omega*time(0, 0));

    for (size_t i = 0; i < f.size1(); i++) {
      f(i, 0) = Amp(i, 0)*cosMOT-Amp(i, 1)*sinMOT;
      f(i, 1) = Amp(i, 2)*cosMOT-Amp(i, 3)*sinMOT;
      f(i, 2) = Amp(i, 4)*cosMOT-Amp(i, 5)*sinMOT;
      f(i, 3) = Amp(i, 6)*cosMOT-Amp(i, 7)*sinMOT;
    }

  }

}
