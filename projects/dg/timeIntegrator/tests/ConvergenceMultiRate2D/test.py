from dgpy import *
from math import *
from termcolor import colored
import sys 

order = 1
dimension = 2

model = GModel()
m = GModel()
m.load("stommel.geo")
m.setOrderN(order, False, False)
m.mesh(dimension)
m.save("stommel.msh")
model.load ('stommel.msh')

CCode = """
#include "fullMatrix.h"
#include "function.h"
extern "C" {
void wind (dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< sol.size1(); i++) {
    sol.set(i,0,sin(xyz(i,1)/1e6)/1e6);
    sol.set(i,1,0);
  }
}
void coriolis (dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< sol.size1(); i++) {
    sol.set(i,0,1e-4+2e-11*xyz(i,1));
  }
}
void extractEta (dataCacheMap *, fullMatrix<double> &eta, fullMatrix<double> &fullSol) {
  for (size_t i = 0; i< fullSol.size1(); i++) {
    eta.set(i,0,fullSol(i,0));
  }
}
}
"""

tmpLib = "./tmp.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CCode, tmpLib);
Msg.Barrier()




RK_TYPES = [ERK_22_C, ERK_33_C, ERK_43_C, ERK_44_C, ERK_43_S, ERK_43_S_C]
cOrders = [2, 2, 2, 2, 3, 3]
n = 6
error = 0
for k in range (0, len(RK_TYPES)):

  groups = dgGroupCollection(model, dimension, order)
  claw = dgConservationLawShallowWater2d()
  solution = dgDofContainer(groups, claw.getNbFields())
  solution_ref = dgDofContainer(groups, claw.getNbFields())
  
  XYZ = groups.getFunctionCoordinates();
  f0 = functionC(tmpLib,"coriolis",1,[XYZ, functionSolution.get()])
  claw.setCoriolisFactor(f0)
  f1 = functionConstant(0)
  claw.setQuadraticDissipation(f1)
  claw.setIsLinear(1)
  f2 = functionConstant([1e-6])
  claw.setLinearDissipation(f2)
  f3 = functionC(tmpLib,"wind",2,[XYZ])
  claw.setSource(f3)
  f4 = functionConstant([1000])
  claw.setBathymetry(f4)
  f5 = functionConstant([0,0,0])
  claw.setBathymetryGradient(f5)
  
  bndWall = claw.newBoundaryWall()
  claw.addBoundaryCondition('Wall', bndWall)

  rk_ref = dgERK(claw,  None,  DG_ERK_44_3_8)

  RKTYPE = RK_TYPES[k]
  cOrder = cOrders[k]
  rk = dgMultirateERK(groups, claw, RKTYPE)
  solution.setAll(0.0)
  solution_ref.setAll(0.0)
  dt = rk.splitGroupsForMultirate(1000,   solution, [solution, solution_ref])
  dt = 2 * dt

  Ti = 0
  Tf = dt
  dt_ref = dt / pow(2, n + 1)
  nbSteps_ref = int(ceil((Tf - Ti) / dt_ref))

  t = Ti
  for i in range (0, nbSteps_ref):
    if(i == nbSteps_ref - 1):
      dt_ref = Tf - t
    rk_ref.iterate(solution_ref, dt_ref, t)
    t = t + dt_ref

  conv = []
  for j in range(0, n):
    solution.setAll(0.0)
    t = Ti
    dtN = dt / pow(2, j)
    nbSteps = int(ceil((Tf - Ti) / dtN))
    for i in range(0, nbSteps):
      if(i == nbSteps - 1):
        dtN = Tf - t
      rk.iterate(solution, dtN, t)
      t = t + dtN
    solution.axpy(solution_ref, -1)
    conv.append(solution.norm())

  old = conv[0]
  for c in range(0, n):
    if(conv[0] / conv[c] < (cOrder**(2*c)) * 0.9):
      error = 1
      print(colored("%.2f - %.2e (%.2f %%)", "red") %(log(old/conv[c],  2), conv[c], conv[0] / (conv[c] * cOrder**(2*c))*100))
    else:
      print(colored("%.2f - %.2e (%.2f %%)", "green") %(log(old/conv[c],  2), conv[c], conv[0] / (conv[c] * cOrder**(2*c))*100))
    old = conv[c]
  print ('')
   

if(error == 0):
  print ("exit with succes :-)")
else:
  print ("exit with failure :-(")
  
sys.exit(error)
