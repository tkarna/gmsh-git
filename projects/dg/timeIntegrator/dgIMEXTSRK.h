#ifndef _DG_IMEX_TSRK_H_
#define _DG_IMEX_TSRK_H_

class dgConservationLaw;
class dgDofManager;
class dgNewton;
class dgAssemblerExplicit;

#include "dgDofContainer.h"

typedef enum {IMEX_TSRK_23 = 1, IMEX_TSRK_34 = 2, IMEX_TSRK_56 = 3, IMEX_TSRK_ev4 = 4} IMEX_TSRK_TYPE;
/** Two-Step Runge Kutta iterator **/
class dgIMEXTSRK
{
 private:
  double _dt;
  dgConservationLaw *_claw;
  dgDofManager *_dofIm;
  dgAssemblerExplicit *_assembler;
  bool ownAssembler;
  dgNewton *_newton;
  bool ownNewton;
  bool initialized;
  std::vector<dgDofContainer> _KIm;
  std::vector<dgDofContainer> _KEx;
//  std::vector<dgDofContainer> _L;
  std::vector<dgDofContainer> _KImOld;
  std::vector<dgDofContainer> _KExOld;
  dgDofContainer _buf;
  dgDofContainer _X;
  dgDofContainer _L;
  double _theta;
  std::vector<double> _aIm, _bIm,_aEx, _bEx, _c, _u, _v, _w, _vEx, _vIm;
  inline void setCoef(IMEX_TSRK_TYPE type);
 public:
  double starter(dgDofContainer &solution, double dt, double t, int order);
  dgIMEXTSRK(dgConservationLaw &claw, dgDofManager &dofIm, IMEX_TSRK_TYPE type, dgDofManager *dofEx=NULL);
  int iterate(dgDofContainer &solution, double dt, double t);
  inline dgNewton &getNewton() {return *_newton;}
  inline dgAssemblerExplicit * getAssembler() {return _assembler;}
};

#endif
