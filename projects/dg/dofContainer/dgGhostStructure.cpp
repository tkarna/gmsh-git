#include "dgGhostStructure.h"
#include "GmshMessage.h"
#include "GmshConfig.h"
#ifdef HAVE_MPI
#include "mpi.h"

template<class dataType> // void including mpi.h in the header
class dgGhostCommunication<dataType>::MPIRequestVector: public std::vector<MPI_Request> {
  public:
  MPIRequestVector(int size, MPI_Request req) : std::vector<MPI_Request>(size, req){};
};


//the unusual _ are there to avoid symbol collision with internals symbols of ompenmpi
template<class dataType>
static MPI_Datatype _getMPIType_();

template<>
MPI_Datatype _getMPIType_<int>() {
  return MPI_INT;
}

template<>
MPI_Datatype _getMPIType_<double>() {
  return MPI_DOUBLE;
}

#endif

template<class dataType>
dgGhostCommunication<dataType>::dgGhostCommunication (
  const std::map<int, std::vector<int> > &sendIdx,
  const std::map<int, std::vector<int> > &recvIdx,
  operation op,
  int nbFields,
  std::vector<dataType> &values):
  _sendIdx (sendIdx),
  _recvIdx (recvIdx),
  _operation(op),
  _nbFields (nbFields),
  _values (values),
  _inProgress (false)
{}

template<class dataType>
void dgGhostCommunication<dataType>::begin()
{ 
  if (_inProgress) {
    Msg::Error("previous communication not completed, cannot start a new one.");
    return;
  }
  _inProgress = true;
  #ifdef HAVE_MPI
  _reqRecv = new MPIRequestVector(Msg::GetCommSize(), MPI_REQUEST_NULL);
  _reqSend = new MPIRequestVector(Msg::GetCommSize(), MPI_REQUEST_NULL);
  for (std::map<int, std::vector<int> >::iterator it = _sendIdx.begin(); it != _sendIdx.end(); ++it) {
    int rank = it->first;
    std::vector<int> &ids = it->second;
    std::vector<dataType> &rankReceived = _receivedValues[rank];
    rankReceived.resize(ids.size() * _nbFields);
    MPI_Irecv(&rankReceived[0], rankReceived.size(), _getMPIType_<dataType>(), rank, 2, MPI_COMM_WORLD, &(*_reqRecv)[rank]);
  }
  for (std::map<int, std::vector<int> >::iterator it = _recvIdx.begin(); it != _recvIdx.end(); ++it) {
    int rank = it->first;
    std::vector<int> &ids = it->second;
    std::vector<dataType> &rankSend = _sendValues[rank];
    rankSend.resize(ids.size() * _nbFields);
    for (size_t i = 0; i < ids.size(); i++)
      for (int j = 0; j < _nbFields; j++)
        rankSend[i * _nbFields + j] = _values[ids[i] * _nbFields + j];
    MPI_Isend(&rankSend[0], rankSend.size(), _getMPIType_<dataType>(), rank, 2, MPI_COMM_WORLD, &(*_reqSend)[rank]);
  }
  #endif
}

template<class dataType>
void dgGhostCommunication<dataType>::end()
{
  if (!_inProgress){
    Msg::Error("no communication in progress");
    return;
  }
  #ifdef HAVE_MPI
  MPI_Waitall(Msg::GetCommSize(), &(*_reqRecv)[0], MPI_STATUSES_IGNORE);
  std::map<int, std::vector<int> >::iterator itSendIdx = _sendIdx.begin();
  for (typename std::map<int, std::vector<dataType> >::iterator it = _receivedValues.begin(); it != _receivedValues.end(); ++it) {
    switch (_operation) {
      case MAX :
        for (size_t i = 0; i < itSendIdx->second.size(); i++)
          for (int j = 0; j < _nbFields; j++){
            dataType &v = _values[itSendIdx->second[i] * _nbFields + j];
            v = std::max(v, it->second[i * _nbFields + j]);
          }
        break;
      case MIN :
        for (size_t i = 0; i < itSendIdx->second.size(); i++)
          for (int j = 0; j < _nbFields; j++){
            dataType &v = _values[itSendIdx->second[i] * _nbFields + j];
            v = std::min(v, it->second[i * _nbFields + j]);
          }
        break;
      case SUM :
        for (size_t i = 0; i < itSendIdx->second.size(); i++)
          for (int j = 0; j < _nbFields; j++)
            _values[itSendIdx->second[i] * _nbFields + j] += it->second[i * _nbFields + j];
        break;
      case ASSIGN :
        for (size_t i = 0; i < itSendIdx->second.size(); i++)
          for (int j = 0; j < _nbFields; j++)
            _values[itSendIdx->second[i] * _nbFields + j] = it->second[i * _nbFields + j];
        break;
    }
    ++ itSendIdx;
  }
  MPI_Waitall(Msg::GetCommSize(), &(*_reqSend)[0], MPI_STATUSES_IGNORE);
  _inProgress = false;
  delete _reqRecv;
  delete _reqSend;
  #endif
}

dgGhostStructure::dgGhostStructure(std::map<int, std::vector<int> >&scatterSendGlobal, std::map<int, int> &global2LocalId)
{
  #ifdef HAVE_MPI
  _scatterSend = scatterSendGlobal;
  // build _scatterReceive based by on _scatterSend by inversing _scatterSend
  std::vector<int> nScatterFrom(Msg::GetCommSize()), nScatterTo(Msg::GetCommSize());
  for (std::map<int, std::vector<int> >::iterator it = _scatterSend.begin(); it != _scatterSend.end(); ++it) {
    nScatterFrom[it->first] = it->second.size();
  }
  MPI_Alltoall(&nScatterFrom[0], 1, MPI_INT, &nScatterTo[0], 1, MPI_INT, MPI_COMM_WORLD);
  std::vector<MPI_Request> reqRecv(Msg::GetCommSize(), MPI_REQUEST_NULL), reqSend(Msg::GetCommSize(), MPI_REQUEST_NULL);
  for (int i = 0; i<Msg::GetCommSize(); i++) {
    if (nScatterTo[i] == 0)
      continue;
    _scatterReceive[i].resize(nScatterTo[i]);
    MPI_Irecv(&_scatterReceive[i][0], nScatterTo[i], MPI_INT, i, 2, MPI_COMM_WORLD, &reqRecv[i]);
  }
  for (std::map<int, std::vector<int> >::iterator it = _scatterSend.begin(); it != _scatterSend.end(); ++it) {
    int rank = it->first;
    MPI_Isend(&it->second[0], it->second.size(), MPI_INT, rank, 2, MPI_COMM_WORLD, &reqSend[rank]);
  }
  MPI_Waitall(Msg::GetCommSize(), &reqRecv[0], MPI_STATUSES_IGNORE);
  MPI_Waitall(Msg::GetCommSize(), &reqSend[0], MPI_STATUSES_IGNORE);

  // convert _scatterReceive and _scatterSend to local numbering
  for(std::map<int, std::vector<int> >::iterator it = _scatterSend.begin(); it != _scatterSend.end(); it++) {
    std::vector<int> &ids = it->second;
    for (size_t i = 0; i < ids.size(); ++i) {
      ids[i] = global2LocalId[ids[i]];
    }
  }
  for(std::map<int, std::vector<int> >::iterator it = _scatterReceive.begin(); it != _scatterReceive.end(); it++) {
    std::vector<int> &ids = it->second;
    for (size_t i = 0; i < ids.size(); ++i) {
      ids[i] = global2LocalId[ids[i]];
    }
  }
  #endif
}

template<class dataType>
dgGhostCommunication<dataType> *dgGhostStructure::newCommunication(operation op, std::vector<dataType> &values, int nbFields)
{
  switch (op)
  {
    case SCATTER :
      return new dgGhostCommunication<dataType>(_scatterSend, _scatterReceive, dgGhostCommunication<dataType>::ASSIGN, nbFields, values);
    case GATHER_SUM :
      return new dgGhostCommunication<dataType>(_scatterReceive, _scatterSend, dgGhostCommunication<dataType>::SUM, nbFields, values);
    case GATHER_MAX :
      return new dgGhostCommunication<dataType>(_scatterReceive, _scatterSend, dgGhostCommunication<dataType>::MAX, nbFields, values);
    case GATHER_MIN :
      return new dgGhostCommunication<dataType>(_scatterReceive, _scatterSend, dgGhostCommunication<dataType>::MIN, nbFields, values);    
  }
  return NULL;
}

template<class dataType>
dgGhostCommunication<dataType>::~dgGhostCommunication()
{
  if (_inProgress)
    end();
}

template<class dataType>
void dgGhostStructure::communicate(operation op, std::vector<dataType> &values, int nbFields)
{
  dgGhostCommunication<dataType> *comm = newCommunication<dataType>(op, values, nbFields);
  comm->begin();
  comm->end();
  delete comm;
}

//specialization for int and double
template class dgGhostCommunication<int>;
template class dgGhostCommunication<double>;
template void dgGhostStructure::communicate<int>(operation op, std::vector<int> &values, int nbFields);
template void dgGhostStructure::communicate<double>(operation op, std::vector<double> &values, int nbFields);
template dgGhostCommunication<int> *dgGhostStructure::newCommunication<int>(operation op, std::vector<int> &values, int nbFields);
template dgGhostCommunication<double> *dgGhostStructure::newCommunication<double>(operation op, std::vector<double> &values, int nbFields);

