#include "GmshConfig.h"
#include "dgDofContainer.h"
#include "function.h"
#include "dgGroupOfElements.h"
#include "dgIntegrationMatrices.h"
#include "dofManager.h"
#if defined (WIN32)&& !defined(__CYGWIN__)
  #include <io.h>
#endif
#include <limits.h>
#include <limits>
#include <sys/stat.h>
#include "OS.h"
#include "GModel.h"
#ifdef HAVE_MPI
#include "mpi.h"
#else
#include "string.h"
#endif
#include <sstream>
#include "MElement.h"
#include <fstream>
#include <iomanip>
#include "dgDofManager.h"
#include "Numeric.h"
#include "dgSpaceTransform.h"
#include "dgMesh.h"
#include "dgMeshJacobian.h"
#include "dgElementVector.h"
#include "dgInterfaceVector.h"
#include "BasisFactory.h"
#include "dgExtrusion.h"

class dgDofContainer::parallelStructure {
  public:
  class commChannel {
    public:
    int rank;
    std::vector<double> sendBuf, recvBuf;
    #ifdef HAVE_MPI
      MPI_Request sendRequest, recvRequest;
      MPI_Datatype sendType, recvType;
    #endif
    
    commChannel(int rank_, int nbSend, std::vector<int> &sendDispl, std::vector<int> &sendBLen, int nbRecv, std::vector<int> &recvDispl, std::vector<int> &recvBLen) {
      rank = rank_;
      sendBuf.resize(nbSend);
      recvBuf.resize(nbRecv);
    #ifdef HAVE_MPI
      if (!sendBLen.empty()) {
        MPI_Type_indexed(sendBLen.size(), sendBLen.data(), sendDispl.data(), MPI_DOUBLE, &sendType);
        MPI_Type_commit( &sendType );
      }
      if (!recvBLen.empty()) {
        MPI_Type_indexed(recvBLen.size(), recvBLen.data(), recvDispl.data(), MPI_DOUBLE, &recvType);
        MPI_Type_commit( &recvType );
      }
    #endif
    }
    void irecv(double* dataPtr, int tag)
    {
    #ifdef HAVE_MPI
      if (!recvBuf.empty())
        MPI_Irecv(dataPtr, 1, recvType, rank, tag, MPI_COMM_WORLD, &recvRequest);
    #endif
    }
    void isend(double* dataPtr, int tag)
    {
    #ifdef HAVE_MPI
      if (!sendBuf.empty())
        MPI_Isend(dataPtr, 1, sendType, rank, tag, MPI_COMM_WORLD, &sendRequest);
    #endif
    }
    void waitRecv()
    {
    #ifdef HAVE_MPI
      if (!recvBuf.empty())
        MPI_Wait(&recvRequest, MPI_STATUS_IGNORE);
    #endif
    }
    int testRecv()
    {
    #ifdef HAVE_MPI
      int flag=0;
      if (!recvBuf.empty())
        MPI_Test(&recvRequest, &flag, MPI_STATUS_IGNORE);
      return flag;
    #endif
    }
    void waitSend()
    {
    #ifdef HAVE_MPI
      if (!sendBuf.empty())
        MPI_Wait(&sendRequest, MPI_STATUS_IGNORE);
    #endif
    }
  };
  int _maxTag;
  int _minTag;
  bool _scattering;
  bool _isDGGhost;
  std::vector<commChannel> channels;
  dgDofContainer *_dof;

  parallelStructure(dgDofContainer *dof, bool isDGGhost, int maxTag = -1, int minTag = 0)
  {
    _scattering=false;
    _maxTag = maxTag;
    _minTag = minTag;
    _dof = dof;
    _isDGGhost = isDGGhost;
    const dgGroupCollection *groups = dof->getGroups();
    int nbFields = dof->getNbFields();
    std::vector<size_t> nbSend(Msg::GetCommSize()), nbRecv(Msg::GetCommSize());
    if(groups->getModel()->getMeshPartitions().size() != (size_t) Msg::GetCommSize() && Msg::GetCommSize()!=1)
      Msg::Warning("Building parallel structure on %d nodes for mesh with %d partitions",
          Msg::GetCommSize(), groups->getModel()->getMeshPartitions().size());
    for(int i=0;i<Msg::GetCommSize();i++) {
      nbSend[i] = 0;
      std::vector<int> sendDispl, sendBlockLen;
      for(size_t j = 0; j < (size_t) groups->getNbImageElementsOnPartition(i, !_isDGGhost); j++){
        int iG = groups->getImageElementGroup(i,j, !_isDGGhost);
        dgGroupOfElements *group= groups->getElementGroup(iG);
        int iE = groups->getImageElementPositionInGroup(i, j, !_isDGGhost);
        int index = _dof->getDofId(iG, iE, 0, 0);
        int nBlock = nbFields*group->getNbNodes();
        if (_maxTag == -1 || (_maxTag >= group->getMultirateTag() && group->getMultirateTag() >= _minTag)) {
          nbSend[i] += nBlock;
          sendDispl.push_back(index);
          sendBlockLen.push_back(nBlock);
        }
      }
      nbRecv[i] = 0;
      std::vector<int> recvDispl, recvBlockLen;
      for(size_t j=0;j < (size_t) groups->getNbGhostElementsFromPartition(i, !_isDGGhost); j++){
        int iG = groups->getGhostElementGroup(i,j, !_isDGGhost);
        dgGroupOfElements *group= groups->getElementGroup(iG);
        int iE = groups->getGhostElementPositionInGroup(i, j, !_isDGGhost);
        int nBlock = nbFields*group->getNbNodes();
        int index = _dof->getDofId(iG, iE, 0, 0) - _dof->getVector().size();
        if (_maxTag == -1 || (_maxTag >= group->getMultirateTag() && group->getMultirateTag() >= _minTag)) {
          nbRecv[i] += nBlock;
          recvDispl.push_back(index);
          recvBlockLen.push_back(nBlock);
        }
      }
      if (nbSend[i] + nbRecv[i] > 0) {
        channels.push_back(commChannel(i, nbSend[i], sendDispl, sendBlockLen ,nbRecv[i], recvDispl, recvBlockLen));
      }
    }
  }
  void scatterBegin(int tag)
  {
    if (_scattering)
      Msg::Fatal("A scatter is already in progress on this dgDofContainer");
    _scattering=true;
    for (std::vector<commChannel>::iterator it = channels.begin(); it != channels.end(); ++it)
      (*it).irecv(&_dof->getGhostVector()(0), tag);
    for (std::vector<commChannel>::iterator it = channels.begin(); it != channels.end(); ++it)
      (*it).isend(&_dof->getVector()(0), tag);
  }
  void scatterEnd()
  {
    if (!_scattering)
      Msg::Fatal("No scatter is in progress on this dgDofContainer");
    for (std::vector<commChannel>::iterator it = channels.begin(); it != channels.end(); ++it)
      (*it).waitRecv();
    for (std::vector<commChannel>::iterator it = channels.begin(); it != channels.end(); ++it)
      (*it).waitSend();
    _scattering=false;
  }
};

dgDofFunction::dgDofFunction(dgDofContainer *dof):function(dof->getNbFields()), _dof(dof){}

dgDofFunction::dgDofFunction():function(0), _dof(NULL){}
void dgDofFunction::setDof(dgDofContainer *dof) {
  _dof = dof;
  _nbCol = dof->getNbFields();
}


static void get3dNodalValuesFrom2dDof(const dgExtrusion &extrusion, const dgDofContainer &dof, size_t groupId, size_t elId, fullMatrix<double> &nodalV)
{
  size_t elId2d, groupId2d;
  extrusion.mapElementID2Element2dID(groupId, elId, groupId2d, elId2d);
  const dgGroupOfElements &group3d = *extrusion.getGroups3d().getElementGroup(groupId);
  for (int i = 0; i < group3d.getNbNodes(); ++i) {
    size_t nodeId2d;
    extrusion.map3d2d(groupId, elId, i, groupId2d, elId2d, nodeId2d);
    const dgFullMatrix<double> &groupProxy = dof.getGroupProxy(groupId2d);
    for (int j = 0; j < dof.getNbFields(); ++j)
      nodalV(i, j) = groupProxy(nodeId2d, elId2d * dof.getNbFields() + j);
  }
}

static dgFullMatrix<double> get3dNodalValuesFrom2dDof(const dgExtrusion &extrusion, const dgDofContainer &dof, const dgGroupOfElements &group)
{
  dgFullMatrix<double> v(group.getNbNodes(), dof.getNbFields(), group.getNbElements(), false);
  fullMatrix<double> dof3del;
  int iGroup = extrusion.getGroups3d().getElementGroupId(&group);
  for (size_t iEl = 0; iEl < group.getNbElements(); ++iEl) {
    v.getBlockProxy(iEl, dof3del);
    get3dNodalValuesFrom2dDof(extrusion, dof, iGroup, iEl, dof3del);
  }
  return v;
}

static void get3dNodalValuesFrom2dDofFace(const dgExtrusion &extrusion, const dgDofContainer &dof, size_t groupId, size_t elId, const std::vector<int> &closure, fullMatrix<double> &nodalV)
{
  size_t elId2d, groupId2d;
  extrusion.mapElementID2Element2dID(groupId, elId, groupId2d, elId2d);
  for (size_t i = 0; i < closure.size(); ++i) {
    size_t nodeId2d;
    extrusion.map3d2d(groupId, elId, closure[i], groupId2d, elId2d, nodeId2d);
    const dgFullMatrix<double> &groupProxy = dof.getGroupProxy(groupId2d);
    for (int j = 0; j < dof.getNbFields(); ++j)
      nodalV(i, j) = groupProxy(nodeId2d, elId2d * dof.getNbFields() + j);
  }
}

static void transposeElement(const fullMatrix<double> &in, fullMatrix<double> &out, size_t nElement)
{
  out.resize(nElement * in.size1(), in.size2() / nElement, false);
  for (size_t i = 0; i < nElement; ++i) {
    for (int j = 0; j < in.size1(); ++j) {
      for (int k = 0; k < out.size2(); ++k) {
        out(i * in.size1() + j, k) = in(j, i * out.size2() + k);
      }
    }
  }
}

//todo : we should store data in the right order on dgDofContainer...
static fullMatrix<double> transposeElementField(const dgFullMatrix<double> &in)
{
  fullMatrix<double> out(in.size1(), in.size2(), false);
  for (size_t iBlock = 0; iBlock < in.nBlock(); ++iBlock) {
    for (int j = 0; j < in.getBlockSize(); ++j) {
      for (int i = 0; i < in.size1(); ++i) {
        out(i, j * in.nBlock() + iBlock) = in(i, iBlock * in.getBlockSize() + j);
      }
    }
  }
  return out;
}


void dgDofFunction::call(dataCacheMap *m, dataCacheDouble *d, fullMatrix<double> &sol)
{
  dgExtrusion *extrusion = m->getGroupCollection()->getExtrusion();
  switch(m->getMode()) {
    case dataCacheMap::INTEGRATION_GROUP_MODE:
    {
      if(m->getGroupOfInterfaces() == NULL) {
        _dof->_getAtIntegrationPoints(*m->getGroupOfElements(), m->getIntegrationMatrices(), sol);
      }
      else {
        _dof->_getAtIntegrationPoints(*m->getGroupOfInterfaces(), m->connectionId(),
                                     m->getIntegrationMatrices(), sol);
      }
      return;
    }
    case dataCacheMap::NODE_GROUP_MODE:
    {
      // nodes on element
      if(m->getGroupOfInterfaces() == NULL) {
        if (extrusion && &extrusion->getGroups2d() == _dof->getGroups()){
          sol = get3dNodalValuesFrom2dDof(*extrusion, *_dof, *m->getGroupOfElements());
        }
        else {
          transposeElement(_dof->getGroupProxy(m->getGroupId()), sol, m->getGroupOfElements()->getNbElements());
        }
        return;
      }

      // nodes on face
      const dgGroupOfFaces &faces = *m->getGroupOfInterfaces();
      if (extrusion && &extrusion->getGroups2d() == _dof->getGroups()) {
        sol.resize(m->nPoint(), _dof->getNbFields());
        fullMatrix<double> fSol(m->nPointByElement(), _dof->getNbFields());
        for (int iF = 0; iF < m->nElement(); ++iF) {
          get3dNodalValuesFrom2dDofFace(*extrusion, *_dof, m->getGroupId(), m->elementId(iF), m->getGroupOfInterfaces()->closure(m->interfaceId(iF), m->connectionId()), fSol);
          for (int i= 0; i < m->nPointByElement(); ++i) {
            for (int j = 0; j < _dof->getNbFields(); ++j) {
              sol(iF * m->nPointByElement() + i, j) = fSol(i, j);
            }
          }
        }
      }
      else {
        const dgFullMatrix<double> &proxyGroup = _dof->getGroupProxy(m->getGroupId());
        for(size_t iF = 0; iF < faces.size(); ++iF) {
          int nFields = _dof->getNbFields();
          const std::vector<int> &closure = m->getGroupOfInterfaces()->closure(m->interfaceId(0), m->connectionId());
          for (int iField = 0; iField < nFields; iField++) {
            const int ip = m->elementId(iF) * nFields + iField;
            for(size_t j = 0; j < closure.size(); j++) {
              sol(iF * closure.size() + j, iField) = proxyGroup(closure[j], ip);
            }
          }
        }
      }
      return;
    }
    case dataCacheMap::NODE_MODE:
    {
      // nodes on element
      if(m->getGroupOfInterfaces() == NULL) {
        if (extrusion && &extrusion->getGroups2d() == _dof->getGroups())
          get3dNodalValuesFrom2dDof(*extrusion, *_dof, m->getGroupId(), m->elementId(0), sol);
        else
          _dof->getGroupProxy(m->getGroupId()).getBlockProxy(m->elementId(0), sol);
        return;
      }

      // nodes on face
      if (extrusion && &extrusion->getGroups2d() == _dof->getGroups()) {
        sol.resize(m->nPoint(), _dof->getNbFields());
        get3dNodalValuesFrom2dDofFace(*extrusion, *_dof, m->getGroupId(), m->elementId(0), m->getGroupOfInterfaces()->closure(m->interfaceId(0), m->connectionId()), sol);
      }
      else {
        const dgFullMatrix<double> &proxyGroup = _dof->getGroupProxy(m->getGroupId());
        int nFields = _dof->getNbFields();
        const std::vector<int> &closure = m->getGroupOfInterfaces()->closure(m->interfaceId(0), m->connectionId());
        sol.resize(m->nPoint(), nFields);
        for (int iField = 0; iField < nFields; iField++) {
          const int ip = m->elementId(0) * nFields + iField;
          for(size_t j = 0; j < closure.size(); j++) {
            sol(j, iField) = proxyGroup(closure[j], ip);
          }
        }
      }
      return;
    }

    case dataCacheMap::POINT_MODE:
    {
      sol.setAll(0.);
      double f[256];
      fullMatrix<double> nodalV;
      const dgGroupOfFaces * faces = m->getGroupOfInterfaces();
      const fullMatrix<double> &uvw = m->parametricCoordinates();
      // volume case
      if(faces == NULL) {
        size_t groupId = m->getGroupId();
        size_t elementId = m->elementId(0);
        if (extrusion && &extrusion->getGroups2d() == _dof->getGroups())
          extrusion->map3d2d(groupId, elementId, groupId, elementId);
        const dgGroupOfElements *group = _dof->getGroups()->getElementGroup(groupId);
        group->getFunctionSpace().f(uvw(0, 0), uvw(0, 1), uvw(0, 2), f);
        _dof->getGroupProxy(groupId).getBlockProxy(m->elementId(0), nodalV);
        for (int j = 0; j < nodalV.size1(); j++) {
          for (int k = 0; k < _dof->getNbFields(); k++) {
            sol(0, k) += nodalV(j, k) * f[j];
          }
        }
        return;
      }
      if (m->getGroupCollection() != _dof->getGroups())
        Msg::Fatal("dgDofFunction not implemented in extruded + interface point mode");
      // interface (boundary) case
      faces->getNodalBasis()->f(uvw(0, 0), uvw(0, 1), uvw(0, 2), f);
      int nbNodes = faces->getNbNodes();
      _dof->getGroupProxy(m->getGroupId()).getBlockProxy(m->elementId(0), nodalV);
      const std::vector<int> &closure = faces->closure(m->interfaceId(0), 0);
      for (int j = 0; j < nbNodes; j++) {
        for (int k = 0; k < _dof->getNbFields(); k++) {
          sol(0, k) += nodalV(closure[j], k) * f[j];
        }
      }
      return;
    }
    case dataCacheMap::MULTIPOINT_MODE:
    {
      sol.setAll(0.);
      double f[256];
      fullMatrix<double> nodalV;
      const dgGroupOfFaces * faces = m->getGroupOfInterfaces();
      const fullMatrix<double> &uvw = m->parametricCoordinates();
      // volume case
      if(faces == NULL) {
        size_t groupId = m->getGroupId();
        for (int i = 0; i < m->nPoint(); ++i) {
          size_t elementId = m->elementId(i);
          if (extrusion && &extrusion->getGroups2d() == _dof->getGroups())
            extrusion->map3d2d(groupId, elementId, groupId, elementId);
          const dgGroupOfElements *group = _dof->getGroups()->getElementGroup(groupId);
          group->getFunctionSpace().f(uvw(i, 0), uvw(i, 1), uvw(i, 2), f);
          _dof->getGroupProxy(groupId).getBlockProxy(m->elementId(i), nodalV);
          for (int j = 0; j < nodalV.size1(); j++) {
            for (int k = 0; k < _dof->getNbFields(); k++) {
              sol(i, k) += nodalV(j, k) * f[j];
            }
          }
        }
        return;
      }
      if (m->getGroupCollection() != _dof->getGroups())
        Msg::Fatal("dgDofFunction not implemented in extruded + interface point mode");
      // interface (boundary) case
      for (int i = 0; i < m->nPoint(); ++i) {
        faces->getNodalBasis()->f(uvw(i, 0), uvw(i, 1), uvw(i, 2), f);
        int nbNodes = faces->getNbNodes();
        _dof->getGroupProxy(m->getGroupId()).getBlockProxy(m->elementId(i), nodalV);
        const std::vector<int> &closure = faces->closure(m->interfaceId(i), 0);
        for (int j = 0; j < nbNodes; j++) {
          for (int k = 0; k < _dof->getNbFields(); k++) {
            sol(i, k) += nodalV(closure[j], k) * f[j];
          }
        }
      }
      return;
    }

    default:
      Msg::Fatal("a function requires a dof function but the algorithm does not provide it");
  }
}

dgDofFunctionGradient::dgDofFunctionGradient(dgDofContainer* dof):
  function(dof->getNbFields()*3), _dof(dof) {}

dgDofFunctionGradient::dgDofFunctionGradient(): function(0), _dof(NULL) {}

void dgDofFunctionGradient::setDof(dgDofContainer* dof) {
  _dof = dof;
  _nbCol = dof->getNbFields() * 3;
}

void dgDofFunctionGradient::call(dataCacheMap* m, dataCacheDouble *d, fullMatrix< double >& sol)
{
  if (m->getMode() == dataCacheMap::POINT_MODE) {
    fullMatrix<double> dVdXi, dVdX, S, dVdXENode;
    double f[256];
    const dgGroupOfElements *group = m->getGroupOfElements();
    const dgIntegrationMatrices &intMatrices = m->getIntegrationMatrices();
    const dgMeshJacobian &jac = m->getJacobians();
    const fullMatrix<double> &uvw = m->parametricCoordinates();
    group->getFunctionSpace().f(uvw(0, 0), uvw(0, 1), uvw(0, 2), f);
    fullMatrix<double> nodalV(group->getNbNodes(), _dof->getNbFields());;
    dgExtrusion *extrusion = m->getGroupCollection()->getExtrusion();
    if (extrusion && &extrusion->getGroups2d() == _dof->getGroups())
      get3dNodalValuesFrom2dDof(*extrusion, *_dof, m->getGroupId(), m->elementId(0), nodalV);
    else
      _dof->getGroupProxy(group).getBlockProxy(m->elementId(0), nodalV);
    dVdXi.resize(group->getDimUVW() * intMatrices.integrationPoints().size1(), _dof->getNbFields());
    dVdX.resize(intMatrices.integrationPoints().size1(), 3 * _dof->getNbFields());
    S.resize(group->getNbNodes(), 3 * _dof->getNbFields());
    dVdXENode.resize(group->getNbNodes(), 3 * _dof->getNbFields());
    intMatrices.dPsi().mult(nodalV, dVdXi); // dVdXi = V * dPsidXi
    jac.multDXiDXElement(m->getGroupId(), m->elementId(0), dVdXi, dVdX); // dVdX = dVdXi * dXidX
    intMatrices.psiW().mult(dVdX, S); // S = < dVdX * Psi >
    intMatrices.massInverse().mult(S, dVdXENode); // dVdXENode = M^-1 * S
    sol.resize(1, _dof->getNbFields() * 3, true);
    for (int j = 0; j < group->getNbNodes(); j++) {
      for (int k = 0; k < _dof->getNbFields(); k++) {
        sol(0, k * 3 + 0) += dVdXENode(j, k * 3 + 0) * f[j];
        sol(0, k * 3 + 1) += dVdXENode(j, k * 3 + 1) * f[j];
        sol(0, k * 3 + 2) += dVdXENode(j, k * 3 + 2) * f[j];
      }
    }
    return;
  }
  if (m->getMode() == dataCacheMap::MULTIPOINT_MODE) {
    sol.setAll(0.);
    fullMatrix<double> dVdXi, dVdX, S, dVdXENode;
    double f[256];
    const dgGroupOfElements *group = m->getGroupOfElements();
    const dgIntegrationMatrices &intMatrices = m->getIntegrationMatrices();
    const dgMeshJacobian &jac = m->getJacobians();
    const fullMatrix<double> &uvw = m->parametricCoordinates();
    for (int i = 0; i < m->nPoint(); ++i) {
      group->getFunctionSpace().f(uvw(i, 0), uvw(i, 1), uvw(i, 2), f);
      fullMatrix<double> nodalV(group->getNbNodes(), _dof->getNbFields());;
      dgExtrusion *extrusion = m->getGroupCollection()->getExtrusion();
      if (extrusion && &extrusion->getGroups2d() == _dof->getGroups())
        get3dNodalValuesFrom2dDof(*extrusion, *_dof, m->getGroupId(), m->elementId(i), nodalV);
      else
        _dof->getGroupProxy(group).getBlockProxy(m->elementId(i), nodalV);
      dVdXi.resize(group->getDimUVW() * intMatrices.integrationPoints().size1(), _dof->getNbFields());
      dVdX.resize(intMatrices.integrationPoints().size1(), 3 * _dof->getNbFields());
      S.resize(group->getNbNodes(), 3 * _dof->getNbFields());
      dVdXENode.resize(group->getNbNodes(), 3 * _dof->getNbFields());
      intMatrices.dPsi().mult(nodalV, dVdXi); // dVdXi = V * dPsidXi
      jac.multDXiDXElement(m->getGroupId(), m->elementId(i), dVdXi, dVdX); // dVdX = dVdXi * dXidX
      intMatrices.psiW().mult(dVdX, S); // S = < dVdX * Psi >
      intMatrices.massInverse().mult(S, dVdXENode); // dVdXENode = M^-1 * S
      for (int j = 0; j < group->getNbNodes(); j++) {
        for (int k = 0; k < _dof->getNbFields(); k++) {
          sol(i, k * 3 + 0) += dVdXENode(j, k * 3 + 0) * f[j];
          sol(i, k * 3 + 1) += dVdXENode(j, k * 3 + 1) * f[j];
          sol(i, k * 3 + 2) += dVdXENode(j, k * 3 + 2) * f[j];
        }
      }
    }
    return;
    
  }
  if (m->getMode() == dataCacheMap::INTEGRATION_GROUP_MODE) {
    dgFullMatrix<double> groupval;
    if(m->getGroupOfInterfaces() == NULL) {
      _dof->_getGradientAtIntegrationPoints(*m->getGroupOfElements(), m->getIntegrationMatrices(), groupval);
    }
    else {
      _dof->_getGradientAtIntegrationPoints(*m->getGroupOfInterfaces(), m->connectionId(), m->getIntegrationMatrices(), groupval);
    }
    transposeElement(groupval, sol, groupval.nBlock());
    return;
  }
  if (m->getMode() == dataCacheMap::NODE_GROUP_MODE) {
    if (m->getGroupOfInterfaces() == NULL) {
      dgFullMatrix<double> gradAtQP;
      _dof->_getGradientAtIntegrationPoints(*m->getGroupOfElements(), m->getIntegrationMatrices(), gradAtQP);
      fullMatrix<double> integratedGrad(m->getGroupOfElements()->getNbNodes(), gradAtQP.size2());
      m->getIntegrationMatrices().psiW().mult(gradAtQP, integratedGrad);
      d->_groupValue.resize(integratedGrad.size1(), _dof->getNbFields() * 3, m->getGroupOfElements()->getNbElements(), false);
      m->getIntegrationMatrices().massInverse().mult(integratedGrad, d->_groupValue);
    }
    else {
      dgFullMatrix<double> gradAtQP;
      _dof->_getGradientAtIntegrationPoints(*m->getGroupOfInterfaces(), m->connectionId(), m->getIntegrationMatrices(), gradAtQP);
      fullMatrix<double> integratedGrad(m->getGroupOfInterfaces()->getNodalBasis()->points.size1(), gradAtQP.size2());
      m->getIntegrationMatrices().psiW().mult(gradAtQP, integratedGrad);
      d->_groupValue.resize(integratedGrad.size1(), _dof->getNbFields() * 3, m->getGroupOfInterfaces()->size(), false);
      m->getIntegrationMatrices().massInverse().mult(integratedGrad, d->_groupValue);
    }
  }
  if(m->getMode() == dataCacheMap::NODE_MODE) {
    if (d->_groupValue.size1() == 0) {
      if (m->getGroupOfInterfaces() == NULL) {
        dgFullMatrix<double> gradAtQP;
        _dof->_getGradientAtIntegrationPoints(*m->getGroupOfElements(), m->getIntegrationMatrices(), gradAtQP);
        fullMatrix<double> integratedGrad(m->getGroupOfElements()->getNbNodes(), gradAtQP.size2());
        m->getIntegrationMatrices().psiW().mult(gradAtQP, integratedGrad);
        d->_groupValue.resize(integratedGrad.size1(), _dof->getNbFields() * 3, m->getGroupOfElements()->getNbElements(), false);
        m->getIntegrationMatrices().massInverse().mult(integratedGrad, d->_groupValue);
      }
      else {
        dgFullMatrix<double> gradAtQP;
        _dof->_getGradientAtIntegrationPoints(*m->getGroupOfInterfaces(), m->connectionId(), m->getIntegrationMatrices(), gradAtQP);
        fullMatrix<double> integratedGrad(m->getGroupOfInterfaces()->getNodalBasis()->points.size1(), gradAtQP.size2());
        m->getIntegrationMatrices().psiW().mult(gradAtQP, integratedGrad);
        d->_groupValue.resize(integratedGrad.size1(), _dof->getNbFields() * 3, m->getGroupOfInterfaces()->size(), false);
        m->getIntegrationMatrices().massInverse().mult(integratedGrad, d->_groupValue);
      }
    }
    d->_groupValue.getBlockProxy(m->getGroupOfInterfaces() == NULL ? m->elementId(0) : m->interfaceId(0), sol);
  }
}

void dgDofContainer::build(const dgGroupCollection *groups)
{
  int dataSize = 0;
  for (int i=0; i< groups->getNbElementGroups();i++){
    int nbNodes    = groups->getElementGroup(i)->getNbNodes();
    int nbElements = groups->getElementGroup(i)->getNbElements();
    dataSize += nbNodes*_nbFields*nbElements;
  }

  // allocate the big vectors
  _data.resize(dataSize, false);
  // create proxys for each group
  int offset = 0;
  _dataProxys.resize(groups->getNbElementGroups()+groups->getNbGhostGroups());
  _groupFirstDofId.resize(groups->getNbElementGroups()+groups->getNbGhostGroups());
  for (int i=0;i<groups->getNbElementGroups();i++){
    dgGroupOfElements *group = groups->getElementGroup(i);
    int nbNodes    = group->getNbNodes();
    int nbElements = group->getNbElements();
    _dataProxys[i].setAsProxy(&_data(offset),nbNodes, _nbFields, nbElements);
    _groupFirstDofId[i] = offset;
    offset += nbNodes*_nbFields*nbElements;
  }

  //ghosts
  int totalNbElementsGhost = 0;
  int dataSizeGhost = 0;
  for (int i=0; i<groups->getNbGhostGroups(); i++){
    int nbNodes    = groups->getGhostGroup(i)->getNbNodes();
    int nbElements = groups->getGhostGroup(i)->getNbElements();
    totalNbElementsGhost +=nbElements;
    dataSizeGhost += nbNodes*_nbFields*nbElements;
  }

  _ghostData.resize(dataSizeGhost, false);
  int ghostOffset=0;
  for (int i=0;i<groups->getNbGhostGroups();i++){
    dgGroupOfElements *group = groups->getGhostGroup(i);
    int nbNodes    = group->getNbNodes();
    int nbElements = group->getNbElements();
    int gid = i+groups->getNbElementGroups();
    _dataProxys[gid].setAsProxy(&_ghostData(ghostOffset), nbNodes, _nbFields, nbElements);
    _groupFirstDofId[gid] = offset;
    offset += nbNodes*_nbFields*nbElements;
    ghostOffset += nbNodes*_nbFields*nbElements;
  }
}

void dgDofContainer::_init() {
  _parallel = NULL;
  _parallelDG = NULL;
  _scatterOp = SCATTER_NONE;
  _function = new dgDofFunction(this);
  _functionGradient = new dgDofFunctionGradient(this);
  _fieldsName = new std::string[_nbFields];
  _fs = &_groups->_mesh->elementVector(0).functionSpace();
  build(_groups);
}

dgDofContainer::dgDofContainer (const dgDofContainer &orig):
  _groups (orig.getGroups()),
  _nbFields (orig.getNbFields())
{
  _init();
}

dgDofContainer::dgDofContainer (const dgGroupCollection &groups,  int nbFields):
  _groups (&groups),
  _nbFields (nbFields)
{
  _init();
}

void dgDofContainer::transferGroups(const dgGroupCollection *newGroups, dgDofContainer *newDof){
  std::map<int, int> dataMap;
  int oldEls=0;
  for(int iGroup=0; iGroup<_groups->getNbElementGroups();iGroup++){
    dgGroupOfElements *g=_groups->getElementGroup(iGroup);
    for(size_t iElem=0; iElem<g->getNbElements();iElem++){
      dataMap[g->getElement(iElem)->getNum()]=oldEls+iElem;
    }
    oldEls+=g->getNbElements();
  }

  int newEls=0;
  for(int iGroup=0; iGroup<newGroups->getNbElementGroups();iGroup++){
    dgGroupOfElements *g=newGroups->getElementGroup(iGroup);
    for(size_t iElem=0; iElem<g->getNbElements();iElem++){
      int nbNodes=g->getNbNodes();
      for(int iNode=0; iNode<nbNodes; iNode++){
        for(int iField=0; iField<_nbFields; iField++){
          newDof->_data((newEls+iElem)*nbNodes*_nbFields+iNode*_nbFields+iField)=_data(dataMap[g->getElement(iElem)->getNum()]*nbNodes*_nbFields+iNode*_nbFields+iField);
        }
      }
    }
    newEls+=g->getNbElements();
  }


}

void dgDofContainer::changeGroups(const dgGroupCollection *newGroups, std::map<int, std::pair<int, int> > groupsMap){
  fullVector<double> oldData=_data;
  build(newGroups);
  for(int iGroup=0; iGroup<newGroups->getNbElementGroups(); iGroup++){
    dgGroupOfElements *g=newGroups->getElementGroup(iGroup);
    int nbNodes=g->getNbNodes();
    for(size_t iElem=0; iElem<g->getNbElements();iElem++){
      int oldJ=groupsMap[g->getElement(iElem)->getNum()].first;
      int newJ=groupsMap[g->getElement(iElem)->getNum()].second;
      for(int iNode=0; iNode<nbNodes; iNode++){
        for(int iField=0; iField<_nbFields; iField++) {
          _data(newJ*_nbFields+iNode*_nbFields+iField) = oldData(oldJ*_nbFields+iNode*_nbFields+iField);
        }
      }
    }
  }
}

void dgDofContainer::setFieldName (int fieldIndex, std::string fieldName) {
  _fieldsName[fieldIndex] = fieldName;
}

std::string dgDofContainer::getFieldName (int fieldIndex) const {
  return _fieldsName[fieldIndex];
}

void dgDofContainer::destroyParallel(){
  if(_parallel){
    delete _parallel;
  }
  if(_parallelDG) {
    delete _parallelDG;
  }
  for (std::map<std::pair<int, int>, parallelStructure*>::iterator it = _parallelMRTag.begin(); it != _parallelMRTag.end(); ++it) {
    delete it->second;
  }
}

dgDofContainer::~dgDofContainer (){
  destroyParallel();
  delete _function;
  delete _functionGradient;
  delete[] _fieldsName;
}


void dgDofContainer::scatterBegin(bool dgGhost)
{
  static int ptag = 1000;
  ptag ++;
  if(_scatterOp != SCATTER_NONE) {
    Msg::Fatal("As scatter operation is already in progress");
  }
  if (dgGhost) {
    if (!_parallelDG)
      _parallelDG = new parallelStructure(this, true);
     _scatterOp = SCATTER_DG;
    _parallelDG->scatterBegin(ptag);
  }
  else {
    if (!_parallel)
      _parallel = new parallelStructure(this, false);
    _scatterOp = SCATTER_CG;
    _parallel->scatterBegin(ptag);
  }
}

void dgDofContainer::scatterBeginMRTag(int maxTag, int minTag)
{
  static int ptag = 1000;
  std::pair<int, int> tagPair = std::make_pair(maxTag, minTag);
  ptag ++;
  if (!_parallelMRTag[tagPair]){
    _parallelMRTag[tagPair] = new parallelStructure(this, true, maxTag, minTag);
  }
  _parallelMRTag[tagPair]->scatterBegin(ptag);
}

void dgDofContainer::scatterEndMRTag(int maxTag, int minTag)
{
  std::pair<int, int> tagPair = std::make_pair(maxTag, minTag);
  if (!_parallelMRTag[tagPair])
    Msg::Fatal ("no scatter begin for this tag");
  _parallelMRTag[tagPair]->scatterEnd();
}

void dgDofContainer::scatterEnd()
{
  switch (_scatterOp) {
    case SCATTER_CG :
      _parallel->scatterEnd();
      break;
    case SCATTER_DG :
      _parallelDG->scatterEnd();
      break;
    default:
      Msg::Warning("dgDofContainer::scatterEnd() called but no scatter operation in progress\n");
  }
  _scatterOp = SCATTER_NONE;
}

void dgDofContainer::scatter(bool dgGhost)
{
  scatterBegin(dgGhost);
  scatterEnd();
}

void dgDofContainer::setAll(double v) {
  for(int i=0;i<_data.size();i++)
    _data(i)=v;
  for(int i=0;i<_ghostData.size();i++)
    _ghostData(i)=v;
}
void dgDofContainer::setAll(std::vector<double> v) {
  if(_nbFields != (int)v.size())
    Msg::Fatal("vector size must match nbfields in dgDofContainer setAll(vector)");
  fullMatrix<double> data, ghostData;
  for (int iGroup = 0; iGroup < _groups->getNbElementGroups(); iGroup++) {
    const dgGroupOfElements &group = *_groups->getElementGroup(iGroup);
    for (size_t iElement = 0; iElement < group.getNbElements(); ++iElement) {
      getGroupProxy(iGroup).getBlockProxy(iElement, data);
      getGroupProxy(iGroup).getBlockProxy(iElement, ghostData);
      for(int k=0; k < _nbFields; k++) {
        for(int i=0; i < group.getNbNodes(); i++) {
          data(i, k) = v[k];
          ghostData(i, k) = v[k];
        }
      }
    }
  }
}
void dgDofContainer::scale(double f)
{
  _data.scale(f);
  _ghostData.scale(f);
}

void dgDofContainer::multiply(const dgDofContainer &x, const dgDofContainer &y){
  if(_nbFields != x.getNbFields() || _nbFields != y.getNbFields())
    Msg::Fatal("vector size must match nbfields in dgDofContainer multiply(vector, vector)");
  fullMatrix<double> dataP, ghostDataP, dataX, ghostDataX, dataY, ghostDataY;
  for (int iGroup = 0; iGroup < _groups->getNbElementGroups(); iGroup++) {
    const dgGroupOfElements &group = *_groups->getElementGroup(iGroup);
    for (size_t iElement = 0; iElement < group.getNbElements(); ++iElement) {
      getGroupProxy(iGroup).getBlockProxy(iElement, dataP);
      getGroupProxy(iGroup).getBlockProxy(iElement, ghostDataP);
      x.getGroupProxy(iGroup).getBlockProxy(iElement, dataX);
      x.getGroupProxy(iGroup).getBlockProxy(iElement, ghostDataX);
      y.getGroupProxy(iGroup).getBlockProxy(iElement, dataY);
      y.getGroupProxy(iGroup).getBlockProxy(iElement, ghostDataY);
      for(int k=0; k < _nbFields; k++) {
        for(int i=0; i < group.getNbNodes(); i++) {
          dataP(i, k) = dataX(i, k) * dataY(i, k);
          ghostDataP(i, k) = ghostDataX(i, k) * ghostDataY(i, k);
        }
      }
    }
  }
}

void dgDofContainer::divide(const dgDofContainer &x, const dgDofContainer &y, int iFieldY){
  if( iFieldY == -1 && (_nbFields != x.getNbFields() || _nbFields != y.getNbFields()))
    Msg::Fatal("vector size must match nbfields in dgDofContainer divide(vector, vector)");
  fullMatrix<double> dataP, ghostDataP, dataX, ghostDataX, dataY, ghostDataY;
  for (int iGroup = 0; iGroup < _groups->getNbElementGroups(); iGroup++) {
    const dgGroupOfElements &group = *_groups->getElementGroup(iGroup);
    for (size_t iElement = 0; iElement < group.getNbElements(); ++iElement) {
      getGroupProxy(iGroup).getBlockProxy(iElement, dataP);
      getGroupProxy(iGroup).getBlockProxy(iElement, ghostDataP);
      x.getGroupProxy(iGroup).getBlockProxy(iElement, dataX);
      x.getGroupProxy(iGroup).getBlockProxy(iElement, ghostDataX);
      y.getGroupProxy(iGroup).getBlockProxy(iElement, dataY);
      y.getGroupProxy(iGroup).getBlockProxy(iElement, ghostDataY);
      for(int k=0; k < _nbFields; k++) {
        for(int i=0; i < group.getNbNodes(); i++) {
          if (iFieldY == -1){
            dataP(i, k) = dataX(i, k) / dataY(i, k);
            ghostDataP(i, k) = ghostDataX(i, k) / ghostDataY(i, k);
          }
          else{
            dataP(i, k) = dataX(i, k) / dataY(i, iFieldY);
            ghostDataP(i, k) = ghostDataX(i, k) / ghostDataY(i, iFieldY);
          }
        }
      }
    }
  }
}

void dgDofContainer::dotProduct(const dgDofContainer &x, fullVector<double> &product){
  if(_nbFields != x.getNbFields())
    Msg::Fatal("vector size must match nbfields in dgDofContainer dotProduct(vector)");
  fullMatrix<double> dataP, dataX;

  fullVector<double> prod(_nbFields);
  prod.setAll(0);
  product.resize(_nbFields);
  for (int iGroup = 0; iGroup < _groups->getNbElementGroups(); iGroup++) {
    const dgGroupOfElements &group = *_groups->getElementGroup(iGroup);
    for (size_t iElement = 0; iElement < group.getNbElements(); ++iElement) {
      getGroupProxy(iGroup).getBlockProxy(iElement, dataP);
      x.getGroupProxy(iGroup).getBlockProxy(iElement, dataX);
      for(int k=0; k < _nbFields; k++) {
        for(int i=0; i < group.getNbNodes(); i++) {
          prod(k) += dataP(i, k) * dataX(i, k);
        }
      }
    }
  }
  #if defined(HAVE_MPI)
  MPI_Allreduce(&prod(0), &product(0), _nbFields, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  #else
  product = prod;
  #endif
}


void dgDofContainer::axpy(const dgDofContainer &x, double a)
{
  _data.axpy(x._data,a);
  _ghostData.axpy(x._ghostData,a);
}

int dgDofContainer::norm0() const{
  int localNorm = _data.size();
#ifdef HAVE_MPI
  int globalNorm;
  MPI_Allreduce(&localNorm, &globalNorm, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  return globalNorm;
#else
  return localNorm;
#endif
}

double dgDofContainer::norm() const{
#ifdef HAVE_MPI
  double localNorm = _data.norm();
  double localNorm2 = localNorm * localNorm;
  double globalNorm2;
  MPI_Allreduce(&localNorm2, &globalNorm2, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  double globalNorm = sqrt(globalNorm2);
  return globalNorm;
#else
  return _data.norm();
#endif
}

double dgDofContainer::norm(std::vector<dgGroupOfElements *>gV) {
  double norm=0;
  for(size_t i=0;i<gV.size();i++){
    dgGroupOfElements* g=gV[i];
    fullMatrix<double>&groupData=getGroupProxy(g);
    for(int r=0;r<groupData.size1();r++){
      for(int c=0;c<groupData.size1();c++){
        norm+=groupData(r,c)*groupData(r,c);
      }
    }
  }
  return norm;
}
void dgDofContainer::save(const std::string name) {
  FILE *f = fopen (name.c_str(),"wb");
  _data.binarySave(f);
  fclose(f);
}
void dgDofContainer::load(const std::string name) {
  FILE *f = fopen (name.c_str(),"rb");
  _data.binaryLoad(f);
  fclose(f);
}

static void multAddInverseMassMatrix ( const dgGroupOfElements & group, const dgFullMatrix<double> &residu, dgFullMatrix<double> &sol)
{
  fullMatrix<double> residuEl;
  fullMatrix<double> solEl;
  fullMatrix<double> iMassEl;
  for(size_t i = 0; i < group.getNbElements(); i++) {
    residu.getBlockProxy(i, residuEl);
    sol.getBlockProxy(i, solEl);
    group.getInverseMassMatrix().getBlockProxy(i, iMassEl);
    solEl.gemm(iMassEl, residuEl);
  }
}

void dgDofContainer::L2Projection(const function *f, dgDofContainer *sol, dgDofContainer *var) {
  dgDofContainer rhs(*_groups, _nbFields);
  dataCacheMap cacheMap(dataCacheMap::INTEGRATION_GROUP_MODE, _groups);
  if(sol)
    cacheMap.setSolutionFunction(sol->getFunction(), sol->getFunctionGradient());
  if(var)
    cacheMap.setVariableFunction(var->getFunction(), var->getFunctionGradient());
  dataCacheDouble &sourceTerm = *cacheMap.get(f);
  for (int iGroup = 0; iGroup < _groups->getNbElementGroups(); iGroup++) {
    const dgGroupOfElements &group = *_groups->getElementGroup(iGroup);
    cacheMap.setGroup(&group);
    const dgIntegrationMatrices &intMatrices = cacheMap.getIntegrationMatrices();
    rhs.getGroupProxy(iGroup).gemm(intMatrices.psiW(), cacheMap.getJacobians().multJElement(group.elementVectorId(), sourceTerm.get()), 1., 0.);
    getGroupProxy(iGroup).setAll(0.);
    multAddInverseMassMatrix(group, rhs.getGroupProxy(iGroup), getGroupProxy(iGroup));
  }
}

L2ProjectionContinuous::L2ProjectionContinuous(dgDofManager* dofManager) :
      _dofManager(dofManager)
{
  _nbFields = _dofManager->getNbFields();
  _dofManager->fillSparsityPattern(false);

  // fill the left hand side with the mass matrix
  _dofManager->zeroMatrix();
  const dgGroupCollection &groups = _dofManager->getGroups();
  fullMatrix<double> massFull;
  for (int iGroup = 0; iGroup < groups.getNbElementGroups (); iGroup++) {
    dgGroupOfElements *group = groups.getElementGroup(iGroup);
    int nbNodes = group->getNbNodes();
    massFull.resize(nbNodes * _nbFields, nbNodes * _nbFields, true);
    fullMatrix<double> massE;
    for (size_t iElement = 0 ; iElement < group->getNbElements(); iElement++) {
      group->getMassMatrix().getBlockProxy(iElement, massE);
      for (int k = 0; k < _nbFields; k++)
        for (int i = 0; i < nbNodes; i++)
          for (int j = 0; j < nbNodes; j++)
            massFull(k * nbNodes + i, k * nbNodes + j) = massE(i, j);
      _dofManager->assembleLHSMatrix(iGroup, iElement, iGroup, iElement, massFull);
    }
  }
}

void L2ProjectionContinuous::apply(dgDofContainer *output, const function * f) {

  if(_nbFields != f->getNbCol())
    Msg::Fatal("size of nbFields (%d) in L2ProjectionContinuous different than size of the specified function (%d) !", _nbFields, f->getNbCol());
  if(_nbFields != output->getNbFields())
    Msg::Fatal("size of nbFields (%d) in L2ProjectionContinuous different than size of the dgDofContainer (%d) !", _nbFields, output->getNbFields());

  // fill the right hand side with the function f
  _dofManager->zeroRHS();
  const dgGroupCollection &groups = _dofManager->getGroups();
  dataCacheMap cacheMap(dataCacheMap::INTEGRATION_GROUP_MODE, &groups);
  dataCacheDouble *fd = cacheMap.get(f);
  dgFullMatrix<double> fJPsi;
  fullMatrix<double> proxy;
  for (int iGroup = 0; iGroup < groups.getNbElementGroups (); iGroup++) {
    dgGroupOfElements *group = groups.getElementGroup(iGroup);
    cacheMap.setGroup(group);
    const dgIntegrationMatrices &intMatrices = cacheMap.getIntegrationMatrices();
    fJPsi.resize(group->getNbNodes(), _nbFields, group->getNbElements(), false);
    fJPsi.gemm(intMatrices.psiW(), cacheMap.getJacobians().multJElement(group->elementVectorId(), fd->get()), 1., 0.);
    for (size_t iElement = 0; iElement < group->getNbElements(); iElement++) {
      fJPsi.getBlockProxy(iElement, proxy);
      _dofManager->assembleRHSVector(iGroup, iElement, proxy);
    }
  }
  _dofManager->solve();
  // copy solution in the dof container
  fullMatrix<double> v, outputE;
  for (int iGroup = 0; iGroup < groups.getNbElementGroups (); iGroup++) {
    dgGroupOfElements *group = groups.getElementGroup(iGroup);
    v.resize (group->getNbNodes() * _nbFields, 1);
    for (size_t iElement = 0 ; iElement < group->getNbElements(); iElement++) {
      _dofManager->getSolution(iGroup, iElement, v);
      output->getGroupProxy(group).getBlockProxy(iElement, outputE);
      v.reshape(outputE.size1(), outputE.size2());
      outputE.setAll(v);
    }
  }
}

void dgDofContainer::fromElsewhereL2Projection(const function *f, const dgGroupCollection *gcEw, std::vector<std::string> tags,
                                 dgDofContainer *sol, dgDofContainer *var) {
  if(_nbFields != f->getNbCol())
    Msg::Fatal("nbFields of dofContainer (%d) different than nbFields of function (%d) in fromElsewhereL2Projection !",
               _nbFields, f->getNbCol());
  if(_groups->getDim() > gcEw->getDim())
    Msg::Error("could not project smth in n dimensions onto smth >n dimensions");
  bool sameDim = false;
  if(gcEw->getDim() == _groups->getDim())
    sameDim = true;
  else if(gcEw->getDim() - _groups->getDim() > 1)
    Msg::Error("could not project smth in n dimensions onto smth in smt different than n or n-1 dim");

  dgDofContainer rhs(*_groups, _nbFields);
  rhs.scale(0.);
  dataCacheMap cacheMapEw(dataCacheMap::INTEGRATION_GROUP_MODE, gcEw);
  if(sol)
    cacheMapEw.setSolutionFunction(sol->getFunction(), sol->getFunctionGradient());
  if(var)
    cacheMapEw.setVariableFunction(var->getFunction(), var->getFunctionGradient());
  dataCacheDouble &sourceTerm = *cacheMapEw.get(f);
  fullMatrix<double> source;

  for (int iGroup = 0; iGroup < _groups->getNbElementGroups(); iGroup++) {
    const dgGroupOfElements &gLocal = *_groups->getElementGroup(iGroup);
    if(!tags.empty() && std::find(tags.begin(), tags.end(), gLocal.getPhysicalTag()) == tags.end())
      continue;

    int ig, ie;
    if(sameDim) {
      gcEw->find(gLocal.getElement(0), ig, ie);
      if(ig == -1)
        Msg::Error("elmt not found in dgVolumeOnSurfaceL2Projection !");
      const dgGroupOfElements * g = gcEw->getElementGroup(ig);
      cacheMapEw.setGroup(g);
    } else {
      gcEw->findFace(gLocal.getElement(0), ig, ie);
      if(ig == -1)
        Msg::Error("elmt not found in dgVolumeOnSurfaceL2Projection !");
      const dgGroupOfFaces * g = gcEw->getFaceGroup(ig);
      cacheMapEw.setInterfaceGroup(g);
    }

    const nodalBasis &fs = gLocal.getFunctionSpace();
    const dgIntegrationMatrices &intMatrices = dgIntegrationMatrices::get(fs.type, cacheMapEw.getIntegrationOrder());
    int nbQP = intMatrices.integrationPoints().size1();
    const dgMeshJacobian &jacobians = _groups->_mesh->getJacobian(cacheMapEw.getIntegrationOrder()); //TODO secure for != orders

    dgFullMatrix<double> Source(nbQP, _nbFields, gLocal.getNbElements(), false);

    for (size_t iElmt = 0; iElmt < gLocal.getNbElements(); iElmt++) {
      if(sameDim) {
        gcEw->find(gLocal.getElement(iElmt), ig, ie);
        if(ie == -1)
          Msg::Error("elmt not found in dgVolumeOnSurfaceL2Projection !");
      } else {
        gcEw->findFace(gLocal.getElement(iElmt), ig, ie);
        if(ie == -1)
          Msg::Error("elmt not found in dgVolumeOnSurfaceL2Projection !");
      }

      Source.getBlockProxy(iElmt, source);
      const fullMatrix<double> &sourceTermMatrix = sourceTerm.get();
      for (int iPt = 0; iPt < nbQP; ++iPt) {
        const double detJ = jacobians.detJElement(iGroup)(iPt, iElmt);
        for (int k = 0; k < _nbFields; k++)
          source(iPt, k) = sourceTermMatrix(ie *nbQP + iPt, k) * detJ;
      }
    }
    rhs.getGroupProxy(iGroup).gemm(intMatrices.psiW(), Source);
    getGroupProxy(iGroup).setAll(0.);
    multAddInverseMassMatrix(gLocal, rhs.getGroupProxy(iGroup), getGroupProxy(iGroup));
  }
}


void dgDofContainer::copy(const dgDofContainer& x)
{
  _data.setAll(x._data);
  _ghostData.setAll(x._ghostData);
}

// multiplies the data array by the inverse mass matrix in place
void dgDofContainer::multiplyByInvMassMatrix()
{
  fullMatrix<double> dataEl, iMassEl;
  for(int k = 0; k < _groups->getNbElementGroups(); k++) {
    dgGroupOfElements *group = _groups->getElementGroup(k);
    int nbNodes = group->getNbNodes();
    fullMatrix<double> tempElemData(nbNodes,_nbFields);
    for(size_t i = 0; i < group->getNbElements(); i++) {
      getGroupProxy(k).getBlockProxy(i, dataEl);
      group->getInverseMassMatrix().getBlockProxy(i, iMassEl);
      iMassEl.mult(dataEl,tempElemData);
      dataEl.setAll(tempElemData);
    }
  }
}

void dgDofContainer::multiplyByMassMatrix()
{
  fullMatrix<double> dataEl, massEl;
  for(int k = 0; k < _groups->getNbElementGroups(); k++) {
    dgGroupOfElements *group = _groups->getElementGroup(k);
    int nbNodes = group->getNbNodes();
    fullMatrix<double> tempElemData(nbNodes,_nbFields);
    for(size_t i = 0; i < group->getNbElements(); i++) {
      getGroupProxy(k).getBlockProxy(i, dataEl);
      group->getMassMatrix().getBlockProxy(i, massEl);
      massEl.mult(dataEl, tempElemData);
      dataEl.setAll(tempElemData);
    }
  }
}

void dgDofContainer::multiplyByLocalMatrix(const dgGroupOfElements &group, const fullMatrix<double> mat)
{
  fullMatrix<double> dataEl;
  int nbNodes = group.getNbNodes();
  fullMatrix<double> tempElemData(nbNodes,_nbFields);
  for(size_t i = 0; i < group.getNbElements(); i++) {
    getGroupProxy(group.elementVectorId()).getBlockProxy(i, dataEl);
    mat.mult(dataEl, tempElemData);
    dataEl.setAll(tempElemData);
  }
}

void dgDofContainer::multiplyByDiagonalVector(const dgGroupOfElements &group, const fullVector<double> &vec)
{
  fullMatrix<double> dataEl;
  int nbNodes = group.getNbNodes();
  fullMatrix<double> tempElemData(nbNodes,_nbFields);
  for(size_t i = 0; i < group.getNbElements(); i++) {
    getGroupProxy(group.elementVectorId()).getBlockProxy(i, dataEl);
    for (int i = 0; i < dataEl.size1(); i++){
      for (int j = 0; j < dataEl.size2(); j++){
	dataEl(i,j) *= vec(i);
      }
    }
  }
}

void dgDofContainer::interpolate(const function *f, dgDofContainer *sol, dgDofContainer *var, int IDgroup, double time, bool scatterSolution){
  dataCacheMap cacheMap(dataCacheMap::NODE_MODE, _groups);
  if(sol)
    cacheMap.setSolutionFunction(sol->getFunction(), sol->getFunctionGradient());
  if(var)
    cacheMap.setVariableFunction(var->getFunction(), var->getFunctionGradient());
  for (int iGroup = 0; iGroup < _groups->getNbElementGroups(); iGroup++) {
    if(IDgroup != -1 && IDgroup != iGroup) continue;
    const dgGroupOfElements &group = *_groups->getElementGroup(iGroup);
    dataCacheDouble &values = *cacheMap.get(f);
    fullMatrix<double> proxy;
    if (time > 0) {function::getTime()->set(time);}
    cacheMap.setGroup(&group);
    for (size_t iElement = 0 ; iElement < group.getNbElements(); ++iElement) {
      cacheMap.setElement(iElement);
       getGroupProxy(iGroup).getBlockProxy(iElement, proxy);
      const fullMatrix<double> &valuesMatrix = values.get();
      for (int iPt = 0; iPt < group.getNbNodes(); iPt++) {
        for (int k = 0; k <_nbFields; k++)
          proxy(iPt, k) = valuesMatrix(iPt, k);
      }
    }
  }
  if (scatterSolution)
  scatter(true);
}

void dgDofContainer::interpolate(const function *f, double time, dgDofContainer *sol, bool scatterSolution){
  this->interpolate(f, sol, NULL, -1, time, scatterSolution);
}

//EMI , RE-INITIALIZATION OF LEVEL SET FUNCTION, MAYBE MOVES THIS SOMEWHERE ELSE
void dgDofContainer::reInitialize(const double width){
  const dgGroupOfElements &group = *_groups->getElementGroup(0);
  if (group.getDimUVW()!=2)
    Msg::Fatal("todo: implement re-initialize for 3D");
  if (group.getOrder()!=1)
    Msg::Fatal("todo: implement recursive re-initialize for high order");


  std::multimap<SPoint3, SPoint3> lines;
  std::map<MVertex*, double> distance_map;
  std::vector<SPoint3>  pts;
  std::vector<MVertex*> pt2Vertex;
  std::vector<double> distances;
  for (int iGroup=0;iGroup<_groups->getNbElementGroups();iGroup++) {
    for (size_t iElement=0 ; iElement<group.getNbElements() ;++iElement) {
      MElement *e = group.getElement(iElement);
      if (e->getNumVertices() != group.getNbNodes())
	Msg::Fatal("todo: implement recursive re-initialize for higher order");

      for (int j = 0; j< e->getNumVertices(); j++){
	MVertex *v = e->getVertex(j);
	std::map<MVertex*, double>::iterator it = distance_map.find(v);
	if (it == distance_map.end()){
	  distance_map.insert(std::make_pair(v, 0.0));
	  pts.push_back(SPoint3(v->x(), v->y(),v->z()));
	  pt2Vertex.push_back(v);
	  distances.push_back(1.e22);
	}
      }

      fullMatrix<double> sol(getGroupProxy(iGroup), iElement*_nbFields,_nbFields);
      std::vector<SPoint3> zeros;
      zeros.clear();
      for (int iPt =0; iPt< group.getNbNodes(); iPt++) {
	int iPt2 = (iPt == group.getNbNodes()-1) ? 0 : iPt+1;
	if (sol(iPt,0)*sol(iPt2,0) < 0.0){
	  MVertex *v1 = e->getVertex(iPt);
	  MVertex *v2 = e->getVertex(iPt2);
	  double x0 = v1->x() - sol(iPt,0)*(v2->x() -v1->x())/(sol(iPt2,0)-sol(iPt,0));
	  double y0 = v1->y() - sol(iPt,0)*(v2->y() -v1->y())/(sol(iPt2,0)-sol(iPt,0));
	  double z0 = v1->z() - sol(iPt,0)*(v2->z() -v1->z())/(sol(iPt2,0)-sol(iPt,0));
	  zeros.push_back(SPoint3(x0,y0,z0));
	}
      }
      if (zeros.size() == 2){
	lines.insert(std::make_pair(zeros[0], zeros[1]));
      }
    }
  }

   FILE * f = fopen("iso2.pos","w");
   fprintf(f,"View \"\"{\n");
   for (std::multimap<SPoint3, SPoint3>::iterator it = lines.begin();
          it != lines.end(); ++it ){
       fprintf(f,"SL(%g,%g,%g,%g,%g,%g){%g,%g};\n",
   	      it->first.x(),  it->first.y(), it->first.z(),
               it->second.x(), it->second.y(), it->second.z(),
               1.0, 1.0);
   }
  fprintf(f,"};\n");
   fclose(f);

  for (std::multimap<SPoint3, SPoint3>::iterator it = lines.begin();
         it != lines.end(); ++it ){
    std::vector<double> iDistances;
    std::vector<SPoint3> iClosePts;
    SPoint3 p1 = it->first;
    SPoint3 p2 = it->second;
    signedDistancesPointsLine(iDistances, iClosePts, pts, p1, p2);
    for (size_t kk = 0; kk< pts.size(); kk++) {
      if (fabs(iDistances[kk]) < distances[kk]){
  	distances[kk] = fabs(iDistances[kk]);
  	MVertex *v = pt2Vertex[kk];
  	distance_map[v] = distances[kk];
      }
    }
  }

  for (int iGroup=0;iGroup<_groups->getNbElementGroups();iGroup++) {
    const dgGroupOfElements &group = *_groups->getElementGroup(iGroup);
    dataCacheMap cacheMap(dataCacheMap::NODE_MODE, _groups);
    fullMatrix<double> proxy;
    cacheMap.setGroup(&group);
    for (size_t iElement=0 ; iElement<group.getNbElements() ;++iElement) {
      cacheMap.setElement(iElement);
      getGroupProxy(iGroup).getBlockProxy(iElement, proxy);
      MElement *e = group.getElement(iElement);
      for (int iPt = 0; iPt< e->getNumVertices(); iPt++){
        std::map<MVertex*, double>::iterator itd = distance_map.find(e->getVertex(iPt));
        if (itd != distance_map.end()){
          double sign  = ( proxy(iPt,0) > 0.0) ? 1.0: -1.0;
          double cutOff;
          if (width == -1.0)
            cutOff = itd->second;
          else
            cutOff = std::min(itd->second, width);
          proxy(iPt,0) = sign*cutOff;
        }
        else
          printf("vertex NOT found \n");
      }
    }
  }

}


void dgDofContainer::exportMultirateTimeStep(){
  std::ostringstream name_oss;
  name_oss<<"multirateTimeStep.msh";
  if(Msg::GetCommSize()>1)
    name_oss<<"_"<<Msg::GetCommRank();
  FILE *f = fopen (name_oss.str().c_str(),"w");
  int COUNT = 0;
  for (int i=0;i < _groups->getNbElementGroups() ;i++){
    COUNT += _groups->getElementGroup(i)->getNbElements();
  }
  fprintf(f,"$MeshFormat\n2.1 0 8\n$EndMeshFormat\n");
  fprintf(f,"$ElementNodeData\n");
  fprintf(f,"1\n");
  fprintf(f,"\"%s\"\n","MultirateTimeStep");
  fprintf(f,"1\n");
  fprintf(f,"0.0\n");
  fprintf(f,"%d\n", Msg::GetCommSize() > 1 ? 4 : 3);
  fprintf(f,"0\n 1\n %d\n",COUNT);
  if(Msg::GetCommSize() > 1) fprintf(f,"%d\n", Msg::GetCommRank());
  for (int i=0;i < _groups->getNbElementGroups()  ;i++){
    dgGroupOfElements *group = _groups->getElementGroup(i);
    int tag = group->getMultirateTag();
    for (size_t iElement=0 ; iElement< group->getNbElements() ;++iElement) {
      MElement *e = group->getElement(iElement);
      int num = e->getNum();
      int numVert=e->getNumVertices();
      fprintf(f,"%d %d",num,numVert);
      for (int k=0;k<numVert;++k) {
        fprintf(f," %.16E ",(tag+1)/3*1.0);
      }
      fprintf(f,"\n");
    }
  }
  fprintf(f,"$EndElementNodeData\n");
  fclose(f);
  return;
}
void dgDofContainer::exportMultirateGroups(){
  std::ostringstream name_oss;
  name_oss<<"multirateGroups.msh";
  if(Msg::GetCommSize()>1)
    name_oss<<"_"<<Msg::GetCommRank();
  FILE *f = fopen (name_oss.str().c_str(),"w");
  int COUNT = 0;
  for (int i=0;i < _groups->getNbElementGroups() ;i++){
    COUNT += _groups->getElementGroup(i)->getNbElements();
  }
  fprintf(f,"$MeshFormat\n2.1 0 8\n$EndMeshFormat\n");
  fprintf(f,"$ElementNodeData\n");
  fprintf(f,"1\n");
  fprintf(f,"\"%s\"\n","MultirateGroups");
  fprintf(f,"1\n");
  fprintf(f,"0.0\n");
  fprintf(f,"%d\n", Msg::GetCommSize() > 1 ? 4 : 3);
  fprintf(f,"0\n 1\n %d\n",COUNT);
  if(Msg::GetCommSize() > 1) fprintf(f,"%d\n", Msg::GetCommRank());
  for (int i=0;i < _groups->getNbElementGroups()  ;i++){
    dgGroupOfElements *group = _groups->getElementGroup(i);
    int tag = group->getMultirateTag();
    for (size_t iElement=0 ; iElement< group->getNbElements() ;++iElement) {
      MElement *e = group->getElement(iElement);
      int num = e->getNum();
      int numVert=e->getNumVertices();
      fprintf(f,"%d %d",num,numVert);
      for (int k=0;k<numVert;++k) {
        fprintf(f," %.16E ",tag*1.0);
      }
      fprintf(f,"\n");
    }
  }
  fprintf(f,"$EndElementNodeData\n");
  fclose(f);
  return;
}

void dgDofContainer::exportGroupIdMsh(){
  std::ostringstream name_oss;
  name_oss<<"groupsId.msh";
  if(Msg::GetCommSize()>1)
    name_oss<<"_"<<Msg::GetCommRank();
  FILE *f = fopen (name_oss.str().c_str(),"w");
  int COUNT = 0;
  for (int i=0;i < _groups->getNbElementGroups() ;i++){
    COUNT += _groups->getElementGroup(i)->getNbElements();
  }
  fprintf(f,"$MeshFormat\n2.1 0 8\n$EndMeshFormat\n");
  fprintf(f,"$ElementNodeData\n");
  fprintf(f,"1\n");
  fprintf(f,"\"%s\"\n","GroupsIds");
  fprintf(f,"1\n");
  fprintf(f,"0.0\n");
  fprintf(f,"%d\n", Msg::GetCommSize() > 1 ? 4 : 3);
  fprintf(f,"0\n 1\n %d\n",COUNT);
  if(Msg::GetCommSize() > 1) fprintf(f,"%d\n", Msg::GetCommRank());
  for (int i=0;i < _groups->getNbElementGroups()  ;i++){
    dgGroupOfElements *group = _groups->getElementGroup(i);
    for (size_t iElement=0 ; iElement< group->getNbElements() ;++iElement) {
      MElement *e = group->getElement(iElement);
      int num = e->getNum();
      int numVert=e->getNumVertices();
      fprintf(f,"%d %d",num,numVert);
      for (int k=0;k<numVert;++k) {
        fprintf(f," %.16E ",i*1.0);
      }
      fprintf(f,"\n");
    }
  }
  fprintf(f,"$EndElementNodeData\n");
  fclose(f);
  return;
}

void dgDofContainer::exportFunctionSurf (function * fun, const std::string name, double time, int step, std::string fieldname, std::vector<std::string> Tags)
{

  int nbExport = fun->getNbCol();
  bool vector = false;
  bool tensor = false;
  if (fun->getNbCol() == 3) {
    nbExport = 1;
    vector = true;
  } else if (fun->getNbCol() == 9) {
    nbExport = 1;
    tensor = true;
  }

  for (int ICOMP = 0; ICOMP<nbExport;++ICOMP){
    std::ostringstream name_oss, name_view;
    if (fieldname!=""){
      name_view<<fieldname;
      name_oss<<name<<"_"<<fieldname<<".msh";
    }
    else if (vector){
      name_view<<"vector";
      name_oss<<name<<".msh";
    }
    else if (tensor) {
      name_view<<"tensor";
      name_oss<<name<<".msh";
    }
    else if (!vector && nbExport > 1){
      name_view<<"comp_"<<ICOMP;
      name_oss<<name<<"_COMP_"<<ICOMP<<".msh";
    }
    else{
      name_view<<"function";
      name_oss<<name<<".msh";
    }
    if(Msg::GetCommSize()>1)
      name_oss<<"_"<<Msg::GetCommRank();
    FILE *f = fopen (name_oss.str().c_str(),"w");
    if(!f){
      Msg::Fatal("Unable to open export file '%s'", name.c_str());
    }

    //Write txt file if export on lines
    bool txtExport = false;
    FILE *f2=NULL;
    if (nbExport == 1 && _groups->getElementGroup(0)->getDimUVW() == 2){
      txtExport = true;
      std::ostringstream name_txt;
      name_txt<<name<<"_"<<fieldname<<".txt";
      f2 = fopen (name_txt.str().c_str(),"w");
    }

    std::vector<dgGroupOfFaces*> faceGroups;
    int COUNT = 0;
    for (int iGFace=0; iGFace < _groups->getNbFaceGroups(); iGFace++){
      for (size_t i=0;i<Tags.size();i++){
        if(Tags[i] == _groups->getFaceGroup(iGFace)->physicalTag()) {
          faceGroups.push_back(_groups->getFaceGroup(iGFace));
          COUNT += _groups->getFaceGroup(iGFace)->size();
        }
      }
    }
    int totalMPI = faceGroups.size();
#ifdef HAVE_MPI
    MPI_Allreduce((void*) &totalMPI, &totalMPI, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
#endif

    if (totalMPI == 0) {
      std::string errMsg = "No boundary faces found with the given physical tag(s): ";
      for (size_t i = 0; i < Tags.size(); i++)
        errMsg += Tags.at(i) += " ";
      Msg::Fatal(errMsg.c_str());
    }

    fprintf(f,"$MeshFormat\n2.1 0 8\n$EndMeshFormat\n");
    fprintf(f,"$ElementNodeData\n");
    fprintf(f,"1\n");
    fprintf(f,"\"%s\"\n",name_view.str().c_str());
    fprintf(f,"1\n");
    fprintf(f,"%f\n", time);
    fprintf(f,"%d\n", Msg::GetCommSize() > 1 ? 4 : 3);
    fprintf(f,"%d\n", step);
    if (vector)  fprintf(f,"3\n");
    else if (tensor) fprintf (f, "9\n");
    else  fprintf(f,"1\n");
    fprintf(f,"%d\n", COUNT);
    if(Msg::GetCommSize() > 1) fprintf(f,"%d\n", Msg::GetCommRank());

    for (size_t iG=0; iG < faceGroups.size(); iG++){

      dataCacheMap cacheMap(dataCacheMap::NODE_MODE, _groups);
      cacheMap.setSolutionFunction (getFunction(), getFunctionGradient());
      dataCacheDouble &value = *cacheMap.get(fun);
      dataCacheDouble &coord = *cacheMap.get(function::getCoordinates());

      dgGroupOfFaces *face = faceGroups[iG];
      cacheMap.setInterfaceGroup(face, 0);
      //const dgFullMatrix<double> &xyz = face->getCoordinates();

      for (size_t iFace=0 ; iFace < face->size(); ++iFace) {
        if (face->meshInterfaceElement(iFace) == NULL){
          Msg::Fatal("Element tag is empty");
        }
        else {
          int num =  face->meshInterfaceElement(iFace)->getNum();

          cacheMap.setInterface(iFace);
          const fullMatrix<double> &valueMatrix = value.get();
          const fullMatrix<double> &coordMatrix = coord.get();
          int nbWrite = valueMatrix.size1();
          fprintf(f,"%d %d", num, nbWrite);
          if (vector || tensor){
            for (int k=0;k<valueMatrix.size1();++k)
              for (int j=0;j< valueMatrix.size2();++j)
                fprintf(f," %.16E ",valueMatrix(k,j));
          }
          else{
            for (int k=0;k<valueMatrix.size1();++k) {
              fprintf(f," %.16E ",valueMatrix(k,ICOMP));
              if (txtExport)  {
                fullMatrix<double> xyzF;
                      fprintf(f2,"%g %g %g %.16E \n", coordMatrix(k,0), coordMatrix(k,1), coordMatrix(k,2), valueMatrix(k,ICOMP));
              }
            }
          }
          fprintf(f,"\n");
        }
      }
    }
    fprintf(f,"$EndElementNodeData\n");
    fclose(f);
    if (txtExport) fclose(f2);
  }
}

static void  writeMshInterpolationSchemes(FILE *f, const std::string &name, const dgGroupCollection &groups)
{
  std::set<const nodalBasis*> allPb;
  for (int i=0; i<groups.getNbElementGroups(); ++i)
    allPb.insert(&groups.getElementGroup(i)->getFunctionSpace());
  fprintf(f, "$InterpolationScheme\n");
  fprintf(f, "\"interp_%s\"\n",name.c_str());
  fprintf(f, "%lu\n", (unsigned long int) allPb.size());
  for (std::set<const nodalBasis*>::iterator it = allPb.begin(); it != allPb.end(); ++it) {
    const polynomialBasis &pb = *(polynomialBasis*)*it;
    fprintf(f, "%i 2\n", pb.parentType);
    fprintf(f, "%i %i\n", pb.coefficients.size1(), pb.coefficients.size2());
    for (int i=0; i<pb.coefficients.size1(); ++i) {
      for (int j=0; j<pb.coefficients.size2(); ++j)
        fprintf(f, "%.16g ", pb.coefficients(i ,j));
      fprintf(f, "\n");
    }
    fprintf(f, "%i %i\n", pb.monomials.size1(), pb.monomials.size2());
    for (int i=0; i<pb.monomials.size1(); ++i) {
      for (int j=0; j<pb.monomials.size2(); ++j)
        fprintf(f, "%.16g ", pb.monomials(i ,j));
      fprintf(f, "\n");
    }
  }
  fprintf(f, "$EndInterpolationScheme\n");
}

void dgDofContainer::exportFunctionMsh (const function * fun, const std::string name, double time, int step, std::string fieldname) const
{
  Msg::Warning("exportFunctionMsh is now a member of dgGroupCollection, calling it from dgDofContainer is deprecated and will be removed");
  _groups->exportFunctionMsh(fun, name, time, step, fieldname, this);
}

void dgDofContainer::importMsh(const std::string filename, double &time, int &step, std::string &fieldName)
{
  //create a map elementId -> (group, idInGroup);
  std::map<int,std::pair<int, int> > elementMap;
  for (int i = 0; i< _groups->getNbElementGroups(); i++) {
    dgGroupOfElements *g = _groups->getElementGroup (i);
    for (size_t j = 0; j < g->getNbElements(); j++)
      elementMap [g->getElement(j)->getNum()] = std::make_pair (i,j);
  }

  for (int ICOMP = 0; ICOMP<_nbFields;++ICOMP){
    std::ostringstream name_oss, name_view;
    name_view<<"comp_"<<ICOMP;
    name_oss<<filename<<"_COMP_"<<ICOMP<<".msh";
    if(Msg::GetCommSize()>1)
      name_oss<<"_"<<Msg::GetCommRank();
    std::ifstream f (name_oss.str().c_str());
    if(!f.is_open()){
      Msg::Fatal("Unable to open import file '%s'", name_oss.str().c_str());
    }
    std::string word;
    int ibuff;
    double dbuff;
    f >> word; if (word!="$MeshFormat") Msg::Fatal("bad file '%s'",name_oss.str().c_str());
    f >> dbuff; if (dbuff<2.0) Msg::Fatal("bad file '%s'",name_oss.str().c_str());
    f >> ibuff >> ibuff;
    f >> word; if (word!="$EndMeshFormat") Msg::Fatal("bad file '%s'",name_oss.str().c_str());
    f >> word;
    if (word =="$InterpolationScheme") {
      while (word != "$EndInterpolationScheme" && !f.eof())
        f >> word;
      f >> word;
    }
    if (word!="$ElementNodeData") Msg::Fatal("bad file '%s'",name_oss.str().c_str());
    int nString;
    f >> nString;
    getline (f, word);
    for (int i = 0; i < nString; ++i) {
      getline (f, word);
      if(i== 0) {
        word.erase( remove( word.begin(), word.end(), '\"' ), word.end() );
        fieldName = word;
      }
    }
    // remove all double quotes
    f >> dbuff >> time;
    f >> dbuff >> step;
    int count, rank, cr;
    f >> rank>> count;
    if(Msg::GetCommSize()>1)
      f >> cr;
    for (int i = 0; i < count; i++) {
      int elementNum, size;
      f >> elementNum >> size;
      std::map<int, std::pair<int, int> > ::iterator it = elementMap.find(elementNum);
      if (it == elementMap.end())  Msg::Fatal("bad element id (%i) in file '%s'",elementNum, name_oss.str().c_str());
      fullMatrix<double> sol(getGroupProxy(it->second.first), it->second.second*_nbFields,_nbFields);
      if (size != sol.size1())  Msg::Fatal("bad size (%i) for element id (%i) in file '%s'",size, elementNum, name_oss.str().c_str());
      for (int k=0;k<sol.size1();++k) {
        if(!(f >> sol(k, ICOMP)))Msg::Fatal("bad file '%s'",name_oss.str().c_str());
      }
    }
    f >> word; if (word!="$EndElementNodeData") Msg::Fatal("bad file '%s' (got '%s')",name_oss.str().c_str(), word.c_str());
  }
}


void dgDofContainer::readNodeData(std::ifstream &f, const std::string name, int ICOMP)
{

  std::string word;
  double dbuff;

  std::map<int,double> cgdata;

  f >> word; if (word!="$NodeData") Msg::Fatal("bad file '%s'",name.c_str());
  f >> dbuff;
  int step;
  getline (f, word);
  getline (f, word);
  f >> dbuff >> dbuff >> dbuff >> step;
  int count, rank, cr;
  f >> rank>> count;
  if(Msg::GetCommSize()>1)
    f >> cr;
  for (int i = 0; i < count; i++) {
    int nodeId;
    double value;
    f >> nodeId >> value;
    cgdata[nodeId] = value;
  }
  f >> word; if (word!="$EndNodeData") Msg::Fatal("bad file '%s' (got '%s')",name.c_str(), word.c_str());

  // for each element in each group: fill nodal solution...
  for (int i = 0; i< _groups->getNbElementGroups(); i++) {
    dgGroupOfElements *g = _groups->getElementGroup (i);
    for (size_t j = 0; j < g->getNbElements(); j++){
      MElement *el = g->getElement(j);
      fullMatrix<double> sol(getGroupProxy(i), j*_nbFields,_nbFields);
      for (int k=0;k<el->getNumVertices();k++){
        sol(k,ICOMP) = cgdata[el->getVertex(k)->getIndex()];
      }
    }
  }

}

void dgDofContainer::importMshNodeData(const std::string name, bool multiFile)
{

  if (multiFile)
    for (int ICOMP = 0; ICOMP<_nbFields;++ICOMP){
      std::ostringstream name_oss;
      name_oss<<name<<"_COMP_"<<ICOMP<<".msh";
      if(Msg::GetCommSize()>1)
        name_oss<<"_"<<Msg::GetCommRank();
      std::ifstream f (name_oss.str().c_str());
      if(!f.is_open()){
        Msg::Fatal("Unable to open import file '%s'", name_oss.str().c_str());
      }
      std::string word;
      int ibuff;
      double dbuff;
      f >> word; if (word!="$MeshFormat") Msg::Fatal("bad file '%s'",name_oss.str().c_str());
      f >> dbuff; if (dbuff<2.0) Msg::Fatal("bad file '%s'",name_oss.str().c_str());
      f >> ibuff >> ibuff;
      f >> word; if (word!="$EndMeshFormat") Msg::Fatal("bad file '%s'",name_oss.str().c_str());
      readNodeData(f, name_oss.str(), ICOMP);
      f.close();
    }
  else {
    std::ifstream f (name.c_str());
    if(!f.is_open()){
      Msg::Fatal("Unable to open import file '%s'", name.c_str());
    }
    std::string word;
    int ibuff;
    double dbuff;
    f >> word; if (word!="$MeshFormat") Msg::Fatal("bad file '%s'",name.c_str());
    f >> dbuff; if (dbuff<2.0) Msg::Fatal("bad file '%s'",name.c_str());
    f >> ibuff >> ibuff;
    f >> word; if (word!="$EndMeshFormat") Msg::Fatal("bad file '%s'",name.c_str());
    for (int ICOMP = 0; ICOMP<_nbFields;++ICOMP) readNodeData(f, name, ICOMP);
    f.close();
  }

}

void dgDofContainer::_readMsh(std::string name, std::string name_dir, double &time, int &step, std::map<int,std::pair<int, int> > elementMap, int nb_parts, int dofF){
  size_t endp = name.size();
  std::string f2name = std::string(name, 1, endp-3);
  std::ifstream f ((name_dir+f2name).c_str());
  if(!f.is_open()){
    Msg::Fatal("Unable to open import file '%s'", (name_dir+f2name).c_str());
  }
  std::string word;
  int ibuff;
  double dbuff;
  f >> word; if (word!="$MeshFormat") Msg::Fatal("bad file '%s'",(name_dir+f2name).c_str());
  f >> dbuff; if (dbuff<2.0) Msg::Fatal("bad file '%s'",(name_dir+f2name).c_str());
  f >> ibuff >> ibuff;
  f >> word; if (word!="$EndMeshFormat") Msg::Fatal("bad file '%s'",(name_dir+f2name).c_str());
  f >> word;
  if (word =="$InterpolationScheme") {
    while (word != "$EndInterpolationScheme" && !f.eof())
      f >> word;
    f >> word;
  }
  if (word!="$ElementNodeData") Msg::Fatal("bad file '%s'",(name_dir+f2name).c_str());
  int nStrings;
  f >> nStrings;
  getline (f, word);
  for (int i = 0; i < nStrings; i++)
    getline (f, word);
  f >> dbuff >> time;
  f >> dbuff >> step;
  int count, rank, cr;
  f >> rank>> count;
  if(nb_parts>1)
    f >> cr;
  for (int i = 0; i < count; i++) {
    int elementNum, size;
    f >> elementNum >> size;
    std::map<int, std::pair<int, int> > ::iterator it = elementMap.find(elementNum);
    if (it != elementMap.end()){
      fullMatrix<double> sol(getGroupProxy(it->second.first), it->second.second*_nbFields,_nbFields);
      if (size != sol.size1())  Msg::Fatal("bad size (%i) for element id (%i) in file '%s'",size, elementNum, (name_dir+f2name).c_str());
      for (int k = 0; k < sol.size1(); ++k) {
        if(!(f >> sol(k, dofF)))Msg::Fatal("bad file '%s'",(name_dir+f2name).c_str());
      }
    }
    else{
      getline(f, word);
    }
  }
  f >> word; if (word!="$EndElementNodeData") Msg::Fatal("bad file '%s' (got '%s')", (name_dir+f2name).c_str(), word.c_str());
}

void dgDofContainer::readMsh(const std::string name, double &time, int &step, int inField, int outField)
{
  if((inField == -1 && outField !=-1) || (outField == -1 && inField !=-1))
    Msg::Fatal("Both fields ids need to be -1 to import all fields or 2 existing positive fields ids should be considered");

  std::map<int,std::pair<int, int> > elementMap;
  for (int i = 0; i< _groups->getNbElementGroups(); i++) {
    dgGroupOfElements *g = _groups->getElementGroup (i);
    for (size_t j = 0; j < g->getNbElements(); j++)
      elementMap [g->getElement(j)->getNum()] = std::make_pair (i,j);
  }
  std::string name_file, name_dir;
  size_t lastp = name.find_last_of('/');
  size_t endp = name.size();
  if(lastp != std::string::npos){
    name_file = std::string(name, lastp+1, endp);
    name_dir = std::string(name, 0, lastp+1);
  }
  else{
    name_file=name;
    name_dir="";
  }
  std::ifstream g1 (name.c_str());
  if(!g1.is_open()){
    Msg::Fatal("Unable to open import file '%s'", name.c_str());
  }
  std::string text, f1name;
  int nb_fields, nb_parts, iField, jField;
  g1 >> text >> text;
  g1 >> nb_fields;
  if(inField < -1 || inField >= _nbFields){
    Msg::Fatal("Field %d does not exist for the actual dgDofContainer", inField);
  }
  if(outField < -1 || outField >= nb_fields){
    Msg::Fatal("Field %d does not exist in the file %s", outField, name.c_str());
  }
  g1 >> text >> text;
  g1 >> nb_parts;
  if (nb_fields > 1){
    for(int iF = 0; iF < nb_fields; iF++){
      g1 >> text >> text;
      g1 >> iField;
      g1 >> text >> text;
      if(iField == outField || outField == -1){
        int dofF;
        if(inField == -1){
          dofF = iF;
        }
        else{
          dofF=inField;
        }
        size_t endp = text.size();
        f1name = std::string(text, 1, endp-3);
        std::ifstream g2 ((name_dir+f1name).c_str());
        if(!g2.is_open()){
          Msg::Fatal("Unable to open import file '%s'", f1name.c_str());
        }
        g2 >> text >> text;
        g2 >> jField;
        if(iField != jField)
          Msg::Fatal("Fields are not compatible!");
        for (int iP = 0; iP < nb_parts; iP++){
          g2 >> text >> text;
          _readMsh(text, name_dir, time, step, elementMap, nb_parts, dofF);
        }
      }
    }
  }
  else{
    for (int iP = 0; iP < nb_parts; iP++){
      g1 >> text >> text;
      _readMsh(text, name_dir, time, step, elementMap, nb_parts, 0);
    }
  }
}


void dgDofContainer::exportMsh(const std::string name, double time, int step, std::string fieldName) const
{
  for (int ICOMP=0; ICOMP<_nbFields; ++ICOMP)
    exportMshSingleComp(ICOMP, name, time, step, fieldName);
}

void dgDofContainer::exportMshSingleComp(int ICOMP, const std::string name, double time, int step, std::string fieldName) const
{
  if (ICOMP >= _nbFields)
    Msg::Fatal("Unable to open export file '%s': ICOMP (%i) greater or equal to the number of fields (%i)", name.c_str(), ICOMP, _nbFields);

  std::ostringstream name_oss, name_view;
  if (fieldName != "")
    name_view<<fieldName;
  else if (!_fieldsName[ICOMP].empty())
    name_view<<_fieldsName[ICOMP];
  else
    name_view<<"comp_"<<ICOMP;
  name_oss<<name<<"_COMP_"<<ICOMP<<".msh";
  if (Msg::GetCommSize()>1)
    name_oss<<"_"<<Msg::GetCommRank();

  FILE *f = fopen ((name_oss.str()).c_str(),"w");
  if (!f)
    Msg::Fatal("Unable to open export file '%s'", name.c_str());

  int COUNT = 0;
  for (int i=0; i<_groups->getNbElementGroups(); i++)
    COUNT += _groups->getElementGroup(i)->getNbElements();
  fprintf(f, "$MeshFormat\n2.1 0 8\n$EndMeshFormat\n");
  writeMshInterpolationSchemes(f, name_view.str(), *_groups);
  fprintf(f, "$ElementNodeData\n");
  fprintf(f, "2\n");
  fprintf(f, "\"%s\"\n", name_view.str().c_str());
  fprintf(f, "\"interp_%s\"\n", name_view.str().c_str());
  fprintf(f, "1\n");
  fprintf(f, "%f\n", time);
  fprintf(f, "%d\n", Msg::GetCommSize() > 1 ? 4 : 3);
  fprintf(f, "%d\n 1\n %d\n", step, COUNT);
  if (Msg::GetCommSize()>1)
    fprintf(f, "%d\n", Msg::GetCommRank());
  for (int i=0; i<_groups->getNbElementGroups(); i++){
    dgGroupOfElements *group = _groups->getElementGroup(i);
    int repeatorder0 = group->getElement(0)->getNumVertices();
    for (size_t iElement=0 ; iElement<group->getNbElements(); ++iElement) {
      MElement *e = group->getElement(iElement);
      int num = e->getNum();
      fullMatrix<double> sol;
      getGroupProxy(i).getBlockProxy(iElement, sol);
      if (sol.size1() == 1)
        fprintf(f, "%d %d",num,repeatorder0);
      else
        fprintf(f, "%d %d",num,sol.size1());
      for (int k=0; k<sol.size1(); ++k) {
        fprintf(f, " %.16E ", sol(k,ICOMP));
        if (sol.size1() == 1)
          for (int j = 1; j < repeatorder0; ++j)
            fprintf(f," %.16E ",sol(k,ICOMP));
      }
      fprintf(f,"\n");
    }
  }
  fprintf(f,"$EndElementNodeData\n");
  fclose(f);
}

void dgDofContainer::exportMshVector(const std::string name, double time, int step, std::string fieldName) const
{
  if (_nbFields != 2 && _nbFields != 3)
    Msg::Fatal("dgDofContainer::exportMshVector only available for dofContainer with 2 or 3 fields");

  std::ostringstream name_oss, name_view;
  if (fieldName != "")
    name_view<<fieldName;
  else
    name_view<<"vector";
  name_oss<<name<<".msh";
  if (Msg::GetCommSize()>1)
    name_oss<<"_"<<Msg::GetCommRank();

  FILE *f = fopen ((name_oss.str()).c_str(),"w");
  if (!f)
    Msg::Fatal("Unable to open export file '%s'", name.c_str());

  int COUNT = 0;
  for (int i=0; i<_groups->getNbElementGroups(); i++)
    COUNT += _groups->getElementGroup(i)->getNbElements();
  fprintf(f, "$MeshFormat\n2.1 0 8\n$EndMeshFormat\n");
  writeMshInterpolationSchemes(f, name_view.str(), *_groups);
  fprintf(f, "$ElementNodeData\n");
  fprintf(f, "2\n");
  fprintf(f, "\"%s\"\n", name_view.str().c_str());
  fprintf(f, "\"interp_%s\"\n", name_view.str().c_str());
  fprintf(f, "1\n");
  fprintf(f, "%f\n", time);
  fprintf(f, "%d\n", Msg::GetCommSize() > 1 ? 4 : 3);
  fprintf(f, "%d\n 3\n %d\n", step, COUNT);
  if (Msg::GetCommSize()>1)
    fprintf(f, "%d\n", Msg::GetCommRank());
  for (int i=0; i<_groups->getNbElementGroups(); i++){
    dgGroupOfElements *group = _groups->getElementGroup(i);
    int repeatorder0 = group->getElement(0)->getNumVertices();
    for (size_t iElement=0 ; iElement<group->getNbElements(); ++iElement) {
      MElement *e = group->getElement(iElement);
      int num = e->getNum();
      fullMatrix<double> sol;
      getGroupProxy(i).getBlockProxy(iElement, sol);
      if (sol.size1() == 1)
        fprintf(f, "%d %d",num,repeatorder0);
      else
        fprintf(f, "%d %d",num,sol.size1());
      for (int k=0; k<sol.size1(); ++k) {
        fprintf(f, " %.16E ", sol(k,0));
        fprintf(f, " %.16E ", sol(k,1));
        fprintf(f, " %.16E ", sol.size2() == 2 ? sol(k,1) : 0.);
        if (sol.size1() == 1)
          for (int j = 1; j < repeatorder0; ++j) {
            fprintf(f, " %.16E ", sol(k,0));
            fprintf(f, " %.16E ", sol(k,1));
            fprintf(f, " %.16E ", sol.size2() == 2 ? sol(k,1) : 0.);
          }
      }
      fprintf(f,"\n");
    }
  }
  fprintf(f,"$EndElementNodeData\n");
  fclose(f);
}

void dgDofContainer::exportMshNodeData(const std::string name, double time, int step, std::string fieldName)
{
  for (int ICOMP = 0; ICOMP<_nbFields;++ICOMP){
    std::ostringstream name_oss, name_view;
    if(fieldName != "")
      name_view<<fieldName;
    else if (!_fieldsName[ICOMP].empty())
      name_view<<_fieldsName[ICOMP];
    else
      name_view<<"comp_"<<ICOMP;
    name_oss<<name<<"_COMP_"<<ICOMP<<".msh";
    if(Msg::GetCommSize()>1)
      name_oss<<"_"<<Msg::GetCommRank();
    FILE *f = fopen (name_oss.str().c_str(),"w");
    if(!f){
      Msg::Fatal("Unable to open export file '%s'", name.c_str());
    }

    // how to recover the total number of nodes ??? Doin' something ugly right now...
    int COUNT = 0;
    int id;
    std::vector<int> visited_nodes;
    for (int i = 0; i< _groups->getNbElementGroups(); i++) {
      dgGroupOfElements *g = _groups->getElementGroup (i);
      for (size_t j = 0; j < g->getNbElements(); j++){
        MElement *el = g->getElement(j);
        fullMatrix<double> sol(getGroupProxy(i), j*_nbFields,_nbFields);
        for (int k=0;k<el->getNumVertices();k++){
          id = el->getVertex(k)->getIndex();
          std::vector<int>::iterator itfind = find(visited_nodes.begin(),visited_nodes.end(),id);
          if (itfind==visited_nodes.end()){
            COUNT++;
            visited_nodes.push_back(id);
          }
        }
      }
    }


    fprintf(f,"$MeshFormat\n2.1 0 8\n$EndMeshFormat\n");
    fprintf(f,"$NodeData\n");
    fprintf(f,"1\n");
    fprintf(f,"\"%s\"\n",name_view.str().c_str());
    fprintf(f,"1\n");
    fprintf(f,"%f\n", time);
    fprintf(f,"%d\n", Msg::GetCommSize() > 1 ? 4 : 3);
    fprintf(f,"%d\n1\n%d\n", step, COUNT);
    if(Msg::GetCommSize() > 1) fprintf(f,"%d\n", Msg::GetCommRank());


    // for each element in each group: write nodal solution if not done yet...
    visited_nodes.clear();
    for (int i = 0; i< _groups->getNbElementGroups(); i++) {
      dgGroupOfElements *g = _groups->getElementGroup (i);
      for (size_t j = 0; j < g->getNbElements(); j++){
        MElement *el = g->getElement(j);
        fullMatrix<double> sol(getGroupProxy(i), j*_nbFields,_nbFields);
        for (int k=0;k<el->getNumVertices();k++){
          id = el->getVertex(k)->getIndex();
          std::vector<int>::iterator itfind = find(visited_nodes.begin(),visited_nodes.end(),id);
          if (itfind==visited_nodes.end()){
            visited_nodes.push_back(id);
            fprintf(f,"%d %.16E\n",el->getVertex(k)->getIndex(), sol(k,ICOMP));
          }
        }
      }
    }
    fprintf(f,"$EndNodeData\n");
    fclose(f);
  }
}

void dgDofContainer::minmax(fullMatrix<double>& minimum, fullMatrix<double>& maximum)
{
  minimum.resize(_nbFields,1);
  maximum.resize(_nbFields,1);
  minimum.setAll(DBL_MAX);
  maximum.setAll(-DBL_MAX);
  for (int iG = 0; iG < _groups->getNbElementGroups(); iG++) {
    const fullMatrix<double> data = getGroupProxy(iG);
    for (int col = 0; col < data.size2(); col++) {
      for (int row = 0; row < data.size1(); row++) {
        int iField = col % _nbFields;
        if ( maximum(iField, 0 ) < data(row,col) )
          maximum(iField, 0 ) = data(row,col);
        else if ( minimum(iField, 0 ) > data(row,col) )
          minimum(iField, 0 ) = data(row,col);
      }
    }
  }
  #ifdef HAVE_MPI
  if (Msg::GetCommSize() > 1) {
    fullMatrix<double> rmin = minimum;
    fullMatrix<double> rmax = maximum;
    MPI_Allreduce(&rmin(0, 0), &minimum(0, 0), _nbFields, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
    MPI_Allreduce(&rmax(0, 0), &maximum(0, 0), _nbFields, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
  }
  #endif
}



//TODO maybe placing this in the GMSH OS.h stuff (with <io.h> and <sys/stat.h>)
static void create_missing_dir(const std::string fullPath) {
  size_t lastp = fullPath.find_last_of('/'); //TODO ?? win ==> backslash ??
  if(lastp == std::string::npos) return;
  std::string dirname = std::string(fullPath, 0, lastp);
  size_t cur=0;
  while(cur!=std::string::npos){
    cur=dirname.find("/",cur+1);
#if defined (WIN32)&& !defined(__CYGWIN__)
    mkdir(dirname.substr(0,cur).c_str());
#else
    mkdir(dirname.substr(0,cur).c_str(),S_IRWXG|S_IRWXU);
#endif
  }
}

static void exportVtk_pvd(const std::string path_pvd,const std::string name_pvtu, const double time) {
  //Main VTK file (pvd->pvtu->vtu)
  if(time==0 || StatFile(path_pvd) != 0 ) {
    FILE *f_pvd = fopen (path_pvd.c_str(),"w");
    if(!f_pvd){
      Msg::Fatal("Unable to open export file '%s'", path_pvd.c_str());
    }
    fprintf(f_pvd, "<?xml version=\"1.0\"?>\n");
    fprintf(f_pvd, "<VTKFile type=\"Collection\" version=\"0.1\" ");
    fprintf(f_pvd, "byte_order=\"%sEndian\">\n", "Little");// : "Big");
    fprintf(f_pvd, "  <Collection>\n");
    fprintf(f_pvd, "    <DataSet timestep=\"%.0f\" group=\"\" part=\"0\" file=\"%s\"/>\n",
                              time, name_pvtu.c_str());
    fprintf(f_pvd, "  </Collection>\n");
    fprintf(f_pvd, "</VTKFile>\n");
    fclose(f_pvd);
  }
  else
  {
    FILE *f_pvd = fopen (path_pvd.c_str(),"r+");
    fseek(f_pvd, -27, SEEK_END);
    if(!f_pvd){
      Msg::Fatal("Unable to open export file '%s'", path_pvd.c_str());
    }
    fprintf(f_pvd, "    <DataSet timestep=\"%.0f\" group=\"\" part=\"0\" file=\"%s\"/>\n",
                              time, name_pvtu.c_str());
    fprintf(f_pvd, "  </Collection>\n");
    fprintf(f_pvd, "</VTKFile>\n");
    fclose(f_pvd);
  }
}

static void exportVtk_pvtu(const std::string path_pvtu, const std::string name_vtu,
			   const int nbFields, const int *nComps, std::string *fieldsNames, int high_order_add, int high_order_coord) {
  FILE *f_pvtu = fopen (path_pvtu.c_str(), "w");
  if(!f_pvtu){
    Msg::Fatal("Unable to open export file '%s'", path_pvtu.c_str());
  }

  std::stringstream scalarnames;
  std::stringstream vectornames;
  std::stringstream tensornames;
  scalarnames << "Scalar=\" ";
  vectornames << "Vectors=\" ";
  tensornames << "Tensors=\" ";
  for (int iField=0; iField<nbFields; iField++){
    std::stringstream *stream;
    switch (nComps[iField]){
    case 2: case 3:
      stream=&vectornames;
      break;
    case 4: case 9:
      stream=&tensornames;
      break;
    default:
      stream=&scalarnames;
      break;
    }
    (*stream) << fieldsNames[iField] << ",";
  }

  fprintf(f_pvtu, "<?xml version=\"1.0\"?>\n");
  fprintf(f_pvtu, "<VTKFile type=\"PUnstructuredGrid\" version=\"0.1\" ");
  fprintf(f_pvtu, "byte_order=\"%sEndian\">\n", "Little"); // : "Big");
  fprintf(f_pvtu, "  <PUnstructuredGrid GhostLevel=\"0\">\n");
  fprintf(f_pvtu, "    <PPointData %s \" %s \" %s \">\n",
	  scalarnames.str().substr(0,scalarnames.str().length()-1).c_str(),
	  vectornames.str().substr(0,vectornames.str().length()-1).c_str(),
	  tensornames.str().substr(0,tensornames.str().length()-1).c_str()
	  );
  for (int iField=0; iField<nbFields; iField++)
    fprintf(f_pvtu, "      <PDataArray type=\"Float64\" Name=\"%s\" NumberOfComponents=\"%d\"/>\n",
	    fieldsNames[iField].c_str(), nComps[iField]);
  fprintf(f_pvtu, "    </PPointData>\n");
  fprintf(f_pvtu, "    <PPoints>\n");
  fprintf(f_pvtu, "      <PDataArray type=\"Float64\" NumberOfComponents=\"%d\"/>\n", 3);
  fprintf(f_pvtu, "    </PPoints>\n");
  if (high_order_add>0){
    fprintf(f_pvtu, "      <PCellData>\n");
    for (int iField=0; iField<nbFields; iField++){
      for (int i=0; i<high_order_add; i++){
	fprintf(f_pvtu, "        <DataArray type=\"Float64\" Name=\"%s_HOsol_%i\" NumberOfComponents=\"%d\"/>\n",
		fieldsNames[iField].c_str(),i, nComps[iField]);
      }
    }
    if (high_order_coord){
      for (int i=0; i<high_order_add; i++){
	fprintf(f_pvtu, "        <DataArray type=\"Float64\" Name=\"HOcoord_%i\" NumberOfComponents=\"%d\"/>\n",
		i, 3);
      }
    }
    fprintf(f_pvtu, "      </PCellData>\n");
  }
  for(int i = 0; i < Msg::GetCommSize(); ++i)
    fprintf(f_pvtu, "    <Piece Source=\"%s%d.vtu\"/>\n", name_vtu.c_str(), i);
  fprintf(f_pvtu, "  </PUnstructuredGrid>\n");
  fprintf(f_pvtu, "</VTKFile>");
  fclose(f_pvtu);
}

/*Build the header of the data-container file .VTU (one per time step, per field and proc)*/
/*Return an array of 7 fpos_t to fill after having put the whole data                     */
static fpos_t * exportVtk_vtuHeader(const std::string path_vtu, const int nbFields, const int* nComps, std::string *fieldsNames, int high_order_add, bool high_order_coord) {
  FILE *f_vtu = fopen (path_vtu.c_str(),"w");
  if(!f_vtu){
    Msg::Fatal("Unable to open export file '%s'", path_vtu.c_str());
  }

  std::stringstream scalarnames;
  std::stringstream vectornames;
  std::stringstream tensornames;
  scalarnames << "Scalar=\" ";
  vectornames << "Vectors=\" ";
  tensornames << "Tensors=\" ";
  for (int iField=0; iField<nbFields; iField++){
    std::stringstream *stream;
    switch (nComps[iField]){
    case 2: case 3:
      stream=&vectornames;
      break;
    case 4: case 9:
      stream=&tensornames;
      break;
    default:
      stream=&scalarnames;
      break;
    }
    (*stream) << fieldsNames[iField] << ",";
  }


  int fpos_size=nbFields*(1+high_order_add)+4+(high_order_coord?high_order_add:0)+3;
  fpos_t *list_fpos = new fpos_t[fpos_size];
  int iPos=0;
  fprintf(f_vtu, "<?xml version=\"1.0\"?>\n");
  fprintf(f_vtu, "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" ");
  fprintf(f_vtu, "byte_order=\"%sEndian\"","Little"); // : "Big");
  fprintf(f_vtu, ">\n");
  fprintf(f_vtu, "  <UnstructuredGrid>\n");
  fprintf(f_vtu, "    <Piece NumberOfPoints=");
  fgetpos(f_vtu,&list_fpos[iPos++]); // ! make sure to have spaces to replace by the text of list_fpos !
  fprintf(f_vtu, "                            NumberOfCells=");
  fgetpos(f_vtu,&list_fpos[iPos++]);
  fprintf(f_vtu, "                            >\n");
  fprintf(f_vtu, "      <PointData %s \" %s \" %s \">\n",
	  scalarnames.str().substr(0,scalarnames.str().length()-1).c_str(),
	  vectornames.str().substr(0,vectornames.str().length()-1).c_str(),
	  tensornames.str().substr(0,tensornames.str().length()-1).c_str()
	  );
  for (int iField=0; iField<nbFields; iField++){
    fprintf(f_vtu, "        <DataArray type=\"Float64\" Name=\"%s\" NumberOfComponents=\"%d\" format=\"appended\" offset=",
	    fieldsNames[iField].c_str(), nComps[iField]);
    fgetpos(f_vtu,&list_fpos[iPos++]);
    fprintf(f_vtu, "                           />\n");
  }
  fprintf(f_vtu, "      </PointData>\n");
  fprintf(f_vtu, "      <Points>\n");
  fprintf(f_vtu, "        <DataArray type=\"Float64\" NumberOfComponents=\"3\" format=\"appended\" offset=");
  fgetpos(f_vtu,&list_fpos[iPos++]);
  fprintf(f_vtu, "                           />\n");
  fprintf(f_vtu, "      </Points>\n");
  fprintf(f_vtu, "      <Cells>\n");
  fprintf(f_vtu, "        <DataArray type=\"Int32\" Name=\"connectivity\" format=\"appended\" offset=");
  fgetpos(f_vtu,&list_fpos[iPos++]);
  fprintf(f_vtu, "                           />\n");
  fprintf(f_vtu, "        <DataArray type=\"Int32\" Name=\"offsets\"  format=\"appended\" offset=");
  fgetpos(f_vtu,&list_fpos[iPos++]);
  fprintf(f_vtu, "                           />\n");
  fprintf(f_vtu, "        <DataArray type=\"Int32\" Name=\"types\" format=\"appended\" offset=");
  fgetpos(f_vtu,&list_fpos[iPos++]);
  fprintf(f_vtu, "                           />\n");
  fprintf(f_vtu, "      </Cells>\n");

  if (high_order_add>0){
    fprintf(f_vtu, "      <CellData>\n");
    for (int iField=0; iField<nbFields; iField++){
      for (int i=0; i<high_order_add; i++){
	fprintf(f_vtu, "        <DataArray type=\"Float64\" Name=\"%s_HOsol_%i\" NumberOfComponents=\"%d\" format=\"appended\" offset=",
		fieldsNames[iField].c_str(),i, nComps[iField]);
	fgetpos(f_vtu,&list_fpos[iPos++]);
	fprintf(f_vtu, "                           />\n");
      }
    }
    if (high_order_coord){
      for (int i=0; i<high_order_add; i++){
	fprintf(f_vtu, "        <DataArray type=\"Float64\" Name=\"HOcoord_%i\" NumberOfComponents=\"%d\" format=\"appended\" offset=",
		i, 3);
	fgetpos(f_vtu,&list_fpos[iPos++]);
	fprintf(f_vtu, "                           />\n");
      }
    }
    fprintf(f_vtu, "      </CellData>\n");
  }
  fprintf(f_vtu, "    </Piece>\n");
  fprintf(f_vtu, "  </UnstructuredGrid>\n");
  fprintf(f_vtu, "  <AppendedData encoding=\"raw\">\n_");
  fclose(f_vtu);
  return list_fpos;
}

static int convertVtkTypeLowOrder(int highOrderType){
  if (highOrderType==21) return 3;
  if (highOrderType==22) return 5;
  if (highOrderType==23) return 9;
  if (highOrderType==25) return 12;
  return highOrderType;
}

void dgDofContainer::exportVtk(const std::string name, double time, int step, std::string exportname)
{
  exportFunctionVtk (getFunction(), name, time, step, exportname);
}

void dgDofContainer::exportFunctionVtk (function * fun, const std::string name, double time, int step, std::string exportname, std::vector<int> nComps_, std::vector<std::string> fieldsNames_,function * coordFun) {
  int nbFields;
  if (nComps_.size()==0)
    nbFields=fun->getNbCol();
  else
    nbFields=nComps_.size();

  int *nComps = new int[nbFields];
  std::string *fieldsNames = new std::string[nbFields];

  if (nComps_.size()==0){
    for (int i=0; i<nbFields; i++)
      nComps[i]=1;
  }else{
    for (int i=0; i<nbFields; i++)
      nComps[i]=nComps_[i];
  }
  int nCompsMax=0;
  std::vector<int> idxField(nbFields);
  idxField[0]=0;
  for (int i=0; i<nbFields; i++){
    nCompsMax=std::max(nCompsMax,nComps[i]);
    if (i>0)
      idxField[i]=idxField[i-1]+nComps[i-1];
  }
  if (fieldsNames_.size()==0){
    for (int i=0; i<nbFields; i++){
      std::stringstream fname;
      fname << exportname << "_" << i;
      fieldsNames[i]=fname.str();
    }
  }else{
    for (int i=0; i<nbFields; i++)
      fieldsNames[i]=fieldsNames_[i];
  }
  std::vector<double> data(std::max(nCompsMax,3));


  //set the different paths
  // PATH/RUN_FIELD_comp#.pvd  (or MAIN/FIELD.pvd if one field)
  // PATH/RUN_FIELD_PV/comp#_#it#.pvtu  (remove comp if one field)
  // PATH/RUN_FIELD_PV/vtu_files/comp#_#it#_#proc#.vtu   (remove comp if one field)

  std::ostringstream name_pvtu, name_vtu, path_pvd, path_pvtu, path_vtu, name_num, name_view;
  std::string path;
    size_t lastp = name.find_last_of('/');
    if(lastp == std::string::npos) { //in the current dir
      path = "";
      name_view << name;
    } else {
      path = std::string(name, 0, lastp + 1);
      name_view << std::string(name, lastp + 1);
    }
    if(exportname == "") {
      name_view << "_function";
      path_pvd << name << "_fun";
    }
    else {
      name_view << "_" << exportname;
      path_pvd << name << "_" << exportname;
    }
    path_pvd << ".pvd";
    name_num << std::setfill('0') << std::setw(nbFields+4) << step;

    name_pvtu << name_view.str() << "_PV/" << name_num.str() << ".pvtu";
    name_vtu  << "vtu_files/" << name_num.str() << "_" ;
    path_pvtu << path << name_pvtu.str();
    path_vtu  << path << name_view.str() << "_PV/" << name_vtu.str() << Msg::GetCommRank() << ".vtu";



    //Writes separate serial UnstructuredGrid files (.vtu)
    // which contain the data of each process

    //HEADER
    int max_vert=0;
    int min_prim_vert=INT_MAX;
    bool dofAsCellData=false;
    bool high_order_coord=true;
    for (int i = 0; i < _groups->getNbElementGroups(); i++) {
      dgGroupOfElements *group = _groups->getElementGroup(i);
      if (group->getOrder()>1)
	dofAsCellData=true;
      max_vert=std::max(group->getNbNodes(),max_vert);
      if (group->getElement(i)->getNumVertices()!=group->getNbNodes() && !coordFun)
	high_order_coord=false;
      min_prim_vert = std::min(min_prim_vert,group->getElement(0)->getNumPrimaryVertices());
    }
    int high_order_add=dofAsCellData?((max_vert-min_prim_vert)):0; //High order rep minus the existing p1

    //Create missing directories and main xml files  (pvd->pvtu->vtu)
    if(Msg::GetCommRank() == 0) {
      create_missing_dir(path_pvd.str());
      create_missing_dir(path_pvtu.str());
      create_missing_dir(path_vtu.str());
      //Main VTK file (lists of time series: '.pvd')
      exportVtk_pvd(path_pvd.str(), name_pvtu.str(), time);
      //Writes the Parallel PUnstructuredGrid file (list of procs: '.pvtu')
      exportVtk_pvtu(path_pvtu.str(), name_vtu.str(),  nbFields, nComps, fieldsNames, high_order_add, high_order_coord);
    }
    Msg::Barrier(); //The directory must be created before others procs can export


    fpos_t *list_fpos = exportVtk_vtuHeader(path_vtu.str(),  nbFields, nComps, fieldsNames, high_order_add, high_order_coord);

    //BODY (binary data)
    int offsetsize=nbFields*(1+high_order_add)+4+(high_order_coord?high_order_add:0);
    std::vector<unsigned int> offset(offsetsize); //first data_size, secondly offsets
    std::vector<std::stringstream*> buffer(offsetsize);
    for(int i = 0; i < offsetsize; i++){
      offset[i]=0;
      buffer[i] = new std::stringstream(std::ios::out|std::ios::binary);
    }
    int offst = 0, connect = 0;

    for (int iG = 0; iG < _groups->getNbElementGroups(); iG++){
      dgGroupOfElements *group = _groups->getElementGroup(iG);
      int cell_type = convertVtkTypeLowOrder(group->getElement(0)->getTypeForVTK());
      if(!cell_type) break;
      int p1nodes_in_cell = group->getElement(0)->getNumPrimaryVertices();
      int cells_in_group = 0;

      dataCacheMap cacheMap(dataCacheMap::NODE_MODE, _groups);
      cacheMap.setSolutionFunction(getFunction(), getFunctionGradient());
      dataCacheDouble &value = *cacheMap.get(fun);
      dataCacheDouble *valueCoord=NULL;
      if (coordFun)
	valueCoord = cacheMap.get(coordFun);
      cacheMap.setGroup(group);

      //    Data
      for (size_t iElement = 0; iElement < group->getNbElements(); ++iElement) {
	MElement *e = group->getElement(iElement);
	cacheMap.setElement(iElement);
	const fullMatrix<double> &val = value.get();
	for(int k = 0; k < val.size1(); k++) {
	  // Data
	  for (int iField=0; iField<nbFields; iField++){
	    for(int j = 0; j < nComps[iField]; j++){
	      data[j] = val(k, idxField[iField]+j);
	    }
	    if (dofAsCellData && k>=e->getNumPrimaryVertices()){
	      buffer[k-e->getNumPrimaryVertices()+nbFields+4+iField*(high_order_add)]->write((char *)(&data[0]), nComps[iField] * sizeof(double)); //one data field for each node except for p1 dof
	    }else{
	      buffer[iField]->write((char *)(&data[0]), nComps[iField] * sizeof(double));
	    }
	  }
	}
	// Coordinates
	int ncoord=high_order_coord?group->getNbNodes():e->getNumPrimaryVertices();
	for(int k = 0; k <ncoord; k++) {
	  const fullMatrix<double> *valCoord;
	  double xyz[3];
	  if (coordFun){
	    valCoord = &valueCoord->get();
	    for(int j = 0; j < 3; j++){
	      data[j] = (*valCoord)(k, j);
	    }
	  }else{
	    xyz[0] = e->getVertex(k)->x();
	    xyz[1] = e->getVertex(k)->y();
	    xyz[2] = e->getVertex(k)->z();
	  }
	  if (dofAsCellData && k>=e->getNumPrimaryVertices()){
	    if (!coordFun){
	      buffer[k-e->getNumPrimaryVertices()+nbFields*(1+high_order_add)+4]->write((char *)(xyz), 3 * sizeof(double));
	    }else{
	      buffer[k-e->getNumPrimaryVertices()+nbFields*(1+high_order_add)+4]->write((char *)(&data[0]), 3 * sizeof(double));
	    }
	  }else{
	    if (!coordFun){
	      buffer[nbFields]->write((char *)(xyz), 3 * sizeof(double));
	    }else{
	      buffer[nbFields]->write((char *)(&data[0]), 3 * sizeof(double));
	    }
	  }
	}
	cells_in_group++; //could be diff to the total size of the group if conditional statement (like printing one tag)
      }
      for (int iField=0; iField<nbFields; iField++){
	offset[iField] += cells_in_group * p1nodes_in_cell * nComps[iField] * sizeof(double);
	for (int i=0; i<high_order_add; i++){
	  offset[i+nbFields+4+iField*(high_order_add)] += cells_in_group * nComps[iField] * sizeof(double);
	}
      }

      offset[nbFields] += cells_in_group * p1nodes_in_cell * 3 * sizeof(double);
      if (high_order_coord){
	for (int i=0; i<high_order_add; i++){
	  offset[i+nbFields*(1+high_order_add)+4] += cells_in_group * 3 * sizeof(double);
	}
      }
//    Connectivity (0 to Nlastpoint-1)  => simple bcs the elmts are discontinuous
      for(int i = 0; i < cells_in_group * p1nodes_in_cell; i++) {
        buffer[nbFields+1]->write((char *)(&connect), sizeof(int));
        connect ++;
      }
      offset[nbFields+1] += cells_in_group * p1nodes_in_cell * sizeof(int);

//    Offsets (where is the beginning of element "x" located in the index)
      for(int i = 0; i < cells_in_group; i++) { //MAYBE BUG WITH THE INTERVALS !! (dont put 0; work for one comp, but not several?)
        offst += p1nodes_in_cell;
        buffer[nbFields+2]->write((char *)(&offst), sizeof(int));
      }
      offset[nbFields+2] += cells_in_group * sizeof(int);

//    Types
      for(int i = 0; i < cells_in_group; i++)
        buffer[nbFields+3]->write((char *)(&cell_type), sizeof(int));
      offset[nbFields+3] += cells_in_group * sizeof(int);

    } //for each Group

    //write all in the file //buffers permits use of ZLIB to compress all data
    std::fstream fstrSol(path_vtu.str().c_str(),std::ios::out|std::ios::binary|std::ios::app);
    for(int i = 0; i < offsetsize; i++) {
      fstrSol.write((char *)(&offset[i]), sizeof(unsigned int));
      fstrSol.write(buffer[i]->str().c_str(), offset[i]);
    }
    fstrSol.close();

    int total_nodes = offset[nbFields+1] / sizeof(int);
    int total_cells = offset[nbFields+2] / sizeof(int);

    offset[0] += sizeof(unsigned int);
    for(int i = 1; i < offsetsize; i++) {
      offset[i] += offset[i - 1] + sizeof(unsigned int);
    }

    //FOOTER
    FILE *f_vtu = fopen (path_vtu.str().c_str(),"a+");
    fprintf(f_vtu, "\n  </AppendedData>\n");
    fprintf(f_vtu, "</VTKFile>");
    fclose(f_vtu);

    //insert offset placements of the data in XML header
    f_vtu = fopen(path_vtu.str().c_str(), "r+");
    int fPos=0;
    fsetpos(f_vtu, &list_fpos[fPos++]);
    fprintf(f_vtu, "\"%d\"", total_nodes);
    fsetpos(f_vtu, &list_fpos[fPos++]);
    fprintf(f_vtu, "\"%d\"", total_cells);
    fsetpos(f_vtu, &list_fpos[fPos++]);
    fprintf(f_vtu, "\"%d\"", 0);
    for (int i=0; i<offsetsize-1; i++){
      fsetpos(f_vtu, &list_fpos[fPos++]);
      fprintf(f_vtu, "\"%lu\"", (unsigned long int) offset[i]);
    }
    fclose(f_vtu);

    for(int i = 0; i < offsetsize; i++)
      delete buffer[i];
    delete [] list_fpos;

  delete []nComps;
  delete []fieldsNames;
}

void dgDofContainer::bezierCrop(double min, double max) {
  fullMatrix<double> lag, bez;
  for (int i = 0; i < _groups->getNbElementGroups(); i++) {
    dgGroupOfElements *group = _groups->getElementGroup(i);
    dgFullMatrix<double> &groupLag = getGroupProxy(i);
    const bezierBasis *bezier = BasisFactory::getBezierBasis(group->getFunctionSpace().type);
    dgFullMatrix<double> groupBez(group->getNbNodes(), _nbFields, group->getNbElements(), false);
    bezier->matrixLag2Bez.mult(groupLag, groupBez);
    for (size_t j = 0; j < group->getNbElements(); j++) {
      groupBez.getBlockProxy(j, bez);
      for (int k = 0; k < group->getNbNodes(); k++){
        bez(k,0) = std::min(max, std::max(min, bez(k, 0)));
      }
    }
    bezier->matrixBez2Lag.mult(groupBez, groupLag);
  }
}

void dgDofContainer::bezierCrop(const function *minF, const function *maxF) {
  fullMatrix<double> lag, bez;
  dataCacheMap cacheMap(dataCacheMap::NODE_MODE, _groups);
  for (int i = 0; i < _groups->getNbElementGroups(); i++) {
    dgGroupOfElements *group = _groups->getElementGroup(i);
    dgFullMatrix<double> &groupLag = getGroupProxy(i);
    const bezierBasis *bezier = BasisFactory::getBezierBasis(group->getElement(0)->getTypeForMSH());
    dgFullMatrix<double> groupBez(group->getNbNodes(), _nbFields, group->getNbElements(), false);
    bezier->matrixLag2Bez.mult(groupLag, groupBez);
    dataCacheDouble &minValues = *cacheMap.get(minF);
    dataCacheDouble &maxValues = *cacheMap.get(maxF);
    cacheMap.setGroup(group);
    for (size_t j = 0; j < group->getNbElements(); j++) {
      cacheMap.setElement(j);
      groupBez.getBlockProxy(j, bez);
      const fullMatrix<double> &minMatrix = minValues.get();
      const fullMatrix<double> &maxMatrix = maxValues.get();
      for (int k = 0; k < group->getNbNodes(); k++){
        bez(k,0) = std::min(maxMatrix(k,0), std::max(minMatrix(k,0), bez(k, 0)));
      }
    }
    bezier->matrixBez2Lag.mult(groupBez, groupLag);
  }
}

dgIdxExporter::dgIdxExporter(const dgDofContainer *dof, std::string fileName, bool exportAsVector): _dof(dof), _fileName(fileName), _iter(std::numeric_limits<int>::max()){
  _exportAsVector = exportAsVector;
  size_t lastp = _fileName.find_last_of('/');
  size_t endp = _fileName.size();
  if(lastp != std::string::npos){
    name_file = std::string(_fileName, lastp+1, endp);
    name_dir = std::string(_fileName, 0, lastp+1);
  }
  else{
    name_file=_fileName;
    name_dir="";
  }
  if (Msg::GetCommRank() == 0){
    std::string idxname =name_dir+name_file+"/"+name_file+".idx";
    create_missing_dir(idxname);
    FILE *f = fopen (idxname.c_str(),"w");
    fprintf(f,"// nb_fields\t%d \n// nb_part  \t%d \n\n", _dof->getNbFields(), Msg::GetCommSize());
    fclose(f);
  }
}

void dgIdxExporter::exportIdx(){
  if (Msg::GetCommRank() == 0)
    create_missing_dir(name_dir+name_file+"/"+name_file);
  Msg::Barrier();
  _dof->exportMsh((name_dir+name_file+"/"+name_file).c_str());
  if(Msg::GetCommRank() == 0){
    std::string writeMode;
    FILE *f = fopen ((name_dir+name_file+"/"+name_file+".idx").c_str(),"a");
    if (_dof->getNbFields() > 1){
      for (int iField = 0; iField < _dof->getNbFields(); iField++){
        std::ostringstream name_comp_oss;
        std::string name_comp_idx;
        //name_oss<<name_file<<"_COMP_"<<iField<<".msh";
        name_comp_oss<<name_file<<"_COMP_"<<iField;
        name_comp_idx=name_comp_oss.str()+".idx";
        FILE *e;
        e = fopen ((name_dir+name_file+"/"+name_comp_idx).c_str(), "w");
        fprintf(f, "// field %d\n", iField);
        fprintf(f,"Merge \"%s\";\n", (name_comp_idx).c_str());
        fprintf(e, "// field %d\n", iField);
        name_comp_oss<<".msh";
        for(int iP = 0; iP < Msg::GetCommSize(); iP++){
          std::ostringstream name_part_oss;
          name_part_oss<<name_comp_oss.str();
          if(Msg::GetCommSize() > 1){
            name_part_oss<<"_"<<iP;
          }
          fprintf(e,"Merge \"%s\";\n", (name_part_oss.str()).c_str());
        }
        fclose(e);
      }
    }
    else{
      for(int iP = 0; iP < Msg::GetCommSize(); iP++){
        std::ostringstream name_part_oss, name_comp_oss;
        name_comp_oss<<name_file<<"_COMP_0.msh";
        name_part_oss<<name_comp_oss.str();
        if(Msg::GetCommSize() > 1){
          name_part_oss<<"_"<<iP;
        }
        fprintf(f,"Merge \"%s\";\n", (name_part_oss.str()).c_str());
      }
    }
    fclose(f);
  }
}

void dgIdxExporter::exportIdx(int iter, double t){
  std::string name_iter, writeMode;
  writeMode = (iter > _iter ? "a" : "w");
  char citer[7];
  sprintf(citer, "%06d", iter);
  name_iter=name_file+"-"+citer;
  std::string itername =name_dir+name_file+"/"+name_iter+"/"+name_iter;
  if (Msg::GetCommRank() == 0)
    create_missing_dir(itername);
  Msg::Barrier();
  if (_exportAsVector)
    _dof->exportMshVector(itername.c_str(), t, iter, name_file);
  else
    _dof->exportMsh(itername.c_str(), t, iter, name_file);
  if(Msg::GetCommRank()==0){
    FILE *f = fopen ((name_dir+name_file+"/"+name_file+".idx").c_str(),"a");//writeMode.c_str());
    if (_exportAsVector) {
      std::ostringstream name_iter_comp_oss, name_comp_oss;
      name_iter_comp_oss<<name_iter;
      FILE *g = fopen ((name_dir+name_file+"/"+name_iter_comp_oss.str()+".idx").c_str(),"w");
      if(Msg::GetCommSize() == 1){
        fprintf(g, "Merge \"%s.msh\";\n", (name_iter+"/"+name_iter_comp_oss.str()).c_str());
      }
      else{
        for(int iP = 0; iP < Msg::GetCommSize(); iP++){
          fprintf(g, "Merge \"%s.msh_%d\";\n", (name_iter+"/"+name_iter_comp_oss.str()).c_str(), iP);
        }
      }
      fclose(g);
      fprintf(f, "Merge \"%s\";\n", (name_iter_comp_oss.str()+".idx").c_str());
    }
    else if (_dof->getNbFields() > 1 ){
      FILE *h = fopen ((name_dir+name_file+"/"+name_iter+".idx").c_str(),"w");
      fprintf(h,"// nb_fields\t%d \n// nb_part  \t%d \n\n", _dof->getNbFields(), Msg::GetCommSize());
      for (int iField = 0; iField < _dof->getNbFields(); iField++){
        std::ostringstream name_oss, name_iter_comp_oss, name_comp_oss;
        name_oss<<name_iter<<"_COMP_"<<iField<<".msh";
        name_iter_comp_oss<<name_iter<<"_COMP_"<<iField;
        name_comp_oss<<name_file<<"_COMP_"<<iField;
        FILE *g = fopen ((name_dir+name_file+"/"+name_iter_comp_oss.str()+".idx").c_str(),"w");
        FILE *e = fopen ((name_dir+name_file+"/"+name_comp_oss.str()+".idx").c_str(),writeMode.c_str());
        fprintf(f, "// field %d\n", iField);
        fprintf(g, "// field %d\n", iField);
        fprintf(e, "// field %d\n", iField);
        fprintf(h, "// field %d\n", iField);
        if(Msg::GetCommSize() == 1){
          fprintf(g, "Merge \"%s\";\n", (name_iter+"/"+name_oss.str()).c_str());
        }
        else{
          for(int iP = 0; iP < Msg::GetCommSize(); iP++){
            fprintf(g, "Merge \"%s_%d\";\n", (name_iter+"/"+name_oss.str()).c_str(), iP);
          }
        }
        fclose(g);
        fprintf(e, "Merge \"%s\";\n", (name_iter_comp_oss.str()+".idx").c_str());
        fclose(e);
        fprintf(f, "Merge \"%s\";\n", (name_iter_comp_oss.str()+".idx").c_str());
        fprintf(h, "Merge \"%s\";\n", (name_iter_comp_oss.str()+".idx").c_str());
      }
      fclose(h);
    }
    else{
      std::ostringstream name_oss, name_iter_oss;
      name_oss<<name_iter<<"_COMP_0.msh";
      name_iter_oss<<name_iter;
      FILE *g = fopen ((name_dir+name_file+"/"+name_iter_oss.str()+".idx").c_str(),"w");
      fprintf(g,"// nb_fields\t%d \n// nb_part  \t%d \n\n", _dof->getNbFields(), Msg::GetCommSize());
      if(Msg::GetCommSize() == 1){
        fprintf(g, "Merge \"%s\";\n", (name_iter+"/"+name_oss.str()).c_str());
      }
      else{
        for(int iP = 0; iP < Msg::GetCommSize(); iP++){
          fprintf(g, "Merge \"%s_%d\";\n", (name_iter+"/"+name_oss.str()).c_str(), iP);
        }
      }
      fclose(g);
      fprintf(f, "Merge \"%s\";\n", (name_iter_oss.str()+".idx").c_str());
    }
    fclose(f);
  }
  _iter = iter;
}

void dgDofContainer::_getAtIntegrationPoints(const dgGroupOfElements &group, const dgIntegrationMatrices &intMatrices, fullMatrix<double> &v) const
{
  int nbIntegrationPoints = intMatrices.getNbIntegrationPoints();
  v.resize(nbIntegrationPoints, _nbFields * group.getNbElements(), false);
  const dgGroupCollection *groups = group.getGroupCollection();
  dgExtrusion *extrusion = groups->getExtrusion();
  if (extrusion && &extrusion->getGroups2d() == _groups)
    v.gemm(intMatrices.psi(), transposeElementField(get3dNodalValuesFrom2dDof(*extrusion, *this, group)), 1., 0.);
  else
    v.gemm(intMatrices.psi(), transposeElementField(getGroupProxy(&group)), 1., 0.);
  v.reshape(nbIntegrationPoints * group.getNbElements(), _nbFields);
}

void dgDofContainer::_getGradientAtIntegrationPoints(const dgGroupOfElements &group, const dgIntegrationMatrices &intMatrices, dgFullMatrix<double> &v) const
{
  const dgGroupCollection *groups = group.getGroupCollection();
  const dgExtrusion *extrusion = groups->getExtrusion();
  if (!extrusion || &extrusion->getGroups2d() != _groups)
    extrusion = NULL;
  const dgMeshJacobian &jac = groups->_mesh->getJacobian(intMatrices.getIntegrationOrder());
  int nbIntegrationPoints = intMatrices.getNbIntegrationPoints();
  int iGroup = groups->getElementGroupId(&group);
  dgFullMatrix<double> dFieldDXi(intMatrices.getNbIntegrationPoints() * group.getDimUVW(), _nbFields, group.getNbElements(), false);
  if (!extrusion)
    intMatrices.dPsi().mult(getGroupProxy(iGroup), dFieldDXi);
  else {
    intMatrices.dPsi().mult(get3dNodalValuesFrom2dDof(*extrusion, *this, group), dFieldDXi);
  }
  fullMatrix<double> dFieldDXiProxy, dFieldDXProxy;
  v.resize(nbIntegrationPoints, _nbFields * 3, group.getNbElements(), false);
  for (size_t iEl = 0; iEl < group.getNbElements(); ++iEl){
    dFieldDXi.getBlockProxy(iEl, dFieldDXiProxy);
    v.getBlockProxy(iEl, dFieldDXProxy);
    jac.multDXiDXElement(group.elementVectorId(), iEl, dFieldDXiProxy, dFieldDXProxy);
  }
  return;
  Msg::Fatal("groupCollection does not match dofContainer group collection");
}

void dgDofContainer::_getAtIntegrationPoints(const dgGroupOfFaces &faces, int iConnection, const dgIntegrationMatrices &intMatrices, fullMatrix<double> &v) const
{
  const dgGroupOfElements &group = faces.elementGroup(iConnection);
  const dgExtrusion *extrusion = group.getGroupCollection()->getExtrusion();
  if (!extrusion || &extrusion->getGroups2d() != _groups)
    extrusion = NULL;
  v.resize(intMatrices.integrationPoints().size1(), _nbFields * faces.size(), false);
  dgFullMatrix<double> dofInterface(faces.getNbNodes(), _nbFields, faces.size(), false);
  int iGroup = group.getGroupCollection()->getElementGroupId(&group);
  const dgFullMatrix<double> &proxyGroup = getGroupProxy(iGroup);
  fullMatrix<double> faceDof;
  for (unsigned int i = 0; i < faces.size(); i++) {
    dofInterface.getBlockProxy(i, faceDof);
    const std::vector<int> &closure = faces.closure(i, iConnection);
    if (extrusion) {
      get3dNodalValuesFrom2dDofFace(*extrusion, *this, iGroup, faces.elementId(i, iConnection), closure, faceDof);
    }
    else {
      for (int iField = 0; iField < _nbFields; iField++) {
        const int ip = faces.elementId(i, iConnection) * _nbFields + iField;
        for(size_t j = 0; j < closure.size(); j++) {
          faceDof(j, iField) = proxyGroup(closure[j], ip);
        }
      }
    }
  }
  intMatrices.psi().mult(transposeElementField(dofInterface), v);
  v.reshape(intMatrices.integrationPoints().size1() * faces.size(), _nbFields);
}

void dgDofContainer::_getGradientAtIntegrationPoints(const dgGroupOfFaces &faces, int iConnection, const dgIntegrationMatrices &intMatrices, dgFullMatrix<double> &v) const
{
  const dgGroupCollection *groups = faces.elementGroup(0).getGroupCollection();
  const dgExtrusion *extrusion = groups->getExtrusion();
  if (!extrusion || &extrusion->getGroups2d() != _groups)
    extrusion = NULL;
  const dgMeshJacobian &jac = groups->_mesh->getJacobian(intMatrices.getIntegrationOrder());
  const int nbQP = intMatrices.getNbIntegrationPoints();
  dgFullMatrix<double> dofInterface(intMatrices.dPsi().size2(), _nbFields, faces.size(), false);
  v.resize(nbQP, 3 * _nbFields, faces.size(), false);
  dgFullMatrix<double> dFieldDXi(nbQP * groups->getDim(), _nbFields, faces.size(), false);
  int iGroup = groups->getElementGroupId(&faces.elementGroup(iConnection));
  const dgFullMatrix<double> &proxyGroup = getGroupProxy(iGroup);
  fullMatrix<double> faceDof;
  for (unsigned int i = 0; i < faces.size(); i++) {
    const std::vector<int> &closure = faces.fullClosure(i, iConnection);
    dofInterface.getBlockProxy(i, faceDof);
    if (!extrusion) {
      for (int iField = 0; iField < _nbFields; iField++) {
        const int ip = faces.elementId(i, iConnection) * _nbFields + iField;
        for(size_t j = 0; j < closure.size(); j++) {
          faceDof(j, iField) = proxyGroup(closure[j], ip);
        }
      }
    }
    else {
      get3dNodalValuesFrom2dDofFace(*extrusion, *this, iGroup, faces.elementId(i, iConnection), faces.fullClosure(i, iConnection), faceDof);
    }
  }
  intMatrices.dPsi().mult(dofInterface, dFieldDXi);

  fullMatrix<double> dFieldDXiProxy, dFieldDXProxy;
  for (size_t iFace = 0; iFace < faces.size(); ++iFace) {
    dFieldDXi.getBlockProxy(iFace, dFieldDXiProxy);
    v.getBlockProxy(iFace, dFieldDXProxy);
    jac.multDXiDXInterface(faces.interfaceVectorId(), iConnection, faces.interfaceId(iFace), dFieldDXiProxy, dFieldDXProxy);
  }
}


