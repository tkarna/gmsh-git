%module dofContainer

%include std_vector.i
%include std_string.i

%import(module="dgpy.dgFunction") "function.h"
%import(module="dgpy.dgCommon") "dgCommon.i"

%{
  #undef HAVE_DLOPEN
  #include "dgConfig.h"
  #include "GmshConfig.h"
  #include "dgDofContainer.h"
%}

%apply double &OUTPUT { double &time }
%apply int &OUTPUT { int &step }
%apply std::string &OUTPUT { std::string &fieldName }
%include "dgDofContainer.h"

%template(VectorDgDofContainer) std::vector<dgDofContainer*, std::allocator<dgDofContainer*> >;

