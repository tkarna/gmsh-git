#ifndef _DG_DOF_CONTAINER_H_
#define _DG_DOF_CONTAINER_H_

#include <vector>
#include "dgFullMatrix.h"
#include "function.h"
#include "dgDofManager.h"
#include "dgGroupOfElements.h"

class dgIntegrationMatrices;

class dgDofContainer;
class dgIdxExporter{
  private:
    const dgDofContainer *_dof;
    std::string _fileName, name_file, name_dir;
    int _iter;
    bool _exportAsVector;

  public:
    dgIdxExporter(const dgDofContainer *dof, std::string fileName, bool _exporAsVector = false);
    void exportIdx();
    void exportIdx(int iter, double t);
};

class dgDofFunction : public function {
  dgDofContainer *_dof;
  public:
  inline const dgDofContainer *getDof()const {return _dof;}
  void setDof(dgDofContainer *dof);
  dgDofFunction(dgDofContainer *dof);
  dgDofFunction();
  void call(dataCacheMap *m, dataCacheDouble *d, fullMatrix<double> &sol);
};

class dgDofFunctionGradient:public function {
  dgDofContainer *_dof;
  public:
  inline const dgDofContainer *getDof()const {return _dof;}
  void setDof(dgDofContainer *dof);
  dgDofFunctionGradient(dgDofContainer *dof);
  dgDofFunctionGradient();
  void call(dataCacheMap *m, dataCacheDouble *d, fullMatrix<double> &sol);
};

/** The DofContainer class provides a vector containing the degrees of freedom */
class dgDofContainer 
{
friend class dgDofFunction;
friend class dgDofFunctionGradient;
private:
  typedef enum {
    SCATTER_NONE,
    SCATTER_CG,
    SCATTER_DG
  } scatterOperation;
  const dgGroupCollection *_groups;
  int _nbFields;
  scatterOperation _scatterOp;
  std::string *_fieldsName;
  fullVector<double> _data; // the full data itself
  fullVector<double> _ghostData;
  const nodalBasis *_fs;
  // parallel
  class parallelStructure;
  parallelStructure *_parallel, *_parallelDG;
  std::map<std::pair<int, int>, parallelStructure *> _parallelMRTag;

  std::vector<dgFullMatrix<double> > _dataProxys; // proxys 
  std::vector<int> _groupFirstDofId;
  function *_function, *_functionGradient;
  // _readMsh, is a subfunction of readMsh, which reads msh files from idx files
  void _readMsh(std::string name, std::string name_dir, double &time, int &step, std::map<int,std::pair<int, int> > elementMap, int nb_parts, int dofF);
  void _init();

  void readNodeData(std::ifstream &f, const std::string name, int ICOMP);

protected:
  void _getAtIntegrationPoints(const dgGroupOfElements &group, const dgIntegrationMatrices &integrationMatrix, fullMatrix<double> &v) const;
  void _getAtIntegrationPoints(const dgGroupOfFaces &faces, int iConnection, const dgIntegrationMatrices &integrationMatrix, fullMatrix<double> &v) const;
  void _getGradientAtIntegrationPoints(const dgGroupOfElements &group, const dgIntegrationMatrices &integrationMatrix, dgFullMatrix<double> &v) const;
  void _getGradientAtIntegrationPoints(const dgGroupOfFaces &faces, int iConnection, const dgIntegrationMatrices &integrationMatrix, dgFullMatrix<double> &v) const;

public:
  dgDofContainer (const dgDofContainer&);
  /**Return a function to evaluate this field */
  inline function *getFunction() const {return _function;}
  /**Return a function to evaluate the gradient of this field */
  inline function *getFunctionGradient() const {return _functionGradient;}
  /**this=this*scale */
  void scale(double f);
  inline int getDofId (int groupId, int elementId, int fieldId, int nodeId) const {
    const fullMatrix<double> &proxy = getGroupProxy(groupId);
    return _groupFirstDofId[groupId]+(elementId*_nbFields+fieldId)*proxy.size1()+nodeId;
  }
  inline int getGroupFirstDofId(int groupId) {
    return _groupFirstDofId[groupId];
  }
  inline const fullVector<double> &getVector() const {return _data;}
  inline fullVector<double> &getVector() {return _data;}
  inline const fullVector<double> &getGhostVector() const {return _ghostData;}
  inline fullVector<double> &getGhostVector() {return _ghostData;}
  /**Returns the norm of the vector*/
  int norm0() const;
  double norm() const;
  double norm(std::vector<dgGroupOfElements*>groups);
  /** this = this+a*dofContainer */
  void axpy(const dgDofContainer &x, double a=1.);
  /** multiply this = x * y */
  void multiply(const dgDofContainer &x, const dgDofContainer &y);
  /** divide this = x / y. if iFieldY != -1, then all the fields of x are divided by the $iFieldY$ field of y. The code is not generalized yet */
  void divide(const dgDofContainer &x, const dgDofContainer &y, int iFieldY = -1);
  /** product = this * x */
  void dotProduct(const dgDofContainer &x, fullVector<double> &product);
  /** Return the proxy to the solution of one group.*/
  inline dgFullMatrix<double> &getGroupProxy(int gId){ return _dataProxys[gId]; }
  /** Return the proxy to the solution of one group.*/
  inline const dgFullMatrix<double> &getGroupProxy(int gId) const { return _dataProxys[gId]; }
  /** Return the proxy to the solution of one group.*/
  inline const dgFullMatrix<double> &getGroupProxy(const dgGroupOfElements* g) const { return _dataProxys[_groups->getElementGroupId(g)]; }
  /** Return the proxy to the solution of one group.*/
  inline dgFullMatrix<double> &getGroupProxy(const dgGroupOfElements* g) { return _dataProxys[_groups->getElementGroupId(g)]; }
  void build (const dgGroupCollection *groups);
  dgDofContainer (const dgGroupCollection &groups, int nbFields);
  ~dgDofContainer (); 
  void destroyParallel();
  void transferGroups(const dgGroupCollection *newGroups, dgDofContainer *newDof);
  void changeGroups(const dgGroupCollection *newGroups, std::map<int, std::pair<int,  int> > groupsMap);
  int getNbFields() const {return _nbFields;}
  void setFieldName(int, std::string);
  std::string getFieldName(int) const;
  /**Copy the original dof to their ghosted copy on other proccesses*/
  void scatter(bool dgGhost = false);
  void scatterBegin(bool dgGhost = false);
  void scatterEnd();
  void scatterBeginMRTag(int maxTag, int minTag = 0);
  void scatterEndMRTag(int maxTag, int minTag = 0);
  void save(const std::string name);
  void load(const std::string name);
  void bezierCrop(double min, double max);
  void bezierCrop(const function *minF, const function *maxF);

  /** Set same value to all DOFs */
  void setAll(double v);
  void setAll(std::vector<double> v);
  /**Project a function onto this vector */
  void L2Projection(const function *f, dgDofContainer *sol=NULL, dgDofContainer *var=NULL);
  /**Set the values of this vector as the value of the function on nodes */
  void interpolate(const function *f, dgDofContainer *sol=NULL, dgDofContainer *var=NULL, int IDgroup=-1, double time = -1, bool scatterSolution=true);
  void interpolate(const function *f, double time, dgDofContainer *sol=NULL, bool scatterSolution=true);

  void reInitialize(const double width);
  /** Export the dof for gmsh visualization */
  void exportMsh(const std::string name, double time=0.0, int step=0, std::string fieldName="") const;
  /** Export the dof for gmsh visualization - only one COMP */
  void exportMshSingleComp(int ICOMP, const std::string name, double time=0.0, int step=0, std::string fieldName="") const;
  /** Export the dof for gmsh visualization - as a vector (only for dof conainer of 2 or 3 fields) */
  void exportMshVector(const std::string name, double time, int step, std::string fieldName) const;
  /** Export the dof for gmsh visualization using "NodeData" file format*/
  void exportMshNodeData(const std::string name, double time=0., int step=0, std::string fieldName="");
  /** Import a msh file in the dof container. 'filename'  refers to the part of the file name before _COMPXX.msh */
  void importMsh(const std::string filename, double &time, int &step, std::string &fieldName);
  /** Import a msh file in the dof container using the "NodeData" file format. 'filename'  refers to the part of the file name before _COMPXX.msh */
  void importMshNodeData(const std::string name, bool multiFile = true);
  /** Import a msh or idx file  */
  void readMsh(const std::string name, double &time, int &step, int inField=-1, int outField=-1);
  /** Export a function for gmsh visualization, (this dof is used as solution). If the function is a vector, export a vector field instead of the three component fields of the vector */
  void exportFunctionMsh (const function * fun, const std::string name, double time=0.0, int step=0, std::string fieldname="") const;
  /** Export a function for gmsh visualization, (this dof is used as solution). It will export a field on a physical surface*/
  void exportFunctionSurf (function * fun, const std::string name, double time=0.0, int step=0, std::string fieldname="", std::vector<std::string> Tags=std::vector<std::string>(0)); 
  /**Export the group ids for gmsh visualization */
  void exportGroupIdMsh();
  void exportMultirateGroups();
  void exportMultirateTimeStep();
  /**Export the vtk files. Otherwise, same as exportMsh //TODO in devlopment !*/
  void exportVtk(const std::string name, double time=0.0, int step=0, std::string fieldname="");
  /** Export a function for vtk visualization, (this dof is used as solution). If the function is a vector, export a vector field instead of the three component fields of the vector */
  /** If dofAsCellData is set, each dof is written as a separate CellData to allow high-order reconstruction */
  void exportFunctionVtk (function * fun, const std::string name, double time, int step, std::string exportname="", std::vector<int> nComps_ = std::vector<int>(), std::vector<std::string> fieldsNames_ = std::vector<std::string>(), function * coordFun=NULL);
  /**Copies data from given dgDofContainer to this container*/
  void copy(const dgDofContainer &x);
  /**Multiplies data array by inverse mass matrix in place.*/
  void multiplyByInvMassMatrix();
  /**Finds minimum/maximum value of each field.*/
  void minmax(fullMatrix<double>& minimum, fullMatrix<double>& maximum);
  /**Multiplies data array by mass matrix in place.*/
  void multiplyByMassMatrix();
  /**Multiplies data array by a custom element-sized matrix in place.*/
  void multiplyByLocalMatrix(const dgGroupOfElements &group, const fullMatrix<double> mat);
  /**In-place multiplies data array by a custom element-sized matrix whose given vector is the diagonal.*/
  void multiplyByDiagonalVector(const dgGroupOfElements &group, const fullVector<double> &vec);
  /** Returns dgGroupCollection associated with this dgDofContainer.*/
  inline const dgGroupCollection *getGroups() const { return _groups; }
  /**Project onto this vector in its groupCollection a function in another groupCollection. Rem: mesh must be the same */
  void fromElsewhereL2Projection(const function *f, const dgGroupCollection *gc, std::vector<std::string> tags,
                                 dgDofContainer *sol=NULL, dgDofContainer *var=NULL);
};

class L2ProjectionContinuous {
private:
  int _nbFields;
  dgDofManager *_dofManager;
public:
  L2ProjectionContinuous(dgDofManager* dofManager);
  ~L2ProjectionContinuous() {}
  void apply(dgDofContainer *output, const function * f);
};

#endif
