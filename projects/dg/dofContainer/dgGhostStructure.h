#ifndef _DG_GHOST_STRUCTURE_H
#define _DG_GHOST_STRUCTURE_H
#include <map>
#include <vector>

//only int and double are implemented

template<class dataType>
class dgGhostCommunication {
public:
  typedef enum {ASSIGN, SUM, MAX, MIN} operation;
private:
  class MPIRequestVector; // avoid #include "mpi.h" in this header file
  MPIRequestVector * _reqRecv, *_reqSend;
  std::map<int, std::vector<int> > _sendIdx, _recvIdx;
  operation _operation;
  int _nbFields;
  std::vector<dataType> &_values;
  std::map<int, std::vector<dataType> > _sendValues, _receivedValues;
  bool _inProgress;
public:
  ~dgGhostCommunication();
  dgGhostCommunication (
    const std::map<int, std::vector<int> > &sendIdx, const std::map<int, std::vector<int> > &recvIdx,
    operation op, int nbFields, std::vector<dataType> &values);
  void begin();
  void end();
};

class dgGhostStructure {
private:
  std::map<int, std::vector<int> > _scatterSend, _scatterReceive;
public:
  typedef enum {SCATTER, GATHER_SUM, GATHER_MAX, GATHER_MIN} operation;
  dgGhostStructure(std::map<int, std::vector<int> >&scatterSendGlobal, std::map<int, int> &global2localId);
  //non blocking reusable-dgGhostCommunication interface
  template<class dataType>
  dgGhostCommunication<dataType> *newCommunication(operation op, std::vector<dataType> &values, int nbFields);
  //simple interface
  template<class dataType>
  void communicate(operation op, std::vector<dataType> &values, int nbFields);
};
#endif
