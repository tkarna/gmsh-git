#include "GModel.h"
#include "Field.h"
#include "slimFields.h"
#include "dgConfig.h"

#ifdef HAVE_PROJ
#include "proj_api.h"
class utmField : public Field
{
  int iField, zone;
  projPJ pj_utm, pj_latlon;
  public:
  std::string getDescription()
  {
    return "Evaluate Field[IField] in Universal Transverse Mercator coordinates.\n";
  }
  utmField()
  {
    iField = 1;
    zone = 0;
    options["IField"] = new FieldOptionInt
      (iField, "Index of the field to evaluate");
    options["Zone"] = new FieldOptionInt
      (zone, "Zone of the UTM projection", & update_needed);
    pj_utm = pj_latlon = NULL;
  }
  const char *getName()
  {
    return "utm";
  }
  virtual ~utmField()
  {
    if(pj_utm) pj_free(pj_utm);
    if(pj_latlon) pj_free(pj_latlon);
  }
  double operator() (double x, double y, double z, GEntity *ge=0)
  {
    Field *field = GModel::current()->getFields()->get(iField);
    if(!field || iField == id) return MAX_LC;
    double lon = x;
    double lat = y;
    if (update_needed){
      if(pj_utm) pj_free(pj_utm);
      if(pj_latlon) pj_free(pj_latlon);
      std::ostringstream convertor;
      convertor << "+proj=utm +ellps=WGS84 +zone=" << zone << " +south";
      const char *utmSyst = convertor.str().c_str();
      if ( !(pj_utm = pj_init_plus(utmSyst)) ) {
        printf("Error init UTM");
        exit(1); }
      if ( !(pj_latlon = pj_init_plus("+proj=latlong +ellps=WGS84")) )  {
        printf("Error init latLon");
        exit(1); }
      update_needed = false;
    }
    int er=0;
    if ( (er = pj_transform( pj_utm, pj_latlon, 1, 1, &lon, &lat, NULL )) != 0) {
      printf ("Transform failed: %s", pj_strerrno (er));
      exit(1); }
    return (*field)(lon, lat, 0);
  }
};
#endif

class xy2d2LonLatField : public Field
{
  int iField;
  double R, phiP, thetaP;
 public:
  std::string getDescription()
  {
    return "Evaluate Field[IField] in geographic coordinates (longitude, latitude) from x and y projected on a plane\n\n";
  }
  xy2d2LonLatField()
  {
    iField = 1;
    options["IField"] = new FieldOptionInt
      (iField, "Index of the field to evaluate.");
    R = 6371e3;
    phiP = 0;
    thetaP = 0;

    options["Radius"] = new FieldOptionDouble
      (R, "radius of the sphere");
    options["Phi"] = new FieldOptionDouble
      (phiP, "longitude of the projection point (in degrees)");
    options["Theta"] = new FieldOptionDouble
      (thetaP, "latitude of the projection point (in degrees)");
  }
  const char *getName()
  {
    return "xy2d2LonLat";
  }
  double operator() (double x2d, double y2d, double z2d, GEntity *ge=0)
  {
    Field *field = GModel::current()->getFields()->get(iField);
    if(!field || iField == id) return MAX_LC;
    double phi = phiP*M_PI/180;
    double theta = thetaP*M_PI/180;
    double pOx = cos(theta)*cos(phi)*R;
    double pOy = cos(theta)*sin(phi)*R;
    double pOz = sin(theta)*R;
    double pPhiX = -sin(phi);
    double pPhiY = cos(phi);
    double pPhiZ = 0;
    double pThetaX = -sin(theta)*cos(phi);
    double pThetaY = -sin(theta)*sin(phi);
    double pThetaZ = cos(theta);
    
    double x = pPhiX * x2d + pThetaX * y2d + pOx;
		double y = pPhiY * x2d + pThetaY * y2d + pOy;
		double z = pPhiZ * x2d + pThetaZ * y2d + pOz;
    return (*field)(atan2(y, x), asin(z / R), 0);
  }
};

void slimRegisterField(GModel *m)
{
  FieldManager &fields = *m->getFields();
#ifdef HAVE_PROJ
  fields.map_type_name["utm"] = new FieldFactoryT<utmField>();
#endif
  fields.map_type_name["xy2d2LonLat"] = new FieldFactoryT<xy2d2LonLatField>();
}
