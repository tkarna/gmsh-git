#ifndef _SLIM_FUNCTION_H_
#define _SLIM_FUNCTION_H_
#include "function.h"
#include "stdint.h"
#include "MElement.h"
#include "dgDofContainer.h"
#include "dgConfig.h"

/**transform the an XYZ function to a latLon / lonLat / latLonDegrees / lonLatDegrees */
class slimFunctionXYZ2LatLon : public function {
	int xid;
	int yid;
	double factor;
	fullMatrix<double> coordF;
  public:
  void call(dataCacheMap *m, fullMatrix<double> &val);
  /**given the coordFormat: latLon or lonLat or latLonDegrees or lonLatDegrees, it return a converted coordinate function */
  slimFunctionXYZ2LatLon(const std::string coordFormat, const function *coordFunction);
};
  
/**transform the an LonLat vector function to a xyz vector function */
class slimFunctionLonLatVector2XYZ : public function {
	int xid;
	int yid;
	double factor;
	fullMatrix<double> vectorF;
	fullMatrix<double> coordF;
  public:
  void call(dataCacheMap *m, fullMatrix<double> &val);
  /**the input is a vector with zonal/meridional components, the coordFormat: \"latLon\" or \"lonLat\" or \"latLonDegrees\" or \"lonLatDegrees\" and the it return a converted coordinate xyz vector function */
  slimFunctionLonLatVector2XYZ(const function *vectorFunction, const std::string coordFormat, const function *coordFunction);
};

/**A function to imbricate 2 functions covering imbricated areas */
class slimFunctionImbricate:public function {
	fullMatrix<double> mainF,subF;
	public:
  void call(dataCacheMap *m,  fullMatrix<double> &val);
  /**A function to imbricate a function covering a bigger area (fMain) with a sub function covering a smaller area (fSub) and returning NaN outside of its area (you can use the slimFunctionStructuredGridFile with the flag nanIfOutside) */
	slimFunctionImbricate(const function *fMain, const function *fSub);
};

class slimFunctionETOPO1:public function {
  int16_t * _values;
  double _dx;
  double _vmin, _vmax;
  size_t _nx, _ny;
   fullMatrix<double> lonlatdegrees;
	public:
  void call(dataCacheMap *m,  fullMatrix<double> &val);
  /**A function to read http://www.ngdc.noaa.gov/mgg/global/relief/ETOPO1/data/bedrock/grid_registered/binary/etopo1_bed_g_i2.zip **/
	slimFunctionETOPO1(const char *filename, const function *flonlatdeg, double vmin, double vmax);
  ~slimFunctionETOPO1();
};

class slimFunctionBathyScheldtDelft:public function {
  double * _values;
  double _x0, _y0, _z0;
  double _dx, _dy, _dz;
  double _vmin, _vmax;
  int _nx, _ny, _nz;
   fullMatrix<double> lonlatdegrees;
	public:
  void call(dataCacheMap *m,  fullMatrix<double> &val);
  /**A function to read the bathymetry of the Scheldt from Delft **/
	slimFunctionBathyScheldtDelft(const char *filename, const function *flonlatdeg, double vmin, double vmax);
  ~slimFunctionBathyScheldtDelft();
};

class slimFunctionBathyScheldtDelftAndETOPO1:public function {
  double * _valuesD;
  double _x0D, _y0D, _z0D;
  double _dxD, _dyD, _dzD;
  int _nxD, _nyD, _nzD;
  int16_t * _valuesE;
  double _dxE;
  size_t _nxE, _nyE;
  double _vmin, _vmax;
   fullMatrix<double> lonlatdegrees;
	public:
  void call(dataCacheMap *m,  fullMatrix<double> &val);
  /**A function to read http://www.ngdc.noaa.gov/mgg/global/relief/ETOPO1/data/bedrock/grid_registered/binary/etopo1_bed_g_i2.zip and the bathymetry of the Scheldt from Delft, and to merge them **/
	slimFunctionBathyScheldtDelftAndETOPO1(const char *filenameD, const char *filenameE, const function *flonlatdeg, double vmin, double vmax);
  ~slimFunctionBathyScheldtDelftAndETOPO1();
};

#ifdef HAVE_PROJ
#include <proj_api.h>
/** Class to project between different co-ordinate systems using the PROJ library */
class slimProjector {
private:
  projPJ _pj_sourceProj, _pj_destinationProj;

public:
  /** Constructor */
  slimProjector(const std::string sourceProjText, const std::string destinationProjText);
  
  /** Project from src_coords into dest_coords
   Note: if projecting to/from lonLat, must supply value in RADIANS and supply first LON(x) then LAT(y)*/
  void projectPoint(fullMatrix<double> *src_coords, fullMatrix<double> *dest_coords);
};
#endif

/** Class to supply a function to calculate the distance and direction to/of the nearest reef from every node. */
class reefDistance {
private:
  dgGroupCollection* _groups;
  dgDofContainer* _reefsDC;
  dgDofContainer* _XY_DC;
  double _R;
  
public:
  /** Constructor: */
  reefDistance(dgGroupCollection* groups, dgDofContainer* reefsDC, dgDofContainer* XY_DC, double R=0) {
    _groups = groups;
    _reefsDC = reefsDC;
    _XY_DC = XY_DC;
    _R=R;
  }
  
  /** Method to calculate distance and direction to nearest reef element.
   *    -> 1. Generate list of reef element centres
   *    -> 2. Get XYZ DC & convert to fullVector
   *    -> 3. Loop over every node in XYZ fV (ie. every 3 elements) and calc. distance between node and nearest reef element centre
   *    -> 4. Save dist. between node and nearest reef in a DC, and unit vector pointing in direction of this reef in another DC
   * 
   * INPUT (in constructor): 1. group of elements | 2. XYZ DC
   * OUTPUT (in access method): 1. distToReefsDC | 2. directionToReefsDC
   */
  void getNearestReefMap(dgDofContainer* distToReefsDC, dgDofContainer* directionToReefsDC);
  
};

/** A class to load data from a reef map, and then access it through a function-class object.
    The function projects the reef map onto the mesh by resolving the map:
      -> At each element's barycentre : for Particle Tracker
      -> Using OLD projection system */
class reefsRaw_oldProj : public function {
private:
  double _Ox, _Oy, _dx, _dy;
  int _nx, _ny, _nz, *_data;
  double _pOx, _pOy, _pOz, _pPhiX, _pPhiY, _pPhiZ, _pThetaX, _pThetaY, _pThetaZ, _pi;
  int _nbReefsOnDc;
  double _R;
  
public:
  /** Constructor */
  reefsRaw_oldProj(const std::string& filename, double RadiusForPlaneProj);
  
  void call(dataCacheMap *cash, fullMatrix<double> &reefsFM) ;
  void clearReefsData() {
    delete[] _data;
    printf("Reefs data cleared from memory.\nNumber of reef elements on Dof Container: %d\n", _nbReefsOnDc);
  }
};

#ifdef HAVE_PROJ
/** A class to load data from a reef map, and then access it through a function-class object.
    The function projects the reef map onto the mesh by resolving the map:
      -> At each element's barycentre : for Particle Tracker
      -> Using PROJ projection system */
class reefsRaw : public function {
private:
  double _Ox, _Oy, _dx, _dy, _pi;
  int _nx, _ny, _nz, *_data;
  slimProjector* _utm_to_lonlat;
  int _nbReefsOnDc;
  
public:
  /** Constructor */
  reefsRaw(const std::string& filename, slimProjector* utm_to_lonlat);
  
  void call(dataCacheMap *cash, fullMatrix<double> &reefsFM) ;
  void clearReefsData() {
    delete[] _data;
    printf("Reefs data cleared from memory.\nNumber of reef elements on Dof Container: %d\n", _nbReefsOnDc);
  }
};
#endif

/** A class to load data from a reef map, and then access it through a function-class object.
    The function projects the reef map onto the mesh by resolving the map:
      -> At element nodes given in lonLatDegrees  : for Bottom Drag */
class reefsSmooth : public function {
private:
  double _Ox, _Oy, _dx, _dy;
  int _nx, _ny, _nz, *_data;
  int _nbReefsOnDc;
  fullMatrix<double> _lonLatDegrees;
  
public:
  /** Constructor */
  reefsSmooth(const std::string& filename, const function *lonLatDegrees);
  
  void call(dataCacheMap *cash, fullMatrix<double> &reefsFM) ;
  void clearReefsData() {
    delete[] _data;
    printf("Reefs data cleared from memory.\nNumber of reef elements on Dof Container: %d\n", _nbReefsOnDc);
  }
};

/**A class to load data from a bathymetry file, and then access it through a function-class object.*/
class bathy : public function {
private:
  double _Ox, _Oy, _dx, _dy;
  int _nx, _ny;
  float *_data;
  fullMatrix<double> _lonLatDegrees;
public:
  bathy(const std::string& filename, const function *lonLatDegrees);
  
  void call(dataCacheMap *m, fullMatrix<double> &bathymetry);
  void clearBathData() {
    delete[] _data;
    printf("Bath info: bathymetry data cleared from memory.\n");
  }
};

class slimCubicWindStress:public function {
  fullMatrix<double> _wind;
  double _f1, _f2, _f3;
 public :
  slimCubicWindStress(const function &_wind, double f1, double f2, double f3);
  void call(dataCacheMap *, fullMatrix<double> &val);
};

/** divide  a transport by (bath + eta) **/
class slimTransportToVelocityFunction:public function {
  fullMatrix<double> _UV, _bath, _sse;
 public :
  slimTransportToVelocityFunction(const function &transport, const function &bath, const function &sse);
  void call(dataCacheMap *, fullMatrix<double> &val);
};

/** divide a flow rate by the river section (integral of bath along boundary "tag") then multiply it locally by bath / (bath + eta)
 *  u = q / (int_tag bath) * bath / (bath + eta) */
class slimFlowRateToVelocity:public function {
  fullMatrix<double> _UV, _bath, _eta, _flowRate, _normal;
  double _section;
 public:
  slimFlowRateToVelocity(dgGroupCollection &group2d, const std::string &bndTag, const function &flowRate, const function &bath, const function &eta);
  void call(dataCacheMap *, fullMatrix<double> &val);
};



SPoint2 intersection2(SPoint2 p1, SPoint2 p2, SPoint2 p3, SPoint2 p4, int& flag);
#endif
