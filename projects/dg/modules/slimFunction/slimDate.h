#ifndef SLIM_DATE_H
#define SLIM_DATE_H
#include "GmshConfig.h"
#include "time.h"
#include <string>
typedef long long int llint;

long gregorianDateToJulianDay(int year, int month,int day);
void julianDayToGregorianDate(long julday, int *year, int *month, int *day);
class slimTmDateShift{
 friend class slimTmDate;
 protected : 
	llint tmSec,tmMDay,tmYear,tmMon,tmHour,tmMin;
	double fracSec;
 public:
	slimTmDateShift();
	void set(long long int year,llint month,llint day,llint hour,llint min,double sec);
	void get(long long int &year,llint &month,llint &day,llint &hour,llint &min,double &sec);
  double operator()();
	void setPeriodNumber(std::string period, double number);
};
class slimTmDate{
 public:
	llint tmSec,tmMDay,tmMon,tmHour,tmMin,tmYDay;
  long long int tmYear;
	double fracSec;
	llint time;
  void normalize();
 public:
  slimTmDate();
  void operator += (double second);
  void operator -= (double second);
  void operator += (slimTmDateShift shift);
  void operator -= (slimTmDateShift shift);
  slimTmDate operator + (slimTmDateShift shift);
  slimTmDate operator - (slimTmDateShift shift);
  slimTmDate operator + (double second);
  slimTmDate operator - (double second);
//  bool operator < (slimTmDate const *sDate) const { return this->operator()() < sDate->operator()(); }
  // for get and set : year 2009=2009;   month  : jan=1 ... dec=12; day : 1st of the month = 1, 30th of the month =30; hour : midnight=0, 2pm=14 
  void get(long long int &year,llint &month,llint &day,llint &hour,llint &min, double &sec);
  llint getYear();
  void set(long long int year,llint month,llint day,llint hour,llint min, double sec);
	void set(double sec);
  int getYearDay();
  void print();
  std::string toString();
  std::string toStringDuration();
  double operator()() const;
	void setPeriodNumber(std::string period, double number);
};
llint slimMkTime(slimTmDate *tm);
#endif
