#include "GmshConfig.h"
#include "slimDate.h"
#include <ctime>
#include "math.h"
#include <stdio.h>
#include "function.h"

/*
 * The following two functions convert between Julian day number and
 * Gregorian/Julian dates (Julian dates are used prior to October 15,
 * 1582; Gregorian dates are used after that).  Julian day number 0 is
 * midday, January 1, 4713 BCE.  The Gregorian calendar was adopted
 * midday, October 15, 1582.
 *
 * Author: Robert Iles, March 1994
 *
 * C Porter: Steve Emmerson, October 1995
 *
 * Original: http://www.nag.co.uk:70/nagware/Examples/calendar.f90
 *
 * There is no warranty on this code.
 */

/*
 * Convert a Julian day number to a Gregorian/Julian date.
 */
//  long	julday;		/* Julian day number to convert */
//  int		*year;		/* Gregorian year (out) */
//  int		*month;		/* Gregorian month (1-12) (out) */
//  int		*day;		/* Gregorian day (1-31) (out) */

void julianDayToGregorianDate(long julday, int *year, int *month, int *day) {
	long	ja, jb, jd;
	int		jc;
	int		je, iday, imonth, iyear;
	double	xc;

	if (julday < 2299161)
		ja = julday;
	else {
		int	ia = (int)(((julday - 1867216) - 0.25) / 36524.25);
		ja = julday + 1 + ia - (int)(0.25 * ia);
	}

	jb = ja + 1524;
	xc = ((jb - 2439870) - 122.1) / 365.25;
	jc = (int)(6680.0 + xc);
	jd = 365 * jc + (int)(0.25 * jc);
	je = (int)((jb - jd) / 30.6001);

	iday = (int)(jb - jd - (int)(30.6001 * je));

	imonth = je - 1;
	if (imonth > 12)
		imonth -= 12;
	iyear = jc - 4715;
	if (imonth > 2)
		iyear -= 1;
	if (iyear <= 0)
		iyear -= 1;

	*year = iyear;
	*month = imonth;
	*day = iday;
}

/*
 * Convert a Gregorian/Julian date to a Julian day number.
 *
 * The Gregorian calendar was adopted midday, October 15, 1582.
 */
long gregorianDateToJulianDay(int year, int month,int day) {
    llint	igreg = 15 + 31 * (10 + (12 * 1582));
    llint	iy;	/* signed, origin 0 year */
    llint	ja;	/* Julian century */
    llint	jm;	/* Julian month */
    llint	jy;	/* Julian year */
    long	julday;	/* returned Julian day number */
    /*
     * Because there is no 0 BC or 0 AD, assume the user wants the start of 
     * the common era if they specify year 0.
     */
		if (year == 0)
			year = 1;
		iy = year;
		if (year < 0)
			iy++;
		if (month > 2){
			jy = iy;
			jm = month + 1;
		}else{
			jy = iy - 1;
			jm = month + 13;
		}

		/*
		 *  Note: SLIGHTLY STRANGE CONSTRUCTIONS REQUIRED TO AVOID PROBLEMS WITH
		 *        OPTIMISATION OR GENERAL ERRORS UNDER VMS!
		 */
		julday = day + (int)(30.6001 * jm);
		//if (jy >= 0){
			julday += 365 * jy;
			julday += 0.25 * jy;
		/*}else{
			double	xi = 365.25 * jy;
			if ((int)xi != xi)
				xi -= 1;
			julday += (int)xi;
		}*/
		julday += 1720995;

		if (day + (31* (month + (12 * iy))) >= igreg){
			ja = jy/100;
			julday -= ja;
			julday += 2;
			julday += ja/4;
		}
		return julday;
}

void	slimTmDate::setPeriodNumber(std::string period, double number){
	if(period=="year"){
		tmYear=number-1900;
	}else if(period=="month"){
  	tmMon=number-1;
	}else if(period=="day"){
		tmMDay=number;
	}else if(period=="hour"){
		tmHour=number;
	}else if(period=="minute"){
		tmMin=number;
	}else if(period=="second"){
		tmSec=number;
	}else {
		Msg::Fatal("wrong time key-word (only year, month, day, hour, minute and second)");
	}
  normalize();
}
void	slimTmDateShift::setPeriodNumber(std::string period, double number){
	if(period=="year"){
		tmYear=number;
	}else if(period=="month"){
  	tmMon=number;
	}else if(period=="day"){
		tmMDay=number;
	}else if(period=="hour"){
		tmHour=number;
	}else if(period=="minute"){
		tmMin=number;
	}else if(period=="second"){
		tmSec=number;
	}else {
		Msg::Fatal("wrong time key-word (only year, month, day, hour, minute and second)");
	}
}

slimTmDate::slimTmDate(){
  tmYear=70;//time 0 corresponds to 1970,1,1
  tmMon=0;
  tmMDay=1;
  tmHour=0;
  tmMin=0;
  tmSec=0;
  fracSec=0;
  normalize();
}
slimTmDateShift::slimTmDateShift(){
  tmYear=0;
  tmMon=0;
  tmMDay=0;
  tmHour=0;
  tmMin=0;
  tmSec=0;
  fracSec=0;
}
static int nDayBefMonth[12]={ 0,31,59,90,120,151,181,212,243,273,304,334 };
static int nDayInMonth[12]={ 31,28,31,30,31,30,31,31,30,31,30,31 };
static void normalizeVar(llint &var0,llint &var1, int n){
  if(var0>=(size_t)n){
    var1+=var0/n;
    var0=var0%n;
  }
}
static void normalizeMonthYear(llint &month, long long int &year) {
  if(month >= 12){
    year += month / 12;
    month = month / 12 - 1;
  }
  else if(month < 0){
    year += month / 12;
    month = month - (month / 12) * 12;
  }
}
int nDayInThisMonth(llint mon, long long int year){
  if(mon == 1)
    return (year % 4 == 0)? 29 : 28;
  else
    return nDayInMonth[mon];
}
int nDayInThisYear(int year){
  return (year%4==0 && (year%100!=0 || year%400==0)) ? 366 : 365;
}
static void normalizeDay(llint &day, llint &mon, long long int &year){
  //TODO divide day by 4*365+1 = 1461  and deduce approx nb years (otherwise too slow :-))
  // after, make adjustements with max 4 first and 4 last months
  year += (day / 1461) * 4;
  day = day % 1461;
  while((size_t)nDayInThisMonth(mon, year) < day){ 
    day -= nDayInThisMonth(mon, year);
    mon += 1;
    normalizeMonthYear(mon, year);
  }
  while(day <= 0){
    mon -= 1;
    normalizeMonthYear(mon, year);
    day += nDayInThisMonth(mon, year);
  }
}
llint slimMkTime(slimTmDate *tm){
  normalizeVar(tm->tmSec,tm->tmMin,60);
  normalizeVar(tm->tmMin,tm->tmHour,60);
  normalizeVar(tm->tmHour,tm->tmMDay,24);
  normalizeMonthYear(tm->tmMon,tm->tmYear);
  normalizeDay(tm->tmMDay,tm->tmMon,tm->tmYear);
  tm->tmYDay=nDayBefMonth[tm->tmMon]+tm->tmMDay;
  int bisextileDays;
  if(tm->tmMon>=2)
    bisextileDays=tm->tmYear/4-70/4;
  else
    bisextileDays=(tm->tmYear-1)/4-70/4;
  return tm->tmSec+60*(tm->tmMin+60*(tm->tmHour+24*((tm->tmMDay-1)+nDayBefMonth[tm->tmMon]+365*(tm->tmYear-70)+bisextileDays)));
}
void slimTmDate::normalize(){
  double sec;
  double fsec = modf(fracSec + tmSec, &sec);
  if(fsec < 0){
    fsec += 1;
    sec  -= 1;
  }
  tmSec = (llint) sec;
  fracSec = fsec;
  time = slimMkTime(this);
}
int slimTmDate::getYearDay(){
  return tmYDay;
}
double slimTmDate::operator()() const {
  return time+fracSec;
}
llint slimTmDate::getYear(){
  return tmYear+1900;
}
void slimTmDate::get(long long int &year,llint &month,llint &day,llint &hour,llint &min,double &sec){
  year=tmYear+1900;
  month=tmMon+1;
  day=tmMDay;
  hour=tmHour;
  min=tmMin;
  sec=tmSec+fracSec;
}
void slimTmDate::set(double sec){
	set(1970,1,1,0,0,sec);
}
void slimTmDate::set(long long int year,llint month,llint day,llint hour,llint min, double sec){
  tmYear=year-1900;
  tmMon=month-1;
  tmMDay=day;
  tmHour=hour;
  tmMin=min;
  tmSec=0;
  fracSec=sec;
  normalize();
}
void slimTmDate::print(){
  printf("year=%lli month=%lli day=%lli hour=%lli min=%lli sec=%lli fsec=%e\n",tmYear+1900,tmMon+1,tmMDay,tmHour,tmMin,tmSec,fracSec);
}
std::string slimTmDate::toString(){
  char ch[100];
  sprintf(ch, "%02d/%02d/%4d %02d:%02d:%07.4f", int(tmMDay), int(tmMon+1), int(tmYear+1900), int(tmHour), int(tmMin), (tmSec+fracSec));
  return std::string(ch);
}
std::string slimTmDate::toStringDuration(){
  char ch[100];
  double duration = operator()();
  int days = int(duration / (3600.*24));
  duration -= days * 3600. * 24;
  int hours = int(duration / 3600.);
  duration -= hours * 3600.;
  int mins = int(duration / 60.);
  duration -= mins * 60.;
  if(days)
    sprintf(ch, "%7d days, %02d hours, %02d mins, %07.4f secs", days, hours, mins, duration);
  else if(hours)
    sprintf(ch, "%02d hours, %02d mins, %07.4f secs", hours, mins, duration);
  else if(mins)
    sprintf(ch, "%02d mins, %07.4f secs", mins, duration);
  else
    sprintf(ch, "%07.4f secs", duration);
  return std::string(ch);
}
void slimTmDate::operator += (slimTmDateShift shift) {
  tmYear+=shift.tmYear;
  tmMon+=shift.tmMon;
  tmMDay+=shift.tmMDay;
  tmHour+=shift.tmHour;
  tmMin+=shift.tmMin;
  tmSec+=shift.tmSec;
  fracSec+=shift.fracSec;
  normalize();
}
void slimTmDate::operator -= (slimTmDateShift shift) {
  tmYear-=shift.tmYear;
  tmMon-=shift.tmMon;
  tmMDay-=shift.tmMDay;
  tmHour-=shift.tmHour;
  tmMin-=shift.tmMin;
  tmSec-=shift.tmSec;
  fracSec-=shift.fracSec;
  normalize();
}
slimTmDate slimTmDate::operator + (double second) {
  slimTmDate date(*this); // copy
  date.fracSec += second;
  date.normalize();
  return date;
}
slimTmDate slimTmDate::operator - (double second) {
  slimTmDate date(*this); // copy
  date.fracSec -= second;
  date.normalize();
  return date;
}

slimTmDate slimTmDate::operator + (slimTmDateShift shift) {
  slimTmDate date;
/*  date.tmYear = tmYear + shift.tmYear;
  date.tmMon  = tmMon  + shift.tmMon;
  date.tmMDay = tmMDay + shift.tmMDay;
  date.tmHour = tmHour + shift.tmHour;
  date.tmMin  = tmMin  + shift.tmMin;
  date.tmSec  = tmSec  + shift.tmSec;
  date.fracSec= fracSec+ shift.fracSec;
  normalize();*/
//  printf("%d+%d+%e+%e=%e\n",tmSec, shift.tmSec, fracSec, shift.fracSec, double(tmSec + shift.tmSec) + fracSec + shift.fracSec);
  date.set(tmYear + shift.tmYear + 1900, tmMon + shift.tmMon + 1, tmMDay + shift.tmMDay,
           tmHour + shift.tmHour, tmMin + shift.tmMin, double(tmSec + shift.tmSec) + fracSec + shift.fracSec);
  return date;
}
slimTmDate slimTmDate::operator - (slimTmDateShift shift) {
  slimTmDate date;
/*  date.tmYear = tmYear - shift.tmYear;
  date.tmMon  = tmMon  - shift.tmMon;
  date.tmMDay = tmMDay - shift.tmMDay;
  date.tmHour = tmHour - shift.tmHour;
  date.tmMin  = tmMin  - shift.tmMin;
  date.tmSec  = tmSec  - shift.tmSec;
  date.fracSec= fracSec- shift.fracSec;
  normalize();*/
  date.set(tmYear - shift.tmYear + 1900, tmMon - shift.tmMon + 1, tmMDay - shift.tmMDay,
           tmHour - shift.tmHour, tmMin - shift.tmMin, double(tmSec  - shift.tmSec) + fracSec - shift.fracSec);
  return date;
}
void slimTmDate::operator +=(double second){
  fracSec+=second;
  normalize();
}
void slimTmDate::operator -=(double second){
  fracSec-=second;
  normalize();
}
void slimTmDateShift::get(long long int &year,llint &month,llint &day,llint &hour,llint &min,double &sec){
  year=tmYear;
  month=tmMon;
  day=tmMDay;
  hour=tmHour;
  min=tmMin;
  sec=tmSec+fracSec;
}
void slimTmDateShift::set(long long int year,llint month,llint day,llint hour,llint min,double sec){
  tmYear=year;
  tmMon=month;
  tmMDay=day;
  tmHour=hour;
  tmMin=min;
  tmSec=0;
  fracSec=sec;
}
double slimTmDateShift::operator()(){
  return fracSec+tmSec+60*(tmMin+60*(tmHour+24*(tmMDay+nDayBefMonth[tmMon])));
}

