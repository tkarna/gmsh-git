#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <string>
#include <cstdlib>
#include <iostream>
#include <sstream> 
#include <cstring>
#include "slimStructData.h"
#include "GmshConfig.h"
#include "function.h"
#include "slimDate.h"
#include "dgConfig.h"
#include "time.h"
#ifdef HAVE_NETCDF
#include "netcdfcpp.h"
#endif

slimStructDataContainerTemporalSerie::slimStructDataContainerTemporalSerie(const std::string fileName):slimStructDataContainer(){
  input.open(fileName.c_str(),std::ios::in);
	if(!input.good())
		Msg::Fatal("Unable to open %s\n",fileName.c_str());
	std::string line,period;
	double number;
	while(getline(input,line)){
		if(line=="initial{" ||line == "initial {"){
			while(getline(input,line)){
				if(line.find("}")!=std::string::npos)
					break;
				std::istringstream stream1;
				stream1.str(line);
				stream1>>period>>number;
				initialDate.setPeriodNumber(period,number);
			} 
		}
		break;
	}
	while(getline(input,line)){
		if (line=="shift{"||line == "shift {"){
			while(getline(input,line)){
				if(line.find("}")!=std::string::npos){
					firstDataInFile=input.tellg();
					break;
				}
				std::istringstream stream1;
				stream1.str(line);
				stream1>>period>>number;
				shiftDate.setPeriodNumber(period,number);
			}
		}
		break;
	}
	uncoupledTime=true;
	shiftTime=shiftDate();
	initialTime=initialDate();
	dataT0=new double[1];
	dataT1=new double[1];
	o=new double[1];
	o[0]=initialTime;
	d=new double[1];
	d[0]=shiftTime;
	n=new int[1];
	spaceDim=0;
	tIdOld=-1;
	begData=input.tellg();
	oldValue=0;
}
double slimStructDataContainerTemporalSerie::opfunction (double *dataTable,int *p){
	return dataTable[0];
}
void slimStructDataContainerTemporalSerie::setData(double *dataTable,int tId){
	if(tId<0 || input.eof())
		Msg::Fatal("Out of bound in slimStructDataContainerTemporalSerie\n");
	if(tId<tIdOld){
		input.clear();
		input.seekg(begData);//return to the begining of the file
		tIdOld=0;
		input>>dataTable[0];
		while(tIdOld<tId){
			input>>dataTable[0];
			tIdOld++;
		}
		return;
	}
	if(tId==tIdOld){
		dataTable[0]=oldValue;
		return;
	}
	while(tIdOld<tId){
		input>>dataTable[0];
		tIdOld++;
	}
	oldValue=dataTable[0];
}
//********************************************************************
double slimStructDataContainerSimple::opfunction(double *dataTable,int *p){
	int i,j,k;
	i = spaceDim<1?0:std::max(0,std::min(n[0]-1,p[0]));
	j = spaceDim<2?0:std::max(0,std::min(n[1]-1,p[1]));
	k = spaceDim<3?0:std::max(0,std::min(n[2]-1,p[2]));
	double v=dataTable[(i*n[1]+j)*n[2]+k];
	return v;
}
slimStructDataContainerSimple::slimStructDataContainerSimple(const std::string fileName):slimStructDataContainer(){
        dataT0 = NULL;
        dataT1 = NULL;
	dataCopy = NULL;
	uncoupledTime=false;
	timeDim=0;
	o=new double[3];
	d=new double[3];
	n=new int[3];
	std::ifstream input;
	input.open(fileName.c_str(),std::ios::in);
	if(!input.is_open())
		Msg::Fatal("cannot open file \'%s\'.\n", fileName.c_str());
	if(fileName.substr(fileName.size()-4,4)!=".bin"){
		input >> o[0] >> o[1] >> o[2];
		input >> d[0] >> d[1] >> d[2];
		input >> n[0] >> n[1] >> n[2];
		int nt = n[0] * n[1] * n[2];
		spaceDim=3;
		if (n[2]<2) spaceDim=2;
		else if(n[1]<2)spaceDim=1;
	
		data = new double[nt];
		for(int i = 0; i < nt; i++){
			input >> data[i];
		}
	} else {
		input.read((char *)o, 3 * sizeof(double));
		input.read((char *)d, 3 * sizeof(double));
		input.read((char *)n, 3 * sizeof(int));
		nt = n[0] * n[1] * n[2];
		spaceDim=3;
		if (n[2]<2) spaceDim=2;
		else if(n[1]<2)spaceDim=1;
		data = new double[nt];
		input.read((char *)data, nt * sizeof(double));
	}
	input.close();
}
void slimStructDataContainerSimple::setMaxIfZero(){
	if (!dataCopy)
		dataCopy = new double[n[0]*n[1]*n[2]];
	for (int i=0;i<n[0]*n[1]*n[2];++i)
		dataCopy[i]=data[i];
	int dim = spaceDim;
	int p[3];
	for (int i=0; i<n[0];i++){
		p[0]=i;
		for (int j=0; j<n[1];j++){
			p[1]=j;
			for (int k=0; k<n[2];k++){
				p[2]=k;
				if ((*this).opfunction(data,p)==0){
					double max=0;
					double min=0;
					for (int ii=0; ii<3;++ii){
						for (int jj=0; jj<3;++jj){
							for (int kk=0; kk<3;++kk){
								int p_ext[3] = {p[0]+ii-1,p[1]+jj-1,p[2]+kk-1};
								int iii =std::max(0,std::min(n[0]-1,p_ext[0]));
								int jjj = dim<2?0:std::max(0,std::min(n[1]-1,p_ext[1]));
								int kkk = dim<3?0:std::max(0,std::min(n[2]-1,p_ext[2]));
								max = std::max(max,dataCopy[(iii*n[1]+jjj)*n[2]+kkk]);
								min = std::min(min,dataCopy[(iii*n[1]+jjj)*n[2]+kkk]);
							}
						}
					}
					double value;
					if (max != 0)
						value = max;
					else
						value = min;
					int iii =std::max(0,std::min(n[0]-1,p[0]));
					int jjj = dim<2?0:std::max(0,std::min(n[1]-1,p[1]));
					int kkk = dim<3?0:std::max(0,std::min(n[2]-1,p[2]));
					data[(iii*n[1]+jjj)*n[2]+kkk]=value;
				}
			}
		}
	}
}
//***************************************************************
#ifdef HAVE_NETCDF
slimStructDataContainerNetcdf::~slimStructDataContainerNetcdf(){
   delete ncFile;
}
void slimStructDataContainerNetcdf::setData(double *dataTable, int tId){
	switch(spaceDim){
		case 1:
			if(uncoupledTime){
			  var->set_cur(tId,0);
			  var->get(dataTable,1,nx);
			}else {
			  var->set_cur(0,0);
			  var->get(dataTable,nx);
			}
			break;
		case 2:
			if(uncoupledTime){
                             //printf("in 2d");
                             if(Flag3d){
                               var->set_cur(tId,1,0,0);
                             }
                             else {
                               var->set_cur(tId,0,0);
                             }
			    var->get(dataTable,1,nx,ny);
                            //printf("-\n");

			    for (int j=0; j<nx; j++) {
			      for (int l=0; l<ny; l++) {
// 				int k[2] = {j,l};
 				//printf("%f\t",opfunction(dataTable,k));
			      }
 			      //printf("\n");
			    }
			}else {
				var->set_cur(0,0);
				var->get(dataTable,nx,ny);
			}
			break;
		case 3:
			if(uncoupledTime){
			  //var->set_cur(tId,0,0,0);
			  var->set_cur(tId,0,0,0);
			  //printf("setcurok\n");

			  var->get(dataTable,1,nz,nx,ny);
// 			  for (int i=0; i<nz; i++) {
// 			    for (int j=0; j<nx; j++) {
// 			      for (int l=0; l<ny; l++) {
// 				int k[3] = {i,j,l};
// 				printf("%f\n",opfunction(dataTable,k));
// 			      }
// 			    }
// 			  }
			  
			    //printf("getDataok\n");
			    //printf("v0=%f\n",dataTable[0]);
			}else {
			  var->set_cur(0,0,0);
			  var->get(dataTable,nx,ny,nz);
			}
		  break;
		default:
			break;
	}
}
double slimStructDataContainerNetcdf::opfunction(double *dataTable, int *p){
  int i,j,k;
  i = spaceDim<1?0:std::max(0,std::min(n[0]-1,p[0]));
  j = spaceDim<2?0:std::max(0,std::min(n[1]-1,p[1]));
  k = spaceDim<3?0:std::max(0,std::min(n[2]-1,p[2]));
  nx = spaceDim<1?1:n[0];
  ny = spaceDim<2?1:n[1];
  nz = spaceDim<3?1:n[2];
  return dataTable[(i*ny+j)+k*(nx*ny)];

}
slimStructDataContainerNetcdf::slimStructDataContainerNetcdf(std::string fileName, std::string variable,std::string xDim,std::string yDim):slimStructDataContainer(){
	Flag3d =false;
        std::string zDim="levels_nb";
	std::string tDim="time";
        ncFile = new NcFile(fileName.c_str(),NcFile::ReadOnly);
	data = dataT0 = dataT1 = dataCopy = NULL;
  if (!ncFile->is_valid())
    Msg::Fatal("couldn't open netcdf file %s!",fileName.c_str());
  var   = ncFile->get_var(variable.c_str());
	NcError *err= new NcError(NcError::silent_nonfatal);
	NcVar *varx  = ncFile->get_var(xDim.c_str());
        NcVar *vary  = ncFile->get_var(yDim.c_str());
        NcVar *varz  = ncFile->get_var(zDim.c_str());
	NcVar *vart  = ncFile->get_var(tDim.c_str());
	spaceDim=0;
	if(varx)spaceDim++;
	if(vary)spaceDim++;
	if(varz)spaceDim++;
	NcAtt* att=var->get_att("add_offset");
	addOffset = att?att->values()->as_double(0):0;
	att=var->get_att("scale_factor");
	scaleFactor = att?att->values()->as_double(0):1;
	double xx[2],yy[2],zz[2],tt[2];
	o = new double[3];
        d = new double[3];
	n = new int[3];
	n[0]=1;n[1]=1;n[2]=1;
	//printf("spaceDim=%d\n",spaceDim);
	
	switch(spaceDim){
		case 1:
			varx->get(xx, 2);
			nx  = varx->get_dim(0)->size();
			o[0]=xx[0];
			d[0]=xx[1]-xx[0];
			n[0]=nx;
			nrec=nx;
			att=varx->get_att("units");
			if(att && strcmp(att->as_string(0), "degrees_east")== 0)
				while(o[0]>180)
					o[0]-=360;
			break;
		case 2:
			varx->get(xx, 2);
			vary->get(yy, 2);
			nx  = varx->get_dim(0)->size();
			ny  = vary->get_dim(0)->size();
//                         o[0]=xx[0];
//                         o[1]=yy[0];
//                         d[0]=0,d[1]=yy[1]-yy[0];
//                         n[0]=nx,n[1]=ny;
//                         nrec=nx*ny;
			if (xx[0]==90.0) {
			  o[0]=xx[0]+0.5;
			}
			else {
			  o[0]= xx[0];
			}
			o[1]=yy[0];
			d[0]=xx[1]-o[0],d[1]=yy[1]-yy[0];
			n[0]=nx,n[1]=ny;
			nrec=nx*ny;
                        //printf("nx = %d\n",nx);
                        //printf("ny = %d\n",ny);
                        //printf("d0 = %f\n",d[0]);
                        //printf("d1 = %f\n",d[1]);
			att=varx->get_att("units");
			if(att && strcmp(att->as_string(0),"degrees_east")== 0)
				while(o[0]>180)
					o[0]-=360;
			att=vary->get_att("units");
			if(att && strcmp(att->as_string(0),"degrees_east")== 0)
				while(o[1]>180)
					o[1]-=360;
			break;
		case 3:
			varx->get(xx, 2);
			//printf("xx[0]=%f\n",xx[0]);
			vary->get(yy, 2);
			//printf("yy[0]=%f\n",yy[0]);
			varz->get(zz, 2);
			//printf("zz[0]=%f\n",zz[0]);
			nx  = varx->get_dim(0)->size();
			ny  = vary->get_dim(0)->size();
			nz  = varz->get_dim(0)->size();
			//printf("nx=%d\n",nx);
			//printf("nx=%d\n",ny);
			//printf("nx=%d\n",nz);
			if (xx[0]==90.0) {
			  o[0]=xx[0]+0.5;
			}
			else {
			  o[0]=xx[0];
			}
			o[1]=yy[0],o[2]=zz[0];
			d[0]=xx[1]-o[0],d[1]=yy[1]-yy[0],d[2]=zz[1]-zz[0];
			//printf("%f\n",d[2]);
			n[0]=nx,n[1]=ny,n[2]=nz;
			//printf("%f\n",o[2]);
			nrec=nx*ny*nz;
			att=varx->get_att("units");
			if(att && strcmp(att->as_string(0),"degrees_east")== 0)
				while(o[0]>180)
					o[0]-=360;
			att=vary->get_att("units");
			if(att && strcmp(att->as_string(0),"degrees_east")== 0)
				while(o[1]>180)
					o[1]-=360;
			att=varz->get_att("units");
			if(att && strcmp(att->as_string(0),"degrees_east")== 0)
				while(o[2]>180)
					o[2]-=360;
			break;
		default :
			Msg::Fatal("the number of dimension of the netcdf data is %d\n",var->num_dims());
			break;
	}
	if(vart) {
                //printf("dans la condition time\n");
		uncoupledTime=true;
		timeDim=1;
		double toSecond=-1;
		if(vart->get_att("units")){
			std::string initialDateStr=vart->get_att("units")->as_string(0);
			if (initialDateStr.find("hours since",0)!=std::string::npos){
				initialDateStr=initialDateStr.substr(12,initialDateStr.size()-12);
				toSecond=3600;
			}
			if (initialDateStr.find("days since",0)!=std::string::npos){
				initialDateStr=initialDateStr.substr(10,initialDateStr.size()-10);
				toSecond=86400;	
			}
                        slimTmDate date;
                        int yy, mm, dd, hour, min; 
                        double sec;
                        sscanf(initialDateStr.c_str(),"%d-%d-%d %d:%d:%lf", &yy, &mm, &dd, &hour, &min, &sec);
                        vart->get(tt,2);
                        long julianDay=gregorianDateToJulianDay(yy,mm,dd) + tt[0]/24;
                        julianDayToGregorianDate(julianDay,&yy,&mm,&dd);
                        date.set(yy,mm,dd,hour,min,sec);
                        initialTime=date(); //convert the time since year 0 in hours to time since 1970 in seconds
                        shiftTime=(tt[1]-tt[0])*toSecond;

		}else{
                  printf("mettre qqch en plus\n");
                  initialTime = 0.;
                  shiftTime = 1.;
			//The initial date and shift date must be set with setInitialDate(double) and setShiftDate(double)
		}
		dataT0=new double[nrec];
		dataT1=new double[nrec];
		setData(dataT0,0);
		setData(dataT1,0);
		timeDependent=true;
	}else {
		data=new double[nrec];
		setData(data,0);
		timeDependent=false;
	}
	delete err;
}
#endif

//***************************************************************
double slimStructDataInterpolator::opfunction(slimStructDataContainer *container, double *xx, double time){
	if(container->uncoupledTime){
		double timeData=(time-container->initialTime)/container->shiftTime;
		int tId=floor(timeData);
		double tXi=timeData-tId;
		if(tId!=tIdOld){
			container->setData(container->dataT0,tId);
			container->setData(container->dataT1,tId+1);
		}
		tIdOld=tId;
		return container->addOffset+container->scaleFactor*((1-tXi)*getData(container,container->dataT0,xx)+tXi*getData(container,container->dataT1,xx));
	}else return container->addOffset+container->scaleFactor*getData(container,container->data,xx);
}
//***************************************************************
double slimStructDataInterpolatorGaussian::getData(slimStructDataContainer *container,double *data, double *xx){
  if(container->spaceDim==0)return data[0];
  addOffset=0;
  scaleFactor=1;  
  int dim=container->spaceDim;
  double *o=container->getOrigin();
  double *d=container->getDelta();
  int ll=0;
  int id[16];
  int cid[16];
  double xi[16];
  double xlat[94]={88.5420, 86.6531, 84.7532, 82.8508, 80.9473,79.0435, 77.1394, 75.2351, 73.3307, 71.4262, 69.5217, 67.6171, 65.7125,
  63.8079, 61.9033, 59.9986, 58.0939, 56.1893, 54.2846, 52.3799, 50.4752,48.5705, 46.6658, 44.7611, 42.8564, 40.9517, 39.0470, 37.1422, 35.2375,
  33.3328, 31.4281, 29.5234, 27.6186, 25.7139, 23.8092, 21.9044, 19.9997,18.0950, 16.1902, 14.2855, 12.3808, 10.4760, 8.5713, 6.6666, 4.7618, 2.8571,
  0.9524, -0.9524, -2.8571, -4.7618, -6.6666, -8.5713, -10.4760, -12.3808,-14.2855, -16.1902, -18.0950, -19.9997, -21.9044, -23.8092, -25.7139,
  -27.6186, -29.5234, -31.4281, -33.3328, -35.2375, -37.1422, -39.0470,-40.9517, -42.8564, -44.7611, -46.6658, -48.5705, -50.4752, -52.3799,
  -54.2846, -56.1893, -58.0939, -59.9986, -61.9033, -63.8079, -65.7125,-67.6171, -69.5217, -71.4262, -73.3307, -75.2351, -77.1394, -79.0435,
  -80.9473, -82.8508, -84.7532, -86.6531, -88.5420};
  int jj=0;
  for (int k=0; k<92;k++){
    if ((xx[0]>-xlat[k])&&((xx[0]+xlat[k])<(-xlat[k+1]+xlat[k]))){
      d[0]=(-xlat[k+1]+xlat[k]);
      xi[0]=(xx[0]+xlat[k])/d[0];
      xi[0]=std::min(1.,std::max(0.,xi[0]));
      jj=k;
    }
  }

  if (xx[0]==-xlat[0]){
    xi[0]=0;
    jj=0;
  }
  if (xx[0]>-xlat[93]) {
    xi[0]=1;
    jj=93;
    ll=1;
  }
  
  id[1]=(int)((xx[1]-o[1])/d[1]);
  xi[1]=(xx[1]-o[1]-id[1]*d[1])/d[1];
  xi[1]=std::min(1.,std::max(0.,xi[1]));
  
  double v=0;
  for(int i=0;i<1<<dim;i++){
    double f=1;
    for(int j=0;j<dim;j++){
      if(i&(1<<j)){
        if (j==1) {
           cid[1]=id[1];
        } else {
           cid[0]=jj;
        }
        f*=1-xi[j];
        if (ll==1){f*=0;}
      }else{
        if (j==1) {
          cid[1]=id[1]+1;
        } else {
          cid[0]=jj+1;
        }
        f*=xi[j];
        if (ll==1){f*=0;}
      }
    }
    double val=container->opfunction(data,cid);
    v+=f*val;
  }
  return addOffset+scaleFactor*(v);
}

double slimStructDataInterpolatorMultilinear::getData(slimStructDataContainer *container,double *data, double *xx){
	if(container->spaceDim==0)return data[0];
	int dim=container->spaceDim;
  int id[16];
  int cid[16];
  double xi[16];
  double *o=container->getOrigin();
  double *d=container->getDelta();
  int *n=container->getSize();
  for(int i=0;i<dim;i++){
    id[i]=(int)((xx[i]-o[i])/d[i]);
    xi[i]=(xx[i]-o[i]-id[i]*d[i])/d[i];
		if(_nanIfOutside){
			if (n[i]>1){
				xi[i]=(xx[i]<std::min(o[i],o[i]+(n[i]-1)*d[i])?NAN:xi[i]);
				xi[i]=(xx[i]>std::max(o[i],o[i]+(n[i]-1)*d[i])?NAN:xi[i]);
			}else
				xi[i]=0;
		}
		else
			xi[i]=std::min(1.,std::max(0.,xi[i]));
  }
  double v=0;
  double vref=0;
  for(int i=0;i<1<<dim;i++){
    double f=1;
    for(int j=0;j<dim;j++){
      if(i&(1<<j)){
        cid[j]=id[j];
        f*=1-xi[j];
      }else{
        cid[j]=id[j]+1;
        f*=xi[j];
      }
    }
    double val=container->opfunction(data,cid);
    if(_periodicity>0){
      if(i==0){
        vref=val;
      }else{
        while(val>vref+_periodicity/2){
         val-=_periodicity; 
        }
        while(val<vref-_periodicity/2){
         val+=_periodicity; 
        }
      }
    }
    v+=f*val;
  }
  return v;
}
/***************************************************************************/
slimStructDataInterpolatorBicubic::slimStructDataInterpolatorBicubic(){
  tIdOld=0;
}
double slimStructDataInterpolatorBicubic::get2dData(double *data,int i, int j)const
{
  int ii[2]={i,j};
  return container->opfunction(data,ii);
}

double slimStructDataInterpolatorBicubic::get2dDiff(double *data,int i, int j, int dim)const
{
  if((i == 0 && dim == 0) || (j == 0 && dim == 1))
    return (-3 * get2dData(data,i, j) + 4 * get2dData(data,i + 1 * (dim == 0), j + 1 * (dim == 1))
      - get2dData(data,i + 2 * (dim == 0), j + 2 * (dim == 1))) / (2 * d[dim]);
  if((i == (n[0] - 1) && dim == 0) || (j == (n[1] - 1) && dim == 1))
    return -(-3 * get2dData(data,i, j) + 4 * get2dData(data,i - 1 * (dim == 0), j + 1 * (dim == 1))
      - get2dData(data,i - 2 * (dim == 0), j + 2 * (dim == 1))) / (2 * d[dim]);
  return (get2dData(data,i + 1 * (dim == 0), j + 1 * (dim == 1))
    - get2dData(data,i - 1 * (dim == 0), j - 1 * (dim == 1))) / (2 * d[dim]);
}
double slimStructDataInterpolatorBicubic::get2dCrossDiff(double *data,int i, int j)const
{
  double dx1, dx2, dx3;
  if(j == 0) {
    dx1 = get2dDiff(data,i, j, 0);
    dx2 = get2dDiff(data,i, j + 1, 0);
    dx3 = get2dDiff(data,i, j + 2, 0);
    return (-3 * dx1 + 4 * dx2 - dx3) / (2 * d[1]);
  }
  if(j == (n[1] - 1)) {
    dx1 = get2dDiff(data,i, j, 0);
    dx2 = get2dDiff(data,i, j - 1, 0);
    dx3 = get2dDiff(data,i, j - 2, 0);
    return -(-3 * dx1 + 4 * dx2 - dx3) / (2 * d[1]);
  }
  dx1 = get2dDiff(data,i, j - 1, 0);
  dx2 = get2dDiff(data,i, j + 1, 0);
  return (dx2 - dx1) / (2 * d[1]);
}

void slimStructDataInterpolatorBicubic::bcucof(double y[], double y1[], double y2[], double y12[], double d1, double d2, double *c)const
{
  static int wt[16][16] = {
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
    {-3, 0, 0, 3, 0, 0, 0, 0, -2, 0, 0, -1, 0, 0, 0, 0},
    {2, 0, 0, -2, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 0, 0, -3, 0, 0, 3, 0, 0, 0, 0, -2, 0, 0, -1},
    {0, 0, 0, 0, 2, 0, 0, -2, 0, 0, 0, 0, 1, 0, 0, 1},
    {-3, 3, 0, 0, -2, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, -3, 3, 0, 0, -2, -1, 0, 0},
    {9, -9, 9, -9, 6, 3, -3, -6, 6, -6, -3, 3, 4, 2, 1, 2},
    {-6, 6, -6, 6, -4, -2, 2, 4, -3, 3, 3, -3, -2, -1, -1, -2},
    {2, -2, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0, 2, -2, 0, 0, 1, 1, 0, 0},
    {-6, 6, -6, 6, -3, -3, 3, 3, -4, 4, 2, -2, -2, -2, -1, -1},
    {4, -4, 4, -4, 2, 2, -2, -2, 2, -2, -2, 2, 1, 1, 1, 1}
  };
  int l, k, j, i;
  double xx, d1d2, cl[16], x[16];
  d1d2 = d1 * d2;
  for(i = 0; i < 4; i++) {
    x[i] = y[i];
    x[i + 4] = y1[i] * d1;
    x[i + 8] = y2[i] * d2;
    x[i + 12] = y12[i] * d1d2;
  }
  for(i = 0; i < 16; i++) {
    xx = 0.0;
    for(k = 0; k < 16; k++)
      xx += wt[i][k] * x[k];
    cl[i] = xx;
  }
  l = 0;
  for(i = 0; i < 4; i++) {
    for(j = 0; j < 4; j++) {
      c[i*4+j] = cl[l];
      l++;
    }
  }
}
double slimStructDataInterpolatorBicubic::getData(slimStructDataContainer *_container,double *data,double *xx){
  int dim=container->spaceDim;
  o=container->getOrigin();
  d=container->getDelta();
  n=container->getSize();
  // bicubic interpolation (2d only)
  // Following "PRESS W.H., FLANNERY B.P., TEUKOLSKY S.A., VETTERLING W.T.
  // Numerical recipes. The art of scientific computing, 1986", page 98 et seq.
  if(dim!=2)Msg::Fatal("bicubiv interpolation is only for 2d fields");
  int id[2];
  double xi[2];
  double xy[2];
  xy[0]=xx[0];
  if(dim<2)
    xy[1]=0;
  else
    xy[1]=xx[1];
  // get the ids of the neighboring points
  for(int i = 0; i < 2; i++) {
    id[i] = (int)((xy[i] - o[i]) / d[i]);
    xi[i] = (xy[i] - (o[i] + id[i] * d[i])) / d[i];
  }
  double dx[4], dy[4], dxy[4];
  // Computation of derivatives
  dx[0] = get2dDiff(data,id[0], id[1], 0);
  dx[1] = get2dDiff(data,id[0]+1, id[1], 0);
  dx[2] = get2dDiff(data,id[0]+1, id[1]+1, 0);
  dx[3] = get2dDiff(data,id[0], id[1]+1, 0);

  dy[0] = get2dDiff(data,id[0], id[1], 1);
  dy[1] = get2dDiff(data,id[0]+1, id[1], 1);
  dy[2] = get2dDiff(data,id[0]+1, id[1]+1, 1);
  dy[3] = get2dDiff(data,id[0], id[1]+1, 1);

  dxy[0] = get2dCrossDiff(data,id[0], id[1]);
  dxy[1] = get2dCrossDiff(data,id[0]+1, id[1]);
  dxy[2] = get2dCrossDiff(data,id[0]+1, id[1]+1);
  dxy[3] = get2dCrossDiff(data,id[0], id[1]+1);

  double Y[4] = {
    get2dData(data,id[0], id[1]),
    get2dData(data,id[0]+1, id[1]),
    get2dData(data,id[0]+1, id[1]+1),
    get2dData(data,id[0], id[1]+1)
  };
  double c[16];
  bcucof(Y, dx, dy, dxy, d[0], d[1], c);
  double v=0.;
  for(int i = 3; i >= 0; i--)
    v += xi[0] * (v) + ((c[i*4+3] * xi[1] + c[i*4+2]) * xi[1] + c[i*4+1]) * xi[1] + c[i*4];
  return v;
}
//***************************************************************
void slimFunctionStructData::call(dataCacheMap *m, fullMatrix<double> &val){
  for(int pt=0;pt<val.size1();pt++){
    double x[3]={coordF(pt,0),coordF(pt,1),coordF(pt,2)};
    val(pt,0)=(*_interp).opfunction(_container,x,timeF(pt,0));
  }
}
void slimFunctionStructData::setTimeFunction(const function *timeFunction){
  setArgument(timeF,timeFunction);
}
slimFunctionStructData::slimFunctionStructData(slimStructDataContainer *container, slimStructDataInterpolator *interp, const function *coordFunction): function(1){
  _container=container;
  _interp=interp;
  setArgument(coordF,coordFunction);
	minValue=-DBL_MAX;
	maxValue=+DBL_MAX;
}
