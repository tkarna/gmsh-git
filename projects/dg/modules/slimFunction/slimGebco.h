#ifndef _SLIM_GEBCO_H_
#define _SLIM_GEBCO_H_
#include "dgConfig.h"
#ifdef HAVE_NETCDF
/**Read the gebco bathymetry netcdf file generated with the interactive map available in https://www.bodc.ac.uk/data/online_delivery/gebco/select/ */
class slimFunctionGebco: public function {
	public:
	double *o,*d;
	int *n;
	slimStructDataContainerSimple* container;
	double *data;
	slimStructDataInterpolatorMultilinear *interp;
  fullMatrix<double> coordF,timeF;
	double minValue, maxValue;
/** fileName of a netcdf file containing the gebco bathymetru (see ncdump -h fileName and https://www.bodc.ac.uk/data/online_delivery/gebco/select/) */
	slimFunctionGebco(std::string fileName);
  void call(dataCacheMap *m,fullMatrix<double> &val);
/** coordFunction is a function returning the coordinate vector (lonLatDegrees for example) */
  void setCoordFunction(const function *coordFunction){
    setArgument(coordF,coordFunction);
  }
	void addMask(double *dataNoMask, double *dataMask, int nlon, int nlat);
/** set Minimal value after interpolation */
  void setMin(const double _minValue){
		minValue=_minValue;
  }
/** set Maximal value after interpolation */
  void setMax(const double _maxValue){
		maxValue=_maxValue;
  }
};
#endif
#endif
