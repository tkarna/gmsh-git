#ifndef _SLIM_TEMPORAL_SERIE_H_
#define _SLIM_TEMPORAL_SERIE_H
#include "slimDate.h"
#include "function.h"
#include <iostream>
#include <fstream>
class slimTemporalSerieIterator;
class slimDateInterpolator {
 public:
  slimDateInterpolator() {}
  virtual ~slimDateInterpolator() {}
  virtual std::vector<double> get(double time, slimTemporalSerieIterator &it) = 0;
  virtual void initialize(slimTemporalSerieIterator &it) = 0;
};

class slimDateInterpolatorLinear : public slimDateInterpolator {
 public:
  std::vector<double> get(double time, slimTemporalSerieIterator &it);
  void initialize(slimTemporalSerieIterator &it);
};


class slimFunctionTemporalSerie : public function {
  slimTemporalSerieIterator *_it;
  bool _datedData, _meanConservation, _ownInterp;
  slimDateInterpolator * _slimInterp;
  std::vector<double> _currData;
  double _currentTime;
  fullMatrix<double> time;
  double _scale;
 public:
  slimFunctionTemporalSerie(std::string fileName, const function *timeFunction = NULL, int dim=1, slimDateInterpolator * slimInterp=NULL);
  ~slimFunctionTemporalSerie();
  void usePeriodicYear();
  void call(dataCacheMap *m, fullMatrix<double> &val);
};

//rewrite from scratch, does not support periodicity nor vector but can go back in time (load the whole file in the constructor)
class slimTemporalSerie: public function{
  fullMatrix<double> _time;
  slimTmDate _initialTime;
  slimTmDate _currentTime, _nextTime;
  double _currentTimeD, _nextTimeD;
  slimTmDateShift _step;
  std::vector<double> _values;
  int _idx;
 public:
  slimTemporalSerie(const std::string filename);
  double get (double t);
  void call(dataCacheMap *m, fullMatrix<double> &val);
};

#endif
