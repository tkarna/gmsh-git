%module slimFunction
%{
  #undef HAVE_DLOPEN
  #include "dgConfig.h"
  #include "ncDataDz.h"
  #include "slimFunction.h"
  #include "slimStructData.h"
  #include "slimTpxo.h"
  #include "slimGebco.h"
  #include "slimTemporalSerie.h"
  #include "slimFields.h"
  #include "slimLonLat.h"
  #include "slimIterateHelper.h"
  #include "selfeNetcdfReader.h"
%}

%import(module="dgpy.dgCommon") "dgCommon.i"
%import(module="dgpy.dgFunction") "function.h"

%rename (_print) print;
%include "dgConfig.h"
%include "ncDataDz.h"
%include "slimFunction.h"
%include "slimStructData.h"
%include "slimTpxo.h"
%include "slimGebco.h"
%include "slimTemporalSerie.h"
%include "slimFields.h"
%include "slimLonLat.h"
%include "slimIterateHelper.h"
%include "selfeNetcdfReader.h"
