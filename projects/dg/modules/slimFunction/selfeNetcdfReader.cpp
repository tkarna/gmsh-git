#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include "slimStructData.h"
#include "GmshConfig.h"
#include "function.h"
#include "slimDate.h"
#include <iostream>
#include "dgConfig.h"
#include "selfeNetcdfReader.h"
#include "functionGeneric.h"

#ifdef HAVE_NETCDF
#include "netcdfcpp.h"

selfeTransectFunc::selfeTransectFunc(std::string fileName, std::string varName, std::string xName, std::string yName, std::string tName, const function * coordFunction, const function *timeFunction):function(1)
{
  ncFile = new NcFile(fileName.c_str(),NcFile::ReadOnly);
  if (!ncFile->is_valid())
    Msg::Fatal("couldn't open netcdf file %s!",fileName.c_str());
  dataVar = ncFile->get_var(varName.c_str());
  NcVar *xVar    = ncFile->get_var(xName.c_str());
  NcVar *yVar    = ncFile->get_var(yName.c_str());
  NcVar *tVar    = ncFile->get_var(tName.c_str());
  // dimensions
  nTime = tVar->get_dim(0)->size();
  nPoints = xVar->get_dim(0)->size();
  // store x,y
  timeArray.resize(nTime,0);
  xArray.resize(nPoints,0);
  yArray.resize(nPoints,0);
  dataArray.resize(nPoints,0);
  dataArrayOld.resize(nPoints,0);
  xVar->get(xArray.data(),nPoints);
  yVar->get(yArray.data(),nPoints);
  tVar->get(timeArray.data(),nTime);
  _xScale = _yScale = 1000.0;
  for (int i=0; i<nPoints; i++) {
    _xScale += xArray[i];
    _yScale += yArray[i];
  }
  _xScale /= nPoints;
  _yScale /= nPoints;

  _startTime = 0;
  _currentTime = -1;
  if(coordFunction)
    setArgument(coordF, coordFunction);
  if(timeFunction)
    setArgument(timeF, timeFunction);
}

void selfeTransectFunc::setTime(double time)
{ 
  if (_currentTime == time)
    return;
  double t = time + _startTime;
  if (t < timeArray[0])
    Msg::Fatal("Requested time before first point in the data set! %f %f",t,timeArray[0]);
  if (t > timeArray[nTime-1])
    Msg::Fatal("Requested time after last point in the data set! %f %f",t,timeArray[nTime-1]);
  int iTime=1;
  for (; iTime<nTime; iTime++)
    if (timeArray[iTime-1]<=t && t<=timeArray[iTime])
      break;
  dataVar->set_cur(0,iTime-1);
  dataVar->get(dataArrayOld.data(),nPoints,1);
  dataVar->set_cur(0,iTime);
  dataVar->get(dataArray.data(),nPoints,1);
  double weightNew=( t-timeArray[iTime-1] ) / ( timeArray[iTime]-timeArray[iTime-1] );
  double weightOld=1.0-weightNew;
  for (int i=0; i<nPoints; i++)
    dataArray[i] = dataArrayOld[i]*weightOld + dataArray[i]*weightNew;
  _currentTime = time;
}

selfeTransectFunc::~selfeTransectFunc()
{
  delete ncFile;
}

void selfeTransectFunc::call(dataCacheMap *m,fullMatrix<double> &val)
{
  double time = timeF(0,0);
  double TOL=1e-6;
  double xTol = TOL*_xScale;
  double yTol = TOL*_yScale;
  setTime(time);
  for(int pt=0;pt<val.size1();pt++){
    double x=coordF(pt,0);
    double y=coordF(pt,1);
    // find rectangle that contains (x,y)
    int iP=-1;
    for (int i=1; i<nPoints; i++) {
//       printf("%d x: %f %f y: %f %f\n",i,xArray[i-1], xArray[i], yArray[i-1], yArray[i]);
//       printf("%d x: %f %f y: %f %f\n",i,xArray[i-1]-x, xArray[i]-x, yArray[i-1]-y, yArray[i]-y);
//       printf("%d bool: %d %d %d %d\n",i,xArray[i-1]<=x+xTol, x-xTol<xArray[i], yArray[i-1]<=y+yTol, y-yTol<yArray[i]);
      double xMin = std::min(xArray[i-1],xArray[i]);
      double xMax = std::max(xArray[i-1],xArray[i]);
      double yMin = std::min(yArray[i-1],yArray[i]);
      double yMax = std::max(yArray[i-1],yArray[i]);
      bool allTrue = xMin<=x+xTol && x-xTol<xMax && yMin<=y+yTol && y-yTol<yMax;
      if (allTrue) {
        iP = i;
        break;
      }
    }
    if (iP < 0) { // not found
      printf("Warning point not on transect line %f %f\n",x,y);
      val(pt,0)=0.0;
    } else { // found
      // project (x,y) on line connecting i-1,i points
      // use projection to compute interpolation weights
      double dx = xArray[iP]-xArray[iP-1];
      double dy = yArray[iP]-yArray[iP-1];
      double dxP = x-xArray[iP-1];
      double dyP = y-yArray[iP-1];
      double weight2 = (dxP*dx+dyP*dy)/(dx*dx+dy*dy);
      double weight1 = 1.0-weight2;
      val(pt,0)=dataArray[iP-1]*weight1 + dataArray[iP]*weight2;
    }
  }
}
#endif
