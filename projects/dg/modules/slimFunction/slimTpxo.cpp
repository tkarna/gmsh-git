#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include "slimStructData.h"
#include "GmshConfig.h"
#include "function.h"
#include "slimDate.h"
#include <iostream>
#include "dgConfig.h"
#include "slimTpxo.h"
#include "dgDofContainer.h"
#include "functionGeneric.h"

#ifdef HAVE_NETCDF
#include "netcdfcpp.h"
slimFunctionTpxo::slimFunctionTpxo(std::string fileName,std::string amplitudeName, std::string phaseName, std::string lonName, std::string latName, const function * coordFunction, const function *timeFunction):function(1){

	std::map<std::string,double> freqMap,astro0Map;
	freqMap["m2 "]=1.405189e-04;  astro0Map["m2 "]=1.731557546;  
	freqMap["s2 "]=1.454441e-04;  astro0Map["s2 "]=0.000000000;  
	freqMap["n2 "]=1.378797e-04;  astro0Map["n2 "]=6.050721243;  
	freqMap["k2 "]=1.458423e-04;  astro0Map["k2 "]=3.487600001;  
	freqMap["k1 "]=7.292117e-05;  astro0Map["k1 "]=0.173003674; 
	freqMap["o1 "]=6.759774e-05;  astro0Map["o1 "]=1.558553872;  
	freqMap["p1 "]=7.252295e-05;  astro0Map["p1 "]=6.110181633;  
	freqMap["q1 "]=6.495854e-05;  astro0Map["q1 "]=5.877717569;  
	freqMap["mf "]=0.053234e-04;  astro0Map["mf "]=1.756042456;
	freqMap["mm "]=0.026392e-04;  astro0Map["mm "]=1.964021610; 
	freqMap["m4 "]=2.810378e-04;  astro0Map["m4 "]=3.463115091;   
	freqMap["ms4"]=2.859630e-04;  astro0Map["ms4"]=1.731557546;   
	freqMap["mn4"]=2.783984e-04;  astro0Map["mn4"]=1.499093481;   
	std::string argList[54]={"   ","sa ", "ssa", "mm ", "msf", "mf ", "lt ", "alpha1", "2q1", "sigma1", "q1 ", "rho1", "o1 ", "tau1", "m1 ", "chi1", "m_pi1", "p1 ", "s1 ", "k1 ", "psi1", "phi1", "theta1", "j1 ", "oo1", "2n2", "mu2", "n2 ", "nu2", "m2a", "m2 ", "m2b", "lambda2", "l2 ", "t2 ", "s2 ", "r2 ", "k2 ", "eta2", "mms2", "2sm2", "m3 ", "mk3", "s3 ", "mn4", "m4 ", "ms4", "mk4", "s4 ", "s5 ", "m6 ", "s6 ", "s7 ", "s8 "};
	std::map<std::string,int> mapArgList;
	for (int i=0; i<54; i++)
		mapArgList[argList[i]]=i;	

	ncFile = new NcFile(fileName.c_str(),NcFile::ReadOnly);
	if (!ncFile->is_valid())
		Msg::Fatal("couldn't open netcdf file %s!",fileName.c_str());
	NcVar *varAmp  = ncFile->get_var(amplitudeName.c_str());
	NcVar *varPha  = ncFile->get_var(phaseName.c_str());
	NcVar *varLon  = ncFile->get_var(lonName.c_str());
	NcVar *varLat  = ncFile->get_var(latName.c_str());
	NcVar *varComp = ncFile->get_var("con");

	o = new double[3];
	d = new double[3];
	n = new int[3];
	int nlon  = varLon->get_dim(0)->size();
	int nlat  = varLon->get_dim(1)->size();
	int nrec = nlon * nlat;
	double *lon = new double [nrec], *lat = new double [nrec];
	varLon->get(lon,nlon,nlat);
	varLat->get(lat,nlon,nlat);
	o[0]=lon[0],o[1]=lat[0],o[2]=0;
	d[0]=lon[nlon]-lon[0],d[1]=lat[1]-lat[0],d[2]=0;
	n[0]=nlon,n[1]=nlat,n[2]=1;

	bool needSphereMask=(lon[nrec-1]==360 && lon[0]==d[0]);
	lon_0_360=(lon[nrec-1]>180);
        delete []lon;
        delete []lat;
	double *dataAmpNoMask=NULL;
	double *dataPhaNoMask=NULL;
	if(needSphereMask){
		dataAmpNoMask=new double[nlon*nlat];
		dataPhaNoMask=new double[nlon*nlat];
		o[0]=0.;
		n[0]=nlon+1;
		nrec=n[0]*n[1]*n[2];
	}
	NC= varComp->get_dim(0)->size();
	int NCp= varComp->get_dim(1)->size();
	char *con=new char[NCp];
	//get the component list, frequencies and initial astro arguments
	for (int nc=0;nc<NC;nc++){
		varComp->set_cur(nc,0);
		varComp->get(con,1,NCp);
		con[NCp-1]='\0';
		compList.push_back(con);

		freq.push_back(freqMap[compList[nc]]);
		astro0.push_back(astro0Map[compList[nc]]);
		indiceNodal.push_back(mapArgList[compList[nc]]);
	}

	//get the amplitude data
	for (int nc=0;nc<NC;nc++){
		dataAmp.push_back(new double[nrec]);
		varAmp->set_cur(nc,0,0);
		if(needSphereMask){
			varAmp->get(dataAmpNoMask,1,nlon,nlat);
			addMask(dataAmpNoMask,dataAmp[nc],nlon,nlat);
		}else
			varAmp->get(dataAmp[nc],1,nlon,nlat);
	}

	//get the phase data
	for (int nc=0;nc<NC;nc++){
		dataPha.push_back(new double[nrec]);
		varPha->set_cur(nc,0,0);
		if(needSphereMask){
			varPha->get(dataPhaNoMask,1,nlon,nlat);
			addMask(dataPhaNoMask,dataPha[nc],nlon,nlat);
		}else
			varPha->get(dataPha[nc],1,nlon,nlat);
	}

	//create the containers
	for (int nc=0;nc<NC;nc++){
		containerAmp.push_back(new slimStructDataContainerSimple(dataAmp[nc],o,d,n));
		containerPha.push_back(new slimStructDataContainerSimple(dataPha[nc],o,d,n));
	}

	//set interpolators
	interpAmp=new slimStructDataInterpolatorMultilinear();
  interpAmp->setNanIfOutside(false);
	interpPha=new slimStructDataInterpolatorMultilinear();
  interpPha->setNanIfOutside(false);
	interpPha->setPeriodicity(360.);


	//set relative time
	slimTmDate tm1992;
	tm1992.tmYear=92;                       
	tm1992.tmMDay=1;                        
	relTime1992=slimMkTime(&tm1992);           

	//set nodal correction and astronomical argument (time dependant)
	nodal=new double[54];
	astro=new double[54];

	if(needSphereMask){
		delete[] dataAmpNoMask;
		delete[] dataPhaNoMask;
	}

  _useDofContainers=false;
  if(coordFunction)
    setArgument(coordF, coordFunction);
  _coordFunction = coordFunction;
  if(timeFunction)
    setArgument(timeF, timeFunction);
}

void slimFunctionTpxo::call(dataCacheMap *m,fullMatrix<double> &val){
	for(int pt=0;pt<val.size1();pt++){
		double lon=coordF(pt,0);
		lon=(lon_0_360&&lon<containerAmp[0]->o[0]?360+lon:lon);
		double x[3]={lon,coordF(pt,1), 0.};
		double tpxoTime = timeF(pt,0)-relTime1992;
		astronomical_argument_and_nodal_correction(tpxoTime,nodal,astro);
		val(pt,0)=0;
    double amp,pha;
		for (int nc=0;nc<NC;nc++){
      if(_useDofContainers){
        amp = ampFullMatrices[nc](pt,0);
        pha = phaFullMatrices[nc](pt,0)*M_PI/180;
      }else{
        amp=(*interpAmp).opfunction(containerAmp[nc],x,0.0);
        pha=(*interpPha).opfunction(containerPha[nc],x,0.0)*M_PI/180;
      }
			double u=astro[indiceNodal[nc]]*M_PI/180;
			double f=nodal[indiceNodal[nc]];
			val(pt,0)+=amp*f*cos(freq[nc]*tpxoTime+astro0[nc]+u-pha);
		}
	}
}

void slimFunctionTpxo::addMask(double *dataNoMask, double *dataMask, int nlon, int nlat){
	for (int i=0;i<nlon;i++)
		for (int j=0;j<nlat;j++)
			dataMask[j+(nlat)*(i+1)]=dataNoMask[j+nlat*i];
	for (int j=0;j<nlat;j++)
		dataMask[j+nlat*0]=dataNoMask[j+nlat*(nlon-1)];	
}

void slimFunctionTpxo::useDofContainers(dgGroupCollection *groups){
  _useDofContainers=true;
  function *fzero = new functionConstant(0.);
  slimFunctionStructData *ampFuns, *phaFuns;
  ampFullMatrices.resize(NC);
  phaFullMatrices.resize(NC);
	for (int nc=0;nc<NC;nc++){
    ampFuns = new slimFunctionStructData(containerAmp[nc],interpAmp, _coordFunction);
    ampFuns->setTimeFunction(fzero);
    phaFuns = new slimFunctionStructData(containerPha[nc],interpPha, _coordFunction);
    phaFuns->setTimeFunction(fzero);
    ampDofContainers.push_back(new dgDofContainer(*groups, 1));
    phaDofContainers.push_back(new dgDofContainer(*groups, 1));
    ampDofContainers[nc]->interpolate(ampFuns);
    phaDofContainers[nc]->interpolate(phaFuns);
    setArgument(ampFullMatrices[nc],ampDofContainers[nc]->getFunction());
    setArgument(phaFullMatrices[nc],phaDofContainers[nc]->getFunction());
    delete ampFuns;
    delete phaFuns;
	}
  delete ncFile;
	for (int nc=0;nc<NC;nc++){
    delete[] dataAmp[nc];
    delete[] dataPha[nc];
  }
  delete fzero;

  //avoid the interpolation between 0 and 360 for the phases.
  
  for (int nc=0;nc<NC;nc++){
    for (int iGroup = 0; iGroup < groups->getNbElementGroups(); iGroup++) {
      const dgGroupOfElements &group = *groups->getElementGroup(iGroup);
      fullMatrix<double> proxy;
      for (size_t iElement = 0 ; iElement < group.getNbElements(); ++iElement) {
        phaDofContainers[nc]->getGroupProxy(iGroup).getBlockProxy(iElement, proxy);
        double vref = proxy(0, 0) ;
        for (int iPt = 1; iPt < group.getNbNodes(); iPt++) {
          while( proxy(iPt, 0) > vref + 180)
            proxy(iPt,0) -= 360; 
          while( proxy(iPt, 0) < vref - 180)
            proxy(iPt,0) += 360; 
        }
      }
    }
    char fileAmp[20];
    char filePha[20];
    sprintf(fileAmp,"output/ampDof_%02d",nc);
    sprintf(filePha,"output/phaDof_%02d",nc);
  }
}

slimFunctionTpxo::~slimFunctionTpxo(){
  printf("delete tpxo\n");
  if(!_useDofContainers){
	  for(int nc=0;nc<NC;nc++){
	  	delete[] dataAmp[nc];
	  	delete[] dataPha[nc];
	  }
	  delete interpAmp;
	  delete interpPha;
	  delete ncFile;
  }else{
	  for(int nc=0;nc<NC;nc++){
	  	delete ampDofContainers[nc];
	  	delete phaDofContainers[nc];
	  }
  }
	delete[] nodal;
	delete[] astro;
}

void astronomical_argument_and_nodal_correction(double time1, double *f, double *u){
	// based on:
	// ARGUMENTS and ASTROL FORTRAN subroutines SUPPLIED by RICHARD RAY, March 1999
	// and tje matlab remake of ARGUMENTS by Lana Erofeeva, Jan 2003
	double tmp1,tmp2,temp1,temp2;
	double cosn,cos2n,sinn,sin2n,sin3n;
	double rad=M_PI/180.;
	double s,h,p,omega;
	time1=time1/86400+48622;
	//
	//     Determine equilibrium arguments
	//     -------------------------------
	astrol( time1, s, h, p, omega );
	/*
     double hour, t1, t2;
     hour = (time1 - int(time1))*24;
     t1 = 15.0*hour;
     t2 = 30.0*hour;
		 arg[ 0] = 0;
		 arg[ 1] = h - pp;                                  // Sa
		 arg[ 2] = 2.0*h;                                   // Ssa
		 arg[ 3] = s - p;                                   // Mm
		 arg[ 4] = 2.0*s - 2.0*h;                           // MSf
		 arg[ 5] = 2.0*s;                                   // Mf
		 arg[ 6] = 3.0*s - p;                             // Mt
		 arg[ 7] = t1 - 5.0*s + 3.0*h + p - 90.0;      // alpha1
		 arg[ 8] = t1 - 4.0*s + h + 2.0*p - 90.0;        // 2Q1
		 arg[ 9] = t1 - 4.0*s + 3.0*h - 90.0;          // sigma1
		 arg[10] = t1 - 3.0*s + h + p - 90.0;           // q1
		 arg[11] = t1 - 3.0*s + 3.0*h - p - 90.0;     // rho1
		 arg[12] = t1 - 2.0*s + h - 90.0;                 // o1
		 arg[13] = t1 - 2.0*s + 3.0*h + 90.0;           // tau1
		 arg[14] = t1 - s + h + 90.0;                     // M1
		 arg[15] = t1 - s + 3.0*h - p + 90.0;           // chi1
		 arg[16] = t1 - 2.0*h + pp - 90.0;                // M_PI1
		 arg[17] = t1 - h - 90.0;                         // p1
		 arg[18] = t1 + 90.0;                             // s1
		 arg[19] = t1 + h + 90.0;                         // k1
		 arg[20] = t1 + 2.0*h - pp + 90.0;                // psi1
		 arg[21] = t1 + 3.0*h + 90.0;                   // phi1
		 arg[22] = t1 + s - h + p + 90.0;                 // theta1
		 arg[23] = t1 + s + h - p + 90.0;                 // J1
		 arg[24] = t1 + 2.0*s + h + 90.0;                 // OO1
		 arg[25] = t2 - 4.0*s + 2.0*h + 2.0*p;             // 2N2
		 arg[26] = t2 - 4.0*s + 4.0*h;                    // mu2
		 arg[27] = t2 - 3.0*s + 2.0*h + p;                // n2
		 arg[28] = t2 - 3.0*s + 4.0*h - p;               // nu2
		 arg[29] = t2 - 2.0*s + h + pp;                     // M2a
		 arg[30] = t2 - 2.0*s + 2.0*h;                      // M2
		 arg[31] = t2 - 2.0*s + 3.0*h - pp;               // M2b
		 arg[32] = t2 - s + p + 180.;                     // lambda2
		 arg[33] = t2 - s + 2.0*h - p + 180.;             // L2
		 arg[34] = t2 - h + pp;                             // t2
		 arg[35] = t2;                                      // S2
		 arg[36] = t2 + h - pp + 180.;                    // R2
		 arg[37] = t2 + 2.0*h;                              // K2
		 arg[38] = t2 + s + 2.0*h - pp;                     // eta2
		 arg[39] = t2 - 5.0*s + 4.0*h + p;                 // MNS2
		 arg[40] = t2 + 2.0*s - 2.0*h;                      // 2SM2
		 arg[41] = 1.5*arg[30];                            // M3
		 arg[42] = arg[19] + arg[30];                       // MK3
		 arg[43] = 3.0*t1 ;                               // S3
		 arg[44] = arg[27] + arg[30];                       // MN4
		 arg[45] = 2.0*arg[30];                             // M4
		 arg[46] = arg[30] + arg[35];                       // MS4
		 arg[47] = arg[30] + arg[37];                       // MK4
		 arg[48] = 4.0*t1;                                 // S4
		 arg[49] = 5.0*t1;                                 // S5
		 arg[50] = 3.0*arg[30];                           // M6
		 arg[51] = 3.0*t2;                                // S6
		 arg[52] = 7.0*t1;                                  // S7
		 arg[53] = 4.0*t2;                                 // S8
	 */
	//
	//     determine nodal corrections f and u 
	//     -----------------------------------
	sinn = sin(omega*rad);
	cosn = cos(omega*rad);
	sin2n = sin(2.0*omega*rad);
	cos2n = cos(2.0*omega*rad);
	sin3n = sin(3.0*omega*rad);
	f[ 0] = 0;
	f[ 1] = 1.0;                                     // Sa
	f[ 2] = 1.0;                                     // Ssa
	f[ 3] = 1.0 - 0.130*cosn;                        // Mm
	f[ 4] = 1.0;                                     // MSf
	f[ 5] = 1.043 + 0.414*cosn;                      // Mf
	f[ 6] = sqrt(pow2((1.0+.203*cosn+.040*cos2n))+pow2(.203*sinn+.040*sin2n));  // Mt
	f[ 7] = 1.0;                                     // alpha1
	f[ 8] = sqrt(pow2(1.+.188*cosn)+pow2(.188*sinn));  // 2Q1
	f[ 9] = f[8];                                    // sigma1
	f[10] = f[8];                                    // q1
	f[11] = f[8];                                    // rho1
	f[12] = sqrt(pow2(1.0+0.189*cosn-0.0058*cos2n)+pow2(0.189*sinn-0.0058*sin2n));   // O1
	f[13] = 1.0;                                     // tau1
	//    tmp1  = 2.*cos(p*rad)+.4*cos((p-omega)*rad)
	//    tmp2  = sin(p*rad)+.2*sin((p-omega)*rad)         // Doodson's
	tmp1  = 1.36*cos(p*rad)+.267*cos((p-omega)*rad);  // Ray's
	tmp2  = 0.64*sin(p*rad)+.135*sin((p-omega)*rad);
	f[14] = sqrt(tmp1*tmp1 + tmp2*tmp2);                 // M1
	f[15] = sqrt(pow2(1.+.221*cosn)+pow2(.221*sinn));  // chi1
	f[16] = 1.0;                                     // M_PI1
	f[17] = 1.0;                                     // P1
	f[18] = 1.0;                                     // S1
	f[19] = sqrt(pow2(1.+.1158*cosn-.0029*cos2n)+pow2(.1554*sinn-.0029*sin2n));  // K1
	f[20] = 1.0;                                     // psi1
	f[21] = 1.0;                                     // phi1
	f[22] = 1.0;                                     // theta1
	f[23] = sqrt(pow2(1.+.169*cosn)+pow2(.227*sinn));  // J1
	f[24] = sqrt(pow2(1.0+0.640*cosn+0.134*cos2n)+pow2(0.640*sinn+0.134*sin2n));  // OO1
	f[25] = sqrt(pow2(1.-.03731*cosn+.00052*cos2n)+pow2(.03731*sinn-.00052*sin2n));     // 2N2
	f[26] = f[25];                                   // mu2
	f[27] = f[25];                                   // N2
	f[28] = f[25];                                   // nu2
	f[29] = 1.0;                                    // M2a
	f[30] = f[25];                                   // M2
	f[31] = 1.0;                                     // M2b
	f[32] = 1.0;                                     // lambda2
	temp1 = 1.-0.25*cos(2.0*p*rad)-0.11*cos((2.0*p-omega)*rad)-0.04*cosn;
	temp2 = 0.25*sin(2.0*p)+0.11*sin((2.0*p-omega)*rad)+ 0.04*sinn;
	f[33] = sqrt(temp1*temp1 + temp2*temp2);               // L2
	f[34] = 1.0;                                     // t2
	f[35] = 1.0;                                     // S2
	f[36] = 1.0;                                     // R2
	f[37] = sqrt(pow2(1.+.2852*cosn+.0324*cos2n)+pow2(.3108*sinn+.0324*sin2n));   // K2
	f[38] = sqrt(pow2(1.+.436*cosn)+pow2(.436*sinn));  // eta2
	f[39] = pow2(f[30]);                                // MNS2
	f[40] = f[30];                                   // 2SM2
	f[41] = 1.0;   // wrong                           // M3
	f[42] = f[19]*f[30];                             // MK3
	f[43] = 1.0;                                     // S3
	f[44] = pow2(f[30]);                             // MN4
	f[45] = f[44];                                   // M4
	f[46] = f[44];                                   // MS4
	f[47] = f[30]*f[37];                             // MK4
	f[48] = 1.0;                                     // S4
	f[49] = 1.0;                                     // S5
	f[50] = f[30]*f[30]*f[30];                       // M6
	f[51] = 1.0;                                     // S6
	f[52] = 1.0;                                     // S7
	f[53] = 1.0;                                     // S8

	u[ 0] = 0;
	u[ 1] = 0.0;                                   // Sa
	u[ 2] = 0.0;                                   // Ssa
	u[ 3] = 0.0;                                   // Mm
	u[ 4] = 0.0;                                   // MSf
	u[ 5] = -23.7*sinn + 2.7*sin2n - 0.4*sin3n;      // Mf
	u[ 6] = atan(-(.203*sinn+.040*sin2n)/(1.0+.203*cosn+.040*cos2n))/rad;   // Mt
	u[ 7] = 0.0;                                    // alpha1
	u[ 8] = atan(.189*sinn/(1.+.189*cosn))/rad;      // 2Q1
	u[ 9] = u[8];                                    // sigma1
	u[10] = u[8];                                    // q1
	u[11] = u[8];                                    // rho1
	u[12] = 10.8*sinn - 1.3*sin2n + 0.2*sin3n;       // O1
	u[13] = 0.0;                                    // tau1
	u[14] = atan2(tmp2,tmp1)/rad;                    // M1
	u[15] = atan(-.221*sinn/(1.+.221*cosn))/rad;     // chi1
	u[16] = 0.0;                                    // M_PI1
	u[17] = 0.0;                                    // P1
	u[18] = 0.0;                                    // S1
	u[19] = atan((-.1554*sinn+.0029*sin2n)/(1.+.1158*cosn-.0029*cos2n))/rad;   // K1
	u[20] = 0.0;                                    // psi1
	u[21] = 0.0;                                    // phi1
	u[22] = 0.0;                                    // theta1
	u[23] = atan(-.227*sinn/(1.+.169*cosn))/rad;     // J1
	u[24] = atan(-(.640*sinn+.134*sin2n)/(1.+.640*cosn+.134*cos2n))/rad;     // OO1
	u[25] = atan((-.03731*sinn+.00052*sin2n)/(1.-.03731*cosn+.00052*cos2n))/rad; // 2N2
	u[26] = u[25];                                   // mu2
	u[27] = u[25];                                   // N2
	u[28] = u[25];                                   // nu2
	u[29] = 0.0;                                  // M2a
	u[30] = u[25];                                   // M2
	u[31] = 0.0;                                    // M2b
	u[32] = 0.0;                                    // lambda2
	u[33] = atan(-temp2/temp1)/rad;                  // L2
	u[34] = 0.0;                                    // t2
	u[35] = 0.0;                                    // S2
	u[36] = 0.0;                                    // R2
	u[37] = atan(-(.3108*sinn+.0324*sin2n)/(1.+.2852*cosn+.0324*cos2n))/rad;   // K2
	u[38] = atan(-.436*sinn/(1.+.436*cosn))/rad;     // eta2
	u[39] = u[30]*2.0;                               // MNS2
	u[40] = u[30];                                   // 2SM2
	u[41] = 1.5*u[30];                             // M3
	u[42] = u[30] + u[19];                           // MK3
	u[43] = 0.0;                                    // S3
	u[44] = u[30]*2.0;                               // MN4
	u[45] = u[44];                                   // M4
	u[46] = u[30];                                   // MS4
	u[47] = u[30]+u[37];                             // MK4
	u[48] = 0.0;                                    // S4
	u[49] = 0.0;                                    // S5
	u[50] = u[30]*3.0;                             // M6
	u[51] = 0.0;                                    // S6
	u[52] = 0.0;                                    // S7
	u[53] = 0.0;                                    // S8
}
void astrol( double time, double &s, double &h, double &p, double &N){
	//  Computes the basic astronomical mean longitudes  s, h, p, N.
	//  Note N is not N', i.e. N is decreasing with time.
	//  These formulae are for the period 1990 - 2010, and were derived
	//  by David Cartwright (personal comm., Nov. 1990).
	//  time is UTC in decimal MJD.
	//  All longitudes returned in degrees.
	//  R. D. Ray    Dec. 1990
	double circle=360;
	double T = time - 51544.4993;
	// mean longitude of moon
	// ----------------------
	s = 218.3164 + 13.17639648 * T;
	// mean longitude of sun
	// ---------------------
	h = 280.4661 +  0.98564736 * T;
	// mean longitude of lunar perigee
	// -------------------------------
	p =  83.3535 +  0.11140353 * T;
	// mean longitude of ascending lunar node
	// --------------------------------------
	N = 125.0445 -  0.05295377 * T;

	s = fmod(s,circle);
	h = fmod(h,circle);
	p = fmod(p,circle);
	N = fmod(N,circle);

	s=(s<0?s+circle:s);
	p=(p<0?p+circle:p);
	h=(h<0?h+circle:h);
	N=(N<0?N+circle:N);
	return;
}
#endif
