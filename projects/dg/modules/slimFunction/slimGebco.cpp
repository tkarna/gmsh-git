#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include "slimStructData.h"
#include "GmshConfig.h"
#include "function.h"
#include "slimDate.h"
#include <iostream>
#include "dgConfig.h"
#include "slimGebco.h"

#ifdef HAVE_NETCDF
#include "netcdfcpp.h"
slimFunctionGebco::slimFunctionGebco(std::string fileName):function(1){
	NcFile *ncFile = new NcFile(fileName.c_str(),NcFile::ReadOnly);
	if (!ncFile->is_valid())
		Msg::Fatal("couldn't open netcdf file %s!",fileName.c_str());
	NcError *err= new NcError(NcError::silent_nonfatal);
	NcVar *var  = ncFile->get_var("z");
	NcVar *varDx = ncFile->get_var("spacing");
	NcVar *varDim = ncFile->get_var("dimension");
	NcVar *varLon  = ncFile->get_var("x_range");
	NcVar *varLat  = ncFile->get_var("y_range");
	double xRange[2], yRange[2];
	varLon->get(xRange,2);
	varLat->get(yRange,2);
	o = new double[3];
	o[0]=xRange[0],o[1]=yRange[0],o[2]=0;
	d = new double[3];
	varDx->get(d,2);
	d[2]=0;
	n = new int[3];
	varDim->get(n,2);
	n[2]=1;
	int nlon=n[0];
	int nlat=n[1];
	int nrec = nlon*nlat;
	data=new double[nrec];

	double * dataBrut = new double[nrec];
	var->get(dataBrut,nrec);
	for (int i=0;i<nlon;i++){
		for (int j=0;j<nlat;j++){
			data[(nlat-j-1)+nlat*i]=dataBrut[i+nlon*j];
		}
	}

	container = new slimStructDataContainerSimple(data,o,d,n);
	delete err;
	delete ncFile;

	//set interpolators
	interp=new slimStructDataInterpolatorMultilinear();

	//default min and max values
	minValue=-DBL_MAX;
	maxValue=DBL_MAX;
  delete [] dataBrut;
}

void slimFunctionGebco::call(dataCacheMap *m,fullMatrix<double> &val){
	for(int pt=0;pt<val.size1();pt++){
		double lon=coordF(pt,0);
		double lat=coordF(pt,1);
		double x[3]={lon,lat,coordF(pt,2)};
		double value=(*interp).opfunction(container,x,0.0);
		val(pt,0)=std::max(minValue,std::min(maxValue,value));
	}
}
void slimFunctionGebco::addMask(double *dataNoMask, double *dataMask, int nlon, int nlat){
	for (int i=0;i<nlon;i++)
		for (int j=0;j<nlat;j++)
			dataMask[j+(nlat)*(i+1)]=dataNoMask[j+nlat*i];
	for (int j=0;j<nlat;j++)
		dataMask[j+nlat*0]=dataNoMask[j+nlat*(nlon-1)];	
}
#endif
