#include "GmshConfig.h"
#include <sstream>
#include <fstream>
#include "function.h"
#include "slimStructData.h"
#include "functionGeneric.h"
#include "slimFunction.h"
#include "SPoint3.h"
#include "dgFunctionEvaluator.h"
#include "dgFunctionIntegratorInterface.h"
#include "dgConfig.h"

// functions used in slim
void slimFunctionLonLatVector2XYZ::call(dataCacheMap *m, fullMatrix<double> &val) {
  for(int pt=0;pt<val.size1();pt++){
    double lon=coordF(pt,xid)*factor;
    double lat=coordF(pt,yid)*factor;
    double uLat=vectorF(pt,xid);
    double uLon=vectorF(pt,yid);
    val(pt,0) = -uLat*sin(lat)*cos(lon)-uLon*cos(lat)*sin(lon);
    val(pt,1) = -uLat*sin(lat)*sin(lon)+uLon*cos(lat)*cos(lon);
    val(pt,2) =  uLat*cos(lat);
  }
}
slimFunctionLonLatVector2XYZ::slimFunctionLonLatVector2XYZ(const function *vectorFunction, const std::string coordFormat, const function *coordFunction): function(3) {
  xid=yid=0;
  factor=1;
  if(coordFormat=="latLon"){
    xid=1;
    yid=0;
    factor=1;
  }else if(coordFormat=="latLonDegrees") {
    xid=1;
    yid=0;
    factor=M_PI/180;
  }else if(coordFormat=="lonLat") {
    xid=0;
    yid=1;
    factor=1;
  }else if(coordFormat=="lonLatDegrees") {
    xid=0;
    yid=1;
    factor=M_PI/180;
  }
  else {
    Msg::Fatal("wrong coordinate format for slimFunctionLonLatVector2XYZ\n");
  }
  setArgument(vectorF,vectorFunction);
  setArgument(coordF,coordFunction);
}
//***************************************************************
void slimFunctionXYZ2LatLon::call(dataCacheMap *m, fullMatrix<double> &val){
  for(int pt=0;pt<val.size1();pt++){
    double x=coordF(pt,0), y=coordF(pt,1), z=coordF(pt,2);
    double r=sqrt(x*x+y*y+z*z);
    val(pt,xid) = asin(z/r)*factor;
    val(pt,yid) = atan2(y,x)*factor;
    val(pt,2) = r;
  }
}
slimFunctionXYZ2LatLon::slimFunctionXYZ2LatLon(const std::string coordFormat, const function *coordFunction): function(3) {
  xid=yid=0;
  factor=1;
  if(coordFormat=="latLon"){
    xid=0;
    yid=1;
    factor=1;
  }else if(coordFormat=="latLonDegrees") {
    xid=0;
    yid=1;
    factor=180/M_PI;
  }else if(coordFormat=="lonLat") {
    xid=1;
    yid=0;
    factor=1;
  }else if(coordFormat=="lonLatDegrees") {
    xid=1;
    yid=0;
    factor=180/M_PI;
  }
  else {
    Msg::Fatal("wrong coordinate format for slimFunctionXYZ2LatLon\n");
  }
  setArgument(coordF,coordFunction);
}
//***************************************************************
void slimFunctionImbricate::call(dataCacheMap *m,  fullMatrix<double> &val){
  for(int pt=0;pt<val.size1();pt++){
    val(pt,0)=(subF(pt,0)!=subF(pt,0))?mainF(pt,0):subF(pt,0);
  }
}
slimFunctionImbricate::slimFunctionImbricate(const function *fMain, const function *fSub): function(1){
  setArgument(mainF,fMain);
  setArgument(subF,fSub);
}
//***************************************************************
void slimFunctionETOPO1::call(dataCacheMap *m,  fullMatrix<double> &val)
{
  for (int i = 0; i < val.size1(); ++i) {
    double lon = lonlatdegrees(i, 0);
    double lat = lonlatdegrees(i, 1);
    int ix = (lon + 180) / _dx;
    int iy = (90 - lat) / _dx;
    double xi = (lon - (ix * _dx - 180)) / _dx;
    double eta = (90  - iy * _dx - lat) / _dx;
    int16_t b[4] = {_values[ix + iy * _nx], _values[(ix + 1) + iy * _nx], _values[ix + 1 + (iy + 1) * _nx], _values[ix + (iy + 1) * _nx]};
    val(i, 0) = -((1 - xi) * (1 - eta) * b[0] + xi * (1 - eta) * b[1] + xi * eta * b[2] + (1 - xi) * eta * b[3]);
    if (val(i, 0) < _vmin)
      val(i, 0) = _vmin;
    if (val(i, 0) > _vmax)
      val(i, 0) = _vmax;
  }
}
slimFunctionETOPO1::~slimFunctionETOPO1()
{
  delete [] _values;
}

slimFunctionETOPO1::slimFunctionETOPO1(const char *filename, const function *flonlatdeg, double vmin, double vmax):function(1)
{
  setArgument(lonlatdegrees, flonlatdeg);
  FILE *f = fopen(filename, "rb");
  _nx = 21601;
  _ny = 10801;
  _dx = 1./60;
  _vmin = vmin;
  _vmax = vmax;
  _values = new int16_t[_nx * _ny];
  if (fread((void*) _values, sizeof(int16_t), _nx * _ny, f) != _nx * _ny)
    Msg::Fatal("Cannot read the correct size of data");
  fclose(f);
}
//***************************************************************
void slimFunctionBathyScheldtDelft::call(dataCacheMap *m,  fullMatrix<double> &val)
{
  for (int i = 0; i < val.size1(); ++i) {
    double lon = lonlatdegrees(i, 0);
    double lat = lonlatdegrees(i, 1);
    int ix = (lon - _x0) / _dx;
    int iy = (lat - _y0) / _dy;
    if (ix < 0 || ix >= _nx || iy < 0 || iy >= _ny)
    	val(i, 0) = _vmax;
    else {
	    double xi = (lon - (ix * _dx + _x0)) / _dx;
  	  double eta = (lat - (iy * _dy + _y0)) / _dy;
    	double b[4] = {_values[ix * _ny + iy], _values[(ix + 1) * _ny + iy], _values[(ix + 1) * _ny + (iy + 1)], _values[ix * _ny + (iy + 1)]};
    	val(i, 0) = ((1 - xi) * (1 - eta) * b[0] + xi * (1 - eta) * b[1] + xi * eta * b[2] + (1 - xi) * eta * b[3]);
    	if (val(i, 0) < _vmin)
      	val(i, 0) = _vmin;
    	if (val(i, 0) > _vmax)
      	val(i, 0) = _vmax;
    }
  }
}
slimFunctionBathyScheldtDelft::~slimFunctionBathyScheldtDelft()
{
  delete [] _values;
}

slimFunctionBathyScheldtDelft::slimFunctionBathyScheldtDelft(const char *filename, const function *flonlatdeg, double vmin, double vmax):function(1)
{
  setArgument(lonlatdegrees, flonlatdeg);
  FILE *f = fopen(filename, "rb");
  size_t dum;
  dum = fread (&_x0, sizeof(double), 1, f);
  dum = fread (&_y0, sizeof(double), 1, f);
  dum = fread (&_z0, sizeof(double), 1, f);
  dum = fread (&_dx, sizeof(double), 1, f);
  dum = fread (&_dy, sizeof(double), 1, f);
  dum = fread (&_dz, sizeof(double), 1, f);
  dum = fread (&_nx, sizeof(int), 1, f);
  dum = fread (&_ny, sizeof(int), 1, f);
  dum = fread (&_nz, sizeof(int), 1, f);
  _vmin = vmin;
  _vmax = vmax;
  _values = new double[_nx * _ny];
  dum += fread((void*) _values, sizeof(double), _nx * _ny, f);
  fclose(f);
}













//***************************************************************
void slimFunctionBathyScheldtDelftAndETOPO1::call(dataCacheMap *m,  fullMatrix<double> &val)
{
  for (int i = 0; i < val.size1(); ++i) {
    double lon = lonlatdegrees(i, 0);
    double lat = lonlatdegrees(i, 1);
    if (lon < _x0D || lon > _x0D + _dxD * _nxD || lat < _y0D || lat > _y0D + _dyD * _nyD) {
	    int ix = (lon + 180) / _dxE;
  	  int iy = (90 - lat) / _dxE;
    	double xi = (lon - (ix * _dxE - 180)) / _dxE;
    	double eta = (90  - iy * _dxE - lat) / _dxE;
    	int16_t b[4] = {_valuesE[ix + iy * _nxE], _valuesE[(ix + 1) + iy * _nxE], _valuesE[ix + 1 + (iy + 1) * _nxE], _valuesE[ix + (iy + 1) * _nxE]};
    	val(i, 0) = -((1 - xi) * (1 - eta) * b[0] + xi * (1 - eta) * b[1] + xi * eta * b[2] + (1 - xi) * eta * b[3]);
    }
    else {
    	int ix = (lon - _x0D) / _dxD;
    	int iy = (lat - _y0D) / _dyD;
	    double xi = (lon - (ix * _dxD + _x0D)) / _dxD;
  	  double eta = (lat - (iy * _dyD + _y0D)) / _dyD;
    	double b[4] = {_valuesD[ix * _nyD + iy], _valuesD[(ix + 1) * _nyD + iy], _valuesD[(ix + 1) * _nyD + (iy + 1)], _valuesD[ix * _nyD + (iy + 1)]};
    	val(i, 0) = ((1 - xi) * (1 - eta) * b[0] + xi * (1 - eta) * b[1] + xi * eta * b[2] + (1 - xi) * eta * b[3]);
    }
    if (val(i, 0) < _vmin)
      val(i, 0) = _vmin;
    if (val(i, 0) > _vmax)
      val(i, 0) = _vmax;
  }
}
slimFunctionBathyScheldtDelftAndETOPO1::~slimFunctionBathyScheldtDelftAndETOPO1()
{
  delete [] _valuesD;
  delete [] _valuesE;
}

slimFunctionBathyScheldtDelftAndETOPO1::slimFunctionBathyScheldtDelftAndETOPO1(const char *filenameD, const char *filenameE, const function *flonlatdeg, double vmin, double vmax):function(1)
{
  setArgument(lonlatdegrees, flonlatdeg);
  // ETOPO file
  FILE *fE = fopen(filenameE, "rb");
  _nxE = 21601;
  _nyE = 10801;
  _dxE = 1./60;
  _valuesE = new int16_t[_nxE * _nyE];
  size_t dum;
  dum = fread((void*) _valuesE, sizeof(int16_t), _nxE * _nyE, fE);
  fclose(fE);
  // Delft file
  FILE *fD = fopen(filenameD, "rb");
  dum = fread (&_x0D, sizeof(double), 1, fD);
  dum = fread (&_y0D, sizeof(double), 1, fD);
  dum = fread (&_z0D, sizeof(double), 1, fD);
  dum = fread (&_dxD, sizeof(double), 1, fD);
  dum = fread (&_dyD, sizeof(double), 1, fD);
  dum = fread (&_dzD, sizeof(double), 1, fD);
  dum = fread (&_nxD, sizeof(int), 1, fD);
  dum = fread (&_nyD, sizeof(int), 1, fD);
  dum = fread (&_nzD, sizeof(int), 1, fD);
  _valuesD = new double[_nxD * _nyD];
  dum += fread((void*) _valuesD, sizeof(double), _nxD * _nyD, fD);
  fclose(fD);
  // min et max values
  _vmin = vmin;
  _vmax = vmax;
}

#ifdef HAVE_PROJ
/** Class to project between different co-ordinate systems using the PROJ library */
slimProjector::slimProjector(const std::string sourceProjText, const std::string destinationProjText) {
  if ( !(_pj_sourceProj = pj_init_plus( sourceProjText.c_str() )) )  {
    printf("Error initialising source projection\n");
    exit(1); }
  if ( !(_pj_destinationProj = pj_init_plus( destinationProjText.c_str() )) ) {
    printf("Error initialising destination projection\n");
    exit(1); }
}
  
/** Project a point from source system into destination system.
  Note: if projecting to/from lonLat, must supply value in RADIANS and supply first LON(x) then LAT(y)*/
void slimProjector::projectPoint(fullMatrix<double> *src_coords, fullMatrix<double> *dest_coords){
  int er=0;
  double x_src = src_coords->get(0,0);
  double y_src = src_coords->get(1,1);
  if ( (er = pj_transform( _pj_sourceProj, _pj_destinationProj, 1, 1, &x_src, &y_src, NULL )) != 0) {
    printf ("Transform failed: %s\n", pj_strerrno (er));
    exit(1);
  }
  dest_coords->set(0,0, x_src);
  dest_coords->set(1,1, y_src);
}
#endif

void reefDistance::getNearestReefMap(dgDofContainer* distToReefsDC, dgDofContainer* directionToReefsDC)
{
  //Initialise vector w/ x(even elements) and y(odd elements) of reef element barycentres
  std::vector<double> reefElementCentres;
  //Initialise vector w/ 1x x(even elements) and 1x y(odd elements) per reef element node. Vector elements (0->5 are for reef Element 1 etc.)
  std::vector<double> reefElementNodes;
  printf("reefElementCentres has size %d, ie. %d reef elements.\n", static_cast<int>( reefElementCentres.size() ), static_cast<int>( reefElementCentres.size() )/2);
  //set up reefsDC fnEvalr
  dgFunctionEvaluator reefsEval(_groups, _reefsDC->getFunction());

  int nbGroups = _groups->getNbElementGroups();
  for (int groupId = 0; groupId < nbGroups; groupId++) {
    const dgGroupOfElements *groupOfElements = _groups->getElementGroup(groupId);
    int nbElements = groupOfElements->getNbElements();
    printf("getNearestReefMap:: Group %d contains %d elements\n", groupId, nbElements);

//    FILE* outCentres = fopen("outCentres.dat", "w");
    //Loop over all elements to build reef element centres & nodes lists
    for (int elementId=0; elementId<nbElements; elementId++) {
      //Evaluate reef map in this element:
      MElement *el = groupOfElements->getElement(elementId);
      if (!el->isInside(0.25, 0.25, 0.0)) printf("NearestReefMap Problem: evaluating reef map at point NOT in element.\n");
      fullMatrix<double> res;
      reefsEval.computeElem(groupId, elementId, 0.25, 0.25, 0.0, res);
      int reefId = static_cast<int>(res(0,0));

      if (reefId == 0) continue;
      //We are on a reef
      SPoint3 centre = el->barycenter();
      reefElementCentres.push_back(centre.x());
      reefElementCentres.push_back(centre.y());
//      fprintf(outCentres, "%.10f %.10f\n", centre.x(), centre.y());
      //Loop over all 3 vertices:
      for (int vertexNb=0; vertexNb<el->getNumVertices(); vertexNb++) {
        MVertex* vert = el->getVertex(vertexNb);
        reefElementNodes.push_back(vert->x());
        reefElementNodes.push_back(vert->y());
      }
    }

    printf("reefElementCentres vector has size %d, ie. %d reef elements.\n", static_cast<int>( reefElementCentres.size() ), static_cast<int>( reefElementCentres.size() )/2);

    //Get fullVector list of x,y of mesh nodes, and of reef map
    fullVector<double> XYfV = _XY_DC->getVector();
    fullVector<double> reefsFV = _reefsDC->getVector();

    //Get fullVector lists of distToReefsDC and directionToReefsDC
    fullVector<double>& distToReefsFV = distToReefsDC->getVector();
    fullVector<double>& directionToReefsFV = directionToReefsDC->getVector();

//       printf("Length of fullVectors:\n XY(Z): %d, reefsFV: %d, distToReefsFV: %d, directionToReefsFV: %d\n", static_cast<int>(XYfV.size()), static_cast<int>(reefsFV.size()), static_cast<int>(distToReefsFV.size()), static_cast<int>(directionToReefsFV.size()));
//       printf("XYZ: 1st 4 nodes are %f, %f, %f, %f\nXYZ: seperated nodes are %f, %f, %f\n", XYfV(0), XYfV(1), XYfV(2), XYfV(3), XYfV(0), XYfV(XYfV.size()/3), XYfV((XYfV.size()*2)/3));

    //Loop over all elements on DC
    int nodesPerElement=3;
    int nbFieldsXYZ = XYfV.size()/distToReefsFV.size();
    int nbFieldsDirToReefs = directionToReefsFV.size()/distToReefsFV.size();
    printf("XYZ DC has %d fields & dir2Reefs has %d...\n", nbFieldsXYZ, nbFieldsDirToReefs);
    for (int element=0; element<distToReefsFV.size()/nodesPerElement; element++) {
      //Loop over all nodes in element
      for (int node=0; node<nodesPerElement; node++) {
        double node_x = XYfV(element*nbFieldsXYZ*nodesPerElement + node);
        double node_y = XYfV(element*nbFieldsXYZ*nodesPerElement + node + nodesPerElement*1);
        //If we are on a reef, distance and direction are 0
        if (reefsFV(element*nodesPerElement + node) > 0) {
          distToReefsFV(element*nodesPerElement + node) = 0;
          directionToReefsFV(element*nbFieldsDirToReefs*nodesPerElement + node) = 0;
          directionToReefsFV(element*nbFieldsDirToReefs*nodesPerElement + node + nodesPerElement*1) = 0;
          continue;
        }
        //Step 1 - Loop over every reef element, calc. distance to its centre and record distance to closest centre, and reef element nb.
        double minDist = 10000000.0, minDistToReefEdge = 10000000.0;
        int minDistReefElem=-1;
        for (size_t reefElem=0; reefElem<reefElementCentres.size()/2; reefElem++) {   //NB: each reefEl has 2 entries in vector; x&y
          double x_dist, y_dist, dist;
          x_dist = reefElementCentres[reefElem*2] - node_x;
          y_dist = reefElementCentres[reefElem*2 + 1] - node_y;
          dist = sqrt(x_dist*x_dist + y_dist*y_dist);
          if (dist < minDist) {
            minDist = dist;
            minDistReefElem = reefElem;
          }
        }

        //Step 2 - Loop over closest reef element's nodes and calc. distance to intersection between successive nodes & myPos to element centre
        // NB: Step 2 is so that distToReefDC measures distance to reef's edge and not centre
        SPoint2 intersectionPoint;
        SPoint2 myPosition(node_x, node_y);
        SPoint2 myClosestReefCentre(reefElementCentres[minDistReefElem*2], reefElementCentres[minDistReefElem*2 + 1]);
//        printf("\n distance to nearest reef element centre: x:%f, y:%f\n", myClosestReefCentre.x()-myPosition.x(), myClosestReefCentre.y()-myPosition.y());
//        printf(" closest reef element is nb %d\n",minDistReefElem);
        for (int reefNode=0; reefNode<nodesPerElement; reefNode++) {
          int intersectionFlag = 0;
          SPoint2 reefNodeA(reefElementNodes[minDistReefElem*nodesPerElement*2+2*reefNode], reefElementNodes[minDistReefElem*nodesPerElement*2+2*reefNode + 1]);
          SPoint2 reefNodeB(reefElementNodes[minDistReefElem*nodesPerElement*2+2*((reefNode+1)%3)], reefElementNodes[minDistReefElem*nodesPerElement*2+2*((reefNode+1)%3) + 1]);
          
          intersectionPoint = intersection2(myPosition, myClosestReefCentre, reefNodeA, reefNodeB, intersectionFlag);
//          printf("Intersection flag is:%d\n", intersectionFlag);
          if (intersectionFlag != 1) continue;
          double x_dist, y_dist;
          x_dist = intersectionPoint.x() - node_x;
          y_dist = intersectionPoint.y() - node_y;
          //Now convert Point-->Reef (stereo)vector to 3D cartesian coords, to get distance in metres
//           printf("_R is %f\n", _R);
          /*if (_R > 1.0) {
            double stereoCoords[2], stereoVector[2], xyzVector[2];
            stereoCoords[0] = node_x;
            stereoCoords[1] = node_y;
            stereoVector[0] = x_dist;
            stereoVector[1] = y_dist;
            printf("old vector dist: %f", sqrt(x_dist*x_dist + y_dist*y_dist));
            //stereoVectorToCartesian(stereoCoords, stereoVector, xyzVector, _R);
            velocity3D(stereoCoords, stereoVector, xyzVector, _R);
            minDistToReefEdge = sqrt(xyzVector[0]*xyzVector[0] + xyzVector[1]*xyzVector[1]);
            printf("   new vector dist: %f\n", minDistToReefEdge);
          }
          else*/ minDistToReefEdge = sqrt(x_dist*x_dist + y_dist*y_dist);
        }
        
        //printf("Calculated distances to closest reef.\n -> Distance to reef centre: %fkm\n -> Distance to nearest edge: %fkm\n", minDist/1000.0, minDistToReefEdge/1000.0);

        //printf("On node %d. Nearest reef is %fkm away.\n", node, minDist/1000.0);

        //Now get unit vector pointing to nearest reef elem
        double x_dist_unit = (reefElementCentres[minDistReefElem*2] - node_x)/minDist;
        double y_dist_unit = (reefElementCentres[minDistReefElem*2 + 1] - node_y)/minDist;

        //Save to DCs:
        distToReefsFV(element*nodesPerElement + node) = minDistToReefEdge;
        directionToReefsFV(element*nbFieldsDirToReefs*nodesPerElement + node) = x_dist_unit;
        directionToReefsFV(element*nbFieldsDirToReefs*nodesPerElement + node + nodesPerElement*1) = y_dist_unit;
      }
      //End of loop over mesh nodes
      }
    //End of loop over elements
  }
  //End of loop over mesh elements
}

/**A class to load data from a reef map, and then access it through a function-class object.
    The function projects the reef map onto the mesh by resolving the map at a given point. */
reefsSmooth::reefsSmooth(const std::string& filename, const function *lonLatDegrees) : function(1){
  setArgument(_lonLatDegrees,lonLatDegrees);
  FILE *input = fopen(filename.c_str(), "r");
  if (! input) printf("Error: Cannot open Reef Map.");
  _nbReefsOnDc = 0;
  
  double buf;
  fscanf(input, "%lf %lf %lf %lf %lf %lf %d %d %d", &_Ox, &_Oy, &buf, &_dx, &_dy, &buf, &_nx, &_ny, &_nz);
  printf( "Initialising Reef Map data: Ox: %e Oy: %e dx: %e dy: %e Nx: %d Ny: %d\n", _Ox, _Oy, _dx, _dy, _nx, _ny);
//     double latRange = (double)_ny*_dy;
//     printf ("Latitude range is %.15g degrees or %d units.\n", latRange, (int)(latRange/_dy));
  _data = new int[_nx*_ny];
  int row=0, col=0;
  for (row=0; row<_ny; row++) {
    for (col=0; col<_nx; col++) {
      fscanf(input, "%d", &_data[row*_nx+col]);
    }
  }
  fclose(input);
  printf("Reefs data loaded into memory.\n");
}

/** Generate SMOOTH reefsDC (evaluate reef map at lonLatDegrees points) */
void reefsSmooth::call(dataCacheMap *cash, fullMatrix<double> &reefsFM) {
  for (int i=0; i<reefsFM.size1(); i++) {
    double rx = _lonLatDegrees(i,0)-_Ox;
    double ry = _lonLatDegrees(i,1)-_Oy;
    int iX = rx/_dx;
    int iY = ry/_dy;
    
    //Smooth and clip:
//         double alpha = (rx - iX*_dx) / _dx;
//         double beta = (ry - iY*_dy) / _dy;
//         double b = 
//         (1-alpha)*(1-beta)*(double)_data[iY*_nx+iX]
//         +alpha*(1-beta)*(double)_data[(iY+1)%_ny*_nx+iX]
//         +alpha*beta*(double)_data[(iY+1)%_ny*_nx+(iX+1)%_nx]
//         +(1-alpha)*beta*(double)_data[iY*_nx+(iX+1)%_nx];
//         printf("alpha is %f, beta is %f, b is %f\n", alpha, beta, b);
    
//         if (b<0) b=0;
//         else if (b<100) b = b + 5*exp(-b/5.0);
//         else if (b < 210) b = b - 10*exp((b-210.)/10.);
//         else b = 200;
    
    if ( iY < 0 || iX < 0 || iY > _ny || iX > _nx ) { //If we are below range of map, there are no reefs.  
      reefsFM.set (i, 0, 0.0); 
      continue;
    }
    reefsFM.set ( i, 0, _data[iY*_nx + iX] );
    if ( i==0 && _data[iY*_nx + iX] != 0) _nbReefsOnDc++;
  }
}

/**A class to load data from a reef map, and then access it through a function-class object.
    The function projects the reef map onto the mesh by resolving the map at each element's barycentre 
    -> Using OLD PROJECTION SYSTEM */
reefsRaw_oldProj::reefsRaw_oldProj(const std::string& filename, double RadiusForPlaneProj) : function(1){
  if (RadiusForPlaneProj<0.1) {
    printf("ERROR! Radius given to reefsRaw_oldProj is only %f!\n", RadiusForPlaneProj);
    exit(EXIT_FAILURE);
  }
  _R = RadiusForPlaneProj;
  FILE *input = fopen(filename.c_str(), "r");
  if (! input) printf("Error: Cannot open Reef Map.");
  _nbReefsOnDc = 0;
  
  double buf;
  fscanf(input, "%lf %lf %lf %lf %lf %lf %d %d %d", &_Ox, &_Oy, &buf, &_dx, &_dy, &buf, &_nx, &_ny, &_nz);
  printf( "Initialising Reef Map data: Ox: %e Oy: %e dx: %e dy: %e Nx: %d Ny: %d\n", _Ox, _Oy, _dx, _dy, _nx, _ny);
//     double latRange = (double)_ny*_dy;
//     printf ("Latitude range is %.15g degrees or %d units.\n", latRange, (int)(latRange/_dy));
  _data = new int[_nx*_ny];
  int row=0, col=0;
  for (row=0; row<_ny; row++) {
    for (col=0; col<_nx; col++) {
      fscanf(input, "%d", &_data[row*_nx+col]);
    }
  }
  fclose(input);
  
  //Projection params:
  _pi = acos(-1);
  double phi = 146.5892/180*_pi;
  double theta = -17.147569/180*_pi;
  _pOx = cos(theta)*cos(phi)*_R;
  _pOy = cos(theta)*sin(phi)*_R;
  _pOz = sin(theta)*_R;
  _pPhiX = -sin(phi);
  _pPhiY = cos(phi);
  _pPhiZ = 0;
  _pThetaX = -sin(theta)*cos(phi);
  _pThetaY = -sin(theta)*sin(phi);
  _pThetaZ = cos(theta);
  
  printf("Reefs data loaded into memory.\n");
}

/** Generate RAW reefsDC (evaluate reef map at element's barycentre) */
void reefsRaw_oldProj::call(dataCacheMap *cash, fullMatrix<double> &reefsFM) {
  for(int iE = 0; iE < cash->nElement(); ++iE) {
    int ElementId;
    const dgGroupOfElements *group = cash->getGroupOfElements();
    ElementId = cash->elementId(iE);
    MElement *e = group->getElement(ElementId);
    SPoint3 elemCentre = e->barycenter();
    double lon, lat;

    //elemCentre is in (x,y)plane; need to convert it to lonLat
    double x = _pPhiX * elemCentre.x() + _pThetaX * elemCentre.y() + _pOx;
    double y = _pPhiY * elemCentre.x() + _pThetaY * elemCentre.y() + _pOy;
    double z = _pPhiZ * elemCentre.x() + _pThetaZ * elemCentre.y() + _pOz;
    double r = sqrt(x*x+y*y+z*z);
    lon = atan2(y,x)*180.0/_pi;
    lat = asin(z/r)*180.0/_pi;

    double rx = lon-_Ox;
    double ry = lat-_Oy;
    int iX = rx/_dx;
    int iY = ry/_dy;
    //       printf("element: %d || iX = %d || iY = %d\n", ElementId, iX, iY);
    for (int i=0; i<cash->nPointByElement(); i++) {
      if ( iY < 0 ) { reefsFM.set (i, 0, 0.0); continue;}  //If we are below range of map, there are no reefs.
      reefsFM(iE * cash->nPointByElement() + i, 0) = static_cast<double>( _data[iY*_nx + iX] );
      if ( i==0 && _data[iY*_nx + iX] != 0) _nbReefsOnDc++;
    }
  }
}

#ifdef HAVE_PROJ
/**A class to load data from a reef map, and then access it through a function-class object.
    The function projects the reef map onto the mesh by resolving the map at each element's barycentre 
    -> Using PROJ projection library */
reefsRaw::reefsRaw(const std::string& filename, slimProjector* utm_to_lonlat) : function(1){
  printf("Projecting reef map onto mesh using PROJ projection library\n");
  _utm_to_lonlat = utm_to_lonlat;
  FILE *input = fopen(filename.c_str(), "r");
  if (! input) printf("Error: Cannot open Reef Map.\n");
  _nbReefsOnDc = 0;
  _pi = acos(-1);
  
  double buf;
  fscanf(input, "%lf %lf %lf %lf %lf %lf %d %d %d", &_Ox, &_Oy, &buf, &_dx, &_dy, &buf, &_nx, &_ny, &_nz);
  printf( "Initialising Reef Map data: Ox: %e Oy: %e dx: %e dy: %e Nx: %d Ny: %d\n", _Ox, _Oy, _dx, _dy, _nx, _ny);
//     double latRange = (double)_ny*_dy;
//     printf ("Latitude range is %.15g degrees or %d units.\n", latRange, (int)(latRange/_dy));
  _data = new int[_nx*_ny];
  int row=0, col=0;
  for (row=0; row<_ny; row++) {
    for (col=0; col<_nx; col++) {
      fscanf(input, "%d", &_data[row*_nx+col]);
    }
  }
  fclose(input);
  printf("Reefs data loaded into memory.\n");
}

/** Generate RAW reefsDC (evaluate reef map at element's barycentre) */
void reefsRaw::call(dataCacheMap *cash, fullMatrix<double> &reefsFM) {
  for (int iE = 0; iE < cash->nElement(); ++iE) {
    int ElementId;
    const dgGroupOfElements *group = cash->getGroupOfElements();
    ElementId = cash->elementId(iE);
    MElement *e = group->getElement(ElementId);
    SPoint3 elemCentre = e->barycenter();
    fullMatrix<double> xyz(2,2), lonlat(2,2);
    xyz.set(0,0, elemCentre.x());
    xyz.set(1,1, elemCentre.y());

    //elemCentre is in (x,y)mesh; need to convert it to lonLat
    _utm_to_lonlat->projectPoint(&xyz, &lonlat);
    double rx = (lonlat(0,0)*180.0/_pi)-_Ox;
    double ry = (lonlat(1,1)*180.0/_pi)-_Oy;
    int iX = rx/_dx;
    int iY = ry/_dy;
    //       printf("element: %d || iX = %d || iY = %d\n", ElementId, iX, iY);
    int nP = cash->nPointByElement();
    for (int i=0; i<nP; i++) {
      if ( iY < 0 ) { reefsFM.set (i, 0, 0.0); continue;}  //If we are below range of map, there are no reefs.
      reefsFM.set ( iE * nP + i, 0, static_cast<double>( _data[iY*_nx + iX] ) );
      if ( i==0 && _data[iY*_nx + iX] != 0) _nbReefsOnDc++;
    }
  }
}
#endif

/**A class to load data from a bathymetry file, and then access it through a function-class object.*/
bathy::bathy(const std::string& filename, const function *lonLatDegrees) : function(1){
  setArgument(_lonLatDegrees,lonLatDegrees);
  FILE *input = fopen(filename.c_str(), "r");
  double buf;

  fread (&_Ox, sizeof(double), 1, input);
  fread (&_Oy, sizeof(double), 1, input);
  fread (&buf, sizeof(double), 1, input);
  fread (&_dx, sizeof(double), 1, input);
  fread (&_dy, sizeof(double), 1, input);
  fread (&buf, sizeof(double), 1, input);
  fread (&_nx, sizeof(int), 1, input);
  fread (&_ny, sizeof(int), 1, input);
  fread (&buf, sizeof(int), 1, input);
  
  printf("Bath info: Ox: %.6g, Oy: %.6g, dx: %.15g, dy: %.15g, Nx: %d, Ny: %d\n", _Ox, _Oy, _dx, _dy, (int)_nx, (int)_ny);
  _data = new float[_nx*_ny];
  size_t check;
  check = fread(_data, sizeof(float), _nx*_ny, input);
  if ((int)check != _nx*_ny) {
    printf( "Bath info: reading data from file failed. Bytes read: %d, total expected: %d\n", (int)(_nx*_ny*sizeof(float)), _nx*_ny*(int)(sizeof(float)) );
  }
  printf("Bath info: bathymetry data loaded into memory. Bytes read: %d MB\n", _nx*_ny*(int)(sizeof(float))/(1024*1024));
  fclose(input);
}
  
/**Set the bathymetry*/
void bathy::call(dataCacheMap *m, fullMatrix<double> &bathymetry) {
  for (int i = 0; i< bathymetry.size1(); i++) {
    double rx = (_lonLatDegrees(i,0)-_Ox);
    double ry = (_lonLatDegrees(i,1)-_Oy);
    int iX = rx/_dx;
    int iY = ry/_dy;
    
    if ( iX > _nx || iY > _ny || iY < 0 || iX < 0 ) { //If we are to the right of bathymetry range, set const depth
      bathymetry.set (i, 0, 200); 
      continue;
    }
    
    double alpha = (rx - iX*_dx) / _dx;
    double beta = (ry - iY*_dy) / _dy;
    double b = 
    (1-alpha)*(1-beta)*_data[iY*_nx+iX]
    +alpha*(1-beta)*_data[(iY+1)%_ny*_nx+iX]
    +alpha*beta*_data[(iY+1)%_ny*_nx+(iX+1)%_nx]
    +(1-alpha)*beta*_data[iY*_nx+(iX+1)%_nx];
    b = -b;
    
    if (b<0) b=5;
    else if (b<100) b = b + 5*exp(-b/5.0);
    else if (b < 210) b = b - 10*exp((b-210.)/10.);
    else b = 200;
    bathymetry.set (i, 0, b);
    //    bathymetry.set (i, 0, -_data[iY*_nx + iX]);
  }
}

SPoint2 intersection2(SPoint2 p1, SPoint2 p2, SPoint2 p3, SPoint2 p4, int& flag)
{
  double x1,y1;
  double x2,y2;
  double x3,y3;
  double x4,y4;
  double ua,ub;
  double num_ua;
  double num_ub;
  double denom;
  double e;
  x1 = p1.x();
  y1 = p1.y();
  x2 = p2.x();
  y2 = p2.y();
  x3 = p3.x();
  y3 = p3.y();
  x4 = p4.x();
  y4 = p4.y();
  e = 0.000001;
  denom = (y4-y3)*(x2-x1) - (x4-x3)*(y2-y1);
  if(fabs(denom)<e){
    flag = 0;
    return SPoint2(0.0,0.0);
  }
  num_ua = (x4-x3)*(y1-y3) - (y4-y3)*(x1-x3);
  num_ub = (x2-x1)*(y1-y3) - (y2-y1)*(x1-x3);
  ua = num_ua/denom;
  ub = num_ub/denom;
  if(ua<=1.0 && ua>=0.0 && ub<=1.0 && ub>=0.0){
    flag = 1;
    return SPoint2(x1+ua*(x2-x1),y1+ua*(y2-y1));
  }
  else{
    flag = 0;
    return SPoint2(0.0,0.0);
  }
}

slimCubicWindStress::slimCubicWindStress(const function &wind, double f1, double f2, double f3):function(2)
{
  _f1 = f1;
  _f2 = f2;
  _f3 = f3;
  setArgument(_wind, &wind);
}

void slimCubicWindStress::call(dataCacheMap *, fullMatrix<double> &val) 
{
  for (int i = 0; i < val.size1(); i++) {
    double u = _wind(i, 0);
    double v = _wind(i, 1);
    double n = hypot(u, v);
    double f = _f1 + _f2 * n + _f3 * n * n;
    val(i, 0) = f * u ;
    val(i, 1) = f * v;
  }
}

slimTransportToVelocityFunction::slimTransportToVelocityFunction(const function &transport, const function &bath, const function &sse):function(2)
{
  setArgument(_UV, &transport);
  setArgument(_bath, &bath);
  setArgument(_sse, &sse);
}


void slimTransportToVelocityFunction::call(dataCacheMap *, fullMatrix<double> &val) 
{
  for (int i = 0; i < val.size1(); i++) {
    val(i, 0) = _UV(i,0)/(_bath(i,0) + _sse(i,0));
    val(i, 1) = _UV(i,1)/(_bath(i,0) + _sse(i,0));
  }
}

slimFlowRateToVelocity::slimFlowRateToVelocity(dgGroupCollection &group2d, const std::string &bndTag, const function &flowRate, const function &bath, const function &eta):function(2)
{
  setArgument(_flowRate, &flowRate);
  setArgument(_bath, &bath);
  setArgument(_eta, &eta);
  setArgument(_normal, function::getNormals());
  dgFunctionIntegratorInterface integ(&group2d, &bath);
  fullMatrix<double> section(1, 1);
  integ.compute(bndTag, section);
  _section = section(0, 0);
}

void slimFlowRateToVelocity::call(dataCacheMap *, fullMatrix<double> &val) 
{
  for (int i = 0; i < val.size1(); ++i) 
  {
    double H = _bath(i, 0) + _eta(i, 0);
    double un = _flowRate(i, 0) / _section * _bath(i, 0) / H;
    val(i, 0) = -un * _normal(i, 0);
    val(i, 1) = -un * _normal(i, 1);
  }
}
