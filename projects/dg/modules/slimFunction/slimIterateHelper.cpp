#include "slimIterateHelper.h"
#if defined (WIN32)&& !defined(__CYGWIN__)
  #include <io.h>
#endif
#include <sys/stat.h>
#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include <fstream>

static std::string getTimeStr(int type, slimTmDate & date) {
  switch(type) {
    case 3:
      return date.toString().c_str();
    case 2:
      return date.toStringDuration().c_str();
    case 1:
    {
      char num[32];
      sprintf(num, "%e", date());
      return std::string(num);
    }
  }
  return "bad time format !";
}

void slimIterateHelper::setInitialDate(long long int year,long long int month,long long int day,long long int hour,long long int min,double sec) {
  if(!_initSet) {
    _absoluteDate.set(year,month,day,hour,min,sec);
    _beginDate = _absoluteDate;
    _lastRegularExport = _absoluteDate;
    _exportDate.insert(&_beginDate);
    _initSet = true;
  }
  else
    Msg::Fatal("initial date already set !");
}

void slimIterateHelper::setFinalDate(long long int year,long long int month,long long int day,long long int hour,long long int min,double sec) {
  if(!_finalSet) {
    _finalDate.set(year,month,day,hour,min,sec);
    _exportDate.insert(&_finalDate);
    _finalSet = true;
  }
  else
    Msg::Fatal("final date already set !");
}

double slimIterateHelper::getNextTimeStepAndExport() {
  _hasExported = false;
  if(!_initSet || !_finalSet)
    Msg::Fatal("Use of setInitialDate and setFinalDate before all !");
  if(_exportDate.size() <= 0)
    Msg::Fatal("End already reached ! Use a while loop with the isTheEnd() boolean function ! [in python: while(not ih.isTheEnd()):]");
  double nearestExceptionalExport = (*_exportDate.begin())->operator()() - _absoluteDate();
  slimTmDate nextRegularExport = _lastRegularExport + _exportStep;
  double nearestRegularExport = nextRegularExport() - _absoluteDate();
  double nearestExport = std::min(nearestExceptionalExport, nearestRegularExport) ;
  //export or not?

  if(nearestRegularExport < _minDt || nearestExceptionalExport < _minDt) {
  //TODO make relative error && if 2 steps very close => bug?nearestExport<0
    exportAll();

    //moving forward
    while(nearestExceptionalExport < _minDt)
    {
      _exportDate.erase(_exportDate.begin()); //TODO are objects deleted ??
      if(_exportDate.size() > 0)
        nearestExceptionalExport = (*_exportDate.begin())->operator()() - _absoluteDate();
      else
        nearestExceptionalExport = 1e100;
    }
    if(nearestRegularExport < _minDt)
      _lastRegularExport += _exportStep;

    //recompute
    nextRegularExport = _lastRegularExport + _exportStep;
    nearestRegularExport = nextRegularExport() - _absoluteDate();
    nearestExport = std::min(nearestExceptionalExport, nearestRegularExport);
  }
  //next time step
  long long int nbStepsFromExport = (long long int) ceil( nearestExport * .999999 / _maxDt);
  if(nbStepsFromExport < 0) // if nbStepsFromExport > max(long long int)
    Msg::Fatal("Thinking your stuff is not feasible in your life with given args. Check the quality of your mesh elements.");
  _step = nearestExport / nbStepsFromExport;
  _absoluteDate += _step;
  _nbIter++;
  if(_step < _minDt)
    Msg::Fatal("Below the minimum dt (%e < %e)", _step, _minDt);

  TIME->set(_absoluteDate());

  return _step;
}


//TODO maybe placing this in the GMSH OS.h stuff (with <io.h> and <sys/stat.h>)
static void create_missing_dir(const std::string fullPath) {
  size_t lastp = fullPath.find_last_of('/'); //TODO ?? win ==> backslash ??
  if(lastp == std::string::npos) return;
  std::string dirname = std::string(fullPath, 0, lastp);
  size_t cur=0;
  while(cur!=std::string::npos){
    cur=dirname.find("/",cur+1);
#if defined (WIN32)&& !defined(__CYGWIN__)
    mkdir(dirname.substr(0,cur).c_str());
#else
    mkdir(dirname.substr(0,cur).c_str(),S_IRWXG|S_IRWXU);
#endif
  }
}
static bool fileExists( std::string FileName )
{
  FILE* fp = fopen( FileName.c_str(), "rb" );
  if( fp != NULL )
  {
    fclose( fp );
    return true;
  }
  return false;
}


void slimIterateHelper::exportAll(int nIt, double time) {
  slimTmDate dateTm;
  if(nIt == -1) {
    nIt = _nbExport;
    dateTm = _absoluteDate;
  }
  else
    dateTm.set(time);

  if(Msg::GetCommRank() == 0)
    Msg::Info("Export %d (t=%s)", nIt, getTimeStr(_printExport, dateTm).c_str() );

  for(size_t i = 0; i < _exportStr.size(); i++) {

    //Export Vtk (field and functions)
    if(_exportVtk && _exportFun[i])
      _exportCont[i]->exportFunctionVtk(_exportFun[i], _exportName, dateTm(), nIt, _exportStr[i]);
    else if(_exportVtk)
      _exportCont[i]->exportVtk(_exportName, dateTm(), nIt, _exportStr[i]);

    //Export Msh (field and functions)
    if(_exportMsh) {
      char num[10];
      sprintf(num,"%06d",nIt);
      if(_exportFun[i])
        _exportCont[i]->exportFunctionMsh(_exportFun[i], _exportName + num, dateTm(), nIt, _exportStr[i]);
      else
        _exportCont[i]->exportMsh(_exportName + '_' + _exportStr[i] + num, dateTm(), nIt, _exportStr[i]);
    }

    //Export Binary files (TODO only the fields option ?)
    if(_exportBin) {
      char num[15];
      sprintf(num, "%06d_%03d", nIt, Msg::GetCommRank() );
      if(nIt==0) create_missing_dir(_exportName + "_bin/");
      _exportCont[i]->save(_exportName + "_bin/" + _exportStr[i] + num );
    }
  }
  _nbExport++;
  _hasExported = true;
}

void slimIterateHelper::importAll(int nbExport, double t) {
  char num[15], numField[15], numProc[15];
  sprintf(num, "%06d_%03d", nbExport, Msg::GetCommRank() );
  sprintf(numField, "%06d", nbExport);
  sprintf(numProc, "_%d", Msg::GetCommRank());

  //catch the date if t not specified
  if(t == -1e10) {
    int fieldNum = -1;
    for(size_t i = 0; i < _exportStr.size(); i++)
      if(!_exportFun[i]) {
        fieldNum = i;
        break;
      }
    if(fieldNum == -1)
      Msg::Fatal("No field specified (only functions): no data to load !");
    std::string filename = _exportName + "_" + _exportStr[fieldNum] + numField + "_COMP_0.msh" + (Msg::GetCommSize()>1?numProc:"");
    if( ! fileExists(filename) )
      Msg::Fatal("The file %s is inacessible or dont exist !\n(check if the exportMsh option is set true; "
                 "only needed if time is not given)", filename.c_str());
    std::ifstream file;
    file.open(filename.c_str());
    std::string line = "";
    int topass = 100;
    bool solGet = false;
    while(topass > 0) {
      if(file.eof())
        Msg::Fatal("Date not found in file %s !", filename.c_str());
      topass--;
      getline(file, line);
      if(line == "$ElementNodeData") {
        getline(file, line);
        topass = atoi(line.c_str()) + 2;
        solGet = true;
      }
    }
    if(!solGet)
      Msg::Fatal("$ElementNodeData not found in the %d first lines of %s !", 100, filename.c_str());
    t = atof(line.c_str());
    file.close();
  }

  //print the date
  _absoluteDate.set(1970, 1, 1, 0, 0, t);
  if(Msg::GetCommRank() == 0)
    Msg::Info("Import %d (t=%s)", nbExport, getTimeStr(_printExport, _absoluteDate).c_str() );

  //import
  for(size_t i = 0; i < _exportStr.size(); i++) {
    if(!_exportFun[i]) { //only the fields !
      //TODO : commrank constant for one mesh ? or possible switches ?
      std::string filename = _exportName + "_bin/" + _exportStr[i] + num;
      if( ! fileExists(filename) )
        Msg::Fatal("The file %s is inacessible or dont exist !\n(check if the exportBin option is set true)", filename.c_str());
      _exportCont[i]->load(filename);
      //_exportCont[i]->scatter(); //TODO necessary ? i.e. save&load ghosts ?
    }
  }

  // shift to the right date
  _nbExport = nbExport + 1;
  _absoluteDate.set(1970, 1, 1, 0, 0, t);
  double lastExp = floor( ( _minDt + _absoluteDate() - _beginDate() ) / _exportStep() ) * _exportStep() + _beginDate(); // minDt for num purp
  _lastRegularExport.set(1970, 1, 1, 0, 0, lastExp);

  TIME->set(t);

  while(_exportDate.size() > 0 && (*_exportDate.begin())->operator()() - _absoluteDate() < _minDt)
    _exportDate.erase(_exportDate.begin());
}

bool slimIterateHelper::isTheEnd() {
  if(!_initSet || !_finalSet)
    Msg::Fatal("Use of setInitialDate and setFinalDate before all !");
  bool isTheEnd = (_finalDate() - _absoluteDate() < _minDt) || _nbIter > _nbMaxIter;
  if(isTheEnd) {
    exportAll();
    if(_printExport && Msg::GetCommRank() == 0)
      Msg::Info("isTheEnd\n");
  }
  return isTheEnd;
}

void slimIterateHelper::setTime(std::string str) { // "init[:step]:end"
  size_t found = str.find(':');
  size_t found2 = str.find(':', found + 1);
  setInitialDate(1970, 1, 1, 0, 0, std::atof(str.substr(0, found).c_str()) );
  if(found2 != std::string::npos) {
    _maxDt = std::atof(str.substr(found + 1, found2 - found - 1).c_str()) ;
    setFinalDate(1970, 1, 1, 0, 0, std::atof(str.substr(found2 + 1).c_str()) );
  }
  else
    setFinalDate(1970, 1, 1, 0, 0, std::atof(str.substr(found + 1).c_str()) );
}

void slimIterateHelper::setExport(std::string str) {  // "[init,]step[:extra1[,extra2][,extra3][,...]]"
  size_t found = str.find(':');
  size_t found2 = str.find(',');
  if(found2 < found) {
    _lastRegularExport.set(1970, 1, 1, 0, 0, std::atof(str.substr(0, found2).c_str()) );
    _exportStep.set(0, 0, 0, 0, 0, std::atof(str.substr(found2 + 1, found - found2 - 1).c_str()) );
  }
  else
    _exportStep.set(0, 0, 0, 0, 0, std::atof(str.substr(0, found).c_str()) );
    
  if(found == std::string::npos || found == str.size() - 1) // not extra
    return;
  found2 = str.find(',', found + 1);
  while (found2 != std::string::npos && found2 != str.size() - 1) {
    slimTmDate* date = new slimTmDate();
    date->set( std::atof(str.substr(found + 1, found2 - found - 1).c_str()) );
    _exportDate.insert(date);
    found = found2;
    found2 = str.find(',', found + 1);
  }
  slimTmDate* date = new slimTmDate();
  date->set( std::atof(str.substr(found + 1).c_str()) );
  _exportDate.insert(date);
}

slimIterateHelper::slimIterateHelper():
  _maxDt(1e22),
  _minDt(1e-9),
  _exportVtk(false),
  _exportMsh(true),
  _exportBin(false),
  _initSet(false),
  _finalSet(false),
  _hasExported(false),
  _exportName("output/default"),
  _nbExport(0),
  _nbMaxIter(1e5),
  _nbIter(0),
  _printExport(1)
{
  _exportStep.set(0, 0, 0, 0, 0, 1e18); // up to 31 billion of years
  TIME = function::getTime();
}
