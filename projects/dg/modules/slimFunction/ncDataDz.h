#define _NC_DATA_DZ_H_

#include <fstream>
#include <float.h>
#include "GmshConfig.h"
#include "function.h"
#include "dgConfig.h"


#ifdef HAVE_NETCDF
#include "netcdfcpp.h"

//************************************************************
/**Function using the slimStructDataContainer and the slimStructDataInterpolator classes */
class NcVar;
class NcFile;
class ncDataDz {
  public:
    int spaceDim;
    int timeDim;
    double addOffset,scaleFactor,oldValue;
    double *dataT0, *dataT1;
    int nx,ny,nz,nrec,ntime;
    std::vector<double> _depth;
    NcVar *var;
    NcFile *ncFile;
    double *d,*o;
    int *n;
    double _periodicity;
    /**Tri-linearly interpolatation. To be used with a functionNcDataDz*/
    double getData(double *data,double *xx);
    double opfunction(double *data,int *p);
    ncDataDz(std::string fileName, std::string variable,std::string xDim,std::string yDim, const std::vector<double>& depth);
    ~ncDataDz(){
      if(dataT0) delete[] dataT0;
      if(dataT1) delete[] dataT1;
      delete ncFile;
    }
};
class functionNcDataDz: public function {
  ncDataDz *_container;
  fullMatrix<double> coordF,timeF;
  double minValue,maxValue;
  int tIdOld, tId;
  public:	
    void call(dataCacheMap *m, fullMatrix<double> &val);
    /**function returning the time to be parsed outside of the coordFunction when the time is uncoupled (e.g. netcdf) */
    void setTimeFunction(const function *timeFunction);
    /**Function that interpolates a scalar value at each point of a regular grid defined by the class ncDataDz*/
    functionNcDataDz(ncDataDz *container, const function *coordFunction);
    /** Set the minimal value returned by the interpolation */
    void setMinValue(const double _minValue){
      minValue=_minValue;
    }
    /** Set the maximal value returned by the interpolation */
    void setMaxValue(const double _maxValue){
      maxValue=_maxValue;
    }
};

#endif
