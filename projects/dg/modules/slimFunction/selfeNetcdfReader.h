#ifndef _SELFE_READER_H_
#define _SELFE_READER_H_
#include "dgConfig.h"
#ifdef HAVE_NETCDF

class NcFile;

/** Evaluates 2d selfe netcdf transect files at boundary. Transect must be extracted along the mesh boundary */
class selfeTransectFunc: public function{
public:
  NcFile *ncFile;
  NcVar *dataVar;
  int nTime, nPoints;
  double _currentTime, _startTime, _xScale, _yScale;
//   double *xArray, *yArray, *dataArrayOld, *dataArray, *timeArray;
  std::vector<double> xArray, yArray, dataArrayOld, dataArray, timeArray;
  fullMatrix<double> coordF,timeF;
  selfeTransectFunc(std::string fileName, std::string varName, std::string xName, std::string yName, std::string tName, const function *coordFunction=function::getCoordinates(), const function *timeFunction=function::getTime());
  ~selfeTransectFunc();
  void call(dataCacheMap *m,fullMatrix<double> &val);
/**coordFunction is a function returning the coordinate vector (lonLatDegrees for example) and timeFunction is a function returning the absolute time */
  void setCoordAndTimeFunctions(const function *coordFunction,const function *timeFunction){
    setArgument(coordF,coordFunction);
    setArgument(timeF,timeFunction);
  };
  /** Fetches the data arrays in memory and interpolates to the given time*/
  void setStartTime(double time) { _startTime=time; };
  /** Fetches the data arrays in memory and interpolates to the given time*/
  void setTime(double time);
};
#endif
#endif
