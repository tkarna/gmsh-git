#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include <string>
#include <cstdlib>
#include <iostream>
#include <sstream> 
#include "GmshConfig.h"
#include "function.h"
#include "dgConfig.h"
#include "time.h"
#include "ncDataDz.h"

#ifdef HAVE_NETCDF
#include "netcdfcpp.h"

double ncDataDz::opfunction(double *data, int *p){
  int i,j,k;
  i = spaceDim<1?0:std::max(0,std::min(n[0]-1,p[0]));
  j = spaceDim<2?0:std::max(0,std::min(n[1]-1,p[1]));
  k = spaceDim<3?0:std::max(0,std::min(n[2]-1,p[2]));
  nx = spaceDim<1?1:n[0];
  ny = spaceDim<2?1:n[1];
  nz = spaceDim<3?1:n[2];
  return data[(i*ny+j)+k*(nx*ny)];
}
//***************************************************************************************
double ncDataDz::getData(double *data, double *xx){
  int dim=spaceDim;
  int id[16];
  int cid[16];
  double xi[16];
  int jj=0;
  for (unsigned int j=0;j<_depth.size();j++){
    if ((-xx[2]>_depth[j])&&(-xx[2]<_depth[j+1])) jj=j;
    if (xx[2]+_depth[j]<0.01) jj=j;
  }
   for(int i=0;i<dim;i++){
    if (i==2) { 
      if (jj==0) {
	d[2]= _depth[1]-_depth[0];
      } else if (jj==23){
	d[2]=_depth[23]-_depth[22];
      } else {
      d[2]=_depth[jj+1]-_depth[jj];
      }
    }
    id[i]=(int)((xx[i]-o[i])/d[i]);
    if (i==2) {
      xi[i]=(-xx[i]-_depth[jj])/d[i];
    } else {
    xi[i]=(xx[i]-o[i]-id[i]*d[i])/d[i];
    }
    xi[i]=std::min(1.,std::max(0.,xi[i]));
  }

  double v=0;
  for(int i=0;i<1<<dim;i++){
    double f=1;
    for(int j=0;j<dim;j++){
      if(i&(1<<j)){
	if (j<2) {
           cid[j]=id[j];
	} else {
	   cid[2]=jj;
	}
        f*=1-xi[j];
      }else{
	if (j<2) {
	  cid[j]=id[j]+1;
	} else {
	  cid[2]=jj+1;
	}
        f*=xi[j];
      }
    }

    double val=opfunction(data,cid);
    v+=f*val;
  }
  return addOffset+scaleFactor*(v);
}

ncDataDz::ncDataDz(std::string fileName,std::string variable,std::string xDim,std::string yDim, const std::vector<double>& depth) {
//ncDataDz::ncDataDz(std::string fileName,std::string variable,std::string yDim,std::string xDim, const std::vector<double>& Depth) {  
  addOffset=0;
  scaleFactor=1;  
  _depth=depth;
  //_depth=Depth;
  std::string zDim="levels_nb";
  //std::string zDim="depth";
  std::string tDim="time";
  ncFile = new NcFile(fileName.c_str(),NcFile::ReadOnly);
  if (!ncFile->is_valid())
    Msg::Fatal("couldn't open netcdf file %s!",fileName.c_str());
  var   = ncFile->get_var(variable.c_str());
  NcError *err= new NcError(NcError::silent_nonfatal);
  NcVar *varx  = ncFile->get_var(xDim.c_str());
  NcVar *vary  = ncFile->get_var(yDim.c_str());
  NcVar *varz  = ncFile->get_var(zDim.c_str());
  NcVar *vart  = ncFile->get_var(tDim.c_str());
  spaceDim=3;
  NcAtt* att=var->get_att("add_offset");
  addOffset = att?att->values()->as_double(0):0;
  att=var->get_att("scale_factor");
  scaleFactor = att?att->values()->as_double(0):1;
  double xx[2],yy[2],zz[2];
  o = new double[3];
  d = new double[3];
  n = new int[3];
  n[0]=1;n[1]=1;n[2]=1;
  
  varx->get(xx, 2);
  vary->get(yy, 2);
  varz->get(zz, 2);
  nx  = varx->get_dim(0)->size();
  ny  = vary->get_dim(0)->size();
  nz  = varz->get_dim(0)->size();

  if (xx[0]==90.0) {
    o[0]=xx[0]+0.5;
  }
  else {
    o[0]=xx[0];
  }
  o[1]=yy[0],o[2]=zz[0];
  d[0]=xx[1]-o[0],d[1]=yy[1]-yy[0],d[2]=zz[1]-zz[0];
  n[0]=nx,n[1]=ny,n[2]=nz;
  nrec=nx*ny*nz;
  
  if(vart) {
          printf("dans la condition time\n");
          dataT0=new double[nrec];
          dataT1=new double[nrec];
          var->set_cur(1,0,0,0);
          var->get(dataT0,1,nz,nx,ny);
          var->set_cur(1,0,0,0);
          var->get(dataT1,1,nz,nx,ny);
  }
  delete err;
    
}

//***************************************************************
void functionNcDataDz::call(dataCacheMap *m, fullMatrix<double> &val){
  for(int pt=0;pt<val.size1();pt++){
      double timeData=timeF(pt,0);
      tId=floor(timeData);
      double tXi=timeData-tId;
      if(tId!=tIdOld){
        _container->var->set_cur(tId,0,0,0);
        _container->var->get(_container->dataT0,1,_container->nz,_container->nx,_container->ny);
        _container->var->set_cur(tId+1,0,0,0);
        _container->var->get(_container->dataT1,1,_container->nz,_container->nx,_container->ny);

      }   
    double x[3]={coordF(pt,0),coordF(pt,1),coordF(pt,2)};
    //double x[3]={coordF(pt,1),coordF(pt,0),coordF(pt,2)};
    val(pt,0)=(1-tXi)*_container->getData(_container->dataT0,x)+tXi*_container->getData(_container->dataT1,x);
  }
  tIdOld=tId;
}
void functionNcDataDz::setTimeFunction(const function *timeFunction){
  setArgument(timeF,timeFunction);
}

functionNcDataDz::functionNcDataDz(ncDataDz *container, const function *coordFunction): function(1){
  tIdOld=-1;
  tId = 0;
  _container=container;
  setArgument(coordF,coordFunction);
  minValue=-DBL_MAX;
  maxValue=+DBL_MAX;
}

#endif
