#ifndef _SLIM_ITERATE_HELPER_H_
#define _SLIM_ITERATE_HELPER_H_

#include "slimDate.h"
#include "dgDofContainer.h"
#include "functionGeneric.h"


class less_date {
public:
  bool operator()(slimTmDate const *a, slimTmDate const *b) {
    return a->operator()() < b->operator()();
  }
};

/* Export at right time, and help managing the time loop
** 
**/
//TODO add the convergence component . . . (modify time step in consequence)
//TODO add print customisable output ?
class slimIterateHelper {
  slimTmDate _beginDate;
  slimTmDate _absoluteDate;
  slimTmDate _finalDate;
  double _step, _maxDt, _minDt;
  std::set<slimTmDate*, less_date> _exportDate;
  slimTmDate _lastRegularExport;
  slimTmDateShift _exportStep;
  bool _exportVtk, _exportMsh, _exportBin, _initSet, _finalSet, _hasExported;
  std::vector<dgDofContainer*> _exportCont;
  std::vector<std::string> _exportStr;
  std::vector<function*> _exportFun;
  std::string _exportName;
  int _nbExport, _nbMaxIter, _nbIter, _printExport;
  functionConstant * TIME;

public:
  slimIterateHelper();
  void setTime(std::string st);
  void setExport(std::string st);

  void setInitialDate(double sec) { setInitialDate(1970, 1, 1, 0, 0, sec); }
  void setInitialDate(long long int year,long long int month,long long int day,long long int hour,long long int min,double sec);

  void setFinalDate(double sec) { setFinalDate(1970, 1, 1, 0, 0, sec); }
  void setFinalDate(long long int year,long long int month,long long int day,long long int hour,long long int min,double sec);

  // each step, export
  void setExportStep(double sec) {  _exportStep.set(0, 0, 0, 0, 0, sec); }
  void setExportStep(long long int hour,long long int min,double sec) { _exportStep.set(0, 0, 0, hour, min, sec); }
  void setExportStep(long long int year,long long int month,long long int day,long long int hour,long long int min,double sec) {
    _exportStep.set(year, month, day, hour, min, sec);
  }

  // exceptionnal export, at fixed time
  void setExportDate(double sec) { slimTmDate* date = new slimTmDate(); date->set(sec); _exportDate.insert(date); }
  void setExportDate(long long int year,long long int month,long long int day,long long int hour,long long int min,double sec) {
    slimTmDate* date = new slimTmDate(); date->set(year, month, day, hour, min, sec); _exportDate.insert(date);
  }

  void setMaxDt(double maxdt) { if(maxdt > _minDt) _maxDt = maxdt; else Msg::Fatal("maxDt (%e) < minDt (%e) !", maxdt, _minDt); }
  void setMinDt(double mindt) { if(mindt < _maxDt) _minDt = mindt; else Msg::Fatal("minDt (%e) > maxDt (%e) !", mindt, _maxDt); }
  void setNbMaxIter(int nbMaxIter) { _nbMaxIter = nbMaxIter; }
  //void setNumExport(int num) { _nbExport = num; } // warn: should be unemployed

  void addExport(dgDofContainer* solution, function* funToExport, std::string name) {
    _exportCont.push_back(solution); _exportFun.push_back(funToExport); _exportStr.push_back(name);
  }
  void addExport(dgDofContainer* solution, std::vector<function*> funToExport, std::vector<std::string> name) {
    if(funToExport.size() != name.size())
      Msg::Fatal("in slimEterateHelper addExport : both vectors (funs and names) must be of same size !");
    for(std::vector<function*>::iterator it = funToExport.begin(); it != funToExport.end(); it++) {
      _exportCont.push_back(solution); _exportFun.push_back(*it); _exportStr.push_back(name[it - funToExport.begin()]);
    }
  }
  void addExport(dgDofContainer* solution, std::string name) {
    _exportCont.push_back(solution); _exportFun.push_back(NULL); _exportStr.push_back(name);
  }
  void setFileName(std::string nFile) { _exportName = nFile; }
  void exportVtk(bool isset) { _exportVtk = isset; }
  void exportMsh(bool isset) { _exportMsh = isset; }
  void exportBin(bool isset) { _exportBin = isset; }
  void printExport(int print) { _printExport = print; }

  double getNextTimeStepAndExport();
  void exportAll(int nIt=-1, double time=0);
  void importAll(int nbExport, double t=-1e10);
  bool isTheEnd();
  bool hasExported() { return _hasExported; }
  void importAndExportAll(int nbExport, double t) { importAll(nbExport, t); _nbExport--; exportAll(); }

  double getCurrentTime() { return _absoluteDate(); }
  double getSimulTime() { return _absoluteDate() - _beginDate(); }
  double getRemainingTime() { return _finalDate() - _absoluteDate(); }
  slimTmDate getCurrentDate() { return _absoluteDate; }
  int getIter() { return _nbIter; }
  std::string getTime() { return _absoluteDate.toString(); }
//  double getCPUTime() { return ; }
};


#endif
