#ifndef _SLIM_TPXO_H_
#define _SLIM_TPXO_H_
#include "dgConfig.h"
#include "slimStructData.h"
#include "dgDofContainer.h"
#include "dgGroupOfElements.h"
template < class t > inline t pow2(t x)
{
  return x * x;
}
#ifdef HAVE_NETCDF
void astrol( double time, double &s, double &h, double &p, double &N);
void astronomical_argument_and_nodal_correction(double time1, double *arg, double *f);

class NcFile;

/**Reconstruct tidal signal from the global ocean tidal model tpxo in the netcdf format*/

class slimFunctionTpxo: public function {
	public:
	double *o,*d;
	int *n;
	std::vector<slimStructDataContainerSimple*> containerAmp, containerPha;
	slimStructDataContainerSimple *containerAmpTest;
	slimStructDataContainerSimple **containerAmpBis;
	std::vector<double*> dataAmp, dataPha;
	std::vector<std::string> compList; //list of the tidal constituents
	std::vector<double> freq,astro0; //frequencies of the tide and initial astronomical argument
	std::vector<int> indiceNodal; //indice giving the location of the tidal constituant in the argument routine
  std::vector<dgDofContainer*> ampDofContainers, phaDofContainers;
  std::vector<fullMatrix<double> > ampFullMatrices, phaFullMatrices;
  const function *_coordFunction;
	slimStructDataInterpolatorMultilinear *interpAmp,*interpPha;
  fullMatrix<double> coordF,timeF;

	int NC; //number of constituants
	double *astro,*nodal;
  time_t relTime1992;
	bool lon_0_360, _useDofContainers;
	NcFile *ncFile;
/**fileName to a netcdf file containing tidal harmonical downloaded at http://volkov.oce.orst.edu/tides/, amplitudeName/phaseName/lonName/latName are the names of the variable refering the real amplitude/real phase/longitude name/latitude name in the netcdf file (see ncdump -h fileName) */
	slimFunctionTpxo(std::string fileName,std::string amplitudeName, std::string phaseName, std::string lonName, std::string latName, const function *coordFunciton = NULL, const function *timeFunction = NULL);
  void call(dataCacheMap *m,fullMatrix<double> &val);
/**coordFunction is a function returning the coordinate vector (lonLatDegrees for example) and timeFunction is a function returning the absolute time */
  void setCoordAndTimeFunctions(const function *coordFunction,const function *timeFunction){
    setArgument(coordF,coordFunction);
    setArgument(timeF,timeFunction);
    _coordFunction=coordFunction;
  }
	void addMask(double *dataNoMask, double *dataMask, int nlon, int nlat);
  //interpolate the amplitude and phase on dofs that are used instead of the interpolation at each time step
  void useDofContainers(dgGroupCollection *groups);
	~slimFunctionTpxo();
};
#endif
#endif
