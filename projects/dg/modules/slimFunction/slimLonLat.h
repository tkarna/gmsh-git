#ifndef _SLIM_LON_LAT_H_
#define _SLIM_LON_LAT_H_
#include <function.h>
class dgExtrusion;

class slimLonLat {
  function *_latLonDegree, *_lonLatDegree, *_latLonRadian, *_lonLatRadian, *_toDelete[2], *_angle;
  void *_pjLatLon, *_pjModel;
  void _initProjections(const std::string projParameter);
 public :
  slimLonLat(const std::string projParameter);
  //pre-compute 2d and 3d
  slimLonLat(dgExtrusion &extrusion, int integrationOrder, const std::string projParameter);
  ~slimLonLat();
  const function &latLonDegree() const {return *_latLonDegree;}
  const function &latLonRadian() const {return *_latLonRadian;}
  const function &lonLatDegree() const {return *_lonLatDegree;}
  const function &lonLatRadian() const {return *_lonLatRadian;}
  const function &angle() const {return *_angle;}
};

class slimLonLatVectorFunction: public function {
  fullMatrix<double> _vectorLon, _vectorLat, _angle;
  public:
  slimLonLatVectorFunction(const slimLonLat &ll, const function &vectorLon, const function &vectorLat);
  void call(dataCacheMap *m, fullMatrix<double> &val);
};
#endif
