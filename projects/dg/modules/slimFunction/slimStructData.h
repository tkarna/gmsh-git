#ifndef _SLIM_STRUCT_DATA_H_
#define _SLIM_STRUCT_DATA_H_
#include "slimDate.h"
#include <fstream>
#include <float.h>
#include "GmshConfig.h"
#include "function.h"
#include "dgConfig.h"

/**container for structured data */
class slimStructDataContainer {
	public:
	int spaceDim;
	int timeDim;
	int tIdOld;
	bool timeDependent;
	long double initialTime;
        bool asClimato;
	double shiftTime;
	double addOffset,scaleFactor,oldValue;
	bool uncoupledTime;
	std::streampos begData;
  double *data,*dataCopy,*dataT0,*dataT1;
	virtual double opfunction(double *dataTable, int *p)=0;
	virtual void setData(double *dataTable,int tId)=0;
	slimStructDataContainer(){
		timeDependent=false;
		initialTime=0;
		shiftTime=0;
		uncoupledTime=false;
		addOffset=0;
		scaleFactor=1;
	}
	virtual ~slimStructDataContainer(){
		if(data) delete[] data;
		if(dataT0) delete[] dataT0;
		if(dataT1) delete[] dataT1;
		if(dataCopy) delete[] dataCopy;
	}
	virtual double *getOrigin()=0;
	virtual int *getSize()=0;
	virtual double *getDelta()=0;
	virtual void setMaxIfZero()=0;
        /**set step=1 and origin=0 for the temporal variable*/
        inline void useAsClimato(bool _asClimato){
          asClimato=_asClimato;
          initialTime=0;
          shiftTime=1;
        }
};
//************************************************************
/**container scalar temporal series */
class slimStructDataContainerTemporalSerie:public slimStructDataContainer {
  std::ifstream input;
	public:
	int *n;
	double *o,*d;
	slimTmDate initialDate;
	slimTmDate dataDate;
	slimTmDateShift shiftDate;
	std::streampos firstDataInFile;
	/**fileName corresponds to an ascii file.
   * with the following format:\n initial {\n year XXX\n month XXX \n day XXX\n hour XXX\n minute XXX\n second XXX\n }\n shift {\n year XXX\n month XXX \n day XXX\n hour XXX\n minute XXX\n second XXX\n }\n */
	slimStructDataContainerTemporalSerie(const std::string fileName);
	double opfunction (double *dataTable,int *p);
	void setData(double *dataTable,int tId);
  double *getOrigin() {
  	return o;
  }
  double *getDelta() {
  	return d;
  }
  int *getSize() {
  	return n;
  }
  void setMaxIfZero(){
  	Msg::Fatal("setMaxIfZero is not implemented in slimStructDataContainerNetcdf\n");
  }
};
//************************************************************
/**container for binary structured data*/
class slimStructDataContainerSimple:public slimStructDataContainer {
	public:
	double *o;
	double *d;
	int *n;
	int nt;
	double opfunction(double *dataTable,int *p);
	void setData(double *dataTable, int tId=0){
		data=dataTable;
	}
	void setMaxIfZero();
	/**fileName corresponds to a binary (if the extension is .bin) or an ascii file.
   * with the following format:version1\nxy\nx0 y0\ndx dy\nnx ny\nv(0,0) v(0,1)  ... v(1,0) v(1,1)  ...  */
	slimStructDataContainerSimple(const std::string fileName);
	slimStructDataContainerSimple(double *_data, double *_o, double *_d, int *_n):slimStructDataContainer(){
		uncoupledTime=false;
		timeDim=0;
		data=_data;
		o=_o;
		d=_d;
		n=_n;
		nt=n[0]*n[1]*n[2];
		spaceDim=1;
		if(n[1]>1)spaceDim++;
		if(n[2]>1)spaceDim++;
	}
	/** Set the limit outside of which the data is considered as NaN */
	void setDataLimit(double limit){
		for(int i = 0; i < nt; i++){
			if(data[i]>=limit)
				data[i]=NAN;
		}
	}
	~slimStructDataContainerSimple(){
		delete []n;
		delete []d;
		delete []o;
	}
	double *getDelta(){
		return d;
	}
	double *getOrigin(){
		return o;
	}
	int *getSize(){
		return n;
	}
};
//************************************************************
#ifdef HAVE_NETCDF
class NcVar;
class NcFile;
/**NetCDF (network Common Data Form).
 * is a set of software libraries and machine-independent data formats that support the creation,
 * access, and sharing of array-oriented scientific data
 * (http://www.unidata.ucar.edu/software/netcdf/) */
class slimStructDataContainerNetcdf:public slimStructDataContainer {
	public:
	int nx,ny,nz,nrec,ntime;
	NcVar *var;
	NcFile *ncFile;
	double *d,*o;
	int tIdOld;
	int *n;
        bool Flag3d;
 /**the initial date (second) from 01-01-1979 00:00 (Greenwich time) */
  void setDataInitialTime(double t){ initialTime=t; }
 /**the shift date (second) */
  void setDataShiftTime(double dt){ shiftTime=dt; }
  void setData(double *dataTable, int tId);
  double opfunction(double *dataTable, int *p);
  slimStructDataContainerNetcdf(std::string fileName, std::string variable,std::string xDim,std::string yDim);
  /**If you use a 2d mesh but your data comes from 3d netcdf*/
  inline void set3dUse2d(bool flag3d){Flag3d = flag3d;}
/**fileName is the Netcdf-file\nvariable is the name of the variable to load (use ncdump -h fileName.nc to obtain the heading of the file)\n xDim,yDim,zDim,tDim refer to the dimension names (e.g. \"lon\",\"lat\",\"level\" and \"time\". Parse \" \" if the data does not have one of the dimensions */
  double *getOrigin() {return o;}
  double *getDelta() {return d;}
  int *getSize() {return n;}

~slimStructDataContainerNetcdf();
  void setMaxIfZero(){Msg::Fatal("setMaxIfZero is not implemented in slimStructDataContainerNetcdf\n");}
};
#endif
//************************************************************
/**interpolation for structured data */
class slimStructDataInterpolator {
  public:
	int tIdOld;
	bool nan_if_outside;
	slimStructDataInterpolator(){
		tIdOld=-1;
	}
	double opfunction(slimStructDataContainer *container, double *xx, double time);
	virtual double getData(slimStructDataContainer *container,double *data, double *xx)=0;
  virtual ~slimStructDataInterpolator(){};
};
//************************************************************
class slimStructDataInterpolatorGaussian:public slimStructDataInterpolator{
  public:
    double addOffset,scaleFactor;
    int tIdOld;
    double _periodicity;
    slimStructDataInterpolatorGaussian():slimStructDataInterpolator(){ 
                tIdOld=0;
                _periodicity=0;
    }
    double getData(slimStructDataContainer *container,double *data, double *xx);
};
/**A class to perform linear interpolation of data given on structured grid */
class slimStructDataInterpolatorMultilinear:public slimStructDataInterpolator {
  public:
  double _periodicity;
	bool _nanIfOutside;
	int tIdOld;
  /**If the boolean nanIfOutside is enabled, the function returns nan when interpolating outside of the table. */
	inline void setNanIfOutside (int nanIfOutside) { _nanIfOutside = nanIfOutside; }
  /**If this function is parsed, continuity of the interpolated function of periocity is insured */
	inline void setPeriodicity (double periodicity) { _periodicity = periodicity; }
  /**Tri-linearly interpolatation. To be used with a slimStructDataContainer and slimFunctionStructData */
	slimStructDataInterpolatorMultilinear():slimStructDataInterpolator(){ 
		tIdOld=0;
		_periodicity=0;
    _nanIfOutside=true;
	}
	double getData(slimStructDataContainer *container,double *data, double *xx);
};
//************************************************************
/**A function to perform bicubic interpolation of data given on structured grid */
class slimStructDataInterpolatorBicubic:public slimStructDataInterpolator{
	public:
  slimStructDataContainer *container;
	int tIdOld;
  int *n;
  double *o,*d;
  /**Bicubic interpolatation. To be used with a slimStructDataContainer and slimFunctionStructData */
	slimStructDataInterpolatorBicubic();
  double get2dData(double *data,int i, int j)const;
  double get2dDiff(double *data,int i, int j, int dim)const;
  double get2dCrossDiff(double *data,int i, int j)const;
  void bcucof(double y[], double y1[], double y2[], double y12[], double d1, double d2, double *c)const;
  double getData(slimStructDataContainer *_container,double *data,double *xx);
};

 
/**Function using the slimStructDataContainer and the slimStructDataInterpolator classes */
class slimFunctionStructData: public function {
	slimStructDataContainer *_container;
	slimStructDataInterpolator *_interp;
	fullMatrix<double> coordF,timeF;
	double minValue,maxValue;
	public:	
  void call(dataCacheMap *m, fullMatrix<double> &val);
  /**function returning the time to be parsed outside of the coordFunction when the time is uncoupled (e.g. netcdf) */
	void setTimeFunction(const function *timeFunction);
	/**Function that interpolates a scalar value at each point of a regular grid defined by the class slimStuctDataContainer and using the interpolation scheme slimStructDataInterpolation */
	slimFunctionStructData(slimStructDataContainer *container, slimStructDataInterpolator *interp, const function *coordFunction);
	/** Set the minimal value returned by the interpolation */
	void setMinValue(const double _minValue){
    minValue=_minValue;
	}
	/** Set the maximal value returned by the interpolation */
	void setMaxValue(const double _maxValue){
    maxValue=_maxValue;
	}
};
	
#endif
