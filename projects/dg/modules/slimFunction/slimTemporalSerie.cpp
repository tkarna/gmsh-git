#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <cstring>
#include <fstream>
#include "GmshConfig.h"
#include "function.h"
#include "slimDate.h"
#include "dgConfig.h"
#include "slimTemporalSerie.h"
#include <algorithm>

static std::string removeSpaces(std::string s){
  s.erase(remove_if(s.begin(), s.end(), isspace), s.end());
  return s;
}
class slimTemporalSerieIterator {
  slimTemporalSerieIterator(const slimTemporalSerieIterator&);
  slimTemporalSerieIterator& operator=(const slimTemporalSerieIterator&);
  std::ifstream file;
 public:
  std::vector<double> allData;
  int lastReadedLine;
  std::vector<double> lastData;
  std::vector<double> nextData;
  slimTmDate lastTime;
  slimTmDate nextTime;
  slimTmDate initTime;
  int nbValues;
  bool periodicYear;
  std::string fileName;
  std::streampos firstLineOfDataInFile;
  slimTmDateShift shift;
  slimTemporalSerieIterator(int dim): lastReadedLine(0), lastData(dim), nextData(dim) {}
  void open(std::string nameFile) {
    file.open(nameFile.c_str(), std::ifstream::in);
    if(!file.is_open())  Msg::Fatal("cannot open file \'%s\'.\n", nameFile.c_str());
    fileName = nameFile;
  }
  ~slimTemporalSerieIterator() { close(); }
  std::istream & getLine(std::string &line) { lastReadedLine++; return std::getline(file, line);  }
  void resize(int dim) { lastData.resize(dim); nextData.resize(dim); }
  void getNextData(std::vector<double> & data) {
    char str[256];
    char * end;
    std::string line;
    if(!getLine(line) || removeSpaces(line) == "}"){
      if(periodicYear){
        close();
        open(fileName);
        seekg(firstLineOfDataInFile);
        getLine(line);
      }
      else Msg::Fatal("end of file in temporal interpolation of %s:line %s\n", fileName.c_str(),line.c_str());
    }
    std::strcpy(str, line.c_str());
    end = str;
    for(size_t i = 0; i < data.size(); i++) {
      if (*end == '\0')
        Msg::Fatal("Data missing in temporal serie (asked vector too large)");
      data[i] = strtod(end, &end);
    }
    if (*end != '\0')
      Msg::Warning("Data unused in temporal serie (asked vector smaller than given data)");  
  }
  void close(){
    if(file.is_open()) file.close();
  }
  void seekg(std::streampos pos){
    file.seekg(pos,std::ios_base::beg);
  }
  std::streampos tellg(){
    return file.tellg();
  }
};


static void readDischargeHeaderBlock(std::string line, long long int &year, llint &month, llint &day, llint &hour, llint &minute, double &second){
  if ( line.substr(0, 4) == "year")
    year = atoll((line.substr(4,line.size())).c_str());
  else if ( line.substr(0, 5) == "month")
    month = atoll((line.substr(5,line.size())).c_str());
  else if ( line.substr(0, 3) == "day")
    day = atoll((line.substr(3,line.size())).c_str());
  else if ( line.substr(0, 4) == "hour")
    hour = atoll((line.substr(4,line.size())).c_str());
  else if ( line.substr(0, 6) == "minute")
    minute = atoll((line.substr(6,line.size())).c_str());
  else if ( line.substr(0, 6) == "second")
    second = atof((line.substr(6,line.size())).c_str());
  else 
    Msg::Error("Strange line in a temporal Serie: %s\n", line.c_str());
}


void slimDateInterpolatorLinear::initialize(slimTemporalSerieIterator &it) {
  it.getNextData(it.lastData);
  it.getNextData(it.nextData);
  it.lastTime = it.initTime;
  if(it.shift()) { // regular data
    it.nextTime = it.lastTime;
    it.nextTime += it.shift;
  }
  else { // dated data
    it.initTime -= it.lastData[0]; // ref = first time value
    it.nextTime = it.initTime;
    it.nextTime += it.nextData[0];
  }
}

std::vector<double> slimDateInterpolatorLinear::get(double time, slimTemporalSerieIterator &it) {
  if(time < it.lastTime()){
    if(time < it.initTime())
      Msg::Fatal("time not existing in this file: %s",it.fileName.c_str());
    else
      Msg::Fatal("cannot going back in a temporal interpolation (not implemented)");
  }

  if(it.shift()) {
    while(time > it.nextTime()) {
      it.lastTime = it.nextTime;
      it.nextTime += it.shift;
      it.lastData = it.nextData;
      it.getNextData(it.nextData);
    }
    std::vector<double> realData(it.nextData.size());
    double alpha = (time - it.lastTime()) / (it.nextTime() - it.lastTime());
    for(size_t i = 0; i < realData.size(); i++)
      realData[i] = it.lastData[i] * (1. - alpha) + it.nextData[i] * alpha;    
    return realData;
  }
  else { // dates given in seconds from initial time
    while(time > it.nextTime()) {
      it.lastTime = it.nextTime;
      it.lastData = it.nextData;
      it.getNextData(it.nextData);
      it.nextTime = it.initTime;
      it.nextTime += it.nextData[0];
      if(it.lastTime() >= it.nextTime())
        Msg::Fatal("the dates for temporal interpolation must strictly increase !");
    }
    std::vector<double> realData(it.nextData.size() - 1);
    double alpha = (time - it.lastTime()) / (it.nextTime() - it.lastTime());
    for(size_t i = 0; i < realData.size(); i++)
      realData[i] = it.lastData[i+1] * (1. - alpha) + it.nextData[i+1] * alpha;
    return realData;
  }
}

slimFunctionTemporalSerie::~slimFunctionTemporalSerie()
{
  delete _it;
  if(_ownInterp) delete _slimInterp;
}


slimFunctionTemporalSerie::slimFunctionTemporalSerie(std::string fileName, const function *timeFunction, int dim, slimDateInterpolator * slimInterp):
  function(dim),
  _currData(dim)
{
  _it = new slimTemporalSerieIterator(dim + 1);
  if (timeFunction)
    setArgument(time, timeFunction);
  else
    setArgument(time, function::getTime());

  _ownInterp  = ! slimInterp;
  _slimInterp = slimInterp ? slimInterp : new slimDateInterpolatorLinear();

  _it->fileName = fileName;
  _it->open(fileName.c_str());

  _datedData = true;
  _meanConservation = false;

  std::string line;
  while (true) {
    if(!_it->getLine(line))
      Msg::Fatal("the time serie file is ill-formated");
    line = removeSpaces(line);
    if (line == "initial{") {
      long long int year=1990;
      llint month=1,day=1,hour=0,minute=0;
      double second=0;
      _it->getLine(line);
      line = removeSpaces(line);
      while (line != "}"){
        readDischargeHeaderBlock(line, year, month, day, hour, minute, second);
        _it->getLine(line);
        line = removeSpaces(line);
      }
     _it->initTime.set(year, month, day, hour, minute, second);
    }
    else if (line == "shift{") {
      _datedData = false;
      long long int year=0;
      llint month=0,day=0,hour=0,minute=0;
      double second=0;
      _it->getLine(line);
      line = removeSpaces(line);
      while (line != "}"){
        readDischargeHeaderBlock(line, year, month, day, hour, minute, second);
        _it->getLine(line);
        line = removeSpaces(line);
      }
      _it->shift.set(year, month, day, hour, minute, second);
      _scale = 1.;
      break; //todo: remove it when "data{" generalized
    }
    else if(line == "data{") {
      long long int year=0;
      llint month=0,day=0,hour=0,minute=0;
      double second=0;
      _it->getLine(line);
      line = removeSpaces(line);
      while (line != "}") {
        readDischargeHeaderBlock(line, year, month, day, hour, minute, second);
        _it->getLine(line);
        line = removeSpaces(line);
      }
      slimTmDateShift date;
      date.set(year, month, day, hour, minute, second); // unit of time in data
      _scale = date();
      if(!_scale) _scale = 1.; // default: second
      break;
    }
  }
  _it->firstLineOfDataInFile=_it->tellg();

  
  if(!_datedData)
    _it->resize(dim); //no temporal data
  
  _slimInterp->initialize(*_it);
  _currentTime = _it->initTime();
  _currData = _slimInterp->get(_it->initTime(), *_it);

  _it->periodicYear = false;
}

void slimFunctionTemporalSerie::call(dataCacheMap *m, fullMatrix<double> &val) {
 	for(int i = 0; i < val.size1(); i++) {
    double t = time(i, 0);
    if(t != _currentTime) {
      _currData = _slimInterp->get(t / _scale, *_it);
      _currentTime = t;
    }
    for (int j = 0; j < val.size2(); j++)
      val(i, j) = _currData[j];
	}
}

void slimFunctionTemporalSerie::usePeriodicYear(){
  _it->periodicYear = true;
}


slimTemporalSerie::slimTemporalSerie(const std::string filename):function(1)
{
  setArgument(_time, function::getTime());
  std::ifstream f(filename.c_str(), std::ifstream::in);
  if (!f.is_open())
    Msg::Fatal("slimTemporalSerie : cannot open file %s", filename.c_str());
  std::string line;
  long long int year=0;
  llint month=0,day=0,hour=0,minute=0;
  double second;
  std::getline(f, line);
  line = removeSpaces(line);
  if (line != "initial{")
    Msg::Fatal("wrong format in file %s, 'initial{' expected\n", filename.c_str());
  std::getline(f, line);
  line = removeSpaces(line);
  while (line != "}") {
    readDischargeHeaderBlock(line, year, month, day, hour, minute, second);
    std::getline(f, line);
    line = removeSpaces(line);
  }
  _initialTime.set(year, month, day, hour, minute, second); // unit of time in data
  std::getline(f, line);
  year = 0;
  month = 0;
  day = 0;
  hour = 0;
  minute = 0;
  second = 0;
  line = removeSpaces(line);
  if (line != "shift{")
    Msg::Fatal("wrong format in file %s, 'shift{' expected\n", filename.c_str());
  std::getline(f, line);
  line = removeSpaces(line);
  while (line != "}") {
    readDischargeHeaderBlock(line, year, month, day, hour, minute, second);
    std::getline(f, line);
    line = removeSpaces(line);
  }
  _step.set(year, month, day, hour, minute, second); // unit of time in data
  double v;
  while (f>>v) 
    _values.push_back(v);
  _idx = 0;
  _currentTime = _initialTime;
  _nextTime = _initialTime;
  _nextTime += _step;
  _nextTimeD = _nextTime();
  _currentTimeD = _currentTime();
}

double slimTemporalSerie::get(double t)
{
  //while (t * (1 - 1e-8) > _nextTimeD) {
  while (t  > _nextTimeD) {
    _idx++;
    if (_idx + 1 >= _values.size()) {
      Msg::Fatal("no more value in temporal interpolation");
    }
    _currentTime += _step;
    _nextTime += _step;
    _nextTimeD = _nextTime();
    _currentTimeD = _currentTime();
  }
  //while (t * (1 + 1e-8)< _currentTimeD) {
  while (t < _currentTimeD) {
    _idx--;
    if (_idx < 0) {
      Msg::Fatal("no more value in temporal interpolation");
    }
    _currentTime -= _step;
    _nextTime -= _step;
    _nextTimeD = _nextTime();
    _currentTimeD = _currentTime();
  }
  double alpha = (t - _currentTimeD) / (_nextTimeD - _currentTimeD);
  return _values[_idx] * (1 - alpha) + _values[_idx + 1] * alpha;
}


void slimTemporalSerie::call(dataCacheMap *m, fullMatrix<double> &val)
{
  val.setAll(get(_time(0, 0)));
}
