#include "function.h"
#include "slimLonLat.h"
#include "functionGeneric.h"

#include "dgConfig.h"

#ifdef HAVE_PROJ
#include <proj_api.h>
class slimLatLonFunction: public function {
  fullMatrix<double> _xyz;
  projPJ &_pjLatLon, &_pjModel;
 public :
  slimLatLonFunction(projPJ &pjLatLon, projPJ &pjModel):function(3),
  _pjLatLon(pjLatLon),
  _pjModel(pjModel)
  {
    setArgument(_xyz, function::getCoordinates());
  }
  void call(dataCacheMap *m, fullMatrix<double> &val)
  {
    for (int i = 0; i < val.size1(); i++) {
      double x = _xyz(i, 0);
      double y = _xyz(i, 1);
      val(i, 2) = 0.;
      int er=0;
      if ( (er = pj_transform( _pjModel, _pjLatLon, 1, 1, &x, &y, NULL )) != 0) {
        Msg::Fatal("libproj Transform failed: %s\n", pj_strerrno (er));
      }
      val(i, 0) = y;
      val(i, 1) = x;
    }
  }
};

class slimLonLatFunctionConvert: public function {
  fullMatrix<double> _latLonRadian;
  bool _degree, _lonLat;
 public: 
  slimLonLatFunctionConvert(const function &latLonRadian, bool degree, bool lonLat):function(3)
  {
    _degree = degree;
    _lonLat = lonLat;
    setArgument(_latLonRadian, &latLonRadian);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val)
  {
    for (int i = 0; i < val.size1(); i++) {
      val(i, 0) = (_lonLat ? _latLonRadian(i, 1) : _latLonRadian(i, 0));
      val(i, 1) = (_lonLat ? _latLonRadian(i, 0) : _latLonRadian(i, 1));
      val(i, 2) = _latLonRadian(i, 2);
      if (_degree) {
        val(i, 0) *= 180. / M_PI;
        val(i, 1) *= 180. / M_PI;
      }
    }
  }
};

class slimLonLatAngleFunction: public function {
  fullMatrix<double> _xyz;
  projPJ &_pjLatLon, &_pjModel;
 public :
  slimLonLatAngleFunction(projPJ &pjLatLon, projPJ &pjModel):function(1),
  _pjLatLon(pjLatLon),
  _pjModel(pjModel)
  {
    setArgument(_xyz, function::getCoordinates());
  }
  void call(dataCacheMap *m, fullMatrix<double> &val)
  {
    for (size_t i = 0; i < _xyz.size1(); i++) {
      double x[2], y[2];
      x[0] = _xyz.get(i, 0) - 1000.0;
      x[1] = _xyz.get(i, 0) + 1000.0;
      y[0] = _xyz.get(i, 1);
      y[1] = _xyz.get(i, 1);
      int er=0;
      if ( (er = pj_transform( _pjModel, _pjLatLon, 2, 1, x, y, NULL )) != 0) {
        Msg::Fatal ("Transform failed: %s\n", pj_strerrno (er));
      }
      double dlon = x[1] - x[0];
      double dlat = y[1] - y[0];
      val(i, 0) = atan2(dlat, dlon);
    }
  }
};
#endif

slimLonLatVectorFunction::slimLonLatVectorFunction(const slimLonLat &ll, const function &vectorLon, const function &vectorLat):function(2)
{
  setArgument(_angle, &ll.angle());
  setArgument(_vectorLon, &vectorLon);
  setArgument(_vectorLat, &vectorLat);
}

void slimLonLatVectorFunction::call(dataCacheMap *m, fullMatrix<double> &val)
{
  for (size_t i = 0; i < val.size1(); i++) {
    double dlon = _vectorLon(i,0);
    double dlat = _vectorLat(i,0);
    double angle = _angle(i,0);
    double dx = dlon*cos(angle) + dlat*sin(angle);
    double dy = dlat*cos(angle) + dlon*sin(angle);
    val.set(i, 0, dx);
    val.set(i, 1, dy);
  }
}

void slimLonLat::_initProjections(const std::string projParameter)
{
#ifdef HAVE_PROJ
  std::string latLonParameter = "+proj=latlong +ellps=WGS84";
  _pjLatLon = pj_init_plus(latLonParameter.c_str());
  if (!_pjLatLon)
    Msg::Fatal("cannot create a libproj projection with parameters '%s'", latLonParameter.c_str());
  _pjModel = pj_init_plus(projParameter.c_str());
  if (!_pjModel)
    Msg::Fatal("cannot create a libproj projection with parameters '%s'", projParameter.c_str());
#endif 
}

slimLonLat::slimLonLat(const std::string projParameter)
{
#ifdef HAVE_PROJ
  _initProjections(projParameter);
  _toDelete[0] = NULL;
  _toDelete[1] = NULL;
  _latLonRadian = new slimLatLonFunction(_pjLatLon, _pjModel);
  _latLonDegree = new slimLonLatFunctionConvert(*_latLonRadian, true, false);
  _lonLatRadian = new slimLonLatFunctionConvert(*_latLonRadian, false, true);
  _lonLatDegree = new slimLonLatFunctionConvert(*_latLonRadian, true, true);
  _angle = new slimLonLatAngleFunction(_pjLatLon, _pjModel);
#else 
  Msg::Fatal("slimLonLat not available without libproj");
#endif
}


slimLonLat::slimLonLat(dgExtrusion &extrusion, int integrationOrder, const std::string projParameter)
{
#ifdef HAVE_PROJ
  _initProjections(projParameter);
  _toDelete[0] = new slimLatLonFunction(_pjLatLon, _pjModel);
  _toDelete[1] = new slimLonLatAngleFunction(_pjLatLon, _pjModel);
  _latLonRadian = new functionPrecomputedExtrusion(extrusion, integrationOrder, *_toDelete[0]);
  _angle = new functionPrecomputedExtrusion(extrusion, integrationOrder, *_toDelete[1]);
  _latLonDegree = new slimLonLatFunctionConvert(*_latLonRadian, true, false);
  _lonLatRadian = new slimLonLatFunctionConvert(*_latLonRadian, false, true);
  _lonLatDegree = new slimLonLatFunctionConvert(*_latLonRadian, true, true);
#else 
  Msg::Fatal("slimLonLat not available without libproj");
#endif
}

slimLonLat::~slimLonLat()
{
#ifdef HAVE_PROJ
  delete _latLonDegree;
  delete _latLonRadian;
  delete _lonLatDegree;
  delete _lonLatRadian;
  delete _angle;
  pj_free(_pjLatLon);
  pj_free(_pjModel);
  if (_toDelete[0])
    delete _toDelete[0];
  if (_toDelete[1])
    delete _toDelete[1];
#endif
}
