#include "dgConservationLawFunction.h"
#include "dgConservationLawLEE.h"
#include "function.h"
#include "Numeric.h"
#include "functionGeneric.h"
#include "GmshConfig.h"

#include "dgDofContainer.h"
#include "dgFunctionIntegrator.h"

#if defined (HAVE_POST)
  #include "PView.h"
  #include "PViewData.h"
#endif


static const double _gamma = 1.4;

bool dgConservationLawLEE::isLinear() const {
  return true;
}

bool dgConservationLawLEE::isConstantJac() const {
  return true;
}

class dgConservationLawLEE::volumeTerm0 : public function {


  int _dim, _nVar;
  bool _isSource, _isGradMFTerm;
  fullMatrix<double> _sol, _meanFlow, _gradMeanFlow,  _source;

public:

  volumeTerm0(int dim, const function *meanFlowFunc, const function *gradMeanFlowFunc,
                                          const function *sourceFunc) : function(dim+2) {
    _dim = dim;
    _nVar = dim+2;
    setArgument(_sol, function::getSolution());
    setArgument(_meanFlow, meanFlowFunc);
    if (sourceFunc == 0) _isSource = false;
    else {
      _isSource = true;
      setArgument(_source, sourceFunc);
    }
    if (gradMeanFlowFunc == 0) _isGradMFTerm = false;
    else {
      _isGradMFTerm = true;
      setArgument(_gradMeanFlow, gradMeanFlowFunc);
    }
  };

  void call(dataCacheMap *m, fullMatrix<double> &val) {       // TODO: Add mean flow gradient term and source terms
    val.setAll(0.);
    if (_isSource)
      for (int i=0; i<val.size1(); i++) {
        val(i,0) += _source(i,0);
        val(i,1) += _source(i,1);
        val(i,2) += _source(i,2);
        val(i,3) += _source(i,3);
        if (_dim == 3) val(i,4) += _source(i,4);
      }
    if (_isGradMFTerm) {
      static const double OneMinusGamma = 1.-_gamma;
      if (_dim == 2)
        for (int i=0; i<val.size1(); i++) {
          const double &rho = _sol(i,0);
          const double &rho0U = _sol(i,1), &rho0V = _sol(i,2);
          const double &p = _sol(i,3);
          const double &rho0 = _meanFlow(i,0);
          const double &u0 = _meanFlow(i,1), &v0 = _meanFlow(i,2);
          const double &du0dx = _gradMeanFlow(i,1), &du0dy = _gradMeanFlow(i,5);
          const double &dv0dx = _gradMeanFlow(i,2), &dv0dy = _gradMeanFlow(i,6);
          const double &dp0dx = _gradMeanFlow(i,3), &dp0dy = _gradMeanFlow(i,7);
          val(i,1) -= (u0*du0dx+v0*du0dy)*rho+du0dx*rho0U+du0dy*rho0V;
          val(i,2) -= (u0*dv0dx+v0*dv0dy)*rho+dv0dx*rho0U+dv0dy*rho0V;
          val(i,3) -= OneMinusGamma*((dp0dx*rho0U+dp0dy*rho0V)/rho0-(du0dx+dv0dy)*p);
        }
      else
        for (int i=0; i<val.size1(); i++) {
          const double &rho = _sol(i,0);
          const double &rho0U = _sol(i,1), &rho0V = _sol(i,2), &rho0W = _sol(i,3);
          const double &p = _sol(i,4);
          const double &rho0 = _meanFlow(i,0);
          const double &u0 = _meanFlow(i,1), &v0 = _meanFlow(i,2), &w0 = _meanFlow(i,3);
          const double &du0dx = _gradMeanFlow(i,1), &du0dy = _gradMeanFlow(i,6), &du0dz = _gradMeanFlow(i,11);
          const double &dv0dx = _gradMeanFlow(i,2), &dv0dy = _gradMeanFlow(i,7), &dv0dz = _gradMeanFlow(i,12);
          const double &dw0dx = _gradMeanFlow(i,3), &dw0dy = _gradMeanFlow(i,8), &dw0dz = _gradMeanFlow(i,13);
          const double &dp0dx = _gradMeanFlow(i,4), &dp0dy = _gradMeanFlow(i,9), &dp0dz = _gradMeanFlow(i,14);
          val(i,1) -= (u0*du0dx+v0*du0dy+w0*du0dz)*rho+du0dx*rho0U+du0dy*rho0V+du0dz*rho0W;
          val(i,2) -= (u0*dv0dx+v0*dv0dy+w0*dv0dz)*rho+dv0dx*rho0U+dv0dy*rho0V+dv0dz*rho0W;
          val(i,3) -= (u0*dw0dx+v0*dw0dy+w0*dw0dz)*rho+dw0dx*rho0U+dw0dy*rho0V+dw0dz*rho0W;
          val(i,4) -= OneMinusGamma*((dp0dx*rho0U+dp0dy*rho0V+dp0dz*rho0W)/rho0-(du0dx+dv0dy+dw0dz)*p);
        }
    }
  };

};



inline void CalcLEEFlux2D(double rho0, double u0, double v0, double p0, double c0Sq,
                                    double rho, double rho0U, double rho0V, double p,
                                                        int i, fullMatrix<double> &val)
{
  val(i,0) = rho0U + rho*u0;                      // x-flux, cont. eq.
  val(i,1) = rho0U*u0 + p;                        // x-flux, mom. eq. 1
  val(i,2) = rho0V*u0;                            // x-flux, mom. eq. 2
  val(i,3) = c0Sq*rho0U + p*u0;                   // x-flux, press. eq.
  val(i,4) = rho0V + rho*v0;                      // y-flux, cont. eq.
  val(i,5) = rho0U*v0;                            // y-flux, mom. eq. 1
  val(i,6) = rho0V*v0 + p;                        // y-flux, mom. eq. 2
  val(i,7) = c0Sq*rho0V + p*v0;                   // y-flux, press. eq.
}



void CalcLEEFlux3D(double rho0, double u0, double v0, double w0, double p0, double c0Sq,
                           double rho, double rho0U, double rho0V, double rho0W, double p,
                                                             int i, fullMatrix<double> &val)
{
  val(i,0) = rho0U + rho*u0;                      // x-flux, cont. eq.
  val(i,1) = rho0U*u0 + p;                        // x-flux, mom. eq. 1
  val(i,2) = rho0V*u0;                        // x-flux, mom. eq. 2
  val(i,3) = rho0W*u0;                        // x-flux, mom. eq. 3
  val(i,4) = c0Sq*rho0U + p*u0;                   // x-flux, press. eq.
  val(i,5) = rho0V + rho*v0;                      // y-flux, cont. eq.
  val(i,6) = rho0U*v0;                        // y-flux, mom. eq. 1
  val(i,7) = rho0V*v0 + p;                        // y-flux, mom. eq. 2
  val(i,8) = rho0W*v0;                        // y-flux, mom. eq. 3
  val(i,9) = c0Sq*rho0V + p*v0;                   // y-flux, press. eq.
  val(i,10) = rho0W + rho*w0;                     // z-flux, cont. eq.
  val(i,11) = rho0U*w0;                       // z-flux, mom. eq. 1
  val(i,12) = rho0V*w0;                       // z-flux, mom. eq. 2
  val(i,13) = rho0W*w0 + p;                       // z-flux, mom. eq. 3
  val(i,14) = c0Sq*rho0W + p*w0;                  // z-flux, press. eq.
}



class dgConservationLawLEE::volumeTerm1 : public function {


  int _dim;
  fullMatrix<double> _sol, _meanFlow;

public:

  volumeTerm1(int dim, const function *meanFlowFunc) : function(dim*(dim+2)) {
    _dim = dim;
    setArgument(_sol, function::getSolution());
    setArgument(_meanFlow, meanFlowFunc);
  };

  void call(dataCacheMap *m, fullMatrix<double> &val) {
    if (_dim == 2)
      for (int i=0; i<val.size1(); i++) {
        const double &rho = _sol(i,0);
        const double &rho0U = _sol(i,1), &rho0V = _sol(i,2);
        const double &p = _sol(i,3);
        const double &rho0 = _meanFlow(i,0);
        const double &u0 = _meanFlow(i,1), &v0 = _meanFlow(i,2);
        const double &p0 = _meanFlow(i,3);
        const double c0Sq = _gamma*p0/rho0;
        CalcLEEFlux2D(rho0, u0, v0, p0, c0Sq, rho, rho0U, rho0V, p, i, val);
    }
    else
      for (int i=0; i<val.size1(); i++) {
        const double &rho = _sol(i,0);
        const double &rho0U = _sol(i,1), &rho0V = _sol(i,2), &rho0W = _sol(i,3);
        const double &p = _sol(i,4);
        const double &rho0 = _meanFlow(i,0);
        const double &u0 = _meanFlow(i,1), &v0 = _meanFlow(i,2), &w0 = _meanFlow(i,3);
        const double &p0 = _meanFlow(i,4);
        const double c0Sq = _gamma*p0/rho0;
        CalcLEEFlux3D(rho0, u0, v0, w0, p0, c0Sq, rho, rho0U, rho0V, rho0W, p, i, val);
      }
  };

};



fullVector<double> upwindFlux2D(double nx, double ny, double rho0, double u0, double v0, double p0,
                                                double rhoL, double rho0UL, double rho0VL, double pL,
                                                  double rhoR, double rho0UR, double rho0VR, double pR)
{


  const double c0Sq = _gamma*p0/rho0;
  const double c0 = sqrt(c0Sq);
  const double c0Inv = 1./c0;

  const double v0n = u0*nx+v0*ny;
  double fluxEnt, fluxVort;                                                       // 2 advected modes (entropy + vorticity)
  if (v0n > 0.) {
    fluxEnt = v0n*(rhoL-pL/c0Sq);
    fluxVort = v0n*(ny*rho0UL-nx*rho0VL);
  }
  else if (v0n < 0.) {
    fluxEnt = v0n*(rhoR-pR/c0Sq);
    fluxVort = v0n*(ny*rho0UR-nx*rho0VR);
  }
  else {
    fluxEnt = 0.;
    fluxVort = 0.;
  }
  const double fluxAcR = (v0n+c0)*(nx*rho0UL+ny*rho0VL+c0Inv*pL);                 // Right-travelling acoustic mode
  const double fluxAcL = (v0n-c0)*(-nx*rho0UR-ny*rho0VR+c0Inv*pR);                // Left-travelling acoustic mode

  fullVector<double> rFlux(4);
  rFlux(0) = fluxEnt+0.5*c0Inv*(fluxAcR+fluxAcL);
  rFlux(1) = ny*fluxVort+0.5*nx*(fluxAcR-fluxAcL);
  rFlux(2) = -nx*fluxVort+0.5*ny*(fluxAcR-fluxAcL);
  rFlux(3) = 0.5*c0*(fluxAcR+fluxAcL);

  return rFlux;

}



fullVector<double> upwindFlux3D(double nx, double ny, double nz, double rho0, double u0, double v0, double w0, double p0,
                                                      double rhoL, double rho0UL, double rho0VL, double rho0WL, double pL,
                                                        double rhoR, double rho0UR, double rho0VR, double rho0WR, double pR)
{


  const double c0Sq = _gamma*p0/rho0;
  const double c0 = sqrt(c0Sq);
  const double c0Inv = 1./c0;

  const double v0n = u0*nx+v0*ny+w0*nz;
  const double c0InvSq = c0Inv*c0Inv;
  double fluxAdv0, fluxAdv1, fluxAdv2;                                            // 3 advected modes (entropy + vorticity)
  if (v0n > 0.) {
    fluxAdv0 = v0n*(nx*rhoL+nz*rho0VL-ny*rho0WL-nx*c0InvSq*pL);
    fluxAdv1 = v0n*(ny*rhoL-nz*rho0UL+nx*rho0WL-ny*c0InvSq*pL);
    fluxAdv2 = v0n*(nz*rhoL+ny*rho0UL-nx*rho0VL-nz*c0InvSq*pL);
  }
  else if (v0n < 0.) {
    fluxAdv0 = v0n*(nx*rhoR+nz*rho0VR-ny*rho0WR-nx*c0InvSq*pR);
    fluxAdv1 = v0n*(ny*rhoR-nz*rho0UR+nx*rho0WR-ny*c0InvSq*pR);
    fluxAdv2 = v0n*(nz*rhoR+ny*rho0UR-nx*rho0VR-nz*c0InvSq*pR);
  }
  else {
    fluxAdv0 = 0.;
    fluxAdv1 = 0.;
    fluxAdv2 = 0.;
  }
  const double fluxAcR = (v0n+c0)*(nx*rho0UL+ny*rho0VL+nz*rho0WL+c0Inv*pL);       // Right-travelling acoustic mode
  const double fluxAcL = (v0n-c0)*(-nx*rho0UR-ny*rho0VR-nz*rho0WR+c0Inv*pR);      // Left-travelling acoustic mode

  fullVector<double> rFlux(5);
  rFlux(0) = nx*fluxAdv0+ny*fluxAdv1+nz*fluxAdv2+0.5*c0Inv*(fluxAcR+fluxAcL);
  rFlux(1) = -nz*fluxAdv1+ny*fluxAdv2+0.5*nx*(fluxAcR-fluxAcL);
  rFlux(2) = nz*fluxAdv0-nx*fluxAdv2+0.5*ny*(fluxAcR-fluxAcL);
  rFlux(3) = -ny*fluxAdv0+nx*fluxAdv1+0.5*nz*(fluxAcR-fluxAcL);
  rFlux(4) = 0.5*c0*(fluxAcR+fluxAcL);

  return rFlux;

}



fullVector<double> LFFlux2D(double nx, double ny, double rho0, double u0, double v0, double p0,
                                                double rhoL, double rho0UL, double rho0VL, double pL,
                                                  double rhoR, double rho0UR, double rho0VR, double pR)
{


  const double c0Sq = _gamma*p0/rho0;
  const double maxCharVel = sqrt(c0Sq)+fabs(u0*nx+v0*ny);

  fullMatrix<double> FL(1,8), FR(1,8);
  CalcLEEFlux2D(rho0, u0, v0, p0, c0Sq, rhoL, rho0UL, rho0VL, pL, 0, FL);
  CalcLEEFlux2D(rho0, u0, v0, p0, c0Sq, rhoR, rho0UR, rho0VR, pR, 0, FR);
  fullVector<double> Fn(4);
  for (int i=0; i<4; i++) Fn(i) = (FL(0,i)+FR(0,i))*nx+(FL(0,i+4)+FR(0,i+4))*ny;

  fullVector<double> rFlux(4);
  rFlux(0) = 0.5*(Fn(0)-maxCharVel*(rhoR-rhoL));
  rFlux(1) = 0.5*(Fn(1)-maxCharVel*(rho0UR-rho0UL));
  rFlux(2) = 0.5*(Fn(2)-maxCharVel*(rho0VR-rho0VL));
  rFlux(3) = 0.5*(Fn(3)-maxCharVel*(pR-pL));

  return rFlux;

}



fullVector<double> LFFlux3D(double nx, double ny, double nz, double rho0, double u0, double v0, double w0, double p0,
                                                  double rhoL, double rho0UL, double rho0VL, double rho0WL, double pL,
                                                    double rhoR, double rho0UR, double rho0VR, double rho0WR, double pR)
{


  const double c0Sq = _gamma*p0/rho0;
  const double maxCharVel = sqrt(c0Sq)+fabs(u0*nx+v0*ny+w0*nz);

  fullMatrix<double> FL(1,15), FR(1,15);
  CalcLEEFlux3D(rho0, u0, v0, w0, p0, c0Sq, rhoL, rho0UL, rho0VL, rho0WL, pL, 0, FL);
  CalcLEEFlux3D(rho0, u0, v0, w0, p0, c0Sq, rhoR, rho0UR, rho0VR, rho0WR, pR, 0, FR);
  fullVector<double> Fn(5);
  for (int i=0; i<5; i++) Fn(i) = (FL(0,i)+FR(0,i))*nx+(FL(0,i+5)+FR(0,i+5))*ny+(FL(0,i+10)+FR(0,i+10))*nz;

  fullVector<double> rFlux(5);
  rFlux(0) = 0.5*(Fn(0)-maxCharVel*(rhoR-rhoL));
  rFlux(1) = 0.5*(Fn(1)-maxCharVel*(rho0UR-rho0UL));
  rFlux(2) = 0.5*(Fn(2)-maxCharVel*(rho0VR-rho0VL));
  rFlux(3) = 0.5*(Fn(3)-maxCharVel*(rho0WR-rho0WL));
  rFlux(4) = 0.5*(Fn(4)-maxCharVel*(pR-pL));

  return rFlux;

}



class dgConservationLawLEE::interfaceTerm : public function {


  int _dim;
  fullMatrix<double> _normals, _solL, _solR, _meanFlow;

public:

  interfaceTerm(int dim, const function *meanFlowFunc) : function(2*(dim+2)) {
    _dim = dim;
    setArgument (_normals, function::getNormals());
    setArgument (_solL, function::getSolution(), 0);
    setArgument (_solR, function::getSolution(), 1);
    setArgument (_meanFlow, meanFlowFunc);
  }

  void call(dataCacheMap *m, fullMatrix<double> &val) {
    if (_dim == 2)
      for(int i=0; i<val.size1(); i++) {
        fullVector<double> rFlux = upwindFlux2D(_normals(i,0), _normals(i,1),
                                                _meanFlow(i,0), _meanFlow(i,1), _meanFlow(i,2), _meanFlow(i,3),
                                                                  _solL(i,0), _solL(i,1), _solL(i,2), _solL(i,3),
                                                                  _solR(i,0), _solR(i,1), _solR(i,2), _solR(i,3));
        for (int j=0; j<4; j++) {
          val(i,j) = -rFlux(j);
          val(i,j+4) = rFlux(j);
        }
      }
    else
      for(int i=0; i<val.size1(); i++) {
        fullVector<double> rFlux = upwindFlux3D(_normals(i,0), _normals(i,1), _normals(i,2), _meanFlow(i,0),
                                                _meanFlow(i,1), _meanFlow(i,2), _meanFlow(i,3), _meanFlow(i,4),
                                                _solL(i,0), _solL(i,1), _solL(i,2), _solL(i,3), _solL(i,4), _solR(i,0),
                                                                        _solR(i,1), _solR(i,2), _solR(i,3), _solR(i,4));
        for (int j=0; j<5; j++) {
          val(i,j) = -rFlux(j);     // Flux for left element
          val(i,j+5) = rFlux(j);    // Flux for right element
        }
      }
  };

};



class dgConservationLawLEE::maxConvectiveSpeed : public function {


  int _dim;
  fullMatrix<double> _meanFlow;

public:

  maxConvectiveSpeed(int dim, const function *meanFlowFunc) : function(1) {
    _dim = dim;
    setArgument(_meanFlow, meanFlowFunc);
  };

  void call(dataCacheMap *m, fullMatrix<double> &val) {
    for(int i=0; i<val.size1(); i++) {
      const double c0Sq = _gamma*_meanFlow(i,_dim+1)/_meanFlow(i,0);
      double v0Sq = _meanFlow(i,1)*_meanFlow(i,1)+_meanFlow(i,2)*_meanFlow(i,2);
      if (_dim == 3) v0Sq += _meanFlow(i,3)*_meanFlow(i,3);
      val(i,0) = sqrt(c0Sq)+sqrt(v0Sq);
    }
  };

};



/*==============================================================================
 * Conservation law : constructor - setup - destructor
 *============================================================================*/

dgConservationLawLEE::dgConservationLawLEE(int dim, const std::string tag, const function *meanFlowFunc,
                                             const function *gradMeanFlowFunc, const function *sourceFunc) :
    dgConservationLawFunction(dim+2)
{

  _dim = dim;
  _meanFlowFunc = meanFlowFunc;
  _sourceFunc = sourceFunc;

  _volumeTerm0[tag] = new volumeTerm0(dim, meanFlowFunc, gradMeanFlowFunc, sourceFunc);
  _volumeTerm1[tag] = new volumeTerm1(dim, meanFlowFunc);
  _interfaceTerm0[tag] = new interfaceTerm(dim, meanFlowFunc);
  _maximumConvectiveSpeed[tag] = new maxConvectiveSpeed(dim, meanFlowFunc);

}

dgConservationLawLEE::~dgConservationLawLEE() {
}



/*==============================================================================
 * Specific boundary conditions
 *============================================================================*/

class dgBoundaryConditionLEEWall : public dgBoundaryCondition {

  class term : public function {
    int _dim;
    bool _MFCorrection;
    fullMatrix<double> _normals, _solL, _meanFlow, _pVal;
  public:
    term(int dim, const function *meanFlowFunc, const bool MFCorrection) : function(dim+2) {
      _dim = dim;
      _MFCorrection = MFCorrection;
      setArgument (_normals, function::getNormals());
      setArgument (_solL, function::getSolution());
      setArgument (_meanFlow, meanFlowFunc);
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      if (_dim == 2)
        for(int i=0; i<val.size1(); i++) {
          const double &nx = _normals(i,0), &ny = _normals(i,1);
          const double &rhoL = _solL(i,0);
          const double &rho0UL = _solL(i,1), &rho0VL = _solL(i,2);
          const double &pL = _solL(i,3);
          const double &rho0 = _meanFlow(i,0);
          double u0 = _meanFlow(i,1), v0 = _meanFlow(i,2);
          if (_MFCorrection) {
            const double Vel0N = u0*nx + v0*ny;
            u0 -= Vel0N*nx;
            v0 -= Vel0N*ny;
          }
          const double &p0 = _meanFlow(i,3);
          const double c0Sq = _gamma*p0/rho0;
          const double rho0VelN = rho0UL*nx + rho0VL*ny;
          const double rho0UBC = rho0UL-rho0VelN*nx, rho0VBC = rho0VL-rho0VelN*ny;
          fullMatrix<double> flux(1,8);
          CalcLEEFlux2D(rho0, u0, v0, p0, c0Sq, rhoL, rho0UBC, rho0VBC, pL, 0, flux);
          for (int j=0; j<4; j++) val(i,j) = -(flux(0,j)*nx+flux(0,j+4)*ny);
        }
      else
        for(int i=0; i<val.size1(); i++) {
          const double &nx = _normals(i,0), &ny = _normals(i,1), &nz = _normals(i,2);
          const double &rhoL = _solL(i,0);
          const double &rho0UL = _solL(i,1), &rho0VL = _solL(i,2), &rho0WL = _solL(i,3);
          const double &pL = _solL(i,4);
          const double &rho0 = _meanFlow(i,0);
          double u0 = _meanFlow(i,1), v0 = _meanFlow(i,2), w0 = _meanFlow(i,3);
          if (_MFCorrection) {
            const double Vel0N = u0*nx + v0*ny + w0*nz;
            u0 -= Vel0N*nx;
            v0 -= Vel0N*ny;
            w0 -= Vel0N*nz;
          }
          const double &p0 = _meanFlow(i,4);
          const double c0Sq = _gamma*p0/rho0;
          const double rho0VelN = rho0UL*nx + rho0VL*ny + rho0WL*nz;
          const double rho0UBC = rho0UL-rho0VelN*nx, rho0VBC = rho0VL-rho0VelN*ny, rho0WBC = rho0WL-rho0VelN*nz;
          fullMatrix<double> flux(1,15);
          CalcLEEFlux3D(rho0, u0, v0, w0, p0, c0Sq, rhoL, rho0UBC, rho0VBC, rho0WBC, pL, 0, flux);
          for (int j=0; j<5; j++) val(i,j) = -(flux(0,j)*nx+flux(0,j+5)*ny+flux(0,j+10)*nz);
        }
    };
  };

public:

  dgBoundaryConditionLEEWall(int dim, const function *meanFlowFunc, const bool MFCorrection) {
    _term0 = new term(dim, meanFlowFunc, MFCorrection);
  }

  ~dgBoundaryConditionLEEWall() {
    delete _term0;
  }

};



class dgBoundaryConditionLEENonReflect : public dgBoundaryCondition {

  class term : public function {
    int _dim;
    fullMatrix<double> _normals, _solL, _meanFlow, _pVal;
  public:
    term(int dim, const function *meanFlowFunc, const function *prescFunc) : function(dim+2) {
      _dim = dim;
      setArgument (_normals, function::getNormals());
      setArgument (_solL, function::getSolution());
      setArgument (_meanFlow, meanFlowFunc);
      setArgument (_pVal, prescFunc);
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      if (_dim == 2)
        for(int i=0; i<val.size1(); i++) {
          fullVector<double> rFlux = upwindFlux2D(_normals(i,0), _normals(i,1),
                                                  _meanFlow(i,0), _meanFlow(i,1), _meanFlow(i,2), _meanFlow(i,3),
                                                                    _solL(i,0), _solL(i,1), _solL(i,2), _solL(i,3),
                                                                    _pVal(i,0), _pVal(i,1), _pVal(i,2), _pVal(i,3));
          for (int j=0; j<4; j++) val(i,j) = -rFlux(j);
        }
      else
        for(int i=0; i<val.size1(); i++) {
          fullVector<double> rFlux = upwindFlux3D(_normals(i,0), _normals(i,1), _normals(i,2), _meanFlow(i,0),
                                                  _meanFlow(i,1), _meanFlow(i,2), _meanFlow(i,3), _meanFlow(i,4),
                                                        _solL(i,0), _solL(i,1), _solL(i,2), _solL(i,3), _solL(i,4),
                                                        _pVal(i,0), _pVal(i,1), _pVal(i,2), _pVal(i,3), _pVal(i,4));
          for (int j=0; j<5; j++) val(i,j) = -rFlux(j);
        }
    };
  };

public:

  dgBoundaryConditionLEENonReflect(int dim, const function *meanFlowFunc, const function *prescFunc = 0) {
    _term0 = new term(dim, meanFlowFunc, prescFunc);
  }

  ~dgBoundaryConditionLEENonReflect() {
    delete _term0;
  }

};



dgBoundaryCondition *dgConservationLawLEE::newBoundaryWall(const bool MFCorrection) {
  return new dgBoundaryConditionLEEWall(_dim,_meanFlowFunc,MFCorrection);
}



dgBoundaryCondition *dgConservationLawLEE::newBoundaryNonReflect(const function *prescFunc) {
  const function *pFunc = (prescFunc == 0) ? new functionConstant(std::vector<double>(_dim+2,0.)) : prescFunc;
  return new dgBoundaryConditionLEENonReflect(_dim,_meanFlowFunc,pFunc);
}
