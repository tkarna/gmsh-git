%module lee

%{
  #undef HAVE_DLOPEN
  #include "dgConservationLawLEE.h"
%}

%import(module="dgpy.conservationLaw") "dgConservationLaw.h";
%import(module="dgpy.conservationLaw") "dgConservationLawFunction.h";

%include "dgConservationLawLEE.h"
