#ifndef _DG_CONSERVATION_LAW_LEE_H
#define _DG_CONSERVATION_LAW_LEE_H

#include "dgConservationLawFunction.h"
class dgDofContainer;



class dgConservationLawLEE : public dgConservationLawFunction
{

public:
  virtual bool isLinear() const;
  virtual bool isConstantJac() const;
  dgConservationLawLEE(int dim, const std::string tag, const function *meanFlowFunc,
                         const function *gradMeanFlowFunc, const function *sourceFunc = 0);
  ~dgConservationLawLEE();

  // Add new wall BC. If MFCorrection is true, force mean flow parallel to face in BC calculation.
  dgBoundaryCondition *newBoundaryWall(const bool MFCorrection = false);
  // Add new characteristic-based non-reflecting BC, optionally prescribing "external" variables (prescFunc)
  dgBoundaryCondition *newBoundaryNonReflect(const function *prescFunc = 0);

protected:

  int _dim;
  const function *_meanFlowFunc, *_sourceFunc;

  // DG-terms
  class volumeTerm0;
  class volumeTerm1;
  class interfaceTerm;
  class maxConvectiveSpeed;

};



#endif
