//Lagrangian random walk advection-diffusion code. Offline particle-tracker. Called from /GreatBarrierReef/LPT_Caller.py

#include <fstream>
#include <iostream>
#include <cmath>
#include <ctime>

#include <cstdlib>
#include <sstream>
#include <iomanip>
#include "dgParticleTracker2D.h"

using namespace std;

void dgParticleTracker2D::particleMoveSeeds(dgDofContainer* solution, dgDofContainer* windUV, fullMatrix<double>* connectivityMatrix, int n, double windcoef) 
{
  //std::cout<<"Debug: move seeds\n";
  //Set random number generator seed using system clock
  if (n==1) {
    srand(time(NULL)+n);//time(NULL));
  }
  //Define dynamic variables
  double t=0, z=0, dx, dy, pullv=0, pullu=0, d, dGrad_x, dGrad_y, K, dKdx=0, dKdy=0, K_xyplusHalfdxdy;
  
  int M = _ParticleArray->printNbParticles();

  //Define position and velocity variables
  double u, v;
  double *randoox = new double[M];
  double *randooy = new double[M];
  double jac[3][3], ijac[3][3];
  double dxyz[3], duvw[3];
  double b[3], a_proj_b[3], b_unit[3];
  double wu, wv;
  
  int dead, alive; 
  
  dgFunctionEvaluator FunctionEval(_group, solution->getFunction());
  dgFunctionEvaluator BathymetryEval(_group, _bathymetry->getFunction());
  dgFunctionEvaluator BathymetryGradEval(_group, _bathymetry->getFunctionGradient());
  dgFunctionEvaluator DiffusivityEval(_group, _diffusivity->getFunction());
  dgFunctionEvaluator DiffusivityGradEval(_group, _diffusivity->getFunctionGradient());
  dgFunctionEvaluator WindUVEval(_group, windUV->getFunction());
  
  //std::cout<<"Debug: init\n";

  //Initialise output files
  std::ostringstream fname;
  std::ofstream outStats;
  //Define filename
  fname << _LP_OutputDir << "/particleStats.dat";
  //Open file (replace if already exists)
  std::string temp = fname.str();
  outStats.open( temp.c_str(), std::ios::out | std::ios::app );

  //std::cout<<"Debug: file ok\n";
  
  //Set initial state as alive & initial positions of particles
  t=static_cast<double>(n)*_dt;
  dead=0; alive=0;	//Reset particle-counters
  
  //Generate random-number arrays for x & y which provide 1 random number per particle
  rando(randoox,M);
  rando(randooy,M);
  if (M==1) { 
    randoox[0] = (static_cast<double>(rand())/(static_cast<double>(RAND_MAX)+1.0) - 0.5)*2;
    randooy[0] = (static_cast<double>(rand())/(static_cast<double>(RAND_MAX)+1.0) - 0.5)*2;
  }
//  cout << "Random number arrays generated. M is still " <<M << endl;

  //std::cout<<"Debug: p loop.\n";
  //START OF PARTICLE LOOP:
  for (int m=0; m<M; m++) //m corresponds to particle number
  {
    if (_ParticleArray->getParticle(m)->getState() != 0) continue;    //Do not recalculate particle position if it has already died or settled

    //Reality check
//     if (_ParticleArray->getParticle(m)->x() != _ParticleArray->getParticle(m)->x() || _ParticleArray->getParticle(m)->y() != _ParticleArray->getParticle(m)->y()) {
//       cout <<endl<< "Warning! NaN detected at start of main loop!\n -> n: "<<n<<", m: "<<m<<", particle state: "<<_ParticleArray->getParticle(m)->getState()<<" || x: "<<_ParticleArray->getParticle(m)->x()<<", y:"<< _ParticleArray->getParticle(m)->y()<<endl;
//     }
    
    //Load position variables:
    double x_n = _ParticleArray->getParticle(m)->x();
    double y_n = _ParticleArray->getParticle(m)->y();
    SPoint3 xyz(x_n, y_n, 0.0), uvw;
    double x_nplus1, y_nplus1;
    
    int iGroup, iElem;
    _ParticleArray->getParticle(m)->getElementId(&iGroup, &iElem);
    if (iGroup > 1) printf("WARNING! iGroup is %d (m is %d)\n", iGroup, m);
    
    MElement *e;
    e = _group->getElementGroup(iGroup)->getElement(iElem);
    e->xyz2uvw(xyz, uvw);
    
    //Pre-calculation checks:
    //1. Check if particle dies 
    if (_mortFlag == true) {
      if (isDead(t) == 1) {
        dead++;
        std::cout<<"Debug: dead.\n";
        _ParticleArray->getParticle(m)->setState(2);
        continue;
      }
    }
    
    //2&3. Check if particle competency changes and if particle settles
    if (_settleFlag == true) {
      
      if (_settleType == 3 && t > _t_preComp) {
        int particleComp = _ParticleArray->getParticle(m)->getCompetency();
        _ParticleArray->getParticle(m)->setCompetency( assessCompetency(particleComp, t) );
      }
    }
    
    //4. Calculate depth
    fullMatrix<double> bathFM;
    fullMatrix<double> bathGradFM;
    fullMatrix<double> diffFM;
    fullMatrix<double> diffGradFM;
    
    BathymetryEval.computeElem(iGroup, iElem, uvw[0], uvw[1], uvw[2], bathFM);
    BathymetryGradEval.computeElem(iGroup, iElem, uvw[0], uvw[1], uvw[2], bathGradFM);
    d=bathFM(0,0);
    dGrad_x=bathGradFM(0,0);
    dGrad_y=bathGradFM(0,1);

    //If particle is alive and unsettled, record it:
    alive++;
    
    //6. Get field velocity at current point:
    fullMatrix<double> r;
    FunctionEval.computeElem(iGroup, iElem, uvw[0], uvw[1], uvw[2], r);
//    double r_elev;
//    r_elev = r(0,0);
    u = r(0,1);
    v = r(0,2);
//      cout <<endl<< "******>> r_elev = "<< r_elev<< ", r_u = " << u << ", r_v = " << v <<" <<******"<<endl;

    //7. Get diffusivity
    DiffusivityEval.computeElem(iGroup, iElem, uvw[0], uvw[1], uvw[2], diffFM);
    DiffusivityGradEval.computeElem(iGroup, iElem, uvw[0], uvw[1], uvw[2], diffGradFM);
    K = diffFM(0,0);
    dKdx = diffGradFM(0,0);
    dKdy = diffGradFM(0,1);
//      cout<<"K = "<<K<<endl;
//      cout<<"dKdx = "<<dKdx<<", dKdy = "<<dKdy<<endl;
    
    //std::cout<<"Debug: wind\n";
    // Retrieve wind speed
    fullMatrix<double> windFM;
    WindUVEval.computeElem(iGroup, iElem, uvw[0], uvw[1], uvw[2], windFM);
    wu = windFM(0,0);
    wv = windFM(0,1);

    //8. Calculate advective element
    dx = _dt * ( (K/d)*dGrad_x + dKdx + u + pullu  + wu*windcoef);
    dy = _dt * ( (K/d)*dGrad_y + dKdy + v + pullv + wv*windcoef);
//     dx = _dt * K*(1.0/5000.0); // ( (K/d)*dGrad_x + dKdx + u + pullu );
//     dy = _dt * 0.0; //( (K/d)*dGrad_y + dKdy + v + pullv );
    
    K_xyplusHalfdxdy=K;

    if (u == 0 && v == 0) cout<<"Warning! Advective components are null! n is "<<n<<endl;  
    //10. Compute next step of path (ie. pos.n at t=t+_dt)
    x_nplus1 = x_n + dx + randoox[m] * sqrt( 2.0 * K_xyplusHalfdxdy * _dt );
    y_nplus1 = y_n + dy + randooy[m] * sqrt( 2.0 * K_xyplusHalfdxdy * _dt );
    
    //11. Move the particle through elements to destination
    dxyz[0] = x_nplus1 - x_n;
    dxyz[1] = y_nplus1 - y_n;

    int index=0;
    while (1)//index<=40)	
    {
      index++;
      //Get uvw (NB: need to have new uvw at start of each loop cycle)
      e->xyz2uvw(xyz, uvw);
//      cout << endl<<"--------------------"<<endl<<"* Beginning \"while\" loop || Index="<<index<<" || Currently in Element "<<e->getNum()<<" || u="<<uvw[0]<<" v="<<uvw[1]<<" w="<<uvw[2]<<" || x="<<xyz[0]<<" y="<<xyz[1]<<" z="<<xyz[2]<<endl;
      
      /*dxyz --> duvw*/
      e->getJacobian(0, 0, 0, jac);
      Inverse(jac, ijac);
      //Transpose(ijac);

      convertVectorCoords(duvw, dxyz, ijac);
      
      for (int i=0; i<3; i++)	{	uvw[i] += duvw[i]; }
      
      double remaining = 0.;
      int edge_id=-1;
      if(uvw[1] < 0 && uvw[1] / duvw[1] > remaining) {
        edge_id = 0;
        remaining = uvw[1] / duvw[1];
      }
      if(uvw[0] + uvw[1] - 1 > 0 && (uvw[0] + uvw[1] - 1) / (duvw[0] + duvw[1]) > remaining) {
        edge_id = 1;
        remaining = (uvw[0] + uvw[1] - 1) / (duvw[0] + duvw[1]);
      }
      if(uvw[0] < 0 && uvw[0] / duvw[0] > remaining) {
        edge_id = 2;
        remaining = uvw[0] / duvw[0];
      }
      if(edge_id == -1) {
        e->primaryPnt(uvw[0],uvw[1],uvw[2], xyz);
        break;
      }

      /*Move particle to element edge*/
      uvw[0] = uvw[0] - (duvw[0]*remaining);
      uvw[1] = uvw[1] - (duvw[1]*remaining);
      /*Update remaining vector*/
      dxyz[0] *= remaining; //Used to recalculate duvw at beginning of every cycle
      dxyz[1] *= remaining;
      /*Retrieve and record current co-ordinates*/
      e->primaryPnt(uvw[0],uvw[1],uvw[2], xyz);
      /*Get neighbour at this edge*/
      int neighGroup, neighId;
      _neighbourMap->getNeighbour(iGroup, iElem, edge_id, neighGroup, neighId);

      //If there is no neighbour...
      if (neighGroup == -1) {
        
        /*First, check if we are at the Open Sea boundary; if so, set new state of particle and leave it where it is*/
        int faceGroupId, faceId, connectionId;
        _neighbourMap->getFace(iGroup, iElem, edge_id, faceGroupId, faceId, connectionId);
        std::string tag = _group->getFaceGroup(faceGroupId)->physicalTag();
        if (tag == "Open Sea North" || tag == "Open Sea Central" || tag == "Open Sea South" || tag == "Open Sea Neutral") {
          _ParticleArray->getParticle(m)->setState(3);
          break;
        }
        
        /*1. Compute edge vectors*/
        int vertex_id = (edge_id+1)%3;
        int vertex2_id = edge_id;
        
        MVertex *vert_1 = e->getVertex(vertex_id);
        MVertex *vert_2 = e->getVertex(vertex2_id);
        
        b_unit[0] = vert_1->x() - vert_2->x();
        b_unit[1] = vert_1->y() - vert_2->y();
        b[0] = vert_1->x() - xyz.x();
        b[1] = vert_1->y() - xyz.y();
        
        /*2. Invert edge vector direction if particle moving backwards*/
        if (projectionMag(dxyz,b_unit)<0) { 
          vertex_id = edge_id;
          vertex2_id = (edge_id+1)%3;
          
          b_unit[0] = vert_2->x() - vert_1->x();
          b_unit[1] = vert_2->y() - vert_1->y(); 
          b[0] = vert_2->x() - xyz.x();
          b[1] = vert_2->y() - xyz.y();
        }
        
        /*3. Project particle vector onto element edge*/
        projection(dxyz,b_unit,a_proj_b);

        /*4. Test: Is projection smaller or greater than remaining?*/
        if (projectionMag(dxyz,b_unit) < sqrt(b[0]*b[0]+b[1]*b[1]))	{
          /*4a. Move to projected point*/
          xyz.setPosition(xyz.x()+a_proj_b[0], xyz.y()+a_proj_b[1], z);       
//          cout << "Projection of vector onto element edge. Mag(a_proj_b)="<<projectionMag(duvw,b_unit)<<". Mag(b)="<<sqrt(b[0]*b[0]+b[1]*b[1])<<" Breaking."<< endl;
          for (int i=0; i<3; i++) { dxyz[i]=0; }
          break;
        }
        else {
          /*4b.1. Move to corner and resize vector*/
          xyz.setPosition(xyz.x()+b[0], xyz.y()+b[1], z);
          for (int i=0; i<2; i++) { dxyz[i] = a_proj_b[i] - b[i]; }
          
          /*4b.2. Identify neighbour (NB: edge_id different from previous use of getNeighbour)*/
          if (vertex_id==edge_id) edge_id=(edge_id+2)%3;
          else edge_id = (edge_id+1)%3;
          _neighbourMap->getNeighbour(iGroup, iElem, edge_id, neighGroup, neighId);
        }
       
      }
//      cout << "* New x,y,z: x="<<xyz[0]<<" y="<<xyz[1]<<" z="<<xyz[2]<<" || New dx,dy: dx="<<dxyz[0]<<" dy="<<dxyz[1]<<endl;
//      cout << "* New u,v,w: u="<<uvw[0]<<" v="<<uvw[1]<<" w="<<uvw[2]<<endl;
      
      /* If we have reached this point, then we should have a neighbour. 
       * Now move over to neighbouring element*/
      if (neighGroup != -1 && neighGroup < 1) { 
        iGroup = neighGroup;
        iElem = neighId;
        //Save new group & element IDs in particle object
        _ParticleArray->getParticle(m)->setElementId(iGroup, iElem);
        //NB: must update e for element-move loop (while(1){...})
        e = _group->getElementGroup(iGroup)->getElement(iElem);
      }
      else if (neighGroup == -1) {
        //cout<<"BIG Error!! Have projected to corner but have no neighbour! Trapped! Breaking..."<<endl;
        break;
      }
      else {
        printf("Error: neighbour's groupId is corrupted. Neighbour probably doesn't exist, but you're trying to go there. neighGroup = %d. Breaking...", neighGroup);
        break;
      } 
      
    } //END: element-move loop

    //Check for numerical errors so far:
    if (xyz[0] != xyz[0] || xyz[1] != xyz[1]) {
      cout << "NaN detected in main loop!\n -> n: "<<n<<", m: "<<m<<", particle state: "<<_ParticleArray->getParticle(m)->getState()<<" || x: "<<xyz[0]<<", y:"<< xyz[1]<<", x_nplus1: "<<x_nplus1<<", y_nplus1: "<<y_nplus1<<"\n";
    }
    //Set new x_n:
    x_nplus1 = xyz[0];
    y_nplus1 = xyz[1];
    _ParticleArray->getParticle(m)->setPosition(x_nplus1, y_nplus1);
    
    
    //Reset Reef-proximity advection terms
    pullu = 0;
    pullv = 0;
  }//END: m-loop

  //Record no. of particles in each state at the end of this time-steps
  // -> NB: the above loop 'sees' all particles that are still alive, 
  //        but not all the particles that are dead or settled or out of domain (only the ones settling/dying/leaving domain in this loop)
  int particleStats[5] = {0};
  //_ParticleArray->getStats(particleStats);
  if (_mortFlag == true) {
    for (int m=0; m<M; m++) {
      int state = _ParticleArray->getParticle(m)->getState();
      particleStats[state]++;
    }
  }

  _ParticleArray->setStats(particleStats);

  outStats << t << " " << particleStats[0] << " " << particleStats[1] << " " << particleStats[2] << " " << particleStats[3] << " " << particleStats[4] << endl;
  printf("End of loop cycle %d of %d. Particle stats: %d alive, %d settled, %d dead, %d out of domain. %d alive & competent. TOTAL: %d\n", n, _N, particleStats[0], particleStats[1], particleStats[2], particleStats[3], particleStats[4], particleStats[0]+particleStats[1]+particleStats[2]+particleStats[3]);

  //Deallocate arrays	
  delete[] randoox;
  delete[] randooy;
  
  outStats.close();
}


void dgParticleTracker2D::particleMove(dgDofContainer* solution, fullMatrix<double>* connectivityMatrix, int n) 
{
  //Set random number generator seed using system clock
  if (n==1) {
    srand(time(NULL)+n);//time(NULL));
  }
  //Define dynamic variables
  double t=0, z=0, dx, dy, pullv=0, pullu=0, d, dGrad_x, dGrad_y, K, dKdx=0, dKdy=0, K_xyplusHalfdxdy;
  
  int M = _ParticleArray->printNbParticles();

  //Define position and velocity variables
  double u, v;
  double *randoox = new double[M];
  double *randooy = new double[M];
  double jac[3][3], ijac[3][3];
  double dxyz[3], duvw[3];
  double b[3], a_proj_b[3], b_unit[3];
  
  int dead=0, settled=0, alive=0, outOfDomain=0, aliveAndCompetent=0;  //Set up particle counters
  
  dgFunctionEvaluator FunctionEval(_group, solution->getFunction());
  dgFunctionEvaluator BathymetryEval(_group, _bathymetry->getFunction());
  dgFunctionEvaluator BathymetryGradEval(_group, _bathymetry->getFunctionGradient());
  dgFunctionEvaluator DiffusivityEval(_group, _diffusivity->getFunction());
  dgFunctionEvaluator DiffusivityGradEval(_group, _diffusivity->getFunctionGradient());
  dgFunctionEvaluator ReefEval(_group, _reefs->getFunction());
  dgFunctionEvaluator ReefsDistanceEval(_group, _distToReefs->getFunction());
  dgFunctionEvaluator ReefsDirectionEval(_group, _directionToReefs->getFunction());
  
  t=static_cast<double>(n)*_dt;
  
  //Generate random-number arrays for x & y which provide 1 random number per particle
  rando(randoox,M);
  rando(randooy,M);
  if (M==1) { 
    randoox[0] = (static_cast<double>(rand())/(static_cast<double>(RAND_MAX)+1.0) - 0.5)*2;
    randooy[0] = (static_cast<double>(rand())/(static_cast<double>(RAND_MAX)+1.0) - 0.5)*2;
  }
//  cout << "Random number arrays generated. M is still " <<M << endl;

  //START OF PARTICLE LOOP:
  for (int m=0; m<M; m++) //m corresponds to particle number
  {
    if (_ParticleArray->getParticle(m)->getState() != 0) continue;    //Do not recalculate particle position if it has already died or settled

    //Pre-calculation checks: 1. Check if particle dies 
    if (_mortFlag == true) {
      if (isDead(t) == 1) {
        dead++;
        _ParticleArray->getParticle(m)->setState(2);
        continue;
      }
    }
    
    //Reality check
//     if (_ParticleArray->getParticle(m)->x() != _ParticleArray->getParticle(m)->x() || _ParticleArray->getParticle(m)->y() != _ParticleArray->getParticle(m)->y()) {
//       cout <<endl<< "Warning! NaN detected at start of main loop!\n -> n: "<<n<<", m: "<<m<<", particle state: "<<_ParticleArray->getParticle(m)->getState()<<" || x: "<<_ParticleArray->getParticle(m)->x()<<", y:"<< _ParticleArray->getParticle(m)->y()<<endl;
//     }
    
    //Load position variables:
    double x_n = _ParticleArray->getParticle(m)->x();
    double y_n = _ParticleArray->getParticle(m)->y();
    SPoint3 xyz(x_n, y_n, 0.0), uvw;
    double x_nplus1, y_nplus1;
    
    int iGroup, iElem;
    _ParticleArray->getParticle(m)->getElementId(&iGroup, &iElem);
    if (iGroup > 1) printf("WARNING! iGroup is %d (m is %d)\n", iGroup, m);
    
    MElement *e;
    e = _group->getElementGroup(iGroup)->getElement(iElem);
    e->xyz2uvw(xyz, uvw);
    
    //TODO: is the following needed?
    //Load element & group of elements
//     if (iGroup == -1 && iElem == -1) {   //group & element IDs are not set in particle object
//       cout << "WARNING: Group & element IDs are not set in the particle object. Check why. This simulation will take longer than necessary."
//       //Load element if not already loaded
//       e->xyz2uvw(xyz, uvw);
//       _group->find(e, iGroup, iElem);
//       e = _gm->getMeshElementByCoord(xyz, 2);
//     }
//    cout << "element is " << e->getNum()<<endl;
    
    //Pre-calculation checks: 2&3. Check if particle competency changes and if particle settles
    if (_settleFlag == true) {
      
      if (_settleType == 3 && t > _t_preComp) {
        int particleComp = _ParticleArray->getParticle(m)->getCompetency();
        _ParticleArray->getParticle(m)->setCompetency( assessCompetency(particleComp, t) );
      }
    
      //3. Check if particle settles
      if (_settleType != 0) { //NB: double-check that settling is also enabled in settleParams
        //3b. Check if we're on a reef
        fullMatrix<double> reefFM;
        ReefEval.computeElem(iGroup, iElem, 0.25, 0.25, 0.0, reefFM);
        int reefId = static_cast<int>(reefFM(0,0));
        if ( (reefId > 0 && _settleOverWhichReefType == 2) 
          || (reefId > _nbOfShallowReefs && _settleOverWhichReefType == 1) 
          || (reefId > 0 && reefId <= _nbOfShallowReefs && _settleOverWhichReefType == 0) ) //We are on REEF!
        {
          if (isSettled(t,m) == 1) {   //NB: could change this to settleMass(double) if needed
            settled++;
            _ParticleArray->getParticle(m)->setState(1);  
            //Update connectivity matrix
              //NB: subtract 1 from reef IDs so that the lowest ReefId (reefId=1) goes to first row/col of CMatrix (row/col 0)
            double previousConnectivity = connectivityMatrix->get(_ParticleArray->getParticle(m)->getSource()-1, reefId-1);
            connectivityMatrix->set(_ParticleArray->getParticle(m)->getSource()-1, reefId-1, previousConnectivity+1);
            continue;
          }
        }
      }
    }
    
    //4. Calculate depth
    fullMatrix<double> bathFM;
    fullMatrix<double> bathGradFM;
    fullMatrix<double> diffFM;
    fullMatrix<double> diffGradFM;
    
    BathymetryEval.computeElem(iGroup, iElem, uvw[0], uvw[1], uvw[2], bathFM);
    BathymetryGradEval.computeElem(iGroup, iElem, uvw[0], uvw[1], uvw[2], bathGradFM);
    d=bathFM(0,0);
    dGrad_x=bathGradFM(0,0);
    dGrad_y=bathGradFM(0,1);
    
//      cout <<"dGrad_x = "<<dGrad_x <<", dGrad_y = "<<dGrad_y<<", d/1,000 = "<<d/1000.0<<endl;
//      cout<<endl<<"Water depth = "<<d<<endl;
    
    //5. Calculate swimming velocity
    if (_swimFlag == true) {
      if (t > _swimStart*3600.0) {
        //Check if we are close enough to a reef to swim but not yet on a reef
        // -> nb: use different criteria for these two tests, as reefsDistFM can=0 even when not on reef - cf. notes
        fullMatrix<double> reefsDistFM;
        fullMatrix<double> reefFM;
        ReefsDistanceEval.computeElem(iGroup, iElem, uvw[0], uvw[1], uvw[2], reefsDistFM);
        ReefEval.computeElem(iGroup, iElem, 0.25, 0.25, 0.0, reefFM);
        if (reefsDistFM(0,0) < _swimDist*1000.0 && reefFM(0,0) < 0.1) {
          //Get vector pointing to nearest reef
          fullMatrix<double> reefDir;
          ReefsDirectionEval.computeElem(iGroup, iElem, uvw[0], uvw[1], uvw[2], reefDir);
          //Normalise vector
          double vectorMag = sqrt(reefDir(0,0)*reefDir(0,0) + reefDir(0,1)*reefDir(0,1));
          //Calculate swimming vector
          pullu = (reefDir(0,0)/vectorMag) * _swimSpeed;
          pullv = (reefDir(0,1)/vectorMag) * _swimSpeed;
          //Test:
          //if (n%1000==0) outSwimming << "VP ("<<x_n<<","<<y_n<<",0.0) {"<<pullu<<","<<pullv<<",0.0};"<<endl;
        }
      }
    }
    
    //6. Get field velocity at current point:
    fullMatrix<double> r;
    FunctionEval.computeElem(iGroup, iElem, uvw[0], uvw[1], uvw[2], r);
//    double r_elev;
//    r_elev = r(0,0);
    u = r(0,1);
    v = r(0,2);
//      cout <<endl<< "******>> r_elev = "<< r_elev<< ", r_u = " << u << ", r_v = " << v <<" <<******"<<endl;

    //7. Get diffusivity
    DiffusivityEval.computeElem(iGroup, iElem, uvw[0], uvw[1], uvw[2], diffFM);
    DiffusivityGradEval.computeElem(iGroup, iElem, uvw[0], uvw[1], uvw[2], diffGradFM);
    K = diffFM(0,0);
    dKdx = diffGradFM(0,0);
    dKdy = diffGradFM(0,1);
//      cout<<"K = "<<K<<endl;
//      cout<<"dKdx = "<<dKdx<<", dKdy = "<<dKdy<<endl;
    
    //8. Calculate advective element
    dx = _dt * ( (K/d)*dGrad_x + dKdx + u + pullu );
    dy = _dt * ( (K/d)*dGrad_y + dKdy + v + pullv );
//     dx = _dt * K*(1.0/5000.0); // ( (K/d)*dGrad_x + dKdx + u + pullu );
//     dy = _dt * 0.0; //( (K/d)*dGrad_y + dKdy + v + pullv );
    
    //9. Calculate diffusivity at x+0.5dx
//     SPoint3 xyz_K(x_n+0.5*dx, y_n+0.5*dy, 0), uvw_K;
//     MElement *e_xplusHalfdx = _gm->getMeshElementByCoord(xyz_K, 2);
//     if (e_xplusHalfdx) {
//       int iGroupK, iElemK;
//       //TODO: shouldn't this next line be e_xplusHalfdx->xyz2uvw(xyz_K, uvw_K); ??TODO - Done
//       e_xplusHalfdx->xyz2uvw(xyz_K, uvw_K);
//       _group->find(e_xplusHalfdx, iGroupK, iElemK);
//       DiffusivityEval.computeElem(iGroupK, iElemK, uvw_K[0], uvw_K[1], uvw_K[2], diffFM);
//       K_xyplusHalfdxdy = diffFM(0,0);
//       if (K_xyplusHalfdxdy<0.0) cout<<"Line 219: K_xyplusHalfdxdy is "<<K_xyplusHalfdxdy<<endl;
//     }
//     else {cout<<"Not ok. m:"<<m<<endl; K_xyplusHalfdxdy=K;}      //**** NB: Not very good? ****
    K_xyplusHalfdxdy=K;

    if (u == 0 && v == 0) cout<<"Warning! Advective components are null! n is "<<n<<endl;  
    //10. Compute next step of path (ie. pos.n at t=t+_dt)
    x_nplus1 = x_n + dx + randoox[m] * sqrt( 2.0 * K_xyplusHalfdxdy * _dt );
    y_nplus1 = y_n + dy + randooy[m] * sqrt( 2.0 * K_xyplusHalfdxdy * _dt );
    
    //11. Move the particle through elements to destination
    dxyz[0] = x_nplus1 - x_n;
    dxyz[1] = y_nplus1 - y_n;

//cout << "dx: "<<dx<<", dy: "<<dy<<" | Random multiplier: "<<sqrt( 2.0 * K_xyplusHalfdxdy * _dt )<<" | randoox, y: "<<randoox[m]<<","<<randooy[m]<< endl;
//cout << "x_n+1 - x_n: "<<x_nplus1-x_n<<", y_n+1WARNING - y_n: "<<y_nplus1-y_n<<endl;

//    cout << endl<<"======================"<<endl<<"x = " << xyz.x() << ", y = " << xyz.y() << ", z = " << xyz.z() <<endl;
//    cout << "dx,dy = "<<dx<<","<<dy<<" || randoox,y = "<<randoox[m]<<","<<randooy[m]<<endl;
//    cout << "219====> dxyz = "<<dxyz[0]<<" , "<<dxyz[1]<< " |||| ====> duvw = "<<duvw[0]<<" , "<<duvw[1]<<endl;

    int index=0;
    while (1)//index<=40)	
    {
      index++;
      //Get uvw (NB: need to have new uvw at start of each loop cycle)
      e->xyz2uvw(xyz, uvw);
//      cout << endl<<"--------------------"<<endl<<"* Beginning \"while\" loop || Index="<<index<<" || Currently in Element "<<e->getNum()<<" || u="<<uvw[0]<<" v="<<uvw[1]<<" w="<<uvw[2]<<" || x="<<xyz[0]<<" y="<<xyz[1]<<" z="<<xyz[2]<<endl;
      
      /*dxyz --> duvw*/
      e->getJacobian(0, 0, 0, jac);
      Inverse(jac, ijac);
      //Transpose(ijac);

      convertVectorCoords(duvw, dxyz, ijac);
//       if (index==1 || index==2) {
// //        outJac<<endl;
//         for (int j=0; j<3; j++)	{
//           for (int i=0; i<3; i++) {
// //            outJac << ijac[i][j]<<" ";
//           }
// //          outJac<<endl;
//         }
//       }
      
      for (int i=0; i<3; i++)	{	uvw[i] += duvw[i]; }
      
      double remaining = 0.;
      int edge_id=-1;
      if(uvw[1] < 0 && uvw[1] / duvw[1] > remaining) {
        edge_id = 0;
        remaining = uvw[1] / duvw[1];
      }
      if(uvw[0] + uvw[1] - 1 > 0 && (uvw[0] + uvw[1] - 1) / (duvw[0] + duvw[1]) > remaining) {
        edge_id = 1;
        remaining = (uvw[0] + uvw[1] - 1) / (duvw[0] + duvw[1]);
      }
      if(uvw[0] < 0 && uvw[0] / duvw[0] > remaining) {
        edge_id = 2;
        remaining = uvw[0] / duvw[0];
      }
      if(edge_id == -1) {
        e->primaryPnt(uvw[0],uvw[1],uvw[2], xyz);
        break;
      }

      /*Move particle to element edge*/
      uvw[0] = uvw[0] - (duvw[0]*remaining);
      uvw[1] = uvw[1] - (duvw[1]*remaining);
      /*Update remaining vector*/
      dxyz[0] *= remaining; //Used to recalculate duvw at beginning of every cycle
      dxyz[1] *= remaining;
      /*Retrieve and record current co-ordinates*/
      e->primaryPnt(uvw[0],uvw[1],uvw[2], xyz);
      /*Get neighbour at this edge*/
      int neighGroup, neighId;
      _neighbourMap->getNeighbour(iGroup, iElem, edge_id, neighGroup, neighId);

      //If there is no neighbour...
      if (neighGroup == -1) {
        
        /*First, check if we are at the Open Sea boundary; if so, set new state of particle and leave it where it is*/
        int faceGroupId, faceId, connectionId;
        _neighbourMap->getFace(iGroup, iElem, edge_id, faceGroupId, faceId, connectionId);
        std::string tag = _group->getFaceGroup(faceGroupId)->physicalTag();
        if (tag == "Open Sea North" || tag == "Open Sea Central" || tag == "Open Sea South" || tag == "Open Sea Neutral") {
          outOfDomain++;
          _ParticleArray->getParticle(m)->setState(3);
          break;
        }
        
        /*1. Compute edge vectors*/
        int vertex_id = (edge_id+1)%3;
        int vertex2_id = edge_id;
        
        MVertex *vert_1 = e->getVertex(vertex_id);
        MVertex *vert_2 = e->getVertex(vertex2_id);
        
        b_unit[0] = vert_1->x() - vert_2->x();
        b_unit[1] = vert_1->y() - vert_2->y();
        b[0] = vert_1->x() - xyz.x();
        b[1] = vert_1->y() - xyz.y();
        
        /*2. Invert edge vector direction if particle moving backwards*/
        if (projectionMag(dxyz,b_unit)<0) { 
          vertex_id = edge_id;
          vertex2_id = (edge_id+1)%3;
          
          b_unit[0] = vert_2->x() - vert_1->x();
          b_unit[1] = vert_2->y() - vert_1->y(); 
          b[0] = vert_2->x() - xyz.x();
          b[1] = vert_2->y() - xyz.y();
        }
        
        /*3. Project particle vector onto element edge*/
        projection(dxyz,b_unit,a_proj_b);

        /*4. Test: Is projection smaller or greater than remaining?*/
        if (projectionMag(dxyz,b_unit) < sqrt(b[0]*b[0]+b[1]*b[1]))	{
          /*4a. Move to projected point*/
          xyz.setPosition(xyz.x()+a_proj_b[0], xyz.y()+a_proj_b[1], z);       
//          cout << "Projection of vector onto element edge. Mag(a_proj_b)="<<projectionMag(duvw,b_unit)<<". Mag(b)="<<sqrt(b[0]*b[0]+b[1]*b[1])<<" Breaking."<< endl;
          for (int i=0; i<3; i++) { dxyz[i]=0; }
          break;
        }
        else {
          /*4b.1. Move to corner and resize vector*/
          xyz.setPosition(xyz.x()+b[0], xyz.y()+b[1], z);
          for (int i=0; i<2; i++) { dxyz[i] = a_proj_b[i] - b[i]; }
          
          /*4b.2. Identify neighbour (NB: edge_id different from previous use of getNeighbour)*/
          if (vertex_id==edge_id) edge_id=(edge_id+2)%3;
          else edge_id = (edge_id+1)%3;
          _neighbourMap->getNeighbour(iGroup, iElem, edge_id, neighGroup, neighId);
        }
       
      }
//      cout << "* New x,y,z: x="<<xyz[0]<<" y="<<xyz[1]<<" z="<<xyz[2]<<" || New dx,dy: dx="<<dxyz[0]<<" dy="<<dxyz[1]<<endl;
//      cout << "* New u,v,w: u="<<uvw[0]<<" v="<<uvw[1]<<" w="<<uvw[2]<<endl;
      
      /* If we have reached this point, then we should have a neighbour. 
       * Now move over to neighbouring element*/
      if (neighGroup != -1 && neighGroup < 1) { 
        iGroup = neighGroup;
        iElem = neighId;
        //Save new group & element IDs in particle object
        _ParticleArray->getParticle(m)->setElementId(iGroup, iElem);
        //NB: must update e for element-move loop (while(1){...})
        e = _group->getElementGroup(iGroup)->getElement(iElem);
      }
      else if (neighGroup == -1) {
        //cout<<"BIG Error!! Have projected to corner but have no neighbour! Trapped! Breaking..."<<endl;
        break;
      }
      else {
        printf("Error: neighbour's groupId is corrupted. Neighbour probably doesn't exist, but you're trying to go there. neighGroup = %d. Breaking...", neighGroup);
        break;
      } 
      
    } //END: element-move loop

    //Check for numerical errors so far:
    if (xyz[0] != xyz[0] || xyz[1] != xyz[1]) {
      cout << "NaN detected in main loop!\n -> n: "<<n<<", m: "<<m<<", particle state: "<<_ParticleArray->getParticle(m)->getState()<<" || x: "<<xyz[0]<<", y:"<< xyz[1]<<", x_nplus1: "<<x_nplus1<<", y_nplus1: "<<y_nplus1<<"\n";
    }
    //Set new x_n:
    x_nplus1 = xyz[0];
    y_nplus1 = xyz[1];
    _ParticleArray->getParticle(m)->setPosition(x_nplus1, y_nplus1);
    
    //If particle is alive and unsettled and still in the domain, record it:
    alive++;
    if (_ParticleArray->getParticle(m)->getCompetency() == 1) aliveAndCompetent++;
    
    //If t > t_precomp AND settling is disabled, then update connectivity matrix using exposure time method
    if (_connectivityExposureTimeFlag == true && _settleFlag == false) {
      if (t > _t_preComp && t < _t_postComp) {
        //Are we on REEF?
        fullMatrix<double> reefFM;
        ReefEval.computeElem(iGroup, iElem, 0.25, 0.25, 0.0, reefFM);
        int reefId = static_cast<int>(reefFM(0,0));
        
        if ( reefId > 0 ) //We are on REEF!
        {
          //NB: subtract 1 from reef IDs so that the lowest ReefId (reefId=1) goes to first row/col of CMatrix (row/col 0)
          double previousConnectivity = connectivityMatrix->get(_ParticleArray->getParticle(m)->getSource()-1, reefId-1);
          connectivityMatrix->set(_ParticleArray->getParticle(m)->getSource()-1, reefId-1, previousConnectivity+_dt);
        }
      }
    }
    
    //Reset Reef-proximity advection terms
    pullu = 0;
    pullv = 0;
  }//END: m-loop

  //Record no. of particles in each state at the end of this time-steps
  // -> NB: the above loop 'sees' all particles that are still alive, 
  //        but not all the particles that are dead or settled or out of domain (only the ones settling/dying/leaving domain in this loop)
//   int particleStats[5] = {0};
//   //_ParticleArray->getStats(particleStats);
//   if (_settleFlag == true) {
//     for (int m=0; m<M; m++) {
//       int state = _ParticleArray->getParticle(m)->getState();
//       particleStats[state]++;
//       if (state == 0 && _settleType == 3) {
//         int aliveAndCompetent = _ParticleArray->getParticle(m)->getCompetency();
//         if (aliveAndCompetent == 1) particleStats[4]++;
//       }
//     }
//   }

  int particleStats[5] = {0};
  _ParticleArray->getStats(particleStats);
  particleStats[0] = alive;
  particleStats[1] = particleStats[1] + settled;
  particleStats[2] = particleStats[2] + dead;
  particleStats[3] = particleStats[3] + outOfDomain;
  particleStats[4] = aliveAndCompetent;
  _ParticleArray->setStats(particleStats);
  
  //Write particleStats to file
  std::ostringstream fname;
  std::ofstream outStats;
  //Define filename
  fname << _LP_OutputDir << "/particleStats.dat";
  //Open file (replace if already exists)
  std::string temp = fname.str();
  
  outStats.open( temp.c_str(), std::ios::out | std::ios::app );
  outStats << t << " " << particleStats[0] << " " << particleStats[1] << " " << particleStats[2] << " " << particleStats[3] << " " << particleStats[4] << endl;
  outStats.close();
  
  printf("End of loop cycle %d of %d. Particle stats: %d alive, %d settled, %d dead, %d out of domain. %d alive & competent. TOTAL: %d\n", n, _N, particleStats[0], particleStats[1], particleStats[2], particleStats[3], particleStats[4], particleStats[0]+particleStats[1]+particleStats[2]+particleStats[3]);
  
  //Deallocate arrays	
  delete[] randoox;
  delete[] randooy;
}

/** Function to verify whether particle acquires or loses competency during time dt. 
 -> Returns 0 if pre-competent, 1 if competent, 2 if post-competent*/
int dgParticleTracker2D::assessCompetency(int particleComp, double t)
{
  if (t < _t_preComp) return 0;
  if (t > _t_postComp) return 2;
  
  //If particle is post-competent, stay that way
  if (particleComp == 2) return 2;
  
  double randomNb = ((randoraw()/2.0)+0.5);
  //TEMP:
  if (randomNb > 1.0) printf("Warning! RandomNb > 1.0 in assessCompetency!\n");
  
  //Check if particle gains competency (NB: acquisition rates are already in (dt-1))
  if (particleComp == 0) {
    if ( randomNb < _compAcquisitionRate ) return 1;
    else return 0;
  }
  else if (particleComp == 1) {
    if ( randomNb < _compLossRate ) return 2;
    else return 1;
  }
  
  printf("Warning! Have reached the end of assessCompetency but shouldn't be here!\n");
  return 0;
}

/** Function to verify whether particle settles during time dt. */
int dgParticleTracker2D::isSettled(double t, int m)
{
  //1. Check if particle is competent
  if (t < _t_preComp || t > _t_postComp) return 0;
  double settRate=0.0;
  //2. Calc. settling prob and check if particles settles during dt
  switch (_settleType) {
    case 0:
      return 0;
    case 1:       /* Flat settling rate (coral larvae) */
      if ( _settRate_max > ((randoraw()/2.0)+0.5) ) return 1;
      else return 0;
    case 2:       /* Pyramidal settling rate (fish larvae) */
      //A. Calc. settling rate
      if (t<=_t_settMax) settRate = _settRate_max * (t - _t_preComp)/(_t_settMax - _t_preComp);
      if (t>_t_settMax) settRate = _settRate_max * (_t_postComp - t)/(_t_postComp - _t_settMax);
      //B. Check if particle settles
      if ( settRate > ((randoraw()/2.0)+0.5) ) return 1;
      else return 0;
    case 3:       /* Flat settling rate if competent*/
      if (_ParticleArray->getParticle(m)->getCompetency() != 1) return 0;
      if ( _settRate_max > ((randoraw()/2.0)+0.5) ) return 1;
      else return 0;
  }
  return 0;
}

/** Function to verify whether particle dies during time dt. */
int dgParticleTracker2D::isDead(double t)
{
//NB: mortRate is prob. of particle dying between t and t+dt  
  double mortRatePerDt;
  
  switch (_mortType) {
    case 0:
      return 0;
    case 1:     /* Const. mortality rate */
      mortRatePerDt = _dt * _mortRate_max/(3600.0*24.0);
      if ( ((randoraw()/2)+0.5) < mortRatePerDt ) return 1;
      else return 0;
    case 2:     /* Weibull mortality rate */
//      mortRatePerDt = 1 - _dt*exp( pow((_mortLambda*t),_mortAlpha) );
      mortRatePerDt = (_dt/(24.0*3600.0)) * _mortNu * _mortLambda * pow((_mortLambda*(t/(24.0*3600.0))), (_mortNu-1.0));
      //WARNING TEMP:
      if ( ((randoraw()/2)+0.5) > 1.0 ) printf("ERROR! randowraw() random nb > 1!\n");
      if ( ((randoraw()/2)+0.5) < mortRatePerDt ) return 1;
      else return 0;
  }
  return 0;
}

//Random number generator for settling & mortality
double randoraw()
{
  double randoo = ((static_cast<double>(rand())/(static_cast<double>(RAND_MAX)+1.0)) - 0.5)*2.0;
  return randoo;
}

//Random number generator
void rando(double *randoo, int M)
{
  double sum=0.0, mean=0.0, temp, stdinv;
  
  temp = 1.0/(static_cast<double>(RAND_MAX)+1.0);
  
  for (int m=0; m<M; m++)
  {
    *(randoo+m) = (static_cast<double>(rand())*temp - 0.5)*2.0;
    sum = sum + *(randoo+m);
  }
  mean = sum/static_cast<double>(M);

  temp=0.0;
  for (int m=0; m<M; m++)
  {
    temp = temp + (mean-*(randoo+m))*(mean-*(randoo+m));
  }
  stdinv = temp/(static_cast<double>(M)-1.0);
  stdinv = 1.0/sqrt(stdinv);

  for (int m=0; m<M; m++)
  {
    *(randoo+m) = *(randoo+m) * stdinv;
  }
}

void Inverse(double Matrix[3][3], double Inverse[3][3])
{
  double determinant =    +Matrix[0][0]*(Matrix[1][1]*Matrix[2][2]-Matrix[2][1]*Matrix[1][2])
                                -Matrix[0][1]*(Matrix[1][0]*Matrix[2][2]-Matrix[1][2]*Matrix[2][0])
                                +Matrix[0][2]*(Matrix[1][0]*Matrix[2][1]-Matrix[1][1]*Matrix[2][0]);
  double invdet = 1/determinant;
  Inverse[0][0] =  (Matrix[1][1]*Matrix[2][2]-Matrix[2][1]*Matrix[1][2])*invdet;
  Inverse[1][0] = -(Matrix[0][1]*Matrix[2][2]-Matrix[0][2]*Matrix[2][1])*invdet;
  Inverse[2][0] =  (Matrix[0][1]*Matrix[1][2]-Matrix[0][2]*Matrix[1][1])*invdet;
  Inverse[0][1] = -(Matrix[1][0]*Matrix[2][2]-Matrix[1][2]*Matrix[2][0])*invdet;
  Inverse[1][1] =  (Matrix[0][0]*Matrix[2][2]-Matrix[0][2]*Matrix[2][0])*invdet;
  Inverse[2][1] = -(Matrix[0][0]*Matrix[1][2]-Matrix[1][0]*Matrix[0][2])*invdet;
  Inverse[0][2] =  (Matrix[1][0]*Matrix[2][1]-Matrix[2][0]*Matrix[1][1])*invdet;
  Inverse[1][2] = -(Matrix[0][0]*Matrix[2][1]-Matrix[2][0]*Matrix[0][1])*invdet;
  Inverse[2][2] =  (Matrix[0][0]*Matrix[1][1]-Matrix[1][0]*Matrix[0][1])*invdet;
}

void Transpose(double Matrix[3][3])
{
  int i,j,temp;
  for(i=0;i<3;i++)
  {
    for(j=i+1;j<3;j++)
    {
      temp=Matrix[i][j];
      Matrix[i][j]=Matrix[j][i];
      Matrix[j][i]=temp;
    }
  }
}

void projection(double* v1, double* v2, double* v1_proj_on_v2)
{
  double v1_dot_v2 = v1[0]*v2[0] + v1[1]*v2[1];
  double Magv2_squared = v2[0]*v2[0] + v2[1]*v2[1];
  v1_proj_on_v2[0] = v2[0] * (v1_dot_v2 / Magv2_squared);
  v1_proj_on_v2[1] = v2[1] * (v1_dot_v2 / Magv2_squared);
}

double projectionMag(double* v1, double* v2)
{
  return (v1[0]*v2[0] + v1[1]*v2[1]) / sqrt(v2[0]*v2[0] + v2[1]*v2[1]);
}

void convertVectorCoords(double target_v[3], double original_v[3], double jac[3][3])
{
  //1. Reset duvw
  for (int i=0; i<3; i++) {
    target_v[i]=0;
  }
  //2. Assign it new value, using a) new dxyz (defined @ end of last cycle) & b) this element's jacobian
  for (int i=0; i<3; i++)  {
    for (int j=0; j<3; j++) {
      target_v[j] = target_v[j] + jac[j][i]*original_v[i];
    }
  }
}

SPoint2 intersection(SPoint2 p1, SPoint2 p2, SPoint2 p3, SPoint2 p4, int& flag)
{
  double x1,y1;
  double x2,y2;
  double x3,y3;
  double x4,y4;
  double ua,ub;
  double num_ua;
  double num_ub;
  double denom;
  double e;
  x1 = p1.x();
  y1 = p1.y();
  x2 = p2.x();
  y2 = p2.y();
  x3 = p3.x();
  y3 = p3.y();
  x4 = p4.x();
  y4 = p4.y();
  e = 0.000001;
  denom = (y4-y3)*(x2-x1) - (x4-x3)*(y2-y1);
  if(fabs(denom)<e){
    flag = 0;
    return SPoint2(0.0,0.0);
  }
  num_ua = (x4-x3)*(y1-y3) - (y4-y3)*(x1-x3);
  num_ub = (x2-x1)*(y1-y3) - (y2-y1)*(x1-x3);
  ua = num_ua/denom;
  ub = num_ub/denom;
  if(ua<=1.0 && ua>=0.0 && ub<=1.0 && ub>=0.0){
    flag = 1;
    return SPoint2(x1+ua*(x2-x1),y1+ua*(y2-y1));
  }
  else{
    flag = 0;
    return SPoint2(0.0,0.0);
  }
}

void stereoVectorToCartesian (double stereoCoords[2], double stereoVector[2], double xyzVector[2], double R) {
  double xi = stereoCoords[0];
  double eta = stereoCoords[1];

  double vxi = stereoVector[0];
  double veta = stereoVector[1];

  double ux = (1-(2*xi*xi)/(4*R*R+xi*xi + eta*eta)) * vxi - 2*eta*xi/(4*R*R+xi*xi + eta*eta) * veta;
  double uy = - 2*eta*xi/(4*R*R+xi*xi + eta*eta) * vxi + (1-(2*eta*eta)/(4*R*R+xi*xi + eta*eta)) * veta;
  xyzVector[0] = ux;
  xyzVector[1] = uy;
}

void velocity3D (double stereoCoords[2], double stereoVector[2], double xyzVector[3], double R) {
  double xi = stereoCoords[0];
  double eta = stereoCoords[1];
  
  double vxi = stereoVector[0];
  double veta = stereoVector[1];
  
  double x = 4*R*R*xi/(4*R*R+xi*xi + eta*eta);
  double y = 4*R*R*eta/(4*R*R+xi*xi + eta*eta);
  double z = R *(4*R*R-xi*xi-eta*eta)/(4*R*R+xi*xi + eta*eta);
  double ux = vxi*1/R*(z+y*y/(R+z)) - veta*(x*y)/R/(R+z);
  double uy = -vxi*(x*y)/R/(R+z) + veta*1/R*(z+x*x/(R+z));
  double uz = -vxi*(x/R)- veta*(y/R);
  
  xyzVector[0] = ux;
  xyzVector[1] = uy;
  xyzVector[2] = uz;
}

/** Method to print contents of a connectivity matrix */
void connectivityMatrixPrint::CMPrinter(fullMatrix<double>* FM, const std::string filename) {
  std::ofstream outFile;
  std::ostringstream fname;
  //Define filename
  fname << filename << ".dat";
  //Open file (replace if already exists)
  std::string temp = fname.str();
  outFile.open( temp.c_str(), std::ios::out | std::ios::trunc );
  for (int row=0; row<FM->size1(); row++) {
    for (int col=0; col<FM->size2(); col++) {
      outFile << FM->get(row,col) << " ";
    }
    outFile << std::endl;
  }
  outFile.close();
}

/** Seed additional particles at a given point */
void particleArray::addParticlesAtPoint(dgGroupCollection* _group, int M, double x0, double y0, int status, int source_reef)
{
  //Get nb of existing particles
  int existingNbParticles = _totalSeeded;
  
  //Find group & element IDs
  SPoint3 xyz(x0,y0,0.0);
  GModel *gm = _group->getModel();
  MElement *e = gm->getMeshElementByCoord(xyz, 2);
  int iGroup, iElem;
  _group->find(e, iGroup, iElem);
  
  //Seed particles
  for (int i=0; i<M; i++) {      
    _particle_array.push_back( particle(x0,y0,iGroup,iElem,source_reef,status) );
  }
  
  //Update counters
  _alive += M;
  _totalSeeded += M;
  
  //Print info to screen
  printf("**** SEEDING EVEN OCCURRED. %d PARTICLES SEEDED. ****\n", static_cast<int>( _particle_array.size() )-existingNbParticles );
  printf(" -> Total number of particles seeded so far: %d\n", static_cast<int>(_particle_array.size()));
}

/** Seed additional particles using reef map DC onto an existing particleArray object */
void particleArray::addParticlesWithDofContainer(dgGroupCollection* groups, dgDofContainer* DC, double concentration, int minNbParticlesPerHabitat) {
  /*Plan: loop through every element, evaluate DC in each and seed particles where it is non-zero.
   *         Number of particles to seed on element depends on element's surface area
   *         minNbParticlesPerHabitat gives the minimum number of particles to seed per habitat (given by the integer value
   *         of the dof container in each element)
   *         Particles to be seeded randomly all over element
   */
  srand(time(NULL));
  dgFunctionEvaluator reefsEval(groups, DC->getFunction());
  int particleCounter=0;
  int nbOfElementsSeeded=0, nbOfHabitatElements=0;
  int maxHabitatId=-1;
  std::vector<int> habitatsSeeded;
  
  //Count existing nb particles & set particleCounter to last particle Id in existing array
  int existingNbParticles = _totalSeeded;
  particleCounter = existingNbParticles;  //particleCounter now gives the array index of the first of the new particles to add
  //Loop through all mesh elements:
  int nbGroups = groups->getNbElementGroups();
  printf("Beginning particle seeding loop. Cycling through %d group(s) of elements.\n", nbGroups);
  
  //First cycle through all elements to get total surface area of each habitat
  fullMatrix<double> minId_FM, maxId_FM;
  DC->minmax(minId_FM, maxId_FM);
  int maxId;
  maxId = static_cast<int>(maxId_FM(0,0)+0.1);
  
  double* habitatSurfaceAreas = new double [maxId+1];
  int* nbOfElementsInHabitat = new int [maxId+1];
  int* particlesMissing = new int [maxId+1];
  fullMatrix<double> initCM(maxId+1, maxId+1); //This is used for counting later on
  for (int i=0; i<=maxId; i++) {
    habitatSurfaceAreas[i]=0.0;
    nbOfElementsInHabitat[i]=0;
    particlesMissing[i]=0;
  }
  for (int groupId = 0; groupId < nbGroups; groupId++) {
    const dgGroupOfElements *groupOfElements = groups->getElementGroup(groupId);
    int nbElements = groupOfElements->getNbElements();
    for (int elementId=0; elementId<nbElements; elementId++) {
      //Evaluate DC in this element:
      MElement *el = groupOfElements->getElement(elementId);
      fullMatrix<double> res;
      reefsEval.computeElem(groupId, elementId, 0.25, 0.25, 0.0, res);
      int habitatId = static_cast<int>(res(0,0)+0.5);  //Static_cast rounds down
      //Ignore if we are not on a habitat
      if (habitatId == 0) continue;

      //We are in a habitat. Get surface area of element and record it.
      double elSurface = el->getVolume();
      habitatSurfaceAreas[habitatId] += elSurface;
      nbOfElementsInHabitat[habitatId] += 1;
    }
  }
  //Check if the predicted nb of particles seeded falls below the threshold for any habitat, if so record how many particles are missing
  for (int habitatId=0; habitatId<=maxId; habitatId++) {
    if (nbOfElementsInHabitat[habitatId] == 0) continue;
    int nbParticlesInHabitat = static_cast<int>((concentration/1000000.0)*habitatSurfaceAreas[habitatId] + 0.5);  //static_cast rounds down
    if (nbParticlesInHabitat < minNbParticlesPerHabitat) particlesMissing[habitatId] += minNbParticlesPerHabitat-nbParticlesInHabitat;
  }
  //Now loop over all elements to seed habitats
  for (int groupId = 0; groupId < nbGroups; groupId++) {
    const dgGroupOfElements *groupOfElements = groups->getElementGroup(groupId);
    int nbElements = groupOfElements->getNbElements();
    for (int elementId=0; elementId<nbElements; elementId++) {
      //Evaluate reef map in this element:
      MElement *el = groupOfElements->getElement(elementId);
      fullMatrix<double> res;
      reefsEval.computeElem(groupId, elementId, 0.25, 0.25, 0.0, res);
      int habitatId = static_cast<int>(res(0,0)+0.5);
      
      //Do not seed if we are not on a reef
      if (habitatId == 0) continue;
      
      //We are on reef.
      nbOfHabitatElements++;
      //Record highest habitatId:
      if (habitatId > maxHabitatId) maxHabitatId = habitatId;
      //Seed element in parametric space:
      //First calc. no. of particles to seed in element, and count reef as having been seeded
      double elSurface = el->getVolume();
      int nbParticlesInElem = static_cast<int>((concentration/1000000.0)*elSurface + 0.5);  //static_cast rounds down
      if (nbParticlesInElem != 0) {
        //If reef does not have enough particles for minNbParticlesPerHabitat, add however many needed on this element
        if (particlesMissing[habitatId] > 0) {
          nbParticlesInElem += static_cast<int>( static_cast<double>(particlesMissing[habitatId])/static_cast<double>(nbOfElementsInHabitat[habitatId]) + 0.5);
        }
        //Count element as seeded and, if not already done, reef as seeded
        nbOfElementsSeeded++;
        if ( std::find(habitatsSeeded.begin(), habitatsSeeded.end(), habitatId) == habitatsSeeded.end() ) {
          // reef @ habitatId has not already been seeded. Count it.
          habitatsSeeded.push_back(habitatId);
        }
      }
      
      //Now seed particles
      for (int j = 0; j < nbParticlesInElem; j++) {
//        if (j==0) printf("Number of particles in reef element %d (habitatId %d) is %d\n",nbOfHabitatElements, habitatId, nbParticlesInElem );
        double u=rand()*1./RAND_MAX;
        double v=rand()*1./RAND_MAX;
        if(u+v>1){
          u=1-u;
          v=1-v;
        }
        SPoint3 xyz;
        el->primaryPnt(u, v, 0.0, xyz);
        _particle_array.push_back( particle(xyz.x(), xyz.y(), groupId, elementId, habitatId, 0) );

        //Register particle on 'initial connectivity matrix' (for counting later on)
        //NB: source & destination reefs should be equal
        double prevCon = initCM.get(_particle_array[particleCounter].getSource()-1, habitatId-1);
        initCM.set(_particle_array[particleCounter].getSource()-1, habitatId-1, prevCon+1.0);
        
        particleCounter++;
        _totalSeeded++;
      }
    }
  }
  //Get number of particles seeded on least & most populous reefs
  int minNbParticlesOnReef=1000000, maxNbParticlesOnReef=0;
  for (int i=0; i<initCM.size1(); i++) {
    for (int j=0; j<initCM.size1(); j++) {
      int data = static_cast<int>( initCM.get(i,j) );
      if (data > 0 && data < minNbParticlesOnReef) minNbParticlesOnReef = data;
      if (data > maxNbParticlesOnReef) maxNbParticlesOnReef = data;
    }
  }
  
  printf("**** SEEDING EVENT OCCURRED. %d PARTICLES SEEDED. ****\n", static_cast<int>( _particle_array.size() )-existingNbParticles );
  printf(" -> We have %d 'habitat' elements, and have seeded %d of these.\n", nbOfHabitatElements, nbOfElementsSeeded);
  printf(" -> We have seeded %d habitats, or %.2f of total.\n", static_cast<int>(habitatsSeeded.size()), 100.0*static_cast<double>( habitatsSeeded.size() )/static_cast<double>( maxHabitatId ) );
  printf(" -> Highest habitat Id seen on mesh is %d\n",maxHabitatId);
  printf(" -> Most populous habitat has %d particles, least populous has %d particles.\n",  maxNbParticlesOnReef, minNbParticlesOnReef);
  printf(" -> TOTAL NUMBER OF PARTICLES SEEDED SO FAR: %d\n", static_cast<int>(_particle_array.size()));
}

/** Seed additional particles using reef map DC onto an existing particleArray object */
void particleArray::addParticlesWithReefMap(dgGroupCollection* groups, dgDofContainer* reefsDC, fullMatrix<double>* initCM, fullMatrix<double>* reefsToSeed, double concentration, int minNbParticlesPerReef, int seedOverWhichReefs, int nbOfShallowReefs) {
  /*Plan: loop through every element, evaluate reefsDC in each and seed particles where it is non-zero.
   *         Number of particles to seed depends on element's surface area
   *         minPartsPerReef gives the minimum number of particles to seed per reef
   *         Particles to be seeded randomly in element
   *         Seed only over the group of reefs specified in seedOverWhichReefs (0: shallow, 1: deep, 2: all)
   *            -> NB: reefs with reefId > nbOfShallowReefs are deep reefs
   */
  srand(time(NULL));
  dgFunctionEvaluator reefsEval(groups, reefsDC->getFunction());
  int particleCounter=0;
  int nbOfElementsSeeded=0;
  int nbOfReefElements=0, nbOfDeepReefElements=0, nbOfShallowReefElements=0;
  int nbOfShallowReefsSeeded=0, nbOfDeepReefsSeeded=0;
  int nbOfShallowParticles=0, nbOfDeepParticles=0;
  int maxReefId=-1;
  std::vector<int> reefsSeeded;
  
  //Count existing nb particles & set particleCounter to last particle Id in existing array
  int existingNbParticles = _totalSeeded;
  particleCounter = existingNbParticles;  //particleCounter now gives the array index of the first of the new particles to add
  //Loop through all mesh elements:
  int nbGroups = groups->getNbElementGroups();
  printf("Beginning particle seeding loop. Cycling through %d group(s) of elements.\n", nbGroups);
  
  //First cycle through all elements to get total surface area of each reef
  fullMatrix<double> minId_FM, maxId_FM;
  reefsDC->minmax(minId_FM, maxId_FM);
  int maxId;
  maxId = static_cast<int>(maxId_FM(0,0)+0.1);
  
  double* reefSurfaceAreas = new double [maxId+1];
  int* nbOfElementsInReef = new int [maxId+1];
  int* particlesMissing = new int [maxId+1];
  //Initialise arrays to zero
  for (int i=0; i<=maxId; i++) {
    reefSurfaceAreas[i]=0.0;
    nbOfElementsInReef[i]=0;
    particlesMissing[i]=0;
  }
  for (int groupId = 0; groupId < nbGroups; groupId++) {
    const dgGroupOfElements *groupOfElements = groups->getElementGroup(groupId);
    int nbElements = groupOfElements->getNbElements();
    for (int elementId=0; elementId<nbElements; elementId++) {
      //Evaluate reef map in this element:
      MElement *el = groupOfElements->getElement(elementId);
      fullMatrix<double> res;
      reefsEval.computeElem(groupId, elementId, 0.25, 0.25, 0.0, res);
      int reefId = static_cast<int>(res(0,0)+0.5);
      
      //Ignore if we are not on a reef
      if (reefId == 0) continue;
      // If reefsToSeed is non-null, only consider reefIds listed in it
      if (reefsToSeed->get(0,0) != 0) {
        int seed = 0;
        for (int num = 0; num < reefsToSeed->size1(); num++) {
          if (static_cast<int>( reefsToSeed->get(num,num)+0.1 ) == reefId) seed=1;
        }
        if (seed == 0) continue;
      }
      //We are on reef. Get surface area of element and record it.
      double elSurface = el->getVolume();
      reefSurfaceAreas[reefId] += elSurface;
      nbOfElementsInReef[reefId] += 1;
    }
  }
  //Check if the predicted nb of particles seeded falls below the threshold for any reef, if so record how many particles are missing
  for (int reefId=0; reefId<maxId; reefId++) {
    if (nbOfElementsInReef[reefId] == 0) continue;
    int nbParticlesInReef = static_cast<int>((concentration/1000000.0)*reefSurfaceAreas[reefId] + 0.5);  //static_cast rounds down
    if (nbParticlesInReef < minNbParticlesPerReef) particlesMissing[reefId] += minNbParticlesPerReef-nbParticlesInReef;
  }
  //Now loop over all elements to seed reefs
  for (int groupId = 0; groupId < nbGroups; groupId++) {
    const dgGroupOfElements *groupOfElements = groups->getElementGroup(groupId);
    int nbElements = groupOfElements->getNbElements();
    for (int elementId=0; elementId<nbElements; elementId++) {
      //Evaluate reef map in this element:
      MElement *el = groupOfElements->getElement(elementId);
      fullMatrix<double> res;
      reefsEval.computeElem(groupId, elementId, 0.25, 0.25, 0.0, res);
      int reefId = static_cast<int>(res(0,0)+0.5);
      
      //Do not seed if we are not on a reef
      if (reefId == 0) continue;
      
      // If reefsToSeed is non-null, only seed over reefIds listed in it
      if (reefsToSeed->get(0,0) != 0) {
        int seed = 0;
        for (int num = 0; num < reefsToSeed->size1(); num++) {
          if (static_cast<int>( reefsToSeed->get(num,num)+0.1 ) == reefId) seed=1;
        }
        if (seed == 0) continue;
      }
      
      //We are on reef.
      nbOfReefElements++;
      if (reefId > nbOfShallowReefs) nbOfDeepReefElements++;
      else nbOfShallowReefElements++;
      
      //Record highest reefId:
      if (reefId > maxReefId) maxReefId = reefId;
          
      //Check whether to seed this element
      if (reefId > nbOfShallowReefs && seedOverWhichReefs == 0) continue;
      if (reefId <= nbOfShallowReefs && seedOverWhichReefs == 1) continue;
//       printf("830: nbOfReefElements: %d, reefId: %d\n",nbOfReefElements, reefId);

      //Seed element in parametric space:
      //First calc. no. of particles to seed in element, and count reef as having been seeded
      double elSurface = el->getVolume();
      int nbParticlesInElem = static_cast<int>((concentration/1000000.0)*elSurface + 0.5);  //static_cast rounds down
      if (nbParticlesInElem != 0) {
        //If reef does not have enough particles for minNbParticlesPerReef, add however many needed on this element
        if (particlesMissing[reefId] > 0) {
          nbParticlesInElem += static_cast<int>( static_cast<double>(particlesMissing[reefId])/static_cast<double>(nbOfElementsInReef[reefId]) + 0.5);
        }
        //Count element as seeded and, if not already done, reef as seeded
        nbOfElementsSeeded++;
        if ( std::find(reefsSeeded.begin(), reefsSeeded.end(), reefId) == reefsSeeded.end() ) {
          // reef @ reefId has not already been seeded. Count it.
          reefsSeeded.push_back(reefId);
          if (reefId <= nbOfShallowReefs) nbOfShallowReefsSeeded++;
          else nbOfDeepReefsSeeded++;
        }
      }
      
      //Now seed particles
      for (int j = 0; j < nbParticlesInElem; j++) {
//        if (j==0) printf("Number of particles in reef element %d (reefId %d) is %d\n",nbOfReefElements, reefId, nbParticlesInElem );
        double u=rand()*1./RAND_MAX;
        double v=rand()*1./RAND_MAX;
        if(u+v>1){
          u=1-u;
          v=1-v;
        }
        SPoint3 xyz;
        el->primaryPnt(u, v, 0.0, xyz);
        _particle_array.push_back( particle(xyz.x(), xyz.y(), groupId, elementId, reefId, 0) );
        if (reefId > nbOfShallowReefs) nbOfDeepParticles++;
        else nbOfShallowParticles++;

        //Register particle on 'initial connectivity matrix':
        //NB: source & destination reefs should be equal
        double prevCon = initCM->get(_particle_array[particleCounter].getSource()-1, reefId-1);
        initCM->set(_particle_array[particleCounter].getSource()-1, reefId-1, prevCon+1.0);
        
        particleCounter++;
        _totalSeeded++;
      }
    }
  }
  //Get number of particles seeded on least & most populous reefs
  int minNbParticlesOnReef=1000000, maxNbParticlesOnReef=0;
  for (int i=0; i<initCM->size1(); i++) {
    for (int j=0; j<initCM->size1(); j++) {
      int data = static_cast<int>( initCM->get(i,j) );
      if (data > 0 && data < minNbParticlesOnReef) minNbParticlesOnReef = data;
      if (data > maxNbParticlesOnReef) maxNbParticlesOnReef = data;
    }
  }
  
  printf("**** SEEDING EVEN OCCURRED. %d PARTICLES SEEDED. ****\n -> ... of which %d (%.2f) over shallow reefs and %d (%.2f) over deep reefs.\n", static_cast<int>( _particle_array.size() )-existingNbParticles, nbOfShallowParticles, 100.0*static_cast<double>(nbOfShallowParticles)/static_cast<double>( _particle_array.size() -existingNbParticles), nbOfDeepParticles, 100.0*static_cast<double>(nbOfDeepParticles)/static_cast<double>( _particle_array.size() -existingNbParticles) );
  printf(" -> We have %d 'reef' elements, of which %d shallow and %d deep.\n", nbOfReefElements, nbOfShallowReefElements, nbOfDeepReefElements);
  printf(" -> We have seeded %d 'reef' elements in total.\n", nbOfElementsSeeded);
  printf(" -> We have seeded %d reefs, or %.2f of total (%.2f of shallow reefs, %.2f of deep reefs).\n", static_cast<int>(reefsSeeded.size()), 100.0*static_cast<double>( reefsSeeded.size() )/static_cast<double>( maxReefId ), 100.0*static_cast<double>(nbOfShallowReefsSeeded)/static_cast<double>(nbOfShallowReefs), 100.0*static_cast<double>(nbOfDeepReefsSeeded)/static_cast<double>(maxReefId-nbOfShallowReefs) );
  printf(" -> Highest reef Id seen on mesh is %d\n",maxReefId);
  printf(" -> Most populous reef has %d particles, least populous has %d particles.\n",  maxNbParticlesOnReef, minNbParticlesOnReef);
  printf(" -> TOTAL NUMBER OF PARTICLES SEEDED SO FAR: %d\n", static_cast<int>(_particle_array.size()));
}

void particleArray::printPositions(const std::string filename, const std::string filetype, int status)   {
  std::ofstream outFile;
  std::ostringstream fname, res;
  
  //Define filename
  fname << "-";//<< std::setw( 5 ) << std::setfill( '0' ) << m;    //<--Creates seperate file for every particle (if want to track particles)
  res << filename << fname.str() << "." << filetype;
  
  //Open file (replace if already exists)
  std::string temp = res.str();
  outFile.open( temp.c_str(), std::ios::out | std::ios::trunc );
  //outFile.open( temp.c_str(), std::ios::out | std::ios::app );
  
  if (filetype == "dat") {
    //Write to file as list
    for (size_t m=0; m<_particle_array.size(); m++) {
      //Only register if particle is of state 'status'
      if (_particle_array[m].getState() == status) {
        outFile << _particle_array[m].x() << " " << _particle_array[m].y() << " " << _particle_array[m].getSource() << std::endl;
      }
    }
    outFile.close();
  }
  else if (filetype == "pos") {
    //Write to file in .pos format
    outFile << "View \"particlePositions\" {"<<std::endl;
    for (size_t m=0; m<_particle_array.size(); m++) {
      //Only register if particle is of state 'status'
      if (_particle_array[m].getState() == status) {
        outFile << "SP (" << _particle_array[m].x() <<","<< _particle_array[m].y() <<",0) {"<<_particle_array[m].getSource()<<"};" << std::endl;
      }
    }
    outFile << "};"<<std::endl;
    outFile.close();
  }
  else {
    std::cout << "ERROR: printPositions filetype argument is invalid." << std::endl;
    outFile.close();
  }
}
  
void particleArray::printPositionsFollow(const std::string filename, const std::string filetype, int n, int N, double dt)   {
  for (size_t m=0; m<_particle_array.size(); m++) {
    std::ofstream outFile;
    std::ostringstream fname, res;
    
    //Define filename
    fname << "-" << std::setw( 5 ) << std::setfill( '0' ) << m;    //<--Creates seperate file for every particle
    res << filename << fname.str() << "." << filetype;
    std::string temp = res.str();

    if (n==0) {
      outFile.open( temp.c_str(), std::ios::out | std::ios::trunc );
      if (filetype == "pos") outFile << "View \"particlePositions\" {"<<std::endl;
    }
    else outFile.open( temp.c_str(), std::ios::out | std::ios::app );
    
    if (filetype == "dat") {
      //Write to file as list
      outFile << _particle_array[m].x() << " " << _particle_array[m].y() << std::endl;
      outFile.close();
    }
    else if (filetype == "pos") {
      //Write to file in .pos format
      outFile << "SP (" << _particle_array[m].x() <<","<< _particle_array[m].y() <<",0) {" << static_cast<double>(n)*dt/3600.0 << "};" << std::endl;   
      if (n==N) outFile << "};"<<std::endl;
      outFile.close();
    }
    else {
      std::cout << "ERROR: printPositions filetype argument is invalid." << std::endl;
      outFile.close();
    }
    
  }
}
  
void particleArray::printPositionsFollowSingle(const std::string filename, const std::string filetype, int n, int N, double dt, int particleId) {
  std::ofstream outFile;
  std::ostringstream fname, res;
  
  //Define filename
  fname << "-" << std::setw( 5 ) << std::setfill( '0' ) << particleId;    //<--Creates file labelled by particleId
  res << filename << fname.str() << "." << filetype;
  std::string temp = res.str();
  
  if (n==0) {
    outFile.open( temp.c_str(), std::ios::out | std::ios::trunc );
    if (filetype == "pos") { outFile << "View \"particlePositions\" {"<<std::endl; }
  }
  else { outFile.open( temp.c_str(), std::ios::out | std::ios::app ); }
  
  if (filetype == "dat") {
    //Write to file as list
    outFile << _particle_array[particleId].x() << " " << _particle_array[particleId].y() << std::endl;
    outFile.close();
  }
  else if (filetype == "pos") {
    //Write to file in .pos format
    outFile << "SP (" << _particle_array[particleId].x() <<","<< _particle_array[particleId].y() <<",0) {" << static_cast<double>(n)*dt/3600.0 << "};" << std::endl;   
    if (n==N) { outFile << "};"<<std::endl; }
    outFile.close();
  }
  else {
    std::cout << "ERROR: printPositions filetype argument is invalid." << std::endl;
    outFile.close();
  }
}
  
// Function to convert lat/lon (degrees) to Universal Transverse Mercator
// void latLonDegreesToUTM(fullMatrix<double>* UTM, fullMatrix<double>* lonLatDegrees) {
//   projPJ pj_utm, pj_latlon;
//   double x, y;
//   double pi = 4*atan(1.0);
//   x = lonLatDegrees->get(0,0) * pi / 180.0;
//   y = lonLatDegrees->get(1,1) * pi / 180.0;
// 
// //   printf("lat/lon (degrees): %.2f\t%.2f\n", x*180.0/pi, y*180.0/pi);
// //   if ( !(pj_utm = pj_init_plus("+proj=merc +ellps=clrk66 +lat_ts=33")) ) {
//   if ( !(pj_utm = pj_init_plus("+proj=utm +ellps=clrk66 +zone=24")) ) {
//     printf("Error init UTM\n");
//     exit(1); }
//   if ( !(pj_latlon = pj_init_plus("+proj=latlong +ellps=clrk66")) )  {
//     printf("Error init latLon\n");
//     exit(1); }
// //   printf("here!\n");
//   
//   int er=0;
//   if ( (er = pj_transform( pj_latlon, pj_utm, 1, 1, &x, &y, NULL )) != 0) {
//     printf ("Transform failed: %s\n", pj_strerrno (er));
//     exit(1);
//   }
// //   printf("UTM x/y: %.2f\t%.2f\n", x, y);
//   
//   UTM->set(0,0, x);
//   UTM->set(1,1, y);
//   UTM->set(2,2, 0.0);
// }
