%module particle

%{
  #undef HAVE_DLOPEN
  #include "dgParticleTracker2D.h"
  #include "dgConservationLawPorous.h"
  #include "particle.h"
%}
%import(module="dgpy.dgCommon") "dgCommon.i"
%import (module="dgpy.conservationLaw")"dgConservationLaw.h"
%import (module="dgpy.conservationLaw")"dgConservationLawFunction.h"

%include "particle.h"
%include "dgConservationLawPorous.h"
%include "dgParticleTracker2D.h"
