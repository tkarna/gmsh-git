#ifndef _PATICLE_H_
#define _PATICLE_H_

#include <vector>
#include <map>
#include <string>

class GModel;
class MElement;
class MVertex;
class MElementOctree;

class particleMesh;

class dgGroupCollection;
class dgDofContainer;
class function;

template<class t>
class fullMatrix;

class particleList {
  class particle {
    public:
    const MElement *element;
    std::vector<double> f;
    double uvw[3];
  };
 private:
  std::vector<particle> _particles;
  const particleMesh &_mesh;
 public:
  particleList(const particleMesh &mesh, const fullMatrix<double> &coord);
  ~particleList();
  fullMatrix<double> mesh2particle(const fullMatrix<double> &meshVector);
  fullMatrix<double> particle2mesh(const fullMatrix<double> &particleVector);
  fullMatrix<double> function2particle(const dgGroupCollection &groups, const function *f) const;
};

class particleMesh {
  std::vector<MElement*> _elements;
  MElementOctree *_octree;
  std::map<int, int> _vertexIndex;
  fullMatrix<double> *_vertexVolume;
 public:
  particleMesh(const GModel &model, const std::vector<std::string> tags);
  ~particleMesh();
  const MElementOctree &octree() const {return *_octree;}
  int nVertex() const {return _vertexIndex.size();}
  int vertexIndex(const MVertex *v) const;
  const fullMatrix<double> vertexVolume() const;
  void savePos(const std::string &filename, const fullMatrix<double> &data) const;
};

void particleMesh2DGDof(const particleMesh &mesh, const fullMatrix<double> &val, dgDofContainer &dof);
#endif
