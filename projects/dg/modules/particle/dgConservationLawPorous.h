#ifndef _DG_CONSERVATION_POROUS_
#define _DG_CONSERVATION_POROUS_
#include "dgConservationLawFunction.h"
#include "functionGeneric.h"


const int _P_ = 2;
const int _U_ = 0;
const int _V_ = 1;
const int _W_ = 3;

class dgConservationLawPorous : public dgConservationLawFunction {
  class gradPsiTerm;
  class psiTerm;
  class boundarySlipWall;
  class boundaryVelocity;
  class boundaryPressure;
  class boundaryTerm;
  class velocityVector;
  const function *_rhoF, *_muF, *_invKF, *_porosityF, *_gradSVF, *_velocitySol;

  public:
  void setup();

  inline const function* getDensity() {checkSetup(); return _rhoF;};
  inline const function* getViscosity() {checkSetup(); return _muF;};
  inline const function* getVelocity(){ checkSetup(); return _velocitySol; };
  inline const function* getPressure() { 
    checkSetup();    
    return functionExtractCompNew(function::getSolution(),_P_); 
  }; 
 dgConservationLawPorous(const int dim,
			 const function* rhoF, 
			 const function *muF, 
			 const function *oneOverK, 
			 const function *porosity, 
			 const function *gradSV);
  ~dgConservationLawPorous();
  /**slip wall boundary */
  dgBoundaryCondition *newBoundarySlipWall(int);
  /**prescribe velocity field */
  dgBoundaryCondition *newBoundaryVelocity(const function *vel, int);
  /**prescribe pressure field */
  dgBoundaryCondition *newBoundaryPressure(const function *pres, int);
};


#endif
