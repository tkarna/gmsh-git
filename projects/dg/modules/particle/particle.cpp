#include "particle.h"
#include "GModel.h"
#include "MElement.h"
#include "MElementOctree.h"
#include "MVertex.h"
#include "functionSpace.h"
#include "GaussIntegration.h"

static fullMatrix<double> getMassMatrix(const MElement &e)
{
  //todo f and df could be pre-computed based on the element type...
  const nodalBasis &fs = *e.getFunctionSpace();
  int integrationOrder = fs.order * 2 + 1;
  int N = fs.points.size1();
  fullMatrix<double> xi, mass(N, N);
  fullVector<double> weight;
  gaussIntegration::get(fs.parentType, integrationOrder, xi, weight);
  std::vector<double> f(N);
  double df[256][3];
  for (int iQP = 0; iQP < xi.size1(); ++iQP) {
    fs.f(xi(iQP, 0), xi(iQP, 1), xi(iQP, 2), &f[0]);
    fs.df(xi(iQP, 0), xi(iQP, 1), xi(iQP, 2), df);
    double j = e.getJacobian(xi(iQP, 0), xi(iQP, 1), xi(iQP, 2), df);
    for (int iN = 0; iN < N; ++iN) {
      for (int jN = 0; jN < N; ++jN) {
        mass(iN, jN) += f[iN] * f[jN] * j * weight(iQP);
      }
    }
  }
  return mass;
}

particleMesh::particleMesh(const GModel &model, const std::vector<std::string> tags)
{
  std::set<std::string> tagSet(tags.begin(), tags.end());
  std::vector<GEntity*> entities;
  model.getEntities(entities);
  for(size_t i = 0; i < entities.size(); ++i) {
    GEntity &entity = *entities[i];
    for (size_t itag = 0; itag < entity.physicals.size(); ++itag) {
      const std::string pname = model.getPhysicalName(entity.dim(), entity.physicals[itag]);
      if (! tagSet.count(pname))
        continue;
      _elements.reserve(_elements.size() + entity.getNumMeshElements());
      for (size_t iEl = 0; iEl < entity.getNumMeshElements(); ++iEl) {
        _elements.push_back(entity.getMeshElement(iEl));
      }
      break;
    }
  }
  for (size_t iEl = 0; iEl < _elements.size(); ++iEl) {
    const MElement &el = *_elements[iEl];
    for (int i = 0; i < el.getNumVertices(); ++i) {
      int v = el.getVertex(i)->getNum();
      if (_vertexIndex.find(v) == _vertexIndex.end()){
        int newIdx = _vertexIndex.size();
        _vertexIndex[v] = newIdx;
      }
    }
  }
  _octree = new MElementOctree(_elements);
  _vertexVolume = new fullMatrix<double>(nVertex(), 1.);
  for (size_t iEl = 0; iEl < _elements.size(); ++iEl) {
    const MElement &el = *_elements[iEl];
    fullMatrix<double> mass = getMassMatrix(el);
    for (int i = 0; i < mass.size1(); ++i) {
      int idx = vertexIndex(el.getVertex(i));
      for (int j = 0; j < mass.size1(); ++j) {
        (*_vertexVolume)(idx, 0) += mass(i, j);
      }
    }
  }
}

void particleMesh::savePos(const std::string &filename, const fullMatrix<double> &data) const
{
  if (data.size2() < 1 || data.size1() != (int)_vertexIndex.size()) {
    Msg::Error("invalid data size in particleMesh::savePos : %i %i", filename.c_str(), data.size1(), data.size2());
    return;
  }
  FILE *output = fopen(filename.c_str(), "w");
  if (!output) {
    Msg::Error("cannot open output file %s", filename.c_str());
    return;
  }
  fprintf(output, "View \"\"{\n");
  for (size_t iEl = 0; iEl < _elements.size(); ++iEl) {
    MElement &el = *_elements[iEl];
    fprintf(output, data.size2() < 3 ? "ST (" : "VT (");
    for (int iV = 0; iV < el.getNumVertices(); ++iV) {
      MVertex &vertex = *el.getVertex(iV);
      if (iV != 0)
        fprintf(output, ",");
      fprintf(output, "%16g, %16g, %16g", vertex.x(), vertex.y(), vertex.z());
    }
    fprintf(output, ") {");
    for (int iV = 0; iV < el.getNumVertices(); ++iV) {
      int idx = vertexIndex(el.getVertex(iV));
      if (iV != 0)
        fprintf(output, ",");
      fprintf(output, "%16g", data(idx, 0));
      if (data.size2() >= 3)
        fprintf(output, ", %16g, %16g", data(idx, 1), data(idx, 2));
    }
    fprintf(output, "};\n");
  }
  fprintf(output, "};\n");
  fclose(output);
}

int particleMesh::vertexIndex(const MVertex *v) const
{
  int iv = v->getNum();
  std::map<int,int>::const_iterator it = _vertexIndex.find(iv);
  if (it == _vertexIndex.end())
    return -1;
  else
    return it->second;
}

particleMesh::~particleMesh()
{
  delete _octree;
  delete _vertexVolume;
}

particleList::particleList(const particleMesh &mesh, const fullMatrix<double> &coord):
  _mesh(mesh)
{
  _particles.resize(coord.size1());
  for (int i = 0; i < coord.size1(); ++i) {
    particle &p = _particles[i];
    double xyz[3] = {coord(i, 0), coord(i, 1), coord.size2() > 2 ? coord(i, 2) : 0.};
    p.element = _mesh.octree().find(xyz[0], xyz[1], xyz[2]);
    if (p.element){
      p.element->xyz2uvw(xyz, p.uvw);
      const nodalBasis &fs = *p.element->getFunctionSpace();
      p.f.resize(fs.points.size1());
      fs.f(p.uvw[0], p.uvw[1], p.uvw[2], &p.f[0]);
    }
  }
}

fullMatrix<double> particleList::particle2mesh(const fullMatrix<double> &particleData)
{
  fullMatrix<double> meshData(_mesh.nVertex(), particleData.size2());
  if ((size_t)particleData.size1() != _particles.size())
    Msg::Fatal("particle2mesh : invalid particleData length %lu != %lu", particleData.size1(), _particles.size());
  //meshData.setAll(0.);
  for (int iParticle = 0; iParticle < particleData.size1(); ++iParticle) {
    particle &p = _particles[iParticle];
    if (!p.element)
      continue;
    for (size_t iVertex = 0; iVertex < p.f.size(); ++iVertex) {
      int idx = _mesh.vertexIndex(p.element->getVertex(iVertex));
      for (int j = 0; j < meshData.size2(); ++j)
        meshData(idx, j) += particleData(iParticle, j) * p.f[iVertex];
    }
  }
  return meshData;
}

fullMatrix<double> particleList::mesh2particle(const fullMatrix<double> &meshVector)
{
  fullMatrix<double> particleVector(_particles.size(), meshVector.size2());
  if ((int)meshVector.size1() != _mesh.nVertex())
    Msg::Fatal("mesh2particle : invalid meshVector length %z != %i", meshVector.size1(), _mesh.nVertex());
  for (int iParticle = 0; iParticle < particleVector.size1(); ++iParticle) {
    particle &p = _particles[iParticle];
    if (!p.element)
      continue;
    for (size_t iVertex = 0; iVertex < p.f.size(); ++iVertex) {
      int idx = _mesh.vertexIndex(p.element->getVertex(iVertex));
      for (int j = 0; j < meshVector.size2(); ++j)
        particleVector(iParticle, j) += meshVector(idx, j) * p.f[iVertex];
    }
  }
  return particleVector;
}

particleList::~particleList()
{
}

const fullMatrix<double> particleMesh::vertexVolume() const {return *_vertexVolume;}

#include "dgGroupOfElements.h"
#include "dgDofContainer.h"
#include "function.h"
void particleMesh2DGDof(const particleMesh &mesh, const fullMatrix<double> &val, dgDofContainer &dof)
{
  const dgGroupCollection &groups = *dof.getGroups();
  if (val.size1() != mesh.nVertex())
    Msg::Fatal("particleMesh2DGDof : invalid matrix size %i != %i", val.size1(), mesh.nVertex());
  if (val.size2() != dof.getNbFields())
    Msg::Fatal("particleMesh2DGDof : invalid matrix size %i != %i", val.size2(), dof.getNbFields());
  for (int iGroup = 0; iGroup < groups.getNbElementGroups(); ++iGroup) {
    const dgGroupOfElements &group = *groups.getElementGroup(iGroup);
    dgFullMatrix<double> &groupproxy = dof.getGroupProxy(iGroup);
    for (size_t iElement = 0; iElement < group.getNbElements(); ++iElement) {
      const MElement &e = *group.getElement(iElement);
      for (int iVertex = 0; iVertex < e.getNumVertices(); ++iVertex) {
        int idx = mesh.vertexIndex(e.getVertex(iVertex));
        if (idx == -1)
          continue;
        for (int comp = 0; comp < val.size2(); ++comp)
          groupproxy(iVertex, iElement * dof.getNbFields() + comp) = val(idx, comp);
      }
    }
  }
}


fullMatrix<double> particleList::function2particle(const dgGroupCollection &groups, const function *f) const
{
  if (_particles.size() == 0)
    return fullMatrix<double>();
  dataCacheMap cache(dataCacheMap::MULTIPOINT_MODE, &groups);
  dataCacheDouble &vf = *cache.get(f);
  fullMatrix<double> result(_particles.size(), f->getNbCol());
  std::vector<int> elid(_particles.size());
  fullMatrix<double> xi(_particles.size(), 3);
  int groupId = -1;
  for (size_t i = 0; i < _particles.size(); ++i) {
    int gId;
    const double *uvw = _particles[i].uvw;
    xi(i, 0) = uvw[0];
    xi(i, 1) = uvw[1];
    xi(i, 2) = uvw[2];
    groups.find(_particles[i].element, gId, elid[i]);
    if (i != 0 && gId != groupId)
      Msg::Fatal("function2particle implementation is incomplete and requires that all particles belong to the same elementGroup");
    groupId = gId;
  }
  cache.setMultiPoint(*groups.getElementGroup(groupId), elid, xi);
  return vf.get();
}
