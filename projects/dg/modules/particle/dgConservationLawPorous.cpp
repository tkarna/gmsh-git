#include "dgConservationLawPorous.h"
#include "dgConservationLawFunction.h"
#include "dgGroupOfElements.h"
#include "function.h"
#include "float.h"
#include "nodalBasis.h"
#include "dgMeshJacobian.h"
#include "Numeric.h"

#define SQR(x) (x)*(x)

/*
  Flow in porous media

  \nabla p = - \mu_{eff} \nabla^2 u - \mu / K u - \rho C_E / \sqrt{K} |u| u 

  K = d_p^2 \phi^3 / (A (1-\phi) ^2 )

  unknowns : { p , u , v,  w }

*/

class dgConservationLawPorous::velocityVector : public function{
  fullMatrix<double>  solution;
public :
  velocityVector() : function(3)  {
    setArgument (solution, function::getSolution());
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP =val.size1();
    for (size_t i=0; i<nQP; i++) {
      val (i,0) = solution(i,_U_);
      val (i,1) = solution(i,_V_);
      //      val (i,2) = solution(i,_W_);
      val (i,2) = 0.0;
    }
  }
};


class dgConservationLawPorous::gradPsiTerm: public function {
  int _dim;
  fullMatrix<double>  sol, solGrad;
  fullMatrix<double> rho, mu, porosity, invK;
  public:
  gradPsiTerm (int dim, const function* rhoF, const function* muF, const function *porosityF, const function *invKF): 
    function((dim+1)*3),_dim(dim) {
    setArgument(sol,function::getSolution());
    setArgument(solGrad, function::getSolutionGradient());
    setArgument(rho, rhoF);
    setArgument(mu, muF);
    setArgument(porosity,porosityF);
    setArgument(invK, invKF);
  }

  void call(dataCacheMap *m, fullMatrix<double> &val) {
    //const dgGroupOfElements &eGroup = *m->getGroupOfElements();
    size_t nQP = val.size1();
    val.setAll(0.0);
    for(size_t i=0; i< nQP; i++) {
      //int iElement = m->elementId(i/m->nPointByElement());
      //double h = sqrt(4*m->getJacobians().elementVolume(eGroup.elementVectorId())(iElement)/3.14);

      double u = sol(i,_U_);
      double v = sol(i,_V_);
      double w = (_dim == 3) ? sol(i,_W_) : 0.0;
      double p = sol(i,_P_);

      //double _rho = rho(i,0);
      double _mu = mu(i,0);
      //      double _phi = porosity(i,0);
 
      double dpdx = solGrad(i,3*_P_ + 0);
      double dpdy = solGrad(i,3*_P_ + 1);

      double dudx = solGrad(i,3*_U_ + 0);
      double dudy = solGrad(i,3*_U_ + 1);
      double dvdx = solGrad(i,3*_V_ + 0);
      double dvdy = solGrad(i,3*_V_ + 1);

      double dudz = (_dim == 3) ? solGrad(i,3*_U_ + 2) : 0;
      double dvdz = (_dim == 3) ? solGrad(i,3*_V_ + 2) : 0;
      double dwdx = (_dim == 3) ? solGrad(i,3*_W_ + 0) : 0;
      double dwdy = (_dim == 3) ? solGrad(i,3*_W_ + 1) : 0;
      double dwdz = (_dim == 3) ? solGrad(i,3*_W_ + 2) : 0;

      //      double _nu = _mu/_rho;

      const int _nbf = _dim + 1;
      const double invk = invK(i,0);
      //      const double TAU = .010*h*h/_mu;
      const double TAU = 4.e-3;

      //      printf("correction =  %12.5E %12.5E %12.5E\n",TAU*_mu*invk, u, TAU*(dpdx + _mu*invk * u));

      //flux_x (multiply dphi/dx)
      val(i,_U_) =  (p - _mu*dudx) ; 
      val(i,_V_) =  (  - _mu*dvdx) ; 
      val(i,_P_) =  u - TAU * (dpdx + _mu*invk * u);
      
      // flux_y (multiply dphi/dy)
      val(i,_U_+_nbf) = (   - _mu*dudy); 
      val(i,_V_+_nbf) = (p  - _mu*dvdy);
      val(i,_P_+_nbf) =  v  - TAU * (dpdy + _mu*invk * v);
	
      if (_dim == 3){
	val(i,_U_+2*_nbf) = (   - _mu*dudz); 
	val(i,_V_+2*_nbf) = (   - _mu*dvdz); 
	val(i,_W_)        = (   - _mu*dwdx); 
	val(i,_W_+_nbf)   = (   - _mu*dwdy);
	val(i,_W_+2*_nbf) = ( p - _mu*dwdz);
	val(i,_P_+2*_nbf) = w;      
      }
    }
  }
};

class dgConservationLawPorous::psiTerm: public function {
  int _dim;
  fullMatrix<double>  sol, solGrad, gradVS;
  fullMatrix<double> rho, mu, invK;
  public:
  psiTerm (int dim, 
	   const function* rhoF, 
	   const function* muF, 
	   const function *invKF, 
	   const function *gradvsF): 
    function(dim+1), _dim(dim) {
    setArgument(sol,function::getSolution());
    setArgument(solGrad, function::getSolutionGradient());
    setArgument(rho, rhoF);
    setArgument(mu, muF);
    setArgument(invK, invKF);
    setArgument(gradVS,gradvsF);
  }
  
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    //    const int _nbf = _dim + 1;
    
    //    const double C_E = 0.37 ; // whatever

    val.setAll(0.0);
    
    const double C_E = 0.0 ; // whatever

    for (size_t i = 0; i < nQP; i++) {
      const double u = sol(i,_U_);
      const double v = sol(i,_V_);
      const double w = (_dim == 3) ? sol(i,_W_) : 0.0;
      const double normW = sqrt (u*u+v*v+w*w);
  
      const double _rho = rho(i,0);
      const double _mu = mu(i,0);
      const double _invk = invK(i,0);

      //const double dd = solGrad(i,3*_U_ + 0) + solGrad(i,3*_V_ + 1) ;


      const double divVS = _dim == 3 ?  
	gradVS(i, 3*0+0) + gradVS(i, 3*1+1) +  gradVS(i, 3*2+2) :
	gradVS(i, 0) + gradVS(i, 4) ;
      
      const double invK_forchheimer = _invk + normW * C_E * sqrt (_invk) * _rho;

      val (i, _U_) = -u * _mu * invK_forchheimer;
      val (i, _V_) = -v * _mu * invK_forchheimer;
      val (i, _P_) = -divVS;// + dd;
      if (_dim == 3) val (i, _W_) = -w * _mu * invK_forchheimer;
    }
  }
};

//is called by newOutsideValueBC to compute fluxes at boundaries
class dgConservationLawPorous::boundaryTerm:public function {
  int _dim;
  fullMatrix<double> sol, solGrad, normals;
  fullMatrix<double> rho, mu;
public:
  boundaryTerm ( const int dim, 
		 const function* rhoF, 
		 const function* muF):  function(dim+1), _dim(dim){
    setArgument(sol,function::getSolution(), 0);
    setArgument(solGrad,function::getSolutionGradient());
    setArgument(normals,function::getNormals(), 0);
    setArgument(rho, rhoF);
    setArgument(mu, muF);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    //    const dgGroupOfElements &eGroup = *m->getGroupOfElements();
    //    int iElement = m->getElementId();
    size_t nQP = sol.size1();
    val.setAll(0);

    printf("ajiedjeijde\n");

    for(size_t i = 0; i < nQP; i++) {
      double nx = normals(i,0);
      double ny = normals(i,1);
      double nz = normals(i,2);
      double u = sol(i,_U_);
      double v = sol(i,_V_);
      double w = (_dim == 3) ? sol(i,_W_):0.0;
      double p = sol(i,_P_);
      double dudx = solGrad(i,3*_U_+0);
      double dudy = solGrad(i,3*_U_+1);
      double dudz = solGrad(i,3*_U_+2);
      double dvdx = solGrad(i,3*_V_+0);
      double dvdy = solGrad(i,3*_V_+1);
      double dvdz = solGrad(i,3*_V_+2);
      double dudn = (dudx*nx+dudy*ny+dudz*nz);
      double dvdn = (dvdx*nx+dvdy*ny+dvdz*nz);
      
      double un   = u*nx+v*ny+w*nz;
      
      //      double _rho = rho(i, 0);
      double _mu = mu(i, 0);
      
      val(i, _U_) =  ( - p*nx + _mu*dudn);
      val(i, _V_) =  ( - p*ny + _mu*dvdn);
      val(i, _P_) =   - un ; 
      
      if (_dim == 3){
	double dwdx = solGrad(i,3*_W_+0);
	double dwdy = solGrad(i,3*_W_+1);
	double dwdz = solGrad(i,3*_W_+2);
	double dwdn = (dwdx*nx+dwdy*ny+dwdz*nz);
	val(i, _W_) =  ( - p*nz + _mu*dwdn);
      }
    }
  }
};

//impose un=0, so that the convective boundary fluxes simplify
class dgConservationLawPorous::boundarySlipWall : public dgBoundaryCondition {
  class term : public function {
    int _dim;
    fullMatrix<double> normals, sol;
    public:
    term(int dim):function(dim+1),_dim(dim){
      setArgument(sol,function::getSolution());
      setArgument(normals,function::getNormals());
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      size_t nQP = sol.size1();
      for(size_t i = 0; i < nQP; i++) {
        double nx = normals(i, 0);
        double ny = normals(i, 1);
        double p = sol(i, _P_);
        val(i, _U_) = (-p * nx) ;
        val(i, _V_) = (-p * ny) ;
        val(i, _P_) = 0;
	if (_dim == 3){
	  double nz = normals(i, 3);
	  val(i, _W_) = (-p * nz) ;
	}
      }
    }
  };
  term _term;
  public:
  boundarySlipWall(int dim) : _term (dim) {
    _term0 = &_term;
  }
};

class dgConservationLawPorous::boundaryVelocity : public dgBoundaryCondition {
  class term : public function {
    int _dim;
    fullMatrix<double> normals, sol, vel, solGrad;
    fullMatrix<double> rho, mu;
    public:
    term(const function *velF, const function* rhoF, const function* muF, int dim):function(dim+1),_dim(dim){
      setArgument(sol,function::getSolution());
      setArgument(solGrad,function::getSolutionGradient());
      setArgument(normals,function::getNormals());
      setArgument (vel, velF);
      setArgument(rho, rhoF);
      setArgument(mu,muF);
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      size_t nQP = sol.size1();
      for(size_t i=0; i< nQP; i++) {
	double nx = normals(i,0);
	double ny = normals(i,1);
	double nz = normals(i,2);
	double u = vel(i,0);
	double v = vel(i,1);
	double w = (_dim == 3) ? vel(i,2) : 0;
	double p = sol(i,_P_);
	double dudx = solGrad(i,3*_U_+0);
	double dudy = solGrad(i,3*_U_+1);
	double dudz = solGrad(i,3*_U_+2);
	double dvdx = solGrad(i,3*_V_+0);
	double dvdy = solGrad(i,3*_V_+1);
	double dvdz = solGrad(i,3*_V_+2);
	double dudn = (dudx*nx+dudy*ny+dudz*nz);
	double dvdn = (dvdx*nx+dvdy*ny+dvdz*nz);

	double dpdx = solGrad(i,3*_P_+0);
	double dpdy = solGrad(i,3*_P_+1);
	double dpdn = (dpdx*nx+dpdy*ny);
      
	const double TAU = 4.e-3;

	double un   = u*nx+v*ny+w*nz;
      
	//	double _rho = rho(i,0);
	double _mu = mu(i,0);
	val(i,_U_) =  -p*nx + _mu * dudn ;
	val(i,_V_) =  -p*ny + _mu * dvdn ;
	val(i,_P_) =  un + TAU * dpdn ; 
	if (_dim == 3){
	  double dwdx = solGrad(i,3*_W_+0);
	  double dwdy = solGrad(i,3*_W_+1);
	  double dwdz = solGrad(i,3*_W_+2);
	  double dwdn = (dwdx*nx+dwdy*ny+dwdz*nz);
	  val(i,_W_) = -p*nz + _mu * dwdn ;
	}
      }
    }
  };
  term _term;
  public:
  boundaryVelocity(dgConservationLawPorous *claw, const function *velF, int dim):  _term (velF, claw->getDensity(),  claw->getViscosity(), dim) {
    _term0 = &_term;
  }
};


class dgConservationLawPorous::boundaryPressure : public dgBoundaryCondition {
  class term : public function {
    int _dim;
    fullMatrix<double> normals, sol, pres, solGrad;
    fullMatrix<double> rho, mu;
    public:
    term(const function *presF, const function* rhoF, const function* muF, int dim):function(dim+1),_dim(dim){
      setArgument(sol,function::getSolution());
      setArgument(solGrad,function::getSolutionGradient());
      setArgument(normals,function::getNormals());
      setArgument (pres, presF);
      setArgument(rho, rhoF);
      setArgument(mu,muF);
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      size_t nQP = sol.size1();
      for(size_t i=0; i< nQP; i++) {
	double nx = normals(i,0);
	double ny = normals(i,1);
	double nz = normals(i,2);
	double u = sol(i,_U_);
	double v = sol(i,_V_);
	double w = (_dim == 3) ? sol(i,_W_) : 0;
	double p = pres(i,0);
	double dudx = solGrad(i,3*_U_+0);
	double dudy = solGrad(i,3*_U_+1);
	double dudz = solGrad(i,3*_U_+2);
	double dvdx = solGrad(i,3*_V_+0);
	double dvdy = solGrad(i,3*_V_+1);
	double dvdz = solGrad(i,3*_V_+2);
	double dpdx = solGrad(i,3*_P_+0);
	double dpdy = solGrad(i,3*_P_+1);
	double dudn = (dudx*nx+dudy*ny+dudz*nz);
	double dvdn = (dvdx*nx+dvdy*ny+dvdz*nz);
	double dpdn = (dpdx*nx+dpdy*ny);
      
      
	const double TAU = 4.e-3;

	double un   = u*nx+v*ny+w*nz;
      
	//	double _rho = rho(i,0);
	double _mu = mu(i,0);
	val(i,_U_) =  -p*nx + _mu * dudn ;
	val(i,_V_) =  -p*ny + _mu * dvdn ;
	val(i,_P_) =  un + TAU * dpdn ; 
	if (_dim == 3){
	  double dwdx = solGrad(i,3*_W_+0);
	  double dwdy = solGrad(i,3*_W_+1);
	  double dwdz = solGrad(i,3*_W_+2);
	  double dwdn = (dwdx*nx+dwdy*ny+dwdz*nz);
	  val(i,_W_) = -p*nz + _mu * dwdn ;
	}
      }
    }
  };
  term _term;
  public:
  boundaryPressure(dgConservationLawPorous *claw, const function *velF, int dim):  _term (velF, claw->getDensity(),  claw->getViscosity(), dim) {
    _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawPorous::newBoundarySlipWall(int dim){
  return new boundarySlipWall(dim);
}
dgBoundaryCondition *dgConservationLawPorous::newBoundaryVelocity(const function *VEL, int dim){
  return new boundaryVelocity(this, VEL, dim);
}
dgBoundaryCondition *dgConservationLawPorous::newBoundaryPressure(const function *PRES, int dim){
  return new boundaryPressure(this, PRES, dim);
}


/*==============================================================================
 * Conservation law : constructor - setup - destructor
 *============================================================================*/
dgConservationLawPorous::dgConservationLawPorous(const int dim,
						 const function* rhoF, 
						 const function *muF, 
						 const function *oneOverK, 
						 const function *porosity, 
						 const function *gradSV): 
  dgConservationLawFunction(dim+1) {
  _rhoF = rhoF;
  _muF = muF;
  _velocitySol = NULL;
  _invKF = oneOverK;
  _gradSVF = gradSV;
  _porosityF = porosity;
}

void dgConservationLawPorous::setup() {
  _volumeTerm0[""] = new psiTerm(_nbf-1, _rhoF, _muF, _invKF, _gradSVF);
  _volumeTerm1[""] = new gradPsiTerm(_nbf-1, _rhoF, _muF, _porosityF, _invKF);
  _interfaceTerm0[""] = new boundaryTerm(_nbf-1, _rhoF, _muF); 
  _velocitySol = new velocityVector();
}

dgConservationLawPorous::~dgConservationLawPorous() {
  if (_volumeTerm0[""]) delete _volumeTerm0[""];
  if (_volumeTerm1[""]) delete _volumeTerm1[""];
  if (_interfaceTerm0[""]) delete _interfaceTerm0[""];
}
