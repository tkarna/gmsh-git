#include "dgConservationLawFunction.h"
#include "dgConservationLawWaveExplicit.h"
#include "function.h"
#include "Numeric.h"
#include "functionGeneric.h"
#include "GmshConfig.h"
#include "math.h"
#include "dgDofContainer.h"
#include "dgFunctionIntegrator.h"

#if defined (HAVE_POST)
  #include "PView.h"
  #include "PViewData.h"
#endif

/*==============================================================================
 * DG-terms : Wave Equation
 *============================================================================*/

class dgConservationLawWaveExplicit::sourcepTerm : public function {
  int _DIM, _nbf, _thereIsDissipationOnP, _thereIsDamping;
  fullMatrix<double> solp, coef, pmlCoef;
 public:
  sourcepTerm(int DIM, int nbf, int thereIsDissipationOnP,
             const function *coefFunction, const function *pmlCoefFunction) : function(nbf) {
    _DIM = DIM;
    _nbf = nbf;
    _thereIsDissipationOnP = thereIsDissipationOnP;

    setArgument (solp, function::getSolution());
    setArgument (coef, coefFunction);
    _thereIsDamping = 0;
    if (pmlCoefFunction) {
      setArgument (pmlCoef, pmlCoefFunction);
      _thereIsDamping = 1;
    }
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    if (_thereIsDissipationOnP || _thereIsDamping)
      for (size_t i=0; i<nQP; i++) {
        if (_thereIsDissipationOnP)
          val(i,0) -= coef(i,2) * solp(i,0);
        
        if (_thereIsDamping) {
          val(i,0) -= pmlCoef(i,0) * solp(i,0);

        }
      }
  }
};

class dgConservationLawWaveExplicit::sourceUTerm : public function {
  int _DIM, _nbf, _thereIsDissipationOnU, _thereIsDamping;
  fullMatrix<double> solU, coef, pmlCoef;
 public:
  sourceUTerm(int DIM, int nbf, int thereIsDissipationOnU,
             const function *coefFunction, const function *pmlCoefFunction) : function(nbf) {
    _DIM = DIM;
    _nbf = nbf;
    _thereIsDissipationOnU = thereIsDissipationOnU;

    setArgument (solU, function::getSolution());
    setArgument (coef, coefFunction);
    _thereIsDamping = 0;
    if (pmlCoefFunction) {
      setArgument (pmlCoef, pmlCoefFunction);
      _thereIsDamping = 1;
    }
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    if (_thereIsDissipationOnU || _thereIsDamping)
      for (size_t i=0; i<nQP; i++) {
        if (_thereIsDissipationOnU)
          for (int j=0; j<_DIM; j++)
            val(i,j) -= coef(i,3) * solU(i,j);
        if (_thereIsDamping) {
          for(int j=0; j<_DIM; j++)
            val(i,j) -= pmlCoef(i,0) * solU(i,j);
        }
      }
  }
};

class dgConservationLawWaveExplicit::volumepTerm : public function {
  int _DIM, _nbf;
  fullMatrix<double> solU, coef;
 public:
  volumepTerm(int DIM, int nbf,const function *U, const function *coefFunction) : function(3*nbf) {
    _DIM = DIM;
    _nbf = nbf;
    setArgument (solU, U);
    setArgument (coef, coefFunction);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      double c   = coef(i,0);
      double rho = coef(i,1);
      
      double u[3];
      for (int j=0; j<_DIM; j++)
        u[j] = solU(i,j);
      
      for (int j=0; j<_DIM; j++) {
        val(i,j)       = c*c*rho * u[j];
      }
      
    }
  }
};
class dgConservationLawWaveExplicit::volumeUTerm : public function {
  int _DIM, _nbf;
  fullMatrix<double> solp, coef;
 public:
  volumeUTerm(int DIM, int nbf,const function *p, const function *coefFunction) : function(3*nbf) {
    _DIM = DIM;
    _nbf = nbf;
    setArgument (solp, p);
    setArgument (coef, coefFunction);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      double rho = coef(i,1);
      
      double pp = solp(i,0);

      for (int j=0; j<_DIM; j++) {
        val(i,j*_nbf+j) = 1/rho * pp;
      }
      
    }
  }
};
class dgConservationLawWaveExplicit::interfacepTerm : public function {
  int _DIM, _nbf;
  double theta;
  fullMatrix<double> normals, solpL,solpR, solUL, solUR, coefL, coefR;
 public:
  interfacepTerm(int DIM, int nbf, const function *U, double fluxDecentering, const function *coefFunction) : function(2*nbf) {
    _DIM = DIM;
    _nbf = nbf;
    theta = fluxDecentering;
    setArgument (normals, function::getNormals());
    setArgument (solUL, U, 0);
    setArgument (solUR, U, 1);
    setArgument (coefL, coefFunction, 0);
    setArgument (coefR, coefFunction, 1);
    setArgument (solpL, function::getSolution(), 0);
    setArgument (solpR, function::getSolution(), 1);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double n[3];
      for (int j=0; j<_DIM; j++)
        n[j] = normals(i,j);
      
      // Coefficients
      double cL = coefL(i,0), rhoL = coefL(i,1);
      double cR = coefR(i,0), rhoR = coefR(i,1);
      
      // Solution
      double pL = solpL(i,0), ndotuL = 0;
      double pR = solpR(i,0), ndotuR = 0;
      for (int j=0; j<_DIM; j++) {
        ndotuL += n[j] * solUL(i,j);
        ndotuR += n[j] * solUR(i,j);
      }
      
      // Numerical flux (with Riemann)

      double ndotuRiemann = ((ndotuR*rhoR*cR + ndotuL*rhoL*cL) - theta * (pR - pL))/(rhoR*cR + rhoL*cL);
      
      // Fluxes for the left cell
      val(i,0) = -cL*cL*rhoL * ndotuRiemann;

      // Fluxes for the right cell
      val(i,_nbf+0) = cR*cR*rhoR * ndotuRiemann;

      
    }
  }
};
class dgConservationLawWaveExplicit::interfaceUTerm : public function {
  int _DIM, _nbf;
  double theta;
  fullMatrix<double> normals, solpL, solpR, solUL, solUR, coefL, coefR;
 public:
  interfaceUTerm(int DIM, int nbf, const function *p, double fluxDecentering, const function *coefFunction) : function(2*nbf) {
    _DIM = DIM;
    _nbf = nbf;
    theta = fluxDecentering;
    setArgument (normals, function::getNormals());
    setArgument (solpL, p, 0);
    setArgument (solpR, p, 1);
    setArgument (solUL, function::getSolution(), 0);
    setArgument (solUR, function::getSolution(), 1);
    setArgument (coefL, coefFunction, 0);
    setArgument (coefR, coefFunction, 1);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double n[3];
      for (int j=0; j<_DIM; j++)
        n[j] = normals(i,j);
      
      // Coefficients
      double cL = coefL(i,0), rhoL = coefL(i,1);
      double cR = coefR(i,0), rhoR = coefR(i,1);
      
      // Solution
      double pL = solpL(i,0), ndotuL = 0;
      double pR = solpR(i,0), ndotuR = 0;
      for (int j=0; j<_DIM; j++) {
        ndotuL += n[j] * solUL(i,j);
        ndotuR += n[j] * solUR(i,j);
      }
      
      // Numerical flux (with Riemann)
      double pRiemann     = ((pR/(rhoR*cR) + pL/(rhoL*cL))     - theta * (ndotuR - ndotuL))/(1./(rhoR*cR) + 1./(rhoL*cL));

      
      // Fluxes for the left cell
      for (int j=0; j<_DIM; j++)
        val(i,j) = -1/rhoL * n[j] * pRiemann;
      
      // Fluxes for the right cell
      for (int j=0; j<_DIM; j++)
        val(i,_nbf+j) = 1/rhoR * n[j] * pRiemann;
      
    }
  }
};
class dgConservationLawWaveExplicit::maxConvectiveSpeed : public function {
  fullMatrix<double> coef;
 public:
  maxConvectiveSpeed(const function *coefFunction) : function(1) {
    setArgument (coef, coefFunction);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++)
      val(i,0) = coef(i,0);
  }
};


/*==============================================================================
 * DG-terms : Wave Equation with PML (Straight, Circle, Ellipse, Sphere or Ellipsoide)
 *============================================================================*/

class dgConservationLawWaveExplicit::sourcepTermPml : public function {
  int _DIM, _nbf;
  std::string _LayerVersion;
  fullMatrix<double> solp, solU, coef, pmlCoef, pmlDir, xyz;
 public:
  sourcepTermPml(int DIM, int nbf,const function *U, std::string LayerVersion,
                const function *coefFunction,
                const function *pmlCoefFunction,
                const function *pmlDirFunction) : function(nbf) {
    _DIM = DIM;
    _nbf = nbf;
    _LayerVersion = LayerVersion;
    setArgument (solp, function::getSolution());
    setArgument (solU, U);
    setArgument (coef, coefFunction);
    setArgument (pmlCoef, pmlCoefFunction);
    setArgument (pmlDir, pmlDirFunction);
    setArgument (xyz, function::getCoordinates());
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    if (_LayerVersion=="PmlStraight")
      callPmlCurv0(m, val);
    else if ((_LayerVersion=="PmlBeveledCuboid") ||
             (_LayerVersion=="PmlCircle") ||
             (_LayerVersion=="PmlEllipse") ||
             (_LayerVersion=="PmlSphere"))
      callPmlCurv1(m, val);
    else if (_LayerVersion=="PmlEllipsoide")
      callPmlCurv2(m, val);
    else if (_LayerVersion=="PmlSL")
      callPmlSL(m, val);
  }
  void callPmlCurv0(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      double sigmaPmlN = pmlCoef(i,0);
      
      // Solution      
      double pN = solp(i,1);
      // Terms
      val(i,0) = - sigmaPmlN * pN;
      val(i,(1)) = - sigmaPmlN * pN;
      
    }
  }
  void callPmlCurv1(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      // Coefficients
      double c = coef(i,0);
      double rho = coef(i,1);
      
      double sigmaPmlN = pmlCoef(i,0);
      double curvT = pmlCoef(i,2);
      double sigmaPmlT = pmlCoef(i,1)*curvT;
      
      double aN[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        aN[j] = pmlDir(i,j);
      
      // Solution
      double p = solp(i,0);
      double u[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        u[j] = solU(i,j);
      
      double pN = solp(i,1);
      double aNdotu = scalProd(aN,u);
      
      // Terms
      val(i,0) = - sigmaPmlN * pN
                 - sigmaPmlT * (p-pN);
      val(i,(1)) = + rho*c*c * curvT * aNdotu
                        - sigmaPmlN * pN;
        // Pour Axel: 'a' va vers l'extérieur
      
    }
  }
  void callPmlCurv2(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      // Coefficients
      double c = coef(i,0);
      double rho = coef(i,1);
      
      double sigmaPmlN = pmlCoef(i,0);
      double curvT = pmlCoef(i,2);
      double sigmaPmlT = pmlCoef(i,1)*curvT;
      double curvB = pmlCoef(i,3);
      double sigmaPmlB = pmlCoef(i,1)*curvB;
      double aN[3] = {0., 0., 0.};
      //double aT[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++) {
        aN[j] = pmlDir(i,j);
       // aT[j] = pmlDir(i,j+3);
      }
      
      // Solution
      double p = solp(i,0);
      double u[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        u[j] = solU(i,j);
      
      double pN = solp(i,1);
      double pT = solp(i,2);
      double aNdotu = scalProd(aN,u);
      
      // Terms
      val(i,0) = - sigmaPmlN * pN
                 - 0.5*(sigmaPmlT+sigmaPmlB) * (p-pN)
                 - 0.5*(sigmaPmlT-sigmaPmlB) * (2*pT+pN-p);

      val(i,(1)) = + rho*c*c * (curvT+curvB) * aNdotu
                        - sigmaPmlN * pN;
      val(i,(2)) = - rho*c*c * (curvT) * aNdotu
                        - sigmaPmlT * pT;
      
    }
  }
  void callPmlSL(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      // Coefficients
      
      double alpha = pmlCoef(i,0);
      double beta  = pmlCoef(i,1) * pmlCoef(i,2);
      
      double aN[3] = { pmlDir(i,0), pmlDir(i,1), 0.};
      double aT[3] = {-pmlDir(i,1), pmlDir(i,0), 0.};
      
      // Solution
      double p = solp(i,0);
      double u[3] = {solU(i,0), solU(i,1), 0.};
      double q = solp(i,1);
      double v[3] = {solp(i,2), solp(i,3), 0.};
      
      double aN_dot_uav = 0;
      double aT_dot_ubv = 0;
      for (int j=0; j<2; j++) {
        aN_dot_uav += aN[j] * (u[j] + alpha * v[j]);
        aT_dot_ubv += aT[j] * (u[j] + beta  * v[j]);
      }
      
      // Terms
      val(i,0) = - (alpha + beta) * p - alpha * beta * q;
      val(i,1) = p;
      
    }
  }
};
class dgConservationLawWaveExplicit::sourceUTermPml : public function {
  int _DIM, _nbf;
  std::string _LayerVersion;
  fullMatrix<double> solp, solU, coef, pmlCoef, pmlDir, xyz;
 public:
  sourceUTermPml(int DIM, int nbf,const function *p, std::string LayerVersion,
                const function *coefFunction,
                const function *pmlCoefFunction,
                const function *pmlDirFunction) : function(nbf) {
    _DIM = DIM;
    _nbf = nbf;
    _LayerVersion = LayerVersion;
    setArgument (solp, function::getSolution());
    setArgument (solU, function::getSolution());
    setArgument (coef, coefFunction);
    setArgument (pmlCoef, pmlCoefFunction);
    setArgument (pmlDir, pmlDirFunction);
    setArgument (xyz, function::getCoordinates());
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    if (_LayerVersion=="PmlStraight")
      callPmlCurv0(m, val);
    else if ((_LayerVersion=="PmlBeveledCuboid") ||
             (_LayerVersion=="PmlCircle") ||
             (_LayerVersion=="PmlEllipse") ||
             (_LayerVersion=="PmlSphere"))
      callPmlCurv1(m, val);
    else if (_LayerVersion=="PmlEllipsoide")
      callPmlCurv2(m, val);
    else if (_LayerVersion=="PmlSL")
      callPmlSL(m, val);
  }
  void callPmlCurv0(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      
      double aN[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        aN[j] = pmlDir(i,j);
      
      double sigmaPmlN = pmlCoef(i,0);
      
      // Solution
      double u[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        u[j] = solU(i,j);
      
      double pN = solU(i,_DIM);
      double aNdotu = scalProd(aN,u);
      
      // Terms
      for(int j=0; j<_DIM; j++)
        val(i,j) = - sigmaPmlN * aN[j]*aNdotu;
      val(i,(_DIM)) = - sigmaPmlN * pN;
      
    }
  }
  void callPmlCurv1(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      // Coefficients
      double c = coef(i,0);
      double rho = coef(i,1);
      
      double sigmaPmlN = pmlCoef(i,0);
      double curvT = pmlCoef(i,2);
      double sigmaPmlT = pmlCoef(i,1)*curvT;
      
      double aN[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        aN[j] = pmlDir(i,j);
      
      // Solution

      double u[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        u[j] = solU(i,j);
      
      double pN = solU(i,_DIM);
      double aNdotu = scalProd(aN,u);
      
      // Terms
      for (int j=0; j<_DIM; j++)
        val(i,j) = - sigmaPmlN * aN[j]*aNdotu
                     - sigmaPmlT * (u[j]-aN[j]*aNdotu);
      val(i,(_DIM)) = + rho*c*c * curvT * aNdotu
                        - sigmaPmlN * pN;
        // Pour Axel: 'a' va vers l'extérieur
      
    }
  }
  void callPmlCurv2(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      // Coefficients
      double c = coef(i,0);
      double rho = coef(i,1);
      
      double sigmaPmlN = pmlCoef(i,0);
      double curvT = pmlCoef(i,2);
      double sigmaPmlT = pmlCoef(i,1)*curvT;
      double curvB = pmlCoef(i,3);
      double sigmaPmlB = pmlCoef(i,1)*curvB;
      
      double aN[3] = {0., 0., 0.};
      double aT[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++) {
        aN[j] = pmlDir(i,j);
        aT[j] = pmlDir(i,j+3);
      }
      
      // Solution

      double u[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        u[j] = solU(i,j);
      
      double pN = solU(i,_DIM);
      double pT = solU(i,_DIM+1);
      double aNdotu = scalProd(aN,u);
      double aTdotu = scalProd(aT,u);
      
      // Terms
      for (int j=0; j<_DIM; j++)
        val(i,j) = - sigmaPmlN * aN[j]*aNdotu
                     - 0.5*(sigmaPmlT+sigmaPmlB) * (u[j]-aN[j]*aNdotu)
                     - 0.5*(sigmaPmlT-sigmaPmlB) * (2*aT[j]*aTdotu+aN[j]*aNdotu-u[j]);
      val(i,(_DIM)) = + rho*c*c * (curvT+curvB) * aNdotu
                        - sigmaPmlN * pN;
      val(i,(_DIM+1)) = - rho*c*c * (curvT) * aNdotu
                        - sigmaPmlT * pT;
      
    }
  }
  void callPmlSL(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      // Coefficients
      
      double alpha = pmlCoef(i,0);
      double beta  = pmlCoef(i,1) * pmlCoef(i,2);
      
      double aN[3] = { pmlDir(i,0), pmlDir(i,1), 0.};
      double aT[3] = {-pmlDir(i,1), pmlDir(i,0), 0.};
      
      // Solution
      double p = solp(i,0);
      double u[3] = {solU(i,0), solU(i,1), 0.};
      double v[3] = {solU(i,3), solU(i,4), 0.};
      
      double aN_dot_uav = 0;
      double aT_dot_ubv = 0;
      for (int j=0; j<2; j++) {
        aN_dot_uav += aN[j] * (u[j] + alpha * v[j]);
        aT_dot_ubv += aT[j] * (u[j] + beta  * v[j]);
      }
      
      // Terms
      val(i,0) = - (alpha - beta) * (aN[0] * aN_dot_uav - aT[0] * aT_dot_ubv);
      val(i,1) = - (alpha - beta) * (aN[1] * aN_dot_uav - aT[1] * aT_dot_ubv);
      val(i,2) = p;
      val(i,3) = u[0] - alpha * v[0];
      val(i,4) = u[1] + beta * v[1];
      
    }
  }
};
class dgConservationLawWaveExplicit::volumepTermPml : public function {
  int _DIM, _nbf;
  std::string _LayerVersion;
  fullMatrix<double> solp, solU, coef, pmlDir;
 public:
  volumepTermPml(int DIM, int nbf,const function *U, std::string LayerVersion,
                const function *coefFunction,
                const function *pmlCoefFunction,
                const function *pmlDirFunction) : function(3*nbf) {
    _DIM = DIM;
    _nbf = nbf;
    _LayerVersion = LayerVersion;
    setArgument (solp, function::getSolution());
    setArgument (solU, U);
    setArgument (coef, coefFunction);
    setArgument (pmlDir, pmlDirFunction);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    if ((_LayerVersion=="PmlStraight") ||
        (_LayerVersion=="PmlBeveledCuboid") ||
        (_LayerVersion=="PmlCircle") ||
        (_LayerVersion=="PmlEllipse") ||
        (_LayerVersion=="PmlSphere"))
      callPml1AddField(m, val);
    else if (_LayerVersion=="PmlEllipsoide")
      callPml2AddFields(m, val);
  }
  void callPml1AddField(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      // Coefficients
      double c = coef(i,0);
      double rho = coef(i,1);
      double aN[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        aN[j] = pmlDir(i,j);
      
      // Solution
      double u[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        u[j] = solU(i,j);
      double aNdotu = scalProd(aN,u);
      
      // Terms
      for (int j=0; j<_DIM; j++) {
        val(i,j*_nbf)          = c*c*rho * u[j];
        val(i,j*_nbf+(1)) = c*c*rho * aN[j]*aNdotu;
      }
       
    }
  }
  void callPml2AddFields(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      // Coefficients
      double c = coef(i,0);
      double rho = coef(i,1);
      double aN[3] = {0., 0., 0.};
      double aT[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++) {
        aN[j] = pmlDir(i,j);
        aT[j] = pmlDir(i,j+3);
      }
      
      // Solution
      double u[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        u[j] = solU(i,j);
      double aNdotu = scalProd(aN,u);
      double aTdotu = scalProd(aT,u);
      
      // Terms
      for (int j=0; j<_DIM; j++) {
        val(i,j*_nbf)          = c*c*rho * u[j];
        val(i,j*_nbf+(1)) = c*c*rho * aN[j]*aNdotu;
        val(i,j*_nbf+(2)) = c*c*rho * aT[j]*aTdotu;
      }
      
    }
  }
};
class dgConservationLawWaveExplicit::volumeUTermPml : public function {
  int _DIM, _nbf;
  std::string _LayerVersion;
  fullMatrix<double> solp, solU, coef, pmlDir;
 public:
  volumeUTermPml(int DIM, int nbf,const function *p, std::string LayerVersion,
                const function *coefFunction,
                const function *pmlCoefFunction,
                const function *pmlDirFunction) : function(3*nbf) {
    _DIM = DIM;
    _nbf = nbf;
    _LayerVersion = LayerVersion;
    setArgument (solp, p);
    setArgument (solU, function::getSolution());
    setArgument (coef, coefFunction);
    setArgument (pmlDir, pmlDirFunction);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    if ((_LayerVersion=="PmlStraight") ||
        (_LayerVersion=="PmlBeveledCuboid") ||
        (_LayerVersion=="PmlCircle") ||
        (_LayerVersion=="PmlEllipse") ||
        (_LayerVersion=="PmlSphere"))
      callPml1AddField(m, val);
    else if (_LayerVersion=="PmlEllipsoide")
      callPml2AddFields(m, val);
  }
  void callPml1AddField(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      // Coefficients
      double c = coef(i,0);
      double rho = coef(i,1);
      double aN[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        aN[j] = pmlDir(i,j);
      
      // Solution
      double p = solp(i,0);
      double u[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        u[j] = solU(i,j);
      double aNdotu = scalProd(aN,u);
      
      // Terms
      for (int j=0; j<_DIM; j++) {
        val(i,j*_nbf+(j))    = 1/rho * p;
        val(i,j*_nbf+(_DIM)) = c*c*rho * aN[j]*aNdotu;
      }
      
    }
  }
  void callPml2AddFields(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      // Coefficients
      double c = coef(i,0);
      double rho = coef(i,1);
      double aN[3] = {0., 0., 0.};
      double aT[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++) {
        aN[j] = pmlDir(i,j);
        aT[j] = pmlDir(i,j+3);
      }
      
      // Solution
      double p = solp(i,0);
      double u[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        u[j] = solU(i,j);
      double aNdotu = scalProd(aN,u);
      double aTdotu = scalProd(aT,u);
      
      // Terms
      for (int j=0; j<_DIM; j++) {
        val(i,j*_nbf+(j))    = 1/rho * p;
        val(i,j*_nbf+(_DIM)) = c*c*rho * aN[j]*aNdotu;
        val(i,j*_nbf+(_DIM+1)) = c*c*rho * aT[j]*aTdotu;
      }
      
    }
  }
};
class dgConservationLawWaveExplicit::interfacepTermPml : public function {
  int _DIM, _nbf;
  std::string _LayerVersion;
  double theta;
  fullMatrix<double> normals, pmlDir;
  fullMatrix<double> solpL, solUL, coefL;
  fullMatrix<double> solpR, solUR, coefR;
 public:
  interfacepTermPml(int DIM, int nbf, const function *U, std::string LayerVersion, double fluxDecentering,
                   const function *coefFunction,
                   const function *pmlDirFunction) : function(2*nbf) {
    _DIM = DIM;
    _nbf = nbf;
    _LayerVersion = LayerVersion;
    theta = fluxDecentering;
    setArgument (normals, function::getNormals());
    setArgument (solpL, function::getSolution(), 0);
    setArgument (solpR, function::getSolution(), 1);
    setArgument (solUL, U, 0);
    setArgument (solUR, U, 1);
    setArgument (coefL, coefFunction, 0);
    setArgument (coefR, coefFunction, 1);
    setArgument (pmlDir, pmlDirFunction);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    if ((_LayerVersion=="PmlStraight") ||
        (_LayerVersion=="PmlBeveledCuboid") ||
        (_LayerVersion=="PmlCircle") ||
        (_LayerVersion=="PmlEllipse") ||
        (_LayerVersion=="PmlSphere"))
      callPml1AddField(m, val);
    else if (_LayerVersion=="PmlEllipsoide")
      callPml2AddFields(m, val);
  }
  void callPml1AddField(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double n[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        n[j] = normals(i,j);
      
      // Coefficients
      double cL = coefL(i,0), rhoL = coefL(i,1);
      double cR = coefR(i,0), rhoR = coefR(i,1);
      
      double aN[3] = {0., 0., 0.}; 
      for (int j=0; j<_DIM; j++)
        aN[j] += pmlDir(i,j);
      double aNdotn = scalProd(aN,n);
      
      // Solution
      double pL = solpL(i,0), uL[3]={0., 0., 0.};
      double pR = solpR(i,0), uR[3]={0., 0., 0.};
      for (int j=0; j<_DIM; j++) {
        uL[j] = solUL(i,j);
        uR[j] = solUR(i,j);
      }
      double ndotuL = scalProd(n,uL);
      double ndotuR = scalProd(n,uR);
      double aNdotuL = scalProd(aN,uL);
      double aNdotuR = scalProd(aN,uR);
      
      double qNL = solpL(i,1);
      double qNR = solpR(i,1);
      
      // Numerical flux (with Riemann)
      double ndotuRiemann = ((ndotuR*rhoR*cR + ndotuL*rhoL*cL) - theta * (pR - pL))/(rhoR*cR + rhoL*cL);
      
      val(i,0) = -cL*cL*rhoL * ndotuRiemann;
      val(i,1) = - aNdotn*(cR*cR*rhoR*aNdotuR + cL*cL*rhoL*aNdotuL)/2. + cL*(qNR-qNL)/2.;
      val(i,_nbf+0) = cR*cR*rhoR * ndotuRiemann;
      val(i,_nbf+1) = aNdotn*(cR*cR*rhoR*aNdotuR + cL*cL*rhoL*aNdotuL)/2. - cR*(qNR-qNL)/2.;
      
    }
  }
  void callPml2AddFields(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double n[3];
      for (int j=0; j<_DIM; j++)
        n[j] = normals(i,j);
      
      // Coefficients
      double cL = coefL(i,0), rhoL = coefL(i,1);
      double cR = coefR(i,0), rhoR = coefR(i,1);
      
      double aN[3] = {0.,0.,0.};
      double aT[3] = {0.,0.,0.};
      for (int j=0; j<_DIM; j++) {
        aN[j] = pmlDir(i,j);
        aT[j] = pmlDir(i,j+3);
      }
      double aNdotn = scalProd(aN,n);
      double aTdotn = scalProd(aT,n);
      
      // Solution
      double pL = solpL(i,0), uL[3] = {0., 0., 0.};
      double pR = solpR(i,0), uR[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++) {
        uL[j] = solUL(i,j);
        uR[j] = solUR(i,j);
      }
      double ndotuL = scalProd(n,uL);
      double ndotuR = scalProd(n,uR);
      double aNdotuL = scalProd(aN,uL);
      double aNdotuR = scalProd(aN,uR);
      double aTdotuL = scalProd(aT,uL);
      double aTdotuR = scalProd(aT,uR);
      
      double qNL = solpL(i,1);
      double qNR = solpR(i,1);
      double qTL = solpL(i,2);
      double qTR = solpR(i,2);
      
      // Terms
      double ndotuRiemann = ((ndotuR*rhoR*cR + ndotuL*rhoL*cL) - theta * (pR - pL))/(rhoR*cR + rhoL*cL);
      
      val(i,0) = -cL*cL*rhoL * ndotuRiemann;     
      val(i,_nbf+0) = cR*cR*rhoR * ndotuRiemann;
      val(i,1) = - ( aNdotn*(cR*cR*rhoR*aNdotuR + cL*cL*rhoL*aNdotuL)/2. - cL*(qNR-qNL)/2. );
      val(i,2) = - ( aTdotn*(cR*cR*rhoR*aTdotuR + cL*cL*rhoL*aTdotuL)/2. - cL*(qTR-qTL)/2. );      
      val(i,_nbf+1) = aNdotn*(cR*cR*rhoR*aNdotuR + cL*cL*rhoL*aNdotuL)/2. - cR*(qNR-qNL)/2.;
      val(i,_nbf+2) = aTdotn*(cR*cR*rhoR*aTdotuR + cL*cL*rhoL*aTdotuL)/2. - cR*(qTR-qTL)/2.;
      
    }
  }
};

class dgConservationLawWaveExplicit::interfaceUTermPml : public function {
  int _DIM, _nbf;
  std::string _LayerVersion;
  double theta;
  fullMatrix<double> normals, pmlDir;
  fullMatrix<double> solpL, solUL, coefL;
  fullMatrix<double> solpR, solUR, coefR;
 public:
  interfaceUTermPml(int DIM, int nbf, const function *p, std::string LayerVersion, double fluxDecentering,
                   const function *coefFunction,
                   const function *pmlDirFunction) : function(2*nbf) {
    _DIM = DIM;
    _nbf = nbf;
    _LayerVersion = LayerVersion;
    theta = fluxDecentering;
    setArgument (normals, function::getNormals());
    setArgument (solpL, p, 0);
    setArgument (solpR, p, 1);
    setArgument (solUL, function::getSolution(), 0);
    setArgument (solUR, function::getSolution(), 1);
    setArgument (coefL, coefFunction, 0);
    setArgument (coefR, coefFunction, 1);
    setArgument (pmlDir, pmlDirFunction);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    if ((_LayerVersion=="PmlStraight") ||
        (_LayerVersion=="PmlBeveledCuboid") ||
        (_LayerVersion=="PmlCircle") ||
        (_LayerVersion=="PmlEllipse") ||
        (_LayerVersion=="PmlSphere"))
      callPml1AddField(m, val);
    else if (_LayerVersion=="PmlEllipsoide")
      callPml2AddFields(m, val);
  }
  void callPml1AddField(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double n[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        n[j] = normals(i,j);
      
      // Coefficients
      double cL = coefL(i,0), rhoL = coefL(i,1);
      double cR = coefR(i,0), rhoR = coefR(i,1);
      
      double aN[3] = {0., 0., 0.}; 
      for (int j=0; j<_DIM; j++)
        aN[j] += pmlDir(i,j);
      double aNdotn = scalProd(aN,n);
      
      // Solution
      double pL = solpL(i,0), uL[3]={0., 0., 0.};
      double pR = solpR(i,0), uR[3]={0., 0., 0.};
      for (int j=0; j<_DIM; j++) {
        uL[j] = solUL(i,j);
        uR[j] = solUR(i,j);
      }
      double ndotuL = scalProd(n,uL);
      double ndotuR = scalProd(n,uR);
      double aNdotuL = scalProd(aN,uL);
      double aNdotuR = scalProd(aN,uR);
      
      double qNL = solUL(i,_DIM);
      double qNR = solUR(i,_DIM);
      
      // Numerical flux (with Riemann)
      double pRiemann     = ((pR/(rhoR*cR) + pL/(rhoL*cL))     - theta * (ndotuR - ndotuL))/(1./(rhoR*cR) + 1./(rhoL*cL));
      

      for (int j=0; j<_DIM; j++)
        val(i,j) = -1/rhoL * n[j] * pRiemann;
      val(i,_DIM) = - aNdotn*(cR*cR*rhoR*aNdotuR + cL*cL*rhoL*aNdotuL)/2. + cL*(qNR-qNL)/2.;
      
      for (int j=0; j<_DIM; j++)
        val(i,_nbf+j) = 1/rhoR * n[j] * pRiemann;
      val(i,_nbf+_DIM) = aNdotn*(cR*cR*rhoR*aNdotuR + cL*cL*rhoL*aNdotuL)/2. - cR*(qNR-qNL)/2.;
      
    }
  }
  void callPml2AddFields(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double n[3];
      for (int j=0; j<_DIM; j++)
        n[j] = normals(i,j);
      
      // Coefficients
      double cL = coefL(i,0), rhoL = coefL(i,1);
      double cR = coefR(i,0), rhoR = coefR(i,1);
      
      double aN[3] = {0.,0.,0.};
      double aT[3] = {0.,0.,0.};
      for (int j=0; j<_DIM; j++) {
        aN[j] = pmlDir(i,j);
        aT[j] = pmlDir(i,j+3);
      }
      double aNdotn = scalProd(aN,n);
      double aTdotn = scalProd(aT,n);
      
      // Solution
      double pL = solpL(i,0), uL[3] = {0., 0., 0.};
      double pR = solpR(i,0), uR[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++) {
        uL[j] = solUL(i,j+1);
        uR[j] = solUR(i,j+1);
      }
      double ndotuL = scalProd(n,uL);
      double ndotuR = scalProd(n,uR);
      double aNdotuL = scalProd(aN,uL);
      double aNdotuR = scalProd(aN,uR);
      double aTdotuL = scalProd(aT,uL);
      double aTdotuR = scalProd(aT,uR);
      
      double qNL = solUL(i,_DIM);
      double qNR = solUR(i,_DIM);
      double qTL = solUL(i,_DIM+1);
      double qTR = solUR(i,_DIM+1);
      
      // Terms
      double pRiemann     = ((pR/(rhoR*cR) + pL/(rhoL*cL))     - theta * (ndotuR - ndotuL))/(1./(rhoR*cR) + 1./(rhoL*cL));
      

      for (int j=0; j<_DIM; j++)
        val(i,j) = -1/rhoL * n[j] * pRiemann;
      
      for (int j=0; j<_DIM; j++)
        val(i,_nbf+j) = 1/rhoR * n[j] * pRiemann;
      
      val(i,_DIM) = - ( aNdotn*(cR*cR*rhoR*aNdotuR + cL*cL*rhoL*aNdotuL)/2. - cL*(qNR-qNL)/2. );
      val(i,_DIM+1) = - ( aTdotn*(cR*cR*rhoR*aTdotuR + cL*cL*rhoL*aTdotuL)/2. - cL*(qTR-qTL)/2. );
      
      val(i,_nbf+_DIM) = aNdotn*(cR*cR*rhoR*aNdotuR + cL*cL*rhoL*aNdotuL)/2. - cR*(qNR-qNL)/2.;
      val(i,_nbf+_DIM+1) = aTdotn*(cR*cR*rhoR*aTdotuR + cL*cL*rhoL*aTdotuL)/2. - cR*(qTR-qTL)/2.;
      
    }
  }
};
/*==============================================================================
 * DG-terms : Pml - Straight, Circle, Ellipse or Sphere
 *============================================================================*/

class dgConservationLawWaveExplicit::sourcepTermPmlOld : public function {
  int _DIM, _nbf, _incidentFieldsDefined;
  std::string _LayerVersion;
  fullMatrix<double> solp, solU, coef, pmlCoef, pmlDir, pmlDirGrad, incidentFields, xyz;
 public:
  sourcepTermPmlOld(int DIM, int nbf, const function *U, std::string LayerVersion,
                   const function *coefFunction,
                   const function *pmlCoefFunction,
                   const function *pmlDirFunction,
                   const function *incidentFieldsFunction) : function(nbf) {
    _DIM = DIM;
    _nbf = nbf;
    _LayerVersion = LayerVersion;
    setArgument (solp, function::getSolution());
    setArgument (solU, U);
    setArgument (coef, coefFunction);
    setArgument (pmlCoef, pmlCoefFunction);
    setArgument (pmlDir, pmlDirFunction);
    _incidentFieldsDefined = 0;
    if (incidentFieldsFunction!=NULL) {
      setArgument (incidentFields, incidentFieldsFunction);
      _incidentFieldsDefined = 1;
    }
    
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      
      double aN[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        aN[j] = pmlDir(i,j);
      
      // Solution
      
      double p = solp(i,0);
      double q = solp(i,1);
      
      double u[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        u[j] = solU(i,j);
      
      double uInc[3] = {0., 0., 0.};
      if (_incidentFieldsDefined)
        for (int j=0; j<_DIM; j++)
          uInc[j] = incidentFields(i,j+1);
      
      // Source terms
      
      double adotuScat = 0;
      for (int j=0; j<_DIM; j++)
        adotuScat += aN[j] * (u[j] - uInc[j]);
      
      if (_LayerVersion=="PmlStraightOld") {
        
        double sigmaPmlN = pmlCoef(i,0);
        
        val(i,0) = - sigmaPmlN * q;
        val(i,(1)) = - sigmaPmlN * q;
        
      } else if ((_LayerVersion=="PmlCircleOld") ||
                 (_LayerVersion=="PmlEllipseOld") ||
                 (_LayerVersion=="PmlSphereOld")) {
        
        double sigmaPmlN = pmlCoef(i,0);
        double sigmaPmlT = pmlCoef(i,1)*pmlCoef(i,2);
        
        val(i,0) = - sigmaPmlN * q - sigmaPmlT * (p - q);
        val(i,(1)) = - sigmaPmlN * q;
          // Pour Axel: 'a' va vers l'extérieur
        
      }
      
    }
  }
};

class dgConservationLawWaveExplicit::sourceUTermPmlOld : public function {
  int _DIM, _nbf, _incidentFieldsDefined;
  std::string _LayerVersion;
  fullMatrix<double> solp, solU, coef, pmlCoef, pmlDir, pmlDirGrad, incidentFields, xyz;
 public:
  sourceUTermPmlOld(int DIM, int nbf,const function *p, std::string LayerVersion,
                   const function *coefFunction,
                   const function *pmlCoefFunction,
                   const function *pmlDirFunction,
                   const function *incidentFieldsFunction) : function(nbf) {
    _DIM = DIM;
    _nbf = nbf;
    _LayerVersion = LayerVersion;
    setArgument (solp, p);
    setArgument (solU, function::getSolution());
    setArgument (coef, coefFunction);
    setArgument (pmlCoef, pmlCoefFunction);
    setArgument (pmlDir, pmlDirFunction);
    _incidentFieldsDefined = 0;
    if (incidentFieldsFunction!=NULL) {
      setArgument (incidentFields, incidentFieldsFunction);
      _incidentFieldsDefined = 1;
    }
    
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      
      double aN[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        aN[j] = pmlDir(i,j);
      
      // Solution
      
      double q = solU(i,_DIM);
      
      double u[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        u[j] = solU(i,j);
      
      double uInc[3] = {0., 0., 0.};
      if (_incidentFieldsDefined)
        for (int j=0; j<_DIM; j++)
          uInc[j] = incidentFields(i,j+1);
      
      // Source terms
      
      double adotuScat = 0;
      for (int j=0; j<_DIM; j++)
        adotuScat += aN[j] * (u[j] - uInc[j]);
      
      if (_LayerVersion=="PmlStraightOld") {
        
        double sigmaPmlN = pmlCoef(i,0);
        
        for(int j=0; j<_DIM; j++)
          val(i,j) = - sigmaPmlN * aN[j] * adotuScat;
        val(i,(_DIM)) = - sigmaPmlN * q;
        
      } else if ((_LayerVersion=="PmlCircleOld") ||
                 (_LayerVersion=="PmlEllipseOld") ||
                 (_LayerVersion=="PmlSphereOld")) {
        
        double sigmaPmlN = pmlCoef(i,0);
        double sigmaPmlT = pmlCoef(i,1)*pmlCoef(i,2);
        
        for (int j=0; j<_DIM; j++)
          val(i,j) = - sigmaPmlN * aN[j]*adotuScat - sigmaPmlT * (u[j] - aN[j]*adotuScat);
        val(i,(_DIM)) = - sigmaPmlN * q;
          // Pour Axel: 'a' va vers l'extérieur
        
      }
      
    }
  }
};
class dgConservationLawWaveExplicit::volumepTermPmlOld : public function {
  int _DIM, _nbf, _incidentFieldsDefined;
  fullMatrix<double> solp, solU, coef, pmlDir, incidentFields;
 public:
  volumepTermPmlOld(int DIM, int nbf, const function *U,
                   const function *coefFunction,
                   const function *pmlCoefFunction,
                   const function *pmlDirFunction,
                   const function *incidentFieldsFunction) : function(3*nbf) {
    _DIM = DIM;
    _nbf = nbf;
    setArgument (solp, function::getSolution());
    setArgument (solU, U);
    setArgument (coef, coefFunction);
    setArgument (pmlDir, pmlDirFunction);
    _incidentFieldsDefined = 0;
    if (incidentFieldsFunction!=NULL) {
      setArgument (incidentFields, incidentFieldsFunction);
      _incidentFieldsDefined = 1;
    }
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      // Coefficients
      
      double c = coef(i,0);
      double rho = coef(i,1);
      
      double a[3];
      for (int j=0; j<_DIM; j++)
        a[j] = pmlDir(i,j);
      
      // Solution
      
      double u[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        u[j] = solU(i,j);
      
      double uInc[3] = {0., 0., 0.};
      if (_incidentFieldsDefined) {
        for (int j=0; j<_DIM; j++)
          uInc[j] = incidentFields(i,j+1);
      }
      
      // Volume terms
      
      double adotuScat = 0;
      for (int j=0; j<_DIM; j++)
        adotuScat += a[j] * (u[j] - uInc[j]);
        
      for (int j=0; j<_DIM; j++) {
        val(i,j*_nbf)          = c*c*rho * u[j];
        val(i,j*_nbf+(1)) = c*c*rho * a[j] * adotuScat;
      }
      
    }
  }
};
class dgConservationLawWaveExplicit::volumeUTermPmlOld : public function {
  int _DIM, _nbf, _incidentFieldsDefined;
  fullMatrix<double> solp, solU, coef, pmlDir, incidentFields;
 public:
  volumeUTermPmlOld(int DIM, int nbf, const function *p,
                   const function *coefFunction,
                   const function *pmlCoefFunction,
                   const function *pmlDirFunction,
                   const function *incidentFieldsFunction) : function(3*nbf) {
    _DIM = DIM;
    _nbf = nbf;
    setArgument (solp, p);
    setArgument (solU, function::getSolution());
    setArgument (coef, coefFunction);
    setArgument (pmlDir, pmlDirFunction);
    _incidentFieldsDefined = 0;
    if (incidentFieldsFunction!=NULL) {
      setArgument (incidentFields, incidentFieldsFunction);
      _incidentFieldsDefined = 1;
    }
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      // Coefficients
      
      double c = coef(i,0);
      double rho = coef(i,1);
      
      double a[3];
      for (int j=0; j<_DIM; j++)
        a[j] = pmlDir(i,j);
      
      // Solution
      
      double p = solp(i,0);
      double u[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        u[j] = solU(i,j);
      
      double uInc[3] = {0., 0., 0.};
      if (_incidentFieldsDefined) {
        for (int j=0; j<_DIM; j++)
          uInc[j] = incidentFields(i,j+1);
      }
      
      // Volume terms
      
      double adotuScat = 0;
      for (int j=0; j<_DIM; j++)
        adotuScat += a[j] * (u[j] - uInc[j]);
        
      for (int j=0; j<_DIM; j++) {
        val(i,j*_nbf+(j))    = 1/rho * p;
        val(i,j*_nbf+(_DIM)) = c*c*rho * a[j] * adotuScat;
      }
      
    }
  }
};

class dgConservationLawWaveExplicit::interfacepTermPmlOld : public function {
  int _DIM, _nbf, _incidentFieldsDefined;
  double theta;
  fullMatrix<double> normals;
  fullMatrix<double> solpL, solUL, coefL, pmlDirL, incidentFieldsL;
  fullMatrix<double> solpR, solUR, coefR, pmlDirR, incidentFieldsR;
 public:
  interfacepTermPmlOld(int DIM, int nbf, const function *U, double fluxDecentering,
                      const function *coefFunction,
                      const function *pmlDirFunction,
                      const function *incidentFieldsFunction) : function(2*nbf) {
    _DIM = DIM;
    _nbf = nbf;
    theta = fluxDecentering;
    setArgument (normals, function::getNormals());
    setArgument (solpL, function::getSolution(), 0);
    setArgument (solpR, function::getSolution(), 1);
    setArgument (solUL, U, 0);
    setArgument (solUR, U, 1);
    setArgument (coefL, coefFunction, 0);
    setArgument (coefR, coefFunction, 1);
    setArgument (pmlDirL, pmlDirFunction, 0);
    setArgument (pmlDirR, pmlDirFunction, 1);
    _incidentFieldsDefined = 0;
    if (incidentFieldsFunction!=NULL) {
      setArgument (incidentFieldsL, incidentFieldsFunction, 0);
      setArgument (incidentFieldsR, incidentFieldsFunction, 1);
      _incidentFieldsDefined = 1;
    }
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double n[3];
      for (int j=0; j<_DIM; j++)
        n[j] = normals(i,j);
      
      // Coefficients
      double cL = coefL(i,0), rhoL = coefL(i,1);
      double cR = coefR(i,0), rhoR = coefR(i,1);
      double aL[3] = {0., 0., 0.}; 
      double aR[3] = {0., 0., 0.}; 
      for (int j=0; j<_DIM; j++) {
        aL[j] += pmlDirL(i,j);
        aR[j] += pmlDirR(i,j);
      }
      double uIncL[3] = {0., 0., 0.};
      double uIncR[3] = {0., 0., 0.};
      if (_incidentFieldsDefined)
        for (int j=0; j<_DIM; j++) {
          uIncL[j] = incidentFieldsL(i,j+1);
          uIncR[j] = incidentFieldsR(i,j+1);
        }
      
      // Solution
      double pL = solpL(i,0), uL[3]={0., 0., 0.};
      double pR = solpR(i,0), uR[3]={0., 0., 0.};
      for (int j=0; j<_DIM; j++) {
        uL[j] = solUL(i,j);
        uR[j] = solUR(i,j);
      }
      double ndotuL = scalProd(n,uL);
      double ndotuR = scalProd(n,uR);
      
      // Numerical flux (with Riemann)
      double ndotuRiemann = ((ndotuR*rhoR*cR + ndotuL*rhoL*cL) - theta * (pR - pL))/(rhoR*cR + rhoL*cL);
      
      val(i,0) = -cL*cL*rhoL * ndotuRiemann;

      
      val(i,_nbf+0) = cR*cR*rhoR * ndotuRiemann;

      
      // Numerical flux for additionnal equations
      if (_incidentFieldsDefined) {
        
        // (with Lax-Friedrich)
        double adotn, adotuScatL, adotuScatR;
        double qJump = (solpR(i,1)-solpL(i,1))/2.;
        if (norm3(aL)>0.5) { // Left
          adotn = scalProd(aL,n);
          adotuScatL = 0.;
          adotuScatR = 0.;
          for (int j=0; j<_DIM; j++) {
            adotuScatL += aL[j] * (uL[j]-uIncL[j]);
            adotuScatR += aL[j] * (uR[j]-uIncR[j]);
          }
          val(i,1) = - ( adotn*(cR*cR*rhoR*adotuScatR + cL*cL*rhoL*adotuScatL)/2. - cL*qJump );
        }
        if (norm3(aR)>0.5) { // Right
          adotn = scalProd(aR,n);
          adotuScatL = 0.;
          adotuScatR = 0.;
          for (int j=0; j<_DIM; j++) {
            adotuScatL += aR[j] * (uL[j]-uIncL[j]);
            adotuScatR += aR[j] * (uR[j]-uIncR[j]);
          }
          val(i,_nbf+1) = adotn*(cR*cR*rhoR*adotuScatR + cL*cL*rhoL*adotuScatL)/2. - cR*qJump;
        }
        
      } else {
        
        // (with Riemann)
        double adotn, adotuR, adotuL;
        double qJump = (solpR(i,1)-solpL(i,1))/2.;
        if (norm3(aL)>0.5) { // Left
          adotn  = scalProd(aL,n);
          adotuR = scalProd(aL,uR);
          adotuL = scalProd(aL,uL);
          val(i,1) = - ( adotn*(cR*cR*rhoR*adotuR + cL*cL*rhoL*adotuL)/2. - cL*qJump );
        }
        if (norm3(aR)>0.5) { // Right
          adotn  = scalProd(aR,n);
          adotuR = scalProd(aR,uR);
          adotuL = scalProd(aR,uL);
          val(i,_nbf+1) = adotn*(cR*cR*rhoR*adotuR + cL*cL*rhoL*adotuL)/2. - cR*qJump;
        }
        
      }
      
    }
  }
};

class dgConservationLawWaveExplicit::interfaceUTermPmlOld : public function {
  int _DIM, _nbf, _incidentFieldsDefined;
  double theta;
  fullMatrix<double> normals;
  fullMatrix<double> solpL, solUL, coefL, pmlDirL, incidentFieldsL;
  fullMatrix<double> solpR, solUR, coefR, pmlDirR, incidentFieldsR;
 public:
  interfaceUTermPmlOld(int DIM, int nbf,const function *p, double fluxDecentering,
                      const function *coefFunction,
                      const function *pmlDirFunction,
                      const function *incidentFieldsFunction) : function(2*nbf) {
    _DIM = DIM;
    _nbf = nbf;
    theta = fluxDecentering;
    setArgument (normals, function::getNormals());
    setArgument (solpL, p, 0);
    setArgument (solpR, p, 1);
    setArgument (solUL, function::getSolution(), 0);
    setArgument (solUR, function::getSolution(), 1);
    setArgument (coefL, coefFunction, 0);
    setArgument (coefR, coefFunction, 1);
    setArgument (pmlDirL, pmlDirFunction, 0);
    setArgument (pmlDirR, pmlDirFunction, 1);
    _incidentFieldsDefined = 0;
    if (incidentFieldsFunction!=NULL) {
      setArgument (incidentFieldsL, incidentFieldsFunction, 0);
      setArgument (incidentFieldsR, incidentFieldsFunction, 1);
      _incidentFieldsDefined = 1;
    }
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double n[3];
      for (int j=0; j<_DIM; j++)
        n[j] = normals(i,j);
      
      // Coefficients
      double cL = coefL(i,0), rhoL = coefL(i,1);
      double cR = coefR(i,0), rhoR = coefR(i,1);
      double aL[3] = {0., 0., 0.}; 
      double aR[3] = {0., 0., 0.}; 
      for (int j=0; j<_DIM; j++) {
        aL[j] += pmlDirL(i,j);
        aR[j] += pmlDirR(i,j);
      }
      double uIncL[3] = {0., 0., 0.};
      double uIncR[3] = {0., 0., 0.};
      if (_incidentFieldsDefined)
        for (int j=0; j<_DIM; j++) {
          uIncL[j] = incidentFieldsL(i,j+1);
          uIncR[j] = incidentFieldsR(i,j+1);
        }
      
      // Solution
      double pL = solpL(i,0), uL[3]={0., 0., 0.};
      double pR = solpR(i,0), uR[3]={0., 0., 0.};
      for (int j=0; j<_DIM; j++) {
        uL[j] = solUL(i,j);
        uR[j] = solUR(i,j);
      }
      double ndotuL = scalProd(n,uL);
      double ndotuR = scalProd(n,uR);
      
      // Numerical flux (with Riemann)
      double pRiemann     = ((pR/(rhoR*cR) + pL/(rhoL*cL))     - theta * (ndotuR - ndotuL))/(1./(rhoR*cR) + 1./(rhoL*cL));
      

      for (int j=0; j<_DIM; j++)
        val(i,j) = -1/rhoL * n[j] * pRiemann;
      

      for (int j=0; j<_DIM; j++)
        val(i,_nbf+j) = 1/rhoR * n[j] * pRiemann;
      
      // Numerical flux for additionnal equations
      if (_incidentFieldsDefined) {
        
        // (with Lax-Friedrich)
        double adotn, adotuScatL, adotuScatR;
        double qJump = (solUR(i,_DIM)-solUL(i,_DIM))/2.;
        if (norm3(aL)>0.5) { // Left
          adotn = scalProd(aL,n);
          adotuScatL = 0.;
          adotuScatR = 0.;
          for (int j=0; j<_DIM; j++) {
            adotuScatL += aL[j] * (uL[j]-uIncL[j]);
            adotuScatR += aL[j] * (uR[j]-uIncR[j]);
          }
          val(i,_DIM) = - ( adotn*(cR*cR*rhoR*adotuScatR + cL*cL*rhoL*adotuScatL)/2. - cL*qJump );
        }
        if (norm3(aR)>0.5) { // Right
          adotn = scalProd(aR,n);
          adotuScatL = 0.;
          adotuScatR = 0.;
          for (int j=0; j<_DIM; j++) {
            adotuScatL += aR[j] * (uL[j]-uIncL[j]);
            adotuScatR += aR[j] * (uR[j]-uIncR[j]);
          }
          val(i,_nbf+_DIM) = adotn*(cR*cR*rhoR*adotuScatR + cL*cL*rhoL*adotuScatL)/2. - cR*qJump;
        }
        
      } else {
        
        // (with Riemann)
        double adotn, adotuR, adotuL;
        double qJump = (solUR(i,_DIM)-solUL(i,_DIM))/2.;
        if (norm3(aL)>0.5) { // Left
          adotn  = scalProd(aL,n);
          adotuR = scalProd(aL,uR);
          adotuL = scalProd(aL,uL);
          val(i,_DIM) = - ( adotn*(cR*cR*rhoR*adotuR + cL*cL*rhoL*adotuL)/2. - cL*qJump );
        }
        if (norm3(aR)>0.5) { // Right
          adotn  = scalProd(aR,n);
          adotuR = scalProd(aR,uR);
          adotuL = scalProd(aR,uL);
          val(i,_nbf+_DIM) = adotn*(cR*cR*rhoR*adotuR + cL*cL*rhoL*adotuL)/2. - cR*qJump;
        }
        
      }
      
    }
  }
};
/*==============================================================================
 * CG-terms
 *============================================================================*/

class dgConservationLawWaveExplicit::volumepTermCG : public function {
  int _DIM, _nbf, _thereIsDissipationOnU;
  fullMatrix<double> solp, solpGrad,solU, solUGrad, solpPrev,solUPrev, _dt, coef, numCoef;
 public:
  volumepTermCG(int DIM, int nbf, const function *U, const function *gradU, const function *prevSolutionp, const function *prevSolutionU, int thereIsDissipationOnU,
               const function *coefFunction, const function *numCoefFunction) : function(3*nbf) {
    _DIM = DIM;
    _nbf = nbf;
    _thereIsDissipationOnU = thereIsDissipationOnU;
    setArgument (solp, function::getSolution());
    setArgument (solpGrad, function::getSolutionGradient());
    setArgument (solpPrev, prevSolutionp);
    setArgument (solU, U);
    setArgument (solUGrad, gradU);
    setArgument (solUPrev, prevSolutionU);
    setArgument (coef, coefFunction);
    setArgument (numCoef, numCoefFunction);
    setArgument (_dt, function::getDT());
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double c   = coef(i,0);
      double rho = coef(i,1);
      double sigmaU = 0;
      if (_thereIsDissipationOnU)
        sigmaU = coef(i,1);
      
      for (int j=0; j<_DIM; j++) {
        val(i,0  +j*_nbf) = c*c*rho * solU(i,j);
      }
      
      //PSPG TERMS
      double dt = _dt(i,0);
      double kappaNum = numCoef(i,0);
      if (dt > 0.)
        for (int j=0; j<_DIM; j++)
          val(i,j*_nbf) += kappaNum * ((solU(i,j)-solUPrev(i,j))/dt - 1/rho * solpGrad(i,j) + sigmaU * solU(i,j));
    }
  }
};
class dgConservationLawWaveExplicit::volumeUTermCG : public function {
  int _DIM, _nbf, _thereIsDissipationOnU;
  fullMatrix<double> solp, solpGrad, solpPrev,solU, solUGrad, solUPrev, _dt, coef, numCoef;
 public:
  volumeUTermCG(int DIM, int nbf,const function *p,const function *gradp, const function *prevSolutionp, const function *prevSolutionU, int thereIsDissipationOnU,
               const function *coefFunction, const function *numCoefFunction) : function(3*nbf) {
    _DIM = DIM;
    _nbf = nbf;
    _thereIsDissipationOnU = thereIsDissipationOnU;
    setArgument (solp, p);
    setArgument (solpGrad, gradp);
    setArgument (solpPrev, prevSolutionp);
    setArgument (solU, function::getSolution());
    setArgument (solUGrad, function::getSolutionGradient());
    setArgument (solUPrev, prevSolutionU);
    setArgument (coef, coefFunction);
    setArgument (numCoef, numCoefFunction);
    setArgument (_dt, function::getDT());
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      //double c   = coef(i,0);
      double rho = coef(i,1);
      //double sigmaU = 0;
      //if (_thereIsDissipationOnU)
      //  sigmaU = coef(i,1);
      
      for (int j=0; j<_DIM; j++) {
        val(i,j+j*_nbf) = 1/rho   * solp(i,0);
      }
    }
  }
};

/*==============================================================================
 * Conservation law : constructor - setup - destructor
 *============================================================================*/

int dgConservationLawWaveExplicit_nbf(std::string EquationVersion,std::string LayerVersion, int DIM) {
 if(EquationVersion=="p"){  
  if (LayerVersion=="WithoutLayer")                                            // Without layer
    return (1);
  else if (LayerVersion=="BasicLayer")                                         // Basic layer
    return (1);
  else if ((LayerVersion=="PmlStraight") ||
           (LayerVersion=="PmlStraightOld"))                                   // Straight PML (0 curvature)
    return (1)+1;
  else if ((LayerVersion=="PmlBeveledCuboid") ||
           (LayerVersion=="PmlCircle") ||
           (LayerVersion=="PmlCircleOld") ||
           (LayerVersion=="PmlEllipse") ||
           (LayerVersion=="PmlEllipseOld")) {                                  // BeveledCuboid/Circular/Elliptical PML (1 curvature)
    if (DIM!=2) Msg::Error("Dg/Wave: With 'PmlCircle' and 'PmlEllipse', DIM must be 2.");
    return (1)+1;
  }
  else if ((LayerVersion=="PmlSphere") ||
           (LayerVersion=="PmlSphereOld")) {                                   // Spherical PML (1 curvature)
    if (DIM!=3) Msg::Fatal("Dg/Wave: With 'PmlSphere', DIM must be 3.");
    return (1)+1;
  }
  else if (LayerVersion=="PmlEllipsoide") {                                    // Ellipsoidal PML (2 curvatures)
    if (DIM!=3) Msg::Fatal("Dg/Wave: With 'PmlEllipsoide', DIM must be 3.");
    return (1)+2;
  }
  else if (LayerVersion=="PmlSL")                                              // Circular PML of Sophie Laurens
    return 2*(1);
  else
    Msg::Fatal("Dg/Wave: Bad 'LayerVersion'.");
  return -1;
  }
 else if(EquationVersion=="U"){  
  if (LayerVersion=="WithoutLayer")                                            // Without layer
    return (DIM);
  else if (LayerVersion=="BasicLayer")                                         // Basic layer
    return (DIM);
  else if ((LayerVersion=="PmlStraight") ||
           (LayerVersion=="PmlStraightOld"))                                   // Straight PML (0 curvature)
    return (DIM)+1;
  else if ((LayerVersion=="PmlBeveledCuboid") ||
           (LayerVersion=="PmlCircle") ||
           (LayerVersion=="PmlCircleOld") ||
           (LayerVersion=="PmlEllipse") ||
           (LayerVersion=="PmlEllipseOld")) {                                  // BeveledCuboid/Circular/Elliptical PML (1 curvature)
    if (DIM!=2) Msg::Error("Dg/Wave: With 'PmlCircle' and 'PmlEllipse', DIM must be 2.");
    return (DIM)+1;
  }
  else if ((LayerVersion=="PmlSphere") ||
           (LayerVersion=="PmlSphereOld")) {                                   // Spherical PML (1 curvature)
    if (DIM!=3) Msg::Fatal("Dg/Wave: With 'PmlSphere', DIM must be 3.");
    return (DIM)+1;
  }
  else if (LayerVersion=="PmlEllipsoide") {                                    // Ellipsoidal PML (2 curvatures)
    if (DIM!=3) Msg::Fatal("Dg/Wave: With 'PmlEllipsoide', DIM must be 3.");
    return (DIM)+2;
  }
  else if (LayerVersion=="PmlSL")                                              // Circular PML of Sophie Laurens
    return 2*(DIM);
  else
    Msg::Fatal("Dg/Wave: Bad 'LayerVersion'.");
  return -1;
  }
 else
  Msg::Fatal("Dg/Wave: Bad 'LayerVersion'.");
 return -1;
}

dgConservationLawWaveExplicit::dgConservationLawWaveExplicit(std::string EquationVersion, int DIM, std::string LayerVersion) :
    dgConservationLawFunction(dgConservationLawWaveExplicit_nbf(EquationVersion,LayerVersion,DIM))
{
  _DIM = DIM;
  _NumMethod = 1;
  _EquationVersion = EquationVersion;
  _LayerVersion = LayerVersion;
  _thereIsDissipationOnP = 0;
  _thereIsDissipationOnU = 0;
  _thereIsShieldOnP = 0;
  _thereIsShieldOnU = 0;
  _fluxDecentering = 1.;
  addToTagList("");
  _pmlCoef = NULL;
  _pmlDir = NULL;
  setPmlCoef(0);
  setPmlDir(0);
  _p=0;
  _U=0;
}

void dgConservationLawWaveExplicit::setup() {
 if(_EquationVersion=="p")
  switch (_NumMethod) {
    case 1: // DG
      for(std::vector<std::string>::iterator it=_tagList.begin(); it!=_tagList.end(); ++it) {
        const std::string tag = *it;
                                          
        _volumeTerm0[tag]            = new sourcepTerm(_DIM,_nbf,_thereIsDissipationOnP,getPhysCoef(tag),NULL);
        _volumeTerm1[tag]            = new volumepTerm(_DIM,_nbf,getU(),getPhysCoef(tag));
        _interfaceTerm0[tag]         = new interfacepTerm(_DIM,_nbf,getU(),getFluxDecentering(),getPhysCoef(tag));
        _maximumConvectiveSpeed[tag] = new maxConvectiveSpeed(getPhysCoef(tag));
      }
      break;
    case 2: // CG
      for(std::vector<std::string>::iterator it=_tagList.begin(); it!=_tagList.end(); ++it) {
        const std::string tag = *it;
        _volumeTerm0[tag]            = new sourcepTerm(_DIM,_nbf,_thereIsDissipationOnP,getPhysCoef(tag),NULL);
        _volumeTerm1[tag]            = new volumepTermCG(_DIM,_nbf,getU(),getgradU(),getPrevSolutionp(),getPrevSolutionU(),_thereIsDissipationOnU,getPhysCoef(tag),getNumCoef(tag));
        _maximumConvectiveSpeed[tag] = new maxConvectiveSpeed(getPhysCoef(tag));
      }
      break;
  }
  else if(_EquationVersion=="U")
  switch (_NumMethod) {
    case 1: // DG
      for(std::vector<std::string>::iterator it=_tagList.begin(); it!=_tagList.end(); ++it) {
        const std::string tag = *it;
        _volumeTerm0[tag]            = new sourceUTerm(_DIM,_nbf,_thereIsDissipationOnU,getPhysCoef(tag),NULL);
        _volumeTerm1[tag]            = new volumeUTerm(_DIM,_nbf,getp(),getPhysCoef(tag));
        _interfaceTerm0[tag]         = new interfaceUTerm(_DIM,_nbf,getp(),getFluxDecentering(),getPhysCoef(tag));
        _maximumConvectiveSpeed[tag] = new maxConvectiveSpeed(getPhysCoef(tag));
      }
      break;
    case 2: // CG
      for(std::vector<std::string>::iterator it=_tagList.begin(); it!=_tagList.end(); ++it) {
        const std::string tag = *it;
        _volumeTerm0[tag]            = new sourceUTerm(_DIM,_nbf,_thereIsDissipationOnU,getPhysCoef(tag),NULL);
        _volumeTerm1[tag]            = new volumeUTermCG(_DIM,_nbf,getp(),getgradp(),getPrevSolutionp(),getPrevSolutionU(),_thereIsDissipationOnU,getPhysCoef(tag),getNumCoef(tag));
        _maximumConvectiveSpeed[tag] = new maxConvectiveSpeed(getPhysCoef(tag));
      }
      break;
  }
  else
    Msg::Fatal("Dg/Wave: bad EquationVersion (not '%s')", _EquationVersion.c_str());
}

dgConservationLawWaveExplicit::~dgConservationLawWaveExplicit () {
  if (_pmlCoef) delete _pmlCoef;
  if (_pmlDir)  delete _pmlDir;
  for (termMap::const_iterator it=_volumeTerm0.begin(); it!=_volumeTerm0.end(); it++)
    if (_volumeTerm0[it->first]) delete _volumeTerm0[it->first];
  for (termMap::const_iterator it=_volumeTerm1.begin(); it!=_volumeTerm1.end(); it++)
    if (_volumeTerm1[it->first]) delete _volumeTerm1[it->first];
  for (termMap::const_iterator it=_interfaceTerm0.begin(); it!=_interfaceTerm0.end(); it++)
    if (_interfaceTerm0[it->first]) delete _interfaceTerm0[it->first];
  for (termMap::const_iterator it=_maximumConvectiveSpeed.begin(); it!=_maximumConvectiveSpeed.end(); it++)
    if (_maximumConvectiveSpeed[it->first]) delete _maximumConvectiveSpeed[it->first];
}


/*==============================================================================
 * Coefficients : set - get
 *============================================================================*/
const function *dgConservationLawWaveExplicit::getp() {
  if (_EquationVersion=="p")
    Msg::Fatal("Dg/WaveExplicit: getp() cannot be used with EquationVersion='p'.");
  else if ((_EquationVersion=="U") && (_p==0))
    Msg::Fatal("Dg/WaveExplicit: getp() doesn't work: p is empty.");
  else 
    return _p;
  return NULL;
}

const function *dgConservationLawWaveExplicit::getU() {
  if (_EquationVersion=="U")
    Msg::Fatal("Dg/WaveExplicit: getU() cannot be used with EquationVersion='U'.");
  else if ((_EquationVersion=="p") && (_U==0))
    Msg::Fatal("Dg/WaveExplicit: getU() doesn't work: U is empty.");
  else 
    return _U;
  return NULL;
}
const function *dgConservationLawWaveExplicit::getgradp() {
  if (_EquationVersion=="p")
    Msg::Fatal("Dg/WaveExplicit: getp() cannot be used with EquationVersion='p'.");
  else if ((_EquationVersion=="U") && (_gradp==0))
    Msg::Fatal("Dg/WaveExplicit: getp() doesn't work: p is empty.");
  else 
    return _gradp;
  return NULL;
}

const function *dgConservationLawWaveExplicit::getgradU() {
  if (_EquationVersion=="U")
    Msg::Fatal("Dg/WaveExplicit: getU() cannot be used with EquationVersion='U'.");
  else if ((_EquationVersion=="p") && (_gradU==0))
    Msg::Fatal("Dg/WaveExplicit: getU() doesn't work: U is empty.");
  else 
    return _gradU;
  return NULL;
}
int dgConservationLawWaveExplicit::addToTagList(const std::string tag) {
  if (isInTagList(tag))
    return 0; // 'tag' is already in 'tagList'
  else {
    _tagList.push_back(tag);
    return 1; // 'tag' is added in 'tagList'
  }
}

int dgConservationLawWaveExplicit::isInTagList(const std::string tag) {
  for(std::vector<std::string>::iterator it=_tagList.begin(); it!=_tagList.end(); ++it)
    if (*it == tag)
      return 1; // 'tag' is in 'tagList'
  return 0;     // 'tag' is not in 'tagList'
}

const function *dgConservationLawWaveExplicit::getFunctionForTag2(const termMap &map, const std::string tag) const {
  termMap::const_iterator it = map.find(tag);
  if (it == map.end())
    it = map.find("");
  if (it == map.end())
    Msg::Fatal("Dg/Wave: a parameter is not defined.");
  return it->second;
}


/*==============================================================================
 * Add a PML
 *============================================================================*/

void dgConservationLawWaveExplicit::addPml(const std::string tag, const function *incidentFieldsFunction) {
  if (_EquationVersion=="p") {
  if (_LayerVersion=="BasicLayer") {
      delete _volumeTerm0[tag];
      _volumeTerm0[tag] = new sourcepTerm(_DIM,_nbf,_thereIsDissipationOnP,getPhysCoef(tag),getPmlCoef(tag));
  } else if ((_LayerVersion=="PmlStraightOld") ||
             (_LayerVersion=="PmlCircleOld") ||
             (_LayerVersion=="PmlEllipseOld") ||
             (_LayerVersion=="PmlSphereOld")) {
      delete _volumeTerm0[tag];
      delete _volumeTerm1[tag];
      delete _interfaceTerm0[tag];
      _volumeTerm0[tag]    = new sourcepTermPmlOld(_DIM,_nbf,getU(),_LayerVersion,getPhysCoef(tag),getPmlCoef(tag),getPmlDir(tag),incidentFieldsFunction);
      _volumeTerm1[tag]    = new volumepTermPmlOld(_DIM,_nbf,getU(),getPhysCoef(tag),getPmlCoef(tag),getPmlDir(tag),incidentFieldsFunction);
    } else if ((_LayerVersion=="PmlStraight") ||
             (_LayerVersion=="PmlBeveledCuboid") ||
             (_LayerVersion=="PmlCircle") ||
             (_LayerVersion=="PmlEllipse") ||
             (_LayerVersion=="PmlSphere") ||
             (_LayerVersion=="PmlEllipsoide")) {
      delete _volumeTerm0[tag];
      delete _volumeTerm1[tag];
      delete _interfaceTerm0[tag];
      _volumeTerm0[tag]    = new sourcepTermPml(_DIM,_nbf,getU(),_LayerVersion,getPhysCoef(tag),getPmlCoef(tag),getPmlDir(tag));
      _volumeTerm1[tag]    = new volumepTermPml(_DIM,_nbf,getU(),_LayerVersion,getPhysCoef(tag),getPmlCoef(tag),getPmlDir(tag));
      _interfaceTerm0[tag] = new interfacepTermPml(_DIM,_nbf,getU(),_LayerVersion,getFluxDecentering(),getPhysCoef(tag),getPmlDir(tag));
  } else if (_LayerVersion=="PmlSL") {
      delete _volumeTerm0[tag];
      _volumeTerm0[tag]    = new sourcepTermPml(_DIM,_nbf,getU(),_LayerVersion,getPhysCoef(tag),getPmlCoef(tag),getPmlDir(tag));
  } else
      Msg::Fatal("Dg/Wave: Bad 'LayerVersion'.");
  }
  else if (_EquationVersion=="U") {
  if (_LayerVersion=="BasicLayer") {
      delete _volumeTerm0[tag];
      _volumeTerm0[tag] = new sourceUTerm(_DIM,_nbf,_thereIsDissipationOnU,getPhysCoef(tag),getPmlCoef(tag));
  } else if ((_LayerVersion=="PmlStraightOld") ||
             (_LayerVersion=="PmlCircleOld") ||
             (_LayerVersion=="PmlEllipseOld") ||
             (_LayerVersion=="PmlSphereOld")) {
      delete _volumeTerm0[tag];
      delete _volumeTerm1[tag];
      delete _interfaceTerm0[tag];
      _volumeTerm0[tag]    = new sourceUTermPmlOld(_DIM,_nbf,getp(),_LayerVersion,getPhysCoef(tag),getPmlCoef(tag),getPmlDir(tag),incidentFieldsFunction);
      _volumeTerm1[tag]    = new volumeUTermPmlOld(_DIM,_nbf,getp(),getPhysCoef(tag),getPmlCoef(tag),getPmlDir(tag),incidentFieldsFunction);
      _interfaceTerm0[tag] = new interfaceUTermPmlOld(_DIM,_nbf,getp(),getFluxDecentering(),getPhysCoef(tag),getPmlDir(tag),incidentFieldsFunction);
  } else if ((_LayerVersion=="PmlStraight") ||
             (_LayerVersion=="PmlBeveledCuboid") ||
             (_LayerVersion=="PmlCircle") ||
             (_LayerVersion=="PmlEllipse") ||
             (_LayerVersion=="PmlSphere") ||
             (_LayerVersion=="PmlEllipsoide")) {
      delete _volumeTerm0[tag];
      delete _volumeTerm1[tag];
      delete _interfaceTerm0[tag];
      _volumeTerm0[tag]    = new sourceUTermPml(_DIM,_nbf,getp(),_LayerVersion,getPhysCoef(tag),getPmlCoef(tag),getPmlDir(tag));
      _volumeTerm1[tag]    = new volumeUTermPml(_DIM,_nbf,getp(),_LayerVersion,getPhysCoef(tag),getPmlCoef(tag),getPmlDir(tag));
      _interfaceTerm0[tag] = new interfaceUTermPml(_DIM,_nbf,getp(),_LayerVersion,getFluxDecentering(),getPhysCoef(tag),getPmlDir(tag));
  } else if (_LayerVersion=="PmlSL") {
      delete _volumeTerm0[tag];
      _volumeTerm0[tag]    = new sourceUTermPml(_DIM,_nbf,getp(),_LayerVersion,getPhysCoef(tag),getPmlCoef(tag),getPmlDir(tag));
  } else
      Msg::Fatal("Dg/Wave: Bad 'LayerVersion'.");
  }
    else
    Msg::Fatal("Dg/WaveExplicit: bad LayerVersion (not '%s')", _LayerVersion.c_str());
}

class dgConservationLawWaveExplicit::genericPmlCoefSphere : public function {
  int DIM;
  double lPml, c, center[3], radius;
  fullMatrix<double> xyz;
 public:
  genericPmlCoefSphere(int _DIM, double _lPml, double _c,
                       double _center[3], double _radius) : function(3) {
    DIM = _DIM;
    lPml = _lPml;
    c = _c;
    for (int j=0; j<3; j++)
      center[j] = _center[j];
    radius = _radius;
    setArgument (xyz, function::getCoordinates());
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      double r = 0.;
      for (int j=0; j<3; j++)
        r += pow(xyz(i,j) - center[j],2);
      r = sqrt(r);
      
      double h = r-radius;
      if (h>lPml) h=lPml;
      
      val(i,0) = c/lPml * h/(lPml-h);
      val(i,1) = c * (-log(1-h/lPml) - h/lPml);
      val(i,2) = 1/r;
      
    }
  }
};

class dgConservationLawWaveExplicit::genericPmlCoefEllipsoide : public function {
  int DIM;
  double lPml, c, center[3], ellX, ellY, ellZ;
  fullMatrix<double> xyz;
 public:
  genericPmlCoefEllipsoide(int _DIM, double _lPml, double _c,
                           double _center[3], double semiAxis[3]) : function(_DIM+1) {
    DIM = _DIM;
    lPml = _lPml;
    c = _c;
    ellX = semiAxis[0];
    if (semiAxis[1]<0) ellY = ellX;
    else ellY = semiAxis[1];
    if (semiAxis[2]<0) ellZ = ellY;
    else Msg::Fatal("Dg/Wave: useGenericPmlDir() can't yet be used with general ellipsoide.");  // if 3D, ellipsoide of revolution
    for (int j=0; j<3; j++)
      center[j] = _center[j];
    setArgument (xyz, function::getCoordinates());
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      double h=0.;
      
#if defined (HAVE_POST)
      double x[3] = {xyz(i,0),xyz(i,1),xyz(i,2)};
      PView *d = PView::list[0];
      d->getData()->searchScalar(x[0],x[1],x[2],&h);
#endif
      double rCurvT = 0.;
      double rCurvB = 0.;
      
      double aN[3] = {0., 0., 0.};
#if defined (HAVE_POST)
      PView *d1 = PView::list[1];
      d1->getData()->searchVector(x[0],x[1],x[2],&aN[0]);
#endif
      
      double p[3];
      for (int j=0; j<3; j++)
        p[j] = xyz(i,j) - center[j] - h*aN[j];
      
      rCurvT = pow(pow(ellY*p[0]/ellX,2.) + pow(ellX*sqrt(pow(p[1],2)+pow(p[2],2))/ellY,2.),1.5) / (ellX*ellY);
      
      if (DIM==3) {
        double gaussCurv = pow(ellX*ellY*ellZ,2) / pow(pow(p[0]*ellY*ellZ/ellX,2) +
                                                       pow(p[1]*ellX*ellZ/ellY,2) +
                                                       pow(p[2]*ellX*ellY/ellZ,2) ,2);  // For ellipsoide
        rCurvB = 1/(gaussCurv*rCurvT);
      }
      
      if (h>lPml) h=lPml;
      
      val(i,0) = c/lPml * h/(lPml-h);
      val(i,1) = c * (-log(1-h/lPml) - h/lPml);
      val(i,2) = 1/(rCurvT+h);
      if (DIM == 3)
        val(i,3) = 1/(rCurvB+h);

      
    }
  }
};

class dgConservationLawWaveExplicit::genericPmlCoefBeveledCuboid : public function {
  int _DIM;
  std::string _pmlCoefFuncVersion;
  double _lPml, _c, _center[3], _semiAxis[3], _radius, _pmlCoefFuncMulti;
  fullMatrix<double> xyz;
 public:
  genericPmlCoefBeveledCuboid(int DIM, double lPml, double c,
                              double center[3], double semiAxis[3], double radius,
                              const std::string pmlCoefFuncVersion, double pmlCoefFuncMulti) : function(3) {
    _DIM = DIM;
    _lPml = lPml;
    _c = c;
    for (int j=0; j<3; j++) {
      _center[j] = center[j];
      _semiAxis[j] = semiAxis[j];
    }
    _radius = radius;
    _pmlCoefFuncVersion = pmlCoefFuncVersion;
    _pmlCoefFuncMulti = pmlCoefFuncMulti;
    setArgument (xyz, function::getCoordinates());
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      double xyzloc[3] = {0,0,0};
      for (int j=0; j<_DIM; j++)
        xyzloc[j] = fabs(xyz(i,j)-_center[j]);
      
      double corner[3] = {0,0,0};
      for (int j=0; j<_DIM; j++)
        corner[j] = _semiAxis[j]-_radius;
      
      double h=0.;
      double curv=0.;
      
      if ((xyzloc[0]>=corner[0]) && (xyzloc[1]<=corner[1]))             // Prévu pour du 2D
        h = xyzloc[0]-_semiAxis[0];
      else if ((xyzloc[0]<=corner[0]) && (xyzloc[1]>=corner[1]))
        h = xyzloc[1]-_semiAxis[1];
      else if ((xyzloc[0]>=corner[0]) && (xyzloc[1]>=corner[1])) {
        for (int j=0; j<_DIM; j++)
          h += pow(xyzloc[j]-corner[j],2);
        h = sqrt(h);
        if (h>_radius/2)
          curv = 1/h;
        h -= _radius;
      }
      
      if (h<0.) h=0.;
      else if (h>_lPml) h=_lPml;
      
      if (_pmlCoefFuncVersion=="Poly0") {
        val(i,0) = _pmlCoefFuncMulti;
        val(i,1) = _pmlCoefFuncMulti * h;
      }
      else if (_pmlCoefFuncVersion=="Poly1") {
        val(i,0) = _pmlCoefFuncMulti * (h/_lPml);
        val(i,1) = _pmlCoefFuncMulti * (h/_lPml) * (h/2);
      }
      else if (_pmlCoefFuncVersion=="Poly2") {
        val(i,0) = _pmlCoefFuncMulti * pow(h/_lPml,2);
        val(i,1) = _pmlCoefFuncMulti * pow(h/_lPml,2) * (h/3);
      }
      else if (_pmlCoefFuncVersion=="Poly3") {
        val(i,0) = _pmlCoefFuncMulti * pow(h/_lPml,3);
        val(i,1) = _pmlCoefFuncMulti * pow(h/_lPml,3) * (h/4);
      }
      else if (_pmlCoefFuncVersion=="Hyp") {
        val(i,0) = _pmlCoefFuncMulti * _c * 1/(_lPml-h);
        val(i,1) = _pmlCoefFuncMulti * _c * (-log(1-h/_lPml));
      }
      else if (_pmlCoefFuncVersion=="HypSh") {
        val(i,0) = _pmlCoefFuncMulti * _c/_lPml * h/(_lPml-h);
        val(i,1) = _pmlCoefFuncMulti * _c * (-log(1-h/_lPml) - h/_lPml);
      }
      else
        Msg::Fatal("Dg/Wave: useGenericPmlCoef() not implemented for this 'pmlCoefFuncVersion'.");
      
      val(i,2) = curv;
      
    }
  }
};

class dgConservationLawWaveExplicit::genericPmlDirSphere : public function {
  int DIM;
  double center[3];
  fullMatrix<double> xyz;
 public:
  genericPmlDirSphere(int _DIM, double _center[3]) : function(3) {
    DIM = _DIM;
    for (int j=0; j<3; j++)
      center[j] = _center[j];
    setArgument (xyz, function::getCoordinates());
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      double aN[3] = {0.,0.,0.};
      for (int j=0; j<3; j++)
        aN[j] = xyz(i,j) - center[j];
      norme(aN);
      
      for (int j=0; j<3; j++)
        val(i,j) = aN[j];
      
    }
  }
};

class dgConservationLawWaveExplicit::genericPmlDirEllipsoide : public function {
  int DIM;
  double center[3];
  fullMatrix<double> xyz;
 public:
  genericPmlDirEllipsoide(int _DIM, double _center[3]) : function(3*(_DIM)) {
    DIM = _DIM;
    for (int j=0; j<3; j++)
      center[j] = _center[j];
    setArgument (xyz, function::getCoordinates());
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      double x[3] = {xyz(i,0),xyz(i,1),xyz(i,2)};
      
      double aN[3] = {0.,0.,0.};
#if defined (HAVE_POST)
      PView *d1 = PView::list[1];
      d1->getData()->searchVector(x[0],x[1],x[2],&aN[0]);
#endif
      norme(aN);
      
      for (int j=0; j<3; j++)
        val(i,j) = aN[j];
      
      if (DIM==3) {
        
        double aT[3] = {0.,0.,0.};
        double aB[3] = {0.,0.,0.};
        
        double h = 0.;
#if defined (HAVE_POST)
        PView *d0 = PView::list[0];
        d0->getData()->searchScalar(x[0],x[1],x[2],&h);
#endif
        double p[3];
        for (int j=0; j<3; j++)
          p[j] = x[j] - center[j] - h*aN[j];
        
        aB[0] = 0;
        aB[1] = p[2];
        aB[2] = -p[1];
        norme(aB);
        
        for (int j=0; j<3; j++)
          aT[j] = crossProd(aB,aN,j);
        norme(aT);
        
        for (int j=0; j<3; j++)
          val(i,j+3) = aT[j];
        
        for (int j=0; j<3; j++)
          val(i,j+6) = aB[j];
        
      }
      
    }
  }
};

class dgConservationLawWaveExplicit::genericPmlDirBeveledCuboid : public function {
  int DIM;
  double center[3], semiAxis[3], radius;
  fullMatrix<double> xyz;
 public:
  genericPmlDirBeveledCuboid(int _DIM, double _center[3], double _semiAxis[3], double _radius) : function(3) {
    DIM = _DIM;
    for (int j=0; j<3; j++) {
      center[j] = _center[j];
      semiAxis[j] = _semiAxis[j];
    }
    radius = _radius;
    setArgument (xyz, function::getCoordinates());
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      double aN[3] = {0.,0.,0.};
      
      double xyzloc[3] = {0,0,0};
      for (int j=0; j<DIM; j++)
        xyzloc[j] = fabs(xyz(i,j)-center[j]);
      
      double corner[3] = {0,0,0};
      for (int j=0; j<DIM; j++)
        corner[j] = semiAxis[j]-radius;
      
      if ((xyzloc[0]>=corner[0]) && (xyzloc[1]<=corner[1]))             // Prévu pour du 2D
        aN[0] = 1.;
      else if ((xyzloc[0]<=corner[0]) && (xyzloc[1]>=corner[1]))
        aN[1] = 1.;
      else if ((xyzloc[0]>=corner[0]) && (xyzloc[1]>=corner[1])) {
        for (int j=0; j<DIM; j++)
          aN[j] = xyzloc[j]-corner[j];
      }
      norme(aN);
      
      for (int j=0; j<DIM; j++)
        val(i,j) = ((xyz(i,j)-center[j])>=0) ? aN[j] : (-aN[j]);
      
    }
  }
};

void dgConservationLawWaveExplicit::useGenericPmlCoef(const std::string pmlCoefVersion, double lPml, double c,
                                              double centerX, double centerY, double centerZ,
                                              double semiAxisX, double semiAxisY, double semiAxisZ, double radius,
                                              const std::string pmlCoefFuncVersion, double pmlCoefFuncMulti) {
  double center[3] = {centerX, centerY, centerZ};
  double semiAxis[3] = {semiAxisX, semiAxisY, semiAxisZ};
  if (pmlCoefVersion=="Circle")
    _pmlCoef = new genericPmlCoefSphere(2, lPml, c, center, semiAxis[0]);
  else if (pmlCoefVersion=="Ellipse")
    _pmlCoef = new genericPmlCoefEllipsoide(2, lPml, c, center, semiAxis);
  else if (pmlCoefVersion=="BeveledCuboid")
    _pmlCoef = new genericPmlCoefBeveledCuboid(2, lPml, c, center, semiAxis, radius, pmlCoefFuncVersion, pmlCoefFuncMulti);
  else if (pmlCoefVersion=="Sphere")
    _pmlCoef = new genericPmlCoefSphere(3, lPml, c, center, semiAxis[0]);
  else if (pmlCoefVersion=="Ellipsoide")
    _pmlCoef = new genericPmlCoefEllipsoide(3, lPml, c, center, semiAxis);
  else
    Msg::Fatal("Dg/Wave: useGenericPmlCoef() not implemented for this 'pmlCoefVersion'.");
  setPmlCoef(_pmlCoef);
}

void dgConservationLawWaveExplicit::useGenericPmlDir(const std::string pmlDirVersion,
                                             double centerX, double centerY, double centerZ,
                                             double semiAxisX, double semiAxisY, double semiAxisZ, double radius) {
  double center[3] = {centerX, centerY, centerZ};
  double semiAxis[3] = {semiAxisX, semiAxisY, semiAxisZ};
  if (pmlDirVersion=="Circle")
    _pmlDir = new genericPmlDirSphere(2, center);
  else if (pmlDirVersion=="Ellipse")
    _pmlDir = new genericPmlDirEllipsoide(2, center);
  else if (pmlDirVersion=="BeveledCuboid")
    _pmlDir = new genericPmlDirBeveledCuboid(2, center, semiAxis, radius);
  else if (pmlDirVersion=="Sphere")
    _pmlDir = new genericPmlDirSphere(3, center);
  else if (pmlDirVersion=="Ellipsoide")
    _pmlDir = new genericPmlDirEllipsoide(3, center);
  else
    Msg::Fatal("Dg/Wave: useGenericPmlDir() not implemented for this 'pmlDirVersion'.");
  setPmlDir(_pmlDir);
}


/*==============================================================================
 * Specific boundary conditions
 *============================================================================*/

class dgBoundaryConditionWavep : public dgBoundaryCondition {
  class term : public function {
    int _DIM, _nbf, _typeBC;
    double theta;
    std::string _LayerVersion;
    fullMatrix<double> solp, solU, normals, coef, pmlDir, extData;
   public:
    term(int DIM, int nbf, const function *U, std::string LayerVersion, int typeBC, double fluxDecentering,
         const function *coefFunction,
         const function *pmlDirFunction,
         const function *extDataFunction) : function(nbf) {
      _DIM = DIM;
      _nbf = nbf;
      _LayerVersion = LayerVersion;
      _typeBC = typeBC;
      theta = fluxDecentering;
      setArgument (solp, function::getSolution());
      setArgument (solU, U);
      setArgument (normals, function::getNormals());
      setArgument (coef, coefFunction);
      if ((_LayerVersion!="WithoutLayer") && (_LayerVersion!="BasicLayer"))
        setArgument (pmlDir, pmlDirFunction);
      if (extDataFunction != NULL)
        setArgument (extData, extDataFunction);
    }
    void call (dataCacheMap *m, fullMatrix<double> &val) {
      val.scale(0.);
      size_t nQP = val.size1();
      for(size_t i=0; i<nQP; i++) {
        
        double c   = coef(i,0);
        double rho = coef(i,1);
        
        double n[3];
        for (int j=0; j<_DIM; j++)
          n[j] = normals(i,j);
        
        // Average and jump
        double pAverage = 0.;
        double pJump = 0.;
        double ndotuAverage = 0.;
        double ndotuJump = 0.;
        
        switch (_typeBC) {
          
          // Dirichlet BC on u with external fields  (extData contains 'uExt')
          case 1:
            if (theta < 0.01) {
              pAverage = solp(i,0);
              for (int j=0; j<_DIM; j++)
                ndotuAverage += n[j]*extData(i,j);
            }
            else {
              pAverage = solp(i,0);
              pJump    = 0.;
              for (int j=0; j<_DIM; j++) {
                ndotuAverage += n[j]*(extData(i,j)+solU(i,j))/2;
                ndotuJump    += n[j]*(extData(i,j)-solU(i,j))/2;
              }
            }
            break;
          
          // Dirichlet BC on u with flux  (extData contains 'ndotuExt')
          case 2:
            if (theta < 0.01) {
              pAverage     = solp(i,0);
              ndotuAverage = extData(i,0);
            }
            else {
              pAverage = solp(i,0);
              pJump    = 0.;
              ndotuAverage = extData(i,0)/2;
              ndotuJump    = extData(i,0)/2;
              for (int j=0; j<_DIM; j++) {
                ndotuAverage += n[j]*(solU(i,j))/2;
                ndotuJump    -= n[j]*(solU(i,j))/2;
              }
            }
            break;
          
          // Homogeneous Dirichlet BC on u  (extData is EMPTY)
          case 3:
            if (theta < 0.01) {
              pAverage     = solp(i,0);
              ndotuAverage = 0.;
            }
            else {
              pAverage = solp(i,0);
              pJump    = 0.;
              for (int j=0; j<_DIM; j++) {
                ndotuAverage += n[j]*(solU(i,j))/2;
                ndotuJump    -= n[j]*(solU(i,j))/2;
              }
            }
            break;
          
          // Dirichlet BC on p with external fields  (extData contains 'pExt')
          case 4:
            if (theta < 0.01) {
              pAverage = extData(i,0);
              for (int j=0; j<_DIM; j++)
                ndotuAverage += n[j]*solU(i,j);
            }
            else {
              pAverage = (extData(i,0)+solp(i,0))/2;
              pJump    = (extData(i,0)-solp(i,0))/2;
              for (int j=0; j<_DIM; j++)
                ndotuAverage += n[j]*solU(i,j);
            }
            break;
          
          // Homogeneous Dirichlet BC on p  (extData is EMPTY)
          case 5:
            if (theta < 0.01) {
              pAverage = 0.;
              for (int j=0; j<_DIM; j++)
                ndotuAverage += n[j]*solU(i,j);
            }
            else {
              pAverage =  solp(i,0)/2;
              pJump    = -solp(i,0)/2;
              for (int j=0; j<_DIM; j++)
                ndotuAverage += n[j]*solU(i,j);
            }
            break;
            
          // Open BC (extData is EMPTY)
          case 6:
            if (theta < 0.01) {
              ndotuAverage = 1/(rho*c) * solp(i,0);
              for (int j=0; j<_DIM; j++)
                pAverage += (rho*c) * n[j]*solU(i,j);
            }
            else {
              pAverage =  solp(i,0)/2;
              pJump    = -solp(i,0)/2;
              for (int j=0; j<_DIM; j++) {
                ndotuAverage += n[j]*(solU(i,j))/2;
                ndotuJump    -= n[j]*(solU(i,j))/2;
              }
            }
            break;
            
          // Open BC with inflow (extData contains all external fields) - Sommerfeld
          case 7:
            if (theta < 0.01) {
              pAverage     = extData(i,0);
              ndotuAverage = 1/(rho*c) * (solp(i,0)-extData(i,0));
              for (int j=0; j<_DIM; j++) {
                pAverage     += (rho*c) * n[j]*(solU(i,j)-extData(i,j+1));
                ndotuAverage += n[j]*extData(i,j+1);
              }
            }
            else {
              pAverage = (extData(i,0)+solp(i,0))/2;
              pJump    = (extData(i,0)-solp(i,0))/2;
              for (int j=0; j<_DIM; j++) {
                ndotuAverage += n[j]*(extData(i,j+1)+solU(i,j))/2;
                ndotuJump    += n[j]*(extData(i,j+1)-solU(i,j))/2;
              }
            }
            break;
          
          default:
            Msg::Error("Dg/Wave: There is an error in the BOUNDARY CONDITION of the WAVE conservation law (BAD '_typeBC': '%i')", _typeBC);
          
        }
        
        // Riemann solver
        double ndotuRiemann = ndotuAverage - theta * 1/(rho*c) * pJump;
        
        
        // Numerical flux
        val(i,0) = -c*c*rho * ndotuRiemann;

        
        // Numerical flux of additionnal equation
        if ((_LayerVersion!="WithoutLayer") && (_LayerVersion!="BasicLayer")) {
          double adotn=0.;
          for (int j=0; j<_DIM; j++)
            adotn += pmlDir(i,j) * n[j];
          if (fabs(adotn)>0.5) {
            val(i,1) = -c*c*rho * ndotuRiemann;
          }
        }
        
      }
    }
  };
  term _term;
 public:
  dgBoundaryConditionWavep(dgConservationLawWaveExplicit *claw, const std::string tag, int typeBC, const function *extDataFunction) :
      _term(claw->getDIM(),
            claw->getNbFields(),
            claw->getU(),
            claw->getLayerVersion(),
            typeBC,
            claw->getFluxDecentering(),
            claw->getPhysCoef(tag),
            claw->getPmlDir(tag),
            extDataFunction) {
    _term0 = &_term;
  }
};

class dgBoundaryConditionWaveU : public dgBoundaryCondition {
  class term : public function {
    int _DIM, _nbf, _typeBC;
    double theta;
    std::string _LayerVersion;
    fullMatrix<double> solp, solU, normals, coef, pmlDir, extData;
   public:
    term(int DIM, int nbf,const function *p, std::string LayerVersion, int typeBC, double fluxDecentering,
         const function *coefFunction,
         const function *pmlDirFunction,
         const function *extDataFunction) : function(nbf) {
      _DIM = DIM;
      _nbf = nbf;
      _LayerVersion = LayerVersion;
      _typeBC = typeBC;
      theta = fluxDecentering;
      setArgument (solp, p);
      setArgument (solU, function::getSolution());
      setArgument (normals, function::getNormals());
      setArgument (coef, coefFunction);
      if ((_LayerVersion!="WithoutLayer") && (_LayerVersion!="BasicLayer"))
        setArgument (pmlDir, pmlDirFunction);
      if (extDataFunction != NULL)
        setArgument (extData, extDataFunction);
    }
    void call (dataCacheMap *m, fullMatrix<double> &val) {
      val.scale(0.);
      size_t nQP = val.size1();
      for(size_t i=0; i<nQP; i++) {
        
        double c   = coef(i,0);
        double rho = coef(i,1);
        
        double n[3];
        for (int j=0; j<_DIM; j++)
          n[j] = normals(i,j);
        
        // Average and jump
        double pAverage = 0.;
        double pJump = 0.;
        double ndotuAverage = 0.;
        double ndotuJump = 0.;
        
        switch (_typeBC) {
          
          // Dirichlet BC on u with external fields  (extData contains 'uExt')
          case 1:
            if (theta < 0.01) {
              pAverage = solp(i,0);
              for (int j=0; j<_DIM; j++)
                ndotuAverage += n[j]*extData(i,j);
            }
            else {
              pAverage = solp(i,0);
              pJump    = 0.;
              for (int j=0; j<_DIM; j++) {
                ndotuAverage += n[j]*(extData(i,j)+solU(i,j))/2;
                ndotuJump    += n[j]*(extData(i,j)-solU(i,j))/2;
              }
            }
            break;
          
          // Dirichlet BC on u with flux  (extData contains 'ndotuExt')
          case 2:
            if (theta < 0.01) {
              pAverage     = solp(i,0);
              ndotuAverage = extData(i,0);
            }
            else {
              pAverage = solp(i,0);
              pJump    = 0.;
              ndotuAverage = extData(i,0)/2;
              ndotuJump    = extData(i,0)/2;
              for (int j=0; j<_DIM; j++) {
                ndotuAverage += n[j]*(solU(i,j))/2;
                ndotuJump    -= n[j]*(solU(i,j))/2;
              }
            }
            break;
          
          // Homogeneous Dirichlet BC on u  (extData is EMPTY)
          case 3:
            if (theta < 0.01) {
              pAverage     = solp(i,0);
              ndotuAverage = 0.;
            }
            else {
              pAverage = solp(i,0);
              pJump    = 0.;
              for (int j=0; j<_DIM; j++) {
                ndotuAverage += n[j]*(solU(i,j))/2;
                ndotuJump    -= n[j]*(solU(i,j))/2;
              }
            }
            break;
          
          // Dirichlet BC on p with external fields  (extData contains 'pExt')
          case 4:
            if (theta < 0.01) {
              pAverage = extData(i,0);
              for (int j=0; j<_DIM; j++)
                ndotuAverage += n[j]*solU(i,j);
            }
            else {
              pAverage = (extData(i,0)+solp(i,0))/2;
              pJump    = (extData(i,0)-solp(i,0))/2;
              for (int j=0; j<_DIM; j++)
                ndotuAverage += n[j]*solU(i,j);
            }
            break;
          
          // Homogeneous Dirichlet BC on p  (extData is EMPTY)
          case 5:
            if (theta < 0.01) {
              pAverage = 0.;
              for (int j=0; j<_DIM; j++)
                ndotuAverage += n[j]*solU(i,j);
            }
            else {
              pAverage =  solp(i,0)/2;
              pJump    = -solp(i,0)/2;
              for (int j=0; j<_DIM; j++)
                ndotuAverage += n[j]*solU(i,j);
            }
            break;
            
          // Open BC (extData is EMPTY)
          case 6:
            if (theta < 0.01) {
              ndotuAverage = 1/(rho*c) * solp(i,0);
              for (int j=0; j<_DIM; j++)
                pAverage += (rho*c) * n[j]*solU(i,j);
            }
            else {
              pAverage =  solp(i,0)/2;
              pJump    = -solp(i,0)/2;
              for (int j=0; j<_DIM; j++) {
                ndotuAverage += n[j]*(solU(i,j))/2;
                ndotuJump    -= n[j]*(solU(i,j))/2;
              }
            }
            break;
            
          // Open BC with inflow (extData contains all external fields) - Sommerfeld
          case 7:
            if (theta < 0.01) {
              pAverage     = extData(i,0);
              ndotuAverage = 1/(rho*c) * (solp(i,0)-extData(i,0));
              for (int j=0; j<_DIM; j++) {
                pAverage     += (rho*c) * n[j]*(solU(i,j)-extData(i,j+1));
                ndotuAverage += n[j]*extData(i,j+1);
              }
            }
            else {
              pAverage = (extData(i,0)+solp(i,0))/2;
              pJump    = (extData(i,0)-solp(i,0))/2;
              for (int j=0; j<_DIM; j++) {
                ndotuAverage += n[j]*(extData(i,j+1)+solU(i,j))/2;
                ndotuJump    += n[j]*(extData(i,j+1)-solU(i,j))/2;
              }
            }
            break;
          
          default:
            Msg::Error("Dg/Wave: There is an error in the BOUNDARY CONDITION of the WAVE conservation law (BAD '_typeBC': '%i')", _typeBC);
          
        }
        
        // Riemann solver
        double pRiemann     = pAverage     - theta * rho*c * ndotuJump;
        double ndotuRiemann = ndotuAverage - theta * 1/(rho*c) * pJump;
        
        
        // Numerical flux
        for (int j=0; j<_DIM; j++)
          val(i,j)  = -1/rho * n[j]*pRiemann;
        
        // Numerical flux of additionnal equation
        if ((_LayerVersion!="WithoutLayer") && (_LayerVersion!="BasicLayer")) {
          double adotn=0.;
          for (int j=0; j<_DIM; j++)
            adotn += pmlDir(i,j) * n[j];
          if (fabs(adotn)>0.5) {
            val(i,_DIM) = -c*c*rho * ndotuRiemann;
          }
        }
        
      }
    }
  };
  term _term;
 public:
  dgBoundaryConditionWaveU(dgConservationLawWaveExplicit *claw, const std::string tag, int typeBC, const function *extDataFunction) :
      _term(claw->getDIM(),
            claw->getNbFields(),
            claw->getp(),          
            claw->getLayerVersion(),
            typeBC,
            claw->getFluxDecentering(),
            claw->getPhysCoef(tag),
            claw->getPmlDir(tag),
            extDataFunction) {
    _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawWaveExplicit::newBoundaryDirichletOnU(const std::string tag, const function *uExtFunction) {
  if (_EquationVersion=="p")
  return new dgBoundaryConditionWavep(this,tag,1,uExtFunction);
  else if (_EquationVersion=="U")
  return new dgBoundaryConditionWaveU(this,tag,1,uExtFunction);
  return NULL;
}

dgBoundaryCondition *dgConservationLawWaveExplicit::newBoundaryFluxU(const std::string tag, const function *ndotuExtFunction) {
  if (_EquationVersion=="p")
  return new dgBoundaryConditionWavep(this,tag,2,ndotuExtFunction);
  else if (_EquationVersion=="U")
  return new dgBoundaryConditionWaveU(this,tag,2,ndotuExtFunction);
  return NULL;
}

dgBoundaryCondition *dgConservationLawWaveExplicit::newBoundaryHomogeneousDirichletOnU(const std::string tag) {
  if (_EquationVersion=="p")
  return new dgBoundaryConditionWavep(this,tag,3,NULL);
  else if (_EquationVersion=="U")
  return new dgBoundaryConditionWaveU(this,tag,3,NULL);
  return NULL;
}

dgBoundaryCondition *dgConservationLawWaveExplicit::newBoundaryWall(const std::string tag) {
  if (_EquationVersion=="p")
  return new dgBoundaryConditionWavep(this,tag,3,NULL);
  else if (_EquationVersion=="U")
  return new dgBoundaryConditionWaveU(this,tag,3,NULL);
  return NULL;
}

dgBoundaryCondition *dgConservationLawWaveExplicit::newBoundaryDirichletOnP(const std::string tag, const function *pExtFunction) {
  if (_EquationVersion=="p")
  return new dgBoundaryConditionWavep(this,tag,4,pExtFunction);
  else if (_EquationVersion=="U")
  return new dgBoundaryConditionWaveU(this,tag,4,pExtFunction);
  return NULL;
}

dgBoundaryCondition *dgConservationLawWaveExplicit::newBoundaryHomogeneousDirichletOnP(const std::string tag) {
  if (_EquationVersion=="p")
  return new dgBoundaryConditionWavep(this,tag,5,NULL);
  else if (_EquationVersion=="U")
  return new dgBoundaryConditionWaveU(this,tag,5,NULL);
  return NULL;
}

dgBoundaryCondition *dgConservationLawWaveExplicit::newBoundaryOpenOutflow(const std::string tag) {
  if (_EquationVersion=="p")
  return new dgBoundaryConditionWavep(this,tag,6,NULL);
  else if (_EquationVersion=="U")
  return new dgBoundaryConditionWaveU(this,tag,6,NULL);
  return NULL;
}

dgBoundaryCondition *dgConservationLawWaveExplicit::newBoundaryOpenOutflowWithInflow(const std::string tag, const function *fieldsExtFunction) {
  if (_EquationVersion=="p")
  return new dgBoundaryConditionWavep(this,tag,7,fieldsExtFunction);
  else if (_EquationVersion=="U")
  return new dgBoundaryConditionWaveU(this,tag,7,fieldsExtFunction);
  return NULL;
}


/*==============================================================================
 * Specific interface conditions
 *============================================================================*/

// Interface term for the interface domain/shield

class dgConservationLawWaveExplicit::interfacepShieldTerm : public function {
  int _DIM, _nbf, _thereIsShieldOnP, _thereIsShieldOnU;
  double theta;
  fullMatrix<double> normals, solpL, solpR, solUL, solUR, coefL, coefR,coef;
 public:
  interfacepShieldTerm(int DIM, int nbf,const function *U,int thereIsShieldOnP,int thereIsShieldOnU, double fluxDecentering, const function *coefFunction) : function(2*nbf) {
    _DIM = DIM;
    _nbf = nbf;
    theta = fluxDecentering;
    _thereIsShieldOnP = thereIsShieldOnP;
    _thereIsShieldOnU = thereIsShieldOnU;
    setArgument (normals, function::getNormals());
    setArgument (solpL, function::getSolution(), 0);
    setArgument (solpR, function::getSolution(), 1);
    setArgument (solUL, U, 0);
    setArgument (solUR, U, 1);
    setArgument (coefL, coefFunction, 0);
    setArgument (coefR, coefFunction, 1);
    setArgument (coef, coefFunction);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    
    for(size_t i=0; i<nQP; i++) {
      
      double n[3];
      for (int j=0; j<_DIM; j++)
        n[j] = normals(i,j);
      
      // Coefficients
      double cL = coefL(i,0), rhoL = coefL(i,1);
      double cR = coefR(i,0), rhoR = coefR(i,1);
      double zL = rhoL*cL ,zR = rhoR*cR;
      double zS=coefL(i,2);
      double ndotuRiemannL = 0., ndotuRiemannR = 0.;

       
      // Solution
      double pL = solpL(i,0), ndotuL = 0;
      double pR = solpR(i,0), ndotuR = 0;
      for (int j=0; j<_DIM; j++) {
        ndotuL += n[j] * solUL(i,j);
        ndotuR += n[j] * solUR(i,j);
      }

      if(_thereIsShieldOnP){
        ndotuRiemannL = 2*ndotuL*zL/(zR + zL) ;
        ndotuRiemannR = 2*ndotuR*zR/(zL + zL) ;
      }
      if(_thereIsShieldOnU){
        ndotuRiemannL = ndotuL*zL/(zR + zL)-(pR - pL)/(1./zS)/2; ;
        ndotuRiemannR = ndotuR*zR/(zL + zL)-(pR - pL)/(1./zS)/2; ;
      }
      



      // Fluxes for the left cell
      val(i,0) = -cL*cL*rhoL * ndotuRiemannL;

      
      // Fluxes for the right cell
      val(i,_nbf) = cR*cR*rhoR * ndotuRiemannR;

      
    }
  }
};
class dgConservationLawWaveExplicit::interfaceUShieldTerm : public function {
  int _DIM, _nbf, _thereIsShieldOnP, _thereIsShieldOnU;
  double theta;
  fullMatrix<double> normals, solpL, solpR, solUR, solUL, coefL, coefR,coef;
 public:
  interfaceUShieldTerm(int DIM, int nbf,const function *p,int thereIsShieldOnP,int thereIsShieldOnU, double fluxDecentering, const function *coefFunction) : function(2*nbf) {
    _DIM = DIM;
    _nbf = nbf;
    theta = fluxDecentering;
    _thereIsShieldOnP = thereIsShieldOnP;
    _thereIsShieldOnU = thereIsShieldOnU;
    setArgument (normals, function::getNormals());
    setArgument (solpL, p, 0);
    setArgument (solpR, p, 1);
    setArgument (solUL, function::getSolution(), 0);
    setArgument (solUR, function::getSolution(), 1);
    setArgument (coefL, coefFunction, 0);
    setArgument (coefR, coefFunction, 1);
    setArgument (coef, coefFunction);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    
    for(size_t i=0; i<nQP; i++) {
      
      double n[3];
      for (int j=0; j<_DIM; j++)
        n[j] = normals(i,j);
      
      // Coefficients
      double cL = coefL(i,0), rhoL = coefL(i,1);
      double cR = coefR(i,0), rhoR = coefR(i,1);
      double zL = rhoL*cL ,zR = rhoR*cR;
      double zS=coefL(i,2);
      double pRiemannL = 0. , pRiemannR = 0.;
      // Solution
      double pL = solpL(i,0), ndotuL = 0;
      double pR = solpR(i,0), ndotuR = 0;
      for (int j=0; j<_DIM; j++) {
        ndotuL += n[j] * solUL(i,j);
        ndotuR += n[j] * solUR(i,j);
      }

      if(_thereIsShieldOnP){
        pRiemannL     = pL/zL/(1./zL + 1./zR) -  (ndotuR - ndotuL)/(1./zS)/2;
        pRiemannR     = pR/zR/(1./zR + 1./zL) -  (ndotuR - ndotuL)/(1./zS)/2;

      }
      if(_thereIsShieldOnU){
        pRiemannL     = 2*pL/zL/(1./zL + 1./zR);
        pRiemannR     = 2*pR/zR/(1./zR + 1./zL);
      }
      // Fluxes for the left cell
      for (int j=0; j<_DIM; j++)
        val(i,j) = -1/rhoL * n[j] * pRiemannL;
      
      // Fluxes for the right cell
      for (int j=0; j<_DIM; j++)
        val(i,_nbf+j) = 1/rhoR * n[j] * pRiemannR;
      
    }
  }
};

void dgConservationLawWaveExplicit::addInterfaceShield(const std::string tag) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
    if (_EquationVersion=="p")
  _interfaceTerm0[tag] = new interfacepShieldTerm(getDIM(),getNbFields(),getU(),_thereIsShieldOnP,_thereIsShieldOnU,getFluxDecentering(),getPhysCoef(tag));
    else if (_EquationVersion=="U")
  _interfaceTerm0[tag] = new interfaceUShieldTerm(getDIM(),getNbFields(),getp(),_thereIsShieldOnP,_thereIsShieldOnU,getFluxDecentering(),getPhysCoef(tag));
  else
    Msg::Fatal("Dg/WaveExplicit: bad EquationVersion (not '%s')", _EquationVersion.c_str());
}

// Interface term for the interface domain/PML

class dgConservationLawWaveExplicit::interfacepTermDomPml : public function {
  int _DIM, _nbf, _incidentFieldsDefined;
  std::string _LayerVersion;
  double theta;
  fullMatrix<double> normals;
  fullMatrix<double> solpL, solUL, coefL, pmlDirL, incidentFieldsL;
  fullMatrix<double> solpR, solUR, coefR, pmlDirR, incidentFieldsR;
 public:
  interfacepTermDomPml(int DIM, int nbf, const function *U,std::string LayerVersion, double fluxDecentering,
                      const function *coefFunction,
                      const function *pmlDirFunction,
                      const function *incidentFieldsFunction) : function(2*nbf) {
    _DIM = DIM;
    _nbf = nbf;
    _LayerVersion = LayerVersion;
    theta = fluxDecentering;
    setArgument (normals, function::getNormals());
    setArgument (solpL, function::getSolution(), 0);
    setArgument (solpR, function::getSolution(), 1);
    setArgument (solUL, U, 0);
    setArgument (solUR, U, 1);
    setArgument (coefL, coefFunction, 0);
    setArgument (coefR, coefFunction, 1);
    setArgument (pmlDirL, pmlDirFunction, 0);
    setArgument (pmlDirR, pmlDirFunction, 1);
    _incidentFieldsDefined = 0;
    if (incidentFieldsFunction!=NULL) {
      setArgument (incidentFieldsL, incidentFieldsFunction, 0);
      setArgument (incidentFieldsR, incidentFieldsFunction, 1);
      _incidentFieldsDefined = 1;
    }
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double n[3];
      for (int j=0; j<_DIM; j++)
        n[j] = normals(i,j);
      
      // Coefficients
      double cL = coefL(i,0), rhoL = coefL(i,1);
      double cR = coefR(i,0), rhoR = coefR(i,1);
      double aL[3] = {0., 0., 0.};
      double aR[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++) {
        aL[j] += pmlDirL(i,j);
        aR[j] += pmlDirR(i,j);
      }
      double ndotuIncL=0.;
      double ndotuIncR=0.;
      if (_incidentFieldsDefined) {
        for (int j=0; j<_DIM; j++) {
          ndotuIncL += n[j]*incidentFieldsL(i,j+1);
          ndotuIncR += n[j]*incidentFieldsR(i,j+1);
        }
      }
      
      // Solution
      double pL = solpL(i,0), ndotuL = 0;
      double pR = solpR(i,0), ndotuR = 0;
      for (int j=0; j<_DIM; j++) {
        ndotuL += n[j] * solUL(i,j);
        ndotuR += n[j] * solUR(i,j);
      }
      
      // Numerical flux (with Riemann)
      double ndotuRiemann = ((ndotuR*rhoR*cR + ndotuL*rhoL*cL) - theta * (pR - pL))/(rhoR*cR + rhoL*cL);
      
      val(i,0) = -cL*cL*rhoL * ndotuRiemann;
      
      val(i,_nbf+0) = cR*cR*rhoR * ndotuRiemann;
      
      // Numerical flux for additionnal equations (with Riemann)
      if ((_LayerVersion=="WithoutLayer") || (_LayerVersion=="BasicLayer"))
        Msg::Fatal("Dg/Wave: Don't use 'addInterfacePml(...)' with 'WithoutLayer' or 'BasicLayer'.");
      
      if (norm3(aL)>0.5)  // Left
        val(i,1) = - (cL*cL*rhoL * (ndotuRiemann - ndotuIncL));
      if (norm3(aR)>0.5)  // Right
        val(i,_nbf+1) = cR*cR*rhoR * (ndotuRiemann - ndotuIncR);
      if ((norm3(aL)<=0.5) && (norm3(aR)<=0.5)) {
        val(i,1) = - (cL*cL*rhoL * (ndotuRiemann - ndotuIncL));
        val(i,_nbf+1) = cR*cR*rhoR * (ndotuRiemann - ndotuIncR);
      }
      
    }
  }
};
class dgConservationLawWaveExplicit::interfaceUTermDomPml : public function {
  int _DIM, _nbf, _incidentFieldsDefined;
  std::string _LayerVersion;
  double theta;
  fullMatrix<double> normals;
  fullMatrix<double> solpL, solUL, coefL, pmlDirL, incidentFieldsL;
  fullMatrix<double> solpR, solUR, coefR, pmlDirR, incidentFieldsR;
 public:
  interfaceUTermDomPml(int DIM, int nbf,const function *p, std::string LayerVersion, double fluxDecentering,
                      const function *coefFunction,
                      const function *pmlDirFunction,
                      const function *incidentFieldsFunction) : function(2*nbf) {
    _DIM = DIM;
    _nbf = nbf;
    _LayerVersion = LayerVersion;
    theta = fluxDecentering;
    setArgument (normals, function::getNormals());
    setArgument (solpL, p, 0);
    setArgument (solpR, p, 1);
    setArgument (solUL, function::getSolution(), 0);
    setArgument (solUR, function::getSolution(), 1);
    setArgument (coefL, coefFunction, 0);
    setArgument (coefR, coefFunction, 1);
    setArgument (pmlDirL, pmlDirFunction, 0);
    setArgument (pmlDirR, pmlDirFunction, 1);
    _incidentFieldsDefined = 0;
    if (incidentFieldsFunction!=NULL) {
      setArgument (incidentFieldsL, incidentFieldsFunction, 0);
      setArgument (incidentFieldsR, incidentFieldsFunction, 1);
      _incidentFieldsDefined = 1;
    }
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double n[3];
      for (int j=0; j<_DIM; j++)
        n[j] = normals(i,j);
      
      // Coefficients
      double cL = coefL(i,0), rhoL = coefL(i,1);
      double cR = coefR(i,0), rhoR = coefR(i,1);
      double aL[3] = {0., 0., 0.};
      double aR[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++) {
        aL[j] += pmlDirL(i,j);
        aR[j] += pmlDirR(i,j);
      }
      double ndotuIncL=0.;
      double ndotuIncR=0.;
      if (_incidentFieldsDefined) {
        for (int j=0; j<_DIM; j++) {
          ndotuIncL += n[j]*incidentFieldsL(i,j+1);
          ndotuIncR += n[j]*incidentFieldsR(i,j+1);
        }
      }
      
      // Solution
      double pL = solpL(i,0), ndotuL = 0;
      double pR = solpR(i,0), ndotuR = 0;
      for (int j=0; j<_DIM; j++) {
        ndotuL += n[j] * solUL(i,j);
        ndotuR += n[j] * solUR(i,j);
      }
      
      // Numerical flux (with Riemann)
      double pRiemann     = ((pR/(rhoR*cR) + pL/(rhoL*cL))     - theta * (ndotuR - ndotuL))/(1./(rhoR*cR) + 1./(rhoL*cL));
      double ndotuRiemann = ((ndotuR*rhoR*cR + ndotuL*rhoL*cL) - theta * (pR - pL))/(rhoR*cR + rhoL*cL);
      
      for (int j=0; j<_DIM; j++)
        val(i,j) = -1/rhoL * n[j] * pRiemann;
      

      for (int j=0; j<_DIM; j++)
        val(i,_nbf+j) = 1/rhoR * n[j] * pRiemann;
      
      // Numerical flux for additionnal equations (with Riemann)
      if ((_LayerVersion=="WithoutLayer") || (_LayerVersion=="BasicLayer"))
        Msg::Fatal("Dg/Wave: Don't use 'addInterfacePml(...)' with 'WithoutLayer' or 'BasicLayer'.");
      
      if (norm3(aL)>0.5)  // Left
        val(i,_DIM) = - (cL*cL*rhoL * (ndotuRiemann - ndotuIncL));
      if (norm3(aR)>0.5)  // Right
        val(i,_nbf+_DIM) = cR*cR*rhoR * (ndotuRiemann - ndotuIncR);
      if ((norm3(aL)<=0.5) && (norm3(aR)<=0.5)) {
        val(i,_DIM) = - (cL*cL*rhoL * (ndotuRiemann - ndotuIncL));
        val(i,_nbf+_DIM) = cR*cR*rhoR * (ndotuRiemann - ndotuIncR);
      }
      
    }
  }
};

void dgConservationLawWaveExplicit::addInterfacePml(const std::string tag, const function *incidentFieldsFunction) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  if (_EquationVersion=="p")
  _interfaceTerm0[tag] = new interfacepTermDomPml(getDIM(),getNbFields(),getU(),getLayerVersion(),getFluxDecentering(),getPhysCoef(tag),getPmlDir(tag),incidentFieldsFunction);
  else if (_EquationVersion=="U")
  _interfaceTerm0[tag] = new interfaceUTermDomPml(getDIM(),getNbFields(),getp(),getLayerVersion(),getFluxDecentering(),getPhysCoef(tag),getPmlDir(tag),incidentFieldsFunction);
  else
    Msg::Fatal("Dg/WaveExplicit: bad EquationVersion (not '%s')", _EquationVersion.c_str());
}

// Interface term for the interface domain/PML with incident fields  (transition incident/scattered fields)

class dgConservationLawWaveExplicit::interfacepTermDomPmlWithInflow : public function {
  int _DIM, _nbf, _incidentFieldsDefined;
  std::string _LayerVersion;
  double theta;
  fullMatrix<double> normals;
  fullMatrix<double> solpL, solUL, coefL, pmlDirL, incidentFieldsL;
  fullMatrix<double> solpR, solUR, coefR, pmlDirR, incidentFieldsR;
 public:
  interfacepTermDomPmlWithInflow(int DIM, int nbf,const function *U, std::string LayerVersion, double fluxDecentering,
                                const function *coefFunction,
                                const function *pmlDirFunction,
                                const function *incidentFieldsFunction) : function(2*nbf) {
    _DIM = DIM;
    _nbf = nbf;
    _LayerVersion = LayerVersion;
    theta = fluxDecentering;
    setArgument (normals, function::getNormals());
    setArgument (solpL, function::getSolution(), 0);
    setArgument (solpR, function::getSolution(), 1);
    setArgument (solUL, U, 0);
    setArgument (solUR, U, 1);
    setArgument (coefL, coefFunction, 0);
    setArgument (coefR, coefFunction, 1);
    setArgument (pmlDirL, pmlDirFunction, 0);
    setArgument (pmlDirR, pmlDirFunction, 1);
    _incidentFieldsDefined = 0;
    if (incidentFieldsFunction!=NULL) {
      setArgument (incidentFieldsL, incidentFieldsFunction, 0);
      setArgument (incidentFieldsR, incidentFieldsFunction, 1);
      _incidentFieldsDefined = 1;
    }
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double n[3];
      for (int j=0; j<3; j++)
        n[j] = normals(i,j);
      
      // Coefficients
      double cL = coefL(i,0), rhoL = coefL(i,1);
      double cR = coefR(i,0), rhoR = coefR(i,1);
      
      // Fields
      double pL = solpL(i,0), uL[3] = {0., 0., 0.};
      double pR = solpR(i,0), uR[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++) {
        uL[j] = solUL(i,j);
        uR[j] = solUR(i,j);
      }
      double ndotuL = scalProd(n,uL);
      double ndotuR = scalProd(n,uR);
      
      // Stretching direction
      double aL[3] = {0., 0., 0.};
      double aR[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++) {
        aL[j] = pmlDirL(i,j);
        aR[j] = pmlDirR(i,j);
      }
      
      // Incident fields
      double pIncL = 0., ndotuIncL = 0.;
      double pIncR = 0., ndotuIncR = 0.;
      if (_incidentFieldsDefined) {
        pIncL = incidentFieldsL(i,0);
        pIncR = incidentFieldsR(i,0);
        for (int j=0; j<_DIM; j++) {
          ndotuIncL += n[j]*incidentFieldsL(i,j+1);
          ndotuIncR += n[j]*incidentFieldsR(i,j+1);
        }
      }
      
      // Data at the interface
      double a[3], pInc, ndotuInc;
      if (norm3(aL)<-0.5) {
        for (int j=0; j<3; j++)
          a[j] = aR[j];
        pInc = pIncR;
        ndotuInc = ndotuIncR;
      }
      else {
        for (int j=0; j<3; j++)
          a[j] = aL[j];
        pInc = pIncL;
        ndotuInc = ndotuIncL;
      }
      
      // Numerical flux
      double /*pRiemannL,*/ ndotuRiemannL;
      double /*pRiemannR,*/ ndotuRiemannR;
      
      if (scalProd(n,a)>0.5) {  // (L=domain, R=pml)
        
        // With total fields
        //pRiemannL     = (((pR+pInc)/(rhoR*cR) + pL/(rhoL*cL))         - theta * ((ndotuR+ndotuInc) - ndotuL))/(1./(rhoR*cR) + 1./(rhoL*cL));
        ndotuRiemannL = (((ndotuR+ndotuInc)*rhoR*cR + ndotuL*rhoL*cL) - theta * ((pR+pInc) - pL))/(rhoR*cR + rhoL*cL);
        
        // With scattered fields
        //pRiemannR     = ((pR/(rhoR*cR) + (pL-pInc)/(rhoL*cL))         - theta * (ndotuR - (ndotuL-ndotuInc)))/(1./(rhoR*cR) + 1./(rhoL*cL));
        ndotuRiemannR = ((ndotuR*rhoR*cR + (ndotuL-ndotuInc)*rhoL*cL) - theta * (pR - (pL-pInc)))/(rhoR*cR + rhoL*cL);
        
      }
      else {  // (L=pml, R=domain)
        
        // With scattered fields
        /*pRiemannL     = (((pR-pInc)/(rhoR*cR) + pL/(rhoL*cL))         - theta * ((ndotuR-ndotuInc) - ndotuL))/(1./(rhoR*cR) + 1./(rhoL*cL));*/
        ndotuRiemannL = (((ndotuR-ndotuInc)*rhoR*cR + ndotuL*rhoL*cL) - theta * ((pR-pInc) - pL))/(rhoR*cR + rhoL*cL);
        
        // With total fields
        //pRiemannR     = ((pR/(rhoR*cR) + (pL+pInc)/(rhoL*cL))         - theta * (ndotuR - (ndotuL+ndotuInc)))/(1./(rhoR*cR) + 1./(rhoL*cL));
        ndotuRiemannR = ((ndotuR*rhoR*cR + (ndotuL+ndotuInc)*rhoL*cL) - theta * (pR - (pL+pInc)))/(rhoR*cR + rhoL*cL);
        
      }
      
      val(i,0) = -cL*cL*rhoL * ndotuRiemannL;      // Flux vers la droite (cause de gauche) - Champs total

      
      val(i,_nbf+0) = cR*cR*rhoR * ndotuRiemannR;  // Flux vers la gauche (case de droite) - Champs diffracté

      
      if ((_LayerVersion!="WithoutLayer") && (_LayerVersion!="BasicLayer")){
        if (scalProd(n,a)>0.5)
          val(i,_nbf+1) = cR*cR*rhoR * ndotuRiemannR;
        else
          val(i,1) = - (cL*cL*rhoL * ndotuRiemannL);
      }
      
    }
  }
};
class dgConservationLawWaveExplicit::interfaceUTermDomPmlWithInflow : public function {
  int _DIM, _nbf, _incidentFieldsDefined;
  std::string _LayerVersion;
  double theta;
  fullMatrix<double> normals;
  fullMatrix<double> solpL, solUL, coefL, pmlDirL, incidentFieldsL;
  fullMatrix<double> solpR, solUR, coefR, pmlDirR, incidentFieldsR;
 public:
  interfaceUTermDomPmlWithInflow(int DIM, int nbf, const function *p, std::string LayerVersion, double fluxDecentering,
                                const function *coefFunction,
                                const function *pmlDirFunction,
                                const function *incidentFieldsFunction) : function(2*nbf) {
    _DIM = DIM;
    _nbf = nbf;
    _LayerVersion = LayerVersion;
    theta = fluxDecentering;
    setArgument (normals, function::getNormals());
    setArgument (solpL, p, 0);
    setArgument (solpR, p, 1);
    setArgument (solUL, function::getSolution(), 0);
    setArgument (solUR, function::getSolution(), 1);
    setArgument (coefL, coefFunction, 0);
    setArgument (coefR, coefFunction, 1);
    setArgument (pmlDirL, pmlDirFunction, 0);
    setArgument (pmlDirR, pmlDirFunction, 1);
    _incidentFieldsDefined = 0;
    if (incidentFieldsFunction!=NULL) {
      setArgument (incidentFieldsL, incidentFieldsFunction, 0);
      setArgument (incidentFieldsR, incidentFieldsFunction, 1);
      _incidentFieldsDefined = 1;
    }
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double n[3];
      for (int j=0; j<3; j++)
        n[j] = normals(i,j);
      
      // Coefficients
      double cL = coefL(i,0), rhoL = coefL(i,1);
      double cR = coefR(i,0), rhoR = coefR(i,1);
      
      // Fields
      double pL = solpL(i,0), uL[3] = {0., 0., 0.};
      double pR = solpR(i,0), uR[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++) {
        uL[j] = solUL(i,j);
        uR[j] = solUR(i,j);
      }
      double ndotuL = scalProd(n,uL);
      double ndotuR = scalProd(n,uR);
      
      // Stretching direction
      double aL[3] = {0., 0., 0.};
      double aR[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++) {
        aL[j] = pmlDirL(i,j);
        aR[j] = pmlDirR(i,j);
      }
      
      // Incident fields
      double pIncL = 0., ndotuIncL = 0.;
      double pIncR = 0., ndotuIncR = 0.;
      if (_incidentFieldsDefined) {
        pIncL = incidentFieldsL(i,0);
        pIncR = incidentFieldsR(i,0);
        for (int j=0; j<_DIM; j++) {
          ndotuIncL += n[j]*incidentFieldsL(i,j+1);
          ndotuIncR += n[j]*incidentFieldsR(i,j+1);
        }
      }
      
      // Data at the interface
      double a[3], pInc, ndotuInc;
      if (norm3(aL)<-0.5) {
        for (int j=0; j<3; j++)
          a[j] = aR[j];
        pInc = pIncR;
        ndotuInc = ndotuIncR;
      }
      else {
        for (int j=0; j<3; j++)
          a[j] = aL[j];
        pInc = pIncL;
        ndotuInc = ndotuIncL;
      }
      
      // Numerical flux
      double pRiemannL, ndotuRiemannL;
      double pRiemannR, ndotuRiemannR;
      
      if (scalProd(n,a)>0.5) {  // (L=domain, R=pml)
        
        // With total fields
        pRiemannL     = (((pR+pInc)/(rhoR*cR) + pL/(rhoL*cL))         - theta * ((ndotuR+ndotuInc) - ndotuL))/(1./(rhoR*cR) + 1./(rhoL*cL));
        ndotuRiemannL = (((ndotuR+ndotuInc)*rhoR*cR + ndotuL*rhoL*cL) - theta * ((pR+pInc) - pL))/(rhoR*cR + rhoL*cL);
        
        // With scattered fields
        pRiemannR     = ((pR/(rhoR*cR) + (pL-pInc)/(rhoL*cL))         - theta * (ndotuR - (ndotuL-ndotuInc)))/(1./(rhoR*cR) + 1./(rhoL*cL));
        ndotuRiemannR = ((ndotuR*rhoR*cR + (ndotuL-ndotuInc)*rhoL*cL) - theta * (pR - (pL-pInc)))/(rhoR*cR + rhoL*cL);
        
      }
      else {  // (L=pml, R=domain)
        
        // With scattered fields
        pRiemannL     = (((pR-pInc)/(rhoR*cR) + pL/(rhoL*cL))         - theta * ((ndotuR-ndotuInc) - ndotuL))/(1./(rhoR*cR) + 1./(rhoL*cL));
        ndotuRiemannL = (((ndotuR-ndotuInc)*rhoR*cR + ndotuL*rhoL*cL) - theta * ((pR-pInc) - pL))/(rhoR*cR + rhoL*cL);
        
        // With total fields
        pRiemannR     = ((pR/(rhoR*cR) + (pL+pInc)/(rhoL*cL))         - theta * (ndotuR - (ndotuL+ndotuInc)))/(1./(rhoR*cR) + 1./(rhoL*cL));
        ndotuRiemannR = ((ndotuR*rhoR*cR + (ndotuL+ndotuInc)*rhoL*cL) - theta * (pR - (pL+pInc)))/(rhoR*cR + rhoL*cL);
        
      }
      
   // Flux vers la droite (cause de gauche) - Champs total
      for (int j=0; j<_DIM; j++)
        val(i,j+1) = -1/rhoL * n[j] * pRiemannL;
      
  // Flux vers la gauche (case de droite) - Champs diffracté
      for (int j=0; j<_DIM; j++)
        val(i,_nbf+j+1) = 1/rhoR * n[j] * pRiemannR;
      
      if ((_LayerVersion!="WithoutLayer") && (_LayerVersion!="BasicLayer")){
        if (scalProd(n,a)>0.5)
          val(i,_nbf+_DIM) = cR*cR*rhoR * ndotuRiemannR;
        else
          val(i,_DIM) = - (cL*cL*rhoL * ndotuRiemannL);
      }
      
    }
  }
};
void dgConservationLawWaveExplicit::addInterfacePmlWithInflow(const std::string tag, const function *incidentFieldsFunction) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  if (_EquationVersion=="p")
  _interfaceTerm0[tag] = new interfacepTermDomPmlWithInflow(getDIM(),getNbFields(),getU(),getLayerVersion(),getFluxDecentering(),getPhysCoef(tag),getPmlDir(tag),incidentFieldsFunction);
  else if (_EquationVersion=="U")
  _interfaceTerm0[tag] = new interfaceUTermDomPmlWithInflow(getDIM(),getNbFields(),getU(),getLayerVersion(),getFluxDecentering(),getPhysCoef(tag),getPmlDir(tag),incidentFieldsFunction);
  else
    Msg::Fatal("Dg/WaveExplicit: bad EquationVersion (not '%s')", _EquationVersion.c_str());
}

void dgConservationLawWaveExplicit::addInterfaceScatteredTotalFields(const std::string tag, const function *incidentFieldsFunction, const function *directionToScattFunction) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  if (_EquationVersion=="p")
  _interfaceTerm0[tag] = new interfacepTermDomPmlWithInflow(getDIM(),getNbFields(),getU(),getLayerVersion(),getFluxDecentering(),getPhysCoef(tag),directionToScattFunction,incidentFieldsFunction);
  else if (_EquationVersion=="U")
  _interfaceTerm0[tag] = new interfaceUTermDomPmlWithInflow(getDIM(),getNbFields(),getp(),getLayerVersion(),getFluxDecentering(),getPhysCoef(tag),directionToScattFunction,incidentFieldsFunction);

  else
    Msg::Fatal("Dg/WaveExplicit: bad EquationVersion (not '%s')", _EquationVersion.c_str());
}
/*==============================================================================
 * Specific interface conditions for uncoupled region (BC on both sides)
 *============================================================================*/
class dgConservationLawWaveExplicit::interfacepTermUncoupledRegions : public function {
  int _DIM, _nbf, _typeBC;
  std::string _LayerVersion;
  double theta;
  fullMatrix<double> normals;
  fullMatrix<double> solpL, solUL, coefL, pmlDirL, extDataL;
  fullMatrix<double> solpR, solUR, coefR, pmlDirR, extDataR;
 public:
  interfacepTermUncoupledRegions(int DIM, int nbf,const function *U, std::string LayerVersion, int typeBC, double fluxDecentering,
                                const function *coefFunction,
                                const function *pmlDirFunction,
                                const function *extDataFunction) : function(2*nbf) {
    _DIM = DIM;
    _nbf = nbf;
    _LayerVersion = LayerVersion;
    _typeBC = typeBC;
    theta = fluxDecentering;
    setArgument (normals, function::getNormals());
    setArgument (solpL, function::getSolution(), 0);
    setArgument (solpR, function::getSolution(), 1);
    setArgument (solUL, U, 0);
    setArgument (solUR, U, 1);
    setArgument (coefL, coefFunction, 0);
    setArgument (coefR, coefFunction, 1);
    if (_LayerVersion != "WithoutLayer") {
      setArgument (pmlDirL, pmlDirFunction, 0);
      setArgument (pmlDirR, pmlDirFunction, 1);
    }
    if (extDataFunction != NULL) {
      setArgument (extDataL, extDataFunction, 0);
      setArgument (extDataR, extDataFunction, 1);
    }
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double n[3];
      for (int j=0; j<_DIM; j++)
        n[j] = normals(i,j);
      
      // Coefficients
      double cL = coefL(i,0), rhoL = coefL(i,1);
      double cR = coefR(i,0), rhoR = coefR(i,1);
      double zL = rhoL*cL ,zR = rhoR*cR;
      // Average and Jump
      double pAverageL = 0., pJumpL = 0., ndotuAverageL = 0., ndotuJumpL = 0.;
      double pAverageR = 0., pJumpR = 0., ndotuAverageR = 0., ndotuJumpR = 0.;
      
      switch (_typeBC) {
        
        // Homogeneous Dirichlet BC on u  (extData is EMPTY)
        case 3:
          if (theta < 0.01) {
            pAverageL = solpL(i,0);
            pAverageR = solpR(i,0);
            ndotuAverageL = 0.;
            ndotuAverageR = 0.;
          }
          else {
            pAverageL = solpL(i,0);
            pAverageR = solpR(i,0);
            pJumpL = 0.;
            pJumpR = 0.;
            for (int j=0; j<_DIM; j++) {
              ndotuAverageL += n[j]*(solUL(i,j))/2;
              ndotuAverageR += n[j]*(solUR(i,j))/2;
              ndotuJumpL -= n[j]*(solUL(i,j))/2;
              ndotuJumpR += n[j]*(solUR(i,j))/2;
            }
          }
          break;
          
        // Dirichlet BC on p with external fields  (extData contains 'pExt')
        case 4:
          if (theta < 0.01) {
            pAverageL = extDataR(i,0);
            pAverageR = extDataL(i,0);
            for (int j=0; j<_DIM; j++) {
              ndotuAverageL += n[j]*(solUL(i,j));
              ndotuAverageR += n[j]*(solUR(i,j));
            }
          }
          else {
            pAverageL = (extDataR(i,0)+solpL(i,0))/2;
            pAverageR = (solpR(i,0)+extDataL(i,0))/2;
            pJumpL = (extDataR(i,0)-solpL(i,0))/2;
            pJumpR = (solpR(i,0)-extDataL(i,0))/2;
            for (int j=0; j<_DIM; j++) {
              ndotuAverageL += n[j]*solUL(i,j);
              ndotuAverageR += n[j]*solUR(i,j);
            }
            ndotuJumpL = 0.;
            ndotuJumpR = 0.;
          }
          break;
        
        // Homogeneous Dirichlet BC on p  (extData is EMPTY)
        case 5:
          if (theta < 0.01) {
            pAverageL = 0.;
            pAverageR = 0.;
            for (int j=0; j<_DIM; j++) {
              ndotuAverageL += n[j]*(solUL(i,j));
              ndotuAverageR += n[j]*(solUR(i,j));
            }
          }
          else {
            pAverageL =  solpL(i,0)/2;
            pAverageR =  solpR(i,0)/2;
            pJumpL = -solpL(i,0)/2;
            pJumpR =  solpR(i,0)/2;
            for (int j=0; j<_DIM; j++) {
              ndotuAverageL += n[j]*solUL(i,j);
              ndotuAverageR += n[j]*solUR(i,j);
            }
            ndotuJumpL = 0.;
            ndotuJumpR = 0.;
          }
          break;
          
        // Open BC (extData is EMPTY)
        case 6:
          if (theta < 0.01) {
            ndotuAverageL =  1/(rhoL*cL) * solpL(i,0);
            ndotuAverageR = -1/(rhoR*cR) * solpR(i,0);
            for (int j=0; j<_DIM; j++) {
              pAverageL += (rhoL*cL) * n[j]*solUL(i,j);
              pAverageR -= (rhoR*cR) * n[j]*solUR(i,j);
            }
          }
          else {
            pAverageL =  solpL(i,0)/2;
            pAverageR =  solpR(i,0)/2;
            pJumpL    = -solpL(i,0)/2;
            pJumpR    =  solpR(i,0)/2;
            for (int j=0; j<_DIM; j++) {
              ndotuAverageL += n[j]*(solUL(i,j))/2;
              ndotuAverageR += n[j]*(solUR(i,j))/2;
              ndotuJumpL    -= n[j]*(solUL(i,j))/2;
              ndotuJumpR    += n[j]*(solUR(i,j))/2;
            }
          }
          break;
        
        // Open BC with inflow (extData contains all external fields) - Sommerfeld
        case 7:
          if (theta < 0.01) {
            pAverageL     = extDataR(i,0);
            pAverageR     = extDataL(i,0);
            ndotuAverageL = 1/(rhoL*cL) * (solpL(i,0)-extDataR(i,0));
            ndotuAverageR = 1/(rhoR*cR) * (extDataL(i,0)-solpR(i,0));
            for (int j=0; j<_DIM; j++) {
              pAverageL     += (rhoL*cL) * n[j]*(solUL(i,j)-extDataR(i,j+1));
              pAverageR     += (rhoR*cR) * n[j]*(extDataL(i,j)-solUR(i,j+1));
              ndotuAverageL += n[j]*extDataR(i,j+1);
              ndotuAverageR += n[j]*extDataL(i,j+1);
            }
          }
          else {
            pAverageL = (extDataR(i,0)+solpL(i,0))/2;
            pAverageR = (solpR(i,0)+extDataL(i,0))/2;
            pJumpL    = (extDataR(i,0)-solpL(i,0))/2;
            pJumpR    = (solpR(i,0)-extDataL(i,0))/2;
            for (int j=0; j<_DIM; j++) {
              ndotuAverageL += n[j]*(extDataR(i,j+1)+solUL(i,j+1))/2;
              ndotuAverageR += n[j]*(solUR(i,j+1)+extDataL(i,j+1))/2;
              ndotuJumpL    += n[j]*(extDataR(i,j+1)-solUL(i,j+1))/2;
              ndotuJumpR    += n[j]*(solUR(i,j+1)-extDataL(i,j+1))/2;
            }
          }
          break;
          case 8:
            pAverageL = 0;
            pAverageR = 0;
            pJumpL    = 0;
            pJumpR    = 0;
            for (int j= 0; j<_DIM; j++) {
              ndotuAverageL += n[j]*(2*solUL(i,j)*zL/(zR + zL));
              ndotuAverageR += n[j]*(2*solUR(i,j)*zL/(zR + zL));
              ndotuJumpL    += 0;
              ndotuJumpR    += 0;
            }
          break;
        
        default:
          Msg::Error("Dg/Wave: There is an error in the INTERFACE CONDITION for UNCOUPLED REGIONS of the WAVE conservation law (BAD '_typeBC': '%i')", _typeBC);
        
      }
      
      // Numerical flux (with Riemann)
      
      //double pRiemannL     = pAverageL     - theta * rhoL*cL * ndotuJumpL;
      double ndotuRiemannL = ndotuAverageL - theta * 1/(rhoL*cL) * pJumpL;
      
      //double pRiemannR     = pAverageR     - theta * rhoR*cR * ndotuJumpR;
      double ndotuRiemannR = ndotuAverageR - theta * 1/(rhoR*cR) * pJumpR;
      
      val(i,0) = -cL*cL*rhoL * ndotuRiemannL;
      val(i,_nbf+0) = cR*cR*rhoR * ndotuRiemannR;

      
      
      // Numerical flux for additionnal equation (with Riemann)
      if ((_LayerVersion!="WithoutLayer") && (_LayerVersion!="BasicLayer")) {
        
        double aL[3] = {0., 0., 0.};
        double aR[3] = {0., 0., 0.};
        for (int j=0; j<_DIM; j++) {
          aL[j] = pmlDirL(i,j);
          aR[j] = pmlDirR(i,j);
        }
        
        if (scalProd(aL,n)>0.5)
          val(i,1) = -cL*cL*rhoL * ndotuRiemannL;
        
        if (scalProd(aR,n)<-0.5)
          val(i,_nbf+1) = -(-cR*cR*rhoR * ndotuRiemannR);
        
      }
      
    }
  }
};
class dgConservationLawWaveExplicit::interfaceUTermUncoupledRegions : public function {
  int _DIM, _nbf, _typeBC;
  std::string _LayerVersion;
  double theta;
  fullMatrix<double> normals;
  fullMatrix<double> solpL, solUL, coefL, pmlDirL, extDataL;
  fullMatrix<double> solpR, solUR, coefR, pmlDirR, extDataR;
 public:
  interfaceUTermUncoupledRegions(int DIM, int nbf,const function *p, std::string LayerVersion, int typeBC, double fluxDecentering,
                                const function *coefFunction,
                                const function *pmlDirFunction,
                                const function *extDataFunction) : function(2*nbf) {
    _DIM = DIM;
    _nbf = nbf;
    _LayerVersion = LayerVersion;
    _typeBC = typeBC;
    theta = fluxDecentering;
    setArgument (normals, function::getNormals());
    setArgument (solpL, p, 0);
    setArgument (solpR, p, 1);
    setArgument (solUL, function::getSolution(), 0);
    setArgument (solUR, function::getSolution(), 1);
    setArgument (coefL, coefFunction, 0);
    setArgument (coefR, coefFunction, 1);
    if (_LayerVersion != "WithoutLayer") {
      setArgument (pmlDirL, pmlDirFunction, 0);
      setArgument (pmlDirR, pmlDirFunction, 1);
    }
    if (extDataFunction != NULL) {
      setArgument (extDataL, extDataFunction, 0);
      setArgument (extDataR, extDataFunction, 1);
    }
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double n[3];
      for (int j=0; j<_DIM; j++)
        n[j] = normals(i,j);
      
      // Coefficients
      double cL = coefL(i,0), rhoL = coefL(i,1);
      double cR = coefR(i,0), rhoR = coefR(i,1);
      double zL = rhoL*cL ,zR = rhoR*cR;
      double zS=1/100/0.001;
      // Average and Jump
      double pAverageL = 0., pJumpL = 0., ndotuAverageL = 0., ndotuJumpL = 0.;
      double pAverageR = 0., pJumpR = 0., ndotuAverageR = 0., ndotuJumpR = 0.;
      
      switch (_typeBC) {
        
        // Homogeneous Dirichlet BC on u  (extData is EMPTY)
        case 3:
          if (theta < 0.01) {
            pAverageL = solpL(i,0);
            pAverageR = solpR(i,0);
            ndotuAverageL = 0.;
            ndotuAverageR = 0.;
          }
          else {
            pAverageL = solpL(i,0);
            pAverageR = solpR(i,0);
            pJumpL = 0.;
            pJumpR = 0.;
            for (int j=0; j<_DIM; j++) {
              ndotuAverageL += n[j]*(solUL(i,j))/2;
              ndotuAverageR += n[j]*(solUR(i,j))/2;
              ndotuJumpL -= n[j]*(solUL(i,j))/2;
              ndotuJumpR += n[j]*(solUR(i,j))/2;
            }
          }
          break;
          
        // Dirichlet BC on p with external fields  (extData contains 'pExt')
        case 4:
          if (theta < 0.01) {
            pAverageL = extDataR(i,0);
            pAverageR = extDataL(i,0);
            for (int j=0; j<_DIM; j++) {
              ndotuAverageL += n[j]*(solUL(i,j));
              ndotuAverageR += n[j]*(solUR(i,j));
            }
          }
          else {
            pAverageL = (extDataR(i,0)+solpL(i,0))/2;
            pAverageR = (solpR(i,0)+extDataL(i,0))/2;
            pJumpL = (extDataR(i,0)-solpL(i,0))/2;
            pJumpR = (solpR(i,0)-extDataL(i,0))/2;
            for (int j=0; j<_DIM; j++) {
              ndotuAverageL += n[j]*solUL(i,j);
              ndotuAverageR += n[j]*solUR(i,j);
            }
            ndotuJumpL = 0.;
            ndotuJumpR = 0.;
          }
          break;
        
        // Homogeneous Dirichlet BC on p  (extData is EMPTY)
        case 5:
          if (theta < 0.01) {
            pAverageL = 0.;
            pAverageR = 0.;
            for (int j=0; j<_DIM; j++) {
              ndotuAverageL += n[j]*(solUL(i,j));
              ndotuAverageR += n[j]*(solUR(i,j));
            }
          }
          else {
            pAverageL =  solpL(i,0)/2;
            pAverageR =  solpR(i,0)/2;
            pJumpL = -solpL(i,0)/2;
            pJumpR =  solpR(i,0)/2;
            for (int j=0; j<_DIM; j++) {
              ndotuAverageL += n[j]*solUL(i,j);
              ndotuAverageR += n[j]*solUR(i,j);
            }
            ndotuJumpL = 0.;
            ndotuJumpR = 0.;
          }
          break;
          
        // Open BC (extData is EMPTY)
        case 6:
          if (theta < 0.01) {
            ndotuAverageL =  1/(rhoL*cL) * solpL(i,0);
            ndotuAverageR = -1/(rhoR*cR) * solpR(i,0);
            for (int j=0; j<_DIM; j++) {
              pAverageL += (rhoL*cL) * n[j]*solUL(i,j);
              pAverageR -= (rhoR*cR) * n[j]*solUR(i,j);
            }
          }
          else {
            pAverageL =  solpL(i,0)/2;
            pAverageR =  solpR(i,0)/2;
            pJumpL    = -solpL(i,0)/2;
            pJumpR    =  solpR(i,0)/2;
            for (int j=0; j<_DIM; j++) {
              ndotuAverageL += n[j]*(solUL(i,j))/2;
              ndotuAverageR += n[j]*(solUR(i,j))/2;
              ndotuJumpL    -= n[j]*(solUL(i,j))/2;
              ndotuJumpR    += n[j]*(solUR(i,j))/2;
            }
          }
          break;
        
        // Open BC with inflow (extData contains all external fields) - Sommerfeld
        case 7:
          if (theta < 0.01) {
            pAverageL     = extDataR(i,0);
            pAverageR     = extDataL(i,0);
            ndotuAverageL = 1/(rhoL*cL) * (solpL(i,0)-extDataR(i,0));
            ndotuAverageR = 1/(rhoR*cR) * (extDataL(i,0)-solpR(i,0));
            for (int j=0; j<_DIM; j++) {
              pAverageL     += (rhoL*cL) * n[j]*(solUL(i,j)-extDataR(i,j+1));
              pAverageR     += (rhoR*cR) * n[j]*(extDataL(i,j)-solUR(i,j+1));
              ndotuAverageL += n[j]*extDataR(i,j+1);
              ndotuAverageR += n[j]*extDataL(i,j+1);
            }
          }
          else {
            pAverageL = (extDataR(i,0)+solpL(i,0))/2;
            pAverageR = (solpR(i,0)+extDataL(i,0))/2;
            pJumpL    = (extDataR(i,0)-solpL(i,0))/2;
            pJumpR    = (solpR(i,0)-extDataL(i,0))/2;
            for (int j=0; j<_DIM; j++) {
              ndotuAverageL += n[j]*(extDataR(i,j+1)+solUL(i,j))/2;
              ndotuAverageR += n[j]*(solUR(i,j)+extDataL(i,j+1))/2;
              ndotuJumpL    += n[j]*(extDataR(i,j+1)-solUL(i,j))/2;
              ndotuJumpR    += n[j]*(solUR(i,j)-extDataL(i,j+1))/2;
            }
          }
          break;
          case 8:
            pAverageL =solpL(i,0)/zL/(1./zL + 1./zR);
            pAverageR =solpR(i,0)/zR/(1./zR + 1./zL) ;
            pJumpL    = 0;
            pJumpR    = 0;
            for (int j=0; j<_DIM; j++) {
              ndotuAverageL += 0;
              ndotuAverageR += 0;
              ndotuJumpL    -= n[j]*(solUL(i,j)-solUR(i,j))/(1./zS)/2*(rhoL*cL);
              ndotuJumpR    += n[j]*(solUR(i,j)-solUL(i,j))/(1./zS)/2*(rhoR*cR);
            }
          break;
        default:
          Msg::Error("Dg/Wave: There is an error in the INTERFACE CONDITION for UNCOUPLED REGIONS of the WAVE conservation law (BAD '_typeBC': '%i')", _typeBC);
        
      }
      
      // Numerical flux (with Riemann)
      
      double pRiemannL     = pAverageL     - theta * rhoL*cL * ndotuJumpL;
      double ndotuRiemannL = ndotuAverageL - theta * 1/(rhoL*cL) * pJumpL;
      
      double pRiemannR     = pAverageR     - theta * rhoR*cR * ndotuJumpR;
      double ndotuRiemannR = ndotuAverageR - theta * 1/(rhoR*cR) * pJumpR;
      

      for (int j=0; j<_DIM; j++)
        val(i,j) = -1/rhoL * n[j] * pRiemannL;
      

      for (int j=0; j<_DIM; j++)
        val(i,_nbf+j) = 1/rhoR * n[j] * pRiemannR;
      
      
      // Numerical flux for additionnal equation (with Riemann)
      if ((_LayerVersion!="WithoutLayer") && (_LayerVersion!="BasicLayer")) {
        
        double aL[3] = {0., 0., 0.};
        double aR[3] = {0., 0., 0.};
        for (int j=0; j<_DIM; j++) {
          aL[j] = pmlDirL(i,j);
          aR[j] = pmlDirR(i,j);
        }
        
        if (scalProd(aL,n)>0.5)
          val(i,_DIM) = -cL*cL*rhoL * ndotuRiemannL;
        
        if (scalProd(aR,n)<-0.5)
          val(i,_nbf+_DIM) = -(-cR*cR*rhoR * ndotuRiemannR);
        
      }
      
    }
  }
};

void dgConservationLawWaveExplicit::addInterfaceHomogeneousDirichletOnU(const std::string tag) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  if (_EquationVersion=="p")
  _interfaceTerm0[tag] = new interfacepTermUncoupledRegions(getDIM(),getNbFields(),getU(),getLayerVersion(),3,getFluxDecentering(),getPhysCoef(tag),getPmlDir(),NULL);
  else if (_EquationVersion=="U")
  _interfaceTerm0[tag] = new interfaceUTermUncoupledRegions(getDIM(),getNbFields(),getp(),getLayerVersion(),3,getFluxDecentering(),getPhysCoef(tag),getPmlDir(),NULL);
  else
    Msg::Fatal("Dg/WaveExplicit: bad EquationVersion (not '%s')", _EquationVersion.c_str());
}

void dgConservationLawWaveExplicit::addInterfaceWall(const std::string tag) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  if (_EquationVersion=="p")
  _interfaceTerm0[tag] = new interfacepTermUncoupledRegions(getDIM(),getNbFields(),getU(),getLayerVersion(),3,getFluxDecentering(),getPhysCoef(tag),getPmlDir(),NULL);
  else if (_EquationVersion=="U")
  _interfaceTerm0[tag] = new interfaceUTermUncoupledRegions(getDIM(),getNbFields(),getp(),getLayerVersion(),3,getFluxDecentering(),getPhysCoef(tag),getPmlDir(),NULL);
  else
    Msg::Fatal("Dg/WaveExplicit: bad EquationVersion (not '%s')", _EquationVersion.c_str());
}

void dgConservationLawWaveExplicit::addInterfaceDirichletOnP(const std::string tag, const function *fieldsExtFunction) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  if (_EquationVersion=="p")
  _interfaceTerm0[tag] = new interfacepTermUncoupledRegions(getDIM(),getNbFields(),getU(),getLayerVersion(),4,getFluxDecentering(),getPhysCoef(tag),getPmlDir(),fieldsExtFunction);
  else if (_EquationVersion=="U")
  _interfaceTerm0[tag] = new interfaceUTermUncoupledRegions(getDIM(),getNbFields(),getp(),getLayerVersion(),4,getFluxDecentering(),getPhysCoef(tag),getPmlDir(),fieldsExtFunction);
  else
    Msg::Fatal("Dg/WaveExplicit: bad EquationVersion (not '%s')", _EquationVersion.c_str());
}

void dgConservationLawWaveExplicit::addInterfaceHomogeneousDirichletOnP(const std::string tag) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  if (_EquationVersion=="p")
  _interfaceTerm0[tag] = new interfacepTermUncoupledRegions(getDIM(),getNbFields(),getU(),getLayerVersion(),5,getFluxDecentering(),getPhysCoef(tag),getPmlDir(),NULL);
  else if (_EquationVersion=="U")
  _interfaceTerm0[tag] = new interfaceUTermUncoupledRegions(getDIM(),getNbFields(),getp(),getLayerVersion(),5,getFluxDecentering(),getPhysCoef(tag),getPmlDir(),NULL);
  else
    Msg::Fatal("Dg/WaveExplicit: bad EquationVersion (not '%s')", _EquationVersion.c_str());

}

void dgConservationLawWaveExplicit::addInterfaceOpenOutflow(const std::string tag) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  if (_EquationVersion=="p")
  _interfaceTerm0[tag] = new interfacepTermUncoupledRegions(getDIM(),getNbFields(),getU(), getLayerVersion(),8,getFluxDecentering(),getPhysCoef(tag),getPmlDir(),NULL);
  else if (_EquationVersion=="U")
  _interfaceTerm0[tag] = new interfaceUTermUncoupledRegions(getDIM(),getNbFields(),getp(), getLayerVersion(),8,getFluxDecentering(),getPhysCoef(tag),getPmlDir(),NULL);
  else
    Msg::Fatal("Dg/WaveExplicit: bad EquationVersion (not '%s')", _EquationVersion.c_str());
}

void dgConservationLawWaveExplicit::addInterfaceOpenOutflowWithInflow(const std::string tag, const function *fieldsExtFunction) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  if (_EquationVersion=="p")
  _interfaceTerm0[tag] = new interfacepTermUncoupledRegions(getDIM(),getNbFields(),getU(), getLayerVersion(),7,getFluxDecentering(),getPhysCoef(tag),getPmlDir(),fieldsExtFunction);
  else if (_EquationVersion=="U")
  _interfaceTerm0[tag] = new interfacepTermUncoupledRegions(getDIM(),getNbFields(),getp(), getLayerVersion(),7,getFluxDecentering(),getPhysCoef(tag),getPmlDir(),fieldsExtFunction);
  else
    Msg::Fatal("Dg/WaveExplicit: bad EquationVersion (not '%s')", _EquationVersion.c_str());
}

