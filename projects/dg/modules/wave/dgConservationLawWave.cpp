#include "dgConservationLawFunction.h"
#include "dgConservationLawWave.h"
#include "function.h"
#include "Numeric.h"
#include "functionGeneric.h"
#include "GmshConfig.h"
#include <math.h>

#include "dgDofContainer.h"
#include "dgFunctionIntegrator.h"

#if defined (HAVE_POST)
  #include "PView.h"
  #include "PViewData.h"
#endif


/*==============================================================================
 * DG-terms : Wave Equation
 *============================================================================*/

class dgConservationLawWave::sourceTerm : public function {
  int _DIM, _nbf, _thereIsDissipationOnP, _thereIsDissipationOnU, _thereIsDamping;
  fullMatrix<double> sol, coef, pmlCoef;
 public:
  sourceTerm(int DIM, int nbf, int thereIsDissipationOnP, int thereIsDissipationOnU,
             const function *coefFunction, const function *pmlCoefFunction) : function(nbf) {
    _DIM = DIM;
    _nbf = nbf;
    _thereIsDissipationOnP = thereIsDissipationOnP;
    _thereIsDissipationOnU = thereIsDissipationOnU;
    setArgument (sol, function::getSolution());
    setArgument (coef, coefFunction);
    _thereIsDamping = 0;
    if (pmlCoefFunction) {
      setArgument (pmlCoef, pmlCoefFunction);
      _thereIsDamping = 1;
    }
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    if (_thereIsDissipationOnP || _thereIsDissipationOnU || _thereIsDamping)
      for (size_t i=0; i<nQP; i++) {
        if (_thereIsDissipationOnP)
          val(i,0) -= coef(i,2) * sol(i,0);
        if (_thereIsDissipationOnU)
          for (int j=0; j<_DIM; j++)
            val(i,j+1) -= coef(i,3) * sol(i,j+1);
        if (_thereIsDamping) {
          val(i,0) -= pmlCoef(i,0) * sol(i,0);
          for(int j=0; j<_DIM; j++)
            val(i,j+1) -= pmlCoef(i,0) * sol(i,j+1);
        }
      }
  }
};

class dgConservationLawWave::volumeTerm : public function {
  int _DIM, _nbf;
  fullMatrix<double> sol, coef;
 public:
  volumeTerm(int DIM, int nbf, const function *coefFunction) : function(3*nbf) {
    _DIM = DIM;
    _nbf = nbf;
    setArgument (sol, function::getSolution());
    setArgument (coef, coefFunction);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      double c   = coef(i,0);
      double rho = coef(i,1);
      
      double p = sol(i,0);
      double u[3];
      for (int j=0; j<_DIM; j++)
        u[j] = sol(i,j+1);
      
      for (int j=0; j<_DIM; j++) {
        val(i,j*_nbf)       = c*c*rho * u[j];
        val(i,j*_nbf+(1+j)) = 1/rho * p;
      }
      
    }
  }
};

class dgConservationLawWave::interfaceTerm : public function {
  int _DIM, _nbf;
  double theta;
  fullMatrix<double> normals, solL, solR, coefL, coefR;
 public:
  interfaceTerm(int DIM, int nbf, double fluxDecentering, const function *coefFunction) : function(2*nbf) {
    _DIM = DIM;
    _nbf = nbf;
    theta = fluxDecentering;
    setArgument (normals, function::getNormals());
    setArgument (solL, function::getSolution(), 0);
    setArgument (solR, function::getSolution(), 1);
    setArgument (coefL, coefFunction, 0);
    setArgument (coefR, coefFunction, 1);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double n[3];
      for (int j=0; j<_DIM; j++)
        n[j] = normals(i,j);
      
      // Coefficients
      double cL = coefL(i,0), rhoL = coefL(i,1);
      double cR = coefR(i,0), rhoR = coefR(i,1);
      
      // Solution
      double pL = solL(i,0), ndotuL = 0;
      double pR = solR(i,0), ndotuR = 0;
      for (int j=0; j<_DIM; j++) {
        ndotuL += n[j] * solL(i,j+1);
        ndotuR += n[j] * solR(i,j+1);
      }
      
      // Numerical flux (with Riemann)
      double pRiemann     = ((pR/(rhoR*cR) + pL/(rhoL*cL))     - theta * (ndotuR - ndotuL))/(1./(rhoR*cR) + 1./(rhoL*cL));
      double ndotuRiemann = ((ndotuR*rhoR*cR + ndotuL*rhoL*cL) - theta * (pR - pL))/(rhoR*cR + rhoL*cL);
      
      // Fluxes for the left cell
      val(i,0) = -cL*cL*rhoL * ndotuRiemann;
      for (int j=0; j<_DIM; j++)
        val(i,j+1) = -1/rhoL * n[j] * pRiemann;
      
      // Fluxes for the right cell
      val(i,_nbf+0) = cR*cR*rhoR * ndotuRiemann;
      for (int j=0; j<_DIM; j++)
        val(i,_nbf+j+1) = 1/rhoR * n[j] * pRiemann;
      
    }
  }
};

class dgConservationLawWave::maxConvectiveSpeed : public function {
  fullMatrix<double> coef;
 public:
  maxConvectiveSpeed(const function *coefFunction) : function(1) {
    setArgument (coef, coefFunction);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++)
      val(i,0) = coef(i,0);
  }
};


/*==============================================================================
 * DG-terms : Wave Equation with PML (Straight, Circle, Ellipse, Sphere or Ellipsoide)
 *============================================================================*/

class dgConservationLawWave::sourceTermPml : public function {
  int _DIM, _nbf;
  std::string _LayerVersion;
  fullMatrix<double> sol, coef, pmlCoef, pmlDir, xyz;
 public:
  sourceTermPml(int DIM, int nbf, std::string LayerVersion,
                const function *coefFunction,
                const function *pmlCoefFunction,
                const function *pmlDirFunction) : function(nbf) {
    _DIM = DIM;
    _nbf = nbf;
    _LayerVersion = LayerVersion;
    setArgument (sol, function::getSolution());
    setArgument (coef, coefFunction);
    setArgument (pmlCoef, pmlCoefFunction);
    setArgument (pmlDir, pmlDirFunction);
    setArgument (xyz, function::getCoordinates());
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    if (_LayerVersion=="PmlStraight")
      callPmlCurv0(m, val);
    else if ((_LayerVersion=="PmlBeveledCuboid") ||
             (_LayerVersion=="PmlCircle") ||
             (_LayerVersion=="PmlEllipse") ||
             (_LayerVersion=="PmlSphere"))
      callPmlCurv1(m, val);
    else if (_LayerVersion=="PmlEllipsoide")
      callPmlCurv2(m, val);
    else if (_LayerVersion=="PmlStraightLT")
      callPmlStraightLT(m, val);
    else if (_LayerVersion=="PmlEllipsoideLT")
      callPmlEllipsoideLT(m, val);
  }
  void callPmlCurv0(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      double aN[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        aN[j] = pmlDir(i,j);
      
      double sigmaPmlN = pmlCoef(i,0);
      
      // Solution
      double u[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        u[j] = sol(i,j+1);
      
      double pN = sol(i,_DIM+1);
      double aNdotu = scalProd(aN,u);
      
      // Terms
      val(i,0) = - sigmaPmlN * pN;
      for(int j=0; j<_DIM; j++)
        val(i,j+1) = - sigmaPmlN * aN[j]*aNdotu;
      val(i,(_DIM+1)) = - sigmaPmlN * pN;
      
    }
  }
  void callPmlCurv1(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      // Coefficients
      double c = coef(i,0);
      double rho = coef(i,1);
      
      double sigmaPmlN = pmlCoef(i,0);
      double curvT = pmlCoef(i,2);
      double sigmaPmlT = pmlCoef(i,1)*curvT;
      
      double aN[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        aN[j] = pmlDir(i,j);
      
      // Solution
      double p = sol(i,0);
      double u[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        u[j] = sol(i,j+1);
      
      double pN = sol(i,_DIM+1);
      double aNdotu = scalProd(aN,u);
      
      // Terms
      val(i,0) = - sigmaPmlN * pN
                 - sigmaPmlT * (p-pN);
      for (int j=0; j<_DIM; j++)
        val(i,j+1) = - sigmaPmlN * aN[j]*aNdotu
                     - sigmaPmlT * (u[j]-aN[j]*aNdotu);
      val(i,(_DIM+1)) = + rho*c*c * curvT * aNdotu
                        - sigmaPmlN * pN;
        // For AM: 'a' towards the exterior
      
    }
  }
  void callPmlCurv2(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      // Coefficients
      double c = coef(i,0);
      double rho = coef(i,1);
      
      double sigmaPmlN = pmlCoef(i,0);
      double curvT = pmlCoef(i,2);
      double sigmaPmlT = pmlCoef(i,1)*curvT;
      double curvB = pmlCoef(i,3);
      double sigmaPmlB = pmlCoef(i,1)*curvB;
      
      double aN[3] = {0., 0., 0.};
      double aT[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++) {
        aN[j] = pmlDir(i,j);
        aT[j] = pmlDir(i,j+3);
      }
      
      // Solution
      double p = sol(i,0);
      double u[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        u[j] = sol(i,j+1);
      
      double pN = sol(i,_DIM+1);
      double pT = sol(i,_DIM+2);
      double aNdotu = scalProd(aN,u);
      double aTdotu = scalProd(aT,u);
      
      // Terms
      val(i,0) = - sigmaPmlN * pN
                 - 0.5*(sigmaPmlT+sigmaPmlB) * (p-pN)
                 - 0.5*(sigmaPmlT-sigmaPmlB) * (2*pT+pN-p);
      for (int j=0; j<_DIM; j++)
        val(i,j+1) = - sigmaPmlN * aN[j]*aNdotu
                     - 0.5*(sigmaPmlT+sigmaPmlB) * (u[j]-aN[j]*aNdotu)
                     - 0.5*(sigmaPmlT-sigmaPmlB) * (2*aT[j]*aTdotu+aN[j]*aNdotu-u[j]);
      val(i,(_DIM+1)) = + rho*c*c * (curvT+curvB) * aNdotu
                        - sigmaPmlN * pN;
      val(i,(_DIM+2)) = - rho*c*c * (curvT) * aNdotu
                        - sigmaPmlT * pT;
      
    }
  }
  void callPmlStraightLT(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      double aN[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        aN[j] = pmlDir(i,j);
      
      double sigmaPmlN = pmlCoef(i,0);
      
      // Solution
      double p = sol(i,0);
      double u[3] = {0., 0., 0.};
      double v[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++) {
        u[j] = sol(i,j+1);
        v[j] = sol(i,j+_DIM+1);
      }
      double aNdotu = scalProd(aN,u);
      double aNdotv = scalProd(aN,v);
      
      // Terms
      val(i,0) = - sigmaPmlN * p;
      for(int j=0; j<_DIM; j++) {
        val(i,j+1) = - sigmaPmlN * ( (v[j]-u[j]) - aN[j] * (aNdotv-2*aNdotu) );
        val(i,j+_DIM+1) = - sigmaPmlN * (v[j]-u[j]);
      }
      
    }
  }
  void callPmlEllipsoideLT(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      // Coefficients
      double sigmaPmlN = pmlCoef(i,0);
      double sigmaPmlT = pmlCoef(i,1);
      double sigmaPmlB = pmlCoef(i,2);
      
      double aN[3] = {0., 0., 0.};
      double aT[3] = {0., 0., 0.};
      double aB[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++) {
        aN[j] = pmlDir(i,j);
        aT[j] = pmlDir(i,j+3);
        aB[j] = pmlDir(i,j+6);
      }
      
      // Matrices
      double A[3][3], B[3][3], C[3][3];
      for (int i1=0; i1<3; i1++)
        for (int i2=0; i2<3; i2++) {
          A[i1][i2] = (-sigmaPmlN+sigmaPmlT+sigmaPmlB) * aN[i1]*aN[i2]
                    + ( sigmaPmlN-sigmaPmlT+sigmaPmlB) * aT[i1]*aT[i2]
                    + ( sigmaPmlN+sigmaPmlT-sigmaPmlB) * aB[i1]*aB[i2];
          B[i1][i2] = (sigmaPmlT*sigmaPmlB) * aN[i1]*aN[i2]
                    + (sigmaPmlN*sigmaPmlB) * aT[i1]*aT[i2]
                    + (sigmaPmlN*sigmaPmlT) * aB[i1]*aB[i2];
          C[i1][i2] = (sigmaPmlT+sigmaPmlB) * aN[i1]*aN[i2]
                    + (sigmaPmlN+sigmaPmlB) * aT[i1]*aT[i2]
                    + (sigmaPmlN+sigmaPmlT) * aB[i1]*aB[i2];
        }
      
      // Fields
      double p = sol(i,0);
      double p1 = sol(i,_DIM+1);
      double p2 = sol(i,_DIM+2);
      
      double u[3] = {0., 0., 0.};
      double v[3] = {0., 0., 0.};
      double w[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++) {
        u[j] = sol(i,j+1);
        v[j] = sol(i,j+_DIM+3);
        w[j] = sol(i,j+_DIM+3+_DIM);
      }
      
      // Terms
      val(i,0) -= (sigmaPmlN+sigmaPmlT+sigmaPmlB) * p;
      val(i,0) += (sigmaPmlN*sigmaPmlT+sigmaPmlN*sigmaPmlB+sigmaPmlT*sigmaPmlB) * p1;
      val(i,0) -= (sigmaPmlN*sigmaPmlT*sigmaPmlB) * p2;
      val(i,_DIM+1) = p;
      val(i,_DIM+2) = p1;
      
      for (int j=0; j<_DIM; j++)
        for (int k=0; k<_DIM; k++) {
          val(i,j+1)      = - A[j][k] * (v[k]-u[k]) - B[j][k] * w[k];
          val(i,j+_DIM+3) = - C[j][k] * (v[k]-u[k]) - B[j][k] * w[k];
          val(i,j+_DIM+3+_DIM) = v[k]-u[k];
        }
    }
  }
};

class dgConservationLawWave::volumeTermPml : public function {
  int _DIM, _nbf;
  std::string _LayerVersion;
  fullMatrix<double> sol, coef, pmlDir;
 public:
  volumeTermPml(int DIM, int nbf, std::string LayerVersion,
                const function *coefFunction,
                const function *pmlCoefFunction,
                const function *pmlDirFunction) : function(3*nbf) {
    _DIM = DIM;
    _nbf = nbf;
    _LayerVersion = LayerVersion;
    setArgument (sol, function::getSolution());
    setArgument (coef, coefFunction);
    setArgument (pmlDir, pmlDirFunction);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    if ((_LayerVersion=="PmlStraight") ||
        (_LayerVersion=="PmlBeveledCuboid") ||
        (_LayerVersion=="PmlCircle") ||
        (_LayerVersion=="PmlEllipse") ||
        (_LayerVersion=="PmlSphere"))
      callPml1AddField(m, val);
    else if (_LayerVersion=="PmlEllipsoide")
      callPml2AddFields(m, val);
  }
  void callPml1AddField(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      // Coefficients
      double c = coef(i,0);
      double rho = coef(i,1);
      double aN[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        aN[j] = pmlDir(i,j);
      
      // Solution
      double p = sol(i,0);
      double u[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        u[j] = sol(i,j+1);
      double aNdotu = scalProd(aN,u);
      
      // Terms
      for (int j=0; j<_DIM; j++) {
        val(i,j*_nbf)          = c*c*rho * u[j];
        val(i,j*_nbf+(j+1))    = 1/rho * p;
        val(i,j*_nbf+(_DIM+1)) = c*c*rho * aN[j]*aNdotu;
      }
      
    }
  }
  void callPml2AddFields(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      // Coefficients
      double c = coef(i,0);
      double rho = coef(i,1);
      double aN[3] = {0., 0., 0.};
      double aT[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++) {
        aN[j] = pmlDir(i,j);
        aT[j] = pmlDir(i,j+3);
      }
      
      // Solution
      double p = sol(i,0);
      double u[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        u[j] = sol(i,j+1);
      double aNdotu = scalProd(aN,u);
      double aTdotu = scalProd(aT,u);
      
      // Terms
      for (int j=0; j<_DIM; j++) {
        val(i,j*_nbf)          = c*c*rho * u[j];
        val(i,j*_nbf+(j+1))    = 1/rho * p;
        val(i,j*_nbf+(_DIM+1)) = c*c*rho * aN[j]*aNdotu;
        val(i,j*_nbf+(_DIM+2)) = c*c*rho * aT[j]*aTdotu;
      }
      
    }
  }
};

class dgConservationLawWave::interfaceTermPml : public function {
  int _DIM, _nbf;
  std::string _LayerVersion;
  double theta;
  fullMatrix<double> normals, pmlDir;
  fullMatrix<double> solL, coefL;
  fullMatrix<double> solR, coefR;
 public:
  interfaceTermPml(int DIM, int nbf, std::string LayerVersion, double fluxDecentering,
                   const function *coefFunction,
                   const function *pmlDirFunction) : function(2*nbf) {
    _DIM = DIM;
    _nbf = nbf;
    _LayerVersion = LayerVersion;
    theta = fluxDecentering;
    setArgument (normals, function::getNormals());
    setArgument (solL, function::getSolution(), 0);
    setArgument (solR, function::getSolution(), 1);
    setArgument (coefL, coefFunction, 0);
    setArgument (coefR, coefFunction, 1);
    setArgument (pmlDir, pmlDirFunction);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    if ((_LayerVersion=="PmlStraight") ||
        (_LayerVersion=="PmlBeveledCuboid") ||
        (_LayerVersion=="PmlCircle") ||
        (_LayerVersion=="PmlEllipse") ||
        (_LayerVersion=="PmlSphere"))
      callPml1AddField(m, val);
    else if (_LayerVersion=="PmlEllipsoide")
      callPml2AddFields(m, val);
  }
  void callPml1AddField(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double n[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++)
        n[j] = normals(i,j);
      
      // Coefficients
      double cL = coefL(i,0), rhoL = coefL(i,1);
      double cR = coefR(i,0), rhoR = coefR(i,1);
      
      double aN[3] = {0., 0., 0.}; 
      for (int j=0; j<_DIM; j++)
        aN[j] += pmlDir(i,j);
      double aNdotn = scalProd(aN,n);
      
      // Solution
      double pL = solL(i,0), uL[3]={0., 0., 0.};
      double pR = solR(i,0), uR[3]={0., 0., 0.};
      for (int j=0; j<_DIM; j++) {
        uL[j] = solL(i,j+1);
        uR[j] = solR(i,j+1);
      }
      double ndotuL = scalProd(n,uL);
      double ndotuR = scalProd(n,uR);
      double aNdotuL = scalProd(aN,uL);
      double aNdotuR = scalProd(aN,uR);
      
      double qNL = solL(i,_DIM+1);
      double qNR = solR(i,_DIM+1);
      
      // Numerical flux (with Riemann)
      double pRiemann     = ((pR/(rhoR*cR) + pL/(rhoL*cL))     - theta * (ndotuR - ndotuL))/(1./(rhoR*cR) + 1./(rhoL*cL));
      double ndotuRiemann = ((ndotuR*rhoR*cR + ndotuL*rhoL*cL) - theta * (pR - pL))/(rhoR*cR + rhoL*cL);
      
      val(i,0) = -cL*cL*rhoL * ndotuRiemann;
      for (int j=0; j<_DIM; j++)
        val(i,j+1) = -1/rhoL * n[j] * pRiemann;
      val(i,_DIM+1) = - aNdotn*(cR*cR*rhoR*aNdotuR + cL*cL*rhoL*aNdotuL)/2. + theta * cL*(qNR-qNL)/2.;
      
      val(i,_nbf+0) = cR*cR*rhoR * ndotuRiemann;
      for (int j=0; j<_DIM; j++)
        val(i,_nbf+j+1) = 1/rhoR * n[j] * pRiemann;
      val(i,_nbf+_DIM+1) = aNdotn*(cR*cR*rhoR*aNdotuR + cL*cL*rhoL*aNdotuL)/2. - theta * cR*(qNR-qNL)/2.;
      
    }
  }
  void callPml2AddFields(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double n[3];
      for (int j=0; j<_DIM; j++)
        n[j] = normals(i,j);
      
      // Coefficients
      double cL = coefL(i,0), rhoL = coefL(i,1);
      double cR = coefR(i,0), rhoR = coefR(i,1);
      
      double aN[3] = {0.,0.,0.};
      double aT[3] = {0.,0.,0.};
      for (int j=0; j<_DIM; j++) {
        aN[j] = pmlDir(i,j);
        aT[j] = pmlDir(i,j+3);
      }
      double aNdotn = scalProd(aN,n);
      double aTdotn = scalProd(aT,n);
      
      // Solution
      double pL = solL(i,0), uL[3] = {0., 0., 0.};
      double pR = solR(i,0), uR[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++) {
        uL[j] = solL(i,j+1);
        uR[j] = solR(i,j+1);
      }
      double ndotuL = scalProd(n,uL);
      double ndotuR = scalProd(n,uR);
      double aNdotuL = scalProd(aN,uL);
      double aNdotuR = scalProd(aN,uR);
      double aTdotuL = scalProd(aT,uL);
      double aTdotuR = scalProd(aT,uR);
      
      double qNL = solL(i,_DIM+1);
      double qNR = solR(i,_DIM+1);
      double qTL = solL(i,_DIM+2);
      double qTR = solR(i,_DIM+2);
      
      // Terms
      double pRiemann     = ((pR/(rhoR*cR) + pL/(rhoL*cL))     - theta * (ndotuR - ndotuL))/(1./(rhoR*cR) + 1./(rhoL*cL));
      double ndotuRiemann = ((ndotuR*rhoR*cR + ndotuL*rhoL*cL) - theta * (pR - pL))/(rhoR*cR + rhoL*cL);
      
      val(i,0) = -cL*cL*rhoL * ndotuRiemann;
      for (int j=0; j<_DIM; j++)
        val(i,j+1) = -1/rhoL * n[j] * pRiemann;
      
      val(i,_nbf+0) = cR*cR*rhoR * ndotuRiemann;
      for (int j=0; j<_DIM; j++)
        val(i,_nbf+j+1) = 1/rhoR * n[j] * pRiemann;
      
      val(i,_DIM+1) = - ( aNdotn*(cR*cR*rhoR*aNdotuR + cL*cL*rhoL*aNdotuL)/2. - cL*(qNR-qNL)/2. );
      val(i,_DIM+2) = - ( aTdotn*(cR*cR*rhoR*aTdotuR + cL*cL*rhoL*aTdotuL)/2. - cL*(qTR-qTL)/2. );
      
      val(i,_nbf+_DIM+1) = aNdotn*(cR*cR*rhoR*aNdotuR + cL*cL*rhoL*aNdotuL)/2. - cR*(qNR-qNL)/2.;
      val(i,_nbf+_DIM+2) = aTdotn*(cR*cR*rhoR*aTdotuR + cL*cL*rhoL*aTdotuL)/2. - cR*(qTR-qTL)/2.;
      
    }
  }
};


/*==============================================================================
 * CG-terms
 *============================================================================*/

class dgConservationLawWave::volumeTermCG : public function {
  int _DIM, _nbf, _thereIsDissipationOnU;
  fullMatrix<double> sol, solGrad, solPrev, _dt, coef, numCoef;
 public:
  volumeTermCG(int DIM, int nbf, const function *prevSolution, int thereIsDissipationOnU,
               const function *coefFunction, const function *numCoefFunction) : function(3*nbf) {
    _DIM = DIM;
    _nbf = nbf;
    _thereIsDissipationOnU = thereIsDissipationOnU;
    setArgument (sol, function::getSolution());
    setArgument (solGrad, function::getSolutionGradient());
    setArgument (solPrev, prevSolution);
    setArgument (coef, coefFunction);
    setArgument (numCoef, numCoefFunction);
    setArgument (_dt, function::getDT());
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double c   = coef(i,0);
      double rho = coef(i,1);
      double sigmaU = 0;
      if (_thereIsDissipationOnU)
        sigmaU = coef(i,1);
      
      for (int j=0; j<_DIM; j++) {
        val(i,0  +j*_nbf) = c*c*rho * sol(i,j+1);
        val(i,j+1+j*_nbf) = 1/rho   * sol(i,0);
      }
      
      //PSPG TERMS
      double dt = _dt(i,0);
      double kappaNum = numCoef(i,0);
      if (dt > 0.)
        for (int j=0; j<_DIM; j++)
          val(i,j*_nbf) += kappaNum * ((sol(i,j+1)-solPrev(i,j+1))/dt - 1/rho * solGrad(i,j) + sigmaU * sol(i,j+1));
    }
  }
};


/*==============================================================================
 * Conservation law : constructor - setup - destructor
 *============================================================================*/

int dgConservationLawWave_nbf(std::string LayerVersion, int DIM) {
  if (LayerVersion=="WithoutLayer")                                            // Without layer
    return (1+DIM);
  else if (LayerVersion=="BasicLayer")                                         // Basic layer
    return (1+DIM);
  else if (LayerVersion=="PmlStraight")                                        // Straight PML (0 curvature)
    return (1+DIM)+1;
  else if ((LayerVersion=="PmlBeveledCuboid") ||
           (LayerVersion=="PmlCircle") ||
           (LayerVersion=="PmlEllipse")) {                                     // BeveledCuboid/Circular/Elliptical PML (1 curvature)
    if (DIM!=2) Msg::Error("Dg/Wave: With 'PmlCircle' and 'PmlEllipse', DIM must be 2.");
    return (1+DIM)+1;
  }
  else if (LayerVersion=="PmlSphere") {                                        // Spherical PML (1 curvature)
    if (DIM!=3) Msg::Fatal("Dg/Wave: With 'PmlSphere', DIM must be 3.");
    return (1+DIM)+1;
  }
  else if (LayerVersion=="PmlEllipsoide") {                                    // Ellipsoidal PML (2 curvatures)
    if (DIM!=3) Msg::Fatal("Dg/Wave: With 'PmlEllipsoide', DIM must be 3.");
    return (1+DIM)+2;
  }
  else if (LayerVersion=="PmlStraightLT")                                      // Straight PML LT
    return (1+DIM)+DIM;
  else if (LayerVersion=="PmlEllipsoideLT") {                                  // Ellipsoidal PML LT
    if (DIM!=3) Msg::Fatal("Dg/Wave: With 'PmlEllipsoideLT', DIM must be 3.");
    return (1+DIM)+8;
  }
  else
    Msg::Fatal("Dg/Wave: Bad 'LayerVersion'.");
  return -1;
}

dgConservationLawWave::dgConservationLawWave(int DIM, std::string LayerVersion) :
    dgConservationLawFunction(dgConservationLawWave_nbf(LayerVersion,DIM))
{
  _DIM = DIM;
  _NumMethod = 1;
  _LayerVersion = LayerVersion;
  _thereIsDissipationOnP = 0;
  _thereIsDissipationOnU = 0;
  _thereIsShieldOnP = 0;
  _thereIsShieldOnU = 0;
  _thereIsShieldOnP1 = 0;
  _thereIsShieldOnU1 = 0;
  _fluxDecentering = 1.;
  addToTagList("");
  _pmlCoef = NULL;
  _pmlDir = NULL;
  setPmlCoef(0);
  setPmlDir(0);
}

void dgConservationLawWave::setup() {
  switch (_NumMethod) {
    case 1: // DG
      for(std::vector<std::string>::iterator it=_tagList.begin(); it!=_tagList.end(); ++it) {
        const std::string tag = *it;
        _volumeTerm0[tag]            = new sourceTerm(_DIM,_nbf,_thereIsDissipationOnP,_thereIsDissipationOnU,getPhysCoef(tag),NULL);
        _volumeTerm1[tag]            = new volumeTerm(_DIM,_nbf,getPhysCoef(tag));
        _interfaceTerm0[tag]         = new interfaceTerm(_DIM,_nbf,getFluxDecentering(),getPhysCoef(tag));
        _maximumConvectiveSpeed[tag] = new maxConvectiveSpeed(getPhysCoef(tag));
      }
      break;
    case 2: // CG
      for(std::vector<std::string>::iterator it=_tagList.begin(); it!=_tagList.end(); ++it) {
        const std::string tag = *it;
        _volumeTerm0[tag]            = new sourceTerm(_DIM,_nbf,_thereIsDissipationOnP,_thereIsDissipationOnU,getPhysCoef(tag),NULL);
        _volumeTerm1[tag]            = new volumeTermCG(_DIM,_nbf,getPrevSolution(),_thereIsDissipationOnU,getPhysCoef(tag),getNumCoef(tag));
        _maximumConvectiveSpeed[tag] = new maxConvectiveSpeed(getPhysCoef(tag));
      }
      break;
  }
}

dgConservationLawWave::~dgConservationLawWave () {
  if (_pmlCoef) delete _pmlCoef;
  if (_pmlDir)  delete _pmlDir;
  for (termMap::const_iterator it=_volumeTerm0.begin(); it!=_volumeTerm0.end(); it++)
    if (_volumeTerm0[it->first]) delete _volumeTerm0[it->first];
  for (termMap::const_iterator it=_volumeTerm1.begin(); it!=_volumeTerm1.end(); it++)
    if (_volumeTerm1[it->first]) delete _volumeTerm1[it->first];
  for (termMap::const_iterator it=_interfaceTerm0.begin(); it!=_interfaceTerm0.end(); it++)
    if (_interfaceTerm0[it->first]) delete _interfaceTerm0[it->first];
  for (termMap::const_iterator it=_maximumConvectiveSpeed.begin(); it!=_maximumConvectiveSpeed.end(); it++)
    if (_maximumConvectiveSpeed[it->first]) delete _maximumConvectiveSpeed[it->first];
}


/*==============================================================================
 * Coefficients : set - get
 *============================================================================*/

int dgConservationLawWave::addToTagList(const std::string tag) {
  if (isInTagList(tag))
    return 0; // 'tag' is already in 'tagList'
  else {
    _tagList.push_back(tag);
    return 1; // 'tag' is added in 'tagList'
  }
}

int dgConservationLawWave::isInTagList(const std::string tag) {
  for(std::vector<std::string>::iterator it=_tagList.begin(); it!=_tagList.end(); ++it)
    if (*it == tag)
      return 1; // 'tag' is in 'tagList'
  return 0;     // 'tag' is not in 'tagList'
}

const function *dgConservationLawWave::getFunctionForTag2(const termMap &map, const std::string tag) const {
  termMap::const_iterator it = map.find(tag);
  if (it == map.end())
    it = map.find("");
  if (it == map.end())
    Msg::Fatal("Dg/Wave: a parameter is not defined.");
  return it->second;
}


/*==============================================================================
 * Add a PML
 *============================================================================*/

void dgConservationLawWave::addPml(const std::string tag, const function *incidentFieldsFunction) {
  if (_LayerVersion=="BasicLayer") {
      delete _volumeTerm0[tag];
      _volumeTerm0[tag] = new sourceTerm(_DIM,_nbf,_thereIsDissipationOnP,_thereIsDissipationOnU,getPhysCoef(tag),getPmlCoef(tag));
  } else if ((_LayerVersion=="PmlStraightLT") ||
             (_LayerVersion=="PmlEllipsoideLT")) {
      delete _volumeTerm0[tag];
      _volumeTerm0[tag] = new sourceTermPml(_DIM,_nbf,_LayerVersion,getPhysCoef(tag),getPmlCoef(tag),getPmlDir(tag));
  } else if ((_LayerVersion=="PmlStraight") ||
             (_LayerVersion=="PmlBeveledCuboid") ||
             (_LayerVersion=="PmlCircle") ||
             (_LayerVersion=="PmlEllipse") ||
             (_LayerVersion=="PmlSphere") ||
             (_LayerVersion=="PmlEllipsoide")) {
      delete _volumeTerm0[tag];
      delete _volumeTerm1[tag];
      delete _interfaceTerm0[tag];
      _volumeTerm0[tag]    = new sourceTermPml(_DIM,_nbf,_LayerVersion,getPhysCoef(tag),getPmlCoef(tag),getPmlDir(tag));
      _volumeTerm1[tag]    = new volumeTermPml(_DIM,_nbf,_LayerVersion,getPhysCoef(tag),getPmlCoef(tag),getPmlDir(tag));
      _interfaceTerm0[tag] = new interfaceTermPml(_DIM,_nbf,_LayerVersion,getFluxDecentering(),getPhysCoef(tag),getPmlDir(tag));
  } else
      Msg::Fatal("Dg/Wave: Bad 'LayerVersion'.");
}

class dgConservationLawWave::genericPmlCoefSphere : public function {
  int DIM;
  double lPml, c, center[3], radius;
  fullMatrix<double> xyz;
 public:
  genericPmlCoefSphere(int _DIM, double _lPml, double _c,
                       double _center[3], double _radius) : function(3) {
    DIM = _DIM;
    lPml = _lPml;
    c = _c;
    for (int j=0; j<3; j++)
      center[j] = _center[j];
    radius = _radius;
    setArgument (xyz, function::getCoordinates());
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      double r = 0.;
      for (int j=0; j<3; j++)
        r += pow(xyz(i,j) - center[j],2);
      r = sqrt(r);
      
      double h = r-radius;
      if (h>lPml) h=lPml;
      
      val(i,0) = c/lPml * h/(lPml-h);
      val(i,1) = c * (-log(1-h/lPml) - h/lPml);
      val(i,2) = 1/r;
      
    }
  }
};

class dgConservationLawWave::genericPmlCoefEllipsoide : public function {
  int DIM;
  double lPml, c, center[3], ellX, ellY, ellZ;
  fullMatrix<double> xyz;
 public:
  genericPmlCoefEllipsoide(int _DIM, double _lPml, double _c,
                           double _center[3], double semiAxis[3]) : function(_DIM+1) {
    DIM = _DIM;
    lPml = _lPml;
    c = _c;
    ellX = semiAxis[0];
    if (semiAxis[1]<0) ellY = ellX;
    else ellY = semiAxis[1];
    if (semiAxis[2]<0) ellZ = ellY;
    else Msg::Fatal("Dg/Wave: useGenericPmlDir() can't yet be used with general ellipsoide.");  // if 3D, ellipsoide of revolution
    for (int j=0; j<3; j++)
      center[j] = _center[j];
    setArgument (xyz, function::getCoordinates());
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      double h=0.;
      
#if defined (HAVE_POST)
      double x[3] = {xyz(i,0),xyz(i,1),xyz(i,2)};
      PView *d = PView::list[0];
      d->getData()->searchScalar(x[0],x[1],x[2],&h);
#endif
      double rCurvT = 0.;
      double rCurvB = 0.;
      
      double aN[3] = {0., 0., 0.};
#if defined (HAVE_POST)
      PView *d1 = PView::list[1];
      d1->getData()->searchVector(x[0],x[1],x[2],&aN[0]);
#endif
      
      double p[3];
      for (int j=0; j<3; j++)
        p[j] = x[j] - center[j] - h*aN[j];
      
      rCurvT = pow(pow(ellY*p[0]/ellX,2.) + pow(ellX*sqrt(pow(p[1],2)+pow(p[2],2))/ellY,2.),1.5) / (ellX*ellY);
      
      if (DIM==3) {
        double gaussCurv = pow(ellX*ellY*ellZ,2) / pow(pow(p[0]*ellY*ellZ/ellX,2) +
                                                       pow(p[1]*ellX*ellZ/ellY,2) +
                                                       pow(p[2]*ellX*ellY/ellZ,2) ,2);  // For ellipsoide
        rCurvB = 1/(gaussCurv*rCurvT);
      }
      
      if (h>lPml) h=lPml;
      
      val(i,0) = c/lPml * h/(lPml-h);
      val(i,1) = c * (-log(1-h/lPml) - h/lPml);
      val(i,2) = 1/(rCurvT+h);
      if (DIM == 3)
        val(i,3) = 1/(rCurvB+h);

      /*
        c/l * (l/(l-d) - 1)
      = c/l * d/(l-d)
        c/l * [ - l * ln(l-d) + l * ln(l) - d ]
      = c   * [ - ln(1-d/l) - d/l ]
      */
      
    }
  }
};

class dgConservationLawWave::genericPmlCoefEllipsoideLT : public function {
  int DIM;
  double lPml, c, center[3], ellX, ellY, ellZ;
  fullMatrix<double> xyz;
public:
  genericPmlCoefEllipsoideLT(int _DIM, double _lPml, double _c,
                             double _center[3], double semiAxis[3]) : function(_DIM+1) {
    DIM = _DIM;
    lPml = _lPml;
    c = _c;
    ellX = semiAxis[0];
    if (semiAxis[1]<0) ellY = ellX;
    else ellY = semiAxis[1];
    if (semiAxis[2]<0) ellZ = ellY;
    else Msg::Fatal("Dg/Wave: useGenericPmlDir() can't yet be used with general ellipsoide.");  // if 3D, ellipsoide of revolution
    for (int j=0; j<3; j++)
      center[j] = _center[j];
    setArgument (xyz, function::getCoordinates());
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      double h=0.;
      
#if defined (HAVE_POST)
      double x[3] = {xyz(i,0),xyz(i,1),xyz(i,2)};
      PView *d = PView::list[0];
      d->getData()->searchScalar(x[0],x[1],x[2],&h);
#endif
      double rCurvT = 0.;
      double rCurvB = 0.;
      
      double aN[3] = {0., 0., 0.};
#if defined (HAVE_POST)
      PView *d1 = PView::list[1];
      d1->getData()->searchVector(x[0],x[1],x[2],&aN[0]);
#endif
      
      double p[3];
      for (int j=0; j<3; j++)
        p[j] = x[j] - center[j] - h*aN[j];
      
      rCurvT = pow(pow(ellY*p[0]/ellX,2.) + pow(ellX*sqrt(pow(p[1],2)+pow(p[2],2))/ellY,2.),1.5) / (ellX*ellY);
      
      if (DIM==3) {
        double gaussCurv = pow(ellX*ellY*ellZ,2) / pow(pow(p[0]*ellY*ellZ/ellX,2) +
                                                       pow(p[1]*ellX*ellZ/ellY,2) +
                                                       pow(p[2]*ellX*ellY/ellZ,2) ,2);  // For ellipsoide
        rCurvB = 1/(gaussCurv*rCurvT);
      }
      
      if (h>lPml) h=lPml;
      
      double sigmaBar = c * (-log(1-h/lPml) - h/lPml);
      
      val(i,0) = c/lPml * h/(lPml-h);
      val(i,1) = sigmaBar/(h+rCurvT);
      if (DIM==3)
        val(i,2) = sigmaBar/(h+rCurvB);
      
      /*
       c/l * (l/(l-d) - 1)
       = c/l * d/(l-d)
       c/l * [ - l * ln(l-d) + l * ln(l) - d ]
       = c   * [ - ln(1-d/l) - d/l ]
      */
      
    }
  }
};

class dgConservationLawWave::genericPmlCoefBeveledCuboid : public function {
  int _DIM;
  std::string _pmlCoefFuncVersion;
  double _lPml, _c, _center[3], _semiAxis[3], _radius, _pmlCoefFuncMulti;
  fullMatrix<double> xyz;
 public:
  genericPmlCoefBeveledCuboid(int DIM, double lPml, double c,
                              double center[3], double semiAxis[3], double radius,
                              const std::string pmlCoefFuncVersion, double pmlCoefFuncMulti) : function(3) {
    _DIM = DIM;
    _lPml = lPml;
    _c = c;
    for (int j=0; j<3; j++) {
      _center[j] = center[j];
      _semiAxis[j] = semiAxis[j];
    }
    _radius = radius;
    _pmlCoefFuncVersion = pmlCoefFuncVersion;
    _pmlCoefFuncMulti = pmlCoefFuncMulti;
    setArgument (xyz, function::getCoordinates());
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      double xyzloc[3] = {0,0,0};
      for (int j=0; j<_DIM; j++)
        xyzloc[j] = fabs(xyz(i,j)-_center[j]);
      
      double corner[3] = {0,0,0};
      for (int j=0; j<_DIM; j++)
        corner[j] = _semiAxis[j]-_radius;
      
      double h=0.;
      double curv=0.;
      
      if ((xyzloc[0]>=corner[0]) && (xyzloc[1]<=corner[1]))             // Prévu pour du 2D
        h = xyzloc[0]-_semiAxis[0];
      else if ((xyzloc[0]<=corner[0]) && (xyzloc[1]>=corner[1]))
        h = xyzloc[1]-_semiAxis[1];
      else if ((xyzloc[0]>=corner[0]) && (xyzloc[1]>=corner[1])) {
        for (int j=0; j<_DIM; j++)
          h += pow(xyzloc[j]-corner[j],2);
        h = sqrt(h);
        if (h>_radius/2)
          curv = 1/h;
        h -= _radius;
      }
      
      if (h<0.) h=0.;
      else if (h>_lPml) h=_lPml;
      
      if (_pmlCoefFuncVersion=="Poly0") {
        val(i,0) = _pmlCoefFuncMulti;
        val(i,1) = _pmlCoefFuncMulti * h;
      }
      else if (_pmlCoefFuncVersion=="Poly1") {
        val(i,0) = _pmlCoefFuncMulti * (h/_lPml);
        val(i,1) = _pmlCoefFuncMulti * (h/_lPml) * (h/2);
      }
      else if (_pmlCoefFuncVersion=="Poly2") {
        val(i,0) = _pmlCoefFuncMulti * pow(h/_lPml,2);
        val(i,1) = _pmlCoefFuncMulti * pow(h/_lPml,2) * (h/3);
      }
      else if (_pmlCoefFuncVersion=="Poly3") {
        val(i,0) = _pmlCoefFuncMulti * pow(h/_lPml,3);
        val(i,1) = _pmlCoefFuncMulti * pow(h/_lPml,3) * (h/4);
      }
      else if (_pmlCoefFuncVersion=="Hyp") {
        val(i,0) = _pmlCoefFuncMulti * _c * 1/(_lPml-h);
        val(i,1) = _pmlCoefFuncMulti * _c * (-log(1-h/_lPml));
      }
      else if (_pmlCoefFuncVersion=="HypSh") {
        val(i,0) = _pmlCoefFuncMulti * _c/_lPml * h/(_lPml-h);
        val(i,1) = _pmlCoefFuncMulti * _c * (-log(1-h/_lPml) - h/_lPml);
      }
      else
        Msg::Fatal("Dg/Wave: useGenericPmlCoef() not implemented for this 'pmlCoefFuncVersion'.");
      
      val(i,2) = curv;
      
    }
  }
};

class dgConservationLawWave::genericPmlDirSphere : public function {
  int DIM;
  double center[3];
  fullMatrix<double> xyz;
 public:
  genericPmlDirSphere(int _DIM, double _center[3]) : function(3) {
    DIM = _DIM;
    for (int j=0; j<3; j++)
      center[j] = _center[j];
    setArgument (xyz, function::getCoordinates());
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      double aN[3] = {0.,0.,0.};
      for (int j=0; j<3; j++)
        aN[j] = xyz(i,j) - center[j];
      norme(aN);
      
      for (int j=0; j<3; j++)
        val(i,j) = aN[j];
      
    }
  }
};

class dgConservationLawWave::genericPmlDirEllipsoide : public function {
  int DIM;
  double center[3];
  fullMatrix<double> xyz;
public:
  genericPmlDirEllipsoide(int _DIM, double _center[3]) : function(3*(_DIM)) {
    DIM = _DIM;
    for (int j=0; j<3; j++)
      center[j] = _center[j];
    setArgument (xyz, function::getCoordinates());
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      double x[3] = {xyz(i,0),xyz(i,1),xyz(i,2)};
      
      double aN[3] = {0.,0.,0.};
#if defined (HAVE_POST)
      PView *d1 = PView::list[1];
      d1->getData()->searchVector(x[0],x[1],x[2],&aN[0]);
#endif
      norme(aN);
      
      for (int j=0; j<3; j++)
        val(i,j) = aN[j];
      
      if (DIM==3) {
        
        double aT[3] = {0.,0.,0.};
        double aB[3] = {0.,0.,0.};
        
        double h = 0.;
#if defined (HAVE_POST)
        PView *d0 = PView::list[0];
        d0->getData()->searchScalar(x[0],x[1],x[2],&h);
#endif
        double p[3];
        for (int j=0; j<3; j++)
          p[j] = x[j] - center[j] - h*aN[j];
        
        aB[0] = 0;
        aB[1] = p[2];
        aB[2] = -p[1];
        norme(aB);
        
        for (int j=0; j<3; j++)
          aT[j] = crossProd(aB,aN,j);
        norme(aT);
        
        for (int j=0; j<3; j++)
          val(i,j+3) = aT[j];
        
        for (int j=0; j<3; j++)
          val(i,j+6) = aB[j];
        
      }
      
    }
  }
};

class dgConservationLawWave::genericPmlDirBeveledCuboid : public function {
  int DIM;
  double center[3], semiAxis[3], radius;
  fullMatrix<double> xyz;
 public:
  genericPmlDirBeveledCuboid(int _DIM, double _center[3], double _semiAxis[3], double _radius) : function(3) {
    DIM = _DIM;
    for (int j=0; j<3; j++) {
      center[j] = _center[j];
      semiAxis[j] = _semiAxis[j];
    }
    radius = _radius;
    setArgument (xyz, function::getCoordinates());
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      double aN[3] = {0.,0.,0.};
      
      double xyzloc[3] = {0,0,0};
      for (int j=0; j<DIM; j++)
        xyzloc[j] = fabs(xyz(i,j)-center[j]);
      
      double corner[3] = {0,0,0};
      for (int j=0; j<DIM; j++)
        corner[j] = semiAxis[j]-radius;
      
      if ((xyzloc[0]>=corner[0]) && (xyzloc[1]<=corner[1]))             // Prévu pour du 2D
        aN[0] = 1.;
      else if ((xyzloc[0]<=corner[0]) && (xyzloc[1]>=corner[1]))
        aN[1] = 1.;
      else if ((xyzloc[0]>=corner[0]) && (xyzloc[1]>=corner[1])) {
        for (int j=0; j<DIM; j++)
          aN[j] = xyzloc[j]-corner[j];
      }
      norme(aN);
      
      for (int j=0; j<DIM; j++)
        val(i,j) = ((xyz(i,j)-center[j])>=0) ? aN[j] : (-aN[j]);
      
    }
  }
};

void dgConservationLawWave::useGenericPmlCoef(const std::string pmlCoefVersion, double lPml, double c,
                                              double centerX, double centerY, double centerZ,
                                              double semiAxisX, double semiAxisY, double semiAxisZ, double radius,
                                              const std::string pmlCoefFuncVersion, double pmlCoefFuncMulti) {
  double center[3] = {centerX, centerY, centerZ};
  double semiAxis[3] = {semiAxisX, semiAxisY, semiAxisZ};
  if (pmlCoefVersion=="Circle")
    _pmlCoef = new genericPmlCoefSphere(2, lPml, c, center, semiAxis[0]);
  else if (pmlCoefVersion=="Ellipse")
    _pmlCoef = new genericPmlCoefEllipsoide(2, lPml, c, center, semiAxis);
  else if (pmlCoefVersion=="BeveledCuboid")
    _pmlCoef = new genericPmlCoefBeveledCuboid(2, lPml, c, center, semiAxis, radius, pmlCoefFuncVersion, pmlCoefFuncMulti);
  else if (pmlCoefVersion=="Sphere")
    _pmlCoef = new genericPmlCoefSphere(3, lPml, c, center, semiAxis[0]);
  else if (pmlCoefVersion=="Ellipsoide")
    _pmlCoef = new genericPmlCoefEllipsoide(3, lPml, c, center, semiAxis);
  else if (pmlCoefVersion=="EllipsoideLT")
    _pmlCoef = new genericPmlCoefEllipsoideLT(3, lPml, c, center, semiAxis);
  else
    Msg::Fatal("Dg/Wave: useGenericPmlCoef() not implemented for this 'pmlCoefVersion'.");
  setPmlCoef(_pmlCoef);
}

void dgConservationLawWave::useGenericPmlDir(const std::string pmlDirVersion,
                                             double centerX, double centerY, double centerZ,
                                             double semiAxisX, double semiAxisY, double semiAxisZ, double radius) {
  double center[3] = {centerX, centerY, centerZ};
  double semiAxis[3] = {semiAxisX, semiAxisY, semiAxisZ};
  if (pmlDirVersion=="Circle")
    _pmlDir = new genericPmlDirSphere(2, center);
  else if (pmlDirVersion=="Ellipse")
    _pmlDir = new genericPmlDirEllipsoide(2, center);
  else if (pmlDirVersion=="BeveledCuboid")
    _pmlDir = new genericPmlDirBeveledCuboid(2, center, semiAxis, radius);
  else if (pmlDirVersion=="Sphere")
    _pmlDir = new genericPmlDirSphere(3, center);
  else if (pmlDirVersion=="Ellipsoide")
    _pmlDir = new genericPmlDirEllipsoide(3, center);
  else if (pmlDirVersion=="EllipsoideLT")
    _pmlDir = new genericPmlDirEllipsoide(3, center);
  else
    Msg::Fatal("Dg/Wave: useGenericPmlDir() not implemented for this 'pmlDirVersion'.");
  setPmlDir(_pmlDir);
}


/*==============================================================================
 * Specific boundary conditions
 *============================================================================*/

class dgBoundaryConditionWave : public dgBoundaryCondition {
  class term : public function {
    int _DIM, _nbf, _typeBC;
    double theta;
    std::string _LayerVersion;
    fullMatrix<double> sol, normals, coef, pmlDir, extData;
   public:
    term(int DIM, int nbf, std::string LayerVersion, int typeBC, double fluxDecentering,
         const function *coefFunction,
         const function *pmlDirFunction,
         const function *extDataFunction) : function(nbf) {
      _DIM = DIM;
      _nbf = nbf;
      _LayerVersion = LayerVersion;
      _typeBC = typeBC;
      theta = fluxDecentering;
      setArgument (sol, function::getSolution());
      setArgument (normals, function::getNormals());
      setArgument (coef, coefFunction);
      if ((_LayerVersion!="WithoutLayer") && (_LayerVersion!="BasicLayer"))
        setArgument (pmlDir, pmlDirFunction);
      if (extDataFunction != NULL)
        setArgument (extData, extDataFunction);
    }
    void call (dataCacheMap *m, fullMatrix<double> &val) {
      val.scale(0.);
      size_t nQP = val.size1();
      for(size_t i=0; i<nQP; i++) {
        
        double c   = coef(i,0);
        double rho = coef(i,1);
        
        double n[3];
        for (int j=0; j<_DIM; j++)
          n[j] = normals(i,j);
        
        // Average and jump
        double pAverage = 0.;
        double pJump = 0.;
        double ndotuAverage = 0.;
        double ndotuJump = 0.;
        
        switch (_typeBC) {
          
          // Dirichlet BC on u with external fields  (extData contains 'uExt')
          case 1:
            if (theta < 0.01) {
              pAverage = sol(i,0);
              for (int j=0; j<_DIM; j++)
                ndotuAverage += n[j]*extData(i,j);
            }
            else {
              pAverage = sol(i,0);
              pJump    = 0.;
              for (int j=0; j<_DIM; j++) {
                ndotuAverage += n[j]*(extData(i,j)+sol(i,j+1))/2;
                ndotuJump    += n[j]*(extData(i,j)-sol(i,j+1))/2;
              }
            }
            break;
          
          // Dirichlet BC on u with flux  (extData contains 'ndotuExt')
          case 2:
            if (theta < 0.01) {
              pAverage     = sol(i,0);
              ndotuAverage = extData(i,0);
            }
            else {
              pAverage = sol(i,0);
              pJump    = 0.;
              ndotuAverage = extData(i,0)/2;
              ndotuJump    = extData(i,0)/2;
              for (int j=0; j<_DIM; j++) {
                ndotuAverage += n[j]*(sol(i,j+1))/2;
                ndotuJump    -= n[j]*(sol(i,j+1))/2;
              }
            }
            break;
          
          // Homogeneous Dirichlet BC on u  (extData is EMPTY)
          case 3:
            if (theta < 0.01) {
              pAverage     = sol(i,0);
              ndotuAverage = 0.;
            }
            else {
              pAverage = sol(i,0);
              pJump    = 0.;
              for (int j=0; j<_DIM; j++) {
                ndotuAverage += n[j]*(sol(i,j+1))/2;
                ndotuJump    -= n[j]*(sol(i,j+1))/2;
              }
            }
            break;
          
          // Dirichlet BC on p with external fields  (extData contains 'pExt')
          case 4:
            if (theta < 0.01) {
              pAverage = extData(i,0);
              for (int j=0; j<_DIM; j++)
                ndotuAverage += n[j]*sol(i,j+1);
            }
            else {
              pAverage = (extData(i,0)+sol(i,0))/2;
              pJump    = (extData(i,0)-sol(i,0))/2;
              for (int j=0; j<_DIM; j++)
                ndotuAverage += n[j]*sol(i,j+1);
            }
            break;
          
          // Homogeneous Dirichlet BC on p  (extData is EMPTY)
          case 5:
            if (theta < 0.01) {
              pAverage = 0.;
              for (int j=0; j<_DIM; j++)
                ndotuAverage += n[j]*sol(i,j+1);
            }
            else {
              pAverage =  sol(i,0)/2;
              pJump    = -sol(i,0)/2;
              for (int j=0; j<_DIM; j++)
                ndotuAverage += n[j]*sol(i,j+1);
            }
            break;
            
          // Open BC (extData is EMPTY)
          case 6:
            if (theta < 0.01) {
              ndotuAverage = 1/(rho*c) * sol(i,0);
              for (int j=0; j<_DIM; j++)
                pAverage += (rho*c) * n[j]*sol(i,j+1);
            }
            else {
              pAverage =  sol(i,0)/2;
              pJump    = -sol(i,0)/2;
              for (int j=0; j<_DIM; j++) {
                ndotuAverage += n[j]*(sol(i,j+1))/2;
                ndotuJump    -= n[j]*(sol(i,j+1))/2;
              }
            }
            /*
            pJump = -sol(i,0);
            for (int j=0; j<_DIM; j++)
              ndotuJump -= n[j]*sol(i,j+1);
            */
            break;
            
          // Open BC with inflow (extData contains all external fields) - Sommerfeld
          case 7:
            if (theta < 0.01) {
              pAverage     = extData(i,0);
              ndotuAverage = 1/(rho*c) * (sol(i,0)-extData(i,0));
              for (int j=0; j<_DIM; j++) {
                pAverage     += (rho*c) * n[j]*(sol(i,j+1)-extData(i,j+1));
                ndotuAverage += n[j]*extData(i,j+1);
              }
            }
            else {
              pAverage = (extData(i,0)+sol(i,0))/2;
              pJump    = (extData(i,0)-sol(i,0))/2;
              for (int j=0; j<_DIM; j++) {
                ndotuAverage += n[j]*(extData(i,j+1)+sol(i,j+1))/2;
                ndotuJump    += n[j]*(extData(i,j+1)-sol(i,j+1))/2;
              }
            }
            /*
            pJump = 2*extData(i,0)-sol(i,0);   //-1*...
            for (int j=0; j<_DIM; j++)
              ndotuJump += n[j]*(2*extData(i,j+1)-sol(i,j+1));   //-1*...
            */
            break;
          
          default:
            Msg::Error("Dg/Wave: There is an error in the BOUNDARY CONDITION of the WAVE conservation law (BAD '_typeBC': '%i')", _typeBC);
          
        }
        
        // Riemann solver
        double pRiemann     = pAverage     - theta * rho*c * ndotuJump;
        double ndotuRiemann = ndotuAverage - theta * 1/(rho*c) * pJump;
        
        /*
        if (_typeBC==7) {
          pRiemann += extData(i,0);
          for (int j=0; j<_DIM; j++)
            ndotuRiemann += n[j]*extData(i,j+1);
        }
        */
        
        // Numerical flux
        val(i,0) = -c*c*rho * ndotuRiemann;
        for (int j=0; j<_DIM; j++)
          val(i,j+1)  = -1/rho * n[j]*pRiemann;
        
        // Numerical flux of additionnal equation
        if ((_LayerVersion!="WithoutLayer") && (_LayerVersion!="BasicLayer") && (_LayerVersion!="PmlStraightLT") && (_LayerVersion!="PmlEllipsoideLT")) {
          double adotn=0.;
          for (int j=0; j<_DIM; j++)
            adotn += pmlDir(i,j) * n[j];
          if (fabs(adotn)>0.5) {
            // double qpJump = -sol(i,_DIM+1)/2;
            // val(i,_DIM+1) = c*c*rho * adotn * (ndotuJump + 1/(rho*c) * qpJump);
            val(i,_DIM+1) = -c*c*rho * ndotuRiemann; //c*c*rho * adotn * (ndotuJump + theta * 1/(rho*c) * pJump);
          }
        }
        
      }
    }
  };
  term _term;
 public:
  dgBoundaryConditionWave(dgConservationLawWave *claw, const std::string tag, int typeBC, const function *extDataFunction) :
      _term(claw->getDIM(),
            claw->getNbFields(),
            claw->getLayerVersion(),
            typeBC,
            claw->getFluxDecentering(),
            claw->getPhysCoef(tag),
            claw->getPmlDir(tag),
            extDataFunction) {
    _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawWave::newBoundaryDirichletOnU(const std::string tag, const function *uExtFunction) {
  return new dgBoundaryConditionWave(this,tag,1,uExtFunction);
}

dgBoundaryCondition *dgConservationLawWave::newBoundaryFluxU(const std::string tag, const function *ndotuExtFunction) {
  return new dgBoundaryConditionWave(this,tag,2,ndotuExtFunction);
}

dgBoundaryCondition *dgConservationLawWave::newBoundaryHomogeneousDirichletOnU(const std::string tag) {
  return new dgBoundaryConditionWave(this,tag,3,NULL);
}

dgBoundaryCondition *dgConservationLawWave::newBoundaryWall(const std::string tag) {
  return new dgBoundaryConditionWave(this,tag,3,NULL);
}

dgBoundaryCondition *dgConservationLawWave::newBoundaryDirichletOnP(const std::string tag, const function *pExtFunction) {
  return new dgBoundaryConditionWave(this,tag,4,pExtFunction);
}

dgBoundaryCondition *dgConservationLawWave::newBoundaryHomogeneousDirichletOnP(const std::string tag) {
  return new dgBoundaryConditionWave(this,tag,5,NULL);
}

dgBoundaryCondition *dgConservationLawWave::newBoundaryOpenOutflow(const std::string tag) {
  return new dgBoundaryConditionWave(this,tag,6,NULL);
}

dgBoundaryCondition *dgConservationLawWave::newBoundaryOpenOutflowWithInflow(const std::string tag, const function *fieldsExtFunction) {
  return new dgBoundaryConditionWave(this,tag,7,fieldsExtFunction);
}


/*==============================================================================
 * Specific interface conditions
 *============================================================================*/

// Interface term for the interface domain/shield

class dgConservationLawWave::interfaceSheetTerm : public function {
  int _DIM, _nbf, _thereIsShieldOnP, _thereIsShieldOnU,_thereIsShieldOnP1, _thereIsShieldOnU1;
  double theta,yS;
  fullMatrix<double> normals, solL, solR, coefL, coefR,coef;
 public:
  interfaceSheetTerm(int DIM, int nbf,int thereIsShieldOnP,int thereIsShieldOnU,int thereIsShieldOnP1,int thereIsShieldOnU1, double fluxDecentering, const function *coefFunction,double Ys) : function(2*nbf) {
    _DIM = DIM;
    _nbf = nbf;
    theta = fluxDecentering;
    yS=Ys;
    _thereIsShieldOnP = thereIsShieldOnP;
    _thereIsShieldOnU = thereIsShieldOnU;
    _thereIsShieldOnP1 = thereIsShieldOnP1;
    _thereIsShieldOnU1 = thereIsShieldOnU1;
    setArgument (normals, function::getNormals());
    setArgument (solL, function::getSolution(), 0);
    setArgument (solR, function::getSolution(), 1);
    setArgument (coefL, coefFunction, 0);
    setArgument (coefR, coefFunction, 1);
    setArgument (coef, coefFunction);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    
    for(size_t i=0; i<nQP; i++) {
      
      double n[3];
      for (int j=0; j<_DIM; j++)
        n[j] = normals(i,j);
      
      // Coefficients
      double cL = coefL(i,0), rhoL = coefL(i,1);
      double cR = coefR(i,0), rhoR = coefR(i,1);
      double zL = rhoL*cL ,zR = rhoR*cR;
      double pRiemannL = 0. , pRiemannR = 0., ndotuRiemannL = 0., ndotuRiemannR = 0.;
      double pLR =0, pRL =0,  ndotuRL =0, ndotuLR =0;
      // Solution
      double pL = solL(i,0), ndotuL = 0;
      double pR = solR(i,0), ndotuR = 0;
      for (int j=0; j<_DIM; j++) {
        ndotuL += n[j] * solL(i,j+1);
        ndotuR += n[j] * solR(i,j+1);
      }
      if(_thereIsShieldOnP){
        pRiemannL     = -(ndotuR - ndotuL)/(yS) ;
        pRiemannR     = -(ndotuR - ndotuL)/(yS) ;
        ndotuRiemannL = 2*(ndotuL*zL)/(zR + zL);
        ndotuRiemannR = 2*(ndotuR*zR)/(zR + zL);
      }
      if(_thereIsShieldOnU){
        pRiemannL     = 2*pL/zL/(1./zL + 1./zR);
        pRiemannR     = 2*pR/zR/(1./zR + 1./zL);
        ndotuRiemannL = 0.5*(ndotuL*zL+ndotuR*zR)/(zR + zL)-(pR - pL)/(yS)/2; ;
        ndotuRiemannR = 0.5*(ndotuR*zR+ndotuL*zL)/(zL + zL)-(pR - pL)/(yS)/2; ;

      }
      if(_thereIsShieldOnP1){
        ndotuLR=(yS)*(pR+pL)/2+ndotuR;
        ndotuRL=-(yS)*(pR+pL)/2+ndotuL;
        pRiemannL     = (pL/zL+pR/zR)/(1./zL + 1./zR)-0*(ndotuR - ndotuL)/(1./zL + 1./zR) ;           
        pRiemannR     = (pL/zL+pR/zR)/(1./zL + 1./zR)-0*(ndotuR - ndotuL)/(1./zL + 1./zR) ;     
        ndotuRiemannL = ndotuLR-0*(pR - pL)/ (zL + zR);                   
        ndotuRiemannR = ndotuRL-0*(pR - pL)/ (zL + zR);       
      }
      if(_thereIsShieldOnU1){
        pLR=(yS)*(ndotuR+ndotuL)/2+pR;
        pRL=-(yS)*(ndotuR+ndotuL)/2+pL;
        pRiemannL     = pLR;
        pRiemannR     = pRL;
        ndotuRiemannL = (ndotuL*zL+ndotuR*zR)/(zR + zL);
        ndotuRiemannR = (ndotuR*zR+ndotuL*zL)/(zR + zL);
      }
      // Fluxes for the left cell
      val(i,0) = -cL*cL*rhoL * ndotuRiemannL;
      for (int j=0; j<_DIM; j++)
        val(i,j+1) = -1/rhoL * n[j] * pRiemannL;
      
      // Fluxes for the right cell
      val(i,_nbf+0) = cR*cR*rhoR * ndotuRiemannR;
      for (int j=0; j<_DIM; j++)
        val(i,_nbf+j+1) = 1/rhoR * n[j] * pRiemannR;
      
    }
  }
};

void dgConservationLawWave::addInterfaceSheet(const std::string tag,double Ys) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  _interfaceTerm0[tag] = new interfaceSheetTerm(getDIM(),getNbFields(),_thereIsShieldOnP,_thereIsShieldOnU,_thereIsShieldOnP1,_thereIsShieldOnU1,getFluxDecentering(),getPhysCoef(tag),Ys);
}

class dgConservationLawWave::interfaceShieldTerm : public function {
  int _DIM, _nbf;
  fullMatrix<double> normals, solL, solR, coefL, coefR,coef,imptL,impsL,imptR,impsR;
 public:
  interfaceShieldTerm(int DIM, int nbf,const function *coefFunction, const function *f, const function *g) : function(2*nbf) {
    _DIM = DIM;
    _nbf = nbf;

    setArgument (normals, function::getNormals());
    setArgument (solL, function::getSolution(), 0);
    setArgument (solR, function::getSolution(), 1);
    setArgument (coefL, coefFunction, 0);
    setArgument (coefR, coefFunction, 1);
    setArgument (imptL, f, 0);
    setArgument (imptR, f, 1);
    setArgument (impsL, g, 0);
    setArgument (impsR, g, 1);


    setArgument (coef, coefFunction);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    
    for(size_t i=0; i<nQP; i++) {
      
      double n[3];
      for (int j=0; j<_DIM; j++)
        n[j] = normals(i,j);
      
      // Coefficients
      double cL = coefL(i,0), rhoL = coefL(i,1);
      double cR = coefR(i,0), rhoR = coefR(i,1);



      double pRiemannL = 0. , pRiemannR = 0., ndotuRiemannL = 0., ndotuRiemannR = 0.;
      double pLR =0, pRL =0;
      // Solution
      double  ndotuL = 0;
      double  ndotuR = 0;
      double  ndotimtL = 0;
      double  ndotimtR = 0;
      double  ndotimsL = 0;
      double  ndotimsR = 0;
      for (int j=0; j<_DIM; j++) {
        ndotuL += n[j] * solL(i,j+1);
        ndotuR += n[j] * solR(i,j+1);
      }
      for (int j=0; j<_DIM; j++) {
        ndotimtL += n[j] * imptL(i,j+1);
        ndotimtR += n[j] * imptR(i,j+1);
        ndotimsL += n[j] * impsL(i,j+1);
        ndotimsR += n[j] * impsR(i,j+1);

      }
     
        pLR=ndotimtR-ndotimsL;
        pRL=ndotimsR-ndotimtL;
        pRiemannL     = pRL;
        pRiemannR     = pLR;
        ndotuRiemannL = ndotuL;
        ndotuRiemannR = ndotuR;
      
      // Fluxes for the left cell
      val(i,0) = -cL*cL*rhoL * ndotuRiemannL;
      for (int j=0; j<_DIM; j++)
        val(i,j+1) = -1/rhoL * n[j] * pRiemannL;
      
      // Fluxes for the right cell
      val(i,_nbf+0) = cR*cR*rhoR * ndotuRiemannR;
      for (int j=0; j<_DIM; j++)
        val(i,_nbf+j+1) = 1/rhoR * n[j] * pRiemannR;
      }
    }
  
};

void dgConservationLawWave::addInterfaceShield(const std::string tag, const function *f, const function *g) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  _interfaceTerm0[tag] = new interfaceShieldTerm(getDIM(),getNbFields(),getPhysCoef(tag),f,g);
}

// Interface term for the interface domain/PML

class dgConservationLawWave::interfaceTermDomPml : public function {
  int _DIM, _nbf, _incidentFieldsDefined;
  std::string _LayerVersion;
  double theta;
  fullMatrix<double> normals;
  fullMatrix<double> solL, coefL, pmlDirL, incidentFieldsL;
  fullMatrix<double> solR, coefR, pmlDirR, incidentFieldsR;
 public:
  interfaceTermDomPml(int DIM, int nbf, std::string LayerVersion, double fluxDecentering,
                      const function *coefFunction,
                      const function *pmlDirFunction,
                      const function *incidentFieldsFunction) : function(2*nbf) {
    _DIM = DIM;
    _nbf = nbf;
    _LayerVersion = LayerVersion;
    theta = fluxDecentering;
    setArgument (normals, function::getNormals());
    setArgument (solL, function::getSolution(), 0);
    setArgument (solR, function::getSolution(), 1);
    setArgument (coefL, coefFunction, 0);
    setArgument (coefR, coefFunction, 1);
    setArgument (pmlDirL, pmlDirFunction, 0);
    setArgument (pmlDirR, pmlDirFunction, 1);
    _incidentFieldsDefined = 0;
    if (incidentFieldsFunction!=NULL) {
      setArgument (incidentFieldsL, incidentFieldsFunction, 0);
      setArgument (incidentFieldsR, incidentFieldsFunction, 1);
      _incidentFieldsDefined = 1;
    }
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double n[3];
      for (int j=0; j<_DIM; j++)
        n[j] = normals(i,j);
      
      // Coefficients
      double cL = coefL(i,0), rhoL = coefL(i,1);
      double cR = coefR(i,0), rhoR = coefR(i,1);
      double aL[3] = {0., 0., 0.};
      double aR[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++) {
        aL[j] += pmlDirL(i,j);
        aR[j] += pmlDirR(i,j);
      }
      double ndotuIncL=0.;
      double ndotuIncR=0.;
      if (_incidentFieldsDefined) {
        for (int j=0; j<_DIM; j++) {
          ndotuIncL += n[j]*incidentFieldsL(i,j+1);
          ndotuIncR += n[j]*incidentFieldsR(i,j+1);
        }
      }
      
      // Solution
      double pL = solL(i,0), ndotuL = 0;
      double pR = solR(i,0), ndotuR = 0;
      for (int j=0; j<_DIM; j++) {
        ndotuL += n[j] * solL(i,j+1);
        ndotuR += n[j] * solR(i,j+1);
      }
      
      // Numerical flux (with Riemann)
      double pRiemann     = ((pR/(rhoR*cR) + pL/(rhoL*cL))     - theta * (ndotuR - ndotuL))/(1./(rhoR*cR) + 1./(rhoL*cL));
      double ndotuRiemann = ((ndotuR*rhoR*cR + ndotuL*rhoL*cL) - theta * (pR - pL))/(rhoR*cR + rhoL*cL);
      
      val(i,0) = -cL*cL*rhoL * ndotuRiemann;
      for (int j=0; j<_DIM; j++)
        val(i,j+1) = -1/rhoL * n[j] * pRiemann;
      
      val(i,_nbf+0) = cR*cR*rhoR * ndotuRiemann;
      for (int j=0; j<_DIM; j++)
        val(i,_nbf+j+1) = 1/rhoR * n[j] * pRiemann;
      
      // Numerical flux for additionnal equations (with Riemann)
      if ((_LayerVersion=="WithoutLayer") || (_LayerVersion=="BasicLayer"))
        Msg::Fatal("Dg/Wave: Don't use 'addInterfacePml(...)' with 'WithoutLayer' or 'BasicLayer'.");
      
      if (norm3(aL)>0.5)  // Left
        val(i,_DIM+1) = - (cL*cL*rhoL * (ndotuRiemann - ndotuIncL));
      if (norm3(aR)>0.5)  // Right
        val(i,_nbf+_DIM+1) = cR*cR*rhoR * (ndotuRiemann - ndotuIncR);
      if ((norm3(aL)<=0.5) && (norm3(aR)<=0.5)) {
        val(i,_DIM+1) = - (cL*cL*rhoL * (ndotuRiemann - ndotuIncL));
        val(i,_nbf+_DIM+1) = cR*cR*rhoR * (ndotuRiemann - ndotuIncR);
      }
      
    }
  }
};

void dgConservationLawWave::addInterfacePml(const std::string tag, const function *incidentFieldsFunction) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  _interfaceTerm0[tag] = new interfaceTermDomPml(getDIM(),getNbFields(),getLayerVersion(),getFluxDecentering(),getPhysCoef(tag),getPmlDir(tag),incidentFieldsFunction);
}

// Interface term for the interface domain/PML with incident fields  (transition incident/scattered fields)

class dgConservationLawWave::interfaceTermDomPmlWithInflow : public function {
  int _DIM, _nbf, _incidentFieldsDefined;
  std::string _LayerVersion;
  double theta;
  fullMatrix<double> normals;
  fullMatrix<double> solL, coefL, pmlDirL, incidentFieldsL;
  fullMatrix<double> solR, coefR, pmlDirR, incidentFieldsR;
 public:
  interfaceTermDomPmlWithInflow(int DIM, int nbf, std::string LayerVersion, double fluxDecentering,
                                const function *coefFunction,
                                const function *pmlDirFunction,
                                const function *incidentFieldsFunction) : function(2*nbf) {
    _DIM = DIM;
    _nbf = nbf;
    _LayerVersion = LayerVersion;
    theta = fluxDecentering;
    setArgument (normals, function::getNormals());
    setArgument (solL, function::getSolution(), 0);
    setArgument (solR, function::getSolution(), 1);
    setArgument (coefL, coefFunction, 0);
    setArgument (coefR, coefFunction, 1);
    setArgument (pmlDirL, pmlDirFunction, 0);
    setArgument (pmlDirR, pmlDirFunction, 1);
    _incidentFieldsDefined = 0;
    if (incidentFieldsFunction!=NULL) {
      setArgument (incidentFieldsL, incidentFieldsFunction, 0);
      setArgument (incidentFieldsR, incidentFieldsFunction, 1);
      _incidentFieldsDefined = 1;
    }
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double n[3];
      for (int j=0; j<3; j++)
        n[j] = normals(i,j);
      
      // Coefficients
      double cL = coefL(i,0), rhoL = coefL(i,1);
      double cR = coefR(i,0), rhoR = coefR(i,1);
      
      // Fields
      double pL = solL(i,0), uL[3] = {0., 0., 0.};
      double pR = solR(i,0), uR[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++) {
        uL[j] = solL(i,j+1);
        uR[j] = solR(i,j+1);
      }
      double ndotuL = scalProd(n,uL);
      double ndotuR = scalProd(n,uR);
      
      // Stretching direction
      double aL[3] = {0., 0., 0.};
      double aR[3] = {0., 0., 0.};
      for (int j=0; j<_DIM; j++) {
        aL[j] = pmlDirL(i,j);
        aR[j] = pmlDirR(i,j);
      }
      
      // Incident fields
      double pIncL = 0., ndotuIncL = 0.;
      double pIncR = 0., ndotuIncR = 0.;
      if (_incidentFieldsDefined) {
        pIncL = incidentFieldsL(i,0);
        pIncR = incidentFieldsR(i,0);
        for (int j=0; j<_DIM; j++) {
          ndotuIncL += n[j]*incidentFieldsL(i,j+1);
          ndotuIncR += n[j]*incidentFieldsR(i,j+1);
        }
      }
      
      // Data at the interface
      double a[3], pInc, ndotuInc;
      if (norm3(aL)<-0.5) {         // BUG ?
        for (int j=0; j<3; j++)
          a[j] = aR[j];
        pInc = pIncR;
        ndotuInc = ndotuIncR;
      }
      else {
        for (int j=0; j<3; j++)
          a[j] = aL[j];
        pInc = pIncL;
        ndotuInc = ndotuIncL;
      }
      
      // Numerical flux
      double pRiemannL, ndotuRiemannL;
      double pRiemannR, ndotuRiemannR;
      
      if (scalProd(n,a)>0.5) {  // (L=domain, R=pml)
        
        // With total fields
        pRiemannL     = (((pR+pInc)/(rhoR*cR) + pL/(rhoL*cL))         - theta * ((ndotuR+ndotuInc) - ndotuL))/(1./(rhoR*cR) + 1./(rhoL*cL));
        ndotuRiemannL = (((ndotuR+ndotuInc)*rhoR*cR + ndotuL*rhoL*cL) - theta * ((pR+pInc) - pL))/(rhoR*cR + rhoL*cL);
        
        // With scattered fields
        pRiemannR     = ((pR/(rhoR*cR) + (pL-pInc)/(rhoL*cL))         - theta * (ndotuR - (ndotuL-ndotuInc)))/(1./(rhoR*cR) + 1./(rhoL*cL));
        ndotuRiemannR = ((ndotuR*rhoR*cR + (ndotuL-ndotuInc)*rhoL*cL) - theta * (pR - (pL-pInc)))/(rhoR*cR + rhoL*cL);
        
      }
      else {  // (L=pml, R=domain)
        
        // With scattered fields
        pRiemannL     = (((pR-pInc)/(rhoR*cR) + pL/(rhoL*cL))         - theta * ((ndotuR-ndotuInc) - ndotuL))/(1./(rhoR*cR) + 1./(rhoL*cL));
        ndotuRiemannL = (((ndotuR-ndotuInc)*rhoR*cR + ndotuL*rhoL*cL) - theta * ((pR-pInc) - pL))/(rhoR*cR + rhoL*cL);
        
        // With total fields
        pRiemannR     = ((pR/(rhoR*cR) + (pL+pInc)/(rhoL*cL))         - theta * (ndotuR - (ndotuL+ndotuInc)))/(1./(rhoR*cR) + 1./(rhoL*cL));
        ndotuRiemannR = ((ndotuR*rhoR*cR + (ndotuL+ndotuInc)*rhoL*cL) - theta * (pR - (pL+pInc)))/(rhoR*cR + rhoL*cL);
        
      }
      
      val(i,0) = -cL*cL*rhoL * ndotuRiemannL;      // Flux vers la droite (cause de gauche) - Champs total
      for (int j=0; j<_DIM; j++)
        val(i,j+1) = -1/rhoL * n[j] * pRiemannL;
      
      val(i,_nbf+0) = cR*cR*rhoR * ndotuRiemannR;  // Flux vers la gauche (case de droite) - Champs diffracté
      for (int j=0; j<_DIM; j++)
        val(i,_nbf+j+1) = 1/rhoR * n[j] * pRiemannR;
      
      if ((_LayerVersion!="WithoutLayer") && (_LayerVersion!="BasicLayer")){
        if (scalProd(n,a)>0.5)
          val(i,_nbf+_DIM+1) = cR*cR*rhoR * ndotuRiemannR;
        else
          val(i,_DIM+1) = - (cL*cL*rhoL * ndotuRiemannL);
      }
      
    }
  }
};

void dgConservationLawWave::addInterfacePmlWithInflow(const std::string tag, const function *incidentFieldsFunction) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  _interfaceTerm0[tag] = new interfaceTermDomPmlWithInflow(getDIM(),getNbFields(),getLayerVersion(),getFluxDecentering(),getPhysCoef(tag),getPmlDir(tag),incidentFieldsFunction);
}

void dgConservationLawWave::addInterfaceScatteredTotalFields(const std::string tag, const function *incidentFieldsFunction, const function *directionToScattFunction) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  _interfaceTerm0[tag] = new interfaceTermDomPmlWithInflow(getDIM(),getNbFields(),getLayerVersion(),getFluxDecentering(),getPhysCoef(tag),directionToScattFunction,incidentFieldsFunction);
}


/*==============================================================================
 * Specific interface conditions for uncoupled region (BC on both sides)
 *============================================================================*/

class dgConservationLawWave::interfaceTermUncoupledRegions : public function {
  int _DIM, _nbf, _typeBC;
  std::string _LayerVersion;
  double theta;
  fullMatrix<double> normals;
  fullMatrix<double> solL, coefL, pmlDirL, extDataL;
  fullMatrix<double> solR, coefR, pmlDirR, extDataR;
 public:
  interfaceTermUncoupledRegions(int DIM, int nbf, std::string LayerVersion, int typeBC, double fluxDecentering,
                                const function *coefFunction,
                                const function *pmlDirFunction,
                                const function *extDataFunction) : function(2*nbf) {
    _DIM = DIM;
    _nbf = nbf;
    _LayerVersion = LayerVersion;
    _typeBC = typeBC;
    theta = fluxDecentering;
    setArgument (normals, function::getNormals());
    setArgument (solL, function::getSolution(), 0);
    setArgument (solR, function::getSolution(), 1);
    setArgument (coefL, coefFunction, 0);
    setArgument (coefR, coefFunction, 1);
    if (_LayerVersion != "WithoutLayer") {
      setArgument (pmlDirL, pmlDirFunction, 0);
      setArgument (pmlDirR, pmlDirFunction, 1);
    }
    if (extDataFunction != NULL) {
      setArgument (extDataL, extDataFunction, 0);
      setArgument (extDataR, extDataFunction, 1);
    }
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double n[3];
      for (int j=0; j<_DIM; j++)
        n[j] = normals(i,j);
      
      // Coefficients
      double cL = coefL(i,0), rhoL = coefL(i,1);
      double cR = coefR(i,0), rhoR = coefR(i,1);
      
      // Average and Jump
      double pAverageL = 0., pJumpL = 0., ndotuAverageL = 0., ndotuJumpL = 0.;
      double pAverageR = 0., pJumpR = 0., ndotuAverageR = 0., ndotuJumpR = 0.;
      
      switch (_typeBC) {
        
        // Homogeneous Dirichlet BC on u  (extData is EMPTY)
        case 3:
          if (theta < 0.01) {
            pAverageL = solL(i,0);
            pAverageR = solR(i,0);
            ndotuAverageL = 0.;
            ndotuAverageR = 0.;
          }
          else {
            pAverageL = solL(i,0);
            pAverageR = solR(i,0);
            pJumpL = 0.;
            pJumpR = 0.;
            for (int j=0; j<_DIM; j++) {
              ndotuAverageL += n[j]*(solL(i,j+1))/2;
              ndotuAverageR += n[j]*(solR(i,j+1))/2;
              ndotuJumpL -= n[j]*(solL(i,j+1))/2;
              ndotuJumpR += n[j]*(solR(i,j+1))/2;
            }
          }
          break;
          
        // Dirichlet BC on p with external fields  (extData contains 'pExt')
        case 4:
          if (theta < 0.01) {
            pAverageL = extDataR(i,0);
            pAverageR = extDataL(i,0);
            for (int j=0; j<_DIM; j++) {
              ndotuAverageL += n[j]*(solL(i,j+1));
              ndotuAverageR += n[j]*(solR(i,j+1));
            }
          }
          else {
            pAverageL = (extDataR(i,0)+solL(i,0))/2;
            pAverageR = (solR(i,0)+extDataL(i,0))/2;
            pJumpL = (extDataR(i,0)-solL(i,0))/2;
            pJumpR = (solR(i,0)-extDataL(i,0))/2;
            for (int j=0; j<_DIM; j++) {
              ndotuAverageL += n[j]*solL(i,j+1);
              ndotuAverageR += n[j]*solR(i,j+1);
            }
            ndotuJumpL = 0.;
            ndotuJumpR = 0.;
          }
          break;
        
        // Homogeneous Dirichlet BC on p  (extData is EMPTY)
        case 5:
          if (theta < 0.01) {
            pAverageL = 0.;
            pAverageR = 0.;
            for (int j=0; j<_DIM; j++) {
              ndotuAverageL += n[j]*(solL(i,j+1));
              ndotuAverageR += n[j]*(solR(i,j+1));
            }
          }
          else {
            pAverageL =  solL(i,0)/2;
            pAverageR =  solR(i,0)/2;
            pJumpL = -solL(i,0)/2;
            pJumpR =  solR(i,0)/2;
            for (int j=0; j<_DIM; j++) {
              ndotuAverageL += n[j]*solL(i,j+1);
              ndotuAverageR += n[j]*solR(i,j+1);
            }
            ndotuJumpL = 0.;
            ndotuJumpR = 0.;
          }
          break;
          
        // Open BC (extData is EMPTY)
        case 6:
          if (theta < 0.01) {
            ndotuAverageL =  1/(rhoL*cL) * solL(i,0);
            ndotuAverageR = -1/(rhoR*cR) * solR(i,0);
            for (int j=0; j<_DIM; j++) {
              pAverageL += (rhoL*cL) * n[j]*solL(i,j+1);
              pAverageR -= (rhoR*cR) * n[j]*solR(i,j+1);
            }
          }
          else {
            pAverageL =  solL(i,0)/2;
            pAverageR =  solR(i,0)/2;
            pJumpL    = -solL(i,0)/2;
            pJumpR    =  solR(i,0)/2;
            for (int j=0; j<_DIM; j++) {
              ndotuAverageL += n[j]*(solL(i,j+1))/2;
              ndotuAverageR += n[j]*(solR(i,j+1))/2;
              ndotuJumpL    -= n[j]*(solL(i,j+1))/2;
              ndotuJumpR    += n[j]*(solR(i,j+1))/2;
            }
          }
          /*
          pJumpL = -solL(i,0);
          pJumpR =  solR(i,0);
          for (int j=0; j<_DIM; j++) {
            ndotuJumpL -= n[j]*solL(i,j+1);
            ndotuJumpR += n[j]*solR(i,j+1);
          }
          */
          break;
        
        // Open BC with inflow (extData contains all external fields) - Sommerfeld
        case 7:
          if (theta < 0.01) {
            pAverageL     = extDataR(i,0);
            pAverageR     = extDataL(i,0);
            ndotuAverageL = 1/(rhoL*cL) * (solL(i,0)-extDataR(i,0));
            ndotuAverageR = 1/(rhoR*cR) * (extDataL(i,0)-solR(i,0));
            for (int j=0; j<_DIM; j++) {
              pAverageL     += (rhoL*cL) * n[j]*(solL(i,j+1)-extDataR(i,j+1));
              pAverageR     += (rhoR*cR) * n[j]*(extDataL(i,j+1)-solR(i,j+1));
              ndotuAverageL += n[j]*extDataR(i,j+1);
              ndotuAverageR += n[j]*extDataL(i,j+1);
            }
          }
          else {
            pAverageL = (extDataR(i,0)+solL(i,0))/2;
            pAverageR = (solR(i,0)+extDataL(i,0))/2;
            pJumpL    = (extDataR(i,0)-solL(i,0))/2;
            pJumpR    = (solR(i,0)-extDataL(i,0))/2;
            for (int j=0; j<_DIM; j++) {
              ndotuAverageL += n[j]*(extDataR(i,j+1)+solL(i,j+1))/2;
              ndotuAverageR += n[j]*(solR(i,j+1)+extDataL(i,j+1))/2;
              ndotuJumpL    += n[j]*(extDataR(i,j+1)-solL(i,j+1))/2;
              ndotuJumpR    += n[j]*(solR(i,j+1)-extDataL(i,j+1))/2;
            }
          }
          break;
        
        default:
          Msg::Error("Dg/Wave: There is an error in the INTERFACE CONDITION for UNCOUPLED REGIONS of the WAVE conservation law (BAD '_typeBC': '%i')", _typeBC);
        
      }
      
      // Numerical flux (with Riemann)
      
      double pRiemannL     = pAverageL     - theta * rhoL*cL * ndotuJumpL;
      double ndotuRiemannL = ndotuAverageL - theta * 1/(rhoL*cL) * pJumpL;
      
      double pRiemannR     = pAverageR     - theta * rhoR*cR * ndotuJumpR;
      double ndotuRiemannR = ndotuAverageR - theta * 1/(rhoR*cR) * pJumpR;
      
      val(i,0) = -cL*cL*rhoL * ndotuRiemannL;
      for (int j=0; j<_DIM; j++)
        val(i,j+1) = -1/rhoL * n[j] * pRiemannL;
      
      val(i,_nbf+0) = cR*cR*rhoR * ndotuRiemannR;
      for (int j=0; j<_DIM; j++)
        val(i,_nbf+j+1) = 1/rhoR * n[j] * pRiemannR;
      
      
      // Numerical flux for additionnal equation (with Riemann)
      if ((_LayerVersion!="WithoutLayer") && (_LayerVersion!="BasicLayer")) {
        
        double aL[3] = {0., 0., 0.};
        double aR[3] = {0., 0., 0.};
        for (int j=0; j<_DIM; j++) {
          aL[j] = pmlDirL(i,j);
          aR[j] = pmlDirR(i,j);
        }
        
        if (scalProd(aL,n)>0.5)
          val(i,_DIM+1) = -cL*cL*rhoL * ndotuRiemannL;
        
        if (scalProd(aR,n)<-0.5)
          val(i,_nbf+_DIM+1) = -(-cR*cR*rhoR * ndotuRiemannR);
        
      }
      
    }
  }
};

void dgConservationLawWave::addInterfaceHomogeneousDirichletOnU(const std::string tag) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  _interfaceTerm0[tag] = new interfaceTermUncoupledRegions(getDIM(),getNbFields(),getLayerVersion(),3,getFluxDecentering(),getPhysCoef(tag),getPmlDir(),NULL);
}

void dgConservationLawWave::addInterfaceWall(const std::string tag) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  _interfaceTerm0[tag] = new interfaceTermUncoupledRegions(getDIM(),getNbFields(),getLayerVersion(),3,getFluxDecentering(),getPhysCoef(tag),getPmlDir(),NULL);
}

void dgConservationLawWave::addInterfaceDirichletOnP(const std::string tag, const function *fieldsExtFunction) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  _interfaceTerm0[tag] = new interfaceTermUncoupledRegions(getDIM(),getNbFields(),getLayerVersion(),4,getFluxDecentering(),getPhysCoef(tag),getPmlDir(),fieldsExtFunction);
}

void dgConservationLawWave::addInterfaceHomogeneousDirichletOnP(const std::string tag) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  _interfaceTerm0[tag] = new interfaceTermUncoupledRegions(getDIM(),getNbFields(),getLayerVersion(),5,getFluxDecentering(),getPhysCoef(tag),getPmlDir(),NULL);
}

void dgConservationLawWave::addInterfaceOpenOutflow(const std::string tag) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  _interfaceTerm0[tag] = new interfaceTermUncoupledRegions(getDIM(),getNbFields(),getLayerVersion(),6,getFluxDecentering(),getPhysCoef(tag),getPmlDir(),NULL);
}

void dgConservationLawWave::addInterfaceOpenOutflowWithInflow(const std::string tag, const function *fieldsExtFunction) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  _interfaceTerm0[tag] = new interfaceTermUncoupledRegions(getDIM(),getNbFields(),getLayerVersion(),7,getFluxDecentering(),getPhysCoef(tag),getPmlDir(),fieldsExtFunction);
}


/*==============================================================================
 * Compute information
 *============================================================================*/

class dgConservationLawWave::energyFunction : public function {
  int _DIM;
  fullMatrix<double> sol, coef;
 public:
  energyFunction(int DIM,
                 function *solutionFunction,
                 const function *coefFunction) : function(1) {
    _DIM = DIM;
    setArgument (sol, solutionFunction);
    setArgument (coef, coefFunction);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      double c   = coef(i,0);
      double rho = coef(i,1);
      
      double normp = pow(sol(i,0),2);
      double normu = 0.;
      for (int j=0; j<_DIM; j++)
        normu += pow(sol(i,j+1),2);
      
      val(i,0) = 0.5 * (1/(rho*c*c) * normp + rho * normu);
    }
  }
};

class dgConservationLawWave::energyErrorFunction : public function {
  int _DIM;
  fullMatrix<double> sol, solRef, coef;
 public:
  energyErrorFunction(int DIM,
                      function *solutionFunction,
                      function *solutionRefFunction,
                      const function *coefFunction) : function(1) {
    _DIM = DIM;
    setArgument (sol, solutionFunction);
    setArgument (solRef, solutionRefFunction);
    setArgument (coef, coefFunction);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      double c   = coef(i,0);
      double rho = coef(i,1);
      
      double errP = pow(sol(i,0)-solRef(i,0),2);
      double errU = 0.;
      for (int j=0; j<_DIM; j++)
        errU += pow(sol(i,j+1)-solRef(i,j+1),2);
      
      val(i,0) = 0.5 * (1/(rho*c*c) * errP + rho * errU);
    }
  }
};

double dgConservationLawWave::getTotalEnergy(dgGroupCollection *groups,
                                             function *solutionFunc,
                                             std::string tag,
                                             int intOrder) {
  function *_energyFunction = new energyFunction(getDIM(),solutionFunc,getPhysCoef(tag));
  fullMatrix<double> value(1,1);
  dgFunctionIntegrator _totalEnergy(groups, _energyFunction);
  _totalEnergy.compute(value, tag, intOrder);
  delete _energyFunction;
  return value.get(0,0);
}

double dgConservationLawWave::getTotalEnergy(dgGroupCollection *groups,
                                             dgDofContainer *solutionDof,
                                             std::string tag,
                                             int intOrder) {
  function *_energyFunction = new energyFunction(getDIM(),solutionDof->getFunction(),getPhysCoef(tag));
  fullMatrix<double> value(1,1);
  dgFunctionIntegrator _totalEnergy(groups, _energyFunction);
  _totalEnergy.compute(value, tag, intOrder);
  delete _energyFunction;
  return value.get(0,0);
}

double dgConservationLawWave::getTotalEnergyError(dgGroupCollection *groups,
                                                  dgDofContainer *solutionDof,
                                                  function *solutionRefFunc,
                                                  std::string tag,
                                                  int intOrder) {
  function *_energyErrorFunction = new energyErrorFunction(getDIM(),solutionDof->getFunction(),solutionRefFunc,getPhysCoef(tag));
  fullMatrix<double> value(1,1);
  dgFunctionIntegrator _totalEnergyError(groups, _energyErrorFunction);
  _totalEnergyError.compute(value, tag, intOrder);
  delete _energyErrorFunction;
  return value.get(0,0);
}

double dgConservationLawWave::getTotalEnergyError(dgGroupCollection *groups,
                                                  dgDofContainer *solutionDof,
                                                  dgDofContainer *solutionRefDof,
                                                  std::string tag,
                                                  int intOrder) {
  function *_energyErrorFunction = new energyErrorFunction(getDIM(),solutionDof->getFunction(),solutionRefDof->getFunction(),getPhysCoef(tag));
  fullMatrix<double> value(1,1);
  dgFunctionIntegrator _totalEnergyError(groups, _energyErrorFunction);
  _totalEnergyError.compute(value, tag, intOrder);
  delete _energyErrorFunction;
  return value.get(0,0);
}

double dgConservationLawWave::getVolume(dgGroupCollection *groups, std::string tag) {
  function *_unitFunction = new functionConstant(1.);
  fullMatrix<double> value(1,1);
  dgFunctionIntegrator _totalVolume(groups, _unitFunction);
  _totalVolume.compute(value, tag);
  delete _unitFunction;
  return value.get(0,0);
}



