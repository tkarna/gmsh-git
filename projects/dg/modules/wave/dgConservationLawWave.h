#ifndef _DG_CONSERVATION_LAW_WAVE_H
#define _DG_CONSERVATION_LAW_WAVE_H

class dgDofContainer;
class dgGroupCollection;
#include "dgConservationLawFunction.h"

/*
  Conservation law for Wave Propagation:
    \begin{align}
      \partial_t(p)       &= - \rho*c^2 \nabla\cdot\vec{u} - \sigma_p p \\
      \partial_t(\vec{u}) &= - \frac{1}{\rho} \nabla p - \sigma_u \vec{u}
    \end{align}
  
  With stabilization PSPG-terms for CG:
    \begin{align}
      \partial_t(p)       &= - \rho*c^2 \nabla\cdot\vec{u} - \sigma_p p - \nabla\cdot[Residual(Momentum equation)] \\
      \partial_t(\vec{u}) &= - \frac{1}{\rho} \nabla p - \sigma_u \vec{u}
    \end{align}
  
  With Straight PML:
    \begin{align}
      \partial_t(p)       &= - \rho*c^2 \nabla\cdot\vec{u}                                    - \sigmaPml q \\
      \partial_t(\vec{u}) &= - \frac{1}{\rho} \nabla p                                        - \sigmaPml \vec{a}(\vec{a}\cdot(\vec{u}-\vec{u}_e)) \\
      \partial_t(q)       &= - \rho*c^2 \nabla\cdot[\vec{a}(\vec{a}\cdot(\vec{u}-\vec{u}_e))] - \sigmaPml q
    \end{align}
  
  With Circle/Spherical PML:
    \begin{align}
      \partial_t(p)       &= - \rho*c^2 \nabla\cdot\vec{u} - \sigmaPmlN q - \sigmaBarT (p-q) \\
      \partial_t(\vec{u}) &= - \frac{1}{\rho} \nabla p - \sigmaPmlN \vec{a}(\vec{a}\cdot\vec{u}) - \sigmaBarT [\vec{u} - \vec{a}(\vec{a}\cdot\vec{u})] \\
      \partial_t(q)       &= - \rho*c^2 \nabla\cdot[\vec{a}(\vec{a}\cdot(\vec{u}-\vec{u}_e))] - \sigmaPmlN q
    \end{align}
  
  With Ellipsoidal PML:
    \begin{align}
      \partial_t(p)       &= - \rho*c^2 \nabla\cdot\vec{u} - \sigmaPmlN qN
                                                           - \sigmaPmlT qT
                                                           - \sigmaPmlB (p-qN-qT) \\
      \partial_t(\vec{u}) &= - \frac{1}{\rho} \nabla p - \sigmaPmlN \vec{aN}(\vec{aN}\cdot\vec{u})
                                                       - \sigmaPmlT \vec{aT}(\vec{aT}\cdot\vec{u})
                                                       - \sigmaPmlB \vec{aB}(\vec{aB}\cdot\vec{u}) \\
      \partial_t(qN)      &= - \rho*c^2 \nabla\cdot[\vec{aN}(\vec{aN}\cdot\vec{u})] - \sigmaPmlN qN \\
      \partial_t(qT)      &= - \rho*c^2 \nabla\cdot[\vec{aT}(\vec{aT}\cdot\vec{u})] - \sigmaPmlT qT
    \end{align}
  
  Users/Developers:
    Axel Modave <a.modave@ulg.ac.be>
    Abelin Kameni <abelin.kameni@lgep.supelec.fr>
    Mohamed Boubekeur <mohamed.boubekeur@lgep.supelec.fr>
*/

class dgConservationLawWave : public dgConservationLawFunction {
  
  int _DIM;
  int _NumMethod;              // 1 for DG, 2 for CG
  std::string _LayerVersion;   // 'WithoutLayer',
                               // 'BasicLayer' (not a PML),                 => BasicLayer
                               // 'PmlStraight'                             => PmlMain (0 cruvature)
                               // 'PmlCircle', 'PmlEllipse', 'PmlSphere'    => PmlMain (1 curvature)
                               // 'PmlEllipsoide'                           => PmlFull (2 curvatures)
                               // 'PmlStraightLT'
                               // 'PmlEllipsoideLT'
  
  int _thereIsDissipationOnP;  // 1 (yes) and 0 (no)
  int _thereIsDissipationOnU;  // 1 (yes) and 0 (no)
  int _thereIsShieldOnP;       // 1 (yes) and 0 (no)
  int _thereIsShieldOnU;       // 1 (yes) and 0 (no)
  int _thereIsShieldOnP1;       // 1 (yes) and 0 (no)
  int _thereIsShieldOnU1;       // 1 (yes) and 0 (no)
  
  double _fluxDecentering;     // 1. (upwind), 0. (centred), ...
  
  // List of physical tags that are used
  std::vector<std::string> _tagList;
  
  // Coefficients and input fields
  termMap _physCoefMap;           // Physical coefficients (c, rho, sigma_p, sigma_u)
  termMap _incidentFieldsMap;     // Incident fields
  termMap _pmlCoefMap;            // PML: absorption coefficient (scalar)
  termMap _pmlDirMap;             // PML: stretching direction (vector)
  termMap _numCoefMap;            // CG: numerical coefficient
  termMap _prevSolutionMap;         
  function *_pmlCoef;
  function *_pmlDir;
  class genericPmlCoefBeveledCuboid;
  class genericPmlCoefSphere;
  class genericPmlCoefEllipsoide;
  class genericPmlCoefEllipsoideLT;
  class genericPmlDirSphere;
  class genericPmlDirEllipsoide;
  class genericPmlDirBeveledCuboid;
  
  // DG-terms
  class sourceTerm;
  class sourceTermPml;
  class volumeTerm;
  class volumeTermPml;
  class interfaceTerm;
  class interfaceSheetTerm;
  class interfaceShieldTerm;
  class interfaceTermPml;
  class interfaceTermDomPml;
  class interfaceTermDomPmlWithInflow;
  class interfaceTermUncoupledRegions;
  class maxConvectiveSpeed;
  
  // CG-term
  class volumeTermCG;
  
 public:
  dgConservationLawWave(int=2, std::string="WithoutLayer");    // Default: 2D problem, Without PML
  void setup();
  ~dgConservationLawWave();
  
  int getDIM()                  const {return _DIM;}
  int getNumMethod()                  {return _NumMethod;}
  std::string getLayerVersion() const {return _LayerVersion;}
  
  // Choice of the numerical method
  inline void useDG() { _NumMethod = 1; }
  inline void useCG() { _NumMethod = 2; }
  
  // Use dissipation
  inline void useDissipationOnP() { _thereIsDissipationOnP = 1; }
  inline void useDissipationOnU() { _thereIsDissipationOnU = 1; }

  // Use interface shield 
  inline void useInterfaceShieldOnP() { _thereIsShieldOnP = 1; }
  inline void useInterfaceShieldOnU() { _thereIsShieldOnU = 1; }
  inline void useInterfaceShieldOnP1() { _thereIsShieldOnP1 = 1; }
  inline void useInterfaceShieldOnU1() { _thereIsShieldOnU1 = 1; }
  // Add a physical tag in the list of tags that are used
  int addToTagList(const std::string tag);
  int isInTagList(const std::string tag);
  
  // Set/get decentering of the numerical fluxes
  void setFluxDecentering(double value) {_fluxDecentering = value;}
  double getFluxDecentering() {return _fluxDecentering;}
  
  // Set/get parameters and/or input fields for a physical tag
  const function *getFunctionForTag2(const termMap &, const std::string) const;
  
  void setPhysCoef(const function *f)       {_physCoefMap[""] = f;}
  void setPmlCoef(const function *f)        {_pmlCoefMap[""] = f;}
  void setPmlDir(const function *f)         {_pmlDirMap[""] = f;}
  void setNumCoef(const function *f)        {_numCoefMap[""] = f;}
  void setPrevSolution(const function *f)   {_prevSolutionMap[""] = f;}

  
  void setPhysCoef(const std::string tag, const function *f)       {_physCoefMap[tag] = f;   addToTagList(tag);}
  void setPmlCoef(const std::string tag, const function *f)        {_pmlCoefMap[tag] = f;    addToTagList(tag);}
  void setPmlDir(const std::string tag, const function *f)         {_pmlDirMap[tag] = f;     addToTagList(tag);}
  void setNumCoef(const std::string tag, const function *f)        {_numCoefMap[tag] = f;    addToTagList(tag);}
  void setPrevSolution(const std::string tag, const function *f)   {_prevSolutionMap[tag] = f;   addToTagList(tag);}


  void useGenericPmlCoef(const std::string, double layerTickness, double c,
                         double centerX=0., double centerY=0., double centerZ=0.,
                         double semiAxisX=-1., double semiAxisY=-1., double semiAxisZ=-1., double param=0.,
                         const std::string pmlCoefFuncVersion="HypSh", double pmlCoefFuncMulti=1.);
  void useGenericPmlDir(const std::string,
                        double centerX=0., double centerY=0., double centerZ=0.,
                        double semiAxisX=-1., double semiAxisY=-1., double semiAxisZ=-1., double param=0.);
  
  const function *getPhysCoef(const std::string tag="") {return getFunctionForTag2(_physCoefMap, tag);}
  const function *getPmlCoef(const std::string tag="")  {return getFunctionForTag2(_pmlCoefMap, tag);}
  const function *getPmlDir(const std::string tag="")   {return getFunctionForTag2(_pmlDirMap, tag);}
  const function *getNumCoef(const std::string tag="")  {return getFunctionForTag2(_numCoefMap, tag);}
  const function *getPrevSolution(const std::string tag=""){return getFunctionForTag2( _prevSolutionMap, tag);} 

  
  /**Add a Pml treatment in the domain 'TAG' for wave problem. Parameters: TAG, incident fields (if any).*/
  void addPml(const std::string, const function *f=NULL);
  
  /**Add a Dirichlet boundary condition on 'u' for wave equation. Parameter: TAG, boundary value of u (uExtFunction). */
  dgBoundaryCondition *newBoundaryDirichletOnU(const std::string, const function *);
  /**Add a Dirichlet boundary condition on 'u' for wave equation. Parameter: TAG, flux (ndotuExtFunction). */
  dgBoundaryCondition *newBoundaryFluxU(const std::string, const function *);
  /**Add a homogeneous Dirichlet boundary condition on 'u' for wave equation. Parameter: TAG. */
  dgBoundaryCondition *newBoundaryHomogeneousDirichletOnU(const std::string);
  /**Add a wall boundary for wave equation. Parameter: TAG. */  // NB: Same as previous BC.
  dgBoundaryCondition *newBoundaryWall(const std::string);
  /**Add a Dirichlet boundary condition on 'p' for wave equation. Parameter: TAG, boundary value of p (pExtFunction). */
  dgBoundaryCondition *newBoundaryDirichletOnP(const std::string, const function *);
  /**Add a homogeneous Dirichlet boundary condition on 'p' for wave equation. Parameter: TAG. */
  dgBoundaryCondition *newBoundaryHomogeneousDirichletOnP(const std::string);
  /**Add an open boundary condition based on characteristics for wave equation. Parameter: TAG. */
  dgBoundaryCondition *newBoundaryOpenOutflow(const std::string);
  /**Add an open boundary condition based on characteristics with external flux for wave equation. Parameters: TAG, incident flieds. */
  dgBoundaryCondition *newBoundaryOpenOutflowWithInflow(const std::string, const function *);
  
  /**Add an interface domain/Pml for wave equation. Parameter: TAG, incident fields (if any). */
  void addInterfacePml(const std::string, const function *f=NULL);
  /**Add an interface domain/Pml for wave equation with incident fields. Parameter: TAG, incident fields (if any). */
  void addInterfacePmlWithInflow(const std::string, const function *f=NULL);
  /**Add an interface for wave equation with scattered/total fields formulation. Parameter: TAG, incident fields, direction to the domain in scattered formulation. */
  void addInterfaceScatteredTotalFields(const std::string, const function *, const function *);
  
  /**Add a shield interface for wave equation. Parameter: TAG. */

  void addInterfaceSheet(const std::string, double Ys);
  void addInterfaceShield(const std::string, const function *f, const function *g);

  /**Add a wall interface for wave equation. Parameter: TAG. */
  void addInterfaceHomogeneousDirichletOnU(const std::string);
  void addInterfaceWall(const std::string);
  /**Add a interface with a Dirichlet boundary condition on 'p' for wave equation. Parameter: TAG. */
  void addInterfaceDirichletOnP(const std::string, const function *f);
  /**Add a interface with a Homogeneous Dirichlet boundary condition on 'p'. Parameter: TAG. */
  void addInterfaceHomogeneousDirichletOnP(const std::string);
  /**Add an open interface for wave equation, i.e. open boundary based on characteristics. Parameter: TAG. */
  void addInterfaceOpenOutflow(const std::string);
  /**Add an open interface for wave equation, i.e. open boundary based on characteristics with external flux for wave equation. Parameters: TAG, incident fields. */
  void addInterfaceOpenOutflowWithInflow(const std::string, const function *f);
  
  // Get information
  class energyFunction;
  class energyErrorFunction;
  double getTotalEnergy(dgGroupCollection *groups, dgDofContainer *solutionDof, std::string tag="", int intOrder=-1);
  double getTotalEnergy(dgGroupCollection *groups, function *solutionFunc, std::string tag="", int intOrder=-1);
  double getTotalEnergyError(dgGroupCollection *groups, dgDofContainer *solutionDof, dgDofContainer *solutionRefDof, std::string tag="", int intOrder=-1);
  double getTotalEnergyError(dgGroupCollection *groups, dgDofContainer *solutionDof, function *solutionRefFunc, std::string tag="", int intOrder=-1);
  double getVolume(dgGroupCollection *groups, std::string tag="");
  
};


inline void dgConservationLawWaveEquation(int dim=0) {
  Msg::Error("dgConservationLawWaveEquation have been deleted. >>> Use dgConservationLawWave.");
}

inline void dgConservationLawWavePml(int dim=0, int pml=0) {
  Msg::Error("dgConservationLawWavePml have been deleted. >>> Use dgConservationLawWave.");
}


#endif
