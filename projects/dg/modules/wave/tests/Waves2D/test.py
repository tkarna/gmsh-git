from dgpy import *
import math
import sys
import os


implicit = True


# Load Mesh
model = GModel()
model.load('WaveEquation2D.geo')
model.mesh(2)
model.save('WaveEquation2D.msh')


# Build Groups
dim = 2
order = 1
groups = dgGroupCollection(model, dim, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()


# Load Initial conditions
def initialCondition(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    val.set(i,0,math.exp(-(x*x+y*y)/100))
    val.set(i,1,0.0)
    val.set(i,2,0.0)
initialConditionPython = functionPython(3,initialCondition,[xyz])
solution = dgDofContainer(groups,3)
solution.L2Projection(initialConditionPython)
#solution.exportMsh('output/WaveEquation2D-00000',0,0)


# Load Conservation law
law = dgConservationLawWave(dim)
cValue = 340.
rhoValue = 1.293
sigmaValue = 0.
coef = functionConstant([cValue,rhoValue,sigmaValue])
law.setPhysCoef('',coef)


# Load Boundary conditions
law.addBoundaryCondition('Boundaries',law.newBoundaryWall('Boundaries'))


# LOOP

if (implicit):
  
  linsys = linearSystemPETScBlockDouble()
  linsys.setParameter("petscOptions", "-pc_type lu")
  dof = dgDofManager.newDGBlock(groups, 3, linsys)
  dt = 0.01
  solver = dgDIRK(law, dof, 2)
  solver.getNewton().setVerb(10)
  for i in range(1,10):
    t = i*dt
    solver.iterate(solution, dt , t)
    #solution.exportMsh("output/WaveEquation2D-%05d" % (i), t, i)
    
else:
  
  linsys = linearSystemPETScBlockDouble()
  linsys.setParameter("petscOptions", "-pc_type lu")
  dof = dgDofManager.newDGBlock(groups, 3, linsys)
  
  solver = dgERK(law, dof, DG_ERK_44)
  dt = 0.0001
  for i in range(0,1500):
    t = i*dt
    norm = solver.iterate(solution, dt, t)
    #print('|ITER|',i,'IMPLICIT',implicit)
    #solution.exportMsh("output/WaveEquation2D-%05d" % (i), t, i)


normVal = solution.norm()

print ('|normVal| ', normVal)

if (normVal < 10): 
  print ("Exit with success :-)")
  Msg.Exit(0)
else:
  print ("Exit with failure :-(")
  Msg.Exit(1)

