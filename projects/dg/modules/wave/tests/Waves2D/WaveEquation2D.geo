ldom = 50;
lc = 5;

Point(1) = {-ldom, -ldom, 0, lc};
Point(2) = { ldom, -ldom, 0, lc};
Point(3) = { ldom,  ldom, 0, lc};
Point(4) = {-ldom,  ldom, 0, lc};

Line(1) = {4, 3};
Line(2) = {3, 2};
Line(3) = {2, 1};
Line(4) = {1, 4};
Line Loop(5) = {1, 2, 3, 4};

Plane Surface(6) = {5};
Physical Line("Boundaries") = {1, 2, 3, 4};
Physical Surface("Inside") = {6};
