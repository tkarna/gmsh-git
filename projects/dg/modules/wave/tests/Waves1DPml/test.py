from dgpy import *
import os
import time
import math


cValue = 1.293
rhoValue = 340.


#---- Load Mesh
model = GModel()
model.load('SegmentPml.geo')
model.mesh(2)
model.save('SegmentPml.msh')


#---- Build Groups
dim = 1
order = 1
groups = dgGroupCollection(model, dim, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()


#---- Build Initial Conditions
def initialCondition(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)+0.5
    val.set(i,0,math.exp(-40*x*x))
    val.set(i,1,math.exp(-40*x*x)/(cValue*rhoValue))
initialConditionPython = functionPython(2,initialCondition,[xyz])
sol = dgDofContainer(groups,2)
sol.L2Projection(initialConditionPython)


#---- Build Conservation Law with Coefficients
law = dgConservationLawWave(1,'BasicLayer')
coef = functionConstant([cValue,rhoValue])
law.setPhysCoef(coef)
nbFields = law.getNbFields()


#---- Buid PML
lpml = 0.1
def pmlCoefDef(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    sigmaPml = cValue/lpml * (x)/(lpml-x)
    val.set(i,0,sigmaPml)
pmlCoef = functionPython(1,pmlCoefDef,[xyz])
pmlDir = functionConstant([1.,0.,0.])
law.setPmlCoef(pmlCoef)
law.setPmlDir(pmlDir)
law.addPml('PML')


#---- Build Boundary Conditions
law.addBoundaryCondition('BOUNDARY_RIGHT',law.newBoundaryWall('BOUNDARY_RIGHT'))
law.addBoundaryCondition('BOUNDARY_LEFT', law.newBoundaryWall('BOUNDARY_LEFT'))


#---- LOOP
sys = linearSystemPETScBlockDouble()
sys.setParameter("petscOptions", "-pc_type ilu")
dof = dgDofManager.newDGBlock(groups, nbFields, sys)
dt = 0.0001
solver = dgERK(law, dof, DG_ERK_44)
#nbExport = 1
for i in range(0,5001):
  t = i*dt
  solver.iterate(sol, dt, t)
  #if (i % 100 == 0):
  #  erg = (law.getTotalEnergy(groups, sol, 'DOMAIN')-1.10994740164e-05)/(1.10994740164e-05)
  #  print '|ITER|', i, '|DT|', dt, '|t|', t, '|Erg|', erg
  #  sol.exportMsh("output/Segment_Pml_%05i" % (nbExport), t, nbExport)
  #  nbExport = nbExport + 1


ergRef = 1.10994740164e-05
ergNum = law.getTotalEnergy(groups, sol, 'DOMAIN')
error = (ergNum-ergRef)/ergRef

print ('ergRef', ergRef, 'ergNum', ergNum, 'error', error)

if (error < 1e-10): 
  print ("Exit with success :-)")
  Msg.Exit(0)
else:
  print ("Exit with failure :-(")
  Msg.Exit(1)
