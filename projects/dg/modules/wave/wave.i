%module wave

%import(module="dgpy.dgCommon") "dgCommon.i";

%{
  #undef HAVE_DLOPEN
  #include "dgConservationLawWave.h"
  #include "dgConservationLawWaveExplicit.h"
%}

%import(module="dgpy.conservationLaw") "dgConservationLaw.h";
%import(module="dgpy.conservationLaw") "dgConservationLawFunction.h";

%include "dgConservationLawWave.h"
%include "dgConservationLawWaveExplicit.h"
