#include "dgConservationLawFunction.h"
#include "dgConservationLawAdvectionSUPG.h"
#include "function.h"
#include "functionGeneric.h"
#include "fullMatrix.h"
#include "dgGroupOfElements.h"
#include "SPoint3.h"
#include "Numeric.h"
#include "dgMeshJacobian.h"



/*==============================================================================
 * CG-SUPG-terms
 *============================================================================*/


class dgConservationLawAdvectionDiffusionSUPG::maxConvectiveSpeed : public function {
  fullMatrix<double> v;
public:
  maxConvectiveSpeed(const function *vFunction) : function(1) {
    setArgument(v, vFunction);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) { 
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++)
      val(i,0) = sqrt(v(i,0)*v(i,0)+v(i,1)*v(i,1)+v(i,2)*v(i,2));
  }
};


class dgConservationLawAdvectionDiffusionSUPG::distanceSource : public function {
  fullMatrix<double> sol, solgrad, _dt, prevSol, prevSolgrad;
  double E;
public:
  distanceSource(const double _E, const function *_prevSol, const function *_prevSolGrad) : function(4) {
    setArgument(solgrad, function::getSolutionGradient());
    setArgument(sol, function::getSolution());
    setArgument(_dt, function::getDT());
    setArgument(prevSolgrad, _prevSolGrad);
    setArgument(prevSol, _prevSol);
    E = _E;
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) { 
    const dgGroupOfElements &eGroup = *m->getGroupOfElements();
    for (int i=0; i<solgrad.size1(); i++) {
      int iElement = m->elementId(i / m->nPointByElement());
      double h = sqrt(4*m->getJacobians().elementVolume(eGroup.elementVectorId())(iElement)/3.14);
      double lambda = 0.01*h/_dt(i,0);
      //double lambda = 1.0; 
      double s = (sol(i,0) >= 0.0) ? 1.0 : -1.0;
      if (sol(i,0) == 0.0) s = 0.0;
      double sPrev = (prevSol(i,0) >= 0.0) ? 1.0 : -1.0;
      if (prevSol(i,0) == 0.0) sPrev = 0.0;
      double normPhiPrev = 1.e-2+sqrt(prevSolgrad(i,0)*prevSolgrad(i,0)+prevSolgrad(i,1)*prevSolgrad(i,1)+prevSolgrad(i,2)*prevSolgrad(i,2));
      double velDist = (normPhiPrev != 0.0) ? lambda*sPrev/normPhiPrev : 0.0;
      double square = std::min((0.5*M_PI*sol(i,0)/E)*(0.5*M_PI*sol(i,0)/E), 1.0);
      double source = lambda*s*(sqrt(1.0-square));
      val(i,0) = velDist*prevSolgrad(i,0);
      val(i,1) = velDist*prevSolgrad(i,1);
      val(i,2) = velDist*prevSolgrad(i,2);
      val(i,3) = source;
    }
  }
};


class dgConservationLawAdvectionDiffusionSUPG::psiTerm : public function {
  fullMatrix<double> v, vGrad, nu, sol, solgrad, distS, source;
  bool haveV, haveDist;
public:
  psiTerm(const function *vFunction, const function *vFunctionGrad,
	  const function *nuFunction, const function *distSource, 
	  const function *sourceF): function (1){
    haveV = vFunction;
    haveDist = distSource;
    setArgument (source, sourceF);
    if (haveV) {
      setArgument (sol, function::getSolution());
      setArgument (solgrad, function::getSolutionGradient());
      setArgument (v, vFunction);
      setArgument (vGrad, vFunctionGrad);
    }
    if (haveDist)
      setArgument (distS, distSource);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP =val.size1();
      for (size_t i = 0; i < nQP; i++) {
	val (i, 0) = source (i,0) ;
	if (haveV) {
	  double _u = v(i,0);
	  double _v = v(i,1);
	  double _w = v(i,2);
	  if (haveDist) {
	    _u += distS(i,0);
	    _v += distS(i,1);
	    _w += distS(i,2);
	  }
	  // for non conservative
	  double UGrada = _u * solgrad(i, 0) + _v * solgrad(i, 1) + _w * solgrad(i, 2);
	  val (i, 0) -= UGrada ;
	  //for conservative
	  //double divU = vGrad(i,0)+vGrad(i,4)+vGrad(i,8);
	  //val (i,0) -= sol(i,0)*divU;
	}

	if (haveDist)   val(i, 0) += distS(i, 3);
      }
    }
};


class dgConservationLawAdvectionDiffusionSUPG::gradPsiTerm : public function {
  fullMatrix<double> sol, solGrad, v, nu;
  fullMatrix<double> solPrev, _dt, speed, solgrad, distS;
  bool haveV, haveNu, haveDist;
public:
  gradPsiTerm(const function *vFunction, const function *nuFunction,  const function *previousSol,
            const function *maxSpeed, const function *distSource ) : function(3) {
    haveV = vFunction;
    haveNu = nuFunction;
    haveDist = distSource;
    setArgument (solgrad, function::getSolutionGradient());
    if (haveV) {
      setArgument (sol, function::getSolution());
      setArgument (solPrev, previousSol);
      setArgument (v, vFunction);
      setArgument (_dt, function::getDT());
      setArgument (speed, maxSpeed);
    }
    if (haveNu)
      setArgument (nu, nuFunction);
    if (haveDist)
      setArgument (distS, distSource);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.setAll (0);
    //for conservative
    // if (haveV)
    //   for (int i=0; i<val.size1(); i++) {
    //     val(i,0) = sol(i,0)*v(i,0);
    //     val(i,1) = sol(i,0)*v(i,1);
    //     val(i,2) = sol(i,0)*v(i,2);
    //   }
    if (haveNu){
       for (int i=0; i<val.size1(); i++) {
        val(i, 0) -= solgrad(i, 0) * nu(i, 0);
        val(i, 1) -= solgrad(i, 1) * nu(i, 0);
        val(i, 2) -= solgrad(i, 2) * nu(i, 0);
      }
    }
    //SUPG TERMS
    if (haveV ) {
      const dgGroupOfElements &eGroup = *m->getGroupOfElements();
      for (int i = 0; i < val.size1(); i++) {
        int iElement = m->elementId(i / m->nPointByElement());
        double _u = v(i, 0);
        double _v = v(i, 1);
        double _w = v(i, 2);
        double source = 0.0;
        double ULoc = speed(i,0);
        if (haveDist) {
          _u += distS(i,0);
          _v += distS(i,1);
          _w += distS(i,2);
          source = distS(i,3);
          ULoc += sqrt(distS(i,0)*distS(i,0)+distS(i,1)*distS(i,1)+distS(i,2)*distS(i,2));
        }
        double dt = _dt(i,0);
        double dadt = (dt > 0.) ? (sol(i,0) - solPrev(i,0)) / dt : 0.;
        double UGrada = _u * solgrad(i, 0) + _v * solgrad(i, 1) + _w * solgrad(i, 2);
        double Res  = (dadt + UGrada - source);
        double h = sqrt(4.0 * m->getJacobians().elementVolume(eGroup.elementVectorId())(iElement) / 3.14);
        double hV =  eGroup.getStreamwiseLength(iElement); //WARNING STREAMWISE LENGTH WITH ADDITIONAL VELOCITY !!!
        if (hV < 0 || ULoc == 0.0 ) hV = h;
        double alpha = 0.1;
        double tau_DT = (dt > 0.0) ? SQU(2./dt) : 0.0;
        double tau_V = (ULoc != 0.0) ? alpha*(1./sqrt( tau_DT + SQU(2.*ULoc/hV) ) ): 0.0;
        if (haveNu)
          tau_V = alpha*(1./sqrt( SQU(4.*nu(i,0)/(hV*hV)) + SQU(2.*ULoc/hV) ) );
        val(i,0) -= tau_V * Res * _u;
        val(i,1) -= tau_V * Res * _v;
        val(i,2) -= tau_V * Res * _w;
      }
    }
  }
};


//only called for boundary fluxes
class dgConservationLawAdvectionDiffusionSUPG::boundaryTerm : public function {
  fullMatrix<double> normals, sol, v, nu, solgrad;
  bool haveV, haveNu;
public:
  boundaryTerm(const function *vFunction, const function *nuFunction) : function(1) {
    haveV  = vFunction;
    haveNu = nuFunction;
    setArgument(normals, function::getNormals());  
    if (vFunction) {
      setArgument(sol, function::getSolution());
      setArgument(v, vFunction);
    }
    if (nuFunction) {
      setArgument(nu, nuFunction);
      setArgument(solgrad, function::getSolutionGradient());
    }
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.0);
    //conservative
    // if (haveV)
    //   for (int i=0; i<val.size1(); i++) {
    //     double un = v(i,0)*normals(i,0) + v(i,1)*normals(i,1) + v(i,2)*normals(i,2);
    //       val(i,0) = -sol(i,0) * un;
    //   }
    if (haveNu){
      for (int i=0; i<val.size1(); i++) {
	double gradUn = solgrad(i,0)*normals(i,0) + solgrad(i,1)*normals(i,1) + solgrad(i,2)*normals(i,2);
        val(i,0) += nu(i,0)*gradUn;
      }
    }
  }
};


/*==============================================================================
 * Conservation law : constructor - setup - destructor
 *============================================================================*/

dgConservationLawAdvectionDiffusionSUPG::dgConservationLawAdvectionDiffusionSUPG(const function *vFunction, const function *vFunctionGrad, const function *nuFunction) : dgConservationLawFunction(1) {
  _prevSol       = new functionConstant(0.);
  _prevSolGrad   = new functionConstant(std::vector<double>(3,0));
  _vFunction     = vFunction;
  _vFunctionGrad = vFunctionGrad;
  _nuFunction    = nuFunction;
  _sourceTerm    = new functionConstant(0.);
  _E = 0.0;
}

// std::vector<dgTerm*> dgConservationLawAdvectionDiffusionSUPG::newTerms(const dgGroupOfElements &group, const dgIntegrationMatrices &intMatrices, bool jacobianNeeded, dataCacheMap &cacheMap) const{
//   std::vector<dgTerm*> terms;
//   if (_volumeTerm0[""])            delete _volumeTerm0[""];
//   if (_volumeTerm1[""])            delete _volumeTerm1[""];
//   _volumeTerm0[""] = new psiTerm(_vFunction, _vFunctionGrad, _nuFunction, _distanceSource);
//   _volumeTerm1[""] = new gradPsiTerm (_vFunction, _nuFunction, _prevSol, _maximumConvectiveSpeed[""], _distanceSource) ;

//   terms.push_back(dgIntegralTerm::newPsiTerm(_volumeTerm0[""]), group, intMatrices, jacobianNeeded, _nbf, cacheMap));
//   terms.push_back(dgIntegralTerm::newGradPsiTerm( _volumeTerm1[""]  ), group, intMatrices, jacobianNeeded, _nbf, cacheMap));

// }

void dgConservationLawAdvectionDiffusionSUPG::setup() {
  _maximumConvectiveSpeed[""] = _vFunction  ? new maxConvectiveSpeed (_vFunction)            : NULL;
  _distanceSource             = (_E > 0.0 ) ? new distanceSource(_E, _prevSol, _prevSolGrad) : NULL;
  _volumeTerm0[""]            = new psiTerm(_vFunction, _vFunctionGrad, _nuFunction, _distanceSource, _sourceTerm);
  _volumeTerm1[""]            = new gradPsiTerm (_vFunction, _nuFunction, _prevSol, _maximumConvectiveSpeed[""], _distanceSource) ;
  _interfaceTerm0[""]         = new boundaryTerm (_vFunction, _nuFunction);

}

dgConservationLawAdvectionDiffusionSUPG::~dgConservationLawAdvectionDiffusionSUPG() {
  if (_maximumConvectiveSpeed[""]) delete _maximumConvectiveSpeed[""];
  if (_volumeTerm0[""])            delete _volumeTerm0[""];
  if (_volumeTerm1[""])            delete _volumeTerm1[""];
  if (_interfaceTerm0[""])         delete _interfaceTerm0[""];
  if (_distanceSource)             delete _distanceSource;
}

dgConservationLawAdvectionDiffusionSUPG *dgConservationLawAdvectionDiffusionSUPG::diffusionLaw(const function *nu) {
  return new dgConservationLawAdvectionDiffusionSUPG(NULL, NULL, nu);
}
dgConservationLawAdvectionDiffusionSUPG *dgConservationLawAdvectionDiffusionSUPG::advectionLaw(const function *v, const function *vGrad) {
  return new dgConservationLawAdvectionDiffusionSUPG(v, vGrad, NULL);
}
