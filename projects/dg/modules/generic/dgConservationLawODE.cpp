#include "dgConservationLawFunction.h"
#include "dgConservationLawODE.h"
#include "function.h"
   

dgConservationLawODE::dgConservationLawODE(const function &termFull, const function &termIm) : dgConservationLawFunction(termFull.getNbCol()), _nbFields(termFull.getNbCol()) {
  _termFull = &termFull;
  _termIm = &termIm;
}

class dgConservationLawODE::interfaceFlux : public function {
  public:
  interfaceFlux(int nbFields):function(nbFields*2) {}
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    val.setAll(0);
  }
};


void dgConservationLawODE::setup() {
  _volumeTerm0[""] = _termFull;
  _interfaceTerm0[""] = new interfaceFlux(_nbFields);
}

void dgConservationLawODE::setImexMode(imexMode mode) {
  if (mode == IMEX_IMPLICIT){
    _volumeTerm0[""] = _termIm;
  }else if (mode == IMEX_ALL){
    _volumeTerm0[""] = _termFull;
  }else Msg::Error("Unsupported mode %i\n",mode);
}

dgConservationLawODE::~dgConservationLawODE() {
  delete _interfaceTerm0[""];
}
