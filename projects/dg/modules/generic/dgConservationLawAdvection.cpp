#include "dgConservationLawFunction.h"
#include "dgConservationLawAdvection.h"
#include "function.h"
#include "fullMatrix.h"
#include "dgGroupOfElements.h"
#include "SPoint3.h"


/*==============================================================================
 * DG-terms
 *============================================================================*/

class dgConservationLawAdvectionDiffusion::volumeTerm1 : public function {
  fullMatrix<double> sol, v, diffusiveFlux;
  bool haveV, haveNu;
 public:
  volumeTerm1(const function *vF, const function *diffusiveFluxF) : function(3) {
    haveV = vF;
    haveNu = diffusiveFluxF;
    if (haveV) {
      setArgument (sol, function::getSolution());
      setArgument (v, vF);
    }
    if (haveNu) {
      setArgument (diffusiveFlux, diffusiveFluxF);
    }
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) { 
    val.scale (0);
    if (haveV) {
      for(int i=0; i<val.size1(); i++) {
        val(i,0) = sol(i,0)*v(i,0);
        val(i,1) = sol(i,0)*v(i,1);
        val(i,2) = sol(i,0)*v(i,2);
      }
    }
    if (haveNu) {
      for(int i=0; i<val.size1(); i++) {
        val(i,0) += diffusiveFlux(i,0);
        val(i,1) += diffusiveFlux(i,1);
        val(i,2) += diffusiveFlux(i,2);
      }
    }
  }
};

class dgConservationLawAdvectionDiffusion::maxConvectiveSpeed : public function {
  fullMatrix<double> v;
 public:
  maxConvectiveSpeed(const function *vFunction) : function(1) {
    setArgument(v, vFunction);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) { 
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++)
      val(i,0) = hypot(v(i,0),v(i,1));
  }
};

class dgConservationLawAdvectionDiffusion::source : public function {
  fullMatrix<double> _source, _diffusiveFlux, _xyz;
  double _R, _R2;
  bool _haveSource;
public:
  source(const function *sourceF,  const function *diffusiveFluxF, double R) :function (1){
    _R = R;
    _R2 = _R*_R;
    setArgument(_diffusiveFlux,diffusiveFluxF);
    setArgument(_xyz,function::getCoordinates());
    if(sourceF)
    {
      setArgument(_source,sourceF);
      _haveSource = true;
    }
    else
      _haveSource = false;
    
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP =val.size1();
    for(size_t i=0; i < nQP; i++) {
      val(i,0) = 0;
      if(_haveSource){
        val(i,0) += _source(i,0);
      }
      if(_R > 0) {
        const double J = 4.0*_R2/(4.0*_R2+_xyz(i,0)*_xyz(i,0)+_xyz(i,1)*_xyz(i,1));
        val (i,0) += J/_R2*(_xyz(i,0)*_diffusiveFlux(i,0) + _xyz(i,1)*_diffusiveFlux(i,1));
      }
    }
  }
};

class dgConservationLawAdvectionDiffusion::discInterfaceTerm : public function {
  fullMatrix<double> normals, solLeft, solRight, vL, vR, ipTerm;
  bool haveV, haveNu;
 public:
  discInterfaceTerm(const function *vFunction, const function *ipTermF) : function(2) {
    haveV = vFunction;
    haveNu = ipTermF;
    if (vFunction) {
      setArgument (normals, function::getNormals());
      setArgument (solLeft, function::getSolution(), 0);
      setArgument (solRight, function::getSolution(), 1);
      setArgument (vL, vFunction, 0);
      setArgument (vR, vFunction, 1);
    }
    if (ipTermF)
      setArgument (ipTerm, ipTermF);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) { 
    if (haveV) {
      for (int i = 0; i < val.size1(); i++) {
        double unL = vL(i, 0) * normals(i, 0) + vL(i, 1) * normals(i, 1) + vL(i, 2) * normals(i, 2);
        double unR = vR(i, 0) * normals(i, 0) + vR(i, 1) * normals(i, 1) + vR(i, 2) * normals(i, 2);
        
        // Centred flux (in case of opposite fluxes) or full non-null flux
        if(unL * unR <= 0.)
          val(i, 0) = (solLeft(i, 0) * unL + solRight(i, 0) * unR) / ((unL * unR == 0.) ? 1. : 2.);
        // Upwind flux:
        else if(unL < 0.)
          val(i, 0) = solLeft(i, 0)  * unL;
        else
          val(i, 0) = solRight(i, 0) * unR;
        val(i, 1) = - val(i, 0);
      }
    } else {
      val.scale(0);
    }
    if (haveNu) {
      for (int i = 0; i < val.size1(); i++) {
        val(i, 0) += ipTerm(i, 0);
        val(i, 1) += ipTerm(i, 1);
      }
    }
  }
};

class dgConservationLawAdvectionDiffusion::specialUpWInterfaceTerm : public function {
  fullMatrix<double> normals, solLeft, solRight, vL, vR, ipTerm;
  bool haveV, haveNu;
 public:
  specialUpWInterfaceTerm(const function *vFunction, const function *ipTermF) : function(2) {
    haveV = vFunction;
    haveNu = ipTermF;
    if (vFunction) {
      setArgument (normals, function::getNormals());
      setArgument (solLeft, function::getSolution(), 0);
      setArgument (solRight, function::getSolution(), 1);
      setArgument (vL, vFunction, 0);
      setArgument (vR, vFunction, 1);
    }
    if (ipTermF)
      setArgument (ipTerm, ipTermF);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) { 
    if (haveV) {
      for (int i = 0; i < val.size1(); i++) {
        if(solLeft(i, 0) > solRight(i, 0)) {
          double unL = vL(i, 0) * normals(i, 0) + vL(i, 1) * normals(i, 0) + vL(i, 2) * normals(i, 2);
          if(unL < 0.)
            val(i, 0) = solLeft(i, 0)  * unL;
          else
            val(i, 0) = 0.;
        }
        else {
          double unR = vR(i, 0) * normals(i, 0) + vR(i, 1) * normals(i, 0) + vR(i, 2) * normals(i, 2);
          if(unR > 0.)
            val(i, 0) = solRight(i, 0) * unR;
          else
            val(i ,0) = 0.;
        }
        val(i, 1) = - val(i, 0);
      }
    } else {
      val.scale(0);
    }
    if (haveNu) {
      for (int i = 0; i < val.size1(); i++) {
        val(i, 0) += ipTerm(i, 0);
        val(i, 1) += ipTerm(i, 1);
      }
    }
  }
};


class dgConservationLawAdvectionDiffusion::interfaceTerm : public function {
  fullMatrix<double> normals, solLeft, solRight, v, ipTerm;
  bool haveV, haveNu;
 public:
  interfaceTerm(const function *vFunction, const function *ipTermF) : function(2) {
    haveV = vFunction;
    haveNu = ipTermF;
    if (vFunction) {
      setArgument (normals, function::getNormals());
      setArgument (solLeft, function::getSolution(), 0);
      setArgument (solRight, function::getSolution(), 1);
      setArgument (v, vFunction);
    }
    if (ipTermF)
      setArgument (ipTerm, ipTermF);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) { 
    if (haveV) {
      for (int i=0; i<val.size1(); i++) {
        double un = v(i, 0) * normals(i, 0) + v(i, 1) * normals(i, 1) + v(i, 2) * normals(i, 2);
        // Upwind flux:
        val(i, 0) = - (solLeft(i, 0) + solRight(i, 0)) / 2 * un - (solLeft(i, 0) - solRight(i, 0)) / 2 * fabs(un);
        val(i, 1) = - val(i, 0);
        // Centred flux:
        //val(i,0) = - (solLeft(i,0)+solRight(i,0))/2 * un;
        //val(i,1) = - val(i,0);
      }
    } else {
      val.scale(0);
    }
    if (haveNu) {
      for (int i=0; i<val.size1(); i++) {
        val(i,0) += ipTerm(i,0);
        val(i,1) += ipTerm(i,1);
      }
    }
  }
};

class dgConservationLawAdvectionDiffusion::diffusiveFlux : public function {
  fullMatrix<double> solgrad, nu, _xyz;
  double _R, _R2;
 public:
  diffusiveFlux(const function * nuFunction, double R) : function(3) {
    setArgument(solgrad,function::getSolutionGradient());
    setArgument(nu,nuFunction);
    _R = R;
    _R2 = _R*_R;
    if(_R > 0)
      setArgument(_xyz, function::getCoordinates());
    
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) { 
    double oJ2 = 1.0;
    for (int i=0; i<solgrad.size1(); i++) {
      if(_R > 0){
        double x = _xyz(i,0);
        double y = _xyz(i,1);
        const double J = 4.0*_R2 / (4.0*_R2 + x*x + y*y);
        oJ2 = 1. / (J*J);
      }

      if (nu.size2() == 1){
	val(i,0) = -solgrad(i,0) * nu(i,0) * oJ2;
	val(i,1) = -solgrad(i,1) * nu(i,0) * oJ2;
	val(i,2) = -solgrad(i,2) * nu(i,0) * oJ2;
      }else{
	val(i,0) = - solgrad(i,0) * nu(i,0) - solgrad(i,1) * nu(i,1);
	val(i,1) = -solgrad(i,1) * nu(i,2) -  solgrad(i,0) * nu(i,1);
	val(i,2) = 0;//-solgrad(i,2) * nu(i,0);
      }
    }
  }
};

class dgConservationLawAdvectionDiffusion::clip: public function{
  fullMatrix<double> sol;
public:
  clip():function(1){
    setArgument(sol,function::getSolution());
  };
  void call (dataCacheMap *m,fullMatrix<double> &val){
    const int nQP = val.size1();
    for (int k =0; k<nQP;k++){
      val(k,0)=std::min(std::max(sol(k,0),0.0),1.0);
    }
  }
};


/*==============================================================================
 * Boundary conditions
 *============================================================================*/

// Bounday condition for superconductor model: depending on gradient(givenGrad) of previous solution

class dgBoundaryConditionForSupraLaw : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> explicitFuncOfsol, normals,cValue;
    double _a,_b,_c; 
    public:
    term(const function *functionOfsolution, const function *constantFunc, const double a, const double b, const double c): function(1) {
      setArgument (explicitFuncOfsol, functionOfsolution);
      setArgument (normals, function::getNormals());
      setArgument (cValue,constantFunc) ;
      _a=a;
      _b=b;
      _c=c;
    }
    void call (dataCacheMap *m, fullMatrix<double> &val) { 
      size_t nQP = val.size1();
      for(size_t i=0; i< nQP; i++) {
        
	double nx = normals(i,0);
	double ny = normals(i,1);
	double nz = normals(i,2);

	val(i,0) = _a*(explicitFuncOfsol(i,0)+cValue(i,0))*nx + _b*(explicitFuncOfsol(i,1)+cValue(i,1))*ny+_c*(explicitFuncOfsol(i,2)+cValue(i,2))*nz;

      }
    }
  };
  term _term;
  public:
  dgBoundaryConditionForSupraLaw(dgConservationLawFunction *claw,const function *functionOfsolution, const function *constantFunc, const double a, const double b, const double c):_term(functionOfsolution,constantFunc,a,b,c){
    _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawAdvectionDiffusion::newBoundaryForSupraLaw(const function *functionOfsolution, const function *constantFunc, const double a, const double b, const double c){
  return new dgBoundaryConditionForSupraLaw(this,functionOfsolution,constantFunc,a,b,c);
}
    
/*==============================================================================
 * Conservation law : constructor - setup - destructor
 *============================================================================*/

dgConservationLawAdvectionDiffusion::dgConservationLawAdvectionDiffusion(const function *vFunction, const function *nuFunction) : dgConservationLawFunction(1) {
  _vFunction  = vFunction;
  _nuFunction = nuFunction;
  _sourceTerm = _diffusiveFlux = _ipTerm = NULL;
  _vContinuous = true;
  _specUpWind = false;
  _R = -1;
}

void dgConservationLawAdvectionDiffusion::setup() {
  _diffusivity[""]            = _nuFunction;
  _maximumConvectiveSpeed[""] = _vFunction  ? new maxConvectiveSpeed (_vFunction)          : NULL;
  _diffusiveFlux              = _nuFunction ? new diffusiveFlux (_nuFunction, _R)          : NULL;
  _volumeTerm0[""]            = _R > 0      ? new source(_sourceTerm, _diffusiveFlux, _R)      : _sourceTerm;
  _ipTerm                     = _nuFunction ? dgNewIpTerm (1, _diffusiveFlux, _nuFunction) : NULL;
  _volumeTerm1[""]            = _vFunction  ? new volumeTerm1 (_vFunction, _diffusiveFlux) : _diffusiveFlux;
  if(_specUpWind)
    _interfaceTerm0[""]       = _vFunction  ? new specialUpWInterfaceTerm (_vFunction, _ipTerm) : _ipTerm;
  else if(_vContinuous)
    _interfaceTerm0[""]       = _vFunction  ? new interfaceTerm (_vFunction, _ipTerm)           : _ipTerm;
  else
    _interfaceTerm0[""]       = _vFunction  ? new discInterfaceTerm (_vFunction, _ipTerm)       : _ipTerm;
  _interfaceTerm1N[""]         = NULL;//_nuFunction  ? dgNewSymmetricIpTerm(1, _nuFunction) : NULL;
  //_massFactor[""] = new functionConstant(1.0);
  //_clipToPhysics[""]=new clip();
  
}

dgConservationLawAdvectionDiffusion::~dgConservationLawAdvectionDiffusion() {
  if (_maximumConvectiveSpeed[""]) delete _maximumConvectiveSpeed[""];
  if (_diffusiveFlux)              delete _diffusiveFlux;
  if (_ipTerm)                     delete _ipTerm;
  if (_vFunction) {
    if (_volumeTerm1[""])    delete _volumeTerm1[""];
    if (_interfaceTerm0[""]) delete _interfaceTerm0[""];
  }
  if (_clipToPhysics[""]) delete _clipToPhysics[""];
}

dgConservationLawAdvectionDiffusion *dgConservationLawAdvectionDiffusion::diffusionLaw(const function *nu) {
  return new dgConservationLawAdvectionDiffusion(NULL, nu);
}

dgConservationLawAdvectionDiffusion *dgConservationLawAdvectionDiffusion::advectionLaw(const function *v) {
  return new dgConservationLawAdvectionDiffusion(v,NULL);
}

dgConservationLawAdvectionDiffusion *dgConservationLawAdvectionDiffusion::discontinuousAdvectionLaw(const function *v) {
  dgConservationLawAdvectionDiffusion *law = new dgConservationLawAdvectionDiffusion(v,NULL);
  law->_vContinuous = false;
  return law;
}

dgConservationLawAdvectionDiffusion *dgConservationLawAdvectionDiffusion::specialUpwindAdvectionLaw(const function *v) {
  dgConservationLawAdvectionDiffusion *law = new dgConservationLawAdvectionDiffusion(v,NULL);
  law->_vContinuous = false;
  law->_specUpWind  = true;
  return law;
}


