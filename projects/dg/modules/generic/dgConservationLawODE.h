#ifndef _DG_CONSERVATION_LAW_ODE_H
#define _DG_CONSERVATION_LAW_ODE_H

#include "dgConservationLawFunction.h"

/**
 * General ODE (no spatial term, just source)
 * Useful to test time integration techniques
 */
class dgConservationLawODE : public dgConservationLawFunction {
  const function *_termFull, *_termIm;
  const int _nbFields;
  class interfaceFlux;
 public:
  void setImexMode(imexMode mode);
  dgConservationLawODE(const function &termFull, const function &termIm);
  void setup();
  ~dgConservationLawODE();
  };
#endif
