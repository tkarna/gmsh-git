%module generic

%{
  #undef HAVE_DLOPEN
  #include "dgConservationLawAdvection.h"
  #include "dgConservationLawAdvectionSUPG.h"
  #include "dgConservationLawDiffusionSIP.h"
  #include "dgConservationLawODE.h"
%}

%import(module="dgpy.conservationLaw") "dgConservationLaw.h";
%import(module="dgpy.conservationLaw") "dgConservationLawFunction.h";

%include "dgConservationLawAdvection.h"
%include "dgConservationLawAdvectionSUPG.h"
%include "dgConservationLawDiffusionSIP.h"
%include "dgConservationLawODE.h"
