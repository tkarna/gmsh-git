#ifndef _DG_CONSERVATION_LAW_ADVECTION_DIFFUSION_H
#define _DG_CONSERVATION_LAW_ADVECTION_DIFFUSION_H

#include "dgConservationLawFunction.h"

/**
 * Advection-diffusion of a scalar field.
 * The advection and diffusion coefficents are provided by functions
 * dc/dt = - v div(c) + nu lapl(c)
 */
class dgConservationLawAdvectionDiffusion : public dgConservationLawFunction {
  const function *_vFunction, *_nuFunction, *_sourceTerm, *_diffusiveFlux, *_ipTerm;
  bool _vContinuous, _specUpWind;
  double _R;
  class volumeTerm1;
  class source;
  class interfaceTerm;
  class discInterfaceTerm;
  class specialUpWInterfaceTerm;
  class maxConvectiveSpeed;
  class diffusiveFlux;
  class clip;
 public:
  
  /**A new advection-diffusion law.
   * \param v The advection speed vector function  
   * \param nu The scalar diffusivity function */
  dgConservationLawAdvectionDiffusion(const function *v, const function *nu);
  void setup();
  ~dgConservationLawAdvectionDiffusion();
  
  /**Set the function to evaluate the source term */
  inline void setSource(const function *sourceTerm) {_sourceTerm=sourceTerm;}
  /**Spherical in stereographics */
  inline void setIsSpherical(double R) {_R=R;}
 /**return the velocity vector */
  inline const function* getVelocity(){ checkSetup(); return _vFunction; };
  
  /**Create a new instance with diffusion only */
  static dgConservationLawAdvectionDiffusion *diffusionLaw(const function *nuFunction);
  /**Create a new instance with advection only */
  static dgConservationLawAdvectionDiffusion *advectionLaw(const function *vFunction);
  /**Create a new instance with advection only, with discontinuous speed */
  static dgConservationLawAdvectionDiffusion *discontinuousAdvectionLaw(const function *vFunction);
  /**Create a new instance with advection only, with discontinuous speed and upwind based on the max sol value */
  static dgConservationLawAdvectionDiffusion *specialUpwindAdvectionLaw(const function *vFunction);
  
  /* Modified Neumann boundary condition for (Ex,Ey,Hz) superconductors model*/
dgBoundaryCondition *newBoundaryForSupraLaw(const function *functionOfsolution, const function *constantFunc, const double a, const double b, const double c);
};
#endif
