#ifndef _DG_CONSERVATION_LAW_ADVECTION_DIFFUSION_SUPG_H
#define _DG_CONSERVATION_LAW_ADVECTION_DIFFUSION_SUPG_H

#include "dgConservationLawFunction.h"

/**
 * Advection-diffusion of a scalar field.
 * The advection and diffusion coefficents are provided by functions
 * dc/dt = - v div(c) + nu lapl(c)
 */
class dgConservationLawAdvectionDiffusionSUPG : public dgConservationLawFunction {
  const function *_vFunction,*_vFunctionGrad, *_nuFunction, *_distanceSource, *_sourceTerm;
  const function *_prevSol, *_prevSolGrad;
  double _E;
  class gradPsiTerm;
  class psiTerm;
  class boundaryTerm;
  class maxConvectiveSpeed;
  class distanceSource;
 public:
  /**A new advection-diffusion law.
   * \param v The advection speed vector function  
   * \param nu The scalar diffusivity function */
  dgConservationLawAdvectionDiffusionSUPG(const function *v, const function *vGrad, const function *nu);
  void setup();
  ~dgConservationLawAdvectionDiffusionSUPG();
  
  /**Set the function to evaluate the source term */
  inline void setSource(const function *sourceTerm) { checkNonSetup(); _sourceTerm=sourceTerm;}
  inline void setDistance(const double E)                              { checkNonSetup(); _E = E;} ;
  inline void setPreviousSolution(const function *prevSol)             { checkNonSetup(); _prevSol = prevSol; }
  inline void setPreviousSolutionGradient(const function *prevSolGrad) { checkNonSetup(); _prevSolGrad = prevSolGrad; }
  inline const function* getPreviousSolution() { checkSetup(); return _prevSol; }
  inline const function* getVelocity()         { checkSetup(); return _vFunction; };
  
  /* inline const function * getSource(){  */
  /*   checkSetup();  */
  /*   return functionExtractCompNew(_distanceSource,0); } */
  
  /**Create a new instance with diffusion only */
  static dgConservationLawAdvectionDiffusionSUPG *diffusionLaw(const function *nuFunction);
  /**Create a new instance with advection only */
  static dgConservationLawAdvectionDiffusionSUPG *advectionLaw(const function *vFunction, const function *vFunctionGrad);

};
#endif
