from dgpy import *
from math import *
import time, os, sys
import gmshPartition

#### Begin and end time ###
Ti=0
Tf=0.55139
nbSteps = 10000
###########################

order=0 #Spatial order!!
dimension=2

#Partition, extrude and rotat the mesh
model = GModel()
model.load("square.geo")
model.mesh(2)
model.save("square.msh")

model = GModel()
model.load('square.msh')


groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag();
Msg.Exit(0)

e = 1e-3
def vanDerPolFull(term, Y) :
  for i in range (0,Y.size1()) :
    y1 = Y(i, 0)
    y2 = Y(i, 1)
    term.set(i, 0, y2)
    term.set(i, 1, ((1 - y1*y1) * y2 - y1) / e)

def vanDerPolIm(term, Y) :
  for i in range (0,Y.size1()) :
    y1 = Y(i, 0)
    y2 = Y(i, 1)
    term.set(i, 0, 0)
    term.set(i, 1, ((1 - y1*y1) * y2 - y1) / e)

nbFields = 2
solution = dgDofContainer(groups, nbFields)

#Integrate over a domain of size 1 => gives the value
solODE = dgFunctionIntegrator(groups, solution.getFunction())
solMat = fullMatrixDouble(2,1)

termFull=functionPython(nbFields, vanDerPolFull, [function.getSolution()])
termIm=functionPython(nbFields, vanDerPolIm, [function.getSolution()])
claw = dgConservationLawODE(termFull, termIm)

boundary = claw.new0FluxBoundary()
claw.addBoundaryCondition("bottom", boundary)
claw.addBoundaryCondition("top", boundary)
claw.addBoundaryCondition("left", boundary)
claw.addBoundaryCondition("right", boundary)

def initialCondition(val) :
  for i in range (0,val.size1()) :
    val.set(i, 0, 2)
    val.set(i, 1,  -2./3. + ((10./81.) * e) - ((292./2187.) * e*e) - ((1814./19683.) * e**3) )
initF=functionPython(nbFields, initialCondition, [])
solution.interpolate(initF)

petscIm = linearSystemPETScBlockDouble()
dofIm = dgDofManager.newDGBlock(groups, claw.getNbFields(), petscIm)
timeIter = dgIMEXDIMSIM(claw, dofIm, 2)     #timeorder
#timeIter = dgIMEXTSRK(claw, dofIm, 4)     #timeorder
#timeIter = dgIMEXRK(claw, dofIm, 2)     #timeorder
timeIter.getNewton().setVerb(0)     #Verbosity

dt = Tf / nbSteps

if (Msg.GetCommRank()==0):
    print ("Time step:",dt)

t=Ti

timeIter.starter(solution,dt,dt,t,3)

n_export=0


y1=[]
y2=[]
time=[]
for i in range(0,nbSteps-1):
    if (i==nbSteps-1):
        dt=Tf-t
    norm = timeIter.iterate (solution, dt, t)
    solODE.compute(solMat)
    y1.append(solMat(0,0))
    y2.append(solMat(1,0))
    time.append(t)
    t=t+dt
    if (Msg.GetCommRank()==0):
        sys.stdout.write('.')
        sys.stdout.flush()
print ('')

print("Last values of solution:",y1[-1],y2[-1])
print("Reference solution:",1.5419764311030313, -1.1178113574500068)
error1 = sqrt((y1[-1]-1.5419764311030313)**2)
error2 = sqrt((y2[-1]+1.1178113574500068)**2)
if (error1 > 1e-12 or error2 > 1e-12):
  print("Test failed: error is %.15e %.15e",error1, error2)
  Msg.Exit(1)
else:
  print("Test successfully passed")
  Msg.Exit(0)
