#include "dgConservationLawDiffusionSIP.h"
#include "dgGroupOfElements.h"
#include "dgDofContainer.h"
#include "dgMeshJacobian.h"
#include "dgMesh.h"
#include "dgIntegrationMatrices.h"

dgConservationLawDiffusionSIP::dgConservationLawDiffusionSIP(double nuXX, double nuXY, double nuYY, const function *source)
{
  _source = source;
  _kappaXX = nuXX;
  _kappaXY = nuXY;
  _kappaYY = nuYY;
  _SIP = true;
  _ipFactor = -1;
}

void dgConservationLawDiffusionSIP::setIPFactor(double ipFactor)
{
  _ipFactor = ipFactor;
}

void dgConservationLawDiffusionSIP::setBoundaryFunction(const function *fbnd, const function *gradfbnd)
{
  _bnd = fbnd;
  _gradfbnd = gradfbnd;
}

void dgConservationLawDiffusionSIP::setSymmetricIP(bool SIP)
{
  _SIP = SIP;
}

int dgConservationLawDiffusionSIP::getNbFields() const
{
  return 1;
}

void dgConservationLawDiffusionSIP::computeElementaryTimeSteps (const dgGroupOfElements &group, dgDofContainer *solution, std::vector<double> & DT, dgExtrusion *extrusion) const
{
  DT.clear();
  DT.resize(group.getNbElements(), 1.);
}

void dgConservationLawDiffusionSIP::computeElementTerms(const dgGroupOfElements &group, double t, dgDofContainer &solution, dgDofContainer &f, dgDofManager *jacDof, dgFullMatrix<double> *mass, dgDofContainer *variable) const
{
  const nodalBasis &fs = group.getFunctionSpace();
  int iOrder = fs.order * 2 + 1;
  dataCacheMap cacheMap(dataCacheMap::INTEGRATION_GROUP_MODE, group.getGroupCollection(), 0, iOrder);
  cacheMap.setGroup(&group);
  //fullMatrix<double> solQP;
  fullMatrix<double> gradSolQP;
  fullMatrix<double> sourceQP;

  if(_source)
    sourceQP = cacheMap.get(_source)->get();
  //solQP.setAsProxy(cacheMap.get(solution.getFunction()));
  gradSolQP.setAsProxy(cacheMap.get(solution.getFunctionGradient())->get());

  const dgMeshJacobian &jacobian = group.getGroupCollection()->_mesh->getJacobian(iOrder);
  const dgIntegrationMatrices &integ = dgIntegrationMatrices::get(fs.type, iOrder);
  fullMatrix<double> fQP, dXidX, dfQP(integ.getNbIntegrationPoints() * 4, group.getNbElements());
  fQP.resize(integ.getNbIntegrationPoints() * 2, group.getNbElements(), false);
  const fullMatrix<double> &J = jacobian.detJElement(group.elementVectorId());
  int nIP = integ.getNbIntegrationPoints();
  for (size_t iElem = 0; iElem < group.getNbElements(); ++iElem) {
    for (int iIP = 0; iIP < integ.getNbIntegrationPoints(); ++iIP) {
      jacobian.dXiDXElement(group.elementVectorId(), iElem, iIP, dXidX);
      double dudx = gradSolQP(iElem * nIP + iIP, 0);
      double dudy = gradSolQP(iElem * nIP + iIP, 1);
      double j = J(iIP, iElem);
      double fluxX = _kappaXX * dudx + _kappaXY * dudy;
      double fluxY = _kappaXY * dudx + _kappaYY * dudy;
      fQP(iIP * 2 + 0, iElem) = -(dXidX(0, 0) * fluxX + dXidX(0, 1) * fluxY) * j;
      fQP(iIP * 2 + 1, iElem) = -(dXidX(1, 0) * fluxX + dXidX(1, 1) * fluxY) * j;
      if (jacDof) {
        double dfluxX0 = _kappaXX * dXidX(0, 0) + _kappaXY * dXidX(0, 1);
        double dfluxX1 = _kappaXX * dXidX(1, 0) + _kappaXY * dXidX(1, 1);
        double dfluxY0 = _kappaXY * dXidX(0, 0) + _kappaYY * dXidX(0, 1);
        double dfluxY1 = _kappaXY * dXidX(1, 0) + _kappaYY * dXidX(1, 1);
        dfQP(iIP + nIP * 0, iElem) = (dXidX(0, 0) * dfluxX0 + dXidX(0, 1) * dfluxY0) * j;
        dfQP(iIP + nIP * 1, iElem) = (dXidX(1, 0) * dfluxX0 + dXidX(1, 1) * dfluxY0) * j;
        dfQP(iIP + nIP * 2, iElem) = (dXidX(0, 0) * dfluxX1 + dXidX(0, 1) * dfluxY1) * j;
        dfQP(iIP + nIP * 3, iElem) = (dXidX(1, 0) * dfluxX1 + dXidX(1, 1) * dfluxY1) * j;
      }
      if(_source)
        sourceQP(iElem * nIP + iIP, 0) *= j;
    }
  }
  f.getGroupProxy(&group).gemm(integ.dPsiW(), fQP);
  if (_source)
    f.getGroupProxy(&group).gemm(integ.psiW(), sourceQP);
  if (mass)
    mass->setAsProxy(group.getMassMatrix());
  if (jacDof) {
    dgFullMatrix<double> jac(group.getNbNodes() * group.getNbNodes(), 1, group.getNbElements(), false);
    integ.dPsiDPsiW().mult(dfQP, jac);
    fullMatrix<double> ejac;
    int iGroup = group.elementVectorId();
    for (size_t i = 0; i < group.getNbElements(); ++i)  {
      jac.getBlockProxy(i, ejac);
      ejac.reshape(group.getNbNodes(), group.getNbNodes());
      jacDof->assembleLHSMatrix(iGroup, i, iGroup, i, ejac);
    }
  }
}

void dgConservationLawDiffusionSIP::computeFaceTerms(const dgGroupOfFaces &faces, double t, dgDofContainer &solutionDof, dgDofContainer &residual,  dgDofManager *jacDof, dgDofContainer *variable) const
{
  bool bnd = faces.nConnection() == 1;
  const nodalBasis &fsg = faces.elementGroup(0).getFunctionSpace();
  int iOrder = fsg.order * 2 + 1;
  dataCacheMap cacheMap(dataCacheMap::INTEGRATION_GROUP_MODE, faces.elementGroup(0).getGroupCollection(), faces.nConnection() - 1, iOrder);
  cacheMap.setInterfaceGroup(&faces);
  fullMatrix<double> solR, solL, gradSolR, gradSolL;
  const dgMesh &mesh = *faces.elementGroup(0).getGroupCollection()->_mesh;
  const dgIntegrationMatrices &integ = dgIntegrationMatrices::get(fsg.type, iOrder, mesh.interfaceVector(faces.interfaceVectorId()).closureRef(0));
  const dgIntegrationMatrices &integRight = bnd ? integ :dgIntegrationMatrices::get(faces.elementGroup(1).getFunctionSpace().type, iOrder, mesh.interfaceVector(faces.interfaceVectorId()).closureRef(1));
  solL = cacheMap.get(solutionDof.getFunction())->get();
  gradSolL =  cacheMap.get(solutionDof.getFunctionGradient())->get();
  if (!bnd) {
    solR = cacheMap.getSecondaryCache(1)->get(solutionDof.getFunction())->get();
    gradSolR =  cacheMap.getSecondaryCache(1)->get(solutionDof.getFunctionGradient())->get();
  }
  else {
    solR = cacheMap.get(_bnd)->get();
    gradSolR =  cacheMap.get(_gradfbnd)->get();
  }
  int nIP = integ.getNbIntegrationPoints();
  fullMatrix<double> fQP(nIP, faces.size()), dfQP(nIP, faces.size());
  fullMatrix<double> f1QPL(nIP * 2, faces.size()), f1QPR(nIP * 2, faces.size());
  const dgMeshJacobian &meshjac = mesh.getJacobian(iOrder);
  const fullMatrix<double> &J = meshjac.detJInterface(faces.interfaceVectorId());
  const dgGroupOfElements &eGroupL = faces.elementGroup(0);
  const dgGroupOfElements &eGroupR = bnd ? eGroupL : faces.elementGroup(1);
  int n0 = std::max(eGroupL.getFunctionSpace().numFaces, eGroupR.getFunctionSpace().numFaces);
  int p = eGroupL.getOrder();
  int dim = eGroupL.getDimUVW();
  fullMatrix<double> normal, dXidXL, dXidXR, ddfQPL(nIP * 2, faces.size()), ddfQPR(nIP * 2, faces.size());
  for (size_t iFace = 0; iFace < faces.size(); ++iFace) {
    faces.normal(meshjac, iFace, 0, normal);
    double minV = eGroupL.elementVolume(meshjac, faces.elementId(iFace, 0)) ;
    if (!bnd)
      minV = std::min(minV, eGroupR.elementVolume(meshjac, faces.elementId(iFace, 1)));
    double minL = minV / (n0 * faces.interfaceSurface(meshjac, iFace));
    double mufactor = (p + 1) * (p + dim) / (dim * minL) / 2;
    for (int iIP = 0; iIP < nIP; ++iIP) {
      meshjac.dXiDXInterface(faces.interfaceVectorId(), 0, iFace, iIP, dXidXL);
      meshjac.dXiDXInterface(faces.interfaceVectorId(), bnd ? 0 : 1, iFace, iIP, dXidXR);
      double nx = normal(iIP, 0);
      double ny = normal(iIP, 1);
      double nkx = nx * _kappaXX + ny * _kappaXY, nky = nx * _kappaXY + ny * _kappaYY;
      double nkn = nx * nkx + ny * nky;
      double IP = _ipFactor < 0 ? nkn * mufactor : _ipFactor;
      double meanGradx = 0.5 * (gradSolR(iFace * nIP + iIP, 0) + gradSolL(iFace * nIP + iIP, 0));
      double meanGrady = 0.5 * (gradSolR(iFace * nIP + iIP, 1) + gradSolL(iFace * nIP + iIP, 1));
      double jump = (solL(iFace * nIP + iIP, 0) - solR(iFace * nIP + iIP, 0)) / 2.;
      double dFluxL[2] = {nkx * dXidXL(0, 0) + nky * dXidXL(0, 1), nkx * dXidXL(1, 0) + nky * dXidXL(1, 1)};
      double dFluxR[2] = {nkx * dXidXR(0, 0) + nky * dXidXR(0, 1), nkx * dXidXR(1, 0) + nky * dXidXR(1, 1)};
      double j = J(iIP, iFace) ;
      fQP(iIP, iFace) = (nkx * meanGradx + nky * meanGrady - IP * jump) * j;
      if (_SIP) {
        f1QPL(iIP * 2 + 0, iFace) = j * jump * dFluxL[0];
        f1QPL(iIP * 2 + 1, iFace) = j * jump * dFluxL[1];
        f1QPR(iIP * 2 + 0, iFace) = j * jump * dFluxR[0];
        f1QPR(iIP * 2 + 1, iFace) = j * jump * dFluxR[1];
      }
      if(jacDof) {
        dfQP(iIP, iFace) = -IP * j / 2;
        ddfQPL(iIP * 2 + 0, iFace) = -0.5 * j * dFluxL[0];
        ddfQPL(iIP * 2 + 1, iFace) = -0.5 * j * dFluxL[1];
        ddfQPR(iIP * 2 + 0, iFace) = -0.5 * j * dFluxR[0];
        ddfQPR(iIP * 2 + 1, iFace) = -0.5 * j * dFluxR[1];
      }
    }
  }
  int nNode = integ.psiW().size1();
  int nNodeL = eGroupL.getNbNodes();
  if (bnd) {
    fullMatrix<double> f(integ.psiW().size1(), faces.size());
    integ.psiW().mult(fQP, f);
    fullMatrix<double> &fg0 = residual.getGroupProxy(&eGroupL);
    for (size_t i = 0; i < faces.size(); ++i) {
      const std::vector<int> &closure0 = faces.closure(i, 0), fullClosure0 = faces.fullClosure(i, 0);
      for(int j = 0; j < nNode; ++j) {
        fg0(closure0[j], faces.elementId(i, 0)) += f(j, i);
      }
    }
    if (_SIP) {
      fullMatrix<double> f1L(nNodeL, faces.size());
      integ.dPsiW().mult(f1QPL, f1L);
      for (size_t i = 0; i < faces.size(); ++i) {
        const std::vector<int> fullClosure0 = faces.fullClosure(i, 0);
        for(int j = 0; j < nNodeL; ++j) {
          fg0(fullClosure0[j], faces.elementId(i, 0)) += 1 * f1L(j, i);
        }
      }
    }
    if (jacDof) {
      fullMatrix<double> LL(nNodeL, nNodeL);
      dgFullMatrix<double> df(integ.psiPsiW().size1(), 1, faces.size(), false);
      integ.psiPsiW().mult(dfQP, df);
      dgFullMatrix<double> ddfL(nNode * nNodeL, 1, faces.size(), false);
      integ.psiDPsiW().mult(ddfQPL, ddfL);
      fullMatrix<double> proxy, proxyDL;
      for (size_t iFace = 0; iFace < faces.size(); ++iFace) {
        df.getBlockProxy(iFace, proxy);
        ddfL.getBlockProxy(iFace, proxyDL);
        proxy.reshape(nNode, nNode);
        proxyDL.reshape(nNode, nNodeL);
        LL.setAll(0.);
        const std::vector<int> closureL = faces.closure(iFace, 0);
        const std::vector<int> fullClosureL = faces.fullClosure(iFace, 0);
        for (int i = 0; i < nNode; i++) {
          for (int j = 0; j < nNodeL; j++) {
            LL(closureL[i], fullClosureL[j]) += 1 * proxyDL(i, j);
            if (_SIP)
              LL(fullClosureL[j], closureL[i]) += 1 * proxyDL(i, j);
          }
        }
        for (int i = 0; i < nNode; i++) {
          for (int j = 0; j < nNode; j++) {
            LL(closureL[i], closureL[j]) += -proxy(i, j);
          }
        }
        int iGroupL = eGroupL.elementVectorId();
        int iElementL = faces.elementId(iFace, 0);
        jacDof->assembleLHSMatrix(iGroupL, iElementL, iGroupL, iElementL, LL);
      }
    }
  }
  else { // not bnd
    int nNodeR = eGroupR.getNbNodes();
    fullMatrix<double> f(integ.psiW().size1(), faces.size());
    integ.psiW().mult(fQP, f);
    fullMatrix<double> &fg0 = residual.getGroupProxy(&eGroupL), f1L(nNodeL, faces.size());
    fullMatrix<double> &fg1 = residual.getGroupProxy(&eGroupR), f1R(nNodeR, faces.size());
    for (size_t i = 0; i < faces.size(); ++i) {
      const std::vector<int> &closure0 = faces.closure(i, 0), &closure1 = faces.closure(i, 1);
      for(int j = 0; j < nNode; ++j) {
        fg0(closure0[j], faces.elementId(i, 0)) += f(j, i);
        fg1(closure1[j], faces.elementId(i, 1)) -= f(j, i);
      }
    }
    if (_SIP) {
      integ.dPsiW().mult(f1QPL, f1L);
      integRight.dPsiW().mult(f1QPR, f1R);
      for (size_t i = 0; i < faces.size(); ++i) {
        const std::vector<int> &fullClosure0 = faces.fullClosure(i, 0), fullClosure1 = faces.fullClosure(i, 1);
        for(int j = 0; j < nNodeL; ++j) {
          fg0(fullClosure0[j], faces.elementId(i, 0)) += f1L(j, i);
        }
        for(int j = 0; j < nNodeR; ++j) {
          fg1(fullClosure1[j], faces.elementId(i, 1)) += f1R(j, i);
        }
      }
    }
    if (jacDof) {
      int nNodeR = eGroupR.getNbNodes();
      fullMatrix<double> LL(nNodeL, nNodeL);
      fullMatrix<double> LR(nNodeL, nNodeR);
      fullMatrix<double> RL(nNodeR, nNodeL);
      fullMatrix<double> RR(nNodeR, nNodeR);
      dgFullMatrix<double> df(integ.psiPsiW().size1(), 1, faces.size(), false);
      integ.psiPsiW().mult(dfQP, df);
      dgFullMatrix<double> ddfL(nNode * nNodeL, 1, faces.size(), false);
      dgFullMatrix<double> ddfR(nNode * nNodeR, 1, faces.size(), false);
      integ.psiDPsiW().mult(ddfQPL, ddfL);
      integRight.psiDPsiW().mult(ddfQPR, ddfR);
      fullMatrix<double> proxy, proxyDL, proxyDR;
      for (size_t iFace = 0; iFace < faces.size(); ++iFace) {
        df.getBlockProxy(iFace, proxy);
        ddfL.getBlockProxy(iFace, proxyDL);
        ddfR.getBlockProxy(iFace, proxyDR);
        proxy.reshape(nNode, nNode);
        proxyDL.reshape(nNode, nNodeL);
        proxyDR.reshape(nNode, nNodeR);
        RR.setAll(0.);
        LL.setAll(0.);
        RL.setAll(0.);
        LR.setAll(0.);
        const std::vector<int> closureL = faces.closure(iFace, 0);
        const std::vector<int> closureR = faces.closure(iFace, 1);
        const std::vector<int> fullClosureL = faces.fullClosure(iFace, 0);
        const std::vector<int> fullClosureR = faces.fullClosure(iFace, 1);
        for (int i = 0; i < nNode; i++) {
          for (int j = 0; j < nNodeL; j++) {
            LL(closureL[i], fullClosureL[j]) += proxyDL(i, j);
            RL(closureR[i], fullClosureL[j]) += -proxyDL(i, j);
            if (_SIP) {
              LL(fullClosureL[j], closureL[i]) += proxyDL(i, j);
              LR(fullClosureL[j], closureR[i]) += -proxyDL(i, j);
            }
          }
          for (int j = 0; j < nNodeR; j++) {
            LR(closureL[i], fullClosureR[j]) += proxyDR(i, j);
            RR(closureR[i], fullClosureR[j]) += -proxyDR(i, j);
            if (_SIP) {
              RL(fullClosureR[j], closureL[i]) += proxyDR(i, j);
              RR(fullClosureR[j], closureR[i]) += -proxyDR(i, j);
            }
          }
        }
        for (int i = 0; i < nNode; i++) {
          for (int j = 0; j < nNode; j++) {
            LL(closureL[i], closureL[j]) += -proxy(i, j);
            LR(closureL[i], closureR[j]) += proxy(i, j);
            RL(closureR[i], closureL[j]) += proxy(i, j);
            RR(closureR[i], closureR[j]) += -proxy(i, j);
          }
        }
        int iGroupL = eGroupL.elementVectorId();
        int iGroupR = eGroupR.elementVectorId();
        int iElementL = faces.elementId(iFace, 0);
        int iElementR = faces.elementId(iFace, 1);
        jacDof->assembleLHSMatrix(iGroupL, iElementL, iGroupL, iElementL, LL);
        jacDof->assembleLHSMatrix(iGroupL, iElementL, iGroupR, iElementR, LR);
        jacDof->assembleLHSMatrix(iGroupR, iElementR, iGroupL, iElementL, RL);
        jacDof->assembleLHSMatrix(iGroupR, iElementR, iGroupR, iElementR, RR);
      }
    }
  }
}
