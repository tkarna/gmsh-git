#ifndef _DG_CONSERVATION_LAW_DIFFUSION_SIP_H_
#define _DG_CONSERVATION_LAW_DIFFUSION_SIP_H_
#include "dgConservationLaw.h"
class function;
class dgConservationLawDiffusionSIP:public dgConservationLaw {
  double _kappaXX, _kappaYY, _kappaXY;
  double _ipFactor; // if >= 0, this value replaces the factor multiplying the jump
  bool _SIP;
  const function *_source, *_bnd, *_gradfbnd;
  public :
  int getNbFields() const;
  void computeElementTerms(const dgGroupOfElements &group, double t, dgDofContainer &solution, dgDofContainer &residual, dgDofManager *jacDof = NULL, dgFullMatrix<double> *mass = NULL, dgDofContainer *variable = NULL) const;
  void computeFaceTerms(const dgGroupOfFaces &faces, double t, dgDofContainer &solutionDof, dgDofContainer &residual,  dgDofManager *jacDof = NULL, dgDofContainer *variable = NULL) const;
  void computeElementaryTimeSteps (const dgGroupOfElements & group, dgDofContainer *solution, std::vector<double> & DT, dgExtrusion *extrusion = NULL) const;
  dgConservationLawDiffusionSIP(double nuXX, double nuXY, double nuYY, const function *source = NULL);
  bool isConstantJac() const {return true;}
  bool isLinear() const {return false;}
  void setBoundaryFunction(const function *fbnd, const function *gradfbnd);
  void setSymmetricIP(bool SIP);
  void setIPFactor(double IPFactor);
};
#endif
