#include "dgSolverSupra.h"
#include "dgConservationLaw.h"
#include "dgTerm.h"
dgNewtonSupra::dgNewtonSupra (dgDofManager &dof, const dgConservationLaw &law) :
  dgNewton(dof, law),
  _const1Var_in_beta_law (_dof.getGroups(), _law.getNbFields()),
  _const2Var_in_beta_law (_dof.getGroups(), _law.getNbFields()),
  _nsupra (_dof.getGroups(), _law.getNbFields()),
  _BetaOfCurrentSolution (_dof.getGroups(), _law.getNbFields())
{
  _maxit = 30;
  _atol = 1e-14;
  _rtol = 1e-10;
  _verb = 1;
  _law.fixStrongBC(_dof);
  _dof.fillSparsityPattern(!_dof.isContinuous(), &law);
  _assembleLHSMatrix = true;
  _preProc = NULL;
  _returnZeroIfNotConverged = false;
}

//rhs = _F + RG - M * dU * alpha, lhs += M * alpha 
/*solve powerlaw for superconductors materials*/
/** assemble system such as:  \partial_t b(u) = f(t,u) with b(x) = sqrt(x*x+v*v)^alpha * x , 0<alpha<1 **/
/** In this function, the jacobian of _newton is scale by (b^(-1))'(v) **/
void dgNewtonSupra::assembleSystem(double alpha, const dgDofContainer *R, double t, dgDofContainer * variable)
{
  const dgGroupCollection &groups = _dof.getGroups();
  _F.setAll(0.0);
  _law.fixStrongBC(_dof, &_currentSolution);
  _dof.zeroRHS();
  std::vector<dgFullMatrix<double> > MG(groups.getNbElementGroups());
  
  _residual.setAll(0.);
  if (_assembleLHSMatrix)
    _dof.zeroMatrix();
  _currentSolution.scatterBegin(!_dof.isContinuous());
  _BetaOfCurrentSolution.scatterBegin(!_dof.isContinuous());
  _dof.setSupra(_currentSolution, _BetaOfCurrentSolution, _nsupra, _const1Var_in_beta_law, _const2Var_in_beta_law);
  for (int iGroup = 0; iGroup < groups.getNbElementGroups(); iGroup++) {
    const dgGroupOfElements &group = *groups.getElementGroup(iGroup);
    _law.computeElementMass(group, t, &_dU, &_residual, alpha, _assembleLHSMatrix ? &_dof: NULL);
    _law.computeElement(group, t, _BetaOfCurrentSolution, _F, &_dof, variable);
  }
  _currentSolution.scatterEnd();
  _BetaOfCurrentSolution.scatterEnd();
  for (int iGroup = 0; iGroup < groups.getNbFaceGroups (); iGroup++) {
    dgGroupOfFaces *group = groups.getFaceGroup(iGroup);
    if(_dof.isContinuous() && group->nConnection() != 1) continue;
    _law.computeFace(*group, t, _BetaOfCurrentSolution, _F, &_dof, variable);
  }
  if (_law.isConstantJac()) _assembleLHSMatrix=false;
  
  _dof.setMatrixFreeParameters(_currentSolution, _F, alpha);
  _residual.axpy(_F);
  if (R)
    _residual.axpy(*R);
  fullMatrix<double> rhs;
  for (int iGroup = 0; iGroup < groups.getNbElementGroups (); iGroup++){
    dgGroupOfElements *group = groups.getElementGroup(iGroup);
    dgFullMatrix<double> &GG = _residual.getGroupProxy (iGroup);
    for (size_t iElement = 0 ; iElement < group->getNbElements(); iElement++) {
      GG.getBlockProxy(iElement, rhs);
      _dof.assembleRHSVector(iGroup, iElement, rhs);
    }
  }
}

static double invbeta(double a, double b, double c, double n)
{
  double z=0;
  if(n==1) z=a;
  else z = a*pow(a*a+b*b+c*c,(n-1)/2);
  return z;
}
/** solve and get solution=v=b(u), powerofsolution=u, alpha = 1/nsupra - 1**/

const dgDofContainer &dgNewtonSupra::solveBetaPowerLaw ( const dgDofContainer *U0, const dgDofContainer *V0, const dgDofContainer *W0, dgDofContainer *powerSolution, dgDofContainer *nsupraLaw, double alpha, const dgDofContainer *R, double t, dgDofContainer *variable){


const dgGroupCollection &groups = _dof.getGroups();
  int nbFields = _law.getNbFields();
  
  firstNewtonResidual = 0.0;
  bool hasConverged = false;

   if (U0)
    _currentSolution.copy (*U0);
  else
    _currentSolution.setAll (0.);

  _dU.setAll (0.0);
  _const1Var_in_beta_law.copy(*V0);
  _const2Var_in_beta_law.copy(*W0);
  _BetaOfCurrentSolution.copy(*powerSolution);
  _nsupra.copy (*nsupraLaw);

int ii;
  for (ii = 0; ii < _maxit; ii++) {

     if(_preProc) _preProc->call();

    assembleSystem(alpha, R, t, variable);

    // Compute Newton Residual and check convergence
    double newtonResidual = 0;
    newtonResidual = _dof.getNormInfRHS();
    if (newtonResidual != newtonResidual) {
      if (_verb > 0)
        Msg::Info("Newton failed");
      _converged = 0;
      return _dU;
    }
    double conv = 0.0;
    if (ii == 0)
      firstNewtonResidual = newtonResidual;
    if (ii > 0 && firstNewtonResidual == 0.0 )
      conv = log(1. / newtonResidual) / log(10);
    else if (ii > 0. && newtonResidual != 0.0 )
      conv = log(firstNewtonResidual / newtonResidual) / log(10);
    if (_verb > 1)
      Msg::Info("Newton residual (%i): %g (%.2f)", ii, newtonResidual, conv);
      //todo : this check ii>0 are for boundary condition but we have to find another solution
    if (ii > 0 && ((firstNewtonResidual != 0.0 && newtonResidual / firstNewtonResidual < _rtol) || (newtonResidual < _atol))) {
      hasConverged = true;
      break;
    }
    _dof.solve();
    // store for the next newton iteration
    // new sol = solGroup:  U(n+1)^{k+1}      = U(n+1)^{k}      + [U(n+1)^{k+1} - U(n+1)^{k}]
    // and  deltaG:         U(n+1)^{k+1}-U(n) = U(n+1)^{k}-U(n) + [U(n+1)^{k+1} - U(n+1)^{k}]
    fullMatrix<double> v;
    for (int iGroup = 0; iGroup < groups.getNbElementGroups(); iGroup++){
      dgGroupOfElements *group = groups.getElementGroup(iGroup);
      fullMatrix<double> & solgroup = _currentSolution.getGroupProxy(group);
      fullMatrix<double> & deltaG = _dU.getGroupProxy(group);
      fullMatrix<double> & const1Var_in_betaSolgroup = _const1Var_in_beta_law.getGroupProxy(group);
      fullMatrix<double> & const2Var_in_betaSolgroup = _const2Var_in_beta_law.getGroupProxy(group);
      fullMatrix<double> & BetaOfsolgroup = _BetaOfCurrentSolution.getGroupProxy(group);
      fullMatrix<double> & nsupra = _nsupra.getGroupProxy(group); 
      int nbNodes = group->getNbNodes();
      v.resize (nbNodes * nbFields, 1);
      for (size_t iElement = 0 ; iElement < group->getNbElements(); iElement++) {
        _dof.getSolution(iGroup, iElement, v);
        for ( int i = 0; i < nbNodes; i++) {
          for ( int j = 0; j < nbFields; j++) {             	   

            solgroup(i, iElement*nbFields+j) += v(j*nbNodes+i,0);
            deltaG (i, iElement*nbFields + j) += v (j*nbNodes+i, 0);
            BetaOfsolgroup(i, iElement*nbFields+j)=invbeta(solgroup(i,iElement*nbFields+j), const1Var_in_betaSolgroup(i,iElement*nbFields+j),const2Var_in_betaSolgroup(i,iElement*nbFields+j),nsupra(i,iElement*nbFields+j));
          }
        }
      }

       (*powerSolution).copy(_BetaOfCurrentSolution);
    }
    _currentSolution.scatter(!_dof.isContinuous());
    _dU.scatter(!_dof.isContinuous());
    _nsupra.scatter(!_dof.isContinuous());
    _BetaOfCurrentSolution.scatter(!_dof.isContinuous());
    if ( _law.isLinear() ) {
      hasConverged = true;
      break;
    }
  }
  
  if (!hasConverged && _verb>0) {
    Msg::Info("Newton not converged after %i iters !!",_maxit);
    _converged = 0;
  }
  else {
    _converged = ii + 1;
  }
  
  return _dU;
}


void dgSolveSupraBetaPowerLaw::iterateBetaVectorial2DPowerLaw (dgDofContainer &solutionX, dgDofContainer &solutionY, dgDofContainer &betaSolutionX, dgDofContainer &betaSolutionY, dgDofContainer &nsupra, double dt, double t)
{
  
  _X.copy(solutionX);
  _X0.copy(solutionX);
  _Y.copy(solutionY);
  _Y0.setAll(0.0);
  _N.copy(nsupra);
  _betaX.copy(betaSolutionX);
  _betaY.copy(betaSolutionY);
  _M.copy(solutionX);
  solutionX.axpy(_newtonX.solveBetaPowerLaw(&_X, &_Y,&_Y0,&_betaX,&_N,1.0/dt, NULL, t));
  solutionY.axpy(_newtonY.solveBetaPowerLaw(&_Y, &_X,&_Y0,&_betaY,&_N,1.0/dt, NULL, t));
  betaSolutionX.copy(_betaX);
  betaSolutionY.copy(_betaY);
}


 
void dgSolveSupraBetaPowerLaw::iterateBetaVectorial3DPowerLaw (dgDofContainer &solutionX, dgDofContainer &solutionY, dgDofContainer &solutionZ, dgDofContainer &betaSolutionX, dgDofContainer &betaSolutionY, dgDofContainer &betaSolutionZ, dgDofContainer &nsupra, double dt, double t)
{
  _X.copy(solutionX);
  _X0.copy(solutionX);
  _Y.copy(solutionY);
  _Y0.copy(solutionY);
  _Z.copy(solutionZ);
  _N.copy(nsupra);
  _betaX.copy(betaSolutionX);
  _betaY.copy(betaSolutionY);
  _betaZ.copy(betaSolutionZ);
  _M.copy(solutionX);
  solutionX.axpy(_newtonX.solveBetaPowerLaw(&_X, &_Y0, &_Z,&_betaX,&_N,1.0/dt, NULL, t));
  solutionY.axpy(_newtonY.solveBetaPowerLaw(&_Y, &_X0, &_Z,&_betaY,&_N,1.0/dt, NULL, t));
  solutionZ.axpy(_newtonZ.solveBetaPowerLaw(&_Z, &_X0, &_Y0,&_betaZ,&_N,1.0/dt, NULL, t));
  betaSolutionX.copy(_betaX);
  betaSolutionY.copy(_betaY);
  betaSolutionZ.copy(_betaZ); 
}
 

void dgSolveSupraBetaPowerLaw::iterateBetaScalarPowerLaw ( dgDofContainer &solutionX, dgDofContainer &betaSolutionX, dgDofContainer &nsupra, double dt, double t)
{
  _X.copy(solutionX);
  _X0.setAll(0.);
  _N.copy(nsupra);
  _betaX.copy(betaSolutionX);
  _M.copy(solutionX);
  solutionX.axpy(_newtonX.solveBetaPowerLaw(&_X,&_X0,&_X0, &_betaX,&_N,1.0/dt, NULL, t)); 
  betaSolutionX.copy(_betaX);        
}




dgSolveSupraBetaPowerLaw::dgSolveSupraBetaPowerLaw ( dgConservationLaw &clawX, dgConservationLaw &clawY, dgConservationLaw &clawZ, dgDofManager &dofX, dgDofManager &dofY, dgDofManager &dofZ) :
    _newtonX (dofX, clawX),
    _newtonY (dofY, clawY),
    _newtonZ (dofZ, clawZ),
    _X     (dofX.getGroups(), clawX.getNbFields()),
    _Y     (dofY.getGroups(), clawY.getNbFields()),
    _Z     (dofZ.getGroups(), clawY.getNbFields()),
    _N     (dofX.getGroups(), clawX.getNbFields()),
    _X0    (dofX.getGroups(), clawX.getNbFields()),
    _Y0    (dofY.getGroups(), clawY.getNbFields()),
    _betaX (dofX.getGroups(), clawX.getNbFields()),
    _betaY (dofY.getGroups(), clawY.getNbFields()),
    _betaZ (dofZ.getGroups(), clawZ.getNbFields()),
    _M     (dofX.getGroups(), clawX.getNbFields())  
{
}


dgSolveSupraBetaPowerLaw::dgSolveSupraBetaPowerLaw ( dgConservationLaw &clawX, dgConservationLaw &clawY, dgDofManager &dofX, dgDofManager &dofY) :
    _newtonX (dofX, clawX),
    _newtonY (dofY, clawY),
    _newtonZ (dofY, clawY),
    _X     (dofX.getGroups(), clawX.getNbFields()),
    _Y     (dofY.getGroups(), clawY.getNbFields()),
    _Z     (dofY.getGroups(), clawY.getNbFields()),
    _N     (dofX.getGroups(), clawX.getNbFields()),
    _X0    (dofX.getGroups(), clawX.getNbFields()),
    _Y0    (dofY.getGroups(), clawY.getNbFields()),
    _betaX (dofX.getGroups(), clawX.getNbFields()),
    _betaY (dofY.getGroups(), clawY.getNbFields()),
    _betaZ (dofX.getGroups(), clawY.getNbFields()),
    _M     (dofX.getGroups(), clawX.getNbFields())  
{
}



dgSolveSupraBetaPowerLaw::dgSolveSupraBetaPowerLaw (dgConservationLaw &clawX, dgDofManager &dofX) :
    _newtonX (dofX, clawX),
    _newtonY (dofX, clawX),
    _newtonZ (dofX,clawX),
    _X     (dofX.getGroups(), clawX.getNbFields()),
    _Y     (dofX.getGroups(), clawX.getNbFields()),
    _Z     (dofX.getGroups(), clawX.getNbFields()),
    _N     (dofX.getGroups(), clawX.getNbFields()),
    _X0    (dofX.getGroups(), clawX.getNbFields()),
    _Y0    (dofX.getGroups(), clawX.getNbFields()),
    _betaX (dofX.getGroups(), clawX.getNbFields()),
    _betaY (dofX.getGroups(), clawX.getNbFields()),
    _betaZ (dofX.getGroups(), clawX.getNbFields()),
    _M     (dofX.getGroups(), clawX.getNbFields())  
{
}
