
lc = 2.5;
a=20;
H=5;
Point(1) = {0, 0, -H,lc};
Point(2) = {a, 0, -H,lc};
Point(3) = {0, a, -H,lc};
Point(4) = {0, -a, -H,lc};
Point(5) = {-a, 0, -H,lc};
Circle(1) = {3, 1, 2};
Circle(2) = {2, 1, 4};
Circle(3) = {4, 1, 5};
Circle(4) = {5, 1, 3};
Extrude {0, 0, 2*H} {
  Line{4, 1, 2, 3};
}


Compound Surface(10000) = {20, 16, 12, 8};

Line Loop(21)= {1,2,3,4};
Plane Surface(22) = {21};
Line Loop(23)={17,13,9,5};
Plane Surface(24)={23};
Surface Loop(25) = {20,16,12,8,22,24};
Physical Surface("inlet_cylinder") ={22};
Physical Surface("outlet_cylinder") ={24};
Physical Surface("lateral_cylinder") = {20,8,16,12};
Volume(26) = {25};
Physical Volume("cylinder") = {26};



