%module supra

%{
  #undef HAVE_DLOPEN
  #include "dgSolverSupra.h"
%}

%import(module="dgpy.timeIntegrator") "dgNewton.h";

%include "dgSolverSupra.h"
