#ifndef _DG_SOLVER_SUPRA_H_
#define _DG_SOLVER_SUPRA_H_
#include "dgNewton.h"
/*solve powerlaw for superconductors materials*/
/** assemble system such as:  \partial_t b(u) = f(t,u) with b(x) = sqrt(x*x+v*v)^alpha * x , 0<alpha<1 **/
/** In this function, the jacobian of _newton is scale by (b^(-1))'(v) **/
class dgNewtonSupra:public dgNewton
{
 protected:
  dgDofContainer _const1Var_in_beta_law, _const2Var_in_beta_law, _nsupra, _BetaOfCurrentSolution;
 public:
  void assembleSystem(double alpha, const dgDofContainer *R, double t, dgDofContainer *variable = NULL);
  //would it be possible to use the same interface as dgNewton::solve ?
  const dgDofContainer &solveBetaPowerLaw (const dgDofContainer *U0, const dgDofContainer *V0, const dgDofContainer *W0,
                                           dgDofContainer *powerSolution, dgDofContainer *nsupraLaw, double alpha, const dgDofContainer *R, double t,
                                           dgDofContainer *variable = NULL);

  dgNewtonSupra(dgDofManager &dof, const dgConservationLaw &law);
  ~dgNewtonSupra(){};
};


/** solve  \partial_t b(u) = f(t,u) with b(x) = sqrt(x*x+v*v)^alpha * x , -1<alpha<0 **/
/** solution=v=b(u), powerofsolution=u, alpha = 1/nsupra - 1**/
class dgSolveSupraBetaPowerLaw
{
  dgNewtonSupra _newtonX, _newtonY, _newtonZ;
  dgDofContainer _X, _Y,_Z, _N, _X0, _Y0, _betaX,_betaY,_betaZ, _M;
 public:
  inline dgNewtonSupra &getNewtonX() {return _newtonX;}
  inline dgNewtonSupra &getNewtonY() {return _newtonY;}
  inline dgNewtonSupra &getNewtonZ() {return _newtonZ;}
  void iterateBetaVectorial3DPowerLaw ( dgDofContainer &solutionX, dgDofContainer &solutionY,  dgDofContainer &solutionZ, dgDofContainer &powerSolutionX, dgDofContainer &powerSolutionY, dgDofContainer &powerSolutionZ, dgDofContainer &nsupra, double dt, double t);
  void iterateBetaVectorial2DPowerLaw ( dgDofContainer &solutionX, dgDofContainer &solutionY,  dgDofContainer &powerSolutionX, dgDofContainer &powerSolutionY, dgDofContainer &nsupra, double dt, double t);
  void iterateBetaScalarPowerLaw      ( dgDofContainer &solutionX, dgDofContainer &powerSolutionX,  dgDofContainer &nsupra, double dt, double t);
  dgSolveSupraBetaPowerLaw ( dgConservationLaw &clawX, dgConservationLaw &clawY, dgConservationLaw &clawZ, dgDofManager &dofX, dgDofManager &dofY, dgDofManager &dofZ);
  dgSolveSupraBetaPowerLaw ( dgConservationLaw &clawX, dgConservationLaw &clawY, dgDofManager &dofX, dgDofManager &dofY);
  dgSolveSupraBetaPowerLaw ( dgConservationLaw &clawX, dgDofManager &dofX);
};
#endif
