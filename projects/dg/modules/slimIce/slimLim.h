#ifndef SLIM_LIM_H
#define SLIM_LIM_H
#include "fullMatrix.h"
#include "function.h"
#include "dgGroupOfElements.h"
#include "MElement.h"
#include "dgDofContainer.h"
#include "dgConfig.h"

#ifdef HAVE_LIM
class slimLIM {
  // input data forcing etc
  const function *_cloudCover, *_windStress, *_windMod, *_airTempTen, *_totPrec, *_humSpec;
  //input sea ice variables that have changed
  const function *_a_i, *_v_i, *_v_s, *_e_s, *_e_i, *_smv_i; 
  // input from the ocean
  const function *_oVelocity, *_oTemperature,*_oTempKelvin, *_oSalinity, *_coord;
  // ice state variables
  dgDofContainer *_iConcentration, *_iVolume, *_iThick, *_sThick, *_sVolume, *_iConT, *_ti, *_si, *_eti, *_area, *_iVolT, *_alb, *_ts, *_ei, *_es, *_smvi;
  //ocean state variables
  dgDofContainer  *_oQns, *_oQsr, *_oEmp, *_oEmps, *_oTempMod, *_fluxHeat, *_fluxBM, *_fluxSM, *_fluxLG,*_fluxBG, *_fsbbq, *_fdtcn, *_qcmif, *_qldif, *_tempSu, *_precip; //, *_oSalMod,
  // maps DG dofs to lim dofs
  std::vector< std::vector < std::vector<int> > > _mapDofToLIM;
  // parameters
  int _nbIceClasses;
  int _date;
  int _nbLimNodes, _nbLimPoints;
  
  fullMatrix<double> Mei, Mes, Msmvi;
  
  fullMatrix<double> MCloudCover, MWindStress, MWindMod, MAirTempTen, MTotPrec, MHumSpec, MOvel, MOTemp, MOSal, MCoord, Mti, Msi, Meti, Marea;
  
  fullMatrix<double> MiConc, MiThick, MiVol, MsThick, MsVol, MoQns, MoQsr,MoEmp, MoEmps, MiConT, MoTempM,MiHeatF,MiBM,MiSM,MiLG,MiBG, Mfsbbq,Mfdtcn, Mqldif, Mqcmif, MTsu, Mprecip, MiVolT, Malb, Mts;        //, MoTempM, MoSalM
  
  dgGroupCollection *_groups;
  
  public:
  // does the initialization of LIM
  slimLIM(dgGroupCollection *groups2d, int nbIceClasses, const function* coord);
  // destroys dofContainers and LIM
  ~slimLIM();
  //import inital data from functions
  void importInitFunc();
  // loop over all elements, evaluate functions on each element
  void update(int dt, int date, const function* cloudCover, const function* windStress, const function* windMod, const function* airTempTen, const function* totPrec, const function* humSpec,const function *oVelocity, const function* oTemperature, const function* oTempKelvin, const function* oSalinity, const function* a_i, const function* v_i, const function* v_s, const function* e_i, const function* e_s, const function* smv_i);
  void copyToLIM();
  void copyFromLIM();
  const dgDofContainer* getIConcentration() {return _iConcentration;}
  const dgDofContainer* getIVolume() {return _iVolume;}
  const dgDofContainer* getIThick() {return _iThick;}
  const dgDofContainer* getSThick() {return _sThick;}
  const dgDofContainer* getSVolume() {return _sVolume;}
  const dgDofContainer* getiVolT() {return _iVolT;}
  const dgDofContainer* getIConT() {return _iConT;}
  const dgDofContainer* getHeatFlux() {return _fluxHeat;};
  const dgDofContainer* getfluxBM() {return _fluxBM;};
  const dgDofContainer* getfluxBG() {return _fluxBG;};
  const dgDofContainer* getfluxLG() {return _fluxLG;};
  const dgDofContainer* getfluxSM() {return _fluxSM;};
  const dgDofContainer* getfsbbq() {return _fsbbq;};
  const dgDofContainer* getfdtcn() {return _fdtcn;};
  const dgDofContainer* getqcmif() {return _qcmif;};
  const dgDofContainer* getqldif() {return _qldif;};
  const dgDofContainer* getTsu() {return _tempSu;};
  const dgDofContainer* getPrecip() {return _precip;};
  const dgDofContainer* getTi() {return _ti;};
  const dgDofContainer* getSi() {return _si;};
  const dgDofContainer* getEti() {return _eti;};
  const dgDofContainer* getArea() {return _area;};
  const dgDofContainer* getAlb() {return _alb;};
  const dgDofContainer* getTs() {return _ts;};
  const dgDofContainer* getEs() {return _es;};
  const dgDofContainer* getEi() {return _ei;};
  const dgDofContainer* getSmvi() {return _smvi;};
  const dgDofContainer* getOQns() {return _oQns;}
  const dgDofContainer* getOQsr() {return _oQsr;}
};

#endif
#endif

