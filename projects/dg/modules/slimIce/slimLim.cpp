#include "slimLim.h"
#include "fullMatrix.h"
#include "dgGroupOfElements.h"
#include "dgDofContainer.h"
#include "SPoint3.h"
#include "MElement.h"
#include "function.h"
#include <iostream>
#include <fstream>
#include <string>

#ifdef HAVE_LIM

//Link with the fortran code of LIM3
extern "C" {
     extern double __par_slim_MOD_tn[];
     extern double __par_slim_MOD_sn[];
     extern double __par_slim_MOD_ssu_m[];
     extern double __par_slim_MOD_ssv_m[];
     extern double __par_slim_MOD_sss_m[];
     extern double __par_slim_MOD_sst_m[];
     extern double __ice_MOD_t_su[];
     void __sbcice_lim_MOD_sbc_ice_lim(int*);
     void __iceini_MOD_ice_init();
     extern double __dom_ice_MOD_tmask[];
     extern double __dom_ice_MOD_area[];
     //sorties
     extern double __ice_MOD_a_i[];
     extern double __ice_MOD_at_i[];
     extern double __ice_MOD_v_i[];
     extern double __ice_MOD_vt_i[];
     extern double __ice_MOD_v_s[];
     extern double __ice_MOD_ht_i[];
     extern double __ice_MOD_ht_s[];
     extern double __ice_MOD_fbif[];
     extern double __ice_MOD_diag_bot_me[];
     extern double __ice_MOD_diag_sur_me[];
     extern double __ice_MOD_diag_bot_gr[];
     extern double __ice_MOD_diag_lat_gr[];
     extern double __ice_MOD_fsbbq[];
     extern double __ice_MOD_fdtcn[];
     extern double __ice_MOD_qldif[];
     extern double __ice_MOD_qcmif[];
     extern double __par_slim_MOD_sprecip[];
     extern double __ice_MOD_t_su[];
     extern double __ice_MOD_t_i[];
     extern double __ice_MOD_sm_i[];
     extern double __ice_MOD_et_i[];
     extern double __ice_MOD_t_s[];
     extern double __oceanvar_MOD_utaui_ice[];
     extern double __oceanvar_MOD_vtaui_ice[];
     extern double __ice_MOD_e_s[];
     extern double __ice_MOD_e_i[];
     extern double __ice_MOD_smv_i[];
     //atm
     extern double __oceanvar_MOD_windstressu[];
     extern double __oceanvar_MOD_windstressv[];
     extern double __oceanvar_MOD_windmod[];
     extern double __oceanvar_MOD_cloudcover[];
     extern double __oceanvar_MOD_airtempten[];
     extern double __oceanvar_MOD_totprec[];
     extern double __oceanvar_MOD_humspec[];
     //day
     extern int __daymod_MOD_ndastp;
     //position(lat,lon)
     extern double __dom_ice_MOD_gphit[];
     extern double __dom_ice_MOD_glamt[];
     //maillage
     extern double __dom_ice_MOD_e1t_[];
     extern double __dom_ice_MOD_e2t_[];
     //
     extern int __par_slim_MOD_nn_fsbc;
     extern int __in_out_manager_MOD_ln_rstart;
     //flux oceans
     extern double __par_slim_MOD_emp[];
     extern double __par_slim_MOD_emps[];
     extern double __oceanvar_MOD_qsr_ice[];
     extern double __par_slim_MOD_qns[];
     extern double __par_slim_MOD_qsr[];
     extern double __ice_MOD_fr_i[];
     extern double __oceanvar_MOD_alb_ice[];
}

slimLIM::slimLIM(dgGroupCollection *groups2d, int nbIceClasses, const function* coord)
{
  _nbIceClasses = nbIceClasses;
  _coord = coord;
  _groups = groups2d;
  
  // create dof mapping
  int iLIM = 0;
  for ( int iG = 0; iG < _groups->getNbElementGroups(); iG++ ) {
    dgGroupOfElements* group = _groups->getElementGroup(iG);
    std::vector< std::vector<int> > elemVec( group->getNbElements(), std::vector<int>(group->getNbNodes(),-1));
    for ( size_t iE = 0; iE < group->getNbElements(); iE++ )
      for ( int iN = 0; iN < group->getNbNodes(); iN++ ) {
        elemVec[iE][iN] = iLIM;
        iLIM++;
      }
    _mapDofToLIM.push_back(elemVec);
  }
  //creation dof
  _iConcentration = new dgDofContainer(*_groups, _nbIceClasses);
  _iVolume = new dgDofContainer(*_groups, _nbIceClasses);
  _iThick = new dgDofContainer(*_groups, _nbIceClasses);
  _sVolume = new dgDofContainer(*_groups, _nbIceClasses);
  _sThick = new dgDofContainer(*_groups, _nbIceClasses);
  _fluxHeat = new dgDofContainer(*_groups, 1);
  _fluxBM = new dgDofContainer(*_groups, 1);
  _fluxSM = new dgDofContainer(*_groups, 1);
  _fluxLG = new dgDofContainer(*_groups, 1);
  _fluxBG = new dgDofContainer(*_groups, 1);
  _fsbbq = new dgDofContainer(*_groups, 1);
  _fdtcn = new dgDofContainer(*_groups, 1);
  _qcmif = new dgDofContainer(*_groups, 1);
  _qldif = new dgDofContainer(*_groups, 1);
  _precip = new dgDofContainer(*_groups, 1);
  _tempSu = new dgDofContainer(*_groups, _nbIceClasses);
  _ti = new dgDofContainer(*_groups, _nbIceClasses*_nbIceClasses);
  _si = new dgDofContainer(*_groups, _nbIceClasses);
  //_oSalMod = new dgDofContainer(_groups,1);
  _oTempMod = new dgDofContainer(*_groups,_nbIceClasses);
  _oQns = new dgDofContainer(*_groups,1);
  _oQsr = new dgDofContainer(*_groups,1);
  _oEmp = new dgDofContainer(*_groups,1);
  _oEmps = new dgDofContainer(*_groups,1);
  _iConT = new dgDofContainer(*_groups,1);
  _iVolT = new dgDofContainer(*_groups,1);
  _eti = new dgDofContainer(*_groups,1);
  _area = new dgDofContainer(*_groups,1);
  _alb = new dgDofContainer(*_groups,1);
  _ts = new dgDofContainer(*_groups,1);
  _ei = new dgDofContainer(*_groups,_nbIceClasses*_nbIceClasses);
  _es = new dgDofContainer(*_groups,_nbIceClasses);
  _smvi = new dgDofContainer(*_groups,_nbIceClasses);
  
  importInitFunc();
  
  //inputs
  dataCacheMap cacheMapF(dataCacheMap::NODE_MODE, _groups);
  for (int iG=0; iG<_groups->getNbElementGroups(); iG++){
    dgGroupOfElements *group = _groups->getElementGroup(iG);
    cacheMapF.setGroup(group);
    for (size_t iE =0; iE < group->getNbElements(); iE++){
      cacheMapF.setElement(iE);
      double area = group->elementVolume(cacheMapF.getJacobians(), iE);
      for ( int iN = 0; iN < group->getNbNodes(); iN++ ) {
        int iLIM = _mapDofToLIM[iG][iE][iN];
        __dom_ice_MOD_area[iLIM] = area/group->getNbNodes();
      }
    }
  }
  __par_slim_MOD_nn_fsbc = 1;    //frequency of coMODutation
  //put de data
  _nbLimNodes = _iVolume->getVector().size()/_nbIceClasses;
  _nbLimPoints = _iVolume->getVector().size();
  printf("*************** _nbNodes = %i\n", _nbLimNodes);
  printf("*************** _nbPoints = %i\n", _nbLimPoints);
  for(int i=0; i < _nbLimPoints;i++){
   __dom_ice_MOD_tmask[i]= 1.0;   
  }
  printf("ok domice\n");
}

void slimLIM::update(int dt, int date, const function* cloudCover, const function* windStress, const function* windMod, const function* airTempTen, const function* totPrec, const function* humSpec,const function *oVelocity, const function* oTemperature, const function* oTempKelvin, const function* oSalinity, const function* a_i, const function* v_i, const function* v_s, const function* e_i, const function* e_s, const function* smv_i){
  _date = date;
  _cloudCover = cloudCover;
  _windStress = windStress;
  _windMod = windMod;
  _airTempTen = airTempTen;
  _totPrec = totPrec;
  _humSpec = humSpec;
  _oVelocity = oVelocity;
  _oTemperature = oTemperature;
  _oTempKelvin = oTempKelvin;
  _oSalinity = oSalinity;
  _a_i = a_i;
  _v_i = v_i;
  _v_s = v_s;
  _e_i = e_i;
  _e_s = e_s;
  _smv_i = smv_i;
  
  //day
  __daymod_MOD_ndastp = _date; 
  
  //give the data to LIM
  copyToLIM();
  
  //call function in fortran
  __sbcice_lim_MOD_sbc_ice_lim(&dt);
  
  //take the outputs from LIM
  copyFromLIM();
}

//link function_datacachemap
void slimLIM::copyToLIM(){
   dataCacheMap cacheMap(dataCacheMap::NODE_MODE, _groups);
   dataCacheDouble* fCloudCover = cacheMap.get(_cloudCover);
   dataCacheDouble* fAirTempTen = cacheMap.get(_airTempTen);
   dataCacheDouble* fWindStress = cacheMap.get(_windStress);
   dataCacheDouble* fWindMod = cacheMap.get(_windMod);
   dataCacheDouble* fHumSpec = cacheMap.get(_humSpec);
   dataCacheDouble* fTotPrec = cacheMap.get(_totPrec);
   dataCacheDouble* fOVel = cacheMap.get(_oVelocity);
   dataCacheDouble* fOTemp = cacheMap.get(_oTemperature);
   dataCacheDouble* fOSal = cacheMap.get(_oSalinity);
   dataCacheDouble* fa_i = cacheMap.get(_a_i);
   dataCacheDouble* fv_i = cacheMap.get(_v_i);
   dataCacheDouble* fv_s = cacheMap.get(_v_s);
   dataCacheDouble* fe_i = cacheMap.get(_e_i);
   dataCacheDouble* fe_s = cacheMap.get(_e_s);
   dataCacheDouble* fsmv_i = cacheMap.get(_smv_i);
  for (int iG=0; iG<_groups->getNbElementGroups(); iG++){
    dgGroupOfElements *group = _groups->getElementGroup(iG);
    cacheMap.setGroup(group);
    for (size_t iE =0; iE < group->getNbElements(); iE++){
      cacheMap.setElement(iE);
      for ( int iN = 0; iN < group->getNbNodes(); iN++ ) {
        int iLIM = _mapDofToLIM[iG][iE][iN];
        __oceanvar_MOD_cloudcover[iLIM] = fCloudCover->get()(iN,0);
        __oceanvar_MOD_airtempten[iLIM] = fAirTempTen->get()(iN,0);
        __oceanvar_MOD_windstressu[iLIM] = fWindStress->get()(iN,0);
        __oceanvar_MOD_windstressv[iLIM] = fWindStress->get()(iN,1);
        __oceanvar_MOD_windmod[iLIM] = fWindMod->get()(iN,0);
        __oceanvar_MOD_humspec[iLIM] = fHumSpec->get()(iN,0);
        __oceanvar_MOD_totprec[iLIM] = fTotPrec->get()(iN,0);
        __par_slim_MOD_ssu_m[iLIM] = fOVel->get()(iN,0);
        __par_slim_MOD_ssv_m[iLIM] = fOVel->get()(iN,1);
        __par_slim_MOD_sss_m[iLIM] = fOSal->get()(iN,0);
        __par_slim_MOD_sst_m[iLIM] = fOTemp->get()(iN,0);
        __par_slim_MOD_tn[iLIM] = fOTemp->get()(iN,0);
        __par_slim_MOD_sn[iLIM] = fOSal->get()(iN,0);
        for (int iIC = 0; iIC < _nbIceClasses; iIC++) {
          __ice_MOD_a_i[iLIM+iIC*_nbLimNodes] = fa_i->get()(iN,iIC);
          __ice_MOD_v_i[iLIM+iIC*_nbLimNodes] = fv_i->get()(iN,iIC);
          __ice_MOD_v_s[iLIM+iIC*_nbLimNodes]= fv_s->get()(iN,iIC);
          __ice_MOD_smv_i[iLIM+iIC*_nbLimNodes] = fsmv_i->get()(iN,iIC);
          __ice_MOD_e_s[iLIM+iIC*_nbLimNodes] = fe_s->get()(iN,iIC);
          for (int iICD = 0; iICD < _nbIceClasses; iICD++){
            __ice_MOD_e_i[iLIM+(iICD+iIC*_nbIceClasses)*_nbLimNodes] = fe_i->get()(iN,iICD+iIC*_nbIceClasses);
          };
       };
      }
    }
  }
}


void slimLIM::importInitFunc(){
  dataCacheMap cacheMap(dataCacheMap::NODE_MODE, _groups);
  dataCacheDouble* fCoord = cacheMap.get(_coord);
  for (int iG=0; iG<_groups->getNbElementGroups(); iG++){
    dgGroupOfElements *group = _groups->getElementGroup(iG);
    cacheMap.setGroup(group);
    for (size_t iE =0; iE < group->getNbElements(); iE++){
      cacheMap.setElement(iE);
      for ( int iN = 0; iN < group->getNbNodes(); iN++ ) {
        int iLIM = _mapDofToLIM[iG][iE][iN];
        __dom_ice_MOD_gphit[iLIM] = fCoord->get()(iN,0);
        __dom_ice_MOD_glamt[iLIM] = fCoord->get()(iN,1);
      }
    }
  }
}


void slimLIM::copyFromLIM(){
   for (int iG = 0; iG < _groups->getNbElementGroups(); iG++) {
    dgGroupOfElements *group = _groups->getElementGroup(iG);
    int nbNodes = group->getNbNodes();
    dgFullMatrix<double> &gDiConc = _iConcentration->getGroupProxy(iG);
    dgFullMatrix<double> &gDiThick = _iThick->getGroupProxy(iG);
    dgFullMatrix<double> &gDiVol = _iVolume->getGroupProxy(iG);
    dgFullMatrix<double> &gDiVolT = _iVolT->getGroupProxy(iG);
    dgFullMatrix<double> &gDsVol = _sVolume->getGroupProxy(iG);
    dgFullMatrix<double> &gDsThick = _sThick->getGroupProxy(iG);
    dgFullMatrix<double> &gDoTemp = _oTempMod->getGroupProxy(iG);
//     dgFullMatrix<double> &gDoSal = _oSalMod->getGroupProxy(iG);
     dgFullMatrix<double> &gDoQns = _oQns->getGroupProxy(iG);
     dgFullMatrix<double> &gDoQsr = _oQsr->getGroupProxy(iG);
     dgFullMatrix<double> &gDoEmp = _oEmp->getGroupProxy(iG);
     dgFullMatrix<double> &gDoEmps = _oEmps->getGroupProxy(iG);
     dgFullMatrix<double> &gDiConT = _iConT->getGroupProxy(iG);
     dgFullMatrix<double> &gDfHeat = _fluxHeat->getGroupProxy(iG);
     dgFullMatrix<double> &gDfBG = _fluxBG->getGroupProxy(iG);
     dgFullMatrix<double> &gDfLG = _fluxLG->getGroupProxy(iG);
     dgFullMatrix<double> &gDfSM = _fluxSM->getGroupProxy(iG);
     dgFullMatrix<double> &gDfBM = _fluxBM->getGroupProxy(iG);
     dgFullMatrix<double> &gDfsbbq = _fsbbq->getGroupProxy(iG);
     dgFullMatrix<double> &gDfdtcn = _fdtcn->getGroupProxy(iG);
     dgFullMatrix<double> &gDqcmif = _qcmif->getGroupProxy(iG);
     dgFullMatrix<double> &gDqldif = _qldif->getGroupProxy(iG);
     dgFullMatrix<double> &gDtsu = _tempSu->getGroupProxy(iG);
     dgFullMatrix<double> &gDprecip = _precip->getGroupProxy(iG);
     dgFullMatrix<double> &gDti = _ti->getGroupProxy(iG);
     dgFullMatrix<double> &gDsi = _si->getGroupProxy(iG);
     dgFullMatrix<double> &gDeti = _eti->getGroupProxy(iG);
     dgFullMatrix<double> &gDarea = _area->getGroupProxy(iG);
     dgFullMatrix<double> &gDalb = _alb->getGroupProxy(iG);
     dgFullMatrix<double> &gDts = _ts->getGroupProxy(iG);
     dgFullMatrix<double> &gDEi = _ei->getGroupProxy(iG);
     dgFullMatrix<double> &gDEs = _es->getGroupProxy(iG);
     dgFullMatrix<double> &gDsmvi = _smvi->getGroupProxy(iG);
    for (size_t iE =0; iE < group->getNbElements(); iE++) {
      gDiConc.getBlockProxy(iE,MiConc);
      gDiThick.getBlockProxy(iE,MiThick);
      gDiVol.getBlockProxy(iE,MiVol);
      gDsVol.getBlockProxy(iE,MsVol);
      gDiVolT.getBlockProxy(iE,MiVolT);
      gDsThick.getBlockProxy(iE,MsThick);
      gDoTemp.getBlockProxy(iE,MoTempM);
//       gDoSal.getBlockProxy(iE,MoSalM);
      gDoQns.getBlockProxy(iE,MoQns);
      gDoQsr.getBlockProxy(iE,MoQsr);
      gDoEmp.getBlockProxy(iE,MoEmp);
      gDoEmps.getBlockProxy(iE,MoEmps);
      gDiConT.getBlockProxy(iE,MiConT);
      gDfHeat.getBlockProxy(iE,MiHeatF);
      gDfBG.getBlockProxy(iE,MiBG);
      gDfLG.getBlockProxy(iE,MiLG);
      gDfBM.getBlockProxy(iE,MiBM);
      gDfSM.getBlockProxy(iE,MiSM);
      gDfsbbq.getBlockProxy(iE,Mfsbbq);
      gDfdtcn.getBlockProxy(iE,Mfdtcn);
      gDqcmif.getBlockProxy(iE,Mqcmif);
      gDqldif.getBlockProxy(iE,Mqldif);
      gDprecip.getBlockProxy(iE,Mprecip);
      gDtsu.getBlockProxy(iE,MTsu);
      gDti.getBlockProxy(iE,Mti);
      gDsi.getBlockProxy(iE,Msi);
      gDeti.getBlockProxy(iE,Meti);
      gDarea.getBlockProxy(iE,Marea);
      gDalb.getBlockProxy(iE,Malb);
      gDts.getBlockProxy(iE,Mts);
      gDEi.getBlockProxy(iE,Mei);
      gDEs.getBlockProxy(iE,Mes);
      gDsmvi.getBlockProxy(iE,Msmvi);
      for (int iN = 0; iN < nbNodes; iN++) {
        // here you need to fetch the data from lim3
        // since the ice variables are continuous, you can use the mesh vertex numbering
        // to identify the nodes in the 2D mesh
        int iLIM = _mapDofToLIM[iG][iE][iN];
        // here you need to put your lim array on with the correct index!
        //MoSalM(iN,0)=__ice_MOD_fr_i[iLIM];//__par_slim_MOD_sss_m[iLIM]; // this is for a scalar field only!
        MoQns(iN,0)=__par_slim_MOD_qns[iLIM];
        MoQsr(iN,0)=__par_slim_MOD_qsr[iLIM];
        MoEmp(iN,0)=__par_slim_MOD_emp[iLIM];
        MoEmps(iN,0)=__par_slim_MOD_emps[iLIM];
        MiConT(iN,0)= __ice_MOD_at_i[iLIM];
        MiVolT(iN,0)=__ice_MOD_vt_i[iLIM];
        MiHeatF(iN,0)=__ice_MOD_fbif[iLIM];
        MiBG(iN,0)=__ice_MOD_diag_bot_gr[iLIM];
        MiLG(iN,0)=__ice_MOD_diag_lat_gr[iLIM];
        MiBM(iN,0)=__ice_MOD_diag_bot_me[iLIM];
        MiSM(iN,0)=__ice_MOD_diag_sur_me[iLIM];
        Mfsbbq(iN,0)=__ice_MOD_fsbbq[iLIM];
        Mfdtcn(iN,0)=__ice_MOD_fdtcn[iLIM];
        Mqcmif(iN,0)=__ice_MOD_qcmif[iLIM];
        Mqldif(iN,0)=__ice_MOD_qldif[iLIM];
        Meti(iN,0)=__ice_MOD_et_i[iLIM];
        Marea(iN,0)=__dom_ice_MOD_area[iLIM];
        Mprecip(iN,0)=__par_slim_MOD_sprecip[iLIM];
        Malb(iN,0) = __oceanvar_MOD_alb_ice[iLIM];
        Mts(iN,0) = __ice_MOD_t_s[iLIM];
//         MiConT.print("MiConc");
//         printf("iLIM 0 : %i __ice_MOD_at_i[iLIM] = %e\n",_mapDofToLIM[iG][iE][iN], __ice_MOD_at_i[iLIM]); 
        // for fields with ice classes you need to loop over them too
        for (int iIC = 0; iIC < _nbIceClasses; iIC++) {
          // _iConcentration is a dofcontainer with nbIceClasses fields
          // on the right you need to compute the correct index in the LIM array using vertexNum and iIC
          MoTempM(iN,iIC)=__ice_MOD_t_su[iLIM+iIC*_nbLimNodes];
          MiConc(iN,iIC) = __ice_MOD_a_i[iLIM+iIC*_nbLimNodes];
          MiVol(iN,iIC)=__ice_MOD_v_i[iLIM+iIC*_nbLimNodes];
          MiThick(iN,iIC) = __ice_MOD_ht_i[iLIM+iIC*_nbLimNodes];
          MsVol(iN,iIC)=__ice_MOD_v_s[iLIM+iIC*_nbLimNodes];
          MsThick(iN,iIC) = __ice_MOD_ht_s[iLIM+iIC*_nbLimNodes];
          MTsu(iN,iIC) = __ice_MOD_t_su[iLIM+iIC*_nbLimNodes];
          Msi(iN,iIC) = __ice_MOD_sm_i[iLIM+iIC*_nbLimNodes];
          Msmvi(iN,iIC) = __ice_MOD_smv_i[iLIM+iIC*_nbLimNodes];
          Mes(iN,iIC) = __ice_MOD_e_s[iLIM+iIC*_nbLimNodes];
          for (int iICD = 0; iICD < _nbIceClasses; iICD++){
            Mti(iN,iICD+iIC*_nbIceClasses) = __ice_MOD_t_i[iLIM+(iICD+iIC*_nbIceClasses)*_nbLimNodes];
            Mei(iN,iICD+iIC*_nbIceClasses) = __ice_MOD_e_i[iLIM+(iICD+iIC*_nbIceClasses)*_nbLimNodes];
          };
        }  
      }
    }
  }
}

slimLIM::~slimLIM(){
  if ( _iConcentration)   delete _iConcentration;
  if ( _iVolume )         delete _iVolume;
  if ( _iVolT)            delete _iVolT;
  if ( _oTempMod )        delete _oTempMod;
   if ( _iThick )          delete _iThick;
   if ( _sThick)           delete _sThick;
   if ( _sVolume)          delete _sVolume;
   if ( _oEmp)             delete _oEmp;
   if ( _oEmps)            delete _oEmps;
   if ( _oQns)             delete _oQns;
   if ( _oQsr)             delete _oQsr;
   if ( _iConT)            delete _iConT;
   if ( _fluxHeat)         delete _fluxHeat;
   if ( _precip)           delete _precip;
   if ( _tempSu)           delete _tempSu;
   if ( _ti)               delete _ti;
   if ( _si)               delete _si;
   if (_area)              delete _area;
   if(_alb)                delete _alb;
   if (_ts)                delete _ts;
   if (_ei)                delete _ei;
   if (_es)                delete _es;
   if (_smvi)              delete _smvi;
}


#endif
