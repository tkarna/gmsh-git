%module slimIce
%{
  #undef HAVE_DLOPEN
  #include "slimLim.h"
  #include "slimIceDyn.h"
  #include "slimIceDynSteady.h"
%}

%import(module="dgpy.dgCommon") "dgCommon.i"
%import(module="dgpy.dgFunction") "function.h";
%import(module="dgpy.conservationLaw") "dgConservationLaw.h";
%import(module="dgpy.conservationLaw") "dgConservationLawFunction.h";

%rename (_print) print;
%include "slimLim.h"
%include "slimIceDyn.h"
%include "slimIceDynSteady.h"
