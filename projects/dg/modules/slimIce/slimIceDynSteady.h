#define _SLIM_ICE_DYN_STEADY_
#include "dgConservationLawFunction.h"

class slimIceDynSteady : public dgConservationLawFunction {
  class source;
  class viscousTerm;
  class interfacePsiTerm;
  const function * _fzero;
  const function *_gradEta, *_iceMass, *_iceCon, *_iceThick, *_oceanVel, *_atmVel, *_coriolisFactor;
  public:
  void setup();
  slimIceDynSteady(const function* gradEta, const function* iceMass, const function* iceCon, const function *iceThick, const function *oceanVel, const function * atmVel, const function* coriolisFactor);
  ~slimIceDynSteady();
};

