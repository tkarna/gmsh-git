#include "slimIceDynSteady.h"
#include "dgGroupOfElements.h"
#include "functionGeneric.h"
#include "float.h"
#include "function.h"
#include "MElement.h"


class slimIceDynSteady :: source : public function {
 fullMatrix<double> sol, solGradient, _coriolisFactor, _atmVel, _oceanVel, _iceMass, _iceCon, _gradEta;
public:
    source(const function *atmVel, const function *oceanVel, const function *gradEta, const function *iceMass, const function *iceCon, const function *coriolisFactor):function(2){
    setArgument(sol,function::getSolution());
    setArgument(_coriolisFactor,coriolisFactor);
    setArgument(_atmVel, atmVel);
    setArgument(_oceanVel, oceanVel);
    setArgument(_iceMass, iceMass);
    setArgument(_iceCon, iceCon);
    setArgument(_gradEta, gradEta);
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      double g = 9.81;
      int nQP =val.size1();
      double rhoA = 1.3, rhoO = 1000;
      double cA = 1.2e-3, cO = 5.5e-3;
      int thetaA = 0, thetaO = 0;
      for(int i=0; i< nQP; i++) {
        double u = sol(i,0);
        double v = sol(i,1);
        double tauA_x = rhoA*cA*sqrt(_atmVel(i,0)*_atmVel(i,0)+_atmVel(i,1)*_atmVel(i,1))*(cos(thetaA)*_atmVel(i,0)-sin(thetaA)*_atmVel(i,1));
        double tauA_y = rhoA*cA*sqrt(_atmVel(i,0)*_atmVel(i,0)+_atmVel(i,1)*_atmVel(i,1))*(cos(thetaA)*_atmVel(i,1)+sin(thetaA)*_atmVel(i,0));
        double tauO_x = rhoO*cO*(cos(thetaO)*(_oceanVel(i,0)-u)-sin(thetaO)*(_oceanVel(i,1)-v)); //*sqrt((_oceanVel(i,0)-u)*(_oceanVel(i,0)-u)+(_oceanVel(i,1)-v)*(_oceanVel(i,1)-v))
        double tauO_y = rhoO*cO*(cos(thetaO)*(_oceanVel(i,1)-v)+sin(thetaO)*(_oceanVel(i,0)-u));
        val(i,0)=_iceCon(i,0)*(tauA_x+tauO_x)+_iceMass(i,0)*(_coriolisFactor(i,0)*v-g*_gradEta(i,0));
        val(i,1)=_iceCon(i,0)*(tauA_y+tauO_y)+_iceMass(i,0)*(-_coriolisFactor(i,0)*u-g*_gradEta(i,1));
    }
  }
};

class slimIceDynSteady :: viscousTerm : public function {
    fullMatrix <double> _solgrad,_iceMass,_iceThick, _iceCon;
  public:
    viscousTerm(const function *iceMass, const function *iceThick, const function *iceCon): function (6) {
      setArgument(_solgrad,function::getSolutionGradient());
      setArgument(_iceMass,iceMass);
      setArgument(_iceThick,iceThick);
      setArgument(_iceCon,iceCon);
    };
    void call (dataCacheMap *m, fullMatrix<double> &val) {
      int nQP = val.size1();
      double P,xsi, eta,dudx,dudy,dvdx,dvdy;
      double sigma[2][2];
      double e = 2, pstar = 5000, c = 20;
      for(int i=0; i< nQP; i++) {
        dudx = _solgrad(i,0);
        dvdx = _solgrad(i,1);
        dudy = _solgrad(i,2);
        dvdy = _solgrad(i,3);
        P = _iceThick(i,0)*_iceMass(i,0)*pstar*exp(-c*(1-_iceCon(i,0)));
        xsi =2.5e0*P;
        eta = xsi/(e*e);
        sigma[0][0]=(eta+xsi)*dudx+(xsi-eta)*dvdy;//-P/2;
        sigma[0][1]=eta*(dudy+dvdx);
        sigma[1][0]=eta*(dudy+dvdx);
        sigma[1][1]=(eta+xsi)*dvdy+(xsi-eta)*dudx;//-P/2;
        val(i,0) = - sigma[0][0];
        val(i,1) = - sigma[0][1];
        val(i,2) = - sigma[1][0];
        val(i,3) = - sigma[1][1];
        val(i,4) = 0;
        val(i,5) = 0;       
      }
    }
};

class slimIceDynSteady :: interfacePsiTerm : public function {
  fullMatrix <double> _solgrad,_iceMass,_iceThick, _iceCon, normals;
  public:
    interfacePsiTerm(const function *iceMass, const function *iceThick, const function *iceCon): function (2) {
      setArgument(_solgrad,function::getSolutionGradient());
      setArgument(_iceMass,iceMass);
      setArgument(_iceThick,iceThick);
      setArgument(_iceCon,iceCon);
      setArgument(normals,function::getNormals(), 0);
    };
    void call (dataCacheMap *m, fullMatrix<double> &val) {
      int nQP = val.size1();
      double P,xsi, eta,dudx,dudy,dvdx,dvdy;
      double sigma[2][2];
      double e = 2, pstar = 5000, c = 20;
      for(int i=0; i< nQP; i++) {
        double nx = normals(i,0);
        double ny = normals(i,1);
        dudx = _solgrad(i,0);
        dvdx = _solgrad(i,1);
        dudy = _solgrad(i,2);
        dvdy = _solgrad(i,3);
        P = _iceThick(i,0)*_iceMass(i,0)*pstar*exp(-c*(1-_iceCon(i,0)));
        xsi =  2.5e0*P;
        eta = xsi/(e*e);
        sigma[0][0]=(eta+xsi)*dudx+(xsi-eta)*dvdy;//-P/2;
        sigma[0][1]=eta*(dudy+dvdx);
        sigma[1][0]=eta*(dudy+dvdx);
        sigma[1][1]=(eta+xsi)*dvdy+(xsi-eta)*dudx;//-P/2;
        val(i,0)=(nx*sigma[0][0]+ny*sigma[1][0]);
        val(i,1)=(nx*sigma[0][1]+ny*sigma[1][1]);
      }
    }
};

void slimIceDynSteady::setup(){
  _volumeTerm0[""] = new source(_atmVel, _oceanVel, _gradEta, _iceMass, _iceCon, _coriolisFactor);
  _volumeTerm1[""] = new viscousTerm(_iceMass, _iceThick, _iceCon);
  _interfaceTerm0[""] = new interfacePsiTerm(_iceMass, _iceThick, _iceCon);
}

slimIceDynSteady::slimIceDynSteady( const function *gradEta, const function *iceMass, const function *iceCon, const function *iceThick, const function *oceanVel, const function *atmVel, const function* coriolisFactor):dgConservationLawFunction(2){
 _coriolisFactor = _fzero;
 _gradEta = gradEta;
 _iceMass = iceMass;
 _iceCon = iceCon;
 _iceThick = iceThick;
 _oceanVel = oceanVel;
 _atmVel = atmVel;
 _coriolisFactor = coriolisFactor;
}

slimIceDynSteady::~slimIceDynSteady(){
  if (_volumeTerm0[""]) delete _volumeTerm0[""];
  if (_volumeTerm1[""]) delete _volumeTerm1[""];
  if (_interfaceTerm0[""]) delete _interfaceTerm0[""];
}
