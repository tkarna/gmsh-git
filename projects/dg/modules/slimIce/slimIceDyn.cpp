#include "slimIceDyn.h"
#include "dgGroupOfElements.h"
#include "functionGeneric.h"
#include "float.h"
#include "function.h"
#include "MElement.h"

class slimIceDyn :: source : public function {
 fullMatrix<double> sol, solGradient, _coriolisFactor, _atmVel, _oceanVel, _iceMass, _iceCon, _gradEta;
public:
    source(const function *atmVel, const function *oceanVel, const function *gradEta, const function *iceMass, const function *iceCon, const function *coriolisFactor):function(2){
    setArgument(sol,function::getSolution());
    setArgument(_coriolisFactor,coriolisFactor);
    setArgument(_atmVel, atmVel);
    setArgument(_oceanVel, oceanVel);
    setArgument(_iceMass, iceMass);
    setArgument(_iceCon, iceCon);
    setArgument(_gradEta, gradEta);
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      double g = 9.81;
      int nQP =val.size1();
      double rhoA = 1.3, rhoO = 1000.0;
      double cA = 1.2e-3, cO = 5.5e-3;
      int thetaA = 0, thetaO = 0;
      double tauA_x, tauA_y, tauO_x, tauO_y;
      for(int i=0; i< nQP; i++) {
        double u = sol(i,0);
        double v = sol(i,1);
        tauA_x = rhoA*cA*sqrt(_atmVel(i,0)*_atmVel(i,0)+_atmVel(i,1)*_atmVel(i,1))*(cos(thetaA)*_atmVel(i,0)-sin(thetaA)*_atmVel(i,1));
        tauA_y = rhoA*cA*sqrt(_atmVel(i,0)*_atmVel(i,0)+_atmVel(i,1)*_atmVel(i,1))*(cos(thetaA)*_atmVel(i,1)+sin(thetaA)*_atmVel(i,0));
        tauO_x = rhoO*cO*(cos(thetaO)*(_oceanVel(i,0)-u)-sin(thetaO)*(_oceanVel(i,1)-v)); //*sqrt((_oceanVel(i,0)-u)*(_oceanVel(i,0)-u)+(_oceanVel(i,1)-v)*(_oceanVel(i,1)-v))
        tauO_y = rhoO*cO*(cos(thetaO)*(_oceanVel(i,1)-v)+sin(thetaO)*(_oceanVel(i,0)-u));
        val(i,0)=_iceCon(i,0)/_iceMass(i,0)*(tauA_x+tauO_x)+(_coriolisFactor(i,0)*v-g*_gradEta(i,0));
        val(i,1)=_iceCon(i,0)/_iceMass(i,0)*(tauA_y+tauO_y)+(-_coriolisFactor(i,0)*u-g*_gradEta(i,1));
    }
  }
};

class slimIceDyn :: viscousTerm : public function {
    fullMatrix <double> _solgrad,_iceMass,_iceThick, _duPrev, _iceCon;
  public:
    viscousTerm(const function *iceMass, const function *iceThick, const function *duPrev, const function *iceCon): function (6) {
      setArgument(_solgrad,function::getSolutionGradient());
      setArgument(_iceMass,iceMass);
      setArgument(_iceThick,iceThick);
      setArgument(_duPrev,duPrev);
      setArgument(_iceCon,iceCon);
    };
    void call (dataCacheMap *m, fullMatrix<double> &val) {
      int nQP = val.size1();
      double P,xsi, eta,delta,dudx,dudy,dvdx,dvdy;
      double sigma[2][2];
      double e = 2, pstar = 5000, c = 20;
      for(int i=0; i< nQP; i++) {
        dudx = _solgrad(i,0);
        dvdx = _solgrad(i,1);
        dudy = _solgrad(i,2);
        dvdy = _solgrad(i,3);
        delta = (_duPrev(i,0)*_duPrev(i,0)+_duPrev(i,3)*_duPrev(i,3))*(1+1/(e*e))+(_duPrev(i,2)+_duPrev(i,1))*(_duPrev(i,2)+_duPrev(i,1))/(e*e)+2*(_duPrev(i,0)+_duPrev(i,3))*(1-1/(e*e));
        P = _iceThick(i,0)*pstar*exp(-c*(1-_iceCon(i,0)));
        xsi =P/(2*std::max(2e-9,sqrt(delta)));
        eta = xsi/(e*e);
        sigma[0][0]=(eta+xsi)*dudx+(xsi-eta)*dvdy;//-P/2;
        sigma[0][1]=eta*(dudy+dvdx);
        sigma[1][0]=eta*(dudy+dvdx);
        sigma[1][1]=(eta+xsi)*dvdy+(xsi-eta)*dudx;//-P/2;
        val(i,0) = - sigma[0][0];
        val(i,1) = - sigma[0][1];
        val(i,2) = - sigma[1][0];
        val(i,3) = - sigma[1][1];
        val(i,4) = 0;
        val(i,5) = 0;       
      }
    }
};

class slimIceDyn :: gradPsiTerm : public function {
  fullMatrix <double> _solgrad,_iceMass,_iceThick, _duPrev, normals, _iceCon;
  public:
    gradPsiTerm(const function *iceMass, const function *iceThick, const function *duPrev, const function *iceCon): function (2) {
      setArgument(_solgrad,function::getSolutionGradient());
      setArgument(_iceMass,iceMass);
      setArgument(_iceThick,iceThick);
      setArgument(_duPrev,duPrev);
      setArgument(normals,function::getNormals(), 0);
      setArgument(_iceCon,iceCon);
    };
    void call (dataCacheMap *m, fullMatrix<double> &val) {
      int nQP = val.size1();
      double P,xsi, eta,delta,dudx,dudy,dvdx,dvdy;
      double sigma[2][2];
      double e = 2, pstar = 5000, c = 20;
      for(int i=0; i< nQP; i++) {
        double nx = normals(i,0);
        double ny = normals(i,1);
        dudx = _solgrad(i,0);
        dvdx = _solgrad(i,1);
        dudy = _solgrad(i,2);
        dvdy = _solgrad(i,3);
        delta = (_duPrev(i,0)*_duPrev(i,0)+_duPrev(i,3)*_duPrev(i,3))*(1+1/(e*e))+(_duPrev(i,2)+_duPrev(i,1))*(_duPrev(i,2)+_duPrev(i,1))/(e*e)+2*(_duPrev(i,0)+_duPrev(i,3))*(1-1/(e*e));
        P = _iceThick(i,0)*pstar*exp(-c*(1-_iceCon(i,0)));
        xsi = P/(2*std::max(2e-9,sqrt(delta)));
        eta = xsi/(e*e);
        sigma[0][0]=(eta+xsi)*dudx+(xsi-eta)*dvdy;//-P/2;
        sigma[0][1]=eta*(dudy+dvdx);
        sigma[1][0]=eta*(dudy+dvdx);
        sigma[1][1]=(eta+xsi)*dvdy+(xsi-eta)*dudx;//-P/2;
        val(i,0)=(nx*sigma[0][0]+ny*sigma[1][0]);
        val(i,1)=(nx*sigma[0][1]+ny*sigma[1][1]);
      }
    }
};

// class slimIceDyn::diffusivityDotN : public function {
//     fullMatrix <double> _solgrad,_iceMass,_iceThick, _duPrev, normals, _ipTerm, _iceCon;
//   public:
//     diffusivityDotN(const function *iceMass, const function *iceThick, const function *duPrev, const function *iceCon): function (2) {
//       setArgument(_solgrad,function::getSolutionGradient());
//       setArgument(_iceMass,iceMass);
//       setArgument(_iceThick,iceThick);
//       setArgument(_duPrev,duPrev);
//       setArgument(normals,function::getNormals(), 0);
//       setArgument(_iceCon,iceCon);
//     };
//     void call (dataCacheMap *m, fullMatrix<double> &val) {
//       int nQP = val.size1();
//       double P,xsi, eta,delta,dudx,dudy,dvdx,dvdy;
//       double sigma[2][2];
//       double sigmaN[2];
//       double e = 2, pstar = 5000, c = 20;
//       for(int i=0; i< nQP; i++) {
//         double nx = normals(i,0);
//         double ny = normals(i,1);
//         dudx = _solgrad(i,0);
//         dvdx = _solgrad(i,1);
//         dudy = _solgrad(i,2);
//         dvdy = _solgrad(i,3);
//         delta = (_duPrev(i,0)*_duPrev(i,0)+_duPrev(i,3)*_duPrev(i,3))*(1+1/(e*e))+(_duPrev(i,2)+_duPrev(i,1))*(_duPrev(i,2)+_duPrev(i,1))/(e*e)+2*(_duPrev(i,0)+_duPrev(i,3))*(1-1/(e*e));
//         P = _iceThick(i,0)*pstar*exp(-c*(1-_iceCon(i,0)));
//         xsi = P/(2*std::max(2e-9,sqrt(delta)));
//         eta = xsi/(e*e);
//         sigma[0][0]=(eta+xsi)*dudx+(xsi-eta)*dvdy;//-P/2;
//         sigma[0][1]=eta*(dudy+dvdx);
//         sigma[1][0]=eta*(dudy+dvdx);
//         sigma[1][1]=(eta+xsi)*dvdy+(xsi-eta)*dudx;//-P/2;
//         sigmaN[0]=nx*sigma[0][0]+ny*sigma[1][0];
//         sigmaN[1]=nx*sigma[0][1]+ny*sigma[1][1];
//         val(i,0) = nx * sigmaN[0] + ny * sigmaN[1];
//         val(i,1) = nx * sigmaN[0] + ny * sigmaN[1];
//       }
//     }
// };

void slimIceDyn::setup(){
  //_diffusivity[""] = new diffusivityDotN(_iceMass, _iceThick, _duPrev,_iceCon);
  //_viscousT = new viscousTerm(_iceMass, _iceThick, _duPrev,_iceCon);
  _ipTerm = NULL; //dgNewIpTerm (2, _viscousT, _diffusivity[""] );
  _volumeTerm0[""] = new source(_atmVel, _oceanVel, _gradEta, _iceMass, _iceCon, _coriolisFactor);
  _volumeTerm1[""] = new viscousTerm(_iceMass, _iceThick, _duPrev, _iceCon);
  _interfaceTerm0[""] = new gradPsiTerm(_iceMass, _iceThick, _duPrev,_iceCon);
}

slimIceDyn::slimIceDyn(const function *duPrev, const function *gradEta, const function *iceMass, const function *iceCon, const function *iceThick, const function *oceanVel, const function *atmVel, const function* coriolisFactor):dgConservationLawFunction(2){
 _coriolisFactor = _fzero;
 _duPrev = duPrev;
 _gradEta = gradEta;
 _iceMass = iceMass;
 _iceCon = iceCon;
 _iceThick = iceThick;
 _oceanVel = oceanVel;
 _atmVel = atmVel;
 _coriolisFactor = coriolisFactor;
}

class slimIceDyn::boundarySlip : public dgBoundaryCondition{
  class term : public function {
    fullMatrix<double> normals, sol;
    public:
    term():function(2){
      setArgument(sol,function::getSolution());
      setArgument(normals,function::getNormals());
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      int nQP = sol.size1();
      for(int i=0; i< nQP; i++) {
        double nx = normals(i,0);
        double ny = normals(i,1);
        double u = sol(i, 0);
        double v = sol(i, 1);
        double ut = u * ny - v * nx;
        val(i, 0) = ut*ny;
        val(i, 1) = -ut*nx;
      }
    }
  };
  term _term;
  public:
  boundarySlip(slimIceDyn *claw) : _term() {
    _term0 = &_term;
  }
};

dgBoundaryCondition *slimIceDyn::newBoundarySlip(){
  return new boundarySlip(this);
}
// class slimIceDyn::boundaryZero : public dgBoundaryCondition {
//   class term : public function {
//     fullMatrix <double> _solgrad,_iceMass,_iceThick, _duPrev, normals;
//   public:
//     term(dgConservationLawFunction *claw,const function *iceMass, const function *iceThick, const function *duPrev):function(2) {
//       setArgument(_solgrad,function::getSolutionGradient());
//       setArgument(_iceMass,iceMass);
//       setArgument(_iceThick,iceThick);
//       setArgument(_duPrev,duPrev);
//       setArgument(normals,function::getNormals(), 0);
//     };
//     void call (dataCacheMap *m, fullMatrix<double> &val) {
//       int nQP = val.size1();
//       double P,xsi, eta,delta,dudx,dudy,dvdx,dvdy;
//       double sigma[2][2];
//       double e = 2, pstar = 5000, c = 20;
//       for(int i=0; i< nQP; i++) {
//         double nx = normals(i,0);
//         double ny = normals(i,1);
//         dudx = _solgrad(i,0);
//         dvdx = _solgrad(i,1);
//         dudy = _solgrad(i,2);
//         dvdy = _solgrad(i,3);
//         delta = (_duPrev(i,0)*_duPrev(i,0)+_duPrev(i,3)*_duPrev(i,3))*(1+1/(e*e))+(_duPrev(i,2)+_duPrev(i,1))*(_duPrev(i,2)+_duPrev(i,1))/(e*e)+2*(_duPrev(i,0)+_duPrev(i,3))*(1-1/(e*e));
//         P = _iceThick(i,0)*_iceMass(i,0)*pstar*exp(-c*(1-_iceThick(i,0)));
//         xsi = 2.5e4*P;// P/(2*std::max(2e-9,sqrt(delta)));
//         eta = xsi/(e*e);
//         sigma[0][0]=(eta+xsi)*dudx+(xsi-eta)*dvdy;//-P/2;
//         sigma[0][1]=eta*(dudy+dvdx);
//         sigma[1][0]=eta*(dudy+dvdx);
//         sigma[1][1]=(eta+xsi)*dvdy+(xsi-eta)*dudx;//-P/2;
//         val(i,0)+=-(nx*sigma[0][0]+ny*sigma[1][0])*nx;
//         val(i,1)+=-(nx*sigma[0][1]+ny*sigma[1][1])*ny;
// //     fullMatrix<double> normals, sol, _interfaceTerm;
// //     functionReplace replaceFunction;
// //     public:
// //     term(dgConservationLawFunction *claw):function(2) {
// //        setArgument(sol,function::getSolution());
// //        setArgument(normals,function::getNormals());
// //        setArgument(_interfaceTerm,claw->getInterfaceTerm0(""));
// //     }
// //     void call(dataCacheMap *m, fullMatrix<double> &val) {
// //       for(int i=0;i<val.size1(); i++) {
// //         double nx = normals(i,0);
// //         double ny = normals(i,1);
// //         double u = sol(i, 0);
// //         double v = sol(i, 1);
// //         double un = u * nx + v * ny;
// //         val(i,0)-= _interfaceTerm(i,0) ; // un * u;
// //         printf("interfaceT %f %f\n",_interfaceTerm(i,0),_interfaceTerm(i,1));
// //         val(i,1)-= 0;// _interfaceTerm(i,1); //un * v;
//       }
//     }
//   };
//   term _term;
//   public:
//   boundaryZero(slimIceDyn *claw,const function *iceMass, const function *iceThick, const function *duPrev) : _term(claw,iceMass, iceThick, duPrev) {
//     _term0 = &_term;
//   }
// };
// 
// dgBoundaryCondition *slimIceDyn::newBoundaryZero(){
//   return new boundaryZero(this, _iceMass, _iceThick, _duPrev);
// }

slimIceDyn::~slimIceDyn(){
  //delete _fzero;
  //delete _ipTerm;
  //delete _viscousT;
  if (_volumeTerm0[""]) delete _volumeTerm0[""];
  if (_volumeTerm1[""]) delete _volumeTerm1[""];
  if (_interfaceTerm0[""]) delete _interfaceTerm0[""];
  //if (_diffusivity[""]) delete (_diffusivity[""]);
}
