#define _SLIM_ICE_DYN_
#include "dgConservationLawFunction.h"

class slimIceDyn : public dgConservationLawFunction {
  class source;
  class viscousTerm;
  class gradPsiTerm;
  class boundarySlip;
  //class diffusivityDotN;
  const function * _fzero;
  const function * _duPrev, *_gradEta, *_iceMass, *_iceCon, *_iceThick, *_oceanVel, *_atmVel, *_coriolisFactor;
  const function  *_ipTerm;
  public:
  void setup();
  slimIceDyn(const function *duPrev, const function* gradEta, const function* iceMass, const function* iceCon, const function *iceThick, const function *oceanVel, const function * atmVel, const function* coriolisFactor);
  ~slimIceDyn();
  /*slip wall boundary */
  dgBoundaryCondition *newBoundarySlip();
};

