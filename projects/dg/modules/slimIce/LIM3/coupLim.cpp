#include <iostream>
#include <fstream>
#include <string>
using namespace std;


extern "C" {
      extern double __par_slim_MOD_ssu_m[];
      extern double __par_slim_MOD_ssv_m[];
      extern double __par_slim_MOD_sss_m[];
      extern double __par_slim_MOD_sst_m[];
      extern double __ice_MOD_t_su[5];
      extern double __ice_MOD_ht_i;
      extern double __ice_MOD_ht_s;
      void __sbcice_lim_MOD_sbc_ice_lim(int*);
      extern double __sbcice_lim_MOD_alb_ice_os;
      extern double __sbcice_lim_MOD_alb_ice_cs;
      extern double __dom_ice_MOD_tmask[];
      extern double __oceanvar_MOD_utaui_ice;
      extern double __par_slim_MOD_tprecip;
      extern double __oceanvar_MOD_fr1_i0;
      extern double __oceanvar_MOD_fr2_i0;
      extern double __oceanvar_MOD_qns_ice;
      extern double __oceanvar_MOD_qsr_ice;
      extern double __oceanvar_MOD_qla_ice;
      extern double __oceanvar_MOD_dqla_ice;
      //atm
      extern double __oceanvar_MOD_windstressu[];
      extern double __oceanvar_MOD_windstressv[];
      extern double __oceanvar_MOD_windmod[];
      extern double __oceanvar_MOD_cloudcover[];
      extern double __oceanvar_MOD_airtempten[];
      extern double __oceanvar_MOD_totprec[];
      extern double __oceanvar_MOD_humspec[];
      //day
      extern int __daymod_MOD_ndastp;
      //position
      extern double __dom_ice_MOD_gphit[];
      //maillage
      extern double __dom_ice_MOD_e1t[];
      extern double __dom_ice_MOD_e2t[];
      // 
      extern int __par_slim_MOD_nn_fsbc;
      //start
      extern bool __in_out_manager_MOD_ln_rstart;
      extern int __daymod_MOD_ndastp;
/*     extern double par_slim_mp_ssu_m_[];
     extern double par_slim_mp_ssv_m_[];
     extern double par_slim_mp_sss_m_[];
     extern double par_slim_mp_sst_m_[];
//      extern double ice_mp_t_su_[5];
     extern double ice_mp_t_su_[];
//      extern double ice_mp_ht_i_[];
//      extern double ice_mp_ht_s_[];
     void sbcice_lim_mp_sbc_ice_lim_(int*);
     extern double sbcice_lim_mp_alb_ice_os_[];
     extern double sbcice_lim_mp_alb_ice_cs_[];
     extern double dom_ice_mp_tmask_[];
     extern double oceanvar_mp_utaui_ice_[];
     extern double par_slim_mp_tprecip_[];
     extern double oceanvar_mp_fr1_i0_[];
     extern double oceanvar_mp_fr2_i0_[];
     extern double oceanvar_mp_qns_ice_[];
     extern double oceanvar_mp_qsr_ice_[];
     extern double oceanvar_mp_qla_ice_[];
     extern double oceanvar_mp_dqla_ice_[];
     //atm
     extern double oceanvar_mp_windstressu_[];
     extern double oceanvar_mp_windstressv_[];
     extern double oceanvar_mp_windmod_[];
     extern double oceanvar_mp_cloudcover_[];
     extern double oceanvar_mp_airtempten_[];
     extern double oceanvar_mp_totprec_[];
     extern double oceanvar_mp_humspec_[];
     //day
     extern int daymod_mp_ndastp_;
     //position
     extern double dom_ice_mp_gphit_[];
     //maillage
     extern double dom_ice_mp_e1t_[];
     extern double dom_ice_mp_e2t_[];
     // 
     extern int par_slim_mp_nn_fsbc_;
     //start
     extern bool in_out_manager_mp_ln_rstart_;*/
}

int main(){ //definitions des donnees
 
  for (int i=0;i<3;i++){
    __par_slim_MOD_ssu_m[i]=0;
    __par_slim_MOD_ssv_m[i]=0;
    __par_slim_MOD_sss_m[i]=25.0;
    __par_slim_MOD_sst_m[i]=253.0-3*i;
    for (int l=0;l<5;l++){
      __ice_MOD_t_su[i+l*3]=253.0-3*i;
      __dom_ice_MOD_tmask[i+l*3]=1.0; //car sur ocean
    }
    //donnees atm
    __oceanvar_MOD_windstressu[i]=0;
    __oceanvar_MOD_windstressv[i]=0;
    __oceanvar_MOD_windmod[i]=0;
    __oceanvar_MOD_cloudcover[i]=0.5;
    __oceanvar_MOD_airtempten[i]=263.2;
    __oceanvar_MOD_totprec[i]=0;
    __oceanvar_MOD_humspec[i]=0.5;
    //position
    __dom_ice_MOD_gphit[i]=86+i;
    //maillage
    __dom_ice_MOD_e1t[i]=1000;
    __dom_ice_MOD_e2t[i]=1000;
  }
   //__ice_MOD_ht_i=1.0;
   //__ice_MOD_ht_s=0.0;

  //day
  __daymod_MOD_ndastp=20000621;
  printf("date YYYYMMDD %d\n",__daymod_MOD_ndastp);

  //
  __par_slim_MOD_nn_fsbc=1;
  //start
  //in_out_manager_MOD_ln_rstart=1;
  
   int a = 1;
  __sbcice_lim_MOD_sbc_ice_lim(&a);
  
  //affichage albedo
//   printf("albedo clear %f\n",sbcice_lim_MOD_alb_ice_cs_);
//   printf("albedo cover %f\n",sbcice_lim_MOD_alb_ice_os_);
// 
//   printf("ice stress surf %f\n",oceanvar_MOD_utaui_ice_);
//   printf("total precip %f\n",par_slim_MOD_tprecip_);
//  
//   printf("fraction qsr 1 %f\n",oceanvar_MOD_fr1_i0_);
//   printf("fraction qsr 2 %f\n",oceanvar_MOD_fr2_i0_);
//   printf("non solar %f\n",oceanvar_MOD_qns_ice_);
//   printf("solar %f\n",oceanvar_MOD_qsr_ice_);
//   printf("latent over ice %f\n",oceanvar_MOD_qla_ice_);
//   printf("latent sensitivity %f\n",oceanvar_MOD_dqla_ice_);
  
  //printf("nn_fsbc %d\n",__par_slim_MOD_nn_fsbc);
  printf("test : %e\n", __ice_MOD_ht_i);
  printf("test : %e\n", __ice_MOD_ht_s);

}

//------------
//test flux---
//------------
// extern "C" {
//      extern double __par_slim_MOD_ssu_m;
//      extern double __par_slim_MOD_ssv_m;
//      extern double __par_slim_MOD_sss_m;
//      extern double __ice_MOD_t_su[5];
//      extern double __ice_MOD_ht_i;
//      extern double __ice_MOD_ht_s;
//      void __sbcice_lim_MOD_sbc_ice_lim(int*);//, int*);
//      extern double __sbcice_lim_MOD_alb_ice_os;
//      extern double __sbcice_lim_MOD_alb_ice_cs;
//      extern double __dom_ice_MOD_tmask;
//      extern double __oceanvar_MOD_utaui_ice;
//      extern double __par_slim_MOD_tprecip;
//      extern double __oceanvar_MOD_fr1_i0;
//      extern double __oceanvar_MOD_fr2_i0;
//      extern double __oceanvar_MOD_qns_ice;
//      extern double __oceanvar_MOD_qsr_ice;
//      extern double __oceanvar_MOD_qla_ice;
//      extern double __oceanvar_MOD_dqla_ice;
//      //atm
//      extern double __oceanvar_MOD_windstressu;
//      extern double __oceanvar_MOD_windstressv;
//      extern double __oceanvar_MOD_windmod;
//      extern double __oceanvar_MOD_cloudcover;
//      extern double __oceanvar_MOD_airtempten;
//      extern double __oceanvar_MOD_totprec;
//      extern double __oceanvar_MOD_humspec;
//      //day
//      extern int __daymod_MOD_nday;
//      extern int __daymod_MOD_nmonth;
//      extern int __daymod_MOD_nyear;
//      extern int __daymod_MOD_nday_year;
//      extern double __sbcblk_clio_MOD_yearday;
//      extern int __daymod_MOD_ndastp;
//      //declinaison solaire
//      //extern double __sbcblk_clio_MOD_zdecl;
//      //position
//      extern double __dom_ice_MOD_gphit;
//      // dummy variables
// //      extern int __sbcblk_clio_MOD_iday;
// //      extern double __sbcblk_clio_MOD_zxday;
// //      extern double __sbcblk_clio_MOD_zcdecl;
// //      extern double __sbcblk_clio_MOD_zdaycor;
// //      extern double __sbcblk_clio_MOD_zdist;
// //      extern double __sbcblk_clio_MOD_zdlha;
// //      extern double __sbcblk_clio_MOD_zps;
// //      extern double __sbcblk_clio_MOD_zlsrise;
// //      extern double __sbcblk_clio_MOD_zlsset;
// }
// 
// int main(){
//  //definitions des donnees
//    __par_slim_MOD_ssu_m=0;
//    __par_slim_MOD_ssv_m=0;
//    __par_slim_MOD_sss_m=25.0; //psu: tres peu car sous la glace donc comme fraiche
//    for (int i=0;i<5;i++){
//      __ice_MOD_t_su[i]=253.0;
//    }
//    __ice_MOD_ht_i=1.0;
//    __ice_MOD_ht_s=0.0;
//   
//   //appel de la fonction
//   __dom_ice_MOD_tmask=1.0; //car sur ocean
//   
//   //donnees atm
//   __oceanvar_MOD_windstressu=0;
//   __oceanvar_MOD_windstressv=0;
//   __oceanvar_MOD_windmod=0;
//   __oceanvar_MOD_cloudcover=0.5;
//   __oceanvar_MOD_airtempten=263.2;
//   __oceanvar_MOD_totprec=0;
//   __oceanvar_MOD_humspec=0.5;
//   
//   //day
//   __daymod_MOD_ndastp=20000621;
//   printf("ndastp avant %d\n",__daymod_MOD_ndastp);
// 
//   //position
//   __dom_ice_MOD_gphit=86;
//   
//    int a = 1;
//   __sbcice_lim_MOD_sbc_ice_lim(&a);
//   
//   //affichage albedo
//   printf("albedo clear %f\n",__sbcice_lim_MOD_alb_ice_cs);
//   printf("albedo cover %f\n",__sbcice_lim_MOD_alb_ice_os);
//   
//   //printf("temperature surf %f\n",__ice_MOD_t_su);
//  
//   printf("ice stress surf %f\n",__oceanvar_MOD_utaui_ice);
//   
//   printf("total precip %f\n",__par_slim_MOD_tprecip);
//   
//   printf("fraction qsr 1 %f\n",__oceanvar_MOD_fr1_i0);
//   
//   printf("fraction qsr 2 %f\n",__oceanvar_MOD_fr2_i0);
//   
//   printf("non solar %f\n",__oceanvar_MOD_qns_ice);
//   
//   printf("solar %f\n",__oceanvar_MOD_qsr_ice);
//   
//   printf("latent over ice %f\n",__oceanvar_MOD_qla_ice);
//   
//   printf("latent sensitivity %f\n",__oceanvar_MOD_dqla_ice);
//   
//   //printf("declinaison solaire %f\n",__sbcblk_clio_MOD_zdecl);
//   
//   printf("yearday %f\n",__sbcblk_clio_MOD_yearday);
//   
//   printf("nyear %d\n",__daymod_MOD_nday_year);
//   
// //   printf("dummy iday %d\n",__sbcblk_clio_MOD_iday);
// //   printf("dummy zxday %f\n",__sbcblk_clio_MOD_zxday);
// //   printf("zc %f\n",__sbcblk_clio_MOD_zcdecl);
// //   printf("zdaycor %f\n",__sbcblk_clio_MOD_zdaycor);
// //   printf("zdist %f\n",__sbcblk_clio_MOD_zdist);
// //   printf("zdla %f\n",__sbcblk_clio_MOD_zdlha);
// //   printf("zps %f\n",__sbcblk_clio_MOD_zps);
// //   printf("zlsrise %f\n",__sbcblk_clio_MOD_zlsrise);
// //   printf("zlsset %f\n",__sbcblk_clio_MOD_zlsset);
// 
//   printf("nday %d\n", __daymod_MOD_nday);
//   printf("nmonth %d\n", __daymod_MOD_nmonth);
//   printf("nyear %d\n", __daymod_MOD_nyear);
// 
// }

// //-------------
// // test albedo-
// //-------------
// extern "C" {
//      extern double __par_slim_MOD_ssu_m;
//      extern double __par_slim_MOD_ssv_m;
//      extern double __par_slim_MOD_sss_m;
//      extern double __ice_MOD_t_su;
//      extern double __ice_MOD_ht_i;
//      extern double __ice_MOD_ht_s;
//      void __sbcice_lim_MOD_sbc_ice_lim();
//      extern double __sbcice_lim_MOD_alb_ice_os;
//      extern double __sbcice_lim_MOD_alb_ice_cs;
// }
// 
// int main(){
//  //definitions des donnees
//    __par_slim_MOD_ssu_m=0;
//    __par_slim_MOD_ssv_m=0;
//    __par_slim_MOD_sss_m=25.0; //psu: tres peu car sous la glace donc comme fraiche
//    __ice_MOD_t_su=253.0;
//    __ice_MOD_ht_i=1.0;
//    __ice_MOD_ht_s=0.0;
//   
//   //appel de la fonction
//   __sbcice_lim_MOD_sbc_ice_lim();
//   
//   //affichage albedo
//   printf("%f\n",__sbcice_lim_MOD_alb_ice_cs);
//   printf("%f\n",__sbcice_lim_MOD_alb_ice_os);
// }

//---------------------
//fonction avec entree-
//---------------------
// extern "C" {
//      extern double __par_slim_MOD_ssu_m;
//      extern double __par_slim_MOD_ssv_m;
//      extern double __par_slim_MOD_sss_m;
//      extern double __ice_MOD_t_su;
//      extern double __ice_MOD_ht_i;
//      extern double __ice_MOD_ht_s;
//      void __sbcice_lim_MOD_sbc_ice_lim(int*);
//      extern double __sbcice_lim_MOD_alb_ice_os;
//      extern double __sbcice_lim_MOD_alb_ice_cs;
// }
// 
// int main(){
//  //definitions des donnees
//    __par_slim_MOD_ssu_m=0;
//    __par_slim_MOD_ssv_m=0;
//    __par_slim_MOD_sss_m=25.0; 
//    __ice_MOD_t_su=253.0;
//    __ice_MOD_ht_i=1.0;
//    __ice_MOD_ht_s=0.0;
//   
//   //appel de la fonction
//    int a = 1;
//   __sbcice_lim_MOD_sbc_ice_lim(&a);
//   
//   //affichage albedo
//   printf("%f\n",__sbcice_lim_MOD_alb_ice_cs);
//   printf("%f\n",__sbcice_lim_MOD_alb_ice_os);
//}
