#include "slim3dSolver.h"
#include "functionGeneric.h"
#include "dgExtrusion.h"

slim3dSolver::slim3dSolver(const std::string meshFile3d, std::vector<std::string> bottomBndTags, std::vector<std::string> topBndTags) {
  
  _solve2d = _solveW = true;
  _solveUV = _solveUVDiff = true;
  _solveS = _solveSDiff = false;
  _solveT = _solveTDiff = false;
  _solveSed = _solveSedDiff = false;
  _solveTurbulence = false;
  _advectTKE = true;
  _haveMovingMesh = true;
  _bottomFriction = true;
  _initialized = false;
  _useUVLimiter = _useSLimiter = _useTLimiter = _useSedLimiter = true;
  _useModeSplit = true;
  useNewRGrad = false;
  _useConservativeALE = false;
  _useOptimalLimiter = false;
  _useGMVel = false;
  _useIsoDiff = false;
  _useStereo = false;
  _computeUVGrad = false;

  wSolver = rSolver = NULL;
  newRGradSolver = NULL;
  //depthIntSolver = rGradIntSolver = NULL;
  newDepthIntegrator = NULL;
  nodalVolume3d = NULL; 
  nodalVolume2d = NULL; 
  prismConverter = NULL;
  movingMesh = NULL;
  uvLimiter = SLimiter = TLimiter = tkeLimiter = epsLimiter = NULL;
  dgCG3d = dgCG2d = NULL;
  copyField3d = NULL;
  exportDirectory = "output";
  bottomFrictionXi = 0.5;
  model3d = new GModel();
  model3d->load(meshFile3d);
  dimension = 3;
  groups3d = new dgGroupCollection(model3d, dimension);
  order = groups3d->getElementGroup(0)->getOrder();
  if (bottomBndTags.empty())
    bottomBndTags.push_back("bottom");
  bottomTags = bottomBndTags; 
  if (topBndTags.empty())
    topBndTags.push_back("top");
  //groups3d->splitGroupsByVerticalLayer(bottomBndTags);
  if (_solveTurbulence)
    turbulenceSetupFile = "gotmTurb.nml";
  // store all physical tags for boundaries
  std::set<std::string> tags;
  std::vector<GEntity*> entities;
  model3d->getEntities(entities);
  for ( size_t i = 0; i < entities.size(); i++ ) {
    if ( entities[i]->dim() == 2 && entities[i]->physicals.size() >0 ) {
      std::string newTag = model3d->getPhysicalName(2, entities[i]->physicals[0]);
      tags.insert( newTag );
    }
  }
  groups2d = dgGroupCollection::newByTag(model3d, dimension-1, order, topBndTags);
  dgExtrusion *tmpColumnInfo = new dgExtrusion(groups3d, groups2d, topBndTags);
  groups3d->splitGroupsByVerticalLayer(*tmpColumnInfo, bottomBndTags, topBndTags);
  delete tmpColumnInfo;
  columnInfo = new dgExtrusion(groups3d,groups2d,topBndTags);
  groups3d->setExtrusion(columnInfo);
  
  std::copy(tags.begin(),tags.end(),std::back_inserter(physicalTags2d));
  tags.clear();
  for ( size_t i = 0; i < entities.size(); i++ ) {
    if ( entities[i]->dim() == 1 && entities[i]->physicals.size() >0 ) {
      std::string newTag = model3d->getPhysicalName(1, entities[i]->physicals[0]);
      tags.insert( newTag );
    }
  }
  std::copy(tags.begin(),tags.end(),std::back_inserter(physicalTags1d));

  _dofs = new slim3dSolverDofs(this);
  functions = new slim3dSolverFunctions(this);
  _equations = new slim3dSolverEquations(this);

  copy2d3d = new dgSW3dDofCopy(columnInfo);
}

slim3dSolver::~slim3dSolver()
{
  delete model3d;
  delete groups3d;
  delete groups2d;
  delete columnInfo;
  delete _dofs;
  delete functions;
  delete _equations;
  delete copy2d3d;
  if ( wSolver ) delete wSolver;
  //if ( depthIntSolver ) delete depthIntSolver;
  if ( rSolver ) delete rSolver;
  if ( newRGradSolver ) delete newRGradSolver;
  //if ( rGradIntSolver ) delete rGradIntSolver;
  if ( newDepthIntegrator ) delete newDepthIntegrator;
  if ( nodalVolume3d ) delete nodalVolume3d;
  if ( nodalVolume2d ) delete nodalVolume2d;
  if ( prismConverter ) delete prismConverter;
  if ( copyField3d ) delete copyField3d;
  if ( movingMesh ) delete movingMesh;
  if ( uvLimiter ) delete uvLimiter;
  if ( SLimiter ) delete SLimiter;
  if ( TLimiter ) delete TLimiter;
  if ( tkeLimiter ) delete tkeLimiter;
  if ( epsLimiter ) delete epsLimiter;
  if ( sedLimiter ) delete sedLimiter;
  if ( dgCG2d ) delete dgCG2d;
  if ( dgCG3d ) delete dgCG3d;
}

slim3dSolverEquations* slim3dSolver::createEquations()
{
  // cheap sanity-check of the setup
  if ( !_solveTurbulence ) {
    _advectTKE = false;
  }
//   if ( !_solveUVDiff )
//     _bottomFriction = false;
  _dofs->initialize();
  functions->initialize();
  _equations->initialize();
  // create solvers and operators
  wSolver = new dgConservationLawSW3dContinuitySolver(_dofs->wDof3d,INTEGRATE_FROM_BOTTOM);
  //depthIntSolver = new dgConservationLawSW3dContinuitySolver(_dofs->uvIntDof3d,INTEGRATE_FROM_BOTTOM);
  newRGradSolver = new dgConservationLawSW3dContinuitySolver(_dofs->rGradDof3d,INTEGRATE_FROM_TOP);
  //rSolver = new dgConservationLawSW3dContinuitySolver(_dofs->rDof3d,INTEGRATE_FROM_TOP);
  newDepthIntegrator = new slim3dDepthIntegrator(columnInfo);
  nodalVolume3d = new dgNodalVolume(groups3d);
  nodalVolume2d = new dgNodalVolume(groups2d);
  prismConverter = new dgPrismDofConverter(nodalVolume3d->get());
  copyField3d = new dgFieldCopy(groups3d);
  movingMesh = new dgSW3dMovingMesh(groups3d);
  if (_useUVLimiter){
    //uvLimiter = new dgSlopeLimiterVertex (_equations->horMomEq,groups3d, nodalVolume3d->get(), _useOptimalLimiter);
    //uvLimiter = new dgSlopeLimiter4 (_equations->horMomEq,groups3d, _dofs->uvDof);
    uvLimiter = new dgSlopeLimiterUV (_equations->horMomEq,groups3d);
    //uvLimiter = new dgSlopeLimiterOriginal (_equations->horMomEq,groups3d);
  }
  if ( _solveSDiff && !_solveS && _useSLimiter){
    //SLimiter = new dgSlopeLimiter4 (_equations->SDiffEq,groups3d, _dofs->SDof);
    SLimiter = new dgSlopeLimiterOriginal (_equations->SDiffEq,groups3d);
  }
  else if (_useSLimiter && _solveS){
    //SLimiter = new dgSlopeLimiter4 (_equations->SEq,groups3d, _dofs->SDof);
    SLimiter = new dgSlopeLimiterOriginal (_equations->SEq,groups3d);
    //SLimiter = new dgSlopeLimiter2 (_equations->SEq,groups3d, nodalVolume3d->get(), true);
  }
  if ( _solveTDiff && !_solveT && _useTLimiter){
    //TLimiter = new dgSlopeLimiter4 (_equations->TDiffEq,groups3d, _dofs->TDof);
    TLimiter = new dgSlopeLimiterOriginal (_equations->TDiffEq,groups3d);
  }
  else if (_useTLimiter && _solveT){
    //TLimiter = new dgSlopeLimiter4 (_equations->TEq,groups3d, _dofs->TDof);
    TLimiter = new dgSlopeLimiterOriginal (_equations->TEq,groups3d);
  }
  if ( _solveSedDiff && !_solveSed && _useSedLimiter){
    //sedLimiter = new dgSlopeLimiter4 (_equations->sedDiffEq,groups3d, _dofs->sedDof);
    sedLimiter = new dgSlopeLimiterOriginal (_equations->sedDiffEq,groups3d);
  }
  else if (_useSedLimiter && _solveSed){
    //sedLimiter = new dgSlopeLimiter4 (_equations->sedEq,groups3d, _dofs->sedDof);
    sedLimiter = new dgSlopeLimiterOriginal (_equations->sedEq,groups3d);
  }
  if ( _advectTKE ) {
    //tkeLimiter = new dgSlopeLimiter4 (_equations->tkeAdvEq,groups3d, _dofs->tkeDof);
    tkeLimiter = new dgSlopeLimiterOriginal (_equations->tkeAdvEq,groups3d);
    //epsLimiter = new dgSlopeLimiter4 (_equations->epsAdvEq,groups3d, _dofs->epsDof);
    epsLimiter = new dgSlopeLimiterOriginal (_equations->epsAdvEq,groups3d);
  }
  if (_solveTurbulence)
    dgCG3d = new dgCGMeanFilter(groups3d);
  dgCG2d = new dgCGMeanFilter(groups2d);
  _initialized = true;
  return _equations;
}

slim3dSolverEquations* slim3dSolver::getEquations() {
  if ( !_equations->isInitialized() ) {
    Msg::Error("Equations not yet created!");
    return NULL;
  }
  return _equations;
}
