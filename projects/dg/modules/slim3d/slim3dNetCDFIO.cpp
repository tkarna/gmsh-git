#include "slim3dNetCDFIO.h"

mpiArrayStructure::mpiArrayStructure(dgGroupCollection* groups)
{
  _groups=groups;
  // get total node and element counts in comm_world
  std::vector<int> mpiElemCount(Msg::GetCommSize());
  std::vector<int> mpiNodeCount(Msg::GetCommSize());

  _elemCount = 0;
  _nodeCount = 0;
  int maxElemNodes = 0;
  for (int i=0; i<_groups->getNbElementGroups(); i++) {
    _elemCount += _groups->getElementGroup(i)->getNbElements();
    _nodeCount += _groups->getElementGroup(i)->getNbElements()*_groups->getElementGroup(i)->getNbNodes();
    maxElemNodes = std::max(maxElemNodes,_groups->getElementGroup(i)->getNbNodes());
  }
  MPI_Allgather(&_elemCount,1,MPI_INT,mpiElemCount.data(),1,MPI_INT,MPI_COMM_WORLD);
  MPI_Allgather(&_nodeCount,1,MPI_INT,mpiNodeCount.data(),1,MPI_INT,MPI_COMM_WORLD);
  MPI_Allreduce(&maxElemNodes,&_maxElemNodes,1,MPI_INT,MPI_MAX,MPI_COMM_WORLD);

  _totElemCount=0;
  _totNodeCount=0;
  _elemOffset=0;
  _nodeOffset=0;
  for ( int i = 0; i < Msg::GetCommSize(); i++ ) {
    _totElemCount += mpiElemCount[i];
    _totNodeCount += mpiNodeCount[i];
  }
  for ( int i = 0; i < Msg::GetCommRank(); i++ ) {
    _elemOffset += mpiElemCount[i];
    _nodeOffset += mpiNodeCount[i];
  }

}

/* helper function to handle pnetcdf C lib errors */
static void handle_error(int status, int lineno)
{
    Msg::Error( "Error at line %d: (%i) %s\n", lineno, Msg::GetCommRank(),ncmpi_strerror(status));
    MPI_Abort(MPI_COMM_WORLD, 1);
}

slimNetCDFIO::slimNetCDFIO(dgGroupCollection *groups) : mpiArrayStructure(groups)
{
  _groups=groups;
  _initYear = 2010;
  _initMonth = 1;
  _initDay = 1;
  _initHour = _initMinute = _initSecond = 0;
  _timeZoneHour = _timeZoneMinute = 0;
  // construct 2d node x,y,z coords and connectivity matrix
  const dgDofContainer& coordsDof = _groups->coordinatesDof();
  // FIXME any way to avoid copy of data ?
  int iNode=0, iElem=0;
  _xVec.reserve(_nodeCount);
  _yVec.reserve(_nodeCount);
  _zVec.reserve(_nodeCount);
  _connectivity.resize(_elemCount*_maxElemNodes);
  _connectivity.assign(_elemCount*_maxElemNodes,-1);
  for (int i=0; i<_groups->getNbElementGroups(); i++){
    dgGroupOfElements *group = _groups->getElementGroup(i);
    fullMatrix<double> xyz;
    for (size_t iElement=0 ; iElement<group->getNbElements(); ++iElement) {
      coordsDof.getGroupProxy(i).getBlockProxy(iElement, xyz);
      for (int k=0; k<xyz.size1(); ++k) {
        _xVec[iNode] = xyz(k,0);
        _yVec[iNode] = xyz(k,1);
        _zVec[iNode] = xyz(k,2);
        _connectivity[iElem*_maxElemNodes+k] = iNode + _nodeOffset; // global node index
//         posVect[0] = nodeOffset+iNode;
//         ncmpi_put_var1_double(pncfile, nodeXVar, posVect, &xyz(k,0));
//         ncmpi_put_var1_double(pncfile, nodeYVar, posVect, &xyz(k,1));
//         ncmpi_put_var1_double(pncfile, nodeZVar, posVect, &xyz(k,2));
        iNode++;
      }
      iElem++;
    }
  }

}

// TODO add startTime(ymdhms), exportSpool in exporter options
void slimNetCDFIO::exportUGRID(const dgDofContainer *dof, const std::string name, double time, int step, std::string fieldName)
{
  for (int iComp=0; iComp<dof->getNbFields(); ++iComp)
    exportUGRIDSingleComp(dof, iComp, name, time, step, fieldName);
}

void slimNetCDFIO::_define2dHeader(int pncfile)
{
  int ret;
  // dimensions
  ret = ncmpi_def_dim(pncfile, "node", _totNodeCount, &_nodeDim);
  if (ret != NC_NOERR) handle_error(ret, __LINE__);
  ret = ncmpi_def_dim(pncfile, "face", _totElemCount, &_faceDim);
  if (ret != NC_NOERR) handle_error(ret, __LINE__);
  ret = ncmpi_def_dim(pncfile, "nFaceNodes", _maxElemNodes, &_faceNodeDim);
  if (ret != NC_NOERR) handle_error(ret, __LINE__);
  ret = ncmpi_def_dim(pncfile, "time", 1, &_timeDim);
  if (ret != NC_NOERR) handle_error(ret, __LINE__);

  // variables
  int dimVect[3];
  dimVect[0] = _nodeDim;
  ret = ncmpi_def_var(pncfile, "node_x", NC_DOUBLE, 1, dimVect, &_nodeXVar);
  if (ret != NC_NOERR) handle_error(ret, __LINE__);
  ret = ncmpi_def_var(pncfile, "node_y", NC_DOUBLE, 1, dimVect, &_nodeYVar);
  if (ret != NC_NOERR) handle_error(ret, __LINE__);
  ret = ncmpi_def_var(pncfile, "node_z", NC_DOUBLE, 1, dimVect, &_nodeZVar);
  if (ret != NC_NOERR) handle_error(ret, __LINE__);
  dimVect[0] = _timeDim;
  ret = ncmpi_def_var(pncfile, "time", NC_DOUBLE, 1, dimVect, &_timeVar);
  if (ret != NC_NOERR) handle_error(ret, __LINE__);
  std::stringstream startTime;
  startTime << "seconds since " << _initYear << "-";
  startTime << std::setw( 2 ) << std::setfill( '0' ) << _initMonth << "-";
  startTime << std::setw( 2 ) << std::setfill( '0' ) << _initDay << " "; //00:00:00 -8:00";
  startTime << std::setw( 2 ) << std::setfill( '0' ) << _initHour << ":";
  startTime << std::setw( 2 ) << std::setfill( '0' ) << _initMinute << ":";
  startTime << std::setw( 2 ) << std::setfill( '0' ) << _initSecond << " ";
  startTime << std::setw( 2 ) << std::setfill( '0' ) << _timeZoneHour << ":";
  startTime << std::setw( 2 ) << std::setfill( '0' ) << _timeZoneMinute;
  std::string startStr = startTime.str();
  ret = ncmpi_put_att_text(pncfile, _timeVar, "base_date", startStr.length(), startStr.c_str());
  if (ret != NC_NOERR) handle_error(ret, __LINE__);

  dimVect[0] = _faceDim;
  dimVect[1] = _faceNodeDim;
  ret = ncmpi_def_var(pncfile, "face_nodes", NC_INT, 2, dimVect, &_connVar);
  if (ret != NC_NOERR) handle_error(ret, __LINE__);
  int firstElem = 0;
  ret = ncmpi_put_att_int(pncfile, _connVar, "start_index", NC_INT, 1, &firstElem);
  if (ret != NC_NOERR) handle_error(ret, __LINE__);
}

void slimNetCDFIO::_write2dHeader(int pncfile, double time)
{
  int ret;
  MPI_Offset start[2], count[2];
  // TODO only proc 0 writes ??
  // swich to independent mode
  ret = ncmpi_begin_indep_data(pncfile);
  if (ret != NC_NOERR) handle_error(ret, __LINE__);
  if (Msg::GetCommRank() == 0) {
    start[0] = 0;
    count[0] = 1;
    ret = ncmpi_put_vara_double(pncfile,_timeVar,start,count,&time);
    if (ret != NC_NOERR) handle_error(ret, __LINE__);
  }
  // quit independent mode
  ret = ncmpi_end_indep_data(pncfile);
  if (ret != NC_NOERR) handle_error(ret, __LINE__);

  start[0] = _nodeOffset;
  count[0] = _nodeCount;
  ret = ncmpi_put_vara_double_all(pncfile,_nodeXVar,start,count,_xVec.data());
  if (ret != NC_NOERR) handle_error(ret, __LINE__);
  ret = ncmpi_put_vara_double_all(pncfile,_nodeYVar,start,count,_yVec.data());
  if (ret != NC_NOERR) handle_error(ret, __LINE__);
  ret = ncmpi_put_vara_double_all(pncfile,_nodeZVar,start,count,_zVec.data());
  if (ret != NC_NOERR) handle_error(ret, __LINE__);

  start[0] = _elemOffset;
  start[1] = 0;
  count[0] = _elemCount;
  count[1] = 3;
  ret = ncmpi_put_vara_int_all(pncfile,_connVar,start,count,_connectivity.data());
  if (ret != NC_NOERR) handle_error(ret, __LINE__);
}

void slimNetCDFIO::exportUGRIDSingleComp(const dgDofContainer *dof, int iComp, const std::string name, double time, int step, std::string fieldName)
{
  if (dof->getGroups() != _groups )
    Msg::Fatal("dgGroupCollection does not match");
  if (iComp >= dof->getNbFields())
    Msg::Fatal("Unable to open export file '%s': ICOMP (%i) greater or equal to the number of fields (%i)", name.c_str(), iComp, dof->getNbFields());

  std::ostringstream name_nc, name_view;
  if (fieldName != "")
    name_view<<fieldName;
  else if (!dof->getFieldName(iComp).empty())
    name_view<<dof->getFieldName(iComp);
  else
    name_view<<"comp_"<<iComp;
  name_nc<<name<<"_COMP_"<<iComp<<".nc";

  // PNETCDF
  int pncfile, ret;
  ret = ncmpi_create(MPI_COMM_WORLD, name_nc.str().c_str(),
                     NC_CLOBBER|NC_64BIT_OFFSET, MPI_INFO_NULL, &pncfile);
  if (ret != NC_NOERR) handle_error(ret, __LINE__);

  _define2dHeader(pncfile);

  int dataVar;
  int dimVect[3];
  dimVect[0] = _nodeDim;
  dimVect[1] = _timeDim;
  ret = ncmpi_def_var(pncfile, name_view.str().c_str(), NC_DOUBLE, 2, dimVect, &dataVar);
  if (ret != NC_NOERR) handle_error(ret, __LINE__);

  // quit define mode
  ret = ncmpi_enddef(pncfile);
  if (ret != NC_NOERR) handle_error(ret, __LINE__);

  _write2dHeader(pncfile, time);

  // construct data array
  // FIXME any way to avoid copy of data ?
  int iNode=0;
  std::vector<double> dVec(_nodeCount);
  for (int i=0; i<_groups->getNbElementGroups(); i++){
    dgGroupOfElements *group = _groups->getElementGroup(i);
    fullMatrix<double> vals;
    for (size_t iElement=0 ; iElement<group->getNbElements(); ++iElement) {
      dof->getGroupProxy(i).getBlockProxy(iElement, vals);
      for (int k=0; k<vals.size1(); ++k) {
        dVec[iNode] = vals(k,iComp);
        iNode++;
      }
    }
  }
  // write data array
  MPI_Offset start[3], count[3];
  start[0] = _nodeOffset;
  start[1] = 0; // time
  count[0] = _nodeCount;
  count[1] = 1;
  ret = ncmpi_put_vara_double_all(pncfile,dataVar,start,count,dVec.data());
  if (ret != NC_NOERR) handle_error(ret, __LINE__);

  // close
  ret = ncmpi_close(pncfile);
  if (ret != NC_NOERR) handle_error(ret, __LINE__);
}

slim3dNetCDFIO::slim3dNetCDFIO(dgExtrusion* columnInfo, slimNetCDFIO* ncWriter2d)
{
  _columns = columnInfo;
  _ncWriter2d = ncWriter2d;
}

void slim3dNetCDFIO::exportUGRID(const dgDofContainer* dof, const std::string name, double time, int step, std::string fieldName)
{
  if (dof->getGroups() == &_columns->getGroups2d()) {
//     printf("*** exporting 2D : %s ***\n",name.c_str());
    _ncWriter2d->exportUGRID(dof,name,time,step,fieldName);
    return;
  }

//   printf("*** exporting 3D : %s***\n",name.c_str());
  for (int iComp=0; iComp<dof->getNbFields(); ++iComp)
    exportUGRIDSingleComp(dof, iComp, name, time, step, fieldName);
}

void slim3dNetCDFIO::exportUGRIDSingleComp(const dgDofContainer* dof, int iComp, const std::string name, double time, int step, std::string fieldName)
{
  if (dof->getGroups() != &_columns->getGroups3d())
    Msg::Fatal("dgGroupCollection does not match");
  if (iComp >= dof->getNbFields())
    Msg::Fatal("Unable to open export file '%s': ICOMP (%i) greater or equal to the number of fields (%i)", name.c_str(), iComp, dof->getNbFields());

  std::ostringstream name_nc, name_view;
  if (fieldName != "")
    name_view<<fieldName;
  else if (!dof->getFieldName(iComp).empty())
    name_view<<dof->getFieldName(iComp);
  else
    name_view<<"comp_"<<iComp;
  name_nc<<name<<"_COMP_"<<iComp<<".nc";

  // PNETCDF
  int pncfile, ret;
  ret = ncmpi_create(MPI_COMM_WORLD, name_nc.str().c_str(),
                     NC_CLOBBER|NC_64BIT_OFFSET, MPI_INFO_NULL, &pncfile);
  if (ret != NC_NOERR) handle_error(ret, __LINE__);

  /* define */
  // export header for 2d grid, arrays (np,)
  _ncWriter2d->_define2dHeader(pncfile);
  int nVert = _columns->getMaxNbDGPoints();
  int nNode2d = _ncWriter2d->_nodeCount;
  /* dimensions */
  int layersDim;
  ret = ncmpi_def_dim(pncfile, "layers", nVert, &layersDim);
  if (ret != NC_NOERR) handle_error(ret, __LINE__);

  /* variables */
  int kBotVar, dataVar;
  int dimVect[3];
  dimVect[0] = _ncWriter2d->_nodeDim;
  dimVect[1] = layersDim;
  dimVect[2] = _ncWriter2d->_timeDim;
  // variables as (np,nvert) arrays
  ret = ncmpi_def_var(pncfile, name_view.str().c_str(), NC_DOUBLE, 3, dimVect, &dataVar);
  if (ret != NC_NOERR) handle_error(ret, __LINE__);
  // k_bottom (np,) array
  ret = ncmpi_def_var(pncfile, "k_bottom", NC_INT, 1, dimVect, &kBotVar);
  if (ret != NC_NOERR) handle_error(ret, __LINE__);
  // TODO 3D elem connectivity ?

  // quit define mode
  ret = ncmpi_enddef(pncfile);
  if (ret != NC_NOERR) handle_error(ret, __LINE__);

  _ncWriter2d->_write2dHeader(pncfile, time);

  // construct data arrays
  std::vector<double> dVec(nNode2d*nVert,-99.0);
  size_t iPoint, iLevel;
  dgGroupCollection& groups = _columns->getGroups3d();
  for (size_t i=0; i<(size_t)groups.getNbElementGroups(); i++){
    dgGroupOfElements *group = groups.getElementGroup(i);
    fullMatrix<double> vals;
    for (size_t iElement=0 ; iElement<(size_t)group->getNbElements(); ++iElement) {
      dof->getGroupProxy(i).getBlockProxy(iElement, vals);
      for (size_t iNode=0; iNode<(size_t)vals.size1(); ++iNode) {
        _columns->mapDofToVEdgeDG(i,iElement,iNode,iPoint,iLevel);
        dVec[iPoint*nVert+iLevel] = vals(iNode,iComp);
      }
    }
  }
  std::vector<double> kBotVec(nNode2d,0.0);
  for (size_t iP = 0; iP < _columns->getNbVerticals(); iP++ )
    kBotVec[iP] = _columns->getNbDGPointsInVertical(iP);
  // write data array
  MPI_Offset start[3], count[3];
  start[0] = _ncWriter2d->_nodeOffset;
  start[1] = 0;
  start[2] = 0;
  count[0] = nNode2d;
  count[1] = nVert;
  count[2] = 1;
  ret = ncmpi_put_vara_double_all(pncfile,dataVar,start,count,dVec.data());
  if (ret != NC_NOERR) handle_error(ret, __LINE__);
  ret = ncmpi_put_vara_double_all(pncfile,kBotVar,start,count,kBotVec.data());
  if (ret != NC_NOERR) handle_error(ret, __LINE__);

  // close
  ret = ncmpi_close(pncfile);
  if (ret != NC_NOERR) handle_error(ret, __LINE__);
}
