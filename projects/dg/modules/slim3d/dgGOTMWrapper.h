#ifndef _DG_GOTM_WRAPPER__H_
#define _DG_GOTM_WRAPPER__H_
#include "dgConfig.h"
#ifdef HAVE_GOTM
#include <string>
class dgGOTMinterface {
 private:
  int _nlev, _nNamelist;
  double *_tkeGOTM, *_epsGOTM, *_LGOTM, *_nuhGOTM, *_numGOTM;
 public:
  void init(int nlev, std::string namelist);
  void doTurbulence(double dt, double depth, double uTaus, double uTaub, double z0s, double z0b, double* h,double* NN, double* SS);
  void setTKE(double* tke);
  void getTKE(double* tke);
  void setEPS(double* eps);
  void getEPS(double* eps);
  void setL(double* L);
  void getL(double* L);
  void setNUM(double* num);
  void getNUM(double* num);
  void setNUH(double* nuh);
  void getNUH(double* nuh);
};
#endif
#endif

