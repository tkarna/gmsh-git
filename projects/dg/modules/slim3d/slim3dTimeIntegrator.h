#ifndef SLIM3D_TIME_INTEGRATOR_H
#define SLIM3D_TIME_INTEGRATOR_H
#include "slim3dSolver.h"
#include "dgSteady.h"
#include "dgFunctionIntegrator.h"
#include <time.h>
#include "linearSystemPETSc.h"

/** This is the base class for all time integration instants */
class slim3dTimeIntegrator {
protected:
   void _initializeFields();
  
  clock_t _startClock;
  slim3dSolver *_solver;
  int _exportCount;
  double _exportInterval, _endTime, _initTime, _time, _dt, _nextExportTime;
  int _fullExportIntervalRatio;
  double _fullExportInterval, _nextFullExportTime, _fullExportEveryIter;
  int _fineExportStartIndex,_fineExportDuration, _fineExportRatio;
  int _iter, _n3d2d;
  bool _exportEveryIter;
  bool _exploded;
  linearSystem<fullMatrix<double> > *uSys, *SDiffSys, *TDiffSys, *sedDiffSys;
  linearSystem <double> *streamFuncSys;
  dgDofManager *dofManagerU, *dofManagerSDiff, *dofManagerTDiff, *dofManagerSedDiff;
  dgDofManager *dofManagerStreamFunc;
  // TODO remove dgDIRK, create newton directly
  dgSteady *GMStreamFuncSolver;
  // compute mass conservation and tracer deviation
  class massConservationChecker {
    dgFunctionIntegrator _integrator;
    bool _initialized;
    double _initMass;
  public:
    massConservationChecker(dgGroupCollection* groups, const function* func);
    double computeRelativeChange();
  };
  class tracerDeviationChecker {
    dgDofContainer* _dof;
    bool _initialized;
    double _initMin, _initMax;
  public:
    tracerDeviationChecker(dgDofContainer* dof);
    double computeMaxChange(double &minVal, double &maxVal);
  };
  bool _checkMassConservation, _checkDeviation;
  massConservationChecker *_volConservationChecker, *_SConservationChecker, *_TConservationChecker; 
  tracerDeviationChecker  *_TDeviationChecker, *_SDeviationChecker, *_sedDeviationChecker;
public:
  dgNewton *momUNewton, *momVNewton, *SDiffNewton, *TDiffNewton, *sedDiffNewton;
  /** Constructor */
  slim3dTimeIntegrator( slim3dSolver* solver );
  virtual ~slim3dTimeIntegrator();
  inline int getExportCount() { return _exportCount; };
  inline void setExportCount(int i) { _exportCount = i; };
  /** Set time interval between exports */
  void setExportInterval( double d );
  /** set fullExportIntervalRatio */
  void setFullExportIntervalRatio( int r );
  inline int getExportInterval() { return _exportInterval; };
  /** Assing end time */
  inline void setEndTime( double d ) { _endTime = d; };
  inline double getEndTime() { return _endTime; };
  inline void setInitTime( double d ) { _initTime = d; _time = d; };
  inline double getInitTime() { return _initTime; };
  /** Assign 2d time step (s) */
  inline void setTimeStep( double d ) { _dt = d; };
  inline double getTimeStep() { return _dt; };
  /** Assign 3d time step (as a multiple of 2d time steps) */
  inline void set3dTimeStepRatio( int i ) { _n3d2d = i; };
  /** Force export at each 3d iteration */
  inline void setExportAtEveryTimeStep ( bool b ) { _exportEveryIter = b; };
  /** Force full export at each 3d iteration */
  inline void setFullExportAtEveryTimeStep ( bool b ) { _fullExportEveryIter = b; };
  /** Set fine export frequency starting from export i */
  inline void setFineExportStartIndex( int i){ _fineExportStartIndex = i; };
  /** Set fine export interval as a fraction of default export interval */
  inline void setFineExportRatio( int i){ _fineExportRatio = i; };
  /** Set fine export duration as a multiple of default export interval */
  inline void setFineExportDuration( int i){ _fineExportDuration = i; };
  /** Get the time variable */
  inline double getTime(){ return _time;};
  inline void setTime( double d ){ _time = d;};
  /** Get the iteration variable */
  inline int getIter(){ return _iter;};
  inline void setIter( int i ){ _iter = i;};
  /** Conservation and consistance stuff */
  void checkMassConservation(double *volErr = NULL, double *SErr= NULL, double *TErr = NULL);
  void checkTracerConsistency(double *SDev = NULL, double *TDev = NULL); 
  /** Check if export is pending */
  bool checkExport();
  /** Check if full export is pending */
  bool checkFullExport();
  /** Check that fields have not blown up */
  void checkSanity(bool only2d = false);
  /** Check that all the parameters have been set to meaningful values */
  void checkParameters();
  /** A callback function that updates uv dependent variables */
  virtual void uvDependencies(dgDofContainer *uvAv2d, bool hotStart = false);
  /** A callback function that updates uv dependent variables for 2D mode */
  virtual void uv2dDependencies();
  /** A callback function that updates eta dependent variables */
  virtual void etaDependencies(dgDofContainer *eta2d);
  /** A callback function that updates eta dependent variables (for 2d equations) */
  virtual void eta2dDependencies();
  /** A callback function that updates density dependent variables */
  virtual void rhoDependencies(bool compute2dForcing=false);
  /** Iterates the simulation until the end of time */
  virtual void iterate(bool exitWhenDone=false)=0;
  /** a routine for clean exit */
  void terminate(int exitFlag=0);
};

/** Simple predictor-corrector time integration */
class slim3dTimeIntegratorPC : public slim3dTimeIntegrator {
  bool _started;
  std::set<dgIdxExporter*> _fullExporter;
  void _initializeIterator();
public:
  dgDofContainer *uvDof_K;
  dgDofContainer *SDof_K;
  dgDofContainer *TDof_K;
  dgDofContainer *sedDof_K;
  const dgDofContainer *uKim, *vKim, *SKim, *TKim, *sedKim;
  dgDofContainer *tkeDof_K, *tkeDof_K1, *tkeDof_K2, *tkeDof_K3;
  dgDofContainer *epsDof_K, *epsDof_K1, *epsDof_K2, *epsDof_K3;
  dgDofContainer *uvAvDof2d_K, *uvAvDof2d_Kold, *uvAvDof2d_Kold2;
  dgDofContainer *etaDof2d_K, *etaDof2d_Kold, *etaDof2d_Kold2;
  dgDofContainer *etaDof2d_new;
  // for predictor corrector
  dgDofContainer *etaDof2d_tav_new;
  dgDofContainer *uvAvDof2d_tav_new, *uvIntDof2d_tav_new, *uvIntDof2d_tav_newHalf;
  dgDofContainer *uvIntDof2d_old;
  dgDofContainer *uvDof_new, *uvDof_old;
  dgDofContainer *SDof_new, *SDof_old;
  dgDofContainer *TDof_new, *TDof_old;
  dgDofContainer *sedDof_new, *sedDof_old;
  dgDofContainer *sedGroundDof_new, *sedGroundDof_old;
  //dgDofContainer *uvAvDof2d_old2;
  //dgDofContainer *uvAvDof2d_old;
  //dgDofContainer *uvAvDof2d_cur;
//   uvAvDof2d_new = dgDofContainer(*groups2d, 2)
  //dgDofContainer *etaDof2d_old2;
  //dgDofContainer *etaDof2d_old;
  //dgDofContainer *etaDof2d_cur;

  /** Adams-Bashfort coefficients */
  double betaAB3;
  /** Leapfrog-AM3 coefficient (Shchepetkin and McWilliams 2005) **/
  double gammaLFAM3;

  /** Temporal filters *
   * see Shchepetkin and McWilliams 2005 */
  int MStar;
  std::vector<double> weightsA, weightsB;
  void createTemporalWeights();
  /** implicity of semi-implicit vertical diffusion */
  double CrNiTheta;
  slim3dTimeIntegratorPC( slim3dSolver* solver );
  ~slim3dTimeIntegratorPC();
  virtual void iterate(bool exitWhenDone=0);
  /** Takes one time step */
  void advanceOneTimeStep();
  void reStart(std::string directory, int export_i);
  void approximateHotStart(std::string directory, int export_i);
  void initFullExporter(std::string exportDirectory);
  void exportAll();
  void scalingTest();
};

#endif
