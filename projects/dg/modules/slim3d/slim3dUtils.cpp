#include "slim3dUtils.h"
#include "dgIntegrationMatrices.h"
// #include "dgGroupOfElements.h"
// #include "dgDofContainer.h"
#include "SPoint3.h"
#include "dgMesh.h"
#include "dgMeshJacobian.h"
#include "dgExtrusion.h"
#include "GmshDefines.h"
#include "MElement.h"

void dgPrismDofConverter::convertToPZero2d(const dgDofContainer* source, dgDofContainer* dest)
{
  const dgGroupCollection* groups = source->getGroups();
  for (int iG = 0; iG < groups->getNbElementGroups(); iG++) {
    dgGroupOfElements *group = groups->getElementGroup(iG);
    int nbNodes = group->getNbNodes();
    int order = group->getOrder();
    if (order != 1)
      Msg::Fatal("P0 dof converter implemented only for first order");
    const dgFullMatrix<double> *sourceGData = &source->getGroupProxy(iG);
    dgFullMatrix<double> *destGData = &dest->getGroupProxy(iG);
    for (size_t iE =0; iE < group->getNbElements(); iE++) {
      fullMatrix<double> sourceEData, destEData;
      sourceGData->getBlockProxy(iE, sourceEData);
      destGData->getBlockProxy(iE, destEData);
      for ( int iF = 0; iF < source->getNbFields(); iF++ ) {
        // full mean
        double sourceMean = 0;
        for (int iNode = 0; iNode < nbNodes; iNode++ ) {
          sourceMean += sourceEData(iNode,iF);
        }
        sourceMean /= nbNodes;
        for (int iNode = 0; iNode < nbNodes; iNode++ ) {
          destEData(iNode,iF) = sourceMean;
        }
      }
    }
  }
}

void dgPrismDofConverter::convertToPZero(const dgDofContainer* source, dgDofContainer* dest)
{
  const dgGroupCollection* groups = source->getGroups();
  for (int iG = 0; iG < groups->getNbElementGroups(); iG++) {
    dgGroupOfElements *group = groups->getElementGroup(iG);
    int order = group->getOrder();
    int nbNodes = group->getNbNodes();
    const dgFullMatrix<double> *sourceGData = &source->getGroupProxy(iG);
    dgFullMatrix<double> *destGData = &dest->getGroupProxy(iG);
    const dgFullMatrix<double> *volGData = &_nodalVolume->getGroupProxy(iG);
    for (size_t iE =0; iE < group->getNbElements(); iE++) {
      if ( order != 1)
        Msg::Fatal("P0 dof converter implemented only for first order ");
      fullMatrix<double> sourceEData, destEData, volEData;
      sourceGData->getBlockProxy(iE, sourceEData);
      destGData->getBlockProxy(iE, destEData);
      volGData->getBlockProxy(iE, volEData);
      for ( int iF = 0; iF < source->getNbFields(); iF++ ) {
        // full mean
        double sourceMean = 0;
        double totVol = 0;
        for (int iNode = 0; iNode < nbNodes; iNode++ ) {
          sourceMean += sourceEData(iNode,iF) * volEData(iNode,0);
          totVol += volEData(iNode,0);
        }
        sourceMean /= totVol;
        for (int iNode = 0; iNode < nbNodes; iNode++ ) {
          destEData(iNode,iF) = sourceMean;
        }
      }
    }
  }

}

void dgPrismDofConverter::convertToPZeroHorizontal(const dgDofContainer* source, dgDofContainer* dest)
{
  const dgGroupCollection* groups = source->getGroups();
  for (int iG = 0; iG < groups->getNbElementGroups(); iG++) {
    dgGroupOfElements *group = groups->getElementGroup(iG);
    int nbNodes = group->getNbNodes();
    int order = group->getOrder();
    const dgFullMatrix<double> *sourceGData = &source->getGroupProxy(iG);
    dgFullMatrix<double> *destGData = &dest->getGroupProxy(iG);
    const dgFullMatrix<double> *volGData = &_nodalVolume->getGroupProxy(iG);
    for (size_t iE =0; iE < group->getNbElements(); iE++) {
      fullMatrix<double> sourceEData, destEData, volEData;
      sourceGData->getBlockProxy(iE, sourceEData);
      destGData->getBlockProxy(iE, destEData);
      volGData->getBlockProxy(iE, volEData);
      if ( order != 1)
        Msg::Fatal("P0 dof converter implemented only for first order ");
      for ( int iF = 0; iF < source->getNbFields(); iF++ ) {
        // compute for top nodes
        double sourceMean = 0;
        double totVol = 0;
        for (int iNode = 0; iNode < nbNodes/2; iNode++ ) {
          sourceMean += sourceEData(iNode,iF) * volEData(iNode,0);
          totVol += volEData(iNode,0);
        }
        sourceMean /= totVol;
        for (int iNode = 0; iNode < nbNodes/2; iNode++ ) {
          destEData(iNode,iF) = sourceMean;
        }
        if (nbNodes > nbNodes/2) {
          // compute for bottom nodes
          sourceMean = 0;
          totVol = 0;
          for (int iNode = nbNodes/2; iNode < nbNodes; iNode++ ) {
            sourceMean += sourceEData(iNode,iF) * volEData(iNode,0);
            totVol += volEData(iNode,0);
          }
          sourceMean /= totVol;
          for (int iNode = nbNodes/2; iNode < nbNodes; iNode++ ) {
            destEData(iNode,iF) = sourceMean;
          }
        }
      }
    }
  }
}

void dgPrismDofConverter::convertToPZeroHorizontal(const function* f, dgDofContainer* dest)
{
  const dgGroupCollection* groups = dest->getGroups();
  dataCacheMap cacheMap(dataCacheMap::NODE_MODE, groups);
  dataCacheDouble* fCache = cacheMap.get(f,NULL);
  for (int iG = 0; iG < groups->getNbElementGroups(); iG++) {
    dgGroupOfElements *group = groups->getElementGroup(iG);
    int nbNodes = group->getNbNodes();
    int order = group->getOrder();
    dgFullMatrix<double> *destGData = &dest->getGroupProxy(iG);
    cacheMap.setGroup(group);
    const dgFullMatrix<double> *volGData = &_nodalVolume->getGroupProxy(iG);
    for (size_t iE =0; iE < group->getNbElements(); iE++) {
      fullMatrix<double> destEData, volEData;
      destGData->getBlockProxy(iE, destEData);
      volGData->getBlockProxy(iE, volEData);
      cacheMap.setElement(iE);
      if ( order != 1)
        Msg::Fatal("P0 dof converter implemented only for first order ");
      for ( int iF = 0; iF < dest->getNbFields(); iF++ ) {
        // compute for top nodes
        double sourceMean = 0;
        double totVol = 0;
        for (int iNode = 0; iNode < nbNodes/2; iNode++ ) {
          sourceMean += fCache->get()(iNode,iF) * volEData(iNode,0);
          totVol += volEData(iNode,0);
        }
        sourceMean /= totVol;
        for (int iNode = 0; iNode < nbNodes/2; iNode++ ) {
          destEData(iNode,iF) = sourceMean;
        }
        if (nbNodes > nbNodes/2) {
          // compute for bottom nodes
          sourceMean = 0;
          totVol = 0;
          for (int iNode = nbNodes/2; iNode < nbNodes; iNode++ ) {
            sourceMean += fCache->get()(iNode,iF) * volEData(iNode,0);
            totVol += volEData(iNode,0);
          }
          sourceMean /= totVol;
          for (int iNode = nbNodes/2; iNode < nbNodes; iNode++ ) {
            destEData(iNode,iF) = sourceMean;
          }
        }
      }
    }
  }
}

slim3dDepthIntegrator::slim3dDepthIntegrator( dgExtrusion* columns )
{
  _columns = columns;
  _groups3d = &columns->getGroups3d();
  _groups2d = &columns->getGroups2d();
  _nodalHeight = new dgDofContainer(*_groups3d,1);
  _computeNodalHeight();
  _downValue.resize(_groups3d->getElementGroup(0)->getNbElements() * 8,0);// biggest elements are currently hexahedra
}
 
slim3dDepthIntegrator::~slim3dDepthIntegrator()
{
  if (_nodalHeight) delete _nodalHeight; 
}

void slim3dDepthIntegrator::update()
{
  _computeNodalHeight();
}

void slim3dDepthIntegrator::_computeNodalHeight()
{
  for (int iG = 0; iG < _groups3d->getNbElementGroups(); iG++) {
    dgGroupOfElements *group = _groups3d->getElementGroup(iG);
    int order = group->getOrder();
    if (order != 1)
      Msg::Fatal("depth integrator implemented only for first order!");
    int nbNodes = group->getNbNodes();
    dgFullMatrix<double> &groupData = _nodalHeight->getGroupProxy(iG);
    for (size_t iE =0; iE < group->getNbElements(); iE++) {
      int iElem = iE;
      MElement* elem = group->getElement(iElem);
      dgFullMatrix<double> elemData;
      groupData.getBlockProxy(iE,elemData);
      for (int iNode = 0; iNode < nbNodes/2; iNode++ ) {
        double z1 = elem->getShapeFunctionNode(iNode  )->z();
        double z2 = elem->getShapeFunctionNode(iNode+nbNodes/2)->z();
        double height = fabs(z1-z2);
        // specific to P1 prisms
        elemData(iNode  ,0) = 0.5*height;
        elemData(iNode+nbNodes/2,0) = 0.5*height;
      }
    }
  }
}

void slim3dDepthIntegrator::integrate(const dgDofContainer* dof3d, dgDofContainer* dof2d, bool scatterSolution)
{
  if (dof2d->getGroups() != _groups2d ||
      dof3d->getGroups() != _groups3d )
    Msg::Fatal("Cannot integrate DOFs: dgGroupCollection does not match the projector");
//   if (dof3d->getNbFields() != dof2d->getNbFields())
//     Msg::Fatal("Cannot integrate DOFs: number of fields in donor and receiver must match");
  dof2d->scale(0.0);
  int nbFields = std::min( dof3d->getNbFields(),dof2d->getNbFields() );
  for (int iGroup3d = 0; iGroup3d < _groups3d->getNbElementGroups(); iGroup3d++) {
    dgGroupOfElements* group3d = _groups3d->getElementGroup(iGroup3d);
    if (group3d->getDimUVW()!= 3 )
      Msg::Fatal("Cannot integrate DOFs: donor must be a 3D dgDofContainer");
    const dgFullMatrix<double> &valGroupData = dof3d->getGroupProxy(iGroup3d);
    dgFullMatrix<double> &hGroupData = _nodalHeight->getGroupProxy(iGroup3d);
    for (size_t iElem3d = 0; iElem3d < group3d->getNbElements() ; iElem3d++) {
      dgFullMatrix<double> elemData, elemHeight;
      valGroupData.getBlockProxy(iElem3d,elemData);
      hGroupData.getBlockProxy(iElem3d,elemHeight);
      for (int iNode3d = 0; iNode3d < group3d->getNbNodes(); iNode3d++) {
        size_t iVertical=0, iLevel=0;
        _columns->mapDofToVEdgeDG(iGroup3d,iElem3d,iNode3d,iVertical,iLevel);
        size_t iGroup2d=0, iElem2d=0, iNode2d=0;
        _columns->mapVEdgeTo2dNode(iVertical,iGroup2d,iElem2d,iNode2d);
        double height = elemHeight(iNode3d,0);
        for (int iField = 0; iField < nbFields; iField++) {
          int iData2d = dof2d->getDofId(iGroup2d,iElem2d,iField,iNode2d);
          double value = elemData(iNode3d,iField);
          dof2d->getVector()(iData2d) += height*value;
        }
      }
    }
  }
  if (scatterSolution)
    dof2d->scatter(true);
}

void slim3dDepthIntegrator::integrate3d3d(const dgDofContainer* dof, dgDofContainer* intDof)
{
  intDof->scale(0.0);
  int nbFields = std::min( dof->getNbFields(),intDof->getNbFields() );
  for (int iGroup = 0; iGroup < _groups3d->getNbElementGroups(); ++iGroup) {
    dgGroupOfElements* group = _groups3d->getElementGroup(iGroup);
    if (group->getDimUVW()!= 3 )
      Msg::Fatal("Cannot integrate DOFs: donor must be a 3D dgDofContainer");
    const dgFullMatrix<double> &valGroupData = dof->getGroupProxy(iGroup);
    const dgFullMatrix<double> &intValGroupData = intDof->getGroupProxy(iGroup);
    dgFullMatrix<double> &hGroupData = _nodalHeight->getGroupProxy(iGroup);
    int nbNodes = group->getNbNodes();
    for (size_t iElem = 0; iElem < group->getNbElements() ; ++iElem) {
      dgFullMatrix<double> elemData, elemIntData, elemHeight;
      valGroupData.getBlockProxy(iElem,elemData);
      intValGroupData.getBlockProxy(iElem,elemIntData);
      hGroupData.getBlockProxy(iElem,elemHeight);
      for (int iNode = 0; iNode < nbNodes/2; iNode++) {
        size_t iVertical=0, iLevel=0;
        _columns->mapDofToVEdgeDG(iGroup,iElem,iNode,iVertical,iLevel);
        double height = elemHeight(iNode,0);
        if (nbFields > 1)
          Msg::Fatal("_downValue is implemented for only one Field currently");
        for (int iField = 0; iField < nbFields; iField++) {
          /** Linear Approximation is done */
          elemIntData(iNode+nbNodes/2, iField) = -1/6.*(elemData(iNode+nbNodes/2,iField) - elemData(iNode,iField))*height;
          elemIntData(iNode, iField) = -1/6.*(7*elemData(iNode+nbNodes/2,iField) + 5*elemData(iNode,iField))*height;
          if (iLevel == 1) // Not done yet for not flat bottom!!!
            _downValue[iVertical] = -(elemData(iNode,iField) + elemData(iNode+nbNodes/2,iField))* height;
          else {
            elemIntData(iNode+nbNodes/2, iField) += _downValue[iVertical];
            elemIntData(iNode, iField) += _downValue[iVertical];
            _downValue[iVertical] += -(elemData(iNode,iField) + elemData(iNode+nbNodes/2,iField))* height;
          }
          /* interpolation */
          /*if (iLevel == 1) {
            elemIntData(iNode+nbNodes/2, iField) = 0;
            _downValue[iVertical] = - (elemData(iNode,iField) + elemData(iNode+nbNodes/2,iField))* height;
          }
          else {
            elemIntData(iNode+nbNodes/2, iField) = _downValue[iVertical];
            _downValue[iVertical] += - (elemData(iNode,iField) + elemData(iNode+nbNodes/2,iField))* height;
          }
          elemIntData(iNode, iField) = _downValue[iVertical];
          */
        }
      }
    }
  }
  intDof->scatter(true);
}

dgFieldCopy::dgFieldCopy(dgGroupCollection* groups)
{
  _groups = groups;
}

void dgFieldCopy::copy(const dgDofContainer* source, dgDofContainer* dest, int sourceField, int destField)
{
  int nbSourceFields = source->getNbFields();
  int nbDestFields = dest->getNbFields();
  if ( source->getGroups() != _groups || dest->getGroups() != _groups )
    Msg::Fatal("Cannot copy fields: dgDofContainer is built with a different dgGroupCollection");    
  if ( sourceField > nbSourceFields-1 || sourceField < 0 )
    Msg::Fatal("Source field index out of range (%i %i)", sourceField, nbSourceFields);
  if ( destField > nbDestFields-1 || destField < 0 )
    Msg::Fatal("Destination field index out of range (%i %i)", destField, nbDestFields);
  for (int iGroup = 0; iGroup < _groups->getNbElementGroups(); iGroup++) {
    const dgFullMatrix<double> &sourceData = source->getGroupProxy(iGroup);
    dgFullMatrix<double> &destData = dest->getGroupProxy(iGroup);
    int nbElems = sourceData.size2()/nbSourceFields;
    for (int i = 0; i < nbElems; i ++ ) {
      int sourceCol = i*nbSourceFields + sourceField;
      int destCol = i*nbDestFields + destField;
      for (int iNode = 0; iNode < sourceData.size1(); iNode++ ) {
        destData.set(iNode,destCol, sourceData.get(iNode,sourceCol) );
      }
    }
  }
}

dgSW3dDofCopy::dgSW3dDofCopy ( dgExtrusion* columns )
{
  _columns = columns;
  _groups3d = &columns->getGroups3d();
  _groups2d = &columns->getGroups2d();
}

void dgSW3dDofCopy::copyTo3D(dgDofContainer* dof2d, dgDofContainer* dof3d)
{
  if (dof2d->getGroups() != _groups2d ||
      dof3d->getGroups() != _groups3d )
    Msg::Fatal("Cannot copy DOFs: dgGroupCollection does not match the projector");
  if (dof2d->getNbFields() != dof3d->getNbFields())
    Msg::Fatal("Cannot copy DOFs: number of fields in donor and receiver must match");
  dof3d->scale(0.0);
  int nbFields = dof3d->getNbFields();
  for (int iGroup3d = 0; iGroup3d < _groups3d->getNbElementGroups(); iGroup3d++) {
    dgGroupOfElements* group3d = _groups3d->getElementGroup(iGroup3d);
    if (group3d->getDimUVW() != 3 )
      Msg::Fatal("Cannot copy DOFs: receiver must be a 3D dgDofContainer");
    for (size_t iElem3d = 0; iElem3d < group3d->getNbElements() ; iElem3d++) {
      MElement* elem3d = group3d->getElement(iElem3d);
      for (int iNode3d = 0; iNode3d < elem3d->getNumVertices(); iNode3d++) {
        size_t iVertical=0, iLevel=0;
        _columns->mapDofToVEdgeDG(iGroup3d,iElem3d,iNode3d,iVertical,iLevel);
        size_t iGroup2d=0, iElem2d=0, iNode2d=0;
        _columns->mapVEdgeTo2dNode(iVertical,iGroup2d,iElem2d,iNode2d);
        for (int iField = 0; iField < nbFields; iField++) {
          int iData2d = dof2d->getDofId(iGroup2d,iElem2d,iField,iNode2d);
          int iData3d = dof3d->getDofId(iGroup3d,iElem3d,iField,iNode3d);
          if (iData2d < 0 || iData2d >= dof2d->getVector().size()) {
            Msg::Warning("2d vector index out of bounds: %d, %d %d %d",iData2d,iGroup2d,iElem2d,iNode2d);
          }
          if (iData3d < 0 || iData3d >= dof3d->getVector().size())
            Msg::Warning("3d vector index out of bounds: %d, %d %d %d",iData3d,iGroup3d,iElem3d,iNode3d);
          dof3d->getVector()(iData3d) = dof2d->getVector()(iData2d);
        }
      }
    }
  }
}

void dgSW3dDofCopy::copyTo2D(const dgDofContainer* dof3d, dgDofContainer* dof2d, int level3d)
{
  if (dof2d->getGroups() != _groups2d ||
      dof3d->getGroups() != _groups3d )
    Msg::Fatal("Cannot copy DOFs: dgGroupCollection does not match the projector");
  if (dof2d->getNbFields() != dof3d->getNbFields())
    Msg::Fatal("Cannot copy DOFs: number of fields in donor and receiver must match");
  dof2d->scale(0.0);
  int nbFields = dof3d->getNbFields();
  for (int iGroup3d = 0; iGroup3d < _groups3d->getNbElementGroups(); iGroup3d++) {
    dgGroupOfElements* group3d = _groups3d->getElementGroup(iGroup3d);
    for (size_t iElem3d = 0; iElem3d < group3d->getNbElements() ; iElem3d++) {
      MElement* elem3d = group3d->getElement(iElem3d);
      for (int iNode3d = 0; iNode3d < elem3d->getNumVertices(); iNode3d++) {
        size_t iVertical=0, iLevel=0;
        _columns->mapDofToVEdgeDG(iGroup3d,iElem3d,iNode3d,iVertical,iLevel);
        size_t nbLevels = _columns->getNbDGPointsInVertical(iVertical);
        size_t targetLevel = level3d;
        if (level3d < 0)
          targetLevel = nbLevels + level3d;          
        if (iLevel != targetLevel)
          continue;
        size_t iGroup2d=0, iElem2d=0, iNode2d=0;
        _columns->mapVEdgeTo2dNode(iVertical,iGroup2d,iElem2d,iNode2d);
        for (int iField = 0; iField < nbFields; iField++) {
          int iData2d = dof2d->getDofId(iGroup2d,iElem2d,iField,iNode2d);
          int iData3d = dof3d->getDofId(iGroup3d,iElem3d,iField,iNode3d);
          dof2d->getVector()(iData2d) = dof3d->getVector()(iData3d);
        }
      }
    }
  }
}


void dgSW3dDofCopy::_gatherOverVertical(dgDofContainer* dof3d, dgDofContainer* dof2d, gather_operator op)
{
  if (dof2d->getGroups() != _groups2d ||
      dof3d->getGroups() != _groups3d )
    Msg::Fatal("Cannot copy DOFs: dgGroupCollection does not match the projector");
  if (dof2d->getNbFields() != dof3d->getNbFields())
    Msg::Fatal("Cannot copy DOFs: number of fields in donor and receiver must match");
  double initVal = 0.0;
  if ( op == MAX_OPERATION )
    initVal = -1e30;
  else if ( op == MIN_OPERATION )
    initVal = 1e30;
  dof2d->scale(initVal);
  int nbFields = dof3d->getNbFields();
  for (int iGroup3d = 0; iGroup3d < _groups3d->getNbElementGroups(); iGroup3d++) {
    dgGroupOfElements* group3d = _groups3d->getElementGroup(iGroup3d);
    for (size_t iElem3d = 0; iElem3d < group3d->getNbElements() ; iElem3d++) {
      MElement* elem3d = group3d->getElement(iElem3d);
      for (int iNode3d = 0; iNode3d < elem3d->getNumVertices(); iNode3d++) {
        size_t iVertical=0, iLevel=0;
        _columns->mapDofToVEdgeDG(iGroup3d,iElem3d,iNode3d,iVertical,iLevel);
        size_t iGroup2d=0, iElem2d=0, iNode2d=0;
        _columns->mapVEdgeTo2dNode(iVertical,iGroup2d,iElem2d,iNode2d);
        for (int iField = 0; iField < nbFields; iField++) {
          size_t iData2d = dof2d->getDofId(iGroup2d,iElem2d,iField,iNode2d);
          size_t iData3d = dof3d->getDofId(iGroup3d,iElem3d,iField,iNode3d);
          if ( op == MAX_OPERATION )
            dof2d->getVector()(iData2d) = std::max( dof2d->getVector()(iData2d), dof3d->getVector()(iData3d) );
          else if ( op == MIN_OPERATION )
            dof2d->getVector()(iData2d) = std::min( dof2d->getVector()(iData2d), dof3d->getVector()(iData3d) );
          else
            Msg::Fatal("Unknown gather operation: %d",op);
        }
      }
    }
  }
}

void dgSW3dDofCopy::maxOverVertical(dgDofContainer* dof3d, dgDofContainer* dof2d)
{
  _gatherOverVertical(dof3d,dof2d,MAX_OPERATION);
}

void dgSW3dDofCopy::minOverVertical(dgDofContainer* dof3d, dgDofContainer* dof2d)
{
  _gatherOverVertical(dof3d,dof2d,MIN_OPERATION);
}

void dgBathymetryDofGenerator::extractFromMesh(const dgExtrusion* cols, dgDofContainer* bath2dDof)
{
  dgGroupCollection& groups3d = cols->getGroups3d();
  bath2dDof->scale(0.0);
  for (int iGroup3d = 0; iGroup3d < groups3d.getNbElementGroups(); iGroup3d++) {
    dgGroupOfElements* group3d = groups3d.getElementGroup(iGroup3d);
    for (size_t iElem3d = 0; iElem3d < group3d->getNbElements() ; iElem3d++) {
      MElement* elem3d = group3d->getElement(iElem3d);
      for (int iNode3d = 0; iNode3d < elem3d->getNumVertices(); iNode3d++) {
        size_t iVertical=0, iLevel=0;
        cols->mapDofToVEdgeDG(iGroup3d,iElem3d,iNode3d,iVertical,iLevel);
        size_t targetLevel = 0;
        if (iLevel != targetLevel)
          continue;
        size_t iGroup2d=0, iElem2d=0, iNode2d=0;
        cols->mapVEdgeTo2dNode(iVertical,iGroup2d,iElem2d,iNode2d);
        size_t iField = 0;
        double zCoord = elem3d->getVertex(iNode3d)->z();
        size_t iData2d = bath2dDof->getDofId(iGroup2d,iElem2d,iField,iNode2d);
        bath2dDof->getVector()(iData2d) = -zCoord;
      }
    }
  }
}


dgSW3dMovingMesh::dgSW3dMovingMesh ( dgGroupCollection* groups )
{
 _groups = groups;
}

dgSW3dMovingMesh::~dgSW3dMovingMesh()
{ }

void dgSW3dMovingMesh::updateMesh ( const function* zCoordinates )
{
  // update coordinates
  dataCacheMap cacheMap(dataCacheMap::NODE_MODE, _groups);
  dataCacheDouble* zCache = cacheMap.get(zCoordinates,NULL);
  for (int iG = 0; iG < _groups->getNbElementGroups(); iG++) {
    dgGroupOfElements *group = _groups->getElementGroup(iG);
    size_t nbNodes = group->getNbNodes();
    cacheMap.setGroup(group);
    for (size_t iE =0; iE < group->getNbElements(); iE++) {
      size_t iElem = iE;
      cacheMap.setElement(iElem);
      MElement* elem = group->getElement(iElem);
      for (size_t iNode = 0; iNode < nbNodes; iNode++ ) {
        MVertex* v = elem->getShapeFunctionNode(iNode);
        v->z() = zCache->get()(iNode,0);
      }
    }
  }
  // recompute element data
//   for (size_t iG = 0 ; iG < _groups->getNbElementGroups(); iG ++)
//     _groups->getElementGroup(iG)->computeMatrices();
//   for (size_t iF = 0; iF < _groups->getNbFaceGroups(); iF++)
//     _groups->getFaceGroup(iF)->computeMatrices();
  
  dgDofContainer zCoordDof(*_groups,1);
  zCoordDof.interpolate(zCoordinates);
  _groups->updateCoordinates(&zCoordDof);
}

dgCGMeanFilter::dgCGMeanFilter(dgGroupCollection* groups)
{
  _groups = groups;
  //_CGStruct = new dgCGStructure(*_groups);
  _CGStruct = new dgHOMesh(*_groups);
  _nodalVolume = new dgDofContainer(*_groups,1);
  _nodalVolumeSummed = new dgDofContainer(*_groups,1);
  _sourceTimesVolume = new dgDofContainer(*_groups,1);
  _sourceTimesVolumeSummed = new dgDofContainer(*_groups,1);
  // compute weights
  for ( int iG = 0; iG < _groups->getNbElementGroups(); iG++ ) {
    dgGroupOfElements *group = _groups->getElementGroup(iG);
    const nodalBasis &fs = group->getFunctionSpace();
    const dgIntegrationMatrices &matrices = dgIntegrationMatrices::get(fs.type, fs.order * 2 + 1);
    const fullMatrix<double> &detJ = _groups->_mesh->getJacobian(fs.order * 2 + 1).detJElement(iG);
    const fullMatrix<double> &integration = matrices.integrationPoints();
    const fullMatrix<double> &psiW = matrices.psiW();
    int nbQP = integration.size1();
    dgFullMatrix<double> &groupData = _nodalVolume->getGroupProxy(iG);
    for ( size_t iE = 0; iE < _groups->getElementGroup(iG)->getNbElements(); iE++) {
      fullMatrix<double> elemData;
      groupData.getBlockProxy(iE,elemData);
      size_t nbNodes = _groups->getElementGroup(iG)->getNbNodes();
//       double weightSum = 0;
      for ( size_t iN = 0; iN < nbNodes; iN++ ) {
        elemData(iN,0) = 0;
        for ( int iQP = 0; iQP < nbQP; iQP++ )
          elemData(iN,0) += psiW(iN,iQP) * detJ(iQP, iE);
//         weightSum += elemData(iN,0);
      }
//       printf("vol: %e sum: %e diff:%e\n",e->getVolume(),weightSum,e->getVolume()-weightSum);
    }
  }
  _CGStruct->cgOperation(_nodalVolume, _nodalVolumeSummed, _CGStruct->SUM);
}

dgCGMeanFilter::~dgCGMeanFilter()
{
  delete _nodalVolume;
  delete _nodalVolumeSummed;
  delete _sourceTimesVolume;
  delete _sourceTimesVolumeSummed;
  delete _CGStruct;
}

void dgCGMeanFilter::apply(const dgDofContainer* source, dgDofContainer* destination)
{
  int nbFields = source->getNbFields();
  fullMatrix<double> dataSource, dataVolume, dataDest;
  /*
  for (int iGroup = 0; iGroup < _groups->getNbElementGroups(); iGroup++) {
    const dgGroupOfElements &group = *_groups->getElementGroup(iGroup);
    for (size_t iElement = 0; iElement < group.getNbElements(); ++iElement) {
      source->getGroupProxy(iGroup).getBlockProxy(iElement, dataSource);
      _nodalVolume->getGroupProxy(iGroup).getBlockProxy(iElement, dataVolume);
      _sourceTimesVolume->getGroupProxy(iGroup).getBlockProxy(iElement, dataDest);
      for(int k=0; k < nbFields; k++) {
        for(int i=0; i < group.getNbNodes(); i++) {
          dataDest(i, k) = dataSource(i, k) * dataVolume(i, 0);
        }
      }
    }
  }*/
  _sourceTimesVolume->multiply(*source,*_nodalVolume);
  //_sourceTimesVolume->scatter();
  _CGStruct->cgOperation(_sourceTimesVolume, destination, _CGStruct->SUM);
  
  for (int iGroup = 0; iGroup < _groups->getNbElementGroups(); iGroup++) {
    const dgGroupOfElements &group = *_groups->getElementGroup(iGroup);
    for (size_t iElement = 0; iElement < group.getNbElements(); ++iElement) {
      _nodalVolumeSummed->getGroupProxy(iGroup).getBlockProxy(iElement, dataVolume);
      destination->getGroupProxy(iGroup).getBlockProxy(iElement, dataDest);
      for(int k=0; k < nbFields; k++) {
        for(int i=0; i < group.getNbNodes(); i++) {
          dataDest(i, k) = dataDest(i, k) / dataVolume(i, 0);
        }
      }
    }
  }
  destination->scatter();
}



void dgSW3dDofCopy::computeArea2d(dgDofContainer* area){
  for (int iG = 0; iG < _groups2d->getNbElementGroups(); iG++) {
    dgGroupOfElements &group2d = *_groups2d->getElementGroup(iG);
    dgFullMatrix<double> &groupData = area->getGroupProxy(iG);
    for (size_t iElem2d = 0; iElem2d < group2d.getNbElements(); iElem2d++ ) {
      MElement &elem2d=*group2d.getElement(iElem2d);
      fullMatrix<double> elemData;
      groupData.getBlockProxy(iElem2d,elemData);
      size_t nbNodes = _groups2d->getElementGroup(iG)->getNbNodes();
      for ( size_t iN = 0; iN < nbNodes; iN++ ) {
        elemData(iN,0) = elem2d.getVolume();
      }
    }
  }
}
