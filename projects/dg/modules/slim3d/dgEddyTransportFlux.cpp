#include "dgEddyTransportFlux.h"
#include "function.h"
#include "float.h"
#include "dgGroupOfElements.h"

class dgEddyTransportFlux::gradPsiTerm: public function {
  fullMatrix<double> _dflux,_cst;
  
  public:
  gradPsiTerm (const function *cst):function(9){
    setArgument(_dflux,function::getSolutionGradient());
    setArgument(_cst,cst);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = _dflux.size1();
    for(size_t i=0; i< nQP; i++) {
      double fu = _dflux(i,2);
      double fv = _dflux(i,5);
      double fw = _dflux(i,8);
      
      // flux_x
      val(i,0) = 0;
      val(i,1) = 0;
      val(i,2) = 0;//
      // flux_y
      val(i,3) = 0;
      val(i,4) = 0;
      val(i,5) = 0;
      // flux_z
      val(i,6) = _cst(i,0)*_cst(i,0)*fu;
      val(i,7) = _cst(i,0)*_cst(i,0)*fv;
      val(i,8) = _cst(i,0)*_cst(i,0)*fw;
    }
  }
};

class dgEddyTransportFlux::interface0:public function {
  fullMatrix<double> _dfluxL, _dfluxR,_cst;
  fullMatrix<double> _normals;
  public:
  interface0 (const function* cst): function(6){
    setArgument(_dfluxL,function::getSolutionGradient(), 0);
    setArgument(_dfluxR,function::getSolutionGradient(), 1);
    setArgument(_normals,function::getNormals(), 0);
    setArgument(_cst,cst);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = _dfluxL.size1();
    for(size_t i=0; i< nQP; i++) {
      double nz = _normals(2,i);

      val(i,0) = -_cst(i,0)*_cst(i,0)*(_dfluxR(i,2)+_dfluxL(i,2))*nz/2;
      val(i,1) = -_cst(i,0)*_cst(i,0)*(_dfluxR(i,5)+_dfluxL(i,5))*nz/2;
      val(i,2) = -_cst(i,0)*_cst(i,0)*(_dfluxR(i,8)+_dfluxL(i,8))*nz/2;
      val(i,3) = -val(i,0);
      val(i,4) = -val(i,1);
      val(i,5) = -val(i,2);
    }
  }
};

class dgEddyTransportFlux::source: public function {
  fullMatrix<double> _flux, _Nsquare,_ind;
  public :
  source(const function *ind, const function *Nsquare) : function (3){
    setArgument(_flux,function::getSolution());
    setArgument(_Nsquare,Nsquare);
    setArgument(_ind,ind);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP =_flux.size1();
    for (size_t i=0; i<nQP; i++) {
      val (i,0) = _ind(i,0) + _Nsquare(i,0) * _flux(i,0);
      val (i,1) = _ind(i,1) + _Nsquare(i,0) * _flux(i,1);
      val (i,2) = _ind(i,2) + _Nsquare(i,0) * _flux(i,2);
    }
  }
};

void dgEddyTransportFlux::setup() {
  _volumeTerm1[""] = new gradPsiTerm(_cst);
  _volumeTerm0[""] = new source(_ind,_Nsquare);
  _interfaceTerm0[""] = new interface0(_cst);
}

dgEddyTransportFlux::dgEddyTransportFlux(const function* cst, const function* ind, const function* Nsquare): dgConservationLawFunction(3) {
   _cst=cst;
   _ind=ind;
   _Nsquare=Nsquare;
}

dgEddyTransportFlux::~dgEddyTransportFlux() {
    if (_volumeTerm1[""]) delete _volumeTerm1[""];
    if (_interfaceTerm0[""]) delete _interfaceTerm0[""];
    if (_volumeTerm0[""]) delete _volumeTerm0[""];
}
