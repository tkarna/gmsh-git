#ifndef SLIM3D_UTILS_H
#define SLIM3D_UTILS_H
#include "dgGroupOfElements.h"
#include "dgDofContainer.h"
//#include "dgCGStructure.h"
#include "dgHOMesh.h"

class dgExtrusion;

/** Class for integrating 3D data over the whole depth */
class slim3dDepthIntegrator {
  dgExtrusion *_columns;
  dgDofContainer *_nodalHeight;
  dgGroupCollection *_groups3d, *_groups2d;
  std::vector<double>_downValue;
  void _computeNodalHeight();
public:
  /** creates a new depth integrator for the given groups */
  slim3dDepthIntegrator( dgExtrusion* columns );
  ~slim3dDepthIntegrator();
  /** integrates the fields in sourceDof3d over the water depth */
  void integrate(const dgDofContainer *sourceDof3d, dgDofContainer *destDof2d, bool scatterSolution=true);
  /** integrates the fields in sourceDof over the water depth, from the bottom, and put the values in destDof(also 3D) */
  void integrate3d3d(const dgDofContainer *sourceDof, dgDofContainer *destDof);
  /** updates the depth integrator for new mesh */
  void update();
  dgDofContainer* getNodalHeights() { return _nodalHeight; };
};

/** Class for copying data from 2D mesh to 3D (prismatic) mesh.*/
class dgSW3dDofCopy {
  private:
    dgGroupCollection *_groups3d, *_groups2d;
    dgExtrusion* _columns;
    typedef enum {MIN_OPERATION, MAX_OPERATION} gather_operator;
    void _gatherOverVertical(dgDofContainer* dof3d, dgDofContainer* dof2d, gather_operator op);
  public:
    /** Creates a new copy method for given 3D columnInfo and 2D groups.*/
    dgSW3dDofCopy(dgExtrusion* columns);
    /** Copies nodal data in dof2d to all extruded nodes in dof3d.*/
    void copyTo3D(dgDofContainer* dof2d, dgDofContainer* dof3d);
    /** Copies nodal data of given level in dof3d to nodes in dof2d.
     *  Level = 0,1 stands for the bottom most nodes and first level above.
     *  Level = -1,-2 stands for the top most nodes and first level below.*/
    void copyTo2D(const dgDofContainer* dof3d, dgDofContainer* dof2d, int level3d);
    /** Finds minimal value over each vertical and stores it in the 2d dof */
    void minOverVertical(dgDofContainer* dof3d, dgDofContainer* dof2d);
    /** Finds maximal value over each vertical and stores it in the 2d dof */
    void maxOverVertical(dgDofContainer* dof3d, dgDofContainer* dof2d);
    void computeArea2d( dgDofContainer* area);
};

class dgBathymetryDofGenerator {
public:
  static void extractFromMesh(const dgExtrusion* cols, dgDofContainer* bath2dDof);
};


/** A class to convert data in 3D prismatic mesh *
 * Transforms dof to a mean value (P0) */
class dgPrismDofConverter {
  dgDofContainer* _nodalVolume;
public:
  /** create an instance, needs nodal volume dofcontainer */
  dgPrismDofConverter(dgDofContainer *nodalVolume) : _nodalVolume(nodalVolume) {};
  /** converts triangle dof to P0. source and dest can be the same */
  void convertToPZero2d(const dgDofContainer* source, dgDofContainer* dest);
  /** converts dof to P0. source and dest can be the same */
  void convertToPZero(const dgDofContainer* source, dgDofContainer* dest);
  /** converts dof to P0 only in horizontal. P1 prisms only. Source and dest can be the same */
  void convertToPZeroHorizontal(const dgDofContainer* source, dgDofContainer* dest);
  /** converts dof to P0 only in horizontal. P1 prisms only. Source is a function */
  void convertToPZeroHorizontal(const function* source, dgDofContainer* dest);
};

class dgFieldCopy {
  dgGroupCollection* _groups;
public:
  dgFieldCopy(dgGroupCollection *groups);
  void copy(const dgDofContainer* source, dgDofContainer* dest, int sourceField, int destField);
};

class dgCGMeanFilter {
  dgGroupCollection* _groups;
  //dgCGStructure* _CGStruct;
  dgHOMesh* _CGStruct;
  dgDofContainer *_nodalVolume, *_nodalVolumeSummed, *_sourceTimesVolume, *_sourceTimesVolumeSummed;
public:
  dgCGMeanFilter(dgGroupCollection* groups);
  ~dgCGMeanFilter();
  void apply(const dgDofContainer *source, dgDofContainer *destination);
  dgDofContainer* getWeights() { return _nodalVolume; };
};

/** Class to handle the vertical movement of prismatic 3D mesh.*/
class dgSW3dMovingMesh {
  private:
    dgGroupCollection* _groups;
  public:
    /** Builds instance for given groups.*/
    dgSW3dMovingMesh(dgGroupCollection* groups);
    ~dgSW3dMovingMesh();
    /** Updates the mesh. The z-coordinate of each node is replaced by the coordinate
     * given by the function. Note: zCoordinates function should be continuos in space */
    void updateMesh(const function* zCoordinates);
};

#endif
