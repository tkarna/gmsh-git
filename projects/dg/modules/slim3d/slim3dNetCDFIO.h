#ifndef SLIM_NETCDF_IO_H
#define SLIM_NETCDF_IO_H
#include "dgDofContainer.h"
#include "dgGroupOfElements.h"
#include "dgExtrusion.h"
#include "netcdfcpp.h"
#include "ncvalues.h"
#include "pnetcdf.h"
#include <iomanip>
#include <iostream>
#include <sstream>
//#if defined(HAVE_MPI)
#include <mpi.h>
//#endif

class mpiArrayStructure {
protected :
  const dgGroupCollection *_groups;
  int _totNodeCount, _totElemCount, _nodeCount, _elemCount;
  int _nodeOffset, _elemOffset, _maxElemNodes;
public :
  mpiArrayStructure(dgGroupCollection *groups);
  /* Returns number of nodes in current process */
  inline int getNbLocalNodes() { return _nodeCount; };
  /* Returns total number of nodes in all processes */
  inline int getNbGlobalNodes() { return _totNodeCount; };
  /* Returns number of elements in current process */
  inline int getNbLocalElems() { return _elemCount; };
  /* Returns total number of nodes in all processes */
  inline int getNbGlobalElems() { return _totElemCount; };
  /* Returns index of first local node in the global array */
  inline int getNodeOffset() { return _nodeOffset; };
  /* Returns index of first local element in the global array */
  inline int getElemOffset() { return _elemOffset; };
};

/** slimNetCDFIO class provides a read/write in parallel NetCDF format using the UGRID convention. Requires MPI and parallel netcdf. */
class slimNetCDFIO : mpiArrayStructure
{
friend class slim3dNetCDFIO;
private:
  const dgGroupCollection *_groups;
  /* arrays for netcdf write */
  std::vector<double> _xVec, _yVec, _zVec;
  std::vector<int> _connectivity;
protected:
  /* netcdf IDs */
  int _initYear, _initMonth, _initDay, _initHour, _initMinute, _initSecond;
  int _timeZoneHour, _timeZoneMinute;
  int _nodeDim, _faceDim, _faceNodeDim, _timeDim;
  int _nodeXVar, _nodeYVar, _nodeZVar, _connVar, _timeVar;
  void _define2dHeader(int pncfile);
  void _write2dHeader(int pncfile, double time);
public:
  slimNetCDFIO(dgGroupCollection *groups);
  /** Set initial time */
  inline void setInitTime(int year, int month, int day, int hour, int minute, int second, int tzHour=0, int tzMinute=0) {
    _initYear = year;
    _initMonth = month;
    _initDay = day;
    _initHour = hour;
    _initMinute = minute;
    _initSecond = second;
    _timeZoneHour = tzHour;
    _timeZoneMinute = tzMinute;
  };
  /** Export the dof for UGRID netcdf format */
  void exportUGRID(const dgDofContainer *dof, const std::string name, double time=0.0, int step=0, std::string fieldName="");
  /** Export the dof in UGRID netcdf format - only one COMP */
  void exportUGRIDSingleComp(const dgDofContainer *dof, int iComp, const std::string name, double time=0.0, int step=0, std::string fieldName="");
};

/** slim3dNetCDFIO uses dgExtrusion class to obtain vertical structure for 3d I/O files. */
class slim3dNetCDFIO
{
private:
  const dgExtrusion *_columns;
  slimNetCDFIO *_ncWriter2d;
public:
  slim3dNetCDFIO(dgExtrusion *columnInfo, slimNetCDFIO* ncWriter2d);
  /** Export the dof for UGRID netcdf format */
  void exportUGRID(const dgDofContainer *dof, const std::string name, double time=0.0, int step=0, std::string fieldName="");
  /** Export the dof in UGRID netcdf format - only one COMP */
  void exportUGRIDSingleComp(const dgDofContainer *dof, int iComp, const std::string name, double time=0.0, int step=0, std::string fieldName="");
};

#endif
