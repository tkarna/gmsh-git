#ifndef _DG_CONSERVATION_LAW_SW_3D_MOMENTUM_H
#define _DG_CONSERVATION_LAW_SW_3D_MOMENTUM_H
#include "dgConservationLawFunction.h"

/**Horizontal momentum equation for the 3D shallow water. (u, v) */
class dgConservationLawSW3dMomentum : public dgConservationLawFunction {
  class advection;
  class source;
  class riemann;
  class diffusiveFlux;
  class clipToPhysics;
  class boundaryWall;
  class boundaryOpenFree;
  class boundaryWallLinear;
  class boundarySurface;
  class maxConvectiveSpeed;
  class maxDiffusivity;
  const function *_uv2d, *_eta, *_r, *_rGrad, *_rhoSurf, *_w, *_wM, *_dwMdz, *_linearDissipation, *_quadraticDissipation, *_source, *_coriolisFactor, *_bathymetry;
  const function *_fzero, *_fzerov;
  const function *_nuH, *_nuV, *_diffusiveFlux, *_ipTerm, *_maxDiffusivity;
  double _laxFriedrichsFactor,_R;
  bool _useConservativeALE;
  public:
  class diffusivityDotN;
  void setup();
  /** set factor for scaling Lax-Friedrichs penalty term [default LFF = 0]. 
   * Larger value requires smaller explicit time step : CFL ~= 1/LFF *
   * Value LFF=1 corresponds to Aizinger and Dawson CMAME 2007. */
  inline void setLaxFriedrichsFactor(double lff){ _laxFriedrichsFactor = lff;};
  void setUseConservativeALE(bool b) { _useConservativeALE = b; };
  /**set eta function [eta] */
  inline void setEta(const function *eta){ _eta = eta;}
  /**set function for horizontal gradient of the baroclinic head [drdx drdy] */
  inline void setRGrad(const function *f){ _rGrad = f;}
  inline void setRhoSurf(const function *f){ _rhoSurf = f;}
  inline void setR(const function *f){ _r = f;}
  /**set depth averaged uv  function [uInt, vInt] */
  inline void setUV2d(const function *uv2d){ _uv2d = uv2d;}
  /**set vertical velocity function [w] */
  inline void setW(const function *w){ _w = w;}
  /**set vertical mesh velocity [wMesh] */
  inline void setWMesh(const function *wM){ _wM = wM;}
  /**set derivative of vertical mesh velocity [DwMesh/Dz] */
  inline void setWMeshDz(const function *dwMdz){ _dwMdz = dwMdz;}
  /**Set vertical viscosity function (scalar) */
  inline void setNuV(const function *nuV){ _nuV = nuV;}
  /**Set horizontal viscosity function (scalar) */
  inline void setNuH(const function *nuH){ _nuH = nuH;}
  /**set the function to evaluate the coriolis factor.
   * \f{eqnarray*}\frac{du}{dt} &=& -f v \\ \frac{dv}{dt} &=& f u \f} */
  inline void setCoriolisFactor(const function *coriolisFactor){ _coriolisFactor = coriolisFactor;}
  /**set the function to evaluate the linear dissipation.
   * \f{eqnarray*}\frac{du}{dt} &=& -\gamma u\\ \frac{dv}{dt} &=& -\gamma v \f} */
  inline void setLinearDissipation(const function *linearDissipation){ _linearDissipation = linearDissipation;}
  /**set the function to evaluate the quadratic dissipation.
   * \f{eqnarray*}\frac{du}{dt} &=& -c_d u||u||\\ \frac{dv}{dt} &=& -c_d v \f}*/
  inline void setQuadraticDissipation(const function *quadraticDissipation){ _quadraticDissipation = quadraticDissipation;}
  /**set the function to evaluate the source term.
   * \f{eqnarray*} \frac{du}{dt} &=& s(0)\\ \frac{dv}{dt} &=& s(1)\f} */
  inline void setSource(const function *source){ _source = source;}
  inline void setIsSpherical(double R){ _R = R;}
  /**set the function to evaluate the bathymetry h (H = h+eta) */
  inline void setBathymetry(const function *bathymetry) { _bathymetry = bathymetry;}
  dgConservationLawSW3dMomentum();
  ~dgConservationLawSW3dMomentum();
  /** slip wall boundary */
  dgBoundaryCondition *newBoundaryWall(const function* extDiffusiveFlux=NULL, const function* zBotDist=NULL);
  /** Lateral wall boundary condition with normal velocit penalty
   *  By default uvPenaltyFactor=0 */
  dgBoundaryCondition *newBoundaryWallLinear(double uvPenaltyFactor=0);
  /**bnd condition for free surface */
  dgBoundaryCondition *newBoundarySurface(const function* wind);
  /** open boundary condition, with etaOut = etaIn, and u = n*sqrt(gH) /H */
  dgBoundaryCondition *newBoundaryOpenFree(const function* extDiffusiveFlux=NULL);
};

/** Vertical advection and diffusion for 3d horizontal momentum equation for u or v.
 * Since the system is the same for u and v, they are solved separately. */
class dgConservationLawSW3dMomWx : public dgConservationLawFunction {
  class advection;
  class source;
  class interface0;
  class interfaceTerm;
  class diffusiveFlux;
  class clipToPhysics;
  class boundaryBottom;
  class boundarySurface;
  class maxConvectiveSpeed;
  class maxDiffusivity;
  const function *_fzero, *_fzerov;
  const function *_nuV, *_diffusiveFlux, *_ipTerm;
  // determines whether u or v is being computed
  int _comp;
  public:
  void setup();
  virtual bool isLinear() const {return true;}
  /**set which component to solve 0 for u 1 for v [0] */
  inline void setComponent(int c){ _comp = c;}
  /**Set vertical viscosity function (scalar) */
  inline void setNuV(const function *nuV){ _nuV = nuV;}
  dgConservationLawSW3dMomWx();
  ~dgConservationLawSW3dMomWx();
  /**bnd condition for wall
   * User may define a nonzero diffusive flux to create partial slip condition */
  dgBoundaryCondition *newBoundaryBottom(const function* extDiffusiveFlux = NULL,const function* zBotDist = NULL);
  /**bnd condition for free surface */
  dgBoundaryCondition *newBoundarySurface(const function* wind);
  dgFaceTermAssembler *getFaceTerm(const dgGroupOfFaces &group) const;
};

/**The non-conservative shallow water conservation law (external mode): elevation (eta) */
class dgConservationLawSW2dEta : public dgConservationLawFunction {
  class gradPsiTerm;
  class source;
  class riemann;
  class boundaryWall;
  class boundaryOpenFree;
  class maxConvectiveSpeed;
  class maxDiffusiveSpeed;
  class diffusiveFlux;
  bool _linear, _depthIntegrated;
  double _R;
  const function *_bathymetry, *_fzero, *_fzerov, *_UV, *_freshwaterFlux;
  public:
  void setup();
  /**if this flag is true, a linear version of the equations are solved.
   * no quadratic dissipation, no advection, no non-linear free surface term in mass concervation */
  inline void setIsLinear(bool linear) {_linear = linear;}
  /**if this flag is true, the horizontal velocity (u,v) is intepreted as depth integrated rather than depth averaged quantity. Default=false. */
  inline void setUsesDepthIntegratedUV(bool depInt) {_depthIntegrated = depInt;}
  /**set the function to evaluate the horizontal velocity (u, v) */
  inline void setUV(const function *UV) { _UV = UV;}
  inline void setIsSpherical(double R){ _R = R;}
  
  /**set the function to evaluate the bathymetry h (H = h+eta) */
  inline void setBathymetry(const function *bathymetry) { _bathymetry = bathymetry;}
  inline void setFreshwaterFlux(const function *freshwaterFlux) {_freshwaterFlux = freshwaterFlux;}
  dgConservationLawSW2dEta();
  ~dgConservationLawSW2dEta();
  /**slip wall boundary */
  dgBoundaryCondition *newBoundaryWall();
  dgBoundaryCondition *newBoundaryOpenFree();
};

/**The non-conservative shallow water conservation law (external mode): momentum (u, v) */
class dgConservationLawSW2dU : public dgConservationLawFunction {
  class gradPsiTerm;
  class source;
  class riemann;
//   class clipToPhysics;
  class boundaryWall;
  class boundaryOpenFree;
  class maxConvectiveSpeed;
  class maxDiffusiveSpeed;
  class diffusiveFlux;
  bool _linear;
  double _R;
  const function *_source, *_coriolisFactor, *_windStress, *_bathymetry, *_fzero, *_fzerov, *_nuH, *_diffusiveFlux, *_eta, *_ipTerm, *_bathymetryGradient, *_rGradInt, *_rhoSurf;
  const function *_bfCoeff, *_uvBottom;
  public:
  void setup();
  /**if this flag is true, a linear version of the equations are solved.
   * no quadratic dissipation, no advection, no non-linear free surface term in mass concervation */
  inline void setIsLinear(bool linear) {_linear = linear;}
  /**set the function to evaluate the coriolis factor.
   * \f{eqnarray*}\frac{du}{dt} &=& -f v \\ \frac{dv}{dt} &=& f u \f} */
  inline void setCoriolisFactor(const function *f){ _coriolisFactor = f;}
  /**set the function to evaluate the wind stress*/
  inline void setWindStress(const function *f){ _windStress = f;}
  /**set the function to evaluate the bottom friction drag coefficient.
   * The coefficient is computed in the 3D model and is needed for mode splitting.
   * \f{eqnarray*}\frac{du}{dt} &=& -c_d u_b||u_b||\\ \frac{dv}{dt} &=& -c_d v_b||u_b|| \f}*/
  inline void setBottomFrictionCoefficient(const function *f){ _bfCoeff = f;}
  /**set the function to evaluate the 3d velocity at the bottom.
   * The bottom velocity is computed in the 3D model and is needed for mode splitting.
   * \f{eqnarray*}\frac{du}{dt} &=& -c_d u_b||u_b||\\ \frac{dv}{dt} &=& -c_d v_b||u_b|| \f}*/
  inline void setBottomUV(const function *f){ _uvBottom = f;}
  /**set the function to evaluate the source term.
   * \f{eqnarray*} \frac{du}{dt} &=& s(0)\\ \frac{dv}{dt} &=& s(1)\f} */
  inline void setSource(const function *f){ _source = f;}
  /**Set vertical viscosity function (scalar) */
  inline void setNuH(const function *f){ _nuH = f;}
  /**set the function to evaluate the bathymetry h (H = h+eta) */
  inline void setBathymetry(const function *f) { _bathymetry = f;}
  /**set the function to evaluate the gradient of the bathymetry h (dhdx dhdy [dhdz]) */
  inline void setBathymetryGradient(const function *f) { _bathymetryGradient = f;}
  /**set the function to evaluate the free surface elevation (eta) */
  inline void setEta(const function *f) { _eta = f;}
  
  /**set the function to evaluate depth integrated gradient of baroclinic head ( intdrdx intdrdy) */
  inline void setRGradInt(const function *f) { _rGradInt = f;}
  inline void setRhoSurf(const function *f) { _rhoSurf = f;}
  inline void setIsSpherical(double R){ _R = R;}
  dgConservationLawSW2dU();
  ~dgConservationLawSW2dU();
  /**slip wall boundary */
  dgBoundaryCondition *newBoundaryWall(const function* rhoSurfExt=NULL);
  dgBoundaryCondition *newBoundaryOpenFree();
};
#endif
