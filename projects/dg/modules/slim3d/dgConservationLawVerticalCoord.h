#ifndef _DG_CONSERVATION_LAW_VERTICAL_COORD_H
#define _DG_CONSERVATION_LAW_VERTICAL_COORD_H
#include "dgConservationLawFunction.h"
class function;

/**Anisotropic diffusion of a pressure under a varying gravity for appropriate mesh generation in ocean modeling. 
 * see https://braque.mema.ucl.ac.be/svn/private/sam/gmsh/notes/verticalGrid.tex
 */
class dgConservationLawVerticalCoord : public dgConservationLawFunction {
  const function *_ipTermIso, *_n, *_diffusiveFlux, *_normalDiffTensor0, *_normalDiffTensor1, *_Lambda, *_lv, *_mu;
  const function *_fzero;
  class flux;
  class interfaceTerm;
  class diffusiveFlux;
  class normalDiffTensor1;
  class normalDiffTensor0;
  public:

  void setup();
  /**A new advection-diffusion law.
   * The horizontal advection velocity is given by 'uv' vector function while the vertical advection velocity is w.
   * Horizontal and vertical diffusivity are given by the functions nuH and nuV. */
  dgConservationLawVerticalCoord();
  inline void setIsoDiffFull(const function *n, const function *Lambda, const function *lv, const function *mu) {_n = n; _Lambda = Lambda; _lv = lv; _mu = mu;};
  ~dgConservationLawVerticalCoord();

};
#endif
