#ifndef SLIM_3D_FUNCTIONS_H
#define SLIM_3D_FUNCTIONS_H
#include "function.h"
#include "fullMatrix.h"
#include "nodalBasis.h"
#include "polynomialBasis.h"

class dgExtrusion;

/** A class containing all physical constants needed in 3d shallow water */
class slim3dParameters {
public:
  /** gravitational acceleration */
  static double g;
  /** Radius if stereo **/
  static double R;
  /** sea water density */
  static double rho0;
  /** von Karman parameter in bottom friction formulation */
  static double vonKarman;
  /** background (molecylar) viscosity */
  static double nuv0;
  /** background (molecylar) diffusivity of temperature */
  static double kapv0T;
  /** background (molecylar) diffusivity of salinity */
  static double kapv0S;
  /** specific heat */
  static double Cp;
  slim3dParameters() { };
};

/** Equation of State according to Jackett et al. 2006.
 * Salinity is expressed on the Practical Salinity Scale 1978 (PSS-78),
 * Th is potential temperature in Celsius, pressure p is in decibars (1 dbar = 1e4 Pa).
 * Th referenced to pressure p_r = 0 dbar,  all pressures are gauge pressures;
 * that is, they are the absolute pressures less 10.1325 dbar.
 * Last optional argument rho0 is for computing deviation rho' = rho(S,Th,p) - rho0.
 * */
double SW3dEquationOfStateJackett(double S, double Th, double p, double rho0=0);

/** Computes the density in function of pressure.
 * \f$rho\f$ = ratio of polynomials which are functions of \f$S, \theta, P\f$ */
class functionStateEquation : public function {
  fullMatrix<double> S,Th,p;
  double _rho0;
  public:
  functionStateEquation(const function * SFunction,const function * ThFunction,const function * pFunction, double rho0=0);
  void call (dataCacheMap *m, fullMatrix<double> &val);
};

/** Seawater thermal expansion coefficient according to Jackett et al. equation of state *
 * This is equvalent to \f[\frac {\partial \rho}{\partial T}\f] for constant \fS\f and \fp\f */
class functionStateEquationAlpha : public function {
  fullMatrix<double> S,Th,p;
  double _rho0;
  public:
  functionStateEquationAlpha(const function * SFunction,const function * ThFunction,const function * pFunction, double rho0=0);
  void call (dataCacheMap *m, fullMatrix<double> &val);
};

/** Seawater haline contraction coefficient according to Jackett et al. equation of state *
 * This is equvalent to \f[\frac {\partial \rho}{\partial S}\f] for constant \fT\f and \fp\f */
class functionStateEquationBeta : public function {
  fullMatrix<double> S,Th,p;
  double _rho0;
  public:
  functionStateEquationBeta(const function * SFunction,const function * ThFunction,const function * pFunction, double rho0=0);
  void call (dataCacheMap *m, fullMatrix<double> &val);
};

double potentialTemperature(double S, double Th, double p, double pr);
/** Computes the potential temperature (McDougall)**/
class functionPotentialTemperature : public function {
  fullMatrix<double> S,Th,p,pr;
  public:
  functionPotentialTemperature(const function * SFunction,const function * ThFunction,const function * pFunction,const function *prFunction);
  void call (dataCacheMap *m, fullMatrix<double> &val);
};

// Bottom friction stuff
/** Von Karman bottom Friction drag coefficient */
class bottomFrictionDragCoeffVonKarman : public function {
  fullMatrix<double> _bath, _zBot, _roughnessLength;
public:
  bottomFrictionDragCoeffVonKarman(const function *bath, const function *zBot, const function *roughnessLength): function(1) {
    setArgument(_bath,bath);
    setArgument(_zBot,zBot);
    setArgument(_roughnessLength,roughnessLength);
  };
  void call (dataCacheMap *m, fullMatrix<double> &sol ){
    double vonKarman = slim3dParameters::vonKarman;
    for (int i = 0; i < sol.size1(); ++i) {
      double sqrtCoeff = vonKarman / log( ( _bath(i,0) + _zBot(i,0) + _roughnessLength(i,0) ) / _roughnessLength(i,0) );
      sol.set(i, 0, sqrtCoeff*sqrtCoeff);
    }
  };
};

/** Distance to bottom from position where bottom friction velocity is defined.
  * Typically middle of the bottom most element. */
class zBotDist : public function {
  fullMatrix<double> _bath, _zBot;
public:
  zBotDist(const function *bath, const function *zBot) : function(1) {
    setArgument(_bath,bath);
    setArgument(_zBot,zBot);
  };
  void call (dataCacheMap *m, fullMatrix<double> &sol ){
    for (int i = 0; i < sol.size1(); i++) {
        sol.set(i,0, _bath(i,0) + _zBot(i,0));
    }
  };
};

/** The full drag coefficient */
/** This function is currently not used */
/*
class fullDragTerm : public function {
  fullMatrix<double> _bath, _zBot, _uvBot, _roughnessLength;
public:
  fullDragTerm (const function *bath, const function *zBot, const function *uvBot, const function *roughnessLength) : function(2){
    setArgument(_bath,bath);
    setArgument(_zBot,zBot);
    setArgument(_uvBot,uvBot);
    setArgument(_roughnessLength,roughnessLength);
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for (int i = 0; i< val.size1(); i++) {
      double factor = bfFactor(_bath(i,0),_zBot(i,0),_roughnessLength(i,0));
      double normUVBot = hypot( _uvBot(i,0) , _uvBot(i,1) );
      val.set(i,0, factor*factor*normUVBot*_uvBot(i,0) );
      val.set(i,1, factor*factor*normUVBot*_uvBot(i,1) );
    }
  }
};
*/

/** The drag coefficient for 2D (depth averaged) momentum.
  * Here, the stress is taken from the 3D mode, it does not depend on depth averaged uv,
  * so it appears as a source term. Note division by H and negative sign. */
/** This function is currently not used */
/*
class bottomFriction2dTerm : public function {
  fullMatrix<double> _bath, _eta, _zBot, _uvBot, _roughnessLength;
public:
  bottomFriction2dTerm (const function *bath, const function *eta, const function *zBot, const function *uvBot, const function *roughnessLength) : function(2) {
    setArgument(_bath,bath);
    setArgument(_eta,eta);
    setArgument(_zBot,zBot);
    setArgument(_uvBot,uvBot);
    setArgument(_roughnessLength,roughnessLength);
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for (int i = 0; i < val.size1(); i++) {
      double factor = bfFactor(_bath(i,0),_zBot(i,0),_roughnessLength(i,0));
      double normUVBot = hypot( _uvBot(i,0) , _uvBot(i,1) );
      double H = _bath(i,0) + _eta(i,0);
      val.set(i,0, - factor*factor*normUVBot*_uvBot(i,0)/H );
      val.set(i,1, - factor*factor*normUVBot*_uvBot(i,1)/H );
    }
  };
};
*/

/** The drag coefficient for 3D momentum.
  * norm(uv) is treated explicitly, but vertical diffusion is implicit */
class dragNormUb : public function {
  fullMatrix<double> _dragCoeff, _uvBot;
public:
  dragNormUb (const function *dragCoeff, const function *uvBot) :function(1) {
    setArgument(_dragCoeff,dragCoeff);
    setArgument(_uvBot,uvBot);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for (int i = 0; i < val.size1(); i++) {
      double normUVBot = hypot( _uvBot(i,0) , _uvBot(i,1) );
      val.set(i,0, _dragCoeff(i,0) * normUVBot );
    }
  }
};

/** The bottom velocity deviation from depth average. */
class bottomVelocityDeviation : public function {
  fullMatrix<double> _uvBot, _uvInt, _eta, _bath;
public:
  bottomVelocityDeviation (const function *bath, const function *eta, const function *uvBot2d, const function *uvInt2d) :function(2) {
    setArgument(_bath,bath);
    setArgument(_eta,eta);
    setArgument(_uvBot,uvBot2d);
    setArgument(_uvInt,uvInt2d);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for (int i = 0; i < val.size1(); i++) {
      double H = _eta(i,0) + _bath(i,0);
      val.set(i,0, _uvBot(i,0) - _uvInt(i,0)/H );
      val.set(i,1, _uvBot(i,1) - _uvInt(i,1)/H );
    }
  }
};

/** Bottom friction velocity.
  * Used by GOTM turbulence model. */
class uvBotStar : public function {
  fullMatrix<double> _dragCoeff, _uvBot;
public:
  uvBotStar (const function *dragCoeff, const function *uvBot) :function(2) {
    setArgument(_dragCoeff,dragCoeff);
    setArgument(_uvBot,uvBot);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for (int i = 0; i < val.size1(); i++) {
      val.set(i,0, sqrt(_dragCoeff(i,0)) * _uvBot(i,0) );
      val.set(i,1, sqrt(_dragCoeff(i,0)) * _uvBot(i,1) );
    }
  }
};

/** This is the r*=rInt2d/H^2 needed in 2d momentum. TEST. */
class rStarFunction : public function {
  fullMatrix<double> _rInt, _eta, _bath;
public:
  rStarFunction (const function *bath, const function *eta, const function *rInt) :function(1) {
    setArgument(_bath,bath);
    setArgument(_eta,eta);
    setArgument(_rInt,rInt);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for (int i = 0; i < val.size1(); i++) {
      double H = _eta(i,0) + _bath(i,0);
      val.set(i,0, _rInt(i,0)/H/H );
    }
  }
};

/** This is the depth averaged rGradInt needed in 2d momentum. TEST. */
class rGradAvFunction : public function {
  fullMatrix<double> _rGradInt, _eta, _bath;
public:
  rGradAvFunction (const function *bath, const function *eta, const function *rGradInt) :function(2) {
    setArgument(_bath,bath);
    setArgument(_eta,eta);
    setArgument(_rGradInt,rGradInt);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for (int i = 0; i < val.size1(); i++) {
      double H = _eta(i,0) + _bath(i,0);
      val.set(i,0, _rGradInt(i,0)/H );
      val.set(i,1, _rGradInt(i,1)/H );
    }
  }
};

// moving mesh stuff
/** Computes the vertical displacement of the mesh
  * Assuming uniform distribution over the vertical, i.e. top coinsides with eta,
  * bottom is static, linear combination in between. */
class zDisplace : public function {
  fullMatrix<double> _xyz0, _eta, _bath;
public:
  zDisplace (const function *xyz0, const function *eta, const function *bath) : function(1){
    setArgument(_xyz0,xyz0);
    setArgument(_eta,eta);
    setArgument(_bath,bath);
  };
  void call (dataCacheMap *m, fullMatrix<double> &sol) {
    for (int i = 0; i < sol.size1(); i++) {
      double movingMeshFactor = (_xyz0(i,2)+_bath(i,0))/(_bath(i,0));
      sol.set(i,0, _eta(i,0) * movingMeshFactor );
    }
  };
};

/** Computes new z coordinates with sigma-like linear distribution.
 *  The coordinates are conmputed with respect to xyz0 corresponding to eta=0 domain.*/
class zNew : public function {
  fullMatrix<double> _xyz0, _eta, _bath;
public:
  zNew (const function *xyz0, const function *eta, const function *bath) : function(1) {
    setArgument(_xyz0,xyz0);
    setArgument(_eta,eta);
    setArgument(_bath,bath);
  };
  void call (dataCacheMap *m, fullMatrix<double> &sol) {
    for (int i = 0; i < sol.size1(); i++) {
      double movingMeshFactor = (_xyz0(i,2)+_bath(i,0))/(_bath(i,0));
      sol.set(i,0, _xyz0(i,2) + _eta(i,0) * movingMeshFactor );
    }
  }
};

/** Computes the mesh velocity in the whole water column.
 *  Here the distribution is made with respect to the static coordinates xyz0. */
class wMesh : public function {
  fullMatrix<double> _xyz0, _eta, _bath, _wMeshSurf;
public:
  wMesh (const function *xyz0, const function *eta, const function *bath, const function *wMeshSurf) : function(1){
    setArgument(_xyz0,xyz0);
    setArgument(_eta,eta);
    setArgument(_bath,bath);
    setArgument(_wMeshSurf,wMeshSurf);
  };
  void call (dataCacheMap *m, fullMatrix<double> &sol) {
    for (int i = 0; i < sol.size1(); i++) {
      double movingMeshFactor = (_xyz0(i,2)+_bath(i,0))/(_bath(i,0));
      sol.set(i,0, _wMeshSurf(i,0) * movingMeshFactor );
    }
  };
};

/** Computes the vertical gradient of mesh velocity.
 *  The gradient is defined versus z coordinate, that changes is time,
 *  therefore h+eta is needed in the denominator. */
class dwMeshDz : public function {
  fullMatrix<double> _wMeshSurf, _eta, _bath;
public:
  dwMeshDz (const function *wMeshSurf, const function *eta, const function *bath) : function(1) {
    setArgument(_bath,bath);
    setArgument(_wMeshSurf,wMeshSurf);
    setArgument(_eta,eta);
  };
  void call (dataCacheMap *m, fullMatrix<double> &sol) {
    for (int i = 0; i < sol.size1(); i++) {
      sol.set(i,0, _wMeshSurf(i,0)/( _bath(i,0)+_eta(i,0) ) ); // eta is needed here!
    }
  };
};

/** Computes the vertical mesh velocity on the surface */
class wMeshSurf : public function {
  fullMatrix<double> _gradEta2d, _wSurf, _uvSurf;
public:
  wMeshSurf (const function *gradEta2d,const function *wSurf, const function *uvSurf) :function(1) {
    setArgument(_gradEta2d,gradEta2d);
    setArgument(_uvSurf,uvSurf);
    setArgument(_wSurf,wSurf);
  };
  void call (dataCacheMap *m, fullMatrix<double> &sol) {
    for (int i = 0; i < sol.size1(); i++) {
      double uDotGradEta = _uvSurf(i,0) * _gradEta2d(i,0) + _uvSurf(i,1) * _gradEta2d(i,1);
      sol.set(i,0, _wSurf(i,0) - uDotGradEta );
    }
  };
};

/** compute uv correction to so that the depth integral corresponds to uvIntTarget *
 * All functions must be defined in the 3D mesh                                    */
class uvCorrector : public function {
  fullMatrix<double> _uvInt, _uvIntTarget, _eta, _bath;
public:
  uvCorrector (const function *uvInt, const function *uvIntTarget, const function *eta, const function *bath) : function(2) {
    setArgument(_uvIntTarget,uvIntTarget);
    setArgument(_uvInt,uvInt);
    setArgument(_eta,eta);
    setArgument(_bath,bath);
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for (int i = 0; i < val.size1(); i++) {
      double d = _eta(i,0) + _bath(i,0);
//       val.set(i,0, _uvIntTarget(i,0)/d - _uvInt(i,0)/d );
//       val.set(i,1, _uvIntTarget(i,1)/d - _uvInt(i,1)/d );
      val.set(i,0, _uvIntTarget(i,0) - _uvInt(i,0)/d ); // uvTarget is depth average
      val.set(i,1, _uvIntTarget(i,1) - _uvInt(i,1)/d );
    }
  };
};

/** compute uv deviation from the depth integral *
 * All functions must be defined in the 3D mesh  */
class uvDeviation : public function {
  fullMatrix<double> _uv, _uvInt, _eta, _bath;
public:
  uvDeviation (const function *uv, const function *uvInt, const function *eta, const function *bath) : function(2) {
    setArgument(_uvInt,uvInt);
    setArgument(_uv,uv);
    setArgument(_eta,eta);
    setArgument(_bath,bath);
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for (int i = 0; i < val.size1(); i++) {
      double d = _eta(i,0) + _bath(i,0);
      val.set(i,0, _uv(i,0) - _uvInt(i,0)/d );
      val.set(i,1, _uv(i,1) - _uvInt(i,1)/d );
    }
  };
};

/** the uv deviation terms \tilde{u}^2, \tilde{u}\tilde{v}, \tilde{v}^2 *
 * All functions must be defined in the 3D mesh  */
class uvDeviationProducts : public function {
  fullMatrix<double> _uvDev;
public:
  uvDeviationProducts (const function *uvDev) : function(3) {
    setArgument(_uvDev,uvDev);
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for (int i = 0; i < val.size1(); i++) {
      val.set(i,0, _uvDev(i,0) *_uvDev(i,0) );
      val.set(i,1, _uvDev(i,0) *_uvDev(i,1) );
      val.set(i,2, _uvDev(i,1) *_uvDev(i,1) );
    }
  };
};

/** compute the advection term of \tilde{u}\tilde{\mathbf{u}} for the 2D mode *
 * All functions must be defined in the 2D mesh  */
class uvDevAdvectionTerm : public function {
  fullMatrix<double> _uvDevProdIntGrad, _eta, _bath;
public:
  uvDevAdvectionTerm (const function *uvDevProdIntGrad, const function *eta, const function *bath) : function(2) {
    setArgument(_uvDevProdIntGrad,uvDevProdIntGrad);
    setArgument(_eta,eta);
    setArgument(_bath,bath);
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for (int i = 0; i < val.size1(); i++) {
      double H2 = pow_int( _eta(i,0)+_bath(i,0) ,2);
      double duudx = _uvDevProdIntGrad(i,0);
      double duvdx = _uvDevProdIntGrad(i,3);
      double dvvdy = _uvDevProdIntGrad(i,7);
      double duvdy = _uvDevProdIntGrad(i,4);
      val.set(i,0, - duudx/H2 - duvdy/H2 );
      val.set(i,1, - duvdx/H2 - dvvdy/H2 );
    }
  };
};

/** convert depth averaged velocity to depth integrated velocity */
class uvAv2uvIntFunction : public function {
  fullMatrix<double> _uvAv2d, _eta, _bath;
public:
  uvAv2uvIntFunction (const function *uvAv2d, const function *eta, const function *bath) : function(2) {
    setArgument(_uvAv2d,uvAv2d);
    setArgument(_eta,eta);
    setArgument(_bath,bath);
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for (int i = 0; i < val.size1(); i++) {
      double d = _eta(i,0) + _bath(i,0);
      val.set(i,0, _uvAv2d(i,0)*d );
      val.set(i,1, _uvAv2d(i,1)*d );
    }
  };
};

/** hydrostatic pressure */
class hydrostatPressure : public function {
  fullMatrix<double> _xyz, _eta;
public:
  hydrostatPressure (const function *xyz, const function *eta) : function (1) {
    setArgument(_xyz,xyz);
    setArgument(_eta,eta);
  };
  void call (dataCacheMap *, fullMatrix<double> &val) {
    for (int i = 0; i < val.size1(); i++) {
      val.set(i,0,9.81*1027*(_eta(i,0)-_xyz(i,2))/1e4);
    }
  };
};

/** Smagorinsky horizontal viscosity/diffusivity for 3D fields */
class smagorinsky : public function {
  fullMatrix<double> _factor, _edgeLengthH, _uvGradient;
public:
smagorinsky(const function *factor, const function *edgeLengthH, const function *uvGradient) : function (1) {
    setArgument(_factor,factor);
    setArgument(_edgeLengthH,edgeLengthH);
    setArgument(_uvGradient,uvGradient);
  };
  void call (dataCacheMap *, fullMatrix<double> &val) {
    for (int i = 0; i < val.size1(); i++) {
      double dudx = _uvGradient(i, 0);
      double dudy = _uvGradient(i, 1);
      double dvdx = _uvGradient(i, 3);
      double dvdy = _uvGradient(i, 4);
      // TODO replace by 2D inradius
      double indiameter2D = 0.577*_edgeLengthH(i,0); // assuming equilateral triangle
      double Cs = _factor(i,0);
      val.set(i,0, pow_int(Cs/M_PI*indiameter2D, 2)*sqrt( pow_int(dudx-dvdy, 2) + pow_int(dudy+dvdx, 2) ) );
    }
  };
};


class etaGradRhoSurf : public function {
  fullMatrix<double> _etaGrad, _rhoSurf;
public:
etaGradRhoSurf(const function *etaGrad, const function *rhoSurf) : function (2) {
    setArgument(_etaGrad,etaGrad);
    setArgument(_rhoSurf,rhoSurf);
  };
  void call (dataCacheMap *, fullMatrix<double> &val) {
    double rho0 = slim3dParameters::rho0;
    for (int i = 0; i < val.size1(); i++) {
      val.set(i,0, _rhoSurf(i,0)/rho0 * _etaGrad(i,0) );
      val.set(i,1, _rhoSurf(i,0)/rho0 * _etaGrad(i,1) );
    }
  };
};

/** For obsolete pressure gradient formulation */
class divideByH : public function {
  fullMatrix<double> _f, _bath, _eta;
public:
  divideByH (const function *f, const function *bath, const function *eta) : function(f->getNbCol()) {
    setArgument(_f,f);
    setArgument(_bath,bath);
    setArgument(_eta,eta);
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for (int i = 0; i < val.size1(); i++) {
      double H = _eta(i,0) + _bath(i,0);
      for (int j = 0; j< val.size2(); j++) {
        val.set(i,j, _f(i,j)/H );
      }
    }
  };
};
class transformStereo2XYZ : public function {
  fullMatrix<double> _uv, _xyz;
  double _R;
public:
  transformStereo2XYZ (const function *uv, const function *xyz, double R) : function(3) {
    setArgument(_uv,uv);
    setArgument(_xyz,xyz);
    _R = R; 
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for (int i = 0; i < val.size1(); i++) {
      double xi = _xyz.get(i,0);
      double eta = _xyz.get(i,1);

      double vxi = _uv.get(i, 0);
      double veta = _uv.get(i, 1);
    
      double ux = (1-(2*xi*xi)/(4*_R*_R+xi*xi + eta*eta)) * vxi - 2*eta*xi/(4*_R*_R+xi*xi + eta*eta) * veta;
      double uy = - 2*eta*xi/(4*_R*_R+xi*xi + eta*eta) * vxi + (1-(2*eta*eta)/(4*_R*_R+xi*xi + eta*eta)) * veta;
      val.set(i,0,ux);
      val.set(i,1,uy);
    }
  };
};
class transformXYZ2Stereo : public function {
  fullMatrix<double> _uv, _xyz;
  double _R;
public:
  transformXYZ2Stereo (const function *uv, const function *xyz, double R) : function(3) {
    setArgument(_uv,uv);
    setArgument(_xyz,xyz);
    _R = R; 
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for (int i = 0; i < val.size1(); i++) {
      double xi = _xyz.get(i,0);
      double eta = _xyz.get(i,1);

      double x = 4*_R*_R*xi/(4*_R*_R + xi*xi + eta*eta);
      double y = 4*_R*_R*eta/(4*_R*_R + xi*xi + eta*eta);
      double z = _R *(4*_R*_R-xi*xi-eta*eta)/(4*_R*_R + xi*xi + eta*eta);
      
      double vx = _uv.get(i, 0);
      double vy = _uv.get(i, 1);
      double vz = _uv.get(i, 2);
    
      double vxi = vx - x/(_R+z)*vz;
      double veta = vy - y/(_R+z)*vz;
      val.set(i,0,vxi);
      val.set(i,1,veta);
    }
  };
};
class transformLonLat2Stereo : public function {
  fullMatrix<double> _uv, _xyz;
  double _R;
public:
  transformLonLat2Stereo (const function *uv, const function *xyz, double R) : function(3) {
    setArgument(_uv,uv);
    setArgument(_xyz,xyz);
    _R = R; 
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for (int i = 0; i < val.size1(); i++) {
      double xi = _xyz.get(i,0);
      double eta = _xyz.get(i,1);

      double x = 4*_R*_R*xi/(4*_R*_R + xi*xi + eta*eta);
      double y = 4*_R*_R*eta/(4*_R*_R + xi*xi + eta*eta);
      double z = _R *(4*_R*_R-xi*xi-eta*eta)/(4*_R*_R + xi*xi + eta*eta);
      double theta = asin(z/_R);
      double phi = atan2(y,x);
      
      double vlon = _uv.get(i, 0);
      double vlat = _uv.get(i, 1);
    
      double vxi = -sin(phi)*vlon - cos(phi)*(sin(theta) + cos(theta)/(1.0+sin(theta)))*vlat;
      double veta = cos(phi)*vlon - sin(phi)*(sin(theta) + cos(theta)/(1.0+sin(theta)))*vlat;
      val.set(i,0,vxi);
      val.set(i,1,veta);
    }
  };
};
class transformStereo2LonLat : public function {
  fullMatrix<double> _uv, _xyz;
  double _R;
public:
  transformStereo2LonLat (const function *uv, const function *xyz, double R) : function(3) {
    setArgument(_uv,uv);
    setArgument(_xyz,xyz);
    _R = R; 
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for (int i = 0; i < val.size1(); i++) {
      double xi = _xyz.get(i,0);
      double eta = _xyz.get(i,1);
      double x = 4*_R*_R*xi/(4*_R*_R + xi*xi + eta*eta);
      double y = 4*_R*_R*eta/(4*_R*_R + xi*xi + eta*eta);
      double z = _R *(4*_R*_R-xi*xi-eta*eta)/(4*_R*_R + xi*xi + eta*eta);
      double theta = asin(z/_R);
      double phi = atan2(y,x);
      double vxi = _uv.get(i, 0);
      double veta = _uv.get(i, 1);
    
      double ux = (1-(2*xi*xi)/(4*_R*_R+xi*xi + eta*eta)) * vxi - 2*eta*xi/(4*_R*_R+xi*xi + eta*eta) * veta;
      double uy = - 2*eta*xi/(4*_R*_R+xi*xi + eta*eta) * vxi + (1-(2*eta*eta)/(4*_R*_R+xi*xi + eta*eta)) * veta;
      double uz = -4*_R*xi/(4*_R*_R+xi*xi + eta*eta) * vxi - 4*_R*eta/(4*_R*_R+xi*xi + eta*eta) * veta;
      double vlon = -sin(theta)*cos(phi)*ux - sin(theta)*sin(phi)*uy + cos(theta)*uz;
      double vlat = -sin(phi)*ux + cos(phi)*uy;
    
      val.set(i,0,vlat);
      val.set(i,1,vlon);
    }
  };
};
class eadySpeed : public function {
  fullMatrix<double> _nb, _bath, _eta;
public:
  eadySpeed (const function *nb, const function *bath, const function *eta) : function(1) {
    setArgument(_nb,nb);
    setArgument(_bath,bath);
    setArgument(_eta,eta);
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for(int i = 0; i < val.size1(); i++) {
      double H = _eta(i,0) + _bath(i,0);
      val.set(i, 0, std::max(0.1, sqrt(std::max(0.0,_nb.get(i,0))) * H/M_PI));
    }
  };
};
class GMIndepTerm : public function {
  fullMatrix<double> _rhoGrad;
public:
  GMIndepTerm (const function *rhoGrad) : function(3) {
    setArgument(_rhoGrad,rhoGrad);
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    double g = slim3dParameters::g;
    double rho0 = slim3dParameters::rho0;
    for(int i = 0; i < val.size1(); i++){
      val.set(i, 0, _rhoGrad.get(i,0) * g / rho0);
      val.set(i, 1, _rhoGrad.get(i,1) * g / rho0);
      val.set(i, 2, 0);
    }
  };
};

class GMVelocity : public function {
  fullMatrix<double> _streamFuncGrad;
public:
  GMVelocity (const function *streamFuncGrad) : function(3) {
    setArgument(_streamFuncGrad,streamFuncGrad);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    for(int i = 0; i < val.size1(); i++) {
      val.set(i, 0, _streamFuncGrad.get(i,2) );
      val.set(i, 1, _streamFuncGrad.get(i,5) );
      val.set(i, 2, -_streamFuncGrad.get(i,0) - _streamFuncGrad.get(i,4) );
    }  
  };
};


class slopeTapering : public function {
  fullMatrix<double> _rhoGrad, _nb;
public:
  slopeTapering (const function *rhoGrad, const function *nb) : function(1) {
    setArgument(_rhoGrad, rhoGrad);
    setArgument(_nb, nb);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    double slopeLim = 0.01;
    double g = slim3dParameters::g;
    double rho0 = slim3dParameters::rho0;
    double Sx, Sy;
    for(int i = 0; i < val.size1(); i++) {
      double grz = -_nb.get(i,0) * rho0 / g;
      if (grz == 0) {
	Sx = slopeLim;
	Sy = slopeLim;
      }
      else {
	Sx = -_rhoGrad.get(i,0) / grz;
	Sy = -_rhoGrad.get(i,1) / grz;
      }
      val.set(i, 0, 100 * 0.5 * (1 + tanh( (4e-3 - sqrt(Sx*Sx+Sy*Sy)) / 1e-3) ));
    }  
  };
};

class slopeFunction : public function {
  fullMatrix<double> _rhoGrad, _nb;
public:
  slopeFunction (const function *rhoGrad, const function *nb) : function(2) {
    setArgument(_rhoGrad, rhoGrad);
    setArgument(_nb, nb);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    double slopeLim = 0.01;
    double g = slim3dParameters::g;
    double rho0 = slim3dParameters::rho0;
    double Sx, Sy;
    for(int i = 0; i < val.size1(); i++) {
      double grz = -_nb.get(i,0) * rho0 / g;
      if (grz == 0) {
	Sx = slopeLim;
	Sy = slopeLim;
      }
      else {
	Sx = -_rhoGrad.get(i,0) / grz;
	Sy = -_rhoGrad.get(i,1) / grz;
      }
      val.set(i, 0, Sx);
      val.set(i, 1, Sy);
    }  
  };
};

class kappavConvFunc : public function {
  fullMatrix<double> _nb;
public:
  kappavConvFunc (const function *nb) : function(1) {
    setArgument(_nb, nb);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    double g = slim3dParameters::g;
    double rho0 = slim3dParameters::rho0;
    for(int i = 0; i < val.size1(); i++) {
      double grz = -_nb.get(i,0) * rho0 / g;
      if (grz > 0)
	val.set(i, 0, 1);
      else
        val.set(i, 0, slim3dParameters::kapv0S);
    }  
  };
};

class domeSmagorinsky : public function {
  double _factor, _max;
  fullMatrix <double> _uvGrad;
  const dgExtrusion &_extrusion;
 public:
  domeSmagorinsky(double factor, double max, const function * uvGrad, const dgExtrusion &extrusion);
  void call(dataCacheMap *cmap, fullMatrix<double> &val);
};

class pacanowskiPhilander : public function {
  double _nuB, _nu0, _alpha;
  fullMatrix<double> _uvGrad, _rhoGrad;
 public :
  pacanowskiPhilander(const function *uvGrad, const function *rhoGrad, double nu0, double nuB, double alpha);
  void call(dataCacheMap *cmap, fullMatrix<double> &val);
};

class linearStateEquation : public function {
  double _alpha, _T0;
  fullMatrix<double> _T;
 public :
  linearStateEquation(const function *T, double alpha, double T0);
  void call(dataCacheMap *cmap, fullMatrix<double> &val);
};

class verticalVelocitySediment : public function {
  fullMatrix<double> _sed, _w, _S;
 public :
  verticalVelocitySediment(const function *sed, const function *w, const function *S);
  void call(dataCacheMap *cmap, fullMatrix<double> &val);
};

class sedimentBottomFlux : public function {
  fullMatrix<double> _sed, _uvBot, _wind, _bath, _sedGround, _S;
 public :
  sedimentBottomFlux(const function *sed, const function *uvBot, const function *wind, const function *bath, const function *sedGround, const function *S);
  void call(dataCacheMap *cmap, fullMatrix<double> &val);
};

#endif
