%module slim3d
%{
  #undef HAVE_DLOPEN
  #include "slim3dSolver.h"
  #include "slim3dEquations.h"
  #include "slim3dTimeIntegrator.h"
  #include "slim3dFunctions.h"
  #include "slim3dUtils.h"
  #include "dgConservationLawSW3dMomentum.h"
  #include "dgConservationLawSW3dContinuity.h"
  #include "dgConservationLawSW3dTracer.h"
  #include "dgConservationLawVerticalCoord.h"
  #include "dgSW3dVerticalModel.h"
  #include "dgEddyTransportFlux.h"
  #include "dgFEGradient.h"
  #include "slim3dNetCDFIO.h"
%}

%import(module="dgpy.dgCommon") "dgCommon.i"
%import(module="dgpy.dgFunction") "function.h";
%import(module="dgpy.conservationLaw") "dgConservationLaw.h";
%import(module="dgpy.conservationLaw") "dgConservationLawFunction.h";

%rename (_print) print;
%include "slim3dSolver.h"
%include "slim3dEquations.h"
%apply double &OUTPUT { double *volErr, double *SErr, double *TErr}
%apply double &OUTPUT { double *SDev, double *TDev}
%include "slim3dTimeIntegrator.h"
%include "slim3dFunctions.h"
%include "slim3dUtils.h"
%include "dgConservationLawSW3dMomentum.h"
%include "dgConservationLawSW3dContinuity.h"
%include "dgConservationLawSW3dTracer.h"
%include "dgConservationLawVerticalCoord.h"
%include "dgSW3dVerticalModel.h"
%include "dgEddyTransportFlux.h"
%include "dgFEGradient.h"
%include "slim3dNetCDFIO.h"
