#include "slim3dTimeIntegrator.h"
#include <iomanip>
#include <sstream>
#include <time.h>
#include "dgSW3dVerticalModel.h"
#include "functionGeneric.h"
#include "dgTimer.h"

slim3dTimeIntegrator::slim3dTimeIntegrator(slim3dSolver* solver) : _solver(solver)
{
  _time = _endTime = _initTime = 0;
  _exportCount = 0;
  _iter = 0;
  _exportInterval = _nextExportTime = -1;
  _fullExportInterval = _nextFullExportTime = -1;
  _fullExportIntervalRatio = -1;
  _fineExportStartIndex = _fineExportDuration = -1;
  _fineExportRatio = -1;
  _dt = -1;
  _n3d2d = -1;
  _exportEveryIter = false;
  _fullExportEveryIter =  false;
  _exploded = false;
  slim3dSolverEquations *e = solver->getEquations();
  uSys = NULL;
  SDiffSys = NULL;
  TDiffSys = NULL;
  sedDiffSys = NULL;
  SDiffNewton = NULL;
  TDiffNewton = NULL;
  sedDiffNewton = NULL;
  momUNewton = NULL;
  momVNewton = NULL;
  _volConservationChecker = _SConservationChecker = _TConservationChecker = NULL;
  _TDeviationChecker = _SDeviationChecker = _sedDeviationChecker =  NULL;
  if ( _solver->getSolveUVImplicitVerticalDiffusion() ) {
//     uSys = new dgLinearSystemExtrusion(e->vertMomUEq, groups3d, solver->columnInfo);
    uSys = new linearSystemPETSc<fullMatrix<double > >();
    dofManagerU = dgDofManager::newDGBlock(solver->groups3d, 1, uSys);
    momUNewton = new dgNewton(*dofManagerU, *e->vertMomUEq);
    momUNewton->setMaxIt(4);
    
    momVNewton = new dgNewton(*dofManagerU, *e->vertMomUEq);
    momVNewton->setMaxIt(4);
  }
  
  if ( _solver->getSolveSImplicitVerticalDiffusion() ) {
//     SDiffSys = new dgLinearSystemExtrusion(e->SDiffEq, groups3d, solver->columnInfo);
    SDiffSys = new linearSystemPETSc<fullMatrix<double> >();
    dofManagerSDiff = dgDofManager::newDGBlock (solver->groups3d, 1, SDiffSys);
    SDiffNewton = new dgNewton(*dofManagerSDiff, *e->SDiffEq);
    SDiffNewton->setAtol(1e-8);
    SDiffNewton->setRtol(1e-8);
    SDiffNewton->setMaxIt(4);
  }

  // TODO check if SDiffSys can be used instead
  if ( _solver->getSolveTImplicitVerticalDiffusion() ) {
//     TDiffSys = new dgLinearSystemExtrusion(e->TDiffEq, groups3d, solver->columnInfo);
    TDiffSys = new linearSystemPETSc<fullMatrix<double> >();
    dofManagerTDiff = dgDofManager::newDGBlock (solver->groups3d, 1, TDiffSys);
    TDiffNewton = new dgNewton(*dofManagerTDiff, *e->TDiffEq);
    TDiffNewton->setAtol(1e-8);
    TDiffNewton->setRtol(1e-8);
    TDiffNewton->setMaxIt(4);
  }
  
  if ( _solver->getSolveSedImplicitVerticalDiffusion() ) {
    sedDiffSys = new linearSystemPETSc<fullMatrix<double> >();
    dofManagerSedDiff = dgDofManager::newDGBlock (solver->groups3d, 1, sedDiffSys);
    sedDiffNewton = new dgNewton(*dofManagerSedDiff, *e->sedDiffEq);
    sedDiffNewton->setAtol(1e-8);
    sedDiffNewton->setRtol(1e-8);
    sedDiffNewton->setMaxIt(4);
  }
  
  if (_solver->getSolveGMVel()) {
    streamFuncSys = new linearSystemPETSc<double>();
    streamFuncSys->setParameter("petscOptions", "-pc_type lu");
    dofManagerStreamFunc = dgDofManager::newCG(solver->groups3d, 3, streamFuncSys);
    GMStreamFuncSolver = new dgSteady(*e->GMEq, *dofManagerStreamFunc);
  }
  else{
    streamFuncSys = NULL;
    GMStreamFuncSolver = NULL;
  }
  
}

slim3dTimeIntegrator::~slim3dTimeIntegrator()
{
  if ( uSys ) delete uSys;
  if ( SDiffSys ) delete SDiffSys;
  if ( TDiffSys ) delete TDiffSys;
  if ( sedDiffSys ) delete sedDiffSys;
  if ( streamFuncSys) delete streamFuncSys;
  if(momUNewton) delete momUNewton;
  if(momVNewton) delete momVNewton;
  if(SDiffNewton) delete SDiffNewton;
  if(TDiffNewton) delete TDiffNewton;
  if(sedDiffNewton) delete sedDiffNewton;
  if (_volConservationChecker) delete _volConservationChecker;
  if (_SConservationChecker)   delete _SConservationChecker;
  if (_TConservationChecker)   delete _TConservationChecker;
  if (_SDeviationChecker)      delete _SDeviationChecker;
  if (_TDeviationChecker)      delete _TDeviationChecker;
  if (_sedDeviationChecker)    delete _sedDeviationChecker;
}

void slim3dTimeIntegrator::checkParameters()
{
  if (_endTime <= 0)
    Msg::Fatal("endTime must be set");
  if (_endTime <= _initTime)
    Msg::Fatal("endTime must bigger than initTime");
  if (_exportInterval <= 0)
    Msg::Fatal("exportInterval must be set");
  if (_nextExportTime <= 0)
    Msg::Fatal("nextExportTime must be set");
  if (_dt <= 0)
    Msg::Fatal("timeStep must be set");
  if (_n3d2d <= 0)
    Msg::Fatal("3d time step ratio must be set");
  if ( _solver->getUseModeSplit() == false && _n3d2d != 1 ) {
    Msg::Warning("Mode split disabled -> setting M to 1");
    _n3d2d = 1;
  }
}


void slim3dTimeIntegrator::setExportInterval( double d )
{ 
  _exportInterval = d;
  _nextExportTime = d;
  if (_fullExportIntervalRatio > 0){
    _fullExportInterval = _fullExportIntervalRatio * d;
    _nextFullExportTime = _fullExportInterval;
  }
}

void slim3dTimeIntegrator::setFullExportIntervalRatio( int r )
{
  _fullExportIntervalRatio = r;
  if (_exportInterval > 0){
    _fullExportInterval = r * _exportInterval;
    _nextFullExportTime = _fullExportInterval;
  }
}

bool slim3dTimeIntegrator::checkFullExport()
{
  if (_fullExportIntervalRatio == -1) // will be the case if checkFullExports are not used
    return false;
  double EXPTOLERANCE = 1e-4;
  if (_time - _initTime >= _nextFullExportTime - EXPTOLERANCE || _fullExportEveryIter ) {
    _nextFullExportTime += _fullExportInterval;
    return true;
  }
  return false;
}

bool slim3dTimeIntegrator::checkExport()
{
  if (_exploded) {_exportCount++; return true;}
  double EXPTOLERANCE = 1e-4;
  double expIntervalFactor = 1.0;
  if ( _exportCount >= _fineExportStartIndex &&
       _exportCount <  _fineExportStartIndex + _fineExportDuration*_fineExportRatio )
    expIntervalFactor = 1.0/_fineExportRatio;
  if (_time - _initTime >= _nextExportTime - EXPTOLERANCE || _exportEveryIter ) {
    _nextExportTime += _exportInterval*expIntervalFactor;
    _exportCount++;
    return true;
  }
  return false;
}

void slim3dTimeIntegrator::checkSanity(bool only2d)
{
  double normEta = 0, normU = 0, normS = 0, normT = 0, normNuv = 0;
  if ( _solver->getDofs()->etaDof2d )
    normEta = _solver->getDofs()->etaDof2d->norm();
  if ( !only2d ) {
    if ( _solver->getDofs()->uvDof )
      normU = _solver->getDofs()->uvDof->norm();
    if ( _solver->getDofs()->SDof )
      normS = _solver->getDofs()->SDof->norm();
    if ( _solver->getDofs()->TDof )
      normT = _solver->getDofs()->TDof->norm();
    if ( _solver->getDofs()->nuvDof )
      normNuv = _solver->getDofs()->nuvDof->norm();
  }
  double maxNorm = 1e10;
  if ( (!(normEta<maxNorm)) || (!(normU<maxNorm)) || (!(normS<maxNorm)) || (!(normT<maxNorm)) || (!(normNuv<maxNorm)) ) {
    Msg::Info("Exploded ? eta:%d uv:%d S:%d T:%d nuv:%d", !(normEta<maxNorm),!(normU<maxNorm),!(normS<maxNorm),!(normT<maxNorm), !(normNuv<maxNorm));
    _exploded = true;
  }
}

void slim3dTimeIntegrator::_initializeFields()
{
  slim3dSolverFunctions *f = _solver->functions;
  slim3dSolverDofs *d = _solver->getDofs();
  // NOTE gotm must not be initialized too early FIXME
  //_solver->getEquations()->turbMod->initialize(); 
  d->xyzOrigDof->interpolate(f->xyzFunc3d);
  d->xyzOrigDof->scatter(true);
  if (d->SDof) {
    if (!f->SInitFunc)
      Msg::Fatal("an init function must be provided for the salinity");
    d->SDof->interpolate(f->SInitFunc);
    d->SDof->scatter(true);
  }
  if ( d->TDof) {
    if (!f->TInitFunc)
      Msg::Fatal("an init function must be provided for the temperature");
    d->TDof->interpolate(f->TInitFunc);
    d->TDof->scatter(true);
  }
  if ( d->sedDof) {
    if(f->sedInitFunc) {
      d->sedDof->interpolate(f->sedInitFunc);
      d->sedDof->scatter(true);
    }
    else {
      d->sedDof->setAll(0.);
    }
  }
  if (f->uvInitFunc) {
    d->uvDof->interpolate(f->uvInitFunc);
    d->uvDof->scatter(true);
  }
  else {
    d->uvDof->setAll(0.);
  }
  _solver->newDepthIntegrator->integrate(d->uvDof,d->uvIntDof2d);
  if (f->wInitFunc) {
    d->wDof3d->interpolate(f->wInitFunc);
    d->wDof3d->scatter(true);
  }
  else {
    d->wDof3d->setAll(0.);
  }
  if ( d->uvAvDof2d ) {
    if (f->uvAvInitFunc) {
      d->uvAvDof2d->interpolate(f->uvAvInitFunc);
      d->uvAvDof2d->scatter(true);
    }
    else {
      d->uvAvDof2d->setAll(0.);
    }
  }
  if(f->etaInitFunc) {
    d->etaDof2d->interpolate(f->etaInitFunc);
    d->etaDof2d->scatter(true);
  }
  else {
    d->etaDof2d->setAll(0.);
  }
  if (d->rhoDof3d)
    d->rhoDof3d->interpolate(f->rhoFunc);
}

void slim3dTimeIntegrator::terminate(int exitFlag)
{
  clock_t diffClock = clock() - _startClock;
  double cpuTime =  (double)diffClock / ((double)CLOCKS_PER_SEC);
  if ( Msg::GetCommRank() == 0 )
    printf("CPU time in main loop: %7.3f s\n",cpuTime);
  Msg::Exit(exitFlag);
}

void slim3dTimeIntegrator::eta2dDependencies()
{
  slim3dSolverDofs *d = _solver->getDofs();
  _solver->dgCG2d->apply(d->etaDof2d,d->etaDof2dCG);
}

void slim3dTimeIntegrator::etaDependencies(dgDofContainer *etaDof2d)
{
  slim3dSolver *s = _solver;
  slim3dSolverDofs *d = _solver->getDofs();
  slim3dSolverFunctions *f = _solver->functions;
  s->dgCG2d->apply(etaDof2d,d->etaDof2dCG);
  d->etaDof2dCG->scatter(true);

  // update mesh 
  if ( _solver->getFlagMovingMesh() ) {
    s->movingMesh->updateMesh(f->newZFunc); // expensive
    s->newDepthIntegrator->update();
    s->nodalVolume3d->update();
  }
  d->zDof->interpolate(f->newZFunc);
  if ( _solver->getComputeBottomFriction() ) {
    dgDofContainer tmpDof2d = dgDofContainer(*_solver->groups2d, 1);
    s->copy2d3d->copyTo2D(d->zDof,&tmpDof2d,1);
    s->copy2d3d->copyTo2D(d->zDof,d->zBotDof2d,0);
    double xi = s->bottomFrictionXi;
    d->zBotDof2d->scale(1-xi);
    d->zBotDof2d->axpy(tmpDof2d,xi);
    d->zBotDof2d->scatter(true);
    s->copy2d3d->copyTo3D(d->zBotDof2d,d->zBotDof3d);
    d->zBotDof3d->scatter(true);
  }
}

void slim3dTimeIntegrator::uvDependencies(dgDofContainer *uvIntTargetDof2d, bool hotStart)
{
  static dgTimer &timerUVdep = dgTimer::root().child("advance").child("3d").child("UV").child("UV dependencies");
  static dgTimer &timerw = timerUVdep.child("w");
  slim3dSolver *s = _solver;
  slim3dSolverDofs *d = _solver->getDofs();
  slim3dSolverFunctions *f = _solver->functions;
  slim3dSolverEquations *e = _solver->getEquations();

  s->newDepthIntegrator->integrate(d->uvDof,d->uvIntDof2d,false);
  if ( s->getSolve2d() && _solver->getUseModeSplit() ) {
    // uv correction
    uvCorrector uvCorr( d->uvIntDof2d->getFunction(), uvIntTargetDof2d->getFunction(), d->etaDof2dCG->getFunction(), d->bathDof2d->getFunction() );
    d->uvCorrDof3d->interpolate(&uvCorr,-1,NULL,false);
    if (!hotStart){
      d->uvDof->axpy(*d->uvCorrDof3d,1);
      d->uvDof->scatter(true);
    }
    s->newDepthIntegrator->integrate(d->uvDof,d->uvIntDof2d,false);
  }
  d->uvIntDof2d->scatter(true);
  // compute w
  if ( s->getSolveW() ) {
    timerw.start(true);
    #define NEW_W 0
    #if NEW_W
      e->uvHorDivEq->computeAllTerms(_time,*d->uvHorDivDof,*d->uvHorDivDof,false);
      d->uvHorDivDof->multiplyByInvMassMatrix();
      s->newDepthIntegrator->integrate3d3d(d->uvHorDivDof,d->wDof3d);
      d->wDof3d->scatter(true);
    #else
      s->wSolver->iterate(e->wEq,d->wDof3d, _time, true);
    #endif
    timerw.stop();
    
    //s->prismConverter->convertToPZero(d->wDof3d, d->wDof3d);
    s->copy2d3d->copyTo2D(d->wDof3d,d->wSurfDof2d,-1);
    d->wSurfDof2d->scatter(true);
    s->copy2d3d->copyTo2D(d->uvDof,d->uvSurfDof2d,-1);
    d->uvSurfDof2d->scatter(true);
  }
  if ( s->getFlagMovingMesh() ) {
    // compute wMesh
    d->wMeshSurfDof2d->interpolate(f->wMeshSurfFunc);
  }

  if ( s->getSolveUVGrad()){
    e->uvGradEq->computeAllTerms(_time, *d->uvGradDof, *d->uvGradDof, false);
    d->uvGradDof->multiplyByInvMassMatrix();
    s->newDepthIntegrator->integrate(d->uvGradDof, d->uvGradDof2d);
    d->uvGradDof2d->divide(*d->uvGradDof2d, *d->bathDof2d, 0);
  }

  if ( s->getComputeBottomFriction() || s->getSolveSed() ) {
    // bottom friction velocity
    double xi = s->bottomFrictionXi;
    dgDofContainer uvDofTemp2d(*s->groups2d,2);
    s->copy2d3d->copyTo2D(d->uvDof,&uvDofTemp2d,1);
    s->copy2d3d->copyTo2D(d->uvDof,d->uvBotDof2d,0);
    d->uvBotDof2d->scale(1-xi);
    d->uvBotDof2d->axpy(uvDofTemp2d,xi);
  }
  if ( s->getComputeBottomFriction() ) {
    if ( s->getSolveTurbulence() ) {
      d->uvTauBDof2d->L2Projection(f->uvTauBFunc2d);
    }
  }
}

void slim3dTimeIntegrator::uv2dDependencies() {
  slim3dSolver *s = _solver;
  slim3dSolverDofs *d = _solver->getDofs();
  slim3dSolverFunctions *f = _solver->functions;
  dgDofContainer uvDeviationProdDof3d(*_solver->groups3d,3);
  uvDeviationProdDof3d.interpolate(f->uvDevProdFunc);
  s->newDepthIntegrator->integrate(&uvDeviationProdDof3d,d->uvDeviationProdIntDof2d);  
}

void slim3dTimeIntegrator::rhoDependencies(bool compute2dForcing)
{
  slim3dSolver *s = _solver;
  slim3dSolverDofs *d = _solver->getDofs();
  slim3dSolverFunctions *f = _solver->functions;
  slim3dSolverEquations *e = _solver->getEquations();
 
  //if (true){
    d->rhoDof3d->interpolate(f->rhoFunc);
    d->rhoDof3d->scatter(true);
    s->copy2d3d->copyTo2D(d->rhoDof3d, d->rhoSurfDof2d,-1);
    d->rhoSurfDof2d->scatter(true);
    s->newRGradSolver->iterate(e->newRGradEq, d->rGradDof3d, true);
    if ( compute2dForcing )
      s->newDepthIntegrator->integrate(d->rGradDof3d, d->rGradIntDof2d);
  //}
  //else{ // old way (wrong)
  //  s->rSolver->iterate(e->depthIntegratorRho,d->rDof3d, _time, false);
  //  d->rDof3d->scale(-1./slim3dParameters::rho0);
  //  d->rDof3d->scatter(true);
  //  //d->rhoDof3d->interpolate(f->rhoFunc);
  //  e->rhoGradEq->computeAllTerms(_time,*d->rhoGradDof3d,*d->rhoGradDof3d);
  //  d->rhoGradDof3d->multiplyByInvMassMatrix();
  //  if ( compute2dForcing ) {
  //    s->newDepthIntegrator->integrate(d->rDof3d,d->rIntDof2d);
  //    s->copy2d3d->copyTo2D(d->rDof3d,d->rBotDof2d,0);
  //  }
  //} 
  if(s->getSolveGMVel() ) {
    dgDofContainer rhoGradDDof3d(*s->groups3d,3);
    e->rhoGradEq->computeAllTerms(_time,rhoGradDDof3d,rhoGradDDof3d);
    rhoGradDDof3d.multiplyByInvMassMatrix();
    e->rhoGradContProj = new dgL2ProjectionCG(&rhoGradDDof3d);
    e->rhoGradContProj->project(d->rhoGradGMVelDof3d,rhoGradDDof3d.getFunction());
    delete e->rhoGradContProj;
    dgDofContainer streamFuncDDof3d(*s->groups3d,3);
    GMStreamFuncSolver->solve(streamFuncDDof3d);
    e->streamFuncContProj = new dgL2ProjectionCG(&streamFuncDDof3d);
    e->streamFuncContProj->project(d->streamFuncDof3d,streamFuncDDof3d.getFunction());
    delete e->streamFuncContProj;
    d->GMVelDof3d->interpolate(f->GMVelFunc);
  }
}

slim3dTimeIntegratorPC::slim3dTimeIntegratorPC(slim3dSolver* solver) : slim3dTimeIntegrator(solver)
{
  dgGroupCollection *groups3d = _solver->groups3d;
  dgGroupCollection *groups2d = _solver->groups2d;
  uvDof_K = new dgDofContainer(*groups3d, 2);
  if ( solver->getAdvectTurbulentEnergy() ) {
    tkeDof_K = new dgDofContainer(*groups3d, 1);
    tkeDof_K1 = new dgDofContainer(*groups3d, 1);
    tkeDof_K2 = new dgDofContainer(*groups3d, 1);
    tkeDof_K3 = new dgDofContainer(*groups3d, 1);
    epsDof_K = new dgDofContainer(*groups3d, 1);
    epsDof_K1 = new dgDofContainer(*groups3d, 1);
    epsDof_K2 = new dgDofContainer(*groups3d, 1);
    epsDof_K3 = new dgDofContainer(*groups3d, 1);
  } else {
    tkeDof_K = tkeDof_K1 = tkeDof_K2 = tkeDof_K3 = NULL;
    epsDof_K = epsDof_K1 = epsDof_K2 = epsDof_K3 = NULL;
  }
  uvAvDof2d_K = new dgDofContainer(*groups2d, 2);
  uvAvDof2d_Kold = new dgDofContainer(*groups2d, 2);
  uvAvDof2d_Kold2 = new dgDofContainer(*groups2d, 2);
  etaDof2d_K = new dgDofContainer(*groups2d, 1);
  etaDof2d_Kold = new dgDofContainer(*groups2d, 1);
  etaDof2d_Kold2 = new dgDofContainer(*groups2d, 1);
  etaDof2d_new = new dgDofContainer(*groups2d, 1);
  //uKim = vKim = SKim = TKim = sedKim = NULL;
  uKim = momUNewton->getCurrentRHS();
  vKim = momVNewton->getCurrentRHS();
  SKim = SDiffNewton->getCurrentRHS();
  TKim = TDiffNewton->getCurrentRHS();
  sedKim = sedDiffNewton->getCurrentRHS();

  etaDof2d_tav_new = new dgDofContainer(*groups2d, 1);
  uvAvDof2d_tav_new = new dgDofContainer(*groups2d, 2);
  uvIntDof2d_tav_new = new dgDofContainer(*groups2d, 2);
  uvIntDof2d_tav_newHalf = new dgDofContainer(*groups2d, 2);
  uvDof_new = new dgDofContainer(*groups3d, 2);
  uvDof_old  = new dgDofContainer(*groups3d, 2);
  // TODO check bools
  if ( solver->getSolveS() || solver->getSolveSImplicitVerticalDiffusion() ) {
    SDof_K = new dgDofContainer(*groups3d, 1);
    SDof_new = new dgDofContainer(*groups3d, 1);
    SDof_old  = new dgDofContainer(*groups3d, 1);
  } else {
    SDof_K = SDof_new = SDof_old = NULL;
  }
  if ( solver->getSolveT() || solver->getSolveTImplicitVerticalDiffusion() ) {
    TDof_K = new dgDofContainer(*groups3d, 1);
    TDof_new = new dgDofContainer(*groups3d, 1);
    TDof_old  = new dgDofContainer(*groups3d, 1);
  } else {
    TDof_K = TDof_new = TDof_old = NULL;
  }
  if ( solver->getSolveSed() || solver->getSolveSedImplicitVerticalDiffusion() ) {
    sedDof_K = new dgDofContainer(*groups3d, 1);
    sedDof_new = new dgDofContainer(*groups3d, 1);
    sedDof_old  = new dgDofContainer(*groups3d, 1);
    sedGroundDof_new = new dgDofContainer(*groups2d, 1);
    sedGroundDof_old  = new dgDofContainer(*groups2d, 1);
  } else {
    sedDof_K = sedDof_new = sedDof_old = NULL;
    sedGroundDof_new = sedGroundDof_old = NULL;
  }
  //uvAvDof2d_old2 = new dgDofContainer(*groups2d, 2);
  //uvAvDof2d_old = new dgDofContainer(*groups2d, 2);
  //uvAvDof2d_cur = new dgDofContainer(*groups2d, 2);
  //etaDof2d_old2 = new dgDofContainer(*groups2d, 1);
  //etaDof2d_old = new dgDofContainer(*groups2d, 1);
  //etaDof2d_cur = new dgDofContainer(*groups2d, 1);
  uvIntDof2d_old = new dgDofContainer(*groups2d, 2);
  // time stepper parameters
  betaAB3 = 5./12;
  gammaLFAM3 = 1./12.;
  CrNiTheta = 0.6;
  _started = false;
  _initializeFields();
}

slim3dTimeIntegratorPC::~slim3dTimeIntegratorPC()
{
  delete uvDof_K;
  delete SDof_K;
  delete TDof_K;
  delete sedDof_K;
  delete tkeDof_K;
  delete tkeDof_K1;
  delete tkeDof_K2;
  delete tkeDof_K3;
  delete epsDof_K;
  delete epsDof_K1;
  delete epsDof_K2;
  delete epsDof_K3;
  delete uvAvDof2d_K;
  delete uvAvDof2d_Kold;
  delete uvAvDof2d_Kold2;
  delete etaDof2d_K;
  delete etaDof2d_Kold;
  delete etaDof2d_Kold2;

  delete etaDof2d_tav_new;
  delete uvAvDof2d_tav_new;
  delete uvIntDof2d_tav_new;
  delete uvIntDof2d_tav_newHalf;
  delete uvDof_new;
  delete uvDof_old;
  delete SDof_new;
  delete SDof_old;
  delete TDof_new;
  delete TDof_old;
  delete sedDof_new;
  delete sedDof_old;
  delete sedGroundDof_new;
  delete sedGroundDof_old;

  //delete uvAvDof2d_old2;
  //delete uvAvDof2d_old;
  //delete uvAvDof2d_cur;
  //delete etaDof2d_old2;
  //delete etaDof2d_old;
  //delete etaDof2d_cur;
  delete uvIntDof2d_old;
}

void slim3dTimeIntegratorPC::_initializeIterator()
{
  if (_fullExportIntervalRatio > 0)
    initFullExporter(_solver->exportDirectory);
  createTemporalWeights();
  slim3dSolverDofs* d = _solver->getDofs();
  slim3dSolverFunctions* f = _solver->functions;
  etaDof2d_new->copy(*d->etaDof2d);
  etaDof2d_tav_new->copy(*d->etaDof2d);
  uvAvDof2d_tav_new->copy(*d->uvAvDof2d);
  // TODO generalize to uvIntFrom2dFunc ???
  uvAv2uvIntFunction uvIntFunc(d->uvAvDof2d->getFunction(),d->etaDof2d->getFunction(),f->bathFunc2d);
  uvIntDof2d_tav_new->interpolate((function*)&uvIntFunc);
  eta2dDependencies();
  etaDependencies(d->etaDof2d);
  rhoDependencies();
//   uvDependencies(d->uvAvDof2d);
  //_solver->exporter->exportAll(_iter,_time - _initTime,_dt);  ENLEVER
  uvDof_old->copy(*d->uvDof);
  uvDof_new->copy(*d->uvDof);
  if ( _solver->getSolveS() || _solver->getSolveSImplicitVerticalDiffusion() ) {
    SDof_old->copy(*d->SDof);
    SDof_new->copy(*d->SDof);
  }
  if ( _solver->getSolveT() || _solver->getSolveTImplicitVerticalDiffusion() ) {
    TDof_old->copy(*d->TDof);
    TDof_new->copy(*d->TDof);
  }
  if ( _solver->getSolveSed() || _solver->getSolveSedImplicitVerticalDiffusion() ) {
    sedDof_old->copy(*d->sedDof);
    sedDof_new->copy(*d->sedDof);
    sedGroundDof_old->copy(*d->sedGroundDof2d);
    sedGroundDof_new->copy(*d->sedGroundDof2d);
  }
  checkParameters();
}

void slim3dTimeIntegratorPC::iterate(bool exitWhenDone)
{
  while ( _time < _endTime ) {
    advanceOneTimeStep();
    checkSanity();
  }
  if (exitWhenDone)
    terminate(0);
}

void slim3dTimeIntegratorPC::scalingTest()
{
  static dgTimer &timerMethod1 = dgTimer::root().child("globalmethod");
  static dgTimer &timerMethod2 = dgTimer::root().child("localmethod");
  dgDofContainer *dof1 = new dgDofContainer(*_solver->groups3d,1);
  dgDofContainer *dof2 = new dgDofContainer(*_solver->groups3d,1);
  // init dofs with random data
  dof1->setAll(1.0);
  srand(2.0);
  for (int i=0; i<dof1->getVector().size(); i++)
    dof1->getVector().set(i, 10*static_cast<float>(rand())/static_cast<float>(RAND_MAX)-5 );
  printf("Initialized dof: %.4f\n",dof1->norm());
  timerMethod1.start();
  // apply operations on full dofContainer
  for (int j=0; j<3000; j++ ) {
    for (int i = 0; i < 10; i++) {
      dof2->copy(*dof1);
      dof2->scale(1.20);
      dof2->axpy(*dof1,1.45);
    }
    dof2->scatter();
  }
  timerMethod1.stop();

  timerMethod2.start();
  // apply operations on element proxy
  for (int j=0; j<3000; j++ ) {
    for (int iG=0; iG<_solver->groups3d->getNbElementGroups(); iG++) {
      dgFullMatrix<double> *groupData1 = &dof1->getGroupProxy(iG);
      dgFullMatrix<double> *groupData2 = &dof2->getGroupProxy(iG);
      for (int i = 0; i < 10; i++) {
        groupData2->setAll(*groupData1);
        groupData2->scale(1.20);
        groupData2->axpy(*groupData1,1.45);
      }
    }
    dof2->scatter();
  }
  timerMethod2.stop();
  delete dof1;
  delete dof2;
}

void slim3dTimeIntegratorPC::advanceOneTimeStep()
{
  if (!_started) {
    _startClock = clock();
    _started = true;
    _initializeIterator();
  }
  static dgTimer &timer = dgTimer::root().child("advance");
  timer.start();
  static dgTimer &timer2d = timer.child("2d");
  static dgTimer &timer3d = timer.child("3d");
  static dgTimer &timerUV = timer3d.child("UV");
  static dgTimer &timerUVimp = timerUV.child("UVimp");
  static dgTimer &timerUVdep = timerUV.child("UV dependencies");
  static dgTimer &timerS = timer3d.child("S");
  static dgTimer &timerSimp = timerS.child("Simp");
  static dgTimer &timerT = timer3d.child("T");
  static dgTimer &timerTimp = timerT.child("Timp");
  static dgTimer &timerSed = timer3d.child("Sed");
  static dgTimer &timerSedimp = timerSed.child("Sedimp");
  static dgTimer &timerRhodep = timer3d.child("rho dependencies");
  static dgTimer &timergotm = timer.child("gotm");
  slim3dSolver *s = _solver;
  slim3dSolverDofs *d = _solver->getDofs();
  slim3dSolverEquations *e = _solver->getEquations();
  if (_exploded) terminate(-1);
  // Predictor-Corrector time stepper
  // 
  // ---|------|------|------|---
  //   n-1/2   n    n+1/2   n+1
  //    \____________/
  // 1) Prediction step
  //    All variables are at n (n-1 also known)
  //    - interpolate 3d variables (uv,T,S) to n-1/2
  //    - advance uv, T, S from n-1/2 to n+1/2 with RHS at n
  //    - compute rho and uv depended variables at n+1/2
  //    - correct uv depth average to match uv2d^{n} (because never is not yet available)
  //    - advance 2d (eta, uv2d) from n to n+1
  //      - store temporal averages for eta,uv2d at n+1 and uv2d at n+1/2
  //    - update geometry to match eta n+1
  // ---|------|------|------|---
  //   n-1/2   n    n+1/2   n+1
  //           \____________/
  // 2) Correction step
  //    - recompute uv, rho dependent variables to match new geomoetry
  //    - correct uv depth average to match uv2d^{n+1/2}
  //    - advance uv from n to n+1 with RHS at n+1/2
  //    - update uv dependent variables
  //    - correct uv to match uv2d^{n+1}
  //    - interpolate uv back to n+1/2, correct to match uv2d^{n+1/2}
  //    - advance T,S from n to n+1
  //    - compute rho depentent variables
  //    - interpolate T,S back to n+1/2 also
  //
  //     n-1/2 -> oldHalf
  //     n     -> old
  //     n+1/2 -> newHalf
  //     n+1   -> new
  double DT = _dt*_n3d2d;
  _time = _initTime + _iter*DT;
  timer3d.start();
  timerUV.start();
  /** 3D prection step : uv **/
  //   UV,S,T from n-1/2 to n+1/2 with rhs at n
  //   do *not* compute viscous terms yet (only pressure grad, only adv, cori)
  e->horMomEq->setUseConservativeALE(false);
  e->horMomEq->computeAllTerms(_time,*d->uvDof,*uvDof_K,false); // ***
  uvDof_K->multiplyByInvMassMatrix();
  if ( _solver->getUseConservativeALE() ) e->horMomEq->setUseConservativeALE(true);
  d->uvDof->copy(*uvDof_old);
  d->uvDof->scale(0.5-2*gammaLFAM3);
  d->uvDof->axpy(*uvDof_new,0.5+2*gammaLFAM3);
  d->uvDof->axpy(*uvDof_K,DT*(1-2*gammaLFAM3)); // from n-1/2+2*gamma to n+1/2
  if (s->getFlagUVLimiter() && !s->getUseConservativeALE()) {
    d->uvDof->scatter(true);
    s->uvLimiter->apply(d->uvDof);
  } else {
    d->uvDof->scatter(true); // now we are at step newHalf
  }

  // correct depth average to to uv2d^n ( n+1/2 is not yet available, so use n )
  timerUVdep.start();
  uvDependencies(uvIntDof2d_tav_new);
  timerUVdep.stop();
  timerUV.stop();

  /** 3D prection step : T,S **/
  timerS.start();
  if ( _solver->getSolveS() ) {
    // salinity explicit
    e->SEq->setUseConservativeALE(false);
    e->SEq->computeAllTerms(_time, *d->SDof, *SDof_K, false);
    SDof_K->multiplyByInvMassMatrix();
    if ( _solver->getUseConservativeALE() ) e->SEq->setUseConservativeALE(true);
    d->SDof->copy(*SDof_old);
    d->SDof->scale(0.5-2*gammaLFAM3);
    d->SDof->axpy(*SDof_new,0.5+2*gammaLFAM3);
    d->SDof->axpy(*SDof_K,DT*(1-2*gammaLFAM3)); // from from n-1/2+2*gamma to n+1/2
    if (s->getFlagSLimiter() && !s->getUseConservativeALE()) {
      d->SDof->scatter(true);
      s->SLimiter->apply(d->SDof);
    } else {
      d->SDof->scatter(true);
    }
  }
  timerS.stop();
  timerT.start();
  if ( _solver->getSolveT() ) {
    // salinity explicit
    e->TEq->setUseConservativeALE(false);
    e->TEq->computeAllTerms(_time,*d->TDof,*TDof_K,false);
    TDof_K->multiplyByInvMassMatrix();
    if ( _solver->getUseConservativeALE() ) e->TEq->setUseConservativeALE(true);
    d->TDof->copy(*TDof_old);
    d->TDof->scale(0.5-2*gammaLFAM3);
    d->TDof->axpy(*TDof_new,0.5+2*gammaLFAM3);
    d->TDof->axpy(*TDof_K,DT*(1-2*gammaLFAM3)); // from from n-1/2+2*gamma to n+1/2
    if (s->getFlagTLimiter() && !s->getUseConservativeALE()) {
      d->TDof->scatter(true);
      s->TLimiter->apply(d->TDof);
    } else {
      d->TDof->scatter(true);
    }
  }
  timerT.stop();
  timerRhodep.start();
  rhoDependencies(true); // rho dependencies at n+1/2
  timerRhodep.stop();
  timerSed.start(); 
  // begin Sediment module
  if ( _solver->getSolveSed() ) {
    s->copy2d3d->copyTo2D(d->sedDof,d->sedBottomDof2d,0);
    d->sedBottomFluxDof2d->interpolate(_solver->functions->sedBottomFluxFunc2d, _time);
    d->sedGroundDof2d->copy(*sedGroundDof_old);
    d->sedGroundDof2d->scale(0.5-2*gammaLFAM3);
    d->sedGroundDof2d->axpy(*sedGroundDof_new,0.5+2*gammaLFAM3);
    d->sedGroundDof2d->axpy(*d->sedBottomFluxDof2d, - DT*(1-2*gammaLFAM3)); // from from n-1/2+2*gamma to n+1/2
    d->sedGroundDof2d->scatter(true);
    e->sedEq->setUseConservativeALE(false);
    e->sedEq->computeAllTerms(_time,*d->sedDof,*sedDof_K,false);
    sedDof_K->multiplyByInvMassMatrix();
    if ( _solver->getUseConservativeALE() ) e->sedEq->setUseConservativeALE(true);
    d->sedDof->copy(*sedDof_old);
    d->sedDof->scale(0.5-2*gammaLFAM3);
    d->sedDof->axpy(*sedDof_new,0.5+2*gammaLFAM3);
    d->sedDof->axpy(*sedDof_K,DT*(1-2*gammaLFAM3)); // from from n-1/2+2*gamma to n+1/2
    if (s->getFlagSedLimiter() && !s->getUseConservativeALE()) {
      d->sedDof->scatter(true);
      s->sedLimiter->apply(d->sedDof);
    } else {
      d->sedDof->scatter(true);
    }
  }
  timerSed.stop();
  timer3d.stop();
  
  timer2d.start();
  /** advance 2d **/
  if ( _solver->getSolve2d() ) {  //   from n to n+1 with 3d forcing at n+1/2
    //   store uv2d time averages at n+1/2 and n+1
    etaDof2d_new->copy(*etaDof2d_tav_new);
    if ( !_solver->getUseModeSplit() )
      d->uvIntDof2d->copy(*uvIntDof2d_old);
    etaDof2d_tav_new->scale(0.0);
    uvAvDof2d_tav_new->scale(0.0);
    uvIntDof2d_tav_new->scale(0.0);
    uvIntDof2d_tav_newHalf->scale(0.0);
    uv2dDependencies();
    for (int i2d = 0; i2d < MStar; i2d++) {
      double time2d = _time + (i2d+0.5)*_dt;
      // NOTE: standard AB3
      e->eta2dEq->computeAllTerms(time2d,*d->etaDof2d,*etaDof2d_K,false);
      etaDof2d_K->multiplyByInvMassMatrix();
      int iterAB = i2d;
      if ( _n3d2d == 1 ) {
        iterAB = _iter;
      }
      if (iterAB == 0) {
        // Euler step to begin with !
        etaDof2d_new->axpy(*etaDof2d_K,_dt);
      } else if (iterAB == 1) {
        etaDof2d_new->axpy(*etaDof2d_K,_dt*(1.5));
        etaDof2d_new->axpy(*etaDof2d_Kold,_dt*(-(0.5)));
      } else {
        etaDof2d_new->axpy(*etaDof2d_K,_dt*(1.5+betaAB3));
        etaDof2d_new->axpy(*etaDof2d_Kold,_dt*(-(0.5+2*betaAB3)));
        etaDof2d_new->axpy(*etaDof2d_Kold2,_dt*betaAB3);
      }
      etaDof2d_new->scatterBegin(true);
      
      if ( s->getUseModeSplit() ) {
//        uvAvDof2d_old->copy(*d->uvAvDof2d);
        e->uv2dEq->computeAllTerms(time2d,*d->uvAvDof2d,*uvAvDof2d_K,false);
        uvAvDof2d_K->multiplyByInvMassMatrix();
        if (iterAB == 0) {
          // Euler step to begin with !
          d->uvAvDof2d->axpy(*uvAvDof2d_K,_dt);
        } else if (iterAB == 1) {
          d->uvAvDof2d->axpy(*uvAvDof2d_K,_dt*(1.5));
          d->uvAvDof2d->axpy(*uvAvDof2d_Kold,_dt*(-(0.5)));
        } else {
          d->uvAvDof2d->axpy(*uvAvDof2d_K,_dt*(1.5+betaAB3));
          d->uvAvDof2d->axpy(*uvAvDof2d_Kold,_dt*(-(0.5+2*betaAB3)));
          d->uvAvDof2d->axpy(*uvAvDof2d_Kold2,_dt*betaAB3);
        }
        d->uvAvDof2d->scatterBegin(true);
      }
      
      eta2dDependencies();
  //       d->etaDof2dCG->scatterBegin(true);

      if ( iterAB > 0 ) {
        etaDof2d_Kold2->copy(*etaDof2d_Kold);
        uvAvDof2d_Kold2->copy(*uvAvDof2d_Kold);
      }
      etaDof2d_Kold->copy(*etaDof2d_K);
      uvAvDof2d_Kold->copy(*uvAvDof2d_K);

      // temporal averages
  //       dgDofContainer uvIntTarget2d = dgDofContainer(*s->groups2d,2);
  //       uvAv2uvIntFunction uvIntFunc(d->uvAvDof2d->getFunction(),d->etaDof2d->getFunction(),f->bathFunc2d);
  //       uvAv2uvIntFunction uvIntOldFunc(uvAvDof2d_old->getFunction(),d->etaDof2d->getFunction(),f->bathFunc2d);
  //       uvIntTarget2d.interpolate((function*)&uvIntFunc);
  //       uvIntDof2d_tav_new->axpy(uvIntTarget2d,weightsA[i2d]); // for n+1
  //       uvIntDof2d_tav_newHalf->axpy(uvIntTarget2d,(0.5)*weightsB[i2d]); // for n+1/2
  //       uvIntTarget2d.interpolate((function*)&uvIntOldFunc);
  //       uvIntDof2d_tav_newHalf->axpy(uvIntTarget2d,(0.5)*weightsB[i2d]); // for n+1/2

      uvIntDof2d_tav_new->axpy(*d->uvAvDof2d,weightsA[i2d]); // for n+1
      uvIntDof2d_tav_newHalf->axpy(*d->uvAvDof2d,1.0*weightsB[i2d]); // for n+1/2
  //       uvIntDof2d_tav_newHalf->axpy(*d->uvAvDof2d,(0.5)*weightsB[i2d]); // for n+1/2
  //       uvIntDof2d_tav_newHalf->axpy(*uvAvDof2d_old,(0.5)*weightsB[i2d]); // for n+1/2

      etaDof2d_new->scatterEnd();
      d->etaDof2d->copy(*etaDof2d_new);
      etaDof2d_tav_new->axpy(*d->etaDof2d,weightsA[i2d]); // for n+1
      if ( s->getUseModeSplit() ) 
        d->uvAvDof2d->scatterEnd();
      uvAvDof2d_tav_new->axpy(*d->uvAvDof2d,weightsA[i2d]); // for n+1
      //       d->etaDof2dCG->scatterEnd();
      checkSanity(true);
      if (_exploded) return;
    }
  }
  
  //   update geometry to eta^{n+1}
  if ( _solver->getUseConservativeALE() ) {
    if (uvDof_new) uvDof_new->multiplyByMassMatrix();
    if (SDof_new) SDof_new->multiplyByMassMatrix();
    if (TDof_new) TDof_new->multiplyByMassMatrix();
    if (sedDof_new) sedDof_new->multiplyByMassMatrix();
  }
  etaDependencies(etaDof2d_tav_new);
  if ( _solver->getUseConservativeALE() ) {
    if (uvDof_new) uvDof_new->multiplyByInvMassMatrix();
    if (SDof_new) SDof_new->multiplyByInvMassMatrix();
    if (TDof_new) TDof_new->multiplyByInvMassMatrix();
    if (sedDof_new) sedDof_new->multiplyByMassMatrix();
  }
  timer2d.stop();
  
  timer3d.start();
  timerUV.start();
  // need to translate dependent variables to new geometry
  // correct depth average to to uv2d^{n+1/2} ( n+1/2 is now available )
  timerUVdep.start();
  uvDependencies(uvIntDof2d_tav_newHalf);
  timerUVdep.stop();
  timerUV.stop();
  timerRhodep.start();
  rhoDependencies();
  timerRhodep.stop();
  timerUV.start();

  /** 3D corrector step : uv **/
  //   UV from n to n+1 with n+1/2 rhs
  //   using uv^n+1/2 eta^n+1
  _time = _initTime + (_iter+0.5)*DT;
  e->horMomEq->computeAllTerms(_time,*d->uvDof,*uvDof_K,false); // ***
  uvDof_K->multiplyByInvMassMatrix();
  // TODO set uvDof_old to uvNew
  uvDof_old->copy(*uvDof_new);
  uvDof_new->axpy(*uvDof_K,DT); // from n to n+1
  timerUVimp.start();
  if ( _solver->getSolveUVImplicitVerticalDiffusion() ) {
    s->copyField3d->copy(uvDof_new,d->uDof,0,0);
    s->copyField3d->copy(uvDof_new,d->vDof,1,0);
    if (uKim && CrNiTheta != 1)
      d->uDof->axpy(*uKim,1-CrNiTheta);
    if (vKim && CrNiTheta != 1)
      d->vDof->axpy(*vKim,1-CrNiTheta);
    e->vertMomUEq->setComponent(0);
    uKim = &momUNewton->solve (d->uDof, 1./DT, NULL, _time);
    d->uDof->axpy(*uKim,CrNiTheta);
    e->vertMomUEq->setComponent(1);
    vKim = &momVNewton->solve (d->vDof, 1./DT, NULL, _time);
    d->vDof->axpy(*vKim,CrNiTheta);
    s->copyField3d->copy(d->uDof,uvDof_new,0,0);
    s->copyField3d->copy(d->vDof,uvDof_new,0,1);
  }
  timerUVimp.stop();
  uvDof_new->scatter(true);
  if ( s->getFlagUVLimiter())
    s->uvLimiter->apply(uvDof_new);

  /** 3D corrector step : T,S **/
  //   interpolate UV back to n+1/2
  d->uvDof->scale(0.5);
  d->uvDof->axpy(*uvDof_old, 0.25);
  d->uvDof->axpy(*uvDof_new, 0.25);
  // correct depth average to to uv2d^{n+1/2}
  timerUVdep.start();
  uvDependencies(uvIntDof2d_tav_newHalf); // TODO bf update is not necessary here (for tracers)
  timerUVdep.stop();
  timerUV.stop();
  
  timerS.start();
  //   advance T,S from n to n+1 with UV and rhs at n+1/2
  if ( _solver->getSolveS() ) {
    // salinity explicit
    e->SEq->computeAllTerms(_time,*d->SDof,*SDof_K,false);
    SDof_K->multiplyByInvMassMatrix();
    SDof_old->copy(*SDof_new);
    SDof_new->axpy(*SDof_K,DT); // from n to n+1
  }
  timerSimp.start();
  if ( _solver->getSolveSImplicitVerticalDiffusion() ) {
    //salinity implicit
    if (SKim && CrNiTheta != 1)
      SDof_new->axpy(*SKim,1-CrNiTheta);
    SKim = &SDiffNewton->solve (d->SDof, 1./DT, NULL, _time);
    SDof_new->axpy(*SKim,CrNiTheta);
  }
  timerSimp.stop();
  if ( _solver->getSolveS() || _solver->getSolveSImplicitVerticalDiffusion() ) {
    if (s->getFlagSLimiter()) {
      SDof_new->scatter(true);
      s->SLimiter->apply(SDof_new);
    } else {
      SDof_new->scatter(true);
    }
    // interpolate S back to n+1/2
    d->SDof->scale(0.5);
    d->SDof->axpy(*SDof_old, 0.25);
    d->SDof->axpy(*SDof_new, 0.25);
  }
  timerS.stop();

  timerT.start();
  if ( _solver->getSolveT() ) {
    // salinity explicit
    e->TEq->computeAllTerms(_time,*d->TDof,*TDof_K,false);
    TDof_K->multiplyByInvMassMatrix();
    TDof_old->copy(*TDof_new);
    TDof_new->axpy(*TDof_K,DT); // from n to n+1
  }
  timerTimp.start();
  if ( _solver->getSolveTImplicitVerticalDiffusion() ) {
    //salinity implicit
    if (TKim && CrNiTheta != 1)
      TDof_new->axpy(*TKim,1-CrNiTheta);
    TKim = &TDiffNewton->solve (d->TDof, 1./DT, NULL, _time);
    TDof_new->axpy(*TKim,CrNiTheta);
  }
  timerTimp.stop();
  if ( _solver->getSolveT() || _solver->getSolveTImplicitVerticalDiffusion() ) {
    if (s->getFlagTLimiter()) {
      TDof_new->scatter(true);
      s->TLimiter->apply(TDof_new);
    } else {
      TDof_new->scatter(true);
    }
    // interpolate T back to n+1/2
    d->TDof->scale(0.5);
    d->TDof->axpy(*TDof_old, 0.25);
    d->TDof->axpy(*TDof_new, 0.25);
  }
  timerT.stop();

  timerSed.start();
  if ( _solver->getSolveSed() ){
    s->copy2d3d->copyTo2D(d->sedDof,d->sedBottomDof2d,0);
    d->sedBottomFluxDof2d->interpolate(_solver->functions->sedBottomFluxFunc2d);
    sedGroundDof_old->copy(*sedGroundDof_new);
    sedGroundDof_new->axpy(*d->sedBottomFluxDof2d, -DT);

    e->sedEq->computeAllTerms(_time,*d->sedDof,*sedDof_K,false);
    sedDof_K->multiplyByInvMassMatrix();
    sedDof_old->copy(*sedDof_new);
    sedDof_new->axpy(*sedDof_K,DT); // from n to n+1
  }
  timerSedimp.start();
  if ( _solver->getSolveSedImplicitVerticalDiffusion() ) {
    //salinity implicit
    if (sedKim && CrNiTheta != 1){
      sedDof_new->axpy(*sedKim,1-CrNiTheta);
      s->copy2d3d->copyTo2D(sedKim,d->sedBottomDof2d,0);
      d->sedBottomFluxDof2d->interpolate(_solver->functions->sedBottomFluxFunc2d, _time);
      sedGroundDof_new->axpy(*d->sedBottomFluxDof2d, -(1-CrNiTheta));
    }
    sedKim = &sedDiffNewton->solve (d->sedDof, 1./DT, NULL, _time);
    sedDof_new->axpy(*sedKim,CrNiTheta);
    s->copy2d3d->copyTo2D(sedKim,d->sedBottomDof2d,0);
    d->sedBottomFluxDof2d->interpolate(_solver->functions->sedBottomFluxFunc2d);
    sedGroundDof_new->axpy(*d->sedBottomFluxDof2d, CrNiTheta);
  }
  timerSedimp.stop();
  if ( _solver->getSolveSed() || _solver->getSolveSedImplicitVerticalDiffusion() ) {
    if (s->getFlagSedLimiter()) {
      sedDof_new->scatter(true);
      s->sedLimiter->apply(sedDof_new);
    } else {
      sedDof_new->scatter(true);
    }
    d->sedDof->copy(*sedDof_new);
    d->sedGroundDof2d->copy(*sedGroundDof_new);
  }
  timerSed.stop();
  timerSed.stop();
  if ( s->getSolveGMVel() ) {
    e->nbMod->computeHeights();
    e->nbMod->computeNN();
    e->nbMod->exportNNData(d->nbDof3d);
  }
  timer3d.stop();
  
  timergotm.start();
#ifdef HAVE_GOTM
  slim3dSolverFunctions *f = _solver->functions;
  if ( s->getSolveTurbulence() && s->getAdvectTurbulentEnergy() ) {
    // tke advection
    e->tkeAdvEq->computeAllTerms(_time,*d->tkeDof,*tkeDof_K1);
    tkeDof_K1->multiplyByInvMassMatrix();
    tkeDof_K->scale(0.0);
    tkeDof_K->axpy(*tkeDof_K1,1.5+betaAB3);
    tkeDof_K->axpy(*tkeDof_K2,-(0.5+2*betaAB3));
    tkeDof_K->axpy(*tkeDof_K3,betaAB3);
    d->tkeDof->axpy(*tkeDof_K,DT);
    tkeDof_K3->copy(*tkeDof_K2);
    tkeDof_K2->copy(*tkeDof_K1);

    // eps advection
    e->epsAdvEq->computeAllTerms(_time,*d->epsDof,*epsDof_K1);
    epsDof_K1->multiplyByInvMassMatrix();
    epsDof_K->scale(0.0);
    epsDof_K->axpy(*epsDof_K1,1.5+betaAB3);
    epsDof_K->axpy(*epsDof_K2,-(0.5+2*betaAB3));
    epsDof_K->axpy(*epsDof_K3,betaAB3);
    d->epsDof->axpy(*epsDof_K,DT);
    epsDof_K3->copy(*epsDof_K2);
    epsDof_K2->copy(*epsDof_K1);
    
    d->tkeDof->scatter(true);
    d->epsDof->scatter(true);
    _solver->tkeLimiter->apply(d->tkeDof);
    _solver->epsLimiter->apply(d->epsDof);
  }
  if ( _solver->getSolveTurbulence() ) {
    _solver->prismConverter->convertToPZeroHorizontal(d->SDof,d->SDofCenter);
    _solver->prismConverter->convertToPZeroHorizontal(d->TDof,d->TDofCenter);
    if ( f->windStressFunc ) {
      d->windStressDof2dCenter->interpolate(f->windStressFunc);
      _solver->prismConverter->convertToPZero2d(d->windStressDof2dCenter,d->windStressDof2dCenter);
    }
    _solver->prismConverter->convertToPZero2d(d->uvTauBDof2d,d->uvTauBDof2dCenter);
    _solver->prismConverter->convertToPZeroHorizontal(d->uvDof,d->uvDofCenter);
    _solver->prismConverter->convertToPZeroHorizontal(d->zDof,d->zDofCenter);
    _solver->prismConverter->convertToPZeroHorizontal(d->tkeDof,d->tkeDofCenter);
    _solver->prismConverter->convertToPZeroHorizontal(d->epsDof,d->epsDofCenter);
    _solver->prismConverter->convertToPZeroHorizontal(d->etaDof2dCG->getFunction(),d->etaDof3dCenter);
    e->turbMod->updateGOTM((1)*DT);

    _solver->dgCG3d->apply(d->nuvDofCenter,d->nuvDof);
    _solver->dgCG3d->apply(d->kappavDofCenter,d->kappavDof);
    _solver->dgCG3d->apply(d->tkeDofCenter,d->tkeDof);
    _solver->dgCG3d->apply(d->epsDofCenter,d->epsDof);
  }
#endif
  timergotm.stop();
  timer3d.stop();

  // set all fields to n+1 (for the exports and next iteration)
  timer2d.start();
  d->etaDof2d->copy(*etaDof2d_tav_new);
  d->uvAvDof2d->copy(*uvAvDof2d_tav_new);
  timer2d.stop();
  timer3d.start();
  timerS.start();
  if ( _solver->getSolveS() || _solver->getSolveSImplicitVerticalDiffusion() )
    d->SDof->copy(*SDof_new);
  timerS.stop();
  timerT.start();
  if ( _solver->getSolveT() || _solver->getSolveTImplicitVerticalDiffusion() )
    d->TDof->copy(*TDof_new);
  /*if ( _solver->getSolveSed()){ // not necessary. Since sed is a passive tracer, it has not been putting nack to n+1/2 as T and S before
    d->sedDof->copy(*sedDof_new);
    d->sedGroundDof2d->copy(*sedGroundDof_new);
  }*/
  timerT.stop();
  timerRhodep.start();
  rhoDependencies();
  timerRhodep.stop();
  
  timerUV.start();
  d->uvDof->copy(*uvDof_new);
  // correct depth average to to uv2d^{n+1}
  timerUVdep.start();
  uvDependencies(uvIntDof2d_tav_new);
  timerUVdep.stop();
  uvDof_new->copy(*d->uvDof);
  timerUV.stop();
  
  timer3d.stop();
  timer2d.start();
  // store uvInt at n+1 for no split option
  if ( !_solver->getUseModeSplit() )
    uvIntDof2d_old->copy(*d->uvIntDof2d);
 
  _iter++;
  _time = _initTime + _iter*DT; // for exports, all fields are now at n+1
  timer2d.stop();
  //timer.printFull();
  timer.stop();
}

void slim3dTimeIntegratorPC::reStart(std::string directory, int export_i)
{
  
  checkParameters();
  createTemporalWeights();
  
  slim3dSolverDofs *d = _solver->getDofs();
  
  directory.append("/");
  Msg::Info("Loading initial state from directory : \'%s\', export index : %d",directory.c_str(),export_i);
  std::stringstream iExportStream;
  iExportStream << "-" << std::setfill('0') << std::setw(6) << export_i;
  std::string iExportStr = iExportStream.str();
  double t;
  int step;
  
  std::string fieldName;
  Msg::Info("reading file: %s",(directory+"uv"+iExportStr).c_str());
  d->uvDof->readMsh(directory+"uv/uv"+iExportStr+".idx",t,step);
  d->uvDof->scatter(true);
  uvDof_new->copy(*d->uvDof); // same as i
  Msg::Info("reading file: %s",(directory+"uvOld"+iExportStr).c_str());
  uvDof_old->readMsh(directory+"uvOld/uvOld"+iExportStr+".idx",t,step);
  uvDof_old->scatter(true);

  
  // set correct time, setup exporter
  double EXPTOLERANCE = 1e-4;
  _time = t; //is given in second anyway because of fullExpUVOld
  _iter = _time/_n3d2d/(_dt*(1-1e-10)); // some problem occured without 0.999
  _nextExportTime = _time - fmod(_time, _exportInterval-EXPTOLERANCE) + _exportInterval;
  _nextFullExportTime = _time - fmod(_time, _fullExportInterval-EXPTOLERANCE) + _fullExportInterval;
  _exportCount = export_i;
  _time = t + _initTime;
  
  
  if (_solver->getSolveUVImplicitVerticalDiffusion()){
    dgDofContainer *tmpUKim = momUNewton->getCurrentSolution();
    Msg::Info("reading file: %s",(directory+"uKim"+iExportStr).c_str());
    tmpUKim->readMsh(directory+"uKim/uKim"+iExportStr+".idx",t,step);
    uKim = tmpUKim;
    dgDofContainer *tmpVKim = momVNewton->getCurrentSolution();
    Msg::Info("reading file: %s",(directory+"vKim"+iExportStr).c_str());
    tmpVKim->readMsh(directory+"vKim/vKim"+iExportStr+".idx",t,step);
    vKim = tmpVKim;
  }

  
  if ( _solver->getSolveS() || _solver->getSolveSImplicitVerticalDiffusion() ) {
    Msg::Info("reading file: %s",(directory+"S"+iExportStr).c_str());
    d->SDof->readMsh(directory+"S/S"+iExportStr+".idx",t,step);
    d->SDof->scatter(true);
    SDof_new->copy(*d->SDof); // same as i
    Msg::Info("reading file: %s",(directory+"SOld"+iExportStr).c_str());
    SDof_old->readMsh(directory+"SOld/SOld"+iExportStr+".idx",t,step);
    SDof_old->scatter(true);
  }
  if (_solver->getSolveSImplicitVerticalDiffusion()){
    dgDofContainer *tmpSKim = SDiffNewton->getCurrentSolution();
    Msg::Info("reading file: %s",(directory+"SKim"+iExportStr).c_str());
    tmpSKim->readMsh(directory+"SKim/SKim"+iExportStr+".idx",t,step);
    SKim = tmpSKim;
  }
  if ( _solver->getSolveT() || _solver->getSolveTImplicitVerticalDiffusion() ) {
    Msg::Info("reading file: %s",(directory+"T"+iExportStr).c_str());
    d->TDof->readMsh(directory+"T/T"+iExportStr+".idx",t,step);
    d->TDof->scatter(true);
    TDof_new->copy(*d->TDof); // same as i
    Msg::Info("reading file: %s",(directory+"TOld"+iExportStr).c_str());
    TDof_old->readMsh(directory+"TOld/TOld"+iExportStr+".idx",t,step);
    TDof_old->scatter(true);
  }
  if (_solver->getSolveTImplicitVerticalDiffusion()){
    dgDofContainer *tmpTKim = TDiffNewton->getCurrentSolution();
    Msg::Info("reading file: %s",(directory+"TKim"+iExportStr).c_str());
    tmpTKim->readMsh(directory+"TKim/TKim"+iExportStr+".idx",t,step);
    TKim = tmpTKim;
  }
  
  if ( _solver->getSolveSed() || _solver->getSolveSedImplicitVerticalDiffusion() ) {
    Msg::Info("reading file: %s",(directory+"sed"+iExportStr).c_str());
    d->sedDof->readMsh(directory+"sed/sed"+iExportStr+".idx",t,step);
    d->sedDof->scatter(true);
    sedDof_new->copy(*d->sedDof); // same as i
    Msg::Info("reading file: %s",(directory+"sedOld"+iExportStr).c_str());
    sedDof_old->readMsh(directory+"sedOld/sedOld"+iExportStr+".idx",t,step);
    sedDof_old->scatter(true);
  }
  if (_solver->getSolveTImplicitVerticalDiffusion()){
    dgDofContainer *tmpSedKim = sedDiffNewton->getCurrentSolution();
    Msg::Info("reading file: %s",(directory+"sedKim"+iExportStr).c_str());
    tmpSedKim->readMsh(directory+"sedKim/sedKim"+iExportStr+".idx",t,step);
    sedKim = tmpSedKim;
  }
  
  Msg::Info("reading file: %s",(directory+"eta2d"+iExportStr).c_str());
  d->etaDof2d->readMsh(directory+"eta2d/eta2d"+iExportStr+".idx",t,step);
  d->etaDof2d->scatter(true);
  Msg::Info("reading file: %s",(directory+"eta2dTavNew"+iExportStr).c_str());
  etaDof2d_tav_new->readMsh(directory+"eta2dTavNew/eta2dTavNew"+iExportStr+".idx",t,step);
  etaDof2d_tav_new->scatter(true);
  //   update geometry to eta^{n+1}
  etaDependencies(etaDof2d_tav_new);
  
  Msg::Info("reading file: %s",(directory+"uvAv2d"+iExportStr).c_str());
  d->uvAvDof2d->readMsh(directory+"uvAv2d/uvAv2d"+iExportStr+".idx",t,step);
  d->uvAvDof2d->scatter(true);
  Msg::Info("reading file: %s",(directory+"uvInt2dTavNew"+iExportStr).c_str());
  uvIntDof2d_tav_new->readMsh(directory+"uvInt2dTavNew/uvInt2dTavNew"+iExportStr+".idx",t,step);
  uvIntDof2d_tav_new->scatter(true);
  

#ifdef HAVE_GOTM
  if ( _solver->getSolveTurbulence() ) {
    // must be initialized before assign state
    _solver->getEquations()->turbMod->initialize(); 
    
    Msg::Info("reading file: %s",(directory+"tke"+iExportStr).c_str());
    d->tkeDof->readMsh(directory+"tke/tke"+iExportStr+".idx",t,step);
    d->tkeDof->scatter(true);

    Msg::Info("reading file: %s",(directory+"eps"+iExportStr).c_str());
    d->epsDof->readMsh(directory+"eps/eps"+iExportStr+".idx",t,step);
    d->epsDof->scatter(true);

    Msg::Info("reading file: %s",(directory+"nuv"+iExportStr).c_str());
    d->nuvDof->readMsh(directory+"nuv/nuv"+iExportStr+".idx",t,step);
    d->nuvDof->scatter(true);

    Msg::Info("reading file: %s",(directory+"kappav"+iExportStr).c_str());
    d->kappavDof->readMsh(directory+"kappav/kappav"+iExportStr+".idx",t,step);
    d->kappavDof->scatter(true);
    
    Msg::Info("reading file: %s",(directory+"nuvCenter"+iExportStr).c_str()); 
    d->nuvDofCenter->readMsh(directory+"nuvCenter/nuvCenter"+iExportStr+".idx",t,step);
    d->nuvDofCenter->scatter(true);

    Msg::Info("reading file: %s",(directory+"kappavCenter"+iExportStr).c_str());
    d->kappavDofCenter->readMsh(directory+"kappavCenter/kappavCenter"+iExportStr+".idx",t,step);
    d->kappavDofCenter->scatter(true);
    
    Msg::Info("reading file: %s",(directory+"epsK2"+iExportStr).c_str());
    epsDof_K2->readMsh(directory+"epsK2/epsK2"+iExportStr+".idx",t,step);
    epsDof_K2->scatter(true);
    Msg::Info("reading file: %s",(directory+"epsK3"+iExportStr).c_str());
    epsDof_K3->readMsh(directory+"epsK3/epsK3"+iExportStr+".idx",t,step);
    epsDof_K3->scatter(true);
    Msg::Info("reading file: %s",(directory+"tkeK2"+iExportStr).c_str());
    tkeDof_K2->readMsh(directory+"tkeK2/tkeK2"+iExportStr+".idx",t,step);
    tkeDof_K2->scatter(true);
    Msg::Info("reading file: %s",(directory+"tkeK3"+iExportStr).c_str());
    tkeDof_K3->readMsh(directory+"tkeK3/tkeK3"+iExportStr+".idx",t,step);
    tkeDof_K3->scatter(true);

    Msg::Info("reading file: %s",(directory+"uvCenter"+iExportStr).c_str());
    d->uvDofCenter->readMsh(directory+"uvCenter/uvCenter"+iExportStr+".idx",t,step);
    d->uvDofCenter->scatter(true);
    // uvDofCenter will be overwritten in advanceOneTimeStep, so we need to evaluate it now
    _solver->getEquations()->turbMod->evaluateUVAtCellCenters();
    
  }
#endif
  
  // uvDof has been corrected before export
  // get uvInt from its depth integral
  uvDependencies(uvAvDof2d_tav_new, true);
  rhoDependencies();

  //_solver->exporter->exportDofs(_iter,_time - _initTime,_dt);
  //s->exporter->setExportCount(export_i+1); 
}

void slim3dTimeIntegratorPC::approximateHotStart(std::string directory, int export_i)
{
  // NOTE it is diffucult to recover the exact state of multistep time integratiors.
  // This would require to store uvOld Told Sold uKim vKim SKim TKim dofs additionally.
  // This is only approximate, because the first step will be less accurate (as in normal init)
  // Required files:
  // uv_00009_COMP_0.msh
  // uv_00009_COMP_1.msh
  // S_00009_COMP_0.msh  (if S solved)
  // T_00009_COMP_0.msh  (if T solved)
  // eta2d_00009_COMP_0.msh
  // uvAv2d_00009_COMP_0.msh
  // uvAv2d_00009_COMP_1.msh
  // tke_00009_COMP_0.msh (if turbulence)
  // eps_00009_COMP_0.msh (if turbulence)
  // nuv_00009_COMP_0.msh (if turbulence)
  // kappav_00009_COMP_0.msh (if turbulence)
 
  checkParameters();
  createTemporalWeights();
 
  slim3dSolver *s = _solver;
  slim3dSolverDofs *d = _solver->getDofs();
  slim3dSolverFunctions *f = _solver->functions;
 
  directory.append("/");
  Msg::Info("Loading initial state from directory \n  %s\n  export index %d",directory.c_str(),export_i);
  std::stringstream iExportStream;
  iExportStream << "_" << std::setfill('0') << std::setw(5) << export_i;
  std::string iExportStr = iExportStream.str();
  iExportStream.str("");
  iExportStream << "_" << std::setfill('0') << std::setw(5) << export_i-1;
  std::string iOldExportStr = iExportStream.str();
 
  d->xyzOrigDof->interpolate(f->xyzFunc3d);
  d->xyzOrigDof->scatter(true);
 
  double t;
  int step;
  std::string fieldName;
  Msg::Info("reading file: %s\n",(directory+"uv"+iExportStr).c_str());
  d->uvDof->importMsh(directory+"uv"+iExportStr,t,step,fieldName);
  d->uvDof->scatter(true);
  Msg::Info(" norm: %g\n",d->uvDof->norm());
  uvDof_new->copy(*d->uvDof); 
  uvDof_old->copy(*d->uvDof);

  // set correct time, setup exporter
  _time = t;
  _iter = _time/_n3d2d/_dt;
  //s->exporter->setExportCount(export_i);!!!!!!!!!!
  _nextExportTime = _time + _exportInterval;

  if ( _solver->getSolveS() || _solver->getSolveSImplicitVerticalDiffusion() ) {
    Msg::Info("reading file: %s\n",(directory+"S"+iExportStr).c_str());
    d->SDof->importMsh(directory+"S"+iExportStr,t,step,fieldName);
    d->SDof->scatter(true);
    Msg::Info(" norm: %g\n",d->SDof->norm());
    SDof_new->copy(*d->SDof);
    SDof_old->copy(*d->SDof);
  }
  if ( _solver->getSolveT() || _solver->getSolveTImplicitVerticalDiffusion() ) {
    Msg::Info("reading file: %s\n",(directory+"T"+iExportStr).c_str());
    d->TDof->importMsh(directory+"T"+iExportStr,t,step,fieldName);
    d->TDof->scatter(true);
    Msg::Info(" norm: %g\n",d->TDof->norm());
    TDof_new->copy(*d->TDof);
    TDof_old->copy(*d->TDof);
  }
  Msg::Info("reading file: %s\n",(directory+"eta2d"+iExportStr).c_str());
  d->etaDof2d->importMsh(directory+"eta2d"+iExportStr,t,step,fieldName);
  d->etaDof2d->scatter(true);
  Msg::Info(" norm: %g\n",d->etaDof2d->norm());
  etaDof2d_tav_new->copy(*d->etaDof2d);
  eta2dDependencies();
  //   update geometry to eta^{n+1}
  etaDependencies(etaDof2d_tav_new);
 
  Msg::Info("reading file: %s\n",(directory+"uvAv2d"+iExportStr).c_str());
  d->uvAvDof2d->importMsh(directory+"uvAv2d"+iExportStr,t,step,fieldName);
  d->uvAvDof2d->scatter(true);
  Msg::Info(" norm: %g\n",d->uvAvDof2d->norm());
  uvAvDof2d_tav_new->copy(*d->uvAvDof2d);
 
  // uvDof has been corrected before export
  // get uvInt from its depth integral
  s->newDepthIntegrator->integrate(d->uvDof,d->uvIntDof2d);
  uvDependencies(d->uvIntDof2d, true);
  uvIntDof2d_tav_new->copy(*d->uvIntDof2d);
  rhoDependencies();

  // NOTE for implicit vertical diffusion the tendencies uKim vKim TKim are needed from previous iteration
  // This could be implemented by exporting+loading uKim vKim TKim
  // or by recomputing uKim from uvDofOld and nuvOld (not stored normally)
  // for now the old tendencies are ignored (i.e. first step is backward euler euler)

#ifdef HAVE_GOTM
  if ( _solver->getSolveTurbulence() ) {
    // must be initialized before assign state
    //e->turbMod->initialize();
   
    printf("reading file: %s\n",(directory+"tke"+iExportStr).c_str());
    d->tkeDof->importMsh(directory+"tke"+iExportStr,t,step,fieldName);
    d->tkeDof->scatter(true);
    printf(" norm: %g\n",d->tkeDof->norm());

    printf("reading file: %s\n",(directory+"eps"+iExportStr).c_str());
    d->epsDof->importMsh(directory+"eps"+iExportStr,t,step,fieldName);
    d->epsDof->scatter(true);
    printf(" norm: %g\n",d->epsDof->norm());

    printf("reading file: %s\n",(directory+"nuv"+iExportStr).c_str());
    d->nuvDof->importMsh(directory+"nuv"+iExportStr,t,step,fieldName);
    d->nuvDof->scatter(true);
    printf(" norm: %g\n",d->nuvDof->norm());

    printf("reading file: %s\n",(directory+"kappav"+iExportStr).c_str());
    d->kappavDof->importMsh(directory+"kappav"+iExportStr,t,step,fieldName);
    d->kappavDof->scatter(true);
    printf(" norm: %g\n",d->kappavDof->norm());
   

    // NOTE it is not easy to recover the K2 and K3 for AB3 (would require etaCG2d, mesh, uv, w)
    // compute K2 and copy to K3 (first step is ~AB2)
//     tkeResidu->compute(_time,*d->tkeDof,*tkeDof_K2);
//     tkeDof_K3->copy(*tkeDof_K2);
//     epsResidu->compute(_time,*d->epsDof,*epsDof_K2);
//     epsDof_K3->copy(*epsDof_K2);
  }
#endif
}

void slim3dTimeIntegratorPC::initFullExporter(std::string exportDirectory){
  slim3dSolver *s = _solver;
  slim3dSolverDofs *d = _solver->getDofs();
  _fullExporter.insert(new dgIdxExporter(d->uvDof, exportDirectory + "/uv"));
  _fullExporter.insert(new dgIdxExporter(d->uvAvDof2d, exportDirectory + "/uvAv2d"));
  _fullExporter.insert(new dgIdxExporter(d->etaDof2d, exportDirectory + "/eta2d"));
  _fullExporter.insert(new dgIdxExporter(uvIntDof2d_tav_new, exportDirectory + "/uvInt2dTavNew"));
  _fullExporter.insert(new dgIdxExporter(etaDof2d_tav_new, exportDirectory + "/eta2dTavNew"));
  _fullExporter.insert(new dgIdxExporter(uvDof_old, exportDirectory + "/uvOld"));
  if (s->getSolveUVImplicitVerticalDiffusion()){
    _fullExporter.insert(new dgIdxExporter(uKim, exportDirectory + "/uKim")); 
    _fullExporter.insert(new dgIdxExporter(vKim, exportDirectory + "/vKim")); 
  }
  if (s->getSolveS() || s->getSolveSImplicitVerticalDiffusion() || s->getSolveTurbulence())
    _fullExporter.insert(new dgIdxExporter(d->SDof, exportDirectory + "/S"));
  if (s->getSolveS() || s->getSolveSImplicitVerticalDiffusion())
    _fullExporter.insert(new dgIdxExporter(SDof_old, exportDirectory + "/SOld"));
  if (s->getSolveSImplicitVerticalDiffusion())
    _fullExporter.insert(new dgIdxExporter(SKim, exportDirectory + "/SKim")); 
  if (s->getSolveT() || s->getSolveTImplicitVerticalDiffusion() || s->getSolveTurbulence())
    _fullExporter.insert(new dgIdxExporter(d->TDof, exportDirectory + "/T"));
  if (s->getSolveT() || s->getSolveTImplicitVerticalDiffusion())
    _fullExporter.insert(new dgIdxExporter(TDof_old, exportDirectory + "/TOld"));
  if (s->getSolveTImplicitVerticalDiffusion())
    _fullExporter.insert(new dgIdxExporter(TKim, exportDirectory + "/TKim")); 
  if (s->getSolveSed() || s->getSolveSedImplicitVerticalDiffusion()){
    _fullExporter.insert(new dgIdxExporter(d->sedDof, exportDirectory + "/sed"));
    _fullExporter.insert(new dgIdxExporter(sedDof_old, exportDirectory + "/sedOld"));
  }
  if (s->getSolveSedImplicitVerticalDiffusion())
    _fullExporter.insert(new dgIdxExporter(sedKim, exportDirectory + "/sedKim")); 
  if (s->getSolveTurbulence()){
    _fullExporter.insert(new dgIdxExporter(d->nuvDof, exportDirectory + "/nuv"));
    _fullExporter.insert(new dgIdxExporter(d->kappavDof, exportDirectory + "/kappav"));
    _fullExporter.insert(new dgIdxExporter(d->nuvDofCenter, exportDirectory + "/nuvCenter"));
    _fullExporter.insert(new dgIdxExporter(d->kappavDofCenter, exportDirectory + "/kappavCenter"));
    _fullExporter.insert(new dgIdxExporter(d->uvDofCenter, exportDirectory + "/uvCenter"));
    if (s->getAdvectTurbulentEnergy()){
      _fullExporter.insert(new dgIdxExporter(d->tkeDof, exportDirectory + "/tke"));
      _fullExporter.insert(new dgIdxExporter(d->epsDof, exportDirectory + "/eps"));
      _fullExporter.insert(new dgIdxExporter(tkeDof_K2, exportDirectory + "/tkeK2"));
      _fullExporter.insert(new dgIdxExporter(tkeDof_K3, exportDirectory + "/tkeK3"));
      _fullExporter.insert(new dgIdxExporter(epsDof_K2, exportDirectory + "/epsK2"));
      _fullExporter.insert(new dgIdxExporter(epsDof_K3, exportDirectory + "/epsK3"));
    }
  }
}

void slim3dTimeIntegratorPC::exportAll(){
  for (std::set<dgIdxExporter*>::iterator it = _fullExporter.begin(); it != _fullExporter.end(); ++it){
    (*it)->exportIdx(_exportCount, _time - _initTime);
  }
}


/** temporal weights a_m according to Shchepetkin and McWilliams 2005 **/
double temporalWeightA(int m, int M, int p, int q, double r, double tau0) {
  double tau = double(m+1)/double(M) / tau0;
  return pow(tau,p) * ( 1 - pow(tau,q) ) - r*tau;
}

/** cost functional for a_m                                  *
  * free parameters: A_0, tau0                               *
  * sum a_m = 1     is easy to ensure by scaling a_m by A_0  *
  * sum m/M*a_m = 1   is non-linear in tau0, optimized       **/
double temporalWeightError(int M, int p, int q, double r, double tau0, double root,
                           std::vector<double> &a) {
  int MStar = (int)floor( root*tau0*M );
  a.resize(MStar,0);
  // create weights a
  double sum = 0;
  for ( int i = 0; i < MStar; i++ ) {
    a[i] = temporalWeightA(i, M, p, q, r, tau0);
    sum += a[i];
  }
  // normalize
  for ( int i = 0; i < MStar; i++ ) {
    a[i] /= sum;
  }
  // compute centroid (should be 1.0)
  double centroid = 0;
  for ( int i = 0; i < MStar; i++ ) {
    centroid += double(i+1)/double(M)*a[i];
  }
  return 1.0 - centroid;
}

/** Given the set of parameters finds the optimal temporal weights **/
double optimizeTemporalWeightsA(double M, double p, double q, double r, double root, std::vector<double> &a) {
  double TOL = 1e-6;
  double tau0 = double((p+2)*(p+q+2)) / double((p+1)*(p+q+1));
  double tau1 = tau0 + 0.1;
  // secant method, find y(tau) = 0
  double y0 = temporalWeightError(M,p,q,r,tau0,root,a);
  double y1 = temporalWeightError(M,p,q,r,tau1,root,a);
  double tau = tau1 - y1*(tau1-tau0)/(y1-y0);
  double y = temporalWeightError(M,p,q,r,tau,root,a);
  int n = 1;
  while ( fabs(y) > TOL ) {
    tau0 = tau1;
    y0 = y1;
    tau1 = tau;
    y1 = y;
    tau = tau1 - y1*(tau1-tau0)/(y1-y0);
    y = temporalWeightError(M,p,q,r,tau,root,a);
    n = n +1;
    if ( n > 100 ) {
      Msg::Fatal("Optimizing temporal weights failed!");
    }
  }
  //if ( fabs(y) < TOL ) 
  //  Msg::Info("secant converged %d %e",n,y);
  return tau;
}

void slim3dTimeIntegratorPC::createTemporalWeights()
{
  if (_n3d2d == 1) {
    MStar = 1;
    weightsA.push_back(1.0);
    weightsB.push_back(1.0);
    return;
  }
  // weights according to Shchepetkin and McWilliams 2009
  double p, q, r, root;
  //p = 2; q = 1; r = 0.1696907; root = 0.7833889553246562;
  //p = 2; q = 2; r = 0.2346283; root = 0.8510653572389005;
  //p = 2; q = 3; r = 0.2664452; root = 0.8878628864796678;
  p = 2; q = 4; r = 0.2846158; root = 0.9105557291905246;
  //p = 2; q = 6; r = 0.2961888; root = 0.9387753064094538;
  //p = 3; q = 8; r = 0.1369941; root = 0.9809873256837536;

  optimizeTemporalWeightsA( _n3d2d, p, q, r, root, weightsA );
  //weightsA.resize(0);
  //weightsA.resize(_n3d2d, 1./_n3d2d);
  MStar = weightsA.size();
  // compute B
  weightsB.resize(MStar,0.0);
  double reverseSum=0;
  for (int i=MStar-1; i >=0; i--) {
    reverseSum += weightsA[i];
    weightsB[i] = reverseSum/_n3d2d;
  }
  if ( Msg::GetCommRank() == 0 ) {
    printf("Temporal average weights: M: %d    MStar: %d\n",_n3d2d,MStar);
/*    printf("A:\n");
    for (int i=0; i < MStar; i++) {
      printf("%2d: %.15f\n",i,weightsA[i]);
    }
    printf("B:\n");
    for (int i=0; i < MStar; i++) {
      printf("%2d: %.15f\n",i,weightsB[i]);
    }*/
    double asum = 0, bsum = 0;
    double aCenter = 0, bCenter = 0;
    for (int i=0; i < MStar; i++) {
      asum += weightsA[i];
      bsum += weightsB[i];
      aCenter += (i+1)*weightsA[i]/_n3d2d;
      bCenter += (double(i)+0.5)*weightsB[i]/_n3d2d;
    }
    printf("A sum: %.14e \n",asum);
    printf("B sum: %.14e \n",bsum);
    printf("A center: %.14e \n",aCenter);
    printf("B center: %.14e \n",bCenter);
  }
}

slim3dTimeIntegrator::massConservationChecker::massConservationChecker(dgGroupCollection* groups, const function* func) : _integrator(groups,func)
{
  _initialized = false;
  _initMass = 0;
}

double slim3dTimeIntegrator::massConservationChecker::computeRelativeChange()
{
  fullMatrix<double> totMass(1,1);
  _integrator.compute(totMass);
  if ( !_initialized ) {
    _initMass = totMass(0,0);
    _initialized = true;
  }
  return (totMass(0,0)-_initMass)/_initMass;
}

slim3dTimeIntegrator::tracerDeviationChecker::tracerDeviationChecker(dgDofContainer* dof)
{
  _dof = dof;
  _initialized = false;
  _initMax = _initMin = 0;
}

double slim3dTimeIntegrator::tracerDeviationChecker::computeMaxChange(double &minVal, double &maxVal)
{
  fullMatrix<double> minFM(1,1), maxFM(1,1);
  _dof->minmax(minFM,maxFM);
  minVal = minFM(0,0);
  maxVal = maxFM(0,0);
  if ( !_initialized ) {
    _initMin = minVal;
    _initMax = maxVal;
    _initialized = true;
  }
  return std::max( fabs(minVal-_initMin), fabs(maxVal-_initMax) );
}

void slim3dTimeIntegrator::checkMassConservation(double *volErr, double *SErr, double *TErr){
  double relErr;
  if (!_volConservationChecker)
    _volConservationChecker = new massConservationChecker(_solver->groups3d, _solver->functions->oneFunc);
  relErr= _volConservationChecker->computeRelativeChange();
  Msg::Info("    Rel. Dev. Volume  %3.9e", relErr);
  if (volErr != NULL) (*volErr) = relErr;
  if (_solver->getSolveS()){
    if (!_SConservationChecker)
      _SConservationChecker = new massConservationChecker(_solver->groups3d, _solver->getDofs()->SDof->getFunction());
    relErr= _SConservationChecker->computeRelativeChange();
    Msg::Info("    Rel. Dev. S Mass  %3.9e", relErr);
    if (SErr != NULL) (*SErr) = relErr;
  }
  if (_solver->getSolveT()){
    if (!_TConservationChecker)
      _TConservationChecker = new massConservationChecker(_solver->groups3d, _solver->getDofs()->TDof->getFunction());
    relErr= _TConservationChecker->computeRelativeChange();
    Msg::Info("    Rel. Dev. T Mass  %3.9e", relErr);
    if (TErr != NULL) (*TErr) = relErr;
  }
}

void slim3dTimeIntegrator::checkTracerConsistency(double *SDev, double *TDev){
  double minVal, maxVal, maxDev;
  if (_solver->getSolveS()){
    if (!_SDeviationChecker)
      _SDeviationChecker = new tracerDeviationChecker(_solver->getDofs()->SDof);
    maxDev = _SDeviationChecker->computeMaxChange(minVal, maxVal);
    if (SDev != NULL) (*SDev) = maxDev;
    Msg::Info("    S Min. %2.9e Max. %2.9e Dev. %2.9e", minVal,maxVal,maxDev);
  }
  if (_solver->getSolveT()){
    if (!_TDeviationChecker)
      _TDeviationChecker = new tracerDeviationChecker(_solver->getDofs()->TDof);
    maxDev = _TDeviationChecker->computeMaxChange(minVal, maxVal);
    if (TDev != NULL) (*TDev) = maxDev;
    Msg::Info("    T Min. %2.9e Max. %2.9e Dev. %2.9e", minVal,maxVal,maxDev);
  }
  if (_solver->getSolveSed()){
    if (!_sedDeviationChecker)
      _sedDeviationChecker = new tracerDeviationChecker(_solver->getDofs()->sedDof);
    maxDev = _sedDeviationChecker->computeMaxChange(minVal, maxVal);
    Msg::Info("    sed Min. %2.9e Max. %2.9e Dev. %2.9e", minVal,maxVal,maxDev);
  }
}
