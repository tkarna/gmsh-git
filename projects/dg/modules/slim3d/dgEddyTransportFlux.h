#ifndef _DG_EDDY_TRANSPORT_FLUX_H
#define _DG_EDDY_TRANSPORT_FLUX_H
#include "dgConservationLawFunction.h"


class dgEddyTransportFlux : public dgConservationLawFunction {
  class interface0;
  class source;
  class gradPsiTerm;
  const function *_cst, * _ind,* _Nsquare;
  
  public:
  void setup();
  dgEddyTransportFlux(const function *cst, const function *ind, const function *Nsquare);
  ~dgEddyTransportFlux();
};


// class binding;
// void dgEddyTransportFluxRegisterBindings(binding *b);
#endif
