#include "slim3dEquations.h"
#include "slim3dSolver.h"
#include "functionGeneric.h"
#include "dgSW3dVerticalModel.h"
#include "dgExtrusion.h"

slim3dSolverDofs::slim3dSolverDofs(slim3dSolver *solver): _solver(solver) {
  // init all dofs to NULL
  uDof = vDof = uvDof = uvDofCenter = uvSurfDof2d = uvAvDof2d = uvIntTargetDof3d = NULL;
  uvHorDivDof = NULL; 
  uvIntDof2d = uvCorrDof3d = uvBotDof2d = NULL;
  windStressDof2dCenter = NULL;
  uvTauBDof2d = uvTauBDof2dCenter = NULL;
  wDof3d = wSurfDof2d = wMeshSurfDof2d = NULL;
  etaDof2d = etaDof2dCG = etaDof3dCenter = NULL;
  rDof3d = rIntDof2d = rBotDof2d = NULL;
  rhoDof3d = rGradDof3d = rhoGradDof3d = rhoSurfDof2d = rGradIntDof2d = NULL;
  SDof = SDofCenter = TDof = TDofCenter = NULL;
  sedDof = sedBottomDof2d = sedGroundDof2d = sedBottomFluxDof2d = NULL;
  zDof = zDofCenter = zBotDof2d = zBotDof3d = NULL;
  uvDeviationProdIntDof2d = NULL;
  xyzOrigDof = nnDof = ssDof = nuvDof = nuvDofCenter = kappavDof = kappavDofCenter = NULL;
  tkeDof = tkeDofCenter = epsDof = epsDofCenter = lDof = NULL;
  uvGradDof  = uvGradDof2d= NULL;
  nbDof3d = rhoGradGMVelDof3d = GMVelDof3d = NULL;
  bathDof2d = NULL;
  _initialized = false;
}

void slim3dSolverDofs::initialize()
{
  if (_initialized)
    return;
  dgGroupCollection *groups3d = _solver->groups3d;
  dgGroupCollection *groups2d = _solver->groups2d;
  // TODO allocate according to solver options
  uvDof = new dgDofContainer(*groups3d, 2);
  uvHorDivDof = new dgDofContainer(*groups3d, 1);
  wDof3d = new dgDofContainer(*groups3d, 1);
  //wDof3dCopy = new dgDofContainer(*groups3d, 1);
  uvAvDof2d = new dgDofContainer(*groups2d, 2);
  uvIntTargetDof3d = new dgDofContainer(*groups3d, 2);
  uvIntDof2d = new dgDofContainer(*groups2d, 2);
  uvCorrDof3d = new dgDofContainer(*groups3d, 2);
  uvDeviationProdIntDof2d = new dgDofContainer(*groups2d, 3); // this is needed
  etaDof2d = new dgDofContainer(*groups2d, 1);
  etaDof2dCG = new dgDofContainer(*groups2d, 1);
  // pressure related
  if (true){
    rGradDof3d = new dgDofContainer(*groups3d, 2);
    rhoGradDof3d = new dgDofContainer(*groups3d, 2);
    rhoDof3d = new dgDofContainer(*groups3d, 1);
    rhoSurfDof2d = new dgDofContainer(*groups2d, 1);
    rGradIntDof2d = new dgDofContainer(*groups2d, 2);
  }
  else {
    rDof3d = new dgDofContainer(*groups3d, 1);
    rIntDof2d = new dgDofContainer(*groups2d, 1);
    rBotDof2d = new dgDofContainer(*groups2d, 1);
  }
  // bottom friction related
  uvBotDof2d = new dgDofContainer(*groups2d, 2);
  uvTauBDof2d = new dgDofContainer(*groups2d, 2);
  zBotDof2d = new dgDofContainer(*groups2d, 1);
  zBotDof3d = new dgDofContainer(*groups3d, 1);
  if ( _solver->_solveUVDiff ) {
    vDof = new dgDofContainer(*groups3d, 1);
    uDof = new dgDofContainer(*groups3d, 1);
  }
  // moving mesh related
  uvSurfDof2d = new dgDofContainer(*groups2d, 2);
  wSurfDof2d = new dgDofContainer(*groups2d, 1);
  wMeshSurfDof2d = new dgDofContainer(*groups2d, 1);
  wMeshSurfDof2d->setAll(0.);
  xyzOrigDof = new dgDofContainer(*groups3d, 3);
  zDof = new dgDofContainer(*groups3d, 1);
  if ( _solver->_solveS || _solver->getSolveSImplicitVerticalDiffusion() ||
    _solver->_solveTurbulence || _solver->getSolveGMVel() ) {
    SDof = new dgDofContainer(*groups3d, 1);
  }
  if ( _solver->_solveT || _solver->getSolveTImplicitVerticalDiffusion() ||
    _solver->_solveTurbulence || _solver->getSolveGMVel()) {
    TDof = new dgDofContainer(*groups3d, 1);
  }
  if ( _solver->_solveSed || _solver->getSolveSedImplicitVerticalDiffusion() ){
    sedDof = new dgDofContainer(*groups3d, 1);
    sedBottomDof2d = new dgDofContainer(*groups2d, 1);
    sedGroundDof2d = new dgDofContainer(*groups2d, 1);
    sedBottomFluxDof2d = new dgDofContainer(*groups2d, 1);
  }
  if ( _solver->_solveTurbulence ) {
    nuvDof = new dgDofContainer(*groups3d, 1);
    kappavDof = new dgDofContainer(*groups3d, 1);
    TDofCenter = new dgDofContainer(*groups3d, 1);
    SDofCenter = new dgDofContainer(*groups3d, 1);
    uvDofCenter = new dgDofContainer(*groups3d, 2);
    etaDof3dCenter = new dgDofContainer(*groups3d, 1);
    windStressDof2dCenter = new dgDofContainer(*groups2d, 2);
    uvTauBDof2dCenter = new dgDofContainer(*groups2d, 2);
    zDofCenter = new dgDofContainer(*groups3d, 1);
    nnDof = new dgDofContainer(*groups3d, 1);
    ssDof = new dgDofContainer(*groups3d, 1);
    nuvDofCenter = new dgDofContainer(*groups3d, 1);
    kappavDofCenter = new dgDofContainer(*groups3d, 1);
    tkeDof = new dgDofContainer(*groups3d, 1);
    tkeDofCenter = new dgDofContainer(*groups3d, 1);
    epsDof = new dgDofContainer(*groups3d, 1);
    epsDofCenter = new dgDofContainer(*groups3d, 1);
    lDof = new dgDofContainer(*groups3d, 1);
  }
  if ( _solver->getSolveUVGrad()){
    uvGradDof = new dgDofContainer(*groups3d, 6);
    uvGradDof2d = new dgDofContainer(*groups2d, 6);
  }
  if ( _solver->getSolveGMVel() ) {
    rhoGradGMVelDof3d = new dgDofContainer(*groups3d, 3);
    nbDof3d = new dgDofContainer(*groups3d, 1);
    streamFuncDof3d = new dgDofContainer(*groups3d, 3);
    GMVelDof3d = new dgDofContainer(*groups3d, 3);
  }
  bathDof2d = new dgDofContainer(*groups2d, 1);
  _initialized = true;
}

slim3dSolverDofs::~slim3dSolverDofs()
{
  if ( uvDof )             delete uvDof;
  if ( uvHorDivDof )       delete uvHorDivDof;
  if ( wDof3d )            delete wDof3d;
  if ( uvAvDof2d )         delete uvAvDof2d;
  if ( uvIntTargetDof3d )  delete uvIntTargetDof3d;
  if ( uvIntDof2d )        delete uvIntDof2d;
  if ( uvCorrDof3d )       delete uvCorrDof3d;
  if ( etaDof2d )          delete etaDof2d;
  if ( etaDof2dCG )        delete etaDof2dCG;
  if ( uvBotDof2d )        delete uvBotDof2d;
  if ( uvTauBDof2d )       delete uvTauBDof2d;
  if ( zBotDof2d )         delete zBotDof2d;
  if ( zBotDof3d )         delete zBotDof3d;
  if ( vDof )              delete vDof;
  if ( uDof )              delete uDof;
  if ( uvSurfDof2d )       delete uvSurfDof2d;
  if ( wSurfDof2d )        delete wSurfDof2d;
  if ( wMeshSurfDof2d )    delete wMeshSurfDof2d;
  if ( xyzOrigDof )        delete xyzOrigDof;
  if ( zDof )              delete zDof;
  if ( rDof3d )            delete rDof3d;
  if ( rIntDof2d )         delete rIntDof2d;
  if ( rBotDof2d )         delete rBotDof2d;
  if ( rGradDof3d)         delete rGradDof3d;
  if ( rhoGradDof3d)       delete rhoGradDof3d;
  if ( rhoDof3d )          delete rhoDof3d;
  if ( rhoSurfDof2d)       delete rhoSurfDof2d;
  if ( rGradIntDof2d )     delete rGradIntDof2d;
  if ( SDof )              delete SDof;
  if ( TDof )              delete TDof;
  if ( sedDof )            delete sedDof;
  if ( sedBottomDof2d )    delete sedBottomDof2d;
  if ( sedGroundDof2d )    delete sedGroundDof2d;
  if ( sedBottomFluxDof2d) delete sedBottomFluxDof2d;
  if ( nuvDof )            delete nuvDof;
  if ( kappavDof )         delete kappavDof;
  if ( TDofCenter )        delete TDofCenter;
  if ( SDofCenter )        delete SDofCenter;
  if ( uvDofCenter )       delete uvDofCenter;
  if ( etaDof3dCenter )    delete etaDof3dCenter;
  if ( windStressDof2dCenter ) delete windStressDof2dCenter;
  if ( uvTauBDof2dCenter ) delete uvTauBDof2dCenter;
  if ( zDofCenter )        delete zDofCenter;
  if ( nnDof )             delete nnDof;
  if ( ssDof )             delete ssDof;
  if ( nuvDofCenter )      delete nuvDofCenter;
  if ( kappavDofCenter )   delete kappavDofCenter;
  if ( tkeDof )            delete tkeDof;
  if ( tkeDofCenter )      delete tkeDofCenter;
  if ( epsDof )            delete epsDof;
  if ( epsDofCenter )      delete epsDofCenter;
  if ( lDof )              delete lDof;
  if (uvDeviationProdIntDof2d) delete uvDeviationProdIntDof2d;
  if ( nbDof3d )           delete nbDof3d;
  if ( uvGradDof )         delete uvGradDof;
  if ( uvGradDof2d )       delete uvGradDof2d;
  if ( rhoGradGMVelDof3d)  delete rhoGradGMVelDof3d;
  if ( streamFuncDof3d)    delete streamFuncDof3d;
  if ( GMVelDof3d)         delete GMVelDof3d;
  if ( bathDof2d)          delete bathDof2d;
  
}

slim3dSolverFunctions::slim3dSolverFunctions(slim3dSolver *solver) : _solver(solver)
{
  if (!solver->groups2d)
    Msg::Fatal("No 2d mesh loaded in solver");
  if (!solver->groups3d)
    Msg::Fatal("No 3d mesh loaded in solver");
  zeroFunc = new functionConstant(0.0);
  std::vector<double> zeroVec2(2,0.0);
  zeroFunc2 = new functionConstant(zeroVec2);
  std::vector<double> zeroVec(3,0.0);
  zeroFunc3 = new functionConstant(zeroVec);
  tinyFunc = new functionConstant(1e-10);
  oneFunc = new functionConstant(1.0);
  xyzFunc3d = solver->groups3d->getFunctionCoordinates();
  xyzFunc2d = solver->groups2d->getFunctionCoordinates();
  zFunc = functionExtractCompNew(xyzFunc3d,2);
  timeFunc = function::getTime();

  // functions depending on user input
  uvCorrectorFunc = NULL;
  pHydroStatFunc = rhoFunc = rhoFuncCenter = eosAlphaFunc = eosBetaFunc = NULL;
  SCenterGradientFunc = TCenterGradientFunc = SInitGradientFunc = TInitGradientFunc = NULL;
  bathFunc2d = bathFunc3d = bathGradFunc2d = bathGradFunc3d = NULL;
  coriolisFunc = NULL;
  TInitFunc = SInitFunc = TFunc = SFunc = sedInitFunc = NULL;
  uvInitFunc = uvAvInitFunc = etaInitFunc = wInitFunc = NULL;
  uvDevFunc = uvDevProdFunc = uvDevAdvectionFunc = NULL;
  newZFunc = wMeshSurfFunc = wMeshFunc = wMeshDzFunc = zBotDistFunc = NULL;
  nuvFunc = nuhTotal = kappavFunc = kappahTotal = NULL;
  _nuv0Func = _kapv0Func = NULL;
  z0BFunc = z0SFunc = uvTauBFunc2d = uvTauBFunc3d = windStressFunc = NULL;
  bottomFrictionFunc = NULL;
  bottomFrictionDragCoeff2d = bottomFrictionDragCoeff3d = bottomUVDeviation = NULL;
  SInitGradientFunc = TInitGradientFunc = zeroFunc3;
  Stereo2XYZ = Stereo2LonLat = Stereo2LonLat2d = NULL;
  XYZ2Stereo = LonLat2Stereo = NULL;
  slopeFunc = slopeTap = NULL;
  _bathDof3d = NULL;
  eadySpeedFunc = GMIndepTermFunc = GMVelFunc = NULL;
  _initialized = false;
}

void slim3dSolverFunctions::initialize()
{
  slim3dSolverDofs *dofs = _solver->getDofs();
  if (!dofs->isInitialized())
    Msg::Fatal("Dofs must be allocated before calling slim3dSolverFunctions::initialize()");
  if (! bathFunc3d && ! bathFunc2d && ! bathGradFunc3d && ! bathGradFunc2d) {
    dgBathymetryDofGenerator::extractFromMesh(_solver->columnInfo, dofs->bathDof2d);
    dofs->bathDof2d->scatter();
    _bathDof3d = new dgDofContainer(*_solver->groups3d, 1);
    dgSW3dDofCopy copy(_solver->columnInfo);
    copy.copyTo3D(dofs->bathDof2d, _bathDof3d);
    _bathDof3d->scatter();
    bathFunc2d = dofs->bathDof2d->getFunction();
    bathGradFunc2d = dofs->bathDof2d->getFunctionGradient();
    bathFunc3d = _bathDof3d->getFunction();
    bathGradFunc3d = _bathDof3d->getFunctionGradient();
  }
  else if (!bathFunc3d || !bathFunc2d || !bathGradFunc3d || !bathGradFunc2d) {
    Msg::Fatal("either bathFunc2d, bathFunc3d, bathGradFunc2d and bathGradFunc3d should be set or none of them");
  }
  else
    dofs->bathDof2d->interpolate(bathFunc2d);
  
  if (!SInitFunc && _solver->getSolveS())
    Msg::Fatal("SInitFunc must be set");
  if (!TInitFunc && _solver->getSolveT())
    Msg::Fatal("TInitFunc must be set");
  if (!sedInitFunc && _solver->getSolveSed())
    Msg::Fatal("sedInitFunc must be set");

  uvCorrectorFunc = new uvCorrector( dofs->uvIntDof2d->getFunction(),
                                     dofs->uvIntTargetDof3d->getFunction(),
                                     dofs->etaDof2dCG->getFunction(),
                                     bathFunc3d );
  uvDevFunc = new uvDeviation( dofs->uvDof->getFunction(),
                               dofs->uvIntDof2d->getFunction(),
                               dofs->etaDof2dCG->getFunction(),
                               bathFunc3d );
  uvDevProdFunc = new uvDeviationProducts(uvDevFunc);
  uvDevAdvectionFunc = new uvDevAdvectionTerm(dofs->uvDeviationProdIntDof2d->getFunctionGradient(),dofs->etaDof2dCG->getFunction(),bathFunc2d);
  // set T and S functions either to the dofs (that are marched in time) or to the initial condition
  const function *TCenterFunc=NULL, *SCenterFunc=NULL;
  if ( _solver->_solveS  || _solver->getSolveGMVel()) {
    dofs->SDof->interpolate(SInitFunc);    
    SFunc = dofs->SDof->getFunction();
    if ( _solver->_solveTurbulence ) {
      SCenterFunc = dofs->SDofCenter->getFunction();
      SCenterGradientFunc = dofs->SDofCenter->getFunctionGradient();
    }
  } else {
    SFunc = SCenterFunc = SInitFunc;
    SCenterGradientFunc = SInitGradientFunc;
  }
  if ( _solver->_solveT || _solver->getSolveGMVel()) {
    dofs->TDof->interpolate(TInitFunc);
    TFunc = dofs->TDof->getFunction();
    if ( _solver->_solveTurbulence ) {
      TCenterFunc = dofs->TDofCenter->getFunction();
      TCenterGradientFunc = dofs->TDofCenter->getFunctionGradient();
    }
  } else {
    TFunc = TCenterFunc = TInitFunc;
    TCenterGradientFunc = TInitGradientFunc;
  }
  // rho realated functions
  pHydroStatFunc = zeroFunc;
  if (! rhoFunc) {
    rhoFunc = new functionStateEquation(SFunc,TFunc,pHydroStatFunc,slim3dParameters::rho0);
  }
  else if (_solver->_solveTurbulence) {
    Msg::Fatal("changing the state equation while solving the turbulence is not implemented");
  }

  if (_solver->_solveTurbulence) {
    rhoFuncCenter = new functionStateEquation( SCenterFunc , TCenterFunc,
                                           pHydroStatFunc,slim3dParameters::rho0);
    eosAlphaFunc = new functionStateEquationAlpha(SCenterFunc , TCenterFunc,
                                                 pHydroStatFunc, slim3dParameters::rho0);
    eosBetaFunc = new functionStateEquationBeta( SCenterFunc, TCenterFunc,
                                                 pHydroStatFunc, slim3dParameters::rho0);
  }
  //std::vector<int> xyComp(2,0);
  //xyComp[0] = 0;
  //xyComp[1] = 1;
  //rGradFunc = functionExtractCompNew(dofs->rGradDof3d->getFunction(), xyComp);
  // moving mesh
  const function *xyzOrigFunc3d = dofs->xyzOrigDof->getFunction();
  const function *etaGeometryFunc = dofs->etaDof2dCG->getFunction();
  const function *etaGeometryGradientFunc2d = dofs->etaDof2dCG->getFunctionGradient();
  const function *wSurfFunc2d = dofs->wSurfDof2d->getFunction();
  const function *uvSurfFunc2d= dofs->uvSurfDof2d->getFunction();
  const function *wMeshSurfDofFunc = dofs->wMeshSurfDof2d->getFunction();
  newZFunc = new zNew(xyzOrigFunc3d,etaGeometryFunc,bathFunc3d);
  wMeshSurfFunc = new wMeshSurf(etaGeometryGradientFunc2d, wSurfFunc2d,uvSurfFunc2d);
  // NOTE using wMeshSurfFunc directly here fails in parallel
  wMeshFunc = new wMesh(xyzOrigFunc3d, etaGeometryFunc,bathFunc3d,wMeshSurfDofFunc);
  wMeshDzFunc = new dwMeshDz(wMeshSurfDofFunc,etaGeometryFunc,bathFunc3d);
  // diffusivity
  _nuv0Func = new functionConstant(slim3dParameters::nuv0);
  // TODO: differentiate between kapv0S and kapv0T ?
  _kapv0Func = new functionConstant(slim3dParameters::kapv0S);
  if (_solver->_solveTurbulence) {
    nuvFunc = functionSumNew(dofs->nuvDof->getFunction(),_nuv0Func);
    kappavFunc = functionSumNew(dofs->kappavDof->getFunction(),_kapv0Func);
  }
  else {
    if ( !nuvFunc && _solver->getSolveUVImplicitVerticalDiffusion() ) {
      Msg::Warning("nuvFunction not defined, using molecular diffusivity");
      nuvFunc = _nuv0Func;
    }
    if ( !kappavFunc && (_solver->getSolveSImplicitVerticalDiffusion() ||
                         _solver->getSolveTImplicitVerticalDiffusion() ||
                         _solver->getSolveSedImplicitVerticalDiffusion() ) ) {
      Msg::Warning("kappavFunction not defined, using molecular diffusivity");
      kappavFunc = _kapv0Func;
    }
  }
  // bottom friction
  if ( _solver->getComputeBottomFriction() ) {
    // TODO rename to bottomRoughnessFunc
    if (!z0BFunc)
      Msg::Fatal("bottomRoughnessFunc must be set");
    if (!z0SFunc)
      Msg::Fatal("surfaceRoughnessFunc must be set");
    // TODO rename
    const function *zBottomFunc2d = dofs->zBotDof2d->getFunction();
    const function *zBottomFunc3d = dofs->zBotDof3d->getFunction();
    const function *uvBottomFunc2d = dofs->uvBotDof2d->getFunction();
    const function *uvIntFunc2d = dofs->uvIntDof2d->getFunction();    
    const function *etaGeometryFunc2d = dofs->etaDof2dCG->getFunction();
    // bottom friction velocity in 2d and 3d geometries

    //if ( !windStressFunc )
    // windStressFunc = zeroFunc3;
    zBotDistFunc = new zBotDist(bathFunc3d, zBottomFunc3d);
    if (!bottomFrictionDragCoeff2d & !bottomFrictionDragCoeff3d){
      bottomFrictionDragCoeff2d = new bottomFrictionDragCoeffVonKarman(bathFunc2d, zBottomFunc2d, z0BFunc);
      bottomFrictionDragCoeff3d = new bottomFrictionDragCoeffVonKarman(bathFunc3d, zBottomFunc3d, z0BFunc);
    }
    else if (!bottomFrictionDragCoeff2d || !bottomFrictionDragCoeff3d)
      Msg::Fatal("either bottomFrictionDragCoeff2d and bottomFrictionDragCoeff3d should be set or none of them");
    else if (_solver->getSolveTurbulence())
      Msg::Info("Not sure that GOTM can be used without von Karman bottom friction parametrization\n");
    if (_solver->_solveTurbulence) {
      uvTauBFunc2d = new uvBotStar(bottomFrictionDragCoeff2d, uvBottomFunc2d);
      uvTauBFunc3d = new uvBotStar(bottomFrictionDragCoeff2d, uvBottomFunc2d);
    }
    
    bottomFrictionFunc = new dragNormUb(bottomFrictionDragCoeff2d, uvBottomFunc2d);
    bottomUVDeviation = new bottomVelocityDeviation(bathFunc2d,etaGeometryFunc2d,uvBottomFunc2d,
                                                    uvIntFunc2d);
  }
  if(_solver->getUseStereo()){
    Stereo2XYZ = new transformStereo2XYZ(dofs->uvDof->getFunction(), xyzFunc3d, slim3dParameters::R);
    Stereo2LonLat = new transformStereo2LonLat(dofs->uvDof->getFunction(), xyzFunc3d, slim3dParameters::R);
    Stereo2LonLat2d = new transformStereo2LonLat(dofs->uvAvDof2d->getFunction(), xyzFunc2d, slim3dParameters::R);
    LonLat2Stereo = new transformLonLat2Stereo(dofs->uvDof->getFunction(), xyzFunc3d, slim3dParameters::R);
    XYZ2Stereo = new transformXYZ2Stereo(dofs->uvDof->getFunction(), xyzFunc3d, slim3dParameters::R);
  }
  if(_solver->getSolveGMVel()){
    eadySpeedFunc = new eadySpeed(dofs->nbDof3d->getFunction(), bathFunc3d, dofs->etaDof2d->getFunction());
    GMIndepTermFunc = new GMIndepTerm(dofs->rhoGradDof3d->getFunction());
    const function *streamGradFunc = dofs->streamFuncDof3d->getFunctionGradient();
    GMVelFunc = new GMVelocity(streamGradFunc);
  }
  if(_solver->getSolveIsoDiff()){
    slopeFunc = new slopeFunction(dofs->rhoGradDof3d->getFunction(), dofs->nbDof3d->getFunction());
    slopeTap = new slopeTapering(dofs->rhoGradDof3d->getFunction(), dofs->nbDof3d->getFunction());
    if(!_solver->getSolveTurbulence())
      kappavFunc = new kappavConvFunc(dofs->nbDof3d->getFunction()); 
  }
  if(_solver->getSolveSed()){
    wSedFunc = new verticalVelocitySediment(dofs->sedDof->getFunction(), dofs->wDof3d->getFunction(), dofs->SDof->getFunction());
    sedBottomFluxFunc2d = new sedimentBottomFlux(dofs->sedBottomDof2d->getFunction(), dofs->uvBotDof2d->getFunction(), windStressFunc, bathFunc2d, dofs->sedGroundDof2d->getFunction(), dofs->SDof->getFunction());
  }
  _initialized= true;
}

void slim3dSolverFunctions::initializeBath()
{
  slim3dSolverDofs *dofs = _solver->getDofs();
  if (!dofs->isInitialized())
    Msg::Fatal("Dofs must be allocated before calling slim3dSolverFunctions::initialize()");
  if (! bathFunc3d && ! bathFunc2d && ! bathGradFunc3d && ! bathGradFunc2d) {
    dgBathymetryDofGenerator::extractFromMesh(_solver->columnInfo, dofs->bathDof2d);
    dofs->bathDof2d->scatter();
    _bathDof3d = new dgDofContainer(*_solver->groups3d, 1);
    dgSW3dDofCopy copy(_solver->columnInfo);
    copy.copyTo3D(dofs->bathDof2d, _bathDof3d);
    _bathDof3d->scatter();
    bathFunc2d = dofs->bathDof2d->getFunction();
    bathGradFunc2d = dofs->bathDof2d->getFunctionGradient();
    bathFunc3d = _bathDof3d->getFunction();
    bathGradFunc3d = _bathDof3d->getFunctionGradient();
  }
  else if (!bathFunc3d || !bathFunc2d || !bathGradFunc3d || !bathGradFunc2d) {
    Msg::Fatal("either bathFunc2d, bathFunc3d, bathGradFunc2d and bathGradFunc3d should be set or none of them");
  }
  else
    dofs->bathDof2d->interpolate(bathFunc2d);
}

slim3dSolverFunctions::~slim3dSolverFunctions()
{
  if (zeroFunc) delete zeroFunc;
  if (zeroFunc3) delete zeroFunc3;
  if (tinyFunc) delete tinyFunc;
  if (oneFunc) delete oneFunc;
  if (uvCorrectorFunc) delete uvCorrectorFunc;
  if (rhoFunc) delete rhoFunc;
  if (rhoFuncCenter) delete rhoFuncCenter;
  if (eosAlphaFunc) delete eosAlphaFunc;
  if (eosBetaFunc) delete eosBetaFunc;
  if (newZFunc) delete newZFunc;
  if (wMeshSurfFunc) delete wMeshSurfFunc;
  if (wMeshFunc) delete wMeshFunc;
  if (wMeshDzFunc) delete wMeshDzFunc;
  if (_nuv0Func) delete _nuv0Func;
  if (_kapv0Func) delete _kapv0Func;
  if (nuvFunc) delete nuvFunc;
  if (kappavFunc) delete kappavFunc;
  if (uvTauBFunc2d) delete uvTauBFunc2d;
  if (uvTauBFunc3d) delete uvTauBFunc3d;
  if (zBotDistFunc) delete zBotDistFunc;
  if (bottomFrictionFunc) delete bottomFrictionFunc;
  if (eadySpeedFunc) delete eadySpeedFunc;
  if (GMIndepTermFunc) delete GMIndepTermFunc;
  if (GMVelFunc) delete GMVelFunc;
  if (slopeFunc) delete slopeFunc;
  if (slopeTap) delete slopeTap;
  if (_bathDof3d) delete _bathDof3d;
  if (wSedFunc) delete wSedFunc;
  if (sedBottomFluxFunc2d) delete sedBottomFluxFunc2d;
}


slim3dSolverEquations::~slim3dSolverEquations()
{
  delete horMomEq;
  delete vertMomUEq;
  delete wEq;
  delete newRGradEq;
#ifdef HAVE_GOTM
  if ( turbMod ) delete turbMod;
#endif
  delete SEq;
  delete TEq;
  delete tkeAdvEq;
  delete epsAdvEq;
  delete SDiffEq;
  delete TDiffEq;
  delete sedDiffEq;
  delete depthIntegratorRho;
  delete eta2dEq;
  delete uv2dEq;
  delete uvHorDivEq;
  if ( _solver->getSolveUVGrad())
    delete uvGradEq;
  if ( _solver->getSolveGMVel() ) {
    delete rhoGradEq;
    delete nbMod;
    delete rhoGradContProj;
    delete streamFuncContProj;
    delete GMEq;
  }
}

slim3dSolverEquations::slim3dSolverEquations(slim3dSolver* solver) : _solver(solver)
{
  // create all equations with no arguments
  horMomEq = new dgConservationLawSW3dMomentum();
  vertMomUEq = new dgConservationLawSW3dMomWx();
  wEq = new dgConservationLawSW3dContinuity(0);
  newRGradEq = new dgConservationLawSW3dPressure(NULL, INTEGRATE_FROM_TOP);
#ifdef HAVE_GOTM
  turbMod = NULL;
#endif
  SEq = new dgConservationLawSW3dTracer();
  TEq = new dgConservationLawSW3dTracer();
  sedEq = new dgConservationLawSW3dTracer();
  tkeAdvEq = new dgConservationLawSW3dTracer();
  epsAdvEq = new dgConservationLawSW3dTracer();
  SDiffEq = new dgConservationLawSW3dTracerVDiff();
  TDiffEq = new dgConservationLawSW3dTracerVDiff();
  sedDiffEq = new dgConservationLawSW3dTracerVDiff();
  depthIntegratorRho = new dgSW3dDepthIntegrate(1,INTEGRATE_FROM_TOP);
  eta2dEq = new dgConservationLawSW2dEta();
  uv2dEq = new dgConservationLawSW2dU();
  uvHorDivEq = new dgUVHorDivergence();
  uvGradEq = NULL;
  rhoGradEq = NULL;
  rhoGradContProj = NULL;
  streamFuncContProj = NULL;
  nbMod = NULL;
//GMEq = new dgEddyTransportFlux();
  GMEq = NULL;
  _initialized = false;
}

void slim3dSolverEquations::initialize()
{
  slim3dSolverDofs* dofs = _solver->getDofs();
  if (!dofs->isInitialized())
    Msg::Fatal("Dofs must be allocated before calling slim3dSolverEquations::initialize()");
  slim3dSolverFunctions* funcs = _solver->functions;
  if (!funcs->isInitialized())
    Msg::Fatal("Functions must be allocated before calling slim3dSolverEquations::initialize()");
  if ( _solver->getSolveTurbulence() && !_solver->getComputeBottomFriction() )
    Msg::Fatal("Turbulence closure model requires bottom friction");
  // turbulence
#ifdef HAVE_GOTM
  if ( _solver->_solveTurbulence ) {
    turbMod = new dgSW3dTurbulenceGOTM(_solver->columnInfo,_solver->turbulenceSetupFile);
    // TODO check which of these are not actually needed!
    turbMod->setZFunction(dofs->zDofCenter->getFunction());
    turbMod->setUVFunction(dofs->uvDofCenter->getFunction());
    turbMod->setUVGradientFunction(dofs->uvDofCenter->getFunctionGradient());
    turbMod->setEOSAlphaFunction(funcs->eosAlphaFunc);
    turbMod->setEOSBetaFunction(funcs->eosBetaFunc);
    turbMod->setSGradientFunction(funcs->SCenterGradientFunc);
    turbMod->setTGradientFunction(funcs->TCenterGradientFunc);
    turbMod->setZ0SFunction(funcs->z0SFunc);
    turbMod->setZ0BFunction(funcs->z0BFunc);
    if ( funcs->windStressFunc)
      turbMod->setWindStressFunction(dofs->windStressDof2dCenter->getFunction());
    else
      turbMod->setWindStressFunction(funcs->zeroFunc3);
    turbMod->setUVStarBFunction(dofs->uvTauBDof2dCenter->getFunction());
    turbMod->setNuvContainer(dofs->nuvDofCenter);
    turbMod->setKappavContainer(dofs->kappavDofCenter);
    turbMod->setTkeContainer(dofs->tkeDofCenter);
    turbMod->setEpsContainer(dofs->epsDofCenter);
    turbMod->setLContainer(dofs->lDof);
    turbMod->setSSContainer(dofs->ssDof);
    turbMod->setNNContainer(dofs->nnDof);
    turbMod->setSContainer(dofs->SDofCenter);
    turbMod->setTContainer(dofs->TDofCenter);
  }
#else
  if ( _solver->_solveTurbulence )
    Msg::Fatal("GOTM not compiled");
#endif
    
  if ( _solver->getSolveGMVel() ){
    nbMod = new dgSW3dVerticalModel(_solver->columnInfo);
    nbMod->setComputeNNWithEOS(true);
    nbMod->setZFunction(funcs->newZFunc);
    nbMod->setSContainer(dofs->SDof);
    nbMod->setTContainer(dofs->TDof);
    
  }
  const function *bath3d = funcs->bathFunc3d;
  const function *eta2d = dofs->etaDof2d->getFunction();
  const function *bath2d = funcs->bathFunc2d;
  const function *uv3d = dofs->uvDof->getFunction();
  const function *uvInt2d = dofs->uvIntDof2d->getFunction();
  const function *uvAv2d = dofs->uvAvDof2d->getFunction();
  const function *w3d = dofs->wDof3d->getFunction();
  //const function *w3dCopy = dofs->wDof3dCopy->getFunction();
  const function *wMesh = funcs->wMeshFunc;
  const function *wMeshDz = funcs->wMeshDzFunc;
  const function *rGrad = dofs->rGradDof3d->getFunction();
  //const function *r = dofs->rDof3d->getFunction();
  const function *rGradInt2d = dofs->rGradIntDof2d->getFunction();
  const function *rhoSurf2d = dofs->rhoSurfDof2d->getFunction();
  //const function *rBot2d = dofs->rBotDof2d->getFunction();
  const function* bottomFrictionDragCoeff2d = funcs->bottomFrictionDragCoeff2d;
  const function* bottomUVDeviation = funcs->bottomUVDeviation;
  const function* wSed = funcs->wSedFunc;
  horMomEq->setBathymetry(bath3d);
  horMomEq->setEta(eta2d);
  horMomEq->setUV2d(uvInt2d);
  horMomEq->setW(w3d);
  horMomEq->setWMesh(wMesh);
  horMomEq->setWMeshDz(wMeshDz);
  //horMomEq->setR(r);
  horMomEq->setRGrad(rGrad);
  horMomEq->setRhoSurf(rhoSurf2d);
  if (funcs->coriolisFunc)
    horMomEq->setCoriolisFactor(funcs->coriolisFunc);
  if (funcs->nuhTotal)
    horMomEq->setNuH(funcs->nuhTotal);
  if (!_solver->_solveUVDiff && funcs->nuvFunc) {
    Msg::Warning("Vertical diffusion equation of UV disabled, assigning nuv to horizontal momentum equation");
    horMomEq->setNuV(funcs->nuvFunc);
  }
  if ( _solver->_solveUVDiff ) {
    vertMomUEq->setNuV(funcs->nuvFunc);
  }

  wEq->setUV(uv3d);
  wEq->setEta(eta2d);
  wEq->setBathymetry(bath3d);
  //wEq->setW(w3dCopy);
 
  newRGradEq->setRhoFunction(funcs->rhoFunc);
  dgBoundaryCondition* newRGradSymmBnd = newRGradEq->newSymmetryBoundary("");
  newRGradEq->addBoundaryCondition(_solver->physicalTags2d,newRGradSymmBnd);

  if ( _solver->_solveS ) {
    SEq->setUV(uv3d);
    SEq->setW(w3d);
    SEq->setEta(eta2d);
    SEq->setBath(bath3d);
    SEq->setWMesh(wMesh);
    SEq->setWMeshDz(wMeshDz);
    if (funcs->kappahTotal)
      SEq->setKappaH(funcs->kappahTotal);
    if (!_solver->_solveSDiff && funcs->kappavFunc) {
      Msg::Warning("Vertical diffusion equation of S disabled, assigning kappav to S equation");
      SEq->setKappaV(funcs->kappavFunc);
    }
    if (_solver->getSolveGMVel()) {
      SEq->setGMVel(dofs->GMVelDof3d->getFunction());
      SEq->setKappaV(funcs->kappavFunc);
    }
    if (_solver->getSolveIsoDiff()){
      if (!_solver->getSolveGMVel())
	Msg::Fatal("to Solve Iso Diff, solving GMVel is required!");
      SEq->setIsoDiff(funcs->slopeFunc, funcs->slopeTap);
    }
  }

  if ( _solver->_solveSDiff ) {
    SDiffEq->setBath(bath3d);
    SDiffEq->setKappaV(funcs->kappavFunc);
    dgBoundaryCondition* SDiffZeroBnd = SDiffEq->new0FluxBoundary();
    SDiffEq->addBoundaryCondition(_solver->physicalTags2d,SDiffZeroBnd);
  }

  if ( _solver->_solveT ) {
    TEq->setUV(uv3d);
    TEq->setW(w3d);
    TEq->setEta(eta2d);
    TEq->setBath(bath3d);
    TEq->setWMesh(wMesh);
    TEq->setWMeshDz(wMeshDz);
    if (funcs->kappahTotal)
      TEq->setKappaH(funcs->kappahTotal);
    if (!_solver->_solveTDiff && funcs->kappavFunc) {
      Msg::Warning("Vertical diffusion equation of T disabled, assigning kappav to T equation");
      TEq->setKappaV(funcs->kappavFunc);
    }
    if (_solver->getSolveGMVel()){
      rhoGradEq = new dgFEGradient(funcs->rhoFunc);
      dgBoundaryCondition* rhoGradSymmBnd = rhoGradEq->newSymmetryBoundary();
      rhoGradEq->addBoundaryCondition(_solver->physicalTags2d,rhoGradSymmBnd);
      TEq->setGMVel(dofs->GMVelDof3d->getFunction());
      TEq->setKappaV(funcs->kappavFunc);
    }
    if (_solver->getSolveIsoDiff()){
      if (!_solver->getSolveGMVel())
	Msg::Fatal("to Solve Iso Diff, solving GMVel is required!");
      TEq->setIsoDiff(funcs->slopeFunc, funcs->slopeTap);
    }
  }

  if ( _solver->_solveTDiff ) {
    TDiffEq->setBath(bath3d);
    TDiffEq->setKappaV(funcs->kappavFunc);
    dgBoundaryCondition* TDiffZeroBnd = TDiffEq->new0FluxBoundary();
    TDiffEq->addBoundaryCondition(_solver->physicalTags2d,TDiffZeroBnd);
  }
  
  if ( _solver->_solveSed ){
    sedEq->setUV(uv3d);
    sedEq->setW(wSed);
    sedEq->setEta(eta2d);
    sedEq->setBath(bath3d);
    sedEq->setWMesh(wMesh);
    sedEq->setWMeshDz(wMeshDz);
    if (funcs->kappahTotal)
      sedEq->setKappaH(funcs->kappahTotal);
    if (!_solver->_solveSedDiff && funcs->kappavFunc) {
      Msg::Warning("Vertical diffusion equation of SedEq disabled, assigning kappav to Sediment equation");
      sedEq->setKappaV(funcs->kappavFunc);
    }
    dgBoundaryCondition* sedBottomFluxBnd = sedEq->newNeumannBoundary(dofs->sedBottomFluxDof2d->getFunction()); // must be the dof and not directly the function. To not be updated between sedBottom Equation and sed Equation
    sedEq->addBoundaryCondition(_solver->bottomTags , sedBottomFluxBnd);
  }
  
  if ( _solver->_solveSedDiff ) {
    sedDiffEq->setBath(bath3d);
    sedDiffEq->setKappaV(funcs->kappavFunc);
    dgBoundaryCondition* sedDiffZeroBnd = sedDiffEq->new0FluxBoundary();
    sedDiffEq->addBoundaryCondition(_solver->physicalTags2d,sedDiffZeroBnd);
  }
  
  if ( _solver->_advectTKE ) {
    tkeAdvEq->setUV(uv3d);
    tkeAdvEq->setW(w3d);
    tkeAdvEq->setEta(eta2d);
    tkeAdvEq->setBath(bath3d);
    tkeAdvEq->setWMesh(wMesh);
    tkeAdvEq->setWMeshDz(wMeshDz);
    epsAdvEq->setUV(uv3d);
    epsAdvEq->setW(w3d);
    epsAdvEq->setEta(eta2d);
    epsAdvEq->setBath(bath3d);
    epsAdvEq->setWMesh(wMesh);
    epsAdvEq->setWMeshDz(wMeshDz);
  }

  eta2dEq->setBathymetry(bath2d);
  if ( _solver->_useModeSplit )
    eta2dEq->setUV(uvAv2d);
  else {
    eta2dEq->setUsesDepthIntegratedUV(true);
    eta2dEq->setUV(uvInt2d);
  }
  uv2dEq->setBathymetry(bath2d);
  uv2dEq->setEta(eta2d);
  //uv2dEq->setRInt(rInt2d);
  //uv2dEq->setRInt(funcs->zeroFunc);
  //uv2dEq->setRBot(rBot2d);
  uv2dEq->setRGradInt(rGradInt2d);
  uv2dEq->setRhoSurf(rhoSurf2d);
  if (funcs->windStressFunc)
    uv2dEq->setWindStress(funcs->windStressFunc);
  
  if ( _solver->getComputeBottomFriction() ) {
    uv2dEq->setBottomFrictionCoefficient(bottomFrictionDragCoeff2d);
    uv2dEq->setBottomUV(bottomUVDeviation);
  }
  if (funcs->coriolisFunc)
    uv2dEq->setCoriolisFactor(funcs->coriolisFunc);
  if (funcs->nuhTotal)
    uv2dEq->setNuH(funcs->nuhTotal);
  if (funcs->uvDevAdvectionFunc)
    uv2dEq->setSource(funcs->uvDevAdvectionFunc);
  
  uvHorDivEq->setBath(bath3d);
  uvHorDivEq->setUV(uv3d);
  uvHorDivEq->setEta(eta2d);
  dgBoundaryCondition* uvHorDivBoundaryWall = uvHorDivEq->newBoundaryWall();
  uvHorDivEq->addBoundaryCondition(_solver->physicalTags2d,uvHorDivBoundaryWall);

  if (_solver->getSolveUVGrad()){
    uvGradEq = new dgFEVectorGradient(uv3d);
    uvGradEq->setBath(bath3d);
    uvGradEq->setEta(eta2d);
    dgBoundaryCondition* uvGradBndCond = uvGradEq->newSymmetryBoundary();
    uvGradEq->addBoundaryCondition(_solver->physicalTags2d, uvGradBndCond);
  }
  
  depthIntegratorRho->setFunction(funcs->rhoFunc);
  dgBoundaryCondition* rZeroBnd = depthIntegratorRho->new0FluxBoundary();
  dgBoundaryCondition* rSymmBnd = depthIntegratorRho->newSymmetryBoundary("");
  for (size_t i = 0; i < _solver->physicalTags2d.size(); i++ ) {
    if ( _solver->physicalTags2d[i] != "top" )
      depthIntegratorRho->addBoundaryCondition(_solver->physicalTags2d[i],rZeroBnd);
    else
      depthIntegratorRho->addBoundaryCondition(_solver->physicalTags2d[i],rSymmBnd);
  }
  
  if (_solver->getSolveGMVel()){
    GMEq = new dgEddyTransportFlux(funcs->eadySpeedFunc, funcs->GMIndepTermFunc, dofs->nbDof3d->getFunction());
    //GMEq->setFunction(funcs->eadySpeedFunc, funcs->GMIndepTermFunc, dofs->nbDof3d->getFunction());
    dgBoundaryCondition* GMEqBnd = GMEq->new0FluxBoundary();
    GMEq->addBoundaryCondition(_solver->physicalTags2d,GMEqBnd);
    for (size_t i = 0; i < _solver->physicalTags2d.size(); ++i)
      GMEq->addStrongBoundaryCondition(2,_solver->physicalTags2d[i],funcs->zeroFunc3);// **DEMANDER A JON**
    // c'est un probleme de definition des physical tag
  }
  _initialized = true;
}
