#include "dgConservationLawFunction.h"
#include "fullMatrix.h"
#include "dgGroupOfElements.h"
#include "SPoint3.h"
#include "functionGeneric.h"
#include "dgConservationLawSW3dTracer.h"
#include "dgGroupOfElements.h"
#include "dgFunctionConstantByElement.h"
#include "slim3dFunctions.h"
#include "dgMeshJacobian.h"

class dgConservationLawSW3dTracer :: isopycnalDiffusion : public function {
  fullMatrix <double> _solgrad, _slope, _Kedd;
  public:
    isopycnalDiffusion(const function *slope, const function *Kedd): function (3) {
      setArgument(_Kedd,Kedd);
      setArgument(_slope,slope);
      setArgument(_solgrad,function::getSolutionGradient());
    };
    void call (dataCacheMap *m, fullMatrix<double> &val) {
      size_t nQP = val.size1();
      double alpha;
      for(size_t i=0; i< nQP; i++) {
        //Normal;
        alpha=0.0000001;
        val(i,0) = -_Kedd(i,0)*( _solgrad(i,0)+_slope(i,0)*_solgrad(i,2));
        val(i,1) = -_Kedd(i,0)*( _solgrad(i,1)+_slope(i,1)*_solgrad(i,2));
        val(i,2) = -_Kedd(i,0)*(_slope(i,0)*_solgrad(i,0)+_slope(i,1)*_solgrad(i,1)+(alpha+_slope(i,0)*_slope(i,0)+_slope(i,1)*_slope(i,1))*_solgrad(i,2));
       //MMS
       //alpha=1.e-3;
       //val(i,0)=-_Kedd(i,0)*_solgrad(i,0);
       //val(i,1)=-_Kedd(i,0)*_solgrad(i,1)*alpha;
       //val(i,2)=0;
    } 
  }
};

class dgConservationLawSW3dTracer::interfaceIso : public function {
  fullMatrix <double> normals, slope, Kedd;
  public:
    interfaceIso(const function *slopeF, const function *KeddF): function (1) {
      setArgument(Kedd, KeddF);
      setArgument(slope, slopeF);
      setArgument(normals, function::getNormals());
    };
    void call (dataCacheMap *m, fullMatrix<double> &val) {
      size_t nQP = val.size1();
      double alpha;
      for(size_t i=0; i< nQP; i++) {
        double Sn[3];
        double nx = normals(i,0);
        double ny = normals(i,1);
        double nz = normals(i,2);
        const double sx = slope(i, 0);
        const double sy = slope(i, 1);
        const double k = Kedd(i, 0);
        //Normal
        //Sn = n . K
        alpha = 0.0000001;
        Sn[0] = k * (nx + sx * nz);
        Sn[1] = k * (ny + sy * nz);
        Sn[2] = k * (nx * sx + ny * sy + (alpha + sx * sx + sy * sy) * nz);

        //MMS
        //Sn[0] = k * (nx);
        //Sn[1] = k * (ny*alpha);
        //Sn[2] = 0;

 
	val(i, 0) = Sn[0] * nx + Sn[1] * ny + Sn[2] * nz; //Normal ou MMS(oriented)
        //val(i, 0) = (Sn[0] * nx + Sn[1] * ny*alpha + Sn[2] * nz)/alpha; //MMS(eigenvalue)
        //val(i,0) = 1.e3; //MMS(riviere pure)
      }
    }
};

class dgConservationLawSW3dTracer::maxConvectiveSpeed : public function {
  fullMatrix<double> uv, w;
  public:
  maxConvectiveSpeed(const function *uvF, const function *wF): function(1) {
    setArgument(uv, uvF);
    setArgument(w, wF);
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++) {
      val(i,0) = sqrt( uv(i,0)*uv(i,0) + uv(i,1)*uv(i,1) + w(i,0)*w(i,0) );
    }
  }
};

class dgConservationLawSW3dTracer::source : public function {
  fullMatrix<double> _sol, _dwMdz, _xyz, solgrad, _nuH, _sourceFunc, _ref;
  double _R;
  int _dt,_T;
  bool havedw, haveNu, haveSource, haveCall;
  dgConservationLawSW3dTracer *_claw;
  public:
  source(dgConservationLawSW3dTracer *claw, const function *dwMdz, const function *nuH, const function *source, double R, int dt, int T, const function *ref): function(1) {
    _dt=dt;
    _T = T;
    _R=R;
    havedw = dwMdz;
    haveNu = nuH;
    haveSource = source;
    haveCall = ref;
    setArgument (_sol, function::getSolution());
    setArgument ( _xyz, function::getCoordinates());
    setArgument(solgrad,function::getSolutionGradient());
    if (haveCall)
      setArgument (_ref, ref);
    if (haveSource)
      setArgument (_sourceFunc, source);
    if(haveNu && _R>0)
      setArgument(_nuH,nuH);
    if(havedw)
      setArgument ( _dwMdz, dwMdz);
    _claw = claw;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    val.setAll (0.);
    for(int i=0; i< val.size1(); i++) {
      if (haveSource)
        val(i,0) += _sourceFunc(i,0);
      if(haveCall)
        val(i,0) += (_ref(i,0)-_sol(i,0))*_dt/_T;
      if(havedw && !_claw->_useConservativeALE)
        val(i,0) += -_sol(i,0)*_dwMdz(i,0);
      if(_R > 0 && haveNu)
      {
        double J = 4*_R*_R/(4*_R*_R+_xyz(i,0)*_xyz(i,0)+_xyz(i,1)*_xyz(i,1));
        val(i,0) += (_xyz(i,0)*solgrad(i,0)/J*_nuH(i,0) + _xyz(i,1)*solgrad(i,1)/J*_nuH(i,0))/_R/_R;
      }
    }
  }
};

class dgConservationLawSW3dTracer::advection : public function {
  fullMatrix<double> _c, _uv, _w, _wMesh, _diffusiveFlux,_isopycnalDiffusion, _xyz, _uGM;
  double _R;
  bool haveV, haveWMesh, haveNu, haveIso, haveGM;
  public:
  advection(const function *uv,const function *w,const function *wM, const function *diffusiveFluxF, const function *isopycnalDiffusion, double R, const function *uGM): function(3) {
    haveV = uv;
    haveNu = diffusiveFluxF;
    haveWMesh = wM;
    haveIso = isopycnalDiffusion;
    haveGM = uGM;
    _R = R;
    setArgument ( _c, function::getSolution());
    setArgument ( _xyz, function::getCoordinates());
    if (haveIso)
      setArgument (_isopycnalDiffusion,isopycnalDiffusion);
    if (haveV) {
      setArgument ( _uv, uv);
      setArgument ( _w, w);
    }
    if (haveWMesh)
      setArgument ( _wMesh, wM);
    if (haveNu) {
      setArgument ( _diffusiveFlux, diffusiveFluxF);
    }
    if (haveGM) {
      setArgument ( _uGM, uGM);
    }
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    val.scale (0);
    for(int i=0; i< val.size1(); i++) {
      double J = 1.0;
       if(_R > 0)
         J = 4*_R*_R/(4*_R*_R+_xyz(i,0)*_xyz(i,0)+_xyz(i,1)*_xyz(i,1));
      if (haveV) {
        val(i,0) = _c(i,0)*_uv(i,0)/J;
        val(i,1) = _c(i,0)*_uv(i,1)/J;
        val(i,2) = _c(i,0)*_w(i,0);
      }
      if (haveGM) {
        val(i,0) += _c(i,0)*_uGM(i,0)/J;
        val(i,1) += _c(i,0)*_uGM(i,1)/J;
        val(i,2) += _c(i,0)*_uGM(i,2);
      }
      if (haveWMesh)
        val(i,2) += _c(i,0)*(-_wMesh(i,0));
      if (haveNu) {
        val(i,0) += _diffusiveFlux(i,0);
        val(i,1) += _diffusiveFlux(i,1);
        val(i,2) += _diffusiveFlux(i,2);
      }
      if (haveIso) {
        val(i,0) +=  _isopycnalDiffusion(i,0);
        val(i,1) +=  _isopycnalDiffusion(i,1);
        val(i,2) +=  _isopycnalDiffusion(i,2);
      }
    }
  }
};

class dgConservationLawSW3dTracer::interfaceTerm : public function {
  fullMatrix<double> _normals, _cL, _cR, _uvL, _uvR, _wL, _wR, _wMeshL, _wMeshR, _etaL, _etaR, _bath, _ipTerm, _ipTermIso, _xyz, _uGMR, _uGML;
  bool haveV, haveWMesh, haveNu, haveIso, haveGM;
  double _laxFriedrichsFactor, _R;
  public:
  interfaceTerm(const function *uv, const function *w, const function *wM, const function* eta, const function* bath, const function *ipTerm, const function *ipTermIso, double lff, double R, const function* uGM):function(1) {
    haveV = uv;
    haveWMesh = wM;
    haveNu = ipTerm;
    haveIso = ipTermIso;
    haveGM = uGM;
    _R = R;
    setArgument (_etaL, eta, 0);
    setArgument (_etaR, eta, 1);
    setArgument ( _xyz, function::getCoordinates());
    if (bath)
      setArgument (_bath, bath, 0);
    if (haveV) {
      setArgument ( _uvL, uv, 0);
      setArgument ( _uvR, uv, 1);
      setArgument (_wL, w, 0);
      setArgument (_wR, w, 1);
    }
    if (haveWMesh) {
      setArgument (_wMeshL, wM, 0);
      setArgument (_wMeshR, wM, 1);
    }
    if (haveV || haveWMesh) {
      setArgument ( _normals, function::getNormals());
      setArgument (_cL, function::getSolution(), 0);
      setArgument (_cR, function::getSolution(), 1);
    }
    if (haveGM){
      setArgument(_uGML,uGM,0);
      setArgument(_uGMR,uGM,1);
    }
    if (ipTerm)
      setArgument (_ipTerm, ipTerm);
    if (ipTermIso)
      setArgument (_ipTermIso, ipTermIso);
    _laxFriedrichsFactor = lff;

  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    double g = slim3dParameters::g;
    // determine which element is below
    dataCacheMap* dgMap = m;
    int layerIdL = dgMap->getGroupOfElements()->getLayerId();
    int layerIdR = dgMap->getSecondaryCache(1)->getGroupOfElements()->getLayerId();
    bool rightIsBelow = layerIdR < layerIdL;
    double J;
    for(int i=0; i< val.size1(); i++) {
      J = 1.0;
      if(_R > 0){
         J = 4*_R*_R/(4*_R*_R+_xyz(i,0)*_xyz(i,0)+_xyz(i,1)*_xyz(i,1));
      }
      if (haveGM){
	      _uvL(i,0)+=_uGML(i,0);
	      _uvL(i,1)+=_uGML(i,1);
	      _wL(i,0)+=_uGML(i,2);
	      _uvR(i,0)+=_uGMR(i,0);
	      _uvR(i,1)+=_uGMR(i,1);
	      _wR(i,0)+=_uGMR(i,2);
      }
      if (haveV || haveWMesh) {
        double nx = _normals(i,0);
        double ny = _normals(i,1);
        double nz = _normals(i,2);
        double un;
        double wL = _wL(i,0), wR = _wR(i,0);
        if (haveWMesh) {
          wL -= _wMeshL(i,0);
          wR -= _wMeshR(i,0);
        }
        if (haveV) {
          double unL = nx*_uvL(i,0) + ny*_uvL(i,1);
          double unR = nx*_uvR(i,0) + ny*_uvR(i,1);
          un = (unL+unR)/2;
          if (fabs(nz) < 1e-6) { // vertical face
            //linear equations
            double h = _bath(i,0);
            double etaR = _etaR(i,0);
            double etaL = _etaL(i,0);
            double etaM = 0.5*(_etaL(i,0)+_etaR(i,0));
            double sq_g_h = sqrt(g/(h+etaM));
            un = (unL+unR + (etaL-etaR)*sq_g_h)/2;
          } else { // horizontal face, top/bottom
            if (rightIsBelow)
              un = unR + nz*wR;
            else
              un = unL + nz*wL;
          }
        }
        else {
          un = 0.;
        }
        double c = un>0 ? _cL(i,0) : _cR(i,0);
        val(i,0) = -c*un/J;
        if (_laxFriedrichsFactor) {
          // Lax friedrichs
          double uvwnL = nx*_uvL(i,0) + ny*_uvL(i,1) + nz*wL;
          double uvwnR = nx*_uvR(i,0) + ny*_uvR(i,1) + nz*wR;
          double gamma = 0.5*( fabs(uvwnL) + fabs(uvwnR) );
          double cJ = (_cL(i,0) - _cR(i,0))/2;
          val(i,0) += -_laxFriedrichsFactor*gamma*cJ;
        }
      }
      if (haveNu) {
        val(i,0) += _ipTerm(i,0);
      }
      if (haveIso){
        val(i,0) += _ipTermIso(i,0);
      }
    }
  }
};

class dgConservationLawSW3dTracer::diffusiveFlux : public function {
  fullMatrix<double> solgrad, _nuH, _nuV, _xyz;
  double _R;
  public:
  diffusiveFlux(const function * nuH, const function * nuV, double R):function(3) {
    _R = R;
    setArgument(solgrad,function::getSolutionGradient());
    setArgument(_nuH,nuH);
    setArgument(_nuV,nuV);
    setArgument ( _xyz, function::getCoordinates());
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    for(int i=0; i< solgrad.size1(); i++) {
      double J = 1.0;
       if(_R > 0)
         J = 4*_R*_R/(4*_R*_R+_xyz(i,0)*_xyz(i,0)+_xyz(i,1)*_xyz(i,1));
      val(i,0) = -solgrad(i,0)*_nuH(i,0)/J/J;
      val(i,1) = -solgrad(i,1)*_nuH(i,0)/J/J;
      val(i,2) = -solgrad(i,2)*_nuV(i,0);
    }
  }
};

class dgConservationLawSW3dTracer::boundaryInFlux : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> normals, sol, solGrad, uv, w, outValue;
    fullMatrix <double> cExt, cGradExt, interfaceTerm;
    functionReplace outReplace;
    public:
    term(dgConservationLawFunction *claw, const function *uvF, const function *wF,const function *outVal):function(claw->getNbFields()) {
      outReplace.addChild();
      addFunctionReplace (outReplace);
      outReplace.replace (cExt, function::getSolution(), 1);
      outReplace.replace (cGradExt, function::getSolutionGradient(), 1);
      outReplace.get(interfaceTerm,claw->getInterfaceTerm0(""));
      setArgument(sol,function::getSolution());
      setArgument(solGrad,function::getSolutionGradient());
      setArgument(uv,uvF);
      setArgument(w,wF);
      setArgument(outValue,outVal);
      setArgument(normals,function::getNormals());
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      for(int i=0;i<val.size1(); i++) {
          double nx = normals(i,0);
          double ny = normals(i,1);
          double nz = normals(i,2);
          // compute normal velocity
          double un = uv(i,0)*nx + uv(i,1)*ny + w(i,0)*nz;
          if ( un < 0 )
            cExt(i,0) = outValue(i,0);
          else
            cExt(i,0) = sol(i,0);
      }
      outReplace.compute();
      for(int i=0;i<val.size1(); i++) {
        val(i,0) = interfaceTerm(i,0);
      }
    }
  };
  term _term;
  public:
  boundaryInFlux(const function* outValue, const function* uv, const function* w, dgConservationLawFunction *claw): _term(claw, uv, w, outValue) {
    _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawSW3dTracer::newInFluxBoundary(const function* outValue){
  return new boundaryInFlux(outValue, _uv, _w, this);
}

class dgConservationLawSW3dTracer::boundaryRelaxation : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> normals, sol, solGrad, uv, w, outValue;
    fullMatrix <double> cExt, cIn, cGradExt, interfaceTerm;
    functionReplace outReplace;
    double _TRelax;
    public:
    term(dgConservationLawFunction *claw, const function *uvF, const function *wF,const function *outVal, double TRelax):function(claw->getNbFields()) {
      outReplace.addChild();
      addFunctionReplace (outReplace);
      outReplace.replace (cIn, function::getSolution(), 0);
      outReplace.replace (cExt, function::getSolution(), 1);
      outReplace.replace (cGradExt, function::getSolutionGradient(), 1);
      outReplace.get(interfaceTerm,claw->getInterfaceTerm0(""));
      setArgument(sol,function::getSolution());
      setArgument(solGrad,function::getSolutionGradient());
      setArgument(uv,uvF);
      setArgument(w,wF);
      setArgument(outValue,outVal);
      setArgument(normals,function::getNormals());
      _TRelax = TRelax;
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      for(int i=0;i<val.size1(); i++) {
          double nx = normals(i,0);
          double ny = normals(i,1);
          double nz = normals(i,2);
          // compute normal velocity
          double un = uv(i,0)*nx + uv(i,1)*ny + w(i,0)*nz;
          if ( un < 0 )
            cExt(i,0) = outValue(i,0);
          else
            cExt(i,0) = sol(i,0) + 1/_TRelax * ( outValue(i,0) - sol(i,0) );
          cIn(i,0) = sol(i,0) + 1/_TRelax * ( outValue(i,0) - sol(i,0) );
      }
      outReplace.compute();
      for(int i=0;i<val.size1(); i++) {
        val(i,0) = interfaceTerm(i,0);
      }
    }
  };
  term _term;
  public:
  boundaryRelaxation(const function* outValue, double TRelax, const function* uv, const function* w, dgConservationLawFunction *claw): _term(claw, uv, w, outValue, TRelax) {
    _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawSW3dTracer::newRelaxationBoundary(const function* outValue, double TRelax){
  return new boundaryRelaxation(outValue, TRelax, _uv, _w, this);
}

class dgConservationLawSW3dTracer::diffusivityDotN : public function {
 public:
  fullMatrix<double> _nuH, _nuV, _normals;
  void call(dataCacheMap *m, fullMatrix<double> &val)
  {
    for (int i = 0; i < val.size1(); i++) {
      for (int j = 0; j < val.size2(); j++) {
        double nx = _normals(i,0);
        double ny = _normals(i,1);
        double nz = _normals(i,2);
        val(i, j)= nx*nx*_nuH(i,0) + ny*ny*_nuH(i,0) +nz*nz*_nuV(i,0);
      }
    }
  }
  diffusivityDotN(const function *nuV, const function *nuH) : function(1)
  {
    setArgument (_nuV, nuV);
    setArgument (_nuH, nuH);
    setArgument (_normals,function::getNormals());
  }
};

class dgConservationLawSW3dTracer::boundarySurfaceT : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> normals, _noSolarFlux;
    public:
    term(dgConservationLawFunction *claw, const function* noSolarFlux):function(claw->getNbFields()) {
      setArgument(normals,function::getNormals());
      setArgument(_noSolarFlux,noSolarFlux);
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      double rho0 = slim3dParameters::rho0;
      double Cp = slim3dParameters::Cp;
      for(int i=0;i<val.size1(); i++) {
          double nz = normals(i,2);
          val(i,0) = _noSolarFlux(i,0)/(rho0*Cp)*nz;
      }
    }
  };
  term _termT;
  public:
  boundarySurfaceT(dgConservationLawFunction *claw, const function* noSolarFlux):_termT(claw,noSolarFlux) {
    _term0 = &_termT;
  }
};

dgBoundaryCondition *dgConservationLawSW3dTracer::newBoundarySurfaceT(const function* noSolarFlux){
  return new boundarySurfaceT(this,noSolarFlux);
}

class dgConservationLawSW3dTracer::boundarySurfaceS : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> normals, _salinityFlux,_salinity;
    public:
    term(dgConservationLawFunction *claw, const function* salinityFlux):function(claw->getNbFields()) {
      setArgument(normals,function::getNormals());
      setArgument(_salinityFlux,salinityFlux);
      setArgument(_salinity,function::getSolution());
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      double rho0 = slim3dParameters::rho0;
      for(int i=0;i<val.size1(); i++) {
          double nz = normals(i,2);
          val(i,0) = _salinityFlux(i,0)*_salinity(i,0)/rho0*nz;
      }
    }
  };
  term _termS;
  public:
  boundarySurfaceS(dgConservationLawFunction *claw, const function* salinityFlux):_termS(claw,salinityFlux) {
    _term0 = &_termS;
  }
};

dgBoundaryCondition *dgConservationLawSW3dTracer::newBoundarySurfaceS(const function* salinityFlux){
  return new boundarySurfaceS(this,salinityFlux);
}

void dgConservationLawSW3dTracer::setup () {
  bool haveDiff = _kappaH || _kappaV;
  bool haveIso = _Kedd;
  bool haveCall = _ref;
  if (! _ref){
    _dt = 0;
    _T=1;
    _ref = _fzero;}
  if (!_Kedd)
    _Kedd = _fzero;
  if (!_kappaH )
    _kappaH = _fzero;
  if (!_kappaV )
    _kappaV = _fzero;
  if (!_bath)
    Msg::Fatal("dgConservationLawSW3dTracer: Bath function must be defined");
  if (haveIso){
    printf("haveiso ok");
    _isopycnalDiffusion = new isopycnalDiffusion(_slope,_Kedd);
    _interfaceIso = new interfaceIso(_slope, _Kedd);
  }
  //_ipTermIso = (haveIso) ? dgNewIpTerm(1,_isopycnalDiffusion,_interfaceIso,false) : NULL;
  // classical
  _ipTermIso = (haveIso) ? dgNewIpTerm(1,_isopycnalDiffusion,_interfaceIso): NULL;
  // riviere
  //_ipTermIso = (haveIso) ? dgNewIpTermAnisotropic(1,_isopycnalDiffusion,_interfaceIso) : NULL;
  // Ern
  //_ipTermIso = (haveIso) ? dgNewIpTermErnRef(1,_isopycnalDiffusion,_ipAniso) : NULL;

  _diffusivity[""] = haveDiff ? new diffusivityDotN(_kappaV ,_kappaH ) : NULL;
  _maximumConvectiveSpeed[""] = new maxConvectiveSpeed ( _uv, _w);
  _diffusiveFlux = haveDiff ? new diffusiveFlux (_kappaH ,_kappaV , _R) : NULL;
  _ipTerm = haveDiff ? dgNewIpTerm (1, _diffusiveFlux, _diffusivity[""] ) : NULL;
  _volumeTerm0[""] = ( _dwMdz || (haveDiff && _R>0) || _sourceFunc || haveCall) ? new source ( this, _dwMdz , _kappaH, _sourceFunc, _R, _dt, _T, _ref) : NULL;
  _volumeTerm1[""] = new advection ( _uv, _w, _wM, _diffusiveFlux,_isopycnalDiffusion,_R, _uGM);
  _interfaceTerm0[""] = new interfaceTerm ( _uv, _w, _wM, _eta, _bath,_ipTerm,_ipTermIso, _laxFriedrichsFactor,_R, _uGM);
}

dgConservationLawSW3dTracer::~dgConservationLawSW3dTracer() {
  if (_fzero) delete _fzero;
  if (_fzero) delete _fzeroVec;
  if (_isopycnalDiffusion) delete _isopycnalDiffusion;
  if (_interfaceIso) delete _interfaceIso;
  if (_ipTermIso) delete _ipTermIso;
  if (_maximumConvectiveSpeed[""]) delete _maximumConvectiveSpeed[""];
  if ( _uv) {
    if (_volumeTerm0[""]) delete _volumeTerm0[""];
    if (_volumeTerm1[""]) delete _volumeTerm1[""];
    if (_interfaceTerm0[""]) delete _interfaceTerm0[""];
  }
  if (_diffusiveFlux) delete _diffusiveFlux;
  if (_ipTerm) delete _ipTerm;
}

dgConservationLawSW3dTracer::dgConservationLawSW3dTracer(const function *uv,const function *w) : dgConservationLawFunction(1)
{
  _fzero = new functionConstant(0.);
  _fzeroVec = NULL;
  _Kedd = NULL;
  _ref=NULL;
  _dt=0;
  _T=1;
  _sourceFunc = NULL;
  _uv = uv;
  _w = w;
  _wM = NULL;
  _dwMdz = NULL;
  _kappaH = NULL;
  _kappaV = NULL;
  _eta = _fzero;
  _bath = NULL;
  _laxFriedrichsFactor = 0.0;
  _isopycnalDiffusion = NULL;
  _interfaceIso = NULL;
  _ipTermIso=NULL;
  _diffusiveFlux = NULL;
  _ipTerm = NULL;
  _R = -1.0;
  _useConservativeALE = false;
  _uGM=NULL;
}

dgConservationLawSW3dTracer::dgConservationLawSW3dTracer() : dgConservationLawFunction(1)
{
  _fzero = new functionConstant(0.);
  std::vector<double> zeroVec(2,0.0);
  _fzeroVec = new functionConstant(zeroVec);
  _Kedd = NULL;
  _sourceFunc = NULL;
  _uv = _fzeroVec;
  _w = _fzero;
  _wM = NULL;
  _dwMdz = NULL;
  _kappaH = NULL;
  _kappaV = NULL;
  _eta = _fzero;
  _bath = NULL;
  _laxFriedrichsFactor = 0.0;
  _isopycnalDiffusion = NULL;
  _interfaceIso = NULL;
  _ipTermIso=NULL;
  _diffusiveFlux = NULL;
  _ipTerm = NULL;
  _R = -1.0;
  _ref=NULL;
  _dt=0;
  _T=1;
  _useConservativeALE = false;
  _uGM=NULL;
}

dgConservationLawSW3dTracerVDiff::dgConservationLawSW3dTracerVDiff() : dgConservationLawFunction(1)
{
  _fzero = new functionConstant(0.);
  _kappaV = NULL;
  _bath = NULL;
  _diffusiveFlux = NULL;
  _ipTerm = NULL;
}

dgConservationLawSW3dTracerVDiff::~dgConservationLawSW3dTracerVDiff()
{
  delete _fzero;
  if (_maximumConvectiveSpeed[""]) delete _maximumConvectiveSpeed[""];
  if (_volumeTerm0[""]) delete _volumeTerm0[""];
  if (_diffusiveFlux) delete _diffusiveFlux;
  if (_ipTerm) delete _ipTerm;
}

class dgConservationLawSW3dTracerVDiff::diffusivityDotN : public function {
 public:
  fullMatrix<double> _nuH, _nuV, _normals;
  void call(dataCacheMap *m, fullMatrix<double> &val)
  {
    for (int i = 0; i < val.size1(); i++) {
      for (int j = 0; j < val.size2(); j++) {
        double nx = _normals(i,0);
        double ny = _normals(i,1);
        double nz = _normals(i,2);
        val(i, j)= nx*nx*_nuH(i,0) + ny*ny*_nuH(i,0) +nz*nz*_nuV(i,0);
      }
    }
  }
  diffusivityDotN(const function *nuV, const function *nuH) : function(1)
  {
    setArgument (_nuV, nuV);
    setArgument (_nuH, nuH);
    setArgument (_normals,function::getNormals());
  }
};

class dgConservationLawSW3dTracerVDiff::diffusiveFlux : public function {
  fullMatrix<double> solgrad, _nuH, _nuV;
  public:
  diffusiveFlux(const function * nuH, const function * nuV):function(3) {
    setArgument(solgrad,function::getSolutionGradient());
    setArgument(_nuH,nuH);
    setArgument(_nuV,nuV);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for(int i=0; i< solgrad.size1(); i++) {
      val(i,0) = -solgrad(i,0)*_nuH(i,0);
      val(i,1) = -solgrad(i,1)*_nuH(i,0);
      val(i,2) = -solgrad(i,2)*_nuV(i,0);
    }
  }
};

void dgConservationLawSW3dTracerVDiff::setup () {
  if (!_kappaV )
    Msg::Fatal("tracerVdiff: KappaV function must be defined");
  if (!_bath)
    Msg::Fatal("tracerVdiff: Bath function must be defined");
  _diffusivity[""] = new diffusivityDotN(_kappaV , _fzero );
  _maximumConvectiveSpeed[""] = NULL;
  _diffusiveFlux = new diffusiveFlux (_fzero ,_kappaV );
  _ipTerm = dgNewIpTerm (1, _diffusiveFlux, _diffusivity[""] );
  _volumeTerm0[""] = NULL;
  _volumeTerm1[""] = _diffusiveFlux;
  _interfaceTerm0[""] = _ipTerm;
}

dgFaceTermAssembler *dgConservationLawSW3dTracerVDiff::getFaceTerm(const dgGroupOfFaces &group) const
{
  if(group.orientationTag() == dgGroupOfFaces::ORIENTATION_HORIZONTAL)
    return dgConservationLawFunction::getFaceTerm(group);
  return NULL;
}
