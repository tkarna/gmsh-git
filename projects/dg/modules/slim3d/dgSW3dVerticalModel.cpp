#include "dgSW3dVerticalModel.h"
#include "dgExtrusion.h"
#include "fullMatrix.h"
#include <limits.h>
#include "slim3dFunctions.h"
#include <complex>
#include <iostream>
#include "dgMesh.h"
#include "dgMeshJacobian.h"
#include "GmshDefines.h"
#include "dgIntegrationMatrices.h"
#include "dgNeighbourMap.h"
#include "BasisFactory.h"
#include "ElementType.h"

dgSW3dVEdgeData::dataArray::dataArray(int n) : _nAllocated(0), _data(NULL)
{
  allocate(n);
}

void dgSW3dVEdgeData::dataArray::allocate(int n)
{
  clear();
  _data = new double[ n ];
  _nAllocated = n;
  for (int i = 0; i < _nAllocated; i++ )
    _data[i] = 0;
}

void dgSW3dVEdgeData::dataArray::clear()
{
  if (_data && _nAllocated) delete[] _data;
  _nAllocated = 0;
  _data = NULL;
}

dgSW3dVEdgeArray::dgSW3dVEdgeArray(const dgExtrusion* vEdges, arrayType type): dgSW3dVEdgeData(vEdges)
{
  _type = type;
  // allocate memory
  _data.resize(_vEdges->getNbVerticals());
  for (size_t iVert = 0; iVert < _vEdges->getNbVerticals(); iVert++) {
    int nPoints = _type == CG ? _vEdges->getNbCGPointsInVertical(iVert) :
                               _vEdges->getNbDGPointsInVertical(iVert);
    _data[iVert].allocate(nPoints);
  }
}

void dgSW3dVEdgeArray::importFromFunction(const function* f, int fieldId) {
  dgGroupCollection* groups = &_vEdges->getGroups3d();
  if ( _type == CG )
    Msg::Fatal("Wrong array type. Function values can be imported only for DG arrays");
  dataCacheMap cacheMap(dataCacheMap::NODE_MODE, groups);
  dataCacheDouble* fCache = cacheMap.get(f,NULL);
  for (int iG = 0; iG < groups->getNbElementGroups(); iG++) {
    dgGroupOfElements *group = groups->getElementGroup(iG);
    int nbNodes = group->getNbNodes();
    cacheMap.setGroup(group);
    for (size_t iE =0; iE < group->getNbElements(); iE++) {
      cacheMap.setElement(iE);
      for (int iNode = 0; iNode < nbNodes; iNode++ ) {
        size_t iVertical, iLayer;
        _vEdges->mapDofToVEdgeDG(iG,iE,iNode,iVertical,iLayer);
        _data[iVertical][iLayer] = fCache->get()(iNode,fieldId);
      }
    }
  }
}

void dgSW3dVEdgeArray::importFromDof(const dgDofContainer* c) {
//   if ( _type == CG )
//     Msg::Error("Wrong array type. Dof values can be imported only for DG arrays");
  dgGroupCollection& groups = _vEdges->getGroups3d();
  for (int iG = 0; iG < groups.getNbElementGroups(); iG++) {
    dgGroupOfElements *group = groups.getElementGroup(iG);
    int nbNodes = group->getNbNodes();
    const dgFullMatrix<double> *groupData = &c->getGroupProxy(iG);
    for (size_t iE =0; iE < group->getNbElements(); iE++) {
      fullMatrix<double> elemData;
      groupData->getBlockProxy(iE, elemData);
      for (int iNode = 0; iNode < nbNodes; iNode++ ) {
        size_t iVertical, iLayer;
        if ( _type == DG )
          _vEdges->mapDofToVEdgeDG(iG,iE,iNode,iVertical,iLayer);
        else
          _vEdges->mapDofToVEdgeCG(iG,iE,iNode,iVertical,iLayer);
        _data[iVertical][iLayer] = elemData(iNode,0);
      }
    }
  }
}

void dgSW3dVEdgeArray::exportToDof(dgDofContainer* c, bool suppressTopBottom) {
  dgGroupCollection& groups = _vEdges->getGroups3d();
  for (int iG = 0; iG < groups.getNbElementGroups(); iG++) {
    dgGroupOfElements *group = groups.getElementGroup(iG);
    int nbNodes = group->getNbNodes();
    dgFullMatrix<double> *groupData = &c->getGroupProxy(iG);
    for (size_t iE =0; iE < group->getNbElements(); iE++) {
      fullMatrix<double> elemData;
      groupData->getBlockProxy(iE, elemData);
      for (int iNode = 0; iNode < nbNodes; iNode++ ) {
        size_t iVertical, iLayer;
        if ( _type == CG )
          _vEdges->mapDofToVEdgeCG(iG,iE,iNode,iVertical,iLayer);
        else
          _vEdges->mapDofToVEdgeDG(iG,iE,iNode,iVertical,iLayer);
        if (suppressTopBottom) {
          size_t layerMax = _type == DG ?  _vEdges->getNbDGPointsInVertical(iVertical)-1 :
                                        _vEdges->getNbCGPointsInVertical(iVertical)-1 ;
          if (iLayer == 0) iLayer = 1;
          if (iLayer == layerMax) iLayer = layerMax-1;
        }
        elemData(iNode,0) = _data[iVertical][iLayer];
      }
    }
  }
}

void dgSW3dVEdgeArray::print(std::string label)
{
  printf("VEdgeArray %s:\n",label.c_str());
  for (size_t iVert = 0; iVert < _vEdges->getNbVerticals(); iVert++) {
    printf("iVert: %lu\n",(unsigned long)iVert);
    for (size_t iLev=80; iLev < _vEdges->getNbDGPointsInVertical(iVert) ; iLev++)
      printf("%lu:\t%.12f\n",(unsigned long)iLev,_data[iVert][iLev]);
    printf("\n");
  }
}

dgSW3dVEdgeScalar::dgSW3dVEdgeScalar(const dgExtrusion* vEdges): dgSW3dVEdgeData(vEdges)
{
  // allocate memory
  _data.resize(_vEdges->getNbVerticals());
  for (size_t iVert = 0; iVert < _vEdges->getNbVerticals(); iVert++) {
    _data[iVert].allocate( 1 );
  }
}

void dgSW3dVEdgeScalar::importFromFunction(const function* f, int fieldId) {
  dgGroupCollection &groups2d = _vEdges->getGroups2d();
  dataCacheMap cacheMap(dataCacheMap::NODE_MODE, &groups2d);
  dataCacheDouble* fCache = cacheMap.get(f,NULL);
  for (size_t iVert = 0; iVert<_vEdges->getNbVerticals(); iVert++) {
    size_t iG2d, iE2d, iN2d;
    _vEdges->mapVEdgeTo2dNode(iVert,iG2d,iE2d,iN2d);
    dgGroupOfElements *group = groups2d.getElementGroup(iG2d);
    cacheMap.setGroup(group);
    cacheMap.setElement(iE2d);
    _data[iVert][0] = fCache->get()(iN2d,fieldId);
  }
}

void dgSW3dVEdgeScalar::importFromDof(const dgDofContainer* c) {
  for (size_t iVert = 0; iVert<_vEdges->getNbVerticals(); iVert++) {
    size_t iG2d, iE2d, iN2d;
    _vEdges->mapVEdgeTo2dNode(iVert,iG2d,iE2d,iN2d);
    const dgFullMatrix<double> *groupData = &c->getGroupProxy(iG2d);
    fullMatrix<double> elemData;
    groupData->getBlockProxy(iE2d, elemData);
    _data[iVert][0] = elemData(iN2d,0);
  }
}

void dgSW3dVEdgeScalar::exportToDof(dgDofContainer* c, bool supress) {
  for (size_t iVert = 0; iVert<_vEdges->getNbVerticals(); iVert++) {
    size_t iG2d, iE2d, iN2d;
    _vEdges->mapVEdgeTo2dNode(iVert,iG2d,iE2d,iN2d);
    dgFullMatrix<double> *groupData = &c->getGroupProxy(iG2d);
    fullMatrix<double> elemData;
    groupData->getBlockProxy(iE2d, elemData);
    elemData(iN2d,0) = _data[iVert][0];
  }
}

// TODO remove
void dgSW3dVerticalModel::convertToTriIntPoints(const dgDofContainer* source, dgDofContainer* dest)
{
  double f[256];
  const dgGroupCollection* sourceGroups =  source->getGroups();
  for (int iG = 0; iG < sourceGroups->getNbElementGroups(); iG++) {
    dgGroupOfElements* sourceGroup = sourceGroups->getElementGroup(iG);
    int nbNodes = sourceGroup->getNbNodes();
    const nodalBasis* fs = BasisFactory::getNodalBasis(ElementType::getTag(sourceGroup->getFunctionSpace().parentType, 1));
    double integration[6][3] = { {1./6,1./6,-1},
                                 {4./6,1./6,-1},
                                 {1./6,4./6,-1},
                                 {1./6,1./6, 1},
                                 {4./6,1./6, 1},
                                 {1./6,4./6, 1} };
    int nbQP = 6;
    fullMatrix<double> collocation(nbNodes, nbQP);
    collocation.setAll(0);
    for (int xi = 0; xi < nbQP; xi ++) {
      fs->f(integration[xi][0], integration[xi][1], integration[xi][2], f);
      for (int i=0; i<nbNodes; i++) {
        collocation (xi,i) = f[i];
      }
    }
    collocation.mult(source->getGroupProxy(iG),dest->getGroupProxy(iG));
  }
}

void dgSW3dVerticalModel::convertFromTriIntPoints(const dgDofContainer* source, dgDofContainer* dest)
{
  double f[256];
  const dgGroupCollection* sourceGroups =  source->getGroups();
  for (int iG = 0; iG < sourceGroups->getNbElementGroups(); iG++) {
    dgGroupOfElements* sourceGroup = sourceGroups->getElementGroup(iG);
    int nbNodes = sourceGroup->getNbNodes();
    const nodalBasis* fs = BasisFactory::getNodalBasis(ElementType::getTag(sourceGroup->getFunctionSpace().parentType, 1));
    double integration[6][3] = { {1./6,1./6,-1},
                                 {4./6,1./6,-1},
                                 {1./6,4./6,-1},
                                 {1./6,1./6, 1},
                                 {4./6,1./6, 1},
                                 {1./6,4./6, 1} };
    int nbQP = 6;
    fullMatrix<double> collocation(nbNodes, nbQP);
    collocation.setAll(0);
    for (int xi = 0; xi < nbQP; xi ++) {
      fs->f(integration[xi][0], integration[xi][1], integration[xi][2], f);
      for (int i=0; i<nbNodes; i++) {
        collocation (xi,i) = f[i];
      }
    }
    collocation.invertInPlace();
    collocation.mult(source->getGroupProxy(iG),dest->getGroupProxy(iG));
  }
}

void dgSW3dVEdgeScalar::print(std::string label)
{
  printf("VEdgeScalar %s:\n",label.c_str());
  for (size_t iVert = 0; iVert < _vEdges->getNbVerticals(); iVert++) {
    printf("iVert: %lu\n", (unsigned long)iVert);
    printf("%d:\t%.12f\n",0,_data[iVert][0]);
  }
}

#ifdef HAVE_GOTM
dgSW3dTurbulenceGOTM::dgSW3dTurbulenceGOTM(const dgExtrusion* columns, const std::string turbNamelist) : dgSW3dVerticalModel(columns),
    _tkeArray(_vColumns,dgSW3dVEdgeArray::CG),
    _epsArray(_vColumns,dgSW3dVEdgeArray::CG),
    _lArray(_vColumns,dgSW3dVEdgeArray::CG),
    _nuvArray(_vColumns,dgSW3dVEdgeArray::CG),
    _kapvArray(_vColumns,dgSW3dVEdgeArray::CG)
{
  _nnDof = NULL;
  _ssDof = NULL;
  _kapvDof = NULL;
  _nuvDof = NULL;
  _tkeDof = NULL;
  _epsDof = NULL;
  _lDof = NULL;
  _gotmTurbulenceNamelist = turbNamelist;
  setUseOldVelocityInSS(true);
  setComputeNNWithEOS(false);
  _initialized = false;
}

void dgSW3dTurbulenceGOTM::initialize()
{
  if (_initialized) {
    Msg::Warning("GOTM already initialized");
    return;
  }
  _gotmInterface.init(_vColumns->getNbCGPointsInVertical(0),_gotmTurbulenceNamelist);
  // get init values from GOTM
  for (size_t iV = 0; iV < _vColumns->getNbVerticals(); iV++ ) {
    _storeGOTMVertical(iV);
  }
  if ( !_tkeDof || !_epsDof  )
    Msg::Fatal("TKE and EPS dofContainers must be assigned before initialization");
  _storeDiffusivities();
  _initialized=true;
}


void dgSW3dTurbulenceGOTM::updateGOTM(double dt)
{
  if (!_initialized) initialize();
// solve GOTM
  _setInputArrays();
  for (size_t iV = 0; iV < _vColumns->getNbVerticals(); iV++ ) {
    _setGOTMVertical(iV);
    _solveGOTM(iV,dt);
    _storeGOTMVertical(iV);
  }
  _storeDiffusivities();
}

void dgSW3dTurbulenceGOTM::_setGOTMVertical(int iV)
{
// 1) copy data from 1d arrays[iVert] to GOTM, tke eps num nuh
  _gotmInterface.setTKE(_tkeArray.getVEdgeData(iV));
  _gotmInterface.setEPS(_epsArray.getVEdgeData(iV));
  _gotmInterface.setNUM(_nuvArray.getVEdgeData(iV));
  _gotmInterface.setNUH(_kapvArray.getVEdgeData(iV));
  _gotmInterface.setL(_lArray.getVEdgeData(iV));
}
void dgSW3dTurbulenceGOTM::_setInputArrays()
{
  // 2) copy function vals to arrays[iVert], nn ss layerH
  // TODO: change uvTauB uvTauS to magnitude ?
  if (_kapvDof) _kapvArray.importFromDof(_kapvDof);
  if (_nuvDof) _nuvArray.importFromDof(_nuvDof);
  if (_epsDof) _epsArray.importFromDof(_epsDof);
  if (_tkeDof) _tkeArray.importFromDof(_tkeDof);
  computeBottomFriction();
  computeHeights();
  computeNN();
  computeSS();
}
void dgSW3dTurbulenceGOTM::_solveGOTM(int iV, double dt)
{
// 3) run do_turbulence
  _gotmInterface.doTurbulence(dt, _HScalar(iV), _uStarSScalar(iV),_uStarBScalar(iV), _z0SScalar(iV), _z0BScalar(iV),_dArray.getVEdgeData(iV),_nnArray.getVEdgeData(iV), _ssArray.getVEdgeData(iV));
}
void dgSW3dTurbulenceGOTM::_storeGOTMVertical(int iV)
{
// 4) copy data from GOTM to arrays[iVert], the eps num nuh
  _gotmInterface.getTKE(_tkeArray.getVEdgeData(iV));
  _gotmInterface.getEPS(_epsArray.getVEdgeData(iV));
  _gotmInterface.getNUM(_nuvArray.getVEdgeData(iV));
  _gotmInterface.getNUH(_kapvArray.getVEdgeData(iV));
  _gotmInterface.getL(_lArray.getVEdgeData(iV));
}
void dgSW3dTurbulenceGOTM::_storeDiffusivities()
{
// 5) update dofContainer num nuh
  if (_nnDof) _nnArray.exportToDof(_nnDof);
  if (_ssDof) _ssArray.exportToDof(_ssDof);
  if (_kapvDof) _kapvArray.exportToDof(_kapvDof);
  if (_nuvDof) _nuvArray.exportToDof(_nuvDof);
  if (_tkeDof) _tkeArray.exportToDof(_tkeDof,true);
  if (_epsDof) _epsArray.exportToDof(_epsDof,true);
  if (_lDof) _lArray.exportToDof(_lDof);
}
#endif

dgSW3dVerticalModel::dgSW3dVerticalModel(const dgExtrusion* columns) : _vColumns(columns),
    _uStarSScalar(_vColumns),
    _uStarBScalar(_vColumns),
    _z0SScalar(_vColumns),
    _z0BScalar(_vColumns),
    _HScalar(_vColumns),
    _uArray(_vColumns,dgSW3dVEdgeArray::CG),
    _vArray(_vColumns,dgSW3dVEdgeArray::CG),
    _uOldArray(_vColumns,dgSW3dVEdgeArray::CG),
    _vOldArray(_vColumns,dgSW3dVEdgeArray::CG),
    _TArray(_vColumns,dgSW3dVEdgeArray::DG),
    _SArray(_vColumns,dgSW3dVEdgeArray::DG),
    _ssArray(_vColumns,dgSW3dVEdgeArray::CG),
    _nnArray(_vColumns,dgSW3dVEdgeArray::CG),
    _dArray(_vColumns,dgSW3dVEdgeArray::CG),
    _rhoArray(_vColumns,dgSW3dVEdgeArray::CG),
    _zArray(_vColumns,dgSW3dVEdgeArray::DG),
    _tempDGArray(_vColumns,dgSW3dVEdgeArray::DG),
    _dRhodZArray(_vColumns,dgSW3dVEdgeArray::DG),
    _dSdZArray(_vColumns,dgSW3dVEdgeArray::DG),
    _dTdZArray(_vColumns,dgSW3dVEdgeArray::DG),
    _TCenterArray(_vColumns,dgSW3dVEdgeArray::CG),
    _SCenterArray(_vColumns,dgSW3dVEdgeArray::CG),
    _eosBetaArray(_vColumns,dgSW3dVEdgeArray::DG),
    _eosAlphaArray(_vColumns,dgSW3dVEdgeArray::DG),
    _dudzArray(_vColumns,dgSW3dVEdgeArray::DG),
    _dvdzArray(_vColumns,dgSW3dVEdgeArray::DG),
    _dudzArrayOld(_vColumns,dgSW3dVEdgeArray::DG),
    _dvdzArrayOld(_vColumns,dgSW3dVEdgeArray::DG)
{
  _groups = &_vColumns->getGroups3d();
  _groups2d = &_vColumns->getGroups2d();
  _zFunction = NULL;
  _nnFunction = NULL;
  _ssFunction = NULL;
  _uvFunction = NULL;
  _uvGradFunction = NULL;
  _TGradFunction = NULL;
  _SGradFunction = NULL;
  _eosBetaFunction = NULL;
  _bathFunction = NULL;
  _z0BFunction = NULL;
  _z0SFunction = NULL;
  _TDof = NULL;
  _SDof = NULL;
  _useOldUVInSS = false;
  _computeNNWithEOS = false;
  _useFEGradient = false;
  _initialize();
}

void dgSW3dVerticalModel::_initialize()
{
  _order = _groups->getElementGroup(0)->getOrder();
  const nodalBasis* fs = NULL;
  switch (_order) {
    case 1: fs = BasisFactory::getNodalBasis(MSH_LIN_2); break;
    case 2: fs = BasisFactory::getNodalBasis(MSH_LIN_3); break;
    case 3: fs = BasisFactory::getNodalBasis(MSH_LIN_4); break;
    case 4: fs = BasisFactory::getNodalBasis(MSH_LIN_5); break;
    case 5: fs = BasisFactory::getNodalBasis(MSH_LIN_6); break;
    case 6: fs = BasisFactory::getNodalBasis(MSH_LIN_7); break;
    case 7: fs = BasisFactory::getNodalBasis(MSH_LIN_8); break;
    case 8: fs = BasisFactory::getNodalBasis(MSH_LIN_9); break;
    case 9: fs = BasisFactory::getNodalBasis(MSH_LIN_10); break;
    case 10: fs = BasisFactory::getNodalBasis(MSH_LIN_11); break;
    default :
    Msg::Fatal("Could not create order %d line function space", _order);
  }
  // evaluation points (between nodes) in parametric coordinates
  _nb1DElemNodes = _order+1;
  fullMatrix<double> xi(_nb1DElemNodes-1,3);
  _linePsiCenter.resize(_nb1DElemNodes-1,_nb1DElemNodes);
  for (int i = 0; i < _nb1DElemNodes-1; i++) {
    xi(i,0) = -1.0 + (i*2 + 1.0)/(_order);
    double sf[100];
    fs->f(xi(i,0), xi(i,1), xi(i,2), sf);
    // node ordering: 0---2---...-(N-1)---1
    _linePsiCenter(i,0) = sf[0];
    _linePsiCenter(i,_nb1DElemNodes-1) = sf[1];
    for (int j = 1; j < _nb1DElemNodes-1; j++)
      _linePsiCenter(i,j) = sf[j+1];
  }

}

dgSW3dVerticalModel::~dgSW3dVerticalModel()
{

}

void dgSW3dVerticalModel::computeBottomFriction()
{
  double rho0 = slim3dParameters::rho0;
  if (!_z0BFunction)
    Msg::Fatal("Required function is missing: z0B");
  if (!_z0SFunction)
    Msg::Fatal("Required function is missing: z0S");
  if (!_windStressFunction)
    Msg::Fatal("Required function is missing: windStress");
  if (!_uvStarBFunction)
    Msg::Fatal("Required function is missing: uvStarB");
  _z0BScalar.importFromFunction(_z0BFunction);
  _z0SScalar.importFromFunction(_z0SFunction);
  dataCacheMap cacheMap(dataCacheMap::NODE_MODE, _groups2d);
  dataCacheDouble* uvTauBCache = cacheMap.get(_uvStarBFunction,NULL);
  dataCacheDouble* windStressCache = cacheMap.get(_windStressFunction,NULL);
  for (size_t iVert = 0; iVert<_vColumns->getNbVerticals(); iVert++) {
    size_t iG2d, iE2d, iN2d;
    _vColumns->mapVEdgeTo2dNode(iVert,iG2d,iE2d,iN2d);
    dgGroupOfElements *group = _groups2d->getElementGroup(iG2d);
    cacheMap.setGroup(group);
    cacheMap.setElement(iE2d);
    // compute |u_surf^star| from the wind stress
    double windStressNorm = hypot( windStressCache->get()(iN2d,0) , windStressCache->get()(iN2d,1) );
    _uStarSScalar(iVert) = sqrt( windStressNorm/rho0 );
    _uStarBScalar(iVert) = hypot( uvTauBCache->get()(iN2d,0) , uvTauBCache->get()(iN2d,1) );
  }
}

void dgSW3dVerticalModel::evaluateFunctionAtCellCenters(const function* f, dgSW3dVEdgeArray *arrayCG, int fieldId)
{
  if (arrayCG->isDG())
    Msg::Fatal("Wrong array type. Center values can be imported only for CG arrays");
  _tempDGArray.importFromFunction(f,fieldId);
  for (size_t iVert = 0; iVert<_vColumns->getNbVerticals(); iVert++) {
    // compute u at cell centers:
    // for each point i, compute linear combination psi_i,j * node_j
    (*arrayCG)(iVert,0) = 0;
    for (size_t iLev=1; iLev <_vColumns->getNbCGPointsInVertical(iVert); iLev++) {
      (*arrayCG)(iVert,iLev) = 0;
      int firstNode = (int)floor((iLev-1)/_order) * _nb1DElemNodes;
      int iXi = (iLev-1) % _order;
      for (int i = 0; i < _linePsiCenter.size2(); i++) {
        (*arrayCG)(iVert,iLev) += _linePsiCenter(iXi,i) * _tempDGArray(iVert,firstNode+i);
      }
    }
  }
}

void dgSW3dVerticalModel::evaluateUVAtCellCenters()
{ 
  evaluateFunctionAtCellCenters(_uvFunction,&_uArray,0);
  evaluateFunctionAtCellCenters(_uvFunction,&_vArray,1);
  
}

void dgSW3dVerticalModel::computeSS()
{
  if (!_uvFunction)
    Msg::Fatal("Required function is missing: uv");
  if (_useOldUVInSS) {
    for (size_t iVert = 0; iVert<_vColumns->getNbVerticals(); iVert++) {
      for (size_t iLev=0; iLev < _vColumns->getNbCGPointsInVertical(iVert) ; iLev++) {
        _uOldArray(iVert,iLev) = _uArray(iVert,iLev);
        _vOldArray(iVert,iLev) = _vArray(iVert,iLev);
      }
    }
  }
  evaluateFunctionAtCellCenters(_uvFunction,&_uArray,0);
  evaluateFunctionAtCellCenters(_uvFunction,&_vArray,1);
  for (size_t iVert = 0; iVert<_vColumns->getNbVerticals(); iVert++) {
    // computation of shear frequency as in GOTM (finite differences)
    for (size_t iLev= 1; iLev < _vColumns->getNbCGPointsInVertical(iVert)-1 ; iLev++) {
      double u0,u1,uOld0,uOld1,v0,v1,vOld0,vOld1,hMean,SSx,SSy;
      u0    = _uArray(iVert,iLev);
      u1    = _uArray(iVert,iLev+1);
      v0    = _vArray(iVert,iLev);
      v1    = _vArray(iVert,iLev+1);
      hMean = 0.5*(_dArray(iVert,iLev+1)+_dArray(iVert,iLev));
      if (_useOldUVInSS) {
        double cnpar = 0.5; // GOTM implicity parameter
        uOld0 = _uOldArray(iVert,iLev);
        uOld1 = _uOldArray(iVert,iLev+1);
        vOld0 = _vOldArray(iVert,iLev);
        vOld1 = _vOldArray(iVert,iLev+1);
        SSx = ( cnpar * (u1-u0) + (1.-cnpar)*(uOld1-uOld0) ) * 0.5*( u1-u0 + uOld1-uOld0 )
              /hMean/hMean; // in GOTM code
        SSy = ( cnpar * (v1-v0) + (1.-cnpar)*(vOld1-vOld0) ) * 0.5*( v1-v0 + vOld1-vOld0 )
              /hMean/hMean; // in GOTM code
      } else {
        SSx = ( u1-u0 )*( u1-u0 )/hMean/hMean;
        SSy = ( v1-v0 )*( v1-v0 )/hMean/hMean;
      }
      _ssArray(iVert,iLev) = SSx + SSy;
    }
  }
  if (_uvGradFunction) {
    // take also the gradient into account
    if (_useOldUVInSS) {
      for (size_t iVert = 0; iVert<_vColumns->getNbVerticals(); iVert++) {
        for (size_t iLev=0; iLev < _vColumns->getNbDGPointsInVertical(iVert) ; iLev++) {
          _dudzArrayOld(iVert,iLev) = _dudzArray(iVert,iLev);
          _dvdzArrayOld(iVert,iLev) = _dvdzArray(iVert,iLev);
        }
      }
    }
    _dudzArray.importFromFunction(_uvGradFunction,2);
    _dvdzArray.importFromFunction(_uvGradFunction,5);
    for (size_t iVert = 0; iVert<_vColumns->getNbVerticals(); iVert++) {
      for (size_t iLev= 1; iLev < _vColumns->getNbCGPointsInVertical(iVert)-1 ; iLev++) {
        double meanDuDz = 0.5*( _dudzArray(iVert,2*iLev-1) + _dudzArray(iVert,2*iLev) );
        double meanDvDz = 0.5*( _dvdzArray(iVert,2*iLev-1) + _dvdzArray(iVert,2*iLev) );
        double meanDuDzOld = meanDuDz;
        double meanDvDzOld = meanDvDz;
        if (_useOldUVInSS) {
          meanDuDzOld = 0.5*( _dudzArrayOld(iVert,2*iLev-1) + _dudzArrayOld(iVert,2*iLev) );
          meanDvDzOld = 0.5*( _dvdzArrayOld(iVert,2*iLev-1) + _dvdzArrayOld(iVert,2*iLev) );
        }
        double SSx = meanDuDz*meanDuDzOld;
        double SSy = meanDvDz*meanDvDzOld;
        if ( _useFEGradient )
          _ssArray(iVert,iLev) = SSx + SSy;
      }
    }
  }
  // copy top/bottom values from interior (these values are not used, only for visualization)
  for (size_t iVert = 0; iVert<_vColumns->getNbVerticals(); iVert++) {
    _ssArray(iVert,0) = _ssArray(iVert,1); // bottom
    _ssArray(iVert,_vColumns->getNbCGPointsInVertical(iVert)-1) = _ssArray(iVert,_vColumns->getNbCGPointsInVertical(iVert)-2); // top
  }
}

void dgSW3dVerticalModel::computeHeights()
{
  if (!_zFunction)
    Msg::Fatal("Required function is missing: z");
  _zArray.importFromFunction(_zFunction);
  for (size_t iVert = 0; iVert<_vColumns->getNbVerticals(); iVert++) {
    _HScalar(iVert) = _zArray(iVert,_vColumns->getNbDGPointsInVertical(iVert)-1) - _zArray(iVert,0);
    for (size_t iLev=0; iLev <_vColumns->getNbCGPointsInVertical(iVert)-1; iLev++) {
      int offset = iLev + iLev/_order;
      _dArray(iVert,iLev+1) = _zArray(iVert,offset+1) - _zArray(iVert,offset);
    }
  }
}

void dgSW3dVerticalModel::computeNN()
{
  double g = slim3dParameters::g;
  double rho0 = slim3dParameters::rho0;
  if (!_computeNNWithEOS) {
    if (!_TDof)
      Msg::Fatal("Required DOF is missing: T");
    if (!_SDof)
      Msg::Fatal("Required DOF is missing: S");
    evaluateFunctionAtCellCenters(_TDof->getFunction(),&_TCenterArray);
    evaluateFunctionAtCellCenters(_SDof->getFunction(),&_SCenterArray);
    _zArray.importFromFunction(_zFunction);
    // computation with alpha and beta
    if (!_eosAlphaFunction)
      Msg::Fatal("Equation of state Alpha function is required");
    if (!_eosBetaFunction)
      Msg::Fatal("Equation of state Beta function is required");
    _eosBetaArray.importFromFunction(_eosBetaFunction);
    _eosAlphaArray.importFromFunction(_eosAlphaFunction);
    if (_TGradFunction && _SGradFunction) {
      _dSdZArray.importFromFunction(_SGradFunction,2);
      _dTdZArray.importFromFunction(_TGradFunction,2);
    }
    for (size_t iVert = 0; iVert<_vColumns->getNbVerticals(); iVert++) {
      for (size_t iLev= 1; iLev < _vColumns->getNbCGPointsInVertical(iVert)-1 ; iLev++) {
        double hFD = 0.5*(_dArray(iVert,iLev+1)+_dArray(iVert,iLev));
        double dTdz = ( _TCenterArray(iVert,iLev+1) - _TCenterArray(iVert,iLev) )/hFD;
        double dSdz = ( _SCenterArray(iVert,iLev+1) - _SCenterArray(iVert,iLev) )/hFD;
        if (_TGradFunction && _SGradFunction){
          double dTdzFE = 0.5*(_dTdZArray(iVert,2*iLev-1)+_dTdZArray(iVert,2*iLev));
          double dSdzFE = 0.5*(_dSdZArray(iVert,2*iLev-1)+_dSdZArray(iVert,2*iLev));
          if (_useFEGradient) {
            dTdz = dTdzFE;
            dSdz = dSdzFE;
          }
        }
        double alpha = 0.5*(_eosAlphaArray(iVert,2*iLev-1)+_eosAlphaArray(iVert,2*iLev));
        double beta = 0.5*(_eosBetaArray(iVert,2*iLev-1)+_eosBetaArray(iVert,2*iLev));
        _nnArray(iVert,iLev) = -g/rho0 * ( alpha*dTdz + beta*dSdz );
      }
      _nnArray(iVert,0) = _nnArray(iVert,1);
      _nnArray(iVert,_vColumns->getNbCGPointsInVertical(iVert)-1) = _nnArray(iVert,_vColumns->getNbCGPointsInVertical(iVert)-2);
    }
  } else {
    // computation with EOS and FD
    if (!_TDof)
      Msg::Fatal("Required DOF is missing: T");
    if (!_SDof)
      Msg::Fatal("Required DOF is missing: S");
    evaluateFunctionAtCellCenters(_TDof->getFunction(),&_TCenterArray);
    evaluateFunctionAtCellCenters(_SDof->getFunction(),&_SCenterArray);
    _zArray.importFromFunction(_zFunction);
    for (size_t iVert = 0; iVert<_vColumns->getNbVerticals(); iVert++) {
      // computation of gradient of potential density
      for (size_t iLev= 1; iLev < _vColumns->getNbCGPointsInVertical(iVert)-1 ; iLev++) {
        double pr = g*rho0*(0.0-_zArray(iVert,iLev*2))/1e4;
        double r1 = SW3dEquationOfStateJackett(_SCenterArray(iVert,iLev),_TCenterArray(iVert,iLev),pr, rho0);
        double r2 = SW3dEquationOfStateJackett(_SCenterArray(iVert,iLev+1),_TCenterArray(iVert,iLev+1),pr, rho0);
        _nnArray(iVert,iLev) = -g/rho0*(r2-r1)/(0.5*(_dArray(iVert,iLev+1)+_dArray(iVert,iLev)));
      }
      _nnArray(iVert,0) = _nnArray(iVert,1);
      _nnArray(iVert,_vColumns->getNbCGPointsInVertical(iVert)-1) = _nnArray(iVert,_vColumns->getNbCGPointsInVertical(iVert)-2);
    }
  }
  // suppress neg values !!
//   for (int iVert = 0; iVert<_vColumns->getNbVerticals(); iVert++) {
//     for (int iLev= 0; iLev < _vColumns->getNbCGPointsInVertical(iVert) ; iLev++) {
//       _nnArray(iVert,iLev) = std::max(_nnArray(iVert,iLev),0.0);
//     }
//   }
}

void dgSW3dVerticalModel::convectiveAdjustment(double nnThreshold) {
  if ( !_TDof && !_SDof)
    Msg::Fatal("Either T or S dgDofContainer must be defined");
  // fetch T and S data to arrays
  if (_TDof) _TArray.importFromDof(_TDof);
  if (_SDof) _SArray.importFromDof(_SDof);
  _zArray.importFromFunction(_zFunction);
  // loop over verticals
  // if nn < 0 mix T and S (set to average)
  for (size_t iVert = 0; iVert<_vColumns->getNbVerticals(); iVert++) {
    bool mixAll = false;
    for (size_t iLev=1; iLev < _vColumns->getNbCGPointsInVertical(iVert)-1 ; iLev++)
       if (_nnArray(iVert,iLev) < -nnThreshold)
         mixAll = true;
    if (mixAll) {
      for (size_t iLev=1; iLev < _vColumns->getNbCGPointsInVertical(iVert)-1 ; iLev++) {
        if (_nnArray(iVert,iLev) < -nnThreshold) {
              double SMean = 0;
              double TMean = 0;
              SMean =  (_SArray(iVert,iLev*2)+ _SArray(iVert,iLev*2+1)+ _SArray(iVert,iLev*2-1))/3;
              _SArray(iVert,iLev*2-1)= SMean;
              _SArray(iVert,iLev*2)= SMean;
              _SArray(iVert,iLev*2+1)= SMean;
              TMean =  (_TArray(iVert,iLev*2)+ _TArray(iVert,iLev*2+1)+ _TArray(iVert,iLev*2-1))/2;
              _TArray(iVert,iLev*2-1)= TMean;
              _TArray(iVert,iLev*2)= TMean;
              _TArray(iVert,iLev*2+1)= TMean;
        }
      }
    }
  }
  // store T,S data back to DOF container
  if (_TDof) _TArray.exportToDof(_TDof);
  if (_SDof) _SArray.exportToDof(_SDof);
}
