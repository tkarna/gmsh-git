#include "dgConservationLawFunction.h"
#include "fullMatrix.h"
#include "dgGroupOfElements.h"
#include "SPoint3.h"
#include "functionGeneric.h"
#include "dgConservationLawVerticalCoord.h"
#include "dgConservationLawSW3dMomentum.h"
#include "dgGroupOfElements.h"

/*
Order 1 tensor obtained by post multiplication of the diffusivity tensor by the normal vector (K.n) 
*/
class dgConservationLawVerticalCoord :: normalDiffTensor1 : public function {
  fullMatrix <double> _normals, _n, _Lambda, _mu;
  public:
    normalDiffTensor1 (const function *n, const function *Lambda, const function *mu): function (3) {
      setArgument(_n,n);
      setArgument(_Lambda,Lambda);
      setArgument(_mu,mu);
      setArgument(_normals,function::getNormals());
    };
    void call (dataCacheMap *m, fullMatrix<double> &val) {
      size_t nQP = val.size1();
      double K[3][3];
      for(size_t i=0; i< nQP; i++) {
	      double nx = _normals(i,0);
        double ny = _normals(i,1);
        double nz = _normals(i,2);
	      K[0][0] = _Lambda(i,0) + (_mu(i,0)-_Lambda(i,0))*_n(i,0)*_n(i,0);
	      K[0][1] = (_mu(i,0)-_Lambda(i,0))*_n(i,0)*_n(i,1);
	      K[1][0] = (_mu(i,0)-_Lambda(i,0))*_n(i,1)*_n(i,0);
	      K[0][2] = (_mu(i,0)-_Lambda(i,0))*_n(i,0)*_n(i,2);
	      K[2][0] = (_mu(i,0)-_Lambda(i,0))*_n(i,2)*_n(i,0);
	      K[1][2] = (_mu(i,0)-_Lambda(i,0))*_n(i,1)*_n(i,2);
	      K[2][1] = (_mu(i,0)-_Lambda(i,0))*_n(i,2)*_n(i,1);
	      K[1][1] = _Lambda(i,0) + (_mu(i,0)-_Lambda(i,0))*_n(i,1)*_n(i,1);
	      K[2][2] = _Lambda(i,0) + (_mu(i,0)-_Lambda(i,0))*_n(i,2)*_n(i,2);
        val(i,0) = (K[0][0]*nx+K[0][1]*ny+K[0][2]*nz);
        val(i,1) = (K[1][0]*nx+K[1][1]*ny+K[1][2]*nz);
        val(i,2) = (K[2][0]*nx+K[2][1]*ny+K[2][2]*nz);
    }
  }
};


class dgConservationLawVerticalCoord :: diffusiveFlux : public function {
  fullMatrix <double> _solgrad, _n, _Lambda, _mu;
  public:
    diffusiveFlux (const function *n, const function *Lambda, const function *mu): function (3) {
      setArgument(_n,n);
      setArgument(_Lambda,Lambda);
      setArgument(_mu,mu);
      setArgument(_solgrad,function::getSolutionGradient());
    };
    void call (dataCacheMap *m, fullMatrix<double> &val) {
      size_t nQP = val.size1();
      double K[3][3];
      for(size_t i=0; i< nQP; i++) {
	      K[0][0] = _Lambda(i,0) + (_mu(i,0)-_Lambda(i,0))*_n(i,0)*_n(i,0);
	      K[0][1] = (_mu(i,0)-_Lambda(i,0))*_n(i,0)*_n(i,1);
	      K[1][0] = (_mu(i,0)-_Lambda(i,0))*_n(i,1)*_n(i,0);
	      K[0][2] = (_mu(i,0)-_Lambda(i,0))*_n(i,0)*_n(i,2);
	      K[2][0] = (_mu(i,0)-_Lambda(i,0))*_n(i,2)*_n(i,0);
	      K[1][2] = (_mu(i,0)-_Lambda(i,0))*_n(i,1)*_n(i,2);
	      K[2][1] = (_mu(i,0)-_Lambda(i,0))*_n(i,2)*_n(i,1);
	      K[1][1] = _Lambda(i,0) + (_mu(i,0)-_Lambda(i,0))*_n(i,1)*_n(i,1);
	      K[2][2] = _Lambda(i,0) + (_mu(i,0)-_Lambda(i,0))*_n(i,2)*_n(i,2);
        val(i,0) = (K[0][0]* _solgrad(i,0)+K[0][1]*_solgrad(i,1)+K[0][2]*_solgrad(i,2));
        val(i,1) = (K[1][0]* _solgrad(i,0)+K[1][1]*_solgrad(i,1)+K[1][2]*_solgrad(i,2));
        val(i,2) = (K[2][0]* _solgrad(i,0)+K[2][1]*_solgrad(i,1)+K[2][2]*_solgrad(i,2));
      } 
  }
};

/*
Order 0 tensor obtained by pre and post multiplication of the diffusivity tensor by the normal vector (n.K.n)
*/
class dgConservationLawVerticalCoord :: normalDiffTensor0 : public function {
  fullMatrix <double> _normals, _n, _Lambda, _mu;
  public:
    normalDiffTensor0(const function *n, const function *Lambda, const function *mu): function (1) {
      setArgument(_n,n);
      setArgument(_normals,function::getNormals());
      setArgument(_Lambda,Lambda);
      setArgument(_mu,mu);
    };
    void call (dataCacheMap *m, fullMatrix<double> &val) {
      size_t nQP = val.size1();
      double K[3][3];
      for(size_t i=0; i< nQP; i++) {
	      double Sn[3];
	      double nx = _normals(i,0);
        double ny = _normals(i,1);
        double nz = _normals(i,2);
	      K[0][0] = _Lambda(i,0) + (_mu(i,0)-_Lambda(i,0))*_n(i,0)*_n(i,0);
	      K[0][1] = (_mu(i,0)-_Lambda(i,0))*_n(i,0)*_n(i,1);
	      K[1][0] = (_mu(i,0)-_Lambda(i,0))*_n(i,1)*_n(i,0);
	      K[0][2] = (_mu(i,0)-_Lambda(i,0))*_n(i,0)*_n(i,2);
	      K[2][0] = (_mu(i,0)-_Lambda(i,0))*_n(i,2)*_n(i,0);
	      K[1][2] = (_mu(i,0)-_Lambda(i,0))*_n(i,1)*_n(i,2);
	      K[2][1] = (_mu(i,0)-_Lambda(i,0))*_n(i,2)*_n(i,1);
	      K[1][1] = _Lambda(i,0) + (_mu(i,0)-_Lambda(i,0))*_n(i,1)*_n(i,1);
	      K[2][2] = _Lambda(i,0) + (_mu(i,0)-_Lambda(i,0))*_n(i,2)*_n(i,2);
        Sn[0] = (nx*K[0][0]+ny*K[0][1]+nz*K[0][2]);
        Sn[1] = (nx*K[1][0]+ny*K[1][1]+nz*K[1][2]);
        Sn[2] = (nx*K[2][0]+ny*K[2][1]+nz*K[2][2]);
	      val(i,0) = Sn[0]*nx + Sn[1]*ny + Sn[2]*nz;
    }
  }
};


class dgConservationLawVerticalCoord::flux : public function {
  fullMatrix<double> _diffusiveFlux, _n, _lv, _mu;
  public:
  flux(const function *diffusiveFlux, const function *n, const function *lv, const function *mu): function(3) {
    
    setArgument (_diffusiveFlux,diffusiveFlux);
    setArgument (_n,n);
    setArgument (_lv,lv);
    setArgument (_mu,mu);
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    val.scale (0);
    for(int i=0; i< val.size1(); i++) {
	    val(i,0) +=  _diffusiveFlux(i,0) - _mu(i,0) * _n(i,0) / _n(i,2) / _lv(i,0);
      val(i,1) +=  _diffusiveFlux(i,1) - _mu(i,0) * _n(i,1) / _n(i,2) / _lv(i,0);
      val(i,2) +=  _diffusiveFlux(i,2) - _mu(i,0) * _lv(i,0);
      }
  }
};

class dgConservationLawVerticalCoord::interfaceTerm : public function {
  fullMatrix<double> _ipTermIso;
  public:
  interfaceTerm(const function *ipTermIso):function(2) {
      setArgument (_ipTermIso, ipTermIso);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    // determine which element is below
    val.scale(0);
    for(int i=0; i< val.size1(); i++) {
	    val(i,0) = _ipTermIso(i,0);
	    val(i,1) = _ipTermIso(i,1);
    }
  }
};


void dgConservationLawVerticalCoord::setup () {
  _normalDiffTensor1 = new normalDiffTensor1(_n,_Lambda,_mu) ;
  _diffusiveFlux = new diffusiveFlux(_n,_Lambda,_mu);
  _normalDiffTensor0 = new normalDiffTensor0(_n,_Lambda,_mu);
  
  _ipTermIso = dgNewIpTerm(1,_diffusiveFlux,_normalDiffTensor0, false);
  _volumeTerm1[""] = new flux (_diffusiveFlux, _n, _lv, _mu);
  _interfaceTerm0[""] = new interfaceTerm (_ipTermIso);
//_interfaceTerm1N[""] = dgNewSymIpTerm3d(1, _normalDiffTensor1);
}

dgConservationLawVerticalCoord::~dgConservationLawVerticalCoord() {
  delete _normalDiffTensor1;
  delete _normalDiffTensor0;
  delete _diffusiveFlux;
  delete _fzero;
  delete _ipTermIso;
  delete _volumeTerm1[""];
  delete _interfaceTerm0[""];
  delete _interfaceTerm1N[""];
}

dgConservationLawVerticalCoord::dgConservationLawVerticalCoord() : dgConservationLawFunction(1)
{
  _fzero = new functionConstant(0.);
  _Lambda = _fzero;
  _lv = _fzero;
  _n = _fzero;
  _mu = _fzero;
}
