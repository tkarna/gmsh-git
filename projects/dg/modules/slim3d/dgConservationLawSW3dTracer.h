#ifndef _DG_CONSERVATION_LAW_SW_3D_TRACER_H
#define _DG_CONSERVATION_LAW_SW_3D_TRACER_H
#include "dgConservationLawFunction.h"
class function;

/**Advection and diffusion of a scalar, the advection and diffusion are provided by functions. 
 * \f[\frac{dc}{dt} + v. \nabla c - nu \Delta c = 0\f]
 */
class dgConservationLawSW3dTracer : public dgConservationLawFunction {
  const function *_uv, *_w, *_wM, *_eta, *_bath, *_dwMdz, *_kappaH, *_kappaV, *_diffusiveFlux, *_ipTerm, *_slope, *_Kedd, *_isopycnalDiffusion, *_interfaceIso, *_ipTermIso, *_sourceFunc, *_uGM;
  const function *_fzero, *_fzeroVec;
  double _laxFriedrichsFactor,_R;
  int _dt,_T;
  const function *_ref;
  bool _useConservativeALE;
  class source;
  class advection;
  class interfaceTerm;
  class maxConvectiveSpeed;
  class diffusiveFlux;
  class boundaryInFlux;
  class boundaryRelaxation;
  class isopycnalDiffusion;
  class interfaceIso;
  class diffusivityDotN;
  class boundarySurfaceT;
  class boundarySurfaceS;
  protected:
      dgGroupCollection *_groups;
  public:

  void setup();
  /**A new advection-diffusion law.
   * The horizontal advection velocity is given by 'uv' vector function while the vertical advection velocity is w.
   * Horizontal and vertical diffusivity are given by the functions kappaH and kappaV. */
  dgConservationLawSW3dTracer(const function *uv,const function *w);
  /**A new advection-diffusion law */
  dgConservationLawSW3dTracer();
  /** set factor for scaling Lax-Friedrichs penalty term [default LFF = 0]. */
  inline void setLaxFriedrichsFactor(double lff){ _laxFriedrichsFactor = lff;}
  inline void setUseConservativeALE(bool b) {_useConservativeALE = b;};
  inline void setSource(const function *f) {_sourceFunc = f;};
  inline void setCallRef(int dt, int T, const function *ref){_dt=dt;_T=T; _ref=ref;};
  inline void setUV(const function *f) {_uv = f;};
  inline void setW(const function *f) {_w = f;};
  inline void setKappaH(const function *kap) {_kappaH = kap;};
  inline void setKappaV(const function *kap) {_kappaV = kap;};
  inline void setWMesh(const function *wM) {_wM = wM;};
  inline void setEta(const function *eta) {_eta = eta;};
  inline void setBath(const function *bath) {_bath = bath;};
  inline void setWMeshDz(const function *dwMdz) {_dwMdz = dwMdz;};
  inline void setIsSpherical(double R) {_R= R;};
  inline void setIsoDiff(const function *S, const function *Kedd) {_slope = S; _Kedd = Kedd;};
  inline void setGMVel(const function *uGM) {_uGM = uGM; };
  ~dgConservationLawSW3dTracer();
  /** Boundary condition that sets the in-flowing tracer concentration to a prescribed value */
  dgBoundaryCondition *newInFluxBoundary(const function* outValue);
   /**Boundary condition for free surface for the temperature*/
  dgBoundaryCondition *newBoundarySurfaceT(const function* noSolarFlux);
     /**Boundary condition for free surface for the salinity*/
  dgBoundaryCondition *newBoundarySurfaceS(const function* salinityFlux);
  /** As InFlux boundary but also nudges the tracer towards the prescribed value with the given relaxation time scale */
  dgBoundaryCondition *newRelaxationBoundary(const function* outValue, double TRelax);
};

/**Diffusion of a scalar, the diffusivity provided by a function.
 * \f[\frac{dc}{dt} - nu \Delta c = 0\f]
 * This is lightweight version contains only vertical diffusion
 * to speed up the implict solver!
 */
class dgConservationLawSW3dTracerVDiff : public dgConservationLawFunction {
  const function *_bath, *_kappaV, *_diffusiveFlux, *_ipTerm;
  const function *_fzero;
  class maxConvectiveSpeed;
  class diffusiveFlux;
  class diffusivityDotN;
  public:

  void setup();
  /**A new advection-diffusion law.
   * Vertical diffusivity is given by the functions kappaV. */
  dgConservationLawSW3dTracerVDiff();
  virtual bool isLinear() const {return true;}
  inline void setKappaV(const function *kap) {_kappaV = kap;};
  inline void setBath(const function *bath) {_bath = bath;};
  ~dgConservationLawSW3dTracerVDiff();
  dgFaceTermAssembler *getFaceTerm(const dgGroupOfFaces &group) const;
};

#endif
