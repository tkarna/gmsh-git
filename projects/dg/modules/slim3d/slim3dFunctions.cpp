#include "slim3dFunctions.h"
#include "dgExtrusion.h"

inline static double _pow2(double x) {return x * x;}

// default values
double slim3dParameters::g = 9.81;
double slim3dParameters::R = 6371220.0;
double slim3dParameters::rho0 = 1027.0;
double slim3dParameters::vonKarman = 0.41587379967373655;
double slim3dParameters::nuv0 = 1.e-6;
double slim3dParameters::kapv0T = 1.e-6;
double slim3dParameters::kapv0S = 1.e-6;
double slim3dParameters::Cp = 4.e3; 

double SW3dEquationOfStateJackettBeta(double S, double Th, double p, double rho0) {
    if (S < 0)
      S = 0; // positivity of tracers cannot be ensured
    static double a[12] = { 9.9984085444849347e2, 7.3471625860981584e0, -5.3211231792841769e-2,
                      3.6492439109814549e-4, 2.5880571023991390e0, -6.7168282786692355e-3,
                      1.9203202055760151e-3, 1.1798263740430364e-2, 9.8920219266399117e-8,
                      4.6996642771754730e-6, -2.5862187075154352e-8, -3.2921414007960662e-12 };
    static double b[13] = { 1.0,7.2815210113327091e-3, -4.4787265461983921e-5, 3.3851002965802430e-7,
                      1.3651202389758572e-10, 1.7632126669040377e-3, -8.8066583251206474e-6,
                      -1.8832689434804897e-10, 5.7463776745432097e-6, 1.4716275472242334e-9,
                      6.7103246285651894e-6, -2.4461698007024582e-17, -9.1534417604289062e-18 };
    double Pn = a[0] + Th*a[1] + Th*Th*a[2] + Th*Th*Th*a[3] + S*a[4] + Th*S*a[5] + S*S*a[6] +
                p*a[7] + p*Th * Th*a[8] + p*S*a[9] + p*p*a[10] + p*p*Th*Th * a[11];
    double dndS = a[4] + a[5]*Th  + 2*a[6]*S + a[9]*p;
    double Pd = b[0] + Th*b[1] + Th*Th*b[2] + Th*Th*Th*b[3] + Th*Th*Th*Th*b[4] + S*b[5] + S*Th*b[6] +
                S*Th*Th*Th*b[7] + pow(S,1.5)*b[8] + pow(S,1.5)*Th*Th*b[9] + p*b[10] +
                p*p*Th*Th*Th*b[11] + p*p*p*Th*b[12];
    double dddS = b[5] + b[6]*Th + b[7]*Th*Th*Th + 1.5*b[8]*sqrt(S) + 1.5*b[9]*sqrt(S)*Th*Th;
    double beta = (Pd*dndS - Pn*dddS)/Pd/Pd;
    return beta;
}

functionStateEquationBeta::functionStateEquationBeta(const function * SFunction,const function * ThFunction,const function * pFunction, double rho0):function(1) {
  setArgument(S,SFunction);
  setArgument(Th,ThFunction);
  setArgument(p,pFunction);
  _rho0 = rho0;
}
void functionStateEquationBeta::call (dataCacheMap *m, fullMatrix<double> &val) {
  for(int i=0; i< S.size1(); i++) {
    val(i,0) = SW3dEquationOfStateJackettBeta(S(i,0), Th(i,0), p(i,0),_rho0);
  }
}

double SW3dEquationOfStateJackettAlpha(double S, double Th, double p, double rho0) {
    if (S < 0)
      S = 0; // positivity of tracers cannot be ensured
    static double a[12] = { 9.9984085444849347e2, 7.3471625860981584e0, -5.3211231792841769e-2,
                      3.6492439109814549e-4, 2.5880571023991390e0, -6.7168282786692355e-3,
                      1.9203202055760151e-3, 1.1798263740430364e-2, 9.8920219266399117e-8,
                      4.6996642771754730e-6, -2.5862187075154352e-8, -3.2921414007960662e-12 };
    static double b[13] = { 1.0,7.2815210113327091e-3, -4.4787265461983921e-5, 3.3851002965802430e-7,
                      1.3651202389758572e-10, 1.7632126669040377e-3, -8.8066583251206474e-6,
                      -1.8832689434804897e-10, 5.7463776745432097e-6, 1.4716275472242334e-9,
                      6.7103246285651894e-6, -2.4461698007024582e-17, -9.1534417604289062e-18 };
    double Pn = a[0] + Th*a[1] + Th*Th*a[2] + Th*Th*Th*a[3] + S*a[4] + Th*S*a[5] + S*S*a[6] +
                p*a[7] + p*Th * Th*a[8] + p*S*a[9] + p*p*a[10] + p*p*Th*Th * a[11];
    double dndT = a[1] + 2*a[2]*Th +3*a[3]*Th*Th + a[5]*S + 2*a[8]*p*Th + 2*a[11]*p*p*Th;
    double Pd = b[0] + Th*b[1] + Th*Th*b[2] + Th*Th*Th*b[3] + Th*Th*Th*Th*b[4] + S*b[5] + S*Th*b[6] +
                S*Th*Th*Th*b[7] + pow(S,1.5)*b[8] + pow(S,1.5)*Th*Th*b[9] + p*b[10] +
                p*p*Th*Th*Th*b[11] + p*p*p*Th*b[12];
    double dddT = b[1] + 2*b[2]*Th + 3*b[3]*Th*Th + 4*b[4]*Th*Th*Th + b[6]*S + 3*b[7]*S*Th*Th +
                2*b[9]*pow(S,1.5)*Th + 3*b[11]*p*p*Th*Th + b[12]*p*p*p;
    double alpha = (Pd*dndT - Pn*dddT)/Pd/Pd;
    return alpha;
}

functionStateEquationAlpha::functionStateEquationAlpha(const function * SFunction,const function * ThFunction,const function * pFunction, double rho0):function(1) {
  setArgument(S,SFunction);
  setArgument(Th,ThFunction);
  setArgument(p,pFunction);
  _rho0 = rho0;
}
void functionStateEquationAlpha::call (dataCacheMap *m, fullMatrix<double> &val) {
  for(int i=0; i< S.size1(); i++) {
    val(i,0) = SW3dEquationOfStateJackettAlpha(S(i,0), Th(i,0), p(i,0),_rho0);
  }
}

double SW3dEquationOfStateJackett(double S, double Th, double p, double rho0) {
    if (S < 0)
      S = 0; // positivity of tracers cannot be ensured
    static double a[12] = { 9.9984085444849347e2, 7.3471625860981584e0, -5.3211231792841769e-2,
                      3.6492439109814549e-4, 2.5880571023991390e0, -6.7168282786692355e-3,
                      1.9203202055760151e-3, 1.1798263740430364e-2, 9.8920219266399117e-8,
                      4.6996642771754730e-6, -2.5862187075154352e-8, -3.2921414007960662e-12 };
    static double b[13] = { 1.0,7.2815210113327091e-3, -4.4787265461983921e-5, 3.3851002965802430e-7,
                      1.3651202389758572e-10, 1.7632126669040377e-3, -8.8066583251206474e-6,
                      -1.8832689434804897e-10, 5.7463776745432097e-6, 1.4716275472242334e-9,
                      6.7103246285651894e-6, -2.4461698007024582e-17, -9.1534417604289062e-18 };
    double Pn = a[0] + Th*a[1] + Th*Th*a[2] + Th*Th*Th*a[3] + S*a[4] + Th*S*a[5] + S*S*a[6] +
                p*a[7] + p*Th * Th*a[8] + p*S*a[9] + p*p*a[10] + p*p*Th*Th * a[11];
    double Pd = b[0] + Th*b[1] + Th*Th*b[2] + Th*Th*Th*b[3] + Th*Th*Th*Th*b[4] + S*b[5] + S*Th*b[6] +
                S*Th*Th*Th*b[7] + S * sqrt(S) * (b[8] + Th*Th*b[9]) + p*b[10] +
                p*p*Th*Th*Th*b[11] + p*p*p*Th*b[12];
    double rho = Pn/Pd - rho0;
    return rho;
}

functionStateEquation::functionStateEquation(const function * SFunction,const function * ThFunction,const function * pFunction, double rho0):function(1) {
  setArgument(S,SFunction);
  setArgument(Th,ThFunction);
  setArgument(p,pFunction);
  _rho0 = rho0;
}
void functionStateEquation::call (dataCacheMap *m, fullMatrix<double> &val) {
  for(int i=0; i< S.size1(); i++) {
    //double Thh = potentialTemperature(S(i,0), Th(i,0), p(i,0), 0);
    //printf("%g %g %g\n", Thh, Th(i,0), Thh-Th(i,0));
    val(i,0) = SW3dEquationOfStateJackett(S(i,0), Th(i,0), p(i,0),_rho0);
  }
}

double potentialTemperature(double S, double T, double p, double pr) {
  static double a[7] = {8.65483913395442e-6, -1.41636299744881e-6, -7.38286467135737e-9,
                        -8.38241357039698e-6, 2.83933368585534e-8, 1.77803965218656e-8,
                        1.71155619208233e-10};
  double sol = T + (p-pr) * (a[0] + a[1]*S + a[2]*(p+pr) + a[3]*T + a[4]*S*T + a[5]*T*T + a[6]*T*(p+pr));
  return sol;
}

functionPotentialTemperature::functionPotentialTemperature(const function *SFunction,const function * ThFunction,const function * pFunction, const function *prFunction):function(1){
  setArgument(S,SFunction);
  setArgument(Th,ThFunction);
  setArgument(p,pFunction);
  setArgument(pr,prFunction);
}

void functionPotentialTemperature::call (dataCacheMap *m, fullMatrix<double> &val) {
  for(int i=0; i< S.size1(); i++) {
    val(i,0) = potentialTemperature(S(i,0), Th(i,0), p(i,0), pr(i,0)); 
  }
}

domeSmagorinsky::domeSmagorinsky(double factor, double max, const function * uvGrad, const dgExtrusion &extrusion) :
  function(1),
  _factor(factor),
  _max(max),
  _extrusion(extrusion)
{
  setArgument(_uvGrad, uvGrad);
}

void domeSmagorinsky::call(dataCacheMap *cmap, fullMatrix<double> &val)
{
  for (int i = 0; i < val.size1(); i++) {
    double area = _extrusion.area2d(cmap->getGroupId(), cmap->elementId(i/cmap->nPointByElement()));
    val(i, 0) = std::min(_max, _factor * area *
      sqrt(_pow2(_uvGrad(i, 0)) + 0.5 * _pow2(_uvGrad(i, 1) + _uvGrad(i, 3)) + _pow2(_uvGrad(i, 4))));
  }
}

pacanowskiPhilander::pacanowskiPhilander(const function *uvGrad, const function *rhoGrad, double nu0, double nuB, double alpha):
  function(1)
{
  _nuB = nuB;
  _nu0 = nu0;
  _alpha = alpha;
  setArgument(_uvGrad, uvGrad);
  setArgument(_rhoGrad, rhoGrad);
}

void pacanowskiPhilander::call(dataCacheMap *cmap, fullMatrix<double> &val)
{
  for (int i = 0; i < val.size1(); i++) {
    const double N2 = std::max(0.,  -_rhoGrad(i, 2) / slim3dParameters::rho0 * slim3dParameters::g);
    const double M2 = _pow2(_uvGrad(i, 2)) + _pow2(_uvGrad(i, 5));
    const double Ri = N2 / M2;
    if (Ri > 1000 || Ri != Ri)
      val(i, 0) = _nuB;
    else
      val(i, 0) = _nu0 / _pow2(1. + _alpha * Ri) + _nuB;
  }
}

linearStateEquation::linearStateEquation(const function *T, double alpha, double T0):
  function(1),
  _alpha(alpha),
  _T0(T0)
{
  setArgument(_T, T);
}

void linearStateEquation::call(dataCacheMap *cmap, fullMatrix<double> &val)
{
  for (int i = 0; i < val.size1(); i++) {
    val(i, 0) = _alpha * (_T(i, 0) - _T0);
  }
}

verticalVelocitySediment::verticalVelocitySediment(const function *sed, const function *w, const function *S):
  function(1)
{
  setArgument(_sed, sed);
  setArgument(_w, w);
  setArgument(_S, S);
}

void verticalVelocitySediment::call(dataCacheMap *cmap, fullMatrix<double> &val)
{
  double factorW = 1e-2;
  for (int i = 0; i < val.size1(); i++) {
    //if (_sed(i,0) > 3) Msg::Warning("vertical Velocity for sediment parametrization not valid anymore if sediment concentration greater than 3kg m^-3");
    if (_sed(i,0) < -1e-9) Msg::Warning(" Attention, les sediments ont une concentration negative dans la vitesse");
    double wS = std::max(0., std::min(2e-4, factorW * _sed(i,0))); // wS equal to max 2mm/s
    wS = (_S(i,0) > 2) ? wS : 0;
    if (wS > 1e-4) {Msg::Warning("ok, ca marche");}
    val(i, 0) = _w(i,0) - wS;
  }
}

sedimentBottomFlux::sedimentBottomFlux(const function *sed, const function *uvBot, const function *wind, const function *bath, const function *sedGround, const function *S):
  function(1)
{
  setArgument(_sed, sed);
  setArgument(_uvBot, uvBot);
  setArgument(_wind, wind);
  setArgument(_bath, bath);
  setArgument(_sedGround, sedGround);
  setArgument(_S, S);
}

void sedimentBottomFlux::call(dataCacheMap *cmap, fullMatrix<double> &val)
{
  double factorW = 1e-2;
  double u0 = 0.3;
  double w0 = 10;
  double a01 = 2.8e-2;
  double a02 = 1.44e-1;
  double a1 = 1e-6;
  double n = 4.;
  double g = slim3dParameters::g;
  double omega2 = 2.4538;
  for (int i = 0; i < val.size1(); i++) {
    //if (_sed(i,0) > 3) Msg::Warning("vertical Velocity for sediment parametrization not valid anymore if sediment concentration greater than 3kg m^-3");
    double uBot = hypot(_uvBot(i,0), _uvBot(i,1));
    double wS = std::max(0., std::min(2e-4, factorW * _sed(i,0))); // wS equal to max 2mm/s
    wS = (_S(i,0) > 2) ? wS : 0;
    if (_sed(i,0) < -1e-9) Msg::Warning(" Attention, les sediments ont une concentration negative dans le flux");
    double sed0 = std::max(0., _sed(i,0));
    double settling = (uBot < u0) ? - sed0 * wS * _pow2(1 - uBot/u0) : 0;
    if (_sedGround(i,0) < 1e-9)
      val(i,0) = settling;
    else{
      //double uWind = hypot(_wind(i,0), _wind(i,1)) + MAUVAIS!!!! LE VENT DU DOF, C'EST PAS LE VENT MAIS LE FORCING!!!;
      //double alpha = (_wind(i,0)*_uvBot(i,0) + _wind(i,1)*_uvBot(i,1))/(uWind*uBot);
      //double a0 = a01 + (a02 - a01) * (0.5 - atan(10*alpha)/(2*atan(10)));
      //double F = 0.245*exp(-0.905 * omega2 * _bath(i,0) / g - 0.0207);
      //double erosion0 = a0 * pow(uWind/w0, 3)*pow(uBot/u0, n)*F; 
      //double erosion1 = (uBot > u0) ? a1*(pow(uBot/u0, n) - 1) : 0; 
      val(i,0) = settling;// - erosion0 - erosion1;
    }
    // ET ERODER PAS PLUS QUE CE QU'IL Y A DANS SEDGROUND !
  }
}
