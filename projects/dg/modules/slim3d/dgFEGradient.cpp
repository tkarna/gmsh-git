#include "dgFEGradient.h"
#include "function.h"
#include "slim3dFunctions.h"

class dgFEGradient::gradPsiTerm: public function {
  fullMatrix<double> _rho,_xyz;
  double _R;
 
  public:
  gradPsiTerm (const function *rho, double R):function(9){
    setArgument(_rho,rho);
    setArgument(_xyz,function::getCoordinates());
    _R = R;
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    double J = 1.0;
    for(size_t i=0; i< nQP; i++) {
      if(_R > 0)
        J = 4*_R*_R/(4*_R*_R+_xyz(i,0)*_xyz(i,0)+_xyz(i,1)*_xyz(i,1));
      val(i,0) = -_rho(i,0) / J;
      val(i,1) = 0;
      val(i,2) = 0;
      val(i,3) = 0;
      val(i,4) =  -_rho(i,0) / J;
      val(i,5) = 0;
      val(i,6) = 0;
      val(i,7) = 0;
      val(i,8) = -_rho(i,0);
    }
  }
};

class dgFEGradient::interface0:public function {
  fullMatrix<double> _rhoL,_rhoR,_xyz;
  fullMatrix<double> _normals;
  double _R;
  public:

  interface0 (const function* rho,  double R): function(3){

    setArgument(_rhoL,rho,0);
    setArgument(_rhoR,rho,1);
    setArgument(_xyz,function::getCoordinates());
    _R = R;
    setArgument(_normals,function::getNormals(), 0);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = _rhoL.size1();
    double J = 1.0;
    for(size_t i=0; i< nQP; i++) {
      if(_R > 0)
        J = 4*_R*_R/(4*_R*_R+_xyz(i,0)*_xyz(i,0)+_xyz(i,1)*_xyz(i,1));
      double nx = _normals(i, 0);
      double ny = _normals(i, 1);
      double nz = _normals(i, 2);
      double rho = 0.5*(_rhoL(i,0)+_rhoR(i,0));
      val(i,0) = rho*nx/J;
      val(i,1) = rho*ny/J;
      val(i,2) = rho*nz;
    }
  }
};

class dgFEGradient::source: public function {
  fullMatrix<double> _rho, _xyz, sol;
  double _R;
  public :
  source(const function *rho, double R) : function (3){
    setArgument(_rho,rho);
    setArgument(sol,function::getSolution());
    setArgument(_xyz,function::getCoordinates());
    _R = R;
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP =_rho.size1();
    double J = 1.0;
    // BUG sol has wrong size!
    //printf("sol size %d %d\n", sol.size1(),sol.size2());
    if(_R > 0) {
      for (size_t i=0; i<nQP; i++) {
        J = 4*_R*_R/(4*_R*_R+_xyz(i,0)*_xyz(i,0)+_xyz(i,1)*_xyz(i,1));
        val (i,0) = (-_xyz(i,0)*_rho(i,0)/2.0/_R/_R + sol(i,0)/J);
        val (i,1) = (-_xyz(i,1)*_rho(i,0)/2.0/_R/_R + sol(i,1)/J);
        val (i,2) = sol(i,2);
      }
    }
  }
};


void dgFEGradient::setup() {
  _volumeTerm1[""] = new gradPsiTerm(_rho,  _R);
  _interfaceTerm0[""] = new interface0(_rho,  _R);
  _volumeTerm0[""] = (_R > 0) ? new source(_rho,_R) : NULL;
  if (!_rho)
    Msg::Fatal("source function must be set");
}

dgFEGradient::dgFEGradient(const function* rho): dgConservationLawFunction(3) {
  _rho = rho;
   _R = -1;
}

dgFEGradient::dgFEGradient(): dgConservationLawFunction(3) {
  _rho = NULL;
  _R = -1;
}

dgFEGradient::~dgFEGradient() {
  if ( _volumeTerm1[""] ) delete _volumeTerm1[""];
  if ( _interfaceTerm0[""] ) delete _interfaceTerm0[""];
  if (_volumeTerm0[""]) delete _volumeTerm0[""];
}

class dgFEVectorGradient::gradPsiTerm: public function {
  fullMatrix<double> _uv,_xyz;
  double _R;
 
  public:
  gradPsiTerm (const function *uv, double R):function(18){
    setArgument(_uv,uv);
    setArgument(_xyz,function::getCoordinates());
    _R = R;
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    double J = 1.0;
    val.setAll(0.);
    for(size_t i=0; i< nQP; i++) {
      if(_R > 0)
        J = 4*_R*_R/(4*_R*_R+_xyz(i,0)*_xyz(i,0)+_xyz(i,1)*_xyz(i,1));
      val(i,0) = -_uv(i,0) / J;
      val(i,3) = -_uv(i,1) / J;
      val(i,7) = -_uv(i,0) / J;
      val(i,10) = -_uv(i,1) / J;
      val(i,14) = -_uv(i,0) / J;
      val(i,17) = -_uv(i,1) / J;
      //val(i,0) = -_uv(i,0) / J;
      //val(i,2) = 0;
      //val(i,4) = 0;
      //val(i,6) = 0;
      //val(i,8) =  -_uv(i,0) / J;
      //val(i,10) = 0;
      //val(i,12) = 0;
      //val(i,14) = 0;
      //val(i,16) = -_uv(i,0);
      //val(i,1) = -_uv(i,1) / J;
      //val(i,3) = 0;
      //val(i,5) = 0;
      //val(i,7) = 0;
      //val(i,9) =  -_uv(i,1) / J;
      //val(i,11) = 0;
      //val(i,13) = 0;
      //val(i,15) = 0;
      //val(i,17) = -_uv(i,1);
    }
  }
};

class dgFEVectorGradient::interface0:public function {
  fullMatrix<double> _uvL,_uvR, _etaL, _etaR, _bath,_xyz;
  fullMatrix<double> _normals;
  double _R;
  public:
  interface0 (const function *uv, const function *eta, const function *bathymetry, double R): function(6){
    setArgument(_bath, bathymetry, 0);
    setArgument(_uvL, uv, 0);
    setArgument(_uvR, uv, 1);
    setArgument(_etaL, eta, 0);
    setArgument(_etaR, eta, 1);
    setArgument(_xyz,function::getCoordinates());
    _R = R;
    setArgument(_normals,function::getNormals(), 0);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = _uvL.size1();
    double g = slim3dParameters::g;
    double J = 1.0;
    for(size_t i=0; i< nQP; i++) {
      if(_R > 0)
        J = 4*_R*_R/(4*_R*_R+_xyz(i,0)*_xyz(i,0)+_xyz(i,1)*_xyz(i,1));
      double nx = _normals(i, 0);
      double ny = _normals(i, 1);
      double nz = _normals(i, 2);
      double etaM = 0.5 * (_etaL(i,0) + _etaR(i,0));
      double u = 0.5*(_uvL(i,0)+_uvR(i,0)) + nx * 0.5*sqrt(g/(_bath(i,0) + etaM)) * (_etaL(i,0) - _etaR(i,0));
      double v = 0.5*(_uvL(i,1)+_uvR(i,1)) + ny * 0.5*sqrt(g/(_bath(i,0) + etaM)) * (_etaL(i,0) - _etaR(i,0));
      val(i,0) = u*nx/J;
      val(i,1) = u*ny/J;
      val(i,2) = u*nz;
      val(i,3) = v*nx/J;
      val(i,4) = v*ny/J;
      val(i,5) = v*nz;
      //val(i,0) = u*nx/J;
      //val(i,2) = u*ny/J;
      //val(i,4) = u*nz;
      //val(i,1) = v*nx/J;
      //val(i,3) = v*ny/J;
      //val(i,5) = v*nz;
    }
  }
};

class dgFEVectorGradient::source: public function {
  fullMatrix<double> _uv, _xyz, sol;
  double _R;
  public :
  source(const function *uv, double R) : function (3){
    setArgument(_uv,uv);
    setArgument(sol,function::getSolution());
    setArgument(_xyz,function::getCoordinates());
    _R = R;
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    //size_t nQP =_uv.size1();
    //double J = 1.0;
    if(_R > 0) {
      Msg::Fatal("Stereo not implemented for vector gradient equation");
      //for (size_t i=0; i<nQP; i++) {
      //  J = 4*_R*_R/(4*_R*_R+_xyz(i,0)*_xyz(i,0)+_xyz(i,1)*_xyz(i,1));
      //}
    }
  }
};


void dgFEVectorGradient::setup() {
  _volumeTerm1[""] = new gradPsiTerm(_uv,  _R);
  _interfaceTerm0[""] = new interface0(_uv, _eta, _bath, _R);
  _volumeTerm0[""] = (_R > 0) ? new source(_uv,_R) : NULL;
  if (!_uv)
    Msg::Fatal("source function must be set");
}

dgFEVectorGradient::dgFEVectorGradient(const function* uv): dgConservationLawFunction(6) {
  _uv = uv;
  _eta = NULL;
  _bath = NULL;
   _R = -1;
}

dgFEVectorGradient::dgFEVectorGradient(): dgConservationLawFunction(6) {
  _uv = NULL;
  _eta = NULL;
  _bath = NULL;
  _R = -1;
}

dgFEVectorGradient::~dgFEVectorGradient() {
  if ( _volumeTerm1[""] ) delete _volumeTerm1[""];
  if ( _interfaceTerm0[""] ) delete _interfaceTerm0[""];
  if (_volumeTerm0[""]) delete _volumeTerm0[""];
}

/** Compute in dg the horizontal divergence of uv */
class dgUVHorDivergence::gradPsiTerm: public function {
  fullMatrix<double> _uv;
  public:
  gradPsiTerm (const function *uv):function(3){
    setArgument(_uv, uv);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++) {
      val(i,0) = -_uv(i,0);
      val(i,1) = -_uv(i,1);
      val(i,2) = 0;
    }
  }
};

class dgUVHorDivergence::interface0: public function {
  fullMatrix<double> _bath, _uvL, _uvR, _etaL, _etaR, _normals;
  public:
  interface0 (const function *bathymetry, const function *uv, const function *eta):function(1){
    setArgument(_bath, bathymetry, 0);
    setArgument(_uvL, uv, 0);
    setArgument(_uvR, uv, 1);
    setArgument(_etaL, eta, 0);
    setArgument(_etaR, eta, 1);
    setArgument(_normals, function::getNormals());
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    double g = slim3dParameters::g;
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++) {
      double etaM = 0.5 * (_etaL(i,0) + _etaR(i,0));
      double u = 0.5*(_uvL(i,0) + _uvR(i,0)) ;
      double v = 0.5*(_uvL(i,1) + _uvR(i,1)) ;
      val(i,0) = u * _normals(i,0) + v * _normals(i,1) + 0.5*sqrt(g/(_bath(i,0) + etaM)) * (_etaL(i,0) - _etaR(i,0));
    }
  }
};

void dgUVHorDivergence::setup() {
  _volumeTerm1[""] = new gradPsiTerm(_uv);
  _interfaceTerm0[""] = new interface0(_bath, _uv, _eta);
}

dgUVHorDivergence::dgUVHorDivergence(): dgConservationLawFunction(1) {
  _bath = NULL;
  _uv = NULL;
  _eta = NULL;
}

dgUVHorDivergence::~dgUVHorDivergence() {
  if ( _volumeTerm1[""] ) delete _volumeTerm1[""];
  if ( _interfaceTerm0[""] ) delete _interfaceTerm0[""];
}

class dgUVHorDivergence::boundaryWall : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> _normals, _uv, _eta, _xyz;
    fullMatrix <double> externalUV, interfaceTerm, externalEta;
    functionReplace replaceFunction;
    public:
    term(dgConservationLawFunction *claw, const function *uv, const function *eta):function(claw->getNbFields()) {
      replaceFunction.addChild();
      addFunctionReplace (replaceFunction);
      replaceFunction.replace (externalUV, uv, 1);
      replaceFunction.replace (externalEta, eta, 1);
      replaceFunction.get(interfaceTerm,claw->getInterfaceTerm0(""));
      setArgument(_uv,uv);
      setArgument(_normals,function::getNormals());
      setArgument(_eta,eta);
      setArgument(_xyz,function::getCoordinates());
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      for(int i=0;i<val.size1(); i++) {
        //double nx = _normals(i,0);
        //double ny = _normals(i,1);
        double u = _uv(i,0), v = _uv(i,1);
        //double uvN = u*nx+v*ny;
        externalUV(i,0) = u;// - 2*uvN*nx;
        externalUV(i,1) = v;// - 2*uvN*ny;
        externalEta(i,0) = _eta(i,0);
      }    
      replaceFunction.compute();
      for(int i=0;i<val.size1(); i++)
          val(i,0) = interfaceTerm(i,0);
    }
  };
  term _term;
  public:
  boundaryWall(const function* _uv, const function* _eta, dgConservationLawFunction *claw): _term(claw, _uv, _eta) {
    _term0 = &_term;
  }
};

dgBoundaryCondition *dgUVHorDivergence::newBoundaryWall(){
  return new boundaryWall(_uv, _eta, this);
}
