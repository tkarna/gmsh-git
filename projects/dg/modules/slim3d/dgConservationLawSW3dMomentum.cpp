#include "dgConservationLawSW3dMomentum.h"
#include "functionGeneric.h"
#include "float.h"
#include "dgGroupOfElements.h"
#include "slim3dFunctions.h"
#include "dgMeshJacobian.h"

inline void approxRoe(double etaL, double etaR, double uL, double uR, double vL, double vR, double h, double& eta, double& u, double &v) {
  double HR = etaR + h, HL = etaL + h;
//   if(HR<0 || HL<0) printf(" HR = %e HL =%e\n", HR,HL);
  double HuL = uL*HL, HuR = uR*HR;
  double HvL = vL*HL, HvR = vR*HR;
  double HM = (HL+HR)/2, HJ = (HL-HR)/2;
  double HuM = (HuL+HuR)/2, HuJ = (HuL-HuR)/2;
  double sqHL = sqrt(HL), sqHR = sqrt(HR);
  double u_roe = (sqHL*uL + sqHR*uR) / (sqHL + sqHR);
  double v_roe = (sqHL*vL + sqHR*vR) / (sqHL + sqHR);
  double c_roe = sqrt(slim3dParameters::g*HM);
  double H = HM + (HuJ - u_roe *HJ) / c_roe;
  double Hu = HuM + (c_roe - u_roe*u_roe/c_roe) *HJ + u_roe/c_roe *HuJ;
  double Hv = -v_roe*u_roe/c_roe*HJ + v_roe/c_roe*HuJ + (u_roe>0 ? -v_roe*HJ+HvL : v_roe*HJ+HvR);
  u = Hu / H;
  v = Hv / H;
  eta  = H-h;
}

inline void linearRoe(double etaL, double etaR, double uL, double uR, double h, double& eta, double& u) {
  double g = slim3dParameters::g;
  //linear equations
  double etaM = 0.5*(etaL+etaR);
  double sq_g_h = sqrt(g/(h+etaM));
  eta = (etaL+etaR + (uL-uR)/sq_g_h)/2;
  u = (uL+uR + (etaL-etaR)*sq_g_h)/2;
}

class dgConservationLawSW3dMomentum::maxConvectiveSpeed: public function {
  fullMatrix<double> _bath, _sol, _eta, _w;
  public:
  maxConvectiveSpeed (const function *eta, const function *bathymetry, const function *w):function(3){
    setArgument(_eta,eta);
    setArgument(_w,w);
    setArgument(_bath,bathymetry);
    setArgument(_sol,function::getSolution());
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    double g = slim3dParameters::g;
    for(int i=0; i< val.size1(); i++) {
      double normU = sqrt(_sol(i,0)*_sol(i,0)+_sol(i,1)*_sol(i,1)+_w(i,0)*_w(i,0));
      double c = sqrt( (_bath(i,0)+_eta(i,0))*g);
      val(i,0) = c + normU;
    }
  }
};

class dgConservationLawSW3dMomentum::advection: public function {
 fullMatrix<double> _bath, _UV, _UVGrad, _eta, _rhoSurf, _w, _wMesh, _diff,_xyz;
 double _R;
 bool _haveDiffusion, _haveWMesh;
  public:
  advection (const function *eta, const function *rhoSurf, const function *bathymetry, const function* w, const function* wM, const function* diff, double R):function(6){
    _haveWMesh = wM;
    _haveDiffusion = diff;
    setArgument(_bath,bathymetry);
    setArgument(_eta,eta);
    setArgument(_rhoSurf,rhoSurf);
    setArgument(_w,w);
    if ( _haveWMesh)
      setArgument(_wMesh,wM);
    setArgument(_UV,function::getSolution());
    setArgument(_UVGrad,function::getSolutionGradient());
    if ( _haveDiffusion)
      setArgument(_diff,diff);
    setArgument(_xyz,function::getCoordinates());
    _R=R;

  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    double g = slim3dParameters::g;
    double rho0 = slim3dParameters::rho0;
    for(size_t i=0; i< nQP; i++) {
      double eta = _eta(i,0);
      double rhoSurf = _rhoSurf(i,0);
      double u = _UV(i,0);
      double v = _UV(i,1);
      double w = _w(i,0);
      double J = 1;
      if (_R>0)
        J = 4*_R*_R/(4*_R*_R+_xyz(i,0)*_xyz(i,0)+_xyz(i,1)*_xyz(i,1));
      if (_haveWMesh)
        w += -_wMesh(i,0);
      // flux_x
      val(i,0) = u*u/J + g*(1+rhoSurf/rho0)*eta/J;
      val(i,1) = u*v/J;
      // flux_y
      val(i,2) = v*u/J;
      val(i,3) = v*v/J + g*(1+rhoSurf/rho0)*eta/J;
      // flux_z
      val(i,4) = w*u;
      val(i,5) = w*v;
    }
    if ( _haveDiffusion) {
      for(size_t i=0; i< nQP; i++) {
      // flux_x
        val(i,0) += _diff(i,0); //dudx
        val(i,1) += _diff(i,1); //dvdx
      // flux_y
        val(i,2) += _diff(i,2); //dudy
        val(i,3) += _diff(i,3); //dvdy
      // flux_z
        val(i,4) += _diff(i,4);
        val(i,5) += _diff(i,5);
      }
    }
  }
};

class dgConservationLawSW3dMomentum::source: public function {
  fullMatrix<double> _xyz, _eta, _rhoSurf, _rGrad, _UV, _UVGradient, _dwMeshdz,_coriolisFactor, _linearDissipation, _quadraticDissipation, _source;
  double _R;
  dgConservationLawSW3dMomentum* _claw;
  public :
  source(dgConservationLawSW3dMomentum* claw, const function *dwMdz, const function *linearDissipationF, const function *quadraticDissipationF, const function *coriolisFactorF, const function *sourceF, double R, const function *eta, const function *rhoSurf, const function *rGrad) :
    function (2){
    setArgument(_UV,function::getSolution());
    setArgument(_UVGradient,function::getSolutionGradient());
    setArgument(_dwMeshdz,dwMdz);
    setArgument(_coriolisFactor,coriolisFactorF);
    setArgument(_linearDissipation,linearDissipationF);
    setArgument(_quadraticDissipation,quadraticDissipationF);
    setArgument(_source,sourceF);
    setArgument(_xyz,function::getCoordinates());
    setArgument(_rGrad, rGrad);
    _R=R;
    if(_R>0){
      setArgument(_eta,eta);
      setArgument(_rhoSurf,rhoSurf);
    }
    _claw = claw;
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    double g = slim3dParameters::g;
    double rho0 = slim3dParameters::rho0;
    size_t nQP =val.size1();
    for (size_t i=0; i<nQP; i++) {
      double u = _UV(i,0);
      double v = _UV(i,1);
      double normu = hypot(u,v);
      val (i,0) = +_coriolisFactor(i,0)*v - (_linearDissipation(i,0) +_quadraticDissipation(i,0)*normu)*u + _source(i,0);
      val (i,1) = -_coriolisFactor(i,0)*u - (_linearDissipation(i,0) +_quadraticDissipation(i,0)*normu)*v + _source(i,1);
      
      val(i,0) += -g*_rGrad(i,0);
      val(i,1) += -g*_rGrad(i,1);

      if ( _claw->_useConservativeALE ) {
        val (i,0) +=  - u*_dwMeshdz(i,0);
        val (i,1) +=  - v*_dwMeshdz(i,0);
      }
      if (_R>0){
        val(i,0)+= (_xyz(i,0)*(u*u+v*v)-3*(_xyz(i,0)*u*u+_xyz(i,1)*u*v)- g*_xyz(i,0)*(1+_rhoSurf(i,0)/rho0)*_eta(i,0))/(2*_R*_R);
        val(i,1)+= (_xyz(i,1)*(u*u+v*v)-3*(_xyz(i,0)*v*u+_xyz(i,1)*v*v)- g*_xyz(i,1)*(1+_rhoSurf(i,0)/rho0)*_eta(i,0))/(2*_R*_R);
      }
    }
  }
};

class dgConservationLawSW3dMomentum::riemann:public function {
  fullMatrix<double> _xyz, _etaL, _etaR, _rhoSurfL, _rhoSurfR, _UL, _UR, _U2dL, _U2dR, _solGradL, _solGradR, _wL, _wR,_wMeshL, _wMeshR;
  fullMatrix<double> _bath, _normals, _ipTerm;
  bool _haveDiff,_haveWMesh;
  double _laxFriedrichsFactor,_R;
  public:
  riemann (const function *uv2d, const function *eta, const function *rhoSurf, const function *bathymetry, const function* w, const function* wM, const function *ipTerm, double lff,double R): function(2){
    _haveWMesh = wM;
    _haveDiff = ipTerm;
    setArgument(_UL,function::getSolution(), 0);
    setArgument(_UR,function::getSolution(), 1);
    setArgument(_etaL,eta, 0);
    setArgument(_etaR,eta, 1);
    setArgument(_rhoSurfL,rhoSurf, 0);
    setArgument(_rhoSurfR,rhoSurf, 1);
    setArgument(_U2dL,uv2d, 0);
    setArgument(_U2dR,uv2d, 1);
    setArgument(_bath,bathymetry, 0);
    setArgument(_wL,w, 0);
    setArgument(_wR,w, 1);
    if (_haveWMesh) {
      setArgument(_wMeshL,wM, 0);
      setArgument(_wMeshR,wM, 1);
    }
    setArgument(_normals,function::getNormals(), 0);
    if ( _haveDiff) {
      setArgument (_ipTerm, ipTerm);
    }
    _laxFriedrichsFactor = lff;
    setArgument(_xyz,function::getCoordinates());
    _R=R;
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    double g = slim3dParameters::g;
    double rho0 = slim3dParameters::rho0;
    dataCacheMap* dgMap = m;
    int layerIdIn = dgMap->getGroupOfElements()->getLayerId();
    int layerIdOut = dgMap->getSecondaryCache(1)->getGroupOfElements()->getLayerId();
    bool outIsTop = layerIdIn < layerIdOut;
    size_t nQP = _UL.size1();
    for(size_t i=0; i< nQP; i++) {
      double J = 1;
      if (_R>0)
        J = 4*_R*_R/(4*_R*_R+_xyz(i,0)*_xyz(i,0)+_xyz(i,1)*_xyz(i,1));
      double nx = _normals(i,0);
      double ny = _normals(i,1);
      double nz = _normals(i,2);

      double h = _bath(i,0);
      double un,vn; // in normal,tangential coordinates
      double u,v; // in x,y,z coordinates
      double unL = nx*_UL(i,0) + ny*_UL(i,1), unR = nx*_UR(i,0) + ny*_UR(i,1);
      double vnL = -ny*_UL(i,0) + nx*_UL(i,1), vnR = - ny*_UR(i,0) + nx*_UR(i,1);
      double un2dL = nx*_U2dL(i,0) + ny*_U2dL(i,1), un2dR = nx*_U2dR(i,0) + ny*_U2dR(i,1);
      double vn2dL = -ny*_U2dL(i,0) + nx*_U2dL(i,1), vn2dR = - ny*_U2dR(i,0) + nx*_U2dR(i,1);
      double eta;
      double rhoSurf = 0.5*(_rhoSurfL(i,0) + _rhoSurfR(i,0));
      double wL = _wL(i,0), wR = _wR(i,0);
      if (_haveWMesh) {
        wL += -_wMeshL(i,0);
        wR += -_wMeshR(i,0);
      }
      if (rhoSurf < -100000)
        printf("bad rho\n");
      if (_etaR(i, 0) < -100000)
        printf("bad eta\n");
      if (fabs(nz)<1e-8) { // lateral face
        if (true ) {
          // should be symmetric for lateral bnd conditions
          double etaM = 0.5*(_etaL(i,0)+_etaR(i,0));
          double sq_g_h = sqrt(g/(h+etaM));
          // NOTE: using un2d instead of un is essential for stability
          //eta = etaM + ((un2dL/HL-un2dR/HR)/sq_g_h)/2;
          eta = etaM + ((un2dL-un2dR)/sq_g_h)/2; // uv2d is depth average
          un = 0.5*(unL+unR) + 0.5*(_etaL(i,0)-_etaR(i,0))*sq_g_h;
          //un = 0.5*(unL+unR) + 0.5*(wL - wR)*1;
          vn = un > 0 ? vnL : vnR;
      if (un2dL < -100000) printf("bad un2dL\n");
      if (un2dR < -100000) printf("bad un2dR\n");
        }
        else{
          un = 0;
          vn = 0;
          approxRoe(_etaL(i,0), _etaR(i,0), un2dL/h, un2dR/h, vn2dL/h, vn2dR/h, h, eta, un, vn);
        }
        if ( un > 0 ) { // upwinding
          u = _UL(i,0);
          v = _UL(i,1);
        } else {
          u = _UR(i,0);
          v = _UR(i,1);
        }
      } else {
        // down value (Aizinger & Dawson)
        if (outIsTop) {
          un = nx*_UL(i,0) + ny*_UL(i,1) + nz*wL;
          eta = _etaL(i,0);
        } else {
          un = nx*_UR(i,0) + ny*_UR(i,1) + nz*wR;
          eta = _etaR(i,0);
        }
        if ( un > 0 ) { // upwinding
          u = _UL(i,0);
          v = _UL(i,1);
        } else {
          u = _UR(i,0);
          v = _UR(i,1);
        }
      }
      val(i,0) = -u*un/J -g*(1+rhoSurf/rho0)*eta*nx/J;
      val(i,1) = -v*un/J -g*(1+rhoSurf/rho0)*eta*ny/J;
      // Lax friedrichs
      if (_laxFriedrichsFactor) {
        double uvwnL = nx*_UL(i,0) + ny*_UL(i,1) + nz*wL;
        double uvwnR = nx*_UR(i,0) + ny*_UR(i,1) + nz*wR;
        double gamma = 0.5 * ( fabs(uvwnL) + fabs(uvwnR) );
        double uJ = (_UL(i,0)-_UR(i,0))/2, vJ = (_UL(i,1)-_UR(i,1))/2;
        val(i,0) += -_laxFriedrichsFactor*gamma*uJ;
        val(i,1) += -_laxFriedrichsFactor*gamma*vJ;
      }
      if ( _haveDiff) {
        val(i,0) += _ipTerm(i,0);
        val(i,1) += _ipTerm(i,1);
      }
      if (val(i, 0) < -100000) printf("bad u riemann\n");
      if (val(i, 1) < -100000) printf("bad v riemann\n");
    }
  }
};

class dgConservationLawSW3dMomentum::diffusiveFlux : public function {
  fullMatrix<double> _UVgradient, nuH, nuV;
  public:
  diffusiveFlux(const function * nuHFunction,const function * nuVFunction):function(6) {
    setArgument(_UVgradient,function::getSolutionGradient());
    setArgument(nuH,nuHFunction);
    setArgument(nuV,nuVFunction);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    for(int i=0; i< _UVgradient.size1(); i++) {
      val(i,0) = -_UVgradient(i,0)*nuH(i,0); //dudx
      val(i,1) = -_UVgradient(i,3)*nuH(i,0); //dvdx
      val(i,2) = -_UVgradient(i,1)*nuH(i,0); //dudy
      val(i,3) = -_UVgradient(i,4)*nuH(i,0); //dvdy
      val(i,4) = -_UVgradient(i,2)*nuV(i,0); //dudz
      val(i,5) = -_UVgradient(i,5)*nuV(i,0); //dvdz
    }
  }
};

class dgConservationLawSW3dMomentum::maxDiffusivity : public function {
  fullMatrix<double> _nu1,_nu2;
  public:
  maxDiffusivity(const function *nu1, const function *nu2):function(1) {
    setArgument (_nu1, nu1);
    setArgument (_nu2, nu2);
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++) {
      val(i,0) = std::max(_nu1(i,0),_nu2(i,0));
    }
  }
};

dgConservationLawSW3dMomentum::dgConservationLawSW3dMomentum() : dgConservationLawFunction(2){
   // u v
  _fzero = new functionConstant(0.);
  _fzerov = new functionConstant(std::vector<double>(2,0));
  _bathymetry = NULL;
  _nuH = NULL;
  _nuV = NULL;
  _diffusivity[""] = NULL;
  _diffusiveFlux = NULL;
  _ipTerm = NULL;
  _linearDissipation = _fzero;
  _coriolisFactor = _fzero;
  _quadraticDissipation = _fzero;
  _eta = _fzero;
  _w = _dwMdz = _fzero;
  _wM = NULL;
  _uv2d = NULL;
  _source = _fzerov;
  _r = _fzero;
  _rGrad = _fzerov;
  _rhoSurf = _fzero;
  _maxDiffusivity = NULL;
  _laxFriedrichsFactor = 0.0;
  _R = -1.0;
}

class dgConservationLawSW3dMomentum::diffusivityDotN : public function {
 public:
  fullMatrix<double> _nuH, _nuV, _normals;
  void call(dataCacheMap *m, fullMatrix<double> &val)
  {
    for (int i = 0; i < val.size1(); i++) {
      for (int j = 0; j < val.size2(); j++) {
        double nx = _normals(i,0);
        double ny = _normals(i,1);
        double nz = _normals(i,2);
        val(i, j)= nx*nx*_nuH(i,0) + ny*ny*_nuH(i,0) +nz*nz*_nuV(i,0);
      }
    }
  }
  diffusivityDotN(const function *nuV, const function *nuH) : function(1)
  {
    setArgument (_nuV, nuV);
    setArgument (_nuH, nuH);
    setArgument (_normals,function::getNormals());
  }
};

void dgConservationLawSW3dMomentum::setup() {
  if (!_bathymetry)
    Msg::Fatal("dgConservationLawSW3dMomentum: Bathymetry function must be defined");
  if (!_uv2d)
    Msg::Fatal("dgConservationLawSW3dMomentum: Depth integrated velocity (uv2d) function must be defined");
  bool haveDiffusion = _nuH || _nuV;
  if (haveDiffusion) {
    if (_nuH == NULL) _nuH = _fzero;
    if (_nuV == NULL) _nuV = _fzero;
    _maxDiffusivity = new diffusivityDotN(_nuV,_nuH);
    std::vector<const function *> fArray;
    fArray.push_back(_maxDiffusivity);
    fArray.push_back(_maxDiffusivity);
    _diffusivity[""] = functionCatCompNew(fArray);
  }
  _diffusiveFlux = haveDiffusion ? new diffusiveFlux (_nuH,_nuV) : NULL;
  _ipTerm = haveDiffusion ? dgNewIpTerm (_nbf, _diffusiveFlux, _diffusivity[""]) : NULL;
  //_volumeTerm0[""] = new source(this, _dwMdz, _linearDissipation, _quadraticDissipation, _coriolisFactor, _source, _R, _eta, _r);
  //_volumeTerm1[""] = new advection(_eta, _r, _bathymetry, _w,_wM, _diffusiveFlux,_R);
  //_interfaceTerm0[""] = new riemann(_uv2d, _eta, _r, _bathymetry, _w,_wM, _ipTerm,_laxFriedrichsFactor,_R);
  _volumeTerm0[""] = new source(this, _dwMdz, _linearDissipation, _quadraticDissipation, _coriolisFactor, _source, _R, _eta, _rhoSurf, _rGrad);
  _volumeTerm1[""] = new advection(_eta, _rhoSurf, _bathymetry, _w,_wM, _diffusiveFlux,_R);
  _interfaceTerm0[""] = new riemann(_uv2d, _eta, _rhoSurf, _bathymetry, _w,_wM, _ipTerm,_laxFriedrichsFactor,_R);
  _maximumConvectiveSpeed[""] = new maxConvectiveSpeed(_eta, _bathymetry, _w);
}

dgConservationLawSW3dMomentum::~dgConservationLawSW3dMomentum() {
  delete _fzero;
  delete _fzerov;
  if (_ipTerm) delete _ipTerm;
  if (_volumeTerm0[""]) delete _volumeTerm0[""];
  if (_volumeTerm1[""]) delete _volumeTerm1[""];
  if (_interfaceTerm0[""]) delete _interfaceTerm0[""];
  if (_maximumConvectiveSpeed[""]) delete _maximumConvectiveSpeed[""];
  if (_clipToPhysics[""]) delete _clipToPhysics[""];
  if (_maxDiffusivity) delete _maxDiffusivity;
  if (_diffusivity[""]) delete _diffusivity[""];
  if (_diffusiveFlux) delete _diffusiveFlux;
}

class dgConservationLawSW3dMomentum::boundaryWall : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> _normals, _UV, _UVGrad, _UVInt, _eta, _rhoSurf, _w, _wMesh,_extDiffFlux, _zBotDist;
    fullMatrix <double> externalSol, externalSolGrad, externalUVInt, interfaceTerm, externalEta, externalW, externalWMesh, externalIPTerm, externalDiffFlux, externalRhoSurf;
    functionReplace replaceFunction;
    bool haveWMesh, haveDiff, haveZBotDist;
    public:
    term(dgConservationLawFunction *claw, const function *eta, const function *rhoSurf, const function *uvInt, const function *w, const function *wM, const function *ipTerm, const function *extDiffFlux, const function *zBotDist):function(claw->getNbFields()) {
      replaceFunction.addChild();
      addFunctionReplace (replaceFunction);
      replaceFunction.replace (externalSol, function::getSolution(), 1);
      replaceFunction.replace (externalUVInt, uvInt, 1);
      replaceFunction.replace (externalEta, eta, 1);
      replaceFunction.replace (externalW, w, 1);
      replaceFunction.replace (externalRhoSurf, rhoSurf, 1);
      replaceFunction.get(interfaceTerm,claw->getInterfaceTerm0(""));
      setArgument(_UV,function::getSolution());
      setArgument(_UVGrad,function::getSolutionGradient());
      setArgument(_UVInt,uvInt);
      setArgument(_normals,function::getNormals());
      setArgument(_eta,eta);
      setArgument(_rhoSurf,rhoSurf);
      setArgument(_w,w);
      setArgument(_extDiffFlux,extDiffFlux);
      haveZBotDist = zBotDist;
      if (haveZBotDist)
        setArgument(_zBotDist,zBotDist);
      
      haveWMesh = wM;
      if (haveWMesh) {
        setArgument(_wMesh,wM);
        replaceFunction.replace (externalWMesh, wM, 1);
      }
      haveDiff = ipTerm;
//       if (ipTerm)
//         replaceFunction.replace (externalIPTerm, ipTerm, 0);
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      double rho0 = slim3dParameters::rho0;
      // bnd condition for non-linear momentum (more unstable)
      for(int i=0;i<val.size1(); i++) {
        double nx = _normals(i,0);
        double ny = _normals(i,1);
        double nz = _normals(i,2);
        // mirror velocity vector about the interface
        double u = _UV(i,0), v = _UV(i,1), wTilde = _w(i,0);
        if (haveWMesh)
          wTilde = _w(i,0) - _wMesh(i,0);
        double uvwN = u*nx+v*ny+wTilde*nz;
        externalSol(i,0) = u - 2*uvwN*nx;
        externalSol(i,1) = v - 2*uvwN*ny;
        externalW(i,0)   = wTilde - 2*uvwN*nz;       
        double uvIntN = _UVInt(i,0)*nx + _UVInt(i,1)*ny;
        externalUVInt(i,0) = _UVInt(i,0) - 2*nx*uvIntN;
        externalUVInt(i,1) = _UVInt(i,1) - 2*ny*uvIntN;
        externalEta(i,0) = _eta(i,0);
        externalRhoSurf(i,0) = _rhoSurf(i,0);

        if (haveWMesh)
          externalWMesh(i,0) = _wMesh(i,0);
      }
      // ipterm should not (?) be set to zero
      // diffusive should instead (?)
//       if (haveDiff)
//         externalIPTerm.scale(0);
      replaceFunction.compute();
      for(int i=0;i<val.size1(); i++) {
        for(int j=0;j<val.size2(); j++) {
          val(i,j) = interfaceTerm(i,j);
          // additional source term for top/bottom
          if (haveZBotDist) {
            double nz = _normals(i,2);
            double uvBot = pow(_zBotDist(i,0)*_UVGrad(i,j+4) + _UV(i,j),1);
            val(i,j) += _extDiffFlux(i,0) * uvBot * nz; // nz < 0 on bottom
          }
          else if (haveDiff) {
            double nz = _normals(i,2);
            val(i,j) += _extDiffFlux(i,j)/rho0 * nz; // nz < 0 on bottom
          }
        }
      }
    }
  };
  term _term;
  public:
  boundaryWall(const function* _eta, const function* _rhoSurf, const function* _uvInt, const function* _w, const function* _wM, const function* _ip, const function* _extDiff, const function* _zBotDist, dgConservationLawFunction *claw): _term(claw, _eta, _rhoSurf, _uvInt, _w, _wM, _ip, _extDiff, _zBotDist) {
    _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawSW3dMomentum::newBoundaryWall(const function* extDiffFlux, const function *zBotDist){
  this->checkSetup();
  if (!zBotDist){
    if (!extDiffFlux)
      return new boundaryWall(_eta, _rhoSurf, _uv2d, _w, _wM, _ipTerm, _fzerov, NULL, this);
    return new boundaryWall(_eta, _rhoSurf, _uv2d, _w, _wM, _ipTerm, extDiffFlux, NULL, this);
  }
  return new boundaryWall(_eta, _rhoSurf, _uv2d, _w, _wM, _ipTerm, extDiffFlux, zBotDist, this);
}

class dgConservationLawSW3dMomentum::boundaryWallLinear : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> _normals, _UV, _UVgrad, _eta, _rhoSurf, _nuH,_xyz;
    double _uvPenalty,_R;
    bool _haveDiff;
    public:
    term(dgConservationLawFunction *claw, const function *eta, const function *rhoSurf, const function* nuH, double uvPenaltyFactor, double R):function(claw->getNbFields()) {
      setArgument(_UV,function::getSolution());
      setArgument(_UVgrad,function::getSolutionGradient());
      setArgument(_normals,function::getNormals());
      setArgument(_eta,eta);
      setArgument(_rhoSurf,rhoSurf);
      setArgument(_xyz, function::getCoordinates());
      _R=R;
      _uvPenalty = uvPenaltyFactor;
      _haveDiff = nuH;
      if (_haveDiff)
        setArgument(_nuH,nuH);
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      double g = slim3dParameters::g;
      double rho0 = slim3dParameters::rho0;
      // bnd condition for linear momentum
      for(int i=0;i<val.size1(); i++) {
        double J = 1;
        if (_R>0)
          J = 4*_R*_R/(4*_R*_R+_xyz(i,0)*_xyz(i,0)+_xyz(i,1)*_xyz(i,1));
        double nx = _normals(i,0);
        double ny = _normals(i,1);
        double nz = _normals(i,2);
        double eta = _eta(i,0)/J;
        double rhoSurf = _rhoSurf(i,0)/J;
        double un =  _UV(i,0)*nx/J + _UV(i,1)*ny/J;
        if ( fabs(nz) < 1e-8 ) { // only for lateral faces!
          val(i,0) = -g*(1+rhoSurf/rho0)*eta*nx;
          val(i,1) = -g*(1+rhoSurf/rho0)*eta*ny;
          val(i,0) += -_uvPenalty*un*nx;
          val(i,1) += -_uvPenalty*un*ny;
          if (_haveDiff) {
            double dudn = _UVgrad(i,0)*nx + _UVgrad(i,1)*ny;// + _UVgrad(i,2)*nz;
            double dvdn = _UVgrad(i,3)*nx + _UVgrad(i,4)*ny;// + _UVgrad(i,5)*nz;
            double nGradUVn = dudn*nx + dvdn*ny;
            val(i,0) += -_nuH(i,0)*nGradUVn*nx;// - jumpPenaltyX;
            val(i,1) += -_nuH(i,0)*nGradUVn*ny;// - jumpPenaltyY; // only very small effect
          }
        }
      }
    }
  };
  term _term;
  public:
  boundaryWallLinear(const function* _eta, const function* _r, const function* _nuH, double _uvPenalty, double _R, dgConservationLawFunction *claw): _term(claw, _eta, _r, _nuH, _uvPenalty,_R) {
    _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawSW3dMomentum::newBoundaryWallLinear(double uvPenaltyFactor){
  this->checkSetup();
  return new boundaryWallLinear(_eta, _r, _nuH, uvPenaltyFactor,_R, this);
}

class dgConservationLawSW3dMomentum::boundarySurface : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> _normals, _UV, _UVgrad, _eta, _wind, _nuH, _nuV;
    bool _haveDiffusion;
    public:
    term(dgConservationLawFunction *claw, const function* eta, const function* wind, const function* nuH, const function* nuV):function(claw->getNbFields()) {
      setArgument(_UV,function::getSolution());
      setArgument(_UVgrad,function::getSolutionGradient());
      setArgument(_eta,eta);
      setArgument(_normals,function::getNormals());
      setArgument(_wind,wind);
      _haveDiffusion = nuV;
      if (_haveDiffusion) {
        setArgument(_nuV,nuV);
        setArgument(_nuH,nuH);
      }
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      double g = slim3dParameters::g;
      double rho0 = slim3dParameters::rho0;

      // bnd condition for linear momentum
      for(int i=0;i<val.size1(); i++) {
          double nx = _normals(i,0);
          double ny = _normals(i,1);
          double nz = _normals(i,2);
          double eta = _eta(i,0);
          val(i,0) = -g*eta*nx;
          val(i,1) = -g*eta*ny;
          if (_haveDiffusion) {
            val(i,0) += _wind(i,0)/rho0*nz;
            val(i,1) += _wind(i,1)/rho0*nz;
          }
          if (_haveDiffusion) {
            double dudn = _UVgrad(i,0)*nx + _UVgrad(i,1)*ny;// + _UVgrad(i,2)*nz;
            double dvdn = _UVgrad(i,3)*nx + _UVgrad(i,4)*ny;// + _UVgrad(i,5)*nz;
            double nGradUVn = dudn*nx + dvdn*ny;
            double dudzn = _UVgrad(i,2)*nx + _UVgrad(i,5)*ny;
            val(i,0) += -_nuH(i,0)*nGradUVn*nx - _nuV(i,0)*dudzn*nx*nz;// - jumpPenaltyX;
            val(i,1) += -_nuH(i,0)*nGradUVn*ny - _nuV(i,0)*dudzn*ny*nz;// - jumpPenaltyY;
          }
      }
    }
  };
  term _term;
  public:
  boundarySurface(dgConservationLawFunction *claw, const function* eta, const function* wind, const function* nuH, const function* nuV):_term(claw,eta,wind,nuH,nuV) {
    _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawSW3dMomentum::newBoundarySurface(const function* wind){
  return new boundarySurface(this,_eta,wind,_nuH,_nuV);
}

class dgConservationLawSW3dMomentum::boundaryOpenFree : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> _normals, _UV, _UVGrad, _UVInt, _eta, _rhoSurf, _bath, _w, _wMesh,_extDiffFlux;
    fullMatrix <double> externalSol, externalSolGrad, externalUVInt, interfaceTerm, externalEta, externalRhoSurf, externalBath, externalW, externalWMesh, externalIPTerm, externalDiffFlux;
    functionReplace replaceFunction;
    bool haveWMesh, haveDiff;
    public:
    term(dgConservationLawFunction *claw, const function *eta, const function *rhoSurf, const function *bath, const function *uvInt, const function *w, const function *wM, const function *ipTerm, const function *extDiffFlux):function(claw->getNbFields()) {
      replaceFunction.addChild();
      addFunctionReplace (replaceFunction);
      replaceFunction.replace (externalSol, function::getSolution(), 1);
      replaceFunction.replace (externalUVInt, uvInt, 1);
      replaceFunction.replace (externalEta, eta, 1);
      replaceFunction.replace (externalRhoSurf, rhoSurf, 1);
      replaceFunction.replace (externalBath, bath, 1);
      replaceFunction.replace (externalW, w, 1);
      replaceFunction.get(interfaceTerm,claw->getInterfaceTerm0(""));
      setArgument(_normals,function::getNormals());
      setArgument(_eta,eta);
      setArgument(_rhoSurf,rhoSurf);
      setArgument(_bath,bath);
      setArgument(_w,w);
      setArgument(_extDiffFlux,extDiffFlux);
      haveWMesh = wM;
      if (haveWMesh) {
        setArgument(_wMesh,wM);
        replaceFunction.replace (externalWMesh, wM, 1);
      }
      haveDiff = ipTerm;
//       if (ipTerm)
//         replaceFunction.replace (externalIPTerm, ipTerm, 0);
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      double rho0 = slim3dParameters::rho0;
      double g = slim3dParameters::g;
      // bnd condition for non-linear momentum (more unstable)
      for(int i=0;i<val.size1(); i++) {
          double nx = _normals(i,0);
          double ny = _normals(i,1);
          double H = _bath(i,0) + _eta(i,0);
          externalSol(i,0) = nx * sqrt(g/H) * _eta(i,0);
          externalSol(i,1) = ny * sqrt(g/H) * _eta(i,0);
          externalW(i,0)   = _w(i,0);       
          externalUVInt(i,0) = nx * sqrt(g/H) * _eta(i,0);
          externalUVInt(i,1) = ny * sqrt(g/H) * _eta(i,0);
          externalEta(i,0) = _eta(i,0);
          externalRhoSurf(i,0) = _rhoSurf(i,0);
          externalBath(i,0) = _bath(i,0);
          if (haveWMesh)
            externalWMesh(i,0) = _wMesh(i,0);
      }
      // ipterm should not (?) be set to zero
      // diffusive should instead (?)
//       if (haveDiff)
//         externalIPTerm.scale(0);
      replaceFunction.compute();
      for(int i=0;i<val.size1(); i++) {
        for(int j=0;j<val.size2(); j++) {
          val(i,j) = interfaceTerm(i,j);
          // additional source term for top/bottom
          if (haveDiff) {
            double nz = _normals(i,2);
            val(i,j) += _extDiffFlux(i,j)/rho0 * nz; // nz < 0 on bottom
          }
        }
      }
    }
  };
  term _term;
  public:
  boundaryOpenFree(const function* _eta, const function* _rhoSurf, const function* _bath, const function* _uvInt, const function* _w, const function* _wM, const function* _ip, const function* _extDiff, dgConservationLawFunction *claw): _term(claw, _eta, _rhoSurf, _bath, _uvInt, _w, _wM, _ip, _extDiff) {
    _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawSW3dMomentum::newBoundaryOpenFree(const function *extDiffFlux){
  this->checkSetup();
  if (!extDiffFlux)
    return new boundaryOpenFree(_eta, _rhoSurf, _bathymetry, _uv2d, _w, _wM, _ipTerm, _fzerov, this);
  else
    return new boundaryOpenFree(_eta, _rhoSurf, _bathymetry, _uv2d, _w, _wM, _ipTerm, extDiffFlux, this);
}


// ********* 3D horizontal momentum for U or V, vertical diffusion only

class dgConservationLawSW3dMomWx::interfaceTerm : public function {
  fullMatrix<double> _ip;
  public:
  interfaceTerm(const function * ipTerm):function(2) {
    setArgument(_ip,ipTerm);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for(int i=0; i< _ip.size1(); i++) {
      val(i,0) = _ip(i,0);
      val(i,1) = _ip(i,1);
    }
  }
};

dgFaceTermAssembler *dgConservationLawSW3dMomWx::getFaceTerm(const dgGroupOfFaces &group) const
{
  if(group.orientationTag() == dgGroupOfFaces::ORIENTATION_HORIZONTAL)
    return dgConservationLawFunction::getFaceTerm(group);
  return NULL;
}

class dgConservationLawSW3dMomWx::diffusiveFlux : public function {
  fullMatrix<double> _Ugrad,_nuV;
  public:
  diffusiveFlux(const function * nuVFunction):function(3) {
    setArgument(_Ugrad,function::getSolutionGradient());
    setArgument(_nuV,nuVFunction);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for(int i=0; i< _Ugrad.size1(); i++) {
      val(i,0) = 0; //dudx
      val(i,1) = 0; //dudy
      val(i,2) = -_Ugrad(i,2)*_nuV(i,0); //dudz
    }
  }
};

dgConservationLawSW3dMomWx::dgConservationLawSW3dMomWx() : dgConservationLawFunction(1){
   // u
  _fzero = new functionConstant(0.);
  _nuV = NULL;
  _diffusivity[""] = NULL;
  _diffusiveFlux = NULL;
  _ipTerm = NULL;
  _comp = 0;
}

void dgConservationLawSW3dMomWx::setup() {
  bool haveDiffusion = _nuV;
  _diffusivity[""] = new dgConservationLawSW3dMomentum::diffusivityDotN(_nuV , _fzero );
  _diffusiveFlux = haveDiffusion ? new diffusiveFlux (_nuV) : NULL;
  _ipTerm = haveDiffusion ? dgNewIpTerm (_nbf, _diffusiveFlux, _diffusivity[""]) : NULL;
  _volumeTerm0[""] = NULL;
  _volumeTerm1[""] = _diffusiveFlux;
  _interfaceTerm0[""] = _ipTerm;
  _maximumConvectiveSpeed[""] = NULL;
}

dgConservationLawSW3dMomWx::~dgConservationLawSW3dMomWx() {
  delete _fzero;
  if (_ipTerm) delete _ipTerm;
  if (_diffusiveFlux) delete _diffusiveFlux;
  if (_volumeTerm0[""]) delete _volumeTerm0[""];
//   if (_interfaceTerm0[""]) delete _interfaceTerm0[""];
  if (_maximumConvectiveSpeed[""]) delete _maximumConvectiveSpeed[""];
  if (_diffusivity[""]) delete _diffusivity[""];
}

class dgConservationLawSW3dMomWx::boundaryBottom : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> _normals, _UV, _UVGrad, _eta, _zBotDist;
    fullMatrix <double> _extUV, _interfaceTerm, _extEta, _inNuV, _extNuV, _extDiffFlux, _extUVGrad, _diffFluxExt, _diffFluxIn;
    functionReplace replaceFunction;
    bool haveDiffusion;
    public:
    term(dgConservationLawFunction *claw, const function* nuIn, const function* extDiffFlux, const function* diffFlux, const function* zBotDist):function(claw->getNbFields()) {
      replaceFunction.addChild();
      addFunctionReplace (replaceFunction);
      replaceFunction.replace (_extUV, function::getSolution(), 1);
      replaceFunction.get(_interfaceTerm,claw->getInterfaceTerm0(""));
      setArgument(_UV,function::getSolution());
      setArgument(_UVGrad,function::getSolutionGradient());
      setArgument(_zBotDist,zBotDist);
      setArgument(_normals,function::getNormals());
      haveDiffusion = nuIn;
      if (haveDiffusion) {
        setArgument(_extDiffFlux,extDiffFlux);
//         setArgument(_diffFluxIn,diffFlux,0);
//         replaceFunction.replace (_diffFluxExt, diffFlux, 1);
//         replaceFunction.replace (_diffFluxIn, diffFlux, 0);
//         setArgument(_diffFluxExt,diffFlux,1);
//         setArgument(_inNuV,nuIn);
//         replaceFunction.replace (_extNuV, nuIn, 1);
//         replaceFunction.replace (_extUVGrad, function::getSolutionGradient(), 1);
      }
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      // bnd condition for non-linear momentum (more unstable)
//       _diffFluxIn.setAll(0.0);
//       _diffFluxExt.setAll(0.0);
      for(int i=0;i<val.size1(); i++) {
          // this should be according to the kinematic condition u.n = 0
          _extUV(i,0) = 0;//-_UV(i,0);
          if (haveDiffusion) {
            // force nuV*UVGrad(i,2) = extDiffFlux
//             _extNuV(i,0) = 0;//_inNuV(i,0);
//             _extUVGrad(i,0) = 0;
//             _extUVGrad(i,1) = 0;
//             _extUVGrad(i,2) = _extDiffFlux(i,0) * uvBot/_inNuV(i,0);
//             _diffFluxIn(i,0) = 0;
//             _diffFluxIn(i,1) = 0;
//             _diffFluxIn(i,2) = -_extDiffFlux(i,0) * uvBot;
//             _diffFluxExt(i,0) = 0;
//             _diffFluxExt(i,1) = 0;
//             _diffFluxExt(i,2) = -_extDiffFlux(i,0) * uvBot;
          }
      }
      replaceFunction.compute();
      for(int i=0;i<val.size1(); i++) {
        for(int j=0;j<val.size2(); j++) {
          // computing the ip penalty term is useful when comparing to GOTM
          // but not (?) when comparing to analytical log profile
          //val(i,j) = _interfaceTerm(i,j); more stable without the ip term
          // additional source term for bottomFriction
          val(i, j) = 0.;
          if (haveDiffusion) {
            double nz = _normals(i,2);
            double uvBot = pow(_zBotDist(i,0)*_UVGrad(i,2) + _UV(i,0),1);
            val(i,j) += _extDiffFlux(i,0) * uvBot * nz; // nz < 0 on bottom
          }
        }
      }
    }
  };
  term _term;
  public:
  boundaryBottom(dgConservationLawFunction *claw, const function* nuIn, const function* extDiffFlux, const function* diffFlux, const function* zBotDist): _term(claw, nuIn,extDiffFlux,diffFlux,zBotDist) {
    _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawSW3dMomWx::newBoundaryBottom(const function* extDiffFlux,const function* zBotDist){
  this->checkSetup();
  if (!extDiffFlux)
    return new boundaryBottom(this, _nuV, _fzero, _diffusiveFlux, zBotDist);
  return new boundaryBottom(this, _nuV, extDiffFlux, _diffusiveFlux,zBotDist);
}

class dgConservationLawSW3dMomWx::boundarySurface : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> normals, _UV, _wind, _nuV;
    int *_comp;
    bool _haveDiffusion;
    public:
    term(dgConservationLawFunction *claw, const function* wind, const function* nuV, int *comp):function(claw->getNbFields()) {
      setArgument(_UV,function::getSolution());
      setArgument(normals,function::getNormals());
      setArgument(_wind,wind);
      _haveDiffusion = nuV;
      if (_haveDiffusion)
        setArgument(_nuV,nuV);
      _comp = comp;
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      double rho0 = slim3dParameters::rho0;
      // bnd condition for linear momentum
      for(int i=0;i<val.size1(); i++) {
          double nz = normals(i,2);
          if (_haveDiffusion) {
            val(i,0) = _wind(i,*_comp)/rho0*nz;
          }
      }
    }
  };
  term _term;
  public:
  boundarySurface(dgConservationLawFunction *claw, const function* wind, const function* nuV, int *comp):_term(claw,wind,nuV,comp) {
    _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawSW3dMomWx::newBoundarySurface(const function* wind){
  return new boundarySurface(this,wind,_nuV, &_comp);
}

// external mode -- shallow water equations

class dgConservationLawSW2dEta::maxConvectiveSpeed: public function {
  fullMatrix<double> _bath, _eta,_UV;
  public:
  maxConvectiveSpeed (const function *bathymetry,const function *UV):function(1){
    setArgument(_bath,bathymetry);
    setArgument(_eta,function::getSolution());
    setArgument(_UV,UV);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    double g = slim3dParameters::g;
    for(int i=0; i< val.size1(); i++)
      val(i,0) = sqrt((_bath(i,0)+_eta(i,0))*g + hypot(_UV(i,0),_UV(i,1)));
  }
};

class dgConservationLawSW2dEta::gradPsiTerm: public function {
  fullMatrix<double> bath, _eta, _UV,_xyz;
  bool _linear, _depthIntegrated;
  double _R;
  public:
  gradPsiTerm (const function *bathymetry, const function *UV2d, bool linear, bool depthIntegrated, double R):function(3){
    setArgument(bath,bathymetry);
    setArgument(_eta,function::getSolution());
    setArgument(_UV,UV2d);
    _linear = linear;
    _depthIntegrated = depthIntegrated;
    _R=R;
    setArgument(_xyz,function::getCoordinates());
    
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++) {
      double J = 1;
      if (_R>0)
        J = 4*_R*_R/(4*_R*_R+_xyz(i,0)*_xyz(i,0)+_xyz(i,1)*_xyz(i,1));
      double h = bath(i,0);
      double eta = _eta(i,0);
      double u = _UV(i,0);
      double v = _UV(i,1);
      if (!_linear)
        h+=eta;
      if (_depthIntegrated)
        h = 1.0;
      // flux_x
      val(i,0) = h*u/J;
      // flux_y
      val(i,1) = h*v/J;
      // flux_z
      val(i,2) = 0;
    }
  }
};

class dgConservationLawSW2dEta::source: public function {
  fullMatrix<double> qw,_xyz,_UV;
  double _R;
  public :
  source(const function *freshwaterFlux, const function *UV2d, double R) : function (1){
    setArgument(qw,freshwaterFlux);
    _R=R;
    if (_R>0)
      setArgument(_UV,UV2d);
    setArgument(_xyz,function::getCoordinates());
    
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP =val.size1();
    for (size_t i=0; i<nQP; i++) {
      val (i,0) = qw(i,0);
      if (_R>0)
        val(i,0) += (-_xyz(i,0)*_UV(i,0)+_xyz(i,1)*_UV(i,1))/(_R*_R)*(_R>0);
    }
  }
};

class dgConservationLawSW2dEta::riemann:public function {
  fullMatrix<double> _etaL, _etaR, _UVL,_UVR, bath, bathR, _normals, ip,_xyz;
  bool _linear, _depthIntegrated;
  double _R;
  public:
  riemann (const function *bathymetry, const function *UV2d, bool linear, bool depthIntegrated, double R): function(1){
    setArgument(_etaL,function::getSolution(), 0);
    setArgument(_etaR,function::getSolution(), 1);
    setArgument(_UVL,UV2d, 0);
    setArgument(_UVR,UV2d, 1);
    setArgument(bath,bathymetry, 0);
    setArgument(bathR,bathymetry, 1);
    setArgument(_normals,function::getNormals(), 0);
    _linear = linear;
    _depthIntegrated = depthIntegrated;
    _R=R;
    setArgument(_xyz,function::getCoordinates());
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    double g = slim3dParameters::g;
    size_t nQP = _etaL.size1();
    for(size_t i=0; i< nQP; i++) {
      double J = 1;
      if (_R>0)
        J = 4*_R*_R/(4*_R*_R+_xyz(i,0)*_xyz(i,0)+_xyz(i,1)*_xyz(i,1));
      double nx = _normals(i,0);
      double ny = _normals(i,1);
      double uL =  nx*_UVL(i,0) + ny*_UVL(i,1), uR =  nx*_UVR(i,0) + ny*_UVR(i,1);
      double vL = -ny*_UVL(i,0) + nx*_UVL(i,1), vR = -ny*_UVR(i,0) + nx*_UVR(i,1);
      double h = bath(i,0);
      double eta,u,v=0,Hu;
      if (_depthIntegrated) {
        double etaM = 0.5*(_etaL(i,0)+_etaR(i,0));
        Hu = (uL+uR + (_etaL(i,0)-_etaR(i,0))*sqrt(g/(h+etaM)))/2;
      } else {
        if (_linear) {
          linearRoe(_etaL(i,0), _etaR(i,0), uL, uR, h, eta, u);
          Hu = h*u;
        }else{
          approxRoe(_etaL(i,0), _etaR(i,0), uL, uR, vL, vR, h, eta, u, v);
          Hu = (h+eta)*u;
        }
      }
      val(i,0) = -Hu/J;
    }
  }
};

class dgConservationLawSW2dEta::boundaryWall : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> _normals, sol;
    public:
    term():function(1){
      setArgument(sol,function::getSolution());
      setArgument(_normals,function::getNormals());
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      size_t nQP = sol.size1();
      for(size_t i=0; i< nQP; i++) {
        val(i,0) = 0;
      }
    }
  };
  term _term;
  public:
  boundaryWall(dgConservationLawFunction *claw) {
    _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawSW2dEta::newBoundaryWall(){
  return new boundaryWall(this);
}

class dgConservationLawSW2dEta::boundaryOpenFree : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> _normals, _eta, _UV, _bath, interfaceTerm;
    fullMatrix<double> externalUV, externalEta, externalBath;
    functionReplace replaceFunction;
    public:
    term(dgConservationLawFunction *claw, const function *uv, const function *bath):function(claw->getNbFields()){
      replaceFunction.addChild();
      addFunctionReplace(replaceFunction);
      replaceFunction.replace(externalUV, uv, 1);
      replaceFunction.replace(externalEta, function::getSolution(), 1);
      replaceFunction.replace(externalBath, bath, 1);
      replaceFunction.get(interfaceTerm, claw->getInterfaceTerm0(""));
      setArgument(_eta,function::getSolution());
      setArgument(_UV,uv);
      setArgument(_bath,bath);
      setArgument(_normals,function::getNormals());
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      double g = slim3dParameters::g;
      for(int i=0; i< val.size1(); i++) {
        double nx = _normals(i,0);
        double ny = _normals(i,1);
        double H = _bath(i,0) + _eta(i,0);
        externalUV(i,0) = 0*_UV(i,0) + nx * sqrt(g/H) * _eta(i,0);
        externalUV(i,1) = 0*_UV(i,1) + ny * sqrt(g/H) * _eta(i,0);
        externalEta(i,0) = _eta(i,0);
        //if ( fabs(_eta(i,0)) + fabs( _UV(i,0) - nx * sqrt(g*H) * _eta(i,0)) + fabs(_UV(i,1) - ny * sqrt(g*H) * _eta(i,0)) > 1e-12 )
          //printf("eta : %g, ue : %g %g\n", _eta(i,0), _UV(i,0) - nx * sqrt(g*H) * _eta(i,0),  _UV(i,1) - ny * sqrt(g*H) * _eta(i,0));
        externalBath(i,0) = _bath(i,0);
      }
      replaceFunction.compute();
      for(int i=0; i< val.size1(); i++) 
        for(int j=0; j < val.size2(); j++)
          val(i,j) = interfaceTerm(i,j);
    }
  };
  term _term;
  public:
  boundaryOpenFree(dgConservationLawFunction *claw, const function *uv, const function *bath) : _term(claw, uv, bath){
    _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawSW2dEta::newBoundaryOpenFree(){
  return new boundaryOpenFree(this, _UV, _bathymetry);
}

void dgConservationLawSW2dEta::setup() {
  _volumeTerm0[""] = _freshwaterFlux ? new source(_freshwaterFlux,_UV,_R) : NULL;
  _volumeTerm1[""] = new gradPsiTerm(_bathymetry,_UV, _linear,_depthIntegrated,_R);
  _interfaceTerm0[""] = new riemann(_bathymetry, _UV, _linear,_depthIntegrated,_R);
  _maximumConvectiveSpeed[""] = new maxConvectiveSpeed(_bathymetry,_UV);
}

dgConservationLawSW2dEta::dgConservationLawSW2dEta():dgConservationLawFunction(1) {
  // eta
  _fzero = new functionConstant(0.);
  _fzerov = new functionConstant(std::vector<double>(2,0));
  _bathymetry = _freshwaterFlux = NULL;
  _UV = _fzerov;
  _linear = false;
  _depthIntegrated = false;
  _R=-1.0;
}

dgConservationLawSW2dEta::~dgConservationLawSW2dEta() {
  delete _fzero;
  delete _fzerov;
  if (_volumeTerm0[""]) delete _volumeTerm0[""];
  if (_volumeTerm1[""]) delete _volumeTerm1[""];
  if (_interfaceTerm0[""]) delete _interfaceTerm0[""];
  if (_maximumConvectiveSpeed[""]) delete _maximumConvectiveSpeed[""];
}

class dgConservationLawSW2dU::maxConvectiveSpeed: public function {
  fullMatrix<double> bath, _eta, _UV;
  bool _linear;
  public:
  maxConvectiveSpeed (const function *bathymetry, const function *eta, bool linear):function(1){
    setArgument(bath,bathymetry);
    setArgument(_eta,eta);
    setArgument(_UV,function::getSolution());
    _linear = linear;
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    double g = slim3dParameters::g;
    if (_linear)
      for(int i=0; i< val.size1(); i++)
        val(i,0) = sqrt(bath(i,0)*g);
    else
      for(int i=0; i< val.size1(); i++)
        val(i,0) = sqrt((bath(i,0)+_eta(i,0))*g)+hypot(_UV(i,0),_UV(i,1));
  }
};

class dgConservationLawSW2dU::gradPsiTerm: public function {
  fullMatrix<double> _bath, _UV, _eta, _rhoSurf, _diff,_xyz;
  bool _linear, _isDiffusive;
  double _R;
  public:
  gradPsiTerm (const function *bathymetry, const function *eta, const function *rhoSurf, const function *diffusiveFlux, bool linear, double R):function(6){
    setArgument(_bath,bathymetry);
    setArgument(_eta,eta);
    setArgument(_rhoSurf,rhoSurf);
    setArgument(_UV,function::getSolution());
    _isDiffusive = diffusiveFlux;
    if (_isDiffusive) {
      setArgument(_diff,diffusiveFlux);
    }
    _linear = linear;
    _R=R;
    setArgument(_xyz,function::getCoordinates());
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    double g = slim3dParameters::g;
    double rho0 = slim3dParameters::rho0;
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++) {
      double J = 1;
      if (_R>0)
        J = 4*_R*_R/(4*_R*_R+_xyz(i,0)*_xyz(i,0)+_xyz(i,1)*_xyz(i,1));
      double h = _bath(i,0);
      double eta = _eta(i,0);
      double rhoSurf = _rhoSurf(i,0);
      double u = _UV(i,0);
      double v = _UV(i,1);
      if (!_linear)
        h+=eta;
      // flux_x
      val(i,0) = g*(1+rhoSurf/rho0)*eta/J;
      val(i,1) = 0;
      // flux_y
      val(i,2) = 0;
      val(i,3) = g*(1+rhoSurf/rho0)*eta/J;
      if (!_linear) {
        val(i,0) += u*u/J;
        val(i,1) += u*v/J;
        val(i,2) += v*u/J;
        val(i,3) += v*v/J;
      }
      if (_isDiffusive) {
        val(i,0) += _diff(i,0);
        val(i,1) += _diff(i,1);
        val(i,2) += _diff(i,2);
        val(i,3) += _diff(i,3);
      }
      // flux_z
      val(i,4) = 0;
      val(i,5) = 0;
    }
  }
};

class dgConservationLawSW2dU::source: public function {
  fullMatrix<double> _xyz, _eta, _rGradInt, _rhoSurf, _bath, _bathGrad, _UV, _UVGradient, _coriolisFactor, _source, _windStress, _nuH, _bfCoeff, _uvBot;
  bool _linear, _isDiffusive;
  double _R;
  public :
  source(const function *bath, const function *bathGradient, const function *eta, const function *rGradInt, const function *rhoSurf, const function *nuH, const function *coriolisFactor, const function *windStress, const function *source, const function *bfCoeff, const function *uvBot, bool linear, double R) :
    function (2){
    setArgument(_UV,function::getSolution());
    setArgument(_UVGradient,function::getSolutionGradient());
    setArgument(_coriolisFactor,coriolisFactor);
    setArgument(_windStress,windStress);
    setArgument(_source,source);
    setArgument(_eta,eta);
    setArgument(_rGradInt, rGradInt);
    setArgument(_rhoSurf, rhoSurf);
    setArgument(_bath,bath);
    setArgument(_bathGrad,bathGradient);
    setArgument(_bfCoeff,bfCoeff);
    setArgument(_uvBot,uvBot);
    _linear = linear;
    _isDiffusive = nuH;
    if ( _isDiffusive )
      setArgument(_nuH,nuH);
    setArgument(_xyz,function::getCoordinates());
    _R=R;
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    double g = slim3dParameters::g;
    double rho0 = slim3dParameters::rho0;
    size_t nQP =val.size1();
    for (size_t i=0; i<nQP; i++) {
      double J = 1;
      if (_R>0)
        J = 4*_R*_R/(4*_R*_R+_xyz(i,0)*_xyz(i,0)+_xyz(i,1)*_xyz(i,1));
      double u = _UV(i,0);
      double v = _UV(i,1);
      double dudx = _UVGradient(i,0);
      double dudy = _UVGradient(i,1);
      double dvdx = _UVGradient(i,3);
      double dvdy = _UVGradient(i,4);
      double H = _eta(i,0) + _bath(i,0);
      val (i,0) =  _coriolisFactor(i,0)*v + _windStress(i,0)/(H*rho0) + _source(i,0);
      val (i,1) = -_coriolisFactor(i,0)*u + _windStress(i,1)/(H*rho0) + _source(i,1);
      
      val(i,0) += -g * _rGradInt(i,0) / H;
      val(i,1) += -g * _rGradInt(i,1) / H;

      if (_R>0){
        val(i,0) += -_xyz(i,0)*(1+_rhoSurf(i,0)/rho0)*_eta(i,0)/(2*_R*_R);
        val(i,1) += -_xyz(i,1)*(1+_rhoSurf(i,0)/rho0)*_eta(i,0)/(2*_R*_R);
      }
      if(_isDiffusive){
        val(i,0) += _nuH(i, 0)/_bath(i, 0)*(_bathGrad(i, 0)*dudx+_bathGrad(i, 1)*dudy);
        val(i,1) += _nuH(i, 0)/_bath(i, 0)*(_bathGrad(i, 0)*dvdx+_bathGrad(i, 1)*dvdy);
    }
      if(!_linear) {
        double div = dudx+dvdy;
        val (i,0) += u*div/J;
        val (i,1) += v*div/J;
        if (_R>0){
          val(i,0)+= (2*_xyz(i,0)*u*u+2*_xyz(i,1)*u*v-_xyz(i,0)*(u*u+v*v))/(2*_R*_R);
          val(i,1)+= (2*_xyz(i,0)*u*v+2*_xyz(i,1)*v*v-_xyz(i,1)*(u*u+v*v))/(2*_R*_R);
        }
        // new bottom friction splitting
        double bfU = _uvBot(i,0) + u;
        double bfV = _uvBot(i,1) + v;
        double bfNorm = hypot(bfU,bfV);
        val (i,0) +=  -_bfCoeff(i,0)/H*bfNorm*bfU;
        val (i,1) +=  -_bfCoeff(i,0)/H*bfNorm*bfV;
      }
    }
  }
};

class dgConservationLawSW2dU::diffusiveFlux:public function {
  fullMatrix<double> _UVGradient,nuH;
  public:
  diffusiveFlux (const function *nuF): function(6){
    setArgument(_UVGradient,function::getSolutionGradient());
    setArgument(nuH,nuF);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++) {
      val(i,0) = -_UVGradient(i,0)*nuH(i,0); //dudx
      val(i,1) = -_UVGradient(i,3)*nuH(i,0); //dvdx
      val(i,2) = -_UVGradient(i,1)*nuH(i,0); //dudy
      val(i,3) = -_UVGradient(i,4)*nuH(i,0); //dvdy
      val(i,4) = 0; //dudz
      val(i,5) = 0; //dvdz
    }
  }
};

class dgConservationLawSW2dU::riemann:public function {
  fullMatrix<double> _xyz,_UVL, _UVR, _etaL, _etaR, _rhoSurfL, _rhoSurfR, bath, _normals, ip;
  double _R;
  bool _linear, _isDiffusive;
  public:
  riemann (const function *bathymetry, const function *eta, const function *rhoSurf, const function *ipTerm, bool linear, double R): function(2){
    setArgument(_UVL,function::getSolution(), 0);
    setArgument(_UVR,function::getSolution(), 1);
    setArgument(_etaL,eta, 0);
    setArgument(_etaR,eta, 1);
    setArgument(_rhoSurfL, rhoSurf,0);
    setArgument(_rhoSurfR, rhoSurf,1);
    setArgument(bath,bathymetry, 0);
    setArgument(_normals,function::getNormals(), 0);
    _isDiffusive = ipTerm;
    if (_isDiffusive) {
      setArgument(ip, ipTerm);
    }
    _linear = linear;
    _R=R;
    setArgument(_xyz,function::getCoordinates());
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    double g = slim3dParameters::g;
    double rho0 = slim3dParameters::rho0;
    size_t nQP = _UVL.size1();
    for(size_t i=0; i< nQP; i++) {
      double J = 1;
      if (_R>0)
        J = 4*_R*_R/(4*_R*_R+_xyz(i,0)*_xyz(i,0)+_xyz(i,1)*_xyz(i,1));
      double Fun, Fut;
      double nx = _normals(i,0);
      double ny = _normals(i,1);
      double uL = nx*_UVL(i,0) + ny*_UVL(i,1), uR = nx*_UVR(i,0) + ny*_UVR(i,1);
      double vL = -ny*_UVL(i,0) + nx*_UVL(i,1), vR = - ny*_UVR(i,0) + nx*_UVR(i,1);
      double h = bath(i,0);
      double eta,u,v=0;
      double rhoSurf = 0.5*(_rhoSurfL(i,0) + _rhoSurfR(i,0));
      if (_linear) {
        linearRoe(_etaL(i,0), _etaR(i,0), uL, uR, h, eta, u);
        Fun = -g*(1+rhoSurf/rho0)*eta;
        Fut = 0;
      }else{
        approxRoe(_etaL(i,0), _etaR(i,0), uL, uR, vL, vR, h, eta, u, v);
        Fun = -g*(1+rhoSurf/rho0)*eta - u*u;
        Fut = -u*v;
      }
      val(i,0) = Fun*nx/J - Fut*ny/J;
      val(i,1) = Fun*ny/J + Fut*nx/J;

      if (_isDiffusive) {
        val(i,0)+=ip(i,0);
        val(i,1)+=ip(i,1);
      }
    }
  }
};

/*class dgConservationLawSW2dU::boundaryWall : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> _normals, _UV, _eta, _bath, _rhoSurf, _rhoSurfExt,_xyz;
    double _R;
    bool haveRhoSurfExt;
    public:
    term(const function* eta, const function *rhoSurf, const function *bathymetry, double R, const function* rhoSurfExt):function(2){
      setArgument(_UV,function::getSolution());
      setArgument(_normals,function::getNormals());
      setArgument(_eta,eta);
      setArgument(_rhoSurf,rhoSurf);
      haveRhoSurfExt = rhoSurfExt;
      if (haveRhoSurfExt)
        setArgument(_rhoSurfExt,rhoSurfExt);
      setArgument(_bath,bathymetry);
      _R=R;
      setArgument(_xyz,function::getCoordinates());
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      double g = slim3dParameters::g;
      double rho0 = slim3dParameters::rho0;
      size_t nQP = _UV.size1();
      for(size_t i=0; i< nQP; i++) {
        double J = 1;
        if (_R>0)
          J = 4*_R*_R/(4*_R*_R+_xyz(i,0)*_xyz(i,0)+_xyz(i,1)*_xyz(i,1));
        double nx = _normals(i,0);
        double ny = _normals(i,1);
        double unJ = _UV(i,0)*nx + _UV(i,1) * ny;
        double H = _bath(i,0) + _eta(i,0);
        double eta = (_eta(i,0)  + sqrt(H/g) * unJ)/J;
        double rhoSurf = _rhoSurf(i,0)/J;
        if (haveRhoSurfExt)
          rhoSurf = 0.5*(_rhoSurf(i,0) + _rhoSurfExt(i,0))/J;
        val(i,0) = -g*(1+rhoSurf/rho0)*eta*nx;
        val(i,1) = -g*(1+rhoSurf/rho0)*eta*ny;
      }
    }
  };
  term _term;
  public:
  boundaryWall(dgConservationLawFunction *claw, const function* eta, const function *rhoSurf, const function *bathymetry, double R, const function *rhoSurfExt) : _term(eta,rhoSurf,bathymetry,R, rhoSurfExt) {
    _term0 = &_term;
  }
};*/


class dgConservationLawSW2dUWallExtValue : public function {
  fullMatrix<double> solIn, normals;
public:
  dgConservationLawSW2dUWallExtValue():function (2){
    setArgument (normals, function::getNormals());
    setArgument (solIn, function::getSolution(), 0);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    for (size_t i=0; i< nQP; i++) {
      const double nx = normals (i,0);
      const double ny = normals (i,1);
      double un = solIn (i,0) * nx + solIn (i,1) * ny;
      val (i,0) = solIn (i,0) - 2 * un * nx; // with utan different from 0
      val (i,1) = solIn (i,1) - 2 * un * ny;
      //val (i,0) = -un * nx; // with utan = 0
      //val (i,1) = -un * ny;
    }
  }
};

dgBoundaryCondition *dgConservationLawSW2dU::newBoundaryWall(const function* rhoSurfExt){
  function *boundaryWallUV = new dgConservationLawSW2dUWallExtValue();
  std::vector<const function*> *toReplace = new std::vector<const function*>();
  std::vector<const function*> *replaceBy = new std::vector<const function*>();
  toReplace->push_back(function::getSolution());
  replaceBy->push_back(boundaryWallUV);
  if (rhoSurfExt){
    toReplace->push_back(_rhoSurf);
    replaceBy->push_back(rhoSurfExt);
  }
  return newOutsideValueBoundaryGeneric("",*toReplace,*replaceBy);
}

class dgConservationLawSW2dU::boundaryOpenFree : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> _normals, _UV, _eta, _bath, _rhoSurf, _rhoSurfExt, interfaceTerm;
    fullMatrix<double> externalUV, externalEta, externalBath, externalRhoSurf;
    functionReplace replaceFunction;
    double _R;
    public:
    term(dgConservationLawFunction *claw, const function* eta, const function *rhoSurf, const function *bathymetry, double R):function(claw->getNbFields()){
      replaceFunction.addChild();
      addFunctionReplace(replaceFunction);
      replaceFunction.replace (externalUV, function::getSolution(), 1);
      replaceFunction.replace (externalEta, eta, 1);
      replaceFunction.replace (externalRhoSurf, rhoSurf, 1);
      replaceFunction.replace (externalBath, bathymetry, 1);
      replaceFunction.get (interfaceTerm,claw->getInterfaceTerm0(""));
      setArgument(_UV,function::getSolution());
      setArgument(_normals,function::getNormals());
      setArgument(_eta,eta);
      setArgument(_rhoSurf,rhoSurf);
      setArgument(_bath,bathymetry);
      _R=R;
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      double g = slim3dParameters::g;
      for(int i=0; i< val.size1(); i++) {
        double nx = _normals(i,0);
        double ny = _normals(i,1);
        double H = _bath(i,0) + _eta(i,0);
        externalUV(i,0) = nx * sqrt(g/H) * _eta(i,0);
        externalUV(i,1) = ny * sqrt(g/H) * _eta(i,0);
        externalEta(i,0) = _eta(i,0);
        externalRhoSurf(i,0) = _rhoSurf(i,0);
        externalBath(i,0) = _bath(i,0);
      }
      replaceFunction.compute();
      for(int i=0; i< val.size1(); i++) 
        for(int j=0; j < val.size2(); j++)
          val(i,j) = interfaceTerm(i,j);
    }
  };
  term _term;
  public:
  boundaryOpenFree(dgConservationLawFunction *claw, const function* eta, const function *rhoSurf, const function *bathymetry, double R) : _term(claw, eta,rhoSurf,bathymetry,R) {
    _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawSW2dU::newBoundaryOpenFree(){
  return new boundaryOpenFree(this,_eta, _rhoSurf, _bathymetry,_R);
}

void dgConservationLawSW2dU::setup() {
  if (!_bathymetry)
    Msg::Error("dgConservationLawSW2dU: Bathymetry function must be defined");
  _diffusiveFlux = _nuH ? new diffusiveFlux (_nuH) : NULL;
  if (_nuH) {
    std::vector<const function *> fArray;
    fArray.push_back(_nuH);
    fArray.push_back(_nuH);
    _diffusivity[""] = functionCatCompNew(fArray);
    _diffusiveFlux = new diffusiveFlux (_nuH);
    _ipTerm = dgNewIpTerm(_nbf, _diffusiveFlux, _diffusivity[""]);
  } else {
    _diffusivity[""] = NULL;
    _diffusiveFlux = NULL;
    _ipTerm = NULL;
  }
  _volumeTerm0[""] = new source(_bathymetry, _bathymetryGradient,_eta, _rGradInt, _rhoSurf,_nuH, _coriolisFactor, _windStress, _source, _bfCoeff, _uvBottom,_linear,_R);
  _volumeTerm1[""] = new gradPsiTerm(_bathymetry, _eta, _rhoSurf,_diffusiveFlux, _linear,_R);
  _interfaceTerm0[""] = new riemann(_bathymetry, _eta, _rhoSurf, _ipTerm, _linear,_R);
  _maximumConvectiveSpeed[""] = new maxConvectiveSpeed(_bathymetry,_eta,_linear);
}

dgConservationLawSW2dU::dgConservationLawSW2dU():dgConservationLawFunction(2) {
  // eta u v
  _fzero = new functionConstant(0.);
  _fzerov = new functionConstant(std::vector<double>(2,0));
  _bathymetry = NULL;
  _diffusiveFlux = NULL;
  _coriolisFactor = _fzero;
  _windStress = _fzerov;
  _bfCoeff = _fzero;
  _uvBottom = _fzerov;
  _bathymetryGradient = _fzerov;
  _source = _fzerov;
  _linear = false;
  _nuH = NULL;
  _eta = _fzero;
  _rGradInt = _fzerov;
  _rhoSurf = _fzero;
  _R=-1.0;
}

dgConservationLawSW2dU::~dgConservationLawSW2dU() {
  delete _fzero;
  delete _fzerov;
  if (_volumeTerm0[""]) delete _volumeTerm0[""];
  if (_volumeTerm1[""]) delete _volumeTerm1[""];
  if (_interfaceTerm0[""]) delete _interfaceTerm0[""];
  if (_maximumConvectiveSpeed[""]) delete _maximumConvectiveSpeed[""];
  if (_diffusiveFlux) delete _diffusiveFlux;
}
