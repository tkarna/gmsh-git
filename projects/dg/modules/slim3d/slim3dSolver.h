#ifndef SLIM_3D_SOLVER_H
#define SLIM_3D_SOLVER_H
#include "slim3dEquations.h"
#include "dgGroupOfElements.h"
#include "GModel.h"
// #include "dgDofContainer.h"
#include "slim3dUtils.h"
#include "dgLimiter.h"
#include "dgConservationLawSW3dContinuity.h"
#include "slim3dTimeIntegrator.h"

// class slim3dSolverEquations;
// class slim3dSolverFunctions;
class slim3dExporter;

/** A class to handle time integration of 3d shallow water equations
 * Most members are kept public in order to allow easy access in python */
class slim3dSolver {
private:
  bool _initialized;
  bool _solveS, _solveT, _solveSDiff, _solveTDiff, _solveUV, _solveUVDiff, _solveSed, _solveSedDiff;
  bool _solve2d, _solveW;
  bool _solveTurbulence, _advectTKE, _haveMovingMesh, _bottomFriction;
  bool _useUVLimiter, _useSLimiter, _useTLimiter, _useSedLimiter;
  bool _useModeSplit;
  bool _useStereo;
  bool _useConservativeALE;
  bool _useOptimalLimiter;
  bool _useGMVel;
  bool _useIsoDiff;
  bool _computeUVGrad;
  slim3dSolverEquations *_equations;
  slim3dSolverDofs *_dofs;
public:
  bool useNewRGrad;
  friend class slim3dSolverEquations;
  friend class slim3dSolverFunctions;
  friend class slim3dSolverDofs;
  std::vector<std::string> physicalTags1d, physicalTags2d, bottomTags;
  GModel *model3d;
  dgGroupCollection *groups3d, *groups2d;
  int order, dimension;
  dgExtrusion *columnInfo;
  slim3dSolverFunctions *functions;
  std::string exportDirectory;
  /** fortran namelist file for initializing GOTM */
  std::string turbulenceSetupFile;
  /** position where bottom friction velocity is computed *
   * In parametric coordinates (0 bottom, 1 top of bottom most element */
  double bottomFrictionXi;
  /** solvers */
  dgConservationLawSW3dContinuitySolver *wSolver, *rSolver;
  //dgConservationLawSW3dContinuitySolver *depthIntSolver, *rGradIntSolver; // OBSOLETE
  dgConservationLawSW3dContinuitySolver *newRGradSolver;
  /** operators */
  dgSW3dDofCopy *copy2d3d;
  slim3dDepthIntegrator *newDepthIntegrator;
  dgNodalVolume *nodalVolume3d;
  dgNodalVolume *nodalVolume2d;
  dgPrismDofConverter *prismConverter;
  dgFieldCopy *copyField3d;
  dgSW3dMovingMesh *movingMesh;
  dgLimiter *uvLimiter, *SLimiter, *TLimiter, *tkeLimiter, *epsLimiter, *sedLimiter;
  dgCGMeanFilter *dgCG3d, *dgCG2d;
//   dgCGStructure *dgCG3d, *dgCG2d;

  /** Create a solver instance and load 3d and 2d mesh */
  slim3dSolver(const std::string meshFile3d, std::vector<std::string> bottomBndTags = std::vector<std::string>(), std::vector<std::string> topBndTags = std::vector<std::string>());
  ~slim3dSolver();
  /** Creates all the dofs, functions and equations * 
   *  based on user defined options */
  slim3dSolverEquations* createEquations();
  /** Returns pointer to equations object, NULL if not yet created */
  slim3dSolverEquations* getEquations();
  /** Returns pointer to dofs object, NULL if not yet created */
  slim3dSolverDofs* getDofs(bool init = false) {
    if ( !_dofs->isInitialized() ) {
      if (init) {
        _dofs->initialize();
        return _dofs;
      }
      Msg::Error("Dofs not yet created!");
      return NULL;
    }
    return _dofs;
  };
  
  /** Set flag for computing bottom friction */
  void setComputeBottomFriction(bool f) { _bottomFriction = f; };
  bool getComputeBottomFriction() { return _bottomFriction; };
  /** Set flag for solving 2d equations (eta,uvAv) */
  void setSolve2d(bool f) { _solve2d = f; };
  bool getSolve2d() { return _solve2d; };
  /** Set flag for solving vertical velocity (w) */
  void setSolveW(bool f) { _solveW = f; };
  bool getSolveW() { return _solveW; };
  /** Set flag for solving salinity transport equation */
  void setSolveS(bool f) { _solveS = f; };
  bool getSolveS() { return _solveS; };
  /** Set flag for solving temperature transport equation */
  void setSolveT(bool f) { _solveT = f; };
  bool getSolveT() { return _solveT; };
  /** Set flag for solving sediment equation */
  void setSolveSed(bool f) { _solveSed = f; };
  bool getSolveSed() { return _solveSed; };
  /** Set flag for solving horizontal velocity equation */
  void setSolveUV(bool f) { _solveUV = f; };
  bool getSolveUV() { return _solveUV; };
  void setUseStereo(bool f){_useStereo = f;};
  double getUseStereo(){return _useStereo;};
  /** Set flag for solving implicit vertical diffusion of momentum */
  void setSolveUVImplicitVerticalDiffusion(bool f) { _solveUVDiff = f; };
  bool getSolveUVImplicitVerticalDiffusion() { return _solveUVDiff; };
  /** Set flag for solving implicit vertical diffusion of salinity */
  void setSolveSImplicitVerticalDiffusion(bool f) { _solveSDiff = f; };
  bool getSolveSImplicitVerticalDiffusion() { return _solveSDiff; };
  /** Set flag for solving implicit vertical diffusion of temperature */
  void setSolveTImplicitVerticalDiffusion(bool f) { _solveTDiff = f; };
  bool getSolveTImplicitVerticalDiffusion() { return _solveTDiff; };
  /** Set flag for solving implicit vertical diffusion of sediment */
  void setSolveSedImplicitVerticalDiffusion(bool f) { _solveSedDiff = f; };
  bool getSolveSedImplicitVerticalDiffusion() { return _solveSedDiff; };
  /** Set flag for solving turbulence closure model */
  void setSolveTurbulence(bool f) { _solveTurbulence = f; };
  bool getSolveTurbulence() { return _solveTurbulence; };
  /** Set flag for solving turbulent kinetic energy advection (tke and eps) */
  void setAdvectTurbulentEnergy(bool f) { _advectTKE = f; };
  bool getAdvectTurbulentEnergy() { return _advectTKE; };
  /** Set flag for updating the mesh geometry at each 3d time step */
  void setFlagMovingMesh(bool f) { _haveMovingMesh = f; };
  bool getFlagMovingMesh() { return _haveMovingMesh; };
  /** Set flag for uv slope limiter */
  void setFlagUVLimiter(bool f) { _useUVLimiter = f; };
  bool getFlagUVLimiter() { return _useUVLimiter; };
  void setFlagSLimiter(bool f) { _useSLimiter = f; };
  bool getFlagSLimiter() { return _useSLimiter; };
  void setFlagTLimiter(bool f) { _useTLimiter = f; };
  bool getFlagTLimiter() { return _useTLimiter; };
  void setFlagSedLimiter(bool f) { _useSedLimiter = f; };
  bool getFlagSedLimiter() { return _useSedLimiter; };
  /** Set flag for using mode splitting. *
   *  If true, the 2d momentum equation is solved, otherwise eta equation
   *  uses depth integrated 3d velocity. */
  void setUseModeSplit(bool f) { _useModeSplit = f; };
  bool getUseModeSplit() { return _useModeSplit; };
  /** Set flag for using mass conservative arbitrary Lagrangian Eulerian (ALE) formulation */
  void setUseConservativeALE(bool b) { _useConservativeALE = b; };
  bool getUseConservativeALE() { return _useConservativeALE; };
  /** Set flag for using optimal (QP) slope limiter */
  void setUseOptimalLimiter(bool b) { _useOptimalLimiter = b; };
  bool getUseOptimalLimiter() { return _useOptimalLimiter; };
  /** set compute Gent McWilliams velocity */
  void setSolveGMVel(bool f) {_useGMVel = f; };
  bool getSolveGMVel() { return _useGMVel; };
  void setSolveIsoDiff(bool f) {_useIsoDiff = f; };
  bool getSolveIsoDiff() { return _useIsoDiff; };
  void setSolveUVGrad(bool f) {_computeUVGrad = f;};
  bool getSolveUVGrad() {return _computeUVGrad;};
  const dgExtrusion &extrusion() const { return *columnInfo; }
};


#endif
