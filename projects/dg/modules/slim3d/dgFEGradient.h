#ifndef _DG_FE_GRADIENT_H
#define _DG_FE_GRADIENT_H
#include "dgConservationLawFunction.h"

/**
 * A simple equation for computing the gradients of a scalar field using the DG weak form.
 * The right hand side is the gradient of a scalar function rho integrated by parts.
 */
class dgFEGradient : public dgConservationLawFunction {
  class interface0;
  class gradPsiTerm;
  class source;
 // class massFactor;
  const function *_rho;
  double _R;

  public:
  void setup();
  dgFEGradient();
  dgFEGradient(const function *rho);
  inline void setIsSpherical(double R){_R=R;};
  inline void setFunction(const function *f) { _rho = f; };
  ~dgFEGradient();
};

/**
 * A simple equation for computing the gradients of a vector (2 components) field using the DG weak form.
 * The right hand side is the gradient of a vector function integrated by parts.
 * The output is given like this : val(i,0) = du/dx, val(i,1) = du/dy, val(i,2) = du/dz, val(i,3) = dv/dx, ...
 */
class dgFEVectorGradient : public dgConservationLawFunction {
  class interface0;
  class gradPsiTerm;
  class source;
 // class massFactor;
  const function *_uv, *_eta, *_bath;
  double _R;

  public:
  void setup();
  dgFEVectorGradient();
  dgFEVectorGradient(const function *uv);
  inline void setIsSpherical(double R){_R=R;};
  inline void setBath(const function *f) { _bath = f; };
  inline void setUV(const function *f) { _uv = f; };
  inline void setEta(const function *f) { _eta = f; };
  ~dgFEVectorGradient();
};

class dgUVHorDivergence : public dgConservationLawFunction {
  class interface0;
  class gradPsiTerm;
  class boundaryWall;
  const function *_bath, *_uv, *_eta; 
  
  public:
  void setup();
  dgUVHorDivergence();
  inline void setBath(const function *f) { _bath = f; };
  inline void setUV(const function *f) { _uv = f; };
  inline void setEta(const function *f) { _eta = f; };
  ~dgUVHorDivergence();
  dgBoundaryCondition *newBoundaryWall();
};

#endif
