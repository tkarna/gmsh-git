#ifndef SLIM3D_EQUATIONS_H
#define SLIM3D_EQUATIONS_H
#include "dgConfig.h"
#include "function.h"
#include "dgDofContainer.h"
#include "slim3dFunctions.h"
// #include "slim3dSolver.h"
#include "dgConservationLawSW3dMomentum.h"
#include "dgConservationLawSW3dContinuity.h"
#include "dgConservationLawSW3dTracer.h"
#include "dgFEGradient.h"
#include "dgMesh2MeshProjection.h"
#include "dgEddyTransportFlux.h"

class dgSW3dVerticalModel;
class dgSW3dTurbulenceGOTM;

class slim3dSolver;
/** A class that contains all the functions needed by the 3d shallow water equations solver. *
 * Most functions are initialized as NULL, they will be created in initialize() routine, 
 * depending on solver parameters. */
class slim3dSolverFunctions {
private:
  slim3dSolver *_solver;
  bool _initialized;
  const function *_kapv0Func, *_nuv0Func;
  dgDofContainer *_bathDof3d;

public:
  const function *zeroFunc, *zeroFunc2, *zeroFunc3, *tinyFunc, *oneFunc, *xyzFunc3d, *xyzFunc2d, *zFunc, *timeFunc;
  const function *uvCorrectorFunc;
  const function *pHydroStatFunc;
  const function *rhoFunc, *rhoFuncCenter, *eosAlphaFunc, *eosBetaFunc;
  const function *bathFunc2d, *bathFunc3d, *bathGradFunc2d, *bathGradFunc3d;
  const function *coriolisFunc;
  const function *TInitFunc, *SInitFunc, *TInitGradientFunc, *SInitGradientFunc, *sedInitFunc;
  const function *uvInitFunc, *uvAvInitFunc, *etaInitFunc, *wInitFunc;
  const function *SFunc, *TFunc, *SCenterGradientFunc, *TCenterGradientFunc;
  // moving mesh
  const function *newZFunc, *wMeshSurfFunc, *wMeshFunc, *wMeshDzFunc;
  // diffusivity
  const function *nuvFunc, *nuhTotal, *kappavFunc, *kappahTotal;
  // bottom friction
  const function *z0BFunc, *z0SFunc, *uvTauBFunc2d, *uvTauBFunc3d, *windStressFunc;
  const function *bottomFrictionFunc, *zBotDistFunc;
  const function *bottomFrictionDragCoeff2d, *bottomFrictionDragCoeff3d, *bottomUVDeviation;
  //const function *rGradFunc;
  const function *Stereo2XYZ;
  const function *Stereo2LonLat;
  const function *Stereo2LonLat2d;
  const function *XYZ2Stereo;
  const function *LonLat2Stereo;
  const function *uvDevFunc, *uvDevProdFunc, *uvDevAdvectionFunc;
  const function *eadySpeedFunc, *GMIndepTermFunc, *GMVelFunc;
  const function *slopeFunc, *slopeTap;
  // vertical velocity for sediment equation
  const function *wSedFunc, *sedBottomFluxFunc2d;
  /** Creates an instance for given solver */
  slim3dSolverFunctions(slim3dSolver *solver);
  ~slim3dSolverFunctions();
  /** Initializes functions that depend on user defined parameters *
   * The slim3dSolverDofs must be allocated before */
  void initialize();
  /** true if initialized */
  inline bool isInitialized() {return _initialized;};
  /** when bathymetry is already needed to define other functions */
  void initializeBath();
};

/** A class that contains all the 3d equations. */
class slim3dSolverEquations {
private:
  slim3dSolver *_solver;
  bool _initialized;
public:
  // TODO rename
  /** 3d horizontal momentum equation */
  dgConservationLawSW3dMomentum *horMomEq;
  /** Vertical diffusion for horizontal momentum equation */
  dgConservationLawSW3dMomWx *vertMomUEq;
  /** Free surface equation (depth averaged 2d) */
  dgConservationLawSW2dEta *eta2dEq;
  /** 2d horizontal momentum equation (depth averaged 2d) */
  dgConservationLawSW2dU *uv2dEq;
  dgUVHorDivergence *uvHorDivEq;
  /** 3d continuity equation for computing w (vertical velocity) */
  dgConservationLawSW3dContinuity *wEq;
#ifdef HAVE_GOTM
  /** turbulence closure model */
  dgSW3dTurbulenceGOTM *turbMod;
#endif
  /** 3d transport equation for salinity */
  dgConservationLawSW3dTracer *SEq;
  /** 3d transport equation for temperature */
  dgConservationLawSW3dTracer *TEq;
  /** 3d transport equation for sediment */
  dgConservationLawSW3dTracer *sedEq;
  /** 3d transport equation for turbulent kinetic energy */
  dgConservationLawSW3dTracer *tkeAdvEq;
  /** 3d transport equation for turbulence dissipation rate */
  dgConservationLawSW3dTracer *epsAdvEq;
  /** Vertical diffusion for salinity */
  dgConservationLawSW3dTracerVDiff *SDiffEq;
  /** Vertical diffusion for temperature */
  dgConservationLawSW3dTracerVDiff *TDiffEq;
  /** Vertical diffusion for sediment */
  dgConservationLawSW3dTracerVDiff *sedDiffEq;
  /** Vertical integrator for integrating r (baroclinic head) */
  dgSW3dDepthIntegrate *depthIntegratorRho;
  /** new way to compute rGRad */
  dgConservationLawSW3dPressure *newRGradEq;

  /** uv Gradient, for smagorinsky nuH */
  dgFEVectorGradient *uvGradEq;

  dgFEGradient *rhoGradEq;
  dgL2ProjectionCG *rhoGradContProj, *streamFuncContProj;
  dgEddyTransportFlux *GMEq;
  /** Brunt vaisala frequency */
  dgSW3dVerticalModel *nbMod;
  /** Creates an instance for given solver */
  slim3dSolverEquations(slim3dSolver *solver);
  ~slim3dSolverEquations();
  /** Initializes all required equations *
   * The slim3dSolverDofs and slim3dSolverFunctions must be initialized before */
  void initialize();
  /** true if initialized */
  inline bool isInitialized() {return _initialized;};
};

/** A class that contains all the dgDofContainers required by the 3d shallow water equations.
 * The dofContainers are public members for allowing access in python. */
class slim3dSolverDofs {
private:
  bool _initialized;
  slim3dSolver *_solver;
public:
  // check for unnecessary ones!
  /** u/v for implicit vertical diffusion */
  dgDofContainer *uDof,*vDof;
  /** uv horizontal 3d velocity */
  dgDofContainer *uvDof;
  /** uv horizontal Gradient 3d Velocity */
  dgDofContainer *uvHorDivDof;
  /** uv in center of triangles for GOTM */
  dgDofContainer *uvDofCenter;
  /** surface uv for moving mesh */
  dgDofContainer *uvSurfDof2d;
  /** uv for 2d equations */ 
  dgDofContainer *uvAvDof2d;
  /** depth integrated uvDof */
  dgDofContainer *uvIntDof2d;
  /** target depth integrated uv for velocity correction 3d */
  dgDofContainer *uvIntTargetDof3d; // TODO check if can be removed
  /** for correcting uvDof to match uvAvDof2d */
  dgDofContainer *uvCorrDof3d; // TODO check if can be removed
  /** uvDof evaluated at the middle of bottom most element for bottom friction */
  dgDofContainer *uvBotDof2d;
  /** bottom friction velocity */
  dgDofContainer *uvTauBDof2d;
  /** bottom friction velocity in center of triangles for GOTM */
  dgDofContainer *uvTauBDof2dCenter;
  /** windStress in center of triangles for GOTM */
  dgDofContainer *windStressDof2dCenter;
  /** for depth integrated advection of uvDeviation */
  dgDofContainer *uvDeviationProdIntDof2d;
  /** w vertical velocity */
  dgDofContainer *wDof3d;
  /** w vertical velocity copy for jumps in w*/
  //dgDofContainer *wDof3dCopy;
  /** w at the surface to moving mesh */
  dgDofContainer *wSurfDof2d;
  /** wMesh, vertical velocity of the mesh at the surface for moving mesh */
  dgDofContainer *wMeshSurfDof2d;
  /** eta, water elevation in 2d */
  dgDofContainer *etaDof2d;
  /** a continuous projection of etaDof2d for moving mesh */
  dgDofContainer *etaDof2dCG;
  /** etaDof3d in center of triangles for GOTM */
  dgDofContainer *etaDof3dCenter;
  /** r, baroclinic head */
  dgDofContainer *rDof3d;
  /** r integrated in vertical */
  dgDofContainer *rIntDof2d;
  /** r integrated in vertical */
  dgDofContainer *rBotDof2d;  
  /** rho, density */
  dgDofContainer *rhoDof3d;
  dgDofContainer *rhoGradDof3d;
  dgDofContainer *rGradDof3d;
  dgDofContainer *rhoSurfDof2d;
  /** horizontal gradient of r integrated in vertical */
  dgDofContainer *rGradIntDof2d;
  /** salinity */
  dgDofContainer *SDof;
  /** salinity in center of triangles for GOTM */
  dgDofContainer *SDofCenter;
  /** temperature */
  dgDofContainer *TDof;
  /** temperature in center of triangles for GOTM */
  dgDofContainer *TDofCenter;
  /** Sediment */
  dgDofContainer *sedDof, *sedBottomDof2d, *sedGroundDof2d, *sedBottomFluxDof2d;
  /** vertical coordinates of nodes for moving mesh */
  dgDofContainer *zDof;
  /** vertical coordinates in center of triangles for GOTM */
  dgDofContainer *zDofCenter;
  /** vertical coordinates in the middle of bottom most element for bottom friction */
  dgDofContainer *zBotDof2d;
  /** a container to access zBotDof2d in 3d */
  dgDofContainer *zBotDof3d;
  /** original (static) coordinates of nodes */
  dgDofContainer *xyzOrigDof;
  /** nn, buoyancy frequency squared for GOTM */
  dgDofContainer *nnDof;
  /** ss, vertical shear frequency squared for GOTM */
  dgDofContainer *ssDof;
  /** vertical eddy viscosity for momentum */
  dgDofContainer *nuvDof;
  /** vertical eddy viscosity in center of triangles for GOTM */
  dgDofContainer *nuvDofCenter;
  /** vertical eddy diffusivity for tracers */
  dgDofContainer *kappavDof;
  /** vertical eddy diffusivity in center of triangles for GOTM */
  dgDofContainer *kappavDofCenter;
  /** turbulent kinetic energy for GOTM */
  dgDofContainer *tkeDof;
  /** turbulent kinetic energy in center of triangles for GOTM */
  dgDofContainer *tkeDofCenter;
  /** turbulent kinetic energy dissipation rate */
  dgDofContainer *epsDof;
  /** dissipation rate in center of triangles for GOTM */
  dgDofContainer *epsDofCenter;
  /** turbulent lenth scale for GOTM */
  dgDofContainer *lDof;
  /** brunt vaisala frequency */
  dgDofContainer *nbDof3d;
  /** uvGrad, for smagorinsky diffusion */
  dgDofContainer *uvGradDof;
  dgDofContainer *uvGradDof2d;
  /** density gradient with cg weak formulation */
  dgDofContainer *rhoGradGMVelDof3d;
  dgDofContainer * streamFuncDof3d;
  dgDofContainer *GMVelDof3d;
  dgDofContainer *bathDof2d;
  slim3dSolverDofs(slim3dSolver *solver);
  ~slim3dSolverDofs();
  /** Allocates all required dofContainers */
  void initialize();
  /** true if initialized */
  inline bool isInitialized() {return _initialized;};
};

#endif
