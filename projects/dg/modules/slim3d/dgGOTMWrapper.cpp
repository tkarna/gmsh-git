#include "dgGOTMWrapper.h"
#include "stdlib.h"
#include <iostream>
#include <string>
#include <cstring>
#ifdef HAVE_GOTM
struct F90Array {
  void* dataPtr; //base address typically the first descriptor element
  int metadata[10]; //this should be only big enough to hold the remainder of the descriptor
};
extern "C" {
  #include "gotm.h"
  // routines
  void GOTM_turbulence_init_turbulence(int* namlst,const char* fn, int* nlev,size_t);
  void GOTM_turbulence_do_turbulence(int* nlev,double* dt,double* depth,double* u_taus,double* u_taub,double* z0s,double* z0b,double* h,double* NN,double* SS, double* xp);
  void GOTM_mtridiagonal_init_tridiagonal(int* N);
  // arrays
  extern F90Array GOTM_turbulence_tke;
  extern F90Array GOTM_turbulence_eps;
  extern F90Array GOTM_turbulence_l;
  extern F90Array GOTM_turbulence_num;
  extern F90Array GOTM_turbulence_nuh;
}
void dgGOTMinterface::init(int nlev, std::string namelist)
{
  _nlev = nlev-1; // fortran arrays are from 0...nlev
  _nNamelist = 2;
  size_t lenStr = namelist.length();
  GOTM_turbulence_init_turbulence(&_nNamelist,namelist.c_str(),&nlev,lenStr);
  GOTM_mtridiagonal_init_tridiagonal(&nlev);
  _tkeGOTM = ((double*) GOTM_turbulence_tke.dataPtr);
  _epsGOTM = ((double*) GOTM_turbulence_eps.dataPtr);
  _LGOTM = ((double*) GOTM_turbulence_l.dataPtr);
  _nuhGOTM = ((double*) GOTM_turbulence_nuh.dataPtr);
  _numGOTM = ((double*) GOTM_turbulence_num.dataPtr);
}

void dgGOTMinterface::doTurbulence(double dt, double depth, double uTaus, double uTaub, double z0s, double z0b, double* h,double* NN, double* SS)
{
  GOTM_turbulence_do_turbulence(&_nlev,&dt,&depth,&uTaus,&uTaub,&z0s,&z0b,h,NN,SS,NULL);
}

void dgGOTMinterface::setTKE(double* tke)
{
  for (int i = 0; i < _nlev+1 ; i++)
    _tkeGOTM[i] = tke[i];
}

void dgGOTMinterface::getTKE(double* tke)
{
  for (int i = 0; i < _nlev+1 ; i++)
    tke[i] = _tkeGOTM[i];
}

void dgGOTMinterface::setEPS(double* eps)
{
  for (int i = 0; i < _nlev+1 ; i++)
    _epsGOTM[i] = eps[i];
}

void dgGOTMinterface::getEPS(double* eps)
{
  for (int i = 0; i < _nlev+1 ; i++)
    eps[i] = _epsGOTM[i];
}

void dgGOTMinterface::setL(double* L)
{
  for (int i = 0; i < _nlev+1 ; i++)
    _LGOTM[i] = L[i];
}

void dgGOTMinterface::getL(double* L)
{
  for (int i = 0; i < _nlev+1 ; i++)
    L[i] = _LGOTM[i];
}

void dgGOTMinterface::setNUM(double* num)
{
  for (int i = 0; i < _nlev+1 ; i++)
    _numGOTM[i] = num[i];
}

void dgGOTMinterface::getNUM(double* num)
{
  for (int i = 0; i < _nlev+1 ; i++)
    num[i] = _numGOTM[i];
}

void dgGOTMinterface::setNUH(double* nuh)
{
  for (int i = 0; i < _nlev+1 ; i++)
    _nuhGOTM[i] = nuh[i];
}

void dgGOTMinterface::getNUH(double* nuh)
{
  for (int i = 0; i < _nlev+1 ; i++)
    nuh[i] = _nuhGOTM[i];
}

#endif

