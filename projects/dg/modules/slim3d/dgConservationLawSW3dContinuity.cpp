#include "dgConservationLawFunction.h"
#include "dgIntegrationMatrices.h"
#include "fullMatrix.h"
#include "dgGroupOfElements.h"
#include "dgDofContainer.h"
#include "SPoint3.h"
#include "function.h"
#include "dgConservationLawSW3dContinuity.h"
#include "Numeric.h"
#include <limits.h>
#include "slim3dFunctions.h"
#include "dgMesh.h"
#include "dgMeshJacobian.h"
#include "GmshDefines.h"
#include "dgTerm.h"

class dgConservationLawSW3dContinuity::advection : public function {
  fullMatrix<double> _xyz;
  double _R;
  fullMatrix<double> _uv;
  public:
  advection(const function* uv,double R):function(3) {
    setArgument (_uv, uv);
    _R=R;
    if (_R > 0)
      setArgument(_xyz,function::getCoordinates());
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++) {
      double J = 1;
      if (_R > 0)
        J = 4*_R*_R/(4*_R*_R+_xyz(i,0)*_xyz(i,0)+_xyz(i,1)*_xyz(i,1));
      val(i,0) = _uv(i,0)/J;
      val(i,1) = _uv(i,1)/J;
      val(i,2) = 0;
    }
  }
};

class dgConservationLawSW3dContinuity::source : public function {
  fullMatrix<double> _xyz,_uv;
  double _R;
  public:
  source(double R,const function* uv): function(1) {
    setArgument (_uv,uv);
    _R=R;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++) {
      val(i,0) = (-_xyz(i,0)*_uv(i,0)+_xyz(i,1)*_uv(i,1))/(_R*_R);
    }
  }
};

class dgConservationLawSW3dContinuity::riemann : public function {
  fullMatrix<double> normals, _wIn, _wOut,_UIn, _UOut,_etaIn,_etaOut,_bathymetry,_xyz;//_wOldIn,_wOldOut;
  double _R;
  bool _propagateFromTop;
  public:
  riemann(const function* uv, const function* eta, const function* bath, bool propagateFromTop,double R):function (2) {

    setArgument (normals, function::getNormals(), 0);
    setArgument (_wIn, function::getSolution(), 0);
    setArgument (_wOut, function::getSolution(), 1);
    setArgument (_UIn, uv, 0);
    setArgument (_UOut, uv, 1);
    setArgument (_etaIn, eta, 0);
    setArgument (_etaOut, eta, 1);
    setArgument (_bathymetry, bath);
    //setArgument (_wOldIn, w, 0); 
    //setArgument (_wOldOut, w, 1);
    _propagateFromTop = propagateFromTop;
    _R=R;
    setArgument(_xyz,function::getCoordinates());
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    double g = slim3dParameters::g;
    // determine which element is below
    dataCacheMap* dgMap = m;
    int layerIdIn = dgMap->getGroupOfElements()->getLayerId();
    int layerIdOut = dgMap->getSecondaryCache(1)->getGroupOfElements()->getLayerId();
    bool outIsTop = layerIdIn < layerIdOut;
    if (_propagateFromTop) // proper boundary behaviour
      outIsTop = layerIdIn <= layerIdOut;
    for(int i=0; i< val.size1(); i++) {
      double J = 1;
      if (_R>0)
        J = 4*_R*_R/(4*_R*_R+_xyz(i,0)*_xyz(i,0)+_xyz(i,1)*_xyz(i,1));
      double nx = normals(i,0);
      double ny = normals(i,1);
      double nz = normals(i,2);
      double unIn = nx*_UIn(i,0) + ny*_UIn(i,1);
      double unOut = nx*_UOut(i,0) + ny*_UOut(i,1);
      double un = (unIn+unOut)/2;
      double wn = 0;
      if (fabs(nz) < 1e-8) { // vertical face
        //linear equations
        double h = _bathymetry(i,0);
        double etaM = 0.5*(_etaIn(i,0)+_etaOut(i,0));
        double sq_g_h = sqrt(g/(h+etaM));
        un = (unIn+unOut + (_etaIn(i,0)-_etaOut(i,0))*sq_g_h)/2;
        //un = (unIn+unOut + 1*(_wOldIn(i,0) - _wOldOut(i,0)))/2;
        //if (fabs(_wOldIn(i,0) - _wOldOut(i,0)) >1e-6)
        //  printf("erreur!!!!+grand que 0 %g\n", _wOldIn(i,0) - _wOldOut(i,0));
        //printf("win, wout :%g %g\n", _wIn(i,0), _wOut(i,0));
      } else { // horizontal face, top/bottom
        if (outIsTop == _propagateFromTop) {
          un = unOut;
          wn = nz*_wOut(i,0);
        } else {
          un = unIn;
          wn = nz*_wIn(i,0);
        }
      }
      val(i,0) = - (un/J + wn);
      val(i,1) = -val(i,0);
    }
    
  }
};

dgConservationLawSW3dContinuity::dgConservationLawSW3dContinuity( int propagateFromTop ) :
  dgConservationLawFunction(1), _propagateFromTop(propagateFromTop){
    _uv = NULL;
    _eta = NULL;
    _bathymetry = NULL;
    _R=0;
}
void dgConservationLawSW3dContinuity::setup() {
  _volumeTerm0[""] = (_R > 0) ? new source (_R,_uv) : NULL;
  _volumeTerm1[""] = new advection (_uv,_R);
  _interfaceTerm0[""] = new riemann (_uv, _eta, _bathymetry, _propagateFromTop,_R);
  if (!_uv)
    Msg::Fatal("dgConservationLawSW3dContinuity: uv function must be set");
  if (!_eta)
    Msg::Fatal("dgConservationLawSW3dContinuity: eta function must be set");
}
dgConservationLawSW3dContinuity::~dgConservationLawSW3dContinuity() {
  if (_volumeTerm0[""]) delete _volumeTerm0[""];
  if (_volumeTerm1[""]) delete _volumeTerm1[""];
  if (_interfaceTerm0[""]) delete _interfaceTerm0[""];
}
class dgConservationLawSW3dContinuity::boundaryWall : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> normals, _uv, _eta;
    fullMatrix <double> externalUV, interfaceTerm, externalEta;
    functionReplace replaceFunction;
    public:
    term(dgConservationLawFunction *claw, const function *uv, const function *eta):function(claw->getNbFields()) {
      replaceFunction.addChild();
      addFunctionReplace (replaceFunction);
      replaceFunction.replace (externalUV, uv, 1);
      replaceFunction.replace (externalEta, eta, 1);
      replaceFunction.get(interfaceTerm,claw->getInterfaceTerm0(""));
      setArgument(_uv,uv);
      setArgument(normals,function::getNormals());
      setArgument(_eta,eta);
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      for(int i=0;i<val.size1(); i++) {
          double nx = normals(i,0);
          double ny = normals(i,1);
          double nz = normals(i,2);
          // compute normal/tangential velocity
          double un =  _uv(i,0)*nx + _uv(i,1)*ny;
          //double ut = -_uv(i,0)*ny + _uv(i,1)*nx;
          if (fabs(nz)>1e-8) { // non used!! (0 flux used for bottom)
          // symmetry for horizontal faces
            externalUV(i,0) = _uv(i,0);
            externalUV(i,1) = _uv(i,1);
          } else {
          // flip normal component
            externalUV(i,0) = _uv(i,0) - 2*un*nx;
            externalUV(i,1) = _uv(i,1) - 2*un*ny;
          }
          // symmetry for velocity and elevation
//           externalUV(i,0) = _uv(i,0);
//           externalUV(i,1) = _uv(i,1);
          externalEta(i,0) = _eta(i,0);
      }
      replaceFunction.compute();
      for(int i=0;i<val.size1(); i++) {
        for(int j=0;j<val.size2(); j++)
          val(i,j) = interfaceTerm(i,j);
      }
    }
  };
  term _term;
  public:
  boundaryWall(const function* _uv, const function* _eta, dgConservationLawFunction *claw): _term(claw, _uv, _eta) {
    _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawSW3dContinuity::newBoundaryWall(){
  return new boundaryWall(_uv, _eta, this);
}

class dgConservationLawSW3dContinuity::boundaryFreeSurf : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> normals, _uv, _w,_fwFlux;
    public:
    term(dgConservationLawFunction *claw, const function *uv, const function *w, const function *fwFlux):function(claw->getNbFields()) {
      setArgument(_uv,uv);
      setArgument(_w,w);
      setArgument(_fwFlux,fwFlux);
      setArgument(normals,function::getNormals());
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      for(int i=0;i<val.size1(); i++) {
          double nx = normals(i,0);
          double ny = normals(i,1);
          val(i,0) = _uv(i,0) *nx + _uv(i,1) * ny;
      }
    }
  };
  term _term;
  public:
  boundaryFreeSurf(const function* _uv,const function* _w,const function* _fwFlux, dgConservationLawFunction *claw): _term(claw, _uv, _w,_fwFlux) {
    _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawSW3dContinuity::newBoundaryFreeSurf(const function* fwFlux){
  return new boundaryFreeSurf(_uv, function::getSolution(), fwFlux,this);
}

class dgConservationLawSW3dPressure::advection : public function {
  fullMatrix<double> _rho;
  public:
  advection(const function* rho):function(6) {
    setArgument (_rho, rho);
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    double rho0 = slim3dParameters::rho0;
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++) {
      // d /dx
      val(i,0) = _rho(i,0)/rho0;
      val(i,1) = 0;
      // d /dy
      val(i,2) = 0;
      val(i,3) = _rho(i,0)/rho0;
      // d /dz
      val(i,4) = 0;
      val(i,5) = 0;
    }
  }
};
class dgConservationLawSW3dPressure::riemann : public function {
  fullMatrix<double> normals, _gradPIn, _gradPOut,_rhoIn,_rhoOut;
  bool _propagateFromTop;
  public:
  riemann(const function* rho, bool propagateFromTop):function (2) {
    setArgument (normals, function::getNormals(), 0);
    setArgument (_gradPIn, function::getSolution(), 0);
    setArgument (_gradPOut, function::getSolution(), 1);
    setArgument (_rhoIn, rho, 0);
    setArgument (_rhoOut, rho, 1);
    _propagateFromTop=propagateFromTop;
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    double rho0 = slim3dParameters::rho0;
    // determine which element is below
    dataCacheMap* dgMap = m;
    int layerIdIn = dgMap->getGroupOfElements()->getLayerId();
    int layerIdOut = dgMap->getSecondaryCache(1)->getGroupOfElements()->getLayerId();
    bool outIsTop = layerIdIn < layerIdOut;
    if (_propagateFromTop) // proper boundary behaviour
      outIsTop = layerIdIn <= layerIdOut;
    for(int i=0; i< val.size1(); i++) {
      double nx = normals(i,0);
      double ny = normals(i,1);
      double nz = normals(i,2);
      double rho = 0, gradPupwindX =0, gradPupwindY =0;
      if (fabs(nz) < 1e-6) { // horizontal -> vertical face
        rho = 0.5*(_rhoIn(i,0)+_rhoOut(i,0));
      } else { // horizontal face, top/bottom
        if (outIsTop == _propagateFromTop) {
          rho = _rhoOut(i,0);
          gradPupwindX = _gradPOut(i,0);
          gradPupwindY = _gradPOut(i,1);
        } else {
          rho = _rhoIn(i,0);
          gradPupwindX = _gradPIn(i,0);
          gradPupwindY = _gradPIn(i,1);
        }
      }
      val(i,0) = - (rho/rho0*nx + gradPupwindX*nz);
      val(i,1) = - (rho/rho0*ny + gradPupwindY*nz);
    }
  }
};

dgConservationLawSW3dPressure::dgConservationLawSW3dPressure(const function* rho, int propagateFromTop) :
 dgConservationLawFunction(2), _rho(rho), _propagateFromTop(propagateFromTop) {
  // dP/dx dP/dy
}

dgConservationLawSW3dPressure::dgConservationLawSW3dPressure(int propagateFromTop): dgConservationLawFunction(2)
{
  _rho = NULL;
}

void dgConservationLawSW3dPressure::setup() {
  if (!_rho)
    Msg::Fatal("dgConservationLawSW3dPressure: rho function must be set");
  _volumeTerm0[""] = NULL;
  _volumeTerm1[""] = new advection (_rho);
  _interfaceTerm0[""] = new riemann (_rho,_propagateFromTop);
}
dgConservationLawSW3dPressure::~dgConservationLawSW3dPressure() {
  if (_volumeTerm0[""]) delete _volumeTerm0[""];
  if (_volumeTerm1[""]) delete _volumeTerm1[""];
  if (_interfaceTerm0[""]) delete _interfaceTerm0[""];
}

class dgSW3dDepthIntegrate::source : public function {
  fullMatrix<double> _source;
  int _nbFields;
  public:
  source(const function* source): function(source->getNbCol()) {
    setArgument (_source, source);
    _nbFields = source->getNbCol();
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++) {
      for (int j=0; j < _nbFields; j++)
        val(i,j) = +_source(i,j);
    }
  }
};

class dgSW3dDepthIntegrate::riemann : public function {
  fullMatrix<double> normals,_solIn,_solOut;
  bool _propagateFromTop;
  int _nbFields;
  public:
  riemann(const function* source, bool propagateFromTop):function (2*source->getNbCol()) {
    setArgument (normals, function::getNormals(), 0);
    setArgument (_solIn, function::getSolution(), 0);
    setArgument (_solOut, function::getSolution(), 1);
    _propagateFromTop=propagateFromTop;
    _nbFields = source->getNbCol();
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    // determine which element is below
    dataCacheMap* dgMap = m;
    int layerIdIn = dgMap->getGroupOfElements()->getLayerId();
    int layerIdOut = dgMap->getSecondaryCache(1)->getGroupOfElements()->getLayerId();
    bool outIsTop = layerIdIn < layerIdOut;
    bool isBoundary = dgMap->getGroupOfInterfaces()->nConnection() == 1;
    if (_propagateFromTop) // proper boundary behaviour
      outIsTop = layerIdIn <= layerIdOut;
    for(int i=0; i< val.size1(); i++) {
      double nz = normals(i,2);
      for (int j = 0; j < _nbFields; j++) {
        double solUpwindIn=0,solUpwindOut=0;
        if (!isBoundary) {
          if (outIsTop == _propagateFromTop) {
            solUpwindIn = -_solOut(i,j);
            solUpwindOut = 0;
          } else { //  out is top always
            solUpwindIn = 0;
            solUpwindOut = _solIn(i,j);
          }
        }else{
          if (layerIdIn == 0) {
            // bottom bnd condition
            if (_propagateFromTop) {
              solUpwindIn =  0;
              solUpwindOut = 0;
            } else {
              solUpwindIn = -_solOut(i,j);
              solUpwindOut = 0;
            }
          } else {
            // top bnd condition
            if (_propagateFromTop) {
              solUpwindIn =  -_solOut(i,j);
              solUpwindOut = 0;
            } else {
              solUpwindIn = 0;
              solUpwindOut = 0;
            }
          }
        }
        val(i,j) = solUpwindIn*nz;
        val(i,j+_nbFields) = solUpwindOut*nz;
      }
    }
  }
};

dgSW3dDepthIntegrate::dgSW3dDepthIntegrate( int nbFields, int propagateFromTop ):
  dgConservationLawFunction(nbFields), _nbFields(nbFields), _propagateFromTop(propagateFromTop)
{
  _f = NULL;
}

dgSW3dDepthIntegrate::dgSW3dDepthIntegrate(const function* func, int propagateFromTop):
  dgConservationLawFunction(func->getNbCol()),
  _nbFields(func->getNbCol()), _propagateFromTop(propagateFromTop),
  _f(func)
{
}

void dgSW3dDepthIntegrate::setup()
{
  if (!_f)
    Msg::Fatal("dgSW3dDepthIntegrate: Integrant function must be set");
  if ( _f->getNbCol() != _nbFields )
    Msg::Fatal("dgSW3dDepthIntegrate: Integrant function size is wrong (%d != %d)",_f->getNbCol(),_nbFields);
    
  _volumeTerm0[""] = new source (_f);
  _volumeTerm1[""] = NULL;
  _interfaceTerm0[""] = new riemann (_f,_propagateFromTop);
}

dgSW3dDepthIntegrate::~dgSW3dDepthIntegrate()
{
  if (_volumeTerm0[""]) delete _volumeTerm0[""];
  if (_volumeTerm1[""]) delete _volumeTerm1[""];
  if (_interfaceTerm0[""]) delete _interfaceTerm0[""];
}

dgFaceTermAssembler *dgSW3dDepthIntegrate::getFaceTerm(const dgGroupOfFaces &group) const
{
  if(group.orientationTag() == dgGroupOfFaces::ORIENTATION_HORIZONTAL)
    return dgConservationLawFunction::getFaceTerm(group);
  return NULL;
  
}


dgConservationLawSW3dContinuitySolver::dgConservationLawSW3dContinuitySolver(dgDofContainer *sol,bool propagateFromTop){
  _propagateFromTop = propagateFromTop;
  const dgGroupCollection *groupColl = sol->getGroups();
  if (groupColl->getMaxGroupLayerId() < 0)
    Msg::Fatal("Groups have not been split in layers!");
  int maxGroupLayerId = std::max(groupColl->getMaxGroupLayerId(),0);
  _groupsByLayer.resize(maxGroupLayerId+1);
  _groupsOfFacesByLayer.resize(maxGroupLayerId+1);
  for(int iElemGroup=0;iElemGroup<groupColl->getNbElementGroups();iElemGroup++){
    dgGroupOfElements *elemGroup=groupColl->getElementGroup(iElemGroup);
    _groupsByLayer[elemGroup->getLayerId()].push_back(iElemGroup);
  }
  for(int iFaceGroup=0;iFaceGroup<groupColl->getNbFaceGroups();iFaceGroup++){
    dgGroupOfFaces *faceGroup=groupColl->getFaceGroup(iFaceGroup);
    int minLayer=INT_MAX;
    int maxLayer=-1;
    for(int iConn=0;iConn<faceGroup->nConnection();iConn++){
      const dgGroupOfElements& elemG = faceGroup->elementGroup(iConn);
      if (sol->getGroups()->getElementGroupId(&elemG) >= groupColl->getNbElementGroups())
        continue; // ignore ghost groups
      int layerId=elemG.getLayerId();
      if(layerId<minLayer)
        minLayer=layerId;
      if(layerId>maxLayer)
        maxLayer=layerId;
    }
    // both boundaries can be non-zero, need to compute both
    _groupsOfFacesByLayer[minLayer].push_back(faceGroup);
    if (maxLayer != minLayer)
      _groupsOfFacesByLayer[maxLayer].push_back(faceGroup);
  }
  // allocate mass matrix
  int nbElemGroups = groupColl->getNbElementGroups();
  _iMass.resize(nbElemGroups);
  for(int iElemGroup=0;iElemGroup<nbElemGroups;iElemGroup++){
    dgGroupOfElements *group=groupColl->getElementGroup(iElemGroup);
    int nbNodes = group->getNbNodes();
    _iMass[iElemGroup].resize(nbNodes, nbNodes, group->getNbElements(), true);
  }
  // build mass matrix
  fullMatrix<double> iMass;
  for(int iElemGroup=0;iElemGroup<nbElemGroups;iElemGroup++){
    dgGroupOfElements *elemGroup=groupColl->getElementGroup(iElemGroup);
    const nodalBasis &fs = elemGroup->getFunctionSpace();
    const dgIntegrationMatrices &intMatrices = dgIntegrationMatrices::get(fs.type, fs.order * 2 + 1);
    const dgMeshJacobian &jacobians = groupColl->_mesh->getJacobian(fs.order * 2 + 1);
    const fullMatrix<double> &detJ = jacobians.detJElement(iElemGroup);
    int nbQP = intMatrices.integrationPoints().size1();
    int nbNodes = fs.points.size1();
    for (int iQP=0;iQP< nbQP ; iQP++ ){
      for (size_t iElem = 0; iElem < elemGroup->getNbElements(); iElem++){
        _iMass[iElemGroup].getBlockProxy(iElem, iMass);
        for (int k=0;k<nbNodes;k++){
          for (int l=0;l<nbNodes;l++) {
            int iX = 2; // dPsiDz
            for(int alpha = 0; alpha<elemGroup->getDimUVW(); alpha++)
              iMass(k,l) -= intMatrices.psiDPsiW()(k*nbNodes+l,alpha + elemGroup->getDimUVW()*iQP)*jacobians.invJElement(iElemGroup, iElem,iQP,alpha,iX)*detJ(iQP, iElem);
          }
        }
      }
    }
  }
  for(int iFaceGroup=0;iFaceGroup<groupColl->getNbFaceGroups();iFaceGroup++){
    dgGroupOfFaces *faceGroup=groupColl->getFaceGroup(iFaceGroup);
    int nbConnections = faceGroup->nConnection();
    int nbFaceElem = faceGroup->size();
    const nodalBasis &fs = *faceGroup->getNodalBasis();
    int nbFaceNodes=fs.points.size1();
    const dgIntegrationMatrices &intMatrices = dgIntegrationMatrices::get(fs.type, 2 * fs.order + 1);
    const dgMeshJacobian &jacobians = groupColl->_mesh->getJacobian(2 * fs.order + 1);
    const fullMatrix<double> &detJ = jacobians.detJInterface(faceGroup->interfaceVectorId());
    if (faceGroup->orientationTag() == dgGroupOfFaces::ORIENTATION_VERTICAL)
      continue;
    int iLayerMax = INT_MIN, iLayerMin = INT_MAX, iConnMax=0,iConnMin=0;
    for (int iC = 0; iC < nbConnections; iC++) {
      int iL = faceGroup->elementGroup(iC).getLayerId();
      if (iL > iLayerMax) {
        iLayerMax = iL;
        iConnMax = iC;
      }
      if (iL < iLayerMin) {
        iLayerMin = iL;
        iConnMin = iC;
      }
    }
    // propagateFromTop => take upper connection
    int iConn = _propagateFromTop ? iConnMax : iConnMin;
    bool isBnd = nbConnections == 1;
    const dgGroupOfElements& elemGroup = faceGroup->elementGroup(iConn);
    int nbQP = intMatrices.integrationPoints().size1();
    const dgFullMatrix<double> &groupNormals = jacobians.normal(iFaceGroup, iConn);
    const int iEGroup = sol->getGroups()->getElementGroupId(&elemGroup);
    if (iEGroup >= nbElemGroups)
      continue; // ignore ghost groups
    for (int iElement=0 ; iElement<nbFaceElem ;iElement++) {
      const std::vector<int> &closure = faceGroup->closure(iElement, iConn);
      int iElementVol = faceGroup->elementId(iElement, iConn);
      _iMass[iEGroup].getBlockProxy(iElementVol, iMass);
      for (int iQP = 0; iQP < nbQP; iQP++) {
        // face normals
        fullMatrix<double> normals;
        groupNormals.getBlockProxy(iElement,normals);
        double nz = normals(iQP, 2);
        if (isBnd && (_propagateFromTop == (nz > 1e-8)))
          continue;
        for (int k=0;k<nbFaceNodes;k++){
          for (int l=0;l<nbFaceNodes;l++) {
            iMass(closure[l],closure[k]) += nz * intMatrices.psiPsiW()(k*nbFaceNodes+l, iQP)*detJ(iQP, faceGroup->interfaceId(iElement));
          }
        }
      }
    }
  }
  // invert!
  for(int iElemGroup=0;iElemGroup<nbElemGroups;iElemGroup++){
    dgGroupOfElements *group=groupColl->getElementGroup(iElemGroup);
    for (size_t iElem=0;iElem<group->getNbElements();iElem++){
      _iMass[iElemGroup].getBlockProxy(iElem, iMass);
      iMass.invertInPlace();
      if (fabs(iMass(0,0)) > 1e12) {
        Msg::Fatal("Could not invert mass matrix");
      }
    }
  }
}

dgConservationLawSW3dContinuitySolver::~dgConservationLawSW3dContinuitySolver()
{
}

void dgConservationLawSW3dContinuitySolver::iterate(dgConservationLawFunction *claw, dgDofContainer *sol, double time, bool scatterSolution){
  fullMatrix<double> residuEl,solEl;
  fullMatrix<double> iMassEl;
  int nbFields = claw->getNbFields();
  if (sol->getNbFields() != nbFields )
    Msg::Fatal("nbFields of the solution (%d) DofContainer does not match with the law (%d)",sol->getNbFields(),nbFields);
  const dgGroupCollection &groups = *sol->getGroups();
  dgDofContainer resd(groups, nbFields);
  // clear previous results (pollutes external values in Riemann)
  sol->setAll(0.0);
  for(int iL=0;iL<=groups.getMaxGroupLayerId();iL++){
    resd.setAll(0.0);
    int iLayer = _propagateFromTop ? groups.getMaxGroupLayerId() - iL : iL;
    // Residual Volume
    for(size_t iGroupInLayer=0;iGroupInLayer<_groupsByLayer[iLayer].size();iGroupInLayer++){
      int iGroup=_groupsByLayer[iLayer][iGroupInLayer];
      dgGroupOfElements &elemGroup = *groups.getElementGroup(iGroup);
      claw->computeElement(elemGroup, time, *sol, resd, NULL);
    }
    // Residual Faces
    for(size_t iGroupInLayer=0;iGroupInLayer<_groupsOfFacesByLayer[iLayer].size();iGroupInLayer++){
      dgGroupOfFaces *faceGroup=_groupsOfFacesByLayer[iLayer][iGroupInLayer];
      // compute residual
      claw->computeFace(*faceGroup, time, *sol, resd, NULL);
    }
    // Solve layer !
    for(size_t iGroupInLayer=0;iGroupInLayer<_groupsByLayer[iLayer].size();iGroupInLayer++){
      int iGroup=_groupsByLayer[iLayer][iGroupInLayer];
      dgGroupOfElements *elemGroup=groups.getElementGroup(iGroup);
      // multiply by inverse of <dpsidz*psi>
      for(size_t iEl=0;iEl<elemGroup->getNbElements();iEl++){
        resd.getGroupProxy(iGroup).getBlockProxy(iEl, residuEl);
        sol->getGroupProxy(iGroup).getBlockProxy(iEl, solEl);
        _iMass[iGroup].getBlockProxy(iEl, iMassEl);
        iMassEl.mult(residuEl,solEl);
      }
    }
  }
  if (scatterSolution)
    sol->scatter(true);
}

