#ifndef _DG_CONSERVATION_LAW_SW3D_CONTINUITY_H
#define _DG_CONSERVATION_LAW_SW3D_CONTINUITY_H
#include "dgConservationLawFunction.h"
#include "dgGroupOfElements.h"

/** Flag for integration direction **/
enum {INTEGRATE_FROM_TOP=1, INTEGRATE_FROM_BOTTOM=0};

/**Continuity equation for 3D shallow water equation.
 * Solves the vertical velocity based on the horizontal velocity field. */
class dgConservationLawSW3dContinuity : public dgConservationLawFunction {
  // u3d is horizontal 3d velocity
  // the 2d fields have been projected on 3d container for easier access
  // they are thus constants in z
  const function *_uv, *_eta, *_bathymetry;// *_w;
  double _R;
  bool _propagateFromTop;
  class advection;
  class source;
  class riemann;
  class boundaryWall;
  class boundaryFreeSurf;
  public:
  inline void setIsSpherical(double R){ _R = R;}
  dgConservationLawSW3dContinuity( int propagateFromTop );
  ~dgConservationLawSW3dContinuity();
  void setup();
  /**slip wall boundary */
  dgBoundaryCondition *newBoundaryWall();
  dgBoundaryCondition *newBoundaryFreeSurf(const function* freshWaterFlux);
  inline void setUV(const function *f) { _uv = f; };
  inline void setEta(const function *f) { _eta = f; };
  inline void setBathymetry(const function *f) { _bathymetry = f; };
  //inline void setW(const function *f) { _w = f; };
};

/**Pressure equation for 3D shallow water equation.
 * Solves the horizontal gradient of pressure field based on horizontal gradient of the density. */
class dgConservationLawSW3dPressure : public dgConservationLawFunction {
  const function *_rho;
  bool _propagateFromTop;
  class advection;
  class source;
  class riemann;
  public:
  dgConservationLawSW3dPressure( const function* rho, int propagateFromTop );
  dgConservationLawSW3dPressure( int propagateFromTop );
  void setRhoFunction(const function *f){ _rho = f; };
  ~dgConservationLawSW3dPressure();
  void setup();
};

/** Depth integrates a field in vertical.
 *  */
class dgSW3dDepthIntegrate : public dgConservationLawFunction {
  int _nbFields;
  bool _propagateFromTop;
  class advection;
  class source;
  class riemann;
  const function *_f;
  public:
  dgSW3dDepthIntegrate( int nbFields, int propagateFromTop );
  dgSW3dDepthIntegrate( const function *func, int propagateFromTop );
  inline void setFunction(const function *s) { _f = s; };
  ~dgSW3dDepthIntegrate();
  void setup();
  dgFaceTermAssembler *getFaceTerm(const dgGroupOfFaces &group) const;
};

/**Solving the continuity equation for 3d shallow water flows 
 * Class to solve the 3d shallow water continuity equation using a Gauss-Seidel iteration from bottom to top 
 * (or vice-versa if propagateFromTop==1).*/
class dgConservationLawSW3dContinuitySolver{
  private:
  std::vector<dgFullMatrix<double> > _iMass;
  std::vector<std::vector<int> >_groupsByLayer;
  std::vector<std::vector<dgGroupOfFaces*> >_groupsOfFacesByLayer;
  bool _propagateFromTop;
  public:
  dgConservationLawSW3dContinuitySolver(dgDofContainer *sol, bool propagateFromTop=false);
  ~dgConservationLawSW3dContinuitySolver();
  /**Solve the 3d shallow water continuity equation using a Gauss-Seidel iteration from bottom to top. */
  void iterate(dgConservationLawFunction *claw, dgDofContainer *sol, double time, bool scatterSolution = true);
};
#endif
