#ifndef _DG_SW3D_VERTICAL_COLUMNS_H
#define _DG_SW3D_VERTICAL_COLUMNS_H

class dgExtrusion;

#include "dgConfig.h"
#include "dgGroupOfElements.h"
#include "dgGOTMWrapper.h"
#include "dgDofContainer.h"

/** Parent data storage class for vertical 1D arrays */
class dgSW3dVEdgeData {
protected:
  /** a simple array class that provides double* pointer required by GOTM */
  class dataArray {
  private:
    int _nAllocated;
    double* _data;
  public:
    dataArray() : _nAllocated(0), _data(NULL) {};
    dataArray(int n);
    void allocate(int n);
    void clear();
    ~dataArray() { clear(); };
    inline double* getDoublePointer() { return _data; };
    inline double operator[](int i) const { return _data[i]; };
    inline double &operator[](int i) { return _data[i]; };
  };
  const dgExtrusion* _vEdges;
  std::vector< dataArray > _data;
public:
  /** Copies nodal data from the given function */
  virtual void importFromFunction(const function* f, int fieldId)=0;
  /** Copies nodal data from the given dgDofContainer */
  virtual void importFromDof(const dgDofContainer* c)=0;
  /** Copies nodal data to the given dgDofContainer */
  virtual void exportToDof(dgDofContainer* c, bool suppressValues=false)=0;
  dgSW3dVEdgeData(const dgExtrusion* vEdges) : _vEdges(vEdges){};
  virtual ~dgSW3dVEdgeData() {
    _data.clear();
  };
  inline double* getVEdgeData(int iVertical) { return _data[iVertical].getDoublePointer(); };

};

/** Data storage for vertical 1D arrays */
class dgSW3dVEdgeArray : public dgSW3dVEdgeData {
public:
  /** Defines whether the array stores duplicate nodes or not */
  enum arrayType {DG,CG};
private:
  arrayType _type;
public:
  dgSW3dVEdgeArray(const dgExtrusion* vEdges, arrayType type=DG);
  inline double operator() (int iVertical, int iLevel) const { return _data[iVertical][iLevel]; };
  inline double& operator() (int iVertical, int iLevel) { return _data[iVertical][iLevel]; };
  /** Copies nodal data from the given function */
  void importFromFunction(const function* f, int fieldId=0);
  /** Copies nodal data from the given dgDofContainer */
  void importFromDof(const dgDofContainer* c);
  /** Copies nodal data to the given dgDofContainer */
  void exportToDof(dgDofContainer* c, bool suppressTopBottom=false);
  /** Prints the content */
  void print(std::string label="");
  inline bool isDG() { return _type == DG; };
};

/** Data storage for vertical 1D scalars (one value per vertical) */
class dgSW3dVEdgeScalar : public dgSW3dVEdgeData {
public:
  dgSW3dVEdgeScalar(const dgExtrusion* vEdges);
  inline double operator() (int iVertical) const { return _data[iVertical][0]; };
  inline double& operator() (int iVertical) { return _data[iVertical][0]; };
  /** Copies nodal data from the given function */
  void importFromFunction(const function* f, int fieldId=0);
  /** Copies nodal data from the given 2d dgDofContainer */
  void importFromDof(const dgDofContainer* c);
  /** Copies nodal data to the given 2d dgDofContainer */
  void exportToDof(dgDofContainer* c, bool suppressVals=false);
  /** Prints the content */
  void print(std::string label="");
};

/** Interface to handle operations in 1D vertical edges. */
class dgSW3dVerticalModel {
  // vertical refers to 1d FD array, column refers to a column of 3d elements
  // layer refers to layer of 3d elems, point refers to a node in 1d vertical
  private:
  bool _useOldUVInSS,_computeNNWithEOS, _useFEGradient;
  int _order, _nb1DElemNodes;
  // 1d basis function values for evaluating at midpoint between nodes
  fullMatrix<double> _linePsiCenter;
  const function *_uvFunction, *_uvGradFunction, *_ssFunction, *_nnFunction, *_bathFunction, *_zFunction, *_SGradFunction, *_TGradFunction;
  const function *_eosAlphaFunction, *_eosBetaFunction;
  const function *_z0BFunction, *_z0SFunction, *_windStressFunction, *_uvStarBFunction;
  void _initialize(); // allocates data structures, builds maps, builds vertical cols
  protected:
  dgDofContainer *_TDof, *_SDof;
  const dgGroupCollection *_groups, *_groups2d;
  const dgExtrusion *_vColumns;
  // storage for scalars in each vertical
  dgSW3dVEdgeScalar _uStarSScalar, _uStarBScalar, _z0SScalar, _z0BScalar, _HScalar;
  // storages for arrays in each vertical (DG)
  dgSW3dVEdgeArray _uArray, _vArray, _uOldArray, _vOldArray, _TArray, _SArray;
  dgSW3dVEdgeArray  _ssArray, _nnArray, _dArray;
  dgSW3dVEdgeArray _rhoArray, _zArray, _tempDGArray, _dRhodZArray, _dSdZArray, _dTdZArray, _TCenterArray, _SCenterArray;
  dgSW3dVEdgeArray _eosBetaArray, _eosAlphaArray;
  dgSW3dVEdgeArray _dudzArray, _dvdzArray,_dudzArrayOld, _dvdzArrayOld;

  public:
  dgSW3dVerticalModel(const dgExtrusion* columns);
  ~dgSW3dVerticalModel();
  void setUseOldVelocityInSS(bool flag) {_useOldUVInSS = flag;};
  void setUseFEGradient(bool flag) {_useFEGradient = flag;};
  void setComputeNNWithEOS(bool flag) {_computeNNWithEOS = flag;};
  /** computes the Brunt-Vaisala frequency squared from given rho function. */
  void computeNN();
  /** computes the vertical shear frequency squared from uv function.
   * If useOldVelocity==true, uses formulation from Burchard, Ocean Modelling 4, 2002. */
  void computeSS();
  /** computes the heights of vertical edges and the total depth
   * Note: these must be known for computeNN, computeSS */
  void computeHeights();
  /** assigns bottom/top roughness lengths and friction velocities from the given functions */
  void computeBottomFriction();
  /** Evaluates the function at midpoints between nodes (in vertical) and stores to CG array */
  void evaluateFunctionAtCellCenters(const function* f, dgSW3dVEdgeArray* array, int fieldId=0);
  /* is used after hotStart for exact computing of SS */
  void evaluateUVAtCellCenters();
  /**Performs simple convective adjustment by mixing T and S in unstable elements
   * The parameter eps defines a tolerance for negative buoyancy frequency squared */
  void convectiveAdjustment(double eps);
  /**Set a function for the horizontal 3d velocity (u,v) */
  inline void setUVFunction(const function* f) {_uvFunction = f;};
  /**Set a function for the horizontal 3d velocity gradient (dudx,dudy,dudz,dvdx,dvdy,dvdz) */
  inline void setUVGradientFunction(const function* f) {_uvGradFunction = f;};
  /**Set a function for vertical coordinates */
  inline void setZFunction(const function* f) {_zFunction = f;};
  /**Set a function for equation of state Alpha function */
  inline void setEOSAlphaFunction(const function* f) {_eosAlphaFunction = f;};
  /**Set a function for equation of state Beta function */
  inline void setEOSBetaFunction(const function* f) {_eosBetaFunction = f;};
  /**Set a function for salinity gradient (dSdx, dSdy, dSdz) */
  inline void setSGradientFunction(const function* f) {_SGradFunction = f;};
  /**Set a function for temperature gradient  (dTdx, dTdy, dTdz) */
  inline void setTGradientFunction(const function* f) {_TGradFunction = f;};
  /** Set a function for surface roughness length
   * Should be a function in defined in 2d  mesh */
  inline void setZ0SFunction(const function* f0S) { _z0SFunction = f0S; };
  /** Set a function for bottom roughness length
   * Should be a function defined in 2d  mesh */
  inline void setZ0BFunction(const function* f0B) { _z0BFunction = f0B; };
  /** Set a function for bottom friction velocity [uStar,vStar]
   * Should be a function in defined in 2d  mesh */
  inline void setUVStarBFunction(const function* f) { _uvStarBFunction = f; };
  /** Set a function for surface friction velocity [uStar,vStar]
   * Should be a function in defined in 2d  mesh */
  inline void setWindStressFunction(const function* f) { _windStressFunction = f; };
  /**Set dgDofContainer for temperature (T) */
  inline void setTContainer(dgDofContainer* c) { _TDof = c;};
  /**Set dgDofContainer for salinity (S) */
  inline void setSContainer(dgDofContainer* c) { _SDof = c;};
  inline dgSW3dVEdgeArray* getSSArray() { return &_ssArray; };
  inline dgSW3dVEdgeArray* getNNArray() { return &_nnArray; };
  inline void exportNNData(dgDofContainer* dof) { _nnArray.exportToDof(dof); };
  inline void exportSSData(dgDofContainer* dof) { _ssArray.exportToDof(dof); };
  void convertToTriIntPoints(const dgDofContainer* source, dgDofContainer* dest);
  void convertFromTriIntPoints(const dgDofContainer* source, dgDofContainer* dest);
};

#ifdef HAVE_GOTM
/**Interface to run GOTM turbulence closure. */
class dgSW3dTurbulenceGOTM : public dgSW3dVerticalModel {
private:
  std::string _gotmTurbulenceNamelist;
  void _setGOTMVertical(int iVertical);   // copies data from buffer to gotm 1d arrays
  void _setInputArrays(); // copies data from functions to arrays nn ss layerH
  void _solveGOTM(int iVertcal,double dt); // calls GOTM turbulence library
  void _storeGOTMVertical(int iVertical); // copies data from gotm to buffer
  void _storeDiffusivities(); // copies num nuh data to dofcontainers
  bool _initialized;
  dgGOTMinterface _gotmInterface;
  // turbulece related arrays
  dgSW3dVEdgeArray _tkeArray, _epsArray, _lArray, _nuvArray, _kapvArray;
  dgDofContainer *_ssDof, *_nnDof, *_nuvDof, *_kapvDof, *_epsDof, *_tkeDof, *_lDof;
public:
  void initialize();
  dgSW3dTurbulenceGOTM(const dgExtrusion* columns, const std::string turbNamelist);
 /**Solve GOTM iteration using the given time step */
  void updateGOTM(double dt);
  /**Set dgDofContainer for tracer diffusivity */
  inline void setKappavContainer(dgDofContainer* c) { _kapvDof = c;};
  /**Set dgDofContainer for viscosity of momentum */
  inline void setNuvContainer(dgDofContainer* c) { _nuvDof = c;};
  /**Set dgDofContainer for epsilon */
  inline void setEpsContainer(dgDofContainer* c) { _epsDof = c;};
  /**Set dgDofContainer for the turbulent lenght scale L */
  inline void setLContainer(dgDofContainer* c) { _lDof = c;};
  /**Set dgDofContainer for turbulent kinetic energy */
  inline void setTkeContainer(dgDofContainer* c) { _tkeDof = c;};
  /**Set dgDofContainer for the (squared) buoyancy frequency */
  inline void setNNContainer(dgDofContainer* c) { _nnDof = c;};
  /**Set dgDofContainer for the (squared) shear frequency */
  inline void setSSContainer(dgDofContainer* c) { _ssDof = c;};
};
#endif

#endif
