#!/usr/bin/env python
# -*- coding: utf-8 -*-
from dgpy import *
from shutil import copyfile
import os, math

# .-------------------------------.
# |         Description           |
# '-------------------------------'
# Surface gravity waves in a closed rectangular domain
# Test1 : Volume conservation
# Salinity set to constant value -> Test2: tracer consistency
# Temperature is non-constant -> Test3: tracer mass conservation

# .-------------------------------.
# |     test case  tolerances     |
# '-------------------------------'
relVolTolerance = 1e-14
relTracerMassTolerance = 1e-14
TracerDevTolerance = 1.2e-4 #arbitrary

# .-------------------------------.
# |        generate mesh          |
# '-------------------------------'
g2 = GModel()
g2.load("box2d.geo")
g2.mesh(2)
g2.save("box2d.msh")
nbLayers = 4
def getLayersSigma (e,v) :
  h = 50 *(1-v[0]/5000.)/2. + 50 *(1+v[0]/5000.)/2.
  #h = 20 *(1-x/5000.)/2. + 5 *(1+x/5000.)/2.
  #h = 50 * (math.sin(6*3.14*(1-x/5000.)/2.)+1) + 80
  return [z * h /nbLayers for z in  range(nbLayers + 1)]
from extrude import *
extrude(mesh('box2d.msh'), getLayersSigma).write('box.msh')

# .-------------------------------.
# |   mesh and output directory   |
# '-------------------------------'

meshFile = 'box.msh'
odir = 'movingMesh/'
# create outputdir if !exists
if Msg.GetCommRank()==0 and not os.path.exists(odir) :
  os.mkdir(odir)
Msg.Barrier()
copyfile(meshFile,odir + '/mesh.msh')

# .-------------------------------.
# |         create solver         |
# '-------------------------------'
s = slim3dSolver(meshFile, ["bottom"], ["top"])
# options
s.setSolveS(1)
s.setSolveT(1)
s.setSolveSImplicitVerticalDiffusion(0)
s.setSolveTImplicitVerticalDiffusion(0)
s.setSolveUVImplicitVerticalDiffusion(0)
s.setSolveTurbulence(0)
s.setAdvectTurbulentEnergy(0)
s.setComputeBottomFriction(0)
s.exportDirectory = odir
s.turbulenceSetupFile = "gotmEstuaryTurb.nml"
s.setUseConservativeALE(1)

# .-------------------------------.
# |         set functions         |
# '-------------------------------'
f = s.functions

def bathF(val,xyz) :
  for i in range(val.size1()) :
    val.set(i,0, 50 *(1-xyz(i,0)/5000)/2. + 50 *(1+xyz(i,0)/5000)/2. )
    #val.set(i,0, 20 *(1-xyz(i,0)/5000)/2. + 5 *(1+xyz(i,0)/5000)/2. )
    #val.set(i,0, 50 * (math.sin(6*3.14*(1-xyz(i,0)/5000)/2.)+1) + 80 )
bathFunc2dcont = functionPython(1,bathF,[f.xyzFunc2d])
bathFunc3dcont = functionPython(1,bathF,[f.xyzFunc3d])
bathDof2d = dgDofContainer(s.groups2d,1)
bathDof2d.interpolate(bathFunc2dcont)
f.bathFunc2d = bathDof2d.getFunction()
f.bathGradFunc2d = bathDof2d.getFunctionGradient()
bathDof3d = dgDofContainer(s.groups3d,1)
bathDof3d.interpolate(bathFunc3dcont)
f.bathFunc3d = bathDof3d.getFunction()
f.bathGradFunc3d = bathDof3d.getFunctionGradient()

def SF(val,xyz) :
  for i in range(val.size1()) :
    #val.set(i,0, 5*(-xyz(i,2)/100) + 3 *(1+xyz(i,2)/100) )
    #val.set(i,0, -42 *(1-xyz(i,0)/5000)/2. + 4 *(1+xyz(i,0)/5000)/2. )
    val.set(i,0, 4.00 )
f.SInitFunc = functionPython(1,SF,[f.xyzFunc3d])
def TF(val,xyz) :
  for i in range(val.size1()) :
    val.set(i,0, 5*(-xyz(i,2)/50) + 3 *(1+xyz(i,2)/50) )
f.TInitFunc = functionPython(1,TF,[f.xyzFunc3d])

slim3dParameters.rho0 = 1002.83351

etaAmp = 0.1
def etaF(val,xyz) :
  for i in range(val.size1()) :
    val.set(i,0,  etaAmp*math.exp(- (xyz(i,0)/2000)**2) )
f.etaInitFunc = functionPython(1,etaF,[f.xyzFunc2d])

f.z0BFunc = functionConstant(0.005)
f.z0SFunc = functionConstant(0.02)

# .-------------------------------.
# |       create equations        |
# '-------------------------------'
e = s.createEquations()
d = s.getDofs()

eta2d = d.etaDof2d.getFunction()
uvAv2d = d.uvAvDof2d.getFunction()
eta2dCG = d.etaDof2dCG.getFunction()

horMomEq = e.horMomEq
horMomBndZero = horMomEq.new0FluxBoundary()
horMomBndSymm = horMomEq.newSymmetryBoundary('')
horMomBndWall = horMomEq.newBoundaryWall()
horMomEq.addBoundaryCondition('bottom',horMomBndWall) # must be wall for bath!
horMomEq.addBoundaryCondition('sideL',horMomBndWall)
horMomEq.addBoundaryCondition('river',horMomBndWall)
horMomEq.addBoundaryCondition('sideR',horMomBndWall)
horMomEq.addBoundaryCondition('sea',horMomBndWall)
horMomEq.addBoundaryCondition('top',horMomBndSymm) # zero for nonconst tracers!

if s.getSolveUVImplicitVerticalDiffusion() :
  vertMomUEq = e.vertMomUEq
  vertMomUBndZero = vertMomUEq.new0FluxBoundary()
  vertMomUBndBottom = vertMomUEq.newBoundaryBottom(f.bottomFrictionFunc,f.zBotDistFunc)
  vertMomUBndSymm = vertMomUEq.newSymmetryBoundary('')
  vertMomUEq.addBoundaryCondition('top',vertMomUBndZero)
  vertMomUEq.addBoundaryCondition('bottom',vertMomUBndBottom)
  vertMomUEq.addBoundaryCondition('sideL',vertMomUBndZero)
  vertMomUEq.addBoundaryCondition('river',vertMomUBndZero)
  vertMomUEq.addBoundaryCondition('sideR',vertMomUBndZero)
  vertMomUEq.addBoundaryCondition('sea',vertMomUBndZero)

wEq = e.wEq
wBndZero = wEq.new0FluxBoundary()
wBndSymm = wEq.newSymmetryBoundary('')
wEq.addBoundaryCondition('sideL',wBndZero)
wEq.addBoundaryCondition('river',wBndZero)
wEq.addBoundaryCondition('sideR',wBndZero)
wEq.addBoundaryCondition('sea',wBndZero)
wEq.addBoundaryCondition('top',wBndSymm) # must be symm!!
wEq.addBoundaryCondition('bottom',wBndZero)

if s.getSolveS() :
  SEq = e.SEq
  SBndZero = SEq.new0FluxBoundary()
  SEq.addBoundaryCondition(['sideL','river','sideR','sea','top','bottom'],SBndZero)

if s.getSolveT() :
  TEq = e.TEq
  TBndZero = TEq.new0FluxBoundary()
  TEq.addBoundaryCondition(['sideL','river','sideR','sea','top','bottom'],TBndZero)

if s.getAdvectTurbulentEnergy() :
  tkeAdvEq = e.tkeAdvEq
  tkeAdvBndZero = tkeAdvEq.new0FluxBoundary()
  tkeAdvEq.addBoundaryCondition(['sideL','river','sideR','sea','top','bottom'],tkeAdvBndZero)

  epsAdvEq = e.epsAdvEq
  epsAdvBndZero = epsAdvEq.new0FluxBoundary()
  epsAdvEq.addBoundaryCondition(['sideL','river','sideR','sea','top','bottom'],epsAdvBndZero)

eta2dEq = e.eta2dEq
etaBndZero = eta2dEq.new0FluxBoundary()
eta2dEq.addBoundaryCondition(['sideL','river','sideR','sea'],etaBndZero)

uv2dEq = e.uv2dEq
uv2dBndWall = uv2dEq.newBoundaryWall()
uv2dEq.addBoundaryCondition(['sideL','river','sideR','sea'],uv2dBndWall)

# .-------------------------------.
# |     setup time integrator     |
# '-------------------------------'
t = slim3dTimeIntegratorPC(s)

rk = dgERK(e.eta2dEq, None, DG_ERK_EULER)
dtRK = rk.computeInvSpectralRadius(d.etaDof2d)
CFL = 0.7263
dt = CFL*dtRK
#dt = 0.2 # for convergence
if Msg.GetCommRank() == 0 :
  print('Runge-Kutta dt :' + str(dtRK) + ' CFL:' + str(CFL) + ' dt:' + str(dt))

M = 10
t.setTimeStep(dt)
t.set3dTimeStepRatio(M)
t.setExportInterval(200)
t.setEndTime(8e3)
#t.setExportAtEveryTimeStep(True)

## .-------------------------------.
# |     setup export options      |
# '-------------------------------'

# list of fields to export
export = []
export.append(dgIdxExporter(d.SDof, odir + "/salinity"))
export.append(dgIdxExporter(d.uvDof, odir + "/uv", True))
export.append(dgIdxExporter(d.etaDof2d, odir + "/eta"))

#initial export
for e in export :
  e.exportIdx(t.getExportCount(), t.getTime())

tic = time.time()
while t.getTime() < t.getEndTime() :
  t.advanceOneTimeStep()
  t.checkSanity()
  if t.checkExport() :
    Msg.Info("export %i" % t.getExportCount())
    for e in export :
      e.exportIdx(t.getExportCount(), t.getTime())
    Msg.Info("%5i time : %g  normuv %6.12e clock %.1fs" % (t.getIter(), t.getTime(), d.uvDof.norm(), time.time() - tic))
    [relVolErr, relSErr, relTErr] = t.checkMassConservation()
    [absSDev, absTDev] = t.checkTracerConsistency()
    if abs(relVolErr) > relVolTolerance :
      print ('Volume conservation is broken!!')
      Msg.Exit(-1)
    if abs(absSDev) > TracerDevTolerance :
      print ('Tracer consistency is broken!!')
      Msg.Exit(-1)
    if max(abs(relSErr), abs(relTErr)) > relTracerMassTolerance :
      print ('Tracer mass conservation is broken!!')
      Msg.Exit(-1)
  if t.checkFullExport() :
    t.exportAll()
t.terminate(0)
