//*********** box2d.geo *************//
Cx = 5000;
Cy = 1000; // coarse, 2 layers
lc = 1000;
// Cy = 500; // med, 4 layers
// lc = 500;
// Cy = 200; // fine, 10 layers
// lc = 200;
// Cy = 100; // extraFine, 20 layers
// lc = 100;
L = 5;
Lup = 0;

Point(1) = {-Cx, -Cy, 0, lc};
Point(2) = {Cx , -Cy, 0, lc};
Point(3) = {Cx , Cy , 0, lc};
Point(4) = {-Cx, Cy , 0, lc};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};

Physical Line("sideL") = {1};
Physical Line("river") = {2};
Physical Line("sideR") = {3};
Physical Line("sea") = {4};

Line Loop(5) = {1,2,3,4};
Plane Surface(6) = {5};
Physical Surface("") = {6};

Mesh.Algorithm3D=4; // frontal [lobes]
Mesh.RecombinationAlgorithm=0; // standard instead of blossom
