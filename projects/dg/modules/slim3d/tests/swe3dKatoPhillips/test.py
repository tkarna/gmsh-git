#!/usr/bin/env python
# -*- coding: utf-8 -*-
from dgpy import *
from sys import stdout
from shutil import copyfile
import time, math, os

CCode ="""
#include "fullMatrix.h"
#include "function.h"
extern "C" {
/***** initial salinity *****/
void initS (dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< val.size1(); i++) {
    // nn is nearly 1e-4
    val.set(i,0, - 13.3e-3* xyz(i,2)); // rho0= 1027, without compressibility
  }
}
}
"""
tmpLib = "./tmp.dylib"
if Msg.GetCommRank() == 0 :
  functionC.buildLibrary (CCode, tmpLib)
Msg.Barrier()

# .-------------------------------.
# |         Description           |
# '-------------------------------'
# Kato-Phillips mixed layer deepening


# .-------------------------------.
# |     test case  tolerances     |
# '-------------------------------'
mixed_layer_depth_rel_err_TOL = 5e-2
spin_up_time = 10000
oprefix = 'mixed_layer_depth'
plotFigures = False
saveFigures = False

# .-------------------------------.
# |        generate mesh          |
# '-------------------------------'
meshStem = 'katoPhillips'
g2 = GModel()
g2.load(meshStem+'2d.geo')
g2.mesh(2)
g2.save(meshStem+'2d.msh')
#nbLayers = 2
nbLayers = 50
#nbLayers = 250
depth = 50.0
def getLayersSigma (e, v) :
  return [z * depth /nbLayers for z in  range(nbLayers + 1)]
from extrude import *
extrude(mesh(meshStem+'2d.msh'), getLayersSigma).write(meshStem+'.msh')

# .-------------------------------.
# |   mesh and output directory   |
# '-------------------------------'
meshFile = meshStem+'.msh'
meshFile2d = meshStem+'2d.msh'

odir = 'temp/'
# create outputdir if !exists
if not os.path.exists(odir) :
  os.mkdir(odir)
copyfile(meshFile,odir + 'mesh.msh')
#copyfile(meshFile2d,odir + 'mesh2d.msh')

# .-------------------------------.
# |         create solver         |
# '-------------------------------'
s = slim3dSolver(meshFile, ["bottom"], ["top"])
# options
s.setSolve2d(1)
s.setSolveW(1)
s.setSolveS(0)
s.setSolveSImplicitVerticalDiffusion(1)
s.setSolveUVImplicitVerticalDiffusion(1)
s.setSolveTurbulence(1)
s.setAdvectTurbulentEnergy(0)
s.setComputeBottomFriction(1)
s.setFlagMovingMesh(0)
s.exportDirectory = odir
s.turbulenceSetupFile = "katoPhillipsTurb.nml"
s.setFlagUVLimiter(0)

# .-------------------------------.
# |         set functions         |
# '-------------------------------'
f = s.functions

windStressX = 0.1027
windFunc = functionConstant([windStressX,0.0])

bathFunc = functionConstant(depth)
f.bathFunc2d = bathFunc
f.bathFunc3d = bathFunc
f.bathGradFunc2d = f.zeroFunc3
f.bathGradFunc3d = f.zeroFunc3

f.TInitFunc = functionConstant(10)
f.SInitFunc = functionC(tmpLib,'initS',1,[f.xyzFunc3d])

f.nuhTotal = functionConstant(1)

# background diffusivity (from GOTM)
slim3dParameters.nuv0 = 1.3e-6
slim3dParameters.kapv0T = 1.4e-7
slim3dParameters.kapv0S = 1.1e-9
slim3dParameters.rho0 = 1027
f.z0BFunc = functionConstant(0.1)
f.z0SFunc = functionConstant(0.02)
f.windStressFunc = windFunc

# .-------------------------------.
# |       create equations        |
# '-------------------------------'
e = s.createEquations()
d = s.getDofs()

horMomEq = e.horMomEq
horMomBndSides = horMomEq.newBoundaryWallLinear(0.1)
horMomBndWall = horMomEq.newBoundaryWall()
horMomBndZero = horMomEq.new0FluxBoundary()
horMomBndSymm = horMomEq.newSymmetryBoundary('')
uFunc = functionExtractCompNew(function.getSolution(),0)
vFunc = functionExtractCompNew(function.getSolution(),1)
vNegFunc = functionScaleNew(vFunc,-1)
uvNegFunc = functionCatCompNew([uFunc,vNegFunc])
horMomBndOut = horMomEq.newOutsideValueBoundary('',uvNegFunc)
horMomEq.addBoundaryCondition(['side1','side3'],horMomBndSides)
horMomEq.addBoundaryCondition(['side2','side4'],horMomBndSymm)
#horMomEq.addBoundaryCondition(['bottom','top'],horMomBndZero)
horMomEq.addBoundaryCondition('top',horMomBndSymm) # zero for nonconst tracers!
horMomEq.addBoundaryCondition('bottom',horMomBndWall) # must be wall for bath!

if s.getSolveUVImplicitVerticalDiffusion() :
  vertMomUEq = e.vertMomUEq
  vertMomUBndSurf = vertMomUEq.newBoundarySurface(windFunc)
  vertMomUBndZero = vertMomUEq.new0FluxBoundary()
  vertMomUEq.addBoundaryCondition('top',vertMomUBndSurf)
  vertMomUEq.addBoundaryCondition(['bottom','side1','side2','side3','side4'],vertMomUBndZero)

if s.getSolveW() :
  wEq = e.wEq
  wBndZero = wEq.new0FluxBoundary()
  wBndSymm = wEq.newSymmetryBoundary('')
  wEq.addBoundaryCondition(['side1','side3'],wBndZero)
  wEq.addBoundaryCondition(['side2','side4'],wBndSymm)
  wEq.addBoundaryCondition('top',wBndSymm) # must be symm!!
  wEq.addBoundaryCondition('bottom',wBndZero)

if s.getAdvectTurbulentEnergy() :
  tkeAdvEq = e.tkeAdvEq
  tkeAdvBndZero = tkeAdvEq.new0FluxBoundary()
  tkeAdvBndSymm = tkeAdvEq.newSymmetryBoundary('')
  tkeAdvBndOpen = tkeAdvEq.newInFluxBoundary(f.tinyFunc)
  tkeAdvEq.addBoundaryCondition(['side1','side3'],tkeAdvBndZero)
  tkeAdvEq.addBoundaryCondition(['side2','side4'],tkeAdvBndSymm)
  tkeAdvEq.addBoundaryCondition(['top','bottom'],tkeAdvBndZero)

  epsAdvEq = e.epsAdvEq
  epsAdvBndZero = epsAdvEq.new0FluxBoundary()
  epsAdvBndSymm = epsAdvEq.newSymmetryBoundary('')
  epsAdvBndOpen = epsAdvEq.newInFluxBoundary(f.tinyFunc)
  epsAdvEq.addBoundaryCondition(['side1','side3'],epsAdvBndZero)
  epsAdvEq.addBoundaryCondition(['side2','side4'],epsAdvBndSymm)
  epsAdvEq.addBoundaryCondition(['top','bottom'],epsAdvBndZero)

eta2dEq = e.eta2dEq
etaBndZero = eta2dEq.new0FluxBoundary()
etaBndSymm = eta2dEq.newSymmetryBoundary('')
eta2dEq.addBoundaryCondition(['side1','side3'],etaBndZero)
eta2dEq.addBoundaryCondition(['side2','side4'],etaBndSymm)

uv2dEq = e.uv2dEq
uv2dEq.setWindStress(windFunc)
uv2dBndWall = uv2dEq.newBoundaryWall()
uv2dBndSymm = uv2dEq.newSymmetryBoundary('')
uv2dEq.addBoundaryCondition(['side1','side3'],uv2dBndWall)
uv2dEq.addBoundaryCondition(['side2','side4'],uv2dBndSymm)

# .-------------------------------.
# |     setup time integrator     |
# '-------------------------------'
t = slim3dTimeIntegratorPC(s)

rk = dgERK(e.eta2dEq, None, DG_ERK_EULER)
dtRK = rk.computeInvSpectralRadius(d.etaDof2d)
CFL = 0.7236
dt = CFL*dtRK
if Msg.GetCommRank() == 0 :
  print('Runge-Kutta dt :' + str(dtRK) + ' CFL:' + str(CFL) + ' dt:' + str(dt))

t.setTimeStep(12)
t.set3dTimeStepRatio(5)
t.setExportInterval(5*60*20)
t.setEndTime(108000/2)
#t.setExportAtEveryTimeStep(True)

# list of fields to export
export = []
export.append(dgIdxExporter(d.SDof, odir + "/salinity"))
export.append(dgIdxExporter(d.uvDof, odir + "/uv", True))
export.append(dgIdxExporter(d.etaDof2d, odir + "/eta"))
export.append(dgIdxExporter(d.nuvDof, odir + "/nuv"))

#initial export
for e in export :
  e.exportIdx(t.getExportCount(), t.getTime() - 0)

# .-------------------------------.
# |for computing mixed layer depth|
# '-------------------------------'
from scipy import *
from numpy import *
x = 0.49
y = 0.49
nZ = 50
etaVal = 0
bathVal = 50
zVals = linspace(-bathVal,etaVal,nZ)


maxExportCount = int(t.getEndTime()/t.getExportInterval())+1
export_counts = range(1,maxExportCount)
nbTimes = len(export_counts)
tkeVals = zeros( (len(zVals),nbTimes) )
dgDCMap = dataCacheMap(dataCacheMap.POINT_MODE, s.groups3d)
tkeCache = dgDCMap.get(d.tkeDof.getFunction())
times = zeros( (nbTimes, 1))
zML  = zeros(nbTimes)
zMLa = zeros(nbTimes)

def find_lin_root(x,y,y_target) :
  if shape(x) != shape(y) :
    print ("arrays must have same size")
  one = mat(ones(shape(x)))
  A = bmat('x,one')
  coeffs = linalg.solve(A,y)
  x_target = (y_target - coeffs[1])/coeffs[0]
  return x_target[0,0]

def compute_mixed_layer_depth(z,tke,tke_threshold) :
  nz = len(tke)
  zp = 0
  ix = nonzero( tke > tke_threshold )
  thresholdFound = True
  if ix[0].size == 0 :
    ix = nz-1
    thresholdFound = False
  else :
    ix = ix[0][0]
  sliceIx = ix-1
  if ix == 0 :
    sliceIx = 0
  if z[sliceIx] == z[sliceIx+1] :
    zML = z[sliceIx]
  else :
    # note to self: slices are always horzontal T transposes
    # note to self: slice index must be one extra, e.g. 0:2 for elems 0,1 (like range)
    zMat = mat(z[sliceIx:(sliceIx+2)]).T
    tkeMat = mat(tke[sliceIx:sliceIx+2]).T
    if thresholdFound :
      zML = find_lin_root(zMat,tkeMat,tke_threshold)
    else :
      zML = 0.5*( zMat[0,0] + zMat[1,0] )
  return zML

def analytical_mixed_layer_depth(times) :
  ustar = 1e-2
  N0 = 1e-2
  zML = -1.05*ustar*sqrt(times/N0)
  return zML

# .-------------------------------.
# |     time integration loop     |
# '-------------------------------'
tic = time.time()
while t.getTime() < t.getEndTime() :
  t.advanceOneTimeStep()
  t.checkSanity()
  if t.checkExport() :
    Msg.Info("%5i time %g normuv %6.12e clock %.1fs" % (t.getIter(),t.getTime(), d.uvDof.norm(), time.time() - tic))
    Msg.Info("export %i" % t.getExportCount())
    for e in export :
      e.exportIdx(t.getExportCount(), t.getTime())
    iT = t.getExportCount()-1
    times[iT] = t.getTime()
    for iZ in range(size(zVals)) :
      checkElem = dgDCMap.setPoint(s.groups3d,x,y,zVals[iZ])
      tkeVals[iZ,iT] = tkeCache.get()(0,0)
    # compute mixed layer depth for this export
    zML[iT] = compute_mixed_layer_depth(zVals,tkeVals[:,iT],1e-5)
    zMLa[iT] = analytical_mixed_layer_depth(times[iT])
    print (' Mixed layer depth: {0:e} (ref: {1:e})'.format(zML[iT],zMLa[iT]))
    # test tolerance
    if times[iT] > spin_up_time :
      relErr = abs((zML[iT]-zMLa[iT])/zMLa[iT])
      print ('  relative error: {0:e}'.format(relErr))
      if relErr > mixed_layer_depth_rel_err_TOL :
        print (' ** Mixed layer depth is wrong -- Test failed !!')
        Msg.Exit(-1)

if plotFigures :
  import matplotlib.pyplot as plt
  from matplotlib import rc
  rc('text', usetex=True)
  rc('font', family='times', size=12)

  print ("Plotting ...")
  plt.hold(True)
  plt.plot(times/3600,-zML,'b')
  plt.plot(times/3600,-zMLa,'k:')
  plt.title(r'Mixed layer depth [$m$]')
  plt.xlabel(r'$t$ [$h$]')
  if saveFigures :
    filename = odir+oprefix+'.pdf'
    print ("Saving plot to {name:s}".format(name=filename))
    plt.savefig(filename)
  plt.show()

# success
Msg.Exit(0)
