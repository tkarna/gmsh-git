//*********** katoPhillips2d.geo *************//
Cx = 8000;
Cy = 8000;
L = 5;
Lup = 0;
lc = 19000;

Point(1) = {0.0, 0.0, 0, lc};
Point(2) = {Cx , 0.0, 0, lc};
Point(3) = {Cx , Cy , 0, lc};
Point(4) = {0.0, Cy , 0, lc};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};

Physical Line("side1") = {1};
Physical Line("side2") = {2};
Physical Line("side3") = {3};
Physical Line("side4") = {4};

Line Loop(5) = {1,2,3,4};
Plane Surface(6) = {5};
Physical Surface("") = {6};
