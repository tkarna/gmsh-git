d = 5e5;
Point(1) = {-d, -d, 0};
Point(2) = {d, -d, 0};
Point(3) = {d, d, 0};
Point(4) = {-d, d, 0};
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line Loop(5) = {3, 4, 1, 2};

Field[1] = MathEval;
//Field[1].F = "2e5";
Field[1].F = "1e5";
Background Field = 1;

Plane Surface(6) = {5};
Physical Surface("Surface")={6};
Physical Line("Wall") = {1,2,3,4};

Mesh.LcIntegrationPrecision = 1e-3;
Mesh.CharacteristicLengthExtendFromBoundary=0;
Mesh.CharacteristicLengthFromPoints=0;
