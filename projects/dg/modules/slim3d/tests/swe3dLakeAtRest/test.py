#!/usr/bin/env python
# -*- coding: utf-8 -*-
from dgpy import *
#from sys import stdout
from shutil import copyfile
import time, math, os, inspect
from math import *
#from matplotlib import pyplot as plt

# set Ctrl-C to default signal (terminates immediately)
import signal
signal.signal(signal.SIGINT, signal.SIG_DFL)

normUTolerance = 5e-10

# .-------------------------------.
# |        generate mesh          |
# '-------------------------------'
g = GModel()
g.load("square.geo")
g.mesh(2)
g.save("square.msh")

nbLayers = 5

def getLayers(e, v) :
  dist = sqrt(v[0]*v[0]+v[1]*v[1])
  #h = 1000
  h = 600
  if dist < 4e5 :
    #h = 800 - 200 * cos(dist/4e5*pi) #shallower in the middle of the domain
    h = 800 + 200 * cos(dist/4e5*pi) #deeper in the middle of the domain
  return [z * h /nbLayers for z in  range(nbLayers + 1)]
from extrude import *
extrude(mesh("square.msh"), getLayers).write("square3d.msh")

# .-------------------------------.
# |   mesh and output directory   |
# '-------------------------------'

meshFile = "square3d.msh"
odir = 'output'
# create outputdir if !exists
if Msg.GetCommRank()==0 and not os.path.exists(odir) :
    os.mkdir(odir)
if Msg.GetCommRank()==0 :
  copyfile(meshFile,odir + '/mesh.msh')
  pythonFile = inspect.getfile(inspect.currentframe())
  copyfile(pythonFile,odir + '/' + pythonFile)

# .-------------------------------.
# |         create solver         |
# '-------------------------------'

s = slim3dSolver(meshFile, ["bottom_Surface"], ["top_Surface"])
# options
s.setSolveS(0)
s.setSolveT(1)
s.setSolveSImplicitVerticalDiffusion(0)
s.setSolveTImplicitVerticalDiffusion(0)
s.setSolveUVImplicitVerticalDiffusion(0)
s.setSolveTurbulence(0)
s.setAdvectTurbulentEnergy(0)
s.setComputeBottomFriction(1)
s.setFlagMovingMesh(1)
s.exportDirectory = odir
s.setFlagUVLimiter(1)
s.setFlagTLimiter(0)
#s.setUseConservativeALE(1)
if s.getSolveTurbulence() :
  s.turbulenceSetupFile = "squareTurb.nml"

# .-------------------------------.
# |         set functions         |
# '-------------------------------'
f = s.functions
d = s.getDofs(True)

f.z0BFunc = functionConstant(0.01)
f.z0SFunc = functionConstant(0.00)
#f.kappahTotal = functionConstant(30)
#f.kappavFunc = functionConstant(0.01)
#f.kappavFunc = pacanowskiPhilander(d.uvDof.getFunctionGradient(), d.rhoDof3d.getFunctionGradient(), 1e-2, 1e-5, 10)
#f.nuvFunc = functionConstant(0.1)
#f.nuhTotal = functionConstant(200)
f.rhoFunc = linearStateEquation(d.TDof.getFunction(), -0.1, 20)
#f.rhoFunc = functionConstant(1.)
f.bottomFrictionDragCoeff2d = functionConstant(2.5e-3)
f.bottomFrictionDragCoeff3d = functionConstant(2.5e-3)

slim3dParameters.rho0 = 1021
CCode = """
#include "fullMatrix.h"
#include "function.h"
extern "C" {
void coriolis (dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &xyz) {
  for (size_t i = 0; i < xyz.size1(); ++i) {
    val.set(i, 0, 1e-4 + 2e-11 * xyz.get(i, 1));
  }
}
void temp (dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &xyz) {
  for (size_t i = 0; i < xyz.size1(); i++){
    val.set(i,0,25 + 8.2e-3*xyz(i,2) ); 
    //val.set(i,0,25 ); 
  }
}
void rhoF (dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &xyz) {
  for (size_t i = 0; i < val.size1(); i++){
    val.set(i,0,-1.5 - 8.2e-4*xyz(i,2) ); 
    //val.set(i,0,-0.1 *(xyz(i,0)-10) ); // c'est pas xyz ici, c'est T!!
  }
}
}
"""
tmpLib = "./tmp.dylib"
if (Msg.GetCommRank() == 0) :
  functionC.buildLibrary(CCode,  tmpLib);
Msg.Barrier()

f.coriolisFunc3d= functionC(tmpLib, "coriolis",  1,  [function.getCoordinates()])
f.coriolisFunc2d= f.coriolisFunc3d

f.TInitFunc = functionC(tmpLib, "temp", 1, [function.getCoordinates()])
#f.rhoFunc = functionC(tmpLib, "rhoF", 1, [function.getCoordinates()])
#f.rhoFunc = functionC(tmpLib, "rhoF", 1, [d.TDof.getFunction()])
f.SInitFunc = functionConstant(30)
f.etaInitFunc = functionConstant(0)

# .-------------------------------.
# |       create equations        |
# '-------------------------------'

e = s.createEquations()

horMomEq = e.horMomEq
#horMomEq.setIsSpherical
horMomEq.setLaxFriedrichsFactor(0.0)
#horMomEq.setNuV(f.nuVFunc)
horMomBndSides = horMomEq.newBoundaryWallLinear(0.0)
horMomBndZero = horMomEq.new0FluxBoundary()
horMomBndSymm = horMomEq.newSymmetryBoundary('')
#horMomBndTop =  horMomEq.newBoundarySurface(fWind)
horMomBndWall = horMomEq.newBoundaryWall()
horMomEq.addBoundaryCondition('bottom_Surface',horMomBndWall)
horMomEq.addBoundaryCondition('Wall',horMomBndWall)
#horMomEq.addBoundaryCondition('cut',horMomBndWall)
#horMomEq.addBoundaryCondition('paste',horMomBndWall)
horMomEq.addBoundaryCondition('vertical_bottom',horMomBndWall)
horMomEq.addBoundaryCondition('top_Surface',horMomBndSymm)
#horMomEq.addBoundaryCondition('top_Surface',horMomBndTop)

if s.getSolveUVImplicitVerticalDiffusion() :
  vertMomUEq = e.vertMomUEq
  vertMomUBndZero = vertMomUEq.new0FluxBoundary()
  vertMomUBndBottom = vertMomUEq.newBoundaryBottom(f.bottomFrictionFunc, f.zBotDistFunc)
  vertMomUBndSymm = vertMomUEq.newSymmetryBoundary('')
  #vertMomUBndTop =  vertMomUEq.newBoundarySurface(fWind)
  vertMomUEq.addBoundaryCondition('top_Surface', vertMomUBndZero)
  vertMomUEq.addBoundaryCondition('bottom_Surface', vertMomUBndBottom)
  vertMomUEq.addBoundaryCondition('Wall', vertMomUBndZero)
  #vertMomUEq.addBoundaryCondition('cut', vertMomUBndZero)
  #vertMomUEq.addBoundaryCondition('paste', vertMomUBndZero)
  vertMomUEq.addBoundaryCondition('vertical_bottom', vertMomUBndZero)

wEq = e.wEq
#wEq.setIsSpherical(R)
wBndZero = wEq.new0FluxBoundary()
wBndSymm = wEq.newSymmetryBoundary('')
wEq.addBoundaryCondition('Wall',wBndZero)
#wEq.addBoundaryCondition('cut',wBndZero)
#wEq.addBoundaryCondition('paste',wBndZero)
wEq.addBoundaryCondition('vertical_bottom',wBndZero)
wEq.addBoundaryCondition('top_Surface',wBndSymm)
wEq.addBoundaryCondition('bottom_Surface',wBndZero)

if s.getSolveT() :
  TEq = e.TEq
  TEq.setLaxFriedrichsFactor(0.0)
  TBndZero = TEq.new0FluxBoundary()
  TBndSymm = TEq.newSymmetryBoundary('')
  TEq.addBoundaryCondition('Wall',TBndZero)
  #TEq.addBoundaryCondition('cut',TBndZero)
  #TEq.addBoundaryCondition('paste',TBndZero)
  TEq.addBoundaryCondition('vertical_bottom',TBndZero)
  TEq.addBoundaryCondition('top_Surface',TBndZero)
  #TBndOut = TEq.newOutsideValueBoundary('', f.TInitFunc)
  TEq.addBoundaryCondition('bottom_Surface',TBndZero)
  #TEq.addBoundaryCondition('bottom_Surface',TBndSymm)
  #TEq.addBoundaryCondition('bottom_Surface',TBndOut)

eta2dEq = e.eta2dEq
#eta2dEq.setIsSpherical(R)
etaBndZero = eta2dEq.new0FluxBoundary()
eta2dEq.addBoundaryCondition('Wall',etaBndZero)
#eta2dEq.addBoundaryCondition('cut',etaBndZero)
#eta2dEq.addBoundaryCondition('paste',etaBndZero)


uv2dEq = e.uv2dEq
#uv2dEq.setIsSpherical(R)
#uv2dEq.setWindStress(fWind)
uv2dBndWall = uv2dEq.newBoundaryWall()
uv2dEq.addBoundaryCondition('Wall',uv2dBndWall)
#uv2dEq.addBoundaryCondition('cut',uv2dBndWall)
#uv2dEq.addBoundaryCondition('paste',uv2dBndWall)

# .-------------------------------.
# |     setup time integrator     |
# '-------------------------------'
t = slim3dTimeIntegratorPC(s)

rk = dgERK(e.eta2dEq, None, DG_ERK_EULER)
dtRK = rk.computeInvSpectralRadius(d.etaDof2d)
CFL = 0.7263
dt = CFL*dtRK
#dt = 0.2 # for convergence
if Msg.GetCommRank() == 0 :
  print('Runge-Kutta dt :' + str(dtRK) + ' CFL:' + str(CFL) + ' dt:' + str(dt))

t.setTimeStep(dt)
t.set3dTimeStepRatio(10)
t.setExportInterval(86400 /5)
t.setEndTime(86400 *2)
t.setExportAtEveryTimeStep(False)

## .-------------------------------.
# |     setup export options      |
# '-------------------------------'

# list of fields to export
export = []
export.append(dgIdxExporter(d.uvDof, odir + "/uv", True))
export.append(dgIdxExporter(d.etaDof2d, odir + "/eta"))

#initial export
for e in export :
  e.exportIdx(t.getExportCount(), t.getTime())

tic = time.time()
while t.getTime() < t.getEndTime() :
  t.advanceOneTimeStep()
  t.checkSanity()
  if t.checkExport() :
    Msg.Info("export %i" % t.getExportCount())
    for e in export :
      e.exportIdx(t.getExportCount(), t.getTime())
    Msg.Info("%5i time : %g  normuv %6.12e clock %.1fs" % (t.getIter(), t.getTime(), d.uvDof.norm(), time.time() - tic))
    normU = d.uvDof.norm()
    if normU > normUTolerance :
      print ('rest conservation is broken!!')
      Msg.Exit(-1)
  if t.checkFullExport() :
    t.exportAll()
t.terminate(0)

