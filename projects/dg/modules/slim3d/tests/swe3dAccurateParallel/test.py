import os, sys
from dgpy import *
from periodicMap import *
from extrudePeriodic import *

name = 'square'
os.system('rm *.msh')
#os.system('rm ' + name + '_*')
os.system('rm checkNorms.txt')


nPart = 1
inputMesh = name+'.msh'
mesh1 = name+'_'+str(nPart)+'.msh'
mesh2 = name+'_'+str(nPart)+'b.msh'
mesh3d = name+'_'+str(nPart)+'_3d.msh'

os.system('gmsh -2 '+name+'.geo')
os.system('ls')

os.system('cp ' + inputMesh + ' ' + mesh1)
def shiftOperation(node, iPerBound) :
  n = [node[0] - 2*9e5/60, node[1], node[2]]
  return n
cutTags = ["cut"]
pasteTags = ["paste"]
mapFilename = "periodicMesh.txt"
periodicMap(mesh1, mesh2, 1, shiftOperation, cutTags, pasteTags, mapFilename)

h = 1600
nbLayers = 15
zLayers = [0, 10, 25, 45, 100, 200, 500, 800, 1200, 1600]
def getLayers(e, v) :
  #return zLayers
  return [z * h /nbLayers for z in  range(nbLayers + 1)]

extrudePeriodic(mesh(mesh2), getLayers, mapFilename, mapFilename).write(mesh3d)

os.system('rundgpy danilov.py')

nPart = 8
mesh1 = name+'_'+str(nPart)+'.msh'
mesh2 = name+'_'+str(nPart)+'b.msh'
mesh3d = name+'_'+str(nPart)+'_3d.msh'

model = GModel()
model.load(inputMesh)
pOpt = meshPartitionOptions()
pOpt.setNumOfPartitions(nPart)
PartitionMesh(model, pOpt)
model.save(mesh1)
periodicMap(mesh1, mesh2, 1, shiftOperation, cutTags, pasteTags, mapFilename)
extrudePeriodic(mesh(mesh2), getLayers, mapFilename, mapFilename).write(mesh3d)

#os.system('mpirun -np ' + str(nPart) + ' xterm -e  rundgpy  --valgrind danilov.py ')
os.system('mpirun -np ' + str(nPart) + ' rundgpy danilov.py')

f = open("checkNorms.txt", 'r')
n1 = float(f.readline())
n8 = float(f.readline())

#print('%21.18e\n' % n1)
#print('%21.18e\n' % n8)

error = 0
if (abs(n1-n8) > 1e-13) :
  error = 1

if error == 0 :
  print("exit with success")
else :
  print("exit with failure")

os.system('rm -rf output')
sys.exit(error)
