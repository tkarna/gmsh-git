#ifndef _DG_CONSERVATION_RICHARDS_3D_H
#define _DG_CONSERVATION_RICHARDS_3D_H
#include "dgConservationLawFunction.h"

// ####################
//     COMMON STUFF
// ####################

class function;
class dgConservationLawRichardsCommon { /// TODO change using a namespace !
 public:
  class gradPsiTerm;
  class diffusiveFlux;
  class interfaceTerm;
  class interfaceDiffusivity;
  class surfaceFlux;
  class mainConductivity;

  static dgBoundaryCondition * newEvaporationBoundary(dgConservationLawFunction *claw,
                                                      const std::string tag,
                                                      const function *flux,
                                                      const function *state,
                                                      const std::vector<const function *> toReplace,
                                                      const std::vector<const function *> replaceBy,
                                                      const std::vector<const function *> toReplace2,
                                                      const std::vector<const function *> replaceBy2);
  static dgBoundaryCondition * newEvaporationBoundary(dgConservationLawFunction *claw,
                                                      const std::string tag,
                                                      const function *flux,
                                                      const function *state,
                                                      const std::vector<const function *> toReplace,
                                                      const std::vector<const function *> replaceBy) {
    std::vector<const function*> dummy;
    return newEvaporationBoundary(claw, tag, flux, state, toReplace, replaceBy, dummy, dummy);
  }
  static const function * newEvaporationBoundaryState(const function *flux, const function *pressure,
                                                      const function *atmWPressure, const function *oldState);
};

// ##################
//     IMPLICIT
// ##################

/**Richards equations for saturated/unsaturated flows
 * \f{eqnarray*} \frac{d\theta}{dt} + \nabla .(K \nabla(h+z)) &=& 0 \\
 * \theta &=& f_{\theta}(h) \\
 */

class dgConservationLawRichardsHform : public dgConservationLawFunction {
  const function *_KFunc, *_gFunc, *_CFunc, *_ipTerm, *_sourceFunc, *_diffusiveFlux, *_interfaceDiff, *_HFunc, *_gHFunc;
  bool _upWind;
  class gradPsiTerm;
  class interfaceTerm;
  class diffusiveFlux;
  class interfaceDiffusivity;
  class surfaceBC;
  class dgIPTermR;
 public:
  void setup();
  dgConservationLawRichardsHform(const function *K, const function *g, const function *s, const function *C);
  ~dgConservationLawRichardsHform();
  void setParam(const std::string key, const double value);
  void setGH(const function * gh) { _gHFunc = gh; }
  void setH(const function * h) { _HFunc = h; }
  dgBoundaryCondition * newSurfaceBC(const function * Ksurf, const function * hsoil, const function * hsurf, const function * xyz);
  function * newSurfaceFlux(const function *HSurfF,   const function *HSoilF,   const function *KSurfF,
                            const function *normalsF, const function *surfaceF, const function *minlF);  
};

// -----------------------------------
// Richards, system form => disabled
// -----------------------------------

/*
class dgConservationLawRichards : public dgConservationLawFunction {
  const function *_KFunc, *_fThFunc, *_gFunc, *_ipTerm, *_sourceFunc, *_diffusiveFlux, *_dvtFunc;
  bool _upWind;
  class gradPsiTerm;
  class interfaceTerm;
  class maxConvectiveSpeed;
  class diffusiveFlux;
  class constraint;
  class boundaryPressure;
  class dgIPTermR;
  class diffusivityTransformed;
  public:

  void setup();
  // Richards equation. \f$K\f$ is the tensorial diffusivity and \f$f_{\theta}\f$ is the retention curve relation
  dgConservationLawRichards(const function *K,const function *fTh,const function *dvt,const function *g,const function *s);
  ~dgConservationLawRichards();
  // Static method to create an instance with vertical gravity and any source term.
  static dgConservationLawRichards *newDgConservationLawRichards(const function *K,const function *fTh);
  void setParam(const std::string key,const double value);

};*/


// ##################
//     EXPLICIT
// ##################


class dgConservationLawRichardsExplicitTh : public dgConservationLawFunction {
  const function *_KFunc, *_hFunc, *_ghFunc, *_gFunc, *_ipTerm, *_sourceFunc,
                 *_diffusiveFlux, *_state, *_diffusivityScalar, *_fm1Th, *_eta;
  bool _upWind;
  std::vector<bool> _default;
  class constraintGradPsiTerm;
  class ipTermR;
 public:
  dgConservationLawRichardsExplicitTh(const function *K, const function *h, const function *gh,
                                      const function *state, const function *fm1Th, const function *eta);
  ~dgConservationLawRichardsExplicitTh();
  void setup();
  void setParam(const std::string key, const double value);
  void setFunction(const std::string key, const function *f);
  function * newSurfaceFlux(const function *HSurfF,   const function *HSoilF,   const function *KSurfF,
                            const function *normalsF, const function *surfaceF, const function *minlF);

};

class dgConservationLawRichardsExplicitThDiff : public dgConservationLawFunction {
  const function *_state, *_eta, *_fh, *_gfh, *_fm1, *_h;
  class ipTermR;
  class constraintGradPsiTerm;
  class surfaceFlux;
 public:
  dgConservationLawRichardsExplicitThDiff(const function *state, const function *eta, const function *fh, const function *gradfh,
                                                                                 const function *fm1, const function *h);
  ~dgConservationLawRichardsExplicitThDiff();
  function * newSurfaceFlux(const function *normalsF, const function * thSurfF);
  void setup();
};

// TODO (if possible) optimize the flux computation to make 1 evaluation for both th and h per time step
class dgConservationLawRichardsExplicitH : public dgConservationLawFunction {
  const function *_KFunc, *_sourceFunc, *_diffusiveFlux, *_gFunc, *_stateF, *_solFixF, *_CFunc, *_eta, *_ipTerm, *_diffusivityScalar;
  const function *_capillarCap, *_hForced, *_ghForced, *_stabilityFactor;
  bool _upWind, _massF;
  class dgIPTermR;
  class diffusivityMod;
  class factorMass;
  class interfStabilityFactor;
  class surfaceBC;
 public:
  dgConservationLawRichardsExplicitH(const function *K, const function *gravity, const function *state,
                                     const function *solFix, const function *CF, const function *eta);
  ~dgConservationLawRichardsExplicitH();
  void setup();
  inline void setSource(const function *sourceTerm){ _sourceFunc = sourceTerm; }
  const function * getAppliedC();
  void setParam(const std::string key, const double value);
  static dgConservationLawRichardsExplicitH * newForced(const function *K, const function *gravity, const function *state,
                                     const function *solFix, const function *CF, const function *eta,
                                     const function *h, const function *gh, bool massFactor=true);
  function * newSurfaceFlux(const function *HSurfF,   const function *HSoilF,   const function *KSurfF,
                            const function *normalsF, const function *surfaceF, const function *minlF);
  dgBoundaryCondition * newSurfaceBC(const function * hsoil, const function * hsurf);
};



#endif
