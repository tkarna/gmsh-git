from SWNI import *
from utilities import *
from Common import *
import time


try : os.mkdir("output")
except: 0
outputname = "SWNI1D"
mesh = 'line_converging'
genMesh(mesh, 1, 1)


#############
# FUNCTIONS #
#############


CFunctions = ''' 
#include "fullMatrix.h"
#include "function.h"
extern "C" {

void analyt (dataCacheMap *m, fullMatrix<double> &out, fullMatrix<double> &xyz) {
  for (size_t i = 0; i < out.size1(); i++) {
    double x = xyz(i, 0);
    out.set(i, 0, 1.5 * exp(-1.5 * (x-2) * (x-2)) );
  }
}

}
'''

tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CFunctions, tmpLib);
Msg.Barrier()

##################
# INITIALISATION #
##################

# definition of land surface

lsm = LandSurfaceMap()
surface = landSurfManning(1.)
lsm.add("domain", surface)

hasworked = True

for i in range(2):
  # construction of the law
  if(i==0):
    print ("DG explicit")
    Soptions = SWNIOptions(isImplicit=True, controlVolumeFE=False, setVerb=1, DMax=10, setMaxIt=100, isDG=True)
    r000 = 1.42138449166
    rm101 = 0.418966194707
  else:
    print ("CG implicit CVFE")
    Soptions = SWNIOptions(isImplicit=True, controlVolumeFE=True, setVerb=1, DMax=10, setMaxIt=100, isDG=False)
    r000 = 1.41814780287
    rm101 = 0.415768397568
  S = SWNI(mesh + '.msh', finalizeInitialisation=False, options=Soptions)
  S.finalizeInit(lsm, source=None)

  # boundary conditions

  C0 = functionConstant(0)
  S.bndNeumann(C0, ['lB', 'rB'])

  # initial conditions

  analytic = functionC(tmpLib, "analyt", 1, [S.XYZ])
  S.setH(analytic)

  # time and exports

  h = slimIterateHelper()
  h.setTime("0:.0025:5")
  h.setExport(".5")
  h.setFileName("output/" + outputname)
  h.addExport(S.H, "newh")
  h.addExport(S.H, analytic, "analytic")
  h.addExport(S.H, S.nl, "newnlTerm")
  h.exportBin(True)
  h.setNbMaxIter(1000000)
  printEach = 100
  i=0; x = time.clock(); t=0; dt=1; norm=0; normH=0; nbIter=1
  nbIMin = 1000; nbIMax = 0; nbIMean = 0

  #h.importAll(28, 2.8)


  glbMass = S.getGlobalMass()
  if (Msg.GetCommRank()==0) :
    print ('|IT|', 0, '|DT|', "--", '|t|', 0, '|CPU|', '%.1e' % (0), '|Mass|', '%0.16g' % (glbMass), '|NORM|', "--")

  while (not h.isTheEnd()):
    i = i + 1
    dt = h.getNextTimeStepAndExport()
    t = h.getCurrentTime()

    nbIter = S.iterate(dt, t)
    nbIMin = min(nbIMin, nbIter)
    nbIMax = max(nbIMax, nbIter)
    nbIMean = nbIMean + nbIter
  #  if(i==1): exit()

    if (i % printEach == 0) :
      glbMass = S.getGlobalMass()
      nbIMean = float(nbIMean) / printEach;
      if (Msg.GetCommRank() == 0) :
        print ('|IT|', '%6d' % (i), '|DT|', dt, '|t|','%.2e' % (t), '|CPU|', '%.1e' % (time.clock()-x), '|Mass|', '%21.16g' % (glbMass), '|NbIter|', '%4g<%4g<%4g' % (nbIMin, nbIMean, nbIMax))
      nbIMin = 1000; nbIMax = 0; nbIMean = 0

    if (isnan(nbIter) or not nbIter) :
      glbMass = S.getGlobalMass()
      if(Msg.GetCommRank() == 0) :
        if(isnan(nbIter)):
          print('NAN, abording . last iter is')
        else:
          print('not converged, abording . Last iter is')
        print ('|IT|', i, '|DT|', dt, '|t|', t, '|CPU|', '%.1e' % (time.clock()-x), '|Mass|', '%21.16g' % (glbMass), '|NORM|', '%21.16g' % (norm))
      h.exportAll()
      break

  glbMass = S.getGlobalMass()

  if(abs(glbMass - 2.170224828286662) > 1e-12):
    print ("mass leakage : ", glbMass, "!=", 2.170224828286662)
    exitWithFailure()

  evaluator = dgFunctionEvaluator(S.gc, S.HF)
  result = fullMatrixDouble(1,1)
  evaluator.compute(0.,0.,0.,result)
  if(abs(result(0,0) - r000) > 1e-8):
    print ("result different than previously at 0,0,0: ", result(0,0), "!=", r000)
    hasworked = False
  evaluator.compute(-1.,0.,1.,result)
  if(abs(result(0,0) - rm101) > 1e-8):
    print ("result different than previously at -1,0,1: ", result(0,0), "!=", rm101)
    hasworked = False

if(hasworked):
  exitWithSuccess()
else:
  exitWithFailure()

