scale = 4;
lr = .01 * scale;


Point(1) = {-scale, 0, scale, lr};
Point(2) = { 0., 0,  0 , lr};
Point(3) = { scale, 0, scale, lr};
Line(1) = {1, 2};
Line(2) = {2, 3};
Physical Line("domain") = {1,2};
Physical Point("lB") = {1};
Physical Point("rB") = {3};
