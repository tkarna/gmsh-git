from Richards import *
import utilities as u
from Common import *
import time
import math
import string
import gmshPartition

# input / output
# ==============

try : os.mkdir("output"); 
except: 0;
outputname = "A1HydrusMulti"
mesh = 'columnH1_1D'
genMesh(mesh, 1, 1)
mesh = gmshPartition.simple(mesh + '.msh')

# Main parameters
# ===============

runs = {
        ('Implicit','Continuous'),
        ('Implicit','Discontinuous'),
#        ('Explicit','Continuous',1,1),
        ('Explicit','Discontinuous',1,1),
#        ('Explicit','Discontinuous',2,2)
#        ('Explicit','Discontinuous',5,5)
#        ('Explicit','Discontinuous',6,6)
       };

maxDTI = 1
maxDTE = 1

upBnd = functionConstant(0)

finalTime = 90 * 60
exportSteps = '360'
#exportSteps = '120:2,5'

success = True
expectedMass = 0.1503

# Soils
# =====

sm = SoilMap()
soil_sand = soilVGm(4.1, 1.964, 7.22e-6, 0.35, 0.02, 6.95e-6, 0.02, 0.35, 0.2875)
sm.add("volume", soil_sand)

#interpolator = None
interpolator = 50  # designed to speed-up computation of soil functions (less precision)


# initial conditions
# ==================

def initialValues(val, xyz):
  for i in range(0, xyz.size1()):
    x = xyz.get(i, 0)
    y = xyz.get(i, 1)
    z = xyz.get(i, 2)
    val.set (i, 0, - 1.5)
    #val.set (i, 0, - 0.0)

if (Msg.GetCommRank()==0):
  print ('\nruns:')
  for run in runs:
    print ('\t',run)
  print()

implStr = ['i', 'implicit'];
explStr = ['e', 'explicit'];
contStr = ['c', 'continuous'];
discStr = ['d', 'discontinuous'];

for run in runs:
  if(type(run[1]) is str and run[1].lower() in contStr):
    spatial = 'Continuous'
    isContinuous = True
  elif(type(run[1]) is str and run[1].lower() in discStr):
    spatial = 'Discontinuous'
    isContinuous = False
  else:
    Msg.Fatal('bad format for runs')

  Roptions = RichardsOptions(isCG=isContinuous, interpolator=interpolator, homogeneousLimiter=False) # warn: shld wrks wthout homLim
  if  (type(run[0]) is str and run[0].lower() in implStr):
    R = RichardsImplicit(mesh, sm, options=Roptions)
    temporal = 'Implicit'
    isImplicit = True
  elif(type(run[0]) is str and run[0].lower() in explStr):
    R = RichardsExplicit(mesh, sm, options=Roptions)
    n, n, n, k = run
    temporal = 'Explicit ('+str(n)+','+str(k)+')'
    isImplicit = False
  else:
    Msg.Fatal("bad format for runs")
 
  if (Msg.GetCommRank()==0): print ('\nRun: '+temporal, spatial+'\n')

  # Bnd conditions
  # ==============

  C0 = functionConstant(0)
  R.bndNeumann(C0, ['north', 'east', 'south', 'west', 'bottom'])
  R.bndDirichlet(upBnd, 'top')
  #R.bndNeumann(C0, 'top')


  h = slimIterateHelper()
  h.setInitialDate(0)
  h.setFinalDate(finalTime)
  h.setExport(exportSteps)
  #h.setExport('120:1597,1598,1599,1600,1601,1602,1603')
  #h.setExportStep(1)
  nbSubStr = temporal[0] + spatial[0]
  if(not isImplicit): nbSubStr = nbSubStr + '%d_%d' % (n, k)
  h.setFileName("output/" + outputname)
  h.addExport(R.H, nbSubStr + "h")
  h.addExport(R.W, nbSubStr + "th")
  if(not isImplicit): h.addExport(R.H, R.state, nbSubStr + "st")
  h.exportBin(True)
  h.printExport(True)
  h.setNbMaxIter(10000000)

  printEach = 100
  if(isImplicit) : h.setMaxDt(maxDTI)
  else           : h.setMaxDt(maxDTE)
  i = 0; x = time.clock();
  nbIMin = 1000; nbIMax = 0; nbIMean = 0

  INI = functionPython(1, initialValues, [R.XYZ])
  R.seth(INI)

  gmass = R.getGlobalMass()
  if(Msg.GetCommRank() == 0):
    print ('|Mass|', '{0:21.16e}'.format(gmass))

  while (not h.isTheEnd()):
    i = i + 1
    dt = h.getNextTimeStepAndExport()
    t = h.getCurrentTime()

    if(isImplicit):
      nbSub = R.iterate(dt, t)
      nbIMin = min(nbIMin, nbSub)
      nbIMax = max(nbIMax, nbSub)
      nbIMean = nbIMean + nbSub
    else:
      nbSub = R.iterate(dt, t, n, k)

    if (i % printEach == 0) :
      gmass = R.getGlobalMass()
      if (Msg.GetCommRank() == 0) :
        sys.stdout.write('|IT| {0:6d} |DT| {1} |t| {2:.3e} |CPU| {3:.1e} '.format(i, dt, t, time.clock()-x))
        if(isImplicit):
          nbIMean = float(nbIMean) / printEach;
          sys.stdout.write('|NWits| {0:2d} <{1:4.1f} <{2:2d} '.format(nbIMin, nbIMean, nbIMax))
          nbIMin = 1000; nbIMax = 0; nbIMean = 0
        print ('|Mass|', '{0:21.16e}'.format(gmass))
      if(abs(gmass) > 1e10):
        h.exportAll()
        exit()
        
    if(nbSub == 0):
      exit() # stops when newton dont converge

  gmass = R.getGlobalMass()
  if(Msg.GetCommRank() == 0) :
    sys.stdout.write('|IT| {0:6d} |DT| {1} |t| {2:.2e} |CPU| {3:.1e} '.format(i, dt, t, time.clock()-x))
    print ('|Mass|', '{0:21.16e}'.format(gmass))
  
  if(abs(gmass - expectedMass) > 1e-3):
    success = False
    u.printOnce("mass check failed for this run: %.16e != %.16e\n" % (gmass, expectedMass));
  else:
    u.printOnce("mass check passed with success !\n")

if(Msg.GetCommRank() == 0) :
  try : os.system("rm -r ./*tmp*"); 
  except: 0;

  if(success == True):
    u.printOnce("Exit with success !\n")
    exitWithSuccess()
  else:
    u.printOnce("Exit with failure !\n")
    exitWithFailure()


