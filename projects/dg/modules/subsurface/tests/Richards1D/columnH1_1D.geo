size = .6;
lc = size / 30 * 0.999999;


Point(1) = {0, 0, 0, lc};
Point(2) = {0, 0, -size, lc};
Line(1) = {2, 1};

Physical Line("volume") = {1};
Physical Point("top") = {1};
Physical Point("bottom") = {2};



