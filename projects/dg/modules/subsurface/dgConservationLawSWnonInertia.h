#ifndef _DG_CONSERVATION_SW_NON_INERTIA_H
#define _DG_CONSERVATION_SW_NON_INERTIA_H
#include "dgConservationLawFunction.h"
class function;


/**
 * Shallow waters without inertia terms, also known as Diffusive Wave approximation
 *  {\partial H\over\partial t} = \nabla\cdot(D \nabla(H+b))
 *  H = water layer height, b = bathymetry, D = diffusion (which needs to be limited)
 *
 *  nonLinearTerm = H^{3/2} \sqrt{g/G}, with G=gn^2H^{-1/3} (Manning), G=g/C^2 (Chezy), G=f/8 (Darcy-W)
 *  => use nlFactor in a landSurface object (dgTagToFunction)
 */

class dgConservationLawSWnonInertia : public dgConservationLawFunction {
  const function *_gBathFunc, *_nlFunc, *_source, *_gh, *_bath, *_mwdFunc;
  class gradPsiTerm;
  class interfaceTerm;
  class interfaceTermBnd;
  class interfaceSlopeTermBnd;
  class integralTerm;
  double _Dmax, _sqrtSkip, _sqrtSkipA, _sqrtSkipB;
  bool _initialized, _clipped, _interfaceTermNodal;
  const function *_volumeTermNodal, *_volumeTermIntegral, *_volumeFunction, *_height;
public:
  void setup();
  void setDmax(double dmax);
  inline const double getDmax() const { return _Dmax; }
  inline const double getSqrtSkip() const { return _sqrtSkip; }
  inline const double getSqrtSkipA() const { return _sqrtSkipA; }
  inline const double getSqrtSkipB() const { return _sqrtSkipB; }
  dgConservationLawSWnonInertia(const function * gBathymetry, const function * nl=NULL, const function * source=NULL, const function * gh=NULL, const function * bath=NULL);
  void setSource(const function * source);
  void setNLTerm(const function * nl) { _nlFunc = nl; }
  void setMobileWaterDepth(const function * mwd) { _mwdFunc = mwd; }
  void setClipped(bool clip) { _clipped = clip; }
  void setControlVolumes(const function * fN, const function *fI, const function *z, bool interface=true)
                        { _volumeTermNodal = fN; _volumeFunction = fI; _height = z; _interfaceTermNodal = interface; }
  ~dgConservationLawSWnonInertia();
//  function * getNewInterfaceTermBnd(const function * gBathF, const function * nlF);
  function * getNewInterfaceSlopeBnd(const function * gBathF, const function * nlF, const function *gHF);
  dgTermAssembler * getTerm(const dgGroupOfElements &group) const;
  dgFaceTermAssembler *getFaceTerm(const dgGroupOfFaces &group) const;
};

class dgConservationLawSWkinematic : public dgConservationLawFunction {
  const function *_gBathFunc, *_nlFunc, *_source;
  class gradPsiTerm;
  class interfaceTerm;
  class interfaceTermBnd;
  double _Dmax;  
  bool _initialized, _clipped;
public:
  void setup();
  inline void setDmax(double dmax) { _Dmax = dmax; }
  inline double getDmax() { return _Dmax; }
  void setClipped(bool clip) { _clipped = clip; }
  void setSource(const function * source);
  void setNLTerm(const function * nl) { _nlFunc = nl; }
  dgConservationLawSWkinematic(const function * gBathymetry, const function * nl, const function * source);
  ~dgConservationLawSWkinematic();
  function * getNewInterfaceTermBnd(const function * gBathF, const function * nlF);
};

#endif
