#ifndef _DG_TAG_TO_FUNCTION_H_
#define _DG_TAG_TO_FUNCTION_H_

//#include <boost/static_assert.hpp>
#include "fullMatrix.h"
#include "function.h"
#include "dgGroupOfElements.h"
//#include <sys/time.h>  //TEMP !!
#include "functionLookupTable.h"


/* General mapping of functions depending on local properties defined by physical tags.
** A set of function depending of these properties could be defined.
** Use : once child of dgTTFObj created (as above), use like that (here for soil):
**
** // create a map
** dgTTFMap<soil> map;
**
** // insert default input functions (here h and th)
** map.addDefaultInput("pressure", h);
** map.addDefaultInput("waterContent", th);
**
** // creating local properties and add them to the map
** map.add("tagSoila", soilVG(prop1a,prop2a,prop3a,prop4a,...));
** map.add("tagSoilb", soilVG(prop1b,prop2b,prop3b,prop4b,...));
** map.add("tagSoilc", soilVG(prop1c,prop2c,prop3c,prop4c,...));
** 
** // when all the volume physical tags are completed, get functions
** function * retention = map.getFunction("retention");
** function * inv_condu = map.getFunction("conductivity_inv");
**
** // use functions as usually ...
**
**/


class dgTTFObj;
typedef void (dgTTFObj::*functionGeneric) (fullMatrix<double> &, const std::vector<fullMatrix<double> > &);

// dtRRFObj => mother class (declare virtual functions) => child class (implements functions with right params)
class dgTTFObj {
  int _id;
public:
  virtual functionGeneric getPtrFunction(std::string fName) {return functionGeneric();} //=> get the value of the static mother class (to be same for childs)
  virtual std::vector<std::string> getInputVariables(std::string fName) {return std::vector<std::string>();} //=> get the value of the static mother class (to be same for childs)
  virtual int getNbFields(std::string fName) {return 0;}
  virtual std::set<double> getInterpPoints(std::string fName, int nbInterpPoints) {return std::set<double>();} //TODO

  dgTTFObj(){
    static int id_increment = 0;
    _id = id_increment++;
  }
  virtual ~dgTTFObj(){}
  inline int getID() { return _id; }
};

template <typename T>
class dgTTFMap {
private:
  std::map<std::string,T*> _mapO;          //physical TAG <-> obj with specific properties
  std::map<std::string,function*> _mapDIF; //name of default INPUT functions <-> the function pointer
  bool isInit;

  class functionTTF : public function {
    void(T::*_func)(fullMatrix<double> &, const std::vector<fullMatrix<double> > &) ;
    const std::map<std::string,T*> &_mapO;
    std::vector<fullMatrix<double> > _IM; //INPUT matrix(es)
  public:
    functionTTF(const std::map<std::string,const function*> &mapIF, const std::map<std::string,function*> &mapDIF,
                const std::string &nameF, const std::map<std::string,T*> &mapO, int nb): _mapO(mapO), function(nb) {
      std::vector<std::string> names = (_mapO.begin()->second)->getInputVariables(nameF);
      _IM.resize(names.size());
      _func = (void(T::*)(fullMatrix<double> &, const std::vector<fullMatrix<double> > &)) ((_mapO.begin())->second)->getPtrFunction(nameF);
      for(int i = 0; i < names.size(); i++) {
        std::map<std::string,const function*>::const_iterator it = mapIF.find(names[i]);
        if(it != mapIF.end())
          setArgument(_IM[i], it->second);
        else {
          std::map<std::string,function*>::const_iterator itD = mapDIF.find(names[i]); // default values
          if(itD != mapDIF.end())
            setArgument(_IM[i], itD->second);
          else
            Msg::Fatal("Input function not specified : '%s'", names[i].c_str());
        }
      }
    }
    void call(dataCacheMap *m, fullMatrix<double> &out) {
      std::string tag = m->getGroupOfElements()->getPhysicalTag();
      if(tag == "")
        Msg::Fatal("Untagged group ! (in dgTagToFunction)");
      typename std::map<std::string,T*>::const_iterator it = _mapO.find(tag);
      if(it == _mapO.end())
        Msg::Fatal("Unknown tag: '%s' in dgTTF", tag.c_str());
      ( (it->second)->*(_func) )( out , _IM ) ;  // object->method(out, arrayOfInput)
    }
  };
  class functionID : public function {
    const std::map<std::string,T*> &_mapO;
   public:
    functionID(const std::map<std::string,T*> &mapO) : _mapO(mapO), function(1) {}
    void call(dataCacheMap *m, fullMatrix<double> &out) {
      std::string tag = m->getGroupOfElements()->getPhysicalTag();
      if(tag == "")
        Msg::Fatal("Untagged group ! (in dgTagToFunction)");
      typename std::map<std::string,T*>::const_iterator it = _mapO.find(tag);
      if(it == _mapO.end())
        Msg::Fatal("Unknown tag: '%s' in dgTTF", tag.c_str());
      for(int i = 0; i < out.size1(); i++)
        out.set(i, 0, (it->second)->getID() );
    }
  };
  // specific code for Lookup tables with physicalTag-dependent functions
  enum LTTYPE { NORMAL , INVERT , DERIV };
  class functionTTF_LT : public function {
    const std::map<std::string, T*> &_mapO;
    functionTTF _originalFunction;
    fullMatrix<double> _IM; //INPUT matrix
    std::map<std::string, functionLT*> _funcLT;
   public:
    functionTTF_LT(const std::map<std::string, const function*> &mapIF, const std::map<std::string,function*> &mapDIF, const std::string &nameF,
                   const std::map<std::string,T*> &mapO, int nb, int nbInterpPoints, dgGroupCollection *gc, functionInterpolator *ip,
                   LTTYPE lttype=NORMAL, const function *y=NULL) : _mapO(mapO), _originalFunction(mapIF, mapDIF, nameF, mapO, nb), function(nb) {
      std::vector<std::string> names = (_mapO.begin()->second)->getInputVariables(nameF);
      if(names.size() != 1)
        Msg::Fatal("The lookup table option concern functions with only one variable !");

      const function *x;
      std::map<std::string,const function*>::const_iterator itIF = mapIF.find(names[0]);
      if(itIF != mapIF.end())
        x = itIF->second;
      else {
        std::map<std::string,function*>::const_iterator itDIF = mapDIF.find(names[0]); // default values
        if(itDIF != mapDIF.end())
          x = itDIF->second;
        else
          Msg::Fatal("Input function not specified : '%s'", names[0].c_str());
      }
      if(lttype == INVERT) {
        if(y == NULL)
          Msg::Fatal("bug in invert input function");
        setArgument(_IM, y);
      } else {
        setArgument(_IM, x);
      }

      typename std::map<std::string, T*>::const_iterator it;
      for(it = _mapO.begin(); it != _mapO.end(); it++) {
        std::set<double> nexus = it->second->getInterpPoints(nameF, nbInterpPoints);
        std::string physicalTag = it->first;
        dgGroupOfElements *elGroup = gc->getElementGroup(physicalTag); //goal: get 1 group with the good tag for init
        functionLTTree * funcTemp = new functionLTTree(&_originalFunction, x, gc, ip, nexus, NULL, elGroup);
        switch(lttype) {
          case NORMAL:
            _funcLT[physicalTag] = funcTemp;
            break;
          case INVERT:
            _funcLT[physicalTag] = funcTemp->newInverse(y, nexus, elGroup);
            delete funcTemp;
            break;
          case DERIV:
            _funcLT[physicalTag] = funcTemp->newDerivative();
            delete funcTemp;
        }
      }
    }
    void call(dataCacheMap *m, fullMatrix<double> &out) {
      std::string tag = m->getGroupOfElements()->getPhysicalTag();
      if(tag == "")
        Msg::Fatal("Untagged group ! (in dgTagToFunction)");
      typename std::map<std::string, functionLT*>::const_iterator it = _funcLT.find(tag);
      if(it == _funcLT.end())
        Msg::Fatal("Unknown tag: '%s' in dgTTF", tag.c_str());
      it->second->call2(m, out, _IM); //shortcuts the classical function::call
    }
  };

public:
  dgTTFMap() : isInit(false) {}
  ~dgTTFMap() {
    /*
    for(typename std::map<std::string,T*>::iterator it = _mapO.begin(); it != _mapO.end(); it++) {
      if(it->second) delete (it->second);
    }
    */
  }
  void addDefaultInput(std::string nameFunc, function* fun) {
    _mapDIF[nameFunc] = fun;
  }
  void add(std::string tag, T* obj) {
    if(isInit)
      Msg::Fatal("first use addTTFObj, then use getFunction !");
    _mapO[tag] = obj;
  }
  void add(std::vector<std::string> tag, T* obj) {
    if(isInit)
      Msg::Fatal("first use addTTFObj, then use getFunction !");
    for(std::vector<std::string>::iterator it = tag.begin(); it != tag.end(); it++)
      _mapO[*it] = obj;
  }
  function * getFunction(std::string nameFunc) {
    std::map<std::string, const function*> empty;
    return getFunction(nameFunc, empty);
  }
  function * getFunction(std::string nameFunc, std::vector<std::string> label, std::vector<const function*> func) {
    if(label.size() != func.size())
      Msg::Fatal("the getFunction(name,vec,vec) have to possess same lenghts for the vec !");
    std::map<std::string,const function*> map;
    for(int i = 0; i < label.size(); i++)
      map[label[i]] = func[i];
    return getFunction(nameFunc, map);
  }
  function * getFunction(std::string nameFunc, std::map<std::string, const function*> inputFuns) {
    if(_mapO.size() == 0)
      Msg::Fatal("Before getFunction, make use of the add function of the map !");
    int nbFields = ((_mapO.begin())->second)->getNbFields(nameFunc);
    function *fun = new functionTTF(inputFuns, _mapDIF, nameFunc, _mapO, nbFields);
    isInit = true;
    return fun;
  }
  // specific code for Lookup tables with physicalTag-dependent functions
  function * getFunctionLT(std::string nameFunc, int nbInterpPoints, dgGroupCollection *gc, functionInterpolator *ip) {
    std::vector<std::string> label;
    std::vector<const function*> func;
    return getFunctionLT(nameFunc, nbInterpPoints, gc, ip, label, func);
  }
  function * getFunctionLT(std::string nameFunc, int nbInterpPoints, dgGroupCollection *gc, functionInterpolator *ip,
                           std::vector<std::string> label, std::vector<const function*> func) {
    if(_mapO.size() == 0)
      Msg::Fatal("Before getFunction, make use of the add function of the map !");
    std::map<std::string,const function*> map;
    for(int i = 0; i < label.size(); i++)
      map[label[i]] = func[i];
    int nbFields = ((_mapO.begin())->second)->getNbFields(nameFunc);
    function *fun = new functionTTF_LT(map, _mapDIF, nameFunc, _mapO, nbFields, nbInterpPoints, gc, ip, NORMAL);
    isInit = true;
    return fun;
  }
  function * getFunctionLT_invert(std::string nameFunc, int nbInterpPoints, dgGroupCollection *gc, functionInterpolator *ip, const function *x) {
    std::vector<std::string> label; std::vector<const function*> func;
    return getFunctionLT_invert(nameFunc, nbInterpPoints, gc, ip, x, label, func);
  }
  function * getFunctionLT_invert(std::string nameFunc, int nbInterpPoints, dgGroupCollection *gc, functionInterpolator *ip, 
                                  const function *x, std::vector<std::string> label, std::vector<const function*> func) {
    if(_mapO.size() == 0)
      Msg::Fatal("Before getFunction, make use of the add function of the map !");
    std::map<std::string,const function*> map;
    for(int i = 0; i < label.size(); i++)
      map[label[i]] = func[i];
    int nbFields = ((_mapO.begin())->second)->getNbFields(nameFunc);
    function *fun = new functionTTF_LT(map, _mapDIF, nameFunc, _mapO, nbFields, nbInterpPoints, gc, ip, INVERT, x);
    isInit = true;
    return fun;
  }
  function * getFunctionLT_deriv(std::string nameFunc, int nbInterpPoints, dgGroupCollection *gc, functionInterpolator *ip) {
    std::vector<std::string> label; std::vector<const function*> func;
    return getFunctionLT_deriv(nameFunc, nbInterpPoints, gc, ip, label, func);
  }
  function * getFunctionLT_deriv(std::string nameFunc, int nbInterpPoints, dgGroupCollection *gc, functionInterpolator *ip, 
                                 std::vector<std::string> label, std::vector<const function*> func) {
    if(_mapO.size() == 0)
      Msg::Fatal("Before getFunction, make use of the add function of the map !");
    std::map<std::string,const function*> map;
    for(int i = 0; i < label.size(); i++)
      map[label[i]] = func[i];
    int nbFields = ((_mapO.begin())->second)->getNbFields(nameFunc);
    function *fun = new functionTTF_LT(map, _mapDIF, nameFunc, _mapO, nbFields, nbInterpPoints, gc, ip, DERIV);
    isInit = true;
    return fun;
  }
  function * getIDFunction() {
    function *fun = new functionID(_mapO);
    isInit = true;
    return fun;
  }
};



// SOILS

//mother class
class soil : public dgTTFObj {
protected:
  double _ths;
  double _thr;
  double _ksat;  // m/s
public:
  soil(){}
  virtual ~soil(){}
  // output functions
  functionGeneric getPtrFunction(std::string nameF);
  // input variables
  virtual std::vector<std::string> getInputVariables(std::string nameF);
  // nb output variables
  int getNbFields(std::string nameF);

  virtual void retention(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input) = 0;
  virtual void retention_inv(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input) = 0;
  virtual void dRetention(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input) = 0;
  virtual void conductivity(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input) = 0;
  virtual void conductivity1(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input) = 0;
  virtual void conductivity_inv(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input) = 0;
  virtual void dConductivity(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input) = 0;
  virtual void airEntryP(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input) = 0;
  virtual void airEntryWC(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input) = 0;
  virtual void valAtMaxDRetention(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input) = 0;
  void state_eta(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
  void state_eta2(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
  void state_eta3(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
  void state(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
  void state_history(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
  void ths(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
  void thr(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
  void ks(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
};

//Van Genuchten child class
class soilVG : public soil {
  double _alpha; // 1/m
  double _beta;
  double _l;
  double _nu;
  double _ss;
  double _hcut;
  double _thcut;
  double _valAtMaxDRetention;
  double findRoot(double init);
  void setCut();
  inline double retention_func(double h);
  // optimal interpolations
  std::set<double> getInterpPoints(std::string fName, int nbInterpPoints);
public:
  void set(double a, double b, double k, double ts, double tr) {
    _alpha = a ;   _beta = b;  _ksat = k;  _ths = ts;  _thcut = _ths;
    _hcut  = 0.;   _thr = tr;  _l = .5;    _ss = 0.;   _nu = 1.0 - 1.0 / _beta;
    _valAtMaxDRetention = - pow(_nu, 1. / _beta) / _alpha;
  }
  void set(double a, double b, double k, double ts, double tr, double l) { set(a, b, k, ts, tr); _l = l; }
  soilVG() { set(3, 2, 1e-6, .4, 0); }
  soilVG(double a, double b, double k, double ts, double tr) { set(a, b, k, ts, tr); }
  soilVG(double a, double b, double k, double ts, double tr, double l) { set(a, b, k, ts, tr, l); }
  ~soilVG(){}

  void retention(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
  void retention_inv(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
  void dRetention(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
  void conductivity(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
  void conductivity1(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
  void conductivity_inv(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
  void dConductivity(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
  void airEntryP(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
  void airEntryWC(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
  void valAtMaxDRetention(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);

  void setSatSlope(double Ss) { _ss = Ss; setCut(); }
};

//modified Van Genuchten child class
class soilVGm : public soil {
  double _alpha; // 1/m
  double _beta;
  double _l;
  double _nu;
  double _kk;    // m/s
  double _tha;
  double _thm;
  double _thk;
  double _hs;    // m
  double _hk;    // m
  double _valAtMaxDRetention;
  inline double retention_func(double h);
  // optimal interpolations
  std::set<double> getInterpPoints(std::string fName, int nbInterpPoints);
public:
  void set(double a,  double b,  double ks, double ts, double tr,
           double kk, double ta, double tm, double tk, double l) {
    _alpha = a;  _beta = b;  _ksat = ks;  _ths = ts;  _thr = tr;
    _kk    = kk; _tha  = ta; _thm  = tm;  _thk = tk;  _l   = l;
    _nu = 1.0 - 1.0 / _beta;
    _hs = -pow(pow((_thm - _tha) / (_ths - _tha), 1.0 / _nu) - 1.0, 1. / _beta) / _alpha;
    _hk = -pow(pow((_thm - _tha) / (_thk - _tha), 1.0 / _nu) - 1.0, 1. / _beta) / _alpha;
    _valAtMaxDRetention = - pow(_nu, 1. / _beta) / _alpha;
  }
  void set(double a, double b, double k, double ts, double tr) {
    set(a, b, k, ts, tr, k, tr, ts, ts, 0.5); //same as not modified VG
  }
  soilVGm() {	set(3, 2, 1e-6, .3, 0); }
  soilVGm(double a, double b, double ks, double ts, double tr) {
    set(a, b, ks, ts, tr);
  }
  soilVGm(double a,  double b,  double ks, double ts, double tr,
          double kk, double ta, double tm, double tk, double l = .5) {
    set(a, b, ks, ts, tr, kk, ta, tm, tk, l);
  }
  ~soilVGm(){}

  void retention(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
  void retention_inv(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
  void dRetention(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
  void conductivity(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
  void conductivity1(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
  void conductivity_inv(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
  void dConductivity(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
  void airEntryP(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
  void airEntryWC(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
  void valAtMaxDRetention(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
};


// SHALLOW WATERS

//mother class
class landSurface : public dgTTFObj {
protected:
//  double _ths;
public:
  landSurface(){}
  virtual ~landSurface(){}
  // output functions
  functionGeneric getPtrFunction(std::string nameF);
  // input variables
  virtual std::vector<std::string> getInputVariables(std::string nameF);
  // nb output variables
  int getNbFields(std::string nameF);
  // optimal interpolations
  std::set<double> getInterpPoints(std::string fName, int nbInterpPoints) { Msg::Fatal("interpolations for landsurface not impl. !"); return std::set<double>(); }

  virtual void bottomFactor(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input) = 0;
  virtual void coeff(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input) = 0;
  virtual void nlFactor(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input) = 0;
};

class landSurfManning : public landSurface {
private:
  double _n;
  double _n2;
public:
  landSurfManning(double n) : _n(n) { _n2 = _n * _n; }
  void bottomFactor(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
  void coeff(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
  void nlFactor(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
};

class landSurfChezy : public landSurface {
private:
  double _C;
  double _C2;
public:
  landSurfChezy(double C) : _C(C) { _C2 = _C * _C; }
  void bottomFactor(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
  void coeff(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
  void nlFactor(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
};

class landSurfWeibach : public landSurface {
private:
  double _fact;
  double _fact2;
public:
  landSurfWeibach(double f) { _fact2 = 9.80665 * 8. / f; _fact = sqrt(_fact2); }
  landSurfWeibach(double f, double g) { _fact2 = g * 8. / f; _fact = sqrt(_fact2); }
  void bottomFactor(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
  void coeff(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
  void nlFactor(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input);
};

#endif
