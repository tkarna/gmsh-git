#include "dgNewtonKrabbenhoft.h"
#include "functionGeneric.h"
#include "dgIntegrationMatrices.h"
#include "dgConservationLaw.h"

class dgNewtonKrabbenhoft::replacedFunction : public function {
  functionReplace _yReplace;
  fullMatrix<double> _toReplace, _solution, &_replaceBy;
public:
  replacedFunction(const dgConservationLaw *claw, const function *toRepF, const function *f, fullMatrix<double> &replaceBy) : 
                  function(claw->getNbFields()), _replaceBy(replaceBy) {
    _yReplace.addChild();
    addFunctionReplace(_yReplace);
    _yReplace.replace(_toReplace, toRepF);
    _yReplace.get(_solution, f);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    for(int i = 0; i < _toReplace.size1(); i ++)
      for(int j = 0; j < _toReplace.size2(); j ++)
        _toReplace(i, j) = _replaceBy(i, j);
    _yReplace.compute();
    for(int i = 0; i < val.size1(); i++)
      for(int j = 0; j < val.size2(); j++)
        val(i, j) = _solution(i, j);
  }
};

dgNewtonKrabbenhoft::dgNewtonKrabbenhoft (dgDofManager &dof, const dgConservationLaw &law, const function *w, const function *f_w, const function *dwds, const function *wTol, const function *w_field_func) :
  dgNewton(dof, law, false),
  _dwds(dwds),
  _wTol(wTol),
  _w(w),
  _wIni(dof.getGroups(), 1),
  _currentW(dof.getGroups(), 1),
  _sparsityPatternFilled(false)
{
//  _deltaW = functionMinusNew(_w, _wIni.getFunction());
  _deltaW = functionMinusNew(_currentW.getFunction(), _wIni.getFunction());
  _f_w = new replacedFunction(&law, w_field_func ? w_field_func : w, f_w, _wTild);
//  _w_f = new replacedFunction(&law, sol, w   , _replaceByWF);
//  _dwf = new replacedFunction(&law, sol, dwds, _replaceByWF);
//  _deltaWF = new replacedFunction(&law, sol, _deltaW, _replaceByS);
}


dgNewtonKrabbenhoft::~dgNewtonKrabbenhoft() {
  if(_f_w) delete _f_w;
//  if(_w_f) delete _w_f;
//  if(_dwf) delete _dwf;
//  if(_deltaWF) delete _deltaWF;
  if(_deltaW) delete _deltaW;
}


const dgDofContainer &dgNewtonKrabbenhoft::solve (const dgDofContainer *U0, double alpha, const dgDofContainer *R, double t, dgDofContainer *variable) {

  if(!_sparsityPatternFilled) {
    _dof.fillSparsityPattern(!_dof.isContinuous(), &_law);
    _sparsityPatternFilled = true;
  }

  const dgGroupCollection &groups = _dof.getGroups();
  int nbFields = _law.getNbFields();
  _dU.setAll (0);

  firstNewtonResidual = 0.0;
  bool hasConverged = false;

  if (U0)
    _currentSolution.copy (*U0);
  else
    _currentSolution.setAll (0.);

  //_wIni.L2Projection(_w, &_currentSolution, variable); //copy w^n (with integration points)
  _wIni.interpolate(_w, &_currentSolution, variable); //copy w^n (with integration points)
  _wIni.scatter();

  int ii;
  for (ii = 0; ii < _maxit; ii++) {

    //_currentW.L2Projection(_w, &_currentSolution, variable); //TODO could be optimized to no stockage of the currentW but by group computation
    _currentW.interpolate(_w, &_currentSolution, variable);
    _currentW.scatter();

    if(_preProc) _preProc->call();

    assembleSystem (alpha, R, t, variable);

    // Compute Newton Residual and check convergence
    double newtonResidual = 0;
    newtonResidual = _dof.getNormInfRHS();
    if (newtonResidual != newtonResidual) {
      if (_verb > 0)
        Msg::Info("Newton failed");
      _converged = 0;
      return _dU;
    }
    double conv = 0.0;
    if (ii == 0)
      firstNewtonResidual = newtonResidual;
    if (ii > 0 && firstNewtonResidual == 0.0 )
      conv = log(1. / newtonResidual) / log(10);
    else if (ii > 0. && newtonResidual != 0.0 )
      conv = log(firstNewtonResidual / newtonResidual) / log(10);
    if (_verb > 1)
      Msg::Info("Newton residual (%i): %g (%.2f)", ii, newtonResidual, conv);
      //todo : this check ii>0 are for boundary condition but we have to find another solution
    if (ii > 0 && ((firstNewtonResidual != 0.0 && newtonResidual / firstNewtonResidual < _rtol) || (newtonResidual < _atol))) {
      hasConverged = true;
      break;
    }
    _dof.solve();

    // store for the next newton iteration: v = [U(n+1)^{k+1} - U(n+1)^{k}]
    // new sol = solGroup:  U(n+1)^{k+1}      = U(n+1)^{k}      + [U(n+1)^{k+1} - U(n+1)^{k}]
    // and  deltaG:         U(n+1)^{k+1}-U(n) = U(n+1)^{k}-U(n) + [U(n+1)^{k+1} - U(n+1)^{k}]
    fullMatrix<double> v, vQP, wTildQP, wTildRes, iMassEl;
    dataCacheMap *cacheMapN = new dataCacheMap(dataCacheMap::NODE_MODE, &groups);
    dataCacheMap *cacheMapI = new dataCacheMap(dataCacheMap::INTEGRATION_GROUP_MODE, &groups);
    cacheMapI->setSolutionFunction(_currentSolution.getFunction(), _currentSolution.getFunctionGradient());
    cacheMapN->setSolutionFunction(_currentSolution.getFunction(), _currentSolution.getFunctionGradient());
    if(variable) {
      cacheMapI->setVariableFunction(variable->getFunction(), variable->getFunctionGradient());
      cacheMapN->setVariableFunction(variable->getFunction(), variable->getFunctionGradient());
    }
    dataCacheDouble *dwdsTermN = cacheMapN->get(_dwds);
    dataCacheDouble *wTermN = cacheMapN->get(_w);
    dataCacheDouble *wTolTerm = cacheMapN->get(_wTol);
    dataCacheDouble *f_wTerm = cacheMapN->get(_f_w);

    for (int iGroup = 0; iGroup < groups.getNbElementGroups(); iGroup++){
      dgGroupOfElements *group = groups.getElementGroup(iGroup);
      cacheMapI->setGroup(group);
      cacheMapN->setGroup(group);

      const dgIntegrationMatrices &intMatrices = cacheMapI->getIntegrationMatrices();
      int nbQP = intMatrices.integrationPoints().size1();

      fullMatrix<double> & solgroup = _currentSolution.getGroupProxy(group);
      fullMatrix<double> & deltaG = _dU.getGroupProxy(group);
      int nbNodes = group->getNbNodes();
      v.resize (nbNodes * nbFields, 1, false);
      vQP.resize (nbQP, nbFields, false);
      wTildQP.resize (nbQP, nbFields, false);
      wTildRes.resize (nbNodes, nbFields, false);
      _wTild.resize (nbNodes, nbFields, false);
      for (size_t iElement = 0 ; iElement < group->getNbElements(); iElement++) {
        _dof.getSolution (iGroup, iElement, v);
        v.resize(nbNodes, nbFields, false);

        // alternative variable switching method (Krabbenhoft 2007)
        // Compute predictor Dw = dw/dv * Dv (in the QP 'space')
        // Then element-wise L2 proj of wTild = w + Dw
/*       
        intMatrices.psi().mult(v, vQP);
        const fullMatrix<double> & dwds = dwdsTerm->get();
        const fullMatrix<double> & w = wTerm->get();
        for (int iQPt = 0; iQPt < nbQP; iQPt++) {
          const double detJ = group->getDetJ(iElement, iQPt);
          for (int k = 0; k < nbFields; k++) {
            const double deltaW = dwds(iQPt, k) * vQP(iQPt, k);
            wTildQP(iQPt, k) = (w(iQPt, k) + deltaW) * detJ;
          }
        }
        wTildRes.setAll(0.);
        wTildRes.gemm(intMatrices.psiW(), wTildQP);
        group->getInverseMassMatrix().getBlockProxy(iElement, iMassEl);
        _wTild.setAll(0.);
        _wTild.gemm(iMassEl, wTildRes);*/

        cacheMapN->setElement(iElement);

        const fullMatrix<double> & dwdsN = dwdsTermN->get();
        const fullMatrix<double> & wN = wTermN->get();
        for ( int i = 0; i < nbNodes; i++) {
          for ( int k = 0; k < nbFields; k++) {
            _wTild(i, k) = wN(i, k) + dwdsN(i, k) * v(i, k);
          }
        }

//TODO try a L2 proj of f_w on this group (elem for memory?)
        cacheMapN->setElement(iElement);
        const fullMatrix<double> & f_w = f_wTerm->get(); // compute sol = f_w(_wTild)
        const fullMatrix<double> & wTol = wTolTerm->get();


        for ( int i = 0; i < nbNodes; i++) {
          for ( int k = 0; k < nbFields; k++) {
            const int iEl_iFld = iElement * nbFields + k;

            // Apply th-form in unsaturated area, h-form otherwise
            
            double vmod = v(i, k);
            if(_wTild(i, k) < wTol(i, k)) { // Krabbenhoft variable switching
              vmod = f_w(i, k) - solgroup(i, iEl_iFld);
            }
            solgroup (i, iEl_iFld) += vmod;
            deltaG   (i, iEl_iFld) += vmod;
          }
        }
      }
    }
  }
 
  if (!hasConverged && _verb > 0) {
    Msg::Info("Newton not converged after %i iters !!",_maxit);
    _converged = 0;
  }
  else {
    _converged = ii + 1;
  }
  return _dU;
}

void dgNewtonKrabbenhoft::assembleSystem (double alpha, const dgDofContainer *R, double t, dgDofContainer *variable)
{
  const dgGroupCollection &groups = _dof.getGroups();
  int nbFields = _law.getNbFields();

  dataCacheMap cacheMap(dataCacheMap::NODE_MODE, &groups);
  cacheMap.setSolutionFunction(_currentSolution.getFunction(), _currentSolution.getFunctionGradient());
  if(variable)
    cacheMap.setVariableFunction(variable->getFunction(), variable->getFunctionGradient());
  dataCacheDouble *deltaWTerm = cacheMap.get(_deltaW); //mixed : massFact * (xj-xn) ~= (yj-yn)  == Taylor 1st order
  dataCacheDouble *dwdsTerm = cacheMap.get(_dwds);

  //init iteration
  _F.setAll(0.0);
  _law.fixStrongBC(_dof, &_currentSolution);
  _dof.zeroRHS();
  std::vector<dgFullMatrix<double> > MG(groups.getNbElementGroups());
  
  if (_assembleLHSMatrix) {
    _currentSolution.scatterBegin(!_dof.isContinuous());
    _dof.zeroMatrix();
    //assemble LHS for volume term
    
    for (int iGroup = 0; iGroup < groups.getNbElementGroups(); iGroup++) {
      dgGroupOfElements &group = *groups.getElementGroup(iGroup);
      cacheMap.setGroup(&group);
      int nbNodes = group.getNbNodes();
      fullMatrix<double> JE(nbFields * nbNodes, nbFields * nbNodes);
      _law.computeElement(group, t, _currentSolution, _F, &_dof, variable);

      //todo : MG[iGroup] is not filled !!

      if (alpha != 0.) {
        for (size_t iElement = 0 ; iElement < group.getNbElements(); iElement++) {
          //remove the mass matrix for LHS
          JE.setAll(0.);
          for (int k = 0; k < nbFields; k++) {
            if (!_law.isTemporal(k)) continue;
            fullMatrix<double> massE(nbNodes, nbNodes); //todo optimize (data created for nothing in 2nd case)
            if(_law.isTemporal(k) & 0x4) {  // Mixed form
              cacheMap.setElement(iElement);
              fullMatrix<double> massPure;
              MG[iGroup].getBlockProxy(iElement, massPure);
              const fullMatrix<double> & dwds = dwdsTerm->get();
              for(int i = 0; i < nbNodes; i++) {
                for(int j = 0; j < nbNodes; j++) { // todo: merge with other loops
                  massE(i, j) = dwds(i, k) * massPure(i, j);
                }
              }
            } else {
              MG[iGroup].getBlockProxy(iElement, massE);
            }
            if (_law.isTemporal(k) & 0x2)  //Mass-lumping
              for (int i = 0; i < nbNodes; i++)
                for (int j = 0; j < nbNodes; j++)
                  JE (k*nbNodes+i, k*nbNodes+i) += massE (i, j) * alpha;
            else
              for (int i = 0; i < nbNodes; i++)
                for (int j = 0; j < nbNodes; j++)
                  JE (k*nbNodes+i, k*nbNodes+j) += massE (i, j) * alpha;
          }
          _dof.assembleLHSMatrix(iGroup, iElement, iGroup, iElement, JE);
        }
      }
    }
    _currentSolution.scatterEnd();
    //assemble LHS for interface term
    for (int iGroup = 0; iGroup < groups.getNbFaceGroups (); iGroup++) {
      dgGroupOfFaces *group = groups.getFaceGroup(iGroup);
      const int nbConnections = group->nConnection();
      if(_dof.isContinuous() && nbConnections != 1) continue;
      _law.computeFace(*group, t, _currentSolution, _F, &_dof, variable);
    }
  }

  _dof.setMatrixFreeParameters(_currentSolution, _F, alpha);

  // finally compute the last term of the RHS: -alpha*M*dU term 
  // and compute the newtonResidual G(dU)= F - alpha*M*dU 
  fullMatrix<double> rhs, f; 
  for (int iGroup = 0; iGroup < groups.getNbElementGroups (); iGroup++){
    dgGroupOfElements *group = groups.getElementGroup(iGroup);
    cacheMap.setGroup(group);
    const int nbNodes = group->getNbNodes();
    const dgFullMatrix<double> *RG = (R ? &R->getGroupProxy (group) : NULL);
    dgFullMatrix<double> &GG = _residual.getGroupProxy (group);
    fullMatrix<double> RE, deltaE, massE;

    for (size_t iElement = 0 ; iElement < group->getNbElements(); iElement++) {
      _F.getGroupProxy(iGroup).getBlockProxy(iElement, f);
      GG.getBlockProxy(iElement, rhs);
      rhs.setAll(f);
      if (RG) {
        RG->getBlockProxy(iElement, RE);
        rhs.axpy(RE, 1.);
      }
      // add the mass matrix for G(dU)
      if (alpha != 0.) for (int k = 0; k < nbFields; k++) {
        if (!_law.isTemporal(k)) continue;

        bool isMixedForm = _law.isTemporal(k) & 0x4;
        if(isMixedForm) {
          cacheMap.setElement(iElement);
        }
        else {
          _dU.getGroupProxy(iGroup).getBlockProxy(iElement, deltaE);
        }
        MG[iGroup].getBlockProxy(iElement, massE);
        const fullMatrix<double> & delta = (isMixedForm) ? deltaWTerm->get() : deltaE ;
        if (_law.isTemporal(k) & 0x2)  //Mass-lumping
          for (int i = 0; i < nbNodes; i++) {
            for (int j = 0; j < nbNodes; j++)
              rhs (i, k) -= massE(i, j) * delta(i, k) * alpha;
          }
        else
          for (int i = 0; i < nbNodes; i++)
            for (int j = 0; j < nbNodes; j++)
              rhs (i, k) -= massE(i, j) * delta(j, k) * alpha;
      }
      //add last term of RHS
      _dof.assembleRHSVector(iGroup, iElement, rhs);
    }
  }
}


dgAssemblerExplicitMixed::dgAssemblerExplicitMixed(dgDofManager *dof, const dgConservationLaw &law, const function *secondVar):
  dgAssemblerExplicit(dof, law),
  _errorMainVar(1.),
  _errorSecVar(1.),
  _maxIter(10), 
  _secondVar(secondVar)
{}

void dgAssemblerExplicitMixed::solve (dgDofContainer &U0, double alpha, dgDofContainer *, double t, dgDofContainer &dU, bool recomputeMassMatrix, dgDofContainer *variable) {
  dgDofContainer R(U0), lastSecVar(U0);
  R.setAll(0.0);
  lastSecVar.interpolate(_secondVar);
  lastSecVar.scatter();
  const function *diffMixed = functionMinusNew(_secondVar, lastSecVar.getFunction());
  const function *mixedTimeStep = functionScaleNew(diffMixed, -alpha); //TODO good sign ??

  int ii = 0;
  double errMain = 1e100, errSec = 1e100;
  while(ii < _maxIter && errMain < _errorMainVar && errSec < _errorSecVar) {
    //TODO
    //errMain = U0.
    //errSec = 
    R.interpolate(mixedTimeStep);
    R.scatter();
    dgAssemblerExplicit::solve(U0, alpha, &R, t, dU, false, variable);
  }
  delete mixedTimeStep;
  delete diffMixed; //TODO optim
}
