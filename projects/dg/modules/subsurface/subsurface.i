%module subsurface

%include std_vector.i
%{
  #undef HAVE_DLOPEN
  #include "dgConservationLawRichards.h"
  #include "dgConservationLawSWnonInertia.h"
  #include "dgNewtonKrabbenhoft.h"
  #include "dgTagToFunction.h"
%}

%import(module="dgpy.conservationLaw") "dgConservationLaw.h";
%import(module="dgpy.conservationLaw") "dgConservationLawFunction.h";

%import(module="dgpy.dgCommon") "dgCommon.i";
%import(module="dgpy.dgFunction") "function.h";
namespace std {
%template(VectorFunctionConst) vector<const function*, std::allocator<const function*> >;
%template(PairFunctionInt) pair<const function*, int>;
%template(VectorPairFunctionInt) vector<pair<const function*, int> >;
}

%include "dgConservationLawRichards.h"
%include "dgConservationLawSWnonInertia.h"
%include "dgNewtonKrabbenhoft.h"
%include "dgTagToFunction.h"
%template(SoilMap) dgTTFMap<soil>;
%template(LandSurfaceMap) dgTTFMap<landSurface>;
