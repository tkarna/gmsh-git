#ifdef DG_RICHARDS_COMMON_SET

/*#include "dgConservationLawFunction.h"
#include "fullMatrix.h"
#include "dgGroupOfElements.h"
#include "SPoint3.h"
#include "function.h"
#include "dgConservationLawRichards.h"
#include "dataCacheMap.h"*/

#include "functionGeneric.h"
#include <limits>


// #############################################
// #############################################
//            EXPLICIT IMPLEMENTATION
// #############################################
// #############################################


dgConservationLawRichardsExplicitTh::dgConservationLawRichardsExplicitTh(const function *K, const function *h, const function *gh,
                                                                         const function *state, const function *fm1Th, const function *eta)
                                                                     : dgConservationLawFunction(1)
{
  _KFunc = K;
  _hFunc = h;
  _ghFunc = gh;
  _state = state;
  _fm1Th = fm1Th;
  _eta = eta;

  //default values
  std::vector<double> vg(3); vg[0]=0.0; vg[1]=0.0; vg[2]=1.0;
  _gFunc = new functionConstant(vg);
  _sourceFunc = new functionConstant(0.0);
  temporal[0] = 1; // >1 => mass-lumping
  _upWind = false;
  _default.resize(6, true); //size of the default values
  _diffusiveFlux = _diffusivityScalar = _ipTerm = NULL;
}

void dgConservationLawRichardsExplicitTh::setParam(const std::string key, const double value) {
  if(key == "upwind")
    _upWind = value ? true : false;
  else if(key == "lumping")
    temporal[0] = value ? 2 : 1;
  else
    Msg::Fatal("%s doesnt exists as param of Richards equation !", key.c_str());
}

void dgConservationLawRichardsExplicitTh::setFunction(const std::string key, const function *f) {
  checkNonSetup ();
  const function **toMod=NULL;
  int i = -1;
  if(key == "source")                        { toMod = &_sourceFunc; i = 2;}
  else if(key == "gravitational_vector")     { toMod = &_gFunc;      i = 3;}
  else
    Msg::Fatal("%s doesnt exists as function of Richards equation !\n"
               "(thr,ths,source,gravitational_vector or derivative_variable_transform)", key.c_str());
  //we delete the default functions, but not the external ones
  if(_default[i]) {
    delete (*toMod);
    *toMod = f;
    _default[i] = false;
  } else
    *toMod = f;
}

class dgConservationLawRichardsExplicitTh::ipTermR : public function {
  fullMatrix<double> normals, KL, KR, hL, hR, diffFluxL, diffFluxR, stateL, stateR, hProjL, hProjR, eta;
  const dgConservationLawFunction *_law;  
fullMatrix<double> sol;
  public:
  ipTermR(const function *KF, const function *diffFluxF, const function *hF, const function *stateF, const function *fm1Th, 
          const function *etaF, const dgConservationLawFunction *law): function(2), _law(law) {
    setArgument (normals, function::getNormals());
    setArgumentWrapped (KL, KF, 1, "conductivity term", 0);
    setArgument (KR, KF, 1);
    setArgumentWrapped (hL, hF, 1, "h term", 0);
    setArgument (hR, hF, 1);
    setArgumentWrapped (stateL, stateF, 1, "state term", 0);
    setArgument (stateR, stateF, 1);
    setArgumentWrapped (diffFluxL, diffFluxF, 3, "diffusiveFlux term", 0);
    setArgument (diffFluxR, diffFluxF, 1);
    setArgumentWrapped (hProjL, fm1Th, 1, "inverse retention term", 0);
    setArgument (hProjR, fm1Th, 1);
//    setArgumentWrapped (eta, etaF, 1, "eta term", 0);
setArgument(sol, function::getSolution(),1);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    Msg::Fatal("deprecated: change ip+- to ipL/ipR");
    fullMatrix<double> muFactor = dgConservationLawFunction::muFactor(m);
    bool isBC = false;
    const dgGroupOfFaces &iGroup = *m->getGroupOfInterfaces();
    if(iGroup.nConnection() == 1) //BC
    {
      dgBoundaryCondition *bc = _law->getBoundaryCondition(iGroup.physicalTag());
//      const function *bcF = bc->getOutsideValue(function::getSolution()); //exists if Dirichlet => no IP term if no Dirichlet...
      if(bc) isBC = true;
    }

    for (int iPt = 0; iPt < val.size1(); iPt++) {
      double nu = std::max(KL(iPt, 0), KR(iPt, 0));
      double meanNormalFlux = 0;
      if(isBC) {
        for (int iDim = 0; iDim < 3; iDim ++) {
          meanNormalFlux += diffFluxL(iPt, iDim) * normals(iPt, iDim);
        }
//        printf("nu = %e, fluxes=%e|%e, sol=%e ; %e-%e  %e-%e\n", nu, diffFluxL(iPt, 2), diffFluxR(iPt, 2), sol(iPt, 0), hL(iPt, 0), hR(iPt, 0), hProjL(iPt, 0), hProjR(iPt, 0));
//        nu = nu * 100.;
      } else {
        for (int iDim = 0; iDim < 3; iDim ++) {
          meanNormalFlux += (diffFluxL(iPt, iDim) + diffFluxR(iPt, iDim)) * normals(iPt, iDim) / 2.;
        }
      }
      double solutionJump;
//      if(stateL(iPt, 0) < 0. && stateR(iPt, 0) < 0.) { // creates strange behavior otherwise...
      if(stateL(iPt, 0) < 0. || stateR(iPt, 0) < 0.) {
        //solutionJump = (hProjL(iPt, 0) - hProjR(iPt, 0)) / 2. * nu * muFactor; // stable but not as fast as H
        //solutionJump = (hL(iPt, 0) - hR(iPt, 0)) / 2. * nu * muFactor; // ?
        solutionJump = (hL(iPt, 0) - hR(iPt, 0) + hProjL(iPt, 0) - hProjR(iPt, 0)) / 2. * nu * muFactor(iPt, 0); // less stable ? 
        /*double diffh = hProjL(iPt, 0) - hProjR(iPt, 0);
        if(diffh > 0.)
          solutionJump = std::min( std::max(diffh, hL(iPt, 0) - hR(iPt, 0)) , diffh * 2.) / 2. * nu * muFactor;
        else
          solutionJump = std::min( std::max(diffh * 2., hL(iPt, 0) - hR(iPt, 0)) , diffh) / 2. * nu * muFactor;*/
      } 
      else
        solutionJump = 0.;
        //solutionJump = (hL(iPt, 0) - hR(iPt, 0)) / 2. * nu * muFactor;
//      if(isBC) printf("jump=%e  nF=%e\n", solutionJump, meanNormalFlux);

      val(iPt, 0) = - meanNormalFlux - solutionJump;
      val(iPt, 1) =   meanNormalFlux + solutionJump;
    }
  }
};


void dgConservationLawRichardsExplicitTh::setup () {
  _volumeTerm0[""]            = _sourceFunc;
  _diffusivity[""]            = new dgConservationLawRichardsCommon::mainConductivity(_KFunc);
  _diffusivityScalar          = new dgConservationLawRichardsCommon::interfaceDiffusivity(_KFunc, _ghFunc);
  _diffusiveFlux              = new dgConservationLawRichardsCommon::diffusiveFlux(_KFunc, _ghFunc);
  //massFactor = 1
  _volumeTerm1[""]            = new dgConservationLawRichardsCommon::gradPsiTerm(_KFunc, _gFunc, _diffusiveFlux);
  _ipTerm                     = new ipTermR(_diffusivityScalar, _diffusiveFlux, _hFunc, _state, _fm1Th, _eta, this);
  _interfaceTerm0[""]         = new dgConservationLawRichardsCommon::interfaceTerm (_KFunc, _gFunc, _ipTerm, _upWind, NULL, this);
//  _maximumConvectiveSpeed[""] = new maxConvectiveSpeed (_KFunc);
}

dgConservationLawRichardsExplicitTh::~dgConservationLawRichardsExplicitTh() {
//  if (_maximumConvectiveSpeed[""]) delete _maximumConvectiveSpeed[""];
  if (_interfaceTerm0[""])         delete _interfaceTerm0[""];
  if (_ipTerm)                     delete _ipTerm;
  if (_diffusivityScalar)          delete _diffusivityScalar;
  if (_volumeTerm1[""])            delete _volumeTerm1[""];
  if (_diffusiveFlux)              delete _diffusiveFlux;
  if (_diffusivity[""])            delete _diffusivity[""];
}

function * dgConservationLawRichardsExplicitTh::newSurfaceFlux(const function *HSurfF, const function *HSoilF,
                                                               const function *KSurfF, const function *normalsF,
                                                               const function *surfaceF, const function *minlF) {
  checkSetup();
  return new dgConservationLawRichardsCommon::surfaceFlux(_KFunc, _gFunc, _diffusiveFlux,
                                                          _ghFunc, HSoilF, HSurfF, _state,
                                                          normalsF, surfaceF, minlF, KSurfF);
}

//############################################################################################################################
//############################################################################################################################

dgConservationLawRichardsExplicitThDiff::dgConservationLawRichardsExplicitThDiff(const function *state, const function *eta, 
                                                                                 const function *fh, const function *gradfh,
                                                                                 const function *fm1, const function *h)
                                                                     : dgConservationLawFunction(1)
{
  _state = state;
  _eta = eta;
  _fh = fh;
  _gfh = gradfh;
  _fm1 = fm1;
  _h = h;
}

static const double TH_DIFF_ETA_FACT = 0.25;

class dgConservationLawRichardsExplicitThDiff::constraintGradPsiTerm : public function {
  fullMatrix<double> eta, grad_fh, grad_th, state;
 public:
  constraintGradPsiTerm(const function *etaF, const function *gfhF, const function *stateF) : function(3) {
    setArgument(eta, etaF);
    setArgument(grad_fh, gfhF);
    setArgument(state, stateF);
    setArgument(grad_th, function::getSolutionGradient());
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    if(state(0, 0) >= 0) {
      for (int i = 0; i < eta.size1(); i++) {
        val(i, 0) = - eta(i, 0) * (grad_th(i, 0) - grad_fh(i, 0)) * TH_DIFF_ETA_FACT;
        val(i, 1) = - eta(i, 0) * (grad_th(i, 1) - grad_fh(i, 1)) * TH_DIFF_ETA_FACT;
        val(i, 2) = - eta(i, 0) * (grad_th(i, 2) - grad_fh(i, 2)) * TH_DIFF_ETA_FACT;
      }
    } else {
      for (int i = 0; i < eta.size1(); i++) {
        val(i, 0) = 0.;
        val(i, 1) = 0.;
        val(i, 2) = 0.;
      }
    }
  }
};
#include "dgMesh.h"
class dgConservationLawRichardsExplicitThDiff::ipTermR : public function {
  fullMatrix<double> diffFluxL, diffFluxR, diffL, diffR, fhL, fhR, normals, solL, solR, stateL, stateR;
  fullMatrix<double> solL_R, solR_L, invSolL, invSolR, hR, hL;
  functionReplace replacer;
 public:
  ipTermR(const function *diffFluxF, const function *diffusionF, const function *fhF, const function *fm1, const function *hF, const function *stateF)
      : function(2) {
    setArgument(normals, function::getNormals());
    setArgument(solL, function::getSolution(), 0);
    setArgument(solR, function::getSolution(), 1);
    setArgument(diffFluxL, diffFluxF, 0);
    setArgument(diffFluxR, diffFluxF, 1);
    setArgument(diffL, diffusionF, 0);
    setArgument(diffR, diffusionF, 1);
    setArgument(fhL, fhF, 0);
    setArgument(fhR, fhF, 1);
    setArgument(stateL, stateF, 0);
    setArgument(stateR, stateF, 1);

    // replace h in f(h) by h=f-1(theta)
    replacer.addChild();
    addFunctionReplace(replacer);
    replacer.replace(hL, hF, 0);
    replacer.replace(hR, hF, 1);
    setArgument(invSolL, fm1, 0);
    setArgument(invSolR, fm1, 1);
    replacer.get(solL_R, fhF, 1); // ! inverted
    replacer.get(solR_L, fhF, 0);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
/*int iFace = m->getInterfaceId();
int eli = m->getGroupOfInterfaces()->elementId(iFace, 0);
int elj = m->getGroupOfInterfaces()->elementId(iFace, 1);
fullMatrix<double> aaa(val.size1(), 5);*/

    if(stateL(0,0) < 0. && stateR(0,0) < 0) { // Full unsaturated
      for(int iPt = 0; iPt < val.size1(); iPt++) {
        val(iPt, 0) = val(iPt, 1) = 0.;
      }
    } else {
      fullMatrix<double> muFactor = dgConservationLawFunction::muFactor(m);
      if(stateL(0,0) > 0. && stateR(0,0) > 0.) { // Full saturated
        for (int iPt = 0; iPt < val.size1(); iPt++) {
          hL(iPt, 0) = invSolL(iPt, 0);
          hR(iPt, 0) = invSolR(iPt, 0);
        }
        replacer.compute();
        for (int iPt = 0; iPt < val.size1(); iPt++) {
          double nu = std::max(diffL(iPt, 0), diffR(iPt, 0)) * TH_DIFF_ETA_FACT;
          double meanNormalFlux = 0;
          for (int iDim = 0; iDim < 3; iDim ++) {
            meanNormalFlux += ((diffFluxL(iPt, iDim) + diffFluxR(iPt, iDim)) * normals(iPt, iDim)) * .5;
          }
          double solutionJump = (solL(iPt, 0) - solR(iPt, 0) + solL_R(iPt, 0) - solR_L(iPt, 0)) * .25 * nu * muFactor(iPt, 0);
/*          if(eli > elj) {
            solutionJump = (solL(iPt, 0) - solR(iPt, 0) + solL_R(iPt, 0) - solR_L(iPt, 0));
//            aaa(iPt, 0) = solutionJump;
            solutionJump *= .25 * nu * muFactor;
            aaa(iPt, 1) = solutionJump;
            aaa(iPt, 2) = solutionJump;
            aaa(iPt, 3) = diffL(iPt, 0);
            aaa(iPt, 4) = diffR(iPt, 0);
          } else {
            solutionJump = (solR(iPt, 0) - solL(iPt, 0) + solR_L(iPt, 0) - solL_R(iPt, 0));
//            aaa(iPt, 0) = solutionJump;
            solutionJump *= .25 * nu * muFactor;
//            aaa(iPt, 1) = solutionJump;
            solutionJump *= -1;
            aaa(iPt, 2) = solutionJump;
            aaa(iPt, 3) = diffR(iPt, 0);
            aaa(iPt, 4) = diffL(iPt, 0);
          }*/
/*aaa(iPt, 0) = diffLRRL;
aaa(iPt, 1) = diffLR;
aaa(iPt, 2) = (diffLRRL + diffLR) * .25 * nu * muFactor;*/
          val(iPt, 0) = - meanNormalFlux - solutionJump;
          val(iPt, 1) =   meanNormalFlux + solutionJump;
        }
      
      } else { // Between sat and unsaturated (mixed)
        for (int iPt = 0; iPt < val.size1(); iPt++) {
          double nu = std::max(diffL(iPt, 0), diffR(iPt, 0)) * TH_DIFF_ETA_FACT;
          double meanNormalFlux = 0;
          for (int iDim = 0; iDim < 3; iDim ++) {
            meanNormalFlux += ((diffFluxL(iPt, iDim) + diffFluxR(iPt, iDim)) * normals(iPt, iDim)) * .5;
          }
          double solutionJump = (solL(iPt, 0) - fhL(iPt, 0) - solR(iPt, 0) + fhR(iPt, 0)) * .5 * nu * muFactor(iPt, 0);
/*          if(eli > elj) {
            solutionJump = (solL(iPt, 0) - solR(iPt, 0) + fhL(iPt, 0) - fhR(iPt, 0)) * .25 * nu * muFactor;
            aaa(iPt, 0) = solutionJump;
            aaa(iPt, 1) = 1;
          } else {
            solutionJump = (solR(iPt, 0) - solL(iPt, 0) + fhR(iPt, 0) - fhL(iPt, 0)) * .25 * nu * muFactor;
            aaa(iPt, 0) = solutionJump;
            aaa(iPt, 1) = 2;
            solutionJump *= -1;
          }*/
          //double solutionJump = (solL(iPt, 0) - solR(iPt, 0)) * .5 * nu * muFactor;
/*aaa(iPt, 0) = 2;
aaa(iPt, 1) = fhL(iPt,0) - fhR(iPt,0);
aaa(iPt, 2) = 2;*/
          val(iPt, 0) = - meanNormalFlux - solutionJump;
          val(iPt, 1) =   meanNormalFlux + solutionJump;
        }
      }
    }
//val.setAll(0.);

/*
int ig, ie, ielmt = 17;
int jg, je, jelmt = 18;
m->getGroupCollection()->_mesh->findElement(ielmt, ig, ie);
m->getGroupCollection()->_mesh->findElement(jelmt, jg, je);
if( (eli == ie && elj == je) || (eli == je && elj == ie) ) {
  printf("face %d from %d:\n", iFace, Msg::GetCommRank() );
//  val.print();
//  printf("%21.16e\n",aaa(0,0));
//  printf("%21.16e\t%21.16e\n",aaa(0,0),aaa(0,1));
//  printf("%21.16e\t%21.16e\t%21.16e\n",aaa(0,0),aaa(0,1),aaa(0,2));
  printf("%21.16e\t%21.16e\t%21.16e\t%21.16e\t%21.16e\n",aaa(0,0),aaa(0,1),aaa(0,2),aaa(0,3),aaa(0,4));
}
*/


  }
};

class dgConservationLawRichardsExplicitThDiff::surfaceFlux : public function {
  fullMatrix<double> state, normals, gradTh, eta, thG, thS, diffFlux;
  public:
  surfaceFlux(const function * stateF, const function *normalsF, const function * thSurfF, const function *etaF, const function *diffFluxF): function(1) {
    setArgument(normals, normalsF);
    //setArgument(gradTh, function::getSolutionGradient());
    setArgument(diffFlux, diffFluxF);
    setArgument(thG, function::getSolution());
    setArgument(eta, etaF);
    setArgument(thS, thSurfF);
    setArgument(state, stateF);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for (int iPt = 0; iPt < val.size1(); iPt++) {
      double fluxTh = 0.;
      for (int iDim = 0; iDim < 3; iDim ++)
      //  fluxTh += gradTh(iPt, iDim) * eta(iPt, 0) * TH_DIFF_ETA_FACT * normals(iPt, iDim);
        fluxTh += diffFlux(iPt, 0) * normals(iPt, iDim) * .5;
      double solutionJumpTh = 0.;
      if(state(0, 0) >= 0) {
        solutionJumpTh = (thG(iPt, 0) - thS(iPt, 0)) * TH_DIFF_ETA_FACT / 2. * eta(iPt, 0);
        //printf("thG=%e   thS=%e\n", thG(iPt,0), thS(iPt,0));
      }

//  printf("%e=%e-%e , %e\n", solutionJumpTh, thG(iPt, 0), thS(iPt, 0), fluxTh);
      val(iPt, 0) = - solutionJumpTh; //+ fluxTh
    }
  }
};
function * dgConservationLawRichardsExplicitThDiff::newSurfaceFlux(const function *normalsF, const function * thSurfF) {
  checkSetup();
  return new surfaceFlux(_state, normalsF, thSurfF, _eta, _volumeTerm1[""]);
}

void dgConservationLawRichardsExplicitThDiff::setup () {
  _diffusivity[""]            = _eta;
  _volumeTerm1[""]            = new constraintGradPsiTerm(_eta, _gfh, _state);
  _interfaceTerm0[""]         = new ipTermR(_volumeTerm1[""], _diffusivity[""], _fh, _fm1, _h, _state);
}

dgConservationLawRichardsExplicitThDiff::~dgConservationLawRichardsExplicitThDiff() {
  if (_interfaceTerm0[""])         delete _interfaceTerm0[""];
  if (_volumeTerm1[""])            delete _volumeTerm1[""];
}



//############################################################################################################
//############################################################################################################


class dgConservationLawRichardsExplicitH::factorMass : public function {
  fullMatrix<double> eta, C, K;
 public:
  factorMass(const function *CF, const function *etaF, const function *KF) : function(1) {
    setArgumentWrapped(eta, etaF, 1, "eta", 0);
    setArgumentWrapped(C, CF, 1, "capillary pressure", 0);
    setArgumentWrapped(K, KF, 1, "conductivity", 0);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for (int i = 0; i < val.size1(); i++)
      val(i, 0) = std::max( C(i, 0), K(i, 0) / eta(i, 0) );
  }
};


class dgConservationLawRichardsExplicitH::dgIPTermR : public function {
  fullMatrix<double> diffusiveFluxL, diffusiveFluxR, KL, KR, solutionL, solutionR,
                     normal, stateL, stateR, solutionFixL, solutionFixR, gsolutionL, gsolutionR,
                     applCL, applCR, etaL, etaR, factor;
  bool hasSolFix;
  const dgConservationLawFunction *_law;
 public:
  dgIPTermR (const function *diffusivity, const function *diffusiveFlux, const function *stateF, const function *solFixF,
             const function *applCF, const function *etaF, const dgConservationLawFunction *law,
             const function *factorF, const function *hF = NULL)
                : function(2), _law(law) {
    setArgumentWrapped(diffusiveFluxL, diffusiveFlux, 3, "diffusiveFlux term", 0);
    setArgument       (diffusiveFluxR, diffusiveFlux, 1);
    setArgumentWrapped(KL, diffusivity, 1, "diffusivity term", 0);
    setArgument       (KR, diffusivity, 1);
    if(hF) {
      setArgumentWrapped(solutionL, hF, 1, "solution forced", 0);
      setArgument       (solutionR, hF, 1);
    } else {
      setArgument(solutionL, function::getSolution(), 0);
      setArgument(solutionR, function::getSolution(), 1);
    }
    setArgument(gsolutionL, function::getSolutionGradient(), 0); //TEMP !!
    setArgument(gsolutionR, function::getSolutionGradient(), 1); //TEMP !!
    setArgument(normal, function::getNormals(), 0);
    setArgumentWrapped(stateL, stateF, 1, "state term", 0);
    setArgument       (stateR, stateF, 1);
    setArgumentWrapped(applCL, applCF, 1, "applied dRetention (C or capillary capacity) term", 0);
    setArgument       (applCR, applCF, 1);
    setArgumentWrapped(etaL, etaF, 1, "eta", 0);
    setArgument       (etaR, etaF, 1);
    setArgumentWrapped(factor, factorF, 2, "intern factor", 0);
    hasSolFix = solFixF > 0;
    if(hasSolFix) {
      setArgumentWrapped(solutionFixL, solFixF, 1, "solutionFix term", 0);
      setArgument(solutionFixR, solFixF, 1);
    }
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    fullMatrix<double> mufactor = dgConservationLawFunction::muFactor(m) ;

    bool isBC = false;
    const dgGroupOfFaces &iGroup = *m->getGroupOfInterfaces();
    if(iGroup.nConnection() == 1) //BC
    {
      dgBoundaryCondition *bc = _law->getBoundaryCondition(iGroup.physicalTag());
      if(bc) isBC = true;
    }
//if(isBC)
//  printf("%e - %e\n", -diffusiveFluxR(0,2) / KR(0,0), -diffusiveFluxL(0,2) / KL(0,0) );
    for (int iPt = 0; iPt < val.size1(); iPt++) {

      double meanNormalFluxL = 0, meanNormalFluxR = 0;
      for (int iDim = 0; iDim < 3; iDim ++) {
        meanNormalFluxL += diffusiveFluxL(iPt, iDim) * normal(iPt, iDim);
        meanNormalFluxR += diffusiveFluxR(iPt, iDim) * normal(iPt, iDim);
      }

      double solL, solR, C;
      if(isBC) { // Right = virtual
        solL = solutionL(iPt, 0);
        solR = solutionR(iPt, 0);
        C = applCL(iPt, 0);
        //meanNormalFluxR = meanNormalFluxL;
      } else {
        if (! hasSolFix) {
          solL = solutionL(iPt, 0);
          solR = solutionR(iPt, 0);
        } else {
          solL = stateL(iPt, 0) >= 0. ? solutionL(iPt, 0) : solutionFixL(iPt, 0);
          solR = stateR(iPt, 0) >= 0. ? solutionR(iPt, 0) : solutionFixR(iPt, 0); // could provide extra stability
        }
        meanNormalFluxL *= factor(iPt, 0); // stabilisation to not apply excessive flux
        meanNormalFluxR *= factor(iPt, 1);
//      printf("normal = %e  %e\n", meanNormalFluxL,  meanNormalFluxR);
        C = std::min(applCL(iPt, 0), applCR(iPt, 0));
      }

      double nu = std::max(KL(iPt, 0), KR(iPt, 0));
      const double eta = std::min(etaL(iPt, 0), etaR(iPt, 0));
      nu = std::min(nu * 4., eta * C) ; // stabilisation to not apply excessive jump terms  //*4 for big jumps
      double solutionJumpPenalty = (solL - solR) * .5 * mufactor(iPt, 0) * nu ;
      //if(solL - solR > 10) printf("jump   = %e ~ %e=%e-%e\n", solutionJumpPenalty, solutionL(iPt, 0)- solutionR(iPt, 0), solL, solR);
      //if(solR - solL < -10) printf("jump   = %e ~ %e=%e-%e\n", solutionJumpPenalty, solutionL(iPt, 0)- solutionR(iPt, 0), solL, solR);
      //if(isBC) printf("jump   = %e ~ %e=%e-%e\n", solutionJumpPenalty, solutionL(iPt, 0)- solutionR(iPt, 0), solL, solR);

/*      int intf = m->getInterfaceId();
      int grp = m->getGroupId();
      int nC = iGroup.nConnection();
      if(nC == 2 && grp == 0) {
        const int elL = iGroup.elementId(intf, 0) ;
        const int elR = iGroup.elementId(intf, 1) ;
        if(elL==1472) {
          printf("L[%d][%d]: 2*flux=%e=%e+%e    jump=%e\n", grp,intf, meanNormalFluxL+meanNormalFluxR, meanNormalFluxL, meanNormalFluxR, solutionJumpPenalty);
        }
        else if(elR==1472) {
          printf("R[%d][%d]: 2*flux=%e=%e+%e    jump=%e\n", grp,intf, meanNormalFluxL+meanNormalFluxR, meanNormalFluxR, meanNormalFluxL, solutionJumpPenalty);
          printf("   fact=%e, CL=%e, CR=%e\n", factor(iPt, 0), applCL(iPt, 0), applCR(iPt, 0));
        }
      }*/
//meanNormalFluxL = solutionJumpPenalty = meanNormalFluxR = 0.;
//solutionJumpPenalty = 0.;
//double mean = (meanNormalFluxR + meanNormalFluxL)/2.;
//if(isBC)
//meanNormalFluxR = meanNormalFluxL = 0.;
//solutionJumpPenalty = 0.;

/*
if(!isBC) {
int ig, ie, jg, je;
m->getGroupCollection()->_mesh->findElement(18049, ig, ie);
m->getGroupCollection()->_mesh->findElement(18069, jg, je);
int ei = m->getGroupOfInterfaces()->elementId(m->getInterfaceId(), 0);
int ej = m->getGroupOfInterfaces()->elementId(m->getInterfaceId(), 1);
int gi = m->getGroupCollection()->getElementGroupId(&m->getGroupOfInterfaces()->elementGroup(0));
int gj = m->getGroupCollection()->getElementGroupId(&m->getGroupOfInterfaces()->elementGroup(1));
if( (ie == ei && ig == gi && je == ej && jg == gj) || (ie == ej && ig == gj && je == ei && jg == gi)) {
  printf("Jump = %e = %e * %e * %e\n", solutionJumpPenalty, solL-solR, mufactor, nu);
}
}*/
      val(iPt, 0) = - (meanNormalFluxL + solutionJumpPenalty);
      val(iPt, 1) = - (meanNormalFluxR + solutionJumpPenalty);
//      printf("diff = %e\n", meanNormalFluxL-meanNormalFluxR);
      //if(isBC && fabs(meanNormalFluxR) > 1e-10) printf("%e, %e, %e\n",meanNormalFluxL, meanNormalFluxR, solutionJumpPenalty);
    }
//    val.setAll(0.);
  }
};


class dgConservationLawRichardsExplicitH::interfStabilityFactor : public function {
  fullMatrix<double> CL, CR, etaL, etaR, KL, KR;
 public:
  interfStabilityFactor(const function *CF, const function *etaF, const function *KF) : function(2) {
    setArgumentWrapped(CL, CF, 1, "capillary pressure", 0);
    setArgument(CR, CF, 1);
    setArgumentWrapped(etaL, etaF, 1, "eta (max diff)", 0);
    setArgument(etaR, etaF, 1);
    setArgumentWrapped(KL, KF, 1, "scalar conductivity", 0);
    setArgument(KR, KF, 1);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for (int i = 0; i < val.size1(); i++) {
      val(i, 0) = 1;//std::min( etaL(i, 0) * minMaxC / KL(i, 0), 1.0);  // lowering the flux to reach stability
      val(i, 1) = 1;//std::min( etaR(i, 0) * minMaxC / KR(i, 0), 1.0);
    }
  }
};

dgConservationLawRichardsExplicitH::dgConservationLawRichardsExplicitH(const function *KF, const function *gF, const function *state,
                                                                       const function *solFix, const function *CF, const function *eta)
                                                                       : dgConservationLawFunction(1)
{
  _KFunc = KF;
  _sourceFunc = _hForced = _ghForced = NULL;
  _massF = true;
  _gFunc = gF;
  _stateF = state;
  _solFixF = solFix;
  _CFunc = CF;
  _eta = eta;
  _upWind = false;
  temporal[0] = 2; // forced lumping: seems instable without it !
  _diffusivityScalar = _diffusiveFlux = _capillarCap = _stabilityFactor = _ipTerm = NULL;
}

dgConservationLawRichardsExplicitH * dgConservationLawRichardsExplicitH::newForced(const function *K, const function *gravity, const function *state,
                                     const function *solFix, const function *CF, const function *eta,
                                     const function *h, const function *gh, bool massFactor) {
  dgConservationLawRichardsExplicitH * law = new dgConservationLawRichardsExplicitH(K, gravity, state, solFix, CF, eta);
  law->_hForced = h;
  law->_ghForced = gh;
  law->_massF = massFactor;
  return law;
}

const function * dgConservationLawRichardsExplicitH::getAppliedC() {
  checkSetup();
  return new factorMass(_CFunc, _eta, _diffusivity[""]);
}

void dgConservationLawRichardsExplicitH::setParam(const std::string key, const double value) {
  if(key == "upwind")
    _upWind = value ? true : false;
  else if(key == "lumping")
    temporal[0] = value ? 2 : 1;
  else
    Msg::Fatal("%s doesnt exists as param of Richards equation !", key.c_str());
}

void dgConservationLawRichardsExplicitH::setup() {
  _volumeTerm0[""]            = _sourceFunc;
  _diffusivity[""]            = new dgConservationLawRichardsCommon::mainConductivity(_KFunc);
  _diffusivityScalar          = new dgConservationLawRichardsCommon::interfaceDiffusivity(_KFunc, _ghForced);
  _diffusiveFlux              = new dgConservationLawRichardsCommon::diffusiveFlux(_KFunc, _ghForced);
  _capillarCap                = new factorMass(_CFunc, _eta, _diffusivity[""]);
  if(_massF) _massFactor[""]  = std::make_pair(_capillarCap, false);
  _stabilityFactor            = new interfStabilityFactor(_CFunc, _eta, _diffusivity[""]);
  _volumeTerm1[""]            = new dgConservationLawRichardsCommon::gradPsiTerm(_KFunc, _gFunc, _diffusiveFlux);
  _ipTerm                     = new dgIPTermR(_diffusivityScalar, _diffusiveFlux, _stateF, _solFixF, _capillarCap,
                                              _eta, this, _stabilityFactor, _hForced);
  _interfaceTerm0[""]         = new dgConservationLawRichardsCommon::interfaceTerm (_KFunc, _gFunc, _ipTerm, _upWind, _stabilityFactor, this);
}

dgConservationLawRichardsExplicitH::~dgConservationLawRichardsExplicitH() {
  if (_interfaceTerm0[""]) delete _interfaceTerm0[""];
  if (_ipTerm)             delete _ipTerm;
  if (_volumeTerm1[""])    delete _volumeTerm1[""];
  if (_diffusiveFlux)      delete _diffusiveFlux;
  if (_massFactor.count(""))     delete _massFactor[""].first;
  if (_diffusivity[""])    delete _diffusivity[""];
}

function * dgConservationLawRichardsExplicitH::newSurfaceFlux(const function *HSurfF, const function *HSoilF,
                                                              const function *KSurfF, const function *normalsF,
                                                              const function *surfaceF, const function *minlF) {
  checkSetup();
  return new dgConservationLawRichardsCommon::surfaceFlux(_KFunc, _gFunc, NULL/*_diffusiveFlux*/,
                                                          _ghForced ? _ghForced : function::getSolutionGradient(),
                                                          HSoilF, HSurfF, _stateF,
                                                          normalsF, surfaceF, minlF, KSurfF);
}

class dgConservationLawRichardsExplicitH::surfaceBC : public dgBoundaryCondition {
  class IPModBC : public function {
    fullMatrix<double> Hg, Hs, gh, g, K, normals;
   public:
    IPModBC(const function * hsoil, const function * hsurf, const function * KF, const function * gF) : function(1) {
      setArgument(normals, function::getNormals());
      setArgumentWrapped(Hg, hsoil, 1, "equivalent height for soil", 0);
      setArgumentWrapped(Hs, hsurf, 1, "height of surface water layer", 0);
      setArgumentWrapped(g, gF, 3, "direction of gravitation", 0);
      setArgumentWrapped(K, KF, 9, "conductivity soil", 0);    
      setArgument(gh, function::getSolutionGradient());
    }
    void call (dataCacheMap *m, fullMatrix<double> &val) {
      /*const dgGroupOfElements &eGroup = *m->getGroupOfElements();
      int p = eGroup.getOrder();
      int dim = eGroup.getElement(0)->getDim();
      const dgGroupOfFaces &iGroup = *m->getGroupOfInterfaces();
      double surf = iGroup.getInterfaceSurface( m->getInterfaceId() );
      double minl = eGroup.getElementVolume( m->getElementId() ) / surf;
      double muFactor = (p + 1) * (p + dim) / (dim * minl); */ 
      fullMatrix<double> muFactor = dgConservationLawFunction::muFactor(m);
      for (int i = 0; i < val.size1(); i++) {
        double nu = std::max(K(i, 0), std::max(K(i, 4), K(i, 8)));
        double jumpTerm = ( Hg(i, 0) - Hs(i, 0) ) / 2. * muFactor(i, 0) * nu;
        double normalFlux = 0.;
        for(int k = 0; k < 3; k++)
          normalFlux += ( (g(i, 0) + gh(i, 0)) * K(i, k*3) + (g(i, 1) + gh(i, 1)) * K(i, k*3+1) + (g(i, 2) + gh(i, 2)) * K(i, k*3+2)  ) * normals(i, k);

        val(i, 0) = normalFlux - jumpTerm;
      }
    }
  };
  IPModBC _surfFlux;
 public:
  surfaceBC(const function * hsoil, const function * hsurf, const function * KF, const function * gF) : _surfFlux(hsoil, hsurf, KF, gF) {
    _term0 = &_surfFlux;
    _type = "dirichletMod";
  }
};


dgBoundaryCondition * dgConservationLawRichardsExplicitH::newSurfaceBC(const function * hsoil, const function * hsurf) {
  checkSetup();
  return new surfaceBC(hsoil, hsurf, _KFunc, _gFunc);
}

#endif
