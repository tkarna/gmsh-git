#ifndef _DG_NEWTON_KRABBENHOFT_H_
#define _DG_NEWTON_KRABBENHOFT_H_
#include "dgNewton.h"
#include "dgAssemblerExplicit.h"

class function;
class dgDofContainer;
class dgNewtonKrabbenhoft : public dgNewton
{
  class replacedFunction;

  replacedFunction *_f_w;
  const function *_dwds, *_wTol, *_w, *_deltaW;
  dgDofContainer _wIni, _currentW;
  fullMatrix<double> _wTild;
  bool _sparsityPatternFilled;

  void assembleSystem (double alpha, const dgDofContainer *R, double t, dgDofContainer *variable = NULL);
 public:
  dgNewtonKrabbenhoft (dgDofManager &dof, const dgConservationLaw &law, const function *w,
                            const function *f, const function *dwds, const function *wTol, const function *w_field_func);
  const dgDofContainer &solve (const dgDofContainer *U0, double alpha, const dgDofContainer *R, double t, dgDofContainer *variable = NULL);

  ~dgNewtonKrabbenhoft();
};

class dgAssemblerExplicitMixed : public dgAssemblerExplicit
{
  double _errorMainVar, _errorSecVar;
  int _maxIter;
  const function *_secondVar;
 public:
  dgAssemblerExplicitMixed(dgDofManager *dof, const dgConservationLaw &law, const function *secondVar);
  void solve (dgDofContainer &U0, double alpha, dgDofContainer *R, double t, dgDofContainer &dU, bool recomputeMassMatrix = false, dgDofContainer *variable = NULL);
//  void setSecondVar(const function *sV) { _secondVar = sV; }
  void setMainErr(const double err) { _errorMainVar = err; }
  void setSecErr (const double err) { _errorSecVar = err; }
  void setMaxIter(const int mIt) { _maxIter = mIt; }
};

#endif
