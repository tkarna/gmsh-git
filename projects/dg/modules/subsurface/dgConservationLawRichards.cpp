#include "dgConservationLawFunction.h"
#include "fullMatrix.h"
#include "dgGroupOfElements.h"
#include "SPoint3.h"
#include "function.h"
#include "dgConservationLawRichards.h"
#include "dgGroupOfElements.h"

// ############################################
// ############################################
//    Common functions for Richards equation
// ############################################
// ############################################

class dgConservationLawRichardsCommon::gradPsiTerm : public function {
  fullMatrix<double> K, g, diff;
  public:
  gradPsiTerm(const function *KF, const function *gF, const function *diffF): function(3) {
    setArgumentWrapped(K, KF, 9, "conductivity term", 0);
    setArgumentWrapped(g, gF, 3, "gravity term", 0);
    setArgumentWrapped(diff, diffF, 3, "diffusion term", 0);
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for(int i = 0; i < val.size1(); i++) {
      val(i, 0) = - g(i, 0) * K(i, 0) - g(i, 1) * K(i, 1) - g(i, 2) * K(i, 2) + diff(i, 0);
      val(i, 1) = - g(i, 0) * K(i, 3) - g(i, 1) * K(i, 4) - g(i, 2) * K(i, 5) + diff(i, 1);
      val(i, 2) = - g(i, 0) * K(i, 6) - g(i, 1) * K(i, 7) - g(i, 2) * K(i, 8) + diff(i, 2);
    }
  }
};

class dgConservationLawRichardsCommon::diffusiveFlux : public function {
  fullMatrix<double> solgrad, K, sol;
  bool isSolGrad;
  public:
  diffusiveFlux(const function * KF, const function *gH = NULL) : function(3) {
    if(gH) {
      setArgumentWrapped(solgrad, gH, 3, "gradient pressure", 0);
      isSolGrad = false;
    } else {
      setArgument(solgrad, function::getSolutionGradient());
      isSolGrad = true;
    }
    //setArgument(sol, function::getSolution());
    setArgumentWrapped(K, KF, 9, "conductivity term", 0);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    for(int i = 0; i < solgrad.size1(); i++) {
      val(i, 0) = - solgrad(i, 0) * K(i, 0) - solgrad(i, 1) * K(i, 1) - solgrad(i, 2) * K(i, 2);
      val(i, 1) = - solgrad(i, 0) * K(i, 3) - solgrad(i, 1) * K(i, 4) - solgrad(i, 2) * K(i, 5);
      val(i, 2) = - solgrad(i, 0) * K(i, 6) - solgrad(i, 1) * K(i, 7) - solgrad(i, 2) * K(i, 8);
    }
    /*
    if(m->getGroupOfInterfaces() != NULL) 
    {
      const dgGroupOfFaces &iGroup = *m->getGroupOfInterfaces();
      int intf = m->getInterfaceId();
      int grp = m->getGroupId();
      int nC = iGroup.nConnection();
      if(nC == 2 && grp == 0) {
        const int elL = iGroup.elementId(intf, 0) ;
        const int elR = iGroup.elementId(intf, 1) ;
        if(elR==1472) {
          printf("%d::%d\n", elL, elR);
          for(int i = 0; i < solgrad.size1(); i++)
            printf("R[%d][%d](%d): grad=%e,%e,%e  K=%e, sol=%e\n", grp,intf, i, solgrad(i, 0), solgrad(i, 1), solgrad(i, 2), K(i,0), sol(i,0));
        }
      }
      
    }*/
  }
};

class dgConservationLawRichardsCommon::mainConductivity : public function {
  fullMatrix<double> K;
  public:
  mainConductivity(const function * KF) : function(1) {
    setArgumentWrapped(K, KF, 9, "conductivity term", 0);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for(int i = 0; i < K.size1(); i++) {
      val(i, 0) = std::max(std::max(K(i, 0), K(i, 4)), K(i, 8));
    }
  }
};

class dgConservationLawRichardsCommon::interfaceDiffusivity : public function {
  fullMatrix<double> normals, gradSol, K;
 public:
  interfaceDiffusivity(const function *KF, const function *gH = NULL) : function(1) {
    setArgument(normals, function::getNormals());
    if(gH) {
      setArgumentWrapped(gradSol, gH, 3, "gradient pressure", 0);
    } else {
      setArgument(gradSol, function::getSolutionGradient());
    }
    setArgumentWrapped(K, KF, 9, "conductivity term", 0);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    for (int iPt = 0; iPt < val.size1(); iPt++) {
      double nu = 0, normalGrad = 0;
      for(int iDim = 0; iDim < 3; iDim++) {
          normalGrad += gradSol(iPt, iDim) * normals(iPt, iDim);
          nu += ( K(iPt, iDim * 3    ) * gradSol(iPt, 0)
                + K(iPt, iDim * 3 + 1) * gradSol(iPt, 1)
                + K(iPt, iDim * 3 + 2) * gradSol(iPt, 2) ) * normals(iPt, iDim);
      }
      nu = (fabs(normalGrad) > 1e-8) ? nu / normalGrad : std::max(std::max(K(iPt, 0), K(iPt, 4)), K(iPt, 8)) ;
      val.set(iPt, 0, nu);
    }
  }
};

class dgConservationLawRichardsCommon::interfaceTerm : public function {
  fullMatrix<double> normals, KL, KR, g, ipTerm, factor;
  bool _upWind, haveFactor;
 public:
  interfaceTerm(const function *KF, const function *gF, const function *ipTermF, bool upWind, const function *factorF=NULL, const dgConservationLawFunction *law=NULL): function(2) {
    setArgument (normals, function::getNormals());
    setArgumentWrapped (KL, KF, 9, "conductivity term", 0);
    setArgumentWrapped (KR, KF, 9, "conductivity term", 1);
    setArgumentWrapped (g, gF, 3, "gravity term", 0);
    setArgumentWrapped (ipTerm, ipTermF, 2, "ip term", 0);
    if(factorF)
      setArgumentWrapped (factor, factorF, 2, "intern factor term", 0);
    haveFactor = factorF > 0;
    _upWind = upWind;
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for(int i = 0; i < val.size1(); i++) {
      // n . (K . g)
      double nx = normals(i, 0);
      double ny = normals(i, 1);
      double nz = normals(i, 2);
      double uL = g(i, 0) * KL(i, 0) + g(i, 1) * KL(i, 1) + g(i, 2) * KL(i, 2);
      double vL = g(i, 0) * KL(i, 3) + g(i, 1) * KL(i, 4) + g(i, 2) * KL(i, 5);
      double wL = g(i, 0) * KL(i, 6) + g(i, 1) * KL(i, 7) + g(i, 2) * KL(i, 8);
      double uR = g(i, 0) * KR(i, 0) + g(i, 1) * KR(i, 1) + g(i, 2) * KR(i, 2);
      double vR = g(i, 0) * KR(i, 3) + g(i, 1) * KR(i, 4) + g(i, 2) * KR(i, 5);
      double wR = g(i, 0) * KR(i, 6) + g(i, 1) * KR(i, 7) + g(i, 2) * KR(i, 8);
      double fluxL = uL * nx + vL * ny + wL * nz;
      double fluxR = uR * nx + vR * ny + wR * nz;
      if(haveFactor) {
        fluxL *= factor(i, 0);
        fluxR *= factor(i, 1);
//      printf("interf = %e  %e\n", fluxL, fluxR);
      }
      double fluxM = (fluxL + fluxR + ipTerm(i, 0) + ipTerm(i, 1)) * .5; // ip(0) = ipLeft and ip(1) = ipRight
//printf("F = %e = %e + %e + %e + %e \n", fluxM, fluxL, fluxR, ipTerm(i,0), ipTerm(i,1) );


//const dgGroupOfFaces &iGroup = *m->getGroupOfInterfaces();
//if(iGroup.nConnection() == 1 && _law->getBoundaryCondition(iGroup.physicalTag()) && fabs(fluxR) > 1e-14 )
//  printf("fluxG = %e + %e = %e   norm=%e %e %e \n", fluxL, ipTerm(i,0), fluxM, nx, ny, nz);

      if(_upWind) {
      printf("upwind\n");
        if(fluxM > 0) {
          val(i, 0) =  fluxL + ipTerm(i, 0);
          val(i, 1) = -fluxL - ipTerm(i, 0);
        } else {
          val(i, 0) =  fluxR + ipTerm(i, 1);
          val(i, 1) = -fluxR - ipTerm(i, 1);
        }
      }
      else {
        val(i, 0) =  fluxM;
        val(i, 1) = -fluxM;
      }
//      printf("total  = %e  %e\n\n", val(i, 0), val(i, 1));
//val(i, 0) = val(i, 1) = 0.;
    }
  }
};

class dgConservationLawRichardsCommon::surfaceFlux : public function {
  fullMatrix<double> normals, KSoil, g, diffusiveFlux, gradh, hG, hS, state, xyz, surface, KSurf, minl;
  fullMatrix<double> thG, thS, gradTh, eta;
public:
  surfaceFlux(const function *KF, const function *gF, const function *diffusiveFluxF,
              const function *gradhF, const function * hF, const function * hSurfF, const function * stateF,
              const function *normalsF, const function *surfaceF, const function * minlF, const function * KSurfF) : function(1) {
    setArgument (normals, normalsF);
/*    normalBehavior = ! surfaceF;*/
    if(surfaceF)
      setArgument (surface, surfaceF);
    setArgumentWrapped (KSoil, KF, 9, "conductivity soil", 0);
    setArgumentWrapped (KSurf, KSurfF, 9, "conductivity surface", 0);
    setArgumentWrapped (g, gF, 3, "gravitationnal vector", 0);
//    setArgumentWrapped (diffusiveFlux, diffusiveFluxF, 3, "diffusive flux", 0);
    setArgument (gradh, gradhF);
    setArgument (hG, hF);
    setArgument (hS, hSurfF);
//    setArgument (gradTh, function::getSolutionGradient() );
//    setArgument (thG, function::getSolution());
//    setArgument (thS, thSurfF);
//    setArgument (eta, etaF);
    setArgument (minl, minlF);
//    if(stateF)
//      setArgument (state, stateF);
//    setArgument (xyz, xyzF);
//    printf("id : %p\n", hSurfF);
  }

  void call (dataCacheMap *m, fullMatrix<double> &val) {
    const dgGroupOfElements &eGroup = *m->getGroupOfElements();
    int p = eGroup.getOrder();
    int dim = eGroup.getDimUVW();
    /*double surf;
    if(normalBehavior) {
      muFactor = dgConservationLawFunction::muFactor(m);
    } else {
      const dgGroupOfElements &eGroup = *m->getGroupOfElements();
      int p = eGroup.getOrder();
      int dim = eGroup.getElement(0)->getDim();
      int iElement = m->getElementId();
      double minl = eGroup.getElementVolume(iElement) / surface(0, 0);
      muFactor = (p + 1) * (p + dim) / (dim * minl);
    }*/
    //double minl = eGroup.getElementVolume(iElement) / surf;
    //double muFactor = (p + 1) * (p + dim) / (dim * minl);

    //double surfMin = minl(0, 0) * minl(0, 0);
    double e = minl(0, 0);// * surfMin / surface(0, 0);
    double muFactor = (p + 1) * (p + dim) / (dim * e);

    for(int iPt = 0; iPt < val.size1(); iPt++) {

      // gravitationnal
      double K[9];
      for(int i = 0; i < 9; i++)
        //K[i] = KSoil(iPt, i);
        K[i] = std::max( KSoil(iPt, i), KSurf(iPt, i) ) ;   // to do that, use of the same K is mandatory for the diffusive flux !
        //K[i] = (KSoil(iPt, i) + KSurf(iPt, i)) / 2.;
      //double u = g(iPt, 0) * K[0] + g(iPt, 1) * K[1] + g(iPt, 2) * K[2];
      //double v = g(iPt, 0) * K[3] + g(iPt, 1) * K[4] + g(iPt, 2) * K[5];
      //double w = g(iPt, 0) * K[6] + g(iPt, 1) * K[7] + g(iPt, 2) * K[8];
      double u = (g(iPt, 0) + gradh(iPt, 0)) * K[0] + (g(iPt, 1) + gradh(iPt, 1)) * K[1] + (g(iPt, 2) + gradh(iPt, 2)) * K[2];
      double v = (g(iPt, 0) + gradh(iPt, 0)) * K[3] + (g(iPt, 1) + gradh(iPt, 1)) * K[4] + (g(iPt, 2) + gradh(iPt, 2)) * K[5];
      double w = (g(iPt, 0) + gradh(iPt, 0)) * K[6] + (g(iPt, 1) + gradh(iPt, 1)) * K[7] + (g(iPt, 2) + gradh(iPt, 2)) * K[8];
      double flux = u * normals(iPt, 0) + v * normals(iPt, 1) + w * normals(iPt, 2);

//      printf("%e  %e\n", g(iPt, 2) * K[8], diffusiveFlux(iPt, 2) );
//      printf("%e  %e\n", g(iPt, 2), gradh(iPt, 2) );

      // IP (diffusion flux)
/*      double normalFlux = 0, normalGrad = 0, nuG = 0, nuS = 0;
      for (int iDim = 0; iDim < dimXYZ; iDim ++) {
        normalFlux += diffusiveFlux(iPt, iDim) * normals(iPt, iDim);
      }*/

      double nuG = std::max(KSoil(iPt, 0), std::max(KSoil(iPt, 4), KSoil(iPt, 8)));
      double nuS = std::max(KSurf(iPt, 0), std::max(KSurf(iPt, 4), KSurf(iPt, 8)));

      double nu = std::max(nuG, nuS); // (nuG + nuS) * .5;

      double solutionJump = (hG(iPt, 0) - hS(iPt, 0)) / 2. * muFactor * nu;


//      if(fabs(flux-solutionJump) > 1e3) printf("solJump=%e , hG=%e, hS=%e, nu=%e\n", -solutionJump, hG(iPt,0), hS(iPt,0), nu );

      //val(iPt, 0) = flux - normalFlux - solutionJump;
      //printf("%e , %e=%e-%e\n", flux, -solutionJump, hG(iPt, 0), hS(iPt, 0));
      val(iPt, 0) = flux - solutionJump;
    }
  }
};

typedef std::map<const function*, const function*> mapFunction;

class dgBoundaryConditionEvaporationBoundary : public dgBoundaryCondition {
  class termF : public function {
    fullMatrix<double> flux, dirichletFlux, state;
    dgBoundaryCondition * outsideGeneric;
    functionReplace outReplace;
   public:
    termF(dgConservationLawFunction *claw, const function *fluxF, const function *stateF,
          const std::string tag, const mapFunction *replace_map, const mapFunction *replace_map2=NULL) : function(1) {

      setArgumentWrapped(flux, fluxF, 1, "evaporation flux", 0);
      setArgumentWrapped(state, stateF, 1, "state (0=dirichlet, 1=neumann)", 0);

      outsideGeneric = claw->newOutsideValueBoundaryGeneric(tag, replace_map, replace_map2);
      outReplace.addChild();
      addFunctionReplace (outReplace);
      outReplace.get(dirichletFlux, outsideGeneric->getTerm0()); //trick to compute only when requested
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      if(state(0,0)) { // normal flux
        for (int iPt = 0; iPt < val.size1(); iPt++) {
          val(iPt, 0) = flux(iPt, 0);
        }
      } else { // dirichlet BC
        outReplace.compute();
        for (int iPt = 0; iPt < val.size1(); iPt++) {
          val(iPt, 0) = dirichletFlux(iPt, 0);
        }
      }
    }
  };
  termF _termF;
 public:
  dgBoundaryConditionEvaporationBoundary(dgConservationLawFunction *claw, const function *flux, const function *st, const std::string tag,
                                          mapFunction *replace_map, const mapFunction *replace_map2=NULL) :
                                         _termF(claw, flux, st, tag, replace_map, replace_map2) {
    //_outsideValue = *replace_map;   //issues with neumann... => to be correct we have to ask if we limit per element ... hum
    _term0 = &_termF;
    _type = "evaporation";
  }
};

dgBoundaryCondition * dgConservationLawRichardsCommon::newEvaporationBoundary(dgConservationLawFunction *claw,
                                                                              const std::string tag,
                                                                              const function *flux,
                                                                              const function *state,
                                                                              const std::vector<const function *> toReplace,
                                                                              const std::vector<const function *> replaceBy,
                                                                              const std::vector<const function *> toReplace2,
                                                                              const std::vector<const function *> replaceBy2) {
  if(toReplace.size() != replaceBy.size())
    Msg::Fatal("toReplace must be of the same size as replaceBy in outsideValueBoundaryGeneric");
  if(toReplace2.size() != replaceBy2.size())
    Msg::Fatal("toReplace2 must be of the same size as replaceBy2 in outsideValueBoundaryGeneric");
  mapFunction replace_map;
  //replace_map[pressure] = atmWPressure; // replace pressure by atmWPressure !
  for(size_t i = 0; i < toReplace.size(); i ++)
    replace_map[toReplace[i]] = replaceBy[i];
  mapFunction replace_map2;
  for(size_t i = 0; i < toReplace2.size(); i ++)
    replace_map2[toReplace2[i]] = replaceBy2[i];
  return new dgBoundaryConditionEvaporationBoundary(claw, flux, state, tag, &replace_map, &replace_map2);
}

class evaporationBoundaryState : public function {
  fullMatrix<double> flux, H, atmWPressure, oldState;
 public:
  evaporationBoundaryState(const function *fluxF, const function *pressure, const function *atmWPressureF, const function *oldStateF) : function(1) {
    setArgumentWrapped(flux, fluxF, 1, "evaporation flux", 0);
    setArgumentWrapped(H, pressure, 1, "pressure head", 0);
    setArgumentWrapped(atmWPressure, atmWPressureF, 1, "atmospheric water pressure", 0);    
    setArgumentWrapped(oldState, oldStateF, 1, "state at previous time step", 0);    
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    double avgF = 0.;
    for (int iPt = 0; iPt < flux.size1(); iPt++) avgF += flux(iPt, 0);
    avgF /= (double) flux.size1();
    int neumann = 1;
    if(avgF < 0.) { // evaporation case
      if(oldState(0,0) == 0)
        neumann = 0;
      else {
        double avgP = 0.;
        for (int iPt = 0; iPt < H.size1(); iPt++) avgP += H(iPt, 0);
        avgP /= (double) H.size1();
        if(avgP < atmWPressure(0, 0)) //suppose constant atm pressure on 1 elemt
          neumann = 0;
      }
    }
    val(0,0) = neumann;
  }
};
const function * dgConservationLawRichardsCommon::newEvaporationBoundaryState(const function *flux, const function *pressure,
                                                                              const function *atmWPressure, const function *oldState) {
  return new evaporationBoundaryState(flux, pressure, atmWPressure, oldState);
}

// #############################################
// #############################################
//            IMPLICIT IMPLEMENTATIONS
// #############################################
// #############################################
// Rem: explicit one is in another file !

class dgConservationLawRichardsHform::dgIPTermR : public function {
  fullMatrix<double> diffusiveFluxL, diffusiveFluxR, diffusivityL, diffusivityR, solutionL, solutionR, normal;
  int _nbFields;
 public:
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    fullMatrix<double> mufactor = dgConservationLawFunction::muFactor(m);  

    for (int iPt = 0; iPt < val.size1(); iPt++) {
      double nu, meanNormalFluxL = 0., meanNormalFluxR = 0.;
      for (int iDim = 0; iDim < 3; iDim ++) {
        //meanNormalFlux += (diffusiveFluxL(iPt, iDim) + diffusiveFluxR(iPt, iDim)) * normal(iPt, iDim) / 2.;
        meanNormalFluxL += diffusiveFluxL(iPt, iDim) * normal(iPt, iDim);
        meanNormalFluxR += diffusiveFluxR(iPt, iDim) * normal(iPt, iDim);
      }
      //meanNormalFlux = (meanNormalFluxL + meanNormalFluxR)  *.5;
/*        if(meanNormalFlux > 0.) {
//          nu = diffusivityL(iPt, k);
//          meanNormalFlux = meanNormalFluxL;
      } else {
//          nu = diffusivityR(iPt, k);
//          meanNormalFlux = meanNormalFluxR;
      }*/
      nu = std::max(diffusivityR(iPt, 0), diffusivityL(iPt, 0));
      double solutionJumpPenalty = (solutionL(iPt, 0) - solutionR(iPt, 0)) / 2. * mufactor(iPt, 0) * nu;
      val(iPt, 0) = - (meanNormalFluxL + solutionJumpPenalty);
      val(iPt, 1) = - (meanNormalFluxR + solutionJumpPenalty);
    }
  }
  dgIPTermR (int nbFields, const function *diffusiveFlux, const function *diffusivity, const function *solF = NULL): function(2), _nbFields(nbFields) {
    setArgumentWrapped(diffusiveFluxL, diffusiveFlux, _nbFields * 3, "diffusive flux", 0);
    setArgument(diffusiveFluxR, diffusiveFlux, 1);
    setArgumentWrapped(diffusivityL, diffusivity, _nbFields, "diffusivity", 0);
    setArgument(diffusivityR, diffusivity, 1);
    if(solF) {
      setArgumentWrapped(solutionL, solF, 1, "solution", 0);
      setArgument(solutionR, solF, 1);
    } else {
      setArgument(solutionL, function::getSolution(), 0);
      setArgument(solutionR, function::getSolution(), 1);
    }
    setArgument(normal, function::getNormals(), 0);
  }
};


void dgConservationLawRichardsHform::setup () {
  _diffusivity[""] = new dgConservationLawRichardsCommon::mainConductivity(_KFunc);
//  _maximumConvectiveSpeed[""] = new maxConvectiveSpeed (_KFunc);
  _diffusiveFlux = new dgConservationLawRichardsCommon::diffusiveFlux(_KFunc, _gHFunc);
  _volumeTerm1[""] = new dgConservationLawRichardsCommon::gradPsiTerm(_KFunc, _gFunc, _diffusiveFlux);
  _volumeTerm0[""] = _sourceFunc;
  if(! temporal[0] & 0x4) {
    _massFactor[""] = std::make_pair(_CFunc, false);
  }
  _interfaceDiff = new dgConservationLawRichardsCommon::interfaceDiffusivity(_KFunc, _gHFunc);
  std::vector<double> vec(3);
  vec[0] = vec[1] = vec[2] = 0.;
  _ipTerm = new dgIPTermR(1, _diffusiveFlux, _interfaceDiff, _HFunc);
  //_ipTerm = dgNewIpTerm(1, _diffusiveFlux, _interfaceDiff, true);
  //_ipTerm = dgNewIpTerm(1, _diffusiveFlux, _diffusivity[""], true);
  _interfaceTerm0[""] = new dgConservationLawRichardsCommon::interfaceTerm(_KFunc, _gFunc, _ipTerm, _upWind);
}

dgConservationLawRichardsHform::~dgConservationLawRichardsHform() {
//  if (_maximumConvectiveSpeed[""]) delete _maximumConvectiveSpeed[""];
//  if (_volumeTerm0[""]) delete _volumeTerm0[""];
  if (_volumeTerm1[""]) delete _volumeTerm1[""];
  if (_interfaceTerm0[""]) delete _interfaceTerm0[""];
  if (_diffusiveFlux) delete _diffusiveFlux;
  if (_ipTerm) delete _ipTerm;
  if (_interfaceDiff) delete _interfaceDiff;
}

dgConservationLawRichardsHform::dgConservationLawRichardsHform(const function *K,
                                                     const function *g, const function *s, const function *C) : dgConservationLawFunction(1)
{
  _KFunc = K;
  _HFunc = NULL;
  _gHFunc = NULL;
  _gFunc = g;
  _sourceFunc = s;
  _CFunc = C;
  temporal[0] = 1; // 2 == mass-lumping
  _upWind = false;
  _diffusiveFlux = _interfaceDiff = _ipTerm = NULL;
}

void dgConservationLawRichardsHform::setParam(const std::string key, const double value) {
  if(key == "upwind") {
    _upWind = value ? true : false;
  }
  else if(key == "lumping") {
    temporal[0] = value ? temporal[0] | 0x2 : (temporal[0] & 0x2 ? temporal[0] - 0x2 : temporal[0] );
  }
  else if(key == "mixed") {
    temporal[0] = value ? temporal[0] | 0x4 : (temporal[0] & 0x4 ? temporal[0] - 0x4 : temporal[0] );
  }
  else
   Msg::Fatal("%s doesnt exist as param of Richards H-form equation !", key.c_str());
}


class dgConservationLawRichardsHform::surfaceBC : public dgBoundaryCondition {
  dgConservationLawRichardsCommon::surfaceFlux _surfFlux;
 public:
  surfaceBC(const function * Ksurf, const function * hsoil, const function * hsurf, const function * K, 
            const function * g, const function * diffusiveFlux, const function * minl) :
          _surfFlux(K, g, diffusiveFlux, function::getSolutionGradient(),
                    hsoil, hsurf, NULL, function::getNormals(), NULL, minl, Ksurf) {
    Msg::Fatal("deprecated");
    _term0 = &_surfFlux;
    _type = "outsideValue";
  }
};
dgBoundaryCondition * dgConservationLawRichardsHform::newSurfaceBC(const function * Ksurf, const function * hsoil, const function * hsurf, const function * minl) {
  checkSetup();
  return new dgConservationLawRichardsHform::surfaceBC(Ksurf, hsoil, hsurf, _KFunc, _gFunc, _diffusiveFlux, minl);
}

function * dgConservationLawRichardsHform::newSurfaceFlux(const function *HSurfF, const function *HSoilF,
                                                              const function *KSurfF, const function *normalsF,
                                                              const function *surfaceF, const function *minlF) {
  checkSetup();
  return new dgConservationLawRichardsCommon::surfaceFlux(_KFunc, _gFunc, _diffusiveFlux,
                                                          function::getSolutionGradient(),
                                                          HSoilF, HSurfF, NULL,
                                                          normalsF, surfaceF, minlF, KSurfF);
}

// ############################################################################################
// ############################################################################################
// ############################################################################################

// Richards, system form => disabled


/*

#define H 1   //H=1: th first variable, h second; H=0: inversion
#define T 1-H

class dgConservationLawRichards::gradPsiTerm : public function {
  fullMatrix<double> sol, K, g, diff;
  public:
  gradPsiTerm(const function *KF,const function *gF,const function *diffF): function(6) {
    //    (0 1 2)         0  1              0   1   2   3  4  5
    //K = (3 4 5)  sol = (th h) solgrad = (thx thy thz hx hy hz)
    //    (6 7 8)
    setArgument (sol, function::getSolution());
    setArgument (K,KF);
    setArgument (g,gF);
    setArgument (diff,diffF);
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    val.scale (0);
    for(int i=0; i< val.size1(); i++) { //TODO non-diagonal K
      val(i,0+T) = -g(i,0) * K(i,0) + diff(i,0+T);
      val(i,2+T) = -g(i,1) * K(i,4) + diff(i,2+T);
      val(i,4+T) = -g(i,2) * K(i,8) + diff(i,4+T);
      val(i,0+H) = 0;
      val(i,2+H) = 0;
      val(i,4+H) = 0;
    }
  }
};

class dgConservationLawRichards::maxConvectiveSpeed : public function {
  fullMatrix<double> _Kv;
  const function *_K;
  public:
  maxConvectiveSpeed(const function *KF): function(1) {
//TODO bring derivative  ∂K/∂z !!
    _K=KF;
    setArgument(_Kv, KF);
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
//    fullMatrix<double> vgradK = functionGradient(K);
    for(int i=0; i < val.size1(); i++) {
      val(i,0) = _Kv(i,0);//vgradK(i,0); //approx O(1)
    }
  }
};

class dgConservationLawRichards::interfaceTerm : public function {
  fullMatrix<double> normals, solLeft, solRight, KL, KR, g, ipTerm, dvtL, dvtR;
  bool _upWind;
  public:
  interfaceTerm(const function *KF, const function *gF, const function *ipTermF, const function *dvtF, bool upWind):function(4) {
      setArgument (normals, function::getNormals());
      setArgument (solLeft, function::getSolution(), 0);
      setArgument (solRight, function::getSolution(), 1);
      setArgument (KL, KF, 0);
      setArgument (KR, KF, 1);
      setArgument (g, gF);
      setArgument (ipTerm, ipTermF);
      setArgument (dvtL, dvtF, 0);
      setArgument (dvtR, dvtF, 1);
      _upWind = upWind;
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    val.scale(0);
    for(int i = 0; i < val.size1(); i++) {

      // L&R parts of the grav. flux
      double nx = normals(i,0);
      double ny = normals(i,1);
      double nz = normals(i,2);
      double uL = -g(i,0)*KL(i,0);
      double vL = -g(i,1)*KL(i,4);
      double wL = -g(i,2)*KL(i,8);
      double uR = -g(i,0)*KR(i,0);
      double vR = -g(i,1)*KR(i,4);
      double wR = -g(i,2)*KR(i,8);
      double unL = uL*nx + vL*ny + wL*nz;
      double unR = uR*nx + vR*ny + wR*nz;

      double jumpL = ipTerm(i,2)*dvtL(i,0);
      double jumpR = ipTerm(i,3)*dvtR(i,0);
      if(_upWind) {
        //Upwind of the whole (grav, diff, jump)
        double fluxL = -unL+ipTerm(i,0);
        double fluxR = -unR+ipTerm(i,1);
  //printf("%g;%g\n",fluxL,fluxR);
//if(xyz(i,2)>-0.0001) printf("%g :: %g  (%e,%e,%e)\n",fluxL,fluxR,xyz(i,0),xyz(i,1),xyz(i,2));
        if(fluxL < 0 && fluxR < 0) {   //influx
          val(i,0+T) =  fluxL+jumpL;
          val(i,2+T) = -fluxL-jumpL;
        } else if(fluxR > 0 && fluxL > 0) {
          val(i,0+T) =  fluxR+jumpR;
          val(i,2+T) = -fluxR-jumpR;
        } else {
          val(i,0+T) =  (fluxL+jumpL+fluxR+jumpR)*.5;
          val(i,2+T) = -(fluxL+jumpL+fluxR+jumpR)*.5;
        }
      }
      else {
        //Mean of the whole
        double fluxM = (-unL+ipTerm(i,0)+ipTerm(i,2)-unR+ipTerm(i,1)+ipTerm(i,3))*.5;
        val(i,0+T) =  fluxM;
        val(i,2+T) = -fluxM;
      }

      val(i,0+H) = 0.0;
      val(i,2+H) = 0.0;
    }
  }
};

class dgConservationLawRichards::diffusiveFlux : public function {
  fullMatrix<double> solgrad, K, dvt, sol;
  public:
  diffusiveFlux(const function * KFunction, const function * dvtFunction):function(6) {
    setArgument(solgrad,function::getSolutionGradient());
    setArgument(K,KFunction);
    setArgument(dvt,dvtFunction); //  ∂(variable transform)/∂h
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    //    (0 1 2)         0  1              0   1   2   3  4  5
    //K = (3 4 5)  sol = (th h) solgrad = (thx thy thz hx hy hz)
    //    (6 7 8)
    for(int i=0; i< solgrad.size1(); i++) {
      val(i,0+T) = -dvt(i,0)*(solgrad(i,3*H)*K(i,0)+solgrad(i,3*H+1)*K(i,1)+solgrad(i,3*H+2)*K(i,2));
      val(i,2+T) = -dvt(i,0)*(solgrad(i,3*H)*K(i,3)+solgrad(i,3*H+1)*K(i,4)+solgrad(i,3*H+2)*K(i,5));
      val(i,4+T) = -dvt(i,0)*(solgrad(i,3*H)*K(i,6)+solgrad(i,3*H+1)*K(i,7)+solgrad(i,3*H+2)*K(i,8));
      val(i,0+H) = 0.0;
      val(i,2+H) = 0.0;
      val(i,4+H) = 0.0;
      
    }
  }
};

class dgConservationLawRichards::diffusivityTransformed : public function {
  fullMatrix<double> K, dvt;
  public:
  diffusivityTransformed(const function *KF,const function *dvtF): function(9) {
    setArgument(K,KF);
    setArgument(dvt,dvtF);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for(int i=0; i < K.size1(); i++)
      for(int j=0; j < 9; j++)
        val(i,j) = dvt(i,0) * K(i,j);
  }
};

class dgConservationLawRichards::constraint : public function {
  fullMatrix<double> sol, fTh, source;
  public:
  constraint(const function *fThF,const function *sourceF):function(2) {
    setArgument(sol,function::getSolution());
    setArgument(fTh,fThF);
    setArgument(source,sourceF);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for(int i=0; i< sol.size1(); i++) {
      val(i,T) = source(i,0);
      val(i,H) = sol(i,T) - fTh(i,0); //constraint
    }
  }
};

class dgConservationLawRichards::dgIPTermR : public function { //diff: return L&R components, 3x3 diffusivity, jump over H
  fullMatrix<double> diffusiveFluxL, diffusiveFluxR, diffusivityL, diffusivityR, solutionL, solutionR, normals, gradSolL, gradSolR;
  int _nbFields;
 public:
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    const dgGroupOfFaces &iGroup = *m->getGroupOfInterfaces();
    const dgGroupOfElements &eGroupL = *m->getGroupOfElements();
    const dgGroupOfElements &eGroupR = *m->getSecondaryCache(1)->getGroupOfElements();
    int iFace = m->getInterfaceId();
    int iElementL = m->getElementId();
    int iElementR = m->getSecondaryCache(1)->getElementId();
    int p = eGroupL.getOrder();
    int dim = eGroupL.getElement(0)->getDim();
    double minl = std::min(eGroupL.getElementVolume(iElementL), eGroupR.getElementVolume(iElementR))/iGroup.getInterfaceSurface(iFace);
    double muFactor = (p+1)*(p+dim)/(dim*minl);
    int dimXYZ = diffusiveFluxR.size2()/2;
    for (int iPt = 0; iPt < val.size1(); iPt++) {
      double normalFluxL = 0, normalFluxR = 0, normalGradL = 0, normalGradR = 0;
      double nuL = 0, nuR = 0;
      for (int iDim = 0; iDim < dimXYZ; iDim ++) {
        normalFluxL += diffusiveFluxL(iPt,T+2*iDim) * normals(iPt,iDim);
        normalFluxR += diffusiveFluxR(iPt,T+2*iDim) * normals(iPt,iDim);
        //Anisotropic Diffusivity Tensor : scalar diffusivity taken in the pressure gradient direction
        nuL += (diffusivityL(iPt,iDim*3  )*gradSolL(iPt,3*H+0)
               +diffusivityL(iPt,iDim*3+1)*gradSolL(iPt,3*H+1)
               +diffusivityL(iPt,iDim*3+2)*gradSolL(iPt,3*H+2)) * normals(iPt,iDim);
        nuR += (diffusivityR(iPt,iDim*3  )*gradSolR(iPt,3*H+0)
               +diffusivityR(iPt,iDim*3+1)*gradSolR(iPt,3*H+1)
               +diffusivityR(iPt,iDim*3+2)*gradSolR(iPt,3*H+2)) * normals(iPt,iDim);
        normalGradL += gradSolL(iPt,3*H+iDim) * normals(iPt,iDim);
        normalGradR += gradSolR(iPt,3*H+iDim) * normals(iPt,iDim);
      }
      nuL = (fabs(normalGradL) > 1e-5) ? nuL/normalGradL : (diffusivityL(iPt,0)+diffusivityL(iPt,4)+diffusivityL(iPt,8))/3.;
      nuR = (fabs(normalGradR) > 1e-5) ? nuR/normalGradR : (diffusivityR(iPt,0)+diffusivityR(iPt,4)+diffusivityR(iPt,8))/3.;
//if(iGroup.nConnection()==1) printf("hL:%e\thR:%e\nnuL:%e\tnuR:%e\t(%e:%e:%e)\n",solutionL(iPt,1),solutionR(iPt,1),nuL,nuR,xyz(iPt,0),xyz(iPt,1),xyz(iPt,2));
//      nuR /= normalGradR;
//printf("%g\t\t%g\n",nuL,nuR);
      double solutionJump = (solutionL(iPt,H)-solutionR(iPt,H))/2*muFactor; //jump over H
      val(iPt,0) = -normalFluxL;
      val(iPt,1) = -normalFluxR;
      val(iPt,2) = -solutionJump*nuL;
      val(iPt,3) = -solutionJump*nuR;
    }
  }
  dgIPTermR (const function *diffusiveFlux, const function *diffusivity):function(4) {
    setArgument(diffusiveFluxL,diffusiveFlux,0);
    setArgument(diffusiveFluxR,diffusiveFlux,1);
    setArgument(diffusivityL,diffusivity,0);
    setArgument(diffusivityR,diffusivity,1);
    setArgument(solutionL,function::getSolution(),0);
    setArgument(solutionR,function::getSolution(),1);
    setArgument(gradSolL, function::getSolutionGradient(),0);
    setArgument(gradSolR, function::getSolutionGradient(),1);
    setArgument(normals,function::getNormals(),0);
  }
};


void dgConservationLawRichards::setup () {
  _diffusivity[""] = new diffusivityTransformed(_KFunc,_dvtFunc);
  _maximumConvectiveSpeed[""] = new maxConvectiveSpeed (_KFunc);
  _diffusiveFlux = new diffusiveFlux (_KFunc,_dvtFunc);
  _ipTerm = new dgIPTermR(_diffusiveFlux, _diffusivity[""]) ;
  _volumeTerm1[""] = new gradPsiTerm(_KFunc, _gFunc, _diffusiveFlux);
  _interfaceTerm0[""] = new interfaceTerm (_KFunc, _gFunc, _ipTerm,_dvtFunc,_upWind);
  _volumeTerm0[""] = new constraint(_fThFunc,_sourceFunc);
}

dgConservationLawRichards::~dgConservationLawRichards() {
  if (_maximumConvectiveSpeed[""]) delete _maximumConvectiveSpeed[""];
  if (_volumeTerm0[""]) delete _volumeTerm0[""];
  if (_volumeTerm1[""]) delete _volumeTerm1[""];
  if (_interfaceTerm0[""]) delete _interfaceTerm0[""];
  if (_diffusiveFlux) delete _diffusiveFlux;
  if (_ipTerm) delete _ipTerm;
}

dgConservationLawRichards::dgConservationLawRichards(const function *K,const function *fTh,const function *dvt, 
                                                     const function *g,const function *s) : dgConservationLawFunction(2)
{
  _KFunc = K;
  _fThFunc = fTh;
  _gFunc = g;
  _dvtFunc = dvt;  // ∂(variable transform)/∂h
  _sourceFunc = s;
  temporal[T] = 1; // 2==mass-lumping
  temporal[H] = false;
  _upWind = false;
}

dgConservationLawRichards *dgConservationLawRichards::newDgConservationLawRichards(const function *K,const function *fTh)
{
  std::vector<double> vg(3); vg[0]=0.0; vg[1]=0.0; vg[2]=1.0;
  return new dgConservationLawRichards(K,fTh,new functionConstant(1.0),new functionConstant(vg),new functionConstant(0.0));
}

void dgConservationLawRichards::setParam(const std::string key,const double value) {
  if(key == "upwind") {
    _upWind = value?true:false;
  }
  else if(key == "lumping") {
     temporal[T] = value?2:1;
  }
  else
   Msg::Fatal("%s doesnt exist as param of Richards equation !",key.c_str()); 
} 

*/

#define DG_RICHARDS_COMMON_SET
#include "dgConservationLawRichardsExplicit.cpp"

