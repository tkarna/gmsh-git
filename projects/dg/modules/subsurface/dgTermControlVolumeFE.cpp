#include "dgTerm.h"
#include "dgMeshJacobian.h"
#include "function.h"
#include "dgIntegrationMatrices.h"
#include "dgDofContainer.h"
#include "dgDofManager.h"
#include "functionGeneric.h"

void dgTermAssemblerControlVolumeFE::compute(const dgGroupOfElements &group, double t, dgDofContainer &solution, dgDofContainer &residual, dgDofManager *jacDof, dgFullMatrix<double> *mass, dgDofContainer *variable)
{
  // compute classic terms (i.e. mass term, source term, adv term or other diff term)
  _classicTermAssembler->compute(group, t, solution, residual, jacDof, mass, variable);

  bool tensorialDiffusion = _volume->getNbCol() == residual.getGroups()->getDim() * residual.getGroups()->getDim();
  if(!tensorialDiffusion && _volume->getNbCol() > 1) Msg::Error("the volume term of dgTermAssemblerControlVolumeFE must be scalar or dimxdim !");
  if(residual.getNbFields() > 1) Msg::Fatal("dgTermAssemblerControlVolumeFE not implemented for multi-fields laws");

  // initialization
  dataCacheMap cacheMapN(dataCacheMap::NODE_MODE, residual.getGroups());
  dataCacheMap cacheMapI(dataCacheMap::INTEGRATION_GROUP_MODE, residual.getGroups());
  cacheMapN.setSolutionFunction(solution.getFunction(), solution.getFunctionGradient());
  cacheMapI.setSolutionFunction(solution.getFunction(), solution.getFunctionGradient());
  if(variable) {
    cacheMapN.setVariableFunction(variable->getFunction(), variable->getFunctionGradient());
    cacheMapI.setVariableFunction(variable->getFunction(), variable->getFunctionGradient());
  }
  cacheMapN.setGroup(&group);
  cacheMapI.setGroup(&group);
  dataCacheDouble *termN = cacheMapN.get(_nodal);
  dataCacheDouble *termI = cacheMapI.get(_volume);
  dataCacheDouble *heightN = cacheMapN.get(_height);
  dataCacheDouble *solN = cacheMapN.get(function::getSolution());
  dataCacheDouble *solI = cacheMapI.get(function::getSolution());
  dataCacheDouble *gradSolN = cacheMapN.get(function::getSolutionGradient());
  dataCacheDouble *gradSolI = cacheMapI.get(function::getSolutionGradient());
  fullMatrix<double> residualProxy, jacobianProxy;
  fullMatrix<double> termE (group.getNbNodes(), 1), termEeps (group.getNbNodes(), 1); // i, 1
  fullMatrix<double> jacE (group.getNbNodes() * group.getNbNodes(), 1); // i*j, 1
  fullMatrix<double> gradXi (cacheMapI.getIntegrationMatrices().getNbIntegrationPoints() * group.getDimUVW(), 1);
  fullMatrix<double> integratedGrad(group.getNbNodes(), 3 * _nbf);
  dgFullMatrix<double> jacobianK; // i*j, 1, iElmt
  bool dependOnGrad = false;
  if(jacDof) {
    jacobianK.resize(group.getNbNodes() * group.getNbNodes(), residual.getNbFields() * residual.getNbFields(), group.getNbElements(), true);
    dependOnGrad = (gradSolN && termN->doIDependOn(*gradSolN)) || (gradSolI && termI->doIDependOn(*gradSolI));
  }

  // loop over elements (dont use BLAS, dont use functionDerivator)
  for (int iElement = 0; iElement < (int)group.getNbElements(); ++iElement) {
    cacheMapN.setElement(iElement);
    cacheMapI.setElement(iElement);
 
    // residual
    computeResidual(cacheMapI, termI->get(), termN->get(), solN->get(), heightN->get(), termE);
    residual.getGroupProxy(&group).getBlockProxy(iElement, residualProxy);
    residualProxy.add(termE);

    // jacobian
    if (jacDof) {
      jacE.setAll(0.);
      const double eps = EPSILON_FINITE_DIFFERENCE;
      const double o_eps = 1. / eps;
      for(int jNode = 0; jNode < group.getNbNodes(); jNode++)
      {
        fullMatrix<double> &varI = solI->set(); // i, 1*nbf
        fullMatrix<double> &varN = solN->set(); // i, 1*nbf
        varN(jNode, 0) += eps;
        cacheMapI.getIntegrationMatrices().psi().mult(varN, varI); // variable nodal => variable QP
        if(dependOnGrad) {
          fullMatrix<double> &gradI = gradSolI->set(); // i, 3*nbf
          fullMatrix<double> &gradN = gradSolN->set(); // i, 3*nbf
          cacheMapI.getIntegrationMatrices().dPsi().mult(varN, gradXi); // variable nodal => grad QP/dXi
          cacheMapI.getJacobians().multDXiDXElement(cacheMapI.getGroupId(), iElement, gradXi, gradI); // grad QP/dXi => grad QP
          cacheMapI.getIntegrationMatrices().psiW().mult(gradI, integratedGrad); // grad QP => integral(gradQP psi)
          cacheMapI.getIntegrationMatrices().massInverse().mult(integratedGrad, gradN); // integralGrad => grad Node
        }
        computeResidual(cacheMapI, termI->get(), termN->get(), varN, heightN->get(), termEeps);
        for(int iNode = 0; iNode < group.getNbNodes(); iNode++)
          jacE(iNode * group.getNbNodes() + jNode, 0) = (termEeps(iNode, 0) - termE(iNode, 0)) * o_eps;
        varN(jNode, 0) -= eps;
      }
      solI->set(); //not mandatory, but more secure
      solN->set();
      gradSolI->set();
      gradSolN->set();
      jacobianK.getBlockProxy(iElement, jacobianProxy);
      jacobianProxy.setAll(jacE);
    }
  }
  if (jacDof)
    jacDof->reorderAndAssembleGroupJacobian(group, jacobianK);
}

// the whole formula is Sum_l k_il (u_l - u_i) Integral[ gradPsi_l . K(u) . gradPsi_i ]
// where k_il is the upwinded value of termN evaluated at Nodes, and K(u) is termI (scalar or tensor) evaluated at QP
void dgTermAssemblerControlVolumeFE::computeResidual(dataCacheMap &cacheI, const fullMatrix<double> &termI, 
                          const fullMatrix<double> &termN, const fullMatrix<double> &var, const fullMatrix<double> &height,
                          fullMatrix<double> &term)
{
//static int count = 0;
  const dgGroupOfElements &group = *cacheI.getGroupOfElements();
  fullMatrix<double> termIN (group.getNbNodes(), group.getNbNodes()); // l, i
  getIntegralPart(cacheI, cacheI.elementId(0), termI, termIN);
//if(cacheI.getElementId()==0)  termIN.print();

  for(int iNode = 0; iNode < group.getNbNodes(); iNode++) {
    term(iNode, 0) = 0.;
    double tmp=0.;
    for(int lNode = 0; lNode < group.getNbNodes(); lNode++) {
//      term(iNode, 0) += termIN(lNode, iNode) * (var(lNode, 0) + height(lNode, 0)) * termN(lNode,0); //TEST without approx for k_il=1
      if(iNode == lNode) continue;
      double tmp_il = (var(lNode, 0)+height(lNode, 0) - var(iNode, 0)-height(iNode, 0)) //* termIN(lNode, iNode);
      * std::max(0., termIN(lNode, iNode));
      //* fabs(termIN(lNode, iNode)); //TODO wthout fabs ... ??
      double k_il = tmp_il > 0. ? termN(lNode, 0) : termN(iNode, 0); //volume upwind
//      printf("%e  %e\n", termIN(lNode, iNode) * (var(lNode, 0) + height(lNode, 0)), (var(lNode, 0)+height(lNode, 0) - var(iNode, 0)-height(iNode, 0)) * termIN(lNode, iNode));
      term(iNode, 0) += k_il * tmp_il;
tmp += tmp_il * k_il;
    }
//    printf("%d : sum = %e %e (diff=%e)\n", count, term(iNode, 0), tmp, fabs(term(iNode, 0) - tmp));
  }
//printf("H=%e %e %e %e ; %e\n", var(0,0)+height(0,0), var(1,0)+height(1,0), var(2,0)+height(2,0), var(3,0)+height(3,0), term(3,0));
//term.print();
/*  if(count == 3)
    exit(1);
  count++;*/
}

// integral(dPsi_l K dPsi_i)
void dgTermAssemblerControlVolumeFE::getIntegralPart(dataCacheMap &cache, int iElement, const fullMatrix<double> &termQP, fullMatrix<double> &termN)
{
  // termQP : nbQP x nbFields
  termN.setAll(0.);
  const int nbQP = termQP.size1();
  const int nbFields = termQP.size2();
  const int nbNodes = termN.size1();
  const int dim = cache.getGroupCollection()->getDim();
  if(nbFields > 1) // tensorial term: dim*dim (checked before)
  {
/*    fullMatrix<double> termQPJ(dim*dim * nbQP, 1);
    cache.getJacobians().multDXiDXDXiDXJElement(cache.getGroupId(), iElement, termQP, termQPJ); //dim^2 * nbQP , 1
    const fullMatrix<double> &dpsidpsiW = cache.getIntegrationMatrices().dPsiDPsiW(); // nbNodes^2, dim^2 * nbQP
    termN.reshape(nbNodes * nbNodes, 1);
    termN.gemm(dpsidpsiW, termQPJ);
    termN.reshape(nbNodes, nbNodes); //  l, i or i, l ???
*/
  Msg::Warning("tensorial not tested !!");
    const dgFullMatrix<double> &dXiDX = cache.getJacobians().getElementJacobian(cache.getGroupId());
    const fullMatrix<double> &DetJ = cache.getJacobians().getElementJacobianDeterminant(cache.getGroupId());
    const fullMatrix<double> &dpsi = cache.getIntegrationMatrices().dPsi();
    const fullVector<double> &w = cache.getIntegrationMatrices().integrationWeights();
    for(int iNode = 0; iNode < nbNodes; iNode++) {
      for(int lNode = 0; lNode < nbNodes; lNode++) {
        if(iNode == lNode) continue; // as not needed
        for(int iPt = 0; iPt < nbQP; iPt++) {
          const int jj = (iElement * nbQP + iPt) * 3;
          for(int iDim = 0; iDim < dim; iDim++) {
            double dPsiJi = 0., dPsiJl = 0.;
            for(int jDim = 0; jDim < dim; jDim++) {
              dPsiJi += dpsi(iPt * dim + jDim, iNode) * dXiDX(jDim, jj + iDim); // grad psi_i * J
              dPsiJl += dpsi(iPt * dim + jDim, lNode) * dXiDX(jDim, jj + iDim); // grad psi_l * J
            }
            termN(lNode, iNode) += dPsiJl * termQP(iPt, iNode * nbNodes + lNode) * dPsiJi * DetJ(iPt, iElement) * w(iPt);
          }
        }
      }
    }
  }
  else // scalar term
  {
    const dgFullMatrix<double> &dXiDX = cache.getJacobians().getElementJacobian(cache.getGroupId());
    const fullMatrix<double> &DetJ = cache.getJacobians().getElementJacobianDeterminant(cache.getGroupId());
    const fullMatrix<double> &dpsi = cache.getIntegrationMatrices().dPsi();
    const fullVector<double> &w = cache.getIntegrationMatrices().integrationWeights();
//    if(int(w.size()) != nbQP) printf("buuug !\n");
//printf("dim = %d, dimJ=%d\n", dim, dXiDX.size2() / nbQP / DetJ.size2());
//printf("nbqp = %d   %d   %d   %d", termQP.size1(), DetJ.size1(), w.size(), dpsi.size1() / dim);
    for(int iNode = 0; iNode < nbNodes; iNode++) {
      for(int lNode = 0; lNode < nbNodes; lNode++) {
//        if(iNode == lNode) continue; // as not needed
//        if(iNode == lNode+2 || iNode+2 == lNode) continue; //TODO remove !!
//        if(iNode == lNode+1 || iNode+1 == lNode) continue; //TODO remove !!
        for(int iPt = 0; iPt < nbQP; iPt++) {
          const int jj = (iElement * nbQP + iPt) * 3;
          double dPsiJDPsiJ = 0.;
          for(int iDim = 0; iDim < dim; iDim++) {
            double dPsiJi = 0.;
            double dPsiJl = 0.;
            for(int jDim = 0; jDim < dim; jDim++) {
              dPsiJi += dpsi(iPt * dim + jDim, iNode) * dXiDX(jDim, jj + iDim); // grad psi_i * J
              dPsiJl += dpsi(iPt * dim + jDim, lNode) * dXiDX(jDim, jj + iDim); // grad psi_l * J
            }
            dPsiJDPsiJ += dPsiJi * dPsiJl;
          }
          termN(lNode, iNode) += termQP(iPt, 0) * dPsiJDPsiJ * DetJ(iPt, iElement) * w(iPt);
        }
      }
    }
  }
}

 
// #####################################
// ############  FACE PART  ############ Warning: we suppose that the int order is the same at L and R of interface
// #####################################

void dgFaceTermAssemblerControlVolumeFE::compute(const dgGroupOfFaces &faces, double t, dgDofContainer &solution, dgDofContainer &residual, dgDofManager *jacDof, dgDofContainer *variable)
{
  const int nbConnections = faces.nConnection();
  if(nbConnections > 2)
    Msg::Fatal("dgFaceTermAssemblerControlVolumeFE not designed for connections > 2 ! (nbC=%d)\n", nbConnections);
  function::getTime()->set(t);
  dataCacheMap cacheMap(dataCacheMap::NODE_MODE, residual.getGroups(), nbConnections - 1);
  dataCacheMap cacheMapElmt(dataCacheMap::NODE_MODE, residual.getGroups(), nbConnections);
  cacheMap.setInterfaceGroup(&faces);
  cacheMap.setSolutionFunction(solution.getFunction(), solution.getFunctionGradient());
  cacheMapElmt.setSolutionFunction(solution.getFunction(), solution.getFunctionGradient());
  if(variable) {
    cacheMap.setVariableFunction(variable->getFunction(), variable->getFunctionGradient());
    cacheMapElmt.setVariableFunction(variable->getFunction(), variable->getFunctionGradient());
  }
  dataCacheDouble * term = cacheMap.get(_term);
  dataCacheDouble * solL = cacheMap.get(function::getSolution());
  dataCacheDouble * gradSolL = cacheMap.get(function::getSolutionGradient());
  dataCacheDouble * solR = NULL, * gradSolR = NULL;
  if(nbConnections == 2) { 
    solR = cacheMap.getSecondaryCache(1)->get(function::getSolution());
    gradSolR = cacheMap.getSecondaryCache(1)->get(function::getSolutionGradient());
  }
  dataCacheDouble * solElmt = cacheMapElmt.get(function::getSolution());

  const int nbTotFields = _term->getNbCol();
  const int nbNodes = faces.getNbNodes();
  const int nbConnectionsInFlux = nbTotFields / _nbf;
  dgFullMatrix<double> residualFace(nbNodes, nbTotFields, faces.size(), false);
  fullMatrix<double> residualEps(nbNodes, nbTotFields);


  fullMatrix<double> proxyResidual, proxyJacobian;
  std::vector<fullMatrix<double> >jacobian(nbConnections);
  std::vector<fullMatrix<double> > jacobians;
  bool dependOnGrad = false;

  if(jacDof) {
    dependOnGrad = (gradSolL && term->doIDependOn(*gradSolL)) || (gradSolR && term->doIDependOn(*gradSolR)) ;
  }

if(!dependOnGrad) { // this part works
  for (size_t iFace = 0 ; iFace < faces.size() ; ++iFace) {
    cacheMap.setInterface(iFace);
    residualFace.getBlockProxy(iFace, proxyResidual);
    computeResidual(cacheMap, term->get(), proxyResidual);

    if (jacDof) {
      const double eps = EPSILON_FINITE_DIFFERENCE;
      const double o_eps = 1. / eps;
      for(int iConn = 0; iConn < nbConnections; iConn++) {
        jacobian[iConn].resize(nbNodes * nbNodes, _nbf * _nbf * faces.size() * nbConnectionsInFlux, false);
        for(int iConnFlux = 0; iConnFlux < nbConnectionsInFlux; iConnFlux++) {
          const int ibloc = iFace * nbConnectionsInFlux + iConnFlux;
          proxyJacobian.setAsProxy(jacobian[iConn], ibloc * _nbf * _nbf, _nbf * _nbf);

          for(int jNode = 0; jNode < nbNodes; jNode++) {
            for(int jField = 0; jField < _nbf; jField++) {
              fullMatrix<double> &var = (iConn == 0 ? solL->set() : solR->set());
              var(jNode, jField) += eps;
              computeResidual(cacheMap, term->get(), residualEps);

              for(int iNode = 0; iNode < nbNodes; iNode++)
                for(int iField = 0; iField < _nbf; iField++)
                  proxyJacobian(iNode * nbNodes + jNode, iField * _nbf + jField) 
                      = (residualEps(iNode, iField) - proxyResidual(iNode, iField)) * o_eps;
              var(jNode, jField) -= eps;
            }
          }
        }
        if(iConn == 0) solL->set();
        else solR->set();
      }
    }
  }

  std::vector<fullMatrix<double> *> residualProxies(nbConnections);
  for (int i = 0; i < nbConnections; i++)
    residualProxies[i] = &residual.getGroupProxy(&faces.elementGroup(i));
  dgFaceTermAssembler::mapFromInterface(faces, _nbf, residualFace, residualProxies);
  if(jacDof) {
    std::vector<fullMatrix<double> > dummyVecMat(faces.nConnection());
    jacDof->reorderAndAssembleFaceGroupJacobian(faces, _nbf, jacobian, dummyVecMat);
  }
}
else //depend on grad
{
Msg::Fatal("control volumes FE depending on grad at interfaces still dont work !\n");
  jacobians.resize(nbConnections);
  for(int iConn = 0; iConn < nbConnections; iConn++) {
    const int nbNodesElmt = faces.elementGroup(iConn).getNbNodes();
    jacobians[iConn].resize(nbNodes * nbNodesElmt, _nbf * _nbf * faces.size() * nbConnectionsInFlux, true);
  }
  fullMatrix<double> restoreVar (nbNodes, _nbf);
  fullMatrix<double> restoreGrad (nbNodes, _nbf * 3);
  for (size_t iFace = 0 ; iFace < faces.size() ; ++iFace) {
    cacheMap.setInterface(iFace);
    residualFace.getBlockProxy(iFace, proxyResidual);
    computeResidual(cacheMap, term->get(), proxyResidual);

    if (jacDof) {
//              printf("coucou3: %d  %d  %d\n", nbConnections, nbConnectionsInFlux, nbNodes );
      const double eps = EPSILON_FINITE_DIFFERENCE;
      const double o_eps = 1. / eps;
      for(int iConn = 0; iConn < nbConnections; iConn++) {
        const dgGroupOfElements & curGroup = faces.elementGroup(iConn);
        const int idCurrGroup = residual.getGroups()->getElementGroupId(&curGroup);
        if(!cacheMapElmt.getGroupOfElements() || cacheMapElmt.getGroupId() != idCurrGroup)
          cacheMapElmt.setGroup(&curGroup);
        const int idCurrElmt = faces.elementId(iFace, iConn);
        cacheMapElmt.setElement(idCurrElmt);
        fullMatrix<double> varElmt = solElmt->get(); // copy as output = const

        const int nbNodesElmt = curGroup.getNbNodes();
        const std::vector<int> &closure = faces.closure(iFace, iConn);

        //integration and jacobian infos
        const dgIntegrationMatrices &integration = cacheMapElmt.getIntegrationMatrices();
        const dgMeshJacobian &jacobian = cacheMapElmt.getJacobians();
        fullMatrix<double> gradXiElmt (integration.getNbIntegrationPoints() * curGroup.getDimUVW(), 1);
        fullMatrix<double> gradIElmt (cacheMapElmt.getIntegrationMatrices().getNbIntegrationPoints(), 3);
        fullMatrix<double> integratedGradElmt (nbNodesElmt, 3 * _nbf);
        fullMatrix<double> gradVarElmt (nbNodesElmt, 3 * _nbf);
        
        for(int iConnFlux = 0; iConnFlux < nbConnectionsInFlux; iConnFlux++) {
          const int ibloc = iFace * nbConnectionsInFlux + iConnFlux;
          proxyJacobian.setAsProxy(jacobians[iConn], ibloc * _nbf * _nbf, _nbf * _nbf);

          for(int jNode = 0; jNode < nbNodesElmt; jNode++) {
            for(int jField = 0; jField < _nbf; jField++) {
              fullMatrix<double> &var = (iConn == 0 ? solL->set() : solR->set());
              fullMatrix<double> &gradVar = (iConn == 0 ? gradSolL->set() : gradSolR->set());
              varElmt(jNode, jField) += eps;

              //TODO optimize this !! => direct nodal without QP (and also in dgDofContainer)
              //compute new gradient on element
              integration.dPsi().mult(varElmt, gradXiElmt);
              jacobian.multDXiDXElement(idCurrGroup, idCurrElmt, gradXiElmt, gradIElmt);
              integration.psiW().mult(gradIElmt, integratedGradElmt);
              integration.massInverse().mult(integratedGradElmt, gradVarElmt);

              //maj var on Elmt => var on Face ; save to restore just after
              for(int iNodeFace = 0; iNodeFace < nbNodes; iNodeFace++) {
                restoreVar(iNodeFace, jField) = var(iNodeFace, jField); //TODO could be outside this loop
                var(iNodeFace, jField) = varElmt(closure[iNodeFace], jField);
                for(int iDimField = 0; iDimField < gradVar.size2(); iDimField++) {
                  restoreGrad(iNodeFace, iDimField) = gradVar(iNodeFace, iDimField);
                  gradVar(iNodeFace, iDimField) = gradVarElmt(closure[iNodeFace], iDimField);
                }
              }
              if(idCurrElmt == 10) {
              printf("azefh\n");
              gradVar.print();
              restoreGrad.print();}


              //derivate
              computeResidual(cacheMap, term->get(), residualEps);
              for(int iNode = 0; iNode < nbNodes; iNode++)
                for(int iField = 0; iField < _nbf; iField++)
                  proxyJacobian(iNode * nbNodesElmt + jNode, iField * _nbf + jField) 
                      += (residualEps(iNode, iField) - proxyResidual(iNode, iField)) * o_eps;

              //restore
              varElmt(jNode, jField) -= eps;
              for(int iNodeFace = 0; iNodeFace < nbNodes; iNodeFace++) {
                var(iNodeFace, jField) = restoreVar(iNodeFace, jField);
                for(int iDimField = 0; iDimField < gradVar.size2(); iDimField++)
                  gradVar(iNodeFace, iDimField) = restoreGrad(iNodeFace, iDimField);
              }
            }
          }
        }
        if(iConn == 0) solL->set();
        else solR->set();
      }
    }
  }
  /*
printf("A\n");
jacobian.print();
printf("B\n");
jacobians[0].print();
if(jacobians.size() > 1) {
printf("C\n");
jacobians[1].print();}*/
  std::vector<fullMatrix<double> *> residualProxies(nbConnections);
  for (int i = 0; i < nbConnections; i++)
    residualProxies[i] = &residual.getGroupProxy(&faces.elementGroup(i));
  dgFaceTermAssembler::mapFromInterface(faces, _nbf, residualFace, residualProxies);
  if(jacDof) {
    std::vector<fullMatrix<double> > dummyMat(nbConnections);
    jacDof->reorderAndAssembleFaceGroupJacobian(faces, _nbf, dummyMat, jacobians);
  }
}
}


void dgFaceTermAssemblerControlVolumeFE::computeResidual(dataCacheMap &cache, const fullMatrix<double> &termN, fullMatrix<double> &term)
{
  const dgGroupOfFaces &group = *cache.getGroupOfInterfaces();
  fullMatrix<double> integralPhi (group.getNbNodes(), 1); // l, i
  getIntegralPart(cache, cache.interfaceId(0), integralPhi);
  for(int iField = 0; iField < termN.size2(); iField++)
    for(int iNode = 0; iNode < group.getNbNodes(); iNode++)
      term(iNode, iField) = termN(iNode, iField) * integralPhi(iNode, 0);
}


void dgFaceTermAssemblerControlVolumeFE::getIntegralPart(dataCacheMap &cache, int iFace, fullMatrix<double> &termN)
{
  termN.setAll(0.);
  const int nbNodes = termN.size1();
  const dgGroupOfFaces &faces = *cache.getGroupOfInterfaces();
  const fullMatrix<double> &detJ = cache.getJacobians().detJInterface(faces.interfaceVectorId());
  const fullMatrix<double> &psi = cache.getIntegrationMatrices().psi();
  const fullVector<double> &w = cache.getIntegrationMatrices().integrationWeights();
  for(int iNode = 0; iNode < nbNodes; iNode++)
    for(int iPt = 0; iPt < w.size(); iPt++)
      termN(iNode, 0) += psi(iPt, iNode) * detJ(iPt, faces.interfaceId(iFace)) * w(iPt);
}


