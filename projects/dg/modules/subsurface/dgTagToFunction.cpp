#include "dgTagToFunction.h"

#define alt_verbose 30
#define EPS 1e-8

/*
 *   SOILS (Richards equation)
 */


//TODO change the string constants into CONSTANTS in the object...


// output functions
functionGeneric soil::getPtrFunction(std::string s) {
  if     (s == "retention")         return (functionGeneric) &soil::retention;
  else if(s == "retention_inv")     return (functionGeneric) &soil::retention_inv;
  else if(s == "dRetention")        return (functionGeneric) &soil::dRetention;
  else if(s == "conductivity")      return (functionGeneric) &soil::conductivity;
  else if(s == "conductivity1")     return (functionGeneric) &soil::conductivity1;
  else if(s == "conductivity_inv")  return (functionGeneric) &soil::conductivity_inv;
  else if(s == "dConductivity")     return (functionGeneric) &soil::dConductivity;
  else if(s == "state")             return (functionGeneric) &soil::state;
  else if(s == "state_history")     return (functionGeneric) &soil::state_history;
  else if(s == "state_eta")         return (functionGeneric) &soil::state_eta;
  else if(s == "state_eta2")        return (functionGeneric) &soil::state_eta2;
  else if(s == "state_eta3")        return (functionGeneric) &soil::state_eta3;
  else if(s == "ths")               return (functionGeneric) &soil::ths;
  else if(s == "thr")               return (functionGeneric) &soil::thr;
  else if(s == "Ks")                return (functionGeneric) &soil::ks;
  else if(s == "airEntryWC")        return (functionGeneric) &soil::airEntryWC;
  else if(s == "airEntryP")         return (functionGeneric) &soil::airEntryP;
  else if(s == "valAtMaxDRetention")return (functionGeneric) &soil::valAtMaxDRetention;
  else
    Msg::Fatal("Bad name of soil output function: '%s'", s.c_str());
  return functionGeneric();
}
//input variables
std::vector<std::string> soil::getInputVariables(std::string s) {
  std::vector<std::string> out;
  if       (s == "ths" || s == "thr" || s == "Ks" || s == "airEntryWC" || s == "airEntryP" || s == "valAtMaxDRetention") {
    out.resize(0);
  } else if(s == "retention" || s == "dRetention" || s == "conductivity"|| s == "conductivity1" || s == "dConductivity") {
    out.resize(1);
    out[0] = "pressure";
  } else if(s == "retention_inv" || s == "conductivity_inv") {
    out.resize(1);
    out[0] = "water_content";
  } else if(s == "state") {
    out.resize(2);
    out[0] = "mean_pressure";
    out[1] = "mean_water_content";
  } else if(s == "state_history") {
    out.resize(3);
    out[0] = "mean_pressure";
    out[1] = "mean_water_content";
    out[2] = "old_state";
  } else if(s == "state_eta") {
    out.resize(3);
    out[0] = "pressure";
    out[1] = "eta";
    out[2] = "old_state";
  } else if(s == "state_eta2") {
    out.resize(5);
    out[0] = "pressure";
    out[1] = "eta";
    out[2] = "old_state";
    out[3] = "conductivity";
    out[4] = "capillary_capacity";
  } else if(s == "state_eta3") {
    out.resize(3);
    out[0] = "pressure";
    out[1] = "eta";
    out[2] = "conductivity_over_capacity";
  } else
    Msg::Fatal("Bad name of soil output function: '%s'", s.c_str());
  return out;
}
int soil::getNbFields(std::string s) {
  if     (s == "retention" || s == "dRetention" || s == "retention_inv" || s == "state" || s == "valAtMaxDRetention" ||
          s == "ths" || s == "thr" || s == "airEntryWC" || s == "airEntryP" || s == "state_history" || s == "state_eta" ||
          s == "state_eta2" || s == "state_eta3" || s == "conductivity1")
    return 1;
  else if(s == "conductivity" || s == "dConductivity" || s == "conductivity_inv" || s == "Ks")
    return 9;
  else
    Msg::Fatal("Bad name of soil output function: '%s'", s.c_str());
  return 0;
}
std::set<double> soilVG::getInterpPoints(std::string s, int nbInterpPoints) {
  double v[14] = {-100., -10., -2., -1., -.5, -.2, -.1, -.05, -.01, -.005, 0, 0.001, 1, 2};
  std::set<double> vect(v, v+14);
  if(s == "retention"  || s == "conductivity1")
    ;
  else
    Msg::Fatal("Interpolation not implemented for this soil function. Use of getFunction()");
  return vect;
}
std::set<double> soilVGm::getInterpPoints(std::string s, int nbInterpPoints) {
  double v[14] = {-100., -10., -2., -1., -.5, -.2, -.1, -.05, -.01, -.005, 0, 0.001, 1, 2};
  std::set<double> vect(v, v+14);
  if(s == "retention"  || s == "conductivity1")
    ;
  else
    Msg::Fatal("Interpolation not implemented for this soil function. Use of getFunction()");
  return vect;
}

void soil::state(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  double meanH=in[0](0,0), meanTh=in[1](0,0);
  for (int i = 0; i < out.size1(); i++) {
    if( (meanTh >= _ths && meanH > - 5e-1) || (meanH >= 0. && meanTh > _ths - 1e-2))
      out.set(i, 0, 1.);
//    else if(meanTh < _ths && meanH > 0.)
//      out.set(i, 0, 0.);
    else
      out.set(i, 0, -1.);
  }
}

void soil::state_history(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  double meanH=in[0](0,0), old_state=in[2](0,0);
  double state = old_state;
  if(old_state >= 0.) { // was saturated
    if(meanH < -1e-0)
      state = -1.;
  } else {              // was unsaturated
    if(meanH > -1e-1)
      state =  1.;
  }
  for (int i = 0; i < out.size1(); i++)
    out.set(i, 0, state);
}

void soil::state_eta(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  fullMatrix<double> K(out.size1(), 9), C(out.size1(), 1);
  conductivity(K, in);
  dRetention(C, in);
  int state = 0;
  for (int i = 0; i < out.size1(); i++) {
    const double h = in[0](i, 0);
    const double eta = in[1](i, 0);
    //    const double old_state = in[2](i, 0); // not used yet
    double Kmain = std::max(K(i, 0), std::max(K(i, 4), K(i, 8)));

    if(h > 0) {
      if(state == -1) {
        state = 0; //mixed
        break;
      }
      state = 1; // fully saturated
    }
    else if(state == 1 || Kmain / C(i, 0) > eta) {
      state = 0; // mixed = one foot on sat, one foot on unsat ; or in 
      break;
    }
    else
      state = -1;
  }
  double st = (double)state;
  for (int i = 0; i < out.size1(); i++) {
    out.set(i, 0, st);
  }
  
}
void soil::state_eta2(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  int state = 0;
  for (int i = 0; i < out.size1(); i++) {
    const double h = in[0](i, 0);
    const double eta = in[1](i, 0);
    //    const double old_state = in[2](i, 0); // not used yet
    const double Kmain = std::max(in[3](i, 0), std::max(in[3](i, 4), in[3](i, 8)));
    const double C = in[4](i, 0);

    if(h >= 0.) {
      if(state == -1) {
        state = 0.; //mixed
        break;
      }
      state = 1; // fully saturated
    }
    else if(state == 1 || Kmain / C > eta) {
      state = 0; // mixed = one foot on sat, one foot on unsat ; or in 
      break;
    }
    else {
      state = -1;
    }
  }
  double st = (double)state;  
  for (int i = 0; i < out.size1(); i++)
    out.set(i, 0, st);
}
void soil::state_eta3(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  int state = 0;
  for (int i = 0; i < out.size1(); i++) {
    const double h = in[0](i, 0);
    const double eta = in[1](i, 0);
    //    const double old_state = in[2](i, 0); // not used yet
    const double KoC = in[2](i, 0);

    if(h >= 0.) {
      if(state == -1) {
        state = 0.; //mixed
        break;
      }
      state = 1; // fully saturated
    }
    else if(state == 1 || KoC > eta) {
      state = 0; // mixed = one foot on sat, one foot on unsat ; or in 
      break;
    }
    else {
      state = -1;

    }
  }
  double st = (double)state;  
  for (int i = 0; i < out.size1(); i++)
    out.set(i, 0, st);
}

void soil::ths(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  for (int i = 0; i < out.size1(); i++) {
    out.set(i, 0, _ths);
  }
}
void soil::thr(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  for (int i = 0; i < out.size1(); i++) {
    out.set(i, 0, _thr);
  }
}
void soil::ks(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  for (int i = 0; i < out.size1(); i++) {
    out.set(i, 0, _ksat); out.set(i, 1, 0.   ); out.set(i, 2, 0.   );
    out.set(i, 3, 0.   ); out.set(i, 4, _ksat); out.set(i, 5, 0.   );
    out.set(i, 6, 0.   ); out.set(i, 7, 0.   ); out.set(i, 8, _ksat);
  }
}


double soilVG::findRoot(double init) { //special case for dRetention ...
  return 0.0;
}

void soilVG::setCut() {
  _hcut  = findRoot(0.);
  //TODO
  Msg::Fatal("soilVG::setCut in dgTagToFunction.cpp to implement !");
  _thcut = _ths;//retention(&_hcut);
}

/*--Van Genuchten classical functions--*/
double soilVG::retention_func(double h) {
  return _thr + (_ths - _thr) * pow(1. + pow(-_alpha * h, _beta), -1. + 1. / _beta);
}
void soilVG::retention(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  for (int i = 0; i < out.size1(); i++) {
    if(in[0](i,0) >= _hcut)
      out.set(i, 0, _thcut + (in[0](i,0) - _hcut) * _ss);
    else
      out.set(i, 0, retention_func( in[0](i, 0) )  );
  }
}
void soilVG::retention_inv(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  for (int i = 0; i < out.size1(); i++) {
    double th = in[0](i,0);
    if(th >= _thcut) {
      if(_ss == 0)
        out.set(i, 0, 0.0); //avoid nan
      else
        out.set(i, 0, (th - _thcut) / _ss + _hcut );
    } else {
      th = std::max(th, _thr + 1e-8);
      out.set(i, 0, -pow(pow( (th - _thr) / (_ths - _thr), -1.0 / (1.0 - 1.0 / _beta)) - 1.0, 1.0 / _beta) / _alpha );
    }
  }
}
void soilVG::dRetention(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  for (int i = 0; i < out.size1(); i++) {
    if(in[0](i,0) >= _hcut)
      out.set(i, 0, _ss);
    else 
      out.set(i, 0, (_ths - _thr) * _alpha * _beta * _nu 
                    * pow(-_alpha * in[0](i,0), _beta - 1.0)
                    * pow(1.0 + pow(-_alpha * in[0](i,0), _beta), -_nu - 1.0)
             );
  }
}
static inline double square(double x) {return x*x;}
void soilVG::conductivity(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  double K;
  for (int i = 0; i < out.size1(); i++) {
    if(in[0](i,0) >= 0.0)
      K = _ksat;
    else {
      double Se = pow(1. + pow(-_alpha * in[0](i,0), _beta), -_nu);
      K = _ksat * pow(Se, _l) * square(1. - pow(1. - pow(Se, 1. / _nu), _nu));
    }
    out.set(i, 0, K); out.set(i, 1, 0); out.set(i, 2, 0);
    out.set(i, 3, 0); out.set(i, 4, K); out.set(i, 5, 0);
    out.set(i, 6, 0); out.set(i, 7, 0); out.set(i, 8, K);
  }
}
void soilVG::conductivity1(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  double K;
  for (int i = 0; i < out.size1(); i++) {
    if(in[0](i,0) >= 0.0)
      K = _ksat;
    else {
      double Se = pow(1. + pow(-_alpha * in[0](i,0), _beta), -_nu);
      K = _ksat * pow(Se, _l) * square(1. - pow(1. - pow(Se, 1. / _nu), _nu));
    }
    out.set(i, 0, K);
  }
}
void soilVG::conductivity_inv(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) { Msg::Fatal("not implemented !"); }
void soilVG::dConductivity(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  double dK;
  for (int i = 0; i < out.size1(); i++) {
    if(in[0](i,0) >= 0.0)
      dK = 0.0;
    else {
      double ahb = pow(-_alpha * in[0](i,0), _beta);
      double ap = 1. + ahb,  am = 1. + 1. / ahb;
      dK = _ksat * _alpha * _beta * _nu * pow(ap, -_l * _nu) * (1. - pow(am, -_nu)) * ahb / (-_alpha * in[0](i,0))
                 * (.5 / ap * (1. - pow(am , -_nu)) + 2. * pow(am, -_nu - 1.) / ahb / ahb );
    }
    out.set(i, 0, dK); out.set(i, 1, 0.); out.set(i, 2, 0.);
    out.set(i, 3, 0.); out.set(i, 4, dK); out.set(i, 5, 0.);
    out.set(i, 6, 0.); out.set(i, 7, 0.); out.set(i, 8, dK);
  }
}
void soilVG::airEntryP(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  for (int i = 0; i < out.size1(); i++) {
    out.set(i, 0, - 1. / _alpha);
  }
}
void soilVG::airEntryWC(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  for (int i = 0; i < out.size1(); i++) {
    out.set(i, 0, retention_func(-1. / _alpha) );
  }
}
void soilVG::valAtMaxDRetention(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input) {
  for (int i = 0; i < out.size1(); i++)
    out.set(i, 0, _valAtMaxDRetention);
}

/*--Van Genuchten modified functions--*/
double soilVGm::retention_func(double h) {
  return _tha + (_thm - _tha) * pow(1. + pow(-_alpha * h, _beta), -_nu);
}
void soilVGm::retention(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  for (int i = 0; i < out.size1(); i++) {
    if(in[0](i,0) >= _hs)
      out.set(i, 0, _ths);
    else
      out.set(i, 0, retention_func(in[0](i, 0)) );
  }
}
void soilVGm::retention_inv(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  for (int i = 0; i < out.size1(); i++) {
    double th = in[0](i,0);
    if(th >= _ths) {
      out.set(i, 0, 0.0); //--not true, but also not defined…
    }
    else {
      th = std::max(th, _thr + 1e-8);
      out.set(i, 0, - pow(pow( (th - _tha) / (_thm - _tha), -1.0 / _nu) - 1.0, 1.0 / _beta) / _alpha );
      if(out(i, 0) > 0.) Msg::Fatal("error retention_inv : th=%g :: h=%g\n", th, out(i,0));
      if(isnan(out(i, 0))) Msg::Fatal("error retention_inv : th=%g :: h=%g\n", th, out(i,0));
    }
  }
}
void soilVGm::dRetention(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  for (int i = 0; i < out.size1(); i++) {
    const double h = in[0](i, 0);
    if(h >= _hs)
      out.set(i, 0, 0.);
    else
      out.set(i, 0, (_thm - _tha) * _alpha * _beta * _nu 
                    * pow(-_alpha * h, _beta - 1.0)
                    * pow(1.0 + pow(-_alpha * h, _beta), -_nu - 1.0)
             );
  }
}
void soilVGm::conductivity(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  for (int i = 0; i < out.size1(); i++) {
    const double h = in[0](i, 0);
    double K;
    if(h >= _hs) {
      K = _ksat;
    } else if(h > _hk) {
  	  K = _kk + (h - _hk) / (_hs - _hk) * (_ksat - _kk);
    } else {
      double th = _tha + (_thm - _tha) * pow(1. + pow(-_alpha * h, _beta), -_nu);
      double Fr = pow(1. - pow((_thr - _tha) / (_thm - _tha), 1 / _nu), _nu);
      double Fk = pow(1. - pow((_thk - _tha) / (_thm - _tha), 1 / _nu), _nu);
      double F  = pow(1. - pow(( th  - _tha) / (_thm - _tha), 1 / _nu), _nu);
      K = _kk * sqrt((th - _thr) / (_thk - _thr)) * square((F - Fr) / (Fk - Fr));
      if(isnan(K)) Msg::Fatal("error conductivity : h=%g :: th=%g\n", h, th);
    }
    out.set(i, 0, K); out.set(i, 1, 0); out.set(i, 2, 0);
    out.set(i, 3, 0); out.set(i, 4, K); out.set(i, 5, 0);
    out.set(i, 6, 0); out.set(i, 7, 0); out.set(i, 8, K);
  }
}
void soilVGm::conductivity1(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  for (int i = 0; i < out.size1(); i++) {
    const double h = in[0](i, 0);
    double K;
    if(h >= _hs) {
      K = _ksat;
    } else if(h > _hk) {
  	  K = _kk + (h - _hk) / (_hs - _hk) * (_ksat - _kk);
    } else {
      double th = _tha + (_thm - _tha) * pow(1. + pow(-_alpha * h, _beta), -_nu);
      double Fr = pow(1. - pow((_thr - _tha) / (_thm - _tha), 1 / _nu), _nu);
      double Fk = pow(1. - pow((_thk - _tha) / (_thm - _tha), 1 / _nu), _nu);
      double F  = pow(1. - pow(( th  - _tha) / (_thm - _tha), 1 / _nu), _nu);
      K = _kk * sqrt((th - _thr) / (_thk - _thr)) * square((F - Fr) / (Fk - Fr));
      if(isnan(K)) Msg::Fatal("error conductivity : h=%g :: th=%g\n", h, th);
    }
    out.set(i, 0, K);
  }
}
void soilVGm::conductivity_inv(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) { Msg::Fatal("not implemented !"); }
void soilVGm::dConductivity(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  std::vector<fullMatrix<double> > vKLeft, vKRight;
  vKLeft[0]  = fullMatrix<double>(out.size1(),1);
  vKRight[0] = fullMatrix<double>(out.size1(),1);
  for (int i = 0; i < out.size1(); i++) {
    vKLeft[0] (i, 0) = in[0](i, 0) - EPS / 2.;
    vKRight[0](i, 0) = in[0](i, 0) + EPS / 2.;
  }
  conductivity(vKLeft[0] , vKLeft);  //replace h- val by K- val
  conductivity(vKRight[0], vKRight); //replace h+ val by K+ val
  double dK;
  for (int i = 0; i < out.size1(); i++) {
    if(in[0](i, 0) >= _hs)
      dK = 0.0;
    else if(in[0](i,0) > _hk)
      dK = (_ksat - _kk) / (_hs - _hk);
    else {
      //--numerical derivative computation
      dK = ( vKLeft[0](i, 0) - vKRight[0](i, 0) ) / EPS;
    }
    out.set(i, 0, dK); out.set(i, 1, 0.); out.set(i, 2, 0.);
    out.set(i, 3, 0.); out.set(i, 4, dK); out.set(i, 5, 0.);
    out.set(i, 6, 0.); out.set(i, 7, 0.); out.set(i, 8, dK);
  }
}
void soilVGm::airEntryP(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  for (int i = 0; i < out.size1(); i++) {
    out.set(i, 0, - 1. / _alpha);
  }
}
void soilVGm::airEntryWC(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  for (int i = 0; i < out.size1(); i++) {
    out.set(i, 0, retention_func(-1. / _alpha) );
  }
}
void soilVGm::valAtMaxDRetention(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &input) {
  for (int i = 0; i < out.size1(); i++)
    out.set(i, 0, _valAtMaxDRetention);
}


/*
 * SURFACE (Shallow waters equations)
 */

functionGeneric landSurface::getPtrFunction(std::string s) {
  if     (s == "bottomFactor")        return (functionGeneric) &landSurface::bottomFactor;
  else if(s == "coeff")               return (functionGeneric) &landSurface::coeff;
  else if(s == "nlFactor")            return (functionGeneric) &landSurface::nlFactor;
  else
    Msg::Fatal("Bad name of soil output function: '%s'", s.c_str());
  return functionGeneric();
}

std::vector<std::string> landSurface::getInputVariables(std::string s) {
  std::vector<std::string> out;
  if (s == "bottomFactor" || s == "coeff") {
    out.resize(1);
    out[0] = "water_height";
  }
  if (s == "nlFactor") {
    out.resize(2);
    out[0] = "water_height";
    out[1] = "water_height_nonlinear";
  } else
    Msg::Fatal("Bad name of soil output function: '%s'", s.c_str());
  return out;
}

int landSurface::getNbFields(std::string s) {
  if (s == "bottomFactor" || s == "coeff" || s == "nlFactor")
    return 1;
  else
    Msg::Fatal("Bad name of soil output function: '%s'", s.c_str());
  return 0;
}


void landSurfManning::bottomFactor(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  for (int i = 0; i < out.size1(); i++) {
    double H = in[0](i, 0);
    if(H <= 0.)
      out.set(i, 0, 0.);
    else
      out.set(i, 0, pow(H, 2./3) / _n );
  }
}

void landSurfChezy::bottomFactor(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  for (int i = 0; i < out.size1(); i++) {
    double H = in[0](i, 0);
    if(H <= 0.)
      out.set(i, 0, 0.);
    else
      out.set(i, 0, pow(H, 1./2) * _C );
  }
}

void landSurfWeibach::bottomFactor(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  for (int i = 0; i < out.size1(); i++) {
    double H = in[0](i, 0);
    out.set(i, 0, pow(H, 1./2) * _fact );
  }
}

///

void landSurfManning::coeff(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  for (int i = 0; i < out.size1(); i++) {
    double H = in[0](i, 0);
    if(H > 0.)
      out.set(i, 0, pow(H, 4./3) / _n2 );
    else
      out.set(i, 0, 0.);
  }
}

void landSurfChezy::coeff(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  for (int i = 0; i < out.size1(); i++) {
    double H = in[0](i, 0);
    if(H > 0.)    
      out.set(i, 0, H * _C2 );
    else
      out.set(i, 0, 0.);
  }
}

void landSurfWeibach::coeff(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  for (int i = 0; i < out.size1(); i++) {
    double H = in[0](i, 0);
    out.set(i, 0, H * _fact2 );
  }
}

///

#define MIN_NLFACTOR_MANNING_DERIV 1e-3 //5e-4
#define MIN_NLFACTOR_MANNING 0 //2e-3 //1e-3 //2e-2 //5e-3


void landSurfManning::nlFactor(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  for (int i = 0; i < out.size1(); i++) {
    double H = in[0](i, 0);
    double H_nonlin = in[1](i, 0);


    if(H > MIN_NLFACTOR_MANNING_DERIV)
      out.set(i, 0, pow(H_nonlin, 5./3) / _n );
    else if(H > MIN_NLFACTOR_MANNING)
      out.set(i, 0, pow(H, 5./3) / _n );
    else
      out.set(i, 0, pow(MIN_NLFACTOR_MANNING, 5./3) / _n );


/*    if(H > MIN_NLFACTOR_MANNING_DERIV)
      out.set(i, 0, pow(H_nonlin, 5./3) / _n );
    else if(H > MIN_NLFACTOR_MANNING) {
//      out.set(i, 0, pow(H, 5./3) / _n );
      double deriv = m->getDerivEps() * 5./3 * pow(MIN_NLFACTOR_MANNING, 2./3) / _n ; // not efficient …
      out.set(i, 0, pow(H, 5./3) / _n - deriv );
    }
    else
      out.set(i, 0, pow(MIN_NLFACTOR_MANNING, 5./3) / _n );*/

/*    if(H - m->getDerivEps() > MIN_NLFACTOR_MANNING)
      out.set(i, 0, pow(H, 5./3) / _n );
    else
      out.set(i, 0, pow(MIN_NLFACTOR_MANNING, 5./3) / _n );*/


/*    if(H > -MIN_NLFACTOR_MANNING)
      out.set(i, 0, pow(H + MIN_NLFACTOR_MANNING, 5./3) / _n );
    else
      out.set(i, 0, 0.);*/
/*    if(H > 0.)
      out.set(i, 0, pow(H, 5./3) / _n );
    else
      out.set(i, 0, 0.); */
      //out.set(i, 0, -pow(H, 5./3) / _n );
  }
}

void landSurfChezy::nlFactor(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  for (int i = 0; i < out.size1(); i++) {
    double H = in[0](i, 0);
    if(H > 0.)
      out.set(i, 0, pow(H, 3./2) * _C );
    else
      out.set(i, 0, 0.);
  }
}

void landSurfWeibach::nlFactor(fullMatrix<double> &out, const std::vector<fullMatrix<double> > &in) {
  for (int i = 0; i < out.size1(); i++) {
    double H = in[0](i, 0);
    if(H > 0.)
      out.set(i, 0, pow(H, 3./2) * _fact );
    else
      out.set(i, 0, 0.);
  }
}

