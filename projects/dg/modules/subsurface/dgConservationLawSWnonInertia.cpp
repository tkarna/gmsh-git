#include "dgConservationLawSWnonInertia.h"
#include "fullMatrix.h"
#include "dgGroupOfElements.h"
#include "function.h"
#include "dgMeshJacobian.h"
#include "dgTerm.h"

class clipNegative : public function {
  fullMatrix<double> _sol;
public:
  clipNegative() : function(1) {
    setArgument(_sol, function::getSolution());
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    double mean = 0.;
    for(int i = 0; i < _sol.size1(); i++) {
      mean += _sol(i, 0);
    }
    mean /= (double) _sol.size1();
    
    double lambda = 1.;
    for(int i = 0; i < val.size1(); i++) {
      if(_sol(i, 0) < 0.) {
        double deviation = _sol(i, 0) - mean;
        if(fabs(deviation) > 1e-10 * mean) //avoid numerical problem wth division by nearly 0
          lambda = std::min( mean / fabs(deviation), lambda);
      }
    }
    lambda = std::max(0., lambda);
    for(int i = 0; i < val.size1(); i++)
      val(i, 0) = lambda * _sol(i, 0) + (1. - lambda) * mean;
  }
};

void dgConservationLawSWnonInertia::setDmax(double dmax) {
  _Dmax = dmax;
  _sqrtSkip = 9. / (4. * dmax * dmax);
  printf("1/sqrt(gh) : max=%e => limh=%e\n", dmax, _sqrtSkip);
  _sqrtSkipA = -pow(_sqrtSkip, -3./2.) / 2.;
  _sqrtSkipB = 3./2. * pow(_sqrtSkip, -1./2.);
}

inline static double sqrtInvLimited(double norm, const dgConservationLawSWnonInertia *law) {
  if(norm > law->getSqrtSkip())
    return 1. / sqrt(norm);
  else
    return law->getSqrtSkipA() * norm + law->getSqrtSkipB();
}

#define EPS_SQRT 0    // limit gradH to min [ 3 / (2*EPS_SQRT) ] (for polynoms)  => not used anymore as limiting applied
#define EPS_POW  0    // following Santillana
#define SWNI_ALPHA 0.05 // smooth upper threshold for diffusion (alpha=0 -> simple threshold)
#define SWNI_ALPHA2 SWNI_ALPHA * SWNI_ALPHA

/*
#define MAX_INV_SQRT 25. // diff value
#define SQRT_SKIP 9. / (4. * MAX_INV_SQRT * MAX_INV_SQRT) //0. //001
const double SQRT_SKIP_A = -pow(SQRT_SKIP, -3./2.) / 2.;
const double SQRT_SKIP_B = 3./2. * pow(SQRT_SKIP, -1./2.);
*/

class dgConservationLawSWnonInertia::gradPsiTerm : public function {
  fullMatrix<double> _gSol, _nlTerm, _gBath, _gH;
  dgConservationLawSWnonInertia * _law;
  fullMatrix<double> sol, bath ; //TMP
  bool haveNotBath;
public:
  gradPsiTerm(const function * gBathF, const function * nlF, const function * gH, const function * bathF, dgConservationLawSWnonInertia * law) : function(3) {
//    setArgument(_gSol, function::getSolutionGradient());
    setArgument(_gBath, gBathF);
    setArgument(_nlTerm, nlF);
    //_constantGH = gH ? true : false;
    if(gH) {
      setArgumentWrapped(_gH, gH, 3, "water gradient", 0);
    }
    else
      setArgument(_gH, function::getSolutionGradient());
    _law = law;
    setArgument(sol, function::getSolution()); //TMP
    if(bathF) setArgument(bath, bathF); //TMP
    haveNotBath = !bathF;
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
/*  if(val.size1() == 4) { //supposing we are in P1
    if(haveNotBath)
      Msg::Error("test in SWNI");

    double gx = - _gH(i, 0) - _gBath(i, 0);
    double gy = - _gH(i, 1) - _gBath(i, 1);
    double norm = sqrt(gx * gx + gy * gy);
    if(!norm) {
      val(i, 0) = val(i, 1) = 0.;
      return;
    }
    double sqrtGH = sqrt(norm);

    double hmax = sol(i, 0) + bath(i, 0);
    int imax = 0;
    for(int i = 1; i < 3; i++) {
      double h = sol(i, 0) + bath(i, 0);
      if(h > hmax) {
        hmax = h;
        imax = i;
      }
    }
    double x = _nlTerm(i, 0) / sqrtGH;
    double q = _law->getDmax();
    //double nl = ( -sqrt((x - q) * (x - q) + SWNI_ALPHA2) + sqrt(q * q + SWNI_ALPHA2) + x ) / 2.;
    double nl = std::min(x, q);
    for(int i = 0; i < 3; i++) {
      val(i, 0) = nl * gx;
      val(i, 1) = nl * gx;
      val(i, 2) = 0;
    }
    val(3, 0) = (val(0, 0) + val(1, 0) + val(2, 0) ) / 3.;
    val(3, 1) = (val(0, 1) + val(1, 1) + val(2, 1) ) / 3.;
    val(3, 2) = 0.;
  }
  else // old code */
    //const double Dmax = _law->getDmax();
    for(int i = 0; i < val.size1(); i++) {

      double gx = - _gH(i, 0) - _gBath(i, 0);
      double gy = - _gH(i, 1) - _gBath(i, 1);
      double norm = sqrt(gx * gx + gy * gy);

      if(!norm) {
        val(i, 0) = val(i, 1) = val(i, 2) = 0.;
        return;
      }

      double nl = _nlTerm(i, 0) * sqrtInvLimited(norm, _law);
      //nl = std::min(nl, Dmax);
      //nl = ( -sqrt((nl - Dmax) * (nl - Dmax) + SWNI_ALPHA2) + sqrt(Dmax * Dmax + SWNI_ALPHA2) + nl ) / 2.;

      val(i, 0) = nl * gx;
      val(i, 1) = nl * gy;
      val(i, 2) = 0.;
    }
  }
};

class dgConservationLawSWnonInertia::interfaceTerm : public function { // de gauche à droite
  fullMatrix<double> _normals, _gBathL, _gBathR, _nlL, _nlR, _HL, _HR, _gHL, _gHR;
  dgConservationLawSWnonInertia * _law;
public:
  interfaceTerm(const function * gBathF, const function * nlF, const function * gH, dgConservationLawSWnonInertia * law) : function(2) {
    setArgument(_normals, function::getNormals());
    setArgument(_HL,    function::getSolution(), 0);
    setArgument(_HR,    function::getSolution(), 1);
    setArgument(_gBathL,  gBathF, 0);
    setArgument(_gBathR,  gBathF, 1);
    setArgument(_nlL,     nlF, 0);
    setArgument(_nlR,     nlF, 1);
    _law = law;
    //_constantGH = gH ? true : false;
    if(gH) {
      setArgumentWrapped(_gHL, gH, 3, "water gradient", 0);
      setArgumentWrapped(_gHR, gH, 3, "water gradient", 1);
    }
    else {
      setArgument(_gHL,   function::getSolutionGradient(), 0);
      setArgument(_gHR,   function::getSolutionGradient(), 1);
    }
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    fullMatrix<double> mufactor = dgConservationLawFunction::muFactor(m) ;
    //const double Dmax = _law->getDmax();

    double maxNLR = 0., maxNLL = 0.;
    for(int i = 0; i < val.size1(); i++) {
      maxNLL = std::max(maxNLL, _nlL(i, 0));
      maxNLR = std::max(maxNLR, _nlR(i, 0));
    }

    for(int i = 0; i < val.size1(); i++) {
      double nx = _normals(i, 0);
      double ny = _normals(i, 1);
      double gx = (_gHL(i, 0) + _gBathL(i, 0) + _gHR(i, 0) + _gBathR(i, 0)) / 2.;
      double gy = (_gHL(i, 1) + _gBathL(i, 1) + _gHR(i, 1) + _gBathR(i, 1)) / 2.;

      double jump = mufactor(i, 0) * (_HL(i, 0) - _HR(i, 0));
      //gx -= jump * nx;
      //gy -= jump * ny;
      double norm = sqrt(gx * gx + gy * gy);
      if(!norm) {
        val(i, 0) = val(i, 1) = 0.;
        return;
      }
      double u = gx * nx + gy * ny;
      double nl;
      if(u < 0.)
        nl = _nlL(i, 0) + EPS_POW;
      else
        nl = _nlR(i, 0) + EPS_POW;
//      nl = (_nlL(i, 0) + _nlR(i, 0)) / 2.;

      double sqrt_inv = sqrtInvLimited(norm, _law);
      nl *= sqrt_inv;

      /*if(norm >= EPS_SQRT)
        sqrtGH = sqrt(norm);
      else {
        sqrtGH = -0.5 * pow(EPS_SQRT, -3. / 2) * norm * norm + 3. / 2 * pow(EPS_SQRT, -1/2.) * norm; //poly 2nd order
        //double norm2 = norm * norm;
        //sqrtGH = 3 / 8. * pow(EPS_SQRT, -5. / 2) * norm2 * norm - 10 / 8. * pow(EPS_SQRT, -3. / 2) * norm2 
        //       + 15 / (8. * sqrt(EPS_SQRT)) * norm; //poly 3rd order
      }*/

      //nl = std::min(nl, Dmax);
      //nl = ( -sqrt((nl - Dmax) * (nl - Dmax) + SWNI_ALPHA2) + sqrt(Dmax*Dmax + SWNI_ALPHA2) + nl ) / 2.;


      //jump *= _solL(i, 0) > _solR(i, 0) ? _nlL(i, 0) : _nlR(i, 0) ;
      //jump *= 4. / sqrtGH;
      //double nlJump = std::min( (_nlL(i, 0) + _nlR(i, 0)) * .5 * sqrt_inv, Dmax);
      double nlJump = (_nlL(i, 0) + _nlR(i, 0)) * .5 * sqrt_inv;
      jump *= nlJump;

      val(i, 0) = nl * u - jump;
      val(i, 1) = - val(i, 0);
    }
  }
};

class dgConservationLawSWnonInertia::integralTerm : public function {
  fullMatrix<double> _nl, _gBath, _gSol;
  dgConservationLawSWnonInertia * _law;
 public:
  integralTerm(const function * gBath, const function *nl, const function * gH, dgConservationLawSWnonInertia * law) : function(1), _law(law) {
    setArgument(_gBath, gBath, 0);
    setArgument(_nl, nl, 0);
    if(gH) {
      setArgumentWrapped(_gSol, gH, 3, "water gradient", 0);
    }
    else
      setArgument(_gSol, function::getSolutionGradient());
    
//    if(mwd)
//      setArgument(_mobwd, mwd, 0);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    for(int i = 0; i < val.size1(); i++) {
      double gx = _gSol(i, 0) + _gBath(i, 0);
      double gy = _gSol(i, 1) + _gBath(i, 1);
      double norm = sqrt(gx * gx + gy * gy);
      if(!norm) {
        val(i, 0) = 0.;
        return;
      }
      val(i, 0) = - _nl(i, 0) * sqrtInvLimited(norm, _law); // negative sign as diff term
    }
  }
};

/*class dgConservationLawSWnonInertia::interfaceTermBnd : public function { // de gauche à droite
  fullMatrix<double> _normals, _gBathL, _nlL, _solL, _gSolL;
public:
  interfaceTermBnd(const function * gBathF, const function * nlF) : function(1) {
    setArgument(_normals, function::getNormals());
    setArgument(_solL,    function::getSolution(), 0);
    setArgument(_gSolL,   function::getSolutionGradient(), 0);
    setArgument(_gBathL,  gBathF, 0);
    setArgument(_nlL,     nlF, 0);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
  
    for(int i = 0; i < val.size1(); i++) {
      double nx = _normals(i, 0);
      double ny = _normals(i, 1);
      double gx = _gSolL(i, 0) + _gBathL(i, 0) ;
      double gy = _gSolL(i, 1) + _gBathL(i, 1) ;
      double jump = 0;//mufactor * _solL(i, 0);
      gx += jump * nx;
      gy += jump * ny;
      double norm = sqrt(gx * gx + gy * gy);
      if(norm) {
        gx /= - norm;
        gy /= - norm;
      }
      else {
        val(i, 0) = val(i, 1) = 0.;
        return;
      }
      double u = gx * nx + gy * ny;
      u *= _nlL(i, 0) + EPS_POW;

      val(i, 0) = u * sqrt(norm);
//      if(norm >= EPS_SQRT)
//        val(i, 0) = u * sqrt(norm);
//      else {
//        double poly = -0.5 * pow(EPS_SQRT, -3. / 2) * norm * norm + 3. / 2 * pow(EPS_SQRT, -1/2.) * norm; //poly 2nd order
//        //double norm2 = norm * norm;
//        //double poly = 3 / 8. * pow(EPS_SQRT, -5. / 2) * norm2 * norm - 10 / 8. * pow(EPS_SQRT, -3. / 2) * norm2 
//        //            + 15 / (8. * sqrt(EPS_SQRT)) * norm; //poly 3rd order
//        val(i, 0) = - u * poly;
//      }
    }
  }
};
function * dgConservationLawSWnonInertia::getNewInterfaceTermBnd(const function * gBathF, const function * nlF) {
  return new interfaceTermBnd(gBathF, nlF);
}*/

class dgConservationLawSWnonInertia::interfaceSlopeTermBnd : public function { // de gauche à droite
  fullMatrix<double> _normals, _gBath, _nl, _sol, _gSolL, _gSolR;
  dgConservationLawSWnonInertia * _law;  
public:
  interfaceSlopeTermBnd(const function * gBathF, const function * nlF, const function *gHF, dgConservationLawSWnonInertia * law) : function(1) {
    setArgument(_normals, function::getNormals());
//    setArgument(_solL,    function::getSolution(), 0);
    setArgument(_gSolL,   function::getSolutionGradient(), 0);
    setArgument(_gSolR,   gHF, 0);
    setArgument(_gBath,  gBathF, 0);
    setArgument(_nl,     nlF, 0);
    _law = law;    
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
  
    for(int i = 0; i < val.size1(); i++) {
      double nx = _normals(i, 0);
      double ny = _normals(i, 1);
      double gx = (_gSolL(i, 0) + _gSolR(i, 0)) * .5 + _gBath(i, 0);
      double gy = (_gSolL(i, 1) + _gSolR(i, 1)) * .5 + _gBath(i, 1);
      
      double norm = sqrt(gx * gx + gy * gy);
      if(!norm) {
        val(i, 0) = 0.;
        return;
      }
      double u = gx * nx + gy * ny;
      double nl = _nl(i, 0) * sqrtInvLimited(norm, _law);
      val(i, 0) = u * nl;
    }
  }
};
function * dgConservationLawSWnonInertia::getNewInterfaceSlopeBnd(const function * gBathF, const function * nlF, const function *gHF) {
  return new interfaceSlopeTermBnd(gBathF, nlF, gHF, this);
}


void dgConservationLawSWnonInertia::setup() {
  if(_nlFunc == NULL)
    Msg::Error("the nl function has to be declared in SWnonInertia !");
  _interfaceTerm0[""] = new interfaceTerm(_gBathFunc, _nlFunc, _gh, this);
  _volumeTerm0[""]    = _source;
  if(!_volumeFunction){
    _volumeTerm1[""]    = new gradPsiTerm(_gBathFunc, _nlFunc, _gh, _bath, this);
}  else{
    _volumeTermIntegral = new integralTerm(_gBathFunc, _volumeFunction, _gh, this);
    }
  if(_clipped) _clipToPhysics[""] = new clipNegative();
  _initialized = true;
}



void dgConservationLawSWnonInertia::setSource(const function * source) {
  if(_initialized)
    Msg::Fatal("setSource in SWnonInertia should be used before setup() !");
  _source = source;
}

dgConservationLawSWnonInertia::~dgConservationLawSWnonInertia() {
  if(_interfaceTerm0[""]) delete _interfaceTerm0[""];
  if(_volumeTerm1[""])    delete _volumeTerm1[""];
  if(_volumeTermIntegral) delete _volumeTermIntegral;
}

dgConservationLawSWnonInertia::dgConservationLawSWnonInertia(const function * gBathymetry, const function * nl,
                                                             const function * source, const function * gh,
                                                             const function * bath) : dgConservationLawFunction(1) {
  _gBathFunc = gBathymetry;
  _nlFunc = nl;
  _source = source;
  _Dmax = 1e10;
  _initialized = _clipped = _interfaceTermNodal = false;
  _gh = gh;
  _bath = bath;
  temporal[0] = 2;
  _volumeTermNodal = _volumeTermIntegral = _volumeFunction = _mwdFunc = NULL;
  _sqrtSkip = _sqrtSkipA = _sqrtSkipB = 0.;
}


dgTermAssembler *dgConservationLawSWnonInertia::getTerm(const dgGroupOfElements &group) const
{
  checkSetup();
  std::vector<dgTerm*> terms;
  const function *vf0 = functionForTag(_volumeTerm0, group.getPhysicalTag());
  if (vf0)
    terms.push_back(dgIntegralTerm::newPsiTerm(vf0, _nbf, isLinear()));

  if(!_volumeTermIntegral)
  {
    const function *vf1 = functionForTag(_volumeTerm1, group.getPhysicalTag());
    if (vf1)
      terms.push_back(dgIntegralTerm::newGradPsiTerm(vf1, _nbf, isLinear()));
    return new dgTermAssemblerIntegral(terms);
  }

  // Control Volumes method
  dgTermAssembler * classicAssemblers = new dgTermAssemblerIntegral(terms);
  return new dgTermAssemblerControlVolumeFE(_nbf, _volumeTermNodal, _volumeTermIntegral, _height, classicAssemblers);
}


dgFaceTermAssembler *dgConservationLawSWnonInertia::getFaceTerm(const dgGroupOfFaces &group) const
{
  checkSetup();
  const function *normalFluxF = NULL;
  switch (group.nConnection()) {
    case 1 :
      normalFluxF = getBoundaryCondition(group.physicalTag())->getTerm0();
      break;
    case 2 :
      normalFluxF = functionForTag(_interfaceTerm0, group.physicalTag());
      break;
    case 3 :
       normalFluxF = functionForTag(_bifurcationTerm0, group.physicalTag());
       break;
    default : Msg::Fatal("Number of connections invalid: %d", group.nConnection());
  }
  if (normalFluxF) {
    if(!_interfaceTermNodal)
      return new dgFaceTermAssemblerIntegral(dgIntegralTerm::newInterfacePsiTerm(normalFluxF, isLinear()), _nbf);

    // Control Volumes method
    return new dgFaceTermAssemblerControlVolumeFE(normalFluxF, _nbf);
  }
  return NULL;
}


// ##############################################################################
// ###################### CINEMATIC WAVE APPROXIMATION ##########################
// ##############################################################################

class dgConservationLawSWkinematic::gradPsiTerm : public function {
  fullMatrix<double> _nlTerm, _gBath;
  dgConservationLawSWkinematic * _law;
public:
  gradPsiTerm(const function * gBathF, const function * nlF, dgConservationLawSWkinematic * law) : function(3), _law(law) {
    setArgument(_gBath, gBathF);
    setArgument(_nlTerm, nlF);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    for(int i = 0; i < val.size1(); i++) {
      double gx = - _gBath(i, 0);
      double gy = - _gBath(i, 1);
      double norm = sqrt(gx * gx + gy * gy);

      double speed = norm ? _nlTerm(i, 0) / sqrt(norm) : 1e100;
      speed = std::min(_law->getDmax(), speed);
      val(i, 0) = speed * gx;
      val(i, 1) = speed * gy;
      val(i, 2) = 0.;
    }
  }
};

class dgConservationLawSWkinematic::interfaceTerm : public function {
  fullMatrix<double> _normals, _gBathL, _gBathR, _nlL, _nlR, _solL, _solR;
  dgConservationLawSWkinematic * _law;  
public:
  interfaceTerm(const function * gBathF, const function * nlF, dgConservationLawSWkinematic * law) : function(2), _law(law) {
    setArgument(_normals, function::getNormals());
    setArgument(_solL,    function::getSolution(), 0);
    setArgument(_solR,    function::getSolution(), 1);
    setArgument(_gBathL,  gBathF, 0);
    setArgument(_gBathR,  gBathF, 1);
    setArgument(_nlL,     nlF, 0);
    setArgument(_nlR,     nlF, 1);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    for(int i = 0; i < val.size1(); i++) {
      double nx = _normals(i, 0);
      double ny = _normals(i, 1);
      double gx = (_gBathL(i, 0) + _gBathR(i, 0) ) / 2.;
      double gy = (_gBathL(i, 1) + _gBathR(i, 1) ) / 2.;
      double norm = sqrt(gx * gx + gy * gy);
      double grad = gx * nx + gy * ny;
      double fact;
      if(grad < 0.)
        fact = (_nlL(i, 0) + EPS_POW);
      else
        fact = (_nlR(i, 0) + EPS_POW);
      double speed = norm ? fact / sqrt(norm) : 1e100;
      speed = std::min(_law->getDmax(), speed);

      val(i, 0) = speed * grad;
      val(i, 1) = - val(i, 0);
    }
  }
};

class dgConservationLawSWkinematic::interfaceTermBnd : public function {
  fullMatrix<double> _normals, _gBathL, _nlL, _solL;
public:
  interfaceTermBnd(const function * gBathF, const function * nlF) : function(1) {
    setArgument(_normals, function::getNormals());
    setArgument(_solL,    function::getSolution(), 0);
    setArgument(_gBathL,  gBathF, 0);
    setArgument(_nlL,     nlF, 0);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
  
    for(int i = 0; i < val.size1(); i++) {
      double nx = _normals(i, 0);
      double ny = _normals(i, 1);
      double gx = _gBathL(i, 0) ;
      double gy = _gBathL(i, 1) ;
      double norm = sqrt(gx * gx + gy * gy);
      if(norm) {
        gx /= norm;
        gy /= norm;
      }
      else {
        val(i, 0) = val(i, 1) = 0.;
        return;
      }
      double u = gx * nx + gy * ny;
      u *= _nlL(i, 0) + EPS_POW;
      val(i, 0) = u * sqrt(norm);
    }
  }
};
function * dgConservationLawSWkinematic::getNewInterfaceTermBnd(const function * gBathF, const function * nlF) {
  return new interfaceTermBnd(gBathF, nlF);
}

void dgConservationLawSWkinematic::setup() {
  if(_nlFunc == NULL)
    Msg::Error("the nl function has to be declared in SWkinematic !");
  _volumeTerm1[""]    = new gradPsiTerm(_gBathFunc, _nlFunc, this);
  _interfaceTerm0[""] = new interfaceTerm(_gBathFunc, _nlFunc, this);
  _volumeTerm0[""]    = _source;
  if(_clipped) _clipToPhysics[""] = new clipNegative();
  _initialized = true;
}


dgConservationLawSWkinematic::~dgConservationLawSWkinematic() {
  if(_interfaceTerm0[""]) delete _interfaceTerm0[""];
  if(_volumeTerm1[""])    delete _volumeTerm1[""];
}

dgConservationLawSWkinematic::dgConservationLawSWkinematic(const function * gBathymetry, const function * nl,
                                                           const function * source) : dgConservationLawFunction(1) {
  _gBathFunc = gBathymetry;
  _nlFunc = nl;
  _source = source;
  _Dmax = 1e10;
  _initialized = _clipped = false;
}

void dgConservationLawSWkinematic::setSource(const function * source) {
  if(_initialized)
    Msg::Fatal("setSource in SWnonInertia should be used before setup() !");
  _source = source;
}

