#include "dgConservationLawPerfectGasALE.h"
#include "function.h"
#include "functionGeneric.h"
#include "SVector3.h"


class totalForceSurface : public function{
  fullMatrix<double> solution, gradSolution, normals, viscosity;
  double _gamma;
  int _dim;
  public :
  totalForceSurface(int dim, double gamma, const function *viscosityF):function(dim)
  {
    setArgument(solution, function::getSolution());
    setArgument(gradSolution, function::getSolutionGradient());
    setArgument(normals, function::getNormals());
    setArgument(viscosity, viscosityF);
    _gamma = gamma;
    _dim = dim;
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    for (int i = 0; i < val.size1(); ++ i) {
      const double rho = solution(i, 0);
      const double overRho = 1. / rho;
      const double rhoE = solution(i, _dim + 1);
      const double rhou2 = solution(i, 1) * solution(i, 1) + solution(i, 2) * solution(i, 2)
        + ((_dim == 3) ? solution(i, 3) * solution(i, 3) : 0);
      double p = (_gamma - 1) * (rhoE - rhou2 * 0.5 * overRho); 
      const double u = solution(i, 1) * overRho;
      const double v = solution(i, 2) * overRho;
      const double dudx = (gradSolution(i, 3) - gradSolution(i, 0) * u) * overRho;
      const double dudy = (gradSolution(i, 4) - gradSolution(i, 1) * u) * overRho;
      const double dvdx = (gradSolution(i, 6) - gradSolution(i, 0) * v) * overRho;
      const double dvdy = (gradSolution(i, 7) - gradSolution(i, 1) * v) * overRho;
      const double mu = viscosity(i, 0);
      const double nx = normals(i, 0), ny = normals(i, 1), nz = normals(i, 2);
      val(i, 0) = - p * nx + mu * (2 * dudx * nx + (dudy + dvdx) * ny);
      val(i, 1) = - p * ny + mu * ((dudy + dvdx) * nx + 2 * dvdy * ny);
      
      if (_dim == 3) {
        const double w = solution(i, 3) * overRho;
        const double dudz = (gradSolution(i, 5) - gradSolution(i, 2) * u) * overRho;
        const double dvdz = (gradSolution(i, 8) - gradSolution(i, 2) * v) * overRho;
        const double dwdx = (gradSolution(i, 9) - gradSolution(i, 0) * w) * overRho;
        const double dwdy = (gradSolution(i, 10) - gradSolution(i, 1) * w) * overRho;
        const double dwdz = (gradSolution(i, 11) - gradSolution(i, 2) * w) * overRho;
        val(i, 0) += mu * (dudz + dwdx) * nz;
        val(i, 1) += mu * (dvdz + dwdy) * nz;
        val(i, 2) = - p * nz + mu * ((dudz + dwdx) * nx + (dvdz + dwdy) * ny + 2 * dwdz * nz);
      }
      
    }
  }
};

class dgPerfectGasLawALE::viscousNormalStress : public function{
  fullMatrix<double>  normals, gradSolution,solution;
  fullMatrix<double>  mu; 
public :
  viscousNormalStress( const function* muF, const function *gradFunc = NULL ) : function(3)  { //TODO rhoF enlevé
    setArgument (normals,function::getNormals(), 0);
    setArgument(solution, function::getSolution()); 
    setArgument(gradSolution, function::getSolutionGradient());
    setArgument(mu, muF);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    for (size_t i = 0; i < nQP; i++) {
      double nx = -normals(i, 0); //normals should be inward TODO: vérifier 
      double ny = -normals(i, 1);
      
      const double rho = solution(i,0); //TODO
      const double overRho = 1. / rho;
      const double u = solution(i, 1) * overRho;
      const double v = solution(i, 2) * overRho;
      
      const double dudx = (gradSolution(i, 3) - gradSolution(i, 0) * u) * overRho;
      const double dudy = (gradSolution(i, 4) - gradSolution(i, 1) * u) * overRho;
      const double dvdx = (gradSolution(i, 6) - gradSolution(i, 0) * v) * overRho;
      const double dvdy = (gradSolution(i, 7) - gradSolution(i, 1) * v) * overRho;


      val (i,0) = mu(i, 0) * (2.*dudx*nx     + (dudy+dvdx)*ny);   // TODO: -2/3 *( dudx+dvdy )
      val (i,1) = mu(i, 0) * ((dvdx+dudy)*nx + 2.*dvdy*ny);       // TODO: -2/3 *( dudx+dvdy )
      val (i,2) = 0.0;
    }
  }
};

class dgPerfectGasLawALE::inviscousNormalStress : public function{
  fullMatrix<double>  sol, normals;
  double _gamma;
public :
  inviscousNormalStress(double gamma) : function(3)  {
    setArgument (sol, function::getSolution());
    setArgument (normals, function::getNormals(), 0);
    _gamma = gamma;
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    const double GM1 = _gamma-1.;
    for (size_t i = 0; i < nQP; i++) {
      
      double nx = -normals(i, 0);//normals should be inward TODO: vérifier le sens des normales
      double ny = -normals(i, 1);
      const double &r = sol(i,0), &ru = sol(i,1), &rv = sol(i,2), &re = sol(i,3);
      const double invrho = 1./r;
      const double u = ru*invrho;
      const double v = rv*invrho;
      const double rV2 = ru*u+rv*v;
      const double p = GM1*(re-0.5*rV2);
      
      val (i, 0) =  -p*nx;
      val (i, 1) =  -p*ny;
      val (i, 2) = 0.0;
    }
  }
};

class dgPerfectGasLawIPTerm3 : public function {
  fullMatrix<double> diffusiveFluxL, diffusiveFluxR, kappaL, kappaR, muL, muR, solutionL, solutionR, normals, gradsolutionL;
  double _R, _gamma;
 public:
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    fullMatrix<double> ipf = dgConservationLawFunction::muFactor(m);
    for (int iPt = 0; iPt < val.size1(); iPt++) {
      const double mufactor = ipf(iPt, 0);
      const double rhoL = solutionL(iPt, 0);
      const double rhoR = solutionR(iPt, 0);
      const double cv = _R / (_gamma - 1);
      const double ecL = (solutionL(iPt, 1) * solutionL(iPt, 1) + solutionL(iPt, 2) * solutionL(iPt, 2) + solutionL(iPt, 3) * solutionL(iPt, 3)) / (2 * rhoL);
      const double ecR = (solutionR(iPt, 1) * solutionR(iPt, 1) + solutionR(iPt, 2) * solutionR(iPt, 2) + solutionR(iPt, 3) * solutionR(iPt, 3)) / (2 * rhoR);
      const double TL = (solutionL(iPt, 4) - ecL) / (rhoL * cv);
      const double TR = (solutionR(iPt, 4) - ecR) / (rhoR * cv);
      const double muOverRho = std::max(muR(iPt, 0) / rhoR, muL(iPt, 0) / rhoL);
      const double kappa = std::max(kappaR(iPt, 0), kappaL(iPt, 0));
      for (int k = 0; k < 5; k++) {
        double meanNormalFlux = 0;
        for (int iDim = 0; iDim < 3; iDim ++)
          meanNormalFlux += ((diffusiveFluxL(iPt, k + 5 * iDim) + diffusiveFluxR(iPt, k + 5 * iDim)) * normals(iPt, iDim)) / 2;
        val(iPt, k) = -meanNormalFlux;
      }
      double rhoEPenalty = ((TL - TR) * kappa + (ecL - ecR) * std::max(muR(iPt, 0), muL(iPt, 0))) * 0.5 * mufactor;
      rhoEPenalty = (solutionL(iPt, 4)/solutionL(iPt, 0) - solutionR(iPt, 4)/solutionR(iPt, 0))/2 * kappa/cv * mufactor;
      val(iPt, 4) -= rhoEPenalty;
      for(int d = 1; d <= 3; ++d) {
        const double uPenalty = (solutionL(iPt, d) - solutionR(iPt, d)) * 0.5 * mufactor * muOverRho;
        val(iPt, d) -= uPenalty;
      }
    }
  }
  dgPerfectGasLawIPTerm3 (const function *diffusiveFlux, const function *mu, const function *kappa, double R, double gamma):function(5) {
    _R = R;
    _gamma = gamma;
    setArgument(diffusiveFluxL,diffusiveFlux, 0);
    setArgument(diffusiveFluxR,diffusiveFlux, 1);
    setArgument(solutionL,function::getSolution(), 0);
    setArgument(solutionR,function::getSolution(), 1);
    setArgument(kappaL, kappa, 0);
    setArgument(kappaR, kappa, 1);
    setArgument(muL, mu, 0);
    setArgument(muR, mu, 1);
    setArgument(normals, function::getNormals(), 0);
  }
};


class dgPerfectGasLawIPTerm : public function {
  fullMatrix<double> diffusiveFluxL, diffusiveFluxR, kappaL, kappaR, muL, muR, solutionL, solutionR, normals, gradsolutionL;
  double _R, _gamma;
  int _dim;
 public:
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    double nbFields = _dim + 2;
    int dimXYZ = diffusiveFluxR.size2() / nbFields;
    fullMatrix<double> ipf = dgConservationLawFunction::muFactor(m);
    for (int iPt = 0; iPt < val.size1(); iPt++) {
      double mufactor = ipf(iPt, 0);
      const double cv = _R / (_gamma - 1);
      const double ecL = (solutionL(iPt, 1) * solutionL(iPt, 1) + solutionL(iPt, 2) * solutionL(iPt, 2) + ((_dim == 3) ? solutionL(iPt, 3) * solutionL(iPt, 3): 0)) / (2 * solutionL(iPt, 0));
      const double ecR = (solutionR(iPt, 1) * solutionR(iPt, 1) + solutionR(iPt, 2) * solutionR(iPt, 2) + ((_dim == 3) ? solutionR(iPt, 3) * solutionR(iPt, 3): 0)) / (2 * solutionR(iPt, 0));
      const double TL = (solutionL(iPt, _dim + 1) - ecL) / (solutionL(iPt, 0) * cv);
      const double TR = (solutionR(iPt, _dim + 1) - ecR) / (solutionR(iPt, 0) * cv);
      const double muOverRho = std::max(muR(iPt, 0) / solutionR(iPt, 0), muL(iPt, 0) / solutionL(iPt, 0));
      const double kappa = std::max(kappaR(iPt, 0), kappaL(iPt, 0));
      for (int k = 0; k < nbFields; k++) {
        double meanNormalFlux = 0;
        for (int iDim = 0; iDim < dimXYZ; iDim ++)
          meanNormalFlux += ((diffusiveFluxL(iPt, k + nbFields * iDim) + diffusiveFluxR(iPt, k + nbFields * iDim)) * normals(iPt, iDim)) / 2;
        val(iPt, k) = -meanNormalFlux;
      }
      double rhoEPenalty = ((TL - TR) * kappa + (ecL - ecR) * std::max(muR(iPt, 0), muL(iPt, 0))) * 0.5 * mufactor;
      rhoEPenalty = (solutionL(iPt, _dim + 1)/solutionL(iPt, 0) - solutionR(iPt, _dim + 1)/solutionR(iPt, 0))/2 * kappa/cv * mufactor;
      /*static int count = 0;
      if ((count++) % 500000 == 0)
        printf("jump Ec + jump Tcv = %.2e jump E = %.2e // %.2e = %.2e\n", ecL - ecR + (TL - TR) * cv, solutionL(iPt, 3)/solutionL(iPt, 0) - solutionR(iPt, 3)/solutionR(iPt, 0), kappa/cv, std::max(muL(iPt, 0), muR(iPt, 0))); */
      val(iPt, _dim + 1) -= rhoEPenalty;
      for(int d = 1; d <= _dim; ++d) {
        const double uPenalty = (solutionL(iPt, d) - solutionR(iPt, d)) * 0.5 * mufactor * muOverRho;
        val(iPt, d) -= uPenalty;
      }
    }
  }
  dgPerfectGasLawIPTerm (int dim, const function *diffusiveFlux, const function *mu, const function *kappa, double R, double gamma):function((dim+2)), _dim(dim) {
    _R = R;
    _gamma = gamma;
    setArgument(diffusiveFluxL,diffusiveFlux, 0);
    setArgument(diffusiveFluxR,diffusiveFlux, 1);
    setArgument(solutionL,function::getSolution(), 0);
    setArgument(solutionR,function::getSolution(), 1);
    setArgument(kappaL, kappa, 0);
    setArgument(kappaR, kappa, 1);
    setArgument(muL, mu, 0);
    setArgument(muR, mu, 1);
    setArgument(normals, function::getNormals(), 0);
  }
};


class dgPerfectGasLawALE::velocityVector : public function{
  fullMatrix<double>  solution, meshVel;
  double _dim;
public :
  velocityVector(const double dim) : function(3)  {
    setArgument (solution, function::getSolution());
    _dim = dim;
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP =val.size1();
    for (size_t i=0; i<nQP; i++) {
      double rho = solution(i,0);
      double rhou = solution(i,1);
      double rhov = solution(i,2);
      double rhow =  solution(i,3);
      if (_dim ==2) rhow = 0.0 ;
      val (i,0) = rhou/rho;
      val (i,1) = rhov/rho;
      val (i,2) = rhow/rho;
    }
  }
};

/*
\rho \left(\frac{\partial \mathbf{v}}{\partial t} + \mathbf{v} \cdot \nabla \mathbf{v}\right) = -\nabla p + \mu \nabla^2 \mathbf{v} + \left( \frac13 \mu + \mu^v) \nabla (\nabla \cdot \mathbf{v} \right) + \rho \mathbf{f} 
*/


class dgPerfectGasLawALE::advection : public function {
  fullMatrix<double> sol, meshVel;//,coord;
  double _gamma;
  public:
  advection(double gamma_, const function *meshVelFunction): function(12) {
    setArgument(sol, function::getSolution());
    setArgument(meshVel, meshVelFunction);
//    setArgument(coord, function::getCoordinates());
    _gamma = gamma_;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    const size_t nQP = val.size1();      
    const double GM1 = _gamma-1.;
    for (size_t k = 0 ; k < nQP; k++ ){
      const double &r = sol(k,0), &ru = sol(k,1), &rv = sol(k,2), &re = sol(k,3);
      const double invrho = 1./r;
      const double u = ru*invrho;
      const double v = rv*invrho;
      const double urel = u-meshVel(k,0);
      const double vrel = v-meshVel(k,1);
//      const double q11 = ru*urel;
//      const double q12 = rv*urel;
//      const double q21 = ru*vrel;
//      const double q22 = rv*vrel;
      const double rV2 = ru*u+rv*v; //the pression is unchanged
      const double p = GM1*(re-0.5*rV2);
//      const double H = invrho*(re+p); // specific enthalpy
//      if (k == 0)
//      {   printf("\ncoord  = %f, %f ",coord(k,0) ,coord(k,1) );
//          printf("\nvel  = %f, %f ",u ,v);
//          printf("\nmeshvel  = %f, %f ", meshVel(k,0),meshVel(k,1));
//          getchar();
//      }
      val(k,0)  = r*urel;
      val(k,1)  = ru*urel   +   p;
      val(k,2)  = rv*urel;
      val(k,3)  = re*urel   +   u*p;

      val(k,0+4) = r*vrel;
      val(k,1+4) = ru*vrel;
      val(k,2+4) = rv*vrel  +   p;
      val(k,3+4) = re*vrel  +   v*p;

      val(k,0+8) = 0.;
      val(k,1+8) = 0.;
      val(k,2+8) = 0.;
      val(k,3+8) = 0.;
    }
  }
};

class dgPerfectGasLawALE::diffusion : public function {
  fullMatrix<double> sol, grad, mu, kappa;
  double _cv;
  public:
  diffusion (const function * muFunction, const function * kappaFunction, double cv):function(12) {
    setArgument (sol, function::getSolution());
    setArgument (grad, function::getSolutionGradient());
    setArgument (mu, muFunction);
    setArgument (kappa, kappaFunction);
    _cv = cv;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    const size_t nQP = val.size1();      
    for (size_t k = 0 ; k < nQP; k++ ){
      const double muk    = mu(k,0);
      const double kappak = kappa(k,0);      
      //      printf("%g %g grad rho (%g %g)\n",muk,kappak,grad(k*3+0,0),grad(k*3+1,0));
    // find gradients of primitive variables    
      const double overRho = 1./sol(k,0);
      const double u = sol(k,1) * overRho;
      const double v = sol(k,2) * overRho;
      const double e = sol(k,3) * overRho;
      // grad u =  (grad (\rho u) - (grad\rho) u) / \rho
      const double drhodx = grad(k, 0);
      const double drhody = grad(k, 1);
      const double dudx = (grad(k,3) - drhodx * u) * overRho;
      const double dudy = (grad(k,4) - drhody * u) * overRho;
      const double dvdx = (grad(k,6) - drhodx * v) * overRho;
      const double dvdy = (grad(k,7) - drhody * v) * overRho;
      const double dedx = (grad(k,9) - drhodx * e) * overRho;
      const double dedy = (grad(k,10)- drhody * e) * overRho;
      // shear stress tensor - Newtonian fluid without bulk viscosity (\lambda=-\mu)  
      const double tauxx = muk * (dudx - dvdy);
      const double tauxy = muk * (dudy + dvdx);
      const double tauyy = - tauxx;
    
      val(k, 0) = 0.;
      val(k, 4) = 0.;
      val(k, 8) = 0.;

      val(k,1)   = -tauxx;
      val(k,1+4) = -tauxy;
      val(k,1+8) = 0;

      val(k,2)   = -tauxy;
      val(k,2+4) = -tauyy;    
      val(k,2+8) = 0;
    
      // k grad T  
      val(k,3+0)    = -kappak / _cv * (dedx - u * dudx - v * dvdx);
      val(k,3+4)    = -kappak / _cv * (dedy - u * dudy - v * dvdy);
      val(k,3+8)    = 0;
      
      // v . tau - momentum dissipation
      val(k,3)      -= (u * tauxx + v * tauxy);
      val(k,3+4)    -= (u * tauxy + v * tauyy);         
      
      //      printf("%g %g %g %g %g %g %g %g \n",val(k,0),val(k,1),val(k,2),val(k,3),val(k,4),val(k,5),val(k,6),val(k,7));
    }
  }
};

class dgPerfectGasLawALE::source : public function {
  fullMatrix<double> sol,s, meshVelDiv;
  int _nbf;
  bool hasSource;
public:
  source(const function *meshVelDivFunc, int nbf, const function *sourceFunction):function(nbf), _nbf(nbf) {
    setArgument (sol, function::getSolution());
    setArgument (meshVelDiv, meshVelDivFunc); 
    
    if (sourceFunction) {
    
      setArgument (s, sourceFunction);
      hasSource = true;
      
    }else 
    {
        hasSource = false;
    } 
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    const size_t nQP = val.size1();      
    for (size_t k = 0 ; k < nQP; k++ ){
      for (int iF=0;iF<_nbf;iF++){
        
        val(k,iF) = - meshVelDiv(k,0)*sol(k,iF); 

        if (hasSource) 
        {
            val(k,iF) = val(k,iF) + sol(k,iF)*s(0,iF);
        }
        
      }
    }
  }
};

class dgPerfectGasLawALE::clipToPhysics : public function {
  fullMatrix<double> sol;
  double _presMin, _rhoMin, _gamma;
public:
  clipToPhysics(double presMin, double rhoMin, double gamma_): function(4)  {
    setArgument (sol, function::getSolution());
    _presMin=presMin;
    _rhoMin=rhoMin;
    _gamma = gamma_;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    const size_t nDofs = val.size1();
    for (size_t k = 0 ; k < nDofs; k++ ){
      val(k,0) = sol(k,0);
      val(k,1) = sol(k,1);
      val(k,2) = sol(k,2);
      val(k,3) = sol(k,3);
      if (sol(k,0) < _rhoMin){
        //printf("CL: clip rho min =%g \n", _rhoMin);
        val(k,0) = _rhoMin;
      }
      double rhoV2 = sol(k,1)*sol(k,1)+sol(k,2)*sol(k,2);
      rhoV2 /= sol(k,0);
      const double p = (_gamma-1)*(sol(k,3) - 0.5*rhoV2);
      if (p < _presMin) {
        val(k,3) = _presMin / (_gamma-1) +  0.5 *rhoV2 ; 
        //printf("CL: clip pres min =%g \n ", val(k,3));
      }
    }
  }
};

inline void _LaxFriedrichsEuler(double _gamma, double nx, double ny, double uMesh,double vMesh,
                                double rL, double ruL, double rvL, double reL,
                                double rR, double ruR, double rvR, double reR,
                                double &F0, double &F1, double &F2, double &F3)
{

  const double GM1      = _gamma-1.;

  // Left flux & max. convective speed
  const double invrL    = 1./rL;
  const double uL       = ruL*invrL;      
  const double vL       = rvL*invrL;
  const double urelL    = uL-uMesh;
  const double vrelL    = vL-vMesh;
  const double rVSqL    = ruL*uL+rvL*vL;                         // Density*velocity^2
  const double pL       = GM1*(reL-0.5*rVSqL);                                    // Pressure
  

  const double Fx0L     = rL *urelL;
  const double Fx1L     = ruL*urelL     +   pL;
  const double Fx2L     = rvL*urelL;
  const double Fx3L     = reL*urelL     +   uL*pL;   // Flux in x-direction

  const double Fy0L     = rL *vrelL;
  const double Fy1L     = ruL*vrelL;
  const double Fy2L     = rvL*vrelL     +   pL;
  const double Fy3L     = reL*vrelL     +   vL*pL;   // Flux in y-direction
  
  const double maxCSL   = sqrt(_gamma*pL*invrL)+sqrt(urelL*urelL+vrelL*vrelL);//+sqrt(uMesh*uMesh+vMesh*vMesh);            // Max. convective speed

  // Right flux & max. convective speed
  const double invrR    = 1./rR;
  const double uR       = ruR*invrR;      
  const double vR       = rvR*invrR;
  const double urelR    = uR-uMesh;
  const double vrelR    = vR-vMesh;
  const double rVSqR    = ruR*uR+rvR*vR;                                         // Density*velocity^2
  const double pR       = GM1*(reR-0.5*rVSqR);                            // Specific enthalpy
  
  const double Fx0R     = rR *urelR;
  const double Fx1R     = ruR*urelR     +   pR;
  const double Fx2R     = rvR*urelR;
  const double Fx3R     = reR*urelR     +   uR*pR;   // Flux in x-direction

  const double Fy0R     = rR *vrelR;
  const double Fy1R     = ruR*vrelR;
  const double Fy2R     = rvR*vrelR     +   pR;
  const double Fy3R     = reR*vrelR     +   vR*pR;   // Flux in y-direction
  
  const double maxCSR   = sqrt(_gamma*pR*invrR)+sqrt(urelR*urelR+vrelR*vrelR);//+sqrt(uMesh*uMesh+vMesh*vMesh);            // Max. convective speed

  const double alpha    = std::max(maxCSL,maxCSR);                           // Relaxation constant in Lax-Friedrichs

  F0 = -0.5*((Fx0R+Fx0L)*nx+(Fy0R+Fy0L)*ny-alpha*(rR-rL)); 
  F1 = -0.5*((Fx1R+Fx1L)*nx+(Fy1R+Fy1L)*ny-alpha*(ruR-ruL));
  F2 = -0.5*((Fx2R+Fx2L)*nx+(Fy2R+Fy2L)*ny-alpha*(rvR-rvL));
  F3 = -0.5*((Fx3R+Fx3L)*nx+(Fy3R+Fy3L)*ny-alpha*(reR-reL));

}

class dgPerfectGasLawALE::riemannLF : public function {
  fullMatrix<double> normals, solL, solR, meshVel;
  double _gamma;
  public:
  riemannLF(double gamma_, const function *meshVelFunction):function(4) {
    setArgument (normals, function::getNormals(), 0);
    setArgument (solL, function::getSolution(), 0);
    setArgument (solR, function::getSolution(), 1);
    setArgument (meshVel, meshVelFunction);
    _gamma = gamma_;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++) {
      _LaxFriedrichsEuler(_gamma, normals(i,0), normals(i,1),meshVel(i,0),meshVel(i,1),
                          solL(i,0), solL(i,1), solL(i,2), solL(i,3),
                          solR(i,0), solR(i,1), solR(i,2), solR(i,3),
                          val(i,0), val(i,1), val(i,2), val(i,3));
    }
  }
};



class dgPerfectGasLawALE::maxConvectiveSpeed : public function {
  fullMatrix<double> sol, meshVel;
  double _gamma;
  public:
  maxConvectiveSpeed(double gamma_, const function *meshVelFunction):function(1) {
    setArgument (sol, function::getSolution());
    setArgument (meshVel, meshVelFunction);
    _gamma = gamma_;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++) {
      const double u  = sol(i,1)/sol(i,0);
      const double v  = sol(i,2)/sol(i,0);
      const double uMesh  = meshVel(i,0);
      const double vMesh  = meshVel(i,1);
      const double v2 =  u*u+v*v; 
      const double v2Mesh = uMesh*uMesh + vMesh*vMesh;
      const double rhov2= sol(i,1)*u+sol(i,2)*v;
      const double p  = (_gamma-1.0)*(sol(i,3) - 0.5 * rhov2);
      //      printf("p = %g %g %g %g %g\n",p,val(i,0),val(i,1),val(i,2),val(i,3));
      const double c   = sqrt(_gamma*p/sol(i,0));
      val(i,0) = c+ sqrt(v2) + sqrt(v2Mesh); // ne change rien au problème car la fonction n'est pas utilisée
    }
  }
};


class dgPerfectGasLawALE::diffusivity : public function {
  fullMatrix<double> mu, kappa, solution;
  double _cv;
  int _dim;
  public:
  diffusivity(const function *muFunction, const function *kappaFunction, double cv, int dim):function(dim+2) {
    setArgument (mu, muFunction);
    setArgument (kappa, kappaFunction);
    setArgument (solution, function::getSolution());
    _dim = dim;
    _cv = cv;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++) {
      double rho = solution(i,0);
      val(i,0) = 0.0;
      for (int k = 1; k <= _dim; k++)
        val(i,k) = mu(i,0)/rho;
      val(i,_dim + 1) = kappa(i,0);// / (_cv * rho);
    }
  }
};


void dgPerfectGasLawALE::setup() {
  if (_R == 0)
    Msg::Fatal("Gas constant and gamma must be set");
  double cv = _R / (_gamma - 1);
  if (_dim == 3 ) {
    Msg::Fatal("dgPerfectGasLawALE not implemented in 3D!!");
  }
  else {
    _maximumConvectiveSpeed[""] = new maxConvectiveSpeed(_gamma, _meshVelocity);
    _clipToPhysics[""] = new clipToPhysics(1e-3, 1e-3, _gamma);
    _riemannTerm = new riemannLF(_gamma, _meshVelocity);
    _convectiveFlux = new advection(_gamma,_meshVelocity);
    if (_muFunction && _kappaFunction)
      _diffusiveFlux = new diffusion (_muFunction, _kappaFunction, cv);
  }
  _volumeTerm0[""] = new source (_meshVelocityDiv, _nbf, _sourceFunction);
  if (_muFunction && _kappaFunction) {
    _diffusivity[""] = new diffusivity (_muFunction, _kappaFunction, cv,  _dim);
    if (_dim == 3)
      _IPTerm = new dgPerfectGasLawIPTerm3 (_diffusiveFlux, _muFunction, _kappaFunction, _R, _gamma);
    else
      _IPTerm = new dgPerfectGasLawIPTerm (_dim, _diffusiveFlux, _muFunction, _kappaFunction, _R, _gamma);
    _volumeTerm1[""] = functionSumNew(_convectiveFlux, _diffusiveFlux);
    _interfaceTerm0[""] = functionSumNew(_riemannTerm, _IPTerm);
    //_interfaceTerm1N[""] = dgNewSymmetricIpTerm(_dim + 2, _muFunction);
  } else {
    _volumeTerm1[""] = _convectiveFlux;
    _interfaceTerm0[""] = _riemannTerm;
  }
  _velocitySol = new velocityVector(_dim);
  _totalForceSurface = new totalForceSurface(_dim, _gamma, _muFunction);
  _viscousNS = new viscousNormalStress(_muFunction); //TODO
  _inviscousNS = new inviscousNormalStress(_gamma); //TODO
}


dgPerfectGasLawALE::~dgPerfectGasLawALE() {
  if (_volumeTerm1[""]) delete _volumeTerm1[""];
  if (_interfaceTerm0[""]) delete _interfaceTerm0[""];
  if (_clipToPhysics[""]) delete _clipToPhysics[""];
  if (_volumeTerm0[""]) delete _volumeTerm0[""];
  if (_maximumConvectiveSpeed[""]) delete _maximumConvectiveSpeed[""];
  if (_diffusivity[""]) delete _diffusivity[""];
  if (_interfaceTerm1N[""]) delete _interfaceTerm1N[""];
  if (_IPTerm) {
    delete _IPTerm;
    delete _riemannTerm;
  }
  if (_diffusiveFlux) {
    delete _diffusiveFlux;
    delete _convectiveFlux;
  }
}


dgPerfectGasLawALE::dgPerfectGasLawALE(int d) : dgConservationLawFunction((d == 3) ? 5 : 4), _dim(d) {
  // \rho \rho u \rho v \rho e
  function *fzero = new functionConstant(0.);
  _kappaFunction = fzero;
  _muFunction = fzero;
  _R = 0;
  std::vector<double> zerov(_nbf);
  _sourceFunction = new functionConstant(zerov);;
}

//-------------------------------------------------------------------------------
// A class for slip and non slip walls
// could easily add moving walls ...
//-------------------------------------------------------------------------------
class dgBoundaryConditionPerfectGasLawALE2dWall : public dgBoundaryCondition {
  class fullTerm : public function {
    functionReplace diffusionReplace, advectionReplace;
    fullMatrix<double> solIn, normals, riemann, ip, solOutDiff, solOutAdv, diffFluxOut, diffFluxIn, wallVelocity;
    bool _nonSlip;
    double _gamma;
    public:
    fullTerm (bool nonSlip, dgPerfectGasLawALE *claw, const function *wallVelocityF) : function (4) {
      _gamma = claw->getGamma();
      _nonSlip = nonSlip;
      setArgument (normals, function::getNormals());
      setArgument (solIn, function::getSolution(), 0);
      advectionReplace.addChild();
      addFunctionReplace (advectionReplace);
      advectionReplace.replace (solOutAdv, function::getSolution(), 1);
      advectionReplace.get (riemann, claw->_riemannTerm);
      setArgument (wallVelocity, wallVelocityF);
      if (_nonSlip) {
        diffusionReplace.addChild();
        addFunctionReplace (diffusionReplace);
        diffusionReplace.replace (solOutDiff, function::getSolution(), 1);
        diffusionReplace.replace (diffFluxOut, claw->_diffusiveFlux, 1);
        diffusionReplace.get (ip, claw->_IPTerm);
        setArgument (diffFluxIn, claw->_diffusiveFlux, 0);
      }
    }
    void call (dataCacheMap *m, fullMatrix<double> &val) {
      size_t nQP = val.size1();
      // 1) fill the external values
      for (size_t i=0; i< nQP; i++) {
        const double nx = normals (i,0);
        const double ny = normals (i,1);
        double vn = solIn (i,1) * nx + solIn (i,2) * ny;
        //advective flux
        solOutAdv (i,0) = solIn (i,0);
        solOutAdv (i,1) = solIn (i,1) - 2 * vn * nx;
        solOutAdv (i,2) = solIn (i,2) - 2 * vn * ny;
        solOutAdv (i,3) = solIn (i,3);
        
        const double vnMesh = wallVelocity (i,0) * nx + wallVelocity (i,1) * ny;
       
        solOutAdv (i,1) += 2 * vnMesh * nx;//vnorm
        solOutAdv (i,2) += 2 * vnMesh * ny;
        
        //diffusive flux (only for non slip conditions)
        if (_nonSlip) {
          //-------------------------------------------------------------------------------
          // Non Slip Dirichlet BC --
          // What to put on the right side of the IP (no velocity, same density) 
          // Assume Adiabatic Wall --> no heat transfer -> \partial_n T = 0
          // So, we use the same temperature as inside
          //-------------------------------------------------------------------------------
          solOutDiff (i,0) = solIn (i,0);
          solOutDiff (i,3) = solIn (i,3) - 0.5 * (solIn(i,1) * solIn(i,1) + solIn(i,2) * solIn(i,2))/solIn(i,0);
          solOutDiff (i,1) = solIn (i,0) * wallVelocity (i,0);
          solOutDiff (i,2) = solIn (i,0) * wallVelocity (i,1);
          solOutDiff (i,3) += 0.5 * (wallVelocity (i,0) * wallVelocity (i,0) + wallVelocity(i,1) * wallVelocity(i,1))*solIn(i,0);
          //-------------------------------------------------------------------------------
          // Non Slip Neumann BC --
          // Compute normal diffusive fluxes at the boundary
          // Assume Adiabatic Wall --> no heat transfer -> no flux for component 3 (in 2D)
          // we re-use the IP function to compute the complete diffusive flux on the boundary
          //-------------------------------------------------------------------------------
          for (int k=0; k<2; k++) {
            diffFluxOut (i, 0 + k*4) = diffFluxIn (i, 0 + k*4);
            diffFluxOut (i, 1 + k*4) = diffFluxIn (i, 1 + k*4);
            diffFluxOut (i, 2 + k*4) = diffFluxIn (i, 2 + k*4);
            diffFluxOut (i, 3 + k*4) = -diffFluxIn (i, 3 + k*4);
          }
        }
      }
      // 2) compute the term
      if (_nonSlip) {
        advectionReplace.compute();
        diffusionReplace.compute();
        for (size_t i = 0; i< nQP; i++) {

          double FLUX[4];
          _LaxFriedrichsEuler(_gamma, normals(i,0), normals(i,1),wallVelocity (i,0) ,wallVelocity(i,1), //TODO :  adapter au conditions aux frontières
                              solIn(i,0), solIn(i,1), solIn(i,2), solIn(i,3),
                              solOutAdv(i,0), solOutAdv(i,1), solOutAdv(i,2), solOutAdv(i,3),
                              FLUX[0], FLUX[1], FLUX[2], FLUX[3]);

          for (int j = 0; j < 4; j ++) 
            val (i, j) = FLUX[j] + ip (i, j);
        }
      } else {
        advectionReplace.compute();
        for (size_t i = 0; i< nQP; i++)
          for (size_t j = 0; j < 4; j ++) 
            val (i, j) = riemann (i, j);
      }
    }
  };
  public:
  dgBoundaryConditionPerfectGasLawALE2dWall(int type, dgPerfectGasLawALE *claw, const function *velocityF){
    claw->checkSetup();
    switch (type) {
      case 1 : _term0 = new fullTerm (false, claw, NULL); break;
      case 2 : _term0 = new fullTerm (true, claw, NULL); break;
      case 3 : _term0 = new fullTerm (true, claw, velocityF); break;
      throw;
    }
  }
  ~dgBoundaryConditionPerfectGasLawALE2dWall(){
    delete _term0;
  }
};


dgBoundaryCondition *dgPerfectGasLawALE::newNonSlipWallBoundary(const function *meshVelFunc) {
  if(_dim == 2)
  return new dgBoundaryConditionPerfectGasLawALE2dWall(3, this, meshVelFunc);
  else Msg::Fatal("newNonSlipWallBoundary not implemented in 3D!!");
  return 0;
//  else return new dgBoundaryConditionPerfectGasLaw3dWall(2, this, NULL);
}


dgBoundaryCondition *dgPerfectGasLawALE::newSlipWallBoundary(const function *meshVelFunc) {
  if(_dim == 2)
  return new dgBoundaryConditionPerfectGasLawALE2dWall(1, this, meshVelFunc);
  else Msg::Fatal("newSlipWallBoundary not implemented in 3D!!");
  return 0;
//  else return new dgBoundaryConditionPerfectGasLaw3dWall(1, this, NULL);
}
