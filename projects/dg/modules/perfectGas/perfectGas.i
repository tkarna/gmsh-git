%module perfectGas

%{
  #undef HAVE_DLOPEN
  #include "dgConservationLawPerfectGas.h"
  #include "dgConservationLawPerfectGasALE.h"
%}

%import(module="dgpy.conservationLaw") "dgConservationLaw.h"
%import(module="dgpy.conservationLaw") "dgConservationLawFunction.h"

%include "dgConservationLawPerfectGas.h"
%include "dgConservationLawPerfectGasALE.h"
