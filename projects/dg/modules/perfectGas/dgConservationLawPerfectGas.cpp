#include "dgConservationLawPerfectGas.h"
#include "function.h"
#include "functionGeneric.h"
#include "SVector3.h"
class totalForceSurface : public function{
  fullMatrix<double> solution, gradSolution, normals, viscosity;
  double _gamma;
  int _dim;
  public :
  totalForceSurface(int dim, double gamma, const function *viscosityF):function(dim)
  {
    setArgument(solution, function::getSolution());
    setArgument(gradSolution, function::getSolutionGradient());
    setArgument(normals, function::getNormals());
    setArgument(viscosity, viscosityF);
    _gamma = gamma;
    _dim = dim;
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    for (int i = 0; i < val.size1(); ++ i) {
      const double rho = solution(i, 0);
      const double overRho = 1. / rho;
      const double rhoE = solution(i, _dim + 1);
      const double rhou2 = solution(i, 1) * solution(i, 1) + solution(i, 2) * solution(i, 2)
        + ((_dim == 3) ? solution(i, 3) * solution(i, 3) : 0);
      double p = (_gamma - 1) * (rhoE - rhou2 * 0.5 * overRho); 
      const double u = solution(i, 1) * overRho;
      const double v = solution(i, 2) * overRho;
      const double dudx = (gradSolution(i, 3) - gradSolution(i, 0) * u) * overRho;
      const double dudy = (gradSolution(i, 4) - gradSolution(i, 1) * u) * overRho;
      const double dvdx = (gradSolution(i, 6) - gradSolution(i, 0) * v) * overRho;
      const double dvdy = (gradSolution(i, 7) - gradSolution(i, 1) * v) * overRho;
      const double mu = viscosity(i, 0);
      const double nx = normals(i, 0), ny = normals(i, 1), nz = normals(i, 2);
      val(i, 0) = - p * nx + mu * (2 * dudx * nx + (dudy + dvdx) * ny);
      val(i, 1) = - p * ny + mu * ((dudy + dvdx) * nx + 2 * dvdy * ny);
      if (_dim == 3) {
        const double w = solution(i, 3) * overRho;
        const double dudz = (gradSolution(i, 5) - gradSolution(i, 2) * u) * overRho;
        const double dvdz = (gradSolution(i, 8) - gradSolution(i, 2) * v) * overRho;
        const double dwdx = (gradSolution(i, 9) - gradSolution(i, 0) * w) * overRho;
        const double dwdy = (gradSolution(i, 10) - gradSolution(i, 1) * w) * overRho;
        const double dwdz = (gradSolution(i, 11) - gradSolution(i, 2) * w) * overRho;
        val(i, 0) += mu * (dudz + dwdx) * nz;
        val(i, 1) += mu * (dvdz + dwdy) * nz;
        val(i, 2) = - p * nz + mu * ((dudz + dwdx) * nx + (dvdz + dwdy) * ny + 2 * dwdz * nz);
      }
    }
  }
};

class dgPerfectGasLawIPTerm3 : public function {
  fullMatrix<double> diffusiveFluxL, diffusiveFluxR, kappaL, kappaR, muL, muR, solutionL, solutionR, normals, gradsolutionL;
  double _R, _gamma;
 public:
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    fullMatrix<double> ipf = dgConservationLawFunction::muFactor(m);
    for (int iPt = 0; iPt < val.size1(); iPt++) {
      const double mufactor = ipf(iPt, 0);
      const double rhoL = solutionL(iPt, 0);
      const double rhoR = solutionR(iPt, 0);
      const double cv = _R / (_gamma - 1);
      const double ecL = (solutionL(iPt, 1) * solutionL(iPt, 1) + solutionL(iPt, 2) * solutionL(iPt, 2) + solutionL(iPt, 3) * solutionL(iPt, 3)) / (2 * rhoL);
      const double ecR = (solutionR(iPt, 1) * solutionR(iPt, 1) + solutionR(iPt, 2) * solutionR(iPt, 2) + solutionR(iPt, 3) * solutionR(iPt, 3)) / (2 * rhoR);
      const double TL = (solutionL(iPt, 4) - ecL) / (rhoL * cv);
      const double TR = (solutionR(iPt, 4) - ecR) / (rhoR * cv);
      const double muOverRho = std::max(muR(iPt, 0) / rhoR, muL(iPt, 0) / rhoL);
      const double kappa = std::max(kappaR(iPt, 0), kappaL(iPt, 0));
      for (int k = 0; k < 5; k++) {
        double meanNormalFlux = 0;
        for (int iDim = 0; iDim < 3; iDim ++)
          meanNormalFlux += ((diffusiveFluxL(iPt, k + 5 * iDim) + diffusiveFluxR(iPt, k + 5 * iDim)) * normals(iPt, iDim)) / 2;
        val(iPt, k) = -meanNormalFlux;
      }
      double rhoEPenalty = ((TL - TR) * kappa + (ecL - ecR) * std::max(muR(iPt, 0), muL(iPt, 0))) * 0.5 * mufactor;
      rhoEPenalty = (solutionL(iPt, 4)/solutionL(iPt, 0) - solutionR(iPt, 4)/solutionR(iPt, 0))/2 * kappa/cv * mufactor;
      val(iPt, 4) -= rhoEPenalty;
      for(int d = 1; d <= 3; ++d) {
        const double uPenalty = (solutionL(iPt, d) - solutionR(iPt, d)) * 0.5 * mufactor * muOverRho;
        val(iPt, d) -= uPenalty;
      }
    }
  }
  dgPerfectGasLawIPTerm3 (const function *diffusiveFlux, const function *mu, const function *kappa, double R, double gamma):function(5) {
    _R = R;
    _gamma = gamma;
    setArgument(diffusiveFluxL,diffusiveFlux, 0);
    setArgument(diffusiveFluxR,diffusiveFlux, 1);
    setArgument(solutionL,function::getSolution(), 0);
    setArgument(solutionR,function::getSolution(), 1);
    setArgument(kappaL, kappa, 0);
    setArgument(kappaR, kappa, 1);
    setArgument(muL, mu, 0);
    setArgument(muR, mu, 1);
    setArgument(normals, function::getNormals(), 0);
  }
};

class dgPerfectGasLawIPTerm : public function {
  fullMatrix<double> diffusiveFluxL, diffusiveFluxR, kappaL, kappaR, muL, muR, solutionL, solutionR, normals, gradsolutionL;
  double _R, _gamma;
  int _dim;
 public:
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    double nbFields = _dim + 2;
    int dimXYZ = diffusiveFluxR.size2() / nbFields;
    fullMatrix<double> ipf = dgConservationLawFunction::muFactor(m);
    for (int iPt = 0; iPt < val.size1(); iPt++) {
      double mufactor = ipf(iPt, 0);
      const double cv = _R / (_gamma - 1);
      const double ecL = (solutionL(iPt, 1) * solutionL(iPt, 1) + solutionL(iPt, 2) * solutionL(iPt, 2) + ((_dim == 3) ? solutionL(iPt, 3) * solutionL(iPt, 3): 0)) / (2 * solutionL(iPt, 0));
      const double ecR = (solutionR(iPt, 1) * solutionR(iPt, 1) + solutionR(iPt, 2) * solutionR(iPt, 2) + ((_dim == 3) ? solutionR(iPt, 3) * solutionR(iPt, 3): 0)) / (2 * solutionR(iPt, 0));
      const double TL = (solutionL(iPt, _dim + 1) - ecL) / (solutionL(iPt, 0) * cv);
      const double TR = (solutionR(iPt, _dim + 1) - ecR) / (solutionR(iPt, 0) * cv);
      const double muOverRho = std::max(muR(iPt, 0) / solutionR(iPt, 0), muL(iPt, 0) / solutionL(iPt, 0));
      const double kappa = std::max(kappaR(iPt, 0), kappaL(iPt, 0));
      for (int k = 0; k < nbFields; k++) {
        double meanNormalFlux = 0;
        for (int iDim = 0; iDim < dimXYZ; iDim ++)
          meanNormalFlux += ((diffusiveFluxL(iPt, k + nbFields * iDim) + diffusiveFluxR(iPt, k + nbFields * iDim)) * normals(iPt, iDim)) / 2;
        val(iPt, k) = -meanNormalFlux;
      }
      double rhoEPenalty = ((TL - TR) * kappa + (ecL - ecR) * std::max(muR(iPt, 0), muL(iPt, 0))) * 0.5 * mufactor;
      rhoEPenalty = (solutionL(iPt, _dim + 1)/solutionL(iPt, 0) - solutionR(iPt, _dim + 1)/solutionR(iPt, 0))/2 * kappa/cv * mufactor;
      /*static int count = 0;
      if ((count++) % 500000 == 0)
        printf("jump Ec + jump Tcv = %.2e jump E = %.2e // %.2e = %.2e\n", ecL - ecR + (TL - TR) * cv, solutionL(iPt, 3)/solutionL(iPt, 0) - solutionR(iPt, 3)/solutionR(iPt, 0), kappa/cv, std::max(muL(iPt, 0), muR(iPt, 0))); */
      val(iPt, _dim + 1) -= rhoEPenalty;
      for(int d = 1; d <= _dim; ++d) {
        const double uPenalty = (solutionL(iPt, d) - solutionR(iPt, d)) * 0.5 * mufactor * muOverRho;
        val(iPt, d) -= uPenalty;
      }
    }
  }
  dgPerfectGasLawIPTerm (int dim, const function *diffusiveFlux, const function *mu, const function *kappa, double R, double gamma):function((dim+2)), _dim(dim) {
    _R = R;
    _gamma = gamma;
    setArgument(diffusiveFluxL,diffusiveFlux, 0);
    setArgument(diffusiveFluxR,diffusiveFlux, 1);
    setArgument(solutionL,function::getSolution(), 0);
    setArgument(solutionR,function::getSolution(), 1);
    setArgument(kappaL, kappa, 0);
    setArgument(kappaR, kappa, 1);
    setArgument(muL, mu, 0);
    setArgument(muR, mu, 1);
    setArgument(normals, function::getNormals(), 0);
  }
};

class dgPerfectGasLaw::velocityVector : public function{
  fullMatrix<double>  solution;
  double _dim;
public :
  velocityVector(const double dim) : function(3)  {
    setArgument (solution, function::getSolution());
    _dim = dim;
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP =val.size1();
    for (size_t i=0; i<nQP; i++) {
      double rho = solution(i,0);
      double rhou = solution(i,1);
      double rhov = solution(i,2);
      double rhow =  solution(i,3);
      if (_dim ==2) rhow = 0.0 ;
      val (i,0) = rhou/rho;
      val (i,1) = rhov/rho;
      val (i,2) = rhow/rho;
    }
  }
};

/*
\rho \left(\frac{\partial \mathbf{v}}{\partial t} + \mathbf{v} \cdot \nabla \mathbf{v}\right) = -\nabla p + \mu \nabla^2 \mathbf{v} + \left( \frac13 \mu + \mu^v) \nabla (\nabla \cdot \mathbf{v} \right) + \rho \mathbf{f} 
*/

#define RIEM_TOL   ((double)1e-12)       /* Rel error to achieve in pstar */
#define RIEM_ITER  (20)                   /* Max number of iterations */

#define smallu ((double)1e-14)
#define smallp ((double)1e-14)

#define max2(a,b)  ( (a)>(b) ? (a) : (b) )
#define max3(a,b,c) ( max2( a , max2(b,c) ) )

bool riemannExact( double GAMMA,
                   double rho1,                /* inputs */
                   double  u1,
                   double  v1,
                   double  w1,
                   double  p1,
                   double rho2,
                   double  u2,
                   double  v2,
                   double  w2,
                   double  p2,
                   double *rhoav,               /* outputs */
                   double  *uav,
                   double  *utav,
                   double  *uttav,
                   double  *pav ) 
{
  
  /*
   * local variables (previously in commons)
   */
  // double rho,u,p,ut,utt;
    
  int n;
  double   plft, vlft, ulft, utlft, uttlft, clft;
  double   prght, vrght, urght, utrght, uttrght, crght;
  double   pstar, ustar, rhostr, cestar, westar, vstar;
  double   rhos, us, uts, utts, ps;
  double   ws, vs, wes, ces;
  double   wlft, wrght;
  double   scrch1, scrch2, scrch3, scrch4;
  double   prevpstar, relpstar;
  // double Q[4];


  //
  // rml - convert to flash unknowns;
  //

  vlft= (double)1/ rho1;                   // volume over mass 
  ulft= u1;
  utlft= v1;
  uttlft= w1;
  plft= p1;

  if(plft/vlft < 0.0)
    return false;
  clft=sqrt(GAMMA*plft/vlft);       // rho * eulerian sound speed 

  vrght= (double)1/ rho2;
  urght= u2;
  utrght= v2;
  uttrght= w2;
  prght= p2;
  if(prght/vrght < 0.0)
    return false;
  crght=sqrt(GAMMA*prght/vrght);

  //
  // Start of Bruce's original code
  //


  pstar=prght-plft-crght*(urght-ulft);
  pstar=plft+pstar*clft/(clft+crght);
  pstar=max2(smallp,pstar);

  // 
  // rml - add ability to jump of out loop when converged
  //

  prevpstar=pstar;                           // save previous value 
  relpstar=RIEM_TOL;

  for (n=0; n<RIEM_ITER && relpstar>=RIEM_TOL ; n++)
    {
      scrch1=0.5*(GAMMA+1.)/GAMMA;
      wlft=1.+scrch1*(pstar-plft)/plft;
      wrght=1.+scrch1*(pstar-prght)/prght;

      if(wlft<0.0 || wrght<0.0)
        return false;
      wlft=clft*sqrt(wlft);
      wrght=crght*sqrt(wrght);

      scrch1=4.*vlft*wlft*wlft;
      scrch1=-scrch1*wlft/(scrch1-(GAMMA+1.)*(pstar-plft));
      scrch2=4.*vrght*wrght*wrght;
      scrch2=scrch2*wrght/(scrch2-(GAMMA+1.)*(pstar-prght));
      scrch3=ulft-(pstar-plft)/wlft;
      scrch4=urght+(pstar-prght)/wrght;
      pstar=pstar+(scrch4-scrch3)*(scrch1*scrch2)/(scrch2-scrch1);
      pstar=max2(smallp,pstar);

      //
      // rml - compute relative error
      //

      relpstar=fabs(pstar-prevpstar)/prevpstar;
      // printf("pstar %e  diff %e  relpstar %e\n",
      //     pstar,pstar-prevpstar,relpstar); 

      prevpstar=pstar;
    }

  if (relpstar>=RIEM_TOL)
    {
      printf("Riemann solver failed : residual %12.5E\n",relpstar);
      return false;
    }

  scrch3=ulft-(pstar-plft)/wlft;
  scrch4=urght+(pstar-prght)/wrght;
  ustar=0.5*(scrch3+scrch4);

  if (ustar>=0)
    scrch1= 1;
  else
    scrch1= -1;


  scrch2=0.5*(1.+scrch1);
  scrch3=0.5*(1.-scrch1);
  ps=plft*scrch2+prght*scrch3;
  us=ulft*scrch2+urght*scrch3;
  uts=utlft*scrch2+utrght*scrch3;
  utts=uttlft*scrch2+uttrght*scrch3;           // rml 3rd dimension 

  vs=vlft*scrch2+vrght*scrch3;
  rhos=1./vs;
  ws=wlft*scrch2+wrght*scrch3;

  if(ps*vs < 0)
    return false;

  ces=sqrt(GAMMA*ps*vs);
  vstar=vs-(pstar-ps)/(ws*ws);
  vstar=max2(vstar,(GAMMA-1.)/(GAMMA+1.)*vs);
  rhostr=1./vstar;

  if(pstar*vstar < 0)
    return false;

  cestar=sqrt(GAMMA*pstar*vstar);
  wes=ces-scrch1*us;
  westar=cestar-scrch1*ustar;
  scrch4=ws*vs-scrch1*us;

  if ( (pstar-ps) >= 0 ) {
    wes=scrch4;
    westar=scrch4;
  }


  //
  //   compute correct state for rarefaction fan
  //

  scrch1=max3(wes-westar,wes+westar,smallu);
  scrch1=(wes+westar)/scrch1;
  scrch1=0.5*(1.+scrch1);
  scrch2=1.-scrch1;

  *rhoav= scrch1*rhostr+scrch2*rhos;
  *uav= scrch1*ustar+scrch2*us;
  *utav= uts;
  *uttav= utts;                                 // rml 3rd dimension //
  *pav=scrch1*pstar+scrch2*ps;

  if (westar>=0) {
    *rhoav= rhostr;
    *uav= ustar;
    *pav= pstar;
  }

  if (wes<0) {
    *rhoav= rhos;
    *uav= us;
    *pav= ps;
  }
    
  return true;
}

static inline void _ROE2D (const double &_GAMMA,
                           const double &nx,  
                           const double &ny,  
                           const double *solL,
                           const double *solR,
                           double *FLUX) {

  //sol cons var: rho, rhou, rhov, rhoE
  //u prim var  : rho, u, v, p
  const double gamma = _GAMMA;
  const double GM1 = gamma - 1.;
  
  double uL[4], uR[4];
  const double overRhoL = 1./solL[0];

  uL[0] = solL[0];
  uL[1] = solL[1]*overRhoL; 
  uL[2] = solL[2]*overRhoL;
  uL[3] = GM1*(solL[3] - 0.5* (solL[1]*solL[1]+solL[2]*solL[2])*overRhoL); 
  
  const double overRhoR = 1./solR[0];   
  uR[0] = solR[0];
  uR[1] = solR[1]*overRhoR; 
  uR[2] = solR[2]*overRhoR;
  uR[3] = GM1*(solR[3] - 0.5* (solR[1]*solR[1]+solR[2]*solR[2])*overRhoR); 

  //* --- central contributions ---*/

  double halfrhoun;                            
  // --- left contributions 
  halfrhoun = 0.5*(uL[0]*(uL[1]*nx + uL[2]*ny));
  double HL        = gamma/GM1* uL[3]/uL[0]+0.5*(uL[1]*uL[1]+uL[2]*uL[2]);
  FLUX[0] = halfrhoun;
  FLUX[1] = halfrhoun*uL[1] + .5*uL[3]*nx;
  FLUX[2] = halfrhoun*uL[2] + .5*uL[3]*ny;
  FLUX[3] = halfrhoun*HL; 
  
  // --- right contributions  
  halfrhoun = 0.5*(uR[0]*(uR[1]*nx+uR[2]*ny));
  double HR        = gamma/GM1* uR[3]/uR[0]+0.5*(uR[1]*uR[1]+uR[2]*uR[2]); 
  FLUX[0] += halfrhoun;
  FLUX[1] += halfrhoun*uR[1] + .5*uR[3]*nx;
  FLUX[2] += halfrhoun*uR[2] + .5*uR[3]*ny;
  FLUX[3] += halfrhoun*HR; 

  /* --- add rhoe dissipation ---*/             
  
  double sqr_rhoL = sqrt(uL[0]);                                        
  double sqr_rhoR = sqrt(uR[0]);                                         
  double invz1  = 1./ (sqr_rhoL + sqr_rhoR);
  
  //rhoe average state                                    
  double u    = ( sqr_rhoL* uL[1] + sqr_rhoR * uR[1] ) * invz1;   
  double v    = ( sqr_rhoL* uL[2] + sqr_rhoR * uR[2] ) * invz1;   
  double H    = ( sqr_rhoL* HL    + sqr_rhoR * HR    ) * invz1;   
  
  double dU[4];
  for (int k=0;k<4;k++) dU[k] = solR[k] - solL[k];
  
  double un   = u*nx + v*ny ; 
  double tet  = ny*u - nx*v;
  
  double u2 = u*u + v*v;                                                  
  double c2 = GM1 * ( H - 0.5 * u2);
  double c = sqrt(c2);
  
  double g1  = gamma - 1.;
  double oC  = 1./c;
  double oC2 = oC*oC;
  
  double g1OnC2 = g1*oC2;
  double TtOnT     = (1.0 - u2*g1OnC2);
  
  // matrix of left eigenvectors  
  double L[16] = {
    nx*TtOnT       ,nx*u*g1OnC2      ,nx*v*g1OnC2 , -nx*g1OnC2, // L1
    - tet          , ny              ,-nx         , 0,          // L3
    g1*u2 - c*un   , c*nx - g1*u     , c*ny - g1*v, g1,         // L3
    g1*u2 + c*un   ,-c*nx - g1*u     ,-c*ny - g1*v, g1};        // L4
  
  // characteristic decomposition of differences  
  double dW[4] = {0,0,0,0};
  int idx = 0;
  for (int i=0;i<4;i++)
    for (int j=0;j<4;j++)
      dW[i] += L[idx++]*dU[j];
  
  // matrix of right eigenvectors
  double R[16] = {
    //R1 //R2   //R3                //R4              
    nx   ,0     ,0.5*oC2            ,0.5*oC2,
    u*nx ,ny    ,0.5*(nx*oC + u*oC2),0.5*(-nx*oC + u*oC2),
    v*nx ,- nx  ,0.5*(ny*oC + v*oC2),0.5*(-ny*oC + v*oC2),
    u2*nx,tet   ,0.5*(un*oC + H*oC2),0.5*(-un*oC + H*oC2)};
  
  // eigenvalues
  // TODO : shouldn't we take into account an entropy correction ?
  // absorb half the surface : scaling wrt central term
  
  const double A = 0.5;
  double eps = 1.e-6;
  double absUn = (fabs(un) + eps*c)*A;
  double lA[4] = {absUn,
                  absUn,
                  fabs(un + c)*A,
                  (fabs(un - c)+eps*c)*A};
  
  // add wave contributions to flux
  int index = 0;
  
  for (int k=0;k<4;k++) {
    double lflux = FLUX[k];
    for (int j=0;j<4;j++)
      lflux -= lA[j]*dW[j]*R[index++];
    FLUX[k] = -lflux;
  }
}

static inline void _ROE3D (const double &_GAMMA,
                           const double &nx,  
                           const double &ny,  
                           const double &nz,  
                           const double *solL,
                           const double *solR,
                           double *FLUX) {

  //sol cons var: rho, rhou, rhov, rhoE
  //u prim var  : rho, u, v, p
  const double gamma = _GAMMA;
  const double GM1 = gamma - 1.;
  
  double uL[5], uR[5];
  const double overRhoL = 1./solL[0];

  uL[0] = solL[0];
  uL[1] = solL[1]*overRhoL; 
  uL[2] = solL[2]*overRhoL;
  uL[3] = solL[3]*overRhoL;
  uL[4] = GM1*(solL[4] - 0.5* (solL[1]*solL[1]+solL[2]*solL[2]+solL[3]*solL[3])*overRhoL); 
  
  const double overRhoR = 1./solR[0];   
  uR[0] = solR[0];
  uR[1] = solR[1]*overRhoR; 
  uR[2] = solR[2]*overRhoR;
  uR[3] = solR[3]*overRhoR;
  uR[4] = GM1*(solR[4] - 0.5* (solR[1]*solR[1]+solR[2]*solR[2]+solR[3]*solR[3])*overRhoR); 

  //* --- central contributions ---*/

  double halfrhoun;                            
  // --- left contributions 
  halfrhoun = 0.5*(uL[0]*(uL[1]*nx + uL[2]*ny+ uL[3]*nz));
  double HL        = gamma/GM1* uL[4]/uL[0]+0.5*(uL[1]*uL[1]+uL[2]*uL[2]+uL[3]*uL[3]);
  FLUX[0] = halfrhoun;
  FLUX[1] = halfrhoun*uL[1] + .5*uL[4]*nx;
  FLUX[2] = halfrhoun*uL[2] + .5*uL[4]*ny;
  FLUX[3] = halfrhoun*uL[3] + .5*uL[4]*nz;
  FLUX[4] = halfrhoun*HL; 
  
  // --- right contributions  
  halfrhoun = 0.5*(uR[0]*(uR[1]*nx+uR[2]*ny));
  double HR        = gamma/GM1* uR[4]/uR[0]+0.5*(uR[1]*uR[1]+uR[2]*uR[2]+uR[3]*uR[3]); 
  FLUX[0] += halfrhoun;
  FLUX[1] += halfrhoun*uR[1] + .5*uR[4]*nx;
  FLUX[2] += halfrhoun*uR[2] + .5*uR[4]*ny;
  FLUX[3] += halfrhoun*uR[3] + .5*uR[4]*nz;
  FLUX[4] += halfrhoun*HR; 

  /* --- add rhoe dissipation ---*/             
  
  double sqr_rhoL = sqrt(uL[0]);                                        
  double sqr_rhoR = sqrt(uR[0]);                                         
  double invz1  = 1./ (sqr_rhoL + sqr_rhoR);
  
  //rhoe average state                                    
  double u    = ( sqr_rhoL* uL[1] + sqr_rhoR * uR[1] ) * invz1;   
  double v    = ( sqr_rhoL* uL[2] + sqr_rhoR * uR[2] ) * invz1;   
  double w    = ( sqr_rhoL* uL[3] + sqr_rhoR * uR[3] ) * invz1;   
  double H    = ( sqr_rhoL* HL    + sqr_rhoR * HR    ) * invz1;   

  double un   = u*nx + v*ny + w*nz; 

  double tet1  = nz*v - ny*w;
  double tet2  = nx*w - nz*u;
  double tet3  = ny*u - nx*v;

  double V2 = 0.5*(u*u + v*v + w*w);                                                  
  double c2 = GM1 * ( H - V2);
  double c = sqrt(c2);
  
  double g1  = gamma - 1.;
  double oC  = 1./c;
  double oC2 = oC*oC;
  
  double g1OnC2 = g1*oC2;
  double TtOnT     = (1.0 - V2*g1OnC2);

  
  double dU[5];
  for (int k=0;k<5;k++) dU[k] = solR[k] - solL[k];

  static double dW[5];
  dW[0] = ((nx*TtOnT - tet1)   * dU[0] +
                  nx*u*g1OnC2  * dU[1] +
           ( nz + nx*v*g1OnC2) * dU[2] +
           (-ny + nx*w*g1OnC2) * dU[3] -
           nx*g1OnC2           * dU[4]);
  
  dW[1] = ((ny*TtOnT - tet2)   * dU[0] +
           (-nz + ny*u*g1OnC2) * dU[1] +
                  ny*v*g1OnC2  * dU[2] +
           ( nx + ny*w*g1OnC2) * dU[3] -
           ny*g1OnC2           * dU[4]);
  
  dW[2] = ((nz*TtOnT - tet3)   * dU[0] +
           ( ny + nz*u*g1OnC2) * dU[1] +
           (-nx + nz*v*g1OnC2) * dU[2] +
                  nz*w*g1OnC2  * dU[3] -
           nz*g1OnC2           * dU[4]);
  
  dW[3] = ((GM1*V2 - c*un)     * dU[0] +
           (c*nx - GM1*u)       * dU[1] +
           (c*ny - GM1*v)       * dU[2] +
           (c*nz - GM1*w)       * dU[3] +
           GM1                  * dU[4]);

  dW[4] = ((GM1*V2 + c*un)     * dU[0] -
           (c*nx + GM1*u)       * dU[1] -
           (c*ny + GM1*v)       * dU[2] -
           (c*nz + GM1*w)       * dU[3] +
           GM1                  * dU[4]);

  const double A = 0.5;
  double absUn = fabs(un)*A;

  dW[0] *= absUn;
  dW[1] *= absUn;
  dW[2] *= absUn;
  dW[3] *= fabs(un+c)*A;
  dW[4] *= fabs(un-c)*A;
  
  FLUX[0] -=  (nx                  *dW[0] +
               ny                  *dW[1] +
               nz                  *dW[2] +
               0.5*oC2             *dW[3] +
               0.5*oC2             *dW[4]);
  
  FLUX[1] -= ((u*nx)               *dW[0] +
              (u*ny - nz)          *dW[1] +
              (u*nz + ny)          *dW[2] +
              0.5*( nx*oC + u*oC2) *dW[3] +
              0.5*(-nx*oC + u*oC2) *dW[4]);
  
  FLUX[2] -= ((v*nx + nz)          *dW[0] +
              (v*ny)               *dW[1] +
              (v*nz - nx)          *dW[2] +
              0.5*( ny*oC + v*oC2) *dW[3] +
              0.5*(-ny*oC + v*oC2) *dW[4]);
  
  FLUX[3] -= ((w*nx - ny)          *dW[0] +
              (w*ny + nx)          *dW[1] +
              (w*nz)               *dW[2] +
              0.5*( nz*oC + w*oC2) *dW[3] +
              0.5*(-nz*oC + w*oC2) *dW[4]);
  
  FLUX[4] -= ((V2*nx + tet1)      *dW[0] +
              (V2*ny + tet2)      *dW[1] +
              (V2*nz + tet3)      *dW[2] +
              0.5*( un*oC + H*oC2) *dW[3] + 
              0.5*(-un*oC + H*oC2) *dW[4]); 
}

class dgPerfectGasLaw::advection : public function {
  fullMatrix<double> sol;
  double _gamma;
  public:
  advection(double gamma_): function(12) {
    setArgument(sol, function::getSolution());
    _gamma = gamma_;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    const size_t nQP = val.size1();      
    const double GM1 = _gamma - 1.0;
    for (size_t k = 0 ; k < nQP; k++ ){
      const double invrho = 1./sol(k,0);

      const double q12 = sol(k,1)*sol(k,2)*invrho;
      const double q11 = sol(k,1)*sol(k,1)*invrho;
      const double q22 = sol(k,2)*sol(k,2)*invrho;

      const double p = GM1*sol(k,3) - 0.5*GM1*(q11+q22);
      const double qq = invrho*(sol(k,3)+p); // specific enthalpy 

      val(k,0) = sol(k,1);
      val(k,1) = q11+p;
      val(k,2) = q12;
      val(k,3) = sol(k,1)*qq;

      val(k,0+4) = sol(k,2);
      val(k,1+4) = q12;
      val(k,2+4) = q22+p;
      val(k,3+4) = sol(k,2)*qq;

      val(k,0+8) = 0.;
      val(k,1+8) = 0.;
      val(k,2+8) = 0.;
      val(k,3+8) = 0.;
    }
  }
};

class dgPerfectGasLaw::advection3d : public function {
  fullMatrix<double> sol;
  double _gamma;
  public:
  advection3d(double gamma_):function(15) {
    setArgument (sol, function::getSolution());
    _gamma = gamma_;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    const size_t nQP = val.size1();      
    const double GM1 = _gamma - 1.0;
    for (size_t k = 0 ; k < nQP; k++ ){
      const double invrho = 1./sol(k,0);

      const double q12 = sol(k,1)*sol(k,2)*invrho;
      const double q13 = sol(k,1)*sol(k,3)*invrho;
      const double q23 = sol(k,2)*sol(k,3)*invrho;
      const double q11 = sol(k,1)*sol(k,1)*invrho;
      const double q22 = sol(k,2)*sol(k,2)*invrho;
      const double q33 = sol(k,3)*sol(k,3)*invrho;

      const double p = GM1 * sol(k, 4) - GM1 * (q11 + q22 + q33) * 0.5;
      const double H = invrho * (sol(k, 4) + p);  // specific enthalpy 

      val(k,0) = sol(k, 1);
      val(k,1) = q11 + p;
      val(k,2) = q12;
      val(k,3) = q13;
      val(k,4) = sol(k, 1) * H;

      val(k,0+5) = sol(k, 2);
      val(k,1+5) = q12;
      val(k,2+5) = q22 + p;
      val(k,3+5) = q23;
      val(k,4+5) = sol(k, 2) * H;

      val(k,0+10) = sol(k, 3);
      val(k,1+10) = q13;
      val(k,2+10) = q23;
      val(k,3+10) = q33 + p;
      val(k,4+10) = sol(k, 3) * H;
    }
  }
};


class dgPerfectGasLaw::diffusion : public function {
  fullMatrix<double> sol, grad, mu, kappa;
  double _cv;
  public:
  diffusion (const function * muFunction, const function * kappaFunction, double cv):function(12) {
    setArgument (sol, function::getSolution());
    setArgument (grad, function::getSolutionGradient());
    setArgument (mu, muFunction);
    setArgument (kappa, kappaFunction);
    _cv = cv;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    const size_t nQP = val.size1();      
    for (size_t k = 0 ; k < nQP; k++ ){
      const double muk    = mu(k,0);
      const double kappak = kappa(k,0);      
      //      printf("%g %g grad rho (%g %g)\n",muk,kappak,grad(k*3+0,0),grad(k*3+1,0));
    // find gradients of primitive variables    
      const double overRho = 1./sol(k,0);
      const double u = sol(k,1) * overRho;
      const double v = sol(k,2) * overRho;
      const double e = sol(k,3) * overRho;
      // grad u =  (grad (\rho u) - (grad\rho) u) / \rho
      const double drhodx = grad(k, 0);
      const double drhody = grad(k, 1);
      const double dudx = (grad(k,3) - drhodx * u) * overRho;
      const double dudy = (grad(k,4) - drhody * u) * overRho;
      const double dvdx = (grad(k,6) - drhodx * v) * overRho;
      const double dvdy = (grad(k,7) - drhody * v) * overRho;
      const double dedx = (grad(k,9) - drhodx * e) * overRho;
      const double dedy = (grad(k,10)- drhody * e) * overRho;
      // shear stress tensor - Newtonian fluid without bulk viscosity (\lambda=-\mu)  
      const double tauxx = muk * (dudx - dvdy);
      const double tauxy = muk * (dudy + dvdx);
      const double tauyy = - tauxx;
    
      val(k, 0) = 0.;
      val(k, 4) = 0.;
      val(k, 8) = 0.;

      val(k,1)   = -tauxx;
      val(k,1+4) = -tauxy;
      val(k,1+8) = 0;

      val(k,2)   = -tauxy;
      val(k,2+4) = -tauyy;    
      val(k,2+8) = 0;
    
      // k grad T  
      val(k,3+0)    = -kappak / _cv * (dedx - u * dudx - v * dvdx);
      val(k,3+4)    = -kappak / _cv * (dedy - u * dudy - v * dvdy);
      val(k,3+8)    = 0;
      
      // v . tau - momentum dissipation
      val(k,3)      -= (u * tauxx + v * tauxy);
      val(k,3+4)    -= (u * tauxy + v * tauyy);         
      
      //      printf("%g %g %g %g %g %g %g %g \n",val(k,0),val(k,1),val(k,2),val(k,3),val(k,4),val(k,5),val(k,6),val(k,7));
    }
  }
};


class dgPerfectGasLaw::diffusion3d : public function {
  fullMatrix<double> sol, grad, mu, kappa;
  double _cv;
  public:
  diffusion3d(const function * muFunction, const function * kappaFunction, double cv):function(15) {
    setArgument (sol, function::getSolution());
    setArgument (grad, function::getSolutionGradient());
    setArgument (mu, muFunction);
    setArgument (kappa, kappaFunction);
    _cv = cv;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    const size_t nQP = val.size1();      
    const double overThree = 1.0/3.0;
    
    for (size_t k = 0 ; k < nQP; k++ ){
      const double muk    = mu(k,0);
      const double kappak = kappa(k,0);      
      //      printf("%g %g grad rho (%g %g)\n",muk,kappak,grad(k*3+0,0),grad(k*3+1,0));
    // find gradients of primitive variables    
      const double overRho = 1./sol(k,0);
      const double u = sol(k,1) * overRho;
      const double v = sol(k,2) * overRho;
      const double w = sol(k,3) * overRho;
      // grad v =  (grad (\rho u) - (grad\rho) u) / \rho 
      const double dudx = (grad(k,3)  - grad(k,0) * u) * overRho;
      const double dudy = (grad(k,4)  - grad(k,1) * u) * overRho;
      const double dudz = (grad(k,5)  - grad(k,2) * u) * overRho;
      const double dvdx = (grad(k,6)  - grad(k,0) * v) * overRho;
      const double dvdy = (grad(k,7)  - grad(k,1) * v) * overRho;    
      const double dvdz = (grad(k,8)  - grad(k,2) * v) * overRho;    
      const double dwdx = (grad(k,9)  - grad(k,0) * w) * overRho;
      const double dwdy = (grad(k,10) - grad(k,1) * w) * overRho;    
      const double dwdz = (grad(k,11) - grad(k,2) * w) * overRho;    
      // shear stress tensor - Newtonian fluid without bulk viscosity (\lambda=-2/3*\mu)
      const double divOverThree = (dudx + dvdy + dwdz) * overThree;

      const double tauxx = 2.0 * muk * (dudx - divOverThree);
      const double tauyy = 2.0 * muk * (dvdy - divOverThree);
      const double tauzz = 2.0 * muk * (dwdz - divOverThree);

      const double tauxy = muk * (dudy + dvdx);
      const double tauxz = muk * (dudz + dwdx);
      const double tauyz = muk * (dvdz + dwdy);

      val(k,1) = -tauxx;
      val(k,2) = -tauxy;
      val(k,3) = -tauxz;
    
      val(k,1+5) = -tauxy;
      val(k,2+5) = -tauyy;    
      val(k,3+5) = -tauyz;    

      val(k,1+10) = -tauxz;
      val(k,2+10) = -tauyz;    
      val(k,3+10) = -tauzz;

      // heat flux
      const double kOverRhoCv = kappak * overRho / _cv;
      const double Ek = (u * u + v * v + w * w);
      const double eMinus = sol(k,4) * overRho - Ek;
    
      // k grad T
      val(k,4)    = -kOverRhoCv*(grad(k, 12) - eMinus*grad(k, 0) - (grad(k, 3) * u + grad(k, 6) * v + grad(k, 9) * w));
      val(k,4+5)  = -kOverRhoCv*(grad(k, 13) - eMinus*grad(k, 1) - (grad(k, 4) * u + grad(k, 7) * v + grad(k,10) * w));
      val(k,4+10) = -kOverRhoCv*(grad(k, 14) - eMinus*grad(k, 2) - (grad(k, 5) * u + grad(k, 8) * v + grad(k,11) * w));
      
      // v . tau - momentum dissipation
      
      val(k,4)    -= (u * tauxx + v * tauxy + w * tauxz);
      val(k,4+5)  -= (u * tauxy + v * tauyy + w * tauyz);         
      val(k,4+10) -= (u * tauxz + v * tauyz + w * tauzz);         
      
      //      printf("%g %g %g %g %g %g %g %g \n",val(k,0),val(k,1),val(k,2),val(k,3),val(k,4),val(k,5),val(k,6),val(k,7));
      
    }
  }
};

class dgPerfectGasLaw::source : public function {
  fullMatrix<double> sol,s;
  int _nbf;
public:
  source(int nbf, const function *sourceFunction):function(nbf), _nbf(nbf) {
    setArgument (sol, function::getSolution());
    setArgument (s, sourceFunction);
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    const size_t nQP = val.size1();      
    for (size_t k = 0 ; k < nQP; k++ ){
      for (int iF=0;iF<_nbf;iF++){
        val(k,iF) = sol(k,iF)*s(0,iF);
      }
    }
  }
};

class dgPerfectGasLaw::clipToPhysics : public function {
  fullMatrix<double> sol;
  double _presMin, _rhoMin, _gamma;
public:
  clipToPhysics(double presMin, double rhoMin, double gamma_): function(4)  {
    setArgument (sol, function::getSolution());
    _presMin=presMin;
    _rhoMin=rhoMin;
    _gamma = gamma_;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    const size_t nDofs = val.size1();
    for (size_t k = 0 ; k < nDofs; k++ ){
      val(k,0) = sol(k,0);
      val(k,1) = sol(k,1);
      val(k,2) = sol(k,2);
      val(k,3) = sol(k,3);
      if (sol(k,0) < _rhoMin){
        //printf("CL: clip rho min =%g \n", _rhoMin);
        val(k,0) = _rhoMin;
      }
      double rhoV2 = sol(k,1)*sol(k,1)+sol(k,2)*sol(k,2);
      rhoV2 /= sol(k,0);
      const double p = (_gamma-1)*(sol(k,3) - 0.5*rhoV2);
      if (p < _presMin) {
        val(k,3) = _presMin / (_gamma-1) +  0.5 *rhoV2 ; 
        //printf("CL: clip pres min =%g \n ", val(k,3));
      }
    }
  }
};

class dgPerfectGasLaw::clipToPhysics3d : public function {
  fullMatrix<double> sol;
  double _presMin, _rhoMin, _gamma;
public:
  clipToPhysics3d(double presMin, double rhoMin, double gamma_):function(5)
  {
    setArgument (sol, function::getSolution());
    _presMin=presMin;
    _rhoMin=rhoMin;
    _gamma = gamma_;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    const size_t nDofs = val.size1();
    for (size_t k = 0 ; k < nDofs; k++ ){
      val(k,0) = sol(k,0);
      val(k,1) = sol(k,1);
      val(k,2) = sol(k,2);
      val(k,3) = sol(k,3);
      val(k,4) = sol(k,4);
      if (sol(k,0) < _rhoMin){
        //printf("CL: clip rho min =%g \n", _rhoMin);
        val(k,0) = _rhoMin;
      }
      double rhoV2 = sol(k,1)*sol(k,1)+sol(k,2)*sol(k,2)+sol(k,3)*sol(k,3);
      rhoV2 /= sol(k,0);
      const double p = (_gamma-1)*(sol(k,4) - 0.5*rhoV2);
      if (p < _presMin) {
        val(k,4) = _presMin / (_gamma-1) +  0.5 *rhoV2 ; 
        //printf("CL: clip pres min =%g \n ", val(k,3));
      }
    }
  }
};


// Lax-Friedrichs numerical flux
class dgPerfectGasLaw::riemannLF : public function {
  fullMatrix<double> normals, solL, solR;
  double _gamma;
  public:
  riemannLF(double gamma_):function(4) {
    setArgument (normals, function::getNormals(), 0);
    setArgument (solL, function::getSolution(), 0);
    setArgument (solR, function::getSolution(), 1);
    _gamma = gamma_;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    const double GM1 = _gamma-1.;
    for(size_t i=0; i< nQP; i++) {
      const double &nx = normals(i,0);
      const double &ny = normals(i,1);

      // Left flux & max. convective speed
      const double &rL = solL(i,0), &ruL = solL(i,1), &rvL = solL(i,2), &reL = solL(i,3);
      const double invrL = 1./rL;
      const double q12L = ruL*rvL*invrL;
      const double q11L = ruL*ruL*invrL;
      const double q22L = rvL*rvL*invrL;
      const double rVSqL = q11L+q22L;                                           // Density*velocity^2
      const double pL = GM1*(reL-0.5*rVSqL);                                    // Pressure
      const double qqL = invrL*(reL+pL);                                        // Specific enthalpy
      const double &Fx0L = ruL, Fx1L = q11L+pL, &Fx2L = q12L, Fx3L = ruL*qqL;   // Flux in x-direction
      const double &Fy0L = rvL, &Fy1L = q12L, Fy2L = q22L+pL, Fy3L = rvL*qqL;   // Flux in y-direction
      const double maxCSL = sqrt(_gamma*pL*invrL)+sqrt(rVSqL*invrL);            // Max. convective speed

      // Right flux & max. convective speed
      const double &rR = solR(i,0), &ruR = solR(i,1), &rvR = solR(i,2), &reR = solR(i,3);
      const double invrR = 1./rR;
      const double q12R = ruR*rvR*invrR;
      const double q11R = ruR*ruR*invrR;
      const double q22R = rvR*rvR*invrR;
      const double rVSqR = q11R+q22R;                                           // Density*velocity^2
      const double pR = GM1*(reR-0.5*rVSqR);                                    // Pressure
      const double qqR = invrR*(reR+pR);                                        // Specific enthalpy
      const double &Fx0R = ruR, Fx1R = q11R+pR, &Fx2R = q12R, Fx3R = ruR*qqR;   // Flux in x-direction
      const double &Fy0R = rvR, &Fy1R = q12R, Fy2R = q22L+pR, Fy3R = rvR*qqR;   // Flux in y-direction
      const double maxCSR = sqrt(_gamma*pR*invrR)+sqrt(rVSqR*invrR);            // Max. convective speed

      const double alpha = std::max(maxCSL,maxCSR);                             // Relaxation constant in Lax-Friedrichs

      val(i,0) = -0.5*((Fx0R+Fx0L)*nx+(Fy0R+Fy0L)*ny-alpha*(rR-rL));
      val(i,1) = -0.5*((Fx1R+Fx1L)*nx+(Fy1R+Fy1L)*ny-alpha*(ruR-ruL));
      val(i,2) = -0.5*((Fx2R+Fx2L)*nx+(Fy2R+Fy2L)*ny-alpha*(rvR-rvL));
      val(i,3) = -0.5*((Fx3R+Fx3L)*nx+(Fy3R+Fy3L)*ny-alpha*(reR-reL));
    }
  }
};


class dgPerfectGasLaw::riemann : public function {
  fullMatrix<double> normals, solL, solR;
  double _gamma;
  public:
  riemann(double gamma_):function(4) {
    setArgument (normals, function::getNormals(), 0);
    setArgument (solL, function::getSolution(), 0);
    setArgument (solR, function::getSolution(), 1);
    _gamma = gamma_;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++) {
      const double solLeft [4] = {solL(i,0),solL(i,1),solL(i,2),solL(i,3)};
      const double solRight[4] = {solR(i,0),solR(i,1),solR(i,2),solR(i,3)};
      double FLUX[4] ;
      const double nx = normals(i,0);
      const double ny = normals(i,1);
      _ROE2D (_gamma,nx,ny,solLeft,solRight,FLUX);

      val(i,0) = FLUX[0];
      val(i,1) = FLUX[1];
      val(i,2) = FLUX[2];
      val(i,3) = FLUX[3];
    }
  }
};

class dgPerfectGasLaw::riemann3d : public function {
  fullMatrix<double> normals, solL, solR;
  double _gamma;
  public:
  riemann3d(double gamma_):function(5) {
    setArgument (normals, function::getNormals(), 0);
    setArgument (solL, function::getSolution(), 0);
    setArgument (solR, function::getSolution(), 1);
    _gamma = gamma_;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    size_t nQP = val.size1();
    for(size_t i=0; i < nQP; i++) {
      const double solLeft [5] = {solL(i,0),solL(i,1),solL(i,2),solL(i,3),solL(i,4)};
      const double solRight[5] = {solR(i,0),solR(i,1),solR(i,2),solR(i,3),solR(i,4)};
      double FLUX[5] ;
      const double nx = normals(i,0);
      const double ny = normals(i,1);
      const double nz = normals(i,2);
      _ROE3D (_gamma,nx,ny,nz,solLeft,solRight,FLUX);

      val(i,0) = FLUX[0];
      val(i,1) = FLUX[1];
      val(i,2) = FLUX[2];
      val(i,3) = FLUX[3];
      val(i,4) = FLUX[4];
    }
  }
};

class dgPerfectGasLaw::riemannGodunov : public function {
  fullMatrix<double> normals, solL, solR;
  double _gamma;
  public:
  riemannGodunov(double gamma_):function(4) {
    setArgument (normals, function::getNormals(), 0);
    setArgument (solL, function::getSolution(), 0);
    setArgument (solR, function::getSolution(), 1);
    _gamma = gamma_;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++) {
      const double nx = normals(i,0);
      const double ny = normals(i,1);

      const double unL = (solL(i,1)*nx+solL(i,2)*ny)/solL(i,0);
      const double utL = (solL(i,1)*ny-solL(i,2)*nx)/solL(i,0);
      const double rhoV2L = (unL*unL+utL*utL)*solL(i,0);
      const double pL = (_gamma-1.)*solL(i,3) - 0.5*(_gamma-1.)* rhoV2L;

      const double unR = (solR(i,1)*nx+solR(i,2)*ny)/solR(i,0);
      const double utR = (solR(i,1)*ny-solR(i,2)*nx)/solR(i,0);
      const double rhoV2R = (unR*unR+utR*utR)*solR(i,0);
      const double pR = (_gamma-1.)*solR(i,3) - 0.5*(_gamma-1.)* rhoV2R;

      double rhoAv,unAv,utAv,usAv,pAv;
      if (!riemannExact(_gamma, solL(i,0), unL, utL, 0.0, pL,
                   solR(i,0), unR, utR, 0.0, pR,
                   &rhoAv,&unAv,&utAv,&usAv,&pAv)) {
         unAv = (unL + unR)/2;
         usAv = 0;
         pAv = (pL + pR)/2;
         rhoAv = (solL(i,0) + solR(i,0))/2;
      }

                   
      const double vxAv = unAv * nx + utAv *ny;
      const double vyAv = unAv * ny - utAv *nx;
      // p = rho E (G-1) - 0.5 (G-1) * rho V^2
      // rho E = p / (G-1) + 0.5 * rho V^2
      const double rhoE = 
        (1./(_gamma-1.)) * pAv + 0.5 * rhoAv * (vxAv*vxAv + vyAv*vyAv);
      
      /*      printf("%g %g %g %g, (%g %g) %g %g %g %g\n",
             solL(i,0), solL(i,1), solL(i,2), solL(i,3),pL,pAv, 
             rhoAv, rhoAv*vxAv,rhoAv*vyAv,rhoE);
      */
      const double F0 = rhoAv * unAv;
      /*
      const double qq = invrho*(sol(k,3)+p);

      val(k,0)   = sol(k,1);
      val(k,1) = q11+p;
      val(k,2) = q12;
      val(k,3) = sol(k,1)*qq;
      */

      val(i,0) = -F0;
      val(i,1) = -(F0*vxAv + pAv*nx);
      val(i,2) = -(F0*vyAv + pAv*ny);
      val(i,3) = - (rhoE+pAv) * unAv;
    }
  }
};

class dgPerfectGasLaw::riemannGodunov3d : public function {
  fullMatrix<double> normals, solL, solR;
  double _gamma;
  public:
  riemannGodunov3d(double gamma_):function(5) {
    setArgument (normals, function::getNormals(), 0);
    setArgument (solL, function::getSolution(), 0);
    setArgument (solR, function::getSolution(), 1);
    _gamma = gamma_;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++) {
      const double nx = normals(i,0);
      const double ny = normals(i,1);
      const double nz = normals(i,2);
      SVector3 e;
      if (fabs(nx) > fabs(ny) && fabs(nx) > fabs(nz)){
        e = SVector3(0,1,0);
      }
      else {
        e = SVector3(1,0,0);
      }
      SVector3 N (nx,ny,nz);
      SVector3 T = crossprod(N,e); T.normalize();
      SVector3 S = crossprod(T,N);

      const double unL = (solL(i,1)*nx+solL(i,2)*ny+solL(i,3)*nz)/solL(i,0);
      const double unR = (solR(i,1)*nx+solR(i,2)*ny+solR(i,3)*nz)/solR(i,0);
      
      const double utL = (solL(i,1)*T.x()+solL(i,2)*T.y()+solL(i,3)*T.z())/solL(i,0);
      const double utR = (solR(i,1)*T.x()+solR(i,2)*T.y()+solR(i,3)*T.z())/solR(i,0);

      const double usL = (solL(i,1)*S.x()+solL(i,2)*S.y()+solL(i,3)*S.z())/solL(i,0);
      const double usR = (solR(i,1)*S.x()+solR(i,2)*S.y()+solR(i,3)*S.z())/solR(i,0);

      const double rhoV2L = (unL*unL+utL*utL+usL*usL)*solL(i,0);
      const double pL = (_gamma-1.)*solL(i,4) - 0.5*(_gamma-1.)* rhoV2L;

      const double rhoV2R = (unR*unR+utR*utR+usR*usR)*solR(i,0);
      const double pR = (_gamma-1.)*solR(i,4) - 0.5*(_gamma-1.)* rhoV2R;

      double rhoAv,unAv,utAv,usAv,pAv;
      riemannExact(_gamma, solL(i,0), unL,utL,usL,pL,
                   solR(i,0), unR,utR,usR,pR,
                   &rhoAv,&unAv,&utAv,&usAv,&pAv);

                   
      const double vxAv = unAv * N.x() + utAv *T.x() + usAv *S.x() ;
      const double vyAv = unAv * N.y() + utAv *T.y() + usAv *S.y() ;
      const double vzAv = unAv * N.z() + utAv *T.z() + usAv *S.z() ;

      // p = rho E (G-1) - 0.5 (G-1) * rho V^2
      // rho E = p / (G-1) + 0.5 * rho V^2
      const double rhoE = 
        (1./(_gamma-1.)) * pAv + 0.5 * rhoAv * (vxAv*vxAv + vyAv*vyAv+ vzAv*vzAv);
      

      const double F0 = rhoAv * unAv;

      val(i,0) = -F0;
      val(i,1) = -(F0*vxAv + pAv*nx);
      val(i,2) = -(F0*vyAv + pAv*ny);
      val(i,3) = -(F0*vzAv + pAv*nz);
      val(i,4) = - (rhoE+pAv) * unAv;
    }
  }
};

class dgPerfectGasLaw::maxConvectiveSpeed : public function {
  fullMatrix<double> sol;
  double _gamma;
  public:
  maxConvectiveSpeed(double gamma_):function(1) {
    setArgument (sol, function::getSolution());
    _gamma = gamma_;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++) {
      const double rhov2 = (sol(i,1)*sol(i,1) + sol(i,2)*sol(i,2))/sol(i,0);
      const double p = (_gamma-1.0)*(sol(i,3) - 0.5 * rhov2);
      //      printf("p = %g %g %g %g %g\n",p,val(i,0),val(i,1),val(i,2),val(i,3));
      const double c = sqrt(_gamma*p/sol(i,0));
      val(i,0) = c+sqrt(rhov2/sol(i,0));
    }
  }
};

class dgPerfectGasLaw::maxConvectiveSpeed3d : public function {
  fullMatrix<double> sol;
  double _gamma;
  public:
  maxConvectiveSpeed3d(double gamma_):function(1) {
    setArgument (sol, function::getSolution());
    _gamma = gamma_;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++) {
      const double rhov2 = (sol(i,1)*sol(i,1) + sol(i,2)*sol(i,2)+ sol(i,3)*sol(i,3))/sol(i,0);
      const double p = (_gamma-1.0)*(sol(i,4) - 0.5 * rhov2);
      const double c = sqrt(_gamma*p/sol(i,0));
      val(i,0) = c+sqrt(rhov2/sol(i,0));
    }
  }
};

class dgPerfectGasLaw::diffusivity : public function {
  fullMatrix<double> mu, kappa, solution;
  double _cv;
  int _dim;
  public:
  diffusivity(const function *muFunction, const function *kappaFunction, double cv, int dim):function(dim+2) {
    setArgument (mu, muFunction);
    setArgument (kappa, kappaFunction);
    setArgument (solution, function::getSolution());
    _dim = dim;
    _cv = cv;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++) {
      double rho = solution(i,0);
      val(i,0) = 0.0;
      for (int k = 1; k <= _dim; k++)
        val(i,k) = mu(i,0)/rho;
      val(i,_dim + 1) = kappa(i,0);// / (_cv * rho);
      //val(i,_dim + 1) = kappa(i,0) / (_cv * rho);
    }
  }
};

void dgPerfectGasLaw::setup() {
  if (_R == 0)
    Msg::Fatal("Gas constant and gamma must be set");
  double cv = _R / (_gamma - 1);
  if (_dim == 3 ) {
    _maximumConvectiveSpeed[""] = new maxConvectiveSpeed3d(_gamma);
    _clipToPhysics[""] = new clipToPhysics3d (1e-3, 1e-3, _gamma);
    _convectiveFlux = new advection3d(_gamma);
    _riemannTerm = new riemannGodunov3d(_gamma);
    if (_muFunction && _kappaFunction) {
      _diffusiveFlux = new diffusion3d (_muFunction, _kappaFunction, cv);
    }
  } else {
    _maximumConvectiveSpeed[""] = new maxConvectiveSpeed(_gamma);
    _clipToPhysics[""] = new clipToPhysics(1e-3, 1e-3, _gamma);
//    _riemannTerm = new riemannGodunov(_gamma);
    _riemannTerm = new riemannLF(_gamma);
    _convectiveFlux = new advection(_gamma);
    if (_muFunction && _kappaFunction)
      _diffusiveFlux = new diffusion (_muFunction, _kappaFunction, cv);
  }
  if (_sourceFunction)
    _volumeTerm0[""] = new source (_nbf, _sourceFunction);
  if (_muFunction && _kappaFunction) {
    _diffusivity[""] = new diffusivity (_muFunction, _kappaFunction, cv,  _dim);
    if (_dim == 3)
      _IPTerm = new dgPerfectGasLawIPTerm3 (_diffusiveFlux, _muFunction, _kappaFunction, _R, _gamma);
    else
      _IPTerm = new dgPerfectGasLawIPTerm (_dim, _diffusiveFlux, _muFunction, _kappaFunction, _R, _gamma);
    _volumeTerm1[""] = functionSumNew(_convectiveFlux, _diffusiveFlux);
    _interfaceTerm0[""] = functionSumNew(_riemannTerm, _IPTerm);
    //_interfaceTerm1N[""] = dgNewSymmetricIpTerm(_dim + 2, _muFunction);
  } else {
    _volumeTerm1[""] = _convectiveFlux;
    _interfaceTerm0[""] = _riemannTerm;
  }
  _velocitySol = new velocityVector(_dim);
  _totalForceSurface = new totalForceSurface(_dim, _gamma, _muFunction);
}

dgPerfectGasLaw::~dgPerfectGasLaw() {
  if (_volumeTerm1[""]) delete _volumeTerm1[""];
  if (_interfaceTerm0[""]) delete _interfaceTerm0[""];
  if (_clipToPhysics[""]) delete _clipToPhysics[""];
  if (_volumeTerm0[""]) delete _volumeTerm0[""];
  if (_maximumConvectiveSpeed[""]) delete _maximumConvectiveSpeed[""];
  if (_diffusivity[""]) delete _diffusivity[""];
  if (_interfaceTerm1N[""]) delete _interfaceTerm1N[""];
  if (_IPTerm) {
    delete _IPTerm;
    delete _riemannTerm;
  }
  if (_diffusiveFlux) {
    delete _diffusiveFlux;
    delete _convectiveFlux;
  }
}

dgPerfectGasLaw::dgPerfectGasLaw(int d) : dgConservationLawFunction((d == 3) ? 5 : 4), _dim(d) {
  // \rho \rho u \rho v \rho e
  function *fzero = new functionConstant(0.);
  _kappaFunction = fzero;
  _muFunction = fzero;
  _R = 0;
  std::vector<double> zerov(_nbf);
  _sourceFunction = new functionConstant(zerov);;
}

//-------------------------------------------------------------------------------
// A class for slip and non slip walls
// could easily add moving walls ...
//-------------------------------------------------------------------------------
class dgBoundaryConditionPerfectGasLaw2dWall : public dgBoundaryCondition {
  class fullTerm : public function {
    functionReplace diffusionReplace, advectionReplace;
    fullMatrix<double> solIn, normals, riemann, ip, solOutDiff, solOutAdv, diffFluxOut, diffFluxIn, wallVelocity;
    bool _nonSlip, _moving;
    double _gamma;
    public:
    fullTerm (bool nonSlip, dgPerfectGasLaw *claw, const function *wallVelocityF) : function (4) {
      _gamma = claw->getGamma();
      _nonSlip = nonSlip;
      _moving = wallVelocityF;
      setArgument (normals, function::getNormals());
      setArgument (solIn, function::getSolution(), 0);
      advectionReplace.addChild();
      addFunctionReplace (advectionReplace);
      advectionReplace.replace (solOutAdv, function::getSolution(), 1);
      advectionReplace.get (riemann, claw->_riemannTerm);
      if (_nonSlip) {
        diffusionReplace.addChild();
        addFunctionReplace (diffusionReplace);
        diffusionReplace.replace (solOutDiff, function::getSolution(), 1);
        diffusionReplace.replace (diffFluxOut, claw->_diffusiveFlux, 1);
        diffusionReplace.get (ip, claw->_IPTerm);
        setArgument (diffFluxIn, claw->_diffusiveFlux, 0);
        if(_moving)
          setArgument (wallVelocity, wallVelocityF);
      }
    }
    void call (dataCacheMap *m, fullMatrix<double> &val) {
      size_t nQP = val.size1();
      // 1) fill the external values
      for (size_t i=0; i< nQP; i++) {
        const double nx = normals (i,0);
        const double ny = normals (i,1);
        double vn = solIn (i,1) * nx + solIn (i,2) * ny;
        //advective flux
        solOutAdv (i,0) = solIn (i,0);
        solOutAdv (i,1) = solIn (i,1) - 2 * vn * nx;
        solOutAdv (i,2) = solIn (i,2) - 2 * vn * ny;
        solOutAdv (i,3) = solIn (i,3);
        //diffusive flux (only for non slip conditions)
        if (_nonSlip) {
          //-------------------------------------------------------------------------------
          // Non Slip Dirichlet BC --
          // What to put on the right side of the IP (no velocity, same density) 
          // Assume Adiabatic Wall --> no heat transfer -> \partial_n T = 0
          // So, we use the same temperature as inside
          //-------------------------------------------------------------------------------
          solOutDiff (i,0) = solIn (i,0);
          solOutDiff (i,3) = solIn (i,3) - 0.5 * (solIn(i,1) * solIn(i,1) + solIn(i,2) * solIn(i,2))/solIn(i,0);
          if (!_moving) {
            solOutDiff (i,1) = 0;
            solOutDiff (i,2) = 0;
          } else {
            solOutDiff (i,1) = solIn (i,0) * wallVelocity (i,0);
            solOutDiff (i,2) = solIn (i,0) * wallVelocity (i,1);
            solOutDiff (i,3) += 0.5 * (wallVelocity (i,0) * wallVelocity (i,0) + wallVelocity(i,1) * wallVelocity(i,1))*solIn(i,0);
          }
          //-------------------------------------------------------------------------------
          // Non Slip Neumann BC --
          // Compute normal diffusive fluxes at the boundary
          // Assume Adiabatic Wall --> no heat transfer -> no flux for component 3 (in 2D)
          // we re-use the IP function to compute the complete diffusive flux on the boundary
          //-------------------------------------------------------------------------------
          for (int k=0; k<2; k++) {
            diffFluxOut (i, 0 + k*4) = diffFluxIn (i, 0 + k*4);
            diffFluxOut (i, 1 + k*4) = diffFluxIn (i, 1 + k*4);
            diffFluxOut (i, 2 + k*4) = diffFluxIn (i, 2 + k*4);
            diffFluxOut (i, 3 + k*4) = -diffFluxIn (i, 3 + k*4);
          }
        }
      }
      // 2) compute the term
      if (_nonSlip) {
        advectionReplace.compute();
        diffusionReplace.compute();
        for (size_t i = 0; i< nQP; i++) {

          //todo why is riemannGodunov broken here ?
          const double nx = normals(i,0);
          const double ny = normals(i,1);
          const double solLeft [4] = {solIn(i,0),solIn(i,1),solIn(i,2),solIn(i,3)};
          const double solRight [4] = {solOutAdv(i,0),solOutAdv(i,1),solOutAdv(i,2),solOutAdv(i,3)};
          double FLUX[4];
          _ROE2D (_gamma,nx,ny,solLeft,solRight,FLUX);

          for (int j = 0; j < 4; j ++) 
            val (i, j) = FLUX[j] + ip (i, j);
            //val (i, j) = riemann (i, j) + ip (i, j);
        }
      } else {
        advectionReplace.compute();
        for (size_t i = 0; i< nQP; i++)
          for (size_t j = 0; j < 4; j ++) 
            val (i, j) = riemann (i, j);
      }
    }
  };
  public:
  dgBoundaryConditionPerfectGasLaw2dWall(int type, dgPerfectGasLaw *claw, const function *velocityF){
    claw->checkSetup();
    switch (type) {
      case 1 : _term0 = new fullTerm (false, claw, NULL); break;
      case 2 : _term0 = new fullTerm (true, claw, NULL); break;
      case 3 : _term0 = new fullTerm (true, claw, velocityF); break;
      throw;
    }
  }
  ~dgBoundaryConditionPerfectGasLaw2dWall(){
    delete _term0;
  }
};

class dgBoundaryConditionPerfectGasLaw3dWall : public dgBoundaryCondition {
  class fullTerm : public function {
    functionReplace diffusionReplace, advectionReplace;
    fullMatrix<double> solIn, normals, riemann, ip, solOutDiff, solOutAdv, diffFluxOut, diffFluxIn, wallVelocity;
    bool _nonSlip, _moving;
    public:
    fullTerm (bool nonSlip, dgPerfectGasLaw *claw, const function *wallVelocityF) : function (5) {
      _nonSlip = nonSlip;
      _moving = wallVelocityF;
      setArgument (normals, function::getNormals());
      setArgument (solIn, function::getSolution(), 0);
      advectionReplace.addChild();
      addFunctionReplace (advectionReplace);
      advectionReplace.replace (solOutAdv, function::getSolution(), 1);
      advectionReplace.get (riemann, claw->_riemannTerm);
      if (_nonSlip) {
        diffusionReplace.addChild();
        addFunctionReplace (diffusionReplace);
        diffusionReplace.replace (solOutDiff, function::getSolution(), 1);
        diffusionReplace.replace (diffFluxOut, claw->_diffusiveFlux, 1);
        diffusionReplace.get (ip, claw->_IPTerm);
        setArgument (diffFluxIn, claw->_diffusiveFlux, 0);
        if(_moving)
          setArgument (wallVelocity, wallVelocityF);
      }
    }
    void call (dataCacheMap *m, fullMatrix<double> &val) {
      size_t nQP = val.size1();
      // 1) fill the external values
      for (size_t i=0; i< nQP; i++) {
        const double nx = normals (i,0);
        const double ny = normals (i,1);
        const double nz = normals (i,2);
        double vn = solIn (i,1) * nx + solIn (i,2) * ny + solIn (i,3) * nz;
        //advective flux
        solOutAdv (i,0) = solIn (i,0);
        solOutAdv (i,1) = solIn (i,1) - 2 * vn * nx;
        solOutAdv (i,2) = solIn (i,2) - 2 * vn * ny;
        solOutAdv (i,3) = solIn (i,3) - 2 * vn * nz;
        solOutAdv (i,4) = solIn (i,4);
        //diffusive flux (only for non slip conditions)
        if (_nonSlip) {
          //-------------------------------------------------------------------------------
          // Non Slip Dirichlet BC --
          // What to put on the right side of the IP (no velocity, same density) 
          // Assume Adiabatic Wall --> no heat transfer -> \partial_n T = 0
          // So, we use the same temperature as inside
          //-------------------------------------------------------------------------------
          solOutDiff (i,0) = solIn (i,0);
          solOutDiff (i,4) = solIn (i,4) - 0.5 * (solIn(i,1) * solIn(i,1) + solIn(i,2) * solIn(i,2)+ solIn(i,3) * solIn(i,3))/solIn(i,0);
          if (!_moving) {
            solOutDiff (i,1) = 0;
            solOutDiff (i,2) = 0;
            solOutDiff (i,3) = 0;
          } else {
            solOutDiff (i,1) = solIn (i,0) * wallVelocity (i,0);
            solOutDiff (i,2) = solIn (i,0) * wallVelocity (i,1);
            solOutDiff (i,3) = solIn (i,0) * wallVelocity (i,2);
            solOutDiff (i,4) += 0.5 * (wallVelocity (i,0) * wallVelocity (i,0) + wallVelocity(i,1) * wallVelocity(i,1) + wallVelocity(i,2) * wallVelocity(i,2))*solIn(i,0);
          }
          //-------------------------------------------------------------------------------
          // Non Slip Neumann BC --
          // Compute normal diffusive fluxes at the boundary
          // Assume Adiabatic Wall --> no heat transfer -> no flux for component 3 (in 2D)
          // we re-use the IP function to compute the complete diffusive flux on the boundary
          //-------------------------------------------------------------------------------
          for (int k=0; k<3; k++) {
            diffFluxOut (i, 0 + k*5) = 0;
            diffFluxOut (i, 1 + k*5) = diffFluxIn (i, 1 + k*5);
            diffFluxOut (i, 2 + k*5) = diffFluxIn (i, 2 + k*5);
            diffFluxOut (i, 3 + k*5) = diffFluxIn (i, 3 + k*5);
            diffFluxOut (i, 4 + k*5) = -diffFluxIn (i, 4 + k*5);
          }
        }
      }
      // 2) compute the term
      if (_nonSlip) {
        advectionReplace.compute();
        diffusionReplace.compute();
        for (size_t i = 0; i< nQP; i++) {
          //todo why is riemannGodunov broken here ?
      /*    const double nx = normals(i,0);
          const double ny = normals(i,1);
          const double nz = normals(i,2);
          const double solLeft [5] = {solIn(i,0),solIn(i,1),solIn(i,2),solIn(i,3),solIn(i,4)};
          const double solRight [5] = {solOutAdv(i,0),solOutAdv(i,1),solOutAdv(i,2),solOutAdv(i,3),solOutAdv(i,4)};
          double FLUX[5];
          _ROE3D (GAMMA,nx,ny,nz,solLeft,solRight,FLUX);*/

          for (size_t j = 0; j < 5; j ++) 
            //val (i, j) = FLUX[j] + ip (i, j);
            val (i, j) = riemann (i, j) + ip (i, j);
        }
      } else {
        advectionReplace.compute();
        for (size_t i = 0; i< nQP; i++)
          for (size_t j = 0; j < 5; j ++) 
            val (i, j) = riemann (i, j);
      }
    }
  };
  public:
  dgBoundaryConditionPerfectGasLaw3dWall(int type, dgPerfectGasLaw *claw, const function *velocityF){
    claw->checkSetup();
    switch (type) {
      case 1 : _term0 = new fullTerm (false, claw, NULL); break;
      case 2 : _term0 = new fullTerm (true, claw, NULL); break;
      case 3 : _term0 = new fullTerm (true, claw, velocityF); break;
      throw;
    }
  }
  ~dgBoundaryConditionPerfectGasLaw3dWall(){
    delete _term0;
  }
};

#if 0
// same in 3D
class dgBoundaryConditionPerfectGasLaw3dWall : public dgBoundaryCondition {
  int _type;
  class term : public dataCacheDouble {
    dataCacheDouble &sol,&normals;
  public:
    //-------------------------------------------------------------------------------
    // NON VISCOUS BOUNDARY FLUX
    //-------------------------------------------------------------------------------
    term(dataCacheMap &cacheMap):
      dataCacheDouble(cacheMap,1,5),
      sol(cacheMap.get(function::getSolution(), this)),
      normals(cacheMap.get(function::getNormals(), this)){}
    void _eval () { 
      size_t nQP = sol().size1();
      for(size_t i=0; i< nQP; i++) {
        const double nx = normals(i,0);
        const double ny = normals(i,1);
        const double nz = normals(i,2);
        
        const double solLeft [5] = {sol(i,0),sol(i,1),sol(i,2),sol(i,3),sol(i,4)};
        const double vn = (solLeft [1] * nx +  solLeft [2] * ny + solLeft [3] * nz);
        const double solRight[5] = {sol(i,0),
                                    sol(i,1) - 2 * vn  * nx,
                                    sol(i,2) - 2 * vn  * ny,
                                    sol(i,3) - 2 * vn  * nz,
                                    sol(i,4)};
        double FLUX[5] ;
        _ROE3D (GAMMA,nx,ny,nz,solLeft,solRight,FLUX);
        val(i,0) = FLUX[0];
        val(i,1) = FLUX[1];
        val(i,2) = FLUX[2];
        val(i,3) = FLUX[3];
        val(i,4) = FLUX[4];
      }
    }
  };
  
//-------------------------------------------------------------------------------
// Non Slip Dirichlet BC --
// What to put on the right side of the IP (no velocity, same density) 
// Assume Adiabatic Wall --> no heat transfer -> \partial_n T = 0
// So, we use the same temperature as inside
//-------------------------------------------------------------------------------
  class dirichletNonSlip : public dataCacheDouble {
    dataCacheDouble &sol;
    public:
    dirichletNonSlip(dataCacheMap &cacheMap):
    dataCacheDouble(cacheMap,1,5),
    sol(cacheMap.get(function::getSolution(), this)){}
    void _eval () { 
      size_t nQP = sol().size1();
      for(size_t i=0; i< nQP; i++) {
        val(i,0) = sol(i,0);
        val(i,1) = 0.0;
        val(i,2) = 0.0;
        val(i,3) = 0.0;
        val(i,4) = sol(i,4) - 0.5 * (sol(i,1) * sol(i,1) + sol(i,2) * sol(i,2)+ sol(i,3) * sol(i,3))/sol(i,0);
      }
    }
  };

//-------------------------------------------------------------------------------
// Non Slip Neumann BC --
// Compute normal diffusive fluxes at the boundary
// Assume Adiabatic Wall --> no heat transfer -> no flux for component 3 (in 2D)
//-------------------------------------------------------------------------------

  class neumannNonSlip : public dataCacheDouble {
    dgConservationLawFunction *_claw;
    dataCacheDouble &sol,&normals;
    dataCacheDouble *diffusiveFlux;
  public:
    neumannNonSlip(dataCacheMap &cacheMap, dgConservationLawFunction *claw):
      dataCacheDouble(cacheMap,1,5),
      _claw (claw),
      sol(cacheMap.get(function::getSolution(), this)),
      normals(cacheMap.get(function::getNormals(), this)){
      diffusiveFlux=_claw->newDiffusiveFlux(cacheMap);
      if (diffusiveFlux)diffusiveFlux->addMeAsDependencyOf(this);
    }
    void _eval () { 
      size_t nQP = sol().size1();
      const fullMatrix<double> &dfl = (*diffusiveFlux)();

      for(size_t i=0; i< nQP; i++) {
        for (int k=0;k<4;k++) { 
          val(i,k) = 
            dfl(i,k+5*0) *normals(i,0) +
            dfl(i,k+5*1) *normals(i,1) +
            dfl(i,k+5*2) *normals(i,2);
        }
        val(i,4) = 0.0; 
      }
    }
    ~neumannNonSlip (){}
  };

//-------------------------------------------------------------------------------
// Slip Wall Dirichlet BC --
// Assume zero normal derivatives of all variables --> put the same as inside 
//-------------------------------------------------------------------------------

  class dirichletSlip : public dataCacheDouble {
    dataCacheDouble &sol;
    public:
    dirichletSlip(dataCacheMap &cacheMap):
    dataCacheDouble(cacheMap,1,cacheMap.get(function::getSolution(), NULL)().size2()),
    sol(cacheMap.get(function::getSolution(), this)){}
    void _eval () { 
      size_t nQP = sol().size1();
      for(size_t i=0; i< nQP; i++) {
        for(int k=0; k< sol().size2(); k++) {
          val(i,k) = sol(i,k);
        }
      }
    }
  };

//-------------------------------------------------------------------------------
// Slip Wall or Symmetry Neumann BC -- assume NO FLUXES AT ALL
//-------------------------------------------------------------------------------

  class neumannSlip : public dataCacheDouble {
    dgConservationLawFunction *_claw;
    dataCacheDouble &sol,&normals;
  public:
    neumannSlip(dataCacheMap &cacheMap, dgConservationLawFunction *claw):
      _claw (claw),
      dataCacheDouble(cacheMap,1,5),
      sol(cacheMap.get(function::getSolution(), this)),
      normals(cacheMap.get(function::getNormals(), this)){
    }
    void _eval () { 
      size_t nQP = sol().size1();
      val.setAll(0.0);
    }
  };

  public:
    dgBoundaryConditionPerfectGasLaw3dWall(int type, dgPerfectGasLaw *claw):_type(type), dgBoundaryCondition(claw){}
    dataCacheDouble *newBoundaryTerm(dataCacheMap &cacheMapLeft) const {
      return new term(cacheMapLeft);
    }
    dataCacheDouble *newDiffusiveDirichletBC(dataCacheMap &cacheMapLeft) const {
      switch(_type){
      case 1 : return new dirichletSlip(cacheMapLeft);
      case 2 : return new dirichletNonSlip(cacheMapLeft);
      }
    }
    dataCacheDouble *newDiffusiveNeumannBC(dataCacheMap &cacheMapLeft) const {
      switch(_type){
      case 1 : return new neumannSlip(cacheMapLeft,_claw);
      case 2 : return new neumannNonSlip(cacheMapLeft,_claw);
      }
    }
};
#endif


dgBoundaryCondition *dgPerfectGasLaw::newNonSlipWallBoundary() {
  if(_dim == 2)
  return new dgBoundaryConditionPerfectGasLaw2dWall(2, this, NULL);
  else return new dgBoundaryConditionPerfectGasLaw3dWall(2, this, NULL);
}

dgBoundaryCondition *dgPerfectGasLaw::newSlipWallBoundary() {
  if(_dim == 2)
  return new dgBoundaryConditionPerfectGasLaw2dWall(1, this, NULL);
  else return new dgBoundaryConditionPerfectGasLaw3dWall(1, this, NULL);
}

dgBoundaryCondition *dgPerfectGasLaw::newMovingWallBoundary( const function *velocity) {
  if(_dim == 2) 
  return new dgBoundaryConditionPerfectGasLaw2dWall(3, this, velocity);
  else return new dgBoundaryConditionPerfectGasLaw3dWall(3, this, velocity);
}
