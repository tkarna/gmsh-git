#ifndef _DG_CONSERVATION_LAW_PERFECT_GAS_ALE_H_
#define _DG_CONSERVATION_LAW_PERFECT_GAS_ALE_H_
#include "dgConservationLawFunction.h"
#include "functionGeneric.h" //TODO
// Compressible Navier-Stokes equations
/**Perfect Gas conservation law (works both in 2D and 3D).
The bulk viscosity (mu^v) is here assumed zero.
 * \f[\rho \left(\frac{\partial \mathbf{v}}{\partial t} + \mathbf{v} \cdot \nabla \mathbf{v}\right) = -\nabla p + \mu \nabla^2 \mathbf{v} + ( \frac 13 \mu + \mu^v) \nabla (\nabla \cdot \mathbf{v} ) + \rho \mathbf{f}\f] 
 * or
 * \f{eqnarray*}
 * \frac{d\rho}{dt}  + \frac{d\rho u}{dx}  + \frac{d\rho v}{dy} &=& 0 \\
 * \frac{d(\rho u)}{dt} + \frac{d(\rho u^2+p)}{dx} + \frac{d(\rho u v)}{dy} - \frac{d t_{xx}}{dx} - \frac{dt_{xy}}{dy} &=& 0 \\
 * \frac{d(\rho v)}{dt} + \frac{d(\rho uv)}{dx} + \frac{d(\rho v^2+p)}{dy} - \frac{d t_{xy}}{dx} - \frac{dt_{yy}}{dy} &=& 0 \\
 * \frac{d(\rho E)}{dt} + \frac{d(\rho uH)}{dx} + \frac{d(\rho vH)}{dy} - \frac{d(ut_{xx}+vt_{xy}-q_x)}{dx} - \frac{d(ut_{xy}+vt_{yy}-q_y)}{dy}&=& 0
 * \f} 
 * with
 * \f{eqnarray*}
 * p &=& \rho R T = (\gamma -1) \left( \rho E - \rho \frac{u . u}{2}\right)\\
 * \rho H &=& \rho E + p\\
 * t_{ij} &=& 2\mu\frac{\nabla u + \nabla u^T}{2} + I \lambda \nabla \cdot u\\
 * 0 &=& dim \lambda + 2 \mu \quad \mbox{with dim the dimension of the model}\\
 * q &=& - \kappa \nabla T \\
 * C_v &=& \frac R{\gamma -1}
 * \f} 
 */

class dgPerfectGasLawALE : public dgConservationLawFunction {
  class advection;
  class diffusion;
  class riemannLF;
  class source;
  class maxConvectiveSpeed;
  class diffusivity;
  class clipToPhysics;
  class velocityVector;
  class viscousNormalStress; //TODO
  class inviscousNormalStress;
  
  // the name of the functions for 
  // viscosity (_muFunctionName)
  // thermal conductivity (_kappaFunctionName)
  const function *_totalForceSurface;
  const function *_viscousNS; //TODO
  const function *_inviscousNS;
  public:
  const function *_kappaFunction, *_muFunction;
  double _R, _gamma;
  const function *_sourceFunction, *_convectiveFlux, *_diffusiveFlux, *_IPTerm, *_riemannTerm;
  const function *_velocitySol;
  const function *_meshVelocity, *_meshVelocityDiv;
  // dimension of the problem (1,2 or 3)
  int _dim;

  void setup();
  ~dgPerfectGasLawALE();

  /**return the velocity vector */
  inline const function* getVelocity(){ return _velocitySol; };
  inline double getGamma() const {return _gamma;}
  const function * getTotalForceSurface() {checkSetup(); return _totalForceSurface;};

 /**return the viscous stresses at an interface*/
  inline const function * getViscousNormalStress(){ checkSetup(); return _viscousNS; } //TODO: done recup de DGincomp
  /**return the unviscous stresses (pressure normal) at the surface */
  inline const function * getInviscousNormalStress(){ checkSetup(); return _inviscousNS; }
  /**return the total force to the surface */
  inline const function* getTotalForce(){
    checkSetup();
    return  functionSumNew(getViscousNormalStress(), getInviscousNormalStress());
  }


  dgPerfectGasLawALE(int);  
  /**non slip wall */
  dgBoundaryCondition *newNonSlipWallBoundary(const function *meshVelFunc);
  /**slip wall */
  dgBoundaryCondition *newSlipWallBoundary(const function *meshVelFunc);
  /**set the fonctions to compute \f$\mu\f$ and \f$\kappa\f$, the scalar viscosity and thermal conductivity coefficients */
  inline void setViscosityAndThermalConductivity (const function  *mu, const function  *kappa){
    _muFunction = mu;
    _kappaFunction = kappa;
  }
  inline void setGasConstantAndGamma (double  R, double gamma_){
    _R = R;
    _gamma = gamma_;
  }
  /**set the function to compute the source term */
  inline void setSource(const function *a){
    _sourceFunction = a;
  }
  /**set the function to compute the mesh velocity */
  inline void setMeshVelocity(const function *vel, const function *velDiv){
    _meshVelocity = vel;
    _meshVelocityDiv = velDiv;
  }
};

#endif
