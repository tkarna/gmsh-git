#include "dgConservationLawShallowWaterAbsLayer2d.h"
#include "dgDofContainer.h"
#include "dgFunctionIntegrator.h"
#include "dgFunctionIntegratorInterface.h"
#include "dgGroupOfElements.h"
#include "dgMeshJacobian.h"

#include "float.h"
#include "functionGeneric.h"


/*==============================================================================
 * Roe Solver for Shallow Water Equations
 *============================================================================*/

void dgConservationLawShallowWaterAbsLayer2d::RoeSolver(double uL, double uR, double vL, double vR, double HL, double HR,
                                                        double &uStar, double &vStar, double &HStar, double g, double upwindFact) {
  
  double HuL = uL*HL, HvL = vL*HL, sqHL = sqrt(HL);
  double HuR = uR*HR, HvR = vR*HR, sqHR = sqrt(HR);
  double HMean = (HL+HR)/2, HuMean = (HuL+HuR)/2;
  double HJump = (HL-HR)/2, HuJump = (HuL-HuR)/2;
  
  double u_roe = (sqHL*uL + sqHR*uR) / (sqHL + sqHR);
  double v_roe = (sqHL*vL + sqHR*vR) / (sqHL + sqHR);
  double c_roe = sqrt(g*HMean);
  
  double Hu = HuMean + 1/c_roe * (u_roe*HuJump - (u_roe*u_roe - c_roe*c_roe) * HJump);
  double Hv = 1/c_roe * (v_roe * HuJump - u_roe*v_roe * HJump);
  if (upwindFact>0) {
    double upwindFactor = atan(u_roe/100*upwindFact)/M_PI+0.5;
    Hv += upwindFactor * (-v_roe*HJump+HvL) + (1-upwindFactor) * (v_roe*HJump+HvR);
  }
  else
    Hv += ( u_roe > 0 ? (-v_roe*HJump+HvL) : (v_roe*HJump+HvR) );
  
  HStar = HMean + (HuJump - u_roe*HJump) / c_roe;
  uStar = Hu / HStar;
  vStar = Hv / HStar;
  
}


/*==============================================================================
 * DG-terms
 *============================================================================*/

class dgConservationLawShallowWaterAbsLayer2d::sourceTerm : public function {
  int _nbf;
  bool _isLinear, _isBathymetryGradient, _isCoriolis, _isWindStress, _isBottomFriction, _isDiffusion;
  fullMatrix<double> sol, solGradient, bathymetry, bathymetryGradient, coriolisFactor, windStress, bottomFrictionCoef, diffusivity;
public:
  sourceTerm (int nbf, bool isLinear,
              const function *bathymetryF,
              const function *bathymetryGradientF,
              const function *coriolisFactorF,
              const function *windStressF,
              const function *bottomFrictionCoefF,
              const function *diffusivityF) : function(nbf) {
    
    _nbf = nbf;
    _isLinear = isLinear;
    
    _isBathymetryGradient = bathymetryGradientF;
    _isCoriolis = coriolisFactorF;
    _isWindStress = windStressF;
    _isBottomFriction = bottomFrictionCoefF;
    _isDiffusion = diffusivityF;
    
    setArgument(sol, function::getSolution());
    setArgument(bathymetry, bathymetryF);
    if ((!_isLinear)||(_isDiffusion))
      setArgument(solGradient, function::getSolutionGradient());
    if (_isBathymetryGradient)
      setArgument(bathymetryGradient, bathymetryGradientF);
    if (_isCoriolis)
      setArgument(coriolisFactor, coriolisFactorF);
    if (_isWindStress)
      setArgument(windStress, windStressF);
    if (_isBottomFriction)
      setArgument(bottomFrictionCoef, bottomFrictionCoefF);
    if (_isDiffusion) {
      if (!_isBathymetryGradient)
        Msg::Fatal("Dg/SW/AbsLayer: BathymetryGradient required for terms of diffusion.");
      setArgument(diffusivity, diffusivityF);
    }
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP =val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      double eta = sol(i,0);
      double u = sol(i,1);
      double v = sol(i,2);
      double H = bathymetry(i,0);
      
      if (!_isLinear) {
        H += eta;
        double divU = solGradient(i,3) + solGradient(i,7);
        val(i,1) += divU*u;
        val(i,2) += divU*v;
      }
      if (_isCoriolis) {
        val(i,1) =  coriolisFactor(i,0) * v;
        val(i,2) = -coriolisFactor(i,0) * u;
      }
      if (_isWindStress) {
        double density = 1e6;
        val(i,1) += windStress(i,0) / (density * H);
        val(i,2) += windStress(i,1) / (density * H);
      }
      if (_isBottomFriction) {
        double normUV = sqrt(u*u+v*v);
        val(i,1) -= bottomFrictionCoef(i,0) * u*normUV;
        val(i,2) -= bottomFrictionCoef(i,0) * v*normUV;
      }
      
      if (_isDiffusion) {
        double dHdx = bathymetryGradient(i,0);
        double dHdy = bathymetryGradient(i,1);
        if (!_isLinear) {
          dHdx += solGradient(i,0);
          dHdy += solGradient(i,1);
        }
        val(i,1) += diffusivity(i,0)/H * (dHdx*solGradient(i,3) + dHdy*solGradient(i,4));
        val(i,2) += diffusivity(i,0)/H * (dHdx*solGradient(i,6) + dHdy*solGradient(i,7));
      }
      
    }
  }
};

class dgConservationLawShallowWaterAbsLayer2d::gradPsiTerm: public function {
  int _nbf;
  bool _isLinear, _isDiffusion;
  fullMatrix<double> sol, gravity, bathymetry, diffusiveFlux;
public:
  gradPsiTerm (int nbf, bool isLinear,
               const function *gravityF,
               const function *bathymetryF,
               const function *diffusiveFluxF) : function(3*nbf) {
    
    _nbf = nbf;
    _isLinear = isLinear;
    _isDiffusion = diffusiveFluxF;
    
    setArgument(sol, function::getSolution());
    setArgument(gravity, gravityF);
    setArgument(bathymetry, bathymetryF);
    if (_isDiffusion)
      setArgument(diffusiveFlux,diffusiveFluxF);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      double eta = sol(i,0);
      double u = sol(i,1);
      double v = sol(i,2);
      
      double g = gravity(i,0);
      double H = bathymetry(i,0);
      if (!_isLinear)
        H += eta;
      
      // flux_x
      val(i,0) = H*u;
      val(i,1) = g*eta;
      val(i,2) = 0.;
      if (!_isLinear) {
        val(i,1) += u*u;
        val(i,2) += u*v;
      }
      if (_isDiffusion) {
        val(i,1) += diffusiveFlux(i,1);
        val(i,2) += diffusiveFlux(i,2);
      }
      
      // flux_y
      val(i,_nbf+0) = H*v;
      val(i,_nbf+1) = 0.;
      val(i,_nbf+2) = g*eta;
      if (!_isLinear) {
        val(i,_nbf+1) += v*u;
        val(i,_nbf+2) += v*v;
      }
      if (_isDiffusion) {
        val(i,_nbf+1) += diffusiveFlux(i,_nbf+1);
        val(i,_nbf+2) += diffusiveFlux(i,_nbf+2);
      }
      
    }
  }
};

class dgConservationLawShallowWaterAbsLayer2d::interfaceTerm:public function {
  int _nbf;
  bool _isLinear, _isDiffusion;
  double _upwindFactor;
  fullMatrix<double> solL, solR, normals, gravity, bathymetry, ipTerm;
public:
  interfaceTerm (int nbf, bool isLinear,
                 const function *gravityF,
                 const function *bathymetryF,
                 const function *ipTermF,
                 const double upwindFactorRiemann) : function(2*nbf) {
    
    _nbf = nbf;
    _isLinear = isLinear;
    _isDiffusion = ipTermF;
    _upwindFactor = upwindFactorRiemann;
    
    setArgument(solL, function::getSolution(), 0);
    setArgument(solR, function::getSolution(), 1);
    setArgument(normals, function::getNormals(), 0);
    setArgument(gravity, gravityF);
    setArgument(bathymetry, bathymetryF);
    if (_isDiffusion)
      setArgument(ipTerm, ipTermF);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double nx = normals(i,0);
      double ny = normals(i,1);
      
      double etaL = solL(i,0);
      double etaR = solR(i,0);
      double uL =  nx*solL(i,1) + ny*solL(i,2);
      double uR =  nx*solR(i,1) + ny*solR(i,2);
      double vL = -ny*solL(i,1) + nx*solL(i,2);
      double vR = -ny*solR(i,1) + nx*solR(i,2);
      
      double g = gravity(i,0);
      double h = bathymetry(i,0);
      
      double Feta, Fun, Fut;
      
      if (_isLinear) {
        
        double etaRiemann = (etaL+etaR)/2 + sqrt(h/g) * (uL-uR)/2;
        double unRiemann  = (uL+uR)/2  + sqrt(g/h) * (etaL-etaR)/2;
        
        Fun = -g*etaRiemann;
        Fut = 0;
        Feta = -h*unRiemann;
        
      } else {
        
        double HL = etaL+h;
        double HR = etaR+h;
        
        if (HR<0 || HL<0) printf(" HR = %e HL =%e\n", HR,HL);
        
        double u, v, H;
        RoeSolver(uL, uR, vL, vR, HL, HR, u, v, H, g, _upwindFactor);
        double eta = H-h;
        
        Feta = -H*u;
        Fun  = -g*eta -u*u;
        Fut  =        -u*v;
        
      }
      
      val(i,0) = Feta;
      val(i,1) = (Fun * nx - Fut * ny);
      val(i,2) = (Fun * ny + Fut * nx);
      
      for (int j=0; j<_nbf; j++)
        val(i,_nbf+j) = -val(i,j);
      
      if (_isDiffusion) {
        val(i,1) += ipTerm(i,1);
        val(i,2) += ipTerm(i,2);
        val(i,_nbf+1) += ipTerm(i,_nbf+1);
        val(i,_nbf+2) += ipTerm(i,_nbf+2);
      }
      
    }
  }
};

class dgConservationLawShallowWaterAbsLayer2d::diffusiveFlux : public function {
  int _nbf;
  fullMatrix<double> solGrad, diffusivity;
public:
  diffusiveFlux(int nbf, const function *diffusivityF) : function(2*nbf) {
    _nbf = nbf;
    setArgument(solGrad,function::getSolutionGradient());
    setArgument(diffusivity,diffusivityF);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      val(i,1) = -diffusivity(i,0)*solGrad(i,3);
      val(i,2) = -diffusivity(i,0)*solGrad(i,6);
      val(i,_nbf+1) = -diffusivity(i,0)*solGrad(i,4);
      val(i,_nbf+2) = -diffusivity(i,0)*solGrad(i,7);
    }
  }
};

class dgConservationLawShallowWaterAbsLayer2d::diffusivitySmagorinsky : public function {
  fullMatrix<double> solGrad, lengthMesh;
public:
  diffusivitySmagorinsky(const function *lengthMeshF) : function(1) {
    setArgument(solGrad,function::getSolutionGradient());
    setArgument(lengthMesh,lengthMeshF);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    Msg::Warning("Dg/SW/AbsLayer: diffusivitySmagorinsky does not work !!!");
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      double lc = lengthMesh(i,0);
      double dxU = solGrad(i,3);
      double dyU = solGrad(i,4);
      double dxV = solGrad(i,6);
      double dyV = solGrad(i,7);
      val(i,0) = pow(0.1*lc,2) * pow(2.*pow(dxU,2) + 2.*pow(dyV,2) + pow(dyU+dxV,2), 0.5);
    }
  }
};

class dgConservationLawShallowWaterAbsLayer2d::maxConvectiveSpeed :  public function {
  fullMatrix<double> sol, gravity, bathymetry;
  bool _isLinear;
public:
  maxConvectiveSpeed (int nbf, bool isLinear,
                      const function *gravityF,
                      const function *bathymetryF) : function(nbf) {
    _isLinear = isLinear;
    setArgument(sol, function::getSolution());
    setArgument(gravity, gravityF);
    setArgument(bathymetry, bathymetryF);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    if (_isLinear)
      for (int i=0; i<val.size1(); i++)
        val(i,0) = sqrt(gravity(i,0)*bathymetry(i,0));
    else
      for (int i=0; i<val.size1(); i++)
        val(i,0) = sqrt((bathymetry(i,0)+sol(i,0))*gravity(i,0))+hypot(sol(i,1),sol(i,2));
  }
};


/*==============================================================================
 * DG-terms for Layer
 *============================================================================*/

class dgConservationLawShallowWaterAbsLayer2d::sourceTermLayer: public function {
  int _nbf;
  std::string _LayerVersion;
  bool _isLinear, _isBathymetryGradient, _isCoriolis, _isWindStress;
  bool _isBottomFriction, _isDiffusion, _isAbsLayer, _isAbsLayerDir, _isAbsLayerExtFields, _FLAG2;
  fullMatrix<double> sol, solGradient, bathymetry, bathymetryGradient, coriolisFactor, windStress;
  fullMatrix<double> bottomFrictionCoef, diffusivity, absLayerCoef, absLayerDir, absLayerExtFields;
public:
  sourceTermLayer (int nbf, bool isLinear, std::string LayerVersion,
                   const function *bathymetryF,
                   const function *bathymetryGradientF,
                   const function *coriolisFactorF,
                   const function *windStressF,
                   const function *bottomFrictionCoefF,
                   const function *diffusivityF,
                   const function *absLayerCoefF,
                   const function *absLayerDirF,
                   const function *absLayerExtFieldsF,
                   bool FLAG1,
                   bool FLAG2) : function(nbf) {
    
    _nbf = nbf;
    _FLAG2 = FLAG2;
    _isLinear = isLinear;
    _LayerVersion = LayerVersion;
    
    _isBathymetryGradient = bathymetryGradientF;
    _isCoriolis = coriolisFactorF;
    _isWindStress = windStressF;
    _isBottomFriction = bottomFrictionCoefF;
    _isDiffusion = diffusivityF;
    _isAbsLayer = absLayerCoefF;
    _isAbsLayerDir = absLayerDirF;
    _isAbsLayerExtFields = absLayerExtFieldsF;
    
    if (!FLAG1) {
      _isLinear = false;
      _isBathymetryGradient = false;
      //_isCoriolis = NULL;
      //_isWindStress = NULL;
      _isBottomFriction = false;
      _isDiffusion = false;
    }
    
    setArgument(sol, function::getSolution());
    setArgument(bathymetry, bathymetryF);
    if (!_isLinear || _isDiffusion)
      setArgument(solGradient, function::getSolutionGradient());
    if (_isBathymetryGradient)
      setArgument(bathymetryGradient, bathymetryGradientF);
    if (_isCoriolis)
      setArgument(coriolisFactor, coriolisFactorF);
    if (_isWindStress)
      setArgument(windStress, windStressF);
    if (_isBottomFriction)
      setArgument(bottomFrictionCoef, bottomFrictionCoefF);
    if (_isDiffusion) {
      if (!_isBathymetryGradient)
        Msg::Fatal("Dg/SW/AbsLayer: BathymetryGradient required for terms of diffusion.");
      setArgument(diffusivity, diffusivityF);
    }
    if (_isAbsLayer)
      setArgument(absLayerCoef, absLayerCoefF);
    else
      if (LayerVersion!="WithoutLayer")
        Msg::Fatal("Dg/SW/AbsLayer: absLayerCoef required if 'LayerVersion' different from 'WithoutLayer'.");
    if (_isAbsLayerDir)
      setArgument(absLayerDir, absLayerDirF);
    else
      if ((LayerVersion=="PmlStraightForPureWave") || (LayerVersion=="PmlStraight"))
        Msg::Fatal("Dg/SW/AbsLayer: absLayerDir required if 'LayerVersion' is 'PmlStraightForPureWave' or 'PmlStraight'.");
    if (_isAbsLayerExtFields)
      setArgument(absLayerExtFields, absLayerExtFieldsF);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP =val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      double eta = sol(i,0);
      double u = sol(i,1);
      double v = sol(i,2);
      double H = bathymetry(i,0);
      double density = 1e6;
      double normUV=0., dHdx=0., dHdy=0., divU=0.;
      
      // Classical terms
      
      if (!_isLinear) {
        H += eta;
        divU = solGradient(i,3) + solGradient(i,7);
        val(i,1) += divU*u;
        val(i,2) += divU*v;
      }
      if (_isCoriolis) {
        val(i,1) =  coriolisFactor(i,0) * v;
        val(i,2) = -coriolisFactor(i,0) * u;
      }
      if (_isWindStress) {
        val(i,1) += windStress(i,0) / (density * H);
        val(i,2) += windStress(i,1) / (density * H);
      }
      if (_isBottomFriction) {
        normUV = sqrt(u*u+v*v);
        val(i,1) -= bottomFrictionCoef(i,0) * u*normUV;
        val(i,2) -= bottomFrictionCoef(i,0) * v*normUV;
      }
      if (_isDiffusion) {
        dHdx = bathymetryGradient(i,0);
        dHdy = bathymetryGradient(i,1);
        if (!_isLinear) {
          dHdx += solGradient(i,0);
          dHdy += solGradient(i,1);
        }
        val(i,1) += diffusivity(i,0)/H * (dHdx*solGradient(i,3) + dHdy*solGradient(i,4));
        val(i,2) += diffusivity(i,0)/H * (dHdx*solGradient(i,6) + dHdy*solGradient(i,7));
      }
      
      // Absorbing layer terms
      
      double etaF = 0., uF = 0., vF = 0.;
      if (_isAbsLayerExtFields) {
        etaF = absLayerExtFields(i,0);
        uF   = absLayerExtFields(i,1);
        vF   = absLayerExtFields(i,2);
      }
      
      if (_LayerVersion=="FRS") {
        double sigma = absLayerCoef(i,0);
        val(i,0) -= sigma * (eta-etaF);
        val(i,1) -= sigma * (u-uF);
        val(i,2) -= sigma * (v-vF);
      }
      else if (_LayerVersion=="Lavelle") {
        double sigma[2] = {absLayerCoef(i,0), absLayerCoef(i,1)};
        val(i,0) -= (sigma[0]+sigma[1]) * (eta-etaF);
        val(i,1) -= sigma[0] * (u-uF);
        val(i,2) -= sigma[1] * (v-vF);
      }
      else if (_LayerVersion=="PmlStraightForPureWave" || _LayerVersion=="PmlStraight") {
        double sigma = absLayerCoef(i,0);
        double a[2] = {absLayerDir(i,0), absLayerDir(i,1)};
        double tmp = a[0]*(u-uF) + a[1]*(v-vF);
        if (_LayerVersion=="PmlStraight")
          tmp -= sol(i,4);
        val(i,0) -= sigma * sol(i,3);
        val(i,1) -= sigma * a[0] * tmp;
        val(i,2) -= sigma * a[1] * tmp;
        val(i,3) -= sigma * sol(i,3);
        if (_LayerVersion=="PmlStraight") {
          if ((!_isLinear) and (_FLAG2))
            val(i,4) += divU * (a[0]*u + a[1]*v);
          if (_isCoriolis)
            val(i,4) += coriolisFactor(i,0) * (a[0]*(v-vF) - a[1]*(u-uF));
          if (_isWindStress)
            val(i,4) += (a[0]*windStress(i,0) + a[1]*windStress(i,1)) / (density * H);
          if (_isBottomFriction and _FLAG2)
            val(i,4) -= bottomFrictionCoef(i,0) * (a[0]*u + a[1]*v) * normUV;
          if (_isDiffusion and _FLAG2) {
            val(i,4) += diffusivity(i,0)/H * a[0] * (dHdx*solGradient(i,3) + dHdy*solGradient(i,4));
            val(i,4) += diffusivity(i,0)/H * a[1] * (dHdx*solGradient(i,6) + dHdy*solGradient(i,7));
          }
        }
      }
      
    }
  }
};

class dgConservationLawShallowWaterAbsLayer2d::gradPsiTermLayer : public function {
  int _nbf;
  std::string _LayerVersion;
  bool _isLinear, _isDiffusion, _isAbsLayer, _isAbsLayerDir, _isAbsLayerExtFields, _FLAG2;
  fullMatrix<double> sol, gravity, bathymetry, diffusiveFlux, absLayerCoef, absLayerDir, absLayerExtFields;
public:
  gradPsiTermLayer (int nbf, bool isLinear, std::string LayerVersion,
                    const function *gravityF,
                    const function *bathymetryF,
                    const function *diffusiveFluxF,
                    const function *absLayerCoefF,
                    const function *absLayerDirF,
                    const function *absLayerExtFieldsF,
                    bool FLAG1,
                    bool FLAG2) : function(3*nbf) {
    
    _nbf = nbf;
    _FLAG2 = FLAG2;
    _isLinear = isLinear;
    _LayerVersion = LayerVersion;
    
    _isDiffusion = diffusiveFluxF;
    _isAbsLayer = absLayerCoefF;
    _isAbsLayerDir = absLayerDirF;
    _isAbsLayerExtFields = absLayerExtFieldsF;
    
    if (!FLAG1) {
      _isLinear = false;
      _isDiffusion = false;
    }
    
    setArgument(sol, function::getSolution());
    setArgument(gravity, gravityF);
    setArgument(bathymetry, bathymetryF);
    if (_isDiffusion)
      setArgument(diffusiveFlux,diffusiveFluxF);
    if (_isAbsLayer)
      setArgument(absLayerCoef, absLayerCoefF);
    else
      if (LayerVersion!="WithoutLayer")
        Msg::Fatal("Dg/SW/AbsLayer: absLayerCoef required if 'LayerVersion' different from 'WithoutLayer'.");
    if (_isAbsLayerDir)
      setArgument(absLayerDir, absLayerDirF);
    else
      if ((LayerVersion=="PmlStraightForPureWave") || (LayerVersion=="PmlStraight"))
        Msg::Fatal("Dg/SW/AbsLayer: absLayerDir required if 'LayerVersion' is 'PmlStraightForPureWave' or 'PmlStraight'.");
    if (_isAbsLayerExtFields)
      setArgument(absLayerExtFields, absLayerExtFieldsF);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      double eta = sol(i,0);
      double u = sol(i,1);
      double v = sol(i,2);
      
      double g = gravity(i,0);
      double H = bathymetry(i,0);
      if (!_isLinear)
        H += eta;
      
      // Classical terms
      
      val(i,0)      = H*u;
      val(i,1)      = g*eta;
      val(i,2)      = 0.;
      
      val(i,_nbf+0) = H*v;
      val(i,_nbf+1) = 0.;
      val(i,_nbf+2) = g*eta;
      
      if (!_isLinear) {
        val(i,1)      += u*u;
        val(i,2)      += u*v;
        val(i,_nbf+1) += v*u;
        val(i,_nbf+2) += v*v;
      }
      
      if (_isDiffusion) {
        val(i,1)      += diffusiveFlux(i,1);
        val(i,2)      += diffusiveFlux(i,2);
        val(i,_nbf+1) += diffusiveFlux(i,_nbf+1);
        val(i,_nbf+2) += diffusiveFlux(i,_nbf+2);
      }
      
      // Absorbing layer terms
      
      if ((_LayerVersion=="PmlStraightForPureWave") ||
          (_LayerVersion=="PmlStraight")) {
        
        double uF = 0., vF = 0.;
        if (_isAbsLayerExtFields) {
          uF   = absLayerExtFields(i,1);
          vF   = absLayerExtFields(i,2);
        }
        
        double a[2] = {absLayerDir(i,0), absLayerDir(i,1)};
        double udota = (u-uF)*a[0] + (v-vF)*a[1];
        
        val(i,3)      = H * udota * a[0];
        val(i,_nbf+3) = H * udota * a[1];
        
        if ((_LayerVersion=="PmlStraight") and (_isDiffusion) and (_FLAG2)) {
          val(i,4)      += (a[0]*diffusiveFlux(i,1)      + a[1]*diffusiveFlux(i,2));
          val(i,_nbf+4) += (a[0]*diffusiveFlux(i,_nbf+1) + a[1]*diffusiveFlux(i,_nbf+2));
        }
      }
      
    }
  }
};

class dgConservationLawShallowWaterAbsLayer2d::interfaceTermLayer : public function {
  int _nbf;
  std::string _LayerVersion;
  bool _isLinear, _isDiffusion, _isAbsLayerDir, _isAbsLayerExtFields, _FLAG2;
  double _upwindFactor;
  fullMatrix<double> solL, solR, normals, gravity, bathymetry, ipTerm, absLayerDir, absLayerExtFields;
public:
  interfaceTermLayer (int nbf, bool isLinear, std::string LayerVersion,
                      const function *gravityF,
                      const function *bathymetryF,
                      const function *ipTermF,
                      const function *absLayerCoefF,
                      const function *absLayerDirF,
                      const function *absLayerExtFieldsF,
                      const double upwindFactorRiemann,
                      bool FLAG1,
                      bool FLAG2) : function(2*nbf) {
    
    _nbf = nbf;
    _FLAG2 = FLAG2;
    _isLinear = isLinear;
    _LayerVersion = LayerVersion;
    
    _isDiffusion = ipTermF;
    _isAbsLayerDir = absLayerDirF;
    _isAbsLayerExtFields = absLayerExtFieldsF;
    _upwindFactor = upwindFactorRiemann;
    
    if (!FLAG1) {
      _isLinear = false;
      _isDiffusion = false;
    }
    
    setArgument(solL, function::getSolution(), 0);
    setArgument(solR, function::getSolution(), 1);
    setArgument(normals, function::getNormals(), 0);
    setArgument(gravity, gravityF);
    setArgument(bathymetry, bathymetryF);
    if (_isDiffusion)
      setArgument(ipTerm, ipTermF);
    if (_isAbsLayerDir)
      setArgument(absLayerDir, absLayerDirF);
    else
      if ((LayerVersion=="PmlStraightForPureWave") || (LayerVersion=="PmlStraight"))
        Msg::Fatal("Dg/SW/AbsLayer: absLayerDir required if 'LayerVersion' is 'PmlStraightForPureWave' or 'PmlStraight'.");
    if (_isAbsLayerExtFields)
      setArgument(absLayerExtFields, absLayerExtFieldsF);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double nx = normals(i,0);
      double ny = normals(i,1);
      
      double etaL = solL(i,0);
      double etaR = solR(i,0);
      double uL =  nx*solL(i,1) + ny*solL(i,2);
      double uR =  nx*solR(i,1) + ny*solR(i,2);
      double vL = -ny*solL(i,1) + nx*solL(i,2);
      double vR = -ny*solR(i,1) + nx*solR(i,2);
      
      double g = gravity(i,0);
      double h = bathymetry(i,0);
      
      double Feta, Fun, Fut;
      
      // Classical terms
      
      if (_isLinear) {
        
        double etaRiemann = (etaL+etaR)/2 - sqrt(h/g) * (uR-uL)/2;
        double unRiemann  = (uL+uR)/2  - sqrt(g/h) * (etaR-etaL)/2;
        
        Fun = -g*etaRiemann;
        Fut = 0;
        Feta = -h*unRiemann;
        
      } else {
        
        double HL = etaL+h;
        double HR = etaR+h;
        
        if (HR<0 || HL<0) printf(" HR = %e HL =%e\n", HR,HL);
        
        double u, v, H;
        RoeSolver(uL, uR, vL, vR, HL, HR, u, v, H, g, _upwindFactor);
        double eta = H-h;
        
        Feta = -H*u;
        Fun  = -g*eta -u*u;
        Fut  =        -u*v;
        
      }
      
      val(i,0) = Feta;
      val(i,1) = (Fun * nx - Fut * ny);
      val(i,2) = (Fun * ny + Fut * nx);
      
      for (int j=0; j<_nbf; j++)
        val(i,_nbf+j) = -val(i,j);
      
      if (_isDiffusion) {
        val(i,1) += ipTerm(i,1);
        val(i,2) += ipTerm(i,2);
        val(i,_nbf+1) += ipTerm(i,_nbf+1);
        val(i,_nbf+2) += ipTerm(i,_nbf+2);
      }
      
      // Absorbing layer terms
      
      if ((_LayerVersion=="PmlStraightForPureWave") ||
          (_LayerVersion=="PmlStraight")) {
        
        double uF = 0., vF = 0.;
        if (_isAbsLayerExtFields) {
          uF   = absLayerExtFields(i,1);
          vF   = absLayerExtFields(i,2);
        }
        
        double a[2] = {absLayerDir(i,0), absLayerDir(i,1)};
        double adotn  = a[0]*nx + a[1]*ny;
        double adotuL = a[0]*(solL(i,1)-uF) + a[1]*(solL(i,2)-vF);
        double adotuR = a[0]*(solR(i,1)-uF) + a[1]*(solR(i,2)-vF);
        double etaNL = solL(i,3);
        double etaNR = solR(i,3);
        val(i,3)      = - h * ( adotn*(adotuR + adotuL)/2. - sqrt(g/h) * (etaNR-etaNL)/2. );
        val(i,_nbf+3) = - val(i,3);
        
        if ((_LayerVersion=="PmlStraight") and (_isDiffusion) and (_FLAG2)) {
          val(i,4)      += (a[0]*ipTerm(i,1)      + a[1]*ipTerm(i,2));
          val(i,_nbf+1) += (a[0]*ipTerm(i,_nbf+1) + a[1]*ipTerm(i,_nbf+2));
        }
        
      }
      
    }
  }
};


/*==============================================================================
 * Conservation law : constructor - setup - destructor
 *============================================================================*/

int dgConservationLawShallowWaterAbsLayer2_nbf(std::string LayerVersion) {
  if ((LayerVersion=="WithoutLayer") ||             // Without layer
      (LayerVersion=="FRS") ||                      // FRS
      (LayerVersion=="Lavelle"))                    // Lavelle
    return 3;
  else if (LayerVersion=="PmlStraightForPureWave")  // Straight PML only with propagation terms 
    return 4;
  else if (LayerVersion=="PmlStraight")             // Straight PML
    return 5;
  else
    Msg::Fatal("Dg/SW/AbsLayer: Bad 'LayerVersion'.");
  return -1;
}

dgConservationLawShallowWaterAbsLayer2d::dgConservationLawShallowWaterAbsLayer2d(std::string LayerVersion) : 
dgConservationLawFunction(dgConservationLawShallowWaterAbsLayer2_nbf(LayerVersion))
{
  _fzero = new functionConstant(0.);
  _isLinear = false;
  _LayerVersion = LayerVersion;
  _coriolisFactor = NULL;
  _gravityAcceleration = NULL;
  _bathymetry = NULL;
  _bathymetryGradient = NULL;
  _windStress = NULL;
  _bottomFrictionCoef = NULL;
  _diffusivity = NULL;
  _lengthMeshSmagorinskyDiffusivity = NULL;
  _diffusivityVec = NULL;
  _diffusiveFlux = NULL;
  _ipTerm = NULL;
  _absLayerCoef = 0;
  _absLayerDir = 0;
  _upwindFactorRiemann = -1;
}

void dgConservationLawShallowWaterAbsLayer2d::setup() {
  if (_lengthMeshSmagorinskyDiffusivity)
    _diffusivity = new diffusivitySmagorinsky(_lengthMeshSmagorinskyDiffusivity);
  if (_diffusivity) {
    Msg::Warning("Dg/SW/AbsLayer: DIFFUSION has not been (really) checked yet. If it works, remove this comment.");
    std::vector<const function*> fArray;
    fArray.push_back(_fzero);
    for (int i=1; i<_nbf; i++)
      fArray.push_back(_diffusivity);
    _diffusivityVec = functionCatCompNew(fArray);
    _diffusiveFlux = new diffusiveFlux(_nbf, _diffusivity);
    _ipTerm = dgNewIpTerm(_nbf, _diffusiveFlux, _diffusivityVec);
  }
  _volumeTerm0[""] = new sourceTerm(_nbf, _isLinear, _bathymetry, _bathymetryGradient, _coriolisFactor, _windStress, _bottomFrictionCoef, _diffusivity);
  _volumeTerm1[""] = new gradPsiTerm(_nbf, _isLinear, _gravityAcceleration, _bathymetry, _diffusiveFlux);
  _interfaceTerm0[""] = new interfaceTerm(_nbf, _isLinear, _gravityAcceleration, _bathymetry, _ipTerm, _upwindFactorRiemann);
  _maximumConvectiveSpeed[""] = new maxConvectiveSpeed(_nbf, _isLinear, _gravityAcceleration, _bathymetry);
}

dgConservationLawShallowWaterAbsLayer2d::~dgConservationLawShallowWaterAbsLayer2d() {
  delete _fzero;
  if (_diffusivity) {
    delete _diffusivityVec;
    delete _diffusiveFlux;
    delete _ipTerm;
  }
  if (_lengthMeshSmagorinskyDiffusivity)
    delete _diffusivity;
  for (termMap::const_iterator it=_volumeTerm0.begin(); it!=_volumeTerm0.end(); it++)
    if (_volumeTerm0[it->first]) delete _volumeTerm0[it->first];
  for (termMap::const_iterator it=_volumeTerm1.begin(); it!=_volumeTerm1.end(); it++)
    if (_volumeTerm1[it->first]) delete _volumeTerm1[it->first];
  for (termMap::const_iterator it=_interfaceTerm0.begin(); it!=_interfaceTerm0.end(); it++)
    if (_interfaceTerm0[it->first]) delete _interfaceTerm0[it->first];
  for (termMap::const_iterator it=_maximumConvectiveSpeed.begin(); it!=_maximumConvectiveSpeed.end(); it++)
    if (_maximumConvectiveSpeed[it->first]) delete _maximumConvectiveSpeed[it->first];
}


/*==============================================================================
 * Add a layer
 *============================================================================*/

void dgConservationLawShallowWaterAbsLayer2d::addLayer(const std::string tag, const function *incidentFieldsF, bool FLAG1, bool FLAG2) {
  if (_LayerVersion=="BasicLayer")
    Msg::Error("Dg/SW/AbsLayer: addLayer doesn't work with 'LayerVersion'='BasicLayer'.");
  else if ((_LayerVersion=="FRS") ||
           (_LayerVersion=="Lavelle") ||
           (_LayerVersion=="PmlStraightForPureWave") ||
           (_LayerVersion=="PmlStraight")) {
    if (_volumeTerm0[tag])    delete _volumeTerm0[tag];
    if (_volumeTerm1[tag])    delete _volumeTerm1[tag];
    if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
    _volumeTerm0[tag]    = new sourceTermLayer(_nbf, _isLinear, _LayerVersion,
                                               _bathymetry, _bathymetryGradient, _coriolisFactor, _windStress, _bottomFrictionCoef, _diffusivity,
                                               _absLayerCoef, _absLayerDir, incidentFieldsF, FLAG1, FLAG2);
    _volumeTerm1[tag]    = new gradPsiTermLayer(_nbf, _isLinear, _LayerVersion,
                                                _gravityAcceleration, _bathymetry, _diffusiveFlux,
                                                _absLayerCoef, _absLayerDir, incidentFieldsF, FLAG1, FLAG2);
    _interfaceTerm0[tag] = new interfaceTermLayer(_nbf, _isLinear, _LayerVersion,
                                                  _gravityAcceleration, _bathymetry, _ipTerm,
                                                  _absLayerCoef, _absLayerDir, incidentFieldsF, _upwindFactorRiemann, FLAG1, FLAG2);
  } else
    Msg::Fatal("Dg/SW/AbsLayer: Bad 'LayerVersion'.");
}


/*==============================================================================
 * Specific boundary conditions
 *============================================================================*/

class dgConservationLawShallowWaterAbsLayer2d::boundaryCondition : public dgBoundaryCondition {
  class term : public function {
    int _nbf, _typeBC;
    bool _isLinear, _isAbsLayerDir;
    double _upwindFactor;
    std::string _LayerVersion;
    fullMatrix<double> sol, normals, gravity, bathymetry, extData, absLayerDir;
  public:
    term(int nbf, int typeBC, std::string LayerVersion,
         const function *gravityF, const function *bathymetryF, bool isLinear,
         const double upwindFactorRiemann,
         const function *absLayerDirF,
         const function *extDataFunction) : function(nbf) {
      
      _nbf = nbf;
      _typeBC = typeBC;
      _isLinear = isLinear;
      _upwindFactor = upwindFactorRiemann;
      _LayerVersion = LayerVersion;
      _isAbsLayerDir = absLayerDirF;
      
      setArgument(sol, function::getSolution());
      setArgument(normals, function::getNormals());
      setArgument(gravity, gravityF);
      setArgument(bathymetry, bathymetryF);
      
      if (_isAbsLayerDir)
        setArgument(absLayerDir, absLayerDirF);
      else
        if ((LayerVersion=="PmlStraightForPureWave") || (LayerVersion=="PmlStraight"))
          Msg::Fatal("Dg/SW/AbsLayer: absLayerDir required if 'LayerVersion' is 'PmlStraightForPureWave' or 'PmlStraight'.");
      
      if (extDataFunction != NULL)
        setArgument (extData, extDataFunction);
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      val.scale(0.);
      size_t nQP = sol.size1();
      for(size_t i=0; i< nQP; i++) {
        
        double nx = normals(i,0);
        double ny = normals(i,1);
        
        double etaL=0, uL=0, vL=0;
        double etaR=0, uR=0, vR=0;
        
        switch (_typeBC) {
            
            // Dirichlet BC on u with external fields  (extData contains 'uExt')
          case 1:
            etaL = sol(i,0);
            etaR = sol(i,0);
            uL =  nx*sol(i,1) + ny*sol(i,2);
            uR =  nx*extData(i,0) + ny*extData(i,1);
            vL = -ny*sol(i,1) + nx*sol(i,2);
            vR = -ny*sol(i,1) + nx*sol(i,2);
            break;
            
            // Dirichlet BC on u with flux  (extData contains 'ndotuExt')
          case 2:
            etaL = sol(i,0);
            etaR = sol(i,0);
            uL =  nx*sol(i,1) + ny*sol(i,2);
            uR =  extData(i,0);
            vL = -ny*sol(i,1) + nx*sol(i,2);
            vR = -ny*sol(i,1) + nx*sol(i,2);
            break;
            
            // Homogeneous Dirichlet BC on u  (extData is EMPTY)
          case 3:
            etaL = sol(i,0);
            etaR = sol(i,0);
            uL =  nx*sol(i,1) + ny*sol(i,2);
            uR =  0;
            vL = -ny*sol(i,1) + nx*sol(i,2);
            vR = -ny*sol(i,1) + nx*sol(i,2);
            break;
            
            // Dirichlet BC on p with external fields  (extData contains 'pExt')
          case 4:
            etaL = sol(i,0);
            etaR = extData(i,0);
            uL =  nx*sol(i,1) + ny*sol(i,2);
            uR =  nx*sol(i,1) + ny*sol(i,2);
            vL = -ny*sol(i,1) + nx*sol(i,2);
            vR = -ny*sol(i,1) + nx*sol(i,2);
            break;
            
            // Homogeneous Dirichlet BC on p  (extData is EMPTY)
          case 5:
            etaL = sol(i,0);
            etaR = 0;
            uL =  nx*sol(i,1) + ny*sol(i,2);
            uR =  nx*sol(i,1) + ny*sol(i,2);
            vL = -ny*sol(i,1) + nx*sol(i,2);
            vR = -ny*sol(i,1) + nx*sol(i,2);
            break;
            
            // Open BC (extData is EMPTY) - Flather
          case 6:
            etaL = sol(i,0);
            etaR = 0;
            uL =  nx*sol(i,1) + ny*sol(i,2);
            uR =  0;
            vL = -ny*sol(i,1) + nx*sol(i,2);
            vR =  0;
            break;
            
            // Open BC with inflow (extData contains all external fields) - Flather
          case 7:
            etaL = sol(i,0);
            etaR = extData(i,0);
            uL =  nx*sol(i,1)     + ny*sol(i,2);
            uR =  nx*extData(i,1) + ny*extData(i,2);
            vL = -ny*sol(i,1)     + nx*sol(i,2);
            vR = -ny*extData(i,1) + nx*extData(i,2);
            break;
            
          default:
            Msg::Fatal("Dg/SW/AbsLayer: There is an error in the BOUNDARY CONDITION (BAD '_typeBC': '%i')", _typeBC);
            
        }
        
        double g = gravity(i,0);
        double h = bathymetry(i,0);
        double Feta, Fun, Fut;
        
        if (_isLinear) {
          
          double etaRiemann = (etaR+etaL)/2 - sqrt(h/g) * (uR-uL)/2;
          double unRiemann  = (uR+uL)/2  - sqrt(g/h) * (etaR-etaL)/2;
          
          Feta = -h*unRiemann;
          Fun  = -g*etaRiemann;
          Fut  = 0;
          
        } else {
          
          double HL = etaL+h;
          double HR = etaR+h;
          
          if (HR<0 || HL<0) printf(" HR = %e HL =%e\n", HR,HL);
          
          double u, v, H;
          RoeSolver(uL, uR, vL, vR, HL, HR, u, v, H, g, _upwindFactor);
          double eta = H-h;
          
          Feta = -H*u;
          Fun  = -g*eta -u*u;
          Fut  =        -u*v;
          
        }
        
        val(i,0) = Feta;
        val(i,1) = Fun * nx - Fut * ny;
        val(i,2) = Fun * ny + Fut * nx;
        
        if ((_LayerVersion=="PmlStraightForPureWave") || (_LayerVersion=="PmlStraight")) {
          double aNdotn = absLayerDir(i,0)*nx + absLayerDir(i,1)*ny;
          if (fabs(aNdotn) > 0.01) {
            val(i,3) = Feta;
            //if (_LayerVersion=="PmlStraight")
            //  val(i,4) = aNdotn * Fun;
          }
        }
        
      }
    }
  };
  term _term;
public:
  boundaryCondition(dgConservationLawShallowWaterAbsLayer2d *claw, int typeBC, const function *extDataFunction) :
  _term(claw->getNbFields(),
        typeBC,
        claw->getLayerVersion(),
        claw->getGravityAcceleration(),
        claw->getBathymetry(),
        claw->isLinear(),
        claw->getUpwindFactorRiemann(),
        claw->getAbsLayerDir(),
        extDataFunction) {
    _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawShallowWaterAbsLayer2d::newBoundaryDirichletOnU(const function *uExtFunction) {
  Msg::Warning("Dg/SW/AbsLayer: newBoundaryDirichletOnU(...) has not been checked yet. If it works, remove this comment.");
  return new boundaryCondition(this,1,uExtFunction);
}

dgBoundaryCondition *dgConservationLawShallowWaterAbsLayer2d::newBoundaryFluxU(const function *ndotuExtFunction) {
  Msg::Warning("Dg/SW/AbsLayer: newBoundaryFluxU(...) has not been checked yet. If it works, remove this comment.");
  return new boundaryCondition(this,2,ndotuExtFunction);
}

dgBoundaryCondition *dgConservationLawShallowWaterAbsLayer2d::newBoundaryHomogeneousDirichletOnU() {
  return new boundaryCondition(this,3,NULL);
}

dgBoundaryCondition *dgConservationLawShallowWaterAbsLayer2d::newBoundaryWall() {
  return new boundaryCondition(this,3,NULL);
}

dgBoundaryCondition *dgConservationLawShallowWaterAbsLayer2d::newBoundaryDirichletOnEta(const function *pExtFunction) {
  Msg::Warning("Dg/SW/AbsLayer: newBoundaryDirichletOnEta(...) has not been checked yet. If it works, remove this comment.");
  return new boundaryCondition(this,4,pExtFunction);
}

dgBoundaryCondition *dgConservationLawShallowWaterAbsLayer2d::newBoundaryHomogeneousDirichletOnEta() {
  Msg::Warning("Dg/SW/AbsLayer: newBoundaryHomogeneousDirichletOnEta(...) has not been checked yet. If it works, remove this comment.");
  return new boundaryCondition(this,5,NULL);
}

dgBoundaryCondition *dgConservationLawShallowWaterAbsLayer2d::newBoundaryOpenOutflow() {
  return new boundaryCondition(this,6,NULL);
}

dgBoundaryCondition *dgConservationLawShallowWaterAbsLayer2d::newBoundaryOpenOutflowWithInflow(const function *fieldsExtFunction) {
  return new boundaryCondition(this,7,fieldsExtFunction);
}


/*==============================================================================
 * Specific interface conditions for uncoupled region (BC on both sides)
 *============================================================================*/

class dgConservationLawShallowWaterAbsLayer2d::interfaceTermUncoupledRegions : public function {
  int _nbf, _typeBC;
  bool _isLinear, _isAbsLayerDir;
  double _upwindFactor;
  std::string _LayerVersion;
  fullMatrix<double> normals, gravity, bathymetry;
  fullMatrix<double> solL, absLayerDirL, extDataL;
  fullMatrix<double> solR, absLayerDirR, extDataR;
public:
  interfaceTermUncoupledRegions(int nbf, int typeBC, std::string LayerVersion,
                                const function *gravityF, const function *bathymetryF, bool isLinear,
                                const double upwindFactorRiemann,
                                const function *absLayerDirF,
                                const function *extDataFunction) : function(2*nbf) {
    _nbf = nbf;
    _typeBC = typeBC;
    _isLinear = isLinear;
    _upwindFactor = upwindFactorRiemann;
    _LayerVersion = LayerVersion;
    _isAbsLayerDir = absLayerDirF;
    
    setArgument (solL, function::getSolution(), 0);
    setArgument (solR, function::getSolution(), 1);
    setArgument (normals, function::getNormals());
    setArgument (gravity, gravityF);
    setArgument (bathymetry, bathymetryF);
    if (_isAbsLayerDir) {
      setArgument(absLayerDirL, absLayerDirF, 0);
      setArgument(absLayerDirR, absLayerDirF, 1);
    }
    else
      if ((LayerVersion=="PmlStraightForPureWave") || (LayerVersion=="PmlStraight"))
        Msg::Fatal("Dg/SW/AbsLayer: absLayerDir required if 'LayerVersion' is 'PmlStraightForPureWave' or 'PmlStraight'.");
    if (extDataFunction != NULL) {
      setArgument (extDataL, extDataFunction, 0);
      setArgument (extDataR, extDataFunction, 1);
    }
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double nx = normals(i,0);
      double ny = normals(i,1);
      
      double etaL_L=0., uL_L=0., vL_L=0.;
      double etaR_L=0., uR_L=0., vR_L=0.;
      double etaL_R=0., uL_R=0., vL_R=0.;
      double etaR_R=0., uR_R=0., vR_R=0.;
      
      double tmp;
      
      switch (_typeBC) {
          
          // Continuity of fields
        case 0:
          etaL_L = solL(i,0);
          etaR_L = solR(i,0);
          uL_L =  nx*solL(i,1) + ny*solL(i,2);
          uR_L =  nx*solR(i,1) + ny*solR(i,2);
          vL_L = -ny*solL(i,1) + nx*solL(i,2);
          vR_L = -ny*solR(i,1) + nx*solR(i,2);
          
          etaL_R = solL(i,0);
          etaR_R = solR(i,0);
          uL_R =  nx*solL(i,1) + ny*solL(i,2);
          uR_R =  nx*solR(i,1) + ny*solR(i,2);
          vL_R = -ny*solL(i,1) + nx*solL(i,2);
          vR_R = -ny*solR(i,1) + nx*solR(i,2);
          break;
          
          // Dirichlet BC on u with external fields  (extData contains 'uExt')
        case 1:
          etaL_L = solL(i,0);
          etaR_L = solL(i,0);
          uL_L =  nx*solL(i,1) + ny*solL(i,2);
          uR_L =  nx*extDataL(i,0) + ny*extDataL(i,1);
          vL_L = -ny*solL(i,1) + nx*solL(i,2);
          vR_L = -ny*solL(i,1) + nx*solL(i,2);
          
          etaL_R = solR(i,0);
          etaR_R = solR(i,0);
          uL_R =  nx*extDataR(i,0) + ny*extDataR(i,1);
          uR_R =  nx*solR(i,1) + ny*solR(i,2);
          vL_R = -ny*solR(i,1) + nx*solR(i,2);
          vR_R = -ny*solR(i,1) + nx*solR(i,2);
          break;
          
          // Dirichlet BC on u with flux  (extData contains 'ndotuExt')
        case 2:
          etaL_L = solL(i,0);
          etaR_L = solL(i,0);
          uL_L =  nx*solL(i,1) + ny*solL(i,2);
          uR_L =  extDataL(i,0);
          vL_L = -ny*solL(i,1) + nx*solL(i,2);
          vR_L = -ny*solL(i,1) + nx*solL(i,2);
          
          etaL_R = solR(i,0);
          etaR_R = solR(i,0);
          uL_R =  extDataR(i,0);
          uR_R =  nx*solR(i,1) + ny*solR(i,2);
          vL_R = -ny*solR(i,1) + nx*solR(i,2);
          vR_R = -ny*solR(i,1) + nx*solR(i,2);
          break;
          
          // Homogeneous Dirichlet BC on u  (extData is EMPTY)
        case 3:
          etaL_L = solL(i,0);
          etaR_L = solL(i,0);
          uL_L =  nx*solL(i,1) + ny*solL(i,2);
          uR_L =  0;
          vL_L = -ny*solL(i,1) + nx*solL(i,2);
          vR_L = -ny*solL(i,1) + nx*solL(i,2);
          
          etaL_R = solR(i,0);
          etaR_R = solR(i,0);
          uL_R =  0;
          uR_R =  nx*solR(i,1) + ny*solR(i,2);
          vL_R = -ny*solR(i,1) + nx*solR(i,2);
          vR_R = -ny*solR(i,1) + nx*solR(i,2);
          break;
          
          // Dirichlet BC on p with external fields  (extData contains 'pExt')
        case 4:
          etaL_L = solL(i,0);
          etaR_L = extDataL(i,0);
          uL_L =  nx*solL(i,1) + ny*solL(i,2);
          uR_L =  nx*solL(i,1) + ny*solL(i,2);
          vL_L = -ny*solL(i,1) + nx*solL(i,2);
          vR_L = -ny*solL(i,1) + nx*solL(i,2);
          
          etaL_R = extDataR(i,0);
          etaR_R = solR(i,0);
          uL_R =  nx*solR(i,1) + ny*solR(i,2);
          uR_R =  nx*solR(i,1) + ny*solR(i,2);
          vL_R = -ny*solR(i,1) + nx*solR(i,2);
          vR_R = -ny*solR(i,1) + nx*solR(i,2);
          break;
          
          // Homogeneous Dirichlet BC on p  (extData is EMPTY)
        case 5:
          etaL_L = solL(i,0);
          etaR_L = 0;
          uL_L =  nx*solL(i,1) + ny*solL(i,2);
          uR_L =  nx*solL(i,1) + ny*solL(i,2);
          vL_L = -ny*solL(i,1) + nx*solL(i,2);
          vR_L = -ny*solL(i,1) + nx*solL(i,2);
          
          etaL_R = 0;
          etaR_R = solR(i,0);
          uL_R =  nx*solR(i,1) + ny*solR(i,2);
          uR_R =  nx*solR(i,1) + ny*solR(i,2);
          vL_R = -ny*solR(i,1) + nx*solR(i,2);
          vR_R = -ny*solR(i,1) + nx*solR(i,2);
          break;
          
          
          // Open BC (extData is EMPTY)
        case 6:
          etaL_L = solL(i,0);
          etaR_L = 0;
          uL_L =  nx*solL(i,1) + ny*solL(i,2);
          uR_L =  0;
          vL_L = -ny*solL(i,1) + nx*solL(i,2);
          vR_L =  0;
          
          etaL_R = 0;
          etaR_R = solR(i,0);
          uL_R =  0;
          uR_R =  nx*solR(i,1) + ny*solR(i,2);
          vL_R =  0;
          vR_R = -ny*solR(i,1) + nx*solR(i,2);
          break;
          
          // Open BC with inflow (extData contains all external fields) - Flather
        case 7:
          etaL_L = solL(i,0);
          etaR_L = extDataL(i,0);
          uL_L =  nx*solL(i,1)     + ny*solL(i,2);
          uR_L =  nx*extDataL(i,1) + ny*extDataL(i,2);
          vL_L = -ny*solL(i,1)     + nx*solL(i,2);
          vR_L = -ny*extDataL(i,1) + nx*extDataL(i,2);
          
          etaL_R = extDataR(i,0);
          etaR_R = solR(i,0);
          uL_R =  nx*extDataR(i,1) + ny*extDataR(i,2);
          uR_R =  nx*solR(i,1)     + ny*solR(i,2);
          vL_R = -ny*extDataR(i,1) + nx*extDataR(i,2);
          vR_R = -ny*solR(i,1)     + nx*solR(i,2);
          break;
          
          // Scattered/Total fields transition
          // (extData contains all incident fields - absLayerDir indicates the scattered fields side)
        case 8:
          
          tmp = absLayerDirL(i,0)*nx + absLayerDirL(i,1)*ny;
          if (fabs(tmp)<0.5)
            tmp = absLayerDirR(i,0)*nx + absLayerDirR(i,1)*ny;
          if (fabs(tmp)<0.5)
            Msg::Error("Dg/SW/AbsLayer: bad 'absLayerDir' for the INTERFACE CONDITION '8'.");
          
          if (tmp>0.5) {  // (L=total, R=scattered)
            
            etaL_L = solL(i,0);
            etaR_L = solR(i,0)+extDataR(i,0);
            uL_L =  nx*solL(i,1) + ny*solL(i,2);
            uR_L =  nx*(solR(i,1)+extDataR(i,1)) + ny*(solR(i,2)+extDataR(i,2));
            vL_L = -ny*solL(i,1) + nx*solL(i,2);
            vR_L = -ny*(solR(i,1)+extDataR(i,1)) + nx*(solR(i,2)+extDataR(i,2));
            
            etaL_R = solL(i,0)-extDataL(i,0);
            etaR_R = solR(i,0);
            uL_R =  nx*(solL(i,1)-extDataL(i,1)) + ny*(solL(i,2)-extDataL(i,2));
            uR_R =  nx*solR(i,1) + ny*solR(i,2);
            vL_R = -ny*(solL(i,1)-extDataL(i,1)) + nx*(solL(i,2)-extDataL(i,2));
            vR_R = -ny*solR(i,1) + nx*solR(i,2);
            break;
            
          }
          else {  // (L=scattered, R=total)
            
            etaL_L = solL(i,0);
            etaR_L = solR(i,0)-extDataR(i,0);
            uL_L =  nx*solL(i,1) + ny*solL(i,2);
            uR_L =  nx*(solR(i,1)-extDataR(i,1)) + ny*(solR(i,2)-extDataR(i,2));
            vL_L = -ny*solL(i,1) + nx*solL(i,2);
            vR_L = -ny*(solR(i,1)-extDataR(i,1)) + nx*(solR(i,2)-extDataR(i,2));
            
            etaL_R = solL(i,0)+extDataL(i,0);
            etaR_R = solR(i,0);
            uL_R =  nx*(solL(i,1)+extDataL(i,1)) + ny*(solL(i,2)+extDataL(i,2));
            uR_R =  nx*solR(i,1) + ny*solR(i,2);
            vL_R = -ny*(solL(i,1)+extDataL(i,1)) + nx*(solL(i,2)+extDataL(i,2));
            vR_R = -ny*solR(i,1) + nx*solR(i,2);
            break;
            
          }
          break;
          
        default:
          Msg::Fatal("Dg/SW/AbsLayer: There is an error in the INTERFACE CONDITION for UNCOUPLED REGIONS (BAD '_typeBC': '%i')", _typeBC);
          
      }
      
      double g = gravity(i,0);
      double h = bathymetry(i,0);
      double Feta_L, Fun_L, Fut_L;
      double Feta_R, Fun_R, Fut_R;
      
      if (_isLinear) {
        
        Feta_R = -h*(uL_R+uR_R)/2     - sqrt(g*h) * (etaL_R-etaR_R)/2;
        Fun_R  = -g*(etaL_R+etaR_R)/2 - sqrt(g*h) * (uL_R-uR_R)/2;
        Fut_R  = 0;
        
        Feta_L = -h*(uL_L+uR_L)/2     - sqrt(g*h) * (etaL_L-etaR_L)/2;
        Fun_L  = -g*(etaL_L+etaR_L)/2 - sqrt(g*h) * (uL_L-uR_L)/2;
        Fut_L  = 0;
        
      } else {
        
        double HL_L = etaL_L+h;
        double HR_L = etaR_L+h;
        
        double HL_R = etaL_R+h;
        double HR_R = etaR_R+h;
        
        double u_L, v_L, H_L;
        double u_R, v_R, H_R;
        RoeSolver(uL_L, uR_L, vL_L, vR_L, HL_L, HR_L, u_L, v_L, H_L, g, _upwindFactor);
        RoeSolver(uL_R, uR_R, vL_R, vR_R, HL_R, HR_R, u_R, v_R, H_R, g, _upwindFactor);
        
        Feta_L = -H_L*u_L;
        Fun_L  = -g*(H_L-h) -u_L*u_L;
        Fut_L  =            -u_L*v_L;
        
        Feta_R = -H_R*u_R;
        Fun_R  = -g*(H_R-h) -u_R*u_R;
        Fut_R  =            -u_R*v_R;
      }
      
      val(i,0) = Feta_L;
      val(i,1) = (Fun_L * nx - Fut_L * ny);
      val(i,2) = (Fun_L * ny + Fut_L * nx);
      
      val(i,_nbf+0) = -Feta_R;
      val(i,_nbf+1) = -(Fun_R * nx - Fut_R * ny);
      val(i,_nbf+2) = -(Fun_R * ny + Fut_R * nx);
      
      if ((_LayerVersion=="PmlStraightForPureWave") || (_LayerVersion=="PmlStraight")) {
        double aLdotn = absLayerDirL(i,0)*nx + absLayerDirL(i,1)*ny;
        double aRdotn = absLayerDirR(i,0)*nx + absLayerDirR(i,1)*ny;
        if (fabs(aLdotn) > 0.01) {
          val(i,3) = Feta_L;
          //if (_LayerVersion=="PmlStraight")
          //  val(i,4) = aLdotn * Fun_L;
        }
        if (fabs(aRdotn) > 0.01) {
          val(i,_nbf+3) = -Feta_R;
          //if (_LayerVersion=="PmlStraight")
          //  val(i,_nbf+4) = -aRdotn * Fun_R;
        }
      }
      
    }
  }
};

void dgConservationLawShallowWaterAbsLayer2d::addInterfaceContinuity(const std::string tag) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  _interfaceTerm0[tag] = new interfaceTermUncoupledRegions(getNbFields(),0,getLayerVersion(),getGravityAcceleration(),getBathymetry(),isLinear(),getUpwindFactorRiemann(),getAbsLayerDir(),NULL);
}

void dgConservationLawShallowWaterAbsLayer2d::addInterfaceDirichletOnU(const std::string tag, const function *uExtFunction) {
  Msg::Warning("Dg/SW/AbsLayer: addInterfaceDirichletOnU(...) has not been checked yet. If it works, remove this comment.");
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  _interfaceTerm0[tag] = new interfaceTermUncoupledRegions(getNbFields(),1,getLayerVersion(),getGravityAcceleration(),getBathymetry(),isLinear(),getUpwindFactorRiemann(),getAbsLayerDir(),uExtFunction);
}

void dgConservationLawShallowWaterAbsLayer2d::addInterfaceFluxU(const std::string tag, const function *ndotuExtFunction) {
  Msg::Warning("Dg/SW/AbsLayer: addInterfaceFluxU(...) has not been checked yet. If it works, remove this comment.");
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  _interfaceTerm0[tag] = new interfaceTermUncoupledRegions(getNbFields(),2,getLayerVersion(),getGravityAcceleration(),getBathymetry(),isLinear(),getUpwindFactorRiemann(),getAbsLayerDir(),ndotuExtFunction);
}

void dgConservationLawShallowWaterAbsLayer2d::addInterfaceHomogeneousDirichletOnU(const std::string tag) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  _interfaceTerm0[tag] = new interfaceTermUncoupledRegions(getNbFields(),3,getLayerVersion(),getGravityAcceleration(),getBathymetry(),isLinear(),getUpwindFactorRiemann(),getAbsLayerDir(),NULL);
}

void dgConservationLawShallowWaterAbsLayer2d::addInterfaceWall(const std::string tag) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  _interfaceTerm0[tag] = new interfaceTermUncoupledRegions(getNbFields(),3,getLayerVersion(),getGravityAcceleration(),getBathymetry(),isLinear(),getUpwindFactorRiemann(),getAbsLayerDir(),NULL);
}

void dgConservationLawShallowWaterAbsLayer2d::addInterfaceDirichletOnEta(const std::string tag, const function *pExtFunction) {
  Msg::Warning("Dg/SW/AbsLayer: addInterfaceDirichletOnEta(...) has not been checked yet. If it works, remove this comment.");
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  _interfaceTerm0[tag] = new interfaceTermUncoupledRegions(getNbFields(),4,getLayerVersion(),getGravityAcceleration(),getBathymetry(),isLinear(),getUpwindFactorRiemann(),getAbsLayerDir(),pExtFunction);
}

void dgConservationLawShallowWaterAbsLayer2d::addInterfaceHomogeneousDirichletOnEta(const std::string tag) {
  Msg::Warning("Dg/SW/AbsLayer: addInterfaceHomogeneousDirichletOnEta(...) has not been checked yet. If it works, remove this comment.");
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  _interfaceTerm0[tag] = new interfaceTermUncoupledRegions(getNbFields(),5,getLayerVersion(),getGravityAcceleration(),getBathymetry(),isLinear(),getUpwindFactorRiemann(),getAbsLayerDir(),NULL);
}

void dgConservationLawShallowWaterAbsLayer2d::addInterfaceOpenOutflow(const std::string tag) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  _interfaceTerm0[tag] = new interfaceTermUncoupledRegions(getNbFields(),6,getLayerVersion(),getGravityAcceleration(),getBathymetry(),isLinear(),getUpwindFactorRiemann(),getAbsLayerDir(),NULL);
}

void dgConservationLawShallowWaterAbsLayer2d::addInterfaceOpenOutflowWithInflow(const std::string tag, const function *fieldsExtFunction) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  _interfaceTerm0[tag] = new interfaceTermUncoupledRegions(getNbFields(),7,getLayerVersion(),getGravityAcceleration(),getBathymetry(),isLinear(),getUpwindFactorRiemann(),getAbsLayerDir(),fieldsExtFunction);
}

void dgConservationLawShallowWaterAbsLayer2d::addInterfaceScatteredTotalFields(const std::string tag, const function *incFieldsF, const function *scattDirF) {
  const function *scattDir=NULL;
  if (scattDirF)
    scattDir = scattDirF;
  else if (getAbsLayerDir())
    scattDir = getAbsLayerDir();
  else
    Msg::Fatal("Dg/SW/AbsLayer: 'scattDir' is missing for 'addInterfaceScatteredTotalFields'.");
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  _interfaceTerm0[tag] = new interfaceTermUncoupledRegions(getNbFields(),8,getLayerVersion(),getGravityAcceleration(),getBathymetry(),isLinear(),getUpwindFactorRiemann(),scattDir,incFieldsF);
}


/*==============================================================================
 * Compute information
 *============================================================================*/

class dgConservationLawShallowWaterAbsLayer2d::energyFunction : public function {
  fullMatrix<double> sol, gravity, bathymetry;
  bool _isLinear;
public:
  energyFunction(bool isLinear,
                 function *solutionFunction,
                 const function *gravityF,
                 const function *bathymetryF) : function(1) {
    _isLinear = isLinear;
    setArgument (sol, solutionFunction);
    setArgument (gravity, gravityF);
    setArgument (bathymetry, bathymetryF);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      double g = gravity(i,0);
      double H = bathymetry(i,0);
      if (!_isLinear)
        H += sol(i,0);
      
      double normeta = pow(sol(i,0),2);
      double normu = pow(sol(i,1),2) + pow(sol(i,2),2);
      
      val(i,0) = 0.5 * (g * normeta + H * normu);
    }
  }
};

class dgConservationLawShallowWaterAbsLayer2d::energyErrorFunction : public function {
  fullMatrix<double> sol, solRef, gravity, bathymetry;
  bool _isLinear;
public:
  energyErrorFunction(bool isLinear,
                      function *solutionFunction,
                      function *solutionRefFunction,
                      const function *gravityF,
                      const function *bathymetryF) : function(1) {
    _isLinear = isLinear;
    setArgument (sol, solutionFunction);
    setArgument (solRef, solutionRefFunction);
    setArgument (gravity, gravityF);
    setArgument (bathymetry, bathymetryF);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      double g = gravity(i,0);
      double H = bathymetry(i,0);
      if (!_isLinear)
        H += solRef(i,0);
      
      double normeta = pow(sol(i,0)-solRef(i,0),2);
      double normu = pow(sol(i,1)-solRef(i,1),2) + pow(sol(i,2)-solRef(i,2),2);
      
      val(i,0) = 0.5 * (g * normeta + H * normu);
    }
  }
};

double dgConservationLawShallowWaterAbsLayer2d::getTotalEnergy(dgGroupCollection *groups,
                                                               function *solutionFunc,
                                                               std::string tag,
                                                               int intOrder) {
  function *_energyFunction = new energyFunction(isLinear(),solutionFunc,getGravityAcceleration(),getBathymetry());
  fullMatrix<double> value(1,1);
  dgFunctionIntegrator _totalEnergy(groups, _energyFunction);
  _totalEnergy.compute(value, tag, intOrder);
  delete _energyFunction;
  return value.get(0,0);
}

double dgConservationLawShallowWaterAbsLayer2d::getTotalEnergy(dgGroupCollection *groups,
                                                               dgDofContainer *solutionDof,
                                                               std::string tag,
                                                               int intOrder) {
  function *_energyFunction = new energyFunction(isLinear(),solutionDof->getFunction(),getGravityAcceleration(),getBathymetry());
  fullMatrix<double> value(1,1);
  dgFunctionIntegrator _totalEnergy(groups, _energyFunction);
  _totalEnergy.compute(value, tag, intOrder);
  delete _energyFunction;
  return value.get(0,0);
}

double dgConservationLawShallowWaterAbsLayer2d::getTotalEnergyError(dgGroupCollection *groups,
                                                                    dgDofContainer *solutionDof,
                                                                    function *solutionRefFunc,
                                                                    std::string tag,
                                                                    int intOrder) {
  function *_energyErrorFunction = new energyErrorFunction(isLinear(),solutionDof->getFunction(),solutionRefFunc,getGravityAcceleration(),getBathymetry());
  fullMatrix<double> value(1,1);
  dgFunctionIntegrator _totalEnergyError(groups, _energyErrorFunction);
  _totalEnergyError.compute(value, tag, intOrder);
  delete _energyErrorFunction;
  return value.get(0,0);
}

double dgConservationLawShallowWaterAbsLayer2d::getTotalEnergyError(dgGroupCollection *groups,
                                                                    dgDofContainer *solutionDof,
                                                                    dgDofContainer *solutionRefDof,
                                                                    std::string tag,
                                                                    int intOrder) {
  function *_energyErrorFunction = new energyErrorFunction(isLinear(),solutionDof->getFunction(),solutionRefDof->getFunction(),getGravityAcceleration(),getBathymetry());
  fullMatrix<double> value(1,1);
  dgFunctionIntegrator _totalEnergyError(groups, _energyErrorFunction);
  _totalEnergyError.compute(value, tag, intOrder);
  delete _energyErrorFunction;
  return value.get(0,0);
}

double dgConservationLawShallowWaterAbsLayer2d::getVolume(dgGroupCollection *groups, std::string tag) {
  function *_unitFunction = new functionConstant(1.);
  fullMatrix<double> value(1,1);
  dgFunctionIntegrator _totalVolume(groups, _unitFunction);
  _totalVolume.compute(value, tag);
  delete _unitFunction;
  return value.get(0,0);
}

