#ifndef _SLIM_MOVING_BATH_WETTING_DRYING_H_
#define _SLIM_MOVING_BATH_WETTING_DRYING_H_
#include "function.h"

class movingBathFactor : public function {
  fullMatrix<double> sol, bath;
  public:
  double alpha;
  movingBathFactor(const function *originalBathF, double alphaF);
  void call (dataCacheMap *m, fullMatrix<double> &val);
};

class movingBathFactorGradient : public function {
  fullMatrix<double> sol, bath, solGradient, bathGradient;
  double alpha;
  public:
  movingBathFactorGradient(const function *originalBathF,  const function *originalBathGradientF, double alphaF);
  void call (dataCacheMap *m, fullMatrix<double> &val);
};

class movingBath : public function {
  fullMatrix<double> bath, sol;
  public:
  double alpha;
  movingBath(const function *originalBathF, double alphaF);
  void call (dataCacheMap *m, fullMatrix<double> &val);
};

class movingBathGradient : public function {
  fullMatrix<double> bath, bathGradient, sol, solGradient;
  public:
  double alpha;
  movingBathGradient(const function *originalBathF, const function *originalBathGradientF, double alphaF);
  void call (dataCacheMap *m, fullMatrix<double> &val);
};
#endif
