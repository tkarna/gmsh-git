#!/usr/bin/env rundgpy

#We import gmsg dg module to interact with the c++ library 
#c++ functions from gmsh can now be called in the python code
from dgpy import *
#We import common python packages
import os, math
import  Common, gmshPartition

#A GModel is a mesh database
model = GModel  ()
#The mesh "stommel_square.msh" is generated using the geometry file "stommel_square.geo"
#The generated mesh is 2d (second argument) and of order 1 (third argument)
Common.genMesh('stommel_square', 2, 1)
#The mesh is partitionned for the number of processes
model.load(gmshPartition.simple('stommel_square.msh'))
#Polynomial order of the discretization
order = 3
#Groups are used to differentiate different elements of the mesh
#Surface elements and edges belong to different groups
#Group can be split into sub-groups (i.e. split groups of elements that belong to searate physical entities) 
groups = dgGroupCollection(model, 2, order)
#Create directory for output
try : os.mkdir("output")
except: pass

#For functions that need to be fast (often called), c++ code can be included in the python files and compiled during runtime
#This is an example of c++ functions used to define the forcings (wind), coriolis and the analytical solution
#Not that the analytical solution could be written in python as it is called only once
CCode = """
#include "fullMatrix.h"
#include "function.h"
extern "C" {
  void wind (dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
    for (size_t i = 0; i< sol.size1(); i++) {
      sol.set(i,0,0.1* sin(M_PI*xyz(i,1)/1e6)/1e6);
      sol.set(i,1,0);
    }
  }
  void coriolis (dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
    for (size_t i = 0; i< sol.size1(); i++) {
      sol.set(i,0,1e-4+2e-11*(xyz(i,1)));
    }
  }
  void UVEtaAnalytic (dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &xyz) {
    double L = 1e6;
    double tau0 = 0.1;
    double rho = 1000;
    double gamm = 1e-6;
    double delta = 1;
    double g = 9.80616;
    double h = 1000;
    double f0 = 1e-4;
    double beta = 2e-11;
    double epsilon = gamm / (L * beta);
    double z1 = (-1 + sqrt(1 + (2 * M_PI * delta * epsilon) * (2 * M_PI * delta * epsilon))) / (2 * epsilon);
    double z2 = (-1 - sqrt(1 + (2 * M_PI * delta * epsilon) * (2 * M_PI * delta * epsilon))) / (2 * epsilon);
    double D = ((exp(z2) - 1) * z1 + (1 - exp(z1)) * z2) / (exp(z1) - exp(z2));
    for (size_t i = 0; i< val.size1(); i++) {
      double x = xyz(i,0) / L + 0.5;
      double y = xyz(i,1) / L;
      double f1 = M_PI / D * (1 + ((exp(z2) - 1) * exp(x * z1) + (1 - exp(z1)) * exp(x * z2)) / (exp(z1) - exp(z2)));
      double f2 = 1 / D * (((exp(z2) - 1) * z1 * exp(x * z1) + (1 - exp(z1)) * z2 * exp(x * z2)) / (exp(z1) - exp(z2)));
      //eta
      val(i, 0) = D * tau0 * f0 * L / (M_PI * gamm * rho * delta * g * h) * (-gamm / (f0 * delta * M_PI) * f2 * sin(M_PI * y) +
         1 / M_PI * f1 * (cos(M_PI * y) * (1 + beta * y) - beta / M_PI * sin(M_PI * y)));
      //u
      val(i, 1) = D * tau0 / (M_PI * gamm * rho * h) * f1 * sin(M_PI * y);
      //v
      val(i, 2) = D * tau0 / (M_PI * gamm * rho * delta * h) * f2 * cos(M_PI * y);
      //w
      val(i, 3) = 0;
    }
  }
}
"""

#The c++ code is compiled
CCodeLib = "./CCode.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CCode, CCodeLib);
Msg.Barrier()

#Here we define the conservation law to use. Thera are several conservation laws
#available (see the files beginning with dgConservationLaw)
#Two-dimensional shallow water are considered here
claw = dgConservationLawShallowWater2d()
#The container that contains the degrees of freedom
#For the shallow water equations, claw.getNbFields() is 3 (u, v and eta)
solution = dgDofContainer(groups, claw.getNbFields())
XYZ = groups.getFunctionCoordinates();
#In this example, we consider the linear shallow water equation
#(availability of the analytical solution)
claw.setIsLinear(True)
#We use the functions defined in c++ above and pass it to the 
#conservation law 
coriolis = functionC(CCodeLib,"coriolis",1,[XYZ])
claw.setCoriolisFactor(coriolis)
wind = functionC(CCodeLib,"wind",2,[XYZ])
claw.setSource(wind)
#Various parameters: gamma is the linear dissipation coefficient and bath is the water depth at rest
gamma = functionConstant([1e-6])
claw.setLinearDissipation(gamma)
bath = functionConstant([1000])
claw.setBathymetry(bath)
#We enforce the boundary conditions
claw.addBoundaryCondition('Wall',claw.newBoundaryWall())
#The linear system and its solver
#Here we choose a block structured system implemented in petsc
#We pass additional options to petsc (lu in serial, ilu in parallel)
sys = linearSystemPETScBlockDouble ()
if(Msg.GetCommSize() > 1):
  sys.setParameter("petscOptions", "-pc_type bjacobi -sub_pc_type lu")
else:
  sys.setParameter("petscOptions", "-pc_type lu")
dof = dgDofManager.newDGBlock (groups, 3, sys)
#We can choose severa time discretization. Here an implicit euler,
#as we as interested in a stationary solution
implicitEuler = dgDIRK(claw, dof, 1)
implicitEuler.getNewton().setAtol(1e-6)
implicitEuler.getNewton().setRtol(1e-6)
implicitEuler.getNewton().setVerb(10)

#The time integration loop is written in the python file
#Here, 60 iterations are considered
dt = 2e5
t = 0.0
solution.exportMsh("output/stommel-%06d" % (0), 0, 0) #export initial solution
for i in range (60) :
  t = t+dt
  print('|ITER|',i,'|DT|',dt, '|t|', t)
  implicitEuler.iterate(solution, dt, t)
  solution.exportMsh("output/stommel-%06d" % (i+1), t, i+1) #export solution after each time step

#We use a function to define the error versus the analytical solution (the latest being written above in c++)
#Then, we integrate this error over the domain
#Elevation eta is defined up to a constant. Hence, we do not compute error for eta.
def errorAnalytical(val, sol, analytic):
  for i in range (sol.size1()):
    val.set(i, 0, (sol(i, 0) - analytic(i, 0))**2);
    val.set(i, 1, (sol(i, 1) - analytic(i, 1))**2);
    val.set(i, 2, (sol(i, 2) - analytic(i, 2))**2);
UVEtaAnalytic = functionC(CCodeLib,"UVEtaAnalytic",4,[XYZ])
errorAnalyticalF = functionPython(3,errorAnalytical,[solution.getFunction(), UVEtaAnalytic])
integrator = dgFunctionIntegrator(groups, errorAnalyticalF)
integral = fullMatrixDouble(3,1)
integrator.compute(integral)
errorU = [math.sqrt(integral(1,0))/1e12, math.sqrt(integral(2,0))/1e12]
print ("L2 error on (u, v): (%e, %e)\n" % (errorU[0], errorU[1]))
#Abnormal termination if the error is too high 
if (errorU[0] + errorU[1] > 4e-10):
  Msg.Error("Error is too high");
  Msg.Exit(1)
Msg.Exit(0)
