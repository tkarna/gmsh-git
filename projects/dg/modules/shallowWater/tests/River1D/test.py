from dgpy import *
from gmshpy import *
from Common import *
import time, math, os, os.path, sys
geo=".geo"
msh=".msh"
order=1
dimension=1
#clscale=1

CCode = """
#include "fullMatrix.h"
#include "function.h"
extern "C" {
  double pow2(double value){
  	return value*value;
  }
	void gaussian(dataCacheMap *,fullMatrix<double> &f,fullMatrix<double> &xyz){
    for (size_t i=0; i<xyz.size1(); i++){
      f.set(i,0,0.1*exp(-0.5*pow2(xyz(i,0)-0.5e5)/pow2(0.5e4))); 
    }
	}
	void chezyManningDissipation(dataCacheMap *,fullMatrix<double> &C,fullMatrix<double> &section, fullMatrix<double> &width){
    for (size_t i=0; i<C.size1(); i++){
      double Heff=section(i,0)/width(i,0);
      double manning=0.01;
      double chezy=pow(section(i,0)/(width(i,0)+2*Heff),1./6.)/manning;
      double g=9.81;
      C.set(i,0,g/(pow2(chezy)*Heff)); 
    }
	}
	void merge2scalars(dataCacheMap *,fullMatrix<double> &uv,fullMatrix<double> &u, fullMatrix<double> &v){
    for (size_t i=0; i<uv.size1(); i++){
      uv.set(i,0,u(i,0)); 
      uv.set(i,1,v(i,0)); 
    }
	}
	void widthData(dataCacheMap *m,fullMatrix<double> &w,fullMatrix<double> &bath, fullMatrix<double> &eta, fullMatrix<double> &x){
    for (size_t i=0; i<bath.size1(); i++){
      //w.set(i,0,100.); 
      w.set(i,0,(100+(100-50)/bath(i,0)*eta(i,0))); 
    }
	}
	void sectionData(dataCacheMap *m,fullMatrix<double> &s,fullMatrix<double> &bath, fullMatrix<double> &eta){
    for (size_t i=0; i<s.size1(); i++){
      //s.set(i,0, 100*(eta(i,0)+bath(i,0))); 
      s.set(i,0,(100*(eta(i,0)+bath(i,0))+(100-50)/bath(i,0)*(pow2(eta(i,0))-pow2(bath(i,0)))/2)  ); 
    }
	}
	void square(dataCacheMap *m,fullMatrix<double> &ww,fullMatrix<double> &w){
    for (size_t i=0; i<w.size1(); i++){
      ww.set(i,0,w(i,0)*w(i,0)); 
    }
	}
}
"""

tmpLib = "./tmp.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CCode, tmpLib);
Msg.Barrier()

model = GModel()
meshName="river"
genMesh(meshName, dimension, order)
#p = os.system("gmsh -1 %s.geo -%d -order %d -clscale %f" % (meshName, dimension, order, clscale))
model.load (meshName+geo)
model.load (meshName+msh)

curv1D = dgSpaceTransform1DCurvilinear()
groups = dgGroupCollection(model, dimension, order, curv1D)
groups.splitGroupsByPhysicalTag()
XYZ=groups.getFunctionCoordinates()

hydroLaw = dgConservationLawShallowWater1d()
hydroLaw.setElevationInterp()
etaInterp=hydroLaw.getElevationInterp()

#-------------------------
# Geometry
#-------------------------
bath=functionConstant([10.])
widthData = functionC(tmpLib,"widthData",1,[bath,etaInterp])
sectionData = functionC(tmpLib,"sectionData",1,[bath,etaInterp])
hydroLaw.setWidthData(widthData)
hydroLaw.setSectionData(sectionData)

#-------------------------
# Initial conditions
#-------------------------
etaGaussian = functionC(tmpLib,"gaussian",1,[XYZ])
zeroF=functionConstant([0.0])
initialSection = functionC(tmpLib,"sectionData",1,[bath,etaGaussian])
initialSol = functionC(tmpLib,"merge2scalars",2,[initialSection,zeroF])

#-------------------------
# Hydro solution
#-------------------------
hydroSol = dgDofContainer(groups, hydroLaw.getNbFields())
hydroSol.interpolate(initialSol)
hydroLaw.setHydroSolution(hydroSol.getFunction())
hydroLaw.setElevation()
hydroLaw.setWidth()

#-------------------------
# get Fields
#-------------------------
etaF = hydroLaw.getElevation()
width= hydroLaw.getWidth()
section = hydroLaw.getSection()
velocity = hydroLaw.getVelocity()

#-------------------------
# Supplementary terms1
#-------------------------
quadDiss=functionC(tmpLib,"chezyManningDissipation",1,[section,width])
hydroLaw.setQuadraticDissipation(quadDiss)
diffusivity=functionConstant(1.)
hydroLaw.setDiffusivity(diffusivity)
hydroLaw.setSetup()

#-------------------------
# Boundary conditions
#-------------------------
hydroLaw.addBoundaryCondition("Downstream",hydroLaw.newBoundaryWall())
hydroLaw.addBoundaryCondition("Upstream",hydroLaw.newBoundaryWall())

#-------------------------
# Time stepping
#-------------------------

integratorS=dgFunctionIntegrator(groups,section)
ISv = fullMatrixDouble(1,1)
Sv=[]
usquare = functionC(tmpLib,"square",1,[velocity])
integratoruu=dgFunctionIntegrator(groups,usquare)
Iuuv = fullMatrixDouble(1,1)
uuv=[]
integratorS.compute(ISv)
integratoruu.compute(Iuuv)
Msg.Info("%d int(S)dx=%.4e int(u*u)dx=%.4e" % (0,ISv(0,0),Iuuv(0,0)))

distance=functionExtractCompNew(XYZ,0)

#-------------------------
print ("Solve")
#-------------------------
rkHydro=dgERK(hydroLaw,None,DG_ERK_44)
t=0.0
nbExport = 0
for i in range (0,1000) :
  dt = 1
  normHydro = rkHydro.iterate(hydroSol,dt,t)
  t = t+dt
  if (i % 100 == 0) :
    integratorS.compute(ISv)
    integratoruu.compute(Iuuv)
    Msg.Info("%d %03d int(S)dx=%.4e int(u*u)dx=%.4e" % (nbExport,i,ISv(0,0),Iuuv(0,0)))
    #hydroSol.exportFunctionMsh(section,"tidalRiver/section-%06d" %i, t, nbExport)  
    #exportFunctionTxt(model,groups,hydroSol,section,distance,"tidalRiver/section",nbExport)
    Sv.append(ISv(0,0))
    uuv.append(Iuuv(0,0))
    nbExport  = nbExport  + 1


Msg.Info("%d %03d int(S)dx=%.4e int(u*u)dx=%.4e" % (nbExport,i,ISv(0,0),Iuuv(0,0)))
if (abs(ISv(0,0)/7.5126e+07 - 1) < 1.e-3 and abs(Iuuv(0,0)/5.1291e+01 - 1) < 1.e-3):
  print ("Exit with success :-)")
  sys.exit(0)
else:
  print ("Exit with failure :-(")
  sys.exit(1)

