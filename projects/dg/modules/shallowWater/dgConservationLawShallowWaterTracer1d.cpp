#include "dgConservationLawShallowWaterTracer1d.h"
#include "dgConservationLawShallowWater1d.h"
#include "dgNeighbourMap.h"
#include "function.h"
#include "Numeric.h" 
#include "assert.h" 
#include "stdlib.h"
#include <string.h>
#include "MEdge.h"
#include "MVertex.h"
#include <algorithm>
#include "Numeric.h"
#include "MElement.h"

static double g = 9.81;

dgConservationLawShallowWaterTracer1d::dgConservationLawShallowWaterTracer1d() : dgConservationLawFunction(1)
{
  function *fzero = new functionConstant(0.);
  _source = fzero;
  _hydroSolution=NULL;
  _elevation = fzero;
  _width = NULL;
  _tracer = NULL;
  _typeRiemann = "LAX";
  _zeroFluxBifurcations=false;
  _diffusivity = fzero;
  _x1d = fzero;
  _sigmaFactBif = -1;
}

//*************************************
// Terms of the equation
//***************************************
class dgConservationLawShallowWaterTracer1d::massFactor: public function {
  fullMatrix<double> hydroSol;
public:
  massFactor(const function *hydroSolF) : function(1) {
    setArgument (hydroSol, hydroSolF);
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    for(int i=0; i< val.size1(); i++) {
      val(i,0) = hydroSol(i,0);
    }
  }
};

class dgConservationLawShallowWaterTracer1d::advection: public function {
  fullMatrix<double> hydroSol,tracer,tracerGradient, diffusivity;
public:
  advection(const function *hydroSolF, const function *tracerF, const function *diffusivityF) : function(3) {
    setArgument (hydroSol, hydroSolF);
    setArgument (tracer, tracerF);
    setArgument (tracerGradient, function::getSolutionGradient());
    setArgument (diffusivity, diffusivityF);
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    for(int i=0; i< val.size1(); i++) {
      double section=hydroSol(i,0);
      double velocity=hydroSol(i,1);
      val(i,0) = section*velocity*tracer(i,0) - diffusivity(i,0) * section * tracerGradient(0,0) ;
      val(i,1) = 0.;
      val(i,2) = 0.;
    }
  }
};

class dgConservationLawShallowWaterTracer1d::source: public function {
  fullMatrix<double>  source1,hydroSol;
public :
  source(const function *sourceF, const function *hydroSolF) : function(1)
  {
    setArgument (hydroSol, hydroSolF);
    setArgument (source1, sourceF);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for (int i=0; i<val.size1(); i++) {
      double section=hydroSol(i,0);
      val (i,0)  =  source1(i,0)*section;
    }
  }
};


class dgConservationLawShallowWaterTracer1d::riemannLAX:public function {
  fullMatrix<double> normals, hydroSolL, hydroSolR;
  fullMatrix<double> widthL, widthR, elevationL, elevationR;
  fullMatrix<double> tracerL, tracerR;
  fullMatrix<double> diffusivityL, diffusivityR;
  fullMatrix<double> tracerGradientL, tracerGradientR;
public:
  riemannLAX(const function *hydroSolF, const function *elevationF, const function *widthF, const function *tracerF, const function *diffusivityF): function(2) {
    setArgument(normals,function::getNormals(), 0);
    setArgument(hydroSolL,hydroSolF, 0);
    setArgument(hydroSolR,hydroSolF, 1);
    setArgument(elevationL,elevationF, 0);
    setArgument(elevationR,elevationF, 1); 
    setArgument(widthL,widthF, 0); 
    setArgument(widthR,widthF, 1); 
    setArgument(tracerL,tracerF, 0); 
    setArgument(tracerR,tracerF, 1); 
    setArgument(diffusivityL,diffusivityF,0);
    setArgument(diffusivityR,diffusivityF,1);
    setArgument(tracerGradientL,function::getSolutionGradient(),0);
    setArgument(tracerGradientR,function::getSolutionGradient(),1);
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    for (int iPt = 0; iPt < val.size1(); ++iPt) {
      double sL = hydroSolL(iPt,0),     sR = hydroSolR(iPt,0);
      double uL = hydroSolL(iPt,1),     uR = hydroSolR(iPt,1);
      double wL = widthL(iPt,0),        wR = widthR(iPt,0);
      double cL = tracerL(iPt,0) ,      cR = tracerR(iPt,0);
      double n0 = normals(iPt,0); 
      double sM = (sL + sR)/2;
      double uM = (uL + uR)/2;
      double cUp=uM>0?cL:cR;
      double F1,F2;
      dgConservationLawShallowWater1dLaxSolver(sL, sR, uL, uR, wL, wR, 0, 0, n0, F1, F2);
      val(iPt,0) = - F1 * cUp ;

      double sigma_factor=1;
      double dcdxL = tracerGradientL(iPt,0),   dcdxR = tracerGradientR(iPt,0);
      double kappaL   = diffusivityL(iPt,0),   kappaR = diffusivityR(iPt,0);
      double dx = std::min(m->element(iPt/m->nPointByElement())->getEdge(0).length(),m->getSecondaryCache(1)->element(iPt/m->nPointByElement())->getEdge(0).length());
      int order = m->getGroupOfElements()->getOrder();
      int dim = m->element(iPt/m->nPointByElement())->getDim();
      double kappa = std::max(kappaL, kappaR);
      double sigma = sigma_factor*kappa*(order+1)*(order+dim)/(dim*dx);
      val(iPt,0) += sM * kappa * ( dcdxL + dcdxR )/2 - sM * sigma * ( cL - cR )/2;
      val(iPt,1) = - val(iPt,0);
    }
  }
};

//**********************************************
// Bifurcation solver
//**********************************************
class dgConservationLawShallowWaterTracer1d::riemannBifurcation:public function {
  fullMatrix<double> normals1, normals2, normals3, hydroSol1, hydroSol2, hydroSol3;
  fullMatrix<double> eta1, eta2, eta3, w1, w2, w3;
  fullMatrix<double> tracer1, tracer2, tracer3;
  fullMatrix<double> sol1_eta, sol2_eta, sol3_eta, eta1_w, eta2_w, eta3_w ;
  fullMatrix<double> diffusivity1,diffusivity2,diffusivity3;
  fullMatrix<double> tracerGradient1,tracerGradient2,tracerGradient3;
  functionReplace replaceElevation1, replaceElevation2, replaceElevation3, replaceWidth1, replaceWidth2, replaceWidth3;
  bool zeroFlux;
  double sigmaFactor;

public:
  riemannBifurcation(const function *hydroSolF, const function *elevationF, const function *widthF, const function *tracerF, const function *diffusivityF, bool zeroFluxBif, double sigmaFactorBif):function(3) {
    setArgument (normals1, function::getNormals(), 0);
    setArgument (normals2, function::getNormals(), 1);
    setArgument (normals3, function::getNormals(), 2);
    setArgument (hydroSol1, hydroSolF, 0);
    setArgument (hydroSol2, hydroSolF, 1);
    setArgument (hydroSol3, hydroSolF, 2);
    setArgument (tracer1, tracerF, 0);
    setArgument (tracer2, tracerF, 1);
    setArgument (tracer3, tracerF, 2);
    setArgument (tracerGradient1, function::getSolutionGradient(), 0);
    setArgument (tracerGradient2, function::getSolutionGradient(), 1);
    setArgument (tracerGradient3, function::getSolutionGradient(), 2);
    setArgument (diffusivity1, diffusivityF, 0);
    setArgument (diffusivity2, diffusivityF, 1);
    setArgument (diffusivity3, diffusivityF, 2);

    addFunctionReplace(replaceElevation1);//The elevation depends on the solution
    replaceElevation1.replace(sol1_eta, hydroSolF,0);
    replaceElevation1.get(eta1,elevationF,0);
    addFunctionReplace(replaceWidth1);//The width depends on elevation
    replaceWidth1.replace(eta1_w, elevationF,0);
    replaceWidth1.get(w1,widthF,0);

    addFunctionReplace(replaceElevation2);//The elevation depends on the solution
    replaceElevation2.replace(sol2_eta, hydroSolF,1);
    replaceElevation2.get(eta2,elevationF,1);
    addFunctionReplace(replaceWidth2);//The width depends on elevation
    replaceWidth2.replace(eta2_w, elevationF,1);
    replaceWidth2.get(w2,widthF,1);

    addFunctionReplace(replaceElevation3);//The elevation depends on the solution
    replaceElevation3.replace(sol3_eta, hydroSolF,2);
    replaceElevation3.get(eta3,elevationF,2);
    addFunctionReplace(replaceWidth3);//The width depends on elevation
    replaceWidth3.replace(eta3_w, elevationF,2);
    replaceWidth3.get(w3,widthF,2);

    zeroFlux = zeroFluxBif;
    sigmaFactor = sigmaFactorBif; 
  }

  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    for (int iPt = 0; iPt < val.size1(); ++iPt) {
      fullVector<double> u(6);
      fullVector<double> n(3);
      u(0)=hydroSol1(iPt,0); u(1)=hydroSol2(iPt,0); u(2)=hydroSol3(iPt,0);
      u(3)=hydroSol1(iPt,1); u(4)=hydroSol2(iPt,1); u(5)=hydroSol3(iPt,1);
      n(0)=normals1(iPt,0); n(1)=normals2(iPt,0); n(2)=normals3(iPt,0);
      if(zeroFlux){
        if(u(3)>0 && u(4)>0){
          val(iPt,0)=-u(0)*u(3)*n(0)*tracer1(iPt,0);
          val(iPt,1)=-u(1)*u(4)*n(1)*tracer1(iPt,0);
          val(iPt,2)=0;
        }else {
          val.setAll(0.);
        }
        return;
      }

      fullVector<double> ub(6);
      fullVector<double> caracs(3);
      fullVector<double> eta(3);
      fullVector<double> w(3);
      fullVector<double> du(6);
      fullVector<double> fu(6);
      fullVector<double> fub(6);
      fullMatrix<double> dfdu(6,6);
      sol1_eta(iPt,0)=u(0); replaceElevation1.compute(); eta(0)=eta1(iPt,0);
      sol2_eta(iPt,0)=u(1); replaceElevation2.compute(); eta(1)=eta2(iPt,0);
      sol3_eta(iPt,0)=u(2); replaceElevation3.compute(); eta(2)=eta3(iPt,0);
      eta1_w(iPt,0)=eta(0); replaceWidth1.compute(); w(0)=w1(iPt,0);
      eta2_w(iPt,0)=eta(1); replaceWidth2.compute(); w(1)=w2(iPt,0);
      eta3_w(iPt,0)=eta(2); replaceWidth3.compute(); w(2)=w3(iPt,0);
      caracs(0) = 0.5*(u(0) + n(0)*u(3)*sqrt(u(0)*w(0)/g));
      caracs(1) = 0.5*(u(1) + n(1)*u(4)*sqrt(u(1)*w(1)/g));
      caracs(2) = 0.5*(u(2) + n(2)*u(5)*sqrt(u(2)*w(2)/g));
      double maxNorm=1e-8;
      double eps=1e-3;
      double delta[6]={u(0)*eps,u(1)*eps,u(2)*eps,eps,eps,eps};
      int nr;
      for (nr=0; nr < 10; nr++){
        system(u, eta, w, caracs, n, fu);
        for(int i=0;i<6;i++){
          u(i)+=delta[i];
          sol1_eta(iPt,0)=u(0); replaceElevation1.compute(); eta(0)=eta1(iPt,0);
          sol2_eta(iPt,0)=u(1); replaceElevation2.compute(); eta(1)=eta2(iPt,0);
          sol3_eta(iPt,0)=u(2); replaceElevation3.compute(); eta(2)=eta3(iPt,0);
          eta1_w(iPt,0)=eta(0); replaceWidth1.compute(); w(0)=w1(iPt,0);
          eta2_w(iPt,0)=eta(1); replaceWidth2.compute(); w(1)=w2(iPt,0);
          eta3_w(iPt,0)=eta(2); replaceWidth3.compute(); w(2)=w3(iPt,0);
          system(u, eta, w, caracs, n, fub);
          for(int j=0;j<6;j++)
            dfdu.set(j,i,(fub(j)-fu(j))/delta[i]);
          u(i)-=delta[i];
        }
        dfdu.luSolve(fu,du);
        for (int i=0;i<6;i++)
          u(i)-=du(i);
        sol1_eta(iPt,0)=u(0); replaceElevation1.compute(); eta(0)=eta1(iPt,0);
        sol2_eta(iPt,0)=u(1); replaceElevation2.compute(); eta(1)=eta2(iPt,0);
        sol3_eta(iPt,0)=u(2); replaceElevation3.compute(); eta(2)=eta3(iPt,0);
        eta1_w(iPt,0)=eta(0); replaceWidth1.compute(); w(0)=w1(iPt,0);
        eta2_w(iPt,0)=eta(1); replaceWidth2.compute(); w(1)=w2(iPt,0);
        eta3_w(iPt,0)=eta(2); replaceWidth3.compute(); w(2)=w3(iPt,0);
        if(du.norm()<maxNorm) break;
      }
      if(nr==10) Msg::Fatal("Newton for bifurcation solver diverges | norm:%e u=%.3f %.3f %.3f eta=%.3f %.3f %.3f\n",du.norm(),u(3),u(4),u(5),eta(0),eta(1),eta(2));

      double u1=u(3), u2=u(4), u3=u(5);
      double s1=u(0), s2=u(1), s3=u(2);
      double n1=n(0), n2=n(1), n3=n(2);
      double c1=tracer1(iPt,0), c2=tracer2(iPt,0), c3=tracer3(iPt,0);
      double fluxMass1=-u1*s1*n1;
      double fluxMass2=-u2*s2*n2;
      double fluxMass3=-u3*s3*n3;
      if(n1*u1<=0 && u2*n2>=0 && u3*n3>=0){
        val(iPt,0)=fluxMass1*c1;
        val(iPt,1)=fluxMass2*c1;
        val(iPt,2)=fluxMass3*c1;
      }else if(n1*u1>=0 && u2*n2<=0 && u3*n3>=0){
        val(iPt,0)=fluxMass1*c2;
        val(iPt,1)=fluxMass2*c2;
        val(iPt,2)=fluxMass3*c2;
      }else if(n1*u1>=0 && u2*n2>=0 && u3*n3<=0){
        val(iPt,0)=fluxMass1*c3;
        val(iPt,1)=fluxMass2*c3;
        val(iPt,2)=fluxMass3*c3;
      }else if(n1*u1>=0 && u2*n2<=0 && u3*n3<=0){
        val(iPt,0)=fluxMass1*(fluxMass2*c2+fluxMass3*c3)/(fluxMass2+fluxMass3);
        val(iPt,1)=fluxMass2*c2;
        val(iPt,2)=fluxMass3*c3;
      }else if(n1*u1<=0 && u2*n2>=0 && u3*n3<=0){
        val(iPt,0)=fluxMass1*c1;
        val(iPt,1)=fluxMass2*(fluxMass1*c1+fluxMass3*c3)/(fluxMass1+fluxMass3);
        val(iPt,2)=fluxMass3*c3;
      }else if(n1*u1<=0 && u2*n2<=0 && u3*n3>=0){
        val(iPt,0)=fluxMass1*c1;
        val(iPt,1)=fluxMass2*c2;
        val(iPt,2)=fluxMass3*(fluxMass1*c1+fluxMass2*c2)/(fluxMass1+fluxMass2);
      }else {
        printf("bifurcation configuration not take into account yet: u1*n1=%.2f u2*n2=%.2f u3*n3=%.2f\n",u1*n1,u2*n2,u3*n3);
      }
      if (sigmaFactor>0){
        double kappa  = std::max(diffusivity1(iPt,0),diffusivity2(iPt,0));
        kappa = std::max(kappa,diffusivity3(iPt,0));
        double dx = m->element(iPt / m->nPointByElement())->getEdge(0).length();
        int order = m->getGroupOfElements()->getOrder();
        int dim = m->element(iPt/m->nPointByElement())->getDim();
        double sigma = sigmaFactor*kappa*(order+1)*(order+dim)/(dim*dx);
        double sM = (s1+s2+s3)/3;
        double c1prime = (c2+c3)/2;
        double c2prime = (c1+c3)/2;
        double c3prime = (c1+c2)/2;
        val(iPt,0) +=  - sM * sigma * (c1-c1prime)/2;
        val(iPt,1) +=  - sM * sigma * (c2-c2prime)/2;
        val(iPt,2) +=  - sM * sigma * (c3-c3prime)/2;
      }
    }
  }

  void system(fullVector<double> &u, fullVector<double> &eta, fullVector<double> &w, fullVector<double> &carac, fullVector<double> &n, fullVector<double> &f){
    f(0) = .5*(u(0) + n(0)*u(3)*sqrt(u(0)*w(0)/g)) - carac(0);
    f(1) = .5*(u(1) + n(1)*u(4)*sqrt(u(1)*w(1)/g)) - carac(1);
    f(2) = .5*(u(2) + n(2)*u(5)*sqrt(u(2)*w(2)/g)) - carac(2);
    f(3) = n(0)*u(0)*u(3) + n(1)*u(1)*u(4) + n(2)*u(2)*u(5);
    f(4) = .5*u(3)*u(3) + g*eta(0) - .5*u(4)*u(4) - g*eta(1);
    f(5) = .5*u(3)*u(3) + g*eta(0) - .5*u(5)*u(5) - g*eta(2);
  }
};

//**********************************************
// Boundary conditions
//**********************************************

class dgConservationLawShallowWaterTracer1d::boundaryWall : public dgBoundaryCondition {
  class term : public function {
  public:
    term () : function (1) {
    }
    void call (dataCacheMap *map, fullMatrix<double> &val) { 
      val(0,0) = 0;
    }
  };
public:
  term _term;
  boundaryWall(dgConservationLawShallowWaterTracer1d *claw) : _term () {
    _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawShallowWaterTracer1d::newBoundaryWall(){
  return new boundaryWall(this);
}


class dgConservationLawShallowWaterTracer1d::forceTracer:public dgBoundaryCondition {
  class term:public function {
    fullMatrix<double> normals;
    fullMatrix<double> sectionExt, velocityExt, hydroSolIn, elevationExt, elevationIn, widthExt, widthIn;
    fullMatrix<double> tracerIn, tracerExt;
    fullMatrix<double> hydroSolExt_eta, elevationExt_w;
    fullMatrix<double> tracerGradientIn, tracerGradientExt;
    fullMatrix<double> diffusivityIn;
    functionReplace elevationExtReplace,widthExtReplace;
  public:
    term(const function *sectionExtF, const function *velocityExtF, const function *tracerExtF, const function *tracerGradientExtF, const function *hydroSolF, const function *widthF, const function *elevationF, const function *tracerF , const function *tracerGradientF, const function *diffusivityF):function(1){
      setArgument(normals, function::getNormals());
      setArgument(hydroSolIn, hydroSolF);
      setArgument(sectionExt, sectionExtF);
      setArgument(velocityExt, velocityExtF);
      setArgument(widthIn, widthF); //widthExt is computed with functionReplace
      setArgument(elevationIn, elevationF); //elevationExt is computed with functionReplace
      setArgument(tracerIn, tracerF);
      setArgument(tracerExt, tracerExtF);
      setArgument(tracerGradientIn, tracerGradientF);
      setArgument(tracerGradientExt, tracerGradientExtF);
      setArgument(diffusivityIn,diffusivityF);

      addFunctionReplace(elevationExtReplace);
      elevationExtReplace.replace(hydroSolExt_eta,hydroSolF);
      elevationExtReplace.get(elevationExt, elevationF);

      addFunctionReplace(widthExtReplace);
      widthExtReplace.replace(elevationExt_w,elevationF);
      widthExtReplace.get(widthExt, widthF);
    }
    void call (dataCacheMap *map, fullMatrix<double> &val) { 
      for (int iPt = 0; iPt < val.size1(); ++iPt) {
        double sIn = hydroSolIn(iPt,0), sExt = sectionExt(iPt,0);
        double uIn = hydroSolIn(iPt,1), uExt = velocityExt(iPt,0);
        hydroSolExt_eta(iPt,0)=sExt; 
        elevationExtReplace.compute();
        elevationExt_w(iPt,0)=elevationExt(iPt,0);
        widthExtReplace.compute();
        double wIn = widthIn(iPt,0), wExt = widthExt(iPt,0);
        double cIn = tracerIn(iPt,0), cExt = tracerExt(iPt,0);
        double n0 = normals(iPt,0);
        double sM = (sIn + sExt)/2;
        double F1,F2;
        dgConservationLawShallowWater1dLaxSolver(sIn, sExt, uIn, uExt, wIn, wExt, 0., 0., n0, F1, F2);
        if(map->elementId(iPt/map->nPointByElement())>0)
          val(iPt,0) = - F1 * (uIn>=0?cIn:cExt);//upstream
        else
          val(iPt,0) = - F1 * (uIn>=0?cExt:cIn);//downstream

        double sigma_factor=1;
        double dcdxIn = tracerGradientIn(iPt,0),   dcdxExt = tracerGradientExt(iPt,0);
        double kappa  = diffusivityIn(iPt,0);
        double dx = map->element(iPt/map->nPointByElement())->getEdge(0).length();
        int order = map->getGroupOfElements()->getOrder();
        int dim = map->element(iPt/map->nPointByElement())->getDim();
        double sigma = sigma_factor*kappa*(order+1)*(order+dim)/(dim*dx);
        val(iPt,0)+=sM * kappa * ( dcdxIn + dcdxExt )/2 - sM * sigma * ( cIn - cExt )/2;

      }
    }
  };
public:
  term  _term;
  forceTracer (dgConservationLawShallowWaterTracer1d *claw, 
  const function *sectionExtF, const function *velocityExtF, const function *tracerExtF, const function *tracerGradientExtF, const function *hydroSolF, const function *widthF, const function *elevationF, const function *tracerF, const function *tracerGradientF, const function *diffusivity
  ):_term (sectionExtF, velocityExtF, tracerExtF,tracerGradientExtF, hydroSolF, widthF, elevationF, tracerF, tracerGradientF,diffusivity){
      _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawShallowWaterTracer1d::newForceTracer(const function *sectionExtF, const function *velocityExtF, const function *tracerExtF, const function *tracerGradientExtF){
  return new forceTracer(this, sectionExtF, velocityExtF, tracerExtF,tracerGradientExtF, _hydroSolution, _width, _elevation, _tracer, function::getSolutionGradient(), _diffusivity);
}

//**********************************************
// Setup
//**********************************************
void dgConservationLawShallowWaterTracer1d::setup () {
  _volumeTerm0[""] = new source(_source,_hydroSolution);
  _volumeTerm1[""] = new advection (_hydroSolution, _tracer, _diffusivity);
  _interfaceTerm0[""] = new riemannLAX (_hydroSolution, _elevation, _width, _tracer, _diffusivity);
  _bifurcationTerm0[""] = new riemannBifurcation (_hydroSolution, _elevation, _width, _tracer,_diffusivity, _zeroFluxBifurcations,_sigmaFactBif);
  _massFactor[""] = std::make_pair(new massFactor(_hydroSolution), false);
}

dgConservationLawShallowWaterTracer1d::~dgConservationLawShallowWaterTracer1d () {
  if (_volumeTerm0[""]) delete _volumeTerm0[""];
  if (_volumeTerm1[""]) delete _volumeTerm1[""];
  if (_clipToPhysics[""]) delete _clipToPhysics[""];
  if (_bifurcationTerm0[""]) delete _bifurcationTerm0[""];
  if (_massFactor.count("") != 0)     delete _massFactor[""].first;
}
