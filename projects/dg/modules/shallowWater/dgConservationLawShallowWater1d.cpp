#include "dgConservationLawShallowWater1d.h"
#include "dgGroupOfElements.h"
#include "dgConservationLawShallowWater2d.h"
#include "MElement.h"

static double g = 9.81;
dgConservationLawShallowWater1d::dgConservationLawShallowWater1d() : dgConservationLawFunction(2)
{
  function *fzero = new functionConstant(0.);
  _hydroSol=NULL;
  _linearDissipation = fzero;
  _quadraticDissipation=fzero;
  _sourceMass = fzero;
  _sourceMomentum = fzero;
  _elevation = fzero;
  _elevationInterp = fzero;
  _widthData = fzero;
  _width = fzero;
  _typeRiemann = "LAX";
  _diffusivity = fzero;
  _clipSectionValue = 0;
  _useClipToPhysics = false;
}
//***************************************
// Depending field (elevation and width)
//***************************************

class dgConservationLawShallowWater1d::elevationShallow : public function{
  fullMatrix<double>  sectionData, widthData, solution, elevation;
  functionReplace sectionReplace, widthReplace;
  fullMatrix<double> elevationInterpWidth, elevationInterpSection, widthInterp, sectionNR, widthNR;
public :
  elevationShallow(const function *hydroSol, const function *sectionDataF, const function *widthDataF, const function *elevationInterpF) : function(1)
  {
    setArgument (solution, hydroSol );
    setArgument (sectionData, sectionDataF);
    setArgument (widthData, widthDataF);
    //width depends on elevation
    addFunctionReplace (widthReplace);
    widthReplace.replace (elevationInterpWidth,elevationInterpF);
    widthReplace.get(widthNR, widthDataF);
    //section depends on elevation
    addFunctionReplace (sectionReplace);
    sectionReplace.replace (elevationInterpSection,elevationInterpF);
    sectionReplace.get(sectionNR, sectionDataF);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    double deta=0;
    double max_norm=1e-8,norm=0;
    int max_iter=10;
    fullMatrix<double> eta(val.size1(),1);
    fullMatrix<double> s0(val.size1(),1);
    for (int i=0; i<val.size1(); i++) {
      s0(i,0)=solution(i,0);
      eta(i,0)=0;
    }
    int nr;
    for (nr=0; nr<=max_iter; nr++){
      norm=0;
      for (int i=0; i<val.size1(); i++)
        elevationInterpWidth(i,0)=eta(i,0);
      widthReplace.compute();
      for (int i=0; i<val.size1(); i++)
        elevationInterpSection(i,0)=eta(i,0);
      sectionReplace.compute();
      for (int i=0; i<val.size1(); i++) {
        deta=-(sectionNR(i,0)-s0(i,0))/widthNR(i,0);
        eta(i,0)+=deta;
        norm=std::max(norm,fabs(deta));
      }
      if(norm<max_norm) break;
    }
    for (int i=0; i<val.size1(); i++){
      val(i,0)=eta(i,0);
    }
  }
};

class dgConservationLawShallowWater1d::widthShallow : public function{
  fullMatrix<double>  widthData, elevation;
  functionReplace widthReplace;
  fullMatrix<double> elevationInterpWidth, widthInterp, width;
public :
  widthShallow(const function *widthDataF, const function *elevationF, const function *elevationInterpF) : function(1){
    setArgument (widthData, widthDataF);
    setArgument (elevation, elevationF);
    //width depends on elevation
    addFunctionReplace (widthReplace);
    widthReplace.replace (elevationInterpWidth,elevationInterpF);
    widthReplace.get(width, widthDataF);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for (int i=0; i<val.size1(); i++) {
      elevationInterpWidth(i,0)=elevation(i,0);
      widthReplace.compute();
      val (i,0) = width(i,0);
    }
  }
};

//*************************************
// Terms of the equation
//*************************************
class dgConservationLawShallowWater1d::advection: public function {
  fullMatrix<double> sol, solGradient, elevation, diffusivity;
public:
  advection(const function *hydroSolF, const function *elevationF, const function *diffusivityF) : function(6) {
    setArgument(sol, hydroSolF);
    setArgument(solGradient, function::getSolutionGradient());
    setArgument(elevation, elevationF);
    setArgument(diffusivity, diffusivityF);
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++) {
      double u = sol(i,1);
      double eta = elevation(i,0);
      double section=sol(i,0);
      double nu = diffusivity(i,0);
      double dudx = solGradient(i,3);
      // flux_x
      val(i,0) = section*u;
      val(i,1) = .5 * u*u + g*eta - nu*dudx ;
      // flux_y
      val(i,2) = 0;
      val(i,3) = 0;
      // flux_z
      val(i,4) = 0;
      val(i,5) = 0;
    }
  }
};

class dgConservationLawShallowWater1d::source: public function {
  fullMatrix<double>  solution, solutionGradient, diffusivity;
  fullMatrix<double>  linearDissipation, quadraticDissipation, sourceMass, sourceMomentum;
public :
  source(const function *hydroSol, const function *linearDissipationF, const function *quadraticDissipationF, const function *sourceMassF, const function *sourceMomentumF, const function *diffusivityF) : function(2)
  {
    setArgument (linearDissipation, linearDissipationF);
    setArgument (quadraticDissipation, quadraticDissipationF);
    setArgument (sourceMass, sourceMassF);
    setArgument (sourceMomentum, sourceMomentumF);
    setArgument (solution, hydroSol);
    setArgument (solutionGradient, function::getSolutionGradient());
    setArgument (diffusivity, diffusivityF);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for (int i=0; i<val.size1(); i++) {
      //Mass
      val(i,0)  =   sourceMass(i,0);

      //Momentum
      double u = solution(i,1);
      val (i,1)  = - linearDissipation(i,0)*u;
      val (i,1) += - quadraticDissipation(i,0)*fabs(u)*u;
      val (i,1) +=   sourceMomentum(i,0);

      //Diffusion
      double nu = diffusivity(i,0);
      double S = solution(i,0);
      double dSdx = solutionGradient(i,0);
      double dudx = solutionGradient(i,3);
      val(i,1) += nu / S * dudx * dSdx;
    }
  }
};

//**********************************************
// Riemann solvers
//**********************************************

class dgConservationLawShallowWater1d::riemannLAX:public function {
  fullMatrix<double> solL, solR;
  fullMatrix<double> widthL, widthR, elevationL, elevationR;
  fullMatrix<double> diffusivityL, diffusivityR, solGradientL, solGradientR;
  fullMatrix<double> normalsL, normalsR;
  fullMatrix<double> solL_eta, solR_eta, etaL_w, etaR_w;
  fullMatrix<double> etaL, etaR, wL, wR;
  functionReplace replaceElevationL, replaceElevationR, replaceWidthL, replaceWidthR;
public:
  riemannLAX(const function *hydroSol, const function *width, const function *elevation, const function *widthData, const function *diffusivity): function(4) {
    setArgument(normalsL,function::getNormals(), 0);
    setArgument(normalsR,function::getNormals(), 1);
    setArgument(solL, hydroSol, 0);
    setArgument(solR, hydroSol, 1);
    setArgument(solGradientL,function::getSolutionGradient(), 0);
    setArgument(solGradientR,function::getSolutionGradient(), 1);
    setArgument(elevationL, elevation, 0);
    setArgument(elevationR, elevation, 1); 
    setArgument(widthL, width, 0); 
    setArgument(widthR, width, 1); 
    setArgument(diffusivityL, diffusivity, 0);
    setArgument(diffusivityR, diffusivity, 1); 
    
    addFunctionReplace(replaceElevationL);//The elevation depends on the solution
    replaceElevationL.replace(solL_eta, hydroSol, 0);
    replaceElevationL.get(etaL,elevation, 0);
    addFunctionReplace(replaceWidthL);//The width depends on elevation
    replaceWidthL.replace(etaL_w, elevation, 0);
    replaceWidthL.get(wL,widthData, 0);

    addFunctionReplace(replaceElevationR);//The elevation depends on the solution
    replaceElevationR.replace(solR_eta, hydroSol, 1);
    replaceElevationR.get(etaR, elevation, 1);
    addFunctionReplace(replaceWidthR);//The width depends on elevation
    replaceWidthR.replace(etaR_w, elevation, 1);
    replaceWidthR.get(wR,widthData, 1);
  };
  void call (dataCacheMap *m, fullMatrix<double> &val)  {
    for (int iPt = 0; iPt < val.size1(); ++iPt) {
      double sL = solL(iPt,0),          sR = solR(iPt,0);
      double uL = solL(iPt,1),          uR = solR(iPt,1) ;
      double pL = g*elevationL(iPt,0),  pR = g*elevationR(iPt,0);
      double nL = normalsL(iPt,0);

      const dgGroupOfElements *groupR = m->getGroupOfElements();
      const dgGroupOfElements *groupL = m->getSecondaryCache(1)->getGroupOfElements();
      if(groupR->getPhysicalTag() == groupL->getPhysicalTag()){
        double fs, fu;
        dgConservationLawShallowWater1dLaxSolver(sL, sR, uL, uR, widthL(iPt,0), widthR(iPt,0), pL, pR, nL, fs, fu);
        val(iPt,0) = -fs;
        val(iPt,1) = -fu;
        val(iPt,2) = -val(iPt,0);
        val(iPt,3) = -val(iPt,1);
        //Diffusion flux
        double sigma_factor=1;
        double dudxL = solGradientL(iPt,3),   dudxR = solGradientR(iPt,3);
        double nuL   = diffusivityL(iPt,0),     nuR = diffusivityR(iPt,0);
        double dx = std::min(m->element(iPt/m->nPointByElement())->getEdge(0).length(),m->getSecondaryCache(1)->element(iPt/m->nPointByElement())->getEdge(0).length());
        int order = m->getGroupOfElements()->getOrder();
        int dim = m->element(iPt/m->nPointByElement())->getDim();
        double nu = std::max(nuL, nuR);
        double sigma = sigma_factor*nu*(order+1)*(order+dim)/(dim*dx);
        val(iPt,0) += nu * ( dudxL + dudxR )/2 + sigma * ( uL - uR )/2;
      }else{ //Riemann discontinuity solver
        fullVector<double> u(4);
        fullVector<double> ub(4);
        fullVector<double> caracs(2);
        fullVector<double> eta(2);
        fullVector<double> w(2);
        fullVector<double> n(2);
        fullVector<double> du(4);
        fullVector<double> fu(4);
        fullVector<double> fub(4);
        fullMatrix<double> dfdu(4,4);
        u(0)=sL; u(1)=sR;
        u(2)=uL; u(3)=uR;
        n(0)=normalsL(iPt,0); n(1)=normalsR(iPt,0);
        solL_eta(iPt,0)=u(0); replaceElevationL.compute(); eta(0)=etaL(iPt,0);
        solR_eta(iPt,0)=u(1); replaceElevationR.compute(); eta(1)=etaR(iPt,0);
        etaL_w(iPt,0)=eta(0); replaceWidthL.compute(); w(0)=wL(iPt,0);
        etaR_w(iPt,0)=eta(1); replaceWidthR.compute(); w(1)=wR(iPt,0);
        caracs(0) = 0.5*(u(0) + n(0)*u(2)*sqrt(u(0)*w(0)/g));
        caracs(1) = 0.5*(u(1) + n(1)*u(3)*sqrt(u(1)*w(1)/g));
        double maxNorm=1e-8;
        double eps=1e-3;
        double delta[4]={u(0)*eps,u(1)*eps,eps,eps};
        int nr;
        for (nr=0; nr < 10; nr++){
          systemRiemannDiscontinuity(u, eta, w, caracs, n, fu);
          for(int i=0;i<4;i++){
            u(i)+=delta[i];
            solL_eta(iPt,0)=u(0); replaceElevationL.compute(); eta(0)=etaL(iPt,0);
            solR_eta(iPt,0)=u(1); replaceElevationR.compute(); eta(1)=etaR(iPt,0);
            etaL_w(iPt,0)=eta(0); replaceWidthL.compute(); w(0)=wL(iPt,0);
            etaR_w(iPt,0)=eta(1); replaceWidthR.compute(); w(1)=wR(iPt,0);
            systemRiemannDiscontinuity(u, eta, w, caracs, n, fub);
            for(int j=0;j<4;j++)
              dfdu.set(j,i,(fub(j)-fu(j))/delta[i]);
            u(i)-=delta[i];
          }
          dfdu.luSolve(fu,du);
          for (int i=0;i<4;i++)
            u(i)-=du(i);
          solL_eta(iPt,0)=u(0); replaceElevationL.compute(); eta(0)=etaL(iPt,0);
          solR_eta(iPt,0)=u(1); replaceElevationR.compute(); eta(1)=etaR(iPt,0);
          etaL_w(iPt,0)=eta(0); replaceWidthL.compute(); w(0)=wL(iPt,0);
          etaR_w(iPt,0)=eta(1); replaceWidthR.compute(); w(1)=wR(iPt,0);
          if(du.norm()<maxNorm) break;
        }
        if(nr==10) Msg::Fatal("Newton for discontinuity solver diverges | norm:%e u=%.3f %.3f eta=%.3f %.3f %s-%s\n",du.norm(),u(2),u(3),eta(0),eta(1),groupL->getPhysicalTag().c_str(),groupR->getPhysicalTag().c_str());
        val(iPt,0)=-u(2)*u(0)*n(0);
        val(iPt,1)=-(.5*u(2)*u(2) + g*eta(0))*n(0);
        val(iPt,2)=-u(3)*u(1)*n(1);
        val(iPt,3)=-(.5*u(3)*u(3) + g*eta(1))*n(1);
      }
    }
   }
  void systemRiemannDiscontinuity(fullVector<double> &u, fullVector<double> &eta, fullVector<double> &w, fullVector<double> &carac, fullVector<double> &n, fullVector<double> &f){
    f(0) = .5*(u(0) + n(0)*u(2)*sqrt(u(0)*w(0)/g)) - carac(0);
    f(1) = .5*(u(1) + n(1)*u(3)*sqrt(u(1)*w(1)/g)) - carac(1);
    f(2) = n(0)*u(0)*u(2) + n(1)*u(1)*u(2);
    f(3) = .5*u(0)*u(2) + g*eta(0) - .5*u(1)*u(3) - g*eta(1);
  }
};
void dgConservationLawShallowWater1dLaxSolver(double sL, double sR,double uL, double uR, double wL, double wR, double pL, double pR, double n0, double &fs, double &fu){
  double cL = fabs(uL)+sqrt(g*sL/wL);
  double cR = fabs(uR)+sqrt(g*sR/wR);
  double aMax = std::max(cL,cR);
	fs = .5*( sL*uL + sR*uR )*n0 +  aMax*(sL-sR) ;
	fu = .5*( .5*uL*uL+pL  + .5*uR*uR+pR )*n0 + aMax*(uL-uR);       
}
void dgConservationLawShallowWater1dRoeSolver(double sL, double sR,double uL, double uR, double wL, double wR, double pL, double pR, double n0, double &fs, double &fu){
  double fLs = sL*uL*n0, fRs =  sR*uR*n0;
  double fLu =  (.5*uL*uL+pL)*n0, fRu = (.5*uR*uR+pR)*n0;
  double uroe = ( uL * sqrt(sL) + uR * sqrt(sR) ) / ( sqrt(sL) + sqrt(sR) );
  double croe = sqrt( g * (sL/wL + sR/wR));
  double sj = sR-sL;
  double uj = uR-uL;
  double wj = wR-wL;
  double kroe1[2] = {1,uroe - croe};
  double kroe2[2] = {1,uroe + croe};
  double lroe1=uroe - croe;
  double lroe2=uroe + croe;
  double aroe1 = ( (croe + uroe) * sj/wj - uj ) / (2*croe);
  double aroe2 = ( (croe - uroe) * sj/wj + uj ) / (2*croe);
  fs = (fLs + fRs)/2 - 0.5 * (aroe1 * fabs(lroe1) * kroe1[0] + aroe2 * fabs(lroe2) * kroe2[0]);
  fu = (fLu + fRu)/2 - 0.5 * (aroe1 * fabs(lroe1) * kroe1[1] + aroe2 * fabs(lroe2) * kroe2[1]);       
}

//**********************************************
//Clip to physics
//**********************************************

class dgConservationLawShallowWater1d::clipToPhysics : public function {
  fullMatrix<double> sol;
  double _SMin;
public:
  clipToPhysics(const function *hydroSol, double SMin):function(3) {
    setArgument (sol, hydroSol);
    _SMin=SMin;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    for (int i = 0 ; i < val.size1(); i++ ){
      val(i,0) = sol(i,0);
      val(i,1) = sol(i,1);
      if ( sol(i,0) < _SMin){
        val(i,0) = _SMin;
      }
    }
  }
};

//**********************************************
// Boundary conditions
//**********************************************

class dgConservationLawShallowWater1d::boundaryWall : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> sol, normals, elevation;
  public:
    term (const function *hydroSol, const function *elevationF) : function (2) {
      setArgument (sol, hydroSol);
      setArgument (elevation, elevationF);
      setArgument (normals, function::getNormals());
    }
    void call (dataCacheMap *map, fullMatrix<double> &val) { 
      val(0,0) = 0;
      val(0,1) = - g*elevation(0,0)*normals(0,0);

    }
  };
public:
  term _term;
  boundaryWall(dgConservationLawShallowWater1d *claw) : _term (claw->getHydroSolution(), claw->getElevation()) {
    _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawShallowWater1d::newBoundaryWall(){
  return new boundaryWall(this);
}

class dgConservationLawShallowWater1d::forceSol:public dgBoundaryCondition {
  class term:public function {
    fullMatrix<double> normals, solExt, solExt_eta, sol, width, widthExt;
    fullMatrix<double> sectionExt,velocityExt;
    fullMatrix<double> elevationExt, elevationExt_w, elevation;
    functionReplace elevationExtReplace, widthExtReplace;
  public:
    term(const function *sectionExtF, const function *velocityExtF, const function *solF, const function *elevationF, const function *widthF):function(2){
      setArgument(sectionExt, sectionExtF);
      setArgument(velocityExt, velocityExtF);
      setArgument(sol, solF);
      setArgument(normals, function::getNormals());
      setArgument(elevation, elevationF);
      setArgument(width, widthF);

      addFunctionReplace(elevationExtReplace);
      elevationExtReplace.replace(solExt_eta,solF);
      elevationExtReplace.get(elevationExt, elevationF);

      addFunctionReplace(widthExtReplace);
      widthExtReplace.replace(elevationExt_w,elevationF);
      widthExtReplace.get(widthExt, widthF);
    }
    void call (dataCacheMap *map, fullMatrix<double> &val) { 
      double uIn = sol(0,1), uExt = velocityExt(0,0);
      double sIn = sol(0,0), sExt = sectionExt(0,0);
      double n0 = normals(0,0);

      solExt_eta(0,0) = sExt;
      elevationExtReplace.compute();
      double pIn = g*elevation(0,0), pExt = g*elevationExt(0,0);

      elevationExt_w(0,0)=elevationExt(0,0);
      widthExtReplace.compute();
      double wIn = width(0,0), wExt = widthExt(0,0);

      double F1,F2;
      dgConservationLawShallowWater1dLaxSolver(sIn, sExt, uIn, uExt, wIn, wExt, pIn, pExt, n0, F1, F2);
      val(0,0) = - F1;
      val(0,1) = - F2;
    }
  };
public:
  term  _term;
  forceSol (dgConservationLawShallowWater1d *claw, const function *forcedSection, const function *forcedVelocity) : _term (forcedSection,forcedVelocity,claw->getHydroSolution(), claw->getElevation(), claw->getWidth()) {
    _term0 = &_term;
  }
};
dgBoundaryCondition *dgConservationLawShallowWater1d::newForceSolution(const function *forcedSection, const function *forcedVelocity){
  return new forceSol(this, forcedSection, forcedVelocity);
}

class dgConservationLawShallowWater1d::boundaryCouplingSW2D:public dgBoundaryCondition {
  class term:public function {
    fullMatrix<double> normals, solExt, sol;
    double _width, _section0;
  public:
    term(const function *solExtF, const function *solF, const double width, const double section0):function(2){
      setArgument(solExt, solExtF);
      setArgument(sol, solF);
      setArgument(normals, function::getNormals());
      _width = width;
      _section0 = section0;
    }
    void call (dataCacheMap *map, fullMatrix<double> &val) { 
      double uIn = sol(0,1), uExt = solExt(0,1);
      double vIn = 0, vExt = solExt(0,2);
      double _bath = _section0/_width;
      double HIn = sol(0,0)/_width, HExt =  solExt(0,0)/_width;
      double n0 = normals(0,0);
      double uStar,vStar,HStar, AStar;
      roeSolver(uExt, uIn, vExt, vIn, HExt, HIn, uStar, vStar, HStar, AStar);
      double etaStar = HStar - _bath;
      double ffs =  n0 * uStar * HStar * _width;
      double ffu =  n0 * ( uStar * uStar / 2 + g * etaStar );
      val(0,0) = -ffs;
      val(0,1) = -ffu;
    }
  };
public:
  term  _term;
  boundaryCouplingSW2D (dgConservationLawShallowWater1d *claw, const function *solExtF, double width, double section0): _term(solExtF, claw->getHydroSolution(), width, section0) {
    _term0 = &_term;
  }
};
dgBoundaryCondition *dgConservationLawShallowWater1d::newBoundaryCouplingSW2D(const function *solExtF, double width, double section0){
  return new boundaryCouplingSW2D(this, solExtF, width, section0);
}

class dgConservationLawShallowWater1d::forceUpstreamDischarge:public dgBoundaryCondition {
  class term:public function {
    fullMatrix<double> normals, solExt, sol, width;
    fullMatrix<double> dischargeExt;
    fullMatrix<double> elevation;
    public:
    term(const function *dischargeExtF, const function *solF, const function *elevationF, const function *widthF):function(2){
      setArgument(dischargeExt, dischargeExtF);
      setArgument(sol, solF);
      setArgument(normals, function::getNormals());
      setArgument(elevation, elevationF);
      setArgument(width, widthF);
    }
    void call (dataCacheMap *map, fullMatrix<double> &val) { 
      double sIn = sol(0,0), sExt = sIn;
      double uIn = sol(0,1), uExt = -dischargeExt(0,0)/sIn;
      double n0 = normals(0,0);
      double pIn = g*elevation(0,0), pExt = pIn;
      double wIn = width(0,0), wExt = wIn;
      double F1,F2;
      dgConservationLawShallowWater1dLaxSolver(sIn, sExt, uIn, uExt, wIn, wExt, pIn, pExt, n0, F1, F2);
      val(0,0) = - F1;
      val(0,1) = - F2;
    }
  };
public:
  term  _term;
  forceUpstreamDischarge (dgConservationLawShallowWater1d *claw, const function *forcedDischarge) : _term (forcedDischarge,claw->getHydroSolution(), claw->getElevation(), claw->getWidth()) {
    _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawShallowWater1d::newForceUpstreamDischarge(const function *forcedDischarge){
  return new forceUpstreamDischarge(this, forcedDischarge);
}

//**********************************************
// Bifurcation solver
//**********************************************
class dgConservationLawShallowWater1d::riemannBifurcation:public function {
  fullMatrix<double> normals1, normals2, normals3, sol1, sol2, sol3;
  fullMatrix<double> eta1, eta2, eta3, w1, w2, w3;
  fullMatrix<double> sol1_eta, sol2_eta, sol3_eta, eta1_w, eta2_w, eta3_w ;
  fullMatrix<double> elevation1,elevation2,elevation3;
  fullMatrix<double> width1,width2,width3;
  functionReplace replaceElevation1, replaceElevation2, replaceElevation3, replaceWidth1, replaceWidth2, replaceWidth3;

public:
  riemannBifurcation(const function *hydroSol, const function *elevation, const function *width, const function *widthData):function(6) {
    setArgument (normals1, function::getNormals(), 0);
    setArgument (normals2, function::getNormals(), 1);
    setArgument (normals3, function::getNormals(), 2);
    setArgument (sol1, hydroSol, 0);
    setArgument (sol2, hydroSol, 1);
    setArgument (sol3, hydroSol, 2);
    setArgument (elevation1, elevation, 0);
    setArgument (elevation2, elevation, 1);
    setArgument (elevation3, elevation, 2);
    setArgument (width1, width, 0);
    setArgument (width2, width, 1);
    setArgument (width3, width, 2);

    addFunctionReplace(replaceElevation1);//The elevation depends on the solution
    replaceElevation1.replace(sol1_eta, hydroSol, 0);
    replaceElevation1.get(eta1,elevation, 0);
    addFunctionReplace(replaceWidth1);//The width depends on elevation
    replaceWidth1.replace(eta1_w, elevation, 0);
    replaceWidth1.get(w1,widthData, 0);

    addFunctionReplace(replaceElevation2);//The elevation depends on the solution
    replaceElevation2.replace(sol2_eta, hydroSol, 1);
    replaceElevation2.get(eta2,elevation, 1);
    addFunctionReplace(replaceWidth2);//The width depends on elevation
    replaceWidth2.replace(eta2_w, elevation, 1);
    replaceWidth2.get(w2,widthData, 1);

    addFunctionReplace(replaceElevation3);//The elevation depends on the solution
    replaceElevation3.replace(sol3_eta, hydroSol, 2);
    replaceElevation3.get(eta3,elevation, 2);
    addFunctionReplace(replaceWidth3);//The width depends on elevation
    replaceWidth3.replace(eta3_w, elevation, 2);
    replaceWidth3.get(w3,widthData, 2);
  }

  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    fullVector<double> u(6);
    fullVector<double> ub(6);
    fullVector<double> caracs(3);
    fullVector<double> eta(3);
    fullVector<double> w(3);
    fullVector<double> n(3);
    fullVector<double> du(6);
    fullVector<double> fu(6);
    fullVector<double> fub(6);
    fullMatrix<double> dfdu(6,6);

    u(0)=sol1(0,0); u(1)=sol2(0,0); u(2)=sol3(0,0);
    u(3)=sol1(0,1); u(4)=sol2(0,1); u(5)=sol3(0,1);
    n(0)=normals1(0,0); n(1)=normals2(0,0); n(2)=normals3(0,0);

    sol1_eta(0,0)=u(0); replaceElevation1.compute(); eta(0)=eta1(0,0);
    sol2_eta(0,0)=u(1); replaceElevation2.compute(); eta(1)=eta2(0,0);
    sol3_eta(0,0)=u(2); replaceElevation3.compute(); eta(2)=eta3(0,0);

    eta1_w(0,0)=eta(0); replaceWidth1.compute(); w(0)=w1(0,0);
    eta2_w(0,0)=eta(1); replaceWidth2.compute(); w(1)=w2(0,0);
    eta3_w(0,0)=eta(2); replaceWidth3.compute(); w(2)=w3(0,0);

    caracs(0) = 0.5*(u(0) + n(0)*u(3)*sqrt(u(0)*w(0)/g));
    caracs(1) = 0.5*(u(1) + n(1)*u(4)*sqrt(u(1)*w(1)/g));
    caracs(2) = 0.5*(u(2) + n(2)*u(5)*sqrt(u(2)*w(2)/g));
    
    double maxNorm=1e-8;
    double eps=1e-3;
    double delta[6]={u(0)*eps,u(1)*eps,u(2)*eps,eps,eps,eps};

		int nr;
		for (nr=0; nr < 10; nr++){
		  systemRiemannBifurcation(u, eta, w, caracs, n, fu);
			for(int i=0;i<6;i++){
				u(i)+=delta[i];
				sol1_eta(0,0)=u(0); replaceElevation1.compute(); eta(0)=eta1(0,0);
				sol2_eta(0,0)=u(1); replaceElevation2.compute(); eta(1)=eta2(0,0);
				sol3_eta(0,0)=u(2); replaceElevation3.compute(); eta(2)=eta3(0,0);
				eta1_w(0,0)=eta(0); replaceWidth1.compute(); w(0)=w1(0,0);
				eta2_w(0,0)=eta(1); replaceWidth2.compute(); w(1)=w2(0,0);
				eta3_w(0,0)=eta(2); replaceWidth3.compute(); w(2)=w3(0,0);
				systemRiemannBifurcation(u, eta, w, caracs, n, fub);
				for(int j=0;j<6;j++)
					dfdu.set(j,i,(fub(j)-fu(j))/delta[i]);
				u(i)-=delta[i];
			}
			dfdu.luSolve(fu,du);
			for (int i=0;i<6;i++)
				u(i)-=du(i);
			sol1_eta(0,0)=u(0); replaceElevation1.compute(); eta(0)=eta1(0,0);
			sol2_eta(0,0)=u(1); replaceElevation2.compute(); eta(1)=eta2(0,0);
			sol3_eta(0,0)=u(2); replaceElevation3.compute(); eta(2)=eta3(0,0);
			eta1_w(0,0)=eta(0); replaceWidth1.compute(); w(0)=w1(0,0);
			eta2_w(0,0)=eta(1); replaceWidth2.compute(); w(1)=w2(0,0);
			eta3_w(0,0)=eta(2); replaceWidth3.compute(); w(2)=w3(0,0);
			if(du.norm()<maxNorm) break;
		}
		//	if(nr==10) Msg::Fatal("Newton for bifurcation solver diverges | norm:%e u=%.3f %.3f %.3f eta=%.3f %.3f %.3f %s-%s-%s\n",du.norm(),u(3),u(4),u(5),eta(0),eta(1),eta(2),group1->getPhysicalTag().c_str(),group2->getPhysicalTag().c_str(),group3->getPhysicalTag().c_str());

    val(0,0)=-u(3)*u(0)*n(0);
    val(0,1)=-(.5*u(3)*u(3) + g*eta(0))*n(0);
    val(0,2)=-u(4)*u(1)*n(1);
    val(0,3)=-(.5*u(4)*u(4) + g*eta(1))*n(1);
    val(0,4)=-u(5)*u(2)*n(2);
    val(0,5)=-(.5*u(5)*u(5) + g*eta(2))*n(2);
  }

  void systemRiemannBifurcation(fullVector<double> &u, fullVector<double> &eta, fullVector<double> &w, fullVector<double> &carac, fullVector<double> &n, fullVector<double> &f){
    f(0) = .5*(u(0) + n(0)*u(3)*sqrt(u(0)*w(0)/g)) - carac(0);
    f(1) = .5*(u(1) + n(1)*u(4)*sqrt(u(1)*w(1)/g)) - carac(1);
    f(2) = .5*(u(2) + n(2)*u(5)*sqrt(u(2)*w(2)/g)) - carac(2);
    f(3) = n(0)*u(0)*u(3) + n(1)*u(1)*u(4) + n(2)*u(2)*u(5);
    f(4) = .5*u(3)*u(3) + g*eta(0) - .5*u(4)*u(4) - g*eta(1);
    f(5) = .5*u(3)*u(3) + g*eta(0) - .5*u(5)*u(5) - g*eta(2);
  }
};
//**********************************************
// Setup
//**********************************************
void dgConservationLawShallowWater1d::setElevation() {
  _elevation = new elevationShallow(_hydroSol,_sectionData, _widthData, _elevationInterp);
}
void dgConservationLawShallowWater1d::setWidth(){
  _width = new widthShallow(_widthData,_elevation, _elevationInterp);
}

void dgConservationLawShallowWater1d::setup () {
  _volumeTerm0[""] = new source (_hydroSol, _linearDissipation, _quadraticDissipation,_sourceMass, _sourceMomentum, _diffusivity);
  _volumeTerm1[""] = new advection (_hydroSol, _elevation,_diffusivity);
  _interfaceTerm0[""] = new riemannLAX (_hydroSol,_width, _elevation, _widthData, _diffusivity);
  _bifurcationTerm0[""] = new riemannBifurcation (_hydroSol,_elevation, _width, _widthData);
  if(_useClipToPhysics)
    _clipToPhysics[""] = new clipToPhysics(_hydroSol, _clipSectionValue);
}

dgConservationLawShallowWater1d::~dgConservationLawShallowWater1d () {
  if (_volumeTerm0[""]) delete _volumeTerm0[""];
  if (_volumeTerm1[""]) delete _volumeTerm1[""];
  if (_clipToPhysics[""]) delete _clipToPhysics[""];
  if (_bifurcationTerm0[""]) delete _bifurcationTerm0[""];
  if (_clipToPhysics[""]) delete _clipToPhysics[""];
}
