#include "dgConservationLawShallowWaterTracer2d.h"
#include "dgConservationLawShallowWater2d.h"
#include "dgNeighbourMap.h"
#include "function.h"
#include "Numeric.h" 
#include "assert.h" 
#include "stdlib.h"
#include <string.h>
#include "MEdge.h"
#include "MVertex.h"
#include <algorithm>
#include "Numeric.h"


//*************************************
// Terms of the equation
//***************************************
class dgConservationLawShallowWaterTracer2d::massFactor: public function {
  fullMatrix<double> hydroSol, bath;
public:
  massFactor(const function *hydroSolF, const function *bathF) : function(1) {
    setArgument (hydroSol, hydroSolF);
    setArgument (bath, bathF);
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    for(int i=0; i< val.size1(); i++) {
      val(i,0) = bath(i,0) + hydroSol(i,0);
    }
  }
};

class dgConservationLawShallowWaterTracer2d::gradPsiTerm: public function {
  fullMatrix<double> hydroSol, tracer, diffusivity, bath, movingBathFactor, _xyz, diffusiveFlux;
  double _R;
public:
  gradPsiTerm(const function *hydroSolF, const function *tracerF,  const function *bathF, const function *diffusiveFluxF, const function *movingBathFactorF, double R) : function(2) {
    setArgument (hydroSol, hydroSolF);
    setArgument (bath, bathF);
    setArgument (tracer, tracerF);
    setArgument (diffusiveFlux, diffusiveFluxF);
    setArgument (movingBathFactor, movingBathFactorF);
    setArgument (_xyz, function::getCoordinates());
    _R = R;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    for(int i=0; i< tracer.size1(); i++) {
      double eta=hydroSol(i,0);
      double H = bath(i,0) + eta;
      double u=hydroSol(i,1);
      double v=hydroSol(i,2);
      val(i,0) = H * u * tracer(i,0) + diffusiveFlux(i,0);
      val(i,1) = H * v * tracer(i,0) + diffusiveFlux(i,1);
    }
  }
};

class dgConservationLawShallowWaterTracer2d::diffusiveFlux: public function {
  fullMatrix<double> kappa, hydroSol, bath, tracerGradient;
  public:
  diffusiveFlux (const function *hydroSolF, const function *bathF, const function *kappaF): function(2){
    setArgument(hydroSol,hydroSolF);
    setArgument(bath,bathF);
    setArgument(tracerGradient,function::getSolutionGradient());
    setArgument(kappa,kappaF);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    for(int i = 0; i < hydroSol.size1(); i++) {
      double H = hydroSol(i,0) + bath(i,0);
      val(i,0) = -kappa(i,0) * H * tracerGradient(i,0);
      val(i,1) = -kappa(i,0) * H * tracerGradient(i,1);
    }
  }
};

class dgConservationLawShallowWaterTracer2d::psiTerm: public function {
  fullMatrix<double>  source, hydroSol, bath;
public :
  psiTerm(const function *sourceF, const function *hydroSolF, const function *bathF) : function(1){
    setArgument (hydroSol, hydroSolF);
    setArgument (bath, bathF);
    setArgument (source, sourceF);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for (int i=0; i<source.size1(); i++) {
      double H=hydroSol(i,0) + bath(i,0);
      val (i,0)  =  source(i,0) * H;
    }
  }
};

class dgConservationLawShallowWaterTracer2d::riemannRoe:public function {
  fullMatrix<double> tracerL, tracerR, normals, ip, _xyz;
  fullMatrix<double> hydroSolL, hydroSolR;
  fullMatrix<double> bath, movingBathFactorL, movingBathFactorR;
  double _R, _upwindFactor;
  bool useDiff;
  public:
  riemannRoe (const function *tracerF, const function *hydroSolF, const function *bathF, const function *ipTerm, double R, const function *movingBathFactorF, const double upwindFactorRiemann, bool useDiff):function(2){
    setArgument(tracerL, tracerF, 0);
    setArgument(tracerR, tracerF, 1);
    setArgument(hydroSolL, hydroSolF, 0);
    setArgument(hydroSolR, hydroSolF, 1);
    setArgument(bath, bathF, 0);
    setArgument(normals,function::getNormals(), 0);
    setArgument(movingBathFactorL, movingBathFactorF,0);
    setArgument(movingBathFactorR, movingBathFactorF,1);
    useDiff = false;
    if (ipTerm) {
      useDiff = true;
      setArgument(ip, ipTerm);
    }
    _R = R;
    if(_R>0) {
      setArgument(_xyz, function::getCoordinates());
    }
    _upwindFactor = upwindFactorRiemann;
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    double J = 1.0;
    for(int i=0; i< tracerL.size1(); i++) {
      if(_R > 0)
      {
        double x = _xyz(i,0);
        double y = _xyz(i,1);
        J = 4.0*_R*_R/(4.0*_R*_R+x*x+y*y);
      }
      
      double nx = normals(i,0), ny = normals(i,1);
      double cL = tracerL(i,0), cR = tracerR(i,0);
      double uL = nx*hydroSolL(i,1) + ny*hydroSolL(i,2), uR = nx*hydroSolR(i,1) + ny*hydroSolR(i,2);
      double vL = -ny*hydroSolL(i,1) + nx*hydroSolL(i,2), vR = - ny*hydroSolR(i,1) + nx*hydroSolR(i,2);
      
      double HR = hydroSolR(i,0) + bath(i,0);
      double HL = hydroSolL(i,0) + bath(i,0);
      if(HR<0 || HL<0) printf("in sw2d tracer Riemann:  HR = %e HL = %e\n", HR,HL);
      double u,v,H,Amb;
      roeSolver(uL, uR, vL, vR, HL, HR, u, v, H, Amb, movingBathFactorL(i,0), movingBathFactorR(i,0), _upwindFactor);
      double Feta = -H*u/J/Amb;

      val(i,0) = Feta  * (uL > 0 ? cL : cR) ;
      val(i,1) = -val(i,0);
      
      if(useDiff){
        val(i,0) += H * ip(i,0);
        val(i,1) += H * ip(i,1);
      }
    }
  }
};


//**********************************************
// Boundary conditions
//**********************************************

class dgConservationLawShallowWaterTracer2d::boundaryWall : public dgBoundaryCondition {
  class term : public function {
  public:
    term () : function (1) {
    }
    void call (dataCacheMap *map, fullMatrix<double> &val) { 
      for (int i=0; i<val.size1(); i++){
        val(i,0) = 0;
      }
    }
  };
public:
  term _term;
  boundaryWall(dgConservationLawShallowWaterTracer2d *claw) : _term () {
    _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawShallowWaterTracer2d::newBoundaryWall(){
  return new boundaryWall(this);
}


class dgConservationLawShallowWaterTracer2d::forceTracer:public dgBoundaryCondition {
  class term:public function {
    fullMatrix<double> normals;
    fullMatrix<double> hydroSolExt, hydroSolIn, bath, movingBathFactor;
    fullMatrix<double> tracerIn, tracerExt;
    fullMatrix<double> tracerGradientIn, tracerGradientExt;
    fullMatrix<double> _xyz;
    double _R, _upwindFactor;
  public:
    term(const function *hydroSolExtF, const function *tracerExtF, const function *tracerGradientExtF, const function *hydroSolF, const function *tracerF, const function *tracerGradientF, const function *bathF, const function *movingBathFactorF, double R, double upwindFactor):function(1){
      setArgument(normals, function::getNormals());
      setArgument(hydroSolIn, hydroSolF);
      setArgument(hydroSolExt, hydroSolExtF);
      setArgument(tracerIn, tracerF);
      setArgument(tracerExt, tracerExtF);
      setArgument(bath, bathF);
      setArgument(tracerGradientIn, tracerGradientF);
      setArgument(tracerGradientExt, tracerGradientExtF);
      setArgument(movingBathFactor,movingBathFactorF);
      setArgument(_xyz,function::getCoordinates());
      _R = R;
      _upwindFactor = upwindFactor;
    }
    void call (dataCacheMap *map, fullMatrix<double> &val) { 
      for (int i=0; i<tracerIn.size1(); i++){
        double nx = normals(i,0);
        double ny = normals(i,1);
        double etaIn = hydroSolIn(i,0), etaExt = hydroSolExt(i,0);
        double uIn = nx * hydroSolIn(i,1) + ny * hydroSolIn(i,2);
        double uExt = nx * hydroSolExt(i,1) + ny * hydroSolExt(i,2);
        double vIn = -ny * hydroSolIn(i,1) + nx * hydroSolIn(i,2);
        double vExt = -ny * hydroSolExt(i,1) + nx * hydroSolExt(i,2);
        double cIn = tracerIn(i,0), cExt = tracerExt(i,0);
        double HIn= etaIn + bath(i,0) , HExt = etaExt + bath(i,0);

        if(HIn<0 || HExt<0) printf(" HIn = %e HExt =%e\n", HIn, HExt);
        double u,v,H,Amb;
        roeSolver(uIn, uExt, vIn, vExt, HIn, HExt, u, v, H, Amb, movingBathFactor(i,0), movingBathFactor(i,0), _upwindFactor);

        val(i,0) = H*u * ( uIn > 0 ? cIn : cExt );   
      }
    }
  };
public:
  term  _term;
  forceTracer (dgConservationLawShallowWaterTracer2d *claw, 
  const function *hydroSolExtF, const function *tracerExtF, const function *tracerGradientExtF, const function *hydroSolF, const function *tracerF, const function *tracerGradientF, const function *bathF, const function *movingBathFactorF, double R, double upwindFactor):
  _term (hydroSolExtF, tracerExtF, tracerGradientExtF, hydroSolF, tracerF, tracerGradientF, bathF, movingBathFactorF, R, upwindFactor ){
      _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawShallowWaterTracer2d::newForceTracer(const function *hydroSolExtF, const function *tracerExtF, const function *tracerGradientExtF){
  return new forceTracer(this, hydroSolExtF, tracerExtF, tracerGradientExtF, _hydroSolution, _tracer, function::getSolutionGradient(), _bathymetry, _movingBathFactor, _R, _upwindFactorRiemann);
}

//**********************************************
// Setup
//**********************************************
void dgConservationLawShallowWaterTracer2d::setup () {
  if(_useMovingBathWD){
    _bathymetry = new movingBath(_originalBathymetry, _alphaMovingBathWD);
    _movingBathFactor = new movingBathFactor(_originalBathymetry, _alphaMovingBathWD);
  }

  _diffusivity[""] = _kappa;
  if(_kappa){
    _diffusiveFlux = new diffusiveFlux (_hydroSolution, _bathymetry, _kappa);
    _diffusion = _kappa;
    _ipTerm = dgNewIpTerm(1, _diffusiveFlux, _diffusion);
  }

  _volumeTerm0[""] = new psiTerm(_source, _hydroSolution, _bathymetry);
  _volumeTerm1[""] = new gradPsiTerm (_hydroSolution, _tracer, _bathymetry, _diffusiveFlux,  _movingBathFactor, _R);
  _interfaceTerm0[""] = new riemannRoe (_tracer, _hydroSolution, _bathymetry, _ipTerm, _R,  _movingBathFactor, _upwindFactorRiemann, _useDiff);
  _massFactor[""] = std::make_pair(new massFactor(_hydroSolution, _bathymetry), false);
}

dgConservationLawShallowWaterTracer2d::dgConservationLawShallowWaterTracer2d() : dgConservationLawFunction(1) {
  fzero = new functionConstant(0.);
  fzerov = new functionConstant(std::vector<double>(2, 0.));
  _source = fzero;
  _hydroSolution=NULL;
  _tracer = NULL;
  _kappa = fzero;
  _useDiff = false;
  _ipTerm = NULL;
  _diffusiveFlux = NULL;
  _R = -1.0;

  _alphaMovingBathWD = 0.0;
  _useMovingBathWD = false;
  _bathymetry = NULL;
  _originalBathymetry = NULL;
  _movingBathFactor = new functionConstant(1.);
  _upwindFactorRiemann = -1;
}

dgConservationLawShallowWaterTracer2d::~dgConservationLawShallowWaterTracer2d () {
  delete fzero;
  delete fzerov;
  delete _movingBathFactor;
  if (_volumeTerm0[""]) delete _volumeTerm0[""];
  if (_volumeTerm1[""]) delete _volumeTerm1[""];
  if (_interfaceTerm0[""]) delete _interfaceTerm0[""];
  if (_massFactor.count("") != 0)     delete _massFactor[""].first;
  if (_useMovingBathWD) {
    delete _bathymetry;
  }
}
