%module shallowWater

%{
  #undef HAVE_DLOPEN
  #include "dgConservationLawShallowWater1d.h"
  #include "dgConservationLawShallowWater2d.h"
  #include "dgConservationLawShallowWaterAbsLayer2d.h"
  #include "dgConservationLawShallowWaterTracer1d.h"
  #include "dgConservationLawShallowWaterTracer2d.h"
  #include "slimMovingBathWettingDrying.h"
%}

%import(module="dgpy.dgFunction") "function.h";
%import(module="dgpy.conservationLaw") "dgConservationLaw.h";
%import(module="dgpy.conservationLaw") "dgConservationLawFunction.h";
%import(module="dgpy.timeIntegrator") "dgAssemblerExplicit.h";

%include "dgConservationLawShallowWater1d.h"
%include "dgConservationLawShallowWater2d.h"
%include "dgConservationLawShallowWaterAbsLayer2d.h"
%include "dgConservationLawShallowWaterTracer1d.h"
%include "dgConservationLawShallowWaterTracer2d.h"
%include "slimMovingBathWettingDrying.h"
