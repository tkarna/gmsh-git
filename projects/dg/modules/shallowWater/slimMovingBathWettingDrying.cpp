#include "function.h"
#include "slimMovingBathWettingDrying.h"

movingBathFactor::movingBathFactor(const function *originalBathF, double alphaF):function(1){
  setArgument(sol, function::getSolution());
  setArgument(bath, originalBathF);
  alpha = alphaF;
}
void movingBathFactor::call (dataCacheMap *m, fullMatrix<double> &val) {
  for (int k = 0 ; k < bath.size1(); k++ ){
    double H = bath(k,0)+sol(k,0);
    val(k,0) = 0.5 * H / sqrt( H*H + alpha*alpha ) + 0.5;
  }
}

movingBathFactorGradient::movingBathFactorGradient(const function *originalBathF,  const function *originalBathGradientF, double alphaF):function(2) {
  setArgument(sol, function::getSolution());
  setArgument(solGradient, function::getSolutionGradient());
  setArgument(bath, originalBathF);
  setArgument(bathGradient, originalBathGradientF);
  alpha = alphaF;
}
void movingBathFactorGradient::call (dataCacheMap *m, fullMatrix<double> &val){
  for (int k = 0 ; k < bath.size1(); k++ ){
    double H = bath(k,0) + sol(k,0);
    double dHdx = bathGradient(k,0) + solGradient(k,0);
    double dHdy = bathGradient(k,1) + solGradient(k,1);
    val(k,0) = 0.5 * dHdx / sqrt( H*H + alpha*alpha ) - 0.5 * H*H * dHdx / pow(H*H + alpha*alpha, 3.0/2);
    val(k,1) = 0.5 * dHdy / sqrt( H*H + alpha*alpha ) - 0.5 * H*H * dHdy / pow(H*H + alpha*alpha, 3.0/2);
  }
}

movingBath::movingBath(const function *originalBathF, double alphaF):function(1){
  setArgument(sol, function::getSolution());
  setArgument(bath, originalBathF);
  alpha = alphaF;
}
void movingBath::call (dataCacheMap *m, fullMatrix<double> &val) {
  for (int k = 0 ; k < bath.size1(); k++ ){
    double h = bath(k,0) + sol(k,0);
    val(k,0) = bath(k,0) + 0.5 * ( sqrt( h*h + alpha*alpha ) - h );
  }
}

movingBathGradient::movingBathGradient(const function *originalBathF, const function *originalBathGradientF, double alphaF):function(2){
  setArgument(sol, function::getSolution());
  setArgument(solGradient, function::getSolutionGradient());
  setArgument(bath, originalBathF);
  setArgument(bathGradient, originalBathGradientF);
  alpha = alphaF;
}
void movingBathGradient::call (dataCacheMap *m, fullMatrix<double> &val) {
  for (int k = 0 ; k < bath.size1(); k++ ){
    double H = bath(k,0) + sol(k,0);
    double HGradient_x = bathGradient(k,0) + solGradient(k,0);
    double HGradient_y = bathGradient(k,1) + solGradient(k,1);
    val(k,0) = bathGradient(k,0) + 0.5 * ( H * HGradient_x / sqrt( H*H + alpha*alpha ) - HGradient_x );
    val(k,1) = bathGradient(k,1) + 0.5 * ( H * HGradient_y / sqrt( H*H + alpha*alpha ) - HGradient_y );
  }
}
