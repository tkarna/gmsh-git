#ifndef _DG_CONSERVATION_LAW_SHALLOW_WATER_TRACER_1D_
#define _DG_CONSERVATION_LAW_SHALLOW_WATER_TRACER_1D_
#include "dgConservationLawFunction.h"
#include "dgDofContainer.h"
/**The 1D tracer equation consistent with the dgConservationLawShallowWater1d */
class dgConservationLawShallowWaterTracer1d : public dgConservationLawFunction {
  class massFactor;
  class advection;
  class source;
  class riemannLAX;
	class riemannBifurcation;
  class boundaryWall;
	class forceTracer;
	const function *_source, *_elevation, *_width;
  const function* _hydroSolution;
  const function *_tracer;
  const function *_diffusivity;
  const function *_x1d;
  std::string _typeRiemann;
  bool _zeroFluxBifurcations;
  double _sigmaFactBif;
  public:
  void setup();
  inline void setRiemannSolver(std::string type) {checkNonSetup(); _typeRiemann = type;}

  /**set the function to evaluate the source in the tracer equation */
  inline void setSource(const function *source){checkNonSetup(); _source = source;}
  /**set the hydrodynamic solution function (section, velocity) computed by the corresponding hydrodynamic equations */
  inline void setHydroSolution(const function *hydroSolution){checkNonSetup(); _hydroSolution = hydroSolution;}
  /** set the width computed by the corresponding hydrodynamic equations */
  inline void setWidth(const function *width){checkNonSetup(); _width = width;}
  /**Set the elevation field computed by the corresponding hydrodynamic equations */
  inline void setElevation(const function *elevation){checkNonSetup(); _elevation = elevation;}
  /**set the tracer function to be computed by this law */
  inline void setTracer(const function *tracer){checkNonSetup(); _tracer = tracer;}
  /**set diffusivity used in this advection-diffusion equation */
  inline void setDiffusivity(const function *diffusivity){checkNonSetup(); _diffusivity = diffusivity;}
  /**set river-axis distance */
  inline void setRiverDistance(const function *x1d){_x1d = x1d;}
  /**set 0flux at bifurcation */
  inline void setZeroFluxAtBifurcations(){ _zeroFluxBifurcations= true;}
  /**add a penalty term on the tracer value at the bifurcations, must be positive and used to smooth a field in a river network with constant bathymetry*/
  inline void setSigmaFactorBifurcations(double sigmaFactorBif){_sigmaFactBif=sigmaFactorBif;}

	//boundary conditions
  /**slip wall boundary */
  dgBoundaryCondition *newBoundaryWall();
  /**force the solution (section, velocity and tracer) */
  dgBoundaryCondition *newForceTracer(const function *sectionExtF, const function *velocityExtF, const function *tracerExtF, const function *tracerGradientF);

  dgConservationLawShallowWaterTracer1d();
  ~dgConservationLawShallowWaterTracer1d();
};


#endif
