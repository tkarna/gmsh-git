#ifndef _DG_CONSERVATION_LAW_SHALLOW_WATER_ABSLAYER_2D_
#define _DG_CONSERVATION_LAW_SHALLOW_WATER_ABSLAYER_2D_
#include "dgConservationLawFunction.h"
#include "functionGeneric.h"
#include "function.h"
#include "dgDofContainer.h"


/**The non-conservative shallow water conservation law with absorbing layer. (eta, u, v) */
class dgConservationLawShallowWaterAbsLayer2d : public dgConservationLawFunction {
  
  std::string _LayerVersion;   // 'WithoutLayer',
                               // 'FRS',
                               // 'Lavelle',
                               // 'PmlStraightForPureWave',
                               // 'PmlStraight'
  
  class maxConvectiveSpeed;
  class sourceTerm;
  class sourceTermLayer;
  class gradPsiTerm;
  class gradPsiTermLayer;
  class interfaceTerm;
  class interfaceTermLayer;
  class diffusiveFlux;
  class diffusivitySmagorinsky;
  class interfaceTermUncoupledRegions;
  class boundaryCondition;
  
  /** if this flag is true, a linear version of the equations are solved. */
  bool _isLinear;
  /** a factor to smooth the if in Hv term in the roe riemann solver */
  double _upwindFactorRiemann;
  
  const function *_fzero;
  const function *_bathymetry;
  const function *_bathymetryGradient;
  const function *_coriolisFactor;
  const function *_gravityAcceleration;
  const function *_windStress;
  const function *_bottomFrictionCoef;
  const function *_diffusivity;
  const function *_lengthMeshSmagorinskyDiffusivity;
  const function *_diffusivityVec;
  const function *_diffusiveFlux;
  const function *_ipTerm;
  
  const function *_absLayerCoef;
  const function *_absLayerDir;
  
  static void RoeSolver(double uL, double uR, double vL, double vR, double HL, double HR,
                        double &uStar, double &vStar, double &HStar, double g, double upwindFact);
  
 public:
  
  dgConservationLawShallowWaterAbsLayer2d(std::string="WithoutLayer");
  ~dgConservationLawShallowWaterAbsLayer2d();
  void setup();
  
  std::string getLayerVersion() const {return _LayerVersion;}
  void addLayer(const std::string, const function *f, bool FLAG1=true, bool FLAG2=true);
  void addLayer(const std::string TAG, bool FLAG1=true, bool FLAG2=true) {
    addLayer(TAG, NULL, FLAG1, FLAG2);
  }
  
  // SET
  inline void setIsLinear(bool isLinear)                                      { _isLinear = isLinear; }
  inline void setUpwindFactorRiemann(double upwindFactorRiemann)              { _upwindFactorRiemann = upwindFactorRiemann; }
  inline void setBathymetry(const function *bathymetry)                       { _bathymetry = bathymetry; }
  inline void setBathymetryGradient(const function *bathymetryGradient)       { _bathymetryGradient = bathymetryGradient; }
  inline void setCoriolisFactor(const function *coriolisFactor)               { _coriolisFactor = coriolisFactor; }
  inline void setGravityAcceleration(const function *gravityAcceleration)     { _gravityAcceleration = gravityAcceleration; }
  inline void setWindStress(const function *windStress)                       { _windStress = windStress; }
  inline void setBottomFrictionCoef(const function *bottomFrictionCoef)       { _bottomFrictionCoef = bottomFrictionCoef; }
  inline void setDiffusivity(const function *diffusivity)                     { _diffusivity = diffusivity; };
  inline void setLengthMeshSmagorinskyDiffusivity(const function *lengthMesh) { _lengthMeshSmagorinskyDiffusivity = lengthMesh; };
  inline void setAbsLayerCoef(const function *absLayerCoef)                   { _absLayerCoef = absLayerCoef; }
  inline void setAbsLayerDir(const function *absLayerDir)                     { _absLayerDir = absLayerDir; }
  
  // GET
  virtual bool isLinear() const                   { return _isLinear; }
  inline double getUpwindFactorRiemann()          { return _upwindFactorRiemann; }
  inline const function *getBathymetry()          { return _bathymetry; }
  inline const function *getBathymetryGradient()  { return _bathymetryGradient; }
  inline const function *getCoriolisFactor()      { return _coriolisFactor; }
  inline const function *getGravityAcceleration() { return _gravityAcceleration; }
  inline const function *getWindStress()          { return _windStress; }
  inline const function *getBottomFrictionCoef()  { return _bottomFrictionCoef; }
  inline const function *getDiffusivity()         { return _diffusivity; }
  inline const function *getAbsLayerCoef()        { return _absLayerCoef; }
  inline const function *getAbsLayerDir()         { return _absLayerDir; }
  inline const function *getDepth()               { return functionSumNew(_bathymetry,functionExtractCompNew(function::getSolution(),0)); }
  inline const function *getElevation()           { return functionExtractCompNew(function::getSolution(),0); }
  inline const function *getVelocity() {
    std::vector<int> comp;
    comp.push_back(1), comp.push_back(2);
    return functionExtractCompNew(function::getSolution(),comp);
  }
  
  // Boundary conditions
  dgBoundaryCondition *newBoundaryDirichletOnU(const function *);
  dgBoundaryCondition *newBoundaryFluxU(const function *);
  dgBoundaryCondition *newBoundaryHomogeneousDirichletOnU();
  dgBoundaryCondition *newBoundaryWall();
  dgBoundaryCondition *newBoundaryDirichletOnEta(const function *);
  dgBoundaryCondition *newBoundaryHomogeneousDirichletOnEta();
  dgBoundaryCondition *newBoundaryOpenOutflow();
  dgBoundaryCondition *newBoundaryOpenOutflowWithInflow(const function *);
  
  // Interface conditions
  void addInterfaceContinuity(const std::string);
  void addInterfaceDirichletOnU(const std::string, const function *f);
  void addInterfaceFluxU(const std::string, const function *f);
  void addInterfaceHomogeneousDirichletOnU(const std::string);
  void addInterfaceWall(const std::string);
  void addInterfaceDirichletOnEta(const std::string, const function *f);
  void addInterfaceHomogeneousDirichletOnEta(const std::string);
  void addInterfaceOpenOutflow(const std::string);
  void addInterfaceOpenOutflowWithInflow(const std::string, const function *f);
  void addInterfaceScatteredTotalFields(const std::string, const function *f, const function *fDir=NULL);
  
  // Get additional information
  class energyFunction;
  class energyErrorFunction;
  double getTotalEnergy(dgGroupCollection *groups, dgDofContainer *solutionDof, std::string tag="", int intOrder=-1);
  double getTotalEnergy(dgGroupCollection *groups, function *solutionFunc, std::string tag="", int intOrder=-1);
  double getTotalEnergyError(dgGroupCollection *groups, dgDofContainer *solutionDof, dgDofContainer *solutionRefDof, std::string tag="", int intOrder=-1);
  double getTotalEnergyError(dgGroupCollection *groups, dgDofContainer *solutionDof, function *solutionRefFunc, std::string tag="", int intOrder=-1);
  double getVolume(dgGroupCollection *groups, std::string tag="");
  
};
#endif
