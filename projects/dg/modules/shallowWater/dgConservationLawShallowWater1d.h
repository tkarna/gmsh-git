#ifndef _DG_CONSERVATION_LAW_SHALLOW_WATER_1D_
#define _DG_CONSERVATION_LAW_SHALLOW_WATER_1D_
#include "dgConservationLawFunction.h"
#include "functionGeneric.h"
/**function to compute the flux*/
void dgConservationLawShallowWater1dLaxSolver(double sL, double sR,double uL, double uR, double wL, double wR, double pL, double pR, double n0, double &fs, double &fu);
void dgConservationLawShallowWater1dRoeSolver(double sL, double sR,double uL, double uR, double wL, double wR, double pL, double pR, double n0, double &fs, double &fu);

/**The non-conservative shallow water conservation law. (section, u) */
class dgConservationLawShallowWater1d : public dgConservationLawFunction {
  class advection;
  class source;
  class riemannLAX;
	class riemannBifurcation;
  class clipToPhysics;
  class boundaryWall;
	class forceSol;
  class forceUpstreamDischarge;
  class boundaryCouplingSW2D;
  class elevationShallow;
	class widthShallow;
	class widthDataShallow;
	class sectionDataShallow;
  const function *_hydroSol;
	const function *_linearDissipation, *_quadraticDissipation;
  const function *_sourceMass, *_sourceMomentum;
  const function *_diffusivity;
  const function *_width, *_widthData, *_sectionData, *_elevation, *_elevationInterp;
  const function *_x1d;
  std::string _typeRiemann;
  bool _zeroFluxBifurcations;
  double _clipSectionValue;
  bool _useClipToPhysics;
  public:
  void setup();
  inline void setRiemannSolver(std::string type) {_typeRiemann = type;}

  /**set the solution function */
  inline void setHydroSolution(const function *hydroSol){_hydroSol = hydroSol;}
  /**set the function used for the section of the river */
  inline void setSectionData(const function *sectionData) {_sectionData = sectionData;}
  /**set the function used for the width of the river */
  inline void setWidthData(const function *widthData) {_widthData = widthData;}
  /**set the function used to interpolate the section and the width */
  /**getElevationInterp, necesarry to create the sectionData and widthData functions */
  inline const function* setElevationInterp() {return new functionConstant(0.);};
  inline const function* getElevationInterp() {return _elevationInterp;};
  void setElevation();
  void setWidth();
  /**set the function to evaluate the linear dissipation Kr u */
  inline void setLinearDissipation(const function *linearDissipation){_linearDissipation = linearDissipation;}
  /**set the function to evaluate the quadratic dissipation Kr u |u| */
  inline void setQuadraticDissipation(const function *quadraticDissipation){_quadraticDissipation = quadraticDissipation;}
  /**set diffusivity nu_h */
  inline void setDiffusivity(const function *diffusivity){_diffusivity = diffusivity;}
  /**clip to phisics, the minimum of section, below the section is clipped*/
  inline void clipSection(const double value){
    _clipSectionValue=value;
    _useClipToPhysics=true;
  }
  /**set river-axis distance */
  inline void setRiverDistance(const function *x1d){_x1d = x1d;}

  /**set the function to evaluate the source in the mass equation Sm */
  inline void setSourceMass(const function *sourceMass){_sourceMass = sourceMass;}
  /**set the function to evaluate the source in the momentum equation Sm */
  inline void setSourceMomentum(const function *sourceMomentum){_sourceMomentum = sourceMomentum;}
  inline void setSetup(){checkSetup();}

  inline const function* getSection() {return  functionExtractCompNew(_hydroSol,0);};
  inline const function* getVelocity() {return  functionExtractCompNew(_hydroSol,1);};
  inline const function* getWidth() {return _width;};
  inline const function* getElevation() {return _elevation;};
  inline const function* getHydroSolution() {return _hydroSol;};
	//boundary conditions
  /**slip wall boundary */
  dgBoundaryCondition *newBoundaryWall();
  /**force the solution (section and velocity) */
  dgBoundaryCondition *newForceSolution(const function *forcedSection, const function *forcedVelocity);
  /** force a velocity of -Q/S with Q the forcedDischarge and S the inner section*/
  dgBoundaryCondition *newForceUpstreamDischarge(const function *forcedDischarge);
  /** Force the coupling using the 2D roe solver and a regular channel with vertical walls*/
  dgBoundaryCondition *newBoundaryCouplingSW2D(const function *solExtF, double width, double section0);

  dgConservationLawShallowWater1d();
  ~dgConservationLawShallowWater1d();
};
#endif
