#ifndef _DG_CONSERVATION_LAW_SHALLOW_WATER_TRACER_2D_
#define _DG_CONSERVATION_LAW_SHALLOW_WATER_TRACER_2D_
#include "dgConservationLawFunction.h"
#include "dgDofContainer.h"
/**The 1D tracer equation consistent with the dgConservationLawShallowWater2d */
class dgConservationLawShallowWaterTracer2d : public dgConservationLawFunction {
  class massFactor;
  class gradPsiTerm;
  class psiTerm;
  class riemannRoe;
  class boundaryWall;
	class forceTracer;
  class diffusiveFlux;
  const function *_kappa;
  const function *_diffusion;
	const function *_source;
  const function *_hydroSolution;
  const function *_tracer;
  const function *_diffusiveFlux;
  const function *_ipTerm;
  const function *_movingBathFactor;
  const function *fzero, *fzerov;
  double _R;
  function *_bathymetry, *_originalBathymetry;
  double _upwindFactorRiemann;
  bool _useDiff;
  bool _useMovingBathWD;
  double _alphaMovingBathWD;

  public:
  void setup();

  /**set the function to evaluate the source in the tracer equation */
  inline void setSource(const function *source1){_source = source1;}
  /**set the hydrodynamic solution function (section, velocity) computed by the corresponding hydrodynamic equations */
  inline void setHydroSolution(const function *hydroSolution){_hydroSolution = hydroSolution;}
  /**set the tracer function to be computed by this law */
  inline void setTracer(const function *tracer){_tracer = tracer;}
  /**set diffusivity used in this advection-diffusion equation */
  inline void setDiffusivity(const function *kappa){_useDiff = true; _kappa = kappa;}
  inline void setIsSpherical(double R) {_R = R;}
  inline void setBathymetry(function *bathymetry) { _bathymetry= bathymetry; }
  /**set the function to evaluate the diffusion term */
  inline void setUpwindFactorRiemann(double upwindFactorRiemann){
    _upwindFactorRiemann=upwindFactorRiemann;
  }
  inline void setMovingBathWettingDrying(double alphaMovingBathWD){
    _useMovingBathWD=true;
    _alphaMovingBathWD = alphaMovingBathWD;
    _originalBathymetry = _bathymetry;
  }

	//boundary conditions
  /**slip wall boundary */
  dgBoundaryCondition *newBoundaryWall();
  /**force the solution (section, velocity and tracer) */
  dgBoundaryCondition *newForceTracer(const function *hydroSolExtF, const function *tracerExtF, const function *tracerGradientExtF);

  dgConservationLawShallowWaterTracer2d();
  ~dgConservationLawShallowWaterTracer2d();
};


#endif
