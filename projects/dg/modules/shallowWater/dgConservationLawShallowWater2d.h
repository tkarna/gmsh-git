#ifndef _DG_CONSERVATION_LAW_SHALLOW_WATER_2D_
#define _DG_CONSERVATION_LAW_SHALLOW_WATER_2D_
#include "dgConservationLawFunction.h"
#include "functionGeneric.h"
#include "function.h"
#include "dgDofContainer.h"
#include "slimMovingBathWettingDrying.h"

void roeSolver(double uL, double uR, double vL, double vR, double HL, double HR,
               double &uStar, double &vStar, double &HStar, double &AStar,
               double mbFactL=1.0, double mbFactR=1.0 , double upwindFact=-1.0);

/**The non-conservative shallow water conservation law. (eta, u, v) */
class dgConservationLawShallowWater2d : public dgConservationLawFunction {
  
  //class massFactor;
  class clipToPhysics;
  class maxDiffusiveSpeed;
  class maxConvectiveSpeed;
  class gradPsiTerm;
  class source;
  class diffusiveFlux;
  class diffusivity;
  class riemann;
  
  class boundaryWall;
  class boundaryForcedDischarge;
  class boundaryCouplingSW1D;
  
  bool _linear, _constantJac;
  bool _useMovingBathWD;
  double _alphaMovingBathWD;
  double _upwindFactorRiemann;
  double _R;
  
  const function *_bathymetry, *_bathymetryGradient;
  const function *_originalBathymetry, *_originalBathymetryGradient;
  const function *_linearDissipation, *_quadraticDissipation, *_source, *_coriolisFactor, *_coordinatesF;
  const function *_movingBathFactor, *_movingBathFactorGradient;
  const function *_fzero, *_fzerov, *_nu, *_diffusiveFlux, *_diffusion, *_ipTerm, *_xyz;

  const function *_sourceTerm, *_sourceTermLin, *_gradPsiTerm, *_gradPsiTermLin,
    *_riemannTerm, *_riemannTermLin, *_maxSpeed, *_maxSpeedFilt;
  imexMode _imexMode;
  bool _linearIMEX, _constantJacIMEX, _linearFilter;
 public:
  
  std::map<int, bool> dryFlag;
  
  dgConservationLawShallowWater2d();
  ~dgConservationLawShallowWater2d();
  void setup();
  
  /**if this flag is true, a linear version of the equations are solved.
   * no quadratic dissipation, no advection, no non-linear free surface term in mass conservation */
  inline void setIsLinear(bool linear) {_linear = linear; _constantJac = linear;}
  virtual void setImexMode(imexMode mode);
  void setLinearFilterMode(bool set);
  virtual bool isLinear() const {return (_linear || _linearIMEX);}
  virtual bool isConstantJac() const {return (_constantJac || _constantJacIMEX);}
  /**if this flag is true, a spherical version of the equations are solved*/
  inline void setIsSpherical(double R) {_R = R;}
  inline double getRadius() {return _R;}
  void setCoordinatesFunction(function *xyz);
  
  /**set the function to evaluate the coriolis factor \f{eqnarray*} \frac{du}{dt} &= &-f v \\ \frac{dv}{dt} &=& f u\f} */
  inline void setCoriolisFactor(const function *coriolisFactor) { _coriolisFactor = coriolisFactor;}
  /**set the function to evaluate the linear dissipation \f{eqnarray*}\frac{du}{dt} &=& -\gamma u\\ \frac {dv}{dt} &=& -\gamma v \f}*/
  inline void setLinearDissipation(const function *linearDissipation) { _linearDissipation = linearDissipation;}
  /**set the function to evaluate the quadratic dissipation \f{eqnarray*}\frac{du}{dt} &=& -c_d u||u||\\ \frac{dv}{dt}&= &-c_d v \f}*/
  inline void setQuadraticDissipation(const function *quadraticDissipation) { _quadraticDissipation = quadraticDissipation;}
  /**set the function to evaluate the source term \f{eqnarray*}\frac{du}{dt} &=& s(0)\\ \frac{dv}{dt} &=& s(1)\f} */
  inline void setSource(const function *source) { _source = source;}
  /**set the function to evaluate the bathymetry h (H = h+eta) */
  inline void setBathymetry(function *bathymetry) { _bathymetry= bathymetry; }
  /**set the function to evaluate the gradient of the bathymetry: grad(h) */
  inline void setBathymetryGradient(function *bathymetryGradient) { _bathymetryGradient = bathymetryGradient;}
  /**set the function to evaluate the diffusion term */
  inline void setDiffusivity(const function *nuFunction) { _nu = nuFunction;}
  /**use the moving bath WD (Karna et al, 2011) and define the bathymetry and the moving bathymetry */
  inline void setMovingBathWettingDrying(double alphaMovingBathWD) {
    _useMovingBathWD = true;
    _alphaMovingBathWD = alphaMovingBathWD;
    _originalBathymetry = _bathymetry;
    _originalBathymetryGradient= _bathymetryGradient;
  }
  /** a factor to smooth the if in Hv term in the roe riemann solver */
  inline void setUpwindFactorRiemann(double upwindFactorRiemann) {
    _upwindFactorRiemann=upwindFactorRiemann;
  }
  
  inline const function *getDepth() {
    return functionSumNew(_bathymetry,functionExtractCompNew(function::getSolution(),0));
  }
  inline const function *getBathymetry() {
    if(_useMovingBathWD)
      return new movingBath(_originalBathymetry,_alphaMovingBathWD);
    else
      return _bathymetry;
  }
  inline const function *getElevation() {
    return functionExtractCompNew(function::getSolution(),0);
  }
  inline const function *getVelocity() {
    std::vector<int> comp;
    comp.push_back(1), comp.push_back(2);
    return functionExtractCompNew(function::getSolution(),comp);
  }
  
  // Boundary conditions
  
  /**slip wall boundary */
  dgBoundaryCondition *newBoundaryWall();
  dgBoundaryCondition *newForcedDischarge(dgDofContainer *solution, const function *discharge, std::string tag);
  dgBoundaryCondition *newBoundaryCouplingSW1D(const function *solExtF);
};
#endif
