#include "dgConservationLawShallowWater2d.h"
#include "dgGroupOfElements.h"
#include "dgDofContainer.h"
#include "functionGeneric.h"
#include "float.h"
#include "dgFunctionIntegratorInterface.h"
#include "dgMeshJacobian.h"
#include "slimMovingBathWettingDrying.h"

static double g = 9.80616;



/*==============================================================================
 * DG-terms
 *============================================================================*/

class dgConservationLawShallowWater2d::clipToPhysics : public function {
  fullMatrix<double> sol, bathymetry;
  double _hThreshold;
 public:
  clipToPhysics(const function *bathymetryF, double hMin) : function(3) {
    setArgument (sol, function::getSolution());
    setArgument (bathymetry, bathymetryF);
    _hThreshold = hMin;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    const size_t nQP = val.size1();
    for (size_t k=0; k<nQP; k++){
      double u = sol(k,1);
      double v = sol(k,2);
      val(k,0) = sol(k,0);
      val(k,1) = u;
      val(k,2) = v;
      //val(k,1) = u * fu;
      //val(k,2) = v * fu;
      if (!((sol(k,0)+bathymetry(k,0))>0.05)) {
        Msg::Info("Solution Clipped, H_%d=%f",k, sol(k,0)+bathymetry(k,0));
        val(k,0) = -bathymetry(k,0)+0.05;
      }
    }
  }
  /*void call (dataCacheMap *m, fullMatrix<double> &val) {
    const size_t nQP = val.size1();
    double meanH=0;
    double minH = DBL_MAX;
    bool elementDry = false;
    int highestBathK = 0;
    int highestEtaK = 0;
    int highestHK = 0;
    for (size_t k = 0 ; k < nQP; k++ ){
      val(k,0) = sol(k,0) + bathymetry(k,0);
      if (bathymetry(k,0) < bathymetry(highestBathK,0))
        highestBathK = k;
      if (sol(k,0) > sol(highestEtaK,0))
        highestEtaK = k;
      if (sol(k,0) + bathymetry(k,0) > sol(highestHK,0)  + bathymetry(highestHK,0))
        highestHK = k;
      meanH += val (k,0);
      minH = std::min (minH, val (k,0));
    }
    meanH /= nQP;
    bool isDambreak = sol(highestHK,0) + bathymetry(highestBathK,0) - _hThreshold > 0;
    // from bunya paper
    elementDry = dryMap[m->getElement()] ?
        meanH < _hThreshold || !isDambreak :
        meanH < _hThreshold;
//     if(dryMap[cacheMap->getElement()] == 1 && elementDry == 0)
//       printf("element %p DRY->WET\n",cacheMap->getElement());
//     if(dryMap[cacheMap->getElement()] == 0 && elementDry == 1)
//       printf("element %p      WET->DRY\n",cacheMap->getElement());
    dryMap[m->getElement()] = elementDry;
    //     if(meanH<_hThreshold) Msg::Warning("meanH = %f\n",meanH);
    double _hLimiter = 0.5*_hThreshold;
    if ( minH < _hLimiter) {
      double alpha = std::max((_hLimiter - meanH) / (minH - meanH), 0.);
      for (size_t k = 0 ; k < nQP; k++) {
        double h = bathymetry(k,0);
        val (k,0) = meanH + (val(k,0) - meanH)*alpha - h;
        val (k,1) = isDambreak ? sol (k,1) : 0; // U needed for flooding
        val (k,2) = isDambreak ? sol (k,2) : 0;
      }
    } else {
      for (size_t k = 0 ; k < nQP; k++) {
        val (k,0) = sol(k,0);
        val(k,1) = sol(k,1);
        val(k,2) = sol(k,2);
      }
    }
  }*/
};

class dgConservationLawShallowWater2d::maxDiffusiveSpeed: public function {
  fullMatrix<double> nu;
 public:
/*  maxConvectiveSpeed (const function *nuF):function(3){
    setArgument(nu, nuF);
  }*/
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    /*for(int i=0; i< val.size1(); i++) {
      val(i,0) = sqrt(bath(i,0)*g);
    }*/
  }
};

class dgConservationLawShallowWater2d::maxConvectiveSpeed: public function {
  fullMatrix<double> bath, sol, _xyz;
  bool _linear, _filterLinear;
  double _R;
 public:
  maxConvectiveSpeed (const function *bathymetry, bool linear, double R, bool filterLinear):function(1){
    setArgument(bath,bathymetry);
    setArgument(sol,function::getSolution());
    _linear = linear;
    _filterLinear = filterLinear;
    _R = R;
    if(_R>0) {
      setArgument(_xyz, function::getCoordinates());
    }
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    if (_linear){
      if (_filterLinear)
        val.setAll(0);
      else
        for (int i=0; i<val.size1(); i++)
          val(i,0) = sqrt(bath(i,0)*g);
    }else
      if (!_filterLinear)
        for (int i=0; i<val.size1(); i++)
          val(i,0) = sqrt((bath(i,0)+sol(i,0))*g)+hypot(sol(i,1),sol(i,2));
      else
        for (int i=0; i<val.size1(); i++)
          val(i,0) = sqrt(sol(i,0)*g)+hypot(sol(i,1),sol(i,2));  
    /*if (_R>0)
      for (int i=0; i<val.size1(); i++) {
      double x = _xyz(i,0);
      double y = _xyz(i,1);
      double J = 4.0*_R*_R/(4.0*_R*_R+x*x+y*y);
      val(i,0) /= J;
      }*/
  }
};

class dgConservationLawShallowWater2d::gradPsiTerm: public function {
  fullMatrix<double> bath, sol, diff, _xyz, movingBathFactor;
  bool _linear, _isDiffusive;
  double _R;
  public:
  gradPsiTerm (const function *bathymetryF, const function *diffusiveFluxF, bool linear, double R, const function *movingBathFactorF, const function *xyz):function(9){
    setArgument(bath,bathymetryF);
    setArgument(sol,function::getSolution());
    _isDiffusive = diffusiveFluxF;
    if (_isDiffusive) {
      setArgument(diff,diffusiveFluxF);
    }
    _linear = linear;
    _R = R;
    if(_R>0) {
      setArgument(_xyz, xyz ? xyz : function::getCoordinates());
    }
    setArgument(movingBathFactor, movingBathFactorF);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();

    double wd_g = g;
    double J = 1.0;
    for(size_t i=0; i< nQP; i++) {
      if (_R > 0){
        double x = _xyz(i,0);
        double y = _xyz(i,1);
        J = 4.0*_R*_R/(4.0*_R*_R+x*x+y*y);
	J = 1.0;
      }
      double H = bath(i,0);
      double eta = sol(i,0);
      double u = sol(i,1);
      double v = sol(i,2);
      double A = movingBathFactor(i,0);//equals 1 if no WD
      if (!_linear)
        H+=eta;
      // flux_x
      val(i,0) = H*u * J/A;
      val(i,1) = wd_g*eta*J;
      val(i,2) = 0;
      // flux_y
      val(i,3) = H*v * J/A;
      val(i,4) = 0;
      val(i,5) = wd_g*eta*J;
      if (!_linear) {
        val(i,1) += u*u*J;
        val(i,2) += u*v*J;
        val(i,4) += v*u*J;
        val(i,5) += v*v*J;
      }
      if (_isDiffusive) {
        val(i,1) += diff(i,1);
        val(i,2) += diff(i,2);
        val(i,4) += diff(i,4);
        val(i,5) += diff(i,5);
      }
      // flux_z
      val(i,6) = 0;
      val(i,7) = 0;
      val(i,8) = 0;
    }
  }
};


class dgConservationLawShallowWater2d::source: public function {
  fullMatrix<double> sol, solGradient, coriolisFactor, linearDissipation, quadraticDissipation, _source, _nu, _xyz;
  fullMatrix<double> _bathymetry, _bathymetryGradient, _movingBathFactor, _movingBathFactorGradient;
  bool _linear, _isDiffusive, _useMovingBathWD;
  double _R;
  public :
  source(const function *linearDissipationF, const function *quadraticDissipationF, const function *coriolisFactorF, const function *sourceF, 
         const function *bathymetryF, const function *bathymetryGradientF, const function *nuF, bool linear, double R, 
         const function *movingBathFactorF, const function *movingBathFactorGradientF, const function *xyz) :function (3){
    _linear = linear;
    _R = R;
    _isDiffusive = nuF;
    _useMovingBathWD = movingBathFactorF;

    if(nuF && bathymetryF && !bathymetryGradientF){
      Msg::Fatal("The gradient of the bathymetry is missing since we want to use diffusion with a bathymetry");
    }
    setArgument(sol,function::getSolution());
    setArgument(coriolisFactor,coriolisFactorF);
    setArgument(linearDissipation,linearDissipationF);
    setArgument(quadraticDissipation,quadraticDissipationF);
    setArgument(_source,sourceF);
    setArgument(_bathymetry,bathymetryF);
    if(_isDiffusive || !_linear){
      setArgument(solGradient,function::getSolutionGradient());
    }
    if(_isDiffusive){
      setArgument(_nu,nuF);
      setArgument(_bathymetryGradient,bathymetryGradientF);
    }
    if(_R>0)
      setArgument(_xyz, xyz ? xyz : function::getCoordinates());
    if (_useMovingBathWD) {
      setArgument(_movingBathFactor, movingBathFactorF);
      setArgument(_movingBathFactorGradient, movingBathFactorGradientF);
    }

  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP =val.size1();
    //printf("%s\n",(_R>0)?"true":"false");
    double J = 1.0;
    for(size_t i=0; i< nQP; i++) {
      if(_R > 0){
        double x = _xyz(i,0);
        double y = _xyz(i,1);
        J = 4.0*_R*_R/(4.0*_R*_R+x*x+y*y);
	J = 1.0;
      }
      double eta = sol(i,0);
      double u = sol(i,1);
      double v = sol(i,2);
      val (i,0) = 0;//1/_R/_R*(_xyz(i,0)*u + _xyz(i,1)*v);//0.;
      val (i,1) = coriolisFactor(i,0)*v - linearDissipation(i,0) * u + _source(i,0);
      val (i,2) = -coriolisFactor(i,0)*u - linearDissipation(i,0) * v + _source(i,1);
      if (_R>0) {
        val (i,1) -= g*(eta)*_xyz(i,0)/2.0/_R/_R;
        val (i,2) -= g*(eta)*_xyz(i,1)/2.0/_R/_R;
      }
      if(_isDiffusive){
        double H = _bathymetry(i,0);
        double dHdx = _bathymetryGradient(i, 0);
        double dHdy = _bathymetryGradient(i, 1);
        if (!_linear) {
          H += sol(i,0);
          dHdx += solGradient(i,0);
          dHdy += solGradient(i,1);
        }
        val(i,1) += _nu(i, 0)/H*(dHdx*solGradient(i, 3)+dHdy*solGradient(i, 4));
        val(i,2) += _nu(i, 0)/H*(dHdx*solGradient(i, 6)+dHdy*solGradient(i, 7));
      }
      if(!_linear){
        double div = solGradient(i,3) + solGradient(i,7);
        double bottomFriction = hypot(u,v)*quadraticDissipation(i,0);

        val (i,1) +=   u*(- bottomFriction + div/J);
        val (i,2) +=   v*(- bottomFriction + div/J);
        if (_R > 0) {
          val(i,1)+= (-_xyz(i,0)*(u*u+v*v))/(2.0*_R*_R);
          val(i,2)+= (-_xyz(i,1)*(u*u+v*v))/(2.0*_R*_R);   
        }
      }
      if(_useMovingBathWD){
        double H = _bathymetry(i,0) + eta;
        double A = _movingBathFactor(i,0);
        double dAdx = _movingBathFactorGradient(i,0);
        double dAdy = _movingBathFactorGradient(i,1);
        val(i,0) += - H / ( A*A ) * ( dAdx * u + dAdy * v);
      }
      val(i,0) *= J*J;
      val(i,1) *= J*J;
      val(i,2) *= J*J;
    }
  }
};

class massFactor: public function {
  fullMatrix<double> _xyz;
  double _R;
 public :
  massFactor(double R, const function *xyz) : function(3) {
    _R = R;
    setArgument(_xyz, xyz ? xyz : function::getCoordinates());
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    int nQP=val.size1();
    for (int i=0; i<nQP; i++) {
      double x = _xyz(i,0);
      double y = _xyz(i,1);
      double J = 4.0*_R*_R/(4.0*_R*_R+x*x+y*y);
      val(i,0) = J*J;
      val(i,1) = J*J;
      val(i,2) = J*J;
    }
  }
};

class dgConservationLawShallowWater2d::diffusiveFlux : public function {
  fullMatrix<double> solGrad, nu;
 public:
  diffusiveFlux(const function *nuF) : function(6) {
    setArgument(solGrad,function::getSolutionGradient());
    setArgument(nu,nuF);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      val(i,0) = 0;
      val(i,1) = -nu(i,0)*solGrad(i,3);
      val(i,2) = -nu(i,0)*solGrad(i,6);
      val(i,3) = 0;
      val(i,4) = -nu(i,0)*solGrad(i,4);
      val(i,5) = -nu(i,0)*solGrad(i,7);
    }
  }
};

class dgConservationLawShallowWater2d::diffusivity : public function {
  fullMatrix<double> nu;
 public:
  diffusivity(const function *nuF) : function(3) {
    setArgument(nu,nuF);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      val(i,0) = 0;
      val(i,1) = nu(i,0);
      val(i,2) = nu(i,0);
    }
  }
};

class dgConservationLawShallowWater2d::riemann:public function {
  fullMatrix<double> solL, solR, normals, ip, _xyz;
  fullMatrix<double> bath, movingBathFactorL, movingBathFactorR;
  bool _linear, _isDiffusive;
  double _R, _upwindFactor;
  public:
  riemann (const function *bathymetry, const function *ipTerm, bool linear, double R, const function *movingBathFactorF, const double upwindFactorRiemann, const function *xyz): function(3){
    setArgument(solL,function::getSolution(), 0);
    setArgument(solR,function::getSolution(), 1);
    setArgument(bath,bathymetry, 0);
    setArgument(normals,function::getNormals(), 0);
    setArgument(movingBathFactorL, movingBathFactorF,0);
    setArgument(movingBathFactorR, movingBathFactorF,1);
    _isDiffusive = ipTerm;
    if (_isDiffusive) {
      setArgument(ip, ipTerm);
    }
    _linear = linear;
    _R = R;
    if(_R>0) {
      setArgument(_xyz, xyz ? xyz : function::getCoordinates());
    }
    _upwindFactor = upwindFactorRiemann;
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = solL.size1();
    int rev = 1;
    double J = 1.0;
    for(size_t i=0; i< nQP; i++) {
      rev = 1;
      if(_R > 0)
      {
        double x = _xyz(i,0);
        double y = _xyz(i,1);
        J = 4.0*_R*_R/(4.0*_R*_R+x*x+y*y);
	J = 1.0;
        const dgGroupOfElements *groupR = m->getGroupOfElements();
        const dgGroupOfElements *groupL = m->getSecondaryCache(1)->getGroupOfElements();
        if(groupR->getPhysicalTag() != groupL->getPhysicalTag())
          rev = -1;
      }
      
      double Fun, Fut, Feta;
      double nx = normals(i,0);
      double ny = normals(i,1);
/*      if(rev<0)
      {
        double RR = sqrt(_xyz.get(i,0)*_xyz.get(i,0) + _xyz.get(i,1)*_xyz.get(i,1));
        printf("nx = %.16g nx2 = %.16g, ny = %.16g ny2 = %.16g, 2R = %.16g , RR = %.16g\n ", nx,_xyz.get(i,0)/RR,ny,_xyz.get(i,1)/RR, 2*_R, RR);
}*/
      double uL = nx*solL(i,1) + ny*solL(i,2), uR = nx*solR(i,1) + ny*solR(i,2);
      double vL = -ny*solL(i,1) + nx*solL(i,2), vR = - ny*solR(i,1) + nx*solR(i,2);
      
      double h = bath(i,0);
      
      uR *= rev;
      
      if (_linear) {
        //linear equations 
        double etaR = solR(i,0);
        double etaL = solL(i,0);
        double sq_g_h = sqrt(g/h);
        double etaRiemann = (etaL+etaR + (uL-uR)/sq_g_h)/2;
        double unRiemann = (uL+uR + (etaL-etaR)*sq_g_h)/2;
        Fun = -g*etaRiemann*J;
        Fut = 0;
        Feta = -(h)*unRiemann*J;
      }else{
        double HR = solR(i,0) + h, HL = solL(i,0) + h;
        if(HR<0 || HL<0) printf(" HR = %e HL =%e\n", HR,HL);
        double u,v,H,Amb;
        roeSolver(uL, uR, vL, vR, HL, HR, u, v, H, Amb, movingBathFactorL(i,0), movingBathFactorR(i,0), _upwindFactor);
        double eta  = H-h;
        Fun = -g*eta*J - u*u*J;
        Fut = -u*v*J;
        Feta = -H*u*J/Amb;
      }

      val(i,0) = Feta;
      val(i,1) = Fun * nx - Fut * ny;
      val(i,2) = Fun * ny + Fut * nx;
      /*val(i,3) = -val(i,0);
      val(i,4) = - Fun * nx * rev + Fut * ny;
      val(i,5) = - Fun * ny * rev - Fut * nx;*/
      
      //printf("%f\n", val(i,0));

      if (_isDiffusive) {
        val(i,1)+=ip(i,1);
        val(i,2)+=ip(i,2);
        /*val(i,4)+=ip(i,4)*rev;
        val(i,5)+=ip(i,5)*rev;*/
      }
    }
  }
};

void roeSolver(double uL, double uR, double vL, double vR, double HL, double HR,
               double &uStar, double &vStar, double &HStar, double &AStar,
               double mbFactL, double mbFactR , double upwindFact){
  double HuL = uL*HL, HuR = uR*HR;
  double HvL = vL*HL, HvR = vR*HR;
  double HM = (HL+HR)/2, HJ = (HL-HR)/2;
  double HuM = (HuL+HuR)/2, HuJ = (HuL-HuR)/2;
  double sqHL = sqrt(HL), sqHR = sqrt(HR);
  double u_roe = (sqHL*uL + sqHR*uR) / (sqHL + sqHR);
  double v_roe = (sqHL*vL + sqHR*vR) / (sqHL + sqHR);
  double c_roe = sqrt(g*HM);
  double Hu = HuM + (c_roe - u_roe*u_roe/c_roe) *HJ + u_roe/c_roe *HuJ;
  double Hv;
  if (upwindFact>0) {
    double upwindFactor=atan(u_roe/100*upwindFact)/M_PI+0.5;
    Hv = -v_roe*u_roe/c_roe*HJ + v_roe/c_roe*HuJ + upwindFactor *( -v_roe*HJ+HvL) + (1-upwindFactor)*(v_roe*HJ+HvR);
  }
  else
    Hv= -v_roe*u_roe/c_roe*HJ + v_roe/c_roe*HuJ + (u_roe>0 ? -v_roe*HJ+HvL : v_roe*HJ+HvR);
  HStar = HM + (HuJ - u_roe *HJ) / c_roe;
  uStar = Hu / HStar;
  vStar = Hv / HStar;
  AStar = (sqHL * mbFactL + sqHR * mbFactL)/(sqHL + sqHR); 
}
	
void dgConservationLawShallowWater2d::setCoordinatesFunction(function *xyz){
  _xyz=xyz;
}


/*==============================================================================
 * Conservation law : constructor - setup - destructor
 *============================================================================*/

dgConservationLawShallowWater2d::dgConservationLawShallowWater2d() : dgConservationLawFunction(3) {
  // eta u v
  _fzero = new functionConstant(0.);
  _fzerov = new functionConstant(std::vector<double>(2,0));
  _bathymetry = NULL;
  _bathymetryGradient = NULL;
  _originalBathymetry = NULL;
  _originalBathymetryGradient = NULL;
  _linearDissipation = _fzero;
  _coriolisFactor = _fzero;
  _quadraticDissipation = _fzero;
  _source = _fzerov;
  _linear = false;
  _constantJac = false;
  _coordinatesF = NULL;
  _R = -1.0;
  _nu = NULL;
  _useMovingBathWD = false;
  _movingBathFactor = new functionConstant(1.);
  _movingBathFactorGradient = _fzerov;
  _upwindFactorRiemann = -1;
  _ipTerm = NULL;
  _diffusiveFlux = NULL;
  _diffusion = NULL;
  _imexMode = IMEX_ALL;
  _linearIMEX=false;
  _constantJacIMEX=false;
  _linearFilter=false;
}

void dgConservationLawShallowWater2d::setImexMode(imexMode mode) {
  if (mode == IMEX_IMPLICIT){
    _volumeTerm0[""] = _sourceTermLin;
    _volumeTerm1[""]=_gradPsiTermLin;
    _interfaceTerm0[""] = _riemannTermLin;
    _linearIMEX=true;
    _constantJacIMEX=true;
  }else{
    _volumeTerm0[""] =_sourceTerm;
    _volumeTerm1[""]=_gradPsiTerm;
    _interfaceTerm0[""] = _riemannTerm;
    _linearIMEX=false;
    _constantJacIMEX=false;
  }
  _imexMode = mode;
}

void dgConservationLawShallowWater2d::setLinearFilterMode(bool set) {
  if (set)
    _maximumConvectiveSpeed[""] = _maxSpeedFilt;
  else
    _maximumConvectiveSpeed[""] = _maxSpeed;
  _linearFilter=set;
}

void dgConservationLawShallowWater2d::setup() {
  if (_useMovingBathWD) {
    _bathymetry = new movingBath(_originalBathymetry, _alphaMovingBathWD);
    _bathymetryGradient = new movingBathGradient(_originalBathymetry, _originalBathymetryGradient, _alphaMovingBathWD);
    _movingBathFactor = new movingBathFactor(_originalBathymetry, _alphaMovingBathWD);
    _movingBathFactorGradient = new movingBathFactorGradient(_originalBathymetry,_originalBathymetryGradient, _alphaMovingBathWD);
  }
  //_volumeTerm0[""] = new source(_linearDissipation, _quadraticDissipation, _coriolisFactor, _source, _linear);
  
  _sourceTerm = new source(_linearDissipation, _quadraticDissipation, _coriolisFactor, _source, _bathymetry, _bathymetryGradient, _nu, false || _linear, _R, _movingBathFactor, _movingBathFactorGradient, _xyz);
  _gradPsiTerm = new gradPsiTerm(_bathymetry, _diffusiveFlux, false || _linear,_R, _movingBathFactor, _xyz);
  _riemannTerm = new riemann(_bathymetry, _ipTerm, false || _linear, _R, _movingBathFactor, _upwindFactorRiemann, _xyz);
  _maxSpeed = new maxConvectiveSpeed(_bathymetry, _linear, _R, false);
  _sourceTermLin = new source(_linearDissipation, _quadraticDissipation, _coriolisFactor, _source, _bathymetry, _bathymetryGradient, _nu, true, _R, _movingBathFactor, _movingBathFactorGradient, _xyz);
  _gradPsiTermLin = new gradPsiTerm(_bathymetry, _diffusiveFlux, true,_R, _movingBathFactor, _xyz);
  _riemannTermLin = new riemann(_bathymetry, _ipTerm, true, _R, _movingBathFactor, _upwindFactorRiemann, _xyz);
  _maxSpeedFilt = new maxConvectiveSpeed(_bathymetry, _linear, _R, true);
  setImexMode(_imexMode);
  setLinearFilterMode(_linearFilter);
  _diffusivity[""] = _nu;
  _diffusiveFlux = _nu ? new diffusiveFlux (_nu) : NULL;
  _diffusion = _nu ? new diffusivity(_nu) : NULL;
  _ipTerm = _nu ? dgNewIpTerm(3, _diffusiveFlux, _diffusion) : NULL;

  /*if (_R>0)
    _massFactor[""] = std::make_pair(new massFactor(_R, _xyz), true);*/
  //_interfaceTerm0[""] = _nu ? : new riemann(_bathymetry,NULL, _linear);


  _clipToPhysics[""] = new clipToPhysics(_bathymetry, 0.01);
}

dgConservationLawShallowWater2d::~dgConservationLawShallowWater2d() {
  delete _fzero;
  delete _fzerov;
  if (_volumeTerm0[""]) delete _volumeTerm0[""];
  if (_volumeTerm1[""]) delete _volumeTerm1[""];
  if (_interfaceTerm0[""]) delete _interfaceTerm0[""];
  if (_ipTerm) delete _ipTerm;
  if (_diffusiveFlux) delete _diffusiveFlux;
  if (_diffusion) delete _diffusion;
  if (_maximumConvectiveSpeed[""]) delete _maximumConvectiveSpeed[""];
  if (_clipToPhysics[""]) delete _clipToPhysics[""];
  if (_massFactor.count("") != 0) delete _massFactor[""].first;
  if (_useMovingBathWD) {
    delete _bathymetry;
    delete _bathymetryGradient;
  }
}



/*==============================================================================
 * Specific boundary conditions
 *============================================================================*/

// BC : Wall

/*class dgConservationLawShallowWater2d::boundaryWall : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> normals, sol, _xyz, bath;
    bool _linear;
    double _R;
   public:
    term(bool linear, double R, const function *bathF) : function(3) {
      setArgument(sol,function::getSolution());
      setArgument(_xyz,function::getCoordinates());
      setArgument(normals,function::getNormals());
      setArgument(bath,bathF);
      _linear = linear;
      _R = R;
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      size_t nQP = sol.size1();
      for (size_t i=0; i<nQP; i++) {
        double nx = normals(i,0);
        double ny = normals(i,1);
        double eta = sol(i,0);
        double J = 1.0;
        if (_R>0) {
          double x = _xyz(i,0);
          double y = _xyz(i,1);
          J = 4*_R*_R/(4*_R*_R+x*x+y*y);
	  J = 1.0;
        }
        double u = sol(i, 1);
        double v = sol(i, 2);
        double un = u * nx + v * ny;
        // val(i, 0) = 0;
        // val(i, 1) = -g*eta*nx/J;
        // val(i, 2) = -g*eta*ny/J;
        // if(!_linear && un > 0){
        //   val(i, 1)-=un*u/J;
        //   val(i, 2)-=un*v/J;
        // }
        if ( _linear) {
          val(i, 0) = 0;//bath(i, 0) * un * J;
          val(i, 1) = -g*eta*nx;
          val(i, 2) = -g*eta*ny;
        } else {
          double uL = un;
          double vL = u * (-ny) + v * nx;
          double h = bath(i,0);
          double HL = eta + h;
          double HStar,uStar,vStar,Amb;
          //roeSolver(uL, 0, vL, 0, HL, HL, uStar, vStar, HStar, Amb);
          roeSolver(uL, -uL, vL, vL, HL, HL, uStar, vStar, HStar, Amb);
          val(i,0) = 0;//(-HStar * uStar + (HL * un)) * J;
          double FunL = - g * ((HStar -h)) * J - (uStar*uStar) * J;
          double FutL = - (uStar*vStar) * J;
          val(i,1) = FunL * nx - FutL * ny;
          val(i,2) = FunL * ny + FutL * nx;
	/*
	  if (uStar>0) {
            val(i, 1) = (uStar * uStar - uL*uL) / J;
            val(i, 2) = (uStar * vStar - uL*vL) / J;
	  }*/ /*       
	}
      }
    }
  };
  term _term;
public:
  boundaryWall(dgConservationLawShallowWater2d *claw) : _term(claw->isLinear(), claw->getRadius(), claw->getBathymetry()) {
    _term0 = &_term;
  }
};*/
class dgConservationLawShallowWater2dWallExtValue : public function {
  fullMatrix<double> solIn, normals;
public:
  dgConservationLawShallowWater2dWallExtValue():function (3){
    setArgument (normals, function::getNormals());
    setArgument (solIn, function::getSolution(), 0);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    for (size_t i=0; i< nQP; i++) {
      const double nx = normals (i,0);
      const double ny = normals (i,1);
      val(i, 0) = solIn(i, 0);
      double un = solIn (i,1) * nx + solIn (i,2) * ny;
      val(i,1) = solIn (i,1) - 2 * un * nx; // with utan different from 0
      val(i,2) = solIn (i,2) - 2 * un * ny;
      //val (i,0) = -un * nx; // with utan = 0
      //val (i,1) = -un * ny;
    }
  }
};

dgBoundaryCondition *dgConservationLawShallowWater2d::newBoundaryWall(){
  //return new boundaryWall(this);
  function *boundaryWallUV = new dgConservationLawShallowWater2dWallExtValue();
  std::vector<const function*> *toReplace = new std::vector<const function*>();
  std::vector<const function*> *replaceBy = new std::vector<const function*>();
  toReplace->push_back(function::getSolution());
  replaceBy->push_back(boundaryWallUV);
  return newOutsideValueBoundaryGeneric("",*toReplace,*replaceBy);
}

// BC : Forced Discharge

class dgConservationLawShallowWater2d::boundaryForcedDischarge : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> normals, sol, _xyz, bath, discharge, mbFact;
    bool _linear;
    double _R, S0;
  public:
    term(bool linear, double R, const function *bathF, dgDofContainer *solution, const function *dischargeF, std::string tag, const function *movingBathF):function(3){
      setArgument(sol,function::getSolution());
      setArgument(_xyz,function::getCoordinates());
      setArgument(normals,function::getNormals());
      setArgument(bath,bathF);
      setArgument(discharge,dischargeF);
      setArgument(mbFact,movingBathF);
      _linear = linear;
      _R = R;
      dgFunctionIntegratorInterface integrator(solution->getGroups(), bathF, solution); 
      fullMatrix<double> Smat;
      integrator.compute(tag, Smat);
      S0 = Smat(0,0);
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      double J = 1;
      double Fun, Fut, Feta;
      for (int i=0; i<sol.size1(); i++) {
        if (_R>0) {
          double x = _xyz(i,0);
          double y = _xyz(i,1);
          J = 4.0*_R*_R/(4.0*_R*_R+x*x+y*y);
	  J = 1.0;
        }
        double nx = normals(i,0);
        double ny = normals(i,1);
        double uL = nx*sol(i,1) + ny*sol(i,2), uR =  - discharge(i,0) / S0;
        double vL = -ny*sol(i,1) + nx*sol(i,2), vR = vL;
        double h = bath(i,0);
        double etaL = sol(i,0);
        double etaR = etaL;
        if (_linear) {
          double sq_g_h = sqrt(g/h);
          double etaRiemann = (etaL+etaR + (uL-uR)/sq_g_h)/2;
          double unRiemann = (uL+uR + (etaL-etaR)*sq_g_h)/2;
          Fun = -g*etaRiemann*J;
          Fut = 0;
          Feta = -(h)*unRiemann*J;
        } else {
          double HR = etaR + h, HL = etaL + h;
          if(HR<0 || HL<0) printf("forced Discharge HR = %e HL =%e\n", HR,HL);
          double u,v,H,Amb;
          roeSolver(uL, uR, vL, vR, HL, HR, u, v, H, Amb, mbFact(i,0), mbFact(i,0), -1);
          double eta  = H-h;
          Fun = -g*eta*J - u*u*J;
          Fut = -u*v*J;
          Feta = -(h+eta)*u*J/Amb;
        }
        val(i,0) = Feta;
        val(i,1) = Fun * nx - Fut * ny;
        val(i,2) = Fun * ny + Fut * nx;
      }
    }
  };
  term _term;
public:
  boundaryForcedDischarge(dgConservationLawShallowWater2d *claw, dgDofContainer *solution, const function *discharge, std::string tag) : _term(claw->isLinear(), claw->getRadius(), claw->getBathymetry(), solution, discharge, tag, claw->_movingBathFactor) {
    _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawShallowWater2d::newForcedDischarge(dgDofContainer *solution, const function *discharge, std::string tag){
  return new boundaryForcedDischarge(this, solution, discharge, tag);
}

// BC : Coupling SW1D

class dgConservationLawShallowWater2d::boundaryCouplingSW1D:public dgBoundaryCondition {
  class term:public function {
    fullMatrix<double> normals, solExt, sol, bath, _xyz;
    double _R;
  public:
    term(const function *solExtF, const function *bathF,const double R):function(3){
      setArgument(solExt, solExtF);
      setArgument(sol, function::getSolution());
      setArgument(bath, bathF);
      setArgument(normals, function::getNormals());
      _R = R;
      if (_R>0)
        setArgument(_xyz, function::getCoordinates());
    }
    void call (dataCacheMap *map, fullMatrix<double> &val) { 
      double J = 1;
      for (int i=0; i<sol.size1(); i++){
        if (_R>0) {
          double x = _xyz(i,0);
          double y = _xyz(i,1);
          J = 4.0*_R*_R/(4.0*_R*_R+x*x+y*y);
	  J = 1.0;
        }
        double nx = normals(i,0), ny = normals(i,1);
        double uIn = nx * sol(i,1) + ny * sol(i,2);
        double vIn = -ny * sol(i,1) + nx * sol(i,2); 
        double uExt = solExt(i,1);
        double vExt = 0.0;
        double etaIn = sol(i,0), etaExt = solExt(i,0);
        double h = bath(i,0);
        double HIn = h + etaIn;
        double HExt = h + etaExt;
        double uStar,vStar,HStar, AStar;
        //printf("2D u = %.5e, %.5e  H = %.5e %.5e eta %.5e %.5e \n",uIn, uExt, HIn, HExt, etaIn, etaExt);
        roeSolver(uIn, uExt, vIn, vExt, HIn, HExt, uStar, vStar, HStar, AStar);
        double etaStar = HStar - h;
        double Feta = - HStar * uStar * J;
        double Fun = - g * etaStar * J - uStar * uStar * J;
        double Fut = - uStar * vStar * J;
        val(i,0) = Feta;
        val(i,1) = Fun * nx - Fut * ny;
        val(i,2) = Fun * ny + Fut * nx;
      }
    }
  };
public:
  term  _term;
  boundaryCouplingSW1D (dgConservationLawShallowWater2d *claw, const function *solExtF) : _term(solExtF, claw->getBathymetry(), claw->getRadius()) {
    _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawShallowWater2d::newBoundaryCouplingSW1D(const function *solExtF){
  return new boundaryCouplingSW1D(this, solExtF);
}
