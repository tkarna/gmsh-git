#include "dgConservationLawNavierStokes1d.h"
#include "function.h"
#include "Numeric.h" 
#include "assert.h" 
#include "stdlib.h"
#include <string.h>


static double g = 9.81;
static double eps = 1.e-5;

struct dataBif{
  double n1;
  double n2; 
  double n3;
  double beta1;
  double beta2;
  double beta3;
  double W1;
  double W2;
  double W3;
  double A0_1;
  double A0_2;
  double A0_3;
  double rho;
  double p0;
};

struct dataDisc{
  double n1;
  double n2; 
  double beta1;
  double beta2;
  double W1;
  double W2;
  double A0_1;
  double A0_2;
  double rho;
  double p0;
};


struct dataWK{
  double AL ;
  double vitL ; 
  double pC ;
  double beta ;
  double A0 ;
  double Rmu ;
  double p0 ;
  double rho;
};


//--------------------------------------------------------------------------//

static void newtonBif(fullVector<double> &x, fullVector<double> &res, void *data)
{
  dataBif *ndat = (dataBif*)data;
  
  double n1 = ndat->n1;
  double n2 = ndat->n2;
  double n3 = ndat->n3;
  double beta1  = ndat->beta1;
  double beta2 = ndat->beta2;
  double beta3 = ndat->beta3;
  double W1  = ndat->W1;
  double W2 = ndat->W2;
  double W3 = ndat->W3;
  double A0_1 = ndat->A0_1;
  double A0_2 = ndat->A0_2;
  double A0_3 = ndat->A0_3;
  double rho = ndat->rho;
  double p0  = ndat->p0;
  
  //x: uL, AL, UR1, AR1, UR2, AR2
  double u1 = x(0);
  double A1 = x(1) + A0_1;
  double u2 = x(2);
  double A2 = x(3) + A0_2;
  double u3 = x(4);
  double A3 = x(5) + A0_3;
	 
  double p1 =   (p0 + beta1*( sqrt(A1) - sqrt(A0_1) ))/rho;
  double p2 =   (p0 + beta2*( sqrt(A2) - sqrt(A0_2) ))/rho;
  double p3 =   (p0 + beta3*( sqrt(A3) - sqrt(A0_3) ))/rho;
  
  res(0) =  u1 + n1 * 4.*pow(A1, 0.25)*sqrt(beta1/(2*rho)) - W1;
  res(1) =  u2 + n2 * 4.*pow(A2, 0.25)*sqrt(beta2/(2*rho)) - W2;
  res(2) =  u3 + n3 * 4.*pow(A3, 0.25)*sqrt(beta3/(2*rho)) - W3;

  res(3) =  u1*A1*n1 + u2*A2*n2 + u3*A3*n3;
  res(4) = .5*u1*u1 + p1 - .5*u2*u2 - p2;
  res(5) = .5*u1*u1 + p1 - .5*u3*u3 - p3;

}

// -----------------------------------------------------------------------------
static void newtonDisc(fullVector<double> &x, fullVector<double> &res, void *data)
{
  dataDisc *ndat = (dataDisc*)data;
  
  double n1 = ndat->n1;
  double n2 = ndat->n2;
  double beta1  = ndat->beta1;
  double beta2 = ndat->beta2;
  double W1  = ndat->W1;
  double W2 = ndat->W2;
  double A0_1 = ndat->A0_1;
  double A0_2 = ndat->A0_2;
  double rho = ndat->rho;
  double p0  = ndat->p0;

  //x: uL, AL, UR1, AR1
  double u1 = x(0);
  double A1 = x(1) + A0_1;
  double u2 = x(2);
  double A2 = x(3) + A0_2;
	 
  double p1 =   (p0 + beta1*( sqrt(A1) - sqrt(A0_1) ))/rho;
  double p2 =   (p0 + beta2*( sqrt(A2) - sqrt(A0_2) ))/rho;
  
  res(0) =  u1 + n1 * 4.*pow(A1, 0.25)*sqrt(beta1/(2*rho)) - W1;  
  res(1) =  u2 + n2 * 4.*pow(A2, 0.25)*sqrt(beta2/(2*rho)) - W2;
  res(2) =  u1*A1*n1 + u2*A2*n2 ;
  res(3) = .5*u1*u1 + p1 - .5*u2*u2 - p2;

}

// -----------------------------------------------------------------------------
static void newtonWK(fullVector<double> &x, fullVector<double> &res, void *data)
{
  dataWK *ndat = (dataWK*)data;
  
  double AL = ndat->AL;
  double vitL = ndat->vitL;
  double pC = ndat->pC;
  double beta = ndat->beta;
  double A0 = ndat->A0;
  double Rmu = ndat->Rmu;
  double p0 = ndat->p0;
  double rho = ndat->rho;
  
  if (x(0) < 0.0) x(0) = 1.e-3;
  //assert(x(0)>=0.);  
    
  double u_OD = vitL + 4.*(pow(AL, 0.25)-pow(x(0), 0.25))*sqrt(beta/(2*rho));
  double p_OD = p0 + beta*( sqrt(x(0)) - sqrt(A0)  ) ;
  
  res(0) = Rmu*x(0)*u_OD - p_OD + pC;
}

//--------------------------------------------------------------------------//
static void computeRiemannDiscontinuity(int iPt, fullMatrix<double> &val, void * Pndat, fullMatrix<double> &solL, fullMatrix<double> &solR)
{
  dataDisc *ndat = (dataDisc*)Pndat;

  double n1 = ndat->n1;
  double n2 = ndat->n2;
  double _beta1  = ndat->beta1;
  double _beta2 = ndat->beta2;
  double _A0_1 = ndat->A0_1;
  double _A0_2 = ndat->A0_2;
  double _rho = ndat->rho;
  double _p0  = ndat->p0;
  
  double A1 = _A0_1 + solL(iPt,0);
  double A2 = _A0_2 + solR(iPt,0);
  double u1 = solL(iPt,1);
  double u2 = solR(iPt,1);

  double W1 = u1 + n1*4.*pow(A1, 0.25)*sqrt(_beta1/(2*_rho));
  double W2 = u2 + n2*4.*pow(A2, 0.25)*sqrt(_beta2/(2*_rho));
             
  fullVector<double> x(4);
  x(0) = solL(iPt,1)+eps;//u1
  x(1) = solL(iPt,0)+eps;//eta1  
  x(2) = solR(iPt,1)+10*eps;//u2
  x(3) = solR(iPt,0)+10*eps;//eta2  
  
  dataDisc ndat2 = { n1, n2, _beta1, _beta2, W1, W2, _A0_1, _A0_2, _rho, _p0}; 
  
  double Relax = 0.95;
  double Tol = 1.e-4;

  bool success = newton_fd(newtonDisc, x, &ndat2, Relax, Tol);
  if (!success)  {
    printf("Error:  exit riemannDiscontinuitySolver\n"); exit(1);
  }
  u1 = x(0);
  A1 = x(1) + _A0_1;
  u2 = x(2);
  A2 = x(3) + _A0_2;
      
  double p1 =  (_p0 + _beta1*(sqrt(A1) - sqrt(_A0_1) ))/_rho;
  double F1_a = A1*u1*n1;
  double F1_b = (.5*u1*u1 + p1)*n1;
      
  double p2 =  (_p0 + _beta2*(sqrt(A2) - sqrt(_A0_2) ))/_rho;
  double F2_a = A2*u2*n2;
  double F2_b = (.5*u2*u2 + p2)*n2;
      
  val(iPt,0) = -F1_a;
  val(iPt,1) = -F1_b;
  val(iPt,2) = -F2_a;
  val(iPt,3) = -F2_b; 

}

// -----------------------------------------------------------------------------

dgConservationLawNavierStokes1d::dgConservationLawNavierStokes1d() : dgConservationLawFunction(2)
{
  // eta u
  function *fzero = new functionConstant(0.);
  _pressure = fzero;
  _celerity = fzero;
  _characteristic = fzero;
  _bathymetry = fzero;
  _sourceMass = fzero;
  _linearDissipation = fzero;
  _beta = fzero;
  _diameterAndCelerity0 = fzero;
  _shallow = false;
  _blood = false;
  _p0 = 0.0;
  _rho = 1.0;
  _typeRiemann = "LAX";
}

//------------------------------------------------------------------------
class dgConservationLawNavierStokes1d::celerityShallow : public function{
 fullMatrix<double>  solution, bathymetry;
public :
  celerityShallow(const function *bathymetryF) : function(1)
  {
    setArgument (solution, function::getSolution());
    setArgument (bathymetry, bathymetryF);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP =val.size1();
    for (size_t i=0; i<nQP; i++) {
      double h = bathymetry(i,0);
      double eta = solution(i,0);
      val (i,0) = sqrt(g*(h+eta));
    }
  }
};

class dgConservationLawNavierStokes1d::pressureShallow : public function{
 fullMatrix<double>  solution;
public :
  pressureShallow() : function(1)
  {
    setArgument (solution, function::getSolution());
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP =val.size1();
    for (size_t i=0; i<nQP; i++) {
      double eta = solution(i,0);
      val (i,0) = g*eta ;
    }
  }
};

class dgConservationLawNavierStokes1d::charShallow : public function{
 fullMatrix<double>  celerity;
public :
  charShallow(const function *celerityF) : function(1)
  {
    setArgument (celerity, celerityF);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP =val.size1();
    for (size_t i=0; i<nQP; i++) {
      val (i,0) = celerity(i,0);
    }
  }
};

//------------------------------------------------------------------------
class dgConservationLawNavierStokes1d::celerityBlood : public function{
  fullMatrix<double>  solution, bathymetry, beta;
  double _rho ; 
public :
  celerityBlood(const function *bathymetryF, const function *betaF, const double rho ) : function(1)
  {
    setArgument (solution, function::getSolution());
    setArgument (bathymetry, bathymetryF);
    setArgument (beta, betaF);
    _rho = rho;
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP =val.size1();
    for (size_t i=0; i<nQP; i++) {
      double A0 = bathymetry(i,0);
      double A = A0 + solution(i,0); 
      double c = pow(A, 0.25)*sqrt( beta(i,0) /(2*_rho));
      val (i,0) = c;
    }
  }
};
class dgConservationLawNavierStokes1d::A0Blood : public function{
  fullMatrix<double>  DC0;
public :
  A0Blood(const function *DC0F ) : function(1)  {
    setArgument (DC0, DC0F);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP =val.size1();
    for (size_t i=0; i<nQP; i++) {
      double D0 = DC0(i,0) ;
      val (i,0) = 0.25*M_PI*D0*D0;
    }
  }
};
function *dgConservationLawNavierStokes1d::newA0Blood(const function *f0){
  return new A0Blood(f0);
}

class dgConservationLawNavierStokes1d::betaBlood : public function{
  fullMatrix<double>  DC0;
  double _rho ; 
public :
  betaBlood(const function *DC0F, const double rho ) : function(1)  {
    setArgument (DC0, DC0F);
    _rho = rho;
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP =val.size1();
    for (size_t i=0; i<nQP; i++) {
      double D0 = DC0(i,0);
      double c0 = DC0(i,1);
      double A0 = 0.25*M_PI*D0*D0;
      double beta = c0*c0*2.*_rho/sqrt(A0);
      val (i,0) = beta;
    }
  }
};
function *dgConservationLawNavierStokes1d::newBetaBlood(const function *f0, const double rho){
  return new betaBlood(f0, rho);
}

class dgConservationLawNavierStokes1d::pressureBlood : public function{
  fullMatrix<double>  solution, bathymetry, beta;
  double _rho ;
  double _p0 ;
public :
  pressureBlood(const function *bathymetryF, const function *betaF, const double rho, const double p0) : function(1)
  {
    setArgument (solution, function::getSolution());
    setArgument (bathymetry, bathymetryF);
    setArgument (beta, betaF);
    _rho = rho;
    _p0 = p0;
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP =val.size1();
    for (size_t i=0; i<nQP; i++) {
      double A0 = bathymetry(i,0);
      double A = A0 + solution(i,0);
      double p = ( _p0 +beta(i,0)*(sqrt(A)-sqrt(A0)) ) /_rho;
      val (i,0) = p ;
    }
  }
};

class dgConservationLawNavierStokes1d::charBlood : public function{
  fullMatrix<double>  celerity;
public :
  charBlood(const function *celerityF) : function(1) {
    setArgument (celerity, celerityF);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP =val.size1();
    for (size_t i=0; i<nQP; i++) {
      val (i,0) = 4*celerity(i,0); 
    }
  }
};

//------------------------------------------------------------------------


class dgConservationLawNavierStokes1d::clipToPhysics : public function {
  fullMatrix<double> sol, bathymetry;
  double _hMin;
public:
  clipToPhysics(const function *bathymetryF, double hMin):function(3) {
    setArgument (sol, function::getSolution());
    setArgument (bathymetry, bathymetryF);
    _hMin=hMin;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    size_t nQP = val.size1();
    for (size_t k = 0 ; k < nQP; k++ ){
      double h = bathymetry(k,0);
      val(k,0) = sol(k,0);
      val(k,1) = sol(k,1);
      double H = sol(k,0)+h;
      if (H < _hMin){
        val(k,0) = _hMin;
      }
    }
  }
};

class dgConservationLawNavierStokes1d::maxConvectiveSpeed : public function {
  fullMatrix<double> sol, celerity;
  public:
  maxConvectiveSpeed(const function *celerityF):function(1) {
    setArgument (sol, function::getSolution());
    setArgument (celerity, celerityF);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++) {
      val(i,0) = sol(i,1) + celerity(i,0);
    }
  }
};

class dgConservationLawNavierStokes1d::advection: public function {
  fullMatrix<double> sol, bathymetry, pressure;
  public:
  advection(const function *bathymetryF, const function *pressureF) : function(6) {
    setArgument (sol, function::getSolution());
    setArgument (bathymetry, bathymetryF);
    setArgument (pressure, pressureF);
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++) {
      double eta = sol(i,0);
      double u = sol(i,1);
      // flux_x
      val(i,0) = (bathymetry(i,0)+eta)*u;
      val(i,1) = .5*u*u + pressure(i,0);
      // flux_y
      val(i,2) = 0;
      val(i,3) = 0;
      // flux_z
      val(i,4) = 0;
      val(i,5) = 0;
    }
  }
};

class dgConservationLawNavierStokes1d::source: public function {
  fullMatrix<double>  solution, linearDissipation, bathymetry, sourceMass;
public :
  source(const function *linearDissipationF, const function *bathymetryF, const function *sourceMassF) : function(2)
  {
    setArgument (linearDissipation, linearDissipationF);
    setArgument (solution, function::getSolution());
    setArgument (bathymetry, bathymetryF);
    setArgument (sourceMass, sourceMassF);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP =val.size1();
    for (size_t i=0; i<nQP; i++) {
      double h = bathymetry(i,0);
      double eta = solution(i,0);
      double u = solution(i,1);
      //Mass
      val (i,0)  =   sourceMass(i,0);
      //printf("h, eta, u : %f %f %f \n",  h, eta, u );
      //Momentum
      val (i,1) = - linearDissipation(i,0)*u/(h+eta) ;
    }
  }
};


class dgConservationLawNavierStokes1d::riemannLAX:public function {
  fullMatrix<double> normals, normals2, solL, solR, _bathymetryL, _bathymetryR;
  fullMatrix<double> _maxSpeedL, _maxSpeedR, _pressureL, _pressureR;
  fullMatrix<double>  _betaL, _betaR;
  double _p0, _rho;
  //fullMatrix<double> _sectionL, _sectionR;
public:
  riemannLAX(const function *bathymetry, const function *pressure,  const function *maxSpeed ,
	     const function *beta, double rho, double p0): function(4) {
    setArgument(normals, function::getNormals(), 0);
    setArgument(normals2,function::getNormals(), 1);
    setArgument(solL,function::getSolution(), 0);
    setArgument(solR,function::getSolution(), 1);
    setArgument(_bathymetryL,bathymetry, 0);
    setArgument(_bathymetryR,bathymetry, 1);
    setArgument(_pressureL,pressure, 0);
    setArgument(_pressureR,pressure, 1); 
    setArgument(_maxSpeedL,maxSpeed, 0); 
    setArgument(_maxSpeedR,maxSpeed, 1); 
    //setArgument(_sectionL,section, 0); 
    //setArgument(_sectionR,section, 1); 
    setArgument(_betaL,beta, 0); 
    setArgument(_betaR,beta, 1); 
    _p0 = p0;
    _rho = rho;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    for (int i = 0; i < val.size1(); ++i) {
      double hL = _bathymetryL(i,0),hR = _bathymetryR(i,0);
      double HL = solL(i,0) + hL,   HR = solR(i,0) + hR;
      //double HL = _sectionL(i,0)  HR = _sectionR(i,0);
      double uL = solL(i,1) ,       uR = solR(i,1) ;
      double n0 = normals(i,0); 
      double n2 = normals2(i,0); 

      // Discontinuity 
      if (_betaL(i,0) != _betaR(i,0) || hL != hR){
        double W1=0.0, W2=0.0; 
        dataDisc ndat = { n0, n2, _betaL(i,0), _betaR(i,0),
          W1, W2, _bathymetryL(i,0), _bathymetryR(i,0), _rho, _p0}; 

        void * Pndat = &ndat; 
        computeRiemannDiscontinuity(i, val, Pndat, solL, solR);
      }
      else{
        double pL = _pressureL(i,0),  pR = _pressureR(i,0);
        double F1,F2;

        F1 = .5*( HL*uL + HR*uR )*n0;
        F2 = .5*( .5*uL*uL+pL  + .5*uR*uR+pR )*n0;
        double maxlambdaL = _maxSpeedL(i,0); 
        double maxlambdaR = _maxSpeedR(i,0); 
        const double aMax = std::max(maxlambdaL,maxlambdaR);
        F1 += aMax*(HL-HR);
        F2 += aMax*(uL-uR);

        val(i,0) = -F1;
        val(i,1) = -F2;
        val(i,2) = -val(i,0);
        val(i,3) = -val(i,1);
      }
    }
  }
};

class dgConservationLawNavierStokes1d::riemannROE:public function {
  fullMatrix<double> normals, normals2, solL, solR, _bathymetryL, _bathymetryR;
  fullMatrix<double> _pressureL, _pressureR;
  functionReplace roecReplace, roepReplace;
  fullMatrix<double> cRoe, val1, valBath;
  fullMatrix<double> pRoe, val2;
  fullMatrix<double>  _betaL, _betaR;
  double _p0, _rho;
public:
  riemannROE(const function *bathymetry, const function *pressure,
	     const function *celerity ,
	     const function *beta, double rho, double p0 ): function(4) {
    setArgument(normals, function::getNormals(), 0);
    setArgument(normals2,function::getNormals(), 1);
    setArgument(solL,function::getSolution(), 0);
    setArgument(solR,function::getSolution(), 1);
    setArgument(_bathymetryL,bathymetry, 0);
    setArgument(_bathymetryR,bathymetry, 1); 
    setArgument(_pressureL,pressure, 0);
    setArgument(_pressureR,pressure, 1);
    addFunctionReplace(roecReplace);
    roecReplace.replace(val1, function::getSolution());
    roecReplace.replace(valBath, bathymetry);
    roecReplace.get(cRoe, celerity);
    addFunctionReplace(roepReplace);
    roepReplace.replace(val2, function::getSolution());//warning add beta and A0
    roepReplace.get(pRoe, pressure);
    setArgument(_betaL,beta, 0); 
    setArgument(_betaR,beta, 1); 
    _p0 = p0;
    _rho = rho;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for (int i = 0; i < val.size1(); ++i) {
      double hL = _bathymetryL(i,0),hR = _bathymetryR(i,0);
      double HL = solL(i,0) + hL,   HR = solR(i,0) + hR;
      double h = (hL+hR)/2;
      double HM = (HL+HR)/2;
      val1(i,0) = HM-h;
      valBath(i,0) = h;
    }
    roecReplace.compute();
    for (int i = 0; i < val.size1(); ++i) {
      double hL = _bathymetryL(i,0),hR = _bathymetryR(i,0);
      double h = (hL+hR)/2;
      double HL = solL(i,0) + hL,   HR = solR(i,0) + hR;
      double uL = solL(i,1) ,       uR = solR(i,1) ;
      double HM = (HL+HR)/2, HJ = (HL-HR)/2;
      double HuL = uL*HL, HuR = uR*HR;
      double HuJ = (HuL-HuR)/2;
      double sqHL = sqrt(HL), sqHR = sqrt(HR);
      double u_roe = (sqHL*uL + sqHR*uR) / (sqHL + sqHR);
      double H = HM + (HuJ - u_roe *HJ) / cRoe(i,0); 
      double eta  = H-h;
      val2(i,0) = eta;
    }
    roepReplace.compute();

    for (int i = 0; i < val.size1(); ++i) {
      double hL = _bathymetryL(i,0),hR = _bathymetryR(i,0);
      double HL = solL(i,0) + hL,   HR = solR(i,0) + hR;
      double uL = solL(i,1) ,       uR = solR(i,1) ;
      double n0 = normals(i,0); 
      double n2 = normals2(i,0); 
      // Discontinuity 
      if (_betaL(i,0) != _betaR(i,0) || hL != hR){
        double W1=0.0, W2=0.0; 
        dataDisc ndat = { n0, n2, _betaL(i,0), _betaR(i,0),
          W1, W2, _bathymetryL(i,0), _bathymetryR(i,0), _rho, _p0}; 
        void * Pndat = &ndat; 
        computeRiemannDiscontinuity(i, val, Pndat, solL, solR);
      }
      else{
        double F1,F2;
        double sqHL = sqrt(HL), sqHR = sqrt(HR);
        double HuL = uL*HL, HuR = uR*HR;
        double HM = (HL+HR)/2, HJ = (HL-HR)/2;
        double HuM = (HuL+HuR)/2, HuJ = (HuL-HuR)/2;
        double u_roe = (sqHL*uL + sqHR*uR) / (sqHL + sqHR);
        double H = HM + (HuJ - u_roe *HJ) / cRoe(i,0); 
        double Hu = HuM + (cRoe(i,0) - u_roe*u_roe/cRoe(i,0)) *HJ + u_roe/cRoe(i,0) *HuJ;
        double u = Hu / H;
        double p = pRoe(i,0);
        F1 = H*u*n0;
        F2 = (.5*u*u + p)*n0;

        val(i,0) = -F1;
        val(i,1) = -F2;
        val(i,2) = -val(i,0);
        val(i,3) = -val(i,1);  
      }
    }
  }
};
class dgConservationLawNavierStokes1d::riemannUPWIND:public function {
  fullMatrix<double> normals1, normals2, solL, solR, _bathymetryL, _bathymetryR;
  fullMatrix<double>  _charaL, _charaR;
  fullMatrix<double>  _betaL, _betaR;
  double _p0, _rho;
public:
  riemannUPWIND(const function *bathymetry, const function *pressure,
		const function *characteristic, const function *beta, 
		double rho, double p0): function(4) {
    setArgument(normals1, function::getNormals(), 0);
    setArgument(normals2, function::getNormals(), 1);
    setArgument(solL,function::getSolution(), 0);
    setArgument(solR,function::getSolution(), 1);
    setArgument(_bathymetryL,bathymetry, 0);
    setArgument(_bathymetryR,bathymetry, 1); 
    setArgument(_charaL,characteristic, 0);
    setArgument(_charaR,characteristic, 1);
    setArgument(_betaL,beta, 0); 
    setArgument(_betaR,beta, 1);    
    _p0 = p0;
    _rho = rho;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    for (int i = 0; i < val.size1(); ++i) {
      double hL = _bathymetryL(i,0),hR = _bathymetryR(i,0);
      double uL = solL(i,1) ,       uR = solR(i,1) ;
      double n1 = normals1(i,0); 
      double n2 = normals2(i,0); 

      // Discontinuity 
      if (_betaL(i,0) != _betaR(i,0) || hL != hR){
        double W1=0.0, W2=0.0; 
        dataDisc ndat = { n1, n2, _betaL(i,0), _betaR(i,0),
          W1, W2, _bathymetryL(i,0), _bathymetryR(i,0), _rho, _p0}; 

        void * Pndat = &ndat; 
        computeRiemannDiscontinuity(i, val, Pndat, solL, solR);      
      }
      else{
        double beta = .5*(_betaL(i,0) + _betaR(i,0));
        double A0   = .5*( hL+hR); 
        double W1  =  uL + _charaL(i,0); 
        double W2  =  uR - _charaR(i,0);
        double H = pow(.25*(W1-W2),4.)*pow(_rho/(2*beta),2.);
        double u = .5*(W1+W2);
        double p =( _p0 + beta*(sqrt(H)-sqrt(A0)))/_rho;
        double F1 = H*u*n1;
        double F2 = (.5*u*u + p)*n1;

        val(i,0) = -F1;
        val(i,1) = -F2;
        val(i,2) = -val(i,0);
        val(i,3) = -val(i,1);
      } 
    }
  }

};

class dgConservationLawNavierStokes1d::riemannHLL:public function {
  fullMatrix<double> normals, normals2, solL, solR, _bathymetryL, _bathymetryR;
  fullMatrix<double> _pressureL, _pressureR,  _celerityL, _celerityR;
  functionReplace roecReplace;
  fullMatrix<double> cRoe, val1, valBath;
  fullMatrix<double>  _betaL, _betaR;
  double _p0, _rho;
public:
  riemannHLL(const function *bathymetry, const function *pressure,
	     const function *celerity,  const function *beta, 
		double rho, double p0  ): function(4) {
    setArgument(normals, function::getNormals(), 0);
    setArgument(normals2,function::getNormals(), 1);
    setArgument(solL,function::getSolution(), 0);
    setArgument(solR,function::getSolution(), 1);
    setArgument(_bathymetryL,bathymetry, 0);
    setArgument(_bathymetryR,bathymetry, 1); 
    setArgument(_pressureL,pressure, 0);
    setArgument(_pressureR,pressure, 1);
    setArgument(_celerityL,celerity, 0);
    setArgument(_celerityR,celerity, 1);
    addFunctionReplace(roecReplace);
    roecReplace.replace(val1, function::getSolution());
    roecReplace.replace(valBath, bathymetry);
    roecReplace.get(cRoe, celerity);
    setArgument(_betaL,beta, 0); 
    setArgument(_betaR,beta, 1); 
    _p0 = p0;
    _rho = rho;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    for (int i = 0; i < val.size1(); ++i) {
      double hL = _bathymetryL(i,0);
      double hR = _bathymetryR(i,0);
      double HL = solL(i,0) + hL, HR = solR(i,0) + hR;
      double h = (hL+hR)/2;
      double Hstar = (HL+HR)/2;
      val1(i,0) = Hstar-h;
      valBath(i,0) = h;
    }
    roecReplace.compute();
    for (int i = 0; i < val.size1(); ++i) {
      double hL = _bathymetryL(i,0);  double hR = _bathymetryR(i,0);
      double HL = solL(i,0) + hL, HR = solR(i,0) + hR;
      double uL = solL(i,1) , uR = solR(i,1) ;
      double n0 = normals(i,0);
      double n2 = normals2(i,0);

      // Discontinuity 
      if (_betaL(i,0) != _betaR(i,0) || hL != hR){
        double W1=0.0, W2=0.0; 
        dataDisc ndat = { n0, n2, _betaL(i,0), _betaR(i,0),
          W1, W2, hL, hR, _rho, _p0}; 

        void * Pndat = &ndat; 
        computeRiemannDiscontinuity(i, val, Pndat, solL, solR);
      }
      else{
        double pL = _pressureL(i,0);   
        double pR = _pressureR(i,0);
        double sqHL = sqrt(HL), sqHR = sqrt(HR);
        double F1,F2;

        double ustar = (sqHL*uL + sqHR*uR) / (sqHL + sqHR); 
        double cstar =  cRoe(i,0); 
        double SL = std::min(uL - _celerityL(i,0), ustar - cstar);
        double SR = std::min(uR + _celerityR(i,0),  ustar + cstar);
        if (SL >= 0.){
          F1 =  HL*uL*n0;  
          F2 =  (.5*uL*uL + pL )*n0  ;
        }
        else if (SR <=0.){
          F1 =  HR*uR*n0;  
          F2 =  (.5*uR*uR + pR )*n0  ;
        }
        else{
          F1 = (SR*HL*uL*n0-SL*HR*uR*n0 + SL*SR*(HR-HL))/(SR-SL);
          F2 = (SR*(.5*uL*uL + pL )*n0-SL*(.5*uR*uR + pR )*n0  + SL*SR*(uR-uL))/(SR-SL);
        }

        val(i,0) = -F1;
        val(i,1) = -F2;
        val(i,2) = -val(i,0);
        val(i,3) = -val(i,1);
      }
    }
  }
};


class dgConservationLawNavierStokes1d::riemannBifurcation:public function {
  fullMatrix<double> normals1, normals2, normals3, sol1, sol2, sol3;
  fullMatrix<double> _bath1, _bath2, _bath3, _press1, _press2, _press3;
  fullMatrix<double> _char1, _char2, _char3, _beta1, _beta2, _beta3;
  double _rho, _p0;
  fullMatrix<double> pRep1, val1, pRep2, val2, pRep3, val3, bat1, bat2, bat3, bet1, bet2, bet3;
  functionReplace replace1, replace2, replace3;
  public:
  riemannBifurcation(const function *bathymetry, const function *pressure, 
		     const function *characteristic, const function *beta, 
		     double rho, double p0):function(6) {
    setArgument (normals1, function::getNormals (), 0);
    setArgument (normals2, function::getNormals (), 1);
    setArgument (normals3, function::getNormals (), 2);
    setArgument (sol1, function::getSolution (), 0);
    setArgument (sol2, function::getSolution (), 1);
    setArgument (sol3, function::getSolution (), 2);
    setArgument (_bath1, bathymetry, 0);
    setArgument (_bath2, bathymetry, 1);
    setArgument (_bath3, bathymetry, 2);
    setArgument (_press1, pressure, 0);
    setArgument (_press2, pressure, 1);
    setArgument (_press3, pressure, 2);
    setArgument (_char1, characteristic, 0);
    setArgument (_char2, characteristic, 1);
    setArgument (_char3, characteristic, 2);
    setArgument (_beta1, beta, 0);
    setArgument (_beta2, beta, 1);
    setArgument (_beta3, beta, 2);
    addFunctionReplace(replace1);
    replace1.replace(val1, function::getSolution());
    replace1.replace(bat1, bathymetry);
    replace1.replace(bet1, beta);
    replace1.get(pRep1, pressure);
    addFunctionReplace(replace2);
    replace2.replace(val2, function::getSolution());
    replace2.replace(bat2, bathymetry);
    replace2.replace(bet2, beta);
    replace2.get(pRep2, pressure);
    addFunctionReplace(replace3);
    replace3.replace(val3, function::getSolution());
    replace3.replace(bat3, bathymetry);
    replace3.replace(bet3, beta);
    replace3.get(pRep3, pressure);
    _rho = rho;
    _p0 = p0;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    for (int i = 0; i < val.size1(); ++i) {
      double n1 = normals1(i,0); 
      double n2 = normals2(i,0); 
      double n3 = normals3(i,0); 
      double W1 = sol1(i,1)  + n1*_char1(i,0);
      double W2 = sol2(i,1)  + n2*_char2(i,0);
      double W3 = sol3(i,1)  + n3*_char3(i,0);
      dataBif ndat = { n1, n2, n3, _beta1(i,0), _beta2(i,0), _beta3(i,0), 
        W1, W2, W3, _bath1(i,0), _bath2(i,0), _bath3(i,0), _rho, _p0}; 

      fullVector<double> x(6);
      x(0) = sol1(i,1)+eps;//u1
      x(1) = sol1(i,0)+eps;//eta1
      x(2) = sol2(i,1)+eps;//u2
      x(3) = sol2(i,0)+eps;//eta2
      x(4) = sol3(i,1)+eps;//u3
      x(5) = sol3(i,0)+eps;//eta3 

      bool success = newton_fd(newtonBif, x, &ndat, 0.9, 1.e-5);
      if (!success)  {
        printf("Error:  exit riemannBifurcationSolver \n"); exit(1);
      }

      double u1 = x(0);
      double A1 = x(1) + _bath1(i,0);
      double u2 = x(2);
      double A2 = x(3) + _bath2(i,0);
      double u3 = x(4);
      double A3 = x(5) + _bath3(i,0);

      val1(i,0) = x(1) ;
      bat1(i,0) = _bath1(i,0);
      bet1(i,0) = _beta1(i,0);
      replace1.compute();
      //double p1 =  (_p0 + _beta1(i,0)*(sqrt(A1) - sqrt(_bath1(i,0)) ))/_rho;
      double p1 = pRep1(i,0);
      double F1_a = A1*u1*n1;
      double F1_b = (.5*u1*u1 + p1)*n1;

      val2(i,0) = x(3) ; 
      bat2(i,0) = _bath2(i,0);
      bet2(i,0) = _beta2(i,0);
      replace2.compute();
      //double p2 =  (_p0 + _beta2(i,0)*(sqrt(A2) - sqrt(_bath2(i,0)) ))/_rho;
      double p2 = pRep2(i,0);
      double F2_a = A2*u2*n2;
      double F2_b = (.5*u2*u2 + p2)*n2;

      val3(i,0) = x(5) ; 
      bat3(i,0) = _bath3(i,0);
      bet3(i,0) = _beta3(i,0);
      replace3.compute();
      //double p3 =  (_p0 + _beta3(i,0)*(sqrt(A3) - sqrt(_bath3(i,0)) ))/_rho;
      double p3 = pRep3(i,0);
      double F3_a = A3*u3*n3;
      double F3_b = (.5*u3*u3 + p3)*n3;

      val(i,0) = -F1_a;
      val(i,1) = -F1_b;
      val(i,2) = -F2_a;
      val(i,3) = -F2_b; 
      val(i,4) = -F3_a;
      val(i,5) = -F3_b; 
    }
  }
};



class dgConservationLawNavierStokes1d::boundaryWall : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> sol, normals, pressure;
    public:
    term (const function *pressureF) : function (2) {
      setArgument (sol, function::getSolution());
      setArgument (pressure, pressureF);
      setArgument (normals, function::getNormals());
    }
    void call (dataCacheMap *map, fullMatrix<double> &val) { 
      for (int i = 0; i < val.size1(); ++i) {
        val(i,0) = 0;
        val(i,1) = -pressure(i,0)*normals(i,0);
      }
    }
  };
  public:
  term _term;
  boundaryWall(dgConservationLawNavierStokes1d *claw) : _term (claw->getPressureFlux()) {
    _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawNavierStokes1d::newBoundaryWall(){
  return new boundaryWall(this);
}


class dgConservationLawNavierStokes1d::inletAP : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> sol, normals, inlet, bathymetry, chara, beta;
    double _rho, _p0;
    public:
    term (const function *inletF, const function * bathymetryF, const function * charaF, 
	  const function *betaF, const double rho, const double p0) : function (2) {
      setArgument (sol, function::getSolution());
      setArgument (normals, function::getNormals());
      setArgument (inlet, inletF);
      setArgument (bathymetry, bathymetryF);
      setArgument (chara, charaF);
      setArgument (beta, betaF);
      _rho = rho;
      _p0 = p0;
    }
    void call (dataCacheMap *map, fullMatrix<double> &val) { 
      for (int i = 0; i < val.size1(); ++i) {
        double A0 = bathymetry(i,0);
        double c0 = pow(A0,.25)*sqrt(beta(i,0)/(2*_rho));//replace
        double W20 = -4*c0;
        double W2 =  sol(i,1) - chara(i,0);
        double W1 = W20 + 4*sqrt(2/_rho)*sqrt(inlet(i,0)-_p0+beta(i,0)*sqrt(A0));
        double u = 0.5*(W1+W2);
        double A = pow(.25*(W1-W2),4.)*pow(_rho/(2*beta(i,0)),2.);
        double p = _p0 + beta(i,0)*(sqrt(A)-sqrt(A0));
        double n0 = normals(i,0) ;
        double F1 = A*u*n0;
        double F2 = (.5*u*u+p)*n0;  
        val(i,0) = -F1;
        val(i,1) = -F2;
      }
    }
  };
  public:
  term  _term;
  inletAP (dgConservationLawNavierStokes1d *claw, const function *inletF) : _term (inletF, claw->getBathymetry(), 
      claw->getCharacteristic(), claw->getBeta(),
      claw->getDensity(), claw->getP0()) {
    _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawNavierStokes1d::newInletAbsorbingPressure(const function *inletPRES){
  return new inletAP(this, inletPRES);
}


class dgConservationLawNavierStokes1d::inletAV : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> sol, normals, inlet, bathymetry, chara, beta;
    double _rho, _p0;
    public:
    term (const function *inletF, const function * bathymetryF,
	  const function * charaF,  const function *betaF,
	  const double rho, const double p0) : function (2) {
      setArgument (sol, function::getSolution());
      setArgument (normals, function::getNormals());
      setArgument (inlet, inletF);
      setArgument (bathymetry, bathymetryF);
      setArgument (chara, charaF);
      setArgument (beta, betaF);
      _rho = rho;
      _p0 = p0;
    }
    void call (dataCacheMap *map, fullMatrix<double> &val) { 
      for (int i = 0; i < val.size1(); ++i) {
        double A0 = bathymetry(i,0);
        double c0 = pow(A0,.25)*sqrt(beta(i,0)/(2*_rho));//replace
        double W20 = -4*c0;
        double W2 =  sol(i,1) - chara(i,0);
        double UIN = inlet(i,0);
        double W1 = 2.*UIN - W20;
        double u = 0.5*(W1+W2);
        double A = pow(.25*(W1-W2),4.)*pow(_rho/(2*beta(i,0)),2.);
        double p = (_p0 + beta(i,0)*(sqrt(A)-sqrt(A0)))/_rho;;
        double n0 = normals(i,0) ;
        double F1 = A*u*n0;
        double F2 = (.5*u*u+p)*n0;  
        val(i,0) = -F1;
        val(i,1) = -F2;
      }
    }
  };
  public:
  term  _term;
  inletAV (dgConservationLawNavierStokes1d *claw, const function *inletF) : _term (inletF, claw->getBathymetry(), 
										   claw->getCharacteristic(), claw->getBeta(),
										   claw->getDensity(), claw->getP0()) {
    _term0 = &_term;
  }
};


dgBoundaryCondition *dgConservationLawNavierStokes1d::newInletAbsorbingVelocity(const function *inletVEL){
  return new inletAV(this, inletVEL);
}

class dgConservationLawNavierStokes1d::inletRV : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> sol, normals, inlet, bathymetry, pressure;
    double _rho, _p0;
    functionReplace pReplace;
    fullMatrix<double> pRep, val, valBath, valBeta;
    public:
    term (const function *inletF, const function * bathymetryF, const function * pressureF,
	  const double rho, const double p0) : function (2) {
      setArgument (sol, function::getSolution());
      setArgument (normals, function::getNormals());
      setArgument (inlet, inletF);
      setArgument (bathymetry, bathymetryF);
      setArgument (pressure, pressureF);
      _rho = rho;
      _p0 = p0;

    }
    void call (dataCacheMap *map, fullMatrix<double> &val) { 
      for (int i = 0; i < val.size1(); ++i) {
        double A = bathymetry(i,0) + sol(i,0);
        double n0 = normals(i,0) ;
        double u = inlet(i,0);
        double p = pressure(i,0);
        double F1 = A*u*n0;
        double F2 = (.5*u*u+p)*n0;  
        val(i,0) = -F1;
        val(i,1) = -F2;
      }
    }
  };
  public:
  term  _term;
  inletRV (dgConservationLawNavierStokes1d *claw, const function *inletF) : _term (inletF, claw->getBathymetry(), 
      claw->getPressureFlux(),
      claw->getDensity(), claw->getP0()) {
    _term0 = &_term;
  }
};
dgBoundaryCondition *dgConservationLawNavierStokes1d::newInletReflectingVelocity(const function *inletVEL){
  return new inletRV(this, inletVEL);
}


class dgConservationLawNavierStokes1d::outletRT : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> Rt, sol, normals, inlet, bathymetry, chara, beta;
    double _rho, _p0;
    public:
    term (const function *RtF, const function * bathymetryF, const function * charaF, 
        const function *betaF, const double rho, const double p0) : function (2) {
      setArgument (sol, function::getSolution());
      setArgument (normals, function::getNormals());
      setArgument (Rt, RtF);
      setArgument (bathymetry, bathymetryF);
      setArgument (chara, charaF);
      setArgument (beta, betaF);
      _rho = rho;
      _p0 = p0;
    }
    void call (dataCacheMap *map, fullMatrix<double> &val) { 
      for(int i = 0; i < val.size1(); ++i) {
        double n0 = normals(i,0) ; 
        double A0 = bathymetry(i,0);
        double c0 = pow(A0,.25)*sqrt(beta(i,0)/(2*_rho));
        double W1 =  sol(i,1) + chara(i,0);
        double init = 4*c0;
        double u =  (1.-Rt(i,0))*(W1 - init)/2.;
        double A = A0 + sol(i,0);
        double p = (_p0 + beta(i,0)*(sqrt(A)-sqrt(A0)))/_rho;

        double F1 = A*u*n0;
        double F2 = (.5*u*u+p)*n0;  
        val(i,0) = -F1;
        val(i,1) = -F2;
      }
    }
  };
  public:
  term  _term;
  outletRT (dgConservationLawNavierStokes1d *claw, const function *RtF) : _term (RtF, claw->getBathymetry(), 
      claw->getCharacteristic(), claw->getBeta(),
      claw->getDensity(), claw->getP0()) {
    _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawNavierStokes1d::newOutletTerminalResistance(const function *Rt){
  return new outletRT(this, Rt);
}

class dgConservationLawNavierStokes1d::outletWK : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> RCP, sol, normals, inlet, bathymetry, chara, beta, time, dt;
    double _rho, _p0;
    double A_n, u_n, p_n;
    fullVector<double> k, pRK,ARK,uRK,alpha,b;
    int iRK;
    public:
    term (const function *RCPF, const function * bathymetryF, const function * charaF, 
        const function *betaF, const double rho, const double p0) : function (2) {
      setArgument (sol, function::getSolution());
      setArgument (normals, function::getNormals());
      setArgument (time, function::getTime()); 
      setArgument (dt, function::getDT());
      setArgument (RCP, RCPF);
      setArgument (bathymetry, bathymetryF);
      setArgument (chara, charaF);
      setArgument (beta, betaF);
      _rho = rho;
      _p0 = p0;
      iRK = 0;
      A_n = 0.0; u_n = 0.0; p_n = p0;
      k.resize(4); pRK.resize(5); 
      ARK.resize(5); uRK.resize(5); alpha.resize(4); b.resize(4);
      pRK(0) = p0;
      ARK(0) =  0.0; 
      uRK(0) =  0.0; 
      alpha(0) = 0.0;  alpha(1) = 0.5;  alpha(2) = 0.5;  alpha(3) = 1.0;
      b(0) = 1./6.; b(1) = 1./3.; b(2) = 1./3.; b(3) = 1./6.;
    }
    void call (dataCacheMap *map, fullMatrix<double> &val) { 
      for (int i = 0; i < val.size1(); ++i) {
        double n0 = normals(i,0) ;
        double A0 = bathymetry(i,0);
        double c0 = pow(A0,.25)*sqrt(beta(i,0)/(2*_rho));
        double A = A0 + sol(i,0);
        double u = sol(i,1);
        double R = RCP(i,0);
        double Rmu1 = _rho*c0/A0;
        double Rmu2 = R - Rmu1;
        double C    =  RCP(i,1);
        double Pout    =  RCP(i,2);
        if (C == 0.0) Rmu1 = R;	

        //if (ARK(iRK) == 0.0) ARK(iRK) = A0;
        //double Qin  = ARK(iRK)*uRK(iRK);
        //double Qout =  (pRK(iRK) - Pout) / Rmu2; 
        if (A_n == 0.0) A_n = A0;
        double Qin  = A_n*u_n;
        double Qout = (p_n - Pout) / Rmu2;   

        //k(iRK) = 1./C*(Qin - Qout);
        //if ((iRK+1) %4 != 0)
        //	pRK(iRK+1) = pRK(0) + alpha(iRK+1)*k(iRK);
        //else 
        //	pRK(iRK+1) = pRK(0) + dt(i,0)*(b(0)*k(0)+b(1)*k(1)+b(2)*k(2)+b(3)*k(3));
        //double pC = pRK(iRK+1);
        double pC = Pout;
        if (C != 0.0) pC =  p_n + dt(i,0)/C*(Qin - Qout);

        //A_n
        fullVector<double> x(1); x(0) = A;
        dataWK ndat = { A, u+eps,  pC, beta(i,0), A0, Rmu1, _p0, _rho}; 
        bool success = newton_fd(newtonWK, x, &ndat, 0.9, 1.e-5);
        if (!success)  {  printf("Error:  exit windkessel Solver\n");  exit(1);	}
        ARK(iRK+1) = x(0);
        double A_0D = ARK(iRK+1) ;

        //u_n
        double p_0D = _p0 +beta(i,0)*(sqrt(A_0D)-sqrt(A0)) ;
        uRK(iRK+1) = (p_0D - pC)/(Rmu1*A_0D);
        double u_0D  = uRK(iRK+1);

        double Aup = pow(2.*pow(A_0D, 0.25) - pow(A,0.25),4); 
        double p = (_p0 + beta(i,0)*(sqrt(Aup)-sqrt(A0)))/_rho;

        double F1 = Aup*u*n0;
        double F2 = (.5*u*u+p)*n0;  
        val(i,0) = -F1;
        val(i,1) = -F2;

        iRK++;

        if (iRK %4 == 0){
          p_n = pC;
          A_n = A_0D;
          u_n = u_0D;
          //pRK(0) = pRK(4);
          //ARK(0) = ARK(4);
          //uRK(0) = uRK(4);
          iRK = 0;
        }
      }

    }
  };
  public:
  term  _term;
  outletWK (dgConservationLawNavierStokes1d *claw, const function *RCPF) : _term ( RCPF, claw->getBathymetry(), 
      claw->getCharacteristic(), claw->getBeta(),
      claw->getDensity(), claw->getP0()) {
    _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawNavierStokes1d::newOutletWindkessel(const function *RCP){
  return new outletWK(this, RCP);
}

void dgConservationLawNavierStokes1d::setup () {
  if (_shallow) {
    //_section = new sectionShallow()
    _pressure = new pressureShallow();
    _celerity = new celerityShallow(_bathymetry);
    _characteristic = new charShallow(_celerity);
  }
  else if (_blood){
    //_section = new sectionBloods()
    _pressure = new pressureBlood(_bathymetry, _beta, _rho, _p0);
    _celerity = new celerityBlood(_bathymetry, _beta, _rho);
    _characteristic = new charBlood(_celerity);
  }

  _volumeTerm0[""] = new source ( _linearDissipation, _bathymetry, _sourceMass);
  _volumeTerm1[""] = new advection (_bathymetry, _pressure);
  _maximumConvectiveSpeed[""] = new maxConvectiveSpeed ( _celerity);

  if (_typeRiemann == "LAX") 
    _interfaceTerm0[""] = new riemannLAX ( _bathymetry, _pressure, _maximumConvectiveSpeed[""], _beta, _rho, _p0);
   else if (_typeRiemann == "ROE") 
     _interfaceTerm0[""] = new riemannROE ( _bathymetry, _pressure, _celerity , _beta, _rho, _p0 );
   else if (_typeRiemann == "UPW") 
     _interfaceTerm0[""] = new riemannUPWIND ( _bathymetry, _pressure, _characteristic, _beta, _rho, _p0);
   else if (_typeRiemann == "HLL") 
     _interfaceTerm0[""] = new riemannHLL ( _bathymetry, _pressure, _celerity, _beta, _rho, _p0);

  _clipToPhysics[""] = new clipToPhysics(_bathymetry, 1e-5);
  _bifurcationTerm0[""] = new riemannBifurcation (_bathymetry, _pressure, _characteristic, _beta, _rho, _p0);

}

dgConservationLawNavierStokes1d::~dgConservationLawNavierStokes1d () {
  if (_volumeTerm0[""]) delete _volumeTerm0[""];
  if (_volumeTerm1[""]) delete _volumeTerm1[""];
  if (_interfaceTerm0[""]) delete _interfaceTerm0[""];
  if (_bifurcationTerm0[""]) delete _bifurcationTerm0[""];
  if (_maximumConvectiveSpeed[""]) delete _maximumConvectiveSpeed[""];
  if (_clipToPhysics[""]) delete _clipToPhysics[""];
  if (_pressure) delete _pressure;
  if (_celerity) delete _celerity;
  if (_characteristic) delete _characteristic;
}
