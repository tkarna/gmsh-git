#include "dgConservationLawStokes3d.h"
#include "dgConservationLawFunction.h"
#include "dgGroupOfElements.h"
#include "function.h"
#include "float.h"
#include "nodalBasis.h"
#include "dgMeshJacobian.h"
#include "Numeric.h"


class  dgConservationLawStokes3d::velocityVector : public function{
  fullMatrix<double>  solution;
public :
  velocityVector() : function(3)  {
    setArgument (solution, function::getSolution());
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP =val.size1();
    for (size_t i=0; i<nQP; i++) {
      val (i,0) = solution(i,0);
      val (i,1) = solution(i,1);
      val (i,2) = solution(i,2);

    }
  }
};
//MAXIMUM CONVECTIVE SPEED:

class  dgConservationLawStokes3d::maxConvectiveSpeed: public function {
  fullMatrix<double> sol;
  public:
  maxConvectiveSpeed ():function(1){
    setArgument(sol,function::getSolution());
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
      for(int i=0; i< val.size1(); i++)
        val(i,0) = sqrt(sol(i,0)*sol(i,0)+sol(i,1)*sol(i,1)+sol(i,2)*sol(i,2)); //Velocity Magnitude
  }
};

class  dgConservationLawStokes3d::diffusivity : public function {
  double _nu;
  public:
  diffusivity(const double nu ):function(4) {
    _nu = nu;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++) {
      val(i,0) = _nu;
      val(i,1) = _nu;
      val(i,2) = _nu;
      val(i,3) = 0.0;
    }
  }
};
//*********************************************
//				GradPsi Term
//*********************************************
class  dgConservationLawStokes3d::gradPsiTerm: public function {
  fullMatrix<double>  sol, solGrad, solPrev, solPrevGrad, speed, source, _dt;
  double _rho, _nu, _UGlob;
  public:
  gradPsiTerm (const function *maxSpeed, const function *previousSol, const function *previousSolGrad,
	       const function *sourceF, const double rho, const double nu, const double UGlob):function(12){
    setArgument(sol,function::getSolution());
    setArgument(solPrev, previousSol);
    setArgument(solPrevGrad, previousSolGrad);
    setArgument(solGrad,function::getSolutionGradient());
    setArgument (_dt, function::getDT());
    setArgument(source,sourceF);
    setArgument(speed, maxSpeed);
    _rho = rho; _nu = nu; _UGlob = UGlob;
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    const dgGroupOfElements &eGroup = *m->getGroupOfElements();
    size_t nQP = val.size1();
    val.setAll(0.0);

    for(size_t i=0; i< nQP; i++) {
      int iElement = m->elementId(i/m->nPointByElement());

      double u = sol(i,0);
      double v = sol(i,1);
      double w = sol(i,2);
      double p = sol(i,3);
      double dt = _dt(i,0);
 
      double dudx = solGrad(i,0);
      double dudy = solGrad(i,1);
      double dudz = solGrad(i,2);
      double dvdx = solGrad(i,3);
      double dvdy = solGrad(i,4);
      double dvdz = solGrad(i,5);
      double dwdx = solGrad(i,6);
      double dwdy = solGrad(i,7);
      double dwdz = solGrad(i,8);
      double dpdx = solGrad(i,9);
      double dpdy = solGrad(i,10);
      double dpdz = solGrad(i,11);

      double s0 = source(i,0);
      double s1 = source(i,1);
      double s2 = source(i,2);

      //old solution
      double uOld = solPrev(i,0);
      double vOld = solPrev(i,1);
      double wOld = solPrev(i,2);
    

      double dudt = (dt > 0.0) ? (u-uOld)/dt: 0.0;
      double dvdt = (dt > 0.0) ? (v-vOld)/dt: 0.0;
      double dwdt = (dt > 0.0) ? (w-wOld)/dt: 0.0;

      double laplU = eGroup.getLaplacianOfFunction(iElement, 0);
      double laplV = eGroup.getLaplacianOfFunction(iElement, 1);
      double laplW = eGroup.getLaplacianOfFunction(iElement, 2);
      if (laplU == -1000) laplU = 0.0;
      if (laplV == -1000) laplV = 0.0;
      if (laplW == -1000) laplW = 0.0;

 // REMOVE THE CONVECTIVE TERMS:
//*********************************************
      //flux_x (multiply dphi/dx)
      val(i,0) =  p/_rho - _nu*dudx; 
      val(i,1) =         - _nu*dvdx; 
      val(i,2) =       	 - _nu*dwdx;
      val(i,3) =  u;
	  //cout << val(i,3)
      // flux_y (multiply dphi/dy)
      val(i,0+4) =        - _nu*dudy; 
      val(i,1+4) = p/_rho - _nu*dvdy;
      val(i,2+4) =        - _nu*dwdy;
      val(i,3+4) = v;

      // flux_z (multiply dphi/dz)
      val(i,0+8) =        - _nu*dudz; 
      val(i,1+8) =        - _nu*dvdz;
      val(i,2+8) = p/_rho - _nu*dwdz;
      val(i,3+8) = w;
//*********************************************


      //double h = 2.*eGroup.getInnerRadius(iElement);
//      double h = sqrt(4*eGroup.getElementVolume(iElement)/3.14); //for triangle elmt in 2D
      double h = pow((8*m->getJacobians().elementVolume(eGroup.elementVectorId())(iElement)/sqrt(2)),(1.0/3.0)); //for tetra elmt in 3D
      double hV = eGroup.getStreamwiseLength(iElement);
      if (hV < 0) hV = h;  //if not computed, set to isotropic length
    
      double alpha = 1.0;
      double tau_DT = (dt > 0.0) ? SQU(2./dt) : 0.0;

      // double tau_SIMPLE_P = alpha*(h/(2.*_UGlob)*std::min(_UGlob*h/(3.*_nu), 1.));
      // double UV = (ULoc == 0.0) ? _UGlob : ULoc ;
      // double tau_SIMPLE_V = alpha*(hV/(2.* UV)*std::min( ULoc*hV/(3.*_nu), 1.));

      double tau_FULL_P = alpha*(1./sqrt( tau_DT + SQU(4.*_nu/(h*h)) + SQU(2.*_UGlob/h) ) );
     
      //add PSPG stab
      double tau_P = tau_FULL_P; 
      eGroup.setPSPGStabilisation(iElement, tau_P);
      val(i,3)    -= tau_P*(dudt+dpdx-_nu*laplU-s0);
      val(i,3+4)  -= tau_P*(dvdt+dpdy-_nu*laplV-s1);
      val(i,3+8)  -= tau_P*(dwdt+dpdz-_nu*laplW-s2); 
//*********************************************
// REMOVE SUPG and GRAD-DIV
/*
      //add SUPG stab
      double tau_V = tau_FULL_V; 
      val(i,0) -= tau_V*(dudt+dpdx-_nu*laplU-s0)*u;
      val(i,1) -= tau_V*(dvdt+dpdy-_nu*laplV-s1)*u;
      val(i,2) -= tau_V*(dwdtdpdz-_nu*laplW-s2)*u;

      val(i,0+4) -= tau_V*(dudt+dpdx-_nu*laplU-s0)*v;
      val(i,1+4) -= tau_V*(dvdt+dpdy-_nu*laplV-s1)*v;
      val(i,2+4) -= tau_V*(dwdt+dpdz-_nu*laplW-s2)*v;

      val(i,0+8) -= tau_V*(dudt+dpdx-_nu*laplU-s0)*w;
      val(i,1+8) -= tau_V*(dvdt+dpdy-_nu*laplV-s1)*w;
      val(i,2+8) -= tau_V*(dwdt+dpdz-_nu*laplW-s2)*w;

      //add GRAD-DIV stab
      double tau_C = alpha*sqrt( SQU(_nu) + SQU(2.*ULoc*h) );
      //double tau_C = alpha*(h*ULoc/std::min(ULoc*h/(6*_nu),3.));
      //double tau_C  = 0.0;
      val(i,0)   -= tau_C*divU;
      val(i,1)   -= 0.0;
      val(i,2)   -= 0.0;

      val(i,0+4) -= 0.0; 
      val(i,1+4) -= tau_C*divU;
      val(i,2+4) -= 0.0;

      val(i,0+8) -= 0.0; 
      val(i,1+8) -= 0.0;
      val(i,2+8) -= tau_C*divU;
*/
//*********************************************

    }
  }
};

//*********************************************
//				Boundary Term
//*********************************************
//is called by newOutsideValueBC to compute fluxes at boundaries
class  dgConservationLawStokes3d::boundaryTerm:public function {
  fullMatrix<double> sol, solPrev,solGrad, normals, speed, source , _dt;
  double _nu,_rho;
public:
  boundaryTerm ( const function *maxSpeed, const function *previousSol, 
		 const function *sourceF, const double rho, const double nu): function(4){
    setArgument(sol,function::getSolution(), 0);
    setArgument(solPrev, previousSol);
    setArgument(solGrad,function::getSolutionGradient());
    setArgument(normals,function::getNormals(), 0);
    setArgument(source,sourceF);
    setArgument(_dt, function::getDT());
    setArgument(speed, maxSpeed);
    _rho = rho;
    _nu = nu;
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    const dgGroupOfElements &eGroup = *m->getGroupOfElements();
    size_t nQP = sol.size1();
    val.setAll(0);
    for(size_t i=0; i< nQP; i++) {
      int iElement = m->elementId(i/m->nPointByElement());
     double nx = normals(i,0);
     double ny = normals(i,1);
     double nz = normals(i,2);

     double u = sol(i,0);
     double v = sol(i,1);
     double w = sol(i,2);
     double p = sol(i,3);

     double uOld = solPrev(i,0);
     double vOld = solPrev(i,1);
     double wOld = solPrev(i,2);

     double dt = _dt(i,0);
     double dudx = solGrad(i,0);
     double dudy = solGrad(i,1);
     double dudz = solGrad(i,2);
     double dvdx = solGrad(i,3);
     double dvdy = solGrad(i,4);
     double dvdz = solGrad(i,5);
     double dwdx = solGrad(i,6);
     double dwdy = solGrad(i,7);
     double dwdz = solGrad(i,8);

     double dudn = (dudx*nx+dudy*ny+dudz*nz);
     double dvdn = (dvdx*nx+dvdy*ny+dvdz*nz);
     double dwdn = (dwdx*nx+dwdy*ny+dwdz*nz);

     double dpdx = solGrad(i,9);
     double dpdy = solGrad(i,10);
     double dpdz = solGrad(i,11);

     double un   = u*nx+v*ny+w*nz;
     double unOld = uOld*nx+vOld*ny+wOld*nz;

     double dundt =  (dt > 0.0) ? (un-unOld)/dt: 0.0;
     double dpdn  = (dpdx*nx+dpdy*ny+dpdz*nz);
     double sn    = source(i,0)*nx + source(i,1)*ny + source(i,2)*nz;

     double laplU = eGroup.getLaplacianOfFunction(iElement, 0);
     double laplV = eGroup.getLaplacianOfFunction(iElement, 1);
     double laplW = eGroup.getLaplacianOfFunction(iElement, 2);

     if (laplU == -1000) laplU = 0.0;
     if (laplV == -1000) laplV = 0.0;
     if (laplW == -1000) laplW = 0.0;
     double laplUN = laplU*nx+laplV*ny+laplW*nz;

 // REMOVE THE CONVECTIVE TERMS:
//*********************************************
     val(i,0) =   -p/_rho*nx + _nu * dudn;
     val(i,1) =   -p/_rho*ny + _nu * dvdn;
     val(i,2) =   -p/_rho*nz + _nu * dwdn ; 
     val(i,3) =   - un ; 
//*********************************************
     //boundary integral modification for PSPG term
     double tau_P = eGroup.getPSPGStabilisation(iElement);
     val(i,3) -= tau_P*(dundt+dpdn-_nu*laplUN-sn);   
   
    }
  }
};

//*********************************************
//				Riemann Term
//*********************************************
//keep this for boundary conditions with ouside values
//compute upwind fluxes
class  dgConservationLawStokes3d::riemann:public function {
  fullMatrix<double> solL, solR, normals, ipTerm ;
  double _rho;
public:

  riemann (const double rho, const function *ipTermF): function(8){
    setArgument(solL,function::getSolution(), 0);
    setArgument(solR,function::getSolution(), 1);
    setArgument(normals,function::getNormals(), 0);
    _rho = rho;
    setArgument (ipTerm, ipTermF);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    printf("arghhh riemann \n");
    size_t nQP = solL.size1();
    for(size_t i=0; i< nQP; i++) {
      double nx = normals(i,0);
      double ny = normals(i,1);
      double nz = normals(i,2);

      double uL = solL(i,0);
      double vL = solL(i,1);
      double wL = solL(i,2);
      double pL = solL(i,3);

      double uR = solR(i,0);
      double vR = solR(i,1);
      double wR = solR(i,2);
      double pR = solR(i,3);

      double unL = (uL*nx+vL*ny+wL*nz);
      double unR = (uR*nx+vR*ny+wR*nz);

      double F1, F2, F3, F4;

      F1 = - 0.5*(unL*uL+unR*uR) - 0.5*(pL+pR)/_rho*nx ;
      F2 = - 0.5*(unL*vL+unR*vR) - 0.5*(pL+pR)/_rho*ny  ;
      F3 = - 0.5*(unL*wL+unR*wR) - 0.5*(pL+pR)/_rho*nz ;
      F4 = - 0.5*(unL+unR) ;
      // double mean = 0.5*(unL + unR) ;
      // double sign = (mean >= 0.0) ? 0.5 : - 0.5;
      // F1 -=sign*(unL*uL-unR*uR);
      // F2 -=sign*(unL*vL-unR*vR);
      double aPlus = 0.5*fabs(F4);
      F1 += aPlus*(uL-uR);
      F2 += aPlus*(vL-vR);
      F3 += aPlus*(wL-wR);

      val(i,0) = F1;
      val(i,1) = F2;
      val(i,2) = F3;
      val(i,3) = F4;
      val(i,4) = -val(i,0);
      val(i,5) = -val(i,1);
      val(i,6) = -val(i,2);
      val(i,7) = -val(i,3);

      // val(i,0) += ipTerm(i,0);
      // val(i,1) += ipTerm(i,1);
      // val(i,2) += ipTerm(i,2);
      // val(i,3) += ipTerm(i,3);
      // val(i,4) += ipTerm(i,4);
      // val(i,5) += ipTerm(i,5);

    }
  }
};

//*********************************************
//				Diffusive Flux
//*********************************************
class  dgConservationLawStokes3d::diffusiveFlux:public function {
  fullMatrix<double> solGrad; 
  double _nu;
  public:

  diffusiveFlux (const double nu): function(12){
    setArgument(solGrad,function::getSolutionGradient());
    _nu = nu;
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++) {

      val(i,0) = - solGrad(i,0)*_nu; //dudx
      val(i,1) = - solGrad(i,3)*_nu; //dvdx
      val(i,2) = - solGrad(i,6)*_nu; //dwdx 
	  val(i,3) = 0.0; //dpdx 
      val(i,4) = - solGrad(i,1)*_nu; //dudy
      val(i,5) = - solGrad(i,4)*_nu; //dvdy
      val(i,6) = - solGrad(i,7)*_nu; //dwdy
 	  val(i,7) = 0.0; //dpdy 
      val(i,8) = - solGrad(i,2)*_nu; //dudz
      val(i,9) = - solGrad(i,5)*_nu; //dvdz
      val(i,10) = - solGrad(i,8)*_nu; //dwdz
	  val(i,11) = 0.0; //dpdz 
    }
  }
};

class  dgConservationLawStokes3d::source: public function {
  fullMatrix<double> _source;
  public :
  source(const function *sourceF): function (3){
    setArgument(_source,sourceF);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP =val.size1();
    for (size_t i=0; i<nQP; i++) {
      val (i,0) = _source(i,0);
      val (i,1) = _source(i,1);
      val (i,2) = _source(i,2);
    }
  }
};

//impose un=0, so that the convective boundary fluxes simplify
class  dgConservationLawStokes3d::boundarySlipWall : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> normals, sol;
    double _rho, _nu;
    public:
    term(const double rho, const double nu):function(4){
      setArgument(sol,function::getSolution());
      setArgument(normals,function::getNormals());
      _rho = rho;
      _nu = nu;
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      size_t nQP = sol.size1();
      for(size_t i=0; i< nQP; i++) {
        double nx = normals(i,0);
        double ny = normals(i,1);
        double nz = normals(i,2);
        double p = sol(i,3);
        val(i,0) = -p/_rho*nx ;
        val(i,1) = -p/_rho*ny ;
		val(i,2) = -p/_rho*nz ;
		val(i,3) = 0;
      }
    }
  };
  term _term;
  public:
  boundarySlipWall( dgConservationLawStokes3d *claw) : _term (claw->getDensity(), claw->getViscosity()) {
    _term0 = &_term;
  }
};

class  dgConservationLawStokes3d::boundaryVelocity : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> normals, sol, vel, solGrad;
    double _rho, _nu;
    public:
    term(const function *velF, const double rho, const double nu):function(4){
      setArgument(sol,function::getSolution());
      setArgument(solGrad,function::getSolutionGradient());
      setArgument(normals,function::getNormals());
      setArgument (vel, velF);
      _rho = rho;  _nu = nu;
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      size_t nQP = sol.size1();
      for(size_t i=0; i< nQP; i++) {
        double nx = normals(i,0);
        double ny = normals(i,1);
        double nz = normals(i,2);
		double u = vel(i,0);
		double v = vel(i,1);
		double w = vel(i,2);
        double p = sol(i,3);
		double dudx = solGrad(i,0);
		double dudy = solGrad(i,1);
		double dudz = solGrad(i,2);
		double dvdx = solGrad(i,3);
		double dvdy = solGrad(i,4);
		double dvdz = solGrad(i,5);
		double dwdx = solGrad(i,6);
		double dwdy = solGrad(i,7);
		double dwdz = solGrad(i,8);

		const double dudn = (dudx*nx+dudy*ny+dudz*nz);
		const double dvdn = (dvdx*nx+dvdy*ny+dvdz*nz);
		const double dwdn = (dwdx*nx+dwdy*ny+dwdz*nz);
		double un = u*nx+v*ny+w*nz;

// REMOVE THE CONVECTIVE TERMS:
//*********************************************
		val(i,0) = -p/_rho*nx + _nu * dudn ;
		val(i,1) = -p/_rho*ny + _nu * dvdn ;
		val(i,2) = -p/_rho*nz + _nu * dwdn ;
		val(i,3) =   -un  ; 
//*********************************************
      }
    }
  };
  term _term;
  public:
  boundaryVelocity( dgConservationLawStokes3d *claw, const function *velF):  _term (velF, claw->getDensity(),  claw->getViscosity()) {
    _term0 = &_term;
  }
};

class  dgConservationLawStokes3d::boundaryPressure : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> normals, sol, pres, solGrad;
    double _rho, _nu;
    public:
    term(const function *presF, const double rho, const double nu):function(4){
      setArgument(sol,function::getSolution());
      setArgument(solGrad,function::getSolutionGradient());
      setArgument(normals,function::getNormals());
      setArgument (pres, presF);
      _rho = rho;  _nu = nu;
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      size_t nQP = sol.size1();
      for(size_t i=0; i< nQP; i++) {
        double nx = normals(i,0);
        double ny = normals(i,1);
        double nz = normals(i,2);

		double u = sol(i,0);
		double v = sol(i,1);
		double w = sol(i,2);
        double p = pres(i,0);

		double dudx = solGrad(i,0);
		double dudy = solGrad(i,1);
		double dudz = solGrad(i,2);
		double dvdx = solGrad(i,3);
		double dvdy = solGrad(i,4);
		double dvdz = solGrad(i,5);
		double dwdx = solGrad(i,6);
		double dwdy = solGrad(i,7);
		double dwdz = solGrad(i,8);

		const double dudn = (dudx*nx+dudy*ny+dudz*nz);
		const double dvdn = (dvdx*nx+dvdy*ny+dvdz*nz);
		const double dwdn = (dwdx*nx+dwdy*ny+dwdz*nz);
		double un = u*nx+v*ny+w*nz;

// REMOVE THE CONVECTIVE TERMS:
//*********************************************
		val(i,0) = -p/_rho*nx + _nu * dudn ;
		val(i,1) = -p/_rho*ny + _nu * dvdn ;
		val(i,2) = -p/_rho*nz + _nu * dwdn ;
		val(i,3) =   -un  ; 
//*********************************************
; 
      }
    }
  };
  term _term;
  public:
  boundaryPressure( dgConservationLawStokes3d *claw, const function *presF):  _term (presF, claw->getDensity(), claw->getViscosity()) {
    _term0 = &_term;
  }
};

dgBoundaryCondition * dgConservationLawStokes3d::newBoundarySlipWall(){
  return new boundarySlipWall(this);
}
dgBoundaryCondition * dgConservationLawStokes3d::newBoundaryVelocity(const function *VEL){
  return new boundaryVelocity(this, VEL);
}
dgBoundaryCondition * dgConservationLawStokes3d::newBoundaryPressure(const function *PRES){
  return new boundaryPressure(this, PRES);
}

void  dgConservationLawStokes3d::setup() {
  _maximumConvectiveSpeed[""] = new maxConvectiveSpeed();
  _diffusivity[""] = new diffusivity (_nu);
  _volumeTerm0[""] = new source(_source);
  _volumeTerm1[""] = new gradPsiTerm(_maximumConvectiveSpeed[""], _prevSol, _prevSolGrad,
				     _source, _rho, _nu, _UGlob);
  _velocitySol = new velocityVector();
  _diffusiveFlux = (_nu != 0.0) ? new diffusiveFlux (_nu) : NULL;

  //the dgBC::newOutsideValue is called to fix strongBC and calls interfaceTerm0 for the boundaryFlux 
  _interfaceTerm0[""] = new boundaryTerm(_maximumConvectiveSpeed[""], _prevSol,
					 _source, _rho,_nu); 

  _ipTerm = _nu ? dgNewIpTerm (_nbf, _diffusiveFlux, _diffusivity[""]) : NULL;
  //_interfaceTerm0[""] = _ipTerm;
  //_interfaceTerm0[""] = new riemann(_rho, _ipTerm);
 
}

 dgConservationLawStokes3d:: dgConservationLawStokes3d(): dgConservationLawFunction(4) {
  // u,v,w,p
  _fzero = new functionConstant(0.);
  _fzerov = new functionConstant(std::vector<double>(3,0));
  _fzerograd = new functionConstant(std::vector<double>(12,0));
  //_fzerograd = new functionConstant(std::vector<double>(9,0)); 
  _source = _fzerov;
  _prevSol = _fzerov;
  _prevSolGrad = _fzerograd;
  _nu = 0.0;
  _rho = 1.0;
  _UGlob = 1.0;
  _ipTerm = NULL;
  temporal[0] = true; // for u
  temporal[1] = true; // for v
  temporal[2] = true; // for w
  temporal[3] = false; // for p
}

 dgConservationLawStokes3d::~ dgConservationLawStokes3d() {
  delete _fzero;
  delete _fzerov;
  delete _fzerograd;
  if (_ipTerm) delete _ipTerm;
  if (_diffusivity[""]) delete _diffusivity[""];
  if (_volumeTerm0[""]) delete _volumeTerm0[""];
  if (_volumeTerm1[""]) delete _volumeTerm1[""];
  if (_interfaceTerm0[""]) delete _interfaceTerm0[""];
  if (_maximumConvectiveSpeed[""]) delete _maximumConvectiveSpeed[""];
}
