#include "dgConservationLawNavierStokesIncomp3d_EM.h"
#include "function.h"
#include "float.h"

class dgConservationLawNavierStokesIncomp3d_EM::velocityVector : public function{
  fullMatrix<double>  solution;
public :
  velocityVector() : function(3)  {
    setArgument (solution, function::getSolution());
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP =val.size1();
    for (size_t i=0; i<nQP; i++) {
      val (i,0) = solution(i,0);
      val (i,1) = solution(i,1);
      val (i,2) = solution(i,2);
    }
  }
};

class dgConservationLawNavierStokesIncomp3d_EM::maxConvectiveSpeed: public function {
  fullMatrix<double> sol;
  public:
  maxConvectiveSpeed ():function(1){
    setArgument(sol,function::getSolution());
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
      for(int i=0; i< val.size1(); i++)
        val(i,0) = sqrt(sol(i,0)*sol(i,0)+sol(i,1)*sol(i,1)+sol(i,2)*sol(i,2)); 
  }
};

class dgConservationLawNavierStokesIncomp3d_EM::diffusivity : public function {
  double _nu;
  public:
  diffusivity(const double nu ):function(4) {
    _nu = nu;
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++) {
      val(i,0) = _nu;
      val(i,1) = _nu;
      val(i,2) = _nu;
      val(i,3) = 0.0;
    }
  }
};

class dgConservationLawNavierStokesIncomp3d_EM::gradPsiTerm: public function {
  fullMatrix<double>  sol, diff, solGrad, speed;
  dgConservationLawNavierStokesIncomp3d_EM *law;
  bool _isDiffusive;
  double _rho;
  public:
  gradPsiTerm (const function *diffusiveFlux,  const function *maxSpeed, const double rho):function(12){
    setArgument(sol,function::getSolution());
    setArgument(solGrad,function::getSolutionGradient());
    _isDiffusive = diffusiveFlux;
    if (_isDiffusive) setArgument(diff,diffusiveFlux);
    setArgument(speed,maxSpeed);
    _rho = rho;
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();

    for(size_t i=0; i< nQP; i++) {
      double u = sol(i,0);
      double v = sol(i,1);
      double w = sol(i,2);
      double p = sol(i,3);

      // flux_x
      val(i,0) = u*u + p/_rho;
      val(i,1) = u*v;
      val(i,2) = u*w;
      val(i,3) = u;

      // flux_y
      val(i,4) = v*u;
      val(i,5) = v*v + p/_rho;
      val(i,6) = v*w;
      val(i,7) = v;
    
      // flux_z
      val(i,8) = w*u;
      val(i,9) = w*v;
      val(i,10) = w*w + p/_rho;
      val(i,11) = w;

      //add diffusive part
      if (_isDiffusive) {
        val(i,0) -= diff(i,0);
        val(i,1) -= diff(i,1);
        val(i,2) -= diff(i,2);
        val(i,4) -= diff(i,4);
	val(i,5) -= diff(i,5);
	val(i,6) -= diff(i,6);
	val(i,8) -= diff(i,8);
	val(i,9) -= diff(i,9);
	val(i,10)-= diff(i,10);
      }

      double hSize = 0.1;

      //add PSPG stab
      double U = 1.0;
      double tau_P = hSize/(2.*U);
      val(i,3)  -= tau_P*solGrad(i,8);//dpdx
      val(i,7)  -= tau_P*solGrad(i,9);//dpdy
      val(i,11) -= tau_P*solGrad(i,10);//dpdz

      //add SUPG stab
      double Uloc = 1.0; //speed(i,0);
      double tau_V = hSize/(2.*Uloc);
      double UGradu = u*solGrad(i,0)+v*solGrad(i,1)+w*solGrad(i,2);
      double UGradv = u*solGrad(i,3)+v*solGrad(i,4)+w*solGrad(i,5);
      double UGradw = u*solGrad(i,6)+v*solGrad(i,7)+w*solGrad(i,8);
      val(i,0) -= tau_V*UGradu*u;
      val(i,1) -= tau_V*UGradv*u;
      val(i,2) -= tau_V*UGradw*u;
      val(i,4) -= tau_V*UGradu*v;
      val(i,5) -= tau_V*UGradv*v;
      val(i,6) -= tau_V*UGradw*v;
      val(i,8) -= tau_V*UGradu*w;
      val(i,9) -= tau_V*UGradv*w;
      val(i,10)-= tau_V*UGradw*w;

    }
  }
};


class dgConservationLawNavierStokesIncomp3d_EM::source: public function {
  fullMatrix<double> sol, solGradient, _source, _xyz;
  dgConservationLawNavierStokesIncomp3d_EM *law;
  public :
  source(const function *sourceF) :
    function (4){
    setArgument(sol,function::getSolution());
    setArgument(solGradient,function::getSolutionGradient());
    setArgument(_source,sourceF);
    setArgument(_xyz,getFunctionCoordinates());
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP =val.size1();
    for (size_t i=0; i<nQP; i++) {
      double u = sol(i,1);
      double v = sol(i,2);
      val (i,0) = 0.0; //_source(i,0);
      val (i,1) = 0.0; //_source(i,1);
      val (i,2) = 0.0; //_source(i,2);
      val (i,3) = 0.0; //_source(i,3);
    }
  }
};

//keep this for boundary conditions with ouside values
class dgConservationLawNavierStokesIncomp3d_EM::riemann:public function {
  fullMatrix<double> solL, solR;
  dgConservationLawNavierStokesIncomp3d_EM *law;
public:
  riemann (): function(8){
    setArgument(solL,function::getSolution(), 0);
    setArgument(solR,function::getSolution(), 1);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = solL.size1();
    for(size_t i=0; i< nQP; i++) {
      double F1, F2, F3, F4;
      F1 = 0.0;  F2 = 0.0;
      F3 = 0.0;  F4 = 0.0;
      val(i,0) = F1;
      val(i,1) = F2;
      val(i,2) = F3;
      val(i,3) = F4;
      val(i,4) = -val(i,0);
      val(i,5) = -val(i,1);
      val(i,6) = -val(i,2);
      val(i,7) = -val(i,2); //really two ??
    }
  }
};

class dgConservationLawNavierStokesIncomp3d_EM::diffusiveFlux:public function {
  fullMatrix<double> solGrad; 
  double _nu;
  public:
  diffusiveFlux (const double nu): function(12){
    setArgument(solGrad,function::getSolutionGradient());
    _nu = nu;
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++) {
      val(i,0) = _nu*solGrad(i,0); //dudx
      val(i,1) = _nu*solGrad(i,1); //dvdx
      val(i,2) = _nu*solGrad(i,2); //dwdx
      val(i,3) = 0.0;
      val(i,4) = _nu*solGrad(i,4);//dudy
      val(i,5) = _nu*solGrad(i,5);//dvdy
      val(i,6) = _nu*solGrad(i,6);//dwdz
      val(i,7) = 0.0;
      val(i,8) = _nu*solGrad(i,8);//dudz
      val(i,9) = _nu*solGrad(i,9);//dvdz
      val(i,10) = _nu*solGrad(i,10);//dwdz
      val(i,11) = 0.0;
    }
  }
};

class dgConservationLawNavierStokesIncomp3d_EM::boundaryWall : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> normals, sol, solGrad;
    double _rho, _nu;
    public:
    term(const double rho, const double nu):function(4){
      setArgument(sol,function::getSolution());
      setArgument(solGrad,function::getSolutionGradient());
      setArgument(normals,function::getNormals());
      _rho = rho;   _nu = nu;
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      size_t nQP = sol.size1();
      for(size_t i=0; i< nQP; i++) {
        double nx = normals(i,0);
        double ny = normals(i,1);
        double nz = normals(i,2);
        double p = sol(i,3);
        double gradun = solGrad(i,0)*nx+solGrad(i,4)*ny+solGrad(i,8)*nz;
        double gradvn = solGrad(i,1)*nx+solGrad(i,5)*ny+solGrad(i,9)*nz;
        double gradwn = solGrad(i,2)*nx+solGrad(i,6)*ny+solGrad(i,10)*nz;
        val(i,0) = -p/_rho*nx + _nu *gradun;
        val(i,1) = -p/_rho*ny + _nu *gradvn;
        val(i,2) = -p/_rho*nz + _nu *gradwn;
        val(i,3) = 0;
      }
    }
  };
  term _term;
  public:
  boundaryWall(dgConservationLawNavierStokesIncomp3d *claw) : _term (claw->getDensity(), claw->getViscosity()) {
    _term0 = &_term;
  }
};

class dgConservationLawNavierStokesIncomp3d_EM::boundaryVelocity : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> normals, sol, vel;
    double _rho;
    public:
    term(const function *velF, const double rho):function(4){
      setArgument(sol,function::getSolution());
      setArgument(normals,function::getNormals());
      setArgument (vel, velF);
      _rho = rho;
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      size_t nQP = sol.size1();
      for(size_t i=0; i< nQP; i++) {
        double nx = normals(i,0);
        double ny = normals(i,1);
        double nz = normals(i,2);
        double u = vel(i,0);
        double v = vel(i,1);
        double w = vel(i,2);
        double p = sol(i,3);
        double un = (u*nx+v*ny+w*nz);
        val(i,0) = -u*un-p/_rho*nx; //add diffusive bc
        val(i,1) = -v*un-p/_rho*ny; //add diffusive bc
        val(i,2) = -w*un-p/_rho*nz; //add diffusive bc
        val(i,3) = -un;
      }
    }
  };
  term _term;
  public:
  boundaryVelocity(dgConservationLawNavierStokesIncomp3d *claw, const function *velF):  _term (velF, claw->getDensity()) {
    _term0 = &_term;
  }

};

class dgConservationLawNavierStokesIncomp3d_EM::boundaryPressure : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> normals, sol, solGrad, pres;
    double _rho, _nu;
    public:
    term(const function *presF, const double rho, const double nu):function(4){
      setArgument(sol,function::getSolution());
      setArgument(solGrad,function::getSolutionGradient());
      setArgument(normals,function::getNormals());
      setArgument (pres, presF);
      _rho = rho;
      _nu = nu;
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      size_t nQP = sol.size1();
      for(size_t i=0; i< nQP; i++) {
        double nx = normals(i,0);
        double ny = normals(i,1);
        double nz = normals(i,2);
        double u = sol(i,0);
        double v = sol(i,1);
        double w = sol(i,2);
        double p = pres(i,0);
        double gradun = solGrad(i,0)*nx+solGrad(i,4)*ny+solGrad(i,8)*nz;
        double gradvn = solGrad(i,1)*nx+solGrad(i,5)*ny+solGrad(i,9)*nz;
        double gradwn = solGrad(i,2)*nx+solGrad(i,6)*ny+solGrad(i,10)*nz;
        double un = u*nx+v*ny+w*nz;
        val(i,0) = -u*un-p/_rho*nx + _nu*gradun;
        val(i,1) = -v*un-p/_rho*ny + _nu*gradvn;
        val(i,2) = -w*un-p/_rho*nz + _nu*gradwn;
        val(i,3) = -un;
      }
    }
  };
  term _term;
  public:
  boundaryPressure(dgConservationLawNavierStokesIncomp3d *claw, const function *presF):  _term (presF, claw->getDensity(), claw->getViscosity()) {
    _term0 = &_term;
  }

};

dgBoundaryCondition *dgConservationLawNavierStokesIncomp3d::newBoundaryWall(){
  return new boundaryWall(this);
}

dgBoundaryCondition *dgConservationLawNavierStokesIncomp3d::newBoundaryVelocity(const function *VEL){
  return new boundaryVelocity(this, VEL);
}

dgBoundaryCondition *dgConservationLawNavierStokesIncomp3d::newBoundaryPressure(const function *PRES){
  return new boundaryPressure(this, PRES);
}

void dgConservationLawNavierStokesIncomp3d_EM::setup() {
  _diffusiveFlux = (_nu != 0.0) ? new diffusiveFlux (_nu) : NULL;
  _diffusivity[""] = new diffusivity (_nu);
  _ipTerm = _nu ? dgNewIpTerm (_nbf, _diffusiveFlux, _diffusivity[""]) : NULL;
  _maximumConvectiveSpeed[""] = new maxConvectiveSpeed();
  _volumeTerm0[""] = new source(_source);
  _volumeTerm1[""] = new gradPsiTerm(_diffusiveFlux, _maximumConvectiveSpeed[""], _rho);
  _interfaceTerm0[""] = _ipTerm;
  _velocitySol = new velocityVector();
}

dgConservationLawNavierStokesIncomp3d::dgConservationLawNavierStokesIncomp3d(): dgConservationLawFunction(4) {
  // u,v,w,p
  _fzero = new functionConstant(0.);
  _fzerov = new functionConstant(std::vector<double>(4,0));
  _source = _fzerov;
  _nu = 0.0;
  _rho = 1.0;
  _ipTerm = NULL;
  temporal[0] = true;
  temporal[1] = true;
  temporal[2] = true;
  temporal[3] = false;
}

dgConservationLawNavierStokesIncomp3d::~dgConservationLawNavierStokesIncomp3d() {
  delete _fzero;
  delete _fzerov;
  if (_ipTerm) delete _ipTerm;
  if (_diffusivity[""]) delete _diffusivity[""];
  if (_volumeTerm0[""]) delete _volumeTerm0[""];
  if (_volumeTerm1[""]) delete _volumeTerm1[""];
  if (_interfaceTerm0[""]) delete _interfaceTerm0[""];
  if (_maximumConvectiveSpeed[""]) delete _maximumConvectiveSpeed[""];
}
