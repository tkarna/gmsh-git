%module navierStokesIncomp

%{
  #undef HAVE_DLOPEN
  #include "dgConservationLawNavierStokes1d.h"
  #include "dgConservationLawNavierStokesIncomp2d.h"
  #include "dgConservationLawNavierStokesIncomp3d.h"
  #include "dgConservationLawStokes3d.h"
%}

%import(module="dgpy.dgCommon") "dgCommon.i"
%import(module="dgpy.conservationLaw") "dgConservationLaw.h";
%import(module="dgpy.conservationLaw") "dgConservationLawFunction.h";

%include "dgConservationLawNavierStokes1d.h"
%include "dgConservationLawNavierStokesIncomp2d.h"
%include "dgConservationLawNavierStokesIncomp3d.h"
%include "dgConservationLawStokes3d.h"
