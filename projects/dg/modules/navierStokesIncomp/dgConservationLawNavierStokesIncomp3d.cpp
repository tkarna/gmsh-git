#include "dgConservationLawNavierStokesIncomp3d.h"
#include "dgConservationLawFunction.h"
#include "dgGroupOfElements.h"
#include "function.h"
#include "float.h"
#include "nodalBasis.h"
#include "dgMeshJacobian.h"
#include "Numeric.h"

class  dgConservationLawNavierStokesIncomp3d::velocityVector : public function{
  fullMatrix<double>  solution;
public :
  velocityVector() : function(3)  {
    setArgument (solution, function::getSolution());
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP =val.size1();
    for (size_t i=0; i<nQP; i++) {
      val (i,0) = solution(i,0);
      val (i,1) = solution(i,1);
      val (i,2) = solution(i,2);

    }
  }
};

class dgConservationLawNavierStokesIncomp3d::viscousNormalStress : public function{// Deviatoric term: 2*mu*D*normal
  fullMatrix<double>  normals, solGrad;
  fullMatrix<double> mu; 
public :
  viscousNormalStress(const function* rhoF, const function* muF,const function *gradFunc = NULL ) : function(3)  {
    setArgument(normals,function::getNormals(), 0);
    setArgument (solGrad,gradFunc ? gradFunc : function::getSolutionGradient());
    setArgument(mu,muF);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP =val.size1();
    for (size_t i=0; i<nQP; i++) {
      double nx = -normals(i,0);
      double ny = -normals(i,1);
      double nz = -normals(i,2);
      
      double dudx = solGrad(i,0);
      double dudy = solGrad(i,1);
      double dudz = solGrad(i,2);
      double dvdx = solGrad(i,3);
      double dvdy = solGrad(i,4);
      double dvdz = solGrad(i,5);
      double dwdx = solGrad(i,6);
      double dwdy = solGrad(i,7);
      double dwdz = solGrad(i,8);
      
      double _mu = mu(i,0);
      val (i,0) = _mu* (2.*dudx*nx     +  (dudy+dvdx)*ny   + (dudz+dwdx)*nz);
      val (i,1) = _mu* ((dvdx+dudy)*nx +  2.*dvdy*ny       + (dvdz+dwdy)*nz);
      val (i,2) = _mu* ((dwdx+dudz)*nx +  (dwdy+dvdz)*ny   + 2.*dwdz*nz    );
    }
  }
};



class dgConservationLawNavierStokesIncomp3d::inviscousNormalStress : public function{// pressure term: -p*I*normal
  fullMatrix<double>  solution, normals;
public :
  inviscousNormalStress() : function(3)  {
    setArgument (solution, function::getSolution());
    setArgument(normals,function::getNormals(), 0);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP =val.size1();
    for (size_t i=0; i<nQP; i++) {
      double nx = -normals(i,0);
      double ny = -normals(i,1);
      double nz = -normals(i,2);
      double p =  solution(i,3);
      val (i,0) = - p*nx;
      val (i,1) = - p*ny;
      val (i,2) = - p*nz;
    }
  }
};

class dgConservationLawNavierStokesIncomp3d::wallShearStress : public function{ // tau = abs(tangential traction) 
  fullMatrix<double>   normals, solGrad;
  fullMatrix<double>  mu; 
public :
  wallShearStress(const function* rhoF, const function* muF,const function *gradFunc = NULL) : function(3)  {
    setArgument(normals,function::getNormals(), 0);  
    setArgument (solGrad,gradFunc ? gradFunc : function::getSolutionGradient());
    setArgument(mu,muF);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP =val.size1();
    for (size_t i=0; i<nQP; i++) {
      double nx = normals(i,0);
      double ny = normals(i,1);
      double nz = normals(i,2);
      
      double dudx = solGrad(i,0);
      double dudy = solGrad(i,1);
      double dudz = solGrad(i,2);
      double dvdx = solGrad(i,3);
      double dvdy = solGrad(i,4);
      double dvdz = solGrad(i,5);
      double dwdx = solGrad(i,6);
      double dwdy = solGrad(i,7);
      double dwdz = solGrad(i,8);
      
      double _mu = mu(i,0);
      double t1 = _mu* (2.*dudx*nx     +  (dudy+dvdx)*ny   + (dudz+dwdx)*nz);
      double t2 = _mu* ((dvdx+dudy)*nx +  2.*dvdy*ny       + (dvdz+dwdy)*nz);
      double t3 = _mu* ((dwdx+dudz)*nx +  (dwdy+dvdz)*ny   + 2.*dwdz*nz    );
      double tn = t1*nx+t2*ny+t3*nz;

      val (i,0) = t1 - tn*nx;
      val (i,1) = t2 - tn*ny;
      val (i,2) = t3 - tn*nz;
    }
  }
};

class dgConservationLawNavierStokesIncomp3d::wallShearStressNorm : public function{
  fullMatrix<double> wss;
public :
  wallShearStressNorm(const function* wallShearStress) : function(1)  {
    setArgumentWrapped (wss, wallShearStress, 3, "wall shear stress", 0);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    for (size_t i = 0; i < nQP; i++) {
      val (i, 0) = sqrt(wss(i,0)*wss(i,0) + wss(i,1)*wss(i,1) + wss(i,2)*wss(i,2));
    }
  }
};

class  dgConservationLawNavierStokesIncomp3d::maxConvectiveSpeed: public function {
  fullMatrix<double> sol;
  public:
  maxConvectiveSpeed ():function(1){
    setArgument(sol,function::getSolution());
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
      for(int i=0; i< val.size1(); i++)
        val(i,0) = sqrt(sol(i,0)*sol(i,0)+sol(i,1)*sol(i,1)+sol(i,2)*sol(i,2)); 
  }
};

class  dgConservationLawNavierStokesIncomp3d::gradPsiTerm: public function {
  fullMatrix<double>  sol, solGrad, solPrev, solPrevGrad, speed, source, _dt;
  bool _haveSource;
  double  _UGlob;
  fullMatrix<double> rho, mu;
  public:
  gradPsiTerm (const function *maxSpeed, const function *previousSol, const function *previousSolGrad,
	       const function *sourceF, const function* rhoF, const function* muF, const double UGlob):function(12){
    setArgument(sol,function::getSolution());
    setArgument(solPrev, previousSol);
    setArgument(solPrevGrad, previousSolGrad);
    setArgument(solGrad,function::getSolutionGradient());
    setArgument (_dt, function::getDT());
    setArgument(mu,muF);
    setArgument(rho, rhoF);
    if (sourceF){
      _haveSource = true;
      setArgument(source,sourceF);
     }
     else
       _haveSource = false;
    setArgument(speed, maxSpeed);
    _UGlob = UGlob;
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    const dgGroupOfElements &eGroup = *m->getGroupOfElements();
    size_t nQP = val.size1();
    val.setAll(0.0);
    for(size_t i=0; i< nQP; i++) {
      int iElement = m->elementId(i/m->nPointByElement());

      double u = sol(i,0);
      double v = sol(i,1);
      double w = sol(i,2);
      double p = sol(i,3);
      double dt = _dt(i,0);
 
      double dudx = solGrad(i,0);
      double dudy = solGrad(i,1);
      double dudz = solGrad(i,2);
      double dvdx = solGrad(i,3);
      double dvdy = solGrad(i,4);
      double dvdz = solGrad(i,5);
      double dwdx = solGrad(i,6);
      double dwdy = solGrad(i,7);
      double dwdz = solGrad(i,8);
      double dpdx = solGrad(i,9);
      double dpdy = solGrad(i,10);
      double dpdz = solGrad(i,11);

      double UGradu = u*dudx+v*dudy+w*dudz;
      double UGradv = u*dvdx+v*dvdy+w*dvdz; 
      double UGradw = u*dwdx+v*dwdy+w*dwdz; 
      double divU  = dudx+dvdy+dwdz;

      double s0=0.0, s1=0.0, s2=0.0;
      if (_haveSource){
        s0 = source(i,0);
        s1 = source(i,1);
        s2 = source(i,2);
      }

      //old solution
      double uOld = solPrev(i,0);
      double vOld = solPrev(i,1);
      double wOld = solPrev(i,2);
    
      double dudt = (dt > 0.0) ? (u-uOld)/dt: 0.0;
      double dvdt = (dt > 0.0) ? (v-vOld)/dt: 0.0;
      double dwdt = (dt > 0.0) ? (w-wOld)/dt: 0.0;

      double laplU = eGroup.getLaplacianOfFunction(iElement, 0);
      double laplV = eGroup.getLaplacianOfFunction(iElement, 1);
      double laplW = eGroup.getLaplacianOfFunction(iElement, 2);
      if (laplU == -1000) laplU = 0.0;
      if (laplV == -1000) laplV = 0.0;
      if (laplW == -1000) laplW = 0.0;

      double _rho = rho(i,0);
      double _nu = mu(i,0)/_rho;
      //flux_x (multiply dphi/dx)
      val(i,0) =  p/_rho - _nu*dudx +  u*u ; 
      val(i,1) =         - _nu*dvdx +  v*u ; 
      val(i,2) =       	 - _nu*dwdx +  w*u ;
      val(i,3) =  u;

      // flux_y (multiply dphi/dy)
      val(i,0+4) =        - _nu*dudy + u*v; 
      val(i,1+4) = p/_rho - _nu*dvdy + v*v;
      val(i,2+4) =        - _nu*dwdy + w*v;
      val(i,3+4) = v;

      // flux_z (multiply dphi/dz)
      val(i,0+8) =        - _nu*dudz + u*w; 
      val(i,1+8) =        - _nu*dvdz + v*w;
      val(i,2+8) = p/_rho - _nu*dwdz + w*w;
      val(i,3+8) = w;

      double h = 1.5*pow(m->getJacobians().elementVolume(eGroup.elementVectorId())(iElement)/sqrt(3.0),1.0/3.0);
      double hV = eGroup.getStreamwiseLength(iElement);
      if (hV <0) hV= h;
      double ULoc = speed(i,0);
    
      double alpha = 1.0;
      double tau_DT = (dt > 0.0) ? SQU(2./dt) : 0.0;

      //double tau_SIMPLE_P = alpha*(h/(2.*_UGlob)*std::min(_UGlob*h/(3.*_nu), 1.));
      //double UV = (ULoc == 0.0) ? _UGlob : ULoc ;
      //double tau_SIMPLE_V = alpha*(hV/(2.* UV)*std::min( ULoc*hV/(3.*_nu), 1.));

      double tau_FULL_P = alpha*(1./sqrt( tau_DT + SQU(4.*_nu/(h*h)) + SQU(2.*_UGlob/h) ) );
      double tau_FULL_V = alpha*(1./sqrt( tau_DT + SQU(4.*_nu/(hV*hV)) + SQU(2.*ULoc/hV) ) );
     
      //add PSPG stab
      double tau_P = tau_FULL_P; 
      eGroup.setPSPGStabilisation(iElement, tau_P);
      val(i,3)    -= tau_P*(dudt+UGradu+dpdx/_rho-_nu*laplU-s0);
      val(i,3+4)  -= tau_P*(dvdt+UGradv+dpdy/_rho-_nu*laplV-s1);
      val(i,3+8)  -= tau_P*(dwdt+UGradw+dpdz/_rho-_nu*laplW-s2); 

      //add SUPG stab
      double tau_V = tau_FULL_V; 
      val(i,0) -= tau_V*(dudt+UGradu+dpdx/_rho-_nu*laplU-s0)*u;
      val(i,1) -= tau_V*(dvdt+UGradv+dpdy/_rho-_nu*laplV-s1)*u;
      val(i,2) -= tau_V*(dwdt+UGradw+dpdz/_rho-_nu*laplW-s2)*u;

      val(i,0+4) -= tau_V*(dudt+UGradu+dpdx/_rho-_nu*laplU-s0)*v;
      val(i,1+4) -= tau_V*(dvdt+UGradv+dpdy/_rho-_nu*laplV-s1)*v;
      val(i,2+4) -= tau_V*(dwdt+UGradw+dpdz/_rho-_nu*laplW-s2)*v;

      val(i,0+8) -= tau_V*(dudt+UGradu+dpdx/_rho-_nu*laplU-s0)*w;
      val(i,1+8) -= tau_V*(dvdt+UGradv+dpdy/_rho-_nu*laplV-s1)*w;
      val(i,2+8) -= tau_V*(dwdt+UGradw+dpdz/_rho-_nu*laplW-s2)*w;

      //add GRAD-DIV stab
      double tau_C = alpha*sqrt( SQU(_nu) + SQU(2.*ULoc*h) );
      //double tau_C = alpha*(h*ULoc/std::min(ULoc*h/(6*_nu),3.));
      //double tau_C  = 0.0;
      val(i,0)   -= tau_C*divU;
      val(i,1)   -= 0.0;
      val(i,2)   -= 0.0;

      val(i,0+4) -= 0.0; 
      val(i,1+4) -= tau_C*divU;
      val(i,2+4) -= 0.0;

      val(i,0+8) -= 0.0; 
      val(i,1+8) -= 0.0;
      val(i,2+8) -= tau_C*divU;

    }
  }
};

//is called by newOutsideValueBC to compute fluxes at boundaries
class  dgConservationLawNavierStokesIncomp3d::boundaryTerm:public function {
  fullMatrix<double> sol, solPrev,solGrad, normals, speed, source , _dt;
  fullMatrix<double> rho, mu;
public:
  boundaryTerm ( const function *maxSpeed, const function *previousSol, 
		 const function *sourceF, const function* rhoF, const function* muF): function(4){
    setArgument(sol,function::getSolution(), 0);
    setArgument(solPrev, previousSol);
    setArgument(solGrad,function::getSolutionGradient());
    setArgument(normals,function::getNormals(), 0);
    setArgument(mu,muF);
    setArgument(rho, rhoF);
    setArgument(source,sourceF);
    setArgument(_dt, function::getDT());
    setArgument(speed, maxSpeed);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    const dgGroupOfElements &eGroup = *m->getGroupOfElements();
    size_t nQP = sol.size1();
    val.setAll(0);
    for(size_t i=0; i< nQP; i++) {
     int iElement = m->elementId(i/m->nPointByElement());
     double nx = normals(i,0);
     double ny = normals(i,1);
     double nz = normals(i,2);

     double u = sol(i,0);
     double v = sol(i,1);
     double w = sol(i,2);
     double p = sol(i,3);

     double dudx = solGrad(i,0);
     double dudy = solGrad(i,1);
     double dudz = solGrad(i,2);
     double dvdx = solGrad(i,3);
     double dvdy = solGrad(i,4);
     double dvdz = solGrad(i,5);
     double dwdx = solGrad(i,6);
     double dwdy = solGrad(i,7);
     double dwdz = solGrad(i,8);

     double dudn = (dudx*nx+dudy*ny+dudz*nz);
     double dvdn = (dvdx*nx+dvdy*ny+dvdz*nz);
     double dwdn = (dwdx*nx+dwdy*ny+dwdz*nz);

     double un   = u*nx+v*ny+w*nz;

     double laplU = eGroup.getLaplacianOfFunction(iElement, 0);
     double laplV = eGroup.getLaplacianOfFunction(iElement, 1);
     double laplW = eGroup.getLaplacianOfFunction(iElement, 2);

     if (laplU == -1000) laplU = 0.0;
     if (laplV == -1000) laplV = 0.0;
     if (laplW == -1000) laplW = 0.0;
     double _rho = rho(i,0);
     double _nu = mu(i,0)/_rho; 

     val(i,0) =   - u*un -p/_rho*nx + _nu * dudn;
     val(i,1) =   - v*un -p/_rho*ny + _nu * dvdn;
     val(i,2) =   - w*un -p/_rho*nz + _nu * dwdn ; 
     val(i,3) =   - un ; 

     //boundary integral modification for PSPG term
     //double tau_P = eGroup.getPSPGStabilisation(iElement);
     //val(i,3) -= tau_P*(dundt+UGradun+dpdn/_rho-_nu*laplUN-sn);   
   
    }
  }
};

class dgConservationLawNavierStokesIncomp3d::source: public function {
  fullMatrix<double> _source;
public :
  source(const function *sourceF): function (4){
    setArgument(_source,sourceF);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP =val.size1();
    for (size_t i=0; i<nQP; i++) {
      val (i,0) = _source(i,0);
      val (i,1) = _source(i,1);
      val (i,2) = _source(i,2);
      val (i,3) = _source(i,3);
    }
  }
};

//impose un=0, so that the convective boundary fluxes simplify
class  dgConservationLawNavierStokesIncomp3d::boundarySlipWall : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> normals, sol;
    fullMatrix<double> rho, mu;
    public:
    term(const function* rhoF, const function* muF):function(4){
      setArgument(sol,function::getSolution());
      setArgument(normals,function::getNormals());
      setArgument(mu,muF);
      setArgument(rho, rhoF);
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      size_t nQP = sol.size1();
      for(size_t i=0; i< nQP; i++) {
        double nx = normals(i,0);
        double ny = normals(i,1);
        double nz = normals(i,2);
        double p = sol(i,3);
	double _rho = rho(i,0);
        val(i,0) = -p/_rho*nx ;
        val(i,1) = -p/_rho*ny ;
	val(i,2) = -p/_rho*nz ;
	val(i,3) = 0;
      }
    }
  };
  term _term;
  public:
  boundarySlipWall( dgConservationLawNavierStokesIncomp3d *claw) : _term (claw->getDensity(), claw->getViscosity()) {
    _term0 = &_term;
  }
};

class  dgConservationLawNavierStokesIncomp3d::boundaryVelocity : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> normals, sol, vel, solGrad;
    fullMatrix<double> rho, mu;
    public:
    term(const function *velF, const function* rhoF, const function* muF):function(4){
      setArgument(sol,function::getSolution());
      setArgument(solGrad,function::getSolutionGradient());
      setArgument(normals,function::getNormals());
      setArgument (vel, velF);
      setArgument(mu,muF);
      setArgument(rho, rhoF);
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      size_t nQP = sol.size1();
      for(size_t i=0; i< nQP; i++) {
        double nx = normals(i,0);
        double ny = normals(i,1);
        double nz = normals(i,2);
	double u = vel(i,0);
	double v = vel(i,1);
	double w = vel(i,2);
        double p = sol(i,3);
	double dudx = solGrad(i,0);
	double dudy = solGrad(i,1);
	double dudz = solGrad(i,2);
	double dvdx = solGrad(i,3);
	double dvdy = solGrad(i,4);
	double dvdz = solGrad(i,5);
	double dwdx = solGrad(i,6);
	double dwdy = solGrad(i,7);
	double dwdz = solGrad(i,8);
	
	const double dudn = (dudx*nx+dudy*ny+dudz*nz);
	const double dvdn = (dvdx*nx+dvdy*ny+dvdz*nz);
	const double dwdn = (dwdx*nx+dwdy*ny+dwdz*nz);
	double un = u*nx+v*ny+w*nz;
	
	double _rho = rho(i,0);
	double _nu = mu(i,0)/_rho;
	val(i,0) = - u*un  -p/_rho*nx + _nu * dudn ;
	val(i,1) = - v*un  -p/_rho*ny + _nu * dvdn ;
	val(i,2) = - w*un  -p/_rho*nz + _nu * dwdn ;
	val(i,3) =   -un  ; 
      }
    }
  };
  term _term;
  public:
  boundaryVelocity( dgConservationLawNavierStokesIncomp3d *claw, const function *velF):  _term (velF, claw->getDensity(),  claw->getViscosity()) {
    _term0 = &_term;
  }
};

class  dgConservationLawNavierStokesIncomp3d::boundaryPressure : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> normals, sol, pres, solGrad;
   fullMatrix<double> rho, mu;
    public:
    term(const function *presF, const function* rhoF, const function* muF):function(4){
      setArgument(sol,function::getSolution());
      setArgument(solGrad,function::getSolutionGradient());
      setArgument(normals,function::getNormals());
      setArgument (pres, presF);
      setArgument(mu,muF);
      setArgument(rho, rhoF);
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      size_t nQP = sol.size1();
      for(size_t i=0; i< nQP; i++) {
        double nx = normals(i,0);
        double ny = normals(i,1);
        double nz = normals(i,2);

	double u = sol(i,0);
	double v = sol(i,1);
	double w = sol(i,2);
        double p = pres(i,0);
	
	double dudx = solGrad(i,0);
	double dudy = solGrad(i,1);
	double dudz = solGrad(i,2);
	double dvdx = solGrad(i,3);
	double dvdy = solGrad(i,4);
	double dvdz = solGrad(i,5);
	double dwdx = solGrad(i,6);
	double dwdy = solGrad(i,7);
	double dwdz = solGrad(i,8);
	
	const double dudn = (dudx*nx+dudy*ny+dudz*nz);
	const double dvdn = (dvdx*nx+dvdy*ny+dvdz*nz);
	const double dwdn = (dwdx*nx+dwdy*ny+dwdz*nz);
	double un = u*nx+v*ny+w*nz;

	double _rho = rho(i,0);
	double _nu = mu(i,0)/_rho;
	val(i,0) = - u*un  -p/_rho*nx + _nu * dudn ;
	val(i,1) = - v*un  -p/_rho*ny + _nu * dvdn ;
	val(i,2) = - w*un  -p/_rho*nz + _nu * dwdn ;
	val(i,3) =   -un  ; 
      }
    }
  };
  term _term;
  public:
  boundaryPressure( dgConservationLawNavierStokesIncomp3d *claw, const function *presF):  _term (presF, claw->getDensity(), claw->getViscosity()) {
    _term0 = &_term;
  }
};

dgBoundaryCondition * dgConservationLawNavierStokesIncomp3d::newBoundarySlipWall(){
  return new boundarySlipWall(this);
}
dgBoundaryCondition * dgConservationLawNavierStokesIncomp3d::newBoundaryVelocity(const function *VEL){
  return new boundaryVelocity(this, VEL);
}
dgBoundaryCondition * dgConservationLawNavierStokesIncomp3d::newBoundaryPressure(const function *PRES){
  return new boundaryPressure(this, PRES);
}

function*  dgConservationLawNavierStokesIncomp3d::newWallShearStressSmooth(const function *grad)
{
    _wallShearStress = new wallShearStress(_rhoF, _muF, grad);
    return   new wallShearStressNorm(_wallShearStress);
}

function*  dgConservationLawNavierStokesIncomp3d::newViscousNormalStressSmooth(const function *grad)
{
    checkSetup();
    return new viscousNormalStress(_rhoF, _muF, grad);
}

/*==============================================================================
 * Conservation law : constructor - setup - destructor
 *============================================================================*/
 dgConservationLawNavierStokesIncomp3d:: dgConservationLawNavierStokesIncomp3d(const function* rhoF, const function *muF, const double UGlob): dgConservationLawFunction(4) {
  // u,v,w,p
  _fzero = new functionConstant(0.);
  _fzerov = new functionConstant(std::vector<double>(4,0));
  _fzerograd = new functionConstant(std::vector<double>(12,0));
  _source = _fzerov;
  _prevSol = _fzerov;
  _prevSolGrad = _fzerograd;
  _rhoF = rhoF;
  _muF = muF;
  _UGlob = 1.0;
  temporal[0] = true; // for u
  temporal[1] = true; // for v
  temporal[2] = true; // for w
  temporal[3] = false; // for p
}


void  dgConservationLawNavierStokesIncomp3d::setup() {
  _maximumConvectiveSpeed[""] = new maxConvectiveSpeed();
  _volumeTerm0[""] = new source(_source);
  _volumeTerm1[""] = new gradPsiTerm(_maximumConvectiveSpeed[""], _prevSol, _prevSolGrad,
				     _source, _rhoF, _muF, _UGlob);
  _velocitySol = new velocityVector();
  _viscousNS = new viscousNormalStress(_rhoF, _muF);
  _inviscousNS = new inviscousNormalStress();
  _wallShearStress = new wallShearStress(_rhoF, _muF);
  _wallShearStressNorm = new wallShearStressNorm(_wallShearStress);

  //the dgBC::newOutsideValue is called to fix strongBC and calls interfaceTerm0 for the boundaryFlux 
  _interfaceTerm0[""] = new boundaryTerm(_maximumConvectiveSpeed[""], _prevSol,
					 _source, _rhoF,_muF); 

}


 dgConservationLawNavierStokesIncomp3d::~dgConservationLawNavierStokesIncomp3d() {
  delete _fzero;
  delete _fzerov;
  delete _fzerograd;
  if (_volumeTerm0[""]) delete _volumeTerm0[""];
  if (_volumeTerm1[""]) delete _volumeTerm1[""];
  if (_interfaceTerm0[""]) delete _interfaceTerm0[""];
  if (_maximumConvectiveSpeed[""]) delete _maximumConvectiveSpeed[""];
}
