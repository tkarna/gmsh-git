Mesh.CharacteristicLengthFactor = 0.03;
Mesh.Algorithm = 8;
lc = 0.01;
Point(1)={0.01,0.01,0,lc};
Point(2)={0,0.01,0,lc};
Point(3)={0,0.0,0,lc};
Point(4)={0.01,0,0,lc};

Line(1)={1,2};
Line(2)={2,3};
Line(3)={3,4};
Line(4)={4,1};
Line Loop(5) = {3, 4, 1, 2};
Plane Surface(6) = {5};

Physical Point("corner")={2};
Physical Line("top")={1};
Physical Line("bottom")={3};
Physical Line("left")={2};
Physical Line("right")={4};
Physical Surface(1)={6};



Transfinite Line {1, 3} = 40 Using Bump .25;
Transfinite Line {2, 4} = 40 Using Bump .25;
Transfinite Surface {6};
