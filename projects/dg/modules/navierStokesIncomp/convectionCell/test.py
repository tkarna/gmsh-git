from dgpy import *
from Incompressible import *
from math import *
import sys
import os

try : os.mkdir("output"); 
except: 0;
#-------------------------------------------------
#-- Rayleigh Benard Instability
#-------------------------------------------------
#-------------------------------------------------
#-- Parameters
#-------------------------------------------------

# \mathrm{Gr_L} = \frac{g \beta (T_s - T_\infty ) L^3}{\nu ^2}\, 
L = 0.01
T_REF = 283.
T_HOT = 293.5
meshName = "ConvectionCell"
RHO = 1000.0
MU  = 1.e-3
HEAT_CAPACITY = 4180.
HEAT_CONDUCTIVITY = 0.6 # W / (m K)
HEAT_EXPANSION = 207 *1e-6;
NU = HEAT_CONDUCTIVITY / (HEAT_CAPACITY * RHO)
UGlob = MU / (RHO * L)

Grashof = 9.81 * HEAT_EXPANSION * ( T_HOT - T_REF ) * L**3 / (NU*NU)

print ('Grashof Number = ', Grashof, 'alpha(visc) = ', MU / RHO,  'alpha(therm) = ', NU, 'DT_THERM = ', L*L/NU)

genMesh(meshName,2,1)

#-------------------------------------------------
#-- Initial function
#-------------------------------------------------
def initF(FCT, XYZ):
  for i in range(0,FCT.size1()):
    y = XYZ.get(i,1)
    #FCT.set(i,0, vmax*(1-(y/0.5)*(y/0.5)))
    FCT.set(i,0, 0) #u
    FCT.set(i,1, 0) #v
    FCT.set(i,2, 0) #p
#    FCT.set(i,3, T_HOT + y*(T_REF-T_HOT)/L) #T
    FCT.set(i,3, T_REF) #T

def TemperatureWallBC_2D(FCT, TFIX):
  for i in range(0,FCT.size1()):
    FCT.set(i,0, 0.0) 
    FCT.set(i,1, 0.0) 
    FCT.set(i,2, 0.0)
    FCT.set(i,3, TFIX(i,0))
    FCT.set(i,4, fixed)
    FCT.set(i,5, fixed)
    FCT.set(i,6, free)
    FCT.set(i,7, fixed)

def EVERYTHINGFIXED(FCT, TFIX):
  for i in range(0,FCT.size1()):
    FCT.set(i,0, 0.0) 
    FCT.set(i,1, 0.0) 
    FCT.set(i,2, 0.0)
    FCT.set(i,3, TFIX(i,0))
    FCT.set(i,4, fixed)
    FCT.set(i,5, fixed)
    FCT.set(i,6, fixed)
    FCT.set(i,7, fixed)

#-------------------------------------------------
#-- Initialization
#-------------------------------------------------
ns = Incompressible(meshName, 2)
rhoF = functionConstant(RHO)
muF = functionConstant(MU)
THOTF = functionConstant(T_HOT)
TCOLDF = functionConstant(T_REF)
heatConductivityF = functionConstant(HEAT_CONDUCTIVITY)
heatCapacityF = functionConstant(HEAT_CAPACITY)
heatExpansionCoefficientF = functionConstant(HEAT_EXPANSION)
ns.initializeIncompBoussinesq(initF, rhoF, muF , UGlob, heatConductivityF,heatCapacityF, heatExpansionCoefficientF, T_REF)

#print ns.law.getNbFields()

#-------------------------------------------------
#-- Boundary Conditions
#-------------------------------------------------
THOT = functionPython(8, TemperatureWallBC_2D, [THOTF])
TCOLD  = functionPython(8, TemperatureWallBC_2D, [TCOLDF])
CORNER  = functionPython(8, EVERYTHINGFIXED, [TCOLDF])

ns.strongBoundaryConditionLine('right', TCOLD)
ns.strongBoundaryConditionLine('left', THOT)
ns.strongBoundaryConditionLine('top', ns.ADIABATIC_WALL)
ns.strongBoundaryConditionLine('bottom', ns.ADIABATIC_WALL)
#ns.strongBoundaryConditionPoint('corner', CORNER)

#-------------------------------------------------
#-- Steady solve
#-------------------------------------------------

petscOptions ="-ksp_rtol 1.e-5 -pc_type ilu -pc_factor_levels 1"
maxIter = 30
#ns.steadySolve(maxIter, petscOptions)

#-------------------------------------------------
#-- Pseudo TimeStepping
#-------------------------------------------------
nbTimeSteps = 128
dt0 = 2
ATol = 1.e-8
RTol = 1.e-5
Verb = 2

ns.pseudoTimeSteppingSolve(2, nbTimeSteps, dt0, ATol, RTol, Verb, petscOptions,1.3)

sys.exit(success)
