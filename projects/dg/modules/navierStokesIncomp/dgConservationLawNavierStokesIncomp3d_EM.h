#ifndef _DG_CONSERVATION_NAVIER_STOKES_INCOMP_3D_EM
#define _DG_CONSERVATION_NAVIER_STOKES_INCOMP_3D_EM
#include "dgConservationLawFunction.h"

/**The stabilized incompressible Navier-Stokes equations. (u,v,w,p) */
class dgConservationLawNavierStokesIncomp3d_EM : public dgConservationLawFunction {
  class diffusiveFlux;
  class gradPsiTerm;
  class source;
  class boundaryWall;
  class boundaryVelocity;
  class boundaryPressure;
  class inletVelocity;
  class maxConvectiveSpeed;
  class velocityVector;
  class diffusivity;
  class riemann;
  const function *_source, *_fzero, *_fzerov, *_diffusiveFlux,*_ipTerm;
  const function *_velocitySol;
  double _rho, _nu;

  public:
  void setup();
  
  /**set the function to evaluate the source term \f{eqnarray*}\frac{du}{dt} = s(0)\\ \frac{dv}{dt} &=& s(1)\f} */
  inline void setSource(const function *source){ checkNonSetup(); _source = source;}
  /**set the density rho */
  inline void setDensity(const double rho){checkNonSetup(); _rho = rho;} 
  /**set the kinematic viscosity nu */
  inline void setViscosity(const double nu){checkNonSetup(); _nu = nu;} 
  inline const double getDensity() {return _rho;};
  inline const double getViscosity() {return _nu;};

  /**return the velocity vector */
  inline const function* getVelocity(){ checkSetup(); return _velocitySol; };
  /**return the pressure */
  inline function* getPressure() { 
    checkSetup();    
    return functionExtractCompNew(function::getSolution(),3); 
  }; 


  dgConservationLawNavierStokesIncomp3d_EM();
  ~dgConservationLawNavierStokesIncomp3d();
  /**non-slip wall boundary */
  dgBoundaryCondition *newBoundaryWall();
  /**prescribe velocity field */
  dgBoundaryCondition *newBoundaryVelocity(const function *vel);
  /**prescribe pressure field */
  dgBoundaryCondition *newBoundaryPressure(const function *pres);
};
#endif
