#include "dgConservationLawNavierStokesIncomp2d.h"
#include "dgConservationLawFunction.h"
#include "dgGroupOfElements.h"
#include "function.h"
#include "float.h"
#include "nodalBasis.h"
#include "dgMeshJacobian.h"
#include "Numeric.h"
#include "spallartAlmaras.h"

#define SQR(x) (x)*(x)

class dgConservationLawNavierStokesIncomp2d::velocityVector : public function{
  fullMatrix<double>  solution;
public :
  velocityVector() : function(3)  {
    setArgument (solution, function::getSolution());
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP =val.size1();
    for (size_t i=0; i<nQP; i++) {
      val (i,0) = solution(i,0);
      val (i,1) = solution(i,1);
      val (i,2) = 0.0;
    }
  }
};
class dgConservationLawNavierStokesIncomp2d::viscousNormalStress : public function{
  fullMatrix<double>  normals, solGrad;
  fullMatrix<double>  mu; 
public :
  viscousNormalStress(const function* rhoF, const function* muF, const function *gradFunc = NULL ) : function(3)  {
    setArgument (normals,function::getNormals(), 0);
    setArgument (solGrad,gradFunc ? gradFunc : function::getSolutionGradient());
    setArgument(mu, muF);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    for (size_t i = 0; i < nQP; i++) {
      double nx = -normals(i, 0);//normals should be inward
      double ny = -normals(i, 1);
      double dudx = solGrad(i, 0);
      double dudy = solGrad(i, 1);
      double dvdx = solGrad(i, 3);
      double dvdy = solGrad(i, 4);

      val (i,0) = mu(i, 0) * (2.*dudx*nx     + (dudy+dvdx)*ny);
      val (i,1) = mu(i, 0) * ((dvdx+dudy)*nx + 2.*dvdy*ny);
      val (i,2) = 0.0;
    }
  }
};

class dgConservationLawNavierStokesIncomp2d::viscousStress : public function{
  fullMatrix<double>  solGrad;
public :
  viscousStress( ) : function(9)  {
    setArgument (solGrad,function::getSolutionGradient());
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    for (size_t i = 0; i < nQP; i++) {
      double dudx = solGrad(i, 0);
      double dudy = solGrad(i, 1);
      double dvdx = solGrad(i, 3);
      double dvdy = solGrad(i, 4);

      val (i,0) = 2.*dudx;
      val (i,1) = (dudy+dvdx);
      val (i,2) = 0.0;
      val (i,3) = (dvdx+dudy); 
      val (i,4) = 2.*dvdy;
      val (i,5) = 0.0;
      val (i,6) = 0.0;
      val (i,7) = 0.0;
      val (i,8) = 0.0;
    }
  }
};


class dgConservationLawNavierStokesIncomp2d::inviscousNormalStress : public function{
  fullMatrix<double>  solution, normals;
public :
  inviscousNormalStress() : function(3)  {
    setArgument (solution, function::getSolution());
    setArgument (normals, function::getNormals(), 0);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    for (size_t i = 0; i < nQP; i++) {
      double nx = -normals(i, 0);//normals should be inward
      double ny = -normals(i, 1);
      double p =  solution(i, 2);
      val (i, 0) =  -p*nx;
      val (i, 1) =  -p*ny;
      val (i, 2) = 0.0;
    }
  }
};

class dgConservationLawNavierStokesIncomp2d::wallShearStress : public function{
  fullMatrix<double>  normals, solGrad;
  fullMatrix<double>  mu; 
public :
  wallShearStress(const function* rhoF, const function* muF, const function *gradFunc = NULL) : function(3)  {
    setArgument(normals,function::getNormals(), 0);
    setArgument (solGrad,gradFunc ? gradFunc : function::getSolutionGradient());
    setArgument (mu, muF);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    for (size_t i = 0; i < nQP; i++) {
      double nx = normals(i, 0);
      double ny = normals(i, 1);
      double dudx = solGrad(i, 0);
      double dudy = solGrad(i, 1);
      double dvdx = solGrad(i, 3);
      double dvdy = solGrad(i, 4);
      double t1 = mu(i, 0)* (2.*dudx*nx     + (dudy+dvdx)*ny);
      double t2 = mu(i, 0)* ((dvdx+dudy)*nx + 2.*dvdy*ny);
      double tn = t1*nx+t2*ny;
      val (i, 0) = t1 - tn*nx;
      val (i, 1) = t2 - tn*ny;
      val (i, 2) = 0.0;
    }
  }
};
class dgConservationLawNavierStokesIncomp2d::wallShearStressNorm : public function{
  fullMatrix<double> wss;
public :
  wallShearStressNorm(const function* wallShearStress) : function(1)  {
    setArgument (wss, wallShearStress);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    for (size_t i = 0; i < nQP; i++) {
      val (i, 0) = sqrt(wss(i,0)*wss(i,0) + wss(i,1)*wss(i,1) + wss(i,2)*wss(i,2));
    }
  }
};


class dgConservationLawNavierStokesIncomp2d::maxConvectiveSpeed: public function {
  fullMatrix<double> sol;
  public:
  maxConvectiveSpeed ():function(1){
    setArgument(sol, function::getSolution());
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
      for(int i = 0; i < val.size1(); i++)
        val(i,0) = sqrt(sol(i,0)*sol(i,0) + sol(i,1)*sol(i,1)); 
  }
};


class dgConservationLawNavierStokesIncomp2d::clipToPhysics : public function {
  fullMatrix<double> rho;
public:
  clipToPhysics(const function *rhoF):function(3) {
    setArgument(rho, rhoF);
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    size_t nQP = val.size1();
    for (size_t k = 0 ; k < nQP; k++ ){
      double rhoVal = rho(k, 0);
      if (rhoVal < 0.0){
        val(k, 0) = 0.0;
        val(k, 1) = 0.0;
      }
    }
  }
};


class dgConservationLawNavierStokesIncomp2d::gradPsiTerm: public function {
  fullMatrix<double>  sol, solGrad, solPrev, solPrevGrad, speed, source, _dt;
  double _UGlob, _referenceTemperature, _referenceConcentration;
  fullMatrix<double> rho, mu,heatConductivity,heatCapacity,heatExpansionCoefficient, distanceToTheWall, concentrationDiffusivity, concentrationExpansionCoefficient, oneOverPermeability;
  int HEAT_INDEX, SA_INDEX, CONCENTRATION_INDEX, _nbf;
  bool _darcy;
  public:
  gradPsiTerm (const function *maxSpeed, const function *previousSol, const function *previousSolGrad,
	       const function *sourceF, const function* rhoF, const function* muF, const double UGlob, 
	       const function *heatExpansionCoefficientF, 
	       const function *heatConductivityF, 
	       const function *heatCapacityF, 
	       double T, 
	       const function *concentrationExpansionCoefficientF, 
	       const function *concentrationDiffusivityF, 
	       double C, 
	       const function *distanceToTheWallF, 
         const function *oneOverPermeabilityF, 
	       int heat_index, int sa_index, int concentration_index,  
	       int nbf): function(nbf*3), HEAT_INDEX(heat_index), SA_INDEX(sa_index), CONCENTRATION_INDEX(concentration_index), _nbf(nbf){
    setArgument(sol,function::getSolution());
    setArgument(solPrev, previousSol);
    setArgument(solPrevGrad, previousSolGrad);
    setArgument(solGrad, function::getSolutionGradient());
    setArgument(_dt, function::getDT());
    setArgument(rho, rhoF);
    setArgument(mu, muF);
    setArgument(source, sourceF);
    setArgument(speed, maxSpeed);
    _UGlob = UGlob;
    _darcy = false;
    if (oneOverPermeabilityF) {
      _darcy = true;
      setArgument(oneOverPermeability, oneOverPermeabilityF);
    }
    if (HEAT_INDEX >= 0){
      _referenceTemperature = T;
      setArgument(heatExpansionCoefficient, heatExpansionCoefficientF);
      setArgument(heatConductivity, heatConductivityF);
      setArgument(heatCapacity, heatCapacityF);      
    }
    if (CONCENTRATION_INDEX >= 0){
      _referenceConcentration = C;
      setArgument(concentrationExpansionCoefficient, concentrationExpansionCoefficientF);
      setArgument(concentrationDiffusivity, concentrationDiffusivityF);
    }
    if (SA_INDEX >= 0){
      setArgument(distanceToTheWall, distanceToTheWallF);      
    }
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    const dgGroupOfElements &eGroup = *m->getGroupOfElements();
    size_t nQP = val.size1();
    val.setAll(0.0);
    for(size_t i=0; i< nQP; i++) {
      int iElement = m->elementId(i/m->nPointByElement());

      double u = sol(i,0);
      double v = sol(i,1);
      double p = sol(i,2);
      double dt = _dt(i,0);
 
      double dudx = solGrad(i,0);
      double dudy = solGrad(i,1);
      double dvdx = solGrad(i,3);
      double dvdy = solGrad(i,4);
      double dpdx = solGrad(i,6);
      double dpdy = solGrad(i,7);
      double UGradu = u*dudx+v*dudy;
      double UGradv = u*dvdx+v*dvdy; 
      double divU  = dudx+dvdy;

      double _rho = rho(i,0);
      double _mu = mu(i,0);
      if (SA_INDEX >= 0) {
	//	double nuTilda = sol(i,SA_INDEX);
	double nuTilda = std::max(sol(i,SA_INDEX),0.0);
	double chi = _rho*nuTilda/_mu;
	_mu += _rho*nuTilda*spallartAlmaras::fv1(chi);
	//	printf("MU IN GRADPSITERM : %12.5E %12.5E %12.5E %12.5E\n",_mu,nuTilda,sol(i,SA_INDEX),spallartAlmaras::fv1(chi));
      }   
      double _nu = _mu/_rho;
      double s1 = source(i,1)/_rho + ((HEAT_INDEX >= 0)?  9.81 * heatExpansionCoefficient(i, 0) * (sol(i,HEAT_INDEX) - _referenceTemperature) : 0);	
      double s0 = source(i,0)/_rho; 

      double laplU = eGroup.getLaplacianOfFunction(iElement, 0); 
      double laplV = eGroup.getLaplacianOfFunction(iElement, 1);
      if (laplU == -1000) laplU = 0.0;
      if (laplV == -1000) laplV = 0.0;
     
      //flux_x (multiply dphi/dx)
      val(i,0) =  (p - _mu*dudx +  _rho*u*u) ; 
      val(i,1) =  (  - _mu*dvdx +  _rho*v*u) ; 
      val(i,2) =  u;
      
      // flux_y (multiply dphi/dy)
      val(i,0+_nbf) = (   - _mu*dudy + _rho*u*v); 
      val(i,1+_nbf) = (p  - _mu*dvdy + _rho*v*v);
      val(i,2+_nbf) =  v;

      //double h = 2.*eGroup.getInnerRadius(iElement);
      double h = sqrt(4*m->getJacobians().elementVolume(eGroup.elementVectorId())(iElement)/3.14);
      double hV = eGroup.getStreamwiseLength(iElement);
      if (hV <= 0.0) hV= h;
      double ULoc = speed(i,0);
      if (ULoc == 0.0) ULoc = _UGlob;

      double alpha = 0.5;
      double tau_DT = (dt > 0.0) ? SQU(2./dt) : 0.0;

      double tau_FULL_P = alpha*(1./sqrt( tau_DT + SQU(4.*_nu/(h*h)) + SQU(4.*_UGlob/h)  ) ); 
      double tau_FULL_V = alpha*(1./sqrt( tau_DT + SQU(4.*_nu/(hV*hV)) + SQU(4.*ULoc/hV) ) ); //ULoc
      
      double dudt = (dt > 0.) ? (u - solPrev(i,0)) / dt : 0.;
      double dvdt = (dt > 0.) ? (v - solPrev(i,1)) / dt : 0.;
      double Rm_u = (dudt+UGradu+dpdx/_rho -_nu*laplU-s0);
      double Rm_v = (dvdt+UGradv+dpdy/_rho -_nu*laplV-s1);
      if (_darcy) {
        double invK = oneOverPermeability(i, 0);
        Rm_u += u * invK;
        Rm_v += v * invK;
      }

      //add PSPG stab
      double tau_P = tau_FULL_P ; 
      eGroup.setPSPGStabilisation(iElement, tau_P);
      val(i,2)   -= tau_P*Rm_u;
      val(i,2+_nbf) -= tau_P*Rm_v;
             
      //add SUPG stab
      double tau_V = _rho*tau_FULL_V; 
      val(i,0)   -= tau_V*Rm_u*u; 
      val(i,1)   -= tau_V*Rm_v*u; 
      val(i,0+_nbf) -= tau_V*Rm_u*v;
      val(i,1+_nbf) -= tau_V*Rm_v*v;

      //add GRAD-DIV stab
      //      double tau_C = 100*_rho*alpha*sqrt( SQU(_nu) + SQU(2.*ULoc*h) );//ULoc
      double tau_C = 0*_rho*alpha*(h*ULoc/std::min(ULoc*h/(6*_nu),3.));
      //tau_C  = 0.0;
      val(i,0)   -= tau_C*divU;
      val(i,1)   -= 0.0;
      val(i,0+_nbf) -= 0.0; 
      val(i,1+_nbf) -= tau_C*divU;


      // TEMPERATURE TERM
      if (HEAT_INDEX >= 0){	
        double alpha_T = heatConductivity(i,0)/(_rho * heatCapacity(i,0));

        // turbuence model for the energy
        if (SA_INDEX >=0){
          double _nu = _mu / _rho;
          //	  double nuTilda = sol(i,SA_INDEX);//
          double nuTilda = std::max(sol(i,SA_INDEX),0.0);
          double chi = nuTilda/_nu;
          const double Prandlt_turb = 0.85;
          alpha_T += nuTilda * spallartAlmaras::fv1(chi)/Prandlt_turb;	 
          //	  printf("%12.5E\n",alpha_T);
        }

        const double dTdx = solGrad(i,3*HEAT_INDEX+0);
        const double dTdy = solGrad(i,3*HEAT_INDEX+1);
        val(i, 0    + HEAT_INDEX)   -= dTdx * alpha_T;
        val(i, _nbf + HEAT_INDEX)   -= dTdy * alpha_T;

        //SUPG TERMS
        if (1){
          const double Temperature = sol(i,HEAT_INDEX);
          double dTdt = (dt > 0.) ? (Temperature - solPrev(i,HEAT_INDEX)) / dt : 0.;
          double UGradT = u * dTdx + v * dTdy ;
          double Res_T  = (dTdt + UGradT);
          double tau_T = alpha*(1./sqrt(tau_DT +  SQU(4.*alpha_T/(hV*hV)) + SQU(2.*ULoc/hV) ) );
          val(i,  0  + HEAT_INDEX) -= tau_T * Res_T * u;
          val(i,_nbf + HEAT_INDEX) -= tau_T * Res_T * v;
        }
      }
      // SPALLART ALMARAS
      if (SA_INDEX >= 0){	
	//	double nuTilda = sol(i,SA_INDEX);//
	double nuTilda = std::max(sol(i,SA_INDEX),0.0);
	const double alpha_SA = (1./spallartAlmaras::_sigma) * (nuTilda + _nu ) ;
	// FIXME : simple advection -- diffusion 
	//const double alpha_SA = (1./spallartAlmaras::_sigma) * ( _nu ) ;
	const double dNuTildadx = solGrad(i,3*SA_INDEX+0);
	const double dNuTildady = solGrad(i,3*SA_INDEX+1);
	val(i, 0    + SA_INDEX)   -= dNuTildadx * alpha_SA;
	val(i, _nbf + SA_INDEX)   -= dNuTildady * alpha_SA;
	if (1){
	  const double nuTilda = sol(i,SA_INDEX);
	  double dNudt = (dt > 0.) ? (nuTilda - solPrev(i,SA_INDEX)) / dt : 0.;
	  double UGradNu = u * dNuTildadx + v * dNuTildady ;
	  const double dNuTildadx = solGrad(i,3*SA_INDEX+0);
	  const double dNuTildady = solGrad(i,3*SA_INDEX+1);
	  const double dudy = solGrad(i,3*0+1);
	  const double dvdx = solGrad(i,3*1+0);
	  const double wz = (dudy-dvdx)/2.0;
	  const double vorticityMagnitude = sqrt(2*wz*wz);
	  const double chi = nuTilda / _nu;
	  const double distance = distanceToTheWall(i,0);
	  spallartAlmaras::setup(distance, nuTilda, chi, vorticityMagnitude);
	  double GradNuTildaSquared = dNuTildadx*dNuTildadx + dNuTildady*dNuTildady;
	  const double VAL_SA = (GradNuTildaSquared * spallartAlmaras::_cb2 / spallartAlmaras::_sigma +  
				 spallartAlmaras::_cb1 * (1.- spallartAlmaras::_ft2) * spallartAlmaras::_st * nuTilda-
				 (spallartAlmaras::_cw1 * spallartAlmaras::_fw - (spallartAlmaras::_cb1/spallartAlmaras::_kappa) * spallartAlmaras::_ft2) * 
				 SQR(nuTilda/distance)) ; 	  
	  double Res_Nu  = (dNudt + UGradNu-VAL_SA);
	  //	  double tau_Nu = alpha*(1./sqrt(tau_DT +  SQU(4.*alpha_SA/(hV*hV)) + SQU(2.*ULoc/hV) ) );
	  double tau_Nu = alpha*(1./sqrt(tau_DT +  SQU(4.*alpha_SA/(hV*hV)) + SQU(2.*ULoc/hV) ) );
	  val(i,  0  + SA_INDEX) -= tau_Nu * Res_Nu * u ;
	  val(i,_nbf + SA_INDEX) -= tau_Nu * Res_Nu * v ;
	  //	  val(i,  0  + SA_INDEX) *= 0.0;
	  //	  val(i,_nbf + SA_INDEX) *= 0.0;
	}
	//	printf("GRADPSITERM : %12.5E %12.5E %12.5E\n",alpha_SA,val(i, 0    + SA_INDEX),val(i, _nbf    + SA_INDEX));
      }

    }
  }
};

//is called by newOutsideValueBC to compute fluxes at boundaries
class dgConservationLawNavierStokesIncomp2d::boundaryTerm:public function {
  fullMatrix<double> sol, solPrev,solGrad, normals, speed, source , _dt;
  fullMatrix<double> rho, mu,  _heatExpansionCoefficient;
  int HEAT_INDEX;
public:
  boundaryTerm ( const function *maxSpeed, const function *previousSol, 
		 const function *sourceF, const function* rhoF, const function* muF, 
		 const function *heatExpansionCoefficientF, 
		 const double referenceTemperature, 
		 const double referenceDensity, int heat_index, int NBF): function(NBF),
									  HEAT_INDEX(heat_index){
    setArgument(sol,function::getSolution(), 0);
    setArgument(solPrev, previousSol);
    setArgument(solGrad,function::getSolutionGradient());
    setArgument(normals,function::getNormals(), 0);
    setArgument(rho, rhoF);
    setArgument(mu, muF);
    setArgument(source, sourceF);
    setArgument(speed, maxSpeed);
    setArgument(_dt, function::getDT());
    if (HEAT_INDEX >= 0){
      setArgument(_heatExpansionCoefficient, heatExpansionCoefficientF);
    }
//    setArgument(speed, maxSpeed);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    const dgGroupOfElements &eGroup = *m->getGroupOfElements();
    size_t nQP = sol.size1();
    val.setAll(0);
    for(size_t i = 0; i < nQP; i++) {
      int iElement = m->elementId(i/m->nPointByElement());
     double nx = normals(i,0);
     double ny = normals(i,1);
     double u = sol(i,0);
     double v = sol(i,1);
     double p = sol(i,2);
     double dudx = solGrad(i,0);
     double dudy = solGrad(i,1);
     double dvdx = solGrad(i,3);
     double dvdy = solGrad(i,4);
     double dudn = (dudx*nx+dudy*ny);
     double dvdn = (dvdx*nx+dvdy*ny);
     
     double un   = u*nx+v*ny;
     double laplU = eGroup.getLaplacianOfFunction(iElement, 0);
     double laplV = eGroup.getLaplacianOfFunction(iElement, 1);
     if (laplU == -1000) laplU = 0.0;
     if (laplV == -1000) laplV = 0.0;
     
     double _rho = rho(i, 0);
     double _mu = mu(i, 0);

     val(i, 0) =  ( - _rho*u*un - p*nx + _mu*dudn);
     val(i, 1) =  ( - _rho*v*un - p*ny + _mu*dvdn);
     val(i, 2) =   - un ; 
 
     //boundary integral modification for PSPG term
     //     double tau_P = eGroup.getPSPGStabilisation(iElement);
     //     val(i,2) -= tau_P*(UGradun+dpdn/_rho-_nu*laplUN-sn);   
   
    }
  }
};

class dgConservationLawNavierStokesIncomp2d::psiTerm: public function {
  fullMatrix<double> _source, _rho, _mu, _heatExpansionCoefficient, _sol,_solGrad,_xyz,_distanceToTheWall, _oneOverPermeability,_gradientOfSolidVelocity;
  const double _referenceTemperature, _referenceDensity;
  const int HEAT_INDEX, SA_INDEX;
  bool _darcy;
public :
  psiTerm(const function *sourceF, 
	 const function *rhoF, 
	 const function *muF, 
	 const function *heatExpansionCoefficientF, 
	  const double referenceTemperature, 
	  const double referenceDensity, 
	  const function *distanceToTheWallF, 
	  const function *oneOverPermeabilityF, 
	  const function *gradientOfSolidVelocityF, 
	  int heat_index, int sa_index, int NBF): function (NBF), 
						  _referenceTemperature(referenceTemperature),
						  _referenceDensity(referenceDensity),
						  HEAT_INDEX(heat_index),
						  SA_INDEX(sa_index), _darcy(false)
  {
    setArgument(_source, sourceF);
    setArgument(_rho, rhoF);
    setArgument(_mu, muF);
    if (oneOverPermeabilityF){
      _darcy = true;
      setArgument(_oneOverPermeability, oneOverPermeabilityF);
      setArgument(_gradientOfSolidVelocity, gradientOfSolidVelocityF);
    }

    if (HEAT_INDEX >= 0 || SA_INDEX >= 0 || _darcy){
      setArgument(_solGrad,function::getSolutionGradient());
      setArgument(_sol,function::getSolution());
      setArgument(_xyz, function::getCoordinates());
    }
    if (HEAT_INDEX >= 0){
      setArgument(_heatExpansionCoefficient, heatExpansionCoefficientF);
    }
    if (SA_INDEX >= 0){
      setArgument(_distanceToTheWall, distanceToTheWallF);
    }
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    for (size_t i = 0; i < nQP; i++) {
      const double RHO = _rho(i,0);
      val (i, 0) = RHO * _source(i, 0);
      val (i, 1) = RHO * _source(i, 1);
      val (i, 2) = RHO * _source(i, 2);

      if (_darcy){
        const double oneOverK = _oneOverPermeability(i, 0); 
        const double dusdx = _gradientOfSolidVelocity(i, 3*0+0); 
        const double dvsdy = _gradientOfSolidVelocity(i, 3*1+1); 
        val (i, 0) += _sol(i,0) * oneOverK;
        val (i, 1) += _sol(i,1) * oneOverK;
        val (i, 2) -= (dusdx+dvsdy);
      }

      if (HEAT_INDEX >= 0){
        const double u = _sol(i,0);
        const double v = _sol(i,1);
        const double Temperature = _sol(i,HEAT_INDEX);
        val (i, 1) += 9.81 * _heatExpansionCoefficient(i, 0) * (Temperature - _referenceTemperature) * _referenceDensity;	
        //const double dUdx = _solGrad(i,0);
        //const double dVdy = _solGrad(i,4);
        const double dTdx = _solGrad(i,3*HEAT_INDEX+0);
        const double dTdy = _solGrad(i,3*HEAT_INDEX+1);
        double UGradT = u * dTdx + v * dTdy;
        val (i, HEAT_INDEX) = - UGradT ; //(dUdx + dVdy)*Temperature
      }
      if (SA_INDEX >= 0){
        const double u = _sol(i,0);
        const double v = _sol(i,1);
        const double _nu = _mu(i,0) / RHO;
        //	const double nuTilda = _sol(i,SA_INDEX);//
        const double nuTilda =std::max(_sol(i,SA_INDEX),0.0);

        const double dNuTildadx = _solGrad(i,3*SA_INDEX+0);
        const double dNuTildady = _solGrad(i,3*SA_INDEX+1);

        const double dudy = _solGrad(i,3*0+1);
        const double dvdx = _solGrad(i,3*1+0);
        const double wz = (dudy-dvdx)/2.;
        const double vorticityMagnitude = sqrt(2*wz*wz);
        const double chi = nuTilda / _nu;
        const double distance = _distanceToTheWall(i,0);
        spallartAlmaras::setup(distance, nuTilda, chi, vorticityMagnitude);
        double GradNuTildaSquared = dNuTildadx*dNuTildadx + dNuTildady*dNuTildady;
        /*
           \frac{\partial \tilde \nu}{\partial t} + u_j \frac{\partial \tilde \nu}{\partial x_j} = c_{b1}(1-f_{t2})\tilde S \tilde \nu - \left[c_{w1}f_w - \frac{c_{b1}}{\kappa^2}f_{t2}\right] \left(\frac{\tilde \nu}{d} \right)^2 + \frac{1}{\sigma} \left[ \frac{\partial}{\partial x_j} \left( \left( \nu + \tilde \nu \right) \frac{\partial \tilde \nu}{\partial x_j} \right) + c_{b2}\frac{\partial \tilde \nu}{\partial x_i} \frac{\partial \tilde \nu}{\partial x_i} \right] 
           */
        const double VAL_SA = (GradNuTildaSquared * spallartAlmaras::_cb2 / spallartAlmaras::_sigma +  
            spallartAlmaras::_cb1 * (1.- spallartAlmaras::_ft2) * spallartAlmaras::_st * nuTilda-
            (spallartAlmaras::_cw1 * spallartAlmaras::_fw - (spallartAlmaras::_cb1/spallartAlmaras::_kappa_2) * spallartAlmaras::_ft2) * 
            SQR(nuTilda/distance)) ; 
        double UGradNu = u * dNuTildadx + v * dNuTildady;
        val (i, SA_INDEX) = -UGradNu + VAL_SA;
        //	printf("PSITERM : %g %g %g %g %g %g %g %12.5E %g %g %g\n",distance,VAL_SA,_sol (i, SA_INDEX),wz,vorticityMagnitude,chi,distance,nuTilda,spallartAlmaras::_ft2,spallartAlmaras::_st,spallartAlmaras::_fw);
        //	printf("%g\n",VAL_SA,);
      }
    }
  }
};

//impose un=0, so that the convective boundary fluxes simplify
class dgConservationLawNavierStokesIncomp2d::boundarySlipWall : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> normals, sol;
    fullMatrix<double> rho, mu;
    public:
    term(const function* rhoF, const function* muF):function(3){
      setArgument(sol,function::getSolution());
      setArgument(normals,function::getNormals());
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      size_t nQP = sol.size1();
      for(size_t i = 0; i < nQP; i++) {
        double nx = normals(i, 0);
        double ny = normals(i, 1);
        double p = sol(i, 2);
        val(i, 0) = (-p * nx) ;
        val(i, 1) = (-p * ny) ;
        val(i, 2) = 0;
      }
    }
  };
  term _term;
  public:
  boundarySlipWall(dgConservationLawNavierStokesIncomp2d *claw) : _term (claw->getDensity(), claw->getViscosity()) {
    _term0 = &_term;
  }
};

class dgConservationLawNavierStokesIncomp2d::boundaryVelocity : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> normals, sol, vel, solGrad;
    fullMatrix<double> rho, mu;
    public:
    term(const function *velF, const function* rhoF, const function* muF):function(3){
      setArgument(sol,function::getSolution());
      setArgument(solGrad,function::getSolutionGradient());
      setArgument(normals,function::getNormals());
      setArgument (vel, velF);
      setArgument(rho, rhoF);
      setArgument(mu,muF);
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      size_t nQP = sol.size1();
      for(size_t i=0; i< nQP; i++) {
        double nx = normals(i,0);
        double ny = normals(i,1);
	double u = vel(i,0);
	double v = vel(i,1);
        double p = sol(i,2);
	double dudx = solGrad(i,0);
	double dudy = solGrad(i,1);
	double dvdx = solGrad(i,3);
	double dvdy = solGrad(i,4);
	const double dudn = (dudx*nx+dudy*ny);
	const double dvdn = (dvdx*nx+dvdy*ny);
	double un = u*nx+v*ny;
	double _rho = rho(i,0);
	double _mu = mu(i,0);
	val(i,0) = - _rho*u*un  -p*nx + _mu * dudn ;
	val(i,1) = - _rho*v*un  -p*ny + _mu * dvdn ;
	val(i,2) =   -un  ; 
      }
    }
  };
  term _term;
  public:
  boundaryVelocity(dgConservationLawNavierStokesIncomp2d *claw, const function *velF):  _term (velF, claw->getDensity(),  claw->getViscosity()) {
    _term0 = &_term;
  }
};

class dgConservationLawNavierStokesIncomp2d::boundaryPressure : public dgBoundaryCondition {
  class term : public function {
    fullMatrix<double> normals, sol, pres, solGrad;
    fullMatrix<double> rho, mu;
    public:
    term(const function *presF, const function* rhoF, const function* muF):function(3){
      setArgument(sol,function::getSolution());
      setArgument(solGrad,function::getSolutionGradient());
      setArgument(normals,function::getNormals());
      setArgument (pres, presF);
      setArgument(rho, rhoF);
      setArgument(mu,muF);
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      size_t nQP = sol.size1();
      val.setAll(0.0);
      for(size_t i=0; i< nQP; i++) {
        double nx = normals(i,0);
        double ny = normals(i,1);
	double u = sol(i,0);
	double v = sol(i,1);
        double p = pres(i,0);
	double dudx = solGrad(i,0);
	double dudy = solGrad(i,1);
	double dvdx = solGrad(i,3);
	double dvdy = solGrad(i,4);
	const double dudn = (dudx*nx+dudy*ny);
	const double dvdn = (dvdx*nx+dvdy*ny);
	double un = u*nx+v*ny;
	double _rho = rho(i,0);
	double _mu  = mu(i,0);
	val(i,0) =  (- _rho*u*un -p*nx + _mu * dudn) ;
	val(i,1) =  (- _rho*v*un -p*ny + _mu * dvdn) ;
	val(i,2) =   -un  ; 
      }
    }
  };
  term _term;
  public:
  boundaryPressure(dgConservationLawNavierStokesIncomp2d *claw, const function *presF):  _term (presF, claw->getDensity(), claw->getViscosity()) {
    _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawNavierStokesIncomp2d::newBoundarySlipWall(){
  return new boundarySlipWall(this);
}
dgBoundaryCondition *dgConservationLawNavierStokesIncomp2d::newBoundaryVelocity(const function *VEL){
  return new boundaryVelocity(this, VEL);
}
dgBoundaryCondition *dgConservationLawNavierStokesIncomp2d::newBoundaryPressure(const function *PRES){
  return new boundaryPressure(this, PRES);
}


class massFactorForNavierStokes2d: public function {
  fullMatrix<double> rho;
 public :
  massFactorForNavierStokes2d(const function *rhoF, int NBF) : function(NBF) {
    setArgument (rho, rhoF);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    int nQP=val.size1();
    val.setAll(1.0);
    for (int i=0; i<nQP; i++) {
      const double _rho = rho(i,0);
      val(i,0) = _rho;
      val(i,1) = _rho;
      val(i,2) = 0.0;
    }
  }
};


/*==============================================================================
 * Conservation law : constructor - setup - destructor
 *============================================================================*/
dgConservationLawNavierStokesIncomp2d::dgConservationLawNavierStokesIncomp2d(const function* rhoF, 
									     const function *muF, 
									     const double UGlob,
									     const function *heatConductivityF, 
									     const function *heatCapacityF, 
									     const function *heatExpansionCoefficientF,
									     const double referenceTemperature, 
									     const double referenceDensity,
									     const function *distanceToTheWallF, 
									     const function *oneOverPermeability, 
									     const function *gradientOfSolidVelocity): 
  dgConservationLawFunction(heatConductivityF ? (distanceToTheWallF ? 5 : 4) : (distanceToTheWallF ? 4 : 3)),  HEAT_INDEX (heatConductivityF ? 3 : -1), SA_INDEX(distanceToTheWallF ? (heatConductivityF ? 4 : 3) : -1), CONCENTRATION_INDEX (-1)  {
  // u,v,p
  
  _fzero = new functionConstant(0.);
  _fzerov = new functionConstant(std::vector<double>(_nbf,0));
  _fzerograd = new functionConstant(std::vector<double>(3*_nbf,0));
  _userSource = _fzerov;
  _prevSol = _fzerov;
  _prevSolGrad = _fzerograd;
  _rhoF = rhoF;
  _muF = muF;
  _UGlob = UGlob;  
  _velocitySol = NULL;
  temporal[0] = true;
  temporal[1] = true;
  temporal[2] = false;
  // Buoyancy 
  if (HEAT_INDEX >= 0){
    _referenceDensity = referenceDensity;
    _referenceTemperature = referenceTemperature;
    _heatConductivityF = heatConductivityF;
    _heatExpansionCoefficientF = heatExpansionCoefficientF;
    _heatCapacityF = heatCapacityF;
    temporal[HEAT_INDEX] = true;
  }
  if (SA_INDEX >= 0){
    temporal[SA_INDEX] = true;
    _distanceToTheWallF = distanceToTheWallF;
  }
  _oneOverPermeability = oneOverPermeability;
  _gradientOfSolidVelocity = gradientOfSolidVelocity;
}

function*  dgConservationLawNavierStokesIncomp2d::newWallShearStressSmooth(const function *grad)
{
    _wallShearStress = new wallShearStress(_rhoF, _muF, grad);
    return   new wallShearStressNorm(_wallShearStress);
}

function*  dgConservationLawNavierStokesIncomp2d::newViscousNormalStressSmooth(const function *grad)
{
    checkSetup();
    return new viscousNormalStress(_rhoF, _muF, grad);
}

void dgConservationLawNavierStokesIncomp2d::setup() {
  _maximumConvectiveSpeed[""] = new maxConvectiveSpeed();
  _volumeTerm0[""] = new psiTerm(_userSource, _rhoF, _muF, _heatExpansionCoefficientF, _referenceTemperature, _referenceDensity, _distanceToTheWallF, _oneOverPermeability, _gradientOfSolidVelocity, 
				 HEAT_INDEX, SA_INDEX,_nbf);
  _volumeTerm1[""] = new gradPsiTerm(_maximumConvectiveSpeed[""], _prevSol, _prevSolGrad,
				     _userSource, _rhoF, _muF, _UGlob,  
				     _heatExpansionCoefficientF, 
				     _heatConductivityF, 
				     _heatCapacityF,  
				     _referenceTemperature,
				     _concentrationExpansionCoefficientF, 
				     _concentrationDiffusivityF,
				     _referenceConcentration,
				     _distanceToTheWallF, 
             _oneOverPermeability,
				     HEAT_INDEX, SA_INDEX, CONCENTRATION_INDEX, _nbf);
  _velocitySol = new velocityVector();
  _viscousS = new viscousStress();
  _viscousNS = new viscousNormalStress(_rhoF, _muF);
  _inviscousNS = new inviscousNormalStress();
  _wallShearStress = new wallShearStress(_rhoF, _muF);
  _wallShearStressNorm = new wallShearStressNorm(_wallShearStress);

  //the dgBC::newOutsideValue is called to fix strongBC and calls interfaceTerm0 for the boundaryFlux 
  _interfaceTerm0[""] = new boundaryTerm(_maximumConvectiveSpeed[""], _prevSol,
					 _userSource, _rhoF,_muF,_heatExpansionCoefficientF, _referenceTemperature, _referenceDensity,  HEAT_INDEX, _nbf); 
  _clipToPhysics[""] = new clipToPhysics (_rhoF);

  _massFactor[""] = std::make_pair(new massFactorForNavierStokes2d(_rhoF,_nbf), false);
  //_massFactor[""] = std::make_pair(_rhoF, false);
 
}

dgConservationLawNavierStokesIncomp2d::~dgConservationLawNavierStokesIncomp2d() {
  delete _fzero;
  delete _fzerov;
  delete _fzerograd;
  if (_volumeTerm0[""]) delete _volumeTerm0[""];
  if (_volumeTerm1[""]) delete _volumeTerm1[""];
  if (_interfaceTerm0[""]) delete _interfaceTerm0[""];
  if (_maximumConvectiveSpeed[""]) delete _maximumConvectiveSpeed[""];
  if (_clipToPhysics[""]) delete _clipToPhysics[""];
}
