#ifndef _DG_CONSERVATION_NAVIER_STOKES_INCOMP_2D_
#define _DG_CONSERVATION_NAVIER_STOKES_INCOMP_2D_
#include "dgConservationLawFunction.h"
#include "functionGeneric.h"

/**The stabilized incompressible Navier-Stokes equations. (u,v,p) */
class dgConservationLawNavierStokesIncomp2d : public dgConservationLawFunction {
  class gradPsiTerm;
  class psiTerm;
  class boundarySlipWall;
  class boundaryVelocity;
  class boundaryPressure;
  class inletVelocity;
  class maxConvectiveSpeed;
  class velocityVector;
  class boundaryTerm;
  class boundaryTermST;
  class clipToPhysics;
  class viscousStress;
  class viscousNormalStress;
  class inviscousNormalStress;
  class wallShearStress;
  class wallShearStressNorm;
  const function *_userSource, *_fzero, *_fzerov, *_fzerograd;
  const function *_rhoF, *_muF;
  const function *_diffusiveFlux;
  const function *_velocitySol,*_viscousS,  *_viscousNS, *_inviscousNS, *_wallShearStress, *_wallShearStressNorm;
  const function *_heatConductivityF,*_heatExpansionCoefficientF,*_heatCapacityF;
  const function *_concentrationExpansionCoefficientF,*_concentrationDiffusivityF;
  const function *_distanceToTheWallF;
  const function *_oneOverPermeability; // if we assume a darcy's law 
  const function *_gradientOfSolidVelocity; // if we assume a darcy's law 
  const function *_prevSol, *_prevSolGrad;
  double _UGlob, _referenceTemperature, _referenceDensity, _referenceConcentration;

  const int HEAT_INDEX;
  const int SA_INDEX;
  const int CONCENTRATION_INDEX;

  public:
  void setup();
  
  /**set the function to evaluate the source term.
   * \f{eqnarray*} \frac{du}{dt} &=& s(0)\\ \frac{dv}{dt} &=& s(1)\f} */
  inline void setSource(const function *usrsource){ checkNonSetup(); _userSource = usrsource;}
  /**set the previous solution */
  inline void setPreviousSolution(const function *prevSol){checkNonSetup(); _prevSol = prevSol; }
  /**set the previous solution gradient */
  inline void setPreviousSolutionGradient(const function *prevSolGrad){checkNonSetup(); _prevSolGrad = prevSolGrad; }

  inline const function* getDensity() {checkSetup(); return _rhoF;};
  inline const function* getViscosity() {checkSetup(); return _muF;};
  inline const double getGlobalVelocity() {checkSetup(); return _UGlob;};
  inline const function* getPreviousSolution(){return _prevSol; }
 
  /**return the velocity vector */
  inline const function* getVelocity(){ checkSetup(); return _velocitySol; };
  /**return the pressure */
  inline const function* getPressure() { 
    checkSetup();    
    return functionExtractCompNew(function::getSolution(),2); 
  }; 
  inline const function* getTurbulentViscosity() { 
    if (SA_INDEX <= 0){
      Msg::Fatal("This conservation law does not compute the turbulent viscosity");
    }
    checkSetup();    
    return functionExtractCompNew(function::getSolution(),SA_INDEX); 
  }; 
  /**return the pressure */
  inline const function* getTemperature() { 
    if (HEAT_INDEX <= 0){
      Msg::Fatal("This conservation law does not compute the temperature");
    }
    checkSetup();    
    return functionExtractCompNew(function::getSolution(),HEAT_INDEX); 
  }; 
  /**return the viscous stresses */
  inline const function * getViscousStress(){ checkSetup(); return _viscousS; }
  /**return the viscous stresses at an interface*/
  inline const function * getViscousNormalStress(){ checkSetup(); return _viscousNS; }
 /**return the viscous stresses at an interface with a given smooth gradient */
  function * newViscousNormalStressSmooth(const function *grad);
  function * newWallShearStressSmooth(const function *grad);

  /**return the unviscous stresses (pressure normal) at the surface */
  inline const function * getInviscousNormalStress(){ checkSetup(); return _inviscousNS; }
  /**return the total force to the surface */
  inline const function* getTotalForce(){
    checkSetup();
    return  functionSumNew(getViscousNormalStress(), getInviscousNormalStress());
  }
  /** return wall shear stress */
  inline const function * getWSS(){ checkSetup(); return _wallShearStress; }
  /** return wall shear stress */
  inline const function * getWSSNorm(){ checkSetup(); 
    return _wallShearStressNorm; 
}

  dgConservationLawNavierStokesIncomp2d(const function *rhoF, 
					const function *muF, 
					const double UGlob, 
					const function *heatConductivityF = 0, 
					const function *heatCapacityF = 0, 
					const function *heatExpansionCoefficientF = 0,
					const double referenceTemperature = 293.0,
					const double referenceDensity = 1000.0,
					const function *distanceToTheWallF = 0,
					const function *oneOverPermeability = 0, 
					const function *gradientOfSolidVelocity = 0);
  ~dgConservationLawNavierStokesIncomp2d();
  /**slip wall boundary */
  dgBoundaryCondition *newBoundarySlipWall();
  /**prescribe velocity field */
  dgBoundaryCondition *newBoundaryVelocity(const function *vel);
  /**prescribe pressure field */
  dgBoundaryCondition *newBoundaryPressure(const function *pres);
};


#endif
