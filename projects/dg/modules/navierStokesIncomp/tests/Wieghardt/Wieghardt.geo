
lc = 5;

Point(1) = {0,0,0,lc/20};   
Point(2) = {35,0,0,lc/20};
Point(3) = {35,40,0,lc};  
Point(4) = {-20,40,0,lc};  
Point(5) = {-20,0,0,lc};  
Point(6) = {0,40,0,lc};   
Point(7) = {-20,3,0,lc};   
Point(8) = {35,3,0,lc};   
Point(9) = {0,3,0,lc/20};   

//Line(1) = {1, 2};
//Line(2) = {2, 3};
//Line(3) = {3, 4};
//Line(4) = {4, 5};
//Line(5) = {5, 1};

//Line Loop(1) = {1, 2, 3, 4, 5};
//Plane Surface(1) = {1};


Line(7) = {4, 6};
Line(8) = {6, 3};
Line(9) = {3, 8};
Line(10) = {8, 2};
Line(11) = {2, 1};
Line(12) = {1, 9};
Line(13) = {9, 8};
Line(14) = {7, 9};
Line(15) = {5, 1};
Line(16) = {5, 7};
Line(17) = {7, 4};
Line Loop(18) = {17, 7, 8, 9, -13, -14};
Plane Surface(19) = {18};
Line Loop(20) = {16, 14, -12, -15};
Plane Surface(21) = {20};
Line Loop(22) = {13, 10, 11, 12};
Plane Surface(23) = {22};
Transfinite Line {14, 15} = 20 Using Progression 1./1.1;
Transfinite Line {13} = 30 Using Progression 1.1;
Transfinite Line {11} = 30 Using Progression 1./1.1;
Transfinite Line {10} = 50 Using Progression 1./1.1;
Transfinite Line {12, 16} = 50 Using Progression 1.1;
Transfinite Surface {21};
Transfinite Surface {23};

Physical Surface("All") = {21,23,19};
Physical Line("Wall") = {11};
Physical Line("Inlet") = {16,17};
Physical Line("Outlet") = {9,10};
Physical Line("Symmetry") = {7,8,15};
Physical Point("CornerBottomLeft") = {5};
Recombine Surface {19, 21, 23};
