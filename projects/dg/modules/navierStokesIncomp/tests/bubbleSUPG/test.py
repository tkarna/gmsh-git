from dgpy import *
from levelset import *
from Common import *
from math import *
import os
import time
import sys

try : os.mkdir("output");
except: 0;

os.system("rm output/*")
os.system("rm bubbleAdapt*.msh")
os.system("rm bubble_simu*.msh")
os.system("rm bubble_current.msh")
os.system("rm Area.dat")
x0 = time.clock()
GmshSetOption('General', 'Verbosity', 1.0)

#-------------------------------------------------
#-- Initial level set
#-------------------------------------------------
def initLS(FCT, XYZ):
        for i in range(0,FCT.size1()):
                x = XYZ.get(i,0)
                y = XYZ.get(i,1)
                phi = math.sqrt((x-.5)*(x-.5) + (y-.5)*(y-.5))-.15
                bw = 0.1; #level set bandwidth
                if (fabs(phi) < bw):
                        phiFilter = 2*bw/pi*sin(0.5*pi*phi/bw)
                else:
                        sign = 1.0 if phi > 0.0 else -1.0
                        phiFilter = 2*bw/pi*sign
                FCT.set(i,0,  phiFilter)
                

CCode = """
#include "fullMatrix.h"
#include "function.h"
extern "C" {
void velocityC (dataCacheMap *, fullMatrix<double> &fct, fullMatrix<double> &xyz, fullMatrix<double> &time) {
double t = time(0,0);
for (size_t i = 0; i< fct.size1(); i++) {
   double x = xyz(i,0);
   double y = xyz(i,1);
   double pi = 3.14;
   //for circle
   double u = -2*pi*(y-0.5); 
   double v = 2*pi*(x-0.5);
   //for bubble in cannal
   double ucan = 1.0*(1-((y-0.5)/0.5)*((y-0.5)/0.5));
   double vcan = 0.0;
   fct.set(i,0,ucan);
   fct.set(i,1,vcan);
   fct.set(i,2,0.0);
  }
}
}
"""
tmpLib = "./tmp.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CCode, tmpLib);

#-------------------------------------------------
#-- Parameters Common
#-------------------------------------------------
meshLS = "bubble"
dt = 0.1
nbExport = 1

#-------------------------------------------------
#-- Parameters LS
#-------------------------------------------------
doAdapt = False
doFEM   = True
doREInit = False
explicit = False
timeOrder  = 2

order = 1
dimension = 2
ls = levelset(meshLS, order, dimension, doAdapt, doFEM, doREInit)
ls.initLevelset(initLS)

if (doAdapt):
        R = 0.15
        ls.E = R/5.0
        lcMin = ls.E/10
        lcMax = 2*3.14*R/10
        eps  = 1.e-1
        #typeOfMetric = "frey" # grad and hession
        typeOfMetric = "coupez" # distance with N and E        
        ls.initAdapt(typeOfMetric, lcMin, lcMax, eps)
        
#-------------------------------------------------
#-- Levelset law and BCs
#-------------------------------------------------

def funbc(ls):
        ls.law.addBoundaryCondition('bottom',ls.law.new0FluxBoundary()) 
        ls.law.addBoundaryCondition('top',ls.law.new0FluxBoundary())
        if (doFEM):
                #ls.law.addBoundaryCondition('left', ls.law.newOutsideValueBoundary("left", function.getSolution() )) #initSOL.getFunction())) #function.getSolution() #NE MARCHE PAS CAR FORMULATION NON CONSERVATIVE
                ls.law.addStrongBoundaryCondition(1, 'left', ls.solutions[0].getFunction()) #initSOL.getFunction()) #ls.solutions[0].getFunction()
                ls.law.addBoundaryCondition('left', ls.law.new0FluxBoundary())
                ls.law.addBoundaryCondition('right', ls.law.new0FluxBoundary())
        else:
                ls.law.addBoundaryCondition('left', ls.law.newOutsideValueBoundary("left", function.getSolution() )) #initSOL.getFunction())) #function.getSolution()
                ls.law.addBoundaryCondition('right', ls.law.newOutsideValueBoundary("right", function.getSolution()))

VELC = functionC(tmpLib,"velocityC",3,[ls.XYZ, TIME])
ls.setLaw(VELC,funbc)

initSOL = dgDofContainer(ls.groups[0],ls.nbFields)
initSOL.interpolate(ls.INIT)

#-------------------------------------------------
#-- Run
#-------------------------------------------------
ATol = 1.e-8
RTol = 1.e-5
Verb = 4
petscOptions = "-pc_type lu" #for monitoring linear solver -ksp_monitor"
#petscOptions ="-ksp_rtol 1.e-2 -pc_type ilu -pc_factor_levels 2"

ls.initSolver(explicit, timeOrder, ATol,RTol, Verb, petscOptions, nbExport)

i = 0
while (ls.t <  2.5):
        if (i % 2 == 0):
          ls.solutions[0].reInitialize(ls.E)
          
        ls.doOneIteration(dt, i)
        i = i + 1

print ('|TOTAL CPU|',time.clock() - x0)

#-------------------------------------------------
#-- Validation
#-------------------------------------------------
fun=open('Area.dat',"r")
lines = fun.readlines()
lastLine = lines[-1].split()
print (lines[-1])
Aloss = fabs(float(lastLine[2]))
fun.close()

print ('|Area loss| ', Aloss)
if ( Aloss < 0.35): 
        print ("Exit with success :-")
        sys.exit(success)
else:
        print ("Exit with failure :-(")
        sys.exit(fail)
