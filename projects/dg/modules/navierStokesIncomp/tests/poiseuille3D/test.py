from dgpy import *
from Incompressible import *
from Common import *
from math import *
import gmshPartition
import sys
import os

try : os.mkdir("output");
except: 0;


#-------------------------------------------------
#-- 3D Hagen-Poiseuille Flow
#-------------------------------------------------
# You should obtain the analytical pressure at the inlet 
# (-dp/dx) = vmax*2*rho*nu/h^2
# example: nu=1,   vmax=2, R=0.5, DL=5.0  --> Pin=80
# example: nu=0.1, vmax=2, R=0.5, DL=5.0   --> Pin=8.0
# WSS = 4*mu/R * 2*vmax
# example mu = 0.1 : vmax = 2 --> wss = 0.8

#-------------------------------------------------
#-- Parameters
#-------------------------------------------------
rho = 1.0
mu  = 0.1 
vmax = 2.0
UGlob = 1.0
  
dim = 3
meshName = "PoiseuilleCylinder"
genMesh(meshName, dim, 1)


#-------------------------------------------------
#-- Initial function
#-------------------------------------------------
def initF(FCT, XYZ):
  for i in range(0,FCT.size1()):
    y = XYZ.get(i,1)
    FCT.set(i,0, 0.0) #u
    FCT.set(i,1, 0.0) #v
    FCT.set(i,2, UGlob) #w
    FCT.set(i,3, 0.0) #p

#-------------------------------------------------
#-- Case specific functions
#-------------------------------------------------
def VelBC(FCT, XYZ):
  for i in range(0,FCT.size1()):
    x = XYZ.get(i,0)    
    y = XYZ.get(i,1)    
    FCT.set(i,0, 0.0) #u
    FCT.set(i,1, 0.0) #v
    #FCT.set(i,2, 1.0) #w
    FCT.set(i,2, vmax*(1-(x*x+y*y)/(0.5*0.5)))		
    FCT.set(i,3, 0.0) #p
    FCT.set(i,4, fixed)
    FCT.set(i,5, fixed)
    FCT.set(i,6, fixed)
    FCT.set(i,7, free)

#-------------------------------------------------
#-- Initialization
#-------------------------------------------------
ns = Incompressible(meshName, dim)
rhoF = functionConstant(rho)
muF = functionConstant(mu)
ns.initializeIncomp(initF, rhoF, muF , UGlob)

print ('**** MAX Reynolds = ', (rho*UGlob*1.0/mu))

#-------------------------------------------------
#-- Boundary Conditions
#-------------------------------------------------
VEL = functionPython(8, VelBC, [ns.XYZ])
ns.strongBoundaryConditionSurface('Inlet', VEL)
ns.strongBoundaryConditionSurface('Wall', ns.WALL)
ns.weakPresBoundaryCondition('Outlet', ns.PZERO)

#-------------------------------------------------
#-- Steady solve
#-------------------------------------------------
#petscOptions ="-ksp_rtol 1.e-3 -pc_type lu -ksp_monitor -pc_factor_levels 2"
petscOptions = ""
if (Msg.GetCommSize() == 1) :
  petscOptions ="-ksp_rtol 1.e-3 -pc_type ilu -pc_factor_levels 1"
else :
  petscOptions ="-ksp_rtol 1.e-3 -sub_pc_type ilu -sub_pc_factor_levels 1"
#ns.steadySolve(petscOptions)

#-------------------------------------------------
#-- Pseudo TimeStepping
#-------------------------------------------------
nbTimeSteps = 3
dt0 = 20
ATol = 1.e-5
RTol = 1.e-8
Verb = 2
ns.pseudoTimeSteppingSolve(2, nbTimeSteps, dt0, ATol, RTol, Verb, petscOptions)

#-------------------------------------------------
#-- Validation 
#-------------------------------------------------

#Analytical:
L = 5 #from the .geo file
R = 0.5 #from the .geo file
pANA = 4*mu*L*vmax/R**2 #pANA is the difference of pressure between the inlet and outlet
print ('Analytical value for pressure difference: ',  pANA)

#Numerical:
eval = dgFunctionEvaluator(ns.groups, ns.solution.getFunction())
result = fullMatrixDouble(4,1);
eval.compute(0,0,0,result)
result._print("result")
pNUM = result.get(3,0)
print ('Numerical value for pressure difference: ' , pNUM)

#Comparison:
diff = abs((pNUM-pANA)/pANA)

#-------------------------------------------------
#-- wall shear stress computation
#-------------------------------------------------
ns.solution.exportFunctionSurf(ns.law.getVelocity(), 'output/surface', 0, 0, 'VEL', ['Inlet', 'Outlet'])
ns.solution.exportFunctionSurf(ns.law.getWSSNorm(), 'output/surface', 0, 0, 'WSS', ['Wall'])
ns.computeWallShearStressL2FE("Wall")

a = 0.0
b = 0.0
c = 1.0
d = -2.5
plane = gLevelsetPlane(a,b,c,d, 99)
planeCut = ns.model.buildCutGModel(plane, True, True)
#ns.model = planeCut
#planeCut.writeMSH("plane.msh",2.2,False,False,True,1.0,0,0)
#exit(1);

hFD = 0.1
nbFD = 4
dirPerp = SVector3(a,b,c)
ns.computeWallShearStressFD(planeCut, dirPerp, hFD, nbFD, mu, "levelset_L99", 1.0)

#-------------------------------------------------
#-- Exit
#-------------------------------------------------

print ('|error DP| = ', diff)
if (diff < 1.3e-1): 
  print ("Exit with success :-)")
  Msg.Exit(0)
else:
  print ("Exit with failure :-(")
  Msg.Exit(1)

