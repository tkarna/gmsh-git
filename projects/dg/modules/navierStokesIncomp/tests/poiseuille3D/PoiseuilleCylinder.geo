//-*- C++ -*-
/*********************************************       
Cube uniformly meshed along all three axes      
**********************************************/       
lc = 0.2;      
// Radius of the cylinder:
R = 0.5; 
// Length of the cylinder:
L = 5; 

Point(0) = {0, 0, 0, lc}; 
Point(1) = {R, 0, 0, lc}; 
Point(2) = {0, R, 0, lc};
Point(3) = {-R, 0, 0, lc};
Point(4) = {0, -R, 0, lc};

Circle(1) = {1,0,2};
Circle(2) = {2,0,3};
Circle(3) = {3,0,4};
Circle(4) = {4,0,1};

nb_pts_edge = 7;
nb_layers = 15;

For iLine In {1:4}
	Transfinite Line{iLine} = nb_pts_edge;
EndFor

Line Loop(1) = {1,2,3,4};
Plane Surface(1) = {1};

out[] = Extrude{0, 0, L}{Surface{1}; Layers{nb_layers};};


//RingIn:
Line Loop(2) = {6,7,8,9};

//Both rings at the inlet and at the outlet will have nr. 20:
//Physical Line(20) = {1,2,3,4,6,7,8,9};


Physical Surface("Wall") = {out[2],out[3],out[4],out[5]};
Physical Surface("Inlet") = {out[1]};
Physical Surface("Outlet") = {out[0]};

Physical Volume ("Vol") = {1};
