from dgpy import *
from Incompressible import *
from math import *
import time
import sys

os.system("mkdir -p output")
os.system("rm -f output/*")
os.system("rm -f Blasius_adapt*")
#-------------------------------------------------
#-- Turbulent NACA
#-------------------------------------------------
#-------------------------------------------------
#-- Parameters
#-------------------------------------------------

Re = 1e4
L = 1
rho = 1.0
mu = 0.01
UGlob = mu * Re / (L * rho)

print 'UGLOB = ',UGlob

nbTimeSteps = 200
timeOrder = 1
dt0 = 1.e-3#/factor
ATol = 1.e-3
RTol = 1.e-3
Verb = 100

meshName = "naca12_2d"

ut = UGlob * math.sqrt (0.0135 / math.pow (Re, 1./7.))
uplus = UGlob / ut

#-------------------------------------------------
#-- Validation - COMPARISON WITH ANALYTICAL DATA
#-------------------------------------------------
def validate(ns) : 
    
  evalSol=dgFunctionEvaluator(ns.groups, ns.solution.getFunction())
  
  fun = open("Wieghardt3.dat",'w')

  # yplus = 1 ---> y = 1/(ut Re)
  yplus = mu/ (rho*ut)
  for i in range(1,10000):
    y = yplus * (i+1)
    fun.write("%g " % i)
    result = fullMatrixDouble(1,1)
    evalSol.compute(35., y,0.0,result)
    u = result.get(0,0) / uplus
    # spalding solution
#    yplus_spalding = u + math.exp(k*B) * (math.exp(
    fun.write("%g " % u)
    fun.write("\n")

  fun.close()
  
#-------------------------------------------------
#-- Initial function
#-------------------------------------------------
def initF(FCT, XYZ) :
  for i in range(0,FCT.size1()):
    y = XYZ.get(i,1)
    FCT.set(i,0, UGlob)
    FCT.set(i,1, 1.e-12*y*y*y) 
    FCT.set(i,2, 0.0) 
    FCT.set(i,3, 5*mu) 

def InletBC(FCT, XYZ) :
  for i in range(0,FCT.size1()):
    FCT.set(i,0, UGlob)
    FCT.set(i,1, 0.0)
    FCT.set(i,2, 0.0)
    FCT.set(i,3, 5*mu)
    FCT.set(i,4, fixed)
    FCT.set(i,5, fixed)
    FCT.set(i,6, free)
    FCT.set(i,7, fixed)

print('MAX Reynolds (End wing)= ',UGlob*rho*1/mu);
print('YPlus = 1 ---> y = ',mu/(ut * rho));
print('BL Thickness @ x=10 = ',4.9*math.sqrt(mu*10/UGlob));
print('BL Thickness @ x=20 = ',4.9*math.sqrt(mu*20/UGlob));

genMesh(meshName,2,1)

rhoF = functionConstant(rho)
muF = functionConstant(mu)


ns = Incompressible(meshName, 2)
DIST_TO_MESH  = gLevelsetDistMesh(ns.model,"Wall")
DIST  = functionLevelsetValueNew(DIST_TO_MESH)
WD = functionPrecomputed (ns.groups,3,1)
WD.compute (DIST)

ns.initializeIncomp(initF, rhoF, muF , UGlob, WD)
ns.solution.exportFunctionMsh(DIST, 'output/ns', 0, 0, 'dist')
exit(1)

INLET    = functionPython(8, InletBC, [ns.XYZ])
ns.strongBoundaryConditionLine('Inlet', INLET)
ns.strongBoundaryConditionLine('Symmetry', INLET)
#ns.strongBoundaryConditionLine('Symmetry', ns.VELX)
ns.strongBoundaryConditionLine('Wall', ns.WALL)
ns.strongBoundaryConditionLine('Outlet', ns.PZERO)
#ns.weakPresBoundaryCondition('Outlet', ns.PZERO)

petscOptions ="-ksp_rtol 1.e-3 -pc_type lu -pc_factor_levels 2 -ksp_monitor"
#UGlob = UGlob / 10
ns.pseudoTimeSteppingSolve(timeOrder, nbTimeSteps, dt0, ATol, RTol, Verb, petscOptions,1.5)
validate (ns)
ns.solution.exportFunctionSurf(ns.law.getWSS(), 'output/surface', 0, 0, 'Cf', ['Wall'])

  

