from dgpy import *
from Incompressible import *
from Common import *
from math import *
import gmshPartition
import sys
import os

try : os.mkdir("output");
except: 0;

os.system("rm output/*")

#-------------------------------------------------
#-- 3D Hagen-Poiseuille Flow
#-------------------------------------------------
# You should obtain the analytical pressure at the inlet 
# (-dp/dx) = vmax*2*rho*nu/h^2
# example: nu=1,   vmax=2, R=0.5, DL=5.0  --> Pin=80
# example: nu=0.1, vmax=2, R=0.5, DL=5.0   --> Pin=8.0

#-------------------------------------------------
#-- Parameters
#-------------------------------------------------
rho = 1.0
mu  = 0.1 
vmax = 2.0
UGlob = 1.0
  
dim = 3

print('---- GENERATE BOX MESH')
boxName = "rect3D"
genMesh(boxName,dim,1)
g = GModel()
g.load(boxName+msh)
#exit(1)

print('---- DEFINE LEVELSET FUNCTIONS')
gSTL = GModel()
gSTL.load("cylPHYS.msh")
ls1 = gLevelsetDistMesh(gSTL,"wall", 1)
ls2 = gLevelsetMathEvalAll(["-x+2.0", "-1.0", "0.0", "0.0",
			    "0.0", "0.0", "0.0",
			    "0.0", "0.0", "0.0",
			    "0.0", "0.0", "0.0"], 2)
ls3 = gLevelsetMathEvalAll(["x-8.0","1.0", "0.0", "0.0",
			    "0.0", "0.0", "0.0",
			    "0.0", "0.0", "0.0",
			    "0.0", "0.0", "0.0"], 3)
myLS = gLevelsetIntersection([ls1,ls2,ls3])


print('---- ADAPT MESH WITH LEVELSET')
GmshSetOption("Mesh","SmoothRatio",   2.0);
hmin = 2.e-3
hmax = 0.35
E = 0.2
g.adaptMesh([5,5,5], [ls1,ls2,ls3], [[E,hmin,hmax],[E,hmin,hmax],[E,hmin,hmax]], 10, False)
#g.adaptMesh([5], [myLS], [[E,hmin,hmax]], 20, False)
g.save("rect3D_ADAPT"+msh)

print('---- SPLIT the mesh')
meshName = "rect3D_SPLIT"
g2 = g.buildCutGModel(myLS, False, True)
g2.writeMSH(meshName+msh,2.2,False,False,True,1.0,0,0)

#-------------------------------------------------
#-- Initial function
#-------------------------------------------------
def initF(FCT, XYZ):
  for i in range(0,FCT.size1()):
    y = XYZ.get(i,1)
    FCT.set(i,0, UGlob) #u
    FCT.set(i,1, 0.0) #v
    FCT.set(i,2, 0.0) #w
    FCT.set(i,3, 0.0) #p

#-------------------------------------------------
#-- Case specific functions
#-------------------------------------------------
def VelBC(FCT, XYZ):
  for i in range(0,FCT.size1()):
    x = XYZ.get(i,0)    
    y = XYZ.get(i,1)
    z = XYZ.get(i,2)   
    FCT.set(i,0, vmax*(1.-(z*z+y*y)/(0.5*0.5))) #u
    #FCT.set(i,0, vmax) #u
    FCT.set(i,1, 0.0) #v
    FCT.set(i,2, 0.0) #w		
    FCT.set(i,3, 0.0) #p
    FCT.set(i,4, fixed)
    FCT.set(i,5, fixed)
    FCT.set(i,6, fixed)
    FCT.set(i,7, free)

#-------------------------------------------------
#-- Initialization
#-------------------------------------------------
ns = Incompressible(meshName, dim, ["vol_in"])
rhoF = functionConstant(rho)
muF = functionConstant(mu)
ns.initializeIncomp(initF, rhoF, muF , UGlob)

print ("MAX Reynolds = ", (rho*UGlob*1.0/mu))

#-------------------------------------------------
#-- Boundary Conditions
#-------------------------------------------------
VEL = functionPython(8, VelBC, [ns.XYZ])
ns.strongBoundaryConditionSurface('levelset_S2', VEL)
ns.strongBoundaryConditionSurface('levelset_S1', ns.WALL)
ns.strongBoundaryConditionSurface('levelset_S3', ns.PZERO)

#-------------------------------------------------
#-- Steady solve
#-------------------------------------------------

petscOptions = ""
if (Msg.GetCommSize() == 1) :
  petscOptions ="-ksp_rtol 1.e-3 -pc_type ilu -pc_factor_levels 1"
else :
  petscOptions ="-ksp_rtol 1.e-3 -sub_pc_type ilu -sub_pc_factor_levels 1"

petscOptions ="-ksp_rtol 1.e-4 -pc_type lu -ksp_monitor -pc_factor_levels 0"
ns.steadySolve(20, petscOptions)

#-------------------------------------------------
#-- Pseudo TimeStepping
#-------------------------------------------------
nbTimeSteps = 3
dt0 = 20
ATol = 1.e-5
RTol = 1.e-8
Verb = 2
#ns.pseudoTimeSteppingSolve(2, nbTimeSteps, dt0, ATol, RTol, Verb, petscOptions)

ns.solution.exportFunctionSurf(ns.law.getVelocity(), 'output/surface', 0, 0, 'vel', ['levelset_S2', 'levelset_S3'])
#ns.solution.exportFunctionSurf(ns.law.getWSSNorm(), 'output/surface', 0, 0, 'wss', ['Wall'])

#-------------------------------------------------
#-- Validation 
#-------------------------------------------------

#Analytical:
L = 10 #from the .geo file
R = 0.5 #from the .geo file
pANA = 4*mu*L*vmax/R**2 #pANA is the difference of pressure between the inlet and outlet
print ("Analytical value for pressure difference: ",  pANA)

#Numerical:
eval = dgFunctionEvaluator(ns.groups, ns.solution.getFunction())
result = fullMatrixDouble(4,1);
eps = 1.e-4;
eval.compute(0+eps,0,0,result)
pNUM = result.get(3,0)
print ("Numerical value for pressure difference: " , pNUM)

#Comparison:
diff = abs((pNUM-pANA)/pANA)

print ('|error DP| = ', diff)
if (diff < 1.3e-1): 
  print ("Exit with success :-)")
  Msg.Exit(0)
else:
  print ("Exit with failure :-(")
  Msg.Exit(1)
