from dgpy import *
from Incompressible import *
from math import *
import sys
import os

try : os.mkdir("output");
except: 0;

os.system("rm *.msh output/*")
#-------------------------------------------------
#-- 2D Hagen-Poiseuille Flow
#-------------------------------------------------
# You should obtain the analytical pressure at the inlet 
# (-dp/dx) = vmax*2*rho*nu/h^2
# cf = 2*mu*vmax/h = 8*mu (if vmax = 2)
# example: mu=1,   vmax=2, h=0.5, DX=2   --> Pin=32
# example: mu=0.1, vmax=2, h=0.5, DX=2   --> Pin=3.2
 
#-------------------------------------------------
#-- Parameters
#-------------------------------------------------
meshName = "Poiseuille2D"
vmax = 2.0
UGlob = 2./3.*vmax
RHO = 1.0
MU  = 0.1

print('*** Reynolds= %g NU=%g ' % (RHO*UGlob*1/MU, MU/RHO))
genMesh(meshName,2,1)

#-------------------------------------------------
#-- Initial function
#-------------------------------------------------
def initF(FCT, XYZ):
  for i in range(0,FCT.size1()):
    y = XYZ.get(i,1)
    #FCT.set(i,0, vmax*(1-(y/0.5)*(y/0.5)))
    FCT.set(i,0, UGlob) #u
    FCT.set(i,1, 0.0) #v
    FCT.set(i,2, 0.0) #p

#-------------------------------------------------
#-- Case specific functions
#-------------------------------------------------
def VelBC(FCT, XYZ ):
  for i in range(0,FCT.size1()):
    y = XYZ.get(i,1)
    FCT.set(i,0, vmax*(1-(y/0.5)*(y/0.5)))
    #FCT.set(i,0, 1.0)
    FCT.set(i,1, 0.0)
    FCT.set(i,2, 0.0)
    FCT.set(i,3, fixed)
    FCT.set(i,4, fixed)
    FCT.set(i,5, free)
    
#-------------------------------------------------
#-- Initialization
#-------------------------------------------------
ns = Incompressible(meshName, 2)
rhoF = functionConstant(RHO)
muF = functionConstant(MU)
ns.initializeIncomp(initF, rhoF, muF , UGlob)

#-------------------------------------------------
#-- Boundary Conditions
#-------------------------------------------------
VEL = functionPython(6, VelBC, [ns.XYZ])

ns.strongBoundaryConditionLine('Inlet', VEL)
ns.strongBoundaryConditionLine('WallTop', ns.WALL)
ns.strongBoundaryConditionLine('WallBottom', ns.WALL)
ns.weakPresBoundaryCondition('Outlet', ns.PZERO)

#-------------------------------------------------
#-- Steady solve
#-------------------------------------------------

petscOptions ="-ksp_rtol 1.e-3 -pc_type lu -pc_factor_levels 0"
#maxIter = 30
#ns.steadySolve(maxIter, petscOptions)

#-------------------------------------------------
#-- Pseudo TimeStepping
#-------------------------------------------------
nbTimeSteps = 3
dt0 = 1000
ATol = 1.e-8
RTol = 1.e-5
Verb = 2

ns.pseudoTimeSteppingSolve(2, nbTimeSteps, dt0, ATol, RTol, Verb, petscOptions)

#-------------------------------------------------
#-- wall shear stress computation
#-------------------------------------------------

ns.solution.exportFunctionSurf(ns.law.getVelocity(), 'output/surface', 0, 0, 'VEL', ['Inlet', 'Outlet'])
ns.solution.exportFunctionSurf(ns.law.getWSSNorm(), 'output/surface', 0, 0, 'WSS', ['WallTop'])

hFD = 0.001 #0.9*hmin
nbFD = 5
name = "WallTop"
fScale  = 1.0
NParam = 0
dirPerp = SVector3(0., 0., 1.0)

ns.computeWallShearStressFD(ns.model, dirPerp, hFD, nbFD, MU, name, fScale, NParam)
ns.computeWallShearStressLEAST_SQUARES(MU, name, fScale)
ns.computeWallShearStressEIGEN(MU, name, 1.0)
ns.computeWallShearStressL2FE(name)

#-------------------------------------------------
#-- Validation 
#-------------------------------------------------

eval = dgFunctionEvaluator(ns.groups, ns.solution.getFunction())
result = fullMatrixDouble(3,1);
eval.compute(0,0,0,result)
pNUM = result.get(2,0)
pANA = 32.*MU
diff = fabs((pNUM-pANA)/pANA)

print ('|error DP| ', diff)
if (diff < 1.e-1): 
  print ("Exit with success :-)")
  sys.exit(success)
else:
  print ("Exit with failure :-(")
  sys.exit(fail)
