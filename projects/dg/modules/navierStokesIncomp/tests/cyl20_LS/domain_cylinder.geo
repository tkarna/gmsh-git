// Gmsh project created on Wed Sep  5 12:12:02 2012
Point(1) = {-15, -7.5, 0, 0.5};
Point(2) = {-15, 7.5, 0, 0.5};
Point(3) = {15, 7.5, 0, 0.5};
Point(4) = {15, -7.5, 0, 0.5};

Line(1) = {2, 1};
Line(2) = {1, 4};
Line(3) = {4, 3};
Line(4) = {3, 2};

Physical Line("inlet") = {1};
Physical Line("top") = {4};
Physical Line("outlet") = {3};
Physical Line("bottom") = {2};

Line Loop(15) = {1, 2, 3, 4};
Plane Surface(16) = {15};
Physical Surface("domain") = {16};
