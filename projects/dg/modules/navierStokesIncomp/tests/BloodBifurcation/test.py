from Bloodflow import *
from math import *
import os

try : os.mkdir("output");
except: 0;

os.system("rm output/*")

#----------------------------------------------------------------  
#-- Blood Parameters
#----------------------------------------------------------------  
meshName = "bifurcation"
order = 4
Fluxes  = ['UPW', 'HLL', 'ROE', 'LAX']

for Flux in Fluxes :
    print("***************** FLUX = %s *****************" % Flux)
    rho  = 1.06 
    ViscDyn = 0.0
    ViscLaw = 'PoiseuilleProfile' # ='FlatProfile'
    p0 = 0.0

    # Diameter and pulse wave velocity for each artery:
    #data_Dc   = {"aorta":[1.0 , 116.5], "iliac1":[1.0 , 116.5],"iliac2":[1.0 , 116.5]} # idem: ok
    data_Dc   = {"aorta":[1.0 , 116.5], "iliac1":[0.4 , 116.5],"iliac2":[0.4 , 116.5]} #symmetrique: ok
    #data_Dc   = {"aorta":[1.0 , 116.5], "iliac1":[0.7 , 100.],"iliac2":[0.1 , 250.]}    #asymmetrique: ok

    data_Leakage   = {"aorta":0.0 , "iliac1":0.0, "iliac2":0.0}

    #----------------------------------------------------------------  
    #-- Initialization
    #----------------------------------------------------------------  

    (model, groups, law, solution) = initializeIncomp( meshName,order, Flux, rho, ViscDyn, ViscLaw, p0, data_Dc, data_Leakage )

    #----------------------------------------------------------------  
    #-- Boundary Conditions
    #----------------------------------------------------------------

    InletFileName = "BloodVel_Profile.dat"
    os.putenv("INLETFILE", InletFileName)

    #--inletAbsorbingPressureBoundaryCondition(law,'Inlet',inlet_pres)
    inletAbsorbingVelocityBoundaryCondition(law,'Inlet',inlet_vel)

    #-- 0D model, outlet param
    Rt = 0.0
    #R = 1890.0
    #C = 6.31e-4
    #Pout = 13333.0
    MyRt = functionConstant([Rt])
    outletTerminalResistanceBoundaryCondition(law,'Outlet1',MyRt) 
    outletTerminalResistanceBoundaryCondition(law,'Outlet2',MyRt) 
    #MyWK = functionConstant([R,C, Pout])
    #outletWindkesselBoundaryCondition(law,'Outlet1',MyWK)
    #outletWindkesselBoundaryCondition(law,'Outlet2',MyWK)

    #----------------------------------------------------------------  
    #-- Solvers for NS
    #----------------------------------------------------------------  
    Tc = 0.1
    tFinal = 0.306 #0.5
    dt_export = Tc/10
    explicit = True

    outDir = 'output'
    outList = ['aorta', 'iliac1', 'iliac2']

    unsteadySolve(explicit, model, groups, law, solution, tFinal, dt_export, outDir, outList)

    #----------------------------------------------------------------  
    #-- Validation
    #----------------------------------------------------------------

    fun=open('output/pressure-iliac1.txt',"r")
    lines = fun.readlines()
    lastLine = lines[-1].split()
    print (lines[-1])
    p_daughter = float(lastLine[2])
    p_daughterA = 90
    diff_d = fabs((p_daughter-p_daughterA)/p_daughterA)
    fun.close()

    fun=open('output/pressure-aorta.txt',"r")
    lines = fun.readlines()
    lastLine = lines[-1].split()
    print (lines[-1])
    p_mother = float(lastLine[2])
    p_motherA = 30
    diff_m = fabs((p_mother-p_motherA)/p_motherA)
    fun.close()

    print ('|error DP_mother| ', diff_m)
    print ('|error DP_daughter| ', diff_d)
    if (diff_m > 1.e-1 or diff_d > 1.e-1): 
        print ("Exit with failure :-(")
        sys.exit(fail)
print ("Exit with success :-)")
Msg.Exit(0)


