#ifndef _DG_CONSERVATION_STOKES_3D
#define _DG_CONSERVATION_STOKES_3D
#include "dgConservationLawFunction.h"
#include "functionGeneric.h"

/**The stabilized incompressible Navier-Stokes equations. (u,v,w,p,[T]) */
class  dgConservationLawStokes3d : public dgConservationLawFunction {
  class diffusiveFlux;
  class gradPsiTerm;
  class source;
  class boundarySlipWall;
  class boundaryVelocity;
  class boundaryPressure;
  class inletVelocity;
  class maxConvectiveSpeed;
  class velocityVector;
  class diffusivity;
  class riemann;
  class boundaryTerm;
  const function *_source, *_fzero, *_fzerov, *_fzerograd;
  const function *_diffusiveFlux, *_ipTerm;
  const function *_velocitySol;
  const function *_prevSol, *_prevSolGrad;
  double _rho, _nu, _UGlob;

  public:
  void setup();
  
  /**set the function to evaluate the source term.
   * \f{eqnarray*} \frac{du}{dt} &=& s(0)\\ \frac{dv}{dt} &=& s(1)\f} */
  inline void setSource(const function *source){ checkNonSetup(); _source = source;}
  /**set the density rho */
  inline void setDensity(const double rho){checkNonSetup(); _rho = rho;} 
  /**set the kinematic viscosity nu */
  inline void setViscosity(const double nu){checkNonSetup(); _nu = nu;} 
  /**set the global velocity (needed for PSPG stab), default value is 1. */
  inline void setGlobalVelocity(const double UGlob){checkNonSetup(); _UGlob = UGlob;} 
  inline const double getDensity() {return _rho;};
  inline const double getViscosity() {return _nu;};
  inline const double getGlobalVelocity() {return _UGlob;};

  /**set the previous solution */
  inline void setPreviousSolution(const function *prevSol){ _prevSol = prevSol; }
  /**set the previous solution gradient */
  inline void setPreviousSolutionGradient(const function *prevSolGrad){ _prevSolGrad = prevSolGrad; }
  inline const function* getPreviousSolution(){return _prevSol; }

  /**return the velocity vector */
  inline const function* getVelocity(){ checkSetup(); return _velocitySol; };
  /**return the pressure */
  inline function* getPressure() { 
    checkSetup();    
    return functionExtractCompNew(function::getSolution(),3); 
  }; 

   dgConservationLawStokes3d();
  ~ dgConservationLawStokes3d();
  /**slip wall boundary */
  dgBoundaryCondition *newBoundarySlipWall();
  /**prescribe velocity field */
  dgBoundaryCondition *newBoundaryVelocity(const function *vel);
  /**prescribe pressure field */
  dgBoundaryCondition *newBoundaryPressure(const function *pres);
};
#endif
