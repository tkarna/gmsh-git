#ifndef _DG_CONSERVATION_LAW_NAVIER_STOKES_1D_
#define _DG_CONSERVATION_LAW_NAVIER_STOKES_1D_
#include "dgConservationLawFunction.h"
#include "functionGeneric.h"
class dgConservationLawNavierStokes1d : public dgConservationLawFunction {
  class advection;
  class source;
  class riemannLAX;
  class riemannROE;
  class riemannHLL;
  class riemannUPWIND;
  class riemannBifurcation;
  class boundaryWall;
  class inletAV;
  class inletRV;
  class inletAP;
  class outletRT;
  class outletWK;
  class clipToPhysics;
  class maxConvectiveSpeed;
  class celerityShallow;
  class pressureShallow;
  class charShallow;
  class celerityBlood;
  class pressureBlood;
  class charBlood;
  class A0Blood;
  class betaBlood;
  const function  *_bathymetry, *_linearDissipation, *_pressure, *_celerity, *_characteristic, *_beta, *_sourceMass;
  const function *_diameterAndCelerity0;
  double _rho, _p0;
  bool _shallow, _blood;
  std::string _typeRiemann;
  public:
  void setup();
  ~dgConservationLawNavierStokes1d();
  /**set the 1D shallow water formulation */
  inline void setShallowFormulation() {checkNonSetup(); _shallow = true;  _blood = false;}
  /**set the 1D blood flow formulation */
  inline void setBloodFormulation()   {checkNonSetup(); _shallow = false; _blood = true;}
  /**set the type of Riemann Solver (LAX, HLL, ROE, UPW) */
  inline void setRiemannSolver(std::string type) {checkNonSetup(); _typeRiemann = type;}

  /**set the function to evaluate the bathymetry h (H = h+eta)*/
  inline void setBathymetry(const function *bathymetry) {checkNonSetup(); _bathymetry = bathymetry;}
  /**set the function to evaluate the linear dissipation Kr u/A*/
  inline void setLinearDissipation(const function *linearDissipation){checkNonSetup(); _linearDissipation = linearDissipation;}
  /**set the equilibrium Pressure p0 */
  inline void setEquilibriumPressure(const double p0){checkNonSetup(); _p0 = p0;}
  /**set the density rho */
  inline void setDensity(const double rho){checkNonSetup(); _rho = rho;} 

  /**set the function to evaluate the pressure flux */
  inline void setPressureFlux(const function *pressure) {checkNonSetup(); _pressure = pressure;}
  /**set the function to the characteristics */
  inline void setCharacteristicFunction(const function *chars) {checkNonSetup(); _characteristic = chars;}

  /**set the function to evaluate the source in the mass equation Sm */
  inline void setSourceMass(const function *sourceMass){checkNonSetup(); _sourceMass = sourceMass;}

  /**set the beta parameter */
  inline void setParamPressureLaw(const function *beta) {checkNonSetup(); _beta = beta;}
  /**set a function that contains diameter and celerity */
  inline void setDiameterAndCelerity(const function *DC0) { 
    checkNonSetup(); 
    _bathymetry =  newA0Blood(DC0);
    _beta = newBetaBlood(DC0, getDensity());
  }
  
  //emi check this
  inline function* getPressure() {checkSetup(); return functionScaleNew(_pressure,_rho);};
  /**getArea A = eta + A0 */
  inline function* getArea() { 
    checkSetup();    
    //return _section;
    function *eta = functionExtractCompNew(function::getSolution(),0); 
    return  functionSumNew(eta, _bathymetry); 
  }; 
  /**getFlowRate (Q = A*u) */
  inline function* getFlowRate() {
    checkSetup(); 
    function *vel = functionExtractCompNew(function::getSolution(),1); 
    return functionProdNew(vel, getArea());
  };
  // Marie to check
  inline function* getVelocity() {
    checkSetup(); 
    return functionExtractCompNew(function::getSolution(),1);
  };

  inline const function* getPressureFlux() {checkSetup(); return _pressure;};
  inline const function* getBathymetry() {checkSetup(); return _bathymetry;};
  inline const function* getCharacteristic() {checkSetup(); return _characteristic;};
  inline const function* getBeta() {return _beta;};
  inline const double getDensity() {return _rho;};
  inline const double getP0() {return _p0;};

  dgConservationLawNavierStokes1d();
  /**Slip wall boundary*/
  dgBoundaryCondition *newBoundaryWall();
  /**inlet absorbing pressure boundary condition */
  dgBoundaryCondition *newInletAbsorbingPressure(const function *pres);
  /**inlet absorbing velocity boundary condition */
  dgBoundaryCondition *newInletAbsorbingVelocity(const function *vel);
  /**inlet reflecting velocity boundary condition */
  dgBoundaryCondition *newInletReflectingVelocity(const function *vel);
  /**outlet with terminal resistance coefficient Rt */
  dgBoundaryCondition *newOutletTerminalResistance(const function *Rt);
  /**outlet RCR windkessel model.
   * \arg RCP vector [R C Pout]*/
  dgBoundaryCondition *newOutletWindkessel(const function *RCP);

  function *newA0Blood(const function *f);
  function *newBetaBlood(const function *f, const double rho);
};

#endif
