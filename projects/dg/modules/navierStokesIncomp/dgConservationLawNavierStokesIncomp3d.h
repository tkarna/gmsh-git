#ifndef _DG_CONSERVATION_NAVIER_STOKES_INCOMP_3D
#define _DG_CONSERVATION_NAVIER_STOKES_INCOMP_3D
#include "dgConservationLawFunction.h"
#include "functionGeneric.h"

/**The stabilized incompressible Navier-Stokes equations. (u,v,w,p,[T]) */
class  dgConservationLawNavierStokesIncomp3d : public dgConservationLawFunction {
  class gradPsiTerm;
  class boundarySlipWall;
  class boundaryVelocity;
  class boundaryPressure;
  class inletVelocity;
  class maxConvectiveSpeed;
  class velocityVector;
  class boundaryTerm;
  class source;
  class viscousNormalStress;
  class inviscousNormalStress;
  class wallShearStress;
  class wallShearStressNorm;
  const function *_source, *_fzero, *_fzerov, *_fzerograd;
  const function *_rhoF, *_muF;
  const function *_diffusiveFlux;
  const function *_velocitySol, *_viscousNS, *_inviscousNS, *_wallShearStress, *_wallShearStressNorm;
  const function *_prevSol, *_prevSolGrad;
  double  _UGlob;

  public:
  void setup();
  
  /**set the function to evaluate the source term.
   * \f{eqnarray*} \frac{du}{dt} &=& s(0)\\ \frac{dv}{dt} &=& s(1)\f} */
  inline void setSource(const function *source){ checkNonSetup(); _source = source;}
  /**set the global velocity (needed for PSPG stab), default value is 1. */
  inline void setGlobalVelocity(const double UGlob){checkNonSetup(); _UGlob = UGlob;} 
  /**set the previous solution */
  inline void setPreviousSolution(const function *prevSol){ _prevSol = prevSol; }
  /**set the previous solution gradient */
  inline void setPreviousSolutionGradient(const function *prevSolGrad){ _prevSolGrad = prevSolGrad; }
  inline const function* getPreviousSolution(){return _prevSol; }

  inline const function* getDensity() {return _rhoF;};
  inline const function* getViscosity() {return _muF;};
  inline const double getGlobalVelocity() {return _UGlob;};
  /**return the velocity vector */
  inline const function* getVelocity(){ checkSetup(); return _velocitySol; };
  /**return the pressure */
  inline function* getPressure() { 
    checkSetup();    
    return functionExtractCompNew(function::getSolution(),3); 
  }; 

  /**return the viscous stresses at an interface*/
  inline const function * getViscousNormalStress(){ checkSetup(); return _viscousNS; }
  /**return the unviscous stresses (pressure normal) at the surface */
  inline const function * getInviscousNormalStress(){ checkSetup(); return _inviscousNS; }
  /**return the viscous stresses at an interface with a given smooth gradient */
  function * newViscousNormalStressSmooth(const function *grad);
  function * newWallShearStressSmooth(const function *grad);
  
  /**return the total force to the surface */
  inline const function* getTotalForce(){
    checkSetup();
    return  functionSumNew(getViscousNormalStress(), getInviscousNormalStress());
  }
  /** return wall shear stress */
  inline const function * getWSS(){ checkSetup(); return _wallShearStress; }
  /** return wall shear stress */
  inline const function * getWSSNorm(){ checkSetup(); return _wallShearStressNorm; }
  
   dgConservationLawNavierStokesIncomp3d(const function *rhoF, const function *muF, const double UGlob);
  ~ dgConservationLawNavierStokesIncomp3d();
  /**slip wall boundary */
  dgBoundaryCondition *newBoundarySlipWall();
  /**prescribe velocity field */
  dgBoundaryCondition *newBoundaryVelocity(const function *vel);
  /**prescribe pressure field */
  dgBoundaryCondition *newBoundaryPressure(const function *pres);
};
#endif
