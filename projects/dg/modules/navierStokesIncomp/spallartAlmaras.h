#ifndef _SPALLART_ALMARAS_
#define _SPALLART_ALMARAS_

namespace spallartAlmaras 
{
  const double _cb1   = 0.1355;
  const double _sigma = 2./3.;
  const double _cb2 = 0.622;
  const double _kappa = 0.41;
  const double _kappa_2 = _kappa * _kappa;
  const double _cw2 = 0.3;
  const double _cw3 = 2.0;
  const double _cw3_6 = pow(_cw3, 6.0);
  const double _cv1 = 7.1;
  const double _cv1_3 = pow(_cv1, 3.0);
  const double _ct3 = 1.2;
  const double _ct4 = 0.5;
  const double _cw1 = _cb1/_kappa_2 + (1.+_cb2) / _sigma;
  double _fv1, _fv2, _st, _ft2, _fw;
  
  void setup (double d, double nut, double chi, double omega){
    const double chi3 = chi*chi*chi;
    _fv1 = chi3 / (chi3 + _cv1_3);
    _fv2 = 1. - chi / (1 + chi * _fv1);    

    const double sbar = nut * _fv2 / (_kappa_2 *d *d);

    // do not allow _st to be negative
    if (sbar >= - 0.7 * omega) 
      _st = omega + sbar;
    else 
      _st = omega + (omega * (.7 *.7 * omega + 0.9 * sbar)) / ((0.9 - 2*0.7)*omega - sbar);
    
    _ft2 = _ct3 * exp (-_ct4 * chi * chi );
    const double r = std::min ( nut / (_st * _kappa_2 *d *d ) , 10.0);
    const double r2 = r*r;
    const double r6 = r2*r2*r2;
    const double g = r + _cw2 * (r6 - r);
    const double g2 = g*g;
    const double g6 = g2*g2*g2;
    _fw = g * pow ((1 + _cw3_6)/(g6 + _cw3_6), 1./6.);
  } 
  double fv1 ( double chi ) {
    const double chi3 = chi*chi*chi;
    return chi3 / (chi3 + _cv1_3);    
  }
}

#endif
