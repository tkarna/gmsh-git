#include "function.h"
#include "functionGeneric.h"
#include "math.h"
#include "Numeric.h"

#include "dgConservationLawFunction.h"
#include "dgConservationLawMaxwell.h"
#include "dgDofContainer.h"
#include "dgFunctionIntegrator.h"


/*==============================================================================
 * DG-terms
 *============================================================================*/

class dgConservationLawMaxwell::psiVolumeTerm : public function {
  int sigmaDefined, sourceFieldDefined;
  fullMatrix<double> sol, epsilon, sigma, sourceField;
 public:
  psiVolumeTerm(int nbf,
                const function *epsilonFunction,
                const function *sigmaFunction,
                const function *sourceFieldFunction) : function(nbf) {
    setArgument (sol, function::getSolution());
    setArgument (epsilon, epsilonFunction);
    sigmaDefined = 0;
    if (sigmaFunction) {
      setArgument (sigma, sigmaFunction);
      sigmaDefined = 1;
    }
    sourceFieldDefined = 0;
    if (sourceFieldFunction) {
      setArgument (sourceField, sourceFieldFunction);
      sourceFieldDefined = 1;
    }
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    if (sigmaDefined)
      for (size_t i=0; i<nQP; i++)
          for (int j=0; j<3; j++)
            val(i,j) -= (sigma(i,j)/epsilon(i,j)) * sol(i,j);
    if (sourceFieldDefined)
      for (size_t i=0; i<nQP; i++)
          for (size_t j=0; j<3; j++)
            val(i,j) += sourceField(i,j)/epsilon(i,j);
  }
};

class dgConservationLawMaxwell::dPsiVolumeTerm : public function {
  int _nbf;
  fullMatrix<double> sol, epsilon, mu;
 public:
  dPsiVolumeTerm(int nbf,
                 const function *epsilonFunction,
                 const function *muFunction) : function(nbf*3) {
    _nbf = nbf;
    setArgument (sol, function::getSolution());
    setArgument (epsilon, epsilonFunction);
    setArgument (mu, muFunction);
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double e[3] = {sol(i,0), sol(i,1), sol(i,2)};
      double h[3] = {sol(i,3), sol(i,4), sol(i,5)};
      
      // term \partial_x
      val(i,0) = 0;
      val(i,1) = h[2]/epsilon(i,1);
      val(i,2) =-h[1]/epsilon(i,2);
      val(i,3) = 0;
      val(i,4) =-e[2]/mu(i,1);
      val(i,5) = e[1]/mu(i,2);
      
      // term \partial_y
      val(i,_nbf+0) =-h[2]/epsilon(i,0);
      val(i,_nbf+1) = 0;
      val(i,_nbf+2) = h[0]/epsilon(i,2);
      val(i,_nbf+3) = e[2]/mu(i,0);
      val(i,_nbf+4) = 0;
      val(i,_nbf+5) =-e[0]/mu(i,2);
      
      // term \partial_z
      val(i,2*_nbf+0) = h[1]/epsilon(i,0);
      val(i,2*_nbf+1) =-h[0]/epsilon(i,1);
      val(i,2*_nbf+2) = 0;
      val(i,2*_nbf+3) =-e[1]/mu(i,0);
      val(i,2*_nbf+4) = e[0]/mu(i,1);
      val(i,2*_nbf+5) = 0;
      
    }
  }
};

class dgConservationLawMaxwell::interfaceTerm : public function {
  int _nbf;
  fullMatrix<double> normals;
  fullMatrix<double> solL, epsilonL, muL;
  fullMatrix<double> solR, epsilonR, muR;
 public:
   interfaceTerm(int nbf,
                 const function *epsilonFunction,
                 const function *muFunction) : function (nbf*2) {
    _nbf = nbf;
    setArgument (normals, function::getNormals(), 0);
    setArgument (solL, function::getSolution(), 0);
    setArgument (solR, function::getSolution(), 1);
    setArgument (epsilonL, epsilonFunction, 0);
    setArgument (epsilonR, epsilonFunction, 1);
    setArgument (muL, muFunction, 0);
    setArgument (muR, muFunction, 1);
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double n[3];
      for (int j=0; j<3; j++)
        n[j] = normals(i,j);
      
      // Physical parameters
      double epsilonVecL[3], muVecL[3];
      double epsilonVecR[3], muVecR[3];
      for (int j=0; j<3; j++) {
        epsilonVecL[j] = epsilonL(i,j);
        epsilonVecR[j] = epsilonR(i,j);
        muVecL[j]      = muL(i,j);
        muVecR[j]      = muR(i,j);
      }
      double epsilonValL=0., muValL=0.;
      double epsilonValR=0., muValR=0.;
      for (int j=0; j<3; j++) {
        epsilonValL += pow(n[j],2)*epsilonVecL[j];
        epsilonValR += pow(n[j],2)*epsilonVecR[j];
        muValL      += pow(n[j],2)*muVecL[j];
        muValR      += pow(n[j],2)*muVecR[j];
      }
      double ZL = pow(muValL/epsilonValL, 0.5);
      double ZR = pow(muValR/epsilonValR, 0.5);
      double YL = 1/ZL;
      double YR = 1/ZR;
      double ZMean = (ZR+ZL)/2.;
      double YMean = (YR+YL)/2.;
      
      // Fields
      double eL[3], eR[3], eYMean[3], eJump[3], eRiemann[3];
      double hL[3], hR[3], hZMean[3], hJump[3], hRiemann[3];
      for (int j=0; j<3; j++) {
        eL[j] = solL(i,j);
        hL[j] = solL(i,3+j);
        eR[j] = solR(i,j);
        hR[j] = solR(i,3+j);
      }
      for (int j=0; j<3; j++) {
        eYMean[j] = (YR*eR[j]+YL*eL[j])/2.;
        hZMean[j] = (ZR*hR[j]+ZL*hL[j])/2.;
        eJump[j] = (eR[j]-eL[j])/2.;
        hJump[j] = (hR[j]-hL[j])/2.;
      }
      for (int j=0; j<3; j++) {
        eRiemann[j] = (eYMean[j] + crossProd(n,hJump,j)) / YMean;
        hRiemann[j] = (hZMean[j] - crossProd(n,eJump,j)) / ZMean;
      }
      
      // Fluxes
      for (int j=0; j<3; j++) {
        // Fluxes for the left
        val(i,j)   =  crossProd(n,hRiemann,j) / epsilonVecL[j];
        val(i,3+j) = -crossProd(n,eRiemann,j) / muVecL[j];
        // Fluxes for the right
        val(i,_nbf+j)   = -crossProd(n,hRiemann,j) / epsilonVecR[j];
        val(i,_nbf+3+j) =  crossProd(n,eRiemann,j) / muVecR[j];
      }
      
    }
  }
};

class dgConservationLawMaxwell::maxConvectiveSpeed : public function {
  fullMatrix<double> epsilon, mu;
 public:
  maxConvectiveSpeed(int nbf,
                     const function *epsilonFunction,
                     const function *muFunction) : function(nbf) {
    setArgument (epsilon, epsilonFunction);
    setArgument (mu, muFunction);
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++)
      val(i,0) = 1/pow(fmin(epsilon(i,0)*mu(i,0), fmin(epsilon(i,1)*mu(i,1), epsilon(i,2)*mu(i,2))), 0.5);
  }
};


/*==============================================================================
 * DG-terms : Pml
 *============================================================================*/

class dgConservationLawMaxwell::psiVolumeTermPml : public function {
  std::string _LayerVersion;
  fullMatrix<double> sol, pmlCoef, pmlDir;
 public:
  psiVolumeTermPml(int nbf, std::string LayerVersion,
                   const function *pmlCoefFunction,
                   const function *pmlDirFunction) : function(nbf) {
    _LayerVersion = LayerVersion;
    setArgument (sol, function::getSolution());
    setArgument (pmlCoef, pmlCoefFunction);
    setArgument (pmlDir, pmlDirFunction);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    if (_LayerVersion=="PmlStraight")
      callPmlCurv0(m, val);
    else if (_LayerVersion=="PmlStraightOld")
      callPmlCurv0_Old(m, val);
    else if (_LayerVersion=="PmlSphere")
      callPmlCurv1_Old(m, val);
  }
  void callPmlCurv0 (dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      double sigmaPml = pmlCoef(i,0);
      double kPml = pmlCoef(i,1);
      double t1[3] = {pmlDir(i,3), pmlDir(i,4), pmlDir(i,5)};
      double t2[3] = {pmlDir(i,6), pmlDir(i,7), pmlDir(i,8)};
      double e1 = sol(i,6), h1 = sol(i,8);
      double e2 = sol(i,7), h2 = sol(i,9);
      for (int j=0; j<3; j++) {
        val(i,j)   -= sigmaPml * (t1[j] * e1 + t2[j] * e2);
        val(i,3+j) -= sigmaPml * (t1[j] * h1 + t2[j] * h2);
      }
      for (int j=0; j<4; j++)
        val(i,6+j) -= (sigmaPml+kPml) * sol(i,6+j);
    }
  }
  void callPmlCurv0_Old (dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      double sigmaPml = pmlCoef(i,0);
      double kPml = pmlCoef(i,1);
      for (int j=0; j<6; j++) {
        val(i,j)   -= sigmaPml * sol(i,6+j);
        val(i,6+j) -= (sigmaPml+kPml) * sol(i,6+j);
      }
    }
  }
  void callPmlCurv1_Old (dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      double sigmaPml = pmlCoef(i,0);
      double sigmaPmlBar = pmlCoef(i,1);
      for (int j=0; j<6; j++) {
        val(i,j)   -= (sigmaPml-sigmaPmlBar) * sol(i,6+j) + sigmaPmlBar * sol(i,j);
        val(i,6+j) -= sigmaPml * sol(i,6+j);
      }
    }
  }
};

class dgConservationLawMaxwell::dPsiVolumeTermPml : public function {
  int _nbf;
  fullMatrix<double> sol, epsilon, mu, pmlDir;
  std::string _LayerVersion;
 public:
  dPsiVolumeTermPml(int nbf, std::string LayerVersion,
                    const function *epsilonFunction,
                    const function *muFunction,
                    const function *pmlDirFunction) : function(nbf*3) {
    _nbf = nbf;
    _LayerVersion = LayerVersion;
    setArgument (sol, function::getSolution());
    setArgument (epsilon, epsilonFunction);
    setArgument (mu, muFunction);
    setArgument (pmlDir, pmlDirFunction);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    if (_LayerVersion=="PmlStraight")
      callPml1AddField(m, val);
    else if ((_LayerVersion=="PmlStraightOld") || (_LayerVersion=="PmlSphere"))
      callPml1AddField_Old(m, val);
  }
  void callPml1AddField(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      // Physical parameters
      double epsilonVec[3], muVec[3];
      for (int j=0; j<3; j++) {
        epsilonVec[j] = epsilon(i,j);
        muVec[j]      = mu(i,j);
      }
      
      // Fields
      double e[3] = {sol(i,0), sol(i,1), sol(i,2)};
      double h[3] = {sol(i,3), sol(i,4), sol(i,5)};
      
      // term \partial_x
      val(i,0) = 0;
      val(i,1) = h[2]/epsilonVec[1];
      val(i,2) =-h[1]/epsilonVec[2];
      val(i,3) = 0;
      val(i,4) =-e[2]/muVec[1];
      val(i,5) = e[1]/muVec[2];
      
      // term \partial_y
      val(i,_nbf+0) =-h[2]/epsilonVec[0];
      val(i,_nbf+1) = 0;
      val(i,_nbf+2) = h[0]/epsilonVec[2];
      val(i,_nbf+3) = e[2]/muVec[0];
      val(i,_nbf+4) = 0;
      val(i,_nbf+5) =-e[0]/muVec[2];
      
      // term \partial_z
      val(i,2*_nbf+0) = h[1]/epsilonVec[0];
      val(i,2*_nbf+1) =-h[0]/epsilonVec[1];
      val(i,2*_nbf+2) = 0;
      val(i,2*_nbf+3) =-e[1]/muVec[0];
      val(i,2*_nbf+4) = e[0]/muVec[1];
      val(i,2*_nbf+5) = 0;
      
      // LAYER
      
      double a[3]  = {pmlDir(i,0), pmlDir(i,1), pmlDir(i,2)};
      double t1[3] = {pmlDir(i,3), pmlDir(i,4), pmlDir(i,5)};
      double t2[3] = {pmlDir(i,6), pmlDir(i,7), pmlDir(i,8)};
      
      double edott1 = scalProd(e,t1);
      double edott2 = scalProd(e,t2);
      double hdott1 = scalProd(h,t1);
      double hdott2 = scalProd(h,t2);
      
      for (int j=0; j<3; j++) {
        val(i,j*_nbf+6+0) = hdott2 * a[j] / epsilonVec[0];
        val(i,j*_nbf+6+1) =-hdott1 * a[j] / epsilonVec[0];
        val(i,j*_nbf+6+2) =-edott2 * a[j] / muVec[0];
        val(i,j*_nbf+6+3) = edott1 * a[j] / muVec[0];
      }
      
    }
  }
  void callPml1AddField_Old(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      // Physical parameters
      double epsilonVec[3];
      double muVec[3];
      for (int j=0; j<3; j++) {
        epsilonVec[j] = epsilon(i,j);
        muVec[j]      = mu(i,j);
      }
      
      // Fields
      double e[3] = {sol(i,0), sol(i,1), sol(i,2)};
      double h[3] = {sol(i,3), sol(i,4), sol(i,5)};
      
      // term \partial_x
      val(i,0) = 0;
      val(i,1) = h[2]/epsilonVec[1];
      val(i,2) =-h[1]/epsilonVec[2];
      val(i,3) = 0;
      val(i,4) =-e[2]/muVec[1];
      val(i,5) = e[1]/muVec[2];
      
      // term \partial_y
      val(i,_nbf+0) =-h[2]/epsilonVec[0];
      val(i,_nbf+1) = 0;
      val(i,_nbf+2) = h[0]/epsilonVec[2];
      val(i,_nbf+3) = e[2]/muVec[0];
      val(i,_nbf+4) = 0;
      val(i,_nbf+5) =-e[0]/muVec[2];
      
      // term \partial_z
      val(i,2*_nbf+0) = h[1]/epsilonVec[0];
      val(i,2*_nbf+1) =-h[0]/epsilonVec[1];
      val(i,2*_nbf+2) = 0;
      val(i,2*_nbf+3) =-e[1]/muVec[0];
      val(i,2*_nbf+4) = e[0]/muVec[1];
      val(i,2*_nbf+5) = 0;
      
      // LAYER
      
      double a[3] = {pmlDir(i,0), pmlDir(i,1), pmlDir(i,2)};
      double acrosse_mu[3];
      double acrossh_epsilon[3];
      for (int j=0; j<3; j++) {
        acrosse_mu[j]      = crossProd(a,e,j)/muVec[j];
        acrossh_epsilon[j] = crossProd(a,h,j)/epsilonVec[j];
      }
      
      for (int k=0; k<3; k++) {
        for (int j=0; j<3; j++) {
          val(i,k*_nbf+6+j)   =-acrossh_epsilon[j]*a[k];
          val(i,k*_nbf+6+j+3) = acrosse_mu[j]*a[k];
        }
      }
      
    }
  }
};

class dgConservationLawMaxwell::interfaceTermPml : public function {
  int _nbf;
  std::string _LayerVersion, _LayerFluxes;
  fullMatrix<double> normals;
  fullMatrix<double> solL, epsilonL, muL, pmlDirL;
  fullMatrix<double> solR, epsilonR, muR, pmlDirR;
 public:
  interfaceTermPml(int nbf, std::string LayerVersion, std::string LayerFluxes,
                   const function *epsilonFunction,
                   const function *muFunction,
                   const function *pmlDirFunction) : function (nbf*2) {
    _nbf = nbf;
    _LayerVersion = LayerVersion;
    _LayerFluxes = LayerFluxes;
    setArgument (normals, function::getNormals(), 0);
    setArgument (solL, function::getSolution(), 0);
    setArgument (solR, function::getSolution(), 1);
    setArgument (epsilonL, epsilonFunction,0);
    setArgument (epsilonR, epsilonFunction,1);
    setArgument (muL, muFunction,0);
    setArgument (muR, muFunction,1);
    setArgument (pmlDirL, pmlDirFunction, 0);
    setArgument (pmlDirR, pmlDirFunction, 1);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    if (_LayerVersion=="PmlStraight")
      callPml1AddField(m, val);
    else if ((_LayerVersion=="PmlStraightOld") || (_LayerVersion=="PmlSphere"))
      callPml1AddField_Old(m, val);
  }
  void callPml1AddField(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double n[3];
      for (int j=0; j<3; j++)
        n[j] = normals(i,j);
      
      // Physical parameters
      double epsilonVecL[3], muVecL[3];
      double epsilonVecR[3], muVecR[3];
      for (int j=0; j<3; j++) {
        epsilonVecL[j] = epsilonL(i,j);
        epsilonVecR[j] = epsilonR(i,j);
        muVecL[j]      = muL(i,j);
        muVecR[j]      = muR(i,j);
      }
      double epsilonValL=0., muValL=0.;
      double epsilonValR=0., muValR=0.;
      for (int j=0; j<3; j++) {
        epsilonValL += pow(n[j],2)*epsilonVecL[j];
        epsilonValR += pow(n[j],2)*epsilonVecR[j];
        muValL      += pow(n[j],2)*muVecL[j];
        muValR      += pow(n[j],2)*muVecR[j];
      }
      double ZL = pow(muValL/epsilonValL, 0.5);
      double ZR = pow(muValR/epsilonValR, 0.5);
      double YL = 1/ZL;
      double YR = 1/ZR;
      double ZMean = (ZR+ZL)/2.;
      double YMean = (YR+YL)/2.;
      
      // Fields
      double eL[3], eR[3], eYMeanR[3], eYMeanL[3], eMean[3], eJump[3], eRiemannR[3], eRiemannL[3];
      double hL[3], hR[3], hZMeanR[3], hZMeanL[3], hMean[3], hJump[3], hRiemannR[3], hRiemannL[3];
      for (int j=0; j<3; j++) {
        eL[j] = solL(i,j);
        hL[j] = solL(i,3+j);
        eR[j] = solR(i,j);
        hR[j] = solR(i,3+j);
      }
      for (int j=0; j<3; j++) {
        eYMeanR[j] = YR*(eR[j]+eL[j])/2.;
        eYMeanL[j] = YL*(eR[j]+eL[j])/2.;
        hZMeanR[j] = ZR*(hR[j]+hL[j])/2.;
        hZMeanL[j] = ZL*(hR[j]+hL[j])/2.;
        eMean[j] = (eR[j]+eL[j])/2.;
        hMean[j] = (hR[j]+hL[j])/2.;
        eJump[j] = (eR[j]-eL[j])/2.;
        hJump[j] = (hR[j]-hL[j])/2.;
      }
      for (int j=0; j<3; j++) {
        eRiemannR[j] = (eYMeanR[j] + crossProd(n,hJump,j)) / YMean;
        hRiemannR[j] = (hZMeanR[j] - crossProd(n,eJump,j)) / ZMean;
        eRiemannL[j] = (eYMeanL[j] + crossProd(n,hJump,j)) / YMean;
        hRiemannL[j] = (hZMeanL[j] - crossProd(n,eJump,j)) / ZMean;
      }
      
      // Fluxes (to the left, then to the right)
      for (int j=0; j<3; j++) {
        val(i,j)        =  crossProd(n,hRiemannL,j)/epsilonVecL[j];
        val(i,3+j)      = -crossProd(n,eRiemannL,j)/muVecL[j];
        val(i,_nbf+j)   = -crossProd(n,hRiemannR,j)/epsilonVecR[j];
        val(i,_nbf+3+j) =  crossProd(n,eRiemannR,j)/muVecR[j];
      }
      
      // LAYER
      
      double aL[3], t1L[3], t2L[3];
      double aR[3], t1R[3], t2R[3];
      for (int j=0; j<3; j++) {
        aL[j]  = pmlDirL(i,j);
        aR[j]  = pmlDirR(i,j);
        t1L[j] = pmlDirL(i,j+3);
        t1R[j] = pmlDirR(i,j+3);
        t2L[j] = pmlDirL(i,j+6);
        t2R[j] = pmlDirR(i,j+6);
      }
      double ndotaL = scalProd(n,aL);
      double ndotaR = scalProd(n,aR);
      if (_LayerFluxes=="Friedrich") {
        // FRIEDRICH
        double cL = 1/pow(muValL*epsilonValL, 0.5);
        double cR = 1/pow(muValR*epsilonValR, 0.5);
        val(i,6)      =-ndotaL*scalProd(hMean,t2L) / epsilonVecL[0] + cL*(solR(i,6)-solL(i,6))/2.;
        val(i,7)      = ndotaL*scalProd(hMean,t1L) / epsilonVecL[0] + cL*(solR(i,7)-solL(i,7))/2.;
        val(i,8)      = ndotaL*scalProd(eMean,t2L) / muVecL[0]      + cL*(solR(i,8)-solL(i,8))/2.;
        val(i,9)      =-ndotaL*scalProd(eMean,t1L) / muVecL[0]      + cL*(solR(i,9)-solL(i,9))/2.;
        val(i,_nbf+6) = ndotaR*scalProd(hMean,t2R) / epsilonVecR[0] - cR*(solR(i,6)-solL(i,6))/2.;
        val(i,_nbf+7) =-ndotaR*scalProd(hMean,t1R) / epsilonVecR[0] - cR*(solR(i,7)-solL(i,7))/2.;
        val(i,_nbf+8) =-ndotaR*scalProd(eMean,t2R) / muVecR[0]      - cR*(solR(i,8)-solL(i,8))/2.;
        val(i,_nbf+9) = ndotaR*scalProd(eMean,t1R) / muVecR[0]      - cR*(solR(i,9)-solL(i,9))/2.;
      } else if (_LayerFluxes=="Riemann") {
        // RIEMANN
        val(i,6)      =-ndotaL*scalProd(hRiemannL,t2L) / epsilonVecL[0];
        val(i,7)      = ndotaL*scalProd(hRiemannL,t1L) / epsilonVecL[0];
        val(i,8)      = ndotaL*scalProd(eRiemannL,t2L) / muVecL[0];
        val(i,9)      =-ndotaL*scalProd(eRiemannL,t1L) / muVecL[0];
        val(i,_nbf+6) = ndotaR*scalProd(hRiemannR,t2R) / epsilonVecR[0];
        val(i,_nbf+7) =-ndotaR*scalProd(hRiemannR,t1R) / epsilonVecR[0];
        val(i,_nbf+8) =-ndotaR*scalProd(eRiemannR,t2R) / muVecR[0];
        val(i,_nbf+9) = ndotaR*scalProd(eRiemannR,t1R) / muVecR[0];
      } else
        Msg::Fatal("Dg/Maxwell: bad LayerFluxes (not '%s')", _LayerFluxes.c_str());
      
    }
  }
  void callPml1AddField_Old(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double n[3];
      for (int j=0; j<3; j++)
        n[j] = normals(i,j);
      
      // Physical parameters
      double epsilonVecL[3], muVecL[3];
      double epsilonVecR[3], muVecR[3];
      for (int j=0; j<3; j++) {
        epsilonVecL[j] = epsilonL(i,j);
        epsilonVecR[j] = epsilonR(i,j);
        muVecL[j]      = muL(i,j);
        muVecR[j]      = muR(i,j);
      }
      double epsilonValL=0., muValL=0.;
      double epsilonValR=0., muValR=0.;
      for (int j=0; j<3; j++) {
        epsilonValL += pow(n[j],2)*epsilonVecL[j];
        epsilonValR += pow(n[j],2)*epsilonVecR[j];
        muValL      += pow(n[j],2)*muVecL[j];
        muValR      += pow(n[j],2)*muVecR[j];
      }
      double ZL = pow(muValL/epsilonValL, 0.5);
      double ZR = pow(muValR/epsilonValR, 0.5);
      double YL = 1/ZL;
      double YR = 1/ZR;
      double ZMean = (ZR+ZL)/2.;
      double YMean = (YR+YL)/2.;
      
      // Fields
      double eL[3], eR[3], eYMean[3], eJump[3], eRiemann[3];
      double hL[3], hR[3], hZMean[3], hJump[3], hRiemann[3];
      for (int j=0; j<3; j++) {
        eL[j] = solL(i,j);
        eR[j] = solR(i,j);
        hL[j] = solL(i,j+3);
        hR[j] = solR(i,j+3);
      }
      for (int j=0; j<3; j++) {
        eYMean[j] = (YR*eR[j]+YL*eL[j])/2.;
        hZMean[j] = (ZR*hR[j]+ZL*hL[j])/2.;
        eJump[j] = (eR[j]-eL[j])/2.;
        hJump[j] = (hR[j]-hL[j])/2.;
      }
      for (int j=0; j<3; j++) {
        eRiemann[j] = (eYMean[j] + crossProd(n,hJump,j)) / YMean;
        hRiemann[j] = (hZMean[j] - crossProd(n,eJump,j)) / ZMean;
      }
      
      // Fluxes
      
      for (int j=0; j<3; j++) {
        double temp1 = crossProd(n,hRiemann,j);
        double temp2 = crossProd(n,eRiemann,j);
        val(i,j)        =  temp1/epsilonVecL[j];
        val(i,3+j)      = -temp2/muVecL[j];
        val(i,_nbf+j)   = -temp1/epsilonVecR[j];
        val(i,_nbf+3+j) =  temp2/muVecR[j];
      }
      
      // LAYER
      
      double aL[3], aR[3];
      for (int j=0; j<3; j++) {
        aL[j]  = pmlDirL(i,j);
        aR[j]  = pmlDirR(i,j);
      }
      double ndotaL = scalProd(n,aL);
      double ndotaR = scalProd(n,aR);
      if (_LayerFluxes=="Friedrich") {
        // FRIEDRICH
        double cL = 1/pow(muValL*epsilonValL, 0.5);
        double qeJump[3];
        double qhJump[3];
        for (int j=0; j<3; j++) {
          qeJump[j] = (solR(i,6+j)-solL(i,6+j))/2.;
          qhJump[j] = (solR(i,9+j)-solL(i,9+j))/2.;
        }
        for (int j=0; j<3; j++) {
          val(i,6+j)      =  ndotaL*crossProd(aL,hRiemann,j)/epsilonVecL[j] + cL*qeJump[j];
          val(i,9+j)      = -ndotaL*crossProd(aL,eRiemann,j)/muVecL[j]      + cL*qhJump[j];
          val(i,_nbf+6+j) = -ndotaR*crossProd(aR,hRiemann,j)/epsilonVecR[j] - cL*qeJump[j];
          val(i,_nbf+9+j) =  ndotaR*crossProd(aR,eRiemann,j)/muVecR[j]      - cL*qhJump[j];
          /*
          val(i,6+j)      =  ndotaL*crossProd(aL,hMean,j)/epsilonVecL[j] + cL*(solR(i,6+j)-solL(i,6+j))/2.;
          val(i,9+j)      = -ndotaL*crossProd(aL,eMean,j)/muVecL[j]      + cL*(solR(i,9+j)-solL(i,9+j))/2.;
          val(i,_nbf+6+j) = -ndotaR*crossProd(aR,hMean,j)/epsilonVecR[j] - cL*(solR(i,6+j)-solL(i,6+j))/2.;
          val(i,_nbf+9+j) =  ndotaR*crossProd(aR,eMean,j)/muVecR[j]      - cL*(solR(i,9+j)-solL(i,9+j))/2.;
           */
        }
      } else if (_LayerFluxes=="Riemann") {
        // RIEMANN
        for (int j=0; j<3; j++) {
          val(i,6+j)      =  ndotaL*crossProd(aL,hRiemann,j)/epsilonVecL[j];
          val(i,9+j)      = -ndotaL*crossProd(aL,eRiemann,j)/muVecL[j];
          val(i,_nbf+6+j) = -ndotaR*crossProd(aR,hRiemann,j)/epsilonVecR[j];
          val(i,_nbf+9+j) =  ndotaR*crossProd(aR,eRiemann,j)/muVecR[j];
        }
      } else
        Msg::Fatal("Dg/Maxwell: bad LayerFluxes (not '%s')", _LayerFluxes.c_str());
      
    }
  }
};


/*==============================================================================
 * Conservation law : constructor - setup - destructor
 *============================================================================*/

int dgConservationLawMaxwell_nbf(std::string LayerVersion) {
  if (LayerVersion=="WithoutLayer")
    return 6;
  else if (LayerVersion=="PmlStraight")
    return 10;
  else if ((LayerVersion=="PmlStraightOld") || (LayerVersion=="PmlSphere"))
    return 12;
  else
    Msg::Fatal("Dg/Maxwell: bad LayerVersion (not '%s')", LayerVersion.c_str());
  return -1;
}

dgConservationLawMaxwell::dgConservationLawMaxwell(std::string LayerVersion) :
    dgConservationLawFunction(dgConservationLawMaxwell_nbf(LayerVersion))
{
  _LayerVersion = LayerVersion;
  addToTagList("");
  setSigma(0);
  setIncidentField(0);
  setSourceField(0);
  setPmlCoef(0);
  setPmlDir(0);
}

void dgConservationLawMaxwell::setup() {
  for(std::vector<std::string>::iterator it=_tagList.begin(); it!=_tagList.end(); ++it) {
    const std::string tag = *it;
    _volumeTerm0[tag]            = new psiVolumeTerm     (_nbf,getEpsilon(tag),getSigma(tag),getSourceField(tag));
    _volumeTerm1[tag]            = new dPsiVolumeTerm    (_nbf,getEpsilon(tag),getMu(tag));
    _interfaceTerm0[tag]         = new interfaceTerm     (_nbf,getEpsilon(tag),getMu(tag));
    _maximumConvectiveSpeed[tag] = new maxConvectiveSpeed(_nbf,getEpsilon(tag),getMu(tag));
  }
}

dgConservationLawMaxwell::~dgConservationLawMaxwell () {
  for(std::vector<std::string>::iterator it=_tagList.begin(); it!=_tagList.end(); ++it) {
    const std::string tag = *it;
    if (_volumeTerm0[tag])            delete _volumeTerm0[tag];
    if (_volumeTerm1[tag])            delete _volumeTerm1[tag];
    if (_maximumConvectiveSpeed[tag]) delete _maximumConvectiveSpeed[tag];
    if (_interfaceTerm0[tag])         delete _interfaceTerm0[tag];
  }
}


/*==============================================================================
 * Coefficients : set - get
 *============================================================================*/

int dgConservationLawMaxwell::addToTagList(const std::string tag) {
  if (isInTagList(tag))
    return 0; // 'tag' is already in 'tagList'
  else {
    _tagList.push_back(tag);
    return 1; // 'tag' is added in 'tagList'
  }
}

int dgConservationLawMaxwell::isInTagList(const std::string tag) {
  for(std::vector<std::string>::iterator it=_tagList.begin(); it!=_tagList.end(); ++it)
    if (*it == tag)
      return 1; // 'tag' is in 'tagList'
  return 0;     // 'tag' is not in 'tagList'
}

const function *dgConservationLawMaxwell::getFunctionForTag2(const termMap &map, const std::string tag) const {
  termMap::const_iterator it = map.find(tag);
  if (it == map.end())
    it = map.find("");
  if (it == map.end())
    Msg::Fatal("Dg/Maxwell: a parameter is not defined.");
  return it->second;
}


/*==============================================================================
 * Add a PML
 *============================================================================*/

void dgConservationLawMaxwell::addPml(const std::string tag, std::string LayerFluxes) {
  if ((_LayerVersion=="PmlStraight") ||
      (_LayerVersion=="PmlStraightOld") ||
      (_LayerVersion=="PmlSphere")) {
    delete _volumeTerm0[tag];
    delete _volumeTerm1[tag];
    delete _interfaceTerm0[tag];
    _volumeTerm0[tag]    = new psiVolumeTermPml (_nbf,_LayerVersion,getPmlCoef(tag),getPmlDir(tag));
    _volumeTerm1[tag]    = new dPsiVolumeTermPml(_nbf,_LayerVersion,getEpsilon(tag),getMu(tag),getPmlDir(tag));
    _interfaceTerm0[tag] = new interfaceTermPml (_nbf,_LayerVersion,LayerFluxes,getEpsilon(tag),getMu(tag),getPmlDir(tag));
  } else
    Msg::Fatal("Dg/Maxwell: bad LayerVersion (not '%s')", _LayerVersion.c_str());
}

void dgConservationLawMaxwell::addInterfacePml(const std::string tag) {
  if ((_LayerVersion=="PmlStraight") ||
      (_LayerVersion=="PmlStraightOld") ||
      (_LayerVersion=="PmlSphere")) {
    delete _interfaceTerm0[tag];
    _interfaceTerm0[tag] = new interfaceTermPml (_nbf,_LayerVersion,"Riemann",getEpsilon(tag),getMu(tag),getPmlDir(tag));
  } else
    Msg::Fatal("Dg/Maxwell: bad LayerVersion (not '%s')", _LayerVersion.c_str());
}


/*==============================================================================
 * Specific boundary conditions
 *============================================================================*/

class dgBoundaryConditionMaxwell : public dgBoundaryCondition {
  class term : public function {
    int _typeBC;
    std::string _LayerVersion;
    fullMatrix<double> normals, sol, epsilon, mu, pmlDir, extData;    
   public:
    term(int nbf, int typeBC, std::string LayerVersion,
         const function *epsilonFunction,
         const function *muFunction,
         const function *pmlDirFunction,
         const function *extDataFunction) : function(nbf) {
      
      _typeBC = typeBC;
      _LayerVersion = LayerVersion;
      
      setArgument (sol, function::getSolution());
      setArgument (normals, function::getNormals());
      setArgument (epsilon, epsilonFunction);
      setArgument (mu, muFunction);
      
      if (_LayerVersion != "WithoutLayer")
        setArgument (pmlDir, pmlDirFunction);
      
      if (extDataFunction != NULL)
        setArgument (extData, extDataFunction);
      
    }
    void call (dataCacheMap *m, fullMatrix<double> &val) { 
      val.scale(0.);
      size_t nQP = val.size1();
      if (_typeBC) // _typeBC=0 : Symmetry condition (all fluxes are avoided)
      for(size_t i=0; i< nQP; i++) {
        
        double n[3];
        for (int j=0; j<3; j++)
          n[j] = normals(i,j);
        
        // Physical parameters
        
        double epsilonVec[3], muVec[3];
        for (int j=0; j<3; j++) {
          epsilonVec[j] = epsilon(i,j);
          muVec[j]      = mu(i,j);
        }
        
        double epsilonVal=0., muVal=0.;
        for (int j=0; j<3; j++) {
          epsilonVal += pow(n[j],2)*epsilonVec[j];
          muVal      += pow(n[j],2)*muVec[j];
        }
        
        double Z = pow(muVal/epsilonVal,0.5);
        double Y = 1/Z;
        
        // Fields
        
        double e[3], eMean[3] = {0.,0.,0.}, eJump[3] = {0.,0.,0.}, ncrosseJump[3], eRiemann[3];
        double h[3], hMean[3] = {0.,0.,0.}, hJump[3] = {0.,0.,0.}, ncrosshJump[3], hRiemann[3];
        
        for (int j=0; j<3; j++) {
          e[j] = sol(i,j);
          h[j] = sol(i,3+j);
        }
        
        switch (_typeBC) {
          
          // Dirichlet on 'e'
          case 1:
            for (int j=0; j<3; j++) {
              eMean[j] = (extData(i,j)+e[j])/2.;
              hMean[j] = h[j];
              eJump[j] = (extData(i,j)-e[j])/2.;
              hJump[j] = 0.;
            }
            break;
          
          // Homogeneous Dirichlet on 'e' (Electric wall)
          case 2:
            for (int j=0; j<3; j++) {
              eMean[j] = e[j]/2.;
              hMean[j] = h[j];
              eJump[j] = -e[j]/2.;
              hJump[j] = 0.;
            }
            break;
          
          // Dirichlet on 'h'
          case 3:
            for (int j=0; j<3; j++) {
              eMean[j] = e[j];
              hMean[j] = (extData(i,j)+h[j])/2.;
              eJump[j] = 0.;
              hJump[j] = (extData(i,j)-h[j])/2.;
            }
            break;
          
          // Homogeneous Dirichlet on 'h' (Magnetic wall)
          case 4:
            for (int j=0; j<3; j++) {
              eMean[j] = e[j];
              hMean[j] = h[j]/2.;
              eJump[j] = 0.;
              hJump[j] = -h[j]/2.;
            }
            break;
          
          // Silver-Muller
          case 7:
            for (int j=0; j<3; j++) {
              eMean[j] = 0.;
              hMean[j] = 0.;
              eJump[j] = -e[j];
              hJump[j] = -h[j];
            }
            break;
            
          // Silver-Muller with External Data
          case 8:
            for (int j=0; j<3; j++) {
              eMean[j] = (extData(i,j)  +e[j])/2.;
              hMean[j] = (extData(i,j+3)+h[j])/2.;
              eJump[j] = (extData(i,j)  -e[j])/2.;
              hJump[j] = (extData(i,j+3)-h[j])/2.;
            }
            break;
            
          // Symmetry
          case 9:
            for (int j=0; j<3; j++) {
              eMean[j] = e[j];
              hMean[j] = h[j];
              eJump[j] = 0.;
              hJump[j] = 0.;
            }
            break;
            
          default:
            Msg::Error("Dg/Maxwell: Bad boundary condition in conservation law (BAD '_typeBC': '%i')", _typeBC);
          
        }
        
        for (int j=0; j<3; j++) {
          ncrosseJump[j] = crossProd(n,eJump,j);
          ncrosshJump[j] = crossProd(n,hJump,j);
        }
        
        for (int j=0; j<3; j++) {
          hRiemann[j] = hMean[j] - ncrosseJump[j]/Z;
          eRiemann[j] = eMean[j] + ncrosshJump[j]/Y;
        }
        
        for (int j=0; j<3; j++) {
          val(i,j)   =  crossProd(n,hRiemann,j)/epsilonVec[j];
          val(i,3+j) = -crossProd(n,eRiemann,j)/muVec[j];
        }
        
        // LAYER
        
        if (_LayerVersion == "PmlStraight") {
          double a[3]  = {pmlDir(i,0), pmlDir(i,1), pmlDir(i,2)};
          double t1[3] = {pmlDir(i,3), pmlDir(i,4), pmlDir(i,5)};
          double t2[3] = {pmlDir(i,6), pmlDir(i,7), pmlDir(i,8)};
          double ndota = scalProd(n,a);
          val(i,6) = -ndota*scalProd(hRiemann,t2)/epsilonVec[0];
          val(i,7) =  ndota*scalProd(hRiemann,t1)/epsilonVec[0];
          val(i,8) =  ndota*scalProd(eRiemann,t2)/muVec[0];
          val(i,9) = -ndota*scalProd(eRiemann,t1)/muVec[0];
        }
        else if (_LayerVersion == "PmlStraightOld") {
          double a[3] = {pmlDir(i,0), pmlDir(i,1), pmlDir(i,2)};
          double ndota = scalProd(n,a);
          for (int j=0; j<3; j++) {
            val(i,6+j)   =  ndota*crossProd(a,hRiemann,j)/epsilonVec[j];
            val(i,6+3+j) = -ndota*crossProd(a,eRiemann,j)/muVec[j];
          }
        }
        
      }
    }
  };
  term _term;
 public:
  dgBoundaryConditionMaxwell(dgConservationLawMaxwell *claw, const std::string tag, int typeBC, const function *extDataFunction) :
      _term(claw->getNbFields(),
            typeBC,
            claw->getLayerVersion(),
            claw->getEpsilon(tag),
            claw->getMu(tag),
            claw->getPmlDir(tag),
            extDataFunction) {
    _term0 = &_term;
  }
};

dgBoundaryCondition *dgConservationLawMaxwell::newBoundaryDirichletOnE(const std::string tag, const function *eExtFunction) {
  return new dgBoundaryConditionMaxwell(this,tag,1,eExtFunction);
}

dgBoundaryCondition *dgConservationLawMaxwell::newBoundaryHomogeneousDirichletOnE(const std::string tag) {
  return new dgBoundaryConditionMaxwell(this,tag,2,NULL);
}

dgBoundaryCondition *dgConservationLawMaxwell::newBoundaryElectricWall(const std::string tag) {
  return new dgBoundaryConditionMaxwell(this,tag,2,NULL);
}

dgBoundaryCondition *dgConservationLawMaxwell::newBoundaryDirichletOnH(const std::string tag, const function *hExtFunction) {
  return new dgBoundaryConditionMaxwell(this,tag,3,hExtFunction);
}

dgBoundaryCondition *dgConservationLawMaxwell::newBoundaryHomogeneousDirichletOnH(const std::string tag) {
  return new dgBoundaryConditionMaxwell(this,tag,4,NULL);
}

dgBoundaryCondition *dgConservationLawMaxwell::newBoundaryMagneticWall(const std::string tag) {
  return new dgBoundaryConditionMaxwell(this,tag,4,NULL);
}

dgBoundaryCondition *dgConservationLawMaxwell::newBoundarySilverMuller(const std::string tag) {
  return new dgBoundaryConditionMaxwell(this,tag,7,NULL);
}

dgBoundaryCondition *dgConservationLawMaxwell::newBoundarySilverMullerDirichlet(const std::string tag, const function *eExtFunction) {
  return new dgBoundaryConditionMaxwell(this,tag,8,eExtFunction);
}

dgBoundaryCondition *dgConservationLawMaxwell::newBoundarySymmetry(const std::string tag) {
  return new dgBoundaryConditionMaxwell(this,tag,9,NULL);
}


/*==============================================================================
 * Interface condition
 *============================================================================*/

class dgConservationLawMaxwell::interfaceTermPmlWithIncident : public function {
  int _nbf, _incidentFieldsDefined;
  std::string _LayerVersion;
  fullMatrix<double> normals;
  fullMatrix<double> solL, epsilonL, muL, pmlDirL, incidentFieldsL;
  fullMatrix<double> solR, epsilonR, muR, pmlDirR, incidentFieldsR;
 public:
   interfaceTermPmlWithIncident(int nbf, std::string LayerVersion,
                                const function *epsilonFunction,
                                const function *muFunction,
                                const function *pmlDirFunction,
                                const function *incidentFieldsFunction) : function (nbf*2) {
    _nbf = nbf;
    _LayerVersion = LayerVersion;
    setArgument (normals, function::getNormals(), 0);
    setArgument (solL, function::getSolution(), 0);
    setArgument (solR, function::getSolution(), 1);
    setArgument (epsilonL, epsilonFunction, 0);
    setArgument (epsilonR, epsilonFunction, 1);
    setArgument (muL, muFunction, 0);
    setArgument (muR, muFunction, 1);
    setArgument (pmlDirL, pmlDirFunction, 0);
    setArgument (pmlDirR, pmlDirFunction, 1);
    
    _incidentFieldsDefined = 0;
    if (incidentFieldsFunction!=NULL) {
      setArgument (incidentFieldsL, incidentFieldsFunction, 1);
      setArgument (incidentFieldsR, incidentFieldsFunction, 0);
      _incidentFieldsDefined = 1;
    }
    
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {

      double n[3];
      for (int j=0; j<3; j++)
        n[j] = normals(i,j);
      
      // Pml-Directions
      double aL[3], t1L[3], t2L[3];
      double aR[3], t1R[3], t2R[3];
      for (int j=0; j<3; j++) {
        aL[j]  = pmlDirL(i,j);
        aR[j]  = pmlDirR(i,j);
        t1L[j] = pmlDirL(i,j+3);
        t1R[j] = pmlDirR(i,j+3);
        t2L[j] = pmlDirL(i,j+6);
        t2R[j] = pmlDirR(i,j+6);
      }
      double ndotaL = scalProd(n,aL);
      double ndotaR = scalProd(n,aR);
      
      // Physical parameters
      double epsilonVecL[3], muVecL[3];
      double epsilonVecR[3], muVecR[3];
      for (int j=0; j<3; j++) {
        epsilonVecL[j] = epsilonL(i,j);
        epsilonVecR[j] = epsilonR(i,j);
        muVecL[j]      = muL(i,j);
        muVecR[j]      = muR(i,j);
      }
      double epsilonValL=0., muValL=0.;
      double epsilonValR=0., muValR=0.;
      for (int j=0; j<3; j++) {
        epsilonValL += pow(n[j],2)*epsilonVecL[j];
        epsilonValR += pow(n[j],2)*epsilonVecR[j];
        muValL      += pow(n[j],2)*muVecL[j];
        muValR      += pow(n[j],2)*muVecR[j];
      }
      double ZL = pow(muValL/epsilonValL,0.5);
      double ZR = pow(muValR/epsilonValR,0.5);
      double YL = 1/ZL;
      double YR = 1/ZR;
      double ZMean = (ZR+ZL)/2.;
      double YMean = (YR+YL)/2.;
      
      // Fields
      double eL[3], eR[3];
      double hL[3], hR[3];
      for (int j=0; j<3; j++) {
        eL[j] = solL(i,j);
        eR[j] = solR(i,j);
        hL[j] = solL(i,j+3);
        hR[j] = solR(i,j+3);
      }
      double eIncL[3] = {0.,0.,0.}, eIncR[3] = {0.,0.,0.};
      double hIncL[3] = {0.,0.,0.}, hIncR[3] = {0.,0.,0.};
      if (_incidentFieldsDefined)
        for (int j=0; j<3; j++) {
          eIncL[j] = incidentFieldsL(i,j);
          eIncR[j] = incidentFieldsR(i,j);
          hIncL[j] = incidentFieldsL(i,j+3);
          hIncR[j] = incidentFieldsR(i,j+3);
        }
      
      // Data at the interface  ---   AM: Qque chose de pas clair
      double a[3], eInc[3], hInc[3];
      if (norm3(aL)>0.5) {
        for (int j=0; j<3; j++) {
          a[j] = aL[j];
          eInc[j] = eIncL[j];
          hInc[j] = hIncL[j];
        }
      }
      else {
        for (int j=0; j<3; j++) {
          a[j] = aR[j];
          eInc[j] = eIncR[j];
          hInc[j] = hIncR[j];
        }
      }
      
      // Riemann Solver
      double eYMean[3], eJump[3], eRiemannL[3], eRiemannR[3];
      double hZMean[3], hJump[3], hRiemannL[3], hRiemannR[3];
      
      if (scalProd(n,a)>0.5) {  // (L=domain, R=pml)
        
        // With total fields
        for (int j=0; j<3; j++) {
          eYMean[j] = (YR*(eR[j]+eInc[j]) + YL*eL[j])/2.;
          hZMean[j] = (ZR*(hR[j]+hInc[j]) + ZL*hL[j])/2.;
          eJump[j] = ((eR[j]+eInc[j]) - eL[j])/2.;
          hJump[j] = ((hR[j]+hInc[j]) - hL[j])/2.;
        }
        for (int j=0; j<3; j++) {
          eRiemannL[j] = (eYMean[j] + crossProd(n,hJump,j)) / YMean;
          hRiemannL[j] = (hZMean[j] - crossProd(n,eJump,j)) / ZMean;
        }
        
        // With scattered fields
        for (int j=0; j<3; j++) {
          eYMean[j] = (YR*eR[j] + YL*(eL[j]-eInc[j]))/2.;
          hZMean[j] = (ZR*hR[j] + ZL*(hL[j]-hInc[j]))/2.;
          eJump[j] = (eR[j] - (eL[j]-eInc[j]))/2.;
          hJump[j] = (hR[j] - (hL[j]-hInc[j]))/2.;
        }
        for (int j=0; j<3; j++) {
          eRiemannR[j] = (eYMean[j] + crossProd(n,hJump,j)) / YMean;
          hRiemannR[j] = (hZMean[j] - crossProd(n,eJump,j)) / ZMean;
        }
        
      }
      else {  // (L=pml, R=domain)
        
        // With scattered fields
        for (int j=0; j<3; j++) {
          eYMean[j] = (YR*(eR[j]-eInc[j]) + YL*eL[j])/2.;
          hZMean[j] = (ZR*(hR[j]-hInc[j]) + ZL*hL[j])/2.;
          eJump[j] = ((eR[j]-eInc[j]) - eL[j])/2.;
          hJump[j] = ((hR[j]-hInc[j]) - hL[j])/2.;
        }
        for (int j=0; j<3; j++) {
          eRiemannL[j] = (eYMean[j] + crossProd(n,hJump,j)) / YMean;
          hRiemannL[j] = (hZMean[j] - crossProd(n,eJump,j)) / ZMean;
        }
        
        // With total fields
        for (int j=0; j<3; j++) {
          eYMean[j] = (YR*eR[j] + (YL*(eL[j]+eInc[j])))/2.;
          hZMean[j] = (ZR*hR[j] + (ZL*(hL[j]+hInc[j])))/2.;
          eJump[j] = (eR[j] - (eL[j]+eInc[j]))/2.;
          hJump[j] = (hR[j] - (hL[j]+hInc[j]))/2.;
        }
        for (int j=0; j<3; j++) {
          eRiemannR[j] = (eYMean[j] + crossProd(n,hJump,j)) / YMean;
          hRiemannR[j] = (hZMean[j] - crossProd(n,eJump,j)) / ZMean;
        }
        
      }
      
      // Fluxes
      for (int j=0; j<3; j++) {
        // Fluxes for the left
        val(i,j)   =  crossProd(n,hRiemannL,j)/epsilonVecL[j];
        val(i,3+j) = -crossProd(n,eRiemannL,j)/muVecL[j];
        // Fluxes for the right
        val(i,_nbf+j)   = -crossProd(n,hRiemannR,j)/epsilonVecR[j];
        val(i,_nbf+3+j) =  crossProd(n,eRiemannR,j)/muVecR[j];
      }
      
      if (_LayerVersion=="PmlStraight") {
        double aLcrosshL_epsilonL[3];
        double aRcrosshR_epsilonR[3];
        double aLcrosseL_muL[3];
        double aRcrosseR_muR[3];
        for (int j=0; j<3; j++) {
          aLcrosshL_epsilonL[j] = crossProd(aL,hRiemannL,j)/epsilonVecL[j];
          aRcrosshR_epsilonR[j] = crossProd(aR,hRiemannR,j)/epsilonVecR[j];
          aLcrosseL_muL[j]      = crossProd(aL,eRiemannL,j)/muVecL[j];
          aRcrosseR_muR[j]      = crossProd(aR,eRiemannR,j)/muVecR[j];
        }
        val(i,6) = ndotaL*scalProd(t1L,aLcrosshL_epsilonL);
        val(i,7) = ndotaL*scalProd(t2L,aLcrosshL_epsilonL);
        val(i,8) =-ndotaL*scalProd(t1L,aLcrosseL_muL);
        val(i,9) =-ndotaL*scalProd(t2L,aLcrosseL_muL);
        val(i,_nbf+6) =-ndotaR*scalProd(t1R,aRcrosshR_epsilonR);
        val(i,_nbf+7) =-ndotaR*scalProd(t2R,aRcrosshR_epsilonR);
        val(i,_nbf+8) = ndotaR*scalProd(t1R,aRcrosseR_muR);
        val(i,_nbf+9) = ndotaR*scalProd(t2R,aRcrosseR_muR);
      }
      else if ((_LayerVersion=="PmlStraightOld") || (_LayerVersion=="PmlSphere"))
        for (int j=0; j<3; j++) {
          // From left (in cell left)
          val(i,6+j) =  ndotaL*crossProd(aL,hRiemannL,j)/epsilonVecL[j];
          val(i,9+j) = -ndotaL*crossProd(aL,eRiemannL,j)/muVecL[j];
          // From right (in cell right)
          val(i,_nbf+6+j) = -ndotaR*crossProd(aR,hRiemannR,j)/epsilonVecR[j];
          val(i,_nbf+9+j) =  ndotaR*crossProd(aR,eRiemannR,j)/muVecR[j];
        }
      
    }
  }
};

void dgConservationLawMaxwell::addInterfacePmlWithIncident(const std::string tag, const function *incidentFieldsFunction) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  _interfaceTerm0[tag] = new interfaceTermPmlWithIncident(getNbFields(),getLayerVersion(),getEpsilon(tag),getMu(tag),getPmlDir(tag),incidentFieldsFunction);
}

class dgConservationLawMaxwell::interfaceTermUncoupledRegions : public function {
  int _nbf,_typeBC;
  fullMatrix<double> normals;
  fullMatrix<double> sol_L, epsilonL, muL, extDataL;
  fullMatrix<double> sol_R, epsilonR, muR, extDataR;
 public:
   interfaceTermUncoupledRegions(int nbf,
                                 const function *epsilonFunction,
                                 const function *muFunction,
                                 int typeBC,
                                 const function *extDataFunction) : function (nbf*2) {
    _nbf = nbf;
    _typeBC=typeBC;
    setArgument (normals, function::getNormals(), 0);
    setArgument (sol_L, function::getSolution(), 0);
    setArgument (sol_R, function::getSolution(), 1);
    setArgument (epsilonL, epsilonFunction, 0);
    setArgument (epsilonR, epsilonFunction, 1);
    setArgument (muL, muFunction, 0);
    setArgument (muR, muFunction, 1);
    if (extDataFunction != NULL) {
      setArgument (extDataL, extDataFunction, 1);
      setArgument (extDataR, extDataFunction, 0);
    }
    
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      double n[3] = {normals(i,0), normals(i,1), normals(i,2)};
      
      // Physical parameters
      double epsilonVecL[3], muVecL[3];
      double epsilonVecR[3], muVecR[3];
      for (int j=0; j<3; j++) {
        epsilonVecL[j] = epsilonL(i,j); muVecL[j] = muL(i,j);
        epsilonVecR[j] = epsilonR(i,j); muVecR[j] = muR(i,j);
      }
      double epsilonValL=0., muValL=0.;
      double epsilonValR=0., muValR=0.;
      for (int j=0; j<3; j++) {
        epsilonValL += pow(n[j],2)*epsilonVecL[j];
        epsilonValR += pow(n[j],2)*epsilonVecR[j];
        muValL      += pow(n[j],2)*muVecL[j];
        muValR      += pow(n[j],2)*muVecR[j];
      }
      double ZL = pow(muValL/epsilonValL,0.5);
      double ZR = pow(muValR/epsilonValR,0.5);
      double YL = 1/ZL;
      double YR = 1/ZR;
      
      // Fields
      double eL[3], eR[3], hL[3], hR[3];
      for (int j=0; j<3; j++){
        eL[j] = sol_L(i,j);
        eR[j] = sol_R(i,j);
        hL[j] = sol_L(i,j+3);
        hR[j] = sol_R(i,j+3);
      }

      double eMeanL[3]={0,0,0}, eMeanR[3]={0,0,0};
      double hMeanL[3]={0,0,0}, hMeanR[3]={0,0,0};
      double eJumpL[3]={0,0,0}, eJumpR[3]={0,0,0};
      double hJumpL[3]={0,0,0}, hJumpR[3]={0,0,0};
      double ncrosseJumpL[3]={0,0,0}, ncrosshJumpL[3]={0,0,0};
      double ncrosseJumpR[3]={0,0,0}, ncrosshJumpR[3]={0,0,0};
      double hRiemannL[3],eRiemannL[3], hRiemannR[3], eRiemannR[3];
      
      // Riemann Solver
      switch (_typeBC) {
          
          // Homogeneous Dirichlet on 'e' (Electric wall)
          case 1:
            for (int j=0; j<3; j++){
              eMeanR[j] = eR[j]/2.;
              eMeanL[j] = eL[j]/2.;
              eJumpR[j] = eR[j]/2.;
              eJumpL[j] = -eL[j]/2.;
              hMeanR[j] = hR[j];
              hMeanL[j] = hL[j];
              hJumpR[j] = 0.;
              hJumpL[j] = 0.;
            }
            break;
          
          // Homogeneous Dirichlet on 'h' (Magnetic wall)
          case 2:
            for (int j=0; j<3; j++){
              eMeanR[j] = eR[j];
              eJumpR[j] = 0.;
              hMeanR[j] = hR[j]/2.;
              hJumpR[j] = hR[j]/2.;
              
              eMeanL[j] = eL[j];
              eJumpL[j] = 0.;
              hMeanL[j] = hL[j]/2.;
              hJumpL[j] = -hL[j]/2.;
            }
            break;
          
          case 3:
            for (int j=0; j<3; j++){
              eMeanR[j] = eR[j]/2.;
              eMeanL[j] = eL[j]/2.;
              eJumpR[j] = eR[j]/2.;
              eJumpL[j] = -eL[j]/2.;
              hMeanR[j] = hR[j]/2.;
              hMeanL[j] = hL[j]/2.;
              hJumpR[j] = hR[j]/2.;
              hJumpL[j] = -hL[j]/2.;
	          }
            /*for (int j=0; j<3; j++){
              eMeanR[j] = eR[j];
              eMeanL[j] = eL[j];
              eJumpR[j] = 0.;
              eJumpL[j] = 0.;
              hMeanR[j] = hR[j];
              hMeanL[j] = hL[j];
              hJumpR[j] = 0;
              hJumpL[j] = 0;
	          }*/
            break;
          
          case 8:
            for (int j=0; j<3; j++) {
              eMeanR[j] = (extDataL(i,j)+eR[j])/2.;
              eMeanL[j] = (extDataR(i,j)+eL[j])/2.;
              hMeanR[j] = (extDataL(i,j+3)+hR[j])/2.;
              hMeanL[j] = (extDataR(i,j+3)+hL[j])/2.;
              eJumpR[j] = (-extDataL(i,j)+eR[j])/2.;
              eJumpL[j] = (extDataR(i,j)-eL[j])/2.;
              hJumpR[j] = (-extDataL(i,j+3)+hR[j])/2.;
              hJumpL[j] = (extDataR(i,j+3)-hL[j])/2.;
            }
            break;
          
          default:
            Msg::Error("Dg/Maxwell: Bad boundary condition in conservation law (BAD '_typeBC': '%i')", _typeBC);
        }
            for (int j=0; j<3; j++) {
          ncrosseJumpL[j] = crossProd(n,eJumpL,j);
          ncrosshJumpL[j] = crossProd(n,hJumpL,j);
          ncrosseJumpR[j] = crossProd(n,eJumpR,j);
          ncrosshJumpR[j] = crossProd(n,hJumpR,j);
        }
        
        for (int j=0; j<3; j++) {
          hRiemannL[j] = hMeanL[j] - ncrosseJumpL[j]/ZL;
          eRiemannL[j] = eMeanL[j] + ncrosshJumpL[j]/YL;
          hRiemannR[j] = hMeanR[j] - ncrosseJumpR[j]/ZR;
          eRiemannR[j] = eMeanR[j] + ncrosshJumpR[j]/YR;
        }
        
      for (int j=0; j<3; j++) {
        // Fluxes for the left
        val(i,j)   =  crossProd(n,hRiemannL,j)/epsilonVecL[j];
        val(i,3+j) = -crossProd(n,eRiemannL,j)/muVecL[j];
        // Fluxes for the right
        val(i,_nbf+j)   = -crossProd(n,hRiemannR,j)/epsilonVecR[j];
        val(i,_nbf+3+j) =  crossProd(n,eRiemannR,j)/muVecR[j];
      }
   }
  }
};

void dgConservationLawMaxwell::addInterfaceEwall(const std::string tag) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  _interfaceTerm0[tag] = new interfaceTermUncoupledRegions(_nbf,getEpsilon(tag),getMu(tag),1,NULL);
}

void dgConservationLawMaxwell::addInterfaceHwall(const std::string tag) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  _interfaceTerm0[tag] = new interfaceTermUncoupledRegions(_nbf,getEpsilon(tag),getMu(tag),2,NULL);
}

void dgConservationLawMaxwell::addInterfaceSM(const std::string tag) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  _interfaceTerm0[tag] = new interfaceTermUncoupledRegions(_nbf,getEpsilon(tag),getMu(tag),3,NULL);
}

void dgConservationLawMaxwell::addInterfaceSMD(const std::string tag, const function *eExtFunction) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  _interfaceTerm0[tag] = new interfaceTermUncoupledRegions(_nbf,getEpsilon(tag),getMu(tag),8,eExtFunction);
}

class dgConservationLawMaxwell::interfaceSheetTerm : public function {
  int _nbf;
  double _Ys;
  fullMatrix<double> normals;
  fullMatrix<double> solL, epsilonL, muL;
  fullMatrix<double> solR, epsilonR, muR;
 public:
   interfaceSheetTerm(int nbf,double Ys,
                 const function *epsilonFunction,
                 const function *muFunction) : function (nbf*2) {
    _nbf = nbf;
    _Ys = Ys;
    setArgument (normals, function::getNormals(), 0);
    setArgument (solL, function::getSolution(), 0);
    setArgument (solR, function::getSolution(), 1);
    setArgument (epsilonL, epsilonFunction, 0);
    setArgument (epsilonR, epsilonFunction, 1);
    setArgument (muL, muFunction, 0);
    setArgument (muR, muFunction, 1);
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double n[3];
      for (int j=0; j<3; j++)
        n[j] = normals(i,j);
      
      // Physical parameters
      double epsilonVecL[3], muVecL[3];
      double epsilonVecR[3], muVecR[3];
      for (int j=0; j<3; j++) {
        epsilonVecL[j] = epsilonL(i,j);
        epsilonVecR[j] = epsilonR(i,j);
        muVecL[j]      = muL(i,j);
        muVecR[j]      = muR(i,j);
      }
      double epsilonValL=0., muValL=0.;
      double epsilonValR=0., muValR=0.;
      for (int j=0; j<3; j++) {
        epsilonValL += pow(n[j],2)*epsilonVecL[j];
        epsilonValR += pow(n[j],2)*epsilonVecR[j];
        muValL      += pow(n[j],2)*muVecL[j];
        muValR      += pow(n[j],2)*muVecR[j];
      }
      /*double ZL = pow(muValL/epsilonValL, 0.5);
      double ZR = pow(muValR/epsilonValR, 0.5);
      double YL = 1/ZL;
      double YR = 1/ZR;
      double YMean = (YR+YL);
      double ZMean = (ZR+ZL);*/

      // Fields
      //double eL[3], eR[3];
      double eRiemannR[3], eRiemannL[3];
      double hL[3], hR[3], hRiemannR[3], hRiemannL[3];
      double hJump[3];
      //double eLR[3],eRL[3];
      //double hLR[3],hRL[3];
      for (int j=0; j<3; j++) {
        //eL[j] = solL(i,j);
        hL[j] = solL(i,3+j);
        //eR[j] = solR(i,j);
        hR[j] = solR(i,3+j);
      }


      for (int j=0; j<3; j++) {
       // eYMean[j] = (YR*eR[j]+YL*eL[j])/2.;
        hJump[j] = (hR[j]-hL[j]);
      }

      for (int j=0; j<3; j++) {
        eRiemannR[j] = crossProd(n,hJump,j) / _Ys;
        eRiemannL[j] = crossProd(n,hJump,j) / _Ys;
        hRiemannR[j] = hR[j];
        hRiemannL[j] = hL[j];
      }
	/*        
      for (int j=0; j<3; j++) {
        eLR[j] = eR[j];
        eRL[j] = eL[j];
      }
    for (int j=0; j<3; j++) {

        hLR[j] = _Ys*crossProd(n,eLR,j)+hR[j];
        hRL[j] = _Ys*crossProd(n,eRL,j)+hL[j];
        
      }



      for (int j=0; j<3; j++) {
        eRiemannR[j] = YR*(eR[j]+eL[j])/YMean ;
        eRiemannL[j] = YL*(eR[j]+eL[j])/YMean ;
        hRiemannR[j] = hRL[j];
        hRiemannL[j] = hLR[j];
      }*/
      
      // Fluxes
      
      for (int j=0; j<3; j++) {
        // Fluxes for the left
        val(i,j)   =  crossProd(n,hRiemannL,j) / epsilonVecL[j];
        val(i,3+j) =  -crossProd(n,eRiemannL,j) / muVecL[j];
        // Fluxes for the right
        val(i,_nbf+j)   =  -crossProd(n,hRiemannR,j) / epsilonVecR[j];
        val(i,_nbf+3+j) =  crossProd(n,eRiemannR,j) / muVecR[j];
      }
      
    }
  }
};

void dgConservationLawMaxwell::addInterfaceSheet(const std::string tag,double Ys) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  _interfaceTerm0[tag] = new interfaceSheetTerm(_nbf,Ys,getEpsilon(tag),getMu(tag));
}

class dgConservationLawMaxwell::interfaceShieldTerm : public function {
  int _nbf;

  fullMatrix<double> normals;
  fullMatrix<double> solL, epsilonL, muL,impsL,imptL;
  fullMatrix<double> solR, epsilonR, muR,impsR,imptR;
 public:
   interfaceShieldTerm(int nbf,
                 const function *epsilonFunction,
                 const function *muFunction,const function *f,const function *g) : function (nbf*2) {
    _nbf = nbf;

    setArgument (normals, function::getNormals(), 0);
    setArgument (solL, function::getSolution(), 0);
    setArgument (solR, function::getSolution(), 1);
    setArgument (epsilonL, epsilonFunction, 0);
    setArgument (epsilonR, epsilonFunction, 1);
    setArgument (muL, muFunction, 0);
    setArgument (muR, muFunction, 1);
    setArgument (imptL, f, 0);
    setArgument (imptR, f, 1);
    setArgument (impsL, g, 0);
    setArgument (impsR, g, 1);
  };
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double n[3];
      for (int j=0; j<3; j++)
        n[j] = normals(i,j);
      
      // Physical parameters
      double epsilonVecL[3], muVecL[3];
      double epsilonVecR[3], muVecR[3];
      for (int j=0; j<3; j++) {
        epsilonVecL[j] = epsilonL(i,j);
        epsilonVecR[j] = epsilonR(i,j);
        muVecL[j]      = muL(i,j);
        muVecR[j]      = muR(i,j);
      }
      double epsilonValL=0., muValL=0.;
      double epsilonValR=0., muValR=0.;
      for (int j=0; j<3; j++) {
        epsilonValL += pow(n[j],2)*epsilonVecL[j];
        epsilonValR += pow(n[j],2)*epsilonVecR[j];
        muValL      += pow(n[j],2)*muVecL[j];
        muValR      += pow(n[j],2)*muVecR[j];
      }


      // Fields
      double eRiemannR[3], eRiemannL[3],imtR[3],imsR[3];
      double hL[3], hR[3], hRiemannR[3], hRiemannL[3],imtL[3],imsL[3];
      double hJumpR[3],hJumpL[3];
      //double eLR[3],eRL[3];
      //double hLR[3],hRL[3];
      for (int j=0; j<3; j++) {

        hL[j] = solL(i,3+j);

        hR[j] = solR(i,3+j);
        imtR[j] = imptR(i,3+j);
        imsR[j] = impsR(i,3+j);
        imtL[j] = imptL(i,3+j);
        imsL[j] = impsL(i,3+j);
      }


      for (int j=0; j<3; j++) {

        hJumpR[j] = (imtR[j]-imsL[j]);
        hJumpL[j] = (imsR[j]-imtL[j]);
      }

      for (int j=0; j<3; j++) {
        eRiemannR[j] = crossProd(n,hJumpR,j);
        eRiemannL[j] = crossProd(n,hJumpL,j);
        hRiemannR[j] = hR[j];
        hRiemannL[j] = hL[j];
      }
	/*        
      for (int j=0; j<3; j++) {
        eLR[j] = eR[j];
        eRL[j] = eL[j];
      }
    for (int j=0; j<3; j++) {


        hLR[j] = _Ys*crossProd(n,eLR,j)+hR[j];
        hRL[j] = _Ys*crossProd(n,eRL,j)+hL[j];
        
      }



      for (int j=0; j<3; j++) {
        eRiemannR[j] = YR*(eR[j]+eL[j])/YMean ;
        eRiemannL[j] = YL*(eR[j]+eL[j])/YMean ;
        hRiemannR[j] = hRL[j];
        hRiemannL[j] = hLR[j];
      }*/
      
      // Fluxes
      
      for (int j=0; j<3; j++) {
        // Fluxes for the left
        val(i,j)   =  crossProd(n,hRiemannL,j) / epsilonVecL[j];
        val(i,3+j) =  -crossProd(n,eRiemannL,j) / muVecL[j];
        // Fluxes for the right
        val(i,_nbf+j)   =  -crossProd(n,hRiemannR,j) / epsilonVecR[j];
        val(i,_nbf+3+j) =  crossProd(n,eRiemannR,j) / muVecR[j];
      }
      
    }
  }
};

void dgConservationLawMaxwell::addInterfaceShield(const std::string tag, const function *f, const function *g) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  _interfaceTerm0[tag] = new interfaceShieldTerm(_nbf,getEpsilon(tag),getMu(tag),f,g);
}
/*==============================================================================
 * Compute information
 *============================================================================*/

class dgConservationLawMaxwell::energyFunction : public function {
  fullMatrix<double> sol, epsilon, mu;
 public:
  energyFunction(function *solutionFunction,
                 const function *epsilonFunction,
                 const function *muFunction) : function(1) {
    setArgument (sol, solutionFunction);
    setArgument (epsilon, epsilonFunction);
    setArgument (mu, muFunction);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      double e[3] = {sol(i,0), sol(i,1), sol(i,2)};
      double h[3] = {sol(i,3), sol(i,4), sol(i,5)};
      double d[3], b[3];
      for (int j=0; j<3; j++) {
        d[j] = epsilon(i,j)*e[j];
        b[j] = mu(i,j)*h[j];
      }
      val(i,0) = 0.5 * (scalProd(h,b) + scalProd(e,d));
    }
  }
};

double dgConservationLawMaxwell::getTotalEnergy(dgGroupCollection *groups, dgDofContainer *solution, std::string tag) {
  function *_energyFunction = new energyFunction(solution->getFunction(),getEpsilon(tag),getMu(tag));
  fullMatrix<double> value(1,1);
  dgFunctionIntegrator _totalEnergy(groups, _energyFunction);
  _totalEnergy.compute(value, tag);
  delete _energyFunction;
  return value.get(0,0);
}

double dgConservationLawMaxwell::getVolume(dgGroupCollection *groups, std::string tag) {
  function *_unitFunction = new functionConstant(1.);
  fullMatrix<double> value(1,1);
  dgFunctionIntegrator _totalEnergy(groups, _unitFunction);
  _totalEnergy.compute(value, tag);
  delete _unitFunction;
  return value.get(0,0);
}


