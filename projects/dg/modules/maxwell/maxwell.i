%module maxwell

%{
  #undef HAVE_DLOPEN
  #include "dgConservationLawMaxwell.h"
  #include "dgConservationLawMaxwellExplicit.h"
%}

%import(module="dgpy.dgCommon") "dgCommon.i";
%import(module="dgpy.conservationLaw") "dgConservationLaw.h";
%import(module="dgpy.conservationLaw") "dgConservationLawFunction.h";

%include "dgConservationLawMaxwell.h"
%include "dgConservationLawMaxwellExplicit.h"
