#ifndef _DG_CONSERVATION_LAW_MAXWELL_EXPLICIT_H
#define _DG_CONSERVATION_LAW_MAXWELL_EXPLICIT_H
#include "dgConservationLawFunction.h"
class dgDofContainer;

/*                                                                                                                     
  Conservative law for Maxwell-Ampère equation:                                                                        
    \begin{align}                                                                                                      
      \varepsilon \partial_t(\vec{E}) = \rot{\vec{H}} + \sigma \vec{E}                                                 
    \end{align}                                                                                                        
                                                                                                                       
  Conservative law for Maxwell-Faraday equation:                                                                       
    \begin{align}                                                                                                      
      \mu \partial_t(\vec{H}) = -\rot{\vec{E}}                                                                         
    \end{align}                                                                                                        
                                                                                                                       
  Users/Developers:                                                                                                    
    Axel Modave <a.modave@ulg.ac.be>                                                                                   
    ...                                                                                                                
*/
class dgConservationLawMaxwellExplicit : public dgConservationLawFunction {

  std::string _EquationVersion;  // 'E' (Ampère) or 'H' (Faraday)                                                      
  std::string _LayerVersion;     // 'WithoutLayer', 'PmlStraight' or 'PmlSphere'                                       

  // List of tags that are used                                                                                        
  std::vector<std::string> _tagList;

  // Coefficients and input fields                                                                                     
  termMap _epsilonMap;  // epsilon_x, epsilon_y, epsilon_z                                                             
  termMap _muMap;       // mu_x, mu_y, mu_z                    

 termMap _sigmaMap;    // sigma_x, sigma_y, sigma_z                                                                   
  termMap _incidentFieldMap;
  termMap _sourceFieldMap;
  termMap _pmlCoefMap;  // PML: absorption coefficient (scalar)                                                        
  termMap _pmlDirMap;   // PML: absorption direction (vector)                                                          

  // Explicit field                                                                                                    
  const function *_E;  // 'E' for _EquationVersion='H'                                                                 
  const function *_H;  // 'H' for _EquationVersion='E'                                                                 

  // DG-terms                                                                                                          
  class psiVolumeETerm;
  class psiVolumeHTerm;
  class psiVolumeETermPml;
  class psiVolumeHTermPml;
  class dPsiVolumeETerm;
  class dPsiVolumeHTerm;
  class dPsiVolumeETermPml;
  class dPsiVolumeHTermPml;
  class interfaceETerm;
 class interfaceHTerm;
  class interfaceETermPml;
  class interfaceHTermPml;
  class interfaceETermPmlWithIncident;
  class interfaceHTermPmlWithIncident;
  class interfaceETermUncoupledRegions;
  class interfaceHTermUncoupledRegions;
  class maxConvectiveSpeed;

 public:
  dgConservationLawMaxwellExplicit(std::string EquationVersion, std::string LayerVersion="WithoutLayer");
  void setup();
  ~dgConservationLawMaxwellExplicit();

  std::string getEquationVersion() const {return _EquationVersion;}
  std::string getLayerVersion()    const {return _LayerVersion;}

// Set/get explicit fields   
void setE(const function *f) {if (getEquationVersion()=="E") Msg::Fatal("setE(...) cannot be used for E-equation."); else _E=f;}
  void setH(const function *f) {if (getEquationVersion()=="H") Msg::Fatal("setH(...) cannot be used for H-equation."); else _H=f;}
  const function *getE();
  const function *getH();

  // Add a physical tag in the list of tags that are used                                                              
  int addToTagList(const std::string tag);
  int isInTagList(const std::string tag);

  // Set/get coefficients and/or input fields for a physical tag                                                       
  const function *getFunctionForTag2(const termMap &, const std::string) const;

  void setEpsilon(const function *f)       {_epsilonMap[""] = f;}
  void setMu(const function *f)            {_muMap[""] = f;}
  void setSigma(const function *f)         {_sigmaMap[""] = f;}
  void setIncidentField(const function *f) {_incidentFieldMap[""] = f;}
  void setSourceField(const function *f)   {_sourceFieldMap[""] = f;}
  void setPmlCoef(const function *f)       {_pmlCoefMap[""] = f;}
  void setPmlDir(const function *f)        {_pmlDirMap[""] = f;}

 void setEpsilon(const std::string tag, const function *f)       {_epsilonMap[tag] = f;       addToTagList(tag);}
  void setMu(const std::string tag, const function *f)            {_muMap[tag] = f;            addToTagList(tag);}
  void setSigma(const std::string tag, const function *f)         {_sigmaMap[tag] = f;         addToTagList(tag);}
  void setIncidentField(const std::string tag, const function *f) {_incidentFieldMap[tag] = f; addToTagList(tag);}
  void setSourceField(const std::string tag, const function *f)   {_sourceFieldMap[tag] = f;   addToTagList(tag);}
  void setPmlCoef(const std::string tag, const function *f)       {_pmlCoefMap[tag] = f;       addToTagList(tag);}
  void setPmlDir(const std::string tag, const function *f)        {_pmlDirMap[tag] = f;        addToTagList(tag);}

  const function *getEpsilon(const std::string tag="")       {return getFunctionForTag2(_epsilonMap, tag);}
  const function *getMu(const std::string tag="")            {return getFunctionForTag2(_muMap, tag);}
  const function *getSigma(const std::string tag="")         {return getFunctionForTag2(_sigmaMap, tag);}
  const function *getIncidentField(const std::string tag="") {return getFunctionForTag2(_incidentFieldMap, tag);}
  const function *getSourceField(const std::string tag="")   {return getFunctionForTag2(_sourceFieldMap, tag);}
  const function *getPmlCoef(const std::string tag="")       {return getFunctionForTag2(_pmlCoefMap, tag);}
  const function *getPmlDir(const std::string tag="")        {return getFunctionForTag2(_pmlDirMap, tag);}

  // Add a Pml treatment for a physical tag                                                                            
  void addPml(const std::string);
  void addInterfacePml(const std::string);
 // Interface condition                                                                                               
  void addInterfacePmlWithIncident(const std::string tag, const function *f=NULL);

  // Boundary conditions                                                                                               


  dgBoundaryCondition *newBoundarySymmetry(const std::string);
  dgBoundaryCondition *newBoundaryDirichletOnE(const std::string, const function *);
  dgBoundaryCondition *newBoundaryHomogeneousDirichletOnE(const std::string);
  dgBoundaryCondition *newBoundaryElectricWall(const std::string);
  dgBoundaryCondition *newBoundaryDirichletOnH(const std::string, const function *);
  dgBoundaryCondition *newBoundaryHomogeneousDirichletOnH(const std::string);
  dgBoundaryCondition *newBoundaryMagneticWall(const std::string);
  dgBoundaryCondition *newBoundarySilverMuller(const std::string);
  dgBoundaryCondition *newBoundarySilverMullerDirichletOnE(const std::string, const function*);
  dgBoundaryCondition *newBoundarySilverMullerDirichletOnH(const std::string, const function*);

  //Interface Conditions                                                                                               
  void addInterfaceHomogeneousDirichletOnE(const std::string tag);
  void addInterfaceHomogeneousDirichletOnH(const std::string tag);
  // void addInterfaceEwall(const std::string tag);                  
 // void addInterfaceHwall(const std::string tag);                                                                    

  // Get information                                                                                                   
  class electricEnergyFunction;
  class magneticEnergyFunction;
  double getElectricEnergy(dgGroupCollection *groups, dgDofContainer *solution, std::string tag="");
  double getMagneticEnergy(dgGroupCollection *groups, dgDofContainer *solution, dgDofContainer *previousSolution, std::string tag="");
  double getVolume(dgGroupCollection *groups, std::string tag="");

};



#endif




