#ifndef _DG_CONSERVATION_LAW_MAXWELL_H
#define _DG_CONSERVATION_LAW_MAXWELL_H
class dgDofContainer;
class dgGroupCollection;
#include "dgConservationLawFunction.h"

/*
  Conservative law for Maxwell Equations:
    \begin{align}
      \varepsilon \partial_t(\vec{E}) =  \rot{\vec{H}} + \sigma \vec{E}
      \mu         \partial_t(\vec{H}) = -\rot{\vec{E}}
    \end{align}

  Users/Developers:
    Axel Modave <a.modave@ulg.ac.be>
    ...
*/

class dgConservationLawMaxwell : public dgConservationLawFunction {
  
  std::string _LayerVersion;   // 'WithoutLayer', 'PmlStraight' or 'PmlSphere'
  
  // List of tags that are used
  std::vector<std::string> _tagList;
  
  // Coefficients and input fields
  termMap _epsilonMap;  // epsilon_x, epsilon_y, epsilon_z
  termMap _muMap;       // mu_x, mu_y, mu_z
  termMap _sigmaMap;    // sigma_x, sigma_y, sigma_z
  termMap _incidentFieldMap;
  termMap _sourceFieldMap;
  termMap _pmlCoefMap;  // PML: absorption coefficient (scalar)
  termMap _pmlDirMap;   // PML: absorption direction (vector)

  
  // DG-terms
  class psiVolumeTerm;
  class psiVolumeTermPml;
  class dPsiVolumeTerm;
  class dPsiVolumeTermPml;
  class interfaceTerm;
  class interfaceTermPml;
  class interfaceTermPmlWithIncident;
  class interfaceTermUncoupledRegions;
  class interfaceSheetTerm;
  class interfaceShieldTerm;
  class riemannInterDomains;
  class maxConvectiveSpeed;
  
 public:
  dgConservationLawMaxwell(std::string LayerVersion="WithoutLayer");
  void setup();
  ~dgConservationLawMaxwell();
  
  std::string getLayerVersion() const {return _LayerVersion;}
  
  // Add a physical tag in the list of tags that are used
  int addToTagList(const std::string tag);
  int isInTagList(const std::string tag);
  
  // Set/get coefficients and/or input fields for a physical tag
  const function *getFunctionForTag2(const termMap &, const std::string) const;
  
  void setEpsilon(const function *f)       {_epsilonMap[""] = f;}
  void setMu(const function *f)            {_muMap[""] = f;}
  void setSigma(const function *f)         {_sigmaMap[""] = f;}
  void setIncidentField(const function *f) {_incidentFieldMap[""] = f;}
  void setSourceField(const function *f)   {_sourceFieldMap[""] = f;}
  void setPmlCoef(const function *f)       {_pmlCoefMap[""] = f;}
  void setPmlDir(const function *f)        {_pmlDirMap[""] = f;}
  
  void setEpsilon(const std::string tag, const function *f)       {_epsilonMap[tag] = f;       addToTagList(tag);}
  void setMu(const std::string tag, const function *f)            {_muMap[tag] = f;            addToTagList(tag);}
  void setSigma(const std::string tag, const function *f)         {_sigmaMap[tag] = f;         addToTagList(tag);}
  void setIncidentField(const std::string tag, const function *f) {_incidentFieldMap[tag] = f; addToTagList(tag);}
  void setSourceField(const std::string tag, const function *f)   {_sourceFieldMap[tag] = f;   addToTagList(tag);}
  void setPmlCoef(const std::string tag, const function *f)       {_pmlCoefMap[tag] = f;       addToTagList(tag);}
  void setPmlDir(const std::string tag, const function *f)        {_pmlDirMap[tag] = f;        addToTagList(tag);}
  
  const function *getEpsilon(const std::string tag="")       {return getFunctionForTag2(_epsilonMap, tag);}
  const function *getMu(const std::string tag="")            {return getFunctionForTag2(_muMap, tag);}
  const function *getSigma(const std::string tag="")         {return getFunctionForTag2(_sigmaMap, tag);}
  const function *getIncidentField(const std::string tag="") {return getFunctionForTag2(_incidentFieldMap, tag);}
  const function *getSourceField(const std::string tag="")   {return getFunctionForTag2(_sourceFieldMap, tag);}
  const function *getPmlCoef(const std::string tag="")       {return getFunctionForTag2(_pmlCoefMap, tag);}
  const function *getPmlDir(const std::string tag="")        {return getFunctionForTag2(_pmlDirMap, tag);}
  

  
  // Add a Pml treatment for a physical tag
  void addPml(const std::string, std::string LayerFluxes="Friedrich");
  
  // Boundary conditions
  dgBoundaryCondition *newBoundarySymmetry(const std::string);
  dgBoundaryCondition *newBoundaryDirichletOnE(const std::string, const function *);
  dgBoundaryCondition *newBoundaryHomogeneousDirichletOnE(const std::string);
  dgBoundaryCondition *newBoundaryElectricWall(const std::string);
  dgBoundaryCondition *newBoundaryDirichletOnH(const std::string, const function *);
  dgBoundaryCondition *newBoundaryHomogeneousDirichletOnH(const std::string);
  dgBoundaryCondition *newBoundaryMagneticWall(const std::string);
  dgBoundaryCondition *newBoundarySilverMuller(const std::string);
  dgBoundaryCondition *newBoundarySilverMullerDirichlet(const std::string, const function *);
  
  // Interface conditions
  void addInterfacePml(const std::string);
  void addInterfacePmlWithIncident(const std::string tag, const function *f=NULL);
  void addInterfaceEwall(const std::string tag);
  void addInterfaceHwall(const std::string tag);
  void addInterfaceSM(const std::string tag);
  void addInterfaceSMD(const std::string tag, const function *);
  void addInterfaceSheet(const std::string,double Ys);
  void addInterfaceShield(const std::string, const function *f, const function *g);
  
  // Get information
  class energyFunction;
  double getTotalEnergy(dgGroupCollection *groups, dgDofContainer *solution, std::string tag="");
  double getVolume(dgGroupCollection *groups, std::string tag="");
  
};

#endif
