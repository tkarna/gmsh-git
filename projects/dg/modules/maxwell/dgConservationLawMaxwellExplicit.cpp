#include "function.h"
#include "functionGeneric.h"
#include "math.h"
#include "Numeric.h"

#include "dgConservationLawFunction.h"
#include "dgConservationLawMaxwellExplicit.h"
#include "dgDofContainer.h"
#include "dgFunctionIntegrator.h"


/*==============================================================================
 * DG-terms
 *============================================================================*/

class dgConservationLawMaxwellExplicit::psiVolumeETerm : public function {
  int sigmaDefined, sourceFieldDefined;
  fullMatrix<double> solE, epsilon, sigma, sourceField;
 public:
  psiVolumeETerm(int nbf,
                 const function *epsilonFunction,
                 const function *sigmaFunction,
                 const function *sourceFieldFunction) : function(nbf) {
    setArgument (solE, function::getSolution());
    setArgument (epsilon, epsilonFunction);
    sigmaDefined = 0;
    if (sigmaFunction) {
      setArgument (sigma, sigmaFunction);
      sigmaDefined = 1;
    }
    sourceFieldDefined = 0;
    if (sourceFieldFunction) {
      setArgument (sourceField, sourceFieldFunction);
      sourceFieldDefined = 1;
    }
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    if (sigmaDefined)
      for (size_t i=0; i<nQP; i++)
          for (size_t j=0; j<3; j++)
            val(i,j) -= (sigma(i,j)/epsilon(i,j)) * solE(i,j);
    if (sourceFieldDefined)
      for (size_t i=0; i<nQP; i++)
          for (size_t j=0; j<3; j++)
            val(i,j) += sourceField(i,j)/epsilon(i,j);
  }
};

class dgConservationLawMaxwellExplicit::dPsiVolumeETerm : public function {
  int _nbf;
  fullMatrix<double> solH, epsilon;
 public:
  dPsiVolumeETerm(int nbf, const function *H, const function *epsilonFunction) : function(nbf*3) {
    _nbf = nbf;
    setArgument (solH, H);
    setArgument (epsilon, epsilonFunction);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double h[3] = {solH(i,0), solH(i,1), solH(i,2)};
      
      // term \partial_x
      val(i,0) = 0;
      val(i,1) = h[2]/epsilon(i,1);
      val(i,2) =-h[1]/epsilon(i,2);
      
      // term \partial_y
      val(i,_nbf+0) =-h[2]/epsilon(i,0);
      val(i,_nbf+1) = 0;
      val(i,_nbf+2) = h[0]/epsilon(i,2);
      
      // term \partial_z
      val(i,2*_nbf+0) = h[1]/epsilon(i,0);
      val(i,2*_nbf+1) =-h[0]/epsilon(i,1);
      val(i,2*_nbf+2) = 0;
      
    }
  }
};


class dgConservationLawMaxwellExplicit::dPsiVolumeHTerm : public function {
  int _nbf;
  fullMatrix<double> solE, mu;
 public:
  dPsiVolumeHTerm(int nbf,  const function *E, const function *muFunction) : function(nbf*3) {
    _nbf = nbf;
    setArgument (solE, E);
    setArgument (mu, muFunction);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double e[3] = {solE(i,0), solE(i,1), solE(i,2)};
      
      // term \partial_x
      val(i,0) = 0;
      val(i,1) =-e[2]/mu(i,1);
      val(i,2) = e[1]/mu(i,2);
      
      // term \partial_y
      val(i,_nbf+0) = e[2]/mu(i,0);
      val(i,_nbf+1) = 0;
      val(i,_nbf+2) =-e[0]/mu(i,2);
      
      // term \partial_z
      val(i,2*_nbf+0) =-e[1]/mu(i,0);
      val(i,2*_nbf+1) = e[0]/mu(i,1);
      val(i,2*_nbf+2) = 0;
      
    }
  }
};

class dgConservationLawMaxwellExplicit::interfaceETerm : public function {
  int _nbf;
  fullMatrix<double> normals;
  fullMatrix<double> solH_L, solE_L, epsilonL, muL;
  fullMatrix<double> solH_R, solE_R, epsilonR, muR;
public:
  interfaceETerm(int nbf,
		 const function *H,
		 const function *epsilonFunction,
		 const function *muFunction) : function(nbf*2) {
    _nbf = nbf;
    setArgument (normals, function::getNormals(), 0);
    setArgument (solE_L, function::getSolution(), 0);
    setArgument (solE_R, function::getSolution(), 1);
    setArgument (solH_L, H, 0);
    setArgument (solH_R, H, 1);
    setArgument (epsilonL, epsilonFunction, 0);
    setArgument (epsilonR, epsilonFunction, 1);
    setArgument (muL, muFunction, 0);
    setArgument (muR, muFunction, 1);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double n[3] = {normals(i,0), normals(i,1), normals(i,2)};
      
      // Physical parameters
      double epsilonVecL[3], muVecL[3];
      double epsilonVecR[3], muVecR[3];
      for (int j=0; j<3; j++) {
        epsilonVecL[j] = epsilonL(i,j);
        epsilonVecR[j] = epsilonR(i,j);
        muVecL[j]      = muL(i,j);
        muVecR[j]      = muR(i,j);
      }
      double epsilonValL=0., muValL=0.;
      double epsilonValR=0., muValR=0.;
      for (int j=0; j<3; j++) {
        epsilonValL += pow(n[j],2)*epsilonVecL[j];
        epsilonValR += pow(n[j],2)*epsilonVecR[j];
        muValL      += pow(n[j],2)*muVecL[j];
        muValR      += pow(n[j],2)*muVecR[j];
      }
      
      double ZL = pow(muValL/epsilonValL, 0.5);
      double ZR = pow(muValR/epsilonValR, 0.5);
      double ZMean = (ZR+ZL)/2.;
     
      
      // Fields
      double eL[3], eR[3], eJump[3];
      double hL[3], hR[3], hZMean[3], hRiemann[3];
      for (int j=0; j<3; j++) {
        eL[j] = solE_L(i,j);
        hL[j] = solH_L(i,j);
        eR[j] = solE_R(i,j);
        hR[j] = solH_R(i,j);
      }
      for (int j=0; j<3; j++) {
        hZMean[j] = (ZR*hR[j]+ZL*hL[j])/2.;
        eJump[j] = (eR[j]-eL[j])/2.;
      }
      for (int j=0; j<3; j++) {
        hRiemann[j] = (hZMean[j] - crossProd(n,eJump,j)) / ZMean;
      }
      
      // Fields
      // double hMean[3];
      /*
      for (int j=0; j<3; j++)
        hMean[j] = ((ZR*solH_R(i,j) + ZL*solH_L(i,j))/2.) / ZMean;
      */
      // for (int j=0; j<3; j++)
      //  hMean[j] = (solH_R(i,j)+solH_L(i,j))/2.;
      
      // Fluxes
      for (int j=0; j<3; j++) {
        val(i,j)   =  crossProd(n,hRiemann,j) / epsilonVecL[j];
        val(i,_nbf+j)   = -crossProd(n,hRiemann,j) / epsilonVecR[j];
       
      }
      
    }
  }
};


class dgConservationLawMaxwellExplicit::interfaceHTerm : public function {
  int _nbf;
  fullMatrix<double> normals;
  fullMatrix<double> solE_L,solH_L, epsilonL, muL;
  fullMatrix<double> solE_R, solH_R, epsilonR, muR;
public:
  interfaceHTerm(int nbf,
		 const function *E,
		 const function *epsilonFunction,
		 const function *muFunction) : function(nbf*2) {
    _nbf = nbf;
    setArgument (normals, function::getNormals(), 0);
    setArgument (solH_L, function::getSolution(), 0);
    setArgument (solH_R, function::getSolution(), 1);
    setArgument (solE_L, E, 0);
    setArgument (solE_R, E, 1);
    setArgument (epsilonL, epsilonFunction, 0);
    setArgument (epsilonR, epsilonFunction, 1);
    setArgument (muL, muFunction, 0);
    setArgument (muR, muFunction, 1);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double n[3] = {normals(i,0), normals(i,1), normals(i,2)};
      
      // Physical parameters
      double epsilonVecL[3], muVecL[3];
      double epsilonVecR[3], muVecR[3];
      for (int j=0; j<3; j++) {
        epsilonVecL[j] = epsilonL(i,j);
        epsilonVecR[j] = epsilonR(i,j);
        muVecL[j]      = muL(i,j);
        muVecR[j]      = muR(i,j);
      }
      double epsilonValL=0., muValL=0.;
      double epsilonValR=0., muValR=0.;
      for (int j=0; j<3; j++) {
        epsilonValL += pow(n[j],2)*epsilonVecL[j];
        epsilonValR += pow(n[j],2)*epsilonVecR[j];
        muValL      += pow(n[j],2)*muVecL[j];
        muValR      += pow(n[j],2)*muVecR[j];
      }
      double YL = pow(muValL/epsilonValL, -0.5);
      double YR = pow(muValR/epsilonValR, -0.5);
      double YMean = (YR+YL)/2.;
      
      // Fields
      double eL[3], eR[3], eYMean[3], eRiemann[3];
      double hL[3], hR[3], hJump[3];
      for (int j=0; j<3; j++) {
        eL[j] = solE_L(i,j);
        hL[j] = solH_L(i,j);
        eR[j] = solE_R(i,j);
        hR[j] = solH_R(i,j);
      }
      for (int j=0; j<3; j++) {
        eYMean[j] = (YR*eR[j]+YL*eL[j])/2.;
        hJump[j] = (hR[j]-hL[j])/2.;
      }
      for (int j=0; j<3; j++) {
        eRiemann[j] = (eYMean[j] + crossProd(n,hJump,j)) / YMean;
      }
      
     
      // Fields
      // double eMean[3];
      /*
      for (int j=0; j<3; j++)
        eMean[j] = ((YR*solE_R(i,j) + YL*solE_L(i,j))/2.) / YMean;
      */
      // for (int j=0; j<3; j++)
      //  eMean[j] = (solE_R(i,j) + solE_L(i,j))/2.;
      
      // Fluxes
      for (int j=0; j<3; j++) {
        val(i,j) = -crossProd(n,eRiemann,j) / muVecL[j];
        val(i,_nbf+j) =  crossProd(n,eRiemann,j) / muVecR[j];
      
      }
      
    }
  }
};


class dgConservationLawMaxwellExplicit::maxConvectiveSpeed : public function {
  fullMatrix<double> epsilon, mu;
 public:
  maxConvectiveSpeed(int nbf,
                     const function *epsilonFunction,
                     const function *muFunction) : function(nbf) {
    setArgument (epsilon, epsilonFunction);
    setArgument (mu, muFunction);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++)
      val(i,0) = 1/pow(fmin(epsilon(i,0)*mu(i,0), fmin(epsilon(i,1)*mu(i,1), epsilon(i,2)*mu(i,2))), 0.5);
  }
};


/*==============================================================================
 * DG-terms : Pml
 *============================================================================*/

class dgConservationLawMaxwellExplicit::psiVolumeETermPml : public function {
  fullMatrix<double> solE, pmlCoef, pmlDir;
 public:
  psiVolumeETermPml(int nbf,
                    const function *pmlCoefFunction,
                    const function *pmlDirFunction) : function(nbf) {
    setArgument (solE, function::getSolution());
    setArgument (pmlCoef, pmlCoefFunction);
    setArgument (pmlDir, pmlDirFunction);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      double sigmaPml = pmlCoef(i,0);
      double t1[3] = {pmlDir(i,3), pmlDir(i,4), pmlDir(i,5)};
      double t2[3] = {pmlDir(i,6), pmlDir(i,7), pmlDir(i,8)};
      double e1 = solE(i,3);
      double e2 = solE(i,4);
      val(i,0) -= sigmaPml * (t1[0] * e1 + t2[0] * e2);
      val(i,1) -= sigmaPml * (t1[1] * e1 + t2[1] * e2);
      val(i,2) -= sigmaPml * (t1[2] * e1 + t2[2] * e2);
      val(i,3) -= sigmaPml * e1;
      val(i,4) -= sigmaPml * e2;
    }
  }
};

class dgConservationLawMaxwellExplicit::psiVolumeHTermPml : public function {
  fullMatrix<double> solH, pmlCoef, pmlDir;
 public:
  psiVolumeHTermPml(int nbf,
                    const function *pmlCoefFunction,
                    const function *pmlDirFunction) : function(nbf) {
    setArgument (solH, function::getSolution());
    setArgument (pmlCoef, pmlCoefFunction);
    setArgument (pmlDir, pmlDirFunction);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      double sigmaPml = pmlCoef(i,0);
      double t1[3] = {pmlDir(i,3), pmlDir(i,4), pmlDir(i,5)};
      double t2[3] = {pmlDir(i,6), pmlDir(i,7), pmlDir(i,8)};
      double h1 = solH(i,3);
      double h2 = solH(i,4);
      val(i,0) -= sigmaPml * (t1[0] * h1 + t2[0] * h2);
      val(i,1) -= sigmaPml * (t1[1] * h1 + t2[1] * h2);
      val(i,2) -= sigmaPml * (t1[2] * h1 + t2[2] * h2);
      val(i,3) -= sigmaPml * h1;
      val(i,4) -= sigmaPml * h2;
    }
  }
};

class dgConservationLawMaxwellExplicit::dPsiVolumeETermPml : public function {
  int _nbf;
  fullMatrix<double> solH, epsilon, pmlDir;
 public:
  dPsiVolumeETermPml(int nbf,
                     const function *H,
                     const function *epsilonFunction,
                     const function *pmlDirFunction) : function(nbf*3) {
    _nbf = nbf;
    setArgument (solH, H);
    setArgument (epsilon, epsilonFunction);
    setArgument (pmlDir, pmlDirFunction);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double a[3]  = {pmlDir(i,0), pmlDir(i,1), pmlDir(i,2)};
      double t1[3] = {pmlDir(i,3), pmlDir(i,4), pmlDir(i,5)};
      double t2[3] = {pmlDir(i,6), pmlDir(i,7), pmlDir(i,8)};
      
      // Fields/parameters
      double epsilonVec[3] = {epsilon(i,0), epsilon(i,1), epsilon(i,2)};
      double h[3] = {solH(i,0), solH(i,1), solH(i,2)};
      double acrossh_epsilon[3];
      for (int j=0; j<3; j++)
        acrossh_epsilon[j] = crossProd(a,h,j)/epsilonVec[j];
      
      // term \partial_x
      val(i,0) = 0;
      val(i,1) = h[2]/epsilonVec[1];
      val(i,2) =-h[1]/epsilonVec[2];
      val(i,3) =-scalProd(t1,acrossh_epsilon) * a[0];
      val(i,4) =-scalProd(t2,acrossh_epsilon) * a[0];
      
      // term \partial_y
      val(i,_nbf+0) =-h[2]/epsilonVec[0];
      val(i,_nbf+1) = 0;
      val(i,_nbf+2) = h[0]/epsilonVec[2];
      val(i,_nbf+3) =-scalProd(t1,acrossh_epsilon) * a[1];
      val(i,_nbf+4) =-scalProd(t2,acrossh_epsilon) * a[1];
      
      // term \partial_z
      val(i,2*_nbf+0) = h[1]/epsilonVec[0];
      val(i,2*_nbf+1) =-h[0]/epsilonVec[1];
      val(i,2*_nbf+2) = 0;
      val(i,2*_nbf+3) =-scalProd(t1,acrossh_epsilon) * a[2];
      val(i,2*_nbf+4) =-scalProd(t2,acrossh_epsilon) * a[2];
      
    }
  }
};

class dgConservationLawMaxwellExplicit::dPsiVolumeHTermPml : public function {
  int _nbf;
  fullMatrix<double> solE, mu, pmlDir;
 public:
  dPsiVolumeHTermPml(int nbf,
                     const function *E,
                     const function *muFunction,
                     const function *pmlDirFunction) : function(nbf*3) {
    _nbf = nbf;
    setArgument (solE, E);
    setArgument (mu, muFunction);
    setArgument (pmlDir, pmlDirFunction);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double a[3]  = {pmlDir(i,0), pmlDir(i,1), pmlDir(i,2)};
      double t1[3] = {pmlDir(i,3), pmlDir(i,4), pmlDir(i,5)};
      double t2[3] = {pmlDir(i,6), pmlDir(i,7), pmlDir(i,8)};
      
      // Fields/parameters
      double muVec[3] = {mu(i,0), mu(i,1), mu(i,2)};
      double e[3] = {solE(i,0), solE(i,1), solE(i,2)};
      double acrosse_mu[3];
      for (int j=0; j<3; j++)
        acrosse_mu[j] = crossProd(a,e,j)/muVec[j];
      
      // term \partial_x
      val(i,0) = 0;
      val(i,1) =-e[2]/muVec[1];
      val(i,2) = e[1]/muVec[2];
      val(i,3) = scalProd(t1,acrosse_mu) * a[0];
      val(i,4) = scalProd(t2,acrosse_mu) * a[0];
      
      // term \partial_y
      val(i,_nbf+0) = e[2]/muVec[0];
      val(i,_nbf+1) = 0;
      val(i,_nbf+2) =-e[0]/muVec[2];
      val(i,_nbf+3) = scalProd(t1,acrosse_mu) * a[1];
      val(i,_nbf+4) = scalProd(t2,acrosse_mu) * a[1];
      
      // term \partial_z
      val(i,2*_nbf+0) =-e[1]/muVec[0];
      val(i,2*_nbf+1) = e[0]/muVec[1];
      val(i,2*_nbf+2) = 0;
      val(i,2*_nbf+3) = scalProd(t1,acrosse_mu) * a[2];
      val(i,2*_nbf+4) = scalProd(t2,acrosse_mu) * a[2];
      
    }
  }
};

class dgConservationLawMaxwellExplicit::interfaceETermPml : public function {
  int _nbf;
  std::string _EquationVersion;
  fullMatrix<double> normals;
  fullMatrix<double> solH_L, epsilonL, muL, pmlDirL;
  fullMatrix<double> solH_R, epsilonR, muR, pmlDirR;
 public:
  interfaceETermPml(int nbf,
                    const function *H,
                    const function *epsilonFunction,
                    const function *muFunction,
                    const function *pmlDirFunction) : function (nbf*2) {
    _nbf = nbf;
    setArgument (normals, function::getNormals(), 0);
    setArgument (solH_L, H, 0);
    setArgument (solH_R, H, 1);
    setArgument (epsilonL, epsilonFunction, 0);
    setArgument (epsilonR, epsilonFunction, 1);
    setArgument (muL, muFunction, 0);
    setArgument (muR, muFunction, 1);
    setArgument (pmlDirL, pmlDirFunction, 0);
    setArgument (pmlDirR, pmlDirFunction, 1);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double n[3] = {normals(i,0), normals(i,1), normals(i,2)};
      
      // Pml-Directions
      double aL[3], t1L[3], t2L[3];
      double aR[3], t1R[3], t2R[3];
      for (int j=0; j<3; j++) {
        aL[j]  = pmlDirL(i,j); t1L[j] = pmlDirL(i,j+3); t2L[j] = pmlDirL(i,j+6);
        aR[j]  = pmlDirR(i,j); t1R[j] = pmlDirR(i,j+3); t2R[j] = pmlDirR(i,j+6);
      }
      double ndotaL = scalProd(n,aL);
      double ndotaR = scalProd(n,aR);
      
      // Physical parameters
      double epsilonVecL[3], muVecL[3];
      double epsilonVecR[3], muVecR[3];
      for (int j=0; j<3; j++) {
        epsilonVecL[j] = epsilonL(i,j); muVecL[j] = muL(i,j);
        epsilonVecR[j] = epsilonR(i,j); muVecR[j] = muR(i,j);
      }
      double epsilonValL=0., muValL=0.;
      double epsilonValR=0., muValR=0.;
      for (int j=0; j<3; j++) {
        epsilonValL += pow(n[j],2)*epsilonVecL[j];
        epsilonValR += pow(n[j],2)*epsilonVecR[j];
        muValL      += pow(n[j],2)*muVecL[j];
        muValR      += pow(n[j],2)*muVecR[j];
      }
      
      // Fields
      double hMean[3];
      for (int j=0; j<3; j++)
        hMean[j] = (solH_R(i,j)+solH_L(i,j))/2.;
      /*
      for (int j=0; j<3; j++)
        hMean[j] = ((ZR*solH_R(i,j)+ZL*solH_L(i,j))/2.) / ZMean;
      */
      
      // Fluxes
      for (int j=0; j<3; j++) {
        double temp = crossProd(n,hMean,j);
        val(i,j)      =  temp/epsilonVecL[j];
        val(i,_nbf+j) = -temp/epsilonVecR[j];
      }
      
      // Additionnal fluxes
      double aLcrossh_epsilonL[3];
      double aRcrossh_epsilonR[3];
      for (int j=0; j<3; j++) {
        aLcrossh_epsilonL[j] = crossProd(aL,hMean,j)/epsilonVecL[j];
        aRcrossh_epsilonR[j] = crossProd(aR,hMean,j)/epsilonVecR[j];
      }
      val(i,3)      = ndotaL*scalProd(t1L,aLcrossh_epsilonL);
      val(i,4)      = ndotaL*scalProd(t2L,aLcrossh_epsilonL);
      val(i,_nbf+3) =-ndotaR*scalProd(t1R,aRcrossh_epsilonR);
      val(i,_nbf+4) =-ndotaR*scalProd(t2R,aRcrossh_epsilonR);
      
    }
  }
};

class dgConservationLawMaxwellExplicit::interfaceHTermPml : public function {
  int _nbf;
  fullMatrix<double> normals;
  fullMatrix<double> solE_L, epsilonL, muL, pmlDirL;
  fullMatrix<double> solE_R, epsilonR, muR, pmlDirR;
 public:
  interfaceHTermPml(int nbf,
                    const function *E,
                    const function *epsilonFunction,
                    const function *muFunction,
                    const function *pmlDirFunction) : function (nbf*2) {
    _nbf = nbf;
    setArgument (normals, function::getNormals(), 0);
    setArgument (solE_L, E, 0);
    setArgument (solE_R, E, 1);
    setArgument (epsilonL, epsilonFunction, 0);
    setArgument (epsilonR, epsilonFunction, 1);
    setArgument (muL, muFunction, 0);
    setArgument (muR, muFunction, 1);
    setArgument (pmlDirL, pmlDirFunction, 0);
    setArgument (pmlDirR, pmlDirFunction, 1);
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double n[3] = {normals(i,0), normals(i,1), normals(i,2)};
      
      // Pml-Directions
      double aL[3], t1L[3], t2L[3];
      double aR[3], t1R[3], t2R[3];
      for (int j=0; j<3; j++) {
        aL[j]  = pmlDirL(i,j); t1L[j] = pmlDirL(i,j+3); t2L[j] = pmlDirL(i,j+6);
        aR[j]  = pmlDirR(i,j); t1R[j] = pmlDirR(i,j+3); t2R[j] = pmlDirR(i,j+6);
      }
      double ndotaL = scalProd(n,aL);
      double ndotaR = scalProd(n,aR);
      
      // Physical parameters
      double epsilonVecL[3], muVecL[3];
      double epsilonVecR[3], muVecR[3];
      for (int j=0; j<3; j++) {
        epsilonVecL[j] = epsilonL(i,j); muVecL[j] = muL(i,j);
        epsilonVecR[j] = epsilonR(i,j); muVecR[j] = muR(i,j);
      }
      double epsilonValL=0., muValL=0.;
      double epsilonValR=0., muValR=0.;
      for (int j=0; j<3; j++) {
        epsilonValL += pow(n[j],2)*epsilonVecL[j];
        epsilonValR += pow(n[j],2)*epsilonVecR[j];
        muValL      += pow(n[j],2)*muVecL[j];
        muValR      += pow(n[j],2)*muVecR[j];
      }
      
      // Fields
      double eMean[3];
      for (int j=0; j<3; j++)
        eMean[j] = (solE_R(i,j)+solE_L(i,j))/2.;
      /*
      for (int j=0; j<3; j++)
        eMean[j] = ((YR*solE_R(i,j)+YL*solE_L(i,j))/2.) / YMean;
      */
      
      // Fluxes
      for (int j=0; j<3; j++) {
        double temp = crossProd(n,eMean,j);
        val(i,j)      = -temp/muVecL[j];
        val(i,_nbf+j) =  temp/muVecR[j];
      }
      
      // Additionnal fluxes
      double aLcrosse_muL[3];
      double aRcrosse_muR[3];
      for (int j=0; j<3; j++) {
        aLcrosse_muL[j]      = crossProd(aL,eMean,j)/muVecL[j];
        aRcrosse_muR[j]      = crossProd(aR,eMean,j)/muVecR[j];
      }
      val(i,3)      =-ndotaL*scalProd(t1L,aLcrosse_muL);
      val(i,4)      =-ndotaL*scalProd(t2L,aLcrosse_muL);
      val(i,_nbf+3) = ndotaR*scalProd(t1R,aRcrosse_muR);
      val(i,_nbf+4) = ndotaR*scalProd(t2R,aRcrosse_muR);
    }
    
  }
};


/*==============================================================================
 * Conservation law : constructor - setup - destructor
 *============================================================================*/

int dgConservationLawMaxwellExplicit_nbf(std::string LayerVersion) {
  if (LayerVersion=="WithoutLayer")
    return 3;
  else if (LayerVersion=="PmlStraight")
    return 5;
  else
    Msg::Fatal("Dg/MaxwellExplicit: bad LayerVersion (not '%s')", LayerVersion.c_str());
  return -1;
}

dgConservationLawMaxwellExplicit::dgConservationLawMaxwellExplicit(std::string EquationVersion,
                                                                   std::string LayerVersion) :
    dgConservationLawFunction(dgConservationLawMaxwellExplicit_nbf(LayerVersion))
{
  _EquationVersion = EquationVersion;
  _LayerVersion = LayerVersion;
  addToTagList("");
  setSigma(0);
  setIncidentField(0);
  setSourceField(0);
  setPmlCoef(0);
  setPmlDir(0);
  _E=0;
  _H=0;
}

void dgConservationLawMaxwellExplicit::setup() {
  if (_EquationVersion=="E")
    for (std::vector<std::string>::iterator it=_tagList.begin(); it!=_tagList.end(); ++it) {
      const std::string tag = *it;
      //_volumeTerm0[tag]            = new psiVolumeETerm    (_nbf,getEpsilon(tag),getSigma(tag),getSourceField(tag));
      _volumeTerm1[tag]            = new dPsiVolumeETerm   (_nbf,getH(),getEpsilon(tag));
      _interfaceTerm0[tag]         = new interfaceETerm    (_nbf,getH(),getEpsilon(tag),getMu(tag));
      _maximumConvectiveSpeed[tag] = new maxConvectiveSpeed(_nbf,getEpsilon(tag),getMu(tag));
    }
  else if (_EquationVersion=="H")
    for (std::vector<std::string>::iterator it=_tagList.begin(); it!=_tagList.end(); ++it) {
      const std::string tag = *it;
      _volumeTerm1[tag]            = new dPsiVolumeHTerm   (_nbf,getE(),getMu(tag));
      _interfaceTerm0[tag]         = new interfaceHTerm    (_nbf,getE(),getEpsilon(tag),getMu(tag));
      _maximumConvectiveSpeed[tag] = new maxConvectiveSpeed(_nbf,getEpsilon(tag),getMu(tag));
    }
  else
    Msg::Fatal("Dg/MaxwellExplicit: bad EquationVersion (not '%s')", _EquationVersion.c_str());
}

dgConservationLawMaxwellExplicit::~dgConservationLawMaxwellExplicit() {
  for (std::vector<std::string>::iterator it=_tagList.begin(); it!=_tagList.end(); ++it) {
    const std::string tag = *it;
    if (_volumeTerm0[tag])            delete _volumeTerm0[tag];
    if (_volumeTerm1[tag])            delete _volumeTerm1[tag];
    if (_maximumConvectiveSpeed[tag]) delete _maximumConvectiveSpeed[tag];
    if (_interfaceTerm0[tag])         delete _interfaceTerm0[tag];
  }
}


/*==============================================================================
 * Coefficients : set - get
 *============================================================================*/

const function *dgConservationLawMaxwellExplicit::getE() {
  if (_EquationVersion=="E")
    Msg::Fatal("Dg/MaxwellExplicit: getE() cannot be used with EquationVersion='E'.");
  else if ((_EquationVersion=="H") && (_E==0))
    Msg::Fatal("Dg/MaxwellExplicit: getE() doesn't work: E is empty.");
  else 
    return _E;
  return NULL;
}

const function *dgConservationLawMaxwellExplicit::getH() {
  if (_EquationVersion=="H")
    Msg::Fatal("Dg/MaxwellExplicit: getH() cannot be used with EquationVersion='H'.");
  else if ((_EquationVersion=="E") && (_H==0))
    Msg::Fatal("Dg/MaxwellExplicit: getH() doesn't work: H is empty.");
  else 
    return _H;
  return NULL;
}

int dgConservationLawMaxwellExplicit::addToTagList(const std::string tag) {
  if (isInTagList(tag))
    return 0; // 'tag' is already in 'tagList'
  else {
    _tagList.push_back(tag);
    return 1; // 'tag' is added in 'tagList'
  }
}

int dgConservationLawMaxwellExplicit::isInTagList(const std::string tag) {
  for(std::vector<std::string>::iterator it=_tagList.begin(); it!=_tagList.end(); ++it)
    if (*it == tag)
      return 1; // 'tag' is in 'tagList'
  return 0;     // 'tag' is not in 'tagList'
}

const function *dgConservationLawMaxwellExplicit::getFunctionForTag2(const termMap &map, const std::string tag) const {
  termMap::const_iterator it = map.find(tag);
  if (it == map.end())
    it = map.find("");
  if (it == map.end())
    Msg::Fatal("Dg/Maxwell: a parameter is not defined.");
  return it->second;
}


/*==============================================================================
 * Add a PML
 *============================================================================*/

void dgConservationLawMaxwellExplicit::addPml(const std::string tag) {
  if ((_LayerVersion=="PmlStraight")) {
    if (_EquationVersion=="E") {
      delete _volumeTerm0[tag];
      delete _volumeTerm1[tag];
      delete _interfaceTerm0[tag];
      _volumeTerm0[tag]    = new psiVolumeETermPml (_nbf,getPmlCoef(tag),getPmlDir(tag));
      _volumeTerm1[tag]    = new dPsiVolumeETermPml(_nbf,getH(),getEpsilon(tag),getPmlDir(tag));
      _interfaceTerm0[tag] = new interfaceETermPml (_nbf,getH(),getEpsilon(tag),getMu(tag),getPmlDir(tag));
    }
    else if (_EquationVersion=="H") {
      delete _volumeTerm1[tag];
      delete _interfaceTerm0[tag];
      _volumeTerm0[tag]    = new psiVolumeHTermPml (_nbf,getPmlCoef(tag),getPmlDir(tag));
      _volumeTerm1[tag]    = new dPsiVolumeHTermPml(_nbf,getE(),getMu(tag),getPmlDir(tag));
      _interfaceTerm0[tag] = new interfaceHTermPml (_nbf,getE(),getEpsilon(tag),getMu(tag),getPmlDir(tag));
    }
    else
      Msg::Fatal("Dg/MaxwellExplicit: bad EquationVersion (not '%s')", _EquationVersion.c_str());
  }
  else
    Msg::Fatal("Dg/MaxwellExplicit: bad LayerVersion (not '%s')", _LayerVersion.c_str());
}

void dgConservationLawMaxwellExplicit::addInterfacePml(const std::string tag) {
  if ((_LayerVersion=="PmlStraight")) {
    if (_EquationVersion=="E") {
      delete _interfaceTerm0[tag];
      _interfaceTerm0[tag] = new interfaceETermPml(_nbf,getH(),getEpsilon(tag),getMu(tag),getPmlDir(tag));
    }
    else if (_EquationVersion=="H") {
      delete _interfaceTerm0[tag];
      _interfaceTerm0[tag] = new interfaceHTermPml(_nbf,getE(),getEpsilon(tag),getMu(tag),getPmlDir(tag));
    }
    else
      Msg::Fatal("Dg/MaxwellExplicit: bad EquationVersion (not '%s')", _EquationVersion.c_str());
  }
  else
    Msg::Fatal("Dg/MaxwellExplicit: bad LayerVersion (not '%s')", _LayerVersion.c_str());
}


/*==============================================================================
 * Interface condition
 *============================================================================*/

class dgConservationLawMaxwellExplicit::interfaceETermPmlWithIncident : public function {
  int _nbf, _incidentFieldsDefined;
  fullMatrix<double> normals;
  fullMatrix<double> solH_L, epsilonL, muL, pmlDirL, incidentFieldsL;
  fullMatrix<double> solH_R, epsilonR, muR, pmlDirR, incidentFieldsR;
 public:
   interfaceETermPmlWithIncident(int nbf,
                                 const function *H,
                                 const function *epsilonFunction,
                                 const function *muFunction,
                                 const function *pmlDirFunction,
                                 const function *incidentFieldsFunction) : function (nbf*2) {
    _nbf = nbf;
    setArgument (normals, function::getNormals(), 0);
    setArgument (solH_L, H, 0);
    setArgument (solH_R, H, 1);
    setArgument (epsilonL, epsilonFunction, 0);
    setArgument (epsilonR, epsilonFunction, 1);
    setArgument (muL, muFunction, 0);
    setArgument (muR, muFunction, 1);
    setArgument (pmlDirL, pmlDirFunction, 0);
    setArgument (pmlDirR, pmlDirFunction, 1);
    _incidentFieldsDefined = 0;
    if (incidentFieldsFunction!=NULL) {
      setArgument (incidentFieldsL, incidentFieldsFunction, 0);
      setArgument (incidentFieldsR, incidentFieldsFunction, 1);
      _incidentFieldsDefined = 1;
    }
    
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for(size_t i=0; i<nQP; i++) {
      
      double n[3] = {normals(i,0), normals(i,1), normals(i,2)};
      
      // Pml-Directions
      double aL[3], t1L[3], t2L[3];
      double aR[3], t1R[3], t2R[3];
      for (int j=0; j<3; j++) {
        aL[j]  = pmlDirL(i,j); t1L[j] = pmlDirL(i,j+3); t2L[j] = pmlDirL(i,j+6);
        aR[j]  = pmlDirR(i,j); t1R[j] = pmlDirR(i,j+3); t2R[j] = pmlDirR(i,j+6);
      }
      double ndotaL = scalProd(n,aL);
      double ndotaR = scalProd(n,aR);
      
      // Physical parameters
      double epsilonVecL[3], muVecL[3];
      double epsilonVecR[3], muVecR[3];
      for (int j=0; j<3; j++) {
        epsilonVecL[j] = epsilonL(i,j); muVecL[j] = muL(i,j);
        epsilonVecR[j] = epsilonR(i,j); muVecR[j] = muR(i,j);
      }
      double epsilonValL=0., muValL=0.;
      double epsilonValR=0., muValR=0.;
      for (int j=0; j<3; j++) {
        epsilonValL += pow(n[j],2)*epsilonVecL[j];
        epsilonValR += pow(n[j],2)*epsilonVecR[j];
        muValL      += pow(n[j],2)*muVecL[j];
        muValR      += pow(n[j],2)*muVecR[j];
      }
      
      // Fields
      double hL[3], hR[3];
      for (int j=0; j<3; j++) {
        hL[j] = solH_L(i,j);
        hR[j] = solH_R(i,j);
      }
      double hIncL[3] = {0.,0.,0.}, hIncR[3] = {0.,0.,0.};
      if (_incidentFieldsDefined)
        for (int j=0; j<3; j++) {
          hIncL[j] = incidentFieldsL(i,j+3);
          hIncR[j] = incidentFieldsR(i,j+3);
        }
      
      // Data at the interface  ---   AM: Qque chose de pas clair
      double a[3], hInc[3];
      if (norm3(aL)>0.5)
        for (int j=0; j<3; j++) {
          a[j] = aL[j];
          hInc[j] = hIncL[j];
        }
      else
        for (int j=0; j<3; j++) {
          a[j] = aR[j];
          hInc[j] = hIncR[j];
        }
      
      // Mean fields
      double hMeanL[3], hMeanR[3];
      if (scalProd(n,a)>0.5)  // (L=domain, R=pml)
        for (int j=0; j<3; j++) { 
          hMeanL[j] = ((hR[j]+hInc[j]) + hL[j]) / 2.; // With total fields
          hMeanR[j] = (hR[j] + (hL[j]-hInc[j])) / 2.; // With scattered fields
        }
      else  // (L=pml, R=domain)
        for (int j=0; j<3; j++) {
          hMeanL[j] = ((hR[j]-hInc[j]) + hL[j]) / 2.; // With scattered fields
          hMeanR[j] = (hR[j] + (hL[j]+hInc[j])) / 2.; // With total fields
        }
      /*
      if (scalProd(n,a)>0.5)  // (L=domain, R=pml)
        for (int j=0; j<3; j++) { 
          hMeanL[j] = ((ZR*(hR[j]+hInc[j]) + ZL*hL[j])/2.) / ZMean; // With total fields
          hMeanR[j] = ((ZR*hR[j] + ZL*(hL[j]-hInc[j]))/2.) / ZMean; // With scattered fields
        }
      else  // (L=pml, R=domain)
        for (int j=0; j<3; j++) {
          hMeanL[j] = ((ZR*(hR[j]-hInc[j]) + ZL*hL[j])/2.) / ZMean; // With scattered fields
          hMeanR[j] = ((ZR*hR[j] + ZL*(hL[j]+hInc[j]))/2.) / ZMean; // With total fields
        }
      */
      
      
      // Fluxes
      for (int j=0; j<3; j++) {
        val(i,j)      =  crossProd(n,hMeanL,j)/epsilonVecL[j];
        val(i,_nbf+j) = -crossProd(n,hMeanR,j)/epsilonVecR[j];
      }
      
      // Additionnal fluxes
      double aLcrosshL_epsilonL[3];
      double aRcrosshR_epsilonR[3];
      for (int j=0; j<3; j++) {
        aLcrosshL_epsilonL[j] = crossProd(aL,hMeanL,j)/epsilonVecL[j];
        aRcrosshR_epsilonR[j] = crossProd(aR,hMeanR,j)/epsilonVecR[j];
      }
      val(i,3)      = ndotaL*scalProd(t1L,aLcrosshL_epsilonL);
      val(i,4)      = ndotaL*scalProd(t2L,aLcrosshL_epsilonL);
      val(i,_nbf+3) =-ndotaR*scalProd(t1R,aRcrosshR_epsilonR);
      val(i,_nbf+4) =-ndotaR*scalProd(t2R,aRcrosshR_epsilonR);
      
    }
  }
};

class dgConservationLawMaxwellExplicit::interfaceHTermPmlWithIncident : public function {
  int _nbf, _incidentFieldsDefined;
  fullMatrix<double> normals;
  fullMatrix<double> solE_L, epsilonL, muL, pmlDirL, incidentFieldsL;
  fullMatrix<double> solE_R, epsilonR, muR, pmlDirR, incidentFieldsR;
 public:
   interfaceHTermPmlWithIncident(int nbf,
                                 const function *E,
                                 const function *epsilonFunction,
                                 const function *muFunction,
                                 const function *pmlDirFunction,
                                 const function *incidentFieldsFunction) : function (nbf*2) {
    _nbf = nbf;
    setArgument (normals, function::getNormals(), 0);
    setArgument (solE_L, E, 0);
    setArgument (solE_R, E, 1);
    setArgument (epsilonL, epsilonFunction, 0);
    setArgument (epsilonR, epsilonFunction, 1);
    setArgument (muL, muFunction, 0);
    setArgument (muR, muFunction, 1);
    setArgument (pmlDirL, pmlDirFunction, 0);
    setArgument (pmlDirR, pmlDirFunction, 1);
    _incidentFieldsDefined = 0;
    if (incidentFieldsFunction!=NULL) {
      setArgument (incidentFieldsL, incidentFieldsFunction, 0);
      setArgument (incidentFieldsR, incidentFieldsFunction, 1);
      _incidentFieldsDefined = 1;
    }
    
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      double n[3] = {normals(i,0), normals(i,1), normals(i,2)};
      
      // Pml-Directions
      double aL[3], t1L[3], t2L[3];
      double aR[3], t1R[3], t2R[3];
      for (int j=0; j<3; j++) {
        aL[j] = pmlDirL(i,j); t1L[j] = pmlDirL(i,j+3); t2L[j] = pmlDirL(i,j+6);
        aR[j] = pmlDirR(i,j); t1R[j] = pmlDirR(i,j+3); t2R[j] = pmlDirR(i,j+6);
      }
      double ndotaL = scalProd(n,aL);
      double ndotaR = scalProd(n,aR);
      
      // Physical parameters
      double epsilonVecL[3], muVecL[3];
      double epsilonVecR[3], muVecR[3];
      for (int j=0; j<3; j++) {
        epsilonVecL[j] = epsilonL(i,j); muVecL[j] = muL(i,j);
        epsilonVecR[j] = epsilonR(i,j); muVecR[j] = muR(i,j);
      }
      double epsilonValL=0., muValL=0.;
      double epsilonValR=0., muValR=0.;
      for (int j=0; j<3; j++) {
        epsilonValL += pow(n[j],2)*epsilonVecL[j];
        epsilonValR += pow(n[j],2)*epsilonVecR[j];
        muValL      += pow(n[j],2)*muVecL[j];
        muValR      += pow(n[j],2)*muVecR[j];
      }
      
      // Fields
      double eL[3], eR[3];
      for (int j=0; j<3; j++) {
        eL[j] = solE_L(i,j);
        eR[j] = solE_R(i,j);
      }
      double eIncL[3] = {0.,0.,0.}, eIncR[3] = {0.,0.,0.};
      if (_incidentFieldsDefined)
        for (int j=0; j<3; j++) {
          eIncL[j] = incidentFieldsL(i,j);
          eIncR[j] = incidentFieldsR(i,j);
        }
      
      // Data at the interface  ---   AM: Qque chose de pas clair
      double a[3], eInc[3];
      if (norm3(aL)>0.5)
        for (int j=0; j<3; j++) {
          a[j] = aL[j];
          eInc[j] = eIncL[j];
        }
      else
        for (int j=0; j<3; j++) {
          a[j] = aR[j];
          eInc[j] = eIncR[j];
        }
      
      // Riemann Solver
      double eMeanL[3], eMeanR[3];
      if (scalProd(n,a)>0.5)  // (L=domain, R=pml)
        for (int j=0; j<3; j++) {
          eMeanL[j] = ((eR[j]+eInc[j]) + eL[j]) / 2.; // With total fields
          eMeanR[j] = (eR[j] + (eL[j]-eInc[j])) / 2.; // With scattered fields
        }
      else  // (L=pml, R=domain)
        for (int j=0; j<3; j++) { 
          eMeanL[j] = ((eR[j]-eInc[j]) + eL[j]) / 2.; // With scattered fields
          eMeanR[j] = (eR[j] + (eL[j]+eInc[j])) / 2.; // With total fields
        }
      /*
      if (scalProd(n,a)>0.5)  // (L=domain, R=pml)
        for (int j=0; j<3; j++) {
          eMeanL[j] = ((YR*(eR[j]+eInc[j]) + YL*eL[j])/2.) / YMean; // With total fields
          eMeanR[j] = ((YR*eR[j] + YL*(eL[j]-eInc[j]))/2.) / YMean; // With scattered fields
        }
      else  // (L=pml, R=domain)
        for (int j=0; j<3; j++) { 
          eMeanL[j] = ((YR*(eR[j]-eInc[j]) + YL*eL[j])/2.) / YMean; // With scattered fields
          eMeanR[j] = ((YR*eR[j] + YL*(eL[j]+eInc[j]))/2.) / YMean; // With total fields
        }
      */
      
      // Fluxes
      for (int j=0; j<3; j++) {
        val(i,j)      = -crossProd(n,eMeanL,j)/muVecL[j];
        val(i,_nbf+j) =  crossProd(n,eMeanR,j)/muVecR[j];
      }
      
      // Additionnal fluxes
      double aLcrosseL_muL[3];
      double aRcrosseR_muR[3];
      for (int j=0; j<3; j++) {
        aLcrosseL_muL[j] = crossProd(aL,eMeanL,j)/muVecL[j];
        aRcrosseR_muR[j] = crossProd(aR,eMeanR,j)/muVecR[j];
      }
      val(i,3)      =-ndotaL*scalProd(t1L,aLcrosseL_muL);
      val(i,4)      =-ndotaL*scalProd(t2L,aLcrosseL_muL);
      val(i,_nbf+3) = ndotaR*scalProd(t1R,aRcrosseR_muR);
      val(i,_nbf+4) = ndotaR*scalProd(t2R,aRcrosseR_muR);
      
    }
  }
};

void dgConservationLawMaxwellExplicit::addInterfacePmlWithIncident(const std::string tag, const function *incidentFieldsFunction) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  if (_EquationVersion=="E")
    _interfaceTerm0[tag] = new interfaceETermPmlWithIncident(_nbf,getH(),getEpsilon(tag),getMu(tag),getPmlDir(tag),incidentFieldsFunction);
  else if (_EquationVersion=="H")
    _interfaceTerm0[tag] = new interfaceHTermPmlWithIncident(_nbf,getE(),getEpsilon(tag),getMu(tag),getPmlDir(tag),incidentFieldsFunction);
  else
    Msg::Fatal("Dg/MaxwellExplicit: bad EquationVersion (not '%s')", _EquationVersion.c_str());
}


/*==============================================================================
 * Specific boundary conditions
 *============================================================================*/

class dgBoundaryConditionMaxwellE : public dgBoundaryCondition {
  class term : public function {
    int  _typeBC;
    std::string _LayerVersion;
    fullMatrix<double> normals, solE, solH, epsilon, mu,pmlDir, extData;    
  public:
    term(int nbf, int typeBC, std::string LayerVersion,
         const function *H,
         const function *epsilonFunction,
         const function *muFunction,
         const function *pmlDirFunction,
         const function *extDataFunction) : function(nbf) {
      _typeBC = typeBC;
      _LayerVersion = LayerVersion;
      setArgument (solH, H);
      setArgument (normals, function::getNormals());
      setArgument (solE, function::getSolution());
      setArgument (epsilon, epsilonFunction);
      setArgument (mu, muFunction);
      if (_LayerVersion != "WithoutLayer")
        setArgument (pmlDir, pmlDirFunction);
      if (extDataFunction != NULL)
        setArgument (extData, extDataFunction);
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) { 
      val.scale(0.);
      size_t nQP = val.size1();
      if (_typeBC) // _typeBC=0 : Symmetry condition (all fluxes are avoided)
	for (size_t i=0; i< nQP; i++) {
        
	  double n[3] = {normals(i,0), normals(i,1), normals(i,2)};
        
	  // Physical parameters
        
	  double epsilonVec[3], muVec[3];
	  for (int j=0; j<3; j++) {
	    epsilonVec[j] = epsilon(i,j);
	    muVec[j]      = mu(i,j);
	  }
        
	  double epsilonVal=0., muVal=0.;
	  for (int j=0; j<3; j++) {
	    epsilonVal += pow(n[j],2)*epsilonVec[j];
	    muVal      += pow(n[j],2)*muVec[j];
	  }
        
	  double Z = pow(muVal/epsilonVal,0.5);
        
	  double hMean[3] = {0., 0., 0.}, hRiemann[3];
	  double eJump[3]={0.,0.,0.}, ncrosseJump[3];
	  switch (_typeBC) {
	    // Dirichlet on 'e'
          case 1:
            for (int j=0; j<3; j++)
              hMean[j] = solH(i,j);
            break;
	    // Homogeneous Dirichlet on 'e' (Electric wall)
          case 2:
            for (int j=0; j<3; j++)
              hMean[j] = solH(i,j);
            break;
	    // Dirichlet on 'h'
          case 3:
            for (int j=0; j<3; j++)
              hMean[j] = extData(i,j);
            break;
	    // Homogeneous Dirichlet on 'h' (Magnetic wall)
          case 4:
            for (int j=0; j<3; j++)
              hMean[j] = 0.;
            break;
	    // Silver Muller
          case 5:
            for (int j=0; j<3; j++){
              eJump[j] = -solE(i,j);
              hMean[j] = 0;
	    }
            break;
	    // Silver Muller Dirichlet On H
          case 6:
            for (int j=0; j<3; j++){
              eJump[j] = -solE(i,j);
              hMean[j] = extData(i,j)+solH(i,j);
	    }
            break;
	    // Silver Muller Dirichlet On E
          case 7:
            for (int j=0; j<3; j++){
              eJump[j] = (extData(i,j)-solE(i,j));
              hMean[j] = solH(i,j);
	    }
            break;
          default:
	    Msg::Error("Dg/MaxwellExplicit: Bad boundary condition in conservation law (BAD '_typeBC': '%i')", _typeBC);
	  }
        
	  for (int j=0; j<3; j++)
	    ncrosseJump[j] = crossProd(n,eJump,j);

	  for (int j=0; j<3; j++)
	    hRiemann[j] = hMean[j] - ncrosseJump[j]/Z;

	  for (int j=0; j<3; j++)
	    val(i,j) = crossProd(n,hRiemann,j)/epsilonVec[j];
        
	  // Additionnal fields
	  if (_LayerVersion == "StraightPml") {
	    double a[3]  = {pmlDir(i,0), pmlDir(i,1), pmlDir(i,2)};
	    double t1[3] = {pmlDir(i,3), pmlDir(i,4), pmlDir(i,5)};
	    double t2[3] = {pmlDir(i,6), pmlDir(i,7), pmlDir(i,8)};
	    double ndota = scalProd(n,a);
	    if (fabs(ndota)>0.99)
	      for (int j=0; j<3; j++) {
		val(i,3) += t1[j]*val(i,j);
		val(i,4) += t2[j]*val(i,j);
	      }
	  }
        
	}
    }
  };
  term _term;
public:
  dgBoundaryConditionMaxwellE(dgConservationLawMaxwellExplicit *claw, const std::string tag, int typeBC, const function *extDataFunction) :
    _term(claw->getNbFields(), typeBC, claw->getLayerVersion(),
	  claw->getH(),
	  claw->getEpsilon(),
	  claw->getMu(),
	  claw->getPmlDir(tag),
	  extDataFunction) {
    _term0 = &_term;
  }
};



/*
class dgBoundaryConditionMaxwellH : public dgBoundaryCondition {
  class term : public function {
    int _typeBC;
    std::string _LayerVersion;
    fullMatrix<double> normals, solE, mu, pmlDir, extData;    
   public:
    term(int nbf, int typeBC, std::string LayerVersion,
         const function *E,
         const function *muFunction,
         const function *pmlDirFunction,
         const function *extDataFunction) : function(nbf) {
      _typeBC = typeBC;
      _LayerVersion = LayerVersion;
      setArgument (solE, E);
      setArgument (normals, function::getNormals());
      setArgument (mu, muFunction);
      if (_LayerVersion != "WithoutLayer")
        setArgument (pmlDir, pmlDirFunction);
      if (extDataFunction != NULL)
        setArgument (extData, extDataFunction);
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) { 
      val.scale(0.);
      size_t nQP = val.size1();
      if (_typeBC) // _typeBC=0 : Symmetry condition (all fluxes are avoided)
      for (size_t i=0; i< nQP; i++) {
        
        double n[3] = {normals(i,0), normals(i,1), normals(i,2)};
        
        double eMean[3] = {0., 0., 0.};
        switch (_typeBC) {
          // Dirichlet on 'e'
          case 1:
            for (int j=0; j<3; j++)
              eMean[j] = extData(i,j);
            break;
          // Homogeneous Dirichlet on 'e' (Electric wall)
          case 2:
            for (int j=0; j<3; j++)
              eMean[j] = 0.;
            break;
          // Dirichlet on 'h'
          case 3:
            for (int j=0; j<3; j++)
              eMean[j] = solE(i,j);
            break;
          // Homogeneous Dirichlet on 'h' (Magnetic wall)
          case 4:
            for (int j=0; j<3; j++)
              eMean[j] = solE(i,j);
            break;
          default:
            Msg::Error("Dg/Maxwell: Bad boundary condition in conservation law (BAD '_typeBC': '%i')", _typeBC);
        }
        
        for (int j=0; j<3; j++)
          val(i,j) = -crossProd(n,eMean,j)/mu(i,j);
        
        // Additionnal fields
        if (_LayerVersion == "StraightPml") {
          double a[3]  = {pmlDir(i,0), pmlDir(i,1), pmlDir(i,2)};
          double t1[3] = {pmlDir(i,3), pmlDir(i,4), pmlDir(i,5)};
          double t2[3] = {pmlDir(i,6), pmlDir(i,7), pmlDir(i,8)};
          double ndota = scalProd(n,a);
          if (fabs(ndota)>0.99)
            for (int j=0; j<3; j++) {
              val(i,3) += t1[j]*val(i,j);
              val(i,4) += t2[j]*val(i,j);
            }
        }
        
      }
    }
  };
  term _term;
 public:
  dgBoundaryConditionMaxwellH(dgConservationLawMaxwellExplicit *claw, const std::string tag, int typeBC, const function *extDataFunction) :
      _term(claw->getNbFields(), typeBC, claw->getLayerVersion(),
            claw->getE(),
            claw->getMu(),
            claw->getPmlDir(tag),
            extDataFunction) {
    _term0 = &_term;
  }
};*/


class dgBoundaryConditionMaxwellH : public dgBoundaryCondition {
  class term : public function {
    int _typeBC;
    std::string _LayerVersion;
    fullMatrix<double> normals, solH,solE, epsilon,mu, pmlDir, extData;    
  public:
    term(int nbf, int typeBC, std::string LayerVersion,
         const function *E,
         const function *epsilonFunction,
         const function *muFunction,
         const function *pmlDirFunction,
         const function *extDataFunction) : function(nbf) {
      _typeBC = typeBC;
      _LayerVersion = LayerVersion;
      setArgument (solE, E);
      setArgument (normals, function::getNormals());
      setArgument (solH, function::getSolution());
      setArgument (mu, muFunction);
      setArgument (epsilon, epsilonFunction);
      if (_LayerVersion != "WithoutLayer")
        setArgument (pmlDir, pmlDirFunction);
      if (extDataFunction != NULL)
        setArgument (extData, extDataFunction);
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) { 
      val.scale(0.);
      size_t nQP = val.size1();
      if (_typeBC) // _typeBC=0 : Symmetry condition (all fluxes are avoided)
	for (size_t i=0; i< nQP; i++) {
        
	  double n[3] = {normals(i,0), normals(i,1), normals(i,2)};
	  // Physical parameters
        
	  double epsilonVec[3], muVec[3];
	  for (int j=0; j<3; j++) {
	    epsilonVec[j] = epsilon(i,j);
	    muVec[j]      = mu(i,j);
	  }
        
	  double epsilonVal=0., muVal=0.;
	  for (int j=0; j<3; j++) {
	    epsilonVal += pow(n[j],2)*epsilonVec[j];
	    muVal      += pow(n[j],2)*muVec[j];
	  }
        
	  double Y = pow(muVal/epsilonVal,-0.5);

	  double eMean[3] = {0., 0., 0.}, eRiemann[3];
	  double hJump[3]={0.,0.,0.}, ncrosshJump[3];
	  switch (_typeBC) {
	    // Dirichlet on 'e'
          case 1:
            for (int j=0; j<3; j++)
              eMean[j] = extData(i,j);
            break;
	    // Homogeneous Dirichlet on 'e' (Electric wall)
          case 2:
            for (int j=0; j<3; j++)
              eMean[j] = 0.;
            break;
	    // Dirichlet on 'h'
          case 3:
            for (int j=0; j<3; j++)
              eMean[j] = solE(i,j);
            break;
	    // Homogeneous Dirichlet on 'h' (Magnetic wall)
          case 4:
            for (int j=0; j<3; j++)
              eMean[j] = solE(i,j);
            break;
	    // Silver Muller
          case 5:
            for (int j=0; j<3; j++){
              eMean[j] = 0;
	      hJump[j] = -solH(i,j);
	    }
            break;

	    // Silver Muller Dirichlet On H
          case 6:
            for (int j=0; j<3; j++){
              eMean[j] = solE(i,j);
	      hJump[j] = extData(i,j)-solH(i,j);
	    }
            break; 
	    // Silver Muller Dirichlet On E
          case 7:
            for (int j=0; j<3; j++){
              eMean[j] = (solE(i,j)+extData(i,j));
	      hJump[j] = -solH(i,j);
	    }
            break; 
          default:
	    Msg::Error("Dg/Maxwell: Bad boundary condition in conservation law (BAD '_typeBC': '%i')", _typeBC);
	  }
	  for (int j=0; j<3; j++)
	    ncrosshJump[j] = crossProd(n,hJump,j);

	  for (int j=0; j<3; j++)
	    eRiemann[j] = eMean[j] + ncrosshJump[j]/Y;

	  for (int j=0; j<3; j++)
	    val(i,j) = -crossProd(n,eRiemann,j)/muVec[j];
        
	  // Additionnal fields
	  if (_LayerVersion == "StraightPml") {
	    double a[3]  = {pmlDir(i,0), pmlDir(i,1), pmlDir(i,2)};
	    double t1[3] = {pmlDir(i,3), pmlDir(i,4), pmlDir(i,5)};
	    double t2[3] = {pmlDir(i,6), pmlDir(i,7), pmlDir(i,8)};
	    double ndota = scalProd(n,a);
	    if (fabs(ndota)>0.99)
	      for (int j=0; j<3; j++) {
		val(i,3) += t1[j]*val(i,j);
		val(i,4) += t2[j]*val(i,j);
	      }
	  }
        
	}
    }
  };
  term _term;
public:
  dgBoundaryConditionMaxwellH(dgConservationLawMaxwellExplicit *claw, const std::string tag, int typeBC, const function *extDataFunction) :
    _term(claw->getNbFields(), typeBC, claw->getLayerVersion(),
	  claw->getE(),
	  claw->getEpsilon(),
	  claw->getMu(),
	  claw->getPmlDir(tag),
	  extDataFunction) {
    _term0 = &_term;
  }
};





dgBoundaryCondition *dgConservationLawMaxwellExplicit::newBoundarySymmetry(const std::string tag) {
  if (_EquationVersion=="E")
    return new dgBoundaryConditionMaxwellE(this,tag,0,NULL);
  else if (_EquationVersion=="H")
    return new dgBoundaryConditionMaxwellH(this,tag,0,NULL);
  return NULL;
}

dgBoundaryCondition *dgConservationLawMaxwellExplicit::newBoundaryDirichletOnE(const std::string tag, const function *eExtFunction) {
  if (_EquationVersion=="E")
    return new dgBoundaryConditionMaxwellE(this,tag,1,eExtFunction);
  else if (_EquationVersion=="H")
    return new dgBoundaryConditionMaxwellH(this,tag,1,eExtFunction);
  return NULL;
}

dgBoundaryCondition *dgConservationLawMaxwellExplicit::newBoundaryHomogeneousDirichletOnE(const std::string tag) {
  if (_EquationVersion=="E")
    return new dgBoundaryConditionMaxwellE(this,tag,2,NULL);
  else if (_EquationVersion=="H")
    return new dgBoundaryConditionMaxwellH(this,tag,2,NULL);
  return NULL;
}

dgBoundaryCondition *dgConservationLawMaxwellExplicit::newBoundaryElectricWall(const std::string tag) {
  if (_EquationVersion=="E")
    return new dgBoundaryConditionMaxwellE(this,tag,2,NULL);
  else if (_EquationVersion=="H")
    return new dgBoundaryConditionMaxwellH(this,tag,2,NULL);
  return NULL;
}

dgBoundaryCondition *dgConservationLawMaxwellExplicit::newBoundaryDirichletOnH(const std::string tag, const function *hExtFunction) {
  if (_EquationVersion=="E")
    return new dgBoundaryConditionMaxwellE(this,tag,3,hExtFunction);
  else if (_EquationVersion=="H")
    return new dgBoundaryConditionMaxwellH(this,tag,3,hExtFunction);
  return NULL;
}

dgBoundaryCondition *dgConservationLawMaxwellExplicit::newBoundaryHomogeneousDirichletOnH(const std::string tag) {
  if (_EquationVersion=="E")
    return new dgBoundaryConditionMaxwellE(this,tag,4,NULL);
  else if (_EquationVersion=="H")
    return new dgBoundaryConditionMaxwellH(this,tag,4,NULL);
  return NULL;
}

dgBoundaryCondition *dgConservationLawMaxwellExplicit::newBoundaryMagneticWall(const std::string tag) {
  if (_EquationVersion=="E")
    return new dgBoundaryConditionMaxwellE(this,tag,4,NULL);
  else if (_EquationVersion=="H")
    return new dgBoundaryConditionMaxwellH(this,tag,4,NULL);
  return NULL;
}

dgBoundaryCondition *dgConservationLawMaxwellExplicit::newBoundarySilverMuller(const std::string tag) {
  if (_EquationVersion=="E")
    return new dgBoundaryConditionMaxwellE(this,tag,5,NULL);
  else if (_EquationVersion=="H")
    return new dgBoundaryConditionMaxwellH(this,tag,5,NULL);
  return NULL;
}


dgBoundaryCondition *dgConservationLawMaxwellExplicit::newBoundarySilverMullerDirichletOnH(const std::string tag, const function *hExtFunction) {
  if (_EquationVersion=="E")
    return new dgBoundaryConditionMaxwellE(this,tag,6,hExtFunction);
  else if (_EquationVersion=="H")
    return new dgBoundaryConditionMaxwellH(this,tag,6,hExtFunction);
  return NULL;
}

dgBoundaryCondition *dgConservationLawMaxwellExplicit::newBoundarySilverMullerDirichletOnE(const std::string tag, const function *eExtFunction) {
  if (_EquationVersion=="E")
    return new dgBoundaryConditionMaxwellE(this,tag,7,eExtFunction);
  else if (_EquationVersion=="H")
    return new dgBoundaryConditionMaxwellH(this,tag,7,eExtFunction);
  return NULL;
}


/********************************************************************************/
/* Interface term for uncoupled regions*/
/*******************************************************************************/


class dgConservationLawMaxwellExplicit::interfaceHTermUncoupledRegions : public function {
  int _nbf,_typeBC;
  fullMatrix<double> normals;
  fullMatrix<double> solH_L, solE_L, epsilonL, muL, extDataL;
  fullMatrix<double> solH_R, solE_R, epsilonR, muR, extDataR;
public:
  interfaceHTermUncoupledRegions(int nbf,
				 const function *solE_Function,
                                 const function *epsilonFunction,
                                 const function *muFunction,
                                 int typeBC,
                                 const function *extDataFunction) : function (nbf*2) {
    _nbf = nbf;
    _typeBC=typeBC;
    setArgument (normals, function::getNormals(), 0);
    setArgument (solH_L, function::getSolution(), 0);
    setArgument (solH_R, function::getSolution(), 1);
    setArgument (solE_L, solE_Function, 0);
    setArgument (solE_R, solE_Function, 1);
    setArgument (epsilonL, epsilonFunction, 0);
    setArgument (epsilonR, epsilonFunction, 1);
    setArgument (muL, muFunction, 0);
    setArgument (muR, muFunction, 1);
    if (extDataFunction != NULL) {
      setArgument (extDataL, extDataFunction, 1);
      setArgument (extDataR, extDataFunction, 0);
    }
    
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      double n[3] = {normals(i,0), normals(i,1), normals(i,2)};
      
      // Physical parameters
      double epsilonVecL[3], muVecL[3];
      double epsilonVecR[3], muVecR[3];
      for (int j=0; j<3; j++) {
        epsilonVecL[j] = epsilonL(i,j); muVecL[j] = muL(i,j);
        epsilonVecR[j] = epsilonR(i,j); muVecR[j] = muR(i,j);
      }
      double epsilonValL=0., muValL=0.;
      double epsilonValR=0., muValR=0.;
      for (int j=0; j<3; j++) {
        epsilonValL += pow(n[j],2)*epsilonVecL[j];
        epsilonValR += pow(n[j],2)*epsilonVecR[j];
        muValL      += pow(n[j],2)*muVecL[j];
        muValR      += pow(n[j],2)*muVecR[j];
      }
      double YL = pow(muValL/epsilonValL,-0.5);
      double YR = pow(muValR/epsilonValR,-0.5);
      
      // Fields
      double eL[3], eR[3], hL[3], hR[3];
      for (int j=0; j<3; j++){
        eL[j] = solE_L(i,j);
        eR[j] = solE_R(i,j);  
        hL[j] = solH_L(i,j);
        hR[j] = solH_R(i,j);
      }

      double eMeanL[3]={0,0,0}, eMeanR[3]={0,0,0};
      double hJumpL[3]={0,0,0}, hJumpR[3]={0,0,0};
      double ncrosshJumpL[3]={0,0,0};
      double ncrosshJumpR[3]={0,0,0};
      double eRiemannL[3], eRiemannR[3];
      // Riemann Solver
      switch (_typeBC) {
          
	// Homogeneous Dirichlet on 'e' (Electric wall)
      case 1:
	for (int j=0; j<3; j++){
	  eMeanR[j] = 0*2*eR[j]/2.;
	  eMeanL[j] = 0*2*eL[j]/2.;
	  // hJumpR[j] = 0.;
	  // hJumpL[j] = 0.;
	}
	break;
          
	// Homogeneous Dirichlet on 'h' (Magnetic wall)
      case 2:
	for (int j=0; j<3; j++){
	  eMeanR[j] = eR[j];
	  hJumpR[j] = hR[j]/2.;
	  eMeanL[j] = eL[j];
	  hJumpL[j] = -hL[j]/2.;
	}
	break;
          
      case 3:
	for (int j=0; j<3; j++){
	  eMeanR[j] = eR[j]/2.;
	  eMeanL[j] = eL[j]/2.;
	  hJumpR[j] = hR[j]/2.;
	  hJumpL[j] = -hL[j]/2.;
	}
            
	break;
          
      case 8:
	for (int j=0; j<3; j++) {
	  eMeanR[j] = (extDataL(i,j)+eR[j])/2.;
	  eMeanL[j] = (extDataR(i,j)+eL[j])/2.;
	  hJumpR[j] = (-extDataL(i,j+3)+hR[j])/2.;
	  hJumpL[j] = (extDataR(i,j+3)-hL[j])/2.;
	}
	break;
          
      default:
	Msg::Error("Dg/Maxwell: Bad boundary condition in conservation law (BAD '_typeBC': '%i')", _typeBC);
      }
        
      for (int j=0; j<3; j++) {
	   
	ncrosshJumpL[j] = crossProd(n,hJumpL,j); 
	ncrosshJumpR[j] = crossProd(n,hJumpR,j);
      }
        
      for (int j=0; j<3; j++) {
	eRiemannL[j] = eMeanL[j] + ncrosshJumpL[j]/YL;
	eRiemannR[j] = eMeanR[j] + ncrosshJumpR[j]/YR;
      }
        
      for (int j=0; j<3; j++) {
        // Fluxes for the left
        val(i,j) = -crossProd(n,eRiemannL,j)/muVecL[j];
        // Fluxes for the right
        val(i,_nbf+j) =  crossProd(n,eRiemannR,j)/muVecR[j];
      }
    }
  }
};





class dgConservationLawMaxwellExplicit::interfaceETermUncoupledRegions : public function {
  int _nbf,_typeBC;
  fullMatrix<double> normals;
  fullMatrix<double> solE_L, solH_L, epsilonL, muL, extDataL;
  fullMatrix<double> solE_R, solH_R, epsilonR, muR, extDataR;
public:
  interfaceETermUncoupledRegions(int nbf,
				 const function *solH_Function,
                                 const function *epsilonFunction,
                                 const function *muFunction,
                                 int typeBC,
                                 const function *extDataFunction) : function (nbf*2) {
    _nbf = nbf;
    _typeBC=typeBC;
    setArgument (normals, function::getNormals(), 0);
    setArgument (solE_L, function::getSolution(), 0);
    setArgument (solE_R, function::getSolution(), 1);
    setArgument (solH_L, solH_Function, 0);
    setArgument (solH_R, solH_Function, 1);
    setArgument (epsilonL, epsilonFunction, 0);
    setArgument (epsilonR, epsilonFunction, 1);
    setArgument (muL, muFunction, 0);
    setArgument (muR, muFunction, 1);
    if (extDataFunction != NULL) {
      setArgument (extDataL, extDataFunction, 1);
      setArgument (extDataR, extDataFunction, 0);
    }
    
  };
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++) {
      
      double n[3] = {normals(i,0), normals(i,1), normals(i,2)};
      
      // Physical parameters
      double epsilonVecL[3], muVecL[3];
      double epsilonVecR[3], muVecR[3];
      for (int j=0; j<3; j++) {
        epsilonVecL[j] = epsilonL(i,j); muVecL[j] = muL(i,j);
        epsilonVecR[j] = epsilonR(i,j); muVecR[j] = muR(i,j);
      }
      double epsilonValL=0., muValL=0.;
      double epsilonValR=0., muValR=0.;
      for (int j=0; j<3; j++) {
        epsilonValL += pow(n[j],2)*epsilonVecL[j];
        epsilonValR += pow(n[j],2)*epsilonVecR[j];
        muValL      += pow(n[j],2)*muVecL[j];
        muValR      += pow(n[j],2)*muVecR[j];
      }
      double ZL = pow(muValL/epsilonValL,0.5);
      double ZR = pow(muValR/epsilonValR,0.5);
      
      // Fields
      double eL[3], eR[3], hL[3], hR[3];
      for (int j=0; j<3; j++){
        eL[j] = solE_L(i,j);
        eR[j] = solE_R(i,j);
        hL[j] = solH_L(i,j);
        hR[j] = solH_R(i,j);
      }

      double hMeanL[3]={0,0,0}, hMeanR[3]={0,0,0};
      double eJumpL[3]={0,0,0}, eJumpR[3]={0,0,0};
      double ncrosseJumpL[3]={0,0,0};
      double ncrosseJumpR[3]={0,0,0};
      double hRiemannL[3], hRiemannR[3];
      // Riemann Solver
      switch (_typeBC) {
          
	// Homogeneous Dirichlet on 'e' (Electric wall)
      case 1:
	for (int j=0; j<3; j++){
	  eJumpR[j] = 0*eR[j]/2.;
	  eJumpL[j] = -0*eL[j]/2.;
	  hMeanR[j] = hR[j];
	  hMeanL[j] = hL[j];
	}
	break;
          
	// Homogeneous Dirichlet on 'h' (Magnetic wall)
      case 2:
	for (int j=0; j<3; j++){
	  eJumpR[j] = 0.;
	  hMeanR[j] = hR[j]/2.;
	  eJumpL[j] = 0.;
	  hMeanL[j] = hL[j]/2.;
	}
	break;
          
      case 3:
	for (int j=0; j<3; j++){
	  eJumpR[j] = eR[j]/2.;
	  eJumpL[j] = -eL[j]/2.;
	  hMeanR[j] = hR[j]/2.;
	  hMeanL[j] = hL[j]/2.;
	}
            
	break;
          
      case 8:
	for (int j=0; j<3; j++) {
	  hMeanR[j] = (extDataL(i,j+3)+hR[j])/2.;
	  hMeanL[j] = (extDataR(i,j+3)+hL[j])/2.;
	  eJumpR[j] = (-extDataL(i,j)+eR[j])/2.;
	  eJumpL[j] = (extDataR(i,j)-eL[j])/2.;
	}
	break;
          
      default:
	Msg::Error("Dg/Maxwell: Bad boundary condition in conservation law (BAD '_typeBC': '%i')", _typeBC);
      }
      for (int j=0; j<3; j++) {
	ncrosseJumpL[j] = crossProd(n,eJumpL,j);
	ncrosseJumpR[j] = crossProd(n,eJumpR,j);
      }
        
      for (int j=0; j<3; j++) {
	hRiemannL[j] = hMeanL[j] - ncrosseJumpL[j]/ZL; 
	hRiemannR[j] = hMeanR[j] - ncrosseJumpR[j]/ZR; 
      }
        
      for (int j=0; j<3; j++) {
        // Fluxes for the left
        val(i,j)   =  crossProd(n,hRiemannL,j)/epsilonVecL[j];       
        // Fluxes for the right
        val(i,_nbf+j)   = -crossProd(n,hRiemannR,j)/epsilonVecR[j];
      }
    }
  }
};





void dgConservationLawMaxwellExplicit::addInterfaceHomogeneousDirichletOnE(const std::string tag) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  if (_EquationVersion=="E")
    _interfaceTerm0[tag] = new interfaceETermUncoupledRegions(_nbf,getH(),getEpsilon(tag),getMu(tag),1,NULL);
  else if (_EquationVersion=="H")
    _interfaceTerm0[tag] = new interfaceHTermUncoupledRegions(_nbf,getE(),getEpsilon(tag),getMu(tag),1,NULL);
  else
    Msg::Fatal("Dg/MaxwellExplicit: bad EquationVersion (not '%s')", _EquationVersion.c_str());
}


void dgConservationLawMaxwellExplicit::addInterfaceHomogeneousDirichletOnH(const std::string tag) {
  if (_interfaceTerm0[tag]) delete _interfaceTerm0[tag];
  if (_EquationVersion=="E")
    _interfaceTerm0[tag] = new interfaceETermUncoupledRegions(_nbf,getH(),getEpsilon(tag),getMu(tag),2,NULL);
  else if (_EquationVersion=="H")
    _interfaceTerm0[tag] = new interfaceHTermUncoupledRegions(_nbf,getE(),getEpsilon(tag),getMu(tag),2,NULL);
  else
    Msg::Fatal("Dg/MaxwellExplicit: bad EquationVersion (not '%s')", _EquationVersion.c_str());

}



/*==============================================================================
 * Compute information
 *============================================================================*/

class dgConservationLawMaxwellExplicit::electricEnergyFunction : public function {
  fullMatrix<double> solE, epsilon;
 public:
  electricEnergyFunction(function *solutionFunction, const function *epsilonFunction) : function(1) {
    setArgument (solE, solutionFunction);
    setArgument (epsilon, epsilonFunction);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++)
      for (int j=0; j<3; j++)
        val(i,0) += 0.5*epsilon(i,j)*pow(solE(i,j),2);
  }
};

class dgConservationLawMaxwellExplicit::magneticEnergyFunction : public function {
  fullMatrix<double> solH, solHprev, mu;
 public:
  magneticEnergyFunction(function *solutionFunction, function *previousSolutionFunction, const function *muFunction) : function(1) {
    setArgument (solH, solutionFunction);
    setArgument (solHprev, previousSolutionFunction);
    setArgument (mu, muFunction);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    val.scale(0.);
    size_t nQP = val.size1();
    for (size_t i=0; i<nQP; i++)
      for (int j=0; j<3; j++)
        val(i,0) += 0.5*mu(i,j)*solH(i,j)*solHprev(i,j);
  }
};

double dgConservationLawMaxwellExplicit::getElectricEnergy(dgGroupCollection *groups, dgDofContainer *solution, std::string tag) {
  function *_electricEnergyFunction = new electricEnergyFunction(solution->getFunction(),getEpsilon(tag));
  fullMatrix<double> value(1,1);
  dgFunctionIntegrator _totalElectricEnergy(groups, _electricEnergyFunction);
  _totalElectricEnergy.compute(value, tag);
  delete _electricEnergyFunction;
  return value.get(0,0);
}

double dgConservationLawMaxwellExplicit::getMagneticEnergy(dgGroupCollection *groups, dgDofContainer *solution, dgDofContainer *prevSolution, std::string tag) {
  function *_magneticEnergyFunction = new magneticEnergyFunction(solution->getFunction(),prevSolution->getFunction(),getMu(tag));
  fullMatrix<double> value(1,1);
  dgFunctionIntegrator _totalMagneticEnergy(groups, _magneticEnergyFunction);
  _totalMagneticEnergy.compute(value, tag);
  delete _magneticEnergyFunction;
  return value.get(0,0);
}

double dgConservationLawMaxwellExplicit::getVolume(dgGroupCollection *groups, std::string tag) {
  function *_unitFunction = new functionConstant(1.);
  fullMatrix<double> value(1,1);
  dgFunctionIntegrator _totalEnergy(groups, _unitFunction);
  _totalEnergy.compute(value, tag);
  delete _unitFunction;
  return value.get(0,0);
}




