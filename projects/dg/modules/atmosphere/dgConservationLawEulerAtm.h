#ifndef _DG_CONSERVATION_LAW_EULER_ATM_H_
#define _DG_CONSERVATION_LAW_EULER_ATM_H_
#include "dgConservationLawFunction.h"
#include "dgConservationLaw.h"
#include "dgDofContainer.h"
// Compressible euler equations for atmosphere
// (works both in 2D and 3D).
//

enum filterMode {FILTER_NONE, FILTER_LINEAR, FILTER_LINEARVERTICAL};

class activeTerms {
 public:
  double adv_v, adv_t, p_grad, v_div, cor, grav, diff, lax, rhoPot;
  activeTerms(){
    adv_v = true;
    adv_t = true;
    p_grad = true;
    v_div = true;
    cor = true;
    grav = true;
    diff = true;
    lax = true;
    rhoPot = false;
  }
  void setAdvV(bool active){adv_v = active;}
  void setAdvT(bool active){adv_t = active;}
  void setPGrad(bool active){p_grad = active;}
  void setVDiv(bool active){v_div = active;}
  void setCor(bool active){cor = active;}
  void setGrav(bool active){grav = active;}
  void setDiff(bool active){diff = active;}
  void setLax(bool active){lax = active;}
  void setRhoPot(bool active){rhoPot = active;}
};

class dgEulerAtmLaw : public dgConservationLawFunction {
 private:
  activeTerms _trms;
  const function *_nuh, *_nuv, *_gradNuh, *_gradNuv;
  double _H_over_V;
  function *_xyz;
  function *_fcor;
  int _dim, _timeDir;
  double _g,_gamma,_p0,_Rd;
  double _R;
  dgDofContainer *_rhoHs,*_rhoThetaHs;
  filterMode _filterMode;
  imexMode _imexMode;
  bool _isLinear,_isConstantJac;
  class source;
  class source3d;
  class sourceLowOrder;
  //  class sourceRestrictOrder;
  class diffusiveFlux;
  class advection;
  class advection3d;
  class advectionRestrictOrder;
  class interfaceFlux;
  class interfaceFlux3d;
  class maxConvectiveSpeed;
  class maxConvectiveSpeed2d;
  class maxConvectiveSpeed3d;
  std::vector<const function *> _allTerms; //First is complete term, second is linear, third is linear vertical
  std::vector<const function **> _usedTerms; 
  fullVector<int> _VIntMass;
  termMap _customVolumeTerm0;
  termMap _lowOrderVolumeTerm0;
  termMap _restrictOrderVolumeTerm1;

  
  maxConvectiveSpeed *_maxSpeed;

  public:

  dgEulerAtmLaw(int d);
   ~dgEulerAtmLaw();
   void setup();
   void setSource(std::string tags, function *customSource);
   void setPhysicalConstants(double gamma_, double Rd_, double p0_, double g_, double R_=-1);
   void setHydrostaticState(dgDofContainer *rhoHs_, dgDofContainer *rhoThetaHs_);
   void setCoriolisFactor(function *fcor);
   void setNu(const function * nuh, const function * nuhv, const function * gradNuh, const function * gradNuv, double H_over_V=1);
   void setCoordinatesFunction(function *xyz);
   dgBoundaryCondition *newBoundaryWall();
   void setFilterMode(filterMode mode);
   void setIsLinear(bool isLinear){_isLinear = isLinear;};
   void setIsConstantJac(bool isConstantJac){_isConstantJac = isConstantJac;};
   activeTerms *getActiveTerms(){return &_trms;};
   virtual void setImexMode(imexMode mode);
   virtual void setTimeDirection(int timeDir);
   virtual bool isLinear() const;
   virtual bool isConstantJac() const;
   virtual int isTemporal(int iField) const;
   dgFaceTermAssembler *getFaceTerm(const dgGroupOfFaces &group) const;
   dgTermAssembler *getTerm(const dgGroupOfElements &group) const;
};

dgEulerAtmLaw *newEulerAtmLaw(int dim){return new dgEulerAtmLaw(dim);}

#endif

