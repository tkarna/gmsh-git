#include "dgConservationLawEulerAtm.h"
#include "function.h"
#include "functionGeneric.h"
#include "SVector3.h"
#include "dgTerm.h"
#include "dgIntegrationMatrices.h"
#include "MElement.h"


bool dgEulerAtmLaw::isLinear() const {
  return _isLinear;
}

bool dgEulerAtmLaw::isConstantJac() const {
  return _isConstantJac;
}

int dgEulerAtmLaw::isTemporal(int iField) const {
  return 1;
  //  return (iField>=8)? 0 : 1;
}

class IPTerm : public function {
  fullMatrix<double> diffusiveFluxL, diffusiveFluxR, nuhL, nuhR, nuvL, nuvR, solutionL, solutionR, normal;
  int _nbFields;
  bool _vertOnly;
  public:
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    const dgGroupOfElements &eGroupL = *m->getGroupOfElements();
    const dgGroupOfElements &eGroupR = *m->getSecondaryCache(1)->getGroupOfElements();
    double rev=1;
    if(eGroupR.getPhysicalTag() != eGroupL.getPhysicalTag()){
      rev = -1;
    }
    fullMatrix<double> ipf = dgConservationLawFunction::muFactor(m);
    for (int iPt = 0; iPt < val.size1(); iPt++) {
      double mufactor = ipf(iPt, 0);
      double nx = normal(iPt, 0);
      double ny = normal(iPt, 1);
      double nz = normal(iPt, 2);
      if (_vertOnly){
        nx = 0;
        ny = 0;
      }
      double diffusiveFluxNormGradL[5] = {diffusiveFluxL(iPt, 0)*nx + diffusiveFluxL(iPt, 5)*ny + diffusiveFluxL(iPt, 10)*nz,
        diffusiveFluxL(iPt, 1)*nx + diffusiveFluxL(iPt, 6)*ny + diffusiveFluxL(iPt, 11)*nz,
        diffusiveFluxL(iPt, 2)*nx + diffusiveFluxL(iPt, 7)*ny + diffusiveFluxL(iPt, 12)*nz,
        diffusiveFluxL(iPt, 3)*nx + diffusiveFluxL(iPt, 8)*ny + diffusiveFluxL(iPt, 13)*nz,
        diffusiveFluxL(iPt, 4)*nx + diffusiveFluxL(iPt, 9)*ny + diffusiveFluxL(iPt, 14)*nz};
      double diffusiveFluxNormGradR[5] = {diffusiveFluxR(iPt, 0)*nx*rev + diffusiveFluxR(iPt, 5)*ny*rev + diffusiveFluxR(iPt, 10)*nz,
        diffusiveFluxR(iPt, 1)*nx*rev + diffusiveFluxR(iPt, 6)*ny*rev + diffusiveFluxR(iPt, 11)*nz,
        diffusiveFluxR(iPt, 2)*nx*rev + diffusiveFluxR(iPt, 7)*ny*rev + diffusiveFluxR(iPt, 12)*nz,
        diffusiveFluxR(iPt, 3)*nx*rev + diffusiveFluxR(iPt, 8)*ny*rev + diffusiveFluxR(iPt, 13)*nz,
        diffusiveFluxR(iPt, 4)*nx*rev + diffusiveFluxR(iPt, 9)*ny*rev + diffusiveFluxR(iPt, 14)*nz};

      //As seen from the left
      double solRFromL[5] = { solutionR(iPt, 0), solutionR(iPt,1), solutionR(iPt,2), solutionR(iPt, 3), solutionR(iPt,4)};
      double diffusiveFluxNormGradRFromL[5] = {diffusiveFluxNormGradR[0], diffusiveFluxNormGradR[1],diffusiveFluxNormGradR[2],diffusiveFluxNormGradR[3],diffusiveFluxNormGradR[4]};
      if (rev < 0){
        double rhoUnRSt = solutionR(iPt,1)*nx + solutionR(iPt,2)*ny + solutionR(iPt,3)*nz;
        solRFromL[1] -= 2 * rhoUnRSt * nx;
        solRFromL[2] -= 2 * rhoUnRSt * ny;
        double diffusiveFluxNormGradUnRSt = {diffusiveFluxNormGradRFromL[1]*nx + diffusiveFluxNormGradRFromL[2]*ny + diffusiveFluxNormGradRFromL[3]*nz};
        diffusiveFluxNormGradRFromL[1] -= 2 * diffusiveFluxNormGradUnRSt * nx;
        diffusiveFluxNormGradRFromL[2] -= 2 * diffusiveFluxNormGradUnRSt * ny;
      }
      //As seen from the right
      double solLFromR[5] = { solutionL(iPt,0), solutionL(iPt,1), solutionL(iPt,2), solutionL(iPt,3), solutionL(iPt,4) };
      double diffusiveFluxNormGradLFromR[5] = {diffusiveFluxNormGradL[0], diffusiveFluxNormGradL[1],diffusiveFluxNormGradL[2],diffusiveFluxNormGradL[3],diffusiveFluxNormGradL[4]};
      if (rev < 0){
        double rhoUnLSt = solutionL(iPt,1)*nx + solutionL(iPt,2)*ny + solutionL(iPt,3)*nz;
        solLFromR[1] -= 2 * rhoUnLSt * nx;
        solLFromR[2] -= 2 * rhoUnLSt * ny;
        double diffusiveFluxNormGradUnLSt = {diffusiveFluxNormGradLFromR[1]*nx + diffusiveFluxNormGradLFromR[2]*ny + diffusiveFluxNormGradLFromR[3]*nz};
        diffusiveFluxNormGradLFromR[1] -= 2 * diffusiveFluxNormGradUnLSt * nx;
        diffusiveFluxNormGradLFromR[2] -= 2 * diffusiveFluxNormGradUnLSt * ny;

      }
      for (int k = 0; k < _nbFields; k++) {
        int iNu = std::min((int) k, nuhL.size2()-1);
        double meanNormalFluxL = (diffusiveFluxNormGradL[k] + diffusiveFluxNormGradRFromL[k]) / 2;
        double meanNormalFluxR = (diffusiveFluxNormGradLFromR[k] + diffusiveFluxNormGradR[k]) / 2;
        double nu = 0;
        double solutionJumpPenaltyL = 0, solutionJumpPenaltyR = 0;
        double nuLn = nuhL(iPt, iNu) * (nx * nx + ny * ny) + nuvL(iPt, iNu) * nz * nz;
        double nuRn = nuhR(iPt, iNu) * (nx * nx + ny * ny) + nuvR(iPt, iNu) * nz * nz;
        nu = 0.5 * (nuLn + nuRn);
        solutionJumpPenaltyL = (solutionL(iPt, k) - solRFromL[k]) / 2. * mufactor*nu;
        solutionJumpPenaltyR = (solLFromR[k] - solutionR(iPt, k)) / 2. * mufactor*nu;
        val(iPt, k)             = - (meanNormalFluxL + solutionJumpPenaltyL);
        val(iPt, k + _nbFields) =   (meanNormalFluxR + solutionJumpPenaltyR);
      }
    }
  }

  IPTerm (int nbFields, const function *diffusiveFlux, const function *nuh, const function *nuv, bool vertOnly): function(2 * nbFields), _nbFields(nbFields), _vertOnly(vertOnly) {
    setArgument(diffusiveFluxL, diffusiveFlux, 0);
    setArgument(diffusiveFluxR, diffusiveFlux, 1);
    setArgument(nuhL, nuh, 0);
    setArgument(nuhR, nuh, 1);
    setArgument(nuvL, nuv, 0);
    setArgument(nuvR, nuv, 1);
    setArgument(solutionL, function::getSolution(), 0);
    setArgument(solutionR, function::getSolution(), 1);
    setArgument(normal, function::getNormals(), 0);
  }
};


class dgEulerAtmLaw::diffusiveFlux : public function {
  fullMatrix<double> _solgrad, _xyz;
  fullMatrix<double> _nuh, _nuv;
  size_t _nbFields;
  bool _vertOnly;
  int _dim;
 public:
  diffusiveFlux(const function *nuh, const function *nuv, int dim, bool vertOnly) : function((dim==3)? 15 : 12) {
    _dim = dim;
    _vertOnly = vertOnly;
    _nbFields = (dim==3)? 5 : 4;
    setArgument(_solgrad,function::getSolutionGradient());
    setArgument(_nuh,nuh);
    setArgument(_nuv,nuv);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) { 
    for (size_t iField = 0; iField < _nbFields; iField++){
      int iNu = std::min((int)iField, _nuh.size2()-1);
      for (int i=0; i < val.size1(); i++) {
        if (_dim == 2){
          val(i, iField) = _vertOnly ? 0 : - _nuh(i,iNu) * _solgrad(i, 3 * iField);
          val(i, _nbFields + iField) =  - _nuv(i,iNu) * _solgrad(i, 3 * iField + 1);
        }
        else if (_dim == 3){
          val(i, iField) = _vertOnly ? 0 : (- _solgrad(i, 3 * iField) * _nuh(i,iNu));
          val(i, _nbFields + iField) = _vertOnly ? 0 : (- _solgrad(i, 3 * iField + 1) * _nuh(i,iNu));
          val(i, 2 * _nbFields + iField) =  - _solgrad(i, 3 * iField + 2) * _nuv(i,iNu);
        }
        else Msg::Fatal("Wrong dimension: %i",_dim);
      }
    }
  }
};

class dgEulerAtmLaw::advection : public function {
  fullMatrix<double> sol, solGrad;
  fullMatrix<double> diffusiveFlux, rhoHs, rhoThetaHs;
  double _gamma, _p0, _Rd;
  bool _linear, _vertOnly;
  public:
  advection(double gamma_, double p0_, double Rd_,
      function *diffusiveFlux_,
      dgDofContainer *rhoHs_, dgDofContainer *rhoThetaHs_, bool linear_=false, bool vertOnly_=false): function(12) {
    setArgument(sol, function::getSolution());
    if (diffusiveFlux_){
      setArgument(solGrad, function::getSolutionGradient());
      setArgument(diffusiveFlux, diffusiveFlux_);
    }
    setArgument(rhoHs, rhoHs_->getFunction());
    setArgument(rhoThetaHs, rhoThetaHs_->getFunction());
    _gamma = gamma_;
    _p0=p0_;
    _Rd=Rd_;
    _linear = linear_;
    _vertOnly = vertOnly_;
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    for (size_t i = 0 ; i < val.size1(); i++ ){
      const double rho = sol(i,0)+rhoHs(i,0);
      const double invrho = 1./rho;

      double q11, q12, q22, theta, pp;
      if (!_linear){
        q12 = sol(i,1) * sol(i,2) * invrho;
        q11 = sol(i,1) * sol(i,1) * invrho;
        q22 = sol(i,2) * sol(i,2) * invrho;
        theta = (rhoThetaHs(i,0) + sol(i,3)) * invrho;
        double p = _p0 * pow(rho * theta * _Rd / _p0, _gamma);
        double pHs = _p0 * pow(rhoThetaHs(i,0) * _Rd / _p0, _gamma);
        pp = p-pHs;
      }else{
        q12 = 0;
        q11 = 0;
        q22 = 0;
        theta = rhoThetaHs(i,0) / rhoHs(i,0);
        pp =_gamma*_p0/rhoThetaHs(i,0)*sol(i, 3);
      }     

      if (!_vertOnly){
        val(i,0) = sol(i,1);
        val(i,1) = pp + q11;
        val(i,2) = q12;
        val(i,3) = theta * sol(i,1);

      }else{
        for (int j = 0; j < 4; j++) val(i,j) = 0;
      }

      val(i,0+4) = sol(i,2);
      val(i,1+4) = q12;
      val(i,2+4) = pp + q22;
      val(i,3+4) = theta * sol(i,2);

      //      if (!_linear && diffusiveFlux.size2()!=0){
      //        for (int iField= 0; iField < diffusiveFlux.size2(); iField++){
      //          val(i, iField) += diffusiveFlux(i, iField);
      //        }
      //      }
//      }

      val(i,0+8) = 0;
      val(i,1+8) = 0;
      val(i,2+8) = 0;
      val(i,3+8) = 0;
    }
  }
};

class dgEulerAtmLaw::advection3d : public function {
  fullMatrix<double> sol, _gradSol, _xyz;
  fullMatrix<double> diffusiveFlux, rhoHs, rhoThetaHs;
  double _gamma,_p0,_Rd,_R,_g;
  bool _linear, _vertOnly;
  activeTerms _trms;
  public:
  advection3d(double g_, double gamma_, double p0_, double Rd_, double R_,
      function *diffusiveFlux_,
      dgDofContainer *rhoHs_, dgDofContainer *rhoThetaHs_,
      function *xyz, bool linear_, bool vertOnly_, activeTerms trms_):function(15) {
    _g = g_;
    _gamma = gamma_;
    _p0 = p0_;
    _Rd = Rd_;
    _R = R_;
    _linear = linear_;
    _vertOnly = vertOnly_;
    _trms = trms_;
    setArgument (sol, function::getSolution());
    setArgument(rhoHs, rhoHs_->getFunction());
    setArgument(rhoThetaHs, rhoThetaHs_->getFunction());
    if(_R>0) 
      setArgument(_xyz, xyz ? xyz : function::getCoordinates());
    if (diffusiveFlux_ || _trms.rhoPot)
      setArgument(_gradSol, function::getSolutionGradient());
    if (diffusiveFlux_)
      setArgument(diffusiveFlux, diffusiveFlux_);

  }
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    const size_t nQP = val.size1();      
    for (size_t i = 0 ; i < nQP; i++ ){
      double q12, q13, q23, q11, q22, q33;
      double theta, pp;
      if (!_linear){
        const double rho = (_trms.rhoPot ? _gradSol(i,2) : sol(i,0)) + rhoHs(i,0);
        const double invrho = 1./rho;

        q12 = _trms.adv_v * sol(i,1)*sol(i,2)*invrho;
        q13 = _trms.adv_v * sol(i,1)*sol(i,3)*invrho;
        q23 = _trms.adv_v * sol(i,2)*sol(i,3)*invrho;
        q11 = _trms.adv_v * sol(i,1)*sol(i,1)*invrho;
        q22 = _trms.adv_v * sol(i,2)*sol(i,2)*invrho;
        q33 = _trms.adv_v * sol(i,3)*sol(i,3)*invrho;
        theta = (rhoThetaHs(i,0)+sol(i,4))*invrho;
        double p = _p0 * pow(rho * theta * _Rd / _p0, _gamma);
        double pHs = _p0 * pow(rhoThetaHs(i,0) * _Rd / _p0, _gamma);
        pp = _trms.p_grad * (p - pHs);






        //TEST
        //q12 = 0;
        //q13 = 0;
        //q23 = 0;
        //q11 = 0;
        //q22 = 0;
        //q33 = 0;
        //theta = rhoThetaHs(i,0)/rhoHs(i,0);
        //pp =_trms.p_grad*_gamma*_p0/rhoThetaHs(i,0)*sol(i, 4);

      }else{
        q12 = 0;
        q13 = 0;
        q23 = 0;
        q11 = 0;
        q22 = 0;
        q33 = 0;
        theta = rhoThetaHs(i,0)/rhoHs(i,0);
        pp =_trms.p_grad*_gamma*_p0/rhoThetaHs(i,0)*sol(i, 4);
      }

      if (!_vertOnly){
        val(i,0) = _trms.v_div * sol(i, 1);
        val(i,1) = pp + q11;
        val(i,2) = q12;
        val(i,3) = q13;
        val(i,4) = _trms.adv_t * theta * sol(i,1);

        val(i,0+5) = _trms.v_div *  sol(i, 2);
        val(i,1+5) = q12;
        val(i,2+5) = pp + q22;
        val(i,3+5) = q23;
        val(i,4+5) = _trms.adv_t * theta * sol(i,2);
      }else{
        for (int j = 0; j < 10; j++) val(i,j) = 0;
      }

      val(i,0+10) = _trms.v_div * sol(i, 3);
      val(i,1+10) = q13;
      val(i,2+10) = q23;
      val(i,3+10) = pp + q33 + (_trms.grav * _trms.rhoPot) * _g * sol(i, 0);
      val(i,4+10) = _trms.adv_t * theta * sol(i,3);

      //Even though diffusion is linear, we consider nonlinear,
      //because the diffusivity can change, hence the jacobian
      if (!_linear && diffusiveFlux.size2()!=0){
        for (int iField= 0; iField < diffusiveFlux.size2(); iField++){
          val(i, iField) += _trms.diff *  diffusiveFlux(i, iField);
        }
      }

    }
  }
};




class dgEulerAtmLaw::sourceLowOrder : public function {
  fullMatrix<double>  sol;
  double _g;
  int _idx;
  activeTerms _trms;
public:
  sourceLowOrder(double g_, int dim, activeTerms trms_):function((dim==3)? 5 : 4), _g(g_), _idx((dim==3)? 3 : 2) {
    setArgument (sol, function::getSolution()); 
    _trms = trms_;
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    const size_t nQP = val.size1();     
    val.setAll(0.0);
    for (size_t i = 0 ; i < nQP; i++ ){
      val(i,_idx) = - (_trms.grav * !_trms.rhoPot) * _g * sol(i,0);
    }
  }
};

class dgEulerAtmLaw::source3d : public function {
  fullMatrix<double> rhoHs, rhoThetaHs;
  fullMatrix<double> _xyz, _fcor, _nuh, _nuv, _gradNuh, _gradNuv, sol, _gradSol;
  double _gamma, _p0, _R, _Rd;
  bool _linear, _vertOnly;
  activeTerms _trms;
public:
  source3d(double g_, double gamma_, double p0_, double Rd_, double R_, 
      dgDofContainer *rhoHs_, dgDofContainer *rhoThetaHs_,
      function *xyz, function *fcor,
      const function *nuh, const function *gradNuh, bool linear_, bool vertOnly_, activeTerms trms_):
    function(5),_gamma(gamma_),_p0(p0_),_R(R_),_Rd(Rd_),_linear(linear_),_vertOnly(vertOnly_) {
    setArgument (sol, function::getSolution()); 
    setArgument(rhoHs, rhoHs_->getFunction());
    setArgument(rhoThetaHs, rhoThetaHs_->getFunction());
    if (_R>0)
      setArgument(_xyz, xyz ? xyz : function::getCoordinates());
    if (fcor)
      setArgument(_fcor, fcor);
    if (nuh){
      setArgument(_nuh, nuh);
      setArgument(_gradNuh, gradNuh);
    }
    if (nuh || _trms.rhoPot)
      setArgument(_gradSol,function::getSolutionGradient());
    _trms = trms_;
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    const size_t nQP = val.size1();      
    if (_fcor.size1() == 0){
      _fcor.resize(nQP, 1, true);
    }

    for (size_t i = 0 ; i < nQP; i++ ){
      if(_R > 0 && !_vertOnly) {

        double x = _xyz(i,0);
        double y = _xyz(i,1);
        double z = _xyz(i,2);

        const double rho = (_trms.rhoPot ? _gradSol(i,2) : sol(i,0)) + rhoHs(i,0);
        const double invrho = 1./rho;

        double stereoAdv1, stereoAdv2, stereoAdv3;
        double theta, pp;
        if (!_linear){
          const double q11 = sol(i,1)*sol(i,1)*invrho;
          const double q12 = sol(i,1)*sol(i,2)*invrho;
          const double q13 = sol(i,1)*sol(i,3)*invrho;
          const double q22 = sol(i,2)*sol(i,2)*invrho;
          const double q23 = sol(i,2)*sol(i,3)*invrho;
          stereoAdv1 = (x * q22 - y * q12) / (2 * _R * (_R + z)) + q13 / (_R + z);
          stereoAdv2 = (y * q11 - x * q12) / (2 * _R * (_R + z)) + q23 / (_R + z);
          stereoAdv3 = - (q11 + q22) / (_R + z);
          theta = (rhoThetaHs(i,0)+sol(i,4))*invrho;
          double p = _p0 * pow(rho * theta * _Rd / _p0, _gamma);
          double pHs = _p0 * pow(rhoThetaHs(i,0) * _Rd / _p0, _gamma);
          pp = p - pHs;
        }else{
          stereoAdv1 = 0;
          stereoAdv2 = 0;
          stereoAdv3 = 0;
          pp = _gamma*_p0/rhoThetaHs(i,0)*sol(i,4);
        }
        

        double stereoDiff1 = 0, stereoDiff2 = 0, stereoDiff3 = 0;
        if (!_linear && _nuh.size1() != 0) {
          double dudx = _gradSol(i,3*1+0);
          double dudy = _gradSol(i,3*1+1);
          double dvdx = _gradSol(i,3*2+0);
          double dvdy = _gradSol(i,3*2+1);
          double dwdx = _gradSol(i,3*3+0);
          double dwdy = _gradSol(i,3*3+1);
          double dnuhdx, dnuhdy;
          if (_nuh.size2() == 1){
            dnuhdx = _gradNuh(i,0);
            dnuhdy = _gradNuh(i,1);
          }else{
            dnuhdx = _gradNuh(i, 3 + 0);
            dnuhdy = _gradNuh(i, 3 + 1);
          }
          stereoDiff1 = 1/(_R + z) * (sol(i,3) * dnuhdx + 2 * _nuh(i,0) * dwdx 
                                      + sol(i,2) / (2 * _R) * (x * dnuhdy - y * dnuhdx)
                                      + _nuh(i,0)/_R * (x * dvdy - y * dvdx)
                                      )
            - _nuh(i,0) * sol(i,1) / (_R + z) / (_R + z) * (1 + (x*x + y*y)/(4*_R*_R));
          if (_nuh.size2() != 1){
            dnuhdx = _gradNuh(i, 6 + 0);
            dnuhdy = _gradNuh(i, 6 + 1);
          }
          stereoDiff2 = 1/(_R + z) * (sol(i,3) * dnuhdy + 2 * _nuh(i,0) * dwdy 
                                      + sol(i,1) / (2 * _R) * (y * dnuhdx - x * dnuhdy)
                                      + _nuh(i,0)/_R * (y * dudx - x * dudy)
                                      )
            - _nuh(i,0) * sol(i,2) / (_R + z) / (_R + z) * (1 + (x*x + y*y)/(4*_R*_R));
          if (_nuh.size2() != 1){
            dnuhdx = _gradNuh(i, 9 + 0);
            dnuhdy = _gradNuh(i, 9 + 1);
          }
          stereoDiff3 = -1/(_R + z) * (sol(i,1) * dnuhdx + sol(i,2) * dnuhdy 
                                      + 2*_nuh(i,0)*(dudx+dvdy))
            + 1 / (_R + z) / (_R + z) * (_nuh(i,0)/_R * (x * sol(i,1) + y * sol(i,2)) - 2 * _nuh(i,0) * sol(i,3));
          
        }

//      stereoAdv1 = 0;
//      stereoAdv2 = 0;
//      stereoAdv3 = 0;

        double fcor_sign = 1;
        if (m->getGroupOfElements()->getPhysicalTag() == "volume_Bottom")
          fcor_sign = -1;

        //Treating coriolis semi-implicitely
        if (_linear)
          fcor_sign *= 0.5;


        //      fcor_sign *= 1;



        val(i,0) = 0;
        val(i,1) = - _trms.p_grad * x / (2 * _R * _R) * pp - _trms.adv_v * stereoAdv1 + _trms.diff * stereoDiff1 + fcor_sign * _trms.cor * _fcor(i,0) * sol(i,2);
        val(i,2) = - _trms.p_grad * y / (2 * _R * _R) * pp - _trms.adv_v * stereoAdv2 + _trms.diff * stereoDiff2 - fcor_sign * _trms.cor * _fcor(i,0) * sol(i,1);
        val(i,3) = - _trms.adv_v * stereoAdv3 + _trms.diff * stereoDiff3;
        val(i,4) = 0;
      }else{     
        val(i,0) = 0.0;
        val(i,1) = 0.0;
        val(i,2) = 0.0;
        val(i,3) = 0.0;
        val(i,4) = 0.0;
      }
    }
  }
};


class dgEulerAtmLaw::interfaceFlux : public function {
  fullMatrix<double> normals, solL, solR;
  fullMatrix<double> _ipTerm;
  fullMatrix<double> rhoThetaHsL, rhoThetaHsR, rhoHsL, rhoHsR;
  double _gamma,_p0,_Rd;
  bool _linear, _vertOnly;
  int *_timeDir;
  public:
  interfaceFlux(double gamma_, double p0_, double Rd_,
                function *ipTerm_,
                dgDofContainer *rhoHs_, dgDofContainer *rhoThetaHs_, int *timeDir_, bool linear_, bool vertOnly_):function(8) {
    setArgument (normals, function::getNormals(), 0);
    setArgument (solL, function::getSolution(), 0);
    setArgument (solR, function::getSolution(), 1);
    setArgument(rhoHsL, rhoHs_->getFunction(),0);
    setArgument(rhoHsR, rhoHs_->getFunction(),1);
    setArgument(rhoThetaHsL, rhoThetaHs_->getFunction(),0);
    setArgument(rhoThetaHsR, rhoThetaHs_->getFunction(),1);
    if (ipTerm_){
      setArgument(_ipTerm, ipTerm_);
    }
    _gamma = gamma_;
    _p0 = p0_;
    _Rd = Rd_;
    _linear = linear_;
    _vertOnly = vertOnly_;
    _timeDir = timeDir_;
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();

    for(size_t i=0; i< nQP; i++) {
     
      double nx = normals(i,0);
      const double ny = normals(i,1);
      if (_vertOnly){
        nx = 0;
      }

      const double rhoL=solL(i,0)+rhoHsL(i,0);
      const double invrhoL = 1./rhoL;   
      double rhoUnL = (solL(i,1) * nx + solL(i,2) * ny);

      const double rhoR=solR(i,0)+rhoHsR(i,0);
      const double invrhoR = 1./rhoR;     
      double rhoUnR = solR(i,1) * nx + solR(i,2) * ny;

      double ppL, ppR, unL, unR, thetaL, thetaR, c;
      if (!_linear){
        unL = rhoUnL * invrhoL;
        unR = rhoUnR * invrhoR;
        thetaL = (rhoThetaHsL(i,0) + solL(i,3)) * invrhoL;
        thetaR = (rhoThetaHsR(i,0) + solR(i,3)) * invrhoR;
        const double pHsL = _p0 * pow(rhoThetaHsL(i,0) * _Rd / _p0, _gamma);
        const double pHsR = _p0 * pow(rhoThetaHsR(i,0) * _Rd / _p0, _gamma);
        const double pL = _p0 * pow(rhoL * thetaL * _Rd / _p0, _gamma);
        const double pR = _p0 * pow(rhoR * thetaR * _Rd / _p0, _gamma);
        ppL = pL - pHsL;
        ppR = pR - pHsR;
        const double cL = sqrt(_gamma * pL / rhoL) + sqrt(solL(i,1) * solL(i,1) + solL(i,2) * solL(i,2)) * invrhoL;
        const double cR = sqrt(_gamma * pR / rhoR) + sqrt(solR(i,1) * solR(i,1) + solR(i,2) * solR(i,2)) * invrhoR;
        c = 0.5 * (cL + cR);
      }else{
        unL = 0;
        unR = 0;
        thetaL = rhoThetaHsL(i,0) / rhoHsL(i,0);
        thetaR = rhoThetaHsR(i,0) / rhoHsR(i,0);
        ppL = _gamma * _p0 / rhoThetaHsL(i,0) * solL(i,3);
        ppR = _gamma * _p0 / rhoThetaHsR(i,0) * solR(i,3);
        c = sqrt(_gamma * _p0 / (0.5 * (rhoHsL(i,0) + rhoHsR(i,0))));   
      }

      c *= (nx*nx+ny*ny);
      c *= *_timeDir;

      val(i,0) = -0.5 * (rhoUnL + rhoUnR - c * (solR(i,0) - solL(i,0)));
      val(i,1) = -0.5 * (ppL * nx + ppR * nx + unL * solL(i,1) + unR * solR(i,1) - c * (solR(i,1) - solL(i,1)));
      val(i,2) = -0.5 * (ppL * ny + ppR * ny + unL * solL(i,2) + unR * solR(i,2) - c * (solR(i,2) - solL(i,2)));
      val(i,3) = -0.5 * (thetaL * rhoUnL + thetaR * rhoUnR - c * (solR(i,3) - solL(i,3)));
      
      val(i,4) = -val(i,0);
      val(i,5) = -val(i,1);
      val(i,6) = -val(i,2);
      val(i,7) = -val(i,3);

//      if (!_linear){
//      for (int iField= 0; iField < _ipTerm.size2(); iField++){
//        val(i, iField) += _ipTerm(i, iField);
//      }
//      }


    }
  }
};

class dgEulerAtmLaw::interfaceFlux3d: public function {
  fullMatrix<double> _xyz;
  fullMatrix<double> normals, solL, solR;
  fullMatrix<double> _ipTerm;
  fullMatrix<double> rhoThetaHsL, rhoThetaHsR, rhoHsL, rhoHsR, _gradSolL, _gradSolR;
  double _g, _gamma, _p0, _Rd, _R;
  bool _linear, _vertOnly;
  int *_timeDir;
  activeTerms _trms;
  public:
  interfaceFlux3d(double g_, double gamma_, double p0_, double Rd_, double R_,
                  function *ipTerm_,
                  dgDofContainer *rhoHs_, dgDofContainer *rhoThetaHs_,
                  function *xyz, int *timeDir_, bool linear_, bool vertOnly_, activeTerms trms_):function(10) {
    _g = g_;
    _gamma = gamma_;
    _p0 = p0_;
    _Rd = Rd_;
    _R = R_;
    _linear = linear_;
    _vertOnly = vertOnly_;
    _timeDir = timeDir_;
    _trms = trms_;
    setArgument (normals, function::getNormals(), 0);
    setArgument (solL, function::getSolution(), 0);
    setArgument (solR, function::getSolution(), 1);
    setArgument(rhoHsL, rhoHs_->getFunction(),0);
    setArgument(rhoHsR, rhoHs_->getFunction(),1);
    setArgument(rhoThetaHsL, rhoThetaHs_->getFunction(),0);
    setArgument(rhoThetaHsR, rhoThetaHs_->getFunction(),1);
    if (ipTerm_){
      setArgument(_ipTerm, ipTerm_);
    }
    if(_R>0) {
      setArgument(_xyz, xyz ? xyz : function::getCoordinates());
    }
    if (_trms.rhoPot){
      setArgument(_gradSolL,function::getSolutionGradient(), 0);
      setArgument(_gradSolR,function::getSolutionGradient(), 1);
    }
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++) {

      double rev=1;
      if(_R > 0) {
        const dgGroupOfElements *groupL = m->getGroupOfElements();
        const dgGroupOfElements *groupR = m->getSecondaryCache(1)->getGroupOfElements();
        if(groupR->getPhysicalTag() != groupL->getPhysicalTag()){
          rev = -1;
        }
      }

      double nx = normals(i,0);
      double ny = normals(i,1);
      const double nz = normals(i,2);
     
      if (_vertOnly){
        nx = 0;
        ny = 0;
      }


      double rhoUnL = solL(i,1) * nx + solL(i,2) * ny + solL(i,3) * nz;     

      double rhoUnR = solR(i,1) * nx * rev + solR(i,2) * ny * rev + solR(i,3) * nz;

//      if (rev < 0){
//      double nx2 = _xyz(i,0)/sqrt(_xyz(i,0)*_xyz(i,0)+_xyz(i,1)*_xyz(i,1));
//      double ny2 = _xyz(i,1)/sqrt(_xyz(i,0)*_xyz(i,0)+_xyz(i,1)*_xyz(i,1));
//      double R2 = sqrt(_xyz(i,0)*_xyz(i,0)+_xyz(i,1)*_xyz(i,1));
//      printf("nx %e nx2 %e err %e ny %e ny2 %e err %e R %e R2 %e err %e\n",nx,nx2,nx-nx2,ny,ny2,ny-ny2, 2*_R, R2, 2*_R-R2);
//      }

      double ppL, ppR, unL, unR, thetaL, thetaR, c = 0;
      if (!_linear){
        
        const double rhoL = (_trms.rhoPot ? _gradSolL(i,2) : solL(i,0)) + rhoHsL(i,0);
        double invrhoL = 1./rhoL;   
        const double rhoR = (_trms.rhoPot ? _gradSolR(i,2) : solR(i,0)) + rhoHsR(i,0);
        double invrhoR = 1./rhoR;     


        unL = _trms.adv_v * rhoUnL * invrhoL;
        unR = _trms.adv_v * rhoUnR * invrhoR;
        //      double cL = (unL+unR > 0) ? unR : (-unL); //This is upwind
        double cL = _trms.adv_v * sqrt(solL(i,1) * solL(i,1) + solL(i,2) * solL(i,2) + solL(i,3) * solL(i,3)) * invrhoL;
        //      double cR = cL;
        double cR = _trms.adv_v * sqrt(solR(i,1) * solR(i,1) + solR(i,2) * solR(i,2) + solR(i,3) * solR(i,3)) * invrhoR;
        thetaL =  (rhoThetaHsL(i,0) + solL(i,4)) * invrhoL;
        thetaR =  (rhoThetaHsR(i,0) + solR(i,4)) * invrhoR;
        const double pHsR = _p0*pow(rhoThetaHsR(i,0)*_Rd/_p0,_gamma);
        const double pHsL = _p0*pow(rhoThetaHsL(i,0)*_Rd/_p0,_gamma);
        const double pR = _p0 * pow(rhoR * thetaR * _Rd / _p0,_gamma);
        const double pL = _p0 * pow(rhoL * thetaL * _Rd / _p0, _gamma);
        ppL = _trms.p_grad * (pL - pHsL);
        ppR = _trms.p_grad * (pR - pHsR);     

        cL += (_trms.adv_t || _trms.p_grad || _trms.v_div || _trms.grav) * sqrt(_gamma * pL / rhoL);   
        cR += (_trms.adv_t || _trms.p_grad || _trms.v_div || _trms.grav) * sqrt(_gamma * pR / rhoR); 
        c = _trms.lax * 0.5 * (cL + cR);


        //TEST
        //unL = 0;
        //unR = 0;
//        thetaL = rhoThetaHsL(i,0) / rhoHsL(i,0);
//        thetaR = rhoThetaHsR(i,0) / rhoHsR(i,0);
//        ppL = _trms.p_grad * _gamma * _p0 / rhoThetaHsL(i,0) * solL(i,4);
//        ppR = _trms.p_grad * _gamma * _p0 / rhoThetaHsR(i,0) * solR(i,4);
//      printf("old c is %e",c);
        //c = _trms.lax * (_trms.adv_t || _trms.p_grad || _trms.v_div || _trms.grav) * sqrt(_gamma * _p0 / (0.5 * (rhoHsL(i,0)+rhoHsR(i,0))));
        //      printf(" and new is %e\n",c);

      }else{
        unL = 0;
        unR = 0;
        thetaL = rhoThetaHsL(i,0) / rhoHsL(i,0);
        thetaR = rhoThetaHsR(i,0) / rhoHsR(i,0);
        ppL = _trms.p_grad * _gamma * _p0 / rhoThetaHsL(i,0) * solL(i,4);
        ppR = _trms.p_grad * _gamma * _p0 / rhoThetaHsR(i,0) * solR(i,4);
        c = _trms.lax * (_trms.adv_t || _trms.p_grad || _trms.v_div || _trms.grav) * sqrt(_gamma * _p0 / (0.5 * (rhoHsL(i,0)+rhoHsR(i,0))));
      }

      double rhoUR = solR(i,1);
      double rhoVR = solR(i,2);
      if (rev < 0){
        double rhoUnRSt = solR(i,1)*nx + solR(i,2)*ny + solR(i,3)*nz;
        rhoUR -= 2 * rhoUnRSt * nx;
        rhoVR -= 2 * rhoUnRSt * ny;
      }

      c *= (nx*nx+ny*ny+nz*nz); 
      c *= *_timeDir;

      //    if (m->getGroupOfInterfaces()->nConnection() == 1) c = 0;

      //      if (m->getGroupOfInterfaces()->nConnection() == 1) rhoUnR = 0;

      //      double fac = 1;
      //      double ppVR = solR(i,6);
//      if (m->getGroupOfInterfaces()->nConnection() == 1){
//      if (nz < 0)
//        Pr = -solL(i,5);
//      else
//        Pr = solL(i,5);
//      ppVR = solL(i,6);
//      }

     
      //  double ppLV = ((!_linear)?solL(i,6):solL(i,7));
      //      double ppRV = ((!_linear)?solR(i,6):solR(i,7));


      val(i,0) = -0.5 * (_trms.v_div * (rhoUnL + rhoUnR) - (1-_trms.rhoPot) * c * (solR(i,0) - solL(i,0)));
      val(i,1) = -0.5 * (ppL * nx + ppR * nx + unL * solL(i,1) + unR * rhoUR - c * (rhoUR - solL(i,1)));
      val(i,2) = -0.5 * (ppL * ny + ppR * ny + unL * solL(i,2) + unR * rhoVR - c * (rhoVR - solL(i,2)));
      val(i,3) = -0.5 * (ppL * nz + ppR * nz + _trms.rhoPot * _trms.grav * _g * nz * (solR(i,0) + solL(i,0)) + unL * solL(i,3) + unR * solR(i,3) - c * (solR(i,3) - solL(i,3)));
      val(i,4) = -0.5 * (_trms.adv_t * (thetaL * rhoUnL + thetaR * rhoUnR) - c * (solR(i,4) - solL(i,4)));

      //As seen from the right
      double rhoUL = solL(i,1);
      double rhoVL = solL(i,2);
      if (rev < 0){
        double rhoUnLSt = solL(i,1)*nx + solL(i,2)*ny + solL(i,3)*nz;
        rhoUL -= 2 * rhoUnLSt * nx;
        rhoVL -= 2 * rhoUnLSt * ny;
      }

      val(i,5) = 0.5 * (_trms.v_div * (rhoUnL + rhoUnR) - (1-_trms.rhoPot) * c * (solR(i,0) - solL(i,0)));
      val(i,6) = 0.5 * (ppL * nx * rev + ppR * nx * rev + unL * rhoUL + unR * solR(i,1) - c * (solR(i,1) - rhoUL));
      val(i,7) = 0.5 * (ppL * ny * rev + ppR * ny * rev + unL * rhoVL + unR * solR(i,2) - c * (solR(i,2) - rhoVL));
      val(i,8) = 0.5 * (ppL * nz + ppR * nz  + _trms.rhoPot * _trms.grav * _g * nz * (solR(i,0) + solL(i,0)) + unL * solL(i,3) + unR * solR(i,3) - c * (solR(i,3) - solL(i,3)));
      val(i,9) = 0.5 * (_trms.adv_t * (thetaL * rhoUnL + thetaR * rhoUnR) - c * (solR(i,4) - solL(i,4)));

      if (!_linear){
        for (int iField= 0; iField < _ipTerm.size2(); iField++){
          val(i, iField) += _trms.diff * _ipTerm(i, iField);
        }
      }

//      if (m->getGroupOfInterfaces()->nConnection()==1 && _xyz(i,2) < 2000){
//      c *= (nx*nx+ny*ny+nz*nz); 
//
//      val(i,0) = 0;
//      val(i,1) = - ppL * nx - 0.0 * unL * solL(i,1);
//      val(i,2) = - ppL * ny - 0.0 * unL * solL(i,2);
//      val(i,3) = - ppL * nz - 0.0 * unL * solL(i,3);
//      val(i,4) = 0;
//
//      val(i,5) = 0;
//      val(i,6) = 0;
//      val(i,7) = 0;
//      val(i,8) = 0;
//      val(i,9) = 0;
//      }



//      //UPWIND ADVECTION
//      double un = 0.5 * (unL + unR);
//      double rhouUp;
//      double rhovUp;
//      double rhowUp;
//      if (un>0){
//      rhouUp = solL(i,1);
//      rhovUp = solL(i,2);
//      rhowUp = solL(i,3);
//      }else{
//      rhouUp = rhoUR;
//      rhovUp = rhoVR;
//      rhowUp = solR(i,3);
//      }
//      val(i, 0) = 0;  
//      val(i, 1) = - un * rhouUp;
//      val(i, 2) = - un * rhovUp;
//      val(i, 3) = - un * rhowUp;
//      val(i, 4) = 0;  
//      if (un>0){
//      rhouUp = rhoUL;
//      rhovUp = rhoVR;
//      rhowUp = solL(i,3);
//      }else{
//      rhouUp = solR(i,1);
//      rhovUp = solR(i,2);
//      rhowUp = solR(i,3);
//      }
//      val(i, 5) = 0;  
//      val(i, 6) =  un * rhouUp;
//      val(i, 7) =  un * rhovUp;
//      val(i, 8) =  un * rhowUp;
//      val(i, 9) = 0;

    }

  }
};


class dgEulerAtmLaw::maxConvectiveSpeed : public function {
protected:
  fullMatrix<double> sol;
  double _gamma, _p0, _Rd;
  fullMatrix<double> rhoHs,rhoThetaHs;
  filterMode _filterMode;
  activeTerms _trms;
public:
  void setFilterMode(filterMode filterMode_) {_filterMode=filterMode_;}
  maxConvectiveSpeed(int nbFields):function(nbFields){}
};

// For automatic time step computation
// First component is horizontal
// Next component is vertical
class dgEulerAtmLaw::maxConvectiveSpeed2d : public maxConvectiveSpeed {
public:
  maxConvectiveSpeed2d(double gamma_, double p0_, double Rd_,
                       dgDofContainer *rhoHs_, dgDofContainer *rhoThetaHs_, activeTerms trms_):maxConvectiveSpeed(2) {
    setArgument (sol, function::getSolution());
    setArgument(rhoHs, rhoHs_->getFunction());
    setArgument(rhoThetaHs, rhoThetaHs_->getFunction());
    _gamma = gamma_;
    _p0=p0_;
    _Rd=Rd_;
    _trms = trms_;
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++) {
      const double rho=sol(i,0)+rhoHs(i,0);
      const double invrho = 1./rho;
      const double rhoTheta = rhoThetaHs(i,0)+sol(i,3);
      const double p = _p0*pow(rhoTheta*_Rd/_p0,_gamma);

      double cH=0, cV=0;
      if (_filterMode == FILTER_LINEARVERTICAL){
        cH=sqrt(_gamma*p/rho); //speed of sound
      } else if (_filterMode == FILTER_NONE){
        cH=sqrt(_gamma*p/rho); //speed of sound
        cV=cH;
      }
      val(i,0) = (_trms.adv_t || _trms.p_grad || _trms.v_div || _trms.grav) * cH + _trms.adv_v * fabs(sol(i,1) * invrho);
      val(i,1) = (_trms.adv_t || _trms.p_grad || _trms.v_div || _trms.grav) * cV + _trms.adv_v * fabs(sol(i,2) * invrho);
      //      printf("val %e %e c %e %e v %e %e\n",val(i,0),val(i,1),cH,cV,fabs(sol(i,1) * invrho),fabs(sol(i,2) * invrho));
    }
  }
};

// For automatic time step computation
// First component is horizontal
// Next component is vertical
class dgEulerAtmLaw::maxConvectiveSpeed3d : public  maxConvectiveSpeed {
  fullMatrix<double> _xyz, _gradSol;
  double _R;
public:
  maxConvectiveSpeed3d(double gamma_, double p0_, double Rd_, double R_,
                       dgDofContainer *rhoHs_, dgDofContainer *rhoThetaHs_,
                       function *xyz, activeTerms trms_):maxConvectiveSpeed(2) {
    setArgument (sol, function::getSolution());
    setArgument(rhoHs, rhoHs_->getFunction());
    setArgument(rhoThetaHs, rhoThetaHs_->getFunction());
    if(R_>0) {
      setArgument(_xyz, xyz ? xyz : function::getCoordinates());
    }
    setArgument(_gradSol,function::getSolutionGradient());
    _gamma = gamma_;
    _p0=p0_;
    _Rd=Rd_;
    _R=R_;
    _trms = trms_;
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) { 
    size_t nQP = val.size1();
    for(size_t i=0; i< nQP; i++) {
      const double rho = (_trms.rhoPot ? _gradSol(i,2) : sol(i,0)) +rhoHs(i,0);
      const double invrho = 1./rho;
      const double rhoTheta = rhoThetaHs(i,0)+sol(i,4);
      const double p = _p0*pow(rhoTheta*_Rd/_p0,_gamma);

      double cH=0, cV=0;
      if (_filterMode == FILTER_LINEARVERTICAL){
        cH=sqrt(_gamma*p/rho); //speed of sound
      } else if (_filterMode == FILTER_NONE){
        cH=sqrt(_gamma*p/rho); //speed of sound
        cV=cH;
      }

      val(i,0) = (_trms.adv_t || _trms.p_grad || _trms.v_div || _trms.grav) * cH + _trms.adv_v * sqrt(sol(i,1) * invrho * sol(i,1) * invrho + sol(i,2) * invrho * sol(i,2) * invrho);
      val(i,1) = (_trms.adv_t || _trms.p_grad || _trms.v_div || _trms.grav) * cV + _trms.adv_v * fabs(sol(i,3) * invrho);
      if (_R > 0)
        val(i,0) /= 4.0 * _R * (_R + _xyz(i,2)) /(4.0 * _R * _R + _xyz(i,0) * _xyz(i,0) + _xyz(i,1) * _xyz(i,1));
    }
  }
};

dgEulerAtmLaw::dgEulerAtmLaw(int d) : dgConservationLawFunction ((d == 3) ? 5 : 4), _dim(d) {
  _fcor = NULL;
  _xyz = NULL;
  _rhoHs = NULL;
  _rhoThetaHs = NULL;
  _nuh = NULL;
  _nuv = NULL;
  _gamma = -1;
  _Rd = -1;
  _p0 = -1;
  _g = -1;
  _R = -1;
  _isLinear = false;
  _isConstantJac = false;
  _imexMode = IMEX_ALL;
  _filterMode = FILTER_NONE;
  _maxSpeed = NULL;
  _timeDir = 1;
}


void dgEulerAtmLaw::setFilterMode(filterMode mode){
  if (_maxSpeed)
    _maxSpeed->setFilterMode(mode);
  _filterMode = mode;
}

void dgEulerAtmLaw::setImexMode(imexMode mode) {
  if (mode == IMEX_IMPLICIT){
    for (size_t iTerm = 0; iTerm < _usedTerms.size(); iTerm++){
      *_usedTerms[iTerm] = _allTerms[iTerm + _filterMode * _usedTerms.size()];
    }
    _isLinear=true;
    _isConstantJac=true;
  }else{
    for (size_t iTerm = 0; iTerm < _usedTerms.size(); iTerm++){
      *_usedTerms[iTerm] = _allTerms[iTerm];
    }
    _isLinear=false;
    _isConstantJac=false;
  }
  _imexMode = mode;
}

void dgEulerAtmLaw::setTimeDirection(int timeDir){
  _timeDir = timeDir;
}

void dgEulerAtmLaw::setup() {
  _VIntMass.resize(getNbFields(), true);
  if (_trms.rhoPot)
    _VIntMass(0)=1;
  if (_gamma<0 || _Rd<0 || _p0<0 || _g<0)
    Msg::Fatal("Some positive physical constants are negative or not set");
  if (_rhoHs==NULL || _rhoThetaHs==NULL)
    Msg::Fatal("Hydrostatic state fields must be set");
  if (_R > 0 && _xyz == NULL)
    Msg::Warning("Stereographic coordinates used without providing a coordinates function");
  _usedTerms.push_back(&_volumeTerm0[""]);
  _usedTerms.push_back(&_lowOrderVolumeTerm0[""]);
  _usedTerms.push_back(&_volumeTerm1[""]);
  _usedTerms.push_back(&_interfaceTerm0[""]);

  // FILTER_NONE  ,  FILTER_LINEAR  ,  FILTER_LINEARVERTICAL
  bool linType[3] = {false, true, true};


  bool vertType[3] = {false, false, true};
  //bool linType[3] = {false, false, false};
  //  bool vertType[3] = {true, true, true};



  if (_dim == 3){
    _maxSpeed  =  new maxConvectiveSpeed3d(_gamma, _p0, _Rd, _R, _rhoHs, _rhoThetaHs, _xyz, _trms);
    const function *sourceLO = new sourceLowOrder (_g, 3, _trms);
    for (int iFilt = 0; iFilt < 3; iFilt++){
      function *diff = (_nuh && _nuv) ? (new diffusiveFlux(_nuh, _nuv, _dim, vertType[iFilt])) : NULL;
      function *ip = (_nuh && _nuv) ? (new IPTerm(getNbFields(), diff, _nuh, _nuv, vertType[iFilt])) : NULL;
      _allTerms.push_back(new source3d (_g, _gamma, _p0, _Rd, _R, _rhoHs, _rhoThetaHs, _xyz, _fcor, _nuh, _gradNuh, linType[iFilt], vertType[iFilt], _trms));
      _allTerms.push_back(sourceLO);
      _allTerms.push_back(new advection3d(_g, _gamma, _p0, _Rd, _R, diff, _rhoHs, _rhoThetaHs, _xyz, linType[iFilt], vertType[iFilt], _trms));
      _allTerms.push_back(new interfaceFlux3d(_g, _gamma, _p0, _Rd, _R, ip, _rhoHs, _rhoThetaHs, _xyz, &_timeDir, linType[iFilt], vertType[iFilt], _trms));
    }
  }else if (_dim  ==  2){
    _maxSpeed =  new maxConvectiveSpeed2d(_gamma, _p0, _Rd, _rhoHs, _rhoThetaHs, _trms);
    const function *sourceLO = new sourceLowOrder (_g, 2, _trms);
    for (int iFilt = 0; iFilt < 3; iFilt++){
      function *diff = (_nuh && _nuv) ? (new diffusiveFlux(_nuh, _nuv, _dim, vertType[iFilt])) : NULL;
      function *ip = (_nuh && _nuv) ? (new IPTerm(getNbFields(), diff, _nuh, _nuv, vertType[iFilt])) : NULL;
      _allTerms.push_back(NULL); //No classical source in 2d
      _allTerms.push_back(sourceLO);
      _allTerms.push_back(new advection(_gamma, _p0, _Rd, diff, _rhoHs, _rhoThetaHs, linType[iFilt], vertType[iFilt]));
      _allTerms.push_back(new interfaceFlux(_gamma, _p0, _Rd, ip,  _rhoHs, _rhoThetaHs, &_timeDir, linType[iFilt],vertType[iFilt]));
    }
  }else Msg::Fatal("Dimension %i not supported by this equation. Only dimensions 2 and 3 are allowed.", _dim);
  if (_allTerms.size() != 3 * _usedTerms.size()) Msg::Fatal("Equation terms mismatch");
  setFilterMode(_filterMode);
  setImexMode(_imexMode);
  _maximumConvectiveSpeed[""]=_maxSpeed;
}

dgEulerAtmLaw::~dgEulerAtmLaw(){
  if (_maxSpeed) delete _maxSpeed;
  for (size_t iTerm = 0; iTerm < _allTerms.size(); iTerm++)
    if (_allTerms[iTerm]) 
        delete _allTerms[iTerm];
}


dgTermAssembler *dgEulerAtmLaw::getTerm(const dgGroupOfElements &group) const
{
  checkSetup();
  std::vector<dgTerm*> terms;
  const function *vf0Custom = functionForTag(_customVolumeTerm0, group.getPhysicalTag());
  if (vf0Custom){
    terms.push_back(dgIntegralTerm::newPsiTerm(vf0Custom, _nbf, isLinear()));
  } 
  const function *vf0 = functionForTag(_volumeTerm0, group.getPhysicalTag());
  const function *vf1 = functionForTag(_volumeTerm1, group.getPhysicalTag());
  if (vf0){
    terms.push_back(dgIntegralTerm::newPsiTerm(vf0, _nbf, isLinear()));
  }
  if (vf1){
    terms.push_back(dgIntegralTerm::newGradPsiTerm(vf1, _nbf, isLinear()));
  }
  const function *LOvf0 = functionForTag(_lowOrderVolumeTerm0, group.getPhysicalTag());
  if (LOvf0){
    //terms.push_back(dgIntegralTerm::newLowOrderFPsiTerm(LOvf0, _nbf, isLinear(), group.getFunctionSpace().order-1, true));
    terms.push_back(dgIntegralTerm::newPsiTerm(LOvf0, _nbf, isLinear()));
  }
  return new dgTermAssemblerIntegral(terms);
}

dgFaceTermAssembler *dgEulerAtmLaw::getFaceTerm(const dgGroupOfFaces &group) const
{
  //  return NULL;
  checkSetup();
  if (group.orientationTag() == dgGroupOfFaces::ORIENTATION_VERTICAL && _filterMode == FILTER_LINEARVERTICAL && _imexMode == IMEX_IMPLICIT)
    return NULL;
  const function *normalFluxF = NULL;
  switch (group.nConnection()) {
    case 1 :
      normalFluxF = getBoundaryCondition(group.physicalTag())->getTerm0(); 
      break;
    case 2 :
      normalFluxF = getInterfaceTerm0(group.physicalTag());
      break;
    default : Msg::Fatal("Number of connections invalid: %d", group.nConnection());
  }
  if (normalFluxF){
    std::vector<int> orders;
    //    orders.push_back(-1);
    //    orders.push_back(group.elementGroup(0).getFunctionSpace().order);
    return new dgFaceTermAssemblerIntegral(dgIntegralTerm::newInterfacePsiTerm(normalFluxF, isLinear()), _nbf);
  }else return NULL;
}

void dgEulerAtmLaw::setPhysicalConstants(double gamma_, double Rd_, double p0_, double g_, double R_){
  _gamma=gamma_;
  _Rd=Rd_;
  _p0=p0_;
  _g=g_;
  _R=R_;
}

void dgEulerAtmLaw::setHydrostaticState(dgDofContainer *rhoHs_, dgDofContainer *rhoThetaHs_){
  _rhoHs=rhoHs_;
  _rhoThetaHs=rhoThetaHs_;
}

void dgEulerAtmLaw::setNu(const function *nuh, const function *nuv, const function *gradNuh, const function *gradNuv, double H_over_V){
  _nuh = nuh;
  _nuv = nuv;
  _gradNuh = gradNuh;
  _gradNuv = gradNuv;
  _H_over_V = H_over_V;
}

void dgEulerAtmLaw::setCoriolisFactor(function *fcor){
  _fcor=fcor;
}

void dgEulerAtmLaw::setSource(std::string tags, function *customSource){
  _customVolumeTerm0[tags] = customSource;
}


void dgEulerAtmLaw::setCoordinatesFunction(function *xyz){
  _xyz=xyz;
}
//------------------------------------------------------------------------------
//                               Boundary conditions
//------------------------------------------------------------------------------
class BoundaryCondition2dWallF : public function {
  fullMatrix<double> solIn, normals;
public:
  BoundaryCondition2dWallF():function (4){
    setArgument (normals, function::getNormals());
    setArgument (solIn, function::getSolution(), 0);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    for (size_t i=0; i< nQP; i++) {
      const double nx = normals (i,0);
      const double ny = normals (i,1);
      double vn = solIn (i,1) * nx + solIn (i,2) * ny;
      val (i,0) = solIn (i,0);
      val (i,1) = solIn (i,1) - 2 * vn * nx; 
      val (i,2) = solIn (i,2) - 2 * vn * ny;
      val (i,3) = solIn (i,3);
    }
  }
};


class BoundaryCondition3dWallF : public function {
  fullMatrix<double> solIn, normals;
public:
  BoundaryCondition3dWallF():function (5){
    setArgument (normals, function::getNormals());
    setArgument (solIn, function::getSolution(), 0);
  }
  void call (dataCacheMap *m, fullMatrix<double> &val) {
    size_t nQP = val.size1();
    for (size_t i=0; i< nQP; i++) {
      const double nx = normals (i,0);
      const double ny = normals (i,1);
      const double nz = normals (i,2);
      double vn = solIn (i,1) * nx + solIn (i,2) * ny + solIn (i,3) * nz;
      val (i,0) = solIn (i,0);
      val (i,1) = solIn (i,1) - 2 * vn * nx;
      val (i,2) = solIn (i,2) - 2 * vn * ny;
      val (i,3) = solIn (i,3) - 2 * vn * nz;
      val (i,4) = solIn (i,4);
    }
  }
};


dgBoundaryCondition *dgEulerAtmLaw::newBoundaryWall(){
  function *BoundaryWallF;
  std::vector<double> *grad = new std::vector<double>();
  if (_dim==3){
    BoundaryWallF = new BoundaryCondition3dWallF();
    for (int i = 0; i < 15; i++)
      grad->push_back(0);
  }else{
    BoundaryWallF = new BoundaryCondition2dWallF();
    for (int i = 0; i < 12; i++)
      grad->push_back(0);
  }
  function *gradient = new functionConstant(*grad);
  std::vector<const function*> *toReplace = new std::vector<const function*>();
  std::vector<const function*> *replaceBy = new std::vector<const function*>();
  toReplace->push_back(function::getSolution());
  toReplace->push_back(function::getSolutionGradient());
  replaceBy->push_back(BoundaryWallF);
  replaceBy->push_back(gradient);
  return newOutsideValueBoundaryGeneric("",*toReplace,*replaceBy);
}
