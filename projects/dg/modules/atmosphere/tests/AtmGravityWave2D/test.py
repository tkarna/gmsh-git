from extrude import *
from dgpy import *
from math import *
from partitionThenExtrude import *
import time, os, sys

#### Physical constants ###
gamma=1.4
Rd=287
p0=1.0e5
g=9.80616
Cv=Rd/(gamma-1)
Cp=Rd*(1+1/(gamma-1))
###########################
#### Begin and end time ###
Ti=0
Tf=1000
###########################

order=3
dimension=2

#Partition, extrude and rotat the mesh
lineModel = GModel()
lineModel.load("line.geo")
lineModel.mesh(1)
lineModel.setOrderN(3,1,0)
lineModel.save("line.msh")
partitionThenExtrudeLine(1)

#Load the mesh
model = GModel()
name='line'
if Msg.GetCommSize()>1:
  partStr='_part_%i' % Msg.GetCommSize()
else:
  partStr=''
model.load(name + partStr + '_2d_XY.msh')

#Create and split groups

groups = dgGroupCollection(model, dimension, order)

groups.splitGroupsByPhysicalTag();
groupsH = dgGroupCollection.newByTag(model, dimension-1, order, ["bottom_domain"])
extrusion = dgExtrusion(groups, groupsH, ["bottom_domain"])
groups.splitFaceGroupsByOrientation(extrusion, ["bottom_domain", "top_domain"]) 

claw = dgEulerAtmLaw(dimension)
solution = dgDofContainer(groups, claw.getNbFields())
XYZ = groups.getFunctionCoordinates()

def hydrostaticState(z) :
    N=1.e-2
    theta0=300
    thetaHs=theta0*exp(N**2*z/g)
    exnerHs=1+ g**2/(Cp*theta0*N**2) * (exp(-N**2*z/g)-1)
    rhoHs=p0/(Rd*thetaHs)*exnerHs**(Cv/Rd)
    return rhoHs,exnerHs,thetaHs

def initialCondition(FCT,  XYZ) :
    a=5000
    dtheta0=1.0e-2
    H=10000
    xc=-50000
    U=20.0
    for i in range (0,XYZ.size1()) :
        x=XYZ(i,0)
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        thetaPert=thetaHs+dtheta0*(sin(pi*z/H)/(1+(x-xc)**2/a**2))
        rhoPert=p0/(Rd*thetaPert)*exnerHs**(Cv/Rd)
        FCT.set(i,0,rhoPert-rhoHs)
        FCT.set(i,1,U*rhoPert)
        FCT.set(i,2,0.0)
        FCT.set(i,3,rhoPert*thetaPert-rhoHs*thetaHs)

def rhoHydrostatic(FCT,  XYZ) :
    for i in range (0,XYZ.size1()) :
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        FCT.set(i,0,rhoHs)

def rhoThetaHydrostatic(FCT,  XYZ) :
    for i in range (0,XYZ.size1()) :
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        FCT.set(i,0,rhoHs*thetaHs)
        
def getVelocity(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        rho=rhoHs+sol(i,0)
        FCT.set(i,0,sol(i,1)/rho)
        FCT.set(i,1,sol(i,2)/rho)
        FCT.set(i,2,0)

def getRhop(FCT, sol) :
    for i in range (0,sol.size1()) :
        FCT.set(i,0,sol(i,0))

def getpp(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        rho=rhoHs+sol(i,0)
        rhoTheta = rhoHs*thetaHs+sol(i,3)
        pHs=p0*(rhoHs*thetaHs*Rd/p0)**gamma
        p=p0*(rhoTheta*Rd/p0)**gamma
        FCT.set(i,0,p-pHs)

def getpHs(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        pHs=p0*(rhoHs*thetaHs*Rd/p0)**gamma
        FCT.set(i,0,pHs)

def getp(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        rhoTheta = rhoHs*thetaHs+sol(i,3)
        p=p0*(rhoTheta*Rd/p0)**gamma
        FCT.set(i,0,p)

def getThetap(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        rho=rhoHs+sol(i,0)
        FCT.set(i,0,(sol(i,3)+(rhoHs*thetaHs))/rho-thetaHs)
def getUVPertNorm(FCT, uv) :
    for i in range (0,uv.size1()) :
        FCT.set(i,0,sqrt((uv(i,0)-20)*(uv(i,0)-20)+uv(i,1)*uv(i,1)))

uv=functionPython(3, getVelocity, [solution.getFunction(), XYZ])
rhop=functionPython(1, getRhop, [solution.getFunction()])
pHs=functionPython(1, getpHs, [solution.getFunction(), XYZ])
p=functionPython(1, getp, [solution.getFunction(), XYZ])
pp=functionPython(1, getpp, [solution.getFunction(), XYZ])
thetap=functionPython(1, getThetap, [solution.getFunction(), XYZ])
uvPertNorm=functionPython(2, getUVPertNorm, [uv])


initF=functionPython(4, initialCondition, [XYZ])
rhoHs = dgDofContainer(groups, 1)
rhoHs.interpolate(functionPython(1, rhoHydrostatic, [XYZ]))
rhoThetaHs = dgDofContainer(groups, 1)
rhoThetaHs.interpolate(functionPython(1, rhoThetaHydrostatic, [XYZ]))

claw.setHydrostaticState(rhoHs,rhoThetaHs)
claw.setPhysicalConstants(gamma,Rd,p0,g)

trms = claw.getActiveTerms();
trms.setAdvV(True);
trms.setAdvT(True);
trms.setPGrad(True);
trms.setVDiv(True);
trms.setCor(False);
trms.setGrav(True);
trms.setDiff(False);
trms.setLax(True);


boundaryWall = claw.newBoundaryWall()
claw.addBoundaryCondition('bottom_domain', boundaryWall)
claw.addBoundaryCondition('top_domain', boundaryWall)

#We need to use a dof container to restrict the initial condition
# (=boundary condition) to the discretization space
initDof = dgDofContainer(groups, 4)
initDof.interpolate(initF)
outsideBoundary = claw.newOutsideValueBoundary("",initDof.getFunction())
claw.addBoundaryCondition('left', outsideBoundary)
claw.addBoundaryCondition('right', outsideBoundary)


timeIterTab=["ERK","IMEX","IMEXV"]

#Export data
def getExp(FCT, uv, rhop, thetap, pp) :
    for i in range (0,uv.size1()) :
        FCT.set(i,0,uv(i,0))
        FCT.set(i,1,uv(i,1))
        FCT.set(i,2,rhop(i,0))
        FCT.set(i,3,thetap(i,0))
        FCT.set(i,4,pp(i,0))
Exp=functionPython(5, getExp, [uv, rhop, thetap, pp])
nCompExp=[2,1,1,1]
namesExp=["uv","rhop","thetap","pp"]
times=[0, 0, 0]
tsteps=[0,0,0]
intsThetaP=[0, 0, 0]
intsUVPertNorm=[0, 0, 0]
integratorThetaP = dgFunctionIntegrator(groups, thetap)
integratorUVPertNorm = dgFunctionIntegrator(groups, uvPertNorm)
for iTimeIter in range(len(timeIterTab)):
  timeIterType = timeIterTab[iTimeIter]
  print ('Time integrator:', timeIterType)
  solution.interpolate(initF)
  if (timeIterType=="ERK"):
    timeIter = dgERK(claw, None, DG_ERK_22)
  elif (timeIterType=="IMEX"):
    claw.setFilterMode(FILTER_LINEAR);
    petscIm = linearSystemPETScBlockDouble()
    dofIm = dgDofManager.newDGBlock(groups, claw.getNbFields(), petscIm)
    timeIter = dgIMEXRK(claw, dofIm, 2)
    timeIter = dgIMEXRK(claw, dofIm, 2)
    timeIter.getNewton().setRtol(1.e-8) #Rtol
    timeIter.getNewton().setVerb(0)     #Verbosity
  elif (timeIterType=="IMEXV"):
    claw.setFilterMode(FILTER_LINEARVERTICAL);
    petscIm = dgLinearSystemExtrusion(claw, groups, extrusion)
    dofIm = dgDofManager.newDGBlock(groups, claw.getNbFields(), petscIm)
    timeIter = dgIMEXRK(claw, dofIm, 2)
    timeIter.getNewton().setRtol(1.e-8) #Rtol
    timeIter.getNewton().setVerb(0)     #Verbosity

  dt=claw.getMinOfTimeSteps(solution, extrusion)
  if (Msg.GetCommRank()==0):
    print ("Time step: ", dt)

  nbSteps = int(ceil(Tf/dt))

  solution.exportFunctionVtk(Exp,'output_'+timeIterType+'/export', 0, 0,"solution",nCompExp,namesExp)

  t=Ti
  n_export=0
  timeStart=time.clock();
  for i in range(0,nbSteps-1):
    #dt=claw.getMinOfTimeSteps(solution)
    if (i==nbSteps-1):
        dt=Tf-t
    norm = timeIter.iterate (solution, dt, t)
    t=t+dt
    if (Msg.GetCommRank()==0):
        sys.stdout.write('.')
        sys.stdout.flush()
  n_export=n_export+1
  solution.exportFunctionVtk(Exp,'output_'+timeIterType+'/export', t, 1,"solution",nCompExp,namesExp)
  print ('\nWriting output {} at time {}'.format(n_export,t))
  print ('')
  print ('Time elapsed: {}'.format(time.clock()-timeStart))
  times[iTimeIter]=time.clock()-timeStart
  intThetaP = fullMatrixDouble(1,1)
  integratorThetaP.compute(intThetaP)
  intsThetaP[iTimeIter]=intThetaP(0,0)
  intUVPertNorm = fullMatrixDouble(2,1)
  integratorUVPertNorm.compute(intUVPertNorm)
  intsUVPertNorm[iTimeIter]=intUVPertNorm(0,0)
  tsteps[iTimeIter]=dt
print("------------------------------------------------------------------")
print ("Time integrator: {}".format(timeIterTab))
print ("Time: {}".format(times))
print ("Time step: {}".format(tsteps))
print ("Integral of potential temperature perturbation: {}".format(intsThetaP))
print ("Integral of velocity perturbation norm: {}".format(intsUVPertNorm))

refThetaP = [1091274.823020396, 1101885.1922484243, 1095395.2517467719]
refUVPertNorm = [4900038.072040573, 4665943.256043618, 4829936.705407048]

errThetaP=0
errUV=0
for i in range(3):
  errThetaP=max(errThetaP, abs((refThetaP[i]-intsThetaP[i])/refThetaP[i]))
  errUV=max(errUV,abs((refUVPertNorm[i]-intsUVPertNorm[i])/refUVPertNorm[i]))
print("------------------------------------------------------------------")
print("Max relative error (thetaP, UVPertNorm): {} {}".format(errThetaP, errUV))
print("------------------------------------------------------------------")
if (errThetaP > 1e-5 or errUV > 1e-5):
  print ("Test failed")
  Msg.Exit(1)
else:
  print ("Test successfully passed")
  Msg.Exit(0)
