%module atmosphere

%{
  #undef HAVE_DLOPEN
  #include "dgConservationLawEulerAtm.h"
%}

%import(module="dgpy.dgCommon") "dgCommon.i";
%import(module="dgpy.conservationLaw") "dgConservationLaw.h";
%import(module="dgpy.conservationLaw") "dgConservationLawFunction.h";

%include "dgConservationLawEulerAtm.h"
