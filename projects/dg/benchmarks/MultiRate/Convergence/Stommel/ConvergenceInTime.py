from gmshpy import *
from dgpy import *
import pylab as pl
from termcolor import colored
from math import *
import time,  os,  sys


order = 1
dimension = 2
clscale = 1

NN=5
Ti=0
error=0

RK_TYPES_SR = [ERK_22_C, ERK_33_C, ERK_43_C, ERK_44_C]
RK_TYPES_MR = [ERK_43_S, ERK_43_S_C, ERK_22_C, ERK_33_C, ERK_43_C, ERK_44_C]
LGD_RK_SR = ['ERK_22', 'ERK_33', 'ERK_43', 'ERK_44']
LGD_RK_MR = ['ERK_43_S', 'ERK_43_S_C', 'ERK_22_C', 'ERK_33_C', 'ERK_43_C', 'ERK_44_C']
cOrders_SR = [2, 3, 3, 4]
cOrders_MR = [3, 3, 2, 2, 2, 2]
plot_colors =['rs-', 'bo-', 'g^-', 'cs-', 'mo-', 'r^-', 'bs-', 'go-', 'c^-', 'ms-']
plot_ideal =['r:', 'b:', 'g:', 'c:', 'm:', 'r:', 'b:', 'g:', 'c:', 'm:']


model = GModel()
if(not os.path.exists("stommel_adapted-%d.msh"%(order))):
  m = GModel()
  m.load("stommel_adapted.geo")
  GmshSetOption('Mesh','CharacteristicLengthFactor',   clscale)
  GmshSetOption('Mesh','Algorithm',   "frontal")
  m.setOrderN(order, False, False)
  m.mesh(dimension)
  m.save("stommel_adapted-%d.msh"%(order))
model.load ('stommel_adapted-%d.msh'%(order))


try : os.mkdir("output");
except: 0;


def toIntegrate(fct, sol):
  for i in range(0,sol.size1()):
    s = sol.get(i,0)
    fct.set(i,0,s*s) 

CCode = """
#include "fullMatrix.h"
#include "function.h"
extern "C" {
void wind (dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< sol.size1(); i++) {
    sol.set(i,0,sin(xyz(i,1)/1e6)/1e6);
    sol.set(i,1,0);
  }
}
void coriolis (dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< sol.size1(); i++) {
    sol.set(i,0,1e-4+2e-11*xyz(i,1));
  }
}
void extractEta (dataCacheMap *, fullMatrix<double> &eta, fullMatrix<double> &fullSol) {
  for (size_t i = 0; i< fullSol.size1(); i++) {
    eta.set(i,0,fullSol(i,0));
  }
}
}
"""

tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CCode, tmpLib);
Msg.Barrier()
fig = pl.figure(1, figsize=(12, 6))
for mL in [1, 1000]:
  if(mL==1):
    pl.subplot(121)
    RK_TYPES = RK_TYPES_SR
    LGD_RK = LGD_RK_SR
    cOrders = cOrders_SR
  else:
    pl.subplot(122)
    RK_TYPES = RK_TYPES_MR
    LGD_RK = LGD_RK_MR
    cOrders = cOrders_MR

  pl.grid(True)
  for k in range (0,  len(RK_TYPES)):
    groups = dgGroupCollection(model, dimension, order)
    claw = dgConservationLawShallowWater2d()
    solution = dgDofContainer(groups, claw.getNbFields())
    solutionRef = dgDofContainer(groups, claw.getNbFields())
    
    XYZ = groups.getFunctionCoordinates();
    f0 = functionC(tmpLib,"coriolis",1,[XYZ, functionSolution.get()])
    claw.setCoriolisFactor(f0)
    f1 = functionConstant(0)
    claw.setQuadraticDissipation(f1)
    claw.setIsLinear(1)
    f2 = functionConstant([1e-6])
    claw.setLinearDissipation(f2)
    f3 = functionC(tmpLib,"wind",2,[XYZ])
    claw.setSource(f3)
    f4 = functionConstant([1000])
    claw.setBathymetry(f4)
    f5 = functionConstant([0,0,0])
    claw.setBathymetryGradient(f5)
    
    bndWall = claw.newBoundaryWall()
    claw.addBoundaryCondition('Wall', bndWall)
    
    
    rk = dgERK(claw,  None,  DG_ERK_44_3_8)
    
    RKTYPE = RK_TYPES[k]
    cOrder = cOrders[k]
    
    rk2 = dgMultirateERK(groups,  claw,  RKTYPE)
    
    dt=rk2.splitGroupsForMultirate(mL,   solution, [solution, solutionRef])
    print 'Method:', LGD_RK[k]
    rk2.printMultirateInfo()
    solution.exportGroupIdMsh()
    Tf=dt
    
    dtRef = dt/pow(2, NN+1)
    nbStepsRef = int(ceil((Tf-Ti)/dtRef))
    t = Ti
    for i in range (1,nbStepsRef+1) :
      if(i == nbStepsRef):
        dtRef = Tf-t
      rk.iterate(solutionRef, dtRef, t)
      t = t +dtRef
    
    conv = []
    timestep= []
    for n in range(0, NN+1):
      solution.setAll(0.0)
      t=Ti
      dtN=dt/pow(2, n)
      nbSteps = int(ceil((Tf-Ti)/dtN))
      for i in range(1, nbSteps+1):
        if(i == nbSteps):
          dtN = Tf-t
        norm = rk2.iterate (solution,  dtN,  t)
        t=t+dtN
      solution.axpy(solutionRef, -1)
      conv.append(solution.norm())
      timestep.append(dtN)
    
    old = conv[0]
    ideal = []
    for c in range(0,  NN+1):
      ideal.append(conv[0]/(2**(cOrder*c)))
      if(conv[0] / conv[c] < (2**(cOrder*c)) * 0.9):
        error = 1
        print(colored("%.2f - %.2e (%.2f %%)",  "red") %(log(old/conv[c],   2),  conv[c],  conv[0] / (conv[c] * 2**(cOrder*c))*100))
        old = conv[c]
      else:
        print(colored("%.2f - %.2e (%.2f %%)",  "green") %(log(old/conv[c],   2),  conv[c],  conv[0] / (conv[c] * 2**(cOrder*c))*100))
        old = conv[c]
    print ''

    pl.loglog(timestep, conv, plot_colors[k], label=LGD_RK[k])
    pl.loglog(timestep, ideal, plot_ideal[k])
    pl.legend(loc='lower right', prop={'size':12})

fig.savefig('time_convergence_stommel.png', dpi=fig.dpi)

if(error == 0):
  print "exit with succes :-)"
else:
  print "exit with failure :-("
  
sys.exit(error)
Msg.Exit(0)
