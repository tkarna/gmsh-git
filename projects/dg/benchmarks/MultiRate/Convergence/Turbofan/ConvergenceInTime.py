from dgpy import *
import pylab as pl
from termcolor import colored
import sys,  os, time, math


if(Msg.GetCommRank() == 0):
  functionC.buildLibraryFromFile ("TURBOFAN.cc",  "lib_turbofan.so");
Msg.Barrier()
  

tlib = "lib_turbofan.so";

order = 1
dimension = 3
clscale = 1

if(not os.path.exists("turbofan-%d.msh"%(order))):
  m   = GModel  ()
  m.load("turbofan.geo")
  m.setOrderN(order, False, False)
  GmshSetOption('Mesh', 'CharacteristicLengthFactor',    clscale)
  m.mesh(dimension)
  OptimizeMesh(m)
  m.save("turbofan-"+str(order)+".msh")
model   = GModel  ()
model.load('turbofan-'+str(order)+'.msh')


RK_TYPES_SR = [ERK_22_C, ERK_33_C, ERK_43_C, ERK_44_C]
RK_TYPES_MR = [ERK_43_S, ERK_43_S_C, ERK_22_C, ERK_33_C, ERK_43_C, ERK_44_C]
LGD_RK_SR = ['ERK_22', 'ERK_33', 'ERK_43', 'ERK_44']
LGD_RK_MR = ['ERK_43_S', 'ERK_43_S_C', 'ERK_22_C', 'ERK_33_C', 'ERK_43_C', 'ERK_44_C']
cOrders_SR = [2, 3, 3, 4]
cOrders_MR = [3, 3, 2, 2, 2, 2]
plot_colors =['rs-', 'bo-', 'g^-', 'cs-', 'mo-', 'r^-', 'bs-', 'go-', 'c^-', 'ms-']
plot_ideal =['r:', 'b:', 'g:', 'c:', 'm:', 'r:', 'b:', 'g:', 'c:', 'm:']

n = 5
error = 0

fig = pl.figure(1, figsize=(12, 6))
for mL in [1,  1000]:
  if(mL==1):
    pl.subplot(121)
    RK_TYPES = RK_TYPES_SR
    LGD_RK = LGD_RK_SR
    cOrders = cOrders_SR
  else:
    pl.subplot(122)
    RK_TYPES = RK_TYPES_MR
    LGD_RK = LGD_RK_MR
    cOrders = cOrders_MR

  pl.grid(True)
  for k in range (0, len(RK_TYPES)):
    
    groups = dgGroupCollection(model, dimension, order)
    xyz = function.getCoordinates()
  
    MFPrecomp = functionC(tlib,  "meanFlow",  5,  [xyz])
    MF = functionPrecomputed(groups,  2*order+1,  5)
    MF.compute(MFPrecomp)
  
    InitScaling= functionC(tlib,   "initialScaling",   5,   [xyz])
    
    claw = dgConservationLawLEE(dimension,  "",  MF,  None,  None)
  
    solution_ref = dgDofContainer(groups, claw.getNbFields())  
    solution_ref.setAll(0.0)
    solution = dgDofContainer(groups, claw.getNbFields())
    solution.setAll(0.0)
  
    rk_ref = dgERK(claw,  None,  DG_ERK_44_3_8)
  
    RKTYPE = RK_TYPES[k]
    cOrder = cOrders[k]
    rk = dgMultirateERK(groups, claw, RKTYPE)
    dt = rk.splitGroupsForMultirate(mL,   solution, [solution, solution_ref])
    print 'Method:', LGD_RK[k]
    rk.printMultirateInfo(0)
    dt = 2 * dt
  
    solution.interpolate(InitScaling)
    solution_ref.interpolate(InitScaling)
    
    coeff = functionConstant([16., 10., 11.771, -5.7843e-05, 27.110, 0]) # With mean flow,  omega = 16,  m = 10,  alpha = 11.771,  MOverN = -5.7843e-05,  mu = (27.110, 0)
    time = function.getTime()
    ISAmp = functionC(tlib,  "inletModeAmp",  8,  [xyz,  coeff])
    amp = functionPrecomputed(groups,  2*order+1,  8)
    for i in range(groups.getNbFaceGroups()) :
      group = groups.getFaceGroup(i)
      if (group.physicalTag() == 'Fan') :
        amp.compute(ISAmp, group)
    
    IS = functionC(tlib,  "inletMode",  5,  [time,  amp,  coeff])
    InletBoundary = claw.newBoundaryNonReflect(IS)
    OutletBoundary = claw.newBoundaryNonReflect()
    InteriorBoundarySpinner = claw.newBoundaryWall(True)
    InteriorBoundaryDuct = claw.newBoundaryWall()
    
    claw.addBoundaryCondition('Fan', InletBoundary)
    claw.addBoundaryCondition('Intake', OutletBoundary)
    claw.addBoundaryCondition('Spinner', InteriorBoundarySpinner)
    claw.addBoundaryCondition('Duct', InteriorBoundaryDuct)
    
    MF.compute(MFPrecomp)
  
    Ti = 0
    Tf = 10*dt
    dt_ref = dt / pow(2, n + 2)
    nbSteps_ref = int(math.ceil((Tf - Ti) / dt_ref))
  
    t = Ti
    for i in range (0, nbSteps_ref):
      if(i == nbSteps_ref - 1):
        dt_ref = Tf - t
      rk_ref.iterate(solution_ref, dt_ref, t)
      solution_ref.exportMsh('output/turbofan-%05i' % (i),  t,  i)
      t = t + dt_ref
  
    conv = []
    timestep= []
    for j in range(0, n):
      solution.interpolate(InitScaling)
      t = Ti
      dtN = dt / pow(2, j)
      nbSteps = int(math.ceil((Tf - Ti) / dtN))
      for i in range(0, nbSteps):
        if(i == nbSteps - 1):
          dtN = Tf - t
        rk.iterate(solution, dtN, t)
        t = t + dtN
      solution.axpy(solution_ref, -1)
      conv.append(solution.norm())
      timestep.append(dtN)
  
    old = conv[0]
    ideal = []
    for c in range(0, n):
      ideal.append(conv[0]/(2**(cOrder*c)))
      if(conv[0] / conv[c] < (2**(cOrder*c)) * 0.9):
        error = 1
        print(colored("%.2f - %.2e (%.2f %%)", "red") %(math.log(old/conv[c],  2), conv[c], conv[0] / (conv[c] * 2**(cOrder*c))*100))
      else:
        print(colored("%.2f - %.2e (%.2f %%)", "green") %(math.log(old/conv[c],  2), conv[c], conv[0] / (conv[c] * 2**(cOrder*c))*100))
      old = conv[c]
    print ''
   
    pl.loglog(timestep, conv, plot_colors[k], label=LGD_RK[k])
    pl.loglog(timestep, ideal, plot_ideal[k])
    pl.legend(loc='lower right', prop={'size':12})

fig.savefig('time_convergence_turbofan.png', dpi=fig.dpi)


if(error == 0):
  print "exit with succes :-)"
else:
  print "exit with failure :-("
  
sys.exit(error)
Msg.Exit(0)
