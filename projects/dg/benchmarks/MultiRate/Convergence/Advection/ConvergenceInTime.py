from dgpy import *
from math import *
import pylab as pl
from termcolor import colored
from periodicMap import *
import sys, os, time 

order = 3
dimension = 2
clscale = 1

def InitialCondition(ic, xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    val=exp(-(x*x+y*y)*200)
    ic.set(i,0,val) 

def Velocity(vel, xyz):
  for i in range(0,xyz.size1()): 
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    vel.set(i,0,0)
    vel.set(i,1,0.1) 
    vel.set(i,2,0) 
def shiftOperation(node, iPerBound) :
  n = [node[0], node[1] + 1, node[2]]
  return n

model = GModel()
if(not os.path.exists("rect%d.msh"%(order))):
  m = GModel()
  m.load("rect.geo")
  GmshSetOption('Mesh','CharacteristicLengthFactor',   clscale)
  GmshSetOption('Mesh','Algorithm',   "frontal")
  m.setOrderN(order, False, False)
  m.mesh(dimension)
  m.save('rect'+str(order)+".msh")
if(Msg.GetCommRank()==0):
  periodicBoundaryNumber = 1  
  # tag of the boundaries that will be cut
  cutTags = ["Bottom"]
  # tag of the boundaries that will pe pasted
  pasteTags = ["Top"]
  # output file
  periodicMap('rect'+str(order)+".msh", 'rect'+str(order)+"PB.msh", periodicBoundaryNumber, shiftOperation, cutTags, pasteTags, "periodicMesh.txt")
model.load('rect'+str(order)+"PB.msh")


RK_TYPES_SR = [ERK_22_C, ERK_33_C, ERK_43_C, ERK_44_C]
RK_TYPES_MR = [ERK_43_S, ERK_43_S_C, ERK_22_C, ERK_33_C, ERK_43_C, ERK_44_C]
LGD_RK_SR = ['ERK_22', 'ERK_33', 'ERK_43', 'ERK_44']
LGD_RK_MR = ['ERK_43_S', 'ERK_43_S_C', 'ERK_22_C', 'ERK_33_C', 'ERK_43_C', 'ERK_44_C']
cOrders_SR = [2, 3, 3, 4]
cOrders_MR = [3, 3, 2, 2, 2, 2]
n = 5
error = 0
plot_colors =['rs-', 'bo-', 'g^-', 'cs-', 'mo-', 'r^-', 'bs-', 'go-', 'c^-', 'ms-']
plot_ideal =['r:', 'b:', 'g:', 'c:', 'm:', 'r:', 'b:', 'g:', 'c:', 'm:']

fig = pl.figure(1, figsize=(12, 6))
for mL in [1, 1000]:
  if(mL==1):
    pl.subplot(121)
    RK_TYPES = RK_TYPES_SR
    LGD_RK = LGD_RK_SR
    cOrders = cOrders_SR
    strategy='Singlerate'
    strategy='Multirate'
  else:
    pl.subplot(122)
    RK_TYPES = RK_TYPES_MR
    LGD_RK = LGD_RK_MR
    cOrders = cOrders_MR

  pl.grid(True)
  for k in range (0, len(RK_TYPES)):
    
    groups = dgGroupCollection(model, dimension, order)
    xyz = groups.getFunctionCoordinates();
    vel = functionPython(3, Velocity, [xyz])
    nu  = functionConstant([0.000015])
    claw = dgConservationLawAdvectionDiffusion(vel, nu)
    solution = dgDofContainer(groups, claw.getNbFields())
    solution_ref = dgDofContainer(groups, claw.getNbFields())
    
    
    claw.addBoundaryCondition('Left', claw.new0FluxBoundary())
    claw.addBoundaryCondition('Right', claw.new0FluxBoundary())
    #claw.addBoundaryCondition('Top', claw.new0FluxBoundary())
    #claw.addBoundaryCondition('Bottom', claw.new0FluxBoundary())
    Init = functionPython(1, InitialCondition, [xyz])
    
    rk_ref = dgERK(claw,  None,  DG_ERK_44_3_8)
  
    RKTYPE = RK_TYPES[k]
    cOrder = cOrders[k]
    rk = dgMultirateERK(groups, claw, RKTYPE)
    dt = rk.splitGroupsForMultirate(mL,   solution, [solution, solution_ref])
    dt = 1 * dt
    print 'Method:', LGD_RK[k], 'DT', dt, rk.speedUp()
    rk.printMultirateInfo()
  
    solution.L2Projection(Init)
    solution_ref.L2Projection(Init)
  
    Ti = 0
    Tf = rk.dtMin()*64
    dt_ref = rk.dtMin()/8
    nbSteps_ref = int(ceil((Tf - Ti) / dt_ref))
  
    t = Ti
    for i in range (0, nbSteps_ref):
      if(i == nbSteps_ref - 1):
        dt_ref = Tf - t
      rk_ref.iterate(solution_ref, dt_ref, t)
      t = t + dt_ref
  
    conv = []
    timestep= []
    cputime= []
    for j in range(0, n):
      solution.L2Projection(Init)
      t = Ti
      dtN = dt / pow(2, j)
      nbSteps = int(ceil((Tf - Ti) / dtN))
      startcpu=time.time()
      for i in range(0, nbSteps):
        if(i == nbSteps - 1):
          dtN = Tf - t
        rk.iterate(solution, dtN, t)
        t = t + dtN
        endcpu=time.time()
      solution.axpy(solution_ref, -1)
      conv.append(solution.norm())
      timestep.append(dtN)
      cputime.append(endcpu-startcpu)
    
    print 'FINAL TIME=', t 
  
  
    old = conv[0]
    ideal = []
    for c in range(0, n):
      ideal.append(conv[0]/(2**(cOrder*c)))
      if(conv[0] / conv[c] < (2**(cOrder*c)) * 0.9):
        error = 1
        print(colored("%.2f - %.2e (%.2f %%)", "red") %(log(old/conv[c],  2), conv[c], conv[0] / (conv[c] * 2**(cOrder*c))*100))
      else:
        print(colored("%.2f - %.2e (%.2f %%)", "green") %(log(old/conv[c],  2), conv[c], conv[0] / (conv[c] * 2**(cOrder*c))*100))
      old = conv[c]
    print ('')
  
    pl.ylim(10e-15, 10e-1)
    pl.loglog(timestep, conv, plot_colors[k], label=LGD_RK[k])
    pl.loglog(timestep, ideal, plot_ideal[k])
    pl.legend(loc='lower right', prop={'size':12})
    #pl.suptitle(strategy)
   

fig.savefig('time_convergence_advection.png', dpi=fig.dpi)

if(error == 0):
  print ("exit with succes :-)")
else:
  print ("exit with failure :-(")
  
sys.exit(error)
