from periodicMap import *

inputMeshFilename = "squareXY_4.msh"
outputMeshFilename = "squareXY_4b.msh"
periodicBoundaryNumber = 2  

# shift operation between the two boundaries to join
def shiftOperation(node, iPerBound) :
  if iPerBound == 0 :
    n = [node[0] - 2, node[1], node[2]]
  if iPerBound == 1 :
    n = [node[0], node[1] - 2, node[2]]
  return n
# tag of the boundaries that will be cut
cutTags = ["cutX", "cutY"]
# tag of the boundaries that will pe pasted
pasteTags = ["pasteX", "pasteY"]
# output file
mapFilename = "periodicMesh.txt"


periodicMap(inputMeshFilename, outputMeshFilename, periodicBoundaryNumber, shiftOperation, cutTags, pasteTags, mapFilename)
