Point(1) = {-.125, 0, 0, .05};
Point(2) = {0.125, 0, 0, .05};
Point(3) = {.125, .5, 0, .05};
Point(4) = {-.125, .5, 0, .05};
Point(5) = {-.125, -.5, 0, .05};
Point(6) = {.125, -.5, 0, .05};
Line(1) = {5, 1};
Line(2) = {2, 6};
Line(3) = {6, 5};
Line(4) = {1, 4};
Line(5) = {4, 3};
Line(6) = {3, 2};
Line Loop(10) = {1, 4, 5, 6, 2, 3};
Plane Surface(11) = {10};

Physical Surface("Surface") = {11};
Physical Line("Bottom") = {3};
Physical Line("Left") = {1, 4};
Physical Line("Top") = {5};
Physical Line("Right") = {6, 2};

//Recombine Surface {9, 11};

Field[1] = MathEval;
Field[1].F = "0.01*1+0.01*100*(y*y+x*x)";
Background Field = 1;
