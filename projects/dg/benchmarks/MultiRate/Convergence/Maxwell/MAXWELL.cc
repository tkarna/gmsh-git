
/*
 * =====================================================================================
 *
 *       Filename:  TURBOFAN.cc
 *
 *    Description:  
 *      Computes mean flow and inlet BC for turbofan intake (slowly varying duct)
 *      - Equations: cf. S. W. Rienstra, "Sound transmission in slowly varying circular
 *        and annular lined ducts with flow", Journal of Fluid Mechanics 380, p.279-296,
 *        1999
 *      - Geometry: cf. S. W. Rienstra and W. Eversman, "A numerical comparison between
 *        the multiple-scales and finite-element solution for sound propagation in lined
 *        flow ducts", Journal of Fluid Mechanics 437, p.367-384, 2001
 *
 *        Version:  1.0
 *        Created:  22.12.2010 10:25:38
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  bseny (BS), bruno.seny@uclouvain.be
 *        Company:  UCL
 *
 * =====================================================================================
 */

#include "fullMatrix.h"
#include "function.h"
#include "dgGroupOfElements.h"
#include "polynomialBasis.h"
#include <cmath>
#include <complex>

extern "C" {

  void outletValueCC(dataCacheMap *dCM, fullMatrix<double> &f, fullMatrix<double> &time, fullMatrix<double> &xyz)
  {
    const double rhoValue=4*M_PI*1e-7;
    const double cValue=1/sqrt(4*M_PI*1e-7*8.85*1e-12);
    const double TT=2;
    const double tho=3./cValue;

    for (size_t i = 0; i < f.size1(); i++) {
      const double x = xyz(i,0);
      const double y = xyz(i,1);
      const double z = xyz(i,2);
      double value = 4*exp(-time(0, 0)*time(0, 0))/(pow(M_PI,0.5)*TT);
      f(i, 0) = value;
      f(i, 1) = value;
      f(i, 2) = 0*value;
      f(i, 3) = 0*value/(cValue*rhoValue);
      f(i, 4) = -value/(cValue*rhoValue);
      f(i, 5) = 0*value/(cValue*rhoValue);
    }

  }
}
