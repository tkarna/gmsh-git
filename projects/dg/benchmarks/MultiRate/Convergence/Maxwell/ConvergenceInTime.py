from gmshpy import *
from dgpy import *
import os
import sys
from termcolor import colored
import time
from math import *

if(Msg.GetCommRank() == 0):
  functionC.buildLibraryFromFile ("MAXWELL.cc",   "lib_maxwell.so");
Msg.Barrier()

mlib="./lib_maxwell.so"

error=0

mu0=4*pi*1e-7
eps0=8.85*1e-12
Ys=50*1e-3
rhoValue=4*pi*1e-7
cValue=1/sqrt(4*pi*1e-7*8.85*1e-12)
TT=2
tho=3./cValue
def outletValueFunc(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    tt=4*cValue*(t-tho)/TT
    value = 4*exp(-tt*tt)/(pow(pi,0.5)*TT)
    val.set(i,0,value)
    val.set(i,1,0*value)
    val.set(i,2,0*value)
    val.set(i,3,0*value/(cValue*rhoValue))
    val.set(i,4,-value/(cValue*rhoValue))
    val.set(i,5,0*value/(cValue*rhoValue))

dimension = 3
order = 1
clscale = 5

model = GModel()
if(not os.path.exists("box%d.msh"%(order))):
  m = GModel()
  m.load("box.geo")
  GmshSetOption('Mesh', 'CharacteristicLengthFactor',    clscale)
  GmshSetOption('Mesh', 'Algorithm',    "frontal")
  m.setOrderN(order,  False,  False)
  m.mesh(dimension)
  m.save("box%d.msh"%(order))
model.load ('box%d.msh'%(order))
mu      = functionConstant([mu0,mu0,mu0])
epsilon = functionConstant([eps0,eps0,eps0])

RK_TYPES = [ERK_22_C,  ERK_33_C,  ERK_43_C,  ERK_44_C,  ERK_22_S, ERK_22_S_C, ERK_43_S,  ERK_43_S_C]
RK_TYPES_NAMES = ["ERK_22_C",  "ERK_33_C",  "ERK_43_C",  "ERK_44_C", "ERK_22_S", "ERK_22_S_C", "ERK_43_S",  "ERK_43_S_C"]
cOrders = [2,  2,  2,  2,  2, 2, 3,  3]
NN = 5
mL = 1000
eroor = 0
for k in range (0,  len(RK_TYPES)):
  groups = dgGroupCollection(model, dimension, order)
  groups.splitGroupsByPhysicalTag()
  xyz = groups.getFunctionCoordinates()
  
  law = dgConservationLawMaxwell()
  law.setMu(mu)
  law.setEpsilon(epsilon)
  initialSolution = functionConstant([0,0,0,0,0,0])
  solution = dgDofContainer(groups, law.getNbFields())
  solutionRef = dgDofContainer(groups, law.getNbFields())
  solution.L2Projection(initialSolution)
  solutionRef.L2Projection(initialSolution)
  
  outletValue = functionPython(6, outletValueFunc, [xyz])
  timeT = function.getTime()
  outletValue2 = functionC(mlib,   "outletValueCC",   6,   [timeT,   xyz])
  
  law.addBoundaryCondition('Air_Bottom',  law.newBoundaryMagneticWall('Air_Bottom'))
  law.addBoundaryCondition('Air_Top',     law.newBoundaryMagneticWall('Air_Top'))
  law.addBoundaryCondition('Air_Left',    law.newBoundaryElectricWall('Air_Left'))
  law.addBoundaryCondition('Air_Right',   law.newBoundaryElectricWall('Air_Right'))
  law.addBoundaryCondition('Air_Outlet',  law.newBoundarySilverMuller('Air_Outlet'))
  #law.addBoundaryCondition('Air_Inlet',   law.newBoundarySilverMullerDirichlet('Air_Inlet', outletValue))
  law.addBoundaryCondition('Air_Inlet',   law.newBoundarySilverMullerDirichlet('Air_Inlet', outletValue2))
  
  law.addInterfaceEwall('Cav_Outlet')
  law.addInterfaceEwall('Cav_Aperture')
  law.addInterfaceEwall('Cav_Inlet_ap')
  law.addInterfaceEwall('Cav_Top')
  law.addInterfaceEwall('Cav_Bottom')
  law.addInterfaceEwall('Cav_Left')
  law.addInterfaceEwall('Cav_Right')

  rkRef = dgERK(law,   None,   DG_ERK_44_3_8)
  RKTYPE=RK_TYPES[k]
  cOrder = cOrders[k]
  rk = dgMultirateERK(groups, law, RKTYPE)
  dt = rk.splitGroupsForMultirate(mL, solution, [solution, solutionRef])
  dt=4*dt

  Ti = 0
  Tf = 5*dt
  
  dtRef = dt/pow(2, NN+1)
  nbStepsRef = int(ceil((Tf-Ti)/dtRef))
  t = Ti
  for i in range (1,nbStepsRef+1) :
    if(i == nbStepsRef):
      dtRef = Tf-t
    rkRef.iterate(solutionRef, dtRef, t)
    t = t +dtRef
  
  conv = []
  timestep= []
  for n in range(0, NN+1):
    solution.setAll(0.0)
    t=Ti
    dtN=dt/pow(2, n)
    nbSteps = int(ceil((Tf-Ti)/dtN))
    for i in range(1, nbSteps+1):
      if(i == nbSteps):
        dtN = Tf-t
      norm = rk.iterate (solution,  dtN,  t)
      t=t+dtN
    solution.axpy(solutionRef, -1)
    conv.append(solution.norm())
    timestep.append(dtN)
  
  print RK_TYPES_NAMES[k], ": Theoretical speedup =", rk.speedUp()
  old = conv[0]
  ideal = []
  for c in range(0,  NN+1):
    ideal.append(conv[0]/(2**(cOrder*c)))
    if(conv[0] / conv[c] < (2**(cOrder*c)) * 0.9):
      error = 1
      print(colored("%.2f - %.2e (%.2f %%)",  "red") %(log(old/conv[c],   2),  conv[c],  conv[0] / (conv[c] * 2**(cOrder*c))*100))
      old = conv[c]
    else:
      print(colored("%.2f - %.2e (%.2f %%)",  "green") %(log(old/conv[c],   2),  conv[c],  conv[0] / (conv[c] * 2**(cOrder*c))*100))
      old = conv[c]
  print ''

if(error == 0):
  print "exit with succes :-)"
else:
  print "exit with failure :-("
  
sys.exit(error)
Msg.Exit(0)
   
