"""     
     File Testing Mass Conservation on a simple 2D advection test case 
     where multirate explicit time integration is applied.
"""

# Import for python
from dgpy import *
from math import *
import time, os
from periodicMap import *



# File name and extensions
outputDir = "output"
FileName = "rect"

try : os.mkdir(outputDir);
except: 0;

# Mesh Parameters 
clscale = 2.0     # Scaling Factor For Mesh Generation

order=3
dim=2

# Iteration Parameters
Ti=0        # Initial Time for Simulation
Tf=10    # Final Time for Simulation
i=0

# Multirate Parameters
RKTYPE = ERK_43_S        # Type of Multirate Method to choose here below
mLmax=5
mL=1000                     # Maximum Number Of Refinement Levels for Multirate


# Physical Parameters
PI = 3.14159
Vx=0        # Advective Speed In x-direction
Vy=0.1      # Advective Speed In y-direction
Vz=0        # Advective Speed in z-direction

""" 
     Function for Initial Condition
"""

def InitialCondition(ic, xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    val=exp(-(x*x+y*y)*200)
    ic.set(i,0,val) 

""" 
     Function for Advection parameters
"""

def Velocity(vel, xyz):
  for i in range(0,xyz.size1()): 
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    vel.set(i,0,Vx)
    vel.set(i,1,Vy) 
    vel.set(i,2,Vz) 

"""
     Function for Integration
"""

def MassIntegrator(fct, sol):
  for i in range(0,sol.size1()):
    s = sol.get(i,0)
    fct.set(i,0,s) 

"""
   shift operation between the two boundaries to join
"""

def shiftOperation(node, iPerBound) :
  n = [node[0], node[1] + 1, node[2]]
  return n

print'*** Build Mesh ***'
if(Msg.GetCommRank()==0 and not os.path.exists(FileName+"%d.msh"%(order))):
  gm=GModel()
  gm.load (FileName+".geo")
  GmshSetOption('Mesh','CharacteristicLengthFactor',   clscale)
  GmshSetOption('Mesh','Algorithm',   "frontal")
  gm.setOrderN(order, False, False)
  gm.mesh(dim)
  gm.save(FileName+str(order)+".msh")

if(Msg.GetCommRank()==0):
  periodicBoundaryNumber = 1  
  # tag of the boundaries that will be cut
  cutTags = ["Paste"]
  # tag of the boundaries that will pe pasted
  pasteTags = ["Cut"]
  # output file
  periodicMap(FileName+str(order)+".msh", FileName+str(order)+"PB.msh", periodicBoundaryNumber, shiftOperation, cutTags, pasteTags, "periodicMesh.txt")

print'*** Loading the Mesh and the Model ***'
model = GModel()
model.load(FileName+str(order)+"PB.msh")

print '*** Create Group Collection ***'
groups=dgGroupCollection(model, dim, order)

print'*** Define Conservation Law ***'
xyz = groups.getFunctionCoordinates()
vel = functionPython(3, Velocity, [xyz])
nu  = functionConstant([0.000015])
law = dgConservationLawAdvectionDiffusion(vel, nu)

print '*** Add Boundary Conditions ***'
law.addBoundaryCondition('Left', law.new0FluxBoundary())
law.addBoundaryCondition('Right', law.new0FluxBoundary())
#law.addBoundaryCondition('Top', law.new0FluxBoundary())
#law.addBoundaryCondition('Bottom', law.new0FluxBoundary())
Init = functionPython(1, InitialCondition, [xyz])

print '*** Set initial condition ***'
solution = dgDofContainer(groups, 1)
solution.L2Projection(Init)

print '*** Set Multirate time integrator and Split Groups For Multirate ***'
rk = dgMultirateERK(groups, law, RKTYPE)
dt = rk.splitGroupsForMultirate(mL, solution, [solution])
dt=10*dt
solution.exportGroupIdMsh()
solution.exportMultirateGroups()
rk.printMultirateInfo()

norm=solution.norm()

print '***  Define Integrator ***'
mass=functionPython(1,  MassIntegrator, [functionSolution.get()])
integrator = dgFunctionIntegrator(groups,  mass,  solution)
totalMass=fullMatrixDouble(1, 1)
totalMassRef=fullMatrixDouble(1, 1)
integrator.compute(totalMassRef, "")

nbExport = 0
t=Ti
nbSteps=int(ceil(Tf/dt))

print ''
print '------------------------- Initializing -------------------------'
print ''
print'| MR DT    | :' , dt
print'| MR SU    | :' , rk.speedUp()
print'| MASS     | :' , totalMassRef.get(0, 0)
print'| NORM     | :' , norm
print'| NB STEPS | :' , nbSteps
print ''
print '----------------------------------------------------------------'
print ''

solution.exportMsh(outputDir+"/mr%d_%d_advection-%06d"%(RKTYPE, mL, 0),   t,  nbExport)
nbExport=nbExport+1


pr = 1   # When do you want to print information
ex = 1   # When do you want to export the solution

startcpu = time.clock()
for i in range(1, nbSteps+1):
  if(i == nbSteps):
    dt = Tf - t
  rk.iterate(solution, dt, t)
  t = t + dt
 # if (i % pr == 0): 
 #   integrator.compute(totalMass, "")
 #   print'|ITER|',i,'|T(sec)|', t, '|NORM|', solution.norm(),'|MC|',abs((totalMass.get(0, 0)-totalMassRef.get(0, 0))/totalMassRef.get(0, 0)) ,  '|CPU|',time.clock() - startcpu

 # if (i % ex == 0):
 #   solution.exportMsh(outputDir+"/mr%d_%d_advection-%06d"%(RKTYPE, mL, i), t, nbExport)
 #   nbExport = nbExport + 1
  
  print'|ITER|',i,'|T(sec)|', t, '|NORM|','|CPU|',time.clock() - startcpu

print ''
print '-------------------------     End      -------------------------'
print ''
print'| TOTAL CPU TIME       | :' , time.clock()-startcpu
print ''
print '----------------------------------------------------------------'
