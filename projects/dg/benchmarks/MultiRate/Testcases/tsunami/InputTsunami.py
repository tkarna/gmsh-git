from dgpy import *
import calendar

# Choose mesh (2K, 10K, 100K, 1M)
mesh = "2K"

# File Name
filename = "world"
outputDir = "output"
bathdir=outputDir+"/"+filename+"_bath_smooth"
bathname=outputDir+"/"+filename+"_bath_smooth"+"/"+filename+"_bath_smooth.idx"

# Mesh Parameters
order = 1
dimension = 2

# Initial & Final Time [year, month, hour, minutes, seconds]
Ti = 0
nbh=0.01
Tf = nbh*3600

# Multirate and Partition Parameters
RK_TYPE = ERK_43_S
mL = 5
algo = 5

"""
  -mrAlgo=1 -> Classical         Partitioning (same number of elements on each partition + min edge cut)
  -mrAlgo=2 -> Single Constraint Partitioning (same total weight of elements on each partition + min edge cut)
  -mrAlgo=1 -> Multi  Constraint Partitioning (same number of elements of each multirate group on each partition + min edge cut)
"""

# Print every "iter" iterations and export every "export" iterations
iter = 1
export = 1

if(Msg.GetCommRank() == 0):
  try: os.system("make")
  except: 0;
  try: os.mkdir(outputDir);
  except: 0

Msg.Barrier()

def printTime (t) :
  return "%dh%02d'%02d\"" % (t/3600, (t%3600)/60, t%60)

def printDate (t):
  date=time.gmtime(t)
  return "%02d/%02d/%04d %02dh%02d'%02d\""%(date[2],  date[1],  date[0],  date[3],  date[4],  date[5])

if (Msg.GetCommRank()==0):
  functionC.buildLibraryFromFile ("TSUNAMI.cc", "lib_tsunami.so");
  functionC.buildLibraryFromFile ("BATH.cc", "lib_bath.so");

Msg.Barrier()
tlib = "lib_tsunami.so"
blib = "lib_bath.so"
