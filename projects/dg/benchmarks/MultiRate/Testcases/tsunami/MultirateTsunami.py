from dgpy import *
from PartitionTsunami import *
from InputTsunami import *
from math import *
import time, os, sys



model = GModel()
loadAndPartitionMesh(model, filename, mL, algo)

if(not os.path.exists(bathname)):
  Msg.Fatal('No valid bathymetry file :' + bathname + ' found, please launch "rundgpy DiffuseBathymetryTsunami.py"')

stereo = dgSpaceTransformSpherical(6371220.)
groups = dgGroupCollection(model, dimension, order, stereo)
XYZ = groups.getFunctionCoordinates();
claw = dgConservationLawShallowWater2d()
bath = dgDofContainer(groups, 1);
bath.readMsh(bathname)
solution = dgDofContainer(groups, claw.getNbFields())
Finit = functionC(tlib, "initial_condition_okada_honshu",3, [XYZ])
solution.interpolate(Finit)
FCoriolis= functionC(tlib, "coriolis", 1, [XYZ])
claw.setBathymetry(bath.getFunction())
claw.setBathymetryGradient(bath.getFunctionGradient())
claw.setCoriolisFactor(FCoriolis)
claw.setIsLinear(True)
claw.setIsSpherical(1, XYZ)
wall=claw.newBoundaryWall()
claw.addBoundaryCondition("Coast", wall)


bottomDrag = functionC(tlib, "bottomDrag", 1, [function.getSolution(), bath.getFunction()])
claw.setLinearDissipation(bottomDrag)

rk = dgMultirateERK(groups, claw, RK_TYPE)
dt=rk.splitGroupsForMultirate(mL, solution, [solution, bath])
rk.printMultirateInfo(0)

evalSol=dgFunctionEvaluator(groups, solution.getFunction())

nbExport = 0
nbSteps=int(ceil((Tf-Ti)/dt))

exporterSol=dgIdxExporter(solution, outputDir+'/mr-tsunami')
norm=solution.norm()

if (Msg.GetCommRank() == 0):
  print ''
  print(colored('******************** Initializing ********************', "blue"))
  print ''
  print '     - Number of processors:', Msg.GetCommSize()
  print '     - Multirate Runge Kutta Scheme:', RK_TYPE
  print '     - Initial Time (Ti):',  printTime(Ti)
  print '     - Final Time (Tf):',  printTime(Tf)
  print '     - Reference Time Step:', printTime(dt)
  print '     - Number of Iterartions:', nbSteps
  print '     - Theoretical Speedup=', rk.speedUp()
  print '     - Norm of Solution at Ti:',  norm
  print ''
  print(colored('******************************************************', "blue"))
  print ''

t=Ti
exporterSol.exportIdx(0, t)
startcpu = time.time()
for i in range (0,nbSteps) :
  if(i == nbSteps - 1):
    dt = Tf -t
  rk.iterate(solution, dt, t)
  t = t + dt
  if ( i%iter == 0 ):
     norm=solution.norm()
     if(Msg.GetCommRank() == 0):
       print(colored('|ITER| %d',  "red")%(i))
       print(colored('------------------------------------------------------', "red"))
       print'|TIME|', printTime(t)
       print'|NORM|',norm
       print'|CPUT|',printTime(time.time() - startcpu)
       print(colored('------------------------------------------------------', "red"))
       print''
  if ( i%export  == 0 ):
    exporterSol.exportIdx(i, t)
    #solution.exportFunctionMsh(currentFunction, outputDir+'/mr_tsunami_current-%06d'%(i), t, nbExport, '')
    nbExport = nbExport + 1

Msg.Barrier()
endcpu=time.time()

if (Msg.GetCommRank() == 0):
  print ''
  print(colored('********************     End      ********************', "blue"))
  print ''
  print '     - Final Time (Tf):',  printTime(t)
  print '     - Norm of Solution at Tf:',  norm
  print '     - Total CPU Time:', endcpu-startcpu
  print ''
  print(colored('******************************************************', "blue"))
  print ''
Msg.Exit(0)
