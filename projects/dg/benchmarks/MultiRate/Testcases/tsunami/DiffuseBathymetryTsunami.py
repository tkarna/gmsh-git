from dgpy import *
from InputTsunami import *
import os

if(Msg.GetCommSize() > 1):
  Msg.Fatal("You need to run the script a first time on only one processor before you run it on more!!")

os.putenv("BATHFILE", "etopo2.bin")
model=GModel()
model.load(filename+".msh")

stereo = dgSpaceTransformSpherical(6371220.)
groups = dgGroupCollection(model, dimension, order, stereo)
XYZ = groups.getFunctionCoordinates();
bath = dgDofContainer(groups, 1);
bathS = functionC(blib, "bath", 1, [XYZ])
bath.interpolate(bathS);
nuB = functionConstant(1e6)
dlaw = dgConservationLawAdvectionDiffusion.diffusionLaw(nuB)
dlaw.addBoundaryCondition("Coast", dlaw.new0FluxBoundary())
sys = linearSystemPETScDouble()
dof = dgDofManager.newCG (groups, 1, sys)
implicitEuler = dgDIRK(dlaw, dof)
implicitEuler.iterate (bath, 1, 0)
bath.bezierCrop(10, 2e6)
bathP = functionC(blib, "diff", 1, [function.getSolution(), bath.getFunction()])
nuB = functionConstant(0)
dlaw = dgConservationLawAdvectionDiffusion.diffusionLaw(nuB)
dlaw.addBoundaryCondition("Coast", dlaw.new0FluxBoundary())
dlaw.setSource(bathP)
sys = linearSystemPETScDouble()
sys.setParameter("petscOptions", "-pc_type lu");
dof = dgDofManager.newCG (groups, 1, sys)
solver = dgSteady(dlaw, dof)
solver.getNewton().setVerb(10)
solver.getNewton().setMaxIt(6)
solver.solve(bath)
exporterBath = dgIdxExporter(bath, bathdir)
exporterBath.exportIdx()

Msg.Exit(0)
