from dgpy import *
from InputTsunami import *
from termcolor import colored
import time, math, os, sys

def loadAndPartitionMesh(model, fileName, ng, algo) :
  if (Msg.GetCommSize() > 1) :
    if(Msg.GetCommRank()== 0) :
      print(colored("*** Partition mesh with algorithm %d ***", "red")%(algo))
      m2 = GModel()
      m2.load(fileName + '.msh')
      pOpt = meshPartitionOptions()
      pOpt.setNumOfPartitions(Msg.GetCommSize())

      os.putenv("BATHFILE", "etopo2.bin")
      ster = dgSpaceTransformSpherical(6371220.)
      groups = dgGroupCollection(m2, dimension, order, ster, True)
      xyz = groups.getFunctionCoordinates();
      bath = dgDofContainer(groups, 1);

      if(not os.path.exists(bathname)):
        Msg.Fatal('No valid bathymetry file :' + bathname + ' found, please launch "rundgpy DiffuseBathymetryTsunami.py"')

      bath.readMsh(bathname)
      claw = dgConservationLawShallowWater2d()
      solution = dgDofContainer(groups, claw.getNbFields())

      Finit = functionC(tlib, "initial_condition_okada_honshu",3, [xyz])
      solution.interpolate(Finit)
      FCoriolis= functionC(tlib, "coriolis", 1, [xyz])
      bottomDrag = functionC(tlib, "bottomDrag", 1, [function.getSolution(), bath.getFunction()])

      claw.setBathymetry(bath.getFunction())
      claw.setBathymetryGradient(bath.getFunctionGradient())
      claw.setCoriolisFactor(FCoriolis)
      claw.setIsLinear(True)
      claw.setIsSpherical(1, xyz)
      claw.setLinearDissipation(bottomDrag)
      
      rk = dgMultirateERK(groups, claw, RK_TYPE)
      dt=rk.splitGroupsForMultirate(ng, solution, [solution, bath])
      rk.printMultirateInfo()

      rk.computeMultiConstrainedPartition(pOpt, algo)
      PartitionMesh(m2,  pOpt)
      rk.printPartitionInfo(Msg.GetCommSize())

      m2.save(fileName + '_partitioned.msh')
      print(colored("*** Partition Done ***", "red"))
    Msg.Barrier()
    model.load(fileName + '_partitioned.msh')
  else :
    model.load(fileName + '.msh')
