#include "fullMatrix.h"
#include "function.h"
#include "math.h"
static double Ox, Oy, dx, dy;
static int nx, ny;
static double *data;
static bool init = false;
static void initialize() {
  FILE *input = fopen("HonshuInitialCondition.bin", "r");
  double buf;
  fread (&Ox, sizeof(double), 1, input);
  fread (&Oy, sizeof(double), 1, input);
  fread (&buf, sizeof(double), 1, input);
  fread (&dx, sizeof(double), 1, input);
  fread (&dy, sizeof(double), 1, input);
  fread (&buf, sizeof(double), 1, input);
  fread (&nx, sizeof(int), 1, input);
  fread (&ny, sizeof(int), 1, input);
  fread (&buf, sizeof(int), 1, input);
  data = new double[nx*ny];
  fread (data, sizeof(double), nx*ny, input);
  init = true;
}
extern "C" {
void initial_condition_okada_honshu (dataCacheMap *, fullMatrix<double> &initialCondition, fullMatrix<double> &XYZ) {
  if (! init) initialize();
  const double R = 6371220;
  for (size_t i = 0; i< initialCondition.size1(); i++) {
    const double xi = XYZ(i,0);
    const double zeta = XYZ(i,1);
    const double x = 4*R*R*xi/(4*R*R+xi*xi + zeta*zeta);
    const double y = 4*R*R*zeta/(4*R*R+xi*xi + zeta*zeta);
    const double z = R *(4*R*R-xi*xi-zeta*zeta)/(4*R*R+xi*xi + zeta*zeta);
    const double lat = asin(z/R)*180/M_PI;
    const double lon = atan2(y,x)*180/M_PI;
    const double lon0 = 142.37;   // deg.
    const double lat0 = 38.32;    // deg.
		double E = (lon-lon0)*2*M_PI/360*cos(lat0*M_PI/180)*R/1000; 
		double N = (lat-lat0)*2*M_PI/360*R/1000;
    double rx = (E-Ox);
    double ry = (N-Oy);
    int iX = rx/dx;
    int iY = ry/dy;
		double b=0;
    double alpha = (rx - iX*dx) / dx;
    double beta = (ry - iY*dy) / dy;
		if( E>=Ox  && E<=Ox+(nx-1)*dx && N>=Oy && N<=Oy+(ny-1)*dy){
     b = (1-alpha)*(1-beta)*data[iX*ny+iY]
      +alpha*(1-beta)*data[(iX+1)*ny+iY]
      +alpha*beta*data[(iX+1)*ny+(iY+1)]
      +(1-alpha)*beta*data[iX*ny+(iY+1)];
		}
    initialCondition.set (i, 0, b);//elevation
    initialCondition.set (i, 1, 0);//vel-x
    initialCondition.set (i, 2, 0);//vel-y
  }
}
void coriolis (dataCacheMap *, fullMatrix<double> &FCT, fullMatrix<double> &stereo) {
  double alpha = 0;
  double Omega = 7.292e-5;
  double R = 6371220.;
  for (size_t i = 0; i< FCT.size1(); i++) {
    double xi = stereo(i,0);
    double beta = stereo(i,1);
    double theta = asin((4*R*R-xi*xi-beta*beta)/(4*R*R+xi*xi + beta*beta));
    FCT.set(i,0,2*Omega*sin(theta));
  }
}

void bottomDrag(dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &sol, fullMatrix<double> &bath){
  for(int i=0;  i<val.size1(); i++){
    double H = sol(i, 0)+bath(i, 0);
    double u = hypot(sol(i,1),sol(i,2));
    val.set(i, 0, 9.81*0.025*0.025*pow(H, -1.333333333333)*u);
  }
}
}
