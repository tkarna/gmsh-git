function acousBC(omega=16,m=10,n=1,meanFlow=true)

  % Computes parameters of the inlet BC for the turbofan intake case
  % Equations: cf. S. W. Rienstra, "Sound transmission in slowly varying circular and annular lined ducts with flow", Journal of Fluid Mechanics 380, p.279-296, 1999 
  % Geometry: cf. S. W. Rienstra and W. Eversman, "A numerical comparison between the multiple-scales and finite-element solution for sound propagation in lined flow ducts", Journal of Fluid Mechanics 437, p.367-384, 2001 

	close all;
	
	omega
	m
	n

	% BC at x = 0 (fan)
	R2 = 1;
	R1 = 0.64212-sqrt(0.04777);
	
	% Flow Parameters
	gam = 1.4;
	E = 2.625;
	F = 0.410299942231283;
	
	% Parameters for search for radial mode
	SamplePerFact = 3; 
	
	% Mean Flow
	if (meanFlow)
		gamm1 = gam-1;
		RSqDiff = R2.^2-R1.^2;
		Func = @(d) 0.5*(F./(d.*RSqDiff)).^2+1/gamm1.*d.^gamm1-E;
		D0 = fzero(Func,1)
		U0 = -F./(D0.*RSqDiff)
		C0 = D0.^(0.5*gamm1)
	else
		D0 = 1
		U0 = 0
		C0 = 1
	end

	% Radial wavenumber
	perEst = 2*pi/R2;
	nSample = 5*SamplePerFact*n;
	aSample = linspace(1e-5,nSample*perEst/2,nSample);
	aBracket = find(radWaveNumFunc(aSample(1:nSample-1),m,R1,R2).*radWaveNumFunc(aSample(2:nSample),m,R1,R2)<0);
	if (size(aBracket,2) >= n)
		alpha = fzero(@(a)radWaveNumFunc(a,m,R1,R2),[aSample(aBracket(n)) aSample(aBracket(n)+1)])
	else
		printf("ERROR: Not enough radial modes have been sampled\n");
	end
%	a = linspace(1,nSample*perEst/2,100); aF = radWaveNumFunc(a,m,R1,R2); plot(a,aF,'-');

	M0 = U0/C0
	sigma = sqrt(1-(C0^2-U0^2)*alpha^2/omega^2);
	if (imag(sigma) > 0.)
	  sigma = -sigma;
	end
	sigma
	mu = omega*(C0*sigma-U0)/(C0^2-U0^2)

	aR1 = alpha*R1;
	if (m == 0)
		dbj1 = -besselj(1,aR1);
		dby1 = -bessely(1,aR1);
	else
		dbj1 = 0.5*(besselj(m-1,aR1)-besselj(m+1,aR1));
		dby1 = 0.5*(bessely(m-1,aR1)-bessely(m+1,aR1));
	end
	MOverN = -dbj1/dby1

end



function res = radWaveNumFunc(alpha,m,R1,R2)

	aR1 = alpha*R1;
	aR2 = alpha*R2;
	
	if (m == 0)
		dbj1 = -besselj(1,aR1);
		dby1 = -bessely(1,aR1);
		dbj2 = -besselj(1,aR2);
		dby2 = -bessely(1,aR2);
	else
		dbj1 = 0.5*(besselj(m-1,aR1)-besselj(m+1,aR1));
		dby1 = 0.5*(bessely(m-1,aR1)-bessely(m+1,aR1));
		dbj2 = 0.5*(besselj(m-1,aR2)-besselj(m+1,aR2));
		dby2 = 0.5*(bessely(m-1,aR2)-bessely(m+1,aR2));
	end
	
	res = dbj2.*dby1-dbj1.*dby2;

end

