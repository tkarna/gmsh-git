from gmshpy import *
from dgpy import *
import os
import time
import math, cmath

RK_TYPE = ERK_43_S_C
mL=1000

if(Msg.GetCommRank() == 0):
  try : os.mkdir("output");
  except: 0;
  functionC.buildLibraryFromFile ("TURBOFAN.cc",  "lib_turbofan.so");
Msg.Barrier()
  
tlib = "lib_turbofan.so";

order = 3
dimension = 3
clscale=5

if(Msg.GetCommRank() == 0):
  print'*** Loading the mesh and the model ***'

if(not os.path.exists("turbofan-%d.msh"%(order))):
  m   = GModel  ()
  m.load("turbofan.geo")
  m.setOrderN(order,  False,  False)
  GmshSetOption('Mesh',  'CharacteristicLengthFactor',     clscale)
  m.mesh(dimension)
  OptimizeMesh(m)          
  m.save("turbofan-"+str(order)+".msh")

model   = GModel  ()
model.load('turbofan-'+str(order)+'.msh')

groups = dgGroupCollection(model, dimension, order)


xyz = function.getCoordinates()


MFPrecomp = functionC(tlib, "meanFlow", 5, [xyz])
#MFPrecomp = functionConstant([1.,0.,0.,0.,1./1.4])
MF = functionPrecomputed(groups, 2*order+1, 5)  
MF.compute(MFPrecomp)

InitScaling= functionC(tlib,  "initialScaling",  5,  [xyz])


law = dgConservationLawLEE(dimension, "", MF, None, None)


if(Msg.GetCommRank() == 0):
  print'*** setting the initial solution ***'
solution = dgDofContainer(groups, law.getNbFields())
solution.setAll(0.)

rk = dgMultirateERK(groups,  law,  RK_TYPE)
dt = rk.splitGroupsForMultirate(mL,  solution,  [solution])
rk.printMultirateInfo(0)
solution.exportGroupIdMsh()
su=rk.speedUp()

solution.interpolate(InitScaling)

if(Msg.GetCommRank() == 0):
  print'*** solve ***'
  print 'DT=', dt
  print 'SU=', su
  print'*************'


#coeff = functionConstant([16.,10.,11.771,-5.7843e-05,10.837,0]) # Without mean flow, omega = 16, m = 10, alpha = 11.771, MOverN = -5.7843e-05, mu = (10.837,0)
coeff = functionConstant([16.,10.,11.771,-5.7843e-05,27.110,0]) # With mean flow, omega = 16, m = 10, alpha = 11.771, MOverN = -5.7843e-05, mu = (27.110,0)
time = function.getTime()
ISAmp = functionC(tlib, "inletModeAmp", 8, [xyz, coeff])
amp = functionPrecomputed(groups, 2*order+1, 8)
for i in range(groups.getNbFaceGroups()) :
  group = groups.getFaceGroup(i)
  if (group.physicalTag() == 'Fan') :
    amp.compute(ISAmp,group)
IS = functionC(tlib, "inletMode", 5, [time, amp, coeff])

InletBoundary = law.newBoundaryNonReflect(IS)
OutletBoundary = law.newBoundaryNonReflect()
InteriorBoundarySpinner = law.newBoundaryWall(True)
InteriorBoundaryDuct = law.newBoundaryWall()

law.addBoundaryCondition('Fan',InletBoundary)
law.addBoundaryCondition('Intake',OutletBoundary)
law.addBoundaryCondition('Spinner',InteriorBoundarySpinner)
law.addBoundaryCondition('Duct',InteriorBoundaryDuct)

MF.compute(MFPrecomp)

#MFViz = dgDofContainer(groups, 5)
#MFViz.interpolate(MFPrecomp)
#MFViz.exportMsh('turbofan_MF')


t=0
solution.exportMsh('output/turbofan-%05i' % 0, 0, 0)
i=0
timer=dgTimer.root()
timer.reset()
Msg.Barrier()
timer.start()
while (t < 1.86*2) :
  i += 1
  rk.iterate (solution,  dt,  t)
  t = t + dt
  timer.stop()
  Msg.Barrier()
  norm=solution.norm()
  if(Msg.GetCommRank() == 0):
    print("Iter %i t=%.15f norm=%.15f cpu=%.15f" % (i,t, norm, timer.get()))
  Msg.Barrier()
  timer.reset()
  Msg.Barrier()
  timer.start()
  if (i % 40 == 0) :
    solution.exportMsh('output/turbofan-%05i' % (i), t, i)

Msg.Exit(0)
