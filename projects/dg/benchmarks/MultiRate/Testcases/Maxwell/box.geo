//Mesh.Dual = 1;
//Mesh.Voronoi=1;

lc = 0.1;
Point(1) = {-0.5,-0.5,-0.5,lc};         
Point(2) = {-0.5,0.5,-0.5,lc};         
Point(3) = {0.5,0.5,-0.5,lc};         
Point(4) = {0.5,-0.5,-0.5,lc};     
Line(1) = {4,3};         
Line(2) = {3,2};         
Line(3) = {2,1};         
Line(4) = {1,4};    

Point(11) = {-0.5,-0.5,0.5,lc};         
Point(22) = {-0.5,0.5,0.5,lc};         
Point(33) = {0.5,0.5,0.5,lc};         
Point(44) = {0.5,-0.5,0.5,lc};         
Line(11) = {44,33};         
Line(22) = {33,22};         
Line(33) = {22,11};     
Line(44) = {11,44};   

Line(111)={1, 11};
Line(222)={22, 2};
Line(333)={3, 33};
Line(444)={44, 4};

//Point(101)={-0.05, -0.0025, 0.3, lc/10};
//Point(102)={-0.05, 0.0025, 0.3, lc/10};
//Point(103)={0.05, 0.0025, 0.3, lc/10};
//Point(104)={0.05, -0.0025, 0.3, lc/10};
//Line(101) = {104,103};         
//Line(102) = {103,102};         
//Line(103) = {102,101};         
//Line(104) = {101,104};

Point(1001) = {-2,-2,2,lc};         
Point(1002) = {-2,2,2,lc};         
Point(1003) = {2,2,2,lc};         
Point(1004) = {2,-2,2,lc};  

Line(1001) = {1004,1003};         
Line(1002) = {1003,1002};         
Line(1003) = {1002,1001};         
Line(1004) = {1001,1004};

Point(1011) = {-2,-2,-2,lc};         
Point(1022) = {-2,2,-2,lc};         
Point(1033) = {2,2,-2,lc};         
Point(1044) = {2,-2,-2,lc};  

Line(1011) = {1044,1033};         
Line(1022) = {1033,1022};         
Line(1033) = {1022,1011};         
Line(1044) = {1011,1044};

Line(1111)={1001, 1011};
Line(1222)={1002, 1022};
Line(1333)={1033, 1003};
Line(1444)={1004, 1044};
    
Line Loop(5) = {2,3,4,1};         
Plane Surface(6) = {5};  

//Line Loop(105) = {102,103,104,101};         
//Plane Surface(106) = {105};

Line Loop(55) = {22,33,44,11}; 
Plane Surface(66) = {55};

Line Loop(555)={222, -2, 333, 22};
Plane Surface(2233)={555};

Line Loop(666)={444,-4,111,44};
Plane Surface(4411)={666};

Line Lopp(777)={222, 3, 111, -33};
Plane Surface(3321)={777};

Line Lopp(888)={444,1,333, -11};
Plane Surface(1143)={888};

Line Loop(1055) = {1002,1003,1004,1001}; 
Plane Surface(1234)={1055};

Line Loop(1066)={1444, 1011,1333,-1001};
Plane Surface(4131)={1066};

Line Loop(1077)={1333, 1002, 1222, -1022};
Plane Surface(3222)={1077};

Line Loop(1088)={1222, 1033, -1111, -1003};
Plane Surface(2313)={1088};

Line Loop(1099)={1444, -1044, -1111, 1004};
Plane Surface(4414)={1099};

Line Loop(1100) = {1022,1033,1044,1011}; 
Plane Surface(1459)={1100};

Surface Loop(1111)={6, 66, 2233, 4411,3321,1143};
Volume(10001)={1111};

Surface Loop(1112)={4414,2313,4131,3222,1459,1234};
Volume(10002)={1112,-1111};

Physical Surface("Cav_Outlet")={6};
Physical Surface("Cav_Inlet_ap")={66};
Physical Surface("Air_Inlet")={1234};
Physical Surface("Air_Top")={3222};
Physical Surface("Cav_Top")={3321};
Physical Surface("Cav_Bottom")={1143};
Physical Surface("Air_Bottom")={4414};
Physical Surface("Air_Left")={4131};
Physical Surface("Cav_Left")={2233};
Physical Surface("Air_Right")={2313};
Physical Surface("Air_Outlet")={1459};
Physical Surface("Cav_Right")={4411};
Physical Volume("Air")={10002};
Physical Volume("Cavity")={10001};






          
