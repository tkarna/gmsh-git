from gmshpy import *
from dgpy import *
import os
import time
import math


dimension = 3
order = 1
clscale = 5
print'---- Load Mesh & Build Groups'

model = GModel()
if(not os.path.exists("box%d.msh"%(order))):
  m = GModel()
  m.load("box.geo")
  GmshSetOption('Mesh', 'CharacteristicLengthFactor',    clscale)
  GmshSetOption('Mesh', 'Algorithm',    "frontal")
  m.setOrderN(order,  False,  False)
  m.mesh(dimension)
  m.save("box%d.msh"%(order))
model.load ('box%d.msh'%(order))

try : os.mkdir("output");
except: 0;

groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()


print'---- Load Conservation law with parameters and source term'

law = dgConservationLawMaxwell()
mu0=4*math.pi*1e-7
eps0=8.85*1e-12
Ys=50*1e-3
mu      = functionConstant([mu0,mu0,mu0])
epsilon = functionConstant([eps0,eps0,eps0])
law.setMu(mu)
law.setEpsilon(epsilon)
rhoValue=4*math.pi*1e-7
cValue=1/math.sqrt(4*math.pi*1e-7*8.85*1e-12)
#incidentFields = functionConstant([0,0,0,0,0,0])
#sourceFields   = functionConstant([0,0,0,0,0,0])
#law.setIncidentField(incidentFields)
#law.setSourceField(sourceFields)


print'---- Load Initial conditions'

initialSolution = functionConstant([0,0,0,0,0,0])
solution = dgDofContainer(groups, law.getNbFields())
solution.L2Projection(initialSolution)
solution.setFieldName(0,'E_x')
solution.setFieldName(1,'E_y')
solution.setFieldName(2,'E_z')
solution.setFieldName(3,'H_x')
solution.setFieldName(4,'H_y')
solution.setFieldName(5,'H_z')
#solution.exportMsh("output/Cuboid-00000", 0, 0)


print'---- Load Boundary conditions'

t=0

#freq=1
#wc=1400*math.pi
#tho=5e-9
#k=1/(6.28e10)
#x0=-0.5
#aa=1e16/4/math.log(100)
#aa=math.log(100)*3e8*3e8/0.75/0.75
#w=1400*math.pi*1e6
#wc=100*math.pi*1e6
#tho=5e-8
#k=1/(6.28e10)
#z0=0.4


TT=2
tho=3./cValue
def outletValueFunc(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    #tt=t-tho
    #if t<=0.5e-8 :
    #  value=math.sin(2*math.pi*1e8*t-(z-3.))
    #else :
    #  value=0
    #if tt>=-1e-15 and tt<=1e-15:
    #  value=k*wc
    #else:
    #  value=k/tt*math.sin(wc*tt-(z-z0))*math.cos(w*tt)
    #value = 10*math.exp(-(100*t-1)*(100*t-1))*math.cos(2*math.pi*freq*(t-z))
    #value = math.sin(2*math.pi*1e9*t-(z-0.4))
    tt=4*cValue*(t-tho)/TT
    #value = math.exp(-aa*tt*tt)
    value = 4*math.exp(-tt*tt)/(math.pow(math.pi,0.5)*TT)
    #value = math.exp( - math.pow(t-0.6,2) / (2*math.pow(0.2,2)) )
    val.set(i,0,value)
    val.set(i,1,0*value)
    val.set(i,2,0*value)
    val.set(i,3,0*value/(cValue*rhoValue))
    val.set(i,4,-value/(cValue*rhoValue))
    val.set(i,5,0*value/(cValue*rhoValue))
outletValue = functionPython(6, outletValueFunc, [xyz])



#law.addBoundaryCondition('Cav_Bottom',  law.newBoundaryElectricWall('Cav_Bottom'))
#law.addBoundaryCondition('Cav_Top',     law.newBoundaryElectricWall('Cav_Top'))
#law.addBoundaryCondition('Cav_Left',    law.newBoundaryElectricWall('Cav_Left'))
#law.addBoundaryCondition('Cav_Right',   law.newBoundaryElectricWall('Cav_Right'))
#law.addBoundaryCondition('Cav_Outlet',  law.newBoundaryElectricWall('Cav_Outlet'))
#law.addBoundaryCondition('Cav_Inlet_ap',law.newBoundaryElectricWall('Cav_Inlet_ap'))

law.addBoundaryCondition('Air_Bottom',  law.newBoundaryMagneticWall('Air_Bottom'))
law.addBoundaryCondition('Air_Top',     law.newBoundaryMagneticWall('Air_Top'))
law.addBoundaryCondition('Air_Left',    law.newBoundaryElectricWall('Air_Left'))
law.addBoundaryCondition('Air_Right',   law.newBoundaryElectricWall('Air_Right'))
law.addBoundaryCondition('Air_Outlet',  law.newBoundarySilverMuller('Air_Outlet'))
law.addBoundaryCondition('Air_Inlet',   law.newBoundarySilverMullerDirichlet('Air_Inlet', outletValue))


#law.addBoundaryCondition('Air_Bottom',  law.newBoundarySilverMuller('Air_Bottom'))
#law.addBoundaryCondition('Air_Top',     law.newBoundarySilverMuller('Air_Top'))
#law.addBoundaryCondition('Air_Left',    law.newBoundarySilverMuller('Air_Left'))
#law.addBoundaryCondition('Air_Right',   law.newBoundarySilverMuller('Air_Right'))
#law.addBoundaryCondition('Air_Outlet',  law.newBoundarySilverMuller('Air_Outlet'))
#law.addBoundaryCondition('Air_Inlet',   law.newBoundarySilverMullerDirichlet('Air_Inlet', outletValue))
law.addInterfaceEwall('Cav_Outlet')
law.addInterfaceEwall('Cav_Aperture')
law.addInterfaceEwall('Cav_Inlet_ap')
law.addInterfaceEwall('Cav_Top')
law.addInterfaceEwall('Cav_Bottom')
law.addInterfaceEwall('Cav_Left')
law.addInterfaceEwall('Cav_Right')
#law.addBoundaryCondition('Inlet',   law.newBoundaryDirichletOnE('Inlet', outletValue))
#law.addBoundaryCondition('Outlet',  law.newBoundaryElectricWall('Outlet'))
#law.addBoundaryCondition('Top1',  law.newBoundaryElectricWall('Top1'))
#law.addBoundaryCondition('Top2',  law.newBoundaryElectricWall('Top2'))
#law.addBoundaryCondition('Bottom1',     law.newBoundaryElectricWall('Bottom1'))
#law.addBoundaryCondition('Bottom2',     law.newBoundaryElectricWall('Bottom2'))
#law.addBoundaryCondition('Left1',    law.newBoundaryMagneticWall('Left1'))
#law.addBoundaryCondition('Left2',    law.newBoundaryMagneticWall('Left2'))
#law.addBoundaryCondition('Right1',   law.newBoundaryMagneticWall('Right1'))
#law.addBoundaryCondition('Right2',   law.newBoundaryMagneticWall('Right2'))
incidField1=fullMatrixDouble(1,6)
incidField2=fullMatrixDouble(1,6)
incidField3=fullMatrixDouble(1,6)
incidField4=fullMatrixDouble(1,6)
incidField5=fullMatrixDouble(1,6)
incidField6=fullMatrixDouble(1,6)



def psolShield(val,xyz, solution):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    val.set(i,0,solution.get(i,0))
    val.set(i,1,solution.get(i,1))
    val.set(i,2,solution.get(i,2))
    val.set(i,3,solution.get(i,3))
    val.set(i,4,solution.get(i,4))
    val.set(i,5,solution.get(i,5))
psol = functionPython(6,psolShield,[xyz, solution.getFunction()])
incidfield = dgFunctionEvaluator(groups, psol)
print'---- LOOP'
#sys = linearSystemPETScBlockDouble()
#sys.setParameter("petscOptions", "-pc_type lu")
#sys.setParameter("petscOptions", "-ksp_type bicg")
#dof = dgDofManager.newDGBlock(groups, 6, sys)
#solver=dgDIRK(law,dof,1)


#solver = dgERK(law, None, DG_ERK_44)


# Multirate Parameters
RKTYPE = ERK_22_S        # Type of Multirate Method to choose here below
mL=1000                # Maximum Number Of Refinement Levels for Multirate
solver = dgMultirateERK(groups, law, RKTYPE)
dt = solver.splitGroupsForMultirate(mL, solution, [solution])
solver.printMultirateInfo()
solution.exportMultirateTimeStep()
solution.exportMultirateGroups()
print 'SU=', solver.speedUp()
print 'dtMin=', solver.dtMin(), 'dtMax=', solver.dtMax(), 'dtRef', solver.dtRef()
tic = time.clock()
#dt =5e-12
numoutput = 1
for i in range(0,5000):
  t = dt*i
  x = time.clock()
  solver.iterate(solution, dt, t)
  norm = solution.norm()
  if (i % 10 == 0):
    print'|ITER|',i,'/ 50000 |NORM|',norm, '|DT|',dt, '|T|',t, '|CPU|',time.clock()-x
    solution.exportMsh("output/Cuboid-%05i" % numoutput, t, numoutput)
    numoutput = numoutput+1
    incidfield.compute(0.50001, 0.0,0.0, incidField1)
    incidfield.compute(-0.50001, 0.0,0.0, incidField2)
    incidfield.compute(0., 0.50001,0.0, incidField3)
    incidfield.compute(0.,-0.50001,0.0, incidField4)
    incidfield.compute(0., 0.0,0.50001, incidField5)
    incidfield.compute(0., 0.0,-0.50001, incidField6)
    #incidfield.compute(0., 0.0,0.2, incidFieldo)
    #incidfield.compute(0, 0.0,0.5, incidFieldb)
  if (i % 10== 0 and Msg.GetCommRank() == 0):
    print '|ITER|', i, 'cpu', time.clock()-tic
    print("%.8e  %.8e %.8e %.8e %.8e %.8e  %.8e \n"% (t, incidField1(0,0),incidField1(0,1),incidField1(0,2),incidField1(0,3),incidField1(0,4),incidField1(0,5)))
    print("%.8e  %.8e %.8e %.8e %.8e %.8e  %.8e \n"% (t, incidField2(0,0),incidField2(0,1),incidField2(0,2),incidField2(0,3),incidField2(0,4),incidField2(0,5)))
    print("%.8e  %.8e %.8e %.8e %.8e %.8e  %.8e \n"% (t, incidField3(0,0),incidField3(0,1),incidField3(0,2),incidField3(0,3),incidField3(0,4),incidField3(0,5)))
    print("%.8e  %.8e %.8e %.8e %.8e %.8e  %.8e \n"% (t, incidField4(0,0),incidField4(0,1),incidField4(0,2),incidField4(0,3),incidField4(0,4),incidField4(0,5)))
    print("%.8e  %.8e %.8e %.8e %.8e %.8e  %.8e \n"% (t, incidField5(0,0),incidField5(0,1),incidField5(0,2),incidField5(0,3),incidField5(0,4),incidField5(0,5)))
    print("%.8e  %.8e %.8e %.8e %.8e %.8e  %.8e \n"% (t, incidField6(0,0),incidField6(0,1),incidField6(0,2),incidField6(0,3),incidField6(0,4),incidField6(0,5)))
    fout_shield=open("J1.dat","a")
    fout_shield.write("%.8e  %.8e %.8e %.8e %.8e %.8e  %.8e \n"% (t, incidField1(0,0),incidField1(0,1),incidField1(0,2),incidField1(0,3),incidField1(0,4),incidField1(0,5)))
    fout_shield.close()
    fout_shield=open("J2.dat","a")
    fout_shield.write("%.8e  %.8e %.8e %.8e %.8e %.8e  %.8e \n"% (t, incidField2(0,0),incidField2(0,1),incidField2(0,2),incidField2(0,3),incidField2(0,4),incidField2(0,5)))
    fout_shield.close()
    fout_shield=open("J3.dat","a")
    fout_shield.write("%.8e  %.8e %.8e %.8e %.8e %.8e  %.8e \n"% (t, incidField3(0,0),incidField3(0,1),incidField3(0,2),incidField3(0,3),incidField3(0,4),incidField3(0,5)))
    fout_shield.close()
    fout_shield=open("J4.dat","a")
    fout_shield.write("%.8e  %.8e %.8e %.8e %.8e %.8e  %.8e \n"% (t, incidField4(0,0),incidField4(0,1),incidField4(0,2),incidField4(0,3),incidField4(0,4),incidField4(0,5)))
    fout_shield.close()
    fout_shield=open("J5.dat","a")
    fout_shield.write("%.8e  %.8e %.8e %.8e %.8e %.8e  %.8e \n"% (t, incidField5(0,0),incidField5(0,1),incidField5(0,2),incidField5(0,3),incidField5(0,4),incidField5(0,5)))
    fout_shield.close()
    fout_shield=open("J6.dat","a")
    fout_shield.write("%.8e  %.8e %.8e %.8e %.8e %.8e  %.8e \n"% (t, incidField6(0,0),incidField6(0,1),incidField6(0,2),incidField6(0,3),incidField6(0,4),incidField6(0,5)))
    fout_shield.close()
