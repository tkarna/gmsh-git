L = -0.3;
W = 2;
H1 = 0.1;
H2 = H1 + 0.9;
lc = 0.1;

Point(1) = {L ,0,0,lc};
Point(2) = {0,0,0,lc};
Point(3) = {W,0,0,lc};
Point(4) = {W,H1,0,lc};
Point(5) = {W,H2,0,lc};
Point(6) = {0,H2,0,lc};
Point(7) = {L,H2,0,lc};
Point(8) = {L,H1,0,lc};
Point(9) = {0,H1,0,lc};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 5};
Line(5) = {5, 6};
Line(6) = {6, 7};
Line(7) = {7, 8};
Line(8) = {8, 1};
Line(9) = {4, 9};
Line(10) = {6, 9};
Line(11) = {8, 9};
Line(12) = {2, 9};

Line Loop(1) = {1, 12, -11, 8};
Line Loop(2) = {2, 3, 9, -12};
Line Loop(3) = {4, 5, 6, 7,11,-9};
Plane Surface(1) = {1};
Plane Surface(2) = {2};
Plane Surface(3) = {3};

Transfinite Line{3,12,-8} = 7 Using Progression 1.6;
Transfinite Line{4,-10,-7} = 8 Using Progression 1.3;

Transfinite Line{-1,-11,6} = 7 Using Progression 1.6;
Transfinite Line{2,-9,-5} = 32 Using Progression 1.1;

Transfinite Surface{1} = {1,2,9,8};
Transfinite Surface{2} = {2,3,4,9};
Transfinite Surface{3} = {4,5,7,8};

//Smoother Surface{4} = 100;

// Recombine the triangles into quads
Recombine Surface{1,2};

// Apply an elliptic smoother to the grid
//Mesh.Smoothing = 100;

Physical Surface("Air") = {1,2,3};
Physical Line("Wall") = {2};
Physical Line("Inlet") = {7,8};
Physical Line("Top") = {5,6};
Physical Line("Outlet") = {3,4};
Physical Line("Left") = {1};
//Physical Point("CornerBottomLeft") = {7};
Field[1] = AttractorAnisoCurve;
