from gmshpy import *
from dgpy import *
from InputBlasius import *
import os, time, math, sys

outputDir = outputDir+'_'+str(order)+'o_'+str(sys.argv[2])+'m_'+str(Msg.GetCommSize())+'p'

if(Msg.GetCommRank() == 0):
  os.system("mkdir -p "+outputDir)
  os.system("rm -fr "+outputDir+"/*")
  os.system("mkdir "+outputDir+"/meshes")
  os.system('cp multirateGroups.msh multirateTimeStep.msh groupsId.msh '+geoname+'.geo '+meshname+'.msh '+meshname+'_%dm_%dp.msh'%(int(sys.argv[2]),  Msg.GetCommSize())+" " +outputDir+"/meshes")

model = GModel()

if(Msg.GetCommSize() > 1):
  model.load (meshname+'_%dm_%dp.msh'%(int(sys.argv[2]),  Msg.GetCommSize()))
else:
  model.load (meshname+'.msh')

groups = dgGroupCollection(model,dimension,order)
xyz = groups.getFunctionCoordinates()

law = dgPerfectGasLaw(dimension)
law.setGasConstantAndGamma(R, Gamma)
muF = functionConstant(mu)
kF = functionConstant(k)
law.setViscosityAndThermalConductivity(muF, kF)

Msg.Info('*** setting the initial solution ***')
solution = dgDofContainer(groups, law.getNbFields())
proj = dgDofContainer(groups, 1)
proju = dgDofContainer(groups, 1)
rotF = functionPython(1, rot, [solution.getFunctionGradient(), xyz])
uF = functionPython(1, vit, [solution.getFunction(), xyz])

#FS = functionPython(4, free_stream, [xyz])
FS = functionC("./ccodeInitial.so","freeStream",4,[xyz])
#TS = functionPython(4, top_stream, [solution.getFunction()])
TS = functionC("./ccodeInitial.so","topStream",4,[solution.getFunction()])
solution.L2Projection(FS)

wallBoundary = law.newNonSlipWallBoundary()
inletBoundary = law.newOutsideValueBoundary("",FS)
outletBoundary = law.newSymmetryBoundary('')
topBoundary = law.newOutsideValueBoundary("",TS)
leftBoundary = law.newSlipWallBoundary()
rightBoundary = leftBoundary

law.addBoundaryCondition('Wall',wallBoundary)
law.addBoundaryCondition('Inlet',inletBoundary)
law.addBoundaryCondition('Outlet',outletBoundary)
law.addBoundaryCondition('Top',topBoundary)
law.addBoundaryCondition('Left',leftBoundary)
law.addBoundaryCondition('Right',rightBoundary)

rk = dgMultirateERK(groups,law,RKTYPE)

t=Ti
nbExport=0
solution.exportMsh(outputDir+"/blasius-%06d"% (0),t, nbExport,'blasius')
proj.L2Projection(rotF)
proj.exportMsh(outputDir+"/rot-%06d" %(0),t, nbExport,'rot')
proju.L2Projection(uF)
proju.exportMsh(outputDir+"/u-%06d" %(0),t, nbExport,'u')
#solution.exportFunctionMsh(law.getVelocity(), outputDir+'/vel-%06d'%(0), t, nbExport,'velocity')
groups.exportFunctionMsh(law.getVelocity(),outputDir+'/vel-%06d'%(0),t,nbExport,'velocity',solution)

dt = rk.splitGroupsForMultirate(int(sys.argv[2]),solution,[solution, proj, proju],fact)
dt = multDt*dt
rk.printMultirateInfo()

Msg.Info('Multirate Speedup=%f'%(rk.speedUp()))
Msg.Info('MR DT = %.10f, DTMIN=%.10f, DTMAX=%.10f'%(dt, rk.dtMin(),rk.dtMax()))

if(Msg.GetCommRank() == 0):
    saveParameters(outputDir+'/meshes/',dt,rk.dtMin(),rk.dtMax(),meshname+'_%dm_%dp.msh'%(int(sys.argv[2]),  Msg.GetCommSize()),
                   ['Wall',wallBoundary.getType(),'Inlet',inletBoundary.getType(),'Outlet',outletBoundary.getType(),'Top',topBoundary.getType(),'Left',leftBoundary.getType(),'Right',rightBoundary.getType()])


Msg.Info('*** solve ***')
nbExport += 1
x = time.clock()

for i in range(1,nbSteps+1) :
  rk.iterate(solution, dt, t)
  t = t+dt
  if (i % nbStepsSave == 0) :
    norm = solution.norm()
    if(Msg.GetCommRank() == 0):
      print('|ITER| %3d , |NORM| %.3f |DT| %.3e |T| %.3e |CPU| %7.3f |S| %.3e'%(i,norm,dt,t,time.clock()-x,t/(time.clock()-x)))
    solution.exportMsh(outputDir+"/blasius-%06d"% (i),t,nbExport,'blasius')
    proj.L2Projection(rotF)
    proj.exportMsh(outputDir+"/rot-%06d" %(i),t,nbExport,'rot')
    proju.L2Projection(uF)
    proju.exportMsh(outputDir+"/u-%06d" %(i),t,nbExport,'u')
    #solution.exportFunctionMsh(law.getVelocity(),outputDir+'/vel-%06d'%(i),t,nbExport,'velocity')
    groups.exportFunctionMsh(law.getVelocity(),outputDir+'/vel-%06d'%(i),t,nbExport,'velocity',solution)
    nbExport = nbExport + 1
        #if (i % (10 * nbStepsSave) == 0) :
#validateBlasius(groups,solution)
          
#validateBlasius(groups,solution)

#os.system("gmsh "+meshname+".msh output/*")


Msg.Exit(0)

