
lc = 3;

Point(1) = {0,0,0,lc/15.0};
Point(2) = {35,0,0,lc/10.0};
Point(3) = {35,100,0,lc*2};
Point(4) = {-20,100,0,lc*1.5};
Point(5) = {-20,0,0,lc*2};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 5};
Line(5) = {5, 1};

Line Loop(1) = {1, 2, 3, 4, 5};
Plane Surface(1) = {1};

Physical Surface("All") = {1};
Physical Line("Wall") = {1};
Physical Line("Inlet") = {3,4};
Physical Line("Outlet") = {2};
Physical Line("Symmetry") = {5};
Physical Point("CornerBottomLeft") = {5};
Field[1] = AttractorAnisoCurve;
