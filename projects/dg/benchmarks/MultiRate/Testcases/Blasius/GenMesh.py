from gmshpy import *
from dgpy import *
from InputBlasius import *
import os, time, math, sys

os.system("rm "+meshname+".msh")

m = GModel()
m.load(geoname+".geo")
GmshSetOption('Mesh', 'CharacteristicLengthFactor',    clscale)
#GmshSetOption('Mesh', 'Algorithm',    "frontal")
m.mesh(dimension)
if(order>1) :
  m.setOrderN(order,  False,  False)
m.save(meshname+".msh")

os.system("gmsh "+meshname+ ".msh")

Msg.Exit(0)

