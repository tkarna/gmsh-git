from dgpy import *
from ProjectMesh import *
import calendar, os, time

# Choose mesh (2K, 10K, 100K, 1M)
#mesh = "2K"
mesh = "100K"

# File Name
filename = "gbr"+mesh
outputDir = "output"
bathdir=outputDir+"/"+filename+"_bath_smooth"
bathname=outputDir+"/"+filename+"_bath_smooth"+"/"+filename+"_bath_smooth.idx"
outputScratch="/scratch/bseny/gbr_simulation/simu1/"


# Mesh Parameters
order = 3
dimension = 2

# Initial & Final Time [year, month, day, hour, minutes, seconds]
Ti = calendar.timegm([2000, 4, 1, 0, 0, 0])
Tf = calendar.timegm([2000, 4, 4, 0, 0, 0])

# Multirate and Partition Parameters
RK_TYPE = ERK_43_S
mL = 1000
algo = 5

"""
  -mrAlgo=1 -> Classical         Partitioning (same number of elements on each partition + min edge cut)
  -mrAlgo=2 -> Single Constraint Partitioning (same total weight of elements on each partition + min edge cut)
  -mrAlgo=1 -> Multi  Constraint Partitioning (same number of elements of each multirate group on each partition + min edge cut)
"""

iter = 10
export = 300

# Physical parameteres for bathymetry smoothing
bNu=20    # Diffusion
bT=10000  # Time for smoothing 


# Physical parameteres for GBR simulation
diff=0
gbrDiff=1

"""
  -gbrDiff=0 -> Constant Diffusion diff
  -gbrDiff=1 -> Parametrization of Smagorinsky 
"""

# Forcings for GBR
gbrWind=1

"""
  -gbrWind=0 -> Sinusoidal wind
  -gbrWind=1 -> Wind data from netcdf 
"""

gbrTide=1

"""
  -gbrTide=0 -> Sinusoidal tide
  -gbrTide=1 -> Tide data from tpxo 
"""


# Lauch Makefile to generate meshes and download forcings
if(Msg.GetCommRank() == 0):
  try : os.system("make "+filename+".msh");
  except: 0;
  try : os.system("make forcings");
  except: 0;
  try : os.mkdir(outputDir);
  except: 0;
Msg.Barrier()

# Project mesh in planar space
if(not os.path.exists(filename+"_tan"+".msh")):
  m = GModel()
  m.load(filename+".msh")
  projectMesh(m)
  m.save(filename+"_tan"+".msh")
Msg.Barrier()

if (Msg.GetCommRank() == 0):
  functionC.buildLibraryFromFile ("BATH.cc", "./lib_bath.so");
  functionC.buildLibraryFromFile ("GBR.cc", "./lib_gbr.so");

Msg.Barrier()

glib = "./lib_gbr.so";
blib= "./lib_bath.so";

# Definition of Python functions
def printTime (t) :
  return "%dh%02d'%02d\"" % (t/3600,  (t%3600)/60,  t%60)

def printDate (t):
  date=time.gmtime(t)
  return "%02d/%02d/%04d %02dh%02d'%02d\""%(date[2], date[1], date[0], date[3], date[4], date[5])


def massIntegrator(fct,  sol, bath):
  for i in range(0,fct.size1()):
    s=sol.get(i, 0)
    depth=bath.get(i, 0)
    fct.set(i, 0, s+depth)
