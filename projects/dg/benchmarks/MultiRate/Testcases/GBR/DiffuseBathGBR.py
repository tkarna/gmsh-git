from dgpy import *
from InputGBR import *
from ProjectMesh import *
import os

if(Msg.GetCommSize() > 1):
  Msg.Fatal("You need to run the script a first time on only one processor before you run it on more!!")
model=GModel()
model.load(filename+"_tan"+".msh")

groups = dgGroupCollection(model, dimension, order)
XYZ = function.getCoordinates()
bath = dgDofContainer(groups, 1);
lonLat = functionC(glib, "lonLat", 3, [XYZ])
bathS = functionC(blib, "bath", 1, [lonLat])
bath.interpolate(bathS);
nuB = functionConstant(bNu)
dlaw = dgConservationLawAdvectionDiffusion.diffusionLaw(nuB)
dlaw.addBoundaryCondition("Coast", dlaw.new0FluxBoundary())
dlaw.addBoundaryCondition("Islands", dlaw.new0FluxBoundary())
dlaw.addBoundaryCondition("Open Sea North", dlaw.new0FluxBoundary())
dlaw.addBoundaryCondition("Open Sea Central", dlaw.new0FluxBoundary())
dlaw.addBoundaryCondition("Open Sea South", dlaw.new0FluxBoundary())
sys = linearSystemPETScDouble()
dof = dgDofManager.newCG (groups, 1, sys)
implicitEuler = dgDIRK(dlaw, dof)
implicitEuler.iterate (bath, bT, 0)
sys = 0
dof = 0
exporterBath = dgIdxExporter(bath, bathdir)
exporterBath.exportIdx()
Msg.Exit(0)
