from gmshpy import *
from dgpy import *
import os, time,  math,  sys

order = int(sys.argv[1]) 
dimension = 3

meshname='sd7003_'+str(order)+'o'

RKTYPE = ERK_43_S
algo = 5
partitionMethod = 3
fact = 1.0
mLmax=16
tolerance = 0.02

# Physics
Mach = 0.1
Reynolds = 100000
Prandtl = 1

#outputDir='simu_3D_RE'+str(Reynolds)+'_P'+str(order)
outputDir='/scratch/bseny/sd7003_simulation/simu_3D_RE'+str(Reynolds)+'_P'+str(order)

try: os.mkdir(outputDir)
except: 0;

D = 1
P = 101325
R = 287.1
T = 293.15
Gamma = 1.4

Cp = Gamma * R / (Gamma-1)
rho = P / ( R * T)
c = math.sqrt(Gamma * R * T)
V = Mach * c
mu  = rho * V * D / Reynolds
k = mu * Cp / Prandtl

Ti = 0
Tf = 10
def rot(f, sol_grad, x) :
  for i in range(0,sol_grad.size1()) :
    dudx = sol_grad.get(i,1*3+0)
    dudy = sol_grad.get(i,1*3+1)
    dudz = sol_grad.get(i,1*3+2)
    dvdx = sol_grad.get(i,2*3+0)
    dvdy = sol_grad.get(i,2*3+1)
    dvdz = sol_grad.get(i,2*3+2)
    dwdx = sol_grad.get(i,3*3+0)
    dwdy = sol_grad.get(i,3*3+1)
    dwdz = sol_grad.get(i,3*3+2)
    f.set(i,0,dwdy-dvdz) 
    f.set(i,1,dudz-dwdx) 
    f.set(i,2,dvdx-dudy) 

def free_stream(FCT,  XYZ) :
  for i in range (0,XYZ.size1()) :
    FCT.set(i,0,rho) 
    FCT.set(i,1,rho*V) 
    FCT.set(i,2,0.0) 
    FCT.set(i,3,0.0) 
    FCT.set(i,4, 0.5*rho*V*V+P/(Gamma-1)) 
