from gmshpy import *
from dgpy import *
from InputSD7003 import *
from math import *
import os, time, sys


Msg.Info('Reynolds= %g\nPrandtl = %g\nMach = %g\n' % (rho*V*D/mu, Prandtl, V/c))
Msg.Info("v = %g\nT=%g\n" %(V,T))

Msg.Info('*** Loading the mesh and the model ***')
model   = GModel  ()
model.load(meshname + '_%dm_%dp.msh'%(int(sys.argv[2]),  Msg.GetCommSize()))
groups = dgGroupCollection(model, dimension, order)

xyz = groups.getFunctionCoordinates()
FS = functionPython(5, free_stream, [xyz])

law=dgPerfectGasLaw(dimension)
law.setGasConstantAndGamma(R, Gamma)
muF = functionConstant(mu)
kF = functionConstant(k)
law.setViscosityAndThermalConductivity(muF, kF);

wallBoundary = law.newNonSlipWallBoundary()
slipWallBoundary = law.newSlipWallBoundary()
outsideBoundary = law.newOutsideValueBoundary("",FS)
law.addBoundaryCondition('Airfoil',wallBoundary)
law.addBoundaryCondition('Lateral',slipWallBoundary)
law.addBoundaryCondition('Box'     ,outsideBoundary)

Msg.Info('*** setting the initial solution ***')
solution = dgDofContainer(groups, law.getNbFields())
proj = dgDofContainer(groups, 1)
rotF = functionPython(3, rot, [solution.getFunctionGradient(), xyz])
solution.L2Projection(FS)

nbExport=0
t=Ti
solution.exportMsh(outputDir+"/sd7003-%06d"% (0),t, nbExport) 
proj.L2Projection(rotF)
proj.exportMsh(outputDir+"/rot-%06d" %(0),t, nbExport) 
solution.exportFunctionMsh(law.getVelocity(), outputDir+'/vel-sd7003-%06d'%(0), t, nbExport)

Msg.Info('*** building multirate setup ***')
rk = dgMultirateERK(groups,  law,  RKTYPE)
dt = rk.splitGroupsForMultirate(int(sys.argv[2]),  solution,  [solution], fact)
dt=100*dt
nbSteps=int(ceil((Tf-Ti)/dt))
Msg.Info('Multirate Speedup=%f'%(rk.speedUp()))
Msg.Info('MR DT = %.10f, DTMIN=%.10f, DTMAX=%10f'%(dt, rk.dtMin(), rk.dtMax()))
Msg.Info('NbSteps=%d'%(nbSteps))
 
Msg.Info('*** solve ***')
nbExport += 1
x = time.clock()
for i in range(1,nbSteps+1) :
   rk.iterate(solution, dt, t)
   t = t + dt
   print t
   if (i % 1 == 0) :
      norm = solution.norm()
      if(Msg.GetCommRank() == 0):
        print'|ITER|',i,'|NORM|',norm, '|DT|',dt, '|T|',t, '|CPU|',time.clock() - x
   if (i % 10 == 0) :
      solution.exportMsh(outputDir+"/sd7003-%06d"% (i),t, nbExport) 
      proj.L2Projection(rotF)
      proj.exportMsh(outputDir+"/rot-%06d" %(i),t, nbExport) 
      solution.exportFunctionMsh(law.getVelocity(), outputDir+'/vel-sd7003-%06d'%(i), t, nbExport)
      nbExport = nbExport + 1
Msg.Exit(0)
