epais=0.00001;
lc=0.1;
a=0.5;
h=1.5;
h0=0;

Merge "skull-2.stl";
Surface Loop(2) = {1};


Point(10)= {-a,-a,h0+h, lc};
Point(11)= {-a,a,h0+h, lc};
Point(12)= {a,a,h0+h, lc};
Point(13)= {a,-a,h0+h, lc};

Line(110)= {10,11};
Line(111)= {11,12};
Line(112)= {12,13};
Line(113)= {13,10};

Line Loop(114)={110,111,112,113};
Plane Surface(1110)={114};

//Point(14)= {-a,-a,h0+1.75*h, lc};
//Point(15)= {-a,a,h0+1.75*h, lc};
//Point(16)= {a,a,h0+1.75*h, lc};
//Point(17)= {a,-a,h0+1.75*h, lc};



Extrude {0, 0, 0.75*h} {

Surface{1110};

}
Surface Loop(3) = {1110,1119,1123,1127,1131,1132};
//Volume(4)={3};


Volume(100)={2};
Volume(101)={3,-2};

Physical Surface("Inlet")={1110};
Physical Surface("Outlet")={1132};
Physical Surface("TopBottom")={1123,1131};
Physical Surface("LeftRight")={1119,1127};
Physical Surface("SurfaceCrane")={1};
Physical Volume("Crane")={100};
Physical Volume("Outside")={101};
