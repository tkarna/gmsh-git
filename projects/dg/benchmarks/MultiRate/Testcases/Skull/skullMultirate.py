from gmshpy import *
from dgpy import *
import os
import time
import math


dimension = 3
order = 1
#clscale = 5
print'---- Load Mesh & Build Groups'

model = GModel()
model.load('skull.msh')

#if(not os.path.exists("skull%d.msh"%(order))):
#  m = GModel()
#  m.load("skull.geo")
#  GmshSetOption('Mesh', 'CharacteristicLengthFactor',    clscale)
#  GmshSetOption('Mesh', 'Algorithm',    "frontal")
#  m.setOrderN(order,  False,  False)
#  m.mesh(dimension)
#  m.save("skull%d.msh"%(order))
#model.load ('skull%d.msh'%(order))

try : os.mkdir("output");
except: 0;

groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()


print'---- Load Conservation law with parameters and source term'

law = dgConservationLawMaxwell()
mu0=4*math.pi*1e-7
eps0=8.85*1e-12
epaisCrane=1e-3
sigmaOs=10

epsO=eps0
muO=mu0
sigmaO=0

epsC=43*eps0
muC=mu0
sigmaC=2

epsilon = functionConstantByTag({"Outside"      : [epsO,epsO,epsO],
                                 "Crane" : [epsC,epsC,epsC]})
mu = functionConstantByTag({"Outside"      : [muO,muO,muO],
                                 "Crane" : [muC,muC,muC]})

sigma   = functionConstantByTag({"Outside"      : [sigmaO,sigmaO,sigmaO],
                                 "Crane" : [sigmaC,sigmaC,sigmaC]})


Yair=math.sqrt(eps0/mu0)

Ys=sigmaOs*epaisCrane


law.setEpsilon(epsilon)
law.setMu(mu)
law.setSigma(sigma)


print'---- Load Initial conditions'

initialSolution = functionConstant([0,0,0,0,0,0])
solution = dgDofContainer(groups, law.getNbFields())
solution.L2Projection(initialSolution)
solution.setFieldName(0,'E_x')
solution.setFieldName(1,'E_y')
solution.setFieldName(2,'E_z')
solution.setFieldName(3,'H_x')
solution.setFieldName(4,'H_y')
solution.setFieldName(5,'H_z')


tho=1.5e-9
aa=11e9*11e9/(4*math.log(100))

def extFields(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
                        
    tt=t-tho
    value= math.exp(-aa*tt*tt)                                
    val.set(i,0,value)
    val.set(i,1,0)
    val.set(i,2,0)
    val.set(i,3,0)
    val.set(i,4,-value/Yair)
    val.set(i,5,0)
extFieldsPython = functionPython(6,extFields,[xyz])

law.addBoundaryCondition('Outlet',  law.newBoundarySilverMuller('Outlet'))
law.addBoundaryCondition('Inlet',   law.newBoundarySilverMullerDirichlet('Inlet', extFieldsPython))

law.addBoundaryCondition('TopBottom',  law.newBoundarySilverMuller('TopBottom'))
law.addBoundaryCondition('LeftRight',  law.newBoundarySilverMuller('LeftRight'))

#law.addInterfaceSheet('SurfaceCrane',Ys)


# Multirate Parameters
RKTYPE = ERK_43_S        # Type of Multirate Method to choose here below
mL=1000                # Maximum Number Of Refinement Levels for Multirate
solver = dgMultirateERK(groups, law, RKTYPE)
dt = solver.splitGroupsForMultirate(mL, solution, [solution])
solver.printMultirateInfo()
solution.exportMultirateTimeStep()
solution.exportMultirateGroups()
print 'SU=', solver.speedUp()
print 'dtMin=', solver.dtMin(), 'dtMax=', solver.dtMax(), 'dtRef', solver.dtRef()
tic = time.clock()
#dt =5e-12
numoutput = 1
for i in range(0,5000):
  t = dt*i
  x = time.clock()
  solver.iterate(solution, dt, t)
  norm = solution.norm()
  if (i % 1 == 0):
    print'|ITER|',i,'/ 50000 |NORM|',norm, '|DT|',dt, '|T|',t, '|CPU|',time.clock()-x
    solution.exportMsh("output/skull-%05i" % numoutput, t, numoutput)
    numoutput = numoutput+1
