from gmshpy import *
from dgpy import *
import os
import time
import math


dimension = 3
order = 1
#clscale = 5
print'---- Load Mesh & Build Groups'

model = GModel()
model.load('skull.msh')

#if(not os.path.exists("skull%d.msh"%(order))):
#  m = GModel()
#  m.load("skull.geo")
#  GmshSetOption('Mesh', 'CharacteristicLengthFactor',    clscale)
#  GmshSetOption('Mesh', 'Algorithm',    "frontal")
#  m.setOrderN(order,  False,  False)
#  m.mesh(dimension)
#  m.save("skull%d.msh"%(order))
#model.load ('skull%d.msh'%(order))

try : os.mkdir("outputLF");
except: 0;

groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()


print'---- Load Conservation law with parameters and source term'

mu0=4*math.pi*1e-7
eps0=8.85*1e-12
epaisCrane=1e-3
sigmaOs=10

epsO=eps0
muO=mu0
sigmaO=0

epsC=43*eps0
muC=mu0
sigmaC=2

epsilon = functionConstantByTag({"Outside"      : [epsO,epsO,epsO],
                                 "Crane" : [epsC,epsC,epsC]})
mu = functionConstantByTag({"Outside"      : [muO,muO,muO],
                                 "Crane" : [muC,muC,muC]})

sigma   = functionConstantByTag({"Outside"      : [sigmaO,sigmaO,sigmaO],
                                 "Crane" : [sigmaC,sigmaC,sigmaC]})


Yair=math.sqrt(eps0/mu0)
Ys=sigmaOs*epaisCrane


lawE = dgConservationLawMaxwellExplicit('E')
lawH = dgConservationLawMaxwellExplicit('H')

lawE.setEpsilon(epsilon)
lawE.setMu(mu)
lawE.setSigma(sigma)

lawH.setEpsilon(epsilon)
lawH.setMu(mu)
lawH.setSigma(sigma)


print'---- Load Initial conditions'

initialSolutionE = functionConstant([0,0,0])
initialSolutionH = functionConstant([0,0,0])



solE = dgDofContainer(groups, lawE.getNbFields())
solE.L2Projection(initialSolutionE)
solE.setFieldName(0,'E_x')
solE.setFieldName(1,'E_y')
solE.setFieldName(2,'E_z')                                                                    

solH = dgDofContainer(groups, lawH.getNbFields())
solH.L2Projection(initialSolutionH)
solH.setFieldName(0,'H_x')
solH.setFieldName(1,'H_y')
solH.setFieldName(2,'H_z')


solHprev = dgDofContainer(groups, lawH.getNbFields())

lawE.setH(solH.getFunction())
lawH.setE(solE.getFunction())

tho=5e-11
aa=11e9*11e9/(4*math.log(100))

def extFieldsE(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)                    
    tt=t-tho
    value= math.exp(-aa*tt*tt)                                
    val.set(i,0,value)
    val.set(i,1,0)
    val.set(i,2,0)
extFieldsPythonE = functionPython(3,extFieldsE,[xyz])

def extFieldsH(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    tt=t-tho
    value= math.exp(-aa*tt*tt)
    val.set(i,0,0)
    val.set(i,1,-value/Yair)
    val.set(i,2,0)
extFieldsPythonH = functionPython(3,extFieldsH,[xyz])



lawE.addBoundaryCondition('Outlet',  lawE.newBoundarySilverMuller('Outlet'))
lawE.addBoundaryCondition('Inlet',   lawE.newBoundarySilverMullerDirichletOnE('Inlet', extFieldsPythonE))

lawE.addBoundaryCondition('TopBottom',  lawE.newBoundarySilverMuller('TopBottom'))
lawE.addBoundaryCondition('LeftRight',  lawE.newBoundarySilverMuller('LeftRight'))

#law.addInterfaceSheet('SurfaceCrane',Ys)


lawH.addBoundaryCondition('Outlet',  lawH.newBoundarySilverMuller('Outlet'))
lawH.addBoundaryCondition('Inlet',   lawH.newBoundarySilverMullerDirichletOnE('Inlet', extFieldsPythonE))

lawH.addBoundaryCondition('TopBottom',  lawH.newBoundarySilverMuller('TopBottom'))
lawH.addBoundaryCondition('LeftRight',  lawH.newBoundarySilverMuller('LeftRight'))



#solverE = dgERK(lawE, None, DG_ERK_44)
#solverH = dgERK(lawH, None, DG_ERK_44)



# Multirate Parameters
RKTYPE = ERK_22_S        # Type of Multirate Method to choose here below
mL=1000                # Maximum Number Of Refinement Levels for Multirate
solverE = dgMultirateERK(groups, lawE, RKTYPE)
dt = solverE.splitGroupsForMultirate(mL, solE, [solE, solH, solHprev])
solverE.printMultirateInfo()
solE.exportMultirateTimeStep()
solE.exportMultirateGroups()
print 'SU=', solverE.speedUp()
print 'dtMin=', solverE.dtMin(), 'dtMax=', solverE.dtMax(), 'dtRef', solverE.dtRef()
solverH = dgMultirateERK(lawH, solverE)
tic = time.clock()

#dt=1e-13

#solverE.getAssembler().setScatterField(solH)
#solverH.getAssembler().setScatterField(solE)
numoutput = 1
for i in range(0,5000):
  t = dt*i
  x = time.clock()
  solverE.iterate(solE, dt, t)
  solHprev.copy(solH);
  solverH.iterate(solH, dt, t+dt/2)
  normE = solE.norm()
  normH = solH.norm()
  if (i % 1 == 0):
    print'|ITER|', i, '|DT|', dt, '|t|', t,'cpu', x- tic, 'normE', solE.norm(), 'normH', solH.norm()
  if (i % 1 == 0):  
    solE.exportMsh("outputLF/FieldsE_%05i" % numoutput, t, numoutput)
    solH.exportMsh("outputLF/FieldsH_%05i" % numoutput, t, numoutput)
    numoutput = numoutput+1
