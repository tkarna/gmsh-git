from dgpy import *
from termcolor import colored
from math import *
import time,  os 

mL = 1000
RKTYPE = ERK_43_S

Ti=0
Tf=10000

order = 1
dimension = 2

clscale = 0.25


model = GModel()
if(not os.path.exists("stommel_adapted-%d.msh"%(order))):
  m = GModel()
  m.load("stommel_adapted.geo")
  GmshSetOption('Mesh','CharacteristicLengthFactor',   clscale)
  GmshSetOption('Mesh','Algorithm',   "frontal")
  m.setOrderN(order, False, False)
  m.mesh(dimension)
  m.save("stommel_adapted-%d.msh"%(order))
model.load ('stommel_adapted-%d.msh'%(order))


try : os.mkdir("output");
except: 0;

# Python Functions
def massIntegrator(fct, sol):
  for i in range(0,sol.size1()):
    s = sol.get(i,0)
    fct.set(i,0,1000+s) 

def printTime (t) :
  return "%dh%02d'%02d\"" % (t/3600,  (t%3600)/60,  t%60)

CCode = """
#include "fullMatrix.h"
#include "function.h"
extern "C" {
void wind (dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< sol.size1(); i++) {
    sol.set(i,0,sin(xyz(i,1)/1e6)/1e6);
    sol.set(i,1,0);
  }
}
void coriolis (dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< sol.size1(); i++) {
    sol.set(i,0,1e-4+2e-11*xyz(i,1));
  }
}
void extractEta (dataCacheMap *, fullMatrix<double> &eta, fullMatrix<double> &fullSol) {
  for (size_t i = 0; i< fullSol.size1(); i++) {
    eta.set(i,0,fullSol(i,0));
  }
}
}
"""

tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CCode, tmpLib);
Msg.Barrier()

groups = dgGroupCollection(model, dimension, order)
claw = dgConservationLawShallowWater2d()
solution = dgDofContainer(groups, claw.getNbFields())

XYZ = groups.getFunctionCoordinates();
f0 = functionC(tmpLib,"coriolis",1,[XYZ, functionSolution.get()])
claw.setCoriolisFactor(f0)
f1 = functionConstant(0)
claw.setQuadraticDissipation(f1)
claw.setIsLinear(1)
f2 = functionConstant([1e-6])
claw.setLinearDissipation(f2)
f3 = functionC(tmpLib,"wind",2,[XYZ])
claw.setSource(f3)
f4 = functionConstant([1000])
claw.setBathymetry(f4)
f5 = functionConstant([0,0,0])
claw.setBathymetryGradient(f5)

bndWall = claw.newBoundaryWall()
claw.addBoundaryCondition('Wall', bndWall)

print '*** Set Multirate Method and Split Groups for Multirate ***'
rk = dgMultirateERK(groups, claw, RKTYPE)
dt=rk.splitGroupsForMultirate(mL,   solution, [solution])
solution.exportMultirateGroups()
rk.printMultirateInfo()

nbSteps = int(ceil((Tf-Ti)/dt))
norm=solution.norm()

mass=functionPython(1,  massIntegrator,  [functionSolution.get()])
integrator = dgFunctionIntegrator(groups,  mass,  solution)
totalMass=fullMatrixDouble(1, 1)
totalMassRef=fullMatrixDouble(1, 1)
integrator.compute(totalMassRef, "")

exporterSol=dgIdxExporter(solution, 'output/stommel')

print ''
print(colored('******************** Initializing ********************', "blue"))
print ''
print '|MR DT    | :', printTime(dt)
print '|MR SU    | :', rk.speedUp()
print '|MASS     | :',  totalMassRef.get(0, 0)
print '|NORM     | :',  norm
print '|NB STEPS | :', nbSteps
print ''
print(colored('******************************************************', "blue"))
print ''

t=Ti
exporterSol.exportIdx(0, t)
startcpu = time.clock()
for i in range (1,nbSteps+1) :
  if(i == nbSteps):
    dt=Tf-t
  norm = rk.iterate (solution, dt, t)
  t = t +dt
  integrator.compute(totalMass, "")
  print'|ITER|', i, '|T(sec)|',  t,  '|NORM|',  solution.norm(), '|MC|', abs((totalMass.get(0,  0)-totalMassRef.get(0,  0))/totalMassRef.get(0,  0)) ,   '|CPU|', time.clock() - startcpu
  if ( i%10 ==0 ):
    integrator.compute(totalMass, "")
    exporterSol.exportIdx(i, t)

endcpu=time.clock()
norm=solution.norm()

print ''
print(colored('********************     End      ********************', "blue"))
print ''
print '     - Final Time (Tf):',  printTime(t)
print '     - Norm of Solution at Tf:',  norm
print '     - Mass of Solution at Tf:', totalMass(0, 0)
print '     - Total CPU Time:', endcpu-startcpu
print ''
print(colored('******************************************************', "blue"))
print ''
Msg.Exit(0)

