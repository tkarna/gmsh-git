from gmshpy import *
from dgpy import *
from InputSD7003 import *
import os, time,  math, sys


Msg.Info('Reynolds= %g\nPrandtl = %g\nMach = %g\n' % (rho*V*D/mu, Prandtl, V/c))
Msg.Info("v = %g\nT=%g\n" %(V,T))

Msg.Info('*** Loading the mesh and the model ***')
model   = GModel  ()
model.load(meshname + '_%dm_%dp.msh'%(int(sys.argv[2]),  Msg.GetCommSize()))
groups = dgGroupCollection(model, dimension, order)

xyz = groups.getFunctionCoordinates()
FS = functionPython(4, free_stream, [xyz])

law=dgPerfectGasLaw(dimension)
law.setGasConstantAndGamma(R, Gamma)
muF = functionConstant(mu)
kF = functionConstant(k)
law.setViscosityAndThermalConductivity(muF, kF);

wallBoundary = law.newNonSlipWallBoundary()
slipWallBoundary = law.newSlipWallBoundary()
outsideBoundary = law.newOutsideValueBoundary("",FS)
law.addBoundaryCondition('Airfoil',wallBoundary)
#law.addBoundaryCondition('Lateral',slipWallBoundary)
law.addBoundaryCondition('Box'     ,outsideBoundary)
law.addBoundaryCondition('Lateral'     ,outsideBoundary)
if sys.argv[3] == "new" :
  law = dgConservationLawPerfectGas2(R, Gamma, k, mu)

Msg.Info('*** setting the initial solution ***')
solution = dgDofContainer(groups, law.getNbFields())
proj = dgDofContainer(groups, 1)
rotF = functionPython(1, rot, [solution.getFunctionGradient(), xyz])
solution.L2Projection(FS)

if(int(sys.argv[1])==1):
  fact=1
Msg.Info('*** building multirate setup ***')
rk = dgMultirateERK(groups,  law,  RKTYPE)
dt = rk.splitGroupsForMultirate(int(sys.argv[2]),  solution,  [solution, proj], fact)
dt=10*dt
Msg.Info('Multirate Speedup=%f'%(rk.speedUp()))
Msg.Info('MR DT = %.10f, DTMIN=%.10f, DTMAX=%.10f'%(dt, rk.dtMin(), rk.dtMax()))
 
Msg.Info('*** solve ***')
t=Ti
startcpu = time.time()
oldcpu = startcpu
nbSteps=50
for i in range(1,nbSteps+1) :
   rk.iterate(solution, dt, t)
   t = t + dt
   newcpu=time.time()
   if(Msg.GetCommRank()==0):
     print i,  t,  newcpu-oldcpu,  newcpu-startcpu
   oldcpu=newcpu
   Msg.Barrier()
endcpu = time.time()
Msg.Barrier()
for p in range(0, Msg.GetCommSize()):
  Msg.Barrier()
  if(Msg.GetCommRank()==p):
    print '--------------------------------------------------'
    print Msg.GetCommRank()
    print '--------------------------------------------------'
    dgTimer.root().printFull()
    print rk.nbVolumeTerms(),  rk.nbInterfaceTerms1(), rk.nbInterfaceTerms2() 
    print '--------------------------------------------------'
    Msg.Barrier()
#if(Msg.GetCommRank()==0):
#  print '*** FINAL ***'
#  print i, t, solution.norm(), (endcpu-startcpu)
#  print 'order=',order,'multirate levels=', int(sys.argv[2])
Msg.Exit(0)
