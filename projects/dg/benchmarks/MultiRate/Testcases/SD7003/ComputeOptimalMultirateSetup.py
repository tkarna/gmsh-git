from dgpy import *
from InputSD7003 import *
import sys
#import pylab as pl

model   = GModel  ()
model.load(meshname+'.msh')

plot_colors =['r--',  'b--',  'g--',  'c--',  'm--',  'k--',  'r:',  'b:',  'g:',  'c:', 'm:', 'k:', 'r', 'b', 'g', 'c', 'm', 'k']
df=0.001
nbDF=int(0.5/df)+1
fig = pl.figure(1,  figsize=(16,  6))
pl.subplot(121)
pl.title("Multirate time step")
pl.subplot(122)
pl.title("Multirate speedup")
for ML in range(14, 17):
  print 'Maximum multirate exponent=', ML
  DT=[]
  FACT=[]
  SU=[]
  for i in range(0, nbDF) :
    fact=0.5+i*df;
    groups = dgGroupCollection(model, dimension, order)

    xyz = groups.getFunctionCoordinates()
    FS = functionPython(4, free_stream, [xyz])
    
    law=dgPerfectGasLaw(dimension)
    law.setGasConstantAndGamma(R, Gamma)
    muF = functionConstant(mu)
    kF = functionConstant(k)
    law.setViscosityAndThermalConductivity(muF, kF);
    
    wallBoundary = law.newNonSlipWallBoundary()
    slipWallBoundary = law.newSlipWallBoundary()
    outsideBoundary = law.newOutsideValueBoundary("",FS)
    law.addBoundaryCondition('Airfoil',wallBoundary)
    law.addBoundaryCondition('Lateral',slipWallBoundary)
    law.addBoundaryCondition('Box'     ,outsideBoundary)
    solution = dgDofContainer(groups, law.getNbFields())
    solution.L2Projection(FS)
    
    rk=dgMultirateERK(groups,  law,  RKTYPE)
    dt=rk.splitGroupsForMultirate(ML,  solution,  [solution], fact)
    FACT.append(fact)
    DT.append(dt)
    SU.append(rk.speedUp())
    print("%.4f %.4e %.4f"%(fact, rk.dtRef(), rk.speedUp()))

  pl.subplot(121)
  pl.plot(FACT,  DT, plot_colors[ML-2], label="ME="+str(ML-1))
  pl.subplot(122)
  pl.plot(FACT,  SU,  plot_colors[ML-2], label="ME="+str(ML-1))

pl.subplot(122)
pl.legend(loc='center left',  bbox_to_anchor=(1,  0.5))
fig.savefig('optimal_multirate_speedup.png',  dpi=fig.dpi)
Msg.Exit(0)
