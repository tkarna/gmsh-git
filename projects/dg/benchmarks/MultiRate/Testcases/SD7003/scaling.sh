order=$1
ml=$2
echo "" > $order"_"$ml"_scaling_results"
for p in 1 2 3 4 5 6 7 8 9 10 11 12; do
  mpirun -np $p rundgpy ParallelSpeedup.py $order $ml new | tee log
  time=`tail -n1 log`
  echo $p $time >> $order"_"$ml"_scaling_results"
done
