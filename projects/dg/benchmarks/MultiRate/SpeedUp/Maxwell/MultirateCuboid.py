from gmshpy import *
from dgpy import *
import os
import time
import math

mu0=4*math.pi*1e-7
eps0=8.85*1e-12
Ys=50*1e-3
rhoValue=4*math.pi*1e-7
cValue=1/math.sqrt(4*math.pi*1e-7*8.85*1e-12)
TT=2
tho=3./cValue
def outletValueFunc(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    tt=4*cValue*(t-tho)/TT
    value = 4*math.exp(-tt*tt)/(math.pow(math.pi,0.5)*TT)
    val.set(i,0,value)
    val.set(i,1,0*value)
    val.set(i,2,0*value)
    val.set(i,3,0*value/(cValue*rhoValue))
    val.set(i,4,-value/(cValue*rhoValue))
    val.set(i,5,0*value/(cValue*rhoValue))

dimension = 3
order = 1
clscale = 5

model = GModel()
if(not os.path.exists("box%d.msh"%(order))):
  m = GModel()
  m.load("box.geo")
  GmshSetOption('Mesh', 'CharacteristicLengthFactor',    clscale)
  GmshSetOption('Mesh', 'Algorithm',    "frontal")
  m.setOrderN(order,  False,  False)
  m.mesh(dimension)
  m.save("box%d.msh"%(order))
model.load ('box%d.msh'%(order))

mu      = functionConstant([mu0,mu0,mu0])
epsilon = functionConstant([eps0,eps0,eps0])

RK_TYPES = [ERK_22_C,  ERK_33_C,  ERK_43_C,  ERK_44_C,  ERK_22_S, ERK_22_S_C, ERK_43_S,  ERK_43_S_C]
RK_TYPES_NAMES = ["ERK_22_C",  "ERK_33_C",  "ERK_43_C",  "ERK_44_C", "ERK_22_S", "ERK_22_S_C", "ERK_43_S",  "ERK_43_S_C"]
ML = [1, 1000]
NB_STEPS=[8, 2]
for k in range (0,  len(RK_TYPES)):
  CPU_TIMES=[]
  TH_SU=[]
  FINAL_TIMES = []
  for g in range(0, len(ML)):
    groups = dgGroupCollection(model, dimension, order)
    groups.splitGroupsByPhysicalTag()
    xyz = groups.getFunctionCoordinates()
    
    law = dgConservationLawMaxwell()
    law.setMu(mu)
    law.setEpsilon(epsilon)
    initialSolution = functionConstant([0,0,0,0,0,0])
    solution = dgDofContainer(groups, law.getNbFields())
    solution.L2Projection(initialSolution)
    
    outletValue = functionPython(6, outletValueFunc, [xyz])
    
    law.addBoundaryCondition('Air_Bottom',  law.newBoundaryMagneticWall('Air_Bottom'))
    law.addBoundaryCondition('Air_Top',     law.newBoundaryMagneticWall('Air_Top'))
    law.addBoundaryCondition('Air_Left',    law.newBoundaryElectricWall('Air_Left'))
    law.addBoundaryCondition('Air_Right',   law.newBoundaryElectricWall('Air_Right'))
    law.addBoundaryCondition('Air_Outlet',  law.newBoundarySilverMuller('Air_Outlet'))
    law.addBoundaryCondition('Air_Inlet',   law.newBoundarySilverMullerDirichlet('Air_Inlet', outletValue))
    
    law.addInterfaceEwall('Cav_Outlet')
    law.addInterfaceEwall('Cav_Aperture')
    law.addInterfaceEwall('Cav_Inlet_ap')
    law.addInterfaceEwall('Cav_Top')
    law.addInterfaceEwall('Cav_Bottom')
    law.addInterfaceEwall('Cav_Left')
    law.addInterfaceEwall('Cav_Right')

    RKTYPE=RK_TYPES[k]
    solver = dgMultirateERK(groups, law, RKTYPE)
    dt = solver.splitGroupsForMultirate(ML[g], solution, [solution])
    TH_SU.append(solver.speedUp())
    t=0.0
    nbSteps=NB_STEPS[g]
    x = time.time()
    for i in range(0,nbSteps):
      solver.iterate(solution, dt, t)
      t = t+dt
    y=time.time()
    CPU_TIMES.append(y-x)
    FINAL_TIMES.append(t)
  print RK_TYPES_NAMES[k], ':TH_SU=',TH_SU[1]/TH_SU[0] , 'EF_SU',(CPU_TIMES[0]*FINAL_TIMES[1])/(CPU_TIMES[1]*FINAL_TIMES[0]), "EFFICIENCY=", (CPU_TIMES[0]*FINAL_TIMES[1])/(CPU_TIMES[1]*FINAL_TIMES[0])/(TH_SU[1]/TH_SU[0]) 
   

Msg.Exit(0)
