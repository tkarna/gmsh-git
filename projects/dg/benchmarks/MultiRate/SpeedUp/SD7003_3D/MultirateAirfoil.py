from gmshpy import *
from dgpy import *
import os
import time
import math

dimension = 3
order = 1
clscale = 5

model = GModel()
if(not os.path.exists("sd7003%d.msh"%(order))):
  m = GModel()
  m.load("sd7003.geo")
  GmshSetOption('Mesh', 'CharacteristicLengthFactor',    clscale)
  GmshSetOption('Mesh', 'Algorithm',    "frontal")
  m.setOrderN(order,  False,  False)
  m.mesh(dimension)
  m.save("sd7003%d.msh"%(order))
model.load ('sd7003%d.msh'%(order))

Mach = 0.1
Reynolds = 100000
Prandtl = 1

D = 1
P = 101325
R = 287.1
T = 293.15
Gamma = 1.4

Cp = Gamma * R / (Gamma-1)
rho = P / ( R * T)
c = math.sqrt(Gamma * R * T)
V = Mach * c
mu  = rho * V * D / Reynolds
k = mu * Cp / Prandtl

def free_stream(FCT,  XYZ) :
  for i in range (0,XYZ.size1()) :
    FCT.set(i,0,rho) 
    FCT.set(i,1,rho*V) 
    FCT.set(i,2,0.0) 
    FCT.set(i,3,0.0) 
    FCT.set(i,4, 0.5*rho*V*V+P/(Gamma-1)) 

RK_TYPES = [ERK_22_C,  ERK_33_C,  ERK_43_C,  ERK_44_C,  ERK_22_S, ERK_22_S_C, ERK_43_S,  ERK_43_S_C]
RK_TYPES_NAMES = ["ERK_22_C",  "ERK_33_C",  "ERK_43_C",  "ERK_44_C", "ERK_22_S", "ERK_22_S_C", "ERK_43_S",  "ERK_43_S_C"]
ML = [1, 1000]
NB_STEPS=[8, 2]
for kk in range (0,  len(RK_TYPES)):
  CPU_TIMES=[]
  TH_SU=[]
  FINAL_TIMES = []
  for g in range(0, len(ML)):
    groups = dgGroupCollection(model, dimension, order)
    xyz = groups.getFunctionCoordinates()
    FS = functionPython(5, free_stream, [xyz])
    
    law=dgPerfectGasLaw(dimension)
    law.setGasConstantAndGamma(R, Gamma)
    muF = functionConstant(mu)
    kF = functionConstant(k)
    law.setViscosityAndThermalConductivity(muF, kF);
    
    wallBoundary = law.newNonSlipWallBoundary()
    slipWallBoundary = law.newSlipWallBoundary()
    outsideBoundary = law.newOutsideValueBoundary("",FS)
    law.addBoundaryCondition('Airfoil',wallBoundary)
    law.addBoundaryCondition('Lateral',slipWallBoundary)
    law.addBoundaryCondition('Box'     ,outsideBoundary)
    
    solution = dgDofContainer(groups, law.getNbFields())
    solution.L2Projection(FS)
    
    RKTYPE=RK_TYPES[kk]
    solver = dgMultirateERK(groups, law, RKTYPE)
    dt = solver.splitGroupsForMultirate(ML[g], solution, [solution])
    TH_SU.append(solver.speedUp())
    t=0.0
    nbSteps=NB_STEPS[g]
    x = time.time()
    for i in range(0,nbSteps):
      solver.iterate(solution, dt, t)
      t = t+dt
    y=time.time()
    CPU_TIMES.append(y-x)
    FINAL_TIMES.append(t)
  print RK_TYPES_NAMES[kk], ':TH_SU=',TH_SU[1]/TH_SU[0] , 'EF_SU',(CPU_TIMES[0]*FINAL_TIMES[1])/(CPU_TIMES[1]*FINAL_TIMES[0]), "EFFICIENCY=", (CPU_TIMES[0]*FINAL_TIMES[1])/(CPU_TIMES[1]*FINAL_TIMES[0])/(TH_SU[1]/TH_SU[0]) 
   

Msg.Exit(0)
