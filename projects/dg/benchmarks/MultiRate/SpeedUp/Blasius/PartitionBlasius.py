from dgpy import *
from gmshpy import *
from InputBlasius import *
import time, math, os, sys

os.system('rm ' + meshname + '.msh')
os.system('rm ' + meshname + '_%dm_%dp.msh'%(int(sys.argv[2]), int(sys.argv[3])))

model   = GModel  ()

if(not os.path.exists(meshname+'.msh')):
  m = GModel()
  m.load(geoname+".geo")
  GmshSetOption('Mesh', 'CharacteristicLengthFactor',    clscale)
  #GmshSetOption('Mesh', 'Algorithm',    "frontal")
  m.mesh(dimension)
  if(order>1) :
    m.setOrderN(order,  False,  False)
  m.save(meshname+'.msh')
model.load(meshname+'.msh')

groups = dgGroupCollection(model, dimension, order)
xyz = groups.getFunctionCoordinates()

law=dgPerfectGasLaw(dimension)
law.setGasConstantAndGamma(R, Gamma)
muF = functionConstant(mu)
kF = functionConstant(k)
law.setViscosityAndThermalConductivity(muF, kF);

solution = dgDofContainer(groups, law.getNbFields())
proj = dgDofContainer(groups, 1)

FS = functionPython(4, free_stream, [xyz])
TS = functionPython(4, top_stream, [solution.getFunction()])
solution.L2Projection(FS)

wallBoundary = law.newNonSlipWallBoundary()
inletBoundary = law.newOutsideValueBoundary("",FS)
outletBoundary = law.newSymmetryBoundary('')
topBoundary = law.newOutsideValueBoundary("",TS)
leftBoundary = law.newSlipWallBoundary()
rightBoundary = leftBoundary

law.addBoundaryCondition('Wall',wallBoundary)
law.addBoundaryCondition('Inlet',inletBoundary)
law.addBoundaryCondition('Outlet',outletBoundary)
law.addBoundaryCondition('Top',topBoundary)
law.addBoundaryCondition('Left',leftBoundary)
law.addBoundaryCondition('Right',rightBoundary)

#-------------------------------------------------
#-- Split for MultiRate & partitions
#-------------------------------------------------
rk=dgMultirateERK(groups,  law,  RKTYPE)
dt=rk.splitGroupsForMultirate(int(sys.argv[2]),  solution,  [solution, proj], fact)
solution.exportGroupIdMsh()
solution.exportMultirateGroups()
solution.exportMultirateTimeStep()
rk.printMultirateInfo()
print ('SU=',  rk.speedUp(),  'DTMIN=', rk.dtMin(), 'DTMAX=', rk.dtMax(), 'DTREF=', dt)
rk.printPartitionInfo()
pOpt = meshPartitionOptions()
pOpt.setNumOfPartitions(int(sys.argv[3]))
if(partitionMethod==1):
  rk.computeClassicalContrainedPartition(pOpt,  algo)
if(partitionMethod==2):
  rk.computeSingleContrainedPartition(pOpt,  algo)
if(partitionMethod==3):
  rk.computeMultiConstrainedPartition(pOpt,  algo)
PartitionMesh(model, pOpt)
rk.updatePartitionInfo(int(sys.argv[3]))
rk.printPartitionInfo()
model.save(meshname + '_%dm_%dp.msh'%(int(sys.argv[2]), int(sys.argv[3])))
#print("%f %f %f %f %f"%(fact, rk.dtMin(), rk.dtMax(), rk.dtRef(), rk.speedUp()))
Msg.Info('Multirate Speedup=%.3f'%(rk.speedUp()))
Msg.Info('MR DT = %.9f, DTMIN=%.9f, DTMAX=%.9f'%(rk.dtRef(), rk.dtMin(),rk.dtMax()))

os.system("gmsh "+meshname + '_%dm_%dp.msh'%(int(sys.argv[2]), int(sys.argv[3]))+' '+'multirateGroups.msh')

Msg.Exit(0)

