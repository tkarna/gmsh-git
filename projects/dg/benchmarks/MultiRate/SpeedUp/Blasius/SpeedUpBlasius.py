from gmshpy import *
from dgpy import *
from InputBlasius import *
import os, time, math, sys

model = GModel()
if(Msg.GetCommSize() > 1):
  model.load (meshname+'_%dm_%dp.msh'%(int(sys.argv[2]),  Msg.GetCommSize()))
else:
  model.load (meshname+'.msh')

muF = functionConstant(mu)
kF = functionConstant(k)

RK_TYPES = [ERK_22_C,  ERK_33_C,  ERK_43_C,  ERK_44_C,  ERK_22_S, ERK_22_S_C, ERK_43_S,  ERK_43_S_C]
RK_TYPES_NAMES = ["ERK_22_C",  "ERK_33_C",  "ERK_43_C",  "ERK_44_C", "ERK_22_S", "ERK_22_S_C", "ERK_43_S",  "ERK_43_S_C"]
ML = [1, 1000]
NB_STEPS=[8, 2]

for k in range (0,  len(RK_TYPES)):
  CPU_TIMES=[]
  TH_SU=[]
  FINAL_TIMES = []
  for g in range(0, len(ML)):
    groups = dgGroupCollection(model,dimension,order)
    xyz = groups.getFunctionCoordinates()

    law = dgPerfectGasLaw(dimension)
    law.setGasConstantAndGamma(R, Gamma)
    law.setViscosityAndThermalConductivity(muF, kF)

    solution = dgDofContainer(groups, law.getNbFields())
    FS = functionC("./ccodeInitial.so","freeStream",4,[xyz])
    TS = functionC("./ccodeInitial.so","topStream",4,[solution.getFunction()])

    solution.L2Projection(FS)

    wallBoundary = law.newNonSlipWallBoundary()
    inletBoundary = law.newOutsideValueBoundary("",FS)
    outletBoundary = law.newSymmetryBoundary('')
    topBoundary = law.newOutsideValueBoundary("",TS)
    leftBoundary = law.newSlipWallBoundary()
    rightBoundary = leftBoundary

    law.addBoundaryCondition('Wall',wallBoundary)
    law.addBoundaryCondition('Inlet',inletBoundary)
    law.addBoundaryCondition('Outlet',outletBoundary)
    law.addBoundaryCondition('Top',topBoundary)
    law.addBoundaryCondition('Left',leftBoundary)
    law.addBoundaryCondition('Right',rightBoundary)

    RKTYPE=RK_TYPES[k]
    rk = dgMultirateERK(groups,law,RKTYPE)
    dt = rk.splitGroupsForMultirate(ML[g],solution,[solution])
    TH_SU.append(rk.speedUp())
    t=Ti
    nbSteps=NB_STEPS[g]
    x = time.time()

    for i in range(1,nbSteps+1) :
      rk.iterate(solution, dt, t)
      t = t+dt
    y = time.time()
    CPU_TIMES.append(y-x)
    FINAL_TIMES.append(t)

  print '%11s'%(RK_TYPES_NAMES[k]), ':TH_SU=%.3f'%(TH_SU[1]/TH_SU[0]) , 'EF_SU %.3f'%((CPU_TIMES[0]*FINAL_TIMES[1])/(CPU_TIMES[1]*FINAL_TIMES[0])), 'EFFICIENCY=%.3f'%((CPU_TIMES[0]*FINAL_TIMES[1])/(CPU_TIMES[1]*FINAL_TIMES[0])/(TH_SU[1]/TH_SU[0]))

Msg.Exit(0)

