from dgpy import *
from gmshpy import *
import time, math, os, sys

Mach = 0.1
Reynolds = 64000
Prandtl = 1

D = 1
P = 101325
R = 287.1
T = 293.15
Gamma = 1.4

Cp = Gamma * R / (Gamma-1)
rho = P / ( R * T)
c = math.sqrt(Gamma * R * T)
V = Mach * c
mu  = rho * V * D / Reynolds
k = mu * Cp / Prandtl
order= int(sys.argv[1])

dimension=2

meshname="sd7003_quad"+str(order)

model   = GModel  ()
model.load(meshname+'.msh')
groups = dgGroupCollection(model, dimension, order)

def free_stream(FCT,  XYZ) :
  for i in range (0,XYZ.size1()) :
    FCT.set(i,0,rho) 
    FCT.set(i,1,rho*V) 
    FCT.set(i,2,0.0) 
    FCT.set(i,3, 0.5*rho*V*V+P/(Gamma-1)) 

RK_TYPES = [ERK_22_C,  ERK_33_C,  ERK_43_C,  ERK_44_C,  ERK_22_S, ERK_22_S_C, ERK_43_S,  ERK_43_S_C]
RK_TYPES_NAMES = ["ERK_22_C",  "ERK_33_C",  "ERK_43_C",  "ERK_44_C", "ERK_22_S", "ERK_22_S_C", "ERK_43_S",  "ERK_43_S_C"]
xyz = groups.getFunctionCoordinates()
FS = functionPython(4, free_stream, [xyz])

law=dgPerfectGasLaw(dimension)
law.setGasConstantAndGamma(R, Gamma)
muF = functionConstant(mu)
kF = functionConstant(k)
law.setViscosityAndThermalConductivity(muF, kF);

wallBoundary = law.newNonSlipWallBoundary()
slipWallBoundary = law.newSlipWallBoundary()
outsideBoundary = law.newOutsideValueBoundary("",FS)
law.addBoundaryCondition('Airfoil',wallBoundary)
law.addBoundaryCondition('Lateral',slipWallBoundary)
law.addBoundaryCondition('Box'     ,outsideBoundary)
solution = dgDofContainer(groups, law.getNbFields())
solution.L2Projection(FS)

rk=dgMultirateERK(groups,  law,  RK_TYPES[int(sys.argv[4])])
dt=rk.splitGroupsForMultirate(int(sys.argv[2]),  solution,  [solution])
solution.exportMultirateGroups()
rk.printMultirateInfo()
print ('SU=',  rk.speedUp(),  'DTMIN=', rk.dtMin(), 'DTMAX=', rk.dtMax(), 'DTREF=', dt)
rk.printPartitionInfo()
pOpt = meshPartitionOptions()
pOpt.setNumOfPartitions(int(sys.argv[3]))
rk.computeMultiConstrainedPartition(pOpt,  5, 15)
PartitionMesh(model, pOpt)
rk.updatePartitionInfo(int(sys.argv[3]))
rk.printPartitionInfo()
model.save(RK_TYPES_NAMES[int(sys.argv[4])]+'_'+ meshname + '_%d_%d.msh'%(int(sys.argv[2]), int(sys.argv[3])))
print("%f %f %f %f %f %f"%(rk.dtMin(), rk.dtMax(), rk.dtRef(), rk.speedUp(), rk.globalImbalance(), rk.speedUp()/rk.globalImbalance()))
Msg.Exit(0)

