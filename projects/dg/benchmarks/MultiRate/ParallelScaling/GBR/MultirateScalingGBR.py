# Import for python
from dgpy import *
from math import *
from ProjectMesh import *
from InputGBR import *
from mpi4py import MPI
from termcolor import colored
import time, os, sys

model = GModel()
model.load(filename + '_partitioned%d_tan%d.msh'%(Msg.GetCommSize(),mL))

# Coordinate system
XYZ = function.getCoordinates();
lonLatDegrees = functionC(glib,"lonLatDegrees",3,[XYZ])
lonLat = functionC(glib,"lonLat",3,[XYZ])

latLonDegrees = functionC(glib,"latLonDegrees",3,[XYZ])
latLon = functionC(glib,"latLon",3,[XYZ])

groups = dgGroupCollection(model, dimension, order)

if(not os.path.exists(bathname)):
  Msg.Fatal('No valid bathymetry file :' + bathname + ' found,  please launch "rundgpy DiffuseBathymetryTsunami.py"')

bath = dgDofContainer(groups, 1);
bath.readMsh(bathname)

def tide (f):
  for i in range(0,f.size1()):
    f.set(i, 0, sin((t-Ti)*2*pi/3600/12))
    f.set(i, 1, 0)
    f.set(i, 2, 0)


claw = dgConservationLawShallowWater2d()
solution = dgDofContainer(groups, claw.getNbFields())

ld = functionConstant(0)
Cd = functionC(glib, "bottomDrag", 1, [function.getSolution(), bath.getFunction()])
Di = functionConstant(diff)
Coriolis = functionC(glib, "coriolis", 1, [XYZ])
source = functionC(glib, "wind", 2, [XYZ])

claw.setCoriolisFactor(Coriolis)
claw.setQuadraticDissipation(Cd)
claw.setLinearDissipation(ld)
claw.setDiffusivity(Di)
claw.setSource(source)
claw.setBathymetry(bath.getFunction())
claw.setBathymetryGradient(bath.getFunctionGradient())

Os = functionPython(3, tide,[])

claw.addBoundaryCondition('Open Sea North',claw.newOutsideValueBoundary("Surface", Os))
claw.addBoundaryCondition('Open Sea Central',claw.newOutsideValueBoundary("Surface", Os))
claw.addBoundaryCondition('Open Sea South',claw.newOutsideValueBoundary("Surface", Os))

claw.addBoundaryCondition('Coast',claw.newBoundaryWall())
claw.addBoundaryCondition('Islands',claw.newBoundaryWall())

rk = dgMultirateERK(groups, claw, RK_TYPE)
dt = rk.splitGroupsForMultirate(mL, solution, [solution, bath])
pvSteps=2*Msg.GetCommSize()*int(pow(2,    mLmax-mL))
nbSteps=8*Msg.GetCommSize()*int(pow(2,    mLmax-mL))
t=0
Msg.Barrier()
rk.initialize()
Msg.Barrier()

for i in range (1,pvSteps+1) :
  rk.iterate(solution, dt, t)
  print i,solution.norm()
  t = t + dt
t_int=t
timer=dgTimer.root()
timer.reset()
Msg.Barrier()
timer.start()

for i in range (1,nbSteps+1) :
  rk.iterate(solution, dt, t)
  t = t + dt

timer.stop()
Msg.Barrier()

nbf2P = float(rk.nbInterfaceTerms2());
nbf1P = float(rk.nbInterfaceTerms1());
nbvP  = float(rk.nbVolumeTerms());
c2cP = float(rk.nbInterfaceTerms2())/float(rk.nbVolumeTerms());
nbf2 = MPI.COMM_WORLD.allreduce(nbf2P,   op=MPI.SUM)
nbf2MIN = MPI.COMM_WORLD.allreduce(nbf2P,   op=MPI.MIN)
nbf2MAX = MPI.COMM_WORLD.allreduce(nbf2P,   op=MPI.MAX)
nbf1 = MPI.COMM_WORLD.allreduce(nbf1P,   op=MPI.SUM)
nbf1MIN = MPI.COMM_WORLD.allreduce(nbf1P,   op=MPI.MIN)
nbf1MAX = MPI.COMM_WORLD.allreduce(nbf1P,   op=MPI.MAX)
nbv = MPI.COMM_WORLD.allreduce(nbvP,   op=MPI.SUM)
nbvMIN = MPI.COMM_WORLD.allreduce(nbvP,   op=MPI.MIN)
nbvMAX = MPI.COMM_WORLD.allreduce(nbvP,   op=MPI.MAX)
c2c = MPI.COMM_WORLD.allreduce(c2cP,    op=MPI.SUM)
c2cMIN = MPI.COMM_WORLD.allreduce(c2cP,    op=MPI.MIN)
c2cMAX = MPI.COMM_WORLD.allreduce(c2cP,    op=MPI.MAX)
maxTime = MPI.COMM_WORLD.allreduce(timer.get(),  op=MPI.MAX)
minTime = MPI.COMM_WORLD.allreduce(timer.get(),  op=MPI.MIN)
norm=solution.norm()
if (Msg.GetCommRank()==0):
   print "# of processors:", Msg.GetCommSize()
   print "imbalances:", rk.bestImbalance(), rk.globalImbalance()
   print "# volume terms (min,  max, ave):", nbvMIN*int(pow(2,    mLmax-int(sys.argv[1]))), nbvMAX*int(pow(2,    mLmax-int(sys.argv[1]))), nbv/Msg.GetCommSize()*int(pow(2,    mLmax-int(sys.argv[1])))
   print "# interface terms 1 (min,  max, ave):", nbf1MIN*int(pow(2,    mLmax-int(sys.argv[1]))), nbf1MAX*int(pow(2,    mLmax-int(sys.argv[1]))), nbf1/Msg.GetCommSize()*int(pow(2,    mLmax-int(sys.argv[1])))
   print "# interface terms 2 (min,  max, ave):", nbf2MIN*int(pow(2,    mLmax-int(sys.argv[1]))), nbf2MAX*int(pow(2,    mLmax-int(sys.argv[1]))), nbf2/Msg.GetCommSize()*int(pow(2,    mLmax-int(sys.argv[1])))
   print " communication2computation ratios (min,  max,  ave):", c2cMIN, c2cMAX, c2c/Msg.GetCommSize()
   print "%d %.8f %f %f %f %f %f %f %f"%(Msg.GetCommSize(),norm,rk.bestImbalance(),rk.globalImbalance(),c2cMIN, c2cMAX, c2c/Msg.GetCommSize(),minTime/(t-t_int),maxTime/(t-t_int))

Msg.Exit(0)

