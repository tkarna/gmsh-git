from dgpy import *
from ProjectMesh import *
import os, time,sys

RK_TYPE = ERK_43_S
mL = int(sys.argv[1])
mLmax = 4
mesh = "100K"
#mesh = "2K"
algo = 5

# File Name
filename = "gbr"+mesh
outputDir = "output"
bathdir=outputDir+"/"+filename+"_bath_smooth"
bathname=outputDir+"/"+filename+"_bath_smooth"+"/"+filename+"_bath_smooth.idx"


# Mesh Parameters
order = 3
dimension = 2

pvSteps = 0
nbSteps = pow(2, mLmax-mL)
Ti = 0


"""
  -mrAlgo=1 -> Classical         Partitioning (same number of elements on each partition + min edge cut)
  -mrAlgo=2 -> Single Constraint Partitioning (same total weight of elements on each partition + min edge cut)
  -mrAlgo=1 -> Multi  Constraint Partitioning (same number of elements of each multirate group on each partition + min edge cut)
"""


# Physical parameteres for bathymetry smoothing
bNu=20    # Diffusion
bT=10000  # Time for smoothing 
diff = 0


# Lauch Makefile to generate meshes and download forcings
if(Msg.GetCommRank() == 0):
  try : os.system("make "+filename+".msh");
  except: 0;
  try : os.system("make forcings");
  except: 0;
  try : os.mkdir(outputDir);
  except: 0;
Msg.Barrier()

# Project mesh in planar space

if (Msg.GetCommRank() == 0):
  functionC.buildLibraryFromFile ("BATH.cc", "./lib_bath.so");
  functionC.buildLibraryFromFile ("GBR.cc", "./lib_gbr.so");

Msg.Barrier()

glib = "./lib_gbr.so"
blib= "./lib_bath.so"

