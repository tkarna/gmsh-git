from dgpy import *
from InputTurboFan import *
from termcolor import colored
import time, math 
import os, sys

model   = GModel  ()
model.load(filename+'.msh')
groups = dgGroupCollection(model, dimension, order)
xyz = function.getCoordinates()


MFPrecomp = functionC(tlib, "meanFlow", 5, [xyz])
#MFPrecomp = functionConstant([1.,0.,0.,0.,1./1.4])
MF = functionPrecomputed(groups, 2*order+1, 5)  
MF.compute(MFPrecomp)

law = dgConservationLawLEE(dimension, "", MF, None, None)

solution = dgDofContainer(groups, law.getNbFields())
solution.setAll(0.)

rk = dgMultirateERK(groups,  law,  RK_TYPE)
dt = rk.splitGroupsForMultirate(int(sys.argv[1]),  solution,  [solution])
solution.exportGroupIdMsh()
rk.printMultirateInfo(0)

pOpt = meshPartitionOptions()
pOpt.setNumOfPartitions(int(sys.argv[2]))
if(int(sys.argv[3])==1):
  rk.computeClassicalContrainedPartition(pOpt,  algo)
if(int(sys.argv[3])==2):
  rk.computeSingleContrainedPartition(pOpt,  algo)
if(int(sys.argv[3])==3):
  rk.computeMultiConstrainedPartition(pOpt,  algo)
PartitionMesh(model, pOpt)
rk.printPartitionInfo(int(sys.argv[2]))
model.save(filename + '_%dm_%dp_%da.msh'%(int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3])))
print("%f %f %f %f"%(rk.dtMin(), rk.dtMax(), rk.dtRef(), rk.speedUp()))
Msg.Exit(0)

