from gmshpy import *
from mpi4py import MPI
from dgpy import *
from InputTurboFan import *
import os, sys
import time
import math, cmath

mL=int(sys.argv[1])

if(Msg.GetCommRank() == 0):
  print'*** Loading the mesh and the model ***'
model   = GModel  ()
model.load(filename + '_%dm_%dp_%da.msh'%(int(sys.argv[1]),  Msg.GetCommSize(),  int(sys.argv[2])))
groups = dgGroupCollection(model, dimension, order)


xyz = function.getCoordinates()


MFPrecomp = functionC(tlib, "meanFlow", 5, [xyz])
#MFPrecomp = functionConstant([1.,0.,0.,0.,1./1.4])
MF = functionPrecomputed(groups, 2*order+1, 5)  
MF.compute(MFPrecomp)

InitScaling= functionC(tlib,  "initialScaling",  5,  [xyz])


law = dgConservationLawLEE(dimension, "", MF, None, None)


if(Msg.GetCommRank() == 0):
  print'*** setting the initial solution ***'
solution = dgDofContainer(groups, law.getNbFields())
solution.setAll(0.)

rk = dgMultirateERK(groups,  law,  RK_TYPE)
dt = rk.splitGroupsForMultirate(mL,  solution,  [solution])
rk.printMultirateInfo(0)
su=rk.speedUp()

solution.interpolate(InitScaling)
#solution.setAll(0.0)

if(Msg.GetCommRank() == 0):
  print'*** solve ***'
  print 'DT=', dt
  print 'SU=', su
  print'*************'


#coeff = functionConstant([16.,10.,11.771,-5.7843e-05,10.837,0]) # Without mean flow, omega = 16, m = 10, alpha = 11.771, MOverN = -5.7843e-05, mu = (10.837,0)
coeff = functionConstant([16.,10.,11.771,-5.7843e-05,27.110,0]) # With mean flow, omega = 16, m = 10, alpha = 11.771, MOverN = -5.7843e-05, mu = (27.110,0)
time = function.getTime()
ISAmp = functionC(tlib, "inletModeAmp", 8, [xyz, coeff])
amp = functionPrecomputed(groups, 2*order+1, 8)
for i in range(groups.getNbFaceGroups()) :
  group = groups.getFaceGroup(i)
  if (group.physicalTag() == 'Fan') :
    amp.compute(ISAmp,group)
IS = functionC(tlib, "inletMode", 5, [time, amp, coeff])

InletBoundary = law.newBoundaryNonReflect(IS)
OutletBoundary = law.newBoundaryNonReflect()
InteriorBoundarySpinner = law.newBoundaryWall(True)
InteriorBoundaryDuct = law.newBoundaryWall()

law.addBoundaryCondition('Fan',InletBoundary)
law.addBoundaryCondition('Intake',OutletBoundary)
law.addBoundaryCondition('Spinner',InteriorBoundarySpinner)
law.addBoundaryCondition('Duct',InteriorBoundaryDuct)

MF.compute(MFPrecomp)

#MFViz = dgDofContainer(groups, 5)
#MFViz.interpolate(MFPrecomp)
#MFViz.exportMsh('turbofan_MF')

pvSteps=int(pow(2, mLmax-mL))
nbSteps=int(pow(2, mLmax-mL))

t=0
rk.initialize()
for i in range (0, pvSteps):
  rk.iterate (solution,  dt,  t)
  t = t + dt

timer=dgTimer.root()
timer.reset()
Msg.Barrier()
timer.start()

for i in range (0, nbSteps):
  rk.iterate (solution,  dt,  t)
  print t,  solution.norm()
  t = t + dt
timer.stop()
Msg.Barrier()
nbf2P = float(rk.nbInterfaceTerms2());
nbvP  = float(rk.nbVolumeTerms());
nbf2 = MPI.COMM_WORLD.allreduce(nbf2P,   op=MPI.SUM)
nbv = MPI.COMM_WORLD.allreduce(nbvP,   op=MPI.SUM)
maxTime = MPI.COMM_WORLD.allreduce(timer.get(),  op=MPI.MAX)
minTime = MPI.COMM_WORLD.allreduce(timer.get(),  op=MPI.MIN)
norm=solution.norm()
if (Msg.GetCommRank()==0):
  print "%.8f %f %f %f %.8f %.8f"%(norm, nbf2*int(pow(2,   mLmax-int(sys.argv[1])))/Msg.GetCommSize(), nbv*int(pow(2,   mLmax-int(sys.argv[1])))/Msg.GetCommSize(), nbf2/nbv, minTime, maxTime)

Msg.Exit(0)
