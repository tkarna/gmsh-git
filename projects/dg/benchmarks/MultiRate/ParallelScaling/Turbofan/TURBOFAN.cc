/*
 * =====================================================================================
 *
 *       Filename:  TURBOFAN.cc
 *
 *    Description:  
 *      Computes mean flow and inlet BC for turbofan intake (slowly varying duct)
 *      - Equations: cf. S. W. Rienstra, "Sound transmission in slowly varying circular
 *        and annular lined ducts with flow", Journal of Fluid Mechanics 380, p.279-296,
 *        1999
 *      - Geometry: cf. S. W. Rienstra and W. Eversman, "A numerical comparison between
 *        the multiple-scales and finite-element solution for sound propagation in lined
 *        flow ducts", Journal of Fluid Mechanics 437, p.367-384, 2001
 *
 *        Version:  1.0
 *        Created:  22.12.2010 10:25:38
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  bseny (BS), bruno.seny@uclouvain.be
 *        Company:  UCL
 *
 * =====================================================================================
 */

#include "fullMatrix.h"
#include "function.h"
#include "dgGroupOfElements.h"
#include "polynomialBasis.h"
#include <cmath>
#include <complex>

extern "C" {

void initialScaling (dataCacheMap *, fullMatrix<double> &initialCondition, fullMatrix<double> &XYZ) {
  for (size_t i = 0; i< initialCondition.size1(); i++) {
    const double x = XYZ(i,0);
    const double y = XYZ(i,1);
    const double z = XYZ(i,2);
    initialCondition.set (i, 0,0.01*sin(x)*sin(y));//elevation
    initialCondition.set (i, 1,0.01*sin(x)*cos(y));//vel-x
    initialCondition.set (i, 2,0.01*cos(z)*sin(y));//vel-y
    initialCondition.set (i, 3,0.01*sin(x)*sin(z));//elevation
    initialCondition.set (i, 4,0.01*sin(z)*cos(y));//vel-x
  }
}
  void inletModeAmp(dataCacheMap *dCM, fullMatrix<double> &f, fullMatrix<double> &xyz, fullMatrix<double> &Coeff)
  {
    const double omega = Coeff(0, 0);
    const int m = (int)Coeff(0, 1);
    const double alpha = Coeff(0, 2), MOverN = Coeff(0, 3);
    const std::complex<double> mu = std::complex<double>(Coeff(0, 4), Coeff(0, 5));

    typedef fullMatrix<std::complex<double> > fullMatComp;

    for (size_t i = 0; i < f.size1(); i++) {
      double &x = xyz(i, 0);
      double &y = xyz(i, 1);
      double &z = xyz(i, 2);
      double theta = atan2(z, y);
      double r = sqrt(y*y+z*z);
      std::complex<double> pAmp, UAxAmp;
      if (alpha == 0.) {                                            // Pure spin (radially uniform) if alpha = 0
        pAmp = std::exp(std::complex<double>(0.,m*theta));
        UAxAmp = pAmp;
      }
      else {
        pAmp = (jn(m,alpha*r)+MOverN*yn(m,alpha*r))*std::exp(std::complex<double>(0.,m*theta));
        UAxAmp = mu*pAmp/omega;
      }
      f(i, 0) = pAmp.real();
      f(i, 1) = pAmp.imag();
      f(i, 2) = UAxAmp.real();
      f(i, 3) = UAxAmp.imag();
      if(r == 0.){
        f(i, 4) = 0.;
        f(i, 5) = 0.;
        f(i, 6) = 0.;
        f(i, 7) = 1.;
      }
      else {
        std::complex<double> UTheta = m/(r*omega)*pAmp;
        f(i, 4) = -sin(theta)*UTheta.real();
        f(i, 5) = -sin(theta)*UTheta.imag();
        f(i, 6) = cos(theta)*UTheta.real();
        f(i, 7) = cos(theta)*UTheta.imag();
      }
    }

  }


  void inletMode(dataCacheMap *dCM, fullMatrix<double> &f, fullMatrix<double> &time, fullMatrix<double> &Amp, fullMatrix<double> &Coeff)
  {

    const double omega = Coeff(0, 0);

    typedef fullMatrix<std::complex<double> > fullMatComp;

    const double cosMOT = cos(-omega*time(0, 0)), sinMOT = sin(-omega*time(0, 0));

    for (size_t i = 0; i < f.size1(); i++) {
      f(i, 0) = Amp(i, 0)*cosMOT-Amp(i, 1)*sinMOT;
      f(i, 1) = Amp(i, 2)*cosMOT-Amp(i, 3)*sinMOT;
      f(i, 2) = Amp(i, 4)*cosMOT-Amp(i, 5)*sinMOT;
      f(i, 3) = Amp(i, 6)*cosMOT-Amp(i, 7)*sinMOT;
      f(i, 4) = f(i, 0);
    }

  }



  void meanFlow(dataCacheMap *dCM, fullMatrix<double> &f, fullMatrix<double> &xyz)
  {
	  // Constants: coordinates & numerics
    static const int AXIALDIR = 0, RADIALDIR1 = 1, RADIALDIR2 = 2;  // Axial and radial directions for tranfo. cyl. coord. <-> Cart. coord.
    static const double Tol = 1.e-8;                                // Tolerance on D0 when solving 1D gas eq.
    static const int ItMax = 50000;                                 // Max. nb. of iterations when solving 1D gas eq.
    static const double exp11 = exp(-11.), omexp11 = 1.-exp11;

    // Constants: physics, flow parameters and geometry
    static const double GAMMA = 1.4;																// Ratio of specific heats
    static const double GAMMAM1 = GAMMA-1.;
    static const double GAMMAM1INV = 1./GAMMAM1;
	  static const double E = 2.625, F = 0.410299942231283;					  // Constants of problem (see ref. paper)
    static const double L = 1.86393;                                // Duct length
    static const double ooL = 1./L;
    static const double lpS = 0.609181213866991, lS = lpS*L;        // Axial position of spinner tip (reduced and physical)

    for (size_t i = 0; i < f.size1(); i++) {

      // Cylindrical coordinates
      const double l = xyz(i,AXIALDIR);																												    // Axial coord.
      const double &xyzRad1 = xyz(i,RADIALDIR1), &xyzRad2 = xyz(i,RADIALDIR2);
      const double r = std::sqrt(xyzRad1*xyzRad1+xyzRad2*xyzRad2);	                              // Radial coord.

      // Geometry
      const double lp = l*ooL;																											              // Reduced axial coordinate
      const double omlp = 1.-lp, lpSq = lp*lp;
      const double r2 = 1.-0.18453*lpSq+0.10158*(exp(-11.*omlp)-exp11)/omexp11;			              // R2
      const double dr2 = (1.11738*exp(-11*omlp)/omexp11-0.36906*lp)*ooL;						              // dR2/dx
      const double r1 = (lp >= lpS) ? 0. : 0.64212-sqrt(0.04777+0.98234*lpSq);			              // R1
      const double dr1 = (lp >= lpS) ? 0. : -0.98234*lp/sqrt(0.98234*lpSq/(ooL*ooL)+0.04777);	    // dR1/dx

      // Useful quantities
      const double r1Sq = r1*r1;
      const double r2Sq = r2*r2;
      const double rSqDiff = r2Sq-r1Sq;
      const double rInv = (r == 0.) ? 0. : 1./r;

      // Calculate mean density D0: resolve Eq. (3.3a) by Newton's method
      double d0Old, d0 = 1.;
      int it = 0;
      do {
        it++;
        const double ratio = F/(d0*rSqDiff);
        const double ratioSq = ratio*ratio;
        const double d0Pow = std::pow(d0,GAMMAM1);
        const double f = 0.5*ratioSq+GAMMAM1INV*d0Pow-E;
        const double df = (d0Pow-ratioSq)/d0;
        d0Old = d0;
        d0 -= f/df;
      } while ((std::fabs(d0-d0Old) >= Tol*d0Old) && (it <= ItMax));

      if (it >= ItMax) std::cout << "WARNING: Solving finished with max. nb. of iterations for l = " << l << ", d0 = " << d0 << std::endl;

      // Calculate u0 and p0 from d0 (Eq. 3.2, 3.3b, caution: minus sign in Eq. 3.2 because U0 < 0 here)
      const double u0 = -F/(d0*rSqDiff);
      const double p0 = std::pow(d0,GAMMA)/GAMMA;

      // Calculate v1 from d0 (Eq. 3.4, caution: minus sign because U0 < 0 here)
      const double v1 = (r == 0.) ? 0. : -F/(r*d0*rSqDiff)*((r*r-r1Sq)*(r2*dr2-r1*dr1)/rSqDiff+r1*dr1);

      // Convert mean flow velocity to Cartesian coordinates and return result
      f(i,0) = d0;											      // Density
      f(i,1+AXIALDIR) = u0;						        // X-velocity
      f(i,1+RADIALDIR1) = v1*xyzRad1*rInv;	  // Y-velocity
      f(i,1+RADIALDIR2) = v1*xyzRad2*rInv;	  // Z-velocity
      f(i,4) = p0;											      // Pressure

//    	// Useful quantities for derivatives
//    	const double FSq = F*F;
//    	const double d0Pow = std::pow(d0,GAMMAM1), d0Pow2 = std::pow(d0,GAMMA+1);
//    	const double dd0 = 2.*d0*FSq*(r2*dr2-r1*dr1)/(rSqDiff*(d0Pow2*rSqDiff*rSqDiff-FSq));
//    	const double dp0 = dd0*d0Pow;

    }

  }

}
