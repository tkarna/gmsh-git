RK="rk22c"
ML=8
AL=3
NPPN=1
echo $RK'_stommel_'$NPPN'_'$ML'_'$AL
echo "" > $RK'_stommel_'$NPPN'_'$ML'_'$AL
for i in 1 2 4 8 16; do
  rundgpy PartitionStommel.py $ML $i $AL
  for j in 1; do
    Mpirun1 -np $((i*NPPN)) rundgpy ScalingMultirateStommel.py $ML $AL | tee log
    time=`tail -n1 log`
    echo $i $j $time >> $RK'_stommel_'$NPPN'_'$ML'_'$AL
  done
done

