from dgpy import *
from termcolor import colored
from math import *
from mpi4py import MPI
import time,  os, sys 
from InputStommel import *


model = GModel()
model.load ('stommel%d_%dm_%dp_%da.msh'%(order, int(sys.argv[1]), Msg.GetCommSize(),int(sys.argv[2])))

CCode = """
#include "fullMatrix.h"
#include "function.h"
extern "C" {
void wind (dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< sol.size1(); i++) {
    sol.set(i,0,sin(xyz(i,1)/1e6)/1e6);
    sol.set(i,1,0);
  }
}
void coriolis (dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< sol.size1(); i++) {
    sol.set(i,0,1e-4+2e-11*xyz(i,1));
  }
}
void extractEta (dataCacheMap *, fullMatrix<double> &eta, fullMatrix<double> &fullSol) {
  for (size_t i = 0; i< fullSol.size1(); i++) {
    eta.set(i,0,fullSol(i,0));
  }
}
}
"""

tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CCode, tmpLib);
Msg.Barrier()

groups = dgGroupCollection(model, dimension, order)
claw = dgConservationLawShallowWater2d()
solution = dgDofContainer(groups, claw.getNbFields())

XYZ = groups.getFunctionCoordinates();
f0 = functionC(tmpLib,"coriolis",1,[XYZ, functionSolution.get()])
claw.setCoriolisFactor(f0)
f1 = functionConstant(0)
claw.setQuadraticDissipation(f1)
claw.setIsLinear(1)
f2 = functionConstant([1e-6])
claw.setLinearDissipation(f2)
f3 = functionC(tmpLib,"wind",2,[XYZ])
claw.setSource(f3)
f4 = functionConstant([1000])
claw.setBathymetry(f4)
f5 = functionConstant([0,0,0])
claw.setBathymetryGradient(f5)

bndWall = claw.newBoundaryWall()
claw.addBoundaryCondition('Wall', bndWall)

rk = dgMultirateERK(groups, claw, RKTYPE)
dt=rk.splitGroupsForMultirate(int(sys.argv[1]),   solution, [solution], fact)
rk.printPartitionInfo()


Msg.Info("SPEEDUP=%f"%(rk.speedUp()))

t=0
pvSteps=int(pow(2,  mLmax-int(sys.argv[1])))
for i in range (1,pvSteps+1) :
  norm = rk.iterate (solution, dt, t)
  t = t +dt

nbSteps=NN*int(pow(2,  mLmax-int(sys.argv[1])))
timer=dgTimer.root()
timer.reset()
Msg.Barrier()
timer.start()
for i in range (1,nbSteps+1) :
  norm = rk.iterate (solution, dt, t)
  t = t +dt
timer.stop()
Msg.Barrier()
nbf2P = float(rk.nbInterfaceTerms2());
nbvP  = float(rk.nbVolumeTerms());
nbf2 = MPI.COMM_WORLD.allreduce(nbf2P,   op=MPI.SUM)
nbv = MPI.COMM_WORLD.allreduce(nbvP,   op=MPI.SUM)
maxTime = MPI.COMM_WORLD.allreduce(timer.get(),  op=MPI.MAX)
minTime = MPI.COMM_WORLD.allreduce(timer.get(),  op=MPI.MIN)
norm=solution.norm()
if (Msg.GetCommRank()==0):
  print "%.15f %f %f %f %f %.8f %.8f"%(norm,rk.globalImbalance(),nbf2*int(pow(2,   mLmax-int(sys.argv[1])))/Msg.GetCommSize(), nbv*int(pow(2,   mLmax-int(sys.argv[1])))/Msg.GetCommSize(), nbf2/nbv, minTime, maxTime)

Msg.Exit(0)

