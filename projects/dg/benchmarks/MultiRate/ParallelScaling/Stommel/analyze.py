from pylab import *
import os,  sys
NPPN="6"
#rk=["rk22c", "rk33c", "rk44c", "rk43s"]
rk=["rk22C", "rk33C", "rk44C", "rk43S"]
colors=["b-s", "r-o", "g-^","c-+"]
nppn=int(NPPN)
ML="8"
AL="3"


def loadFile(filename):
  data = loadtxt(filename)
  lFile=len(data)
  procs=[]
  for i in range (0,   lFile):
    if(data[i,  0] not in procs):
      procs.append(data[i,  0])
      
  procs.sort()
  ndp=len(procs)
  cputime=[inf]*ndp
  for i in range (0,  lFile):
    t=procs.index(data[i,  0])
    cputime[t]=min(cputime[t],  data[i,  7])
  return procs,  cputime

for ll in range(0, len(rk)):
  Fname=rk[ll]+"_stommel_"+NPPN+"_"+ML+"_"+AL+".txt"
  os.system('scp seny@ulghost:/home/seny/SOURCES/gmsh/projects/dg/benchmarks/MultiRate/ParallelScaling/Stommel/%s .'%(Fname))
  procs,  cputime = loadFile(Fname)
  print '-----------------------------------------------'
  print Fname
  print '-----------------------------------------------'
  print array([procs, (procs[0]*cputime[0]/cputime)]).T
  print '-----------------------------------------------'
  loglog(procs, procs, 'k')
  loglog(procs, (procs[0]*cputime[0]/cputime), colors[ll])
  xticks([1*nppn, 2*nppn, 4*nppn, 8*nppn, 16*nppn], [1*nppn, 2*nppn, 4*nppn, 8*nppn, 16*nppn])
  yticks([1*nppn, 2*nppn, 4*nppn, 8*nppn, 16*nppn], [1*nppn, 2*nppn, 4*nppn, 8*nppn, 16*nppn])
  grid(True)

show()
