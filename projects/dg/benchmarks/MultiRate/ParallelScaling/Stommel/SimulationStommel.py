from dgpy import *
from termcolor import colored
from math import *
from mpi4py import MPI
import time,  os, sys 
from InputStommel import *


model = GModel()
model.load ('stommel%d_%dm_%dp_%da.msh'%(order, int(sys.argv[1]), Msg.GetCommSize(),int(sys.argv[2])))

CCode = """
#include "fullMatrix.h"
#include "function.h"
extern "C" {
void wind (dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< sol.size1(); i++) {
    sol.set(i,0,sin(xyz(i,1)/1e6)/1e6);
    sol.set(i,1,0);
  }
}
void coriolis (dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< sol.size1(); i++) {
    sol.set(i,0,1e-4+2e-11*xyz(i,1));
  }
}
void extractEta (dataCacheMap *, fullMatrix<double> &eta, fullMatrix<double> &fullSol) {
  for (size_t i = 0; i< fullSol.size1(); i++) {
    eta.set(i,0,fullSol(i,0));
  }
}
}
"""

tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CCode, tmpLib);
Msg.Barrier()

groups = dgGroupCollection(model, dimension, order)
claw = dgConservationLawShallowWater2d()
solution = dgDofContainer(groups, claw.getNbFields())

XYZ = groups.getFunctionCoordinates();
f0 = functionC(tmpLib,"coriolis",1,[XYZ, functionSolution.get()])
claw.setCoriolisFactor(f0)
f1 = functionConstant(0)
claw.setQuadraticDissipation(f1)
claw.setIsLinear(1)
f2 = functionConstant([1e-6])
claw.setLinearDissipation(f2)
f3 = functionC(tmpLib,"wind",2,[XYZ])
claw.setSource(f3)
f4 = functionConstant([1000])
claw.setBathymetry(f4)
f5 = functionConstant([0,0,0])
claw.setBathymetryGradient(f5)

bndWall = claw.newBoundaryWall()
claw.addBoundaryCondition('Wall', bndWall)

rk = dgMultirateERK(groups, claw, RKTYPE)
dt=rk.splitGroupsForMultirate(int(sys.argv[1]),   solution, [solution], fact)


Msg.Info("SPEEDUP=%f"%(rk.speedUp()))

t=Ti
nbSteps = int(ceil((Tf-Ti)*3600/dt))
timer=dgTimer.root()
timer.reset()
Msg.Barrier()
timer.start()
nbSteps=10000
for i in range (1,nbSteps+1) :
  norm = rk.iterate (solution, dt, t)
  t = t +dt
  if(i%10==0):
    timer.stop()
    Msg.Barrier()
    maxTime = MPI.COMM_WORLD.allreduce(timer.get(),    op=MPI.MAX)
    minTime = MPI.COMM_WORLD.allreduce(timer.get(),    op=MPI.MIN)
    norm=solution.norm() 
    if (Msg.GetCommRank()==0):
      print "%4d %3.8f %3.8f %.8f %.8f"%(i,  t/3600,  norm,  minTime,   maxTime)
    Msg.Barrier()
    timer.reset()
    Msg.Barrier()
    timer.start()


Msg.Exit(0)

