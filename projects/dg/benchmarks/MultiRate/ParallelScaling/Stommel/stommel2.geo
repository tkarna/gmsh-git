l = 5e5;
Point(1) = {-l, -l, 0, 4e5};
Point(2) = {l, -l, 0, 4e5};
Point(3) = {l, l, 0, 4e5};
Point(4) = {-l, l, 0, 0.04e5};
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line Loop(5) = {3, 4, 1, 2};

Plane Surface(6) = {5};
Physical Surface("Surface")={6};
Physical Line("Wall") = {1,2,3,4};
