from dgpy import *
from termcolor import colored
from math import *
import time,  os, sys
from InputStommel import *

model = GModel()
model.load ('stommel%d.msh'%(order))


CCode = """
#include "fullMatrix.h"
#include "function.h"
extern "C" {
void wind (dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< sol.size1(); i++) {
    sol.set(i,0,sin(xyz(i,1)/1e6)/1e6);
    sol.set(i,1,0);
  }
}
void coriolis (dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< sol.size1(); i++) {
    sol.set(i,0,1e-4+2e-11*xyz(i,1));
  }
}
void extractEta (dataCacheMap *, fullMatrix<double> &eta, fullMatrix<double> &fullSol) {
  for (size_t i = 0; i< fullSol.size1(); i++) {
    eta.set(i,0,fullSol(i,0));
  }
}
}
"""

tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CCode, tmpLib);
Msg.Barrier()

groups = dgGroupCollection(model, dimension, order)
claw = dgConservationLawShallowWater2d()
solution = dgDofContainer(groups, claw.getNbFields())

XYZ = groups.getFunctionCoordinates();
f0 = functionC(tmpLib,"coriolis",1,[XYZ, functionSolution.get()])
claw.setCoriolisFactor(f0)
f1 = functionConstant(0)
claw.setQuadraticDissipation(f1)
claw.setIsLinear(1)
f2 = functionConstant([1e-6])
claw.setLinearDissipation(f2)
f3 = functionC(tmpLib,"wind",2,[XYZ])
claw.setSource(f3)
f4 = functionConstant([1000])
claw.setBathymetry(f4)
f5 = functionConstant([0,0,0])
claw.setBathymetryGradient(f5)

bndWall = claw.newBoundaryWall()
claw.addBoundaryCondition('Wall', bndWall)

rk = dgMultirateERK(groups, claw, RKTYPE)
dt=rk.splitGroupsForMultirate(int(sys.argv[1]),   solution, [solution], fact)
solution.exportGroupIdMsh()
rk.printMultirateInfo()

pOpt = meshPartitionOptions()
pOpt.setNumOfPartitions(int(sys.argv[2]))
if(int(sys.argv[3])==1):
  rk.computeClassicalContrainedPartition(pOpt,  5)
if(int(sys.argv[3])==2):
  rk.computeSingleContrainedPartition(pOpt,  5)
if(int(sys.argv[3])==3):
  rk.computeMultiConstrainedPartition(pOpt,  algo)
PartitionMesh(model, pOpt)
rk.updatePartitionInfo(int(sys.argv[2]))
rk.printPartitionInfo()
model.save('stommel%d_%dm_%dp_%da.msh'%(order, int(sys.argv[1]), int(sys.argv[2]),int(sys.argv[3])))
print("%f %f %f %f %f"%(fact, rk.dtMin(), rk.dtMax(), rk.dtRef(), rk.speedUp()))
Msg.Exit(0)

