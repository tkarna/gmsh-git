from gmshpy import *
from dgpy import *
import os, time, math, sys

geoname = 'BlasiusTransfinite'
#geoname = 'Blasius'
#geoname = 'BlasiusBL'

order = int(sys.argv[1])
meshname = geoname+'_'+str(order)+'o'

outputDir='output'

dimension = 2
clscale = 1.0

multDt = 100
Ti = 0
Tf = 1000

#nbSteps = int(math.ceil((Tf-Ti)/dt))
nbStepsSave = 5
nbSteps = 500 * nbStepsSave

RKTYPE = ERK_43_S
algo = 5
partitionMethod = 3
fact = 1.0
tolerance = 0.02

Mach = 0.1
Reynolds = 5000
Prandtl = 0.72

D = 2
P = 101325
R = 287.1
Gamma = 1.4
T = 293.15

Cp = Gamma * R / (Gamma-1)
rho = P / ( R * T)
c = math.sqrt(Gamma * R * T)
V = Mach*c
mu = rho * V * D / Reynolds
k = mu * Cp / Prandtl

#time0 = time.time()
#print("Temps %d"%(time.time()))

CCodeInitial="""
#include "GmshMessage.h"
#include "function.h"
#include "dgGroupOfElements.h"
#include "dgSpaceTransform.h"
#include "fullMatrix.h"
#include <cstdlib>

#define rho %.15e
#define V %.15e
#define P %.15e
#define Gamma %.15e

extern "C" {

void freeStream(dataCacheMap *cacheMap, fullMatrix<double> &FCT, fullMatrix<double> &XYZ) {
  for(int i=0; i<XYZ.size1(); i++){
    double y = XYZ.get(i,1);
    FCT.set(i,0,rho);
    FCT.set(i,1,rho*V);
    FCT.set(i,2,0);
    FCT.set(i,3, 0.5*rho*V*V+P/(Gamma-1));
  }
}

void topStream(dataCacheMap *cacheMap, fullMatrix<double> &FCT, fullMatrix<double> &sol) {
  for(int i=0; i<sol.size1(); i++){
    double vSol = sol.get(i,2);
    FCT.set(i,0,rho);
    FCT.set(i,1,rho*V);
    FCT.set(i,2,rho*vSol);
    FCT.set(i,3, 0.5*rho*(V*V+vSol*vSol)+P/(Gamma-1));
  }
}

}""" % (rho,V,P,Gamma)
    
if (Msg.GetCommRank()==0):
  functionC.buildLibrary (CCodeInitial, "./ccodeInitial.so",True);
Msg.Barrier()

def free_stream(FCT,  XYZ) :
    for i in range (0,XYZ.size1()) :
        y = XYZ.get(i,1)
        FCT.set(i,0,rho)
        #FCT.set(i,1,rho*V*(1-math.exp(-y/100*600)))
        FCT.set(i,1,rho*V)
        FCT.set(i,2,0)
        FCT.set(i,3, 0.5*rho*V*V+P/(Gamma-1))

def top_stream(FCT, sol) :
    for i in range (0,sol.size1()) :
        vSol = sol.get(i,2)
        FCT.set(i,0,rho)
        FCT.set(i,1,rho*V)
        FCT.set(i,2,rho*vSol)
        FCT.set(i,3, 0.5*rho*(V*V+vSol*vSol)+P/(Gamma-1))

def rot(f,sol_grad,x) :
    for i in range(0,sol_grad.size1()) :
        dudy = sol_grad.get(i,1*3+1)
        dvdx = sol_grad.get(i,2*3+0)
        f.set(i,0,dudy-dvdx)

def vit(f,sol,x) :
    for i in range(0,sol.size1()) :
        rhoSol = sol.get(i,0)
        rhouSol = sol.get(i,1)
        uSol = rhouSol/rhoSol
        f.set(i,0,uSol)

#-------------------------------------------------
#-- Save parameters
#-------------------------------------------------
def saveParameters(folderName,dt,dtMin,dtMax,meshFullName,BC) :
  fun = open(folderName+"Parameters.dat","w")
  fun.write("geoname %s\n" % (geoname))
  fun.write("meshname %s\n" % (meshname))
  fun.write("meshFullName %s\n" % (meshFullName))
  fun.write("\n")
  fun.write("order %g\n" % (order))
  fun.write("dimension %g\n" % (dimension))
  fun.write("clscale %g\n" % (clscale))
  fun.write("\n")
  fun.write("RKTYPE %g\n" % (RKTYPE))
  fun.write("algo %g\n" % (algo))
  fun.write("partitionMethod %g\n" % (partitionMethod))
  fun.write("fact %g\n" % (fact))
  fun.write("tolerance %g\n" % (tolerance))
  fun.write("\n")
  fun.write("dt %g\n" % (dt))
  fun.write("multDt %g\n" % (multDt))
  fun.write("dtMin %g\n" % (dtMin))
  fun.write("dtMax %g\n" % (dtMax))
  fun.write("nbStepsSave %g\n" % (nbStepsSave))
  fun.write("\n")
  for i in range(0,len(BC)/2) :
    fun.write("%s %s\n" % (BC[2*i],BC[2*i+1]))
  fun.write("\n")
  fun.write("Mach %g\n" % (Mach))
  fun.write("Reynolds %g\n" % (Reynolds))
  fun.write("Prandtl %g\n" % (Prandtl))
  fun.write("D %g\n" % (D))
  fun.write("mu %g\n" % (mu))
  fun.write("V %g\n" % (V))
  fun.write("c %g\n" % (c))
  fun.write("rho %g\n" % (rho))
  fun.write("P %g\n" % (P))
  fun.write("R %g\n" % (R))
  fun.write("Gamma %g\n" % (Gamma))
  fun.write("Cp %g\n" % (Cp))
  fun.write("T %g\n" % (T))
  fun.write("k %g\n" % (k))
  fun.close()

#-------------------------------------------------
#-- Validation - COMPARISON WITH ANALYTICAL DATA
#-------------------------------------------------
def validateBlasius(_groups,_solution) :
    
    evalSol=dgFunctionEvaluator(_groups, _solution.getFunction())
    evalGrad=dgFunctionEvaluator(_groups,_solution.getFunctionGradient())
    
    analytical = open("BlasiusF.dat",'r')
    diff = 0.0
    result_sum  = 0.0
    
    line = (analytical.readline()).split()
    
    while line:
        eta = float(line[0])
        F = float(line[1])
        dF = float(line[2])
        line = (analytical.readline()).split()
        
        if (eta > 0.0) :
            result  = fullMatrixDouble(3,1)
            dresult = fullMatrixDouble(9,1)
            x = 35
            y = eta*math.sqrt(mu*x*2/V)
            evalSol.compute(x,y,0.0,result)
            evalGrad.compute(x,y,0.0,dresult)
            diff = diff + math.fabs(dF - result.get(0,0)/V)
            result_sum  = result_sum + math.fabs(dF)
    
    analytical.close()
    
    fun = open("Blasius.dat","w")
    fun2 = open("Blasius2.dat","w")
    
    for i in range(0,200):
        fun.write("%g " % (i*3./200.0))
        for j in range(0,10):
            result = fullMatrixDouble(1,1)
            evalSol.compute(2*j, i*3./200.0,0.0,result)
            fun.write("%g " % (result.get(0,0)))
        fun.write("\n")
        result2 = fullMatrixDouble(1,1)
        evalGrad.compute(i*30/200.,1.e-2,0.0,result2)
        fun2.write("%g %g" % (i*30/200.,result2.get(0,0)) )
        fun2.write("\n")
    
    fun.close()
    fun2.close()

    if(Msg.GetCommRank()==0) :
      if (diff/result_sum < 1.e-1) :
        print ("BLASIUS - COMPARISON WITH ANALYTICAL DATA - SUCCESS :-)")
        return True
      else:
        print ("BLASIUS - COMPARISON WITH ANALYTICAL DATA - FAIL :-(, err %.6f"%(diff/result_sum))
        return False

