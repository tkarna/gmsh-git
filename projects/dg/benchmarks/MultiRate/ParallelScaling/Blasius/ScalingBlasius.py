from gmshpy import *
from dgpy import *
from mpi4py import MPI
from InputBlasius import *
import os, time, math, sys

model = GModel()
model.load (meshname+'_%dm_%dp.msh'%(int(sys.argv[2]),  Msg.GetCommSize()))

groups = dgGroupCollection(model,dimension,order)
xyz = groups.getFunctionCoordinates()

law = dgPerfectGasLaw(dimension)
law.setGasConstantAndGamma(R, Gamma)
muF = functionConstant(mu)
kF = functionConstant(k)
law.setViscosityAndThermalConductivity(muF, kF)

solution = dgDofContainer(groups, law.getNbFields())

FS = functionC("./ccodeInitial.so","freeStream",4,[xyz])
TS = functionC("./ccodeInitial.so","topStream",4,[solution.getFunction()])
solution.L2Projection(FS)

wallBoundary = law.newNonSlipWallBoundary()
inletBoundary = law.newOutsideValueBoundary("",FS)
outletBoundary = law.newSymmetryBoundary('')
topBoundary = law.newOutsideValueBoundary("",TS)
leftBoundary = law.newSlipWallBoundary()
rightBoundary = leftBoundary

law.addBoundaryCondition('Wall',wallBoundary)
law.addBoundaryCondition('Inlet',inletBoundary)
law.addBoundaryCondition('Outlet',outletBoundary)
law.addBoundaryCondition('Top',topBoundary)
law.addBoundaryCondition('Left',leftBoundary)
law.addBoundaryCondition('Right',rightBoundary)

rk = dgMultirateERK(groups,law,RKTYPE)

t=Ti
dt = rk.splitGroupsForMultirate(int(sys.argv[2]),solution,[solution])
if(int(sys.argv[1])==1):
    nbSteps=8
if(int(sys.argv[1])>5):
    nbSteps=2

Msg.Barrier()
rk.initialize()
Msg.Barrier()

for i in range (1,nbSteps+1) :
    rk.iterate(solution, dt, t)
    t = t + dt
t_int=t
timer=dgTimer.root()
timer.reset()
Msg.Barrier()
timer.start()

for i in range (1,nbSteps+1) :
    rk.iterate(solution, dt, t)
    t = t + dt

timer.stop()
Msg.Barrier()

nbf2P = float(rk.nbInterfaceTerms2());
nbf1P = float(rk.nbInterfaceTerms1());
nbvP  = float(rk.nbVolumeTerms());
nbf2 = MPI.COMM_WORLD.allreduce(nbf2P,   op=MPI.SUM)
nbf1 = MPI.COMM_WORLD.allreduce(nbf1P,   op=MPI.SUM)
nbv = MPI.COMM_WORLD.allreduce(nbvP,   op=MPI.SUM)
maxTime = MPI.COMM_WORLD.allreduce(timer.get(),  op=MPI.MAX)
minTime = MPI.COMM_WORLD.allreduce(timer.get(),  op=MPI.MIN)
norm=solution.norm()
if (Msg.GetCommRank()==0):
    print "%d %.8f %f %f %f %f %f %f %.8f %.8f"%(Msg.GetCommSize(),norm,rk.bestImbalance(),rk.globalImbalance(),nbf2+nbf1,nbf2*int(pow(2,   mLmax-int(sys.argv[1])))/Msg.GetCommSize(), nbv*int(pow(2,   mLmax-int(sys.argv[1])))/Msg.GetCommSize(), nbf2/nbv,minTime/(t-t_int), maxTime/(t-t_int))


Msg.Exit(0)

