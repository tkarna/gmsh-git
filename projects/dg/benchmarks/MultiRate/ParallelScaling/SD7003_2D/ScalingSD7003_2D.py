from gmshpy import *
from dgpy import *
from mpi4py import MPI
from InputSD7003 import *
import os, time,  math, sys

model   = GModel  ()
model.load(meshname + '_%dm_%dp.msh'%(int(sys.argv[1]),  Msg.GetCommSize()))
groups = dgGroupCollection(model, dimension, order)

xyz = groups.getFunctionCoordinates()
FS = functionPython(4, free_stream, [xyz])

law=dgPerfectGasLaw(dimension)
law.setGasConstantAndGamma(R, Gamma)
muF = functionConstant(mu)
kF = functionConstant(k)
law.setViscosityAndThermalConductivity(muF, kF);

wallBoundary = law.newNonSlipWallBoundary()
slipWallBoundary = law.newSlipWallBoundary()
outsideBoundary = law.newOutsideValueBoundary("",FS)
law.addBoundaryCondition('Airfoil',wallBoundary)
law.addBoundaryCondition('Lateral',slipWallBoundary)
law.addBoundaryCondition('Box'     ,outsideBoundary)

solution = dgDofContainer(groups, law.getNbFields())
solution.L2Projection(FS)

t=0.0

rk = dgMultirateERK(groups,  law,  RKTYPE)
dt = rk.splitGroupsForMultirate(int(sys.argv[1]),  solution,  [solution], fact)
if(int(sys.argv[1])==1):
  nbSteps=512
if(int(sys.argv[1])>5):
  nbSteps=4

Msg.Barrier()
rk.initialize()
Msg.Barrier()

for i in range (1,nbSteps+1) :
  rk.iterate(solution, dt, t)
  t = t + dt
t_int=t
timer=dgTimer.root()
timer.reset()
Msg.Barrier()
timer.start()

for i in range (1,nbSteps+1) :
  rk.iterate(solution, dt, t)
  t = t + dt

timer.stop()
Msg.Barrier()

nbf2P = float(rk.nbInterfaceTerms2());
nbf1P = float(rk.nbInterfaceTerms1());
nbvP  = float(rk.nbVolumeTerms());
nbf2 = MPI.COMM_WORLD.allreduce(nbf2P,   op=MPI.SUM)
nbf1 = MPI.COMM_WORLD.allreduce(nbf1P,   op=MPI.SUM)
nbv = MPI.COMM_WORLD.allreduce(nbvP,   op=MPI.SUM)
maxTime = MPI.COMM_WORLD.allreduce(timer.get(),  op=MPI.MAX)
minTime = MPI.COMM_WORLD.allreduce(timer.get(),  op=MPI.MIN)
norm=solution.norm()
if (Msg.GetCommRank()==0):
  print "%d %.8f %f %f %f %f %f %f %.8f %.8f"%(Msg.GetCommSize(),norm,rk.bestImbalance(),rk.globalImbalance(),nbf2+nbf1,nbf2*int(pow(2,   mLmax-int(sys.argv[1])))/Msg.GetCommSize(), nbv*int(pow(2,   mLmax-int(sys.argv[1])))/Msg.GetCommSize(), nbf2/nbv,minTime/(t-t_int), maxTime/(t-t_int))

Msg.Exit(0)


