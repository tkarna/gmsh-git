from dgpy import *
from gmshpy import *
from InputSD7003 import *
import time, math, os, sys

model   = GModel  ()
model.load(meshname+'.msh')
groups = dgGroupCollection(model, dimension, order)
xyz = groups.getFunctionCoordinates()
FS = functionPython(4, free_stream, [xyz])

law=dgPerfectGasLaw(dimension)
law.setGasConstantAndGamma(R, Gamma)
muF = functionConstant(mu)
kF = functionConstant(k)
law.setViscosityAndThermalConductivity(muF, kF);

wallBoundary = law.newNonSlipWallBoundary()
slipWallBoundary = law.newSlipWallBoundary()
outsideBoundary = law.newOutsideValueBoundary("",FS)
law.addBoundaryCondition('Airfoil',wallBoundary)
law.addBoundaryCondition('Lateral',slipWallBoundary)
law.addBoundaryCondition('Box'     ,outsideBoundary)
solution = dgDofContainer(groups, law.getNbFields())
solution.L2Projection(FS)

rk=dgMultirateERK(groups,  law,  RKTYPE)
dt=rk.splitGroupsForMultirate(int(sys.argv[1]),  solution,  [solution], fact)
rk.printMultirateInfo()
print ('SU=',  rk.speedUp(),  'DTMIN=', rk.dtMin(), 'DTMAX=', rk.dtMax(), 'DTREF=', dt)
rk.printPartitionInfo()
pOpt = meshPartitionOptions()
pOpt.setNumOfPartitions(int(sys.argv[2]))
if(partitionMethod==1):
  rk.computeClassicalContrainedPartition(pOpt,  algo)
if(partitionMethod==2):
  rk.computeSingleContrainedPartition(pOpt,  algo)
if(partitionMethod==3):
  rk.computeMultiConstrainedPartition(pOpt,  algo, nbPartitioningConstraints)
PartitionMesh(model, pOpt)
rk.updatePartitionInfo(int(sys.argv[2]))
rk.printPartitionInfo()
model.save(meshname + '_%dm_%dp.msh'%(int(sys.argv[1]), int(sys.argv[2])))
print("%d %f %f %f %f %f"%(int(sys.argv[2]),rk.bestImbalance(),rk.globalImbalance(),rk.speedUp(),rk.speedUp()/rk.bestImbalance(),rk.speedUp()/rk.globalImbalance()))
Msg.Exit(0)
