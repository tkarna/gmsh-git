from gmshpy import *
from dgpy import *
import os, time,  math,  sys

order = 3
dimension = 2

meshname='sd7003_quad'+str(order)

RKTYPE = ERK_43_S
algo = 5
partitionMethod = 3
fact = 1.0
mLmax=16
nbPartitioningConstraints = 14

# Physics
Mach = 0.1
Reynolds = 64000
Prandtl = 1


D = 1
P = 101325
R = 287.1
T = 293.15
Gamma = 1.4

Cp = Gamma * R / (Gamma-1)
rho = P / ( R * T)
c = math.sqrt(Gamma * R * T)
V = Mach * c
mu  = rho * V * D / Reynolds
k = mu * Cp / Prandtl

def free_stream(FCT,  XYZ) :
  for i in range (0,XYZ.size1()) :
    FCT.set(i,0,rho) 
    FCT.set(i,1,rho*V) 
    FCT.set(i,2,0.0) 
    FCT.set(i,3, 0.5*rho*V*V+P/(Gamma-1)) 
