from dgpy import *
from mpi4py import MPI
from InputTsunami import *
from math import *
import time, os, sys

model = GModel()
model.load(filename + '_%dm_%dp_%da.msh'%(int(sys.argv[1]), Msg.GetCommSize(), int(sys.argv[2])))

os.putenv("BATHFILE", "etopo2.bin")
groups = dgGroupCollection(model, dimension, order)
xyz = groups.getFunctionCoordinates();
bath = dgDofContainer(groups, 1);

bathname=outputDir+"/"+filename+"_bath_smooth"+"/"+filename+"_bath_smooth.idx"
if(os.path.exists(bathname)):
  bath.readMsh(bathname)
else: 
  Msg.Fatal("No corresponding bathymetry file,   launch DiffuseBathymetryTsunami.py")

Finit = functionC(tlib, "initial_condition_okada_honshu",3, [xyz])
FCoriolis= functionC(tlib, "coriolis", 1, [xyz])

claw = dgConservationLawShallowWater2d()
solution = dgDofContainer(groups, claw.getNbFields())
solution.interpolate(Finit)
claw.setBathymetry(bath.getFunction())
claw.setBathymetryGradient(bath.getFunctionGradient())
claw.setCoriolisFactor(FCoriolis)
claw.setIsSpherical(6371220.)
wall=claw.newBoundaryWall()
claw.addBoundaryCondition("Coast", wall)
bottomDrag = functionC(tlib, "bottomDrag", 1, [function.getSolution(), bath.getFunction()])
claw.setQuadraticDissipation(bottomDrag)
#claw.setIsLinear(True)
#bottomDragLinear = functionC(tlib, "bottomDragLinear", 1, [function.getSolution(), bath.getFunction()])
#claw.setLinearDissipation(bottomDragLinear)

rk=dgMultirateERK(groups, claw, RK_TYPE)
dt=rk.splitGroupsForMultirate(int(sys.argv[1]), solution, [solution, bath])
#solution.exportGroupIdMsh()

if(Msg.GetCommRank() == 0):
  print 'SU=', rk.speedUp()
rk.printMultirateInfo(0)

t=Ti
if(mLmax < int(sys.argv[1])):
  Msg.Fatal("The number of multirate groups should be inferior to mLmax")
nbSteps = NN*int(pow(2,  mLmax-int(sys.argv[1])))

Msg.Barrier()
rk.initialize()
Msg.Barrier()

for i in range (1,nbSteps+1) :
  rk.iterate(solution, dt, t)
  t = t + dt

timer=dgTimer.root()
timer.reset()
Msg.Barrier()
timer.start()

for i in range (1,nbSteps+1) :
  rk.iterate(solution, dt, t)
  t = t + dt

timer.stop()
Msg.Barrier()

nbf2P = float(rk.nbInterfaceTerms2());
nbvP  = float(rk.nbVolumeTerms());
nbf2 = MPI.COMM_WORLD.allreduce(nbf2P,   op=MPI.SUM)
nbv = MPI.COMM_WORLD.allreduce(nbvP,   op=MPI.SUM)
maxTime = MPI.COMM_WORLD.allreduce(timer.get(),  op=MPI.MAX)
minTime = MPI.COMM_WORLD.allreduce(timer.get(),  op=MPI.MIN)
norm=solution.norm()
if (Msg.GetCommRank()==0):
  print "%.8f %f %f %f %.8f %.8f"%(norm, nbf2*int(pow(2,   mLmax-int(sys.argv[1])))/Msg.GetCommSize(), nbv*int(pow(2,   mLmax-int(sys.argv[1])))/Msg.GetCommSize(), nbf2/nbv, minTime, maxTime)

Msg.Exit(0)
