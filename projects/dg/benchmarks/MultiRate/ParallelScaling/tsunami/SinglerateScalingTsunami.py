from dgpy import *
from mpi4py import MPI
from SRInputTsunami import *
from math import *
import time, os, sys

model = GModel()
model.load(filename + '_sr_partitioned%d.msh'%(Msg.GetCommSize()))

groups = dgGroupCollection(model, dimension, order)
XYZ = groups.getFunctionCoordinates();
bath = dgDofContainer(groups, 1);
bathname=outputDir+"/"+filename+"_bath_smooth"+"/"+filename+"_bath_smooth.idx"
Msg.Barrier()
if(os.path.exists(bathname)):
  Msg.Info('.... Import Default Smoothed Bathymetry')
  bath.readMsh(bathname)
else: 
  Msg.Fatal("No correct bathymetry file,  launch DiffuseBathymetryTsunami.py")

claw = dgConservationLawShallowWater2d()
solution = dgDofContainer(groups, claw.getNbFields())
Finit = functionC(tlib, "initial_condition_okada_honshu",3, [XYZ])
solution.interpolate(Finit)
FCoriolis= functionC(tlib, "coriolis", 1, [XYZ])
claw.setBathymetry(bath.getFunction())
claw.setBathymetryGradient(bath.getFunctionGradient())
claw.setCoriolisFactor(FCoriolis)
claw.setIsSpherical(6371220.)
claw.setIsLinear(True)
wall=claw.newBoundaryWall()
claw.addBoundaryCondition("Coast", wall)
#bottomDrag = functionC(tlib, "bottomDrag", 1, [function.getSolution(), bath.getFunction()])
#claw.setQuadraticDissipation(bottomDrag)
bottomDragLinear = functionC(tlib, "bottomDragLinear", 1, [function.getSolution(), bath.getFunction()])
claw.setLinearDissipation(bottomDragLinear)

rk=dgMultirateERK(groups, claw, RK_TYPE)
dt=rk.splitGroupsForMultirate(mL, solution, [solution, bath])
rk.printMultirateInfo(0)

t=Ti
if(Msg.GetCommRank() == 0):
  print 'SU=', rk.speedUp()
Msg.Barrier()
rk.initialize()
Msg.Barrier()
timer=dgTimer.root()
timer.reset()
Msg.Barrier()
timer.start()
for i in range (0,nbSteps) :
  rk.iterate(solution, dt, t)
  t = t + dt
timer.stop()
Msg.Barrier()
minRatioProc=rk.nbInterfaceTerms2()/float(rk.nbVolumeTerms());
maxRatioProc=rk.nbInterfaceTerms2()/float(rk.nbVolumeTerms());
minRatio = MPI.COMM_WORLD.allreduce(minRatioProc,   op=MPI.MIN)
maxRatio = MPI.COMM_WORLD.allreduce(maxRatioProc,   op=MPI.MAX)
maxTime = MPI.COMM_WORLD.allreduce(timer.get(),   op=MPI.MAX)
minTime = MPI.COMM_WORLD.allreduce(timer.get(),   op=MPI.MIN)
norm=solution.norm()
if (Msg.GetCommRank()==0):
    print "%.8f %.8f %.8f %.8f %.8f %.8f"%(t, norm,  minRatio, maxRatio, minTime,  maxTime)
Msg.Exit(0)
