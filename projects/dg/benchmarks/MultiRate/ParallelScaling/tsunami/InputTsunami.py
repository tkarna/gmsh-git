from dgpy import *
from math import *
import os

# Multirate and Partition Parameters
RK_TYPE = ERK_22_C
algo = 5
mLmax = 7
NN = 1
fact=1

# Mesh Parameters
order = 2
dimension = 2
clscale=0.20

# Simulation Parameters
Ti = 0
Tf = 5

# File and folder names
filename = "ocean" + str(order)
outputDir = "output"

# Generate files and build folders
if(Msg.GetCommRank() == 0):
  try: os.mkdir(outputDir);
  except: 0
Msg.Barrier()
if(Msg.GetCommRank() == 0):
  try: os.system("make")
  except: 0;
Msg.Barrier()

# Build Libraries
if (Msg.GetCommRank() == 0):
  functionC.buildLibraryFromFile ("BATH.cc", "BATH.so");
  functionC.buildLibraryFromFile ("TSUNAMI.cc", "TSUNAMI.so");
blib = "BATH.so"
tlib = "TSUNAMI.so"
Msg.Barrier()
