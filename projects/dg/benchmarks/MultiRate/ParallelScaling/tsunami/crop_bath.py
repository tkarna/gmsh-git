from gmshpy import *
from dgpy import *
import time
from math import *
import os.path
import time
import os
import sys

#os.putenv("BATHFILE", "etopo2.bin")

model = GModel()
model.load("world.msh")

order = 1
dimension = 2
outputDir="tsunami/"

if (Msg.GetCommRank()==0):
  functionC.buildLibraryFromFile ("BATH.cc", "BATH.so");
Msg.Barrier()

os.putenv("BATHFILE", "etopo2.bin")
stereo = dgSpaceTransformSpherical( 6371220.)
groups = dgGroupCollection(model, dimension, order, stereo)
XYZ = groups.getFunctionCoordinates();
groups.buildGroupsOfInterfaces()
bath = dgDofContainer(groups, 1);
bathS = functionC("BATH.so", "bath", 1, [XYZ])
bathP = functionC("BATH.so", "diff", 1, [function.getSolution(), bathS])

bNu = 1e6    # dynamic viscosity
nuB = functionConstant(bNu)
dlaw = dgConservationLawAdvectionDiffusion.diffusionLaw(nuB)
dlaw.addBoundaryCondition("Coast", dlaw.new0FluxBoundary())
dlaw.setSource(bathP)
sys = linearSystemPETScDouble()
sys.setParameter("petscOptions", "-pc_type lu");
dof = dgDofManager.newCG (groups, 1, sys)
print("smooth")
solver = dgSteady(dlaw, dof)
solver.getNewton().setVerb(10)
solver.getNewton().setMaxIt(6)
solver.solve(bath)
bath.bezierCrop(10, 2e6)
bath.exportMsh(outputDir+"bathCrop", 0, 0);
bathP = functionC("BATH.so", "diff", 1, [function.getSolution(), bath.getFunction()])
dlaw = dgConservationLawAdvectionDiffusion.diffusionLaw(nuB)
dlaw.addBoundaryCondition("Coast", dlaw.new0FluxBoundary())
dlaw.setSource(bathP)
sys = linearSystemPETScDouble()
sys.setParameter("petscOptions", "-pc_type lu");
dof = dgDofManager.newCG (groups, 1, sys)
print("smooth")
solver = dgSteady(dlaw, dof)
solver.getNewton().setVerb(10)
solver.getNewton().setMaxIt(6)
solver.solve(bath)
bath.exportMsh(outputDir+"bathCropCG", 0, 0);
Msg.Exit(0)
