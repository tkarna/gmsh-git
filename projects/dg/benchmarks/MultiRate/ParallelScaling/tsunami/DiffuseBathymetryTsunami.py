from dgpy import *
from InputTsunami import *
import os

if(Msg.GetCommRank() > 1):
  Msg.Fatal("The bathymetry smoothing must be run on one single processor")

os.putenv("BATHFILE", "etopo2.bin")
model=GModel()
model.load(filename + ".msh")

groups = dgGroupCollection(model, dimension, order)
XYZ = groups.getFunctionCoordinates();
bath = dgDofContainer(groups, 1);
bathS = functionC("BATH.so", "bath", 1, [XYZ])
bath.interpolate(bathS);
nuB = functionConstant(1e6)
dlaw = dgConservationLawAdvectionDiffusion.diffusionLaw(nuB)
dlaw.setIsSpherical(6371220.)
dlaw.addBoundaryCondition("Coast", dlaw.new0FluxBoundary())
sys = linearSystemPETScDouble()
dof = dgDofManager.newCG (groups, 1, sys)
implicitEuler = dgDIRK(dlaw, dof)
implicitEuler.iterate (bath, 1, 0)
projector = L2ProjectionContinuous(dof)
projector.apply(bath, bath.getFunction())
bath.bezierCrop(30, 2e6)
exporterBath = dgIdxExporter(bath, outputDir+"/"+filename+"_bath_smooth")
exporterBath.exportIdx()

Msg.Exit(0)
