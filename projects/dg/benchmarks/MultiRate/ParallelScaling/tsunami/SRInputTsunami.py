from dgpy import *
from math import *


mL = 1
mLmax = 8
nbSteps = 10
#nbSteps = 2*int(pow(2, mLmax-mL))
pvSteps = 0

# Choose mesh (2K, 10K, 100K, 1M)
number = 2
# File Name
filename = "water"
#filename = "ocean"
#filename = "world-%i"%number
outputDir = "output"

# Mesh Parameters
order = 1
dimension = 2

# Initial & Final Time [year, month, hour, minutes, seconds]
Ti = 0

# Multirate and Partition Parameters
RK_TYPE = ERK_22_C
algo = 5

if(Msg.GetCommRank() == 0):
  try: os.system("make")
  except: 0;
Msg.Barrier()

if (Msg.GetCommRank() == 0):
  functionC.buildLibraryFromFile ("BATH.cc", "BATH.so");
  functionC.buildLibraryFromFile ("TSUNAMI.cc", "TSUNAMI.so");
blib = "BATH.so"
tlib = "TSUNAMI.so"
Msg.Barrier()
