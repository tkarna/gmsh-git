# -*- coding: utf-8 -*-
from dgpy import *
from gmshpy import *  
from numpy.linalg import *
import time, math, os
import numpy

oDir='output'

def velocity( vel, xz) :
  for i in range(xz.size1()) :
      vel.set(i,0,0)
      vel.set(i,1,0)
      vel.set(i,2,0)

def slopefunction (sl,grxyz) :
   for i in range(grxyz.size1()) :
     sl.set(i,0,0)
     sl.set(i,1,0)


msg = Msg()
order = 1
dimension = 3

h=200
c = 1e-8
alpha = 1.e-3
gamma = 100


def manufacturedSolution (x,y,z) :
  return gamma*math.exp(-c*(x*x+y*y/alpha)/(5))/math.sqrt(4*math.pi*5*alpha);

def solutionEssai(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    v = manufacturedSolution(x,y,z)
    val.set(i,0,v)

def dirichlet(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    v = manufacturedSolution(x,y,z)
    val.set(i,0,v)

def manufacturedRHS(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    U = manufacturedSolution(x,y,z)
    sol = ((4*y*y+4*alpha*x*x)*c*c-4*alpha*5*c)/(5*5*alpha)*U
    val.set(i,0,-sol)


def DIFFERENCE(VAL,sol,xyz) :
    for i in range(sol.size1()) :
        x = xyz.get(i,0)
        y = xyz.get(i,1)
        z = xyz.get(i,2)
        UH = sol.get(i,0)
        U = manufacturedSolution(x,y,z)
        diff = U-UH
        VAL.set(i,0,diff)

def Bis(fin,debut):
    for i in range(debut.size1()):
       fin.set(i,0,debut.get(i,0)*debut.get(i,0))
        
def solveEquation(groups,T,BC):
    law = dgConservationLawSW3dTracer(streamuv,streamw)
    law.setBath(constfunc1)
    law.setIsoDiff(sl,diff)
    qSFunc = functionPython(1, manufacturedRHS, [xyz])
    Qsource.interpolate(qSFunc)
    Qdiff = Qsource.getFunction()
    law.setSource(Qdiff)
    law.addBoundaryCondition('inlet',law.newOutsideValueBoundary('inlet',BC))
    law.addBoundaryCondition('outlet',law.newOutsideValueBoundary('outlet',BC))
    law.addBoundaryCondition('wall',law.newOutsideValueBoundary('wall',BC))
    law.addBoundaryCondition('symm',law.newOutsideValueBoundary('symm',BC))
    nbFields = law.getNbFields()
    print 'law'
    residual = dgDofContainer(groups,1)
    sys = linearSystemPETScBlockDouble() 
    sys.setParameter("petscOptions", "-pc_type lu -ksp_monitor")
    dof = dgDofManager.newDGBlock(groups, 1, sys) 
    steady = dgSteady(law,dof)
    steady.getNewton().setVerb(10)
    steady.solve(T)
  
E = 1
STEP = 2
N=4

angle=math.pi*5/180
j=0
for i in range(N):
      h = 5*math.pow(STEP,i)
      model=GModel()
      cmd = "gmsh -%d boxE.geo -string \'angle = %f;\' -string \'scale = %f;\' " %(dimension,angle,h)
      os.system(cmd)
      print cmd
      model.load('boxE.msh')
      model.save('%s/boxE_%s_%s.msh'%(oDir,str(j),str(i)))
      
      groups = dgGroupCollection(model, dimension, order)
      
      # Conservation law
      xyz = function.getCoordinates()
      vel = functionPython (3,velocity,[xyz])
      sl = functionPython (2,slopefunction,[xyz])
      u = functionExtractCompNew(vel,0)
      v = functionExtractCompNew(vel,1)
      streamuv = functionCatCompNew([u,v])
      streamw = functionExtractCompNew(vel,2)

      constfunc1 = functionConstant([h])
      diff = functionConstant([1])
      
      #dofs necessaires
      Qsource = dgDofContainer(groups,1)
      T = dgDofContainer(groups,1)
      Tessai = dgDofContainer(groups,1)
      Tfuncess = functionPython(1,solutionEssai,[xyz])
      Tessai.interpolate(Tfuncess)
      Tessai.exportMsh('%s/essaiT'%(oDir)+str(j)+str(i),0,0)
      
      #boundary conditions
      BC = functionPython(1, dirichlet, [xyz])

      #solve mms
      solveEquation(groups,Tessai,BC)
      Tessai.exportMsh('%s/NumericalSolution'%(oDir)+str(j)+str(i), 0, 0)

      ERROR = functionPython(1,DIFFERENCE, [Tessai.getFunction(),xyz])
      erreur = dgDofContainer(groups,1)
      erreur.interpolate(ERROR)
      erreur.exportMsh('%s/erreur'%(oDir)+str(j)+str(i),0,0)
      erreurSquare = functionPython(1,Bis,[ERROR])
      erreurSquareDof = dgDofContainer(groups, 1)
      erreurSquareDof.interpolate(erreurSquare)
      L2NORMOFERROR  = dgFunctionIntegrator(groups,erreurSquareDof.getFunction(),T)  
      VALUE =fullMatrixDouble(1,1)
      L2NORMOFERROR.compute(VALUE,"",20)  
      E2 = math.sqrt(VALUE.get(0,0))

      print 'L2 NORM OF THE ERROR FOR h ',h, ' IS ',math.sqrt(VALUE.get(0,0)),math.log(E2) 
      
exit(0)


