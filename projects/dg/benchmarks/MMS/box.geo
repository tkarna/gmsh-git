cl1 = 4000;
Point(1) = {-50000, -50000, 0, cl1};
Point(2) = {-50000, 50000, 0, cl1};
Point(3) = {50000, -50000, 0, cl1};
Point(4) = {50000, 50000, 0, cl1};
Line(1) = {2, 4};
Line(2) = {4, 3};
Line(3) = {3, 1};
Line(4) = {1, 2};

Line Loop(4) = {1,2,3,4};
Plane Surface(5) = {4};

Rotate {{0, 0, 1}, {0, 0, 0}, angle} {
  Surface{5};
}

Transfinite Line {1, 3} = scale;
Transfinite Line {4, 2} = 30*scale;
Transfinite Surface {5};
Recombine Surface "*";
Extrude {0,0,-200} {
  Surface{5};
  Layers{1};
  Recombine;
}

Mesh.Algorithm3D=4;
Physical Surface("inlet") = {5};
Physical Surface("outlet") = {27};
Physical Surface("wall") = {26,14};
Physical Surface("symm") = {18,22};
Physical Volume("vol") = {1};
