from dgpy import *
from math import *
import time, os, sys
import gmshPartition



#### Physical constants ###
gamma=1.4
Rd=287.04
g=9.80616
Cv=Rd/(gamma-1.0)
Cp=Rd*(1.0+1.0/(gamma-1.0))
kappa=Rd/Cp
p0=1.0e5
N=1.e-2
T0=300
###########################
#### Begin and end time ###
Ti=0.0
Tf=96*3600
###########################


order=2
dimension=3

integOrder=2*order+1+2
dataCacheMap.setDefaultIntegrationOrder(integOrder)

model = GModel()

name='aquaplanet'
if Msg.GetCommSize()>1:
  partStr='_part_%i' % Msg.GetCommSize()
else:
  partStr=''
model.load(name + partStr + '_3d.msh')


groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag();
R = 6371229
circleTransform = dgCircleTransform(2*R, groups)
groups._mesh.setSpaceTransform(circleTransform, groups)
groupsH = dgGroupCollection.newByTag(model, dimension-1, order, ["bottom_Top","bottom_Bottom"])
extrusion = dgExtrusion(groups, groupsH, ["bottom_Top","bottom_Bottom"])
groups.splitFaceGroupsByOrientation(extrusion, ["bottom_Top","bottom_Bottom","top_Top", "top_Bottom"]) 

groupsLowOrder = dgGroupCollection(model, dimension, order-1)
rhopLowOrder = dgDofContainer(groupsLowOrder, 1)

claw = dgEulerAtmLaw(dimension)
solution = dgDofContainer(groups, claw.getNbFields())

claw.setFilterMode(FILTER_LINEARVERTICAL);
#claw.setFilterMode(FILTER_LINEAR);

XYZ = groups.getFunctionCoordinates();
xyzCircle = transformToCircle(circleTransform)

xyzCircleInteg = functionPrecomputed(groups, integOrder, 3)
xyzCircleInteg.compute(xyzCircle)





def hydrostaticState(z) :
    thetaHs=T0*exp(N**2*z/g)
    exnerHs=1+ g**2/(Cp*T0*N**2) * (exp(-N**2*z/g)-1)
    rhoHs=p0/(Rd*thetaHs)*exnerHs**(Cv/Rd)
    return rhoHs,exnerHs,thetaHs

    return p0*((1-S/T0)+S/T0*exp(-N**2*z/g))^(Cp/Rd)
def initialCondition(FCT,  stereo) :
    R = 6371229
    rev=1
    for i in range(0,stereo.size1()):
        #Stereo to cartesian
        xi =stereo(i,0)
        beta =stereo(i,1)
        x = 4*R*R*xi/(4*R*R+xi*xi + beta*beta)
        y = 4*R*R*beta/(4*R*R+xi*xi + beta*beta)
        if (FCT.getDataCacheMap().getGroupOfElements().getPhysicalTag() == "Bottom"):
            rev = -1;
        z = R *(4*R*R-xi*xi-beta*beta)/(4*R*R+xi*xi + beta*beta) * rev
        #Cartesian to lonlat
        lambd=(atan2(y,x))+pi
        phi=asin(z/sqrt(x*x+y*y+z*z));
        #Initial state
        zh=stereo(i,2) 
        Lz=20000
        dtheta0=10
        lambdc=pi
        phic=pi/4
        rhoHs,exnerHs,thetaHs = hydrostaticState(zh)
        a=R*acos(sin(phic)*sin(phi)+cos(phic)*cos(phi)*cos(lambd-lambdc))
        A=R/3
        if (a<A):
            s=0.5*(1+cos(pi*a/A))
        else:
            s=0
        dtheta=dtheta0*s*sin(2*pi*zh/Lz)
        thetaPert=thetaHs+dtheta
        rhoPert=p0/(Rd*thetaPert)*exnerHs**(Cv/Rd)
        #Writing solution in stereo
        u0=0
        u_lambda=u0*cos(phi)
        u_phi=0
        uSt=sin(lambd)*u_lambda - (cos(lambd)*sin(phi)+x*rev*cos(phi)/(R+z*rev))*u_phi
        vSt=-cos(lambd)*u_lambda - (sin(lambd)*sin(phi)+y*rev*cos(phi)/(R+z*rev))*u_phi
        FCT.set(i,0,rhoPert-rhoHs)
        FCT.set(i,1,uSt*rhoPert)
        FCT.set(i,2,vSt*rhoPert)
        FCT.set(i,3,0)
        FCT.set(i,4,rhoPert*thetaPert-rhoHs*thetaHs)
        
#        #We try a velocity that crosses the seam
#        thetac=0
#        theta = asin(z/R)
#        lambd = atan2(y,x)
#        u = u0*sin(theta)*cos(lambd)
#        v = -u0*sin(lambd)
#        a = R/2
#        FCT.set(i,1,FCT(i,1)-sin(lambd)*u - (cos(lambd)*sin(theta)+x*rev*cos(theta)/(R+z*rev))*v)
#        FCT.set(i,2,FCT(i,2)+cos(lambd)*u - (sin(lambd)*sin(theta)+y*rev*cos(theta)/(R+z*rev))*v)


def rhoHydrostatic(FCT,  XYZ) :
    for i in range (0,XYZ.size1()) :
        z=XYZ(i,2)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        FCT.set(i,0,rhoHs)

def rhoThetaHydrostatic(FCT,  XYZ) :
    for i in range (0,XYZ.size1()) :
        z=XYZ(i,2)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        FCT.set(i,0,rhoHs*thetaHs)

def velocity3D(F, sol, stereo):
    R = 6371229
    rev = 1
    for i in range(0,sol.size1()):
        if (F.getDataCacheMap().getGroupOfElements().getPhysicalTag() == "Bottom"):
            rev = -1;
        #Horizontal components
        xi =stereo(i,0)
        beta =stereo(i,1)
        gam = stereo(i,2)
        xip = xi * (R + gam)/R
        betap = beta * (R + gam)/R
        Rp = R + gam
        vxi = sol(i,1)
        vbeta = sol(i,2)
        ux = (1-(2*xip**2)/(4*Rp*Rp+xip*xip + betap*betap)) * vxi - 2*betap*xip/(4*Rp*Rp+xip*xip + betap*betap) * vbeta
        uy = - 2*betap*xip/(4*Rp*Rp+xip*xip + betap*betap) * vxi + (1-(2*betap**2)/(4*Rp*Rp+xip*xip + betap*betap)) * vbeta
        uz = -4*Rp*xip*rev/(4*Rp*Rp+xip*xip + betap*betap) * vxi - 4*Rp*betap*rev/(4*Rp*Rp+xip*xip + betap*betap) * vbeta
        #Coordinates for vertical component
        x = 4*Rp*Rp*xip/(4*Rp*Rp+xip*xip + betap*betap)
        y = 4*Rp*Rp*betap/(4*Rp*Rp+xip*xip + betap*betap)
        z = Rp *(4*Rp*Rp-xip*xip-betap*betap)/(4*Rp*Rp+xip*xip + betap*betap) * rev
        #Both
        F.set(i,0,ux+sol(i,3)*x/Rp)
        F.set(i,1,uy+sol(i,3)*y/Rp)
        F.set(i,2,uz+sol(i,3)*z/Rp)

def cartCoord(FCT,  stereo) :
    amplFactor=500
    R = 6371229
    rev=1
    for i in range(0,FCT.size1()):
        xi =stereo(i,0)
        beta =stereo(i,1)
        gam = stereo(i,2) * amplFactor 
        xip = xi * (R + gam)/R
        betap = beta * (R + gam)/R
        Rp = R + gam
        FCT.set(i,0,4*Rp*Rp*xip/(4*Rp*Rp+xip*xip + betap*betap))
        FCT.set(i,1,4*Rp*Rp*betap/(4*Rp*Rp+xip*xip + betap*betap))
        if (FCT.getDataCacheMap().getGroupOfElements().getPhysicalTag() == "Bottom"):
            rev = -1;
        FCT.set(i,2,Rp *(4*Rp*Rp-xip*xip-betap*betap)/(4*Rp*Rp+xip*xip + betap*betap) * rev)

def exportFct(FCT,sol,vel3d,stereo):
    for i in range(0,FCT.size1()):
        z=stereo(i,2)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        rho=rhoHs+sol(i,0)
        #rhop
        FCT.set(i,0,sol(i,0))
        #uvw
        FCT.set(i,1,vel3d(i,0)/rho)
        FCT.set(i,2,vel3d(i,1)/rho)
        FCT.set(i,3,vel3d(i,2)/rho)
        #thetap
        FCT.set(i,4,(sol(i,4)+(rhoHs*thetaHs))/rho-thetaHs)
        #rhoThetap
        FCT.set(i,5,sol(i,4))
        #pp
        pp=gamma*p0/(rhoHs*thetaHs)*sol(i,4)
        FCT.set(i,6,pp)


cartXYZ=functionPython(3, cartCoord, [xyzCircle])
cartVel=functionPython(3, velocity3D, [solution.getFunction(), xyzCircle])

initF=functionPython(5, initialCondition, [xyzCircle])

claw.setCoordinatesFunction(xyzCircleInteg)

solution.interpolate(initF)

rhoHs = dgDofContainer(groups, 1)
rhoHs.interpolate(functionPython(1, rhoHydrostatic, [xyzCircle]))
rhoThetaHs = dgDofContainer(groups, 1)
rhoThetaHs.interpolate(functionPython(1, rhoThetaHydrostatic, [xyzCircle]))

claw.setHydrostaticState(rhoHs,rhoThetaHs)
claw.setPhysicalConstants(gamma,Rd,p0,g,R)


boundaryWall = claw.newBoundaryWall()
claw.addBoundaryCondition('top_Top', boundaryWall)
claw.addBoundaryCondition('bottom_Top', boundaryWall)
claw.addBoundaryCondition('top_Bottom', boundaryWall)
claw.addBoundaryCondition('bottom_Bottom', boundaryWall)

#claw.addBoundaryCondition('Seam', boundaryWall)


nCompExp=[1,3,1,1,1]
namesExp=["rhop","uvw","thetap","rhoThetap","pp"]
exportFunction=functionPython(sum(nCompExp), exportFct, [solution.getFunction(),cartVel,xyzCircle])




#timeIter = dgERK(claw, None, DG_ERK_22)
petscIm = dgLinearSystemExtrusion(claw, groups, extrusion)
##
#petscIm = linearSystemPETScBlockDouble()
##
##petscIm.setParameter("petscOptions",  "")#-ksp_type preonly -pc_type lu -pc_factor_mat_solver_package mumps")
#petscIm.setParameter("petscOptions",  "-ksp_atol 0 -ksp_rtol 1e-8")
##
dofIm = dgDofManager.newDGBlock(groups, claw.getNbFields(), petscIm)
#timeIter = dgDIRK(claw, dofIm, 2)
timeIter = dgIMEXRK(claw, dofIm, 2)     #timeorder
timeIter.getNewton().setAtol(1.e-5) #ATol
timeIter.getNewton().setRtol(1.e-6) #Rtol
timeIter.getNewton().setVerb(2)     #Verbosity

#dt=claw.getMinOfTimeSteps(solution)
dt = min(claw.getMinOfTimeSteps(solution, extrusion)*.6,300)
if (Msg.GetCommRank()==0):
    print "Time step:",dt
nbSteps = int(ceil(Tf/dt))

#initial solution
solution.exportFunctionVtk(exportFunction,'output', 0, 0,"solution",nCompExp,namesExp,cartXYZ)

t=Ti
n_export=0
start = time.clock()
for i in range(0,nbSteps):
    if (i%10 == 0 or i==nbSteps-1):
        solution.exportFunctionVtk(exportFunction,'output', t, i,"solution",nCompExp,namesExp,cartXYZ)
        if (Msg.GetCommRank()==0):
            print '\nWriting output',n_export,'at time',t,'and step',i,'over',nbSteps
            elapsed = (time.clock() - start)
            print 'Time elapsed: ',elapsed,' s'
        n_export=n_export+1
    norm = timeIter.iterate (solution, dt, t)
    t=t+dt
    if (Msg.GetCommRank()==0):
        sys.stdout.write('.')
        sys.stdout.flush()
print ''    
elapsed = (time.clock() - start)
if (Msg.GetCommRank()==0):
    print 'Time elapsed: ',elapsed,' s'
Msg.Exit(0)


