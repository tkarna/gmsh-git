from extrude import *
import sys
from math import *

nameStem = 'aquaplanet'
nbPart = int(sys.argv[1])



nbLayers = 4
H = 10000.

res=H/nbLayers

def getLayersSigma (x, y, iGroup, iElement, iNode) :
  z=0
  zTab=[z]
  while (-z)<H:
    z=z-res
    zTab.append(z)
  return zTab

partition(name+'.msh', name + partStr + '.msh', nbPart)
extrude(name + partStr + '.msh', getLayersSigma).save(name + partStr + '_3d.msh')
Msg.Exit(0)
