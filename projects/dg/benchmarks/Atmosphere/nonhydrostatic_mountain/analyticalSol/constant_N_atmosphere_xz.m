% Provided by: Frank Giraldo

function [T,rho,drhodz] = ...
             constant_N_atmosphere_xz(N,gravity,cp,gamma,rgas,T0,p00,x,z)
% [T,rho,drhodz] = constant_N_atmosphere_xz(N,gravity,cp,gamma,rgas,T0,p00,x,z)
% 
% Compute the profile at n2 = const, with prescribed T0. Notice that
% both T and theta vary with height.

 N2 = N^2;
 delta = 1.0 - (gravity^2)/(cp*N2*T0);
 expi = exp(-N2/gravity*z) + delta*(1.0 - exp(-N2/gravity*z));
 theta = T0*exp(N2/gravity*z);
 T = expi.*theta;
 p = p00*(expi.^(gamma/(gamma-1.0)));
 rho = p./(rgas*T);

 dexpidz = -N2/gravity*(1-delta)*exp(-N2/gravity*z);
 dthetadz = T0*N2/gravity*exp(N2/gravity*z);
 dpdz = p00*gamma/(gamma-1.0)*expi.^(1/(gamma-1.0)).*dexpidz;
 dTdz = expi.*dthetadz + theta.*dexpidz;
 drhodz = 1./(rgas*T).*dpdz - rho./T.*dTdz;

return

