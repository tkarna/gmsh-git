% Provided by: Frank Giraldo

function hv = profile_transform(omega,hc,ac,lc,icase)

% witch of Agnesi
switch icase
case 1 %Linear Hydrostatic Mountain
hv = sqrt(pi/2)*hc*ac*exp(-ac*abs(omega));

case 2 %Schaer Mountain
hv = ac*hc/(2*sqrt(2))*exp(-1/4*(ac*omega).^2).* ...
                      (1 + exp(-(pi*ac/lc)^2)*cosh(pi*ac^2/lc*omega));

case 3 %Linear Nonhydrostatic Mountain
hv = sqrt(pi/2)*hc*ac*exp(-ac*abs(omega));


end %switch
return

