% Provided by: Frank Giraldo
% slightly modified by Sebastien Blaise (called as a function)
% Compute the pseudo-analytic solution for mountain wave test cases.
% The main assumptions are:
%
% linear regime
% uniform mean flow U
% uniform Scorer parameter r
%
% Notation: 
% Fourier transforms are indicated with *v
% shifted arrays (exploiting periodicity) are indicated with *M
% scaled quantities with fc=sqrt(rho00/rho0) are indicated with *s
%
% h: orography profile
% W: vertical velocity
% Mountain Case: 1=Linear Hydrostatic, 2=Schaer, 3=Linear Nonhydrostatic:


function [x, z, Us, Ws, Pis, Thetas] = pseudo_analytic_solution(icase, doPlot)
   
   % Parameters
   gravity=9.80616;
   cp=1004.67d0; 
   rgas=287.17d0;
   cv=cp-rgas; 
   gamma=cp/cv; 
   xappa=rgas/cp;
   p00 = 1e5;
   % physical domain
   dz = 100;
   
   switch icase
   case 1 %Linear Hydrostatic Mountain
   hc=1;
   ac=10000;
   lc=1;
   zmax = 12000;
   xvmin = -120000;
   xvmax = +120000;
   zvmin = 0;
   zvmax = 12000;
   NBV = 0.0195;
   U0 = 20;
   T00 = 250;
   n = 4000
   %domega=-2*pi/xvmin*n/(2*n+1)
   domega   = 0.00000325;
   
   case 2 %Schaer Mountain
   hc=250;
   ac=5000;
   lc=4000;
   zmax = 12000;
   xvmin = -25000;
   xvmax = +25000;
   zvmin = 0;
   zvmax = 12000;
   NBV = 0.01;
   U0 = 10;
   T00 = 280;
   n = 4000
   %domega=-2*pi/xvmin*n/(2*n+1)
   domega   = 0.00000325;
   
   case 3%Linear Nonhydrostatic Mountain
   hc=1;
   ac=1000;
   lc=1;
   zmax = 12000;
   xvmin = -72000;
   xvmax = +72000;
   zvmin = 0;
   zvmax = 12000;
   NBV = 0.01;
   U0 = 10;
   T00 = 280;
   n = 4000
   %domega=-2*pi/xvmin*n/(2*n+1)
   domega   = 0.00000325;
   end %switch
   
   nz = zmax/dz + 1;
   z = linspace(0,zmax,nz);
   
   l = NBV/U0;
   l2 = l^2;
   
   N = 2*n + 1;
   omega = domega*[-n:1:n];
   %x = 2*pi/(N*domega)*[-n:1:n];
   
   % reference atmosphere
   %[X,Z] = meshgrid(x,z);
   %[T00,rho00] = ...
   %   constant_N_atmosphere_xz(NBV,gravity,cp,gamma,rgas,T00,p00,0,0);
   %[T0,rho0,drho0dz] = ...
   %   constant_N_atmosphere_xz(NBV,gravity,cp,gamma,rgas,T00,p00,X,Z);
   
   % evaluate the Fourier transform of the orography profile
   hv = profile_transform(omega,hc,ac,lc,icase);
   %disp('In figure 1: change N and domega to adjust the horizontal scale');
   %figure(1);
   %plot(omega,abs(hv),omega,real(hv),omega,imag(hv));
   %title('Fourier transform of the orography profile');
   %legend([' |hv| ';'Re(hv)';'Im(hv)']);
   
   % compute the Fourier transform of the vertical velocity w
   I = sqrt(-1);
   for j=1:nz
   
     % Notice that for omega==0 we need the average of the left and 
     % right limits
     ETAv(j,:) = (omega> 0) .* ( ...
                              hv.* ( ...
        (abs(omega)<=l).*(exp(I*sqrt(l2-omega.^2)*z(j))) + ...
        (abs(omega)> l).*(exp( -sqrt(omega.^2-l2)*z(j))) ...
                                   ) ...
                               ) + ...
                 (omega< 0) .* conj( ...
                              hv(end:-1:1).* ( ...
        (abs(omega)<=l).*(exp(I*sqrt(l2-(-omega).^2)*z(j))) + ...
        (abs(omega)> l).*(exp( -sqrt((-omega).^2-l2)*z(j))) ...
                                   ) ...
                               ) + ...
                 (omega==0) * (0.5*( ...
                          exp(I*sqrt(l2)*z(j)) + ...
                     conj(exp(I*sqrt(l2)*z(j))) ...
                                   ));
     
     dETAdxv(j,:) = I*omega.*ETAv(j,:);
   
     dETAdzv(j,:) = (omega> 0) .* ( ...
                              hv.* ( ...
        (abs(omega)<=l).*(exp(I*sqrt(l2-omega.^2)*z(j)).* ...
                          (I*sqrt(l2-omega.^2))) + ...
        (abs(omega)> l).*(exp( -sqrt(omega.^2-l2)*z(j)).* ...
                          (-sqrt(omega.^2-l2))) ...
                                   ) ...
                               ) + ...
                 (omega< 0) .* conj( ...
                              hv.* ( ...
        (abs(omega)<=l).*(exp(I*sqrt(l2-(-omega).^2)*z(j)).* ...
                          (I*sqrt(l2-(-omega).^2))) + ...
        (abs(omega)> l).*(exp( -sqrt((-omega).^2-l2)*z(j)).* ...
                          (-sqrt((-omega).^2-l2))) ...
                                   ) ...
                               ) + ...
                 (omega==0) * (0.5*( ...
                          exp(I*sqrt(l2)*z(j))*(I*sqrt(l2)) + ...
                     conj(exp(I*sqrt(l2)*z(j))*(I*sqrt(l2))) ...
                                   ));
   
   end
   
   % reorder as required by ifft
   hvM = circshift(hv,[0 n+1]);
   ETAvM = circshift(ETAv,[0 n+1]);
   dETAdxvM = circshift(dETAdxv,[0 n+1]);
   dETAdzvM = circshift(dETAdzv,[0 n+1]);
   
   % inverse Fourier transform
   hM = ifft(hvM);
   ETAM = ifft(ETAvM,N,2);
   dETAdxM = ifft(dETAdxvM,N,2);
   dETAdzM = ifft(dETAdzvM,N,2);
   
   % Normalize and reorder
   h = N*domega/sqrt(2*pi)*circshift(hM,[0 -n-1]);
   ETA = N*domega/sqrt(2*pi)*circshift(ETAM,[0 -n-1]);
   dETAdx = N*domega/sqrt(2*pi)*circshift(dETAdxM,[0 -n-1]);
   dETAdz = N*domega/sqrt(2*pi)*circshift(dETAdzM,[0 -n-1]);
   
   % Postprocessing
   x = 2*pi/(N*domega)*[-n:1:n];
   
   % Compute the reference atmosphere and add the scale factor 
    %   fc=sqrt(rho00/rho0)
   [X,Z] = meshgrid(x,z);
   [T00,rho00] = ...
      constant_N_atmosphere_xz(NBV,gravity,cp,gamma,rgas,T00,p00,0,0);
   [T0,rho0,drho0dz] = ...
      constant_N_atmosphere_xz(NBV,gravity,cp,gamma,rgas,T00,p00,X,Z);
   fc = sqrt(rho00./rho0);
   dfcdz = -0.5*fc.*(drho0dz./rho0);
   
   ETAs = fc.*real(ETA);
   dETAdxs = fc.*real(dETAdx);
   dETAdzs = fc.*real(dETAdz) + dfcdz.*real(ETA);
   
   % Reconstruct the solution
   Us = U0*(gravity./(gamma*rgas*T0).*ETAs - dETAdzs);
   Ws = U0*dETAdxs;
   Ps = -U0*rho0.*Us;
   Rs = -(gravity./(gamma*rgas*T0).*rho0 + drho0dz) .* ETAs;
   
   P0 = rgas*rho0.*T0;
   Pi0 = (P0/p00).^xappa;
   Theta0 = T0./Pi0;
   Ts = T0.*(Ps./P0 - Rs./rho0);
   Pis = xappa*Pi0./P0.*Ps;
   Thetas = 1./Pi0.*Ts - Theta0./Pi0.*Pis;
   
   %Shift X to conform to Numerical Solution
   x=x + xvmax;
   xvmin=0
   xvmax=2*xvmax

   if (doPlot)
     %Plot Contours
     figure(1);
     set(gca,'FontSize',18);
     contourf(x,z,Pis,20);
     colorbar('SouthOutside');
     title('Exner pressure \pi');
     axis([xvmin xvmax zvmin zvmax]);
     
     figure(2);
     set(gca,'FontSize',18);
     contourf(x,z,Us,20);
     colorbar('SouthOutside');
     title('Horizontal velocity u');
     axis([xvmin xvmax zvmin zvmax]);
     
     figure(3);
     set(gca,'FontSize',18);
     contourf(x,z,Ws,20);
     colorbar('SouthOutside');
     title('Vertical velocity w');
     axis([xvmin xvmax zvmin zvmax]);
     
     
     figure(4);
     set(gca,'FontSize',18);
     contourf(x,z,Thetas,20);
     colorbar('SouthOutside');
     title('Potential Temperature \theta');
     axis([xvmin xvmax zvmin zvmax]);
   end
end
