from partition1d import *
from dgpy import *
from extrude import *
from rotateZtoY import *

H=30000
def hMount(x):
    hc=1
    xc=72000
    ac=1000
    return hc/(1+((x-xc)/ac)**2)

name='line'
nbPart = int(sys.argv[1])

nbLayers = 18
H = 33000.

res=H/nbLayers

def getLayersSigma (element, vertex) :
  x = vertex[0]
  hm=hMount(x)
  z=0
  zMod=hm
  zTab=[-zMod]
  for i in range(nbLayers):
    z=z+res
    zMod = z * (H - hm) / H + hm
    zTab.append(-zMod)
  return zTab

if nbPart>1:
  partStr='_part_%i' % nbPart
else:
  partStr=''
partition1d(name+'.msh', name + partStr + '.msh', nbPart)
extrude(mesh(name + partStr + '.msh'), getLayersSigma).write(name + partStr + '_2d.msh')
rotateZtoY(name + partStr + '_2d.msh',name + partStr + '_2d_XY.msh')
Msg.Exit(0)
