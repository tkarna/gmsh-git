nbx=80;
Point(1) = {-50000, 0, 0};
Point(2) = {50000, 0, 0};
Line(1) = {1, 2};
Transfinite Line {1} = nbx+1;
Physical Line("domain") = {1};
Physical Point("left") = {1};
Physical Point("right") = {2};
