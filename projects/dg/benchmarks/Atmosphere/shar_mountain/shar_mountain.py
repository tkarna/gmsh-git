from dgpy import *
from math import *
import time, os, sys
from oct2py import octave
from scipy import interpolate
import numpy
from matplotlib import pyplot as plt

#### Physical constants ###
gamma=1.4
Rd=287
p0=1.0e5
g=9.80616
Cv=Rd/(gamma-1)
Cp=Rd*(1+1/(gamma-1))
if (Msg.GetCommRank()==0):
  print "gamma:",gamma,"Rd:",Rd,"Cv:",Cv,"Cp:",Cp,"p0:",p0,"g:",g
###########################
#### Begin and end time ###
Ti=0
Tf=5*3600*20
###########################
#### Parameters
U = 10

order=3
dimension=2

model = GModel()
name='line'
if Msg.GetCommSize()>1:
  partStr='_part_%i' % Msg.GetCommSize()
else:
  partStr=''
model.load(name + partStr + '_2d_XY.msh')

groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag();
groupsH = dgGroupCollection.newByTag(model, dimension-1, order, ["bottom_domain"])
extrusion = dgExtrusion(groups, groupsH, ["bottom_domain"])
groups.splitFaceGroupsByOrientation(extrusion, ["bottom_domain", "top_domain"]) 

claw = dgEulerAtmLaw(dimension)
solution = dgDofContainer(groups, claw.getNbFields())

XYZ = groups.getFunctionCoordinates()
            
def hydrostaticState(z) :
    theta0=280
    N=1.e-2
    thetaHs=theta0*exp(N**2*z/g)
    exnerHs=1+ g**2/(Cp*theta0*N**2) * (exp(-N**2*z/g)-1)
    rhoHs=p0/(Rd*thetaHs)*exnerHs**(Cv/Rd)

    return rhoHs,thetaHs

def initialCondition(FCT,  XYZ) :
    a=5000
    dtheta0=1.0e-2
    H=10000
    xc=-50000
    for i in range (0,XYZ.size1()) :
        x=XYZ(i,0)
        z=XYZ(i,1)
        rhoHs,thetaHs = hydrostaticState(z)
        FCT.set(i,0,0)
        FCT.set(i,1,U*rhoHs)
        FCT.set(i,2,0.0)
        FCT.set(i,3,0)

def rhoHydrostatic(FCT,  XYZ) :
    for i in range (0,XYZ.size1()) :
        z=XYZ(i,1)
        rhoHs,thetaHs = hydrostaticState(z)
        FCT.set(i,0,rhoHs)

def rhoThetaHydrostatic(FCT,  XYZ) :
    for i in range (0,XYZ.size1()) :
        z=XYZ(i,1)
        rhoHs,thetaHs = hydrostaticState(z)
        FCT.set(i,0,rhoHs*thetaHs)
        
def getVelocity(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,1)
        rhoHs,thetaHs = hydrostaticState(z)
        rho=rhoHs+sol(i,0)
        FCT.set(i,0,sol(i,1)/rho)
        FCT.set(i,1,sol(i,2)/rho)

def getVelocityPert(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,1)
        rhoHs,thetaHs = hydrostaticState(z)
        rho=rhoHs+sol(i,0)
        FCT.set(i,0,sol(i,1)/rho-U)
        FCT.set(i,1,sol(i,2)/rho)

def getRhop(FCT, sol) :
    for i in range (0,sol.size1()) :
        FCT.set(i,0,sol(i,0))

def getpp(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,1)
        rhoHs,thetaHs = hydrostaticState(z)
        rho=rhoHs+sol(i,0)
        rhoTheta = rhoHs*thetaHs+sol(i,3)
        pHs=p0*(rhoHs*thetaHs*Rd/p0)**gamma
        p=p0*(rhoTheta*Rd/p0)**gamma
        FCT.set(i,0,p-pHs)

def getExnerp(FCT, p, pHs) :
  for i in range (0,FCT.size1()) :
    FCT.set(i,0,(p(i,0)/p0)**(Rd/Cp)-(pHs(i,0)/p0)**(Rd/Cp))

def getpHs(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,1)
        rhoHs,thetaHs = hydrostaticState(z)
        pHs=p0*(rhoHs*thetaHs*Rd/p0)**gamma
        FCT.set(i,0,pHs)

def getp(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,1)
        rhoHs,thetaHs = hydrostaticState(z)
        rhoTheta = rhoHs*thetaHs+sol(i,3)
        p=p0*(rhoTheta*Rd/p0)**gamma
        FCT.set(i,0,p)

def getThetap(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,1)
        rhoHs,thetaHs = hydrostaticState(z)
        rho=rhoHs+sol(i,0)
        FCT.set(i,0,(sol(i,3)+(rhoHs*thetaHs))/rho-thetaHs)

def getNumericalSol(FCT, uwPert, exnerPert, thetaPert):
  for i in range (0,FCT.size1()) :
    FCT.set(i,0,uwPert(i,0))
    FCT.set(i,1,uwPert(i,1))
    FCT.set(i,2,exnerPert(i,0))
    FCT.set(i,3,thetaPert(i,0))
CCode = """
#include "fullMatrix.h"
#include "function.h"
extern "C" {

void sponge (dataCacheMap *, fullMatrix<double> &FCT, fullMatrix<double> &XYZ, fullMatrix<double> &sol, fullMatrix<double> &reference) {
    double g=9.80616;
    for (int i=0; i<XYZ.size1(); i++){
        double z=XYZ(i,1);
        double zb=23000;
        double zt=30000;
        double alpha=0.01;
        if (z<zb){
            FCT.setAll(0);
        }else{
            double tau=-alpha/2*(1+cos(((z-zb)/(zt-zb)-1)*M_PI));
            FCT.set(i,0,tau*(sol(i,0)-reference(i,0)));
            FCT.set(i,1,tau*(sol(i,1)-reference(i,1)));
            FCT.set(i,2,tau*(sol(i,2)-reference(i,2)));
            FCT.set(i,3,tau*(sol(i,3)-reference(i,3)));
        }
    }
}
}
"""
if (Msg.GetCommRank()==0):
    functionC.buildLibrary (CCode, "sourceTerm.so");
Msg.Barrier()

initF=functionPython(4, initialCondition, [XYZ])
solution.interpolate(initF)

rhoHs = dgDofContainer(groups, 1)
rhoHs.interpolate(functionPython(1, rhoHydrostatic, [XYZ]))
rhoThetaHs = dgDofContainer(groups, 1)
rhoThetaHs.interpolate(functionPython(1, rhoThetaHydrostatic, [XYZ]))

claw.setHydrostaticState(rhoHs,rhoThetaHs)
claw.setPhysicalConstants(gamma,Rd,p0,g)

uw=functionPython(2, getVelocity, [solution.getFunction(), XYZ])
uwPert=functionPython(2, getVelocityPert, [solution.getFunction(), XYZ])
rhop=functionPython(1, getRhop, [solution.getFunction()])
pHs=functionPython(1, getpHs, [solution.getFunction(), XYZ])
p=functionPython(1, getp, [solution.getFunction(), XYZ])
pp=functionPython(1, getpp, [solution.getFunction(), XYZ])
exnerp=functionPython(1, getExnerp, [p, pHs])
thetap=functionPython(1, getThetap, [solution.getFunction(), XYZ])
numericalSol=functionPython(4, getNumericalSol, [uwPert, exnerp, thetap]) 


boundaryWall = claw.newBoundaryWall()
#top is actually bottom because of the downward ocean extrusion
claw.addBoundaryCondition('top_domain', boundaryWall)

#We need to use a dof container to restrict the initial condition
# (=boundary condition) to the discretization space
initDof = dgDofContainer(groups, 4)
initDof.interpolate(initF)

spongeTerm=functionC("sourceTerm.so","sponge",4,[XYZ, solution.getFunction(), initDof.getFunction()])
claw.setSource("", spongeTerm);


outsideBoundary = claw.newOutsideValueBoundary("",initDof.getFunction())
claw.addBoundaryCondition('bottom_domain', outsideBoundary)
claw.addBoundaryCondition('left', outsideBoundary)
claw.addBoundaryCondition('right', outsideBoundary)


#claw.setFilterMode(FILTER_LINEARVERTICAL);
#claw.setFilterMode(FILTER_LINEAR);
#petscIm = dgLinearSystemExtrusion(claw, groups, extrusion)

petscIm = linearSystemPETScBlockDouble()
#petscIm.setParameter("petscOptions",  "-ksp_type preonly -pc_type lu -pc_factor_mat_solver_package mumps")
petscIm.setParameter("petscOptions",  "-ksp_atol 0 -ksp_rtol 1e-4")
dofIm = dgDofManager.newDGBlock(groups, claw.getNbFields(), petscIm)
#timeIter = dgIMEXRK(claw, dofIm, 2)     #timeorder
timeIter = dgDIRK(claw, dofIm, 2)     #timeorder
timeIter.getNewton().setAtol(0) #ATol
timeIter.getNewton().setRtol(1) #Rtol
timeIter.getNewton().setVerb(3)     #Verbosity

t=Ti
dt=claw.getMinOfTimeSteps(solution, extrusion)
dt=900
if (Msg.GetCommRank()==0):
    print "Time step:",dt

nbSteps = int(ceil(Tf/dt))

if (Msg.GetCommRank()==0):
  print "Computing analytical solution...", 
octave.addpath("analyticalSol")
xAn, zAn, uAn, wAn, exnerAn, thetaAn = octave.call('pseudo_analytic_solution', 2, False) #2 for Schar mountain case, False for non Plot
uAn=uAn.transpose()
wAn=wAn.transpose()
exnerAn=exnerAn.transpose()
thetaAn=thetaAn.transpose()
soluAn = interpolate.RectBivariateSpline(xAn, zAn, uAn)
solwAn = interpolate.RectBivariateSpline(xAn, zAn, wAn)
solExnerAn = interpolate.RectBivariateSpline(xAn, zAn, exnerAn)
solThetaAn = interpolate.RectBivariateSpline(xAn, zAn, thetaAn)
Msg.Barrier()
if (Msg.GetCommRank()==0):
  print "Done" 

def getAnalyticalSol(FCT,  XYZ, uwPert, exnerPert, thetaPert) :
  for i in range(FCT.size1()):
    z = XYZ.get(i,1)
    if (z<=12000) :
      x = XYZ.get(i,0)+25000
      FCT.set(i,0, soluAn.ev(x, z)[0])
      FCT.set(i,1, solwAn.ev(x, z)[0])
      FCT.set(i,2, solExnerAn.ev(x, z)[0])
      FCT.set(i,3, solThetaAn.ev(x, z)[0])
    else :
      FCT.set(i,0,uwPert(i,0))
      FCT.set(i,1,uwPert(i,1))
      FCT.set(i,2,exnerPert(i,0))
      FCT.set(i,3,thetaPert(i,0))
solAn=functionPython(4, getAnalyticalSol, [XYZ, uwPert, exnerp, thetap])
nCompAn = [2,1,1]
namesAn = ["uw","exner","theta"]

def getAnalyticalSolSq(FCT,  XYZ) :
  for i in range(FCT.size1()):
    z = XYZ.get(i,1)
    if (z<=12000) :
      x = XYZ.get(i,0)+25000
      FCT.set(i,0, soluAn.ev(x, z)[0]**2)
      FCT.set(i,1, solwAn.ev(x, z)[0]**2)
      FCT.set(i,2, solExnerAn.ev(x, z)[0]**2)
      FCT.set(i,3, solThetaAn.ev(x, z)[0]**2)
    else :
      FCT.set(i,0,0)
      FCT.set(i,1,0)
      FCT.set(i,2,0)
      FCT.set(i,3,0)
solAnSq=functionPython(4, getAnalyticalSolSq, [XYZ])

def getError(FCT, XYZ, analytic, numerical) :
  for i in range(FCT.size1()):
    z = XYZ.get(i,1)
    for j in range(FCT.size2()):
      if (z<=12000) :
        FCT.set(i,j,(analytic(i,j)-numerical(i,j))**2)
      else :
        FCT.set(i,j,0)
error = functionPython(4, getError, [XYZ, solAn, numericalSol])

integratorError = dgFunctionIntegrator(groups, error)
intErr = fullMatrixDouble(4,1)
integratorAnalyticSq = dgFunctionIntegrator(groups, solAnSq)
intAnalyticSq = fullMatrixDouble(4,1)

#Export data
def getExp(FCT, uw, rhop, thetap, pp) :
    for i in range (FCT.size1()) :
        FCT.set(i,0,uw(i,0))
        FCT.set(i,1,uw(i,1))
        FCT.set(i,2,rhop(i,0))
        FCT.set(i,3,thetap(i,0))
        FCT.set(i,4,pp(i,0))
Exp=functionPython(5, getExp, [uwPert, rhop, thetap, pp])
nCompExp=[2,1,1,1]
namesExp=["uw","rhop","thetap","pp"]

solution.exportFunctionVtk(solAn,'output/export', 0, 0,"solutionAnalytic",nCompAn,namesAn)

errU=[]
errW=[]
errExner=[]
errTheta=[]
time=[]
n_export=0
if (Msg.GetCommRank()==0):
  plt.ion()
  fig = plt.figure()
  pltU,=plt.plot(time,errU,label="u")
  pltV,=plt.plot(time,errW,label="w")
  pltExner,=plt.plot(time,errExner,label="Exner")
  pltTheta,=plt.plot(time,errTheta,label="Theta")
  plt.legend(loc='upper right')
  axes=plt.gca()
  axes.set_ylim(0,1)
for i in range(0,nbSteps-1):
    #dt=groups.getMinOfTimeSteps(claw,solution)
    if (i==nbSteps-1):
        dt=Tf-t
    if (i%1 == 0):
      integratorError.compute(intErr)
      integratorAnalyticSq.compute(intAnalyticSq)
      errU.append(intErr(0,0)/intAnalyticSq(0,0))
      errW.append(intErr(1,0)/intAnalyticSq(1,0))
      errExner.append(intErr(2,0)/intAnalyticSq(2,0))
      errTheta.append(intErr(3,0)/intAnalyticSq(3,0))
      time.append(t/3600)
      solution.exportFunctionVtk(numericalSol,'output/export', t, i,"solution",nCompAn,namesAn)
      if (Msg.GetCommRank()==0):
        pltU.set_data(time,errU)
        pltV.set_data(time,errW)
        pltExner.set_data(time,errExner)
        pltTheta.set_data(time,errTheta)
        axes.relim()
        axes.autoscale_view(True,True,False)
        plt.draw()
        print '\nWriting output',n_export,'at time',t,'and step',i,'over',nbSteps
        print "Error on u:",errU[-1],"w:", errW[-1],"Exner:",errExner[-1],"Theta:",errTheta[-1]
      n_export=n_export+1
    norm = timeIter.iterate (solution, dt, t)
    t=t+dt
    if (Msg.GetCommRank()==0):
        sys.stdout.write('.')
        sys.stdout.flush()
print ''    
integratorError.compute(intErr)
integratorAnalyticSq.compute(intAnalyticSq)
errU.append(intErr(0,0)/intAnalyticSq(0,0))
errW.append(intErr(1,0)/intAnalyticSq(1,0))
errExner.append(intErr(2,0)/intAnalyticSq(2,0))
errTheta.append(intErr(3,0)/intAnalyticSq(3,0))
time.append(t/3600)
if (Msg.GetCommRank()==0):
  print "Error on u:",errU[-1],"w:", errW[-1],"Exner:",errExner[-1],"Theta:",errTheta[-1]        
  outFile = open('output/ErrorVsTime','w')
  outFile.write("t u w exner theta\n")
  for i in range(len(time)):
    outFile.write("%s %s %s %s %s\n" % (time[i], errU[i], errW[i], errExner[i], errTheta[i]))
  outFile.close()
  pltU.set_data(time,errU)
  pltV.set_data(time,errW)
  pltExner.set_data(time,errExner)
  pltTheta.set_data(time,errTheta)
  axes.relim()
  axes.autoscale_view(True,True,True)
  plt.draw()
  fig.savefig('output/ErrorVsTime.pdf')
  plt.draw()
plt.show()
Msg.Exit(0)
