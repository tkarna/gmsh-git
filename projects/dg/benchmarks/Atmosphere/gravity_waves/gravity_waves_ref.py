from extrude import *
from dgpy import *
from math import *
import time, os, sys
import gmshPartition

#### Physical constants ###
gamma=1.4
Rd=287
p0=1.0e5
g=9.80616
Cv=Rd/(gamma-1)
Cp=Rd*(1+1/(gamma-1))
###########################
#### Begin and end time ###
Ti=0
Tf=30
###########################

order=3
dimension=2



model = GModel()
name='line'
if Msg.GetCommSize()>1:
  partStr='_part_%i' % Msg.GetCommSize()
else:
  partStr=''
model.load(name + partStr + '_2d_XY.msh')
#model.load('gravity_waves.msh')


groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag();
groupsH = dgGroupCollection.newByTag(model, dimension-1, order, ["bottom_domain"])
extrusion = dgExtrusion(groups, groupsH, ["bottom_domain"])
groups.splitFaceGroupsByOrientation(extrusion, ["bottom_domain", "top_domain"]) 

claw = dgEulerAtmLaw(dimension)


solution = dgDofContainer(groups, claw.getNbFields())

XYZ = groups.getFunctionCoordinates()



def hydrostaticState(z) :
    N=1.e-2
    theta0=300
    thetaHs=theta0*exp(N**2*z/g)
    exnerHs=1+ g**2/(Cp*theta0*N**2) * (exp(-N**2*z/g)-1)
    rhoHs=p0/(Rd*thetaHs)*exnerHs**(Cv/Rd)
    return rhoHs,exnerHs,thetaHs

def initialCondition(FCT,  XYZ) :
    a=5000
    dtheta0=1.0e-2
    H=10000
    xc=-50000
    U=20.0
    for i in range (0,XYZ.size1()) :
        x=XYZ(i,0)
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        thetaPert=thetaHs+dtheta0*(sin(pi*z/H)/(1+(x-xc)**2/a**2))
        rhoPert=p0/(Rd*thetaPert)*exnerHs**(Cv/Rd)
        FCT.set(i,0,rhoPert-rhoHs)
        FCT.set(i,1,U*rhoPert)
        FCT.set(i,2,0.0)
        FCT.set(i,3,rhoPert*thetaPert-rhoHs*thetaHs)

def rhoHydrostatic(FCT,  XYZ) :
    for i in range (0,XYZ.size1()) :
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        FCT.set(i,0,rhoHs)

def rhoThetaHydrostatic(FCT,  XYZ) :
    for i in range (0,XYZ.size1()) :
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        FCT.set(i,0,rhoHs*thetaHs)
        
def getVelocity(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        rho=rhoHs+sol(i,0)
        FCT.set(i,0,sol(i,1)/rho)
        FCT.set(i,1,sol(i,2)/rho)
        FCT.set(i,2,0)

def getRhop(FCT, sol) :
    for i in range (0,sol.size1()) :
        FCT.set(i,0,sol(i,0))

def getpp(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        rho=rhoHs+sol(i,0)
        rhoTheta = rhoHs*thetaHs+sol(i,3)
        pHs=p0*(rhoHs*thetaHs*Rd/p0)**gamma
        p=p0*(rhoTheta*Rd/p0)**gamma
        FCT.set(i,0,p-pHs)

def getpHs(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        pHs=p0*(rhoHs*thetaHs*Rd/p0)**gamma
        FCT.set(i,0,pHs)

def getp(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        rhoTheta = rhoHs*thetaHs+sol(i,3)
        p=p0*(rhoTheta*Rd/p0)**gamma
        FCT.set(i,0,p)

def getThetap(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        rho=rhoHs+sol(i,0)
        FCT.set(i,0,(sol(i,3)+(rhoHs*thetaHs))/rho-thetaHs)

uv=functionPython(3, getVelocity, [solution.getFunction(), XYZ])
rhop=functionPython(1, getRhop, [solution.getFunction()])
pHs=functionPython(1, getpHs, [solution.getFunction(), XYZ])
p=functionPython(1, getp, [solution.getFunction(), XYZ])
pp=functionPython(1, getpp, [solution.getFunction(), XYZ])
thetap=functionPython(1, getThetap, [solution.getFunction(), XYZ])

initF=functionPython(4, initialCondition, [XYZ])
solution.interpolate(initF)

rhoHs = dgDofContainer(groups, 1)
rhoHs.interpolate(functionPython(1, rhoHydrostatic, [XYZ]))
rhoThetaHs = dgDofContainer(groups, 1)
rhoThetaHs.interpolate(functionPython(1, rhoThetaHydrostatic, [XYZ]))

claw.setHydrostaticState(rhoHs,rhoThetaHs)
claw.setPhysicalConstants(gamma,Rd,p0,g)

boundaryWall = claw.newBoundaryWall()
claw.addBoundaryCondition('bottom_domain', boundaryWall)
claw.addBoundaryCondition('top_domain', boundaryWall)

#We need to use a dof container to restrict the initial condition
# (=boundary condition) to the discretization space
initDof = dgDofContainer(groups, 4)
initDof.interpolate(initF)
outsideBoundary = claw.newOutsideValueBoundary("",initDof.getFunction())
claw.addBoundaryCondition('left', outsideBoundary)
claw.addBoundaryCondition('right', outsideBoundary)

#zero=claw.new0FluxBoundary()
#claw.addBoundaryCondition('bottom', zero)
#claw.addBoundaryCondition('top', zero)
#claw.addBoundaryCondition('left', zero)
#claw.addBoundaryCondition('right', zero)


timeIter = dgERK(claw, None, DG_ERK_33)

#claw.setFilterMode(FILTER_LINEAR);
#petscIm = dgLinearSystemExtrusion(claw, groups, extrusion)

#petscIm = linearSystemPETScBlockDouble()
#petscIm.setParameter("petscOptions",  "-ksp_type preonly -pc_type lu -pc_factor_mat_solver_package mumps")
#petscIm.setParameter("petscOptions",  "-ksp_atol 0 -ksp_rtol 1e-20")

#dofIm = dgDofManager.newDGBlock(groups, claw.getNbFields(), petscIm)

#timeIter = dgIMEXDIMSIM(claw, dofIm, 2)     #timeorder
#timeIter = dgIMEXTSRK(claw, dofIm, 6)     #timeorder
#timeIter = dgIMEXRK(claw, dofIm, 2)     #timeorder
#timeIter = dgDIRK(claw, dofIm, 2)     #timeorder

#timeIter.getNewton().setAtol(1.e-5) #ATol
#timeIter.getNewton().setRtol(1.e-8) #Rtol
#timeIter.getNewton().setVerb(3)     #Verbosity


dt=claw.getMinOfTimeSteps(solution, extrusion)*.8
dt = 1/200.0


if (Msg.GetCommRank()==0):
    print "Time step:",dt

nbSteps = int(round(Tf/dt))

#Export data
def getExp(FCT, uv, rhop, thetap, pp) :
    for i in range (0,uv.size1()) :
        FCT.set(i,0,uv(i,0))
        FCT.set(i,1,uv(i,1))
        FCT.set(i,2,rhop(i,0))
        FCT.set(i,3,thetap(i,0))
        FCT.set(i,4,pp(i,0))
Exp=functionPython(5, getExp, [uv, rhop, thetap, pp])
nCompExp=[2,1,1,1]
namesExp=["uv","rhop","thetap","pp"]

#sBV = 2.0/3*(order+1.0)
#muBV = 0.2
#filterBV = dgFilterBoydVandeven(groups, "", sBV, muBV)


t=Ti
# starter for IMEX TSRK
#t=timeIter.starter(solution,dt,t,3)
# starter for IMEX DIMSIM
#timeIter.starter(solution,dt,dt,t,3)
n_export=0
timeStart=time.clock();
for i in range(0,nbSteps):
    if (i==nbSteps-1):
        dt=Tf-t
    if (i%(100*10) == 0):
        solution.exportFunctionVtk(Exp,'output/export', t*1000, i,"solution",nCompExp,namesExp)
        if (Msg.GetCommRank()==0):
            print '\nWriting output',n_export,'at time',t,'and step',i,'over',nbSteps
            print 'Time elapsed: ',time.clock()-timeStart
        n_export=n_export+1
    norm = timeIter.iterate (solution, dt, t)
    #filterBV.apply(solution)
    t=t+dt
    if (Msg.GetCommRank()==0):
        sys.stdout.write('.')
        sys.stdout.flush()
solution.exportFunctionVtk(Exp,'output/export', Tf*1000, nbSteps,"solution",nCompExp,namesExp)

Msg.Barrier()             
solutionExporter = dgIdxExporter(solution,  "refSolERK")                                                                                                  
Msg.Barrier()                                                                                                                                              
solutionExporter.exportIdx()                                                                                                                               
Msg.Barrier() 

print ''    
print 'Time elapsed: ',time.clock()-timeStart
Msg.Exit(0)
