from dgpy import *
from math import *

havePert=0
order=3

## Angle regards to the equator (rotated test to check the seam)
alpha = 0#35*pi/180

#### Physical constants ###
gamma=1.4
Rd=287.04
g=9.80616
Cv=Rd/(gamma-1.0)
Cp=Rd*(1.0+1.0/(gamma-1.0))
kappa=Rd/Cp
R = 6371229
###########################

#Constants for the initial condition
eta0 = 0.252
etaS = 1
etaT = 0.2
p0=1.0e5
T0 = 288
deltaT = 4.8e5
Tau = 0.005
u0 = 35
Omega = 7.29212e-5


CCodeInitial="""
#include "math.h"
#include "GmshMessage.h"
#include "function.h"
#include "dgGroupOfElements.h"
#include "dgSpaceTransform.h"
#include <cstdlib>

#define alpha %.15e
#define gamma %.15e
#define Rd %.15e
#define g %.15e
#define Cv %.15e
#define Cp %.15e
#define kappa %.15e
#define R %.15e
#define eta0 %.15e
#define etaS %.15e
#define etaT %.15e
#define p0 %.15e
#define T0 %.15e
#define deltaT %.15e
#define Tau %.15e
#define u0 %.15e
#define Omega %.15e
#define havePert %.15e

extern "C" {
double PhiInit(double lambd, double phi, double eta, bool Hs);
void initialCondition(dataCacheMap *cacheMap, fullMatrix<double> &FCT, fullMatrix<double> &stereo);
void rhoHydrostatic(dataCacheMap *cacheMap, fullMatrix<double> &FCT, fullMatrix<double> &stereo);
void rhoThetaHydrostatic(dataCacheMap *cacheMap, fullMatrix<double> &FCT, fullMatrix<double> &stereo);
void fCoriolis(dataCacheMap *cacheMap, fullMatrix<double> &FCT, fullMatrix<double> &stereo);
}

double PhiInit(double lambd, double phi, double eta, bool Hs){
  double etaV = (eta - eta0) * M_PI / 2;
  double phiBar;
  if (eta >= etaT){
    phiBar = T0 * g / Tau * (1 - pow(eta,(Rd * Tau / g)));
  }else{
    phiBar = T0 * g / Tau * (1 - pow(eta,(Rd * Tau / g))) - Rd * deltaT
        * ((log(eta/etaT) + 137.0/60) * etaT*etaT*etaT*etaT*etaT - 5 * etaT*etaT*etaT*etaT * eta + 5 * etaT*etaT*etaT * eta*eta - 10.0/3 * etaT*etaT * eta*eta*eta + 5.0/4 * etaT * eta*eta*eta*eta - 1.0/5 * eta*eta*eta*eta*eta);
  }
  if (Hs) return phiBar;
  double sinphi = sin(phi);
  double cosphi = cos(phi);
  return phiBar + u0 * pow(cos(etaV),1.5)
      * ( (-2 * sinphi*sinphi*sinphi*sinphi*sinphi*sinphi * (cosphi*cosphi + 1.0/3) + 10.0/63) * u0 * pow(cos(etaV),1.5)
            + (8.0/5 * cosphi*cosphi*cosphi * (sinphi*sinphi + 2.0/3) - M_PI/4) * R * Omega );
}

double TInit(double lambd, double phi, double eta, bool Hs){
  double etaV = (eta - eta0) * M_PI / 2;
  double Tbar;
  if (eta >= etaT){
    Tbar = T0 * pow(eta, (Rd * Tau / g));
  }else{
    Tbar = T0 * pow(eta, (Rd * Tau / g)) + deltaT * (etaT - eta)*(etaT - eta)*(etaT - eta)*(etaT - eta)*(etaT - eta);
  }
  double sinphi = sin(phi);
  double cosphi = cos(phi);
  if (Hs) return Tbar;
  return Tbar + 3.0/4 * eta * M_PI * u0 / Rd * sin(etaV) * sqrt(cos(etaV)) 
      * ( (-2 * sinphi*sinphi*sinphi*sinphi*sinphi*sinphi * (cosphi*cosphi + 1.0/3) + 10.0/63) * 2 * u0 * pow(cos(etaV),1.5) 
            + (8.0/5 * cosphi*cosphi*cosphi * (sinphi*sinphi + 2.0/3) - M_PI/4 ) * R * Omega );
}

double getEta(double lambd, double phi, double z, bool Hs){
  //Compute with iterations to find pressure-based coordinates
  double eta_old = 1e99;
  double eta = 1e-7;
  int nbIter = 0;
  while (fabs(eta - eta_old) > 1e-14){
    if (nbIter > 100){
      Msg::Fatal("z to eta coordinates conversion not converged after 100 iterations");
    }
    eta_old = eta;
    double T = TInit(lambd, phi, eta, Hs);
    double Phi = PhiInit(lambd, phi, eta, Hs);
    eta = eta - (- g * z + Phi) / (- Rd / eta * T);
    nbIter = nbIter + 1;
  }
  if (eta < 0){
    Msg::Fatal("Unphysical value for eta: %%e \\n(lambda=%%e phi=%%e z=%%e gz=%%e)\\n",eta, lambd, phi, z, g*z);
  }
  return eta;
}

void initialCondition(dataCacheMap *cacheMap, fullMatrix<double> &FCT, fullMatrix<double> &stereo) {
   dgSpaceTransformSpherical tr(R);
   for (int i=0; i<FCT.size1(); i++){
      //Stereo to cartesian
      double x, y, z;
      tr.stereo2Cart(stereo(i,0),stereo(i,1),stereo(i,2), cacheMap, x, y, z);
      //Rotate cartesian
      double  xRot, yRot, zRot;
      tr.rotateCartX(x, y, z, alpha, xRot, yRot, zRot);
      //Cartesian to lonlat
      double lambd, phi, r;
      tr.cart2LonLat(xRot, yRot, zRot, lambd, phi, r);
      //---------- FULL NONHYDROSTATIC FIELDS ---------
      //Pressure-based coordinates
      double eta = getEta(lambd, phi, r, false);
      //Compute initial condition
      double T = TInit(lambd, phi, eta, false);
      double p = eta * p0;
      double theta = T * pow(p0 / p, Rd / Cp);
      double rho = pow(p / p0, 1.0 / gamma) * p0 / (theta * Rd);
      //----------- HYDROSTATIC STATE (u=0) -----------
      double etaHs = getEta(lambd, phi, r, true);
      double THs = TInit(lambd, phi, etaHs, true);
      double pHs = etaHs * p0;
      double thetaHs = THs * pow(p0 / pHs, Rd / Cp);
      double rhoHs = pow(pHs / p0, 1.0 / gamma) * p0 / (thetaHs * Rd);
      //-----------------------------------------------
      //Velocity
      double etaV = (eta - eta0) * M_PI / 2;
      double u_lambda = u0 * pow(cos(etaV), 1.5) * sin(2*phi)*sin(2*phi);
      double u_phi = 0;
      double u_r = 0;
      //Perturbation
      double lambdac = M_PI / 9.;
      double phic = 2. * M_PI / 9.;
      double up = 1.;
      double Rp = R / 10.;
      double rp = R * acos(sin(phic) * sin(phi) + cos(phic) * cos(phi) * cos(lambd - lambdac));
      double upert = up * exp(-(rp/Rp)*(rp/Rp));
      u_lambda += havePert * upert;
      double ux, uy, uz;
      tr.lonLatToCartVec(lambd, phi, r, u_lambda, u_phi, u_r, ux, uy, uz);
      double uxRot, uyRot, uzRot;
      tr.rotateCartXVec(x, y, z, -alpha, ux, uy, uz, uxRot, uyRot, uzRot);
      double uSt, vSt, wSt;
      tr.cartToStereoVec(stereo(i,0), stereo(i,1), stereo(i,2), uxRot, uyRot, uzRot, cacheMap, uSt, vSt, wSt);


      //Value for eta=1 (bottom)
      double etaB = 1;
      double pB = etaB * p0;
      double phiB = PhiInit(lambd, phi, etaB, false);
      double zB = phiB/g;
      double etaHsB = getEta(lambd, phi, zB, true);
      double pHsB = etaHsB * p0;

      FCT.set(i,0,rho-rhoHs);// -((p-pHs)/g - (pB-pHsB)/g)
      FCT.set(i,1,uSt*rho);
      FCT.set(i,2,vSt*rho);
      FCT.set(i,3,wSt*rho);
      FCT.set(i,4,rho*theta-rhoHs*thetaHs);
   }
}

void rhoHydrostatic(dataCacheMap *cacheMap, fullMatrix<double> &FCT, fullMatrix<double> &stereo) {
   dgSpaceTransformSpherical tr(R);
    for (int i = 0; i < stereo.size1(); i++) {
      //Stereo to cartesian
      double x, y, z;
      tr.stereo2Cart(stereo(i,0),stereo(i,1),stereo(i,2),cacheMap, x, y, z);
      //Rotate cartesian
      double  xRot, yRot, zRot;
      tr.rotateCartX(x, y, z, alpha, xRot, yRot, zRot);
      //Cartesian to lonlat
      double lambd, phi, r;
      tr.cart2LonLat(xRot, yRot, zRot, lambd, phi, r);
      double etaHs = getEta(lambd, phi, stereo(i,2), true);
      double pHs = etaHs * p0;
      double THs = TInit(lambd, phi, etaHs, true);
      double thetaHs = THs * pow(p0 / pHs, Rd / Cp);
      double rhoHs = pow(pHs / p0 , 1.0 / gamma) * p0 / (thetaHs * Rd);
      FCT.set(i,0,rhoHs);
   }
}

void rhoThetaHydrostatic(dataCacheMap *cacheMap, fullMatrix<double> &FCT, fullMatrix<double> &stereo) {
   dgSpaceTransformSpherical tr(R);
    for (int i = 0; i < stereo.size1(); i++) {
      //Stereo to cartesian
      double x, y, z;
      tr.stereo2Cart(stereo(i,0),stereo(i,1),stereo(i,2),cacheMap, x, y, z);
      //Rotate cartesian
      double  xRot, yRot, zRot;
      tr.rotateCartX(x, y, z, alpha, xRot, yRot, zRot);
      //Cartesian to lonlat
      double lambd, phi, r;
      tr.cart2LonLat(xRot, yRot, zRot, lambd, phi, r);
      double etaHs = getEta(lambd, phi, stereo(i,2), true);
      double pHs = etaHs * p0;
      double THs = TInit(lambd, phi, etaHs, true);
      double thetaHs = THs * pow(p0 / pHs, Rd / Cp);
      double rhoHs = pow(pHs / p0, 1.0/gamma) * p0 / (thetaHs * Rd);
      FCT.set(i,0,rhoHs*thetaHs);
   }
}

void fCoriolis(dataCacheMap *cacheMap, fullMatrix<double> &FCT, fullMatrix<double> &stereo) {
   dgSpaceTransformSpherical tr(R);
    for (int i = 0; i < stereo.size1(); i++) {
      //Stereo to cartesian
      double x, y, z;
      tr.stereo2Cart(stereo(i,0),stereo(i,1),stereo(i,2),cacheMap, x, y, z);
      //Rotate cartesian
      double  xRot, yRot, zRot;
      tr.rotateCartX(x, y, z, alpha, xRot, yRot, zRot);
      //Cartesian to lonlat
      double lambd, phi, r;
      tr.cart2LonLat(xRot, yRot, zRot, lambd, phi, r);
      FCT.set(i, 0, 2 * Omega * sin(phi));
  }
}

""" % (alpha, gamma, Rd, g, Cv, Cp, kappa, R, eta0, etaS, etaT, p0, T0, deltaT, Tau, u0, Omega, havePert)



if (Msg.GetCommRank()==0):
    functionC.buildLibrary (CCodeInitial, "./ccodeInitial.so",True);
Msg.Barrier()



