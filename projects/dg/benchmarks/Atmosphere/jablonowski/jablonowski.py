from dgpy import *
from initialCondition import *
import time, os, sys
import gmshPartition

nbStart=40000
reloadSuffix=''
exportBaseDir = '/glade/scratch/sblaise'
if (havePert > 0.5):
    expDirName = 'jablonowski'
else:
    expDirName = 'steadystate'+str(nbStart)
    loadDirName = 'steadystate'+reloadSuffix

CCode = """
#include "fullMatrix.h"
#include "function.h"
#include "dgGroupOfElements.h"
#include "dgSpaceTransform.h"
extern "C" {

void sponge (dataCacheMap *, fullMatrix<double> &FCT, fullMatrix<double> &XYZ, fullMatrix<double> &sol, fullMatrix<double> &reference) {
    double g=9.80616;
    for (int i=0; i<XYZ.size1(); i++){
        double z=XYZ(i,2);
        double zb=10000;
        double zt=17000;
        double alpha=0;//0.015;
        double tau = 0;
        if (z>=zb)
           tau=-alpha/2*(1+cos(((z-zb)/(zt-zb)-1)*M_PI));
        double Bzb=0;
        double Bzt=3000;
        double Balpha=0;//.015;
        if (z<=Bzt)
           tau=-Balpha/2*(1+cos(((Bzb-z)/(Bzb-Bzt)-1)*M_PI));
        FCT.set(i,0,tau*(sol(i,0)-reference(i,0)));
        FCT.set(i,1,tau*(sol(i,1)-reference(i,1)));
        FCT.set(i,2,tau*(sol(i,2)-reference(i,2)));
        FCT.set(i,3,tau*(sol(i,3)-reference(i,3)));
        FCT.set(i,4,tau*(sol(i,4)-reference(i,4)));
   }
}
void sensor0 (dataCacheMap *, fullMatrix<double> &FCT, fullMatrix<double> &sol) {
   for (int i=0; i<FCT.size1(); i++){
       FCT.set(i, 0, sol(i,0));
   }
}
void sensor12 (dataCacheMap *, fullMatrix<double> &FCT, fullMatrix<double> &sol) {
   for (int i=0; i<FCT.size1(); i++){
       FCT.set(i, 0, sqrt(sol(i,1)*sol(i,1)+sol(i,2)*sol(i,2)));
   }
}

void sensor3 (dataCacheMap *, fullMatrix<double> &FCT, fullMatrix<double> &sol) {
   for (int i=0; i<FCT.size1(); i++){
       FCT.set(i, 0, sol(i,3));
   }
}
void sensor4 (dataCacheMap *, fullMatrix<double> &FCT, fullMatrix<double> &sol) {
   for (int i=0; i<FCT.size1(); i++){
       FCT.set(i, 0, sol(i,4));
   }
}

void vecFromScal(dataCacheMap *, fullMatrix<double> &FCT, fullMatrix<double> &s0,fullMatrix<double> &s1,fullMatrix<double> &s2,fullMatrix<double> &s3,fullMatrix<double> &s4){
   for (int i = 0; i < FCT.size1(); i++){
        FCT.set(i,0,s0(i,0));
        FCT.set(i,1,s1(i,0));
        FCT.set(i,2,s2(i,0));
        FCT.set(i,3,s3(i,0));
        FCT.set(i,4,s4(i,0));
   }
}


void getError(dataCacheMap *, fullMatrix<double> &FCT, fullMatrix<double> &analytic, fullMatrix<double> &numerical) {
   for (int i=0; i<FCT.size1(); i++){
      for (int j=0; j<FCT.size2(); j++){
         FCT.set(i,j,(analytic(i,j)-numerical(i,j))*(analytic(i,j)-numerical(i,j)));
      }
   }
}
void getSquare(dataCacheMap *, fullMatrix<double> &FCT, fullMatrix<double> &inFct) {
   for (int i=0; i<FCT.size1(); i++){
      for (int j=0; j<FCT.size2(); j++){
         FCT.set(i,j,(inFct(i,j)*inFct(i,j)));
      }
   }
}
void velocity3D(dataCacheMap *cacheMap, fullMatrix<double> &F, fullMatrix<double> &sol, fullMatrix<double> &stereo){
    double R = 6371229;
    double rev = 1;
    for (int i = 0; i < sol.size1(); i++){
        if (cacheMap->getGroupOfElements()->getPhysicalTag() == "volume_Bottom")
            rev = -1;
        //Horizontal components
        double xi =stereo(i,0);
        double beta =stereo(i,1);
        double gam = stereo(i,2);
        double vxi = sol(i,1);
        double vbeta = sol(i,2);
        double vgamma = sol(i,3);
        double ux = (1-(2*xi*xi)/(4*R*R+xi*xi + beta*beta)) * vxi - 2*beta*xi/(4*R*R+xi*xi + beta*beta) * vbeta + 4*R*xi/(4*R*R+xi*xi + beta*beta) * vgamma;
        double uy = - 2*beta*xi/(4*R*R+xi*xi + beta*beta) * vxi + (1-(2*beta*beta)/(4*R*R+xi*xi + beta*beta)) * vbeta + 4*R*beta/(4*R*R+xi*xi + beta*beta) * vgamma;
        double uz = -4*R*xi*rev/(4*R*R+xi*xi + beta*beta) * vxi - 4*R*beta*rev/(4*R*R+xi*xi + beta*beta) * vbeta - rev*((-4*R*R+xi*xi+beta*beta)/(4*R*R+xi*xi + beta*beta)) * vgamma;
        //Both
        F.set(i,0,ux);
        F.set(i,1,uy);
        F.set(i,2,uz);
   }
}
void expCoord(dataCacheMap *cacheMap, fullMatrix<double> &FCT, fullMatrix<double> &stereo, fullMatrix<double> &sol, fullMatrix<double> &solInit, fullMatrix<double> &rhoHs, fullMatrix<double> &rhoThetaHs, fullMatrix<double> &amplFactor) {
   double gamma=1.4;
   double Rd=287.04;
   double p0=1.0e5;
   double R=6371229;
   dgSpaceTransformSpherical tr(R);
   for (int i=0; i<FCT.size1(); i++){
      double x, y, z;
      double eta = pow((solInit(i,4)+rhoThetaHs(i,0))*Rd/p0, gamma);
      tr.stereo2Cart(stereo(i,0), stereo(i,1), stereo(i,2)*amplFactor(i,0), cacheMap, x, y, z);
      FCT.set(i,0,x);
      FCT.set(i,1,y);
      FCT.set(i,2,z);
   }
}

void getp(dataCacheMap *cacheMap, fullMatrix<double> &FCT, fullMatrix<double> &sol, fullMatrix<double> &rhoThetaHs) {
   double gamma=1.4;
   double Rd=287.04;
   double p0=1.0e5;
   for (int i=0; i<FCT.size1(); i++){
      FCT.set(i,0,p0*(pow((rhoThetaHs(i,0)+sol(i,4))*Rd/p0,gamma)));
   }
}

void vorticity(dataCacheMap *cacheMap, fullMatrix<double> &F, fullMatrix<double> &stereo, fullMatrix<double> &sol, fullMatrix<double> &solGradient, fullMatrix<double> &rhoHs){
    double R=6371229;
    for (int i = 0; i < sol.size1(); i++){
        double xi = stereo(i,0);
        double eta = stereo(i,1);
        double gamm = stereo(i,2);
        double J = 4*R*(R+gamm)/(4*R*R+xi*xi+eta*eta);
        //Horizontal components
        double rho = rhoHs(i,0) + sol(i,0);
        double U = sol(i,1)/rho;
        double V = sol(i,2)/rho;
        double W = sol(i,3)/rho;
        double dRhodx = solGradient(i,0);
        double dRhody = solGradient(i,1);
        double dRhodz = solGradient(i,2);
        double dRhoUdy = solGradient(i,4);
        double dRhoUdz = solGradient(i,5);
        double dRhoVdx = solGradient(i,6);
        double dRhoVdz = solGradient(i,8);
        double dRhoWdx = solGradient(i,9);
        double dRhoWdy = solGradient(i,10);
        double dUdy = (dRhoUdy - U * dRhody) / rho;
        double dUdz = (dRhoUdz - U * dRhodz) / rho;
        double dVdx = (dRhoVdx - V * dRhodx) / rho;
        double dVdz = (dRhoVdz - V * dRhodz) / rho;
        double dWdx = (dRhoWdx - W * dRhodx) / rho;
        double dWdy = (dRhoWdy - W * dRhody) / rho;
        double vort1 = -(J*dVdz - dWdy + (J*V)/(R + gamm))/J;
        double vort2 = (J*dUdz - dWdx + (J*U)/(R + gamm))/J;
        double vort3 = dVdx/J - dUdy/J - (xi*V)/(2*R*(R + gamm)) + (eta*U)/(2*R*(R + gamm));
        F.set(i,0,vort1);
        F.set(i,1,vort2);
        F.set(i,2,vort3);
   }
}

}
"""


if (Msg.GetCommRank()==0):
    functionC.buildLibrary (CCode, "./ccode.so",True);
Msg.Barrier()

#### Begin and end time ###
Ti=0.0
Tf=30*24*3600
###########################


dimension=3

integOrder=2*order+1+2
dataCacheMap.setDefaultIntegrationOrder(integOrder)

model = GModel()

name='aquaplanet'
if Msg.GetCommSize()>1:
  partStr='_part_%i' % Msg.GetCommSize()
else:
  partStr=''
model.load(name + partStr + '_3d.msh')


groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag();
circleTransform = dgCircleTransform(groups)

st = dgSpaceTransformSpherical(R, groups)
#st = dgSpaceTransformSpherical(R)

groups._mesh.setSpaceTransform(st, groups)
groupsH = dgGroupCollection.newByTag(model, dimension-1, order, ["bottom_Top","bottom_Bottom"])
#To compute the surface pressure. Du to the ocean-base extrusion, top is actually bottom
groupsSurface = dgGroupCollection.newByTag(model, dimension-1, order, ["top_Top","top_Bottom"])
extrusion = dgExtrusion(groups, groupsH, ["bottom_Top","bottom_Bottom"])
groups.splitFaceGroupsByOrientation(extrusion, ["bottom_Top","bottom_Bottom","top_Top", "top_Bottom"]) 

XYZ = groups.getFunctionCoordinates();
xyzCircle = transformToCircle(circleTransform)

xyzCircleInteg = functionPrecomputed(groups, integOrder, 3)
xyzCircleInteg.compute(xyzCircle)


fCor_slow=functionC("./ccodeInitial.so","fCoriolis",1,[xyzCircle])
fCor = functionPrecomputed(groups, integOrder, 1)
fCor.compute(fCor_slow)


claw = dgEulerAtmLaw(dimension)
claw.setFilterMode(FILTER_LINEARVERTICAL);
#claw.setFilterMode(FILTER_LINEAR);
claw.setCoriolisFactor(fCor)


solution = dgDofContainer(groups, claw.getNbFields())
        


amplFact = functionConstant(500)


cartVel=functionC("./ccode.so","velocity3D",3,[solution.getFunction(), xyzCircle])

initF=functionC("./ccodeInitial.so","initialCondition",claw.getNbFields(),[xyzCircle])

claw.setCoordinatesFunction(xyzCircleInteg)

#solution.L2Projection(initF)
solution.interpolate(initF)

rhoHsFct = functionPrecomputed(groups, integOrder, 1)
rhoHsFct.compute(functionC("./ccodeInitial.so","rhoHydrostatic",1,[xyzCircle]))

rhoThetaHsFct = functionPrecomputed(groups, integOrder, 1)
rhoThetaHsFct.compute(functionC("./ccodeInitial.so","rhoThetaHydrostatic",1,[xyzCircle]))

rhoHs = dgDofContainer(groups, 1)
rhoHs.interpolate(functionC("./ccodeInitial.so","rhoHydrostatic",1,[xyzCircle]))
rhoThetaHs = dgDofContainer(groups, 1)
rhoThetaHs.interpolate(functionC("./ccodeInitial.so","rhoThetaHydrostatic",1,[xyzCircle]))


#claw.setHydrostaticState(rhoHsFct, rhoThetaHsFct)#rhoHs.getFunction(),rhoThetaHs.getFunction())
claw.setHydrostaticState(rhoHs,rhoThetaHs)
claw.setPhysicalConstants(gamma,Rd,p0,g,R)

trms = claw.getActiveTerms();
trms.setAdvV(True);
trms.setAdvT(True);
trms.setPGrad(True);
trms.setVDiv(True);
trms.setCor(True);
trms.setGrav(True);
trms.setRhoPot(False);
trms.setDiff(False);
trms.setLax(True);




initDof = dgDofContainer(groups, claw.getNbFields())
#initDof.L2Projection(initF)
initDof.interpolate(initF)


toReplace = [function.getSolution(), function.getSolutionGradient()]
replaceBy = [initDof.getFunction(), initDof.getFunctionGradient()]
outsideBoundary = claw.newOutsideValueBoundaryGeneric("",toReplace,replaceBy)

boundaryWall = claw.newBoundaryWall()
#BOTTOM BND
claw.addBoundaryCondition('top_Top', boundaryWall)#
claw.addBoundaryCondition('top_Bottom', boundaryWall)#

#This is actually the top boundary (extrusion made for ocean from top to bottom)
#outsideBoundary = claw.newOutsideValueBoundary("",initDof.getFunction())
claw.addBoundaryCondition('bottom_Top', outsideBoundary)
claw.addBoundaryCondition('bottom_Bottom', outsideBoundary)

#claw.addBoundaryCondition('bottom_Top', boundaryWall)
#claw.addBoundaryCondition('bottom_Bottom', boundaryWall)



spongeTerm=functionC("./ccode.so","sponge",claw.getNbFields(),[xyzCircleInteg, function.getSolution(), initDof.getFunction()])
claw.setSource("", spongeTerm);




c = sqrt(gamma * p0 / 1)


sensorF0=functionC("./ccode.so","sensor0",1,[solution.getFunction()])
sensorF12=functionC("./ccode.so","sensor12",1,[solution.getFunction()])
sensorF3=functionC("./ccode.so","sensor3",1,[solution.getFunction()])
sensorF4=functionC("./ccode.so","sensor4",1,[solution.getFunction()])

#nuStab = dgJumpDiffusion(groups, 4, 2 * order + 1, c, 10, initDof)
#nu = nuStab.diffusivity().getFunction()

nuStab0 = dgArtificialDiffusion(groups, sensorF0, integOrder, 0.03, 1000);
nuStab12 = dgArtificialDiffusion(groups, sensorF12, integOrder, 0.05, 1000);
nuStab3 = dgArtificialDiffusion(groups, sensorF3, integOrder, 0.05, 1000);
nuStab4 = dgArtificialDiffusion(groups, sensorF4, integOrder, 0.05, 1000);

nuStab0.apply(solution)
nuStab12.apply(solution)
nuStab3.apply(solution)
nuStab4.apply(solution)


#nuh=functionC("./ccode.so","vecFromScal", 5, [nuStab0.nuh().getFunction(),nuStab12.nuh().getFunction(),nuStab12.nuh().getFunction(),nuStab3.nuh().getFunction(),nuStab4.nuh().getFunction()])
#nuv=functionC("./ccode.so","vecFromScal", 5, [nuStab0.nuv().getFunction(),nuStab12.nuv().getFunction(),nuStab12.nuv().getFunction(),nuStab3.nuv().getFunction(),nuStab4.nuv().getFunction()])

#nuh = nuStab0.nuh().getFunction()
#nuv = nuStab0.nuv().getFunction()

nuh = functionConstant(0)
nuv = functionConstant(0)

#nuhGrad = functionConstant([0, 0, 0,0, 0, 0,0, 0, 0,0, 0, 0,0, 0, 0])
#nuvGrad = functionConstant([0, 0, 0,0, 0, 0,0, 0, 0,0, 0, 0,0, 0, 0])

nuhGrad = functionConstant([0, 0, 0])
nuvGrad = functionConstant([0, 0, 0])


zero = functionConstant([0])

claw.setNu(nuh, nuv, nuhGrad, nuvGrad)

#claw.setNu(nuhF, nuvF, nuhGrad, nuvGrad)


#claw.addBoundaryCondition('Seam', boundaryWall)
vorticity=functionC("./ccode.so","vorticity",3,[xyzCircle, solution.getFunction(), solution.getFunctionGradient(),rhoHs.getFunction()])

error=functionC("./ccode.so","getError",5,[initF, solution.getFunction()])
analyticSq = functionC("./ccode.so","getSquare",5,[initF])
integratorError = dgFunctionIntegrator(groups, error)
intErr = fullMatrixDouble(5,1)
integratorAnalyticSq = dgFunctionIntegrator(groups, analyticSq)
intAnalyticSq = fullMatrixDouble(5,1)

pres = functionC("./ccode.so","getp",1,[solution.getFunction(), rhoThetaHs.getFunction()]) 
analyticPres = functionC("./ccode.so","getp",1,[initF, rhoThetaHs.getFunction()])
errorPres=functionC("./ccode.so","getError",1,[analyticPres, pres])
analyticSqPres = functionC("./ccode.so","getSquare",1,[analyticPres])
integratorErrorPres = dgFunctionIntegrator(groupsSurface, errorPres)
intErrPres = fullMatrixDouble(1,1)
integratorAnalyticSqPres = dgFunctionIntegrator(groupsSurface, analyticSqPres)
intAnalyticSqPres = fullMatrixDouble(1,1)



def exportFct(FCT,gradSol,stereo, rhoHs,rhoThetaHs, sol, faccor,vort):
    for i in range(0,FCT.size1()):
        #rhop
        FCT.set(i,0,sol(i,0))
        #rhouvw
        FCT.set(i,1,sol(i,1))
        FCT.set(i,2,sol(i,2))
        FCT.set(i,3,sol(i,3))
        FCT.set(i,4,sol(i,3))
        #rhothetap
        FCT.set(i,5,sol(i,4))
        FCT.set(i,6,rhoHs(i,0))
        FCT.set(i,7,rhoThetaHs(i,0))
        FCT.set(i,8,vort(i,0))
        FCT.set(i,9,vort(i,1))
        FCT.set(i,10,vort(i,2))


nCompExp=[1,3,1,1,1,1,3]
namesExp=["rhoPot","rhoUVW","rhoW","rhoThetaP","rhoHs","rhoThetaHs","vorticity_st"]
exportFunction=functionPython(sum(nCompExp), exportFct, [solution.getFunctionGradient(),xyzCircle, rhoHs.getFunction(),rhoThetaHs.getFunction(), solution.getFunction(), fCor_slow, vorticity])#rhoThetaHs.getFunction()])
solutionExporter = dgIdxExporter(solution,  exportBaseDir + '/' + expDirName + '/solution')
expCoord=functionC("./ccode.so","expCoord",3,[xyzCircle, solution.getFunction(), initDof.getFunction(), rhoHs.getFunction(), rhoThetaHs.getFunction(), amplFact])

#timeIter = dgERK(claw, None, DG_ERK_22)
petscIm = dgLinearSystemExtrusion(claw, groups, extrusion)
##
#petscIm = linearSystemPETScBlockDouble()
##
#petscIm.setParameter("petscOptions","-ksp_type preonly -pc_type lu -pc_factor_mat_solver_package mumps")
petscIm.setParameter("petscOptions",  "-ksp_atol 0 -ksp_rtol 1e-5")
##
dofIm = dgDofManager.newDGBlock(groups, claw.getNbFields(), petscIm)
#timeIter = dgDIRK(claw, dofIm, 1)
timeIter = dgIMEXRK(claw, dofIm, 2)     #timeorder
timeIter.getNewton().setAtol(1.e-14) #ATol
timeIter.getNewton().setRtol(1.e-4) #Rtol
#timeIter.getNewton().setVerb(1)     #Verbosity

#sBV = 2.0/3*(order+1.0)
#muBV = 0.2
#filterBV = dgFilterBoydVandeven(groups, "", sBV, muBV)

#dt=claw.getMinOfTimeSteps(solution)
dt = min(claw.getMinOfTimeSteps(solution, extrusion),2000)
#dt = 7200
if (Msg.GetCommRank()==0):
    print ("Time step:",dt)
nbSteps = int(ceil(Tf/dt))



#initial solution
#claw.setImexMode(IMEX_IMPLICIT);
t=Ti

#In order to compute Pp


n_export=0
start = time.clock()
if (havePert < 0.5):
  outFile = open(exportBaseDir + '/' + expDirName + '/ErrorVsTime','w')
#outFile.write("t rhop rhoU rhoV rhoThetap surfPres\n")
  outFile.close()

for i in range(0,nbSteps):
  if (i < nbStart):
      t=t+dt
      continue
  if (i == nbStart and i > 0):
      loadFile=exportBaseDir + '/' + loadDirName + '/solution/solution-0'+str(nbStart)+'.idx'
      print ('Reloading solution ',loadFile)
      solution.readMsh(loadFile) 
  if (i%10000==0 and i > nbStart):
      print ('Writing solution for reload')
      solutionExporter.exportIdx(i, t)                                                                                                       
  if (i%200 == 0 or i==nbSteps-1):
    if (havePert < 0.5):
      integratorError.compute(intErr)
      integratorAnalyticSq.compute(intAnalyticSq)
      integratorErrorPres.compute(intErrPres)
      integratorAnalyticSqPres.compute(intAnalyticSqPres)
    solution.exportFunctionVtk(exportFunction,exportBaseDir + '/' + expDirName + '/output', t, i,"solution",nCompExp,namesExp,expCoord)
    if (Msg.GetCommRank()==0):
      print ('\nWriting output',n_export,'at time',t,'and step',i,'over',nbSteps)
      if (havePert < 0.5):
        outFile = open(exportBaseDir + '/' + expDirName + '/ErrorVsTime','a')
        errRhop=intErr(0,0)/intAnalyticSq(0,0)
        errRhoU=intErr(1,0)/intAnalyticSq(1,0)
        errRhoV=intErr(2,0)/intAnalyticSq(2,0)
        errRhoThetap=intErr(4,0)/intAnalyticSq(4,0)
        errSurfPres=intErrPres(0,0)/intAnalyticSqPres(0,0)
        print ("Error on rhop:",errRhop,"rhoU:", errRhoU,"rhoV:",errRhoV,"RhoThetap:",errRhoThetap,"SurfPres:",errSurfPres)
        outFile.write("%s %s %s %s %s %s\n" % (t/3600, errRhop, errRhoU, errRhoV, errRhoThetap, errSurfPres))
        outFile.close()
      elapsed = (time.clock() - start)
      print ('Time elapsed: ',elapsed,' s')
      n_export=n_export+1
  norm = timeIter.iterate (solution, dt, t)
#  nuStab0.apply(solution)
#  nuStab12.apply(solution)
#  nuStab3.apply(solution)
#  nuStab4.apply(solution)
  t=t+dt
  if (Msg.GetCommRank()==0):
    sys.stdout.write('.')
    sys.stdout.flush()
print ('')    
elapsed = (time.clock() - start)
if (Msg.GetCommRank()==0):
  print ('Time elapsed: ',elapsed,' s')


Msg.Exit(0)


