from dgpy import *
from math import *
import time, os, sys
import gmshPartition

#### Physical constants ###
gamma=1.4
Rd=287.0
p0=1.0e5
g=9.80616
Cv=Rd/(gamma-1.0)
Cp=Rd*(1.0+1.0/(gamma-1.0))
###########################
#### Begin and end time ###
Ti=0.0
Tf=300.0
###########################

#Period of the oscillations
period = 100


order=4
dimension=2

model = GModel()
name='line'
if Msg.GetCommSize()>1:
  partStr='_part_%i' % Msg.GetCommSize()
else:
  partStr=''
model.load(name + partStr + '_2d_XY.msh')

groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag();

claw = dgEulerAtmLaw(dimension)
solution = dgDofContainer(groups, claw.getNbFields())

XYZ = groups.getFunctionCoordinates();
def hydrostaticState(z) :
    thetaHs=300.0
    exnerHs=1.0-g/(Cp*thetaHs)*z
    rhoHs=p0/(Rd*thetaHs)*exnerHs**(Cv/Rd)
    return rhoHs,exnerHs,thetaHs

def initialCondition(FCT,  XYZ) :
  FCT.setAll(0)

def boundaryForcing(FCT, XYZ, time) :
    thetac=5
    xc=500.0
    zc=0.0
    rc=250.0
    
    for i in range (0,XYZ.size1()) :
        x=XYZ(i,0)
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        r=sqrt((x-xc)**2+(z-zc)**2)
        if (r>rc):
            thetaPert=thetaHs
        else:
            thetaPert=thetaHs+thetac/2.0*(1.0+cos(pi*r/rc))*abs(sin(time(i,0)/period*2*pi))
        rhoPert=rhoHs#p0/(Rd*thetaPert)*exnerHs**(Cv/Rd)
        FCT.set(i,0,rhoPert-rhoHs)
        FCT.set(i,1,0.0)
        FCT.set(i,2,0.0)
        FCT.set(i,3,rhoPert*thetaPert-rhoHs*thetaHs)

def rhoHydrostatic(FCT,  XYZ) :
    for i in range (0,XYZ.size1()) :
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        FCT.set(i,0,rhoHs)

def rhoThetaHydrostatic(FCT,  XYZ) :
    for i in range (0,XYZ.size1()) :
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        FCT.set(i,0,rhoHs*thetaHs)
        
def getVelocity(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        rho=rhoHs+sol(i,0)
        FCT.set(i,0,sol(i,1)/rho)
        FCT.set(i,1,sol(i,2)/rho)
        FCT.set(i,2,0)

def getRhop(FCT, sol) :
    for i in range (0,sol.size1()) :
        FCT.set(i,0,sol(i,0))

def getpp(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        rho=rhoHs+sol(i,0)
        rhoTheta = rhoHs*thetaHs+sol(i,3)
        pHs=p0*(rhoHs*thetaHs*Rd/p0)**gamma
        p=p0*(rhoTheta*Rd/p0)**gamma
        FCT.set(i,0,p-pHs)

def getpHs(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        pHs=p0*(rhoHs*thetaHs*Rd/p0)**gamma
        FCT.set(i,0,pHs)

def getp(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        rhoTheta = rhoHs*thetaHs+sol(i,3)
        p=p0*(rhoTheta*Rd/p0)**gamma
        FCT.set(i,0,p)

def getThetap(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        rho=rhoHs+sol(i,0)
        FCT.set(i,0,(sol(i,3)+(rhoHs*thetaHs))/rho-thetaHs)

uv=functionPython(3, getVelocity, [solution.getFunction(), XYZ])
rhop=functionPython(1, getRhop, [solution.getFunction()])
pp=functionPython(1, getpp, [solution.getFunction(), XYZ])
thetap=functionPython(1, getThetap, [solution.getFunction(), XYZ])

initF=functionPython(4, initialCondition, [XYZ])
solution.interpolate(initF)

rhoHs = dgDofContainer(groups, 1)
rhoHs.interpolate(functionPython(1, rhoHydrostatic, [XYZ]))
rhoThetaHs = dgDofContainer(groups, 1)
rhoThetaHs.interpolate(functionPython(1, rhoThetaHydrostatic, [XYZ]))

claw.setHydrostaticState(rhoHs,rhoThetaHs)
claw.setPhysicalConstants(gamma,Rd,p0,g)

boundaryWall = claw.newBoundaryWall()
timeFunction = functionConstant(Ti)
boundaryForcingF=functionPython(4, boundaryForcing, [XYZ, timeFunction])
toReplace = [function.getSolution()]
replaceBy = [boundaryForcingF]
boundaryHeated = claw.newOutsideValueBoundaryGeneric("",toReplace,replaceBy)
claw.addBoundaryCondition('bottom_domain', boundaryWall)
claw.addBoundaryCondition('top_domain', boundaryHeated) #top is actually bottom due to the inverse extrusion
claw.addBoundaryCondition('left', boundaryWall)
claw.addBoundaryCondition('right', boundaryWall)



claw.setFilterMode(FILTER_LINEAR)
petscIm = linearSystemPETScBlockDouble()
petscIm.setParameter("petscOptions","-ksp_type preonly -pc_type lu")
#petscIm.setParameter("petscOptions",  "-pc_type lu -ksp_type preonly -pc_factor_mat_solver_package mumps")
dofIm = dgDofManager.newDGBlock(groups, claw.getNbFields(), petscIm)
#timeIter = dgIMEXTSRK(claw, dofIm, 4)     #timeorder
timeOrder=3
timeIter = dgIMEXRK(claw, dofIm, timeOrder)     #timeorder
#timeIter = dgDIRK(claw, dofIm, 2)     #timeorder
timeIter.getNewton().setVerb(1)     #Verbosity

#dt=claw.getMinOfTimeSteps(solution)*4.0/(2*timeOrder+1) #infinite because no initial velocity
dt=0.6

if (Msg.GetCommRank()==0):
    print ("Time step:",dt)
nbSteps = int(ceil(Tf/dt))

#nbSteps=200

#Export data
def getExp(FCT, uv, rhop, thetap, pp) :
    for i in range (0,uv.size1()) :
        FCT.set(i,0,uv(i,0))
        FCT.set(i,1,uv(i,1))
        FCT.set(i,2,rhop(i,0))
        FCT.set(i,3,thetap(i,0))
        FCT.set(i,4,pp(i,0))
Exp=functionPython(5, getExp, [uv, rhop, thetap, pp])
nCompExp=[2,1,1,1]
namesExp=["uv","rhop","thetap","pp"]

sBV = 2.0/3*(order+1.0)
muBV = 0.2
filterBV = dgFilterBoydVandeven(groups, "", sBV, muBV, True, False)
solutionBV = dgDofContainer(solution);
minDiff = fullMatrixDouble(claw.getNbFields(),1)
maxDiff = fullMatrixDouble(claw.getNbFields(),1)

solution.exportFunctionVtk(Exp,'output/export', 0, 0,"solution",nCompExp,namesExp)

t=Ti
#t=timeIter.starter(solution,dt,t,3)
n_export=0
timeStart=time.clock();


start = time.clock()
for i in range(0,nbSteps-1):
    if (i%10 == 0):
        solution.exportFunctionVtk(Exp,'output/export', t*1000, i,"solution",nCompExp,namesExp)
        if (Msg.GetCommRank()==0):
            print ('\nWriting output',n_export,'at time',t*1000,'ms and step',i,'over',nbSteps)
        n_export=n_export+1
    #We filter the dof manager and evaluate the max difference with unfiltered solution
    solutionBV.copy(solution)
    filterBV.apply(solutionBV)
    solutionBV.axpy(solution, -1)
    solutionBV.minmax(minDiff, maxDiff);
    if (Msg.GetCommRank()==0):
      #Print the max difference on density
      print("[%.1e]"%max(maxDiff(0,0),-minDiff(0,0)))
      print("CFL dt %.1e"%(claw.getMinOfTimeSteps(solution)*4.0/(2*timeOrder+1)))
    ##
    timeIter.iterate(solution, dt, t)
    t=t+dt
    timeFunction.set(t)
    if (Msg.GetCommRank()==0):
        sys.stdout.write('.')
        sys.stdout.flush()
if (Msg.GetCommRank()==0):
    print ('')
#    print 'dt=',dt,
elapsed = (time.clock() - start)
if (Msg.GetCommRank()==0):
    print ('Time elapsed: ',elapsed,' s')

Msg.Exit(0)
