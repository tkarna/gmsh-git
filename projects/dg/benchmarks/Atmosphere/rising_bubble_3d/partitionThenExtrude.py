from extrude import *
import sys
from math import *
from dgpy import * 
name = 'rising_bubble'
nbPart = int(sys.argv[1])


minRes=250
nbLayers = 6
H = 1000.
Hchange=1000

z=0
zTab=[z]
while z<Hchange:
  z=z+minRes
  zTab.append(z)
while z<H:
  z=z+minRes*exp(2.5*(z-12000)/13000)
  zTab.append(z)


def getLayersSigma (element, vertex) :
  x = vertex[0]
  y = vertex[1]
  hMount=1/((x**2/10000**2+y**2/10000**2+1)**(3/2))
  hMount=0
  h = H-hMount
  zTabMnt=[]
  for i in range(len(zTab)):
    zTabMnt.append((zTab[-1]-zTab[-i-1])*h/zTab[-1])
  return zTabMnt

if nbPart>1:
  partStr='_part_%i' % nbPart
else:
  partStr=''

model = GModel()
model.load(name+".msh")
pOpt = meshPartitionOptions()
pOpt.setNumOfPartitions(nbPart)
PartitionMesh(model,  pOpt)   
model.save(name + partStr + '.msh')

extrude(mesh(name + partStr + '.msh'), getLayersSigma).write(name + partStr + '_3d.msh')


Msg.Exit(0)
