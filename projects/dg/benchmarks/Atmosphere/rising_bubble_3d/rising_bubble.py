from dgpy import *
from math import *
import time, os, sys
import gmshPartition

#### Physical constants ###
gamma=1.4
Rd=287.0
p0=1.0e5
g=9.80616
Cv=Rd/(gamma-1.0)
Cp=Rd*(1.0+1.0/(gamma-1.0))
###########################
#### Begin and end time ###
Ti=0.0
Tf=700.0
###########################

order=3
dimension=3

model = GModel()
name='rising_bubble'
if Msg.GetCommSize()>1:
  partStr='_part_%i' % Msg.GetCommSize()
else:
  partStr=''
model.load(name + partStr + '_3d.msh')

groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag();
groupsH = dgGroupCollection.newByTag(model, dimension-1, order, ["bottom_bottom_surf"])
extrusion = dgExtrusion(groups, groupsH, ["bottom_bottom_surf"])
groups.splitFaceGroupsByOrientation(extrusion, ["bottom_bottom_surf", "top_bottom_surf"]) 

claw = dgEulerAtmLaw(dimension)
solution = dgDofContainer(groups, claw.getNbFields())

XYZ = groups.getFunctionCoordinates();
def hydrostaticState(z) :
    thetaHs=300.0
    exnerHs=1.0-g/(Cp*thetaHs)*z
    rhoHs=p0/(Rd*thetaHs)*exnerHs**(Cv/Rd)
    return rhoHs,exnerHs,thetaHs

def initialCondition(FCT,  XYZ) :
    thetac=0.5
    xc=500.0
    yc=500.0
    zc=350.0-1000
    rc=250.0
    for i in range (0,XYZ.size1()) :
        x=XYZ(i,0)
        y=XYZ(i,1)
        z=XYZ(i,2)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        r=sqrt((x-xc)**2+(y-yc)**2+(z-zc)**2)
        if (r>rc):
            thetaPert=thetaHs
        else:
            thetaPert=thetaHs+thetac/2.0*(1.0+cos(pi*r/rc))
        rhoPert=p0/(Rd*thetaPert)*exnerHs**(Cv/Rd)
        FCT.set(i,0,rhoPert-rhoHs)
        FCT.set(i,1,0.0)
        FCT.set(i,2,0.0)
        FCT.set(i,3,0.0)
        FCT.set(i,4,rhoPert*thetaPert-rhoHs*thetaHs)

def rhoHydrostatic(FCT,  XYZ) :
    for i in range (0,XYZ.size1()) :
        z=XYZ(i,2)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        FCT.set(i,0,rhoHs)

def rhoThetaHydrostatic(FCT,  XYZ) :
    for i in range (0,XYZ.size1()) :
        z=XYZ(i,2)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        FCT.set(i,0,rhoHs*thetaHs)
        
def getVelocity(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,2)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        rho=rhoHs+sol(i,0)
        FCT.set(i,0,sol(i,1)/rho)
        FCT.set(i,1,sol(i,2)/rho)
        FCT.set(i,2,sol(i,3)/rho)

def getRhop(FCT, sol) :
    for i in range (0,sol.size1()) :
        FCT.set(i,0,sol(i,0))

def getpp(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,2)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        rho=rhoHs+sol(i,0)
        rhoTheta = rhoHs*thetaHs+sol(i,4)
        pHs=p0*(rhoHs*thetaHs*Rd/p0)**gamma
        p=p0*(rhoTheta*Rd/p0)**gamma
        FCT.set(i,0,p-pHs)

def getpHs(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,2)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        pHs=p0*(rhoHs*thetaHs*Rd/p0)**gamma
        FCT.set(i,0,pHs)

def getp(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,2)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        rhoTheta = rhoHs*thetaHs+sol(i,4)
        p=p0*(rhoTheta*Rd/p0)**gamma
        FCT.set(i,0,p)

def getThetap(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,2)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        rho=rhoHs+sol(i,0)
        FCT.set(i,0,(sol(i,4)+(rhoHs*thetaHs))/rho-thetaHs)

uvw=functionPython(3, getVelocity, [solution.getFunction(), XYZ])
rhop=functionPython(1, getRhop, [solution.getFunction()])
pp=functionPython(1, getpp, [solution.getFunction(), XYZ])
thetap=functionPython(1, getThetap, [solution.getFunction(), XYZ])

initF=functionPython(5, initialCondition, [XYZ])
solution.interpolate(initF)

rhoHs = dgDofContainer(groups, 1)
rhoHs.interpolate(functionPython(1, rhoHydrostatic, [XYZ]))
rhoThetaHs = dgDofContainer(groups, 1)
rhoThetaHs.interpolate(functionPython(1, rhoThetaHydrostatic, [XYZ]))

claw.setHydrostaticState(rhoHs,rhoThetaHs)
claw.setPhysicalConstants(gamma,Rd,p0,g)


boundaryWall = claw.newBoundaryWall()
claw.addBoundaryCondition('none', boundaryWall)
claw.addBoundaryCondition('bottom', boundaryWall)
claw.addBoundaryCondition('left', boundaryWall)
claw.addBoundaryCondition('right', boundaryWall)
claw.addBoundaryCondition('top', boundaryWall)
claw.addBoundaryCondition('bottom_surf', boundaryWall)
claw.addBoundaryCondition('top_surf', boundaryWall)
claw.addBoundaryCondition('bottom_bottom_surf', boundaryWall)
claw.addBoundaryCondition('top_bottom_surf', boundaryWall)



#petsc = linearSystemPETScBlockDouble()
#petsc.setParameter("petscOptions",  "-pc_type lu -ksp_type preonly -pc_factor_mat_solver_package superlu_dist")
##petsc.setParameter("petscOptions",  "-pc_type lu -ksp_type preonly -pc_factor_mat_solver_package mumps")
##petsc.setParameter("petscOptions",  "-ksp_atol 1e-12 -ksp_rtol 1e-12")
#if (Msg.GetCommSize()==1):
#    petsc.setParameter("petscOptions",  "-pc_type lu")#-ksp_atol 1e-12 -ksp_rtol 1e-12")
#dof = dgDofManager.newDGBlock(groups, claw.getNbFields(), petsc)
#timeIter = dgIMEXRK(claw, dof, 2)     #timeorder
#timeIter.getNewton().setVerb(1)     #Verbosity


claw.setFilterMode(FILTER_LINEAR);
#petscIm = dgLinearSystemExtrusion(claw, groups, extrusion)
petscIm = linearSystemPETScBlockDouble()
petscIm.setParameter("petscOptions",  "-ksp_type preonly -pc_type lu")
if (Msg.GetCommSize()>1):
  Msg.Fatal("Direct LU is for serial jobs only. Please change the solver or run serial")
#petscIm.setParameter("petscOptions",  "-ksp_atol 0 -ksp_rtol 1e-12")
dofIm = dgDofManager.newDGBlock(groups, claw.getNbFields(), petscIm)
timeIter = dgIMEXRK(claw, dofIm, 2)     #timeorder
timeIter.getNewton().setAtol(1.e-5) #ATol
timeIter.getNewton().setRtol(1.e-8) #Rtol
timeIter.getNewton().setVerb(2)     #Verbosity

#timeIter = dgERKLinear(claw, groups, DG_ERK_22)




#Export data
def getExp(FCT, uvw, rhop, thetap, pp) :
    for i in range (0,uvw.size1()) :
        FCT.set(i,0,uvw(i,0))
        FCT.set(i,1,uvw(i,1))
        FCT.set(i,2,uvw(i,2))
        FCT.set(i,3,rhop(i,0))
        FCT.set(i,4,thetap(i,0))
        FCT.set(i,5,pp(i,0))
Exp=functionPython(6, getExp, [uvw, rhop, thetap, pp])
nCompExp=[3,1,1,1]
namesExp=["uvw","rhop","thetap","pp"]

solution.exportFunctionVtk(Exp,'test', 0, 0,"solution",nCompExp,namesExp)

#dt=claw.getMinOfTimeSteps(solution, extrusion)
dt=2
if (Msg.GetCommRank()==0):
    print ("Time step:",dt)
nbSteps = int(ceil(Tf/dt))


t=Ti
n_export=0
start = time.clock()
for i in range(0,nbSteps-1):
  if (Msg.GetCommRank()==0):
    print ("max dt is ",claw.getMinOfTimeSteps(solution, extrusion))
  if (i%50 == 0):
    solution.exportFunctionVtk(Exp,'output', t, i,"solution",nCompExp,namesExp)
    if (Msg.GetCommRank()==0):
      print ('\nWriting output',n_export,'at time',t,'and step',i,'over',nbSteps)
      elapsed = (time.clock() - start)
      print ('Time elapsed: ',elapsed,' s')
    n_export=n_export+1
  norm = timeIter.iterate (solution, dt, t)
  t=t+dt
  if (Msg.GetCommRank()==0):
    sys.stdout.write('.')
    sys.stdout.flush()
print ('')    
elapsed = (time.clock() - start)
if (Msg.GetCommRank()==0):
    print ('Time elapsed: ',elapsed,' s')
Msg.Exit(0)
