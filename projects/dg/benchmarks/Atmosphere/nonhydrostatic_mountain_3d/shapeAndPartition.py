from extrude import *
import sys
from math import *

H=33000
def hMount(x,y):
    hc=1
    xc=72000
    yc=14400
    ac=1000
    return hc/(1+(sqrt((x-xc)**2+(y-yc)**2)/ac)**2)

nameStem = 'nonhydrostatic_mountain'
nbProcs = int(sys.argv[1])

f = open(nameStem+'.msh', 'r')
fs = open(nameStem+'_shaped.msh', 'w')
line=""
while (line.count("$Nodes")==0):
    line=f.readline()
    fs.write(line)
line=f.readline()
nbnodes=int(line)
fs.write(line)
print nbnodes

zmax=33000

for inode in range(nbnodes):
    line=f.readline()
    tab=line.split(" ")
    x=float(tab[1])
    y=float(tab[2])
    z=float(tab[3])
    hm=hMount(x,y)
    z=z*(H-hm)/H+hm
    tab[3]=("%lf\n")%z
    fs.write(tab[0]+" "+tab[1]+" "+tab[2]+" "+tab[3])
while (line!=""):
    line=f.readline()
    fs.write(line)

f.close()
fs.close()

model = GModel()
model.load(nameStem+"_shaped.msh")
pOpt = meshPartitionOptions()
pOpt.setNumOfPartitions(nbProcs)
PartitionMesh(model,  pOpt)
model.save(nameStem+'_'+ str(nbProcs)+".msh")

Msg.Exit(0)

