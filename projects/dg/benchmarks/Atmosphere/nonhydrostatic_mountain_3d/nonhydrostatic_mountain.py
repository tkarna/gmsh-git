from dgpy import *
from math import *
import time, os, sys

#### Physical constants ###
gamma=1.4
Rd=287
p0=1.0e5
g=9.80616
Cv=Rd/(gamma-1)
Cp=Rd*(1+1/(gamma-1))

print "gamma:",gamma,"Rd:",Rd,"Cv:",Cv,"Cp:",Cp,"p0:",p0,"g:",g
###########################
#### Begin and end time ###
Ti=0
Tf=5*3600
###########################

order=4
dimension=3

model = GModel()
model.load("nonhydrostatic_mountain_%i.msh"%(Msg.GetCommSize()))

groups = dgGroupCollection(model, dimension, order)
claw = dgEulerAtmLaw(dimension)
solution = dgDofContainer(groups, claw.getNbFields())

XYZ = groups.getFunctionCoordinates()
            
def hydrostaticState(z) :
    theta0=280
    N=1.e-2
    thetaHs=theta0*exp(N**2*z/g)
    exnerHs=1+ g**2/(Cp*theta0*N**2) * (exp(-N**2*z/g)-1)
    rhoHs=p0/(Rd*thetaHs)*exnerHs**(Cv/Rd)

    return rhoHs,thetaHs

def initialCondition(FCT,  XYZ) :
    a=5000
    dtheta0=1.0e-2
    H=10000
    xc=-50000
    U=10.0
    for i in range (0,XYZ.size1()) :
        z=XYZ(i,2)
        rhoHs,thetaHs = hydrostaticState(z)
        FCT.set(i,0,0)
        FCT.set(i,1,U*rhoHs)
        FCT.set(i,2,0)
        FCT.set(i,3,0)
        FCT.set(i,4,0)

def rhoHydrostatic(FCT,  XYZ) :
    for i in range (0,XYZ.size1()) :
        z=XYZ(i,2)
        rhoHs,thetaHs = hydrostaticState(z)
        FCT.set(i,0,rhoHs)

def rhoThetaHydrostatic(FCT,  XYZ) :
    for i in range (0,XYZ.size1()) :
        z=XYZ(i,2)
        rhoHs,thetaHs = hydrostaticState(z)
        FCT.set(i,0,rhoHs*thetaHs)
        
def getVelocity(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,2)
        rhoHs,thetaHs = hydrostaticState(z)
        rho=rhoHs+sol(i,0)
        FCT.set(i,0,sol(i,1)/rho)
        FCT.set(i,1,sol(i,2)/rho)
        FCT.set(i,2,sol(i,3)/rho)

def getRhop(FCT, sol) :
    for i in range (0,sol.size1()) :
        FCT.set(i,0,sol(i,0))

def getpp(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,2)
        rhoHs,thetaHs = hydrostaticState(z)
        rho=rhoHs+sol(i,0)
        rhoTheta = rhoHs*thetaHs+sol(i,4)
        pHs=p0*(rhoHs*thetaHs*Rd/p0)**gamma
        p=p0*(rhoTheta*Rd/p0)**gamma
        FCT.set(i,0,p-pHs)

def getpHs(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,2)
        rhoHs,thetaHs = hydrostaticState(z)
        pHs=p0*(rhoHs*thetaHs*Rd/p0)**gamma
        FCT.set(i,0,pHs)

def getp(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,2)
        rhoHs,thetaHs = hydrostaticState(z)
        rhoTheta = rhoHs*thetaHs+sol(i,4)
        p=p0*(rhoTheta*Rd/p0)**gamma
        FCT.set(i,0,p)

def getThetap(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,2)
        rhoHs,thetaHs = hydrostaticState(z)
        rho=rhoHs+sol(i,0)
        FCT.set(i,0,(sol(i,4)+(rhoHs*thetaHs))/rho-thetaHs)


CCode = """
#include "fullMatrix.h"
#include "function.h"
extern "C" {
void sponge (dataCacheMap *, fullMatrix<double> &FCT, fullMatrix<double> &XYZ, fullMatrix<double> &sol, fullMatrix<double> &reference) {
    double g=9.80616;
    for (int i=0; i<XYZ.size1(); i++){
        double z=XYZ(i,2);
        double zb=23000;
        double zt=33000;
        double alpha=.01;
        if (z<zb){
            FCT.set(i,0,0);
            FCT.set(i,1,0);
            FCT.set(i,2,0);
            FCT.set(i,3,-g*sol(i,0));
            FCT.set(i,4,0);
        }else{
            double tau=-alpha/2*(1+cos(((z-zb)/(zt-zb)-1)*M_PI));
            FCT.set(i,0,tau*(sol(i,0)-reference(i,0)));
            FCT.set(i,1,tau*(sol(i,1)-reference(i,1)));
            FCT.set(i,2,tau*(sol(i,2)-reference(i,2)));
            FCT.set(i,3,tau*(sol(i,3)-reference(i,3))-g*sol(i,0));
            FCT.set(i,4,tau*(sol(i,4)-reference(i,4)));
        }
    }
}
}
"""
if (Msg.GetCommRank()==0):
    functionC.buildLibrary (CCode, "sourceTerm.so");
Msg.Barrier()

uv=functionPython(3, getVelocity, [solution.getFunction(), XYZ])
rhop=functionPython(1, getRhop, [solution.getFunction()])
pHs=functionPython(1, getpHs, [solution.getFunction(), XYZ])
p=functionPython(1, getp, [solution.getFunction(), XYZ])
pp=functionPython(1, getpp, [solution.getFunction(), XYZ])
thetap=functionPython(1, getThetap, [solution.getFunction(), XYZ])


initF=functionPython(5, initialCondition, [XYZ])
solution.interpolate(initF)

rhoHs = dgDofContainer(groups, 1)
rhoHs.interpolate(functionPython(1, rhoHydrostatic, [XYZ]))
rhoThetaHs = dgDofContainer(groups, 1)
rhoThetaHs.interpolate(functionPython(1, rhoThetaHydrostatic, [XYZ]))

claw.setHydrostaticState(rhoHs,rhoThetaHs)
claw.setPhysicalConstants(gamma,Rd,p0,g)


boundaryWall = claw.newBoundaryWall()
claw.addBoundaryCondition('bottom_surf', boundaryWall)

#We need to use a dof container to restrict the initial condition
# (=boundary condition) to the discretization space
initDof = dgDofContainer(groups, 5)
initDof.interpolate(initF)

spongeTerm=functionC("sourceTerm.so","sponge",5,[XYZ, solution.getFunction(), initDof.getFunction()])
claw.setSource("", spongeTerm);


outsideBoundary = claw.newOutsideValueBoundary("",initDof.getFunction())
claw.addBoundaryCondition('top_surf', outsideBoundary)
claw.addBoundaryCondition('bottom', outsideBoundary)
claw.addBoundaryCondition('top', outsideBoundary)
claw.addBoundaryCondition('left', outsideBoundary)
claw.addBoundaryCondition('right', outsideBoundary)

rk = dgERK(claw, None, DG_ERK_22)
t=Ti
dt=groups.getMinOfTimeSteps(claw,solution)

if (Msg.GetCommRank()==0):
    print "Time step:",dt

nbSteps = int(ceil(Tf/dt))

#Export data
def getExp(FCT, uv, rhop, thetap, pp) :
    for i in range (0,uv.size1()) :
        FCT.set(i,0,uv(i,0))
        FCT.set(i,1,uv(i,1))
        FCT.set(i,2,uv(i,2))
        FCT.set(i,3,rhop(i,0))
        FCT.set(i,4,thetap(i,0))
        FCT.set(i,5,pp(i,0))
Exp=functionPython(6, getExp, [uv, rhop, thetap, pp])
nCompExp=[3,1,1,1]
namesExp=["uv","rhop","thetap","pp"]

solution.exportFunctionVtk(Exp,'output/export', 0, 0,"solution",nCompExp,namesExp)

n_export=0
for i in range(0,nbSteps-1):
    #dt=groups.getMinOfTimeSteps(claw,solution)
    if (i==nbSteps-1):
        dt=Tf-t
    if (i%100 == 0):
        if (Msg.GetCommRank()==0):
            print '\nWriting output',n_export,'at time',t,'and step',i,'over',nbSteps,
        solution.exportFunctionVtk(Exp,'output/export', t, i,"solution",nCompExp,namesExp)
        if (Msg.GetCommRank()==0):
            print '[OK]'
        n_export=n_export+1
    norm = rk.iterate (solution, dt, t)
    t=t+dt
    if (Msg.GetCommRank()==0):
        sys.stdout.write('.')
        sys.stdout.flush()
print ''    
