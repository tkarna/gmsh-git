from partition1d import *
from dgpy import *
from extrude import *
from rotateZtoY import *

name='line'
nbPart = int(sys.argv[1])

nbLayers = 5
H = 10000.

res=H/nbLayers

def getLayersSigma (element, vertex) :
  z=0
  zTab=[z]
  for i in range(nbLayers):
    z=z-res
    zTab.append(z)
  return zTab

if nbPart>1:
  partStr='_part_%i' % nbPart
else:
  partStr=''
partition1d(name+'.msh', name + partStr + '.msh', nbPart)
extrude(mesh(name + partStr + '.msh'), getLayersSigma).write(name + partStr + '_2d.msh')
rotateZtoY(name + partStr + '_2d.msh',name + partStr + '_2d_XY.msh')
Msg.Exit(0)
