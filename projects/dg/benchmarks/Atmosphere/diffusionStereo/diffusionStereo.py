from dgpy import *
from math import *
import time, os, sys
import gmshPartition

###########################
#### Begin and end time ###
Ti=0.0
Tf=20000000.
###########################

R = 6371229

order=2
dimension=3


integOrder=2*order+1+4
dataCacheMap.setDefaultIntegrationOrder(integOrder)

model = GModel()

name='aquaplanet_part'
partStr='_%i' % Msg.GetCommSize()
model.load(name + partStr + '_3d.msh')


groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag();

circleTransform = dgCircleTransform(groups)

st = dgSpaceTransformSpherical(R, groups)
groups._mesh.setSpaceTransform(st, groups)

groupsH = dgGroupCollection.newByTag(model, dimension-1, order, ["bottom_Top","bottom_Bottom"])
extrusion = dgExtrusion(groups, groupsH, ["bottom_Top","bottom_Bottom"])
groups.splitFaceGroupsByOrientation(extrusion, ["bottom_Top","bottom_Bottom","top_Top", "top_Bottom"]) 

claw = dgEulerAtmLaw(dimension)
solution = dgDofContainer(groups, claw.getNbFields())

XYZ = groups.getFunctionCoordinates();
xyzCircle = transformToCircle(circleTransform)

xyzCircleInteg = functionPrecomputed(groups, integOrder, 3)
xyzCircleInteg.compute(xyzCircle)

CCode = """
#include "fullMatrix.h"
#include "function.h"
#include "math.h"
#include "stdio.h"
#include "dgGroupOfElements.h"

extern "C" {
void analyticalSol (dataCacheMap *c, fullMatrix<double> &FCT, fullMatrix<double> &XYZ, fullMatrix<double> &time, fullMatrix<double> &R_) {
    double R = R_(0, 0);
    double t = time(0, 0);
    double rev = 1;
    if (c->getGroupOfElements()->getPhysicalTag() == "volume_Bottom"){
            rev = -1;
    }
    for (int i=0; i<XYZ.size1(); i++){
        double x = XYZ(i,0);
        double y = XYZ(i,1);
        double z = XYZ(i,2);
        double V = 10000;
        double J = 4*R*(R+z)/(x*x+y*y+4*R*R);
        double u = (J*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*((R*R)*4.0-x*x+y*y)*(-1.0/4.0))/(R*(R+z))-(J*rev*x*cos((J*y*z*(3.9E1/1.0E1))/(R*V)+1.0))/(R+z)-(J*x*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*(1.0/2.0))/(R*(R+z));
        double v = (J*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((R*R)*4.0+x*x-y*y)*(1.0/4.0))/(R*(R+z))-(J*rev*y*cos((J*y*z*(3.9E1/1.0E1))/(R*V)+1.0))/(R+z)+(J*x*y*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*(1.0/2.0))/(R*(R+z));
        double w = (J*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)))/(R+z)-(J*x*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0))/(R+z)-(J*rev*cos((J*y*z*(3.9E1/1.0E1))/(R*V)+1.0)*((R*R)*-4.0+x*x+y*y)*(1.0/4.0))/(R*(R+z));
        FCT.set(i,0,0);
        FCT.set(i,1,u);
        FCT.set(i,2,v);
        FCT.set(i,3,w);
        FCT.set(i,4,0);

    }
}

void error (dataCacheMap *, fullMatrix<double> &FCT, fullMatrix<double> &sol, fullMatrix<double> &analyticalSol) {
    for (int i=0; i<FCT.size1(); i++){
        FCT.set(i,0,0);
        FCT.set(i,1,pow(sol(i,1)-analyticalSol(i,1),2));
        FCT.set(i,2,pow(sol(i,2)-analyticalSol(i,2),2));
        FCT.set(i,3,pow(sol(i,3)-analyticalSol(i,3),2));
        FCT.set(i,4,0);
    }
}
void source (dataCacheMap *c, fullMatrix<double> &FCT, fullMatrix<double> &XYZ, fullMatrix<double> &time, fullMatrix<double> &R_) {
    double R = R_(0, 0);
    double t = time(0, 0);
    double rev = 1;
    if (c->getGroupOfElements()->getPhysicalTag() == "volume_Bottom"){
            rev = -1;
    }
    for (int i=0; i<XYZ.size1(); i++){
        double x = XYZ(i,0);
        double y = XYZ(i,1);
        double z = XYZ(i,2);
        double J = 4 * R * (R + z) / (4 * R * R + x * x + y * y);
        double V = 10000;
        double source1 = 1.0/(J*J)*((R*R)*1.0/(V*V)*(sin((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)-2.0)*((J*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*(-1.0/2.0))/(R*(R+z))+(J*J)*1.0/(R*R)*(y*y)*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(1.0/2.0)+(J*J)*1.0/(R*R)*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*((R*R)*4.0-x*x+y*y)*(1.0/8.0)-(J*J*J)*1.0/(R*R*R)*x*(y*y*y)*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,3.0)*(1.0/4.0)+((J*J)*rev*x*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(1.0/2.0))/R-(J*J*J)*1.0/(R*R*R)*(y*y)*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,3.0)*((R*R)*4.0-x*x+y*y)*(1.0/8.0)+(J*J)*1.0/(R*R)*x*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*(3.0/4.0)+(J*x*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((J*J*J)*1.0/(R*R*R*R)*rev*x*(y*y)*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*x*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*x*(y*y)*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z)))/(R*(R+z))-(J*J)*1.0/(R*R)*x*(y*y)*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*((J*J*J)*1.0/(R*R*R*R)*rev*x*(y*y)*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*x*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*x*(y*y)*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z))*(1.0/2.0)-(J*J*J)*1.0/(R*R)*rev*x*(y*y)*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,3.0)*(1.0/2.0)+(J*x*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*pow((J*J*J)*1.0/(R*R*R*R)*rev*x*(y*y)*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*x*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*x*(y*y)*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z),2.0)*(1.0/2.0))/(R*(R+z))+(J*x*y*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((J*J*J)*1.0/(R*R*R*R)*rev*x*y*(9.0/4.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*x*(y*y*y)*(9.0/4.0))/(R+z)+(J*J*J*J*J)*1.0/(R*R*R*R*R*R)*rev*x*(y*y*y)*1.0/pow(R+z,2.0)*((R*R)*-4.0+x*x+y*y)*(9.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(2.7E1/1.6E1))/(R+z))*(1.0/2.0))/(R*(R+z))+(J*J*J)*1.0/(R*R*R*R)*1.0/(V*V)*rev*x*(z*z)*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,3.0)*pow(R*z*2.0-J*(y*y)+(R*R)*2.0,2.0)*(1.521E3/4.0E2)-((J*J)*1.0/(R*R*R*R)*rev*(y*y)*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/8.0E1))/V+((J*J*J)*1.0/(R*R*R*R*R)*rev*(y*y)*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,3.0)*((R*R)*4.0-x*x+y*y)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/3.2E2))/V-((J*J*J)*1.0/(R*R*R)*rev*x*y*z*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,3.0)*(R*z*2.0-J*(y*y)+(R*R)*2.0)*(3.9E1/2.0E1))/V-((J*J*J)*1.0/(R*R*R)*rev*x*y*z*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,3.0)*(R*z*3.0-J*(y*y)+(R*R)*3.0)*(3.9E1/2.0E1))/V+(J*J*J)*1.0/(R*R*R*R*R*R*R)*1.0/(V*V)*(rev*rev)*(y*y)*(z*z)*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,3.0)*((R*R)*4.0-x*x+y*y)*pow(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0,2.0)*(7.29E2/2.56E4)-((J*J)*1.0/(R*R*R*R*R)*rev*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,3.0)*((R*R)*4.0-x*x+y*y)*(R*z-J*(y*y)+R*R)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/3.2E2))/V)+(R*R)*1.0/(V*V)*(sin((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)-2.0)*((J*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*(1.0/2.0))/(R*(R+z))-(J*J)*1.0/(R*R)*(x*x)*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(1.0/2.0)+(J*J)*1.0/(R*R)*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*((R*R)*4.0-x*x+y*y)*(1.0/8.0)-(J*J*J)*1.0/(R*R*R)*(x*x*x)*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,3.0)*(1.0/4.0)+((J*J)*rev*x*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(3.0/2.0))/R-(J*J*J)*1.0/(R*R*R)*(x*x)*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,3.0)*((R*R)*4.0-x*x+y*y)*(1.0/8.0)-(J*J*J)*1.0/(R*R)*rev*(x*x*x)*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,3.0)*(1.0/2.0)+(J*J)*1.0/(R*R)*x*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*(3.0/4.0)+(J*y*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((J*J*J)*1.0/(R*R*R*R)*rev*(x*x)*y*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*(x*x)*y*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z)))/(R*(R+z))-(J*J)*1.0/(R*R)*(x*x)*y*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*((J*J*J)*1.0/(R*R*R*R)*rev*(x*x)*y*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*(x*x)*y*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z))*(1.0/2.0)+(J*x*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*pow((J*J*J)*1.0/(R*R*R*R)*rev*(x*x)*y*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*(x*x)*y*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z),2.0)*(1.0/2.0))/(R*(R+z))+(J*x*y*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((J*J*J)*1.0/(R*R*R*R)*rev*x*y*(9.0/4.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*(x*x*x)*y*(9.0/4.0))/(R+z)+(J*J*J*J*J)*1.0/(R*R*R*R*R*R)*rev*(x*x*x)*y*1.0/pow(R+z,2.0)*((R*R)*-4.0+x*x+y*y)*(9.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(2.7E1/1.6E1))/(R+z))*(1.0/2.0))/(R*(R+z))-((J*J*J)*1.0/(R*R)*rev*x*y*z*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(1.17E2/2.0E1))/V+(J*J*J*J*J)*1.0/(R*R*R*R)*1.0/(V*V)*rev*(x*x*x)*(y*y)*(z*z)*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,3.0)*(1.521E3/4.0E2)+((J*J*J*J)*1.0/(R*R*R)*rev*(x*x*x)*y*z*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,3.0)*(3.9E1/1.0E1))/V+((J*J)*1.0/(R*R*R*R)*rev*(x*x)*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/8.0E1))/V+((J*J*J)*1.0/(R*R*R*R*R)*rev*(x*x)*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,3.0)*((R*R)*4.0-x*x+y*y)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/3.2E2))/V+(J*J*J)*1.0/(R*R*R*R*R*R*R)*1.0/(V*V)*(rev*rev)*(x*x)*(z*z)*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,3.0)*((R*R)*4.0-x*x+y*y)*pow(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0,2.0)*(7.29E2/2.56E4)-((J*J)*1.0/(R*R*R*R*R)*rev*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,3.0)*((R*R)*4.0-x*x+y*y)*(R*z-J*(x*x)+R*R)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/3.2E2))/V)-(R*R)*1.0/(V*V)*cos((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)*((J*(1.8E1/5.0))/R-((J*J)*1.0/(R*R)*(x*x)*(9.0/5.0))/(R+z)-(J*1.0/(R*R)*rev*x*z*(9.0/5.0))/V+((J*J)*1.0/(R*R*R)*rev*x*z*((R*R)*-4.0+x*x+y*y)*(9.0/2.0E1))/(V*(R+z)))*(-(J*rev*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V)))/(R+z)-(J*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*(1.0/2.0))/(R*(R+z))+(J*x*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*(1.0/2.0))/(R*(R+z))+(J*J)*1.0/(R*R)*x*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*((R*R)*4.0-x*x+y*y)*(1.0/8.0)+(J*J)*1.0/(R*R)*(x*x)*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*(1.0/4.0)+((J*J)*rev*(x*x)*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(1.0/2.0))/R+(J*x*y*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((J*J*J)*1.0/(R*R*R*R)*rev*(x*x)*y*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*(x*x)*y*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z))*(1.0/2.0))/(R*(R+z))-((J*J*J)*1.0/(R*R)*rev*(x*x)*y*z*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(3.9E1/2.0E1))/V-((J*J)*1.0/(R*R*R*R)*rev*x*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*((R*R)*4.0-x*x+y*y)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/3.2E2))/V)+(J*1.0/(V*V*V)*y*cos((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)*(R*rev*(z*z)*4.0+(R*R)*rev*z*4.0+J*(R*R)*rev*z*4.0-J*rev*(x*x)*z-J*rev*(y*y)*z+J*R*V*x*4.0)*((J*y*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*(-1.0/2.0))/(R*(R+z))-(J*x*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*(1.0/2.0))/(R*(R+z))+(J*J)*1.0/(R*R)*y*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*((R*R)*4.0-x*x+y*y)*(1.0/8.0)+(J*J)*1.0/(R*R)*x*(y*y)*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*(1.0/4.0)+(J*x*y*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((J*J*J)*1.0/(R*R*R*R)*rev*x*(y*y)*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*x*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*x*(y*y)*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z))*(1.0/2.0))/(R*(R+z))+((J*J)*rev*x*y*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(1.0/2.0))/R+((J*J)*1.0/(R*R)*rev*x*z*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(R*z*2.0-J*(y*y)+(R*R)*2.0)*(3.9E1/2.0E1))/V-((J*J)*1.0/(R*R*R*R)*rev*y*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*((R*R)*4.0-x*x+y*y)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/3.2E2))/V)*(9.0/2.0E1))/(R*(R+z)))-(sin((R*V+J*x*z+J*y*z)/(R*V))+3.0)*((J*J*J*J*J*J*J)*1.0/(R*R*R*R*R*R*R*R*R)*(x*x*x)*(y*y*y)*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,3.0)*pow((R*R)*-4.0+x*x+y*y,2.0)*(8.1E1/1.28E2)+((J*J)*rev*x*y*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(3.9E1/5.0))/(R*V)-((J*J)*1.0/(R*R*R)*rev*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*((R*R)*-4.0+x*x+y*y)*((R*R)*4.0-x*x+y*y)*(2.7E1/8.0E1))/V+(J*J*J*J)*1.0/(R*R*R*R*R)*rev*(x*x)*(y*y)*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,4.0)*(R*2.0+z*2.0)*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1)+(J*J*J)*1.0/(R*R*R*R*R)*1.0/(V*V)*(rev*rev)*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,3.0)*pow(R+z*2.0,2.0)*pow((R*R)*-4.0+x*x+y*y,2.0)*((R*R)*4.0-x*x+y*y)*(7.29E2/6.4E3)+(J*J*J)*1.0/(R*R)*1.0/(V*V)*rev*x*(y*y)*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,3.0)*pow(R+z*2.0,2.0)*(1.521E3/1.0E2))-((sin((R*V+J*x*z+J*y*z)/(R*V))*2.0+6.0)*((J*J*J*J)*1.0/(R*R*R*R*R)*rev*(x*x)*(y*y)*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1)+((J*J)*rev*x*y*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(R+z*2.0)*(3.9E1/1.0E1))/(R*V)-((J*J)*1.0/(R*R*R)*rev*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(R+z*2.0)*((R*R)*-4.0+x*x+y*y)*((R*R)*4.0-x*x+y*y)*(2.7E1/1.6E2))/V))/(R+z)-((x*((R*R)*1.0/(V*V)*(sin((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)-2.0)*((J*rev*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V)))/(R+z)+(J*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*(1.0/2.0))/(R*(R+z))-(J*x*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*(1.0/2.0))/(R*(R+z))+(J*J)*1.0/(R*R)*x*(y*y)*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(1.0/4.0)-((J*J)*rev*(y*y)*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(1.0/2.0))/R+(J*J)*1.0/(R*R)*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*((R*R)*4.0+x*x-y*y)*(1.0/8.0)+(J*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((R*R)*4.0+x*x-y*y)*((J*J*J)*1.0/(R*R*R*R)*rev*x*(y*y)*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*x*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*x*(y*y)*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z))*(1.0/4.0))/(R*(R+z))-((J*J)*1.0/(R*R)*rev*y*z*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(R*z*2.0-J*(y*y)+(R*R)*2.0)*(3.9E1/2.0E1))/V-((J*J)*1.0/(R*R*R*R)*rev*x*(y*y)*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/1.6E2))/V)-(J*1.0/(V*V*V)*y*cos((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)*((J*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((R*R)*4.0+x*x-y*y)*(1.0/4.0))/(R*(R+z))-(J*rev*y*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V)))/(R+z)+(J*x*y*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*(1.0/2.0))/(R*(R+z)))*(R*rev*(z*z)*4.0+(R*R)*rev*z*4.0+J*(R*R)*rev*z*4.0-J*rev*(x*x)*z-J*rev*(y*y)*z+J*R*V*x*4.0)*(9.0/2.0E1))/(R*(R+z)))+y*((R*R)*1.0/(V*V)*(sin((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)-2.0)*((J*y*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*(1.0/2.0))/(R*(R+z))+(J*x*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*(1.0/2.0))/(R*(R+z))-(J*J)*1.0/(R*R)*(x*x)*y*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(1.0/4.0)-(J*J)*1.0/(R*R)*x*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*((R*R)*4.0+x*x-y*y)*(1.0/8.0)-(J*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((R*R)*4.0+x*x-y*y)*((J*J*J)*1.0/(R*R*R*R)*rev*(x*x)*y*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*(x*x)*y*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z))*(1.0/4.0))/(R*(R+z))+((J*J)*rev*x*y*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(1.0/2.0))/R-((J*J*J)*1.0/(R*R)*rev*x*(y*y)*z*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(3.9E1/2.0E1))/V+((J*J)*1.0/(R*R*R*R)*rev*(x*x)*y*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/1.6E2))/V)-(R*R)*1.0/(V*V)*cos((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)*((J*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((R*R)*4.0+x*x-y*y)*(1.0/4.0))/(R*(R+z))-(J*rev*y*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V)))/(R+z)+(J*x*y*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*(1.0/2.0))/(R*(R+z)))*((J*(1.8E1/5.0))/R-((J*J)*1.0/(R*R)*(x*x)*(9.0/5.0))/(R+z)-(J*1.0/(R*R)*rev*x*z*(9.0/5.0))/V+((J*J)*1.0/(R*R*R)*rev*x*z*((R*R)*-4.0+x*x+y*y)*(9.0/2.0E1))/(V*(R+z)))))/R+(R*R)*1.0/(V*V)*(sin((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)-2.0)*((J*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0))/(R+z)-((J*J)*(x*x)*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(1.0/2.0))/R+(J*y*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((J*J*J)*1.0/(R*R*R*R)*rev*(x*x)*y*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*(x*x)*y*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z)))/(R+z)+((J*J)*x*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*(1.0/2.0))/R+(J*rev*x*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*(1.0/2.0))/(R*(R+z))-(J*J)*1.0/(R*R)*rev*x*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*((R*R)*-4.0+x*x+y*y)*(1.0/8.0)+((J*J)*1.0/(R*R*R)*rev*(x*x)*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/8.0E1))/V+((J*J*J)*1.0/(R*R*R)*rev*x*y*z*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*((R*R)*-4.0+x*x+y*y)*(3.9E1/8.0E1))/V)*2.0-(R*R)*1.0/(V*V)*cos((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)*(-(J*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)))/(R+z)+(J*x*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0))/(R+z)+(J*rev*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*((R*R)*-4.0+x*x+y*y)*(1.0/4.0))/(R*(R+z)))*((J*(1.8E1/5.0))/R-((J*J)*1.0/(R*R)*(x*x)*(9.0/5.0))/(R+z)-(J*1.0/(R*R)*rev*x*z*(9.0/5.0))/V+((J*J)*1.0/(R*R*R)*rev*x*z*((R*R)*-4.0+x*x+y*y)*(9.0/2.0E1))/(V*(R+z)))+J*1.0/(V*V)*y*cos((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)*((J*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((R*R)*4.0+x*x-y*y)*(1.0/8.0))/(R*(R+z))-(J*rev*y*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*(1.0/2.0))/(R+z)+(J*x*y*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*(1.0/4.0))/(R*(R+z)))*(1.8E1/5.0))/(J*(R+z))+(R*1.0/(V*V)*(sin((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)-2.0)*((J*rev*x*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V)))/(R+z)+(J*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*((R*R)*4.0-x*x+y*y)*(1.0/4.0))/(R*(R+z))+(J*x*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*(1.0/2.0))/(R*(R+z))))/(J*(R+z))-(J*cos((R*V+J*x*z+J*y*z)/(R*V))*(x+y)*(R+z*2.0)*((J*J*J*J)*1.0/(R*R*R*R*R)*rev*(x*x)*(y*y)*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1)+((J*J)*rev*x*y*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(R+z*2.0)*(3.9E1/1.0E1))/(R*V)-((J*J)*1.0/(R*R*R)*rev*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(R+z*2.0)*((R*R)*-4.0+x*x+y*y)*((R*R)*4.0-x*x+y*y)*(2.7E1/1.6E2))/V))/(R*V*(R+z));
        double source2 = -1.0/(J*J)*((R*R)*1.0/(V*V)*(sin((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)-2.0)*((J*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*(-1.0/2.0))/(R*(R+z))+(J*J)*1.0/(R*R)*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*((R*R)*4.0+x*x-y*y)*(1.0/8.0)+(J*J)*1.0/(R*R)*(x*x)*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*(1.0/2.0)+(J*J)*1.0/(R*R)*x*y*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(3.0/4.0)+(J*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((R*R)*4.0+x*x-y*y)*pow((J*J*J)*1.0/(R*R*R*R)*rev*(x*x)*y*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*(x*x)*y*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z),2.0)*(1.0/4.0))/(R*(R+z))-(J*J*J)*1.0/(R*R*R)*(x*x)*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,3.0)*((R*R)*4.0+x*x-y*y)*(1.0/8.0)-((J*J)*rev*y*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(1.0/2.0))/R-(J*J*J)*1.0/(R*R*R)*(x*x*x)*y*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,3.0)*(1.0/4.0)+(J*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((R*R)*4.0+x*x-y*y)*((J*J*J)*1.0/(R*R*R*R)*rev*x*y*(9.0/4.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*(x*x*x)*y*(9.0/4.0))/(R+z)+(J*J*J*J*J)*1.0/(R*R*R*R*R*R)*rev*(x*x*x)*y*1.0/pow(R+z,2.0)*((R*R)*-4.0+x*x+y*y)*(9.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(2.7E1/1.6E1))/(R+z))*(1.0/4.0))/(R*(R+z))+(J*x*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((J*J*J)*1.0/(R*R*R*R)*rev*(x*x)*y*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*(x*x)*y*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z)))/(R*(R+z))+(J*J*J)*1.0/(R*R)*rev*(x*x)*y*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,3.0)*(1.0/2.0)-(J*J)*1.0/(R*R)*x*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*((R*R)*4.0+x*x-y*y)*((J*J*J)*1.0/(R*R*R*R)*rev*(x*x)*y*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*(x*x)*y*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z))*(1.0/4.0)+((J*J*J)*1.0/(R*R)*rev*(y*y)*z*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(3.9E1/2.0E1))/V-(J*J*J*J*J)*1.0/(R*R*R*R)*1.0/(V*V)*rev*(x*x)*(y*y*y)*(z*z)*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,3.0)*(1.521E3/4.0E2)-((J*J*J*J)*1.0/(R*R*R)*rev*(x*x)*(y*y)*z*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,3.0)*(3.9E1/1.0E1))/V+((J*J*J)*1.0/(R*R*R*R*R)*rev*(x*x*x)*y*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,3.0)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/1.6E2))/V+(J*J*J)*1.0/(R*R*R*R*R*R*R)*1.0/(V*V)*(rev*rev)*(x*x*x)*y*(z*z)*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,3.0)*pow(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0,2.0)*(7.29E2/1.28E4)-((J*J)*1.0/(R*R*R*R)*rev*x*y*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/8.0E1))/V-((J*J)*1.0/(R*R*R*R*R)*rev*x*y*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,3.0)*(R*z-J*(x*x)+R*R)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/1.6E2))/V)+(R*R)*1.0/(V*V)*(sin((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)-2.0)*((J*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*(1.0/2.0))/(R*(R+z))+(J*J)*1.0/(R*R)*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*((R*R)*4.0+x*x-y*y)*(1.0/8.0)-(J*J)*1.0/(R*R)*(y*y)*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*(1.0/2.0)+(J*J)*1.0/(R*R)*x*y*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(3.0/4.0)+(J*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((R*R)*4.0+x*x-y*y)*pow((J*J*J)*1.0/(R*R*R*R)*rev*x*(y*y)*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*x*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*x*(y*y)*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z),2.0)*(1.0/4.0))/(R*(R+z))-(J*J*J)*1.0/(R*R*R)*(y*y)*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,3.0)*((R*R)*4.0+x*x-y*y)*(1.0/8.0)-((J*J)*rev*y*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(3.0/2.0))/R-(J*J*J)*1.0/(R*R*R)*x*(y*y*y)*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,3.0)*(1.0/4.0)+(J*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((R*R)*4.0+x*x-y*y)*((J*J*J)*1.0/(R*R*R*R)*rev*x*y*(9.0/4.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*x*(y*y*y)*(9.0/4.0))/(R+z)+(J*J*J*J*J)*1.0/(R*R*R*R*R*R)*rev*x*(y*y*y)*1.0/pow(R+z,2.0)*((R*R)*-4.0+x*x+y*y)*(9.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(2.7E1/1.6E1))/(R+z))*(1.0/4.0))/(R*(R+z))+(J*J*J)*1.0/(R*R)*rev*(y*y*y)*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,3.0)*(1.0/2.0)-(J*y*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((J*J*J)*1.0/(R*R*R*R)*rev*x*(y*y)*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*x*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*x*(y*y)*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z)))/(R*(R+z))-(J*J)*1.0/(R*R)*y*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*((R*R)*4.0+x*x-y*y)*((J*J*J)*1.0/(R*R*R*R)*rev*x*(y*y)*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*x*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*x*(y*y)*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z))*(1.0/4.0)-((J*J)*1.0/(R*R)*rev*z*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(R*z*2.0-J*(y*y)+(R*R)*2.0)*(3.9E1/1.0E1))/V+((J*J*J)*1.0/(R*R*R)*rev*(y*y)*z*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,3.0)*(R*z*2.0-J*(y*y)+(R*R)*2.0)*(3.9E1/2.0E1))/V+((J*J*J)*1.0/(R*R*R)*rev*(y*y)*z*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,3.0)*(R*z*3.0-J*(y*y)+(R*R)*3.0)*(3.9E1/2.0E1))/V-(J*J*J)*1.0/(R*R*R*R)*1.0/(V*V)*rev*y*(z*z)*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,3.0)*pow(R*z*2.0-J*(y*y)+(R*R)*2.0,2.0)*(1.521E3/4.0E2)+((J*J*J)*1.0/(R*R*R*R*R)*rev*x*(y*y*y)*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,3.0)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/1.6E2))/V+(J*J*J)*1.0/(R*R*R*R*R*R*R)*1.0/(V*V)*(rev*rev)*x*(y*y*y)*(z*z)*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,3.0)*pow(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0,2.0)*(7.29E2/1.28E4)-((J*J)*1.0/(R*R*R*R)*rev*x*y*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/8.0E1))/V-((J*J)*1.0/(R*R*R*R*R)*rev*x*y*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,3.0)*(R*z-J*(y*y)+R*R)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/1.6E2))/V)+(R*R)*1.0/(V*V)*cos((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)*((J*(1.8E1/5.0))/R-((J*J)*1.0/(R*R)*(x*x)*(9.0/5.0))/(R+z)-(J*1.0/(R*R)*rev*x*z*(9.0/5.0))/V+((J*J)*1.0/(R*R*R)*rev*x*z*((R*R)*-4.0+x*x+y*y)*(9.0/2.0E1))/(V*(R+z)))*((J*y*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*(1.0/2.0))/(R*(R+z))+(J*x*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*(1.0/2.0))/(R*(R+z))-(J*J)*1.0/(R*R)*(x*x)*y*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(1.0/4.0)-(J*J)*1.0/(R*R)*x*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*((R*R)*4.0+x*x-y*y)*(1.0/8.0)-(J*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((R*R)*4.0+x*x-y*y)*((J*J*J)*1.0/(R*R*R*R)*rev*(x*x)*y*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*(x*x)*y*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z))*(1.0/4.0))/(R*(R+z))+((J*J)*rev*x*y*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(1.0/2.0))/R-((J*J*J)*1.0/(R*R)*rev*x*(y*y)*z*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(3.9E1/2.0E1))/V+((J*J)*1.0/(R*R*R*R)*rev*(x*x)*y*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/1.6E2))/V)+(J*1.0/(V*V*V)*y*cos((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)*(R*rev*(z*z)*4.0+(R*R)*rev*z*4.0+J*(R*R)*rev*z*4.0-J*rev*(x*x)*z-J*rev*(y*y)*z+J*R*V*x*4.0)*((J*rev*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V)))/(R+z)+(J*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*(1.0/2.0))/(R*(R+z))-(J*x*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*(1.0/2.0))/(R*(R+z))+(J*J)*1.0/(R*R)*x*(y*y)*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(1.0/4.0)-((J*J)*rev*(y*y)*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(1.0/2.0))/R+(J*J)*1.0/(R*R)*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*((R*R)*4.0+x*x-y*y)*(1.0/8.0)+(J*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((R*R)*4.0+x*x-y*y)*((J*J*J)*1.0/(R*R*R*R)*rev*x*(y*y)*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*x*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*x*(y*y)*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z))*(1.0/4.0))/(R*(R+z))-((J*J)*1.0/(R*R)*rev*y*z*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(R*z*2.0-J*(y*y)+(R*R)*2.0)*(3.9E1/2.0E1))/V-((J*J)*1.0/(R*R*R*R)*rev*x*(y*y)*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/1.6E2))/V)*(9.0/2.0E1))/(R*(R+z)))-(sin((R*V+J*x*z+J*y*z)/(R*V))+3.0)*(((J*J)*rev*(y*y)*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(3.9E1/5.0))/(R*V)+(J*J*J)*1.0/(R*R)*1.0/(V*V)*rev*(y*y*y)*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,3.0)*pow(R+z*2.0,2.0)*(1.521E3/1.0E2)-(J*J*J*J*J*J*J)*1.0/(R*R*R*R*R*R*R*R*R)*(x*x)*(y*y)*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,3.0)*pow((R*R)*-4.0+x*x+y*y,2.0)*((R*R)*4.0+x*x-y*y)*(8.1E1/2.56E2)+((J*J)*1.0/(R*R*R)*rev*x*y*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V-(J*J*J)*1.0/(R*R*R*R*R)*1.0/(V*V)*(rev*rev)*x*y*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,3.0)*pow(R+z*2.0,2.0)*pow((R*R)*-4.0+x*x+y*y,2.0)*(7.29E2/3.2E3)-(J*J*J*J)*1.0/(R*R*R*R*R)*rev*x*y*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,4.0)*(R*2.0+z*2.0)*((R*R)*-4.0+x*x+y*y)*((R*R)*4.0+x*x-y*y)*(9.0/3.2E1))-((x*((R*R)*1.0/(V*V)*(sin((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)-2.0)*((J*y*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*(-1.0/2.0))/(R*(R+z))-(J*x*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*(1.0/2.0))/(R*(R+z))+(J*J)*1.0/(R*R)*y*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*((R*R)*4.0-x*x+y*y)*(1.0/8.0)+(J*J)*1.0/(R*R)*x*(y*y)*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*(1.0/4.0)+(J*x*y*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((J*J*J)*1.0/(R*R*R*R)*rev*x*(y*y)*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*x*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*x*(y*y)*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z))*(1.0/2.0))/(R*(R+z))+((J*J)*rev*x*y*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(1.0/2.0))/R+((J*J)*1.0/(R*R)*rev*x*z*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(R*z*2.0-J*(y*y)+(R*R)*2.0)*(3.9E1/2.0E1))/V-((J*J)*1.0/(R*R*R*R)*rev*y*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*((R*R)*4.0-x*x+y*y)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/3.2E2))/V)-(J*1.0/(V*V*V)*y*cos((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)*((J*rev*x*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V)))/(R+z)+(J*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*((R*R)*4.0-x*x+y*y)*(1.0/4.0))/(R*(R+z))+(J*x*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*(1.0/2.0))/(R*(R+z)))*(R*rev*(z*z)*4.0+(R*R)*rev*z*4.0+J*(R*R)*rev*z*4.0-J*rev*(x*x)*z-J*rev*(y*y)*z+J*R*V*x*4.0)*(9.0/2.0E1))/(R*(R+z)))-y*((R*R)*1.0/(V*V)*(sin((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)-2.0)*(-(J*rev*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V)))/(R+z)-(J*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*(1.0/2.0))/(R*(R+z))+(J*x*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*(1.0/2.0))/(R*(R+z))+(J*J)*1.0/(R*R)*x*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*((R*R)*4.0-x*x+y*y)*(1.0/8.0)+(J*J)*1.0/(R*R)*(x*x)*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*(1.0/4.0)+((J*J)*rev*(x*x)*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(1.0/2.0))/R+(J*x*y*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((J*J*J)*1.0/(R*R*R*R)*rev*(x*x)*y*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*(x*x)*y*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z))*(1.0/2.0))/(R*(R+z))-((J*J*J)*1.0/(R*R)*rev*(x*x)*y*z*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(3.9E1/2.0E1))/V-((J*J)*1.0/(R*R*R*R)*rev*x*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*((R*R)*4.0-x*x+y*y)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/3.2E2))/V)+(R*R)*1.0/(V*V)*cos((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)*((J*rev*x*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V)))/(R+z)+(J*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*((R*R)*4.0-x*x+y*y)*(1.0/4.0))/(R*(R+z))+(J*x*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*(1.0/2.0))/(R*(R+z)))*((J*(1.8E1/5.0))/R-((J*J)*1.0/(R*R)*(x*x)*(9.0/5.0))/(R+z)-(J*1.0/(R*R)*rev*x*z*(9.0/5.0))/V+((J*J)*1.0/(R*R*R)*rev*x*z*((R*R)*-4.0+x*x+y*y)*(9.0/2.0E1))/(V*(R+z)))))/R-(R*R)*1.0/(V*V)*(sin((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)-2.0)*((J*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)))/(R+z)-(J*y*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((J*J*J)*1.0/(R*R*R*R)*rev*x*(y*y)*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*x*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*x*(y*y)*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z)))/(R+z)-((J*J)*(y*y)*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*(1.0/2.0))/R+((J*J)*x*y*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(1.0/2.0))/R-(J*rev*y*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*(1.0/2.0))/(R*(R+z))+(J*J)*1.0/(R*R)*rev*y*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*((R*R)*-4.0+x*x+y*y)*(1.0/8.0)+((J*J)*1.0/(R*R*R)*rev*z*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(R*z*2.0-J*(y*y)+(R*R)*2.0)*((R*R)*-4.0+x*x+y*y)*(3.9E1/8.0E1))/V-((J*J)*1.0/(R*R*R)*rev*x*y*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/8.0E1))/V)*2.0+J*1.0/(V*V)*y*cos((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)*((J*rev*x*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*(1.0/2.0))/(R+z)+(J*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*((R*R)*4.0-x*x+y*y)*(1.0/8.0))/(R*(R+z))+(J*x*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*(1.0/4.0))/(R*(R+z)))*(1.8E1/5.0)+(J*1.0/(V*V*V)*y*cos((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)*(-(J*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)))/(R+z)+(J*x*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0))/(R+z)+(J*rev*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*((R*R)*-4.0+x*x+y*y)*(1.0/4.0))/(R*(R+z)))*(R*rev*(z*z)*4.0+(R*R)*rev*z*4.0+J*(R*R)*rev*z*4.0-J*rev*(x*x)*z-J*rev*(y*y)*z+J*R*V*x*4.0)*(9.0/2.0E1))/(R*(R+z)))/(J*(R+z))-((sin((R*V+J*x*z+J*y*z)/(R*V))*2.0+6.0)*(((J*J)*rev*(y*y)*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(R+z*2.0)*(3.9E1/1.0E1))/(R*V)-(J*J*J*J)*1.0/(R*R*R*R*R)*rev*x*y*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*((R*R)*-4.0+x*x+y*y)*((R*R)*4.0+x*x-y*y)*(9.0/3.2E1)+((J*J)*1.0/(R*R*R)*rev*x*y*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(R+z*2.0)*((R*R)*-4.0+x*x+y*y)*(2.7E1/8.0E1))/V))/(R+z)-(R*1.0/(V*V)*(sin((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)-2.0)*((J*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((R*R)*4.0+x*x-y*y)*(1.0/4.0))/(R*(R+z))-(J*rev*y*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V)))/(R+z)+(J*x*y*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*(1.0/2.0))/(R*(R+z))))/(J*(R+z))-(J*cos((R*V+J*x*z+J*y*z)/(R*V))*(x+y)*(R+z*2.0)*(((J*J)*rev*(y*y)*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(R+z*2.0)*(3.9E1/1.0E1))/(R*V)-(J*J*J*J)*1.0/(R*R*R*R*R)*rev*x*y*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*((R*R)*-4.0+x*x+y*y)*((R*R)*4.0+x*x-y*y)*(9.0/3.2E1)+((J*J)*1.0/(R*R*R)*rev*x*y*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(R+z*2.0)*((R*R)*-4.0+x*x+y*y)*(2.7E1/8.0E1))/V))/(R*V*(R+z));
        double source3 = (sin((R*V+J*x*z+J*y*z)/(R*V))+3.0)*((J*J*J*J*J*J*J)*1.0/(R*R*R*R*R*R*R*R)*(x*x)*(y*y*y)*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,3.0)*pow((R*R)*-4.0+x*x+y*y,2.0)*(8.1E1/6.4E1)-((J*J)*1.0/(R*R)*rev*y*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*((R*R)*-4.0+x*x+y*y)*(3.9E1/2.0E1))/V+((J*J)*1.0/(R*R)*rev*x*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*((R*R)*-4.0+x*x+y*y)*(2.7E1/2.0E1))/V-(J*J*J)*1.0/(R*R*R)*1.0/(V*V)*rev*(y*y)*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,3.0)*pow(R+z*2.0,2.0)*((R*R)*-4.0+x*x+y*y)*(1.521E3/4.0E2)-(J*J*J)*1.0/(R*R*R*R)*1.0/(V*V)*(rev*rev)*x*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,3.0)*pow(R+z*2.0,2.0)*pow((R*R)*-4.0+x*x+y*y,2.0)*(7.29E2/1.6E3)+(J*J*J*J)*1.0/(R*R*R*R)*rev*x*(y*y)*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,4.0)*(R*2.0+z*2.0)*((R*R)*-4.0+x*x+y*y)*(9.0/8.0))-1.0/(J*J)*((R*R)*1.0/(V*V)*(sin((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)-2.0)*((J*J*J)*1.0/(R*R)*(x*x*x)*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,3.0)*(1.0/2.0)+((J*J)*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*(1.0/2.0))/R+(J*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*pow((J*J*J)*1.0/(R*R*R*R)*rev*(x*x)*y*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*(x*x)*y*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z),2.0))/(R+z)+(J*rev*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*(1.0/2.0))/(R*(R+z))-((J*J)*x*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(3.0/2.0))/R+(J*y*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((J*J*J)*1.0/(R*R*R*R)*rev*x*y*(9.0/4.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*(x*x*x)*y*(9.0/4.0))/(R+z)+(J*J*J*J*J)*1.0/(R*R*R*R*R*R)*rev*(x*x*x)*y*1.0/pow(R+z,2.0)*((R*R)*-4.0+x*x+y*y)*(9.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(2.7E1/1.6E1))/(R+z)))/(R+z)-(J*J)*1.0/(R*R)*rev*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*((R*R)*-4.0+x*x+y*y)*(1.0/8.0)-(J*J*J)*1.0/(R*R)*(x*x)*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,3.0)*(1.0/2.0)-(J*J)*1.0/(R*R)*rev*(x*x)*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(1.0/2.0)-((J*J)*x*y*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*((J*J*J)*1.0/(R*R*R*R)*rev*(x*x)*y*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*(x*x)*y*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z)))/R+(J*J*J)*1.0/(R*R*R)*rev*(x*x)*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,3.0)*((R*R)*-4.0+x*x+y*y)*(1.0/8.0)+((J*J)*1.0/(R*R*R)*rev*x*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/4.0E1))/V+((J*J*J)*1.0/(R*R*R)*rev*(x*x)*y*z*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(3.9E1/2.0E1))/V-((J*J*J)*1.0/(R*R*R*R)*rev*(x*x*x)*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,3.0)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/8.0E1))/V+((J*J*J)*1.0/(R*R*R)*rev*y*z*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*((R*R)*-4.0+x*x+y*y)*(3.9E1/8.0E1))/V-(J*J*J)*1.0/(R*R*R*R*R*R)*1.0/(V*V)*(rev*rev)*(x*x*x)*(z*z)*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,3.0)*pow(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0,2.0)*(7.29E2/6.4E3)-(J*J*J*J*J)*1.0/(R*R*R*R*R)*1.0/(V*V)*rev*(x*x)*(y*y)*(z*z)*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,3.0)*((R*R)*-4.0+x*x+y*y)*(1.521E3/1.6E3)-((J*J*J*J)*1.0/(R*R*R*R)*rev*(x*x)*y*z*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,3.0)*((R*R)*-4.0+x*x+y*y)*(3.9E1/4.0E1))/V+((J*J)*1.0/(R*R*R*R)*rev*x*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,3.0)*(R*z-J*(x*x)+R*R)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/8.0E1))/V)+(R*R)*1.0/(V*V)*(sin((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)-2.0)*((J*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((J*J*J)*1.0/(R*R*R*R)*rev*x*(y*y)*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*x*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*x*(y*y)*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z))*2.0)/(R+z)+((J*J)*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*(3.0/2.0))/R+(J*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*pow((J*J*J)*1.0/(R*R*R*R)*rev*x*(y*y)*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*x*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*x*(y*y)*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z),2.0))/(R+z)+(J*rev*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*(1.0/2.0))/(R*(R+z))-((J*J)*x*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(1.0/2.0))/R-(J*J*J)*1.0/(R*R)*(y*y*y)*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,3.0)*(1.0/2.0)+(J*y*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((J*J*J)*1.0/(R*R*R*R)*rev*x*y*(9.0/4.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*x*(y*y*y)*(9.0/4.0))/(R+z)+(J*J*J*J*J)*1.0/(R*R*R*R*R*R)*rev*x*(y*y*y)*1.0/pow(R+z,2.0)*((R*R)*-4.0+x*x+y*y)*(9.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(2.7E1/1.6E1))/(R+z)))/(R+z)-(J*J)*1.0/(R*R)*rev*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*((R*R)*-4.0+x*x+y*y)*(1.0/8.0)+(J*J*J)*1.0/(R*R)*x*(y*y)*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,3.0)*(1.0/2.0)-((J*J)*(y*y)*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*((J*J*J)*1.0/(R*R*R*R)*rev*x*(y*y)*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*x*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*x*(y*y)*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z)))/R-(J*J)*1.0/(R*R)*rev*(y*y)*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(1.0/2.0)+(J*J*J)*1.0/(R*R*R)*rev*(y*y)*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,3.0)*((R*R)*-4.0+x*x+y*y)*(1.0/8.0)-(J*J*J)*1.0/(R*R*R*R*R)*1.0/(V*V)*rev*(z*z)*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,3.0)*pow(R*z*2.0-J*(y*y)+(R*R)*2.0,2.0)*((R*R)*-4.0+x*x+y*y)*(1.521E3/1.6E3)-((J*J)*1.0/(R*R*R)*rev*y*z*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(R*z*2.0-J*(y*y)+(R*R)*2.0)*(3.9E1/2.0E1))/V-((J*J*J)*1.0/(R*R*R*R)*rev*x*(y*y)*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,3.0)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/8.0E1))/V-(J*J*J)*1.0/(R*R*R*R*R*R)*1.0/(V*V)*(rev*rev)*x*(y*y)*(z*z)*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,3.0)*pow(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0,2.0)*(7.29E2/6.4E3)+((J*J*J)*1.0/(R*R*R*R)*rev*y*z*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,3.0)*(R*z*2.0-J*(y*y)+(R*R)*2.0)*((R*R)*-4.0+x*x+y*y)*(3.9E1/8.0E1))/V+((J*J*J)*1.0/(R*R*R*R)*rev*y*z*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,3.0)*(R*z*3.0-J*(y*y)+(R*R)*3.0)*((R*R)*-4.0+x*x+y*y)*(3.9E1/8.0E1))/V+((J*J)*1.0/(R*R*R*R)*rev*x*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,3.0)*(R*z-J*(y*y)+R*R)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/8.0E1))/V)-(R*R)*1.0/(V*V)*cos((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)*((J*(1.8E1/5.0))/R-((J*J)*1.0/(R*R)*(x*x)*(9.0/5.0))/(R+z)-(J*1.0/(R*R)*rev*x*z*(9.0/5.0))/V+((J*J)*1.0/(R*R*R)*rev*x*z*((R*R)*-4.0+x*x+y*y)*(9.0/2.0E1))/(V*(R+z)))*((J*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0))/(R+z)-((J*J)*(x*x)*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(1.0/2.0))/R+(J*y*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((J*J*J)*1.0/(R*R*R*R)*rev*(x*x)*y*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*(x*x)*y*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z)))/(R+z)+((J*J)*x*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*(1.0/2.0))/R+(J*rev*x*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*(1.0/2.0))/(R*(R+z))-(J*J)*1.0/(R*R)*rev*x*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*((R*R)*-4.0+x*x+y*y)*(1.0/8.0)+((J*J)*1.0/(R*R*R)*rev*(x*x)*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/8.0E1))/V+((J*J*J)*1.0/(R*R*R)*rev*x*y*z*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*((R*R)*-4.0+x*x+y*y)*(3.9E1/8.0E1))/V)-(J*1.0/(V*V*V)*y*cos((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)*(R*rev*(z*z)*4.0+(R*R)*rev*z*4.0+J*(R*R)*rev*z*4.0-J*rev*(x*x)*z-J*rev*(y*y)*z+J*R*V*x*4.0)*((J*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)))/(R+z)-(J*y*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((J*J*J)*1.0/(R*R*R*R)*rev*x*(y*y)*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*x*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*x*(y*y)*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z)))/(R+z)-((J*J)*(y*y)*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*(1.0/2.0))/R+((J*J)*x*y*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(1.0/2.0))/R-(J*rev*y*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*(1.0/2.0))/(R*(R+z))+(J*J)*1.0/(R*R)*rev*y*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*((R*R)*-4.0+x*x+y*y)*(1.0/8.0)+((J*J)*1.0/(R*R*R)*rev*z*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(R*z*2.0-J*(y*y)+(R*R)*2.0)*((R*R)*-4.0+x*x+y*y)*(3.9E1/8.0E1))/V-((J*J)*1.0/(R*R*R)*rev*x*y*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/8.0E1))/V)*(9.0/2.0E1))/(R*(R+z)))-(-(R*R)*1.0/(V*V)*(sin((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)-2.0)*((J*rev*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V)))/(R+z)+(J*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*(1.0/2.0))/(R*(R+z))-(J*x*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*(1.0/2.0))/(R*(R+z))+(J*J)*1.0/(R*R)*x*(y*y)*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(1.0/4.0)-((J*J)*rev*(y*y)*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(1.0/2.0))/R+(J*J)*1.0/(R*R)*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*((R*R)*4.0+x*x-y*y)*(1.0/8.0)+(J*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((R*R)*4.0+x*x-y*y)*((J*J*J)*1.0/(R*R*R*R)*rev*x*(y*y)*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*x*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*x*(y*y)*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z))*(1.0/4.0))/(R*(R+z))-((J*J)*1.0/(R*R)*rev*y*z*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(R*z*2.0-J*(y*y)+(R*R)*2.0)*(3.9E1/2.0E1))/V-((J*J)*1.0/(R*R*R*R)*rev*x*(y*y)*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/1.6E2))/V)+(R*R)*1.0/(V*V)*(sin((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)-2.0)*((J*rev*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*-2.0)/(R+z)-(J*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)))/(R*(R+z))+(J*x*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0))/(R*(R+z))+(J*J)*1.0/(R*R)*x*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*((R*R)*4.0-x*x+y*y)*(1.0/8.0)+(J*J)*1.0/(R*R)*(x*x)*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*(1.0/4.0)-(J*J)*1.0/(R*R)*x*(y*y)*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(1.0/4.0)+((J*J)*rev*(x*x)*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(1.0/2.0))/R+((J*J)*rev*(y*y)*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(1.0/2.0))/R-(J*J)*1.0/(R*R)*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*((R*R)*4.0+x*x-y*y)*(1.0/8.0)-(J*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((R*R)*4.0+x*x-y*y)*((J*J*J)*1.0/(R*R*R*R)*rev*x*(y*y)*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*x*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*x*(y*y)*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z))*(1.0/4.0))/(R*(R+z))+(J*x*y*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((J*J*J)*1.0/(R*R*R*R)*rev*(x*x)*y*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*(x*x)*y*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z))*(1.0/2.0))/(R*(R+z))-((J*J*J)*1.0/(R*R)*rev*(x*x)*y*z*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(3.9E1/2.0E1))/V+((J*J)*1.0/(R*R)*rev*y*z*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(R*z*2.0-J*(y*y)+(R*R)*2.0)*(3.9E1/2.0E1))/V+((J*J)*1.0/(R*R*R*R)*rev*x*(y*y)*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/1.6E2))/V-((J*J)*1.0/(R*R*R*R)*rev*x*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*((R*R)*4.0-x*x+y*y)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/3.2E2))/V)+(R*R)*1.0/(V*V)*(sin((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)-2.0)*(-(J*rev*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V)))/(R+z)-(J*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*(1.0/2.0))/(R*(R+z))+(J*x*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*(1.0/2.0))/(R*(R+z))+(J*J)*1.0/(R*R)*x*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*((R*R)*4.0-x*x+y*y)*(1.0/8.0)+(J*J)*1.0/(R*R)*(x*x)*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*(1.0/4.0)+((J*J)*rev*(x*x)*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(1.0/2.0))/R+(J*x*y*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((J*J*J)*1.0/(R*R*R*R)*rev*(x*x)*y*(3.0/4.0)+(J*J*J)*1.0/(R*R*R*R)*rev*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0)-((J*J*J*J)*1.0/(R*R*R*R*R)*rev*(x*x)*y*((R*R)*-4.0+x*x+y*y)*(9.0/1.6E1))/(R+z))*(1.0/2.0))/(R*(R+z))-((J*J*J)*1.0/(R*R)*rev*(x*x)*y*z*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(3.9E1/2.0E1))/V-((J*J)*1.0/(R*R*R*R)*rev*x*z*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*((R*R)*4.0-x*x+y*y)*(R*z*4.0+J*(R*R)*4.0-J*(x*x)-J*(y*y)+(R*R)*4.0)*(2.7E1/3.2E2))/V)+(R*R)*1.0/(V*V)*cos((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)*((J*rev*x*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V)))/(R+z)+(J*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*((R*R)*4.0-x*x+y*y)*(1.0/4.0))/(R*(R+z))+(J*x*y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*(1.0/2.0))/(R*(R+z)))*((J*(1.8E1/5.0))/R-((J*J)*1.0/(R*R)*(x*x)*(9.0/5.0))/(R+z)-(J*1.0/(R*R)*rev*x*z*(9.0/5.0))/V+((J*J)*1.0/(R*R*R)*rev*x*z*((R*R)*-4.0+x*x+y*y)*(9.0/2.0E1))/(V*(R+z)))+(J*1.0/(V*V*V)*y*cos((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)*((J*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*((R*R)*4.0+x*x-y*y)*(1.0/4.0))/(R*(R+z))-(J*rev*y*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V)))/(R+z)+(J*x*y*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*(1.0/2.0))/(R*(R+z)))*(R*rev*(z*z)*4.0+(R*R)*rev*z*4.0+J*(R*R)*rev*z*4.0-J*rev*(x*x)*z-J*rev*(y*y)*z+J*R*V*x*4.0)*(9.0/2.0E1))/(R*(R+z)))/(J*(R+z))+((sin((R*V+J*x*z+J*y*z)/(R*V))*2.0+6.0)*((J*J*J*J)*1.0/(R*R*R*R)*rev*x*(y*y)*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*((R*R)*-4.0+x*x+y*y)*(9.0/8.0)+((J*J)*1.0/(R*R)*rev*x*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(R+z*2.0)*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V-((J*J)*1.0/(R*R)*rev*y*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(R+z*2.0)*((R*R)*-4.0+x*x+y*y)*(3.9E1/4.0E1))/V))/(R+z)-R*1.0/(V*V)*(sin((J*1.0/(R*R)*(rev*(x*x)*z+rev*(y*y)*z-R*V*x*4.0-(R*R)*rev*z*4.0)*(9.0/1.0E1))/V)-2.0)*1.0/pow(R+z,2.0)*(y*cos((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))-x*sin((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)+R*rev*cos((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*2.0)+(J*cos((R*V+J*x*z+J*y*z)/(R*V))*(x+y)*(R+z*2.0)*((J*J*J*J)*1.0/(R*R*R*R)*rev*x*(y*y)*sin((J*J*J)*1.0/(R*R*R*R)*rev*x*y*((R*R)*-4.0+x*x+y*y)*(3.0/8.0))*1.0/pow(R+z,2.0)*((R*R)*-4.0+x*x+y*y)*(9.0/8.0)+((J*J)*1.0/(R*R)*rev*x*cos((J*1.0/(R*R)*rev*z*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V+1.0)*1.0/pow(R+z,2.0)*(R+z*2.0)*((R*R)*-4.0+x*x+y*y)*(2.7E1/4.0E1))/V-((J*J)*1.0/(R*R)*rev*y*sin((R*V+J*y*z*(3.9E1/1.0E1))/(R*V))*1.0/pow(R+z,2.0)*(R+z*2.0)*((R*R)*-4.0+x*x+y*y)*(3.9E1/4.0E1))/V))/(R*V*(R+z));
        FCT.set(i,0, 0);
        FCT.set(i,1, source1);
        FCT.set(i,2, source2);
        FCT.set(i,3, source3);
        FCT.set(i, 4, 0);

    }
}
void sq (dataCacheMap *, fullMatrix<double> &FCT, fullMatrix<double> &in) {
   for (int i=0; i<FCT.size1(); i++){
        FCT.set(i,0, in(i,0)*in(i,0));
        FCT.set(i,1, in(i,1)*in(i,1));
        FCT.set(i,2, in(i,2)*in(i,2));
        FCT.set(i,3, in(i,3)*in(i,3));
        FCT.set(i, 4, in(i,4)*in(i,4));
   }

}
void nuh (dataCacheMap *c, fullMatrix<double> &FCT, fullMatrix<double> &XYZ, fullMatrix<double> &time, fullMatrix<double> &R_) {
    double R = R_(0, 0);
    double t = time(0, 0);
    double rev = 1;
    if (c->getGroupOfElements()->getPhysicalTag() == "volume_Bottom"){
            rev = -1;
    }
    for (int i=0; i<XYZ.size1(); i++){
        double x = XYZ(i,0);
        double y = XYZ(i,1);
        double z = XYZ(i,2);
        double V = 10000;
        double nuh = (R*R)*1.0/(V*V)*(sin((x*(R+z)*(7.2E1/5.0))/((R*R)*4.0+x*x+y*y)-(rev*z*(R+z)*((R*R)*-4.0+x*x+y*y)*(1.8E1/5.0))/(R*V*((R*R)*4.0+x*x+y*y)))+2.0);
        FCT.set(i, 0, nuh);
    }
}
void nuv (dataCacheMap *c, fullMatrix<double> &FCT, fullMatrix<double> &XYZ, fullMatrix<double> &time, fullMatrix<double> &R_) {
    double R = R_(0, 0);
    double t = time(0, 0);
    double rev = 1;
    if (c->getGroupOfElements()->getPhysicalTag() == "volume_Bottom"){
            rev = -1;
    }
    for (int i=0; i<XYZ.size1(); i++){
        double x = XYZ(i,0);
        double y = XYZ(i,1);
        double z = XYZ(i,2);
        double V = 10000;
        double nuv = sin((z*((x*(R+z)*4.0)/((R*R)*4.0+x*x+y*y)+(y*(R+z)*4.0)/((R*R)*4.0+x*x+y*y)))/V+1.0)+3.0;
        FCT.set(i, 0, nuv);
    }
}
}
"""



if (Msg.GetCommRank()==0):
    functionC.buildLibrary (CCode, "ccode.so")
Msg.Barrier()

Rfunc = functionConstant([R])


analyticalSol=functionC("ccode.so","analyticalSol",5,[xyzCircle, function.getTime(), Rfunc])
analyticalSolSq=functionC("ccode.so","sq",5,[analyticalSol])
errorSol=functionC("ccode.so","error",5,[solution.getFunction(), analyticalSol])

nuh = dgDofContainer(groups, 1)
nuh.L2Projection(functionC("ccode.so","nuh",1,[xyzCircle, function.getTime(), Rfunc]))
nuv = dgDofContainer(groups, 1)
nuv.L2Projection(functionC("ccode.so","nuv",1,[xyzCircle, function.getTime(), Rfunc]))

claw.setNu(nuh.getFunction(), nuv.getFunction(), nuh.getFunctionGradient(), nuv.getFunctionGradient())

trms = claw.getActiveTerms();
trms.setAdvV(False);
trms.setAdvT(False);
trms.setPGrad(False);
trms.setVDiv(False);
trms.setCor(False);
trms.setGrav(False);
trms.setDiff(True);


def velocity3D(F, sol, stereo):
    R = 6371229
    rev = 1
    for i in range(0,sol.size1()):
        if (F.getDataCacheMap().getGroupOfElements().getPhysicalTag() == "volume_Bottom"):
            rev = -1;
        #Horizontal components
        xi =stereo(i,0)
        beta =stereo(i,1)
        gam = stereo(i,2)
        vxi = sol(i,1)
        vbeta = sol(i,2)
        vgamma = sol(i,3)
        ux = (1-(2*xi**2)/(4*R*R+xi*xi + beta*beta)) * vxi - 2*beta*xi/(4*R*R+xi*xi + beta*beta) * vbeta + 4*R*xi/(4*R*R+xi*xi + beta*beta) * vgamma
        uy = - 2*beta*xi/(4*R*R+xi*xi + beta*beta) * vxi + (1-(2*beta**2)/(4*R*R+xi*xi + beta*beta)) * vbeta + 4*R*beta/(4*R*R+xi*xi + beta*beta) * vgamma
        uz = -4*R*xi*rev/(4*R*R+xi*xi + beta*beta) * vxi - 4*R*beta*rev/(4*R*R+xi*xi + beta*beta) * vbeta - rev*((-4*R*R+xi*xi+beta*beta)/(4*R*R+xi*xi + beta*beta)) * vgamma
        #Both
        F.set(i,0,ux)
        F.set(i,1,uy)
        F.set(i,2,uz)


def cartCoord(FCT,  stereo, amplFactor) :
    rev=1
    for i in range(0,FCT.size1()):
        xi =stereo(i,0)
        beta =stereo(i,1)
        gam = stereo(i,2) * amplFactor(i,0)
        FCT.set(i,0,4.0*(R + gam)*R*xi/(4.0*R*R+xi*xi + beta*beta))
        FCT.set(i,1,4.0*(R + gam)*R*beta/(4.0*R*R+xi*xi + beta*beta))
        if (FCT.getDataCacheMap().getGroupOfElements().getPhysicalTag() == "volume_Bottom"):
            rev = -1;
        FCT.set(i,2,(R + gam) *(4.0*R*R-xi*xi-beta*beta)/(4.0*R*R+xi*xi + beta*beta) * rev)

amplFact = functionConstant(500)
cartXYZ=functionPython(3, cartCoord, [xyzCircle, amplFact])
cartVel=functionPython(3, velocity3D, [solution.getFunction(), xyzCircle])
cartAnalyticalSol=functionPython(3, velocity3D, [analyticalSol, xyzCircle])
cartErrorSol=functionPython(3, velocity3D, [errorSol, xyzCircle])

sourceTerm=functionC("ccode.so","source",5,[xyzCircle, function.getTime(), Rfunc]);
sourceDof = dgDofContainer(groups, 5)
sourceDof.L2Projection(sourceTerm)
analyticalDof = dgDofContainer(groups, 5)
analyticalDof.L2Projection(analyticalSol)

claw.setSource("", sourceDof.getFunction());

cartSource=functionPython(3, velocity3D, [sourceDof.getFunction(), xyzCircle])

initF=functionConstant([0, 0, 0, 0, 0])
solution.interpolate(initF)

claw.setCoordinatesFunction(xyzCircleInteg)

rhoHs = dgDofContainer(groups, 1)
rhoHs.interpolate(functionConstant([1]))
rhoThetaHs = dgDofContainer(groups, 1)
rhoThetaHs.interpolate(functionConstant([1]))

claw.setHydrostaticState(rhoHs,rhoThetaHs)
claw.setPhysicalConstants(1,1,1,1,R)

boundaryWall = claw.newBoundaryWall()

toReplace = [solution.getFunction(), solution.getFunctionGradient()]
replaceBy = [analyticalDof.getFunction(), analyticalDof.getFunctionGradient()]
boundaryCnd = claw.newOutsideValueBoundaryGeneric("",toReplace,replaceBy)

claw.addBoundaryCondition('top_Top', boundaryCnd)
claw.addBoundaryCondition('bottom_Top', boundaryCnd)
claw.addBoundaryCondition('top_Bottom', boundaryCnd)
claw.addBoundaryCondition('bottom_Bottom', boundaryCnd)

def exportFct(FCT,sol,vel3d, analyticalSol, errorSol, source, nuh, nuv):
    for i in range(0,FCT.size1()):
        #uvw
        FCT.set(i,0,vel3d(i,0))
        FCT.set(i,1,vel3d(i,1))
        FCT.set(i,2,vel3d(i,2))
        FCT.set(i,3,analyticalSol(i,0))
        FCT.set(i,4,analyticalSol(i,1))
        FCT.set(i,5,analyticalSol(i,2))
        FCT.set(i,6,errorSol(i,0))
        FCT.set(i,7,errorSol(i,1))
        FCT.set(i,8,errorSol(i,2))
        FCT.set(i,9,source(i,0))
        FCT.set(i,10,source(i,1))
        FCT.set(i,11,source(i,2))
        FCT.set(i,12,nuh(i,0))
        FCT.set(i,13,nuv(i,0))

nCompExp=[3,3,3,3,1,1]
namesExp=["uvw","analytical","error","source","nuh","nuv"]
exportFunction=functionPython(sum(nCompExp), exportFct, [solution.getFunction(),cartVel, cartAnalyticalSol, cartErrorSol, cartSource, nuh.getFunction(), nuv.getFunction()])

petscIm = linearSystemPETScBlockDouble()
petscIm.setParameter("petscOptions",  "-ksp_atol 0 -ksp_rtol 1e-6 -ksp_monitor")
dofIm = dgDofManager.newDGBlock(groups, claw.getNbFields(), petscIm)
timeIter = dgDIRK(claw, dofIm, 2) 
timeIter.getNewton().setAtol(0) #ATol
timeIter.getNewton().setRtol(1.e-5) #Rtol
timeIter.getNewton().setVerb(1)     #Verbosity

dt = 5000000
if (Msg.GetCommRank()==0):
    print ("Time step:",dt)
nbSteps = int(ceil(Tf/dt))

integratorError = dgFunctionIntegrator(groups, errorSol)
intErr = fullMatrixDouble(5,1)
integratorAnalyticSq = dgFunctionIntegrator(groups, analyticalSolSq)
intAnalyticSq = fullMatrixDouble(5,1)

claw.setIsLinear(True)
claw.setIsConstantJac(True)

t=Ti
n_export=0
start = time.clock()
outFile = open('ErrorVsTime','w')
outFile.close();
for i in range(0,nbSteps):
    norm = timeIter.iterate (solution, dt, t)
    t=t+dt
    if (Msg.GetCommRank()==0):
        sys.stdout.write('.')
        sys.stdout.flush()
    integratorError.compute(intErr)
    integratorAnalyticSq.compute(intAnalyticSq)
    if (Msg.GetCommRank()==0):
        outFile = open('ErrorVsTime','a')
        errRhoU=intErr(1,0)/intAnalyticSq(1,0)
        errRhoV=intErr(2,0)/intAnalyticSq(2,0)
        errRhoW=intErr(3,0)/intAnalyticSq(3,0)
        print ("\nError on rhoU:",errRhoU,"rhoV:", errRhoV,"rhoW:",errRhoW)
        outFile.write("%s %s %s %s\n" % (t/3600, errRhoU, errRhoV, errRhoW))
        outFile.close()

print ('')    
elapsed = (time.clock() - start)
if (Msg.GetCommRank()==0):
    print ('Time elapsed: ',elapsed,' s')
if (errRhoU > 1e-4 or errRhoV > 2e-4 or errRhoW > 1e-4):
    Msg.Fatal("Error too large")
else:
    Msg.Info("Test successfully passed")
    Msg.Exit(0)

