from gmshPartition import *
from dgpy import *
from extrude import *
from rotateZtoY import *
from gmshpy import *

name='aquaplanet'
nbPart = int(sys.argv[1])

nbLayers = 3
H = 10000.

res=H/nbLayers

def getLayersSigma (element, vertex) :
  z=0
  zTab=[z]
  while (-z)<H:
    z=z-res
    zTab.append(z)
  return zTab

if nbPart>1:
  partStr='_part_%i' % nbPart
else:
  partStr=''


model = GModel()
model.load(name+".msh")
pOpt = meshPartitionOptions()
pOpt.setNumOfPartitions(nbPart)
PartitionMesh(model,  pOpt)   
model.save(name+"_part.msh")

extrude(mesh(name+"_part.msh"), getLayersSigma).write(name+"_part_"+str(nbPart)+"_3d.msh")
Msg.Exit(0) 
