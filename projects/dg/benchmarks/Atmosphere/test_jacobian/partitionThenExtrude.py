from partition1d import *
from dgpy import *
from extrude import *
from rotateZtoY import *

name='aquaplanet'
nbPart = int(sys.argv[1])

nbLayers = 1
H = 3.

res=H/nbLayers

def getLayersSigma (element, vertex) :
  z=0
  zTab=[z]
  for i in range(nbLayers):
    z=z-res
    zTab.append(z)
  return zTab

if nbPart>1:
  partStr='_part_%i' % nbPart
else:
  partStr=''
partition1d(name+'.msh', name + partStr + '.msh', nbPart)
extrude(name + partStr + '.msh', getLayersSigma).save(name + partStr + '_3d.msh')
Msg.Exit(0)
