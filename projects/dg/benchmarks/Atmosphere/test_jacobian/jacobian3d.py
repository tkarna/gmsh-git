from extrude import *
from dgpy import *
from math import *
import time, os, sys
import gmshPartition


order=1
dimension=3
R=2
H=3

model = GModel()
name='aquaplanet_3d.msh'
model.load(name)

groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag();
circleTransform = dgCircleTransform(R, groups)
groups._mesh.setSpaceTransform(circleTransform, groups)

dof = dgDofContainer(groups, 1) 

XYZ = groups.getFunctionCoordinates()

def toIntegrate(FCT, XYZ) :
  for i in range (0,XYZ.size1()) :
    FCT.set(i,0,1)

def toIntegrate2(FCT, XYZ) :
  for i in range (0,XYZ.size1()) :
    FCT.set(i,0,sqrt(XYZ(i,0)**2+XYZ(i,1)**2))

xyzCircle = transformToCircle(circleTransform)

toInteg=functionPython(1, toIntegrate, [xyzCircle])
integrator = dgFunctionIntegratorInterface(groups, toInteg)
integral = fullMatrixDouble(1,1)
integrator.compute("Seam",integral)

toInteg2=functionPython(1, toIntegrate2, [xyzCircle])
dof.interpolate(toInteg2)
integrator2 = dgFunctionIntegrator(groups, dof.getFunction())
integral2 = fullMatrixDouble(1,1)
integrator2.compute(integral2)

integrator3 = dgFunctionIntegrator(groups, toInteg)
integral3 = fullMatrixDouble(1,1)
integrator3.compute(integral3)


def exportFct(FCT,inFct,XYZ,xyzCircle):
    for i in range(0,FCT.size1()):
      FCT.set(i,0,inFct(i,0))
      FCT.set(i,1,XYZ(i,0))
      FCT.set(i,2,XYZ(i,1))
      FCT.set(i,3,XYZ(i,2))
      FCT.set(i,4,xyzCircle(i,0))
      FCT.set(i,5,xyzCircle(i,1))
      FCT.set(i,6,xyzCircle(i,2))

nCompExp=[1,3,3]
namesExp=["function","xyz","xyzCircle"]
exportFunction=functionPython(sum(nCompExp), exportFct, [toInteg2,XYZ, xyzCircle])
dof.exportFunctionVtk(exportFunction,'output', 0, 0,"solution",nCompExp,namesExp, xyzCircle)

print "Perimeter of exterior area of the cylinder of radius",R,"and depth",H,"is",integral(0,0)
print "Analytical value: ",pi*2*R*H
print "Integral of function on cylinder of radius",R,"and depth",H,"is",integral2(0,0)/2
print "Analytical value: ",2*pi*R*R*R/3*H
print "Volume of the cylinder of radius",R,"and depth",H,"is",integral3(0,0)/2
print "Analytical value: ",pi*R*R*H
