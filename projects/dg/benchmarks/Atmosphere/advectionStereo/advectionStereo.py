from dgpy import *
from math import *
import time, os, sys
import gmshPartition

###########################
#### Begin and end time ###
Ti=0.0
Tf=10000*96*3600
###########################

R = 6371229

order=2
dimension=3


integOrder=2*order+1+4
dataCacheMap.setDefaultIntegrationOrder(integOrder)

model = GModel()

name='aquaplanet_part'
partStr='_%i' % Msg.GetCommSize()
model.load(name + partStr + '_3d.msh')


groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag();

circleTransform = dgCircleTransform(groups)

#st = dgSpaceTransformSpherical(R, groups)
#groups._mesh.setSpaceTransform(st, groups)
groups._mesh.setSpaceTransform(circleTransform, groups)

groupsH = dgGroupCollection.newByTag(model, dimension-1, order, ["bottom_Top","bottom_Bottom"])
extrusion = dgExtrusion(groups, groupsH, ["bottom_Top","bottom_Bottom"])
groups.splitFaceGroupsByOrientation(extrusion, ["bottom_Top","bottom_Bottom","top_Top", "top_Bottom"]) 

claw = dgEulerAtmLaw(dimension)
solution = dgDofContainer(groups, claw.getNbFields())

#claw.setFilterMode(FILTER_LINEARVERTICAL);

XYZ = groups.getFunctionCoordinates();
xyzCircle = transformToCircle(circleTransform)

xyzCircleInteg = functionPrecomputed(groups, integOrder, 3)
xyzCircleInteg.compute(xyzCircle)

CCode = """
#include "fullMatrix.h"
#include "function.h"
#include "math.h"
#include "stdio.h"
#include "dgGroupOfElements.h"

extern "C" {
void analyticalSol (dataCacheMap *c, fullMatrix<double> &FCT, fullMatrix<double> &XYZ, fullMatrix<double> &time, fullMatrix<double> &R_) {
    double R = R_(0, 0);
    double t = time(0, 0);
    double rev = 1;
    if (c->getGroupOfElements()->getPhysicalTag() == "volume_Bottom"){
            rev = -1;
    }
    for (int i=0; i<XYZ.size1(); i++){
        double x = XYZ(i,0);
        double y = XYZ(i,1);
        double z = XYZ(i,2);
        double V = 10000;
        double J = 4*R*(R+z)/(x*x+y*y+4*R*R);
        double u = (y*cos((3.141592653589793*sqrt(x*x+y*y)*(1.0/4.0))/R)*-1.0E2)/R;

        double v =(x*cos((3.141592653589793*sqrt(x*x+y*y)*(1.0/4.0))/R)*1.0E2)/R;

        double w =0;

        FCT.set(i,0,0);
        FCT.set(i,1,u);
        FCT.set(i,2,v);
        FCT.set(i,3,w);
        FCT.set(i,4,0);

    }
}

void error (dataCacheMap *, fullMatrix<double> &FCT, fullMatrix<double> &sol, fullMatrix<double> &analyticalSol) {
    for (int i=0; i<FCT.size1(); i++){
        FCT.set(i,0,0);
        FCT.set(i,1,pow(sol(i,1)-analyticalSol(i,1),2));
        FCT.set(i,2,pow(sol(i,2)-analyticalSol(i,2),2));
        FCT.set(i,3,pow(sol(i,3)-analyticalSol(i,3),2));
        FCT.set(i,4,0);
    }
}
void source (dataCacheMap *c, fullMatrix<double> &FCT, fullMatrix<double> &XYZ, fullMatrix<double> &time, fullMatrix<double> &R_) {
    double R = R_(0, 0);
    double t = time(0, 0);
    double rev = 1;
    if (c->getGroupOfElements()->getPhysicalTag() == "volume_Bottom"){
            rev = -1;
    }
    for (int i=0; i<XYZ.size1(); i++){
        double x = XYZ(i,0);
        double y = XYZ(i,1);
        double z = XYZ(i,2);
        double J = 4 * R * (R + z) / (4 * R * R + x * x + y * y);
        double V = 10000;
        double source1 =1.0/(R*R)*x*pow(cos((3.141592653589793*(rev*rev)*pow(x*x+y*y,(rev*rev)*(1.0/2.0))*(1.0/4.0))/R),2.0)*-1.0E4;

        double source2 =1.0/(R*R)*y*pow(cos((3.141592653589793*(rev*rev)*pow(x*x+y*y,(rev*rev)*(1.0/2.0))*(1.0/4.0))/R),2.0)*-1.0E4;

        double source3 =0;

        FCT.set(i,0, 0);
        FCT.set(i,1, source1);
        FCT.set(i,2, source2);
        FCT.set(i,3, source3);
        FCT.set(i, 4, 0);

    }
}
void sq (dataCacheMap *, fullMatrix<double> &FCT, fullMatrix<double> &in) {
   for (int i=0; i<FCT.size1(); i++){
        FCT.set(i,0, in(i,0)*in(i,0));
        FCT.set(i,1, in(i,1)*in(i,1));
        FCT.set(i,2, in(i,2)*in(i,2));
        FCT.set(i,3, in(i,3)*in(i,3));
        FCT.set(i, 4, in(i,4)*in(i,4));
   }

}
}
"""



if (Msg.GetCommRank()==0):
    functionC.buildLibrary (CCode, "ccode.so")
Msg.Barrier()

Rfunc = functionConstant([R])


analyticalSol=functionC("ccode.so","analyticalSol",5,[xyzCircle, function.getTime(), Rfunc])
analyticalSolSq=functionC("ccode.so","sq",5,[analyticalSol])
errorSol=functionC("ccode.so","error",5,[solution.getFunction(), analyticalSol])

trms = claw.getActiveTerms();
trms.setAdvV(True);
trms.setAdvT(False);
trms.setPGrad(False);
trms.setVDiv(False);
trms.setCor(False);
trms.setGrav(False);
trms.setDiff(False);


#claw.setNu(nuhC, nuvC, grad0, grad0)

def velocity3D(F, sol, stereo):
    R = 6371229
    rev = 1
    for i in range(0,sol.size1()):
        if (F.getDataCacheMap().getGroupOfElements().getPhysicalTag() == "volume_Bottom"):
            rev = -1;
        #Horizontal components
        xi =stereo(i,0)
        beta =stereo(i,1)
        gam = stereo(i,2)
        vxi = sol(i,1)
        vbeta = sol(i,2)
        vgamma = sol(i,3)
        ux = (1-(2*xi**2)/(4*R*R+xi*xi + beta*beta)) * vxi - 2*beta*xi/(4*R*R+xi*xi + beta*beta) * vbeta + 4*R*xi/(4*R*R+xi*xi + beta*beta) * vgamma
        uy = - 2*beta*xi/(4*R*R+xi*xi + beta*beta) * vxi + (1-(2*beta**2)/(4*R*R+xi*xi + beta*beta)) * vbeta + 4*R*beta/(4*R*R+xi*xi + beta*beta) * vgamma
        uz = -4*R*xi*rev/(4*R*R+xi*xi + beta*beta) * vxi - 4*R*beta*rev/(4*R*R+xi*xi + beta*beta) * vbeta - rev*((-4*R*R+xi*xi+beta*beta)/(4*R*R+xi*xi + beta*beta)) * vgamma
        #Both
        F.set(i,0,ux)
        F.set(i,1,uy)
        F.set(i,2,uz)

        F.set(i,0,vxi)
        F.set(i,1,vbeta)
        F.set(i,2,vgamma)


def cartCoord(FCT,  stereo, amplFactor) :
    rev=1
    for i in range(0,FCT.size1()):
        xi =stereo(i,0)
        beta =stereo(i,1)
        gam = stereo(i,2) * amplFactor(i,0)
        FCT.set(i,0,4.0*(R + gam)*R*xi/(4.0*R*R+xi*xi + beta*beta))
        FCT.set(i,1,4.0*(R + gam)*R*beta/(4.0*R*R+xi*xi + beta*beta))
        if (FCT.getDataCacheMap().getGroupOfElements().getPhysicalTag() == "volume_Bottom"):
            rev = -1;
        FCT.set(i,2,(R + gam) *(4.0*R*R-xi*xi-beta*beta)/(4.0*R*R+xi*xi + beta*beta) * rev)

        FCT.set(i,0,xi)
        FCT.set(i,1,beta)
        FCT.set(i,2,gam)

amplFact = functionConstant(500)
cartXYZ=functionPython(3, cartCoord, [xyzCircle, amplFact])
cartVel=functionPython(3, velocity3D, [solution.getFunction(), xyzCircle])
cartAnalyticalSol=functionPython(3, velocity3D, [analyticalSol, xyzCircle])
cartErrorSol=functionPython(3, velocity3D, [errorSol, xyzCircle])

sourceTerm=functionC("ccode.so","source",5,[xyzCircle, function.getTime(), Rfunc]);
sourceDof = dgDofContainer(groups, 5)
sourceDof.L2Projection(sourceTerm)
analyticalDof = dgDofContainer(groups, 5)
analyticalDof.L2Projection(analyticalSol)

solution.L2Projection(analyticalSol)

claw.setSource("", sourceDof.getFunction());

cartSource=functionPython(3, velocity3D, [sourceDof.getFunction(), xyzCircle])

#initF=functionConstant([0, 0, 0, 0, 0])
#solution.interpolate(initF)

claw.setCoordinatesFunction(xyzCircleInteg)

rhoHs = dgDofContainer(groups, 1)
rhoHs.interpolate(functionConstant([1]))
rhoThetaHs = dgDofContainer(groups, 1)
rhoThetaHs.interpolate(functionConstant([1]))

claw.setHydrostaticState(rhoHs,rhoThetaHs)
claw.setPhysicalConstants(1,1,1,1,R)

boundaryWall = claw.newBoundaryWall()

toReplace = [solution.getFunction(), solution.getFunctionGradient()]
replaceBy = [analyticalSol, analyticalDof.getFunctionGradient()]
boundaryCnd = claw.newOutsideValueBoundaryGeneric("",toReplace,replaceBy)

claw.addBoundaryCondition('top_Top', boundaryCnd)
claw.addBoundaryCondition('bottom_Top', boundaryCnd)
claw.addBoundaryCondition('top_Bottom', boundaryCnd)
claw.addBoundaryCondition('bottom_Bottom', boundaryCnd)
claw.addBoundaryCondition('Seam', boundaryCnd)

def exportFct(FCT,sol,vel3d, analyticalSol, errorSol, source, rhoHs):
    for i in range(0,FCT.size1()):
        #uvw
        FCT.set(i,0,vel3d(i,0))
        FCT.set(i,1,vel3d(i,1))
        FCT.set(i,2,vel3d(i,2))
        FCT.set(i,3,analyticalSol(i,0))
        FCT.set(i,4,analyticalSol(i,1))
        FCT.set(i,5,analyticalSol(i,2))
        FCT.set(i,6,errorSol(i,0))
        FCT.set(i,7,errorSol(i,1))
        FCT.set(i,8,errorSol(i,2))
        FCT.set(i,9,source(i,0))
        FCT.set(i,10,source(i,1))
        FCT.set(i,11,source(i,2))
        FCT.set(i,12,sol(i,0)+rhoHs(i,0))

nCompExp=[3,3,3,3,1]
namesExp=["uvw","analytical","error","source","rho"]
exportFunction=functionPython(sum(nCompExp), exportFct, [solution.getFunction(),cartVel, cartAnalyticalSol, cartErrorSol, cartSource, rhoHs.getFunction()])

#petscIm = dgLinearSystemExtrusion(claw, groups, extrusion)
#petscIm = linearSystemPETScBlockDouble()
#petscIm.setParameter("petscOptions",  "-ksp_atol 0 -ksp_rtol 1e-8 -ksp_monitor")
#dofIm = dgDofManager.newDGBlock(groups, claw.getNbFields(), petscIm)
#timeIter = dgIMEXRK(claw, dofIm, 2) 
timeIter = dgERK(claw, None, DG_ERK_22)
#timeIter = dgDIRK(claw, dofIm, 2) 

#dt = 1
dt=claw.getMinOfTimeSteps(solution, extrusion)*0.4
#dt = 100
if (Msg.GetCommRank()==0):
    print ("Time step:",dt)
nbSteps = int(ceil(Tf/dt))

integratorError = dgFunctionIntegrator(groups, errorSol)
intErr = fullMatrixDouble(5,1)
integratorAnalyticSq = dgFunctionIntegrator(groups, analyticalSolSq)
intAnalyticSq = fullMatrixDouble(5,1)


solution.exportFunctionVtk(exportFunction,'output', 0, 0,"solution",nCompExp,namesExp,cartXYZ)

t=Ti
n_export=0
start = time.clock()
outFile = open('ErrorVsTime','w')
outFile.close();
for i in range(0,nbSteps):
    dt=claw.getMinOfTimeSteps(solution, extrusion)*0.4
    if (Msg.GetCommRank()==0):
        print ("Time step:",dt)

    if (i%10 == 0 or i==nbSteps-1):
        integratorError.compute(intErr)
        integratorAnalyticSq.compute(intAnalyticSq)
        if (Msg.GetCommRank()==0):
            outFile = open('ErrorVsTime','a')
            errRhoU=intErr(1,0)/intAnalyticSq(1,0)
            errRhoV=intErr(2,0)/intAnalyticSq(2,0)
            errRhoW=0#intErr(3,0)/intAnalyticSq(3,0)
            print ("\nError on rhoU:",errRhoU,"rhoV:", errRhoV,"rhoW:",errRhoW)
            outFile.write("%s %s %s %s\n" % (t/3600, errRhoU, errRhoV, errRhoW))
            outFile.close()

        solution.exportFunctionVtk(exportFunction,'output', t, i,"solution",nCompExp,namesExp,cartXYZ)
        if (Msg.GetCommRank()==0):
            print ('\nWriting output',n_export,'at time',t,'and step',i,'over',nbSteps)
            elapsed = (time.clock() - start)
            print ('Time elapsed: ',elapsed,' s')
        n_export=n_export+1
    norm = timeIter.iterate (solution, dt, t)
    t=t+dt
#    dt = min(100,claw.getMinOfTimeSteps(solution, extrusion))
#    if (Msg.GetCommRank()==0):
#        print ("Time step:",dt)
    if (Msg.GetCommRank()==0):
        sys.stdout.write('.')
        sys.stdout.flush()
print ('')    
elapsed = (time.clock() - start)
if (Msg.GetCommRank()==0):
    print ('Time elapsed: ',elapsed,' s')
Msg.Exit(0)

