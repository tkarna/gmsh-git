from dgpy import *
from math import *
import time, os, sys
import gmshPartition
#from matplotlib import pyplot as plt

#### Physical constants ###
gamma=1.4
Rd=287.0
p0=1.0e5
g=9.80616
Cv=Rd/(gamma-1.0)
Cp=Rd*(1.0+1.0/(gamma-1.0))
###########################
#### Begin and end time ###
# Ti: subwindow6 Tf: subwindow8
Ti=300.0 
Tf=400.0
###########################

order=5
dimension=2

model = GModel()
name='line'
if Msg.GetCommSize()>1:
  partStr='_part_%i' % Msg.GetCommSize()
else:
  partStr=''
model.load(name + partStr + '_2d_XY.msh')

#############  for convergence plot################
###################################################
errp=[]
erru=[]
errv=[]
errthetap=[]
errall=[]
stepsizes=[]

#if (Msg.GetCommRank()==0):
#  plt.ion()
#  fig = plt.figure()
#  pltP,=plt.plot(stepsizes,errp,label="rhop")
#  pltU,=plt.plot(stepsizes,erru,label="rhou")
#  pltV,=plt.plot(stepsizes,errv,label="rhov")
#  pltThetap,=plt.plot(stepsizes,errthetap,label="rhothetap")
#  pltAll,=plt.plot(stepsizes,errall,label="all")
#  plt.legend(loc='upper right')
#  axes=plt.gca()
#  axes.set_ylim(0,1)

groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag();

claw = dgEulerAtmLaw(dimension)
####Load reference solution from file###
reloaded =dgDofContainer(groups,claw.getNbFields())
#reloaded.readMsh("refSolERK/refSolERK.idx")
reloaded.readMsh("refSolERK/subwindow8/subwindow8.idx")
solution = dgDofContainer(groups, claw.getNbFields())

XYZ = groups.getFunctionCoordinates();
def hydrostaticState(z) :
    thetaHs=300.0
    exnerHs=1.0-g/(Cp*thetaHs)*z
    rhoHs=p0/(Rd*thetaHs)*exnerHs**(Cv/Rd)
    return rhoHs,exnerHs,thetaHs

def initialCondition(FCT,  XYZ) :
    thetac=0.5
    xc=500.0
    zc=350.0

 #   xc=0
 #   zc=0

    rc=250.0
    for i in range (0,XYZ.size1()) :
        x=XYZ(i,0)
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        r=sqrt((x-xc)**2+(z-zc)**2)
        if (r>rc):
            thetaPert=thetaHs
        else:
            thetaPert=thetaHs+thetac/2.0*(1.0+cos(pi*r/rc))
        rhoPert=p0/(Rd*thetaPert)*exnerHs**(Cv/Rd)
        FCT.set(i,0,rhoPert-rhoHs)
        FCT.set(i,1,0.0)
        FCT.set(i,2,0.0)
        FCT.set(i,3,rhoPert*thetaPert-rhoHs*thetaHs)

def rhoHydrostatic(FCT,  XYZ) :
    for i in range (0,XYZ.size1()) :
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        FCT.set(i,0,rhoHs)

def rhoThetaHydrostatic(FCT,  XYZ) :
    for i in range (0,XYZ.size1()) :
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        FCT.set(i,0,rhoHs*thetaHs)
        
def getVelocity(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        rho=rhoHs+sol(i,0)
        FCT.set(i,0,sol(i,1)/rho)
        FCT.set(i,1,sol(i,2)/rho)
        FCT.set(i,2,0)

def getRhop(FCT, sol) :
    for i in range (0,sol.size1()) :
        FCT.set(i,0,sol(i,0))

def getpp(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        rho=rhoHs+sol(i,0)
        rhoTheta = rhoHs*thetaHs+sol(i,3)
        pHs=p0*(rhoHs*thetaHs*Rd/p0)**gamma
        p=p0*(rhoTheta*Rd/p0)**gamma
        FCT.set(i,0,p-pHs)

def getpHs(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        pHs=p0*(rhoHs*thetaHs*Rd/p0)**gamma
        FCT.set(i,0,pHs)

def getp(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        rhoTheta = rhoHs*thetaHs+sol(i,3)
        p=p0*(rhoTheta*Rd/p0)**gamma
        FCT.set(i,0,p)

def getThetap(FCT, sol, XYZ) :
    for i in range (0,sol.size1()) :
        z=XYZ(i,1)
        rhoHs,exnerHs,thetaHs = hydrostaticState(z)
        rho=rhoHs+sol(i,0)
        FCT.set(i,0,(sol(i,3)+(rhoHs*thetaHs))/rho-thetaHs)

uv=functionPython(3, getVelocity, [solution.getFunction(), XYZ])
rhop=functionPython(1, getRhop, [solution.getFunction()])
pp=functionPython(1, getpp, [solution.getFunction(), XYZ])
thetap=functionPython(1, getThetap, [solution.getFunction(), XYZ])

#initF=functionPython(4, initialCondition, [XYZ])
#solution.interpolate(initF)

####Load initial solution from file###
readInit =dgDofContainer(groups,claw.getNbFields())
readInit.readMsh("refSolERK/subwindow6/subwindow6.idx")
solution.copy(readInit)

rhoHs = dgDofContainer(groups, 1)
rhoHs.interpolate(functionPython(1, rhoHydrostatic, [XYZ]))
rhoThetaHs = dgDofContainer(groups, 1)
rhoThetaHs.interpolate(functionPython(1, rhoThetaHydrostatic, [XYZ]))

claw.setHydrostaticState(rhoHs,rhoThetaHs)
claw.setPhysicalConstants(gamma,Rd,p0,g)

boundaryWall = claw.newBoundaryWall()
claw.addBoundaryCondition('bottom_domain', boundaryWall)
claw.addBoundaryCondition('top_domain', boundaryWall)
claw.addBoundaryCondition('left', boundaryWall)
claw.addBoundaryCondition('right', boundaryWall)

claw.setFilterMode(FILTER_LINEAR)

solution0 = dgDofContainer(groups, claw.getNbFields())
solution0.copy(solution)

#dt = Tf/700
dt = 0.005
for iruns in range(0,8) :

    petscIm = linearSystemPETScBlockDouble()
    petscIm.setParameter("petscOptions","-ksp_type preonly -pc_type lu")
    #petscIm.setParameter("petscOptions",  "-ksp_atol 1e-12 -ksp_rtol 1e-12")
    dofIm = dgDofManager.newDGBlock(groups, claw.getNbFields(), petscIm)
    #timeIter = dgIMEXDIMSIM(claw,dofIm, IMEX_DIMSIM_5a)     #timeorder
    #timeIter = dgIMEXTSRK(claw, dofIm, IMEX_TSRK_23)     #timeorder
    timeIter = dgIMEXRK(claw, dofIm, 5)     #timeorder
    #timeIter = dgDIRK(claw, dofIm, 2)     #timeorder
    #timeIter.getNewton().setVerb(1)     #Verbosity

    #timeIter.getNewton().setAtol(1.e-13) #ATol
    #timeIter.getNewton().setRtol(1.e-13) #Rtol

    solution.copy(solution0)
    # dt=claw.getMinOfTimeSteps(solution) #infinite because no initial velocity
    dt=dt/2.0

    if (Msg.GetCommRank()==0):
        print "Time step:",dt
    nbSteps = int(round((Tf-Ti)/dt))
    # Compute relative error
    def getRefSolSq(FCT,  refSol) :
        for i in range(FCT.size1()):
            FCT.set(i,0, refSol(i,0)**2)
            FCT.set(i,1, refSol(i,1)**2)
            FCT.set(i,2, refSol(i,2)**2)
            FCT.set(i,3, refSol(i,3)**2)
    solRefSq=functionPython(4, getRefSolSq, [reloaded.getFunction()])

    def getError(FCT, refSol, compSol) :
        for i in range(FCT.size1()):
            FCT.set(i,0,(refSol(i,0)-compSol(i,0))**2)
            FCT.set(i,1,(refSol(i,1)-compSol(i,1))**2)
            FCT.set(i,2,(refSol(i,2)-compSol(i,2))**2)
            FCT.set(i,3,(refSol(i,3)-compSol(i,3))**2)

    error = functionPython(4, getError, [reloaded.getFunction(), solution.getFunction()])
    integratorError = dgFunctionIntegrator(groups, error)
    intErr = fullMatrixDouble(4,1)
    integratorRefSq = dgFunctionIntegrator(groups, solRefSq)
    intRefSq = fullMatrixDouble(4,1)

    #Export data
    def getExp(FCT, uv, rhop, thetap, pp) :
        for i in range (0,uv.size1()) :
            FCT.set(i,0,uv(i,0))
            FCT.set(i,1,uv(i,1))
            FCT.set(i,2,rhop(i,0))
            FCT.set(i,3,thetap(i,0))
            FCT.set(i,4,pp(i,0))
    Exp=functionPython(5, getExp, [uv, rhop, thetap, pp])
    nCompExp=[2,1,1,1]
    namesExp=["uv","rhop","thetap","pp"]
    
    prefixFileName = 'output'+str(iruns)+'/export'

    t=Ti

    # starter for IMEX TSRK, move two sptes ahead here
    #t=timeIter.starter(solution,dt,t,3)
    # starter for IMEX DIMSIM
    #timeIter.starter(solution,dt,dt,t,4)

    n_export=0
    timeStart=time.clock();
    
    start = time.clock()
    #for i in range(2,nbSteps):#TSRK
    for i in range(0,nbSteps):
        #if (i==nbSteps-1):
        #    dt=Tf-t
        #if (i%(50*2**iruns) == 0):
        #    solution.exportFunctionVtk(Exp,prefixFileName, t*1000, i,"solution",nCompExp,namesExp)
        #    if (Msg.GetCommRank()==0):
        #        print '\nWriting output',n_export,'at time',t*1000,'ms and step',i,'over',nbSteps
        #    n_export=n_export+1
        timeIter.iterate(solution, dt, t)
        t=t+dt
        if (Msg.GetCommRank()==0):
            # sys.stdout.write('.')
	    sys.stdout.write("\rDone: %d/%d" % (i+1,nbSteps) )
            sys.stdout.flush()

    print ('')
    print ('Time elapsed: ',time.clock()-timeStart)
#    solution.exportFunctionVtk(Exp,prefixFileName, Tf*1000, nbSteps,"solution",nCompExp,namesExp)

    integratorError.compute(intErr)
    integratorRefSq.compute(intRefSq)
    errp.append(sqrt(intErr(0,0)/intRefSq(0,0)))
    erru.append(sqrt(intErr(1,0)/intRefSq(1,0)))
    errv.append(sqrt(intErr(2,0)/intRefSq(2,0)))
    errthetap.append(sqrt(intErr(3,0)/intRefSq(3,0)))
    errall.append( sqrt((intErr(0,0)+intErr(1,0)+intErr(2,0)+intErr(3,0))/(intRefSq(0,0)+intRefSq(1,0)+intRefSq(2,0)+intRefSq(3,0))) )
    stepsizes.append(dt)

    if (Msg.GetCommRank()==0):
        print "\nError on rhop:",errp[-1],"rhou:",erru[-1],"rhov:", errv[-1],"rhothetap:",errthetap[-1],"all:",errall[-1]
#        outFile = open('output/ErrorVsStep','w')
#        outFile.write("h rhop rhou rhov rhothetap\n")
#        for i in range(len(stepsizes)):
 #           outFile.write("%s %s %s %s %s %s\n" % (stepsizes[i], errp[i], erru[i], errv[i], errthetap[i], errall[i]))
#        outFile.close()
#        pltP.set_data(stepsizes,errp)
#        pltU.set_data(stepsizes,erru)
#        pltV.set_data(stepsizes,errv)
#        pltThetap.set_data(stepsizes,errthetap)
#        pltAll.set_data(stepsizes,errall)
#        axes.relim()
#        axes.autoscale_view(True,True,True)
#        plt.draw()
#        fig.savefig('output/ErrorVsStep.pdf')
#        plt.draw()
#    plt.show()
Msg.Exit(0)
