clear all
close all

nx=12000
nz=320
t='000000'
filename=['Tmp/a5km_' num2str(nx) 'x' num2str(nz) '_t' t '.ascii']
disp(['Loading ' filename])
data=dlmread(filename,' ',1,0);

x = reshape(data(:,1),nx,nz+1);
z = reshape(data(:,2),nx,nz+1);
data8 = reshape(data(:,8),nx,nz+1);
contourf(x(:,1),z(1,:),data8')
