// =========================================================
// 
// Analytic solution for linear gravity wave expansion 
//
// compile with:
//
// Intel:
// > gcc -O2 -lm -lstdc++ analyt_BB2013.cpp -o analyt_BB2013.exe
//
// NEC-SX9:
// > sxc++ -sx9 -Cvopt -ftrace -R transform -R fmtlist  analyt_BB2013.cpp -o analyt_BB2013.SX9.exe
//
//
// author: Michael Baldauf (DWD)
//
// version: 08.07.2011
//          20.04.2012
//          02.05.2012
//
// Lit.:
//   M. Baldauf, S. Brdar (2013):
//   An Analytic Solution for Linear Gravity Waves in a Channel 
//   as a Test for Numerical Models using the Non-Hydrostatic, 
//   Compressible Euler Equations,
//   Quart. J. Royal Met. Soc.,
//   DOI:10.1002/qj.2105,
//
// =========================================================

#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdlib>
#include <cmath>
#include <string>
#include <complex>

using namespace std;

#define  String   basic_string<char>
#define  Complex  complex<double>


// --- global variables ------------------------

// Thermodynamic constants:
double  R_d;
double  cp_d;
double  cv_d;

// geophysical constants:
double  g_erd;       // earth acceleration [m/s^2] 
double  f_Cor;       // Coriolis-Parameter
double  f_Cor_sqr;   // Coriolis-Parameter^2

// atmospheric constants:
double  T0;
double  rho_s;
double  p_s;
double  delta_B;
double  cs_sqr;

double  delta_T;
double  d_bub;
double  x0_bub;

double  h_area;      // height of the channel
double  l_area;      // length of the channel

double  hilf3;   // Hilfsvariable im biquadratischen Polynom a1(s)

int     zaehler_fall_1a;
int     zaehler_fall_1b;
int     zaehler_fall_1c;
int     zaehler_fall_2a;
int     zaehler_fall_2b;



void teste_real ( Complex z, String name_z ) {

  if ( ( abs( z.real() ) < 1.0e-13 ) && ( abs( z.imag() < 1.0e-13 ) ) ) {
      // OK
  } else if ( ( abs( z.real() ) < 1.0e-10 ) && ( abs( z.imag() ) < 1.0e-4 * abs( z.real() ) ) ) {
      // OK
  } else {
    if ( abs( z.imag() ) > abs( z.real() ) * 1.0e-7 ) {
      cout << "Problem: " << name_z << " ist nicht rein reell! "   
           << " "  << name_z << ".real()=" << z.real()
	   << ", " << name_z << ".imag()=" << z.imag() << "\n";
      exit(1);
    }
  }
}

void FT_pp_breth( double x, double z, double t, 
                  Complex *u, Complex *v, Complex *w, Complex *pp, Complex *rhop, 
                  int anz_k ) {

  double   kx,  kz;
  double   dkx, dkz;
  int      m, n;

  Complex  alpha, alpha_sqr;
  Complex  beta,  beta_sqr;
  bool     is_alpha_null, is_beta_null;
  Complex  b_p_a, b_m_a;
  Complex  denom_ab;

  double   p, q;
  Complex  a_t, b_t;
  Complex  sin_at, sin_bt, cos_at, cos_bt;
  double   diskr;
  Complex  diskr_sqrt;

  Complex  kd_p_p, kd_p_m, kd_m_p, kd_m_m;
  Complex  a2p, a2m;

  Complex  LT_m1, LT_0, LT_1, LT_2, LT_3;

  Complex  v_fac;

  Complex  fac_kx, fac_kz;
  Complex  FT_gauss_x, FT_sin_z;

  Complex  u_FT_p,   u_FT_m;
  Complex  v_FT_p,   v_FT_m;
  Complex  w_FT_p,   w_FT_m;
  Complex  p_FT_p,   p_FT_m;
  Complex  rho_FT_p, rho_FT_m;

  Complex  rho_FT_0;
  double   delta_rho;

  double   hilf1, hilf4;
  double   epsilon;
  Complex  hilf_pm;

  double   pi;
  Complex  imag = Complex( 0.0, 1.0 );


  pi = 4.0 * atan(1.0);


  delta_rho = -delta_T / T0 * rho_s;

  dkx = 2.0 * pi / l_area;
  dkz = 2.0 * pi / ( 2.0 * h_area );

  *u    = Complex( 0.0, 0.0 );
  *v    = Complex( 0.0, 0.0 );
  *w    = Complex( 0.0, 0.0 );
  *pp   = Complex( 0.0, 0.0 );
  *rhop = Complex( 0.0, 0.0 );

  for ( n = -anz_k/2; n <= anz_k/2-1; n++ ) {
    kx = dkx * n;

    fac_kx = exp( imag * kx * x );

    // Fourier-trafo of exp( - (x/d_bub)^2 ) :
    hilf1 = d_bub * kx;
    FT_gauss_x = d_bub / ( 2.0 * sqrt(pi) ) * exp( - 0.25 * hilf1 * hilf1 );

    for ( m = 1; m <= 1; m++ ) {
      kz = dkz * m;

      fac_kz = exp( imag * kz * z );

      // Fouriertrafo of sin( pi z/h_area ) in [0..2*H]:
      if ( m == 1 ) {
	FT_sin_z = -0.5 * imag;
      } else if ( m == -1) {
	FT_sin_z =  0.5 * imag;
      } else {
	FT_sin_z = 0.0;
      }

      rho_FT_0 = delta_rho * FT_gauss_x * FT_sin_z;

      hilf4 = kz*kz + 0.25 * delta_B*delta_B;

      p = cs_sqr * ( kx*kx + hilf4 ) + f_Cor_sqr;
      q = kx*kx * hilf3 + f_Cor_sqr * cs_sqr * hilf4;

      diskr = 0.25*p*p - q;
      if ( diskr < 0.0 ) {
	cout << "diskr=" << diskr << " ist <0 bei " << x << z << kx << kz << "\n";
      }
      diskr_sqrt = sqrt( diskr );
      alpha = sqrt( 0.5*p - diskr_sqrt );
      beta  = sqrt( 0.5*p + diskr_sqrt );

      alpha_sqr = alpha * alpha;
      beta_sqr  = beta  * beta;

      kd_p_m =  imag * kz - 0.5*delta_B;
      kd_p_p =  imag * kz + 0.5*delta_B;
      kd_m_p = -kd_p_m;
      kd_m_m = -kd_p_p;

      a2p = g_erd - cs_sqr * kd_p_p;
      a2m = g_erd - cs_sqr * kd_m_p;

      sin_at = sin( alpha * t );
      sin_bt = sin( beta  * t );
      cos_at = cos( alpha * t );
      cos_bt = cos( beta  * t );

      b_p_a = beta + alpha;
      b_m_a = beta - alpha;

      epsilon = 1.0e-20;

      if (  ( abs( b_p_a ) > epsilon ) 
         && ( abs( b_m_a ) > epsilon ) ) {

	denom_ab = 1.0 / ( b_p_a * b_m_a );

        is_alpha_null = ( abs( alpha ) < 1.0e-30 );
        is_beta_null  = ( abs( beta  ) < 1.0e-30 );

        if ( !is_alpha_null && !is_beta_null ) {
          zaehler_fall_1a++;

          LT_m1 = ( -cos_at / alpha_sqr + cos_bt / beta_sqr ) * denom_ab 
                       + 1.0/(alpha_sqr * beta_sqr);
          LT_0  = ( sin_at / alpha     - sin_bt / beta      ) * denom_ab;

        } else if ( is_alpha_null && !is_beta_null ) {
          zaehler_fall_1b++;

          b_t = beta * t; 
          LT_m1 = ( 0.5 * b_t * b_t + cos_bt - 1.0 ) / pow(beta,4);
          LT_0  = ( b_t - sin_bt ) / pow(beta,3);

        } else if ( !is_alpha_null && is_beta_null ) {
          zaehler_fall_1c++;

          a_t = alpha * t; 
          LT_m1 = ( 0.5 * a_t * a_t + cos_at - 1.0 ) / pow(alpha,4);
          LT_0  = ( a_t - sin_at ) / pow(alpha,3);

        } else {
          cout << "dieser Fall darf nicht vorkommen\n";
          exit(1);
        }

        LT_1 = ( cos_at             - cos_bt            ) * denom_ab;
        LT_2 = (-sin_at * alpha     + sin_bt * beta     ) * denom_ab;
        LT_3 = (-cos_at * alpha_sqr + cos_bt * beta_sqr ) * denom_ab;

  
      } else {

        if ( abs( alpha ) > 1.0e-30 ) {

          zaehler_fall_2a++;

          a_t = alpha * t;

          LT_m1 = 1.0/(alpha_sqr*alpha_sqr) * ( -0.5 * a_t * sin_at - cos_at + 1.0);
          LT_0  = 0.5 / alpha_sqr * ( sin_at / alpha - t * cos_at );
          LT_1  = 0.5 / alpha * t * sin_at;
          LT_2  = 0.5 / alpha * ( sin_at + a_t * cos_at );
          LT_3  = cos_at - 0.5 * a_t * sin_at;

        } else {

          zaehler_fall_2b++;

          LT_3  = 1.0;
          LT_2  = t;
          LT_1  = LT_2 * t * 0.5;
          LT_0  = LT_1 * t / 3.0;
          LT_m1 = LT_0 * t * 0.25;
        }

      }

      v_fac = g_erd * rho_FT_0 / rho_s;


      hilf_pm = imag * kx * LT_0 * v_fac;
      u_FT_p =  hilf_pm * a2p;
      u_FT_m = -hilf_pm * a2m;

      if ( m==0 ) {
        *u += u_FT_p * fac_kx;
      } else {
        *u += ( u_FT_p * fac_kz + u_FT_m / fac_kz ) * fac_kx;
      }


      hilf_pm = f_Cor * imag * kx * LT_m1 * v_fac;
      v_FT_p = - hilf_pm * a2p;
      v_FT_m =   hilf_pm * a2m;

      if ( m==0 ) {
        *v += v_FT_p * fac_kx;
      } else {
        *v += ( v_FT_p * fac_kz + v_FT_m / fac_kz ) * fac_kx;
      }


      w_FT_p = - ( LT_2 + (kx*kx*cs_sqr + f_Cor_sqr) * LT_0 ) * v_fac;
      w_FT_m = -w_FT_p;

      if ( m==0 ) {
	*w += w_FT_p * fac_kx;
      } else {
        *w += ( w_FT_p * fac_kz + w_FT_m / fac_kz ) * fac_kx;
      }


      rho_FT_p = ( LT_3 + ( p + g_erd * kd_p_m )  * LT_1 
               + f_Cor_sqr * ( cs_sqr * hilf4 + g_erd * kd_p_m ) * LT_m1 ) *   rho_FT_0 ;
      rho_FT_m = ( LT_3 + ( p + g_erd * kd_m_m )  * LT_1 
               + f_Cor_sqr * ( cs_sqr * hilf4 + g_erd * kd_m_m ) * LT_m1 ) * (-rho_FT_0);

      if ( m==0 ) {
        *rhop += rho_FT_p * fac_kx;
      } else {
        *rhop += ( rho_FT_p * fac_kz + rho_FT_m / fac_kz ) * fac_kx;
      }


      hilf_pm = ( LT_1 + f_Cor_sqr * LT_m1 ) * g_erd * rho_FT_0 ;
      p_FT_p = -a2p * hilf_pm;
      p_FT_m =  a2m * hilf_pm;

      if ( m==0 ) {
        *pp += p_FT_p * fac_kx;
      } else {
        *pp += ( p_FT_p * fac_kz + p_FT_m / fac_kz ) * fac_kx;
      }

    }
  }

  *u    = *u    * dkx;
  *v    = *v    * dkx;
  *w    = *w    * dkx;
  *rhop = *rhop * dkx;
  *pp   = *pp   * dkx;


}


int main () {

  int     nx; // nx   gridpoints in horizontal direction
  int     nz; // nz+1 gridpoints in vertical   direction
  double  dx, dz;
  int     i, k;
  double  x, xs, z;

  int     k_out_min, k_out_max;  

  double  gebietsstreckung_x;
  int     anz_k;

  double  a;
  double  t;
  double  u0;
  String  dummy;
  String  dateikenn;
  String  dateiname;
  bool    flag_out_ascii;
  bool    flag_out_bin;
  ofstream  *dat_ascii;
  ofstream  *dat_bin;

  double  p0, rho0, Theta0;
  double  fac_breth;
  Complex u_b, v_b, w_b, pp_b, rhop_b;
  double  u  , v  , w  , pp  , rhop, Tp, Thetap;


  // read parameters:
  cin >>  nx                 >> dummy;
  cin >>  nz                 >> dummy;
  cin >>  dx                 >> dummy;
  cin >>  dz                 >> dummy;
  cin >>  gebietsstreckung_x >> dummy;
  cin >>  anz_k              >> dummy;
  cin >>  T0                 >> dummy;
  cin >>  f_Cor              >> dummy;
  cin >>  u0                 >> dummy;
  cin >>  delta_T            >> dummy;
  cin >>  x0_bub             >> dummy;
  cin >>  d_bub              >> dummy;
  cin >>  t                  >> dummy;
  cin >>  dateikenn          >> dummy;
  cin >>  flag_out_ascii     >> dummy;
  cin >>  flag_out_bin       >> dummy;
  cin >>  k_out_min          >> dummy;
  cin >>  k_out_max          >> dummy;

  cout << "\n";

  if ( ( ! flag_out_ascii ) && ( ! flag_out_bin ) ) {
    cout << "WARNUNG: es wird weder Ascii- noch binaer ausgegeben!\n\n";
  }

  if ( gebietsstreckung_x < 1.0 ) {
    cout << "WARNUNG: gebietsstreckung_x < 1 .0 \n";
  }

  if ( k_out_min < 0 ) {
    cout << "FEHLER: k_out_min >=0 notwendig!\n";
    exit(1);
  }

  if ( k_out_max > nz ) {
    cout << "FEHLER: k_out_max <=nz notwendig!\n";
    exit(1);
  }

  if ( k_out_min > k_out_max ) {
    cout << "FEHLER: k_out_min <= k_out_max notwendig!\n";
    exit(1);
  }


  // ------ other constants, parameters: --------


  g_erd = 9.80665;   // [m/s^2] earth acceleration

  // thermodynamic constants of dry air (at about 270 K)
  R_d  = 287.05;     // [J/kg/K] individual gas constant
  cp_d = 1005.0;     // [J/kg/K] specific heat capacity under constant pressure 
  cv_d = cp_d - R_d; // [J/kg/K] specific heat capacity under constant volume

  // parameters of the background atmosphere:
  p_s     = 1.0e5;                  // [Pa]     pressure at the bottom
  rho_s   = p_s / ( R_d * T0 );     // [kg/m^3] density  at the bottom
  delta_B = g_erd / ( R_d * T0 );   // [1/m]    'Bretherton' inverse length scale
  cs_sqr  = cp_d/cv_d * R_d * T0;   // [m^2/s^2]  square of sound velocity 

  f_Cor_sqr = f_Cor * f_Cor;

  hilf3 = g_erd * ( cs_sqr * delta_B - g_erd );

  h_area =  nz * dz;
  l_area = (nx * dx) * gebietsstreckung_x;

  if ( anz_k < 30.0/3.1415927 * l_area / d_bub ) {
    cout << "WARNUNG: anz_k ist womoeglich zu klein! \n";
  }

 
  if ( flag_out_ascii ) {
    dateiname = dateikenn + ".ascii";
    dat_ascii = new ofstream( dateiname.c_str(), ios::out );
    if ( ! *dat_ascii ) {
      cout << "FEHLER beim oeffnen der Datei " << dateiname << "\n\n";
    }
    *dat_ascii << "#              x"
               << "               z"
               << "              u'"
               << "              w'"
               << "              p'"
               << "            rho'"
               << "              T'"
               << "          Theta'"
               << "\n";
  }

  if ( flag_out_bin ) {
    dateiname = dateikenn + ".bin";
    dat_bin = new ofstream( dateiname.c_str(), ios::binary | ios::out );
    if ( ! *dat_bin ) {
      cout << "FEHLER beim oeffnen der Datei " << dateiname << "\n\n";
    }
  }

  for ( k=k_out_min; k<= k_out_max; k++ ) {


    z = k * dz;
    cout <<  "k= " << k << "\n";

    fac_breth = exp( - 0.5 * delta_B * z );

    p0     = p_s * fac_breth * fac_breth; 
    rho0   = p0 / ( R_d * T0 );
    Theta0 = T0 * pow( p_s / p0, R_d/cp_d );

    zaehler_fall_1a = 0;
    zaehler_fall_1b = 0;
    zaehler_fall_1c = 0;
    zaehler_fall_2a = 0;
    zaehler_fall_2b = 0;

 
    for ( i=0; i<= nx-1; i++ ) {

      x  = i*dx;
      xs = x - x0_bub - u0*t;

      FT_pp_breth( xs, z, t, &u_b, &v_b, &w_b, &pp_b, &rhop_b, anz_k );

      u      =  u_b.real()   / fac_breth;
      v      =  v_b.real()   / fac_breth;
      w      =  w_b.real()   / fac_breth;
      pp     = pp_b.real()   * fac_breth;
      rhop   = rhop_b.real() * fac_breth;

      Tp     = T0 * ( pp / p0 - rhop / rho0 );
      Thetap = Theta0 * ( Tp / T0 - R_d/cp_d *pp/p0 );

      if ( flag_out_ascii ) {
        dat_ascii->setf( ios::scientific );
        dat_ascii->precision(9);
        *dat_ascii << std::showpos << setw(17) << x;

        dat_ascii->setf( ios::scientific );
        dat_ascii->precision(9);
        *dat_ascii << std::showpos << setw(17) << z;

        dat_ascii->setf( ios::scientific );
        dat_ascii->precision(9);
        *dat_ascii << std::showpos << setw(17) << u;

        dat_ascii->setf( ios::scientific );
        dat_ascii->precision(9);
        *dat_ascii << std::showpos << setw(17) << w;

        dat_ascii->setf( ios::scientific );
        dat_ascii->precision(9);
        *dat_ascii << std::showpos << setw(17) << pp;

        dat_ascii->setf( ios::scientific );
        dat_ascii->precision(9);
        *dat_ascii << std::showpos << setw(17) << rhop;

        dat_ascii->setf( ios::scientific );
        dat_ascii->precision(9);
        *dat_ascii << std::showpos << setw(17) << Tp;

        dat_ascii->setf( ios::scientific );
        dat_ascii->precision(9);
        *dat_ascii << std::showpos << setw(17) << Thetap;
 
        *dat_ascii << "\n";
      }

      if ( flag_out_bin ) {

        a = (double) x;
        dat_bin->write( (char*)&a, sizeof(a) );

        a = (double) z;
        dat_bin->write( (char*)&a, sizeof(a) );

        a = (double) u;
        dat_bin->write( (char*)&a, sizeof(a) );

        a = (double) v;
        dat_bin->write( (char*)&a, sizeof(a) );

        a = (double) w;
        dat_bin->write( (char*)&a, sizeof(a) );

        a = (double) pp;
        dat_bin->write( (char*)&a, sizeof(a) );

        a = (double) rhop;
        dat_bin->write( (char*)&a, sizeof(a) );

        a = (double) Tp;
        dat_bin->write( (char*)&a, sizeof(a) );

        a = (double) Thetap;
        dat_bin->write( (char*)&a, sizeof(a) );

      }

    }

    cout << "Anzahl Fall 1.a =" << zaehler_fall_1a << "\n";
    cout << "Anzahl Fall 1.b =" << zaehler_fall_1b << "\n";
    cout << "Anzahl Fall 1.c =" << zaehler_fall_1c << "\n";
    cout << "Anzahl Fall 2.a =" << zaehler_fall_2a << "\n";
    cout << "Anzahl Fall 2.b =" << zaehler_fall_2b << "\n";

  }


  if ( flag_out_ascii ) {
    delete   dat_ascii;
  }
  if ( flag_out_bin ) {
    delete   dat_bin;
  }

  return 0;

}



