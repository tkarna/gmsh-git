
export ENV_HH="00"


for ENV_MM in  00  30
do

export ENV_MM


export ENV_SS="00"


let TIME=ENV_HH*3600+ENV_MM*60+ENV_SS
echo $TIME " sec."

# Anm.:
# Die Abtastung im Fourierbereich erfolgt wie ueblich mit   delta k = 2 pi / l_area, 
# mit der Gebietsgroesse   l_area = (nx * dx) * gebietsstreckung_x;
# die analytische Lsg. ist damit periodisch mit der Periode l_area.
# d.h. man sollte 
#   gebietsstreckung_x=1 oder groesser waehlen.
# Die Anzahl der abgetasteten Wellenzahlen sollte 
#   mindestens   anz_k >= 10 * l_area / d_bub    betragen

NX=3000
NZ=200

cat > a.inp  <<EOF
 $NX          nx
 $NZ          nz
 100.0        dx
  50.0        dz
  1.0         gebietsstreckung_x
  600         anz_k
  250.0       T0
  0.0         f_Cor
  20.0        u0
 0.0001       delta_T
 100000.0     x0_bub
 5000.0       d_bub
 ${TIME}      t
 Tmp/a5km_${NX}x${NZ}_t${ENV_HH}${ENV_MM}${ENV_SS}    dateikenn
 1            flag_out_ascii(0/1)
 0            flag_out_bin(0/1)
 0            k_out_min
 ${NZ}        k_out_max
EOF

./analyt_BB2013 < a.inp

#ncl plot.ncl

#gv  b_${ENV_HH}${ENV_MM}${ENV_SS}.eps  &


done
