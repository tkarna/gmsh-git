from extrude import *
from dgpy import *
from math import *
import time, os, sys
import gmshPartition
from scipy import interpolate
import scipy
import cPickle as pickle
from partitionThenExtrude import *
#### Physical constants ###

minOrder=5
maxOrder=5
minConv=8
maxConv=15
convType = 'time' #Can be either 'time' or 'space'
meshSplitForTimeConv = 4
Rd=287.05
Cp=1005.0
Cv=Cp-Rd
gamma=Cp/Cv
p0=1.0e5
g=9.80665
#TEST CASE
T0=250
N=sqrt(g*g/(Cp*T0))
theta0=T0
U=20.0
d=5000
dT=1.0e-4
H=10000
xc=-50000
delta=g/(Rd*T0)
###########################
#### Begin and end time ###
Ti=0
Tf=50*60
###########################


dimension=2



def hydrostaticState(z) :
  THs = T0 
  pHs = p0 * exp(-delta*z) 
  thetaHs = THs*(p0/pHs)**(Rd/Cp)
  rho0 = p0 * delta / g
  rhoHs = rho0 * exp(-delta*z)
  return rhoHs,thetaHs
def initialCondition(FCT,  XYZ) :
    for i in range (0,XYZ.size1()) :
        x=XYZ(i,0)
        z=XYZ(i,1)
        rhoHs,thetaHs = hydrostaticState(z)

        Tb= dT * exp(-((x-xc)**2)/(d*d))*sin(pi*z/H)        
        rho0 = p0 / (T0*Rd)
        rhob = rho0 * (-Tb/T0)
        rhoPert = rhoHs + exp(-0.5*delta*z)*rhob

        Tpert = T0 + exp(0.5*delta*z)*Tb
        pPert = p0 * exp(-delta*z) #Only background state
        thetaPert = Tpert*(p0/pPert)**(Rd/Cp)
        FCT.set(i,0,rhoPert-rhoHs)
        FCT.set(i,1,U*rhoPert)
        FCT.set(i,2,0)
        FCT.set(i,3,rhoPert*thetaPert-rhoHs*thetaHs)
#Analytical Solution
def reloadAnalytical(time):
        timeStr = '00%02i00'%(time/60)
        if (Msg.GetCommRank()==0):
          print('Reloading analytical solution at time '+timeStr+' ('+str(time/60)+' minutes)')
        global upAnIntrp, wpAnIntrp, ppAnIntrp, rhopAnIntrp, TpAnIntrp, thetapAnIntrp
        upAnIntrp = pickle.load(open("upAnIntrp_t"+str(time), "rb"))
        wpAnIntrp = pickle.load(open("wpAnIntrp_t"+str(time), "rb"))
#        ppAnIntrp = pickle.load(open("ppAnIntrp_t"+str(time), "rb"))
        rhopAnIntrp = pickle.load(open("rhopAnIntrp_t"+str(time), "rb"))
#        TpAnIntrp = pickle.load(open("TpAnIntrp_t"+str(time), "rb"))
        thetapAnIntrp = pickle.load(open("thetapAnIntrp_t"+str(time), "rb"))
        if (Msg.GetCommRank()==0):
          print('done')
def getAnalyticalSol(FCT,  XYZ, initF):
  for i in range(FCT.size1()):
    x = XYZ(i,0)+150000
    z = XYZ(i,1)
    rhoHs,thetaHs = hydrostaticState(z)
    rhop = rhopAnIntrp.ev(x, z)[0]
    FCT.set(i,0, rhop)
    rho = rhoHs+rhop
    FCT.set(i,1,rho*(U+upAnIntrp.ev(x, z)[0]))
    FCT.set(i,2,rho*wpAnIntrp.ev(x, z)[0])
    theta = thetaHs + thetapAnIntrp.ev(x, z)[0]
    FCT.set(i,3,rho*theta - rhoHs*thetaHs)
def getError(FCT, XYZ, analytic, numerical) :
  for i in range(FCT.size1()):
    for j in range(FCT.size2()):
      FCT.set(i,j,(analytic(i,j)-numerical(i,j))**2)
def getSquare(FCT, fields) :
  for i in range(FCT.size1()):
    for j in range(FCT.size2()):
        FCT.set(i,j,fields(i,j)**2)

timeIters = [DG_ERK_EULER,DG_ERK_22,DG_ERK_33,DG_ERK_44,DG_ERK_44] 

for order in range(minOrder,maxOrder+1):
  if (Msg.GetCommRank()==0 and os.path.isdir('convergence') == False):
    os.makedirs('convergence')
  Msg.Barrier()
  convOutFile = open('convergence/convData_p'+str(order),'w')
  convOutFile.close()
  
  for iConv in range (minConv,maxConv+1) :
    if (Msg.GetCommRank()==0):
      print("Convergence i = %i"%iConv)
      lineModel = GModel()
      lineModel.load("line.geo")
      lineModel.mesh(1)
      
      if (convType == 'space'):
        nbSplit = iConv
      else:
        if (convType == 'time'):
          nbSplit = meshSplitForTimeConv-1
        else:
          Msg.Error('convType should either be ""space"" or ""time""')
      
      for jConv in range (1,nbSplit+1):
        lineModel.refineMesh(1)
      lineModel.save("line.msh")
      partitionThenExtrude(Msg.GetCommSize(),nbSplit)
      
  
    Msg.Barrier()
    expDir = 'convergence/output_p'+str(order)+convType+str(iConv)
    model = GModel()
    name='line'
    if Msg.GetCommSize()>1:
      partStr='_part_%i' % Msg.GetCommSize()
    else:
      partStr=''
    model.load(name + partStr + '_2d_XY_periodic.msh')
  
    groups = dgGroupCollection(model, dimension, order)
    groups.splitGroupsByPhysicalTag();
    groupsH = dgGroupCollection.newByTag(model, dimension-1, order, ["bottom_domain"])
    extrusion = dgExtrusion(groups, groupsH, ["bottom_domain"])
    groups.splitFaceGroupsByOrientation(extrusion, ["bottom_domain", "top_domain"]) 
  
  
    claw = newEulerAtmLaw(dimension)
    solution = dgDofContainer(groups, claw.getNbFields())
    XYZ = groups.getFunctionCoordinates()
  
    initF=functionPython(4, initialCondition, [XYZ])
    solution.L2Projection(initF)
  
    reloadAnalytical(0)
    analyticalSol=functionPython(4, getAnalyticalSol, [XYZ, initF])
    
    errorCont = functionPython(4, getError, [XYZ, analyticalSol, initF])
    error = functionPython(4, getError, [XYZ, analyticalSol, solution.getFunction()])
    solAnSq=functionPython(4, getSquare, [analyticalSol])
    integratorErrorCont = dgFunctionIntegrator(groups, errorCont)
    integratorError = dgFunctionIntegrator(groups, error)
    intErr = fullMatrixDouble(4,1)
    intErrCont = fullMatrixDouble(4,1)
    integratorAnalyticSq = dgFunctionIntegrator(groups, solAnSq)
    intAnalyticSq = fullMatrixDouble(4,1)   
    
    integratorErrorCont.compute(intErrCont)
    integratorError.compute(intErr)
    integratorAnalyticSq.compute(intAnalyticSq)
    if (Msg.GetCommRank()==0):
      print("Continuous abs error on rhop: "+str(sqrt(intErrCont(0,0))))
      print("Continuous abs error on rhou: "+str(sqrt(intErrCont(1,0))))
      print("Continuous abs error on rhow: "+str(sqrt(intErrCont(2,0))))
      print("Continuous abs error on rhothetap: "+str(sqrt(intErrCont(3,0))))
      print("Continuous rel error on rhop: "+str(intErrCont(0,0)/intAnalyticSq(0,0)))
      print("Continuous rel error on rhou: "+str(intErrCont(1,0)/intAnalyticSq(0,0)))
      print("Continuous rel error on rhothetap: "+str(intErrCont(3,0)/intAnalyticSq(0,0)))
      print("Discrete abs error on rhop: "+str(sqrt(intErr(0,0))))
      print("Discrete abs error on rhou: "+str(sqrt(intErr(1,0))))
      print("Discrete abs error on rhow: "+str(sqrt(intErr(2,0))))
      print("Discrete abs error on rhothetap: "+str(sqrt(intErr(3,0))))
      print("Discrete rel error on rhop: "+str(intErr(0,0)/intAnalyticSq(0,0)))
      print("Discrete rel error on rhou: "+str(intErr(1,0)/intAnalyticSq(0,0)))
      print("Discrete rel error on rhothetap: "+str(intErr(3,0)/intAnalyticSq(0,0)))
    
            
    def rhoHydrostatic(FCT,  XYZ) :
        for i in range (0,XYZ.size1()) :
            z=XYZ(i,1)
            rhoHs,thetaHs = hydrostaticState(z)
            FCT.set(i,0,rhoHs)
    
    def rhoThetaHydrostatic(FCT,  XYZ) :
        for i in range (0,XYZ.size1()) :
            z=XYZ(i,1)
            rhoHs,thetaHs = hydrostaticState(z)
            FCT.set(i,0,rhoHs*thetaHs)
            
    def getVelocity(FCT, sol, XYZ) :
        for i in range (0,sol.size1()) :
            z=XYZ(i,1)
            rhoHs,thetaHs = hydrostaticState(z)
            rho=rhoHs+sol(i,0)
            FCT.set(i,0,sol(i,1)/rho)
            FCT.set(i,1,sol(i,2)/rho)
            FCT.set(i,2,0)
    
    def getRhop(FCT, sol) :
        for i in range (0,sol.size1()) :
            FCT.set(i,0,sol(i,0))
    
    def getpp(FCT, sol, XYZ) :
        for i in range (0,sol.size1()) :
            z=XYZ(i,1)
            rhoHs,thetaHs = hydrostaticState(z)
            rho=rhoHs+sol(i,0)
            rhoTheta = rhoHs*thetaHs+sol(i,3)
            pHs=p0*(rhoHs*thetaHs*Rd/p0)**gamma
            p=p0*(rhoTheta*Rd/p0)**gamma
            FCT.set(i,0,p-pHs)
    
    def getpHs(FCT, sol, XYZ) :
        for i in range (0,sol.size1()) :
            z=XYZ(i,1)
            rhoHs,thetaHs = hydrostaticState(z)
            pHs=p0*(rhoHs*thetaHs*Rd/p0)**gamma
            FCT.set(i,0,pHs)
    
    def getp(FCT, sol, XYZ) :
        for i in range (0,sol.size1()) :
            z=XYZ(i,1)
            rhoHs,thetaHs = hydrostaticState(z)
            rhoTheta = rhoHs*thetaHs+sol(i,3)
            p=p0*(rhoTheta*Rd/p0)**gamma
            FCT.set(i,0,p)
    
    def getThetap(FCT, sol, XYZ) :
        for i in range (0,sol.size1()) :
            z=XYZ(i,1)
            rhoHs,thetaHs = hydrostaticState(z)
            rho=rhoHs+sol(i,0)
            FCT.set(i,0,(sol(i,3)+(rhoHs*thetaHs))/rho-thetaHs)
    
    uv=functionPython(3, getVelocity, [solution.getFunction(), XYZ])
    rhop=functionPython(1, getRhop, [solution.getFunction()])
    pHs=functionPython(1, getpHs, [solution.getFunction(), XYZ])
    p=functionPython(1, getp, [solution.getFunction(), XYZ])
    pp=functionPython(1, getpp, [solution.getFunction(), XYZ])
    thetap=functionPython(1, getThetap, [solution.getFunction(), XYZ])
    
    
    rhoHs = dgDofContainer(groups, 1)
    rhoHs.interpolate(functionPython(1, rhoHydrostatic, [XYZ]))
    rhoThetaHs = dgDofContainer(groups, 1)
    rhoThetaHs.interpolate(functionPython(1, rhoThetaHydrostatic, [XYZ]))
    
    claw.setHydrostaticState(rhoHs,rhoThetaHs)
    claw.setPhysicalConstants(gamma,Rd,p0,g)
    
    trms = claw.getActiveTerms();
    trms.setAdvV(True);
    trms.setAdvT(True);
    trms.setPGrad(True);
    trms.setVDiv(True);
    trms.setCor(True);
    trms.setGrav(True);
    trms.setRhoPot(False);
    trms.setDiff(False);
    trms.setLax(True);
    
    
    c = sqrt(gamma * p0 / 1)
    #nuStab = dgJumpDiffusion(groups, 3, 2 * order + 1, c, 200)
    #nu = nuStab.diffusivity()
    #nuStab.apply(solution)
    #claw.setNuFactor(nu)
    
    boundaryWall = claw.newBoundaryWall()
    claw.addBoundaryCondition('bottom_domain', boundaryWall)
    claw.addBoundaryCondition('top_domain', boundaryWall)
    
    #We need to use a dof container to restrict the initial condition
    # (=boundary condition) to the discretization space
    #initDof = dgDofContainer(groups, 4)
    #initDof.interpolate(initF)
    #outsideBoundary = claw.newOutsideValueBoundary("",initDof.getFunction())
    #claw.addBoundaryCondition('left', outsideBoundary)
    #claw.addBoundaryCondition('right', outsideBoundary)
    
    #zero=claw.new0FluxBoundary()
    #claw.addBoundaryCondition('bottom', zero)
    #claw.addBoundaryCondition('top', zero)
    #claw.addBoundaryCondition('left', zero)
    #claw.addBoundaryCondition('right', zero)
    
    if (convType == 'space'):
      timeIter = dgERK(claw, None, timeIters[min(order,len(timeIters))])
    else:
      filterMode = FILTER_LINEAR
      claw.setFilterMode(filterMode)
      if (filterMode == FILTER_LINEARVERTICAL):
        petscIm = dgLinearSystemExtrusion(claw, groups, extrusion)
        petscIm.setParameter("petscOptions",  "-ksp_type preonly -pc_type lu")
      else:
        petscIm = linearSystemPETScBlockDouble()
        if (Msg.GetCommSize() > 1):
          petscIm.setParameter("petscOptions",  "-ksp_type preonly -pc_type lu -pc_factor_mat_solver_package mumps")
        else:
          petscIm.setParameter("petscOptions",  "-ksp_type preonly -pc_type lu")
      dofIm = dgDofManager.newDGBlock(groups, claw.getNbFields(), petscIm)
#      timeIter = dgERK(claw, None, timeIters[min(order,len(timeIters))])
      timeIter = dgIMEXRK(claw, dofIm, 2)     #timeorder
    #claw.setFilterMode(FILTER_LINEAR);
    ##petscIm = dgLinearSystemExtrusion(claw, groups, extrusion)
    #
    #petscIm = linearSystemPETScBlockDouble()
    ##petscIm.setParameter("petscOptions",  "-ksp_type preonly -pc_type lu -pc_factor_mat_solver_package mumps")
    ##petscIm.setParameter("petscOptions",  "-ksp_atol 0 -ksp_rtol 1e-20")
    #dofIm = dgDofManager.newDGBlock(groups, claw.getNbFields(), petscIm)
    ##timeIter = dgIMEXDIMSIM(claw, dofIm, 2)     #timeorder
    ##timeIter = dgIMEXTSRK(claw, dofIm, 4)     #timeorder
    #timeIter = dgIMEXRK(claw, dofIm, 2)     #timeorder
    ##timeIter = dgDIRK(claw, dofIm, 2)     #timeorder
    #timeIter.getNewton().setAtol(1.e-5) #ATol
    #timeIter.getNewton().setRtol(1.e-8) #Rtol
    #timeIter.getNewton().setVerb(3)     #Verbosity
    
    if (convType == 'space'):
      dt=claw.getMinOfTimeSteps(solution, extrusion)*.5
    else:
      dt=claw.getMinOfTimeSteps(solution, extrusion)*2
    #Constant time step to avoid temporal convergence effects
#    dt=claw.getMinOfTimeSteps(solution, extrusion)/(2**(maxConv-iConv))
  
  #  dt=0.01
    #dt=100
    #Modify the time step such that if corresponds to the analytical solution exports
    #In minutes
    interv=10.0
    interv=interv*60.0
    nbStepInInterv = floor(interv/dt)
    dt=interv/nbStepInInterv

    if (convType == 'time'):
      dt = dt/2**iConv
    
    if (Msg.GetCommRank()==0):
        print("Time step:",dt)
    
    nbSteps = int(ceil(Tf/dt))
    
    #Export data
    def getExp(FCT, sol, thetap, rhoHs, rhoThetaHs) :
        for i in range (0,FCT.size1()) :
            FCT.set(i,0,sol(i,0))
            FCT.set(i,1,sol(i,1))
            FCT.set(i,2,sol(i,2))
            FCT.set(i,3,sol(i,3))
            FCT.set(i,4,thetap(i,0))
            FCT.set(i,5,rhoHs(i,0))
            FCT.set(i,6,rhoThetaHs(i,0))
    Exp=functionPython(7, getExp, [solution.getFunction(), thetap, rhoHs.getFunction(), rhoThetaHs.getFunction()])
    thetapAn=functionPython(1, getThetap, [analyticalSol, XYZ])
    ExpAn=functionPython(7, getExp, [analyticalSol, thetapAn, rhoHs.getFunction(), rhoThetaHs.getFunction()])
    nCompExp=[1,2,1,1,1,1]
    namesExp=["rhop","rhouv","rhothetap","thetap","rhoHs","rhoThetaHs"]
    
    #sBV = 2.0/3*(order+1.0)
    #muBV = 0.2
    #filterBV = dgFilterBoydVandeven(groups, "", sBV, muBV)
    
    
    t=Ti
    # starter for IMEX TSRK
    #t=timeIter.starter(solution,dt,t,3)
    # starter for IMEX DIMSIM
    #timeIter.starter(solution,dt,dt,t,3)
    
    n_export=0
    timeStart=time.clock();
    for i in range(0,nbSteps-1):
        t=dt*i #More accurate, but dt cannot be changed
        if (t%interv/interv < 1e-11 or (interv-t%interv)/interv < 1e-11):
          if (t/interv > 1e-11):
            reloadAnalytical(int(round(t)))
            integratorError.compute(intErr)
            integratorAnalyticSq.compute(intAnalyticSq)
            if (Msg.GetCommRank()==0):
              print("Rel Error on rhop: "+str(intErr(0,0)/intAnalyticSq(0,0)))
              print("Rel Error on rhou: "+str(intErr(1,0)/intAnalyticSq(1,0)))
              print("Rel error on rhow: "+str(intErr(2,0)/intAnalyticSq(2,0)))
              print("Rel Error on rhothetap: "+str(intErr(3,0)/intAnalyticSq(3,0)))
              convOutFile = open('convergence/convData_p'+str(order),'a')
              convOutFile.write("%i %e %e %e %e %e\n" % (iConv, t, intErr(0,0)/intAnalyticSq(0,0),intErr(1,0)/intAnalyticSq(1,0),intErr(2,0)/intAnalyticSq(2,0),intErr(3,0)/intAnalyticSq(3,0)))
              convOutFile.close()
  
    
          solution.exportFunctionVtk(Exp,expDir+'/export', t, i,"solution",nCompExp,namesExp)
          solution.exportFunctionVtk(ExpAn,expDir+'/analytic', t, i,"solution",nCompExp,namesExp)
          solution.exportFunctionVtk(error,expDir+'/error', t, i,"solution",[1,1,1,1],["rhop","rhou","rhov","rhothetap"])
          solution.exportFunctionVtk(errorCont,expDir+'/errorCont', t, i,"solution",[1,1,1,1],["rhop","rhou","rhov","rhothetap"])
          if (Msg.GetCommRank()==0):
            print('\nWriting output %i at time %e and step %i over %i' %(n_export,t,i,nbSteps))
            print('Time elapsed: '+str(time.clock()-timeStart))
          n_export=n_export+1
        norm = timeIter.iterate (solution, dt, t)
    #    nuStab.apply(solution);
    #    filterBV.apply(solution)
        if (Msg.GetCommRank()==0):
            sys.stdout.write('.')
            sys.stdout.flush()
    print ('')
    print ('Time elapsed: '+str(time.clock()-timeStart))
Msg.Exit(0)
