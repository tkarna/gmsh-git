import time, os, sys
from scipy import interpolate
import scipy
import cPickle as pickle

U0=20

def reloadAnalytical(time):
	nxAn=12000
	nzAn=320
        timeStr = '00%02i00'%(time/60)
        print('Loading analytical solution at time '+timeStr+' ('+str(time/60)+' minutes)')
	with open('analyticalSol/Tmp/a5km_'+str(nxAn)+'x'+str(nzAn)+'_t'+timeStr+'.ascii', 'rb') as file:
	  header = file.next()
          xAn = scipy.empty(nxAn)
          zAn = scipy.empty(nzAn+1)
          upAn=scipy.empty([nxAn,nzAn+1])
          wpAn=scipy.empty([nxAn,nzAn+1])
          ppAn=scipy.empty([nxAn,nzAn+1])
          rhopAn=scipy.empty([nxAn,nzAn+1])
          TpAn=scipy.empty([nxAn,nzAn+1])
          thetapAn=scipy.empty([nxAn,nzAn+1])
  
          for j in range(nzAn+1):
            for i in range(nxAn):
              data=file.next().strip().split(' ')
              xAn[i] = float(data[0])
              zAn[j] = float(data[1])
              upAn[i][j] = float(data[2])
              wpAn[i][j] = float(data[3])
              ppAn[i][j] = float(data[4])
              rhopAn[i][j] = float(data[5])
              TpAn[i][j] = float(data[6])
              thetapAn[i][j] = float(data[7])
	upAnIntrp = interpolate.RectBivariateSpline(xAn, zAn, upAn)
	wpAnIntrp = interpolate.RectBivariateSpline(xAn, zAn, wpAn)
	ppAnIntrp = interpolate.RectBivariateSpline(xAn, zAn, ppAn)
	rhopAnIntrp = interpolate.RectBivariateSpline(xAn, zAn, rhopAn)
	TpAnIntrp = interpolate.RectBivariateSpline(xAn, zAn, TpAn)
	thetapAnIntrp = interpolate.RectBivariateSpline(xAn, zAn, thetapAn)
        pickle.dump(upAnIntrp, open("upAnIntrp_t"+str(time), "wb"))
        pickle.dump(wpAnIntrp, open("wpAnIntrp_t"+str(time), "wb"))
        pickle.dump(ppAnIntrp, open("ppAnIntrp_t"+str(time), "wb"))
        pickle.dump(rhopAnIntrp, open("rhopAnIntrp_t"+str(time), "wb"))
        pickle.dump(TpAnIntrp, open("TpAnIntrp_t"+str(time), "wb"))
        pickle.dump(thetapAnIntrp, open("thetapAnIntrp_t"+str(time), "wb"))
        print('done')

for i in range(6):
  t = 600 * i
  reloadAnalytical(t)


