function plotConv(deltat, iTime, spaceOrTime)
  legends = cell(4,1);
  legends(1,1)='\rho''';
  legends(2,1)='\rho u';
  legends(3,1)='\rho w';
  legends(4,1)='(\rho \theta)''';
  if (spaceOrTime(1:4) == 'spac')
    colors=['k','g','b','r'];
    begin_tri=[5,4,4,3];
    if (strcmp(deltat,'0.0001'))
      end_tri=[8,7,6,5];
    else
      end_tri=[7,6,5,4];
    endif
    isUp=[true,true,false,false];
    for order=1:4
      data = load(["convergence_deltat" deltat "/convData_p" num2str(order)]);
      h=300./8./(2.^(data(iTime:4:end,1)-1));
      plot_convergence(sqrt(data(iTime:4:end,3:6)),h,legends,colors(order),spaceOrTime,begin_tri(order),end_tri(order),isUp(order));
    end
    subplot(2,2,1)
    ax=get (gca, 'children');
    ax2=ax;
    ax2(17)=ax(5);
    ax2(5)=ax(17);
    ax2(18)=ax(10);
    ax2(10)=ax(18);
    ax2(19)=ax(15);
    ax2(15)=ax(19);
    set (gca, 'children', ax2) 
    legend('p1','p2','p3','p4','Location','NorthWest')
    matlab2tikz(['convergenceSpace_t' num2str(iTime) '.tikz'], \
                'height', '\figureheight', 'width', '\figurewidth', \
                'showInfo', false);
  else
    order=5
    #IMEXV
    data = load(["convergence_IMEXV/convData_p" num2str(order)]);
    h=0.45871559633027525./2.^(data(iTime:4:end,1));
    plot_convergence(sqrt(data(iTime:4:end,3:6)),h,legends,'k',spaceOrTime,1,3,false);
    #IMEXALL
    data = load(["convergence_IMEXALL/convData_p" num2str(order)]);
    h=7.792207792207792./2.^(data(iTime:4:end,1));
    plot_convergence(sqrt(data(iTime:4:end,3:6)),h,legends,'b',spaceOrTime,4,7,true);
    matlab2tikz(['convergenceTime_t' num2str(iTime) '.tikz'], \
                'height', '\figureheight', 'width', '\figurewidth', 'showInfo', false);
  endif

end