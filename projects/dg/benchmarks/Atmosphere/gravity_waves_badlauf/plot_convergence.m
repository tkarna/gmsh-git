function plot_convergence(fields,h,legends,color,type,begin_tri,end_tri,upTri) % type is either "space" or "time"
    %begin_tri: first point used for the convergence triangle
    %end_tri: last point used for the convergence triangle
    %upTri: should the triangle be above the line (true) or below (false)
    %Some parameters you can tune...
    %offset in ratio of the log full range
    offset=0; %So that the triangle is not on the line
    offset_text_x=.01; %slide text regards to the triangle (upper triangle)
    offset_text_y=.025;

    nbSubPlotLines=2;

    if strcmp(type,'space')
        legend_h='Horizontal mesh size [km]';
    elseif strcmp(type,'time')
        legend_h='Time step [s]';
    else
        error('Unknown type. Must be either ''space'' or ''time''');
    end
    
#    figure;
    
    nFields=length(legends(:,1));
    for iField=1:nFields
      field=fields(:,iField);
      nbCol=ceil(nFields/nbSubPlotLines);
      subplot(nbSubPlotLines,nbCol,iField);
      hold on;
      loglog(h,field,[color 'o-'],'MarkerFaceColor',color,'MarkerSize',2,'LineWidth',.5);
      ranges=axis;
      xRange = log(ranges(2))-log(ranges(1));
      yRange = log(ranges(4))-log(ranges(3));
      if (mod(iField,nbCol)==1)
        ylabel('L_2 error [-]');
      endif
      if (iField >= nFields-1)
        xlabel(legend_h);
      else
        xlabel('\dummy');
      endif
      title(legends(iField));
      convLine=polyfit (log10(h(begin_tri:end_tri)), log10(field(begin_tri:end_tri)), 1);
      A=convLine(1);
      B=convLine(2);
      h0 = h(begin_tri);
      h1 = h(end_tri);
      f0 = 10^(A*log10(h0)+B);
      f1 = 10^(A*log10(h1)+B);
      if (upTri == true)
        line([h0 h1],[f0 f1]*10^offset,'Color',color,'LineStyle',':');
        line([h0 h1],[f0 f0]*10^offset,'Color',color,'LineStyle',':');
        line([h1 h1],[f0 f1]*10^offset,'Color',color,'LineStyle',':');
        text(h1*10^(offset_text_x*xRange),f0*10^(offset*yRange)*10^(-offset_text_y*yRange),num2str(A,'%.1f'),'Color',color);
      else
        line([h0 h1],[f0 f1]*10^offset,'Color',color,'LineStyle',':');
        line([h0 h1],[f1 f1]*10^(-offset),'Color',color,'LineStyle',':');
        line([h0 h0],[f1 f0]*10^(-offset),'Color',color,'LineStyle',':');
        text(h0*10^(offset_text_x*xRange),f1*10^(-offset*yRange)*10^(offset_text_y*yRange),num2str(A,'%.1f'),'Color',color);
      endif
   end
end
