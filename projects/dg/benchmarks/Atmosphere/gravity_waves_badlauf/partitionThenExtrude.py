from partition1d import *
from dgpy import *
from extrude import *
from rotateZtoY import *
from periodicMap import *


def partitionThenExtrude(nbPart,iConv):	

  nbLayers = 1*(2**iConv)
  H = 10000.
  
  res=H/nbLayers

  name='line'

  def getLayersSigma (element, vertex) :
    z=0
    zTab=[z]
    for i in range(nbLayers):
      z=z-res
      zTab.append(z)
    return zTab
	
	
  if nbPart>1:
    partStr='_part_%i' % nbPart
  else:
    partStr=''
  partition1d(name+'.msh', name + partStr + '.msh', nbPart)
  extrude(mesh(name + partStr + '.msh'), getLayersSigma).write(name + partStr + '_2d.msh')
  rotateZtoY(name + partStr + '_2d.msh',name + partStr + '_2d_XY.msh')

  def shiftOperation(node, iPerBound) :
    n = [node[0]+300000, node[1], node[2]]
    return n
  
  #Periodic info
  if(Msg.GetCommRank()==0):
    periodicBoundaryNumber = 1  
    # tag of the boundaries that will be cut
    cutTags = ["left"]
    # tag of the boundaries that will pe pasted
    pasteTags = ["right"]
    # output file
    periodicMap(name + partStr + '_2d_XY.msh', name + partStr + '_2d_XY_periodic.msh', periodicBoundaryNumber, shiftOperation, cutTags, pasteTags, "periodicMesh.txt")



