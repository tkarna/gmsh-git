from dgpy import *
from initialCondition import *
import time, os, sys
import gmshPartition

CCode = """
#include "fullMatrix.h"
#include "function.h"
#include "dgGroupOfElements.h"
extern "C" {

void sponge (dataCacheMap *, fullMatrix<double> &FCT, fullMatrix<double> &XYZ, fullMatrix<double> &sol, fullMatrix<double> &reference) {
    double g=9.80616;
    for (int i=0; i<XYZ.size1(); i++){
        double z=XYZ(i,2);
        double zb=17000;
        double zt=31000;
        double alpha=0.01;
        double tau = 0;
        if (z>=zb)
           tau=-alpha/2*(1+cos(((z-zb)/(zt-zb)-1)*M_PI));
        double Bzb=0;
        double Bzt=2000;
        double Balpha=.00;
        if (z<=Bzt)
           tau=-Balpha/2*(1+cos(((Bzb-z)/(Bzb-Bzt)-1)*M_PI));
        FCT.set(i,0,tau*(sol(i,0)-reference(i,0)));
        FCT.set(i,1,tau*(sol(i,1)-reference(i,1)));
        FCT.set(i,2,tau*(sol(i,2)-reference(i,2)));
        FCT.set(i,3,tau*(sol(i,3)-reference(i,3)));
        FCT.set(i,4,tau*(sol(i,4)-reference(i,4)));
   }
}
void sensor0 (dataCacheMap *, fullMatrix<double> &FCT, fullMatrix<double> &sol) {
   for (int i=0; i<FCT.size1(); i++){
       FCT.set(i, 0, sol(i,0));
   }
}
void sensor12 (dataCacheMap *, fullMatrix<double> &FCT, fullMatrix<double> &sol) {
   for (int i=0; i<FCT.size1(); i++){
       FCT.set(i, 0, sqrt(sol(i,1)*sol(i,1)+sol(i,2)*sol(i,2)));
   }
}

void sensor3 (dataCacheMap *, fullMatrix<double> &FCT, fullMatrix<double> &sol) {
   for (int i=0; i<FCT.size1(); i++){
       FCT.set(i, 0, sol(i,3));
   }
}
void sensor4 (dataCacheMap *, fullMatrix<double> &FCT, fullMatrix<double> &sol) {
   for (int i=0; i<FCT.size1(); i++){
       FCT.set(i, 0, sol(i,4));
   }
}

void vecFromScal(dataCacheMap *, fullMatrix<double> &FCT, fullMatrix<double> &s0,fullMatrix<double> &s1,fullMatrix<double> &s2,fullMatrix<double> &s3,fullMatrix<double> &s4){
   for (int i = 0; i < FCT.size1(); i++){
        FCT.set(i,0,s0(i,0));
        FCT.set(i,1,s1(i,0));
        FCT.set(i,2,s2(i,0));
        FCT.set(i,3,s3(i,0));
        FCT.set(i,4,s4(i,0));
   }
}


void getError(dataCacheMap *, fullMatrix<double> &FCT, fullMatrix<double> &analytic, fullMatrix<double> &numerical) {
   for (int i=0; i<FCT.size1(); i++){
      for (int j=0; j<FCT.size2(); j++){
         FCT.set(i,j,(analytic(i,j)-numerical(i,j))*(analytic(i,j)-numerical(i,j)));
      }
   }
}
void getSquare(dataCacheMap *, fullMatrix<double> &FCT, fullMatrix<double> &inFct) {
   for (int i=0; i<FCT.size1(); i++){
      for (int j=0; j<FCT.size2(); j++){
         FCT.set(i,j,(inFct(i,j)*inFct(i,j)));
      }
   }
}
void velocity3D(dataCacheMap *cacheMap, fullMatrix<double> &F, fullMatrix<double> &sol, fullMatrix<double> &stereo){
    double R = 6371229;
    double rev = 1;
    for (int i = 0; i < sol.size1(); i++){
        if (cacheMap->getGroupOfElements()->getPhysicalTag() == "volume_Bottom")
            rev = -1;
        //Horizontal components
        double xi =stereo(i,0);
        double beta =stereo(i,1);
        double gam = stereo(i,2);
        double vxi = sol(i,1);
        double vbeta = sol(i,2);
        double vgamma = sol(i,3);
        double ux = (1-(2*xi*xi)/(4*R*R+xi*xi + beta*beta)) * vxi - 2*beta*xi/(4*R*R+xi*xi + beta*beta) * vbeta + 4*R*xi/(4*R*R+xi*xi + beta*beta) * vgamma;
        double uy = - 2*beta*xi/(4*R*R+xi*xi + beta*beta) * vxi + (1-(2*beta*beta)/(4*R*R+xi*xi + beta*beta)) * vbeta + 4*R*beta/(4*R*R+xi*xi + beta*beta) * vgamma;
        double uz = -4*R*xi*rev/(4*R*R+xi*xi + beta*beta) * vxi - 4*R*beta*rev/(4*R*R+xi*xi + beta*beta) * vbeta - rev*((-4*R*R+xi*xi+beta*beta)/(4*R*R+xi*xi + beta*beta)) * vgamma;
        //Both
        F.set(i,0,ux);
        F.set(i,1,uy);
        F.set(i,2,uz);
   }
}
}
"""

if (Msg.GetCommRank()==0):
    functionC.buildLibrary (CCode, "./ccode.so",True);
Msg.Barrier()

#### Begin and end time ###
Ti=0.0
Tf=30*24*3600
###########################


dimension=3

integOrder=2*order+1+2
dataCacheMap.setDefaultIntegrationOrder(integOrder)

model = GModel()

name='aquaplanet'
if Msg.GetCommSize()>1:
  partStr='_part_%i' % Msg.GetCommSize()
else:
  partStr=''
model.load(name + partStr + '_3d.msh')


groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag();
circleTransform = dgCircleTransform(groups)

st = dgSpaceTransformSpherical(R, groups)
#st = dgSpaceTransformSpherical(R)

groups._mesh.setSpaceTransform(st, groups)
groupsH = dgGroupCollection.newByTag(model, dimension-1, order, ["bottom_Top","bottom_Bottom"])
extrusion = dgExtrusion(groups, groupsH, ["bottom_Top","bottom_Bottom"])
groups.splitFaceGroupsByOrientation(extrusion, ["bottom_Top","bottom_Bottom","top_Top", "top_Bottom"]) 

XYZ = groups.getFunctionCoordinates();
xyzCircle = transformToCircle(circleTransform)

xyzCircleInteg = functionPrecomputed(groups, integOrder, 3)
xyzCircleInteg.compute(xyzCircle)


fCor_slow=functionC("./ccodeInitial.so","fCoriolis",1,[xyzCircle])
fCor = functionPrecomputed(groups, integOrder, 1)
fCor.compute(fCor_slow)


claw = dgEulerAtmLaw(dimension)
claw.setFilterMode(FILTER_LINEARVERTICAL);
#claw.setFilterMode(FILTER_LINEAR);
claw.setCoriolisFactor(fCor)


solution = dgDofContainer(groups, claw.getNbFields())
        


amplFact = functionConstant(500)
cartXYZ=functionC("./ccodeInitial.so","cartCoord",3,[xyzCircle, amplFact])


cartVel=functionC("./ccode.so","velocity3D",3,[solution.getFunction(), xyzCircle])

initF=functionC("./ccodeInitial.so","initialCondition",5,[xyzCircle])

claw.setCoordinatesFunction(xyzCircleInteg)

solution.L2Projection(initF)
#solution.interpolate(initF)

rhoHsFct = functionPrecomputed(groups, integOrder, 1)
rhoHsFct.compute(functionC("./ccodeInitial.so","rhoHydrostatic",1,[xyzCircle]))

rhoThetaHsFct = functionPrecomputed(groups, integOrder, 1)
rhoThetaHsFct.compute(functionC("./ccodeInitial.so","rhoThetaHydrostatic",1,[xyzCircle]))

rhoHs = dgDofContainer(groups, 1)
rhoHs.interpolate(functionC("./ccodeInitial.so","rhoHydrostatic",1,[xyzCircle]))
rhoThetaHs = dgDofContainer(groups, 1)
rhoThetaHs.interpolate(functionC("./ccodeInitial.so","rhoThetaHydrostatic",1,[xyzCircle]))


#claw.setHydrostaticState(rhoHsFct, rhoThetaHsFct)#rhoHs.getFunction(),rhoThetaHs.getFunction())
claw.setHydrostaticState(rhoHs,rhoThetaHs)
claw.setPhysicalConstants(gamma,Rd,p0,g,R)

trms = claw.getActiveTerms();
trms.setAdvV(True);
trms.setAdvT(True);
trms.setPGrad(True);
trms.setVDiv(True);
trms.setCor(True);
trms.setGrav(True);
trms.setDiff(True);
trms.setLax(True);




initDof = dgDofContainer(groups, 5)
initDof.L2Projection(initF)
#initDof.interpolate(initF)


toReplace = [function.getSolution(), function.getSolutionGradient()]
replaceBy = [initDof.getFunction(), initDof.getFunctionGradient()]
outsideBoundary = claw.newOutsideValueBoundaryGeneric("",toReplace,replaceBy)

boundaryWall = claw.newBoundaryWall()
#BOTTOM BND
claw.addBoundaryCondition('top_Top', outsideBoundary)#
claw.addBoundaryCondition('top_Bottom', outsideBoundary)#

#This is actually the top boundary (extrusion made for ocean from top to bottom)
#outsideBoundary = claw.newOutsideValueBoundary("",initDof.getFunction())
claw.addBoundaryCondition('bottom_Top', outsideBoundary)
claw.addBoundaryCondition('bottom_Bottom', outsideBoundary)

#claw.addBoundaryCondition('bottom_Top', boundaryWall)
#claw.addBoundaryCondition('bottom_Bottom', boundaryWall)



spongeTerm=functionC("./ccode.so","sponge",5,[xyzCircleInteg, function.getSolution(), initDof.getFunction()])
claw.setSource("", spongeTerm);




c = sqrt(gamma * p0 / 1)


sensorF0=functionC("./ccode.so","sensor0",1,[solution.getFunction()])
sensorF12=functionC("./ccode.so","sensor12",1,[solution.getFunction()])
sensorF3=functionC("./ccode.so","sensor3",1,[solution.getFunction()])
sensorF4=functionC("./ccode.so","sensor4",1,[solution.getFunction()])

#nuStab = dgJumpDiffusion(groups, 4, 2 * order + 1, c, 10, initDof)
#nu = nuStab.diffusivity().getFunction()

nuStab0 = dgArtificialDiffusion(groups, sensorF0, integOrder, 0.03, 1000);
nuStab12 = dgArtificialDiffusion(groups, sensorF12, integOrder, 0.05, 1000);
nuStab3 = dgArtificialDiffusion(groups, sensorF3, integOrder, 0.05, 1000);
nuStab4 = dgArtificialDiffusion(groups, sensorF4, integOrder, 0.05, 1000);

nuStab0.apply(solution)
nuStab12.apply(solution)
nuStab3.apply(solution)
nuStab4.apply(solution)


#nuh=functionC("./ccode.so","vecFromScal", 5, [nuStab0.nuh().getFunction(),nuStab12.nuh().getFunction(),nuStab12.nuh().getFunction(),nuStab3.nuh().getFunction(),nuStab4.nuh().getFunction()])
#nuv=functionC("./ccode.so","vecFromScal", 5, [nuStab0.nuv().getFunction(),nuStab12.nuv().getFunction(),nuStab12.nuv().getFunction(),nuStab3.nuv().getFunction(),nuStab4.nuv().getFunction()])

#nuh = nuStab0.nuh().getFunction()
#nuv = nuStab0.nuv().getFunction()

nuh = functionConstant(0)
nuv = functionConstant(0)

#nuhGrad = functionConstant([0, 0, 0,0, 0, 0,0, 0, 0,0, 0, 0,0, 0, 0])
#nuvGrad = functionConstant([0, 0, 0,0, 0, 0,0, 0, 0,0, 0, 0,0, 0, 0])

nuhGrad = functionConstant([0, 0, 0])
nuvGrad = functionConstant([0, 0, 0])


zero = functionConstant([0])

claw.setNu(nuh, nuv, nuhGrad, nuvGrad)

#claw.setNu(nuhF, nuvF, nuhGrad, nuvGrad)


#claw.addBoundaryCondition('Seam', boundaryWall)


error=functionC("./ccode.so","getError",5,[initF, solution.getFunction()])
analyticSq = functionC("./ccode.so","getSquare",5,[initF])

integratorError = dgFunctionIntegrator(groups, error)
intErr = fullMatrixDouble(5,1)
integratorAnalyticSq = dgFunctionIntegrator(groups, analyticSq)
intAnalyticSq = fullMatrixDouble(5,1)


def exportFct(FCT,vel3d,stereo, rhoThetaHs, sol, nuh, nuv, faccor,error):
    for i in range(0,FCT.size1()):
        #rhop
        FCT.set(i,0,sol(i,0))
        #rhouvw
        FCT.set(i,1,vel3d(i,0))
        FCT.set(i,2,vel3d(i,1))
        FCT.set(i,3,vel3d(i,2))
        FCT.set(i,4,sol(i,3))
        #rhothetap
        FCT.set(i,5,(sol(i,4)))
        FCT.set(i,6,error(i,0))
        FCT.set(i,7,error(i,4))
        FCT.set(i,8,error(i,1)*error(i,1)+error(i,2)*error(i,2)+error(i,3)*error(i,3))
        FCT.set(i,9,nuh(i,0))
        FCT.set(i,10,nuv(i,0))

nCompExp=[1,3,1,1,1,1,1,1,1]
namesExp=["rhop","rhouvw","rhow","rhothetap","errorRhop", "errorRhoThetaP","errorUvw","nuh","nuv"]
exportFunction=functionPython(sum(nCompExp), exportFct, [cartVel,xyzCircle, rhoThetaHs.getFunction(), solution.getFunction(), nuh, nuv, fCor_slow, error])#rhoThetaHs.getFunction()])



#timeIter = dgERK(claw, None, DG_ERK_22)
#petscIm = dgLinearSystemExtrusion(claw, groups, extrusion)
##
petscIm = linearSystemPETScBlockDouble()
##
#petscIm.setParameter("petscOptions","-ksp_type preonly -pc_type lu -pc_factor_mat_solver_package mumps")
petscIm.setParameter("petscOptions",  "-ksp_atol 0 -ksp_rtol 1e-4")
##
dofIm = dgDofManager.newDGBlock(groups, claw.getNbFields(), petscIm)
#timeIter = dgDIRK(claw, dofIm, 1)
timeIter = dgIMEXRK(claw, dofIm, 2)     #timeorder
timeIter.getNewton().setAtol(1.e-14) #ATol
timeIter.getNewton().setRtol(1.e-4) #Rtol
timeIter.getNewton().setVerb(1)     #Verbosity

sBV = 2.0/3*(order+1.0)
muBV = 0.2
filterBV = dgFilterBoydVandeven(groups, "", sBV, muBV)

#dt=claw.getMinOfTimeSteps(solution)
dt = min(claw.getMinOfTimeSteps(solution, extrusion),200)*.8

#dt = 7200
if (Msg.GetCommRank()==0):
    print ("Time step:",dt)
nbSteps = int(ceil(Tf/dt))

#initial solution
solution.exportFunctionVtk(exportFunction,'/scratch/sblaise/steadystate/output', 0, 0,"solution",nCompExp,namesExp,cartXYZ)
#claw.setImexMode(IMEX_IMPLICIT);
t=Ti
n_export=0
start = time.clock()
outFile = open('ErrorVsTime','w')
#outFile.write("t rhop rhoU rhoV rhoThetap\n")
outFile.close()
timerAll = dgTimer.root();
for i in range(0,nbSteps):
  if (i == 2):
    timerAll.start();
  if (i%100 == 0 or i==nbSteps-1):
    integratorError.compute(intErr)
    integratorAnalyticSq.compute(intAnalyticSq)
    solution.exportFunctionVtk(exportFunction,'/scratch/sblaise/steadystate/output', t, i,"solution",nCompExp,namesExp,cartXYZ)
    if (Msg.GetCommRank()==0):
      print ('\nWriting output',n_export,'at time',t,'and step',i,'over',nbSteps)
      outFile = open('ErrorVsTime','a')
      errRhop=intErr(0,0)/intAnalyticSq(0,0)
      errRhoU=intErr(1,0)/intAnalyticSq(1,0)
      errRhoV=intErr(2,0)/intAnalyticSq(2,0)
      errRhoThetap=intErr(4,0)/intAnalyticSq(4,0)
      print ("Error on rhop:",errRhop,"rhoU:", errRhoU,"rhoV:",errRhoV,"RhoThetap:",errRhoThetap)
      outFile.write("%s %s %s %s %s\n" % (t/3600, errRhop, errRhoU, errRhoV, errRhoThetap))
      outFile.close()
      elapsed = (time.clock() - start)
      print ('Time elapsed: ',elapsed,' s')
      n_export=n_export+1
  norm = timeIter.iterate (solution, dt, t)
  nuStab0.apply(solution)
  nuStab12.apply(solution)
  nuStab3.apply(solution)
  nuStab4.apply(solution)
  t=t+dt
  if (Msg.GetCommRank()==0):
    sys.stdout.write('.')
    sys.stdout.flush()
print ('')    
elapsed = (time.clock() - start)
if (Msg.GetCommRank()==0):
  print ('Time elapsed: ',elapsed,' s')

timerAll.stop();

timerAll.printFull()

Msg.Exit(0)


