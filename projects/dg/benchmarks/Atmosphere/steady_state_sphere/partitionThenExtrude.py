from gmshPartition import *
from dgpy import *
from extrude import *
from rotateZtoY import *
from gmshpy import *
from initialCondition import *
from ctypes import *

libInitial=cdll.LoadLibrary("./ccodeInitial.so")
libInitial.PhiInit.restype = c_double;

name='aquaplanet'
nbPart = int(sys.argv[1])

nbLayers = 20
H = 20000 # This is not actually the height (etaTop defines maximum height), wut we need something large enough for generating the first mesh not too anisotropic
etaTop = 0.1


def getLayersSigma (element, vertex) :
  return [-z * H /nbLayers for z in  range(nbLayers + 1)]

if nbPart>1:
  partStr='_part_%i' % nbPart
else:
  partStr=''

model = GModel()
model.load(name+".msh")
if (order != 1):
  model.setOrderN(order, 1, 0)
model.save(name+"_order.msh")
model.load(name+"_order.msh")
pOpt = meshPartitionOptions()
pOpt.setNumOfPartitions(nbPart)
PartitionMesh(model,  pOpt)   
model.save(name+"_part.msh")

extrude(mesh(name+"_part.msh"), getLayersSigma).write(name+"_part_3d.msh")

model3d = GModel()
model3d.load(name+"_part_3d.msh")
groups = dgGroupCollection(model3d, 3, order)
groups.splitGroupsByPhysicalTag()
circleTransform = dgCircleTransform(groups)
XYZ = groups.getFunctionCoordinates();
xyzCircle = transformToCircle(circleTransform)



# 1 for NODE_MODE 
cacheMap = dataCacheMap(dataCacheMap.NODE_MODE, groups);

tr = dgSpaceTransformSpherical(R);

oldCoords = []
for iGroup in range(groups.getNbElementGroups()):
  group = groups.getElementGroup(iGroup)
  cacheMap.setGroup(group)
  f = cacheMap.get(xyzCircle)
  groupCoords = []
  for iElem in range(group.getNbElements()):
    elem = group.getElement(iElem)
    cacheMap.setElement(iElem)
    coords = f.get()
    elemCoords = []
    for iVertex in range(elem.getNumVertices()):
      elemCoords.append([coords(iVertex, 0), coords(iVertex, 1), coords(iVertex, 2)])
    groupCoords.append(elemCoords)
  oldCoords.append(groupCoords)


for iGroup in range(groups.getNbElementGroups()):
  group = groups.getElementGroup(iGroup)
  cacheMap.setGroup(group)
  for iElem in range(group.getNbElements()):
    elem = group.getElement(iElem)
    cacheMap.setElement(iElem)
    for iVertex in range(elem.getNumVertices()):
      vertex = elem.getVertex(iVertex)
      xSt = oldCoords[iGroup][iElem][iVertex][0]
      ySt = oldCoords[iGroup][iElem][iVertex][1]
      zSt = oldCoords[iGroup][iElem][iVertex][2]
      zInMin = 1e99
      zInMax = -1e99
      zOutMin = 1e99
      zOutMax = -1e99
      #for iVertex2 in range(elem.getNumVertices()):
      #  xSt2 = oldCoords[iGroup][iElem][iVertex2][0]
      #  ySt2 = oldCoords[iGroup][iElem][iVertex2][1]
      #  zSt2 = oldCoords[iGroup][iElem][iVertex2][2]
      #  if ((xSt2-xSt)**2 + (ySt2-ySt)**2 < 1):
      #    zInMin = min(zSt2, zInMin)
      #    zInMax = max(zSt2, zInMax)
      #    x, y, z = tr.stereo2Cart(xSt2,ySt2,zSt2, cacheMap)
      #    xRot, yRot, zRot = tr.rotateCartX(x, y, z, alpha)
      #    lambd, phi, r = tr.cart2LonLat(xRot, yRot, zRot)
      #    eta = 1 - abs(zSt2 / H) * (1-etaTop)
      #    Phi = libInitial.PhiInit(c_double(lambd), c_double(phi), c_double(eta), c_double(u0))
      #    zSt2 = Phi / g
      #    zOutMin = min(zSt2, zOutMin)
      #    zOutMax = max(zSt2, zOutMax)


      x, y, z = tr.stereo2Cart(xSt,ySt,zSt, cacheMap)
      xRot, yRot, zRot = tr.rotateCartX(x, y, z, alpha)
      lambd, phi, r = tr.cart2LonLat(xRot, yRot, zRot)
      eta = 1 - abs(zSt / H) * (1-etaTop)
      Phi = libInitial.PhiInit(c_double(lambd), c_double(phi), c_double(eta), c_double(u0))
      zSt = Phi / g

      #Phi = PhiInit(pi, 0, eta, u0)
      #if (zSt > 1e-5):
      #  zSt = Phi / g

#      zSt = zOutMin + (zSt - zInMin) * (zOutMax - zOutMin) / (zInMax - zInMin)
      vertex.setXYZ(xSt, ySt, zSt)

         

model3d.save(name + partStr + '_3d.msh')



Msg.Exit(0) 
