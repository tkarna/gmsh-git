from gmshPartition import *
from dgpy import *
from extrude import *
from rotateZtoY import *
from gmshpy import *


name='square'

squareModel = GModel()
squareModel.load(name+".geo")
squareModel.mesh(2)
squareModel.save(name+".msh")


nbPart = int(sys.argv[1])

nbLayers = 10
H = 25000.

res=H/nbLayers

def getLayersSigma (element, vertex) :
  z=0
  zTab=[z]
  for i in range(nbLayers):
    z=z-res
    zTab.append(z)
  return zTab

if nbPart>1:
  partStr='_part_%i' % nbPart
else:
  partStr=''

model = GModel()
model.load(name+".msh")
pOpt = meshPartitionOptions()
pOpt.setNumOfPartitions(nbPart)
PartitionMesh(model,  pOpt)
model.save(name + partStr + '.msh')

extrude(mesh(name + partStr + '.msh'), getLayersSigma).write(name + partStr + '_3d.msh')
Msg.Exit(0) 
