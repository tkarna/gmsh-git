from dgpy import *
import time, os, sys
from math import *

CCode = """
#include "GmshMessage.h"
#include "fullMatrix.h"
#include "function.h"
#include "dgGroupOfElements.h"
#include <cstdlib>

extern "C" {

double TInit(double lambd, double phi, double eta, double u0p){
  double gamma=1.4;
  double Rd=287.04;
  double g=9.80616;
  double Cv=Rd/(gamma-1.0);
  double Cp=Rd*(1.0+1.0/(gamma-1.0));
  double kappa=Rd/Cp;
  double R = 6371229;
  double eta0 = 0.252;
  double etaS = 1;
  double etaT = 0.2;
  double p0=1.0e5;
  double T0 = 288;
  double deltaT = 4.8e5;
  double Tau = 0.005;
  double Omega = 7.29212e-5;
  double etaV, Tbar;
  etaV = (eta - eta0) * M_PI / 2;
  if (eta >= etaT)
    Tbar = T0 * pow(eta, (Rd * Tau / g));
  else
    Tbar = T0 * pow(eta, (Rd * Tau / g)) + deltaT * pow(etaT - eta, 5);
  return Tbar + 3.0/4 * eta * M_PI * u0p / Rd * sin(etaV) * pow(cos(etaV), 0.5) 
    * ( (-2 * pow(sin(phi), 6) * (pow(cos(phi), 2) + 1.0/3) + 10.0/63) * 2 * u0p * pow(cos(etaV), 1.5) 
	+ (8.0/5 * pow(cos(phi), 3) * (pow(sin(phi), 2) + 2.0/3) - M_PI/4 ) * R * Omega );
}

double PhiInit(double lambd, double phi, double eta, double u0p){
  double gamma=1.4;
  double Rd=287.04;
  double g=9.80616;
  double Cv=Rd/(gamma-1.0);
  double Cp=Rd*(1.0+1.0/(gamma-1.0));
  double kappa=Rd/Cp;
  double R = 6371229;
  double eta0 = 0.252;
  double etaS = 1;
  double etaT = 0.2;
  double p0=1.0e5;
  double T0 = 288;
  double deltaT = 4.8e5;
  double Tau = 0.005;
  double Omega = 7.29212e-5;
  double etaV, etaBar;
  etaV = (eta - eta0) * M_PI / 2;
  if (eta >= etaT)
    etaBar = T0 * g / Tau * (1 - pow(eta, (Rd * Tau / g)));
  else
    etaBar = T0 * g / Tau * (1 - pow(eta, (Rd * Tau / g))) - Rd * deltaT
      * ((log(eta/etaT) + 137.0/60) * pow(etaT, 5) - 5 * pow(etaT, 4) * eta + 5 * pow(etaT, 3) * pow(eta, 2) - 10.0/3 * pow(etaT, 2) * pow(eta, 3) + 5.0/4 * etaT * pow(eta, 4) - 1.0/5 * pow(eta, 5));
  return etaBar + u0p * pow(cos(etaV), 1.5)
    * ( (-2 * pow(sin(phi),6) * (pow(cos(phi),2) + 1.0/3) + 10.0/63) * u0p * pow(cos(etaV),1.5)
	+ (8.0/5 * pow(cos(phi),3) * (pow(sin(phi),2) + 2.0/3) - M_PI/4) * R * Omega );
}

double getEta(double lambd, double phi, double z, double u0p){
  double g=9.80616;
  double Rd=287.04;
  //Compute with iterations to find pressure-based coordinates
  double eta_old = 1e99;
  double eta = 1e-7;
  int nbIter=0;
  double T, Phi;
  while (fabs(eta - eta_old) > 1e-14){
    if (nbIter > 100){
      printf(\"z to eta coordinates conversion not converged after 100 iterations\");
    }
    eta_old = eta;
    T = TInit(lambd, phi, eta, u0p);
    Phi = PhiInit(lambd, phi, eta, u0p);
    eta = eta - (- g * z + Phi) / (- Rd / eta * T);
    nbIter = nbIter + 1;
    if (eta < 0){
      printf(\"Unphysical value for eta: %e (lambda=%e phi=%e z=%e gz=%e T=%e Phi=%e)\",eta, lambd, phi, z, g*z, T,Phi);
    }
  }
  return eta;
}

void initialCondition(dataCacheMap *cacheMap, fullMatrix<double> &F, fullMatrix<double> &XYZ){
  double p0=1.0e5;
  double gamma=1.4;
  double Rd=287.04;
  double Cp=Rd*(1.0+1.0/(gamma-1.0));
  double eta;
  double u0 = 35;
  double lambda=0;
  double phi=M_PI/2;
  double g=9.80616;
  double p, theta, rho, etaHs, THs, pHs, thetaHs, rhoHs;
    for (int i = 0; i < F.size1(); i++){
    eta = getEta(lambda, phi, XYZ(i, 2), u0);
    double T = TInit(lambda, phi, eta, u0);
    p = eta * p0;
    theta = T * pow(p0/p,Rd/Cp);
    rho = pow(p / p0,1.0/gamma) * p0/(theta * Rd);
    etaHs = getEta(lambda, phi, XYZ(i,2),0);
    THs = TInit(lambda, phi, etaHs, 0);
    pHs = etaHs * p0;
    thetaHs = THs * pow(p0/pHs,Rd/Cp);
    rhoHs = pow(pHs / p0,1.0/gamma) * p0/(thetaHs * Rd);

    //Value for z=0
    double etaB = getEta(lambda, phi, 0, u0);
    double pB = etaB * p0;
    double etaHsB = getEta(lambda, phi, 0,0);
    double pHsB = etaHsB * p0;
    

    F(i, 0) = -((p-pHs)/g - (pB-pHsB)/g); //rho-rhoHs;
//    F(i, 0) = rho-rhoHs;
    F(i, 1) = 0;
    F(i, 2) = 0;
    F(i, 3) = 0;
    F(i, 4) = rho*theta - rhoHs*thetaHs;
   }
}


void rhoHydrostatic(dataCacheMap *cacheMap, fullMatrix<double> &F, fullMatrix<double> &XYZ){
  double p0=1.0e5;
  double gamma=1.4;
  double Rd=287.04;
  double Cp=Rd*(1.0+1.0/(gamma-1.0));
  double lambda=0;
  double phi=M_PI/2;
  double etaHs, THs, pHs, thetaHs, rhoHs;
  for (int i = 0; i < F.size1(); i++){
    etaHs = getEta(lambda, phi, XYZ(i,2),0);
    THs = TInit(lambda, phi, etaHs, 0);
    pHs = etaHs * p0;
    thetaHs = THs * pow(p0/pHs,Rd/Cp);
    rhoHs = pow(pHs / p0,1.0/gamma) * p0/(thetaHs * Rd);
    F(i, 0) = rhoHs;
  }
}

void rhoThetaHydrostatic(dataCacheMap *cacheMap, fullMatrix<double> &F, fullMatrix<double> &XYZ){
  double p0=1.0e5;
  double gamma=1.4;
  double Rd=287.04;
  double Cp=Rd*(1.0+1.0/(gamma-1.0));
  double lambda=0;
  double phi=M_PI/2;
  double etaHs, THs, pHs, thetaHs, rhoHs;
  for (int i = 0; i < F.size1(); i++){
    etaHs = getEta(lambda, phi, XYZ(i,2),0);
    THs = TInit(lambda, phi, etaHs, 0);
    pHs = etaHs * p0;
    thetaHs = THs * pow(p0/pHs,Rd/Cp);
    rhoHs = pow(pHs / p0,1.0/gamma) * p0/(thetaHs * Rd);
    F(i, 0) = rhoHs*thetaHs;
  }
}

void exportCoord(dataCacheMap *cacheMap, fullMatrix<double> &F, fullMatrix<double> &XYZ){
  for (int i = 0; i < F.size1(); i++){
    F(i, 0) = XYZ(i, 0);
    F(i, 1) = XYZ(i, 1);
    F(i, 2) = XYZ(i, 2)*50;
  }
}
}
"""

if (Msg.GetCommRank()==0):
    functionC.buildLibrary (CCode, "./ccode.so",True);
Msg.Barrier()

Ti = 0
Tf = 30*24*3600

dimension = 3
order = 2
integOrder=2*order+1
dataCacheMap.setDefaultIntegrationOrder(integOrder)

model = GModel()

name='square'
if Msg.GetCommSize()>1:
  partStr='_part_%i' % Msg.GetCommSize()
else:
  partStr=''
model.load(name + partStr + '_3d.msh')


groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag();

groupsH = dgGroupCollection.newByTag(model, dimension-1, order, ["bottom_surf"])
extrusion = dgExtrusion(groups, groupsH, ["bottom_surf"])
groups.splitFaceGroupsByOrientation(extrusion, ["bottom_surf","top_surf"]) 

XYZ = groups.getFunctionCoordinates();

claw = dgEulerAtmLaw(dimension)
claw.setFilterMode(FILTER_LINEAR);
solution = dgDofContainer(groups, claw.getNbFields())

trms = claw.getActiveTerms();
trms.setAdvV(False);
trms.setAdvT(True);
trms.setPGrad(True);
trms.setVDiv(True);
trms.setCor(False);
trms.setGrav(True);
trms.setDiff(False);
trms.setLax(True);
trms.setRhoPot(True);



initF=functionC("./ccode.so","initialCondition",claw.getNbFields(),[XYZ])
solution.interpolate(initF)
rhoHs = dgDofContainer(groups, 1)
rhoHs.interpolate(functionC("./ccode.so","rhoHydrostatic",1,[XYZ]))
rhoThetaHs = dgDofContainer(groups, 1)
rhoThetaHs.interpolate(functionC("./ccode.so","rhoThetaHydrostatic",1,[XYZ]))

gamma=1.4
Rd=287.04
p0=1.0e5
g=9.80616
R = -1
claw.setHydrostaticState(rhoHs,rhoThetaHs)
claw.setPhysicalConstants(gamma,Rd,p0,g,R)


initDof = dgDofContainer(groups, claw.getNbFields())
initDof.interpolate(initF)

toReplace = [function.getSolution(), function.getSolutionGradient()]
replaceBy = [initDof.getFunction(), initDof.getFunctionGradient()]# functionConstant([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])]#, function.getSolutionGradient()]#, initDof.getFunctionGradient()]#Dof.getFunction(), initDof.getFunctionGradient()]
outsideBoundary = claw.newOutsideValueBoundaryGeneric("",toReplace,replaceBy)
boundaryWall = claw.newBoundaryWall()
claw.addBoundaryCondition('top_surf', boundaryWall)#Top is bottom...
claw.addBoundaryCondition('bottom_surf', outsideBoundary)#
claw.addBoundaryCondition('bottom', boundaryWall)#
claw.addBoundaryCondition('right', boundaryWall)#
claw.addBoundaryCondition('top', boundaryWall)#
claw.addBoundaryCondition('left', boundaryWall)#

petscIm = linearSystemPETScBlockDouble()
#petscIm.setParameter("petscOptions","-ksp_type preonly -pc_type lu -pc_factor_mat_solver_package mumps")
petscIm.setParameter("petscOptions",  "-ksp_atol 0 -ksp_rtol 1e-4")
dofIm = dgDofManager.newDGBlock(groups, claw.getNbFields(), petscIm)
#timeIter = dgDIRK(claw, dofIm, 2)
timeIter = dgIMEXRK(claw, dofIm, 2)     #timeorder
timeIter.getNewton().setAtol(1.e-8) #ATol
timeIter.getNewton().setRtol(1.e-5) #Rtol
timeIter.getNewton().setVerb(1)     #Verbosity
dt = min(claw.getMinOfTimeSteps(solution, extrusion),200)*.8
dt=5
if (Msg.GetCommRank()==0):
    print ("Time step:",dt)
nbSteps = int(ceil(Tf/dt))


#claw.setImexMode(IMEX_IMPLICIT);
t=Ti

def exportFct(FCT,sol,initialCond,rhoThetaHs):
    for i in range(0,FCT.size1()):

        rhoTheta = sol(i,4) + rhoThetaHs(i,0);
        p = p0 * (rhoTheta * Rd / p0)**gamma;
        pHs = p0 * (rhoThetaHs(i,0) * Rd / p0)**gamma;
        pp = p - pHs;


        FCT.set(i,0,sol(i,0))
        FCT.set(i,1,sol(i,1))
        FCT.set(i,2,sol(i,2))
        FCT.set(i,3,sol(i,3))
        FCT.set(i,4,sol(i,4))
        FCT.set(i,5,sol(i,0)+pp*g)


nCompExp=[1,1,1,1,1,1]
namesExp=['rhop','rhou','rhov','rhow','rhothetap','diffRhoP']
exportFunction=functionPython(sum(nCompExp), exportFct, [solution.getFunction(), initF, rhoThetaHs.getFunction()])


n_export=0
start = time.clock()
for i in range(0,nbSteps):
  if (i%100 == 0 or i==nbSteps-1):
    solution.exportFunctionVtk(exportFunction,'output', t, i,"solution",nCompExp,namesExp,functionC("./ccode.so","exportCoord",3,[XYZ]))
    if (Msg.GetCommRank()==0):
      print ('\nWriting output',n_export,'at time',t,'and step',i,'over',nbSteps)
      elapsed = (time.clock() - start)
      print ('Time elapsed: ',elapsed,' s')
      n_export=n_export+1
  norm = timeIter.iterate (solution, dt, t)
  t=t+dt
  if (Msg.GetCommRank()==0):
    sys.stdout.write('.')
    sys.stdout.flush()
print ('')    
elapsed = (time.clock() - start)
if (Msg.GetCommRank()==0):
  print ('Time elapsed: ',elapsed,' s')

Msg.Exit(0)



