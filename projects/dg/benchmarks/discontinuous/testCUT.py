from dgpy import *
from Common import *
import os
import time
import math


TIME = function.getTime()
os.system("rm output/*")

def initF(FCT, XYZ):
	for i in range(0,FCT.size1()):
          	x = XYZ.get(i,0)
		y = XYZ.get(i,1)	
		FCT.set(i, 0, x+y-0.5) 

#-------------------------------------------------
#-- Initialize level set problem
#-------------------------------------------------

print'---- Load Mesh & Geometry'
meshName0  = "square"
meshName   = "square2"
lc = 0.06
cmd = "sed -e 's/XXXX/%g/g' %s > %s" % (lc, meshName0+geo, meshName+geo)
os.system(cmd)
#meshName   = "cyl"
dim = 2
cut = False
quad = False
ADAPT = True

if (quad):
     GmshSetOption('Mesh', 'Algorithm', 8.0) 
     GmshSetOption('Mesh','RecombineAll', 1.0)
else:	
     GmshSetOption('Mesh', 'Algorithm', 4.0)
     
print 'generate a mesh'
genMesh(meshName,dim,1)
g = GModel()
g.load(meshName+geo)
if (not ADAPT):
	g.load(meshName+msh)

ls0 = gLevelsetPlane(0.,-1.,0.,0.5)
ls1 = gLevelsetPlane(-1.,0.,0.,0.4)
ls2 = gLevelsetMathEval("Sqrt((x-0.5)^2+(y-0.5)^2)-0.25")
ls3 = gLevelsetMathEval("-0.44+x")
ls33 = gLevelsetMathEval("-4.4+x")
ls4 = gLevelsetPopcorn(0.5, 0.5, 0.2, 0.25, 4.0, 0.08) #xc, yc, zc, r0, A, sigma
#ls5 = gLevelsetCrack([ls1,ls0])

i1 = gLevelsetIntersection([ls2,ls3])
#i2 = gLevelsetUnion([ls1,ls2])

#myLS = i1 
#myLS = ls3
myLS = ls2

print'---- If (ADAPT) adapt mesh with ls'
if (ADAPT):
	adaptAllDim = False
	g.adaptMesh([4], [myLS], [[0.1, 0.001, 0.08]], 15, adaptAllDim)
	g.save("squareADAPT.msh")
	
g2 = g.buildCutGModel(myLS, cut, True)
g2.save("squareMYCUT"+msh)

exit(success) 

