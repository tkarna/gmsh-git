from Common import *
import os
import time
import math


TIME = function.getTime()
os.system("rm output/*")

#-------------------------------------------------
#-- Case specific functions
#-------------------------------------------------
def analytical(FCT, XYZ):
        for i in range(0,FCT.size1()):
		x = XYZ.get(i,0)
                y = XYZ.get(i,1)	
		z = XYZ.get(i,2)		
		value = 2.0*y
		FCT.set(i, 0, value)
                FCT.set(i, 1, 0.0)
                FCT.set(i, 2, 0.0)

#-------------------------------------------------
#-- Initialize field
#-------------------------------------------------
print'---- Load Mesh & Geometry'
meshName = "square2"
order = 1
dim = 2
nbFields = 3

genMesh(meshName,dim,1)
model = GModel()
model.load(meshName+geo)
model.load(meshName+msh)
groups = dgGroupCollection(model, dim, order)

XYZ = groups.getFunctionCoordinates()
INIT = functionPython(nbFields, analytical, [XYZ])

print'---- Export field'
solution = dgDofContainer(groups, nbFields)
solution.interpolate(INIT)
solution.exportMsh('output/init', 0, 0)

#-------------------------------------------------
#-- Compute FD derivatives
#-------------------------------------------------

print'---- Compute derivatives'

fun = open("valuesField.txt","w")
fun.write("#x y z value \n")
#eval2 = dgFunctionEvaluator(groups, ns.law.getVelocity(), solution)
eval2 = dgFunctionEvaluator(groups, solution.getFunction(), solution)
res = fullMatrixDouble(3,1)

nums = model.getEdgesByStringTag('bottom')
N = 20
nbFD = 5
hFD = 0.1 #0.9*hmin
for k in range(len(nums)):
	edge = model.getEdgeByTag(nums[k])

        #Loop over parametrization
        #############################
        umin = edge.getLowerBound()
	umax = edge.getUpperBound()
	pOld = edge.point(umin)
	s = 0.0
	for i in range (0,N+1) :
		u = umin + (i*1.)/(N*1.) * (umax - umin)
		p = edge.point(u)

                #evaluating field at different points in normal direction
                der = edge.firstDer(u);
                perp = SVector3(0.0, 0.0, 1.0)
                n = crossprod(der, perp)
                d = [None]*nbFD
                for j in range (0,nbFD) :
                    p2x = p.x() + (j*hFD)*n.x()
                    p2y = p.y() + (j*hFD)*n.y()
                    p2z = p.z() + (j*hFD)*n.z()
                    eval2.compute(p2x, p2y, p2z, res)
                    #get the wall velocity parallel to the wall
                    vel = SVector3(res.get(0,0),res.get(1,0), 0.0)
                    vn = dot(vel,n)
                    vel_t = SVector3(vel.x() - vn*n.x(),vel.y() - vn*n.y(),vel.z() - vn*n.z())
                    d[j] = norm(vel_t)
                #FD computations of the derivative
                myDer  = 0.0
                if (nbFD == 2) :
                    myDer = (d[1]-d[0])/hFD
                elif (nbFD == 3) :
                    myDer = (-d[2] + 4.*d[1] - 3*d[0])/(2.*hFD)
                elif (nbFD == 4) :
                    myDer = (1./3.*d[3] - 3./2.*d[2] + 3.*d[1] - 11./6.*d[0])/hFD
                elif (nbFD == 5) :
                    myDer = (-1./4.*d[4] + 4./3.*d[3] - 3.*d[2] + 4.*d[1] - 25./12.*d[0])/hFD

                #WSS computations
                #WSS = mu*myDer
                
                print "p ", p.x(), " myDer ", myDer
                fun.write("%g %g %g %g \n" % (p.x(),p.y(), p.z(), myDer))

        fun.write("####################### \n")
        
        #Loop over parametrization
        #############################
        nbV = edge.getNumMeshVertices()
        vb = edge.getBeginVertex()
        eval2.compute(vb.x(),vb.y(),vb.z(), res)
        fun.write("%g %g %g %g \n" % (vb.x(),vb.y(), vb.z(), res.get(0,0)))
        for i in range (1,nbV) :
            v = edge.getMeshVertex(i)
            eval2.compute(v.x(),v.y(),v.z(), res)
            fun.write("%g %g %g %g \n" % (v.x(),v.y(), v.z(), res.get(0,0)))
        ve = edge.getEndVertex()
        eval2.compute(ve.x(),ve.y(),ve.z(), res)
        fun.write("%g %g %g %g \n" % (ve.x(),ve.y(), ve.z(), res.get(0,0)))


fun.write("\n")
