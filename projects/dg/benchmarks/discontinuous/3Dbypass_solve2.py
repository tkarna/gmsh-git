from dgpy import *
from Common import *
from Incompressible import *
import os
import time
import math


TIME = function.getTime()
os.system("rm output/*")

rho = 1.0
#mu  = 0.0023 #Re=800 
mu  = 0.0039 #Re=500
vmax = 2
UGlob = 1
R = 0.97
dim = 3

dim = 3
order = 1 

#-------------------------------------------------
#-- Initial function
#-------------------------------------------------
def initF(FCT, XYZ):
  for i in range(0,FCT.size1()):
    y = XYZ.get(i,1)
    FCT.set(i,0, 0.0) #u
    FCT.set(i,1, 0.0) #v
    FCT.set(i,2, 0.1) #w
    FCT.set(i,3, 0.0) #p

#-------------------------------------------------
#-- Case specific functions
#-------------------------------------------------
def VelBC(FCT, XYZ):
  for i in range(0,FCT.size1()):
    x = XYZ.get(i,0)    
    y = XYZ.get(i,1)
    z = XYZ.get(i,2)   
    FCT.set(i,0, 0.0) #u
    FCT.set(i,1, 0.0) #v
    #FCT.set(i,2, 0.1) #w
    FCT.set(i,2, vmax*(1-((x-0.745)*(x-0.745)+(y+0.735)*(y+0.735))/(R*R))) #w		
    FCT.set(i,3, 0.0) #p
    FCT.set(i,4, fixed)
    FCT.set(i,5, fixed)
    FCT.set(i,6, fixed)
    FCT.set(i,7, free)

#-------------------------------------------------
#-- Initialization
#-------------------------------------------------
meshName = "bypass_aniso_unstructBL2" #3DbypassBF2
ns = Incompressible(meshName, dim)
rhoF = functionConstant(rho)
muF = functionConstant(mu)
ns.initializeIncomp(initF, rhoF, muF , UGlob)

print "MAX Reynolds = ", (rho*UGlob*2*R/mu)

#-------------------------------------------------
#-- Boundary Conditions
#-------------------------------------------------
VEL = functionPython(8, VelBC, [ns.XYZ])

#3DbypassBF2
ns.strongBoundaryConditionSurface('Inlet', VEL)
ns.strongBoundaryConditionSurface('Wall', ns.WALL)
ns.strongBoundaryConditionSurface('Outlet', ns.PZERO)
	
#-------------------------------------------------
#-- Steady solve
#-------------------------------------------------

petscOptions = ""
if (Msg.GetCommSize() == 1) :
  petscOptions ="-ksp_rtol 1.e-3 -pc_type ilu -pc_factor_levels 1"
else :
  petscOptions ="-ksp_rtol 1.e-3 -sub_pc_type ilu -sub_pc_factor_levels 1"

petscOptions ="-ksp_rtol 1.e-4 -pc_type lu -ksp_monitor -pc_factor_levels 0"
#ns.steadySolve(20, petscOptions)

#-------------------------------------------------
#-- Pseudo TimeStepping
#-------------------------------------------------
nbTimeSteps = 25
dt0 = 0.1
ATol = 1.e-5
RTol = 1.e-8
Verb = 2
ns.pseudoTimeSteppingSolve(2, nbTimeSteps, dt0, ATol, RTol, Verb, petscOptions)

ns.solution.exportMshNodeData("output/sol_blMESH",0,0)
ns.solution.exportFunctionSurf(ns.law.getWSSNorm(), 'output/surface', 0, 0, 'wss', ['Wall'])
ns.solution.exportFunctionSurf(ns.law.getVelocity(), 'output/surface', 0, 0, 'vel', ['Outlet', 'Inlet'])
ns.solution.exportFunctionSurf(ns.law.getPressure(), 'output/surface', 0, 0, 'pres', ['Outlet', 'Inlet'])

#-------------------------------------------------
#-- Validation 
#-------------------------------------------------

#Analytical:
L = 23.5 #10 #from the .geo file
R = 0.97 #from the .geo file
pANA = 4*mu*L*vmax/R**2 #pANA is the difference of pressure between the inlet and outlet
print "Analytical value for pressure difference: ",  pANA

#Numerical:
eval = dgFunctionEvaluator(ns.groups, ns.solution.getFunction())
result_in = fullMatrixDouble(4,1);
result_out = fullMatrixDouble(4,1);
eps = 5.e-2;
eval.compute(-2.6,-8.1,-13+eps,result_in)
eval.compute(-3.1,-8.0,10.5-eps,result_out)
pNUM_in = result_in.get(3,0)
pNUM_out = result_out.get(3,0)
pNUM = pNUM_in - pNUM_out
print "MAX Reynolds = ", (rho*UGlob*2*R/mu)
print "Numerical value for pressure difference: " , pNUM
print "Numerical value for pressure at inlet: " , pNUM_in
print "Numerical value for pressure at outlet: " , pNUM_out

#Comparison:
diff = abs((pNUM-pANA)/pANA)

print ('|error DP| = ', diff)

