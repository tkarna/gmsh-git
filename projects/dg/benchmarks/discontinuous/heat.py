from dgpy import *
from Common import *
import os
import time
import math

TIME = function.getTime()
try : os.mkdir("output");
except: 0;
os.system("rm output/*")

#-------------------------------------------------
#-- Case specific functions
#-------------------------------------------------
def initF(FCT, XYZ):
	for i in range(0,FCT.size1()):
          	x = XYZ.get(i,0)
		temp = 10
		FCT.set(i,0, temp)

def initLS(FCT, XYZ):
	for i in range(0,FCT.size1()):
          	x = XYZ.get(i,0)
		FCT.set(i,0, x-0.5) 

#-------------------------------------------------
#-- Initialize level set problem
#-------------------------------------------------

print'---- Load Mesh & Geometry'
CG = False
CUT = False
kappa1 = 1.0
kappa2 = 2.0
Tin = 10.0
Tout = 1.0
order = 1
dimension = 2

meshName = "square"
genMesh(meshName, dimension, order)

if (CUT):
	g = GModel()
	g.load(meshName+msh)
	ls1 = gLevelsetPlane(-1.,0.,0.,0.5)	
	g2 = g.buildCutGModel(ls1, True, True)
	meshName2 = "squareMYCUT"	
	g2.save(meshName2+msh)
	meshName = meshName2
	#reload and rewrite the mesh for renumbering
	g3 = GModel()
	g3.load(meshName2+msh)
	g3.save(meshName2+msh)

model = GModel()
model.load(meshName+msh)

print'---- Build Groups'
groups = dgGroupCollection(model, dimension, order)
if (CUT):
	groups.splitGroupsByPhysicalTag()
XYZ = groups.getFunctionCoordinates()

print'---- Load Conservation law'
if (CUT):
	data_kappa   = {"inside_out":kappa1, "inside_in":kappa2} 
	kappa = functionConstantByTag(data_kappa)
else:
	INITLS = functionPython(1, initLS, [XYZ])
	kappa = functionLevelsetSmoothNew(INITLS, kappa1, kappa2, 0.05)
	#kappa = functionConstant([1.])

if (CG):
	law = dgConservationLawAdvectionDiffusionSUPG.diffusionLaw(kappa) #CG
else:
	law = dgConservationLawAdvectionDiffusion.diffusionLaw(kappa) #DG

print'---- Initial condition'
INIT_TEMP = functionPython(1, initF, [XYZ])
solution = dgDofContainer(groups,law.getNbFields())
solution.interpolate(INIT_TEMP) 
solution.exportMsh('output/initT', 0., 0)

print'---- Load Boundary conditions'
out1 = functionConstant([Tin])
out2 = functionConstant([Tout])

law.addBoundaryCondition('left', law.newOutsideValueBoundary("",out1))
law.addBoundaryCondition('right', law.newOutsideValueBoundary("",out2))
if (CG):
	law.addStrongBoundaryCondition(1, 'left', out1)
	law.addStrongBoundaryCondition(1, 'right', out2)
	
if (CUT):
	law.addBoundaryCondition('top_in'   , law.new0FluxBoundary())
	law.addBoundaryCondition('top_out'   , law.new0FluxBoundary())
	law.addBoundaryCondition('bottom_in' , law.new0FluxBoundary())
	law.addBoundaryCondition('bottom_out' , law.new0FluxBoundary())
else:
	law.addBoundaryCondition('top'   , law.new0FluxBoundary())
	law.addBoundaryCondition('bottom' , law.new0FluxBoundary())

#-------------------------------------------------
#-- solver setup
#-------------------------------------------------

timeOrder = 1

print'---- Solver Definition'
dof = None
if (CG):
	petsc =  linearSystemPETScDouble()
	dof = dgDofManager.newCG(groups, 1, petsc)
	petsc.setParameter("petscOptions", "-pc_type lu")
	petsc.setParameter("matrix_reuse", "same_matrix")
else:
	petsc = linearSystemPETScBlockDouble()
	petsc.setParameter("petscOption", "-pc_type lu -ksp_monitor")
	dof = dgDofManager.newDGBlock(groups, 1, petsc)


#-------------------------------------------------
#-- steady solve
#-------------------------------------------------
print'---- Solve'	
maxIter = 100
steady = dgSteady(law, dof)
steady.getNewton().setVerb(2)
steady.getNewton().setMaxIt(maxIter)
steady.solve(solution)
solution.exportMsh("output/steadySol", 0, 0)

#-------------------------------------------------
#-- solve
#-------------------------------------------------	
'''
solver = dgDIRK(law, dof, timeOrder)
solver.getNewton().setAtol(1.e-8)
solver.getNewton().setRtol(1.e-5)
solver.getNewton().setVerb(2)

t = 0
x = time.clock()
dt0 = 0.01

dt = dt0
rinit = 1.0
for i in range (1,100):
	
	t = t+dt
	print'|ITER|',i, '|DT|',dt,'|T(sec)|',t
	
	solver.iterate(solution, dt, t)
	res = solver.getNewton().getFirstResidual()
	
	print'|ITER|',i,'|NORM|',res,'|DT|',dt, '|T|',t, '|CPU|',time.clock() - x
	solution.exportMsh("output/T-%05d" % (i), t, i)
	
	if (i == 2) :
		rinit = res
	if (i > 2) :
		print 'CONVERGENCE : ' , (res/rinit)
		dt = dt0 * math.pow(rinit/res,1.5)
		if (dt > 60000) : 
			dt = 60000
		if (res/rinit < 1.e-8) : 
			break 
'''
#-------------------------------------------------
#-- Validation 
#-------------------------------------------------

eval = dgFunctionEvaluator(groups, solution.getFunction())
result = fullMatrixDouble(1,1);
eval.compute(0.5,0.3,0,result)

e = 0.5
TNUM = result.get(0,0)
TANA = (Tout*kappa2/e + Tin*kappa1/e)/(kappa1/e+kappa2/e)

print "TANA", TANA
print "TNUM", TNUM
