
lc = 0.0005;
//lc = 0.01;
Point(1) = {0.00,0.0000,0,lc};         
Point(2) = {0.00,0.0101,0,lc};         
Point(3) = {0.08,0.0101,0,lc};         
Point(4) = {0.08,0.0000,0,lc}; 
              
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

Line Loop(5) = {1, 2, 3, 4};
Plane Surface(6) = {5};

Physical Line("inlet")={1};
Physical Line("top")={2};
Physical Line("outlet")={3};
Physical Line("bottom")={4};

Physical Surface("domain")={6};

