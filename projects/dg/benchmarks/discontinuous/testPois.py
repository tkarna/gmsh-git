from dgpy import *
from Common import *
import os
import time
import math

def poiseuille(FCT, XYZ):
	for i in range(0,FCT.size1()):
          	x = XYZ.get(i,0)
		y = XYZ.get(i,1)
		#val = (x-0.5)*(x-0.5)+(y-0.5)*(y-0.5)-(0.3*0.3)
		val = 100*x*x #+1.0*y*y
		FCT.set(i, 0, val)
		
#-------------------------------------------------
#-- Initialize level set problem
#-------------------------------------------------

print'---- Load Mesh & Geometry'
dim = 2
CUT = False
ADAPT = True

meshName0  = "square"
meshName   = "square2"
lc = 0.02
cmd = "sed -e 's/XXXX/%g/g' %s > %s" % (lc, meshName0+geo, meshName+geo)
os.system(cmd)

print'---- Generate isotropic mesh'
genMesh(meshName, dim, 1)
g = GModel()
g.load(meshName+geo)

#define simplefunction from levelset matheval
ls1 = gLevelsetMathEval("-x+0.5")
ls2 = gLevelsetMathEval("(x-0.5)^2+(y-0.5)^2-(0.3)^2")

#define simplefunction from evaluator on finite elemnt field
oldG = GModel()
oldG.load(meshName+msh)
groups = dgGroupCollection(oldG, dim, 1)
XYZ = groups.getFunctionCoordinates()
POIS = functionPython(1, poiseuille, [XYZ])
solution = dgDofContainer(groups,1)
solution.interpolate(POIS)
solution.exportMsh('output/pois', 0, 0)
eval1 = dgFunctionEvaluator(groups, solution.getFunction())

print'---- adapt mesh '
g = GModel()
g.load(meshName+geo)
lcMin = 0.0001
lcMax =  0.1
nbIter = 25
g.adaptMesh([2], [eval1], [[0.1, lcMin, lcMax]], nbIter, True)# 1 levelset, 2 hessian, 5=levelset

g.save("testpois.msh")

exit(success) 
