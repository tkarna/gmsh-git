from dgpy import *
from Incompressible import *
from Common import *
import os
import time
import math
import sys

os.system("rm output/*")
os.system("rm LiftAndDrag.dat")

# Cylinder Case with LEVEL SET GEOMETRY

#-------------------------------------------------
#-- Parameters
#-------------------------------------------------
RHO = 1.0
MU  = 0.001
UGlob = 0.2
D = 0.1
print('------------------------------------')
print("---- CYLINDER PROBLEM AT Re = %g" % (RHO*UGlob*D/MU))
print('------------------------------------')

#-------------------------------------------------
#-- Initial function
#-------------------------------------------------
def initF(FCT, XYZ):
	for i in range(0,FCT.size1()):
		y = XYZ.get(i,1)
		u = 4*0.3*y*(0.41-y)/(0.41*0.41)
		FCT.set(i,0, u) #u  UGlob
		FCT.set(i,1, 0.0) #v
		FCT.set(i,2, 0.0) #p

#-------------------------------------------------
#-- Case specific functions
#-------------------------------------------------
def VelBC(FCT, XYZ ):
	vmax = 2.0;
	for i in range(0,FCT.size1()):
		y = XYZ.get(i,1)
		u = 4*0.3*y*(0.41-y)/(0.41*0.41)
		FCT.set(i,0, u)
		FCT.set(i,1, 0.0)
		FCT.set(i,2, 0.0)
		FCT.set(i,3, fixed)
		FCT.set(i,4, fixed)
		FCT.set(i,5, free)

#-------------------------------------------------
#-- Initialize level set problem
#-------------------------------------------------

print ('---- Generating initial mesh ')
meshName   = "cyl20"
g = GModel()
g.load(meshName+geo)

ls = gLevelsetSphere(0.2, 0.2, 0, 0.05)
myLS = ls

print ('---- Adapt mesh with ls')
g.adaptMesh([4], [myLS], [[0.006, 1e-4, 0.04, 150]], 10, True)
meshNameSPLIT = "cyl20_SPLIT"
g2 = g.buildCutGModel(myLS, False, True)
g2.save(meshNameSPLIT+msh)

#-------------------------------------------------
#-- Initialization
#-------------------------------------------------
ns = Incompressible(meshNameSPLIT, 2, ["domain_out"])
rhoF = functionConstant(RHO)
muF = functionConstant(MU)
ns.initializeIncomp(initF, rhoF, muF , UGlob)

#-------------------------------------------------
#-- Boundary conditions
#-------------------------------------------------
VEL = functionPython(6, VelBC, [ns.XYZ])

ns.strongBoundaryConditionLine('Inlet', VEL)
ns.strongBoundaryConditionLine('levelset_L1', ns.WALL)
ns.strongBoundaryConditionLine('WallTop', ns.WALL)
ns.strongBoundaryConditionLine('WallBottom', ns.WALL)
ns.strongBoundaryConditionLine('Outlet', ns.PZERO)

#-------------------------------------------------
#-- Solve
#-------------------------------------------------

nbTimeSteps = 20
dt0 = 10.0
ATol = 1.e-8
RTol = 1.e-5
Verb = 2
petscOptions ="-ksp_rtol 1.e-3 -pc_type lu -pc_factor_levels 0"

#ns.pseudoTimeSteppingSolve(2, nbTimeSteps, dt0, ATol, RTol, Verb, petscOptions)
ns.steadySolve(20, petscOptions)

#-------------------------------------------------
#-- Validation 
#-------------------------------------------------

flowDir=[1., 0., 0.]
spanDir=[0., 1., 0.]
fScale = 0.5*RHO*UGlob*UGlob*D
ns.computeDragAndLift('levelset_L1', 0, 0, fScale, flowDir, spanDir, 'LiftAndDrag.dat')

fun=open('LiftAndDrag.dat',"r")
line1 = fun.readline()
line2 = (fun.readline()).split()
CL_NUM = float(line2[2])
CL_ANA = 0.011
CD_NUM = float(line2[3])
CD_ANA = 5.57
fun.close()

diff_lift = fabs((CL_NUM-CL_ANA)/CL_ANA)
diff_drag = fabs((CD_NUM-CD_ANA)/CD_ANA)
print('-------------------------------------------------------------')
print ('Lift: CL=%.4f CL_ANA=%.4f |error CL| =%.4f ' % (CL_NUM, CL_ANA, diff_lift))
print ('Drag: CD=%.4f CD_ANA=%.4f |error CD| =%.4f ' % (CD_NUM, CD_ANA, diff_drag))
print('-------------------------------------------------------------')

if (diff_lift  < 4.e-1 and diff_drag  < 2.e-1): 
	print ("Exit with success :-)")
	sys.exit(success)
else:
	print ("Exit with failure :-(")
	sys.exit(fail)


