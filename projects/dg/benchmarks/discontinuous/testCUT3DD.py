from dgpy import *
from Common import *
import os
import time
import math


TIME = function.getTime()
os.system("rm output/*")

def initLS(FCT, XYZ):
	for i in range(0,FCT.size1()):
          	x = XYZ.get(i,0)
		y = XYZ.get(i,1)
		z = XYZ.get(i,2)
		FCT.set(i, 0, (x-0.5)**2+(y-0.5)**2+(z-0.5)**2-(0.3)**2)

def wallDistance(FCT, XYZ) :

  for i in range(0,FCT.size1()):
    x = XYZ.get(i,0) 
    y = XYZ.get(i,1)
    z = XYZ.get(i,2) 
    FCT.set(i,0,DIST_TO_MESH (x, y, z));
#-------------------------------------------------
#-- Initialize level set problem
#-------------------------------------------------
   
print'---- Load Mesh & Geometry'
boxName = "rect3D"
dim = 3
order = 1 
CUT = False
ADAPT = True

print'---- Generate isotropic mesh'
genMesh(boxName,dim,order)
g = GModel()
g.load(boxName+msh)

print'---- Test distance to mesh'
gSTL = GModel()
#gSTL.load("cylPHYS.msh")
gSTL.load("bypass.msh")

#XYZ = function.getCoordinates()
#DIST  = functionPython(1, wallDistance, [XYZ])
#DIST_TO_MESH  =gLevelsetDistMesh(gSTL,"wall")
#groups = dgGroupCollection(g, dim, order) 
#solution = dgDofContainer(groups, 1)
#solution.exportFunctionMsh(DIST, 'output/ns', 0, 0, 'dist')

print'---- Define level set on that mesh'
ls1 = gLevelsetDistMesh(gSTL,"wall", 5, 1)
#ls2 = gLevelsetMathEvalAll(["-x+2.0", "-1.0", "0.0", "0.0",
#                           "0.0", "0.0", "0.0",
#                           "0.0", "0.0", "0.0",
#                           "0.0", "0.0", "0.0"], 2)
#ls3 = gLevelsetMathEvalAll(["x-8.0","1.0", "0.0", "0.0",
#                           "0.0", "0.0", "0.0",
#                           "0.0", "0.0", "0.0",
#                           "0.0", "0.0", "0.0"], 3)
#ls1 = gLevelsetCylinder([0.0,0.0,0.0], [1.0,0.0,0.0],0.5,10)
#ls2 = gLevelsetMathEval("-x+2.0", 2)
#ls3 = gLevelsetMathEval("x-8.0", 3)
ls2 = gLevelsetPlane([0.0,0.0,-13.5],[0.0,0.0,-1],2)
ls3 = gLevelsetPlane([0.0,0.0,10.0],[0.0,0.0,1],3)

myLS = gLevelsetIntersection([ls1,ls2,ls3])

print'---- If (ADAPT) adapt mesh with ls'
GmshSetOption("Mesh","SmoothRatio",   2.0);
hmin = 2.e-3
hmax = 0.4
E = 0.2
if (ADAPT):
	GmshSetOption("Mesh","SmoothRatio",   2.1);
	g.adaptMesh([5,5,5], [ls1,ls2,ls3], [[E, hmin, hmax],[E, hmin, hmax],[E, hmin, hmax]], 10, False)
	g.save("rect3D_ADAPT"+msh)
else:
        g.deleteMesh()
        g.load("rect3D_ADAPT"+msh)

print'---- CUT/SPLIT the mesh'
g2 = g.buildCutGModel(myLS, CUT, True)
boxName = "rect3D_SPLIT"	
g2.save(boxName+msh)

exit(success) 

