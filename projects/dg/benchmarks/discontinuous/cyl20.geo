l = 0.2; //before cylinder
L = 2; //after cylinder
H = 0.2; //above and under cylinder
H2 = 0.21;
X = -0.2;
Y = -0.2;

lc2 = 0.005;

Point(6) = {-l-X,-H-Y,0,lc2};
Point(7) = {-l-X,H2-Y,0,lc2};
Point(8) = {-l+L-X,H2-Y,0,lc2};
Point(9) = {-l+L-X,-H-Y,0,lc2};

Line(5) = {7,8};
Line(6) = {8,9};
Line(7) = {9,6};
Line(8) = {6,7};
Line Loop(9) = {5,6,7,8};
Plane Surface(11) = {9};

Physical Surface("domain") = {11};
Physical Line("WallBottom") = {7};
Physical Line("WallTop") = {5};
Physical Line("Inlet") = {8};
Physical Line("Outlet") = {6};

