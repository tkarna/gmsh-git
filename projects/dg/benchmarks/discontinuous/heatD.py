from dgpy import *
from Common import *
import math 
import os
import time

os.system("rm output/*")

#-------------------------------------------------
#-- Case specific functions
#-------------------------------------------------
def initF(FCT, XYZ):
	for i in range(0,FCT.size1()):
          	x = XYZ.get(i,0)
		temp = 1
		FCT.set(i,0, temp)

#-------------------------------------------------
#-- Initialize level set problem
#-------------------------------------------------
    
print'---- Load Mesh & Geometry'
CG = True
CUT = False
ADAPT = True
kappa1 = 1.0
kappa2 = 1.0
Tin = 10.0
Tout = 3.0
dim = 2

if (CUT and (not CG)):
	print "Dirichlet BC on cut not possible for DG"
	exit(fail)
	
meshName0  = "square"
meshName   = "square2"
lc = 0.02
cmd = "sed -e 's/XXXX/%g/g' %s > %s" % (lc, meshName0+geo, meshName+geo)
os.system(cmd)

print'---- Generate isotropic mesh'
genMesh(meshName, dim, 1)
g = GModel()
g.load(meshName+geo)
if (not ADAPT):
	g.load(meshName+msh)

print'---- Define level set on that mesh'
ls1 = gLevelsetMathEval("-x+0.5")
myLS = ls1

print'---- If (ADAPT) adapt mesh with ls'
if (ADAPT):
	lcMin = 0.0001
	g.adaptMesh(1, myLS, [0.15, lcMin, 0.15, 25], True)

print'---- CUT/SPLIT the mesh'
g2 = g.buildCutGModel(myLS, CUT, True)

meshNameCUT = "squareMYCUT"	
g2.save(meshNameCUT+msh)
meshName = meshNameCUT

print'---- Build Groups'
model = GModel()
model.load(meshName+msh)

groups = dgGroupCollection.newByTag(model, dim, 1, ["inside_out"])
groups.buildGroupsOfInterfaces()
XYZ = groups.getFunctionCoordinates()

print'---- Load Conservation law'
if (CUT):
	kappa = functionConstant(kappa1)
else:
	E = lc
	if (ADAPT):
		E = 2*lcMin;
	INITLS = functionLevelsetValueNew(myLS)
	kappa = functionLevelsetSmoothNew(INITLS, kappa1, kappa2, E)

if (CG):
	law = dgConservationLawAdvectionDiffusionSUPG.diffusionLaw(kappa) #CG
else:
	law = dgConservationLawAdvectionDiffusion.diffusionLaw(kappa) #DG

print'---- Initial condition'
INIT_TEMP = functionPython(1, initF, [XYZ])
solution = dgDofContainer(groups,law.getNbFields())
solution.interpolate(INIT_TEMP) 
solution.exportMsh('output/initT', 0., 0)

print'---- Load Boundary conditions'
out1 = functionConstant([Tin])
out2 = functionConstant([Tout])
#slope = functionConstant([10.0])
outsideBoundary = law.newOutsideValueBoundary("",out2)

law.addBoundaryCondition('left', law.newOutsideValueBoundary("",out1))
law.addBoundaryCondition('right' , law.new0FluxBoundary())
law.addBoundaryCondition('top_in'  , law.new0FluxBoundary())
law.addBoundaryCondition('top_out'  , law.new0FluxBoundary())
law.addBoundaryCondition('bottom_in' , law.new0FluxBoundary())
law.addBoundaryCondition('bottom_out' , law.new0FluxBoundary())
law.addBoundaryCondition('levelset_1' , outsideBoundary)
#law.addBoundaryCondition('levelset_1', law.newNeumannBoundary(slope))
if (CG):
	law.addStrongBoundaryCondition(1, 'left', out1)
	law.addStrongBoundaryCondition(1, 'levelset_1', out2)
	
#-------------------------------------------------
#-- solver setup
#-------------------------------------------------

print'---- Solver Definition'
dof = None
if (CG):
	petsc =  linearSystemPETScDouble()
	dof = dgDofManager.newCG(groups, 1, petsc)
	petsc.setParameter("petscOptions", "-pc_type lu")
	petsc.setParameter("matrix_reuse", "same_matrix")
else:
	petsc = linearSystemPETScBlockDouble()
	petsc.setParameter("petscOption", "-pc_type lu -ksp_monitor")
	dof = dgDofManager.newDGBlock(groups, 1, petsc)

#-------------------------------------------------
#-- steady solve
#-------------------------------------------------
maxIter = 100
steady = dgSteady(law, dof)
steady.getNewton().setVerb(2)
steady.getNewton().setMaxIt(maxIter)
steady.solve(solution)
solution.exportMsh("output/steadySol", 0, 0)

#-------------------------------------------------
#-- unsteady solve
#-------------------------------------------------
"""
timeOrder = 1
solver = dgDIRK(law, dof, timeOrder)
solver.getNewton().setAtol(1.e-8)
solver.getNewton().setRtol(1.e-5)
solver.getNewton().setVerb(2)

print'---- Solve'
t = 0
x = time.clock()
dt0 = 0.01

dt = dt0
rinit = 1.0
for i in range (1,100):
	
	t = t+dt
	print'|ITER|',i, '|DT|',dt,'|T(sec)|',t
	
	solver.iterate(solution, dt, t)
	res = solver.getNewton().getFirstResidual()
	
	print'|ITER|',i,'|NORM|',res,'|DT|',dt, '|T|',t, '|CPU|',time.clock() - x
	solution.exportMsh("output/T-%05d" % (i), t, i)
	
	if (i == 2) :
		rinit = res
	if (i > 2) :
		print 'CONVERGENCE : ' , (res/rinit)
		dt = dt0 * math.pow(rinit/res,1.5)
		if (dt > 60000) : 
			dt = 60000
		if (res/rinit < 1.e-8) : 
			break 
"""
#-------------------------------------------------
#-- Validation 
#-------------------------------------------------

eval1 = dgFunctionEvaluator(groups, solution.getFunction())
result = fullMatrixDouble(1,1);
eval1.compute(0.5,0.3,0,result)

TNUM = result.get(0,0)
TANA = Tout

print "TANA (Dirichlet BC)", TANA
print "TNUM (Dirichlet BC)", TNUM
