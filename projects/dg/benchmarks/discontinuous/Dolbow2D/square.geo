
//lc = XXXX;
//nb = 5;
lc = 0.064;
Point(1) = {-1, -1, 0, lc};
Point(2) = {1, -1, 0, lc};
Point(3) = {1, 1, 0, lc};
Point(4) = {-1, 1, 0, lc};

Line(1) = {4, 3};
Line(2) = {3, 2};
Line(3) = {2, 1};
Line(4) = {1, 4};

Line Loop(5) = {2, 3, 4, 1};
Plane Surface(6) = {5};

//Transfinite Line {1, 2, 3, 4} = nb+1;
//Transfinite Surface {6} = {1,2,3,4};

Physical Line("top") = {1};
Physical Line("bottom") = {3};
Physical Line("left") = {4};
Physical Line("right") = {2};
Physical Surface("inside") = {6};


