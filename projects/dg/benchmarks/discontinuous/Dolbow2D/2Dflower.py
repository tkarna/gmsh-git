#31May2012
#square + flower
from dgpy import *
from Common import *
import math 
import os
import time
	
try : os.mkdir("output");
except: 0;

os.system("rm output/*")

print'       '
print'*--------------------Flower2D-------------------*'
print'|    UCL   |                                    |'
print'|   iMMC   |  DEFINING CASE SPECIFIC FUNCTIONS  |'
print'|  dgCode  |                                    |'
print'*-----------------------------------------------*'

def exactSL(FCT, XYZ):
	for i in range(0,FCT.size1()):
		x = XYZ.get(i,0)
		y = XYZ.get(i,1)
		FCT.set(i,0, x**2+y**2)

def lsbc(FCT, XYZ):
	for i in range(0,FCT.size1()):
		x = XYZ.get(i,0)
		y = XYZ.get(i,1)
		theta = atan(y/x)
		R = 0.5+0.2*sin(6*theta)
		FCT.set(i,0, R**2)

def anaSol(FCT, XYZ):
	for i in range(0,FCT.size1()):
		x = XYZ.get(i,0)
		y = XYZ.get(i,1)
		Tana = x**2+y**2
		FCT.set(i,0, Tana*Tana)	

def error(FCT, F1, F2):
	for i in range(0,F1.size1()):
		FCT.set(i, 0, (F1(i,0)-F2(i,0))**2);

print'       '
print'*--------------------dgCode---------------------*'
print'|          |                                    |'
print'| Flower2D |      GENERATE ANISOTROPIC MESH     |'
print'|          |                                    |'
print'*-----------------------------------------------*'
CG = True
caseName  = "square"

print'---- DEFINE LEVELSET ON MESH'
flower = gLevelsetMathEval("sqrt(x*x+y*y)-0.5-0.2*sin(6*atan(y/x))")
myLS = flower

print'---- LOADING GEOMETRY'		
mesh = GModel()
mesh.load(caseName+geo)

print'---- ADAPT MESH WITH LEVELSET'
mesh.adaptMesh([4], [myLS], [[0.025, 0.00156, 0.0125]], 25, True)

print'---- SLITTING MESH'
model = GModel()
model = mesh.buildCutGModel(myLS, False, True)
name = "flower"
model.save(name+msh)
NAME = "FLOWER"
adapt = GModel()
adapt.load(name+msh)
adapt.save(NAME+msh)
meshName = NAME

print'       '
print'*--------------------dgCode---------------------*'
print'|          |                                    |'
print'| Flower2D |      INITIALIZATION OF PROBLEM     |'
print'|          |                                    |'
print'*-----------------------------------------------*'

print'---- BUILD GROUPS'
MODEL = GModel()
MODEL.load(meshName+msh)
groups = dgGroupCollection.newByTag(MODEL, 2, 1, ["inside_in"])
groups.splitGroupsByPhysicalTag()
XYZ = groups.getFunctionCoordinates()

print'---- LOAD CONSERVATION LAW'
kappa = functionConstant(1)
mySource = functionConstant(-4)

if (CG):
	law = dgConservationLawAdvectionDiffusionSUPG.diffusionLaw(kappa)
else:
	law = dgConservationLawAdvectionDiffusion.diffusionLaw(kappa)

law.setSource(mySource)

print'---- INITIAL CONDITION'
INIT = functionConstant(0)
solution = dgDofContainer(groups,law.getNbFields()) 
solution.interpolate(INIT) 
solution.exportMsh('output/initT', 0., 0) 
	
print'---- BOUNDARY CONDITIONS'
BC = functionPython(1, exactSL, [XYZ])
lsBC = functionPython(1, lsbc, [XYZ])

law.addBoundaryCondition('top', law.newOutsideValueBoundary("",BC))
law.addBoundaryCondition('left', law.newOutsideValueBoundary("", BC)) 
law.addBoundaryCondition('right', law.newOutsideValueBoundary("",BC))
law.addBoundaryCondition('bottom', law.newOutsideValueBoundary("",BC))
law.addBoundaryCondition('levelset_L1', law.newOutsideValueBoundary("", lsBC))

if (CG):
	law.addStrongBoundaryCondition(1, 'top', BC)
	law.addStrongBoundaryCondition(1, 'left', BC)
	law.addStrongBoundaryCondition(1, 'right', BC)
	law.addStrongBoundaryCondition(1, 'bottom', BC)
	law.addStrongBoundaryCondition(1, 'levelset_L1', lsBC)

print'     '
print'*--------------------dgCode---------------------*'
print'|          |                                     |'
print'| Flower2D |           SOLVING PROBLEM           |'
print'|          |                                     |'
print'*-----------------------------------------------*'

print'---- Solver Definition'
if (CG):
	petsc =  linearSystemPETScDouble()
	petsc.setParameter("petscOptions", "-pc_type lu")
	petsc.setParameter("matrix_reuse", "same_matrix")
	dof = dgDofManager.newCG(groups, law.getNbFields(), petsc)
else:
	petsc = linearSystemPETScBlockDouble()
	petsc.setParameter("petscOption", "-pc_type lu -ksp_monitor")
	dof = dgDofManager.newDGBlock(groups, law.getNbFields(), petsc)	

print'---- Solving...'
maxIter = 10000
steady = dgSteady(law, dof)
steady.getNewton().setVerb(10)
steady.getNewton().setRtol(1e15)
steady.getNewton().setMaxIt(maxIter)
steady.solve(solution)
solution.exportMsh("output/solution", 0, 0)

print'       '
print'*--------------------dgCode---------------------*'
print'|          |                                    |'
print'| Flower2D |           POSTPROCESSING           |'
print'|          |                                    |'
print'*-----------------------------------------------*'	

print'     results coming soon ... :-)'

print'---- Calculating result at given point'
X = 0
Y = 0.03
eval1 = dgFunctionEvaluator(groups, solution.getFunction())
result = fullMatrixDouble(1,1)
eval1.compute(X,Y,0,result)
TNUM = result.get(0,0)
TANA = X**2+Y**2 

print'---- Calculating error'
fana = functionPython(1, exactSL, [XYZ])
ana = dgDofContainer(groups, 1)
ana.interpolate(fana)
ana.exportMsh("output/exact_solution", 0, 0)

print'---- integrating ...'
fanaSol = functionPython(1, anaSol, [XYZ])
integrator1 = dgFunctionIntegrator(groups, fanaSol, solution)
matrix1 = fullMatrixDouble(1,1)
integrator1.compute(matrix1, "inside_in")
result1 = matrix1.get(0,0)

ANA = ana.getFunction()
NUM = solution.getFunction()
ferror = functionPython(1, error, [ANA, NUM]);
integrator2 = dgFunctionIntegrator(groups, ferror)
matrix2 = fullMatrixDouble(1,1)
integrator2.compute(matrix2, "inside_in")
result2 = matrix2.get(0,0)

ERROR1 = sqrt(result2)

print'     exporting error ...'
error = dgDofContainer(groups, 1)
error.interpolate(ferror)
error.exportMsh("output/error", 0, 0)

print'       '
print'*---------------------dgCode--------------------*'
print'|          |                                    |'
print'| Flower2D |         SUMMARY FOR RESULTS        |'
print'|          |                                    |'
print'*-----------------------------------------------*'

print "1. ERROR AT ONE POINT: (x=%g,y=%g)" % (X,Y)
print "   Analytical result =", TANA
print "   Numerical  result =", TNUM

print "2. ERROR IN THE BULK FIELD:", ERROR1
