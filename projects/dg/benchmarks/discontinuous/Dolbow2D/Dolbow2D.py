#21Mar2012
#Dolbow2D: square + straight line
from dgpy import *
from Common import *
import math 
import os
import time
	
try : os.mkdir("output");
except: 0;

os.system("rm output/*")

print'       '
print'*--------------------Dolbow2D-------------------*'
print'|    UCL   |                                    |'
print'|   iMMC   |  DEFINING CASE SPECIFIC FUNCTIONS  |'
print'|  dgCode  |                                    |'
print'*-----------------------------------------------*'

def lsBC(FCT, XYZ):
        for i in range(0,XYZ.size1()):
		x = XYZ.get(i,0)
                y = 1./3
		FCT.set(i,0, sin(pi*x)*(cosh(pi*y)-((exp(2*pi)+1)/(exp(2*pi)-1))*sinh(pi*y)))

def exactSL(FCT, XYZ):
	for i in range(0,FCT.size1()):
		x = XYZ.get(i,0)
		y = XYZ.get(i,1)
		FCT.set(i,0, sin(pi*x)*(cosh(pi*y)-((exp(2*pi)+1)/(exp(2*pi)-1))*sinh(pi*y)))	

def anaSol(FCT, XYZ):
	for i in range(0,FCT.size1()):
		x = XYZ.get(i,0)
		y = XYZ.get(i,1)
		Tana = sin(pi*x)*(cosh(pi*y)-((exp(2*pi)+1)/(exp(2*pi)-1))*sinh(pi*y))
		FCT.set(i,0, Tana*Tana)	

def error(FCT, F1, F2):
	for i in range(0,F1.size1()):
		FCT.set(i, 0, (F1(i,0) -F2(i,0))** 2);

print'       '
print'*-----------------dgCode-----------------*'
print'|          |                             |'
print'| Dolbow2D |  GENERATE ANISOTROPIC MESH  |'
print'|          |                             |'
print'*----------------------------------------*'
CG = True
ADAPT = True
fine = 5 #fine-range = [1:6]

caseName  = "Dolbow2D"

print'---- DEFINE LEVELSET ON MESH'
ls = gLevelsetMathEval("y-1/3")
myLS = ls

if (ADAPT):
	print'---- LOADING GEOMETRY'		
	mesh = GModel()
	mesh.load(caseName+geo)
	print'---- ADAPT MESH WITH LEVELSET'
	if fine == 1: #lc = lcmax = 0.064; [E, lcmin, lcmax]; 0.0967989105873
		mesh.adaptMesh([5], [myLS], [[0.064, 0.064, 0.064]], 25, True)
		print'---- SLITTING MESH'
		model = GModel()
		model = mesh.buildCutGModel(myLS, False, True)
		name = "adapt1"
		model.save(name+msh)
		NAME = "ADAPT1"
		adapt = GModel()
		adapt.load(name+msh)
		adapt.save(NAME+msh)
		meshName = NAME
	elif fine == 2: #lc = lcmax = 0.032; [E/2, lcmin/5, lcmax/2]; 0.0355697030569
		mesh.adaptMesh([5], [myLS], [[0.032, 0.0128, 0.032]], 25, True)
		print'---- SPLITTING MESH'
		model = GModel()
		model = mesh.buildCutGModel(myLS, False, True)
		name = "adapt2"
		model.save(name+msh)
		NAME = "ADAPT2"
		adapt = GModel()
		adapt.load(name+msh)
		adapt.save(NAME+msh)
		meshName = NAME
	elif fine == 3: #lc = lcmax = 0.016; [E/4, lcmin/25, lcmax/4]; 0.00911330995603
		mesh.adaptMesh([5], [myLS], [[0.016, 0.00256, 0.016]], 25, True)
		print'---- SLITTING MESH'
		model = GModel()
		model = mesh.buildCutGModel(myLS, False, True)
		name = "adapt3"
		model.save(name+msh)
		NAME = "ADAPT3"
		adapt = GModel()
		adapt.load(name+msh)
		adapt.save(NAME+msh)
		meshName = NAME
	elif fine == 4: #lc = lcmax = 0.008; [E/8, lcmin/125, lcmax/8]; 0.00223522777497
		mesh.adaptMesh([5], [myLS], [[0.008, 0.000512, 0.008]], 25, True)
		print'---- SLITTING MESH'
		model = GModel()
		model = mesh.buildCutGModel(myLS, False, True)
		name = "adapt4"
		model.save(name+msh)
		NAME = "ADAPT4"
		adapt = GModel()
		adapt.load(name+msh)
		adapt.save(NAME+msh)
		meshName = NAME
	elif fine == 5: #lc = lcmax = 0.004; [E/16, lcmin/625, lcmax/16]; 0.000469348970242
		mesh.adaptMesh([5], [myLS], [[0.0045, 0.0001024, 0.004]], 25, True) 
		print'---- SLITTING MESH'
		model = GModel()
		model = mesh.buildCutGModel(myLS, False, True)
		name = "adapt5"
		model.save(name+msh)
		NAME = "ADAPT5"
		adapt = GModel()
		adapt.load(name+msh)
		adapt.save(NAME+msh)
		meshName = NAME
	elif fine == 6: #lc = lcmax = 0.002; [E/32, lcmin/3125, lcmax/32]; 0.000105126052111
		mesh.adaptMesh([5], [myLS], [[0.0024, 0.00002048, 0.002]], 25, True) 
		print'---- SLITTING MESH'
		model = GModel()
		model = mesh.buildCutGModel(myLS, False, True)
		name = "adapt6"
		model.save(name+msh)
		NAME = "ADAPT6"
		adapt = GModel()
		adapt.load(name+msh)
		adapt.save(NAME+msh)
		meshName = NAME	
		
else:
	print'---- LOADING GEOMETRY'		
	mesh = GModel()
	mesh.load(caseName+geo)
	print'---- GENERATING INITIAL ISOTROPIC MESH'
	mesh.adaptMesh([5], [myLS], [[0.064, 0.064, 0.064]], 25, True)
	if fine == 1: #lc = lcmax = 0.064; [E = lcmin = lcmax]; 0.0967989105873
		print'---- SLITTING INITIAL MESH'
		model = GModel()
		model = mesh.buildCutGModel(myLS, False, True)
		name = "refine"
		model.save(name+msh)
		NAME = "REFINE1"
		adapt = GModel()
		adapt.load(name+msh)
		adapt.save(NAME+msh)
		meshName = NAME
	if fine == 2: #lc = lcmax/2 = 0.032; 0.0390818742487
		print'---- REFINING INITIAL MESH BY SPLITTING ELEMENTS'
		mesh.refineMesh(1)
		print'---- SLITTING REFINED MESH'
		model = GModel()
		model = mesh.buildCutGModel(myLS, False, True)
		NAME = "REFINE2"
		model.save(NAME+msh)
		meshName = NAME
	if fine == 3: #lc = lcmax/4 = 0.016; 0.017364736771
		print'---- REFINING INTITIAL MESH BY SPLITTING ELEMENTS'
		mesh.refineMesh(1)
		mesh.refineMesh(1)
		print'---- SLITTING REFINED MESH'
		model = GModel()
		model = mesh.buildCutGModel(myLS, False, True)
		NAME = "REFINE3"
		model.save(NAME+msh)
		meshName = NAME
	if fine == 4: #lc = lcmax/8 = 0.008; 0.00889903836237
		print'---- REFINING INITIAL MESH BY SPLITTING ELEMENTS'
		mesh.refineMesh(1)
		mesh.refineMesh(1)
		mesh.refineMesh(1)
		print'---- SLITTING REFINED MESH'
		model = GModel()
		model = mesh.buildCutGModel(myLS, False, True)
		NAME = "REFINE4"
		model.save(NAME+msh)
		meshName = NAME
	if fine == 5: #lc = lcmax/16 = 0.004; 0.00479770009404
		print'---- REFINING INITIAL MESH BY SPLITTING ELEMENTS'
		mesh.refineMesh(1)
		mesh.refineMesh(1)
		mesh.refineMesh(1)
		mesh.refineMesh(1)
		print'---- SLITTING REFINED MESH'
		model = GModel()
		model = mesh.buildCutGModel(myLS, False, True)
		NAME = "REFINE5"
		model.save(NAME+msh)
		meshName = NAME
	if fine == 6: #lc = lcmax/16 = 0.002; 0.00230611080766
		print'---- REFINING INITIAL MESH BY SPLITTING ELEMENTS'
		mesh.refineMesh(1)
		mesh.refineMesh(1)
		mesh.refineMesh(1)
		mesh.refineMesh(1)
		mesh.refineMesh(1)
		print'---- SLITTING REFINED MESH'
		model = GModel()
		model = mesh.buildCutGModel(myLS, False, True)
		NAME = "REFINE6"
		model.save(NAME+msh)
		meshName = NAME

print'       '
print'*------------------dgCode------------------*'
print'|          |                               |'
print'| Dolbow2D |   INITIALIZATION OF PROBLEM   |'
print'|          |                               |'
print'*------------------------------------------*'

print'---- BUILD GROUPS'
MODEL = GModel()
MODEL.load(meshName+msh)
groups = dgGroupCollection.newByTag(MODEL, 2, 1, ["inside_out"])
groups.splitGroupsByPhysicalTag()
XYZ = groups.getFunctionCoordinates()

print'---- LOAD CONSERVATION LAW'
kappa = functionConstant(1)
if (CG):
	law = dgConservationLawAdvectionDiffusionSUPG.diffusionLaw(kappa)
else:
	law = dgConservationLawAdvectionDiffusion.diffusionLaw(kappa)

print'---- INITIAL CONDITION'
INIT = functionConstant(0)
solution = dgDofContainer(groups,law.getNbFields()) 
solution.interpolate(INIT) 
solution.exportMsh('output/initT', 0., 0) 
	
print'---- BOUNDARY CONDITIONS'
zeroBC = functionConstant(0)
LSBC = functionPython(1, lsBC, [XYZ])

law.addBoundaryCondition('top', law.newOutsideValueBoundary("",zeroBC))
law.addBoundaryCondition('left_out', law.newOutsideValueBoundary("", zeroBC)) 
law.addBoundaryCondition('right_out', law.newOutsideValueBoundary("",zeroBC))
law.addBoundaryCondition('levelset_L1', law.newOutsideValueBoundary("", LSBC))

if (CG):
	law.addStrongBoundaryCondition(1, 'top', zeroBC)
	law.addStrongBoundaryCondition(1, 'left_out', zeroBC)
	law.addStrongBoundaryCondition(1, 'right_out', zeroBC)
	law.addStrongBoundaryCondition(1, 'levelset_L1', LSBC)

print'     '
print'*-------------dgCode-------------*'
print'|          |                     |'
print'| Dolbow2D |   SOLVING PROBLEM   |'
print'|          |                     |'
print'*--------------------------------*'

print'---- Solver Definition'
if (CG):
	petsc =  linearSystemPETScDouble()
#	petsc =  linearSystemFullDouble()
	petsc.setParameter("petscOptions", "-pc_type lu")
	petsc.setParameter("matrix_reuse", "same_matrix")
	dof = dgDofManager.newCG(groups, 1, petsc)
else:
	petsc = linearSystemPETScBlockDouble()
	petsc.setParameter("petscOption", "-pc_type lu -ksp_monitor")
	dof = dgDofManager.newDGBlock(groups, law.getNbFields(), petsc)	

print'---- Solving...'
maxIter = 10000
steady = dgSteady(law, dof)
#steady.getNewton().setVerb(10)
#steady.getNewton().setRtol(1e15)
steady.getNewton().setMaxIt(maxIter)
steady.solve(solution)
solution.exportMsh("output/solution", 0, 0)

print'       '
print'*------------dgCode-------------*'
print'|          |                    |'
print'| Dolbow2D |   POSTPROCESSING   |'
print'|          |                    |'
print'*-------------------------------*'	

print'---- Calculating result at given point'
X = 0.5
Y = 0.335
eval1 = dgFunctionEvaluator(groups, solution.getFunction())
result = fullMatrixDouble(1,1)
eval1.compute(X,Y,0,result)
TNUM = result.get(0,0)
TANA = sin(pi*X)*(cosh(pi*Y)-((exp(2*pi)+1)/(exp(2*pi)-1))*sinh(pi*Y))

print'---- Calculating error'
fana = functionPython(1, exactSL, [XYZ])
ana = dgDofContainer(groups, 1)
ana.interpolate(fana)
ana.exportMsh("output/exact_solution", 0, 0)

print'---- integrating ...'
print'     results coming soon ... :-)'
fanaSol = functionPython(1, anaSol, [XYZ])
integrator1 = dgFunctionIntegrator(groups, fanaSol, solution)
matrix1 = fullMatrixDouble(1,1)
integrator1.compute(matrix1, "inside_out")
result1 = matrix1.get(0,0)

ANA = ana.getFunction()
NUM = solution.getFunction()
ferror = functionPython(1, error, [ANA, NUM]);
integrator2 = dgFunctionIntegrator(groups, ferror)
matrix2 = fullMatrixDouble(1,1)
integrator2.compute(matrix2, "inside_out")
result2 = matrix2.get(0,0)

ERROR1 = sqrt(result2/result1)

print'     exporting error ...'
error = dgDofContainer(groups, 1)
error.interpolate(ferror)
error.exportMsh("output/error", 0, 0)

print'       '
print'*---------------dgCode---------------*'
print'|          |                         |'
print'| Dolbow2D |   SUMMARY FOR RESULTS   |'
print'|          |                         |'
print'*------------------------------------*'

print "1. ERROR AT ONE POINT: (x=%g,y=%g)" % (X,Y)
print "   Analytical result =", TANA
print "   Numerical  result =", TNUM

print "2. ERROR IN THE BULK FIELD:", 'fine =', fine, ERROR1
