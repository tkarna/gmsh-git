from dgpy import *
from Common import *
import os
import time
import math


TIME = function.getTime()
os.system("rm output/*")
#-------------------------------------------------
#-- Case specific functions
#-------------------------------------------------
def exactSL(FCT, XYZ):
        for i in range(0,FCT.size1()):
		x = XYZ.get(i,0)
                y = XYZ.get(i,1)	
		z = XYZ.get(i,2)
		r = sqrt((x-0.5)**2+(y-0.5)**2+(z-0.5)**2)		
		value = log10(r)
		FCT.set(i, 0, value)

def boundaryCondition(FCT, XYZ):
        for i in range(0,FCT.size1()):
		x = XYZ.get(i,0)
                y = XYZ.get(i,1)	
		z = XYZ.get(i,2)
		r = sqrt((x-0.5)**2+(y-0.5)**2+(z-0.5)**2)		
		value = log10(r)
		FCT.set(i, 0, value)
#-------------------------------------------------
#-- Initialize level set problem
#-------------------------------------------------
print'---- Load Mesh & Geometry'
meshName = "square2"
dim = 3
CUT = False
ADAPT = True
order = 1
kappa1 = 1
kappa2 = 100
CG = True

print'---- Generate isotropic mesh'
genMesh(meshName,dim,1)
g = GModel()
g.load(meshName+geo)
if (not ADAPT):
	g.load(meshName+msh)

print'---- Define level set on that mesh'
#ls0 = gLevelsetMathEval("Sqrt((x-0.5)^2+(y-0.5)^2+(z-0.5)^2)-(0.3)")
#ls = gLevelsetMathEval("-x+1/3")
#myLS = ls

ls = gLevelsetPopcorn(0.5, 0.5, 0.2, 0.25, 4.0, 0.08) #xc, yc, zc, r0, A, sigma
myLS = ls

print'---- If (ADAPT) adapt mesh with ls'
if (ADAPT):
	g.adaptMesh([5], [myLS], [[0.2, 0.0001, 0.04, 25]], 40, True)
	g.save("testAdapt"+msh)

print'---- CUT/SPLIT the mesh'
g2 = g.buildCutGModel(myLS, CUT, True)
g2.save("testCUT"+msh)
exit(1)

print'---- Build Groups'
meshName = "testCUT"
model = GModel()
model.load(meshName+msh) 

#1. cube.msh (no cut nor split) -> it seem work okie
'''
#load cube.msh
meshName = "cube"
model = GModel()
model.load(meshName+msh) 

#build groups
groups = dgGroupCollection(model, dim, order) 
groups.splitGroupsByPhysicalTag()
groups.buildGroupsOfInterfaces()
XYZ = groups.getFunctionCoordinates()

#conservation law
kappa = functionConstant(kappa1)
law = dgConservationLawAdvectionDiffusionSUPG.diffusionLaw(kappa)

#initial condition
INIT_TEMP = functionConstant(0)
solution = dgDofContainer(groups,law.getNbFields()) 
solution.interpolate(INIT_TEMP) 
solution.exportMsh('output/initT', 0., 0) 

#boundary conditions
BC = functionConstant([1])

law.addBoundaryCondition('plane1', law.newOutsideValueBoundary("",BC))
law.addBoundaryCondition('plane2', law.newOutsideValueBoundary("",BC))
law.addBoundaryCondition('plane3', law.newOutsideValueBoundary("",BC))
law.addBoundaryCondition('plane4', law.newOutsideValueBoundary("",BC))
law.addBoundaryCondition('plane5', law.newOutsideValueBoundary("",BC))
law.addBoundaryCondition('plane6', law.newOutsideValueBoundary("",BC))

law.addStrongBoundaryCondition(2, 'plane1', BC)
law.addStrongBoundaryCondition(2, 'plane2', BC)
law.addStrongBoundaryCondition(2, 'plane3', BC)
law.addStrongBoundaryCondition(2, 'plane4', BC)
law.addStrongBoundaryCondition(2, 'plane5', BC)
law.addStrongBoundaryCondition(2, 'plane6', BC)

#steady solver
petsc =  linearSystemPETScDouble()
dof = dgDofManager.newCG(groups, 1, petsc)
petsc.setParameter("petscOptions", "-pc_type lu")
petsc.setParameter("matrix_reuse", "same_matrix")

maxIter = 100
steady = dgSteady(law, dof)
steady.getNewton().setVerb(2)
steady.getNewton().setMaxIt(maxIter)
steady.solve(solution)
solution.exportMsh("output/steadySol", 0, 0)

exit(0)
'''

#2. testCUT.msh (use ls to cut/split cube.msh)
	# cut = True 
		# -> Error msg: no boundary condition defined with tag 'none'
		# -> levelset_S1 + levelset_L1: interface & boundary line of interface (includes some nodes of the cut mesh!!!)	
	# cut = False
		# -> mesh error: no levelset_L1: plane1_in= plane1_in + levelset_S1
'''		
if (CUT):
	groups = dgGroupCollection.newByTag(model, dim, order, ["vol_out"]) 
	groups.splitGroupsByPhysicalTag()
	groups.buildGroupsOfInterfaces()
	XYZ = groups.getFunctionCoordinates()
else:
	groups = dgGroupCollection.newByTag(model, dim, order, ["vol_out"])
	groups.splitGroupsByPhysicalTag()
	groups.buildGroupsOfInterfaces() 
	XYZ = groups.getFunctionCoordinates()

print'---- Load Conservation law'
if (CUT):
	kappa = functionConstant(kappa1)
else:
	INITLS = functionLevelsetValueNew(myLS)
	kappa = functionLevelsetSmoothNew(INITLS, kappa2, kappa1, 0.0001)
	
if (CG):
	law = dgConservationLawAdvectionDiffusionSUPG.diffusionLaw(kappa)
else:
	law = dgConservationLawAdvectionDiffusion.diffusionLaw(kappa)
	
print'---- Initial condition'
INIT_TEMP = functionConstant(0)
solution = dgDofContainer(groups,law.getNbFields()) 
solution.interpolate(INIT_TEMP) 
solution.exportMsh('output/initT', 0., 0) 
	
print'---- Load Boundary conditions'
BC = functionConstant([1])
	
law.addBoundaryCondition('plane1_out', law.newOutsideValueBoundary("",BC))
law.addBoundaryCondition('plane2_out', law.newOutsideValueBoundary("",BC))
law.addBoundaryCondition('plane3_out', law.newOutsideValueBoundary("",BC))
law.addBoundaryCondition('plane4_out', law.newOutsideValueBoundary("",BC))
law.addBoundaryCondition('plane5', law.newOutsideValueBoundary("",BC))
law.addBoundaryCondition('plane6', law.newOutsideValueBoundary("",BC))	

law.addBoundaryCondition('levelset_S1', law.newOutsideValueBoundary("", BC))
#law.addBoundaryCondition('levelset_L1', law.newOutsideValueBoundary("", BC))

if (CG):
        law.addStrongBoundaryCondition(2, 'plane1_out', BC)
	law.addStrongBoundaryCondition(2, 'plane2_out', BC)
	law.addStrongBoundaryCondition(2, 'plane3_out', BC)
	law.addStrongBoundaryCondition(2, 'plane4_out', BC)
	law.addStrongBoundaryCondition(2, 'plane5', BC)
	law.addStrongBoundaryCondition(2, 'plane6', BC)
        law.addStrongBoundaryCondition(2, 'levelset_S1', BC)
        #law.addStrongBoundaryCondition(1, 'levelset_L1', BC)
'''

# 3. testCUT.msh (use ls2 to cut/split cube.msh -> popcorn)
	# cut = True
		# -> Error msg: no boundary condition defined with tag 'none'
	# cut = False

if (CUT):
	groups = dgGroupCollection.newByTag(model, dim, order, ["vol_out"]) 
	groups.splitGroupsByPhysicalTag()
	groups.buildGroupsOfInterfaces()
	XYZ = groups.getFunctionCoordinates()
else:
	groups = dgGroupCollection.newByTag(model, dim, order, ["vol_out"])
	groups.splitGroupsByPhysicalTag()
	groups.buildGroupsOfInterfaces() 
	XYZ = groups.getFunctionCoordinates()

print'---- Load Conservation law'

if (CUT):
	kappa = functionConstant(kappa1)
else:
	INITLS = functionLevelsetValueNew(myLS)
	kappa = functionLevelsetSmoothNew(INITLS, kappa2, kappa1, 0.0001)
	
if (CG):
	law = dgConservationLawAdvectionDiffusionSUPG.diffusionLaw(kappa)
else:
	law = dgConservationLawAdvectionDiffusion.diffusionLaw(kappa)
	
print'---- Initial condition'
INIT_TEMP = functionConstant(0)
solution = dgDofContainer(groups,law.getNbFields()) 
solution.interpolate(INIT_TEMP) 
solution.exportMsh('output/initT', 0., 0) 
	
print'---- Load Boundary conditions'
#BC = functionConstant([1])
BC = functionPython(1, boundaryCondition, [XYZ])
	
law.addBoundaryCondition('plane1', law.newOutsideValueBoundary("",BC))
law.addBoundaryCondition('plane2', law.newOutsideValueBoundary("",BC))
law.addBoundaryCondition('plane3', law.newOutsideValueBoundary("",BC))
law.addBoundaryCondition('plane4', law.newOutsideValueBoundary("",BC))
law.addBoundaryCondition('plane5', law.newOutsideValueBoundary("",BC))
law.addBoundaryCondition('plane6', law.newOutsideValueBoundary("",BC))	

law.addBoundaryCondition('levelset_S1', law.newOutsideValueBoundary("", BC))

if (CG):
        law.addStrongBoundaryCondition(2, 'plane1', BC)
	law.addStrongBoundaryCondition(2, 'plane2', BC)
	law.addStrongBoundaryCondition(2, 'plane3', BC)
	law.addStrongBoundaryCondition(2, 'plane4', BC)
	law.addStrongBoundaryCondition(2, 'plane5', BC)
	law.addStrongBoundaryCondition(2, 'plane6', BC)
        law.addStrongBoundaryCondition(2, 'levelset_S1', BC)

#-------------------------------------------------
#-- solver setup
#-------------------------------------------------
print'---- Solver Definition'
dof = None
if (CG):
	petsc =  linearSystemPETScDouble()
	dof = dgDofManager.newCG(groups, 1, petsc)
	petsc.setParameter("petscOptions", "-pc_type lu")
	petsc.setParameter("matrix_reuse", "same_matrix")
else:
	petsc = linearSystemPETScBlockDouble()
	petsc.setParameter("petscOption", "-pc_type lu -ksp_monitor")
	dof = dgDofManager.newDGBlock(groups, 1, petsc)
	
#-------------------------------------------------
#-- steady solve
#-------------------------------------------------
maxIter = 100
steady = dgSteady(law, dof)
steady.getNewton().setVerb(2)
steady.getNewton().setMaxIt(maxIter)
steady.solve(solution)
solution.exportMsh("output/steadySol", 0, 0)
#-------------------------------------------------
#-- Validation
#-------------------------------------------------	
#------------export result at 1 point------------
eval1 = dgFunctionEvaluator(groups, solution.getFunction())
result = fullMatrixDouble(1,1)
x = 0.9
y = 0.9
z = 0.9
eval1.compute(x,y,0,result)
	
TNUM = result.get(0,0)
TANA = log(sqrt((x-0.5)**2+(y-0.5)**2+(z-0.5)**2))

print "1. Error at one point: (x=%g,y=%g)" % (x,y)
print "   Tnum=", TNUM, "& Tana =", TANA
print "   Tnum-Tana =", TNUM-TANA, "& Tnum/Tana =", TNUM/TANA

#------------export exact solution for viewing in post-processing------------
EXACT_SOLUTION = functionPython(1, exactSL, [XYZ])
solution = dgDofContainer(groups,law.getNbFields()) 
solution.interpolate(EXACT_SOLUTION) 
solution.exportMsh('output/exact_solution', 0, 0) 

#------------define integration function-------------
def L2norm(FCT, solution, XYZ):
        for i in range(0,FCT.size1()): 
		x = XYZ.get(i,0) 
                y = XYZ.get(i,1) 
		z = XYZ.get(i,2)
		r = sqrt((x-0.5)**2+(y-0.5)**2+(z-0.5)**2)
		Tana = log(r)
		Tnum = solution(0,i) 
		L2norm = (Tnum-Tana)*(Tnum-Tana)
		FCT.set(i,0, L2norm)

def L2normANA(FCT, solution, XYZ):
        for i in range(0,FCT.size1()): 
		x = XYZ.get(i,0)
                y = XYZ.get(i,1) 
		z = XYZ.get(i,2)
		r = sqrt((x-0.5)**2+(y-0.5)**2+(z-0.5)**2)
		Tana = log(r)
		FCT.set(i,0, Tana*Tana)

L2NORM = functionPython(1, L2norm, [solution.getFunction(), XYZ])
L2NORMANA = functionPython(1, L2normANA, [solution.getFunction(), XYZ])

#------------integration over a groups_of_element/levelset surface------------
errorInte1 = dgFunctionIntegratorInterface(groups, L2NORM, solution)
error1 = fullMatrixDouble(1,1)
anaInte1 = dgFunctionIntegratorInterface(groups, L2NORMANA, solution)
ana1 = fullMatrixDouble(1,1)

if (CUT):
	errorInte1.compute("levelset_S1", error1)
	anaInte1.compute("levelset_S1", ana1)
else:
	errorInte1.compute("levelset_S1", error1)
	anaInte1.compute("levelset_S1", ana1)

ERROR = error1.get(0,0)
ANA = ana1.get(0,0)

print "2. Error on the boundary (levelset) = ", sqrt(ERROR)/sqrt(ANA)

#------------integration over a groups_of_element/volume------------
errorInte2 = dgFunctionIntegrator(groups, L2NORM, solution)
error2 = fullMatrixDouble(1,1)
anaInte2 = dgFunctionIntegrator(groups, L2NORMANA, solution)
ana2 = fullMatrixDouble(1,1)

if (CUT):
	errorInte2.compute(error2, "vol_out")
	anaInte2.compute(ana2, "vol_out")
else:
	errorInte2.compute(error2, "vol_out")
	anaInte2.compute(ana2, "vol_out")

ERROR_VOL = error2.get(0,0)
ANA_VOL = ana2.get(0,0)

print "3. Error on the volume = ", sqrt(ERROR_VOL)/sqrt(ANA_VOL)

exit(0)

#-----------evaluating Geometry Error/Volume Error-----------
constant = 1
constantFunction = functionConstant([constant])
constantFunctionIntegrator = dgFunctionIntegrator(groups, constantFunction, solution)
splitAreaMatrix = fullMatrixDouble(1,1) 
cutAreaMatrix = fullMatrixDouble(1,1) 

if (CUT):
	constantFunctionIntegrator.compute('levelset_S1', cutAreaMatrix) 
	cutArea = cutAreaMatrix.get(0,0)
	splitArea = cutArea
else:
	constantFunctionIntegrator.compute('levelset_S1', splitAreaMatrix) 
	splitArea = splitAreaMatrix.get(0,0)
	constantFunctionIntegrator.compute('levelset_S2', cutAreaMatrix) 
	cutArea = cutAreaMatrix.get(0,0)

print "4. Error on the geometry: Area_difference = split_area - cut_area =", splitArea-cutArea, " & Area_ratio = split_area/cut_area =", splitArea/cutArea
	
