from dgpy import *
from Common import *
import os
import time
import math


TIME = function.getTime()
os.system("rm output/*")

#-------------------------------------------------
#-- Initialize level set problem
#-------------------------------------------------

#GmshSetOption('General', 'Verbosity', 1.)#double here and no int

print'---- Load Mesh & Geometry'
meshName = "cube"
#meshName   = "cyl"
#meshName   = "carotid"
dim = 3
CUT = True
ADAPT = True

print'---- Generate isotropic mesh'
genMesh(meshName,dim,1)
g = GModel()
g.load(meshName+geo)
if (not ADAPT):
	g.load(meshName+msh)

print'---- Define level set on that mesh'
ls0 = gLevelsetMathEval("Sqrt((x-0.5)^2+(y-0.5)^2+(z-0.5)^2)-(0.3)")
ls1 = gLevelsetMathEval("(y-0.5)")
ls33 = gLevelsetMathEval("-4.4+x")
ls2 = gLevelsetPopcorn(0.5, 0.5, 0.5, 0.25, 4.0, 0.1) #r0, A, sigma
ls3 = gLevelsetDistGeom(meshName, "sphere.stl")

plane = gLevelsetPlane([0.0404, 0.0738, 0.05], [0.0, 0.0, 1.0]);
cyl =  gLevelsetCylinder([0.0404, 0.0738, 0.0495], [0.0, 0.0, 1.0], 0.005, 0.001)
ls4 = gLevelsetCrack([plane, cyl])

i1 = gLevelsetIntersection([ls0,ls1])

myLS = ls2#i1

print'---- If (ADAPT) adapt mesh with ls'
if (ADAPT):
	adaptAllDim = True
	g.adaptMesh([5], [myLS], [[0.1, 1.e-3, 1.1]], 10, False)

print'---- CUT/SPLIT the mesh'
g2 = g.buildCutGModel(myLS, CUT, True)
g2.save("cubeMYCUT"+msh)

#g3 = GModel()
#g3.load("cubeMYCUT"+msh)
#g3.save("cubeMYCUT2"+msh)


exit(success) 

