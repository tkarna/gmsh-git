#Woven composite: cell size = (2*2*2) + orthogonal fibers with radius (R=0.1)
#E-glass fibers & epoxy resin: 

from dgpy import *
from Common import *
import math 
import os
import time

Ef = 400e9
nf = 0.3
Em = 72e9
nm = 0.33
disp=0.02 #1%


#meshName = "composite2"
meshName = "3Dwoven-1"
m = GModel()
m.load(meshName+msh)

e = elasticitySolver(m,1)

e.addElasticDomain('vol_out',Em,nm)
e.addElasticDomain('vol_in',Ef,nf)

#traction along y-axis
e.addDirichletBC(2,'plane3_out',1,0.)
e.addDirichletBC(2,'plane3_in',1,0.)
e.addDirichletBC(2,'plane3',1,0.)
e.addDirichletBC(2,'plane4_out',1,disp)
e.addDirichletBC(2,'plane4_in',1,disp)
e.addDirichletBC(2,'plane4',1,disp)
e.addDirichletBC(0,'pt1',0,0.)
e.addDirichletBC(0,'pt1',2,0.)

sys = linearSystemPETScDouble()
print'---- Assembling system'
e.assemble(sys)
print'---- Solving system'
e.solve()
print'---- System solved'

view = e.buildStressesView("stresses")
view.write("stress.msh", 5, False)


