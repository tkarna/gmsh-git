#25May2012
#Woven composite: cell size = (2*2*2) + sinuosidal fibers with radius (R=0.1) and amplitude 0.25
from dgpy import *
from Common import *
import math 
import os

os.system("rm meshAdapt-*")

caseName  = "cube"
print'---- DEFINE LEVELSET ON MESH'
r=0.1
a=0.25
A=str(a)
R=str(r)
PI=str(math.pi)
#x=1.5
ls1 = gLevelsetMathEval('(x-1.5)^2+(y-1)^2-'+R+'^2-'+A+'^2*cos('+PI+'*z)*cos('+PI+'*z)-2*'+A+'*cos('+PI+'*z)*(x-1.5-'+A+'*cos('+PI+'*z))',1)
ls2 = gLevelsetMathEval('(x-1.5)^2+(y-0)^2-'+R+'^2-'+A+'^2*cos('+PI+'*z+'+PI+')*cos('+PI+'*z+'+PI+')-2*'+A+'*cos('+PI+'*z+'+PI+')*(x-1.5-'+A+'*cos('+PI+'*z+'+PI+'))',2)
ls3 = gLevelsetMathEval('(x-1.5)^2+(y-2)^2-'+R+'^2-'+A+'^2*cos('+PI+'*z+'+PI+')*cos('+PI+'*z+'+PI+')-2*'+A+'*cos('+PI+'*z+'+PI+')*(x-1.5-'+A+'*cos('+PI+'*z+'+PI+'))',3)
ls4 = gLevelsetMathEval('(x-1.5)^2+(z-1)^2-'+R+'^2-'+A+'^2*cos('+PI+'*y+'+PI+')*cos('+PI+'*y+'+PI+')-2*'+A+'*cos('+PI+'*y+'+PI+')*(x-1.5-'+A+'*cos('+PI+'*y+'+PI+'))',4)
ls5 = gLevelsetMathEval('(x-1.5)^2+(z-0)^2-'+R+'^2-'+A+'^2*cos('+PI+'*y)*cos('+PI+'*y)-2*'+A+'*cos('+PI+'*y)*(x-1.5-'+A+'*cos('+PI+'*y))',5)
ls6 = gLevelsetMathEval('(x-1.5)^2+(z-2)^2-'+R+'^2-'+A+'^2*cos('+PI+'*y)*cos('+PI+'*y)-2*'+A+'*cos('+PI+'*y)*(x-1.5-'+A+'*cos('+PI+'*y))',6)
#x=0.5
ls7 = gLevelsetMathEval('(x-0.5)^2+(y-1)^2-'+R+'^2-'+A+'^2*cos('+PI+'*z)*cos('+PI+'*z)-2*'+A+'*cos('+PI+'*z)*(x-0.5-'+A+'*cos('+PI+'*z))',7)
ls8 = gLevelsetMathEval('(x-0.5)^2+(y-0)^2-'+R+'^2-'+A+'^2*cos('+PI+'*z+'+PI+')*cos('+PI+'*z+'+PI+')-2*'+A+'*cos('+PI+'*z+'+PI+')*(x-0.5-'+A+'*cos('+PI+'*z+'+PI+'))',8)
ls9 = gLevelsetMathEval('(x-0.5)^2+(y-2)^2-'+R+'^2-'+A+'^2*cos('+PI+'*z+'+PI+')*cos('+PI+'*z+'+PI+')-2*'+A+'*cos('+PI+'*z+'+PI+')*(x-0.5-'+A+'*cos('+PI+'*z+'+PI+'))',9)
ls10 = gLevelsetMathEval('(x-0.5)^2+(z-1)^2-'+R+'^2-'+A+'^2*cos('+PI+'*y+'+PI+')*cos('+PI+'*y+'+PI+')-2*'+A+'*cos('+PI+'*y+'+PI+')*(x-0.5-'+A+'*cos('+PI+'*y+'+PI+'))',10)
ls11 = gLevelsetMathEval('(x-0.5)^2+(z-0)^2-'+R+'^2-'+A+'^2*cos('+PI+'*y)*cos('+PI+'*y)-2*'+A+'*cos('+PI+'*y)*(x-0.5-'+A+'*cos('+PI+'*y))',11)
ls12 = gLevelsetMathEval('(x-0.5)^2+(z-2)^2-'+R+'^2-'+A+'^2*cos('+PI+'*y)*cos('+PI+'*y)-2*'+A+'*cos('+PI+'*y)*(x-0.5-'+A+'*cos('+PI+'*y))',12)

ls20 = gLevelsetUnion([ls1,ls2,ls3,ls4,ls5,ls6,ls7,ls8,ls9,ls10,ls11,ls12])

print'---- LOADING GEOMETRY'		
mesh = GModel()
mesh.load(caseName+geo)
print'---- ADAPT MESH WITH LEVELSET'
thick1=0.2 # ls>0 : outside
minV=0.015
maxV=0.1
thick2=0.03 # ls<0 : inside
tec=5

mesh.adaptMesh([tec],[ls20],[[thick1,minV,maxV,thick2]],10,True)
print'---- SPLITTING MESH'
model1 = GModel()
model1 = mesh.buildCutGModel(ls20, False, True)

name = "composite2"
model1.save(name+msh)

#work on exact mesh

"""
#compute exact surface
def triSurf(x1,y1,z1,x2,y2,z2,x3,y3,z3):
  return 0.5*sqrt((x1*(y2-y3)-x2*(y1-y3)+x3*(y1-y2))*(x1*(y2-y3)-x2*(y1-y3)+x3*(y1-y2))+(y1*(z2-z3)-y2*(z1-z3)+y3*(z1-z2))*(y1*(z2-z3)-y2*(z1-z3)+y3*(z1-z2))+(z1*(x2-x3)-z2*(x1-x3)+z3*(x1-x2))*(z1*(x2-x3)-z2*(x1-x3)+z3*(x1-x2)))

nbstep=100
dz=2./nbstep
dt=2.*math.pi/nbstep
stot=0
for iz in range(nbstep):
  for it in range(nbstep):
    z=iz*dz
    t=it*dt
    z0=z2=z
    z1=z3=z+dz
    y0=y1=r*sin(t)
    y2=y3=r*sin(t+dt)
    x0=r*cos(t)+a*cos(math.pi*z0)
    x1=r*cos(t)+a*cos(math.pi*z1)
    x2=r*cos(t+dt)+a*cos(math.pi*z2)
    x3=r*cos(t+dt)+a*cos(math.pi*z3)
    s=triSurf(x0,y0,z0,x1,y1,z1,x2,y2,z2)+triSurf(x1,y1,z1,x2,y2,z2,x3,y3,z3)
    stot=stot+s
print 'Exact fibers surface = ', stot*8
"""

Geom = "3Dwoven"
modelG = GModel()
modelG.load(Geom+msh)
#compute total surface
gfaces = modelG.bindingsGetFaces()
surftot=0.
for f in gfaces:
  for e in range(f.getNumMeshElements()):
    el = f.getMeshElement(e)
    x1 = (el.getVertex(0), el.getVertex(1), el.getVertex(2))
    surftot = surftot + el.getVolume()
print 'EF Total surface =', surftot

