#24May2012
#Non-woven composite: cell size = (2*2*2) + orthogonal fibers with radius (R=0.45)
#E-glass fibers & epoxy resin: 
#E_f=72.0; G_f=27.7; nu_f=0.3 [GPa]
#E_m=3.5; G_m=1.3; nu_m=0.35 [GPa]
from dgpy import *
from Common import *
import math 
import os
import time
	
try : os.mkdir("output");
except: 0;

os.system("rm output/*")

print'       '
print'*------------------Composite3D------------------*'
print'|    UCL   |                                    |'
print'|   iMMC   |  DEFINING CASE SPECIFIC FUNCTIONS  |'
print'|  dgCode  |                                    |'
print'*-----------------------------------------------*'

caseName  = "cube"
fine = 1
print'---- DEFINE LEVELSET ON MESH'
ls1 = gLevelsetCylinder([0,1,0], [0,0,1], 0.45, 2)
ls2 = gLevelsetCylinder([2,1,0], [0,0,1], 0.45, 2)
ls3 = gLevelsetCylinder([0,0,0], [1,0,0], 0.45, 2)
ls4 = gLevelsetCylinder([0,0,2], [1,0,0], 0.45, 2)
ls5 = gLevelsetCylinder([0,2,2], [1,0,0], 0.45, 2)
ls6 = gLevelsetCylinder([0,2,0], [1,0,0], 0.45, 2)
ls7 = gLevelsetCylinder([1,0,1], [0,1,0], 0.45, 2)
myLS1 = ls1
myLS2 = ls2
myLS3 = ls3
myLS4 = ls4
myLS5 = ls5
myLS6 = ls6
myLS7 = ls7

print'       '
print'*-------------------dgCode------------------*'
print'|             |                             |'
print'| Composite3D |  GENERATE ANISOTROPIC MESH  |'
print'|             |                             |'
print'*-------------------------------------------*'

print'---- LOADING GEOMETRY'		
mesh = GModel()
mesh.load(caseName+geo)
print'---- ADAPT MESH WITH LEVELSET'
if fine == 1:
	mesh.adaptMesh([4,4,4,4,4,4,4], [myLS1,myLS2,myLS3,myLS4,myLS5,myLS6,myLS7], [[0.15,0.01,0.2],[0.15,0.01,0.2],[0.15,0.01,0.2],[0.15,0.01,0.2],[0.15,0.01,0.2],[0.15,0.01,0.2],[0.15,0.01,0.2]], 25, True)
	print'---- SLITTING MESH'
	model1 = GModel()
	model1 = mesh.buildCutGModel(myLS1, False, True)
	model2 = GModel()
	model2 = model1.buildCutGModel(myLS2, False, True)
	model3 = GModel()
	model3 = model2.buildCutGModel(myLS3, False, True)
	model4 = GModel()
	model4 = model3.buildCutGModel(myLS4, False, True)
	model5 = GModel()
	model5 = model4.buildCutGModel(myLS5, False, True)
	model6 = GModel()
	model6 = model5.buildCutGModel(myLS6, False, True)
	model7 = GModel()
	model7 = model6.buildCutGModel(myLS7, False, True)
	name = "composite1"
	model7.save(name+msh)
	NAME = "ADAPT1"
	adapt = GModel()
	adapt.load(name+msh)
	adapt.save(NAME+msh)
	meshName = NAME
elif fine == 2:
	mesh.adaptMesh([4], [myLS], [[0.045, 0.0065, 0.1]], 25, True)
	print'---- SPLITTING MESH'
	model = GModel()
	model = mesh.buildCutGModel(myLS, False, True)
	name = "adapt2"
	model.save(name+msh)
	NAME = "ADAPT2"
	adapt = GModel()
	adapt.load(name+msh)
	adapt.save(NAME+msh)
	meshName = NAME
