
lc = 0.08;

Point(1) = {0,0,0,lc};         
Point(2) = {2,0,0,lc};         
Point(3) = {2,2,0,lc};         
Point(4) = {0,2,0,lc};
Point(5) = {0,0,2,lc};         
Point(6) = {2,0,2,lc};         
Point(7) = {2,2,2,lc};         
Point(8) = {0,2,2,lc}; 
Physical Point("pt1") = {1};
              
//Line(1) = {5, 6};//
Line(2) = {6, 7};
//Line(3) = {7, 8};//
Line(4) = {8, 5};
Line(5) = {8, 4};
//Line(6) = {4, 3};//
Line(7) = {3, 7};
Line(8) = {3, 2};
Line(9) = {2, 6};
//Line(10) = {2, 1};//
Line(11) = {1, 4};
Line(12) = {1, 5};

r=0.1;  //radius
a=0.25; //amplitude
// high fibers first!
tb={1.5, 1.5, 1.5, 0.5, 0.5, 0.5}; //height
tc={0,   1,   2,   0,   1,   2};   //translation
td={Pi,  0,   Pi,  Pi,  0,   Pi};  //phase
nb=4;  // nb of slices of the fiber
nb2=4; // nb of points for splines
iv=0; // iterator volume
il00=0; //iterator line y=0 z=0
il02=0; //iterator line y=2 z=0;
il20=0; //iterator line y=0 z=2;
il22=0; //iterator line y=2 z=2;
iPhysY0=0;
iPhysY2=0;
iPhysZ0=0;
iPhysZ2=0;
iPhysFibS=0;
iPhysFibV=0;

//fibers along y
fy0u=0;fy0d=0;fy2u=0;fy2d=0;
For f In {0:#tb[]-1}
  b=tb[f];
  c=tc[f];
  d=td[f] + Pi;

  //first circle
  t=a*Cos(d);
  p=newp; Point(p) = {b+t-r, 0, c,lc};
  p=newp; Point(p) = {b+t+r, 0, c,lc};
  If ((c+r) > 2)
    l02[il02]=p;il02++;l02[il02]=p-1;il02++;
    p=newp; Point(p) = {b+t, 0, c-r,lc};
    p=newp; Point(p) = {b+t, 0, c};
    s=newreg; Line(s) = {p-3,p-2};
    s=newreg; Circle(s) = {p-2,p,p-1};
    s=newreg; Circle(s) = {p-1,p,p-3};
    ss=newreg; Line Loop(ss) = {s-2:s};
  EndIf
  If ((c-r) < 0)
    l00[il00]=p;il00++;l00[il00]=p-1;il00++;
    p=newp; Point(p) = {b+t, 0, c+r,lc};
    p=newp; Point(p) = {b+t, 0, c};
    s=newreg; Line(s) = {p-2,p-3};
    s=newreg; Circle(s) = {p-3,p,p-1};
    s=newreg; Circle(s) = {p-1,p,p-2};
    ss=newreg; Line Loop(ss) = {s-2:s};
  EndIf
  If (((c+r) < 2) && ((c-r) > 0))
    p=newp; Point(p) = {b+t, 0, c+r,lc};
    p=newp; Point(p) = {b+t, 0, c-r,lc};
    p=newp; Point(p) = {b+t, 0, c};
    s=newreg; Circle(s) = {p-1,p,p-4};
    s=newreg; Circle(s) = {p-4,p,p-2};
    s=newreg; Circle(s) = {p-2,p,p-3};
    s=newreg; Circle(s) = {p-3,p,p-1};
    ss=newreg; Line Loop(ss) = {s-3:s};
    If(b>1) 
      fy0u=ss;
    EndIf
    If(b<1) 
      fy0d=ss;
    EndIf
  EndIf
  ss=newreg; Plane Surface(ss) = {ss-1}; PhysY0[iPhysY0]=ss;iPhysY0++;

  //fiber
  For i In {1:nb}
    For j In {1:nb2}
      y=(i-1)*2/nb+j*2/nb/nb2;
      x=b+a*Cos(3.1415*y+d);
      p=newp; Point(p) = {x-r, y, c,lc};
      p=newp; Point(p) = {x+r, y, c,lc};
      If((c+r) < 2)
        p=newp; Point(p) = {x, y, c+r,lc};
      EndIf
      If((c-r) > 0)
        p=newp; Point(p) = {x, y, c-r,lc};
      EndIf
    EndFor
    y=i*2/nb;
    x=b+a*Cos(3.1415*y+d);
    p=newp; Point(p) = {x, y, c};
    nbS=0;
    If((c+r) > 2)
      If(i==nb)
        l22[il22]=p-2;il22++;l22[il22]=p-3;il22++;
      EndIf
      ns = i==1 ? 2 : 6;
      s=newreg; Spline(s) = {p-4-3*nb2, p+0-3*nb2 : p-3 : 3};
      s=newreg; Spline(s) = {p-3-3*nb2, p+1-3*nb2 : p-2 : 3};
      s=newreg; Spline(s) = {p-2-3*nb2, p+2-3*nb2 : p-1 : 3};
      s=newreg; Line(s) = {p-3,p-2};
      s=newreg; Circle(s) = {p-2,p,p-1};
      s=newreg; Circle(s) = {p-1,p,p-3};
      ss=newreg; Line Loop(ss) = {s-8-ns,s-4,-(s-2),-(s-5)};
      ss=newreg; Plane Surface(ss) = {ss-1}; PhysZ2[iPhysZ2]=ss;iPhysZ2++;
      ss=newreg; Line Loop(ss) = {s-7-ns,s-3,-(s-1),-(s-4)};
      ss=newreg; Ruled Surface(ss) = {ss-1}; vol[iv]=ss;iv++; PhysFibS[iPhysFibS]=ss;iPhysFibS++;
      ss=newreg; Line Loop(ss) = {s-6-ns,s-5,-(s-0),-(s-3)};
      ss=newreg; Ruled Surface(ss) = {ss-1}; vol[iv]=ss;iv++; PhysFibS[iPhysFibS]=ss;iPhysFibS++;
      nbS=2;
    EndIf
    If((c-r) < 0)
      If (i==nb)
        l20[il20]=p-2;il20++;l20[il20]=p-3;il20++;
      EndIf
      ns = i==1 ? 2 : 6;
      s=newreg; Spline(s) = {p-4-3*nb2, p+0-3*nb2 : p-3 : 3};
      s=newreg; Spline(s) = {p-3-3*nb2, p+1-3*nb2 : p-2 : 3};
      s=newreg; Spline(s) = {p-2-3*nb2, p+2-3*nb2 : p-1 : 3};
      s=newreg; Line(s) = {p-2,p-3};
      s=newreg; Circle(s) = {p-3,p,p-1};
      s=newreg; Circle(s) = {p-1,p,p-2};
      ss=newreg; Line Loop(ss) = {s-8-ns,s-5,-(s-2),-(s-4)};
      ss=newreg; Plane Surface(ss) = {ss-1}; PhysZ0[iPhysZ0]=ss;iPhysZ0++;
      ss=newreg; Line Loop(ss) = {s-7-ns,s-3,-(s-1),-(s-5)};
      ss=newreg; Ruled Surface(ss) = {ss-1}; vol[iv]=ss;iv++; PhysFibS[iPhysFibS]=ss;iPhysFibS++;
      ss=newreg; Line Loop(ss) = {s-6-ns,s-4,-(s-0),-(s-3)};
      ss=newreg; Ruled Surface(ss) = {ss-1}; vol[iv]=ss;iv++; PhysFibS[iPhysFibS]=ss;iPhysFibS++;
      nbS=2;
    EndIf
    If (((c+r) < 2) && ((c-r) > 0))
      ns = i==1 ? 2 : 8;
      s=newreg; Spline(s) = {p-5-4*nb2, p-0-4*nb2 : p-4 : 4};
      s=newreg; Spline(s) = {p-4-4*nb2, p+1-4*nb2 : p-3 : 4};
      s=newreg; Spline(s) = {p-3-4*nb2, p+2-4*nb2 : p-2 : 4};
      s=newreg; Spline(s) = {p-2-4*nb2, p+3-4*nb2 : p-1 : 4};
      s=newreg; Circle(s) = {p-1,p,p-4};
      s=newreg; Circle(s) = {p-4,p,p-2};
      s=newreg; Circle(s) = {p-2,p,p-3};
      s=newreg; Circle(s) = {p-3,p,p-1};
      ss=newreg; Line Loop(ss) = {s-0,-(s-4),-(s-8-ns),s-6};
      ss=newreg; Ruled Surface(ss) = {ss-1}; vol[iv]=ss;iv++; PhysFibS[iPhysFibS]=ss;iPhysFibS++;
      ss=newreg; Line Loop(ss) = {s-1, -(s-6), -(s-9-ns), s-5};
      ss=newreg; Ruled Surface(ss) = {ss-1}; vol[iv]=ss;iv++; PhysFibS[iPhysFibS]=ss;iPhysFibS++;
      ss=newreg; Line Loop(ss) = {s-2, -(s-5), -(s-10-ns), s-7};
      ss=newreg; Ruled Surface(ss) = {ss-1}; vol[iv]=ss;iv++; PhysFibS[iPhysFibS]=ss;iPhysFibS++;
      ss=newreg; Line Loop(ss) = {s-3, -(s-7), -(s-11-ns), s-4};
      ss=newreg; Ruled Surface(ss) = {ss-1}; vol[iv]=ss;iv++; PhysFibS[iPhysFibS]=ss;iPhysFibS++;
      nbS=3;
    EndIf
  EndFor
  ss=newreg; Line Loop(ss) = {s-nbS:s};
  If(b>1) 
    fz2u=ss;
  EndIf
  If(b<1) 
    fz2d=ss;
  EndIf
  ss=newreg; Plane Surface(ss) = {ss-1}; PhysY2[iPhysY2]=ss;iPhysY2++;
  If(((c+r) < 2) && ((c-r) > 0))
    ss=newreg; Surface Loop(ss) = {ss-3-16*nb, ss-9-16*(nb-1):ss-9:16, ss-7-16*(nb-1):ss-7:16, ss-5-16*(nb-1):ss-5:16, ss-3-16*(nb-1):ss-3:16, ss-1};
  EndIf
  If(((c+r) > 2) || ((c-r) < 0))
    ss=newreg; Surface Loop(ss) = {ss-3-12*nb, ss-7-12*(nb-1):ss-7:12, ss-5-12*(nb-1):ss-5:12, ss-3-12*(nb-1):ss-3:12, ss-1};
  EndIf
  ss=newreg; Volume(ss) = {ss-1}; PhysFibV[iPhysFibV]=ss;iPhysFibV++;
EndFor


//fibers along z
fz0u=0;fz0d=0;fz2u=0;fz2d=0;
For f In {0:#tb[]-1}
  b=tb[f];
  c=tc[f];
  d=td[f];

  //first circle
  t=a*Cos(d);
  p=newp; Point(p) = {b+t-r, c, 0,lc};
  p=newp; Point(p) = {b+t+r, c, 0,lc};
  If ((c+r) > 2)
    l20[il20]=p;il20++;l20[il20]=p-1;il20++;
    p=newp; Point(p) = {b+t, c-r, 0,lc};
    p=newp; Point(p) = {b+t, c, 0};
    s=newreg; Line(s) = {p-3,p-2};
    s=newreg; Circle(s) = {p-2,p,p-1};
    s=newreg; Circle(s) = {p-1,p,p-3};
    ss=newreg; Line Loop(ss) = {s-2:s};
  EndIf
  If ((c-r) < 0)
    l00[il00]=p;il00++;l00[il00]=p-1;il00++;
    p=newp; Point(p) = {b+t, c+r, 0,lc};
    p=newp; Point(p) = {b+t, c, 0};
    s=newreg; Line(s) = {p-2,p-3};
    s=newreg; Circle(s) = {p-3,p,p-1};
    s=newreg; Circle(s) = {p-1,p,p-2};
    ss=newreg; Line Loop(ss) = {s-2:s};
  EndIf
  If (((c+r) < 2) && ((c-r) > 0))
    p=newp; Point(p) = {b+t, c+r, 0,lc};
    p=newp; Point(p) = {b+t, c-r, 0,lc};
    p=newp; Point(p) = {b+t, c, 0};
    s=newreg; Circle(s) = {p-1,p,p-4};
    s=newreg; Circle(s) = {p-4,p,p-2};
    s=newreg; Circle(s) = {p-2,p,p-3};
    s=newreg; Circle(s) = {p-3,p,p-1};
    ss=newreg; Line Loop(ss) = {s-3:s};
    If(b>1) 
      fz0u=ss;
    EndIf
    If(b<1) 
      fz0d=ss;
    EndIf
  EndIf
  ss=newreg; Plane Surface(ss) = {ss-1}; PhysZ0[iPhysZ0]=ss;iPhysZ0++;

  //fiber
  For i In {1:nb}
    For j In {1:nb2}
      z=(i-1)*2/nb+j*2/nb/nb2;
      x=b+a*Cos(3.1415*z+d);
      p=newp; Point(p) = {x-r, c, z,lc};
      p=newp; Point(p) = {x+r, c, z,lc};
      If((c+r) < 2)
        p=newp; Point(p) = {x, c+r, z,lc};
      EndIf
      If((c-r) > 0)
        p=newp; Point(p) = {x, c-r, z,lc};
      EndIf
    EndFor
    z=i*2/nb;
    x=b+a*Cos(3.1415*z+d);
    p=newp; Point(p) = {x, c, z};
    nbS=0;
    If((c+r) > 2)
      If (i==nb)
        l22[il22]=p-2;il22++;l22[il22]=p-3;il22++;
      EndIf
      ns = i==1 ? 2 : 6;
      s=newreg; Spline(s) = {p-4-3*nb2, p+0-3*nb2 : p-3 : 3};
      s=newreg; Spline(s) = {p-3-3*nb2, p+1-3*nb2 : p-2 : 3};
      s=newreg; Spline(s) = {p-2-3*nb2, p+2-3*nb2 : p-1 : 3};
      s=newreg; Line(s) = {p-3,p-2};
      s=newreg; Circle(s) = {p-2,p,p-1};
      s=newreg; Circle(s) = {p-1,p,p-3};
      ss=newreg; Line Loop(ss) = {s-8-ns,s-4,-(s-2),-(s-5)};
      ss=newreg; Plane Surface(ss) = {ss-1}; PhysY2[iPhysY2]=ss;iPhysY2++;
      ss=newreg; Line Loop(ss) = {s-7-ns,s-3,-(s-1),-(s-4)};
      ss=newreg; Ruled Surface(ss) = {ss-1}; vol[iv]=ss;iv++; PhysFibS[iPhysFibS]=ss;iPhysFibS++;
      ss=newreg; Line Loop(ss) = {s-6-ns,s-5,-(s-0),-(s-3)};
      ss=newreg; Ruled Surface(ss) = {ss-1}; vol[iv]=ss;iv++; PhysFibS[iPhysFibS]=ss;iPhysFibS++;
      nbS=2;
    EndIf
    If((c-r) < 0)
      If (i==nb)
        l02[il02]=p-2;il02++;l02[il02]=p-3;il02++;
      EndIf
      ns = i==1 ? 2 : 6;
      s=newreg; Spline(s) = {p-4-3*nb2, p+0-3*nb2 : p-3 : 3};
      s=newreg; Spline(s) = {p-3-3*nb2, p+1-3*nb2 : p-2 : 3};
      s=newreg; Spline(s) = {p-2-3*nb2, p+2-3*nb2 : p-1 : 3};
      s=newreg; Line(s) = {p-2,p-3};
      s=newreg; Circle(s) = {p-3,p,p-1};
      s=newreg; Circle(s) = {p-1,p,p-2};
      ss=newreg; Line Loop(ss) = {s-8-ns,s-5,-(s-2),-(s-4)};
      ss=newreg; Plane Surface(ss) = {ss-1}; PhysY0[iPhysY0]=ss;iPhysY0++;
      ss=newreg; Line Loop(ss) = {s-7-ns,s-3,-(s-1),-(s-5)};
      ss=newreg; Ruled Surface(ss) = {ss-1}; vol[iv]=ss;iv++; PhysFibS[iPhysFibS]=ss;iPhysFibS++;
      ss=newreg; Line Loop(ss) = {s-6-ns,s-4,-(s-0),-(s-3)};
      ss=newreg; Ruled Surface(ss) = {ss-1}; vol[iv]=ss;iv++; PhysFibS[iPhysFibS]=ss;iPhysFibS++;
      nbS=2;
    EndIf
    If (((c+r) < 2) && ((c-r) > 0))
      ns = i==1 ? 2 : 8;
      s=newreg; Spline(s) = {p-5-4*nb2, p-0-4*nb2 : p-4 : 4};
      s=newreg; Spline(s) = {p-4-4*nb2, p+1-4*nb2 : p-3 : 4};
      s=newreg; Spline(s) = {p-3-4*nb2, p+2-4*nb2 : p-2 : 4};
      s=newreg; Spline(s) = {p-2-4*nb2, p+3-4*nb2 : p-1 : 4};
      s=newreg; Circle(s) = {p-1,p,p-4};
      s=newreg; Circle(s) = {p-4,p,p-2};
      s=newreg; Circle(s) = {p-2,p,p-3};
      s=newreg; Circle(s) = {p-3,p,p-1};
      ss=newreg; Line Loop(ss) = {s-0,-(s-4),-(s-8-ns),s-6};
      ss=newreg; Ruled Surface(ss) = {ss-1}; vol[iv]=ss;iv++; PhysFibS[iPhysFibS]=ss;iPhysFibS++;
      ss=newreg; Line Loop(ss) = {s-1, -(s-6), -(s-9-ns), s-5};
      ss=newreg; Ruled Surface(ss) = {ss-1}; vol[iv]=ss;iv++; PhysFibS[iPhysFibS]=ss;iPhysFibS++;
      ss=newreg; Line Loop(ss) = {s-2, -(s-5), -(s-10-ns), s-7};
      ss=newreg; Ruled Surface(ss) = {ss-1}; vol[iv]=ss;iv++; PhysFibS[iPhysFibS]=ss;iPhysFibS++;
      ss=newreg; Line Loop(ss) = {s-3, -(s-7), -(s-11-ns), s-4};
      ss=newreg; Ruled Surface(ss) = {ss-1}; vol[iv]=ss;iv++; PhysFibS[iPhysFibS]=ss;iPhysFibS++;
      nbS=3;
    EndIf
  EndFor
  ss=newreg; Line Loop(ss) = {s-nbS:s};
  If(b>1) 
    fz2u=ss;
  EndIf
  If(b<1) 
    fz2d=ss;
  EndIf
  ss=newreg; Plane Surface(ss) = {ss-1}; PhysZ2[iPhysZ2]=ss;iPhysZ2++;
  If(((c+r) < 2) && ((c-r) > 0))
    ss=newreg; Surface Loop(ss) = {ss-3-16*nb, ss-9-16*(nb-1):ss-9:16, ss-7-16*(nb-1):ss-7:16, ss-5-16*(nb-1):ss-5:16, ss-3-16*(nb-1):ss-3:16, ss-1};
  EndIf
  If(((c+r) > 2) || ((c-r) < 0))
    ss=newreg; Surface Loop(ss) = {ss-3-12*nb, ss-7-12*(nb-1):ss-7:12, ss-5-12*(nb-1):ss-5:12, ss-3-12*(nb-1):ss-3:12, ss-1};
  EndIf
  ss=newreg; Volume(ss) = {ss-1}; PhysFibV[iPhysFibV]=ss;iPhysFibV++;
EndFor


// !matrix with 12 fibers!
s=newreg; Line(s) = {2,l00[0]};
s=newreg; Line(s) = {l00[1],l00[4]};
s=newreg; Line(s) = {l00[5],l00[2]};
s=newreg; Line(s) = {l00[3],l00[6]};
s=newreg; Line(s) = {l00[7],1};
s=newreg; Line(s) = {6,l02[0]};
s=newreg; Line(s) = {l02[1],l02[4]};
s=newreg; Line(s) = {l02[5],l02[2]};
s=newreg; Line(s) = {l02[3],l02[6]};
s=newreg; Line(s) = {l02[7],5};
s=newreg; Line(s) = {3,l20[0]};
s=newreg; Line(s) = {l20[1],l20[4]};
s=newreg; Line(s) = {l20[5],l20[2]};
s=newreg; Line(s) = {l20[3],l20[6]};
s=newreg; Line(s) = {l20[7],4};
s=newreg; Line(s) = {7,l22[0]};
s=newreg; Line(s) = {l22[1],l22[4]};
s=newreg; Line(s) = {l22[5],l22[2]};
s=newreg; Line(s) = {l22[3],l22[6]};
s=newreg; Line(s) = {l22[7],8};


//ss=newreg; Line Loop(ss) = {1, 2, 3, 4};//z=2
//ss=newreg; Plane Surface(ss) = {ss-1,fz2u,fz2d};
//ss=newreg; Line Loop(ss) = {6, 8, 10, 11};//z=0
//ss=newreg; Plane Surface(ss) = {ss-1,fz0u,fz0d};
//ss=newreg; Line Loop(ss) = {1, -9, 10, 12};//y=0
//ss=newreg; Plane Surface(ss) = {ss-1};
//ss=newreg; Line Loop(ss) = {6, 7, 3, 5};//y=2
//ss=newreg; Plane Surface(ss) = {ss-1};
ss=newreg; Line Loop(ss) = {2, -7, 8, 9};//x=2
ss=newreg; Plane Surface(ss) = {ss-1};
Physical Surface("plane5") = {ss};
ss=newreg; Line Loop(ss) = {4, -12, 11, -5};//x=0
ss=newreg; Plane Surface(ss) = {ss-1};
Physical Surface("plane6") = {ss};
//ss=newreg; Surface Loop(ss) = {ss-11:ss-1:2,vol[]};
//ss=newreg; Volume(ss) = {ss-1};

//with GUI

Line Loop(789) = {8, 765, 19, 31, 43, 55, -775};
Plane Surface(790) = {450, 789}; PhysZ0[iPhysZ0]=790;iPhysZ0++;
Line Loop(791) = {18, 30, 42, 54, 776, 521, 522, 777, -243, -231, -219, -207, -767, 390, 391, -766};
Plane Surface(792) = {638, 791}; PhysZ0[iPhysZ0]=792;iPhysZ0++;
Line Loop(793) = {206, 218, 230, 242, 778, 709, 710, 779, -11, -769, 578, 579, -768};
Plane Surface(794) = {793}; PhysZ0[iPhysZ0]=794;iPhysZ0++;
Line Loop(795) = {7, 780, 189, 190, 781, -562, -550, -538, -526, -776, 58, 59, -775};
Plane Surface(796) = {795}; PhysY2[iPhysY2]=796;iPhysY2++;
Line Loop(797) = {525, 537, 549, 561, 782, 377, 378, 783, -750, -738, -726, -714, -778, 246, 247, -777};
Plane Surface(798) = {140, 797}; PhysY2[iPhysY2]=798;iPhysY2++;
Line Loop(799) = {713, 725, 737, 749, 784, 5, -779};
Plane Surface(800) = {328, 799}; PhysY2[iPhysY2]=800;iPhysY2++;
Line Loop(801) = {2, 780, -186, -174, -162, -150, -770};
Plane Surface(802) = {516, 801}; PhysZ2[iPhysZ2]=802;iPhysZ2++;
Line Loop(803) = {185, 781, 565, 566, 782, -374, -362, -350, -338, -772, 434, 435, -771, 149, 161, 173};
Plane Surface(804) = {704, 803}; PhysZ2[iPhysZ2]=804;iPhysZ2++;
Line Loop(805) = {373, 783, 753, 754, 784, 4, -774, 622, 623, -773, 337, 349, 361};
Plane Surface(806) = {805}; PhysZ2[iPhysZ2]=806;iPhysZ2++;
Line Loop(807) = {9, 770, 145, 146, 771, -431, -419, -407, -395, -766, 14, 15, -765};
Plane Surface(808) = {807}; PhysY0[iPhysY0]=808;iPhysY0++;
Line Loop(809) = {430, 772, 333, 334, 773, -619, -607, -595, -583, -768, 202, 203, -767, 394, 406, 418};
Plane Surface(810) = {74, 809}; PhysY0[iPhysY0]=810;iPhysY0++;
Line Loop(811) = {618, 774, -12, -769, 582, 594, 606};
Plane Surface(812) = {262, 811}; PhysY0[iPhysY0]=812;iPhysY0++;
Physical Surface("plane3") = {PhysY0[]};
Physical Surface("plane4") = {PhysY2[]};
Physical Surface("plane2") = {PhysZ0[]};
Physical Surface("plane1") = {PhysZ2[]};
Physical Surface("fibers") = {PhysFibS[]};
Surface Loop(813) = {786, 802, 808, 804, 810, 792, 798, 794, 800, 788, 806, 812, 796, 790, 509, 515, 513, 511, 495, 493, 499, 497, 481, 479, 477, 483, 467, 465, 463, 461, 570, 558, 546, 534, 536, 548, 560, 572, 429, 417, 405, 403, 415, 427, 439, 441, 758, 746, 734, 722, 724, 736, 748, 760, 683, 699, 697, 703, 701, 685, 669, 667, 665, 681, 687, 671, 655, 653, 651, 649, 617, 605, 593, 591, 603, 615, 627, 629, 158, 160, 172, 184, 196, 194, 182, 170, 101, 117, 133, 139, 137, 135, 119, 103, 87, 85, 91, 107, 123, 121, 105, 89, 41, 29, 27, 39, 51, 53, 65, 63, 358, 346, 348, 360, 372, 384, 382, 370, 273, 289, 305, 321, 327, 325, 323, 307, 291, 275, 277, 293, 309, 311, 295, 279, 227, 215, 217, 229, 241, 253, 251, 239};
Volume(814) = {813};
Physical Volume("vol_in") = {PhysFibV[]};
Physical Volume("vol_out") = {814};

Mesh.CharacteristicLengthFactor=0.4;
Mesh 3;
Print "3Dwoven.msh";
