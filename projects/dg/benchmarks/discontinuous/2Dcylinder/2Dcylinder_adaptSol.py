
from dgpy import *
from Incompressible import *
from Common import *
import os
import time
import math
import sys

try : os.mkdir("output");
except: 0;

os.system("rm output/*")
os.system("rm LiftAndDrag.dat")

TIME = function.getTime()

print'       '
print'*-------------------dgCode-------------------*'
print'|            |                               |'
print'| 2DCylinder |        CASE PARAMETERS        |'
print'|            |                               |'
print'*--------------------------------------------*'

print'---- Setting parameters'
RHO = 1.0
MU  = 1.0
Uref = 40
R = 0.5
Re = RHO*Uref*2*R/MU

print'       '
print'*-------------------dgCode-------------------*'
print'|            |                               |'
print'| 2DCylinder |    CASE SPECIFIC FUNCTIONS    |'
print'|            |                               |'
print'*--------------------------------------------*'

print'---- Defining specific functions'

def initF(FCT, XYZ):
 	for i in range(0,FCT.size1()):
		FCT.set(i,0, Uref) #u
		FCT.set(i,1, 0.0) #v
		FCT.set(i,2, 0.0) #p

def InletBC(FCT, XYZ):
	for i in range(0,FCT.size1()):  
		FCT.set(i,0, Uref) #u
		FCT.set(i,1, 0.0) #v
		FCT.set(i,2, 0.0) #p
		FCT.set(i,3, fixed)
		FCT.set(i,4, fixed)
		FCT.set(i,5, free)

def WallBC_2D(FCT, XYZ):
	for i in range(0,FCT.size1()):
		FCT.set(i,0, 0.0) 
		FCT.set(i,1, 0.0) 
		FCT.set(i,2, 0.0)
		FCT.set(i,3, fixed)
		FCT.set(i,4, fixed)
		FCT.set(i,5, free)

def PZero_2D(FCT, XYZ ):
	for i in range(0,FCT.size1()):
		FCT.set(i,0, 0.0)
		FCT.set(i,1, 0.0)
		FCT.set(i,2, 0.0)
		FCT.set(i,3, free)
		FCT.set(i,4, free)
		FCT.set(i,5, fixed)

def VelX_2D(FCT, XYZ):
	for i in range(0,FCT.size1()):
		FCT.set(i,0, 0.0)
		FCT.set(i,1, 0.0)
		FCT.set(i,2, 0.0)
		FCT.set(i,3, free)
		FCT.set(i,4, fixed)
		FCT.set(i,5, free)

print'       '
print'*--------------------dgCode--------------------*'
print'|            |                                 |'
print'| 2DCylinder |   INITIALIZE LEVELSET PROBLEM   |'
print'|            |                                 |'
print'*----------------------------------------------*'

print '---- Defining Solver for Navier-Stokes problems'
def SolveNS(meshName):
	rhoF = functionConstant(RHO)
	muF = functionConstant(MU)
	ns = Incompressible(meshName, 2)
	ns.initializeIncomp(initF, rhoF, muF, Uref)

	VEL = functionPython(6, InletBC, [ns.XYZ])
	ns.strongBoundaryConditionLine('inlet', VEL)
	ns.weakPresBoundaryCondition('outlet', ns.PZERO)
	ns.strongBoundaryConditionLine('top', ns.VELX)
	ns.strongBoundaryConditionLine('bottom', ns.VELX)
	ns.strongBoundaryConditionLine('levelset_L1', ns.WALL)

	petscOptions = "-ksp_rtol 1.e-3 -pc_type lu -pc_factor_levels 3"
	ns.pseudoTimeSteppingSolve(1, 20, 5, 1.e-5, 1.e-8, 10, petscOptions)
	
	flowDir=[1.0, 0.0, 0.0]
	spanDir=[0.0, 1.0, 0.0]
	fScale = 0.5*RHO*Uref*Uref*1.0
	ns.computeDragAndLift('levelset_L1', 0, 0, fScale, flowDir, spanDir, 'LiftAndDrag.dat')

	return ns

print'       '
print'*-------------------dgCode-------------------*'
print'|            |                               |'
print'| 2DCylinder |      GEOMETRY ADAPTIVITY      |'
print'|            |                               |'
print'*--------------------------------------------*'

dim = 2
order = 1
caseName  = "domain_cylinder"

print'---- DEFINE LEVELSET ON MESH'
sphere = gLevelsetMathEval("sqrt(x*x+y*y)-0.5")
myLS = sphere

print'---- LOADING GEOMETRY'		
mesh = GModel()
mesh.load(caseName+geo)
print'---- ADAPT MESH WITH LEVELSET'
mesh.adaptMesh([5], [myLS], [[1, 0.0001, 0.5]], 25, True) 
print'---- SPLITTING MESH'
model = GModel()
model = mesh.buildCutGModel(myLS, False, True)
name = "2Dcylinder_geo"
model.save(name+msh)
NAME = "2DCYLINDER_GEO"
adapt = GModel()
adapt.load(name+msh)
adapt.save(NAME+msh)
meshName = NAME

print'       '
print'*-------------------dgCode-------------------*'
print'|            |                               |'
print'| 2DCylinder |      SOLUTION ADAPTIVITY      |'
print'|            |                               |'
print'*--------------------------------------------*'

print '---- solving problem on initial mesh ...'
ns1 = SolveNS(meshName)

print '---- do 1st solution adaptivity ...'
NORMVEL1 = functionPython(1, NormVEL_2D, [ns1.solution.getFunction()])
myEval1 = dgFunctionEvaluator(ns1.groups, NORMVEL1)
mesh1 = GModel()
mesh1.load(caseName+geo)
mesh1.adaptMesh([2,5], [myEval1, myLS], [[10000, 0, 1e10], [1, 0.0001, 0.5]], 25, True)
adapt1 = mesh1.buildCutGModel(myLS, False, True)
meshName1 = "2DCYLINDER_SOL1"
adapt1.save(meshName1+msh)

print '---- solving problem on second mesh ...'
ns2 = SolveNS(meshName1)

print '---- do 2nd solution adaptivity ...'
NORMVEL2 = functionPython(1, NormVEL_2D, [ns2.solution.getFunction()])
myEval2 = dgFunctionEvaluator(ns2.groups, NORMVEL2)
mesh2 = GModel()
mesh2.load(caseName+geo)
mesh2.adaptMesh([2,5], [myEval1, myLS], [[20000, 0, 1e10], [1, 0.0001, 0.5]], 25, True)
adapt2 = mesh2.buildCutGModel(myLS, False, True)
meshName2 = "2DCYLINDER_SOL2"
adapt2.save(meshName2+msh)

print '---- solving problem on third mesh ...'
ns3 = SolveNS(meshName2)

print '---- do 3rd solution adaptivity ...'
NORMVEL3 = functionPython(1, NormVEL_2D, [ns2.solution.getFunction()])
myEval3 = dgFunctionEvaluator(ns3.groups, NORMVEL2)
mesh3 = GModel()
mesh3.load(caseName+geo)
mesh3.adaptMesh([2,5], [myEval3, myLS], [[30000, 0, 1e10], [1, 0.0001, 0.5]], 25, True)
adapt3 = mesh2.buildCutGModel(myLS, False, True)
meshName3 = "2DCYLINDER_SOL3"
adapt3.save(meshName3+msh)

#exit(0)

print'       '
print'*-------------------dgCode-------------------*'
print'|            |                               |'
print'| 2DCylinder |   INITIALIZATION OF PROBLEM   |'
print'|            |                               |'
print'*--------------------------------------------*'

print'---- BUILD GROUPS'
#meshName3 = "2DCYLINDER_SOL3"
model = GModel()
model.load(meshName3+msh)
groups = dgGroupCollection.newByTag(model, dim, order, ["domain_out"])		
groups.splitGroupsByPhysicalTag()
XYZ = function.getCoordinates()

print'---- LOAD CONSERVATION LAW'
rhoF = functionConstant(RHO)
muF = functionConstant(MU)
law = dgConservationLawNavierStokesIncomp2d(rhoF, muF, Uref)

solution = dgDofContainer(groups, law.getNbFields())
solutionPREV = dgDofContainer(groups, law.getNbFields())
law.setPreviousSolution(solutionPREV.getFunction())
law.setPreviousSolutionGradient(solutionPREV.getFunctionGradient())

print'---- INITIAL CONDITION'
INIT = functionPython(dim+1, initF, [XYZ])
solution.interpolate(INIT)
solution.exportMsh('output/initNS', 0, 0)

print'---- BOUNDARY CONDITIONS'
VEL = functionPython(6, InletBC, [XYZ])
WALL = functionPython(6, WallBC_2D, [XYZ])
VELX = functionPython(6, VelX_2D, [XYZ])
PZERO = functionPython(6, PZero_2D, [XYZ])

law.addBoundaryCondition('inlet', law.newOutsideValueBoundary("",VEL))
law.addStrongBoundaryCondition(1, 'inlet', VEL)

law.addBoundaryCondition('top', law.newOutsideValueBoundary("",VELX))
law.addStrongBoundaryCondition(1, 'top', VELX)

law.addBoundaryCondition('bottom', law.newOutsideValueBoundary("",VELX))
law.addStrongBoundaryCondition(1, 'bottom', VELX)

law.addBoundaryCondition('levelset_L1', law.newOutsideValueBoundary("",WALL))
law.addStrongBoundaryCondition(1, 'levelset_L1', WALL)

law.addBoundaryCondition('outlet', law.newBoundaryPressure(PZERO))

print'       '
print'*-------------------dgCode-------------------*'
print'|            |                               |'
print'| 2DCylinder |        SOLVING PROBLEM        |'
print'|            |                               |'
print'*--------------------------------------------*'

print'---- Solver Definition'
nbTimeSteps = 20 
dt0 = 0.1
ATol = 1.e-8
RTol = 1.e-5
Verb = 2
solverOptions ="-ksp_rtol 1.e-3 -pc_type lu -pc_factor_levels 0"
timeOrder = 1

petsc =  linearSystemPETScDouble()
petsc.setParameter("petscOptions", solverOptions)
dof = dgDofManager.newCG (groups, law.getNbFields(), petsc)

print'---- Solving...'
print'---- Computing steady solution with pseudo time steps %d ---'  % nbTimeSteps
t = 0.0
dt = dt0
implicitEuler = dgDIRK(law, dof, timeOrder)
implicitEuler.getNewton().setAtol(ATol)
implicitEuler.getNewton().setRtol(RTol)
implicitEuler.getNewton().setVerb(Verb)

rinit = 1.0
for i in range (1,nbTimeSteps+1):
			
	t = t+dt
	print'|ITER|',i, '|DT|',dt,'|T(sec)|',t
	solutionPREV.copy(solution)
				
	groups.buildStreamwiseLength(law.getVelocity(), solution) 

      	implicitEuler.iterate(solution, dt, t)
      	res = implicitEuler.getNewton().getFirstResidual()

	outputstr = "output/ns%06d" % i
	solution.exportFunctionMsh(law.getPressure(), outputstr, t, i, 'pres')
	solution.exportFunctionMsh(law.getVelocity(), outputstr, t, i, 'vel')
	#solution.exportFunctionMsh(law.getVelocityX(), outputstr, t, i, 'velX')
		  
	if (i == 2) :
		rinit = res
	if (i > 2) :
		print 'CONVERGENCE : ' , (res/rinit)
		dt = dt0 * math.pow(rinit/res,1.5)
		if (dt > 60000) : 
			dt = 60000
		if (res/rinit < 1.e-8) : 
			break 
	
print'       '
print'*-------------------dgCode-------------------*'
print'|            |                               |'
print'| 2DCylinder |         POSTPROCESSING        |'
print'|            |                               |'
print'*--------------------------------------------*'

print "---- Computing total force ----"
fileName = 'LiftAndDrag.dat'
itime = 0
iiter = 0
fun = open(fileName,"a")
surface = 'levelset_L1'
flowDir=[1.0, 0.0, 0.0]
spanDir=[0.0, 1.0, 0.0]
fScale = 0.5*RHO*Uref*Uref*(2*R)
if (iiter == 0):
	fun.write("#(1)iter (2)time (3)lift (4)drag \n")
lift = 0.0
drag = 0.0
	  
integ=dgFunctionIntegratorInterface(groups, law.getTotalForce(), solution)
force=fullMatrixDouble(3,1)
integ.compute(surface, force)

print "---- Projecting total forces onto lift and drag directions----"			
for j in range(0,2):
	drag = drag + force.get(j,0)*flowDir[j]
	lift = lift + force.get(j,0)*spanDir[j]

print "---- Computing lift and drag forces ----"	   		
drag = drag/fScale
lift = lift/fScale

print "---- Saving results in file ----"
print >>fun, ' ', iiter,' ',itime,' ',lift,' ',drag
		
fun.close()

print "---- Reattachment length ----"
evalsol = dgFunctionEvaluator(groups, solution.getFunction())
y = 0
z = 0
for i in range (250):
	x = 0.45 + i*0.01
	result = fullMatrixDouble(3,1) 
	evalsol.compute(x,y,z,result)
	ux = result.get(0,0) 
	if (ux < 0):
		print 'x =', x, 'and ux =', ux

print'       '
print'*-------------------dgCode-------------------*'
print'|            |                               |'
print'| 2DCylinder |      SUMMARY FOR RESULTS      |'
print'|            |                               |'
print'*--------------------------------------------*'

print "Flow over 2D circular cyliner at Reynold number =", Re
print "Cl = %f Cd = %f" %(lift, drag)
