#01May2012
#Dolbow3D: cube + sphere; Laplace equation
from dgpy import *
from Common import *
import math 
import os
import time
	
try : os.mkdir("output");
except: 0;

os.system("rm output/*")

print'       '
print'*--------------------Dolbow3D-------------------*'
print'|    UCL   |                                    |'
print'|   iMMC   |  DEFINING CASE SPECIFIC FUNCTIONS  |'
print'|  dgCode  |                                    |'
print'*-----------------------------------------------*'

#interface
def lsBC(FCT, XYZ):
        for i in range(0,XYZ.size1()):
		x = XYZ.get(i,0)
                y = XYZ.get(i,1)
		z = XYZ.get(i,2) #-> z should be calculated relating to x&y such that phi(x,y,z)=0!!!
		r_square = 0.3**2-(x-0.5)**2-(y-0.5)**2
		if (r_square<0):
			r_abs = sqrt(-r_square)
		else:
			r_abs = sqrt(r_square)
		if (z<0.5):
			z0 = -r_abs+0.5
		else:
			z0 = r_abs+0.5
		FCT.set(i,0, sin(pi*x)*(cosh(pi*z0)-((exp(2*pi)+1)/(exp(2*pi)-1))*sinh(pi*z0)))

#plane2:z=0
def bc1(FCT, XYZ):
	for i in range(0,FCT.size1()):
		x = XYZ.get(i,0)
		z = XYZ.get(i,2)
		FCT.set(i,0, sin(pi*x))

#plane3:y=0 & plane4:y=1
def bc2(FCT, XYZ):
	for i in range(0,FCT.size1()):
		x = XYZ.get(i,0)
		z = XYZ.get(i,2)
		FCT.set(i,0, sin(pi*x)*(cosh(pi*z)-((exp(2*pi)+1)/(exp(2*pi)-1))*sinh(pi*z)))

def source(FCT, XYZ):
	for i in range(0,FCT.size1()):
		x = XYZ.get(i,0)
		y = XYZ.get(i,1)
		z = XYZ.get(i,2)
		FCT.set(i,0, x**3+y**3+z**3)

def exactSL(FCT, XYZ):
	for i in range(0,FCT.size1()):
		x = XYZ.get(i,0)
		z = XYZ.get(i,2)
		FCT.set(i,0, sin(pi*x)*(cosh(pi*z)-((exp(2*pi)+1)/(exp(2*pi)-1))*sinh(pi*z)))	

def anaSol(FCT, XYZ):
	for i in range(0,FCT.size1()):
		x = XYZ.get(i,0)
		z = XYZ.get(i,2)
		Tana = sin(pi*x)*(cosh(pi*z)-((exp(2*pi)+1)/(exp(2*pi)-1))*sinh(pi*z))
		FCT.set(i,0, Tana*Tana)
def error(FCT, F1, F2):
	for i in range(0,F1.size1()):
		FCT.set(i, 0, (F1(i,0)-F2(i,0))**2);

print'       '
print'*-----------------dgCode-----------------*'
print'|          |                             |'
print'| Dolbow3D |  GENERATE ANISOTROPIC MESH  |'
print'|          |                             |'
print'*----------------------------------------*'
fine = 4 #fine-range = [1:4]

caseName  = "Dolbow3D"

print'---- DEFINE LEVELSET ON MESH'
ls = gLevelsetMathEval("(x-0.5)*(x-0.5)+(y-0.5)*(y-0.5)+(z-0.5)*(z-0.5)-0.09")
myLS = ls
'''
print'---- LOADING GEOMETRY'		
mesh = GModel()
mesh.load(caseName+geo)
print'---- ADAPT MESH WITH LEVELSET'
if fine == 1: #lc = lcmax = 0.2; [E, lcmin, lcmax]; [0.0175073725757]; [2436]
	mesh.adaptMesh([5], [myLS], [[0.06, 0.045, 0.2]], 25, True)
	print'---- SLITTING MESH'
	model = GModel()
	model = mesh.buildCutGModel(myLS, False, True)
	name = "adapt1"
	model.save(name+msh)
	NAME = "ADAPT1"
	adapt = GModel()
	adapt.load(name+msh)
	adapt.save(NAME+msh)
	meshName = NAME
elif fine == 2: #lc = lcmax = 0.05; [E/2, lcmin/5, lcmax/2]; [0.00646515662927]; [14388]
	mesh.adaptMesh([5], [myLS], [[0.06, 0.006, 0.1]], 25, True)
	print'---- SPLITTING MESH'
	model = GModel()
	model = mesh.buildCutGModel(myLS, False, True)
	name = "adapt2"
	model.save(name+msh)
	NAME = "ADAPT2"
	adapt = GModel()
	adapt.load(name+msh)
	adapt.save(NAME+msh)
	meshName = NAME
elif fine == 3: #lc = lcmax = 0.025; [E/4, lcmin/25, lcmax/4]; [0.00147216539925]; [108795]
	mesh.adaptMesh([5], [myLS], [[0.035, 0.0009, 0.05]], 25, True)
	print'---- SLITTING MESH'
	model = GModel()
	model = mesh.buildCutGModel(myLS, False, True)
	name = "adapt3"
	model.save(name+msh)
	NAME = "ADAPT3"
	adapt = GModel()
	adapt.load(name+msh)
	adapt.save(NAME+msh)
	meshName = NAME
elif fine == 4: #lc = lcmax = 0.0125; [E/8, lcmin/125, lcmax/8]; [0.000301948447216]; [909249]
	mesh.adaptMesh([5], [myLS], [[0.03, 0.00015, 0.025]], 25, True)
	print'---- SLITTING MESH'
	model = GModel()
	model = mesh.buildCutGModel(myLS, False, True)
	name = "adapt4"
	model.save(name+msh)
	NAME = "ADAPT4"
	adapt = GModel()
	adapt.load(name+msh)
	adapt.save(NAME+msh)
	meshName = NAME
'''
print'       '
print'*-------------------dgCode--------------------*'
print'|             |                               |'
print'|  Dolbow3D   |   INITIALIZATION OF PROBLEM   |'
print'|             |                               |'
print'*---------------------------------------------*'

print'---- BUILD GROUPS'
meshName = "ADAPT3"
MODEL = GModel()
MODEL.load(meshName+msh)
groups = dgGroupCollection.newByTag(MODEL, 3, 1, ["vol_out"])
groups.splitGroupsByPhysicalTag()
XYZ = groups.getFunctionCoordinates()

print'---- LOAD CONSERVATION LAW'
kappa = functionConstant(1)
SOURCE = functionPython(1, source, [XYZ])
law = dgConservationLawAdvectionDiffusionSUPG.diffusionLaw(kappa)
law.setSource(SOURCE)

print'---- INITIAL CONDITION'
INIT = functionConstant(0)
solution = dgDofContainer(groups,law.getNbFields()) 
solution.setAll(0)
	
print'---- BOUNDARY CONDITIONS'
BC0 = functionConstant(0)
BC1 = functionPython(1, bc1, [XYZ])
BC2 = functionPython(1, bc2, [XYZ])
LSBC = functionPython(1, lsBC, [XYZ])

law.addBoundaryCondition('plane1', law.newOutsideValueBoundary("", BC0)) #z=1
law.addBoundaryCondition('plane2', law.newOutsideValueBoundary("", BC1)) #z=0
law.addBoundaryCondition('plane3', law.newOutsideValueBoundary("", BC2)) #y=0
law.addBoundaryCondition('plane4', law.newOutsideValueBoundary("", BC2)) #y=1
law.addBoundaryCondition('plane5', law.newOutsideValueBoundary("", BC0)) #x=1
law.addBoundaryCondition('plane6', law.newOutsideValueBoundary("", BC0)) #x=0
law.addBoundaryCondition('levelset_S1', law.newOutsideValueBoundary("", LSBC)) #interface

law.addStrongBoundaryCondition(2, 'plane1', BC0)
law.addStrongBoundaryCondition(2, 'plane2', BC1)
law.addStrongBoundaryCondition(2, 'plane3', BC2)
law.addStrongBoundaryCondition(2, 'plane4', BC2)
law.addStrongBoundaryCondition(2, 'plane5', BC0)
law.addStrongBoundaryCondition(2, 'plane6', BC0)
law.addStrongBoundaryCondition(2, 'levelset_S1', LSBC)

print'     '
print'*--------------dgCode---------------*'
print'|             |                     |'
print'|  Dolbow3D   |   SOLVING PROBLEM   |'
print'|             |                     |'
print'*-----------------------------------*'

print'---- Solver Definition'
dof = None
petsc =  linearSystemPETScDouble()
dof = dgDofManager.newCG(groups, 1, petsc)
petsc.setParameter("petscOptions", "-pc_type lu")
petsc.setParameter("matrix_reuse", "same_matrix")

print'---- Solving...'
maxIter = 10000
steady = dgSteady(law, dof)
steady.getNewton().setVerb(2)
steady.getNewton().setRtol(1e-15)
steady.getNewton().setMaxIt(maxIter)
steady.solve(solution)
solution.exportMsh("output/solution", 0, 0)
	
print'       '
print'*--------------dgCode--------------*'
print'|             |                    |'
print'|  Dolbow3D   |   POSTPROCESSING   |'
print'|             |                    |'
print'*----------------------------------*'

print'     results coming soon ... :-)'
print'---- Calculating result at given point'
X = 0.5
Y = 0.5
Z = 0.1
eval1 = dgFunctionEvaluator(groups, solution.getFunction())
result = fullMatrixDouble(1,1)
eval1.compute(X,Y,Z,result)
TNUM = result.get(0,0)
TANA = sin(pi*X)*(cosh(pi*Z)-((exp(2*pi)+1)/(exp(2*pi)-1))*sinh(pi*Z))

print'---- Calculating error'
fana = functionPython(1, exactSL, [XYZ])
ana = dgDofContainer(groups, 1)
ana.interpolate(fana)
ana.exportMsh("output/exact_solution", 0, 0)

print'     integrating ...'
fanaSol = functionPython(1, anaSol, [XYZ]) #anaSol=exactSL^2
integrator1 = dgFunctionIntegrator(groups, fanaSol, solution)
matrix1 = fullMatrixDouble(1,1)
integrator1.compute(matrix1, "vol_out")
result1 = matrix1.get(0,0)

ANA = ana.getFunction()
NUM = solution.getFunction()
ferror = functionPython(1, error, [ANA, NUM]);
integrator2 = dgFunctionIntegrator(groups, ferror) 
matrix2 = fullMatrixDouble(1,1)
integrator2.compute(matrix2, "vol_out")
result2 = matrix2.get(0,0)

ERROR1 = sqrt(result2) #absolute error
#ERROR1 = sqrt(result2/result1) #relative error

print'     exporting error ...'
error = dgDofContainer(groups, 1)
error.interpolate(ferror)
error.exportMsh("output/error", 0, 0)

print'       '
print'*---------------dgCode---------------*'
print'|          |                         |'
print'| Dolbow3D |   SUMMARY FOR RESULTS   |'
print'|          |                         |'
print'*------------------------------------*'

print "1. ERROR AT ONE POINT: (x=%g,y=%g,z=%g)" % (X,Y,Z)
print "   Analytical result =", TANA
print "   Numerical  result =", TNUM

print "2. ERROR IN THE BULK FIELD:", 'fine =', fine, ERROR1
