#09May2012
from dgpy import *
from Common import *
import os
import time
import math

TIME = function.getTime()
try : os.mkdir("output");
except: 0;
os.system("rm output/*")

print'       '
print'*--------------------Donut3D--------------------*'
print'|    UCL   |                                    |'
print'|   iMMC   |  DEFINING CASE SPECIFIC FUNCTIONS  |'
print'|  dgCode  |                                    |'
print'*-----------------------------------------------*'

def exactSL(FCT, XYZ):
	for i in range(0,FCT.size1()):
		x = XYZ.get(i,0)
		y = XYZ.get(i,1)
		z = XYZ.get(i,2)
		r = sqrt((x-0.5)**2+(y-0.5)**2+(z-0.5)**2)
		FCT.set(i,0, log(r))

def bc(FCT, XYZ):
	for i in range(0,FCT.size1()):
		x = XYZ.get(i,0)
		y = XYZ.get(i,1)
		z = XYZ.get(i,2)
		r = sqrt((x-0.5)**2+(y-0.5)**2+(z-0.5)**2)
		FCT.set(i,0, log(r))

def LSbc(FCT, XYZ):
	for i in range(0,FCT.size1()):
		x = XYZ.get(i,0)
		y = XYZ.get(i,1)
		z = XYZ.get(i,2)
		Rxy = sqrt((x-0.5)**2+(y-0.5)**2)
		alpha = atan(sqrt((z-0.5)**2/(0.2-Rxy)**2))
		if (Rxy<0.2):
			R0 = sqrt(0.05-0.04*sqrt(cos(alpha)*cos(alpha)))
		else:
			R0 = sqrt(0.05+0.04*sqrt(cos(alpha)*cos(alpha)))
		FCT.set(i,0, log(R0))

def source(FCT, XYZ):
	for i in range(0,FCT.size1()):
		x = XYZ.get(i,0)
		y = XYZ.get(i,1)
		z = XYZ.get(i,2)
		r = sqrt((x-0.5)**2+(y-0.5)**2+(z-0.5)**2)
		FCT.set(i,0, -1/(r*r))

print'       '
print'*-------------------dgCode------------------*'
print'|             |                             |'
print'|   Donut3D   |  GENERATE ANISOTROPIC MESH  |'
print'|             |                             |'
print'*-------------------------------------------*'

caseName  = "cube"

print'---- DEFINE LEVELSET ON MESH'
ls = gLevelsetMathEval("(sqrt((x-0.5)*(x-0.5)+(y-0.5)*(y-0.5))-0.2)*(sqrt((x-0.5)*(x-0.5)+(y-0.5)*(y-0.5))-0.2)+(z-0.5)*(z-0.5)-0.01")
myLS = ls

print'---- LOADING GEOMETRY'
mesh = GModel()
mesh.load(caseName+geo)

print'---- GENERATING ANISOTROPIC MESH'
mesh.adaptMesh([4], [myLS], [[0.003, 0.00008, 0.05]], 25, True)

print'---- SPLITTING MESH'
model = GModel()
model = mesh.buildCutGModel(myLS, False, True)
name = "donut"
model.save(name+msh)
NAME = "DONUT"
adapt = GModel()
adapt.load(name+msh)
adapt.save(NAME+msh)
meshName = NAME

print'       '
print'*-------------------dgCode--------------------*'
print'|             |                               |'
print'|   Donut3D   |   INITIALIZATION OF PROBLEM   |'
print'|             |                               |'
print'*---------------------------------------------*'

print'---- BUILD GROUPS'
MODEL = GModel()
MODEL.load(meshName+msh)
groups = dgGroupCollection.newByTag(MODEL, 3, 1, ["vol_out"])
groups.splitGroupsByPhysicalTag()
XYZ = groups.getFunctionCoordinates()

print'---- LOAD CONSERVATION LAW'
kappa = functionConstant(1)
SOURCE = functionPython(1, source, [XYZ])
law = dgConservationLawAdvectionDiffusionSUPG.diffusionLaw(kappa)
law.setSource(SOURCE)

print'---- INITIAL CONDITION'
INIT = functionConstant(0)
solution = dgDofContainer(groups,law.getNbFields()) 
solution.interpolate(INIT) 
	
print'---- BOUNDARY CONDITIONS'
BC = functionPython(1, bc, [XYZ])
LSBC = functionPython(1, LSbc, [XYZ])	
law.addBoundaryCondition('plane1', law.newOutsideValueBoundary("",BC))
law.addBoundaryCondition('plane2', law.newOutsideValueBoundary("",BC)) 
law.addBoundaryCondition('plane3', law.newOutsideValueBoundary("",BC))
law.addBoundaryCondition('plane4', law.newOutsideValueBoundary("",BC))
law.addBoundaryCondition('plane5', law.newOutsideValueBoundary("",BC))
law.addBoundaryCondition('plane6', law.newOutsideValueBoundary("",BC))
law.addBoundaryCondition('levelset_S1', law.newOutsideValueBoundary("",LSBC))

law.addStrongBoundaryCondition(2, 'plane1',BC)
law.addStrongBoundaryCondition(2, 'plane2',BC)
law.addStrongBoundaryCondition(2, 'plane3',BC)
law.addStrongBoundaryCondition(2, 'plane4',BC)
law.addStrongBoundaryCondition(2, 'plane5',BC)
law.addStrongBoundaryCondition(2, 'plane6',BC)
law.addStrongBoundaryCondition(2, 'levelset_S1',LSBC)

print'     '
print'*--------------dgCode---------------*'
print'|             |                     |'
print'|   Donut3D   |   SOLVING PROBLEM   |'
print'|             |                     |'
print'*-----------------------------------*'

print'---- Solver Definition'
dof = None
petsc =  linearSystemPETScDouble()
dof = dgDofManager.newCG(groups, 1, petsc)
petsc.setParameter("petscOptions", "-pc_type lu")
petsc.setParameter("matrix_reuse", "same_matrix")

print'---- Solving...'
maxIter = 10000
steady = dgSteady(law, dof)
steady.getNewton().setVerb(10)
steady.getNewton().setRtol(1e-15)
#steady.getNewton().setMaxIt(maxIter)
steady.solve(solution)
solution.exportMsh("output/solution", 0, 0)
	
print'       '
print'*--------------dgCode--------------*'
print'|             |                    |'
print'|   Donut3D   |   POSTPROCESSING   |'
print'|             |                    |'
print'*----------------------------------*'

print'     results coming soon ... :-)'
print'---- Calculating result at given point'
X = 0.595
Y = 0.5
Z = 0.5
eval1 = dgFunctionEvaluator(groups, solution.getFunction())
result = fullMatrixDouble(1,1)
eval1.compute(X,Y,Z,result)
TNUM = result.get(0,0)
TANA = log(sqrt((X-0.5)**2+(Y-0.5)**2+(Z-0.5)**2))

print'---- Calculating error'
def error(FCT, F1, XYZ):
        for i in range(0,XYZ.size1()):
		x = XYZ.get(i,0) 
                y = XYZ.get(i,1)
		z = XYZ.get(i,2)
		r = sqrt((x-0.5)**2+(y-0.5)**2+(z-0.5)**2)
		Tana = log(r)
		Tnum = F1.get(i,0)
		FCT.set(i,0, (Tnum-Tana)*(Tnum-Tana))

def ana(FCT, XYZ):
        for i in range(0,XYZ.size1()):
		x = XYZ.get(i,0) 
                y = XYZ.get(i,1)
		z = XYZ.get(i,2)
		r = sqrt((x-0.5)**2+(y-0.5)**2+(z-0.5)**2)
		Tana = log(r)
		FCT.set(i,0, Tana*Tana)

ferror = functionPython(1, error, [solution.getFunction(), XYZ]);
fana = functionPython(1, ana, [XYZ]);

print'     volume integrating ...'
integrator1 = dgFunctionIntegrator(groups, ferror, solution)
matrix1 = fullMatrixDouble(1,1)
integrator1.compute(matrix1, "vol_out")
result1 = matrix1.get(0,0)

integrator2 = dgFunctionIntegrator(groups, fana, solution)
matrix2 = fullMatrixDouble(1,1)
integrator2.compute(matrix2, "vol_out")
result2 = matrix2.get(0,0) 

ERROR1_rel = sqrt(result1/result2)

print'     exporting error ...'
error = dgDofContainer(groups, 1)
error.interpolate(ferror)
error.exportMsh("output/error", 0, 0)

print "1. ERROR AT ONE POINT: (x=%g,y=%g,z=%g)" % (X,Y,Z)
print "   Analytical result =", TANA
print "   Numerical  result =", TNUM

print "2. ERROR IN THE BULK FIELD:"
print'    Relative error:', ERROR1_rel
