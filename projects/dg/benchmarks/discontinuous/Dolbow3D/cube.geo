
lc = 0.2;

Point(1) = {0,0,0,lc};         
Point(2) = {1,0,0.0,lc};         
Point(3) = {1,1,0,lc};         
Point(4) = {0,1,0,lc}; 

Point(5) = {0,0,1,lc};         
Point(6) = {1,0,1,lc};         
Point(7) = {1,1,1,lc};         
Point(8) = {0,1,1,lc}; 
              
Line(1) = {5, 6};
Line(2) = {6, 7};
Line(3) = {7, 8};
Line(4) = {8, 5};
Line(5) = {8, 4};
Line(6) = {4, 3};
Line(7) = {3, 7};
Line(8) = {3, 2};
Line(9) = {2, 6};
Line(10) = {2, 1};
Line(11) = {1, 4};
Line(12) = {1, 5};

Line Loop(13) = {1, 2, 3, 4};
Plane Surface(14) = {13};
Line Loop(15) = {6, 8, 10, 11};
Plane Surface(16) = {15};
Line Loop(17) = {1, -9, 10, 12};
Plane Surface(18) = {17};
Line Loop(19) = {6, 7, 3, 5};
Plane Surface(20) = {19};
Line Loop(21) = {2, -7, 8, 9};
Plane Surface(22) = {21};
Line Loop(23) = {4, -12, 11, -5};
Plane Surface(24) = {23};
Surface Loop(25) = {14, 18, 22, 20, 16, 24};
Volume(26) = {25};

Physical Surface("plane1") = {14};
Physical Surface("plane2") = {16};
Physical Surface("plane3") = {18};
Physical Surface("plane4") = {20};
Physical Surface("plane5") = {22};
Physical Surface("plane6") = {24};

Physical Volume("vol") = {26};
