# -*- coding: utf-8 -*-

# Import for python
from dgpy import *
from gmshpy import *
from math import *
import sys

def smoothingBath(groups,bathDC,physicalName,bNu,fT):
  bathDC.exportMsh('bathIn',0,0)
  print '     --> Dynamic Viscosity =', bNu 
  print '     --> Duration =', fT, ' sec'
  nuB = functionConstant(bNu)
  dlaw = dgConservationLawAdvectionDiffusion.diffusionLaw(nuB)
  dlaw.addBoundaryCondition(str(physicalName[0]), dlaw.new0FluxBoundary())
  sys = linearSystemPETScDouble()
  dof = dgDofManager.newCG (groups, 1, sys)
  implicitEuler = dgDIRK(dlaw, dof)
  implicitEuler.iterate (bathDC, fT, 0)
  bathDC.exportMsh('bathOut',0,0)
  sys = 0
  dof = 0
  print 'Bath ok'
