from dgpy import *
import time,math,os,os.path,sys
import toStereo, toLonLat

class slim2d:
  def __init__(self, outputDir):
    self.order=1
    self.dimension=2
    self.outputDir = outputDir
    os.system("mkdir -p %s"%outputDir)
    os.system("rm -f %s/*"%outputDir)
    self.libSlim2d = "Slim2d.so"
    self.R=6371000.
    if (Msg.GetCommRank()==0 ):
      functionC.buildLibraryFromFile ("Slim2d.cc", self.libSlim2d);
    Msg.Barrier()

  def loadMesh(self,meshName, clscale=1):
    extension = meshName[len(meshName)-4:len(meshName)]
    meshName = meshName[0:len(meshName)-4]
    meshNameMsh = meshName + ".msh"
    meshNameGeo = meshName + ".geo"
    if extension == ".geo":
      p = os.system("../../buids/gmsh/gmsh_dynamic %s -%d -order %d -clscale %f"%(meshName, self.dimension, self.order, clscale))
    elif extension == ".msh":
      if(not os.path.exists(meshNameMsh)):
        Msg.Error("Copy or generate the msh from the mesh directory") 
      if(not os.path.exists(meshName+"-Stereo.msh")): 
        m2 = GModel()
        m2.load(meshNameMsh)
        toStereo.projectMesh(m2)
        m2.save("full2-Stereo.msh")
      if(not os.path.exists(meshName+"-lonLat.msh")): 
        m3 = GModel()
        m3.load(meshNameMsh)
        toLonLat.projectMesh(m3, 1.5)#1.5 is the factor to scale Y-axis (plotting purposes)
        m3.save(meshName+"-lonLat.msh")
    else:
      Msg.Error("Strange extension of meshName")

    self.model = GModel()
    self.model.load (meshNameMsh)
  
  def setGroups(self):
    self.groups = dgGroupCollection(self.model, self.dimension, self.order)
    self.groups.splitGroupsByPhysicalTag()
    self.XYZ = self.getFunctionCoordinates()

  def getFunctionCoordinates(self):
    return self.groups.getFunctionCoordinates();

from ScheldtEstuary_bath2d import *
class forcingsDataScheldt:

  def __init__(self, slim2d):
    self.slim2d = slim2d
    listArchive = ["mesh-NWECS+ScheldtEstuary","NWECSData","ScheldtData","shapePreproTidalRivers"]
    for filename in listArchive:
      if(os.path.exists(filename) == 0):
        print "downloading and extracting ",filename
        os.system("curl -O --ftp-port - ftp://braque.mema.ucl.ac.be/slimdata/dgSlimScheldt/%s.zip"%filename)
        os.mkdir(filename)
        fzip = zipfile.ZipFile(filename+".zip")
        fzip.extractall(filename)

  def setBathymetry(self, minV=-1, maxV=3e3):
    self.bathOriginal = bathymetry("bath",self.slim2d.outputDir,self.slim2d.groups, self.slim2d.XYZ, minV, maxV)
    cropOnTag(self.slim2d.groups, self.bathOriginal, 20, 1e3, "Shelf Break")
    cropOnTag(self.slim2d.groups, self.bathOriginal, 10, 1e3, "Bretagne and Basse Normandie")
    self.bathymetry = smoothBathymetry(self.slim2d.groups,self.bathOriginal, minV, maxV)
    
  def setDischarges(self, timeFunction):
    dischargeDir = "ScheldtData/discharges/";
    self.Q_GTC = slimFunctionTemporalSerie(dischargeDir+"terneuzen_discharge_monthly_2000-2008.txt",timeFunction)
    self.Q_GTC.usePeriodicYear()
    self.Q_Bath = slimFunctionTemporalSerie(dischargeDir+"bath_discharge_monthly_1988-2008.txt",timeFunction)
    self.Q_Bath.usePeriodicYear()
    self.Q_DocksFull = slimFunctionTemporalSerie(dischargeDir+"docks_discharge_monthly_1988-2008.txt",timeFunction)
    self.Q_DocksFull.usePeriodicYear()
    self.Q_Docks = functionC(self.slim2d.libSlim2d, "divideBy6", 1, [self.Q_DocksFull])
    self.Q_Meuse = slimFunctionTemporalSerie(dischargeDir+"meuse_discharge_monthly_1971-2007.txt",timeFunction)
    self.Q_Meuse.usePeriodicYear()
    self.Q_Rhine = slimFunctionTemporalSerie(dischargeDir+"rhine_discharge_monthly_1971-2007.txt",timeFunction)
    self.Q_Rhine.usePeriodicYear()
    self.Q_Thames = slimFunctionTemporalSerie(dischargeDir + "thames_discharge_1980_2008",timeFunction)
    self.Q_Seine = slimFunctionTemporalSerie(dischargeDir+"seine_discharge_1980_2008",timeFunction)

  def setTpxoTide(self, timeFunction, bath):
    lonLatDegrees = functionC(self.slim2d.libSlim2d,"lonLatDegrees",3,[self.slim2d.XYZ])
    tideEta = slimFunctionTpxo("NWECSData/tpxo/hf.ES2008.nc","ha","hp","lon_z","lat_z")
    tideEta.setCoordAndTimeFunctions(lonLatDegrees, timeFunction)
    tideEta.useDofContainers(self.slim2d.groups)
    tideU = slimFunctionTpxo("NWECSData/tpxo/uv.ES2008.nc","Ua","up","lon_u","lat_u")
    tideU.setCoordAndTimeFunctions(lonLatDegrees, timeFunction)
    tideU.useDofContainers(self.slim2d.groups)
    tideV = slimFunctionTpxo("NWECSData/tpxo/uv.ES2008.nc","Va","vp","lon_v","lat_v")
    tideV.setCoordAndTimeFunctions(lonLatDegrees, timeFunction)
    tideV.useDofContainers(self.slim2d.groups)
    tideUV = functionC(self.slim2d.libSlim2d,"merge2",2,[tideU, tideV])
    tideUV_stereo = transformLonLat2Stereo(tideUV, self.slim2d.XYZ, self.slim2d.R)
    tideuv = functionC(self.slim2d.libSlim2d,"transport2velocity",3,[tideUV,bath,tideEta]);
    self.tpxoTide = functionC(self.slim2d.libSlim2d,"mergeEtaUV",3,[tideEta,tideuv]);

class slimHydro2d():
  def __init__(self,slim2d):
    self.slim2d = slim2d
    self.groups = slim2d.groups 
    self.claw = dgConservationLawShallowWater2d()
    self.solution = dgDofContainer(self.groups, self.claw.getNbFields())
    ld = functionConstant(0)
    self.claw.setLinearDissipation(ld)
    ldv = functionConstant([0,0])
    self.claw.setSource(ldv)

  def setBathymetry(self,bathDof):
    self.claw.setBathymetry(bathDof.getFunction())
    self.claw.setBathymetryGradient(bathDof.getFunctionGradient())

  def getBathymetry(self):
    return self.claw.getBathymetry()

  def setMovingBathWettingDrying(self,alpha):
    self.claw.setMovingBathWettingDrying(alpha)

  def setInitialSolution(self,initialSolution):
    self.solution.interpolate(initialSolution)

  def setCoriolis(self):
    Coriolis = functionC(self.slim2d.libSlim2d, "coriolis", 1, [self.slim2d.XYZ])
    self.claw.setCoriolisFactor(Coriolis)

  def setUpwindFactorRiemann(self,value):
    self.claw.setUpwindFactorRiemann(value);
    
  def setEarth(self):
    self.claw.setIsSpherical(self.slim2d.R)

  def setDiffusivityOkubo(self,value):
    factor = functionConstant(value)
    self.nu = functionC(self.slim2d.libSlim2d,"okubo",1,[factor]) 
    self.claw.setDiffusivity(self.nu)

  def setDiffusivitySmagorinsky(self):
    nu0 = functionC(self.slim2d.libSlim2d,"smagorinsky",1,[self.solution.getFunctionGradient()])
    self.claw.setDiffusivity(self.nu)

  def setManningFriction(self, manning):
    Cd = functionC(self.slim2d.libSlim2d, "bottomDrag", 1, [self.solution.getFunction(), self.getBathymetry(), manning])
    self.claw.setQuadraticDissipation(Cd)

  def getElevation(self):
    return self.claw.getElevation() 

  def getVelocity(self, coordinateSystem=""):
    if(coordinateSystem==""):
      return self.claw.getVelocity()
    elif(coordinateSystem == "stereo"):
      if(self.R>0):
        uv_stereo = functionC(self.slim2d.libSlim2d,"current",3,[self.solution.getFunction()])
        return uv_stereo
      else:
        Error.Msg("not compatible with stero for the getVelocity")
    elif(coordinateSystem == "lonLat"):
      if(self.R>0): 
        uv_stereo = functionC(self.slim2d.libSlim2d,"current",3,[self.solution.getFunction()])
        uv_lonlat = transformStereo2LonLat(uv_stereo, self.slim2d.getCoordinates(), self.R)
        return uv_lonlat;
      else:
        Error.Msg("not compatible with lonLat for the getVelocity")
    elif(coordinateSystem == "xyz"):
      if(self.R>0):
        uv_stereo = functionC(self.slim2d.libSlim2d,"current",3,[self.solution.getFunction()])
        uv_xyz = transformStereo2XYZ(uv_stereo, self.slim2d.getCoordinates(), self.R)
        return self.claw.getVelocity()
      else:
        Error.Msg("not compatible with xyz for the getVelocity")
    else:
      Error.Msg("unknown coordinate system for getVelocity, only stereo, xyz or lonLat")

  def addBoundaryConditionWall(self,tagList):
    for tag in tagList:
      self.claw.addBoundaryCondition(tag, self.claw.newBoundaryWall())

  def addBoundaryConditionDischarge(self,tagList,discharge):
    for tag in tagList:
      self.claw.addBoundaryCondition(tag, self.claw.newForcedDischarge(self.solution,discharge,tag))

  def addBoundaryConditionSolution(self,tagList,solExt):
    for tag in tagList:
      self.claw.addBoundaryCondition(tag,self.claw.newOutsideValueBoundary("", solExt))
      

