from dgpy import *

def projectMesh (model):
  def project(entity):
    numVertices = entity.getNumMeshVertices()
    for iV in range(0, numVertices): 
      v = entity.getMeshVertex(iV)
      R = (v.x() ** 2 + v.y() ** 2 + v.z() ** 2) ** 0.5
      if not R == 0 :
        v.setXYZ(2 * R * v.x() / (R + v.z()), 2 * R * v.y() / (R + v.z()), 0)

  gvertices = model.bindingsGetVertices()
  for v in gvertices:
    project(v)
  
  gedges = model.bindingsGetEdges()
  for e in gedges:
    project(e)

  gfaces = model.bindingsGetFaces()
  for f in gfaces:
    project(f)
