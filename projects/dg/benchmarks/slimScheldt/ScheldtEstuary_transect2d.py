from dgpy import *
import os 

os.putenv("TRANSECTFILE", "ScheldtData/scheldtTransect.bin");

CCode = """
#include "fullMatrix.h"
#include "function.h"
#include <stdlib.h>
#include "math.h"
#include "MElement.h"
#include "MEdge.h"

static double Ox, Oy, dx, dy;
static int nx, ny;
static double *data;
static bool init = false;
static double R = 6.371e6;

extern "C" {
void initialize() {
  const char * filename = getenv("TRANSECTFILE");
  if (!filename || std::string(filename) == "") {
    fprintf(stderr,"TRANSECTFILE env variable not defined. Use os.putenv(\\"TRANSECTFILE\\", filename) in python.\\n");
    exit(0);
  }
  FILE *input = fopen(filename, "r");
  double buf;
  fread (&Ox, sizeof(double), 1, input);
  fread (&Oy, sizeof(double), 1, input);
  fread (&buf, sizeof(double), 1, input);
  fread (&dx, sizeof(double), 1, input);
  fread (&dy, sizeof(double), 1, input);
  fread (&buf, sizeof(double), 1, input);
  fread (&nx, sizeof(int), 1, input);
  fread (&ny, sizeof(int), 1, input);
  fread (&buf, sizeof(int), 1, input);
  data = new double[nx*ny];
  fread (data, sizeof(double), nx*ny, input);
  init = true;
}
double pow2(double x){
  return x*x;
}
void transectInterpolate (dataCacheMap *m, fullMatrix<double> &transect, fullMatrix<double> &XYZ) {
  if (! init) initialize();
  const double R = 6371220;
  for (size_t i = 0; i< transect.size1(); i++) {
    const double xi = XYZ(i,0);
    const double zeta = XYZ(i,1);
    const double x = 4*R*R*xi/(4*R*R+xi*xi + zeta*zeta);
    const double y = 4*R*R*zeta/(4*R*R+xi*xi + zeta*zeta);
    const double z = R *(4*R*R-xi*xi-zeta*zeta)/(4*R*R+xi*xi + zeta*zeta);
    const double lon = atan2(y,x);
    const double lat = asin(z/R);
    double rx = std::min(std::max((lon-Ox),0.),nx*dx);
    double ry = std::min(std::max((lat-Oy),0.),ny*dy);
    int iX =  floor(rx/dx);
    int iY =  floor(ry/dy);
    double alpha = (rx - iX*dx) / dx;
    double beta = (ry - iY*dy) / dy;
    double b = 0;
    if(lon >= Ox && lon <=Ox+(nx-1)*dx && lat >= Oy && lat <= Oy+(ny-1)*dy){
      b = (1-alpha)*(1-beta)*data[iX*ny+iY]
        +alpha*(1-beta)*data[(iX+1)*ny+iY] 
        +alpha*beta*data[(iX+1)*ny+(iY+1)] 
        +(1-alpha)*beta*data[iX*ny+(iY+1)];
    }
    transect.set (i, 0, b);
  }
}
}
"""
tmpLib = "ScheldtEstuary_transect2d_lib.so"
if (Msg.GetCommRank()==0):
  functionC.buildLibrary (CCode, tmpLib);
Msg.Barrier()

def transect(groups, XYZ):
  transect = dgDofContainer(groups, 1);
  transectS = functionC(tmpLib, "transectInterpolate", 1, [XYZ])
  transect.interpolate(transectS);
  Msg.Info(" Interpolate Transect")
  return transect


