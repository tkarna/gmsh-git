from dgpy import *
from termcolor import colored
from math import *
import time, math, os, os.path, sys, datetime, calendar
from ScheldtEstuary_bath2d import *
import toStereo, toLonLat
from scipy.weave import blitz
from Common import *
import zipfile

listArchive = ["mesh-NWECS+ScheldtEstuary","NWECSData","ScheldtData","shapePreproTidalRivers"]
for filename in listArchive:
  if(os.path.exists(filename) == 0):
    print "downloading ",filename
    os.system("curl -O --ftp-port - ftp://braque.mema.ucl.ac.be/slimdata/dgSlimScheldt/%s.zip"%filename)
    os.mkdir(filename)
    fzip = zipfile.ZipFile(filename+".zip")
    fzip.extractall(filename)

clscale = 1

if (Msg.GetCommRank() == 0):
  functionC.buildLibraryFromFile ("Slim2d.cc", "libSlim2d.so");

Msg.Barrier()
libSlim2d = "libSlim2d.so"

if(not os.path.exists("full2.msh")):
  os.system("cp mesh-NWECS+ScheldtEstuary/full2.msh .") 
  #m = GModel()
  #m.load("mesh-NWECS+ScheldtEstuary/full2.geo")
  #GmshSetOption("Mesh","CharacteristicLengthFactor",   clscale)
  #GmshSetOption("Mesh","Algorithm", 4)
  #GmshSetOption("Mesh","Remove4Triangles",   1)
  #GmshSetOption("Mesh","CharacteristicLengthExtendFromBoundary",   0)
  #GmshSetOption("Mesh", "LcIntegrationPrecision", 1.e-3)
  #genMesh("full2",2, 1)
  #m.save("full2.msh")

if(not os.path.exists("full2-Stereo.msh")): 
  m2 = GModel()
  m2.load("full2.msh")
  toStereo.projectMesh(m2)
  m2.save("full2-Stereo.msh")

if(not os.path.exists("full2-lonLat.msh")): 
  m3 = GModel()
  m3.load("full2.msh")
  toLonLat.projectMesh(m3, 1.5)#1.5 is the factor to scale Y-axis (plotting purposes)
  m3.save("full2-lonLat.msh")


model = GModel()
model.load ('full2-Stereo.msh')

def printTime (t) :
  return "%dh%02d'%02d\"" % (t/3600,  (t%3600)/60,  t%60)
def printDate (t):
  date=time.gmtime(t)
  return "%02d/%02d/%04d %02dh%02d'%02d\""%(date[2], date[1], date[0], date[3], date[4], date[5])

elapsedTime=0
initialDate=datetime.datetime(2000,1,1,12,0,0)
initialTime=calendar.timegm(initialDate.timetuple())
timeFunction = functionConstant(initialTime)
elapsedTimeFunction=functionConstant([elapsedTime])

outputDir = "output/"
try : 
  os.mkdir(outputDir);
except: 0;
os.system("rm -f %s/*"%outputDir)

# Python Functions
def printTime (t) :
  return "%dh%02d'%02d\"" % (t/3600,  (t%3600)/60,  t%60)

groups = dgGroupCollection(model, 2, 1)
claw = dgConservationLawShallowWater2d()
solution = dgDofContainer(groups, claw.getNbFields())
XYZ = groups.getFunctionCoordinates();

#bathymetry
bathOriginal = bathymetry("bath",outputDir,groups, XYZ, minV=-1, maxV=3e3)
cropOnTag(groups, bathOriginal, 20, 1e3, "Shelf Break")
cropOnTag(groups, bathOriginal, 10, 1e3, "Bretagne and Basse Normandie")
bathEffective = smoothBathymetry(groups,bathOriginal, minV=-1, maxV=3e3)
bath4MR = dgDofContainer(groups, 1)
bath4MR.interpolate(bathEffective.getFunction())
bath4MR.bezierCrop(1.,1e12)

#SW2D
claw = dgConservationLawShallowWater2d()
solution = dgDofContainer(groups, claw.getNbFields())
initialSolution = functionConstant([0., 0., 0.])
solution.interpolate(initialSolution)
Coriolis = functionC(libSlim2d, "coriolis", 1, [XYZ])
claw.setCoriolisFactor(Coriolis)
claw.setUpwindFactorRiemann(1.);

R=6371000.
claw.setIsSpherical(R)

nu0 = functionC(libSlim2d,"smagorinsky",1,[solution.getFunctionGradient()])
nu = functionC(libSlim2d,"diffusivityStereo",1,[nu0, XYZ])
#claw.setDiffusivity(nu)

claw.setBathymetry(bathEffective.getFunction())
claw.setBathymetryGradient(bathEffective.getFunctionGradient())
#claw.setMovingBathWettingDrying(0.5)

ld = functionConstant(0)
claw.setLinearDissipation(ld)
claw.setSource(ld)

bath = claw.getBathymetry()
elevation = claw.getElevation()

manning = functionConstant(0.03)
Cd = functionC(libSlim2d, "bottomDrag", 1, [solution.getFunction(), bath, manning])
claw.setQuadraticDissipation(Cd)
#Discharges
dischargeDir = "ScheldtData/discharges/";
claw.addBoundaryCondition("Coast", claw.newBoundaryWall())
claw.addBoundaryCondition("none", claw.newBoundaryWall())

Q_GTC = slimFunctionTemporalSerie(dischargeDir+"terneuzen_discharge_monthly_2000-2008.txt",timeFunction)
Q_GTC.usePeriodicYear()
claw.addBoundaryCondition("Ghent-Terneuzen Canal", claw.newForcedDischarge(solution, Q_GTC, "Ghent-Terneuzen Canal"))

Q_Bath = slimFunctionTemporalSerie(dischargeDir+"bath_discharge_monthly_1988-2008.txt",timeFunction)
Q_Bath.usePeriodicYear()
claw.addBoundaryCondition("Bath Canal", claw.newForcedDischarge(solution, Q_Bath, "Bath Canal"))

Q_DocksFull = slimFunctionTemporalSerie(dischargeDir+"docks_discharge_monthly_1988-2008.txt",timeFunction)
Q_DocksFull.usePeriodicYear()
Q_Docks = functionC(libSlim2d, "divideBy6", 1, [Q_DocksFull])
claw.addBoundaryCondition("Zandvlietsluis North", claw.newForcedDischarge(solution,Q_Docks,"Zandvlietsluis North" ))
claw.addBoundaryCondition("Zandvlietsluis South", claw.newForcedDischarge(solution,Q_Docks,"Zandvlietsluis South"))
claw.addBoundaryCondition("BoudewijnSluis North", claw.newForcedDischarge(solution,Q_Docks,"BoudewijnSluis North"))
claw.addBoundaryCondition("BoudewijnSluis South", claw.newForcedDischarge(solution,Q_Docks,"BoudewijnSluis South"))
claw.addBoundaryCondition("Kallo Sluis"         , claw.newForcedDischarge(solution,Q_Docks,"Kallo Sluis"))
claw.addBoundaryCondition("Hansweert Canal"     , claw.newForcedDischarge(solution,Q_Docks,"Hansweert Canal"))

claw.addBoundaryCondition("Upstream", claw.newBoundaryWall())

Q_Meuse = slimFunctionTemporalSerie(dischargeDir+"meuse_discharge_monthly_1971-2007.txt",timeFunction)
Q_Meuse.usePeriodicYear()
claw.addBoundaryCondition("Meuse",claw.newForcedDischarge(solution,Q_Meuse,"Meuse"))

Q_Rhine = slimFunctionTemporalSerie(dischargeDir+"rhine_discharge_monthly_1971-2007.txt",timeFunction)
Q_Rhine.usePeriodicYear()
claw.addBoundaryCondition("Rhine",claw.newForcedDischarge(solution,Q_Rhine,"Rhine"))

Q_Thames = slimFunctionTemporalSerie(dischargeDir + "thames_discharge_1980_2008",timeFunction)
claw.addBoundaryCondition("Thames", claw.newForcedDischarge(solution,Q_Thames,"Thames"))

Q_Seine = slimFunctionTemporalSerie(dischargeDir+"seine_discharge_1980_2008",timeFunction)
claw.addBoundaryCondition("Seine",claw.newForcedDischarge(solution,Q_Seine,"Seine"))
claw.addBoundaryCondition("Baltic", claw.newBoundaryWall())
claw.addBoundaryCondition("Bretagne and Basse Normandie", claw.newBoundaryWall())

lonLatDegrees = functionC(libSlim2d,"lonLatDegrees",3,[XYZ])
tideEta = slimFunctionTpxo("NWECSData/tpxo/hf.ES2008.nc","ha","hp","lon_z","lat_z")
tideEta.setCoordAndTimeFunctions(lonLatDegrees, timeFunction)
tideEta.useDofContainers(groups)
tideU = slimFunctionTpxo("NWECSData/tpxo/uv.ES2008.nc","Ua","up","lon_u","lat_u")
tideU.setCoordAndTimeFunctions(lonLatDegrees, timeFunction)
tideU.useDofContainers(groups)
tideV = slimFunctionTpxo("NWECSData/tpxo/uv.ES2008.nc","Va","vp","lon_v","lat_v")
tideV.setCoordAndTimeFunctions(lonLatDegrees, timeFunction)
tideV.useDofContainers(groups)
tideUV = functionC(libSlim2d,"merge2",2,[tideU, tideV])
tideUV_stereo = transformLonLat2Stereo(tideUV, XYZ, R)
tideuv = functionC(libSlim2d,"transport2velocity",3,[tideUV,bath,tideEta]);
tpxoTide = functionC(libSlim2d,"mergeEtaUV",3,[tideEta,tideuv]);
tpxo_u = functionExtractCompNew(tpxoTide,1)
tpxo_v = functionExtractCompNew(tpxoTide,2)
claw.addBoundaryCondition('Shelf Break',claw.newOutsideValueBoundary("Surface", tpxoTide))
uv_stereo = functionC(libSlim2d,"current",3,[solution.getFunction()])
uv_lonlat = transformStereo2LonLat(uv_stereo, XYZ, R)

rk = dgMultirateERK(groups, claw, ERK_22_C)
dt = rk.splitGroupsForMultirate(2, solution, [solution, bath4MR])
print "dtMin = ", rk.dtMin(), "dtMax = ", rk.dtMax()
rk.printMultirateInfo(0)
solution.exportGroupIdMsh()
if(Msg.GetCommRank() == 0):
  print 'SU=', rk.speedUp()

nbExport = 0
t = initialTime
elapsedTime = 0
Msg.Info("%d %s" % (nbExport,printDate(t)))
t = initialTime
nbSteps = 10000
for i in range (0,nbSteps):
	if (i % 10 == 0):
		nbExport  = nbExport  + 1
		Msg.Info("%d %s :norm=%.4e" % (nbExport,printDate(t),solution.norm()))
		solution.exportFunctionMsh(elevation,outputDir+"eta%05d"%(nbExport),t,nbExport)
		bath4MR.exportMsh(outputDir+"bath4MR%05d"%nbExport)
	rk.iterate (solution, dt, t)	
	t = t + dt
	timeFunction.set(t)
	elapsedTime = elapsedTime + dt
	elapsedTimeFunction.set(elapsedTime)

Msg.Exit(0)
