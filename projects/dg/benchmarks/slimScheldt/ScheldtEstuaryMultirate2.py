from dgpy import *
from termcolor import colored
from math import *
import math, os, os.path, sys
from scipy.weave import blitz
from Common import *
from slim2d import *
from slimTime import *

time = slimTime(2000,1,1,12,0,0)
slim_2d = slim2d("output")
slim_2d.loadMesh("full2.msh")
slim_2d.setGroups()
forcings = forcingsDataScheldt(slim_2d)
forcings.setBathymetry()
bathEffective = forcings.bathymetry
hydro_2d = slimHydro2d(slim_2d)
hydro_2d.setBathymetry(bathEffective)
hydro_2d.setMovingBathWettingDrying(0.5)
hydro_2d.setCoriolis()
hydro_2d.setUpwindFactorRiemann(0.1)
hydro_2d.setEarth()
#hydro_2d.setDiffusivityOkubo(1.0)
manning = functionConstant(0.023)
hydro_2d.setManningFriction(manning)

initialCondition_2d = functionConstant([0,0,0])
hydro_2d.setInitialSolution(initialCondition_2d)

elevation = hydro_2d.getElevation()
velocity = hydro_2d.getVelocity()

#BND conditions
forcings.setDischarges(time.timeFunction)
forcings.setTpxoTide(time.timeFunction,hydro_2d.getBathymetry())

hydro_2d.addBoundaryConditionWall(["Coast","none","Upstream","Baltic","Bretagne and Basse Normandie"])
hydro_2d.addBoundaryConditionDischarge(["Ghent-Terneuzen Canal"],forcings.Q_GTC)
hydro_2d.addBoundaryConditionDischarge(["Bath Canal"],forcings.Q_Bath)
hydro_2d.addBoundaryConditionDischarge(["Zandvlietsluis North","Zandvlietsluis South","BoudewijnSluis North","BoudewijnSluis South","Kallo Sluis","Hansweert Canal"],forcings.Q_Docks)
hydro_2d.addBoundaryConditionDischarge(["Meuse"],forcings.Q_Meuse)
hydro_2d.addBoundaryConditionDischarge(["Rhine"],forcings.Q_Rhine)
hydro_2d.addBoundaryConditionDischarge(["Thames"],forcings.Q_Thames)
hydro_2d.addBoundaryConditionDischarge(["Seine"],forcings.Q_Seine)
hydro_2d.addBoundaryConditionSolution(["Shelf Break"], forcings.tpxoTide)

#rk = dgMultirateERK(slim_2d.groups, hydro_2d.claw, ERK_22_C)
#nbMultirateGroups = 1
#dt = rk.splitGroupsForMultirate(nbMultirateGroups, hydro_2d.solution, [hydro_2d.solution])
#print "dtMin = ", rk.dtMin(), "dtMax = ", rk.dtMax()
#rk.printMultirateInfo(0)
#hydro_2d.solution.exportGroupIdMsh()
#if(Msg.GetCommRank() == 0):
#  print 'SU=', rk.speedUp()
dt = 0.1
rk=dgERK(hydro_2d.claw,None,DG_ERK_22)

nbExport = 0
Msg.Info("%d %s" % (nbExport,time.printDate()))
nbSteps = 10000
for i in range (0,nbSteps):
  if (i % 10 == 0):
    nbExport  = nbExport  + 1
    Msg.Info("%d %s :norm=%.4e" % (nbExport,time.printDate(),hydro_2d.solution.norm()))
    hydro_2d.solution.exportFunctionMsh(elevation,slim_2d.outputDir+"eta%05d"%(nbExport))
  rk.iterate (hydro_2d.solution, dt, time.t)	
  time.add(dt)
Msg.Exit(0)
