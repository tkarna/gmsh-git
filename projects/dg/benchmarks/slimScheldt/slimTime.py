from dgpy import *
import time, datetime, calendar

class slimTime():
  def __init__(self, year,month,day,hour, minute, second):
    self.initialDate = datetime.datetime(year, month, day, hour, minute, second)
    self.initialTime = calendar.timegm(self.initialDate.timetuple())
    self.t = self.initialTime
    self.timeFunction = functionConstant(self.t)
    self.elapsedTime=0
    self.elapsedTimeFunction = functionConstant(self.elapsedTime)
  def add(self,dt):
    self.time = self.t + dt
    self.timeFunction.set(self.t)
    self.elapsedTime = self.elapsedTime + dt
    self.elapsedTimeFunction.set(self.elapsedTime)
  def printDate(self):
    date=time.gmtime(self.t)
    print "%02d/%02d/%04d %02dh%02d'%02d\""%(date[2], date[1], date[0], date[3], date[4], date[5])
  def printTime(self):
    print "time = %.6g elapsed = %.6g" %(self.t,self.elapsedTime)
