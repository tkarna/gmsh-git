//estuary.geo
L=1e4;
w=1000;
Point(1) = {-5*L, w/2, 0};
Point(2) = {0, w/2, 0};
Point(3) = {-5*L, -w/2, 0};
Point(4) = {0, -w/2, 0};
Line(1) = {1, 2};
Line(2) = {2, 4};
Line(3) = {4, 3};
Line(4) = {3, 1};
Line Loop(5) = {1, 2, 3, 4};
Plane Surface(6) = {5};

Physical Line("Upstream") = {2};
Physical Line("Downstream") = {4};
Physical Surface("Surface") = {6};

Field[1] = MathEval;
Field[1].F = "0.2e4 /5";
Background Field = 1;
