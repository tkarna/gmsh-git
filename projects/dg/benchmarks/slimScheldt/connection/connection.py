from dgpy import *
from gmshpy import *
import time, math, os, os.path, sys
geo=".geo"
msh=".msh"
order=1
clscale=1

os.system("mkdir -p output")
os.system("rm -f output/*")

CCode = """
#include "fullMatrix.h"
#include "function.h"
#include "fullMatrix.h"
#include "dgGroupOfElements.h"
#include "MElement.h"
#include "MEdge.h"
#include "math.h"
extern "C" {
  double pow2(double value){
  	return value*value;
  }
	void gaussian(dataCacheMap *,fullMatrix<double> &f,fullMatrix<double> &xyz){
    for (size_t i=0; i<xyz.size1(); i++){
      f.set(i,0,0*exp(-0.5*pow2(xyz(i,0)-0.5e5)/pow2(0.5e4))); 
    }
	}
	void gaussian2(dataCacheMap *,fullMatrix<double> &f,fullMatrix<double> &xyz){
    for (size_t i=0; i<xyz.size1(); i++){
      f.set(i,0,1*exp(-0.5*pow2(xyz(i,0)+5e3)/pow2(5e2))); 
    }
	}
	void chezyManningDissipation(dataCacheMap *,fullMatrix<double> &C,fullMatrix<double> &section, fullMatrix<double> &width){
    for (size_t i=0; i<C.size1(); i++){
      double Heff=section(i,0)/width(i,0);
      double manning=0.01;
      double chezy=pow(section(i,0)/(width(i,0)+2*Heff),1./6.)/manning;
      double g=9.81;
      C.set(i,0,g/(pow2(chezy)*Heff)); 
    }
	}
	void merge2scalars(dataCacheMap *,fullMatrix<double> &uv,fullMatrix<double> &u, fullMatrix<double> &v){
    for (size_t i=0; i<uv.size1(); i++){
      uv.set(i,0,u(i,0)); 
      uv.set(i,1,v(i,0)); 
    }
	}
	void widthData(dataCacheMap *m,fullMatrix<double> &w,fullMatrix<double> &bath, fullMatrix<double> &eta, fullMatrix<double> &x){
    for (size_t i=0; i<bath.size1(); i++){
      w.set(i,0,1000.); 
      //w.set(i,0,(100+(100-50)/bath(i,0)*eta(i,0))); 
    }
	}
	void sectionData(dataCacheMap *m,fullMatrix<double> &s,fullMatrix<double> &bath, fullMatrix<double> &eta){
    for (size_t i=0; i<s.size1(); i++){
      s.set(i,0, 1000*(eta(i,0)+bath(i,0))); 
      //s.set(i,0,(100*(eta(i,0)+bath(i,0))+(100-50)/bath(i,0)*(pow2(eta(i,0))-pow2(bath(i,0)))/2)  ); 
    }
	}
	void square(dataCacheMap *m,fullMatrix<double> &ww,fullMatrix<double> &w){
    for (size_t i=0; i<w.size1(); i++){
      ww.set(i,0,w(i,0)*w(i,0)); 
    }
	}
  const double R = 6371000.;
  void coriolis (dataCacheMap *, fullMatrix<double> &FCT, fullMatrix<double> &xyz) {
    double alpha = 0;
    double Omega = 7.292e-5;
    for (size_t i = 0; i< FCT.size1(); i++) {
      double xi = xyz(i,0);
      double beta = xyz(i,1);
      double lat = asin((4*R*R-xi*xi-beta*beta)/(4*R*R+xi*xi + beta*beta));
      FCT.set(i,0,2*Omega*sin(lat));
    }
  }
  
  void transport2velocity(dataCacheMap *,fullMatrix<double> &uv, fullMatrix<double> &UV, fullMatrix<double> &bath, fullMatrix<double> &eta){
    for (size_t i = 0; i < eta.size1(); i++) {
      uv.set(i, 0, UV(i,0)/(bath(i,0)+eta(i,0))); 
      uv.set(i, 1, UV(i,1)/(bath(i,0)+eta(i,0))); 
    }
  }
  
  void lonLatDegrees(dataCacheMap *, fullMatrix<double> &lonLat, fullMatrix<double> &XYZ){
    for (size_t i = 0; i < lonLat.size1(); i++) {
      double xi = XYZ(i,0);
      double zeta = XYZ(i,1);
      double x = 4*R*R*xi/(4*R*R +xi*xi + zeta*zeta);
      double y = 4*R*R*zeta/(4*R*R +xi*xi + zeta*zeta);
      double z = R *(4*R*R-xi*xi-zeta*zeta)/(4*R*R+xi*xi + zeta*zeta);
      double lon = atan2(y,x)*180/M_PI;
      double lat = asin(z/R)*180/M_PI;
      lonLat.set(i,0,lon);
      lonLat.set(i,1,lat);
      lonLat.set(i,2,0.);
    }
  }
  void diffusivityStereo(dataCacheMap *, fullMatrix<double> &diff, fullMatrix<double> &diff0, fullMatrix<double> &XYZ){
    for (size_t i = 0; i < diff.size1(); i++) {
      double xi = XYZ(i,0);
      double zeta = XYZ(i,1);
      double J = 4*R*R/(4*R*R + xi*xi + zeta*zeta);
      diff.set(i,0,diff(i,0)/J/J);
    }
  }
  
  void computeErrorVel(dataCacheMap *, fullMatrix<double> &err, fullMatrix<double> &solution1, fullMatrix<double> &solution2){
    for (size_t i = 0; i< err.size1(); i++) {
      err.set(i, 0, solution1(i,1)-solution2(i, 1)); 
      err.set(i, 1, solution1(i,2)-solution2(i, 2)); 
    }
  }
  
  void mergeEtaUV(dataCacheMap *,fullMatrix<double> &sol,fullMatrix<double> &eta, fullMatrix<double> &uv){
    for (size_t i = 0; i < sol.size1(); i++) {
      sol.set(i, 0, eta(i,0)); 
      sol.set(i, 1, uv(i,0)); 
      sol.set(i, 2, uv(i,1)); 
    }
  }
  void merge2(dataCacheMap *,fullMatrix<double> &sol,fullMatrix<double> &u1, fullMatrix<double> &u2){
    for (size_t i = 0; i < sol.size1(); i++) {
      sol.set(i, 0, u1(i,0)); 
      sol.set(i, 1, u2(i,0)); 
    }
  }
  
  void bottomDrag(dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &sol, fullMatrix<double> &bath, fullMatrix<double> &manning){
    for(int i = 0;  i < val.size1(); i++){
      double H = sol(i, 0) + bath(i, 0);
      double ampli = 5;
      double factor = 1.;//(ampli - 1) * exp(-0.5 * H*H / ( 1*1 )) + 1 ;
      double n = factor * manning(i,0);
      val.set(i, 0, 9.81*n*n/(pow(H, 1.333333333333)));
    }
  }
  
  void smagorinsky(dataCacheMap *map, fullMatrix<double> &val, fullMatrix<double> &solGradient){
    for(int i = 0;  i < val.size1(); i++){
      double dudx = solGradient(i, 3);
      double dudy = solGradient(i, 4);
      double dvdx = solGradient(i, 6);
      double dvdy = solGradient(i, 7);
     
      const dgGroupOfElements *g = map->getGroupOfElements();
      int elNum = map->getElementId();
  
      double radi =g->getInnerRadius(elNum);
      
      val(i, 0)= pow_int(0.1*radi, 2)*sqrt(2*dudx*dudx+2*dvdy*dvdy+pow_int(dudy+dvdx, 2));
    }
  }
  
  void current (dataCacheMap *, fullMatrix<double> &current, fullMatrix<double> &solution) {
    for (size_t i = 0; i < current.size1(); i++) {
      current.set(i, 0, solution(i,1));
      current.set(i, 1, solution(i,2));
      current.set(i, 2, 0);
    }
  }
  
  void windStress (dataCacheMap *,fullMatrix<double> &windstress,fullMatrix<double> &winduv, fullMatrix<double> &sol, fullMatrix<double> &bath) {
    for (size_t i = 0; i < windstress.size1(); i++) {
      double normWind=sqrt(winduv(i, 0)*winduv(i, 0)+winduv(i, 1)*winduv(i, 1));
      double rho=1025;
      double sol1 = 0.001*(0.630*normWind+0.066*normWind*normWind)*winduv(i, 0)/(rho*(sol(i, 0)+bath(i, 0)));
      double sol2 = 0.001*(0.630*normWind+0.066*normWind*normWind)*winduv(i, 1)/(rho*(sol(i, 0)+bath(i, 0)));
      windstress.set(i, 0, sol1);
      windstress.set(i, 1, sol2);
    }
  }
  
  void bathVec(dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &bathymetry, fullMatrix<double> &normals, fullMatrix<double> &boundaryData) {
    double vx, vy;
    vx = boundaryData(0, 1);
    vy = boundaryData(0, 2);
    for (size_t i = 0; i < bathymetry.size1(); i++) {
      sol.set(i, 0, bathymetry(i, 0) * (vx*normals(i, 0) + vy*normals(i, 1)) );
    }
  }
}
"""

tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CCode, tmpLib);
Msg.Barrier()

model_1d = GModel()
model_2d = GModel()
meshName_1d = "river"
meshName_2d = "estuary"
print "load meshes"
model_1d.load (meshName_1d+geo)
model_1d.load (meshName_1d+msh)
model_2d.load (meshName_2d+geo)
model_2d.load (meshName_2d+msh)

curv_1d = dgSpaceTransform1DCurvilinear()
groups_1d = dgGroupCollection(model_1d, 1, order, curv_1d)
groups_1d.splitGroupsByPhysicalTag()
XYZ_1d=groups_1d.getFunctionCoordinates()

groups_2d = dgGroupCollection(model_2d, 2, order)
groups_2d.splitGroupsByPhysicalTag()
XYZ_1d=groups_1d.getFunctionCoordinates()

hydroLaw_1d = dgConservationLawShallowWater1d()
hydroLaw_1d.setElevationInterp()
etaInterp_1d=hydroLaw_1d.getElevationInterp()

hydroLaw_2d = dgConservationLawShallowWater2d()
XYZ_2d=groups_2d.getFunctionCoordinates()

zeroF=functionConstant([0.0])
zeroFF=functionConstant([0.0,0.0])

#-------------------------
# Geometry
#-------------------------
bath=functionConstant([10.])
widthData_1d = functionC(tmpLib,"widthData",1,[bath,etaInterp_1d])
sectionData_1d = functionC(tmpLib,"sectionData",1,[bath,etaInterp_1d])
hydroLaw_1d.setWidthData(widthData_1d)
hydroLaw_1d.setSectionData(sectionData_1d)

hydroLaw_2d.setBathymetry(bath)
hydroLaw_2d.setBathymetryGradient(zeroF)

#-------------------------
# Initial conditions
#-------------------------
etaGaussian_1d = functionC(tmpLib,"gaussian",1,[XYZ_1d])
initialSection_1d = functionC(tmpLib,"sectionData",1,[bath,etaGaussian_1d])
initialSol_1d = functionC(tmpLib,"merge2scalars",2,[initialSection_1d,zeroF])

etaGaussian_2d = functionC(tmpLib,"gaussian2",1,[XYZ_2d])
initialSol_2d = functionC(tmpLib,"mergeEtaUV",3,[etaGaussian_2d,zeroFF])

#-------------------------
# Hydro solution
#-------------------------
hydroSol_1d = dgDofContainer(groups_1d, hydroLaw_1d.getNbFields())
hydroSol_1d.interpolate(initialSol_1d)
hydroLaw_1d.setHydroSolution(hydroSol_1d.getFunction())
hydroLaw_1d.setElevation()
hydroLaw_1d.setWidth()

hydroSol_2d = dgDofContainer(groups_2d, hydroLaw_2d.getNbFields())
hydroSol_2d.interpolate(initialSol_2d)

#-------------------------
# get Fields
#-------------------------
etaF_1d = hydroLaw_1d.getElevation()
width_1d= hydroLaw_1d.getWidth()
section_1d = hydroLaw_1d.getSection()
velocity_1d = hydroLaw_1d.getVelocity()

#-------------------------
# Supplementary terms1
#-------------------------
quadDiss_1d=functionC(tmpLib,"chezyManningDissipation",1,[section_1d,width_1d])
#hydroLaw_1d.setQuadraticDissipation(quadDiss_1d)
diffusivity_1d=functionConstant(1.)
#hydroLaw_1d.setDiffusivity(diffusivity_1d)
hydroLaw_1d.setSetup()

eta_2d = hydroLaw_2d.getElevation()
#-------------------------
# Boundary conditions
#-------------------------
coupling12 = dgCoupling_SW1D_SW2D(groups_1d, groups_2d, hydroSol_1d, hydroSol_2d, bath, "Downstream","Upstream")


hydroLaw_1d.addBoundaryCondition("Downstream",hydroLaw_1d.newBoundaryCouplingSW2D(coupling12.getOutsideSolution2(), coupling12.getWidth(), coupling12.getSection0()))
#hydroLaw_1d.addBoundaryCondition("Downstream",hydroLaw_1d.newBoundaryWall())
hydroLaw_1d.addBoundaryCondition("Upstream",hydroLaw_1d.newBoundaryWall())

hydroLaw_2d.addBoundaryCondition("Downstream",hydroLaw_2d.newBoundaryWall())
def testF(fct, sol):
  for i in range(fct.size1()):
    fct.set(i,0, 3)
    fct.set(i,1,sol(i,1))
    fct.set(i,2,0.0)
test = functionPython(3,testF,[hydroSol_2d.getFunction()])
hydroLaw_2d.addBoundaryCondition("Upstream",hydroLaw_2d.newBoundaryCouplingSW1D(coupling12.getOutsideSolution1()))
#hydroLaw_2d.addBoundaryCondition("Upstream",hydroLaw_2d.newBoundaryWall())
#hydroLaw_2d.addBoundaryCondition("Upstream",hydroLaw_2d.newOutsideValueBoundary("", coupling12.getOutsideSolution1()))

hydroLaw_2d.addBoundaryCondition("none",hydroLaw_2d.newBoundaryWall())

#-------------------------
# Time stepping
#-------------------------
t=0.0
dt = 1
nbExport = 0
#-------------------------
print "Solve"
#-------------------------
implicitTs = False
if implicitTs:
  petsc = linearSystemPETScDouble()
  petsc.setParameter("petscOptions",  "-pc_type bjacobi -sub_pc_type ilu")
  dof_1d = dgDofManager.newDG(groups_1d, hydroLaw_1d.getNbFields(), petsc)
  dof_2d = dgDofManager.newDG(groups_2d, hydroLaw_2d.getNbFields(), petsc)
  rkHydro_2d = dgDIRK(hydroLaw_2d, dof_2d, 2) 
  rkHydro_2d.getNewton().setAtol(1.e-5)
  rkHydro_2d.getNewton().setRtol(1.e-5)
  rkHydro_2d.getNewton().setVerb(1)
  rkHydro_1d = dgDIRK(hydroLaw_1d, dof_1d, 2) 
  rkHydro_1d.getNewton().setAtol(1.e-5)
  rkHydro_1d.getNewton().setRtol(1.e-5)
  rkHydro_1d.getNewton().setVerb(1)
else:
  rkHydro_1d=dgERK(hydroLaw_1d,None,DG_ERK_44)
  rkHydro_2d=dgERK(hydroLaw_2d,None,DG_ERK_44)

hydroSol_2d.exportFunctionMsh(etaGaussian_2d,"output/initial_2d")
for i in range (0, 1000000) :
  if( nbExport == 20): break
  if (i % 100 == 0) :
    Msg.Info("%d %03d norm=%.8e" % (nbExport,i,hydroSol_1d.norm()))
    hydroSol_1d.exportMsh("output/sol1d_%05d"%i,t,i,"1d")
    hydroSol_2d.exportMsh("output/sol2d_%05d"%i,t,i,"2d")
    hydroSol_1d.exportFunctionMsh(etaF_1d,"output/eta1d_%05d"%i,t,i)
    hydroSol_2d.exportFunctionMsh(eta_2d,"output/eta2d_%05d"%i,t,i)
    nbExport  = nbExport  + 1
    
  for j in range(rkHydro_2d.getNsteps()) :
    coupling12.updateIntegrals()
    rkHydro_1d.subiterate(hydroSol_1d,dt,t)
    rkHydro_2d.subiterate(hydroSol_2d,dt,t)
  rkHydro_1d.finalize(hydroSol_1d)
  rkHydro_2d.finalize(hydroSol_2d)
  t = t+dt

Msg.Exit(0)


