from dgpy import *
from gmshpy import *
import ScheldtTidalRivers_distance1d as distance1d
import ScheldtTidalRivers_shape1d as shape1d
import ScheldtTidalRivers_export1d as export1d
import ScheldtTidalRivers_discharges1d as discharges1d
import time, math, os, os.path, sys, datetime, calendar
from slimRiver1d import *

def printTime (t) :
  return "%dh%02d'%02d\"" % (t/3600,  (t%3600)/60,  t%60)
def printDate (t):
  date=time.gmtime(t)
  return "%02d/%02d/%04d %02dh%02d'%02d\""%(date[2], date[1], date[0], date[3], date[4], date[5])

elapsedTime=0
initialDate=datetime.datetime(1990,1,1,12,0,0)
initialTime=calendar.timegm(initialDate.timetuple())
elapsedTimeFunction=functionConstant([elapsedTime])
timeFunction = functionConstant(initialTime)

river = slimRiver1d(outputDirectory="outputScheldtTidalRivers")
river.loadMesh("ScheldtTidalRivers.msh",clscale=4)
groups = river.getGroups()
x1d = distance1d.get(groups)
river.setDistance1d(x1d)
#-------------------------
# Geometry
#-------------------------
riverNames=["OUDESCHELDE","DURME","ZEESCHELDE","RINGVAART","RUPEL","KLEINENETE","NETEAFLEIDING","BENEDENNETE","GROTENETE","BENEDENDIJLE","BOVENDIJLE","DIJLEAFLEIDING","ZENNEAFL","ZENNE","DENDER"]
smoothRiverData=30
shapeData=shape1d.riverShape("ScheldtData/shapes",riverNames,smoothRiverData)
shapeData.setFields(groups,x1d)
etaInterp = functionConstant(0.) 
altitudeData,widthData,sectionData=shapeData.get(groups,etaInterp)
river.setGeometry(altitudeData,widthData,sectionData)
#---------------------------
# tracer
#---------------------------
tracer = slimRiverTracer1d(river)
def setElevationAltitudeBank(FCT,altitude):
  for i in range(0,FCT.size1()):
    bottom=altitude(i,1)
    bank=min(altitude(i,2),altitude(i,3))
    layer0=altitude(i,0)
    depthMax = bank-bottom
    FCT.set(i,0,max(min(bank-3,7),0))

elevationInitial = functionPython(1,setElevationAltitudeBank,[altitudeData.getFunction()])
tracer.setInitialCondition(elevationInitial)
tracer.setSolution()
velocity = functionConstant(0.)
elevation = functionConstant(0.)
width = functionConstant(100.)
section = functionConstant(1000.)
hydro = functionC(river.libSlim1d,"merge2scalars",2,[section,velocity])
tracer.setHydro(hydro,width,elevation)
diffusivity = functionConstant(10.)
tracer.setDiffusivity(diffusivity)
#-------------------------
# Boundary conditions
#-------------------------

#DOWNSTREAM
tracerGradientBnd = functionConstant(0.)
tracer.bndForceTracer(elevationInitial,tracerGradientBnd,"downstreamZEESCHELDE")

#UPSTREAM
tracer.bndForceTracer(elevationInitial,tracerGradientBnd,"Ghent-upstreamOUDESCHELDE")
tracer.bndForceTracer(elevationInitial,tracerGradientBnd,"Ghent-upstreamRINGVAART")
tracer.bndForceTracer(elevationInitial,tracerGradientBnd,"upstreamDURME")
tracer.bndForceTracer(elevationInitial,tracerGradientBnd,"upstreamDENDER")
tracer.bndForceTracer(elevationInitial,tracerGradientBnd,"Itegem-upstreamGROTENETE")
tracer.bndForceTracer(elevationInitial,tracerGradientBnd,"Grobbendonk-upstreamKLEINENETE")
tracer.bndForceTracer(elevationInitial,tracerGradientBnd,"Haacht-upstreamBOVENDIJLE")
tracer.bndForceTracer(elevationInitial,tracerGradientBnd,"Eppegem-upstreamZENNE")
#-------------------------
# Initial exports
#-------------------------
export = export1d.export(groups, tracer.solution, width, river.x1d.getFunction(), river.outputDirectory)
export.exportFunctionTxt(tracer.solution.getFunction(),"tracer",0)

#-------------------------
# Iterate
#-------------------------
nbExport = 0

dt = 60
petsc = linearSystemPETScBlockDouble()
petsc.setParameter("petscOptions",  "-pc_type bjacobi -sub_pc_type ilu")
dof = dgDofManager.newDGBlock(groups, tracer.law.getNbFields(), petsc)
tracerSolver = dgDIRK(tracer.law, dof, 1) 
tracerSolver.getNewton().setAtol(1.e-5)
tracerSolver.getNewton().setRtol(1.e-5)
tracerSolver.getNewton().setVerb(1)
#tracerSolver=dgERK(tracer.law,None,DG_ERK_22)

t = initialTime
elapsedTime = 0
for i in range (0,100) :
  normTracer = tracerSolver.iterate(tracer.solution,dt,t)
  normTracer = tracerSolver.getNewton().getFirstResidual()
  if (elapsedTime % 300 == 0) :
    Msg.Info("%d %s :" % (nbExport,printDate(t)))
    export.exportFunctionTxt(tracer.solution.getFunction(),"tracer",nbExport)
    export.exportFunctionPosQuad(tracer.solution.getFunction(),"tracer",nbExport)
    nbExport  = nbExport  + 1

  t = t + dt
  timeFunction.set(t)
  elapsedTime = elapsedTime + dt
  elapsedTimeFunction.set(elapsedTime)
