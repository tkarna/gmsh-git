from dgpy import *
from gmshpy import *
import ScheldtTidalRivers_distance1d as distance1d
import ScheldtTidalRivers_shape1d as shape1d
import ScheldtTidalRivers_export1d as export1d
import ScheldtTidalRivers_discharges1d as discharges1d
import ScheldtTidalRivers_smoothFunction as smooth1d
import time, math, os, os.path, sys, datetime, calendar
from slimRiver1d import *

implicitTs=True
def printTime (t) :
  return "%dh%02d'%02d\"" % (t/3600,  (t%3600)/60,  t%60)
def printDate (t):
  date=time.gmtime(t)
  return "%02d/%02d/%04d %02dh%02d'%02d\""%(date[2], date[1], date[0], date[3], date[4], date[5])

elapsedTime=0
initialDate=datetime.datetime(1990,1,1,12,0,0)
initialTime=calendar.timegm(initialDate.timetuple())
timeFunction = functionConstant(initialTime)
elapsedTimeFunction=functionConstant([elapsedTime])

river = slimRiver1d(outputDirectory="outputScheldtTidalRivers")
river.loadMesh("ScheldtTidalRivers.geo",clscale=4)
groups = river.getGroups()
#-------------------------
# Coordinate
#-------------------------
x1d = distance1d.get(groups)
river.setDistance1d(x1d)

#-------------------------
# Hydro
#-------------------------
hydro = slimRiverHydro1d(river)

#-------------------------
# Hydro: Geometry
#-------------------------
riverNames=["OUDESCHELDE","DURME","ZEESCHELDE","RINGVAART","RUPEL","KLEINENETE","NETEAFLEIDING","BENEDENNETE","GROTENETE","BENEDENDIJLE","BOVENDIJLE","DIJLEAFLEIDING","ZENNEAFL","ZENNE","DENDER"]
smoothRiverData=30
shapeData=shape1d.riverShape("ScheldtData/shapes",riverNames,smoothRiverData)
shapeData.setFields(groups,x1d)
shapeData.squareChannelConnection(groups,"ZEESCHELDE",100,10)
altitudeData,widthData,sectionData=shapeData.get(groups,hydro.etaInterp)
hydro.setGeometry(altitudeData,widthData,sectionData)

#-------------------------
# Hydro: Initial conditions
#-------------------------
velocityInitial=functionConstant([0.])
bankAltitude1=functionExtractCompNew(altitudeData.getFunction(),2)
bankAltitude2=functionExtractCompNew(altitudeData.getFunction(),3)
bottomAltitude=functionExtractCompNew(altitudeData.getFunction(),1)
layer0=functionExtractCompNew(altitudeData.getFunction(),0)
def setElevationAltitudeBank(FCT,altitude):
  for i in range(0,FCT.size1()):
    bottom=altitude(i,1)
    bank=min(altitude(i,2),altitude(i,3))
    layer0=altitude(i,0)
    depthMax = bank-bottom
    FCT.set(i,0,max(min(bank-3,7),0))

elevationInitialFun = functionPython(1,setElevationAltitudeBank,[altitudeData.getFunction()])
elevationInitialSmooth = smooth1d.smooth(river,elevationInitialFun,nIteration=100,dt=50000)
altitudeInitial,widthInitial,sectionInitial = shapeData.get(groups,elevationInitialSmooth.getFunction())
hydro.setInitialCondition(sectionInitial,velocityInitial)

#-------------------------
# Hydro: Set solution
#-------------------------
hydro.setSolution()

#-------------------------
# Hydro: Supplementary terms1
#-------------------------
manning = functionConstant(0.025)
hydro.setManningFriction(manning)
okuboFactor=functionConstant(0.1)
diffusivity=functionC(river.libSlim1d,"okubo",1,[okuboFactor]);
hydro.setDiffusivity(diffusivity)
hydro.setSetup()

#-------------------------
# Hydro: Boundary conditions
#-------------------------

#DOWNSTREAM
etaSinus=functionC(river.libSlim1d,"etaSinus",1,[elapsedTimeFunction])
startV = elevationInitialSmooth.getFunction()
stopV = functionConstant(0)
etaBnd=functionC(river.libSlim1d,"slope",1,[elapsedTimeFunction,startV,stopV])
altDown,widthDown,sectionDown=shapeData.get(groups,etaSinus)
hydro.bndForceSection(sectionDown,"downstreamZEESCHELDE")

#UPSTREAM
Q = discharges1d.ScheldtDischarges(timeFunction)
hydro.bndUpstreamDischarge(Q.Q_OudeSchelde,"Ghent-upstreamOUDESCHELDE")
hydro.bndUpstreamDischarge(Q.Q_Ringvaart,"Ghent-upstreamRINGVAART")
hydro.bndUpstreamDischarge(Q.Q_Durme,"upstreamDURME")
hydro.bndUpstreamDischarge(Q.Q_Dender,"upstreamDENDER")
hydro.bndUpstreamDischarge(Q.Q_GroteNete,"Itegem-upstreamGROTENETE")
hydro.bndUpstreamDischarge(Q.Q_KleineNete,"Grobbendonk-upstreamKLEINENETE")
hydro.bndUpstreamDischarge(Q.Q_Dijle,"Haacht-upstreamBOVENDIJLE")
hydro.bndUpstreamDischarge(Q.Q_Zenne,"Eppegem-upstreamZENNE")

#-------------------------
# Initial exports
#-------------------------
export = export1d.export(groups, hydro.solution, hydro.width, x1d.getFunction(), river.outputDirectory)
export.exportFunctionTxt(layer0,"layer0",0)
export.exportFunctionTxt(bankAltitude1,"bankAltitude1",0)
export.exportFunctionTxt(bankAltitude2,"bankAltitude2",0)
export.exportFunctionTxt(bottomAltitude,"bottomAltitude",0)
export.exportFunctionTxt(hydro.section,"section",0)
export.exportFunctionTxt(hydro.width,"width",0)
export.exportFunctionTxt(hydro.etaF,"eta",0)
export.exportFunctionTxt(shapeData.widthDataLevel.getFunction(),"widthDataLevel",0)
export.exportFunctionTxt(shapeData.altitudeData.getFunction(),"altitudeData",0)
export.exportFunctionTxt(elevationInitialSmooth.getFunction(),"etaInitialSmooth",0)
export.exportFunctionTxt(elevationInitialFun,"etaInitialFun",0)
etaDof = dgDofContainer(groups, 1)
#-------------------------
# Iterate
#-------------------------
integratorS=dgFunctionIntegrator(groups,hydro.section)
ISv = fullMatrixDouble(1,1)
Sv=[]
usquare = functionC(river.libSlim1d,"square",1,[hydro.velocity])
integratoruu=dgFunctionIntegrator(groups,usquare)
Iuuv = fullMatrixDouble(1,1)
uuv=[]
integratorS.compute(ISv)
integratoruu.compute(Iuuv)

if implicitTs:
  dt = 600
  petsc = linearSystemPETScBlockDouble()
  petsc.setParameter("petscOptions",  "-pc_type bjacobi -sub_pc_type ilu")
  dof = dgDofManager.newDGBlock(groups, hydro.law.getNbFields(), petsc)
  hydroSolver = dgDIRK(hydro.law, dof, 2) 
  hydroSolver.getNewton().setAtol(1.e-5)
  hydroSolver.getNewton().setRtol(1.e-5)
  hydroSolver.getNewton().setVerb(1)
else:
  dt = 0.01
  hydroSolver=dgERK(hydro.law,None,DG_ERK_22)

nbExport = 0
t = initialTime
elapsedTime = 0
hydroSolver.getNewton().setVerb(100)
Msg.Info("%d %s : int(S)dx=%.4e int(u*u)dx=%.4e" % (nbExport,printDate(t),ISv(0,0),Iuuv(0,0)))
for i in range (0,10000):
  normHydro = hydroSolver.iterate(hydro.solution,dt,t)
  if implicitTs:
    normHydro = hydroSolver.getNewton().getFirstResidual()
  if (elapsedTime % 600 == 0) :
    nbExport  = nbExport  + 1
    integratorS.compute(ISv)
    integratoruu.compute(Iuuv)
    Msg.Info("%d %s : int(S)dx=%.4e int(u*u)dx=%.4e" % (nbExport,printDate(t),ISv(0,0),Iuuv(0,0)))
    export.exportFunctionTxt(hydro.etaF,"eta",nbExport)
    export.exportFunctionTxt(hydro.velocity,"velocity",nbExport)
    etaDof.interpolate(hydro.etaF)
    export.exportPoint(etaDof.getFunction(),"Melle_eta","ZEESCHELDE",6900,elapsedTime)
    export.exportMsh("test-%05i" % nbExport, elapsedTime, nbExport)
    Sv.append(ISv(0,0))
    uuv.append(Iuuv(0,0))

  t = t + dt
  timeFunction.set(t)
  elapsedTime = elapsedTime + dt
  elapsedTimeFunction.set(elapsedTime)
