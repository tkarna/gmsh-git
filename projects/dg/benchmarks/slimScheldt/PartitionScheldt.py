from dgpy import *
from termcolor import colored
from math import *
import time, math, os, os.path, sys, datetime, calendar
from ScheldtEstuary_bath2d import *
from ScheldtEstuary_transect2d import *
import toStereo
#from scipy.weave import blitz

if (Msg.GetCommRank() == 0):
  functionC.buildLibraryFromFile ("Slim2d.cc", "libSlim2d.so");

Msg.Barrier()
libSlim2d = "libSlim2d.so"

meshname = "full2"

if(not os.path.exists(meshname+".msh")):
  os.system("cp mesh-NWECS+ScheldtEstuary/"+meshname+".msh .") 

if(not os.path.exists(meshname+"-Stereo.msh")): 
  m2 = GModel()
  m2.load(meshname + ".msh")
  toStereo.projectMesh(m2)
  m2.save(meshname+"-Stereo.msh")

#if(not os.path.exists(meshname+"-lonLat.msh")): 
#  m3 = GModel()
#  m3.load(meshname+".msh")
#  toLonLat.projectMesh(m3, 1.5)#1.5 is the factor to scale Y-axis (plotting purposes)
#  m3.save(meshname+"-lonLat.msh")


model = GModel()
model.load (meshname+'-Stereo.msh')

pOpt = meshPartitionOptions()
pOpt.setNumOfPartitions(int(sys.argv[1]))


outputDir = "output/"
try : 
  os.mkdir(outputDir);
except: 0;

os.system("rm -f %s/*"%outputDir)

stereo=dgSpaceTransformSpherical(6371220.)
groups = dgGroupCollection(model, 2, 1, stereo)
claw = dgConservationLawShallowWater2d()
solution = dgDofContainer(groups, claw.getNbFields())
XYZ = groups.getFunctionCoordinates();

#bathymetry
bathOriginal = bathymetry("bath",outputDir,groups, XYZ, minV=-1, maxV=3e3)
cropOnTag(groups, bathOriginal, 20, 1e3, "Shelf Break")
cropOnTag(groups, bathOriginal, 10, 1e3, "Bretagne and Basse Normandie")
bathEffective = smoothBathymetry(groups,bathOriginal, minV=-1, maxV=3e3)
#bathEffective.exportMsh("batheffective")

#SW2D
#claw = dgConservationLawShallowWater2d()
#solution = dgDofContainer(groups, claw.getNbFields())
initialSolution = functionConstant([0., 0., 0.])
solution.interpolate(initialSolution)
Coriolis = functionC(libSlim2d, "coriolis", 1, [XYZ])
claw.setCoriolisFactor(Coriolis)
claw.setUpwindFactorRiemann(1.);

R=6371220.
claw.setIsSpherical(R)

nu0 = functionC(libSlim2d,"smagorinsky",1,[solution.getFunctionGradient()])
nu = functionC(libSlim2d,"diffusivityStereo",1,[nu0, XYZ])
#claw.setDiffusivity(nu)

claw.setBathymetry(bathEffective.getFunction())
claw.setBathymetryGradient(bathEffective.getFunctionGradient())
#solution.exportFunctionMsh(claw.getBathymetry(),    "clawbath1",    0,    0,    '')
claw.setMovingBathWettingDrying(2)
#solution.exportFunctionMsh(claw.getBathymetry(),    "clawbath2",    0,    0,    '')

ld=functionConstant([1e-6])
#claw.setLinearDissipation(ld)
#claw.setSource(ld)

bath = claw.getBathymetry()
elevation = claw.getElevation()

#transectDof = transect(groups,XYZ)
#manning = functionC(libSlim2d, "manningScheldt",1, [transectDof.getFunction()])
#transectDof.exportFunctionMsh(manning,"manning")
manning = functionConstant(0.03)
Cd = functionC(libSlim2d, "bottomDrag", 1, [solution.getFunction(), bath, manning])
claw.setQuadraticDissipation(Cd)


mL=int(sys.argv[2])
mLmax=mL
hydroSolver=dgMultirateERK(groups,  claw,  ERK_22_C)
dt=hydroSolver.splitGroupsForMultirate(mL,  solution,  [solution, bathEffective, bathOriginal])
dt=dt
solution.exportGroupIdMsh()
SU=hydroSolver.speedUp()

if(Msg.GetCommRank() == 0):
  print 'SU=',   SU,    'DT=',   dt


hydroSolver.printMultirateInfo()

algo=5
hydroSolver.computeMultiConstrainedPartition(pOpt,   algo)
PartitionMesh(model,  pOpt)
hydroSolver.printPartitionInfo(int(sys.argv[1]))
model.save(meshname + '_%d_%d-Stereo.msh'%(int(sys.argv[1]), int(sys.argv[2])))
Msg.Exit(0)

