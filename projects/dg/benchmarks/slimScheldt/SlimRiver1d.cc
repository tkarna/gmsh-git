#include "fullMatrix.h"
#include "dgGroupOfElements.h"
#include "MElement.h"
#include "MEdge.h"
#include "math.h"
#include "function.h"
extern "C" {
  double pow2(double value){
  	return value*value;
  }
	void inverseSign(dataCacheMap *m,fullMatrix<double> &f, fullMatrix<double> &f2){
    for (size_t i=0; i<f.size1(); i++){
      f.set(i,0,-f2(i,0)); 
    }
	}
	void gaussian(dataCacheMap *,fullMatrix<double> &f,fullMatrix<double> &xyz){
    for (size_t i=0; i<xyz.size1(); i++){
      //f.set(i,0,1*exp(-0.5*pow2(xyz(i,0)+.2e4)/pow2(.2e4))); 
      f.set(i,0,1*exp(-0.5*pow2(xyz(i,0)-0.5e5)/pow2(0.5e4))); 
      //f.set(i,0,1*exp(-0.5*pow2(xyz(i,0))/pow2(1e4))); 
    }
	}
	void etaSinus(dataCacheMap *,fullMatrix<double> &f,fullMatrix<double> &time){
    for (size_t i=0; i<f.size1(); i++){
      f.set(i,0,1.8*sin(2*M_PI*time(i,0)/44714)); 
    }
	}
	void slope(dataCacheMap *,fullMatrix<double> &f,fullMatrix<double> &time,fullMatrix<double> startV, fullMatrix<double> stopV){
    for (size_t i=0; i<f.size1(); i++){
      f.set(i,0,std::max(stopV(i,0),startV(i,0)+(stopV(i,0)-startV(i,0))/86400*time(i,0))); 
    }
	}
	void okubo(dataCacheMap *m,fullMatrix<double> &f,fullMatrix<double> &factor){
    double dx = m->getElement()->getEdge(0).length();
    for (size_t i=0; i<f.size1(); i++){
      f.set(i,0,factor(i,0)*2.0551e-4*pow(dx,1.15)); 
    }
	}
	void getVelocityFromDischarge(dataCacheMap *,fullMatrix<double> &f,fullMatrix<double> &Q, fullMatrix<double> &S){
    for (size_t i=0; i<f.size1(); i++){
      f.set(i,0,Q(i,0)/S(i,0)); 
    }
	}
	void getMinusVelocityFromDischarge(dataCacheMap *,fullMatrix<double> &f,fullMatrix<double> &Q, fullMatrix<double> &S){
    for (size_t i=0; i<f.size1(); i++){
      f.set(i,0,-Q(i,0)/S(i,0)); 
    }
	}
	void chezyManningDissipation(dataCacheMap *,fullMatrix<double> &diss,fullMatrix<double> &section, fullMatrix<double> &width, fullMatrix<double> &manning){
    for (size_t i=0; i<diss.size1(); i++){
      double w = width(i,0);
      double s = section(i,0);
      double Heff=s/w;
      double chezy=pow(s/(w+2*Heff),1./6.)/manning(i,0);
      double g=9.81;
      diss.set(i,0,g/(pow2(chezy)*Heff)); 
    }
	}
	void merge2scalars(dataCacheMap *,fullMatrix<double> &uv,fullMatrix<double> &u, fullMatrix<double> &v){
    for (size_t i=0; i<uv.size1(); i++){
      uv.set(i,0,u(i,0)); 
      uv.set(i,1,v(i,0)); 
    }
	}
	void widthData(dataCacheMap *m,fullMatrix<double> &w,fullMatrix<double> &bath, fullMatrix<double> &eta, fullMatrix<double> &x){
    for (size_t i=0; i<bath.size1(); i++){
      w.set(i,0,(100+(100-50)/bath(i,0)*eta(i,0))); 
      //w.set(i,0,100.); 
    }
	}
	void square(dataCacheMap *m,fullMatrix<double> &ww,fullMatrix<double> &w){
    for (size_t i=0; i<w.size1(); i++){
      ww.set(i,0,w(i,0)*w(i,0)); 
    }
	}
	void sectionData(dataCacheMap *m,fullMatrix<double> &s,fullMatrix<double> &bath, fullMatrix<double> &eta){
    for (size_t i=0; i<s.size1(); i++){
      s.set(i,0,(100*(eta(i,0)+bath(i,0))+(100-50)/bath(i,0)*(pow2(eta(i,0))-pow2(bath(i,0)))/2)  ); 
      //s.set(i,0, 100*(eta(i,0)+bath(i,0))); 
    }
	}
}
