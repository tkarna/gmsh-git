from dgpy import *
import os 

os.putenv("BATHFILE1", "NWECSData/bath/etopo/NWECS-3868.asc");
os.putenv("BATHFILE2", "ScheldtData/bath/scheldt_bath_delft_gimp_saeftinge_p50cm.bin");

CCode = """
#include "fullMatrix.h"
#include "function.h"
#include <stdlib.h>
#include "math.h"
#include "MElement.h"
#include "MEdge.h"

static double ox1, oy1, dx1, dy1;
static double ox2, oy2, dx2, dy2;
static int nx1, ny1;
static int nx2, ny2;
static bool init = false;
static double *data1 = NULL;
static double *data2 = NULL;
static double R = 6.371e6;

extern "C" {
void initialize() {
  double buf;
  size_t r;
  std::string filename1 = getenv("BATHFILE1");
  FILE* input = fopen(filename1.c_str(),"r");
  r = fscanf(input, "NCOLS        %d \\n",&nx1);
  r = fscanf(input, "NROWS        %d \\n",&ny1);
  r = fscanf(input, "XLLCENTER    %lf \\n",&ox1);
  r = fscanf(input, "YLLCENTER    %lf \\n",&oy1);
  r = fscanf(input, "CELLSIZE     %lf \\n",&dx1);
  dy1 = dx1;
  r = fscanf(input, "NODATA_VALUE %lf \\n",&buf);
  data1 = new double[nx1 * ny1];
  for (int j = 0; j < ny1; j ++){
    for (int i = 0; i < nx1; i++){
      double b;
      r = fscanf(input,"%lf",&b);
      data1[i*ny1 + ny1-j-1]=-b;
    }
  }
  fclose(input);

  std::string filename2 = getenv("BATHFILE2");
  input = fopen(filename2.c_str(), "r");
  r = fread (&ox2, sizeof(double), 1, input);
  r = fread (&oy2, sizeof(double), 1, input);
  r = fread (&buf, sizeof(double), 1, input);
  r = fread (&dx2, sizeof(double), 1, input);
  r = fread (&dy2, sizeof(double), 1, input);
  r = fread (&buf, sizeof(double), 1, input);
  r = fread (&nx2, sizeof(int), 1, input);
  r = fread (&ny2, sizeof(int), 1, input);
  r = fread (&buf, sizeof(int), 1, input);
  data2 = new double[nx2*ny2];
  r = fread (data2, sizeof(double), nx2*ny2, input);
  fclose(input);

  buf=0;
}
double pow2(double x){
  return x*x;
}
void convertStereo2Lonlat(double xi, double zeta, double &lon, double &lat, double factor=180/M_PI){
  double x = 4*R*R*xi/(4*R*R +xi*xi + zeta*zeta);
  double y = 4*R*R*zeta/(4*R*R +xi*xi + zeta*zeta);
  double z = R *(4*R*R-xi*xi-zeta*zeta)/(4*R*R+xi*xi + zeta*zeta);
  lon = atan2(y,x)*factor;
  lat = asin(z/R)*factor;
}
double interpolate(double *_data, double x, double y, double _ox, double _oy, double _dx, double _dy, int _nx, int _ny){
  double rx = std::min(std::max((x-_ox),0.),_nx*_dx);
  double ry = std::min(std::max((y-_oy),0.),_ny*_dy);
  int iX =  floor(rx/_dx);
  int iY =  floor(ry/_dy);
  double alpha = (rx - iX*_dx) / _dx;
  double beta = (ry - iY*_dy) / _dy;
  double b = (1-alpha)*(1-beta)*_data[iX*_ny+iY]
             +alpha*(1-beta)*_data[(iX+1)*_ny+iY] 
             +alpha*beta*_data[(iX+1)*_ny+(iY+1)] 
             +(1-alpha)*beta*_data[iX*_ny+(iY+1)];
  return b;
}
double distOnSphere(double lon0, double lon1, double lat0, double lat1){
   return 2*R*asin(sqrt(pow2(sin((lat1-lat0)/2))+cos(lat0)*cos(lat1)*pow2(sin((lon1-lon0)/2))));
}
void bath(dataCacheMap *,fullMatrix<double> &bath,fullMatrix<double> &XYStereo){
  if(!init){
    initialize();
    init=true;
  }
  for (size_t i=0; i<bath.size1(); i++){
    double lonDeg, latDeg;
    convertStereo2Lonlat(XYStereo(i,0), XYStereo(i,1), lonDeg, latDeg);
    double b;
   if(lonDeg >= ox2 && lonDeg <= (ox2+(nx2-1)*dx2) && latDeg >= oy2 && latDeg <= (oy2+(ny2-1)*dy2)){
      b = interpolate(data2, lonDeg, latDeg, ox2, oy2, dx2, dy2, nx2, ny2);
    }
    else {
      b = std::max(10.,interpolate(data1, lonDeg, latDeg, ox1, oy1, dx1, dy1, nx1, ny1));
    }
    bath.set(i,0,b); 
  }
}
void kappaDiffBath(dataCacheMap *m, fullMatrix<double> &diff, fullMatrix<double> &XYZ){
  double dx1 = m->getElement()->getEdge(0).length();
  double dx2 = m->getElement()->getEdge(1).length();
  double dx3 = m->getElement()->getEdge(2).length();
  double dx = std::max(std::max(dx1,dx2),dx3);
  double lon0[2] = {-4.77*M_PI/180, 4.8*M_PI/180};
  double lat0[2] = {48.32*M_PI/180, 61.3*M_PI/180};
  double lon1, lat1;
  for (size_t i=0; i<diff.size1(); i++){
    convertStereo2Lonlat(XYZ(i,0), XYZ(i,1), lon1, lat1, 1.0);
    double expo = 0;
    for (int k=0; k<2; k++){
      double x = distOnSphere(lon0[k], lon1, lat0[k], lat1);
      expo += exp(-0.5*(x*x)/pow2(1000e3)); 
    }
    double factor = std::max(std::min(1/(1-expo),100.),0.);
    double kappa = dx*dx/60 ;
    diff.set(i,0,kappa); 
  }
}

}

"""
tmpLib = "ScheldtEstuary_bath2d_lib.so"
if (Msg.GetCommRank()==0):
  functionC.buildLibrary (CCode, tmpLib);
Msg.Barrier()

def bathymetry(name, outputDir, groups, XYZ, minV=-1e12, maxV=1e12):
  bath = dgDofContainer(groups, 1);
  if(os.path.exists(outputDir+"%s_COMP_0.msh"%name)):
    Msg.Info('.... Import Default Bathymetry')
    bath.importMsh(outputDir + name)
  else: 
    bathS = functionC(tmpLib, "bath", 1, [XYZ])
    bath.interpolate(bathS);
    Msg.Info(" Interpolate Bath")
    bath.exportMsh(outputDir+name, 0, 0);
    bath.bezierCrop(minV, maxV)
  return bath

def makeContinuous(groups, bath):
  bath.exportMsh("output/bathMakeConitinous")
  sys = linearSystemPETScDouble()
  dof = dgDofManager.newCG (groups, 1, sys)
  projector = L2ProjectionContinuous(dof)
  projector.apply(bath, bath.getFunction())

def smoothBathymetry(groups,bath, minV=-1e12, maxV=1e12):
  Msg.Info(" Smooth Bath")
  bathSmooth = dgDofContainer(groups, 1);
  bathSmooth.interpolate(bath.getFunction());
  XYZ = groups.getFunctionCoordinates();
  nuDiffBath = functionC(tmpLib,"kappaDiffBath",1,[XYZ]);
  dlaw = dgConservationLawAdvectionDiffusion.diffusionLaw(nuDiffBath)
  dlaw.setIsSpherical(6371000.)
  dlaw.addBoundaryCondition("Coast", dlaw.new0FluxBoundary())
  dlaw.addBoundaryCondition("none", dlaw.new0FluxBoundary())
  dlaw.addBoundaryCondition("Ghent-Terneuzen Canal", dlaw.new0FluxBoundary())
  dlaw.addBoundaryCondition("Hansweert Canal", dlaw.new0FluxBoundary())
  dlaw.addBoundaryCondition("Bath Canal", dlaw.new0FluxBoundary())
  dlaw.addBoundaryCondition("Zandvlietsluis North", dlaw.new0FluxBoundary())
  dlaw.addBoundaryCondition("Zandvlietsluis South", dlaw.new0FluxBoundary())
  dlaw.addBoundaryCondition("BoudewijnSluis North", dlaw.new0FluxBoundary())
  dlaw.addBoundaryCondition("BoudewijnSluis South", dlaw.new0FluxBoundary())
  dlaw.addBoundaryCondition("Kallo Sluis", dlaw.new0FluxBoundary())
  dlaw.addBoundaryCondition("Upstream", dlaw.new0FluxBoundary())
  dlaw.addBoundaryCondition("Meuse", dlaw.new0FluxBoundary())
  dlaw.addBoundaryCondition("Rhine", dlaw.new0FluxBoundary())
  dlaw.addBoundaryCondition("Thames", dlaw.new0FluxBoundary())
  dlaw.addBoundaryCondition("Seine", dlaw.new0FluxBoundary())
  dlaw.addBoundaryCondition("Shelf Break", dlaw.new0FluxBoundary())
  dlaw.addBoundaryCondition("Baltic", dlaw.new0FluxBoundary())
  dlaw.addBoundaryCondition("Bretagne and Basse Normandie", dlaw.new0FluxBoundary())

  sys = linearSystemPETScDouble()
  dof = dgDofManager.newCG (groups, 1, sys)
  implicitEuler = dgDIRK(dlaw, dof)
  implicitEuler.iterate (bathSmooth, 1, 0)
  makeContinuous(groups,bathSmooth)
  bathSmooth.bezierCrop(minV, maxV)
  bathSmooth.exportFunctionMsh(nuDiffBath,"output/nuDiffBath")
  return bathSmooth

def cropOnTag(groups, bath, minV, maxV, tag):
  nGroupFaces = groups.getNbFaceGroups()
  for iFG in range(nGroupFaces):
    groupFace = groups.getFaceGroup(iFG)
    if( groupFace.physicalTag() == tag ):
      group = groupFace.elementGroup(0)
      proxy = bath.getGroupProxy(group);
      for iInterface in range(groupFace.size()):
        iE = groupFace.elementId(iInterface,0)
        iVs = groupFace.closure(iInterface,0)
        for iV in iVs:
          bathValue = proxy(iV, iE)
          bathValue = max(minV,min(maxV,bathValue))
          proxy.set(iV, iE, bathValue)
  makeContinuous(groups, bath)


