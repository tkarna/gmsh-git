from dgpy import *
import matplotlib.pyplot as matplot
import matplotlib.animation as animation
from matplotlib.lines import Line2D
import os.path
from numpy import *

def readFile(filename):
  fileTemp=open(filename,"r")
  temp=fileTemp.readline().split()
  fileTemp.close()
  n=len(temp)-1
  sol=open(filename,"r").read()
  sol=sol.split()
  x=[]
  f=[]
  N=1+n;
  for i in range(0,len(sol),2*N):
    x.append(float(sol[i]))
    x.append(float(sol[N+i]))
    x.append(nan)
    fvelem1=[]
    fvelem2=[]
    fvelem3=[]
    for j in range(0,n):
      fvelem1.append(float(sol[i+1+j]))
      fvelem2.append(float(sol[N+i+1+j]))
      fvelem3.append(nan)
    f.append(fvelem1)
    f.append(fvelem2)
    f.append(fvelem3)
  x=array(x)
  f=array(f)
  return x,f

def getMinMax(listFiles):
  minV=1e16
  maxV=-1e16
  for i in range(0,len(listFiles)):
    x,f=readFile(listFiles[i])
    f2=array(f[range(0,len(f),3)])
    f2=array([f2,f[range(1,len(f),3)]])
    minv=f2.min()
    maxv=f2.max()
    minV=minimum(minv,minV)
    maxV=maximum(maxv,maxV)
  return minV,maxV

class MoviePlot(animation.TimedAnimation):
    def __init__(self,listFiles):
        self.listFiles=listFiles
        fig = matplot.figure()
        ax1 = fig.add_subplot(1, 1, 1)
        [x1,f1]=readFile(listFiles[0])
        minV,maxV=getMinMax(listFiles)
        print ("min = ",minV, "max = ", maxV)
        self.nV=len(f1[0])
        self.line=[]
        for k in range(len(f1[0])):
          self.line.append(Line2D([], [], color='blue', linewidth=2))
          ax1.add_line(self.line[-1])
        ax1.set_xlim(x1[0], x1[-2])
        ax1.set_ylim(minV, maxV)
        animation.TimedAnimation.__init__(self, fig, interval=200, blit=True)
        matplot.show()

    def _draw_frame(self, framedata):
        i = framedata
        artists=[]
        [x1,f1]=readFile(self.listFiles[i])
        if self.nV==1:
          self.line[0].set_data(x1,f1)
        else:
          for k in range(self.nV):
            self.line[k].set_data(x1,f1[k][:])

        self._drawn_artists = self.line

    def new_frame_seq(self):
        return iter(range(len(listFiles)))

    def _init_draw(self):
        for l in self.line:
            l.set_data([], [])


class export:
  def __init__(self, groups, solution, width, x, directory):
    self.groups = groups
    self.solution =solution 
    self.width = width
    self.directory = directory
    self.distance = functionExtractCompNew(x,1) 
    self.x1d = functionExtractCompNew(x,0) 
  def movie(self,filename,id0,id1):
    listFiles=[]
    for i in range(id0,id1+1):
      listFiles.append("%s/%s_%05d.txt"%(self.directory,filename,i))
    ani = MoviePlot(listFiles)
  
  def exportMsh(self, filename,t,i):
    self.solution.exportMsh("%s/%s"%(self.directory,filename),t,i)

  def exportFunctionMsh(self, fun,filename,t,i):
    self.solution.exportFunctionMsh(fun,"%s/%s"%(self.directory,filename),t,i)

  def exportPoint(self,evalF,filename,riverTag,distanceFromMouth,time):
    groups = self.groups
    cacheMap=dataCacheMap(dataCacheMap.NODE_MODE,groups)
    cacheMap.setSolutionFunction(self.solution.getFunction(),self.solution.getFunctionGradient())
    xT=0
    yT=0
    zT=0
    for iG in range(groups.getNbElementGroups()):
      group = groups.getElementGroup(iG)
      if(group.getPhysicalTag()==riverTag):
        break

    cacheMap.setGroup(group)
    xEval = cacheMap.get(self.x1d)
    nElem=group.getNbElements()
    for iE in range(group.getNbElements()):
      elem = group.getElement(iE)
      cacheMap.setElement(iE)
      xVal=xEval.get()
      if (xVal(0,0)<=distanceFromMouth and xVal(1,0)>=distanceFromMouth):
        edge = elem.getEdge(0)
        nodes = [edge.getVertex(0), edge.getVertex(1)]
        xEdge = [nodes[0].x(), nodes[1].x()]
        yEdge = [nodes[0].y(), nodes[1].y()]
        zEdge = [nodes[0].z(), nodes[1].z()]
        xi = (xVal(1,0)-distanceFromMouth)/edge.length()
        xT = xi * xEdge[0] + (1-xi) * xEdge[1]
        yT = xi * yEdge[0] + (1-xi) * yEdge[1]
        zT = xi * zEdge[0] + (1-xi) * zEdge[1]
      
    evalFun = dgFunctionEvaluator(groups, evalF, self.solution)
    value  = fullMatrixDouble(1,1)
    evalFun.compute(xT,yT,zT,value)
    filenameTotal = self.directory+"/"+filename+"_ts.txt"
    if (os.path.exists(filenameTotal)):
      tsFile = open(filenameTotal,"a+")
    else:
      tsFile = open(filenameTotal,"w")
    tsFile.write("%.16e %.16e\n" % (time,value.get(0,0)))
    tsFile.close()

  def exportFunctionPosQuad(self, fun, name, index):
    groups = self.groups
    solution = self.solution
    width = self.width
    f=open("%s/%s_%05d_quad.pos" % (self.directory,name,index), "w");
    f.write("View \" name \" {\n")
    nGroups=groups.getNbElementGroups()
    nM=dgNeighbourMap(groups)
    for iG in range(0,nGroups):
      cacheMap=dataCacheMap(dataCacheMap.NODE_MODE,groups) 
      cacheMap.setSolutionFunction(solution.getFunction(),solution.getFunctionGradient())
      funEval = cacheMap.get(fun)
      widthEval = cacheMap.get(width)
      group=groups.getElementGroup(iG)
      cacheMap.setGroup(group)
      nElem=group.getNbElements()
      for iE in range(0,nElem):
        neighbours_down=nM.getNeighbours(iG,iE,0)
        neighbours_up=nM.getNeighbours(iG,iE,1)
        cacheMap.setElement(iE)
        funVal=funEval.get()
        widthVal=widthEval.get()
        elem = group.getElement(iE)
        edge = elem.getEdge(0)
        point = []
        for iNode in range(0,funVal.size1()):
          vertex = edge.getVertex(iNode)
          point.append(array([vertex.x(),vertex.y(),vertex.z()]))
        tangent = []
        if(point[0][2]==0):
          normal= point[1]-point[0]
          t = array([-normal[1],normal[0],0])
        else:
          t=cross(point[1]-point[0],(point[0]+point[1])/2)
        t=t/sqrt(dot(t,t))
        tangent.append(t)
        tangent.append(t)
        if (len(neighbours_down)>0 and neighbours_down[0][0] == iG):
          iGdown = neighbours_down[0][0]
          iEdown = neighbours_down[0][1]
          group_down=groups.getElementGroup(iGdown)
          elem_down = group.getElement(iEdown)
          edge_down = elem_down.getEdge(0)
          vertex_down = edge_down.getVertex(1)
          point_down=[]
          for iNode in range(0,2):
            vertex_down = edge_down.getVertex(iNode)
            point_down.append(array([vertex_down.x(),vertex_down.y(),vertex_down.z()]))
          if point_down[0][2]==0:
            normal = point_down[1]-point_down[0]
            tangent_down=array([-normal[1],normal[0],0])
          else:
            tangent_down=cross(point_down[1]-point_down[0],(point_down[0]+point_down[1])/2)
          tangent_down=tangent_down/sqrt(dot(tangent_down,tangent_down))
          tangent[0]=(tangent[0]+tangent_down)/2
        if(len(neighbours_up)>0 and neighbours_up[0][0] == iG):
          iGup = neighbours_up[0][0]
          iEup = neighbours_up[0][1]
          group_up=groups.getElementGroup(iGup)
          elem_up = group.getElement(iEup)
          edge_up = elem_up.getEdge(0)
          point_up=[]
          for iNode in range(0,2):
            vertex_up = edge_up.getVertex(iNode)
            point_up.append(array([vertex_up.x(),vertex_up.y(),vertex_up.z()]))
          if point_up[0][2]==0:
            normal = point_up[1]-point_up[0]
            tangent_up=array([-normal[1],normal[0],0])
          else:
            tangent_up=cross(point_up[1]-point_up[0],(point_up[0]+point_up[1])/2)
          tangent_up=tangent_up/sqrt(dot(tangent_up,tangent_up))
          tangent[1]=(tangent[1]+tangent_up)/2
          
        f.write("SQ(%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f){%f,%f,%f,%f};\n" % (   
        point[0][0]  + tangent[0][0] * widthVal(0,0) / 2, \
        point[0][1]  + tangent[0][1] * widthVal(0,0) / 2, \
        point[0][2]  + tangent[0][2] * widthVal(0,0) / 2, \
        point[0][0]  - tangent[0][0] * widthVal(0,0) / 2, \
        point[0][1]  - tangent[0][1] * widthVal(0,0) / 2, \
        point[0][2]  - tangent[0][2] * widthVal(0,0) / 2, \
        point[1][0]  - tangent[1][0] * widthVal(1,0) / 2, \
        point[1][1]  - tangent[1][1] * widthVal(1,0) / 2, \
        point[1][2]  - tangent[1][2] * widthVal(1,0) / 2, \
        point[1][0]  + tangent[1][0] * widthVal(1,0) / 2, \
        point[1][1]  + tangent[1][1] * widthVal(1,0) / 2, \
        point[1][2]  + tangent[1][2] * widthVal(1,0) / 2, \
        funVal(0,0), funVal(0,0), funVal(1,0), funVal(1,0)))
    f.write("};")
    f.close()          


  def exportFunctionTxt(self, fun, name, index):
    groups = self.groups
    solution = self.solution
    dist = self.distance
    f=open("%s/%s_%05d.txt" % (self.directory,name,index), "w");
    nGroups=groups.getNbElementGroups()
    cacheMap=dataCacheMap(dataCacheMap.NODE_MODE,groups) #ces 2 lignes etaient dans la boucle suivante avant...
    cacheMap.setSolutionFunction(solution.getFunction(),solution.getFunctionGradient())
    for iG in range(0,nGroups):
      funEval = cacheMap.get(fun)
      xEval = cacheMap.get(dist)
      group=groups.getElementGroup(iG)
      cacheMap.setGroup(group)
      nElem=group.getNbElements()
      for iE in range(0,nElem):
        cacheMap.setElement(iE)
        funVal=funEval.get()
        xVal=xEval.get()
        for iNode in range(0,funVal.size1()):
          f.write("%16f " % xVal(iNode,0))
          for iComp in range(0,funVal.size2()):
            f.write("%16f" % funVal(iNode,iComp))
          f.write("\n")
    f.close()          

  def plotFunction(self, name, index , show=True):
    filename=("%s/%s_%05d.txt" % (directory,name,index))
    [x,f]=getDG_XF(filename)
    matplot.plot(x,f)
    if show:
      matplot.show()


  def getDG_XF(self,filename):
    sol=open("%s/%s"%(self.directory,filename),"r").read()
    sol=sol.split()
    x=[]
    f=[]
    for i in range(0,len(sol),4):
      x.append(float(sol[i]))
      x.append(float(sol[i+2]))
      x.append(nan)
      f.append(float(sol[i+1]))
      f.append(float(sol[i+3]))
      f.append(nan)
    x=array(x)
    f=array(f)
    return x,f

  def getSLIM_XF(self,filename):
    sol=open("%s/%s"%(self.directory,filename),"r").read()
    sol=sol.split()
    x=[]
    f=[]
    for i in range(0,len(sol),4):
      x.append(float(sol[i+2]))
      x.append(float(sol[i]))
      x.append(nan)
      f.append(float(sol[i+3]))
      f.append(float(sol[i+1]))
      f.append(nan)
    x=array(x)
    f=array(f)
    return x,f

  def movie(name,i0,i1,fmin=-1,fmax=1):
    ani = SubplotAnimation1(name,i0,i1,fmin,fmax)
    #ani.save("anim.mp4")
    #matplot.show()
    #os.system("mplayer anim.mp4")
  



#def exportFunctionTxtEval(model, groups, solution, distance, fun, name, index):
#    
#    nEdges=model.getNumMeshElements()
#    f=open("%s_%05d.txt" % (name,index), "w");
#    evalDist = dgFunctionEvaluator(groups, distance, solution)
#    dist=fullMatrixDouble()
#    evalFun = dgFunctionEvaluator(groups, fun, solution)
#    val=fullMatrixDouble()
#    for i in range(1,nEdges+1):
#      e=model.getMeshElementByTag(i)
#      if(e.getDim()==1):
#        v=e.getVertex(0)
#        evalDist.compute(v.x(),v.y(), v.z(), dist)
#        evalFun.compute(v.x(),v.y(), v.z(), val)
#        f.write("%16f %16f\n" % (dist(0,0), val(0,0)) );
#        v=e.getVertex(1)
#        evalDist.compute(v.x(),v.y(), v.z(), dist)
#        evalFun.compute(v.x(),v.y(), v.z(), val)
#        f.write("%16f %16f\n" % (dist(0,0), val(0,0)) );
#    f.close()

#def openDGWithPyPlot(filename1):
#  x1,f1=getDG_XF(filename1)
#  print len(x1)
#  for i in range(0,len(x1),2):
#    xx1=[x1[i],x1[i+1]];
#    ff1=[f1[i],f1[i+1]];
#    matplot.plot(xx1,ff1,'-xb')

#def openSLIMWithPyPlot(filename2):
#  x2,f2=getSLIM_XF(filename2)
#  print len(x2)
#  for i in range(0,len(x2),2):
#    xx2=[x2[i],x2[i+1]];
#    ff2=[f2[i],f2[i+1]];
#    matplot.plot(xx2,ff2,'-or')

#def computeDiff(f1,f2):
#  f1=array(f1)
#  f2=array(f2)
#  normF=linalg.norm(f2-f1)
#  maxDiffF=max(f2-f1)
#  return normF, maxDiffF
