from dgpy import *
from termcolor import colored
from math import *
import time, math, os, os.path, sys, datetime, calendar
from ScheldtEstuary_bath2d import *
from ScheldtEstuary_transect2d import *
import toStereo
#from scipy.weave import blitz

listArchive = ["mesh-NWECS+ScheldtEstuary","NWECSData","ScheldtData","shapePreproTidalRivers"]
for filename in listArchive:
  if(os.path.exists(filename) == 0):
    print "downloading ",filename
    os.system("curl -O --ftp-port - ftp://braque.mema.ucl.ac.be/slimdata/dgSlimScheldt/%s.zip"%filename)
    os.mkdir(filename)
    fzip = zipfile.ZipFile(filename+".zip")
    fzip.extractall(filename)

if (Msg.GetCommRank() == 0):
  functionC.buildLibraryFromFile ("Slim2d.cc", "libSlim2d.so");

Msg.Barrier()
libSlim2d = "libSlim2d.so"

meshname = "full2_"+str(Msg.GetCommSize())+"_"+str(int(sys.argv[1]))

model = GModel()
model.load (meshname+'-Stereo.msh')


def printTime (t) :
  return "%dh%02d'%02d\"" % (t/3600,  (t%3600)/60,  t%60)
def printDate (t):
  date=time.gmtime(t)
  return "%02d/%02d/%04d %02dh%02d'%02d\""%(date[2], date[1], date[0], date[3], date[4], date[5])

elapsedTime=0
initialDate=datetime.datetime(2000,1,1,12,0,0)
initialTime=calendar.timegm(initialDate.timetuple())
timeFunction = functionConstant(initialTime)
elapsedTimeFunction=functionConstant([elapsedTime])

outputDirMR = "output"+sys.argv[1]+'/'
outputDir = "output/"
try : 
  os.mkdir(outputDir);
except: 0;

try : 
  os.mkdir(outputDirMR);
except: 0;


os.system("rm -f %s/*"%outputDir)
os.system("rm -f %s/*"%outputDirMR)

# Python Functions
def printTime (t) :
  return "%dh%02d'%02d\"" % (t/3600,  (t%3600)/60,  t%60)
stereo=dgSpaceTransformSpherical(6371220.)
groups = dgGroupCollection(model, 2, 1, stereo)
claw = dgConservationLawShallowWater2d()
solution = dgDofContainer(groups, claw.getNbFields())
XYZ = groups.getFunctionCoordinates();
#bathymetry
bathOriginal = bathymetry("bath",outputDir,groups, XYZ, minV=-1, maxV=3e3)
cropOnTag(groups, bathOriginal, 20, 1e3, "Shelf Break")
cropOnTag(groups, bathOriginal, 10, 1e3, "Bretagne and Basse Normandie")
bathEffective = smoothBathymetry(groups,bathOriginal, minV=-1, maxV=3e3)

#SW2D
#claw = dgConservationLawShallowWater2d()
#solution = dgDofContainer(groups, claw.getNbFields())
initialSolution = functionConstant([0., 0., 0.])
solution.interpolate(initialSolution)
Coriolis = functionC(libSlim2d, "coriolis", 1, [XYZ])
claw.setCoriolisFactor(Coriolis)
claw.setUpwindFactorRiemann(1.);

R=6371220.
claw.setIsSpherical(R)

nu0 = functionC(libSlim2d,"smagorinsky",1,[solution.getFunctionGradient()])
nu = functionC(libSlim2d,"diffusivityStereo",1,[nu0, XYZ])
#claw.setDiffusivity(nu)

claw.setBathymetry(bathEffective.getFunction())
claw.setBathymetryGradient(bathEffective.getFunctionGradient())
claw.setMovingBathWettingDrying(2)

ld=functionConstant([1e-6])
#claw.setLinearDissipation(ld)
#claw.setSource(ld)

bath = claw.getBathymetry()
elevation = claw.getElevation()

#transectDof = transect(groups,XYZ)
#manning = functionC(libSlim2d, "manningScheldt",1, [transectDof.getFunction()])
#transectDof.exportFunctionMsh(manning,"manning")
manning = functionConstant(0.03)
Cd = functionC(libSlim2d, "bottomDrag", 1, [solution.getFunction(), bath, manning])
claw.setQuadraticDissipation(Cd)

mL=int(sys.argv[1])
mLmax=mL
hydroSolver=dgMultirateERK(groups,  claw,  ERK_22_C)
dt=hydroSolver.splitGroupsForMultirate(mL,  solution,  [solution, bathEffective, bathOriginal])
dt=dt
solution.exportGroupIdMsh()
hydroSolver.printMultirateInfo()
SU=hydroSolver.speedUp()

if(Msg.GetCommRank() == 0):
  print 'SU=',   SU,    'DT=',   dt

#Discharges
dischargeDir = "ScheldtData/discharges/";
claw.addBoundaryCondition("Coast", claw.newBoundaryWall())
claw.addBoundaryCondition("none", claw.newBoundaryWall())

Q_GTC = slimFunctionTemporalSerie(dischargeDir+"terneuzen_discharge_monthly_2000-2008.txt",timeFunction)
Q_GTC.usePeriodicYear()
claw.addBoundaryCondition("Ghent-Terneuzen Canal", claw.newForcedDischarge(solution, Q_GTC, "Ghent-Terneuzen Canal"))

Q_Bath = slimFunctionTemporalSerie(dischargeDir+"bath_discharge_monthly_1988-2008.txt",timeFunction)
Q_Bath.usePeriodicYear()
claw.addBoundaryCondition("Bath Canal", claw.newForcedDischarge(solution, Q_Bath, "Bath Canal"))

Q_DocksFull = slimFunctionTemporalSerie(dischargeDir+"docks_discharge_monthly_1988-2008.txt",timeFunction)
Q_DocksFull.usePeriodicYear()
Q_Docks = functionC(libSlim2d, "divideBy6", 1, [Q_DocksFull])
claw.addBoundaryCondition("Zandvlietsluis North", claw.newForcedDischarge(solution,Q_Docks,"Zandvlietsluis North" ))
claw.addBoundaryCondition("Zandvlietsluis South", claw.newForcedDischarge(solution,Q_Docks,"Zandvlietsluis South"))
claw.addBoundaryCondition("BoudewijnSluis North", claw.newForcedDischarge(solution,Q_Docks,"BoudewijnSluis North"))
claw.addBoundaryCondition("BoudewijnSluis South", claw.newForcedDischarge(solution,Q_Docks,"BoudewijnSluis South"))
claw.addBoundaryCondition("Kallo Sluis"         , claw.newForcedDischarge(solution,Q_Docks,"Kallo Sluis"))
claw.addBoundaryCondition("Hansweert Canal"     , claw.newForcedDischarge(solution,Q_Docks,"Hansweert Canal"))

claw.addBoundaryCondition("Upstream", claw.newBoundaryWall())

Q_Meuse = slimFunctionTemporalSerie(dischargeDir+"meuse_discharge_monthly_1971-2007.txt",timeFunction)
Q_Meuse.usePeriodicYear()
claw.addBoundaryCondition("Meuse",claw.newForcedDischarge(solution,Q_Meuse,"Meuse"))

Q_Rhine = slimFunctionTemporalSerie(dischargeDir+"rhine_discharge_monthly_1971-2007.txt",timeFunction)
Q_Rhine.usePeriodicYear()
claw.addBoundaryCondition("Rhine",claw.newForcedDischarge(solution,Q_Rhine,"Rhine"))

Q_Thames = slimFunctionTemporalSerie(dischargeDir + "thames_discharge_1980_2008",timeFunction)
claw.addBoundaryCondition("Thames", claw.newForcedDischarge(solution,Q_Thames,"Thames"))

Q_Seine = slimFunctionTemporalSerie(dischargeDir+"seine_discharge_1980_2008",timeFunction)
claw.addBoundaryCondition("Seine",claw.newForcedDischarge(solution,Q_Seine,"Seine"))
claw.addBoundaryCondition("Baltic", claw.newBoundaryWall())
claw.addBoundaryCondition("Bretagne and Basse Normandie", claw.newBoundaryWall())

lonLatDegrees = functionC(libSlim2d,"lonLatDegrees",3,[XYZ])
tideEta = slimFunctionTpxo("NWECSData/tpxo/hf.ES2008.nc","ha","hp","lon_z","lat_z")
tideEta.setCoordAndTimeFunctions(lonLatDegrees, timeFunction)
tideEta.useDofContainers(groups)
tideU = slimFunctionTpxo("NWECSData/tpxo/uv.ES2008.nc","Ua","up","lon_u","lat_u")
tideU.setCoordAndTimeFunctions(lonLatDegrees, timeFunction)
tideU.useDofContainers(groups)
tideV = slimFunctionTpxo("NWECSData/tpxo/uv.ES2008.nc","Va","vp","lon_v","lat_v")
tideV.setCoordAndTimeFunctions(lonLatDegrees, timeFunction)
tideV.useDofContainers(groups)
tideUV = functionC(libSlim2d,"merge2",2,[tideU, tideV])
tideUV_stereo = transformLonLat2Stereo(tideUV, XYZ, R)
tideuv = functionC(libSlim2d,"transport2velocity",3,[tideUV,bath,tideEta]);
tpxoTide = functionC(libSlim2d,"mergeEtaUV",3,[tideEta,tideuv]);
tpxo_u = functionExtractCompNew(tpxoTide,1)
tpxo_v = functionExtractCompNew(tpxoTide,2)
claw.addBoundaryCondition('Shelf Break',claw.newOutsideValueBoundary("Surface", tpxoTide))
uv_stereo = functionC(libSlim2d,"current",3,[solution.getFunction()])
uv_lonlat = transformStereo2LonLat(uv_stereo, XYZ, R)

currentFunction = functionC(libSlim2d,  "current",  3,  [function.getSolution()])
waterHeightFunction = functionC(libSlim2d,  "effectiveWaterHeight",  1,  [function.getSolution(), bathOriginal.getFunction()])
etaFunction = functionC(libSlim2d,  "effectiveEta",  1,  [function.getSolution(), bathOriginal.getFunction()])

nbExport = 0
elapsedTime = 0
nbIter = 0
t = initialTime
#Msg.Info("%d %s" % (nbExport,printDate(t)))
NN = int(pow(2,   mLmax-mL))
nbSteps = 1000000*NN
solution.exportFunctionMsh(currentFunction,   outputDirMR+"UV-%05d"%(nbExport),   t,   nbExport,   '')
solution.exportFunctionMsh(etaFunction,   outputDirMR+"E-%05d"%(nbExport),   t,   nbExport,   '')
solution.exportFunctionMsh(waterHeightFunction,   outputDirMR+"H-%05d"%(nbExport),   t,   nbExport,   '')
solution.exportFunctionMsh(claw.getBathymetry(),   outputDirMR+"bathN-%05d"%(nbExport),   t,   nbExport,   '')
for i in range (1,nbSteps+1):
  hydroSolver.iterate(solution,dt,t)
  t = t + dt
  timeFunction.set(t)
  elapsedTime = elapsedTime + dt
  elapsedTimeFunction.set(elapsedTime)
  if (i % NN == 0):
    Msg.Info("%d %d %s :norm=%.4e" % (nbIter, nbExport,printDate(t),solution.norm()))
    nbIter = nbIter+1
  if (i%50 == 0):
    nbExport  = nbExport  + 1
    #Msg.Info("Export %d %s :norm=%.4e" % (nbExport,printDate(t),solution.norm()))
    #solution.exportMsh(outputDir+"sol%05d"%(nbExport),t,i)
    solution.exportFunctionMsh(currentFunction,   outputDirMR+"UV-%05d"%(nbExport),   t,   nbExport,   '')
    solution.exportFunctionMsh(etaFunction,   outputDirMR+"E-%05d"%(nbExport),   t,   nbExport,   '')
    solution.exportFunctionMsh(waterHeightFunction,   outputDirMR+"H-%05d"%(nbExport),   t,   nbExport,   '')
    solution.exportFunctionMsh(claw.getBathymetry(),   outputDirMR+"bathN-%05d"%(nbExport),   t,   nbExport,   '')

Msg.Exit(0)
