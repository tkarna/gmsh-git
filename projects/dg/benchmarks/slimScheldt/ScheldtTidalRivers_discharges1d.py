from dgpy import *
import subprocess
CCode = """
#include "fullMatrix.h"
#include "function.h"
#include "dgGroupOfElements.h"
extern "C" {
	void Q_OudeSchelde(dataCacheMap *,fullMatrix<double> &Qout,fullMatrix<double> &Qin){
    for (size_t i=0; i<Qout.size1(); i++){
      Qout.set(i,0,Qin(i,0)*1./10.); 
    }
	}
	void Q_Ringvaart(dataCacheMap *,fullMatrix<double> &Qout,fullMatrix<double> &Qin){
    for (size_t i=0; i<Qout.size1(); i++){
      Qout.set(i,0,Qin(i,0)*9./10.); 
    }
	}
	void Q_Star(dataCacheMap *,fullMatrix<double> &Qout,fullMatrix<double> &Qin1, fullMatrix<double> &Qin2, fullMatrix<double> &Qin3, fullMatrix<double> &Qin4 ){
    for (size_t i=0; i<Qout.size1(); i++){
      Qout.set(i,0,Qin1(i,0)*1.08 + Qin2(i,0)*1.08 + Qin3(i,0)*1.46 + Qin4(i,0)*1.35 ); 
    }
	}
	void runoff_ZeeSchelde(dataCacheMap *,fullMatrix<double> &runoff,fullMatrix<double> &Qin){
    for (size_t i=0; i<runoff.size1(); i++){
      runoff.set(i,0,Qin(i,0) / 12.8 / 70e3); 
    }
	}
	void runoff_Rupel(dataCacheMap *,fullMatrix<double> &runoff,fullMatrix<double> &Qin){
    for (size_t i=0; i<runoff.size1(); i++){
      runoff.set(i,0,Qin(i,0) / 22.5 / 12e3); 
    }
	}
	void runoff_GroteNete(dataCacheMap *,fullMatrix<double> &runoff,fullMatrix<double> &Qin){
    for (size_t i=0; i<runoff.size1(); i++){
      runoff.set(i,0,Qin(i,0) / 18e3 * (1.35 - 1)); 
    }
	}
	void runoff_BenedenNete(dataCacheMap *,fullMatrix<double> &runoff,fullMatrix<double> &Qin){
    for (size_t i=0; i<runoff.size1(); i++){
      runoff.set(i,0,std::max(1., Qin(i,0) / 51) / 15e3); 
    }
	}
	void runoff_Dijle(dataCacheMap *,fullMatrix<double> &runoff,fullMatrix<double> &Qin){
    for (size_t i=0; i<runoff.size1(); i++){
      runoff.set(i,0,Qin(i,0) / 22e3 * (1.08 - 1)); 
    }
	}
	void runoff_KleineNete(dataCacheMap *,fullMatrix<double> &runoff,fullMatrix<double> &Qin){
    for (size_t i=0; i<runoff.size1(); i++){
      runoff.set(i,0,Qin(i,0) / 16e3 * (1.46 - 1)); 
    }
	}
	void runoff_Zenne(dataCacheMap *,fullMatrix<double> &runoff,fullMatrix<double> &Qin){
    for (size_t i=0; i<runoff.size1(); i++){
      runoff.set(i,0,Qin(i,0) / 13e3 * (1.08 - 1)); 
    }
	}
	void runoff_Durme(dataCacheMap *,fullMatrix<double> &runoff,fullMatrix<double> &Qin){
    for (size_t i=0; i<runoff.size1(); i++){
      runoff.set(i,0,Qin(i,0) / 18.7 / 5.6e3); 
    }
	}
	void runoffComplete(dataCacheMap *m,fullMatrix<double> &runoff,fullMatrix<double> &r_ZeeSchelde,fullMatrix<double> &r_Durme,fullMatrix<double> &r_Dender,fullMatrix<double> &r_Dijle,fullMatrix<double> &r_KleineNete,fullMatrix<double> &r_Zenne,fullMatrix<double> &r_GroteNete,fullMatrix<double> &r_BenedenNete,fullMatrix<double> &r_Rupel){
    std::string tag = m->getGroupOfElements()->getPhysicalTag();
    for (size_t i=0; i<runoff.size1(); i++){
      if(tag == "ZEESCHELDE")
        runoff.set(i,0,r_ZeeSchelde(i,0)); 
      else if(tag == "RUPEL")
        runoff.set(i,0,r_Rupel(i,0)); 
      else if(tag == "OUDESCHELDE")
        runoff.set(i,0,r_ZeeSchelde(i,0)); 
      else if(tag == "RINGVAART")
        runoff.set(i,0,r_ZeeSchelde(i,0)); 
      else if(tag == "DURME")
        runoff.set(i,0,r_Durme(i,0)); 
      else if(tag == "BENEDENNETE")
        runoff.set(i,0,r_BenedenNete(i,0)); 
      else if(tag == "NETEAFLEIDING")
        runoff.set(i,0,r_BenedenNete(i,0)); 
      else if(tag == "KLEINENETE")
        runoff.set(i,0,r_KleineNete(i,0)); 
      else if(tag == "GROTENETE")
        runoff.set(i,0,r_GroteNete(i,0)); 
      else if(tag == "BENEDENDIJLE")
        runoff.set(i,0,r_Dijle(i,0)); 
      else if(tag == "DIJLEAFLEIDING")
        runoff.set(i,0,r_Dijle(i,0)); 
      else if(tag == "BOVENDIJLE")
        runoff.set(i,0,r_Dijle(i,0)); 
      else if(tag == "ZENNE")
        runoff.set(i,0,r_Zenne(i,0)); 
      else if(tag == "ZENNEAFL")
        runoff.set(i,0,r_Zenne(i,0)); 
      else if(tag == "DENDER")
        runoff.set(i,0,r_Dender(i,0)); 
      else
        runoff.set(i,0,0.0); 
    }
	}
}
"""
tmpLib = "ScheldtTidalRivers_discharges1d_lib.so"

if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CCode, tmpLib);
Msg.Barrier()

class ScheldtDischarges():
  def __init__(self, timeFunction):
    ddir = "ScheldtData/discharges/"
    self.Q_Appels = slimFunctionTemporalSerie(ddir+"Appels_discharge_1990_2008",timeFunction)
    self.Q_Eppegem = slimFunctionTemporalSerie(ddir+"Eppegem_discharge_1990_2008",timeFunction)
    self.Q_Dendermonde = slimFunctionTemporalSerie(ddir+"Dendermonde_discharge_1990_2008",timeFunction)
    self.Q_Grobbendonk = slimFunctionTemporalSerie(ddir+"Grobbendonk_discharge_1990_2008",timeFunction)
    self.Q_Haacht = slimFunctionTemporalSerie(ddir+"Haacht_discharge_1990_2008",timeFunction)
    self.Q_Itegem = slimFunctionTemporalSerie(ddir+"Itegem_discharge_1990_2008",timeFunction)
    self.Q_Melle = slimFunctionTemporalSerie(ddir+"Melle_discharge_1990_2008",timeFunction)
    self.Q_Star = functionC(tmpLib,"Q_Star",1,[self.Q_Eppegem,self.Q_Haacht,self.Q_Grobbendonk,self.Q_Itegem])

    self.Q_OudeSchelde = functionC(tmpLib,"Q_OudeSchelde",1,[self.Q_Melle])
    self.Q_Ringvaart = functionC(tmpLib,"Q_Ringvaart",1,[self.Q_Melle])
    self.Q_Durme = functionConstant(1.)
    self.Q_Dender = self.Q_Appels
    self.Q_GroteNete = self.Q_Itegem
    self.Q_KleineNete = self.Q_Grobbendonk
    self.Q_Dijle = self.Q_Haacht
    self.Q_Zenne = self.Q_Eppegem

    self.runoff_ZeeSchelde = functionC(tmpLib,"runoff_ZeeSchelde",1,[self.Q_Star])
    self.runoff_Dender = functionConstant(0.)
    self.runoff_Dijle = functionC(tmpLib,"runoff_Dijle",1,[self.Q_Haacht])
    self.runoff_Rupel = functionC(tmpLib,"runoff_Rupel",1,[self.Q_Star])
    self.runoff_KleineNete = functionC(tmpLib,"runoff_KleineNete",1,[self.Q_Grobbendonk])
    self.runoff_GroteNete = functionC(tmpLib,"runoff_GroteNete",1,[self.Q_Itegem])
    self.runoff_Zenne = functionC(tmpLib,"runoff_Zenne",1,[self.Q_Eppegem])
    self.runoff_BenedenNete = functionC(tmpLib,"runoff_BenedenNete",1,[self.Q_Star])
    self.runoff_Durme = functionC(tmpLib,"runoff_Durme",1,[self.Q_Star])

    self.runoff = functionC(tmpLib, "runoffComplete",1,[self.runoff_ZeeSchelde,self.runoff_Durme,self.runoff_Dender,self.runoff_Dijle, self.runoff_KleineNete,self.runoff_Zenne,self.runoff_GroteNete,self.runoff_BenedenNete,self.runoff_Rupel])


