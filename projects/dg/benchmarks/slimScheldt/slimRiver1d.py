from dgpy import *
import time,math,os,os.path,sys

#general class for the river equations
class slimRiver1d:
  def __init__(self, outputDirectory):
    self.order=1
    self.dimension=1
    self.outputDirectory = outputDirectory
    os.system("mkdir -p %s"%outputDirectory)
    os.system("rm -f %s/*"%outputDirectory)
    self.libSlim1d = "SlimRiver1d.so"
    if (Msg.GetCommRank()==0 ):
      functionC.buildLibraryFromFile ("SlimRiver1d.cc", self.libSlim1d);
    Msg.Barrier()

  def loadMesh(self,meshName, clscale=1):
    extension = meshName[len(meshName)-4:len(meshName)]
    print ("extension is ",extension)
    if extension == ".geo":
      p = os.system("../../buids/gmsh/gmsh_dynamic %s -%d -order %d -clscale %f"%(meshName, self.dimension, self.order, clscale))
      meshNameMsh = meshName[0:len(meshName)-4] + ".msh"
    elif extension == ".msh":
      meshNameMsh = meshName;
    else:
      Msg.Error("Strange extension of meshName")
    
    self.model = GModel()
    self.model.load (meshNameMsh)
  
  def getGroups(self):
    self.curv1D = dgSpaceTransform1DCurvilinear()
    self.groups = dgGroupCollection(self.model, self.dimension, self.order, self.curv1D)
    self.groups.splitGroupsByPhysicalTag()
    return self.groups

  def setDistance1d(self,distance1d):
    self.x1d=distance1d


#Class for the hydrodynamique equation
class slimRiverHydro1d():
  def __init__(self,river1d):
    self.river1d = river1d
    self.groups = river1d.groups
    self.law = dgConservationLawShallowWater1d()
    self.law.setElevationInterp()
    self.etaInterp = self.law.getElevationInterp()
    self.law.setRiverDistance(river1d.x1d.getFunction())
    self.solution = dgDofContainer(self.groups, self.law.getNbFields())
    self.libSlim1d = river1d.libSlim1d

  def setGeometry(self, altitudeData, widthData, sectionData):
    self.law.setWidthData(widthData)
    self.law.setSectionData(sectionData)
    self.altitudedata = altitudeData
    self.widthData = widthData
    self.sectionData = sectionData
  
  def setInitialCondition(self, sectionInitial, velocityInitial):
    self.solutionInitial = functionC(self.libSlim1d,"merge2scalars",2,[sectionInitial,velocityInitial])
  
  def setSolution(self):
    self.solution.interpolate(self.solutionInitial)
    self.law.setHydroSolution(self.solution.getFunction())
    self.law.setElevation()
    self.law.setWidth()
    self.etaF = self.law.getElevation()
    self.width = self.law.getWidth()
    self.section = self.law.getSection()
    self.velocity = self.law.getVelocity()
  
  def setManningFriction(self,manning):
    quadDiss=functionC(self.libSlim1d,"chezyManningDissipation",1,[self.section,self.width,manning])
    quadDiss.thisown = False
    self.law.setQuadraticDissipation(quadDiss)
  
  def setDiffusivity(self,diffusivity):
    self.law.setDiffusivity(diffusivity)
  
  def setSetup(self):
    self.law.setSetup()
    
  def bndUpstreamDischarge(self, Q, tag):
    bnd=self.law.newForceUpstreamDischarge(Q)
    self.law.addBoundaryCondition(tag, bnd)
  
  def bndForceSection(self, section, tag):
    bnd = self.law.newForceSolution(section,self.velocity)
    self.law.addBoundaryCondition(tag, bnd)

  def bndCoast(self, tag):
    bnd = self.law.newBoundaryWall()
    self.law.addBoundaryCondition(tag, bnd)

#Class for the tracer equation

class slimRiverTracer1d:
  def __init__(self, river1d):
    self.river = river1d
    self.law = dgConservationLawShallowWaterTracer1d()
    self.law.setRiverDistance(river1d.x1d.getFunction())
    self.groups = river1d.groups
    self.solution = dgDofContainer(self.groups, self.law.getNbFields())

  def setInitialCondition(self, initialTracer):
    self.initialTracer = initialTracer

  def setSolution(self):
    self.solution.interpolate(self.initialTracer)
    self.law.setTracer(self.solution.getFunction())

  def setHydro(self,hydroSolution,width,elevation):
    self.law.setHydroSolution(hydroSolution)
    self.law.setWidth(width)
    self.law.setElevation(elevation)
    self.section = functionExtractCompNew(hydroSolution,0)
    self.velocity = functionExtractCompNew(hydroSolution,1)

  def setDiffusivity(self,diffusivity):
    self.law.setDiffusivity(diffusivity)
  
  def bndForceTracer(self, tracer, tracerGradient, tag):
    bnd = self.law.newForceTracer(self.section, self.velocity, tracer, tracerGradient)
    self.law.addBoundaryCondition(tag, bnd)
 
  def bndCoast(self, tag):
    bnd = self.law.newBoundaryWall()
    self.law.addBoundaryCondition(tag, bnd)
 

