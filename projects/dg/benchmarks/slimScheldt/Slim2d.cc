#include "fullMatrix.h"
#include "dgGroupOfElements.h"
#include "MElement.h"
#include "MEdge.h"
#include "math.h"
#include "function.h"
extern "C" {
const double R = 6371000.;
double pow2(double value){
  return value*value;
}
void manningScheldt (dataCacheMap *, fullMatrix<double> &FCT, fullMatrix<double> &transect){
  for (size_t i = 0; i< FCT.size1(); i++) {
    double manningShelf = 0.0235;
    double manningConnection = 0.028;
    FCT.set(i,0,manningShelf + transect(i,0)*(manningConnection - manningShelf));
  }
}
void coriolis (dataCacheMap *, fullMatrix<double> &FCT, fullMatrix<double> &xyz) {
  double alpha = 0;
  double Omega = 7.292e-5;
  for (size_t i = 0; i< FCT.size1(); i++) {
    double xi = xyz(i,0);
    double beta = xyz(i,1);
    double lat = asin((4*R*R-xi*xi-beta*beta)/(4*R*R+xi*xi + beta*beta));
    FCT.set(i,0,2*Omega*sin(lat));
  }
}

void transport2velocity(dataCacheMap *,fullMatrix<double> &uv, fullMatrix<double> &UV, fullMatrix<double> &bath, fullMatrix<double> &eta){
  for (size_t i = 0; i < eta.size1(); i++) {
    uv.set(i, 0, UV(i,0)/(bath(i,0)+eta(i,0))); 
    uv.set(i, 1, UV(i,1)/(bath(i,0)+eta(i,0))); 
  }
}
void divideBy6(dataCacheMap *,fullMatrix<double> &fct, fullMatrix<double> &val){
  for (size_t i = 0; i < fct.size1(); i++) {
    fct.set(i, 0, val(i,0)/6.0); 
  }
}

void lonLatDegrees(dataCacheMap *, fullMatrix<double> &lonLat, fullMatrix<double> &XYZ){
  for (size_t i = 0; i < lonLat.size1(); i++) {
    double xi = XYZ(i,0);
    double zeta = XYZ(i,1);
    double x = 4*R*R*xi/(4*R*R +xi*xi + zeta*zeta);
    double y = 4*R*R*zeta/(4*R*R +xi*xi + zeta*zeta);
    double z = R *(4*R*R-xi*xi-zeta*zeta)/(4*R*R+xi*xi + zeta*zeta);
    double lon = atan2(y,x)*180/M_PI;
    double lat = asin(z/R)*180/M_PI;
    lonLat.set(i,0,lon);
    lonLat.set(i,1,lat);
    lonLat.set(i,2,0.);
  }
}
void diffusivityStereo(dataCacheMap *, fullMatrix<double> &diff, fullMatrix<double> &diff0, fullMatrix<double> &XYZ){
  for (size_t i = 0; i < diff.size1(); i++) {
    double xi = XYZ(i,0);
    double zeta = XYZ(i,1);
    double J = 4*R*R/(4*R*R + xi*xi + zeta*zeta);
    diff.set(i,0,diff(i,0)/J/J);
  }
}

void computeErrorVel(dataCacheMap *, fullMatrix<double> &err, fullMatrix<double> &solution1, fullMatrix<double> &solution2){
  for (size_t i = 0; i< err.size1(); i++) {
    err.set(i, 0, solution1(i,1)-solution2(i, 1)); 
    err.set(i, 1, solution1(i,2)-solution2(i, 2)); 
  }
}

void mergeEtaUV(dataCacheMap *,fullMatrix<double> &sol,fullMatrix<double> &eta, fullMatrix<double> &uv){
  for (size_t i = 0; i < sol.size1(); i++) {
    sol.set(i, 0, eta(i,0)); 
    sol.set(i, 1, uv(i,0)); 
    sol.set(i, 2, uv(i,1)); 
  }
}
void merge2(dataCacheMap *,fullMatrix<double> &sol,fullMatrix<double> &u1, fullMatrix<double> &u2){
  for (size_t i = 0; i < sol.size1(); i++) {
    sol.set(i, 0, u1(i,0)); 
    sol.set(i, 1, u2(i,0)); 
  }
}

void bottomDrag(dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &sol, fullMatrix<double> &bath, fullMatrix<double> &manning){
  for(int i = 0;  i < val.size1(); i++){
    double H = sol(i, 0) + bath(i, 0);
    double ampli = 5;
    double factor = 1.;//(ampli - 1) * exp(-0.5 * H*H / ( 1*1 )) + 1 ;
    double n = factor * manning(i,0);
    val.set(i, 0, 9.81*n*n/(pow(H, 1.333333333333)));
  }
}

void smagorinsky(dataCacheMap *map, fullMatrix<double> &val, fullMatrix<double> &solGradient){
  for(int i = 0;  i < val.size1(); i++){
    double dudx = solGradient(i, 3);
    double dudy = solGradient(i, 4);
    double dvdx = solGradient(i, 6);
    double dvdy = solGradient(i, 7);
   
    const dgGroupOfElements *g = map->getGroupOfElements();
    int elNum = map->getElementId();

    double radi =g->getInnerRadius(elNum);
    
    val(i, 0)= pow_int(0.1*radi, 2)*sqrt(2*dudx*dudx+2*dvdy*dvdy+pow_int(dudy+dvdx, 2));
  }
}
void okubo(dataCacheMap *map, fullMatrix<double> &val, fullMatrix<double> &factor){
  for(int i = 0;  i < val.size1(); i++){
    const dgGroupOfElements *g = map->getGroupOfElements();
    int elNum = map->getElementId();
    double radi =g->getInnerRadius(elNum);
    val.set(i, 0, factor(i,0) * 2.0551e-4 * pow( radi, 1.15)) ;
  }
}

void current (dataCacheMap *, fullMatrix<double> &current, fullMatrix<double> &solution) {
  for (size_t i = 0; i < current.size1(); i++) {
    current.set(i, 0, solution(i,1));
    current.set(i, 1, solution(i,2));
    current.set(i, 2, 0);
  }
}
void effectiveWaterHeight (dataCacheMap *, fullMatrix<double> &ewh, fullMatrix<double> &solution, fullMatrix<double> &bath) {
  for (size_t i = 0; i < ewh.size1(); i++) {
    ewh.set(i, 0, solution(i,0)+bath(i, 0));
  }
}

void effectiveEta (dataCacheMap *, fullMatrix<double> &ee, fullMatrix<double> &solution, fullMatrix<double> &bath) {
  for (size_t i = 0; i < ee.size1(); i++) {
    double etaE;
    if((solution(i, 0)+bath(i, 0))>0){
      etaE=solution(i, 0);
    }
    else{
      etaE=-10000.0;
      }
    ee.set(i, 0, etaE);
  }
}

void windStress (dataCacheMap *,fullMatrix<double> &windstress,fullMatrix<double> &winduv, fullMatrix<double> &sol, fullMatrix<double> &bath) {
  for (size_t i = 0; i < windstress.size1(); i++) {
    double normWind=sqrt(winduv(i, 0)*winduv(i, 0)+winduv(i, 1)*winduv(i, 1));
    double rho=1025;
    double sol1 = 0.001*(0.630*normWind+0.066*normWind*normWind)*winduv(i, 0)/(rho*(sol(i, 0)+bath(i, 0)));
    double sol2 = 0.001*(0.630*normWind+0.066*normWind*normWind)*winduv(i, 1)/(rho*(sol(i, 0)+bath(i, 0)));
    windstress.set(i, 0, sol1);
    windstress.set(i, 1, sol2);
  }
}

void bathVec(dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &bathymetry, fullMatrix<double> &normals, fullMatrix<double> &boundaryData) {
  double vx, vy;
  vx = boundaryData(0, 1);
  vy = boundaryData(0, 2);
  for (size_t i = 0; i < bathymetry.size1(); i++) {
    sol.set(i, 0, bathymetry(i, 0) * (vx*normals(i, 0) + vy*normals(i, 1)) );
  }
}
}
