from dgpy import *
from termcolor import colored
from math import *
import time, math, os, os.path, sys, datetime, calendar
from ScheldtEstuary_bath2d import *
import toStereo
from scipy.weave import blitz
import ScheldtTidalRivers_distance1d as distance1d
import ScheldtTidalRivers_shape1d as shape1d
import ScheldtTidalRivers_export1d as export1d
import ScheldtTidalRivers_discharges1d as discharges1d
import ScheldtTidalRivers_smoothFunction as smooth1d
from slimRiver1d import *

order = 1
clscale = 1

if (Msg.GetCommRank() == 0):
  functionC.buildLibraryFromFile ("Slim2d.cc", "libSlim2d.so");

Msg.Barrier()
libSlim2d = "libSlim2d.so"

listArchive = ["mesh-NWECS+ScheldtEstuary","NWECSData","ScheldtData","shapePreproTidalRivers"]
for filename in listArchive:
  if(os.path.exists(filename) == 0):
    print "downloading ",filename
    os.system("curl -O --ftp-port - ftp://braque.mema.ucl.ac.be/slimdata/dgSlimScheldt/%s.zip"%filename)
    os.mkdir(filename)
    fzip = zipfile.ZipFile(filename+".zip")
    fzip.extractall(filename)

if (Msg.GetCommRank() == 0):
  functionC.buildLibraryFromFile ("Slim2d.cc", "libSlim2d.so");

Msg.Barrier()
libSlim2d = "libSlim2d.so"

if(not os.path.exists("full2.msh")):
  os.system("cp mesh-NWECS+ScheldtEstuary/full2.msh .") 
  #m = GModel()
  #m.load("mesh-NWECS+ScheldtEstuary/full2.geo")
  #GmshSetOption("Mesh","CharacteristicLengthFactor",   clscale)
  #GmshSetOption("Mesh","Algorithm", 4)
  #GmshSetOption("Mesh","Remove4Triangles",   1)
  #GmshSetOption("Mesh","CharacteristicLengthExtendFromBoundary",   0)
  #GmshSetOption("Mesh", "LcIntegrationPrecision", 1.e-3)
  #genMesh("full2",2, 1)
  #m.save("full2.msh")

if(not os.path.exists("full2-Stereo.msh")): 
  m2 = GModel()
  m2.load("full2.msh")
  toStereo.projectMesh(m2)
  m2.save("full2-Stereo.msh")

if(not os.path.exists("full2-lonLat.msh")): 
  m3 = GModel()
  m3.load("full2.msh")
  toLonLat.projectMesh(m3, 1.5)#1.5 is the factor to scale Y-axis (plotting purposes)
  m3.save("full2-lonLat.msh")


model_2d = GModel()
model_2d.load ('full2-Stereo.msh')
def printTime (t) :
  return "%dh%02d'%02d\"" % (t/3600,  (t%3600)/60,  t%60)
def printDate (t):
  date=time.gmtime(t)
  return "%02d/%02d/%04d %02dh%02d'%02d\""%(date[2], date[1], date[0], date[3], date[4], date[5])

elapsedTime=0
initialDate=datetime.datetime(2000,1,1,12,0,0)
initialTime=calendar.timegm(initialDate.timetuple())
timeFunction = functionConstant(initialTime)
elapsedTimeFunction=functionConstant([elapsedTime])

outputDir = "output/"
try : 
  os.mkdir(outputDir);
except: 0;
os.system("rm -f %s/*"%outputDir)

# Python Functions
def printTime (t) :
  return "%dh%02d'%02d\"" % (t/3600,  (t%3600)/60,  t%60)

groups_2d = dgGroupCollection(model_2d, 2, order)
hydroLaw_2d = dgConservationLawShallowWater2d()
hydroSol_2d = dgDofContainer(groups_2d, hydroLaw_2d.getNbFields())
XYZ = groups_2d.getFunctionCoordinates();

#bathymetry
bathOriginal = bathymetry("bath",outputDir,groups_2d, XYZ, minV=-1, maxV=3e3)
cropOnTag(groups_2d, bathOriginal, 20, 1e3, "Shelf Break")
cropOnTag(groups_2d, bathOriginal, 10, 1e3, "Bretagne and Basse Normandie")
bathEffective = smoothBathymetry(groups_2d,bathOriginal, minV=-1, maxV=3e3)

#SW2D
hydroLaw_2d = dgConservationLawShallowWater2d()
hydroSol_2d = dgDofContainer(groups_2d, hydroLaw_2d.getNbFields())
initialSolution = functionConstant([0., 0., 0.])
hydroSol_2d.interpolate(initialSolution)
Coriolis = functionC(libSlim2d, "coriolis", 1, [XYZ])
hydroLaw_2d.setCoriolisFactor(Coriolis)
hydroLaw_2d.setUpwindFactorRiemann(1.);

R=6371000.
hydroLaw_2d.setIsSpherical(R)

#nu0 = functionC(libSlim2d,"smagorinsky",1,[hydroSol_2d.getFunctionGradient()])
#nu = functionC(libSlim2d,"diffusivityStereo",1,[nu0, XYZ])
#hydroLaw_2d.setDiffusivity(nu)

hydroLaw_2d.setBathymetry(bathEffective.getFunction())
hydroLaw_2d.setBathymetryGradient(bathEffective.getFunctionGradient())
hydroLaw_2d.setMovingBathWettingDrying(0.5)

ld = functionConstant(0)
#hydroLaw_2d.setLinearDissipation(ld)
#hydroLaw_2d.setSource(ld)

bath_2d = hydroLaw_2d.getBathymetry()
elevation_2d = hydroLaw_2d.getElevation()

manning = functionConstant(0.03)
Cd_2d = functionC(libSlim2d, "bottomDrag", 1, [hydroSol_2d.getFunction(), bath_2d, manning])
hydroLaw_2d.setQuadraticDissipation(Cd_2d)
#Discharges
dischargeDir = "ScheldtData/discharges/";
hydroLaw_2d.addBoundaryCondition("Coast", hydroLaw_2d.newBoundaryWall())
hydroLaw_2d.addBoundaryCondition("none", hydroLaw_2d.newBoundaryWall())

Q_GTC = slimFunctionTemporalSerie(dischargeDir+"terneuzen_discharge_monthly_2000-2008.txt",timeFunction)
Q_GTC.usePeriodicYear()
hydroLaw_2d.addBoundaryCondition("Ghent-Terneuzen Canal", hydroLaw_2d.newForcedDischarge(hydroSol_2d, Q_GTC, "Ghent-Terneuzen Canal"))

Q_Bath = slimFunctionTemporalSerie(dischargeDir+"bath_discharge_monthly_1988-2008.txt",timeFunction)
Q_Bath.usePeriodicYear()
hydroLaw_2d.addBoundaryCondition("Bath Canal", hydroLaw_2d.newForcedDischarge(hydroSol_2d, Q_Bath, "Bath Canal"))

Q_DocksFull = slimFunctionTemporalSerie(dischargeDir+"docks_discharge_monthly_1988-2008.txt",timeFunction)
Q_DocksFull.usePeriodicYear()
def divideBy6(FCT,VAL):
  blitz("FCT[:, 0] = VAL[:, 0]/6", check_size = 0)
Q_Docks = functionNumpy(1, divideBy6, [Q_DocksFull])
hydroLaw_2d.addBoundaryCondition("Zandvlietsluis North", hydroLaw_2d.newForcedDischarge(hydroSol_2d,Q_Docks,"Zandvlietsluis North" ))
hydroLaw_2d.addBoundaryCondition("Zandvlietsluis South", hydroLaw_2d.newForcedDischarge(hydroSol_2d,Q_Docks,"Zandvlietsluis South"))
hydroLaw_2d.addBoundaryCondition("BoudewijnSluis North", hydroLaw_2d.newForcedDischarge(hydroSol_2d,Q_Docks,"BoudewijnSluis North"))
hydroLaw_2d.addBoundaryCondition("BoudewijnSluis South", hydroLaw_2d.newForcedDischarge(hydroSol_2d,Q_Docks,"BoudewijnSluis South"))
hydroLaw_2d.addBoundaryCondition("Kallo Sluis"         , hydroLaw_2d.newForcedDischarge(hydroSol_2d,Q_Docks,"Kallo Sluis"))
hydroLaw_2d.addBoundaryCondition("Hansweert Canal"     , hydroLaw_2d.newForcedDischarge(hydroSol_2d,Q_Docks,"Hansweert Canal"))


Q_Meuse = slimFunctionTemporalSerie(dischargeDir+"meuse_discharge_monthly_1971-2007.txt",timeFunction)
Q_Meuse.usePeriodicYear()
hydroLaw_2d.addBoundaryCondition("Meuse",hydroLaw_2d.newForcedDischarge(hydroSol_2d,Q_Meuse,"Meuse"))

Q_Rhine = slimFunctionTemporalSerie(dischargeDir+"rhine_discharge_monthly_1971-2007.txt",timeFunction)
Q_Rhine.usePeriodicYear()
hydroLaw_2d.addBoundaryCondition("Rhine",hydroLaw_2d.newForcedDischarge(hydroSol_2d,Q_Rhine,"Rhine"))

Q_Thames = slimFunctionTemporalSerie(dischargeDir + "thames_discharge_1980_2008",timeFunction)
hydroLaw_2d.addBoundaryCondition("Thames", hydroLaw_2d.newForcedDischarge(hydroSol_2d,Q_Thames,"Thames"))

Q_Seine = slimFunctionTemporalSerie(dischargeDir+"seine_discharge_1980_2008",timeFunction)
hydroLaw_2d.addBoundaryCondition("Seine",hydroLaw_2d.newForcedDischarge(hydroSol_2d,Q_Seine,"Seine"))
hydroLaw_2d.addBoundaryCondition("Baltic", hydroLaw_2d.newBoundaryWall())
hydroLaw_2d.addBoundaryCondition("Bretagne and Basse Normandie", hydroLaw_2d.newBoundaryWall())

lonLatDegrees = functionC(libSlim2d,"lonLatDegrees",3,[XYZ])
tideEta = slimFunctionTpxo("NWECSData/tpxo/hf.ES2008.nc","ha","hp","lon_z","lat_z")
tideEta.setCoordAndTimeFunctions(lonLatDegrees, timeFunction)
tideEta.useDofContainers(groups_2d)
tideU = slimFunctionTpxo("NWECSData/tpxo/uv.ES2008.nc","Ua","up","lon_u","lat_u")
tideU.setCoordAndTimeFunctions(lonLatDegrees, timeFunction)
tideU.useDofContainers(groups_2d)
tideV = slimFunctionTpxo("NWECSData/tpxo/uv.ES2008.nc","Va","vp","lon_v","lat_v")
tideV.setCoordAndTimeFunctions(lonLatDegrees, timeFunction)
tideV.useDofContainers(groups_2d)
tideUV = functionC(libSlim2d,"merge2",2,[tideU, tideV])
tideUV_stereo = transformLonLat2Stereo(tideUV, XYZ, R)
tideuv = functionC(libSlim2d,"transport2velocity",3,[tideUV,bath_2d,tideEta]);
tpxoTide = functionC(libSlim2d,"mergeEtaUV",3,[tideEta,tideuv]);
tpxo_u = functionExtractCompNew(tpxoTide,1)
tpxo_v = functionExtractCompNew(tpxoTide,2)
hydroLaw_2d.addBoundaryCondition('Shelf Break',hydroLaw_2d.newOutsideValueBoundary("Surface", tpxoTide))
uv_stereo = functionC(libSlim2d,"current",3,[hydroSol_2d.getFunction()])
uv_lonlat = transformStereo2LonLat(uv_stereo, XYZ, R)

##############################################################################################################
#### River 1d stuffs
##############################################################################################################
slim_1d = slimRiver1d(outputDirectory="output")
slim_1d.loadMesh("ScheldtTidalRivers.msh",clscale=4)
groups_1d = slim_1d.getGroups()
#-------------------------
# Coordinate
#-------------------------
x_1d = distance1d.get(groups_1d)
slim_1d.setDistance1d(x_1d)

#-------------------------
# Hydro
#-------------------------
hydro_1d = slimRiverHydro1d(slim_1d)

#-------------------------
# Hydro: Geometry
#-------------------------
riverNames=["OUDESCHELDE","DURME","ZEESCHELDE","RINGVAART","RUPEL","KLEINENETE","NETEAFLEIDING","BENEDENNETE","GROTENETE","BENEDENDIJLE","BOVENDIJLE","DIJLEAFLEIDING","ZENNEAFL","ZENNE","DENDER"]
smoothRiverData=30
shapeData=shape1d.riverShape("ScheldtData/shapes",riverNames,smoothRiverData)
shapeData.setFields(groups_1d,x_1d)
altitudeData,widthData,sectionData=shapeData.get(groups_1d,hydro_1d.etaInterp)
hydro_1d.setGeometry(altitudeData,widthData,sectionData)

#-------------------------
# Hydro: Initial conditions
#-------------------------
velocityInitial=functionConstant([0.])
bankAltitude1=functionExtractCompNew(altitudeData.getFunction(),2)
bankAltitude2=functionExtractCompNew(altitudeData.getFunction(),3)
bottomAltitude=functionExtractCompNew(altitudeData.getFunction(),1)
layer0=functionExtractCompNew(altitudeData.getFunction(),0)
def setElevationAltitudeBank(FCT,altitude):
  for i in range(0,FCT.size1()):
    bottom=altitude(i,1)
    bank=min(altitude(i,2),altitude(i,3))
    layer0=altitude(i,0)
    depthMax = bank-bottom
    FCT.set(i,0,max(min(bank-3,7),0))

elevationInitialFun = functionPython(1,setElevationAltitudeBank,[altitudeData.getFunction()])
elevationInitialSmooth = smooth1d.smooth(slim_1d,elevationInitialFun,nIteration=100,dt=50000)
altitudeInitial,widthInitial,sectionInitial = shapeData.get(groups_1d,elevationInitialSmooth.getFunction())
hydro_1d.setInitialCondition(sectionInitial,velocityInitial)

#-------------------------
# Hydro: Set solution
#-------------------------
hydro_1d.setSolution()

#-------------------------
# Hydro: Supplementary terms1
#-------------------------
fzero = functionConstant(0.)
hydro_1d.law.setLinearDissipation(fzero)
hydro_1d.law.setSourceMass(fzero)
hydro_1d.law.setSourceMomentum(fzero)
manning_1d = functionConstant(0.025)
hydro_1d.setManningFriction(manning_1d)
okuboFactor=functionConstant(0.1)
diffusivity=functionC(slim_1d.libSlim1d,"okubo",1,[okuboFactor]);
hydro_1d.setDiffusivity(diffusivity)
print "before setup"
hydro_1d.setSetup()
print "after setup"

coupling12 = dgCoupling_SW1D_SW2D(groups_1d, groups_2d, hydro_1d.solution, hydroSol_2d, bath_2d, "downstreamZEESCHELDE","Upstream")
print "w=", coupling12.getWidth()
print "coupling ok"
shapeData.squareChannelConnection(groups_1d, "ZEESCHELDE",coupling12.getWidth(), coupling12.getSection0()/coupling12.getWidth())
print "w=", coupling12.getWidth()
print "square ok"
hydro_1d.solution.interpolate(hydro_1d.solutionInitial)
print "done load river stuffs"
#-------------------------
# Hydro: Boundary conditions
#-------------------------

#DOWNSTREAM

#UPSTREAM
Q = discharges1d.ScheldtDischarges(timeFunction)
hydro_1d.bndUpstreamDischarge(Q.Q_OudeSchelde,"Ghent-upstreamOUDESCHELDE")
hydro_1d.bndUpstreamDischarge(Q.Q_Ringvaart,"Ghent-upstreamRINGVAART")
hydro_1d.bndUpstreamDischarge(Q.Q_Durme,"upstreamDURME")
hydro_1d.bndUpstreamDischarge(Q.Q_Dender,"upstreamDENDER")
hydro_1d.bndUpstreamDischarge(Q.Q_GroteNete,"Itegem-upstreamGROTENETE")
hydro_1d.bndUpstreamDischarge(Q.Q_KleineNete,"Grobbendonk-upstreamKLEINENETE")
hydro_1d.bndUpstreamDischarge(Q.Q_Dijle,"Haacht-upstreamBOVENDIJLE")
hydro_1d.bndUpstreamDischarge(Q.Q_Zenne,"Eppegem-upstreamZENNE")

#COUPLING
hydro_1d.law.addBoundaryCondition("downstreamZEESCHELDE",hydro_1d.law.newBoundaryCouplingSW2D(coupling12.getOutsideSolution2(), coupling12.getWidth(), coupling12.getSection0()))
hydroLaw_2d.addBoundaryCondition("Upstream", hydroLaw_2d.newBoundaryCouplingSW1D(coupling12.getOutsideSolution1()))

#-------------------------
# Initial exports
#-------------------------
export_1d = export1d.export(groups_1d, hydro_1d.solution, hydro_1d.width, x_1d.getFunction(), slim_1d.outputDirectory)
export_1d.exportFunctionTxt(layer0,"layer0",0)
export_1d.exportFunctionTxt(bankAltitude1,"bankAltitude1",0)
export_1d.exportFunctionTxt(bankAltitude2,"bankAltitude2",0)
export_1d.exportFunctionTxt(bottomAltitude,"bottomAltitude",0)
export_1d.exportFunctionTxt(hydro_1d.section,"section",0)
export_1d.exportFunctionTxt(hydro_1d.width,"width",0)
export_1d.exportFunctionTxt(hydro_1d.etaF,"eta",0)
export_1d.exportFunctionTxt(shapeData.widthDataLevel.getFunction(),"widthDataLevel",0)
export_1d.exportFunctionTxt(shapeData.altitudeData.getFunction(),"altitudeData",0)
export_1d.exportFunctionTxt(elevationInitialSmooth.getFunction(),"etaInitialSmooth",0)
export_1d.exportFunctionTxt(elevationInitialFun,"etaInitialFun",0)
etaDof = dgDofContainer(groups_1d, 1)


dt = 0.1
hydroSolver_2d=dgERK(hydroLaw_2d,None,DG_ERK_22)
hydroSolver_1d=dgERK(hydro_1d.law,None,DG_ERK_22)


nbExport = 0
t = initialTime
elapsedTime = 0
Msg.Info("%d %s" % (nbExport,printDate(t)))
t = initialTime
for i in range (0,10000):
	if (elapsedTime % 10 == 0):
		nbExport  = nbExport  + 1
		Msg.Info("%d %s :norm=%.4e" % (nbExport,printDate(t),hydroSol_2d.norm()))
		hydroSol_2d.exportFunctionMsh(elevation_2d,outputDir+"eta_2d_%05d"%(nbExport),t,nbExport)
		bathEffective.exportFunctionMsh(bath_2d,outputDir+"bath_2d_%05d"%(nbExport),t,nbExport)
		hydroSol_2d.exportFunctionMsh(uv_lonlat,outputDir+"uv_2d_%05d"%(nbExport),t,nbExport)
		export_1d.exportFunctionTxt(hydro_1d.etaF,"eta",nbExport)
		export_1d.exportFunctionTxt(hydro_1d.velocity,"velocity",nbExport)
    
	for rk in range(hydroSolver_2d.getNsteps()):
		coupling12.updateIntegrals()
		hydroSolver_2d.subiterate(hydroSol_2d,dt,t)
		hydroSolver_1d.subiterate(hydro_1d.solution,dt,t)
	hydroSolver_2d.finalize(hydroSol_2d)
	hydroSolver_1d.finalize(hydro_1d.solution)
	t = t + dt
	timeFunction.set(t)
	elapsedTime = elapsedTime + dt
	elapsedTimeFunction.set(elapsedTime)
Msg.Exit(0)
