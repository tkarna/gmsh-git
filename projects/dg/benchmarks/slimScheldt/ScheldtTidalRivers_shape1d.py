import math
from dgpy import *
from gmshpy import *

import numpy
import matplotlib.pyplot as matplot

CCode = """
#include "fullMatrix.h"
#include "function.h"
static double nLayer=10;
static double dataShift=0;
extern "C" {
  void interpWidth (dataCacheMap *m, fullMatrix<double> &width, fullMatrix<double> &altitudeLayer0,  fullMatrix<double> &widthDataLevel, fullMatrix<double> &elevation) {
    for (size_t i = 0; i< width.size1(); i++) {
      double maxId=nLayer-1;
      double eta=elevation(i,0);
      double varx=eta+dataShift-altitudeLayer0(i,0);
      double var0=std::max(std::min(floor(varx),maxId),0.);
      double var1=std::max(std::min(floor(varx)+1,maxId),0.);
      double w0=widthDataLevel(i,var0);
      double w1=widthDataLevel(i,var1);
      double xi=std::max(std::min(varx-var0,1.),0.);
      double w =  xi*w1 + (1-xi)*w0;
      width.set(i,0, w);
    }
  }
  void interpSection (dataCacheMap *m, fullMatrix<double> &section, fullMatrix<double> &altitudeLayer0, fullMatrix<double> &widthDataLevel, fullMatrix<double> &sectionDataLevel, fullMatrix<double> &elevation) {
    for (size_t i = 0; i< section.size1(); i++) {
      double maxId=nLayer-1;
      double eta=elevation(i,0);
      double varx=eta+dataShift-altitudeLayer0(i,0);
      double var0=std::max(std::min(floor(varx),maxId),0.);
      double var1=std::max(std::min(floor(varx)+1,maxId),0.);
      double w0=widthDataLevel(i,var0);
      double w1=widthDataLevel(i,var1);
      double xi=std::max(std::min(varx-var0,1.),0.);
      double w=xi*w1 + (1-xi)*w0;
      double s0=sectionDataLevel(i,var0);
      double s1=sectionDataLevel(i,var1);
      double s = s0+(w0+w)/2*(varx-var0);
      section.set(i,0, s);  //base*height/2 = area of trapeze
    }
  }
}
"""
libso="ScheldtTidalRivers_shape1d_lib.so"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CCode, libso);
#########################################################
def interpolateLayers(FCT,x1d):
  tagId=riverNames.index(FCT.getDataCacheMap().getGroupOfElements().getPhysicalTag())
  n=nChain[tagId]
  o=oChain[tagId]
  d=dChain[tagId]
  for i in range(0,FCT.size1()):
    x=x1d(i,0)
    id0=int(min(max(math.floor((x-o)/d),0),n-1))
    id1=int(min(max(math.floor((x-o)/d)+1,0),n-1))
    x0=o+id0*d
    xi=max(min((x-x0)/d,1),0)
    for l in range(0,FCT.size2()):
      value=(1-xi)*field[tagId][id0,l]+xi*field[tagId][id1,l]
      FCT.set(i,l,value);
###################################################
def smooth(x,window_len=11,window='hanning'):
  s=numpy.r_[x[window_len-1:0:-1],x,x[-1:-window_len:-1]]
  if window == 'flat': 
      w=numpy.ones(window_len,'d')
  else:
      w=eval('numpy.'+window+'(window_len)')
  y=numpy.convolve(w/w.sum(),s,mode='valid')
  y=y[window_len/2-1:-window_len/2]
  return y
###################################################
class riverShape(object):
  def __init__(self,directory,names,smoothNumber=20):
    global riverNames
    riverNames=names
    self.oChain=[]
    self.dChain=[]
    self.nChain=[]
    self.dLevel=[]
    self.nLevel=[]
    self.data=[]
    self.field=0
    self.riverNames=riverNames
    self.nAltitudeFields=4
    self.smoothNumber=smoothNumber
    for name in riverNames:
      filename=directory+"/"+name+"_shape.txt"
      tagId=riverNames.index(name)
      data0=self.readLayer(filename)
      size=data0.shape
      self.nLevels=self.nLevel[-1]
      for levelId in range(0,size[1]):
        if(self.nChain[-1] > self.smoothNumber):
          dataTemp=smooth(data0[:,levelId],smoothNumber)
        else :
          dataTemp=smooth(data0[:,levelId],max(len(data0)-5,2))
        for chainId in range(self.nChain[-1]):
          data0[chainId,levelId]=dataTemp[chainId]
      self.data.append(data0)
    global nChain,oChain,dChain
    nChain=self.nChain
    oChain=self.oChain
    dChain=self.dChain
  ###################################################
  def squareChannelConnection(self, groups, tag, width, bath):
    N = 10
    #for chainId in range(N):
    #  for levelId in range(10):
    #    eta = self.data[tagId][chainId,0] + float(levelId)
    #    xsi = float(chainId)/(N-1);
    #    print chainId, levelId, xsi
    #    self.data[tagId][chainId,4+levelId] =  xsi * self.data[tagId][chainId,4+levelId] + (1-xsi) * width
    #    self.data[tagId][chainId,4+10+levelId] =  xsi * self.data[tagId][chainId,4+10+levelId] + (1-xsi) * width * (bath + eta)
    cacheMap=dataCacheMap(dataCacheMap.NODE_MODE,groups)
    group = groups.getElementGroup(tag)
    proxy_w = self.widthDataLevel.getGroupProxy(group);
    proxy_s = self.sectionDataLevel.getGroupProxy(group);
    proxy_a = self.altitudeData.getGroupProxy(group);

    for i in range(proxy_w.size1()):
      for j in range(N):
        for k in range(10):
          xsi = float(j+i) / N
          eta = proxy_a(i,j*4) + float(k)
          proxy_w.set(i, j*10+k, xsi * proxy_w(i,j*10+k) + (1-xsi) * width )
          proxy_s.set(i, j*10+k, xsi * proxy_s(i,j*10+k) + (1-xsi) * width * (bath + eta) )

  #############################################@@@@##
  def readLayer(self,filename):
    file=open(filename)
    line=file.readline().split()
    line=file.readline().split()
    self.oChain.append(float(line[0]))
    self.dChain.append(float(line[1]))
    self.dLevel.append(float(line[2]))
    self.nChain.append(int(line[3]))
    self.nLevel.append(int(line[4]))
    line=file.readline().split()
    data0=[]
    for i in range(0,self.nChain[-1]):
      line=file.readline().split()
      line=[float(l) for l in line]
      data0.append(line)
    file.close()
    return numpy.array(data0)
  ###################################################
  def getAltitudeData(self):        
    data=[]
    for rivId in range(0,len(self.riverNames)):
      data.append(self.data[rivId][:,0:4])
    return data
  ###################################################
  def getWidthData(self):        
    data=[]
    for rivId in range(0,len(self.riverNames)):
      data.append(self.data[rivId][:,4:4+10])
    return data
  ###################################################
  def getSectionData(self):        
    data=[]
    for rivId in range(0,len(self.riverNames)):
      data.append(self.data[rivId][:,4+10:4+2*10])
    return data
  ###################################################
  def setFields(self,groups,x1d):
    #-------------------------------------------------
    #-- altitude fields
    #-------------------------------------------------
    self.altitudeData=dgDofContainer(groups,self.nAltitudeFields)
    global field
    field=self.getAltitudeData()
    self.altitudeDataF=functionPython(self.nAltitudeFields,interpolateLayers, [x1d.getFunction()])
    self.altitudeData.interpolate(self.altitudeDataF)
    self.altitudeDataLayer0=functionExtractCompNew(self.altitudeData.getFunction(),0)
    #-------------------------------------------------
    #-- width fields
    #-------------------------------------------------
    self.widthDataLevel=dgDofContainer(groups,self.nLevels)
    field=self.getWidthData()
    self.widthDataLevelF=functionPython(self.nLevels,interpolateLayers, [x1d.getFunction()])
    self.widthDataLevel.interpolate(self.widthDataLevelF)
    #-------------------------------------------------
    #-- section fields
    #-------------------------------------------------
    self.sectionDataLevel=dgDofContainer(groups,self.nLevels)
    field=self.getSectionData()
    self.sectionDataLevelF=functionPython(self.nLevels,interpolateLayers, [x1d.getFunction()])
    self.sectionDataLevel.interpolate(self.sectionDataLevelF)

  def get(self,groups,etaInterp):
    widthData=functionC(libso,"interpWidth",1,[self.altitudeDataLayer0,self.widthDataLevel.getFunction(),etaInterp])
    sectionData=functionC(libso,"interpSection",1,[self.altitudeDataLayer0,self.widthDataLevel.getFunction(),self.sectionDataLevel.getFunction(),etaInterp])
    #-------------------------------------------------
    return self.altitudeData,widthData,sectionData
  ###################################################


