from dgpy import *
import math

def projectMesh (model, factorY):
  def project(entity):
    numVertices = entity.getNumMeshVertices()
    for iV in range(0, numVertices): 
      v = entity.getMeshVertex(iV)
      R = (v.x() ** 2 + v.y() ** 2 + v.z() ** 2) ** 0.5
      x = v.x()
      y = v.y()
      z = v.z()
      lat = math.asin(z/R)*180./math.pi*factorY
      lon = math.atan2(y,x)*180./math.pi
      if not R == 0 :
        v.setXYZ(lon,lat, 0)
  gvertices = model.bindingsGetVertices()
  for v in gvertices:
    project(v)
  gedges = model.bindingsGetEdges()
  for e in gedges:
    project(e)
  gfaces = model.bindingsGetFaces()
  for f in gfaces:
    project(f)

