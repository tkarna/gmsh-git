from dgpy import *
from gmshpy import *
import time, math, os
import ScheldtTidalRivers_export1d as export1d
import os.path
import os
import sys
geo=".geo"
msh=".msh"
order=1
dimension=1
os.system("mkdir -p tracerAlone")
os.system("rm tracerAlone/*")

if (Msg.GetCommRank()==0 ):
  functionC.buildLibraryFromFile ("TidalRiver.cc", "TidalRiver.so");
Msg.Barrier()
g = 9.81;
t = 0;
TIME=function.getTime()

def printTime (t) :
  return "%dh%02d'%02d\"" % (t/3600, (t%3600)/60, t%60)

def merge(FCT,F1,F2):
 for i in range(0,FCT.size1()):
    FCT.set(i,0,F1(i,0));
    FCT.set(i,1,F2(i,0));

model = GModel()
meshName="river"
#p = os.system("gmsh %s.geo -%d -order %d"%(meshName, dimension, order))
model.load (meshName+msh)

curv1D = dgSpaceTransform1DCurvilinear()
groups = dgGroupCollection(model, dimension, order, curv1D)
groups.splitGroupsByPhysicalTag()
#groups.buildGroupsOfInterfaces()

print "*** print initial sol ***"
os.system("mkdir -p tracerAlone")

#TracerLaw
tracerLaw = dgConservationLawShallowWaterTracer1d()
elevation=functionConstant([0.])
tracerLaw.setElevation(elevation)
width=functionConstant([100.])
tracerLaw.setWidth(width)
hydroSol = functionConstant([1000., 0. ])
tracerLaw.setHydroSolution(hydroSol)
diffusivity = functionConstant(10.)
tracerLaw.setDiffusivity(diffusivity)

tracerSol = dgDofContainer(groups, tracerLaw.getNbFields())
tracerInitial=functionConstant([0.])
tracerSol.interpolate(tracerInitial)
tracerSol.exportGroupIdMsh()
tracerLaw.setTracer(tracerSol.getFunction())

tracerDownstream=functionConstant(1.)
velocity=functionExtractCompNew(hydroSol,1)
section=functionExtractCompNew(hydroSol,0)
tracerGradientDownstream=functionConstant(0.)
tracerLaw.addBoundaryCondition("Downstream",tracerLaw.newForceTracer(section,velocity,tracerDownstream,tracerGradientDownstream))

tracerUpstream=functionConstant(1.)
tracerGradientUpstream=functionConstant(0.)
tracerGradientUpstream=tracerSol.getFunctionGradient()
tracerLaw.addBoundaryCondition("Upstream",tracerLaw.newForceTracer(section,velocity,tracerUpstream,tracerGradientUpstream))

XYZ = groups.getFunctionCoordinates()
x1d = functionExtractCompNew(XYZ,0)
export = export1d.export(groups, tracerSol, width, x1d, "tracerAlone")

#Iterate
#tracerSolver=dgERK(tracerLaw,None,DG_ERK_44)
dt = 600
petsc = linearSystemPETScBlockDouble()
petsc.setParameter("petscOptions",  "-pc_type bjacobi -sub_pc_type ilu")
dof = dgDofManager.newDGBlock(groups, tracerLaw.getNbFields(), petsc)
tracerSolver = dgDIRK(tracerLaw, dof, 2) 
tracerSolver.getNewton().setAtol(1.e-5)
tracerSolver.getNewton().setRtol(1.e-5)
tracerSolver.getNewton().setVerb(1)

print "*** solve ***"
t=0
nbExport = 0
tic = time.clock()
for i in range (0,100) :
  tracerSolver.iterate(tracerSol,dt,t)
  normTracer = tracerSolver.getNewton().getFirstResidual()
  if (t % 3600 == 0) :
    Msg.Info("%d | normHydro: %.12g" % (i,normTracer))
    tracerSol.exportMsh("tracerAlone/tracer-%06d" %i , t, nbExport)
    export.exportFunctionTxt(tracerSol.getFunction(),"tracer",nbExport)
    export.exportFunctionPosQuad(tracerSol.getFunction(),"tracer",nbExport)
    nbExport  = nbExport  + 1
  t = t+dt
