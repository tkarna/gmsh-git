from dgpy import *
import subprocess

class solver:
  def __init__(self, groups, law,solution, implicitTs=True, order=2):
    self.implicitTs = implicitTs
    self.solution = solution
    self.rkSolver = None
    if self.implicitTs==True:
      petsc = linearSystemPETScBlockDouble()
      petsc.setParameter("petscOptions",  "-pc_type bjacobi -sub_pc_type ilu")
      dof = dgDofManager.newDGBlock(groups, law.getNbFields(), petsc)
      self.rkSolver = dgDIRK(law, dof, 2)
      self.rkSolver.getNewton().setAtol(1.e-5)
      self.rkSolver.getNewton().setRtol(1.e-6)
      self.rkSolver.getNewton().setVerb(1)   
    else:
      if(order==1):
        self.rkSolver=dgERK(law,None,DG_ERK_EULER)
      elif(order==2):
        self.rkSolver=dgERK(law,None,DG_ERK_22)
      elif(order==3):
        self.rkSolver=dgERK(law,None,DG_ERK_33)
      elif(order==4):
        self.rkSolver=dgERK(law,None,DG_ERK_44)
      elif(order==6):
        self.rkSolver=dgERK(law,None,DG_ERK_65_FEHLBERG)
      else:
        Msg.Error("no dgERK solver for order %d" % order)

  def doOneIteration(self, dt, t):
    self.rkSolver.iterate(self.solution, dt , t)
    if self.implicitTs==True:
      res = self.rkSolver.getNewton().getFirstResidual()
    else:
      res = solution.norm()
    return res
