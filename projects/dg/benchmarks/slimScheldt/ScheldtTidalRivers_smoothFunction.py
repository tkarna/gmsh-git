from dgpy import *
from slimRiver1d import *

def smooth(river,fun,nIteration=100,dt=2000):
  tracer = slimRiverTracer1d(river)
  tracer.setInitialCondition(fun)
  tracer.setSolution()
  velocity = functionConstant(0.)
  elevation = functionConstant(0.)
  width = functionConstant(1.)
  section = functionConstant(1.)
  hydro = functionC(river.libSlim1d,"merge2scalars",2,[section,velocity])
  tracer.setHydro(hydro,width,elevation)
  diffusivity = functionConstant(1.)
  tracer.setDiffusivity(diffusivity)
  tracer.law.setSigmaFactorBifurcations(1.)
  #-------------------------
  # Boundary conditions
  #-------------------------
  
  #DOWNSTREAM
  tracerGradientBnd = tracer.solution.getFunctionGradient() 
  tracer.bndForceTracer(fun,tracerGradientBnd,"downstreamZEESCHELDE")
  #UPSTREAM
  tracer.bndForceTracer(fun,tracerGradientBnd,"Ghent-upstreamOUDESCHELDE")
  tracer.bndForceTracer(fun,tracerGradientBnd,"Ghent-upstreamRINGVAART")
  tracer.bndForceTracer(fun,tracerGradientBnd,"upstreamDURME")
  tracer.bndForceTracer(fun,tracerGradientBnd,"upstreamDENDER")
  tracer.bndForceTracer(fun,tracerGradientBnd,"Itegem-upstreamGROTENETE")
  tracer.bndForceTracer(fun,tracerGradientBnd,"Grobbendonk-upstreamKLEINENETE")
  tracer.bndForceTracer(fun,tracerGradientBnd,"Haacht-upstreamBOVENDIJLE")
  tracer.bndForceTracer(fun,tracerGradientBnd,"Eppegem-upstreamZENNE")
  
  #-------------------------
  # Iterate
  #-------------------------
  
  petsc = linearSystemPETScBlockDouble()
  petsc.setParameter("petscOptions",  "-pc_type bjacobi -sub_pc_type ilu")
  dof = dgDofManager.newDGBlock(river.groups, tracer.law.getNbFields(), petsc)
  tracerSolver = dgDIRK(tracer.law, dof, 1) 
  tracerSolver.getNewton().setAtol(1.e-4)
  tracerSolver.getNewton().setRtol(1.e-4)
  tracerSolver.getNewton().setVerb(1)
  
  t=0
  for i in range (1,nIteration) :
    normTracer = tracerSolver.iterate(tracer.solution,dt,t)
    normTracer = tracerSolver.getNewton().getFirstResidual()
    #tracer.solution.bezierCrop(minFun,maxFun)
    if (i % 10 == 0) :
      Msg.Info("smooth function %d/%d  :" % (i,nIteration))
    t = t + dt
  Msg.Info("smooth function %d/%d  :" % (i+1,nIteration))
  return tracer.solution
