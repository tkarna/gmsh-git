#include "fullMatrix.h"
#include "dgGroupOfElements.h"
#include "MElement.h"
#include "MEdge.h"
#include "math.h"
#include "function.h"
extern "C" {
  double pow2(double value){
  	return value*value;
  }
	void inverseSign(dataCacheMap *,fullMatrix<double> &f, fullMatrix<double> &f2){
    for (size_t i=0; i<f.size1(); i++){
      f.set(i,0,-f2(i,0)); 
    }
	}
	void extractComp0(dataCacheMap *,fullMatrix<double> &f, fullMatrix<double> &fvec){
    for (size_t i=0; i<fvec.size1(); i++){
      f.set(i,0,fvec(i,0)); 
    }
	}
	void extractComp1(dataCacheMap *,fullMatrix<double> &f, fullMatrix<double> &fvec){
    for (size_t i=0; i<fvec.size1(); i++){
      f.set(i,0,fvec(i,1)); 
    }
	}
	void gaussian(dataCacheMap *,fullMatrix<double> &f,fullMatrix<double> &xyz){
    for (size_t i=0; i<xyz.size1(); i++){
      //f.set(i,0,1*exp(-0.5*pow2(xyz(i,0)+.2e4)/pow2(.2e4))); 
      f.set(i,0,1*exp(-0.5*pow2(xyz(i,0)-0.5e5)/pow2(0.5e4))); 
      //f.set(i,0,1*exp(-0.5*pow2(xyz(i,0))/pow2(1e4))); 
    }
	}
	void etaSinus(dataCacheMap *,fullMatrix<double> &f,fullMatrix<double> &time){
    for (size_t i=0; i<f.size1(); i++){
      f.set(i,0,sin(2*M_PI*time(i,0)/44714)); 
    }
	}
	void okubo(dataCacheMap *m,fullMatrix<double> &f,fullMatrix<double> &factor){
    double dx = m->getElement()->getEdge(0).length();
    for (size_t i=0; i<f.size1(); i++){
      f.set(i,0,factor(i,0)*2.0551e-4*pow(dx,1.15)); 
    }
	}
	void getVelocityFromDischarge(dataCacheMap *,fullMatrix<double> &f,fullMatrix<double> &Q, fullMatrix<double> &S){
    for (size_t i=0; i<f.size1(); i++){
      f.set(i,0,Q(i,0)/S(i,0)); 
    }
	}
	void getMinusVelocityFromDischarge(dataCacheMap *,fullMatrix<double> &f,fullMatrix<double> &Q, fullMatrix<double> &S){
    for (size_t i=0; i<f.size1(); i++){
      f.set(i,0,-Q(i,0)/S(i,0)); 
    }
	}
	void coordCircle(dataCacheMap *,fullMatrix<double> &s,fullMatrix<double> &XYZ){
    for (size_t i=0; i<s.size1(); i++){
      double R=sqrt(pow2(XYZ(i,0))+pow2(XYZ(i,1)));
      double alpha=atan2(XYZ(i,1),XYZ(i,0));
      s.set(i,0,R*alpha); 
    }
	}
	void sectionNR(dataCacheMap *,fullMatrix<double> &section,fullMatrix<double> &bath, fullMatrix<double> &eta, fullMatrix<double> &width){
    for (size_t i=0; i<section.size1(); i++){
      section.set(i,0,(bath(i,0)+eta(i,0))*width(i,0)); 
    }
	}
	void chezyManningDissipation(dataCacheMap *,fullMatrix<double> &C,fullMatrix<double> &section, fullMatrix<double> &width){
    for (size_t i=0; i<C.size1(); i++){
      double Heff=section(i,0)/width(i,0);
      double manning=0.01;
      double chezy=pow(section(i,0)/(width(i,0)+2*Heff),1./6.)/manning;
      double g=9.81;
      C.set(i,0,g/(pow2(chezy)*Heff)); 
    }
	}
	void merge2scalars(dataCacheMap *,fullMatrix<double> &uv,fullMatrix<double> &u, fullMatrix<double> &v){
    for (size_t i=0; i<uv.size1(); i++){
      uv.set(i,0,u(i,0)); 
      uv.set(i,1,v(i,0)); 
    }
	}
	void widthData(dataCacheMap *m,fullMatrix<double> &w,fullMatrix<double> &bath, fullMatrix<double> &eta, fullMatrix<double> &x){
    std::string tag=m->getGroupOfElements()->getPhysicalTag();
    double w0=50;
    double w1=100;
    if(tag=="river2")
      w0*=2; w1*=2;
    for (size_t i=0; i<bath.size1(); i++){
      w.set(i,0,(w1+(w1-w0)/bath(i,0)*eta(i,0))); 
      //w.set(i,0,100.); 
    }
	}
	void square(dataCacheMap *m,fullMatrix<double> &ww,fullMatrix<double> &w){
    for (size_t i=0; i<w.size1(); i++){
      ww.set(i,0,w(i,0)*w(i,0)); 
    }
	}
	void sectionData(dataCacheMap *m,fullMatrix<double> &s,fullMatrix<double> &bath, fullMatrix<double> &eta){
    std::string tag=m->getGroupOfElements()->getPhysicalTag();
    double w0=50;
    double w1=100;
    if(tag=="river2")
      w0*=2; w1*=2;
    for (size_t i=0; i<s.size1(); i++){
      s.set(i,0,(w1*(eta(i,0)+bath(i,0))+(w1-w0)/bath(i,0)*(pow2(eta(i,0))-pow2(bath(i,0)))/2)  ); 
      //s.set(i,0, 100*(eta(i,0)+bath(i,0))); 
    }
	}
  void riverDistance(dataCacheMap *m,fullMatrix<double> &s, fullMatrix<double> &xyz){
    double u,v,w;
    MElement *e=m->getGroupOfElements()->getElement(3);
    e->getNode(0,u,v,w);
    //printf("%f %f %f\n",u,v,w);
    SPoint3 p;
    e->pnt(u,v,w,p);
    //printf("%f %f %f\n",p[0],p[1],p[2]);
    for (size_t i=0; i<s.size1(); i++){
     // printf("%f %f %f \n",xyz(i,0),xyz(i,1),xyz(i,2));
      s.set(i,0, xyz(i,0)); 
    }
	}
}
