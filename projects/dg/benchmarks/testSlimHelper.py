from gmshpy import *
from dgpy import *

# MESH
model = GModel()
model.load('column.msh')
order=1
dim=3
gc=dgGroupCollection(model,dim,order)
gc.splitGroupsByPhysicalTag()
gc.buildGroupsOfInterfaces()
solution = dgDofContainer(gc, 1)

# LAW & BC & IC
C0 = functionConstant([0])
C1 = functionConstant([1])
CD = functionConstant([1e-12])

law = dgConservationLawAdvectionDiffusion.diffusionLaw(CD)
bndC0 = law.newNeumannBoundary(C0)
bndOut = law.newOutsideValueBoundary("", C1)
solution.L2Projection(C0)
law.addBoundaryCondition('side1' , bndC0)
law.addBoundaryCondition('side2' , bndC0)
law.addBoundaryCondition('side3' , bndC0)
law.addBoundaryCondition('side4' , bndC0)
law.addBoundaryCondition('bottom', bndC0)
law.addBoundaryCondition('top'   , bndOut)

# TEMPORAL (year, month, day, hour, minutes, seconds)
h = slimIterateHelper()
h.setInitialDate(1986, 5, 8, 4, 0, 0.)
h.setFinalDate(2011, 1, 16, 20, 57, 0.)
h.setExportStep(0, 0, 100, 0, 0, 0.)  #export each 100 days
h.setExportDate(2000, 1, 1, 0, 0, 0.) #exceptional exports
h.setExportDate(1990, 1, 1, 0, 0, 0.)
h.setExportDate(2010, 1, 1, 0, 0, 0.)
h.setMaxDt(40*24*3600) #in seconds
h.setNbMaxIter(10000)

# EXPORTS
h.addExport(solution, "solution")
h.addExport(solution, solution.getFunctionGradient(), "gradient")
h.addExport(solution, CD, "diffusion")
h.setFileName("output/testSlimHelper")
#h.exportVtk(True)   #by default false
#h.exportMsh(False)  #by default true

# SOLVER
rk = dgRungeKutta();

# LOOP
while (not h.isTheEnd()):
  dt = h.getNextTimeStepAndExport()
  t = h.getCurrentTime()
  norm  = rk.iterateEuler(law, t, dt, solution)
  print "|ITER| %6d |DT| %.2e |T| %s |NORM| %e" % (h.getIter(), dt, h.getTime(), norm)
