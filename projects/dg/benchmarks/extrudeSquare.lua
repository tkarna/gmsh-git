dofile("extrude.lua")

mmodel = GModel();
mmodel:load("square.geo");
mmodel:mesh(2)

function sigma (x, y, i, nlayers)
  return (-math.sin((2*x+0.5)*math.pi)-1.2)*i/nlayers
end

extrudeMesh(mmodel,sigma,3):save("square_extruded.msh")
