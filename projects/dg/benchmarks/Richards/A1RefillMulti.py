from Richards import *
import time
import math
import string


# Main parameters
# ===============

meshSize = 25
runs = {
#        ('Implicit','Continuous'),
#        ('Implicit','Discontinuous'),
        ('Explicit','Discontinuous',1,1,1 ,1),
#        ('Explicit','Discontinuous',1,1,25,4),
#        ('Explicit','Discontinuous',4,4,4 ,4),
#        ('Explicit','Discontinuous',4,4,25,4),
#        ('Explicit','Discontinuous',4,4,125,4),
#        ('Explicit','Discontinuous',4,4,625,4),
#        ('Explicit','Discontinuous',4,4,3125,4),
       };

maxDTI = 10
maxDTE = 1

def fluxTwoH(val, dt):
  if(dt(0, 0) > 3600 * 2):
    for i in range(val.size1()):
      val.set(i, 0, 0.)
  else:
    for i in range(val.size1()):
      val.set(i, 0, 1e-5)

finalTime = 3600 * 3
exportSteps = '360:120,240'
#exportSteps = '10'

mesh = 'meshes/column1m4layers_1D.msh'
outputname = "A1RefillMulti"


# Soils
# =====

sm = SoilMap()
soil_sand = soilVG(14.5, 2.68, 8.25e-5, 0.43, 0.045)
soil_loam = soilVG(3.6 , 1.56, 2.89e-6, 0.43, 0.078)
soil_clay = soilVG(0.8 , 1.09, 5.56e-6, 0.38, 0.068)
sm.add("VTop" , soil_sand)
sm.add("VMTop", soil_loam)
sm.add("VMBot", soil_clay)
sm.add("VBot" , soil_loam)

interpolator = None
#interpolator = 10  # designed to speed-up computation of soil functions (less precision)


# initial conditions
# ==================

def initialValues(val, xyz):
  for i in range(0, xyz.size1()):
    x = xyz.get(i, 0)
    y = xyz.get(i, 1)
    z = xyz.get(i, 2)
    val.set (i, 0, - z - .5)

print '\nruns:'
for run in runs:
  print '\t',run
print

implStr = ['i', 'implicit'];
explStr = ['e', 'explicit'];
contStr = ['c', 'continuous'];
discStr = ['d', 'discontinuous'];

for run in runs:
  if  (type(run[1]) is str and string.lower(run[1]) in contStr):
    spatial = 'Continuous'
    isContinuous = True
  elif(type(run[1]) is str and string.lower(run[1]) in discStr):
    spatial = 'Discontinuous'
    isContinuous = False
  else:
    Msg.Fatal('bad format for runs')

  if  (type(run[0]) is str and string.lower(run[0]) in implStr):
    R = RichardsImplicit(mesh, sm, isContinuous, interpolator)
    temporal = 'Implicit'
    isImplicit = True
  elif(type(run[0]) is str and string.lower(run[0]) in explStr):
    R = RichardsExplicit(mesh, sm, isContinuous, interpolator)
    n, n, n, k, nbStrongModeH, nbStrongModeW = run
    temporal = 'Explicit ('+str(n)+','+str(k)+'=>'+str(nbStrongModeH)+','+str(nbStrongModeW)+')'
    isImplicit = False
  else:
    Msg.Fatal("bad format for runs")
 
  if (Msg.GetCommRank()==0): print '\nRun: '+temporal, spatial+'\n'

  # Bnd conditions
  # ==============

  upFlux = functionPython(1, fluxTwoH, [R.TIME])
  C0 = functionConstant(0)
  R.bndNeumann(C0, ['side1', 'side2', 'side3', 'side4', 'bottom'])
  R.bndNeumann(upFlux, 'top')

  h = slimIterateHelper()
  h.setInitialDate(0)
  h.setFinalDate(finalTime)
  h.setExport(exportSteps)
  #h.setExportStep(maxDTI)
  nbSubStr = '%03d' % (meshSize) + temporal[0] + spatial[0]
  if(not isImplicit): nbSubStr = nbSubStr + '%d(%d)_%d(%d)' % (n, nbStrongModeH, k, nbStrongModeW)
  h.setFileName("output/" + outputname)
  h.addExport(R.H, nbSubStr + "h")
  h.addExport(R.W, nbSubStr + "th")
  if(not isImplicit): h.addExport(R.H, R.state, nbSubStr + "st")
  h.exportBin(True)
  h.printExport(True)
  h.setNbMaxIter(10000000)


  printEach = 10
  if(isImplicit) : maxDT = maxDTI
  else           : maxDT = maxDTE
  h.setMaxDt(maxDT)
  i = 0; x = time.clock();
  nbIMin = 1000; nbIMax = 0; nbIMean = 0

  INI = functionPython(1, initialValues, [R.XYZ])
  R.setH(INI, maxDT)

  #h.importAll(22, 7200)

  while (not h.isTheEnd()) :
    i = i + 1
    dt = h.getNextTimeStepAndExport()
    t = h.getCurrentTime()

    if(not isImplicit and t > 3600 * 2):
      n = nbStrongModeH
      k = nbStrongModeW

    '''
    if(t > 7200):
      h.setMaxDt(0.1)
      if(t > 8000):
        h.setMaxDt(maxDTI)
    '''
    if(isImplicit):
      nbSub = R.iterate(dt, t)
      nbIMin = min(nbIMin, nbSub)
      nbIMax = max(nbIMax, nbSub)
      nbIMean = nbIMean + nbSub
    else:
      nbSub = R.iterate(dt, t, n, k)

    if (i % printEach == 0) :
      gmass = R.getGlobalMass()
      if (Msg.GetCommRank() == 0) :
        print '|IT|', '{0:6d}'.format(i), '|DT|', dt, '|t|','{0:.2e}'.format(t), '|CPU|', '{0:.1e}'.format( time.clock()-x ),
        if(isImplicit):
          nbIMean = float(nbIMean) / printEach;
          print '|NWits| {0:2d} <{1:4.1f} <{2:2d}'.format(nbIMin, nbIMean, nbIMax),
          nbIMin = 1000; nbIMax = 0; nbIMean = 0
        print '|Mass|', '{0:21.16g}'.format(gmass)
        
    if(nbSub == 0):
      exit() # stops when newton dont converge

  gmass = R.getGlobalMass()
  if(Msg.GetCommRank() == 0) :
    print '|IT|', '{0:6d}'.format(i), '|DT|', dt, '|t|','{0:.2e}'.format(t), '|CPU|', '{0:.1e}'.format( time.clock()-x ),
    print '|Mass|', '{0:21.16g}'.format(gmass)


