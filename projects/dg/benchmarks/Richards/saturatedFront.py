from dgpy import *
import os.path
import time
import math

if(not os.path.exists('output')):
  print 'create first an output directory !!!'

def pow2(x):
  return x*x


# Global parameters
# =================

meshSize = 25
#nbsubiters = map(pow2, range(1, meshSize*6, meshSize / 5))
nbsubiters = [4] #[1,2,4,25]#,100]#,400,900] # [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]

nbsubitersTh = [0, 1, 4, 9, 16]
maxDTI = 5 #60
maxDTE = 1
upBnd = functionConstant(1)
explicitCG = False
implicitCG = explicitCG
alreadyImplicit = False
isMixed = True
finalTime = 60*20
exportSteps = '120:5,20,40,60,80,100'
#exportSteps = '360:5,20,50,100,200,300'
#exportSteps = '1080:10,20,50,100,200,500'

CFunctions = '''
#include "fullMatrix.h"
#include "function.h"
#include "dgDofContainer.h"
extern "C" {
void h_projection (dataCacheMap *, fullMatrix<double> &out,   fullMatrix<double> &hmat, 
                                   fullMatrix<double> &thToHmat, fullMatrix<double> &state) {
  if(state(0,0) >= 0.) { // saturated
  	for(size_t i = 0; i < out.size1(); i++) {
      out.set(i, 0, hmat(i,0) );
  	}
  } else {             // unsaturated
  	for(size_t i = 0; i < out.size1(); i++) {
      out.set(i, 0, thToHmat(i,0) );
  	}
  }
}
void impl_state (dataCacheMap *, fullMatrix<double> &out, fullMatrix<double> &th, fullMatrix<double> &ths) {
  for(size_t i = 0; i < out.size1(); i++) {
    if(th(i, 0) >= ths(i, 0) ) { //* 0.95
      out.set(i, 0, 1);
    } else {
      out.set(i, 0, -1);
    }
  }
}
void K_mod (dataCacheMap *, fullMatrix<double> &out, fullMatrix<double> &eta, fullMatrix<double> &K) {
  for(size_t i = 0; i < out.size1(); i++) {
    double K_main = std::max(K(i, 0), std::max(K(i, 4), K(i, 8)));
    double K_mod = std::min(eta(i, 0), K_main);
    out.set(i, 0, K_mod); out.set(i, 1, 0.   ); out.set(i, 2, 0.   ); 
    out.set(i, 3, 0.   ); out.set(i, 4, K_mod); out.set(i, 5, 0.   ); 
    out.set(i, 6, 0.   ); out.set(i, 7, 0.   ); out.set(i, 8, K_mod);
  }
}
}
'''
tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CFunctions, tmpLib);
Msg.Barrier()


# Mesh
# ====

model = GModel()
model.load('meshes/column_1D_' + str(meshSize) + '.msh')
gc = dgGroupCollection(model, 1, 1) # mesh, dim, order
gc.splitGroupsByPhysicalTag()
#gc.buildGroupsOfInterfaces()

print
print "run for subIters: ", nbsubiters
print


# Variables and constants
# =======================

solution_h = dgDofContainer(gc, 1)
solution_hI = dgDofContainer(gc, 1)
solution_th = dgDofContainer(gc, 1)
solution_tI = dgDofContainer(gc, 1)
cur_h = dgDofContainer(gc, 1)
lst_h = dgDofContainer(gc, 1)
f_h = dgDofContainer(gc, 1)
old_state = dgDofContainer(gc, 1) # ! not 'really' used !
K_lin = dgDofContainer(gc, 9)
lst_C = dgDofContainer(gc, 1)

C0 = functionConstant([0.])
C1 = functionConstant([1.])
C001 = functionConstant([0., 0., 1.])
XYZ = gc.getFunctionCoordinates()
TIME = function.getTime()
DT = function.getDT()
DT.set(maxDTI) # for initial print
minSize = 1. / meshSize / 2. / 4
minSize2 = functionConstant(minSize * minSize)
eta = functionQuotientNew(minSize2, DT)
nbSubIters = functionConstant(1)
#eta_mod = functionProdNew(eta, nbSubIters)


# Soils
# =====

sm = SoilMap()
sm.addDefaultInput("pressure", solution_h.getFunction())
#sm.addDefaultInput("pressure", function.getSolution())
sm.addDefaultInput("water_content", solution_th.getFunction())
soil_sand = soilVG(14.5, 2.68, 8.25000e-5, 0.43, 0.045)
#soil_sand = soilVGm(4.1, 1.964, 7.22e-6, 0.35, 0.02, 6.95e-6, 0.02, 0.35, 0.2875)
sm.add("volume", soil_sand)
K = sm.getFunction("conductivity")
C = sm.getFunction("dRetention")
hToTh = sm.getFunction("retention")
thToH = sm.getFunction("retention_inv")
ths = sm.getFunction("ths")
Ks = sm.getFunction("Ks")
ID = sm.getIDFunction()
#state = sm.getFunction("state_eta", ["eta", "old_state"], [eta, old_state.getFunction()])
state = sm.getFunction("state_eta2", ["eta", "old_state", "conductivity", "capillary_capacity"], 
                                    [eta, old_state.getFunction(), K_lin.getFunction(), C])
stateImpl = functionC(tmpLib, "impl_state"  , 1, [solution_th.getFunction(), ths])
hProj     = functionC(tmpLib, "h_projection", 1, [solution_h.getFunction(), thToH, state]) #or old_state.getFunction()?
hProjImpl = functionC(tmpLib, "h_projection", 1, [solution_h.getFunction(), thToH, stateImpl])
Kmod      = functionC(tmpLib, "K_mod"       , 9, [eta, K]);

def firstComp(val, K):
  for i in range(0, K.size1()):
    val.set(i, 0, K(i, 0))
K1 = functionPython(1, firstComp, [K])


# Conservation Laws
# =================

lawHI = dgConservationLawRichardsHform(K_lin.getFunction(), C001, C0, C) # C only used if not "mixed" ! (otherwise, C used is in newtonkrab.)
lawHI.setParam("lumping", 1) # reduces oscillations
#lawHI.setParam("upwind", 1)
if(isMixed):
  lawHI.setParam("mixed", 1)
lawThI = dgConservationLawRichardsHform(K_lin.getFunction(), C001, C0, None) # used to obtain identic F in th^n+1 - th^n = Dt F
lawThI.setParam("lumping", 1)
lawThI.setH(solution_h.getFunction())
lawThI.setGH(solution_h.getFunctionGradient())

#lawT = dgConservationLawRichardsExplicitTh(K_lin.getFunction(), solution_h.getFunction(), solution_h.getFunctionGradient(), state, thToH, eta)
lawT = dgConservationLawRichardsExplicitH.newForced(K_lin.getFunction(), C001, state, None, C, eta, ID,
                                                      solution_h.getFunction(), solution_h.getFunctionGradient(), False)
lawT.setParam("upwind", 1)
#lawT.setParam("lumping", 1)
lawH = dgConservationLawRichardsExplicitH(K_lin.getFunction(), C001, state, None, C, eta, ID)
lawH.setParam("upwind", 1)
lawD = dgConservationLawRichardsExplicitThDiff(state, eta, f_h.getFunction(), f_h.getFunctionGradient())


# Error estimator
# ===============

Cmod = lawH.getAppliedC()
diffC = functionMinusNew(C, Cmod)
diffH = functionMinusNew(solution_h.getFunction(), lst_h.getFunction())
variationH = functionQuotientNew(diffH, DT)
variationH_sub = functionProdNew(variationH, nbSubIters)
errorOnHApprox = functionProdNew(diffC, variationH_sub)
absErrorOnHApprox = functionAbsNew(errorOnHApprox)


# Bnd conditions
# ==============

bndD0 = lawT.newNeumannBoundary(C0)
bndDF = lawT.newOutsideValueBoundaryGeneric("", [thToH, cur_h.getFunction(), solution_h.getFunction()], [upBnd, upBnd, upBnd], [solution_th.getFunction(), K_lin.getFunction()], [hToTh, Kmod])
#bndDF = lawT.newOutsideValueBoundaryGeneric("", [thToH, cur_h.getFunction(), solution_h.getFunction(),solution_th.getFunction(), K_lin.getFunction()], [upBnd, upBnd, upBnd, hToTh, K])
lawT.addBoundaryCondition(['side1', 'side2', 'side3', 'side4', 'bottom'], bndD0)
lawT.addBoundaryCondition('top'   , bndDF)

bndD0h = lawH.newNeumannBoundary(C0)
bndDFh = lawH.newOutsideValueBoundaryGeneric("", [solution_h.getFunction(), cur_h.getFunction()], [upBnd, upBnd], [K_lin.getFunction()], [K])
#bndDFh = lawH.newOutsideValueBoundaryGeneric("", [solution_h.getFunction(), cur_h.getFunction(),K_lin.getFunction()], [upBnd, upBnd, K])
lawH.addBoundaryCondition(['side1', 'side2', 'side3', 'side4', 'bottom'], bndD0h)
lawH.addBoundaryCondition('top'   , bndDFh)

bndD0hi = lawHI.newNeumannBoundary(C0)
bndDFhi = lawHI.newOutsideValueBoundaryGeneric("", [solution_h.getFunction()], [upBnd], [K_lin.getFunction()], [K])
#bndDFhi = lawHI.newOutsideValueBoundaryGeneric("", [solution_h.getFunction(), K_lin.getFunction()], [upBnd, Ks])
lawHI.addBoundaryCondition(['side1', 'side2', 'side3', 'side4', 'bottom'], bndD0hi)
lawHI.addBoundaryCondition('top'   , bndDFhi)

bndD0Thi = lawThI.newNeumannBoundary(C0)
bndDFThi = lawThI.newOutsideValueBoundaryGeneric("", [solution_h.getFunction()], [upBnd], [K_lin.getFunction()], [K])
lawThI.addBoundaryCondition(['side1', 'side2', 'side3', 'side4', 'bottom'], bndD0Thi)
lawThI.addBoundaryCondition('top'   , bndDFThi)

bndD0d = lawD.newNeumannBoundary(C0)
bndDFhd = lawD.newOutsideValueBoundaryGeneric("", [solution_h.getFunction()], [upBnd], [solution_th.getFunction()], [hToTh])
lawD.addBoundaryCondition(['side1', 'side2', 'side3', 'side4', 'bottom'], bndD0d)
lawD.addBoundaryCondition('top', bndDFhd)


# global computations
# ===================

globalMass = dgFunctionIntegrator(gc, solution_th.getFunction(), solution_th)
massT = fullMatrixDouble (1, 1)
globalMass.compute(massT)

diffh = functionMinusNew(solution_hI.getFunction(), solution_h.getFunction()) # rem: norm already naturally positive...
errorToImplicitH = dgFunctionIntegrator(gc, diffh)
errorH = fullMatrixDouble (1, 1)

diffh2 = functionProdNew(diffh, diffh)
error2ToImplicitH = dgFunctionIntegrator(gc, diffh2)
error2H = fullMatrixDouble (1, 1)

difft = functionMinusNew(solution_tI.getFunction(), solution_th.getFunction())
errorToImplicitT = dgFunctionIntegrator(gc, difft)
errorT = fullMatrixDouble (1, 1)

difft2 = functionProdNew(difft, difft)
error2ToImplicitT = dgFunctionIntegrator(gc, difft2)
error2T = fullMatrixDouble (1, 1)


# explicit solvers
# ================

sysT = linearSystemPETScDouble ()
sysT.setParameter("petscOptions", "-pc_type ilu") #-ksp_monitor
dofManagerT = dofManagerH = None
if(explicitCG):
  dofManagerT = dgDofManager.newCG(gc, 1, sysT)
  dofManagerH = dofManagerT
  L2projcont = L2ProjectionContinuous(dofManagerT)
else: #bec. mass lumping
  dofManagerT = dgDofManager.newDG(gc, 1, sysT)

rkT = dgERK(lawT, dofManagerT, DG_ERK_EULER)
rkH = dgERK(lawH, dofManagerH, DG_ERK_EULER)
rkD = dgERK(lawD, dofManagerH, DG_ERK_EULER)
limiter = dgSlopeLimiter2(lawT)# dgSlopeLimiterField2(lawT, thToH, solution_h.getFunction(), hToTh) 
limiterH = dgSlopeLimiter2(lawH)

rkTI = dgERK(lawThI, dofManagerT, DG_ERK_EULER)

# implicit pre-processing (bef each Newton sub-step)
# ==================================================

nbSubNew = 0
def preProcLinearize():
  global K_lin, K, nbSubNew, lst_C
  curSolNewtonDof = newtonKrabbenhoft.getCurrentSolution()
  K_lin.interpolate(K, curSolNewtonDof, solution_h)
  #lst_C.interpolate(C, curSolNewtonDof, solution_h)
  #curSolNewtonDof.exportMsh("output/subNewtonVar-%06d" % (nbSubNew), nbSubNew, nbSubNew)
  #curSolNewtonDof.exportFunctionMsh(C_lin.getFunction(), "output/subNewtonC-%06d" % (nbSubNew), t, nbSubNew)
  #nbSubNew = nbSubNew + 1

 # pure hform
if(not isMixed):
  CdiffH = functionProdNew(lst_C.getFunction(), diffH)
  iterTh = functionSumNew(solution_th.getFunction(), CdiffH)


# initial conditions
# ==================

def ini(val, xyz):
  for i in range(0, xyz.size1()):
    x = xyz.get(i, 0)
    y = xyz.get(i, 1)
    z = xyz.get(i, 2)
    #if(z > -0.1):
    #  val.set(i, 0, 0)
    #else:
    val.set (i, 0, - 5)
INI = functionPython(1, ini, [XYZ])

def initialization():
  global solution_h, solution_th, lst_h, K_lin, C_lin, K, C
  solution_h.interpolate(INI)
  solution_h.scatter()
  #solution_th.copy(solution_h) #FIXME
  solution_th.L2Projection(hToTh)
  solution_th.scatter()
  lst_h.copy(solution_h)
  #K_lin.set([1e-6, 0, 0, 0, 1e-6, 0, 0, 0, 1e-6])
  #C_lin.set(1.)
  preProcLinearize()


# IMPLICIT
# ========

if (Msg.GetCommRank()==0) :
  print 'IMPLICIT\n'

sys = linearSystemPETScDouble ()
sys.setParameter("petscOptions", "-pc_type ilu") #-ksp_monitor
if(implicitCG):
  dofManager = dgDofManager.newCG(gc, 1, sys)
else:
  dofManager = dgDofManager.newDG(gc, 1, sys)
#thTol = functionConstant(-1e100) #0.95) # disable the krabbenhoft switch -> h form everywhere
thTol = functionScaleNew(ths, 0.95)
newtonKrabbenhoft = dgNewtonKrabbenhoft(dofManager, lawHI, hToTh, thToH, C, thTol, solution_th.getFunction())
#IEH = dgDIRK(lawGHb, dofH, 1)
IEH = dgDIRK(newtonKrabbenhoft, 1)
IEH.getNewton().setRtol(1e-5)
IEH.getNewton().setAtol(1e-9)
IEH.getNewton().setMaxIt(50)
IEH.getNewton().setVerb(1)

preProcLin = processPython(preProcLinearize)
newtonKrabbenhoft.setPreProc(preProcLin)

initialization()

h = slimIterateHelper()
h.setInitialDate(0)
h.setFinalDate(finalTime)
#h.setFinalDate(15)
h.setExport(exportSteps)
#h.setExportStep(maxDTI)
nbSubStr = '%03d' % (meshSize) + 'CI'
h.setFileName("output/satFront" + nbSubStr)
h.addExport(solution_h, "h" + nbSubStr)
h.addExport(solution_th, "th" + nbSubStr)
#h.addExport(f_h, "f_h" + nbSubStr)
#h.addExport(solution_h, C, "C" + nbSubStr)
#h.addExport(solution_h, K1, "K" + nbSubStr)
h.exportBin(True)
h.printExport(True)
h.setNbMaxIter(10000000)

if not alreadyImplicit :

  printEach = 1
  i=0; x = time.clock(); t=0; dt=1;
  h.setMaxDt(maxDTI/20.)

  while (not h.isTheEnd()) :
    i = i + 1
    preProcLinearize()
    nbSubIters.set(1)
    if(i > 10):
      h.setMaxDt(maxDTI)
    dt = h.getNextTimeStepAndExport()
    t = h.getCurrentTime()

    #cur_h.copy(solution_h)
    #lst_h.copy(solution_h)

    nbNewtonSub = IEH.iterate(solution_h, dt, t)
    nbSubNew = 0
    #f_h.L2Projection(hToTh) #, solution_h)
    #f_h.interpolate(hToTh) #, solution_h)

    '''
    if(isMixed):
      rkTI.iterate(solution_th, dt, t)
      solution_h.interpolate(hProjImpl)
    else:
      solution_th.interpolate(iterTh)
      solution_h.interpolate(hProjImpl)
    '''
    solution_th.interpolate(hToTh)

    #cur_h.copy(solution_h)
    ##K_lin.interpolate(K)
    #rkT.iterate(solution_th, dt, t)
    ##limiter.apply(solution_th)

    #solution_h.interpolate(hProjImpl)
    #solution_h.L2Projection(hProjImpl)
    #solution_h.scatter()
#    for j in range(nbsubitersTh):
#      rkD.iterate(solution_th, dt, t)

    #if i == 10:
    #  break
    if (i % printEach == 0) :
      globalMass.compute(massT)
      if (Msg.GetCommRank() == 0) :
        print '|IT|', '{0:6d}'.format(i), '|DT|', dt, '|t|','{0:.2e}'.format(t), '|CPU|', '{0:.1e}'.format(time.clock()-x),
        print '|Mass|', '{0:21.16g}'.format(massT.get(0,0))

    if(nbNewtonSub == 0):
      exit()
  solution_hI.copy(solution_h)
  solution_tI.copy(solution_th)
else:
  h.importAll(16, finalTime)
  solution_hI.copy(solution_h)
  solution_tI.interpolate(hToTh)



f = open('satChange_error_%03d' % (meshSize), 'w')

# EXPLICIT
# ========

print '\nEXPLICIT'

for n in nbsubiters:
  for k in nbsubitersTh:

    if (Msg.GetCommRank()==0) :
      print '\n%d subiters, %d subiters th\n' % (n, k)

    h = slimIterateHelper()
    h.setInitialDate(0)
    h.setFinalDate(finalTime)
    h.setExport(exportSteps)
    #h.setExportStep(maxDTE)
    #h.setFinalDate(500*maxDTE)
    nbSubStr = '%03d' % (meshSize) + 'E%05d' % (n) + ':%04d' % (k)
    h.setFileName("output/satFront" + nbSubStr)
    h.addExport(solution_h, "h" + nbSubStr)
    h.addExport(solution_th, "th" + nbSubStr)
    h.addExport(f_h, "f(h)" + nbSubStr)
    h.addExport(solution_h, state, "State" + nbSubStr)
  #  h.addExport()
  #  h.addExport(solution_h, Cmod, "Cmod" + nbSubStr)
  #  h.addExport(solution_h, C, "C" + nbSubStr)
  #  h.addExport(f_h, "f_h" + nbSubStr)
  #  h.addExport(solution_h, absErrorOnHApprox, "error" + nbSubStr)
    h.exportBin(True)
    h.printExport(True)
    h.setNbMaxIter(10000000)
    printEach = 1
    i=0; x = time.clock(); t=0; dt=1;

    initialization()
    K_lin.interpolate(K)

    nbSub = 0

    while (not h.isTheEnd()) :
      i = i + 1
      nbSubIters.set(n)
      h.setMaxDt(maxDTE)
      dt = h.getNextTimeStepAndExport()
      t = h.getCurrentTime()

      #if (i == 15):#16):
      #  exit()

      cur_h.copy(solution_h)

      for j in range(n - 1):
        rkH.iterate(solution_h, dt / n, t + dt / n * j)
        if(not explicitCG):
          limiterH.apply(solution_h)
        K_lin.interpolate(K)
        #solution_h.exportMsh("output/satFront025-h%06d" % (nbSub), t, nbSub, "h")
        #nbSub = nbSub + 1
        #if(i == 7):
          #solution_h.exportMsh("output/subExpl-%06d" % (nbSub), t  + dt / n * j, nbSub)
          #nbSub = nbSub + 1
      lst_h.copy(solution_h)
      rkH.iterate(solution_h, dt / n, t + dt - dt / n)
      #solution_h.exportMsh("output/satFront025-h%06d" % (nbSub), t, nbSub, "h")
      if(not explicitCG):
        limiterH.apply(solution_h)
      #if(i == 7):
        #solution_h.exportMsh("output/subExpl-%06d" % (nbSub), t + dt - dt / n, nbSub)
        #nbSub = nbSub + 1
      K_lin.interpolate(K) #FIXME Kmod

      #solution_h.exportFunctionMsh(state, "output/satFront025-s%06d" % (nbSub), t, nbSub, "state")
      #cur_h.copy(solution_h)
      rkT.iterate(solution_th, dt, t)
      #solution_th.exportMsh("output/satFront025-th%06d" % (nbSub), t, nbSub, "th")
      #nbSub = nbSub + 1
      if(not explicitCG):
        limiter.apply(solution_th)
      #solution_h.exportFunctionMsh(state, "output/satFront025-s%06d" % (nbSub), t, nbSub, "state")    
      
      if(explicitCG):
        L2projcont.apply(solution_h, hProj)
      else:
        #solution_h.interpolate(hProj)
        solution_h.L2Projection(hProj)
      #solution_h.copy(solution_th)
      
      #solution_h.exportMsh("output/satFront025-h%06d" % (nbSub), t, nbSub, "h")    
      #nbSub = nbSub + 1
      solution_h.scatter()
      K_lin.interpolate(K)
      f_h.L2Projection(hToTh) #, solution_h)
      for j in range(k):
        rkD.iterate(solution_th, dt, t)

      if (i % printEach == 0) :
        globalMass.compute(massT)
        if (Msg.GetCommRank() == 0) :
          print '|IT|', '{0:6d}'.format(i), '|DT|', dt, '|t|','{0:.2e}'.format(t), '|CPU|', '{0:.1e}'.format(time.clock()-x),
          print '|Mass|', '{0:21.16g}'.format(massT.get(0,0))


  errorToImplicitH.compute(errorH)
  error2ToImplicitH.compute(error2H)
  errorToImplicitT.compute(errorT)
  error2ToImplicitT.compute(error2T)
  print 'error on H (%04d): L1 = %e   L2 = %e\nerror on T: L1 = %e   L2 = %e' % (n, errorH(0,0), math.sqrt(error2H(0,0)), errorT(0,0), math.sqrt(error2T(0,0)))
  print >>f, '%05d' % (n), '%21.16e\t%21.16e\t%21.16e\t%21.16e' % (errorH.get(0,0), math.sqrt(error2H(0,0)), errorT(0,0), math.sqrt(error2T(0,0)))

