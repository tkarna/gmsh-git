from Richards import *
import time
import math
import string


# Main parameters
# ===============

meshSize = 25
runs = {
#        ('Implicit','Discontinuous'),
#        ('Explicit','Discontinuous',1,1),
#        ('Explicit','Discontinuous',4,4),
        ('Explicit','Discontinuous',1,0),
#        ('Explicit','Discontinuous',25,4),
#        ('Explicit','Discontinuous',4,25),
#        ('Explicit','Discontinuous',25,25)
       };

maxDTI = 10
maxDTE = 10

#TODO: special bnd cond with minPressure !
upFlux = functionConstant(-3.47222e-08) # 0.3 cm/j
minPressure = functionConstant(-100.)

finalTime = 1 * 24 * 3600
exportSteps = '3600:100,600,1200,2400'

#mesh = 'meshes/column_3D_' + str(meshSize) + '.msh'
mesh = 'meshes/column_1D_' + str(meshSize) + '.msh'
outputname = "A2Evap"

# Soils
# =====

sm = SoilMap()
soil_sand = soilVG(15, 3, 1.15740740741e-4, 0.43, 0.045)
sm.add('volume', soil_sand)

interpolator = None
#interpolator = 10  # designed to speed-up computation of soil functions (less precision)


# initial conditions
# ==================

def initialValues(val, xyz):
  for i in range(0, xyz.size1()):
    x = xyz.get(i, 0)
    y = xyz.get(i, 1)
    z = xyz.get(i, 2)
    val.set (i, 0, - .4)


print '\nruns:'
for run in runs:
  print '\t',run
print

implStr = ['i', 'implicit'];
explStr = ['e', 'explicit'];
contStr = ['c', 'continuous'];
discStr = ['d', 'discontinuous'];

for run in runs:
  if  (type(run[1]) is str and string.lower(run[1]) in contStr):
    spatial = 'Continuous'
    isContinuous = True
  elif(type(run[1]) is str and string.lower(run[1]) in discStr):
    spatial = 'Discontinuous'
    isContinuous = False
  else:
    Msg.Fatal('bad format for runs')

  if  (type(run[0]) is str and string.lower(run[0]) in implStr):
    R = RichardsImplicit(mesh, sm, isContinuous, interpolator)
    temporal = 'Implicit'
    isImplicit = True
  elif(type(run[0]) is str and string.lower(run[0]) in explStr):
    R = RichardsExplicit(mesh, sm, isContinuous, interpolator)
    n, n, n, k = run
    temporal = 'Explicit ('+str(n)+','+str(k)+')'
    isImplicit = False
  else:
    Msg.Fatal("bad format for runs")
 
  if (Msg.GetCommRank()==0): print '\nRun: '+temporal, spatial+'\n'

  # Bnd conditions
  # ==============

  C0 = functionConstant(0)
  #R.bndNeumann(C0, ['north', 'east', 'south', 'west', 'bottom'])
  R.bndNeumann(C0, ['bottom'])
  R.bndEvaporation(upFlux, minPressure, 'top')

  h = slimIterateHelper()
  h.setInitialDate(0)
  h.setFinalDate(finalTime)
  h.setExport(exportSteps)
  #h.setExportStep(maxDTI)
  nbSubStr = '%03d' % (meshSize) + temporal[0] + spatial[0]
  if(not isImplicit): nbSubStr = nbSubStr + '%d_%d' % (n, k)
  h.setFileName("output/" + outputname)
  h.addExport(R.H, nbSubStr + "h")
  h.addExport(R.W, nbSubStr + "th")
  h.addExport(R.H, R.state, nbSubStr + "State")
  h.exportBin(True)
  h.printExport(True)
  h.setNbMaxIter(10000000)

  printEach = 10
  if(isImplicit) : h.setMaxDt(maxDTI)
  else           : h.setMaxDt(maxDTE)
  i = 0; x = time.clock();

  INI = functionPython(1, initialValues, [R.XYZ])
  R.setH(INI)

  while (not h.isTheEnd()) :
    i = i + 1
    dt = h.getNextTimeStepAndExport()
    t = h.getCurrentTime()

    if(isImplicit):
      nbSub = R.iterate(dt, t)
    else:
      nbSub = R.iterate(dt, t, n, k)

    if (i % printEach == 0) :
      gmass = R.getGlobalMass()
      if (Msg.GetCommRank() == 0) :
        print '|IT|', '{0:6d}'.format(i), '|DT|', dt, '|t|','{0:.2e}'.format(t), '|CPU|', '{0:.1e}'.format( time.clock()-x ),
        print '|Mass|', '{0:21.16g}'.format(gmass)

    if(nbSub == 0):
      exit() # stops when newton dont converge

  gmass = R.getGlobalMass()
  if(Msg.GetCommRank() == 0) :
    print '|IT|', '{0:6d}'.format(i), '|DT|', dt, '|t|','{0:.2e}'.format(t), '|CPU|', '{0:.1e}'.format( time.clock()-x ),
    print '|Mass|', '{0:21.16g}'.format(gmass)


