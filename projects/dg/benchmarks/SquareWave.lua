l=1
h=1

PI=3.14159265358979323846264338327950288419716939937510

u0=1
u1=2
u2=4

c=1


print('PI= ', PI)

function initial_condition2( XYZ , FCT2 )
  for i=0,XYZ:size1()-1 do
    X = XYZ:get(i,0)
    Y = XYZ:get(i,1)
    VV= u0*math.cos(PI*X/l)*math.cos(PI*Y/h)+u1*math.cos(2*PI*X/l)*math.cos(2*PI*Y/h)+u2*math.cos(3*PI*X/l)*math.cos(3*PI*Y/h)
    FCT2:set(i,0,VV) 
    FCT2:set(i, 1, 0.0)
    FCT2:set(i, 2, 0.0)
  end
end

function initial_condition( XYZ , FCT )
  for i=0,XYZ:size1()-1 do
    X = XYZ:get(i,0)
    Y = XYZ:get(i,1)
    V= u0*math.cos(PI*X/l)*math.cos(PI*Y/h)+u1*math.cos(2*PI*X/l)*math.cos(2*PI*Y/h)+u2*math.cos(3*PI*X/l)*math.cos(3*PI*Y/h)
    FCT:set(i,0,V) 
  end
end

function Acoeff( XYZ, M, N, FCT )
  m=M:get(0, 0)
  n=N:get(0, 0)
  for i=0,XYZ:size1()-1 do
    X = XYZ:get(i,0)
    Y = XYZ:get(i,1)
    W= u0*math.cos(PI*X/l)*math.cos(PI*Y/h)+u1*math.cos(2*PI*X/l)*math.cos(2*PI*Y/h)+u2*math.cos(3*PI*X/l)*math.cos(3*PI*Y/h)
    FCT:set(i,0,4/l/h*(W)*math.cos(n*PI*X/l)*math.cos(m*PI*Y/h)) 
  end
end

function solT( XYZ, M, N, A_mn, T, FCT )
  m=M:get(0, 0)
  n=N:get(0, 0)
  AMN=A_mn:get(0, 0)
  t=T:get(0, 0)
  for i=0,XYZ:size1()-1 do
    X = XYZ:get(i,0)
    Y = XYZ:get(i,1)
    FCT:set(i,0,math.cos(n*PI*X/l)*math.cos(m*PI*Y/h)*AMN*math.cos(math.sqrt(m*m*PI*PI/h/h+n*n*PI*PI/l/l)*c*t)) 
  end
end


function toIntegrate( sol, FCT3 )
  for i=0,sol:size1()-1 do
    s = sol:get(i,0)
    FCT3:set(i,0,s) 
  end
end

function computeSquareError(ana, num, FCT4)
  for i=0,sol:size1()-1 do
    an = ana:get(i,0)
    nu = num:get(i,0)
    FCT4:set(i,0,(an-nu)*(an-nu)) 
  end
end




order = 1
print'*** Loading the mesh and the model ***'
myModel   = GModel  ()
myModel:load ('wrect.geo')	
if (order == 1) then
   myModel:load ('wrect.msh')
elseif (order == 2) then
   myModel:load ('wrect2.msh')
elseif (order == 3) then
   myModel:load ('wrect3.msh')
elseif (order == 4) then
   myModel:load ('wrect4.msh')
elseif (order == 5) then
   myModel:load ('wrect5.msh')
end

dim=2

xyzF = functionCoordinates.get()

FS = functionLua(1, 'initial_condition', {xyzF})

print '********* START ANALYTICAL SOLUTION ************'

GCa=dgGroupCollection(myModel,dim,order)

GCa:buildGroupsOfInterfaces()

solutionAn=dgDofContainer(GCa,1)
solutionAn:L2Projection(FS)
solutionAn:exportMsh("solution", 0., 0) 


Nn=3
Nm=3

Amn=fullMatrix(Nm, Nn)
for m=1, Nm do
  for n=1, Nn do
    test_integrator=dgFunctionIntegrator(functionLua(2, 'Acoeff', {functionCoordinates.get(),functionConstant({n}) ,functionConstant({m})}))
    Sol=fullMatrix(1, 1)
    test_integrator:compute(solutionAn, Sol)
    Amn:set(m-1, n-1, Sol:get(0, 0))
  end
end

Amn:print("Amn")
t=0
deltaT=0.01
solutionTmp=dgDofContainer(GCa, 1)
solutionFinal=dgDofContainer(GCa, 1)
i=0
while i<11 do
  solutionFinal:setAll(0.0)
  solutionTmp:setAll(0.0)
  print(string.format("i=%d, t=%e", i, t))
 for m=1, Nm do
   for n=1, Nn do
   M=functionConstant({m})
   N=functionConstant({n})
   a=Amn:get(m-1, n-1)
   A=functionConstant({a})
   T=functionConstant({t})
   interm = functionLua(1, 'solT', {xyzF, M, N, A, T})
   solutionTmp:L2Projection(interm)
   solutionFinal:axpy(solutionTmp, 1.0)
   end
 end
 solutionFinal:exportMsh(string.format("output/wave_AN-%06d", i), 0., 0)
 t=t+deltaT
 i=i+1
end

print '********* END ANALYTICAL SOLUTION ************'


print '********* START DGM SOLUTION ************'

law = dgConservationLawWaveEquation(dim)
law:addBoundaryCondition('Walls',law:newBoundaryWall())
FS2 = functionLua(3, 'initial_condition2', {xyzF})
GC=dgGroupCollection(myModel,dim,order)
solTmp=dgDofContainer(GC,3)
solTmp:L2Projection(FS2)

-- choose the Multirate Runge-Kutta method you want to test
RK=1

--[[
    1: Runge-Kutta 2a Conservative (Base method of Order 2,  2 Stages)
    2: Runge-Kutta 2b Conservative (Base method of Order 2,  2 Stages)
    3: Runge-Kutta 33 Conservative (Base method of Order 3,  3 Stages)
    4: Runge-Kutta 43 Conservative (Base method of Order 3,  4 Stages)
    5: Runge-Kutta 44 Conservative (Base method of Order 4,  4 Stages)
    6: Runge-Kutta 43 NC, Order 3  (Base method of Order 3,  4 Stages)
--]]

if (RK == 1) then
  multirateRK=dgRungeKuttaMultirateConservative.new2a(GC,law)
elseif (RK == 2) then
  multirateRK=dgRungeKuttaMultirateConservative.new2b(GC,law)
elseif (RK == 3) then
  multirateRK=dgRungeKuttaMultirateConservative.new33(GC,law)
elseif (RK == 4) then
  multirateRK=dgRungeKuttaMultirateConservative.new43(GC,law)
elseif (RK == 5) then
  multirateRK=dgRungeKuttaMultirateConservative.new44(GC,law)
elseif (RK == 6) then
  multirateRK=dgRungeKuttaMultirate43(GC,law)
end

-- number of groups
nG=2
-- Split Groups for Multirate,  choose number of groups,  compute the reference time step dt
dt=multirateRK:splitForMultirate(nG,   solTmp)
--RK44=dgRungeKutta()
--dt=0.01
--GC:buildGroupsOfInterfaces()
solution=dgDofContainer(GC,3)
solution:L2Projection(FS2)
--solution:exportGroupIdMsh()
--solution:exportMultirateGroupMsh()


print('DT=',dt)

norm=solution:norm()

print('Norm: ', norm)
time=0
i=0
integrator=dgFunctionIntegrator(functionLua(1, 'toIntegrate', {functionSolution.get()}))

print('*** ITER-START ***')
w=fullMatrix(1,1);
integrator:compute(solution,w)

wref=w:get(0,0,0)

print'*****************************************'
print('integral (Multirate RK) : ',wref)
print'******************************************'

nbExport = 0
while i<101 do
   norm = multirateRK:iterate(time, dt, solution)
   time=time+dt
   
   if (i % 1 == 0) then 
      print('*** ITER ***',i)
      print'-----------------------------------------'
      integrator:compute(solution, w)
      print('integral (Multirate RK) : ',w:get(0,0,0))
      
      print(string.format("Mass conservation relative error:\n MR-RK=%0.16e",math.abs((wref-w:get(0,0,0))/wref)));
      print'-----------------------------------------'
   end
   if (i % 10 == 0) then 
      solution:exportMsh(string.format("output/wave_RK%d-%06d",RK,  i), time, nbExport)
      nbExport  = nbExport +1
   end
   i=i+1
end

--newIntegrator=dgFunctionIntegrator(functionLua(1, 'computeSquareError',))
--w=fullMatrix(1, 1)
--newIntegrator:compute
print'*** done ***'
