cl1 = 1;

le=3.14*6378137;
lp=3.14*6356752;
Point(1) = {-le, -lp/2, 0, cl1};
Point(2) = {-le, lp/2, 0, cl1};
Point(3) = {le, lp/2, 0, cl1};
Point(4) = {le, -lp/2, 0, cl1};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

Line Loop(4) = {1,2,3,4};
Plane Surface(5) = {4};
Physical Surface('inlet') = {5};
Field[1] = MathEval;
Field[1].F = "500000";
Background Field = 1;
