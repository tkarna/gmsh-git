# -*- coding: utf-8 -*-
from dgpy import *
from gmshpy import *
from scipy import *    
from matplotlib.pyplot import *         
from numpy.linalg import *
import time, math, os

meshFile =   "rectBoxD.msh" #"atlBox.msh" #"boxInf_00.msh" #"rectBoxC.msh"
meshFile2d = "rectBoxD2d.msh"

model = GModel()
model.load(meshFile)
msg = Msg()
order = 1
dimension = 3
groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByVerticalLayer(["outlet"])
groups.buildGroupsOfInterfaces()

model2d = GModel()
model2d.load (meshFile2d)
groups2d = dgGroupCollection(model2d, dimension-1, order)
groups2d.buildGroupsOfInterfaces()

columnInfo = dgSW3dVerticalColumns(groups,groups2d,["outlet"])

#traitement des données
#----------------------
def lonLat(coord,xyz):
  for i in range(xyz.size1()) :
     re = 3.14*6378137 
     rp = 3.14*6356752
     x = xyz.get(i,0)
     y = xyz.get(i,1)
     z = xyz.get(i,2)
     sol2 = y*90/(rp/2)
     sol1 = x*180/(re)+180
     if (sol1<0):
        sol1 = 0
     if (sol1>360):
        sol1 = 360
     if (sol2<-90):
       sol2= -90
     if (sol2>90):
       sol2 = 90
     coord.set(i, 0, sol1)
     coord.set(i, 1, sol2)
     coord.set(i, 2, z)

#calcul de la pression hydrostatique
def hydrostaticpressure (p,xyz) :
   for i in range(xyz.size1()) :
     z = xyz.get(i,2)
     p.set(i,0,0)#-1000*9.81*z/10000)
     
#equation d'etat lineaire
def stateEq(rh,te,sa,xyz):
  for i in range(xyz.size1()):
    temperature=te.get(i,0)
    salinity=sa.get(i,0)
    rh.set(i,0,1028*(1-1.7e-4*(temperature-9.84)+7.6e-4*(salinity-35)))

def stateLevitus(rhoB,te,sa,pr,xyz):
  for i in range(xyz.size1()):
    t=te.get(i,0)
    s=sa.get(i,0)
    p=pr.get(i,0)
    a=[-0.981761e-1,0.434573e-1,0.805734e0,-0.742029e-2,-0.280374e-2,0.360725e-4,0.310070e-4]
    b=[0.510908e-2,-0.370272e-4,-0.99477e-5,0.66277e-6,0.23342e-6,-0.11949e-8,-0.581815e-8]
    g=[-0.54763e-7,0.590608e-9,0.159405e-9,-0.322632e-11,-0.677365e-21,-0.246374e-13,0.108653e-14]
    c=[a[0]+b[0]*p+g[0]*p*p,a[1]+b[1]*p+g[1]*p*p,a[2]+b[2]*p+g[2]*p*p,a[3]+b[3]*p+g[3]*p*p,a[4]+b[4]*p+g[4]*p*p,a[5]+b[5]*p+g[5]*p*p,a[6]+b[6]*p+g[6]*p*p]
    rhoB.set(i,0,c[0]+c[1]*t+c[2]*s+c[3]*t*t+c[4]*s*t+c[5]*t*t*t+c[6]*s*t*t*t)

#calcul de la pente
def slopefunction (sl,grxyz,nn) :
   for i in range(grxyz.size1()) :
     grx = grxyz.get(i,0)
     gry = grxyz.get(i,1)
     grz = -nn.get(i,0)*1027/9.81
     Sx = -grx/grz
     Sy = -gry/grz
     Slim = 0.01
     if (Sx<0) :
       absSx = -Sx
     else :
       absSx = Sx 
     if (Sy<0) :
       absSy = -Sy
     else :
       absSy = Sy
     sl.set(i,0,0.5*(1+math.tanh((0.01-absSx)/0.001))*Sx)
     sl.set(i,1,0.5*(1+math.tanh((0.01-absSy)/0.001))*Sy)

#Eddy Ferrari
def cste( cc, NN) :
   for i in range(NN.size1()):
      n2 = NN.get(i,0)
      if (n2<0):
        n2 = 0
      val = math.sqrt(n2)*1027/math.pi
      cc.set(i,0,val)
      if (val < 0.1) :
        cc.set(i,0,0.1)
def termR(indR ,gradb) :
   for i in range(gradb.size1()):
      gx = gradb.get(i,0)
      gy = gradb.get(i,1)
      indR.set(i,0,gx*9.81/1027)
      indR.set(i,1,gy*9.81/1027)
      indR.set(i,2,0)

#Diffusion normale (//GOTM)
def diffu(d,xyz):
  for i in range(xyz.size1()):
    z = xyz.get(i,2)
    d.set(i,0,0.0001/2*(1.1+0.9*cos(z*math.pi/200)))
#Diffusion verticale
def mixing(Kc,n2):
   for i in range(n2.size1()) :
    n = n2.get(i,0)
    Kac = 0.0
    if ((-1027/9.81*n)>0) :
      Kac = 1
    Kc.set(i,0,Kac)
#Sommation des diffusions
def diffutot(di,nuN,nuK):
  for i in range(nuN.size1()):
    di.set(i,0,nuN.get(i,0)+nuK.get(i,0))

#Vitesse de GM
def velocityGM (h,gradpsi) :
  for i in range(gradpsi.size1()):
    gradXx = gradpsi.get(i,0)
    gradXz = gradpsi.get(i,2)
    gradYy = gradpsi.get(i,4)
    gradYz = gradpsi.get(i,5)
    h.set(i,0,gradXz)
    h.set(i,1,gradYz)
    h.set(i,2,(-gradXx-gradYy))

#Programme de base
#----------------

#maillage
XYZ = groups.getFunctionCoordinates()
meshlonlat = dgDofContainer(groups, 3)
coord = functionPython(3,lonLat,[XYZ])
meshlonlat.interpolate(coord)
meshlonlat.exportMsh('ll',0,0)

#creation de la diffusivité normale (après par GOTM)
nuDof = dgDofContainer(groups,1)
d = functionPython(1,diffu,[XYZ])
nuDof.interpolate(d)
nuDof.exportMsh('Output/nuN',0,0)

#lecture des données et stockage
nnDof = dgDofContainer(groups, 1)
N2 = nnDof.getFunction()
TDof = dgDofContainer(groups,1)
dz = [0,10,20,30,50,75,100,125,150,200,250,300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500]
tempData = ncDataDz("tempZon4.nc","ocean_temp","lon","lat",dz)
tempFunc = functionNcDataDz(tempData,coord)
TDof.interpolate(tempFunc)
TDof.exportMsh('T',0,0)
T = TDof.getFunction()

SDof = dgDofContainer(groups,1)
salData = ncDataDz("salZon4.nc","ocean_sal","lon","lat",dz)
salFunc = functionNcDataDz(salData,coord)
SDof.interpolate(salFunc)
SDof.exportMsh('S',0,0)
S = SDof.getFunction()

#creation de la densite
#densite potentielle donc p=0
p = functionPython (1,hydrostaticpressure,[XYZ])
#press = dgDofContainer(groups,1)
#press.interpolate(p)
#press.exportMsh('P',0,0)
rhoFunc = functionStateEquation(S,T,p)                  #equation de Jackett et McDougall ***
#rhoFunc=functionPython(1,stateEq,[T,S,XYZ])            #equation d etat linaire
#rhoFunc=functionPython(1,stateLevitus,[T,S,p,XYZ])     #equation UNECSO utilisée par Levitus
rhoDof = dgDofContainer(groups,1)
rhoDof.interpolate(rhoFunc)
rhoDof.exportMsh('rho_0',0,0)

#programme general
#--------------------

#Containers et definitions necessaires pour la diffusion
nn = fullMatrixDouble(1,1)
M = fullMatrixDouble(1,1)
nuV = dgDofContainer(groups,1)
#calcul de la diffusivité verticale supplémentaire
Kc = functionPython(1,mixing,[nnDof.getFunction()])
#Combinaison des diffusivités
nuCDof = dgDofContainer(groups,1)
di = functionPython(1,diffutot,[d,Kc])
nuCDof.interpolate(di)
nuCDof.exportMsh('Output/nuI',0,0)

#creation of dofContainer
gradrhodDof = dgDofContainer (groups, 3)
gradrhodDofTemp = dgDofContainer (groups, 3)
gradrhoDof = dgDofContainer (groups, 3)


#calcul du gradient de la densite
gradrhodEq = dgFEGradient(rhoFunc)
gradrhoSym = gradrhodEq.newSymmetryBoundary('')
gradrhodEq.addBoundaryCondition('inlet',gradrhoSym)
gradrhodEq.addBoundaryCondition('outlet',gradrhoSym)
gradrhodEq.addBoundaryCondition('wall',gradrhoSym)
gradrhodEq.addBoundaryCondition('symm',gradrhoSym)
#creation of residual
gradrhodResidu = dgResidual(gradrhodEq,groups)
gradrhodResidu.compute(0,gradrhodDofTemp,gradrhodDof)
gradrhodDof.multiplyByInvMassMatrix() 
#le rendre continu
cgproj=dgL2ProjectionCG(gradrhodDof)
cg = gradrhodDof.getFunction()
cgproj.project(gradrhoDof,cg)
#calcul de la pente
grad = gradrhoDof.getFunction()
  
sl = functionPython (2,slopefunction,[grad,N2])

#--calcul de la fonction de courant 'eddy transport' par Ferrari 2010
#--------------------------------------------------------------------
cc = functionPython (1,cste,[N2])
indR = functionPython (3,termR,[grad])

psiEq= dgEddyTransportFlux(cc,indR,N2)
psiEqZeroBnd = psiEq.new0FluxBoundary()
psiEq.addBoundaryCondition('inlet',psiEqZeroBnd)
psiEq.addBoundaryCondition('outlet',psiEqZeroBnd)
psiEq.addBoundaryCondition('wall',psiEqZeroBnd)
psiEq.addBoundaryCondition('symm',psiEqZeroBnd)
psiEqBSBnd = functionConstant([0,0,0])
psiEq.addStrongBoundaryCondition(2,"inlet",psiEqBSBnd)
psiEq.addStrongBoundaryCondition(2,"outlet",psiEqBSBnd)
psiEq.addStrongBoundaryCondition(2,"wall",psiEqBSBnd)
psiEq.addStrongBoundaryCondition(2,"symm",psiEqBSBnd)


fieldspsi = psiEq.getNbFields()
psiDof = dgDofContainer(groups,fieldspsi)

residualP = dgDofContainer(groups,fieldspsi)
sysP =  linearSystemPETScDouble()
sysP.setParameter("petscOption", "-pc_type lu")
dofP = dgDofManager.newCG (groups,3,sysP)

#dgSolver.setVerb(10)
sol = dgSteady(psiEq,dofP)

#le rendre continu
psicDof = dgDofContainer (groups, 3)
cgprojP=dgL2ProjectionCG(psiDof)
cgP = psiDof.getFunction()

#--Creation de la vitesse maintenant
#----------------------------------
uGMDof = dgDofContainer(groups,3)
gradPDof = dgDofContainer(groups,9)
psigrad = psicDof.getFunctionGradient()
gradPDof.interpolate(psigrad)
gradslope = gradPDof.getFunction()
h = functionPython (3,velocityGM,[gradslope])

streamu=functionExtractCompNew(uGMDof.getFunction(),0)
streamv=functionExtractCompNew(uGMDof.getFunction(),1)
streamw=functionExtractCompNew(uGMDof.getFunction(),2)
streamuvw=functionCatCompNew([streamu,streamv,streamw])
streamuv = functionCatCompNew([functionExtractCompNew(uGMDof.getFunction(),0),functionExtractCompNew(uGMDof.getFunction(),1)])

law = dgConservationLawSW3dTracer(streamuv,streamw)
constfunc1 = functionConstant([4000])
law.setBath(constfunc1)
law.setKappaV(di)
diff = functionConstant([100])
law.setIsoDiff(sl,diff)
# Boundary condition
v_bc = functionConstant(0)
law.addBoundaryCondition('inlet',law.new0FluxBoundary())
law.addBoundaryCondition('outlet',law.new0FluxBoundary())
law.addBoundaryCondition('wall',law.new0FluxBoundary())
law.addBoundaryCondition('symm',law.new0FluxBoundary())
nbFields = law.getNbFields()

#implicit
#-------
residual = dgDofContainer(groups, law.getNbFields())
sys = linearSystemPETScBlockDouble () #Block
sys.setParameter("petscOption", "-pc_type lu -ksp_monitor")
dof = dgDofManager.newDGBlock(groups, 1, sys) #Block
implicitRK2 = dgDIRK(law, dof,1)
implicitRK2.getNewton().setRtol(1e-6)
implicitRK2.getNewton().setAtol(1e-11)
implicitRK2.getNewton().setVerb(10)

export_interval = 86400
next_export_time = export_interval
dt = 86400
#Frequence de Brunt-Vaisala
zFunc = functionExtractCompNew(XYZ,2)
vmod = dgSW3dVerticalModel(columnInfo)
vmod.setComputeNNWithEOS(True)
vmod.setZFunction(zFunc)
vmod.setTContainer(TDof)
vmod.setSContainer(SDof)
t = 0
#conservation globale
matrixS=fullMatrixDouble(1,1)
matrixT=fullMatrixDouble(1,1)
inteS=dgFunctionIntegrator(groups,S,SDof)
inteT=dgFunctionIntegrator(groups,T,TDof)
inteS.compute(matrixS,"")
inteT.compute(matrixT,"")
S0 = matrixS.get(0,0)
T0 = matrixT.get(0,0)
print '%.16e %.16e' %(matrixS.get(0,0), matrixT.get(0,0))
for j in range(1,10):
  #Frequence de Brunt-Vaisala
  vmod.computeHeights()
  vmod.computeNN()
  vmod.exportNNData(nnDof)
  nnDof.minmax(nn,M)
  print 'jour', j, 'N2 = ', nn(0,0)

  #calcul de la diffusivité verticale supplémentaire
  nuV.interpolate(Kc)
  #nuV.exportMsh('allin/Kc_%2d'%(j),t,j)
  
  #Combinaison des diffusivités
  nuCDof.interpolate(di)
  #nuCDof.exportMsh('allin/nuT_%2d'%(j),t,j)
  
  #vitesse de G-M
  sol.solve(psiDof)
  cgprojP.project(psicDof,cgP)
  gradPDof.interpolate(psigrad)
  uGMDof.interpolate(h)
  
  #calcul du gradient de la densite
  t += dt
  implicitRK2.iterate(SDof, dt, t)
  implicitRK2.iterate(TDof, dt, t)
  rhoDof.interpolate(rhoFunc)
  
  gradrhodResidu.compute(groups,0,gradrhodDofTemp,gradrhodDof)
  gradrhodDof.multiplyByInvMassMatrix() 
  cgproj.project(gradrhoDof,cg)
 
  if ( t >= next_export_time):
     next_export_time = next_export_time + export_interval
     uGMDof.exportFunctionMsh(streamuvw,'Output/U_%2d'%(j), t, j,'U')
     TDof.exportMsh('Output/Tm_%2d'%(j),t,j)
     SDof.exportMsh('Output/Sm_%2d'%(j),t,j)
     rhoDof.exportMsh('Output/R_%2d'%(j),t,j)
     #conservation
     inteS.compute(matrixS,"")
     inteT.compute(matrixT,"")
     print '%.16e %.16e' %((S0-matrixS.get(0,0))/S0,(T0-matrixT.get(0,0))/T0)