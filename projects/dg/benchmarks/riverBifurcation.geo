D=1000;
L=20*D;
lc=D/5;
nb=11*10;

pi = 3.14;
C45 = Cos(pi/10);
S45 = Sin(pi/10);

Point(1) = {-L,0,0,lc};
Point(2) = {0,0, 0,lc};
Point(3) = {L*C45, L*S45, 0,lc};
Point(4) = {L*C45, -L*S45, 0,lc};

Line(11) = {1,2};
Line(12) = {2,3};//warning orient correct
Line(13) = {2,4};//warning orient correct
Transfinite Line {11,12,13} = nb;

Physical Line("river1")={11};
Physical Line("river2")={12};
Physical Line("river3")={13};
Physical Point("Downstream1")={1};
Physical Point("Upstream1")={3};
Physical Point("Upstream2")={4};
