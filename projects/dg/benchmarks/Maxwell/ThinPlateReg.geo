lc = 0.000025*2;

// Dimensions de la plaque (Lx,Ly,Lz) et des extensions
Lx_left=0.000125*2;
Lx=0.006;
Lx_right=0.000125*2;
Ly=0.000125;
Lz=0.000125;

// Rayon des inclusions
r = 0.000063;

// Nombre d'inclusions
nbInc = 24;


//------------------------------------------------------------------------------


bndSurf_Num=0;   // bndSurf_List[];
vertSurf_Num=0;  // vertSurf_List[];

sphLineY_Num=0;  // sphLineY_List[]
sphLineZ_Num=0;  // sphLineZ_List[]  
sphSurfC_Num=0;  // sphSurfC_List[];
sphSurfY_Num=0;  // sphSurfY_List[];
sphSurfZ_Num=0;  // sphSurfZ_List[];
sphVol_Num=0;    // sphVol_List[];

Function extrudeBox
  out=Extrude {Lextrude,0,0} {Line{dr[]}; Surface{surf};};
  dr[0]=out[0];
  dr[1]=out[4];
  dr[2]=out[8];
  dr[3]=out[12];
  surf=out[16];
  bndSurf_List[bndSurf_Num]=out[1];  bndSurf_Num++;
  bndSurf_List[bndSurf_Num]=out[5];  bndSurf_Num++;
  bndSurf_List[bndSurf_Num]=out[9];  bndSurf_Num++;
  bndSurf_List[bndSurf_Num]=out[13]; bndSurf_Num++;
  vertSurf_List[vertSurf_Num]=out[16];  vertSurf_Num++;
Return

Function quartSphere
  center=newp; Point(center)={c[0],c[1],c[2],lc};
  p1=newp; Point(p1)={c[0]-r,c[1],c[2],lc};
  p2=newp; Point(p2)={c[0],c[1],c[2]+r,lc};
  p3=newp; Point(p3)={c[0]+r,c[1],c[2],lc};
  p4=newp; Point(p4)={c[0],c[1]+r,c[2],lc};
  
  arc1=newl; Circle(arc1)={p1,center,p2};
  arc2=newl; Circle(arc2)={p2,center,p3};
  arc3=newl; Circle(arc3)={p3,center,p4};
  arc4=newl; Circle(arc4)={p4,center,p1};
  line=newl; Line(line)={p1,p3};
  
  arc_ll=newll; Line Loop(arc_ll)={arc1,arc2,arc3,arc4};
  plane1_ll=newll; Line Loop(plane1_ll)={arc1,arc2,-line};
  plane2_ll=newll; Line Loop(plane2_ll)={arc3,arc4,line};
  surf1=news; Ruled Surface(surf1)={arc_ll};
  surf2=news; Ruled Surface(surf2)={plane1_ll};
  surf3=news; Ruled Surface(surf3)={plane2_ll};
  
  surf_sl=newsl; Surface Loop(surf_sl)={surf1,surf2,surf3};
  vol=newv; Volume(vol)={surf_sl};
  
  linePrev=newl; Line(linePrev)={pPrev,p1}; pPrev=p3;
  sphLineY_List[sphLineY_Num]=linePrev; sphLineY_Num++;
  sphLineY_List[sphLineY_Num]=arc1;     sphLineY_Num++;
  sphLineY_List[sphLineY_Num]=arc2;     sphLineY_Num++;
  
  sphLineZ_List[sphLineZ_Num]=linePrev; sphLineZ_Num++;
  sphLineZ_List[sphLineZ_Num]=-arc4;    sphLineZ_Num++;
  sphLineZ_List[sphLineZ_Num]=-arc3;    sphLineZ_Num++;
  
  sphSurfC_List[sphSurfC_Num]=surf1;    sphSurfC_Num++;
  sphSurfY_List[sphSurfY_Num]=surf2;    sphSurfY_Num++;
  sphSurfZ_List[sphSurfZ_Num]=surf3;    sphSurfZ_Num++;
  sphVol_List[sphVol_Num]=vol; sphVol_Num++;
Return
 

//------------------------------------------------------------------------------


// ---------------------
// Construction du cadre
// ---------------------

pL[0]=newp; Point(pL[0])={0, 0, 0,lc};
pL[1]=newp; Point(pL[1])={0, 0,Lz,lc};
pL[2]=newp; Point(pL[2])={0,Ly,Lz,lc};
pL[3]=newp; Point(pL[3])={0,Ly, 0,lc};
drL[0]=newl; Line(drL[0])={pL[0],pL[1]};
drL[1]=newl; Line(drL[1])={pL[1],pL[2]};
drL[2]=newl; Line(drL[2])={pL[2],pL[3]};
drL[3]=newl; Line(drL[3])={pL[3],pL[0]};
drL_ll=newll; Line Loop(drL_ll)={drL[]};
surfL=news; Plane Surface(surfL)={drL_ll};

pR[0]=newp; Point(pR[0])={Lx, 0, 0,lc};
pR[1]=newp; Point(pR[1])={Lx, 0,Lz,lc};
pR[2]=newp; Point(pR[2])={Lx,Ly,Lz,lc};
pR[3]=newp; Point(pR[3])={Lx,Ly, 0,lc};
drR[0]=newl; Line(drR[0])={pR[0],pR[1]};
drR[1]=newl; Line(drR[1])={pR[1],pR[2]};
drR[2]=newl; Line(drR[2])={pR[2],pR[3]};
drR[3]=newl; Line(drR[3])={pR[3],pR[0]};
drR_ll=newll; Line Loop(drR_ll)={drR[]};
surfR=news; Plane Surface(surfR)={drR_ll};

drLR[1]=newl; Line(drLR[1])={pL[1],pR[1]};
drLR[2]=newl; Line(drLR[2])={pL[2],pR[2]};
drLR[3]=newl; Line(drLR[3])={pL[3],pR[3]};


// ---------------------------
// Construction des inclusions
// ---------------------------

pPrev = pL[0];
For i In {1:nbInc}
  cx = 0.006/(24*2) * (2*i-1);
  c = {cx,0,0}; // Centre de l'inclusion
  Call quartSphere;
EndFor
pLast = pR[0];

lastline=newl; Line(lastline)={pPrev,pLast};
sphLineY_List[sphLineY_Num]=lastline; sphLineY_Num++;
sphLineZ_List[sphLineZ_Num]=lastline; sphLineZ_Num++;


// -----------------------------
// Construction du cadre (suite)
// -----------------------------

surf0_ll=newll; Line Loop(surf0_ll)={drL[0],-sphLineY_List[],-drR[0],drLR[1]};
surf1_ll=newll; Line Loop(surf1_ll)={drL[1],-drLR[1],-drR[1],drLR[2]};
surf2_ll=newll; Line Loop(surf2_ll)={drL[2],-drLR[2],-drR[2],drLR[3]};
surf3_ll=newll; Line Loop(surf3_ll)={drL[3],-drLR[3],-drR[3],sphLineZ_List[]};

surf0=news; Plane Surface(surf0)={surf0_ll};
surf1=news; Plane Surface(surf1)={surf1_ll};
surf2=news; Plane Surface(surf2)={surf2_ll};
surf3=news; Plane Surface(surf3)={surf3_ll};

surfLatMiddY = {surf0,surf2,sphSurfY_List[]};
surfLatMiddZ = {surf1,surf3,sphSurfZ_List[]};

surfRegMidd_sl=newsl; Surface Loop(surfRegMidd_sl)={surf0,surf1,surf2,surf3,sphSurfC_List[],surfL,-surfR};
volRegMidd=newv; Volume(volRegMidd)={surfRegMidd_sl};


// -----------------------------
// Construction des extensions
// -----------------------------

pLL[0]=newp; Point(pLL[0])={-Lx_left, 0, 0,lc};
pLL[1]=newp; Point(pLL[1])={-Lx_left, 0,Lz,lc};
pLL[2]=newp; Point(pLL[2])={-Lx_left,Ly,Lz,lc};
pLL[3]=newp; Point(pLL[3])={-Lx_left,Ly, 0,lc};
drLL[0]=newl; Line(drLL[0])={pLL[0],pLL[1]};
drLL[1]=newl; Line(drLL[1])={pLL[1],pLL[2]};
drLL[2]=newl; Line(drLL[2])={pLL[2],pLL[3]};
drLL[3]=newl; Line(drLL[3])={pLL[3],pLL[0]};
drLL_ll=newll; Line Loop(drLL_ll)={drLL[]};
surfLL=news; Plane Surface(surfLL)={drLL_ll};

pRR[0]=newp; Point(pRR[0])={Lx+Lx_right, 0, 0,lc};
pRR[1]=newp; Point(pRR[1])={Lx+Lx_right, 0,Lz,lc};
pRR[2]=newp; Point(pRR[2])={Lx+Lx_right,Ly,Lz,lc};
pRR[3]=newp; Point(pRR[3])={Lx+Lx_right,Ly, 0,lc};
drRR[0]=newl; Line(drRR[0])={pRR[0],pRR[1]};
drRR[1]=newl; Line(drRR[1])={pRR[1],pRR[2]};
drRR[2]=newl; Line(drRR[2])={pRR[2],pRR[3]};
drRR[3]=newl; Line(drRR[3])={pRR[3],pRR[0]};
drRR_ll=newll; Line Loop(drRR_ll)={drRR[]};
surfRR=news; Plane Surface(surfRR)={drRR_ll};

drLL_L[0]=newl; Line(drLL_L[0])={pLL[0],pL[0]};
drLL_L[1]=newl; Line(drLL_L[1])={pLL[1],pL[1]};
drLL_L[2]=newl; Line(drLL_L[2])={pLL[2],pL[2]};
drLL_L[3]=newl; Line(drLL_L[3])={pLL[3],pL[3]};
drR_RR[0]=newl; Line(drR_RR[0])={pR[0],pRR[0]};
drR_RR[1]=newl; Line(drR_RR[1])={pR[1],pRR[1]};
drR_RR[2]=newl; Line(drR_RR[2])={pR[2],pRR[2]};
drR_RR[3]=newl; Line(drR_RR[3])={pR[3],pRR[3]};

surf0_ll=newll; Line Loop(surf0_ll)={drLL[0],-drLL_L[0],-drL[0],drLL_L[1]};
surf1_ll=newll; Line Loop(surf1_ll)={drLL[1],-drLL_L[1],-drL[1],drLL_L[2]};
surf2_ll=newll; Line Loop(surf2_ll)={drLL[2],-drLL_L[2],-drL[2],drLL_L[3]};
surf3_ll=newll; Line Loop(surf3_ll)={drLL[3],-drLL_L[3],-drL[3],drLL_L[0]};
surf0=news; Plane Surface(surf0)={surf0_ll};
surf1=news; Plane Surface(surf1)={surf1_ll};
surf2=news; Plane Surface(surf2)={surf2_ll};
surf3=news; Plane Surface(surf3)={surf3_ll};
surfLatLeftY = {surf0,surf2};
surfLatLeftZ = {surf1,surf3};

surf0_ll=newll; Line Loop(surf0_ll)={drR[0],-drR_RR[0],-drRR[0],drR_RR[1]};
surf1_ll=newll; Line Loop(surf1_ll)={drR[1],-drR_RR[1],-drRR[1],drR_RR[2]};
surf2_ll=newll; Line Loop(surf2_ll)={drR[2],-drR_RR[2],-drRR[2],drR_RR[3]};
surf3_ll=newll; Line Loop(surf3_ll)={drR[3],-drR_RR[3],-drRR[3],drR_RR[0]};
surf0=news; Plane Surface(surf0)={surf0_ll};
surf1=news; Plane Surface(surf1)={surf1_ll};
surf2=news; Plane Surface(surf2)={surf2_ll};
surf3=news; Plane Surface(surf3)={surf3_ll};
surfLatRightY = {surf0,surf2};
surfLatRightZ = {surf1,surf3};

surfRegLeft_ls=newsl;  Surface Loop(surfRegLeft_ls)={surfL,-surfLL,surfLatLeftY[],surfLatLeftZ[]};
surfRegRight_ls=newsl; Surface Loop(surfRegRight_ls)={-surfR,surfRR,surfLatRightY[],surfLatRightZ[]};

volRegLeft=newv;  Volume(volRegLeft)={surfRegLeft_ls};
volRegRight=newv; Volume(volRegRight)={surfRegRight_ls};


// ----------------------
// D�finition des r�gions
// ----------------------

Physical Surface("Bnd_Left")  = {surfLL};
Physical Surface("Bnd_Right") = {surfRR};
Physical Surface("Bnd_LatY")  = {surfLatLeftY[],surfLatMiddY[],surfLatRightY[]};
Physical Surface("Bnd_LatZ")  = {surfLatLeftZ[],surfLatMiddZ[],surfLatRightZ[]};

Physical Surface("Interf_Left")  = {surfL};
Physical Surface("Interf_Right") = {surfR};
Physical Surface("Interf_Inc")   = {sphSurfC_List[]};

Physical Volume("Vol_Left")  = {volRegLeft};
Physical Volume("Vol_Midd")  = {volRegMidd};
Physical Volume("Vol_Right") = {volRegRight};
Physical Volume("Vol_Inc")   = {sphVol_List[]};




