
lbase = 1.;
ldom = 2.;
lc = 0.3;


Point(1) = {-lbase/2., -lbase/2., -ldom/2., lc};
Point(2) = { lbase/2., -lbase/2., -ldom/2., lc};
Point(3) = { lbase/2.,  lbase/2., -ldom/2., lc};
Point(4) = {-lbase/2.,  lbase/2., -ldom/2., lc};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};

Line Loop(10) = {1,2,3,4};
Plane Surface(20) = {10};

Extrude {0,0,ldom} {
  Surface{20};
}

Physical Surface("BorderX") = {33,41};
Physical Surface("BorderY") = {29,37};
Physical Surface("BorderZLeft") = {20};
Physical Surface("BorderZRight") = {42};
Physical Volume("Domain") = {1};

Mesh.CharacteristicLengthExtendFromBoundary = 1;
Mesh.CharacteristicLengthFromPoints = 1;

