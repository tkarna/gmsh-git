from gmshpy import *
from dgpy import *
import os
import time
import math


print'---- Load Mesh & Build Groups'

model = GModel()
model.load('CuboidPmlDouble.msh')

dimension = 3
order = 1
groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()


print'---- Load Conservation law with parameters'

mu      = functionConstant([1,1,1])
epsilon = functionConstant([1,1,1])

lawE = dgConservationLawMaxwellExplicit('E','PmlStraight')
lawH = dgConservationLawMaxwellExplicit('H','PmlStraight')
lawE.setMu(mu)
lawH.setMu(mu)
lawE.setEpsilon(epsilon)
lawH.setEpsilon(epsilon)


print'---- Load Initial conditions'

initSolE = functionConstant([0,0,0, 0,0])
solE = dgDofContainer(groups, lawE.getNbFields())
solE.L2Projection(initSolE)
solE.setFieldName(0,'ePml_x')
solE.setFieldName(1,'ePml_y')
solE.setFieldName(2,'ePml_z')
solE.setFieldName(3,'ePml_t1')
solE.setFieldName(4,'ePml_t2')
solE.exportMsh("output/CuboidPmlDoubleE_00000", 0, 0)

initSolH = functionConstant([0,0,0, 0,0])
solH = dgDofContainer(groups, lawH.getNbFields())
solH.L2Projection(initSolH)
solH.setFieldName(0,'hPml_x')
solH.setFieldName(1,'hPml_y')
solH.setFieldName(2,'hPml_z')
solH.setFieldName(3,'hPml_t1')
solH.setFieldName(4,'hPml_t2')
solH.exportMsh("output/CuboidPmlDoubleH_00000", 0, 0)

lawE.setH(solH.getFunction())
lawH.setE(solE.getFunction())


print'---- Build Pml'

CCode = """
#include "fullMatrix.h"
#include "function.h"
#include "math.h"

extern "C" {

double muValue = 1.;
double epsilonValue = 1.;
double cValue = 1./pow(muValue*epsilonValue,0.5);

double ldom=2.;
double lpml=1.;

void pmlCoefDef (dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (size_t i = 0; i<sol.size1(); i++) {
    double rloc = fabs(xyz(i,0)) - ldom/2.;
    double sigmaPml = 0.;
    if (rloc>0)
      sigmaPml = cValue/(lpml*1.01) * (rloc)/(lpml*1.01-rloc);
    sol.set(i,0,sigmaPml);
  }
}

}
"""
tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0) :
  functionC.buildLibrary (CCode, tmpLib);
Msg.Barrier()

pmlCoef = functionC(tmpLib,"pmlCoefDef",1,[xyz])
pmlDir  = functionConstantByTag({"Vol_Left"  : [-1.,0.,0., 0.,-1.,0., 0.,0.,-1.],
                                 "Vol_Midd"  : [ 0.,0.,0., 0., 0.,0., 0.,0., 0.],
                                 "Vol_Right" : [ 1.,0.,0., 0., 1.,0., 0.,0., 1.]})

lawE.setPmlCoef(pmlCoef)
lawH.setPmlCoef(pmlCoef)

lawE.setPmlDir(pmlDir)
lawH.setPmlDir(pmlDir)

lawE.addPml('Vol_Left')
lawH.addPml('Vol_Left')

lawE.addPml('Vol_Right')
lawH.addPml('Vol_Right')


print'---- Load Boundary conditions'

t=0
freq=0.5
def extFieldFunction(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    value = math.cos(2*math.pi*freq*t)
    val.set(i,0,0.)
    val.set(i,1,value)
    val.set(i,2,0.)
    val.set(i,3,0.)
    val.set(i,4,0.)
    val.set(i,5,value)
extField = functionPython(6, extFieldFunction, [xyz])

lawE.addInterfacePmlWithIncident('Interf_Left',extField)
lawH.addInterfacePmlWithIncident('Interf_Left',extField)

lawE.addInterfacePml('Interf_Right')
lawH.addInterfacePml('Interf_Right')

lawE.addBoundaryCondition('Bnd_LatY',  lawE.newBoundaryElectricWall('Bnd_LatY'))
lawE.addBoundaryCondition('Bnd_LatZ',  lawE.newBoundaryMagneticWall('Bnd_LatZ'))
lawE.addBoundaryCondition('Bnd_Left',  lawE.newBoundaryElectricWall('Bnd_Left'))
lawE.addBoundaryCondition('Bnd_Right', lawE.newBoundaryElectricWall('Bnd_Right'))

lawH.addBoundaryCondition('Bnd_LatY',  lawH.newBoundaryElectricWall('Bnd_LatY'))
lawH.addBoundaryCondition('Bnd_LatZ',  lawH.newBoundaryMagneticWall('Bnd_LatZ'))
lawH.addBoundaryCondition('Bnd_Left',  lawH.newBoundaryElectricWall('Bnd_Left'))
lawH.addBoundaryCondition('Bnd_Right', lawH.newBoundaryElectricWall('Bnd_Right'))


print'---- LOOP'

solverE = dgERK(lawE, None, DG_ERK_EULER)
solverH = dgERK(lawH, None, DG_ERK_EULER)

dt = 0.0001
numoutput = 1
for i in range(1,10000+1):
  t = dt*i
  x = time.clock()
  Msg.Barrier()
  solverH.iterate(solH, dt, t-dt/2)
  Msg.Barrier()
  solverE.iterate(solE, dt, t)
  Msg.Barrier()
  normH = solH.norm()
  normE = solE.norm()
  if (i % 250 == 0):
    print'|ITER1|',i,'/ 10000 |NormE|',normE, '|NormH|',normH, '|DT|',dt, '|T|',t, '|CPU|',time.clock()-x
    solH.exportMsh("output/CuboidPmlDoubleH_%05i" % numoutput, t, numoutput)
    solE.exportMsh("output/CuboidPmlDoubleE_%05i" % numoutput, t, numoutput)
    numoutput = numoutput+1

Msg.Exit(0);

