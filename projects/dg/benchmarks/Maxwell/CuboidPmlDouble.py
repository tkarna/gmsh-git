from gmshpy import *
from dgpy import *
import os
import time
import math


print'---- Load Mesh & Build Groups'

model = GModel()
model.load('CuboidPmlDouble.msh')

dimension = 3
order = 1
groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()


print'---- Load Conservation law with parameters'

mu      = functionConstant([1,1,1])
epsilon = functionConstant([1,1,1])

law = dgConservationLawMaxwell('PmlStraight')
law.setMu(mu)
law.setEpsilon(epsilon)


print'---- Load Initial conditions'

initialSolution = functionConstant([0,0,0, 0,0,0, 0,0,0,0])
solution = dgDofContainer(groups, law.getNbFields())
solution.L2Projection(initialSolution)
solution.setFieldName(0,'ePml_x')
solution.setFieldName(1,'ePml_y')
solution.setFieldName(2,'ePml_z')
solution.setFieldName(3,'hPml_x')
solution.setFieldName(4,'hPml_y')
solution.setFieldName(5,'hPml_z')
solution.setFieldName(6,'ePml_t1')
solution.setFieldName(7,'ePml_t2')
solution.setFieldName(8,'hPml_t1')
solution.setFieldName(9,'hPml_t2')
solution.exportMsh("output/CuboidPmlDouble_00000", 0, 0)


print'---- Build Pml'

CCode = """
#include "fullMatrix.h"
#include "function.h"
#include "math.h"

extern "C" {

double muValue = 1.;
double epsilonValue = 1.;
double cValue = 1./pow(muValue*epsilonValue,0.5);

double ldom=2.;
double lpml=1.;

void pmlCoefDef (dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (size_t i = 0; i<sol.size1(); i++) {
    double rloc = fabs(xyz(i,0)) - ldom/2.;
    double sigmaPml = 0.;
    if (rloc>0)
      sigmaPml = cValue/(lpml*1.01) * (rloc)/(lpml*1.01-rloc);
    sol.set(i,0,sigmaPml);
  }
}

}
"""
tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0) :
  functionC.buildLibrary (CCode, tmpLib);

pmlCoef = functionC(tmpLib,"pmlCoefDef",1,[xyz])
pmlDir  = functionConstantByTag({"Vol_Left"  : [-1.,0.,0., 0.,-1.,0., 0.,0.,-1.],
                                 "Vol_Midd"  : [ 0.,0.,0., 0., 0.,0., 0.,0., 0.],
                                 "Vol_Right" : [ 1.,0.,0., 0., 1.,0., 0.,0., 1.]})

law.setPmlCoef(pmlCoef)
law.setPmlDir(pmlDir)

law.addPml('Vol_Left')
law.addPml('Vol_Right')



print'---- Load Boundary conditions'

t=0
freq=0.5
def extFieldFunction(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    value = math.cos(2*math.pi*freq*t)
    val.set(i,0,0.)
    val.set(i,1,value)
    val.set(i,2,0.)
    val.set(i,3,0.)
    val.set(i,4,0.)
    val.set(i,5,value)
extField = functionPython(6, extFieldFunction, [xyz])

law.addInterfacePmlWithIncident('Interf_Left',extField)
law.addInterfacePml('Interf_Right')

law.addBoundaryCondition('Bnd_LatY',  law.newBoundaryElectricWall('Bnd_LatY'))
law.addBoundaryCondition('Bnd_LatZ',  law.newBoundaryMagneticWall('Bnd_LatZ'))
law.addBoundaryCondition('Bnd_Left',  law.newBoundaryElectricWall('Bnd_Left'))
law.addBoundaryCondition('Bnd_Right', law.newBoundaryElectricWall('Bnd_Right'))


print'---- LOOP'

solver = dgERK(law, None, DG_ERK_44)

#sys = linearSystemPETScBlockDouble()
#sys.setParameter("petscOptions", "-pc_type ilu")
#dof = dgDofManager.newDGBlock(groups, law.getNbFields(), sys)
#solver = dgDIRK(law, dof, 2)

dt = 0.001
numoutput = 1
for i in range(1,5000+1):
  t = dt*i
  x = time.clock()
  solver.iterate(solution, dt, t)
  norm = solution.norm()
  if (i % 100 == 0):
    print'|ITER1|',i,'/ 5000 |NORM|',norm, '|DT|',dt, '|T|',t, '|CPU|',time.clock()-x
    solution.exportMsh("output/CuboidPmlDouble_%05i" % numoutput, t, numoutput)
    numoutput = numoutput+1

Msg.Exit(0);

