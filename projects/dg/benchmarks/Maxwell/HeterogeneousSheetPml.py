from dgpy import *
import os
import time
import math


### Parameters

# Physical

nbInc = 15

lBox   = 2e-4
lSheet = lBox*nbInc
lExt   = lSheet/2.
lPml   = lBox/2.

c = 299792458
lc = 2.0e-5
CFL = 0.05
dt = lc/c * CFL
dtPhys = dt

tf = 5e-10

iMax = int(tf/dt)
iExport = iMax/100
iData = 500
iVizu = 1

# Normalized

LREF = 2e-4*15
ZREF = 1
CREF = 299792458
TREF = LREF/CREF

lBox   /= LREF
lSheet /= LREF
lExt   /= LREF
lPml   /= LREF
c      /= CREF
lc     /= LREF
dt     /= TREF
tf     /= TREF


### Load Mesh & Build Groups

model = GModel()
model.load('HeterogeneousSheet.msh')

dimension = 3
order = 1
groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()


### Load Conservation law

law = dgConservationLawMaxwell('PmlStraight')

epsRelWall = 2
sigmaRelInc = 10e3*LREF*ZREF

epsilon = functionConstantByTag({"Vol_Midd"      : [epsRelWall,epsRelWall,epsRelWall],
                                 "Vol_Inc"       : [1.,1.,1.],
                                 "Vol_Left"      : [1.,1.,1.],
                                 "Vol_Right"     : [1.,1.,1.],
                                 "Vol_Pml_Left"  : [1.,1.,1.],
                                 "Vol_Pml_Right" : [1.,1.,1.]})
mu = functionConstant([1.,1.,1.])
sigma   = functionConstantByTag({"Vol_Midd"      : [0.,0.,0.],
                                 "Vol_Inc"       : [sigmaRelInc,sigmaRelInc,sigmaRelInc],
                                 "Vol_Left"      : [0.,0.,0.],
                                 "Vol_Right"     : [0.,0.,0.],
                                 "Vol_Pml_Left"  : [0.,0.,0.],
                                 "Vol_Pml_Right" : [0.,0.,0.]})

law.setEpsilon(epsilon)
law.setMu(mu)
law.setSigma(sigma)

CCode = """
#include "fullMatrix.h"
#include "function.h"
#include "math.h"
extern "C" {
void pmlCoefDef(dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (size_t i = 0; i<sol.size1(); i++) {
    double cValue = 1.;
    double nbInc = 1.;
    double lSheet = 1.;
    double lBox = lSheet/nbInc;
    double lExt = lSheet/2.;
    double lPml = lBox/2.;
    double xloc = fabs(xyz(i,0) - lSheet/2.) - lSheet/2. - lExt;
    double sigmaPml = 0.;
    if (xloc>0.)
      sigmaPml = cValue/lPml * xloc/(lPml-xloc);
    sol.set(i,0,sigmaPml);
  }
}
void incFieldsDef(dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &xyz, fullMatrix<double> &t) {
  for (size_t i=0; i<val.size1(); i++) {
    double lSheet = 1.;
    double posPulse = -20*lSheet;
    double rPulse   =   2*lSheet;
    double value = exp(-pow(((xyz(i,0) - posPulse - t(i,0))/rPulse),2));
    val.set(i,0,0.); // e
    val.set(i,1,0.);
    val.set(i,2,value);
    val.set(i,3,0.); // h
    val.set(i,4,-value);
    val.set(i,5,0.);
  }
}
void initFieldsDef(dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &xyz, fullMatrix<double> &t) {
  for (size_t i=0; i<val.size1(); i++) {
    double lSheet = 1.;
    double posPulse = -20*lSheet;
    double rPulse   =   2*lSheet;
    double value = 0.;
    value = exp(-pow(((xyz(i,0) - posPulse - t(i,0))/rPulse),2));
    val.set(i,0,0.); // e
    val.set(i,1,0.);
    val.set(i,2,value);
    val.set(i,3,0.); // h
    val.set(i,4,-value);
    val.set(i,5,0.);
    val.set(i,6,0.);
    val.set(i,7,0.);
    val.set(i,8,0.);
    val.set(i,9,0.);
  }
}
}
"""
tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0):
  functionC.buildLibrary (CCode, tmpLib);
Msg.Barrier()

t = 0.
tFunc = functionConstant([t])
incFieldsFunc = functionC(tmpLib,"incFieldsDef",6,[xyz,tFunc])
initFieldsFunc = functionC(tmpLib,"initFieldsDef",10,[xyz,tFunc])
pmlCoef = functionC(tmpLib,"pmlCoefDef",1,[xyz])
pmlDir = functionConstantByTag({"Vol_Midd"      : [ 0.,0.,0., 0., 0.,0., 0.,0., 0.],
                                "Vol_Inc"       : [ 0.,0.,0., 0., 0.,0., 0.,0., 0.],
                                "Vol_Left"      : [ 0.,0.,0., 0., 0.,0., 0.,0., 0.],
                                "Vol_Right"     : [ 0.,0.,0., 0., 0.,0., 0.,0., 0.],
                                "Vol_Pml_Left"  : [-1.,0.,0., 0.,-1.,0., 0.,0.,-1.],
                                "Vol_Pml_Right" : [ 1.,0.,0., 0., 1.,0., 0.,0., 1.]})

law.setPmlCoef(pmlCoef)
law.setPmlDir(pmlDir)

#law.addPml('Vol_Pml_Left')
law.addPml('Vol_Pml_Right')

#law.addInterfacePmlWithIncident('Interf_Pml_Left',incFieldsFunc)
law.addInterfacePml('Interf_Pml_Right')


### Load Boundary conditions

law.addBoundaryCondition('Bnd_LatY',  law.newBoundaryMagneticWall('Bnd_LatY'))
law.addBoundaryCondition('Bnd_LatZ',  law.newBoundaryElectricWall('Bnd_LatZ'))
law.addBoundaryCondition('Bnd_Left',  law.newBoundaryDirichletOnE('Bnd_Left',incFieldsFunc))
law.addBoundaryCondition('Bnd_Right', law.newBoundaryElectricWall('Bnd_Right'))


### Build Initial conditions

sol = dgDofContainer(groups, law.getNbFields())
sol.L2Projection(initFieldsFunc)
sol.setFieldName(0,'e_x')
sol.setFieldName(1,'e_y')
sol.setFieldName(2,'e_z')
sol.setFieldName(3,'h_x')
sol.setFieldName(4,'h_y')
sol.setFieldName(5,'h_z')
sol.setFieldName(6,'e_pml1')
sol.setFieldName(7,'e_pml2')
sol.setFieldName(8,'h_pml1')
sol.setFieldName(9,'h_pml2')
sol.exportMsh("output/Fields_0000", 0, 0)

solInc = dgDofContainer(groups, 6)
solInc.setFieldName(0,'eInc_x')
solInc.setFieldName(1,'eInc_y')
solInc.setFieldName(2,'eInc_z')
solInc.setFieldName(3,'hInc_x')
solInc.setFieldName(4,'hInc_y')
solInc.setFieldName(5,'hInc_z')
solInc.L2Projection(incFieldsFunc)
solInc.exportMsh("output/FieldsInc_0000", 0, 0)

pmlCoefDof = dgDofContainer(groups, 1)
pmlCoefDof.setFieldName(0,'sigma')
pmlCoefDof.interpolate(pmlCoef)
pmlCoefDof.exportMsh("output/PmlCoef")

pmlDirDof = dgDofContainer(groups, 9)
pmlDirDof.L2Projection(pmlDir)
pmlDirDof.exportMsh("output/PmlDir")


### LOOP

#sys = linearSystemPETScBlockDouble()
#sys.setParameter("petscOptions", "-pc_type ilu")
#dof = dgDofManager.newDGBlock(groups, law.getNbFields(), sys)
solver = dgERK(law, None, DG_ERK_44)

solEval = dgFunctionEvaluator(groups, sol.getFunction())
solValueA = fullMatrixDouble(1,1)
solValueB = fullMatrixDouble(1,1)
solValueC = fullMatrixDouble(1,1)
solValueD = fullMatrixDouble(1,1)
solValueE = fullMatrixDouble(1,1)
solValueF = fullMatrixDouble(1,1)
solValueG = fullMatrixDouble(1,1)
solValueH = fullMatrixDouble(1,1)
solValueI = fullMatrixDouble(1,1)
solValueJ = fullMatrixDouble(1,1)
solValueK = fullMatrixDouble(1,1)
solValueL = fullMatrixDouble(1,1)
solValueM = fullMatrixDouble(1,1)

numOutput = 1
timeInit = time.clock()
for i in range (1,iMax+1) :
  Msg.Barrier()
  t = dt*i
  tFunc.set(t)
  solver.iterate(sol, dt, t)
  
  if (i % 10 == 0):
    norm = sol.norm()
    if (Msg.GetCommRank() == 0):
      timeCurrent = time.clock()
      timeMeanByIter = (timeCurrent-timeInit)/(1.*i)/(60.)
      timeExp = timeMeanByIter*(iMax-i)
      print '|ITER|',i, '/',iMax, '|NORM|',norm, '|TimeExp|',timeExp
  
  solEval.compute(-lSheet*0.45, lBox*0.05, lBox*0.05, solValueA)
  solEval.compute(-lSheet*0.45, lBox*0.05, lBox*0.25, solValueB)
  solEval.compute(-lSheet*0.45, lBox*0.25, lBox*0.25, solValueC)
  solEval.compute(-lSheet*0.25, lBox*0.05, lBox*0.05, solValueD)
  solEval.compute(-lSheet*0.25, lBox*0.05, lBox*0.25, solValueE)
  solEval.compute(-lSheet*0.25, lBox*0.25, lBox*0.25, solValueF)
  solEval.compute( lSheet*1.25, lBox*0.05, lBox*0.05, solValueG)
  solEval.compute( lSheet*1.25, lBox*0.05, lBox*0.25, solValueH)
  solEval.compute( lSheet*1.25, lBox*0.25, lBox*0.25, solValueI)
  solEval.compute( lSheet*1.45, lBox*0.05, lBox*0.05, solValueK)
  solEval.compute( lSheet*1.45, lBox*0.05, lBox*0.25, solValueL)
  solEval.compute( lSheet*1.45, lBox*0.25, lBox*0.25, solValueM)
  if (Msg.GetCommRank() == 0):
    fileOut = open("output/Data.out","a")
    fileOut.write("%.8e %.8e %.8e %.8e %.8e %.8e %.8e %.8e %.8e %.8e %.8e %.8e %.8e %.8e\n" % (t, solValueA(0,0), solValueB(0,0), solValueC(0,0), solValueD(0,0), solValueE(0,0), solValueF(0,0), solValueG(0,0), solValueH(0,0), solValueI(0,0), solValueJ(0,0), solValueK(0,0), solValueL(0,0), solValueM(0,0)))
    fileOut.close()
  
  if (i % iExport == 0):
    solInc.L2Projection(incFieldsFunc)
    solInc.exportMsh("output/FieldsInc_%04i" % numOutput, t, numOutput)
    sol.exportMsh("output/Fields_%04i" % numOutput, t, numOutput)
    numOutput = numOutput+1


Msg.Exit(0);

