from gmshpy import *
from dgpy import *
import os
import time
import math


### Parameters

CFL = 0.05

lc = 0.3
c = 1.
dt = lc/c * CFL

tf = 100

iMax = int(tf/dt)
numExport = int(iMax/100)


### Load Mesh & Build Groups

model = GModel()
model.load('CuboidAbc.msh')

dimension = 3
order = 1
groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()


### Load Conservation Law

law = dgConservationLawMaxwell()

mu      = functionConstant([1,1,1])
epsilon = functionConstant([1,1,1])

law.setMu(mu)
law.setEpsilon(epsilon)

CCode = """
#include "fullMatrix.h"
#include "function.h"
#include "math.h"
extern "C" {
void incFieldsDef (dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz, fullMatrix<double> &t) {
  for (size_t i = 0; i<sol.size1(); i++) {
    double zloc = xyz(i,2)+1.;
    double freq = 0.2;
    double value = sin(2*M_PI*freq*(t(0,0)-zloc));
    sol.set(i,0,value);
    sol.set(i,1,0.);
    sol.set(i,2,0.);
    sol.set(i,3,0);
    sol.set(i,4,value);
    sol.set(i,5,0.);
  }
}
}
"""
tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0):
  functionC.buildLibrary (CCode, tmpLib);
Msg.Barrier()

t = 0.
tFunc = functionConstant([t])
incFieldsFunc = functionC(tmpLib,"incFieldsDef",6,[xyz,tFunc])
nullFieldsFunc = functionConstant([0,0,0,0,0,0])

law.addBoundaryCondition('BorderX',      law.newBoundaryElectricWall('BorderX'))
law.addBoundaryCondition('BorderY',      law.newBoundaryMagneticWall('BorderY'))
law.addBoundaryCondition('BorderZLeft',  law.newBoundarySilverMullerDirichlet('BorderZLeft',incFieldsFunc))
law.addBoundaryCondition('BorderZRight', law.newBoundarySilverMullerDirichlet('BorderZRight',nullFieldsFunc))
#law.addBoundaryCondition('BorderZLeft',  law.newBoundaryDirichletOnE('BorderZLeft',incFieldsFunc))
#law.addBoundaryCondition('BorderZRight', law.newBoundaryDirichletOnE('BorderZRight',incFieldsFunc))


### Load Initial and Reference Solutions

sol = dgDofContainer(groups, law.getNbFields())
sol.L2Projection(incFieldsFunc)
sol.setFieldName(0,'eAbc_x')
sol.setFieldName(1,'eAbc_y')
sol.setFieldName(2,'eAbc_z')
sol.setFieldName(3,'hAbc_x')
sol.setFieldName(4,'hAbc_y')
sol.setFieldName(5,'hAbc_z')
sol.exportMshSingleComp(0,"output/CuboidAbc_Abc_00000", 0, 0)

solRef = dgDofContainer(groups, 6)
solRef.L2Projection(incFieldsFunc)
solRef.setFieldName(0,'eRef_x')
solRef.setFieldName(1,'eRef_y')
solRef.setFieldName(2,'eRef_z')
solRef.setFieldName(3,'hRef_x')
solRef.setFieldName(4,'hRef_y')
solRef.setFieldName(5,'hRef_z')
solRef.exportMshSingleComp(0,"output/CuboidAbc_Ref_00000", 0, 0)


### LOOP

#sys = linearSystemPETScBlockDouble()
#sys.setParameter("petscOptions", "-pc_type ilu")
#dof = dgDofManager.newDGBlock(groups, law.getNbFields(), sys)
#solver = dgDIRK(law, dof, DG_ERK_44)

solver = dgERK(law, None, DG_ERK_44)

numOutput = 1
timeInit = time.clock()
for i in range(1,iMax):
  Msg.Barrier()
  t = dt*i
  tFunc.set(t)
  solver.iterate(sol, dt, t)
  Msg.Barrier()
  if (i % numExport == 0):
    norm = sol.norm()
    if (Msg.GetCommRank() == 0):
      timeCurrent = time.clock()
      timeMeanByIter = (timeCurrent-timeInit)/(i*60.)
      timeExp = timeMeanByIter*(iMax-i)
      print '|ITER|',i, '/',iMax, '|dt|', dt, '|NORM|',norm, '|TimeExp|',timeExp
    sol.exportMshSingleComp(0,"output/CuboidAbc_Abc_%05i" % numOutput, t, numOutput)
    solRef.L2Projection(incFieldsFunc)
    solRef.exportMshSingleComp(0,"output/CuboidAbc_Ref_%05i" % numOutput, t, numOutput)
    numOutput = numOutput+1

Msg.Exit(0);

