//------------------------------------------------------------------------------


// Parameters

nbInc = 1;
tau = 0.2;

lBox   = 2e-4;
lSheet = lBox*nbInc;
lExt   = lSheet/2.;
lPml   = lBox/2.;

Lx = lSheet;
Ly = lBox/2.;
Lz = lBox/2.;

r = (tau/(Pi*4/3))^(1/3) * lBox;

lcInc = 0.4e-5;
lcAir = 1.6e-5;

// Normalization

LREF = lSheet;

lBox   /= LREF;
lSheet /= LREF;
lExt   /= LREF;
lPml   /= LREF;
Lx /= LREF;
Ly /= LREF;
Lz /= LREF;
r  /= LREF;
lcInc /= LREF;
lcAir /= LREF;


//------------------------------------------------------------------------------


bndSurf_Num=0;   // bndSurf_List[];
vertSurf_Num=0;  // vertSurf_List[];

sphLineY_Num=0;  // sphLineY_List[]
sphLineZ_Num=0;  // sphLineZ_List[]  
sphSurfC_Num=0;  // sphSurfC_List[];
sphSurfY_Num=0;  // sphSurfY_List[];
sphSurfZ_Num=0;  // sphSurfZ_List[];
sphVol_Num=0;    // sphVol_List[];

surfLatY_Num=0;  // surfLatY_List[]
surfLatZ_Num=0;  // surfLatZ_List[]
 

//------------------------------------------------------------------------------


// ---------------------
// Construction du cadre
// ---------------------

pL[0]=newp; Point(pL[0])={0, 0, 0};
pL[1]=newp; Point(pL[1])={0, 0,Lz};
pL[2]=newp; Point(pL[2])={0,Ly,Lz};
pL[3]=newp; Point(pL[3])={0,Ly, 0};
drL[0]=newl; Line(drL[0])={pL[0],pL[1]};
drL[1]=newl; Line(drL[1])={pL[1],pL[2]};
drL[2]=newl; Line(drL[2])={pL[2],pL[3]};
drL[3]=newl; Line(drL[3])={pL[3],pL[0]};
drL_ll=newll; Line Loop(drL_ll)={drL[]};
surfL=news; Plane Surface(surfL)={drL_ll};

pR[0]=newp; Point(pR[0])={Lx, 0, 0};
pR[1]=newp; Point(pR[1])={Lx, 0,Lz};
pR[2]=newp; Point(pR[2])={Lx,Ly,Lz};
pR[3]=newp; Point(pR[3])={Lx,Ly, 0};
drR[0]=newl; Line(drR[0])={pR[0],pR[1]};
drR[1]=newl; Line(drR[1])={pR[1],pR[2]};
drR[2]=newl; Line(drR[2])={pR[2],pR[3]};
drR[3]=newl; Line(drR[3])={pR[3],pR[0]};
drR_ll=newll; Line Loop(drR_ll)={drR[]};
surfR=news; Plane Surface(surfR)={drR_ll};

drLR[1]=newl; Line(drLR[1])={pL[1],pR[1]};
drLR[2]=newl; Line(drLR[2])={pL[2],pR[2]};
drLR[3]=newl; Line(drLR[3])={pL[3],pR[3]};


//------------------------------------------------------------------------------


// ---------------------------
// Construction des inclusions
// ---------------------------

Function quartSphere
  center=newp; Point(center)={c[0],c[1],c[2]};
  p1=newp; Point(p1)={c[0]-r,c[1],c[2]};
  p2=newp; Point(p2)={c[0],c[1],c[2]+r};
  p3=newp; Point(p3)={c[0]+r,c[1],c[2]};
  p4=newp; Point(p4)={c[0],c[1]+r,c[2]};
  
  arc1=newl; Circle(arc1)={p1,center,p2};
  arc2=newl; Circle(arc2)={p2,center,p3};
  arc3=newl; Circle(arc3)={p3,center,p4};
  arc4=newl; Circle(arc4)={p4,center,p1};
  line=newl; Line(line)={p1,p3};
  
  arc_ll=newll; Line Loop(arc_ll)={arc1,arc2,arc3,arc4};
  plane1_ll=newll; Line Loop(plane1_ll)={arc1,arc2,-line};
  plane2_ll=newll; Line Loop(plane2_ll)={arc3,arc4,line};
  surf1=news; Ruled Surface(surf1)={arc_ll};
  surf2=news; Ruled Surface(surf2)={plane1_ll};
  surf3=news; Ruled Surface(surf3)={plane2_ll};
  
  surf_sl=newsl; Surface Loop(surf_sl)={surf1,surf2,surf3};
  vol=newv; Volume(vol)={surf_sl};
  
  linePrev=newl; Line(linePrev)={pPrev,p1}; pPrev=p3;
  sphLineY_List[sphLineY_Num]=linePrev; sphLineY_Num++;
  sphLineY_List[sphLineY_Num]=arc1;     sphLineY_Num++;
  sphLineY_List[sphLineY_Num]=arc2;     sphLineY_Num++;
  
  sphLineZ_List[sphLineZ_Num]=linePrev; sphLineZ_Num++;
  sphLineZ_List[sphLineZ_Num]=-arc4;    sphLineZ_Num++;
  sphLineZ_List[sphLineZ_Num]=-arc3;    sphLineZ_Num++;
  
  sphSurfC_List[sphSurfC_Num]=surf1;    sphSurfC_Num++;
  sphSurfY_List[sphSurfY_Num]=surf2;    sphSurfY_Num++;
  sphSurfZ_List[sphSurfZ_Num]=surf3;    sphSurfZ_Num++;
  sphVol_List[sphVol_Num]=vol; sphVol_Num++;
Return

pPrev = pL[0];
For i In {1:nbInc}
  cx = lBox*(i-0.5);
  c = {cx,0,0}; // Centre de l'inclusion
  Call quartSphere;
EndFor
pLast = pR[0];

lastline=newl; Line(lastline)={pPrev,pLast};
sphLineY_List[sphLineY_Num]=lastline; sphLineY_Num++;
sphLineZ_List[sphLineZ_Num]=lastline; sphLineZ_Num++;


//------------------------------------------------------------------------------


// -----------------------------
// Construction du cadre (suite)
// -----------------------------

surf0_ll=newll; Line Loop(surf0_ll)={drL[0],-sphLineY_List[],-drR[0],drLR[1]};
surf1_ll=newll; Line Loop(surf1_ll)={drL[1],-drLR[1],-drR[1],drLR[2]};
surf2_ll=newll; Line Loop(surf2_ll)={drL[2],-drLR[2],-drR[2],drLR[3]};
surf3_ll=newll; Line Loop(surf3_ll)={drL[3],-drLR[3],-drR[3],sphLineZ_List[]};

surf0=news; Plane Surface(surf0)={surf0_ll};
surf1=news; Plane Surface(surf1)={surf1_ll};
surf2=news; Plane Surface(surf2)={surf2_ll};
surf3=news; Plane Surface(surf3)={surf3_ll};

surfLatMiddY = {surf0,surf2,sphSurfY_List[]};
surfLatMiddZ = {surf1,surf3,sphSurfZ_List[]};

surfRegMidd_sl=newsl; Surface Loop(surfRegMidd_sl)={surf0,surf1,surf2,surf3,sphSurfC_List[],surfL,-surfR};
volRegMidd=newv; Volume(volRegMidd)={surfRegMidd_sl};


//------------------------------------------------------------------------------


// -----------------------------
// Construction des extensions
// -----------------------------

out = Extrude {-lExt,0,0} {Surface{surfL};};
surfInterfL = out[0];
volExtL = out[1];
surfLatY_List[surfLatY_Num] = out[2]; surfLatY_Num++;
surfLatZ_List[surfLatZ_Num] = out[3]; surfLatZ_Num++;
surfLatY_List[surfLatY_Num] = out[4]; surfLatY_Num++;
surfLatZ_List[surfLatZ_Num] = out[5]; surfLatZ_Num++;

out=Extrude {-lPml,0,0} {Surface{out[0]};};
surfFinL = out[0];
volPmlL = out[1];
surfLatY_List[surfLatY_Num] = out[2]; surfLatY_Num++;
surfLatZ_List[surfLatZ_Num] = out[3]; surfLatZ_Num++;
surfLatY_List[surfLatY_Num] = out[4]; surfLatY_Num++;
surfLatZ_List[surfLatZ_Num] = out[5]; surfLatZ_Num++;

out=Extrude {lExt,0,0} {Surface{surfR};};
surfInterfR = out[0];
volExtR = out[1];
surfLatY_List[surfLatY_Num] = out[2]; surfLatY_Num++;
surfLatZ_List[surfLatZ_Num] = out[3]; surfLatZ_Num++;
surfLatY_List[surfLatY_Num] = out[4]; surfLatY_Num++;
surfLatZ_List[surfLatZ_Num] = out[5]; surfLatZ_Num++;

out=Extrude {lPml,0,0} {Surface{out[0]};};
surfFinR = out[0];
volPmlR = out[1];
surfLatY_List[surfLatY_Num] = out[2]; surfLatY_Num++;
surfLatZ_List[surfLatZ_Num] = out[3]; surfLatZ_Num++;
surfLatY_List[surfLatY_Num] = out[4]; surfLatY_Num++;
surfLatZ_List[surfLatZ_Num] = out[5]; surfLatZ_Num++;

/*
Printf("out[0] = %g", out[0]); // Surf Ext
Printf("out[1] = %g", out[1]); // Vol
Printf("out[2] = %g", out[2]); // Surf Lat Y
Printf("out[3] = %g", out[3]); // Surf Lat Z
Printf("out[4] = %g", out[4]); // Surf Lat Y
Printf("out[5] = %g", out[5]); // Surf Lat Z
*/


//------------------------------------------------------------------------------


// ----------------------
// D�finition des r�gions
// ----------------------

Physical Surface("Bnd_LatY")  = {surfLatY_List[],surfLatMiddY[]};
Physical Surface("Bnd_LatZ")  = {surfLatZ_List[],surfLatMiddZ[]};
Physical Surface("Bnd_Left")  = {surfFinL};
Physical Surface("Bnd_Right") = {surfFinR};

Physical Surface("Interf_Inc")   = {sphSurfC_List[]};
Physical Surface("Interf_Left")  = {surfL};
Physical Surface("Interf_Right") = {surfR};
Physical Surface("Interf_Pml_Left")  = {surfInterfL};
Physical Surface("Interf_Pml_Right") = {surfInterfR};

Physical Volume("Vol_Midd")  = {volRegMidd};
Physical Volume("Vol_Inc")   = {sphVol_List[]};
Physical Volume("Vol_Left")  = {volExtL};
Physical Volume("Vol_Right") = {volExtR};
Physical Volume("Vol_Pml_Left")  = {volPmlL};
Physical Volume("Vol_Pml_Right") = {volPmlR};


//------------------------------------------------------------------------------


// ------
// Points
// ------

pVizu[0]=newp; Point(pVizu[0])={-lSheet*0.45, lBox*0.05, lBox*0.05};
pVizu[1]=newp; Point(pVizu[1])={-lSheet*0.45, lBox*0.05, lBox*0.25};
pVizu[2]=newp; Point(pVizu[2])={-lSheet*0.45, lBox*0.25, lBox*0.25};
pVizu[3]=newp; Point(pVizu[3])={-lSheet*0.25, lBox*0.05, lBox*0.05};
pVizu[4]=newp; Point(pVizu[4])={-lSheet*0.25, lBox*0.05, lBox*0.25};
pVizu[5]=newp; Point(pVizu[5])={-lSheet*0.25, lBox*0.25, lBox*0.25};
pVizu[6]=newp; Point(pVizu[6])={ lSheet*1.25, lBox*0.05, lBox*0.05};
pVizu[7]=newp; Point(pVizu[7])={ lSheet*1.25, lBox*0.05, lBox*0.25};
pVizu[8]=newp; Point(pVizu[8])={ lSheet*1.25, lBox*0.25, lBox*0.25};
pVizu[9]=newp; Point(pVizu[9])={ lSheet*1.45, lBox*0.05, lBox*0.05};
pVizu[10]=newp; Point(pVizu[10])={ lSheet*1.45, lBox*0.05, lBox*0.25};
pVizu[11]=newp; Point(pVizu[11])={ lSheet*1.45, lBox*0.25, lBox*0.25};


//------------------------------------------------------------------------------


// ----
// Mesh
// ----

Field[1] = Attractor;
Field[1].FacesList = {sphSurfC_List[]};

Field[2] = Threshold;
Field[2].IField = 1;
Field[2].LcMin = lcInc;
Field[2].LcMax = lcAir;
Field[2].DistMin = r/2;
Field[2].DistMax = r;

Background Field = 2;

Mesh.CharacteristicLengthFromPoints = 1;
Mesh.CharacteristicLengthExtendFromBoundary = 0;
Mesh.Optimize = 1;

