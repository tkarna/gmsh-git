close all;
clear all;


%50 Ghz  5e10 - 2e-11
%500 Mhz  5e8 - 2e-9


% Parameters

nbInc = 1;

lBox   = 2e-4;
lSheet = lBox*nbInc;
lExt   = lSheet/2.;
lPml   = lBox/2.;

c = 299792458;
lc = 2.0e-5;
CFL = 0.05;
dt = lc/c * CFL;

tFin = 1e-11;

T = tFin/dt;
t = (0:T)*dt;

% Lower frenquency : 1/(T*dt)
% Higher frequency : 1/dt


% Incident signal

x0 = -lSheet/4.;
x1 = lSheet + lSheet/4.;
posPulse = -5*lSheet;
rPulse = lSheet;
for i=1:size(t,2)
 fTime0(i) = exp(-((x0-posPulse-c*t(i))/rPulse)^2);
 fTime1(i) = exp(-((x1-posPulse-c*t(i))/rPulse)^2);
 %fTime(i) = sin(2*pi*t(i)*1e12);
end

Fs = 1/dt;                          % Sampling frequency
NFFT = 2^nextpow2(size(t,2));       % Next power of 2 from length of func
fFreq = fft(fTime0,NFFT)/T;
abso = 2*abs(fFreq(1:NFFT/2+1));


% PLOT

figure;

subplot(2,1,1)
hold on
plot(t,fTime0)
plot(t,fTime1,'r')
title('Incident signal')
xlabel('Time  [s]')
ylabel('E(t)  [V/m] ')

subplot(2,1,2)
semilogx((Fs/2)*linspace(0,1,length(abso)),abso,'-o') 
xlabel('Frequency  [Hz]')
ylabel('|E(f)|  [V/m] ')


% EXPERIMENTAL (error in x-scale)
% abso2 = abs([fFreq(NFFT/2+2:end), fFreq(1:NFFT/2+1)]);
% subplot(3,1,3)
% plot((Fs/2)*linspace(-1,1,length(abso2)),abso2,'-o') 
% xlabel('Frequency  [Hz]')
% ylabel('|E(f)|  [V/m] ')


