
import cmath
from gmshpy import *
from dgpy import *
import gmshPartition
import os
import time
import math


print'---- Load Mesh & Build Groups'

model = GModel()
model.load(gmshPartition.simple('ClosedCavityShieldinterface.msh'))


dimension = 3
order = 1
groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()


print'---- Load Conservation law with parameters and source term'

law = dgConservationLawMaxwell()
mu0=4*math.pi*1e-7
eps0=8.85*1e-12


mu      = functionConstant([mu0,mu0,mu0])
epsilon = functionConstant([eps0,eps0,eps0])
rhoValue=4*math.pi*1e-7
cValue=1/math.sqrt(eps0*mu0)
dt =0.5e-11


at1=[0.,0.,0.,0.,0.,0.,0.,0.,0.,0.]
at2=[0.,0.,0.,0.,0.,0.,0.,0.,0.,0.]
at3=[0.,0.,0.,0.,0.,0.,0.,0.,0.,0.]

as1=[0.,0.,0.,0.,0.,0.,0.,0.,0.,0.]
as2=[0.,0.,0.,0.,0.,0.,0.,0.,0.,0.]
as3=[0.,0.,0.,0.,0.,0.,0.,0.,0.,0.]
qt2=[0.,0.,0.,0.,0.,0.,0.,0.,0.,0.]
qs2=[0.,0.,0.,0.,0.,0.,0.,0.,0.,0.]
qt1=[0.,0.,0.,0.,0.,0.,0.,0.,0.,0.]
qs1=[0.,0.,0.,0.,0.,0.,0.,0.,0.,0.]


###############################100###1mm##############
qs1r=[-7.2779770229893089e-12, 3.5707614312121083e-10, -8.1878955765413796e-09, 4.7553577194428948e-07, -6.7520653119903087e-06, 4.2323403573808430e-03, -1.6043276648176628e+12, 6.8003850006962637e+12, -2.1770334519881841e+12, -2.1770334519881841e+12]
qs1i=[0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 8.2074101270072754e+12, -8.2074101270072754e+12]
qs2r=[-8.2050942875529138e+00, -1.2160984121343355e+02, -1.3413642360070464e+03, -1.4483506579684697e+04, -1.3644039582072894e+05, -6.1072263615020933e+06, -7.9093439016861221e+10, -3.2308062500013055e+11, -7.5919443740551172e+11, -7.5919443740551172e+11]
qs2i=[0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 1.9275221303439523e+11, -1.9275221303439523e+11]
Ds=-2.1976798955726373e-01

qt1r=[2.6185038563412381e+14, 1.6558413608901332e+13, 6.8007484610276172e+12, 1.6043292719231646e+12, 3.4247438144158048e-03, -4.8453077651083788e-06, 1.5386699990399735e-07, -6.4666015637060201e-09, 7.1694365382788601e-11, -2.6472513898219578e-12]
qt1i=[0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00]
qt2r=[-2.2625084395139722e+12, -7.3122320557861853e+11, -3.2305244431751306e+11, -7.9093457852782227e+10, -9.2104821215592716e+06, -2.1259773736887201e+05, -1.0848383908730120e+04, -1.7706652542650413e+03, -6.2003559271811504e+01, -7.2860762853436056e-01]
qt2i=[0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00]
Dt=-1.8971485958805280e+02

for i in range(0,10):
  qt2[i]=complex(qt2r[i],qt2i[i])
  qs2[i]=complex(qs2r[i],qs2i[i])
  qt1[i]=complex(qt1r[i],qt1i[i])
  qs1[i]=complex(qs1r[i],qs1i[i])



for i in range(0,10):
   alpha=qt1[i]/qt2[i]
   beta=qt2[i]*dt
   at1[i]=cmath.exp(beta)
   at2[i]=alpha/beta*(1+(beta-1)*cmath.exp(beta))
   at3[i]=alpha/beta*(cmath.exp(beta)-beta-1)


for i in range(0,10):
   alpha=qs1[i]/qs2[i]
   beta=qs2[i]*dt
   as1[i]=cmath.exp(beta)
   as2[i]=alpha/beta*(1+(beta-1)*cmath.exp(beta))
   as3[i]=alpha/beta*(cmath.exp(beta)-beta-1)



law.setMu(mu)
law.setEpsilon(epsilon)
print'---- Load Initial conditions'

initialSolution = functionConstant([0,0,0,0,0,0])
solution = dgDofContainer(groups, law.getNbFields())
solution.L2Projection(initialSolution)
solution.setFieldName(0,'E_x')
solution.setFieldName(1,'E_y')
solution.setFieldName(2,'E_z')
solution.setFieldName(3,'H_x')
solution.setFieldName(4,'H_y')
solution.setFieldName(5,'H_z')
solution.exportMsh("output/Cuboid-00000", 0, 0)


etatr=[dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6)]
etasr=[dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6)]

etati=[dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6)]
etasi=[dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6)]


etatrr=[dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6)]
etasrr=[dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6)]

etatii=[dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6)]
etasii=[dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6),dgDofContainer(groups,6)]


prevsol = dgDofContainer(groups,6)
prevsol.scale(0.)

impt = dgDofContainer(groups,6)
imps = dgDofContainer(groups,6)

impt.scale(0.)
imps.scale(0.)



for i in range(0,10):
  etatr[i].scale(0.)
  etati[i].scale(0.)
  etasr[i].scale(0.)
  etasi[i].scale(0.)
  etatrr[i].scale(0.)
  etatii[i].scale(0.)
  etasrr[i].scale(0.)
  etasii[i].scale(0.)



  








t=0
tho=1.5e-9
aa=11e9*11e9/4/math.log(100)

def outletValueFunc(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    tt=t-tho
    value = math.exp(-aa*tt*tt)
    val.set(i,0,0)
    val.set(i,1,value)
    val.set(i,2,0)
    val.set(i,3,value/(cValue*rhoValue))
    val.set(i,4,0)
    val.set(i,5,0)
outletValue = functionPython(6, outletValueFunc, [xyz])



law.addInterfaceShield('InterfaceO',impt.getFunction(),imps.getFunction())
law.addInterfaceShield('InterfaceI',impt.getFunction(),imps.getFunction())
law.addInterfaceShield('InterfaceT',impt.getFunction(),imps.getFunction())
law.addInterfaceShield('InterfaceB',impt.getFunction(),imps.getFunction())
law.addInterfaceShield('InterfaceL',impt.getFunction(),imps.getFunction())
law.addInterfaceShield('InterfaceR',impt.getFunction(),imps.getFunction())



law.addBoundaryCondition('Output',   law.newBoundarySilverMullerDirichlet('Output', outletValue))
law.addBoundaryCondition('Input',   law.newBoundarySilverMullerDirichlet('Input', outletValue))
law.addBoundaryCondition('Top',     law.newBoundaryElectricWall('Top'))
law.addBoundaryCondition('Bottom',     law.newBoundaryElectricWall('Bottom'))
law.addBoundaryCondition('Left',    law.newBoundaryMagneticWall('Left'))
law.addBoundaryCondition('Right',    law.newBoundaryMagneticWall('Right'))


incidField=fullMatrixDouble(1,6)
def psolShield(val,xyz, solution):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    val.set(i,0,solution.get(i,0))
    val.set(i,1,solution.get(i,1))
    val.set(i,2,solution.get(i,2))
    val.set(i,3,solution.get(i,3))
    val.set(i,4,solution.get(i,4))
    val.set(i,5,solution.get(i,5))
psol = functionPython(6,psolShield,[xyz, solution.getFunction()])
incidfield = dgFunctionEvaluator(groups, psol)
print'---- LOOP'

incidFieldc=fullMatrixDouble(1,6)

print'---- LOOP'
sys = linearSystemPETScBlockDouble()
dof = dgDofManager.newDGBlock(groups, 6, sys)
solver=dgDIRK(law,dof,1)
tic = time.clock()
numoutput = 1
for i in range(0,20000):
  t = dt*i
  x = time.clock()
  prevsol.copy(solution)
  solver.iterate(solution, dt, t)
  norm = solution.norm()
  print'|ITER|',i,'/ 20000 |NORM|',norm, '|DT|',dt, '|T|',t, '|CPU|',time.clock()- tic
  if (i % 100 == 0):
    solution.exportMsh("output/Cav-%05i" % numoutput, t, numoutput)
    numoutput = numoutput+1
  incidfield.compute(0.0, 0.0,0.0, incidFieldc)
  impt.scale(0.)
  imps.scale(0.)
  impt.axpy(solution,Dt)
  imps.axpy(solution,Ds)
  for i in range(0,10):
        etatr[i].copy(etatrr[i])
  	etati[i].copy(etatii[i])
  	etasr[i].copy(etasrr[i])
  	etasi[i].copy(etasii[i])
  	etatrr[i].scale(0.)
  	etatii[i].scale(0.)
  	etasrr[i].scale(0.)
  	etasii[i].scale(0.)
  	etatrr[i].axpy(etatr[i],at1[i].real)
  	etatrr[i].axpy(etati[i],-at1[i].imag)
  	etatrr[i].axpy(prevsol,at2[i].real)
  	etatrr[i].axpy(solution,at3[i].real)
  	etatii[i].axpy(etatr[i],at1[i].imag)
  	etatii[i].axpy(etati[i],at1[i].real)
  	etatii[i].axpy(prevsol,at2[i].imag)
  	etatii[i].axpy(solution,at3[i].imag)
  	etasrr[i].axpy(etasr[i],as1[i].real)
  	etasrr[i].axpy(etasi[i],-as1[i].imag)
  	etasrr[i].axpy(prevsol,as2[i].real)
  	etasrr[i].axpy(solution,as3[i].real)
  	etasii[i].axpy(etasr[i],as1[i].imag)
  	etasii[i].axpy(etasi[i],as1[i].real)
  	etasii[i].axpy(prevsol,as2[i].imag)
  	etasii[i].axpy(solution,as3[i].imag)
  for i in range(0,10):
        impt.axpy(etatrr[i],1.)
        imps.axpy(etasrr[i],1.)
  
  law.addInterfaceShield('InterfaceO',impt.getFunction(),imps.getFunction())
  law.addInterfaceShield('InterfaceI',impt.getFunction(),imps.getFunction())
  law.addInterfaceShield('InterfaceT',impt.getFunction(),imps.getFunction())
  law.addInterfaceShield('InterfaceB',impt.getFunction(),imps.getFunction())
  law.addInterfaceShield('InterfaceL',impt.getFunction(),imps.getFunction())
  law.addInterfaceShield('InterfaceR',impt.getFunction(),imps.getFunction())
  if (i % 1 == 0 and Msg.GetCommRank() == 0):
    print("%.8e  %.8e %.8e %.8e %.8e %.8e  %.8e \n"% (t, incidFieldc(0,0),incidFieldc(0,1),incidFieldc(0,2),incidFieldc(0,3),incidFieldc(0,4),incidFieldc(0,5)))
    fout_shield=open("ES100.dat","a")
    fout_shield.write("%.8e  %.8e %.8e %.8e %.8e %.8e  %.8e \n"% (t, incidFieldc(0,0),incidFieldc(0,1),incidFieldc(0,2),incidFieldc(0,3),incidFieldc(0,4),incidFieldc(0,5)))
    fout_shield.close()
Msg.Exit(0);
