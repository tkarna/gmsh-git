

lc = 0.3;

// Dimensions du domaine (Lx,Ly,Lz) et des extensions

Lx_left=1;
Lx=2;
Lx_right=1;
Ly=1;
Lz=1;


// Points

p0[0]=newp; Point(p0[0])={-Lx_left-Lx/2,-Ly/2,-Lz/2,lc};
p0[1]=newp; Point(p0[1])={-Lx_left-Lx/2, Ly/2,-Lz/2,lc};
p0[2]=newp; Point(p0[2])={-Lx_left-Lx/2, Ly/2, Lz/2,lc};
p0[3]=newp; Point(p0[3])={-Lx_left-Lx/2,-Ly/2, Lz/2,lc};

p1[0]=newp; Point(p1[0])={-Lx/2,-Ly/2,-Lz/2,lc};
p1[1]=newp; Point(p1[1])={-Lx/2, Ly/2,-Lz/2,lc};
p1[2]=newp; Point(p1[2])={-Lx/2, Ly/2, Lz/2,lc};
p1[3]=newp; Point(p1[3])={-Lx/2,-Ly/2, Lz/2,lc};

p2[0]=newp; Point(p2[0])={Lx/2,-Ly/2,-Lz/2,lc};
p2[1]=newp; Point(p2[1])={Lx/2, Ly/2,-Lz/2,lc};
p2[2]=newp; Point(p2[2])={Lx/2, Ly/2, Lz/2,lc};
p2[3]=newp; Point(p2[3])={Lx/2,-Ly/2, Lz/2,lc};

p3[0]=newp; Point(p3[0])={Lx_left+Lx/2,-Ly/2,-Lz/2,lc};
p3[1]=newp; Point(p3[1])={Lx_left+Lx/2, Ly/2,-Lz/2,lc};
p3[2]=newp; Point(p3[2])={Lx_left+Lx/2, Ly/2, Lz/2,lc};
p3[3]=newp; Point(p3[3])={Lx_left+Lx/2,-Ly/2, Lz/2,lc};


// Lines

For i In {0:3}
  l0[i]=newl; Line(l0[i])={p0[i],p0[(i+1)%4]};
  l1[i]=newl; Line(l1[i])={p1[i],p1[(i+1)%4]};
  l2[i]=newl; Line(l2[i])={p2[i],p2[(i+1)%4]};
  l3[i]=newl; Line(l3[i])={p3[i],p3[(i+1)%4]};
EndFor

For i In {0:3}
  l01[i]=newl; Line(l01[i])={p0[i],p1[i]};
  l12[i]=newl; Line(l12[i])={p1[i],p2[i]};
  l23[i]=newl; Line(l23[i])={p2[i],p3[i]};
EndFor


// Surfaces

surfVert_ll[0]=newll; Line Loop(surfVert_ll[0])={l0[]};
surfVert_ll[1]=newll; Line Loop(surfVert_ll[1])={l1[]};
surfVert_ll[2]=newll; Line Loop(surfVert_ll[2])={l2[]};
surfVert_ll[3]=newll; Line Loop(surfVert_ll[3])={l3[]};

For i In {0:3}
  surfVert[i]=news; Plane Surface(surfVert[i])={surfVert_ll[i]};
EndFor

For i In {0:3}
  surfBnd_ll[i]=newll; Line Loop(surfBnd_ll[i])={l0[i],l01[(i+1)%4],-l1[i],-l01[i]};
  surfBndLeft[i]=news; Plane Surface(surfBndLeft[i])={surfBnd_ll[i]};
  surfBnd_ll[i]=newll; Line Loop(surfBnd_ll[i])={l1[i],l12[(i+1)%4],-l2[i],-l12[i]};
  surfBndMidd[i]=news; Plane Surface(surfBndMidd[i])={surfBnd_ll[i]};
  surfBnd_ll[i]=newll; Line Loop(surfBnd_ll[i])={l2[i],l23[(i+1)%4],-l3[i],-l23[i]};
  surfBndRight[i]=news; Plane Surface(surfBndRight[i])={surfBnd_ll[i]};
EndFor

surfBndY[] = {surfBndLeft[1],surfBndLeft[3],surfBndMidd[1],surfBndMidd[3],surfBndRight[1],surfBndRight[3]};
surfBndZ[] = {surfBndLeft[0],surfBndLeft[2],surfBndMidd[0],surfBndMidd[2],surfBndRight[0],surfBndRight[2]};

// Volumes

surfRegLeft_ls=newsl;  Surface Loop(surfRegLeft_ls)={-surfVert[0],surfVert[1],surfBndLeft[]};
surfRegMidd_ls=newsl;  Surface Loop(surfRegMidd_ls)={-surfVert[1],surfVert[2],surfBndMidd[]};
surfRegRight_ls=newsl; Surface Loop(surfRegRight_ls)={-surfVert[2],surfVert[3],surfBndRight[]};

volRegLeft=newv;  Volume(volRegLeft)={surfRegLeft_ls};
volRegMidd=newv;  Volume(volRegMidd)={surfRegMidd_ls};
volRegRight=newv; Volume(volRegRight)={surfRegRight_ls};


// Physical Regions

Physical Surface("Bnd_Left")  = {surfVert[0]};
Physical Surface("Bnd_Right") = {surfVert[3]};

Physical Surface("Bnd_LatY") = {surfBndY[]};
Physical Surface("Bnd_LatZ") = {surfBndZ[]};

Physical Surface("Interf_Left")  = {surfVert[1]};
Physical Surface("Interf_Right") = {surfVert[2]};

Physical Volume("Vol_Left")  = {volRegLeft};
Physical Volume("Vol_Midd")  = {volRegMidd};
Physical Volume("Vol_Right") = {volRegRight};
