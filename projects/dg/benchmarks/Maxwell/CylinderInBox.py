from gmshpy import *
from dgpy import *
import os
import time
import math



print'---- Load Mesh & Build Groups'

model = GModel()
model.load('CylinderInBox.msh')

dimension = 3
order = 1
groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()



print'---- Load Conservation law & Physical parameters'

mu      = functionConstant([1,1,1])
epsilon = functionConstant([1,1,1])

law = dgConservationLawMaxwell() 
law.setMu(mu)
law.setEpsilon(epsilon)



print'---- Load Initial conditions'

def initialSolution(val, xyz ):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    val.set(i,0,0)
    val.set(i,1,0*math.exp(-(x*x + y*y +z*z)))
    val.set(i,2,0) 
    val.set(i,3,0)
    val.set(i,4,0)
    val.set(i,5,0)
#initialSolutionFunc = functionPython(6, initialSolution, [xyz])
initialSolutionFunc = functionConstant([0,0,0,0,0,0])

solution = dgDofContainer(groups, law.getNbFields())
solution.L2Projection(initialSolutionFunc)
solution.setFieldName(0,'e_x')
solution.setFieldName(1,'e_y')
solution.setFieldName(2,'e_z')
solution.setFieldName(3,'h_x')
solution.setFieldName(4,'h_y')
solution.setFieldName(5,'h_z')
solution.exportMsh("output/Maxw-00000", 0, 0)



print'---- Load Source terms'
 
k_onde=2*math.pi
freq = k_onde
t=0

def value_incident(f, xyz ):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)     
    f.set(i,0,0)
    f.set(i,1,0)
    f.set(i,2,(k_onde/freq)*math.cos(k_onde*x-freq*t))
    f.set(i,3,0)
    f.set(i,4,math.cos(k_onde*x-freq*t))
    f.set(i,5,0)

def value_source(f, xyz ):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)    
    f.set(i,0,0)
    f.set(i,1,0)
    f.set(i,2,k_onde*math.sin(k_onde*x-freq*t))
    f.set(i,3,0)
    f.set(i,4,freq*math.sin(k_onde*x-freq*t))
    f.set(i,5,0)

champ_incident = functionPython(6, value_incident, [xyz])
source         = functionPython(6, value_source, [xyz])

law.setIncidentField(champ_incident)
law.setSourceField(source)



print'---- Load Boundary conditions'

def value_inlet(f, xyz ):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    f.set(i,0,0)
    f.set(i,1,0)
    f.set(i,2,0)
    f.set(i,3,0)
    f.set(i,4,0)
    f.set(i,5,0)

def value_outlet(f, xyz ):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    f.set(i,0,0)
    f.set(i,1,0)
    f.set(i,2,0)
    f.set(i,3,0)
    f.set(i,4,0)
    f.set(i,5,0)

#inletFunc  = functionPython(6, value_inlet, [xyz])
#outletFunc = functionPython(6, value_outlet, [xyz])
inletFunc  = functionConstant([0,0,0,0,0,0])
outletFunc = functionConstant([0,0,0,0,0,0])

#law.addBoundaryCondition("wall",    law.newBoundaryWall())
#law.addBoundaryCondition('symmetry',law.newBoundaryWall())
#law.addBoundaryCondition('inlet',   law.newBoundaryWall())
#law.addBoundaryCondition('outlet',  law.newBoundaryWall())
law.addBoundaryCondition('symmetry',law.newSymmetryBoundary('symmetry'))
law.addBoundaryCondition('wall',    law.newSymmetryBoundary('wall'))
law.addBoundaryCondition('inlet',   law.newOutsideValueBoundary('inlet',  inletFunc))
law.addBoundaryCondition('outlet',  law.newOutsideValueBoundary('outlet', outletFunc))

#law.addBoundaryCondition('lateral_cylinder',law.newSymmetryBoundary('lateral_cylinder'))
law.addBoundaryCondition('lateral_cylinder',law.newBoundaryHomogeneousDirichletOnE('lateral_cylinder'))



print'---- LOOP'

solver = dgERK(law, None, DG_ERK_44)
x = time.clock()
CFL = 1.0
t = 0.
for i in range(0,1000): 
  dt = CFL * solver.computeInvSpectralRadius(solution)
  t = t+dt
  #dt = 0.001
  norm = solver.iterate(solution, dt, t)
  t = t + dt
  if (i % 10 == 0):
     print '|ITER|',i, '|NORM|',norm, '|DT|',dt, '|T|',t, '|CPU|',time.clock() - x
     solution.exportMsh("output/MaxwCylinderInBox-%05i" % (i), t, i)

