Mesh.CharacteristicLengthExtendFromBoundary=1;
Mesh.CharacteristicLengthFromPoints=1;

lc = 0.25;
a=1;
H=5;
Point(1) = {0, 0, -H,lc};
Point(2) = {a, 0, -H,lc};
Point(3) = {0, a, -H,lc};
Point(4) = {0, -a, -H,lc};
Point(5) = {-a, 0, -H,lc};
Circle(1) = {3, 1, 2};
Circle(2) = {2, 1, 4};
Circle(3) = {4, 1, 5};
Circle(4) = {5, 1, 3};
Extrude {0, 0, 2*H} {
  Line{4, 1, 2, 3};
}


//Compound Surface(10000) = {20, 16, 12, 8};

Line Loop(21)= {1,2,3,4};
Plane Surface(22) = {21};
Line Loop(23)={17,13,9,5};
Plane Surface(24)={23};
Surface Loop(25) = {20,16,12,8,22,24};
//Physical Surface("inlet_outlet_cylinder") ={22,24};
Physical Surface("lateral_cylinder") = {20,8,16,12};
Volume(26) = {25};
//Physical Volume("cylinder") = {26};





lc2 = 1;
w = 5;
Point(100) = {-w,-w,-w,lc2};
Point(101) = {w,-w,-w,lc2};
Point(102) = {w,w,-w,lc2};
Point(103) = {-w,w,-w,lc2};
Point(104) = {-w,-w,w,lc2};
Point(105) = {w,-w,w,lc2};
Point(106) = {w,w,w,lc2};
Point(107) = {-w,w,w,lc2};
Line(31) = {104,105};
Line(32) = {105,101};
Line(33) = {101,100};
Line(34) = {100,104};
Line(35) = {104,107};
Line(36) = {107,103};
Line(37) = {103,100};
Line(38) = {103,102};
Line(39) = {102,106};
Line(40) = {106,107};
Line(41) = {106,105};
Line(42) = {101,102};

Line Loop(43) = {38,39,40,36};
Plane Surface(44) = {43};
Line Loop(45) = {36,37,34,35};
Plane Surface(46) = {45};

//***
Line Loop(47) = {35,-40,41,-31};
Plane Surface(48) = {47, 23};

Line Loop(49) = {32,42,39,41};
Plane Surface(50) = {49};

//***
Line Loop(51) = {42,-38,37,-33};
Plane Surface(52) = {51, 21};

Line Loop(53) = {33,34,31,32};
Plane Surface(54) = {53};


Physical Surface("inlet") ={44};
Physical Surface("outlet") = {54};
Physical Surface("wall") = {46,50};
Physical Surface("symmetry") ={48,52};

Surface Loop(55) = {50, 54, 52, 44, 48, 46, 16, 20, 8, 12};
Volume(56) = {55};
Physical Volume("box") = {56};
