
x0 = 3.;
y0 = 3.;
z0 = 3.;
lc0 = 0.05;

x1 = 1.;
y1 = 1.;
z1 = 1.;
lc1 = 0.05;


Point(1) = { x0, y0, z0};
Point(2) = { x0,-y0, z0};
Point(3) = {-x0,-y0, z0};
Point(4) = {-x0, y0, z0};

Point(5) = { x0, y0, -z0};
Point(6) = { x0,-y0, -z0};
Point(7) = {-x0,-y0, -z0};
Point(8) = {-x0, y0, -z0};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};
Line(5) = {5,6};
Line(6) = {6,7};
Line(7) = {7,8};
Line(8) = {8,5};
Line(9) = {1,5};
Line(10) = {2,6};
Line(11) = {3,7};
Line(12) = {4,8};


Line Loop(1) = {1,2,3,4};
Plane Surface(100) = {1};
Line Loop(2) = {5,6,7,8};
Plane Surface(200) = {2};

Line Loop(3) = {4,9,-8,-12};
Plane Surface(300) = {3};
Line Loop(4) = {10,6,-11,-2};
Plane Surface(400) = {4};
Line Loop(5) = {11,7,-12,-3};
Plane Surface(500) = {5};
Line Loop(6) = {1,10,-5,-9};
Plane Surface(600) = {6};

Point(21) = { x1, y1, z1};
Point(22) = { x1,-y1, z1};
Point(23) = {-x1,-y1, z1};
Point(24) = {-x1, y1, z1};
Point(25) = { x1, y1, -z1};
Point(26) = { x1,-y1, -z1};
Point(27) = {-x1,-y1, -z1};
Point(28) = {-x1, y1, -z1};

Line(21) = {21,22};
Line(22) = {22,23};
Line(23) = {23,24};
Line(24) = {24,21};
Line(25) = {25,26};
Line(26) = {26,27};
Line(27) = {27,28};
Line(28) = {28,25};
Line(29) = {21,25};
Line(30) = {22,26};
Line(31) = {23,27};
Line(32) = {24,28};


Line Loop(7) = {21,22,23,24};
Plane Surface(700) = {7};
Line Loop(8) = {25,26,27,28};
Plane Surface(800) = {8};

Line Loop(9) = {24,29,-28,-32};
Plane Surface(900) = {9};
Line Loop(10) = {30,26,-31,-22};
Plane Surface(1000) = {10};
Line Loop(11) = {31,27,-32,-23};
Plane Surface(1100) = {11};
Line Loop(12) = {21,30,-25,-29};
Plane Surface(1200) = {12};


Surface Loop(1)={700,800,900,1000,1100,1200};
Volume(1)={1};

Surface Loop(2)={100,200,300,400,500,600};
Volume(2)={2,-1};


Physical Surface("interfaceO") = {800};
Physical Surface("interfaceI") = {700};
Physical Surface("interfaceT") = {900};
Physical Surface("interfaceB") = {1000};
Physical Surface("interfaceL") = {1100};
Physical Surface("interfaceR") = {1200};

Physical Surface("Output") = {200};
Physical Surface("Input") = {100};
Physical Surface("Top") = {300};
Physical Surface("Bottom") = {400};
Physical Surface("Left") = {500};
Physical Surface("Right") = {600};

Physical Volume("box") = {1};
Physical Volume("air") = {2};

//Mesh.CharacteristicLengthExtendFromBoundary = 1;
//Mesh.CharacteristicLengthFromPoints = 1;

