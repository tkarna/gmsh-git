from dgpy import *
import os
import time
import math


### Parameters

# Physical

nbInc = 1

lBox   = 2e-4
lSheet = lBox*nbInc
lExt   = lSheet/2.
lPml   = lBox/2.

c = 299792458
lc = 0.4e-5
CFL = 0.001
dt = lc/c * CFL
dtPhys = dt

tf = 1e-11

iMax = int(tf/dt)
iExport = iMax/100
iData = 500
iVizu = 10

# Normalized

LREF = 2e-4*nbInc
ZREF = 1
CREF = 299792458
TREF = LREF/CREF

lBox   /= LREF
lSheet /= LREF
lExt   /= LREF
lPml   /= LREF
c      /= CREF
lc     /= LREF
dt     /= TREF
tf     /= TREF


### Load Mesh & Build Groups

model = GModel()
model.load('HeterogeneousSheetAbc.msh')

dimension = 3
order = 1
groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()


### Load Conservation Law

law = dgConservationLawMaxwell()

epsRelWall = 2
sigmaRelInc = 10e3*LREF*ZREF

epsilon = functionConstantByTag({"Vol_Midd"      : [epsRelWall,epsRelWall,epsRelWall],
                                 "Vol_Inc"       : [1.,1.,1.],
                                 "Vol_Left"      : [1.,1.,1.],
                                 "Vol_Right"     : [1.,1.,1.],
                                 "Vol_Pml_Left"  : [1.,1.,1.],
                                 "Vol_Pml_Right" : [1.,1.,1.]})
mu = functionConstant([1.,1.,1.])
sigma   = functionConstantByTag({"Vol_Midd"      : [0.,0.,0.],
                                 "Vol_Inc"       : [sigmaRelInc,sigmaRelInc,sigmaRelInc],
                                 "Vol_Left"      : [0.,0.,0.],
                                 "Vol_Right"     : [0.,0.,0.],
                                 "Vol_Pml_Left"  : [0.,0.,0.],
                                 "Vol_Pml_Right" : [0.,0.,0.]})

law.setEpsilon(epsilon)
law.setMu(mu)
law.setSigma(sigma)

CCode = """
#include "fullMatrix.h"
#include "function.h"
#include "math.h"
extern "C" {
void incFieldsDef(dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &xyz, fullMatrix<double> &t) {
  for (size_t i=0; i<val.size1(); i++) {
    double lSheet = 1.;
    double posPulse = -5*lSheet;
    double rPulse   =  1*lSheet;
    double value = exp(-pow(((xyz(i,0) - posPulse - t(0,0))/rPulse),2));
    val.set(i,0,0.); // e
    val.set(i,1,0.);
    val.set(i,2,value);
    val.set(i,3,0.); // h
    val.set(i,4,-value);
    val.set(i,5,0.);
  }
}
}
"""
tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0):
  functionC.buildLibrary (CCode, tmpLib);
Msg.Barrier()

t = 0.
tFunc = functionConstant([t])
incFieldsFunc = functionC(tmpLib,"incFieldsDef",6,[xyz,tFunc])
nullFieldsFunc = functionConstant([0.,0.,0.,0.,0.,0.])

law.addBoundaryCondition('Bnd_LatY',  law.newBoundaryMagneticWall('Bnd_LatY'))
law.addBoundaryCondition('Bnd_LatZ',  law.newBoundaryElectricWall('Bnd_LatZ'))
law.addBoundaryCondition('Bnd_Left',  law.newBoundarySilverMullerDirichlet('Bnd_Left',incFieldsFunc))
law.addBoundaryCondition('Bnd_Right', law.newBoundarySilverMullerDirichlet('Bnd_Right',nullFieldsFunc))


### Load Initial and Reference Solutions

solAbc = dgDofContainer(groups, law.getNbFields())
solAbc.setFieldName(0,'eAbc_x')
solAbc.setFieldName(1,'eAbc_y')
solAbc.setFieldName(2,'eAbc_z')
solAbc.setFieldName(3,'hAbc_x')
solAbc.setFieldName(4,'hAbc_y')
solAbc.setFieldName(5,'hAbc_z')
solAbc.L2Projection(nullFieldsFunc)
solAbc.exportMshSingleComp(2,"output/FieldsAbc_0000", 0, 0)

solInc = dgDofContainer(groups, 6)
solInc.setFieldName(0,'eInc_x')
solInc.setFieldName(1,'eInc_y')
solInc.setFieldName(2,'eInc_z')
solInc.setFieldName(3,'hInc_x')
solInc.setFieldName(4,'hInc_y')
solInc.setFieldName(5,'hInc_z')
solInc.L2Projection(incFieldsFunc)
solInc.exportMshSingleComp(2,"output/FieldsInc_0000", 0, 0)


### LOOP

#sys = linearSystemPETScBlockDouble()
#sys.setParameter("petscOptions", "-pc_type ilu")
#dof = dgDofManager.newDGBlock(groups, law.getNbFields(), sys)

solver = dgERK(law, None, DG_ERK_44)

#solEval = dgFunctionEvaluator(groups, solAbc.getFunction())
#solValue = fullMatrixDouble(1,6)
#xEval = [-lSheet*0.45, -lSheet*0.25, lSheet*1.25, lSheet*1.45]
#yEval = [ lBox*0.05, lBox*0.25]
#zEval = [ lBox*0.05, lBox*0.25]

numOutput = 1
timeInit = time.clock()
for i in range (1,iMax+1) :
  Msg.Barrier()
  t = dt*i
  tFunc.set(t)
  Msg.Barrier()
  solver.iterate(solAbc, dt, t)
  
  if (i % iVizu == 0):
    ergAbc = law.getTotalEnergy(groups, solAbc, "")
    ergInc = law.getTotalEnergy(groups, solInc, "")
    if (Msg.GetCommRank() == 0):
      timeCurrent = time.clock()
      timeMeanByIter = (timeCurrent-timeInit)/(1.*i)/(60.*60.)
      timeExp = timeMeanByIter*(iMax-i)
      print '|ITER|', i, '/', iMax, '|dt|', dtPhys, '|ergAbc|', ergAbc, '|ergInc|', ergInc, '|TimeExp|', timeExp
  
  #if (i % iData == 0):
  #  for iX in range (0,4):
  #    for iY in range (0,2):
  #      for iZ in range (0,2):
  #        solEval.compute(xEval[iX], yEval[iY], zEval[iZ], solValue)
  #        Msg.Barrier()
  #        if (Msg.GetCommRank() == 0):
  #          fileOut = open("output/Data_%i_%i_%i.out" % (iX,iY,iZ), "a")
  #          fileOut.write("%.8e %.8e %.8e %.8e %.8e %.8e %.8e\n" % (t, solValue(0,0), solValue(0,1), solValue(0,2), solValue(0,3), solValue(0,4), solValue(0,5)) )
  #          fileOut.close()
  
  if (i % iExport == 0):
    solAbc.exportMshSingleComp(2,"output/FieldsAbc_%04i" % numOutput, t, numOutput)
    solInc.L2Projection(incFieldsFunc)
    solInc.exportMshSingleComp(2,"output/FieldsInc_%04i" % numOutput, t, numOutput)
    numOutput = numOutput+1


Msg.Exit(0);

