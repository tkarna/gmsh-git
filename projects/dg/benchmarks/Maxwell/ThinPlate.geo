

lc = 0.3;

// Dimensions de la plaque (Lx,Ly,Lz) et des extensions
Lx_left=2;
Lx=1;
Lx_right=2;
Ly=10;
Lz=10;

// Distance minimum d'une inclusion avec les fronti�res/autres-inclusions
tolBound = 0.1;
tolInter = 0.1;

// Rayon minimum/maximum des inclusions
rMin = lc/2;
rMax = Lx/4;

// Nombre d'inclusions
nbIncAsk = 100;


//------------------------------------------------------------------------------


bndSurf_Num=0;   // bndSurf_List[];
vertSurf_Num=0;  // vertSurf_List[];
sphSurf_Num=0;   // sphSurf_List[];
sphVol_Num=0;    // sphVol_List[];
IncNum=0;        // IncCx_List[]; IncCy_List[]; IncCz_List[]; IncR_List[];

Function extrudeBox
  out=Extrude {Lextrude,0,0} {Line{dr[]}; Surface{surf};};
  dr[0]=out[0];
  dr[1]=out[4];
  dr[2]=out[8];
  dr[3]=out[12];
  surf=out[16];
  bndSurf_List[bndSurf_Num]=out[1];  bndSurf_Num++;
  bndSurf_List[bndSurf_Num]=out[5];  bndSurf_Num++;
  bndSurf_List[bndSurf_Num]=out[9];  bndSurf_Num++;
  bndSurf_List[bndSurf_Num]=out[13]; bndSurf_Num++;
  vertSurf_List[vertSurf_Num]=surf;  vertSurf_Num++;
Return

Function sphere
  center=newp; Point(center)={c[0],c[1],c[2],lc};
  p1=newp; Point(p1)={c[0]-r,c[1],c[2],lc};
  p2=newp; Point(p2)={c[0],c[1],c[2]+r,lc};
  p3=newp; Point(p3)={c[0]+r,c[1],c[2],lc};
  p4=newp; Point(p4)={c[0],c[1]+r,c[2],lc};
  arc0=newl; Circle(arc0)={p1,center,p2};
  arc1=newl; Circle(arc1)={p2,center,p3};
  arc2=newl; Circle(arc2)={p3,center,p4};
  arc3=newl; Circle(arc3)={p4,center,p1};
  arc_ll=newll; Line Loop(arc_ll)={arc0,arc1,arc2,arc3};
  surf0=news; Ruled Surface(surf0)={arc_ll};
  surf1=Rotate {{1,0,0},{c[0],c[1],c[2]},  Pi/2} { Duplicata{ Surface{surf0}; } };
  surf2=Rotate {{1,0,0},{c[0],c[1],c[2]},  Pi  } { Duplicata{ Surface{surf0}; } };
  surf3=Rotate {{1,0,0},{c[0],c[1],c[2]},3*Pi/2} { Duplicata{ Surface{surf0}; } };
  surf_sl=newsl; Surface Loop(surf_sl)={surf0,surf1,surf2,surf3};
  vol=newv; Volume(vol)={surf_sl};
  sphSurf_List[sphSurf_Num]=surf0; sphSurf_Num++;
  sphSurf_List[sphSurf_Num]=surf1; sphSurf_Num++;
  sphSurf_List[sphSurf_Num]=surf2; sphSurf_Num++;
  sphSurf_List[sphSurf_Num]=surf3; sphSurf_Num++;
  sphVol_List[sphVol_Num]=vol; sphVol_Num++;
Return


//------------------------------------------------------------------------------


// ---------------------
// Construction du cadre
// ---------------------

p[0]=newp; Point(p[0])={-Lx_left-Lx/2,-Ly/2,-Lz/2,lc};
p[1]=newp; Point(p[1])={-Lx_left-Lx/2, Ly/2,-Lz/2,lc};
p[2]=newp; Point(p[2])={-Lx_left-Lx/2, Ly/2, Lz/2,lc};
p[3]=newp; Point(p[3])={-Lx_left-Lx/2,-Ly/2, Lz/2,lc};
dr[0]=newl; Line(dr[0])={p[0],p[1]};
dr[1]=newl; Line(dr[1])={p[1],p[2]};
dr[2]=newl; Line(dr[2])={p[2],p[3]};
dr[3]=newl; Line(dr[3])={p[3],p[0]};
dr_ll=newll; Line Loop(dr_ll)={dr[]};
surf=news; Plane Surface(surf)={dr_ll};

vertSurf_List[vertSurf_Num]=surf; vertSurf_Num++;

Lextrude = Lx_left;
Call extrudeBox;
Lextrude = Lx;
Call extrudeBox;
Lextrude = Lx_right;
Call extrudeBox;


// ---------------------------
// Construction des inclusions
// ---------------------------

For i In {1:1000}
  
  cx = (Rand(1)-0.5)*Lx;
  cy = (Rand(1)-0.5)*Ly;
  cz = (Rand(1)-0.5)*Lz;
  c = {cx,cy,cz};            // Centre de l'inclusion
  r = Rand(rMax-rMin)+rMin;  // Rayon de l'inclusion
  
  IncValid = 1;  // Inclusion valide ???
  
  If ((Fabs(cx)+r)>(Lx/2-tolBound))
    IncValid = 0;
  EndIf
  If ((Fabs(cy)+r)>(Ly/2-tolBound))
    IncValid = 0;
  EndIf
  If ((Fabs(cz)+r)>(Lz/2-tolBound))
    IncValid = 0;
  EndIf
  For j In {0:(IncNum-1)}
    dist  = (IncCx_List[j]-cx) * (IncCx_List[j]-cx);
    dist += (IncCy_List[j]-cy) * (IncCy_List[j]-cy);
    dist += (IncCz_List[j]-cz) * (IncCz_List[j]-cz);
    dist  = Sqrt(dist);
    dist -= (r+IncR_List[j]);
    If (dist<tolInter)
      IncValid = 0;
    EndIf
  EndFor
  
  If (IncValid==1)
    Call sphere;
    IncCx_List[IncNum] = cx;
    IncCy_List[IncNum] = cy;
    IncCz_List[IncNum] = cz;
    IncR_List[IncNum] = r;
    IncNum++;
  EndIf
  
  If (IncNum==nbIncAsk)
    i=2000;
  EndIf
  
EndFor


// ----------------------
// D�finition des r�gions
// ----------------------

surfBndLeft  = {bndSurf_List[0],bndSurf_List[1],bndSurf_List[2],bndSurf_List[3]};
surfBndMidd  = {bndSurf_List[4],bndSurf_List[5],bndSurf_List[6],bndSurf_List[7]};
surfBndRight = {bndSurf_List[8],bndSurf_List[9],bndSurf_List[10],bndSurf_List[11]};

surfRegLeft_ls=newsl;  Surface Loop(surfRegLeft_ls)={-vertSurf_List[0],vertSurf_List[1],surfBndLeft[]};
surfRegMidd_ls=newsl;  Surface Loop(surfRegMidd_ls)={-vertSurf_List[1],vertSurf_List[2],surfBndMidd[],-sphSurf_List[]};
surfRegRight_ls=newsl; Surface Loop(surfRegRight_ls)={-vertSurf_List[2],vertSurf_List[3],surfBndRight[]};

volRegLeft=newv;  Volume(volRegLeft)={surfRegLeft_ls};
volRegMidd=newv;  Volume(volRegMidd)={surfRegMidd_ls};
volRegRight=newv; Volume(volRegRight)={surfRegRight_ls};

Physical Surface("Bnd_Left")     = {vertSurf_List[0]};
Physical Surface("Bnd_Right")    = {vertSurf_List[3]};

Physical Surface("Bnd_LatLeft")  = {surfBndLeft[]};
Physical Surface("Bnd_LatMidd")  = {surfBndMidd[]};
Physical Surface("Bnd_LatRight") = {surfBndRight[]};

Physical Surface("Interf_Left")  = {vertSurf_List[1]};
Physical Surface("Interf_Right") = {vertSurf_List[2]};
Physical Surface("Interf_Inc")   = {sphSurf_List[]};

Physical Volume("Vol_Left")  = {volRegLeft};
Physical Volume("Vol_Midd")  = {volRegMidd};
Physical Volume("Vol_Right") = {volRegRight};
Physical Volume("Vol_Inc")   = {sphVol_List[]};

