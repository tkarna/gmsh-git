from gmshpy import *
from dgpy import *
import os
import time
import math


print'---- Load Mesh & Build Groups'

model = GModel()
model.load('Cuboid.msh')

dimension = 3
order = 1
groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()


print'---- Load Conservation law with parameters and source term'

law = dgConservationLawMaxwell()

mu      = functionConstant([1,1,1])
epsilon = functionConstant([1,1,1])
law.setMu(mu)
law.setEpsilon(epsilon)

#incidentFields = functionConstant([0,0,0,0,0,0])
#sourceFields   = functionConstant([0,0,0,0,0,0])
#law.setIncidentField(incidentFields)
#law.setSourceField(sourceFields)


print'---- Load Initial conditions'

initialSolution = functionConstant([0,0,0,0,0,0])
solution = dgDofContainer(groups, law.getNbFields())
solution.L2Projection(initialSolution)
solution.setFieldName(0,'E_x')
solution.setFieldName(1,'E_y')
solution.setFieldName(2,'E_z')
solution.setFieldName(3,'H_x')
solution.setFieldName(4,'H_y')
solution.setFieldName(5,'H_z')
solution.exportMsh("output/Cuboid-00000", 0, 0)


print'---- Load Boundary conditions'

t=0
freq=1
def outletValueFunc(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    #value = 10*math.exp(-(100*t-1)*(100*t-1))*math.cos(2*math.pi*freq*(t-z))
    value = math.exp( - math.pow(t-0.6,2) / (2*math.pow(0.2,2)) )
    val.set(i,0,0)
    val.set(i,1,value)
    val.set(i,2,0)
outletValue = functionPython(3, outletValueFunc, [xyz])

#law.addBoundaryCondition('inlet',   law.newOutsideValueBoundary('inlet', outletValue))
law.addBoundaryCondition('inlet',   law.newBoundaryDirichletOnE('inlet', outletValue))
law.addBoundaryCondition('wall',    law.newBoundaryElectricWall('wall'))
law.addBoundaryCondition('symmetry',law.newBoundaryMagneticWall('symmetry'))
law.addBoundaryCondition('outlet',  law.newBoundaryElectricWall('outlet'))


print'---- LOOP'

solver = dgERK(law, None, DG_ERK_44)

dt = 0.0005
numoutput = 1
for i in range(0,7500):
  t = dt*i
  x = time.clock()
  solver.iterate(solution, dt, t)
  norm = solution.norm()
  if (i % 100 == 0):
    print'|ITER|',i,'/ 7500 |NORM|',norm, '|DT|',dt, '|T|',t, '|CPU|',time.clock()-x
    solution.exportMsh("output/Cuboid-%05i" % numoutput, t, numoutput)
    numoutput = numoutput+1


Msg.Exit(0);
