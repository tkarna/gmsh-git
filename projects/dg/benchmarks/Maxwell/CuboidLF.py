from gmshpy import *
from dgpy import *
import os
import time
import math


print'---- Load Mesh & Build Groups'

model = GModel()
model.load('Cuboid.msh')

dimension = 3
order = 1
groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()


print'---- Load Conservation law with parameters and source term'

mu      = functionConstant([1,1,1])
epsilon = functionConstant([1,1,1])

lawE = dgConservationLawMaxwellExplicit('E')
lawH = dgConservationLawMaxwellExplicit('H')
lawE.setMu(mu)
lawH.setMu(mu)
lawE.setEpsilon(epsilon)
lawH.setEpsilon(epsilon)


print'---- Load Initial conditions'

initSolE = functionConstant([0,0,0])
solE = dgDofContainer(groups, lawE.getNbFields())
solE.L2Projection(initSolE)
solE.setFieldName(0,'E_x')
solE.setFieldName(1,'E_y')
solE.setFieldName(2,'E_z')
solE.exportMsh("output/CuboidE_00000", 0, 0)

initSolH = functionConstant([0,0,0])
solH = dgDofContainer(groups, lawH.getNbFields())
solH.L2Projection(initSolH)
solH.setFieldName(0,'H_x')
solH.setFieldName(1,'H_y')
solH.setFieldName(2,'H_z')
solH.exportMsh("output/CuboidH_00000", 0, 0)

solHprev = dgDofContainer(groups, lawH.getNbFields())

lawE.setH(solH.getFunction())
lawH.setE(solE.getFunction())


print'---- Load Boundary conditions'

t=0
freq=1
def outletValueFunc(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    value = math.exp( - math.pow(t-0.6,2) / (2*math.pow(0.2,2)) )
    #value = math.sin(2*math.pi*t)
    val.set(i,0,0)
    val.set(i,1,value)
    val.set(i,2,0)
outletValue = functionPython(3, outletValueFunc, [xyz])

#law.addBoundaryCondition('inlet',   law.newOutsideValueBoundary('inlet', outletValue))
lawE.addBoundaryCondition('inlet',   lawE.newBoundaryDirichletOnE('inlet', outletValue))
lawE.addBoundaryCondition('wall',    lawE.newBoundaryElectricWall('wall'))
lawE.addBoundaryCondition('symmetry',lawE.newBoundaryMagneticWall('symmetry'))
lawE.addBoundaryCondition('outlet',  lawE.newBoundaryElectricWall('outlet'))

lawH.addBoundaryCondition('inlet',   lawH.newBoundaryDirichletOnE('inlet', outletValue))
lawH.addBoundaryCondition('wall',    lawH.newBoundaryElectricWall('wall'))
lawH.addBoundaryCondition('symmetry',lawH.newBoundaryMagneticWall('symmetry'))
lawH.addBoundaryCondition('outlet',  lawH.newBoundaryElectricWall('outlet'))


print'---- LOOP'

solverE = dgERK(lawE, None, DG_ERK_EULER)
solverH = dgERK(lawH, None, DG_ERK_EULER)

dt = 0.0001
numoutput = 1
for i in range(0,10000+1):
  t = dt*i
  x = time.clock()
  solverE.iterate(solE, dt, t)
  solHprev.copy(solH);
  solverH.iterate(solH, dt, t+dt/2)
  normE = solE.norm()
  normH = solH.norm()
  if (i % 250 == 0):
    EErg = lawE.getElectricEnergy(groups,solE,'volume')
    MErg = lawH.getMagneticEnergy(groups,solH,solHprev,'volume')
    TErg = EErg+MErg
    print'|ITER|',i,'/ 10000 |EErg|',EErg, '|MErg|',MErg, '|TErg|',TErg, '|DT|',dt, '|T|',t, '|CPU|',time.clock()-x
    solE.exportMsh("output/CuboidE_%05i" % numoutput, t, numoutput)
    solH.exportMsh("output/CuboidH_%05i" % numoutput, t, numoutput)
    numoutput = numoutput+1


Msg.Exit(0);
