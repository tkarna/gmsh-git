from gmshpy import *
from dgpy import *
import os
import time
import math


print'---- Load Mesh & Build Groups'

model = GModel()
model.load('ThinPlateReg.msh')

dimension = 3
order = 1
groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()


print'---- Load Conservation law with coefficients and source term'

mu      = functionConstant([1,1,1])
epsilon = functionConstant([1,1,1])
sigma   = functionConstantByTag({"Vol_Left"  : [0.],
                                 "Vol_Midd"  : [math.pow(10,-20)],
                                 "Vol_Inc"   : [math.pow(10,3)],
                                 "Vol_Right" : [0.]})

law = dgConservationLawMaxwell('PmlStraight')
law.setMu(mu)
law.setEpsilon(epsilon)
law.setSigma(sigma)


print'---- Load Initial condition'

initialSolution = functionConstant([0,0,0, 0,0,0, 0,0,0, 0])
solution = dgDofContainer(groups, law.getNbFields())
solution.L2Projection(initialSolution)
solution.setFieldName(0,'e_x')
solution.setFieldName(1,'e_y')
solution.setFieldName(2,'e_z')
solution.setFieldName(3,'h_x')
solution.setFieldName(4,'h_y')
solution.setFieldName(5,'h_z')
solution.setFieldName(6,'ePml_1')
solution.setFieldName(7,'ePml_2')
solution.setFieldName(8,'hPml_1')
solution.setFieldName(9,'hPml_2')
solution.exportMsh("output/ThinPlateReg_00000", 0, 0)


print'---- Build Pml'

CCode = """
#include "fullMatrix.h"
#include "function.h"
#include "math.h"

extern "C" {

double muValue = 1.;
double epsilonValue = 1.;
double cValue = 1./pow(muValue*epsilonValue,0.5);

double Lx_left=0.0001;
double Lx=0.006;
double Lx_right=0.0001;

void pmlCoefDef (dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (size_t i = 0; i<sol.size1(); i++) {
    double x = xyz(i,0);
    double rloc = 0;
    double sigmaPml = 0.;
    double lpml = 0.;
    if (x<0) {
      rloc = -x;
      lpml = Lx_left;
//      sigmaPml = cValue/(lpml*1.01) * (rloc)/(lpml*1.01-rloc);
    } else if (x>Lx) {
      rloc = x-Lx;
      lpml = Lx_right;
      sigmaPml = cValue/(lpml*1.01) * (rloc)/(lpml*1.01-rloc);
    }
    sol.set(i,0,sigmaPml);
  }
}

}
"""
tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0) :
  functionC.buildLibrary (CCode, tmpLib);

pmlCoef = functionC(tmpLib,"pmlCoefDef",1,[xyz])
pmlDir = functionConstantByTag({#"Vol_Left"  : [-1.,0.,0., 0.,-1.,0., 0.,0.,-1.],
                                "Vol_Left"  : [ 0.,0.,0., 0., 0.,0., 0.,0., 0.],
                                "Vol_Midd"  : [ 0.,0.,0., 0., 0.,0., 0.,0., 0.],
                                "Vol_Inc"   : [ 0.,0.,0., 0., 0.,0., 0.,0., 0.],
                                "Vol_Right" : [ 1.,0.,0., 0., 1.,0., 0.,0., 1.]})

law.setPmlCoef(pmlCoef)
law.setPmlDir(pmlDir)

#law.addPml('Vol_Left')
law.addPml('Vol_Right')
law.addPml('Interf_Right','Riemann')

t=0
freq=math.pow(10,5)
def extFieldFunction(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    value = math.cos(2*math.pi*freq*t)
    val.set(i,0,0)
    val.set(i,1,value)
    val.set(i,2,0)
    val.set(i,3,0)
    val.set(i,4,0)
    val.set(i,5,value)
extField = functionPython(6, extFieldFunction, [xyz])

#law.addInterfacePmlWithIncident('Interf_Left',extField)


print'---- Load Boundary conditions'

def extFieldBCFunction(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    value = math.cos(2*math.pi*freq*t)
    val.set(i,0,0)
    val.set(i,1,value)
    val.set(i,2,0)
extFieldBC = functionPython(3, extFieldBCFunction, [xyz])

law.addBoundaryCondition('Bnd_Left',  law.newBoundaryDirichletOnE('Bnd_Left',extFieldBC))

#law.addBoundaryCondition('Bnd_Left',  law.newBoundaryHomogeneousDirichletOnE('Bnd_Left'))
law.addBoundaryCondition('Bnd_Right', law.newBoundaryHomogeneousDirichletOnE('Bnd_Right'))
law.addBoundaryCondition('Bnd_LatY',  law.newBoundaryHomogeneousDirichletOnE('Bnd_LatY'))
law.addBoundaryCondition('Bnd_LatZ',  law.newBoundaryHomogeneousDirichletOnH('Bnd_LatZ'))


print'---- LOOP'

numoutput = 1

sys = linearSystemPETScBlockDouble()
sys.setParameter("petscOptions", "-pc_type ilu")
dof = dgDofManager.newDGBlock(groups, law.getNbFields(), sys)
solver = dgDIRK(law, dof, 2)

t0 = 0
dt = 0.000005
for i in range(0,2000):
  t = t0 + dt*i
  x = time.clock()
  solver.iterate(solution, dt, t)
  norm = solution.norm()
  print'|ITER1|',i,'/ 2000 |NORM|',norm,'|DT|',dt, '|T|',t, '|CPU|',time.clock() - x
  if (i % 10 == 0):
    solution.exportMsh("output/ThinPlateReg_%05i" % numoutput, t, numoutput)
    numoutput = numoutput+1


Msg.Exit(0);


