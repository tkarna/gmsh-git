from gmshpy import *
from dgpy import *
import os
import time
import math



TAG = 1

IMP = 1
DEC = 0.

ldom = 10
lpml = 10
lc = 0.5


### Load Mesh & Build Groups

model = GModel()
model.load('CuboidPml.msh')

dimension = 3
order = 1
groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()


### Load Conservation Law

law = dgConservationLawMaxwell('PmlStraightOld')
law.setFluxDecentering(DEC)

mu      = functionConstant([1,1,1])
epsilon = functionConstant([1,1,1])

law.setMu(mu)
law.setEpsilon(epsilon)

CCode = """
#include "fullMatrix.h"
#include "function.h"
#include "math.h"

extern "C" {

double cValue = 1;
double ldom = 10;
double lpml = 10.;

void pmlCoefDef (dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (size_t i = 0; i<sol.size1(); i++) {
    double rloc = xyz(i,2) - ldom/2.;
    double sigmaPml = 0.;
    double kPml = 0.;
    if (rloc>0)
      sigmaPml = (cValue/lpml) * rloc/(lpml-rloc);  // 0.1
    sol.set(i,0,sigmaPml);
    sol.set(i,1,kPml);
  }
}

void incFieldsDef (dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz, fullMatrix<double> &t) {
  for (size_t i = 0; i<sol.size1(); i++) {
    double zloc = xyz(i,2)+5.;
    double freq = 0.025;
    double value = sin(2*M_PI*freq*(t(0,0)-zloc));
    sol.set(i,0,value);
    sol.set(i,1,0.);
    sol.set(i,2,0.);
    sol.set(i,3,0);
    sol.set(i,4,value);
    sol.set(i,5,0.);
  }
}

}
"""
tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0):
  functionC.buildLibrary (CCode, tmpLib);
Msg.Barrier()
pmlCoefFunc = functionC(tmpLib,"pmlCoefDef",2,[xyz])
#pmlDirFunc = functionConstantByTag({"Domain" : [0.,0.,0., 0.,0.,0., 0.,0.,0.],
pmlDirFunc = functionConstantByTag({"Domain" : [0.,0.,1., 1.,0.,0., 0.,1.,0.],
                                    "Pml"    : [0.,0.,1., 1.,0.,0., 0.,1.,0.]})

law.setPmlCoef(pmlCoefFunc)
law.setPmlDir(pmlDirFunc)

if (TAG==1):
  law.addPml('Pml','Friedrich')
if (TAG==2):
  law.addPml('Pml','Friedrich2')
if (TAG==3):
  law.addPml('Pml','Centered')

#law.addPml('Domain','Friedrich')
#law.addPml('Interface','Friedrich')
law.addInterfacePml('Interface')

tFunc = functionConstant([0.])
incFieldsFunc = functionC(tmpLib,"incFieldsDef",6,[xyz,tFunc])

law.addBoundaryCondition('BorderX',      law.newBoundaryElectricWall('BorderX'))
law.addBoundaryCondition('BorderY',      law.newBoundaryMagneticWall('BorderY'))
law.addBoundaryCondition('BorderZLeft',  law.newBoundarySilverMullerDirichlet('BorderZLeft',incFieldsFunc))
law.addBoundaryCondition('BorderZRight', law.newBoundaryElectricWall('BorderZRight'))


### Load Initial and Reference Solutions

initialSolution = functionConstant([0,0,0,0,0,0,0,0,0,0,0,0])
sol = dgDofContainer(groups, law.getNbFields())
sol.L2Projection(initialSolution)
sol.setFieldName(0,'ePml_x')
sol.setFieldName(1,'ePml_y')
sol.setFieldName(2,'ePml_z')
sol.setFieldName(3,'hPml_x')
sol.setFieldName(4,'hPml_y')
sol.setFieldName(5,'hPml_z')
sol.setFieldName(6,'ePml_n')
sol.setFieldName(7,'ePml_t1')
sol.setFieldName(8,'ePml_t2')
sol.setFieldName(9,'hPml_n')
sol.setFieldName(10,'hPml_t1')
sol.setFieldName(11,'hPml_t2')
sol.exportMsh("output/Cuboid_Pml_%i_%05i" % (TAG, 0), 0., 0)

solRef = dgDofContainer(groups, 6)
solRef.L2Projection(incFieldsFunc)
solRef.setFieldName(0,'eRef_x')
solRef.setFieldName(1,'eRef_y')
solRef.setFieldName(2,'eRef_z')
solRef.setFieldName(3,'hRef_x')
solRef.setFieldName(4,'hRef_y')
solRef.setFieldName(5,'hRef_z')
solRef.exportMsh("output/Cuboid_Ref_00000", 0, 0)

pmlCoefDof = dgDofContainer(groups, 2)
pmlCoefDof.setFieldName(0,'sigma')
pmlCoefDof.setFieldName(1,'kappa')
pmlCoefDof.L2Projection(pmlCoefFunc)
pmlCoefDof.exportMsh("output/CoefPml")

pmlDirDof = dgDofContainer(groups, 9)
pmlDirDof.L2Projection(pmlDirFunc)
pmlDirDof.exportMsh("output/DirPml")


### LOOP

sys = linearSystemPETScBlockDouble()
dof = dgDofManager.newDGBlock(groups, law.getNbFields(), sys)

c = 1.
tf = 750

if (IMP):
  solver = dgCrankNicholson(sol, law, dof, 0.)
  solver.getNewton().setVerb(10)
  solver.setComputeExplicitTerm(True)
  CFL = 0.25
  dt = lc/c * CFL
else:
  solver = dgERK(law, dof, DG_ERK_44)
  CFL = 0.04
  dt = lc/c * CFL
  ##dt = 0.02

iMax = int(tf/dt)
numExport = int(iMax/250)

numOutput = 1
timeInit = time.clock()
for i in range(1,iMax):
  Msg.Barrier()
  t = dt*i
  tFunc.set(t)
  if (IMP):
    solver.iterate(t)
  else:
    solver.iterate(sol, dt, t)
  Msg.Barrier()
  if (i % 10 == 0):
    erg = law.getTotalEnergy(groups, sol)
    norm = sol.norm()
    if (Msg.GetCommRank() == 0):
      fileOut = open("output/Cuboid_Data_%i.out" % (TAG), "a")
      fileOut.write("%.8e %.8e %.8e\n" % (t, erg, norm) )
      fileOut.close()
      timeCurrent = time.clock()
      timeMeanByIter = (timeCurrent-timeInit)/(i*60.)
      timeExp = timeMeanByIter*(iMax-i)
      print TAG, '|IMP|', IMP, '|DEC|', DEC, '|ITER|', i, '/', iMax, '|dt|', dt, '|NORM|', norm, '|ERG|', erg, '|TimeExp|', timeExp
  
  if (i % numExport == 0):
    sol.exportMsh("output/Cuboid_Pml_%i_%05i" % (TAG, numOutput), t, numOutput)
    solRef.L2Projection(incFieldsFunc)
    solRef.exportMsh("output/Cuboid_Ref_%05i" % numOutput, t, numOutput)
    numOutput = numOutput+1

Msg.Exit(0);

