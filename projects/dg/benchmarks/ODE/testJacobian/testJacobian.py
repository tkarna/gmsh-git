from dgpy import *
from math import *
import time, os, sys
import gmshPartition
from matplotlib import pyplot as plt

#### Begin and end time ###
Ti=0
Tf=0.55139
nbSteps = 10000
###########################

order=0 #Spatial order!!
dimension=2

model = GModel()
model.load('square.msh')


groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag();

e = 1e-3
def termFull(term, Y) :
  for i in range (0,Y.size1()) :
    y1 = Y(i, 0)
    y2 = Y(i, 1)
    term.set(i, 0, 3*y1-2*y2)    #J=[3 -2]
    term.set(i, 1, y1*y1*y2*y2)  #J=[2*y1*y2*y2    2*y1*y1*y2]

def termIm(term, Y) :
  term.setAll(0)
  
nbFields = 2
solution = dgDofContainer(groups, nbFields)

termFull=functionPython(nbFields, termFull, [function.getSolution()])
termIm=functionPython(nbFields, termIm, [function.getSolution()])
claw = dgConservationLawODE(termFull, termIm)

boundary = claw.new0FluxBoundary()
claw.addBoundaryCondition("bottom", boundary)
claw.addBoundaryCondition("top", boundary)
claw.addBoundaryCondition("left", boundary)
claw.addBoundaryCondition("right", boundary)

def initialCondition(val) :
  for i in range(0,val.size1()):
    val.set(i,0,3);
    val.set(i,1,10);
initF=functionPython(nbFields, initialCondition, [])
solution.interpolate(initF)



jacOps = dgJacobianOperators(claw, groups);
jacOps.computeTermAndJacobian(solution, 0, True);

print("Jacobian")
jacOps.printJacobian();


F = jacOps.getTerm()
#Integrate over a domain of size 1 => gives the value
Finteg = dgFunctionIntegrator(groups, F.getFunction())
FMat = fullMatrixDouble(2,1)
Finteg.compute(FMat)
FMat._print("F")

JF = dgDofContainer(groups, nbFields)
jacOps.jacobianMultiply(F, JF);
JFinteg = dgFunctionIntegrator(groups, JF.getFunction())
JFMat = fullMatrixDouble(2,1)
JFinteg.compute(JFMat)
JFMat._print("JF")

