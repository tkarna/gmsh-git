from dgpy import *
from math import *
import time, os, sys
import gmshPartition
from matplotlib import pyplot as plt
import numpy

#### Begin and end time ###
Ti=0.
Tf=0.3
###########################

order=0 #Spatial order!!
dimension=2

model = GModel()
model.load('square.msh')


groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag();

Forcing = 8.0

def lorenz96(term, Y) :
	N = Y.size2()
	for i in range (0, Y.size1()) :
		term.set(i, 0, Y(i, N-1)*(Y(i, 1) - Y(i, N-2)) - Y(i, 0) + Forcing)
		term.set(i, 1, Y(i, 0)*(Y(i, 2) - Y(i, N-1)) - Y(i, 1) + Forcing)
		for j in range (2, N-1) :
			term.set(i, j, Y(i, j-1)*(Y(i, j+1) - Y(i, j-2)) - Y(i, j) + Forcing)
		term.set(i, N-1, Y(i, N-2)*(Y(i, 0) - Y(i, N-3)) - Y(i, N-1) + Forcing)

nbFields = 10
solution = dgDofContainer(groups, nbFields)

solODE = dgFunctionIntegrator(groups, solution.getFunction())
solMat = fullMatrixDouble(nbFields, 1)

termFull=functionPython(nbFields, lorenz96, [function.getSolution()])
termIm=functionPython(nbFields, None, [function.getSolution()])
claw = dgConservationLawODE(termFull, termIm)

boundary = claw.new0FluxBoundary()
claw.addBoundaryCondition("bottom", boundary)
claw.addBoundaryCondition("top", boundary)
claw.addBoundaryCondition("left", boundary)
claw.addBoundaryCondition("right", boundary)

def initialCondition(val) :
	N = val.size2()
	for i in range (0,val.size1()) :
		for j in range (0, N) :
			val.set(i, j, 4.0*j/(N-1) - 2.0)

initF=functionPython(nbFields, initialCondition, [])
solution.interpolate(initF)
solution0=dgDofContainer(groups, nbFields)
solution0.copy(solution)

nbSteps = 800

for iruns in range(0,1) :
	solution.copy(solution0)
	petscIm = linearSystemPETScBlockDouble()
	petscIm.setParameter("petscOptions",  "-ksp_atol 1.e-14 -ksp_rtol 1.e-14")

	dof = dgDofManager.newDGBlock(groups, claw.getNbFields(), petscIm)
	
#	timeIter = dgERK_test(claw, groups)
	timeIter = dgROK(claw, dof, DG_ROK_4A, 4)
#	timeIter = dgERK(claw, dof, DG_ERK_44)
#	timeIter = dgDIRK(claw, dof, 3)
###	#timeIter = dgERK(claw, None, DG_ERK_65_FEHLBERG)     #timeorder

	
	dt = (Tf-Ti)/nbSteps
	if (Msg.GetCommRank()==0):
		print "Time step:",dt

	t=Ti

	n_export=0

	stepsizes =[]
	y=[]
	solODE.compute(solMat)
	for i in range(0, nbFields) :
		y.append([solMat(i, 0)])
	time=[0.0]
	#for i in range(2,nbSteps) :
	for i in range(0,nbSteps) :
		#if (i==nbSteps-1):
		#    dt=Tf-t
		norm = timeIter.iterate (solution, dt, t)
		solODE.compute(solMat)
		for j in range(0, nbFields) :
			y[j].append(solMat(j, 0))
		t=t+dt
		time.append(t)
		if (Msg.GetCommRank()==0):
			sys.stdout.write('.')
			sys.stdout.flush()

	print ''
	
	for i in range(0, nbFields) :
		plt.plot(time, y[i], label='y'+str(i))
	#plt.legend(loc='lower left')
                print y[i]
#	plt.show()
	
	outfile = open('output-'+str(dt)+'.txt', 'w')
	for i in range(0, nbFields) :
		for j in range(0, nbSteps+1) :
			outfile.write(str(y[i][j])+' ')
		outfile.write('\n')
	outfile.write('\n')
	for i in range(0, nbSteps+1) :
		outfile.write(str(time[i])+'\n')
	outfile.close()
	
	nbSteps = nbSteps*2
	stepsizes.append(dt)
	
	
	#pltConvergence.set_data(stepsizes,err)
	#axes.relim()
	#axes.autoscale_view(True,True,True)
	#plt.draw()
	#fig.savefig('ErrorVsStep.pdf')
	#plt.draw()
	#plt.show()
Msg.Exit(0)
