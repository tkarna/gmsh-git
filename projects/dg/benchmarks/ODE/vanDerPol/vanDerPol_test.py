from dgpy import *
from math import *
import time, os, sys
import gmshPartition
from matplotlib import pyplot as plt
import numpy
#### Begin and end time ###
Ti=0
Tf=0.55139
###########################

order=0 #Spatial order!!
dimension=2

model = GModel()
model.load('square.msh')


groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag();

e = 1.e0
yRef=[1.577355855295980,-0.820801862542605] #1e0
#yRef=[1.563373944229674,-1.000020831855205] #1e-1
#yRef=[1.544452026271384,-1.101574641264297] #1e-2
#yRef=[1.541914793094351, -1.117920408324843] #1e-3
#yRef=[1.541623536346934,-1.119860584764671] #1e-5
err=[]
stepsizes=[]
#if(Msg.GetCommRank()==0):
#    plt.ion()
#    fig = plt.figure()
#    pltConvergence,=plt.plot(stepsizes,err,label='convergence')
#    plt.legend(loc='upper right')
#    axes=plt.gca()
#    axes.set_xscale("log")
#    axes.set_yscale("log")
#    axes.set_title("error vs time step")


def vanDerPolFull(term, Y) :
  for i in range (0,Y.size1()) :
    y1 = Y(i, 0)
    y2 = Y(i, 1)
    term.set(i, 0, y2)
    term.set(i, 1, ((1 - y1*y1) * y2 - y1) / e)

def vanDerPolIm(term, Y) :
  for i in range (0,Y.size1()) :
    y1 = Y(i, 0)
    y2 = Y(i, 1)
    term.set(i, 0, 0)
    term.set(i, 1, ((1 - y1*y1) * y2 - y1) / e)

nbFields = 2
solution = dgDofContainer(groups, nbFields)

#Integrate over a domain of size 1 => gives the value
solODE = dgFunctionIntegrator(groups, solution.getFunction())
solMat = fullMatrixDouble(2,1)

termFull=functionPython(nbFields, vanDerPolFull, [function.getSolution()])
termIm=functionPython(nbFields, vanDerPolIm, [function.getSolution()])
claw = dgConservationLawODE(termFull, termIm)

boundary = claw.new0FluxBoundary()
claw.addBoundaryCondition("bottom", boundary)
claw.addBoundaryCondition("top", boundary)
claw.addBoundaryCondition("left", boundary)
claw.addBoundaryCondition("right", boundary)

def initialCondition(val) :
  for i in range (0,val.size1()) :
    val.set(i, 0, 2)
    val.set(i, 1,  -2./3. + ((10./81.) * e) - ((292./2187.) * e*e) - ((1814./19683.) * e**3) )
initF=functionPython(nbFields, initialCondition, [])
solution.interpolate(initF)
solution0=dgDofContainer(groups, nbFields)
solution0.copy(solution)

nbSteps = 20

for iruns in range(0,5) :

    solution.copy(solution0)
    petscIm = linearSystemPETScBlockDouble()
    petscIm.setParameter("petscOptions",  "-ksp_atol 1.e-14 -ksp_rtol 1.e-14")

    dofIm = dgDofManager.newDGBlock(groups, claw.getNbFields(), petscIm)
    
    #timeIter = dgIMEXTSRK(claw, dofIm, 333)     #timeorder
    timeIter = dgIMEXRK(claw, dofIm, 4)     #timeorder
    #timeIter = dgIMEXLMM(claw, dofIm, IMEX_BDF3)
    #timeIter = dgIMEXDIMSIM(claw,dofIm,IMEX_DIMSIM_5a)
    #timeIter.getNewton().setAtol(1.e-13)
    #timeIter.getNewton().setRtol(1.e-13)
    #timeIter.getNewton().setMaxIt(100)
    #timeIter.getNewton().setVerb(0)     #Verbosity

    
    dt = (Tf-Ti)/nbSteps
    if (Msg.GetCommRank()==0):
        print "Time step:",dt

    t=Ti

    #print "Before starter"
    #solODE.compute(solMat)
    #print "y1",solMat(0,0)
    #print "y2",solMat(1,0)

    # starter for IMEX TSRK
    #t=timeIter.starter(solution,dt,t,3)

    # starter for IMEX LMM
    #t=timeIter.starter(solution,dt,t,3)

    # starter for IMEX DIMSIM
    # timeIter.starter(solution,dt,dt,t,3)

    #print "After starter"
    #solODE.compute(solMat)
    #print "y1",solMat(0,0)
    #print "y2",solMat(1,0)

    n_export=0


    y1=[]
    y2=[]
    time=[]
    #IMEX LMM
    #for i in range(2,nbSteps) :
    for i in range(0,nbSteps) :
        #if (i==nbSteps-1):
        #    dt=Tf-t
        norm = timeIter.iterate (solution, dt, t)
        solODE.compute(solMat)
        y1.append(solMat(0,0))
        y2.append(solMat(1,0))
        time.append(t)
        t=t+dt
        #if (Msg.GetCommRank()==0):
        #    sys.stdout.write('.')
        #    sys.stdout.flush()
    #plt.plot(time,y1,label="y1")
    #plt.plot(time,y2,label="y2")
    #plt.legend(loc='lower left')
    #plt.show()
    nbSteps = nbSteps*2
    stepsizes.append(dt)
    err.append( sqrt((y1[-1]-yRef[0])**2/yRef[0]**2 + (y2[-1]-yRef[1])**2/yRef[1]**2)  )
    if(Msg.GetCommRank()==0):
        print ''    
	print "y1:",y1[-1],"y2:",y2[-1],"error",err[-1],'at time ',t
	print "y1:%20.15f y2:%20.15f"% (y1[-1],y2[-1])
    #pltConvergence.set_data(stepsizes,err)
    #axes.relim()
    #axes.autoscale_view(True,True,True)
    #plt.draw()
    #fig.savefig('ErrorVsStep.pdf')
    #plt.draw()
    #plt.show()
Msg.Exit(0)
