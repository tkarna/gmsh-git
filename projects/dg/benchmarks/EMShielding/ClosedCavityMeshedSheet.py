from gmshpy import *
from dgpy import *
import os
import time
import math

print'---- Load Mesh and Build Groups'

model = GModel()
model.load('ClosedCavityMeshedSheet.msh')

dim = 2
order = 1
groups = dgGroupCollection(model, dim, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()




mu0=4*math.pi*1e-7
eps0=8.85*1e-12
cValue0=1/math.sqrt(eps0*mu0)
cValue=cValue0
rhoValue = mu0

cValue_mat=1/math.sqrt(eps0*mu0)
rhoValue_mat = mu0

sigmaValue = 100./eps0


coef = functionConstantByTag({"EXT"      :[cValue,       rhoValue,      0.,         0.],
                              "CAV"      :[cValue_mat,       rhoValue_mat,      sigmaValue,         0.],
                              "INT"      :[cValue,       rhoValue,      0.,         0.]})




print'---- Build Initial conditions'

initialConditionPython = functionConstant([0.,0.,0.,0.])


solution = dgDofContainer(groups,3)
solution.setFieldName(0,'Ez_pml')
solution.setFieldName(1,'-Hy_pml')
solution.setFieldName(2,'Hx_pml')
solution.L2Projection(initialConditionPython)
solution.exportMsh("output/ClosedCavityMeshedSheet_%05d" % (0), 0, 0)


print'---- Load Conservation law with coefficients'

law = dgConservationLawWave(dim)
law.useDissipationOnP() 
law.setPhysCoef(coef)





print'---- Load Interface and Boundary conditions'

t=0
tho=5e-9
aa=math.log(100)*3e8*3e8/0.75/.75

def extFieldFunction(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    tt=t-tho
    value = math.exp(-aa*tt*tt)
    val.set(i,0,value)
    val.set(i,1,value/(cValue*rhoValue))
    val.set(i,2,0.)
extField = functionPython(3, extFieldFunction, [xyz])




law.addBoundaryCondition('Top',      law.newBoundaryHomogeneousDirichletOnU('Top'))
law.addBoundaryCondition('Bottom',      law.newBoundaryHomogeneousDirichletOnU('Bottom'))
law.addBoundaryCondition('Right',      law.newBoundaryOpenOutflow('Right'))
law.addBoundaryCondition('Left', law.newBoundaryOpenOutflowWithInflow('Left', extField))



incidField=fullMatrixDouble(1,1)


def psolShield(val,xyz, solution):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    s= solution.get(i,0)
    value  = s
    val.set(i,0,value)



print'---- LOOP'


tic = time.clock()

sys = linearSystemPETScBlockDouble()


sys.setParameter("petscOptions", "-pc_type ilu")


dof = dgDofManager.newDGBlock(groups, 3, sys)
solver = dgDIRK(law,dof,2)


dt = 0.2e-11
iterMax = 50000
print'---- LOOP'
for i in range(1,iterMax+1):
  t = t+dt
  print '|ITER|', i, 'cpu', time.clock() - tic
  solver.iterate(solution,dt,t)
  if (i % 100 == 0):
    solution.exportMsh("output/ClosedCavityMeshedSheet_%05d" % (i), t, i)
  psol = functionPython(1,psolShield,[xyz, solution.getFunction()])
  incidfield = dgFunctionEvaluator(groups, psol)
  incidfield.compute(0, 0.0,0.0, incidField)
  print("%.8e  %.8e \n"% (t,incidField(0,0)))
  fout_shield=open("ClosedCavityMeshedSheet.dat","a")
  fout_shield.write("%.8e  %.8e \n"% (t, incidField(0,0)))
  fout_shield.close()


Msg.Exit(0)



