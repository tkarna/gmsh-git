
mm = 0.5;
r  = 0.15;
m  = 3;
n  = 6;

Ltickness = (m)*mm;
Lheight   = (n)*mm;
Lpml  = Lheight/5;
Lext  = Lheight/2;

lc = 0.12; //r/3;
lc2 = 0.12;

For i In {0:m-1}
  x[i] = Ltickness * (i+0.5)/m - Ltickness/2.;
EndFor
For j In {0:n-1}
  y[j] = Lheight * (j+0.5)/n - Lheight/2.;
EndFor

icircle = 0;
Function circle
  center=newp; Point(center)={cx,cy,0};
  p1=newp; Point(p1)={cx-r,cy,0,lc};
  p2=newp; Point(p2)={cx,cy+r,0,lc};
  p3=newp; Point(p3)={cx+r,cy,0,lc};
  p4=newp; Point(p4)={cx,cy-r,0,lc};
  s1=newl; Circle(s1)={p1,center,p2};
  s2=newl; Circle(s2)={p2,center,p3};
  s3=newl; Circle(s3)={p3,center,p4};
  s4=newl; Circle(s4)={p4,center,p1};
  circlloop[icircle]=newll;
  Line Loop(circlloop[icircle])={s1,s2,s3,s4};
  Physical Line(circlloop[icircle]) = {s1,s2,s3,s4};
  circsloop[icircle]=news;
  Plane Surface(circsloop[icircle])={circlloop[icircle]};
  icircle+=1;
Return

For i In {0:m-1}
  For j In {0:n-1}
    cx = x[i];
    cy = y[j];
    Call circle;
  EndFor
EndFor

xboudary[0]=-Ltickness/2-Lpml;
xboudary[1]=-Ltickness/2;
xboudary[2]= Ltickness/2;
xboudary[3]= Ltickness/2+Lext;

p[0]=newp; Point(p[0])= {xboudary[0], Lheight/2, 0, lc2};
p[1]=newp; Point(p[1])= {xboudary[1], Lheight/2, 0, lc};
p[2]=newp; Point(p[2])= {xboudary[2], Lheight/2, 0, lc};
p[3]=newp; Point(p[3])= {xboudary[3], Lheight/2, 0, lc2};
p[4]=newp; Point(p[4])= {xboudary[3],-Lheight/2, 0, lc2};
p[5]=newp; Point(p[5])= {xboudary[2],-Lheight/2, 0, lc};
p[6]=newp; Point(p[6])= {xboudary[1],-Lheight/2, 0, lc};
p[7]=newp; Point(p[7])= {xboudary[0],-Lheight/2, 0, lc2};

For i In {0:7}
  s=newl; Line(s)={p[i], p[((i+1)%8)]};
  lboundary[i]=s;
EndFor

s=newl; Line(s)={p[1], p[6]};
linterface[0] = s;
s=newl; Line(s)={p[2], p[5]};
linterface[1] = s;

zone[0]=news; Line Loop(zone[0])={lboundary[0],linterface[0],lboundary[6],lboundary[7]};
Plane Surface(zone[0])={zone[0]};
zone[1]=news; Line Loop(zone[1])={lboundary[1],linterface[1],lboundary[5],-linterface[0]};
Plane Surface(zone[1])={zone[1],circlloop[]};
zone[2]=news; Line Loop(zone[2])={lboundary[2],lboundary[3],lboundary[4],-linterface[1]};
Plane Surface(zone[2])={zone[2]};

Physical Line("BOUNDARY0") = {lboundary[0]};
Physical Line("BOUNDARY1") = {lboundary[1]};
Physical Line("BOUNDARY2") = {lboundary[2]};
Physical Line("BOUNDARY3") = {lboundary[3]};
Physical Line("BOUNDARY4") = {lboundary[4]};
Physical Line("BOUNDARY5") = {lboundary[5]};
Physical Line("BOUNDARY6") = {lboundary[6]};
Physical Line("BOUNDARY7") = {lboundary[7]};

Physical Line("INTERFACE_LEFT")   = {linterface[0]};
Physical Line("INTERFACE_RIGHT")  = {linterface[1]};
//Physical Line("INTERFACE_FIBERS") = {circlloop[]};

Physical Surface ("PML_LEFT")  = {zone[0]};
Physical Surface ("MATRIX")    = {zone[1]};
Physical Surface ("PML_RIGHT") = {zone[2]};
Physical Surface ("FIBERS")    = {circsloop[]};

