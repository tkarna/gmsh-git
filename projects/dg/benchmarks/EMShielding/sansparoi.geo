cl1 = 0.25;					
Point(1) = {-0, 0, 0, cl1};
Point(2) = {-0, 4, 0, cl1};
Point(3) = {5, 4, 0, cl1};
Point(4) = {5, 0, 0, cl1};
Point(5) = {10.25, 0, 0, cl1};
Point(6) = {10.25, 4, 0, cl1};


Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line(5) = {3, 6};
Line(6) = {6, 5};
Line(7) = {5, 4};

Line Loop(15) = {4, 1, 2, 3};
Plane Surface(105) = {15};

Line Loop(16) = {7, 6, 5, -3};
Plane Surface(106) = {16};

Physical Line("Left") = {1};
Physical Line("Right") = {6};
Physical Line("Top") = {2, 5};
Physical Line("Bottom") = {4, 7};
Physical Line("InterfaceR") = {3};


Physical Surface("EXT1")={105};
Physical Surface("EXT2")={106};

