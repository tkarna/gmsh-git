from gmshpy import *
from dgpy import *
import os
import time
import math


mm    = 1
n     = 5
L     = n*mm
Lcut  = (n)*mm
lsca1 = 4*mm
Lext  = L
Lpml  = L/2

freq=1/4


print'Mesh, Geometry & Groups'

model = GModel()
model.load('FibersShielding.msh')

dim = 2
order = 1
groups = dgGroupCollection(model, dim, order)
groups.splitGroupsByPhysicalTag()
groups.buildGroupsOfInterfaces()
xyz = groups.getFunctionCoordinates()


print'Conservation law'

cValue        = 340.
rhoValue      = 1.293
sigmaValue    = -1.
cValue_incl   = 120
rhoValue_incl = 10*1.293
cValue_mat    = 500
rhoValue_mat  = 2*1.293

coef = functionConstantByTag({"PML"   :[cValue,      rhoValue,      sigmaValue],
                              "EXT1"  :[cValue,      rhoValue,      sigmaValue],
                              "EXT2"  :[cValue,      rhoValue,      sigmaValue],
                              "FIBERS":[cValue_incl, rhoValue_incl, sigmaValue],
                              "MATRIX":[cValue_mat,  rhoValue_mat,  sigmaValue]})

PmlVersion = 1
law = dgConservationLawWave(2,PmlVersion)
law.useDG()
law.setPhysCoef(coef)
law.useDissipationOnP()

print'Initial solutions'

def initialCondition(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    x0=-0.5
    y0=1.45
    r=0.5
    value = math.exp(-5*((x-x0)*(x-x0)+(y-y0)*(y-y0))/(r*r))
    val.set(i,0,value)
    val.set(i,1,0.)#-(1/(rhoValue*cValue))*value)
    val.set(i,2,0.)
    val.set(i,3,0.)
    val.set(i,4,0.)
initialConditionPython = functionPython(5,initialCondition,[xyz])

sol = dgDofContainer(groups,5)
sol.setFieldName(0,'p')
sol.setFieldName(1,'u')
sol.setFieldName(2,'v')
sol.setFieldName(3,'qp')
sol.setFieldName(4,'qu')
sol.L2Projection(initialConditionPython)
sol.exportMsh('output/FibersShielding_00000',0,0)


print'Build PML'

lpml = Lpml
pospml = L/2-Lcut/2-Lext+Lpml

def absCoefDef(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    xloc = math.fabs(x)-math.fabs(pospml)
    sigmaPml = cValue/(lpml*1.01) * (xloc)/((lpml*1.01)-xloc)
    val.set(i,0,sigmaPml)
absCoef = functionPython(1,absCoefDef,[xyz])
absDir = functionConstant([-1.,0.,0.])

law.setAbsCoef(absCoef)
law.setAbsDir(absDir)

law.addPml('PML')
law.addInterfacePml('INTERFACE0')

absCoefSol = dgDofContainer(groups,1)
absCoefSol.interpolate(absCoef)
absCoefSol.exportMsh('output/FibersShielding_AbsCoef',0,0)


print'Boundary conditions'

#def leftBoundaryCondition(val,xyz):
#  for i in range(0,xyz.size1()):
#    x = xyz.get(i,0)
#    y = xyz.get(i,1)
#    value = 1+0*math.cos(2*math.pi*freq*t)
#    val.set(i,0,value)
#    val.set(i,1,(cValue*rhoValue)*value)
#    val.set(i,2,0)
#    val.set(i,3,0.)
#    val.set(i,4,0.)
#leftBoundaryConditionPython = functionPython(6,leftBoundaryCondition,[xyz])

law.addBoundaryCondition('BOUNDARY0', law.newBoundaryWall('BOUNDARY0'))
law.addBoundaryCondition('BOUNDARY1', law.newBoundaryWall('BOUNDARY1'))
law.addBoundaryCondition('BOUNDARY2', law.newBoundaryWall('BOUNDARY2'))
law.addBoundaryCondition('BOUNDARY3', law.newBoundaryWall('BOUNDARY3'))
law.addBoundaryCondition('BOUNDARY5', law.newBoundaryWall('BOUNDARY5'))
law.addBoundaryCondition('BOUNDARY6', law.newBoundaryWall('BOUNDARY6'))
law.addBoundaryCondition('BOUNDARY7', law.newBoundaryWall('BOUNDARY7'))
law.addBoundaryCondition('BOUNDARY8', law.newBoundaryWall('BOUNDARY8'))
law.addBoundaryCondition('BOUNDARY9', law.newBoundaryWall('BOUNDARY9'))
law.addBoundaryCondition('BOUNDARY4', law.newBoundaryWall('BOUNDARY4'))

#law.addBoundaryCondition('BOUNDARY4', law.newOutsideValueBoundary('BOUNDARY4', leftBoundaryConditionPython))
#law.addBoundaryCondition('BOUNDARY4', law.newOutsideValueBoundaryGeneric('BOUNDARY4', leftBoundaryConditionPython,leftBoundaryConditionPython))


print'Loop'

implicit = True;

if (implicit):
  dt = 0.001
  sys = linearSystemPETScBlockDouble()
  #sys.setParameter("petscOptions", "-pc_type lu")
  #sys.setParameter("petscOptions", "-ksp_type bicg")
  dof = dgDofManager.newDGBlock(groups, 5, sys)
  solver=dgDIRK(law,dof,2)
  #solver.getNewton().setMaxit(5000)
  #solver.getNewton().setAtol(1e-8)
  #solver.getNewton().setRtol(1e-6)
  #solver.getNewton().setVerb(10)
  for i in range(1,40):
    t = i*dt
    solver.iterate(sol,dt,t)
    print'|ITER|', i, '|DT|', dt, '|t|', t
    sol.exportMsh("output/FibersShielding_%05i" % (i), t, i)
else:
  dt = 0.000001
  nbExport = 1
  solver = dgERK(law, None, DG_ERK_44)
  for i in range(0,50000):
    t = i*dt
    solver.iterate(sol, dt, t);
    norm = sol.norm()
    if (math.isnan(norm)):
      print 'ERROR : NAN'
      break
    if (i % 100 == 0):
      print'|ITER|', i, '|NORM|', norm, '|DT|', dt, '|t|', t
      sol.exportMsh("output/FibersShielding_%05i" % (nbExport), t, nbExport)
      nbExport  = nbExport + 1
      

Msg.Exit(0)

