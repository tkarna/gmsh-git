from gmshpy import *
from dgpy import *
import os
import time
import math

a=4
b=0.25
c=1


print'Mesh, Geometry & Groups'

model = GModel()
model.load('sansparoi.msh')

dim = 2
order = 1
groups = dgGroupCollection(model, dim, order)
groups.splitGroupsByPhysicalTag()

xyz = groups.getFunctionCoordinates()


print'Conservation law'




mu0=4*math.pi*1e-7
eps0=8.85*1e-12
cValue0=1/math.sqrt(eps0*mu0)

cValue=cValue0

rhoValue = mu0
Ys = (100*1e-3);


coef = functionConstantByTag({"EXT1"      :[cValue,   rhoValue,  0,     0],
                              "EXT2"      :[cValue,   rhoValue,  0,     0],})
PmlVersion = 2
law = dgConservationLawWave(dim)

law.useInterfaceShieldOnP()
law.setPhysCoef(coef)
law.setFluxDecentering(1.)





print'Initial solutions'

def initialCondition(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    y1=3.85
    w=2*math.pi*1e8
    value=0
    if (x<=4.6 and x>=3.1):
       value = math.cos(w*(x-y1))
    val.set(i,0,value)
    val.set(i,1,value/(cValue*rhoValue))
    val.set(i,2,0.)

initialConditionPython1 = functionConstant([0.,0.,0.])

initialConditionPython = functionPython(3,initialCondition,[xyz])
sol = dgDofContainer(groups,3)
sol.setFieldName(0,'p')
sol.setFieldName(1,'u')
sol.setFieldName(2,'v')
sol.L2Projection(initialConditionPython)
sol.exportMsh('output/Paroi-00000',0,0)



print'Boundary conditions'
law.addInterfaceSheet('InterfaceR',Ys)
law.addBoundaryCondition('Left', law.newBoundaryOpenOutflow('Left'))
law.addBoundaryCondition('Top', law.newBoundaryWall('Top'))
law.addBoundaryCondition('Bottom', law.newBoundaryWall('Bottom'))
law.addBoundaryCondition('Right', law.newBoundaryOpenOutflow('Right'))



print'Loop'
implicit = True;
sys = linearSystemPETScBlockDouble()
dof = dgDofManager.newDGBlock(groups,3, sys)
tic = time.clock()
if (implicit):
  dt = 5e-12
  solver=dgDIRK(law,dof,2)
  for i in range(1,15000):
    t = i*dt
    solver.iterate(sol,dt,t)
    print'Loop'
    print'|ITER|', i, '|DT|', dt, '|t|', t,'cpu', time.clock() - tic,'norm', sol.norm()
    if (i % 1 == 0):
      sol.exportMsh("output/ParoiShielding-%05i" % (i), t, i)
 

else:
  dt = 0.1e-16
  nbExport = 1
  #t=0.
  rk = dgERK(law, None, DG_ERK_44)
  #CFL=0.01
  for i in range(0,200000):
    #dt = CFL * rk.computeInvSpectralRadius(sol)
    #t = t+dt
    t = i*dt
    
    rk.iterate(sol, dt, t);
    norm = sol.norm()
    if (math.isnan(norm)):
      print 'ERROR : NAN'
      break
    print'|ITER|', i, '|NORM|', norm, '|DT|', dt, '|t|', t, 'cpu', time.clock() - tic
    if (i % 1000 == 0):
      sol.exportMsh("output/ParoiShielding-%05i" % (nbExport), t, nbExport)
    nbExport  = nbExport + 1
      


Msg.Exit(0)

