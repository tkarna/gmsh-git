from dgpy import *
import gmshPartition
import os
import time
import math


LREF = 1;
ldom = 0.38/LREF;
lpml = 0.1/LREF;
lext = 0.12/LREF;
print'---- Load Mesh and Build Groups'

model = GModel()
model.load(gmshPartition.simple('scattExplicit1.msh'))

dim = 2
order = 1
groups = dgGroupCollection(model, dim, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()
lam =3
freq=5e8
w=2*math.pi*freq
mu0=4*math.pi*1e-7
eps0=8.85*1e-12
cValue0=1/math.sqrt(eps0*mu0)
cValue=cValue0
k=w/cValue
rhoValue = mu0
sigma=1/(100.*1e-3)
coef = functionConstant([cValue,rhoValue,sigma])


print'---- Build Initial conditions'

initialConditionPythonp = functionConstant([0.])
initialConditionPythonU = functionConstant([0.,0.])

solutionp = dgDofContainer(groups,1)
solutionp.setFieldName(0,'Ez')
solutionp.L2Projection(initialConditionPythonp)
solutionp.exportMsh("output/Time_p_%05d" % (0), 0, 0)

solutionU = dgDofContainer(groups,2)
solutionU.setFieldName(0,'-Hy_inc')
solutionU.setFieldName(1,'Hx_inc')
solutionU.L2Projection(initialConditionPythonU)
solutionU.exportMsh("output/Time_U_%05d" % (0), 0, 0)

print'---- Load Conservation law with coefficients'

lawp = dgConservationLawWaveExplicit('p',dim)
lawU = dgConservationLawWaveExplicit('U',dim)

lawp.useInterfaceShieldOnP()
lawU.useInterfaceShieldOnP()

lawp.useInterfaceShieldOnP()
lawU.useInterfaceShieldOnP()



#lawp.useDissipationOnP() 
#lawU.useDissipationOnU() 


lawp.setPhysCoef(coef)
lawU.setPhysCoef(coef)

solUprev = dgDofContainer(groups, lawU.getNbFields())

lawp.setU(solutionU.getFunction())
lawU.setp(solutionp.getFunction())

print'---- Load Interface and Boundary conditions'

t=0
w=1100*math.pi*1e6
wc=1000*math.pi*1e6
tho=5e-8
k=1/(6.28e10)
x0=-0.5
def extFieldFunction(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    value = math.sin(2*math.pi*5e8*t+k*(x-x0))
    val.set(i,0,value)
    val.set(i,1,value/(cValue*rhoValue))
    val.set(i,2,0.)
extField = functionPython(3, extFieldFunction, [xyz])

#lawp.addInterfaceOpenOutflow('BOUNDARY_SCATT_LEFT')
#lawp.addInterfaceOpenOutflow('BOUNDARY_SCATT_RIGHT')
#lawp.addInterfaceOpenOutflow('BOUNDARY_SCATT_BOTTOM')
#lawU.addInterfaceOpenOutflow('BOUNDARY_SCATT_LEFT')
#lawU.addInterfaceOpenOutflow('BOUNDARY_SCATT_RIGHT')
#lawU.addInterfaceOpenOutflow('BOUNDARY_SCATT_BOTTOM')
lawp.addInterfaceShield('BOUNDARY_SCATT_LEFT')
#lawp.addInterfaceShield('BOUNDARY_SCATT_RIGHT')
#lawp.addInterfaceShield('BOUNDARY_SCATT_BOTTOM')
lawU.addInterfaceShield('BOUNDARY_SCATT_LEFT')
#lawU.addInterfaceShield('BOUNDARY_SCATT_RIGHT')
#lawU.addInterfaceShield('BOUNDARY_SCATT_BOTTOM')

lawp.addBoundaryCondition('BOUNDARY_EXT',      lawp.newBoundaryHomogeneousDirichletOnU('BOUNDARY_EXT'))
lawp.addBoundaryCondition('BOUNDARY_EXT1',      lawp.newBoundaryOpenOutflow('BOUNDARY_EXT1'))
lawp.addBoundaryCondition('BOUNDARY_EXT_LEFT', lawp.newBoundaryOpenOutflowWithInflow('BOUNDARY_EXT_LEFT', extField))
lawU.addBoundaryCondition('BOUNDARY_EXT',      lawU.newBoundaryHomogeneousDirichletOnU('BOUNDARY_EXT'))
lawU.addBoundaryCondition('BOUNDARY_EXT1',      lawU.newBoundaryOpenOutflow('BOUNDARY_EXT1'))
lawU.addBoundaryCondition('BOUNDARY_EXT_LEFT', lawU.newBoundaryOpenOutflowWithInflow('BOUNDARY_EXT_LEFT', extField))





incidFieldp=fullMatrixDouble(1,1)
transFieldp=fullMatrixDouble(1,1)
totalFieldp=fullMatrixDouble(1,1)
transFieldp_mat=fullMatrixDouble(1,1)
incidFieldU=fullMatrixDouble(1,1)
transFieldU=fullMatrixDouble(1,1)
totalFieldU=fullMatrixDouble(1,1)
transFieldU_mat=fullMatrixDouble(1,1)


def psolShield(val,xyz, solution):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    s= solution.get(i,0)
    value  = s
    val.set(i,0,value)

def psolShield1(val,xyz, solution):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    s= solution.get(i,1)
    value  = s
    val.set(i,0,value)



print'---- LOOP'


tic = time.clock()
sysp = linearSystemPETScBlockDouble()

sysU = linearSystemPETScBlockDouble()



dofp = dgDofManager.newDGBlock(groups, 1, sysp)
dofU = dgDofManager.newDGBlock(groups, 2, sysU)



solverp = dgERK(lawp,None,DG_ERK_EULER)
solverU = dgERK(lawU,None,DG_ERK_EULER)

dt = 0.1e-11
iterMax = 20000
solverp.getAssembler().setScatterField(solutionU)
solverU.getAssembler().setScatterField(solutionp)

for i in range(1,iterMax+1):
  t = dt*i

  if (i % 100 == 0 and Msg.GetCommRank() == 0):
    print("t=%.8e\n"%(t/cValue0))

  solverp.iterate(solutionp,dt,t)
  solUprev.copy(solutionU);
  solverU.iterate(solutionU, dt, t+dt/2)
  if (i % 100 == 0):
    solutionp.exportMsh("output/Time_p_%05d" % (i), t, i)
    solutionU.exportMsh("output/Time_U_%05d" % (i), t, i)
  psol = functionPython(1,psolShield,[xyz, solutionp.getFunction()])
  totalfieldp = dgFunctionEvaluator(groups, psol)
  incidfieldp = dgFunctionEvaluator(groups, psol)
  transfieldp = dgFunctionEvaluator(groups, psol)
  incidfieldp.compute(0, 0.0,0.0, incidFieldp)
  totalfieldp.compute(-0.15, 0,0.0, totalFieldp)
  transfieldp.compute(0.145, 0.0, 0.0, transFieldp)
  if (i % 1 == 0 and Msg.GetCommRank() == 0):
    print '|ITER|', i, 'cpu', time.clock() - tic
    print("%.8e  %.8e %.8e %.8e \n"% (t,incidFieldp(0,0),totalFieldp(0,0),transFieldp(0,0)))
    fout_shield=open("Time_ppppp.dat","a")
    fout_shield.write("%.8e  %.8e %.8e %.8e \n"% (t, incidFieldp(0,0), totalFieldp(0,0),transFieldp(0,0)))
    fout_shield.close()
Msg.Exit(0)



