
LREF = 1;
ldom = 0.38/LREF;
lpml = 0.1/LREF;
lext = 0.12/LREF;

lc = 0.015/LREF;
lcExt = 2*lc;
lcPmlExt = lc;
lcPmlInt = lc;
lcScatExt = lc;
lcScatInt = lc;
d=0.04;
L  = 0.1/LREF;
x0 = 0.15/LREF;
x1 = 1.4/LREF;
y0 = 0.15/LREF;
y1 = 1.4/LREF;

Point(1) = {0.,0.,0.,lcPmlInt};

Point(2) = {  ldom,    0., 0., lcPmlInt};
Point(3) = {     0,  ldom, 0., lcPmlInt};
Point(4) = { -ldom,    0., 0., lcPmlInt};
Point(5) = {     0, -ldom, 0., lcPmlInt};

Point(6) = {  ldom+lpml,         0., 0., lcPmlExt};
Point(7) = {          0,  ldom+lpml, 0., lcPmlExt};
Point(8) = { -ldom-lpml,         0., 0., lcPmlExt};
Point(9) = {          0, -ldom-lpml, 0., lcPmlExt};

Point(10) = {  ldom+lext,  ldom+lext, 0., lcExt};
Point(11) = { -ldom-lext,  ldom+lext, 0., lcExt};
Point(12) = { -ldom-lext, -ldom-lext, 0., lcExt};
Point(13) = {  ldom+lext, -ldom-lext, 0., lcExt};



Point(26) = { -x0,  L/2, 0, lcScatInt};
Point(27) = { -x0,   y0, 0, lcScatInt};
Point(28) = {  x0,   y0, 0, lcScatInt};
Point(29) = {  x0,  -y0, 0, lcScatInt};
Point(30) = { -x0,  -y0, 0, lcScatInt};
Point(31) = { -x0, -L/2, 0, lcScatInt};
Point(32) = { -x0-d, -L/2, 0, lcScatInt};
Point(33) = { -x0-d, L/2, 0, lcScatInt};

Circle(1) = {2,1,3};
Circle(2) = {3,1,4};
Circle(3) = {4,1,5};
Circle(4) = {5,1,2};

Circle(5) = {6,1,7};
Circle(6) = {7,1,8};
Circle(7) = {8,1,9};
Circle(8) = {9,1,6};

Line(9)  = {10,11};
Line(10) = {11,12};
Line(11) = {12,13};
Line(12) = {13,10};

Line(25) = {33, 26};
Line(26) = {26, 27};
Line(27) = {27, 28};
Line(28) = {28, 29};
Line(29) = {29, 30};
Line(30) = {30, 31};
Line(31) = {31, 32};
Line(32) = {32, 33};


//l=25;
//Transfinite Line (25) = 5;
//Transfinite Line (32) = 20;
//Transfinite Line (31) = 5;
//Transfinite Line (23) = l*(2*x1);
//Transfinite Line (24) = l*(y1-L/2);
//Transfinite Line (25) = l*(x1-x0);
//Transfinite Line (26) = l*(y0-L/2);
//Transfinite Line (27) = l*(2*x0);
//Transfinite Line (28) = l*(2*y0);
//Transfinite Line (29) = l*(2*x0);
//Transfinite Line (30) = l*(y0-L/2);
//Transfinite Line (31) = l*(x1-x0);



Line Loop(1) = {1, 2, 3, 4};
Line Loop(2) = {-8, -7, -6, -5};
Line Loop(3) = {-9, -10, -11, -12};
Line Loop(5) = {-26,-27,-28,-29,-30,-31,-32,-25};

Plane Surface(1) = {1,5};  // DOM
Plane Surface(2) = {1, 2};  // PML
Plane Surface(3) = {2, 3};  // EXT
Plane Surface(5) = {5};  // Cav

Physical Line("BOUNDARY_DOM") = {1, 2, 3, 4};
Physical Line("BOUNDARY_PML") = {-8, -7, -6, -5};
Physical Line("BOUNDARY_EXT_LEFT") = {10};
Physical Line("BOUNDARY_EXT") = {11, 9};
Physical Line("BOUNDARY_EXT1") = {12};
//Physical Line("BOUNDARY_SCATT") = {26, 27, 28, 29, 30};
//Physical Line("BOUNDARY_SCATT_TOP") = {27};
Physical Line("BOUNDARY_SCATT_BOTTOM") = {25,31,29,27};
Physical Line("BOUNDARY_SCATT_LEFT") = {26,30};
Physical Line("BOUNDARY_SCATT_RIGHT") = {28};

Physical Surface("DOM") = {1};
Physical Surface("PML") = {2};
Physical Surface("EXT") = {3};
Physical Surface("CAV") = {5};

