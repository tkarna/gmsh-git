from dgpy import *
import os
import time
import math



#from scattData import *

LREF = 0.5
ldom = 0.5/LREF
lpml = 0.1/LREF
lext = 0.5/LREF

L  = 0.16/LREF
x0 = 0.29/LREF
x1 = 0.30/LREF
y0 = 0.19/LREF
y1 = 0.20/LREF

ar = 0.2/LREF
ax = x1+ar
ay = y1+ar


print'---- Load Mesh and Build Groups'

model = GModel()
model.load('CavityInCuboid.msh')

dim = 2
order = 1
groups = dgGroupCollection(model, dim, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()



print'---- Build Initial conditions'

initialConditionPython = functionConstant([0.,0.,0.,0.])

solutioninc = dgDofContainer(groups,3)
solutioninc.setFieldName(0,'Ez_inc')
solutioninc.setFieldName(1,'-Hy_inc')
solutioninc.setFieldName(2,'Hx_inc')
solutioninc.L2Projection(initialConditionPython)

solutionref = dgDofContainer(groups,3)
solutionref.setFieldName(0,'Ez_ref')
solutionref.setFieldName(1,'-Hy_ref')
solutionref.setFieldName(2,'Hx_ref')
solutionref.L2Projection(initialConditionPython)

solutionpmlA = dgDofContainer(groups,4)
solutionpmlA.setFieldName(0,'Ez_pmlA')
solutionpmlA.setFieldName(1,'-Hy_pmlA')
solutionpmlA.setFieldName(2,'Hx_pmlA')
solutionpmlA.setFieldName(3,'Q_pmlA')
solutionpmlA.L2Projection(initialConditionPython)

solutionpmlB = dgDofContainer(groups,4)
solutionpmlB.setFieldName(0,'Ez_pmlB')
solutionpmlB.setFieldName(1,'-Hy_pmlB')
solutionpmlB.setFieldName(2,'Hx_pmlB')
solutionpmlB.setFieldName(3,'Q_pmlB')
solutionpmlB.L2Projection(initialConditionPython)

solutionpmlC = dgDofContainer(groups,4)
solutionpmlC.setFieldName(0,'Ez_pmlC')
solutionpmlC.setFieldName(1,'-Hy_pmlC')
solutionpmlC.setFieldName(2,'Hx_pmlC')
solutionpmlC.setFieldName(3,'Q_pmlC')
solutionpmlC.L2Projection(initialConditionPython)

solutionpmlD = dgDofContainer(groups,4)
solutionpmlD.setFieldName(0,'Ez_pmlD')
solutionpmlD.setFieldName(1,'-Hy_pmlD')
solutionpmlD.setFieldName(2,'Hx_pmlD')
solutionpmlD.setFieldName(3,'Q_pmlD')
solutionpmlD.L2Projection(initialConditionPython)



print'---- Load Conservation law with coefficients'

lawinc = dgConservationLawWave(dim)
lawref = dgConservationLawWave(dim)
lawpmlA = dgConservationLawWave(dim,'PmlBeveledCuboid')
lawpmlB = dgConservationLawWave(dim,'PmlBeveledCuboid')
lawpmlC = dgConservationLawWave(dim,'PmlBeveledCuboid')
lawpmlD = dgConservationLawWave(dim,'PmlBeveledCuboid')

mu0  = 12.566370614 * pow(10,-7)
eps0 = 8.85 * pow(10,-12)
c0   = 1/math.sqrt(mu0*eps0)

muValue  = 1.
epsValue = 1.
cValue   = 1/math.sqrt(muValue*epsValue)  # 1/Sqrt(Mu*Epsilon)
rhoValue = muValue                        # Mu
coef = functionConstant([cValue,rhoValue])

lawinc.setPhysCoef(coef)
lawref.setPhysCoef(coef)
lawpmlA.setPhysCoef(coef)
lawpmlB.setPhysCoef(coef)
lawpmlC.setPhysCoef(coef)
lawpmlD.setPhysCoef(coef)

dec = 1.
lawinc.setFluxDecentering(dec)
lawref.setFluxDecentering(dec)
lawpmlA.setFluxDecentering(dec)
lawpmlB.setFluxDecentering(dec)
lawpmlC.setFluxDecentering(dec)
lawpmlD.setFluxDecentering(dec)



print'---- Load PML'

alphaDim = c0
alphaNorm = alphaDim*LREF/c0
lawpmlA.useGenericPmlCoef('BeveledCuboid',lpml*1.01,cValue, 0,0,0, ax,ay,0, ar, 'Poly2', alphaNorm)
lawpmlB.useGenericPmlCoef('BeveledCuboid',lpml*1.01,cValue, 0,0,0, ax,ay,0, ar, 'Poly3', alphaNorm)
lawpmlC.useGenericPmlCoef('BeveledCuboid',lpml*1.01,cValue, 0,0,0, ax,ay,0, ar, 'Hyp'  , alphaNorm)
lawpmlD.useGenericPmlCoef('BeveledCuboid',lpml*1.01,cValue, 0,0,0, ax,ay,0, ar, 'HypSh', alphaNorm)

lawpmlA.useGenericPmlDir('BeveledCuboid', 0,0,0, ax,ay,0, ar)
lawpmlB.useGenericPmlDir('BeveledCuboid', 0,0,0, ax,ay,0, ar)
lawpmlC.useGenericPmlDir('BeveledCuboid', 0,0,0, ax,ay,0, ar)
lawpmlD.useGenericPmlDir('BeveledCuboid', 0,0,0, ax,ay,0, ar)

lawpmlA.addPml('PML')
lawpmlB.addPml('PML')
lawpmlC.addPml('PML')
lawpmlD.addPml('PML')

pmlDir = lawpmlA.getPmlDir()
pmlDirDof = dgDofContainer(groups,3)
pmlDirDof.setFieldName(0,'DirX')
pmlDirDof.setFieldName(1,'DirY')
pmlDirDof.setFieldName(2,'DirZ')
pmlDirDof.interpolate(pmlDir)
pmlDirDof.exportMsh("output/Scatt_PmlA_Dir", 0, 0)

pmlDir = lawpmlB.getPmlDir()
pmlDirDof = dgDofContainer(groups,3)
pmlDirDof.setFieldName(0,'DirX')
pmlDirDof.setFieldName(1,'DirY')
pmlDirDof.setFieldName(2,'DirZ')
pmlDirDof.interpolate(pmlDir)
pmlDirDof.exportMsh("output/Scatt_PmlB_Dir", 0, 0)

pmlDir = lawpmlC.getPmlDir()
pmlDirDof = dgDofContainer(groups,3)
pmlDirDof.setFieldName(0,'DirX')
pmlDirDof.setFieldName(1,'DirY')
pmlDirDof.setFieldName(2,'DirZ')
pmlDirDof.interpolate(pmlDir)
pmlDirDof.exportMsh("output/Scatt_PmlC_Dir", 0, 0)

pmlDir = lawpmlD.getPmlDir()
pmlDirDof = dgDofContainer(groups,3)
pmlDirDof.setFieldName(0,'DirX')
pmlDirDof.setFieldName(1,'DirY')
pmlDirDof.setFieldName(2,'DirZ')
pmlDirDof.interpolate(pmlDir)
pmlDirDof.exportMsh("output/Scatt_PmlD_Dir", 0, 0)

pmlCoef = lawpmlA.getPmlCoef()
pmlCoefDof = dgDofContainer(groups,3)
pmlCoefDof.setFieldName(0,'Coef0')
pmlCoefDof.setFieldName(1,'Coef1')
pmlCoefDof.setFieldName(2,'Coef2')
pmlCoefDof.L2Projection(pmlCoef)
pmlCoefDof.exportMsh("output/Scatt_PmlA_Coef", 0, 0)

pmlCoef = lawpmlB.getPmlCoef()
pmlCoefDof = dgDofContainer(groups,3)
pmlCoefDof.setFieldName(0,'Coef0')
pmlCoefDof.setFieldName(1,'Coef1')
pmlCoefDof.setFieldName(2,'Coef2')
pmlCoefDof.L2Projection(pmlCoef)
pmlCoefDof.exportMsh("output/Scatt_PmlB_Coef", 0, 0)

pmlCoef = lawpmlC.getPmlCoef()
pmlCoefDof = dgDofContainer(groups,3)
pmlCoefDof.setFieldName(0,'Coef0')
pmlCoefDof.setFieldName(1,'Coef1')
pmlCoefDof.setFieldName(2,'Coef2')
pmlCoefDof.L2Projection(pmlCoef)
pmlCoefDof.exportMsh("output/Scatt_PmlC_Coef", 0, 0)

pmlCoef = lawpmlD.getPmlCoef()
pmlCoefDof = dgDofContainer(groups,3)
pmlCoefDof.setFieldName(0,'Coef0')
pmlCoefDof.setFieldName(1,'Coef1')
pmlCoefDof.setFieldName(2,'Coef2')
pmlCoefDof.L2Projection(pmlCoef)
pmlCoefDof.exportMsh("output/Scatt_PmlD_Coef", 0, 0)



print'---- Load Interface and Boundary conditions'

t=0
def extFieldFunction(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    value = math.exp(-math.pow((x+ldom+lext+0.2-cValue*t),2)*100.)
    val.set(i,0,value)
extField = functionPython(1, extFieldFunction, [xyz])

dt=0
def extShiftFieldFunction(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    value = math.exp(-math.pow((x+ldom+lext+0.2-cValue*(t+dt/2)),2)*100.)
    val.set(i,0,value)
extShiftField = functionPython(1, extShiftFieldFunction, [xyz])


lawref.addInterfaceHomogeneousDirichletOnP('BOUNDARY_SCATT')
lawref.addInterfaceScatteredTotalFields('INTERFACE', solutioninc.getFunction(),lawpmlA.getPmlDir())
lawref.addBoundaryCondition('BOUNDARY_EXT',      lawref.newBoundaryWall('BOUNDARY_EXT'))
lawref.addBoundaryCondition('BOUNDARY_EXT_LEFT', lawref.newBoundaryWall('BOUNDARY_EXT_LEFT'))
#lawref.addBoundaryCondition('BOUNDARY_EXT_LEFT', lawref.newBoundaryDirichletOnP('BOUNDARY_EXT_LEFT',extShiftField))

lawinc.addBoundaryCondition('BOUNDARY_EXT',      lawinc.newBoundaryWall('BOUNDARY_EXT'))
lawinc.addBoundaryCondition('BOUNDARY_EXT_LEFT', lawinc.newBoundaryDirichletOnP('BOUNDARY_EXT_LEFT',extField))


# PML

lawpmlA.addInterfaceHomogeneousDirichletOnP('BOUNDARY_SCATT')
lawpmlB.addInterfaceHomogeneousDirichletOnP('BOUNDARY_SCATT')
lawpmlC.addInterfaceHomogeneousDirichletOnP('BOUNDARY_SCATT')
lawpmlD.addInterfaceHomogeneousDirichletOnP('BOUNDARY_SCATT')

lawpmlA.addInterfacePmlWithInflow('INTERFACE', solutioninc.getFunction())
lawpmlB.addInterfacePmlWithInflow('INTERFACE', solutioninc.getFunction())
lawpmlC.addInterfacePmlWithInflow('INTERFACE', solutioninc.getFunction())
lawpmlD.addInterfacePmlWithInflow('INTERFACE', solutioninc.getFunction())

lawpmlA.addInterfaceHomogeneousDirichletOnP('BOUNDARY_PML')
lawpmlB.addInterfaceHomogeneousDirichletOnP('BOUNDARY_PML')
lawpmlC.addInterfaceHomogeneousDirichletOnP('BOUNDARY_PML')
lawpmlD.addInterfaceHomogeneousDirichletOnP('BOUNDARY_PML')

lawpmlA.addBoundaryCondition('BOUNDARY_EXT', lawpmlA.newBoundaryWall('BOUNDARY_EXT'))
lawpmlB.addBoundaryCondition('BOUNDARY_EXT', lawpmlB.newBoundaryWall('BOUNDARY_EXT'))
lawpmlC.addBoundaryCondition('BOUNDARY_EXT', lawpmlC.newBoundaryWall('BOUNDARY_EXT'))
lawpmlD.addBoundaryCondition('BOUNDARY_EXT', lawpmlD.newBoundaryWall('BOUNDARY_EXT'))

lawpmlA.addBoundaryCondition('BOUNDARY_EXT_LEFT', lawpmlA.newBoundaryWall('BOUNDARY_EXT_LEFT'))
lawpmlB.addBoundaryCondition('BOUNDARY_EXT_LEFT', lawpmlB.newBoundaryWall('BOUNDARY_EXT_LEFT'))
lawpmlC.addBoundaryCondition('BOUNDARY_EXT_LEFT', lawpmlC.newBoundaryWall('BOUNDARY_EXT_LEFT'))
lawpmlD.addBoundaryCondition('BOUNDARY_EXT_LEFT', lawpmlD.newBoundaryWall('BOUNDARY_EXT_LEFT'))



print'---- LOOP'

sysinc = linearSystemPETScBlockDouble()
sysref = linearSystemPETScBlockDouble()
syspmlA = linearSystemPETScBlockDouble()
syspmlB = linearSystemPETScBlockDouble()
syspmlC = linearSystemPETScBlockDouble()
syspmlD = linearSystemPETScBlockDouble()

sysinc.setParameter("petscOptions", "-ksp_type gmres -pc_type none")
sysref.setParameter("petscOptions", "-ksp_type gmres -pc_type none")
syspmlA.setParameter("petscOptions", "-ksp_type gmres -pc_type none")
syspmlB.setParameter("petscOptions", "-ksp_type gmres -pc_type none")
syspmlC.setParameter("petscOptions", "-ksp_type gmres -pc_type none")
syspmlD.setParameter("petscOptions", "-ksp_type gmres -pc_type none")

dofinc = dgDofManager.newDGBlock(groups, 3, sysinc)
dofref = dgDofManager.newDGBlock(groups, 3, sysref)
dofpmlA = dgDofManager.newDGBlock(groups, 4, syspmlA)
dofpmlB = dgDofManager.newDGBlock(groups, 4, syspmlB)
dofpmlC = dgDofManager.newDGBlock(groups, 4, syspmlC)
dofpmlD = dgDofManager.newDGBlock(groups, 4, syspmlD)

solverinc = dgCrankNicholson(solutioninc, lawinc, dofinc, 0.)
solverref = dgCrankNicholson(solutionref, lawref, dofref, 0.)
solverpmlA = dgCrankNicholson(solutionpmlA, lawpmlA, dofpmlA, 0.)
solverpmlB = dgCrankNicholson(solutionpmlB, lawpmlB, dofpmlB, 0.)
solverpmlC = dgCrankNicholson(solutionpmlC, lawpmlC, dofpmlC, 0.)
solverpmlD = dgCrankNicholson(solutionpmlD, lawpmlD, dofpmlD, 0.)

solverinc.setComputeExplicitTerm(True)
solverref.setComputeExplicitTerm(True)
solverpmlA.setComputeExplicitTerm(True)
solverpmlB.setComputeExplicitTerm(True)
solverpmlC.setComputeExplicitTerm(True)
solverpmlD.setComputeExplicitTerm(True)


INDEX = 1


dt = 1.5*pow(10,-2)
iterMax = 500

fichier = open('output/Resu_%05d.txt' % (INDEX), "w")

for i in range(1,iterMax+1):
  t = i*dt
  
  solverinc.iterate(t)
  solverref.iterate(t)
  solverpmlA.iterate(t)
  solverpmlB.iterate(t)
  solverpmlC.iterate(t)
  solverpmlD.iterate(t)
  
  energyref = lawref.getTotalEnergy(groups,solutionref,'DOM')
  energyerrorpmlA = lawpmlA.getTotalEnergyError(groups,solutionpmlA,solutionref,'DOM')
  energyerrorpmlB = lawpmlB.getTotalEnergyError(groups,solutionpmlB,solutionref,'DOM')
  energyerrorpmlC = lawpmlC.getTotalEnergyError(groups,solutionpmlC,solutionref,'DOM')
  energyerrorpmlD = lawpmlD.getTotalEnergyError(groups,solutionpmlD,solutionref,'DOM')
  
  print '|ITER|', i, '|REF|', energyref, '|A|', energyerrorpmlA, '|B|', energyerrorpmlB, '|C|', energyerrorpmlC, '|D|', energyerrorpmlD
  
  fichier.write('%d %d %e %e %e %e %e %e\n' % (i, iterMax, alphaNorm, alphaDim/c0, energyerrorpmlA, energyerrorpmlB, energyerrorpmlC, energyerrorpmlD))
  
  #refvalue = math.pow(energyref,0.5)
  #errorpmlA = math.pow(energyerrorpmlA/energyref,0.5)
  #errorpmlB = math.pow(energyerrorpmlB/energyref,0.5)
  #errorpmlC = math.pow(energyerrorpmlC/energyref,0.5)
  #errorpmlD = math.pow(energyerrorpmlD/energyref,0.5)
  
  solutioninc.exportMsh("output/Scatt_Inc_%05d" % (i), i, i)
  solutionref.exportMsh("output/Scatt_Ref_%05d" % (i), i, i)
  solutionpmlA.exportMsh("output/Scatt_PmlA_%05d" % (i), i, i)
  solutionpmlB.exportMsh("output/Scatt_PmlB_%05d" % (i), i, i)
  solutionpmlC.exportMsh("output/Scatt_PmlC_%05d" % (i), i, i)
  solutionpmlD.exportMsh("output/Scatt_PmlD_%05d" % (i), i, i)

fichier.close()

#solutioninc.exportMsh("output/Scatt_Inc_%05d" % (INDEX), INDEX, INDEX)
#solutionref.exportMsh("output/Scatt_RefScat_%05d" % (INDEX), INDEX, INDEX)
#solutionpmlA.exportMsh("output/Scatt_PmlCircA_%05d" % (INDEX), INDEX, INDEX)
#solutionpmlB.exportMsh("output/Scatt_PmlCircB_%05d" % (INDEX), INDEX, INDEX)
#solutionpmlC.exportMsh("output/Scatt_PmlCircC_%05d" % (INDEX), INDEX, INDEX)
#solutionpmlD.exportMsh("output/Scatt_PmlCircD_%05d" % (INDEX), INDEX, INDEX)

#print '|INDEX|', INDEX, '|REF|', refvalue, '|A|', errorpmlA, '|B|', errorpmlB, '|C|', errorpmlC, '|D|', errorpmlD

Msg.Exit(0)

