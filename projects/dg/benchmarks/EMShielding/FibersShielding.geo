mm    = 0.5;
n     = 5;
L     = (n)*mm;
Lcut  = (n)*mm;
lsca1 = 4*mm;
Lext  = L;
Lpml  = L/2;



For i In {0:n}
  x[i]=i*L/n;
  y[i]=i*L/n;
EndFor

For i In {0:n-1}
  For j In {0:n-1}
    ij=1000*i+j;
    cpx[ij]=(x[i+1]+x[i])/2+(L/n)*((x[i+1]-x[i])/2);
    cpy[ij]=(y[j+1]+y[j])/2+(L/n)*((y[j+1]-y[j])/2);
  EndFor
EndFor
/*
For i In {0:n-1}
  For j In {0:n-1}
    ij=1000*i+j;
    cpx[ij]=(x[i+1]+x[i])/2+(Rand(1)+Rand(-1))*((x[i+1]-x[i])/2);
    cpy[ij]=(y[j+1]+y[j])/2+(Rand(1)+Rand(-1))*((y[j+1]-y[j])/2);
  EndFor
EndFor*/

Function circle
  center=newp; Point(center)={cx,cy,0};
  p1=newp; Point(p1)={cx-r,cy,0,lsca2};
  p2=newp; Point(p2)={cx,cy+r,0,lsca2};
  p3=newp; Point(p3)={cx+r,cy,0,lsca2};
  p4=newp; Point(p4)={cx,cy-r,0,lsca2};
  s1=newreg; Circle(s1)={p1,center,p2};
  s2=newreg; Circle(s2)={p2,center,p3};
  s3=newreg; Circle(s3)={p3,center,p4};
  s4=newreg; Circle(s4)={p4,center,p1};
  cir[icircle]=newreg; Line Loop(cir[icircle])={s1,s2,s3,s4};
//  Physical Line(cir[icircle])={s1,s2,s3,s4};
  Plane Surface(cir[icircle]+20)={cir[icircle]};
//  Physical Surface(cir[icircle]+20)={cir[icircle]+20};
  circlloop[icircle] = cir[icircle];
  circsloop[icircle] = cir[icircle]+20;
  icircle+=1;
Return

Function creatVoid
    AB=Sqrt((a[0]-b[0])^2+(a[1]-b[1])^2);
    AC=Sqrt((a[0]-c[0])^2+(a[1]-c[1])^2);
    BC=Sqrt((c[0]-b[0])^2+(c[1]-b[1])^2);

    For i In {0:1}
      uAB[i]=(b[i]-a[i])/AB;
      uAC[i]=(c[i]-a[i])/AC;
      u1[i]=uAB[i]+uAC[i];
      uBC[i]=(c[i]-b[i])/BC;
      u2[i]=uBC[i]-uAB[i];
    EndFor

    det=-u1[0]*u2[1]+u1[1]*u2[0];
    t=(u2[1]*(a[0]-b[0])-u2[0]*(a[1]-b[1]))/det;
    For i In {0:1}
      O[i]=a[i]+t*u1[i];
    EndFor

    nAB[0]=-uAB[1];
    nAB[1]=uAB[0];
    r=(nAB[0]*(O[0]-a[0])+nAB[1]*(O[1]-a[1]))/Sqrt(nAB[0]*nAB[0]+nAB[1]*nAB[1])*0.7;
    If (r<0)
      r=-r;
    EndIf

    cx=O[0]; cy=O[1]; 
    hbot=cy-(L/2-Lcut/2);
    htop=L/2+Lcut/2-cy;
    hleft=cx-(L/2-Lcut/2);
    hright=L/2+Lcut/2-cx;

    If ((hbot>=r) && (htop>=r) && (hleft>=r) && (hright>=r))
      rsum+=r; rcomp+=1; lsca2=3*r/6;
      Call circle;
    EndIf

    If ((hleft<r) && (hleft>-r) && (htop>r) && (hbot>r))
      rsum+=r; rcomp+=1; lsca2=3*r/6;
      delta=Sqrt(r*r-hleft*hleft);
      center=newp; Point(center)={cx,cy,0};
      left[ileft]=newp; Point(left[ileft])={L/2-Lcut/2,cy-delta,0,lsca2}; 
      dleft[ileft]=cy-delta;      
      ileft+=1;
      ppp=newp; Point(ppp)={cx+r,cy,0,lsca2};
      left[ileft]=newp; Point(left[ileft])={L/2-Lcut/2,cy+delta,0,lsca2}; 
      dleft[ileft]=cy+delta;
      ileft+=1;
      s=newreg; Circle(s)={left[ileft-2],center,ppp}; lloop[icar]=-s;  icar+=1;
      s=newreg; Circle(s)={ppp,center,left[ileft-1]}; lloop[icar]=-s;  icar+=1;
    EndIf
    If ((hright<r) && (hright>-r) && (htop>r) && (hbot>r))
      rsum+=r; rcomp+=1; lsca2=3*r/6;
      delta=Sqrt(r*r-hright*hright);
      center=newp; Point(center)={cx,cy,0};
      right[iright]=newp; Point(right[iright])={L/2+Lcut/2,cy-delta,0,lsca2}; 
      dright[iright]=cy-delta;      
      iright+=1;
      ppp=newp; Point(ppp)={cx-r,cy,0,lsca2};
      right[iright]=newp; Point(right[iright])={L/2+Lcut/2,cy+delta,0,lsca2}; 
      dright[iright]=cy+delta;
      iright+=1;
      s=newreg; Circle(s)={right[iright-2],center,ppp}; lloop[icar]=s;  icar+=1;
      s=newreg; Circle(s)={ppp,center,right[iright-1]}; lloop[icar]=s;  icar+=1;
    EndIf
    If ((htop<r) && (htop>-r) && (hleft>r) && (hright>r))
      rsum+=r; rcomp+=1; lsca2=3*r/6;
      delta=Sqrt(r*r-htop*htop);
      center=newp; Point(center)={cx,cy,0};
      top[itop]=newp; Point(top[itop])={cx-delta,L/2+Lcut/2,0,lsca2}; 
      dtop[itop]=cx-delta;      
      itop+=1;
      ppp=newp; Point(ppp)={cx,cy-r,0,lsca2};
      top[itop]=newp; Point(top[itop])={cx+delta,L/2+Lcut/2,0,lsca2}; 
      dtop[itop]=cx+delta;
      itop+=1;
      s=newreg; Circle(s)={top[itop-2],center,ppp}; lloop[icar]=-s;  icar+=1;
      s=newreg; Circle(s)={ppp,center,top[itop-1]}; lloop[icar]=-s;  icar+=1;
    EndIf
    If ((hbot<r) && (hbot>-r) && (hleft>r) && (hright>r))
      rsum+=r; rcomp+=1; lsca2=3*r/6;
      delta=Sqrt(r*r-hbot*hbot);
      center=newp; Point(center)={cx,cy,0};
      bot[ibot]=newp; Point(bot[ibot])={cx-delta,L/2-Lcut/2,0,lsca2}; 
      dbot[ibot]=cx-delta;      
      ibot+=1;
      ppp=newp; Point(ppp)={cx,cy+r,0,lsca2};
      bot[ibot]=newp; Point(bot[ibot])={cx+delta,L/2-Lcut/2,0,lsca2}; 
      dbot[ibot]=cx+delta;
      ibot+=1;
      s=newreg; Circle(s)={bot[ibot-2],center,ppp}; lloop[icar]=s;  icar+=1;
      s=newreg; Circle(s)={ppp,center,bot[ibot-1]}; lloop[icar]=s;  icar+=1;
    EndIf
Return

xboudary[0]=L/2-Lcut/2-Lext;
xboudary[1]=L/2-Lcut/2-Lext+Lpml;
xboudary[2]=L/2-Lcut/2;
xboudary[3]=L/2+Lcut/2;
xboudary[4]=L/2+Lcut/2+Lext;

For i In {0:4}
  p[i]=newp; Point(p[i])= {xboudary[i], L/2+Lcut/2, 0, lsca1/10};
EndFor
For i In {5:9}
  p[i]=newp; Point(p[i])= {xboudary[9-i], L/2-Lcut/2, 0, lsca1/10};
EndFor

icircle=0;
ileft=1; iright=1; itop=1; ibot=1;
left[0]=p[3]; right[0]=p[0]; top[0]=p[2]; bot[0]=p[3];
dleft[0]=L/2-Lcut/2; dright[0]=L/2-Lcut/2; dtop[0]=L/2-Lcut/2; dbot[0]=L/2-Lcut/2;
icar=0;
rsum=0;
rcomp=0;
voidsum=0;
For k In {0:n-2}
  For l In {0:n-2}
    ij=1000*k+l;
    ij1=1000*k+l+1;
    i1j=1000*(k+1)+l;
    i1j1=1000*(k+1)+l+1;
    a[0]=cpx[ij];
    a[1]=cpy[ij];
    b[0]=cpx[i1j];  
    b[1]=cpy[i1j];
    c[0]=cpx[i1j1];
    c[1]=cpy[i1j1];
    Call creatVoid;
    
    a[0]=cpx[ij];
    a[1]=cpy[ij];
    b[0]=cpx[ij1];  
    b[1]=cpy[ij1];
    c[0]=cpx[i1j1];
    c[1]=cpy[i1j1];
    Call creatVoid;
  EndFor
EndFor

For i In {0:9}
  s=newreg; Line(s)={p[i], p[((i+1)%10)]};
  lboundary[i]=s;
  lloop[icar]=s; 
  icar+=1;
EndFor

s=newreg; Line(s)={p[1], p[8]};
linterface[0] = s;
s=newreg; Line(s)={p[2], p[7]};
linterface[1] = s;
s=newreg; Line(s)={p[3], p[6]};
linterface[2] = s;

cir[0]=newreg; Line Loop(cir[0])={lboundary[0],linterface[0],lboundary[8],lboundary[9]};
Plane Surface(10)={cir[0]};
cir[1]=newreg; Line Loop(cir[1])={lboundary[1],linterface[1],lboundary[7],-linterface[0]};
Plane Surface(11)={cir[1]};
//Plane Surface(12)={circlloop[]};
cir[3]=newreg; Line Loop(cir[3])={lboundary[2],linterface[2],lboundary[6],-linterface[1]};
Plane Surface(13)={cir[3],circlloop[]};
cir[4]=newreg; Line Loop(cir[4])={lboundary[3],lboundary[4],lboundary[5],-linterface[2]};
Plane Surface(14)={cir[4]};

Physical Line("BOUNDARY0") = {lboundary[0]};
Physical Line("BOUNDARY1") = {lboundary[1]};
Physical Line("BOUNDARY2") = {lboundary[2]};
Physical Line("BOUNDARY3") = {lboundary[3]};
Physical Line("BOUNDARY4") = {lboundary[4]};
Physical Line("BOUNDARY5") = {lboundary[5]};
Physical Line("BOUNDARY6") = {lboundary[6]};
Physical Line("BOUNDARY7") = {lboundary[7]};
Physical Line("BOUNDARY8") = {lboundary[8]};
Physical Line("BOUNDARY9") = {lboundary[9]};
Physical Line("INTERFACE0") = {linterface[0]};
//Physical Line("INTERFACE1") = {linterface[1]};
//Physical Line("INTERFACE2") = {linterface[2]};
//Physical Line("INTERFACEFIBERS") = {circlloop[]};

Physical Surface("PML")={10};
Physical Surface("EXT1")={11};
Physical Surface("FIBERS")={circsloop[]};
Physical Surface("MATRIX")={13};
Physical Surface("EXT2")={14};





