x0 = 0.35;
y0 = 0.35;
x1 = 0.258;
y1 = 0.208;
x2 = 0.25;
y2 = 0.20;

cl0 = 0.02;
cl1 = 0.001;


					
Point(1) = {-x0, -y0, 0, cl0};
Point(2) = {-x0,  y0, 0, cl0};
Point(3) = { x0,  y0, 0, cl0};
Point(4) = { x0, -y0, 0, cl0};



Point(5) = {-x1, -y1, 0, cl0};
Point(6) = {-x1,  y1, 0, cl0};
Point(7) = { x1,  y1, 0, cl0};
Point(8) = { x1, -y1, 0, cl0};

Point(9)  = {-x2, -y2, 0, cl0};
Point(10) = {-x2,  y2, 0, cl0};
Point(11) = { x2,  y2, 0, cl0};
Point(12) = { x2, -y2, 0, cl0};



Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};


Line(5) = {5, 6};
Line(6) = {6, 7};
Line(7) = {7, 8};
Line(8) = {8, 5};

Line(9)  = {9, 10};
Line(10) = {10, 11};
Line(11) = {11, 12};
Line(12) = {12, 9};

//l=1000;
//Transfinite Line (5)  = 2*y1*l;
//Transfinite Line (6)  = 2*x1*l;
//Transfinite Line (7)  = 2*y1*l;
//Transfinite Line (8)  = 2*x1*l;
//Transfinite Line (9)  = 2*y2*l;
//Transfinite Line (10) = 2*x2*l;
//Transfinite Line (11) = 2*y2*l;
//Transfinite Line (12) = 2*x2*l;



Line Loop(17) = {12, 9, 10, 11};
Plane Surface(107) = {17};

Line Loop(16) = {8, 5, 6, 7};
Plane Surface(106) = {16,-17};

Line Loop(15) = {4, 1, 2, 3};
Plane Surface(105) = {15,-16};



Physical Line("Left") = {1};
Physical Line("Right") = {3};
Physical Line("Top") = {2};
Physical Line("Bottom") = {4};






Physical Surface("EXT")={105};
Physical Surface("CAV")={106};
Physical Surface("INT")={107};

Field[1] = Attractor;
Field[1].EdgesList = {5,6,7,8,9,10,11,12};
Field[2] = Threshold;
Field[2].IField = 1;
Field[2].LcMin = cl1;
Field[2].LcMax = cl0;
Field[2].DistMin = 0.005;
Field[2].DistMax = 0.15;
Background Field = 2;



Mesh.CharacteristicLengthFromPoints = 1 ;
Mesh.CharacteristicLengthExtendFromBoundary = 0 ;

