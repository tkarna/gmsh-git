import cmath
from gmshpy import *
from dgpy import *
import os
import time
import math


print'---- Load Mesh and Build Groups'

model = GModel()
model.load('ClosedCavityShieldinterface.msh')

dim = 2
order = 1
groups = dgGroupCollection(model, dim, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()




mu0=4*math.pi*1e-7
eps0=8.85*1e-12
cValue0=1/math.sqrt(eps0*mu0)
cValue=cValue0
rhoValue = mu0
dt = 0.2e-11





coef = functionConstantByTag({"EXT"      :[cValue,       rhoValue,      0.,         0.],
                              "CAV"      :[cValue,       rhoValue,      0.,         0.]})


at1=[0.,0.,0.,0.,0.,0.,0.,0.,0.,0.]
at2=[0.,0.,0.,0.,0.,0.,0.,0.,0.,0.]
at3=[0.,0.,0.,0.,0.,0.,0.,0.,0.,0.]

as1=[0.,0.,0.,0.,0.,0.,0.,0.,0.,0.]
as2=[0.,0.,0.,0.,0.,0.,0.,0.,0.,0.]
as3=[0.,0.,0.,0.,0.,0.,0.,0.,0.,0.]
qt2=[0.,0.,0.,0.,0.,0.,0.,0.,0.,0.]
qs2=[0.,0.,0.,0.,0.,0.,0.,0.,0.,0.]
qt1=[0.,0.,0.,0.,0.,0.,0.,0.,0.,0.]
qs1=[0.,0.,0.,0.,0.,0.,0.,0.,0.,0.]


######################sigma=100 S/m###mu=mu_0####epsilon=epsilon_0###d=8mm##########################

qs1r=[1.0494720153772582e-06, -5.0098860053371600e-05, 2.3494313210107119e-03, -6.4178442245014489e-01, -3.0720281666019192e+09, 1.2344175540576893e+10, -2.9132237751836723e+10, 2.3576332445282749e+10, -1.8072049237807810e+09, -1.8072049237807810e+09]
qs1i=[0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, -1.2180416601961551e+09, 1.2180416601961551e+09]
qs2r=[-5.1974488116569688e+00, -1.0859052393435263e+02, -2.5776964223827581e+03, -6.2966867878054101e+04, -1.2277946616612020e+09, -4.9105562707177544e+09, -1.1012332013483122e+10, -1.6218735920208847e+10, -2.1149149338457085e+10, -2.1149149338457085e+10]
qs2i=[0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 1.3506662232678528e+10, -1.3506662232678528e+10]
Ds=-8.1309322527318279e-04


qt1r=[-4.0399675996736165e-05, 6.0952558462110645e-02, -6.3113031068984649e-01, 8.8712884627439088e+00, 2.9804893232067800e+09, 1.4341569264633251e+10, 6.3465148935048877e+11, 6.1628709587793555e+12, 4.4619497189356102e+10, 4.4619497189356102e+10]
qt1i=[0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, -4.2502937508945079e+09, 4.2502937508945079e+09]
qt2r=[-3.9158220563101347e+00, -6.3017588990697061e+02, -3.0700957107218892e+03, -1.8124633175328774e+04, -1.2179657614284132e+09, -4.9084487113152628e+09, -5.7492583319696602e+10, -2.2057533847285248e+11, -1.5948830411522106e+10, -1.5948830411522106e+10]
qt2i=[0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 0.0000000000000000e+00, 2.1866762804757943e+09, -2.1866762804757943e+09]
Dt=-5.1161939514252488e+01

for i in range(0,10):
  qt2[i]=complex(qt2r[i],qt2i[i])
  qs2[i]=complex(qs2r[i],qs2i[i])
  qt1[i]=complex(qt1r[i],qt1i[i])
  qs1[i]=complex(qs1r[i],qs1i[i])



for i in range(0,10):
   alpha=qt1[i]/qt2[i]
   beta=qt2[i]*dt
   at1[i]=cmath.exp(beta)
   at2[i]=alpha/beta*(1+(beta-1)*cmath.exp(beta))
   at3[i]=alpha/beta*(cmath.exp(beta)-beta-1)


for i in range(0,10):
   alpha=qs1[i]/qs2[i]
   beta=qs2[i]*dt
   as1[i]=cmath.exp(beta)
   as2[i]=alpha/beta*(1+(beta-1)*cmath.exp(beta))
   as3[i]=alpha/beta*(cmath.exp(beta)-beta-1)



print'---- Build Initial conditions'

initialConditionPython = functionConstant([0.,0.,0.,0.])


sol = dgDofContainer(groups,3)
sol.setFieldName(0,'Ez')
sol.setFieldName(1,'-Hy')
sol.setFieldName(2,'Hx')
sol.L2Projection(initialConditionPython)
sol.exportMsh("output/ClosedCavityShieldinterface_%05d" % (0), 0, 0)



etatr=[dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3)]
etasr=[dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3)]

etati=[dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3)]
etasi=[dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3)]


etatrr=[dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3)]
etasrr=[dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3)]

etatii=[dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3)]
etasii=[dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3),dgDofContainer(groups,3)]


prevsol = dgDofContainer(groups,3)
prevsol.scale(0.)

impt = dgDofContainer(groups,3)
imps = dgDofContainer(groups,3)

impt.scale(0.)
imps.scale(0.)



for i in range(0,10):
  etatr[i].scale(0.)
  etati[i].scale(0.)
  etasr[i].scale(0.)
  etasi[i].scale(0.)
  etatrr[i].scale(0.)
  etatii[i].scale(0.)
  etasrr[i].scale(0.)
  etasii[i].scale(0.)



print'---- Load Conservation law with coefficients'

law = dgConservationLawWave(dim)
law.setPhysCoef(coef)





print'---- Load Interface and Boundary conditions'

t=0
tho=5e-9
aa=math.log(100)*3e8*3e8/0.75/.75
def extFieldFunction(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    tt=t-tho
    value = math.exp(-aa*tt*tt)
    val.set(i,0,value)
    val.set(i,1,value/(cValue*rhoValue))
    val.set(i,2,0.)
extField = functionPython(3, extFieldFunction, [xyz])

law.addInterfaceShield('InterfaceR',impt.getFunction(),imps.getFunction())
law.addInterfaceShield('InterfaceL',impt.getFunction(),imps.getFunction())
law.addInterfaceShield('InterfaceT',impt.getFunction(),imps.getFunction())
law.addInterfaceShield('InterfaceB',impt.getFunction(),imps.getFunction())


law.addBoundaryCondition('Top',      law.newBoundaryHomogeneousDirichletOnU('Top'))
law.addBoundaryCondition('Bottom',      law.newBoundaryHomogeneousDirichletOnU('Bottom'))
law.addBoundaryCondition('Right',      law.newBoundaryOpenOutflow('Right'))
law.addBoundaryCondition('Left', law.newBoundaryOpenOutflowWithInflow('Left', extField))



incidField=fullMatrixDouble(1,1)


def psolShield(val,xyz, solution):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    s= solution.get(i,0)
    value  = s
    val.set(i,0,value)



print'---- LOOP'

tic = time.clock()
sys = linearSystemPETScBlockDouble()
#sys.setParameter("petscOptions", "-pc_type ilu")
#dofinc = dgDofManager.newDGBlock(groups, 3, sysinc)
#dofobc = dgDofManager.newDGBlock(groups, 3, sysobc)
dof = dgDofManager.newDGBlock(groups, 3, sys)
solver = dgDIRK(law,dof,2)
#solver = dgERK(law,None,DG_ERK_44)
#solver.getNewton().setVerb(10)


iterMax = 50000


print'---- LOOP'
for i in range(1,iterMax+1):
  t = t+dt
  print '|ITER|', i, 'cpu', time.clock() - tic
  prevsol.copy(sol)
  solver.iterate(sol,dt,t)
  if (i % 100 == 0):
    sol.exportMsh("output/ClosedCavityShieldinterface_%05d" % (i), t, i)
  psol = functionPython(1,psolShield,[xyz, sol.getFunction()])

  incidfield = dgFunctionEvaluator(groups, psol)

  incidfield.compute(0, 0.0,0.0, incidField)
  impt.scale(0.)
  imps.scale(0.)
  impt.axpy(sol,Dt)
  imps.axpy(sol,Ds)
  for ii in range(0,10):
        etatr[ii].copy(etatrr[ii])
  	etati[ii].copy(etatii[ii])
  	etasr[ii].copy(etasrr[ii])
  	etasi[ii].copy(etasii[ii])
  	etatrr[ii].scale(0.)
  	etatii[ii].scale(0.)
  	etasrr[ii].scale(0.)
  	etasii[ii].scale(0.)
  	etatrr[ii].axpy(etatr[ii],at1[ii].real)
  	etatrr[ii].axpy(etati[ii],-at1[ii].imag)
  	etatrr[ii].axpy(prevsol,at2[ii].real)
  	etatrr[ii].axpy(sol,at3[ii].real)
  	etatii[ii].axpy(etatr[ii],at1[ii].imag)
  	etatii[ii].axpy(etati[ii],at1[ii].real)
  	etatii[ii].axpy(prevsol,at2[ii].imag)
  	etatii[ii].axpy(sol,at3[ii].imag)
  	etasrr[ii].axpy(etasr[ii],as1[ii].real)
  	etasrr[ii].axpy(etasi[ii],-as1[ii].imag)
  	etasrr[ii].axpy(prevsol,as2[ii].real)
  	etasrr[ii].axpy(sol,as3[ii].real)
  	etasii[ii].axpy(etasr[ii],as1[ii].imag)
  	etasii[ii].axpy(etasi[ii],as1[ii].real)
  	etasii[ii].axpy(prevsol,as2[ii].imag)
  	etasii[ii].axpy(sol,as3[ii].imag)
  for ii in range(0,10):
        impt.axpy(etatrr[ii],1.)
        imps.axpy(etasrr[ii],1.)

  print("%.8e  %.8e \n"% (t,incidField(0,0)))
  fout_shield=open("ClosedCavityShieldinterface.dat","a")
  fout_shield.write("%.8e  %.8e \n"% (t, incidField(0,0)))
  fout_shield.close()
  law.addInterfaceShield('InterfaceR',impt.getFunction(),imps.getFunction())
  law.addInterfaceShield('InterfaceL',impt.getFunction(),imps.getFunction())
  law.addInterfaceShield('InterfaceT',impt.getFunction(),imps.getFunction())
  law.addInterfaceShield('InterfaceB',impt.getFunction(),imps.getFunction())



Msg.Exit(0)



