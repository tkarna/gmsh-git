from dgpy import *
import os
import time
import math


fichier = open("NumelecCount.dat", "r")
INDEXascii = fichier.readline()
INDEX = int(INDEXascii)
fichier.close()

fichier = open("NumelecCount.dat", "w")
fichier.write('%d' % (INDEX+1))
fichier.close()

if (INDEX>32) :
  Msg.Exit(0)

LREF = 0.5
ldom = 0.5/LREF
lpml = 0.1/LREF
lext = 0.5/LREF

L  = 0.16/LREF
x0 = 0.29/LREF
x1 = 0.30/LREF
y0 = 0.19/LREF
y1 = 0.20/LREF

ar = 0.1/LREF
ax = x1+ar
ay = y1+ar

betaList = [-4, -3.75, -3.5, -3.25, -3, -2.75, -2.5, -2.25,
            -2, -1.75, -1.5, -1.25, -1, -0.75, -0.5, -0.25,
             0,  0.25,  0.5,  0.75,  1,  1.25,  1.5,  1.75,
             2,  2.25,  2.5,  2.75,  3,  3.25,  3.5,  3.75, 4]
alphaNorm = math.pow(10,betaList[INDEX])

print'---- Load Mesh and Build Groups'

model = GModel()
model.load('CavityInCuboid.msh')

dim = 2
order = 2
groups = dgGroupCollection(model, dim, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()

print'---- Build Initial conditions'

initialConditionPython = functionConstant([0.,0.,0.,0.])

solutioninc = dgDofContainer(groups,3)
solutioninc.setFieldName(0,'Ez_inc')
solutioninc.setFieldName(1,'-Hy_inc')
solutioninc.setFieldName(2,'Hx_inc')

solutionref = dgDofContainer(groups,3)
solutionref.setFieldName(0,'Ez_ref')
solutionref.setFieldName(1,'-Hy_ref')
solutionref.setFieldName(2,'Hx_ref')

solutionpmlA = dgDofContainer(groups,4)
solutionpmlA.setFieldName(0,'Ez_pmlA')
solutionpmlA.setFieldName(1,'-Hy_pmlA')
solutionpmlA.setFieldName(2,'Hx_pmlA')
solutionpmlA.setFieldName(3,'Q_pmlA')

solutionpmlB = dgDofContainer(groups,4)
solutionpmlB.setFieldName(0,'Ez_pmlB')
solutionpmlB.setFieldName(1,'-Hy_pmlB')
solutionpmlB.setFieldName(2,'Hx_pmlB')
solutionpmlB.setFieldName(3,'Q_pmlB')

solutionpmlC = dgDofContainer(groups,4)
solutionpmlC.setFieldName(0,'Ez_pmlC')
solutionpmlC.setFieldName(1,'-Hy_pmlC')
solutionpmlC.setFieldName(2,'Hx_pmlC')
solutionpmlC.setFieldName(3,'Q_pmlC')

solutionpmlD = dgDofContainer(groups,4)
solutionpmlD.setFieldName(0,'Ez_pmlD')
solutionpmlD.setFieldName(1,'-Hy_pmlD')
solutionpmlD.setFieldName(2,'Hx_pmlD')
solutionpmlD.setFieldName(3,'Q_pmlD')

solutioninc.L2Projection(initialConditionPython)
solutionref.L2Projection(initialConditionPython)
solutionpmlA.L2Projection(initialConditionPython)
solutionpmlB.L2Projection(initialConditionPython)
solutionpmlC.L2Projection(initialConditionPython)
solutionpmlD.L2Projection(initialConditionPython)

print'---- Load Conservation law with coefficients'

lawinc = dgConservationLawWave(dim)
lawref = dgConservationLawWave(dim)
lawpmlA = dgConservationLawWave(dim,'PmlBeveledCuboid')
lawpmlB = dgConservationLawWave(dim,'PmlBeveledCuboid')
lawpmlC = dgConservationLawWave(dim,'PmlBeveledCuboid')
lawpmlD = dgConservationLawWave(dim,'PmlBeveledCuboid')

mu0  = 12.566370614 * pow(10,-7)
eps0 = 8.85 * pow(10,-12)
c0   = 1/math.sqrt(mu0*eps0)

muValue  = 1.
epsValue = 1.
cValue   = 1/math.sqrt(muValue*epsValue)  # 1/Sqrt(Mu*Epsilon)
rhoValue = muValue                        # Mu
coef = functionConstant([cValue,rhoValue])

lawinc.setPhysCoef(coef)
lawref.setPhysCoef(coef)
lawpmlA.setPhysCoef(coef)
lawpmlB.setPhysCoef(coef)
lawpmlC.setPhysCoef(coef)
lawpmlD.setPhysCoef(coef)

dec = 0.
lawinc.setFluxDecentering(dec)
lawref.setFluxDecentering(dec)
lawpmlA.setFluxDecentering(dec)
lawpmlB.setFluxDecentering(dec)
lawpmlC.setFluxDecentering(dec)
lawpmlD.setFluxDecentering(dec)

print'---- Load PML'
  
lawpmlA.useGenericPmlDir('BeveledCuboid', 0,0,0, ax,ay,0, ar)
lawpmlB.useGenericPmlDir('BeveledCuboid', 0,0,0, ax,ay,0, ar)
lawpmlC.useGenericPmlDir('BeveledCuboid', 0,0,0, ax,ay,0, ar)
lawpmlD.useGenericPmlDir('BeveledCuboid', 0,0,0, ax,ay,0, ar)

lawpmlA.useGenericPmlCoef('BeveledCuboid',lpml,cValue, 0,0,0, ax,ay,0, ar, 'Poly2', alphaNorm)
lawpmlB.useGenericPmlCoef('BeveledCuboid',lpml,cValue, 0,0,0, ax,ay,0, ar, 'Poly3', alphaNorm)
lawpmlC.useGenericPmlCoef('BeveledCuboid',lpml*1.01,cValue, 0,0,0, ax,ay,0, ar, 'Hyp'  , alphaNorm)
lawpmlD.useGenericPmlCoef('BeveledCuboid',lpml*1.01,cValue, 0,0,0, ax,ay,0, ar, 'HypSh', alphaNorm)

lawpmlA.addPml('PML')
lawpmlB.addPml('PML')
lawpmlC.addPml('PML')
lawpmlD.addPml('PML')

print'---- Load Interface and Boundary conditions'

t=0
def extFieldFunction(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    value = math.exp(-math.pow((x+ldom+lext+0.2-cValue*t),2)*100.)
    val.set(i,0,value)
extField = functionPython(1, extFieldFunction, [xyz])

lawref.addInterfaceHomogeneousDirichletOnP('BOUNDARY_SCATT')
lawref.addInterfaceScatteredTotalFields('INTERFACE', solutioninc.getFunction(),lawpmlA.getPmlDir())
lawref.addBoundaryCondition('BOUNDARY_EXT',      lawref.newBoundaryWall('BOUNDARY_EXT'))
lawref.addBoundaryCondition('BOUNDARY_EXT_LEFT', lawref.newBoundaryWall('BOUNDARY_EXT_LEFT'))

lawinc.addBoundaryCondition('BOUNDARY_EXT',      lawinc.newBoundaryWall('BOUNDARY_EXT'))
lawinc.addBoundaryCondition('BOUNDARY_EXT_LEFT', lawinc.newBoundaryDirichletOnP('BOUNDARY_EXT_LEFT',extField))

lawpmlA.addInterfaceHomogeneousDirichletOnP('BOUNDARY_SCATT')
lawpmlB.addInterfaceHomogeneousDirichletOnP('BOUNDARY_SCATT')
lawpmlC.addInterfaceHomogeneousDirichletOnP('BOUNDARY_SCATT')
lawpmlD.addInterfaceHomogeneousDirichletOnP('BOUNDARY_SCATT')

lawpmlA.addInterfacePmlWithInflow('INTERFACE', solutioninc.getFunction())
lawpmlB.addInterfacePmlWithInflow('INTERFACE', solutioninc.getFunction())
lawpmlC.addInterfacePmlWithInflow('INTERFACE', solutioninc.getFunction())
lawpmlD.addInterfacePmlWithInflow('INTERFACE', solutioninc.getFunction())

lawpmlA.addInterfaceHomogeneousDirichletOnP('BOUNDARY_PML')
lawpmlB.addInterfaceHomogeneousDirichletOnP('BOUNDARY_PML')
lawpmlC.addInterfaceHomogeneousDirichletOnP('BOUNDARY_PML')
lawpmlD.addInterfaceHomogeneousDirichletOnP('BOUNDARY_PML')

lawpmlA.addBoundaryCondition('BOUNDARY_EXT', lawpmlA.newBoundaryWall('BOUNDARY_EXT'))
lawpmlB.addBoundaryCondition('BOUNDARY_EXT', lawpmlB.newBoundaryWall('BOUNDARY_EXT'))
lawpmlC.addBoundaryCondition('BOUNDARY_EXT', lawpmlC.newBoundaryWall('BOUNDARY_EXT'))
lawpmlD.addBoundaryCondition('BOUNDARY_EXT', lawpmlD.newBoundaryWall('BOUNDARY_EXT'))

lawpmlA.addBoundaryCondition('BOUNDARY_EXT_LEFT', lawpmlA.newBoundaryWall('BOUNDARY_EXT_LEFT'))
lawpmlB.addBoundaryCondition('BOUNDARY_EXT_LEFT', lawpmlB.newBoundaryWall('BOUNDARY_EXT_LEFT'))
lawpmlC.addBoundaryCondition('BOUNDARY_EXT_LEFT', lawpmlC.newBoundaryWall('BOUNDARY_EXT_LEFT'))
lawpmlD.addBoundaryCondition('BOUNDARY_EXT_LEFT', lawpmlD.newBoundaryWall('BOUNDARY_EXT_LEFT'))

print'---- LOOP'

sysinc = linearSystemPETScBlockDouble()
sysref = linearSystemPETScBlockDouble()
syspmlA = linearSystemPETScBlockDouble()
syspmlB = linearSystemPETScBlockDouble()
syspmlC = linearSystemPETScBlockDouble()
syspmlD = linearSystemPETScBlockDouble()

sysinc.setParameter("petscOptions", "-pc_type ilu")
sysref.setParameter("petscOptions", "-pc_type ilu")
syspmlA.setParameter("petscOptions", "-pc_type ilu")
syspmlB.setParameter("petscOptions", "-pc_type ilu")
syspmlC.setParameter("petscOptions", "-pc_type ilu")
syspmlD.setParameter("petscOptions", "-pc_type ilu")

dofinc = dgDofManager.newDGBlock(groups, 3, sysinc)
dofref = dgDofManager.newDGBlock(groups, 3, sysref)
dofpmlA = dgDofManager.newDGBlock(groups, 4, syspmlA)
dofpmlB = dgDofManager.newDGBlock(groups, 4, syspmlB)
dofpmlC = dgDofManager.newDGBlock(groups, 4, syspmlC)
dofpmlD = dgDofManager.newDGBlock(groups, 4, syspmlD)

solverinc = dgCrankNicholson(solutioninc, lawinc, dofinc, 0.)
solverref = dgCrankNicholson(solutionref, lawref, dofref, 0.)
solverpmlA = dgCrankNicholson(solutionpmlA, lawpmlA, dofpmlA, 0.)
solverpmlB = dgCrankNicholson(solutionpmlB, lawpmlB, dofpmlB, 0.)
solverpmlC = dgCrankNicholson(solutionpmlC, lawpmlC, dofpmlC, 0.)
solverpmlD = dgCrankNicholson(solutionpmlD, lawpmlD, dofpmlD, 0.)

solverinc.setComputeExplicitTerm(True)
solverref.setComputeExplicitTerm(True)
solverpmlA.setComputeExplicitTerm(True)
solverpmlB.setComputeExplicitTerm(True)
solverpmlC.setComputeExplicitTerm(True)
solverpmlD.setComputeExplicitTerm(True)

dt = 1.5*pow(10,-2)

for i in range(1,250+1):
  t = i*dt
  
  x = time.clock()
  solverinc.iterate(t)
  print time.clock()-x
  x = time.clock()
  solverref.iterate(t)
  print time.clock()-x
  x = time.clock() 
  solverpmlA.iterate(t)
  print time.clock()-x
  x = time.clock() 
  solverpmlB.iterate(t)
  print time.clock()-x
  x = time.clock() 
  solverpmlC.iterate(t)
  print time.clock()-x
  x = time.clock()
  solverpmlD.iterate(t)
  print time.clock()-x
  
  energyref = lawref.getTotalEnergy(groups,solutionref,'DOM')
  energyerrorpmlA = lawpmlA.getTotalEnergyError(groups,solutionpmlA,solutionref,'DOM')
  energyerrorpmlB = lawpmlB.getTotalEnergyError(groups,solutionpmlB,solutionref,'DOM')
  energyerrorpmlC = lawpmlC.getTotalEnergyError(groups,solutionpmlC,solutionref,'DOM')
  energyerrorpmlD = lawpmlD.getTotalEnergyError(groups,solutionpmlD,solutionref,'DOM')
  
  print '|INDEX|', INDEX, '|ITER|', i, '|REF|', energyref, '|A|', energyerrorpmlA, '|B|', energyerrorpmlB, '|C|', energyerrorpmlC, '|D|', energyerrorpmlD
  
  fichier = open('outputDec0_o2/Resu_%05d.txt' % (INDEX), "a")
  fichier.write('%d %d %e %e %e %e %e\n' % (INDEX, i, energyref, energyerrorpmlA, energyerrorpmlB, energyerrorpmlC, energyerrorpmlD))
  fichier.close()

Msg.Exit(0)


