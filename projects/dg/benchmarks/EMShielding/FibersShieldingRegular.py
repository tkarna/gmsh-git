from gmshpy import *
from dgpy import *
import os
import time
import math


mm = 0.5;
r  = 0.15;
m  = 3;
n  = 6;

Ltickness = (m)*mm
Lheight   = (n)*mm
Lpml  = Lheight/5
Lext  = Lheight/2


print'Mesh, Geometry & Groups'

model = GModel()
model.load('FibersShieldingRegular.msh')

dim = 2
order = 1
groups = dgGroupCollection(model, dim, order)
groups.splitGroupsByPhysicalTag()
groups.buildGroupsOfInterfaces()
xyz = groups.getFunctionCoordinates()


print'Conservation law'

cValue        = 340.
rhoValue      = 1.293
sigmaValue    = 0.
cValue_incl   = 120
rhoValue_incl = 10*1.293
cValue_mat    = 500
rhoValue_mat  = 2*1.293

coef = functionConstant([cValue, rhoValue, sigmaValue])
coef = functionConstantByTag({"PML_LEFT"  :[cValue,      rhoValue,      sigmaValue],
                              "PML_RIGHT" :[cValue,      rhoValue,      sigmaValue],
                              "FIBERS"    :[cValue_incl, rhoValue_incl, sigmaValue],
                              "MATRIX"    :[cValue_mat,  rhoValue_mat,  sigmaValue]})

PmlVersion = 1
law = dgConservationLawWave(2,PmlVersion)
law.setPhysCoef(coef)
law.useDissipationOnP()


print'Initial solutions'

r=Ltickness/5
x0 = -Ltickness

def initialCondition(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    #x0=-Ltickness/2-Lpml/2
    #y0=0
    #r=1
    #value = math.exp(-5*((x-x0)*(x-x0)+(y-y0)*(y-y0))/(r*r))
    value = math.exp(-((x-x0)*(x-x0))/(r*r))
    val.set(i,0,value)
    val.set(i,1,value/(cValue*rhoValue))
    val.set(i,2,0.)
    val.set(i,3,0.)
    val.set(i,4,0.)
initialConditionPython = functionPython(5,initialCondition,[xyz])

sol = dgDofContainer(groups,5)
sol.setFieldName(0,'p')
sol.setFieldName(1,'u')
sol.setFieldName(2,'v')
sol.setFieldName(3,'qp')
sol.setFieldName(4,'qu')
sol.L2Projection(initialConditionPython)
sol.exportMsh('output/FibersShieldingRegular_00000',0,0)


print'External forcing'

t=0.
def extFields(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    value = math.exp(-((x-cValue*t-x0)*(x-cValue*t-x0))/(r*r))
    val.set(i,0,value)
    val.set(i,1,value/(cValue*rhoValue))
    val.set(i,2,0.)
extFieldsPython = functionPython(3,extFields,[xyz])

def extFieldsU(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    value = math.exp(-((x-cValue*t-x0)*(x-cValue*t-x0))/(r*r))
    val.set(i,0,value/(cValue*rhoValue))
extFieldsUPython = functionPython(1,extFieldsU,[xyz])


print'Build PML'

def absCoefDef(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    if (x>0):
      xloc = math.fabs(x)-Ltickness/2.
      sigmaPml = cValue/(Lext*1.01) * (xloc)/((Lext*1.01)-xloc)
    else:
      xloc = math.fabs(x)-Ltickness/2.
      sigmaPml = cValue/(Lpml*1.01) * (xloc)/((Lpml*1.01)-xloc)
    val.set(i,0,sigmaPml)
absCoef = functionPython(1,absCoefDef,[xyz])

absDir = functionConstantByTag({"PML_LEFT"  :[-1.,0.,0.],
                                "PML_RIGHT" :[ 1.,0.,0.],
                                "FIBERS"    :[ 0.,0.,0.],
                                "MATRIX"    :[ 0.,0.,0.]})

law.setAbsCoef(absCoef)
law.setAbsDir(absDir)

law.addPml('PML_LEFT',extFieldsPython)
law.addInterfacePml('INTERFACE_LEFT',extFieldsPython)
law.addPml('PML_RIGHT')
law.addInterfacePml('INTERFACE_RIGHT')

absCoefSol = dgDofContainer(groups,1)
absCoefSol.interpolate(absCoef)
absCoefSol.exportMsh('output/FibersShieldingRegular_AbsCoef',0,0)


print'Boundary conditions'

law.addBoundaryCondition('BOUNDARY0', law.newBoundaryWall('BOUNDARY0'))
law.addBoundaryCondition('BOUNDARY1', law.newBoundaryWall('BOUNDARY1'))
law.addBoundaryCondition('BOUNDARY2', law.newBoundaryWall('BOUNDARY2'))
law.addBoundaryCondition('BOUNDARY3', law.newBoundaryWall('BOUNDARY3'))
law.addBoundaryCondition('BOUNDARY4', law.newBoundaryWall('BOUNDARY4'))
law.addBoundaryCondition('BOUNDARY5', law.newBoundaryWall('BOUNDARY5'))
law.addBoundaryCondition('BOUNDARY6', law.newBoundaryWall('BOUNDARY6'))

#law.addBoundaryCondition('BOUNDARY7', law.newBoundaryWall('BOUNDARY7'))
#law.addBoundaryCondition('BOUNDARY7', law.newBoundaryDirichletOnP('BOUNDARY7', extFieldsPython))
law.addBoundaryCondition('BOUNDARY7', law.newBoundaryDirichletOnU('BOUNDARY7', extFieldsUPython))
#law.addBoundaryCondition('BOUNDARY7', law.newOutsideValueBoundary('BOUNDARY7', extFieldsPython))


#print'DEF: Compute flux'

#def computeEnergyFlux(surface, time):
#  fluxDir = [1., 0., 0.]
#  integ=dgFunctionIntegratorInterface(groups, law.getTotalEnergyFluxSurface(), solution)
#  energyFlux=fullMatrixDouble(3,1)
#  integ.compute(surface, energyFlux)
#  energyFluxTot = 0.
#  for j in range(0,3):
#    energyFluxTot = energyFluxTot + energyFlux.get(j,0)*fluxDir[j]
#  fun = open("FibersShieldingRegular_Flux.dat","a")
#  print >>fun, time, ' ', energyFluxTot
#  fun.close()
#  print "energyFluxTot: %f" %(energyFluxTot)


print'Loop'

implicit = True;

if (implicit):
  dt = 0.0001
  sys = linearSystemPETScBlockDouble()
  #sys.setParameter("petscOptions", "-pc_type lu")
  #sys.setParameter("petscOptions", "-ksp_type bicg")
  dof = dgDofManager.newDGBlock(groups, 5, sys)
  solver=dgDIRK(law,dof,2)
  #solver.getNewton().setMaxit(5000)
  #solver.getNewton().setAtol(1e-8)
  #solver.getNewton().setRtol(1e-6)
  #solver.getNewton().setVerb(10)
  for i in range(1,100):
    t = i*dt
    solver.iterate(sol,dt,t)
    print'|ITER|', i, '|DT|', dt, '|t|', t
    sol.exportMsh("output/FibersShieldingRegular_%05i" % (i), t, i)
else:
  dt = 0.00001
  nbExport = 1
  solver = dgERK(law, None, DG_ERK_44)
  for i in range(0,50000):
    t = i*dt
    solver.iterate(sol, dt, t);
    norm = sol.norm()
    if (math.isnan(norm)):
      print 'ERROR : NAN'
      break
    if (i % 1 == 0):
      print'|ITER|', i, '|NORM|', norm, '|DT|', dt, '|t|', t
      sol.exportMsh("output/FibersShielding_%05i" % (nbExport), t, nbExport)
      nbExport  = nbExport + 1
      




Msg.Exit(0)

