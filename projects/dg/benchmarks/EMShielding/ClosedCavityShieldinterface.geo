x0 = 0.35;
y0 = 0.35;
x1 = 0.25;
y1 = 0.20;

cl0 = 0.02;
cl1 = 0.02;


					
Point(1) = {-x0, -y0, 0, cl0};
Point(2) = {-x0,  y0, 0, cl0};
Point(3) = { x0,  y0, 0, cl0};
Point(4) = { x0, -y0, 0, cl0};



Point(5) = {-x1, -y1, 0, cl0};
Point(6) = {-x1,  y1, 0, cl0};
Point(7) = { x1,  y1, 0, cl0};
Point(8) = { x1, -y1, 0, cl0};


Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};


Line(5) = {5, 6};
Line(6) = {6, 7};
Line(7) = {7, 8};
Line(8) = {8, 5};

Line Loop(16) = {8, 5, 6, 7};
Plane Surface(106) = {16};

Line Loop(15) = {4, 1, 2, 3};
Plane Surface(105) = {15,-16};



Physical Line("Left") = {1};
Physical Line("Right") = {3};
Physical Line("Top") = {2};
Physical Line("Bottom") = {4};


Physical Line("InterfaceL") = {5};
Physical Line("InterfaceR") = {7};
Physical Line("InterfaceT") = {6};
Physical Line("InterfaceB") = {8};



Physical Surface("EXT")={105};
Physical Surface("CAV")={106};

