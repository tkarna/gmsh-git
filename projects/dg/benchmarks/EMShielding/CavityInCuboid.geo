
LREF = 0.5;
ldom = 0.5/LREF;
lpml = 0.1/LREF;
lext = 0.5/LREF;

L  = 0.16/LREF;
x0 = 0.29/LREF;
x1 = 0.3/LREF;
y0 = 0.19/LREF;
y1 = 0.20/LREF;

ar = 0.2/LREF;
ax = x1+ar;
ay = y1+ar;

lc = 0.03/LREF;

lcScatExt = lc;
lcScatInt = lc;
lcPmlExt = lc;
lcPmlInt = lc;
lcExt = 1.5*lc;
/*
lcScatExt = lc/1.5;
lcScatInt = lc/1.5;
lcPmlExt = lc/1.5;
lcPmlInt = lc/1.5;
lcExt = lc/1.5;
*/
// Cavity

pt_cav[0]=newp;  Point(pt_cav[0])  = { -x1, -L/2, 0, lcScatInt};
pt_cav[1]=newp;  Point(pt_cav[1])  = { -x1,  -y1, 0, lcScatExt};
pt_cav[2]=newp;  Point(pt_cav[2])  = {  x1,  -y1, 0, lcScatExt};
pt_cav[3]=newp;  Point(pt_cav[3])  = {  x1,   y1, 0, lcScatExt};
pt_cav[4]=newp;  Point(pt_cav[4])  = { -x1,   y1, 0, lcScatExt};
pt_cav[5]=newp;  Point(pt_cav[5])  = { -x1,  L/2, 0, lcScatInt};
pt_cav[6]=newp;  Point(pt_cav[6])  = { -x0,  L/2, 0, lcScatInt};
pt_cav[7]=newp;  Point(pt_cav[7])  = { -x0,   y0, 0, lcScatInt};
pt_cav[8]=newp;  Point(pt_cav[8])  = {  x0,   y0, 0, lcScatInt};
pt_cav[9]=newp;  Point(pt_cav[9])  = {  x0,  -y0, 0, lcScatInt};
pt_cav[10]=newp; Point(pt_cav[10]) = { -x0,  -y0, 0, lcScatInt};
pt_cav[11]=newp; Point(pt_cav[11]) = { -x0, -L/2, 0, lcScatInt};

For i In {0:11}
  ln_cav[i]=newl; Line(ln_cav[i])={pt_cav[i], pt_cav[(i+1)%12]};
EndFor
ll_cav=newll; Line Loop(ll_cav) = {ln_cav[]};



// PML region

pt_pmlC[0]=newp; Point(pt_pmlC[0]) = { ax-ar, ay-ar, 0, lcScatExt};
pt_pmlC[1]=newp; Point(pt_pmlC[1]) = { ax-ar,-ay+ar, 0, lcScatExt};
pt_pmlC[2]=newp; Point(pt_pmlC[2]) = {-ax+ar,-ay+ar, 0, lcScatExt};
pt_pmlC[3]=newp; Point(pt_pmlC[3]) = {-ax+ar, ay-ar, 0, lcScatExt};

pt_pmlI[0]=newp; Point(pt_pmlI[0]) = { ax-ar, ay   , 0, lcPmlInt};
pt_pmlI[1]=newp; Point(pt_pmlI[1]) = { ax   , ay-ar, 0, lcPmlInt};
pt_pmlI[2]=newp; Point(pt_pmlI[2]) = { ax   ,-ay+ar, 0, lcPmlInt};
pt_pmlI[3]=newp; Point(pt_pmlI[3]) = { ax-ar,-ay   , 0, lcPmlInt};
pt_pmlI[4]=newp; Point(pt_pmlI[4]) = {-ax+ar,-ay   , 0, lcPmlInt};
pt_pmlI[5]=newp; Point(pt_pmlI[5]) = {-ax   ,-ay+ar, 0, lcScatInt};
pt_pmlI[6]=newp; Point(pt_pmlI[6]) = {-ax   , ay-ar, 0, lcPmlInt};
pt_pmlI[7]=newp; Point(pt_pmlI[7]) = {-ax+ar, ay   , 0, lcPmlInt};

pt_pmlE[0]=newp; Point(pt_pmlE[0]) = { ax-ar  , ay+lpml, 0, lcPmlExt};
pt_pmlE[1]=newp; Point(pt_pmlE[1]) = { ax+lpml, ay-ar  , 0, lcPmlExt};
pt_pmlE[2]=newp; Point(pt_pmlE[2]) = { ax+lpml,-ay+ar  , 0, lcPmlExt};
pt_pmlE[3]=newp; Point(pt_pmlE[3]) = { ax-ar  ,-ay-lpml, 0, lcPmlExt};
pt_pmlE[4]=newp; Point(pt_pmlE[4]) = {-ax+ar  ,-ay-lpml, 0, lcPmlExt};
pt_pmlE[5]=newp; Point(pt_pmlE[5]) = {-ax-lpml,-ay+ar  , 0, lcPmlExt};
pt_pmlE[6]=newp; Point(pt_pmlE[6]) = {-ax-lpml, ay-ar  , 0, lcPmlExt};
pt_pmlE[7]=newp; Point(pt_pmlE[7]) = {-ax+ar  , ay+lpml, 0, lcPmlExt};

For i In {0:3}
  j = 2*i;
  ln_pmlI[2*i]=newl; Circle(ln_pmlI[2*i]) = {pt_pmlI[2*i],pt_pmlC[i],pt_pmlI[2*i+1]};
  ln_pmlE[2*i]=newl; Circle(ln_pmlE[2*i]) = {pt_pmlE[2*i],pt_pmlC[i],pt_pmlE[2*i+1]};
  ln_pmlI[2*i+1]=newl; Line(ln_pmlI[2*i+1]) = {pt_pmlI[2*i+1],pt_pmlI[(2*i+2)%8]};
  ln_pmlE[2*i+1]=newl; Line(ln_pmlE[2*i+1]) = {pt_pmlE[2*i+1],pt_pmlE[(2*i+2)%8]};
EndFor

For i In {0:7}
  ln_pmlS[i]=newl; Line(ln_pmlS[i]) = {pt_pmlI[i],pt_pmlE[i]};
EndFor

For i In {0:7}
  ll_pml[i]=newll; Line Loop(ll_pml[i]) = {-ln_pmlS[i],ln_pmlI[i],ln_pmlS[(i+1)%8],-ln_pmlE[i]};
EndFor

ll_pmlI=newll; Line Loop(ll_pmlI) = {ln_pmlI[]};
ll_pmlE=newll; Line Loop(ll_pmlE) = {ln_pmlE[]};



// Extended domain

pt_ext[0]=newp; Point(pt_ext[0]) = {  ldom+lext,  ldom+lext, 0., lcExt};
pt_ext[1]=newp; Point(pt_ext[1]) = { -ldom-lext,  ldom+lext, 0., lcExt};
pt_ext[2]=newp; Point(pt_ext[2]) = { -ldom-lext, -ldom-lext, 0., lcExt};
pt_ext[3]=newp; Point(pt_ext[3]) = {  ldom+lext, -ldom-lext, 0., lcExt};

For i In {0:3}
  ln_ext[i]=newl; Line(ln_ext[i])={pt_ext[i], pt_ext[(i+1)%4]};
EndFor
ll_ext=newll; Line Loop(ll_ext) = {ln_ext[]};



// Surface and Physical groups

s_cav=news; Plane Surface(s_cav) = {ll_cav};             // SCAT
s_dom=news; Plane Surface(s_dom) = {ll_cav, ll_pmlI};    // DOM
s_ext=news; Plane Surface(s_ext) = {-ll_pmlE, -ll_ext};  // EXT
For i In {0:7}
  s_pml[i]=news; Plane Surface(s_pml[i]) = {ll_pml[i]};  // PML
EndFor

Physical Line("BOUNDARY_SCATT")    = {ln_cav[]};
Physical Line("INTERFACE")         = {ln_pmlI[]};
Physical Line("BOUNDARY_PML")      = {ln_pmlE[]};
Physical Line("BOUNDARY_EXT")      = {ln_ext[2],ln_ext[3],ln_ext[0]};
Physical Line("BOUNDARY_EXT_LEFT") = {ln_ext[1]};

Physical Surface("DOM") = {s_dom};
Physical Surface("PML") = {s_pml[]};
Physical Surface("EXT") = {s_ext};
Physical Surface("SCATT") = {s_cav};

