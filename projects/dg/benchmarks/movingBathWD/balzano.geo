Point(1) = {0, -3.6e3, 0};
Point(2) = {1.38e4, -3.6e3, 0};
Point(3) = {1.38e4, 3.6e3, 0};
Point(4) = {0, 3.6e3, 0};
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line Loop(5) = {3, 4, 1, 2};

Field[1] = MathEval;
Field[1].F = "1200";
Background Field = 1;

Plane Surface(6) = {5};
Physical Surface("Surface")={6};
Physical Line("Wall") = {1,4,3};
Physical Line("OpenBnd") = {2};
