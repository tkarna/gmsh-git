from dgpy import *
from termcolor import colored
from math import *
import time,  os 

order = 1
dimension = 2
clscale = 0.1


os.system("rm -f output/*")
model = GModel()
if(not os.path.exists("balzano-%d.msh"%(order))):
  m = GModel()
  m.load("balzano.geo")
  GmshSetOption('Mesh','CharacteristicLengthFactor',   clscale)
  GmshSetOption('Mesh','CharacteristicLengthExtendFromBoundary',   1)
  GmshSetOption('Mesh','Remove4Triangles',   1)
  GmshSetOption('Mesh','Algorithm', "frontal")
  m.setOrderN(order, False, False)
  m.mesh(dimension)
  m.save("balzano-%d.msh"%(order))
model.load ('balzano-%d.msh'%(order))
time=0
timeFunction = functionConstant(time)

try : os.mkdir("output");
except: 0;

# Python Functions
def printTime (t) :
  return "%dh%02d'%02d\"" % (t/3600,  (t%3600)/60,  t%60)

CCode = """
#include "fullMatrix.h"
#include "function.h"
extern "C" {
  void bottomDrag(dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &sol, fullMatrix<double> &bath,fullMatrix<double> &manning){
    for(int i = 0;  i < val.size1(); i++){
      double H = sol(i, 0) + bath(i, 0);
      val.set(i, 0, 9.81*manning(i,0)*manning(i,0)/(pow(H, 1.333333333333)));
    }
  }
  void bathymetry(dataCacheMap *, fullMatrix<double> &bath, fullMatrix<double> &XYZ){
    for (size_t i = 0; i< bath.size1(); i++){
      double x=XYZ(i,0);
      //bath.set(i,0, 5.0 * x / 13800.0);
      bath.set(i,0, 5-4*exp(-0.5*(x-6.9e3)*(x-6.9e3)/(1e3*1e3)));
    }
  }
  void sinusTide(dataCacheMap *m, fullMatrix<double> &val, fullMatrix<double> &sol, fullMatrix<double> &time){
    for (size_t i = 0; i< val.size1(); i++) {
      val.set(i,0, -4*sin(2*M_PI*time(i,0)/(12*3600)));
      val.set(i,1, sol(i,1));
      val.set(i,2, 0.0);
    }
  }
}
"""
tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CCode, tmpLib);
Msg.Barrier()

groups = dgGroupCollection(model, dimension, order)
claw = dgConservationLawShallowWater2d()
solution = dgDofContainer(groups, claw.getNbFields())
XYZ = groups.getFunctionCoordinates();

#bathymetry
bathDof = dgDofContainer(groups, 1);
bathFunction = functionC(tmpLib, "bathymetry", 1, [XYZ])
bathDof.interpolate(bathFunction)
claw.setBathymetry(bathDof.getFunction())
bathGradient = bathDof.getFunctionGradient()
claw.setBathymetryGradient(bathGradient)
claw.setMovingBathWettingDrying(0.3)
nu = functionConstant(0.)
claw.setDiffusivity(nu)
claw.setUpwindFactorRiemann(1.)

#friction
manning = functionConstant(0.02)
Cd = functionC(tmpLib, "bottomDrag", 1, [function.getSolution(), bathDof.getFunction(), manning])
claw.setQuadraticDissipation(Cd)

# initial conditions
initialSolution = functionConstant([0.,0.,0.])
solution.interpolate(initialSolution)
# boundary conditions
bndWall = claw.newBoundaryWall()
claw.addBoundaryCondition("Wall", bndWall)
sinusT = functionC(tmpLib,"sinusTide", 3, [solution.getFunction(),timeFunction])
claw.addBoundaryCondition("OpenBnd",claw.newOutsideValueBoundary("",sinusT))

sys = linearSystemPETScBlockDouble()
sys.setParameter("petscOptions",  "-pc_type lu")
dof = dgDofManager.newDGBlock(groups, claw.getNbFields(), sys)
implicitTs=True
if(implicitTs):
  solver = dgDIRK(claw,dof,2)
  solver.getNewton().setVerb(1)
  solver.getNewton().setAtol(1.e-8)
  solver.getNewton().setRtol(1.e-8)
else:
  solver = dgERK(claw,dof,DG_ERK_22)

bath = claw.getBathymetry()
depth = claw.getDepth()
dt = 600
iExport = 0
while (time < 2*12*3600 ):
  if(time%1200==0 or iExport==0):
    solution.exportMsh("output/sol-%05i"%iExport,time,iExport,"sol")
    solution.exportFunctionMsh(depth,"output/depth-%05i"%iExport,time,iExport,"depth")
    solution.exportFunctionMsh(bath,"output/movingBath-%05i"%iExport,time,iExport,"movingBath")
    solution.exportFunctionMsh(bathFunction,"output/bath-%05i"%iExport,time,iExport,"bath")
    print(colored("export %d:%.1f norm:%.3e"%(iExport,time/3600,solution.norm()), "blue"))
    iExport = iExport+1
  solver.iterate (solution, dt, time)
  time = time + dt
  timeFunction.set(time)

Msg.Exit(0)

