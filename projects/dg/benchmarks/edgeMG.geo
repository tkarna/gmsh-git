
Point(1) = {-1, 0, 0};
Point(2) = {1, 0, 0};
Line(1) = {1, 2};

Transfinite Line (1) = nbElem;


Physical Point("Left") = {1};
Physical Point("Right") = {2};
Physical Line("Line") = {1};
