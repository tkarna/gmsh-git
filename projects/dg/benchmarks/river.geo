L=1e6;
Point(1) = {0, 0, 0, L/100};
Point(2) = {L, 0, 0, L/100};
Line(1) = {1, 2};

Physical Point("Downstream") = {1};
Physical Point("Upstream") = {2};
Physical Line("Line") = {1};
