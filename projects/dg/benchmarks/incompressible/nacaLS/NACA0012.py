from dgpy import *
from Incompressible import *
from math import *
import time
import sys

os.system("rm output/*")
os.system("rm *.msh")
os.system("rm LiftAndDrag.dat")

# Cylinder Case
# M. Schaefer and S. Turek, "Benchmark computations of laminar flow around a cylinder", Preprints SFB 359 Nummer 96--03, Universitaet Heidelberg, 96--03, 1996

rho = 1.0
mu = 0.01
UGlob = 10
Chord = 5

flowDir=[1., 0., 0.]
liftDir=[0., 1., 0.]
fScale = 0.5*rho*UGlob*UGlob*Chord
  
nbTimeSteps = 10
timeOrder = 1
dt0 = 0.01
RTol = 1.e-5
ATol = 1.e-8
nb_adapt_steps = 4
nbIter = 15         # Bamg iterations
allDim = True
Verb = 2

meshName = "NACA0012"
geoName = "rect"

#-------------------------------------------------
#-- Initial function
#-------------------------------------------------
def initF(FCT, XYZ) :
  for i in range(0,FCT.size1()):
    FCT.set(i,0, UGlob)
    FCT.set(i,1, 0.) 
    FCT.set(i,2, 0.) 

def InletBC(FCT, XYZ) :
  for i in range(0,FCT.size1()):
    FCT.set(i,0, UGlob)
    FCT.set(i,1, 0.)
    FCT.set(i,2, 0.)
    FCT.set(i,3, fixed)
    FCT.set(i,4, fixed)
    FCT.set(i,5, free)

def normVelFunc(FCT, sol):
	for i in range (0,sol.size1()):
		vx = sol.get(i,0)
		vy = sol.get(i,1)
		FCT.set(i,0,math.sqrt(vx*vx+vy*vy))

#-------------------------------------------------
#-- Initialization
#-------------------------------------------------
c = 5
t = 0.12
rad = 1.019*t*t
def myNaca(x,y,z):
     if (x < 0):
       return  sqrt((x-rad)*(x-rad)+y*y)-rad
     elif (x > c):
       return  sqrt((x-c)*(x-c)+y*y)
     else:
       return fabs(y)-5.*t*c*(0.2969*(x/c)**(0.5)-0.1260*(x/c)-0.3516*(x/c)**2 + 0.2843*(x/c)**3-0.1015 *(x/c)**4)

f0 = simpleFunctionPython(myNaca)
lsNACA = gLevelsetSimpleFunction(f0)

print 'MAX Reynolds = ', rho*UGlob*Chord/mu

mesh = GModel()
mesh.load(geoName+".geo")

for step in range(nb_adapt_steps+1):
  
  print "**************************************************"
  print "     Generating mesh: ITER ",   step
  print "**************************************************"
 
  E = 5
  lcMin = 0.0001
  lcMax = 5 #1
  Np = 25
  E_minus = .15
  NEl = 5000+5000*(step-1)

  adaptedMeshName = meshName + "_adapt%d" % step
  adaptedCutMeshName = meshName + "_adapt%d_cut" % step
  mesh = GModel()
  mesh.load(geoName+".geo")
 
  if (step == 0) :
    mesh.adaptMesh([5], [lsNACA], [[E, lcMin, lcMax]], nbIter, allDim)
  else :
    normVelPy = functionPython(1, normVelFunc, [ns.solution.getFunction()])
    normVelEval = dgFunctionEvaluator(ns.groups, normVelPy)
    normVelEval.setDefaultValue(0.)
    mesh.load(meshName+"_adapt%d.msh"%(step-1))
    mesh.adaptMesh([2,5], [normVelEval,lsNACA], [[NEl, 0, 1.e10],[E, lcMin, lcMax]], nbIter, allDim)
    
  mesh.writeMSH(adaptedMeshName+".msh",2.2,False,False,True,1.0,0,0)
#  mesh.load(adaptedMeshName+".msh")
  
  print "**************************************************"
  print "     Computing on mesh: ITER ",   step
  print "**************************************************"

  meshCut = mesh.buildCutGModel(lsNACA, False, True)
  meshCut.writeMSH(adaptedCutMeshName+".msh",2.2,False,False,True,1.0,0,0)

  rhoF = functionConstant(rho)
  muF = functionConstant(mu)
  ns = Incompressible(adaptedCutMeshName, 2, ["domain_out"])
  ns.initializeIncomp(initF, rhoF, muF , UGlob)

  INLET    = functionPython(6, InletBC, [ns.XYZ])
  ns.strongBoundaryConditionLine('inlet', INLET)
  ns.strongBoundaryConditionLine('levelset_L-1', ns.WALL)
  ns.strongBoundaryConditionLine('top', ns.VELX)
  ns.strongBoundaryConditionLine('bottom', ns.VELX)
  ns.weakPresBoundaryCondition('outlet', ns.PZERO)


#  petscOptions ="-ksp_rtol 1.e-3 -pc_type ilu  -pc_factor_levels 2"
  petscOptions ="-ksp_rtol 1.e-3 -pc_type lu -pc_factor_levels 3 "
#  petscOptions ="-ksp_rtol 1.e-3 -pc_type lu -pc_factor_levels 3 -pc_factor_shift_type POSITIVE_DEFINITE"
  ns.pseudoTimeSteppingSolve(timeOrder, nbTimeSteps, dt0, ATol, RTol,Verb, petscOptions)
  
  ns.computeDragAndLift('levelset_L-1', 0, 0, fScale, flowDir, liftDir, 'LiftAndDrag.dat')
  ns.solution.exportFunctionSurf(ns.law.getWSSNorm(), 'output/surface', 0, 0, 'wss', ['levelset_L-1'])
  ns.solution.exportFunctionSurf(ns.law.getPressure(), 'output/surface', 0, 0, 'pres', ['levelset_L-1'])

