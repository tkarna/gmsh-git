
lc = 2;
lc5 = 0.0001;

Point(1) = {-100,-100,0.0,lc};         
Point(2) = {-100,100,0.0,lc};         
Point(3) = {105,100,0.0,lc};         
Point(4) = {105,-100,0.0,lc};  

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};


Point(5) = {0.9,-0.05,0.0,lc5};  
Point(6) = {0.9,0.05,0.0,lc5};
Point(7) = {1.1,0.05,0.0,lc5};
Point(8) = {1.1,-0.05,0.0,lc5};
Line(5) = {5, 6};
Line(6) = {6, 7};
Line(7) = {7, 8};
Line(8) = {8, 5};

Line Loop(5) = {1, 2, 3, 4};
Plane Surface(6) = {5};

Physical Line("inlet")={1};
Physical Line("top")={2};
Physical Line("outlet")={3};
Physical Line("bottom")={4};

Physical Surface("domain")={6};

