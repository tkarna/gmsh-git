/******************************       
Square uniformly meshed       
******************************/       
lc = 0.1;
L = 2; // x
l = 0.5; // y

p = 8; //pas de discrétization

//Field[1] = BoundaryLayer;
//Field[1].far = 0.1;
//Field[1].hwall_n = 0.1;
//Field[1].hwall_t = 0.1;
//Background Field = 1;
//Mesh.CharacteristicLengthExtendFromBoundary = 0;

// definition des points
Point(1) = {0, -l, 0, lc}; 
Point(2) = {L, -l, 0, lc};
Point(3) = {L, l, 0, lc};
Point(4) = {0, l, 0, lc};


Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};

Line Loop(4) = {1,2,3,4};
Plane Surface(5) = {4};
Transfinite Line {1,3} = (p*4*L)+1;
Transfinite Line {2,4} = (p*4)+1  Using Bump 0.01;//0.0003
Transfinite Surface {5} = {1,2,3,4} Left;

Physical Surface("All") = {5};
Physical Line("WallTop") = {1};
Physical Line("WallBottom") = {3};
Physical Line("Inlet") = {4};
Physical Line("Outlet") = {2};


