Mesh.CharacteristicLengthExtendFromBoundary=0;
Point(1) = {0,0,0,.03};   
Point(2) = {35,0,0};
Point(3) = {35,10,0}; 
Point(4) = {0,10,0};  
Point(5) = {0,100,0}; 
Point(6) = {35,100,0};  
Point(7) = {-20,0,0};  
Point(8) = {-20,10,0};  
Point(9) = {-20,100,0};  


Line(1) = {7, 8};
Line(2) = {8, 4};
Line(3) = {4, 1};
Line(4) = {1, 7};
Line(5) = {4, 3};
Line(6) = {3, 2};
Line(7) = {2, 1};
Line(8) = {4, 5};
Line(9) = {5, 6};
Line(10) = {6, 3};
Line(11) = {8, 9};
Line(12) = {9, 5};
Line Loop(13) = {11, 12, -8, -2};
Plane Surface(14) = {13};
Line Loop(15) = {9, 10, -5, 8};
Plane Surface(16) = {15};
Line Loop(17) = {2, 3, 4, 1};
Plane Surface(18) = {17};
Line Loop(19) = {6, 7, -3, 5};
Plane Surface(20) = {19};

Physical Surface("All") = {14,16,18,20};
Physical Line("Wall") = {7};
Physical Line("Inlet") = {1,11,12,9};
Physical Line("Outlet") = {6,10};
Physical Line("Symmetry") = {4};
Physical Point("CornerBottomLeft") = {7};
Field[1] = Attractor;
Field[1].EdgesList = {7,4};
Field[1].NNodesByEdge = 200000;

Field[11] = Attractor;
Field[11].EdgesList = {3};
Field[11].NNodesByEdge = 200000;

Field[2] = BoundaryLayer;
Field[2].IField = 1;
Field[2].hfar = 15;
Field[2].hwall_t = 2;
Field[2].hwall_n = 0.1;
Field[2].ratio = 1.15;

Field[3] = BoundaryLayer;
Field[3].IField = 11;
Field[3].hfar = 15;
Field[3].hwall_t = 2;
Field[3].hwall_n = 0.1;
Field[3].ratio = 1.15;

Field[4] = MinAniso;
Field[4].FieldsList = {2, 3};
Background Field = 4;
