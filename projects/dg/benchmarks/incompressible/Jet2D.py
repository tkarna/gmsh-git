from dgpy import *
from Incompressible import *
from math import *
import sys
import os

#-------------------------------------------------
#-- Jet impinging upon wall
#-------------------------------------------------

#-- This problem consists of a jet impinging upon a wall with a controlled body force 
#-- on a domain Omega = (0, 1) x (0, 1) and can be found e.g. in Hendriana and Bathe (2000). 
#-- A slip boundary condition is assumed on the surface of the left wall.
#-- The body force functions are f1 = 5xy^8 + 10xy^3 + 60*nu*xy^2, f2 = 0	
#-- The fluid density is rho = 1.
#-- Two quite different flow regimes are considere, a diffusion-dominated flow with 
#-- mu = 10 (Re=0.5025) and a convection-dominated flow with mu = 0.001 (Re=5025).
#-- The analytical solution of the problem then reads:
#-- u1 = -5xy**4	
#-- u2 = -0.5 + y**5	
#-- p = 0.5*(y**5-y**10) + 5*mu*y**4

#-------------------------------------------------
#-- Parameters
#-------------------------------------------------
rho = 1.0
mu = 10    #--Re=0.5
#mu = 0.001   #--Re = 5025
UGlob = 0.5
meshName = "Jet2D" #"Jet16X16"

#-------------------------------------------------
#-- Initial function
#-------------------------------------------------
def initF(FCT, XYZ):
	for i in range(0,FCT.size1()):
		x = XYZ.get(i,0)
		y = XYZ.get(i,1)
		uana = -5.0*x*pow(y,4)
		vana = -0.5+pow(y,5)
		pana =  0.5*(pow(y,5)-pow(y,10))+5*mu*pow(y,4)
		FCT.set(i,0, uana) 
		FCT.set(i,1, vana) 
		FCT.set(i,2, pana) 

#-------------------------------------------------
#-- Case specific functions
#-------------------------------------------------
def solAna(FCT, XYZ ):
	for i in range (0,FCT.size1()):
		x = XYZ.get(i,0)
		y = XYZ.get(i,1)
		uana = -5.0*x*pow(y,4)
		vana = -0.5+pow(y,5)
		pana =  0.5*(pow(y,5)-pow(y,10))+5*mu*pow(y,4)
		FCT.set(i,0, 1.0) #uana)
		FCT.set(i,1, 2.0) #vana)
		FCT.set(i,2, 3.0) #pana) 

def sourceF( FCT,XYZ ):
	for i in range (0,FCT.size1()):
		x = XYZ.get(i,0)
		y = XYZ.get(i,1)
		f1 =  5.*x*pow(y,8.)+10*x*pow(y,3.)+60*mu*x*pow(y,2.)
		f2 = 0.0
		FCT.set(i,0, f1) 
		FCT.set(i,1, f2) 
		FCT.set(i,2, 0.0)

def VelLeftBC( FCT,XYZ ):
	for i in range (0,FCT.size1()):
		FCT.set(i,0, 0.0)
		FCT.set(i,1, 0.0)
		FCT.set(i,2, 0.0)
		FCT.set(i,3, fixed)
		FCT.set(i,4, free)
		FCT.set(i,5, free)

def VelTopBC( FCT,XYZ ):
	for i in range (0,FCT.size1()):
		x = XYZ.get(i,0)
		FCT.set(i,0, -5*x)
		FCT.set(i,1, 0.5)
		FCT.set(i,2, 0.0)
		FCT.set(i,3, fixed)
		FCT.set(i,4, fixed)
		FCT.set(i,5, free)

def VelRightBC( FCT,XYZ ):
	for i in range (0,FCT.size1()):
		y = XYZ.get(i,1)
		FCT.set(i,0, -5.0*pow(y,4))
		FCT.set(i,1, -0.5+pow(y,5))
		FCT.set(i,2, 0.0)
		FCT.set(i,3, fixed)
		FCT.set(i,4, fixed)
		FCT.set(i,5, free)
#-------------------------------------------------
#-- Initialization
#-------------------------------------------------
ns = Incompressible(meshName,2)
rhoF = functionConstant(rho)
muF = functionConstant(mu)
ns.initializeIncomp(initF, rhoF, muF , UGlob)
ns.law.setSource(SOURCE)

analytical = dgDofContainer(ns.groups, ns.law.getNbFields())
analytical.L2Projection(ANA)
#analytical.exportFunctionMsh(ns.law.getVelocity(), 'output/Ana', 0, 0, 'vel')
#analytical.exportFunctionMsh(ns.law.getPressure(), 'output/Ana', 0, 0, 'pres')

"""
def simple(FCT, XYZ ):
	for i in range (0,FCT.size1()):
		x = XYZ.get(i,0)
		FCT.set(i,0, x)
TEST  = functionPython(1, simple, [XYZ])

integ2=dgFunctionIntegratorInterface(ns.groups, ANA)
z=fullMatrixDouble(3,1)
integ2.compute('WallTop',z)
print 'INTEG2 =', z(0,0), ' ', z(1,0), ' ', z(2,0)

exit(1)
"""

#-------------------------------------------------
#-- Boundary Conditions
#-------------------------------------------------
ANA  = functionPython(3, solAna, [ns.XYZ])
SOURCE  = functionPython(3, sourceF, [ns.XYZ])
VELLEFT  = functionPython(6, VelLeftBC,  [ns.XYZ])
VELTOP  = functionPython(6, VelTopBC,  [ns.XYZ])
VELRIGHT = functionPython(6, VelRightBC, [ns.XYZ])

ns.strongBoundaryConditionLine('Outlet', VELRIGHT)
ns.strongBoundaryConditionLine('Inlet', VELLEFT)
ns.strongBoundaryConditionLine('WallTop', VELTOP)
ns.strongBoundaryConditionLine('WallBottom', ns.PZERO)

#-------------------------------------------------
#-- Pseudo TimeSteppings
#-------------------------------------------------
nbTimeSteps = 4
dt0 = 10.0
ATol = 1.e-5
RTol = 1.e-8
Verb = 2
petscOptions ="-ksp_rtol 1.e-3 -pc_type ilu -pc_factor_levels 0"
timeOrder = 2
ns.pseudoTimeSteppingSolve(timeOrder, nbTimeSteps, dt0, ATol, RTol, Verb, petscOptions)


#-------------------------------------------------
#-- Validation 
#-------------------------------------------------
