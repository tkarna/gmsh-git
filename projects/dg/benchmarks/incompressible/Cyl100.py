from dgpy import *
from Incompressible import *
from math import *
import sys

os.system("rm output/*")
os.system("rm LiftAndDrag.dat")
#-------------------------------------------------
#-- Cylinder at Reynolds 100
#-------------------------------------------------

#-------------------------------------------------
#-- Parameters
#-------------------------------------------------
RHO = 1.0
MU  = 0.01
UGlob = 0.8
meshName = "Cyl100"

print('*** Reynolds= %g' % (RHO*UGlob*1/MU))

#-------------------------------------------------
#-- Initial function
#-------------------------------------------------
def initF(FCT, XYZ):
	for i in range(0,FCT.size1()):
		FCT.set(i,0, UGlob) #u
		FCT.set(i,1, 0.0) #v
		FCT.set(i,2, 0.0) #p

#-------------------------------------------------
#-- Case specific functions
#-------------------------------------------------
def VelBC(FCT, XYZ ):
	vmax = 2.0;
	for i in range(0,FCT.size1()):
		FCT.set(i,0, UGlob)
		FCT.set(i,1, 0.0)
		FCT.set(i,2, 0.0)
		FCT.set(i,3, fixed)
		FCT.set(i,4, fixed)
		FCT.set(i,5, free)

#-------------------------------------------------
#-- Initialization
#-------------------------------------------------
ns = Incompressible(meshName, 2)
rhoF = functionConstant(RHO)
muF = functionConstant(MU)
ns.initializeIncomp(initF, rhoF, muF , UGlob)
#-------------------------------------------------
#-- Boundary Conditions
#-------------------------------------------------

VEL = functionPython(6, VelBC, [ns.XYZ])
ns.strongBoundaryConditionLine('Inlet', VEL)
ns.strongBoundaryConditionLine('Cylinder', ns.WALL)
ns.weakPresBoundaryCondition('Outlet', ns.PZERO)
ns.weakSlipBoundaryCondition('WallBottom')
ns.weakSlipBoundaryCondition('WallTop')

#-------------------------------------------------
#-- TimeStepping
#-------------------------------------------------

timeOrder = 2
ATol = 1.e-5
RTol = 1.e-8
Verb = 1
nbExport = 10
petscOptions ="-pc_type lu"
 
ns.initSolver(timeOrder, ATol,RTol, Verb, petscOptions, nbExport) 

flowDir=[1., 0., 0.]
spanDir=[0., 1., 0.]
D = 1.0
fScale = 0.5*RHO*UGlob*UGlob*D
dt = 0.1

t = 0.0
tic = time.clock()
for i in range (0,200) :
	Msg.Info("*** Iter %i t=%.2g dt=%.2g cpu=%.2f" % (i,t,dt,time.clock()-tic))
	if (i % nbExport == 0):
		ns.computeDragAndLift('Cylinder', t, i, fScale, flowDir, spanDir, 'LiftAndDrag.dat')
	ns.doOneIteration(dt, i)
	#ns.doOneExplicitIteration(i)
	t = t+dt
	
#-------------------------------------------------
#-- Validation 
#-------------------------------------------------


