/******************************       
Square uniformly meshed       
******************************/       
lc = .2;     
L = 2; // x 
l = 0.5; // y
z = 1.0; // extrude en z
p = 2; //pas de discrétization
nz = 5;  

// definition des points
Point(1) = {0, -l, 0, lc}; 
Point(2) = {L, -l, 0, lc};
Point(3) = {L, l, 0, lc};
Point(4) = {0, l, 0, lc};


Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};


Line Loop(4) = {1,2,3,4};
Plane Surface(5) = {4};
Transfinite Line {1,3} = (p*4*L)+1;
Transfinite Line {2,4} = (p*4)+1; // Using Bump 0.01;//0.0003
Transfinite Surface {5} = {1,2,3,4} Centered;

out[] = Extrude {0,0,z}{ Surface{5}; Layers{nz}; };

Physical Surface("Symmetry") = {5,27};
Physical Surface("Wall") = {14, 22};
Physical Surface("Inlet") = {26};
Physical Surface("Outlet") = {18};
Physical Volume("Vol")={1};
Physical Point("CornerBottomLeft") = {1};



