from Incompressible import *
from dgpy import *
from math import *
import sys
import os

#-------------------------------------------------
#-- Backward facing step over step of height h=1
#-------------------------------------------------

#-------------------------------------------------
#-- Parameters
#-------------------------------------------------
rho = 1.0
mu = 0.01
UGlob = 1.0
meshName = "BackwardFacingStep"

print('*** Reynolds= %g' % (rho*UGlob*1/mu))

#-------------------------------------------------
#-- Initial function
#-------------------------------------------------
def initF(FCT, XYZ):
	for i in range(0,FCT.size1()):
		FCT.set(i,0, UGlob) #u
		FCT.set(i,1, 0.0) #v
		FCT.set(i,2, 0.0) #p
		
#-------------------------------------------------
#-- Case specific functions
#-------------------------------------------------
def InletBC(FCT, XYZ ):
	for i in range(0,FCT.size1()):
		FCT.set(i,0, UGlob)
		FCT.set(i,1, 0.0)
		FCT.set(i,2, 0.0)
		FCT.set(i,3, fixed)
		FCT.set(i,4, fixed)
		FCT.set(i,5, free)
		
def SymmetryBC(FCT, XYZ ):
	for i in range(0,FCT.size1()):
		FCT.set(i,0, 0.0)
		FCT.set(i,1, 0.0)
		FCT.set(i,2, 0.0)
		FCT.set(i,3, free)
		FCT.set(i,4, fixed)
		FCT.set(i,5, free)



#-------------------------------------------------
#-- Initialization
#-------------------------------------------------
ns = Incompressible(meshName, 2)
rhoF = functionConstant(rho)
muF = functionConstant(nu)
ns.initializeIncomp(initF, rhoF, muF , UGlob)
#-------------------------------------------------
#-- Boundary Conditions
#-------------------------------------------------
INLET = functionPython(6, InletBC, [ns.XYZ])
SYMMETRY = functionPython(6, SymmetryBC, [ns.XYZ])

ns.strongBoundaryConditionLine('Inlet', INLET)
ns.strongBoundaryConditionLine('Wall', ns.WALL)
ns.strongBoundaryConditionLine('Symmetry', SYMMETRY)
#ns.weakSymmetryBoundaryCondition('Symmetry')
ns.weakPresBoundaryCondition('Outlet', ns.PZERO)

#-------------------------------------------------
#-- Pseudo TimeStepping
#-------------------------------------------------
nbTimeSteps = 20
dt0 = 10.0
ATol = 1.e-8
RTol = 1.e-5
Verb = 2
petscOptions ="-ksp_rtol 1.e-3 -pc_type lu -ksp_monitor -pc_factor_levels 0"

ns.pseudoTimeSteppingSolve(nbTimeSteps, dt0, ATol, RTol, Verb, petscOptions)

ns.adaptMesh(meshName, "BackwardFacingStepAdapted", 1.e-3, 1.e-4)

#-------------------------------------------------
#-- Validation 
#-------------------------------------------------
