from dgpy import *
from Incompressible import *
from math import *
import time
import sys

#################################################
def initF(FCT, XYZ) :
  for i in range(0,FCT.size1()):
    FCT.set(i,0, UGlob)
    FCT.set(i,1, 0.) 
    FCT.set(i,2, 0.)

#################################################
def computeFiniteDifferenceWSS_NACA(model, groups, law, solution, hFD, nbFD, mu, name, fScale=1.0, NParam = 0) :
  
  if (nbFD > 5):
    print "FD WSS evaluation up to 4th order only !"
    exit(1)
    
  fun = open("WSS_FD_NACA.txt","w")
  fun.write("#x y z wss \n")
  eval2 = dgFunctionEvaluator(groups, law.getVelocity(), solution)
  eval2.setDefaultValue(-10000.0)
  res = fullMatrixDouble(3,1)

  velu=functionExtractCompNew(solution.getFunction(),0)
  evalu = dgFunctionEvaluator(groups, velu)
  velv=functionExtractCompNew(solution.getFunction(),1)
  evalv = dgFunctionEvaluator(groups, velv)
  
  metricU = meshMetric(model)
  metricU.addMetric(2, evalu, [1000, 0.1, 1.0])
  
  metricV = meshMetric(model)
  metricV.addMetric(2, evalv, [1000, 0.1, 1.0])

  #Get the edges
  #############################
  nums = model.getEdgesByStringTag(name)
  for k in range(len(nums)):
    edge = model.getEdgeByTag(nums[k])
    
    #compute number of points for evaluation
    if (NParam > 0):
      N = NParam + 1
    else: 
      N =  nbV = edge.getNumMeshElements()
      
    #loop over points and get the normal direction to the edge
    for i in range (0,N) :
      if (NParam > 0):
        _c = 1.0
        _t = 0.12
        x = 0.0 + (i*1.0+1.)/NParam *_c
        y = 5.*_t*_c*(0.2969*sqrt(x/_c)-0.1260*x/_c-0.3516*x*x/(_c*_c)+0.2843*x*x*x/(_c*_c*_c)-0.1036*x*x*x*x/(_c*_c*_c*_c))
        p = GPoint(x,y,0.0)
        eps = 1.e-3
        dy = 5.*_t*_c*(0.14845/sqrt(x/_c)-0.4144*x*x*x/(_c*_c*_c)+0.8529*x*x/(_c*_c)-0.7032*x/_c-0.126)/_c
        der = SVector3(x+eps, y+(dy*eps), 0.0);
        perp = SVector3(0.0, 0.0, -1.0)
        nn = crossprod(der, perp)
        n = SVector3(nn.x()/norm(nn), nn.y()/norm(nn),nn.z()/norm(nn));
      else:
        e = edge.getMeshElement(i)
        p0 = e.getVertex(0)
        p1 = e.getVertex(1)
        p = GPoint(0.5*(p1.x()+p0.x()),0.5*(p1.y()+p0.y()),0.5*(p1.z()+p0.z()))
        der = SVector3(p1.x()-p0.x(), p1.y()-p0.y(), p1.z()-p0.z())
        perp = SVector3(0.0, 0.0,-1.0)
        nn = crossprod(der, perp)
        n = SVector3(nn.x()/norm(nn), nn.y()/norm(nn),nn.z()/norm(nn));
        
      #revert normal if point does not exist
      pTest = SPoint3(p.x()+hFD*n.x(),p.y()+hFD*n.y(),p.z()+hFD*n.z())
      me = model.getMeshElementByCoord(pTest, 2)
      eval2.compute(pTest.x(), pTest.y(), pTest.z(), res)
      if ((me == None) or (res.get(0,0) == -10000.0)):
        n = SVector3(-1.*n.x(), -1.*n.y(),-1.*n.z());

      #Least square computation
      if (NParam < 0):
        gradu = metricU.getGradient(p0)
        gradv = metricV.getGradient(p0)
        t1 = mu* (2.*gradu.x()*n.x()     + (gradu.y()+gradv.x())*n.y());
        t2 = mu* ((gradv.x()+gradu.y())*n.x() + 2.*gradv.y()*n.y());
        tn = t1*n.x()+t2*n.y();
        wss_x = t1 - tn*n.x();
        wss_y = t2 - tn*n.y();
        wss_norm = sqrt(wss_x*wss_x+wss_y*wss_y)
        fun.write("%g %g %g %g \n" % (p0.x(),p0.y(), p0.z(), wss_norm))
      #Finite diference computation
      else:
        #evaluate tangent velocity at different points in normal direction
         d = [None]*nbFD
         for j in range (0,nbFD) :
           p2x = p.x() + (j*hFD)*n.x()
           p2y = p.y() + (j*hFD)*n.y()
           p2z = p.z() + (j*hFD)*n.z()
           eval2.compute(p2x, p2y, p2z, res)
           #get the wall velocity parallel to the wall
           vel = SVector3(res.get(0,0),res.get(1,0), 0.0)
           vn = dot(vel,n)
           vel_t = SVector3(vel.x() - vn*n.x(),vel.y() - vn*n.y(),vel.z() - vn*n.z())
           d[j] = norm(vel_t)
        
         #FD computations of the derivative
         myDer  = 0.0
         if (nbFD == 2) :
           myDer = (d[1]-d[0])/hFD
         elif (nbFD == 3) :
           myDer = (-d[2] + 4.*d[1] - 3*d[0])/(2.*hFD)
         elif (nbFD == 4) :
           myDer = (1./3.*d[3] - 3./2.*d[2] + 3.*d[1] - 11./6.*d[0])/hFD
         elif (nbFD == 5) :
           myDer = (-1./4.*d[4] + 4./3.*d[3] - 3.*d[2] + 4.*d[1] - 25./12.*d[0])/hFD
           
         #WSS computations
         WSS = mu*myDer/fScale
         if (p.y() >= 0.0): 
           fun.write("%g %g %g %g \n" % (p.x(),p.y(), p.z(), fabs(WSS)))
            
  fun.write("\n")

#################################################
rho = 1.0
mu = 0.001
UGlob = 5
Chord = 1.0

#meshName = "naca12_2d"
#meshName = "Naca_adapt"
#name = "WallTop"

#meshName = "Naca_adapt_cut"
#name = "levelset_L-1"

meshName = "rect12"

model = GModel()
model.load(meshName+msh)
rhoF = functionConstant(rho)
muF = functionConstant(mu)
groups = dgGroupCollection(model, 2, 1)
solution = dgDofContainer(groups, 3)
solution.importMshNodeData("output/sol", True)
law = dgConservationLawNavierStokesIncomp2d(rhoF, muF, UGlob, None , None, None, 293,1000,None)

ns = Incompressible(meshName, 2)
ns.initializeIncomp(initF, rhoF, muF , UGlob)
ns.solution.importMshNodeData("output/sol", True)

#################################################
hFD = 1.e-4
nbFD = 2
fScale = 1.0

#ns.computeWallShearStressFD(hFD, nbFD, mu, name, fScale)
#ns.computeWallShearStressLEAST_SQUARES(mu, name, fScale)
#ns.computeWallShearStressEIGEN(mu, name, 1.0)

#Least Square evaluation
#computeFiniteDifferenceWSS_NACA(model, groups, law, solution, hFD, nbFD, mu, name, 1.0, -1)

#Loop ver LS iso zero
computeFiniteDifferenceWSS_NACA(model, groups, law, solution, hFD, nbFD, mu, name, 1.0, 200)

#Loop over LS approximate geometry
#computeFiniteDifferenceWSS_NACA(model, groups, law, solution, hFD, nbFD, mu, name, 1.0)

#Loop over GEO geometry
#model.load("naca12.geo")
#name = "WallTop"
#computeFiniteDifferenceWSS_NACA(model, groups, law, solution, hFD, nbFD, mu, name, 1.0, 100)

#################################################
