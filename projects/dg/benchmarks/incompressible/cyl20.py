from dgpy import *
from Incompressible import *
from math import *
import time
import sys

os.system("rm output/*")
os.system("rm *.msh")
os.system("rm LiftAndDrag.dat")

# Cylinder Case with EXACT GEOMETRY
# M. Schaefer and S. Turek, "Benchmark computations of laminar flow around a cylinder",
# Preprints SFB 359 Nummer 96--03, Universitaet Heidelberg, 96--03, 1996

#-------------------------------------------------
#-- Cylinder at Reynolds 20
#-------------------------------------------------
ADAPT = False
BL = False
dim = 2

meshName = "cyl20"
if (BL):
	meshName = "cyl20_BL"

if (ADAPT):
	ga = GModel()
	ga.load(meshName+geo)
	ls = gLevelsetSphere(0.2, 0.2, 0, 0.05)
	ga.adaptMesh([4], [ls], [[0.004, 1e-4, 0.05, 150]], 10, True)
	meshName = "cyl20_ANISO"
	ga.save(meshName+msh)
else:
	genMesh(meshName, dim, 1)
		     
#-------------------------------------------------
#-- Parameters
#-------------------------------------------------
RHO = 1.0
MU  = 0.001
UGlob = 0.2
D = 0.1
print ("---- Computing Cylinder at Re=%g ----"  % (RHO*UGlob*D/MU))

#-------------------------------------------------
#-- Initial function
#-------------------------------------------------
def initF(FCT, XYZ):
	for i in range(0,FCT.size1()):
	        y = XYZ.get(i,1)
		u = 4*0.3*y*(0.41-y)/(0.41*0.41)
		FCT.set(i,0, u) #u  UGlob
		FCT.set(i,1, 0.0) #v
		FCT.set(i,2, 0.0) #p

#-------------------------------------------------
#-- Case specific functions
#-------------------------------------------------
def VelBC(FCT, XYZ ):
	for i in range(0,FCT.size1()):
		y = XYZ.get(i,1)
		u = 4*0.3*y*(0.41-y)/(0.41*0.41)
		FCT.set(i,0, u)
		FCT.set(i,1, 0.0)
		FCT.set(i,2, 0.0)
		FCT.set(i,3, fixed)
		FCT.set(i,4, fixed)
		FCT.set(i,5, free)

#-------------------------------------------------
#-- Initialization
#-------------------------------------------------
ns = Incompressible(meshName, dim)
rhoF = functionConstant(RHO)
muF = functionConstant(MU)
ns.initializeIncomp(initF, rhoF, muF , UGlob)
#-------------------------------------------------
#-- Boundary Conditions
#-------------------------------------------------

VEL = functionPython(6, VelBC, [ns.XYZ])
ns.strongBoundaryConditionLine('Inlet', VEL)
ns.strongBoundaryConditionLine('Cylinder', ns.WALL)
ns.strongBoundaryConditionLine('Outlet', ns.PZERO)
ns.strongBoundaryConditionLine('WallTop', ns.WALL)
ns.strongBoundaryConditionLine('WallBottom', ns.WALL)

#-------------------------------------------------
#-- TimeStepping
#-------------------------------------------------
nbTimeSteps = 10
dt0 = 10.0
ATol = 1.e-8
RTol = 1.e-5
Verb = 2
petscOptions ="-ksp_rtol 1.e-3 -pc_type lu -pc_factor_levels 0"

#ns.pseudoTimeSteppingSolve(2, nbTimeSteps, dt0, ATol, RTol, Verb, petscOptions)
ns.steadySolve(20, petscOptions)
	
#-------------------------------------------------
#-- Validation 
#-------------------------------------------------

flowDir=[1., 0., 0.]
spanDir=[0., 1., 0.]
fScale = 0.5*RHO*UGlob*UGlob*D
ns.computeDragAndLift('Cylinder', 0, 0, fScale, flowDir, spanDir, 'LiftAndDrag.dat')

fun=open('LiftAndDrag.dat',"r")
line1 = fun.readline()
line2 = (fun.readline()).split()
CL_NUM = float(line2[2])
CL_ANA = 0.011
CD_NUM = float(line2[3])
CD_ANA = 5.57
fun.close()

diff_lift = fabs((CL_NUM-CL_ANA)/CL_ANA)
diff_drag = fabs((CD_NUM-CD_ANA)/CD_ANA)
print('-------------------------------------------------------------')
print('Error Analysis')
print('-------------------------------------------------------------')
print ('Lift: CL=%.4f CL_ANA=%.4f |error CL| =%.4f ' % (CL_NUM, CL_ANA, diff_lift))
print ('Drag: CD=%.4f CD_ANA=%.4f |error CD| =%.4f ' % (CD_NUM, CD_ANA, diff_drag))


if (diff_lift  < 2.e-1 and diff_drag  < 2.e-1): 
	print ("Exit with success :-)")
	sys.exit(success)
else:
	print ("Exit with failure :-(")
	sys.exit(fail)


