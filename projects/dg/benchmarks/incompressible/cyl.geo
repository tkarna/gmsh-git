K = 2;
K2 = 2;
lDom = 2.2;
lUp = 0.15+0.1/2;
hDomPlus = 0.16+0.1/2; //hDomPlus = 0.15+0.1/2;
hDomMinus = 0.15+0.1/2;

lcFar = 0.01;

Point (1) = {-lUp, -hDomMinus, 0, lcFar*K2};
Point (2) = {-lUp+lDom, -hDomMinus, 0, lcFar*K2};
Point (3) = {-lUp+lDom, hDomPlus, 0, lcFar*K2};
Point (4) = {-lUp, hDomPlus, 0, lcFar*K2};

Point(5) = {0., 0., 0, 0.0005*K};
Point(6) = {0., 0.05, 0, 0.0005*K};
Point(7) = {0.05, 0.05, 0, 0.0005*K};
Point(8) = {0.05, 0.0, 0, 0.0005*K};
Point(9) = {-0.05, 0.0, 0, 0.0005*K};
Point(10) = {0, -0.05, 0, 0.0005*K};

Line (1) = {1,2};
Line (2) = {2,3};
Line (3) = {3,4};
Line (4) = {4,1};

Circle(7) = {6, 5, 8};
Circle(8) = {8, 5, 10};
Circle(9) = {10, 5, 9};
Circle(10) = {9, 5, 6};

Line Loop (5) = {1,2,3,4,7,8,9,10};
Plane Surface (6) = {5};

Physical Line("Inlet")={4};
Physical Line("Outlet")={2};
Physical Line("Channel") = {1,3};
Physical Line("cyl") = {7,8,9,10};
Physical Surface("Domain")={6};



