R = 0.5 ;
l = 4.5; //before cylinder
L = 20; //after cylinder
H = 4.5; //above and under cylinder

p = 0;
r = 0;

lc = 0.08;
lc2 = 0.6;
X = 0;
Y = 0;

Point(6) = {-l-X,-H-r-Y,0,lc2};
Point(7) = {-l-X,H-Y,0,lc2};
Point(8) = {-l+L-X,H-Y,0,lc2};
Point(9) = {-l+L-X,-H-r-Y,0,lc2};

Line(5) = {7,8};
Line(6) = {8,9};
Line(7) = {9,6};
Line(8) = {6,7};
Line Loop(9) = {7,8,5,6};
Plane Surface(11) = {9};

Physical Surface("All") = {11};
Physical Line("WallBottom") = {7};
Physical Line("WallTop") = {5};
Physical Line("Inlet") = {8};
Physical Line("Outlet") = {6};
