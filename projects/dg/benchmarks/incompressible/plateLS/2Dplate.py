
from dgpy import *
from Incompressible import *
from Common import *
import os
import time
import math
import sys

try : os.mkdir("output");
except: 0;
os.system("rm velocitySimulation*dat")
print'       '
print'*-------------------dgCode-------------------*'
print'|            |                               |'
print'| 2DCylinder |        CASE PARAMETERS        |'
print'|            |                               |'
print'*--------------------------------------------*'

print'---- Setting parameters'
RHO = 1.0
MU  = 1e-04 
Uref = 1 
L = 2.07
Re = RHO*Uref*L/MU #Re=20700
nb_adapt_steps = 4

print'       '
print'*-------------------dgCode-------------------*'
print'|            |                               |'
print'| 2DCylinder |    CASE SPECIFIC FUNCTIONS    |'
print'|            |                               |'
print'*--------------------------------------------*'

print'---- Defining specific functions'

def initF(FCT, XYZ):
 	for i in range(0,FCT.size1()):
		y = XYZ.get(i,1)
		FCT.set(i,0, Uref) #u
		FCT.set(i,1, 0.0) #v
		FCT.set(i,2, 0.0) #p

def InletBC(FCT, XYZ):
	for i in range(0,FCT.size1()):  
		FCT.set(i,0, Uref) #u
		FCT.set(i,1, 0.0) #v
		FCT.set(i,2, 0.0) #p
		FCT.set(i,3, fixed)
		FCT.set(i,4, fixed)
		FCT.set(i,5, free)

def normVEL_2D(FCT, sol):
	for i in range(0,sol.size1()):
		u = sol.get(i,0)
		v = sol.get(i,1)
		FCT.set(i,0, sqrt(u*u+v*v))


print'       '
print'*--------------------dgCode--------------------*'
print'|            |                                 |'
print'| 2DCylinder |   INITIALIZE LEVELSET PROBLEM   |'
print'|            |                                 |'
print'*----------------------------------------------*'

print '---- Defining Solver for Navier-Stokes problems'
def SolveNS(meshName):
	print '---- initializing problem:'
	rhoF = functionConstant(RHO)
	muF = functionConstant(MU)
	ns = Incompressible(meshName, 2)
	ns.initializeIncomp(initF, rhoF, muF, Uref)
	NORMVEL_2D = functionPython(1, NormVEL_2D, [ns.solution.getFunction()])

	print '---- defining boundary conditions:'
	VEL = functionPython(6, InletBC, [ns.XYZ])
	ns.strongBoundaryConditionLine('inlet_in', VEL)
	ns.strongBoundaryConditionLine('inlet_out', VEL)
	ns.weakPresBoundaryCondition('outlet_in', ns.PZERO)
	ns.weakPresBoundaryCondition('outlet_out', ns.PZERO)
	ns.strongBoundaryConditionLine('top_in', ns.VELX)
	ns.strongBoundaryConditionLine('top_out', ns.VELX)
	ns.strongBoundaryConditionLine('bottom_in', ns.VELX)
	ns.strongBoundaryConditionLine('bottom_out', ns.VELX)
	ns.strongBoundaryConditionLine('levelset_L1_out', ns.WALL)
	
	print '---- defining solver:'
	petscOptions = "-ksp_rtol 1.e-3 -pc_type lu -pc_factor_levels 3"
	ns.pseudoTimeSteppingSolve(1, 600, 0.1, 1.e-5, 1.e-8, 80, petscOptions)

	print '---- post processing: exporting results'
	ns.solution.exportFunctionSurf(ns.law.getWSS(), 'output/surface', 0, 0, 'wss%d'%step, ['levelset_L1_out'])
	ns.solution.exportFunctionSurf(ns.law.getWSSNorm(), 'output/surface', 0, 0, 'wssNorm%d'%step, ['levelset_L1_out'])
	ns.solution.exportFunctionMsh(NORMVEL_2D, 'output/ns000000', 0, 0, 'velNorm')

	print '---- post processing: exporting velocity profiles'
	fun = open(fileName, "a")
	fun.write("#(eta) (u0 v0) (u1 v1) (u2 v2)\n")
	x0 = 0.5 #eta = 8.8 -> y0=0.0620
	x1 = 1.0 #	       y1=0.0880
	x2 = 1.5 # 	       y2=0.1078
	Re0 = RHO*Uref*x0/MU
	Re1 = RHO*Uref*x1/MU
	Re2 = RHO*Uref*x2/MU
	evalSol = dgFunctionEvaluator(ns.groups, ns.solution.getFunction())
	for i in range(0,20):
	  eta = 0 + i*8.6/20
	  y0 = eta*x0/sqrt(Re0)
	  y1 = eta*x1/sqrt(Re1)
	  y2 = eta*x2/sqrt(Re2)
	  result0 = fullMatrixDouble(3,1) 
	  result1 = fullMatrixDouble(3,1) 
	  result2 = fullMatrixDouble(3,1) 
	  evalSol.compute(x0,y0,0,result0) 
	  evalSol.compute(x1,y1,0,result1)  
	  evalSol.compute(x2,y2,0,result2)   
	  u0 = result0.get(0,0)/Uref
	  v0 = result0.get(1,0)*sqrt(Re0)/Uref
	  u1 = result1.get(0,0)/Uref
	  v1 = result1.get(1,0)*sqrt(Re1)/Uref
	  u2 = result2.get(0,0)/Uref
	  v2 = result2.get(1,0)*sqrt(Re2)/Uref
	  print >>fun, eta, ' ', u0, ' ', v0, ' ', u1, ' ', v1, ' ', u2, ' ', v2
	fun.close()
	return ns

print'       '
print'*-------------------dgCode-------------------*'
print'|            |                               |'
print'| 2DCylinder |      GEOMETRY ADAPTIVITY      |'
print'|            |                               |'
print'*--------------------------------------------*'

print'defining levelset fuction...'
def myLine(x,y,z):
     if (x < -0.005):
       return sqrt((x+0.005)*(x+0.005)+y*y)
     else:
       return fabs(y)

fLine = simpleFunctionPython(myLine)
lsLine = gLevelsetSimpleFunction(fLine)

f1 = gLevelsetMathEval("y")
ls1 = f1
f2 = gLevelsetMathEval("x")
ls2 = f2

dim = 2
order = 1
caseName  = "2Dplate"

print'---- LOADING GEOMETRY'		
mesh = GModel()
mesh.load(caseName+".geo")

for step in range(nb_adapt_steps+1):
  
  print "**************************************************"
  print "     Generating mesh: ITER ",   step
  print "**************************************************"
  lcMin = 0.00001
  lcMax = 0.2
  E = 2*lcMax
  Ne =10000*step #15000

  adaptedMeshName = caseName + "_adapt%d" % step
  adaptedCutMeshName = caseName + "_adapt%d_cut" % step
  mesh = GModel()
  mesh.load(caseName+".geo")
 
  if (step == 0) :
    mesh.adaptMesh([5], [lsLine], [[E, lcMin, lcMax]], 25, True)
  else :
    NORMVEL = functionPython(1, NormVEL_2D, [ns.solution.getFunction()])
    myEval = dgFunctionEvaluator(ns.groups, NORMVEL)
    myEval.setDefaultValue(0.)
    mesh.load(caseName+"_adapt%d.msh"%(step-1))
    mesh.adaptMesh([2,5], [myEval,lsLine], [[Ne, 0., lcMax],[E, lcMin, lcMax]], 25, True)
    
  mesh.writeMSH(adaptedMeshName+".msh",2.2,False,False,True,1.0,0,0)
  
  print "**************************************************"
  print "     Computing on mesh: ITER ",   step
  print "**************************************************"
  print'---- SPLITTING MESH'
  model1 = GModel()
  model1 = mesh.buildCutGModel(ls1, False, True)
  name1 = "2Dplate1_geo"
  model1.save(name1+msh)

  model2 = GModel()
  model2 = model1.buildCutGModel(ls2, False, True)
  name2 = "2Dplate2_geo"
  model2.save(name2+msh)

  adapt = GModel()
  adapt.load(name2+msh)
  adapt.writeMSH(adaptedCutMeshName+".msh",2.2,False,False,True,1.0,0,0)

  print'---- SOLVING'
  fileName = 'velocitySimulation%d.dat'%(step)
  ns = SolveNS(adaptedCutMeshName)
