Point(1) = {-1.5, -1.5, 0, 0.1};
Point(2) = {-1.5, 1.5, 0, 0.1};
Point(3) = {2.07, 1.5, 0, 0.1};
Point(4) = {2.07, -1.5, 0, 0.1};

Line(1) = {2, 1};
Line(2) = {1, 4};
Line(3) = {4, 3};
Line(4) = {3, 2};

Physical Line("inlet") = {1};
Physical Line("top") = {4};
Physical Line("outlet") = {3};
Physical Line("bottom") = {2};

Line Loop(15) = {1, 2, 3, 4};
Plane Surface(16) = {15};
Physical Surface("domain") = {16};
