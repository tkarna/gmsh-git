from dgpy import *
from Incompressible import *
from Common import *
import os
import time
import math
import sys

#http://en.wikipedia.org/wiki/NACA_airfoil
c = 5
t = 0.12
rad = 1.019*t*t
def myNaca(x,y,z):
     if (x < 0):
       return  sqrt((x-rad)*(x-rad)+y*y)-rad
     elif (x > c):
       return  sqrt((x-c)*(x-c)+y*y)
     else:
       return fabs(y)-5.*t*c*(0.2969*(x/c)**(0.5)-0.1260*(x/c)-0.3516*(x/c)**2 + 0.2843*(x/c)**3-0.1015 *(x/c)**4)
 
print ('---- Load Mesh & Geometry')
meshName   = "rect"
dim = 2
CUT = True

#(1=MeshAdapt, 2=Automatic, 5=Delaunay, 6=Frontal, 7=bamg, 8=delquad)
GmshSetOption('Mesh', 'Algorithm', 1.0) 
     
print ('---- Generate a mesh')
#genMesh(meshName,dim,1)
g = GModel()
g.load(meshName+geo)

f0 = simpleFunctionPython(myNaca)
naca0012 = gLevelsetSimpleFunction(f0)

#naca0012A = gLevelsetMathEval (" (x/fabs(x)) * (y-5.*.12*5.0*(0.2969*(fabs(x)/5.0)^(1./2.)-0.1260*(fabs(x)/5.0)-0.3516*(x/5.0)^2 + 0.2843*(fabs(x)/5.0)^3 - 0.1015 *(x/5.0)^4 ))")
#naca0012B = gLevelsetMathEval (" (x/fabs(x)) *(-y-5.*.12*5.0*(0.2969*(fabs(x)/5.0)^(1./2.)-0.1260*(fabs(x)/5.0)-0.3516*(x/5.0)^2 + 0.2843*(fabs(x)/5.0)^3 - 0.1015 *(x/5.0)^4 ))")
#naca0012 = gLevelsetIntersection ( [ naca0012A , naca0012B ] )

#myLS = ls
myLS = naca0012

print ('---- Adapt mesh with ls')
adaptAllDim = True
g.adaptMesh([4], [myLS], [[0.5, 0.1, 1,25,1]], 10, adaptAllDim)
g.save("rect_ADAPT.msh")


if (CUT):	
	meshNameRESULT = "rect_CUT"
	g2 = g.buildCutGModel(myLS, CUT, True)
	g2.save(meshNameRESULT+msh)
else:
	meshNameRESULT = "rect_SPLIT"
	g2 = g.buildCutGModel(myLS, CUT, True)	
	g2.save(meshNameRESULT+msh)
