
H = 1;
lc = H*50/50;
Point(1) = {0, 0, 0, lc};
Point(2) = {-5*H, 0, 0, lc};
Point(3) = {-5*H, 8*H, 0, lc};
Point(4) = {100*H, 8*H, 0, lc};
Point(5) = {100*H, -H, 0, lc};
Point(6) = {0, -H, 0, lc};
Line(1) = {2, 3};
Line(2) = {3, 4};
Line(3) = {4, 5};
Line(4) = {5, 6};
Line(5) = {6, 1};
Line(6) = {1, 2};
Line Loop(7) = {3, 4, 5, 6, 1, 2};
Plane Surface(8) = {7};

Physical Surface("All") = {8};
Physical Line("Wall") = {6,5,4};
Physical Line("Inlet") = {1};
Physical Line("Outlet") = {3};
Physical Line("Symmetry") = {2};
//Physical Point("CornerBottomLeft") = {2};
