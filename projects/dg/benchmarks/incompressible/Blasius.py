from dgpy import *
from Incompressible import *
from math import *
import time
import sys

os.system("rm output/*")
os.system("rm Blasius_adapt*")
#-------------------------------------------------
#-- Blasius Boundary Layer
#-------------------------------------------------
#-------------------------------------------------
#-- Parameters
#-------------------------------------------------
rho = 1.0
mu = 0.01
UGlob = 1.0

nbTimeSteps = 10
timeOrder = 1
dt0 = 1000.
ATol = 1.e-5
RTol = 1.e-8
Verb = 10

ADAPT = True

if (ADAPT) :
  meshName = "BlasiusAdapt"
  nb_adapt_steps = 5
  lcMin = 1.e-3
  lcMax = 10.0
  tolMetric = 1.e-2
else :  
  meshName = "BlasiusUniform"
  nb_adapt_steps = 1
geoName = meshName

#-------------------------------------------------
#-- Initial function
#-------------------------------------------------
def initF(FCT, XYZ) :
  for i in range(0,FCT.size1()):
    y = XYZ.get(i,1)
    FCT.set(i,0, UGlob)
    FCT.set(i,1, 1.e-12*y*y*y) 
    FCT.set(i,2, 0.0) 

def InletBC(FCT, XYZ) :
  for i in range(0,FCT.size1()):
    FCT.set(i,0, UGlob)
    FCT.set(i,1, 0.0)
    FCT.set(i,2, 0.0)
    FCT.set(i,3, fixed)
    FCT.set(i,4, fixed)
    FCT.set(i,5, free)

#-------------------------------------------------
#-- Validation - COMPARISON WITH ANALYTICAL DATA
#-------------------------------------------------
def validateBlasius(ns) : 
    
  evalSol=dgFunctionEvaluator(ns.groups, ns.solution.getFunction())
  evalGrad=dgFunctionEvaluator(ns.groups,ns.solution.getFunctionGradient())
  
  analytical = open("BlasiusF.dat",'r')
  diff = 0.0
  result_sum  = 0.0
  
  line = (analytical.readline()).split()
  
  while line:
    eta = float(line[0])
    F = float(line[1])
    dF = float(line[2])
    line = (analytical.readline()).split()
  
    if (eta > 0.0) :
      result  = fullMatrixDouble(3,1)
      dresult = fullMatrixDouble(9,1)
      x = 35
      y = eta*math.sqrt(mu*x*2/UGlob)
      evalSol.compute(x,y,0.0,result)
      evalGrad.compute(x,y,0.0,dresult)
      diff = diff + fabs(dF - result.get(0,0)/UGlob)
      result_sum  = result_sum + fabs(dF)
                  
  analytical.close()

  fun = open("Blasius.dat","w")
  fun2 = open("Blasius2.dat","w")
  
  for i in range(0,200):
    fun.write("%g " % (i*3./200.0))
    for j in range(0,10):
      result = fullMatrixDouble(1,1)
      evalSol.compute(2*j, i*3./200.0,0.0,result)
      fun.write("%g " % (result.get(0,0)))
    fun.write("\n")
    result2 = fullMatrixDouble(1,1)
    evalGrad.compute(i*30/200.,1.e-2,0.0,result2)
    fun2.write("%g %g" % (i*30/200.,result2.get(0,0)) )
    fun2.write("\n")

  fun.close()
  fun2.close()

  if (diff/result_sum < 1.e-1) :
    print "BLASIUS - COMPARISON WITH ANALYTICAL DATA - SUCCESS :-)"
    return True
  else:
    print "BLASIUS - COMPARISON WITH ANALYTICAL DATA - FAIL :-("
    return False
    
#-------------------------------------------------
#-- Initialization
#-------------------------------------------------

print('MAX Reynolds (End plate)= ',UGlob*rho*35/mu);
print('BL Thickness @ x=10 = ',4.9*math.sqrt(mu*10/UGlob));
print('BL Thickness @ x=20 = ',4.9*math.sqrt(mu*20/UGlob));

genMesh(meshName,2,1)

for step in range(nb_adapt_steps+1):
  
  print "**************************************************"
  print "     Computing on adapted mesh: ITER ",   step
  print "**************************************************"

  rhoF = functionConstant(rho)
  muF = functionConstant(mu)
  ns = Incompressible(meshName, 2)
  ns.initializeIncomp(initF, rhoF, muF , UGlob)

  INLET    = functionPython(6, InletBC, [ns.XYZ])
  ns.strongBoundaryConditionLine('Inlet', INLET)
  ns.strongBoundaryConditionLine('Symmetry', ns.VELX)
  ns.strongBoundaryConditionLine('Wall', ns.WALL)
  ns.weakPresBoundaryCondition('Outlet', ns.PZERO)

  petscOptions ="-ksp_rtol 1.e-3 -pc_type ilu -pc_factor_levels 2"
  ns.pseudoTimeSteppingSolve(timeOrder, nbTimeSteps, dt0, ATol, RTol, Verb, petscOptions)
  ns.solution.exportFunctionSurf(ns.law.getWSS(), 'output/surface', 0, 0, 'Cf', ['Wall'])
  
  if (ADAPT) :
    adaptedMeshName = "Blasius_adapt%d" % step
    nbIter = 15
    allDim = True
    ns.adaptMesh(geoName, adaptedMeshName, tolMetric , lcMin, lcMax, nbIter, allDim)

  if (validateBlasius(ns)):
    sys.exit(success)


sys.exit(fail)

  

