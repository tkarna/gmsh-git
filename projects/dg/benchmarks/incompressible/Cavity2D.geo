/******************************       
Square uniformly meshed       
******************************/       
nb = 30;
lc = 1/(nb*3)*3;     
L = 1; // x 
l = 0.5; // y

// definition des points
Point(1) = {0, -l, 0, lc}; 
Point(2) = {L, -l, 0, lc};
Point(3) = {L, l, 0, lc};
Point(4) = {0, l, 0, lc};


Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};

Line Loop(4) = {1,2,3,4};
Plane Surface(5) = {4};
//Transfinite Line {1,3} = nb+1 Using Bump 0.2;
//Transfinite Line {2,4} = nb+1 Using Bump 0.2;
//Transfinite Surface {5} = {1,2,3,4} Right;
//Warning bottom left corner should be connected to interior mesh (right  is necessary fo this)

Physical Surface("All") = {5};
Physical Line("WallBottom") = {1};
Physical Line("WallTop") = {3};
Physical Line("Inlet") = {4};
Physical Line("Outlet") = {2};
Physical Point("CornerBottomLeft") = {1};





//Recombine Surface {5};
