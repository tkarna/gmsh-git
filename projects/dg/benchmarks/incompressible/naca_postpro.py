from math import *
from operator import itemgetter, attrgetter

File= open("output/surface_wss.txt","r")
class CoordVal:
        def __init__(self, x, y, z, val):
                self.x = x
                self.y = y
                self.z = z
                self.val = val
        def __repr__(self):
                return repr((self.x, self.y, self.z, self.val))


XYZV = []
lines = File.read().splitlines()
File.close()
for line in lines:
    lineSep = line.split(" ")
    x = float(lineSep[0])
    y = float(lineSep[1])
    z = float(lineSep[2])
    val = float(lineSep[3])
    XYZV.append(CoordVal(x,y,z,val))

fun = open("output/surface_wss_post.txt","w")

XYZV.sort(key=lambda CoordVal: CoordVal.x)

xOld = XYZV[0].x
yOld = XYZV[0].y
s = 0
#print  'i', 0, 'X',  XYZV[0].x, 'Y',  XYZV[0].y, 's', s, 'val', XYZV[0].val
fun.write("%g %g %g \n" % (XYZV[0].x, s, XYZV[0].val))
for i in range (1,len(XYZV)):
    if (XYZV[i].y > 0.0):
        s = s + sqrt((XYZV[i].x-xOld)*(XYZV[i].x-xOld)+(XYZV[i].y-yOld)*(XYZV[i].y-yOld))
        fun.write("%g %g %g \n" % (XYZV[i].x, s, XYZV[i].val))
        xOld = XYZV[i].x
        yOld = XYZV[i].y
        #print  'i', i, 'X',  XYZV[i].x, 'Y',  XYZV[i].y, 's', s, 'val', XYZV[i].val
fun.close()
print 'chord length = ', s
