from dgpy import *
from Incompressible import *
from math import *
import time
import sys

os.system("rm output/*")
os.system("rm Naca_adapt*")
os.system("rm LiftAndDrag.dat")
#-------------------------------------------------
#-- Naca Airfoil Re=5000 angle = 0
#-------------------------------------------------
#-------------------------------------------------
#-- Parameters
#-------------------------------------------------
rho = 1.0
mu = 0.001
UGlob = 5
Chord = 1.0

flowDir=[1., 0., 0.]
spanDir=[0., 1., 0.]
fScale = 0.5*rho*UGlob*UGlob*Chord

t = 0.12
lsNACA = gLevelsetNACA00(0,0,Chord,t)

meshName = "rect12"
  
nbTimeSteps = 20
timeOrder = 1
dt0 = 0.1
ATol = 1.e-5
RTol = 1.e-8
Verb = 2

#-------------------------------------------------
#-- Initial function
#-------------------------------------------------
def initF(FCT, XYZ) :
  for i in range(0,FCT.size1()):
    FCT.set(i,0, UGlob)
    FCT.set(i,1, 0.) 
    FCT.set(i,2, 0.) 

def InletBC(FCT, XYZ) :
  for i in range(0,FCT.size1()):
    FCT.set(i,0, UGlob)
    FCT.set(i,1, 0.)
    FCT.set(i,2, 0.)
    FCT.set(i,3, fixed)
    FCT.set(i,4, fixed)
    FCT.set(i,5, free)

def normVelFunc(FCT, sol):
	for i in range (0,sol.size1()):
		vx = sol.get(i,0)
		vy = sol.get(i,1)
		FCT.set(i,0,math.sqrt(vx*vx+vy*vy))
    
#-------------------------------------------------
#-- Initialization
#-------------------------------------------------

print '************** MAX Reynolds = ', rho*UGlob*Chord/mu

E = 0.2
lcMin = 1.e-4
lcMax = 0.4
Np = 35
E_minus = 0.1
adaptedCutMeshName = "Naca_adapt_cut"
GmshSetOption("Mesh","SmoothRatio",  1.8);

mesh = GModel()
mesh.load(meshName+".geo")
mesh.adaptMesh([5], [lsNACA], [[E, lcMin, lcMax, Np, E_minus]], 30, False)
meshCut = mesh.buildCutGModel(lsNACA, False, True)
meshCut.writeMSH(adaptedCutMeshName+".msh",2.2,False,False,True,1.0,0,0)
meshName = adaptedCutMeshName

print "**************************************************"
print "     Computing NS "
print "**************************************************"

rhoF = functionConstant(rho)
muF = functionConstant(mu)
ns = Incompressible(meshName, 2, ["domain_out"])
ns.initializeIncomp(initF, rhoF, muF , UGlob)

INLET  = functionPython(6, InletBC, [ns.XYZ])
ns.strongBoundaryConditionLine('Inlet', INLET)
ns.strongBoundaryConditionLine('Symmetry', ns.VELX)
ns.strongBoundaryConditionLine('levelset_L-1', ns.WALL)
ns.weakPresBoundaryCondition('Outlet', ns.PZERO)

#petscOptions ="-ksp_rtol 1.e-3 -pc_type ilu  -pc_factor_levels 0"
petscOptions ="-ksp_rtol 1.e-4 -pc_type lu -ksp_monitor -pc_factor_levels 0"
ns.pseudoTimeSteppingSolve(timeOrder, nbTimeSteps, dt0, ATol, RTol,Verb, petscOptions)
#ns.computeDragAndLift('Wall', 0, 0, fScale, flowDir, spanDir, 'LiftAndDrag.dat')

ns.solution.exportFunctionSurf(ns.law.getWSSNorm(), 'output/surface', 0, 0, 'wss', ['levelset_L-1'])
ns.solution.exportFunctionSurf(ns.law.getPressure(), 'output/surface', 0, 0, 'pres', ['levelset_L-1'])
ns.solution.exportMshNodeData("output/sol",0,0)

hFD = 0.1*lcMin
nbFD = 5
name = "levelset_L-1"
dirPerp = SVector3(0.0,0.0,1.0)
                            
ns.computeWallShearStressFD(ns.model, dirPerp, hFD, nbFD, mu, name, 1.0)
ns.computeWallShearStressLEAST_SQUARES(mu, name, fScale)
ns.computeWallShearStressEIGEN(mu, name, 1.0)
ns.computeWallShearStressL2FE(name)
