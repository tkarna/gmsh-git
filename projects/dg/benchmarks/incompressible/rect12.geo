
lc = 0.5;

Point(1) = {-4,-4,0.0,lc};         
Point(2) = {-4,4,0.0,lc};         
Point(3) = {16,4,0.0,lc};         
Point(4) = {16,-4,0.0,lc};    

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

Line Loop(5) = {1, 2, 3, 4};
Plane Surface(6) = {5};

Physical Line("Inlet")={1};
Physical Line("Symmetry")={2,4};
Physical Line("Outlet")={3};

Physical Surface("domain")={6};

