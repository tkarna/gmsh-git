Include "RealisticBifurcationPoints.geo";
Mesh.CharacteristicLengthFactor = 10;

Spline(1) = {149:104};
Line(2) = {104,103};
Spline(3) = {103:70};
Line(4) = {70,69};
Spline(5) = {69:1};
Line(6) = {1,149};

Line Loop(1) = {1,2,3,4,5,6};

Plane Surface(1) = {1};


Physical Line("Inlet") = {2};
Physical Line("DeadArtery") = {4};
Physical Line("Outlet") = {6};
Physical Line("Wall") = {1,3,5}; //All splines
Physical Surface("Cells") = {1};

