require("Incompressible2D")

-------------------------------------------------
-- Jet impinging upon wall
-------------------------------------------------

-- This problem consists of a jet impinging upon a wall with a controlled body force 
-- on a domain Ω = [0, 1] × [0, 1] and can be found e.g. in Hendriana and Bathe (2000). 
-- A slip boundary condition is assumed on the surface of the left wall.
-- The body force functions are f1 = 5xy^8 + 10xy^3 + 60*nu*xy^2, f2 = 0	
-- The fluid density is rho = 1.
-- Two quite different flow regimes are considere, a diffusion-dominated flow with 
-- nu = 10 (Re=0.5025) and a convection-dominated flow with nu = 0.001 (Re=5025).
-- The analytical solution of the problem then reads:
-- u1 = − 5xy^4	
-- u2 = − 0.5 + y^5	
-- p = 0.5*(y^5-y^10) + 5*nu*y^4

-------------------------------------------------
-- Parameters
-------------------------------------------------
rho = 1.0
nu = 10    --Re=0.5
--nu = 0.001   --Re = 5025
UGlob = 0.5
meshName = "Jet16X16"

-------------------------------------------------
-- Initial function
-------------------------------------------------

function initF( XYZ, FCT )
  for i=0,FCT:size1()-1 do
     x = XYZ:get(i,0)
     y = XYZ:get(i,1)
     uana = -5.0*x*math.pow(y,4)
     vana = -0.5+math.pow(y,5)
     pana =  0.5*(math.pow(y,5)-math.pow(y,10))+5*nu*math.pow(y,4)
    FCT:set(i,0, uana) --u
    FCT:set(i,1, vana) --v
    FCT:set(i,2, 0.0) --p
  end
end

-------------------------------------------------
-- Case specific functions
-------------------------------------------------
function solAna( XYZ, FCT )
  for i=0,FCT:size1()-1 do
    x = XYZ:get(i,0)
    y = XYZ:get(i,1)
    uana = -5.0*x*math.pow(y,4)
    vana = -0.5+math.pow(y,5)
    pana =  0.5*(math.pow(y,5)-math.pow(y,10))+5*nu*math.pow(y,4)
    FCT:set(i,0, uana)
    FCT:set(i,1, vana )
    FCT:set(i,2, pana) 
  end
end

function sourceF( XYZ, FCT )
  for i=0,FCT:size1()-1 do
     x = XYZ:get(i,0)
     y = XYZ:get(i,1)
     f1 =  5.*x*math.pow(y,8.)+10*x*math.pow(y,3.)+60*nu*x*math.pow(y,2.)
     f2 = 0.0
     FCT:set(i,0, f1) 
     FCT:set(i,1, f2) 
     FCT:set(i,2, 0.0)
  end
end

function VelLeftBC( XYZ, FCT )
   for i=0,FCT:size1()-1 do
    FCT:set(i,0, 0.0)
    FCT:set(i,1, 0.0)
    FCT:set(i,2, 0.0)
    FCT:set(i,3, fixed)
    FCT:set(i,4, free)
    FCT:set(i,5, free)
  end
end

function VelTopBC( XYZ, FCT )
   for i=0,FCT:size1()-1 do
    x = XYZ:get(i,0)
    FCT:set(i,0, -5*x)
    FCT:set(i,1, 0.5)
    FCT:set(i,2, 0.0)
    FCT:set(i,3, fixed)
    FCT:set(i,4, fixed)
    FCT:set(i,5, free)
  end
end

function VelRightBC( XYZ, FCT )
   for i=0,FCT:size1()-1 do
    y = XYZ:get(i,1)
    FCT:set(i,0, -5.0*math.pow(y,4))
    FCT:set(i,1, -0.5+math.pow(y,5))
    FCT:set(i,2, 0.0)
    FCT:set(i,3, fixed)
    FCT:set(i,4, fixed)
    FCT:set(i,5, free)
  end
end

ANA  = functionLua(3, 'solAna', {xyz})
SOURCE  = functionLua(3, 'sourceF', {xyz})
VELLEFT  = functionLua(6, 'VelLeftBC',  {xyz})
VELTOP  = functionLua(6, 'VelTopBC',  {xyz})
VELRIGHT = functionLua(6, 'VelRightBC', {xyz})

-------------------------------------------------
-- Initialization
-------------------------------------------------
initializeIncomp(meshName, rho, nu, UGlob)
law:setSource(SOURCE) 

-------------------------------------------------
-- Boundary Conditions
-------------------------------------------------

strongBoundaryCondition('Outlet', VELRIGHT)
strongBoundaryCondition('Inlet', VELLEFT)
strongBoundaryCondition('WallTop', VELTOP)
strongBoundaryCondition('WallBottom', PRES)

--analytical = dgDofContainer(groups, law:getNbFields())
--analytical:L2Projection(ANA)
--analytical:exportFunctionMsh(law:getVelocity(), string.format('output/anaVel'), 0, 0, 'vel')
--analytical:exportFunctionMsh(law:getPressure(), string.format('output/anaPres'), 0, 0, 'pres')

-------------------------------------------------
-- Pseudo TimeStepping
-------------------------------------------------

nbTimeSteps = 40
dt0 = 1000
ATol = 1.e-5
RTol = 1.e-8
petscOptions ="-ksp_rtol 1.e-3 -pc_type ilu -pc_factor_levels 0"

peudoTimeSteppingSolve(nbTimeSteps, dt0, ATol, RTol, petscOptions)

-------------------------------------------------
-- Validation 
-------------------------------------------------

print('---- Computing Error of Velocity and Pressure')
function L2ErrorPres( sol, ana, FCT )
  for i=0,sol:size1()-1 do
    p = sol:get(i,2)
    pana = ana:get(i,2)
    FCT:set(i,0,(p-pana)*(p-pana)) 
  end
end
function L2NormPres( sol, FCT )
  for i=0,sol:size1()-1 do
    p = sol:get(i,2)
    FCT:set(i,0,p*p) 
  end
end
function L2ErrorVel( sol, ana, FCT )
  for i=0,sol:size1()-1 do
    u = sol:get(i,0)
    v = sol:get(i,1)
    uana = ana:get(i,0)
    vana = ana:get(i,1)
    FCT:set(i,0,(u-uana)*(u-uana)+(v-vana)*(v-vana)) 
  end
end
function L2NormVel( sol, FCT )
  for i=0,sol:size1()-1 do
    u = sol:get(i,0)
    v = sol:get(i,1)
    FCT:set(i,0,u*u+v*v) 
  end
end

integErrorVel =dgFunctionIntegrator(functionLua(1, 'L2ErrorVel', {functionSolution.get(), ANA}))
integErrorPres=dgFunctionIntegrator(functionLua(1, 'L2ErrorPres', {functionSolution.get(), ANA}))
integVel =dgFunctionIntegrator(functionLua(1, 'L2NormPres', {functionSolution.get()}))
integPres=dgFunctionIntegrator(functionLua(1, 'L2NormVel', {functionSolution.get()}))

errorVel=fullMatrix(1,1)
errorPres=fullMatrix(1,1)
lsqVel=fullMatrix(1,1)
lsqPres=fullMatrix(1,1)

integErrorVel:compute(solution, errorVel)
integErrorPres:compute(solution,errorPres)
integVel:compute(solution,lsqVel)
integPres:compute(solution,lsqPres)

normVel=math.sqrt(errorVel:get(0,0)/lsqVel:get(0,0))
normPres=math.sqrt(errorPres:get(0,0)/lsqPres:get(0,0))

print(string.format("L2-norm for Vel=%g log=%g  ",normVel, math.log10(normVel)))
print(string.format("L2-norm for Pres=%g log=%g ",normPres, math.log10(normPres)))