
lc = 10;

//l =  4.5; // before cyl.
//L = 20.0; // after cyl.
//H =  4.5; // half of height
//X =  0.0; // center of cyl.
//Y =  0.0; // center of cyl.
//r = 0.5;  // radius of cyl.

//Point(1) = {-l-X,-Y-H-r,0.0,lc};         
//Point(2) = {-l-X,-Y+H,0.0,lc};         
//Point(3) = {-l-X+L,-Y+H,0.0,lc};         
//Point(4) = {-l-X+L,-Y-H-r,0.0,lc}; 

Point(1) = {-100,-100,0.0,lc};         
Point(2) = {-100,100,0.0,lc};         
Point(3) = {100,100,0.0,lc};         
Point(4) = {100,-100,0.0,lc};    

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

Line Loop(5) = {1, 2, 3, 4};
Plane Surface(6) = {5};

Physical Line("inlet")={1};
Physical Line("top")={2};
Physical Line("outlet")={3};
Physical Line("bottom")={4};

Physical Surface("domain")={6};

