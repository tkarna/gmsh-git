require("Incompressible2D")


function V( sol, FCT )
  for i=0,sol:size1()-1 do
    vx = sol:get(i,0)
    vy = sol:get(i,1)
    FCT:set(i,0,math.sqrt(vx*vx+vy*vy)) 
  end
end

-------------------------------------------------
-- 2D Lid-driven cavity
-------------------------------------------------

-------------------------------------------------
-- Parameters
-------------------------------------------------
rho = 1.0
nu = .001
UGlob = 1
meshName = "Cavity2D"

-------------------------------------------------
-- Initial function
-------------------------------------------------
function initF( XYZ, FCT )
  for i=0,FCT:size1()-1 do
    FCT:set(i,0, 0.1) --u
    FCT:set(i,1, 0.0) --v
    FCT:set(i,2, 0.0) --p
  end
end

-------------------------------------------------
-- Case specific functions
-------------------------------------------------
function VelBC( XYZ, FCT )
   for i=0,FCT:size1()-1 do
    x = XYZ:get(i,0)
    impU  = 1.0*(math.pow(1-math.pow(x-0.5,18)/0.5,2))
    FCT:set(i,0,  impU)
    --FCT:set(i,0, 1.0)
    FCT:set(i,1, 0.0)
    FCT:set(i,2, 0.0)
    FCT:set(i,3, fixed)
    FCT:set(i,4, fixed)
    FCT:set(i,5, free)
  end
end
VEL  = functionLua(6, 'VelBC',  {xyz})

-------------------------------------------------
-- Initialization
-------------------------------------------------
initializeIncomp(meshName, rho, nu, UGlob)

-------------------------------------------------
-- Boundary Conditions
-------------------------------------------------
strongBoundaryCondition('Inlet', WALL)
strongBoundaryCondition('WallBottom', WALL)
strongBoundaryCondition('Outlet', WALL)
strongBoundaryCondition('WallTop', VEL)
strongBoundaryConditionPoint('CornerBottomLeft', PRES)

-------------------------------------------------
-- Pseudo TimeStepping
-------------------------------------------------

nbTimeSteps = 40
dt0 = .001
ATol = 1.e-5
RTol = 1.e-8
petscOptions ="-ksp_rtol 1.e-3 -pc_type lu -pc_factor_levels 2"

peudoTimeSteppingSolve(nbTimeSteps, dt0, ATol, RTol, petscOptions)

evaluatorOfSolution=dgFunctionEvaluator(groups, functionLua(1, 'V', {solution:getFunction()}))
BGM = dgMeshMetricBasedOnHessian(groups, evaluatorOfSolution,3.e-3)
modelAdapt = GModel()
modelAdapt:load('Cavity2D.geo')
BGM:setAsBackgroundMesh(modelAdapt)
modelAdapt:mesh(2)
modelAdapt:save('Cavity2DAdapted.msh')


