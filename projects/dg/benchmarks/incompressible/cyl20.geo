R = 0.05 ;
l = 0.2; //before cylinder
L = 2; //after cylinder
H = 0.2; //above and under cylinder
H2 = 0.21;

p = 0;
r = 0;

lc = 0.1;
lc2 = 0.6;
X = -0.2;
Y = -0.2;

lc1 = 0.0005;
lc2 = 0.05;
Point(1) = {0-X,0-Y,0,lc1};
Point(2) = {R-X,0-Y,0,lc1};
Point(3) = {0-X,R-Y,0,lc1};
Point(4) = {0-X,-R-Y,0,lc1};
Point(5) = {-R-X,0-Y,0,lc1};
Point(6) = {-l-X,-H-r-Y,0,lc2};
Point(7) = {-l-X,H2-Y,0,lc2};
Point(8) = {-l+L-X,H2-Y,0,lc2};
Point(9) = {-l+L-X,-H-r-Y,0,lc2};

Circle(1) = {3,1,2};
Circle(2) = {2,1,4};
Circle(3) = {4,1,5};
Circle(4) = {5,1,3};

Line(5) = {7,8};
Line(6) = {8,9};
Line(7) = {9,6};
Line(8) = {6,7};
Line Loop(9) = {5,6,7,8};
Line Loop(10) = {3,4,1,2};
Plane Surface(11) = {9,10};

Physical Surface("All") = {11};
Physical Line("Cylinder") = {1,2,3,4};
Physical Line("WallBottom") = {7};
Physical Line("WallTop") = {5};
Physical Line("Inlet") = {8};
Physical Line("Outlet") = {6};

Field[1] = BoundaryLayer;
Field[1].EdgesList = {1,2,3,4};
Field[1].hfar = lc2;
Field[1].hwall_n = lc1;
Field[1].hwall_t = lc1;
Field[1].ratio = 1.2;
Field[1].thickness = .01;
Background Field = 1;
