set xlabel 'x'
set ylabel 'tau wall'

plot 'geomBL_FD.txt' u 1:4, 'geomBL_FE.txt' u 1:4, 'geomBL_LS.txt' u 1:4, 'geomANISO_FD.txt' u 1:4, 'geomANISO_FE.txt' u 1:4, 'geomANISO_LS.txt' u 1:4