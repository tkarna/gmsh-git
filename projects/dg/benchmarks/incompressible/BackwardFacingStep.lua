require("Incompressible2D")

-------------------------------------------------
-- Backward Facing Step
-------------------------------------------------

-------------------------------------------------
-- Parameters
-------------------------------------------------
rho = 1.0
nu  = 0.001
UGlob = 0.2
meshName = "BackwardFacingStep"

-------------------------------------------------
-- Initial function
-----------------------------------------------
function initF( XYZ, FCT )
  for i=0,FCT:size1()-1 do
    y = XYZ:get(i,1)
    FCT:set(i,0, UGlob) 
    FCT:set(i,1, 1.e-12*y*y*y) 
    FCT:set(i,2, 0.0) 
  end
end
-------------------------------------------------
-- Case specific functions
-------------------------------------------------
function InletBC( XYZ, FCT )
   for i=0,FCT:size1()-1 do
    FCT:set(i,0, UGlob)
    FCT:set(i,1, 0.0)
    FCT:set(i,2, 0.0)
    FCT:set(i,3, fixed)
    FCT:set(i,4, fixed)
    FCT:set(i,5, free)
  end
end

function SymmetryBC( XYZ, FCT )
   for i=0,FCT:size1()-1 do
    FCT:set(i,0, 0.0)
    FCT:set(i,1, 0.0)
    FCT:set(i,2, 0.0)
    FCT:set(i,3, free)
    FCT:set(i,4, fixed)
    FCT:set(i,5, free)
  end
end
INLET = functionLua(6, 'InletBC', {xyz})
SYMMETRY = functionLua(6, 'SymmetryBC', {xyz})

-------------------------------------------------
-- Initialization
-------------------------------------------------
options = gmshOptions()
options:numberSet('Mesh', 0, 'Algorithm', 7.0)--bamg
options:numberSet('Mesh', 0, 'LcIntegrationPrecision', 1.e-3)

initializeIncomp(meshName, rho, nu, UGlob)

print('MAX Reynolds = ',UGlob*35/nu);
print('BL Thickness @ x=10 = ',4.9*math.sqrt(nu*10/UGlob));
print('BL Thickness @ x=20 = ',4.9*math.sqrt(nu*20/UGlob));

-------------------------------------------------
-- Boundary Conditions
-------------------------------------------------
strongBoundaryCondition('Wall', WALL)
strongBoundaryCondition('Inlet', INLET)
strongBoundaryCondition('Symmetry', SYMMETRY)
--law:addBoundaryCondition('Symmetry', law:newSymmetryBoundary('Symmetry'))
weakPresBoundaryCondition('Outlet', PRESZERO)

-------------------------------------------------
-- Pseudo TimeStepping
-------------------------------------------------

nbTimeSteps = 40
dt0 = 100.0
ATol = 1.e-5
RTol = 1.e-8
petscOptions ="-ksp_rtol 1.e-3 -pc_type ilu -pc_factor_levels 2"

peudoTimeSteppingSolve(nbTimeSteps, dt0, ATol, RTol, petscOptions)

-------------------------------------------------
-- Mesh adaptation
-------------------------------------------------

adaptedMeshName = "BackwardFacingStepAdaped"
adaptMesh(meshName,adaptedMeshName, 1.e-3)
