R = 0.5 ;
l = 4.5; //before cylinder
L = 20; //after cylinder
H = 4.5; //above and under cylinder

p = 0;
r = 0;

lc = 0.1;
lc2 = 0.6;
X = 0;
Y = 0;

Point(1) = {0-X,0-Y,0,0.1};
Point(2) = {R-X,0-Y,0,0.1};
Point(3) = {0-X,R-Y,0,0.1};
Point(4) = {0-X,-R-Y,0,0.1};
Point(5) = {-R-X,0-Y,0,0.1};
Point(6) = {-l-X,-H-r-Y,0,0.6};
Point(7) = {-l-X,H-Y,0,0.6};
Point(8) = {-l+L-X,H-Y,0,0.6};
Point(9) = {-l+L-X,-H-r-Y,0,0.6};

Circle(1) = {3,1,2};
Circle(2) = {2,1,4};
Circle(3) = {4,1,5};
Circle(4) = {5,1,3};

Line(5) = {7,8};
Line(6) = {8,9};
Line(7) = {9,6};
Line(8) = {6,7};
Line Loop(9) = {5,6,7,8};
Line Loop(10) = {3,4,1,2};
Plane Surface(11) = {9,10};

Compound Line(100)={1,2,3,4};
Compound Surface(200)={11};

Physical Surface("All") = {11};
Physical Line("Cylinder") = {1,2,3,4};
Physical Line("WallBottom") = {7};
Physical Line("WallTop") = {5};
Physical Line("Inlet") = {8};
Physical Line("Outlet") = {6};
