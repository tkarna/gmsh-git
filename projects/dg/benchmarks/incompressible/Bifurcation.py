from Incompressible import *
from dgpy import *
from math import *
import time
import sys


#-------------------------------------------------
#-- Blasius Boundary Layer
#-------------------------------------------------

#-------------------------------------------------
#-- Parameters
#-------------------------------------------------
rho = 1.0
nu = 0.01
UGlob = 2

meshAdapt = 1 

if (meshAdapt) :
  meshName = "Ybifurcation"
  #meshName = "RealisticBifurcationMesh"
  geoName = "Ybifurcation_Adapt" 
  #geoName = "RealisticBifurcation_Adapt" 
  GmshSetOption('Mesh', 'Algorithm', 7.0) #7=bamg
  GmshSetOption('Mesh', 'LcIntegrationPrecision', 1.e-4) #epslc1d
  nb_adapt_steps = 20
else :  
  meshName = "FluidInside"
  nb_adapt_steps = 1

#-------------------------------------------------
#-- Initial function
#-------------------------------------------------
def initF(FCT, XYZ) :
  for i in range(0,FCT.size1()):
    y = XYZ.get(i,1)
    FCT.set(i,0, 1.e-12*y*y*y)
    FCT.set(i,1, UGlob) 
    #FCT.set(i,1, 0.0) 
    FCT.set(i,2, 0.0) 

#-------------------------------------------------
#-- Case specific functions
#-------------------------------------------------

def InletBC(FCT, XYZ) :
  for i in range(0,FCT.size1()):
    FCT.set(i,0, 0.0)
    FCT.set(i,1, UGlob)
    #FCT.set(i,1, UGlob*(1-(x/0.5)*(x/0.5)))
    FCT.set(i,2, 0.0)
    FCT.set(i,3, fixed)
    FCT.set(i,4, fixed)
    FCT.set(i,5, free)
"""
def SymmetryBC(FCT, XYZ) :
  for i in range(0,FCT.size1()):
    FCT.set(i,0, 0.0)
    FCT.set(i,1, 0.0)
    FCT.set(i,2, 0.0)
    FCT.set(i,3, free)
    FCT.set(i,4, fixed)
    FCT.set(i,5, free)
"""

#-------------------------------------------------
#-- Pseudo TimeStepping Variables
#-------------------------------------------------

nbTimeSteps = 20
dt0 = 1000.
ATol = 1.e-5
RTol = 1.e-8

#-------------------------------------------------
#-- Initialization
#-------------------------------------------------



print('MAX Reynolds (End plate)= ',UGlob*rho*35/nu);
print('BL Thickness @ x=10 = ',4.9*math.sqrt(nu*10/UGlob));
print('BL Thickness @ x=20 = ',4.9*math.sqrt(nu*20/UGlob));

for step in range(nb_adapt_steps+1):
  
  print "**************************************************"
  print "     Computing on adapted mesh number %d" % step
  print "**************************************************"
  
  ns = Incompressible(meshName, 2)	
  rhoF = functionConstant(rho)
  nuF = functionConstant(nu)
  ns.initializeIncomp(initF, rhoF, nuF , UGlob)

  #-------------------------------------------------
  #-- Boundary Conditions
  #-------------------------------------------------

  INLET    = functionPython(6, InletBC, [ns.XYZ])
  #SYMMETRY = functionPython(6, SymmetryBC, [ns.XYZ])

  ns.strongBoundaryConditionLine('Inlet', INLET)
  #ns.strongBoundaryConditionLine('DeadArtery', ns.WALL)
  ns.strongBoundaryConditionLine('Wall', ns.WALL)
  ns.weakPresBoundaryCondition('Outlet', ns.PZERO)

  #-------------------------------------------------
  #-- solver
  #-------------------------------------------------
timeOrder = 2  
  petscOptions ="-ksp_rtol 1.e-3 -pc_type ilu -pc_factor_levels 0"
  #petscOptions ="-ksp_rtol 1.e-3 -pc_type lu -ksp_monitor -pc_factor_levels 2"
  ns.pseudoTimeSteppingSolve(timeorder, nbTimeSteps, dt0, ATol, RTol, True, petscOptions)

  #-------------------------------------------------
  #-- mesh Adaptation
  #-------------------------------------------------
  print "Just before adaptation"
  if (meshAdapt) : 
    adaptedMeshName = "Bifurc_adapt%d" % step
    ns.adaptMesh(geoName, adaptedMeshName, 1.e-2, 0.06,1.0)

    meshName = adaptedMeshName
  
  
  ns.solution.exportFunctionSurf(ns.law.getWSS(), 'output/surface', 0, 0, 'wss', ['Wall'])
  print "The End"
"""
#-------------------------------------------------
#-- Validation 
#-------------------------------------------------
print "**************************************************"
print "     BLASIUS - COMPARISON WITH ANALYTICAL DATA"
print "**************************************************"

evalSol=dgFunctionEvaluator(ns.groups, ns.solution.getFunction())
evalGrad=dgFunctionEvaluator(ns.groups,ns.solution.getFunctionGradient())

analytical = open("BlasiusF.dat",'r')
diff = 0.0
result_sum  = 0.0

line = (analytical.readline()).split()

while line:
  eta = float(line[0])
  F = float(line[1])
  dF = float(line[2])
  #print "%f %f %f" % (eta,F,dF)
  line = (analytical.readline()).split()

  if (eta > 0.0) :
    result  = fullMatrixDouble(3,1)
    dresult = fullMatrixDouble(9,1)
    x = 35
    y = eta*math.sqrt(nu*x*2/UGlob)
    evalSol.compute(x,y,0.0,result)
    evalGrad.compute(x,y,0.0,dresult)
    diff = diff + fabs(dF - result.get(0,0)/UGlob)
    result_sum  = result_sum + fabs(dF)
    #print "%f %f" % (dF,result.get(0,0)/UGlob)

analytical.close()

#-- Gnuplot
fun = open("Blasius.dat","w")
fun2 = open("Blasius2.dat","w")

for i in range(0,200):
  fun.write("%g " % (i*3./200.0))
  for j in range(0,10):
    result = fullMatrixDouble(1,1)
    evalSol.compute(2*j, i*3./200.0,0.0,result)
    fun.write("%g " % (result.get(0,0)))

  fun.write("\n")
  result2 = fullMatrixDouble(1,1)
  evalGrad.compute(i*30/200.,1.e-2,0.0,result2)
  fun2.write("%g %g" % (i*30/200.,result2.get(0,0)) )
  fun2.write("\n")


fun.close()
fun2.close()

#os.system("gnuplot Blasius.gnu")

#-- End gnuplot
 
if (diff/result_sum < 1.e-1) :
 print "Exit with success :-)"
 sys.exit(success)
else:
 print "Exit with fail:-("
 sys.exit(fail)
"""

