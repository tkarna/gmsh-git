from dgpy import *
from Incompressible import *
from math import *
import sys

os.system("rm output/*")
os.system("rm LiftAndDrag.dat")
#-------------------------------------------------
#-- Cylinder at Reynolds 100
#-------------------------------------------------

#-------------------------------------------------
#-- Parameters
#-------------------------------------------------
RHO = 1.0
MU  = 0.01
UGlob = 1.0
meshName = "Cyl100LS"

print('*** Reynolds= %g' % (RHO*UGlob*1/MU))

#-------------------------------------------------
#-- Initial function
#-------------------------------------------------
def initF(FCT, XYZ):
	for i in range(0,FCT.size1()):
		FCT.set(i,0, 0.0) #u
		FCT.set(i,1, 0.0) #v
		FCT.set(i,2, 0.0) #p

def initLS(FCT, XYZ):
	for i in range(0,FCT.size1()):
          	x = XYZ.get(i,0)
                y = XYZ.get(i,1)
                phi = math.sqrt((x)*(x) + (y)*(y))-.5
		FCT.set(i,0, phi) 

#-------------------------------------------------
#-- Case specific functions
#-------------------------------------------------
def VelBC(FCT, XYZ ):
	vmax = 2.0;
	for i in range(0,FCT.size1()):
		FCT.set(i,0, UGlob)
		FCT.set(i,1, 0.0)
		FCT.set(i,2, 0.0)
		FCT.set(i,3, fixed)
		FCT.set(i,4, fixed)
		FCT.set(i,5, free)

#-------------------------------------------------
#-- Initialization
#-------------------------------------------------
ns = Incompressible(meshName, 2)

INIT = functionPython(1, initLS, [ns.XYZ])
rhoF = functionLevelsetSmoothNew(INIT, -1.0, RHO, 0.3)
muF = functionLevelsetSmoothNew(INIT, -1.0, MU, 0.3)

test = dgDofContainer(ns.groups, 1);
test.interpolate(rhoF)
out4 = "output/rho" 
test.exportMsh(out4, 0., 0)
exit(1)

ns.initializeIncomp(initF, rhoF, muF , UGlob)

#-------------------------------------------------
#-- Boundary Conditions
#-------------------------------------------------

VEL = functionPython(6, VelBC, [ns.XYZ])
ns.strongBoundaryConditionLine('Inlet', VEL)
ns.weakPresBoundaryCondition('Outlet', ns.PZERO)
ns.weakSlipBoundaryCondition('WallBottom')
ns.weakSlipBoundaryCondition('WallTop')

#-------------------------------------------------
#-- TimeStepping
#-------------------------------------------------

timeOrder = 2
ATol = 1.e-5
RTol = 1.e-8
Verb = 2
nbExport = 10
petscOptions =""
 
ns.initSolver(timeOrder, ATol,RTol, Verb, petscOptions, nbExport)

flowDir=[1., 0., 0.]
spanDir=[0., 1., 0.]
D = 1.0
fScale = 0.5*RHO*UGlob*UGlob*D
dt = 0.1

limiter = dgSlopeLimiter(ns.law) #dgLimiterClip(claw)
ns.explicitSolver.setLimiter(limiter)

t = 0.0
tic = time.clock()
for i in range (0,1200) :
	Msg.Info("*** Iter %i t=%.2g dt=%.2g cpu=%.2f" % (i,t,dt,time.clock()-tic))
	if (i % nbExport == 0):
		ns.computeDragAndLift('Cylinder', t, i, fScale, flowDir, spanDir, 'LiftAndDrag.dat')
	limiter.apply(ns.solution) 	
	ns.doOneExplicitIteration(i)
	
	t = t+dt
	
#-------------------------------------------------
#-- Validation 
#-------------------------------------------------


