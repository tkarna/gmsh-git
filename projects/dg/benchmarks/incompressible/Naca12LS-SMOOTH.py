from dgpy import *
from Incompressible import *
from math import *
import time
import sys

os.system("rm output/*")
os.system("rm Naca_adapt*")
os.system("rm LiftAndDrag.dat")
#-------------------------------------------------
#-- Naca Airfoil Re=5000 angle = 0
#-------------------------------------------------
#-------------------------------------------------
#-- Parameters
#-------------------------------------------------
rho = 1.0
mu = 0.1 #0.001
UGlob = 5
Chord = 1.0

flowDir=[1., 0., 0.]
spanDir=[0., 1., 0.]
fScale = 0.5*rho*UGlob*UGlob*Chord

t = 0.12
lsNACA = gLevelsetNACA00(0,0,Chord,t)

meshName = "rect12"
  
nbTimeSteps = 20
timeOrder = 1
dt0 = 0.01
ATol = 1.e-3
RTol = 1.e-2
Verb = 2

#-------------------------------------------------
#-- Initial function
#-------------------------------------------------
def initF(FCT, XYZ) :
  for i in range(0,FCT.size1()):
    FCT.set(i,0, 0.)
    FCT.set(i,1, 0.) 
    FCT.set(i,2, 0.) 

def InletBC(FCT, XYZ) :
  for i in range(0,FCT.size1()):
    FCT.set(i,0, UGlob)
    FCT.set(i,1, 0.)
    FCT.set(i,2, 0.)
    FCT.set(i,3, fixed)
    FCT.set(i,4, fixed)
    FCT.set(i,5, free)

def normVelFunc(FCT, sol):
	for i in range (0,sol.size1()):
		vx = sol.get(i,0)
		vy = sol.get(i,1)
		FCT.set(i,0,math.sqrt(vx*vx+vy*vy))
    
#-------------------------------------------------
#-- Initialization
#-------------------------------------------------
print '************** MAX Reynolds = ', rho*UGlob*Chord/mu

E = 0.15
lcMin = 1.e-3
lcMax = 0.5
Np = 35
E_minus = 0.1

mesh = GModel()
mesh.load(meshName+".geo")
mesh.adaptMesh([5], [lsNACA], [[E, lcMin, lcMax, Np, E_minus]], 30, False)
mesh.writeMSH(meshName+".msh",2.2,False,False,True,1.0,0,0)

print "**************************************************"
print "     Computing NS "
print "**************************************************"

LS  = functionLevelsetValueNew(lsNACA)
rhoF = functionLevelsetSmoothNew(LS, 1.e3, rho, 1.e-2)
muF = functionLevelsetSmoothNew(LS, 1.e3, mu, 1.e-2)

ns = Incompressible(meshName, 2)
ns.initializeIncomp(initF, rhoF, muF , UGlob)

ns.solution.exportFunctionMsh(rhoF, 'output/rho', 0, 0, '')

INLET  = functionPython(6, InletBC, [ns.XYZ])
ns.strongBoundaryConditionLine('Inlet', INLET)
ns.strongBoundaryConditionLine('Symmetry', ns.VELX)
ns.weakPresBoundaryCondition('Outlet', ns.PZERO)

petscOptions ="-ksp_rtol 1.e-3 -pc_type lu  -pc_factor_levels 0"
#petscOptions ="-ksp_rtol 1.e-4 -pc_type ilu -ksp_monitor -pc_factor_levels 0"

maxIter = 20
ns.pseudoTimeSteppingSolve(timeOrder, nbTimeSteps, dt0, ATol, RTol,Verb, petscOptions)
#ns.steadySolve(maxIter, petscOptions)

ns.solution.exportMshNodeData("output/sol",0,0)


