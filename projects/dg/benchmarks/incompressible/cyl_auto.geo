//Mesh.Algorithm=8;//delquad mesher
//Mesh.RecombineAll=1;//recombine all defined surfaces

Mesh.RemeshAlgorithm=0; //(0) nosplit (1) automatic (2) split metis
Mesh.RemeshParametrization=1; //(0) harmonic (1) conformal 

Mesh.CharacteristicLengthFactor=0.6;

Merge "cylindre.msh";
CreateTopology;

Compound Line (100)= {1,2,3,4};
Compound Line (200)= {6,7,8,9};

Compound Surface(200)= {13,17,21,25};
Compound Surface(201)= {1};
Compound Surface(202)= {26};

//Compound Surface(200)={13};
//Compound Surface(201)={17};
//Compound Surface(202)={21};
//Compound Surface(203)={25};

Surface Loop(7) = {200, 201, 202};   
//Surface Loop(7) = {1, 26, 200};   
//Surface Loop(7) = {26,1,200,201, 202, 203};   
//Surface Loop(7) = {26,1,13,17,21,25};    

Volume(8) = {7}; 

Field[2] = MathEvalAniso;
Field[2].m11 = "1./(0.05)^2";
Field[2].m12 = "0";
Field[2].m13 = "0";
Field[2].m22 = "1/(0.05)^2";
Field[2].m23 = "0";
Field[2].m33 = "1/(0.5)^2";
Background Field = 2;

Physical Surface(1) = {200};
Physical Volume(8)  = {8};


