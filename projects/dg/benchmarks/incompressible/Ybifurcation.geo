lc = 0.1;

R = 0.5; //Radius at the inlet
L1 = 4.0; //Length main branch
L2 = 4.0; //Length side braches
alpha = 20*Pi/180.0;


Point(1) = {R, 0.0, 0.0, lc };
Point(2) = {R, L1 , 0.0, lc };

x3 = R+L2*Cos(0.5*Pi-alpha);
y3 = L1+L2*Sin(0.5*Pi-alpha);

Point(3) = {x3, y3, 0.0,lc};

x4 = x3 - 2*R*Cos(alpha);
y4 = y3 + 2*R*Sin(alpha);

x5 = 0.0;
y5 = L1 + 4*(2*R*Sin(alpha));


Point(4) = {x4,y4, 0.0, lc};
Point(5) = {x5, y5 , 0.0, lc };
Point(6) = {-x4,y4, 0.0, lc};
Point(7) = {-x3, y3, 0.0,lc};
Point(8) = {-R, L1 , 0.0, lc};
Point(9) = {-R, 0.0, 0.0, lc};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,5};
Line(5) = {5,6};
Line(6) = {6,7};
Line(7) = {7,8};
Line(8) = {8,9};
Line(9) = {9,1};

Line Loop (1) = {1,2,3,4,5,6,7,8,9};
Plane Surface(1) = {1};

Physical Line ("Inlet") = {9};
Physical Line ("Outlet") = {3,6};
Physical Line ("Wall") = {1,2,4,5,7,8};
Physical Surface ("Inside") = {1};
