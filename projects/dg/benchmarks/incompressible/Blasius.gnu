reset
set term x11 enhanced font 'Helvetica, 18'
#set term png enhanced 
set output 'Blasius.png'

set key inside bottom right vertical Left
set pointsize 1.5

set xr [0:4]
set yr [0:1.1]
set xlabel '{/Symbol h} [-]'
set ylabel 'U_x / U_0 [m/s]'

set grid
U0=1.0
nu = 0.01
eta(r, s) = s * sqrt(U0 / (2 * nu * r))
plot 'BlasiusF.dat' u ($1):($3) t 'Blasius solution' w l, 'Blasius.dat'  u  (eta(20, $1)):($11/U0) \
                                       t 'x = 20.0cm' w points pt 5,\
                                       'Blasius.dat'  u (eta(10, $1)):($6/U0)\
                                       t 'x = 10.0cm' w points pt 5

set term x11
replot



