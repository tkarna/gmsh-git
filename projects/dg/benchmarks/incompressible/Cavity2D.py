from dgpy import *
from Incompressible import *
from math import *
import sys

#-------------------------------------------------
#-- 2D Lid-driven cavity
#-------------------------------------------------

#-------------------------------------------------
#-- Parameters
#-------------------------------------------------
rho = 1.0
mu = 0.01
UGlob = 1.0
meshName = "Cavity2D"

#-------------------------------------------------
#-- Initial function
#-------------------------------------------------
def initF(FCT, XYZ):
	for i in range(0,FCT.size1()):
		FCT.set(i,0, 1.e-6)
		FCT.set(i,1, 0.0)
		FCT.set(i,2, 0.0)

#-------------------------------------------------
#-- Case specific functions
#-------------------------------------------------
def VelBC(FCT, XYZ ):
	for i in range(0,FCT.size1()):
		x = XYZ.get(i,0)
		impU  = 1.0*(pow(1-pow(x-0.5,18)/0.5,2))
		#FCT.set(i,0, UGlob)
		FCT.set(i,0, impU)
		FCT.set(i,1, 0.0)
		FCT.set(i,2, 0.0)
		FCT.set(i,3, fixed)
		FCT.set(i,4, fixed)
		FCT.set(i,5, free)

#-------------------------------------------------
#-- Initialization
#-------------------------------------------------
ns = Incompressible(meshName, 2)
rhoF = functionConstant(rho)
muF = functionConstant(mu)
ns.initializeIncomp(initF, rhoF, muF , UGlob)

#-------------------------------------------------
#-- Boundary Conditions
#-------------------------------------------------
VEL = functionPython(6, VelBC, [ns.XYZ])
ns.strongBoundaryCondition('Inlet', ns.WALL)
ns.strongBoundaryCondition('WallBottom', ns.WALL)
ns.strongBoundaryCondition('Outlet', ns.WALL)
ns.strongBoundaryCondition('WallTop', VEL)
ns.strongBoundaryConditionPoint('CornerBottomLeft', ns.PZERO)

#-------------------------------------------------
#-- Pseudo TimeStepping
#-------------------------------------------------
nbTimeSteps = 10
dt0 = 1.e-4
ATol = 1.e-6
RTol = 1.e-4
Verb = 2
petscOptions ="-ksp_rtol 1.e-3 -pc_type ilu -pc_factor_levels 0"
ns.pseudoTimeSteppingSolve(timeOrder, nbTimeSteps, dt0, ATol, RTol, Verb, petscOptions)

tol = 1.e-3
epslc1d = 1.e-4
ns.adaptMesh(meshName, "Cavity2DAdapted",  tol, epslc1d )

#-------------------------------------------------
#-- Validation 
#-------------------------------------------------

