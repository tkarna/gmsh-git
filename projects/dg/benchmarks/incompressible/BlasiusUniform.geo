Mesh.CharacteristicLengthExtendFromBoundary=1;
Point(1) = {0,0,0,21};   
Point(2) = {135,0,0,21};
Point(3) = {135,10,0,21}; 
Point(4) = {0,10,0,21};  
Point(5) = {0,100,0,21}; 
Point(6) = {135,100,0,21};  
Point(7) = {-20,0,0,21};  
Point(8) = {-20,10,0,21};  
Point(9) = {-20,100,0,21};  


Line(1) = {7, 8};
//Line(2) = {8, 4};
//Line(3) = {4, 1};
Line(4) = {1, 7};
//Line(5) = {4, 3};
Line(6) = {3, 2};
Line(7) = {2, 1};
//Line(8) = {4, 5};
Line(9) = {5, 6};
Line(10) = {6, 3};
Line(11) = {8, 9};
Line(12) = {9, 5};

Line Loop(13) = {10, 6, 7, 4, 1, 11, 12, 9};
Plane Surface(14) = {13};

Physical Surface("All") = {14};
Physical Line("Wall") = {7};
Physical Line("Inlet") = {1,11,12,9};
Physical Line("Outlet") = {6,10};
Physical Line("Symmetry") = {4};
Physical Point("CornerBottomLeft") = {7};
//Mesh.RecombineAll = 1;
//Mesh.RecombinationAlgorithm = 1;
