/******************************       
Square uniformly meshed       
******************************/       
nb = 16;

lc = 1/nb;     
L = 1; 
l = 0.5;

Point(1) = {0, 0, 0, lc}; 
Point(2) = {L, 0, 0, lc};
Point(3) = {L, L, 0, lc};
Point(4) = {0, L, 0, lc};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};

Line Loop(4) = {1,2,3,4};
Plane Surface(5) = {4};
Transfinite Line {1,3} = nb+1 Using Bump 0.1;
Transfinite Line {2,4} = nb+1 Using Bump 0.1;
Transfinite Surface {5} = {1,2,3,4} Right;
Recombine Surface{5};

Physical Surface("All") = {5};
Physical Line("WallBottom") = {1};
Physical Line("WallTop") = {3};
Physical Line("Inlet") = {4};
Physical Line("Outlet") = {2};





