from dgpy import *
from Incompressible import *
import os
import sys
import time
import math


#-------------------------------------------------
#-- 3D Hagen-Poiseuille Flow
#-------------------------------------------------
# You should obtain the analytical pressure at the inlet 
# (-dp/dx) = vmax*2*rho*nu/h^2
# example: nu=1,   vmax=2, R=0.5, DL=5.0  --> Pin=80
# example: nu=0.1, vmax=2, R=0.5, DL=5.0   --> Pin=8.0

#-------------------------------------------------
#-- Parameters
#-------------------------------------------------
rho = 1.0
mu  = 0.1 
vmax = 2.0
UGlob = 1.0

if (os.path.isfile("PoiseuilleCylinder.msh")) :
  print('---- The mesh file already exists ----');

else :
  cmd = "gmsh -3 PoiseuilleCylinder.geo"
  os.system(cmd)
  print('---- Mesh has been executed ----');
  
meshName = "PoiseuilleCylinder"


#-------------------------------------------------
#-- Initial function
#-------------------------------------------------
def initF(FCT, XYZ):
  for i in range(0,FCT.size1()):
    y = XYZ.get(i,1)
    FCT.set(i,0, 0.0) #u
    FCT.set(i,1, 0.0) #v
    FCT.set(i,2, UGlob) #w
    FCT.set(i,3, 0.0) #p

#-------------------------------------------------
#-- Case specific functions
#-------------------------------------------------
def VelBC(FCT, XYZ):
  for i in range(0,FCT.size1()):
    x = XYZ.get(i,0)    
    y = XYZ.get(i,1)    
    FCT.set(i,0, 0.0) #u
    FCT.set(i,1, 0.0) #v
    #FCT.set(i,2, 1.0) #w
    FCT.set(i,2, vmax*(1-(x*x+y*y)/(0.5*0.5)))		
    FCT.set(i,3, 0.0) #p
    FCT.set(i,4, fixed)
    FCT.set(i,5, fixed)
    FCT.set(i,6, fixed)
    FCT.set(i,7, free)

#-------------------------------------------------
#-- Initialization
#-------------------------------------------------

ns = Incompressible(meshName, 3)
rhoF = functionConstant(rho)
muF = functionConstant(mu)
ns.initializeIncomp(initF, rhoF, muF , UGlob)

print 'MAX Reynolds = ', (rho*UGlob*1.0/mu) 

#-------------------------------------------------
#-- Boundary Conditions
#-------------------------------------------------
VEL = functionPython(8, VelBC, [ns.XYZ])
ns.strongBoundaryConditionSurface('Inlet', VEL)
#ns.strongBoundaryConditionSurface('Symmetry', ns.WALL)
ns.strongBoundaryConditionSurface('Wall', ns.WALL)
ns.weakPresBoundaryCondition('Outlet', ns.PZERO)

#-------------------------------------------------
#-- Steady solve
#-------------------------------------------------
#petscOptions ="-ksp_rtol 1.e-3 -pc_type lu -ksp_monitor -pc_factor_levels 2"
petscOptions ="-ksp_rtol 1.e-3 -pc_type ilu -pc_factor_levels 1"
#ns.steadySolve(petscOptions)

#-------------------------------------------------
#-- Pseudo TimeStepping
#-------------------------------------------------
timeOrder = 2
nbTimeSteps = 20
dt0 = 10
ATol = 1.e-5
RTol = 1.e-8
Verb = 2
ns.pseudoTimeSteppingSolve(timeOrder, nbTimeSteps, dt0, ATol, RTol, Verb, petscOptions)

#-------------------------------------------------
#-- Validation 
#-------------------------------------------------

#Analytical:
L = 5 #from the .geo file
R = 0.5 #from the .geo file
pANA = 4*mu*L*vmax/R**2 #pANA is the difference of pressure between the inlet and outlet
print 'Analytical value for pressure difference: ',  pANA

#Numerical:
eval = dgFunctionEvaluator(ns.groups, ns.solution.getFunction())
result = fullMatrixDouble(4,1);
eval.compute(0,0,0,result)
pNUM = result.get(3,0)
print 'Numerical value for pressure difference: ' , pNUM

#Comparison:
diff = abs((pNUM-pANA)/pANA)

print '|error DP| = ', diff
if (diff < 1.e-1): 
	print "Exit with success :-)"
	sys.exit(1)
else:
	print "Exit with failure :-("
	sys.exit(0)
