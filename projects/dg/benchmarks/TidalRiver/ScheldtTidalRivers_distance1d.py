from dgpy import *
import math 
downstreamLengthListElemGroup=[]
################################################
def buildDownstreamLengthListElemGroup(groups):
#  #compute the downstream length of each element 
#  #for each element, we compute the sum of the lengths of the downstream element
#  #assume that one physical tag is defined by river branch and that groups has been splitted by Physical Tag
  global downstreamLengthListElemGroup
  nM=dgNeighbourMap(groups)
  nbElementGroups=groups.getNbElementGroups()
  downstreamLengthListElemGroup=[]
  for iEG in range(0,nbElementGroups):
    EG=groups.getElementGroup(iEG)
    for iE in range(0,EG.getNbElements()):
       neighbours=nM.getNeighbours(iEG,iE,0)
       if(len(neighbours)==0 or neighbours[0][0]!=iEG ):
        firstElemGroupId=iE
        break
  
    iElem=firstElemGroupId
    elem=EG.getElement(iElem)
    downstreamLengthListElemGroup.append(iEG)
    downstreamLengthListElemGroup[iEG]=[]
    distance=0
    while 1 :
      downstreamLengthListElemGroup[iEG].append(distance)
      distance+=elem.getEdge(0).length()
      neighbours=nM.getNeighbours(iEG,iElem,1)

      if len(neighbours)==0:
        downstreamLengthListElemGroup[iEG].append(distance)
        break
      if len(neighbours)==2:
        if neighbours[0][0] !=iEG and  neighbours[1][0] !=iEG:
          downstreamLengthListElemGroup[iEG].append(distance)
          break
      elif neighbours[0][0] != iEG:
          downstreamLengthListElemGroup[iEG].append(distance)
          break

      iElem=neighbours[0][1]
      if(len(neighbours)>1 and neighbours[1][0] ==iEG):
        iElem=neighbours[1][1]
      elem=EG.getElement(iElem)
  
  return downstreamLengthListElemGroup

################################################
def downstreamLength(FCT,XYZ):
  m=FCT.getDataCacheMap()
  ge=m.getGroupOfElements()
  iEG=ge.getGroupCollection().getElementGroupId(ge) 
  iE=m.getElementId()
  downstreamLength=downstreamLengthListElemGroup[iEG][iE]
  v0=ge.getElement(iE).getVertex(0)
  cv0=[v0.x(),v0.y(),v0.z()]
  for i in range(0,FCT.size1()):
    xyz=[XYZ(i,0),XYZ(i,1),XYZ(i,2)]
    dlv=[xyz[0]-cv0[0],xyz[1]-cv0[1],xyz[2]-cv0[2]]
    dl=math.sqrt(dlv[0]*dlv[0]+dlv[1]*dlv[1]+dlv[2]*dlv[2])
    FCT.set(i,0,downstreamLength+dl)

################################################
def get(groups):
  XYZ=groups.getFunctionCoordinates()
  x1d = dgDofContainer(groups, 1)
  downstreamLengthListElemGroup=buildDownstreamLengthListElemGroup(groups)
  x1dF = functionPython(1,downstreamLength,[XYZ])
  x1d.interpolate(x1dF)
  return x1d
