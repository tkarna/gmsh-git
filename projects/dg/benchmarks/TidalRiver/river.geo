//river.geo
L=1e5;
Point(1) = {0, 0, 0};
Point(2) = {L, 0, 0};
Line(3) = {1, 2};

Field[1] = MathEval;
Field[1].F = "0.2e4";
Background Field = 1;

Physical Point("Downstream") = {1};
Physical Point("Upstream") = {2};
Physical Line("Line") = {3};
