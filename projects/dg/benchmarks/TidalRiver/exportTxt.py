from dgpy import *
from gmshpy import *
import matplotlib.pyplot as matplot
import matplotlib.animation as animation
from matplotlib.lines import Line2D
import os.path
import os
from numpy import *

def exportFunctionTxt_evaluator(model, groups, solution, evaluator, distance, name, index):
  nEdges=model.getNumMeshElements()
  f=open("%s_%05d.txt" % (name,index), "w");
  evalDist = dgFunctionEvaluator(groups, distance, solution)
  dist=fullMatrixDouble()
  evalFun = dgFunctionEvaluator(groups, evaluator, solution)
  val=fullMatrixDouble()
  for i in range(1,nEdges+1):
    e=model.getMeshElementByTag(i)
    if(e.getDim()==1):
      v=e.getVertex(0)
      evalDist.compute(v.x(),v.y(), v.z(), dist)
      evalFun.compute(v.x(),v.y(), v.z(), val)
      f.write("%16f %16f\n" % (dist(0,0), val(0,0)) );
      v=e.getVertex(1)
      evalDist.compute(v.x(),v.y(), v.z(), dist)
      evalFun.compute(v.x(),v.y(), v.z(), val)
      f.write("%16f %16f\n" % (dist(0,0), val(0,0)) );
  f.close()

def exportFunctionTxt(model, groups, solution, function, distance, name, index):
  f=open("%s_%05d.txt" % (name,index), "w");
  nGroups=groups.getNbElementGroups()
  for iG in range(0,nGroups):
    cacheMap=dataCacheMap(dataCacheMap.NODE_MODE,groups) 
    cacheMap.setSolutionFunction(solution.getFunction(),solution.getFunctionGradient())
    value = cacheMap.get(function)
    distance = cacheMap.get(distance)
    group=groups.getElementGroup(iG)
    cacheMap.setGroup(group)
    nElem=group.getNbElements()
    for iE in range(0,nElem):
      cacheMap.setElement(iE)
      val=value.get()
      dist=distance.get()
      for iNode in range(0,val.size1()):
        for iComp in range(0,val.size2()):
          f.write("%16f %16f\n" % (dist(iNode,0), val(iNode,iComp)))
  f.close()          



def getDG_XF(filename):
  sol=open(filename,"r").read()
  sol=sol.split()
  x=[]
  f=[]
  for i in range(0,len(sol),4):
    x.append(float(sol[i]))
    x.append(float(sol[i+2]))
    x.append(nan)
    f.append(float(sol[i+1]))
    f.append(float(sol[i+3]))
    f.append(nan)
  x=array(x)
  f=array(f)
  return x,f

def getSLIM_XF(filename):
  sol=open(filename,"r").read()
  sol=sol.split()
  x=[]
  f=[]
  for i in range(0,len(sol),4):
    x.append(float(sol[i+2]))
    x.append(float(sol[i]))
    x.append(nan)
    f.append(float(sol[i+3]))
    f.append(float(sol[i+1]))
    f.append(nan)
  x=array(x)
  f=array(f)
  return x,f
  
def openDGWithPyPlot(filename1):
  x1,f1=getDG_XF(filename1)
  print len(x1)
  for i in range(0,len(x1),2):
    xx1=[x1[i],x1[i+1]];
    ff1=[f1[i],f1[i+1]];
    matplot.plot(xx1,ff1,'-xb')

def openSLIMWithPyPlot(filename2):
  x2,f2=getSLIM_XF(filename2)
  print len(x2)
  for i in range(0,len(x2),2):
    xx2=[x2[i],x2[i+1]];
    ff2=[f2[i],f2[i+1]];
    matplot.plot(xx2,ff2,'-or')

def computeDiff(f1,f2):
  f1=array(f1)
  f2=array(f2)
  normF=linalg.norm(f2-f1)
  maxDiffF=max(f2-f1)
  return normF, maxDiffF

class SubplotAnimationDG_SLIM(animation.TimedAnimation):
    def __init__(self,name1,name2,i0,i1,fmin=-1,fmax=-1):
        self.name1=name1
        self.name2=name2
        self.i0=i0
        self.i1=i1
        fig = matplot.figure()
        ax1 = fig.add_subplot(1, 1, 1)
        [x1,f1]=getDG_XF("%s_%05d.txt"%(self.name1,self.i0))
        [x2,f2]=getSLIM_XF("%s%05d.txt"%(self.name2,self.i0))

        ax1.set_xlabel('x')
        ax1.set_ylabel('y')
        self.line1 = Line2D([], [], color='blue')
        self.line1a = Line2D([], [], color='red')
        ax1.add_line(self.line1)
        ax1.add_line(self.line1a)
        ax1.set_xlim(x1[0], x1[-2])
        #ax1.set_ylim(min(f1)*0.9, max(f1)*1.1)
        ax1.set_ylim(fmin, fmax)

        animation.TimedAnimation.__init__(self, fig, interval=200, blit=True)
        matplot.show()

    def _draw_frame(self, framedata):
        i = framedata
        [x1,f1]=getDG_XF("%s_%05d.txt"%(self.name1,i))
        [x2,f2]=getSLIM_XF("%s%05d.txt"%(self.name2,i))

        self.line1.set_data(x1, f1)
        self.line1a.set_data(x2, f2)

        self._drawn_artists = [self.line1, self.line1a]

    def new_frame_seq(self):
        return iter(range(self.i0,self.i1))

    def _init_draw(self):
        lines =  [self.line1, self.line1a]
        for l in lines:
            l.set_data([], [])

class SubplotAnimation1(animation.TimedAnimation):
    def __init__(self,name1,i0,i1,fmin=-1,fmax=-1):
        self.i0=i0
        self.i1=i1
        self.name1=name1
        fig = matplot.figure()
        ax1 = fig.add_subplot(1, 1, 1)
        [x1,f1]=getDG_XF("%s_%05d.txt"%(self.name1,self.i0))
        self.line1 = Line2D([], [], color='blue')
        ax1.add_line(self.line1)
        ax1.set_xlim(x1[0], x1[-2])
        ax1.set_ylim(fmin, fmax)
        animation.TimedAnimation.__init__(self, fig, interval=200, blit=True)
        matplot.show()

    def _draw_frame(self, framedata):
        i = framedata
        [x1,f1]=getDG_XF("%s_%05d.txt"%(self.name1,i))

        self.line1.set_data(x1, f1)

        self._drawn_artists = [self.line1]

    def new_frame_seq(self):
        return iter(range(self.i0,self.i1))

    def _init_draw(self):
        lines =  [self.line1]
        for l in lines:
            l.set_data([], [])


def mp4Animation(name,i0,i1,fmin=-1,fmax=1):
  ani = SubplotAnimation1(name,i0,i1,fmin,fmax)
  #ani.save("anim.mp4")
  #matplot.show()
  #os.system("mplayer anim.mp4")

def mp4AnimationDG_SLIM(name0,name1,i0,i1,fmin=-1,fmax=1):
  print fmin,fmax, "------------------------------------------_>"
  ani = SubplotAnimationDG_SLIM(name0,name1,i0,i1,fmin,fmax)
  #ani.save("anim.mp4")
  #matplot.show()
  #os.system("mplayer anim.mp4")
