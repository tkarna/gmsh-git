from dgpy import *
from gmshpy import *
import time, math, os
import os.path
import os
import sys
from river1d_downstreamLength import *
from river1d_bathymetry import *

geo=".geo"
msh=".msh"
order=1
dimension=1
outputD="scheldtTidalRiversPrepro/"
os.system("mkdir -p "+outputD)
os.system("rm -f "+ outputD + "*")

if (Msg.GetCommRank()==0 ):
  functionC.buildLibraryFromFile ("ScheldtTidalRivers.cc", "ScheldtTidalRivers.so");
Msg.Barrier()
g = 9.81;
t = 0;
TIME=function.getTime()

def printTime (t) :
  return "%dh%02d'%02d\"" % (t/3600, (t%3600)/60, t%60)

def merge(FCT,F1,F2):
 for i in range(0,FCT.size1()):
    FCT.set(i,0,F1(i,0));
    FCT.set(i,1,F2(i,0));

################################################
model = GModel()
meshName="ScheldtTidalRivers"
#meshName="BifurcationRiver"
#p = os.system("gmsh %s.geo -%d -order %d"%(meshName, dimension, order))
model.load (meshName+geo)
model.load (meshName+msh)

curv1D = dgSpaceTransform1DCurvilinear()
groups = dgGroupCollection(model, dimension, order, curv1D)
groups.splitGroupsByPhysicalTag()
groups.buildGroupsOfInterfaces()

x1d=getDownstreamLength(groups)
x1d.exportMsh(outputD+"x1d",0,0)

