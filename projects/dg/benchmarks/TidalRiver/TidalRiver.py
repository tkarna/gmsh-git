from dgpy import *
from gmshpy import *
#from exportRiver1d import *
import time, math, os
import ScheldtTidalRivers_distance1d as distance1d
import os.path
import os
import sys
import matplotlib.pyplot as matplot
implicitTs=True
geo=".geo"
msh=".msh"
order=1
dimension=1
clscale=1
os.system("mkdir -p tidalRiver")
os.system("rm tidalRiver/*")

if (Msg.GetCommRank()==0 ):
  functionC.buildLibraryFromFile("TidalRiver.cc", "TidalRiver.so");
Msg.Barrier()
g = 9.81;
t = 0;
TIME=function.getTime()

model = GModel()
meshName="river"
p = os.system("gmsh %s.geo -%d -order %d -clscale %f"%(meshName, dimension, order,clscale))
#model.load (meshName+geo)
model.load (meshName+msh)

curv1D = dgSpaceTransform1DCurvilinear()
groups = dgGroupCollection(model, dimension, order, curv1D)
groups.splitGroupsByPhysicalTag()

hydroLaw = dgConservationLawShallowWater1d()
hydroSol = dgDofContainer(groups, hydroLaw.getNbFields())
hydroSol.exportGroupIdMsh()
hydroLaw.setHydroSolution(hydroSol.getFunction())
hydroLaw.setElevationInterp()
XYZ=groups.getFunctionCoordinates()
etaInterp=hydroLaw.getElevationInterp()
x1d = distance1d.get(groups)
x1d.exportMsh("x1d",0,0)


#-------------------------
# Geometry
#-------------------------
bath=functionConstant([10.])
widthData = functionC("TidalRiver.so","widthData",1,[bath,etaInterp])
sectionData = functionC("TidalRiver.so","sectionData",1,[bath,etaInterp])
hydroLaw.setWidthData(widthData)
hydroLaw.setSectionData(sectionData)
hydroLaw.setElevation()
hydroLaw.setWidth()

#-------------------------
# Initial conditions
#-------------------------
etaGaussian = functionC("TidalRiver.so","gaussian",1,[XYZ])
zeroF=functionConstant([0.0])
#initialSection = functionC("TidalRiver.so","sectionData",1,[bath,etaGaussian])
initialSection = functionC("TidalRiver.so","sectionData",1,[bath,zeroF])
initialSol = functionC("TidalRiver.so","merge2scalars",2,[initialSection,zeroF])
hydroSol.interpolate(initialSol)

#-------------------------
# get Fields
#-------------------------
etaF = hydroLaw.getElevation()
width= hydroLaw.getWidth()
section = hydroLaw.getSection()
velocity = hydroLaw.getVelocity()

#-------------------------
# Supplementary terms1

#-------------------------

quadDiss=functionC("TidalRiver.so","chezyManningDissipation",1,[section,width])
hydroLaw.setQuadraticDissipation(quadDiss)
diffusivity=functionConstant(0)
hydroLaw.setDiffusivity(diffusivity)
hydroLaw.setSetup()

#-------------------------
# Boundary conditions
#-------------------------

#DOWNSTREAM
fzero=functionConstant(0.)
etaSinus=functionC("TidalRiver.so","etaSinus",1,[TIME])
sectionSinus=functionC("TidalRiver.so","sectionData",1,[bath,etaSinus])
hydroLaw.addBoundaryCondition("Downstream",hydroLaw.newForceSolution(sectionSinus,velocity))

#UPSTREAM
hydroLaw.addBoundaryCondition("Upstream",hydroLaw.newForceSolution(section,fzero))


#-------------------------
print "Initial Exports"
#-------------------------
hydroSol.exportMsh("tidalRiver/hydroSol",0,0)
hydroSol.exportFunctionMsh(initialSol,"tidalRiver/initialSol",0,0)
hydroSol.exportFunctionMsh(etaGaussian,"tidalRiver/etaGaussian",0,0)
hydroSol.exportFunctionMsh(width,"tidalRiver/width", 0, 0)  
hydroSol.exportFunctionMsh(etaF,"tidalRiver/eta", 0, 0)  
hydroSol.exportFunctionMsh(section,"tidalRiver/section", 0, 0)  

distance=functionExtractCompNew(XYZ,0)

integratorS=dgFunctionIntegrator(groups,section)
ISv = fullMatrixDouble(1,1)
Sv=[]
usquare = functionC("TidalRiver.so","square",1,[velocity])
integratoruu=dgFunctionIntegrator(groups,usquare)
Iuuv = fullMatrixDouble(1,1)
uuv=[]

etaField=dgDofContainer(groups,1)
etaField.exportGroupIdMsh()

#-------------------------
# Time stepping
#-------------------------
if implicitTs:
  dt = 10
  petsc = linearSystemPETScBlockDouble()
  petsc.setParameter("petscOptions",  "-pc_type bjacobi -sub_pc_type ilu")
  dof = dgDofManager.newDGBlock(groups, hydroLaw.getNbFields(), petsc)
  hydroSolver = dgDIRK(hydroLaw, dof, 2)     #timeorder
  hydroSolver.getNewton().setAtol(1.e-5)#ATol
  hydroSolver.getNewton().setRtol(1.e-8)#Rtol
  hydroSolver.getNewton().setVerb(1)    #Verbosity
else:
  dt = 0.1
  hydroSolver=dgERK(hydroLaw,None,DG_ERK_22)

#-------------------------
print "Solve"
#-------------------------
t=0.
nbExport = 0
for i in range (0,31000) :
  t = t+dt
  if implicitTs:
    hydroSolver.iterate(hydroSol, dt , t)
    normHydro = hydroSolver.getNewton().getFirstResidual()
  else:
    normHydro = hydroSolver.iterate(hydroSol,dt,t)
  if (i % 300 == 0) :
    integratorS.compute(ISv)
    integratoruu.compute(Iuuv)
    Msg.Info("%d %03d int(S)dx=%.4e int(u*u)dx=%.4e" % (nbExport,i,ISv(0,0),Iuuv(0,0)))
    Sv.append(ISv(0,0))
    uuv.append(Iuuv(0,0))
#    exportFunctionTxt(groups,hydroSol,distance,etaF,"tidalRiver/eta",nbExport)
#    exportFunctionTxt(groups,hydroSol,distance,section,"tidalRiver/section",nbExport)
#    exportFunctionTxt(groups,hydroSol,distance,velocity,"tidalRiver/velocity",nbExport)
    nbExport  = nbExport  + 1
    if (nbExport == 101):
      break


#mp4Animation("tidalRiver/eta",0,100,-1.2,1.2)    

