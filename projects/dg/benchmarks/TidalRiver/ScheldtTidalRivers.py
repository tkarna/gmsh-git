from dgpy import *
from gmshpy import *
import ScheldtTidalRivers_distance1d as distance1d
import ScheldtTidalRivers_shape1d as shape1d
import ScheldtTidalRivers_export1d as export1d
import time, math, os, os.path, sys, datetime, calendar
from scipy.weave import blitz
import exportRiver1d as export1d
implicitTs=False
geo=".geo"
msh=".msh"
order=1
dimension=1
clscale=2
os.system("mkdir -p scheldtTidalRivers")
os.system("rm scheldtTidalRivers/*")

if (Msg.GetCommRank()==0 ):
  functionC.buildLibraryFromFile ("TidalRiver.cc", "TidalRiver.so");
Msg.Barrier()
g = 9.81;
t = 0;

def printTime (t) :
  return "%dh%02d'%02d\"" % (t/3600,  (t%3600)/60,  t%60)
def printDate (t):
  date=time.gmtime(t)
  return "%02d/%02d/%04d %02dh%02d'%02d\""%(date[2], date[1], date[0], date[3], date[4], date[5])

elapsedTime=0
initialDate=datetime.datetime(1990,1,1,12,0,0)
initialTime=calendar.timegm(initialDate.timetuple())
timeFunction = functionConstant(initialTime)

model = GModel()
meshName="ScheldtTidalRivers"
p = os.system("gmsh %s.geo -%d -order %d -clscale %f"%(meshName, dimension, order,clscale))
model.load (meshName+geo)
model.load (meshName+msh)

curv1D = dgSpaceTransform1DCurvilinear()
groups = dgGroupCollection(model, dimension, order, curv1D)
groups.splitGroupsByPhysicalTag()
x1d=distance1d.get(groups)

riverNames=["OUDESCHELDE","DURME","ZEESCHELDE","RINGVAART","RUPEL","KLEINENETE","NETEAFLEIDING","BENEDENNETE","GROTENETE","BENEDENDIJLE","BOVENDIJLE","DIJLEAFLEIDING","ZENNEAFL","ZENNE","DENDER"]
#---------------------------
# Equations
#---------------------------
hydroLaw = dgConservationLawShallowWater1d()
hydroLaw.setElevationInterp()

#-------------------------
# Geometry
#-------------------------
etaInterp=hydroLaw.getElevationInterp()
smoothRiverData=30
shapeData=shape.riverShape("ScheldtData/shapes",riverNames,smoothRiverData)
shapeData.setFields(groups,x1d)
altitudeData,widthData,sectionData=shapeData.get(groups,etaInterp)
hydroLaw.setWidthData(widthData)
hydroLaw.setSectionData(sectionData)
hydroLaw.setRiverDistance(x1d.getFunction())

#-------------------------
# Initial conditions
#-------------------------
zeroF=functionConstant([0.])
etaInitial = functionConstant([5.])
altitudeInitial,widthInitial,sectionInitial = shapeData.get(groups,etaInitial)
initialSol = functionC("TidalRiver.so","merge2scalars",2,[sectionInitial,zeroF])

#-------------------------
# Hydro solution
#-------------------------
hydroSol = dgDofContainer(groups, hydroLaw.getNbFields())
hydroSol.interpolate(initialSol)
hydroLaw.setHydroSolution(hydroSol.getFunction())
hydroLaw.setElevation()
hydroLaw.setWidth()

#-------------------------
# get Fields
#-------------------------
etaF = hydroLaw.getElevation()
width= hydroLaw.getWidth()
section = hydroLaw.getSection()
velocity = hydroLaw.getVelocity()

#-------------------------
# Time series for upstream discharges
#-------------------------
Q_Appels = slimFunctionTemporalSerie("ScheldtData/discharges/Appels_discharge_1990_2008",timeFunction)
Q_Eppegem = slimFunctionTemporalSerie("ScheldtData/discharges/Eppegem_discharge_1990_2008",timeFunction)
Q_Dendermonde = slimFunctionTemporalSerie("ScheldtData/discharges/Dendermonde_discharge_1990_2008",timeFunction)
Q_Grobbendonk = slimFunctionTemporalSerie("ScheldtData/discharges/Grobbendonk_discharge_1990_2008",timeFunction)
Q_Haacht = slimFunctionTemporalSerie("ScheldtData/discharges/Haacht_discharge_1990_2008",timeFunction)
Q_Itegem = slimFunctionTemporalSerie("ScheldtData/discharges/Itegem_discharge_1990_2008",timeFunction)
Q_Melle = slimFunctionTemporalSerie("ScheldtData/discharges/Melle_discharge_1990_2008",timeFunction)
def Q_OudeScheldeF(FCT,Q):
  blitz("FCT[:,0] = Q[:,0] * 1./10", check_size = 0)
Q_OudeSchelde = functionNumpy(1, Q_OudeScheldeF, [Q_Melle])
def Q_RingvaartF(FCT,Q):
  blitz("FCT[:,0] = Q[:,0] * 9./10", check_size = 0)
Q_Ringvaart = functionNumpy(1, Q_RingvaartF, [Q_Melle])
Q_Durme = functionConstant(1.)
def Q_StarF(FCT,Q_Ep,Q_Ha,Q_Gr,Q_It):
  blitz("FCT[:,0] = Q_Ep[:,0]*1.08 + Q_Ha[:,0]*1.08 + Q_Gr[:,0]*1.46 + Q_It[:,0]*1.35", check_size = 0)
Q_Star = functionNumpy(1, Q_StarF,[Q_Eppegem,Q_Haacht,Q_Grobbendonk,Q_Itegem])
def runoff_ZeescheldeF(FCT,Q):
  blitz("FCT[:,0] = Q[:,0] / 12.8 / 70e3", check_size = 0)
runoff_Zeeschelde = functionNumpy(1, runoff_ZeescheldeF, [Q_Star])
runoff_Oudeschelde = runoff_Zeeschelde
runoff_Ringvaart = runoff_Zeeschelde
def runoff_RupelF(FCT,Q):
  blitz("FCT[:,0] = Q[:,0] / 22.5 / 12e3", check_size = 0)
runoff_Rupel = functionNumpy(1, runoff_RupelF, [Q_Star])
def runoff_GroteNeteF(FCT,Q):
  blitz("FCT[:,0] = Q[:,0] / 18e3 * (1.35 - 1)", check_size = 0)
runoff_GroteNete = functionNumpy(1, runoff_GroteNeteF, [Q_Itegem])
def runoff_BenedenNeteF(FCT,Q):
  blitz("FCT[:,0] = max(1,Q[:,0] / 51) / 15e3", check_size = 0)
runoff_BenedenNete = functionNumpy(1, runoff_BenedenNeteF, [Q_Star])
runoff_Dender=functionConstant(0.)
def runoff_DijleF(FCT,Q):
  blitz("FCT[:,0] = Q[:,0] / 22e3 * (1.08 -1)", check_size = 0)
runoff_Dijle = functionNumpy(1, runoff_DijleF, [Q_Haacht])
def runoff_KleineNeteF(FCT,Q):
  blitz("FCT[:,0] = Q[:,0] / 16e3 * (1.46 -1)", check_size = 0)
runoff_KleineNete = functionNumpy(1, runoff_KleineNeteF, [Q_Grobbendonk])
def runoff_ZenneF(FCT,Q):
  blitz("FCT[:,0] = Q[:,0] / 13e3 * (1.08 -1)", check_size = 0)
runoff_Zenne = functionNumpy(1, runoff_ZenneF, [Q_Eppegem])

#-------------------------
# Supplementary terms1
#-------------------------

quadDiss=functionC("TidalRiver.so","chezyManningDissipation",1,[section,width])
hydroLaw.setQuadraticDissipation(quadDiss)
okuboFactor=functionConstant(10.)
diffusivity=functionC("TidalRiver.so","okubo",1,[okuboFactor]);
hydroLaw.setDiffusivity(diffusivity)
hydroLaw.setSetup()
if implicitTs==False:
  hydroLaw.clipSection(0.1)

#-------------------------
# Boundary conditions
#-------------------------

#DOWNSTREAM
fzero = functionConstant(0.)
elapsedTimeFunction=functionConstant([elapsedTime])
etaSinus=functionC("TidalRiver.so","etaSinus",1,[elapsedTimeFunction])
altDown,widthDown,sectionDown=shapeData.get(groups,fzero)
bndDown = hydroLaw.newForceSolution(sectionDown,velocity)
hydroLaw.addBoundaryCondition("downstreamZEESCHELDE", bndDown)

#ZEESCHELDE
bndUp_OudeSchelde=hydroLaw.newForceUpstreamDischarge(Q_OudeSchelde)
hydroLaw.addBoundaryCondition("Ghent-upstreamOUDESCHELDE",bndUp_OudeSchelde)

#RINGVAART
bndUp_Ringvaart=hydroLaw.newForceUpstreamDischarge(Q_Ringvaart)
hydroLaw.addBoundaryCondition("Ghent-upstreamRINGVAART", bndUp_Ringvaart)

#DURME
bndUp_Durme = hydroLaw.newForceUpstreamDischarge(Q_Durme)
hydroLaw.addBoundaryCondition("upstreamDURME", bndUp_Durme)

#DENDER
bndUp_Dender = hydroLaw.newForceUpstreamDischarge(Q_Appels)
hydroLaw.addBoundaryCondition("upstreamDENDER", bndUp_Dender)

#GROTENETE
bndUp_GroteNete = hydroLaw.newForceUpstreamDischarge(Q_Itegem)
hydroLaw.addBoundaryCondition("Itegem-upstreamGROTENETE", bndUp_GroteNete)

#KLEINENETE
bndUp_KleineNete = hydroLaw.newForceUpstreamDischarge(Q_Grobbendonk)
hydroLaw.addBoundaryCondition("Grobbendonk-upstreamKLEINENETE", bndUp_KleineNete)

#DIJLE
bndUp_Dijle = hydroLaw.newForceUpstreamDischarge(Q_Haacht)
hydroLaw.addBoundaryCondition("Haacht-upstreamBOVENDIJLE", bndUp_Dijle)

#ZENNE
bndUp_Zenne = hydroLaw.newForceUpstreamDischarge(Q_Eppegem)
hydroLaw.addBoundaryCondition("Eppegem-upstreamZENNE", bndUp_Zenne)

#-------------------------
# Initial exports
#-------------------------

hydroSol.exportFunctionMsh(section,"scheldtTidalRivers/section",0,0)
hydroSol.exportFunctionMsh(etaSinus,"scheldtTidalRivers/etaSinus",0,0)
hydroSol.exportMsh("scheldtTidalRivers/hydroSol",0,0)
hydroSol.exportFunctionMsh(etaF,"scheldtTidalRivers/etaF",0,0)
export1d.exportFunctionTxt(groups,hydroSol,x1d.getFunction(),section,"scheldtTidalRivers/section",0)
export1d.exportFunctionTxt(groups,hydroSol,x1d.getFunction(),width,"scheldtTidalRivers/width",0)
export1d.exportFunctionPosQuad(groups,hydroSol,width,section,"scheldtTidalRivers/section",0)
export1d.exportFunctionPosQuad(groups,hydroSol,width,etaSinus,"scheldtTidalRivers/etaSinus",0)

#-------------------------
# Iterate
#-------------------------
integratorS=dgFunctionIntegrator(groups,section)
ISv = fullMatrixDouble(1,1)
Sv=[]
usquare = functionC("TidalRiver.so","square",1,[velocity])
integratoruu=dgFunctionIntegrator(groups,usquare)
Iuuv = fullMatrixDouble(1,1)
uuv=[]
integratorS.compute(ISv)
integratoruu.compute(Iuuv)
Msg.Info("%d int(S)dx=%.4e int(u*u)dx=%.4e" % (0,ISv(0,0),Iuuv(0,0)))
nbExport = 0

if implicitTs:
  dt = 1
  petsc = linearSystemPETScBlockDouble()
  petsc.setParameter("petscOptions",  "-pc_type bjacobi -sub_pc_type ilu")
  dof = dgDofManager.newDGBlock(groups, hydroLaw.getNbFields(), petsc)
  implicitTs = dgDIRK(hydroLaw, dof, 2)
  implicitTs.getNewton().setAtol(1e-7)
  implicitTs.getNewton().setRtol(1e-7)
  implicitTs.getNewton().setVerb(1)
  hydroSolver = dgDIRK(hydroLaw, dof, 2)     #timeorder
  hydroSolver.getNewton().setAtol(1.e-5)#ATol
  hydroSolver.getNewton().setRtol(1.e-8)#Rtol
  hydroSolver.getNewton().setVerb(1)    #Verbosity
else:
  dt = 0.01
  hydroSolver=dgERK(hydroLaw,None,DG_ERK_22)

t = initialTime
elapsedTime = 0
for i in range (0,1000) :
  t = t + dt
  timeFunction.set(t)
  elapsedTime = initialTime + t
  elapsedTimeFunction.set(elapsedTime)
  if implicitTs:
    hydroSolver.iterate(hydroSol, dt , t)
    normHydro = hydroSolver.getNewton().getFirstResidual()
  else:
    normHydro = hydroSolver.iterate(hydroSol,dt,t)

  if (elapsedTime % 0.01 == 0) :
    integratorS.compute(ISv)
    integratoruu.compute(Iuuv)
    Msg.Info("%d %03d int(S)dx=%.4e int(u*u)dx=%.4e" % (nbExport,i,ISv(0,0),Iuuv(0,0)))
    export1d.exportFunctionPosQuad(groups,hydroSol,width,etaF,"scheldtTidalRivers/eta",nbExport)
    export1d.exportFunctionTxt(groups,hydroSol,x1d.getFunction(),etaF,"scheldtTidalRivers/eta",nbExport)
    hydroSol.exportMsh("scheldtTidalRivers/hydroSol-%06d" % i, elapsedTime, nbExport)
    Sv.append(ISv(0,0))
    uuv.append(Iuuv(0,0))
    nbExport  = nbExport  + 1
