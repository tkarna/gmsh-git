h=1;
l=1;
Point(1) = {0, 0, 0, .05};
Point(2) = {l, 0, 0, .05};
Point(3) = {l, h, 0, .05};
Point(4) = {0, h, 0, .05};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

Line Loop(8) = {1, 2, 3, 4};
Plane Surface(9) = {8};

Physical Surface("sprut") = {9};
Physical Line("Walls") = {1, 2, 3, 4};


