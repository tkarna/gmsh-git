--[[ 
     Function for initial conditions
--]]

g = 9.81;

function initial_condition( xyz , f )
  for i=0,xyz:size1()-1 do
    x = xyz:get(i,0)
    if (x<0.0) then
     f:set (i, 0, 40)
     f:set (i, 1, 0)
    else
     f:set (i, 0, 20)
     f:set (i, 1, 0)
    end	
  end
end

--[[ 
     Example of a ShallowWater 1d code
--]]
order=1
dimension=1

model = GModel()
cmd = string.format("gmsh edge.geo -%d -order %d", dimension, order)
os.execute(cmd)
model:load ('edge.msh')

--model:load ('edge.geo')
--options = gmshOptions()
--options:numberSet('Mesh',-1,'ElementOrder',order)
--model:mesh(dimension)
--model:save('edge.msh')

groups = dgGroupCollection(model, dimension, order)
groups:buildGroupsOfInterfaces()

law = dgConservationLawNavierStokes1d()
law:setLinearDissipation(functionConstant({0.}))
law:setBathymetry(functionConstant({0.}))

law:setShallowWaterFormulation()

law:addBoundaryCondition('Left',law:newBoundaryWall())
law:addBoundaryCondition('Right',law:newBoundaryWall())

rk=dgRungeKutta()
limiter = dgSlopeLimiter(law)
rk:setLimiter(limiter) 

FS = functionLua(2, 'initial_condition', {functionCoordinates.get()})
solution = dgDofContainer(groups, law:getNbFields())
solution:L2Projection(FS)

print'*** print initial sol ***'
--solution:exportMsh('output/init', 0, 0)
limiter:apply(solution)
solution:exportMsh('output/init_limit', 0, 0)

print'*** solve ***'

t=0
CFL = 0.2
nbExport = 0
for i=1,15000 do 
    dt = CFL * rk:computeInvSpectralRadius(law,solution);  
    norm = rk:iterate44(law,t, dt,solution)
    --  norm = rk:iterateEuler(law,t, dt,solution)
    t = t+dt
    if (i % 100 == 0) then 
       print('|ITER|',i,'|NORM|',norm,'|DT|',dt,'|CPU|',os.clock()-x)
       solution:exportMsh(string.format('output/solution-%06d', i), t, nbExport)  
       nbExport  = nbExport  + 1
    end
end
