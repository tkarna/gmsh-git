
Ltickness =  4000;
Lheight   = 50000;
Lpmlleft  =  1000;
Lpmlright =  1000;

xDisk = 0;
yDisk = 0;
rDisk = 1000;

lc = 250;


// Disk

pDisk[0]=newp; Point(pDisk[0])={xDisk,yDisk,0};
pDisk[1]=newp; Point(pDisk[1])={xDisk-rDisk,yDisk,0,lc};
pDisk[2]=newp; Point(pDisk[2])={xDisk,yDisk+rDisk,0,lc};
pDisk[3]=newp; Point(pDisk[3])={xDisk+rDisk,yDisk,0,lc};
pDisk[4]=newp; Point(pDisk[4])={xDisk,yDisk-rDisk,0,lc};
lDisk[0]=newl; Circle(lDisk[0])={pDisk[1],pDisk[0],pDisk[2]};
lDisk[1]=newl; Circle(lDisk[1])={pDisk[2],pDisk[0],pDisk[3]};
lDisk[2]=newl; Circle(lDisk[2])={pDisk[3],pDisk[0],pDisk[4]};
lDisk[3]=newl; Circle(lDisk[3])={pDisk[4],pDisk[0],pDisk[1]};
llDisk=newll; Line Loop(llDisk)={lDisk[]};

// Rectangle

xborder[0]=-Ltickness/2-Lpmlleft;
xborder[1]=-Ltickness/2;
xborder[2]= Ltickness/2;
xborder[3]= Ltickness/2+Lpmlright;

For i In {0:3}
  pT[i]=newp; Point(pT[i])={xborder[i], Lheight/2,0,lc};
  pB[i]=newp; Point(pB[i])={xborder[i],-Lheight/2,0,lc};
  lV[i]=newl; Line(lV[i])={pB[i],pT[i]};
EndFor
For i In {0:2}
  lT[i]=newl; Line(lT[i])={pT[i],pT[i+1]};
  lB[i]=newl; Line(lB[i])={pB[i],pB[i+1]};
EndFor
For i In {0:2}
  ll[i]=newll; Line Loop(ll[i])={lB[i], lV[i+1], -lT[i], -lV[i]};
EndFor

// Surfaces

zone[0]=news; Plane Surface(zone[0])={ll[0]};
zone[1]=news; Plane Surface(zone[1])={ll[1],llDisk};
zone[2]=news; Plane Surface(zone[2])={ll[2]};

// Physical elements

Physical Line("BOUNDARY_TOP")    = {lT[]};
Physical Line("BOUNDARY_BOTTOM") = {lB[]};
Physical Line("BOUNDARY_LEFT")   = {lV[0]};
Physical Line("INTERFACE_LEFT")  = {lV[1]};
Physical Line("INTERFACE_RIGHT") = {lV[2]};
Physical Line("BOUNDARY_RIGHT")  = {lV[3]};
Physical Line("BOUNDARY_DISK")   = {lDisk[]};

Physical Surface ("PML_LEFT")  = {zone[0]};
Physical Surface ("MEDIUM")    = {zone[1]};
Physical Surface ("PML_RIGHT") = {zone[2]};
