Ldom = 1e7;
Llayer = 1.5e6;

Mesh.CharacteristicLengthMax=0.5e6;

coord[] = {-Ldom-Llayer, -Ldom, Ldom, Ldom+Llayer};

n = 0;
For k In {0:3}
  For l In {0:3}
    p[k*4+l] = newp; Point(p[k*4+l]) = {coord[l], coord[k], 0};
  EndFor
EndFor

For k In {0:3}
  For l In {0:2}
    lH[k*3+l] = newl; Line(lH[k*3+l]) = {p[k*4+l], p[k*4+l+1]};
  EndFor
EndFor

For k In {0:2}
  For l In {0:3}
    lV[k*4+l] = newl; Line(lV[k*4+l]) = {p[k*4+l], p[k*4+l+4]};
  EndFor
EndFor

For k In {0:2}
  For l In {0:2}
    ll[k*3+l] = newll; Line Loop(ll[k*3+l]) = {lH[k*3+l],lV[k*4+l+1],-lH[k*3+l+3],-lV[k*4+l]};
    s[k*4+l] = news; Plane Surface(s[k*4+l]) = {ll[k*3+l]};
  EndFor
EndFor

Physical Surface("Surface") = {s[]};
Physical Line("Border") = {lH[0],lH[1],lH[2],lV[3],lV[7],lV[11],-lH[11],-lH[10],-lH[9],-lV[8],-lV[4],-lV[0]};
