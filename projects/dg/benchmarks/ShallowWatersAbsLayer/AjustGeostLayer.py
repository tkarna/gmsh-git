from dgpy import *
import time
import math
import gc
import gmshPartition


#----- Parameters

f = 0.729e-4
g = 9.80616
h = 5e3

eta0 = 5e2
L = 1e7
R = 1e6


#----- Model

model = GModel()
model.load(gmshPartition.simple('AjustGeostLayer.msh'))


#----- Group

order = 1
dimension = 2
groups = dgGroupCollection(model, dimension, order)
XYZ = groups.getFunctionCoordinates();


#----- Conservation law with physical parameters

claw = dgConservationLawShallowWaterAbsLayer2d()

CCode = """
#include "fullMatrix.h"
#include "function.h"
extern "C" {
void absLayerCoef(dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (size_t i=0; i<sol.size1(); i++) {
    double g = 9.80616;
    double h = 5e3;
    double c = sqrt(g*h);
    double Ldom = 1e7;
    double Llayer = 1.5e6;
    double xLoc = fabs(xyz(i,0))-Ldom;
    double yLoc = fabs(xyz(i,1))-Ldom;
    double sigmaX = 0;
    double sigmaY = 0;
    if (xLoc>0)
      sigmaX = c/Llayer * xLoc/(Llayer-xLoc);
    if (yLoc>0)
      sigmaY = c/Llayer * yLoc/(Llayer-yLoc);
    sol.set(i, 0, sigmaX);
    sol.set(i, 1, sigmaY);
  }
}
}
"""

tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CCode, tmpLib);
Msg.Barrier()
absLayerCoefFunc = functionC(tmpLib, "absLayerCoef", 2, [XYZ])

bathymetryFunc = functionConstant([h])
coriolisFunc   = functionConstant([f])

claw.setBathymetry(bathymetryFunc)
claw.setCoriolisFactor(coriolisFunc)
claw.setAbsLayerCoef(absLayerCoefFunc)
claw.setIsLinear(1)


#----- Initial solution

def initialCondition(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)-L/2
    y = xyz.get(i,1)-L/2
    eta = eta0*math.exp(-(x*x+y*y)/(R*R))
    val.set(i,0,eta)
    val.set(i,1,0)
    val.set(i,2,0)
initialConditionPython = functionPython(3,initialCondition,[XYZ])

solution = dgDofContainer(groups, claw.getNbFields())
solution.setFieldName(0,'eta_layer')
solution.setFieldName(1,'u_layer')
solution.setFieldName(2,'v_layer')
solution.L2Projection(initialConditionPython)
solution.exportMsh("output/AjustGeost_Layer_000000", 0, 0)


#----- Condition aux limites

BC = claw.newBoundaryWall()
claw.addBoundaryCondition('Border', BC)


#----- Solver

sys = linearSystemPETScBlockDouble()
dof = dgDofManager.newDGBlock (groups, 3, sys)
solver = dgERK(claw, dof, DG_ERK_44)

t = 0
#dt = 50.*(3./(2.*order+1)/2)
dt = 60

nbExport = 1
tic = time.clock()
for i in range (1,1000+1) :
  solver.iterate(solution, dt, t);
  norm = solution.norm()
  t = t+dt
  if (i%50 == 0):
     if Msg.GetCommRank() == 0 :
       print('Iterm %i Norm %.16e    DT%4.2g   T%8.2g   cpu %6.2g' % (i, norm, dt, t, time.clock() - tic))
     solution.exportMsh("output/AjustGeost_Layer_%06d" % (i), t, nbExport)
     nbExport = nbExport+1

print (time.clock()-tic)

Msg.Exit(0)
