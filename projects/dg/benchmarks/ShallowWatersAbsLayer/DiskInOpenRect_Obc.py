from gmshpy import *
from dgpy import *
import os
import time
import math


# Mesh, Geometry & Groups

model = GModel()
model.load('DiskInOpenRect.msh')

dim = 2
order = 1
groups = dgGroupCollection(model, dim, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()


# Conservation law

gValue = 9.81
hValue = 100
cValue = math.sqrt(gValue*hValue)
rhoValue = 1/gValue
physCoef = functionConstant([cValue,rhoValue])

law = dgConservationLawWave(2)
law.setPhysCoef(physCoef)


# Initial solution

initSol = functionConstant([0.,0.,0.])

sol = dgDofContainer(groups,3)
sol.setFieldName(0,'eta_Obc')
sol.setFieldName(1,'u_Obc')
sol.setFieldName(2,'v_Obc')
sol.L2Projection(initSol)
sol.exportMsh('output/DiskInOpenRect_Obc_00000',0,0)


# External forcing

t=0.
x0=-20000
r=12000
def extFields(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    value = math.exp(-((x-cValue*t-x0)*(x-cValue*t-x0))/(r*r))
    val.set(i,0,value)
    val.set(i,1,value/(cValue*rhoValue))
    val.set(i,2,0.)
extFieldsPython = functionPython(3,extFields,[xyz])

law.addInterfaceOpenOutflowWithInflow('INTERFACE_LEFT',extFieldsPython)
law.addInterfaceOpenOutflow('INTERFACE_RIGHT')


# Boundary conditions

law.addBoundaryCondition('BOUNDARY_TOP',    law.newBoundaryWall('BOUNDARY_TOP'))
law.addBoundaryCondition('BOUNDARY_BOTTOM', law.newBoundaryWall('BOUNDARY_BOTTOM'))
law.addBoundaryCondition('BOUNDARY_RIGHT',  law.newBoundaryWall('BOUNDARY_RIGHT'))
law.addBoundaryCondition('BOUNDARY_LEFT',   law.newBoundaryWall('BOUNDARY_LEFT'))
law.addBoundaryCondition('BOUNDARY_DISK',   law.newBoundaryWall('BOUNDARY_DISK'))


# LOOP

dt = 0.5
nbExport = 1
solver = dgERK(law, None, DG_ERK_44)
for i in range(0,2000):
  t = i*dt
  solver.iterate(sol, dt, t);
  norm = sol.norm()
  if (math.isnan(norm)):
    print 'ERROR : NAN'
    break
  if (i % 20 == 0):
    if (Msg.GetCommRank()==0) :
      print'|ITER|', i, '|NORM|', norm, '|DT|', dt, '|t|', t
    sol.exportMsh("output/DiskInOpenRect_Obc_%05i" % (nbExport), t, nbExport)
    nbExport = nbExport + 1


Msg.Exit(0)
