Ldom = 1e7;

Mesh.CharacteristicLengthMax=0.5e6;

Point(1) = {-Ldom, -Ldom, 0};
Point(2) = { Ldom, -Ldom, 0};
Point(3) = { Ldom,  Ldom, 0};
Point(4) = {-Ldom,  Ldom, 0};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

Line Loop(5) = {3, 4, 1, 2};
Plane Surface(6) = {5};

Physical Surface("Surface") = {6};
Physical Line("Border") = {1,2,3,4};
