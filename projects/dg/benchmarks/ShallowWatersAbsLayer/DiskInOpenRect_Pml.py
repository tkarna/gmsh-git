from gmshpy import *
from dgpy import *
import os
import time
import math


Ltickness =  4000
Lheight   = 50000
Lpml      =  1000


# Mesh, Geometry & Groups

model = GModel()
model.load('DiskInOpenRect.msh')

dim = 2
order = 1
groups = dgGroupCollection(model, dim, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()


# Conservation law

gValue = 9.81
hValue = 100
cValue = math.sqrt(gValue*hValue)
rhoValue = 1/gValue
physCoef = functionConstant([cValue,rhoValue])

law = dgConservationLawWave(2,'PmlStraight')
law.setPhysCoef(physCoef)


# Initial solution

initSol = functionConstant([0.,0.,0.,0.])

sol = dgDofContainer(groups,4)
sol.setFieldName(0,'eta_Pml')
sol.setFieldName(1,'u_Pml')
sol.setFieldName(2,'v_Pml')
sol.setFieldName(3,'q_Pml')
sol.L2Projection(initSol)
sol.exportMsh('output/DiskInOpenRect_Pml_00000',0,0)


# Pml and External forcing

law.useGenericPmlDir('BeveledCuboid',                    0,0,0, Ltickness/2.,Lheight,0., 0.)
law.useGenericPmlCoef('BeveledCuboid',Lpml*1.001,cValue, 0,0,0, Ltickness/2.,Lheight,0., 0., 'HypSh')

t=0.
x0=-20000
r=12000
def extFields(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    value = math.exp(-math.pow((x-cValue*t-x0)/r,2))
    val.set(i,0,value)
    val.set(i,1,value/(cValue*rhoValue))
    val.set(i,2,0.)
extFieldsPython = functionPython(3,extFields,[xyz])

solInc = dgDofContainer(groups,3)
solInc.setFieldName(0,'eta_Inc')
solInc.setFieldName(1,'u_Inc')
solInc.setFieldName(2,'v_Inc')
solInc.L2Projection(extFieldsPython)
solInc.exportMsh('output/DiskInOpenRect_Inc_00000',0,0)

law.addPml('PML_LEFT')
law.addInterfacePmlWithInflow('INTERFACE_LEFT',extFieldsPython)
law.addPml('PML_RIGHT')
law.addInterfacePml('INTERFACE_RIGHT')


# Boundary conditions

law.addBoundaryCondition('BOUNDARY_TOP',    law.newBoundaryWall('BOUNDARY_TOP'))
law.addBoundaryCondition('BOUNDARY_BOTTOM', law.newBoundaryWall('BOUNDARY_BOTTOM'))
law.addBoundaryCondition('BOUNDARY_RIGHT',  law.newBoundaryWall('BOUNDARY_RIGHT'))
law.addBoundaryCondition('BOUNDARY_LEFT',   law.newBoundaryWall('BOUNDARY_LEFT'))
law.addBoundaryCondition('BOUNDARY_DISK',   law.newBoundaryWall('BOUNDARY_DISK'))


# LOOP

dt = 0.5
nbExport = 1
solver = dgERK(law, None, DG_ERK_44)
for i in range(0,2000):
  t = i*dt
  solver.iterate(sol, dt, t);
  norm = sol.norm()
  if (math.isnan(norm)):
    print 'ERROR : NAN'
    break
  if (i % 20 == 0):
    if (Msg.GetCommRank()==0) :
      print'|ITER|', i, '|NORM|', norm, '|DT|', dt, '|t|', t
    sol.exportMsh("output/DiskInOpenRect_Pml_%05i" % (nbExport), t, nbExport)
    solInc.L2Projection(extFieldsPython)
    solInc.exportMsh("output/DiskInOpenRect_Inc_%05i" % (nbExport), t, nbExport)
    nbExport  = nbExport + 1


Msg.Exit(0)

