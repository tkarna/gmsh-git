from dgpy import *
import time
import math
import gc
import gmshPartition


#----- Parameters

f = 0.729e-4
g = 9.80616
h = 5e3

eta0 = 5e2
L = 1e7
R = 1e6


#----- Model

model = GModel()
model.load(gmshPartition.simple('AjustGeostFlather.msh'))


#----- Group

order = 1
dimension = 2
groups = dgGroupCollection(model, dimension, order)
XYZ = groups.getFunctionCoordinates();


#----- Conservation law with physical parameters

claw = dgConservationLawShallowWaterAbsLayer2d()

bathymetryFunc = functionConstant([h])
coriolisFunc   = functionConstant([f])

claw.setBathymetry(bathymetryFunc)
claw.setCoriolisFactor(coriolisFunc)
claw.setIsLinear(1)


#----- Initial solution

def initialCondition(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)-L/2
    y = xyz.get(i,1)-L/2
    eta = eta0*math.exp(-(x*x+y*y)/(R*R))
    val.set(i,0, eta)
    val.set(i,1,0)
    val.set(i,2,0)
    #val.set(i,1, 2*y*g/(f*R*R)*eta)
    #val.set(i,2,-2*x*g/(f*R*R)*eta)
initialConditionPython = functionPython(3,initialCondition,[XYZ])

solution = dgDofContainer(groups, claw.getNbFields())
solution.setFieldName(0,'eta_ref')
solution.setFieldName(1,'u_ref')
solution.setFieldName(2,'v_ref')
solution.L2Projection(initialConditionPython)
solution.exportMsh("output/AjustGeost_Flather_000000", 0, 0)


#----- Condition aux limites

BC = claw.newBoundaryOpenOutflow()
claw.addBoundaryCondition('Border', BC)


#----- Solver

sys = linearSystemPETScBlockDouble()
dof = dgDofManager.newDGBlock (groups, 3, sys)
solver = dgERK(claw, dof, DG_ERK_44)

t = 0
#dt = 50.*(3./(2.*order+1)/2)
dt = 60

nbExport = 1
tic = time.clock()
for i in range (1,1000+1) :
  solver.iterate(solution, dt, t);
  norm = solution.norm()
  t = t+dt
  if (i%50 == 0):
     if Msg.GetCommRank() == 0 :
       print('Iterm %i Norm %.16e    DT%4.2g   T%8.2g   cpu %6.2g' % (i, norm, dt, t, time.clock() - tic))
     solution.exportMsh("output/AjustGeost_Flather_%06d" % (i), t, nbExport)
     nbExport = nbExport+1

print (time.clock()-tic)

Msg.Exit(0)
