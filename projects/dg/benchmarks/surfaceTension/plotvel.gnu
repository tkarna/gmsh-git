
set terminal fig color textspecial  fontsize 18
set output "plotvel.fig"

set xlabel '$\sqrt{Ne}$'
set ylabel '$L_\infty(\left||\mathbf{u}\right||)$'
set logscale x
set logscale y

plot "conv.txt" u 1:2  w lp t 'uniform refine', "conva.txt" u 1:2  w lp t 'adaptive refine'
set term aqua
replot
