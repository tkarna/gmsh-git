from dgpy import *
from Incompressible import *
from Common import *
from math import *
import sys

try : os.mkdir("output");
except: 0;

os.system("rm output/*")
#-------------------------------------------------
#-- 2D Laplace problem
#-- static bubble with surface tension
#-------------------------------------------------

#-------------------------------------------------
#-- Parameters
#-------------------------------------------------
meshName = "carre_simple"
dim = 2
UGlob = 1.e-15
R = 1.0
L = 4.0

GAMMA = 2.0
RHO = 1.0
MU  = 1.0
RATIO = 100.0
dt0 = 1.e-6

TRANSIENT = True
ADAPT = False

level = 2
h0 = (L/15.0)
hmax = h0/pow(2,level)
hmin = hmax
E = 2.0*hmax # for smoothing dirac
if (ADAPT):
	hmin = h0/(pow(4,level))
	E = 5*hmin # for smoothing dirac
	GmshSetOption("Mesh","CharacteristicLengthMax", hmax);
else :
	GmshSetOption("Mesh","CharacteristicLengthMin", hmax);
	GmshSetOption("Mesh","CharacteristicLengthMax", hmax);

GmshSetOption("Mesh","CharacteristicLengthFactor", 1.0);

#-------------------------------------------------
#-- Adaptation
#-------------------------------------------------

if (ADAPT): 
	print'---- ADAPT MESH WITH LEVELSET'
	adaptedName = meshName + "_adapt" 
	myLS = gLevelsetSphere(0.0, 0.0, 0.0, 1.0)
	
	mesh = GModel()
	mesh.load(meshName+geo)

	GmshSetOption("Mesh","SmoothRatio",   2.0);
	mesh.adaptMesh([5], [myLS], [[6*hmax,hmin,hmax]], 20, False)
	mesh.writeMSH(meshName+".msh",2.2,False,False,True,1.0,0,0)
else:
	genMesh(meshName, dim, 1)

#-------------------------------------------------
#-- Initial function
#-------------------------------------------------
def initF(FCT, XYZ):
	for i in range(0,FCT.size1()):
		FCT.set(i,0, UGlob) #u
		FCT.set(i,1, 0.0) #v
		FCT.set(i,2, 0.0) #p
		
def areaF(FCT, LS):
  for i in range(0,FCT.size1()):
    phi = LS.get(i,0)
    value = 0.0
    if (phi < 0.0):
      value = 1.0
    FCT.set(i,0, value)

def presinF(FCT, LS, SOL):
  for i in range(0,FCT.size1()):
    phi = LS.get(i,0)
    pres = SOL.get(i,2)
    value = 0.0
    if (phi < 0.0):
      value = pres
    FCT.set(i,0, value)

def presoutF(FCT, LS, SOL):
  for i in range(0,FCT.size1()):
    phi = LS.get(i,0)
    pres = SOL.get(i,2)
    value = 0.0
    if (phi > 0.0):
      value = pres
    FCT.set(i,0, value)
#-----------------------
#-------------------------------------------------
#-- Case specific functions
#-------------------------------------------------
def surfaceTension(FCT, XYZ, LS, RHOF) :

  for i in range(0,FCT.size1()):
    x = XYZ.get(i,0) 
    y = XYZ.get(i,1)
    if (x==0.0 and y == 0):
	    x = 1.e-8;
    phi = LS.get(i,0)
    rho = RHOF.get(i,0)
    kappa = 1.0
    dirac = 0.0
    if (fabs(phi)<E):
	    eps = 1.e-8
	    #Heps  = 0.5 + 1./32.*(45.*((phi+eps)/E)-50*pow((phi+eps)/E, 3.)+21.*pow((phi+eps)/E,5.))
	    #Hmeps = 0.5 + 1./32.*(45.*((phi-eps)/E)-50*pow((phi-eps)/E, 3.)+21.*pow((phi-eps)/E,5.))
	    #Heps  = 0.5*(tanh(pi*(phi+eps)/(E)))
	    #Hmeps = 0.5*(tanh(pi*(phi-eps)/(E)))
	    Heps  = 0.5*(1 + (phi+eps)/E + 1./pi * sin(pi*((phi+eps)/E)))
	    Hmeps = 0.5*(1 + (phi-eps)/E + 1./pi * sin(pi*((phi-eps)/E)))
	    grad_Heav = fabs((Heps - Hmeps)/(2.*eps))
	    dirac = grad_Heav
	    #dirac  = 1./(2.*E)*(1.+cos(pi*phi/E))
	    #dirac  = 1./(E)*(1.-fabs(phi/E))
    nx = x/sqrt(x*x+y*y)
    ny = y/sqrt(x*x+y*y)
    norm = sqrt(nx*nx+ny*ny)
    FCT.set(i,0,-GAMMA*kappa*(nx/norm)*dirac/rho)
    FCT.set(i,1,-GAMMA*kappa*(ny/norm)*dirac/rho)
    FCT.set(i,2,0.0)
#-------------------------------------------------
#-- Initialization
#-------------------------------------------------
ns = Incompressible(meshName, dim)

MY_LEVELSET  = gLevelsetSphere(0.0, 0.0, 0.0, 1.0)
LS  = functionLevelsetValueNew(MY_LEVELSET)

rhoF = functionLevelsetSmoothNew(LS, RHO, RATIO*RHO, E)
muF = functionLevelsetSmoothNew(LS, MU, MU, E)

ns.initializeIncomp(initF, rhoF, muF , UGlob)

ST =  functionPython(3, surfaceTension, [ns.XYZ,LS, rhoF])
ns.law.setSource(ST)

ns.solution.exportFunctionMsh(rhoF, 'output/rho', 0, 0, '')
ns.solution.exportFunctionMsh(ST, 'output/st', 0, 0, '')

#-------------------------------------------------
#-- Boundary Conditions
#-------------------------------------------------

ns.strongBoundaryConditionLine('Inlet', ns.WALL)
ns.strongBoundaryConditionLine('Outlet', ns.PZERO)
ns.strongBoundaryConditionLine('WallTop', ns.WALL)
ns.strongBoundaryConditionLine('WallBottom', ns.WALL)

#-------------------------------------------------
#-- TimeStepping
#-------------------------------------------------
nbTimeSteps = 10
dt0 = 1.e-2
ATol = 1.e-8
RTol = 1.e-5
Verb = 2
petscOptions ="-ksp_rtol 1.e-3 -pc_type lu -pc_factor_levels 0"

if (TRANSIENT):
	ns.initSolver(1, ATol,RTol, Verb, petscOptions, 1)
	ns.doOneIteration(dt0, 0)
	nbiter = 0
	#ns.initSolver(1, ATol,RTol, Verb, petscOptions, 10)
	#dt_phys = 2*R*MU/GAMMA
	#dt_num = sqrt(RHO/GAMMA)*pow(hmin,1.5)
	#dt = min(dt_phys, dt_num)
	#print("dt phys : %g num: %g " %(dt_phys, dt_num))
	#t = 0.0
	#tic = time.clock()
	#for i in range (0,250) :
	#       ns.doOneIteration(dt, i)
        #	t = t+dt0
	#nbiter = 25
else :
	nbiter = ns.pseudoTimeSteppingSolve(1, nbTimeSteps, dt0, ATol, RTol, Verb, petscOptions)

#-------------------------------------------------
#-- Validation
#-------------------------------------------------
print '------------------------------------------------------'
print ("---- Validation  ")
print '------------------------------------------------------'
print ("Nelems =%d  hmax =%.3e"  %  (ns.groups.getElementGroup(0).getNbElements(), hmax))
if (ADAPT):
	print ("ADAPT: hmin =%.3e"  %  (hmin))
Pexact = GAMMA/R

minV=fullMatrixDouble(3,1)
maxV=fullMatrixDouble(3,1)
ns.solution.minmax(minV, maxV)
VMax = math.sqrt(maxV(0,0)*maxV(0,0)+maxV(1,0)*maxV(1,0));
PMax = maxV(2,0)
PMin = minV(2,0)

print'------------------------------------------------------'
print("Laplace = %g  " % (GAMMA*RHO*2.0*R/(MU*MU)))
print ("Velocity max:  %.2e Ca : %g" % (VMax, VMax*MU/GAMMA))
print ("Pressure max :  %.2e min: %.2e" %  (PMax, PMin))
print ("Error Pressure :  %.2e" % ( fabs(Pexact - (PMax - PMin))) )
print ("Error Pressure (rel):  %.2e" % ( fabs(Pexact - (PMax - PMin))/Pexact) )

P_in=fullMatrixDouble(1,1)
P_out=fullMatrixDouble(1,1)
area_in = fullMatrixDouble(1,1)

AREA = functionPython(1, areaF, [LS])
area1=dgFunctionIntegrator(ns.groups, AREA)
area1.compute(area_in, "All")
PRESIN =  functionPython(1, presinF, [LS, ns.solution.getFunction()])
PRESOUT =  functionPython(1, presoutF,[ LS, ns.solution.getFunction()])
integ1 = dgFunctionIntegrator(ns.groups, PRESIN, ns.solution)
integ1.compute(P_in, "All")
integ2 = dgFunctionIntegrator(ns.groups, PRESOUT, ns.solution)
integ2.compute(P_out, "All")

areaIn = area_in(0,0)
areaOut = 16.0 - areaIn

Pin = P_in(0,0)/areaIn
Pout = P_out(0,0)/areaOut
DP = fabs(Pexact - (Pin - Pout))
DP_rel = DP/Pexact

print'------------------------------------------------------'
print ("Pressure in domain Pin: %.2e Pout : %.2e" % (Pin, Pout))
print ("Error Pressure domain DP :%.2e " % DP)
print ("Error Pressure domain DP(rel) :%.2e " % DP_rel)
print'------------------------------------------------------'

os.system("gmsh %s.msh output/ns%06d_pres.msh cut.opt -"%(meshName,nbiter))
os.system("gnuplot plot.plt")
