set terminal fig color textspecial  fontsize 18
set output "plotpres.fig"

set xlabel '$\sqrt{Ne}$'
set ylabel '$\mathcal{E}_{\Delta \bar p}$'
set logscale x
set logscale y

plot "conv.txt" u 1:3  w lp t 'uniform refine', "conva.txt" u 1:3  w lp t 'adaptive refine'

set term aqua
replot

