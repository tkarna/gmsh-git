R = 1.0 ;
l = 2.0; //before cylinder
L = 2.0; //after cylinder
H = 2.0; //above and under cylinder
H2 = 2.0;


Point(1) = {0,0,0};
Point(2) = {R,0,0};
Point(3) = {0,R, 0};
Point(4) = {0,-R,0};
Point(5) = {-R,0,0};
Point(6) = {-l,-H,0};
Point(7) = {-l, H,0};
Point(8) = {L,  H,0};
Point(9) = {L, -H,0};

Circle(1) = {3,1,2};
Circle(2) = {2,1,4};
Circle(3) = {4,1,5};
Circle(4) = {5,1,3};

Line(5) = {7,8};
Line(6) = {8,9};
Line(7) = {9,6};
Line(8) = {6,7};
Line Loop(9) = {5,6,7,8};
Line Loop(10) = {3,4,1,2};
Plane Surface(11) = {9,10};
Plane Surface(12) = {10};

Physical Surface("All_in") = {12};
Physical Surface("All_out") ={11};
Physical Line("Cylinder") = {1,2,3,4};
Physical Line("WallBottom") = {7};
Physical Line("WallTop") = {5};
Physical Line("Inlet") = {8};
Physical Line("Outlet") = {6};


