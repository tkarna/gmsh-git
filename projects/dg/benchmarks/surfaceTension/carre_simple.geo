R = 1.0 ;
l = 2.0; //before cylinder
L = 2.0; //after cylinder
H = 2.0; //above and under cylinder
H2 = 2.0;

//lc = 0.08;

Point(6) = {-l,-H,0};
Point(7) = {-l, H,0};
Point(8) = {L,  H,0};
Point(9) = {L, -H,0};

Line(5) = {7,8};
Line(6) = {8,9};
Line(7) = {9,6};
Line(8) = {6,7};
Line Loop(9) = {5,6,7,8};
Plane Surface(12) = {9};

Physical Surface("All") ={12};
Physical Line("WallBottom") = {7};
Physical Line("WallTop") = {5};
Physical Line("Inlet") = {8};
Physical Line("Outlet") = {6};


