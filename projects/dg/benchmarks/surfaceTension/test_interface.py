from dgpy import *
from Incompressible import *
from Common import *
from math import *
import sys

try : os.mkdir("output");
except: 0;

os.system("rm output/*")
#-------------------------------------------------
#-- 2D Laplace problem
#-- static bubble with surface tension
#-------------------------------------------------

#-------------------------------------------------
#-- Parameters
#-------------------------------------------------
meshName = "carre"
dim = 2
UGlob = 1.e-15
R = 1.0
L = 4.0

GAMMA = 2.0
RHO = 1.0
MU  = 10.0
RATIO = 1.0
dt0 = 1.e-5

TRANSIENT = False
ADAPT = True

level = 1
hmax = 0.2 #(L/30.0)/pow(2,level)
hmin = hmax
if (ADAPT):
	hmin = 0.1*hmax/(pow(4,level))
	GmshSetOption("Mesh","CharacteristicLengthMax", hmax);
else :
	GmshSetOption("Mesh","CharacteristicLengthMin", hmax);
	GmshSetOption("Mesh","CharacteristicLengthMax", hmax);

GmshSetOption("Mesh","CharacteristicLengthFactor", 1.0);

#-------------------------------------------------
#-- Adaptation
#-------------------------------------------------

if (ADAPT): 
	print'---- ADAPT MESH WITH LEVELSET'
	adaptedName = meshName + "_adapt" 
	myLS = gLevelsetSphere(0.0, 0.0, 0.0, 1.0)
	
	mesh = GModel()
	mesh.load(meshName+geo)

	GmshSetOption("Mesh","SmoothRatio",   2.0);
	mesh.adaptMesh([5], [myLS], [[4*hmax,hmin,hmax]], 20, False)
	mesh.writeMSH(meshName+".msh",2.2,False,False,True,1.0,0,0)
	#meshCut = mesh.buildCutGModel(myLS, False, True)
	#meshCut.writeMSH(meshName+".msh",2.2,False,False,True,1.0,0,0)	
else:
	genMesh(meshName, dim, 1)

#-------------------------------------------------
#-- Initial function
#-------------------------------------------------
def initF(FCT, XYZ):
	for i in range(0,FCT.size1()):
		FCT.set(i,0, UGlob) #u
		FCT.set(i,1, 0.0) #v
		FCT.set(i,2, 0.0) #p
		  
#-------------------------------------------------
#-- Initialization
#-------------------------------------------------
ns = Incompressible(meshName, dim)

rhoF = functionConstant(RHO)
muF = functionConstant(MU)

ns.initializeIncomp(initF, rhoF, muF , UGlob)

#-------------------------------------------------
#-- Boundary Conditions
#-------------------------------------------------

ns.strongBoundaryConditionLine('Inlet', ns.PZERO)
ns.strongBoundaryConditionLine('Outlet', ns.PZERO)
ns.strongBoundaryConditionLine('WallTop', ns.PZERO)
ns.strongBoundaryConditionLine('WallBottom', ns.PZERO)

#-------------------------------------------------
#-- TimeStepping
#-------------------------------------------------
nbTimeSteps = 10
dt0 = 1.e-6
ATol = 1.e-8
RTol = 1.e-3
Verb = 2
petscOptions ="-ksp_rtol 1.e-3 -pc_type lu -pc_factor_levels 0"

if (TRANSIENT):
	ns.initSolver(1, ATol,RTol, Verb, petscOptions, 1)
	dt_phys = 2*R*MU/GAMMA
	dt_num = sqrt(RHO/GAMMA)*pow(hmin,1.5)
	dt = min(dt_phys, dt_num)
	print("dt phys : %g num: %g " %(dt_phys, dt_num))
	t = 0.0
	tic = time.clock()
	for i in range (0,2) :
		ns.doOneIteration(dt, i)
		t = t+dt0
	nbiter = 1
else :
	nbiter = ns.pseudoTimeSteppingSolve(1, nbTimeSteps, dt0, ATol, RTol, Verb, petscOptions)

#-------------------------------------------------
#-- Validation
#-------------------------------------------------
print '------------------------------------------------------'
print ("---- Validation  ")
print '------------------------------------------------------'
print ("Nelems =%d  hmax =%.3e"  %  (ns.groups.getElementGroup(0).getNbElements(), hmax))
if (ADAPT):
	print ("ADAPT: hmin =%.3e"  %  (hmin))
Pexact = GAMMA/R

minV=fullMatrixDouble(3,1)
maxV=fullMatrixDouble(3,1)
ns.solution.minmax(minV, maxV)
VMax = math.sqrt(maxV(0,0)*maxV(0,0)+maxV(1,0)*maxV(1,0));
PMax = maxV(2,0)
PMin = minV(2,0)

print'------------------------------------------------------'
print("Laplace = %.12e  " % (GAMMA*RHO*2.0*R/(MU*MU)))
print ("Velocity max:  %.12e Ca : %.12e" % (VMax, VMax*MU/GAMMA))
print ("Pressure max :  %.12e min: %.12e" %  (PMax, PMin))
print ("Error Pressure :  %.12e" % ( fabs(Pexact - (PMax - PMin))/Pexact) )

P_in=fullMatrixDouble(1,1)
P_out=fullMatrixDouble(1,1)
area_in = fullMatrixDouble(1,1)

ONE = functionConstant(1.0)
area1=dgFunctionIntegrator(ns.groups, ONE)
area1.compute(area_in, "All_in")
integ1 = dgFunctionIntegrator(ns.groups, ns.law.getPressure(), ns.solution)
integ1.compute(P_in, "All_in")
integ2 = dgFunctionIntegrator(ns.groups, ns.law.getPressure(), ns.solution)
integ2.compute(P_out, "All_out")

areaIn = area_in(0,0)
areaOut = 16.0 - areaIn

Pin = P_in(0,0)/areaIn
Pout = P_out(0,0)/areaOut
DP_rel = fabs(Pexact - (Pin - Pout))/Pexact

print'------------------------------------------------------'
print ("Pressure in domain Pin: %.12e Pout : %.12e" % (Pin, Pout))
print ("Error Pressure domain DP :%.12e " % DP_rel)
print'------------------------------------------------------'

os.system("gmsh %s.msh output/ns%06d_pres.msh cut.opt -"%(meshName,nbiter))
os.system("gnuplot plot.plt")
