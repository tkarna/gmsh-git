from gmshpy import *
from dgpy import *
import time
import copy

model = GModel()
model.load ('dam.msh')
order = 1
dimension = 2
groups = dgGroupCollection(model, dimension, order)

def initial_condition(f , XYZ ):
  for i in range(0,XYZ.size1()):
    x = XYZ.get(i,0)
    y = XYZ.get(i,1)
    z = XYZ.get(i,2)
    if (y*y+x*x)>0.05:
      f.set(i, 0, 0)
    else:
      f.set(i, 0, 65)
      f.set(i, 1, 0)
      f.set(i, 2, 0)

claw = dgConservationLawShallowWater2d()
XYZ = groups.getFunctionCoordinates()
FS = functionPython(3, initial_condition, [XYZ])
solution = dgDofContainer(groups, claw.getNbFields())
solution.L2Projection(FS)
f4 = functionConstant([10])
claw.setBathymetry(f4)

bndWall = claw.newBoundaryWall()
claw.addBoundaryCondition('Wall', bndWall)

rk=dgRungeKuttaMultirateConservative.new2a(groups,claw)
dt=rk.splitForMultirate(1000,   solution, [solution])
groups.buildGroupsOfInterfaces()

#rk=dgRungeKutta()

limiter = dgSlopeLimiter(claw)
rk.setLimiter(limiter) 

limiter.apply(solution)
solution.exportMsh('output/dambreak-init', 0., 0)
newGroups=dgGroupCollection(model,   dimension,   order);
newSolution=dgDofContainer(newGroups,   claw.getNbFields())
t=0
nbExport = 0
tic = time.clock()
for i in range (1,100+1) :
  print 'NELS=', newGroups.getNbElementGroups()
  norm = rk.iterate(t, dt, solution)
  t = t +dt
  if ( i%10 ==0 ):
     print('|ITER|',i,'|NORM|',solution.norm(),'|DT|',dt, '|T|',t)
     solution.exportMsh("output/dambreak-%06d" % (i), t, nbExport)
     nbExport = nbExport + 1
  if ( i%1 ==0 ):
    #groups.reinitializeGroups([solution])
    newGroups=dgGroupCollection(model,    dimension,    order);
    newSolution=dgDofContainer(newGroups,    claw.getNbFields())
    print 'NELS=', newGroups.getNbElementGroups()
    solution.transferGroups(newGroups, newSolution)
    groups=copy.copy(newGroups)
    solution=copy.copy(newSolution)
    #newGroups.deleteGroups()
    #newSolution.deleteDof()
    rk=dgRungeKuttaMultirateConservative.new2a(groups,claw)
    dt=rk.splitForMultirate(1000,   solution, [solution])
    groups.buildGroupsOfInterfaces()
    rk.setLimiter(limiter) 
    limiter.apply(solution)
    
print (time.clock()-tic)
Msg.Exit(0)
