model = GModel()
--model:load ('square.geo')
model:load ('square.msh')
order=1
dim=2
gc=dgGroupCollection(model,dim,order)
gc:buildGroupsOfInterfaces()


-- conservation law
law = dgConservationLawAdvectionDiffusion.diffusionLaw(functionConstant({0.01}))

-- boundary condition
law:addBoundaryCondition('Border',law:newOutsideValueBoundary("",functionConstant({1})))
--law:addBoundaryCondition('Border',law:new0FluxBoundary())

-- initial condition
function initial_condition( xyz , f )
  for i=0,xyz:size1()-1 do
    x = xyz:get(i,0)
    y = xyz:get(i,1)
    z = xyz:get(i,2)
    f:set (i, 0, math.exp(-100*((x-0.2)^2 +(y-0.3)^2)))
  end
end
ini = functionLua(1,'initial_condition',{functionCoordinates.get()})
solution = dgDofContainer(gc, law:getNbFields())
solution:L2Projection(ini)
solution:exportMsh('output/DiffusionE_00000',0,0)

rk = dgRungeKutta()
dof = dgDofManager.newDG (gc, law:getNbFields(), linearSystemPETSc())


-- main loop explicit
t=0
dt=0.01
print('Explicit RK44')
for i=1,1000 do
  t=t+dt
  norm = rk:iterate44(law,t,dt,solution)
  --norm = dg:RK44_TransformNodalValue(0.00001)
  if (i % 100 == 0) then 
    print('iter',i,norm)
    solution:exportMsh(string.format("output/DiffusionE-%05d", i), t, i/100)
  end
end

--------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------

print('Implicit Euler')
solution:L2Projection(ini)
solution:exportMsh('output/DiffusionI_00000',0,0)

-- main loop implicit
t=0
dt=1
for i=1,10 do
  norm = (1)
  t=t+dt
  dgSolver.implicitEuler(solution, law, dof, dt, t)
  if (i % 1 == 0) then 
    print('iter',i,norm)
    solution:exportMsh(string.format("output/DiffusionI-%05d", i), t, i)
  end
end

