from dgpy import *
from gmshpy import *
import os
import time
import math


# Load model & geometry

model = GModel()
model.load('edge.msh')
order = 1
groups = dgGroupCollection(model, 1, order)
groups.buildGroupsOfInterfaces()
xyz = groups.getFunctionCoordinates()


# Conservation law & boundary conditions

coefv = functionConstant([1000,0,0])
coefnu = functionConstant([0.])
law = dgConservationLawAdvectionDiffusion(coefv,coefnu) 

v_left  = functionConstant([0.*1,0,0])
v_right = functionConstant([0.*2,0,0])
law.addBoundaryCondition('Left' ,law.newOutsideValueBoundary("Left", v_left))
law.addBoundaryCondition('Right',law.newOutsideValueBoundary("Right",v_right))


# Initial solution

def initialSolution(val, xyz ):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    value = 0.*x/2 + 0.*1.5 + math.exp(-150*x**2)
    val.set(i, 0, value)
initialSolutionPython = functionPython(1, initialSolution, [xyz])
solution = dgDofContainer(groups, law.getNbFields())
solution.L2Projection(initialSolutionPython)
solution.exportMsh("output/AdvectionDiffusion1D-00000", 0, 0)


# Solve

print'*** Solve ***'

solverNumber = 2

if (solverNumber == 1) :
  dt = 1e-5
  sys = linearSystemPETScDouble()
  sys.setParameter("petscOptions","-pc_type lu")  # -ksp_monitor_true_residual -mat_view_matlab -vec_view_matlab" )
  dof = dgDofManager.newDG(groups, law.getNbFields(), sys)
  x = time.clock()
  solver = dgDIRK (law, dof, 1)
  solver.getNewton().setVerb(10)
  for i in range (1, 101):
    t = i*dt
    solver.iterate(solution, dt , t)
    solution.exportMsh("output/AdvectionDiffusion1D-%05d" % (i), t, i)
    print'|ITER|', i, '|DT|', dt, '|T|', t, '|CPU|', time.clock()-x
  
elif (solverNumber == 2) :
  dt = 1e-5
  sys = linearSystemPETScDouble()
  sys.setParameter("petscOptions","-pc_type lu")  # -ksp_monitor_true_residual -mat_view_matlab -vec_view_matlab" )
  dof = dgDofManager.newDG(groups, law.getNbFields(), sys)
  x = time.clock()
  solver = dgCrankNicholson(solution, law, dof, 0.)
  solver.getNewton().setVerb(10)
  solver.setComputeExplicitTerm(False)
  for i in range(1, 101) :
    t = i*dt
    solver.iterate(t)
    solution.exportMsh("output/AdvectionDiffusion1D-%05d" % (i), t, i)
    print'|ITER|', i, '|DT|', dt, '|T|', t, '|CPU|', time.clock()-x
  
else :
  dt = 1e-6
  solver = dgRungeKutta() 
  x = time.clock()
  for i in range(1, 1001) :
    t = i*dt
    #norm = solver.iterate44(law,t, dt,solution)
    norm = solver.iterateEuler(law, t, dt, solution)
    if (i % 10 == 0) :
      solution.exportMsh("output/AdvectionDiffusion1D-%05d" % (i/10), t, i/10)
      print'|ITER|', i, '|NORM|', norm, '|DT|', dt, '|T|', t, '|CPU|', time.clock()-x


Msg.Exit(0)

