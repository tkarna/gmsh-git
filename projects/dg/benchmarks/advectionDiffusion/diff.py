from dgpy import *;
import time
import os.path
CCode = """
#include "fullMatrix.h"
#include "dgDataCacheMap.h"
extern "C" {
void bnd (dgDataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< sol.size1(); i++) {
    double x = xyz(i, 0);
    double y = xyz(i, 1);
    sol(i, 0) = (x - 0.2) * (x-0.5) * (y - 0.3) * (y-0.7);
  }
}
}
"""
functionC.buildLibrary (CCode, "libtmp.so");

def genMesh(order):
  name = "meshes/square_p%i.msh" % order;
  if (not os.path.exists(name)):
    g2 = GModel();
    g2.load("square.geo")
    g2.mesh(2)
    if (order > 1):
      g2.setOrderN(order, True, False)
    g2.save(name)
  return name

timing = {};
for order in range(1,10):
  g = GModel()
  g.load("square.geo")
  g.load(genMesh(order))
  groups = dgGroupCollection(g,2,order);
  groups.buildGroupsOfInterfaces();
  nu = functionConstant(1);
  law = dgConservationLawAdvectionDiffusion.diffusionLaw(nu);
  #law.addBoundaryCondition("bnd",law.new0FluxBoundary());
  xyz = function.getCoordinates()
  bndValue = functionC("libtmp.so", "bnd",1, [xyz])
  ##law.addStrongBoundaryCondition(1, "bnd", bndValue);
  law.addBoundaryCondition("bnd", law.newOutsideValueBoundary("", bndValue));

  #sys = linearSystemPETScDouble();
 # sys = linearSystemTAUCSDouble();
  #dof = dgDofManager.newCG(groups, 1, sys);

  sys = linearSystemPETScBlockDouble();
  sys.setParameter("petscOptions", "-pc_type lu -ksp_monitor")
  dof = dgDofManager.newDGBlock(groups, 1, sys);

  solution = dgDofContainer(groups, 1);
  solution.setAll(0.)
  
  tic = time.clock()
  steady = dgSteady(law, dof)
  steady.getNewton().setRtol(1e12)
  steady.getNewton().setVerb(10)
  steady.solve(solution)

  #implicitEuler = dgDIRK(law, dof, 1);
  #implicitEuler.getNewton().setVerb(10)
  #implicitEuler.iterate(solution, 1, 0);

  #newton = dgNewton(dof, law)
  #newton.assembleSystem(0, None, 0)
  #newton.solve(None, 0, None, 0)

  print (time.clock()-tic)

  solution.exportMsh("output/sol-%i" % order)
