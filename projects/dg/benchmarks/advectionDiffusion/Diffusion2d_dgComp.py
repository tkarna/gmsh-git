# Import for python
from gmshpy import *
from dgpy import *
from scipy import *			
from scipy.sparse import *		
from scipy.sparse.linalg import *	
from matplotlib.pyplot import *		
from numpy.linalg import *
from numpy import matrix		

import time, math, os			

N = 12

# Conservation law
diff = functionConstant([10])
law = dgConservationLawAdvectionDiffusion.diffusionLaw(diff)

# Boundary condition
v_bc = functionConstant(0)		
law.addBoundaryCondition('Homogeneous',law.newOutsideValueBoundary("Homogeneous",v_bc))
nbFields = law.getNbFields()

# build the k meshes&solutions
models = []
gcs = []
solutions = []
systems = []
dofs = []
solvers = []
projectors = []
factor = 1.2
f0 = 0.1
scale = f0/factor**(N+5)
order=1
dimension =2

figure()
show()
figure()
cc = 'brgmcyk'

def L2N(L2,TD,TA) :
    for i in range(TD.size1()) :
      s = TD.get(i,0)
      a = TA.get(i,0)
      L2.set(i,0,(s-a)*(s-a))

def anaTrac(Ta,xz,t) :
    for i in range(xz.size1()) :
      x = xz.get(i,0)
      y = xz.get(i,1)
      time = t.get(i,0)
      K = 10*matrix([[1,0],[0,1]])
      X = matrix([[x],[y]])
      Ki = inv(K)
      Xt = X.T
      d = 100
      Ta.set(i,0,math.exp(-(Xt*(Ki*X))/(4*time))/((4*math.pi*time)*math.sqrt(d)))

for k in range(0,N):
  models.append(GModel())
  cmd = "gmsh -%d squareMG.geo -clscale %f" % (dimension,scale)
  os.system(cmd)
  if k == 0:
    scale = f0
  scale = scale/factor
  models[k].load('squareMG.msh')
  models[k].save('squareMG_%02d.msh'%(k))
  gcs.append(dgGroupCollection(models[k], dimension, order))
  gcs[k].buildGroupsOfInterfaces()
  solutions.append(dgDofContainer(gcs[k], nbFields))
  if k == 0:
    solution2 = dgDofContainer(gcs[k], nbFields)

  XYZ = getFunctionCoordinates()
  time2 = functionConstant([0.001])
  sol2 = functionPython(1,anaTrac,[XYZ,time2])
  solutions[k].L2Projection(sol2)

  residual = dgDofContainer(gcs[k], nbFields)
  sys = linearSystemPETScBlockDouble() #Block
  sys.setParameter("petscOption", "-pc_type lu -ksp_monitor")
  dof = dgDofManager.newDGBlock(gcs[k], 1, sys) 
  implicitRK2 = dgDIRK(law, dof,1)
  implicitRK2.getNewton().setRtol(1e-6)
  implicitRK2.getNewton().setAtol(1e-8)
  implicitRK2.getNewton().setVerb(10)
  export_interval =0.001
  next_export_time = export_interval
  dt = 0.001
  t = 0.0
  for i in range (10) :
    t = t+dt
    implicitRK2.iterate(solutions[k], dt, t)
    if ( t >= next_export_time):
      next_export_time = next_export_time + export_interval
      solutions[k].exportMsh("solu-%06d-%02d" % (i,k), t, i)

  if (k > 0):
    projectors.append(dgMesh2MeshProjection(solutions[k],solution2))
    projectors[k-1].projectFromTo(solutions[k],solution2)
    tracNorm = dgDofContainer(gcs[0],1)
    Td = solution2.getFunction()
    L2 = functionPython(1,L2N,[Td,Ta])
    tracNorm.interpolate(L2)
    tracNorm.exportMsh('L2', 0, 0)
    matrixe=fullMatrixDouble(1,1)
    L2inte = dgFunctionIntegrator(gcs[0],tracNorm.getFunction(),solution2)
    L2inte.compute(matrixe,"")
    sol = matrixe.get(0,0)
    plot(-log(scale)/log(2),log(math.sqrt(sol))/log(2),"xb")
    draw()
  else:
    Ta = solutions[0].getFunction()

print'done'
value = raw_input()
