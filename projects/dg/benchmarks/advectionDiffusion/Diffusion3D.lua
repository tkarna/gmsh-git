model = GModel()
-- model:load('tetrahedra.geo')
-- model:load('tetrahedra.msh')
-- model:load('prisms.geo')
-- model:load('prisms.msh')
model:load('mixed.geo')
model:load('mixed.msh')

-- initial condition
function initial_condition( xyz , f )
   for i=0,xyz:size1()-1 do
      x = xyz:get(i,0)
      y = xyz:get(i,1)
      z = xyz:get(i,2)
      f:set (i, 0, math.exp(-50*((x-0.4)^2+(y-0.3)^2)))
--       f:set (i, 0, math.exp(-50*((x-0.4)^2+(z+0.3)^2)))
   end
end

order = 0
groups = dgGroupCollection (model, 3, order)
groups:buildGroupsOfInterfaces()

-- conservation law

law = dgConservationLawAdvectionDiffusion:diffusionLaw(functionConstant({0.1}))
--law = dgConservationLawAdvectionDiffusion:advectionLaw(functionConstant({0.1, 0, 0}))

law:addBoundaryCondition('side1',law:new0FluxBoundary())
law:addBoundaryCondition('side2',law:new0FluxBoundary())
law:addBoundaryCondition('side3',law:new0FluxBoundary())
law:addBoundaryCondition('side4',law:new0FluxBoundary())
law:addBoundaryCondition('top',law:newSymmetryBoundary(0))
law:addBoundaryCondition('bottom',law:newSymmetryBoundary(0))


solution = dgDofContainer (groups, 1)
f = functionLua (1,'initial_condition',{functionCoordinates:get()})
solution:L2Projection(f)

-- main loop
rk = dgRungeKutta()
CFL = 400
dt = CFL*rk:computeInvSpectralRadius(law, solution)
solution:exportMsh(string.format('output/diff3d-%06d',0), 0, 0)
export_count = 0
for i=1,1000 do
  t = i*dt
  norm = rk:iterate44(law, t, dt, solution)
  if (i%100 == 0) then
    print("i", i, "norm", norm, "t", t, "dt", dt) 
    solution:exportMsh(string.format('output/diff3d-%06d',i), t, i)
  end
end
