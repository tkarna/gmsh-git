from dgpy import *
import os
import time
import math

TIME = function.getTime()
try : os.mkdir("output");
except: 0;
os.system("rm output/*")

#-------------------------------------------------
#-- Case specific functions
#-------------------------------------------------
def initF(FCT, XYZ):
  for i in range(0,FCT.size1()):
    x = XYZ.get(i,0)
    y = XYZ.get(i,1)
    temp = 0
    FCT.set(i,0, temp)

def source(FCT, XYZ):
  for i in range(0,FCT.size1()):
    x = XYZ.get(i,0)
    y = XYZ.get(i,1)
    s = 8.0*math.pi*math.pi*math.sin(2*math.pi*x)*math.sin(2*math.pi*y)
    FCT.set(i,0, s)

def exactSL(FCT, XYZ):
  for i in range(0,FCT.size1()):
    x = XYZ.get(i,0)
    y = XYZ.get(i,1)
    sol = math.sin(2*math.pi*x)*math.sin(2*math.pi*y)
    FCT.set(i,0, sol)

def error(FCT, F1, F2):
  for i in range(0,F1.size1()):
    FCT.set(i, 0, (F1(i,0)-F2(i,0))**2);

#-------------------------------------------------
#-- Initialize level set problem
#-------------------------------------------------

print('---- Load Mesh & Geometry')
CG = False
SOURCE = True
Tin = 10.0
Tout = 1.0
order = 2
dimension = 2

meshName = "squareHeat"
g2 = GModel()
g2.load(meshName + ".geo")
g2.mesh(2)
g2.save(meshName + ".msh")

model = GModel()
model.load(meshName+".msh")

print('---- Build Groups')
groups = dgGroupCollection(model, dimension, order)
XYZ = groups.getFunctionCoordinates()

print('---- Load Conservation law')
kappa = functionConstant(1)
mySource = functionPython(1, source, [XYZ])
if (CG):
  law = dgConservationLawAdvectionDiffusionSUPG.diffusionLaw(kappa) #CG
else:
  law = dgConservationLawAdvectionDiffusion.diffusionLaw(kappa) #DG
if (SOURCE):
  law.setSource(mySource)

print('---- Initial condition')
INIT_TEMP = functionPython(1, initF, [XYZ])
solution = dgDofContainer(groups,law.getNbFields())
solution.interpolate(INIT_TEMP) 
solution.exportMsh('output/initT', 0., 0)

print('---- Load Boundary conditions')
out1 = functionConstant([10.])
out2 = functionConstant([1.])
out = functionConstant([0.])

if (SOURCE):
  law.addBoundaryCondition('left', law.newOutsideValueBoundary("",out))
  law.addBoundaryCondition('right', law.newOutsideValueBoundary("",out))
  law.addBoundaryCondition('top', law.newOutsideValueBoundary("",out))
  law.addBoundaryCondition('bottom', law.newOutsideValueBoundary("",out))
  if (CG):
    law.addStrongBoundaryCondition(1, 'left', out)
    law.addStrongBoundaryCondition(1, 'right', out)
    law.addStrongBoundaryCondition(1, 'top', out)
    law.addStrongBoundaryCondition(1, 'bottom', out)
else: 
  law.addBoundaryCondition('left', law.newOutsideValueBoundary("",out1))
  law.addBoundaryCondition('right', law.newOutsideValueBoundary("",out2))
  law.addBoundaryCondition('top'   , law.new0FluxBoundary())
  law.addBoundaryCondition('bottom' , law.new0FluxBoundary())
  if (CG):
    law.addStrongBoundaryCondition(1, 'left', out1)
    law.addStrongBoundaryCondition(1, 'right', out2)
  
#-------------------------------------------------
#-- solver setup
#-------------------------------------------------

print('---- Solver Definition')
dof = None
if (CG):
  petsc =  linearSystemPETScDouble()
  petsc.setParameter("petscOptions", "-pc_type lu")
  petsc.setParameter("matrix_reuse", "same_matrix")
  dof = dgDofManager.newCG(groups, 1, petsc)
else:
  petsc = linearSystemPETScDouble()
  petsc.setParameter("petscOption", "-pc_type lu -ksp_monitor")
  dof = dgDofManager.newDG(groups, 1, petsc)

#-------------------------------------------------
#-- steady solve
#-------------------------------------------------
print('---- Solve')
maxIter = 100
steady = dgSteady(law, dof)
steady.getNewton().setVerb(12)
steady.getNewton().setMaxIt(maxIter)
steady.solve(solution)
solution.exportMsh("output/steadySol", 0, 0)

X = 0.25
Y = 0.25

Tana = math.sin(2*math.pi*X)*math.sin(2*math.pi*Y)
eval0 = dgFunctionEvaluator(groups, solution.getFunction())
result0 = fullMatrixDouble(1,1)
eval0.compute(X,Y,0,result0)
Tnum = result0.get(0,0)

print('---- integrating ...')
fana = functionPython(1, exactSL, [XYZ])
ana = dgDofContainer(groups, 1)
ana.interpolate(fana)
ana.exportMsh("output/exact_solution", 0, 0)
ANA = ana.getFunction()
NUM = solution.getFunction()
ferror = functionPython(1, error, [ANA, NUM]);
integrator = dgFunctionIntegrator(groups, ferror)
matrix = fullMatrixDouble(1,1)
integrator.compute(matrix, "")
result = matrix.get(0,0)

ERROR = math.sqrt(result)

print('     exporting error ...')
error = dgDofContainer(groups, 1)
error.interpolate(ferror)
error.exportMsh("output/error", 0, 0)

print('volume error:', ERROR)
print('Tana & Tnum:', Tana, Tnum)
