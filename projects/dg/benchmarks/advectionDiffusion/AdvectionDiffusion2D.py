from dgpy import *
#from gmshpy import *
import os
import time
import math
from scipy import *			#ADDED FOR MG
from scipy.sparse import *		#ADDED FOR MG
from scipy.sparse.linalg import *	#ADDED FOR MG
from matplotlib.pyplot import *		#ADDED FOR MG
from numpy.linalg import *		#ADDED FOR MG


# Load model & geometry

model = GModel()
model.load('squareEasy.msh')
order = 1
groups = dgGroupCollection(model, 2, order)

#groups.buildGroupsOfInterfaces()


# Conservation law & boundary conditions

coefv = functionConstant([0.06,0.,0])
coefnu = functionConstant([0.0000])
law = dgConservationLawAdvectionDiffusion(coefv,coefnu)


# Initial solution

def initialSolution(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    v = math.exp(-300*((x-0.2)*(x-0.2) + (y-0.1)*(y-0.1)))
    if x > .4 :
      v = 1.0
    elif x < .2 :
      v = -1.0
    else :
      v = 1.
    val.set(i,0,v)

xyz = groups.getFunctionCoordinates()
initialSolutionPython = functionPython(1, initialSolution, [xyz])

outsideValue = functionPython(1, initialSolution, [xyz])
#functionConstant([0,0,0])
law.addBoundaryCondition('bottom',law.newOutsideValueBoundary("bottom",outsideValue))
law.addBoundaryCondition('left'  ,law.newOutsideValueBoundary("bottom",outsideValue))
law.addBoundaryCondition('top'   ,law.newSymmetryBoundary("top"))
law.addBoundaryCondition('right' ,law.newSymmetryBoundary("top"))



solution = dgDofContainer(groups,law.getNbFields())
solution.L2Projection(initialSolutionPython)
solution.exportMsh('output/AdvectionDiffusion2D-00000', 0., 0)

def L1N(L1,Grad) :
    for i in range(Grad.size1()) :
      dx = Grad.get(i,0)
      dy = Grad.get(i,1)
#      L1.set(i,0,math.sqrt((dx*dx)+(dy*dy)))
      L1.set(i,0,math.sqrt((dx*dx)))
      #L2.set(i,0,(s-a))

def JUMP1(L1,sol1,sol2,n) :
#    print sol1.size1(), sol2.size1() 
#    print sol2.size1(), sol2.size2() 
    for i in range(sol1.size1()) :
      l = sol1.get(i,0)
      r = sol2.get(i,0)
      nx = n.get(i,0)
      ny = n.get(i,1)
#      print l, r
      L1.set(i,0,math.fabs((l-r)*nx))

S = solution.getFunction()
G = solution.getFunctionGradient()
NORMALS = S.getNormals()
L1  = functionPython(1,L1N,[G])
L1S = functionPython(1,JUMP1, [(S, 0), (S, 1), (NORMALS, 0)])
L1inte  = dgFunctionIntegrator(groups,L1,solution)  
L1inteS = dgFunctionIntegratorInterface(groups,L1S,solution)  

# Solve

print'*** Solve ***'

implicit = False 


if (implicit) :
  
  sys = linearSystemPETScBlockDouble()
  sys.setParameter("petscOptions", "-pc_type lu")
  dof = dgDofManager.newDGBlock(groups, 1, sys)
  solver = dgDIRK(law, dof, 2)
  solver.getNewton().setVerb(10)
  dt = 1
  for i in range(1, 10):
    t = i*dt
    solver.iterate(solution, dt , t)
    solution.exportMsh("output/AdvectionDiffusion2D-%05i" % (i), t, i)
    print('|ITER|', i, '|DT|', dt, '|T|', t)
  
else:
#  solver = dgRungeKuttaMultirateConservative.new44(groups,claw)
  limiter = dgSlopeLimiter(law)
#  solver = dgERK(law, None, DG_ERK_EULER)
  solver = dgERK(law, None, DG_ERK_44)
#  solver.setLimiter(limiter)
  dt = 1e-2
  figure()
  xlabel('Time(sec)')
  ylabel('Total Variation')
  title('Total Variation vs Time')
  grid(True)
  show(False)
  times = []
  tv = []
  tvb = []
  tvv = []
  for i in range(0, 70):
    t = i*dt
    if ((i-1) % 1 == 0):
      volTerm=fullMatrixDouble(1,1)
      surfTerm=fullMatrixDouble(1,1)
      L1inte.compute(volTerm,"")
      L1inteS.compute("",surfTerm)
      sol = volTerm.get(0,0) + surfTerm.get(0,0)
      print i , volTerm.get(0,0) , surfTerm.get(0,0)
      tv.append(sol)
      tvb.append( volTerm.get(0,0))
      tvv.append( surfTerm.get(0,0))
      times.append(t)
      plot(times,tv,'ro',times,tv,'k',times,tvb,'ko',times,tvv,'k',)
      draw()
      solution.exportMsh("output/AdvectionDiffusion2D-%05i" % (i), t, i/100)
      print('|ITER|', i, '|NORM|', norm, '|DT|', dt, '|T|', t)
    norm = solver.iterate(solution,dt,t)
#    print 'TV (',i,')=', sol

#plot(times,tv,"xb")
show()

Msg.Exit(0)

