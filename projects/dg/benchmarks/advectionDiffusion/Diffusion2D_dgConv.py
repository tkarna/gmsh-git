# Import for python
from gmshpy import *
from dgpy import *
from scipy import *			#ADDED FOR MG
from scipy.sparse import *		#ADDED FOR MG
from scipy.sparse.linalg import *	#ADDED FOR MG
from matplotlib.pyplot import *		#ADDED FOR MG
from numpy.linalg import *		#ADDED FOR MG

import time, math, os			#0S ADDED FOR CMD

N = 12

# Conservation law
v = functionConstant([0,0,0])		#CAN I MAKE IT A SCALAR ?
nu = functionConstant([10])
law = dgConservationLawAdvectionDiffusion(v,nu)
f = functionConstant(1)
law.setSource(f)

# Boundary condition
v_bc = functionConstant(0)		# CALLING THIS V MAKE A SEG FAULT ... WHY ??? :(
law.addBoundaryCondition('Homogeneous',law.newOutsideValueBoundary("Homogeneous",v_bc))
nbFields = law.getNbFields()


# build the k meshes&solutions
models = []
gcs = []
solutions = []
systems = []
dofs = []
solvers = []
projectors = []
factor = 1.2
f0 = 0.1
scale = f0/factor**(N+5)
order=1
dimension =2

figure()
show()
figure()
cc = 'brgmcyk'

def L2N(L2,TD,TA) :
    for i in range(TD.size1()) :
      s = TD.get(i,0)
      a = TA.get(i,0)
      L2.set(i,0,(s-a)*(s-a))
      #L2.set(i,0,(s-a))

for k in range(0,N):
  models.append(GModel())
  cmd = "gmsh -%d squareMG.geo -clscale %f" % (dimension,scale)
  t = time.clock()
  os.system(cmd)
  if k == 0:
    scale = f0
    val = raw_input()
  scale = scale/factor
  print "Time for meshing : %f" % (t-time.clock())
  t = time.clock()
  models[k].load('squareMG.msh')
  print "Time for loading the mesh: %f" % (t-time.clock())
  t = time.clock()
  gcs.append(dgGroupCollection(models[k], dimension, order))
  print "Time for loading the gc: %f" % (t-time.clock())
  t = time.clock()
  gcs[k].buildGroupsOfInterfaces()
  print "Time for building the interfaces: %f" % (t-time.clock())
  solutions.append(dgDofContainer(gcs[k], nbFields))
  if k == 0:
    solution2 = dgDofContainer(gcs[k], nbFields)
  systems.append(linearSystemPETScDouble ())
  systems[k].setParameter("petscOptions","-pc_type lu")# -ksp_monitor_true_residual -mat_view_matlab -vec_view_matlab" )
  dofs.append(dgDofManager.newDG (gcs[k], nbFields, systems[k]))
  #solvers.append(dgSteady(law,dofs[k]))
  #solvers[k].solve(solutions[k])
  solvers.append(dgNewton(dofs[k], law))
  #solvers[k].setMaxIt(1)
  #solvers[k].setVerb(10)
  solutions[k].copy(solvers[k].solve(None,0.0,None,0.0))
  t = time.clock()
  if (k > 0):
    projectors.append(dgMesh2MeshProjection(solutions[k],solution2))
    projectors[k-1].projectFromTo(solutions[k],solution2)
    tracNorm = dgDofContainer(gcs[0],1)
    Td = solution2.getFunction()
    L2 = functionPython(1,L2N,[Td,Ta])
    tracNorm.interpolate(L2)
    tracNorm.exportMsh('L2', 0, 0)
    matrix=fullMatrixDouble(1,1)
    L2inte = dgFunctionIntegrator(gcs[0],L2,solution2)
    L2inte.compute(matrix,"")
    sol = matrix.get(0,0)
    plot(-log(scale)/log(2),log(math.sqrt(sol))/log(2),"xb")
    draw()
  else:
    Ta = solutions[0].getFunction()

  #if k == 0:
  #  xx = linspace(-1,1,n[0]/2+1)
  #  xx = [xx[0:len(xx)-1],xx[1:len(xx)]]
  #  xx = reshape(xx,n[0],1);
  #  plot(xx,x,"%s" % (cc[k%7]))
  #else:
  #  plot(xx,P[k-1]*x,"%s" % (cc[k%7]))
  #draw()
#  s,w = eig(solve(diag(diag(A[i].todense())),A[i].todense()))
#  plot(real(s),imag(s),"%sx" % (cc[i-1]))
print'done'
value = raw_input()
