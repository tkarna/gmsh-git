from dgpy import *
from Common import *
import os
import time
import math

TIME = function.getTime()
try : os.mkdir("output");
except: 0;
os.system("rm output/*")

#-------------------------------------------------
#-- Case specific functions
#-------------------------------------------------
def initF(FCT, XYZ):
	for i in range(0,FCT.size1()):
          	x = XYZ.get(i,0)
		y = XYZ.get(i,1)
		z = XYZ.get(i,2)
		temp = 0
		#temp = sin(2*pi*x)*sin(2*pi*y)*sin(2*pi*z)
		FCT.set(i,0, temp)

def source(FCT, XYZ):
	for i in range(0,FCT.size1()):
		x = XYZ.get(i,0)
		y = XYZ.get(i,1)
		z = XYZ.get(i,2)
		s = 12*pi*pi*sin(2*pi*x)*sin(2*pi*y)*sin(2*pi*z)
		FCT.set(i,0, s)

#-------------------------------------------------
#-- Initialize level set problem
#-------------------------------------------------

print'---- Load Mesh & Geometry'
CG = False
SOURCE = True
Tin = 10.0
Tout = 1.0
order = 1
dimension = 3

meshName = "cubeHeat"
genMesh(meshName, dimension, order)

model = GModel()
model.load(meshName+msh)

print'---- Build Groups'
groups = dgGroupCollection(model, dimension, order)
XYZ = groups.getFunctionCoordinates()

print'---- Load Conservation law'
kappa = functionConstant(1)
mySource = functionPython(1, source, [XYZ])
if (CG):
	law = dgConservationLawAdvectionDiffusionSUPG.diffusionLaw(kappa) #CG
else:
	law = dgConservationLawAdvectionDiffusion.diffusionLaw(kappa) #DG
if (SOURCE):
	law.setSource(mySource)

print'---- Initial condition'
INIT_TEMP = functionPython(1, initF, [XYZ])
solution = dgDofContainer(groups,law.getNbFields())
solution.interpolate(INIT_TEMP) 
solution.exportMsh('output/initT', 0., 0)

print'---- Load Boundary conditions'
out1 = functionConstant([10.])
out2 = functionConstant([1.])
out = functionConstant([0.])

if (SOURCE):
	law.addBoundaryCondition('plane1', law.newOutsideValueBoundary("",out))
	law.addBoundaryCondition('plane2', law.newOutsideValueBoundary("",out))
	law.addBoundaryCondition('plane3', law.newOutsideValueBoundary("",out))
	law.addBoundaryCondition('plane4', law.newOutsideValueBoundary("",out))
	law.addBoundaryCondition('plane5', law.newOutsideValueBoundary("",out))
	law.addBoundaryCondition('plane6', law.newOutsideValueBoundary("",out))
	if (CG):
		law.addStrongBoundaryCondition(2, 'plane1', out)
		law.addStrongBoundaryCondition(2, 'plane2', out)
		law.addStrongBoundaryCondition(2, 'plane3', out)
		law.addStrongBoundaryCondition(2, 'plane4', out)
		law.addStrongBoundaryCondition(2, 'plane5', out)
		law.addStrongBoundaryCondition(2, 'plane6', out)
else: 
	law.addBoundaryCondition('plane1', law.new0FluxBoundary())
	law.addBoundaryCondition('plane2', law.new0FluxBoundary())
	law.addBoundaryCondition('plane3', law.new0FluxBoundary())
	law.addBoundaryCondition('plane4', law.new0FluxBoundary())
	law.addBoundaryCondition('plane5', law.newOutsideValueBoundary("",out2))
	law.addBoundaryCondition('plane6', law.newOutsideValueBoundary("",out1))
	if (CG):
		law.addStrongBoundaryCondition(2, 'plane5', out2)
		law.addStrongBoundaryCondition(2, 'plane6', out1)
	
#-------------------------------------------------
#-- solver setup
#-------------------------------------------------

print'---- Solver Definition'
dof = None
if (CG):
	petsc =  linearSystemPETScDouble()
	petsc.setParameter("petscOptions", "-pc_type lu")
	petsc.setParameter("matrix_reuse", "same_matrix")
	dof = dgDofManager.newCG(groups, 1, petsc)
else:
	petsc = linearSystemPETScDouble()
	petsc.setParameter("petscOption", "-pc_type lu -ksp_monitor")
	dof = dgDofManager.newDG(groups, 1, petsc)
	#petsc = linearSystemPETScBlockDouble()
	#petsc.setParameter("petscOption", "-pc_type lu -ksp_monitor")
	#dof = dgDofManager.newDGBlock(groups, 1, petsc)
#-------------------------------------------------
#-- steady solve
#-------------------------------------------------
print'---- Solve'	
maxIter = 100
steady = dgSteady(law, dof)
steady.getNewton().setVerb(10)
#steady.getNewton().setRtol(1e-12)
#steady.getNewton().setMaxIt(maxIter)
steady.solve(solution)
solution.exportMsh("output/steadySol", 0, 0)
#-------------------------------------------------
#-- post-processing
#-------------------------------------------------
X = 0.2
Y = 0.2
Z = 0.2
eval1 = dgFunctionEvaluator(groups, solution.getFunction())
result = fullMatrixDouble(1,1)
eval1.compute(X,Y,Z,result)
TNUM = result.get(0,0)
TANA = sin(2*pi*X)*sin(2*pi*Y)*sin(2*pi*Z)
print 'Tnum = ', TNUM
print 'Tana = ', TANA
