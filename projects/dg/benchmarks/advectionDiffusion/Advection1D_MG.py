# Import for python
from gmshpy import *
from dgpy import *
from scipy import *			#ADDED FOR MG
from scipy.sparse import *		#ADDED FOR MG
from scipy.sparse.linalg import *	#ADDED FOR MG
from matplotlib.pyplot import *		#ADDED FOR MG
from numpy.linalg import *		#ADDED FOR MG

import time, math, os			#0S ADDED FOR CMD

N = 17

# Conservation law
v = functionConstant([0,0,0])		#CAN I MAKE IT A SCALAR ?
nu = functionConstant([10])
#law = dgConservationLawAdvectionDiffusion.advectionLaw(v) 
law = dgConservationLawAdvectionDiffusion(v,nu) 

# Boundary condition
v_left = functionConstant(0)
law.addBoundaryCondition('Left',law.newOutsideValueBoundary("Left",v_left))
v_right = functionConstant(1)
law.addBoundaryCondition('Right'  ,law.newOutsideValueBoundary("Right",v_right))
nbFields = law.getNbFields()
rk=dgRungeKutta() 


# build the k meshes&solutions
models = []
gcs = []
solutions = []
systems = []
dofs = []
solvers = []
A = []
factor = 1
order=1

for k in range(0,N):
  models.append(GModel())
  cmd = "gmsh -1 edgeMG.geo -string \"nbElem = %d;\"" % (factor+1)
  t = time.clock()
  os.system(cmd)
  print "Time for meshing : %f" % (t-time.clock())
  t = time.clock()
  models[k].load('edgeMG.msh')
  print "Time for loading the mesh: %f" % (t-time.clock())
  factor = factor*2
  t = time.clock()
  gcs.append(dgGroupCollection(models[k], 1, order))
  print "Time for loading the gc: %f" % (t-time.clock())
  t = time.clock()
  gcs[k].buildGroupsOfInterfaces()
  print "Time for building the interfaces: %f" % (t-time.clock())
  solutions.append(dgDofContainer(gcs[k], nbFields))
  systems.append(linearSystemPETScDouble ())
  systems[k].setParameter("petscOptions","-pc_type lu")# -ksp_monitor_true_residual -mat_view_matlab -vec_view_matlab" )
  dofs.append(dgDofManager.newDG (gcs[k], nbFields, systems[k]))
  solvers.append(dgNewton(dofs[k], law))
  t = time.clock()
  solvers[k].assembleSystem(0.0,None,0.0)
  print "Time to assemble the system: %f" % (t-time.clock())
  rows = systems[k].getRowPointers();
  n = len(rows)-1;
  t = time.clock()
  A.append(csr_matrix( (array(systems[k].getData()),array(systems[k].getColumnsIndices()),array(rows)), shape=(n,n) ))
  print "Time to assemble the system: %f" % (t-time.clock())

"""
# Initial condition
def initial_condition(val, xyz ):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    val.set (i, 0, math.exp(-10*((x-0.2)**2 +(y-0.3)**2)))
xyz = getFunctionCoordinates()
FS = functionPython(1, initial_condition, [xyz])
solution.L2Projection(FS)
solution.exportMsh("output/AdvectionImp-00000", 0, 0)
"""

def multigrid(A, b, k, cc='b', nit=50, kc=0, drawFig=1):
  t = 0
  if (k > kc):
    n = A[k].get_shape()
    D = A[k].diagonal()
    L = tril(A[k])
    U = A[k]-L
    P = kron(identity(n[0]/4),[[1,0],[0.5,0.5],[0.5,0.5],[0,1]])
    x = zeros(n[0])
    for i in range(0,nit):
#      x = x + 0.5*(b-A[k]*x)/D
#      x = spsolve(L,b-U*x)
      r = P.transpose()*(b-A[k]*x)
      x = x + P*multigrid(A,r,k-1,cc,1,kc,0)
      for j in range(0,1):
#        x = x + 0.5*(b-A[k]*x)/D
         x = spsolve(L,b-U*x)
      if drawFig: 
        semilogy(i+1,norm(b-A[k]*x)/norm(b),"%sx" % (cc))
        if (time.clock()-t > 0.5):
          draw()
          t = time.clock()
  else:
    x = spsolve(A[k],b)
  if drawFig:
    draw()
  return x
figure()
show()
figure()
cc = 'brgmcyk'
for i in range(1,N):
  n = A[i].get_shape()
  multigrid(A,ones(n[0]),i,cc[(i-1)%7]) #
#  s,w = eig(solve(diag(diag(A[i].todense())),A[i].todense()))
#  plot(real(s),imag(s),"%sx" % (cc[i-1]))
title('Pure diffusion. G-S post smoothing from 2^1 to 2^19 elements.')
xlabel('Number of multigrid iterations')
ylabel('Relative norm of the residual')
print'done'
value = raw_input()
