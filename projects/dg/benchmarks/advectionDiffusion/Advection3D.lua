
model = GModel()
model:load('box.geo')
model:load('box.msh')

-- initial condition
function initial_condition(xyz ,f)
   for i=0,xyz:size1()-1 do
      x = xyz:get(i,0)
      y = xyz:get(i,1)
      z = xyz:get(i,2)
      --f:set (i, 0, math.exp(-100*((x-0.5)^2)))
      --f:set (i, 0, x )
      f:set (i, 0, math.sqrt((x-0.3)*(x-0.3)+(y-0.3)*(y-0.3)+z*z)-0.2)	
      --f:set(i, 0, math.exp((x-.3)*(x-.3)+(y-.3)*(y-.3)+z*z)-.2))
   end
end

-- conservation law
-- advection speed
law = dgConservationLawAdvectionDiffusion.advectionLaw(functionConstant({0,0,0.15}))

-- boundary condition
--freestreem=law:newOutsideValueBoundary(functionConstant({0,0,0.15}))

xyz = functionCoordinates.get()
freestream=law:newOutsideValueBoundary('inlet',functionLua(1,'initial_condition',{functionCoordinates:get()}))

law:addBoundaryCondition('wall',law:new0FluxBoundary())
law:addBoundaryCondition('inlet',freestream)
law:addBoundaryCondition('outlet',law:newSymmetryBoundary())
law:addBoundaryCondition('symmetry',law:newSymmetryBoundary())

groups = dgGroupCollection(model,3,1)
groups:buildGroupsOfInterfaces()

solution = dgDofContainer(groups,1)
solution:L2Projection(functionLua(1,'initial_condition',{xyz}))
solution:exportMsh('output/Adv3D-00000', 0, 0)
rk = dgRungeKutta()

print'***exporting init solution ***'

-- main loop
n = 10
dt = 0.01
t = 0
for i=1,100*n do
  norm = rk:iterate44(law, t, dt, solution)
  t = t + dt
  if (i % n == 0) then 
     print('|ITER|',i,'|NORM|',norm,'|T|',t,'|CPU|',os.clock()-x)
     solution:exportMsh(string.format("output/Adv3D-%05d", i), t, i/n) 
  end
end
