#!/usr/bin/env rundgpy
from dgpy import *
import gmshPartition
model = GModel  ()
model.load (gmshPartition.simple('stommel_square.msh'))
groups = dgGroupCollection(model, 2, 1)


CCode = """
#include "fullMatrix.h"
#include "function.h"
extern "C" {
void wind (dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (int i = 0; i< sol.size1(); i++) {
    sol.set(i,0,3* sin(xyz(i,1)/1e6)/1e6);
    sol.set(i,1,0);
  }
}
void coriolis (dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (int i = 0; i< sol.size1(); i++) {
    sol.set(i,0,1e-4+2e-11*xyz(i,1));
  }
}
void current (dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &sol) {
  for (int i = 0; i< val.size1(); i++) {
    val(i, 0) = sol(i,1);
    val(i, 1) = sol(i,2);
  }
}
void init (dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &xyz) {
  for (int i = 0; i< val.size1(); i++) {
    val(i, 0) = xyz(i,1)* xyz(i,1) * 1e-16;
    val(i, 1) = xyz(i,0)* xyz(i, 0) * 1e-16;
    val(i, 2) = xyz(i,1) * xyz(i,0) * 1e-16;
  }
}
}
"""

tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CCode, tmpLib);
Msg.Barrier()


claw = dgConservationLawShallowWater2d()
solution = dgDofContainer(groups, claw.getNbFields())
XYZ = groups.getFunctionCoordinates();
coriolis = functionC(tmpLib,"coriolis",1,[XYZ])
claw.setCoriolisFactor(coriolis)
claw.setIsLinear(False)
gamma = functionConstant([5e-7])
claw.setLinearDissipation(gamma)
wind = functionC(tmpLib,"wind",2,[XYZ])
init = functionC(tmpLib,"init",3,[XYZ])
solution.L2Projection(init)
claw.setSource(wind)
bath = functionConstant([1000])
#gradBath = functionConstant([0, 0, 0])
claw.setBathymetry(bath)
#claw.setBathymetryGradient(gradBath)
claw.addBoundaryCondition('Wall',claw.newBoundaryWall())

residual = dgDofContainer(groups, claw.getNbFields())
sys = linearSystemPETScBlockDouble ()
if(Msg.GetCommSize() > 1):
  sys.setParameter("petscOptions", "-pc_type bjacobi -sub_pc_type ilu")
else:
  sys.setParameter("petscOptions", "-pc_type lu")
dof = dgDofManager.newDGBlock (groups, 3, sys)
implicitEuler = dgDIRK(claw, dof, 1)
implicitEuler.getNewton().setRtol(1e-6)
implicitEuler.getNewton().setVerb(10)
currentFunction = functionC(tmpLib,"current",2,[solution.getFunction()])

dt = 1e5
t = 0.0
for i in range (100) :
  t = t+dt
  print('|ITER|',i,'|DT|',dt, '|t|', t)
  implicitEuler.iterate(solution, dt, t)
  solution.exportMsh("output/stommel-%06d" % (i), i*dt, i)
  solution.exportFunctionMsh(currentFunction, 'output/current-%06d' % i, i*dt, i, 'f')
