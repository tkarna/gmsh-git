lc = .99*2;
Point(1) = {-1.0, -1.0, 0, lc};
Point(2) = {1, -1.0, 0, lc};
Point(3) = {1, 1, 0, lc};
Point(4) = {-1, 1, 0, lc};
Line(1) = {4, 3};
Line(2) = {3, 2};
Line(3) = {2, 1};
Line(4) = {1, 4};
Line Loop(5) = {2, 3, 4, 1};
Plane Surface(6) = {5};
Physical Line("Homogeneous") = {1, 2, 3, 4};
Physical Surface("Inside") = {6};		// OTHERWISE IT DOESN'T SAVE THE MESH :(
//Physical Point("Corner") = {1,2,3,4};
//Mesh.CharacteristicLengthExtendFromBoundary=1;
//Transfinite Line {1, 2, 4, 3} = nbElem;
//Transfinite Surface {6};
