-- initial condition
function initF ( xyz , f )
  for i=0,xyz:size1()-1 do
    x = xyz:get(i,0)
    y = xyz:get(i,1)
    z = xyz:get(i,2)
    f:set (i, 0, math.atan(150*(((x-.5)*(x-.5)+(y-.5)*(y-.5) - .09))))
  end
end

dimension = 2
order = 1

options = gmshOptions()
options:numberSet('Mesh', 0, 'Algorithm', 7) --bamg
options:numberSet('Mesh', 0, 'LcIntegrationPrecision', 1.e-3)

model = GModel()
model:load('square.geo')
model:mesh(2)
model:save('square.msh')
groups = dgGroupCollection(model, dimension, order)
xyz = functionCoordinates.get()
INIT = functionLua(3, 'initF', {xyz})
evaluatorOfSolution=dgFunctionEvaluator(groups, INIT)


for I=1,5 do
 print('---- Mesh Adaptation I=', I)
 local model = GModel()
 model:load('square.msh')
 groups = dgGroupCollection(model, dimension, order)
 solution = dgDofContainer(groups, 1);
 solution:L2Projection(INIT)
 BGM = dgMeshMetricBasedOnHessian(groups, evaluatorOfSolution, 3.e-2)

 modelAdapt = GModel()
 modelAdapt:load('square.geo')
 BGM:setAsBackgroundMesh(modelAdapt)
 modelAdapt:mesh(2)
 modelAdapt:save('square.msh')
end

model:load('square.msh')
groups = dgGroupCollection(model, dimension, order)
solution = dgDofContainer(groups, 1);
solution:L2Projection(INIT)
solution:exportMsh("adaptedSolution.msh",0,0) 



