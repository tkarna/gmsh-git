model = GModel  ()
model:load ('plaque_supra.geo')
model:load ('plaque_supra.msh')

order = 1
dimension=2

Ec=1e-7
Jc=100 
mu0=4*math.pi*1e-10
c=mu0*Jc/Ec

Bmax = 1e-6/Ec
t=0
dt=1e-6


function diffusivity(f )
  for i=0,f:size1()-1 do
    f:set (i, 0, 1/c )
  end
end

function valueRight(f)
  for i=0,f:size1()-1 do
     f:set(i,0,Bmax)
  end
end

function valueLeft(f)
  for i=0,f:size1()-1 do
    f:set(i,0,-Bmax)
  end
end



nu=fullMatrix(1,1);
nu:set(0,0,1/c)

-- conservation law
-- advection speed
law = dgConservationLawAdvectionDiffusion.diffusionLaw(functionLua(1,'diffusivity',{}))


law:addBoundaryCondition('Top',law:new0FluxBoundary())
law:addBoundaryCondition('Bottom',law:new0FluxBoundary())
leftFunction=functionLua(1,'valueLeft',{})
rightFunction=functionLua(1,'valueRight',{})


law:addBoundaryCondition('Right',law:newNeumannBoundary(rightFunction))
law:addBoundaryCondition('Left',law:newNeumannBoundary(leftFunction))

-- initial condition
function initial_condition( xyz , f )
  for i=0,xyz:size1()-1 do
    x = xyz:get(i,0)
    y = xyz:get(i,1)
    z = xyz:get(i,2)
    f:set (i, 0, 0*math.exp(-100*((x-0.2)^2 +(y-0.3)^2)))
  end
end

groups = dgGroupCollection (model, dimension, order)
groups:buildGroupsOfInterfaces()

solution = dgDofContainer(groups, 1)



solution:L2Projection(functionLua(1,'initial_condition',{functionCoordinates.get()}))

rk=dgRungeKutta()

limiter = dgSlopeLimiter(law)
rk:setLimiter(limiter)

TransformNodalValue = dgSupraTransformNodalValue(law)
rk:setTransformNodalValue(TransformNodalValue)

TransformNodalValue:apply(solution)
solution:exportMsh('output/DiffusionSupra-00000',0,0)
TransformNodalValue:apply_Inverse(solution)

CFL = 5;
dt = CFL * rk:computeInvSpectralRadius(law,solution);  
print('dt',dt)

--dt = 0.001

n=100
t=0
for i=1,20000 do
  norm =  rk:iterate44(law, t, dt, solution)
  t=t+dt
  dt = CFL * rk:computeInvSpectralRadius(law,solution);  
  if (i % n == 0) then 
    print('iter',i,norm)
    TransformNodalValue:apply(solution)
    solution:exportMsh(string.format("output/DiffusionSupra-%05d",i), t, i/n) 
    TransformNodalValue:apply_Inverse(solution)

  end
end
