from gmshpy import *
from dgpy import *
import os
import time
import math


model = GModel  ()
model.load ('cercle.msh')

order=1
dimension=2

groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()


Ec=1e-7
Jc=100
mu0=4*math.pi*1e-10
c=mu0*Jc/Ec
Bmax= 1e-6/(Ec)
t=0



# n-power law
def npower_law(val, xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    val.set (i,0,20.0)

def mod_law(val,xyz, sol1, sol2):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    a=sol1.get(i,0)
    b=sol2.get(i,0)
    h=math.sqrt(a*a+b*b)
    val.set(i,0,h)

# Set dB/dt of External magnetic field: has only one component along z-axis
Bx=0
By=0
dBzOndt=Bmax


def varOfMagFieldForFirstUnknown(val, xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    val.set (i,0,0)
    val.set (i,1,dBzOndt)
    val.set (i,2,0)

def varOfMagFieldForSecondUnknown(val, xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    val.set (i,0,-dBzOndt)
    val.set (i,1,0)
    val.set (i,2,0)

def ExplicitboundaryForFirstUnknown(val,xyz, power2):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    b1=power2.get(i,0)
    b2=power2.get(i,1)
    b3=power2.get(i,2)
    val.set(i,0,-b2)
    val.set(i,1,b1)
    val.set(i,2,0)
    

def ExplicitboundaryForSecondUnknown(val,xyz, power1):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    a1=power1.get(i,0)
    a2=power1.get(i,1)
    a3=power1.get(i,2)
    val.set(i,0,a2)
    val.set(i,1,-a1)
    val.set(i,2,0)
  


varOfmagneticFirstUnknown = functionPython(3,varOfMagFieldForFirstUnknown, [xyz])
varOfmagneticSecondUnknown= functionPython(3,varOfMagFieldForSecondUnknown, [xyz])

# Conservation law
nu = functionConstant([1/c])
                            
law1 = dgConservationLawAdvectionDiffusion.diffusionLaw(nu)
law2 = dgConservationLawAdvectionDiffusion.diffusionLaw(nu)


# build solution vector
FSnull = functionConstant([0.])
npower = functionPython(1, npower_law, [xyz])

solution1 = dgDofContainer(groups, law1.getNbFields())
powersolution1 = dgDofContainer(groups, law1.getNbFields())
solution1.L2Projection(FSnull)
powersolution1.L2Projection(FSnull)

solution2 = dgDofContainer(groups, law2.getNbFields())
powersolution2 = dgDofContainer(groups, law2.getNbFields())
solution2.L2Projection(FSnull)
powersolution2.L2Projection(FSnull)


nsupra=dgDofContainer(groups, law1.getNbFields())

M=dgDofContainer(groups, law1.getNbFields())
M.L2Projection(FSnull)
M.exportMsh("output/normJ-00000",0,0)

nsupra.L2Projection(npower)

solution1.exportMsh("output/J1-00000",0,0)
solution2.exportMsh("output/J2-00000",0,0)


bForFirst= functionPython(3,ExplicitboundaryForFirstUnknown, [xyz, powersolution2.getFunctionGradient()])
bForSecond= functionPython(3,ExplicitboundaryForSecondUnknown, [xyz, powersolution1.getFunctionGradient()])

law1.addBoundaryCondition('border', law1.newBoundaryForSupraLaw(bForFirst,varOfmagneticFirstUnknown,1/c,1/c,0))
law2.addBoundaryCondition('border', law2.newBoundaryForSupraLaw(bForSecond,varOfmagneticSecondUnknown,1/c,1/c,0))



sys = linearSystemPETScDouble ()
sys.setParameter("petscOptions", "-ksp_type bicg")
  
dof1 = dgDofManager.newCG (groups, law1.getNbFields(), sys)
dof2 = dgDofManager.newCG (groups, law2.getNbFields(), sys)
# build solution vector
solve=dgSolveSupraBetaPowerLaw (law1, law2, dof1, dof2)
solve.getNewtonX().setVerb(10)
solve.getNewtonY().setVerb(10)


dt = 0.0001

for i in range (0, 1000000):
  t = t + dt
  #print("Iter %i t=%.2f" % (i,t))
  solve.iterateBetaVectorial2DPowerLaw(solution1,solution2,powersolution1,powersolution2,nsupra,dt,t)
  mod= functionPython(1, mod_law, [xyz,solution1.getFunction(),solution2.getFunction()])
  M.L2Projection(mod)
  print'|ITER|', i, '|DT|', dt, '|t|', t
  #print("Iter %i t=%.2f " % (i,t))
  #print("Iter %i t=%.10f norm1=%.8f norm2=%.8f" % (i,t,solution1.norm(), solution2.norm()))
  if (i % 1== 0) :
    solution1.exportMsh('output/J1-%05i' % (i), t, i)
    solution2.exportMsh('output/J2-%05i' % (i), t, i)
    M.exportMsh('output/normJ-%05i' % (i), t, i)






