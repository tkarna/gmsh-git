from gmshpy import *
from dgpy import *
import os
import time
import math


model = GModel  ()
model.load ('plaque_supra.msh')

Ec=1e-7
Jc=100
mu0=4*math.pi*1e-10
c=mu0*Jc/Ec
Bmax= 1e-6/(Ec)

# Conservation law
nu = functionConstant([1/c])
law = dgConservationLawAdvectionDiffusion.diffusionLaw(nu) 

# Boundary condition
v_left = functionConstant([-Bmax/c])
v_right = functionConstant([Bmax/c])
law.addBoundaryCondition('Left',law.newNeumannBoundary(v_left))
law.addBoundaryCondition('Right'  ,law.newNeumannBoundary(v_right))

law.addBoundaryCondition('Top',law.new0FluxBoundary())
law.addBoundaryCondition('Bottom'  ,law.new0FluxBoundary())


# Initial condition
FSnull = functionConstant([0.]) 

# n-power law
def npower_law(val, xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    val.set (i,0,20.0)


order=1
dimension=2

groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag()

 

# build solution vector
xyz = groups.getFunctionCoordinates()


npower = functionPython(1, npower_law, [xyz])
solution = dgDofContainer(groups, law.getNbFields())
nsupra=dgDofContainer(groups, law.getNbFields())
powerofsolution=dgDofContainer(groups, law.getNbFields())
solution.L2Projection(FSnull)
powerofsolution.L2Projection(FSnull)
nsupra.L2Projection(npower)
solution.exportMsh("output/psupra-00000",0,0)
print'*** solve ***'
t=0
dt = 0.001


#sys = linearSystemPETScBlockDouble ()
sys = linearSystemPETScDouble ()
sys.setParameter("petscOptions", "-ksp_type bicg")

#dof = dgDofManager.newDGBlock (groups, law.getNbFields(), sys)
#dof = dgDofManager.newDG (groups, law.getNbFields(), sys)
dof = dgDofManager.newCG (groups, law.getNbFields(), sys)

solve=dgSolveSupraBetaPowerLaw(law,dof)
#solve.getNewton1().setMaxit(5000)
solve.getNewtonX().setAtol(1e-12)
solve.getNewtonX().setRtol(1e-10)
solve.getNewtonX().setVerb(10)

for i in range (0, 2001):
  t = t + dt
  solve.iterateBetaScalarPowerLaw(solution,powerofsolution,nsupra, dt, t)
  print("Iter %i t=%.2f norm=%.8f" % (i,t,solution.norm()))
  if (i % 1 == 0) :
     solution.exportMsh('output/J-%05i' % (i), t, i)
