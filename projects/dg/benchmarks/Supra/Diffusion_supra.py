from gmshpy import *
from dgpy import *
import os
import time
import math


model = GModel  ()
model.load ('plaque_supra.msh')

Ec=1e-7
Jc=100
mu0=4*math.pi*1e-10
c=mu0*Jc/Ec
Bmax= 4*1e-6/(5*Ec)

# Conservation law
nu = functionConstant([1/c])
law = dgConservationLawAdvectionDiffusion.diffusionLaw(nu) 

# Boundary condition
v_left = functionConstant([Bmax])
v_right = functionConstant([-Bmax])
law.addBoundaryCondition('Left',law.newNeumannBoundary(v_left))
law.addBoundaryCondition('Right'  ,law.newNeumannBoundary(v_right))

law.addBoundaryCondition('Top',law.new0FluxBoundary())
law.addBoundaryCondition('Bottom'  ,law.new0FluxBoundary())


# Initial condition
#def initial_condition(val, xyz):
#  for i in range(0,xyz.size1()):
#    x = xyz.get(i,0)
#    y = xyz.get(i,1)
#    z = xyz.get(i,2)
#    val.set (i,0,0*math.exp(-100*((x-0.2)^2 +(y-0.3)^2)))

# n-power law
def npower_law(val, xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    val.set (i,0,20.0)


order=1
dimension=2

groups = dgGroupCollection(model, dimension, order)
groups.buildGroupsOfInterfaces()
 

# build solution vector
xyz = getFunctionCoordinates()
#FS = functionPython(1, initial_condition, [xyz])

FSnull = functionConstant([0])
npower = functionPython(1, npower_law, [xyz])
solution = dgDofContainer(groups, law.getNbFields())
nsupra=dgDofContainer(groups, law.getNbFields())
powerofsolution=dgDofContainer(groups, law.getNbFields())
solution.L2Projection(FSnull)
powerofsolution.L2Projection(FSnull)
nsupra.L2Projection(npower)
solution.exportMsh("output/supra-00000",0,0)

print'*** solve ***'
t=0
dt = 0.001

sys = linearSystemFullDouble ()
#sys = linearSystemPETScBlockDouble ()


#sys = dgLinearSystemMatrixFreeBJacobi (law, groups)
#dof = dgDofManager.newDGBlock (groups, law.getNbFields(), sys)
#dof = dgDofManager.newDG (groups, law.getNbFields(), sys)
dof = dgDofManager.newCG (groups, law.getNbFields(), sys)

solve=dgSolveSupra(law,dof)
solve.getNewton().setAtol(1e-12)
solve.getNewton().setRtol(1e-10)
solve.getNewton().setVerb(10)

for i in range (0, 10000):
  t = t + dt
  print("Iter %i t=%.2f" % (i,t))
  solve.iteratePowerLaw(solution,powerofsolution,nsupra, dt, t)
  if (i % 10 == 0) :
    solution.exportMsh('output/supra-%05i' % (i), t, i)

