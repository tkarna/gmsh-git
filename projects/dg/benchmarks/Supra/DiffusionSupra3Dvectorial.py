from gmshpy import *
from dgpy import *
import os
import time
import math


model = GModel  ()
model.load ('cylinder.msh')


order=1
dimension=3


groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()

Ec=1e-7
Jc=50
mu0=4*math.pi*1e-10
c=mu0*Jc/Ec
Bmax= 1e-6/(Ec)
t=0
dt =5e-5
freq=50
# Initial condition
FSnull = functionConstant([0.])

# n-power law
def npower_law(val, xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    val.set (i,0,20.0)

def mod_law(val,xyz, sol1, sol2, sol3):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    a=sol1.get(i,0)
    b=sol2.get(i,0)
    c=sol3.get(i,0)
    h=math.sqrt(a*a+b*b+c*c)
    val.set(i,0,h)


# set variation of magnetic field dB/dt = (B1, B2, B3)
dB1Ondt=Bmax
dB2Ondt=0
dB3Ondt=0

def varOfMagFieldForFirstUnknown(val, xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    val.set (i,0,0)
    val.set (i,1,dB3Ondt)
    val.set (i,2,-dB2Ondt)

def varOfMagFieldForSecondUnknown(val, xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    val.set (i,0,-dB3Ondt)
    val.set (i,1,0)
    val.set (i,2,-dB1Ondt)

def varOfMagFieldForThirdUnknown(val, xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    val.set (i,0,dB2Ondt)
    val.set (i,1,-dB1Ondt)
    val.set (i,2,0)


varOfmagneticFirstUnknown = functionPython(3,varOfMagFieldForFirstUnknown, [xyz])
varOfmagneticSecondUnknown= functionPython(3,varOfMagFieldForSecondUnknown, [xyz])
varOfmagneticThirdUnknown= functionPython(3,varOfMagFieldForThirdUnknown, [xyz])


def ExplicitboundaryForFirstUnknown(val,xyz, power2,power3):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    a1=power2.get(i,0)
    a2=power2.get(i,1)
    a3=power2.get(i,2)
    b1=power3.get(i,0)
    b2=power3.get(i,1)
    b3=power3.get(i,2)
    val.set(i,0,-a2-b3)
    val.set(i,1,a1)
    val.set(i,2,b1)

def ExplicitboundaryForSecondUnknown(val,xyz, power1,power3):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    a1=power1.get(i,0)
    a2=power1.get(i,1)
    a3=power1.get(i,2)
    b1=power3.get(i,0)
    b2=power3.get(i,1)
    b3=power3.get(i,2)
    val.set(i,0,a2)
    val.set(i,1,-a1-b3)
    val.set(i,2,b2)

def ExplicitboundaryForThirdUnknown(val,xyz, power1,power2):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    a1=power1.get(i,0)
    a2=power1.get(i,1)
    a3=power1.get(i,2)
    b1=power2.get(i,0)
    b2=power2.get(i,1)
    b3=power2.get(i,2)
    val.set(i,0,a3)
    val.set(i,1,b3)
    val.set(i,2,-a1-b2)


# Conservation law
nu = functionConstant([1/c])
law1 = dgConservationLawAdvectionDiffusion.diffusionLaw(nu)
law2 = dgConservationLawAdvectionDiffusion.diffusionLaw(nu)
law3 = dgConservationLawAdvectionDiffusion.diffusionLaw(nu)
# build solution vector

npower = functionPython(1, npower_law, [xyz])
solution1 = dgDofContainer(groups, law1.getNbFields())
powersolution1 = dgDofContainer(groups, law1.getNbFields())

nsupra1=dgDofContainer(groups, law1.getNbFields())
solution1.L2Projection(FSnull)
powersolution1.L2Projection(FSnull)
nsupra1.L2Projection(npower)
solution1.exportMsh("output/J1-00000",0,0)

solution2 = dgDofContainer(groups, law2.getNbFields())
powersolution2 = dgDofContainer(groups, law2.getNbFields())
solution2.L2Projection(FSnull)
powersolution2.L2Projection(FSnull)
solution2.exportMsh("output/J2-00000",0,0)

solution3 = dgDofContainer(groups, law3.getNbFields())
powersolution3 = dgDofContainer(groups, law3.getNbFields())
solution3.L2Projection(FSnull)
powersolution3.L2Projection(FSnull)
solution3.exportMsh("output/J3-00000",0,0)

M=dgDofContainer(groups, law1.getNbFields())
M.L2Projection(FSnull)
M.exportMsh("output/normJ-00000",0,0)


bForFirst= functionPython(3,ExplicitboundaryForFirstUnknown,[xyz, powersolution2.getFunctionGradient(), powersolution3.getFunctionGradient()])
bForSecond= functionPython(3,ExplicitboundaryForSecondUnknown,[xyz, powersolution1.getFunctionGradient(), powersolution3.getFunctionGradient()])
bForThird= functionPython(3,ExplicitboundaryForThirdUnknown, [xyz, powersolution1.getFunctionGradient(), powersolution2.getFunctionGradient()])

  
law1.addBoundaryCondition('lateral_cylinder',law1.newBoundaryForSupraLaw(bForFirst,varOfmagneticFirstUnknown,1/c,1/c,1/c))
law2.addBoundaryCondition('lateral_cylinder',law2.newBoundaryForSupraLaw(bForSecond,varOfmagneticSecondUnknown,1/c,1/c,1/c))
law3.addBoundaryCondition('lateral_cylinder',law3.newBoundaryForSupraLaw(bForThird,varOfmagneticThirdUnknown,1/c,1/c,1/c))
  
law1.addBoundaryCondition('inlet_cylinder',law1.newBoundaryForSupraLaw(bForFirst,varOfmagneticFirstUnknown,1/c,1/c,1/c))
law2.addBoundaryCondition('inlet_cylinder',law2.newBoundaryForSupraLaw(bForSecond,varOfmagneticSecondUnknown,1/c,1/c,1/c))
law3.addBoundaryCondition('inlet_cylinder',law3.newBoundaryForSupraLaw(bForThird,varOfmagneticThirdUnknown,1/c,1/c,1/c))

law1.addBoundaryCondition('outlet_cylinder',law1.newBoundaryForSupraLaw(bForFirst,varOfmagneticFirstUnknown,1/c,1/c,1/c))
law2.addBoundaryCondition('outlet_cylinder',law2.newBoundaryForSupraLaw(bForSecond,varOfmagneticSecondUnknown,1/c,1/c,1/c))
law3.addBoundaryCondition('outlet_cylinder',law3.newBoundaryForSupraLaw(bForThird,varOfmagneticThirdUnknown,1/c,1/c,1/c))


sys = linearSystemPETScDouble ()
sys.setParameter("petscOptions", "-ksp_type bicg")

#dof1 = dgDofManager.newDGBlock (groups, law1.getNbFields(), sys)
#dof1 = dgDofManager.newDG (groups, law1.getNbFields(), sys)
dof1 = dgDofManager.newCG (groups, law1.getNbFields(), sys)
#dof2 = dgDofManager.newDGBlock (groups, law2.getNbFields(), sys)
#dof2 = dgDofManager.newDG (groups, law2.getNbFields(), sys)
dof2 = dgDofManager.newCG (groups, law2.getNbFields(), sys)
dof3 = dgDofManager.newCG (groups, law3.getNbFields(), sys)
# build solution vector
solve=dgSolveSupraBetaPowerLaw (law1, law2,law3, dof1, dof2,dof3)
  
solve.getNewtonX().setVerb(10)
solve.getNewtonY().setVerb(10)
solve.getNewtonZ().setVerb(10)



startcpu = time.clock()
for i in range (0, 10001):
 
  #print'*** solve ***'
  #sys = linearSystemPETScBlockDouble ()
 
  t = t + dt
  #print("Iter %i t=%.2f" % (i,t))

  solve.iterateBetaVectorial3DPowerLaw(solution1,solution2,solution3,powersolution1,powersolution2,powersolution3,nsupra1,dt,t)
  sol1=solution1.getFunction()
  sol2=solution2.getFunction()
  sol3=solution3.getFunction()
  mod= functionPython(1, mod_law, [xyz,sol1,sol2,sol3])
  M.L2Projection(mod)
 
  print("Iter %i t=%.2f " % (i,t))
  print("Iter %i t=%.10f  cputime=%5.3f" % (i,t,time.clock()))
  #print("Iter %i t=%.10f norm=%.8f" % (i,t,norm))
  if (i % 5 == 0) :
    solution1.exportMsh('output/J1-%05i' % (i), t, i)
    solution2.exportMsh('output/J2-%05i' % (i), t, i)
    solution3.exportMsh('output/J3-%05i' % (i), t, i)
    M.exportMsh('output/normJ-%05i' % (i), t, i)


