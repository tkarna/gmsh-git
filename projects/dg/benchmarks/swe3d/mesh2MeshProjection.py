#!/usr/bin/env python
# -*- coding: utf-8 -*-
from dgpy import *
from gmshpy import *
from sys import stdout
from shutil import copyfile
import time, math, os

#meshFile1 = "prisms1.msh"
#meshFile2 = "prisms2.msh"
#meshFile1 = "prisms1_2d.msh"
#meshFile2 = "prisms2_2d.msh"
meshFile1 = "tracerBox2d.msh"
meshFile2 = "tracerBox2d.msh"

odir = 'projection/'
# create outputdir if !exists
if not os.path.exists(odir) :
  os.mkdir(odir)

model1 = GModel()
model1.load(meshFile1)
order1 = model1.getMeshElementByTag(1).getPolynomialOrder()
dimension1 = model1.getDim()
groups1 = dgGroupCollection(model1, dimension1, order1)
#groups1.splitGroupsByVerticalLayer(["bottom"])
groups1.splitGroupsRandom(5)
groups1.buildGroupsOfInterfaces()
print 'Model1 dimension:',dimension1
model1.save(odir+'mesh1.msh')

model2 = GModel()
model2.load(meshFile2)
order2 = model2.getMeshElementByTag(1).getPolynomialOrder()
dimension2 = model2.getDim()
groups2 = dgGroupCollection(model2, dimension2, order2)
#groups2.splitGroupsByVerticalLayer(["bottom"])
groups2.splitGroupsRandom(3)
groups2.buildGroupsOfInterfaces()
print 'Model2 dimension:',dimension2
model2.save(odir+'mesh2.msh')

dof1_1 = dgDofContainer(groups1,1)
dof1_3 = dgDofContainer(groups1,3)
dof2_1 = dgDofContainer(groups2,1)
dof2_3 = dgDofContainer(groups2,3)

def aaaa( val, xyz ) :
  for i in range(xyz.size1()) :
    val.set(i,0,xyz(i,0)**1.5*(xyz(i,2)-0.5) )
    val.set(i,1,-xyz(i,1) )
    val.set(i,2,math.sin(xyz(i,0)) )
testFunc = functionPython(3, aaaa, [getFunctionCoordinates()])

dof1_3.L2Projection(testFunc)
dof1_3.exportMsh(odir+'dof1_3',0,0)

projector = dgMesh2MeshProjection(dof1_3,dof2_3)
projector.projectFromTo(dof1_3,dof2_3)
dof2_3.exportMsh(odir+'dof2_3',0,0)
print dof1_3.norm(), dof2_3.norm()

dof2_3.L2Projection(testFunc)
projector.projectFromTo(dof2_3,dof1_3)
print dof1_3.norm(), dof2_3.norm()
dof1_3.exportMsh(odir+'dof1_3projected',0,0)

projector.projectFieldFromTo(dof1_3,dof2_1,2,0)
print dof1_3.norm(), dof2_1.norm()
dof2_1.exportMsh('projection/dof2_1',0,0)




