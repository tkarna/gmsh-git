#!/usr/bin/env python
# -*- coding: utf-8 -*-
from dgpy import *
#from sys import stdout
from shutil import copyfile
import time, math, os, inspect
#from matplotlib import pyplot as plt

# set Ctrl-C to default signal (terminates immediately)
import signal
signal.signal(signal.SIGINT, signal.SIG_DFL)

# .-------------------------------.
# |          user input           |
# '-------------------------------'

nPart = Msg.GetCommSize()
#meshFile = 'square_sf_' + str(nPart) + '_5lay.msh'
meshFile = 'square_' + str(nPart) + '_3d.msh'

odir = 'output'
odirOld = 'outputOld'
# create outputdir if !exists
if Msg.GetCommRank()==0 and not os.path.exists(odir) :
  os.mkdir(odir)
if Msg.GetCommRank()==0 :
  copyfile(meshFile,odir + '/mesh.msh')
  pythonFile = inspect.getfile(inspect.currentframe())
  copyfile(pythonFile,odir + '/' + pythonFile)


# .-------------------------------.
# |         create solver         |
# '-------------------------------'

s = slim3dSolver(meshFile, ["bottom_Surface"], ["top_Surface"])
# options
s.setSolveS(0)
s.setSolveT(1)
s.setSolveSImplicitVerticalDiffusion(0)
s.setSolveTImplicitVerticalDiffusion(1)
s.setSolveUVImplicitVerticalDiffusion(1)
s.setSolveTurbulence(0)
s.setAdvectTurbulentEnergy(0)
s.setComputeBottomFriction(1)
s.setFlagMovingMesh(1)
s.exportDirectory = odir
s.setFlagUVLimiter(0)
#s.setUseOptimalLimiter(1)
s.setUseConservativeALE(1)
if s.getSolveTurbulence() :
  s.turbulenceSetupFile = "squareTurb.nml"

# .-------------------------------.
# |         set functions         |
# '-------------------------------'
f = s.functions
d = s.getDofs(True)

f.z0BFunc = functionConstant(0.01)
f.z0SFunc = functionConstant(0.00)
f.kappahTotal = functionConstant(30)
#f.kappavFunc = functionConstant(0.01)
f.kappavFunc = pacanowskiPhilander(d.uvDof.getFunctionGradient(), d.rhoDof3d.getFunctionGradient(), 1e-2, 1e-5, 10)
f.nuvFunc = functionConstant(0.1)
f.nuhTotal = functionConstant(200)
f.rhoFunc = linearStateEquation(d.TDof.getFunction(), -1001.85*1.94552591e-4, 10)
#f.rhoFunc = linearStateEquation(d.TDof.getFunction(), -1027*2e-4, 10)
f.bottomFrictionDragCoeff2d = functionConstant(2.5e-3)
f.bottomFrictionDragCoeff3d = functionConstant(2.5e-3)

slim3dParameters.rho0 = 1000
CCode = """
#include "fullMatrix.h"
#include "function.h"
extern "C" {
void coriolis (dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &xyz) {
  for (size_t i = 0; i < xyz.size1(); ++i) {
    val.set(i, 0, 1e-4 + 2e-11 * xyz.get(i, 1));
  }
}
void temp (dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &xyz) {
  for (size_t i = 0; i < xyz.size1(); i++){
    val.set(i,0,25-5e-6*(xyz(i,1)+8e5) + 8.2e-3*xyz(i,2) ); 
    //val.set(i,0,25-5e-6*(xyz(i,1)+8e5) + 0.3*sin(xyz(i,0)/9e5*2*M_PI) + 8.2e-3*xyz(i,2) ); 
  }
}
void TRelaxation (dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &T, fullMatrix<double> &xyz) {
  double tau = 86400*3;
  double shape, lambd;
  double southBound = -8e5;
  double northBound = 8e5;
  double relaxBand = 5e5; //default value : 1.6e5
  for (size_t i = 0; i < xyz.size1(); i++) {
    shape = 0;
    if (xyz(i,1) < southBound + relaxBand)
      shape = (southBound + relaxBand - xyz(i,1)) / relaxBand;
    if (xyz(i,1) > northBound - relaxBand)
      shape = (xyz(i,1) - northBound + relaxBand) / relaxBand;
    lambd = shape/tau;
    double TInit = 25-5e-6*(xyz(i,1)+8e5) + 8.2e-3*xyz(i,2);    
    val.set(i,0,-lambd * (T(i,0) - TInit));
  }
}
void rOutside (dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &xyz, fullMatrix<double> &eta) {
  //double alpha = - 2e-4;
  double alpha = -1001.85*1.94552591e-4/1027;
  for (size_t i = 0; i < xyz.size1(); i++) {
    double r = alpha * ( (15 - 5e-6*(xyz(i,1)+8e5)) * (eta(i,0)-xyz(i,2)) + 8.2e-3 * (pow(eta(i,0),2) - pow(xyz(i,2),2))/2);
    val.set(i,0,r);
  }
}  
}
"""
tmpLib = "./tmp.dylib"
if (Msg.GetCommRank() == 0) :
  functionC.buildLibrary(CCode,  tmpLib);
Msg.Barrier()

f.coriolisFunc3d= functionC(tmpLib, "coriolis",  1,  [function.getCoordinates()])
f.coriolisFunc2d= f.coriolisFunc3d

f.TInitFunc = functionC(tmpLib, "temp", 1, [function.getCoordinates()])
f.SInitFunc = functionConstant(0)
f.etaInitFunc = functionConstant(0)

# .-------------------------------.
# |       create equations        |
# '-------------------------------'

e = s.createEquations()

#testDof = dgDofContainer(s.groups3d,1)
#shapeTest =  functionC(tmpLib, "TRelaxation", 1, [d.TDof.getFunction(), function.getCoordinates()])
#testDof.interpolate(shapeTest)
#testDof.exportMsh("shapeTest")
#print 'done'
#Msg.Exit(0)

if s.getSolveT() :
  TRelaxFunc = functionC(tmpLib, "TRelaxation", 1, [d.TDof.getFunction(), function.getCoordinates()])

rOutsideFunc = functionC(tmpLib, "rOutside", 1, [function.getCoordinates(), d.etaDof3d.getFunction()])

horMomEq = e.horMomEq
#horMomEq.setIsSpherical
horMomEq.setLaxFriedrichsFactor(0.0)
#horMomEq.setNuV(f.nuVFunc)
horMomBndSides = horMomEq.newBoundaryWallLinear(0.0)
horMomBndZero = horMomEq.new0FluxBoundary()
horMomBndSymm = horMomEq.newSymmetryBoundary('')
#horMomBndTop =  horMomEq.newBoundarySurface(fWind)
horMomBndWall = horMomEq.newBoundaryWall()
horMomEq.addBoundaryCondition('bottom_Surface',horMomBndWall)
horMomEq.addBoundaryCondition('Wall',horMomBndWall)
#horMomEq.addBoundaryCondition('cut',horMomBndWall)
#horMomEq.addBoundaryCondition('paste',horMomBndWall)
horMomEq.addBoundaryCondition('vertical_bottom',horMomBndWall)
horMomEq.addBoundaryCondition('top_Surface',horMomBndSymm)
#horMomEq.addBoundaryCondition('top_Surface',horMomBndTop)

if s.getSolveUVImplicitVerticalDiffusion() :
  vertMomUEq = e.vertMomUEq
  vertMomUBndZero = vertMomUEq.new0FluxBoundary()
  vertMomUBndBottom = vertMomUEq.newBoundaryBottom(f.bottomFrictionFunc, f.zBotDistFunc)
  vertMomUBndSymm = vertMomUEq.newSymmetryBoundary('')
  #vertMomUBndTop =  vertMomUEq.newBoundarySurface(fWind)
  vertMomUEq.addBoundaryCondition('top_Surface', vertMomUBndZero)
  vertMomUEq.addBoundaryCondition('bottom_Surface', vertMomUBndBottom)
  vertMomUEq.addBoundaryCondition('Wall', vertMomUBndZero)
  #vertMomUEq.addBoundaryCondition('cut', vertMomUBndZero)
  #vertMomUEq.addBoundaryCondition('paste', vertMomUBndZero)
  vertMomUEq.addBoundaryCondition('vertical_bottom', vertMomUBndZero)

wEq = e.wEq
#wEq.setIsSpherical(R)
wBndZero = wEq.new0FluxBoundary()
wBndSymm = wEq.newSymmetryBoundary('')
wEq.addBoundaryCondition('Wall',wBndZero)
#wEq.addBoundaryCondition('cut',wBndZero)
#wEq.addBoundaryCondition('paste',wBndZero)
wEq.addBoundaryCondition('vertical_bottom',wBndZero)
wEq.addBoundaryCondition('top_Surface',wBndSymm)
wEq.addBoundaryCondition('bottom_Surface',wBndZero)

if s.getSolveT() :
  TEq = e.TEq
  TEq.setLaxFriedrichsFactor(0.0)
  TEq.setSource(TRelaxFunc)
  TBndZero = TEq.new0FluxBoundary()
  TBndSymm = TEq.newSymmetryBoundary('')
  TEq.addBoundaryCondition('Wall',TBndZero)
  #TEq.addBoundaryCondition('cut',TBndZero)
  #TEq.addBoundaryCondition('paste',TBndZero)
  TEq.addBoundaryCondition('vertical_bottom',TBndZero)
  TEq.addBoundaryCondition('top_Surface',TBndZero)
  TEq.addBoundaryCondition('bottom_Surface',TBndZero)

eta2dEq = e.eta2dEq
#eta2dEq.setIsSpherical(R)
etaBndZero = eta2dEq.new0FluxBoundary()
eta2dEq.addBoundaryCondition('Wall',etaBndZero)
#eta2dEq.addBoundaryCondition('cut',etaBndZero)
#eta2dEq.addBoundaryCondition('paste',etaBndZero)


uv2dEq = e.uv2dEq
#uv2dEq.setIsSpherical(R)
#uv2dEq.setWindStress(fWind)
uv2dBndWall = uv2dEq.newBoundaryWall()
uv2dEq.addBoundaryCondition('Wall',uv2dBndWall)
#uv2dEq.addBoundaryCondition('cut',uv2dBndWall)
#uv2dEq.addBoundaryCondition('paste',uv2dBndWall)

# .-------------------------------
# |     setup export options      |
# '-------------------------------'
exp = s.exporter

#exp.addDofContainer(d.rDof3d)
#exp.addDofContainer(d.rGradDof3d)
exp.addDofContainer(d.uvAvDof2d)
exp.addDofContainer(d.TDof)
#exp.addDofContainer(d.kappavDof)
#exp.addDofContainer(d.xyzOrigDof)
#exp.addDofContainer(d.etaDof3dCG)
#exp.addDofContainer(d.wMeshSurfDof3d)
#exp.addDofContainer(d.etaDof3d)
exp.setTimeExportFormat(exp.DAY)

# .-------------------------------.
# |     setup time integrator     |
# '-------------------------------'
t = slim3dTimeIntegratorPC(s)

rk = dgERK(e.eta2dEq, None, DG_ERK_44)# DG_ERK_EULER)
dtRK = rk.computeInvSpectralRadius(d.etaDof2d)
CFL = 0.5
dt = CFL*dtRK 


if Msg.GetCommRank() == 0 :
  print('Runge-Kutta dt :' + str(dtRK) + ' CFL:' + str(CFL) + ' dt:' + str(dt))

t.setTimeStep(dt)
t.set3dTimeStepRatio(50)
t.setExportInterval(86400/2)
t.setFullExportIntervalRatio(10)
t.setExportAtEveryTimeStep(True)
t.setEndTime(86400*50)
t.initializeFields()
#t.hotStart(odirOld,10)


plotEnergy = False
####ENERGY COMPUTATION
def rhoUU(val, rho, uv, w) :
  for i in range(val.size1()):
    val.set(i,0,rho(i,0) * (uv(i,0)*uv(i,0) + uv(i,1)*uv(i,1) + w(i,0)*w(i,0)))

rhoUUf = functionPython(1, rhoUU, [d.rhoDof3d.getFunction(), d.uvDof.getFunction(), d.wDof3d.getFunction()])

energy = fullMatrixDouble(1,1)
mass = fullMatrixDouble(1,1)
integratorRhoUU = dgFunctionIntegrator(s.groups3d, rhoUUf)
integratorRho = dgFunctionIntegrator(s.groups3d, d.rhoDof3d.getFunction())

integratorRhoUU.compute(energy)
integratorRho.compute(mass)
if Msg.GetCommRank()==0 :
  energyVsTimeFile = open(odir + '/energyTimeSerie.txt', "w");
  energyVsTimeFile.write("%6.4f %1.10f\n" % (t.getTime()/86400., energy(0,0)/mass(0,0)))
  if plotEnergy :
    time = [t.getTime()/86400.]
    energyVec = [energy(0,0)/mass(0,0)] 
    plt.ion()
    fig = plt.figure()
    pltEn,=plt.plot(time,energyVec)
    axes=plt.gca()
    axes.set_xlabel("Time [days]")
    axes.set_ylabel("Energy [$m^2/s^2$]")
    plt.draw()
##########

#t.iterate()
while t.getTime() < t.getEndTime() :
  t.advanceOneTimeStep()
  t.checkSanity()
  if t.checkExport() :
    exp.exportDofs(t.getIter(),t.getTime(),t.getTimeStep())
    integratorRhoUU.compute(energy)
    integratorRho.compute(mass)
    if Msg.GetCommRank()==0 :
      energyVsTimeFile.write("%6.4f %1.10f\n" % (t.getTime()/86400., energy(0,0)/mass(0,0)))
      if plotEnergy :
        time.append(t.getTime()/86400.)
        energyVec.append(energy(0,0)/mass(0,0))
        pltEn.set_data(time,energyVec)
        axes.relim()
        axes.autoscale_view(True,True,True)
        plt.draw()
if plotEnergy and Msg.GetCommRank()==0 :
  fig.savefig(odir + '/energyVsTime.pdf')
t.terminate(0)
