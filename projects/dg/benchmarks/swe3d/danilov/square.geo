//L = 9e5;
L = 9e5/60;
l = 8e5;
Point(1) = {-L, -l, 0};
Point(2) = {L, -l, 0};
Point(3) = {L, l, 0};
Point(4) = {-L, l, 0};
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line Loop(1) = {3, 4, 1, 2};
Plane Surface(1) = {1};


/*
f - > 1
sf - > /2
c - > /4
*/

Transfinite Line{1} = 2;
Transfinite Line{3} = 2;


//Transfinite Line{1} = 6*20 /2;///8;
Transfinite Line{2} = 7*15 /2;///8;
//Transfinite Line{3} = 6*20 /2;///8;
Transfinite Line{4} = 7*15 /2;///8;
Transfinite Surface{1} = {1,2,3,4};

Recombine Surface{1};

Physical Surface("Surface")={1};
Physical Line("Wall") = {1,3};
Physical Line("cut") = {2};
Physical Line("paste") = {4};

