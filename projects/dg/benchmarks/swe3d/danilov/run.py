import os, sys
from dgpy import *
from periodicMap import *
from extrudePeriodic import *

name = 'square'
os.system('rm ' + name + '_*')
os.system('rm -rf output')
nPart = int(sys.argv[1])

inputMesh = name+'.msh'
mesh1 = name+'_'+str(nPart)+'.msh'
mesh2 = name+'_'+str(nPart)+'b.msh'
mesh3d = name+'_'+str(nPart)+'_3d.msh'

os.system('gmsh -2 '+name+'.geo')

model = GModel()
model.load(inputMesh)
pOpt = meshPartitionOptions()
pOpt.setNumOfPartitions(nPart)
PartitionMesh(model, pOpt)
model.save(mesh1)

def shiftOperation(node, iPerBound) :
  n = [node[0] - 2*9e5/60, node[1], node[2]]
  return n
cutTags = ["cut"]
pasteTags = ["paste"]
mapFilename = "periodicMesh.txt"
periodicMap(mesh1, mesh2, 1, shiftOperation, cutTags, pasteTags, mapFilename)

h = 1600
nbLayers = 15
zLayers = [0, 10, 25, 45, 100, 200, 500, 800, 1200, 1600]
def getLayers(e, v) :
  #return zLayers
  return [z * h /nbLayers for z in  range(nbLayers + 1)]
extrudePeriodic(mesh(mesh2), getLayers, mapFilename, mapFilename).write(mesh3d)

os.system('mpirun -np ' + str(nPart) + ' rundgpy danilov.py ')
sys.exit(0)
