/*
 * =====================================================================================
 *
 *       Filename:  warnerEstuaryFunctions.cc
 *
 *    Description:  Provides functions related to the Estuary test case by
                    Warner et al., Ocean Modelling, 2005.
 *
 *         Author:  karna, tuomas.karna@uclouvain.be
 *
 * =====================================================================================
 */

#include "fullMatrix.h"
#include "function.h"
#include "dgGroupOfElements.h"
#include "math.h"
static  double vonKarman = 0.41587379967373655;
double bfFactor(double bath, double zBot, double roughnessLength) {
  double distBot = bath + zBot;
  return vonKarman / log( ( distBot + roughnessLength ) / roughnessLength );
};

extern "C" {
/***** parameters *****/
double Htide = 10, Utide = 0.4, Ttide = 12*3600, Stide=30;
double Hriver = 5, Uriver = 0.08, Sriver=0;
/***** initial salinity *****/
void initS (dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< val.size1(); i++) {
    val.set(i,0, Stide - (Stide-Sriver)*std::min( std::max( (xyz(i,0)+20000)/50000, 0.0), 1.0) );
  }
}
/***** bathymetry *****/
double bathDouble(double x) {
  return Htide *(1-x/50000)/2.0 + Hriver *(1+x/50000)/2.0;
}
double bathGradXDouble(double x) {
  return Htide *(-1/50000)/2.0 + Hriver *(1/50000)/2.0;
}
void bath (dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< val.size1(); i++) {
    val.set(i,0, bathDouble(xyz(i,0)) );
  }
}
void bathGrad (dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< val.size1(); i++) {
    val.set(i,0, bathGradXDouble(xyz(i,0)) );
    val.set(i,1, 0 );
  }
}
/***** tidal forcing *****/
double Qriver = Uriver*Hriver;
double Qtide(double t) {
  return Utide * Htide * sin( 2*M_PI*(-t)/Ttide );
}
/* depth averaged velocity (for 2d mode)*/
void tideUVAv (dataCacheMap *d, fullMatrix<double> &val, fullMatrix<double> &time, fullMatrix<double> &xyz, fullMatrix<double> &eta, fullMatrix<double> &bath) {
  for (size_t i = 0; i< val.size1(); i++) {
    val.set(i,0, (Qtide(time(i,0)) - Qriver)/( Htide + eta(i,0) ) );
    val.set(i,1, 0);
  }
}
void riverUVAv (dataCacheMap *d, fullMatrix<double> &val, fullMatrix<double> &time, fullMatrix<double> &xyz, fullMatrix<double> &eta, fullMatrix<double> &bath) {
  for (size_t i = 0; i< val.size1(); i++) {
    val.set(i,0, -Qriver/( Hriver + eta(i,0) ) );
    val.set(i,1, 0);
  }
}
/* depth integrated velocity (for 3d mode)*/
void tideUVInt (dataCacheMap *d, fullMatrix<double> &val, fullMatrix<double> &time, fullMatrix<double> &xyz, fullMatrix<double> &eta, fullMatrix<double> &bath) {
  for (size_t i = 0; i< val.size1(); i++) {
    val.set(i,0, (Qtide(time(i,0)) - Qriver) );
    val.set(i,1, 0);
  }
}
void riverUVInt (dataCacheMap *d, fullMatrix<double> &val, fullMatrix<double> &time, fullMatrix<double> &xyz, fullMatrix<double> &eta, fullMatrix<double> &bath) {
  for (size_t i = 0; i< val.size1(); i++) {
    val.set(i,0, -Qriver );
    val.set(i,1, 0);
  }
}
/* 3d velocity (for 3d mode)*/
void tideUV3d (dataCacheMap *d, fullMatrix<double> &val, fullMatrix<double> &time, fullMatrix<double> &xyz, fullMatrix<double> &eta, fullMatrix<double> &bath, fullMatrix<double> &uv, fullMatrix<double> &uvInt) {
  for (size_t i = 0; i< val.size1(); i++) {
    double uvAv = (Qtide(time(i,0)) - Qriver)/(bath(i,0)+eta(i,0));
    double uvCorr = (uvInt(i,0)/( eta(i,0) + bath(i,0) ) - uvAv);
    val.set(i,0, uv(i,0) - uvCorr );
    val.set(i,1, 0);
  }
}
void riverUV3d (dataCacheMap *d, fullMatrix<double> &val, fullMatrix<double> &time, fullMatrix<double> &xyz, fullMatrix<double> &eta, fullMatrix<double> &bath, fullMatrix<double> &uv, fullMatrix<double> &uvInt) {
  for (size_t i = 0; i< val.size1(); i++) {
    double uvAv = -Qriver/(bath(i,0)+eta(i,0));
    double uvCorr = (uvInt(i,0)/( eta(i,0) + bath(i,0) ) - uvAv);
    val.set(i,0, uv(i,0) - uvCorr );
    val.set(i,1, 0);
  }
}
/* initial uv */
void initialUV (dataCacheMap *d, fullMatrix<double> &val, fullMatrix<double> &time, fullMatrix<double> &xyz, fullMatrix<double> &eta, fullMatrix<double> &bath) {
  for (size_t i = 0; i< val.size1(); i++) {
    val.set(i,0, ( (Qtide(time(i,0)) - Qriver)*(1-xyz(i,0)/50000)/2.0 - Qriver*(1+xyz(i,0)/50000)/2.0)
      / (bath(i,0)) );
    val.set(i,1, 0);
  }
}
}
