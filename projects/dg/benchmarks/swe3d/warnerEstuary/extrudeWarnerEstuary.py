from extrude import *

nbLayers = 10
hDeep = 10

def getLayersSigma (e, v) :
  h = hDeep * (1-v[0]/50000.)/2. + 5 *(1+v[0]/50000.)/2.
  return [z * h /nbLayers for z in  range(nbLayers + 1)]

def getLayersZ (e, v) :
  zLayers = [hDeep * i * 1. / nbLayers for i in range(nbLayers + 1)]
  h = hDeep * (1-v[0]/50000.)/2. + 5 *(1+v[0]/50000.)/2.
  minIndex, minValue = min(enumerate(zLayers), key=lambda a : abs(h - a[1]))
  return zLayers[0:min(nbLayers, max(1,  minIndex))+2]

extrude(mesh("warnerEstuary2d.msh"), getLayersZ).write("warnerEstuary.msh")
