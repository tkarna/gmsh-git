//*********** warnerEstuary2d.geo *************//
Cx = 50000;
// Cy = 1000; // extra fine
// lc = 1000;
// Cy = 1900; // fine
// lc = 1900;
Cy = 5000; // coarse narrow
lc = 5000;
L = 5;
Lup = 0;

Point(1) = {-Cx, -Cy, 0, lc};
Point(2) = {Cx , -Cy, 0, lc};
Point(3) = {Cx , Cy , 0, lc};
Point(4) = {-Cx, Cy , 0, lc};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};

// Physical Line("sideLTopLine") = {1};
// Physical Line("riverTopLine") = {2};
// Physical Line("sideRTopLine") = {3};
// Physical Line("seaTopLine") = {4};

Physical Line("sideL") = {1};
Physical Line("river") = {2};
Physical Line("sideR") = {3};
Physical Line("sea") = {4};

Line Loop(5) = {1,2,3,4};
Plane Surface(6) = {5};
Physical Surface("volume") = {6};

Mesh.Algorithm3D=4; // frontal [lobes]
Mesh.RecombinationAlgorithm=0; // standard instead of blossom
