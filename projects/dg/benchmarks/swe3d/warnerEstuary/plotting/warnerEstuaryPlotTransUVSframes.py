#!/usr/bin/env python
# -*- coding: utf-8 -*-
from dgpy import *
from sys import stdout
from shutil import copyfile
import time, math, os
from scipy import *
import matplotlib.pyplot as plt
from matplotlib import rc
import matplotlib.colors as colors
import cPickle as pickle

# NOTE to create animation from the frames do (convert 8 frames per second to 25 frames per second in the mp4)
# ffmpeg -r 8 -i warner_fine_snap_S_U_%5d.png -r 25 -b 3000k warnerEstuary_animation.mp4

fontsize = 20
rc('text', usetex=True)
rc('font', family='times', size=fontsize)
figSize = (10,4)

dataDir = 'warnerEstuaryPCextraFineKmin1e-5_uvTilde_cALE/'
idxDir = dataDir + 'idx/'
meshFile = dataDir + 'mesh.msh'
meshFile2d = dataDir + 'mesh2d.msh'
#export_count = 0
#export_count = 336
#export_counts = range(0,69)
#export_counts = range(70,141)
export_counts = range(141,200)
oprefix = 'plots/warner_fine_snap_'
saveFigures = False
saveFigures = True
showFigures = False
#showFigures = True

Msg = Msg()
model = GModel()
model.load(meshFile)
order = model.getMeshElementByTag(1).getPolynomialOrder()
print('Polynomial order: ' + str(order))
dimension = 3

groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByVerticalLayer(["bottom"])

model2d = GModel()
model2d.load (meshFile2d)
groups2d = dgGroupCollection(model2d, dimension-1, order)

# DOFs
etaDof = dgDofContainer(groups2d, 1)
SDof = dgDofContainer(groups, 1)
tkeDof = dgDofContainer(groups, 1)
uvDof = dgDofContainer(groups, 2)

etaFunc = etaDof.getFunction()
SFunc = SDof.getFunction()
tkeFunc = tkeDof.getFunction()
uvFunc = uvDof.getFunction()

def bathFunc(x) :
  return 10*(1.-x/50000.)/2.0 + 5*(1.+x/50000.)/2.0

# horizontal coords
nX = 500
nZ = 200
xs = linspace(-5e4,5e4,nX)
y = 0
Z0 = zeros((nZ,nX)) # static mesh
Z = zeros((nZ,nX))  # moving mesh
X = tile(xs,(nZ,1))
for i,x in enumerate(xs) :
  Z0[:,i] = linspace(-bathFunc(x),0,nZ)

# offset to match Warner et al
xOffset = 5e4
xScale = 1e3
X = (X + xOffset)/xScale
xMin = X.min()
xMax = X.max()

etas = zeros(xs.shape)
SVals = zeros(Z0.shape)
uVals = zeros(Z0.shape)
tkeVals = zeros(Z0.shape)
vVals = zeros(Z0.shape)

dgDCMap2d = dataCacheMap(dataCacheMap.POINT_MODE, groups2d)
etaCache = dgDCMap2d.get(etaFunc)

dgDCMap = dataCacheMap(dataCacheMap.POINT_MODE, groups)
SCache = dgDCMap.get(SFunc)
uvCache = dgDCMap.get(uvFunc)
tkeCache = dgDCMap.get(tkeFunc)

for export_count in export_counts :
  istr = "_{0:05}".format(export_count)
  etaDof.readMsh( idxDir + 'eta2d'+istr+'.idx')
  SDof.readMsh(   idxDir + 'S'+istr+'.idx')
  #tkeDof.readMsh(  idxDir + 'tke'+istr+'.idx')
  uvDof.readMsh(  idxDir + 'uv'+istr+'.idx')
  
  # get eta value (depends onthe mesh!)
  for i,x in enumerate(xs) :
    checkElem = dgDCMap2d.setPoint(groups2d,x,y,0)
    etas[i] = etaCache.get()(0,0)
    Z[:,i] = linspace(-bathFunc(x),etas[i],nZ)

  for i,x in enumerate(xs) :
    for j,z in enumerate(Z0[:,i]) :
      checkElem = dgDCMap.setPoint(groups,x,y,z)
      #minSVal = 1e-15
      #maxSVal = 30 - minSVal
      #SVals[j,i] = min( max( SCache.get()(0,0), minSVal), maxSVal )
      SVals[j,i] = SCache.get()(0,0)
      uVals[j,i] = uvCache.get()(0,0)
      #tkeVals[j,i] = tkeCache.get()(0,0)

  plt.figure(figsize=figSize)
  plt.gcf().subplots_adjust(left=0.09, right=0.93, top=0.90, bottom=0.17)
  #contVals = linspace(-4.999,34.999,9)
  contVals = linspace(-0.0001,30.04,16)
  plt.contourf(X,Z,SVals,contVals)
  cb = plt.colorbar(fraction=0.06, pad=0.03, aspect=10, format='%.0f')
  cb.set_label(r'$S$ [$psu$]')
  cb.set_clim(0,30)
  #CS = plt.contour(X,Z0,SVals,contVals,linewidths=2,linestyles='solid')
  #plt.clabel(CS, inline=1, fontsize=10, fmt='%1.0f')
  plt.plot([xMin,xMax],[-10,-5],'k-',linewidth=2.0)
  plt.ylim(-10,1)
  #plt.xlim(-5e4,5e4)
  #plt.title(r'$S$ [$psu$]')
  plt.ylabel(r'$z$ [$m$]')
  plt.xlabel(r'$x$ [$km$]')
  plt.xticks(linspace(xMin,xMax,11))
  #plt.gca().ticklabel_format(style='sci', scilimits=(1,1),axis='x') # sci style ticks
  plt.gca().set_axis_bgcolor('none') # transparent background
  plt.grid(True)
  #cdict = {'red':   ((0.0, 0.8, 0.8),
                    #(1.0, 0.3, 0.3)),
          #'green': ((0.0, 0.9, 0.9),
                    #(1.0, 0.1, 0.1)),
          #'blue':  ((0.0, 1.0, 1.0),
                    #(1.0, 0.1, 0.1))}
  #fullGrayCmap = colors.LinearSegmentedColormap('fullGray',cdict,256)
  #plt.set_cmap(fullGrayCmap)

  # plot arrows
  if export_count > 0 :
    UVmax = -1e+20
    UVmin = 1e+20
    def draw_arrows(X,Y,U,V) :
      global UVmax, UVmin
      for i in range(X.shape[0]) :
        for j in range(X.shape[1]) :
          #print i,j
          uvNorm = draw_one_arrow(X[i,j],Y[i,j],U[i,j],V[i,j])
          UVmax = max(UVmax,uvNorm)
          UVmin = min(UVmin,uvNorm)
          #return UVmax,UVmin

    def draw_one_arrow(x,y,u,v) :
      headL = 0.33
      shaftW = 0.05
      headW = 0.25
      norm = sqrt(u**2 + v**2)
      scale = 5
      mag = norm*scale
      plt.arrow(x, y, u*scale, v*scale, #color=(0.8,0.8,0.8),
                facecolor = (0.8,0.8,0.8),
                edgecolor = (0.0,0.0,0.0),
                width=shaftW*mag,
                head_width=headW*mag,
                head_length=headL*mag,
                linewidth=0.4*mag,
                length_includes_head=True
                )
      return norm

    ix0 = 1
    ixDiv = 20
    iy0 = 5
    iyDiv = 30
    #print Z0[1,:]
    draw_arrows(    X[ix0:-ix0:ixDiv,iy0:-iy0:iyDiv],
                    Z[ix0:-ix0:ixDiv,iy0:-iy0:iyDiv],
                uVals[ix0:-ix0:ixDiv,iy0:-iy0:iyDiv],
                vVals[ix0:-ix0:ixDiv,iy0:-iy0:iyDiv])
    print 'UV arrow {m:7.4f} {M:7.4f}'.format(M=UVmax,m=UVmin)

  if saveFigures :
    if export_count == 0 or export_count == 200:
      filename = oprefix +'S_U' +istr +'.pdf'
      print 'saving to ' +  filename
      plt.savefig(filename)
    filename = oprefix +'S_U' +istr +'.png'
    print 'saving to ' +  filename
    plt.savefig(filename)

  ## TKE
  #plt.figure(figsize=figSize)
  #plt.gcf().subplots_adjust(left=0.09, right=0.93, top=0.90, bottom=0.17)
  ##contVals = linspace(-0.0001,30.04,16)
  #plt.contourf(X,Z,log10(tkeVals),10)
  ##plt.contourf(X,Z,tkeVals,contVals)
  #cb = plt.colorbar(fraction=0.06, pad=0.03, aspect=10, format='%.2f')
  #cb.set_label(r'TKE [$\log_{10} (m^2 s^{-2}) $]')
  ##cb.set_clim(0,30)
  ##CS = plt.contour(X,Z0,SVals,contVals,linewidths=2,linestyles='solid')
  ##plt.clabel(CS, inline=1, fontsize=10, fmt='%1.0f')
  #plt.plot([xMin,xMax],[-10,-5],'k-',linewidth=2.0)
  #plt.ylim(-10,1)
  ##plt.xlim(-5e4,5e4)
  ##plt.title(r'$S$ [$psu$]')
  #plt.ylabel(r'$z$ [$m$]')
  #plt.xlabel(r'$x$ [$km$]')
  #plt.xticks(linspace(xMin,xMax,11))
  ##plt.gca().ticklabel_format(style='sci', scilimits=(1,1),axis='x') # sci style ticks
  #plt.gca().set_axis_bgcolor('none') # transparent background
  #plt.grid(True)
  #cdict = {'red':   ((0.0, 0.8, 0.8),
                    #(1.0, 0.3, 0.3)),
          #'green': ((0.0, 0.9, 0.9),
                    #(1.0, 0.1, 0.1)),
          #'blue':  ((0.0, 1.0, 1.0),
                    #(1.0, 0.1, 0.1))}
  #fullGrayCmap = colors.LinearSegmentedColormap('fullGray',cdict,256)

  #if saveFigures :
    #filename = oprefix +'tke' +istr +'.pdf'
    #print 'saving to ' +  filename
    #plt.savefig(filename)
    #filename = oprefix +'tke' +istr +'.png'
    #print 'saving to ' +  filename
    #plt.savefig(filename)

  if showFigures :
    plt.show()

