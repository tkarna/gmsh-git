#!/usr/bin/env python
# -*- coding: utf-8 -*-
from dgpy import *
from sys import stdout
from shutil import copyfile
import time, math, os
from scipy import *
import matplotlib.pyplot as plt
from matplotlib import rc
import matplotlib.colors as colors

rc('text', usetex=True)
rc('font', family='times', size=12)
figSize = (10,3)

#odir = 'warnerEstuaryABfine/'
#odir = 'warnerEstuaryABextraFine/'
#odir = 'warnerEstuaryPCextraFine/'
odir = 'warnerEstuaryPCextraFineKmin1e-5_uvTilde_cALE/'
idxDir = odir
meshFile = odir + 'mesh.msh'
meshFile2d = odir + 'mesh2d.msh'
#export_count = 0
export_count = 320
oprefix = 'warner_fine_cALE_snap_'
saveFigures = False
saveFigures = True

Msg = Msg()
model = GModel()
model.load(meshFile)
order = model.getMeshElementByTag(1).getPolynomialOrder()
print('Polynomial order: ' + str(order))
dimension = 3

groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByVerticalLayer(["bottom"])

model2d = GModel()
model2d.load (meshFile2d)
groups2d = dgGroupCollection(model2d, dimension-1, order)

# DOFs
etaDof = dgDofContainer(groups2d, 1)
nnDof = dgDofContainer(groups, 1)
kapvDof = dgDofContainer(groups, 1)
SDof = dgDofContainer(groups, 1)
tkeDof = dgDofContainer(groups, 1)
epsDof = dgDofContainer(groups, 1)
lDof = dgDofContainer(groups, 1)

etaFunc = etaDof.getFunction()
nnFunc = nnDof.getFunction()
kapvFunc = kapvDof.getFunction()
SFunc = SDof.getFunction()
tkeFunc = tkeDof.getFunction()
epsFunc = epsDof.getFunction()
lFunc = lDof.getFunction()

istr = "_{0:05}".format(export_count)
#etaDof.importMsh(   odir + 'eta' + istr)
#etaDof.importMsh(   odir + 'eta2d' + istr)
#nnDof.importMsh(    odir + 'nn'  + istr)
#kapvDof.importMsh(  odir + 'kapv'+ istr)
#SDof.importMsh(     odir + 'S'   + istr)
#tkeDof.importMsh(   odir + 'tke' + istr)
#epsDof.importMsh(   odir + 'eps' + istr)
#lDof.importMsh(     odir + 'l'   + istr)
etaDof.readMsh( idxDir + 'eta2d'+istr+'.idx')
SDof.readMsh(   idxDir + 'S'+istr+'.idx')

def bathFunc(x) :
  return 10*(1.-x/50000.)/2.0 + 5*(1.+x/50000.)/2.0

# horizontal coords
nX = 500
nZ = 200
xs = linspace(-5e4,5e4,nX)
y = 0
Z0 = zeros((nZ,nX)) # static mesh
Z = zeros((nZ,nX))  # moving mesh
X = tile(xs,(nZ,1))
for i,x in enumerate(xs) :
  Z0[:,i] = linspace(-bathFunc(x),0,nZ)

# get eta value (depends onthe mesh!)
etas = zeros(xs.shape)
dgDCMap2d = dataCacheMap(dataCacheMap.POINT_MODE, groups2d)
etaCache = dgDCMap2d.get(etaFunc)
for i,x in enumerate(xs) :
  checkElem = dgDCMap2d.setPoint(groups2d,x,y,0)
  etas[i] = etaCache.get()(0,0)
  Z[:,i] = linspace(-bathFunc(x),etas[i],nZ)

SVals = zeros(Z0.shape)
dgDCMap = dataCacheMap(dataCacheMap.POINT_MODE, groups)
SCache = dgDCMap.get(SFunc)
for i,x in enumerate(xs) :
  for j,z in enumerate(Z0[:,i]) :
    checkElem = dgDCMap.setPoint(groups,x,y,z)
    #minSVal = 1e-15
    #maxSVal = 30 - minSVal
    #SVals[j,i] = min( max( SCache.get()(0,0), minSVal), maxSVal )
    SVals[j,i] = SCache.get()(0,0)

# offset to match Warner et al
xOffset = 5e4
xScale = 1e3
X = (X + xOffset)/xScale
xMin = X.min()
xMax = X.max()
plt.figure(figsize=figSize)
plt.gcf().subplots_adjust(left=0.09, right=0.95, top=0.90, bottom=0.17)
contVals = linspace(-4.999,34.999,9)
plt.contourf(X,Z,SVals,contVals)
cb = plt.colorbar(fraction=0.06, pad=0.03, aspect=10, format='%.0f')
cb.set_label(r'$S$ [$psu$]')
cb.set_clim(0,30)
#CS = plt.contour(X,Z0,SVals,contVals,linewidths=2,linestyles='solid')
#plt.clabel(CS, inline=1, fontsize=10, fmt='%1.0f')
plt.plot([xMin,xMax],[-10,-5],'k-',linewidth=2.0)
plt.ylim(-10,1)
#plt.xlim(-5e4,5e4)
#plt.title(r'$S$ [$psu$]')
plt.ylabel(r'$z$ [$m$]')
plt.xlabel(r'$x$ [$km$]')
plt.xticks(linspace(xMin,xMax,11))
#plt.gca().ticklabel_format(style='sci', scilimits=(1,1),axis='x') # sci style ticks
plt.gca().set_axis_bgcolor('none') # transparent background
plt.grid(True)
cdict = {'red':   ((0.0, 0.8, 0.8),
                   (1.0, 0.3, 0.3)),
         'green': ((0.0, 0.9, 0.9),
                   (1.0, 0.1, 0.1)),
         'blue':  ((0.0, 1.0, 1.0),
                   (1.0, 0.1, 0.1))}
fullGrayCmap = colors.LinearSegmentedColormap('fullGray',cdict,256)
plt.set_cmap(fullGrayCmap)

if saveFigures :
  filename = oprefix +'S' +istr +'.pdf'
  print 'saving to ' +  filename
  plt.savefig(filename)

plt.show()
