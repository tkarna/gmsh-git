#!/usr/bin/env python
# -*- coding: utf-8 -*-
from dgpy import *
from sys import stdout
from shutil import copyfile
import time, math, os
from scipy import *
import matplotlib.pyplot as plt
from matplotlib import rc
from mpl_toolkits.mplot3d import Axes3D

rc('text', usetex=True)
rc('font', family='times', size=12)
figSize = (10,4)

fig = plt.figure(figsize=figSize)
#ax = Axes3D(fig, azim=-125,elev= 45)
#ax = fig.add_subplot(111, projection='3d') # in version 1.0

odir = 'warnerEstuaryAB/'
#odir = 'warnerEstuaryABfine2/'
#odir = 'warnerEstuaryABextraFine/'
meshFile = odir + 'mesh.msh'
#meshFile2d = odir + 'mesh2d.msh'
export_count = 288
oprefix = 'warner_fine_mesh_'
saveFigures = False
#saveFigures = True

Msg = Msg()
model = GModel()
model.load(meshFile)
order = model.getMeshElementByTag(1).getPolynomialOrder()
print('Polynomial order: ' + str(order))
dimension = 3

bbox = model.bounds()
bboxMin = bbox.min()
bboxMax = bbox.max()

groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByVerticalLayer(["bottom"])
groups.buildGroupsOfInterfaces()

def transformCoords(xyz) :
  scalarXY = 1e-3
  offsetX = 5e4
  x = (xyz[0] + offsetX)*scalarXY
  y = xyz[1]*scalarXY
  z = xyz[2]
  return x,y,z
  
def transformSPoint3(pt) :
  x,y,z = transformCoords([pt.x(),pt.y(),pt.z()])
  return SPoint3(x,y,z)

#model2d = GModel()
#model2d.load (meshFile2d)
#groups2d = dgGroupCollection(model2d, dimension-1, order)
#groups2d.buildGroupsOfInterfaces()
def plotElement(e) :
  global bbox
  nedges =  e.getNumEdges()
  for i in range(nedges) :
    edge = e.getEdge(i)
    nv = edge.getNumVertices()
    xs = zeros(nv)
    ys = zeros(nv)
    zs = zeros(nv)
    frontFace = True
    topFace = True
    for j in range(nv) :
      v = edge.getVertex(j)
      (xs[j],ys[j],zs[j]) = transformCoords( [v.x(), v.y(), v.z()] )
      if abs(v.y() - bbox.max().y()) > 1e-6 :
        frontFace = False
      if abs(v.z() - bbox.max().z()) > 1e-6 :
        topFace = False
    lineSpec = 'k-'
    # 3D plot
    #ax.plot(xs,ys,zs,lineSpec)
    if topFace :
      plt.subplot(2,1,1)
      plt.plot(xs,ys,lineSpec)
    if frontFace :
      plt.subplot(2,1,2)
      plt.plot(xs,zs,lineSpec)

bboxTrMin = transformSPoint3(bbox.min())
bboxTrMax = transformSPoint3(bbox.max())

nGroups = groups.getNbElementGroups()
for iG in range(nGroups) :
#for iG in range(1) :
  group = groups.getElementGroup(iG)
  nElem = group.getNbElements()
  for iE in range(nElem) :
    e = group.getElement(iE)
    plotElement(e)

def saveFigure(fieldStr) :
  filename = oprefix + fieldStr +istr +'.pdf'
  print 'saving to ' +  filename
  plt.savefig(filename)

plt.subplot(2,1,1)
plt.gcf().subplots_adjust(left=0.09, right=0.95, top=1.0, bottom=0.17)
plt.gca().set_aspect(2,adjustable='box')
#plt.xlim(-5e4,5e4)
plt.ylim(bboxTrMin.y(),bboxTrMax.y())
plt.xlabel(r'$x$ [km]')
plt.ylabel(r'$y$ [km]')
plt.yticks(linspace(ceil(bboxTrMin.y()),floor(bboxTrMax.y()),3))
plt.minorticks_on()
plt.gca().set_axis_bgcolor('none') # transparent background

plt.subplot(2,1,2)
#plt.gca().set_aspect(2,adjustable='box')
plt.xlabel(r'$x$ [km]')
plt.ylabel(r'$z$ [m]')
plt.minorticks_on()
pos = plt.gca().get_position().bounds # [left, bottom, width, height]
#pos[3] = pos[3] +0.1
plt.gca().set_position([pos[0], pos[1], pos[2], pos[3]+0.1])
plt.gca().set_axis_bgcolor('none') # transparent background

# aspect ratio workaround
#ax.plot([-5e4],[-5e4],[0],'k-')
#ax.plot([-5e4],[+5e4],[0],'k-')
#ax.plot([+5e4],[-5e4],[0],'k-')
#ax.plot([+5e4],[+5e4],[0],'k-')

#ax.set_ylim3d([-5e4,5e4])
#ax.set_xlabel(r'x')
#ax.set_ylabel(r'y')
#ax.set_zlabel(r'z')
#ax.set_ylim3d([-2e3,2e3])

if saveFigures :
  saveFigure('xy_xz')

plt.show()
exit(0)
