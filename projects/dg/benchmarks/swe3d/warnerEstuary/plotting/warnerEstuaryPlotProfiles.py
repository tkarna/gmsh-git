#!/usr/bin/env python
# -*- coding: utf-8 -*-
from dgpy import *
from sys import stdout
from shutil import copyfile
import time, math, os
from scipy import *
import matplotlib.pyplot as plt
from matplotlib import rc

rc('text', usetex=True)
rc('font', family='times', size=12)
figSize = (2,4)

#odir = 'warnerEstuaryABfine/'
#odir = 'warnerEstuaryABextraFine/'
#odir = 'warnerEstuaryPCextraFine/'
odir = 'warnerEstuaryPCextraFineKmin1e-5_uvTilde_cALE/'
meshFile = odir + 'mesh.msh'
meshFile2d = odir + 'mesh2d.msh'
export_count = 288
#oprefix = 'warner_fine_cALE_prof_'
oprefix = 'warner_fine_cALE_prof_'
saveFigures = False
#saveFigures = True
nLevels = 20

Msg = Msg()
model = GModel()
model.load(meshFile)
order = model.getMeshElementByTag(1).getPolynomialOrder()
print('Polynomial order: ' + str(order))
dimension = 3

groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByVerticalLayer(["bottom"])

model2d = GModel()
model2d.load (meshFile2d)
groups2d = dgGroupCollection(model2d, dimension-1, order)

# DOFs
etaDof = dgDofContainer(groups2d, 1)
nnDof = dgDofContainer(groups, 1)
kapvDof = dgDofContainer(groups, 1)
SDof = dgDofContainer(groups, 1)
tkeDof = dgDofContainer(groups, 1)
epsDof = dgDofContainer(groups, 1)
lDof = dgDofContainer(groups, 1)

etaFunc = etaDof.getFunction()
nnFunc = nnDof.getFunction()
kapvFunc = kapvDof.getFunction()
SFunc = SDof.getFunction()
tkeFunc = tkeDof.getFunction()
epsFunc = epsDof.getFunction()
lFunc = lDof.getFunction()

istr = "_{0:05}".format(export_count)
#etaDof.importMsh(   odir + 'eta' + istr)
etaDof.importMsh(   odir + 'eta2d' + istr)
nnDof.importMsh(    odir + 'nn'  + istr)
#kapvDof.importMsh(  odir + 'kapv'+ istr)
kapvDof.importMsh(  odir + 'kappav'+ istr)
SDof.importMsh(     odir + 'S'   + istr)
tkeDof.importMsh(   odir + 'tke' + istr)
epsDof.importMsh(   odir + 'eps' + istr)
lDof.importMsh(     odir + 'l'   + istr)

# horizontal coords
x = -20000
y = 0
nZ = 150
# get eta value (depends onthe mesh!)
etaVal0 = 0.0
dgDCMap2d = dataCacheMap(dataCacheMap.POINT_MODE, groups2d)
etaCache = dgDCMap2d.get(etaFunc)
checkElem = dgDCMap2d.setPoint(groups2d,x,y,0)
etaVal = etaCache.get()(0,0)
bathVal = 10*(1.-x/50000.)/2.0 + 5*(1.+x/50000.)/2.0
bathTKEVal = bathVal - bathVal/nLevels # special value for TKE and eps
zVals = linspace(-bathVal,etaVal0,nZ) # for static mesh
zTKEVals = linspace(-bathTKEVal,etaVal0,nZ) # for static mesh
#print etaVal, zVals

nnVals = zeros(size(zVals))
kapvVals = zeros(size(zVals))
SVals = zeros(size(zVals))
tkeVals = zeros(size(zVals))
epsVals = zeros(size(zVals))
lVals = zeros(size(zVals))
dgDCMap = dataCacheMap(dataCacheMap.POINT_MODE, groups)
nnCache = dgDCMap.get(nnFunc)
kapvCache = dgDCMap.get(kapvFunc)
SCache = dgDCMap.get(SFunc)
tkeCache = dgDCMap.get(tkeFunc)
epsCache = dgDCMap.get(epsFunc)
lCache = dgDCMap.get(lFunc)
for i in range(size(zVals)) :
  checkElem = dgDCMap.setPoint(groups,x,y,zVals[i])
  nnVals[i] = nnCache.get()(0,0)
  kapvVals[i] = kapvCache.get()(0,0)
  SVals[i] = SCache.get()(0,0)
  lVals[i] = lCache.get()(0,0)
  checkElem = dgDCMap.setPoint(groups,x,y,zTKEVals[i])
  tkeVals[i] = tkeCache.get()(0,0)
  epsVals[i] = epsCache.get()(0,0)

zVals = linspace(-bathVal,etaVal,nZ) # correction for moving mesh
zTKEVals = linspace(-bathTKEVal,etaVal,nZ) # correction for moving mesh

def saveFigure(fieldStr) :
  filename = oprefix + fieldStr +istr +'.pdf'
  print 'saving to ' +  filename
  plt.savefig(filename)


abcX, abcY = (0.06,0.91)
abcFontSize = 14
plt.figure(figsize=figSize)
plt.gcf().subplots_adjust(left=0.25)
plt.plot(SVals,zVals,'k')
plt.title(r'$S$ [$psu$]')
plt.ylabel(r'$z$ [$m$]')
xt, xt_lab = plt.xticks()
#plt.xticks(linspace(xt[0],xt[-1],3))
plt.xticks([0,10,20])
plt.grid(True)
plt.figtext(abcX, abcY,'a)',fontsize=abcFontSize)
if saveFigures :
  saveFigure('S')

plt.figure(figsize=figSize)
plt.gcf().subplots_adjust(left=0.25)
plt.plot(tkeVals,zTKEVals,'k')
plt.title(r'TKE [$m^2s^{-2}$]')
plt.ylabel(r'$z$ [$m$]')
xt, xt_lab = plt.xticks()
#plt.xticks(linspace(xt[0],xt[-1],3))
plt.xticks([0,1e-3,2e-3])
plt.gca().ticklabel_format(style='sci', scilimits=(1,1),axis='x') # sci style ticks
plt.grid(True)
plt.figtext(abcX, abcY,'b)',fontsize=abcFontSize)
if saveFigures :
  saveFigure('tke')

plt.figure(figsize=figSize)
plt.gcf().subplots_adjust(left=0.25)
plt.plot(epsVals,zTKEVals,'k')
plt.title(r'$\varepsilon$ [$m^2s^{-3}$]')
plt.ylabel(r'$z$ [$m$]')
plt.xlim(0,1e-4) # custom limits
xt, xt_lab = plt.xticks()
#plt.xticks(linspace(xt[0],xt[-1],3))
plt.xticks([0,5e-5,1e-4])
plt.gca().ticklabel_format(style='sci', scilimits=(1,1),axis='x') # sci style ticks
plt.grid(True)
plt.figtext(abcX, abcY,'c)',fontsize=abcFontSize)
if saveFigures :
  saveFigure('eps')

plt.figure(figsize=figSize)
plt.gcf().subplots_adjust(left=0.25)
plt.plot(lVals,zVals,'k')
plt.title(r'$l$ [$m$]')
plt.ylabel(r'$z$ [$m$]')
xt, xt_lab = plt.xticks()
#plt.xticks(linspace(xt[0],xt[-1],3))
plt.xticks([0,0.5,1])
plt.grid(True)
plt.figtext(abcX, abcY,'d)',fontsize=abcFontSize)
if saveFigures :
  saveFigure('l')

plt.figure(figsize=figSize)
plt.gcf().subplots_adjust(left=0.25)
plt.plot(kapvVals,zVals,'k')
print max(kapvVals)
plt.title(r'$\kappa_V$ [$m^2s^{-1}$]')
plt.ylabel(r'$z$ [$m$]')
xt, xt_lab = plt.xticks()
#plt.xticks(linspace(xt[0],xt[-1],3))
plt.xticks([0,0.01,0.02])
plt.grid(True)
plt.figtext(abcX, abcY,'e)',fontsize=abcFontSize)
if saveFigures :
  saveFigure('kapv')

plt.show()
