#!/usr/bin/env python
# -*- coding: utf-8 -*-
from dgpy import *
from sys import stdout
from shutil import copyfile
import time, math, os
import gmshPartition
import extrude

# .-------------------------------.
# |          user input           |
# '-------------------------------'
meshFile = 'warnerEstuary2d.msh'
nbLayers = 15 #20 # nb sigma levels

# automatic partition if not *_part_n.msh exists
meshFile = gmshPartition.simple(meshFile)
mesh3dFile = meshFile.replace('2d','3d')

# if 3d mesh missing or outdated
if not os.path.exists(mesh3dFile) or os.stat(mesh3dFile).st_mtime < os.stat(meshFile).st_mtime :
  if Msg.GetCommRank() == 0 :
    # create extruded mesh
    Msg.Info('Extruding 3d mesh')
    hSea = 10
    hRiver = 5
    def getLayersSigma (e, v) :
      h = hSea * (1-v[0]/50000.)/2. + hRiver *(1+v[0]/50000.)/2.
      return [z * h /nbLayers for z in  range(nbLayers + 1)]
    mesh2dObj = extrude.mesh(meshFile)
    mesh3dObj = extrude.extrude(mesh2dObj, getLayersSigma)
    Msg.Info('Writing extruded mesh \''+mesh3dFile+'\'')
    mesh3dObj.write(mesh3dFile)

outputDirectory = 'outputs/'
#outputDirectory = 'warnerEstuaryPCextraFine/'
# create outputdir if !exists
if Msg.GetCommRank()==0 and not os.path.exists(outputDirectory) :
  os.mkdir(outputDirectory)
Msg.Barrier()
copyfile(mesh3dFile,outputDirectory + '/mesh.msh')

# .-------------------------------.
# |         create solver         |
# '-------------------------------'
s = slim3dSolver(mesh3dFile, ['bottom_volume'], ['top_volume'])
# options
s.setSolveS(1)
s.setSolveSImplicitVerticalDiffusion(1)
s.setSolveUVImplicitVerticalDiffusion(1)
s.setSolveTurbulence(1)
s.setAdvectTurbulentEnergy(1)
s.setComputeBottomFriction(1)
#s.setUseModeSplit(0)
s.exportDirectory = outputDirectory
s.turbulenceSetupFile = 'warnerEstuaryTurb.nml'
s.setUseConservativeALE(1)

s.setFlagUVLimiter(1)
s.setUseOptimalLimiter(0)

# .-------------------------------.
# |         set functions         |
# '-------------------------------'
f = s.functions
warnerLib='libWarnerEstuary.so'
if Msg.GetCommRank()==0:
  functionC.buildLibraryFromFile ('warnerEstuaryFunctions.cc', warnerLib);
Msg.Barrier()

f.bathFunc2d = functionC(warnerLib,'bath',1,[f.xyzFunc2d])
f.bathFunc3d = functionC(warnerLib,'bath',1,[f.xyzFunc3d])
f.bathGradFunc2d = functionC(warnerLib,'bathGrad',3,[f.xyzFunc2d])
f.bathGradFunc3d = functionC(warnerLib,'bathGrad',3,[f.xyzFunc3d])

f.TInitFunc = functionConstant(10)
f.SInitFunc = functionC(warnerLib,'initS',1,[f.xyzFunc3d])

#slim3dParameters.rho0 = 1027-15.62288322703 # middle
slim3dParameters.rho0 = 1000 # Warner et al. 2005

f.z0BFunc = functionConstant(0.005)
f.z0SFunc = functionConstant(0.02)

# .-------------------------------.
# |       create equations        |
# '-------------------------------'
e = s.createEquations()
d = s.getDofs()

eta2d = d.etaDof2d.getFunction()
uvAv2d = d.uvAvDof2d.getFunction()
eta2dCG = d.etaDof2dCG.getFunction()

# forcings
initUVFunc2d = f.zeroFunc3 #functionC(warnerLib,'initialUV',2,[timeFunc,xyzFunc2d,etaDof2dCG.getFunction(),bathFunc2d])
tideUVFuncAv2d = functionC(warnerLib,'tideUVAv',2,[f.timeFunc,f.xyzFunc2d,eta2dCG,f.bathFunc2d])

riverUVFuncAv2d = functionC(warnerLib,'riverUVAv',2,[f.timeFunc,f.xyzFunc2d,f.zeroFunc,f.bathFunc2d])

horMomEq = e.horMomEq
horMomEq.setLaxFriedrichsFactor(0.0)
#horMomBndSides = horMomEq.newBoundaryWallLinear(0.1)
horMomBndSides = horMomEq.newBoundaryWall()
horMomBndZero = horMomEq.new0FluxBoundary()
horMomBndSymm = horMomEq.newSymmetryBoundary('')
horMomBndWall = horMomEq.newBoundaryWall()
horMomEq.addBoundaryCondition('bottom_volume',horMomBndWall)
horMomEq.addBoundaryCondition('sideL',horMomBndSides)
horMomEq.addBoundaryCondition('river',horMomBndSymm)
horMomEq.addBoundaryCondition('sideR',horMomBndSides)
horMomEq.addBoundaryCondition('sea',horMomBndSymm)
horMomEq.addBoundaryCondition('top_volume',horMomBndSymm)
horMomEq.addBoundaryCondition('vertical_bottom', horMomBndWall)

if s.getSolveUVImplicitVerticalDiffusion() :
  vertMomUEq = e.vertMomUEq
  vertMomUBndZero = vertMomUEq.new0FluxBoundary()
  vertMomUBndBottom = vertMomUEq.newBoundaryBottom(f.bottomFrictionFunc,f.zBotDistFunc)
  vertMomUBndSymm = vertMomUEq.newSymmetryBoundary('')
  vertMomUEq.addBoundaryCondition('top_volume',vertMomUBndZero)
  vertMomUEq.addBoundaryCondition('bottom_volume',vertMomUBndBottom)
  vertMomUEq.addBoundaryCondition('sideL',vertMomUBndZero)
  vertMomUEq.addBoundaryCondition('river',vertMomUBndZero)
  vertMomUEq.addBoundaryCondition('sideR',vertMomUBndZero)
  vertMomUEq.addBoundaryCondition('sea',vertMomUBndZero)
  vertMomUEq.addBoundaryCondition('vertical_bottom', vertMomUBndZero)

wEq = e.wEq
wBndZero = wEq.new0FluxBoundary()
wBndSymm = wEq.newSymmetryBoundary('')
wEq.addBoundaryCondition('sideL',wBndZero)
wEq.addBoundaryCondition('river',wBndSymm)
wEq.addBoundaryCondition('sideR',wBndZero)
wEq.addBoundaryCondition('sea',wBndSymm)
wEq.addBoundaryCondition('top_volume',wBndSymm) # must be symm!!
wEq.addBoundaryCondition('bottom_volume',wBndZero)
wEq.addBoundaryCondition('vertical_bottom',wBndZero)

SEq = e.SEq
SEq.setLaxFriedrichsFactor(0.0)
SBndZero = SEq.new0FluxBoundary()
SBndSymm = SEq.newSymmetryBoundary('')
SBndOpen = SEq.newRelaxationBoundary(f.SInitFunc, 3*3600)
SEq.addBoundaryCondition('sideL',SBndZero)
SEq.addBoundaryCondition('river',SBndOpen)
SEq.addBoundaryCondition('sideR',SBndZero)
SEq.addBoundaryCondition('sea',SBndOpen)
SEq.addBoundaryCondition('top_volume',SBndZero) # zero works for correct wMesh!
SEq.addBoundaryCondition('bottom_volume',SBndZero)
SEq.addBoundaryCondition('vertical_bottom',SBndZero)

if s.getAdvectTurbulentEnergy() :
  tkeAdvEq = e.tkeAdvEq
  tkeAdvEq.setLaxFriedrichsFactor(0.0)
  tkeAdvBndZero = tkeAdvEq.new0FluxBoundary()
  tkeAdvBndSymm = tkeAdvEq.newSymmetryBoundary('')
  tkeAdvBndOpen = tkeAdvEq.newInFluxBoundary(f.tinyFunc)
  tkeAdvEq.addBoundaryCondition('sideL',tkeAdvBndZero)
  tkeAdvEq.addBoundaryCondition('river',tkeAdvBndOpen)
  tkeAdvEq.addBoundaryCondition('sideR',tkeAdvBndZero)
  tkeAdvEq.addBoundaryCondition('sea',tkeAdvBndOpen)
  tkeAdvEq.addBoundaryCondition('top_volume',tkeAdvBndZero)
  tkeAdvEq.addBoundaryCondition('bottom_volume',tkeAdvBndZero)
  tkeAdvEq.addBoundaryCondition('vertical_bottom',tkeAdvBndZero)

  epsAdvEq = e.epsAdvEq
  epsAdvEq.setLaxFriedrichsFactor(0.0)
  epsAdvBndZero = epsAdvEq.new0FluxBoundary()
  epsAdvBndSymm = epsAdvEq.newSymmetryBoundary('')
  epsAdvBndOpen = epsAdvEq.newInFluxBoundary(f.tinyFunc)
  epsAdvEq.addBoundaryCondition('sideL',epsAdvBndZero)
  epsAdvEq.addBoundaryCondition('river',epsAdvBndOpen)
  epsAdvEq.addBoundaryCondition('sideR',epsAdvBndZero)
  epsAdvEq.addBoundaryCondition('sea',epsAdvBndOpen)
  epsAdvEq.addBoundaryCondition('top_volume',epsAdvBndZero)
  epsAdvEq.addBoundaryCondition('bottom_volume',epsAdvBndZero)
  epsAdvEq.addBoundaryCondition('vertical_bottom',epsAdvBndZero)

eta2dEq = e.eta2dEq
etaBndZero = eta2dEq.new0FluxBoundary()
#etaTideUVEta = eta2dEq.newOutsideValueBoundaryGeneric('',[uvAv2d,function.getSolution()],[tideUVFuncAv2d,eta2dCG])
#etaRiverUVEta = eta2dEq.newOutsideValueBoundaryGeneric('',[uvAv2d,function.getSolution()],[riverUVFuncAv2d,f.zeroFunc])
#etaTideUVEta = eta2dEq.newOutsideValueBoundaryGeneric('',[uvAv2d,function.getSolution()],[tideUVFuncAv2d,eta2dCG])
#etaRiverUVEta = eta2dEq.newOutsideValueBoundaryGeneric('',[uvAv2d,function.getSolution()],[riverUVFuncAv2d,f.zeroFunc])
eta2dEq.addBoundaryCondition('sideL',etaBndZero)
eta2dEq.addBoundaryCondition('river',etaBndZero)
eta2dEq.addBoundaryCondition('sideR',etaBndZero)
eta2dEq.addBoundaryCondition('sea',etaBndZero)

uv2dEq = e.uv2dEq
#uv2dTideUVEta = uv2dEq.newOutsideValueBoundaryGeneric('',[function.getSolution(),eta2d],[tideUVFuncAv2d,eta2dCG])
#uv2dRiverUVEta = uv2dEq.newOutsideValueBoundaryGeneric('',[function.getSolution(),eta2d],[riverUVFuncAv2d,f.zeroFunc])
#uv2dTideUVEta = uv2dEq.newOutsideValueBoundaryGeneric('',[function.getSolution(),eta2d],[tideUVFuncAv2d,eta2dCG])
#uv2dRiverUVEta = uv2dEq.newOutsideValueBoundaryGeneric('',[function.getSolution(),eta2d],[riverUVFuncAv2d,f.zeroFunc])
uv2dBndWall = uv2dEq.newBoundaryWall()
uv2dEq.addBoundaryCondition('sideL',uv2dBndWall)
uv2dEq.addBoundaryCondition('river',uv2dBndWall)
uv2dEq.addBoundaryCondition('sideR',uv2dBndWall)
uv2dEq.addBoundaryCondition('sea',uv2dBndWall)

# .-------------------------------
# |     setup time integrator     |
# '-------------------------------'
timeIntegrator = slim3dTimeIntegratorPC(s)
timer=dgTimer.root()

rk = dgERK(e.eta2dEq, None, DG_ERK_EULER)
dtRK = rk.computeInvSpectralRadius(d.etaDof2d)
CFL = 0.5
dt = CFL*dtRK
if Msg.GetCommRank() == 0 :
  print('Runge-Kutta dt :' + str(dtRK) + ' CFL:' + str(CFL) + ' dt:' + str(dt))
# try to estimate dt3d
maxUV = 1.5 # max advective velocity
dt3d = dtRK * math.sqrt(9.81*10) / maxUV
M = int(0.72*dt3d/dt)
#M = 20
if Msg.GetCommRank() == 0 :
  print('Mode split ratio M :' + str(M))

timeIntegrator.setTimeStep(dt)
timeIntegrator.set3dTimeStepRatio(M)
timeIntegrator.setEndTime(400*5*20*43.2)

# .-------------------------------.
# |     setup export options      |
# '-------------------------------'
timeIntegrator.setExportInterval(5*20*43.2)
timeIntegrator.setExportAtEveryTimeStep(False)
timeIntegrator.setFullExportAtEveryTimeStep(False)

exporters = []
exporters.append(dgIdxExporter(d.SDof, outputDirectory + '/S'))
exporters.append(dgIdxExporter(d.uvDof, outputDirectory + '/uv'))
exporters.append(dgIdxExporter(d.uvAvDof2d, outputDirectory + '/uvAv'))
exporters.append(dgIdxExporter(d.etaDof2d, outputDirectory + '/eta'))
if s.getSolveTurbulence() :
  exporters.append(dgIdxExporter(d.nuvDof, outputDirectory + '/nuv'))
  exporters.append(dgIdxExporter(d.tkeDof, outputDirectory + '/tke'))
  exporters.append(dgIdxExporter(d.epsDof, outputDirectory + '/eps'))

# .--------------------.
# |     main loop      |
# '--------------------'
tic = time.time()
def export() :
    Msg.Info('Export %i' % timeIntegrator.getExportCount())
    for e in exporters :
      e.exportIdx(timeIntegrator.getExportCount(), timeIntegrator.getTime())
    dateStr = time.strftime('%Y-%b-%d %H:%M:%S',time.gmtime(timeIntegrator.getTime()))
    Msg.Info('%5i [ %s ] eta %14.8e uv %14.8e %.1fs' % (timeIntegrator.getIter(),dateStr, d.etaDof2d.norm(), d.uvDof.norm(), time.time() - tic))
timer=dgTimer.root()
timer.start()
timeIntegrator.initializeFields()
export()

while timeIntegrator.getTime() < timeIntegrator.getEndTime() :
  timeIntegrator.advanceOneTimeStep()
  timeIntegrator.checkSanity()
  if timeIntegrator.checkExport() :
    export()
    timeIntegrator.checkTracerConsistency()
    tic = time.time()
  if timeIntegrator.checkFullExport() :
    timeIntegrator.exportAll()
if Msg.GetCommRank() == 0 :
  print 'Number of cores',Msg.GetCommSize()
timer.printFull()
timeIntegrator.terminate(0)
