#!/usr/bin/env python
# -*- coding: utf-8 -*-
from gmshpy import *
from dgpy import *
from sys import stdout
import time, math
GmshInitialize()

from extrude import *

mmodel = GModel()
mmodel.load("tracerBox2d.geo")
mmodel.load("tracerBox2d.msh")
mesh_order = mmodel.getMeshElementByTag(1).getPolynomialOrder()
order = mesh_order

depth = 2000
nbLayers = 2

def sigma (x, y, i, nlayers) :
#  return -depth*i/nlayers * (1 + 0.8*math.sin(2*math.pi*(math.sqrt(x*x+y*y)-5e3)/2/1e4))
#  return -depth*i/nlayers * (1 + 0.8*math.exp(-math.sqrt(x*x+y*y)/2/3e2))
  return -depth*i/nlayers * (1 + 0.8*math.atan((math.sqrt(x*x+y*y)-5e3)/2/3e1)*2/math.pi)
  #return -depth*i/nlayers * (1 + 0.2*math.sin(2*math.pi*(x)/2/1e4))
  #return -depth*i/nlayers * (1 + x/1e4)
  #return -depth*i/nlayers
def zCoord (x, y, i, nlayers) :
  bottom = -depth * (1 + 0.2*math.sin(2*math.pi*(x)/2/1e4))
  #bottom = -depth * (1 + x/1e4)
  step = 8
  z = -step*i
  if z < bottom or i == nlayers :
   z = bottom
  return z

physicalNames = ['bottom','volume','side1','side2','side3','side4']
extrudeMesh(mmodel,sigma,nbLayers,order,physicalNames).save("tracerBox.msh")
