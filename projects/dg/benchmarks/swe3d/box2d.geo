//*********** box2d.geo *************//
Cx = 5000;
Cy = 500; // coarse narrow
lc = 500;
L = 5;
Lup = 0;

Point(1) = {-Cx, -Cy, 0, lc};
Point(2) = {Cx , -Cy, 0, lc};
Point(3) = {Cx , Cy , 0, lc};
Point(4) = {-Cx, Cy , 0, lc};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};

Physical Line("sideL") = {1};
Physical Line("river") = {2};
Physical Line("sideR") = {3};
Physical Line("sea") = {4};

Line Loop(5) = {1,2,3,4};
Plane Surface(6) = {5};
Physical Surface("top") = {6};

