#!/usr/bin/env rundgpy

import os, extrude

#onelab section 


nLayers = 8
nPartition = 1
clscale = 3

# mesh
os.system("gmsh -2 dome2d.geo -clscale %g -part %i" % (clscale, nPartition))

# random solver code (in this case extrusion of the mesh)
def getLayers (e, v) :
  x, y, z = v[0:3]
  h = 3600 if y < 300e3 else 600 if y > 600e3 else 600 + (600e3 - y) / 100
  return [z * float(h) / nLayers for z in  range(nLayers + 1)]

mesh3d = extrude.extrude(extrude.mesh("dome2d.msh"), getLayers)

mesh3d.write("dome.msh")

for i, v in enumerate(mesh3d.vertices) :
  mesh3d.vertices[i] = (v[0], v[1], v[2] * 100, v[3])

mesh3d.write("domeZ.msh")

