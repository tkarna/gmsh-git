LX = 1100e3;
LY = 600e3;
YBAY = 800e3;
YSLOPE = 300e3;
XBAY1 = 800e3;
XBAY2 = 900e3;

LC = 30e3;
LCBAY = 11e3;

Point(1) = {0, 0, 0, LC};
Point(2) = {LX, 0, 0, LC};
Point(3) = {LX, YSLOPE, 0, LC};
Point(4) = {LX, LY, 0, LC};
Point(5) = {XBAY2, LY, 0, LCBAY};
Point(6) = {XBAY2, YBAY, 0, LCBAY};
Point(7) = {XBAY1, YBAY, 0, LCBAY};
Point(8) = {XBAY1, LY, 0, LCBAY};
Point(9) = {0, LY, 0, LC};
Point(10) = {0, YSLOPE, 0, LC};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 5};
Line(5) = {5, 6};
Line(6) = {6, 7};
Line(7) = {7, 8};
Line(8) = {8, 9};
Line(9) = {9, 10};
Line(10) = {10, 1};

Line(11) = {5, 8};
Line(12) = {3, 10};

Line Loop(1) = {7, -11, 5, 6};
Plane Surface(1) = {1};
Line Loop(2) = {8, 9, -12, 3, 4, 11};
Plane Surface(2) = {2};
Line Loop(3) = {2, 12, 10, 1};
Plane Surface(3) = {3};

Mesh.CharacteristicLengthFromPoints = 1;
Mesh.CharacteristicLengthExtendFromBoundary = 1;

Physical Surface("Sea") = {1, 2, 3};
Physical Line("River") = {6};
Physical Line("Wall") = {1:5, 7:10};
