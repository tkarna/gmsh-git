#include "fullMatrix.h"
#include "function.h"

extern "C" {
void riverEta(dataCacheMap *d, fullMatrix<double> &val, fullMatrix<double> &xyz)
{
  for (int i = 0; i < val.size1(); ++i) {
    val(i, 0) = -2.54945951458e-2 + 5.09891902915e-7 * (xyz(i, 0)-800000);
  }
}

void riveruv(dataCacheMap *d, fullMatrix<double> &val, fullMatrix<double> &xyz)
{
  for (int i = 0; i < val.size1(); ++i) {
    double zlim = -300 + 25 - 0.0005 * (xyz(i, 0) - 8e5);
    val(i, 0) = 0;
    val(i, 1) = xyz(i, 2) < zlim ? - 0.5 : 0.5;
  }
}
void riverT(dataCacheMap *d, fullMatrix<double> &val, fullMatrix<double> &xyz)
{
  for (int i = 0; i < val.size1(); ++i) {
    double zlim = -300 + 25 - 0.0005 * (xyz(i, 0) - 8e5);
    val(i, 0) = xyz(i, 2) < zlim ? 0 : 10;
  }
}
}
