#!/usr/bin/env python
# -*- coding: utf-8 -*-
from dgpy import *
import math, os

meshFile = 'dome.msh'

odir = 'output'
if Msg.GetCommRank()==0 and not os.path.exists(odir) :
  os.mkdir(odir)
if Msg.GetCommRank()==0:
  functionC.buildLibraryFromFile ("libdome.cc", "libdome.so");
Msg.Barrier()

s = slim3dSolver(meshFile,["bottom_Sea"],["top_Sea"])
s.setSolveS(0)
s.setSolveT(1)
s.setSolveTImplicitVerticalDiffusion(0)
s.setSolveUVImplicitVerticalDiffusion(0)
s.setSolveTurbulence(0)
s.setAdvectTurbulentEnergy(0)
s.setComputeBottomFriction(0)
s.setFlagUVLimiter(1)
s.setUseOptimalLimiter(1)
s.setUseModeSplit(1)
s.exportDirectory = odir

def tinit(t, xyz) :
  for i in range(xyz.size1()) :
    ti = 0 if xyz(i, 1) > 650e3 and xyz(i, 2) < -300 else 10.
    t.set(i, 0, ti)

rho = 1001.85
slim3dParameters.rho0 = rho
f = s.functions
d = s.getDofs(True)
f.TInitFunc = functionPython(1, tinit, [function.getCoordinates()])
f.coriolisFunc = functionConstant(1e-4)
f.rhoFunc = linearStateEquation(d.TDof.getFunction(), -rho*1.94552591e-4, 10)
f.nuhTotal = domeSmagorinsky(0.01, 50, d.uvDof.getFunctionGradient(), s.columnInfo)

f.nuvFunc = pacanowskiPhilander(d.uvDof.getFunctionGradient(), d.rhoDof.getFunctionGradient(), 0.05, 2e-5, 10)

f.kappavFunc = functionConstant(1e-4)
f.kappahTotal = functionConstant(10)


# .-------------------------------.
# |       create equations        |
# '-------------------------------'
e = s.createEquations()

eta2d = d.etaDof2d.getFunction()
eta3d = d.etaDof3d.getFunction()
uvAv2d = d.uvAvDof2d.getFunction()


horMomEq = e.horMomEq
horMomEq.setLaxFriedrichsFactor(0.0)
# TODO: wall boundary gives better results except explodes when front reaches the ends
#   -> generalize Wall cond ?
horMomBndSides = horMomEq.newBoundaryWallLinear(0.5)
#horMomBndSides = horMomEq.newBoundaryWall() # this is bad
horMomBndZero = horMomEq.new0FluxBoundary()
horMomBndSymm = horMomEq.newSymmetryBoundary('')
horMomBndWall = horMomEq.newBoundaryWall()
horMomEq.addBoundaryCondition('bottom_Sea',horMomBndWall) # must be wall for bath!
horMomEq.addBoundaryCondition('River',horMomBndSides)
horMomEq.addBoundaryCondition('Wall',horMomBndSides)
horMomEq.addBoundaryCondition('top_Sea',horMomBndSymm)

wEq = e.wEq
wBndZero = wEq.new0FluxBoundary()
wBndSymm = wEq.newSymmetryBoundary('')
uvRiver = functionC("libdome.so", "riveruv", 2, [f.xyzFunc3d])
wBndRiver = wEq.newOutsideValueBoundary("", uvRiver)
wEq.addBoundaryCondition('River',wBndRiver)
wEq.addBoundaryCondition('Wall',wBndZero)
wEq.addBoundaryCondition('top_Sea',wBndSymm) # must be symm!!
wEq.addBoundaryCondition('bottom_Sea',wBndZero)

TEq = e.TEq
TEq.setLaxFriedrichsFactor(0.0)
TBndZero = TEq.new0FluxBoundary()
TOut = functionC("libdome.so", "riverT", 1, [f.xyzFunc3d])
TBndRiver = TEq.newOutsideValueBoundary("", TOut)
TEq.addBoundaryCondition('River',TBndRiver)
TEq.addBoundaryCondition('Wall',TBndZero)
TEq.addBoundaryCondition('top_Sea',TBndZero)
TEq.addBoundaryCondition('bottom_Sea',TBndZero)

eta2d = d.etaDof2d.getFunction()
uvAv2d = d.uvAvDof2d.getFunction()

eta2dEq = e.eta2dEq
etaBndZero = eta2dEq.new0FluxBoundary()
uv2dRiver = functionConstant([0, 0])
etaRiver = functionC("libdome.so",'riverEta',1,[f.xyzFunc2d])
bndRiverEta = eta2dEq.newOutsideValueBoundaryGeneric('',[uvAv2d,function.getSolution()],[uv2dRiver,etaRiver])
eta2dEq.addBoundaryCondition('Wall',etaBndZero)
#eta2dEq.addBoundaryCondition('River',etaBndZero)
eta2dEq.addBoundaryCondition('River',bndRiverEta)

uv2dEq = e.uv2dEq
uv2dBndWall = uv2dEq.newBoundaryWall()
bndRiverUV = uv2dEq.newOutsideValueBoundaryGeneric('',[function.getSolution(),eta2d],[uv2dRiver,etaRiver])
uv2dEq.addBoundaryCondition('River',bndRiverUV)
#uv2dEq.addBoundaryCondition('River',uv2dBndWall)
uv2dEq.addBoundaryCondition('Wall',uv2dBndWall)

exp = s.exporter
exp.addDofContainer(d.rhoDof)
exp.addDofContainer(d.TDof)
exp.addDofContainer(d.rDof3d)
exp.addDofContainer(d.rGradDof3d)
exp.addDofContainer(d.uvAvDof2d)
#exp.setExportRho(1)

# .-------------------------------.
# |     setup time integrator     |
# '-------------------------------'
t = slim3dTimeIntegratorPC(s)
s.timeIntegrator = t

rk = dgERK(e.eta2dEq, None, DG_ERK_EULER)
dtRK = rk.computeInvSpectralRadius(d.etaDof2d)
CFL = 1
dt = CFL*dtRK
if Msg.GetCommRank() == 0 :
  print('Runge-Kutta dt :' + str(dtRK) + ' CFL:' + str(CFL) + ' dt:' + str(dt))
# try to estimate dt3d
maxUV = 0.9 # max advective velocity
dt3d = dtRK * math.sqrt(9.81*10) / maxUV
M = int(0.72*dt3d/dt)
M = 15
if Msg.GetCommRank() == 0 :
  print('Mode split ratio M :' + str(M))

s.timeIntegrator.setTimeStep(dt)
s.timeIntegrator.set3dTimeStepRatio(M)
s.timeIntegrator.setExportInterval(3600)
s.timeIntegrator.setEndTime(30 * 24*3600)
#s.timeIntegrator.setExportAtEveryTimeStep(True)
s.timeIntegrator.initializeFields()

s.timeIntegrator.iterate()
