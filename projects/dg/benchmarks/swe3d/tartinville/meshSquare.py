from extrude import *
import sys, os

nPartition = 1

os.system("gmsh -2 squareSponge.geo -part %i" % (nPartition))

mesh2d = "squareSponge.msh"
mesh3d = "squareSponge_20layers.msh"

h = 20
nbLayers = 20

#zLayers = [0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000]

def getLayers(e, v) :
  return [z * h /nbLayers for z in  range(nbLayers + 1)]

extrude(mesh(mesh2d), getLayers).write(mesh3d)

