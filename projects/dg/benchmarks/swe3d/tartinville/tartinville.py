#!/usr/bin/env python
# -*- coding: utf-8 -*-
from dgpy import *
from shutil import copyfile
import math, os, inspect

meshFile = 'square_20layers.msh'

odir = 'output'
if Msg.GetCommRank()==0 and not os.path.exists(odir) :
  os.mkdir(odir)
copyfile(meshFile,odir + '/mesh.msh')
pythonFile = inspect.getfile(inspect.currentframe())
copyfile(pythonFile,odir + '/' + pythonFile)

s = slim3dSolver(meshFile,["bottom_Sea"],["top_Sea"])
s.setSolveS(1)
s.setSolveT(0)
s.setSolveSImplicitVerticalDiffusion(0)
s.setSolveUVImplicitVerticalDiffusion(0)
s.setSolveTurbulence(0)
s.setAdvectTurbulentEnergy(0)
s.setComputeBottomFriction(0)
s.setFlagUVLimiter(1)
s.setUseOptimalLimiter(1)
s.setUseModeSplit(1)
s.exportDirectory = odir

def sinit(s, xyz) :
  for i in range(xyz.size1()) :
    d = math.sqrt(xyz(i,0) * xyz(i,0) + xyz(i,1) * xyz(i,1))
    S = 33.75 + 1.1 * math.pow(d/3000., 8)
    si = S if xyz(i, 2) > -10 and d < 3e3 else 34.85
    s.set(i, 0, si)

slim3dParameters.rho0 = 1025
f = s.functions
d = s.getDofs(True)
f.SInitFunc = functionPython(1, sinit, [function.getCoordinates()])
f.coriolisFunc = functionConstant(1.15e-4)
f.rhoFunc = linearStateEquation(d.SDof.getFunction(), 0.78, 33.75)

# .-------------------------------.
# |       create equations        |
# '-------------------------------'
e = s.createEquations()

eta2d = d.etaDof2d.getFunction()
eta3d = d.etaDof3d.getFunction()
uvAv2d = d.uvAvDof2d.getFunction()
eta0 = functionConstant(0)

horMomEq = e.horMomEq
horMomEq.setLaxFriedrichsFactor(0.0)
#horMomBndSides = horMomEq.newBoundaryWallLinear(0.5)
#horMomBndSides = horMomEq.newBoundaryWall() # this is bad
#horMomBndZero = horMomEq.new0FluxBoundary()
horMomBndSymm = horMomEq.newSymmetryBoundary('')
horMomBndWall = horMomEq.newBoundaryWall()
horMomBndOpen = horMomEq.newOutsideValueBoundaryGeneric('',[eta3d],[eta0])
horMomEq.addBoundaryCondition('top_Sea',horMomBndSymm)
horMomEq.addBoundaryCondition('bottom_Sea',horMomBndWall) # must be wall for bath!
horMomEq.addBoundaryCondition('Wall',horMomBndOpen)

if s.getSolveUVImplicitVerticalDiffusion() :
  vertMomUEq = e.vertMomUEq
  vertMomUBndZero = vertMomUEq.new0FluxBoundary()
  vertMomUBndBottom = vertMomUEq.newBoundaryBottom(f.bottomFrictionFunc, f.zBotDistFunc)
  #vertMomUBndSymm = vertMomUEq.newSymmetryBoundary('')
  vertMomUEq.addBoundaryCondition(['Wall','top_Sea'], vertMomUBndZero)
  vertMomUEq.addBoundaryCondition('bottom_Sea',vertMomUBndBottom)

wEq = e.wEq
wBndZero = wEq.new0FluxBoundary()
wBndSymm = wEq.newSymmetryBoundary('')
wEq.addBoundaryCondition(['Wall','top_Sea'],wBndSymm) # must be symm!!
wEq.addBoundaryCondition('bottom_Sea',wBndZero)

SEq = e.SEq
SEq.setLaxFriedrichsFactor(0.0)
SBndZero = SEq.new0FluxBoundary()
SBndOpen = SEq.newInFluxBoundary(f.SInitFunc)
SEq.addBoundaryCondition('top_Sea',SBndZero)
SEq.addBoundaryCondition('bottom_Sea',SBndZero)
SEq.addBoundaryCondition('Wall',SBndOpen)

eta2d = d.etaDof2d.getFunction()
uvAv2d = d.uvAvDof2d.getFunction()

eta2dEq = e.eta2dEq
#etaBndZero = eta2dEq.new0FluxBoundary()
etaBndOpen = eta2dEq.newOutsideValueBoundaryGeneric('',[function.getSolution()],[eta0])
eta2dEq.addBoundaryCondition('Wall',etaBndOpen)

uv2dEq = e.uv2dEq
#uv2dBndWall = uv2dEq.newBoundaryWall()
uv2dBndOpen = uv2dEq.newOutsideValueBoundaryGeneric('',[eta2d],[eta0])
uv2dEq.addBoundaryCondition('Wall',uv2dBndOpen)

exp = s.exporter
exp.addDofContainer(d.rhoDof)
exp.addDofContainer(d.SDof)
#exp.addDofContainer(d.rDof3d)
#exp.addDofContainer(d.rGradDof3d)
exp.addDofContainer(d.uvAvDof2d)
#exp.setExportRho(1)

# .-------------------------------.
# |     setup time integrator     |
# '-------------------------------'
t = slim3dTimeIntegratorPC(s)
s.timeIntegrator = t

rk = dgERK(e.eta2dEq, None, DG_ERK_EULER)
dtRK = rk.computeInvSpectralRadius(d.etaDof2d)
CFL = 1
dt = CFL*dtRK
if Msg.GetCommRank() == 0 :
  print('Runge-Kutta dt :' + str(dtRK) + ' CFL:' + str(CFL) + ' dt:' + str(dt))
# try to estimate dt3d
maxUV = 0.9 # max advective velocity
dt3d = dtRK * math.sqrt(9.81*10) / maxUV
M = int(0.72*dt3d/dt)
M = 15
if Msg.GetCommRank() == 0 :
  print('Mode split ratio M :' + str(M))

s.timeIntegrator.setTimeStep(dt)
s.timeIntegrator.set3dTimeStepRatio(M)
s.timeIntegrator.setExportInterval(3600)
s.timeIntegrator.setEndTime(12*12*3600) # 6 days
s.timeIntegrator.setExportAtEveryTimeStep(False)
s.timeIntegrator.initializeFields()

s.timeIntegrator.iterate()
