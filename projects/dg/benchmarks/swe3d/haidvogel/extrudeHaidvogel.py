from extrude import *

nbLayers = 23
depth = 20.

def getLayers (e, v) :
  return [z * depth/nbLayers for z in  range(nbLayers + 1)]

extrude(mesh("haidvogel2d.msh"), getLayers).write('haidvogel.msh')
