#!/usr/bin/env python
# -*- coding: utf-8 -*-
from dgpy import *
from sys import stdout
from shutil import copyfile
import time, math, os

# set Ctrl-C to default signal (terminates immediately)
import signal
signal.signal(signal.SIGINT, signal.SIG_DFL)

# .-------------------------------.
# |          user input           |
# '-------------------------------'
meshFile = 'haidvogel.msh'

odir = 'haidvogelPC/'
# create outputdir if !exists
if Msg.GetCommRank()==0 and not os.path.exists(odir) :
  os.mkdir(odir)
Msg.Barrier()
copyfile(meshFile,odir + '/mesh.msh')

# .-------------------------------.
# |         create solver         |
# '-------------------------------'
s = slim3dSolver(meshFile,["bottom_top"],["top_top"])
# options
s.setSolveS(1)
s.setSolveSImplicitVerticalDiffusion(0)
s.setSolveUVImplicitVerticalDiffusion(0)
s.setSolveTurbulence(0)
s.setAdvectTurbulentEnergy(0)
s.setComputeBottomFriction(0)
s.setFlagUVLimiter(1)
#s.setUseModeSplit(0)
s.exportDirectory = odir
s.turbulenceSetupFile = "gotmTurb.nml"

# .-------------------------------.
# |         set functions         |
# '-------------------------------'
f = s.functions

f.bathFunc2d = functionConstant(20)
f.bathFunc3d = functionConstant(20)
f.bathGradFunc2d = f.zeroFunc
f.bathGradFunc3d = f.zeroFunc

def initS(FCT,XYZ):
  for i in range(0,XYZ.size1()) :
    FCT.set(i,0,5.5*min( max( (-XYZ(i,0))/50, 0.0), 1.0))
f.SInitFunc = functionPython(1, initS, [f.xyzFunc3d])
f.TInitFunc = functionConstant(10)

#f.z0BFunc = functionConstant(0.005)
#f.z0SFunc = functionConstant(0.02)

slim3dParameters.rho0 = 1001.85

# .-------------------------------.
# |       create equations        |
# '-------------------------------'
e = s.createEquations()
d = s.getDofs()

eta2d = d.etaDof2d.getFunction()
eta3d = d.etaDof3d.getFunction()
uvAv2d = d.uvAvDof2d.getFunction()


horMomEq = e.horMomEq
horMomEq.setLaxFriedrichsFactor(0.0)
# TODO: wall boundary gives better results except explodes when front reaches the ends
#   -> generalize Wall cond ?
horMomBndSides = horMomEq.newBoundaryWallLinear(0.5)
#horMomBndSides = horMomEq.newBoundaryWall() # this is bad
horMomBndZero = horMomEq.new0FluxBoundary()
horMomBndSymm = horMomEq.newSymmetryBoundary('')
horMomBndWall = horMomEq.newBoundaryWall()
horMomEq.addBoundaryCondition('bottom_top',horMomBndWall) # must be wall for bath!
horMomEq.addBoundaryCondition('sideL',horMomBndSides)
horMomEq.addBoundaryCondition('river',horMomBndSides)
horMomEq.addBoundaryCondition('sideR',horMomBndSides)
horMomEq.addBoundaryCondition('sea',horMomBndSides)
horMomEq.addBoundaryCondition('top_top',horMomBndSymm)

wEq = e.wEq
wBndZero = wEq.new0FluxBoundary()
wBndSymm = wEq.newSymmetryBoundary('')
wEq.addBoundaryCondition('sideL',wBndZero)
wEq.addBoundaryCondition('river',wBndSymm)
wEq.addBoundaryCondition('sideR',wBndZero)
wEq.addBoundaryCondition('sea',wBndSymm)
wEq.addBoundaryCondition('top_top',wBndSymm) # must be symm!!
wEq.addBoundaryCondition('bottom_top',wBndZero)

SEq = e.SEq
SEq.setLaxFriedrichsFactor(0.0)
SBndZero = SEq.new0FluxBoundary()
SEq.addBoundaryCondition('sideL',SBndZero)
SEq.addBoundaryCondition('river',SBndZero)
SEq.addBoundaryCondition('sideR',SBndZero)
SEq.addBoundaryCondition('sea',SBndZero)
SEq.addBoundaryCondition('top_top',SBndZero)
SEq.addBoundaryCondition('bottom_top',SBndZero)

eta2dEq = e.eta2dEq
etaBndZero = eta2dEq.new0FluxBoundary()
eta2dEq.addBoundaryCondition('sideL',etaBndZero)
eta2dEq.addBoundaryCondition('river',etaBndZero)
eta2dEq.addBoundaryCondition('sideR',etaBndZero)
eta2dEq.addBoundaryCondition('sea',etaBndZero)

uv2dEq = e.uv2dEq
uv2dBndWall = uv2dEq.newBoundaryWall()
uv2dEq.addBoundaryCondition('sideL',uv2dBndWall)
uv2dEq.addBoundaryCondition('river',uv2dBndWall)
uv2dEq.addBoundaryCondition('sideR',uv2dBndWall)
uv2dEq.addBoundaryCondition('sea',uv2dBndWall)

exp = s.exporter
exp.addDofContainer(d.SDof)
exp.addDofContainer(d.rDof3d)
exp.addDofContainer(d.rGradDof3d)
exp.addDofContainer(d.uvAvDof2d)
#exp.setExportRho(1)

# .-------------------------------.
# |     setup time integrator     |
# '-------------------------------'
t = slim3dTimeIntegratorPC(s)
s.timeIntegrator = t

rk = dgERK(e.eta2dEq, None, DG_ERK_EULER)
dtRK = rk.computeInvSpectralRadius(d.etaDof2d)
CFL = 0.5
dt = CFL*dtRK
if Msg.GetCommRank() == 0 :
  print('Runge-Kutta dt :' + str(dtRK) + ' CFL:' + str(CFL) + ' dt:' + str(dt))
# try to estimate dt3d
maxUV = 0.9 # max advective velocity
dt3d = dtRK * math.sqrt(9.81*10) / maxUV
M = int(0.72*dt3d/dt)
#M = 10
if Msg.GetCommRank() == 0 :
  print('Mode split ratio M :' + str(M))

s.timeIntegrator.setTimeStep(dt)
s.timeIntegrator.set3dTimeStepRatio(M)
s.timeIntegrator.setExportInterval(450)
s.timeIntegrator.setEndTime(70000)
#s.timeIntegrator.setExportAtEveryTimeStep(True)
s.timeIntegrator.initializeFields()

s.timeIntegrator.iterate()
