from dgpy import *
from extrude import *
import sys

nameStem = 'rhineRofi'
nbProcs = int(sys.argv[1])

model = GModel()
model.load(nameStem+".msh")
pOpt = meshPartitionOptions()
pOpt.setNumOfPartitions(nbProcs)
PartitionMesh(model,  pOpt)
model.save(nameStem+'_2d_'+ str(nbProcs)+".msh")

nbLayers = 20
H = 20.
HInlet = 5.
Lriver = 45.0e3

def getLayersSigma (e, v) :
  h = min( H*(1-v[0]/Lriver) + HInlet*(v[0]/Lriver) , H )
  return [z * h /nbLayers for z in  range(nbLayers + 1)]

extrude(mesh(nameStem + '_2d_'+str(nbProcs)+'.msh'), getLayersSigma).write(nameStem + '_3d_'+str(nbProcs)+'.msh')
Msg.Exit(0)
