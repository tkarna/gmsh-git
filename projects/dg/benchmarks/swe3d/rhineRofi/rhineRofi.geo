//*********** rhineRofi2d.geo *************//
Lx = 724000;
Ly = 764000;
Lriver = 45000;
// Wriver = 500;  // fine
Wriver = 7000; // coarse
Yriver = 30000;
lc = 1e6;
L = 5;
Lup = 0;

Point(1) = {0, 0, 0, lc};
Point(2) = {-Lx , 0, 0, lc};
Point(3) = {-Lx , Ly , 0, lc};
Point(4) = {0, Ly , 0, lc};
Point(5) = {0, Yriver+Wriver/2 , 0, lc};
Point(6) = {Lriver, Yriver+Wriver/2 , 0, lc};
Point(7) = {Lriver, Yriver-Wriver/2 , 0, lc};
Point(8) = {0, Yriver-Wriver/2 , 0, lc};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,5};
Line(5) = {5,6};
Line(6) = {6,7};
Line(7) = {7,8};
Line(8) = {8,1};

Physical Line("south") = {1};
Physical Line("west") = {2};
Physical Line("north") = {3};
Physical Line("coast") = {4,8};
Physical Line("riverSide") = {5,7};
Physical Line("riverInlet") = {6};

Line Loop(5) = {1,2,3,4,5,6,7,8};
Plane Surface(6) = {5};
Physical Surface("") = {6};

Field[1] = MathEval;
// Field[1].F = "7e3*(80e3/7e3)^((y-105e3)/(764e3-105e3))"; // coarse
Field[1].F = "(120e3-7e3)*((y-105e3)/(764e3-105e3)) + 2e3";
Field[2] = MathEval;
// Field[2].F = "7e3*(200e3/7e3)^((x+65e3)/(-724e3+65e3))"; // coarse
Field[2].F = "(120e3-7e3)*((x+65e3)/(-724e3+65e3)) + 2e3";
Field[3] = Max;
Field[3].FieldsList = {1, 2};
Field[4] = MathEval;
Field[4].F = "7e3"; // coarse
// Field[4].F = "2e3";
Field[5] = Max;
Field[5].FieldsList = {3, 4};
Background Field = 5;

Mesh.Algorithm = 6;
Mesh.Partitioner = 1;
