echo "" > result
for i in  2 4 8 16 32; do
  rundgpy extrudeAndPartitionMesh.py $i
  Mpirun3 -np $i rundgpy ScalingRhineRofi.py | tee log
  time=`tail -n1 log`
  echo $i $time >> result
done

