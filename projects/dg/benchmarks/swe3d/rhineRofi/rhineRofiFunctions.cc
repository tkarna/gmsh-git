/*
 * =====================================================================================
 *
 *       Filename:  rhineRofiFunctions.cc
 *
 *    Description:  Provides functions related to the River plume test case by
                    de Boer et al., Ocean Dynamics, 2006.
 *
 *         Author:  karna, tuomas.karna@uclouvain.be
 *
 * =====================================================================================
 */

#include "fullMatrix.h"
#include "function.h"
#include "dgGroupOfElements.h"
#include "math.h"

extern "C" {
/***** parameters *****/
double etaAmplitude = 1.00; // mean (Fisher et al. 2009 tidal range 2.00 )
double etaPhase = 0;
double g = 9.81;
double H = 20; // depth
double HInlet = 5; // depth at river inlet
double Lriver = 45e3;
double Wriver = 500;
double Qriver = 1.5e3; // (Fisher et al. 2009)
double Sriver = 0;
double Ssea = 32;
double Ttide = 44714; // M2 tidal period (Fisher et al. 2009)
double Tday = 0.99726968*24*60*60; // sidereal time of Earth revolution
double OmegaEarth = 2*M_PI/Tday;
double OmegaTide = 2*M_PI/Ttide;
double c = sqrt(g*H); // [m/s] wave speed
double latDeg = 52.5;
double phi = (M_PI/180)*latDeg; // latitude in radians
double coriolisF = 2*OmegaEarth*sin(phi); // [rad/s] Coriolis parameter
double kelvinK = OmegaTide/c; // [1/m] initial wave number of tidal wave, no friction
double kelvinM = (coriolisF/c);  // [-] Cross-shore variation
double kelvinEtaDouble(double x,double y, double t) {
  double xNeg = std::min( x, 0.0 );
  double etaAbs = etaAmplitude*exp( xNeg*kelvinM );
  double eta = etaAbs*cos( y*kelvinK - OmegaTide*t );
  return eta;
}
double kelvinVDouble(double x,double y, double t) {
  double vScalar = g*kelvinK/OmegaTide;
  double eta = kelvinEtaDouble(x,y,t);
  double v = vScalar*eta;
  if ( x > 0 )
    v = 0;
  return v;
}
/***** kelvin wave elevation *****/
void kelvinEta (dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &time, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< val.size1(); i++) {
    val.set(i,0, kelvinEtaDouble(xyz(i,0),xyz(i,1), time(i,0)) );
  }
}
void kelvinUV (dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &time, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< val.size1(); i++) {
    val.set(i,0, 0 );
    val.set(i,1, kelvinVDouble(xyz(i,0),xyz(i,1), time(i,0)) );
  }
}
/***** coriolis *****/
void coriolis (dataCacheMap *, fullMatrix<double> &val) {
  for (size_t i = 0; i< val.size1(); i++) {
    val.set(i,0, coriolisF );
  }
}
/***** initial salinity *****/
void initS (dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< val.size1(); i++) {
    double xRiver = std::max( (xyz(i,0)-Lriver/4.)/(Lriver-Lriver/4.), 0. );
    val.set(i,0, (1-xRiver)*Ssea + xRiver*Sriver );
  }
}
void initSSpinUp (dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &time, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< val.size1(); i++) {
    double TSpinUp = 24*3600.0;
    double lambda = std::min( (time(i,0)/TSpinUp) ,1.0);
    double SSpinUp = Ssea -lambda*(Ssea - Sriver);
    double xRiver = std::max( (xyz(i,0)-Lriver/4.)/(Lriver-Lriver/4.), 0. );
    val.set(i,0, (1-xRiver)*Ssea + xRiver*SSpinUp );
  }
}
/***** bathymetry *****/
double bathDouble(double x) {
  return std::min( H*(1-x/Lriver) + HInlet*(x/Lriver) , H );
}
void bath (dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< val.size1(); i++) {
    val.set(i,0, bathDouble(xyz(i,0)) );
  }
}
double bathDxDouble(double x) {
  double dhdx = 0;
  if (x > 0)
    dhdx = H*(-1/Lriver) + HInlet*(1/Lriver);
  return dhdx;
}
void bathGrad (dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< val.size1(); i++) {
    val.set(i,0, bathDxDouble(xyz(i,0)) );
    val.set(i,1, 0 );
    val.set(i,2, 0 );
  }
}
/***** river inlet *****/
void riverUV (dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &eta, fullMatrix<double> &bath) {
  for (size_t i = 0; i< val.size1(); i++) {
    double H = eta(i,0) + bath(i,0);
    val.set(i,0, -Qriver/(Wriver*H) );
    val.set(i,1, 0 );
  }
}

/***** 2d-3d velocity corrector *****/
// corrects uv3d to match uv2d
// uvAv2d is from the 2d mode, uvInt2d is computed from uv3d
void uvCorrector (dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &uvInt3d, fullMatrix<double> &uvAv3d, fullMatrix<double> &eta, fullMatrix<double> &bath) {
  for (size_t i = 0; i< val.size1(); i++) {
    double d = eta(i,0) + bath(i,0);
    val.set(i,0, uvAv3d(i,0) - uvInt3d(i,0)/d );
    val.set(i,1, uvAv3d(i,1) - uvInt3d(i,1)/d );
  }
}
/***** hydrostatic pressure *****/
void hydrostatPressure (dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &xyz, fullMatrix<double> &eta) {
  for (size_t i = 0; i< val.size1(); i++) {
    val.set(i,0,9.81*1027*(eta(i,0)-xyz(i,2))/1e4);
  }
}
/********** 2d uv functions *************/
void divideByH (dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &f, fullMatrix<double> &bath, fullMatrix<double> &eta) {
  for (size_t i = 0; i< val.size1(); i++) {
    double H = eta(i,0) + bath(i,0);
    val.set(i,0, f(i,0)/H );
  }
}
/********** 2d bottom friction *************/
void bottomDragSWE(dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &sol, fullMatrix<double> &bath){
  for(int i=0;  i<val.size1(); i++){
    double H = sol(i, 0)+bath(i, 0);
    val.set(i, 0, 9.81*0.0235*0.0235/(pow(H, 1.333333333333)));
  }
}
void bottomDrag2d(dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &eta, fullMatrix<double> &bath){
  for(int i=0;  i<val.size1(); i++){
    double H = eta(i, 0)+bath(i, 0);
    val.set(i, 0, 9.81*0.05*0.05/(pow(H, 1.333333333333)));
  }
}
}