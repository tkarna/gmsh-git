#!/usr/bin/env python
# -*- coding: utf-8 -*-
from dgpy import *
from sys import stdout
from shutil import copyfile
import time, math, os

# set Ctrl-C to default signal (terminates immediately)
import signal
signal.signal(signal.SIGINT, signal.SIG_DFL)

# .-------------------------------.
# |          user input           |
# '-------------------------------'
nProcStr = ''
if Msg.GetCommSize() > 1 :
 nProcStr = '_'+str(Msg.GetCommSize())
meshStem = 'rhineRofi_3d'
#meshStem = 'rhineRofiAdapted_v4'
meshFile = meshStem+nProcStr+'.msh'

odir ='rhineRofiPC'
#odir ='rhineRofiPCfine2/'
# create outputdir if !exists
if Msg.GetCommRank()==0 and not os.path.exists(odir) :
  os.mkdir(odir)
Msg.Barrier()
copyfile(meshFile,odir + '/mesh.msh')

# .-------------------------------.
# |         create solver         |
# '-------------------------------'
s = slim3dSolver(meshFile)
# options
s.setSolveS(1)
s.setSolveSImplicitVerticalDiffusion(1)
s.setSolveUVImplicitVerticalDiffusion(1)
s.setSolveTurbulence(1)
s.setAdvectTurbulentEnergy(1)
s.setComputeBottomFriction(1)
#s.setFlagUVLimiter(0)
s.exportDirectory = odir
s.turbulenceSetupFile = "rhineRofiTurb.nml"

# .-------------------------------.
# |         set functions         |
# '-------------------------------'
f = s.functions
rhineLib="./libRhineRofi.so"
if (Msg.GetCommRank()==0):
  functionC.buildLibraryFromFile ("rhineRofiFunctions.cc", rhineLib);
Msg.Barrier()

f.bathFunc2d = functionC(rhineLib,'bath',1,[f.xyzFunc2d])
f.bathFunc3d = functionC(rhineLib,'bath',1,[f.xyzFunc3d])
f.bathGradFunc2d = functionC(rhineLib,'bathGrad',3,[f.xyzFunc2d])
f.bathGradFunc3d = functionC(rhineLib,'bathGrad',3,[f.xyzFunc3d])

f.TInitFunc = functionConstant(10)
f.SInitFunc = functionC(rhineLib,'initS',1,[f.xyzFunc3d])
#f.SInitFunc = functionC(rhineLib,'initSSpinUp',1,[f.timeFunc,f.xyzFunc3d])

bgHDiff = 1
f.nuhTotal = functionConstant(bgHDiff)
f.kappahTotal = functionConstant(bgHDiff)

f.z0BFunc = functionConstant(0.005)
f.z0SFunc = functionConstant(0.02)
f.coriolisFunc = functionC(rhineLib,'coriolis',1,[])

etaKelvinFunc = functionC(rhineLib,'kelvinEta',1,[f.timeFunc,function.getCoordinates()])
f.etaInitFunc = etaKelvinFunc
uvKelvinFunc = functionC(rhineLib,'kelvinUV',2,[f.timeFunc,function.getCoordinates()])
f.uvAvInitFunc = uvKelvinFunc

# .-------------------------------.
# |       create equations        |
# '-------------------------------'
e = s.createEquations()
d = s.getDofs()

eta2d = d.etaDof2dCG.getFunction()
eta3d = d.etaDof3d.getFunction()
uvAv2d = d.uvAvDof2d.getFunction()

uvRiverFunc3d = functionC(rhineLib,'riverUV',2,[eta3d,f.bathFunc3d])
uvRiverFunc2d = functionC(rhineLib,'riverUV',2,[eta2d,f.bathFunc2d])

horMomEq = e.horMomEq
horMomEq.setLaxFriedrichsFactor(0.0) # maybe a remedy for instability, increases |v| a lot
horMomBndZero = horMomEq.new0FluxBoundary()
horMomBndOpen = horMomEq.newOutsideValueBoundaryGeneric('',[eta3d],[etaKelvinFunc])
horMomBndCoast = horMomEq.newBoundaryWallLinear(0.5)
#horMomBndCoast = horMomEq.newBoundaryWall()
horMomBndWall = horMomEq.newBoundaryWall()
horMomBndSymm = horMomEq.newSymmetryBoundary('')
horMomEq.addBoundaryCondition(['south','west','north'],horMomBndOpen)
horMomEq.addBoundaryCondition(['coast','riverSide'],horMomBndCoast)
horMomEq.addBoundaryCondition('riverInlet',horMomBndSymm)  ## fix for inlet
horMomEq.addBoundaryCondition('top',horMomBndSymm) # zero for nonconst tracers!
horMomEq.addBoundaryCondition('bottom',horMomBndWall) # must be wall for bath!

if s.getSolveUVImplicitVerticalDiffusion() :
  vertMomUEq = e.vertMomUEq
  vertMomUBndZero = vertMomUEq.new0FluxBoundary()
  vertMomUBndBottom = vertMomUEq.newBoundaryBottom(f.bottomFrictionFunc,
                                                  f.zBotDistFunc)
  vertMomUBndSymm = vertMomUEq.newSymmetryBoundary('')
  vertMomUEq.addBoundaryCondition(['south','west','north','coast','riverSide','riverInlet','top'],
                                  vertMomUBndZero)
  vertMomUEq.addBoundaryCondition('bottom',vertMomUBndBottom)

wEq = e.wEq
wBndZero = wEq.new0FluxBoundary()
wBndSymm = wEq.newSymmetryBoundary('')
wEq.addBoundaryCondition(['south','west','north','riverInlet','top'],wBndSymm)
wEq.addBoundaryCondition(['coast','riverSide'],wBndZero)
wEq.addBoundaryCondition('bottom',wBndZero)

SEq = e.SEq
SEq.setLaxFriedrichsFactor(0.0)
SBndZero = SEq.new0FluxBoundary()
SBndSymm = SEq.newSymmetryBoundary('')
SBndOpen = SEq.newInFluxBoundary(f.SInitFunc)
SEq.addBoundaryCondition(['south','west','north','riverInlet'],SBndOpen)
SEq.addBoundaryCondition(['coast','riverSide','top','bottom'],SBndZero)

if s.getAdvectTurbulentEnergy() :
  tkeAdvEq = e.tkeAdvEq
  tkeAdvEq.setLaxFriedrichsFactor(0.0)
  tkeAdvBndZero = tkeAdvEq.new0FluxBoundary()
  tkeAdvBndSymm = tkeAdvEq.newSymmetryBoundary('')
  tkeAdvBndOpen = tkeAdvEq.newInFluxBoundary(f.tinyFunc)
  tkeAdvEq.addBoundaryCondition(['south','west','north','riverInlet'],tkeAdvBndOpen)
  tkeAdvEq.addBoundaryCondition(['coast','riverSide','top','bottom'],tkeAdvBndZero)

  epsAdvEq = e.epsAdvEq
  epsAdvEq.setLaxFriedrichsFactor(0.0)
  epsAdvBndZero = epsAdvEq.new0FluxBoundary()
  epsAdvBndSymm = epsAdvEq.newSymmetryBoundary('')
  epsAdvBndOpen = epsAdvEq.newInFluxBoundary(f.tinyFunc)
  epsAdvEq.addBoundaryCondition(['south','west','north','riverInlet'],epsAdvBndOpen)
  epsAdvEq.addBoundaryCondition(['coast','riverSide','top','bottom'],epsAdvBndZero)

eta2dEq = e.eta2dEq
etaBndZero = eta2dEq.new0FluxBoundary()
#etaBndOpen = eta2dEq.newOutsideValueBoundaryGeneric('',[function.getSolution(), uvAv2d],[etaKelvinFunc,uvKelvinFunc])
etaBndOpen = eta2dEq.newOutsideValueBoundaryGeneric('',[function.getSolution()],[etaKelvinFunc])
etaBndRiver = eta2dEq.newOutsideValueBoundaryGeneric('',[uvAv2d],[uvRiverFunc2d])
eta2dEq.addBoundaryCondition(['south','west','north'],etaBndOpen)
eta2dEq.addBoundaryCondition(['coast','riverSide'],etaBndZero)
eta2dEq.addBoundaryCondition('riverInlet',etaBndRiver)

uv2dEq = e.uv2dEq
uv2dBndWall = uv2dEq.newBoundaryWall()
uv2dBndSymm = uv2dEq.newSymmetryBoundary('')
uv2dBndOpen = uv2dEq.newOutsideValueBoundaryGeneric('',[eta2d],[etaKelvinFunc])
#uv2dBndOpen = uv2dEq.newOutsideValueBoundaryGeneric('',[function.getSolution(),eta2d],[uvKelvinFunc,etaKelvinFunc])
uv2dBndRiver = uv2dEq.newOutsideValueBoundaryGeneric('',[uvAv2d],[uvRiverFunc2d])
uv2dEq.addBoundaryCondition(['south','west','north'],uv2dBndOpen)
uv2dEq.addBoundaryCondition(['coast','riverSide'],uv2dBndWall)
uv2dEq.addBoundaryCondition('riverInlet',uv2dBndRiver)

# .-------------------------------.
# |     setup export options      |
# '-------------------------------'
exp = s.exporter
exp.setTimeExportFormat(exp.HOUR)
exp.addDofContainer(d.SDof)
exp.addDofContainer(d.uvAvDof2d)
exp.addDofContainer(d.uvIntDof2d)
exp.addDofContainer(d.uvTauBDof2d)
exp.addDofContainer(d.uvBotDof2d)
if s.getSolveTurbulence() :
  exp.addDofContainer(d.nuvDof)
  exp.addDofContainer(d.kappavDof)
  exp.addDofContainer(d.tkeDof)
  exp.addDofContainer(d.epsDof)
  exp.addDofContainer(d.lDof)
  exp.addDofContainer(d.ssDof)
  exp.addDofContainer(d.nnDof)

# .-------------------------------.
# |     setup time integrator     |
# '-------------------------------'
t = slim3dTimeIntegratorPC(s)
s.timeIntegrator = t

rk = dgERK(e.eta2dEq, None, DG_ERK_EULER)
dtRK = rk.computeInvSpectralRadius(d.etaDof2d)
CFL = 0.7236
dt = CFL*dtRK
maxUV = 1.5 # max advective velocity
c2d = math.sqrt(9.81*20.)
dt = c2d/(c2d+maxUV) * dt
if Msg.GetCommRank() == 0 :
  print('Runge-Kutta dt :' + str(dtRK) + ' CFL:' + str(CFL) + ' dt:' + str(dt))
# try to estimate dt3d
dt3d = dtRK * math.sqrt(9.81*20) / maxUV
#M = int(0.72*dt3d/dt)
M = 10
if Msg.GetCommRank() == 0 :
  print('Mode split ratio M :' + str(M))

s.timeIntegrator.setTimeStep(dt)
s.timeIntegrator.set3dTimeStepRatio(M)
#s.timeIntegrator.setExportInterval(44714/24)
#s.timeIntegrator.setEndTime(44714)
s.timeIntegrator.setExportInterval(44714/12/50)
s.timeIntegrator.setExportInterval(86400/4)
s.timeIntegrator.setEndTime(86400*30)
s.timeIntegrator.setExportAtEveryTimeStep(False)
s.timeIntegrator.initializeFields()

s.timeIntegrator.iterate()
