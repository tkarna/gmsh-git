from extrude import *

nbLayers = 6 # Coarse
#nbLayers = 20 # Adapted v3 v4
H = 20.
HInlet = 5.
Lriver = 45.0e3

def getLayersSigma (e, v) :
  h = min( H*(1-v[0]/Lriver) + HInlet*(v[0]/Lriver) , H )
  return [z * h /nbLayers for z in  range(nbLayers + 1)]

extrude(mesh("rhineRofi_2d.msh"), getLayersSigma).write("rhineRofi.msh")
