#!/usr/bin/env python
# -*- coding: utf-8 -*-
from dgpy import *
from sys import stdout
from shutil import copyfile
import time, math, os
from scipy import *
import matplotlib.pyplot as plt
from matplotlib import rc
import matplotlib.colors as colors
import cPickle as pickle

fontSize = 18
rc('text', usetex=True)
rc('font', family='times', size=fontSize)
figSize = (6,5)

fieldStr = 'uv'
#dataDir = 'arachnide_run_adapted3/'
#meshFile = 'rhineRofiAdapted_v3.msh'
#meshFile2d = 'rhineRofiAdapted_v3_2d.msh'
dataDir = 'green_run_adapted4cALEcont/'
#dataDir = 'green_run_adapted4cALElimQPcont/'
meshFile = 'rhineRofiAdapted_v4.msh'
meshFile2d = 'rhineRofiAdapted_v4_2d.msh'
idxDir = dataDir+'idx/'
export_counts = range(360,409)
#export_counts = range(360,370)
odir = 'plots/'
#oprefix = odir+'rhineRofi_adapted3_ts_'
oprefix = odir+'rhineRofi_adapted4_cALE_'
cachedir = 'cache/'
saveFigures = False
saveFigures = True
# NOTE need to run first with generateData = True
generateData = False
generateData = True
x = -10e3
y =  50e3 # 30e3 50e3
#x = -15e3
#y =  20e3 # 30e3 50e3
locStr = '_x{x:03d}_y{y:02d}'.format(x=int(x/1e3),y=int(y/1e3))

# horizontal coords
nT = len(export_counts)
#tMin = 1341360.731668
#tMax = 1386077.199428
tMin = 0
tMax = 913.5*nT #44716.5
ts = linspace(tMin,tMax,nT)
nZ = 51 # must be odd
zMin = -20
zMax = 0
zs0 = linspace(zMin,zMax,nZ)
Z0 = tile(zs0,(nT,1))
T = tile(ts,(nZ,1))
T = T.T

Z = zeros(Z0.shape)
SVals = zeros(Z.shape)
uVals = zeros(Z.shape)
vVals = zeros(Z.shape)
if generateData :
  model = GModel()
  model.load(meshFile)
  order = model.getMeshElementByTag(1).getPolynomialOrder()
  print('Polynomial order: ' + str(order))
  dimension = 3

  groups = dgGroupCollection(model, dimension, order)

  model2d = GModel()
  model2d.load (meshFile2d)
  groups2d = dgGroupCollection(model2d, dimension-1, order)

  # DOFs
  etaDof = dgDofContainer(groups2d, 1)
  etaFunc = etaDof.getFunction()
  SDof = dgDofContainer(groups, 1)
  SFunc = SDof.getFunction()
  uvDof = dgDofContainer(groups, 2)
  uvFunc = uvDof.getFunction()
  dgDCMap = dataCacheMap(dataCacheMap.POINT_MODE, groups)
  SCache = dgDCMap.get(SFunc)
  uvCache = dgDCMap.get(uvFunc)
  dgDCMap2d = dataCacheMap(dataCacheMap.POINT_MODE, groups2d)
  etaCache = dgDCMap2d.get(etaFunc)
  for i,export_count in enumerate(export_counts) :
    istr = "_{0:05}".format(export_count)
    print 'Reading '+idxDir + 'uv/'+ 'uv'+istr+'.idx'
    #etaDof.readMsh(   idxDir + 'eta2d/'+ 'eta2d'+istr+'.idx')
    #SDof.readMsh(     idxDir + 'S/'+ 'S'+istr+'.idx')
    #uvDof.readMsh(     idxDir + 'uv/'+ 'uv'+istr+'.idx')
    etaDof.readMsh(   idxDir  + 'eta2d'+istr+'.idx')
    SDof.readMsh(     idxDir  + 'S'+istr+'.idx')
    uvDof.readMsh(     idxDir + 'uv'+istr+'.idx')
    dgDCMap2d.setPoint(groups2d,x,y,0)
    eta = etaCache.get()(0,0)
    zs = linspace(zMin,eta,nZ)
    Z[i,:] = zs
    for j,z in enumerate(zs0) :
      dgDCMap.setPoint(groups,x,y,z)
      SVals[i,j] = SCache.get()(0,0)
      uVals[i,j] = uvCache.get()(0,0)
      vVals[i,j] = uvCache.get()(0,1)

  # store uVals,vVals,Z on disk !
  pickle.dump( Z    , open( cachedir+'ts'+locStr+'Z'+'.p', 'wb' ) )
  pickle.dump( SVals, open( cachedir+'ts'+locStr+'S'+'.p', 'wb' ) )
  pickle.dump( uVals, open( cachedir+'ts'+locStr+'u'+'.p', 'wb' ) )
  pickle.dump( vVals, open( cachedir+'ts'+locStr+'v'+'.p', 'wb' ) )
else :
  Z = pickle.load( open( cachedir+'ts'+locStr+'Z'+'.p', 'rb' ) )
  SVals = pickle.load( open( cachedir+'ts'+locStr+'S'+'.p', 'rb' ) )
  uVals = pickle.load( open( cachedir+'ts'+locStr+'u'+'.p', 'rb' ) )
  vVals = pickle.load( open( cachedir+'ts'+locStr+'v'+'.p', 'rb' ) )

# offset and scaling for x coords
tScale = 3600
def plotUcontours(iFig, fieldStr, uArray, nContFill,contVals) :
  plt.figure()
  plt.hold(True)
  plt.gcf().subplots_adjust(left=0.10, right=0.88, top=0.95, bottom=0.1)
  #contVals = linspace(-4.999,34.999,9)
  cf = plt.contourf(T/tScale,Z,uArray, nContFill,#contVals,
                  cmap=plt.cm.bone,
                  #colors=( (0.2,0.2,0.2), (1,1,1), (0.6,0.6,0.6), (0.5,0.5,0.5)),
                  )
  cb = plt.colorbar(fraction=0.06, pad=0.03, aspect=10, format='%.2f')
  #cb.set_label(r'$'+fieldStr+'$ [$ms^{-1}$]',fontsize=fontsize)
  #cb.set_clim(SMin,SMax)
  plt.title(r'$'+fieldStr+'$ [$ms^{-1}$]',fontsize=fontSize)
  plt.gca().set_axis_bgcolor('none') # transparent background
  plt.axis('tight')
  if iFig == 0  :
    plt.ylabel(r'$z$ [$m$]')
  else :
    plt.gca().set_yticklabels([])
  plt.xlabel(r'$t$ [$h$]')
  #contVals = linspace(-0.8,0.8,5)
  colorVals = linspace(1.0,0.3,len(contVals))
  colorVals[len(contVals)/2] = 1
  colorStr = []
  for c in colorVals : colorStr.append(str(c))
  cfNeg = plt.contour(T/tScale,Z,uArray,contVals[0:len(contVals)/2],
                  #cmap=plt.cm.gray_r,
                  colors=colorStr[0:len(contVals)/2],
                  linewidths=2, linestyles='dashed')
  plt.clabel(cfNeg, fmt = '%2.2f', fontsize=fontSize)
  cfPos = plt.contour(T/tScale,Z,uArray,contVals[len(contVals)/2:],
                  #cmap=plt.cm.gray_r,
                  colors=colorStr[len(contVals)/2:],
                  linewidths=2, linestyles='solid')
  plt.clabel(cfPos, fmt = '%2.2f', fontsize=fontSize)
  abcX, abcY = (0.10,0.96)
  plt.figtext(abcX, abcY,chr(ord('a') + iFig )+')',fontsize=fontSize+2)
  if saveFigures :
    filename = oprefix + fieldStr + locStr+'.pdf'
    print 'saving to ' +  filename
    plt.savefig(filename)

plotUcontours(0, 'u', uVals, 40, linspace(-0.1,0.1,5) )
plotUcontours(1, 'v', vVals, 25, linspace(-0.8,0.8,5) )

#plt.figure()
#plt.hold(True)
#plt.gcf().subplots_adjust(left=0.1, right=0.9, top=0.90, bottom=0.1)
##contVals = linspace(-4.999,34.999,9)
#cf = plt.contourf(T/tScale,Z,uVals, 40,#contVals,
                #cmap=plt.cm.bone,
                ##colors=( (0.2,0.2,0.2), (1,1,1), (0.6,0.6,0.6), (0.5,0.5,0.5)),
                #)
#cb = plt.colorbar(fraction=0.06, pad=0.03, aspect=10, format='%.2f')
#cb.set_label(r'$u$ [$ms^{-1}$]',fontsize=fontsize)
##cb.set_clim(SMin,SMax)
#plt.gca().set_axis_bgcolor('none') # transparent background
#plt.axis('tight')
#plt.ylabel(r'$z$ [$m$]')
#plt.xlabel(r'$t$ [$h$]')
#contVals = linspace(-.1,0.1,5)
#colorVals = linspace(0.8,0.0,len(contVals))
#colorVals[len(contVals)/2] = 1
#colorStr = []
#for c in colorVals : colorStr.append(str(c))
#cfNeg = plt.contour(T/tScale,Z,uVals,contVals[0:len(contVals)/2],
                ##cmap=plt.cm.gray_r,
                #colors=colorStr[0:len(contVals)/2],
                #linewidths=2, linestyles='dashed')
#plt.clabel(cfNeg, fmt = '%2.2f', fontsize=fontsize)
#cfPos = plt.contour(T/tScale,Z,uVals,contVals[len(contVals)/2:],
                ##cmap=plt.cm.gray_r,
                #colors=colorStr[len(contVals)/2:],
                #linewidths=2, linestyles='solid')
#plt.clabel(cfPos, fmt = '%2.2f', fontsize=fontsize)
#if saveFigures :
  #filename = oprefix + 'u' + locStr+'.pdf'
  #print 'saving to ' +  filename
  #plt.savefig(filename)

plt.show()

