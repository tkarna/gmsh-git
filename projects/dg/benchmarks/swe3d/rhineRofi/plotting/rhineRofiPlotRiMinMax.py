#!/usr/bin/env python
# -*- coding: utf-8 -*-
from dgpy import *
from sys import stdout
from shutil import copyfile
import time, math, os
from scipy import *
import matplotlib.pyplot as plt
from matplotlib import rc
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import AxesGrid

rc('text', usetex=True)
rc('font', family='times', size=12)
figSize = (6,4.3)

datadir = 'green_run_adapted4cALEcont/'
meshFile = 'rhineRofiAdapted_v4.msh'
meshFile2d = 'rhineRofiAdapted_v4_2d.msh'
odir = 'plots/'
oprefix = odir+'rhineRofi_adapted4_cALE_'
saveFigures = False
saveFigures = True

model = GModel()
model.load(meshFile2d)
order = model.getMeshElementByTag(1).getPolynomialOrder()
print('Polynomial order: ' + str(order))
dimension = 2

groups = dgGroupCollection(model, dimension, order)

# DOFs
RiMaxDof2d = dgDofContainer(groups, 1)
RiMinDof2d = dgDofContainer(groups, 1)
RiSurfMaxDof2d = dgDofContainer(groups, 1)
RiSurfMinDof2d = dgDofContainer(groups, 1)

RiMinDof2d.importMsh(     datadir + 'RiMin' )
RiMaxDof2d.importMsh(     datadir + 'RiMax' )
RiSurfMinDof2d.importMsh(     datadir + 'RiSurfMin' )
RiSurfMaxDof2d.importMsh(     datadir + 'RiSurfMax' )

# horizontal coords
nX = 501 # must be odd
nY = 901 # must be odd
xMin = -30e3
xMax = 0e3
xs = linspace(xMin,xMax,nX)
yMin = 15e3
yMax = 100e3
ys = linspace(yMin,yMax,nY)
z = 0.0
X = tile(xs,(nY,1))
Y = tile(ys,(nX,1))
X = X.T

# offset and scaling for x coords
xOffset = 0
xScale = 1e3
yOffset = -30e3
yScale = 1e3
RiMax =  2.0
RiMin = -0.3
def scaleX(x0) :
  return (x0 + xOffset)/xScale
def scaleY(y0) :
  return (y0 + yOffset)/yScale
X = scaleX(X)
Y = scaleY(Y)
xMin = X.min()
xMax = X.max()
yMin = Y.min()
yMax = Y.max()

# stations
#stationsX = [-15e3,-25e3,-10e3,-5e3,-10e3, -3e3]
#stationsY = [ 20e3, 58e3, 70e3,38e3, 35e3, 37e3]
stationsX = [-10e3,-10e3,-15e3,-10e3,-5e3]
stationsY = [ 50e3, 30e3, 20e3, 70e3,38e3]
stationMarkers = [ '^','^','*','*','*',]
stationsX = scaleX(array(stationsX))
stationsY = scaleY(array(stationsY))

RiMinVals = zeros(Y.shape)
RiMaxVals = zeros(Y.shape)
RiSurfMinVals = zeros(Y.shape)
RiSurfMaxVals = zeros(Y.shape)
dgDCMap = dataCacheMap(dataCacheMap.POINT_MODE, groups)
RiMaxCache = dgDCMap.get(RiMaxDof2d.getFunction())
RiMinCache = dgDCMap.get(RiMinDof2d.getFunction())
RiSurfMaxCache = dgDCMap.get(RiSurfMaxDof2d.getFunction())
RiSurfMinCache = dgDCMap.get(RiSurfMinDof2d.getFunction())
for i,x in enumerate(xs) :
  for j,y in enumerate(ys) :
    dgDCMap.setPoint(groups,x,y,z)
    RiMaxVals[i,j] = RiMaxCache.get()(0,0)
    RiMinVals[i,j] = RiMinCache.get()(0,0)
    RiSurfMaxVals[i,j] = RiSurfMaxCache.get()(0,0)
    RiSurfMinVals[i,j] = RiSurfMinCache.get()(0,0)

fig = plt.figure(figsize=figSize)
cdict = {'red':   ((0.0, 0.8, 0.8),
                   (1.0, 0.3, 0.3)),
         'green': ((0.0, 0.9, 0.9),
                   (1.0, 0.1, 0.1)),
         'blue':  ((0.0, 1.0, 1.0),
                   (1.0, 0.1, 0.1))}
fullGrayCmap = colors.LinearSegmentedColormap('fullGray',cdict,256)
#plt.gcf().subplots_adjust(left=0.29, right=0.98, top=1.05, bottom=0.01)
#contVals = linspace(-4.999,34.999,9)
cmKeys = array([RiMin,0,0.25,1,RiMax])
cmColors = [(0,0,1),(1,1,1),(1.0,1,0.0),(1,0,0.0),(0.7,0.15,0.2)]
cmKeys = (cmKeys-cmKeys[0])/(cmKeys[-1]-cmKeys[0])
cmList = []
for i,key in enumerate(cmKeys) :
  cmList.append( ( key, cmColors[i] ) )
colormap = colors.LinearSegmentedColormap.from_list('divRi',cmList)

#plt.subplot(1,4,1)
#im = plt.imshow(RiMaxVals.T, interpolation='gaussian', origin='lower',
                #cmap=colormap,
                #extent=(xMin,xMax,yMin,yMax),
                #alpha=1.0,vmin=RiMin,vmax=RiMax,
                #aspect=1)
##cf = plt.contour(X,Y,SVals,contVals,
                 ##cmap=plt.cm.gray_r,
                 ###colors=( (0.2,0.2,0.2), (1,1,1), (0.6,0.6,0.6), (0.5,0.5,0.5) ),
                 ##linewidths=2)
##plt.clabel(cf, fmt = '%2.f', fontsize=12) #, colors = 'k'
##plt.ylim(-20,0)
#plt.ylabel(r'$y$ [$km$]')
#plt.xlabel(r'$x$ [$km$]')
##plt.axis('equal')
##plt.axis('tight')
##plt.xlim(xMin,xMax)
#plt.xticks(linspace(xMin,xMax,4))
##plt.gca().ticklabel_format(style='sci', scilimits=(1,1),axis='x') # sci style ticks
##abcX, abcY = (0.07,0.91)
##abcFontSize = 16
##plt.figtext(abcX, abcY,'a)',fontsize=abcFontSize)

#plt.subplot(1,4,2)
#im = plt.imshow(RiMinVals.T, interpolation='gaussian', origin='lower',
                #cmap=colormap,
                #extent=(xMin,xMax,yMin,yMax),
                #alpha=1.0,vmin=RiMin,vmax=RiMax,
                #aspect=1)
#plt.xlabel(r'$x$ [$km$]')
#plt.xticks(linspace(xMin,xMax,4))
#plt.gca().set_yticklabels([])

grid = AxesGrid(fig, 111, # similar to subplot(111)
                nrows_ncols = (1, 2), # creates 2x2 grid of axes
                axes_pad=0.2, # pad between axes in inch.
                share_all=True,
                aspect=True,
                label_mode = "L", # location of tick labels, “1” (only the lower left axes), “L” (left most and bottom most axes), or “all”.
                cbar_location = "right", # [right|top]
                cbar_mode="single", #[None|single|each]
                cbar_size="10%", # size of the colorbar
                cbar_pad="10%", # pad between image axes and colorbar axes
                #share_all=True,
                #label_mode = "L",
                #cbar_location = "right",
                #cbar_mode="single",
                )
                
im = grid[0].imshow(RiSurfMaxVals.T, interpolation='gaussian', origin='lower',
                cmap=colormap,
                extent=(xMin,xMax,yMin,yMax),
                alpha=1.0,vmin=RiMin,vmax=RiMax,
                aspect=1)
grid[0].set_xlabel(r'$x$ [$km$]')
grid[0].set_xticks(linspace(xMin,xMax,4))
grid[0].set_ylabel(r'$y$ [$km$]')
grid[0].set_yticks(arange(-10,80,10))
grid[0].axis('tight')
grid[0].set_title('max$_T(Ri)$')
# Plot station markers
def plotStations(axes,xx,yy,markerList) :
  for i in range(len(xx)) :
    axes.plot(xx[i],yy[i],color='k',marker=markerList[i],markersize=6)
    #stationStr = 1 + i
    stationStr = chr(ord('A') + i)
    axes.text(xx[i]-0.1,yy[i]+1.5,stationStr,ha='right',va='bottom',size=12)
plotStations(grid[0],stationsX,stationsY,stationMarkers)
grid[0].text(-0.01,1.02,chr(ord('a') + i)+')',transform = ax.transAxes,size=16)


im = grid[1].imshow(RiSurfMinVals.T, interpolation='gaussian', origin='lower',
                cmap=colormap,
                extent=(xMin,xMax,yMin,yMax),
                alpha=1.0,vmin=RiMin,vmax=RiMax,
                aspect=1)
grid[1].set_xlabel(r'$x$ [$km$]')
grid[1].set_xticks(linspace(xMin,xMax,4))
grid[1].set_title('min$_T(Ri)$')
# stations
plotStations(grid[1],stationsX,stationsY,stationMarkers)

cb = grid.cbar_axes[0].colorbar(im)
#cb.ax.set_aspect(10)
#cb.set_label_text(r'$Ri$')

#cb = plt.colorbar(im, cax = grid.cbar_axes[0], aspect=10, format='%.2f')
#cb = plt.colorbar(im, fraction=0.16, pad=-0.03, aspect=15, format='%.2f')
#cb = plt.colorbar(im, aspect=15, format='%.2f')
#cb.set_label(r'$Ri$')

#cb = plt.colorbar(fraction=0.16, pad=0.03, aspect=10, format='%.2f')
#divider = make_axes_locatable(grid[0])
#cax = divider.append_axes("right", size="5%", pad=0.05)
#plt.colorbar(im, cax=cax)
#cb = plt.colorbar()
#cb.set_label(r'$Ri$')

plt.gca().set_axis_bgcolor('none') # transparent background

if saveFigures :
  filename = oprefix + 'RiMinMax' +'.pdf'
  print 'saving to ' +  filename
  plt.savefig(filename)

plt.show()
