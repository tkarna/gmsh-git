#!/usr/bin/env python
# -*- coding: utf-8 -*-
from dgpy import *
from sys import stdout
from shutil import copyfile
import time, math, os
from scipy import *
import matplotlib.pyplot as plt
from matplotlib import rc
from mpl_toolkits.axes_grid1 import AxesGrid
import matplotlib.colors as colors
import cPickle as pickle

fontsize = 14
rc('text', usetex=True)
rc('font', family='times', size=fontsize)
figSize = (13,4.3)

fieldStr = 'S'
#datadir = 'arachnide_run_adapted3/'
#meshFile = 'rhineRofiAdapted_v3.msh'
#meshFile2d = 'rhineRofiAdapted_v3_2d.msh'
#datadir = 'green_run_adapted4cALEcont/'
datadir = 'green_run_adapted4cALEcont/'
meshFile = 'rhineRofiAdapted_v4.msh'
meshFile2d = 'rhineRofiAdapted_v4_2d.msh'
idxDir = datadir+'idx/'
odir = 'plots/'
oprefix = odir+'rhineRofi_adapted4_cALE_'
cachedir = 'cache/'
saveFigures = False
saveFigures = True
# NOTE need to run first with generateData = True
generateData = False
generateData = True
#export_counts = range(360,409,8)
export_counts = range(360,401,8)
#export_counts = [360]
#export_counts = [368]
#export_counts = [376]
#export_counts = [384]
#export_counts = [392]
#export_counts = [400]
#export_counts = [408]
showPlot = False
showPlot = True
omitYlabel = False
#omitYlabel = True
colorbarOnly = True
#colorbarOnly = False
zTrans = -0.5
locStr = '_z{z:g}'.format(z=-zTrans)
cfname = 'hslice'

# horizontal coords
nX = 501 # must be odd
nY = 901 # must be odd
xMin = -30e3
xMax = 0e3
xs = linspace(xMin,xMax,nX)
z = zTrans
yMin = 15e3
yMax = 100e3
ys = linspace(yMin,yMax,nY)
X = tile(xs,(nY,1))
Y = tile(ys,(nX,1))
X = X.T

xOffset = 0
xScale = 1e3
yOffset = -30e3
yScale = 1e3
SMax = 32
SMin = 15
#SMax = 32
#SMin = 27
# Transform to plot coordinates (km)
def scaleX(x0) :
  return (x0 + xOffset)/xScale
def scaleY(y0) :
  return (y0 + yOffset)/yScale
X = scaleX(X)
Y = scaleY(Y)

# stations
#stationsX = [-15e3,-25e3,-10e3,-5e3,-10e3, -3e3]
#stationsY = [ 20e3, 58e3, 70e3,38e3, 35e3, 37e3]
stationsX = [-10e3,-10e3,-15e3,-10e3,-5e3]
stationsY = [ 50e3, 30e3, 20e3, 70e3,38e3]
stationMarkers = [ '^','^','*','*','*',]
stationsX = scaleX(array(stationsX))
stationsY = scaleY(array(stationsY))

N = len(export_counts)

SVals = zeros(X.shape)
uVals = zeros(X.shape)
vVals = zeros(X.shape)

fig = plt.figure(figsize=figSize)
fig.subplots_adjust(left=0.10, right=0.88, top=0.90, bottom=0.15)
grid = AxesGrid(fig, 111, # similar to subplot(111)
                nrows_ncols = (1, N), # creates 2x2 grid of axes
                axes_pad=0.3, # pad between axes in inch.
                share_all=True,
                aspect=False,
                label_mode = "L", # location of tick labels, “1” (only the lower left axes), “L” (left most and bottom most axes), or “all”.
                cbar_location = "right", # [right|top]
                cbar_mode="single", #[None|single|each]
                cbar_size=0.2, # size of the colorbar
                cbar_pad=0.13, # pad between image axes and colorbar axes
                )


for ifig,export_count in enumerate(export_counts) :
  
  ### Generate data

  print 'processing step {i:d}'.format(i=export_count)
  istr = "_{0:05}".format(export_count)
  if generateData :
    model = GModel()
    model.load(meshFile)
    order = model.getMeshElementByTag(1).getPolynomialOrder()
    dimension = 3

    groups = dgGroupCollection(model, dimension, order)

    # DOFs
    SDof = dgDofContainer(groups, 1)
    uvDof = dgDofContainer(groups, 2)
    SFunc = SDof.getFunction()
    uvFunc = uvDof.getFunction()

    #etaDof.readMsh(   idxDir + 'eta2d/'+ 'eta2d'+istr+'.idx')
    #print 'Reading '+idxDir + 'uv/'+ 'uv'+istr+'.idx'
    #SDof.readMsh(     idxDir + 'S/'+ 'S'+istr+'.idx')
    #uvDof.readMsh(     idxDir + 'uv/'+ 'uv'+istr+'.idx')
    print 'Reading '+idxDir   + 'uv'+istr+'.idx'
    SDof.readMsh(     idxDir  + 'S'+istr+'.idx')
    uvDof.readMsh(     idxDir + 'uv'+istr+'.idx')

    dgDCMap = dataCacheMap(dataCacheMap.POINT_MODE, groups)
    SCache = dgDCMap.get(SFunc)
    uvCache = dgDCMap.get(uvFunc)
    for i,x in enumerate(xs) :
      for j,y in enumerate(ys) :
        dgDCMap.setPoint(groups,x,y,z)
        SVals[i,j] = SCache.get()(0,0)
        uVals[i,j] = uvCache.get()(0,0)
        vVals[i,j] = uvCache.get()(0,1)

    # store uVals,vVals,Z on disk !
    print 'saving data to {0:s}'.format(cachedir+cfname+locStr+'_S'+istr+'.p')
    pickle.dump( SVals, open( cachedir+cfname+locStr+'_S'+istr+'.p', 'wb' ) )
    pickle.dump( uVals, open( cachedir+cfname+locStr+'_u'+istr+'.p', 'wb' ) )
    pickle.dump( vVals, open( cachedir+cfname+locStr+'_v'+istr+'.p', 'wb' ) )
  else :
    SVals = pickle.load( open( cachedir+cfname+locStr+'_S'+istr+'.p', 'rb' ) )
    uVals = pickle.load( open( cachedir+cfname+locStr+'_u'+istr+'.p', 'rb' ) )
    vVals = pickle.load( open( cachedir+cfname+locStr+'_v'+istr+'.p', 'rb' ) )


  ### Plot panels

  #uvMag = sqrt(uVals**2 + vVals**2)
  #print '|UV| max {M:7.4f} min {m:7.4f}'.format(M=uvMag.max(),m=uvMag.min())

  # Plot S contour
  
  # offset and scaling for x coords
  xMin = X.min()
  xMax = X.max()
  yMin = Y.min()
  yMax = Y.max()
  print '  S max {M:7.4f} min {m:7.4f}'.format(m=SVals.min(), M=SVals.max())
  ax = grid[ifig]
  # saturate to min levels
  SVals[nonzero(SVals < SMin +.001)] = SMin + .001
  contVals = linspace(SMin + .0001,SMax +.01,SMax-SMin+1)
  cf = ax.contourf(X,Y,SVals,contVals,
                  #cmap=plt.cm.gray_r,
                  cmap=plt.cm.jet,
                  #colors=( (0.2,0.2,0.2), (1,1,1), (0.6,0.6,0.6), (0.5,0.5,0.5) ),
                  #linewidths=2,
                  aspect=1)
  ax.set_ylabel(r'$y$ [$km$]')
  ax.set_xlabel(r'$x$ [$km$]')
  ax.set_title( r'$t/\tau = {0:2.0f}/6$'.format((export_count-360)*6/48) )
  #ax.axis('equal')
  ax.axis('tight')
  ax.set_xticks(linspace(xMin,xMax,4))
  #plt.gca().ticklabel_format(style='sci', scilimits=(1,1),axis='x') # sci style ticks

  # Plot UV arrows
  
  UVmax = -1e+20
  UVmin = 1e+20
  def draw_arrows(X,Y,U,V) :
    global UVmax, UVmin
    for i in range(X.shape[0]) :
      for j in range(X.shape[1]) :
        #print i,j
        uvNorm = draw_one_arrow(X[i,j],Y[i,j],U[i,j],V[i,j])
        UVmax = max(UVmax,uvNorm)
        UVmin = min(UVmin,uvNorm)
        #return UVmax,UVmin

  def draw_one_arrow(x,y,u,v) :
    headL = 0.33
    shaftW = 0.05
    headW = 0.25
    norm = sqrt(u**2 + v**2)
    scale = 4
    mag = norm*scale
    ax.arrow(x, y, u*scale, v*scale, color=(0.2,0.2,0.2),
              width=shaftW*mag,
              head_width=headW*mag,
              head_length=headL*mag,
              linewidth=0.0,
              length_includes_head=True
              )
    return norm

  ix0 = 20
  ixDiv = 50
  iy0 = 20
  iyDiv = 50
  draw_arrows(    X[ix0:-ix0:ixDiv,iy0:-iy0:iyDiv],
                  Y[ix0:-ix0:ixDiv,iy0:-iy0:iyDiv],
              uVals[ix0:-ix0:ixDiv,iy0:-iy0:iyDiv],
              vVals[ix0:-ix0:ixDiv,iy0:-iy0:iyDiv])
  print '  |UV| arrow min {m:7.4f} max {M:7.4f}'.format(M=UVmax,m=UVmin)

  ax.set_axis_bgcolor('none') # transparent background
  cb = grid.cbar_axes[0].colorbar(cf)
  ctickinterval = 3
  cticks = range(SMin+mod(SMax-SMin,ctickinterval),SMax+1,ctickinterval)
  grid.cbar_axes[0].set_yticks( cticks )
  cb.set_label_text(r'$S [psu]$')

  # Plot station markers
  def plotStations(axes,xx,yy,markerList) :
    for i in range(len(xx)) :
      axes.plot(xx[i],yy[i],color='k',marker=markerList[i],markersize=6)
      #stationStr = 1 + i
      stationStr = chr(ord('A') + i)
      axes.text(xx[i]-0.1,yy[i]+1.5,stationStr,ha='right',va='bottom',size=12)
  plotStations(ax,stationsX,stationsY,stationMarkers)

if saveFigures :
  #filename = oprefix +cfname+'_S'+locStr.replace('.','_')+istr+'.pdf'
  filename = oprefix +cfname+'_S'+'_combo'+locStr.replace('.','_')+'.pdf'
  print 'saving to ' +  filename
  fig.savefig(filename,transparent=True)

if showPlot :
  plt.show()
