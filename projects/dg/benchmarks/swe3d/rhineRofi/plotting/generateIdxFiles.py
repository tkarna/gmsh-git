#!/usr/bin/env python
# -*- coding: utf-8 -*-
from shutil import copyfile
import time, math, os
from string import Template

fieldStr = 'uv'
nbFields = 2
#fieldStr = 'S'
#nbFields = 1
idxdir = 'rhineRofiPCadapt5ncALElimQP/idx/'
#idxdir = 'rhineRofiPCadapt4cALElimQPdiff10/idx/'
#idxdir = 'green_run_adapted4cALElimQPcont/idx/'
#idxdir = 'green_run_adapted4/idx/'
datadir = '../' # relative path to the msh files
export_counts = range(360,409)
#export_counts = [360]
nbParts = 32

if not os.path.exists(idxdir) :
  print 'Creating directory '+idxdir
  os.mkdir(idxdir)

masterIdxHeader = Template(
"""// nb_fields  ${NBFIELDS} 
// nb_part    ${NBPARTS}
"""
)
masterIdxEntry = Template(
"""
// field ${COMP}
Merge "${FIELD}${ISTR}_COMP_${COMP}.idx";"""
)

slaveIdxHeader = Template(
"""// field ${COMP}
"""
)
slaveIdxEntry = Template(
"""Merge "$DIR${FIELD}${ISTR}_COMP_${COMP}.msh_${PART}";
"""
)

for export_i in export_counts :
  print 'Processing '+fieldStr+' '+str(export_i)
  comp = 0
  istr = "_{0:05}".format(export_i)
  istrIdx = "-{0:06}".format(export_i)

  # generate slave index files
  for iField in range(nbFields) :
    content = slaveIdxHeader.substitute(COMP = iField)
    for iPart in range(nbParts) :
      # check if the msh file exists
      mshfile = '{DIR:s}{FIELD:s}{ISTR:s}_COMP_{COMP:d}.msh_{PART:d}'.format(COMP=iField,
                                                                             FIELD=fieldStr,
                                                                             ISTR=istr,
                                                                             PART=iPart,
                                                                             DIR=idxdir+datadir)
      if not os.path.isfile(mshfile) :
        print '** MSH file not found : '+mshfile
        exit(-1)
      content += slaveIdxEntry.substitute(COMP = iField, FIELD=fieldStr, ISTR=istr, PART=iPart, DIR=datadir)
    #print "VVVV"    
    #print content
    #print "^^^"
    filename = fieldStr+istrIdx+'_COMP_'+str(iField)+'.idx'
    print ' saving slave idx file to '+idxdir + filename
    f = open(idxdir + filename, 'w')
    f.write(content)
    f.close()

  # generate master idx file
  content = masterIdxHeader.substitute(NBFIELDS=nbFields, NBPARTS=nbParts)
  for iField in range(nbFields) :
    content += masterIdxEntry.substitute(COMP=iField, ISTR=istrIdx, FIELD=fieldStr)
  content += '\n'
  #print "VVVV"    
  #print content
  #print "^^^"
  filename = fieldStr+istr+'.idx'
  print ' saving master idx file to '+idxdir + filename
  f = open(idxdir + filename, 'w')
  f.write(content)
  f.close()


