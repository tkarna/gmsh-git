#!/usr/bin/env python
# -*- coding: utf-8 -*-
from dgpy import *
from sys import stdout
from shutil import copyfile
import time, math, os
from scipy import *
import matplotlib.pyplot as plt
from matplotlib import rc
from mpl_toolkits.mplot3d import Axes3D

fontsize = 14
rc('text', usetex=True)
rc('font', family='times', size=fontsize)
figSize = (6,6)

fig = plt.figure(figsize=figSize)

meshFile2d = 'rhineRofiAdapted_v3_2d.msh'
oprefix = 'plots/'+'rhineRofi_adapted3_'
doZoom = not False
saveFigures = False
saveFigures = True

Msg = Msg()
model = GModel()
model.load(meshFile2d)
order = model.getMeshElementByTag(1).getPolynomialOrder()
print('Polynomial order: ' + str(order))
dimension = 2

bbox = model.bounds()
bboxMin = bbox.min()
bboxMax = bbox.max()

groups = dgGroupCollection(model, dimension, order)
#groups.splitGroupsByVerticalLayer(["bottom"])
groups.buildGroupsOfInterfaces()

def transformCoords(xyz) :
  scalarXY = 1e-3
  offsetX = 0
  offsetY = -30e3
  x = (xyz[0] + offsetX)*scalarXY
  y = (xyz[1] + offsetY)*scalarXY
  z = xyz[2]
  return x,y,z
  
def transformSPoint3(pt) :
  x,y,z = transformCoords([pt.x(),pt.y(),pt.z()])
  return SPoint3(x,y,z)

def plotElement(e) :
  global bbox
  nedges =  e.getNumEdges()
  for i in range(nedges) :
    edge = e.getEdge(i)
    nv = edge.getNumVertices()
    xs = zeros(nv)
    ys = zeros(nv)
    zs = zeros(nv)
    #frontFace = True
    #topFace = True
    for j in range(nv) :
      v = edge.getVertex(j)
      (xs[j],ys[j],zs[j]) = transformCoords( [v.x(), v.y(), v.z()] )
      #if abs(v.y() - bbox.max().y()) > 1e-6 :
        #frontFace = False
      #if abs(v.z() - bbox.max().z()) > 1e-6 :
        #topFace = False
    lineSpec = 'k-'
    # 3D plot
    #ax.plot(xs,ys,zs,lineSpec)
    #if topFace :
      #plt.subplot(2,1,1)
    plt.plot(xs,ys,lineSpec)
    #if frontFace :
      #plt.subplot(2,1,2)
      #plt.plot(xs,zs,lineSpec)

bboxTrMin = transformSPoint3(bbox.min())
bboxTrMax = transformSPoint3(bbox.max())

nGroups = groups.getNbElementGroups()
for iG in range(nGroups) :
#for iG in range(1) :
  group = groups.getElementGroup(iG)
  nElem = group.getNbElements()
  for iE in range(nElem) :
    e = group.getElement(iE)
    plotElement(e)

#plt.subplot(2,1,1)
plt.gcf().subplots_adjust(left=0.10, right=0.97, top=0.95, bottom=0.1)
##plt.xlim(-5e4,5e4)
#plt.ylim(bboxTrMin.y(),bboxTrMax.y())
plt.xlabel(r'$x$ [km]')
plt.ylabel(r'$y$ [km]')
#plt.yticks(linspace(ceil(bboxTrMin.y()),floor(bboxTrMax.y()),3))
plt.minorticks_on()
plt.gca().set_axis_bgcolor('none') # transparent background
plt.axis('tight')
plt.gca().set_aspect(1,adjustable='box')
if doZoom :
  plt.ylim(-30,70)
  plt.xlim(-45,45)
  plt.yticks([-30,0,20,40,60])
  plt.xticks([-45,-20,0,20,45])
  plt.minorticks_off()
#plt.subplot(2,2,1)
##plt.gca().set_aspect(2,adjustable='box')
#plt.xlabel(r'$x$ [km]')
#plt.ylabel(r'$z$ [m]')
#plt.minorticks_on()
#pos = plt.gca().get_position().bounds # [left, bottom, width, height]
##pos[3] = pos[3] +0.1
#plt.gca().set_position([pos[0], pos[1], pos[2], pos[3]+0.1])
#plt.gca().set_axis_bgcolor('none') # transparent background

if saveFigures :
  zoomStr = ''
  if doZoom :
    zoomStr = '_zoom'
  filename = oprefix+'mesh2d'+zoomStr+'.pdf'
  print 'Saving to {0:s}'.format(filename)
  plt.savefig(filename)

plt.show()
