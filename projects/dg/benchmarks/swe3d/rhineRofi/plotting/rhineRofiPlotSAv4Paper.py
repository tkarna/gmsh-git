#!/usr/bin/env python
# -*- coding: utf-8 -*-
from dgpy import *
from sys import stdout
from shutil import copyfile
import time, math, os
from scipy import *
import matplotlib.pyplot as plt
from matplotlib import rc
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import AxesGrid

fontSize = 10
rc('text', usetex=True)
rc('font', family='times', size=fontSize)
figSize = (6,3.5)

#datadir = 'arachnide_run_adapted3/'
#datadir = 'green_run_adapted4cALEcont/'
#datadir = 'green_run_adapted4cALElimQPcont/'
datadir = 'rhineRofiPCadapt4cALElimQPdiff10/'
subpath = 'averages/'
fieldStr = 'Stav'
meshFile = datadir + subpath +'mesh.msh'
meshFile2d = datadir + subpath +'mesh2d.msh'
odir = 'plots/'
oprefix = odir+'rhineRofi_adapted4_cALE_limQP_'
yy = [0e3, 15e3, 30e3]
iFig = 0
saveFigures = False
#saveFigures = True
showFigures = True

# horizontal coords
nX = 501 # must be odd
nZ = 901 # must be odd
xMin = -30e3
xMax = 0e3
xs = linspace(xMin,xMax,nX)
yOffset = 30e3
zMin = -20
zMax = 0
zs = linspace(zMin,zMax,nZ)
X = tile(xs,(nZ,1))
Z = tile(zs,(nX,1))
X = X.T

# offset and scaling for x coords
xOffset = 0
xScale = 1e3
yScale = 1e3
SMax = 32
SMin = 27
X = (X + xOffset)/xScale
#Y = (Y + yOffset)/yScale
xMin = X.min()
xMax = X.max()
#yMin = Y.min()
#yMax = Y.max()
zMin = Z.min()
zMax = Z.max()

model = GModel()
model.load(meshFile)
order = model.getMeshElementByTag(1).getPolynomialOrder()
print('Polynomial order: ' + str(order))
dimension = 3

groups = dgGroupCollection(model, dimension, order)

# DOFs
SDof = dgDofContainer(groups, 1)
SFunc = SDof.getFunction()
SDof.importMsh(     datadir + subpath+fieldStr)

    

fig = plt.figure(figsize=figSize)
plt.gcf().subplots_adjust(left=0.07, right=0.94, top=0.90, bottom=0.12)
#plt.gcf().subplots_adjust(left=0.28, right=0.96, top=1.05, bottom=0.01)

grid = AxesGrid(fig, 111, # similar to subplot(111)
                nrows_ncols = (1, 3), # creates 2x2 grid of axes
                axes_pad=0.4, # pad between axes in inch.
                share_all=False,
                aspect=False,
                label_mode = "L", # location of tick labels, “1” (only the lower left axes), “L” (left most and bottom most axes), or “all”.
                cbar_location = "right", # [right|top]
                cbar_mode="none", #[None|single|each]
                cbar_size="7%", # size of the colorbar
                cbar_pad="3%", # pad between image axes and colorbar axes
                )
    
for iFig in range(len(yy)) :
  yTrans = yy[iFig]
  y = yTrans + yOffset
  SVals = zeros(X.shape)
  dgDCMap = dataCacheMap(dataCacheMap.POINT_MODE, groups)
  SCache = dgDCMap.get(SFunc)
  for i,x in enumerate(xs) :
    for j,z in enumerate(zs) :
      dgDCMap.setPoint(groups,x,y,z)
      SVals[i,j] = SCache.get()(0,0)

  ax = grid[iFig]
  #contVals = linspace(-4.999,34.999,9)
  im = ax.imshow(SVals.T, interpolation='gaussian', origin='lower',
                  #cmap=fullGrayCmap,
                  cmap=plt.cm.jet,
                  extent=(xMin,xMax,zMin,zMax),
                  alpha=1.0,vmin=SMin,vmax=SMax,
                  aspect=3
                  )
  #plt.axes.set_aspect(2)
  #cb = plt.colorbar(fraction=0.06, pad=0.03, aspect=10, format='%.0f')
  #cb.set_label(r'$S$ [$psu$]')
  #cb.set_clim(SMin,SMax)
  contVals = linspace(27.0001,32.01,6)
  cf = ax.contour(X,Z,SVals,contVals,
                  cmap=plt.cm.gray_r,
                  #colors=( (0.2,0.2,0.2), (1,1,1), (0.6,0.6,0.6), (0.5,0.5,0.5) ),
                  linewidths=2)
  ax.clabel(cf, fmt = '%2.f', fontsize=12)#, colors = ('gray'))
  #plt.ylim(-20,0)
  ax.set_ylabel(r'$z$ [$m$]')
  ax.set_xlabel(r'$x$ [$km$]')
  ax.set_title(r'$y = {0:2.0f} km$'.format(yTrans/yScale) )
  #plt.axis('equal')
  plt.axis('tight')
  #plt.xlim(xMin,xMax)
  ax.set_xticks(linspace(xMin,xMax,4))
  #plt.gca().ticklabel_format(style='sci', scilimits=(1,1),axis='x') # sci style ticks
  abcX, abcY = (0.24,0.96)
  abcFontSize = 16
  ax.text(-0.01,1.02,chr(ord('a') + iFig +1 )+')',transform = ax.transAxes,size=fontSize+2)
  #(abcX, abcY,chr(ord('a') + iFig + 1 )+')',fontsize=fontSize)

plt.gca().set_axis_bgcolor('none') # transparent background

if saveFigures :
  transStr = '_trans_y{0:02}'.format(int(yTrans/yScale))
  filename = oprefix +fieldStr + transStr +'.pdf'
  print 'saving to ' +  filename
  #plt.savefig(filename,transparent=True)

if showFigures : 
  plt.show()
