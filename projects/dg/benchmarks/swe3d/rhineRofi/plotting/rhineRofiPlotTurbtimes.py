#!/usr/bin/env python
# -*- coding: utf-8 -*-
from dgpy import *
from sys import stdout
from shutil import copyfile
import time, math, os
from scipy import *
import matplotlib.pyplot as plt
from matplotlib import rc
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import AxesGrid
import cPickle as pickle

fontsize = 12
rc('text', usetex=True)
rc('font', family='times', size=fontsize)
figSize = (10,10)

fieldStr = 'nn'
#dataDir = 'arachnide_run_adapted3/'
#meshFile = 'rhineRofiAdapted_v3.msh'
#meshFile2d = 'rhineRofiAdapted_v3_2d.msh'
dataDir = 'green_run_adapted4cALEcont/'
meshFile = 'rhineRofiAdapted_v4.msh'
meshFile2d = 'rhineRofiAdapted_v4_2d.msh'
idxDir = dataDir+'idx/'
export_counts = range(360,409)
#export_counts = range(360,370)
odir = 'plots/'
#oprefix = odir+'rhineRofi_adapted3_ts_'
oprefix = odir+'rhineRofi_adapted4_cALE_'
cachedir = 'cache/'
saveFigures = False
saveFigures = True
# NOTE need to run first with generateData = True
generateData = False
generateData = True
#x = -25e3 #-15e3 -10e3 -5e3 -2e3 -3e3 -4e3
#y =  58e3 # 20e3  90e3 38e3 37e3 37e3 37e3
#stationsX = [-15e3,-25e3,-10e3,-5e3,-10e3, -3e3]
#stationsY = [ 20e3, 58e3, 70e3,38e3, 35e3, 37e3]
stationsX = [-15e3,-10e3,-5e3,-10e3]
stationsY = [ 20e3, 70e3,38e3, 50e3]
i = 3
x = stationsX[i]
y = stationsY[i]
locStr = '_x{x:03d}_y{y:02d}'.format(x=int(x/1e3),y=int(y/1e3))

# horizontal coords
nT = len(export_counts)
#tMin = 1341360.731668
#tMax = 1386077.199428
tMin = 0
tMax = 913.5*nT #44716.5
ts = linspace(tMin,tMax,nT)
nZ = 51 # must be odd
zMin = -20
zMax = 0
zs0 = linspace(zMin,zMax,nZ)
Z0 = tile(zs0,(nT,1))
T = tile(ts,(nZ,1))
T = T.T

Z = zeros(Z0.shape)
nnVals = zeros(Z.shape)
ssVals = zeros(Z.shape)
uVals = zeros(Z.shape)
vVals = zeros(Z.shape)
SVals = zeros(Z.shape)
if generateData :
  model = GModel()
  model.load(meshFile)
  order = model.getMeshElementByTag(1).getPolynomialOrder()
  print('Polynomial order: ' + str(order))
  dimension = 3

  groups = dgGroupCollection(model, dimension, order)
  dgCG3d = dgCGMeanFilter(groups)

  model2d = GModel()
  model2d.load (meshFile2d)
  groups2d = dgGroupCollection(model2d, dimension-1, order)

  # DOFs
  etaDof = dgDofContainer(groups2d, 1)
  etaFunc = etaDof.getFunction()
  SDof = dgDofContainer(groups, 1)
  nnDof = dgDofContainer(groups, 1)
  ssDof = dgDofContainer(groups, 1)
  uvDof = dgDofContainer(groups, 2)
  RiDof = dgDofContainer(groups, 1)
  RiFunc = functionQuotientNew(nnDof.getFunction(),ssDof.getFunction())
  dgDCMap = dataCacheMap(dataCacheMap.POINT_MODE, groups)
  SCache = dgDCMap.get(SDof.getFunction())
  nnCache = dgDCMap.get(nnDof.getFunction())
  ssCache = dgDCMap.get(ssDof.getFunction())
  uvCache = dgDCMap.get(uvDof.getFunction())
  dgDCMap2d = dataCacheMap(dataCacheMap.POINT_MODE, groups2d)
  etaCache = dgDCMap2d.get(etaFunc)
  for i,export_count in enumerate(export_counts) :
    istr = "_{0:05}".format(export_count)
    print 'Reading '+idxDir + 'nn/'+ 'nn'+istr+'.idx'
    #etaDof.readMsh(   idxDir + 'eta2d/'+ 'eta2d'+istr+'.idx')
    #nnDof.readMsh(     idxDir + 'S/'+ 'S'+istr+'.idx')
    #uvDof.readMsh(     idxDir + 'uv/'+ 'uv'+istr+'.idx')
    [time, step] = etaDof.readMsh(   idxDir  + 'eta2d'+istr+'.idx')
    [time, step] = nnDof.readMsh(     idxDir  + 'nn'+istr+'.idx')
    [time, step] = ssDof.readMsh(     idxDir  + 'ss'+istr+'.idx')
    [time, step] = uvDof.readMsh(     idxDir + 'uv'+istr+'.idx')
    [time, step] = SDof.readMsh(     idxDir + 'S'+istr+'.idx')
    dgCG3d.apply(nnDof,nnDof)
    dgCG3d.apply(ssDof,ssDof)
    nnDof.exportMsh(dataDir+'/nnCG'+istr,time,export_count)
    ssDof.exportMsh(dataDir+'/ssCG'+istr,time,export_count)
    RiDof.interpolate(RiFunc)
    RiDof.exportMsh(dataDir+'/Ri'+istr,time,export_count)
    
    dgDCMap2d.setPoint(groups2d,x,y,0)
    eta = etaCache.get()(0,0)
    zs = linspace(zMin,eta,nZ)
    Z[i,:] = zs
    for j,z in enumerate(zs0) :
      dgDCMap.setPoint(groups,x,y,z)
      nnVals[i,j] = nnCache.get()(0,0)
      ssVals[i,j] = ssCache.get()(0,0)
      uVals[i,j] = uvCache.get()(0,0)
      vVals[i,j] = uvCache.get()(0,1)
      SVals[i,j] = SCache.get()(0,0)

  # store uVals,vVals,Z on disk !
  pickle.dump( Z    , open( cachedir+'ts'+locStr+'Z'+'.p', 'wb' ) )
  pickle.dump( nnVals, open( cachedir+'ts'+locStr+'nn'+'.p', 'wb' ) )
  pickle.dump( ssVals, open( cachedir+'ts'+locStr+'ss'+'.p', 'wb' ) )
  pickle.dump( uVals, open( cachedir+'ts'+locStr+'u'+'.p', 'wb' ) )
  pickle.dump( vVals, open( cachedir+'ts'+locStr+'v'+'.p', 'wb' ) )
  pickle.dump( SVals, open( cachedir+'ts'+locStr+'S'+'.p', 'wb' ) )
else :
  Z = pickle.load( open( cachedir+'ts'+locStr+'Z'+'.p', 'rb' ) )
  nnVals = pickle.load( open( cachedir+'ts'+locStr+'nn'+'.p', 'rb' ) )
  ssVals = pickle.load( open( cachedir+'ts'+locStr+'ss'+'.p', 'rb' ) )
  uVals = pickle.load( open( cachedir+'ts'+locStr+'u'+'.p', 'rb' ) )
  vVals = pickle.load( open( cachedir+'ts'+locStr+'v'+'.p', 'rb' ) )
  SVals = pickle.load( open( cachedir+'ts'+locStr+'S'+'.p', 'rb' ) )

# offset and scaling for x coords
tScale = 3600

fig = plt.figure(figsize=figSize)
#fig.subplots_adjust(left=0.10, right=0.88, top=0.95, bottom=0.1)
grid = AxesGrid(fig, 111, # similar to subplot(111)
                nrows_ncols = (2, 2), # creates 2x2 grid of axes
                axes_pad=0.4, # pad between axes in inch.
                share_all=True,
                aspect=False,
                label_mode = "L", # location of tick labels, “1” (only the lower left axes), “L” (left most and bottom most axes), or “all”.
                cbar_location = "right", # [right|top]
                cbar_mode="each", #[None|single|each]
                cbar_size="7%", # size of the colorbar
                cbar_pad="1%", # pad between image axes and colorbar axes
                )

#colormap = plt.cm.bone
colormap = plt.cm.jet
def plotContours(i, fieldStr, labelStr,unitStr, datArray, nContFill,contVals,cLim) :
  #plt.figure()
  #plt.gcf()
  ax = grid[i]
  ax.hold(True)
  if len(cLim) > 0 :
    datArray[nonzero(datArray<cLim[0])] = cLim[0]
    datArray[nonzero(datArray>cLim[1])] = cLim[1]
    contVals0 = linspace(cLim[0],cLim[1],nContFill)
  else :
    contVals0 = nContFill
  cf = ax.contourf(T/tScale,Z,datArray, contVals0,
                  cmap=colormap,
                  aspect = 'auto'
                  #colors=( (0.2,0.2,0.2), (1,1,1), (0.6,0.6,0.6), (0.5,0.5,0.5)),
                  )
  cb = grid.cbar_axes[i].colorbar(cf)
  #cb.set_label_text(r''+labelStr+' '+unitStr)
  #cbtl = grid.cbar_axes[i].get_yticklabels()
  #grid.cbar_axes[i].set_yticklabels(cbtl,fontsize,9)
  #grid.cbar_axes[i].set_aspect(0.9,adjustable='box-forced')
  ax.set_title(r''+labelStr+' '+unitStr)
  #cb = plt.colorbar(fraction=0.06, pad=0.03, aspect=10, format='%.2f')
  #cb.set_label(r''+labelStr+' '+unitStr,fontsize=fontsize)
  #if len(cLim) > 0 :
    #cb.set_clim(cLim[0],cLim[1])
  #plt.gca().set_axis_bgcolor('none') # transparent background
  #ax.axis('tight')
  ax.set_ylabel(r'$z$ [$m$]')
  ax.set_xlabel(r'$t$ [$h$]')
  if len(contVals) > 0 :
    #contVals = linspace(-0.8,0.8,5)
    if colormap == plt.cm.bone :
      colorVals = linspace(1.0,0.3,len(contVals))
      colorVals[len(contVals)/2] = 1
    else :
      colorVals = linspace(0.0,0.0,len(contVals))
      colorVals[len(contVals)/2] = 0.8
    colorStr = []
    for c in colorVals : colorStr.append(str(c))
    cfNeg = ax.contour(T/tScale,Z,datArray,contVals[0:len(contVals)/2],
                    #cmap=plt.cm.gray_r,
                    colors=colorStr[0:len(contVals)/2],
                    linewidths=2, linestyles='dashed')
    ax.clabel(cfNeg, fmt = '%2.2f', fontsize=fontsize)
    cfPos = ax.contour(T/tScale,Z,datArray,contVals[len(contVals)/2:],
                    #cmap=plt.cm.gray_r,
                    colors=colorStr[len(contVals)/2:],
                    linewidths=2, linestyles='solid')
    ax.clabel(cfPos, fmt = '%2.2f', fontsize=fontsize)
  
  #if saveFigures :
    #filename = oprefix + fieldStr + locStr+'.pdf'
    #print 'saving to ' +  filename
    #plt.savefig(filename)
  
  ax.text(-0.01,1.02,chr(ord('a') + i)+')',transform = ax.transAxes,size=16)

def optLogLims(d) :
  d = d[nonzero(d == d)]
  #print [ d.min() , d.max()]
  #print [ ceil(log10(d.min())) , floor(log10(d.max()))]
  return [ ceil(log10(d.min())) , floor(log10(d.max()))]

data = nnVals
data[nonzero(nnVals < 0.0)] = sqrt(-1)/0.00
data = data/ssVals
data[nonzero(data > 1.0)] = 1.0
mm = data.min()
MM = data.max()
plotContours(0,'Ri','$Ri = N^2/R^2$','', data, 25, array([0.0,0.25]) , [0,1])
print SVals.min(), SVals.max()
plotContours(1,'S','$S$','[$psu$]', SVals, 25, [] , [])
#plotContours(2,'nn','$N^2$','[log$_{10}(s^{-2})$]', log10(nnVals), 25, [] , [-7,-3])
#plotContours(3,'ss','$R^2$','[log$_{10}(s^{-2})$]', log10(ssVals), 25, [] , [-6,-2])
plotContours(2,'nn','$N^2$','[log$_{10}(s^{-2})$]', log10(nnVals), 25, [] , optLogLims(nnVals))
plotContours(3,'ss','$R^2$','[log$_{10}(s^{-2})$]', log10(ssVals), 25, [] , optLogLims(ssVals))

#abcX, abcY = (0.45,0.92)
#abcFontSize = 14
#plt.figtext(abcX, abcY,'$(x,y)=({x:g},{y:g})$ km'.format(x=x/1e3,y=(y-30e3)/1e3),fontsize=abcFontSize)

#plotContours(0,'Ri','$Ri = N^2/S^2$','', data, 25, array([0.0,0.25]) , [0,1])
#plotContours(1,'nn','log$_{10}(N^2)$','[log$_{10}(s^{-2})$]', log10(nnVals), 25, [] , [-7,-3])
#plotContours(2,'ss','log$_{10}(S^2)$','[log$_{10}(s^{-2})$]', log10(ssVals), 25, [] , [-6,-2])
#plotContours(3,'S','$S$','[$psu$]', SVals, 25, [] , [31.8,32.0])
#plotContours(4,'v','$v$','[$ms^{-1}$]', vVals, 25, [] , [])
#plotContours(5,'u','$u$','[$ms^{-1}$]', uVals, 25, [] , [])

if saveFigures :
  description = 'stratCombo'
  filename = oprefix + description + locStr+'.pdf'
  print 'saving to ' +  filename
  plt.savefig(filename)


plt.show()

