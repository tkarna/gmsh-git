import os, sys
from dgpy import *
import sys
from math import *
from extrude import *
import struct
import array


def process(name) :
  inputMesh = name+'.msh'
  nPart = Msg.GetCommSize()
  mesh1 = name+'_'+str(nPart)+'.msh' if Msg.GetCommSize() > 1 else inputMesh
  mesh3d = name+'_'+str(nPart)+'_3d.msh' if Msg.GetCommSize() > 1 else name + "_3d.msh"

  def partition(inputFile, outputFile, nPartition):
    model = GModel()
    model.load(inputFile)
    pOpt = meshPartitionOptions()
    pOpt.setNumOfPartitions(nPartition)
    PartitionMesh(model, pOpt)
    model.save(outputFile)

  # *** Read the bathymetry *** # 
  def genBath() :
    f = open("data/bath.bin","rb")
    bin = f.read(8*6 + 4*3)
    if bin != "":
      Ox, Oy, Oz, dx, dy, dz, nx, ny, nz = struct.unpack('@ddddddiii', bin)
      data = array.array('d')
      data.fromfile(f, nx*ny)

    def bath (lon, lat) :
      rx = (lon-Ox)
      ry = (lat-Oy)
      iX = int(rx/dx)
      iY = int(ry/dy)
      alpha = (rx - iX*dx) / dx
      beta = (ry - iY*dy) / dy
      if (alpha > 1) or (alpha < 0) or (beta > 1) or (beta < 0) : print("ERROR")
      b = (1-alpha)*(1-beta)*data[iX*ny+iY] +alpha*(1-beta)*data[(iX+1)*ny+iY] +alpha*beta*data[(iX+1)*ny+(iY+1)] +(1-alpha)*beta*data[iX*ny+(iY+1)]
      b = -b
      if (b < 0) :  b = 5
      elif (b < 100) :  b = b + 5*exp(-b/5.0)
      elif (b < 210) :  b = b - 10*exp((b-210.)/10.)
      else : b = 200
      return b

    pj_latlong = pj_init_plus("+proj=latlong +ellps=WGS84")
    pj_merc = pj_init_plus("+proj=utm +ellps=WGS84 +zone=55 +south")
    def projectBath (xy) :
      lon, lat = pj_transform(pj_merc, pj_latlong, xy[0], xy[1])
      return bath(lon, lat)

    def bathymetry(b, xyz):
      for i in range(0,xyz.size1()):
        x = xyz.get(i,0)
        y = xyz.get(i,1)
        z = xyz.get(i,2)
        h = projectBath([x,y])
        b.set(i,0,h) 

    model = GModel()
    model.load(mesh1)
    groups = dgGroupCollection(model, 2, 1)
    xyz = groups.getFunctionCoordinates()
    bathymetryFunc = functionPython(1, bathymetry, [xyz])
    bathDof = dgDofContainer(groups, 1)
    bathDof.interpolate(bathymetryFunc)

    # *** Diffuse the bathymetry *** #
    bNu=20    # Diffusion
    bT=10000*2  # Time for smoothing 
    nuB = functionConstant(bNu)
    dlaw = dgConservationLawAdvectionDiffusion.diffusionLaw(nuB)
    dlaw.addBoundaryCondition(["coast", "island", "burdekinMouth", "openSeaNorth", "openSeaCentral", "openSeaSouth"], dlaw.new0FluxBoundary())
    #dlaw.addBoundaryCondition(["coast", "island", "burdekinMouth", "openSeaNorth", "openSeaCentral", "openSeaSouth"], dlaw.newSymmetryBoundary(""))
    sysPetsc = linearSystemPETScDouble()
    dof = dgDofManager.newCG (groups, 1, sysPetsc)
    implicitEuler = dgDIRK(dlaw, dof)
    implicitEuler.iterate (bathDof, bT, 0)
    exporterBath = dgIdxExporter(bathDof, "./bathSmooth")
    exporterBath.exportIdx()

  if Msg.GetCommRank() == 0 and Msg.GetCommSize() > 1:
    partition(inputMesh, mesh1, nPart)
  Msg.Barrier()
  genBath()
  if Msg.GetCommRank() == 0 :
    model = GModel()
    model.load(mesh1)
    groups = dgGroupCollection(model, 2, 1, True)
    bathDof = dgDofContainer(groups, 1)
    bathDof.readMsh("./bathSmooth/bathSmooth.idx")

    globNodeId2Bath = {}
    dataProx = fullMatrixDouble()
    for iGroup in range(groups.getNbElementGroups()) :
      group = groups.getElementGroup(iGroup)
      for iElement in range(group.getNbElements()) :
        el = group.getElement(iElement)
        bathDof.getGroupProxy(iGroup).getBlockProxy(iElement, dataProx)
        for iPt in range(group.getNbNodes()) :
          vId = el.getVertex(iPt).getNum()
          globNodeId2Bath[vId] = dataProx(iPt,0)

    H = 30
    nbLayers = 5#15
    def getLayers(e, v) :
      #dist = sqrt(pow(v[0] - 72180.503, 2) + pow(v[1] - (-46807.163),2))
      #h = max(min(H, dist/10e3*H),5) 
      h = globNodeId2Bath[v[3]]
      return [z * h /nbLayers for z in  range(nbLayers + 1)]
      #layers = [0]
      #for z in range(nbLayers) :
      #  layers.append(1 + z * (h-1)/(nbLayers-1)) # [1, ..., h]
      #return layers
    
    extrude(mesh(mesh1), getLayers).write(mesh3d)
    print('Info : Done extruding \'' + mesh1 + '\'')
    print('Info : Extruded mesh written in \'' + mesh3d + '\'')
  Msg.Barrier()
  return mesh3d
