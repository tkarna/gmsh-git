#!/usr/bin/python
#check inkscape options before editing :
# snap to point
# do not preserve path on remove

## Usage:
#First set stereoFlag and svgSize
#then run w/ args:
#arg1: input.svg, arg2: output.geo

import re, sys, math
import xml.sax.handler

R = 6.371e6
svgSize = 5000

defaultPhysicalTag = "island"

#stereoFlag: set 0 for cartesian, 1 for stereo (assumes svg was generated using gshhs2svg2.py's 'standard' GBR-stereo output
stereoFlag = 0
print ("stereo flag is %d" % stereoFlag)
# stereo : u = 2 R x / (R + z) ; v = 2 R y / (R + z)

########################################################################################
## parameters
########################################################################################
# Projection Parameters
from dgpy import *
pj_latlong = pj_init_plus("+proj=latlong +ellps=WGS84")
pj_merc = pj_init_plus("+proj=utm +ellps=WGS84 +zone=55 +south")

# inverse of the transformation defined in "svgOutputCoordinates" in gshhs2geo
# input from the svg file, output in cartesian coordinates on a sphere of radius 1
def inputCoordinates (u, v):
  if stereoFlag == 1:
    #NB: check know how svgSize fits into this before using...
    u = u / 50 - 10
    v = v / 50 - 10
    z =  (1 - u*u - v*v) / (1 + u*u + v*v)
    x = -u * (1 + z)
    y = -v * (1 + z)
  ## lon lat
  #previously:
  #v = 200 - v
  #lon = (u / 10 +140) / 180 * math.pi
  #lat = (v / 10 -30) / 180 * math.pi
  #x = math.cos(lat) * math.cos(lon)
  #y = math.cos(lat) * math.sin(lon)
  #z = math.sin(lat)
  #return x, y, z 
  else:
    f = svgSize / (2 * math.pi)
    lon = (u / f - math.pi)
    lat = math.pi / 2 - v / f
    x,y = pj_transform(pj_latlong, pj_merc, lon,lat)

    return x, y, 0
  
########################################################################################
## parameters
########################################################################################

class SVGHandler(xml.sax.handler.ContentHandler):
  def __init__(self, ofile):
    self.allAttractors={}
    self.inTitle = False
    self.inDescription = False
    self.ofile = ofile
    self.title =""
    #self.ip = 2
    self.ip = 0
    self.il = 0
    self.physicalLines = {}
    self.connectingNodes = [] 
    ofile.write("IP = newp;\nIL = newl;\nIS = news;\nIF = newf;\n");
    #if stereoFlag == 0:
    #  ofile.write("Point(IP + 0) = {0, 0, 0};\nPoint(IP + 1) = {0, 0, %e};\nPolarSphere(IS) = {IP + 0, IP + 1};\n" % R);
  def startElement(self, name, attributes):
    if name == "path":
      self.title = ""
      self.description = ""
      self.path = attributes["d"]
    elif name == "title":
      self.inTitle = True
    elif name == "desc":
      self.inDescription = True
  def characters(self, data):
    if self.inTitle:
      if data =='\n':
        self.inTitle = False
      else :
        self.title += data
    if self.inDescription:
      if data =='\n':
        self.inDescription = False
      else :
        self.description += data
  def endElement(self, name):
    if name == "path":
      self.inTitle = False
      self.inDescription = False
      if self.title == "":
        self.title = defaultPhysicalTag
      attractorList = [int(i) for i  in re.findall("Attractor\s*(\d+)", self.description)]
      if len(attractorList) == 0 :
        attractorList = [1]
      self.writePath(self.path, attractorList)
  def writeNode(self, node, attractorList):
    [x, y, z] = node
    if stereoFlag == 1:
      u =  2 * R * x / (1 + z)
      v =  2 * R * y / (1 + z)
    else:
      u = -x / (1 + z)
      v = -y / (1 + z)
    #ofile.write("Point(IP + %i) = {%.16e, %.16e, 0.};\n" % (self.ip, u, v));
    ofile.write("Point(IP + %i) = {%.16e, %.16e, 0.};\n" % (self.ip, x, y)); #c'est moi qui ai change ca
    self.addNodeInAttractors(self.ip, attractorList)
    self.ip = self.ip + 1
  def addNodeInAttractors(self, nodeId, attractorList) :
    for i in attractorList :
      if not i in self.allAttractors :
        self.allAttractors[i] = set()
      self.allAttractors[i].add(nodeId)
  def getConnectingNode(self, node, attractorList):
    for cnode in self.connectingNodes :
      d = [cnode[0] - node[0], cnode[1] - node[1], cnode[2] - node[2]]
      if (d[0] * d[0] + d[1] * d[1] + d[2] * d[2]) < (0.1 * svgSize / R) ** 2 :
        self.addNodeInAttractors(cnode[3], attractorList)
        return cnode[3]
    self.connectingNodes.append([node[0], node[1], node[2], self.ip])
    self.writeNode(node, attractorList)
    return self.ip - 1
  def writePath(self, path, attractorList):
    nodes = path.split()
    closed = (nodes[-1] == "z" or nodes[-1] == "Z")
    relative = (nodes[0] == "m")
    relativeFirst = True
    nodesonly = []
    for snode in nodes :
      if (len(snode.split(",")) == 2) :
        coords = snode.split(",")
        u = float(coords[0])
        v = float(coords[1])
        if (relative):
          if(relativeFirst):
            relativeFirst = False
          else:
            u = lastU + u
            v = lastV + v
          lastU = u
          lastV = v
        nodesonly.append(inputCoordinates(u, v))
    if len(nodesonly) < 2 :
      return
    if not closed :
      beginp = self.getConnectingNode(nodesonly[0], attractorList)
      endp = self.getConnectingNode(nodesonly[-1], attractorList)
      nodesonly.pop(0)
      nodesonly.pop(-1)
    firstp = self.ip
    for node in nodesonly:
      self.writeNode(node, attractorList)
    if len(nodesonly) > 2:
      if closed :
        ofile.write("BSpline(IL + %i) = {IP + %i : IP + %i, IP + %i};\n" % (self.il, firstp, self.ip - 1, firstp))
      else:
        ofile.write("BSpline(IL + %i) = {IP + %i, IP + %i : IP + %i, IP + %i};\n" % (self.il, beginp, firstp, self.ip - 1, endp))
    else:
      ofile.write("Line(IL + %i) = {IP + %i, IP + %i};\n" % (self.il, beginp, endp))
    if (self.title != ""):
      if (self.title in self.physicalLines):
        self.physicalLines[self.title].append(self.il)
      else:
        self.physicalLines[self.title] = [self.il]
    self.il = self.il + 1;
  def endDocument(self):
    for tag, lines in self.physicalLines.items():
      self.ofile.write("Physical Line (\"%s\") = {IL + %i" % (tag, lines[0]));
      for i in range(1, len(lines)):
        self.ofile.write(", IL + %i" % lines[i]);
      self.ofile.write("};\n");
    for id, nodes in self.allAttractors.items() :
      self.ofile.write("Field[%i] = Attractor;\n" % id)
      self.ofile.write("Field[%i].NodesList = {" % id)
      l = len(nodes)
      i = 0
      for n in nodes : 
        self.ofile.write("IP + %i" % (n));
        i += 1
        if i != l :
          self.ofile.write(", ");
      self.ofile.write("};\n");

parser = xml.sax.make_parser()
ofile = open(sys.argv[2], "w")
handler = SVGHandler(ofile)
parser.setContentHandler(handler)
parser.parse(sys.argv[1])
