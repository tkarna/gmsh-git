Include "GBR_TSV_data.geo";
Euclidian Coordinates;

//Field 2: Coast/islands threshold
Field[2] = Threshold;
Field[2].IField = 1;
Field[2].DistMin = 600;
Field[2].DistMax = 20000;
Field[2].LcMin = 1000;
Field[2].LcMax = 10000;

//Field 3 : Attractor from river mouth
Field[3] = Attractor;
Field[3].NodesList = {685};

//Field 4: river mouth threshold
Field[4] = Threshold;
Field[4].IField = 3;
Field[4].DistMin = 4000;
Field[4].DistMax = 40000;
Field[4].LcMin = 500;
Field[4].LcMax = 10000;


//Fields 5-7: Bathymetry
Field[5] = Structured;
Field[5].FileName = "data/bath.bin";
Field[6] = utm;
Field[6].IField = 5;
Field[6].Zone = 55;

Field[7] = Threshold;
Field[7].DistMax = -10;
Field[7].DistMin = -40;
Field[7].IField = 6;
Field[7].LcMax = 0;
Field[7].LcMin = 1;
Field[8] = MathEval;
Field[8].F = "2000 + 5e3*sqrt(F7)";
Field[9] = Min;
Field[9].FieldsList = {2, 4, 8};

Background Field = 9;

Mesh.CharacteristicLengthExtendFromBoundary = 0;
Mesh.CharacteristicLengthFromPoints = 0;
Mesh.LcIntegrationPrecision = 1e-3;
Mesh.Algorithm = 5;
//Mesh.MinimumCurvePoints = 4; //<-- Comment this out to lose small islands
Line Loop(13) = {6};
Line Loop(14) = {1};
Line Loop(15) = {11, 12, 7, 8, 9, 10};
Line Loop(16) = {3};
Line Loop(17) = {4};
Line Loop(18) = {5};
Line Loop(19) = {2};
Plane Surface(20) = {13, 14, 15, 16, 17, 18, 19};
Physical Surface("sea") = {20};

