Mesh.SurfaceEdges = 0;
Mesh.VolumeEdges = 0;
DefineConstant[
  AViewDepth = {0, Name "Cut a at given depth/View", Min 0, Max 10, Step 1,
    Help "View to cut", },
  BDepth = {5, Name "Cut a at given depth/Depth", Min 0, Max 1000, Step 1,
    Help "Depth (m)"},
  Crun = {"postpro/cutdepth.script", Name "Cut a at given depth/cut", Macro "Gmsh",
    Help Str["run the cutdepth scripts"], AutoCheck 0 }
];

DefineConstant[
  AViewTransect = {0, Name "Cut along a transect/View", Min 0, Max 10, Step 1,
    Help "View to cut"},
  BTransect = {0.75, Name "Cut along a transect/Position", Min 0, Max 1, Step 0.001,
    Help Str["Position of the transect", "0 correspond to the  north of the domain", "1 to the south of the domain"]},
  Crun = {"postpro/cuttransect.script", Name "Cut along a transect/cut", Macro "Gmsh",
    Help Str["run the cutdepth scripts"], AutoCheck 0}
];

DefineConstant[
  ZZ = {1000, Name "Z factor/factor", Min 1, Max 10000, Step 100,
    Help "Z factor"},
  ZZrun = {"postpro/transfozz.script", Name "Z factor/apply", Macro "Gmsh",
    Help Str["apply this factor to all views"], AutoCheck 0}
];
