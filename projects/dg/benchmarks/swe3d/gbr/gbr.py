from dgpy import *
import math, os, calendar, time
import prepro

# .-------------------------------.
# |           parameters          |
# '-------------------------------'

meshFile = prepro.process('GBR')
outputDirectory = 'output'
fullOutputDirectory = outputDirectory + "_full"
# Initial and final time [year, month, hour, minutes, seconds]
Tinitial = calendar.timegm([2008, 12, 18, 0, 0, 0])
Tfinal = calendar.timegm([2009, 02, 01, 0, 0, 0])

#enable the tides (topex)
tide = True

#enable the wind (ncep)
wind = True 

riverflow = slimTemporalSerie("data/flowRateBurdekin.txt")
riverSalinity = slimTemporalSerie("data/salinityBurdekin.txt")
riverSediment = slimTemporalSerie("data/sedimentBurdekin.txt")
seaSalinity = functionConstant(35)
seaSediment = functionConstant(0)

# .-------------------------------.
# | end of parameters             |
# '-------------------------------'

# create outputDirectory if it does not exist
if Msg.GetCommRank()==0 and not os.path.exists(outputDirectory) :
  os.mkdir(outputDirectory)
if Msg.GetCommRank()==0 and not os.path.exists(fullOutputDirectory) :
  os.mkdir(fullOutputDirectory)

slimSolver = slim3dSolver(meshFile, ['bottom_sea'], ['top_sea'])
# options
slimSolver.setSolveS(1)
slimSolver.setSolveSImplicitVerticalDiffusion(1)
slimSolver.setSolveSed(0)
slimSolver.setSolveSedImplicitVerticalDiffusion(0)
slimSolver.setSolveUVImplicitVerticalDiffusion(1)
slimSolver.setSolveTurbulence(1)
slimSolver.setAdvectTurbulentEnergy(1)
slimSolver.setComputeBottomFriction(1)
slimSolver.exportDirectory = fullOutputDirectory
slimSolver.turbulenceSetupFile = 'gbrTurb.nml'
#slimSolver.setUseConservativeALE(1)
#slimSolver.setFlagUVLimiter(0)
#slimSolver.setFlagSLimiter(0)

d = slimSolver.getDofs(True)
f = slimSolver.functions
f.initializeBath()

# .......................
#       forcings         
# .......................

proj = slimLonLat(slimSolver.extrusion(), 3, "+proj=utm +ellps=WGS84 +zone=55 +south")

if tide :
  tideSSE = slimFunctionTpxo('data/h_tpxo7.2.nc','ha','hp','lon_z','lat_z', proj.lonLatDegree(), f.timeFunc)
  tideU = slimFunctionTpxo('data/u_tpxo7.2.nc','Ua','up','lon_u','lat_u', proj.lonLatDegree(), f.timeFunc)
  tideV = slimFunctionTpxo('data/u_tpxo7.2.nc','Va','vp','lon_v','lat_v', proj.lonLatDegree(), f.timeFunc)
  tideUV = slimLonLatVectorFunction(proj, tideU, tideV)
  tideuv = slimTransportToVelocityFunction(tideUV, f.bathFunc2d, d.etaDof2d.getFunction())

if wind :
  windInterpolatorU = slimStructDataInterpolatorMultilinear()
  windInterpolatorV = slimStructDataInterpolatorMultilinear()
  windUcontainer = slimStructDataContainerNetcdf('data/wnd10m_csfr_2008-2010.grb2.nc', 'U_GRD_L103', 'lat', 'lon')
  windVcontainer = slimStructDataContainerNetcdf('data/wnd10m_csfr_2008-2010.grb2.nc', 'V_GRD_L103', 'lat', 'lon')
  windU = slimFunctionStructData(windUcontainer, windInterpolatorU, proj.latLonDegree())
  windV = slimFunctionStructData(windVcontainer, windInterpolatorV, proj.latLonDegree())
  windU.setTimeFunction(f.timeFunc)
  windV.setTimeFunction(f.timeFunc)
  windf = slimLonLatVectorFunction(proj, windU, windV)
  f.windStressFunc = slimCubicWindStress(windf, 0., 6.3e-4, 6.6e-5)


# ----------------------------

f.TInitFunc = functionConstant(15)
#f.SInitFunc = functionPython(1, initS, [f.xyzFunc3d])
f.SInitFunc = seaSalinity
f.sedInitFunc = seaSediment

bgHDiff = 1
f.nuhTotal = functionConstant(bgHDiff)
f.kappahTotal = functionConstant(bgHDiff)

f.z0BFunc = functionConstant(0.005)
f.z0SFunc = functionConstant(0.02)
f.etaInitFunc = functionConstant(0.)
Tday = 0.99726968*24*60*60; # sidereal time of Earth revolution
OmegaEarth = 2*math.pi/Tday
latDeg = -19.2
phi = (math.pi/180)*latDeg # latitude in radians
f.coriolisFunc = functionConstant(2*OmegaEarth*math.sin(phi)) # // [rad/s] Coriolis parameter


# .-------------------------------.
# |       create equations        |
# '-------------------------------'
e = slimSolver.createEquations()
d = slimSolver.getDofs()

eta = d.etaDof2dCG.getFunction()
uv = d.uvDof.getFunction()
uvAv2d = d.uvAvDof2d.getFunction()
uvRiverFunc = slimFlowRateToVelocity(slimSolver.groups2d, 'burdekinMouth', riverflow, f.bathFunc2d, eta)


horMomEq = e.horMomEq
horMomEq.setLaxFriedrichsFactor(0.0) # maybe a remedy for instability, increases |v| a lot
if tide :
  horMomBndOpen = horMomEq.newOutsideValueBoundaryGeneric([uv, eta],[tideuv, tideSSE])
else :
  horMomBndOpen = horMomEq.newBoundaryOpenFree()
#horMomBndCoast = horMomEq.newBoundaryWallLinear(0.5)
horMomBndCoast = horMomEq.newBoundaryWall()
horMomBndWall = horMomEq.newBoundaryWall()
if slimSolver.getSolveUVImplicitVerticalDiffusion() or not wind :
  horMomBndTop = horMomEq.new0FluxBoundary() #zero
else :
  horMomBndTop = horMomEq.newBoundarySurface(f.windStressFunc)
horMomBndSymm = horMomEq.newSymmetryBoundary()
horMomBndRiver = horMomEq.newOutsideValueBoundaryGeneric([uv],[uvRiverFunc])
horMomEq.addBoundaryCondition(['openSeaNorth','openSeaSouth','openSeaCentral'],horMomBndOpen)
horMomEq.addBoundaryCondition(['coast','island'],horMomBndCoast)
horMomEq.addBoundaryCondition('burdekinMouth',horMomBndRiver)  ## fix for inlet
#horMomEq.addBoundaryCondition('burdekinMouth',horMomBndSymm)  ## fix for inlet
horMomEq.addBoundaryCondition('top_sea',horMomBndTop) # zero for nonconst tracers!
horMomEq.addBoundaryCondition('bottom_sea',horMomBndWall) # must be wall for bath!

if slimSolver.getSolveUVImplicitVerticalDiffusion() :
  if wind :
    e.vertMomUEq.addBoundaryCondition('top_sea', e.vertMomUEq.newBoundarySurface(f.windStressFunc))
  else :
    e.vertMomUEq.addBoundaryCondition('top_sea', e.vertMomUEq.new0FluxBoundary())
  e.vertMomUEq.addBoundaryCondition(['openSeaNorth','openSeaSouth','openSeaCentral','coast','island','burdekinMouth'], e.vertMomUEq.new0FluxBoundary())
  e.vertMomUEq.addBoundaryCondition('bottom_sea', e.vertMomUEq.newBoundaryBottom(f.bottomFrictionFunc, f.zBotDistFunc))

e.wEq.addBoundaryCondition(['openSeaNorth','openSeaSouth','openSeaCentral','burdekinMouth','top_sea'], e.wEq.newSymmetryBoundary())
e.wEq.addBoundaryCondition(['coast','island','bottom_sea'], e.wEq.new0FluxBoundary())

if slimSolver.getSolveS() :
  e.SEq.setLaxFriedrichsFactor(0.0)
  #e.SEq.addBoundaryCondition(['burdekinMouth'],e.SEq.newInFluxBoundary(riverSalinity))
  e.SEq.addBoundaryCondition(['burdekinMouth'],e.SEq.newOutsideValueBoundary(riverSalinity))
  e.SEq.addBoundaryCondition(['openSeaNorth','openSeaSouth','openSeaCentral'],e.SEq.newInFluxBoundary(seaSalinity))
  e.SEq.addBoundaryCondition(['coast','island','top_sea','bottom_sea'], e.SEq.new0FluxBoundary())

if slimSolver.getSolveSed() :
  e.sedEq.setLaxFriedrichsFactor(0.0)
  e.sedEq.addBoundaryCondition(['burdekinMouth'],e.SEq.newInFluxBoundary(riverSediment))
  e.sedEq.addBoundaryCondition(['openSeaNorth','openSeaSouth','openSeaCentral'],e.SEq.newInFluxBoundary(seaSediment))
  e.sedEq.addBoundaryCondition(['coast','island','top_sea'], e.SEq.new0FluxBoundary())


if slimSolver.getAdvectTurbulentEnergy() :
  for eq in [e.tkeAdvEq, e.epsAdvEq] :
    eq.setLaxFriedrichsFactor(0.0)
    eq.addBoundaryCondition(['openSeaNorth','openSeaSouth','openSeaCentral','burdekinMouth'], eq.newInFluxBoundary(f.tinyFunc))
    eq.addBoundaryCondition(['coast','island','top_sea','bottom_sea'], eq.new0FluxBoundary())

eta2dEq = e.eta2dEq
etaBndZero = eta2dEq.new0FluxBoundary()
if tide : 
  etaBndOpen = eta2dEq.newOutsideValueBoundaryGeneric([function.getSolution(),uvAv2d],[tideSSE, tideuv])
else :
  etaBndOpen = eta2dEq.newBoundaryOpenFree()
etaBndRiver = eta2dEq.newOutsideValueBoundaryGeneric([uvAv2d],[uvRiverFunc])
eta2dEq.addBoundaryCondition(['openSeaNorth','openSeaSouth','openSeaCentral'],etaBndOpen)
eta2dEq.addBoundaryCondition(['coast', 'island'],etaBndZero)
eta2dEq.addBoundaryCondition('burdekinMouth',etaBndRiver)

uv2dEq = e.uv2dEq
uv2dBndWall = uv2dEq.newBoundaryWall()
uv2dBndSymm = uv2dEq.newSymmetryBoundary()
if tide : 
  uv2dBndOpen = uv2dEq.newOutsideValueBoundaryGeneric([function.getSolution(),eta],[tideuv, tideSSE])
else :
  uv2dBndOpen = uv2dEq.newBoundaryOpenFree()
uv2dBndRiver = uv2dEq.newOutsideValueBoundaryGeneric([uvAv2d],[uvRiverFunc])
uv2dEq.addBoundaryCondition(['openSeaNorth','openSeaSouth','openSeaCentral'],uv2dBndOpen)
uv2dEq.addBoundaryCondition(['coast','island'],uv2dBndWall)
uv2dEq.addBoundaryCondition('burdekinMouth',uv2dBndRiver)


# .-------------------------------.
# |     setup time integrator     |
# '-------------------------------'
timeIntegrator = slim3dTimeIntegratorPC(slimSolver)

rk = dgERK(e.eta2dEq, None, DG_ERK_EULER)
dtRK = rk.computeInvSpectralRadius(d.etaDof2d)
CFL = 0.7236
dt = CFL*dtRK
maxUV = 1.5 # max advective velocity
c2d = math.sqrt(9.81*20.)
dt = c2d/(c2d+maxUV) * dt
if Msg.GetCommRank() == 0 :
  Msg.Info('Runge-Kutta dt :' + str(dtRK) + ' CFL:' + str(CFL) + ' dt:' + str(dt))


#dt3d = dtRK * math.sqrt(9.81*20) / maxUV
#M = int(0.72*dt3d/dt)
M = 20
if Msg.GetCommRank() == 0 :
  Msg.Info('Mode split ratio M :' + str(M))

# .-------------------------------.
# |     setup export options      |
# '-------------------------------'
timeIntegrator.setExportInterval(600)
timeIntegrator.setExportAtEveryTimeStep(False)
timeIntegrator.setFullExportAtEveryTimeStep(False)
timeIntegrator.setFullExportIntervalRatio(10)

# list of fields to export
export = []
export.append(dgIdxExporter(d.SDof, outputDirectory + "/salinity"))
export.append(dgIdxExporter(d.uvDof, outputDirectory + "/uv", True))
export.append(dgIdxExporter(d.etaDof2d, outputDirectory + "/eta"))
if (slimSolver.getSolveSed()) :
  export.append(dgIdxExporter(d.sedDof, outputDirectory + "/sediment"))
  export.append(dgIdxExporter(d.sedGroundDof2d, outputDirectory + "/sedimentGround"))

#initial export
for e in export :
  e.exportIdx(timeIntegrator.getExportCount(), timeIntegrator.getTime() - Tinitial)

# .--------------------.
# |     main loop      |
# '--------------------'
timeIntegrator.setTimeStep(dt)
timeIntegrator.set3dTimeStepRatio(M)
timeIntegrator.setInitTime(Tinitial)
timeIntegrator.setEndTime(Tfinal)
timeIntegrator.initializeFields()

#odirOld = 'fullOld'
#timeIntegrator.reStart(odirOld,2)

tic = time.time()
while timeIntegrator.getTime() < timeIntegrator.getEndTime() :
  timeIntegrator.advanceOneTimeStep()
  timeIntegrator.checkSanity()
  timeIntegrator.checkTracerConsistency()
  if timeIntegrator.checkExport() :
    Msg.Info("export %i" % timeIntegrator.getExportCount())
    for e in export :
      e.exportIdx(timeIntegrator.getExportCount(), timeIntegrator.getTime() - Tinitial)
  Msg.Info("%5i [ %s ] normuv %6.12e clock %.1fs" % (timeIntegrator.getIter(),time.strftime("%d %b %Y %H:%M:%S", time.gmtime(timeIntegrator.getTime())), d.uvDof.norm(), time.time() - tic))
  if timeIntegrator.checkFullExport() :
    timeIntegrator.exportAll()
timeIntegrator.terminate(0)
