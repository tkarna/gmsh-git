from dgpy import *
from gmshpy import *

import time, math

model = GModel  ()
model.load ('square.msh')
order = 1
dimension = 2
groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag();

def n(FCT, XYZ):
  for i in range(0,FCT.size1()):
    x = XYZ(i,0)
    y = XYZ(i,1)
    if(x < 1.1):# or x > .9):
      FCT.set(i,0,0)
      FCT.set(i,1,1.0)
      FCT.set(i,2,0)
    else:
      FCT.set(i,0,-.5)
      FCT.set(i,1,.5)
      FCT.set(i,2,0)
    
def g(FCT, XYZ):
  for i in range(0,FCT.size1()):
    x = XYZ(i,0)
    if(x < .5):
      FCT.set(i,0,1)
    else:
      FCT.set(i,0,0.5)
    
def Lambda(FCT, XYZ):
  for i in range(0,FCT.size1()):
    #FCT.set(i,0,10000.0	)
    x = XYZ(i,0)
    if(x > .4 and x < .6):
      FCT.set(i,0,0.0	)
    else:
      FCT.set(i,0,1	)
    
def outsidevalue(FCT):
  for i in range(0,FCT.size1()):
    FCT.set(i,0,0)
    
def outsidevalue2(FCT):
  for i in range(0,FCT.size1()):
    FCT.set(i,0,1)
    
def initialcond(FCT):
  for i in range(0,FCT.size1()):
    FCT.set(i,0,0)
    

claw = dgConservationLawVerticalCoord()
solution = dgDofContainer(groups, claw.getNbFields())

XYZ = getFunctionCoordinates();
FS2 = functionPython(3, n, [XYZ])
FS3 = functionPython(1, g, [XYZ])
FS4 = functionPython(1, Lambda, [XYZ])
FS5 = functionPython(1, outsidevalue, [])
FS6 = functionPython(1, outsidevalue2, [])
claw.setIsoDiffFull(FS2,FS4,FS3)
FS7 = functionPython(1, initialcond, [])
#solution.interpolate(FS7)



Bound0 = claw.new0FluxBoundary()
#Bound1 = claw.newOutsideValueBoundary("Top",FS5)
#Bound2 = claw.newOutsideValueBoundary("Rest",FS6)
claw.addBoundaryCondition("Rest",Bound0)
claw.addBoundaryCondition("Top",Bound0)
claw.addStrongBoundaryCondition(1, "Top", FS5)
#claw.addStrongBoundaryCondition(1, "Rest", FS6)


print "TEST"
groups.buildGroupsOfInterfaces()
#solution.exportMsh('init1', 0., 0)
print "TEST2"

#rk=dgRungeKutta()
sys = linearSystemPETScDouble()
sys.setParameter("petscOptions","-pc_type lu -ksp_monitor")
dof = dgDofManager.newCG (groups, 1, sys)
steady = dgSteady(claw,dof);
steady.getNewton().setRtol(10e-7)
#print '*** Solve the System ***'
steady.getNewton().setMaxIt(2)
steady.getNewton().setVerb(10)
steady.solve(solution)
solution.exportMsh("solutionSteady", 0, 0)
"""
t=0
nbExport = 0
tic = time.clock()
for i in range (1,5000) :
  dt = 0.0000001
  print i
  norm = rk.iterate44ThreeEight(claw,t,dt,solution)
  t = t +dt
  if ( i%10 ==0 ):
   print('|ITER|',i,'|NORM|',norm,'|DT|',dt, '|T|',t)
   solution.exportMsh("solution1-%06d" % (i), t, nbExport)
   nbExport = nbExport + 1
print (time.clock()-tic)
"""
Msg.Exit(0)

