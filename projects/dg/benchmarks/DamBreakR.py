from gmshpy import *
from dgpy import *
import time

model = GModel()
model.load ('dam.msh')
order = 1
dimension = 2
groups = dgGroupCollection(model, dimension, order)

def initial_condition(f , XYZ ):
  for i in range(0,XYZ.size1()):
    x = XYZ.get(i,0)
    y = XYZ.get(i,1)
    z = XYZ.get(i,2)
    if (y*y+x*x)>0.05:
      f.set(i, 0, 0)
    else:
      f.set(i, 0, 5)
      f.set(i, 1, 0)
      f.set(i, 2, 0)

claw = dgConservationLawShallowWater2d()
XYZ = getFunctionCoordinates()
FS = functionPython(3, initial_condition, [XYZ])
solution = dgDofContainer(groups, claw.getNbFields())
solution.L2Projection(FS)
claw.setIsLinear(1)
f4 = functionConstant([10])
claw.setBathymetry(f4)

bndWall = claw.newBoundaryWall()
claw.addBoundaryCondition('Wall', bndWall)

groups.buildGroupsOfInterfaces()
rk=dgRungeKutta()
limiter = dgSlopeLimiter(claw)
rk.setLimiter(limiter) 

limiter.apply(solution)
solution.exportMsh('output/dambreak-init', 0., 0)

t=0
nbExport = 0
tic = time.clock()
for i in range (1,2000+1) :
  dt = 0.0005
  norm = rk.iterate22(claw,t,dt,solution)
  t = t +dt
  if ( i%10 ==0 ):
     print('|ITER|',i,'|NORM|',norm,'|DT|',dt, '|T|',t)
     solution.exportMsh("output/dambreak-%06d" % (i), t, nbExport)
     nbExport = nbExport + 1
print (time.clock()-tic)
Msg.Exit(0)
