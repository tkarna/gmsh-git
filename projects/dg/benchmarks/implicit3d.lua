function dirichlet( XYZ, FCT )
  for i=0,XYZ:size1()-1 do
    FCT:set(i,0,math.sin(6*(XYZ:get(i,0)-0.25))*math.cos(6*XYZ:get(i,1))) 
  end
end


XYZ = functionCoordinates.get();
FS = functionLua(1, 'dirichlet', {XYZ})

model = GModel()
model:load ('box.msh')
order = 1
dimension = 3

nu = functionConstant({1})
v = functionConstant({0,0,5})
law = dgConservationLawAdvectionDiffusion.diffusionLaw(nu)
law:addBoundaryCondition('outlet',law:newOutsideValueBoundary("volume",functionConstant({0})))
law:addBoundaryCondition('inlet',law:newOutsideValueBoundary("volume",functionConstant({1})))
--law:addBoundaryCondition('wall',law:newSymmetryBoundary("volume"))
--law:addBoundaryCondition('symmetry',law:newSymmetryBoundary("volume"))
law:addBoundaryCondition('wall',law:new0FluxBoundary("volume"))
law:addBoundaryCondition('symmetry',law:new0FluxBoundary("volume"))
groups = dgGroupCollection(model, dimension, order)
--groups:splitGroupsRandom(2)
groups:buildGroupsOfInterfaces()
solution = dgDofContainer(groups, law:getNbFields())

-- dof = dgDofManager.newDGBlock (groups, 1, linearSystemPETScBlock())
dof = dgDofManager.newDG (groups, 1, linearSystemPETSc())
--dof = dgDofManager.newCG (groups, 1, linearSystemPETSc(), law)
--dof = dgDofManager.newCGStrongBC (groups, 1, linearSystemPETSc(), law)

--dgSolver.solve(solution, law, dof)
dgSolver.implicitEuler(solution, law, dof, 0.2, 0)

solution:exportMsh("laplace", 0., 0) 
