LcCoast = 10e4;
LcCoastJapan = 1e4;
LcFar=10*LcCoast;

pi = 3.14159265;
Field[1]=Box;
Field[1].XMin=120.3*pi/180;
Field[1].XMax=155*pi/180;
Field[1].YMin=28.2*pi/180;
Field[1].YMax=47.9*pi/180;
Field[1].ZMin=-1e9;
Field[1].ZMax=1e9;
Field[1].VIn=LcCoastJapan;
Field[1].VOut=LcCoast;
Field[2] = LonLat;
Field[2].IField = 1;

Plugin(GSHHS).InFileName="gshhs_f.b";
Plugin(GSHHS).OutFileName="world_bnd.geo";
Plugin(GSHHS).iField=2;
Plugin(GSHHS).MinStraitsFactor=0.5;
Plugin(GSHHS).WritePolarSphere=-2;
//Plugin(GSHHS).Run;
/*

Field[20]=MathEval;
Field[20].F="(4 * 6371e3* 6371e3 * x) / (4 * 6371e3*6371e3 + x*x + y*y)";
Field[21]=MathEval;
Field[21].F="(4 * 6371e3*6371e3 * y) / (4 * 6371e3*6371e3 + x*x + y*y)";
Field[22]=MathEval;
Field[22].F=" 6371e3* (4 * 6371e3*6371e3 - x*x - y*y) / (4 * 6371e3*6371e3 + x*x + y*y)";
Merge "world_bnd.geo";

//Physical lines
nloop_l=newl;
Physical Line("Coast")={1:nloop_l-1};
nloop_ll=newll;
Plane Surface(1)={1:nloop_ll-1};
Physical Surface("Domain")={1};
//Recombine Surface{1};
Mesh.RecombinationAlgorithm = 1;
//Mesh.Algorithm=5;
Mesh.LcIntegrationPrecision=1e-3;
Mesh.CharacteristicLengthExtendFromBoundary = 0;

Field[5] = Structured;
Field[5].FileName = "etopo2.bin";
Field[6] = LonLat;
Field[6].FromStereo = 1;
Field[6].IField = 5;

//inside or outside the pacific
Field[200] = Structured;
Field[200].FileName = "mask_pacific.bin";
Field[201] = LonLat;
Field[201].FromStereo = 1;
Field[201].IField = 200;
Field[202] = MathEval;
Field[202].F="10+F201*10";

Field[7] = MathEval;
Field[7].F = "Sqrt(Fabs(F6))";
Field[8] = Threshold;
Field[8].DistMax = 100;
Field[8].LcMax = 1;
Field[8].LcMin = 0.6;
Field[8].DistMin = 10;
Field[8].IField = 7;
Field[8].Sigmoid = 1;
Field[9] = Min;
Field[9].FieldsList = {8, 4};
Field[1000]=MathEval;
Field[1000].F = "(4 * 6371e3^2)/(4*6371e3^2 + x*x + y*y)";
Field[100] = MathEval;
Field[100].F="min(F105*F500*F311*F411*F410*F611*F8*F202,600e3)/F1000";
Background Field = 100;

//Hessian(H)
Field[103]=MaxEigenHessian;
Field[103].Delta = 8000;
Field[103].IField = 6;
Field[102]=Threshold;
Field[102].IField = 103;
Field[102].LcMax=1;
Field[102].LcMin=1e-6;
Field[102].DistMax=1;
Field[102].DistMin=1e-6;
Field[104]=MathEval;
Field[104].F = "1/Sqrt(Fabs(F102))";
Field[105] = Threshold;
Field[105].IField = 104;
Field[105].LcMin = 1;
Field[105].LcMax = 0.3;
Field[105].DistMax = 200;
Field[105].DistMin = 800;

//background scale size
Field[500] = MathEval;
Field[500].F = "100e3";

//JAPAN
Field[400] = Attractor;
Field[400].EdgesList = {128,171,134,44,129,144,125,139,115,126,136};
Field[400].FieldX = 20;
Field[400].FieldY = 21;
Field[400].FieldZ = 22;
Field[400].NNodesByEdge = 400;

Field[410] = Threshold;
Field[410].IField = 400;
Field[410].LcMax = 1;
Field[410].LcMin = 1;//ca change rien pour le moment
Field[410].DistMax = 1200e3;
Field[410].DistMin = 400e3;
Field[410].Sigmoid = 0;

//Japan East Coast
Field[401] = Attractor;
Field[401].NodesList = {5800:5973,7133:7187,6790:6887};
Field[401].FieldX = 20;
Field[401].FieldY = 21;
Field[401].FieldZ = 22;
Field[401].NNodesByEdge = 40000;

Field[411] = Threshold;
Field[411].IField = 401;
Field[411].LcMax = 1;
Field[411].LcMin = 0.3;
Field[411].DistMax = 1200e3;
Field[411].DistMin = 400e3;
Field[411].Sigmoid = 0;


//PACIIC
Field[300] = Attractor;
Field[300].FieldX = 20;
Field[300].FieldY = 21;
Field[300].FieldZ = 22;
Field[300].NodesList = {2677:2887, 3939:4297, 5364:5399, 6293:6348, 7369:7378, 1665:2668, 5000:5010, 4704:4857};
//small island
Field[301] = Attractor;
Field[301].FieldX = 20;
Field[301].FieldY = 21;
Field[301].FieldZ = 22;
Field[301].EdgesList = {15, 17, 47, 105, 63, 87, 112, 90, 106, 107, 116, 71, 35, 79, 122, 6, 78, 113, 86, 46, 74, 45, 118, 99, 22, 54, 55, 58, 66, 114, 20, 57, 36, 34,97,102,119,103,68,81,70,38,96};
Field[301].NNodesByEdge = 400;
Field[310] = Min;
Field[310].FieldsList = {300, 301};

Field[311] = Threshold;
Field[311].IField = 310;
Field[311].LcMax = 1;
Field[311].LcMin = 0.6;
Field[311].DistMax = 200e3;
Field[311].DistMin = 100e3;
Field[311].Sigmoid = 0;


//Antartic
Field[600] = Attractor;
Field[600].FieldX = 20;
Field[600].FieldY = 21;
Field[600].FieldZ = 22;
Field[600].EdgesList = {3};
Field[600].NNodesByEdge = 100000;

Field[611] = Threshold;
Field[611].IField = 600;
Field[611].LcMax = 2;
Field[611].LcMin = 1;
Field[611].DistMax = 500e3;
Field[611].DistMin = 1000e3;
Field[611].Sigmoid = 0;

