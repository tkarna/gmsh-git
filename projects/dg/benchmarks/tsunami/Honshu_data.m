% Data from Honshu earthquake
close all; clc; clear all;

Lon = 142.37;   % deg.
Lat = 38.32;    % deg.
Re  = 6371;     % km

depth  = 24.4;  % km 
length = 400;   % km
width  = 150;   % km

strike = 193;   % deg.
dip    = 14;    % deg.

rake   = 81;    % deg.

slip   = 14;    % m (?? large impact on amplitude)
open   = 0;     % m (?? no data -> 0?)


[E,N] = meshgrid(-200:5:200, -300:5:300);
[uE,uN,uZ] = okada85(E,N,depth,strike,dip,length,width,rake,slip,open);

lon_range = Lon + E*360/(2*pi*cos(Lat*pi/180)*Re);
lat_range = Lat + N*360/(2*pi*Re);
e_range = (lon_range-Lon)*2*pi/360*cos(Lat*pi/180)*Re;
n_range = (lat_range-Lat)*2*pi/360*Re;

figure
set(gcf,'Unit','normal','Color',[1 1 1],'Name','Honshu earthquake - sea surface displacement [m]')
set(gcf,'position',[0.0805 0.4896 0.147 0.42],'paperpositionmode','auto')
set(gca,'fontsize',15)
contourf(e_range,n_range,uZ); hold on; plot(0,0,'p','markersize',15,'markerfacecolor','w','markeredgecolor','k','linewidth',2)
axis equal; axis tight; colorbar
xlabel('Longitude'); ylabel('Latitude'); title('Sea surface displacement [m]')

%print -depsc Honshu_SeaSurfaceDisplacement.eps
ee=E(1,:);
nn=N(:,1);

fid=fopen('HonshuInitialCondition.bin','w');
fwrite(fid,[ee(1) nn(1) 0],'double');
fwrite(fid,[ee(2)-ee(1) nn(2)-nn(1) 0],'double');
fwrite(fid,[max(size(ee)) max(size(nn)) 1],'int');
fwrite(fid,uZ,'double')
fclose(fid);
