# -*- coding: utf-8 -*-
from dgpy import *
import time
from math import *
import sys
import os
sys.path.append("../")	#because we are in a next folder compared with 'benchmarks'
from smoothingBath import*

model = GModel()
model.load('box.msh')
order = 1
dimension = 2

if (Msg.GetCommRank()==0):
  functionC.buildLibraryFromFile ("BATH.cc", "BATH.so")

XYZ = getFunctionCoordinates()
groups = dgGroupCollection(model, dimension, order)
groups.buildGroupsOfInterfaces()

bath = dgDofContainer(groups, 1)
bathS = functionC("BATH.so", "bath", 1, [XYZ])
bath.interpolate(bathS)

physicalName = ['boundary']	#full boundary: only one!!!
bNu = 50    # dynamic viscosity
fT=10000	#time for the diffusion
smoothingBath(groups,bath,physicalName,bNu,fT)
