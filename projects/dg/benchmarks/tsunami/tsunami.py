from gmshpy import *
from dgpy import *
import time, math, os
import os.path
import os
import sys

def loadAndPartitionMesh(model, fileName) :
  if (Msg.GetCommSize() > 1) :
    Msg.Info("partition mesh")
    if(Msg.GetCommRank()==0) :
      m2 = GModel()
      m2.load(fileName + '.msh')
      pOpt = meshPartitionOptions()
      pOpt.setNumOfPartitions(Msg.GetCommSize())
      pOpt.partition(m2)
      m2.save(fileName + '_partitioned.msh')
    Msg.Info("partition done")
    Msg.Barrier()
    model.load(fileName + '_partitioned.msh')
  else :
    model.load(fileName + '.msh')

def printTime (t) :
  return "%dh%02d'%02d\"" % (t/3600, (t%3600)/60, t%60)

model = GModel()
loadAndPartitionMesh(model, 'world_p3')

order = 3
dimension = 2
outputDir="tsunami/"

if (Msg.GetCommRank()==0):
  functionC.buildLibraryFromFile ("tsunami.cc", "tsunami.so");
  functionC.buildLibraryFromFile ("BATH.cc", "BATH.so");
Msg.Barrier()

os.putenv("BATHFILE", "etopo2.bin")
stereo = dgSpaceTransformSpherical(6371220.)
groups = dgGroupCollection(model, dimension, order, stereo)
XYZ = groups.getFunctionCoordinates();
groups.buildGroupsOfInterfaces()
bath = dgDofContainer(groups, 1);
if(os.path.exists(outputDir+"/bath_smooth_COMP_0.msh")):
  Msg.Info('.... Import Default Smoothed Bathymetry')
  bath.importMsh2(outputDir + "/bath_smooth")
else: 
  bathS = functionC("BATH.so", "bath", 1, [XYZ])
  bath.interpolate(bathS);
  bath.exportMsh(outputDir+"bath", 0, 0);
  nuB = functionConstant(1e6)
  dlaw = dgConservationLawAdvectionDiffusion.diffusionLaw(nuB)
  dlaw.addBoundaryCondition("Coast", dlaw.new0FluxBoundary())
  sys = linearSystemPETScDouble()
  dof = dgDofManager.newCG (groups, 1, sys)
  implicitEuler = dgDIRK(dlaw, dof)
  implicitEuler.iterate (bath, 1, 0)
  bath.bezierCrop(10, 2e6)
  bath.exportMsh(outputDir+"bath_crop", 0, 0);
  bathP = functionC("BATH.so", "diff", 1, [function.getSolution(), bath.getFunction()])
  nuB = functionConstant(0)
  dlaw = dgConservationLawAdvectionDiffusion.diffusionLaw(nuB)
  dlaw.addBoundaryCondition("Coast", dlaw.new0FluxBoundary())
  dlaw.setSource(bathP)
  sys = linearSystemPETScDouble()
  sys.setParameter("petscOptions", "-pc_type lu");
  dof = dgDofManager.newCG (groups, 1, sys)
  solver = dgSteady(dlaw, dof)
  solver.getNewton().setVerb(10)
  solver.getNewton().setMaxIt(6)
  solver.solve(bath)
  bath.exportMsh(outputDir+"bath_smooth", 0, 0);

sys = 0
dof = 0
claw = dgConservationLawShallowWater2d()
solution = dgDofContainer(groups, claw.getNbFields())
Finit = functionC("tsunami.so", "initial_condition_okada_honshu",3, [XYZ])
solution.interpolate(Finit)
FCoriolis= functionC("tsunami.so", "coriolis", 1, [XYZ])
claw.setBathymetry(bath.getFunction())
claw.setBathymetryGradient(bath.getFunctionGradient())
claw.setCoriolisFactor(FCoriolis)
claw.setIsLinear(True)
claw.setIsSpherical(1, XYZ)
wall=claw.newBoundaryWall()
claw.addBoundaryCondition("Coast", wall)


bottomDrag = functionC("tsunami.so", "bottomDrag", 1, [function.getSolution(), bath.getFunction()])
claw.setLinearDissipation(bottomDrag)
solution.exportMsh(outputDir + 'init', 0., 0)

rk=dgERK(claw,None,DG_ERK_44)
t=0
dt=5
evalSol=dgFunctionEvaluator(groups, solution.getFunction())

solution.exportMsh(outputDir + "solution-000000", 0., 0)
nbExport = 0
tic = time.clock()
for i in range (0,20000) :
  norm = rk.iterate(solution,dt,t)
  t = t + dt
  #Msg.Info("%d    norm = %.1e    t = %s    cpu = %s" % (i, norm, printTime(t), printTime(time.clock()-tic)))
  print i, " norm=", solution.norm(), "PHYS time=", printTime(t), "CPU time=", printTime(time.clock()-tic)
  if ( i%60 ==0 ):
    print "exporting solution"
    solution.exportMsh(outputDir + "solution-%06d" % (i), t, nbExport)
    nbExport = nbExport + 1
  timeSerie = open("dart_stations",'r')
  line = (timeSerie.readline()).split()
  while line:
    stationName = line[0]
    lon = float(line[1])
    lat = float(line[2])
    line = (timeSerie.readline()).split()
    result  = fullMatrixDouble(1,1)
    R = 6371220
    x = R*math.cos(lon*math.pi/180)*math.cos(lat*math.pi/180)
    y = R*math.sin(lon*math.pi/180)*math.cos(lat*math.pi/180)
    z = R*math.sin(lat*math.pi/180)
    xs = 2*R*x/(R+z)
    ys = 2*R*y/(R+z)
    evalSol.compute(xs,ys,0.0,result)
    if (i==1):
      tsFile = open(stationName,"w")
    else:
      tsFile = open(stationName,"a")
    tsFile.write("%g %g\n" % (t,result.get(0,0)))
    tsFile.close()
  timeSerie.close()

Msg.Exit(0)
