from dgpy import *
import time, math, os
import os.path
import os
import sys

def loadAndPartitionMesh(model, fileName, ng) :
  outputDir="tsunami/"
  if (Msg.GetCommSize() > 1) :
    Msg.Info("partition mesh")
    if(Msg.GetCommRank()==0) :
      m2 = GModel()
      m2.load(fileName + '.msh')
      pOpt = meshPartitionOptions()
      pOpt.setNumOfPartitions(Msg.GetCommSize())
      ord = 1
      dim = 2
      os.putenv("BATHFILE", "etopo2.bin")
      ster = dgSpaceTransformSpherical(6371220.)
      gr = dgGroupCollection(m2, dim, ord, ster, True)
      xyz = gr.getFunctionCoordinates();
      gr.buildGroupsOfInterfaces()
      bath = dgDofContainer(gr, 1);
      if(os.path.exists(outputDir+"/bath_smooth_COMP_0.msh")):
        Msg.Info('.... Import Default Smoothed Bathymetry')
        bath.importMsh2(outputDir + "/bath_smooth")
      else: 
        bathS = functionC("BATH.so", "bath", 1, [xyz])
        bath.interpolate(bathS);
        bath.exportMsh(outputDir+"bath", 0, 0);
        nuB = functionConstant(1e6)
        dlaw = dgConservationLawAdvectionDiffusion.diffusionLaw(nuB)
        dlaw.addBoundaryCondition("Coast", dlaw.new0FluxBoundary())
        sys = linearSystemPETScDouble()
        dof = dgDofManager.newCG (gr, 1, sys)
        print 'H9'
        implicitEuler = dgDIRK(dlaw, dof)
        print 'H10'
        implicitEuler.iterate (bath, 1, 0)
        bath.bezierCrop(10, 2e6)
        bath.exportMsh(outputDir+"bath_crop", 0, 0);
        bathP = functionC("BATH.so", "diff", 1, [function.getSolution(), bath.getFunction()])
        nuB = functionConstant(0)
        dlaw = dgConservationLawAdvectionDiffusion.diffusionLaw(nuB)
        dlaw.addBoundaryCondition("Coast", dlaw.new0FluxBoundary())
        dlaw.setSource(bathP)
        sys = linearSystemPETScDouble()
        sys.setParameter("petscOptions", "-pc_type lu");
        dof = dgDofManager.newCG (gr, 1, sys)
        solver = dgSteady(dlaw, dof)
        solver.getNewton().setVerb(10)
        solver.getNewton().setMaxIt(6)
        solver.solve(bath)
        bath.exportMsh(outputDir+"bath_smooth", 0, 0);
        
      sys = 0
      dof = 0
      claw = dgConservationLawShallowWater2d()
      solution = dgDofContainer(gr, claw.getNbFields())
      Finit = functionC("tsunami.so", "initial_condition_okada_honshu",3, [xyz])
      solution.interpolate(Finit)
      FCoriolis= functionC("tsunami.so", "coriolis", 1, [xyz])
      claw.setBathymetry(bath.getFunction())
      claw.setBathymetryGradient(bath.getFunctionGradient())
      claw.setCoriolisFactor(FCoriolis)
      claw.setIsLinear(True)
      claw.setIsSpherical(1, xyz)
      wall=claw.newBoundaryWall()
      claw.addBoundaryCondition("Coast", wall)
      
      
      bottomDrag = functionC("tsunami.so", "bottomDrag", 1, [function.getSolution(), bath.getFunction()])
      claw.setLinearDissipation(bottomDrag)
      
      gr.deleteGroupsOfInterfaces()
      rk=dgRungeKuttaMultirateConservative.new2a(gr, claw)
      dt=rk.splitForMultirate(ng, solution, [solution, bath])
      gr.buildGroupsOfInterfaces()
      rk.computePartitionMultipleWeights(pOpt,  5)
      pOpt.partition(m2)
      gr.computePartitionMultirateLoads(Msg.GetCommSize())
      m2.save(fileName + '_partitioned.msh')
    Msg.Info("partition done")
    Msg.Barrier()
    model.load(fileName + '_partitioned.msh')
  else :
    model.load(fileName + '.msh')
