from dgpy import *
import time, math, os
import os.path
import os
import sys
from MRPartition import *

ng=8
def printTime (t) :
  return "%dh%02d'%02d\"" % (t/3600, (t%3600)/60, t%60)

if (Msg.GetCommRank()==0):
  functionC.buildLibraryFromFile ("tsunami.cc", "tsunami.so");
  functionC.buildLibraryFromFile ("BATH.cc", "BATH.so");
Msg.Barrier()

model = GModel()
loadAndPartitionMesh(model, 'world', ng)
order = 1
dimension = 2
outputDir="tsunami/"


os.putenv("BATHFILE", "etopo2.bin")
stereo = dgSpaceTransformSpherical(6371220.)
groups = dgGroupCollection(model, dimension, order, stereo)
XYZ = groups.getFunctionCoordinates();
groups.buildGroupsOfInterfaces()
bath = dgDofContainer(groups, 1);
if(os.path.exists(outputDir+"/bath_smooth_COMP_0.msh")):
  Msg.Info('.... Import Default Smoothed Bathymetry')
  bath.importMsh2(outputDir + "/bath_smooth")
else: 
  bathS = functionC("BATH.so", "bath", 1, [XYZ])
  bath.interpolate(bathS);
  bath.exportMsh(outputDir+"bath", 0, 0);
  nuB = functionConstant(1e6)
  dlaw = dgConservationLawAdvectionDiffusion.diffusionLaw(nuB)
  dlaw.addBoundaryCondition("Coast", dlaw.new0FluxBoundary())
  sys = linearSystemPETScDouble()
  dof = dgDofManager.newCG (groups, 1, sys)
  implicitEuler = dgDIRK(dlaw, dof)
  implicitEuler.iterate (bath, 1, 0)
  bath.bezierCrop(10, 2e6)
  bath.exportMsh(outputDir+"bath_crop", 0, 0);
  bathP = functionC("BATH.so", "diff", 1, [function.getSolution(), bath.getFunction()])
  nuB = functionConstant(0)
  dlaw = dgConservationLawAdvectionDiffusion.diffusionLaw(nuB)
  dlaw.addBoundaryCondition("Coast", dlaw.new0FluxBoundary())
  dlaw.setSource(bathP)
  sys = linearSystemPETScDouble()
  sys.setParameter("petscOptions", "-pc_type lu");
  dof = dgDofManager.newCG (groups, 1, sys)
  solver = dgSteady(dlaw, dof)
  solver.getNewton().setVerb(10)
  solver.getNewton().setMaxIt(6)
  solver.solve(bath)
  bath.exportMsh(outputDir+"bath_smooth", 0, 0);

sys = 0
dof = 0
claw = dgConservationLawShallowWater2d()
solution = dgDofContainer(groups, claw.getNbFields())
Finit = functionC("tsunami.so", "initial_condition_okada_honshu",3, [XYZ])
solution.interpolate(Finit)
FCoriolis= functionC("tsunami.so", "coriolis", 1, [XYZ])
claw.setBathymetry(bath.getFunction())
claw.setBathymetryGradient(bath.getFunctionGradient())
claw.setCoriolisFactor(FCoriolis)
claw.setIsLinear(True)
claw.setIsSpherical(1, XYZ)
wall=claw.newBoundaryWall()
claw.addBoundaryCondition("Coast", wall)


bottomDrag = functionC("tsunami.so", "bottomDrag", 1, [function.getSolution(), bath.getFunction()])
claw.setLinearDissipation(bottomDrag)
solution.exportMsh(outputDir + 'init', 0., 0)

groups.deleteGroupsOfInterfaces()
rk=dgRungeKuttaMultirateConservative.new2a(groups, claw)
dt=rk.splitForMultirate(ng, solution, [solution, bath])
groups.buildGroupsOfInterfaces()
#solution.exportGroupIdMsh()
#solution.exportMultirateGroupMsh()
Msg.Info("Theoretical Speedup %f" %(groups.getMRspeedUp()))

t=0
evalSol=dgFunctionEvaluator(groups, solution.getFunction())

solution.exportMsh(outputDir + "solution-000000", 0., 0)
nbExport = 0
tic = time.clock()
nbSteps=1000000
for i in range (1,nbSteps+1) :
  norm = rk.iterate(t,dt,solution)
  t = t + dt
  Msg.Info("%d    norm = %.1e    t = %s    cpu = %s" % (i, solution.norm(), printTime(t), printTime(time.clock()-tic)))
  if ( i%1 ==0 ):
    solution.exportMsh(outputDir + "solution-%06d" % (i), t, nbExport)
    nbExport = nbExport + 1
  timeSerie = open("dart_stations",'r')
  line = (timeSerie.readline()).split()
  while line:
    stationName = line[0]
    lon = float(line[1])
    lat = float(line[2])
    line = (timeSerie.readline()).split()
    result  = fullMatrixDouble(1,1)
    R = 6371220
    x = R*math.cos(lon*math.pi/180)*math.cos(lat*math.pi/180)
    y = R*math.sin(lon*math.pi/180)*math.cos(lat*math.pi/180)
    z = R*math.sin(lat*math.pi/180)
    xs = 2*R*x/(R+z)
    ys = 2*R*y/(R+z)
    evalSol.compute(xs,ys,0.0,result)
    if (i==1):
      tsFile = open(stationName,"w")
    else:
      tsFile = open(stationName,"a")
    tsFile.write("%g %g\n" % (t,result.get(0,0)))
    tsFile.close()
  timeSerie.close()

Msg.Exit(0)
