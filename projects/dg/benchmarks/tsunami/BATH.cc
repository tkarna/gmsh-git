#include "fullMatrix.h"
#include "function.h"
#include <stdlib.h>
static double Ox, Oy, dx, dy;
static int nx, ny;
static double *data;
static bool init = false;

extern "C" {
static void initialize() {
  const char * filename = getenv("BATHFILE");
  if (!filename || std::string(filename) == "") {
    fprintf(stderr,"BATHFILE env variable not defined. Use os.putenv(\"BATHFILE\", filename) in python.\n");
    exit(0);
  }
  FILE *input = fopen(filename, "r");
  double buf;
  fread (&Ox, sizeof(double), 1, input);
  fread (&Oy, sizeof(double), 1, input);
  fread (&buf, sizeof(double), 1, input);
  fread (&dx, sizeof(double), 1, input);
  fread (&dy, sizeof(double), 1, input);
  fread (&buf, sizeof(double), 1, input);
  fread (&nx, sizeof(int), 1, input);
  fread (&ny, sizeof(int), 1, input);
  fread (&buf, sizeof(int), 1, input);
  data = new double[nx*ny];
  fread (data, sizeof(double), nx*ny, input);
  init = true;
}

void bath (dataCacheMap *, fullMatrix<double> &bath, fullMatrix<double> &XYZ) {
  if (! init) initialize();
  const double R = 6371220;
  for (size_t i = 0; i< bath.size1(); i++) {
    const double xi = XYZ(i,0);
    const double zeta = XYZ(i,1);
    const double x = 4*R*R*xi/(4*R*R+xi*xi + zeta*zeta);
    const double y = 4*R*R*zeta/(4*R*R+xi*xi + zeta*zeta);
    const double z = R *(4*R*R-xi*xi-zeta*zeta)/(4*R*R+xi*xi + zeta*zeta);
    const double lon = asin(z/R);
    const double lat = atan2(y,x);
    double rx = (lat-Ox);
    double ry = (lon-Oy);
    int iX = rx/dx;
    int iY = ry/dy;
    double alpha = (rx - iX*dx) / dx;
    double beta = (ry - iY*dy) / dy;
    double b = 
      (1-alpha)*(1-beta)*data[iX*ny+iY]
      +alpha*(1-beta)*data[(iX+1)*ny+iY]
      +alpha*beta*data[(iX+1)*ny+(iY+1)]
      +(1-alpha)*beta*data[iX*ny+(iY+1)];
    bath.set (i, 0, b);
  }
}

void diff (dataCacheMap *, fullMatrix<double> &a, fullMatrix<double> &b,fullMatrix<double> &c) {
  for (size_t i = 0; i< a.size1(); i++) {
    for (size_t j = 0; j < a.size2(); j++) {
      a(i, j) = b(i, j) - c(i, j);
    }
  }
}
}
