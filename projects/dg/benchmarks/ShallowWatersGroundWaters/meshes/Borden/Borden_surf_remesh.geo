//Mesh.RemeshParametrization=1;
Mesh.CharacteristicLengthFactor = .005;
Mesh.CharacteristicLengthFromPoints = 0;
Merge "Borden_surf.msh";
CreateTopology;

Compound Line(300) = {1};
Compound Line(400) = {3};
Compound Surface(100) = {4,5};
//Compound Surface(100) = {4};
//Compound Surface(200) = {5};
Physical Line("outlet")={300};
Physical Line("border")={400};
Physical Surface("surface")={100};
//Physical Surface("channel")={200};
Mesh.RemeshAlgorithm = 1;
Mesh.Algorithm = 6; //frontal

