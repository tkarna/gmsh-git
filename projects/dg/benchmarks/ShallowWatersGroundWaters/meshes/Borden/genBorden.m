function genBorden(genRemesh, gen2D, gen3D, partitions)

gmsh = '/Applications/Gmsh.app/Contents/MacOS/gmsh';

if nargin < 4
  partitions = 1;
end
if nargin < 3
  gen3D = [3 3];
end
if nargin < 2
  gen2D = true;
end
if nargin < 1
  genRemesh = true;
end

if(genRemesh)
  part = '';
  if(partitions > 1)
    part = [' -part ' int2str(partitions)];
  end
  system([gmsh ' Borden_surf_remesh.geo -2 -o Borden_surf_remeshTMP.msh' part]);
end

part = '';
if(partitions > 1)
  part = ['_part_' int2str(partitions) '_EXT'];
end
nameMsh = ['Borden_surf_remesh' part '.msh'];

if(gen2D)
  shiftheader = 10;
  nbNodes = dlmread('Borden_surf_remeshTMP.msh', ' ', [shiftheader 0 shiftheader 0]);
  surfaceNodes = dlmread('Borden_surf_remeshTMP.msh', ' ', [shiftheader+1 0 shiftheader+nbNodes 3]);
  %nbElmts = dlmread('Borden_surf_remesh.msh', ' ', [shiftheader+nbNodes+3 0 shiftheader+nbNodes+3 0])
  %elements = dlmread('Borden_surf_remesh.msh', ' ', [shiftheader+nbNodes+4 0 shiftheader+nbNodes+nbElmts+3 6]);

  x=surfaceNodes(:,2)';
  y=surfaceNodes(:,3)';

  load surfaceContourMod
  [in,on] = inpolygon(x, y, surfaceContourMod(:,1)', surfaceContourMod(:,2)');

  %figure(6699)
  %plot(surfaceContourMod(:,1), surfaceContourMod(:,2), "m-", x(in), y(in), "go")
  %for i=1:size(surfaceContour, 1)
  %  text(surfaceContourMod(i,1), surfaceContourMod(i,2), num2str(i))
  %end

  fileMshTmp = fopen('Borden_surf_remeshTMP.msh', 'r');
  fileMsh = fopen(nameMsh, 'w');

  for i=1:4
    fprintf(fileMsh, '%s\n', fgetl(fileMshTmp))
  end
  [nbTags, one] = sscanf(fgetl(fileMshTmp), '%d\n');
  fprintf(fileMsh, '%d\n', nbTags + 1);
  for i=1:nbTags
    fprintf(fileMsh, '%s\n', fgetl(fileMshTmp));
  end
  fprintf(fileMsh, '2 4 "channel"\n');
  for i=1:3+nbNodes+2
    fprintf(fileMsh, '%s\n', fgetl(fileMshTmp))
  end

  [nbElmts, one] = sscanf(fgetl(fileMshTmp), '%d\n');
  fprintf(fileMsh, '%d\n', nbElmts);

  for i=1:nbElmts
    valsStr = fgetl(fileMshTmp);
    [type three] = sscanf(valsStr, '%d %d %d');
    nbtags = type(3);
    type = type(2);
    if(type != 2)
      fprintf(fileMsh, '%s\n', valsStr);
    else
      if(nbtags == 2)
        tmpl = '%d %d %d %d %d %d %d %d\n';
      elseif(nbtags == 4)
        tmpl = '%d %d %d %d %d %d %d %d %d %d\n';
      else %if(nbtags == 5)
        tmpl = '%d %d %d %d %d %d %d %d %d %d %d\n';
      end
      vals = sscanf(valsStr, tmpl);
      if( in(vals(4+nbtags)) || in(vals(5+nbtags)) || in(vals(6+nbtags)) )
      %if( in(vals(6)) && in(vals(7)) && in(vals(8)) )
        fprintf(fileMsh, tmpl, vals(1:3), 4, 4, vals(6:6+nbtags))
      else
        fprintf(fileMsh, tmpl, vals(1:3), 3, 3, vals(6:6+nbtags))
      end
    end
  end
  fprintf(fileMsh, '$EndElements\n');
  fclose(fileMshTmp);
  fclose(fileMsh);
end

if(gen3D)
  nbLayersSZ = gen3D(1);
  nbLayersUZ = gen3D(2);
  nbLayers = nbLayersSZ + nbLayersUZ;

  waterlevel = 2.7; %2.76;

  fileMshTmp = fopen(nameMsh, 'r');
  charactlengthfactor = dlmread('Borden_surf_remesh.geo', ' ', [1 2 1 2]);
  filetag = [int2str(charactlengthfactor * 1000) '_' int2str(nbLayersSZ) '-' int2str(nbLayersUZ)];
  fileMsh = fopen(['Borden_remesh' filetag part '.msh'], 'w');
  fileGeo = fopen(['Borden_remesh' filetag '.geo'], 'w');


  % geo file => just topology !
  fprintf(fileGeo, "cl1 = 1;...
Point(1) = {0, 0, 0, 1};...
Point(2) = {0, 1, 0, 1};...
Point(3) = {1, 1, 0, 1};...
Point(4) = {1, 0, 0, 1};...
Point(5) = {0, 0, 1, 1};...
Point(6) = {0, 1, 1, 1};...
Point(7) = {1, 1, 1, 1};...
Point(8) = {1, 0, 1, 1};...
Point(9) = {0, 0.5, 0, 1.0};...
Line(1) = {6, 7};...
Line(2) = {7, 3};...
Line(3) = {3, 2};...
Line(4) = {2, 6};...
Line(5) = {6, 5};...
Line(6) = {5, 1};...
Line(8) = {1, 4};...
Line(9) = {4, 8};...
Line(10) = {8, 5};...
Line(11) = {8, 7};...
Line(12) = {3, 4};...
Line(25) = {2, 9};...
Line(26) = {9, 1};...
Line(27) = {9, 3};...
Line(28) = {4, 9};...
Line Loop(18) = {2, 12, 9, 11};...
Plane Surface(18) = {18};...
Line Loop(20) = {10, 6, 8, 9};...
Plane Surface(20) = {20};...
Line Loop(22) = {4, 1, 2, 3};...
Plane Surface(22) = {22};...
Line Loop(24) = {5, -10, 11, -1};...
Plane Surface(24) = {24};...
Line Loop(29) = {25, 26, -6, -5, -4};...
Plane Surface(30) = {29};...
Line Loop(31) = {27, 3, 25};...
Plane Surface(32) = {31};...
Line Loop(33) = {27, 12, 28};...
Plane Surface(34) = {33};...
Line Loop(35) = {8, 28, 26};...
Plane Surface(36) = {35};...
Surface Loop(37) = {32, 34, 18, 22, 30, 36, 20, 24};...
Volume(38) = {37};...
Physical Line (\"outlet\") = {12};...
Physical Line (\"border\") = {3, 8, 25, 26};...
Physical Surface (\"channel\") = {34};...
Physical Surface (\"surface\") = {32, 36};");

  fclose(fileGeo);

  % msh file

  for i=1:4
    fprintf(fileMsh, '%s\n', fgetl(fileMshTmp))
  end
  [nbTags, one] = sscanf(fgetl(fileMshTmp), '%d\n');
  fprintf(fileMsh, '%d\n', nbTags + 2);
  for i=1:nbTags
    fprintf(fileMsh, '%s\n', fgetl(fileMshTmp));
  end
  fprintf(fileMsh, '2 5 "bordersAndBottom"\n');
  fprintf(fileMsh, '3 6 "volume"\n');
  fprintf(fileMsh, '$EndPhysicalNames\n$Nodes\n');
  fgetl(fileMshTmp); fgetl(fileMshTmp);
  [nbPtsSurf, one] = sscanf(fgetl(fileMshTmp), '%d\n');

  fprintf(fileMsh, '%d\n', nbPtsSurf * (nbLayers+1) );

  for i=1:nbPtsSurf
    fprintf(fileMsh, '%s\n', fgetl(fileMshTmp));
  end

  shiftheader = nbTags+7;
  surfaceNodes = dlmread(nameMsh, ' ', [shiftheader+1 0 shiftheader+nbPtsSurf 3]);
  x = surfaceNodes(:, 2);
  y = surfaceNodes(:, 3);
  z = surfaceNodes(:, 4);

  layersDown = linspace(waterlevel, 0, nbLayersSZ+1);
  layersDown = layersDown(2:end);
  layersUp   = linspace(1, 0, nbLayersUZ) * (1-1./nbLayersUZ);

  % nodes

  for j=1:nbLayersUZ
    for i=1:nbPtsSurf
      zvalue = layersUp(j) * (z(i) - waterlevel) + waterlevel;
      fprintf(fileMsh, '%d %e %e %e\n', j*nbPtsSurf+i, x(i), y(i), zvalue );
    end
  end
  for j=(1:nbLayersSZ)+nbLayersUZ
    for i=1:nbPtsSurf
      zvalue = layersDown(j-nbLayersUZ);
      fprintf(fileMsh, '%d %e %e %e\n', j*nbPtsSurf+i, x(i), y(i), zvalue );
    end
  end

  for i=1:2
    fprintf(fileMsh, '%s\n', fgetl(fileMshTmp));
  end


  % 1D/2D elements
  
  tmpExistingElmts = '';
  [nbElmtsSurf, one] = sscanf(fgetl(fileMshTmp), '%d\n');
  nbTri = -1;
  nbLin = -1;
  for i=1:nbElmtsSurf
    valsStr = fgetl(fileMshTmp);
    if(nbTri == -1)
      [type two] = sscanf(valsStr, '%d %d');
      type = type(2);
      if(type == 2)
        nbLin = i - 1;
        nbTri = nbElmtsSurf - nbLin;
      end
    end
    tmpExistingElmts = [tmpExistingElmts valsStr '\n'];
  end

  fprintf(fileMsh, '%d\n', nbLin * (1+nbLayers) + nbTri * (2+nbLayers));
%  fprintf(fileMsh, '%d\n', nbLin * (1+nbLayers) + nbTri * 2);
  fprintf(fileMsh, tmpExistingElmts);

  surfaceLin = dlmread(nameMsh, ' ', [shiftheader+nbPtsSurf+4 0 shiftheader+nbPtsSurf+3+nbLin 9]);
  surfaceTri = dlmread(nameMsh, ' ', [shiftheader+nbPtsSurf+4+nbLin 0 shiftheader+nbPtsSurf+3+nbElmtsSurf 10]);

  % 2D down
  for i=1:nbTri
    nbparams = surfaceTri(i, 3);
    tmpl = giveTmpl(nbparams, surfaceTri(i, 6:8));
    fprintf(fileMsh, ['%d 2 %d 5 5 ' tmpl '%d %d %d\n'], nbElmtsSurf+i, nbparams, nbLayers*nbPtsSurf + surfaceTri(i, nbparams+(6:-1:4) ) );
  end
  nbWrittenEmts = nbElmtsSurf + nbTri;
  % 2D sides (quads)
  for j=0:nbLayers-1
    for i=1:nbLin
      nbparams = surfaceLin(i, 3);
      tmpl = giveTmpl(nbparams, surfaceLin(i, 6:4+nbparams));
      if(surfaceLin(i,4) == 1) % outlet
        fprintf(fileMsh, ['%d 3 %d 5 5 ' tmpl '%d %d %d %d\n'], nbWrittenEmts+nbLin*j+i, nbparams,
                                                                        j*nbPtsSurf + surfaceLin(i, nbparams+[5 4] ),
                                                                    (j+1)*nbPtsSurf + surfaceLin(i, nbparams+[4 5] ) );
      else % border
        fprintf(fileMsh, ['%d 3 %d 5 5 ' tmpl '%d %d %d %d\n'], nbWrittenEmts+nbLin*j+i, nbparams,
                                                                        j*nbPtsSurf + surfaceLin(i, nbparams+[4 5] ),
                                                                    (j+1)*nbPtsSurf + surfaceLin(i, nbparams+[5 4] ) );
      end
    end
  end
  nbWrittenEmts = nbWrittenEmts + nbLin*nbLayers;
  % 3D
  for j=0:nbLayers-1
    for i=1:nbTri
      nbparams = surfaceTri(i, 3);
      tmpl = giveTmpl(nbparams, surfaceTri(i, 6:8));
      fprintf(fileMsh, ['%d 6 %d 6 6 ' tmpl '%d %d %d %d %d %d\n'], nbWrittenEmts+nbTri*j+i, nbparams,
                                                                          (j+1)*nbPtsSurf + surfaceTri(i, nbparams+(4:6) ),
                                                                              j*nbPtsSurf + surfaceTri(i, nbparams+(4:6) ) );
    end
  end

  fprintf(fileMsh, '$EndElements\n');
  fclose(fileMsh);
  fclose(fileMshTmp);
end

function str=giveTmpl(nbr, vals)
  if(nbr == 2)
    str = '';
  elseif(nbr == 4)  
    str = sprintf('%d %d ', vals(1:2));
  elseif(nbr == 5)
    str = sprintf('%d %d %d ', vals(1:3));
  else
    bug = nbr
    str = '';
  end
