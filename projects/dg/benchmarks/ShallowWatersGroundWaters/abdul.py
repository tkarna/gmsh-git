from SWGW import *
#from SWGW_test import *
#import utilities as u # done in SWGW

# C-functions
# -----------

CFunctions = ''' 
#include "fullMatrix.h"
#include "function.h"
#include "dgConservationLaw.h"
#include "dgGroupOfElements.h"
#include "SVector3.h"
#include <iostream>
extern "C" {

  void rain (dataCacheMap *m, fullMatrix<double> &out, fullMatrix<double> &time) {
    for(size_t i = 0; i < out.size1(); i++) {
      if(time(0, 0) < 20 * 60.)
        out.set(i, 0, 1.2e-5);
      else
        out.set(i, 0, 0);
    }
  }

  void init (dataCacheMap *m, fullMatrix<double> &out, fullMatrix<double> &xyz, fullMatrix<double> &time) {
    for (size_t i = 0; i < out.size1(); i++) {
      double x = xyz(i, 0);
      double y = xyz(i, 1);
      double z = xyz(i, 2);
      out.set(i, 0, -z);
    }
  }

  void drivenP(dataCacheMap *m, fullMatrix<double> &out, fullMatrix<double> &h, fullMatrix<double> &xyz) {
    for (size_t i = 0; i < out.size1(); i++) {
      double z = xyz(i, 2);
      out.set(i, 0, h(i,0) + z);
    }
  }

}
'''
tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CFunctions, tmpLib);
Msg.Barrier()

TIME = function.getTime();

# Parameters
# ----------

maxDT = 0.1
rain = functionC(tmpLib, "rain", 1, [TIME])
nbItersRichardsH = 10
nbItersRichardsW = 4
nbItersRichardsHBoost = 100
nbItersRichardsWBoost = 4


# Spatial definition and construction
# -----------------------------------

meshName = 'meshes/abdul_refined.msh'

sm = SoilMap()
soil_sand = soilVG(2.3, 5.5 , 3.5e-5, 0.34, 0.0)
sm.add("volume", soil_sand)

lsm = LandSurfaceMap()
surface = landSurfManning(.185)
lsm.add("surface", surface)

C = SWGW(meshName, sm, lsm, rainFlux=rain)
C.R.setNbIters(nbItersRichardsHBoost, nbItersRichardsWBoost)

# boundary conditions
# -------------------

C0 = functionConstant(0.)
C000 = functionConstant([0., 0., 0.])
C.bndNeumannR(C0, 'noflux')
C.bndNeumannS(C0, 'top')
C.bndSlopeS(C000, 'bottom')

# initial conditions
# ------------------

INIR = functionC(tmpLib, "init", 1, [C.R.XYZ, TIME])
INIS = functionConstant(1e-10)
C.seth(INIR, INIS)

# post-pro computations
# ---------------------

f = open('outputdata/abdul_flux_new','w')

# approximation of the error from false transient 

#C.R.setEstimator()

# time and exports
# ----------------

printEach = 10
h = slimIterateHelper()
h.setInitialDate(0)
h.setFinalDate(25 * 60)
#h.setFinalDate(1 * 60)
#h.setExport("60:55.3,55.4,55.5,55.6,55.7")
h.setExport("60:5,10,15,20,30,40,50,70,80,90,100,110")
h.setMaxDt(maxDT)

str_subIter = str(nbItersRichardsH)
h.setFileName("output/abdulNew" + str_subIter + "_")
h.addExport(C.S.H, "sH" + str_subIter)
h.addExport(C.R.H, "gh" + str_subIter)
h.addExport(C.R.W, "gth" + str_subIter)
h.addExport(C.R.H, C.R.state, "_gState" + str_subIter)
#h.addExport(C.cplgFluxC, "sCplgFlux" + str_subIter)
#h.addExport(C.limCplgFluxC, "sLimCplgFlux" + str_subIter)
#h.addExport(C.S.H, C.cplgFluxNoLimS, "_sCplgNoLim" + str_subIter)
#h.addExport(C.S.H, C.cplgFluxS, "_sCplg" + str_subIter)
drivenPressure = functionC(tmpLib, "drivenP", 1, [C.R.HF, C.R.XYZ])
h.addExport(C.R.H, drivenPressure, "_gDrivenPressure" + str_subIter)
#h.addExport(C.R.H, C.R.absErrorOnApprox, "_gFalseTransientError" + str_subIter)

#h.exportVtk(True)
#h.exportMsh(False)
h.exportBin(True)
h.printExport(True)
h.setNbMaxIter(1000000)
i=0; t=0; dt=1;
nbIMin = 1000; nbIMax = 0; nbIMean = 0

#h.importAll(6,40)
#h.importAll(7,55.3)
#C.restore(h, 18)
#i=1999


# TEMPORAL LOOP #
#################

u.initClock()
C.display(0,0,0)

while (not h.isTheEnd()) :
  i = i + 1
  #CFL = rkT.computeInvSpectralRadius(TDof3d)
  #h.setMaxDt(min(CFL * factCFL, max_dt_surf))
  dt = h.getNextTimeStepAndExport()
  t = h.getCurrentTime()

  if(i == 2000):
    C.R.setNbIters(nbItersRichardsH, nbItersRichardsW)  

  nbSubR,nbSubS = C.iterate(dt, t)

#  h.exportAll()
#  exit()

  nbIMin = min(nbIMin, nbSubS)
  nbIMax = max(nbIMax, nbSubS)
  nbIMean = nbIMean + nbSubS

  # PRINT

  if (i % printEach == 0) :
    nbIMean = float(nbIMean) / printEach;
    C.display(i, dt, t, '|NWits| {0:2d} <{1:4.1f} <{2:2d}'.format(nbIMin, nbIMean, nbIMax))
    nbIMin = 1000; nbIMax = 0; nbIMean = 0
    print >>f, '%6.3e' % (t), '%21.16e' % (C.S.getBoundaryFlux('bottom'))

  # STOP IF BUG

  if (not nbSubR or not nbSubS) :
    u.printOnce('problem: abording. last iter was :')
    C.display(i, dt, t)
    h.exportAll()
    break

u.printOnce('Last iteration :')
C.display(i, dt, t)


