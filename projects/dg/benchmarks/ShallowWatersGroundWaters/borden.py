from SWGW import *

# C-functions
# -----------

CFunctions = ''' 
#include "fullMatrix.h"
#include "function.h"
#include "dgConservationLaw.h"
#include "dgGroupOfElements.h"
#include "SVector3.h"
#include <iostream>
extern "C" {

  void rain (dataCacheMap *m, fullMatrix<double> &out, fullMatrix<double> &time) {
    for(size_t i = 0; i < out.size1(); i++) {
      if(time(0, 0) < 3000.)
        out.set(i, 0, 5.555e-6);
      else
        out.set(i, 0, 0.);
    }
  }

  void init (dataCacheMap *m, fullMatrix<double> &out, fullMatrix<double> &xyz, fullMatrix<double> &time) {
    for (size_t i = 0; i < out.size1(); i++) {
      double x = xyz(i, 0);
      double y = xyz(i, 1);
      double z = xyz(i, 2);
      out.set(i, 0, - z + 2.78);
    }
  }

  void gaussian (dataCacheMap *m, fullMatrix<double> &out, fullMatrix<double> &xyz, fullMatrix<double> &time) {
    for (size_t i = 0; i < out.size1(); i++) {
      double x = xyz(i, 0);
      double y = xyz(i, 1);
      const double distx = 40.;
      const double disty = 9.;
      double dist = (x-distx)*(x-distx) + (y-disty)*(y-disty);      
      out.set(i, 0, 0.1 * exp(-1.5 * dist));
    }
  }

  void drivenP(dataCacheMap *m, fullMatrix<double> &out, fullMatrix<double> &h, fullMatrix<double> &xyz) {
    for (size_t i = 0; i < out.size1(); i++) {
      double z = xyz(i, 2);
      out.set(i, 0, - h(i,0) - z);
    }
  }

}
'''
tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CFunctions, tmpLib);
Msg.Barrier()

TIME = function.getTime();

# Parameters
# ----------

maxDT = 5
minDT = .5
dtInit = 5
#rain = functionConstant(0)
rain = functionC(tmpLib, "rain", 1, [TIME])
nbItersRichardsH = 18
nbItersRichardsW = 1
#14: 0.1, 15: 0.5, 16: 0.3, 17: 0.25

# Spatial definition and construction
# -----------------------------------

#meshName = 'meshes/Borden44x4.msh'
meshName = 'meshes/Borden33.msh'

sm = SoilMap()
soil_sand = soilVG(6, 1.9, 1e-5, 0.34, 0.0612) # Ss = 1.2E-07
sm.add("volume", soil_sand)

lsm = LandSurfaceMap()
surface = landSurfManning(.3)
channel = landSurfManning(.03) #.03
lsm.add("surface", surface)
lsm.add("channel", channel)

Roptions = RichardsOptions(constantEta=False, homogeneousLimiter=True)
#Soptions = SWNIOptions(microTopo=1e-2, residualSat=1e-2, mobileWaterDepth=1e-4, nlConstantByElement=True)
#Soptions = SWNIOptions(microTopo=0.002, residualSat=1e-10, mobileWaterDepth=1e-10, nlUpwindedByElement=True)
Soptions = SWNIOptions(residualSat=1e-10, mobileWaterDepth=1e-10, microTopo=.002,
                      constantGH=False, constantNL=False, nlConstantByElement=False, nodalNL=False,
                      isDG=False, kinematicWave=False, nlUpwindedByElement=False, clip=False, picard=False,
                      setMaxIt=15, setRtol=1e-5, setVerb=10, controlVolumeFE=True, controlVolumeFEinterface=True, DMax=100)
#Coptions = SWGWOptions(ROptions=Roptions, SOptions=Soptions, fluxType=1, couplingLength=1e-4) # flux=1 : vanderkwaak
#Coptions = SWGWOptions(ROptions=Roptions, SOptions=Soptions, fluxType=1, couplingLength=0.1, meanCplgFlux=False)
Coptions = SWGWOptions(ROptions=Roptions, SOptions=Soptions, fluxType=1, couplingLength=0.25, meanCplgFlux=False)
C = SWGW(meshName, sm, lsm, rainFlux=rain, surfaceTags=['surface', 'channel'], options=Coptions)
C.R.setNbIters(nbItersRichardsH, nbItersRichardsW)

#SPicard = SWNI(model=C.model, landMap=lsm, options=Soptions, tags=['surface', 'channel'])

# boundary conditions
# -------------------

C0 = functionConstant(0.)
C000 = functionConstant([0., 0., 0.])
C.bndNeumannR(C0, 'bordersAndBottom')
C.bndNeumannS(C0, 'border')
#C.bndSlopeS(C000, 'outlet')
#C.bndNeumannS(C0, 'outlet')
C.bndCriticalDepthS('outlet')
#C.bndNeumannR(C0, ['surface', 'channel'])

#SPicard.bndNeumann(C0, 'border')
#SPicard.bndCriticalDepth('outlet')

# initial conditions
# ------------------

INI = functionC(tmpLib, "init", 1, [C.R.XYZ, TIME])
C.R.seth(INI)
Cinit = functionConstant(1e-10)
#gaussian = functionC(tmpLib, "gaussian", 1, [C.S.XYZ, TIME])
#C.S.setH(gaussian)
C.S.setH(Cinit)

# post-pro computations
# ---------------------

f = open('outputdata/borden_outlet_flux_' + str(nbItersRichardsH),'w')

# approximation of the error from false transient

C.R.setEstimator()

# time and exports
# ----------------

printEach = 1
h = slimIterateHelper()
h.setInitialDate(0)
h.setFinalDate(4500)
#h.setFinalDate(1 * 60)
#h.setExport("60:55.3,55.4,55.5,55.6,55.7")
h.setExport("120:5,10,20,40,80")
#h.setExport("60:5,10,20,40,80")
#h.setExportStep(1)

meanHs = dgFunctionEMeanNew(C.S.HF)
scalarizedGH = functionC(C.S.libName, "scalarize2D", 1, [C.S.HG, C.S.bathG])
C1 = functionConstant(1.)
oneOverGradH = functionQuotientNew(C1, scalarizedGH)

str_subIter = str(nbItersRichardsH)
h.setFileName("output/borden" + str_subIter + "_")
h.addExport(C.S.H, "sH" + str_subIter)
h.addExport(C.R.H, "gh" + str_subIter)
h.addExport(C.R.W, "gth" + str_subIter)
h.addExport(C.R.H, C.R.state, "_gState" + str_subIter)
drivenPressure = functionC(tmpLib, "drivenP", 1, [C.R.HF, C.R.XYZ])
h.addExport(C.R.H, drivenPressure, "_gDrivenPressure" + str_subIter)
h.addExport(C.R.H, C.R.absErrorOnApprox, "_gFalseTransientError" + str_subIter)
#h.addExport(C.S.H, C.massAvailSMean, "_sMassAvailSMean" + str_subIter)
#h.addExport(C.S.H, C.massAvailS, "_sMassAvailS" + str_subIter)
#h.addExport(C.S.H, C._kro, "_sKro" + str_subIter)
#h.addExport(C.S.H, meanHs, "_sMeanH" + str_subIter)
#h.addExport(C.S.constantGHC, C.S.constantGHF, "_sGHconst" + str_subIter)
#h.addExport(C.S.H, C.S.nl, "_sNl" + str_subIter)
#h.addExport(C.S.H, oneOverGradH, "_s1oGrad" + str_subIter)
h.addExport(C.S.H, C.cplgFluxS, "_sCplgFlux" + str_subIter)
#h.addExport(C.S.H, C.massAvailSMean, "_sAvailSMean" + str_subIter)

#h.exportVtk(True)
#h.exportMsh(False)
h.exportBin(True)
h.printExport(True)
h.setNbMaxIter(1000000)
i = 0; dt=1; t = 0; dtAdpt = dtInit

#C.restore(h, 15)
#dtInit = 5
C.R.setNbIters(4, nbItersRichardsW) #HACK !


# TEMPORAL LOOP #
#################

u.initClock()
C.display(0,0,t)
h.setMaxDt(dtInit)

while (not h.isTheEnd()) :
  i = i + 1
  #CFL = rkT.computeInvSpectralRadius(TDof3d)
  #h.setMaxDt(min(CFL * factCFL, max_dt_surf))
  dt = h.getNextTimeStepAndExport()
  t = h.getCurrentTime()

  #if(i == 2000):
  #  C.R.setNbIters(nbItersRichardsH, nbItersRichardsW)  

  nbSubR,nbSubS = C.iterate(dt, t)

  # ADAPT DT IF BUG
  '''
  if (not nbSubR or not nbSubS) :
    if(abs(C.S.getGlobalMass()) > 1e5):
      u.printOnce('problem: abording. last iter was :')
      C.display(i, dt, t)
      h.exportAll()
      break
    dtAdpt = max(dt * 0.5, minDT)  # continuing with smaller (hoping no divergence)
    print "change dt from",dt," to ",dtAdpt
    h.setMaxDt(dtAdpt)
  elif(nbSubS <= 6):
    dtAdpt = min(dt * 1.2, maxDT)
    print "change dt from",dt," to ",dtAdpt
    h.setMaxDt(dtAdpt)
  '''

  notConvergingFactor = 2
  j=0
  converged = (nbSubS != 0)
  while(not converged) :
    '''
    print "newton's looping. Trying Picard !"
    SPicard.copy(C.S)
    nbSubS = SPicard.iterate(dt, t)
    C.S.copy(SPicard)
    '''

    print "newton's looping. Trying smaller dt ! (dt/", notConvergingFactor, ", subIt n", j,")"
    while j < notConvergingFactor:
      nbSubS_ = C.S.iterate(dt / notConvergingFactor, t + j*dt/notConvergingFactor)
      if(not nbSubS_):
        j = j*2
        notConvergingFactor *= 2
        break
      nbSubS += nbSubS_
      j=j+1
    if j == notConvergingFactor:
      converged = True

  if (not nbSubR or not nbSubS) :
    print "bug bug bug: convergence not achieved !"
    exit()

  # PRINT

  if (i % printEach == 0) :
    C.display(i, dt, t)
    print >>f, '%6.3e' % (t), '%21.16e' % (C.S.getBoundaryFlux('outlet'))

#  if (nbSubS > 20):
#    break


u.printOnce('Last iteration :')
C.display(i, dt, t)


