import sys
import numpy
import math

sys.path.append('/Users/dubois/WORK/CODING/v2_ifort/LMGC90v2_dev/ChiPy/lib')
sys.path.append('/Users/dubois/WORK/CODING/v2_ifort/LMGC90v2_dev/Pre')

from pre_lmgc import *

from chipy import *

# bavardage de certaines fonctions
echo = 0 

# info generation fichier visu
freq_display = 50
freq_write = 10

# info contact
freq_detect = 1

#       123456789012345678901234567890
type = 'Stored_Delassus_Loops         '
norm = 'Quad '
tol = 0.1666e-3
relax = 1.0
gs_it1 = 51
gs_it2 = 1001

bx=0.25
by=1.0

def createInitialDistributionOfParticles(dt, dewx):

  # generation echantillon

  # on se place en 2D
  dim = 2

  # creation des conteneurs
  #   * pour les corps
  bodies = avatars()
  #   * pour les materiaux
  mat = materials()
  #   * pour les tables de visibilite
  svs = see_tables()
  #   * pour les lois de contact
  tacts = tact_behavs()

  # creations de deux materiaux
  tdur = material(nom='TDURx',type='RIGID',density = dewx)
  mat.addMaterial(tdur)

  # on cree un modele de rigide
  mod = model(nom='rigid', type='MECAx', element='Rxx2D', dimension=dim)

  # on genere 100 particules
  nb_particles=900

  # distribtion aleatoire dans [0.5, 2.[ 
  radii=granulo_Random(nb_particles, 0.002, 0.0021)

  # on recupere le plus petit et le plus grand rayon
  radius_min=min(radii)
  radius_max=max(radii)

  # depot dans une boite rectangulaire
  lx = radius_max*10*3
  ly = radius_max*100*3 
  [nb_remaining_particles, coor]=depositInBox2D(radii, lx, ly)

  # si toutes les particules deposees n'ont pas ete conservees
  if (nb_remaining_particles < nb_particles):
     # on affiche un avertissement
     print "Warning: granulometry changed, since some particles were removed!"

  # boucle d'ajout des disques :
  for i in xrange(0,nb_remaining_particles,1):
    # creation un nouveau disque rigide, constitue du materiau plex
    body=rigidDisk(r=radii[i], center=coor[2*i : 2*(i + 1)], 
                   model=mod, material=tdur, color='BLEUx')
    body.translate(dx=0.225,dy=1.5)
    # ajout du disque dans le conteneur de corps
    bodies += body

  # on declare un corps par paroi
  down =avatar(type='RBDY2', dimension=dim)
  up   =avatar(type='RBDY2', dimension=dim)
  left =avatar(type='RBDY2', dimension=dim)
  right=avatar(type='RBDY2', dimension=dim)

  # on attribue un comportement volumique de type rigide aux parois
  down.addBulk( rigid2d() )
  up.addBulk( rigid2d() )
  left.addBulk( rigid2d() )
  right.addBulk( rigid2d() )

  # on positionne les parois dans l'espace
  down.addNode( 
        node(type='NO2xx', coor=numpy.array([bx, -radius_max]),
        number=1) )
  up.addNode( 
        node(type='NO2xx', coor=numpy.array([bx, (2*by) + radius_max]),
        number=1) )
  left.addNode( 
        node(type='NO2xx', coor=numpy.array([-radius_max, by]),
        number=1) )
  right.addNode( 
        node(type='NO2xx', coor=numpy.array([(2*bx) + radius_max, by]),
        number=1) )

  # on definit les groupes
  down.defineGroups()
  up.defineGroups()
  left.defineGroups()
  right.defineGroups()

  down.defineModel(model=mod)
  up.defineModel(model=mod)
  left.defineModel(model=mod)
  right.defineModel(model=mod)

  # on definit le materiau pour chaque paroi
  down.defineMaterial(material=tdur)
  up.defineMaterial(material=tdur)
  left.defineMaterial(material=tdur)
  right.defineMaterial(material=tdur)

  # on affecte un contacteur jonc a chaque paroi
  # et on l'affecte aux parois
  down.addContactors(type='JONCx', color='WALLx', axe1=bx + radius_max, axe2=radius_max)
  up.addContactors(type='JONCx', color='WALLx', axe1=bx + radius_max, axe2=radius_max)
  left.addContactors(type='JONCx', color='WALLx', axe1=by + radius_max, axe2=radius_max)
  right.addContactors(type='JONCx', color='WALLx', axe1=by + radius_max, axe2=radius_max)

  # on calcule la surface et l'inertie de chaque paroi
  down.computeRigidProperties()
  up.computeRigidProperties()
  left.computeRigidProperties()
  right.computeRigidProperties()
  
  # on ajoute les parois a la liste des corps
  bodies += down; bodies += up; bodies += left; bodies += right

  # on tourne les parois verticales (par rapport a leur propres 
  # centre d'inertie)
  left.rotate(psi=-math.pi/2., center=left.nodes[1].coor)
  right.rotate(psi=math.pi/2., center=right.nodes[1].coor)

  # on fixe les parois
  down.imposeDrivenDof(component=[1, 2, 3], dofty='vlocy')
  up.imposeDrivenDof(component=[1, 2, 3], dofty='vlocy')
  left.imposeDrivenDof(component=[1, 2, 3], dofty='vlocy')
  right.imposeDrivenDof(component=[1, 2, 3], dofty='vlocy')

  # gestion des interactions :
  #   * declaration des lois
  #       - entre particules
  ldkdk=tact_behav(nom='iqsc0',type='IQS_CLB',fric=0.3)
  tacts+=ldkdk

  #   * declaration des tables de visibilite
  #       - entre particules
  svdkdk = see_table(CorpsCandidat='RBDY2',candidat='DISKx',colorCandidat='BLEUx',
                     behav=ldkdk,
                     CorpsAntagoniste='RBDY2',antagoniste='DISKx',colorAntagoniste='BLEUx',
                     alert=2*radius_min)
  svs+=svdkdk
  svdkjc = see_table(CorpsCandidat='RBDY2',candidat='DISKx',colorCandidat='BLEUx',
                     behav=ldkdk,
                     CorpsAntagoniste='RBDY2',antagoniste='JONCx',colorAntagoniste='WALLx',
                     alert=0.5*bx)
  svs+=svdkjc

  # ecriture des fichiers
  writeBodies(bodies,chemin='DATBOX/')
  writeBulkBehav(mat,chemin='DATBOX/')
  writeTactBehav(tacts,svs,chemin='DATBOX/')
  writeDrvDof(bodies,chemin='DATBOX/')
  writeDofIni(bodies,chemin='DATBOX/')
  writeVlocRlocIni(chemin='DATBOX/')

  # initialisation calcul

  checkDirectories()
  utilities_DisableLogMes()

  ####

  theta = 0.5

  ### definition des parametres du calcul ### 
  utilities_logMes('INIT TIME STEPPING')
  TimeEvolution_SetTimeStep(dt)
  Integrator_InitTheta(theta)

  ### lecture du modele ###

  ### model reading ###
  utilities_logMes('READ BODIES')
  RBDY2_ReadBodies()

  utilities_logMes('READ INI DOF')
  overall_ReadIniDof()
  RBDY2_ReadIniDof()

  utilities_logMes('READ BEHAVIOURS')
  bulk_behav_ReadBehaviours()
  tact_behav_ReadBehaviours()

  #LOADS
  DISKx_LoadTactors()
  JONCx_LoadTactors()
  RBDY2_LoadBehaviours()

  utilities_logMes('READ INI Vloc Rloc')
  overall_ReadIniVlocRloc()
  DKJCx_ReadIniVlocRloc()
  DKDKx_ReadIniVlocRloc()

  utilities_logMes('READ DRIVEN DOF')
  RBDY2_ReadDrivenDof()

  ### ecriture paranoiaque du modele ###
  utilities_logMes('WRITE BODIES')
  overall_WriteBodies()
  RBDY2_WriteBodies()

  utilities_logMes('WRITE BEHAVIOURS')
  bulk_behav_WriteBehaviours()
  tact_behav_WriteBehaviours()

  utilities_logMes('WRITE DRIVEN DOF')
  overall_WriteDrivenDof()
  RBDY2_WriteDrivenDof()

  ### post2D ##
  post2D_SetDisplayedField('CONTACT POINT   ')
  post2D_SetDisplayedField('TACTOR          ')
  post2D_Init()

  utilities_logMes('COMPUTE MASS')
  RBDY2_ComputeMass()

  # on remonte les infos
  n_particles = RBDY2_GetNbRBDY2() - 4
  n_info = 14
  particles = numpy.zeros ( [n_particles , n_info], 'd' )

  for i in xrange(0,n_particles) :
     X=RBDY2_GetBodyVector('Coor_',i+1,3)
     particles[ i , 0] = X[0]
     particles[ i , 1] = X[1]
     particles[ i , 2] = 0.
     particles[ i , 3] = 0.  # x velocity
     particles[ i , 4] = 0.  # y velocity
     particles[ i , 5] = 0.  # z velocity
     particles[ i , 6] = RBDY2_GetBodyArea(i+1) #surfac
     particles[ i , 7] = 0.  # x darcy velocity
     particles[ i , 8] = 0.  # y darcy velocity
     particles[ i , 9] = 0.  # z darcy velocity
     particles[ i ,10] = 0.  # x darcy force
     particles[ i ,11] = 0.  # y darcy force
     particles[ i ,12] = 0.  # z darcy force
     particles[ i ,13] = 0.  # compacity

  return particles

## update particles positions and velocities with a time step DT
def updateParticlesPositionsAndVelocities (particles, dt) :

  #
  utilities_logMes('INCREMENT STEP')
  TimeEvolution_IncrementStep()
  RBDY2_IncrementStep()

  utilities_logMes('DISPLAY TIMES')
  TimeEvolution_DisplayStep()

  utilities_logMes('COMPUTE Fext')
  RBDY2_ComputeFext()

  ############################################
  # apply darcy force as an external force
  Fext = numpy.zeros ( [3], 'd' )
  for i in range(0,particles.shape[0]):
    Fext[0] = dt*particles[i,10]
    Fext[1] = dt*particles[i,11]
    ## 3D FZ = particles[i,12]

    #print 'Force Darcy',Fext

    RBDY2_PutBodyVector('Fext_', i+1, Fext)    
  ###############################################

  utilities_logMes('COMPUTE Fint')
  RBDY2_ComputeBulk()
  #
  utilities_logMes('COMPUTE Free Vlocy')
  RBDY2_ComputeFreeVelocity()
  #
  utilities_logMes('SELECT PROX TACTORS')
  overall_SelectProxTactors(freq_detect)
  DKDKx_SelectProxTactors()
  DKJCx_SelectProxTactors()
  #
  DKJCx_RecupRloc()
  DKDKx_RecupRloc()
  nlgs_ExSolver(type, norm, tol, relax, gs_it1, gs_it2)
  DKJCx_StockRloc()
  DKDKx_StockRloc()
  #
  utilities_logMes('COMPUTE DOF')
  RBDY2_ComputeDof()
  #
  utilities_logMes('UPDATE DOF')
  TimeEvolution_UpdateStep()
  RBDY2_UpdateDof()
  #
  utilities_logMes('WRITE LAST DOF')
  overall_WriteOutDof(freq_write)
  RBDY2_WriteOutDof()
  #
  utilities_logMes('WRITE LAST Vloc Rloc')
  overall_WriteOutVlocRloc(freq_write)
  DKDKx_WriteOutVlocRloc()
  DKJCx_WriteOutVlocRloc()
  #
  ### post2D ###
  overall_WriteOutDisplayFile(freq_display)
  post2D_WriteOutDisplayFile(0)
  ### wrtieout handling ###
  overall_CleanWriteOutFlags()

  for i in range(0,particles.shape[0]):

    X=RBDY2_GetBodyVector('Coor_',i+1,3)

    #print 'position: ',X
    particles[ i , 0] =X[0]  # x position
    particles[ i , 1] =X[1]  # y position
    particles[ i , 2] = 0.   # z position

    X=RBDY2_GetBodyVector('V____',i+1,3)

    #print 'vitesse: ',X
    particles[ i , 3] = X[0]  # x velocity
    particles[ i , 4] = X[1]  # y velocity
    particles[ i , 5] = 0.    # z velocity


