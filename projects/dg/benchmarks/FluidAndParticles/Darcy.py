from dgpy import *
from Common import *
import numpy
import subprocess
import math
import time
import os

#----------------------------------------------------------------
#-- CLASS DEFINITION
#-- Darcy's Law :
#  - \nabla \cdot {K \over \mu} \grad P = - \nabla \cdot v_p
#    Alternative form
#  - \nabla \cdot {{K \phi} \over {\mu + \beta \kappa}} \grad P = - \nabla \cdot v_p
#
#   K is the permeability of the bulk that depends on its compacity [m^2]
#   \mu is the dynamic viscosity of the fluid [Pa sec ]
#   v_p is the "mesh" velocity of the particles [m/sec]
#   P is the pressure [Pa]
#   \phi is the local porosity of the medium = 1 - C []
#   C is the compacity = V_particles / V_all [] 
#   \beta =  { 150 \mu C^2} \over {d_p^2 \phi} is the viscous friction coefficient [N sec]
#   Permeability is K = {d_p^2 \phi^3} \over {150 C^2}
#----------------------------------------------------------------

# STATICS
# store particles information at mesh nodes
# particles velocities
dict_vx = {} # 
dict_vy = {}
dict_vz = {}
# particles momentum
dict_mx = {}
dict_my = {}
dict_mz = {}
# particles volumes
dict_volume_node = {}
dict_volume_element_node = {}
# store particles information at mesh elements
dict_volume = {}
dict_compacity = {}

def saveParticles (particles, iteration, T) :
  f = open("particles%d.pos"%(iteration), 'w')
  f.write("View \"\"{\n")
  for i in range(0,particles.shape[0]):
    X = particles[i,0]
    Y = particles[i,1]
    Z = particles[i,2]
    gradpX = particles[i,7]
    gradpY = particles[i,8]
    gradpZ = particles[i,9]
    fX = particles[i,10]
    fY = particles[i,11]
    fZ = particles[i,12]
    compacity = particles[i,13]
    vol = particles[i,6]
    density = 1.0
    mass  = vol * density
    f.write("VP(%g,%g,%g){%g,%g,%g};\n"%(X,Y,Z,fX,fY,fZ))
#    f.write("VP(%g,%g,%g){%g,%g,%g};\n"%(X,Y,Z,gradpX,gradpY,gradpZ))
    #    f.write("SP(%g,%g,%g){%g};\n"%(X,Y,Z,compacity))
    
  f.write("};\n")
  f.close()

def computeBeta ( C, dp,mu ) :
  PHI = 1.-C
  if  PHI > 0.75 : 
      PHI = 0.75
  if (PHI == 0.0) : PHI = 1.e-12
  BETA = (150.*mu/(dp*dp))*(1-PHI)**2/PHI
  return BETA  

def computePermeability ( C, dp ) :
  PHI = 1.-C
  if  PHI > 0.75 : 
      PHI = 0.75
  PERMEABILITY = (dp*dp/150.)*PHI**3/(1.-PHI)**2
  return PERMEABILITY  

def diffusion( FCT, XYZ, mu, dp  ) :
    element = FCT.getDataCacheMap().getElement()
    C = dict_compacity[element.getNum()]
    for i in range(0,XYZ.size1()) :
      PERMEABILITY = computePermeability (C, dp(i,0))
      X = XYZ.get(i,0)
      Y = XYZ.get(i,1)
      Z = XYZ.get(i,2)
      FCT.set(i,0, (1.-C) * PERMEABILITY/mu(i,0)) 

def div_V_particles ( FCT, XYZ ) :
    element = FCT.getDataCacheMap().getElement()
    N = element.getNumVertices()
    vx = fullVectorDouble (N)
    vy = fullVectorDouble (N)
    vz = fullVectorDouble (N)
    gradvx = fullVectorDouble (3)
    gradvy = fullVectorDouble (3)
    gradvz = fullVectorDouble (3)
            
    for j in range(0,N) :        
      v = element.getVertex(j)
      myVX = dict_mx[v.getNum()]/dict_volume_node[v.getNum()]
      myVY = dict_my[v.getNum()]/dict_volume_node[v.getNum()]
      myVZ = dict_mz[v.getNum()]/dict_volume_node[v.getNum()]
      vx.set(j,myVX)
      vy.set(j,myVY)
      vz.set(j,myVZ)

    # HERE I KNOW WE USE P1 TRIANGLES --> GRADIENTS ARE CONSTANT TABARNAK !!!!!
    element.interpolateGrad(vx.getDataPtr(),0.,0.,0., gradvx.getDataPtr() )
    element.interpolateGrad(vy.getDataPtr(),0.,0.,0., gradvy.getDataPtr() )
    element.interpolateGrad(vz.getDataPtr(),0.,0.,0., gradvz.getDataPtr() )
    divv = - (gradvx(0) + gradvy(1) + gradvz(2))
    for i in range(0,XYZ.size1()) :
        FCT.set(i,0,divv) 
        
        
class Darcy(object):

        def addVelocityContributionToVerticesAndElementContributionToVolumes ( self, x, y, z , vx, vy, vz , volume) :
          p = SPoint3(x,y,z)
          element = self.model.getMeshElementByCoord (p, self.dim)
          if (element == None) : return
          ## avoid null pointers !!!
          xu = fullMatrixDouble(2,3)
          xu.set(0,0,x)
          xu.set(0,1,y)
          xu.set(0,2,z)
          element.xyzTouvw (xu)
          N = element.getNumVertices()
          SF = fullVectorDouble (N)
          element.getShapeFunctions(xu.get(1,0),xu.get(1,1),xu.get(1,2),SF.getDataPtr())
          dict_volume[element.getNum()] = dict_volume[element.getNum()] + volume
          v_element = element.getVolume()
          for i in range(0,N) :
            v = element.getVertex(i)
            dict_vx[v.getNum()] = dict_vx[v.getNum()] + SF(i) * vx 
            dict_vy[v.getNum()] = dict_vy[v.getNum()] + SF(i) * vy 
            dict_vz[v.getNum()] = dict_vz[v.getNum()] + SF(i) * vz 
            dict_mx[v.getNum()] = dict_mx[v.getNum()] + SF(i) * vx * volume
            dict_my[v.getNum()] = dict_my[v.getNum()] + SF(i) * vy * volume
            dict_mz[v.getNum()] = dict_mz[v.getNum()] + SF(i) * vz * volume
            dict_volume_node[v.getNum()] = dict_volume_node[v.getNum()] + SF(i) * volume

	def __init__(self, nameMesh, dimension, particles):
            self.dim = dimension
            self.order = 1
            self.computeParameters(particles)
            self.nameMesh = nameMesh
            self.model = GModel()
            self.model.load(nameMesh+'.geo')
	    self.model.load(nameMesh+'.msh')
            self.groups = dgGroupCollection(self.model, self.dim, self.order)
            self.XYZ = function.getCoordinates()
            self.m_u = 1.e-3 # water
            self.mu = functionConstant(self.m_u) # water
            self.dp = functionConstant(self.d_p)
            self.K   = functionPython(1, diffusion, [self.XYZ,self.mu,self.dp])
            self.law = dgConservationLawAdvectionDiffusion.diffusionLaw(self.K)
            self.RHS = functionPython(1, div_V_particles, [self.XYZ])
            self.law.setSource(self.RHS)
            self.BC1 = self.law.new0FluxBoundary()
            self.law.addBoundaryCondition('Wall',self.BC1)
            self.const0 = functionConstant([0.0])
            self.law.addStrongBoundaryCondition(0,'CornerBottomLeft',self.const0)
            self.pressure=dgDofContainer(self.groups,1)

            for i in range(1,self.model.getNumMeshVertices()+1) :  
              dict_volume_element_node[self.model.getMeshVertexByTag(i).getNum()] = 0.0
            for i in range(1,self.model.getNumMeshElements()+1) :  
              element = self.model.getMeshElementByTag(i)
              N = element.getNumVertices()
              SF = 1./N
              v_element = element.getVolume()
              for j in range(0,N) :
                v = element.getVertex(j)
                dict_volume_element_node[v.getNum()] = dict_volume_element_node[v.getNum()] + SF * v_element
                
            

#-------------------------------------------------
#-- Compute average particle diameter and other properties
#-------------------------------------------------

        def computeParameters(self,particles) :
          self.d_p = 0.0
          for i in range(0,particles.shape[0]):
            volume_particle = particles[i,6]
            if (self.dim == 2) :
              d_i = math.sqrt(volume_particle/3.14159)
            else :
              d_i = math.pow(3*volume_particle/(4*3.14159),1./3.)
            self.d_p = self.d_p + d_i
          self.d_p = self.d_p / particles.shape[0]
          print 'Average of the areas is ',self.d_p
          
#-------------------------------------------------
#-- Transfer particles info to the FE mesh
#-------------------------------------------------

        def particlesToMesh (self, particles) :
          # reset particles infos ...
          for i in range(1,self.model.getNumMeshVertices()+1) :  
            dict_vx[self.model.getMeshVertexByTag(i).getNum()] = 0.0
            dict_vy[self.model.getMeshVertexByTag(i).getNum()] = 0.0
            dict_vz[self.model.getMeshVertexByTag(i).getNum()] = 0.0
            dict_mx[self.model.getMeshVertexByTag(i).getNum()] = 0.0
            dict_my[self.model.getMeshVertexByTag(i).getNum()] = 0.0
            dict_mz[self.model.getMeshVertexByTag(i).getNum()] = 0.0
            dict_volume_node[self.model.getMeshVertexByTag(i).getNum()] = 1.e-12
                
          for i in range(1,self.model.getNumMeshElements()+1) :  
            dict_volume[self.model.getMeshElementByTag(i).getNum()] = 0.0
            dict_compacity[self.model.getMeshElementByTag(i).getNum()] = 0.0

          total_v = 0.0
          for i in range(0,particles.shape[0]):
            X = particles[i,0]
            Y = particles[i,1]
            Z = particles[i,2]
            vX = particles[i,3]
            vY = particles[i,4]
            vZ = particles[i,5]
            volume_particle = particles[i,6]
            self.addVelocityContributionToVerticesAndElementContributionToVolumes ( X, Y, Z , vX, vY, vZ, volume_particle) 
            total_v = total_v + volume_particle

          for i in range(1,self.model.getNumMeshElements()+1) :  
            element = self.model.getMeshElementByTag(i)
            if (element.getDim() == self.dim) :
              comp = 0.0
              for j in range (0,element.getNumVertices()) :
                v = element.getVertex(j)
                node_compacity = dict_volume_node[v.getNum()] / dict_volume_element_node[v.getNum()]
                comp = comp + node_compacity

              comp  = comp / element.getNumVertices()
              if (comp >= 1.0) :
                print 'elements are too small with respect to grain size : compacity ', comp ,' is higher than 1'
                comp = .999
              if (comp == 0.0) :
                comp = 1.e-5
              dict_compacity[element.getNum()] = comp 
#              print 'comp(',  element.getNum() , ')=', comp, element.getVolume() 

          total_v2 = 0.
          for i in range(1,self.model.getNumMeshVertices()+1) :  
            v = self.model.getMeshVertexByTag(i)
            vn = dict_volume_node[v.getNum()]
            total_v2 = total_v2 + vn

          print total_v, total_v2

        def computeDragForces (self,particles) :
          for i in range(0,particles.shape[0]):
            X = particles[i,0]
            Y = particles[i,1]
            Z = particles[i,2]
            vX = particles[i,3]
            vY = particles[i,4]
            vZ = particles[i,5]
            vol = particles[i,6]
            C = particles[i,13]
            gradpX = particles[i,7]
            gradpY = particles[i,8]
            gradpZ = particles[i,9]
            K = computePermeability ( C, self.d_p )
    ## DRAG force : assume spherical particles
    ## volume = 4/3 pi r^3
    ## A = pi r^2 = pi * (3*volume/4pi) ^2/3
            A = 3.14 * self.d_p * self.d_p
            vx = -gradpX * K / self.m_u 
            vy = -gradpY * K / self.m_u 
            vz = -gradpZ * K / self.m_u 
            norm_dv = math.sqrt(vx*vx+vy*vy+vz*vz)
            if norm_dv == 0 :
              norm_dv = 1
            
            drag_coef = 0.47 * math.pow((1-C),-3.7)
            drag_force = 1000 * 0.5 * A * norm_dv * norm_dv * drag_coef

            particles[i,10] = -6.*3.14 * self.m_u * self.d_p * vx
            particles[i,11] = -6.*3.14 * self.m_u * self.d_p * vy
            particles[i,12] = -6.*3.14 * self.m_u * self.d_p * vz
#            return
#            particles[i,10] = -drag_force*vx/norm_dv
#            particles[i,11] = -drag_force*vy/norm_dv
#            particles[i,12] = -drag_force*vz/norm_dv
            
        def computeForces (self, particles) :
          for i in range(0,particles.shape[0]):
            X = particles[i,0]
            Y = particles[i,1]
            Z = particles[i,2]
            vX = particles[i,3]
            vY = particles[i,4]
            vZ = particles[i,5]
            vol = particles[i,6]
            gradpX = particles[i,7]
            gradpY = particles[i,8]
            gradpZ = particles[i,9]
            C = particles[i,13]
            # V = - K grad P
            # (see page 46 of A Martin's Ph.D.)
            # F = - VOL grad P + ({\beta VOL} \over {1 - \phi}) V
            K = computePermeability ( C, self.d_p )
            BETA = computeBeta(C,self.d_p,self.m_u)
            FACTOR = vol * ( 1.0 + BETA*K/(C*self.m_u))
            particles[i,10] =  -FACTOR * gradpX * 100
            particles[i,11] =  -FACTOR * gradpY * 100
            particles[i,12] =  -FACTOR * gradpZ * 100


            
## SOLVE THE EQUATION
        def solve (self, particles, iteration, T) :
          sys = linearSystemPETScDouble ()
#          petscOptions ="-ksp_rtol 1.e-5 -pc_type ilu -pc_factor_levels 0 -ksp_monitor"
          petscOptions ="-ksp_rtol 1.e-5 -pc_type ilu -pc_factor_levels 0"
          sys.setParameter("petscOptions", petscOptions)
          dof = dgDofManager.newCG (self.groups,  1,  sys)
          steady = dgSteady(self.law,dof);
          steady.getNewton().setRtol(10e+7)
          steady.solve(self.pressure)
          self.pressure.exportMsh("output/darcy-pre-test-%d"%(iteration), T, iteration);
  ## compute darcy velocities
          evalGrad=dgFunctionEvaluator(self.groups,self.pressure.getFunctionGradient())
          dresult = fullMatrixDouble(3,1)
          for i in range(0,particles.shape[0]):
            X = particles[i,0]
            Y = particles[i,1]
            Z = particles[i,2]
            evalGrad.compute(X,Y,0.0,dresult)
            p = SPoint3(X,Y,Z)
            element = self.model.getMeshElementByCoord (p, self.dim)
            C = dict_compacity[element.getNum()]
            particles[i,7]=dresult(0,0)
            particles[i,8]=dresult(1,0)
            particles[i,9]=dresult(2,0)
            particles[i,13]= C
                  

        def saveDictionary (self, iteration, T) :
          f = open("dictionary%d.pos"%(iteration), 'w')
          f.write("View \"\"{\n")
          for i in range(1,self.model.getNumMeshVertices()+1) :  
            v = self.model.getMeshVertexByTag(i)
            X = v.x()
            Y = v.y()
            Z = v.z()
            #    V = dict_volume_node[v.getNum()]
            vx = dict_vx[v.getNum()]
            vy = dict_vy[v.getNum()]
            vz = dict_vz[v.getNum()]
            mx = dict_mx[v.getNum()]
            my = dict_my[v.getNum()]
            mz = dict_mz[v.getNum()]
            normv = math.sqrt(vx*vx+vy*vy+vz*vz)
            if (normv == 0) : normv = 1.0;
            V = math.sqrt(mx*mx+my*my+mz*mz)/normv
            #    f.write("VP(%g,%g,%g){%g,%g,%g};\n"%(X,Y,Z,vx,vy,vz))
            f.write("SP(%g,%g,%g){%g};\n"%(X,Y,Z,V))
            
          f.write("};\n")
          f.close()
      
        def saveParticlesOnMeshElem (self, iteration, T, what) :
          f = open("elem%d-%d.pos"%(iteration,what), 'w')
          f.write("View \"\"{\n")
          for i in range(1,self.model.getNumMeshElements()+1) :  
            e = self.model.getMeshElementByTag(i)
            if (e.getDim() == self.dim) :
              v0 = e.getVertex(0)
              v1 = e.getVertex(1)
              v2 = e.getVertex(2)
              if (what == 1) :
                V = dict_compacity[e.getNum()]
                f.write("ST(%g,%g,%g,%g,%g,%g,%g,%g,%g){%g,%g,%g};\n"%(v0.x(),v0.y(),v0.z(),v1.x(),v1.y(),v1.z(),v2.x(),v2.y(),v2.z(),V,V,V))
              elif (what == 2) :
                V0 = dict_volume_node[v0.getNum()]
                V1 = dict_volume_node[v1.getNum()]
                V2 = dict_volume_node[v2.getNum()]
                f.write("ST(%g,%g,%g,%g,%g,%g,%g,%g,%g){%g,%g,%g};\n"%(v0.x(),v0.y(),v0.z(),v1.x(),v1.y(),v1.z(),v2.x(),v2.y(),v2.z(),V0,V1,V2))
              elif (what == 3) :
                V = dict_volume[e.getNum()]/e.getVolume()
                f.write("ST(%g,%g,%g,%g,%g,%g,%g,%g,%g){%g,%g,%g};\n"%(v0.x(),v0.y(),v0.z(),v1.x(),v1.y(),v1.z(),v2.x(),v2.y(),v2.z(),V,V,V))
              
          f.write("};\n")
          f.close()
    
