LX = 0.5;
LY = 2;
LC = LX /10;
Point(1) = {0, 0., 0, LC};
Point(2) = {LX, 0., 0, LC};
Point(3) = {LX, LY, 0, LC};
Point(4) = {0, LY, 0, LC};
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line Loop(7) = {1, 2, 3, 4};
Plane Surface(8) = {7};

Physical Line("Wall") = {1,2,3,4};
Physical Point("CornerBottomLeft") = {1};

Physical Surface("Domain") = {8};
