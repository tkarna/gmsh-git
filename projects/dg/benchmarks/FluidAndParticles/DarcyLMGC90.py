from dgpy import *
from LMGC90Particles import *
from Darcy import *
from math import *
import time
import sys
import numpy

######### MAIN PROGRAM ##################################################

os.system("mkdir output")
os.system("mkdir DATBOX")


DT = 0.005
T = 0.
name = 'darcy'
ITMAX = 300
density_particles = 2700.
density_fluid = 1000.


particles = createInitialDistributionOfParticles (DT,density_particles - density_fluid)
darcySolver = Darcy (name,2, particles)



for iteration in range (1,ITMAX) :

  print '## ITER ', iteration, 'T = ', T, '##'

  ## transfer particles info to the mesh
  darcySolver.particlesToMesh ( particles )

  ## compute Darcy
  darcySolver.solve (particles,iteration, T)

  ## compute particles force Due to Fluid Interctaions
  darcySolver.computeForces (particles) 
#  darcySolver.computeDragForces (particles) 

  ## save particle positions and velocities
  saveParticles (particles, iteration, T) 
  darcySolver.saveDictionary ( iteration, T) 
  darcySolver.saveParticlesOnMeshElem ( iteration, T, 1) 
  darcySolver.saveParticlesOnMeshElem ( iteration, T, 2) 
  darcySolver.saveParticlesOnMeshElem ( iteration, T, 3) 

  ## update Particles Positions and Velocities using LMGC90
  updateParticlesPositionsAndVelocities (particles, DT) 

  ## increase time step
  T = T + DT
  
