"""
    Simulations on the GBR with multirate explicit Runge-Kutta time-stepping
"""


# Import for python
from gmshpy import *
from dgpy import *
from math import *
import time

# File name and extensions
filename="gbr_quad"
msh=".msh"
geo=".geo"
outputDir ="output"

# Mesh Parameters
order = 1
dimension = 2

# Time Integration Parameters
Ti = 0   
#Tf = 24*60*60 
Tf=60

# Multirate Parameters
mL = 1000    

# Physical Parameters
PI=3.14159265


# Define The Python Functions
def projectMesh (model):
  def project(entity):
    print entity
    numVertices = entity.getNumMeshVertices()
    m = fullMatrixDouble (3,3)
    for iV in range(0,numVertices): 
      v = entity.getMeshVertex(iV)
      pEphi = v.x()*ePhiX + v.y()*ePhiY + v.z()*ePhiZ
      pEtheta = v.x()*eThetaX + v.y()*eThetaY + v.z()*eThetaZ
      m.set(0, 0, ePhiX)
      m.set(0, 1, ePhiY)
      m.set(0, 2, ePhiZ)
      m.set(1, 0, eThetaX)
      m.set(1, 1, eThetaY)
      m.set(1, 2, eThetaZ)
      m.set(2, 0, -v.x())
      m.set(2, 1, -v.y())
      m.set(2, 2, -v.z())
      m.invertInPlace()
      R = sqrt(v.x()**2 + v.y()**2+ v.z()**2)
      alpha = -Ox*m.get(0,0) - Oy*m.get(1,0) - Oz * m.get(2,0)
      beta = -Ox*m.get(0,1) - Oy*m.get(1,1) - Oz * m.get(2,1)
      v.setXYZ(alpha*R, beta*R, 0)
      #local pEphi = v:x()*ePhiX + v:y()*ePhiY + v:z()*ePhiZ
      #local pEtheta = v:x()*eThetaX + v:y()*eThetaY + v:z()*eThetaZ
      #v:setXYZ(pEphi, pEtheta, 0)

  def pairs(it):
    it = iter(it)
    prev = next(it)
    for v in it:
      yield prev,  v
      prev = v

  #vIter = model.firstVertex()

  gvertices = model.bindingsGetVertices()
  for i,v in pairs(gvertices):
    print"(i=", i, 'v=', v, ')'
    project(v)
  
  gedges = model.bindingsGetEdges()
  for i,e in pairs(gedges):
    project(e)

  gfaces = model.bindingsGetFaces()
  for i,f in pairs(gfaces):
    project(f)


phi = 146.5892/180*PI
theta = -17.147569/180*PI
Ox = cos(theta)*cos(phi)
Oy = cos(theta)*sin(phi)
Oz = sin(theta)
eThetaX = -sin(theta)*cos(phi)
eThetaY = -sin(theta)*sin(phi)
eThetaZ = cos(theta)
ePhiX = -sin(phi)
ePhiY = cos(phi)
ePhiZ = 0

def tide (f):
  for i in range(0,f.size1()):
    f.set(i, 0, sin(t*2*PI/3600/12))
    f.set(i, 1, 0)
    f.set(i, 2, 0)

print ' .... Project the Mesh in Planar Space ....'
m = GModel()
m.load(filename+msh)
projectMesh(m)

print ' .... Loading the Mesh and the Model ....'
m.save(filename+"_tan"+msh)
#m.delete()
model = GModel()
model.load(filename+"_tan"+msh)

# Define C Code to be Compiled before Launching the Testcase
CCode = """
#include "fullMatrix.h"
#include "dgDataCacheMap.h"
#include "dgGroupOfElements.h"
#include "math.h"
static double pOx, pOy, pOz, pPhiX, pPhiY, pPhiZ, pThetaX, pThetaY, pThetaZ;
static bool init = false;

static void initialize() {
  double phi = 146.5892/180*M_PI;
  double theta = -17.147569/180*M_PI;
  double R = 6.37101e6;
  pOx = cos(theta)*cos(phi)*R;
  pOy = cos(theta)*sin(phi)*R;
  pOz = sin(theta)*R;
  pPhiX = -sin(phi);
  pPhiY = cos(phi);
  pPhiZ = 0;
  pThetaX = -sin(theta)*cos(phi);
  pThetaY = -sin(theta)*sin(phi);
  pThetaZ = cos(theta);
  init = true;
}
extern "C" {
void wind (dgDataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< sol.size1(); i++) {
    sol.set(i,0,sin(xyz(i,1)/1e6)/1e6);
    sol.set(i,1,0);
  }
}
void coriolis (dgDataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< sol.size1(); i++) {
    sol.set(i,0,2*7.292e-5*xyz(i,2)/6.37101e6);
  }
}
void lonlat (dgDataCacheMap *, fullMatrix<double> &lonlat, fullMatrix<double> &xyz) {
  if (! init) initialize();
  for (size_t i = 0; i< lonlat.size1(); i++) {
    double x = pPhiX * xyz(i,0) + pThetaX * xyz(i,1) + pOx;
    double y = pPhiY * xyz(i,0) + pThetaY * xyz(i,1) + pOy;
    double z = pPhiZ * xyz(i,0) + pThetaZ * xyz(i,1) + pOz;
    double r = sqrt(x*x+y*y+z*z);
    lonlat.set(i, 0, atan2(y,x));
    lonlat.set(i, 1, asin(z/r));
    lonlat.set(i, 2, 0);
  }
}

void bottomDrag(dgDataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &sol, fullMatrix<double> &bath){
  for(int i=0;  i<val.size1(); i++){
    double H = sol(i, 0)+bath(i, 0);
    val.set(i, 0, -9.81*0.0235*0.0235/(pow(H, 1.333333333333)));
  }
}

void smagorinsky(dgDataCacheMap *map, fullMatrix<double> &val, fullMatrix<double> &solGradient){
  for(int i=0;  i<val.size1(); i++){
    double dudx = solGradient(i, 3);
    double dudy = solGradient(i, 4);
    double dvdx = solGradient(i, 6);
    double dvdy = solGradient(i, 7);
   
    const dgGroupOfElements *g = map->getGroupOfElements();
    int elNum = map->getElementId();

    double radi =g->getInnerRadius(elNum);
    
    val.set(i, 0, pow(0.1*radi, 2)*sqrt(2*dudx*dudx+2*dvdy*dvdy+pow(dudy+dvdx, 2)));
  }
}

void current (dgDataCacheMap *, fullMatrix<double> &current, fullMatrix<double> &solution) {
  for (size_t i = 0; i< current.size1(); i++) {
    current.set(i, 0, solution(i,1));
    current.set(i, 1, solution(i,2));
    current.set(i, 2, 0);
  }
}
}
"""

CCodeBath ="""
#include "fullMatrix.h"
#include "dgDataCacheMap.h"
static double Ox, Oy, dx, dy;
static int nx, ny;
static double *data;
static bool init = false;

static void initialize() {
  FILE *input = fopen("full.bin", "r");
  double buf;
  fread (&Ox, sizeof(double), 1, input);
  fread (&Oy, sizeof(double), 1, input);
  fread (&buf, sizeof(double), 1, input);
  fread (&dx, sizeof(double), 1, input);
  fread (&dy, sizeof(double), 1, input);
  fread (&buf, sizeof(double), 1, input);
  fread (&nx, sizeof(int), 1, input);
  fread (&ny, sizeof(int), 1, input);
  fread (&buf, sizeof(int), 1, input);
  data = new double[nx*ny];
  fread (data, sizeof(double), nx*ny, input);
  init = true;
}
extern "C" {
void bath (dgDataCacheMap *, fullMatrix<double> &bath, fullMatrix<double> &lonlat) {
  if (! init) initialize();
  for (size_t i = 0; i< bath.size1(); i++) {
    double rx = (lonlat(i,0)-Ox);
    double ry = (lonlat(i,1)-Oy);
    int iX = rx/dx;
    int iY = ry/dy;
    double alpha = (rx - iX*dx) / dx;
    double beta = (ry - iY*dy) / dy;
    double b = 
      (1-alpha)*(1-beta)*data[iX*ny+iY]
      +alpha*(1-beta)*data[(iX+1)*ny+iY]
      +alpha*beta*data[(iX+1)*ny+(iY+1)]
      +(1-alpha)*beta*data[iX*ny+(iY+1)];
    b = -b;

    if (b<0) b=5;
    else if (b<100) b = b + 5*exp(-b/5.0);
    else if (b < 210) b = b - 10*exp((b-210.)/10.);
    else b = 200;

//    b = std::max (b, 5.);
//    b = std::min (b, 200.);
    bath.set (i, 0, b);
  }
}
}
"""

print '.... Compiling Libraries ....'
if (Msg.GetCommRank() == 0 ):
  functionC.buildLibrary (CCode, "lib1.so");
  functionC.buildLibrary (CCodeBath, "libbath.so");

Msg.Barrier()

print '.... Define, Interpolate and Smooth Bathymetry ....'
groups = dgGroupCollection(model, dimension, order)
bathDC = dgDofContainer(groups, 1);
XYZ = getFunctionCoordinates();
lonlat = functionC("lib1.so", "lonlat", 3, [XYZ])
bathS = functionC("libbath.so", "bath", 1, [lonlat])
bathDC.interpolate(bathS);
bathDC.exportMsh(outputDir+"/bath", 0, 0);
groups.buildGroupsOfInterfaces()

# diffuse bath
dlaw = dgConservationLawAdvectionDiffusion.diffusionLaw(functionConstant([20]))
dlaw.addBoundaryCondition("Coast", dlaw.new0FluxBoundary())
dlaw.addBoundaryCondition("Islands", dlaw.new0FluxBoundary())
dlaw.addBoundaryCondition("Open Sea", dlaw.new0FluxBoundary())
sys = linearSystemPETScDouble()
dof = dgDofManager.newCG (groups, 1, sys)
dgSolver.iterateImplicitEuler (bathDC, dlaw, dof, 100000, 0)
bathDC.exportMsh(outputDir+"/bath_smooth", 0, 0);
sys.delete()
dof.delete()

print '.... Define Conservation Law,  Set Parameters,  Add Boundary Conditions ....'
claw = dgConservationLawShallowWater2d()
solution = dgDofContainer(groups, claw.getNbFields())
Cd = functionC("lib1.so", "bottomDrag", 1, [solution.getFunction(), bathDC.getFunction()])
Di = functionC("lib1.so", "smagorinsky", 1, [solution.getFunctionGradient()])
claw.setCoriolisFactor(functionC("lib1.so","coriolis",1,[XYZ]))
claw.setQuadraticDissipation(Cd)
claw.setLinearDissipation(functionConstant([0]))
#claw.setDiffusivity(Di)
claw.setDiffusivity(functionConstant([20]))
#claw.setSource(functionC("lib1.so","wind",2,{XYZ}))
claw.setBathymetry(bathDC.getFunction())
claw.setBathymetryGradient(bathDC.getFunctionGradient())
claw.addBoundaryCondition('Open Sea',claw.newOutsideValueBoundary("Surface", functionPython(3,tide, [])))
claw.addBoundaryCondition('Coast',claw.newBoundaryWall())
claw.addBoundaryCondition('none',claw.newBoundaryWall())
claw.addBoundaryCondition('Islands',claw.newBoundaryWall())

print '.... Define Multirate Scheme To Use, Compute the Reference Time Step ....'
groups.deleteGroupsOfInterfaces()
rk=dgRungeKuttaMultirateConservative.new2a(groups, claw)
dt=rk.splitForMultirate(mL, solution, [solution, bathDC])
groups.buildGroupsOfInterfaces()
solution.exportGroupIdMsh()
solution.exportMultirateGroupMsh()

currentFunction = functionC("lib1.so","current",3,[solution.getFunction()])

#limiter = dgLimiterClip(claw)
#rk:setNewLimiter(limiter)

pc=1
t=0
nbExport = 0
print '-------------------- Initializing --------------------'
print ''
print('Reference Time Step:', dt)
print('Norm of Solution At T0:',  solution.norm())
print ''
print '------------------------------------------------------'
solution.exportMsh(outputDir+'/gbr-%06d'%(0), t, nbExport)
solution.exportFunctionMsh(currentFunction, outputDir+'/gbr_current-%06d'%(0), t, nbExport, 'f')


nbExport=nbExport + 1
startcpu = time.clock()
nbSteps = int(ceil(Tf/dt))
print ('.... Start Iterations(',nbSteps,') ....')
print ('')

for i in range(1,nbSteps):
  norm = rk.iterate (t, dt, solution)
  t = t +dt
  if ( i%pc ==0 ):
     print('|ITER|',i,'|NORM|',norm,'|DT|',dt,'|T(hour)|',t/3600 )
     print('|PROC|',Msg.getCommRank(),'|CPU|',time.clock() - startcpu)
     print'---------------------------------------------------------------------------------------------------------'
     print''
  if ( i%(10*pc) ==0 ):
     solution.exportMsh(outputDir+'/gbr-%06d'%(i), t, nbExport)
     solution.exportFunctionMsh(currentFunction, outputDir+'/gbr_current-%06d'%(i), t, nbExport, 'f')
     nbExport = nbExport + 1

endcpu=time.clock()
print'*************************************'
print('|TOTAL CPU|',    endcpu - startcpu)
print'*************************************'
print'.... DONE ....'
