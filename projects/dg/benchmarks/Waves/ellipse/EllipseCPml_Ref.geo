a = 1500;
b = 800;

lc = 30;

pospulse_x = -800;
pospulse_y = 400;
lext = 2500;

Point(101) = { 0, 0, 0, lc};
Point(102) = { 1, 0, 0, lc};

Point(103) = { a, 0, 0, lc/2};
Point(104) = { 0, b, 0, lc};
Point(105) = {-a, 0, 0, lc/2};
Point(106) = { 0,-b, 0, lc};

Point(107) = { pospulse_x, pospulse_y, 0, lc};

Point(108) = { lext+pospulse_x,      pospulse_y,0.,2*lc};
Point(109) = {      pospulse_x, lext+pospulse_y,0.,2*lc};
Point(110) = {-lext+pospulse_x,      pospulse_y,0.,2*lc};
Point(111) = {      pospulse_x,-lext+pospulse_y,0.,2*lc};

Ellipse(201) = {103, 101, 102, 104};
Ellipse(202) = {104, 101, 102, 105};
Ellipse(203) = {105, 101, 102, 106};
Ellipse(204) = {106, 101, 102, 103};

Circle(205) = {108, 107, 109};
Circle(206) = {109, 107, 110};
Circle(207) = {110, 107, 111};
Circle(208) = {111, 107, 108};

bnd_dom[] = {201, 202, 203, 204};
bnd_ext[] = {205, 206, 207, 208};

bnd_dom_ll = newll;
Line Loop(bnd_dom_ll) = {bnd_dom[]};
sur_dom = news;
Plane Surface(sur_dom) = {bnd_dom_ll};

bnd_ext_ll = newll;
Line Loop(bnd_ext_ll) = {bnd_ext[], -bnd_dom[]};
sur_ext = news;
Plane Surface(sur_ext) = {bnd_ext_ll};

Physical Line("INTERFACE") = {bnd_dom[]};
Physical Line("BOUNDARY")  = {bnd_ext[]};

Physical Surface("DOM") = {sur_dom};
Physical Surface("EXT") = {sur_ext};
