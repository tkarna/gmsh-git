from gmshpy import *
from dgpy import *
import os
import time
import math


print'---- Load Mesh and Groups'

model = GModel()
model.load('EllipsePml.msh')

dim = 2
order = 1
groups = dgGroupCollection(model, dim, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()


print'---- Build Initial conditions'

pospulse = -300
r = 20000
def initialCondition(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)-pospulse
    y = xyz.get(i,1)
    val.set(i,0,math.exp(-(x*x+y*y)/r))
    val.set(i,1,0.)
    val.set(i,2,0.)
    val.set(i,3,math.exp(-(x*x+y*y)/r))
initialConditionPython = functionPython(4,initialCondition,[xyz])

solutionref = dgDofContainer(groups,3)
solutionref.setFieldName(0,'p_ref')
solutionref.setFieldName(1,'u_ref')
solutionref.setFieldName(2,'v_ref')
solutionref.L2Projection(initialConditionPython)
solutionref.exportMsh('output/EllipseRef_00000',0,0)

solutionpml = dgDofContainer(groups,4)
solutionpml.setFieldName(0,'p_pml')
solutionpml.setFieldName(1,'u_pml')
solutionpml.setFieldName(2,'v_pml')
solutionpml.setFieldName(3,'q_pml')
solutionpml.L2Projection(initialConditionPython)
solutionpml.exportMsh('output/EllipsePml_00000',0,0)


print'---- Build Conservation laws'

lawref = dgConservationLawWave(dim,0)
lawpml = dgConservationLawWave(dim,1)

cValue = 1.
rhoValue = 1.
sigmaValue = 0.
coef = functionConstant([cValue,rhoValue,sigmaValue])
lawref.setPhysCoef(coef)
lawpml.setPhysCoef(coef)


print'---- Build Pml'

ell_a = 1000*2
ell_b = 800
ell_maxscale = 1.2

CCode = """
#include "fullMatrix.h"
#include "dgDataCacheMap.h"

extern "C" {

double ell_a = 1000*2;
double ell_b = 800;
double ell_maxscale = 1.2;

double cValue = 1.;
double rhoValue = 1.;

void absCoefDef (dgDataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< sol.size1(); i++) {
    
    // Coordinates
    double x = xyz(i,0);
    double y = xyz(i,1);
    
    // Normal vector to the ellipse
    double normale_x = ell_b*x/ell_a;
    double normale_y = ell_a*y/ell_b;
    
    // Norm of the normal vector
    double norm_normale = sqrt(normale_x*normale_x+normale_y*normale_y);
    
    // Scale of the current ellipse
    double ell_scale = sqrt(x*x/(ell_a*ell_a) + y*y/(ell_b*ell_b));   // (1 = small ellipse, 'ell_maxscale' = large ellipse)
    double ell_coord = (ell_scale-1.)/(ell_maxscale-1.);                   // (0 = small ellipse, 1 = large ellipse)
    
    // Value of sigmaPml
    double sigmaPml = 0.;
    if (ell_coord>0)
      sigmaPml = (cValue/160.) * (ell_coord)/((1.01)-ell_coord);
    
    sol.set(i,0,sigmaPml);
  }
}

void absDirDef (dgDataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< sol.size1(); i++) {
    
    // Coordinates
    double x = xyz(i,0);
    double y = xyz(i,1);
    
    // Normal vector to the ellipse
    double normale_x = ell_b*x/ell_a;
    double normale_y = ell_a*y/ell_b;
    
    // Norm of the normal vector
    double norm_normale = sqrt(normale_x*normale_x+normale_y*normale_y);
    
    // Absorption direction
    double vec_x = normale_x/norm_normale;
    double vec_y = normale_y/norm_normale;
    
    sol.set(i,0,vec_x);
    sol.set(i,1,vec_y);
  }
}

}
"""

tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CCode, tmpLib);
absCoef    = functionC(tmpLib,"absCoefDef",1,[xyz])
absDir     = functionC(tmpLib,"absDirDef",2,[xyz])

#coefpmlsave = dgDofContainer(groups,1)
#coefpmlsave.interpolate(absCoef)
#coefpmlsave.exportMsh('output/EllipseCoef',0,0)

lawpml.setAbsCoef(absCoef)
lawpml.setAbsDir(absDir)

lawpml.addPml('PML')
lawpml.addInterfaceWall('BOUNDARY_DOMPML')
lawpml.addInterfacePml('BOUNDARY_DOM')


print'---- Build Boundary conditions'

lawref.addBoundaryCondition('BOUNDARY_DOMREF', lawref.newBoundaryWall('BOUNDARY_DOMREF'))
lawref.addBoundaryCondition('BOUNDARY_OBS',    lawref.newBoundaryWall('BOUNDARY_OBS'))
lawpml.addBoundaryCondition('BOUNDARY_DOMREF', lawpml.newBoundaryWall('BOUNDARY_DOMREF'))
lawpml.addBoundaryCondition('BOUNDARY_OBS',    lawpml.newBoundaryWall('BOUNDARY_OBS'))


print'---- LOOP'

implicit = True;

if (implicit):
  dt = 25
  
  sysref = linearSystemPETScBlockDouble()
  dofref = dgDofManager.newDGBlock(groups, 3, sysref)
  solverref = dgDIRK(lawref, dofref, 2)
  solverref.getNewton().setVerb(10)
  
  syspml = linearSystemPETScBlockDouble()
  dofpml = dgDofManager.newDGBlock(groups, 4, syspml)
  solverpml = dgDIRK(lawpml, dofpml, 2)
  solverpml.getNewton().setVerb(10)
  
  for i in range(1,70):
    t = i*dt
    #solverref.iterate(solutionref, dt , t)
    solverpml.iterate(solutionpml, dt , t)
    print'|ITER|', i, '|DT|', dt, '|t|', t
    #solutionref.exportMsh("output/EllipseRef_%05i" % (i), t, i)
    solutionpml.exportMsh("output/EllipsePml_%05i" % (i), t, i)
  
else:
  dt = 0.0005
  nbExport = 1
  rk = dgRungeKutta()
  for i in range(0,300):
    t = i*dt
    normref = rk.iterate44(lawref, t, dt, solutionref)
    normpml = rk.iterate44(lawpml, t, dt, solutionpml)
    if (math.isnan(normpml)):
      break
    if (i % 5 == 0):
      print'|ITER|', i, '|NORM_Ref|', normref, '|NORM_PML|', normpml, '|DT|', dt, '|t|', t
      solutionref.exportMsh("output/EllipseRef_%05i" % (nbExport), t, nbExport)
      solutionpml.exportMsh("output/EllipsePml_%05i" % (nbExport), t, nbExport)
      nbExport  = nbExport + 1


Msg.Exit(0)

