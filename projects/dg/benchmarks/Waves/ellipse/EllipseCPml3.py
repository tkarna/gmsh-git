from gmshpy import *
from dgpy import *
import os
import time
import math


print'---- Load Mesh and Groups'

model = GModel()
model.load('EllipseCPml.msh')

dim = 2
order = 1
groups = dgGroupCollection(model, dim, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()


print'---- Build Conservation laws'

law = dgConservationLawWave(dim,'BasicLayer')
nbfields = 3

cValue = 1.
rhoValue = 1.
coef = functionConstant([cValue,rhoValue])
law.setPhysCoef(coef)


print'---- Build Initial conditions'

pospulse_x = -800
pospulse_y = 400
r = 25000
def initialCondition(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)-pospulse_x
    y = xyz.get(i,1)-pospulse_y
    val.set(i,0,math.exp(-(x*x+y*y)/r))
    val.set(i,1,0.)
    val.set(i,2,0.)
initialConditionPython = functionPython(nbfields,initialCondition,[xyz])

solution = dgDofContainer(groups,nbfields)
solution.setFieldName(0,'p_basic')
solution.setFieldName(1,'u_basic')
solution.setFieldName(2,'v_basic')
solution.L2Projection(initialConditionPython)
solution.exportMsh('output/EllipseBasic_00000',0,0)


print'---- Build Pml'

model.load('EllipseCPml_Distance.pos')
model.load('EllipseCPml_Direction.pos')
law.useGenericPmlCoef('Ellipse', 120.5,cValue, 0.,0.,0., 1500.,800.,-1.)
law.useGenericPmlDir('Ellipse')

law.addPml('PML')


print'---- Build Boundary conditions'

law.addBoundaryCondition('BOUNDARY', law.newBoundaryWall('BOUNDARY'))


print'---- LOOP'

implicit = True;

if (implicit):
  dt = 25
  sys = linearSystemPETScBlockDouble()
  dof = dgDofManager.newDGBlock(groups, nbfields, sys)
  solver = dgDIRK(law, dof, 2)
  solver.getNewton().setVerb(10)
  for i in range(1,70):
    t = i*dt
    solver.iterate(solution, dt , t)
    solution.exportMsh("output/EllipseBasic_%05i" % (i), t, i)
    print'|ITER|', i, '|DT|', dt, '|t|', t
  
else:
  dt = 0.0005
  nbExport = 1
  rk = dgRungeKutta()
  for i in range(0,300):
    t = i*dt
    norm = rk.iterate44(law, t, dt, solution)
    if (math.isnan(norm)):
      break
    if (i % 5 == 0):
      print'|ITER|', i, '|NORM|', norm, '|DT|', dt, '|t|', t
      solution.exportMsh("output/EllipseBasic_%05i" % (nbExport), t, nbExport)
      nbExport = nbExport + 1


Msg.Exit(0)

