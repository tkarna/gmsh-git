
a = 300*Sqrt(2);
b = 300;

posobs_x = 120;
posobs_y = -50;

lc = 15;


// Ellipse and Pml

Point(101) = { 0, 0, 0, lc};
Point(102) = { 1, 0, 0, lc};

Point(103) = { a, 0, 0, lc/2};
Point(104) = { 0, b, 0, lc};
Point(105) = {-a, 0, 0, lc/2};
Point(106) = { 0,-b, 0, lc};

Ellipse(201) = {103, 101, 102, 104};
Ellipse(202) = {104, 101, 102, 105};
Ellipse(203) = {105, 101, 102, 106};
Ellipse(204) = {106, 101, 102, 103};

bnd_dom[] = {201, 202, 203, 204};
bnd_dom_ll = newll; Line Loop(bnd_dom_ll) = {bnd_dom[]};

For k In {0:#bnd_dom[]-1}
    out[] = Extrude{ Line{bnd_dom[k]}; Layers{5, 4*lc};};// Recombine; };
    bnd_pml[] += out[0];
    sur_pml[] += out[1];
EndFor


// Submarine

Point(1001)  = {  181+posobs_x,  18+posobs_y, 0, lc};
Point(1002)  = {  -70+posobs_x,  18+posobs_y, 0, lc};
Point(1003)  = {  -70+posobs_x,  38+posobs_y, 0, lc};
Point(1004)  = { -120+posobs_x,  38+posobs_y, 0, lc};
Point(1005)  = { -120+posobs_x,  18+posobs_y, 0, lc};
Point(1006)  = { -220+posobs_x,  18+posobs_y, 0, lc};
Point(1007)  = { -238+posobs_x,   0+posobs_y, 0, lc};
Point(10070) = { -220+posobs_x,   0+posobs_y, 0, lc};
Point(1008)  = { -220+posobs_x, -18+posobs_y, 0, lc};
Point(1009)  = {  181+posobs_x, -18+posobs_y, 0, lc};
Point(1010)  = {  191+posobs_x, -38+posobs_y, 0, lc};
Point(1011)  = {  210+posobs_x, -38+posobs_y, 0, lc};
Point(1012)  = {  210+posobs_x, -10+posobs_y, 0, lc};
Point(1013)  = {  228+posobs_x,   0+posobs_y, 0, lc};
Point(1014)  = {  210+posobs_x,  10+posobs_y, 0, lc};
Point(1015)  = {  210+posobs_x,  38+posobs_y, 0, lc};
Point(1016)  = {  191+posobs_x,  38+posobs_y, 0, lc};

Line(1001) = {1001, 1002};
Line(1002) = {1002, 1003};
Line(1003) = {1003, 1004};
Line(1004) = {1004, 1005};
Line(1005) = {1005, 1006};
Circle(1006) = {1006, 10070, 1007};
Circle(1007) = {1007, 10070, 1008};
Line(1008) = {1008, 1009};
Line(1009) = {1009, 1010};
Line(1010) = {1010, 1011};
Line(1011) = {1011, 1012};
Line(1012) = {1012, 1013};
Line(1013) = {1013, 1014};
Line(1014) = {1014, 1015};
Line(1015) = {1015, 1016};
Line(1016) = {1016, 1001};

bnd_obs[] = {1001, 1002, 1003, 1004, 1005, 1006, 1007, 1008, 1009, 1010, 1011, 1012, 1013, 1014, 1015, 1016};
bnd_obs_ll = newll; Line Loop(bnd_obs_ll) = {bnd_obs[]};


// Physical elements

sur_dom = news; Plane Surface(sur_dom) = {bnd_dom_ll, bnd_obs_ll};

Physical Line("BOUNDARY_OBS")  = {bnd_obs[]};
Physical Line("INTERFACE") = {bnd_dom[]};
Physical Line("BOUNDARY") = {bnd_pml[]};

Physical Surface("DOM") = {sur_dom};
Physical Surface("PML") = {sur_pml[]};
