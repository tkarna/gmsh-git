lref      = 3000;
ldom_a    = 1000*2;
ldom_b    = 800;
ldompml_a = 1000*2*1.2;
ldompml_b = 800*1.2;

pospulse = -300;
posobs   = 700;
lobs     = 300;

lc = 120;

Point(1) = {        0, 0, 0, lc};
Point(2) = {   posobs, 0, 0, lc};
Point(3) = { pospulse, 0, 0, lc};

Point(4) = { lref+pospulse,    0, 0, lc};
Point(5) = {      pospulse, lref, 0, lc};
Point(6) = {-lref+pospulse,    0, 0, lc};
Point(7) = {      pospulse,-lref, 0, lc};

Point(10) = { ldom_a,      0, 0, lc};
Point(11) = {      0, ldom_b, 0, lc};
Point(12) = {-ldom_a,      0, 0, lc};
Point(13) = {      0,-ldom_b, 0, lc};

Point(14) = { ldompml_a,         0, 0, lc};
Point(15) = {         0, ldompml_b, 0, lc};
Point(16) = {-ldompml_a,         0, 0, lc};
Point(17) = {         0,-ldompml_b, 0, lc};

Point(20) = { lobs+posobs,    0, 0, lc};
Point(21) = {      posobs, lobs, 0, lc};
Point(22) = {-lobs+posobs,    0, 0, lc};
Point(23) = {      posobs,-lobs, 0, lc};


// Reference domain

Circle(1) = {4, 3, 5};
Circle(2) = {5, 3, 6};
Circle(3) = {6, 3, 7};
Circle(4) = {7, 3, 4};

// Interest domain

Ellipse(10) = {10, 1, 2, 11};
Ellipse(11) = {11, 1, 2, 12};
Ellipse(12) = {12, 1, 2, 13};
Ellipse(13) = {13, 1, 2, 10};

// Outer boundary of the pml

Ellipse(14) = {14, 1, 2, 15};
Ellipse(15) = {15, 1, 2, 16};
Ellipse(16) = {16, 1, 2, 17};
Ellipse(17) = {17, 1, 2, 14};

// Obstacle

Circle(20) = {20, 2, 21};
Circle(21) = {21, 2, 22};
Circle(22) = {22, 2, 23};
Circle(23) = {23, 2, 20};

Physical Line("BOUNDARY_DOMREF") = {1, 2, 3, 4};
Physical Line("BOUNDARY_DOM")    = {10, 11, 12, 13};
Physical Line("BOUNDARY_DOMPML") = {14, 15, 16, 17};
Physical Line("BOUNDARY_OBS")    = {20, 21, 22, 23};

Line Loop(101) = {1, 2, 3, 4};
Line Loop(102) = {10, 11, 12, 13};
Line Loop(103) = {14, 15, 16, 17};
Line Loop(104) = {20, 21, 22, 23};

Plane Surface(1) = {101, 103};
Plane Surface(2) = {102, 104};
Plane Surface(3) = {102, 103};

Physical Surface("EXT") = {1};   // Exterior of the domain (extension for the reference computation)
Physical Surface("DOM") = {2};   // Interest domain, without the obstacle
Physical Surface("PML") = {3};   // PML

