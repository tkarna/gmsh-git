a = 1500;
b = 800;

lc = 30;

Point(101) = { 0, 0, 0, lc};
Point(102) = { 1, 0, 0, lc};

Point(103) = { a, 0, 0, lc/2};
Point(104) = { 0, b, 0, lc};
Point(105) = {-a, 0, 0, lc/2};
Point(106) = { 0,-b, 0, lc};

Ellipse(201) = {103, 101, 102, 104};
Ellipse(202) = {104, 101, 102, 105};
Ellipse(203) = {105, 101, 102, 106};
Ellipse(204) = {106, 101, 102, 103};

bnd_dom[] = {201, 202, 203, 204};
bnd_dom_ll = newll; Line Loop(bnd_dom_ll) = {bnd_dom[]};
sur_dom = news; Plane Surface(sur_dom) = {bnd_dom_ll};

For k In {0:#bnd_dom[]-1}
    out[] = Extrude{ Line{bnd_dom[k]}; Layers{5, 4*lc};};// Recombine; };
    bnd_pml[] += out[0];
    sur_pml[] += out[1];
EndFor

Physical Line("INTERFACE") = {bnd_dom[]};
Physical Line("BOUNDARY") = {bnd_pml[]};

Physical Surface("DOM") = {sur_dom};
Physical Surface("PML") = {sur_pml[]};
