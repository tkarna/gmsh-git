from gmshpy import *
from dgpy import *
import os
import time
import math


print'---- Load Mesh and Groups'

model = GModel()
model.load('Submarine2D.msh')

dim = 2
order = 1
groups = dgGroupCollection(model, dim, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()


print'---- Build Conservation laws'

law = dgConservationLawWave(dim,'PmlEllipse')
nbfields = 4

cValue = 1500.  # Vitesse du son dans l'eau
rhoValue = 1000.
coef = functionConstant([cValue,rhoValue])
law.setPhysCoef(coef)

print'---- Build Initial conditions'

pospulse_x = -170
pospulse_y = 70
r = 2000
def initialCondition(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)-pospulse_x
    y = xyz.get(i,1)-pospulse_y
    val.set(i,0,math.exp(-(x*x+y*y)/r))
    val.set(i,1,0.)
    val.set(i,2,0.)
    val.set(i,3,0.)
initialConditionPython = functionPython(nbfields,initialCondition,[xyz])

solution = dgDofContainer(groups,nbfields)
solution.setFieldName(0,'p_cpml')
solution.setFieldName(1,'u_cpml')
solution.setFieldName(2,'v_cpml')
solution.setFieldName(3,'q_cpml')
solution.L2Projection(initialConditionPython)
solution.exportMsh('output/Submarine2D_00000',0,0)


print'---- Build Pml'

model.load('Submarine2D_Distance.pos')
model.load('Submarine2D_Direction.pos')
law.useGenericAbsCoef(60.5,cValue,300.*math.sqrt(2),300.)
law.useGenericAbsDir()

law.addPml('PML')
law.addInterfacePml('INTERFACE')

coefprintfunc = law.getAbsCoef('PML')
coefprint = dgDofContainer(groups,2)
coefprint.setFieldName(0,'coef_cpml')
coefprint.setFieldName(1,'coef_cpml')
coefprint.L2Projection(coefprintfunc)
coefprint.exportMsh('output/Submarine2D_CoefPrint',0,0)

coefprintfunc = law.getAbsDir('PML')
coefprint = dgDofContainer(groups,3)
coefprint.setFieldName(0,'dir_cpml')
coefprint.setFieldName(1,'dir_cpml')
coefprint.setFieldName(2,'dir_cpml')
coefprint.L2Projection(coefprintfunc)
coefprint.exportMsh('output/Submarine2D_DirPrint',0,0)


print'---- Build Boundary conditions'

law.addBoundaryCondition('BOUNDARY',     law.newBoundaryWall('BOUNDARY'))
law.addBoundaryCondition('BOUNDARY_OBS', law.newBoundaryWall('BOUNDARY_OBS'))


print'---- LOOP'

dt = 0.01
sys = linearSystemPETScBlockDouble()
dof = dgDofManager.newDGBlock(groups, nbfields, sys)
solver = dgDIRK(law, dof, 2)
solver.getNewton().setVerb(10)
for i in range(1,50):
  t = i*dt
  solver.iterate(solution, dt , t)
  solution.exportMsh("output/Submarine2D_%05i" % (i), t, i)
  print'|ITER|', i, '|MAX|', 50, '|t|', t

Msg.Exit(0)

