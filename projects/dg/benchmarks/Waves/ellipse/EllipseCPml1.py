from gmshpy import *
from dgpy import *
import os
import time
import math


print'---- Load Mesh and Groups'

model = GModel()
model.load('EllipseCPml.msh')

dim = 2
order = 1
groups = dgGroupCollection(model, dim, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()


print'---- Build Conservation laws'

law = dgConservationLawWave(dim,'PmlEllipse')
nbfields = 4

cValue = 1.
rhoValue = 1.
coef = functionConstant([cValue,rhoValue])
law.setPhysCoef(coef)


print'---- Build Initial conditions'

pospulse_x = -800
pospulse_y = 400
r = 25000
def initialCondition(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)-pospulse_x
    y = xyz.get(i,1)-pospulse_y
    val.set(i,0,math.exp(-(x*x+y*y)/r))
    val.set(i,1,0.)
    val.set(i,2,0.)
    val.set(i,3,0.)
initialConditionPython = functionPython(nbfields,initialCondition,[xyz])

solution = dgDofContainer(groups,nbfields)
solution.setFieldName(0,'p_ell')
solution.setFieldName(1,'u_ell')
solution.setFieldName(2,'v_ell')
solution.setFieldName(3,'pN_ell')
solution.L2Projection(initialConditionPython)
solution.exportMsh('output/EllipseEll_00000',0,0)


print'---- Build Pml'

model.load('EllipseCPml_Distance.pos')
model.load('EllipseCPml_Direction.pos')
law.useGenericPmlCoef('Ellipse', 20.5,cValue, 0.,0.,0., 1500.,800.,-1.)
law.useGenericPmlDir('Ellipse')

law.addPml('PML')
law.addInterfacePml('INTERFACE')

coefprintfunc = law.getPmlCoef('PML')
coefprint = dgDofContainer(groups,2)
coefprint.setFieldName(0,'coef_cpml')
coefprint.setFieldName(1,'coef_cpml')
coefprint.L2Projection(coefprintfunc)
coefprint.exportMsh('output/EllipseCPmlCoefPrint',0,0)

coefprintfunc = law.getPmlDir('PML')
coefprint = dgDofContainer(groups,3)
coefprint.setFieldName(0,'dir_cpml')
coefprint.setFieldName(1,'dir_cpml')
coefprint.setFieldName(2,'dir_cpml')
coefprint.L2Projection(coefprintfunc)
coefprint.exportMsh('output/EllipseCPmlDirPrint',0,0)


print'---- Build Boundary conditions'

law.addBoundaryCondition('BOUNDARY', law.newBoundaryWall('BOUNDARY'))


print'---- LOOP'

implicit = True;

if (implicit):
  dt = 25
  sys = linearSystemPETScBlockDouble()
  dof = dgDofManager.newDGBlock(groups, nbfields, sys)
  solver = dgDIRK(law, dof, 2)
  solver.getNewton().setVerb(10)
  for i in range(1,70):
    t = i*dt
    solver.iterate(solution, dt , t)
    solution.exportMsh("output/EllipseEll_%05i" % (i), t, i)
    print'|ITER|', i, '|MAX|', 70, '|t|', t
  
else:
  dt = 0.0005
  nbExport = 1
  rk = dgRungeKutta()
  for i in range(0,300):
    t = i*dt
    norm = rk.iterate44(law, t, dt, solution)
    if (math.isnan(norm)):
      break
    if (i % 5 == 0):
      print'|ITER|', i, '|NORM|', norm, '|DT|', dt, '|t|', t
      solution.exportMsh("output/EllipseEll_%05i" % (nbExport), t, nbExport)
      nbExport = nbExport + 1


Msg.Exit(0)

