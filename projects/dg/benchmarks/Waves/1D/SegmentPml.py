from dgpy import *
import os
import time
import math


print'---- Load Mesh and Build Groups'

model = GModel()
model.load('SegmentPml.msh')

dim = 1
order = 1
groups = dgGroupCollection(model, dim, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()


print'---- Build Conservation Law with Coefficients'

cValue = 1.293
rhoValue = 340.
coef = functionConstant([cValue,rhoValue])

law = dgConservationLawWave(1,'BasicLayer')
law.setPhysCoef(coef)

nbFields = law.getNbFields()


print'---- Build Initial Conditions'

def initialCondition(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)+0.5
    val.set(i,0,math.exp(-40*x*x))
    val.set(i,1,math.exp(-40*x*x)/(cValue*rhoValue))
initialConditionPython = functionPython(2,initialCondition,[xyz])

sol = dgDofContainer(groups,2)
sol.setFieldName(0,'p_pml')
sol.setFieldName(1,'u_pml')
sol.L2Projection(initialConditionPython)
sol.exportMsh('output/Segment_Pml_00000',0,0)


print'---- Buid PML'

lpml = 0.1
def pmlCoefDef(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    sigmaPml = cValue/lpml * (x)/(lpml-x)
    val.set(i,0,sigmaPml)

pmlCoef = functionPython(1,pmlCoefDef,[xyz])
pmlDir = functionConstant([1.,0.,0.])

law.setPmlCoef(pmlCoef)
law.setPmlDir(pmlDir)

law.addPml('PML')


print'---- Build Boundary Conditions'

law.addBoundaryCondition('BOUNDARY_RIGHT',law.newBoundaryWall('BOUNDARY_RIGHT'))
law.addBoundaryCondition('BOUNDARY_LEFT', law.newBoundaryWall('BOUNDARY_LEFT'))


print'---- LOOP'

implicit = False
nbExport = 1

sys = linearSystemPETScBlockDouble()
sys.setParameter("petscOptions", "-pc_type ilu")
dof = dgDofManager.newDGBlock(groups, nbFields, sys)
#sys = linearSystemPETScDouble()
#dof = dgDofManager.newCG(groups, nbFields, sys)

if (implicit) :
  dt = 0.005
  solver = dgCrankNicholson(sol, law, dof, 0.)
  solver.getNewton().setVerb(10)
  solver.setComputeExplicitTerm(True)
  nbExport = 1
  for i in range(1,1000):
    t = i*dt
    solver.iterate(t)
    if (i % 10 == 0): 
      sol.exportMsh("output/Segment_Pml_%05i" % (nbExport), t, nbExport)
      nbExport = nbExport + 1
else :
  dt = 0.0001
  solver = dgERK(law, dof, DG_ERK_44)
  nbExport = 1
  for i in range(0,10000):
    t = i*dt
    solver.iterate(sol, dt, t)
    if (i % 100 == 0):
      erg = law.getTotalEnergy(groups, sol, 'DOMAIN')
      print '|ITER|', i, '|DT|', dt, '|t|', t, '|Erg|', erg
      sol.exportMsh("output/Segment_Pml_%05i" % (nbExport), t, nbExport)
      nbExport = nbExport + 1


Msg.Exit(0)

