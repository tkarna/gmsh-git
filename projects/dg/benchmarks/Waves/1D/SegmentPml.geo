
lc = 0.01;

Point(1) = {-1.,  0, 0, lc};
Point(2) = { 0.,  0, 0, lc};
Point(3) = { 0.1, 0, 0, lc};

Line(1) = {1, 2};
Line(2) = {2, 3};

Physical Point("BOUNDARY_LEFT") = {1};
Physical Point("INTERFACE") = {2};
Physical Point("BOUNDARY_RIGHT") = {3};

Physical Line("DOMAIN") = {1};
Physical Line("PML") = {2};

