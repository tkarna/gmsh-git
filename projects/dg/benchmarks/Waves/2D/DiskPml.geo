
ldom = 50;
lpml = 7;
lc = 3;

Point(1) = {0.,0.,0.,lc};

Point(2) = {ldom,0.,0.,lc};
Point(3) = {0,ldom,0.,lc};
Point(4) = {-ldom,0.,0.,lc};
Point(5) = {0,-ldom,0.,lc};

Point(6) = {ldom+lpml,0.,0.,lc};
Point(7) = {0,ldom+lpml,0.,lc};
Point(8) = {-ldom-lpml,0.,0.,lc};
Point(9) = {0,-ldom-lpml,0.,lc};

Circle(1) = {2,1,3};
Circle(2) = {3,1,4};
Circle(3) = {4,1,5};
Circle(4) = {5,1,2};

Circle(5) = {6,1,7};
Circle(6) = {7,1,8};
Circle(7) = {8,1,9};
Circle(8) = {9,1,6};

Line Loop(1) = {1, 2, 3, 4};
Line Loop(2) = {-8, -7, -6, -5};

Plane Surface(1) = {1};
Plane Surface(2) = {1, 2};

Physical Line("INTERFACE") = {1, 2, 3, 4};
Physical Line("BOUNDARY") = {-8, -7, -6, -5};

Physical Surface("DOMAIN") = {1};
Physical Surface("PML") = {2};

