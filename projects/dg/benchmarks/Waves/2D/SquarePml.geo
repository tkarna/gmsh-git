
ldom = 50;
pospml = 25;
lpml = 10;
lc = 5;

// lc div par 2 ==> erreur doit �tre divis� par 4

Point(1) = {-ldom, -ldom       , 0, lc};
Point(2) = {-ldom, -pospml-lpml, 0, lc};
Point(3) = {-ldom, -pospml     , 0, lc};
Point(4) = {-ldom,  ldom       , 0, lc};
Point(5) = { ldom, -ldom       , 0, lc};
Point(6) = { ldom, -pospml-lpml, 0, lc};
Point(7) = { ldom, -pospml     , 0, lc};
Point(8) = { ldom,  ldom       , 0, lc};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {1, 5};
Line(5) = {2, 6};
Line(6) = {3, 7};
Line(7) = {4, 8};
Line(8) = {5, 6};
Line(9) = {6, 7};
Line(10) = {7, 8};

Line Loop(11) = {4, 8, -5, -1};
Line Loop(12) = {5, 9, -6, -2};
Line Loop(13) = {6, 10, -7, -3};

Plane Surface(1) = {11};
Plane Surface(2) = {12};
Plane Surface(3) = {13};

Physical Line("BOUNDARY") = {7, 3, 1, 4, 8, 10};
Physical Line("BOUNDARY_PML") = {2, 9};
Physical Line("INTERFACE_PMLDOM") = {6};
Physical Line("INTERFACE_PMLEXT") = {5};

Physical Surface("EXTENSION") = {1};
Physical Surface("DOMAIN") = {3};
Physical Surface("PML") = {2};

