from gmshpy import *
from dgpy import *
import os
import time
import math


ldom = 50
lpml = 20


print'---- Load Mesh and Build Groups'

model = GModel()
model.load('SquarePmlExtForcing.msh')

dim = 2
order = 1
groups = dgGroupCollection(model, dim, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()


print'---- Build Conservation law with coefficients'

cValue = 1.
rhoValue = 1.
coef = functionConstant([cValue,rhoValue])

law = dgConservationLawWave(dim,'PmlStraight')
law.setPhysCoef(coef)


print'---- Build Initial conditions'

t = 0
def initialConditionDef(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    val.set(i,0,math.exp(-(x*x+y*y)/50))
    val.set(i,1,0.)
    val.set(i,2,0.)
    val.set(i,3,0.)
initialConditionPython = functionPython(4,initialConditionDef,[xyz])

solution = dgDofContainer(groups,4)
solution.setFieldName(0,'p')
solution.setFieldName(1,'u')
solution.setFieldName(2,'v')
solution.setFieldName(3,'q')
solution.L2Projection(initialConditionPython)
solution.exportMsh('output/SquarePmlExtForcing_Test_00000',0,0)


print'---- Buid PML'

def absCoefDef(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    xloc = math.fabs(x)-ldom
    sigmaPml = cValue/(lpml*1.01) * (xloc)/((lpml*1.01)-xloc)
    val.set(i,0,sigmaPml)

t = 0
def incidentFieldsDef(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    val.set(i,0, math.exp(-((x+cValue*t-2*ldom)*(x+cValue*t-2*ldom))/100))
    val.set(i,1,-math.exp(-((x+cValue*t-2*ldom)*(x+cValue*t-2*ldom))/100))
    val.set(i,2,0.)

absCoefPython = functionPython(1,absCoefDef,[xyz])
absDirPython = functionConstantByTag({"DOMAIN" :[0., 0., 0.],
                                      "PML"    :[1., 0., 0.]})
incidentFieldsPython = functionPython(3,incidentFieldsDef,[xyz])

incidentFieldsDof = dgDofContainer(groups,3)
incidentFieldsDof.L2Projection(incidentFieldsPython)
#incidentFieldsDof.interpolate(incidentFieldsPython)

law.setAbsCoef(absCoefPython)
law.setAbsDir(absDirPython)
#law.addPml('PML')
#law.addPml('PML',incidentFieldsPython)
#law.addInterfacePml('INTERFACE',incidentFieldsPython)
law.addPml('PML',incidentFieldsDof.getFunction())
law.addInterfacePml('INTERFACE',incidentFieldsDof.getFunction())
#law.addPml('PML',incidentFieldsDof.getFunction(),incidentFieldsDof.getFunctionGradient())

#solutioncoef = dgDofContainer(groups,1)
#solutioncoef.setFieldName(0,'absCoef')
#solutioncoef.L2Projection(absCoefPython)
#solutioncoef.exportMsh('output/AbsCoef_00000',0,0)


print'---- Build Boundary conditions'

t=0
def incidentU(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    val.set(i,0,-math.exp(-((x+cValue*t-2*ldom)*(x+cValue*t-2*ldom))/100))
    val.set(i,1,0.)
incidentUPython = functionPython(2, incidentU, [xyz])

law.addBoundaryCondition('BOUNDARY',     law.newBoundaryWall('BOUNDARY'))
#law.addBoundaryCondition('BOUNDARY_PML',  law.newBoundaryWall('BOUNDARY_PML'))
law.addBoundaryCondition('BOUNDARY_PML', law.newBoundaryDirichletOnU('BOUNDARY_PML', incidentUPython))


print'---- LOOP'

implicit = True

sys = linearSystemPETScBlockDouble()
dof = dgDofManager.newDGBlock(groups, 4, sys)

if (implicit):
  dt = 1
  solver = dgCrankNicholson(solution, law, dof, 0.)
  solver.getNewton().setVerb(10)
  solver.setComputeExplicitTerm(True)
  for i in range(1,100):
    t = i*dt
    incidentFieldsDof.L2Projection(incidentFieldsPython)
    #incidentFieldsDof.interpolate(incidentFieldsPython)
    solver.iterate(t)
    print'|ITER|', i, '|DT|', dt, '|t|', t
    if (i % 10 == 0):
      solution.exportMsh("output/SquarePmlExtForcing_Test_%05i" % (i), t, i)
else:
  dt = 0.01
  solver = dgERK(law, dof, DG_ERK_44)
  nbExport = 1
  for i in range(0,400):
    t = i*dt
    solver.iterate(solution, dt, t)
    if (i % 40 == 0):
      print'|ITER|', i, '|DT|', dt, '|t|', t
      solution.exportMsh("output/SquarePmlExtForcing_%05i" % (nbExport), t, nbExport)
      nbExport  = nbExport + 1


Msg.Exit(0)

