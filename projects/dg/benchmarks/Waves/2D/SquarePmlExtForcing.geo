
ldom = 50;
lpml = 20;
lc = 5;

// lc div par 2 ==> erreur doit �tre divis� par 4

Point(1) = {-ldom,      ldom, 0, lc};
Point(2) = { ldom,      ldom, 0, lc};
Point(3) = { ldom+lpml, ldom, 0, lc};
Point(4) = { ldom+lpml,-ldom, 0, lc};
Point(5) = { ldom,     -ldom, 0, lc};
Point(6) = {-ldom,     -ldom, 0, lc};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 5};
Line(5) = {5, 6};
Line(6) = {6, 1};
Line(7) = {2, 5};

Line Loop(11) = {1, 7, 5, 6};
Line Loop(12) = {-7, 2, 3, 4};

Plane Surface(1) = {11};
Plane Surface(2) = {12};

Physical Line("BOUNDARY") = {1, 2, 4, 5, 6};
Physical Line("BOUNDARY_PML") = {3};
Physical Line("INTERFACE") = {7};

Physical Surface("DOMAIN") = {1};
Physical Surface("PML") = {2};

