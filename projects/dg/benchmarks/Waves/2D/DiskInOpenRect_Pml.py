from gmshpy import *
from dgpy import *
import os
import time
import math


Ltickness = 8.
Lheight   = 30.
Lpmlleft  = 2.5
Lpmlright = 2.5


print'Mesh, Geometry & Groups'

model = GModel()
model.load('DiskInOpenRect.msh')

dim = 2
order = 1
groups = dgGroupCollection(model, dim, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()


print'Conservation law'

cValue        = 340.
rhoValue      = 1.293
sigmaValue    = 0.
cValue_mat    = 500.
rhoValue_mat  = 2*1.293

coef = functionConstant([cValue, rhoValue, sigmaValue])
#coef = functionConstantByTag({"PML_LEFT"  :[cValue,     rhoValue,     sigmaValue],
#                              "PML_RIGHT" :[cValue,     rhoValue,     sigmaValue],
#                              "MEDIUM"    :[cValue_mat, rhoValue_mat, sigmaValue]})

law = dgConservationLawWave(2,'PmlStraight')
law.setPhysCoef(coef)
#law.useDissipationOnP()


print'Initial solutions'

r=1.
x0 = -Ltickness

def initialCondition(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    value = math.exp(-((x-x0)*(x-x0))/(r*r))
    val.set(i,0,value)
    val.set(i,1,value/(cValue*rhoValue))
    val.set(i,2,0.)
    val.set(i,3,0.)
initialConditionPython = functionPython(4,initialCondition,[xyz])

sol = dgDofContainer(groups,4)
sol.setFieldName(0,'p')
sol.setFieldName(1,'u')
sol.setFieldName(2,'v')
sol.setFieldName(3,'q')
sol.L2Projection(initialConditionPython)
sol.exportMsh('output/DiskInOpenRect_Pml_00000',0,0)


print'External forcing'

t=0.
def extFields(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    value = math.exp(-((x-cValue*t-x0)*(x-cValue*t-x0))/(r*r))
    val.set(i,0,value)
    val.set(i,1,value/(cValue*rhoValue))
    val.set(i,2,0.)
extFieldsPython = functionPython(3,extFields,[xyz])

def extFieldsU(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    value = math.exp(-((x-cValue*t-x0)*(x-cValue*t-x0))/(r*r))
    val.set(i,0,value/(cValue*rhoValue))
    val.set(i,1,0.)
extFieldsUPython = functionPython(2,extFieldsU,[xyz])


print'Build PML'

def absCoefDef(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    if (x>0):
      xloc = math.fabs(x)-Ltickness/2.
      sigmaPml = cValue/(Lpmlright) * (xloc)/((Lpmlright*1.01)-xloc)
    else:
      xloc = math.fabs(x)-Ltickness/2.
      sigmaPml = cValue/(Lpmlleft) * (xloc)/((Lpmlleft*1.01)-xloc)
    val.set(i,0,sigmaPml)
absCoef = functionPython(1,absCoefDef,[xyz])

absDir = functionConstantByTag({"PML_LEFT"  :[-1.,0.,0.],
                                "PML_RIGHT" :[ 1.,0.,0.],
                                "MEDIUM"    :[ 0.,0.,0.]})

law.setAbsCoef(absCoef)
law.setAbsDir(absDir)

law.addPml('PML_LEFT',extFieldsPython)
law.addInterfacePml('INTERFACE_LEFT',extFieldsPython)
law.addPml('PML_RIGHT')
law.addInterfacePml('INTERFACE_RIGHT')

absCoefSol = dgDofContainer(groups,1)
absCoefSol.interpolate(absCoef)
absCoefSol.exportMsh('output/DiskInOpenRect_Pml_AbsCoef',0,0)


print'Boundary conditions'

law.addBoundaryCondition('BOUNDARY0', law.newBoundaryWall('BOUNDARY0'))
law.addBoundaryCondition('BOUNDARY1', law.newBoundaryWall('BOUNDARY1'))
law.addBoundaryCondition('BOUNDARY2', law.newBoundaryWall('BOUNDARY2'))
law.addBoundaryCondition('BOUNDARY3', law.newBoundaryWall('BOUNDARY3'))
law.addBoundaryCondition('BOUNDARY4', law.newBoundaryWall('BOUNDARY4'))
law.addBoundaryCondition('BOUNDARY5', law.newBoundaryWall('BOUNDARY5'))
law.addBoundaryCondition('BOUNDARY6', law.newBoundaryWall('BOUNDARY6'))
law.addBoundaryCondition('BOUNDARY7', law.newBoundaryDirichletOnU('BOUNDARY7', extFieldsUPython))
law.addBoundaryCondition('BOUNDARY_DISK', law.newBoundaryWall('BOUNDARY_DISK'))



print'Loop'

implicit = True;

if (implicit):
  dt = 0.0005
  sys = linearSystemPETScBlockDouble()
  dof = dgDofManager.newDGBlock(groups, 4, sys)
  solver=dgDIRK(law,dof,2)
  for i in range(1,115):
    t = i*dt
    solver.iterate(sol,dt,t)
    print'|ITER|', i, '|DT|', dt, '|t|', t
    sol.exportMsh("output/DiskInOpenRect_Pml_%05i" % (i), t, i)
else:
  dt = 0.00001
  nbExport = 1
  solver = dgERK(law, None, DG_ERK_44)
  for i in range(0,50000):
    t = i*dt
    solver.iterate(sol, dt, t);
    norm = sol.norm()
    if (math.isnan(norm)):
      print 'ERROR : NAN'
      break
    if (i % 1 == 0):
      print'|ITER|', i, '|NORM|', norm, '|DT|', dt, '|t|', t
      sol.exportMsh("output/DiskInOpenRect_Pml_%05i" % (nbExport), t, nbExport)
      nbExport  = nbExport + 1
      


Msg.Exit(0)

