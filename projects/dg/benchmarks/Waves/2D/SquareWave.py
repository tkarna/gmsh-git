from gmshpy import *
from dgpy import *
import os
import time
import math


implicit = True
continuous = False


print'---- Load Mesh & Build Groups'

model = GModel()
model.load('SquareWave.msh')

dim = 2
order = 1
groups = dgGroupCollection(model, dim, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()


print'---- Load Conservation law'

law = dgConservationLawWave(dim)

cValue = 340.
rhoValue = 1.293
sigmaValue = 0.
coef = functionConstant([cValue,rhoValue,sigmaValue])
law.setPhysCoef('',coef)

if (continuous):
  law.useCG();
  solutionPrev = dgDofContainer(groups,3)
  law.setPreviousSolution(solutionPrev.getFunction())
  kappaNum = 100.
  coefNum = functionConstant([kappaNum])
  law.setNumCoef(coefNum)


print'---- Load Initial conditions'

def initialCondition(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    val.set(i,0,math.exp(-(x*x+y*y)/10))
    val.set(i,1,0.0)
    val.set(i,2,0.0)
initialConditionPython = functionPython(3,initialCondition,[xyz])

solution = dgDofContainer(groups,3)
solution.setFieldName(0,'p')
solution.setFieldName(1,'u')
solution.setFieldName(2,'v')
solution.L2Projection(initialConditionPython)
solution.exportMsh('output/SquareWave_00000',0,0)


print'---- Load Boundary conditions'

law.addBoundaryCondition('Boundaries',law.newBoundaryWall('Boundaries'))


print'---- LOOP'

nbExport = 1
if (implicit):
  
  if (continuous):
    sys = linearSystemPETScDouble()
    dof = dgDofManager.newCG(groups, 3, sys)
  else:
    sys = linearSystemPETScBlockDouble()
    dof = dgDofManager.newDGBlock(groups, 3, sys)
  dt = 0.001
  #solver = dgCrankNicholson(solution, law, dof, 0.)
  #solver.getNewton().setVerb(10)
  #solver.setComputeExplicitTerm(True)
  solver = dgDIRK(law, dof, 2)
  solver.getNewton().setVerb(10)
  for i in range(1,100):
    if (continuous):
      solutionPrev.copy(solution)
    t = i*dt
    #solver.iterate(t)
    solver.iterate(solution, dt , t)
    solution.exportMsh("output/SquareWave_%05d" % (i), t, i)
  
else:
  
  if (continuous):
    sys = linearSystemPETScDouble()
    dof = dgDofManager.newCG(groups, 3, sys)
  else:
    sys = linearSystemPETScBlockDouble()
    sys.setParameter("petscOptions", "-pc_type lu")
    dof = dgDofManager.newDGBlock(groups, 3, sys)
  
  solver = dgERK(law, dof, DG_ERK_44)
  dt = 0.0001
  for i in range(0,1500):
    t = i*dt
    norm = solver.iterate(solution, dt, t)
    if (i % 50 == 0):
      print('|ITER|',i,'IMPLICIT',implicit)
      solution.exportMsh("output/SquareWave_-%05d" % (nbExport), t, nbExport)
      nbExport = nbExport + 1


Msg.Exit(0)

