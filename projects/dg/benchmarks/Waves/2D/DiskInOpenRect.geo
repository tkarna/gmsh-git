
Ltickness = 8.;
Lheight   = 30.;
Lpmlleft  = 2.5;
Lpmlright = 2.5;
r         = 1.5;

lc = 0.25;
lc2 = 0.25;

icircle = 0;
cx = 0;
cy = 0;

center=newp; Point(center)={cx,cy,0};
p1=newp; Point(p1)={cx-r,cy,0,lc};
p2=newp; Point(p2)={cx,cy+r,0,lc};
p3=newp; Point(p3)={cx+r,cy,0,lc};
p4=newp; Point(p4)={cx,cy-r,0,lc};
s1=newl; Circle(s1)={p1,center,p2};
s2=newl; Circle(s2)={p2,center,p3};
s3=newl; Circle(s3)={p3,center,p4};
s4=newl; Circle(s4)={p4,center,p1};
circlloop=newll; Line Loop(circlloop)={s1,s2,s3,s4};
circsloop=news; Plane Surface(circsloop)={circlloop};

xboudary[0]=-Ltickness/2-Lpmlleft;
xboudary[1]=-Ltickness/2;
xboudary[2]= Ltickness/2;
xboudary[3]= Ltickness/2+Lpmlright;

p[0]=newp; Point(p[0])= {xboudary[0], Lheight/2, 0, lc2};
p[1]=newp; Point(p[1])= {xboudary[1], Lheight/2, 0, lc};
p[2]=newp; Point(p[2])= {xboudary[2], Lheight/2, 0, lc};
p[3]=newp; Point(p[3])= {xboudary[3], Lheight/2, 0, lc2};
p[4]=newp; Point(p[4])= {xboudary[3],-Lheight/2, 0, lc2};
p[5]=newp; Point(p[5])= {xboudary[2],-Lheight/2, 0, lc};
p[6]=newp; Point(p[6])= {xboudary[1],-Lheight/2, 0, lc};
p[7]=newp; Point(p[7])= {xboudary[0],-Lheight/2, 0, lc2};

For i In {0:7}
  s=newl; Line(s)={p[i], p[((i+1)%8)]};
  lboundary[i]=s;
EndFor

s=newl; Line(s)={p[1], p[6]};
linterface[0] = s;
s=newl; Line(s)={p[2], p[5]};
linterface[1] = s;

zone[0]=news; Line Loop(zone[0])={lboundary[0],linterface[0],lboundary[6],lboundary[7]};
Plane Surface(zone[0])={zone[0]};
zone[1]=news; Line Loop(zone[1])={lboundary[1],linterface[1],lboundary[5],-linterface[0]};
Plane Surface(zone[1])={zone[1],circlloop[]};
zone[2]=news; Line Loop(zone[2])={lboundary[2],lboundary[3],lboundary[4],-linterface[1]};
Plane Surface(zone[2])={zone[2]};

Physical Line("BOUNDARY0") = {lboundary[0]};
Physical Line("BOUNDARY1") = {lboundary[1]};
Physical Line("BOUNDARY2") = {lboundary[2]};
Physical Line("BOUNDARY3") = {lboundary[3]};
Physical Line("BOUNDARY4") = {lboundary[4]};
Physical Line("BOUNDARY5") = {lboundary[5]};
Physical Line("BOUNDARY6") = {lboundary[6]};
Physical Line("BOUNDARY7") = {lboundary[7]};
Physical Line("INTERFACE_LEFT")  = {linterface[0]};
Physical Line("INTERFACE_RIGHT") = {linterface[1]};
Physical Line("BOUNDARY_DISK")   = {s1,s2,s3,s4};

Physical Surface ("PML_LEFT")  = {zone[0]};
Physical Surface ("MEDIUM")    = {zone[1]};
Physical Surface ("PML_RIGHT") = {zone[2]};

