from gmshpy import *
from dgpy import *
import os
import time
import math


simuRef = True
simuPml = True


print'---- Load Mesh and Build Groups'

model = GModel()
model.load('SquarePml.msh')

dim = 2
order = 1
groups = dgGroupCollection(model, dim, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()


print'---- Build Conservation law with coefficients'

cValue = 340.
rhoValue = 1.293
sigmaValue = 0.
coef = functionConstant([cValue,rhoValue,sigmaValue])

if (simuRef):
  lawref = dgConservationLawWave(dim)
  lawref.setPhysCoef(coef)
  
if (simuPml):
  lawpml = dgConservationLawWave(dim,'PmlStraight')
  lawpml.setPhysCoef(coef)


print'---- Build Initial conditions'

def initialCondition(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    val.set(i,0,math.exp(-(x*x+y*y)/10))
    val.set(i,1,0.)
    val.set(i,2,0.)
    val.set(i,3,0.)
initialConditionPython = functionPython(4,initialCondition,[xyz])

if (simuRef):
  solutionref = dgDofContainer(groups,3)
  solutionref.setFieldName(0,'p_ref')
  solutionref.setFieldName(1,'u_ref')
  solutionref.setFieldName(2,'v_ref')
  solutionref.L2Projection(initialConditionPython)
  solutionref.exportMsh('output/Square_Ref_00000',0,0)

if (simuPml):
  solutionpml = dgDofContainer(groups,5)
  solutionpml.setFieldName(0,'p_pml')
  solutionpml.setFieldName(1,'u_pml')
  solutionpml.setFieldName(2,'v_pml')
  solutionpml.setFieldName(3,'qp_pml')
  solutionpml.setFieldName(4,'qu_pml')
  solutionpml.L2Projection(initialConditionPython)
  solutionpml.exportMsh('output/Square_Pml_00000',0,0)


print'---- Buid PML'

lpml = 10
pospml = 25

def absCoefDef(val,xyz):
  for i in range(0,xyz.size1()):
    y = xyz.get(i,1)
    yloc = math.fabs(y)-pospml
    sigmaPml = cValue/(lpml*1.01) * (yloc)/((lpml*1.01)-yloc)
    val.set(i,0,sigmaPml)

absCoef = functionPython(1,absCoefDef,[xyz])
absDir = functionConstant([0.,1.])

if (simuPml):
  lawpml.setAbsCoef(absCoef)
  lawpml.setAbsDir(absDir)
  lawpml.addPml('PML')
  lawpml.addInterfacePml('INTERFACE_PMLDOM')


print'---- Build Boundary conditions'

if (simuRef):
  lawref.addBoundaryCondition('BOUNDARY', lawref.newBoundaryWall('BOUNDARY'))
  lawref.addBoundaryCondition('BOUNDARY_PML', lawref.newBoundaryWall('BOUNDARY_PML'))

if (simuPml):
  lawpml.addInterfaceWall('INTERFACE_PMLEXT')
  lawpml.addBoundaryCondition('BOUNDARY', lawpml.newBoundaryWall('BOUNDARY'))
  lawpml.addBoundaryCondition('BOUNDARY_PML', lawpml.newBoundaryWall('BOUNDARY_PML'))


print'---- LOOP'

implicit = False

if (simuRef):
  sysref = linearSystemPETScBlockDouble()
  dofref = dgDofManager.newDGBlock(groups, 3, sysref)

if (simuPml):
  syspml = linearSystemPETScBlockDouble()
  dofpml = dgDofManager.newDGBlock(groups, 4, syspml)


if (implicit):
  dt = 0.005
  if (simuRef):
    solverref = dgCrankNicholson(solutionref, lawref, dofref, 0.)
    solverref.getNewton().setVerb(10)
    solverref.setComputeExplicitTerm(True)
  if (simuPml):
    solverpml = dgCrankNicholson(solutionpml, lawpml, dofpml, 0.)
    solverpml.getNewton().setVerb(10)
    solverpml.setComputeExplicitTerm(True)
  for i in range(1,40):
    t = i*dt
    if (simuRef):
      solverref.iterate(t)
    if (simuPml):
      solverpml.iterate(t)
    print'|ITER|', i, '|DT|', dt, '|t|', t
    if (simuRef):
      solutionref.exportMsh("output/Square_Ref_%05i" % (i), t, i)
    if (simuPml):
      solutionpml.exportMsh("output/Square_Pml_%05i" % (i), t, i)
else:
  dt = 0.0005
  if (simuRef):
    solverref = dgERK(lawref, dofref, DG_ERK_44)
  if (simuPml):
    solverpml = dgERK(lawpml, dofpml, DG_ERK_44)
  nbExport = 1
  for i in range(0,400):
    t = i*dt
    if (simuRef):
      solverref.iterate(solutionref, dt, t)
    if (simuPml):
      solverpml.iterate(solutionpml, dt, t)
    if (i % 40 == 0):
      print'|ITER|', i, '|DT|', dt, '|t|', t
      if (simuRef):
        solutionref.exportMsh("output/Square_Ref_%05i" % (nbExport), t, nbExport)
      if (simuPml):
        solutionpml.exportMsh("output/Square_Pml_%05i" % (nbExport), t, nbExport)
      nbExport  = nbExport + 1


Msg.Exit(0)

