Merge "Submarine.stp";

Mesh.CharacteristicLengthFactor = 0.1;

Compound Line(34) = {22, 24, 25};
submarineLine[] = {15, 21, 17, 26, 11, 20, 13, 27, 29, 8, 19, 10, 34};
Physical Line(1) = submarineLine[];

Compound Surface(13) = {10};
submarineSurf[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 13};
Physical Surface(2) = submarineSurf[];

