from gmshpy import *
from dgpy import *
import os
import time
import math


print'---- Load Mesh and Groups'

model = GModel()
model.load('Submarine3D.msh')

dim = 3
order = 1
groups = dgGroupCollection(model, dim, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()


print'---- Build Initial conditions'

def initialCondition(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)+300
    y = xyz.get(i,1)
    z = xyz.get(i,2)-450
    val.set(i,0,math.exp(-(x*x+y*y+z*z)/100000))
    val.set(i,1,0.)
    val.set(i,2,0.)
    val.set(i,3,0.)
    val.set(i,4,math.exp(-(x*x+y*y+z*z)/100000))
nbfields = 5
initialConditionPython = functionPython(nbfields,initialCondition,[xyz])
solution = dgDofContainer(groups,nbfields)
solution.setFieldName(0,'p')
solution.setFieldName(1,'u')
solution.setFieldName(2,'v')
solution.setFieldName(3,'w')
solution.setFieldName(4,'q')
solution.L2Projection(initialConditionPython)
solution.exportMsh('output/Submarine3D_00000',0,0)


print'---- Build Conservation law'

law = dgConservationLawWave(dim,1)
cValue = 1500.    # Vitesse du son dans l'eau
rhoValue = 1000.
sigmaValue = 0.
coef = functionConstant([cValue,rhoValue,sigmaValue])
law.setPhysCoef(coef)


print'---- Build PML'

CCode = """
#include "fullMatrix.h"
#include "dgDataCacheMap.h"

extern "C" {

double cValue = 1500.;

double pmlscale = 1.25;
double ldom_a = 3300;
double ldom_b = 1200;

double posellips_x = 2150;
double posellips_y = 0;
double posellips_z = 50;

void absCoefDef (dgDataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< sol.size1(); i++) {
    
    // Coordinates
    double x = xyz(i,0)-posellips_x;
    double y = xyz(i,1)-posellips_y;
    double z = xyz(i,2)-posellips_z;
    
    // Scale of the current ellipsoid
    double r = sqrt((x*x)/(ldom_a*ldom_a)+(y*y)/(ldom_b*ldom_b)+(z*z)/(ldom_b*ldom_b));
    
    // Thickness of the layer at the current point
    double lpml = (pmlscale-1) * sqrt(x*x+y*y+z*z)/r;
    
    // PML-coordinate (from 0 at the interface, to 1 at the outer border)
    double rloc = (r-1)/(pmlscale-1);
    
    // Absorption coefficent
    double sigmaPml = 0.;
    if (rloc>0)
      sigmaPml = cValue/(lpml*1.0001) * (rloc)/(1.0001-rloc);
    
    sol.set(i,0,sigmaPml);
    sol.set(i,1,lpml);
    sol.set(i,2,rloc);
  }
}

void absDirDef (dgDataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< sol.size1(); i++) {
    
    // Coordinates
    double x = xyz(i,0)-posellips_x;
    double y = xyz(i,1)-posellips_y;
    double z = xyz(i,2)-posellips_z;
    
    // Normal vector to the ellipse
    double normale_x = ldom_b*x/ldom_a;
    double normale_y = ldom_a*y/ldom_b;
    double normale_z = ldom_a*z/ldom_b;
    
    // Norm of the normal vector
    double norm_normale = sqrt(normale_x*normale_x+normale_y*normale_y+normale_z*normale_z);
    
    // Absorption direction
    double vecx = normale_x/norm_normale;
    double vecy = normale_y/norm_normale;
    double vecz = normale_z/norm_normale;
    
    sol.set(i,0,vecx);
    sol.set(i,1,vecy);
    sol.set(i,2,vecz);
  }
}

}
"""

tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CCode, tmpLib);
absCoef    = functionC(tmpLib,"absCoefDef",3,[xyz])
absDir     = functionC(tmpLib,"absDirDef",3,[xyz])

law.setAbsCoef(absCoef)
law.setAbsDir(absDir)

law.addPml('PML')
law.addInterfacePml('INTERFACE')


print'---- Load Boundary conditions'

law.addBoundaryCondition('BND_SUBMARINE',  law.newBoundaryWall('BND_SUBMARINE'))
law.addBoundaryCondition('BND_EXTERIEUR',  law.newBoundaryWall('BND_EXTERIEUR'))
law.addBoundaryCondition('BND_TRANCHE_DOM',law.newBoundaryWall('BND_TRANCHE_DOM'))
law.addBoundaryCondition('BND_TRANCHE_PML',law.newBoundaryWall('BND_TRANCHE_PML'))


print'---- LOOP'

sys = linearSystemPETScBlockDouble()
dof = dgDofManager.newDGBlock(groups, nbfields, sys)
dt = 0.1
solver = dgDIRK(law, dof, 2)
solver.getNewton().setVerb(10)
for i in range(1,31):
  t = i*dt
  solver.iterate(solution, dt , t)
  solution.exportMsh("output/Submarine3D_%05d" % (i), t, i)
  print'|ITER|', i, '/ 30 ', '|DT|', dt, '|t|', t


Msg.Exit(0)

