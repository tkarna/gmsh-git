

Mesh.Algorithm = 6;
Mesh.Algorithm3D = 4;

ldom_a = 3300;    // Major radius (x)
ldom_b = 1200;    // Minor radius (y,z)

posellips_x = 2150;
posellips_y = 0;
posellips_z = 50;

pospulse_x = -350;
pospulse_y = 0;
pospulse_z = 500;

lc = 100;
lcPulse = 100;



//-------------
// Submarine
//-------------

Merge "Submarine.msh";
CreateTopology;

// Border of the surface of the submarine
submarineBorder = newll;
submarineLine[] = {15, 21, 17, 26, 11, 20, 13, 27, 29, 8, 19, 10, 34};
Line Loop(submarineBorder) = submarineLine[];

// Surface of the submarine
submarineSurf[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 13};


Physical Surface("BND_SUBMARINE") = {submarineSurf[]};
Physical Surface("BND_SUBMARINE2") = {submarineSurf[]};



//------------
// Ellipsoid
//------------

Point(100) = {posellips_x        , posellips_y , posellips_z        , lc  };
Point(101) = {posellips_x-ldom_a , posellips_y , posellips_z        , lc/2};
Point(102) = {posellips_x+ldom_a , posellips_y , posellips_z        , lc/2};
Point(103) = {posellips_x        , posellips_y , posellips_z+ldom_b , lc  };

Ellipse(200) = {101, 100, 102, 103};
Ellipse(201) = {103, 100, 102, 102};

out[] = Extrude { {1,0,0}, {posellips_x, posellips_y, posellips_z}, -Pi } { Line{200}; Line{201}; };

Line Loop(220) = {200, 201, -205, -202};

Physical Surface("INTERFACE") = {204, 207};



//--------------------------
// Section
//--------------------------

sectionSurfaceDomain = news;
Plane Surface(sectionSurfaceDomain) = {submarineBorder, 220};
Physical Surface("BND_TRANCHE_DOM") = {sectionSurfaceDomain};



//-------------
// Volume
//-------------

surfDomain = newsl;
Surface Loop(surfDomain) = {sectionSurfaceDomain, 204, 207, submarineSurf[]};
//Surface Loop(surfDomain) = {sectionSurfaceDomain, ellsurf[]};
volDomain = newv;
Volume(volDomain) = {surfDomain};
Physical Volume("DOMAIN") = {volDomain};



//-------------
// Pulse
//-------------

pulseCenter = newp;
Point(pulseCenter) = {pospulse_x, pospulse_y, pospulse_z, lcPulse};
Physical Point("PULSE") = {pulseCenter};



//-------------
// Layer (Extrusion)
//-------------
// OLD VERSION : out[] = Extrude{ Surface{204, 207}; Layers{10, 7.5*lc}; Recombine; };
//-------------

SurfacesToExtrude[] = {204, 207}; 
For k In {0:#SurfacesToExtrude[]-1}
    out[] = Extrude{ Surface{SurfacesToExtrude[k]}; Layers{10, 7.5*lc}; Recombine; };
    surfaceOut1[] += out[0];
    volumesOut[] += out[1];
    surfaceOut2[] += out[2];
    surfaceOut2[] += out[4];
EndFor

Physical Surface("BND_EXTERN") = surfaceOut1[];
Physical Surface("BND_TRANCHE_LAYER") = surfaceOut2[];
Physical Volume("LAYER") = volumesOut[];
