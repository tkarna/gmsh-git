
pmlscale = 1.25;
ldom_a = 3300;
ldom_b = 1200;
ldom2_a = ldom_a*pmlscale;
ldom2_b = ldom_b*pmlscale;
//ldom2_a = 5700;
//ldom2_b = 4000;

posellips_x = 2150;
posellips_y = 0;
posellips_z = 50;

posellips2_x = 2150;
posellips2_y = 0;
posellips2_z = 50;

pospulse_x = -350;
pospulse_y = 0;
pospulse_z = 500;

lc = 340;
lcPulse = 100;


//-------------
// Submarine
//-------------

Merge "Submarine.msh";
CreateTopology;

submarinesurf[] = {1, 9, 6, 3, 2, 7, 8, 5, 10, 4};

// Border of the surface of the submarine
submarineBorder = newll;
Line Loop(submarineBorder) = CombinedBoundary{ Surface{submarinesurf[]}; };

// Surface of the submarine
Physical Surface("BND_SUBMARINE") = submarinesurf[];


//--------------------------
// Semi-ellipsoid  (small)
//--------------------------

elldot0 = newp; Point(elldot0) = {posellips_x       , posellips_y       , posellips_z       , lc};
elldot1 = newp; Point(elldot1) = {posellips_x-ldom_a, posellips_y       , posellips_z       , lc};
elldot2 = newp; Point(elldot2) = {posellips_x+ldom_a, posellips_y       , posellips_z       , lc};
elldot3 = newp; Point(elldot3) = {posellips_x       , posellips_y       , posellips_z+ldom_b, lc};
elldot4 = newp; Point(elldot4) = {posellips_x       , posellips_y+ldom_b, posellips_z       , lc};
elldot5 = newp; Point(elldot5) = {posellips_x       , posellips_y       , posellips_z-ldom_b, lc};

ellcurv0 = newl; Ellipse(ellcurv0) = {elldot1, elldot0, elldot2, elldot3};
ellcurv1 = newl; Ellipse(ellcurv1) = {elldot3, elldot0, elldot2, elldot2};
ellcurv2 = newl; Ellipse(ellcurv2) = {elldot1, elldot0, elldot2, elldot4};
ellcurv3 = newl; Ellipse(ellcurv3) = {elldot4, elldot0, elldot2, elldot2};
ellcurv4 = newl; Ellipse(ellcurv4) = {elldot1, elldot0, elldot2, elldot5};
ellcurv5 = newl; Ellipse(ellcurv5) = {elldot5, elldot0, elldot2, elldot2};

ellsurf1[] = Extrude { {1,0,0}, {posellips_x, posellips_y, posellips_z}, Pi/2 } { Line{ellcurv2}; Line{ellcurv3}; };
ellsurf2[] = Extrude { {1,0,0}, {posellips_x, posellips_y, posellips_z}, Pi/2 } { Line{ellcurv4}; Line{ellcurv5}; };

// Border of the semi-ellipsoid

ellborder[] = {ellcurv0, ellcurv1, -ellcurv5, -ellcurv4};
ellborderLoop = newll;
Line Loop(ellborderLoop) = ellborder[];

// Surface of the semi-ellipsoid

ellsurf[] = {ellsurf2[5], ellsurf1[5], ellsurf1[1], ellsurf2[1]};
ellsurfLoop = newsl;
Surface Loop(ellsurfLoop) = ellsurf[];

Physical Surface("INTERFACE") = ellsurf[];


//--------------------------
// Semi-ellipsoid  (large)
//--------------------------

ell2dot0 = newp; Point(ell2dot0) = {posellips2_x        , posellips2_y        , posellips2_z        , lc};
ell2dot1 = newp; Point(ell2dot1) = {posellips2_x-ldom2_a, posellips2_y        , posellips2_z        , lc};
ell2dot2 = newp; Point(ell2dot2) = {posellips2_x+ldom2_a, posellips2_y        , posellips2_z        , lc};
ell2dot3 = newp; Point(ell2dot3) = {posellips2_x        , posellips2_y        , posellips2_z+ldom2_b, lc};
ell2dot4 = newp; Point(ell2dot4) = {posellips2_x        , posellips2_y+ldom2_b, posellips2_z        , lc};
ell2dot5 = newp; Point(ell2dot5) = {posellips2_x        , posellips2_y        , posellips2_z-ldom2_b, lc};

ell2curv0 = newl; Ellipse(ell2curv0) = {ell2dot1, ell2dot0, ell2dot2, ell2dot3};
ell2curv1 = newl; Ellipse(ell2curv1) = {ell2dot3, ell2dot0, ell2dot2, ell2dot2};
ell2curv2 = newl; Ellipse(ell2curv2) = {ell2dot1, ell2dot0, ell2dot2, ell2dot4};
ell2curv3 = newl; Ellipse(ell2curv3) = {ell2dot4, ell2dot0, ell2dot2, ell2dot2};
ell2curv4 = newl; Ellipse(ell2curv4) = {ell2dot1, ell2dot0, ell2dot2, ell2dot5};
ell2curv5 = newl; Ellipse(ell2curv5) = {ell2dot5, ell2dot0, ell2dot2, ell2dot2};

ell2surf1[] = Extrude { {1,0,0}, {posellips2_x, posellips2_y, posellips2_z}, Pi/2 } { Line{ell2curv2}; Line{ell2curv3}; };
ell2surf2[] = Extrude { {1,0,0}, {posellips2_x, posellips2_y, posellips2_z}, Pi/2 } { Line{ell2curv4}; Line{ell2curv5}; };

// Border of the semi-ellipsoid

ell2border[] = {ell2curv0, ell2curv1, -ell2curv5, -ell2curv4};
ell2borderLoop = newll;
Line Loop(ell2borderLoop) = ell2border[];

// Surface of the semi-ellipsoid

ell2surf[] = {ell2surf2[5], ell2surf1[5], ell2surf1[1], ell2surf2[1]};
ell2surfLoop = newsl;
Surface Loop(ell2surfLoop) = ell2surf[]; 

Physical Surface("BND_EXTERIEUR") = ell2surf[];


//--------------------------
// Section
//--------------------------


// Surface of section of the sall ellipsoid

sectionSurfaceDomain = news;
Plane Surface(sectionSurfaceDomain) = {submarineBorder, ellborderLoop};
//Plane Surface(sectionSurfaceDomain) = {ellborderLoop};
Physical Surface("BND_TRANCHE_DOM") = {sectionSurfaceDomain};

// Surface of section between the small and the large ellipsoids

sectionSurfacePML = news;
Plane Surface(sectionSurfacePML) = {ellborderLoop, ell2borderLoop};
Physical Surface("BND_TRANCHE_PML") = {sectionSurfacePML};


//-------------
// Pulse
//-------------

pulseCenter = newp;
Point(pulseCenter) = {pospulse_x, pospulse_y, pospulse_z, lcPulse};
Physical Point("PULSE") = {pulseCenter};


//-------------
// Volume
//-------------

surfDomain = newsl;
Surface Loop(surfDomain) = {sectionSurfaceDomain, ellsurf[], submarinesurf[]};
//Surface Loop(surfDomain) = {sectionSurfaceDomain, ellsurf[]};
volDomain = newv;
Volume(volDomain) = {surfDomain};
Physical Volume("DOMAIN") = {volDomain};

surfPml = newsl;
Surface Loop(surfPml) = {sectionSurfacePML, ellsurf[], ell2surf[]};
volPml = newv;
Volume(volPml) = {surfPml};
Physical Volume("PML") = {volPml};



