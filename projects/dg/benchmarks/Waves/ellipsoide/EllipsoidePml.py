from dgpy import *
import os
import time
import math


FLAG_PML = 1
  # 0 PML
  # 1 PML LT



if (Msg.GetCommRank()==0):
  print '---- Geometrical parameters'

majRad = 3300
minRad = 1200

posX = 2150
posY = 0
posZ = 50

delta = 300

posPulseX = -300
posPulseY = 0
posPulseZ = 450
R = 150



if (Msg.GetCommRank()==0):
  print '---- Physical parameters'

cValue = 1500.
rhoValue = 1000.

h = 75
dt = 0.02 * (h * (1.-1./math.sqrt(3.))/2.) /cValue;
tFinal = 4;
iMax = int(round(tFinal/dt));



if (Msg.GetCommRank()==0):
  print '---- Build mesh and groups'

model = GModel()
model.load('EllipsoidePml.msh')

dim = 3
order = 1
groups = dgGroupCollection(model, dim, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()



if (Msg.GetCommRank()==0):
  print '---- Build conservation law with boundary conditions'

coef = functionConstant([cValue,rhoValue])

if (FLAG_PML==0):
  law = dgConservationLawWave(dim,"PmlEllipsoide")
  law.setPhysCoef(coef)
  model.load('EllipsoidePml_Distance.pos')
  model.load('EllipsoidePml_Direction.pos')
  law.useGenericPmlCoef('Ellipsoide',delta,cValue,posX,posY,posZ,majRad,minRad,-1)
  law.useGenericPmlDir('Ellipsoide')
  law.addPml('PML')
  law.addInterfacePml('INTERFACE')

if (FLAG_PML==1):
  law = dgConservationLawWave(dim,"PmlEllipsoideLT")
  law.setPhysCoef(coef)
  model.load('EllipsoidePml_Distance.pos')
  model.load('EllipsoidePml_Direction.pos')
  law.useGenericPmlCoef('EllipsoideLT',delta,cValue,posX,posY,posZ,majRad,minRad,-1)
  law.useGenericPmlDir('EllipsoideLT')
  law.addPml('PML')

law.addBoundaryCondition('BND_EXTERIEUR',  law.newBoundaryWall('BND_EXTERIEUR'))
law.addBoundaryCondition('BND_TRANCHE_DOM',law.newBoundaryWall('BND_TRANCHE_DOM'))
law.addBoundaryCondition('BND_TRANCHE_PML',law.newBoundaryWall('BND_TRANCHE_PML'))

nbFields = law.getNbFields()
print nbFields



if (Msg.GetCommRank()==0):
  print '---- Save mesh, coefficients and directions'
  os.system("cp EllipsoidePml.msh output/EllipsoidePml_Mesh.msh")

coefPrintFunc = law.getPmlCoef('PML')
coefPrint = dgDofContainer(groups,3)
coefPrint.setFieldName(0,'coefPml_0')
coefPrint.setFieldName(1,'coefPml_1')
coefPrint.setFieldName(2,'coefPml_2')
coefPrint.interpolate(coefPrintFunc)
coefPrint.exportMsh("output/EllipsoidePml_CoefPml.msh",0,0)

dirPrintFunc = law.getPmlDir('PML')
dirPrint = dgDofContainer(groups,9)
dirPrint.setFieldName(0,'dirPml_Nx')
dirPrint.setFieldName(1,'dirPml_Ny')
dirPrint.setFieldName(2,'dirPml_Nz')
dirPrint.setFieldName(3,'dirPml_Tx')
dirPrint.setFieldName(4,'dirPml_Ty')
dirPrint.setFieldName(5,'dirPml_Tz')
dirPrint.setFieldName(6,'dirPml_Bx')
dirPrint.setFieldName(7,'dirPml_By')
dirPrint.setFieldName(8,'dirPml_Bz')
dirPrint.L2Projection(dirPrintFunc)
dirPrint.exportMsh("output/EllipsoidePml_DirPml.msh",0,0)



if (Msg.GetCommRank()==0):
  print '---- Build initial conditions'

def solInitPython6(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)-posPulseX
    y = xyz.get(i,1)-posPulseY
    z = xyz.get(i,2)-posPulseZ
    r = math.pow(math.pow(x,2)+math.pow(y,2)+math.pow(z,2),0.5)
    valP = math.exp(-math.pow(r/R,2))
    val.set(i,0,valP)
    val.set(i,1,0.)
    val.set(i,2,0.)
    val.set(i,3,0.)
    val.set(i,4,0.)
    val.set(i,5,0.)

def solInitPython12(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)-posPulseX
    y = xyz.get(i,1)-posPulseY
    z = xyz.get(i,2)-posPulseZ
    r = math.pow(math.pow(x,2)+math.pow(y,2)+math.pow(z,2),0.5)
    valP = math.exp(-math.pow(r/R,2))
    val.set(i,0,valP)
    val.set(i,1,0.)
    val.set(i,2,0.)
    val.set(i,3,0.)
    val.set(i,4,0.)
    val.set(i,5,0.)
    val.set(i,6,0.)
    val.set(i,7,0.)
    val.set(i,8,0.)
    val.set(i,9,0.)
    val.set(i,10,0.)
    val.set(i,11,0.)

sol = dgDofContainer(groups,nbFields)
sol.setFieldName(0,'p')
sol.setFieldName(1,'uX')
sol.setFieldName(2,'uY')
sol.setFieldName(3,'uZ')

if (FLAG_PML==0):
  sol.setFieldName(4,'pN')
  sol.setFieldName(5,'pT')
  solInitFunc = functionPython(6,solInitPython6,[xyz])
  
if (FLAG_PML==1):
  sol.setFieldName(4,'pN')
  sol.setFieldName(5,'pT')
  sol.setFieldName(6,'vX')
  sol.setFieldName(7,'vY')
  sol.setFieldName(8,'vZ')
  sol.setFieldName(9,'wX')
  sol.setFieldName(10,'wY')
  sol.setFieldName(11,'wZ')
  solInitFunc = functionPython(12,solInitPython12,[xyz])

sol.L2Projection(solInitFunc)
sol.exportMsh("output/EllipsoidePml_%i_Sol_%03i" % (FLAG_PML,0), 0., 0)



if (Msg.GetCommRank()==0):
  print '---- LOOP'

sys = linearSystemPETScBlockDouble()
dof = dgDofManager.newDGBlock(groups, nbFields, sys)
solver = dgERK(law, dof, DG_ERK_44)

nbExport = 1
timeInit = time.clock()
for i in range(1,iMax+1): 
  t = i*dt
  solver.iterate(sol, dt, t)
  Msg.Barrier()
  if (Msg.GetCommRank()==0):
    timeCurrent = time.clock()
    timeMeanByIter = (timeCurrent-timeInit)/i/60/60
    print 'PML', FLAG_PML,'- |INDEX|', i, '/', iMax, '|t|', t, '|ExpTime|', timeMeanByIter*(iMax-i)
  if (((180*i)//(iMax))==nbExport):
    sol.exportMsh("output/EllipsoidePml_%i_Sol_%03i" % (FLAG_PML,nbExport), t, nbExport)
    nbExport = nbExport + 1
  Msg.Barrier()



Msg.Exit(0)
