
Mesh.CharacteristicLengthMax = 300;

Mesh.Optimize = 1;
Mesh.Algorithm = 1;
Mesh.Algorithm3D = 4;
lc = Mesh.CharacteristicLengthMax;


//-------------
// Parameters
//-------------

majRad = 3300;    // Major radius (x)
minRad = 1200;    // Minor radius (y,z)

posX = 2150;
posY = 0;
posZ = 50;

posPulseX = -300;
posPulseY = 0;
posPulseZ = 450;

Ndelta = 1;
delta = 300;


//------------
// Ellipsoid
//------------

ellPoint[0] = newp; Point(ellPoint[0]) = {posX       , posY, posZ       , lc};
ellPoint[1] = newp; Point(ellPoint[1]) = {posX-majRad, posY, posZ       , lc/1.55};
ellPoint[2] = newp; Point(ellPoint[2]) = {posX+majRad, posY, posZ       , lc/1.55};
ellPoint[3] = newp; Point(ellPoint[3]) = {posX       , posY, posZ+minRad, lc};
ellLine[0] = newl; Ellipse(ellLine[0]) = {ellPoint[1], ellPoint[0], ellPoint[2], ellPoint[3]};
ellLine[1] = newl; Ellipse(ellLine[1]) = {ellPoint[3], ellPoint[0], ellPoint[2], ellPoint[2]};
out[] = Extrude { {1,0,0}, {posX, posY, posZ}, -Pi } { Line{ellLine[0]}; Line{ellLine[1]}; };

llTranche = newll; Line Loop(llTranche) = {out[0], out[3], -ellLine[1], -ellLine[0]};
surTranche = news; Plane Surface(surTranche) = {llTranche};

surInterface[] = {out[1], out[4]};

slDom = newsl; Surface Loop(slDom) = {surTranche, surInterface[]};
volDom = newv; Volume(volDom) = {slDom};

Physical Surface("BND_TRANCHE_DOM") = {surTranche};
Physical Surface("INTERFACE") = {surInterface[]};
Physical Volume("DOMAIN") = {volDom};


//-------------
// Pulse
//-------------

pulseCenter = newp; Point(pulseCenter) = {posPulseX, posPulseY, posPulseZ, lc/2};
Point {pulseCenter} In Surface {surTranche};
Physical Point("PULSE") = {pulseCenter};


//-------------
// Layer (Extrusion)
//-------------

SurfacesToExtrude[] = {out[1], out[4]};
For k In {0:#SurfacesToExtrude[]-1}
    out[] = Extrude{ Surface{SurfacesToExtrude[k]}; Layers{Ndelta, delta};};
    surfaceOut1[] += out[0];
    volumesOut[]  += out[1];
    surfaceOut2[] += out[2];
    surfaceOut2[] += out[4];
EndFor

Physical Surface("BND_EXTERIEUR") = surfaceOut1[];
Physical Surface("BND_TRANCHE_PML") = surfaceOut2[];
Physical Volume("PML") = volumesOut[];
