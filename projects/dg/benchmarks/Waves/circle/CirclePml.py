from dgpy import *
import os
import time
import math


print'---- Load Mesh and Build Groups'

model = GModel()
model.load('CirclePml.msh')

dim = 2
order = 1
groups = dgGroupCollection(model, dim, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()


print'---- Build Initial conditions'

def initialCondition(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)+20
    y = xyz.get(i,1)+25
    val.set(i,0,math.exp(-(x*x+y*y)/5))
    val.set(i,1,0.)
    val.set(i,2,0.)
    val.set(i,3,0.)
initialConditionPython = functionPython(4,initialCondition,[xyz])

solutionref = dgDofContainer(groups,3)
solutionref.setFieldName(0,'p_ref')
solutionref.setFieldName(1,'u_ref')
solutionref.setFieldName(2,'v_ref')
solutionref.L2Projection(initialConditionPython)
#solutionref.exportMsh('output/CircleWithoutPml_00000',0,0)

solutionpml = dgDofContainer(groups,4)
solutionpml.setFieldName(0,'p_pml')
solutionpml.setFieldName(1,'u_pml')
solutionpml.setFieldName(2,'v_pml')
solutionpml.setFieldName(3,'q_pml')
solutionpml.L2Projection(initialConditionPython)
solutionpml.exportMsh('output/CirclePml_00000',0,0)


print'---- Load Conservation law with coefficients'

lawref = dgConservationLawWave(dim)
lawpml = dgConservationLawWave(dim,'PmlCircle')

cValue   = 340.
rhoValue = 1.293
coef = functionConstant([cValue,rhoValue])

lawref.setPhysCoef(coef)
lawpml.setPhysCoef(coef)

lawref.setFluxDecentering(1.)
lawpml.setFluxDecentering(1.)

#lawref.useCG()
#kappaNum = 1000000000
#numcoef= functionConstant([kappaNum])
#lawref.setNumCoef(coefref)


print'---- Load PML'

ldom = 50
lpml = 7

def absCoefDef(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    r = math.sqrt((x*x)+(y*y))
    rloc = r-ldom
    sigmaPml = cValue * (1/((lpml*1.01)-rloc) - 1/(lpml*1.01))
    #sigmaPml = cValue/(lpml*1.01) * (rloc)/((lpml*1.01)-rloc)
    sigmaPmlBar = cValue/r * (- math.log((lpml*1.01)-rloc) + math.log(lpml*1.01) - rloc/(lpml*1.01))
    val.set(i,0,sigmaPml)
    val.set(i,1,sigmaPmlBar)
absCoef = functionPython(2,absCoefDef,[xyz])

def absDirDef(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    r = math.sqrt((x*x)+(y*y))
    vecx = x/r
    vecy = y/r
    val.set(i,0,vecx)
    val.set(i,1,vecy)
absDir = functionPython(2,absDirDef,[xyz])

lawpml.setAbsCoef(absCoef)
lawpml.setAbsDir(absDir)

lawpml.addPml('PML')
lawpml.addInterfacePml('INTERFACE')


print'---- Load Boundary conditions'

lawref.addBoundaryCondition('BOUNDARY', lawref.newBoundaryWall('BOUNDARY'))
lawpml.addBoundaryCondition('BOUNDARY', lawpml.newBoundaryWall('BOUNDARY'))


print'---- LOOP'

sysref = linearSystemPETScBlockDouble()
sysref.setParameter("petscOptions", "-pc_type ilu")
dofref = dgDofManager.newDGBlock(groups, 3, sysref)

syspml = linearSystemPETScBlockDouble()
syspml.setParameter("petscOptions", "-pc_type ilu")
dofpml = dgDofManager.newDGBlock(groups, 4, syspml)

#sysref = linearSystemPETScDouble()
#dofref = dgDofManager.newCG(groups, 3, sysref)
#solverref = dgDIRK(lawref, dofref, 2)
#solverpml = dgDIRK(lawpml, dofpml, 2)

solverref = dgCrankNicholson(solutionref, lawref, dofref, 0.)
solverref.setComputeExplicitTerm(True)
solverref.getNewton().setVerb(20)

solverpml = dgCrankNicholson(solutionpml, lawpml, dofpml, 0.)
solverpml.setComputeExplicitTerm(True)
solverpml.getNewton().setVerb(20)


print'---- TAG'


dt = 0.0025
iterMax = 60
for i in range(1,iterMax+1):
  t = i*dt
  #solverref.iterate(solutionref, dt , t)
  #solverref.iterate(t)
  #solverpml.iterate(solutionpml, dt , t)
  solverpml.iterate(t)
  #solutionref.exportMsh("output/CircleWithoutPml_%05d" % (i), t, i)
  solutionpml.exportMsh("output/CirclePml_%05d" % (i), t, i)
  ENERGY = lawref.getTotalEnergy(groups,solutionref,'DOMAIN') + lawref.getTotalEnergy(groups,solutionref,'PML')
  print'|ITER|', i, '/', iterMax, '|DT|', dt, '|t|', t, '|ENERGY|', ENERGY


Msg.Exit(0)

