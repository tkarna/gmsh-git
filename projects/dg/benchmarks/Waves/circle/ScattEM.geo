
LREF = 0.5;
ldom = 0.5/LREF;
lpml = 0.15/LREF;
lext = 0.5/LREF;

lc = 0.025/LREF;
lcExt = 2*lc;
lcPmlExt = lc;
lcPmlInt = lc;
lcScatExt = lc/1.2;
lcScatInt = lc/1.2;

L  = 0.16/LREF;
x0 = 0.29/LREF;
x1 = 0.3/LREF;
y0 = 0.19/LREF;
y1 = 0.20/LREF;

Point(1) = {0.,0.,0.,lcPmlInt};

Point(2) = {  ldom,    0., 0., lcPmlInt};
Point(3) = {     0,  ldom, 0., lcPmlInt};
Point(4) = { -ldom,    0., 0., lcPmlInt};
Point(5) = {     0, -ldom, 0., lcPmlInt};

Point(6) = {  ldom+lpml,         0., 0., lcPmlExt};
Point(7) = {          0,  ldom+lpml, 0., lcPmlExt};
Point(8) = { -ldom-lpml,         0., 0., lcPmlExt};
Point(9) = {          0, -ldom-lpml, 0., lcPmlExt};

Point(20) = { -x1, -L/2, 0, lcScatInt};
Point(21) = { -x1,  -y1, 0, lcScatExt};
Point(22) = {  x1,  -y1, 0, lcScatExt};
Point(23) = {  x1,   y1, 0, lcScatExt};
Point(24) = { -x1,   y1, 0, lcScatExt};
Point(25) = { -x1,  L/2, 0, lcScatInt};
Point(26) = { -x0,  L/2, 0, lcScatInt};
Point(27) = { -x0,   y0, 0, lcScatInt};
Point(28) = {  x0,   y0, 0, lcScatInt};
Point(29) = {  x0,  -y0, 0, lcScatInt};
Point(30) = { -x0,  -y0, 0, lcScatInt};
Point(31) = { -x0, -L/2, 0, lcScatInt};

Circle(1) = {2,1,3};
Circle(2) = {3,1,4};
Circle(3) = {4,1,5};
Circle(4) = {5,1,2};

Circle(5) = {6,1,7};
Circle(6) = {7,1,8};
Circle(7) = {8,1,9};
Circle(8) = {9,1,6};

Line(20) = {20, 21};
Line(21) = {21, 22};
Line(22) = {22, 23};
Line(23) = {23, 24};
Line(24) = {24, 25};
Line(25) = {25, 26};
Line(26) = {26, 27};
Line(27) = {27, 28};
Line(28) = {28, 29};
Line(29) = {29, 30};
Line(30) = {30, 31};
Line(31) = {31, 20};

Line Loop(1) = {1, 2, 3, 4};
Line Loop(2) = {-8, -7, -6, -5};
Line Loop(4) = {20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31};

Plane Surface(1) = {1, 4};  // DOM
Plane Surface(2) = {1, 2};  // PML
Plane Surface(4) = {4};     // SCATT

Physical Line("BOUNDARY_DOM") = {1, 2, 3, 4};
Physical Line("BOUNDARY_PML") = {-8, -7, -6, -5};
Physical Line("BOUNDARY_SCATT") = {20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31};

Physical Surface("DOM") = {1};
Physical Surface("PML") = {2};
Physical Surface("SCATT") = {4};

