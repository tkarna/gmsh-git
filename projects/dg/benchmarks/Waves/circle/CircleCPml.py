from gmshpy import *
from dgpy import *
import os
import time
import math


print'---- Load Mesh and Groups'

model = GModel()
model.load('CircleCPml.msh')

dim = 2
order = 1
groups = dgGroupCollection(model, dim, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()


print'---- Build Conservation laws'

law = dgConservationLawWave(dim,'PmlCircle')
nbfields = 4

cValue = 1.
rhoValue = 1.
coef = functionConstant([cValue,rhoValue])
law.setPhysCoef(coef)


print'---- Build Initial conditions'

pospulse_x = -500
pospulse_y = 500
r = 25000
def initialCondition(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)-pospulse_x
    y = xyz.get(i,1)-pospulse_y
    val.set(i,0,math.exp(-(x*x+y*y)/r))
    val.set(i,1,0.)
    val.set(i,2,0.)
    val.set(i,3,0.)
initialConditionPython = functionPython(nbfields,initialCondition,[xyz])

solution = dgDofContainer(groups,nbfields)
solution.setFieldName(0,'p_cpml_circ')
solution.setFieldName(1,'u_cpml_circ')
solution.setFieldName(2,'v_cpml_circ')
solution.setFieldName(3,'pN_cpml_circ')
solution.L2Projection(initialConditionPython)
solution.exportMsh('output/CircleCPml_00000',0,0)


print'---- Build Pml'

ldom = 1000
lpml = 200*1.01

def pmlCoefDef(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    r = math.sqrt((x*x)+(y*y))
    rloc = r-ldom
    sigmaPml = 0
    sigmaPmlBar = 0
    if ((rloc>0) and (rloc<lpml)):
      sigmaPml = cValue/lpml * (rloc/(lpml-rloc))
      sigmaPmlBar = cValue/r * (- math.log(1-rloc/lpml) - rloc/lpml)
    val.set(i,0,sigmaPml)
    val.set(i,1,sigmaPmlBar)
pmlCoef = functionPython(2,pmlCoefDef,[xyz])

def pmlDirDef(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    r = math.sqrt((x*x)+(y*y))
    vecx = x/r
    vecy = y/r
    val.set(i,0,vecx)
    val.set(i,1,vecy)
pmlDir = functionPython(2,pmlDirDef,[xyz])

#law.setPmlCoef(pmlCoef)
#law.setPmlDir(pmlDir)

law.useGenericPmlCoef('Circle', lpml,cValue, 0.,0.,0., ldom)
law.useGenericPmlDir('Circle')

law.addPml('PML')
law.addInterfacePml('INTERFACE')
law.addInterfaceWall('DOUNDARY_PML')


print'---- Build Boundary conditions'

law.addBoundaryCondition('BOUNDARY_EXT', law.newBoundaryWall('BOUNDARY_EXT'))


print'---- LOOP'

implicit = True;

if (implicit):
  dt = 25
  sys = linearSystemPETScBlockDouble()
  dof = dgDofManager.newDGBlock(groups, nbfields, sys)
  solver = dgDIRK(law, dof, 2)
  solver.getNewton().setVerb(10)
  for i in range(1,70):
    t = i*dt
    solver.iterate(solution, dt , t)
    solution.exportMsh("output/CircleCPml_%05i" % (i), t, i)
    print'|ITER|', i, '|DT|', dt, '|t|', t


Msg.Exit(0)

