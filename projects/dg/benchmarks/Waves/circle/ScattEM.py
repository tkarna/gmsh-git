from dgpy import *
import os
import time
import math

LREF = 0.5
ldom = 0.5/LREF
lpml = 0.15/LREF


print'---- Load Mesh and Build Groups'

model = GModel()
model.load('ScattEM.msh')

dim = 2
order = 1
groups = dgGroupCollection(model, dim, order)
groups.splitGroupsByPhysicalTag()
xyz = groups.getFunctionCoordinates()


print'---- Load Conservation law with coefficients'

lawobc = dgConservationLawWave(dim)
lawpml = dgConservationLawWave(dim,'PmlCircle')
lawpml2 = dgConservationLawWave(dim,'PmlCircle')

#muValue  = 12.566370614 * pow(10,-7)
#epsValue = 8.85 * pow(10,-12)
muValue  = 1
epsValue = 1
cValue   = 1/math.sqrt(muValue*epsValue)  # 1/Sqrt(Mu*Epsilon)
rhoValue = muValue                        # Mu
coef = functionConstant([cValue,rhoValue])

lawobc.setPhysCoef(coef)
lawpml.setPhysCoef(coef)
lawpml2.setPhysCoef(coef)


print'---- Build Initial conditions'

initialConditionPython = functionConstant([0.,0.,0.,0.])

solutionobc = dgDofContainer(groups,3)
solutionobc.setFieldName(0,'Ez_obc')
solutionobc.setFieldName(1,'-Hy_obc')
solutionobc.setFieldName(2,'Hx_obc')
solutionobc.L2Projection(initialConditionPython)

solutionpml = dgDofContainer(groups,4)
solutionpml.setFieldName(0,'Ez_pml')
solutionpml.setFieldName(1,'-Hy_pml')
solutionpml.setFieldName(2,'Hx_pml')
solutionpml.setFieldName(3,'Q_pml')
solutionpml.L2Projection(initialConditionPython)

solutionpml2 = dgDofContainer(groups,4)
solutionpml2.setFieldName(0,'Ez_pml2')
solutionpml2.setFieldName(1,'-Hy_pml2')
solutionpml2.setFieldName(2,'Hx_pml2')
solutionpml2.setFieldName(3,'Q_pml2')
solutionpml2.L2Projection(initialConditionPython)


print'---- Load PML'

def pmlCoefDef(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    r = math.sqrt((x*x)+(y*y))
    rloc = r-ldom
    sigmaPml = 0
    sigmaPmlBar = 0
    if (rloc>0) and (rloc<lpml):
      sigmaPml = cValue * (1/((lpml*1.01)-rloc) - 1/(lpml*1.01))
      sigmaPmlBar = cValue * (- math.log((lpml*1.01)-rloc) + math.log(lpml*1.01) - rloc/(lpml*1.01))
      curv = 1/r
    val.set(i,0,sigmaPml)
    val.set(i,1,sigmaPmlBar)
    val.set(i,2,curv)
pmlCoef = functionPython(3,pmlCoefDef,[xyz])

def pmlDirDef(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    r = math.sqrt((x*x)+(y*y))
    vecx = x/r
    vecy = y/r
    val.set(i,0,vecx)
    val.set(i,1,vecy)
    val.set(i,2,0.)
pmlDir = functionPython(3,pmlDirDef,[xyz])

lawpml.setPmlCoef(pmlCoef)
lawpml.setPmlDir(pmlDir)
lawpml.addPml('PML')

lawpml2.useGenericPmlCoef('Circle', lpml,cValue, 0.,0.,0., ldom)
lawpml2.useGenericPmlDir('Circle')
lawpml2.addPml('PML')


print'---- Load Interface and Boundary conditions'

t=0
def extFieldFunction(val,xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    value = math.exp(-math.pow((x+ldom+0.2-cValue*t),2)*100.)
    val.set(i,0,value)
    val.set(i,1,value)
    val.set(i,2,0.)
extField = functionPython(3, extFieldFunction, [xyz])

lawobc.addInterfaceHomogeneousDirichletOnP('BOUNDARY_SCATT')
lawobc.addInterfaceOpenOutflowWithInflow('BOUNDARY_DOM', extField)
lawobc.addBoundaryCondition('BOUNDARY_PML', lawobc.newBoundaryWall('BOUNDARY_PML'))

lawpml.addInterfaceHomogeneousDirichletOnP('BOUNDARY_SCATT')
lawpml.addInterfaceScatteredTotalFields('BOUNDARY_DOM', extField, pmlDir)
lawpml.addBoundaryCondition('BOUNDARY_PML', lawpml.newBoundaryWall('BOUNDARY_PML'))

lawpml2.addInterfaceHomogeneousDirichletOnP('BOUNDARY_SCATT')
lawpml2.addInterfaceScatteredTotalFields('BOUNDARY_DOM', extField, pmlDir)
lawpml2.addBoundaryCondition('BOUNDARY_PML', lawpml2.newBoundaryWall('BOUNDARY_PML'))


print'---- LOOP'

sysobc = linearSystemPETScBlockDouble()
sysobc.setParameter("petscOptions", "-pc_type ilu")
dofobc = dgDofManager.newDGBlock(groups, 3, sysobc)
solverobc = dgCrankNicholson(solutionobc, lawobc, dofobc, 0.)
solverobc.setComputeExplicitTerm(True)

syspml = linearSystemPETScBlockDouble()
syspml.setParameter("petscOptions", "-pc_type ilu")
dofpml = dgDofManager.newDGBlock(groups, 4, syspml)
solverpml = dgCrankNicholson(solutionpml, lawpml, dofpml, 0.)
solverpml.setComputeExplicitTerm(True)

syspml2 = linearSystemPETScBlockDouble()
syspml2.setParameter("petscOptions", "-pc_type ilu")
dofpml2 = dgDofManager.newDGBlock(groups, 4, syspml2)
solverpml2 = dgCrankNicholson(solutionpml2, lawpml2, dofpml2, 0.)
solverpml2.setComputeExplicitTerm(True)

dt = 3*pow(10,-2)
iterMax = 100
for i in range(1,iterMax+1):
  t = i*dt
  solverobc.iterate(t)
  solutionobc.exportMsh("output/ScattEM_Obc_%05d" % (i), t, i)
  solverpml.iterate(t)
  solutionpml.exportMsh("output/ScattEM_Pml_%05d" % (i), t, i)
  solverpml2.iterate(t)
  solutionpml2.exportMsh("output/ScattEM_PmlGen_%05d" % (i), t, i)
  print '|ITER|', i

Msg.Exit(0)



