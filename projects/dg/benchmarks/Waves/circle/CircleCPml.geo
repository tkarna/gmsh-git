
ldom = 1000;
lpml = 200;
lext = 2000;

lc = 30;

pospulse_x = -500;
pospulse_y = 500;

Point(101) = { 0, 0, 0, lc};
Point(102) = { pospulse_x, pospulse_y, 0, lc};

Point(103) = { ldom,   0., 0., lc};
Point(104) = {   0., ldom, 0., lc};
Point(105) = {-ldom,   0., 0., lc};
Point(106) = {   0.,-ldom, 0., lc};

Point(107) = { ldom+lpml,        0., 0., lc};
Point(108) = {        0., ldom+lpml, 0., lc};
Point(109) = {-ldom-lpml,        0., 0., lc};
Point(110) = {        0.,-ldom-lpml, 0., lc};

Point(111) = { lext+pospulse_x,      pospulse_y, 0., 2*lc};
Point(112) = {      pospulse_x, lext+pospulse_y, 0., 2*lc};
Point(113) = {-lext+pospulse_x,      pospulse_y, 0., 2*lc};
Point(114) = {      pospulse_x,-lext+pospulse_y, 0., 2*lc};

Circle(201) = {103, 101, 104};
Circle(202) = {104, 101, 105};
Circle(203) = {105, 101, 106};
Circle(204) = {106, 101, 103};

Circle(205) = {107, 101, 108};
Circle(206) = {108, 101, 109};
Circle(207) = {109, 101, 110};
Circle(208) = {110, 101, 107};

Circle(209) = {111, 102, 112};
Circle(210) = {112, 102, 113};
Circle(211) = {113, 102, 114};
Circle(212) = {114, 102, 111};

bnd_dom[] = {201, 202, 203, 204};
bnd_pml[] = {205, 206, 207, 208};
bnd_ext[] = {209, 210, 211, 212};

bnd_dom_ll = newll;
Line Loop(bnd_dom_ll) = {bnd_dom[]};
sur_dom = news;
Plane Surface(sur_dom) = {bnd_dom_ll};

bnd_pml_ll = newll;
Line Loop(bnd_pml_ll) = {bnd_pml[], -bnd_dom[]};
sur_pml = news;
Plane Surface(sur_pml) = {bnd_pml_ll};

bnd_ext_ll = newll;
Line Loop(bnd_ext_ll) = {bnd_ext[], -bnd_pml[]};
sur_ext = news;
Plane Surface(sur_ext) = {bnd_ext_ll};

Physical Line("INTERFACE") = {bnd_dom[]};
Physical Line("BOUNDARY_PML")  = {bnd_pml[]};
Physical Line("BOUNDARY_EXT")  = {bnd_ext[]};

Physical Surface("DOM") = {sur_dom};
Physical Surface("PML") = {sur_pml};
Physical Surface("EXT") = {sur_ext};
