model = GModel()
model:load ('balzano3thin.msh')
order = 1
dimension = 2

T = 0

--[[ 
     Function for initial conditions
--]]
function tide( SOL, FCT )
  for i=0,FCT:size1()-1 do
    if (T < 3600*6) then
      FCT:set(i,0,2.0 * math.cos (2*3.14*T/(3600.*12)) +2)  
    else
      FCT:set(i,0,-2.0+2) 
    end
    FCT:set(i,1,0) 
    FCT:set(i,2,0)
  end
end

--[[ 
     Function for initial conditions
--]]
function initial_data( XYZ, FCT )
  for i=0,XYZ:size1()-1 do
    FCT:set(i,0,4) 
    FCT:set(i,1,0.0) 
    FCT:set(i,2,0.0) 
  end
end

CFunctions =[[
void bathymetry (fullMatrix<double> &bath, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< bath.size1(); i++) {
    double x = xyz(i,0);
    if (x < 3600 || x > 6000) 
      bath(i,0) = x/2760;
    else if (x >= 3600 && x <4800)
      bath(i,0) = -x/2760 + 60./23;
    else
      bath(i,0) = x/920 - 100./23;

    bath(i,0) -= 2;
  }
}
void detector (fullMatrix<double> &detect, fullMatrix<double> &bath, fullMatrix<double> &sol) {
  bool limited = false;
  for (size_t i = 0; i< bath.size1 (); i++) {
    double H = sol (i, 0) + bath (i, 0);
    limited |= (H < 0.1);
  }
  detect(0,0) = -1;
  if (limited) {
    detect (0, 1) = 1;
    detect (0, 2) = 1;
  } else {
    detect (0, 1) = -1;
    detect (0, 2) = -1;
  }
}

void manning (fullMatrix<double> &cd, fullMatrix<double> &bath, fullMatrix<double> &sol) {
  double n = 0.02;
  for (size_t i = 0; i< bath.size1(); i++) {
    cd(i,0)= n*n*9.81/pow(bath(i,0)+sol(i,0),1.33); 
  }
}
]]

if (Msg.getCommRank() == 0 ) then
  cfile = io.popen("g++ -O3 -pipe -fPIC -shared -o tmp.so -I ../../../Numeric -I../../../Common -I../../dgbuild/gmsh/Common -x c++ - ","w");
  cfile:write("#include\"fullMatrix.h\"\nextern \"C\" {")
  cfile:write(CFunctions)
  cfile:write("}")
  cfile:close()
end

XYZ = functionCoordinates.get();
FRS = functionLua(3, 'initial_data', {XYZ})
BAT = functionC('tmp.so', 'bathymetry',1, {XYZ})
MAN = functionC('tmp.so', 'manning', 3, {BAT, functionSolution.get()} )
DETECT = functionC('tmp.so', 'detector', 3, {BAT, functionSolution.get()} )
TID = functionLua(3, 'tide', {functionSolution.get()} )

claw = dgConservationLawShallowWater2d()
claw:setQuadraticDissipation(MAN)
claw:setLinearDissipation(functionConstant({0.0}))
claw:setCoriolisFactor(functionConstant({0.0}))
claw:setSource(functionConstant({0,0}))
claw:setBathymetry(BAT)
claw:addBoundaryCondition('Wall',claw:newBoundaryWall())
claw:addBoundaryCondition('Tide',claw:newOutsideValueBoundary(TID))

groups = dgGroupCollection(model, dimension, order)
groups:buildGroupsOfInterfaces()

solution = dgDofContainer(groups, claw:getNbFields())
bfield = dgDofContainer(groups, 1)
bfield:L2Projection(BAT)
bfield:exportMsh(string.format('output/bath-%06d',0), 0, 0)
solution:L2Projection(FRS)
solution:exportMsh(string.format('output/solution-%06d',0), 0,0)

rk=dgRungeKutta()
-- limiter = dgSlopeLimiter(claw)
limiter = dgLimiterClip(claw)
-- limiter:setDetector(DETECT)
rk:setLimiter(limiter)
--limiter:apply(solution)

local x = os.clock()
export_dt = 600;
export_i = 0;
end_time = 24*3600;

CFL = 1
dt = math.max(math.min(CFL*rk:computeInvSpectralRadius (claw,solution),1),0.000001)
t=0;
for i=1,1200000 do
  norm = rk:iterateEuler(claw,t,dt,solution)
  dt = CFL*rk:computeInvSpectralRadius (claw,solution)
  t = t + dt
--   io.write(".")
  if (t > (export_i+1)*export_dt or norm == nan) then
--     io.write("\n")
    io.write(string.format('%3d |ITER| %5d |NORM| %8.3f |T| %9.3f |DT| %.3f |CPU| %.3f\n' ,export_i,i,norm,t,dt,os.clock() - x))
    solution:exportMsh(string.format('output/solution-%06d',i), t, export_i)
    bfield:exportMsh(string.format('output/bath-%06d',i), t, export_i)
    export_i = export_i + 1
  end
  if (t>end_time) then
    break
  end
end
