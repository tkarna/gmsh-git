cl1 = 1;

le=3.14*6378137;
lp=3.14*6356752;
Point(1) = {-le, -lp/2, 0, cl1};
Point(2) = {-le, lp/2, 0, cl1};
Point(3) = {le, lp/2, 0, cl1};
Point(4) = {le, -lp/2, 0, cl1};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

Line Loop(4) = {1,2,3,4};
Plane Surface(5) = {4};

Extrude {0,0,-4000} {
  Surface{5};
  Layers{{7,7,15},{0.025,0.125,1}};
  Recombine;
}
Mesh.Algorithm3D=4;
Physical Surface("inlet") = {5};
Physical Surface("outlet") = {27};
Physical Surface("wall") = {26,14};
Physical Surface("symm") = {18,22};
Physical Volume("vol") = {1};
Field[1] = MathEval;
Field[1].F = "500000";
Background Field = 1;
