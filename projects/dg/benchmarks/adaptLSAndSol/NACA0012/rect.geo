
lc = 1;

c = 5;    // Chord of the airfoil
H = 4*c;  // Height of domain
LUp = 4*c;
LDown = 8*c;

Point(1) = {-LUp,-H/2,0.0,lc};         
Point(2) = {-LUp,H/2,0.0,lc};         
Point(3) = {LDown,H/2,0.0,lc};         
Point(4) = {LDown,-H/2,0.0,lc};    

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};

Line Loop(5) = {1, 2, 3, 4};
Plane Surface(6) = {5};

//Mesh.AnisoMax = 1.e33;
//Mesh.SmoothRatio = 100;

Physical Line("inlet")={1};
Physical Line("top")={2};
Physical Line("outlet")={3};
Physical Line("bottom")={4};

Physical Surface("domain")={6};

