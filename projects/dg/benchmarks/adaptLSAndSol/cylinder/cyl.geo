
lDom = 2.2;
lUp = 0.15+0.1/2;
hDomPlus = 0.16+0.1/2;
hDomMinus = 0.15+0.1/2;

lcFar = 0.01;

Point (1) = {-lUp, -hDomMinus, 0, lcFar};
Point (2) = {-lUp+lDom, -hDomMinus, 0, lcFar};
Point (3) = {-lUp+lDom, hDomPlus, 0, lcFar};
Point (4) = {-lUp, hDomPlus, 0, lcFar};

Line (1) = {1,2};
Line (2) = {2,3};
Line (3) = {3,4};
Line (4) = {4,1};

Line Loop (5) = {1,2,3,4};
Plane Surface (6) = {5};

Physical Line("Inlet")={4};
Physical Line("Outlet")={2};
Physical Line("Channel") = {1,3};
Physical Surface("Domain")={6};

