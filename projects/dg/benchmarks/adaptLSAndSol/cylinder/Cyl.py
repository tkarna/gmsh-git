from dgpy import *
from Incompressible import *
from math import *
import time
import sys

os.system("rm output/*")
os.system("rm *.msh")
os.system("rm LiftAndDrag.dat")

# Cylinder Case
# M. Schaefer and S. Turek, "Benchmark computations of laminar flow around a cylinder", Preprints SFB 359 Nummer 96--03, Universitaet Heidelberg, 96--03, 1996

rho = 1.0
mu = 1.e-3
Umax = 0.3

flowDir=[1., 0., 0.]
liftDir=[0., 1., 0.]
Dcyl = 0.1
Rcyl = Dcyl/2
Umean = 2*Umax/3
fScale = 0.5*rho*Umean*Umean*Dcyl
  
nbTimeSteps = 10
timeOrder = 2
dt0 = 10
RTol = 1.e-5
ATol = 1.e-8
nb_adapt_steps = 3
nbIter = 15         # Bamg iterations
allDim = True
Verb = 2

meshName = "cyl"
geoName = meshName

#-------------------------------------------------
#-- Initial function
#-------------------------------------------------
def initF(FCT, XYZ) :
  for i in range(0,FCT.size1()):
    y = XYZ.get(i,1)
    FCT.set(i,0, Umean)
    FCT.set(i,1, 0.) 
    FCT.set(i,2, 0.) 

def InletBC(FCT, XYZ) :
  Hhalf = 0.5*0.41
  Fact = Umax/(Hhalf*Hhalf)
  for i in range(0,FCT.size1()):
    y = XYZ.get(i,1)
    FCT.set(i,0, Fact*(Hhalf-y)*(Hhalf+y))
    FCT.set(i,1, 0.)
    FCT.set(i,2, 0.)
    FCT.set(i,3, fixed)
    FCT.set(i,4, fixed)
    FCT.set(i,5, free)

def lsFunc(FCT, XYZ):
	for i in range(0,FCT.size1()):
		x = XYZ.get(i,0)
		y = XYZ.get(i,1)
#		FCT.set(i,0, x*x+y*y-Rcyl*Rcyl)
		FCT.set(i,0, sqrt(x*x+y*y)-Rcyl)

def normVelFunc(FCT, sol):
	for i in range (0,sol.size1()):
		vx = sol.get(i,0)
		vy = sol.get(i,1)
		FCT.set(i,0,math.sqrt(vx*vx+vy*vy))

#-------------------------------------------------
#-- Initialization
#-------------------------------------------------

#XYZ = function.getCoordinates()
#lsFuncPy = functionPython(1, lsFunc, [XYZ])

#lsCyl = gLevelsetMathEval("sqrt(x*x+y*y)-Rcyl")
lsCyl = gLevelsetSphere(0.,0.,0.,Rcyl)

print 'MAX Reynolds = ', Umean*rho*Dcyl/mu

mesh = GModel()
mesh.load(geoName+".geo")

for step in range(nb_adapt_steps+1):
  
  print "**************************************************"
  print "     Generating mesh: ITER ",   step
  print "**************************************************"
 
  lcMin = Rcyl/100*10**(-0.5*step)
  lcMax = 0.025
  E = lcMax

  adaptedMeshName = meshName + "_adapt%d" % step
  adaptedCutMeshName = meshName + "_adapt%d_cut" % step
  mesh = GModel()
  mesh.load(geoName+".geo")
 
  if (step == 0) :
    mesh.adaptMesh([5], [lsCyl], [[E, lcMin, lcMax]], nbIter, allDim)
  else :
    normVelPy = functionPython(1, normVelFunc, [ns.solution.getFunction()])
    normVelEval = dgFunctionEvaluator(ns.groups, normVelPy)
    normVelEval.setDefaultValue(0.)
    mesh.load(meshName+"_adapt%d.msh"%(step-1))
#    mesh.adaptMesh([5], [lsCyl], [[E, lcMin, lcMax]], nbIter, allDim)
#    mesh.adaptMesh([7], [normVelEval], [[0., lcMin, lcMax]], nbIter, allDim)
    mesh.adaptMesh([5,7], [lsCyl,normVelEval], [[E, lcMin, lcMax],[0, lcMin, lcMax]], nbIter, allDim)
    
  mesh.writeMSH(adaptedMeshName+".msh",2.2,False,False,True,1.0,0,0)
#  mesh.load(adaptedMeshName+".msh")
  
  print "**************************************************"
  print "     Computing on mesh: ITER ",   step
  print "**************************************************"

  meshCut = mesh.buildCutGModel(lsCyl, False, True)
  meshCut.writeMSH(adaptedCutMeshName+".msh",2.2,False,False,True,1.0,0,0)

  rhoF = functionConstant(rho)
  muF = functionConstant(mu)
  ns = Incompressible(adaptedCutMeshName, 2, ["Domain_out"])
  ns.initializeIncomp(initF, rhoF, muF , Umean)

  INLET  = functionPython(6, InletBC, [ns.XYZ])
  lsFuncPy = functionPython(1, lsFunc, [ns.XYZ])
  ns.strongBoundaryConditionLine('Inlet', INLET)
  ns.strongBoundaryConditionLine('levelset_L1', ns.WALL)
  ns.strongBoundaryConditionLine('Channel', ns.WALL)
  ns.weakPresBoundaryCondition('Outlet', ns.PZERO)


  petscOptions ="-ksp_rtol 1.e-3 -pc_type ilu  -pc_factor_levels 2"
  ns.pseudoTimeSteppingSolve(timeOrder, nbTimeSteps, dt0, ATol, RTol,Verb, petscOptions)
  
  ns.computeDragAndLift('levelset_L1', 0, 0, fScale, flowDir, liftDir, 'LiftAndDrag.dat')
  ns.solution.exportFunctionSurf(ns.law.getWSSNorm(), 'output/surface', 0, 0, 'wss', ['levelset_L1'])
  ns.solution.exportFunctionSurf(ns.law.getPressure(), 'output/surface', 0, 0, 'pres', ['levelset_L1'])

