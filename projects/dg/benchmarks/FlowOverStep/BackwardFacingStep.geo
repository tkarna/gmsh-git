// Gmsh project created on Mon Sept 23 12:42:40 2011

lc = 0.0005;

Point(1) = {0, 0, 0, lc};
Point(2) = {0, 0.0052, 0, lc};
Point(3) = {0.02, -0.0049, 0, lc};
Point(4) = {0.02, 0, 0, lc};
Point(5) = {0.02, 0.0052, 0, lc};
Point(6) = {0.08, -0.0049, 0, lc};
Point(7) = {0.08, 0, 0, lc};
Point(8) = {0.08, 0.0052, 0, lc};

Line(10) = {1, 2};
Line(20) = {3, 4};
Line(30) = {4, 5};
Line(40) = {6, 7};
Line(50) = {7, 8};
Line(60) = {2, 5};
Line(70) = {5, 8};
Line(80) = {1, 4};
Line(90) = {4, 7};
Line(100) = {3, 6};

Transfinite Line{10,30,50} = 20 Using Progression 1.2;
Transfinite Line{20,40} = 50 Using Bump 0.2;
Transfinite Line{-60,-80} = 40 Using Progression 1.09;
Transfinite Line{70,90,100} = 30 Using Progression 1.2;

Line Loop(11) = {10, 60, -30, -80};
Line Loop(12) = {30, 70, -50, -90};
Line Loop(13) = {20, 90, -40, -100};

Plane Surface(1400) = {11};
Plane Surface(1500) = {12};
Plane Surface(1600) = {13};

Transfinite Surface{1400} = {1,2,5,4};
Transfinite Surface{1500} = {4,5,8,7};
Transfinite Surface{1600} = {3,4,7,6};

Recombine Surface{1400};
Recombine Surface{1500};
Recombine Surface{1600};

Mesh.Smoothing = 1;

Physical Line("Inlet") = {10};
Physical Line("Outlet") = {50, 40};
Physical Line("Top") = {60, 70};
Physical Line("Bottom") = {100};
Physical Line("StepWall") = {20};
Physical Line("BottomUpstream") = {80};

Physical Surface("Intenal") = {1600, 1500, 1400};
