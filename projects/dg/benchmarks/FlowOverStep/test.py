from dgpy import *
from Incompressible import *
from math import *
import sys

try : os.mkdir("output");
except: 0;

os.system("rm output/*")
#-------------------------------------------------
#-- BackwardFacingStep at Reynolds 100
#-------------------------------------------------
meshName = "BackwardFacingStep"
genMesh(meshName,2,1)

#-------------------------------------------------
#-- Parameters
#-------------------------------------------------
RHO = 1.23
MU  = 0.001
UGlob = 7.813

print('*** Reynolds= %g' % (RHO*UGlob*(2*0.0052)/MU))


#-------------------------------------------------
#-- Initial function
#-------------------------------------------------
def initF(FCT, XYZ):
	for i in range(0,FCT.size1()):
		FCT.set(i,0, UGlob) #u
		FCT.set(i,1, 0.0) #v
		FCT.set(i,2, 0.0) #p

#-------------------------------------------------
#-- Case specific functions
#-------------------------------------------------
def VelBC(FCT, XYZ ):
	for i in range(0,FCT.size1()):
		FCT.set(i,0, UGlob)
		FCT.set(i,1, 0.0)
		FCT.set(i,2, 0.0)
		FCT.set(i,3, fixed)
		FCT.set(i,4, fixed)
		FCT.set(i,5, free)

#-------------------------------------------------
#-- Initialization
#-------------------------------------------------
ns = Incompressible(meshName, 2)
rhoF = functionConstant(RHO)
muF = functionConstant(MU)
ns.initializeIncomp(initF, rhoF, muF , UGlob)
#-------------------------------------------------
#-- Boundary Conditions
#-------------------------------------------------

VEL = functionPython(6, VelBC, [ns.XYZ])
ns.strongBoundaryConditionLine('Inlet', VEL)
ns.strongBoundaryConditionLine('StepWall', ns.WALL)
ns.strongBoundaryConditionLine('Outlet', ns.PZERO)
ns.strongBoundaryConditionLine('Top', ns.WALL)
ns.strongBoundaryConditionLine('Bottom', ns.WALL)
ns.strongBoundaryConditionLine('BottomUpstream', ns.WALL)

#-------------------------------------------------
#-- TimeStepping
#-------------------------------------------------
nbTimeSteps = 10
dt0 = 15
ATol = 1.e-8
RTol = 1.e-5
Verb = 2
petscOptions ="-ksp_rtol 1.e-3 -pc_type lu -pc_factor_levels 0"

#ns.pseudoTimeSteppingSolve(nbTimeSteps, dt0, ATol, RTol, Verb, petscOptions)

ns.pseudoTimeSteppingSolve(2, nbTimeSteps, dt0, ATol, RTol, Verb, petscOptions)

	
#-------------------------------------------------
#-- Validation 
#-------------------------------------------------

evalsol = dgFunctionEvaluator(ns.groups, ns.solution.getFunction())
y = -0.00488
z = 0
for i in range (150):
       x = 0.021 + i*0.0001
       result = fullMatrixDouble(3,1) # Matrix(3*1) for three variables (u, v, p)
       evalsol.compute(x,y,z,result)
       ux = result.get(0,0) # 0 for u
       #uy = result.get(1,0) # 1 for v
       if (ux >= 0):
          diff = (x-0.0329)/0.0329
          print 'Flow over backward facing step at Re = 100'
          print 'Normalized Reattachment Length = ', (x-0.02)/0.0049
          if (diff < 0.1):
             print 'Exit with success:-)' 
	  else :
             print 'Exit with failure :-('
          break 
