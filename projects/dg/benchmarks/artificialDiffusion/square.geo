lc = .2;
Point(1) = {0.0, 0.0, 0, lc};
Point(2) = {1, 0.0, 0, lc};
Point(3) = {1, 1, 0, lc};
Point(4) = {0, 1, 0, lc};
Line(1) = {4, 3};
Line(2) = {3, 2};
Line(3) = {2, 1};
Line(4) = {1, 4};
Line Loop(5) = {2, 3, 4, 1};
Plane Surface(6) = {5};
Physical Surface("Inside") = {6};
Physical Line("Left") = {4};
Physical Line("Right") = {2};
Physical Line("Sides") = {1,3};
Mesh.CharacteristicLengthExtendFromBoundary=1;

Transfinite Line {1,2, 3, 4} = 21;
Transfinite Surface{6};
//Recombine Surface "*";


Mesh.Algorithm = 6;
