from dgpy import *
import math
import time 

U = 1
SCALE=1
F = 1
dt = 0.005

order = 1
m = GModel()
m.load("square.msh")
for entity in m.bindingsGetVertices() + m.bindingsGetEdges() + m.bindingsGetFaces():
  for iV in range(entity.getNumMeshVertices()): 
    v = entity.getMeshVertex(iV)
    v.setXYZ(v.x() * SCALE, v.y() * SCALE, v.z() * SCALE)
m.save("squareScale.msh")

g = dgGroupCollection(m)


CCode = """
#include "fullMatrix.h"
#include "function.h"
extern "C" {
void velocity (dataCacheMap *, fullMatrix<double> &u, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< u.size1(); i++) {
    u(i, 0) = -xyz(i, 1)+0.5;
    u(i, 1) = xyz(i, 0)-0.5;
  }
}

void init (dataCacheMap *, fullMatrix<double> &u, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< u.size1(); i++) {
    double x = 2*(xyz(i, 0) - 0.75);
    double y = 2*(xyz(i, 1) - 0.5);
    u(i, 0) = exp(- (x * x + y * y) / 0.01);
  }
}
}
"""

cLib = "cLib.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CCode, cLib);
Msg.Barrier()

XYZ = function.getCoordinates()

velocity = functionC(cLib,"velocity",2,[XYZ])
init = functionC(cLib,"init",1,[XYZ])

idof = dgDofContainer(g, 1)
idof.interpolate(init)

jumpDiff = dgJumpDiffusion(g, 0, 2* order +1, 0 * U * F, 1, idof)
u = functionConstant([U, 0, 0])
nu = jumpDiff.diffusivity().getFunction()

nuVoid = functionConstant([0])

f0 = functionConstant(0)
law = dgConservationLawAdvectionDiffusion(velocity, nu)
law.addBoundaryCondition("Sides", law.newOutsideValueBoundary("", f0))
law.addBoundaryCondition("Right", law.newOutsideValueBoundary("", f0))
law.addBoundaryCondition("Left", law.newOutsideValueBoundary("", f0))

sol = dgDofContainer(g, 1)
sol.L2Projection(init)
rk = dgERK(law, None, DG_ERK_22)
rk.setLimiter(jumpDiff)
t = 0

#Export data
def getExp(FCT, sol, nu) :
    for i in range (0,FCT.size1()) :
        FCT.set(i,0,sol(i,0))
        FCT.set(i,1,nu(i,0))
        FCT.set(i,2,nu(i,1))
        FCT.set(i,3,nu(i,2))
nCompExp=[1,1,1,1]
namesExp=["c","nuXX","nuXY","nuYY"]
Exp=functionPython(sum(nCompExp), getExp, [sol.getFunction(), nu])

sol.exportFunctionVtk(Exp,'output/export', 0, 0,"solution",nCompExp,namesExp,function.getCoordinates())

tic = time.clock()

i = 0
while (t < 4*2*math.pi):
  if i % 10 == 0 :
    sol.exportFunctionVtk(Exp,'output/export', i, i,"solution",nCompExp,namesExp,function.getCoordinates())
    sol.exportMsh("output/sol-%05d" % i, t, i)
    sol.exportFunctionMsh(nu, "output/nu-%05d" % i, t, i)
    print("%i %e at time %e" % (i, sol.norm(), time.clock()-tic))
  rk.iterate(sol, dt, t)
  t += dt
  i=i+1
  

