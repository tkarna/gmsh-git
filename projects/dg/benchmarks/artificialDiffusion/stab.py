from dgpy import *
import math
import time 

U = 100
SCALE=1
F = 1
dt = 0.125e-2 / 7 / U * SCALE 

order = 1
m = GModel()
m.load("square.msh")
for entity in m.bindingsGetVertices() + m.bindingsGetEdges() + m.bindingsGetFaces():
  for iV in range(entity.getNumMeshVertices()): 
    v = entity.getMeshVertex(iV)
    v.setXYZ(v.x() * SCALE, v.y() * SCALE, v.z() * SCALE)
m.save("squareScale.msh")

g = dgGroupCollection(m)


def input(u, xyz) :
  for i in range(xyz.size1()):
    x = xyz(i, 0)/SCALE
    y = xyz(i, 1)/SCALE
    u.set(i, 0, -x + (1 if (y < 0.3) else 2 if  (y > 0.7) else 1 + (y - 0.3) * 2.5))

def init(u, xyz) :
  for i in range(xyz.size1()):
    x = 1;
    y = xyz(i, 1)/SCALE
    u.set(i, 0, -x + (1 if (y < 0.3) else 2 if  (y > 0.7) else 1 + (y - 0.3) * 2.5))
idof = dgDofContainer(g, 1)
leftValue = functionPython(1, input, [function.getCoordinates()])
idof.interpolate(leftValue)

jumpDiff = dgJumpDiffusion(g, 0, 2* order +1, U * F, 1, idof)
u = functionConstant([U, 0, 0])
nu = jumpDiff.diffusivity().getFunction()

nuVoid = functionConstant([100, 100, 100])

law = dgConservationLawAdvectionDiffusion(u, nu)
law.addBoundaryCondition("Sides", law.new0FluxBoundary())
law.addBoundaryCondition("Right", law.newOutsideValueBoundary("", idof.getFunction()))
law.addBoundaryCondition("Left", law.newOutsideValueBoundary("", idof.getFunction()))
sol = dgDofContainer(g, 1)
#sol.interpolate(functionPython(1, init, [function.getCoordinates()]))
rk = dgERK(law, None, DG_ERK_22)
rk.setLimiter(jumpDiff)
t = 0

#Export data
def getExp(FCT, sol, nu) :
    for i in range (0,FCT.size1()) :
        FCT.set(i,0,sol(i,0))
        FCT.set(i,1,nu(i,0))
        FCT.set(i,2,nu(i,1))
        FCT.set(i,3,nu(i,2))
nCompExp=[1,1,1,1]
namesExp=["c","nuXX","nuXY","nuYY"]
Exp=functionPython(sum(nCompExp), getExp, [sol.getFunction(), nu])

#sol.exportFunctionVtk(Exp,'output/export', 0, 0,"solution",nCompExp,namesExp,function.getCoordinates())

tic = time.clock()

for i in range(2000) :
  if i % 100 == 0 :
    #sol.exportFunctionVtk(Exp,'output/export', i, i,"solution",nCompExp,namesExp,function.getCoordinates())
    sol.exportMsh("output/sol-%05d" % i, t, i)
    sol.exportFunctionMsh(nu, "output/nu-%05d" % i, t, i)
    print("%i %e at time %e" % (i, sol.norm(), time.clock()-tic))
  rk.iterate(sol, dt, t)
  t += dt

