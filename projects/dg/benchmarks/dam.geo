l = 1;
Point(1) = {-l, -l, 0};
Point(2) = {l, -l, 0};
Point(3) = {l, l, 0};
Point(4) = {-l, l, 0};
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line Loop(5) = {3, 4, 1, 2};

Plane Surface(6) = {5};
Physical Surface("Surface")={6};
Physical Line("Wall") = {1,2,3,4};


Transfinite Line {1, 2, 4, 3} = 100 Using Progression 1;
Transfinite Surface {6};
