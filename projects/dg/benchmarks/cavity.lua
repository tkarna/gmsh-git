MACH = .05;
RHO  = 1.0;
GAMMA = 1.4;
PRES = 1./(MACH*RHO*RHO*GAMMA*GAMMA) 
V = 1 
SOUND = V/MACH

model = GModel()
model:load ('cavity.msh')
order = 3
dimension = 2

claw = dgPerfectGasLaw(dimension)
claw:setViscosityAndThermalConductivity(functionConstant({0.1}), functionConstant({0.1}))
claw:addBoundaryCondition('Wall',claw:newSlipWallBoundary())
claw:addBoundaryCondition('Open',claw:newMovingWallBoundary(functionConstant({1,0})))

groups = dgGroupCollection(model, dimension, order)
groups:buildGroupsOfInterfaces()

solution = dgDofContainer(groups, claw:getNbFields())
solution:L2Projection(functionConstant({RHO,0,0,PRES/0.4}))
solution:exportMsh('output/init', 0., 0)

rk=dgRungeKutta()

CFL = 100;
dt = CFL * rk:computeInvSpectralRadius(claw, solution);
t = 0
nbExport = 0
for i=1,1000000 do
  norm = rk:iterate33(claw,t, dt, solution)
  t = t + dt
  if ( i%100 ==0 ) then
    print ('iter ', i, norm)
    solution:exportMsh(string.format('output/solution-%06d',i), t, nbExport)
    nbExport = nbExport +1
  end
end
