# Script to generate smooth bathymetry. Called from main script.
#NB: bathy class defined in dgParticleTracker2D.h

from dgpy import *

def diffuseBathymetry(groups, UTMtoLonLatDegrees, bathDC, filename):
  bNu=20    # Diffusion
  bT=10000  # Time for smoothing 
  #Get started
  if(Msg.GetCommSize() > 1):
    Msg.Fatal("Cannot generate bathymetry file; you need to run the script a first time on only one processor before you can run it on more!")
  Msg.Info( "Diffuse bathymetry")

  #Create output folder
  try : os.mkdir('./Bath');
  except: 0;
    
  XYZ = function.getCoordinates()

  #OLD (full.bin):
  #bathS = functionC(blib, "bath", 1, [UTMToLonLatDegrees])
  #bathDC.interpolate(bathS);

  #NEW:
  #1. Create a bathy object (initialises object and creates bathymetry function)
  bathymetryObj = bathy("./Data/BathHR.bin", UTMtoLonLatDegrees)
  #2. Interpolate the function onto DC
  bathDC.interpolate(bathymetryObj)
  #3. Delete bathymetry object data
  bathymetryObj.clearBathData()

  #Diffuse & save:
  nuB = functionConstant(bNu)
  dlaw = dgConservationLawAdvectionDiffusion.diffusionLaw(nuB)

  dlaw.addBoundaryCondition("Coast", dlaw.new0FluxBoundary())
  dlaw.addBoundaryCondition("Island", dlaw.new0FluxBoundary())
  dlaw.addBoundaryCondition("Islands", dlaw.new0FluxBoundary())
  dlaw.addBoundaryCondition("Open Sea North", dlaw.new0FluxBoundary())
  dlaw.addBoundaryCondition("Open Sea Neutral", dlaw.new0FluxBoundary())
  dlaw.addBoundaryCondition("Open Sea Central", dlaw.new0FluxBoundary())
  dlaw.addBoundaryCondition("Open Sea South", dlaw.new0FluxBoundary())

  sys = linearSystemPETScDouble()
  dof = dgDofManager.newCG (groups, 1, sys)
  implicitEuler = dgDIRK(dlaw, dof)
  implicitEuler.iterate (bathDC, bT, 0)
  exporterBath=dgIdxExporter(bathDC, "./Bath/"+filename+"_bath_smooth");
  exporterBath.exportIdx()
  sys = 0
  dof = 0
  return bathDC
