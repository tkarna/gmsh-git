#"""
#   Simulations on the GBR with multirate explicit Runge-Kutta time-stepping.
#   All the parameters can be found and modified in the file "InpuGBR.py"
#"""

#   A) HOW TO LAUNCH A SIMULATION
#       Simulation on 1 processor  : rundgpy MultirateGBR.py
#       Simulation on n processors : mpirun -np n rundgpy MultirateGBR.py   ( !!! You need MPI !!! )
#
#"""

# Import for python
from dgpy import *
from gmshpy import *
from math import *
from ProjectMesh import *
from InputGBR import *
from termcolor import colored
import gmshPartition
import time, os, sys

if(Msg.GetCommRank() == 0):
  print('')
  print(colored('SLIM: GBR simulation starting. Loading 1:Mesh, 2:Bathymetry 3:Reef Map 4:Conservation law.',"yellow"))
  print('')

# Lauch Makefile to generate meshes and download forcings
if(Msg.GetCommRank() == 0):
  try : os.mkdir('./Data');
  except: 0;
  try : os.mkdir('./GBR_Functions');
  except: 0;
  if(not os.path.exists(filename+".msh")):
    try : os.system("make "+filename+".msh");
    except: 0;
  try : os.system("make forcings");
  except: 0;
  try : os.mkdir(outputDir);
  except: 0;
  try : os.mkdir(outputDir+'/Evaluator');
  except: 0;

Msg.Barrier()

# Definition of Python functions
def printTime (t) :
  return "%dh%02d'%02d\"" % (t/3600,  (t%3600)/60,  t%60)

def printDate (t):
  date=time.gmtime(t)
  return "%02d/%02d/%04d %02dh%02d'%02d\""%(date[2], date[1], date[0], date[3], date[4], date[5])


# Compile libraries
if (Msg.GetCommRank() == 0):
  functionC.buildLibraryFromFile ("Libraries/GBR.cc", "Libraries/lib_gbr.so")
glib = "Libraries/lib_gbr.so"
Msg.Barrier()

# Load model
model = GModel()
if(Msg.GetCommRank() == 0):
  print(colored('Loading mesh for 1 processor (unpartitioned) ...',"red"))
  if(not os.path.exists(filename+'_tan.msh')):
    Msg.Info("Projected mesh doesn't exist. Projecting it.")
    m = GModel()
    m.load(filename+".msh")
    projectMesh(m)
    m.save(filename+"_tan"+".msh")
Msg.Barrier()
model.load(gmshPartition.simple(filename+'_tan.msh'))  

# Coordinate system
XYZ = function.getCoordinates()
lonLatDegrees = functionC(glib,"lonLatDegrees",3,[XYZ])
lonLat = functionC(glib,"lonLat",3,[XYZ])

latLonDegrees = functionC(glib,"latLonDegrees",3,[XYZ])
latLon = functionC(glib,"latLon",3,[XYZ])

groups = dgGroupCollection(model, dimension, order)
#groups.buildGroupsOfInterfaces()
bathDC = dgDofContainer(groups, 1)


# ****************** Bathymetry ******************

if(os.path.exists("./Bath/"+filename+"_bath_smooth/"+filename+"_bath_smooth.idx")):
  Msg.Info("Smoothed bathymetry already exists.")
  bathDC.readMsh("./Bath/"+filename+"_bath_smooth/"+filename+"_bath_smooth.idx")
  Msg.Info("Smoothed bathymetry read.")
else:
  Msg.Fatal("You need to run DiffuseBathGBR_Standalone first!")
#diffuseBathymetryStereo(bathDC, groups, stereoToLonLatDegrees)
#if using full.bin: diffuseBathymetry(bathDC, groups, lonLat)

# ************************************************

# ****************** Reefs ******************
if (Msg.GetCommRank() == 0):
  print(colored('Loading reef map ...',"red"))

reefsDC = dgDofContainer(groups,1)

reefsDCfilename = "GBR_Functions/reefsFunction_"+filename+"/reefsFunction_"+filename+".idx"
if(os.path.exists(reefsDCfilename)):
  if (Msg.GetCommRank() == 0):
    print ('Reef Map file found: '+reefsDCfilename+' ; loading this.')
  reefsDC.readMsh(reefsDCfilename)
else:
  if (Msg.GetCommRank() == 0):
    print ('Reef Map file '+reefsDCfilename+' not found. Loading reef map ...')
  #1. Create a reefs object (initialise object and create reefs function)
  reefsObj = reefs(reefMapFile, lonLatDegrees, 0) #NB: set last flag to 0 for bottomDrag
  #2. Interpolate the function onto DC
  reefsDC.interpolate(reefsObj)
  exporterReefsDC=dgIdxExporter(reefsDC, 'GBR_Functions/reefsFunction_'+filename)
  exporterReefsDC.exportIdx(0,0)
  #3. Delete reefs object data
  reefsObj.clearReefsData()

# ************************************************

t = Ti
timeFunction = functionConstant(t)

# ****************** Forcings ******************
if (Msg.GetCommRank() == 0):
  print(colored('Setting up forcings ...',"red"))
# .................. Tides    ..................

# Tpxo tide 
tideEta = slimFunctionTpxo("./Data/h_tpxo7.2.nc","ha","hp","lon_z","lat_z")
tideEta.setCoordAndTimeFunctions(lonLatDegrees,timeFunction)
tideU = slimFunctionTpxo("./Data/u_tpxo7.2.nc","Ua","up","lon_u","lat_u")
tideU.setCoordAndTimeFunctions(lonLatDegrees,timeFunction)
tideV = slimFunctionTpxo("./Data/u_tpxo7.2.nc","Va","vp","lon_v","lat_v")
tideV.setCoordAndTimeFunctions(lonLatDegrees,timeFunction)
tideUV = functionC(glib,"latLonVector",3,[latLon,tideU,tideV])

# .................. Wind     ..................

windInterpolator = slimStructDataInterpolatorMultilinear()
windUcontainer = slimStructDataContainerNetcdf("./Data/uwind-2000-2011.nc", "uwnd", "lat", "lon")
windVcontainer = slimStructDataContainerNetcdf("./Data/vwind-2000-2011.nc", "vwnd", "lat", "lon")
windU = slimFunctionStructData(windUcontainer, windInterpolator, latLonDegrees)
windV = slimFunctionStructData(windVcontainer, windInterpolator, latLonDegrees)
windU.setTimeFunction(timeFunction)
windV.setTimeFunction(timeFunction)
windUV = functionC(glib,"latLonVector",3,[latLon,windU,windV])

# ............ Residual currents    ............

#Put data into functions
data_A = functionConstant([F_A, vx_A, vy_A])
data_B = functionConstant([F_B, vx_B, vy_B])
data_C = functionConstant([F_C, vx_C, vy_C])

# NCJ Current forcing
bathTimesVector_A = functionC(glib, "bathVec", 3, [bathDC.getFunction(), function.getNormals(), data_A])
bathTimesVector_B = functionC(glib, "bathVec", 3, [bathDC.getFunction(), function.getNormals(), data_B])
bathTimesVector_C = functionC(glib, "bathVec", 3, [bathDC.getFunction(), function.getNormals(), data_C])

bathIntegralNorthFM = fullMatrixDouble(1,1)
bathIntegralCentralFM = fullMatrixDouble(1,1)
bathIntegralSouthFM = fullMatrixDouble(1,1)

dgFunctionIntegratorInterface(groups, bathTimesVector_A).compute('Open Sea North', bathIntegralNorthFM)
dgFunctionIntegratorInterface(groups, bathTimesVector_B).compute('Open Sea Central', bathIntegralCentralFM)
dgFunctionIntegratorInterface(groups, bathTimesVector_C).compute('Open Sea South', bathIntegralSouthFM)
bathIntegralsFuncNorth = functionConstant(bathIntegralNorthFM(0,0))
bathIntegralsFuncCentral = functionConstant(bathIntegralCentralFM(0,0))
bathIntegralsFuncSouth = functionConstant(bathIntegralSouthFM(0,0))

# Generate SEC UV functions
currentUV_North = functionC(glib, "SEC_UV", 3, [bathIntegralsFuncNorth, function.getNormals(), data_A])
currentUV_Central = functionC(glib, "SEC_UV", 3, [bathIntegralsFuncCentral, function.getNormals(), data_B])
currentUV_South = functionC(glib, "SEC_UV", 3, [bathIntegralsFuncSouth, function.getNormals(), data_C])

outputVec = open("GBR_Functions/CurrentUnitVectors.pos", "w")
outputVec.write("View \"CurrentUnitVectors\" {\n")
outputVec.write("VP (" + "%.5f"%0 + "," + "%.5f"%0 + ",0) {" + "%.5f"%vx_A + "," + "%.5f"%vy_A + ",0};\n")
outputVec.write("VP (" + "%.5f"%0 + "," + "%.5f"%0 + ",0) {" + "%.5f"%vx_B + "," + "%.5f"%vy_B + ",0};\n")
outputVec.write("VP (" + "%.5f"%0 + "," + "%.5f"%0 + ",0) {" + "%.5f"%vx_C + "," + "%.5f"%vy_C + ",0};\n")
outputVec.write("};")
outputVec.close

# **********************************************


# ****************** Cons Law ******************
if (Msg.GetCommRank() == 0):
  print(colored('Setting up conservation law ...',"red"))

claw = dgConservationLawShallowWater2d()
solution = dgDofContainer(groups, claw.getNbFields())

# **********************************************
# ** OPTIONAL 1/3: Load an existing DC as the initial solution DC

if (startFromExistingSolution == 1):
  solution.readMsh( solutionFileName )

# **********************************************

ld = functionConstant(0)

#Cd = functionC(glib, "bottomDrag_old", 1, [solution.getFunction(), bathDC.getFunction()])
Cd = functionC(glib, "bottomDrag", 1, [ solution.getFunction(), bathDC.getFunction(), reefsDC.getFunction() ]) #*** 1/5
if(gbrDiff == 0):
  Di = functionConstant(diff)
elif(gbrDiff == 1):
  Di = functionC(glib, "smagorinsky", 1, [solution.getFunctionGradient()]) #***
Coriolis = functionC(glib, "coriolis", 1, [XYZ])
if(gbrWind == 0):
  source = functionConstant([0.0, 0.0])
elif(gbrWind == 1):
  source = functionC(glib, "windStress", 2, [windUV, solution.getFunction(), bathDC.getFunction()]) #***

claw.setCoriolisFactor(Coriolis)
claw.setQuadraticDissipation(Cd)
claw.setLinearDissipation(ld)
claw.setDiffusivity(Di)
claw.setSource(source)
claw.setBathymetry(bathDC.getFunction())
claw.setBathymetryGradient(bathDC.getFunctionGradient())

tideuv = functionC(glib,"transport2velocity",3,[tideUV,bathDC.getFunction(),solution.getFunction()]) #***
tpxoTide = functionC(glib,"merge",3,[tideEta,tideuv]);

CurrentsAndTideNorth = functionC(glib,"merge3",3,[tideEta, tideuv, currentUV_North])
CurrentsAndTideCentral = functionC(glib,"merge3",3,[tideEta, tideuv, currentUV_Central])
CurrentsAndTideSouth = functionC(glib,"merge3",3,[tideEta, tideuv, currentUV_South])

if(gbrResidual == 0):
  if(gbrTide == 0):
    claw.addBoundaryCondition('Open Sea North',claw.new0FluxBoundary())
    claw.addBoundaryCondition('Open Sea Central',claw.new0FluxBoundary())
    claw.addBoundaryCondition('Open Sea South',claw.new0FluxBoundary())
  elif(gbrTide == 1):
    claw.addBoundaryCondition('Open Sea North',claw.newOutsideValueBoundary("Surface", tpxoTide))
    claw.addBoundaryCondition('Open Sea Central',claw.newOutsideValueBoundary("Surface", tpxoTide))
    claw.addBoundaryCondition('Open Sea South',claw.newOutsideValueBoundary("Surface", tpxoTide))
elif(gbrResidual == 1):
  if(gbrTide == 0):
    claw.addBoundaryCondition('Open Sea North',claw.newOutsideValueBoundary("Surface", currentUV_North))
    claw.addBoundaryCondition('Open Sea Central',claw.newOutsideValueBoundary("Surface", currentUV_Central))
    claw.addBoundaryCondition('Open Sea South',claw.newOutsideValueBoundary("Surface", currentUV_South))
  elif(gbrTide == 1):
    claw.addBoundaryCondition('Open Sea North', claw.newOutsideValueBoundary("Surface", CurrentsAndTideNorth))
    claw.addBoundaryCondition('Open Sea Central', claw.newOutsideValueBoundary("Surface", CurrentsAndTideCentral))
    claw.addBoundaryCondition('Open Sea South', claw.newOutsideValueBoundary("Surface", CurrentsAndTideSouth))

claw.addBoundaryCondition('Coast',claw.newBoundaryWall())
claw.addBoundaryCondition('Island',claw.newBoundaryWall())
claw.addBoundaryCondition('Islands',claw.newBoundaryWall())

# **********************************************
#NB: Must make 1st argument of Cd solution.getFunction() to use the following:
bottomDragDC = dgDofContainer(groups, 1)
bottomDragDC.interpolate(Cd)
exporterQuadDissip=dgIdxExporter(bottomDragDC, 'GBR_Functions/quadDissipation_'+filename)
exporterQuadDissip.exportIdx(0,0)

# ****************** Time Int ******************
#WARNING: in MultiRate, all DofContainers are split into groups
#       ---> Must add every DC used by claw to splitForMultiRate!!!
if (Msg.GetCommRank() == 0):
  print(colored('Setting up time integrator ...',"red"))

rk = dgERK(claw, None, DG_ERK_22)
dt = rk.computeInvSpectralRadius(solution) * 1 

#OPTIONAL 2/3: if starting from existing solution, set dt = old_dt
if (startFromExistingSolution == 1):
  dt = old_dt
# **********************************************

currentFunction = functionC(glib,"current",3,[solution.getFunction()]) #***
tideCurrentFunction = functionC(glib,"current",3,[tpxoTide])

nbSteps = int(ceil((Tf-Ti)/dt))
nbExport = 0

if(Msg.GetCommRank() == 0):
  if(os.path.exists(outputDir+'/mr-gbr')):
    try : os.system("rm -r "+outputDir+'/mr-gbr');
    except: 0;
exporterSol=dgIdxExporter(solution, outputDir+'/mr-gbr')
#exporterSol=dgIdxExporter(currentFunction, outputDir+'/mr-gbr-func')

norm=solution.norm()

if (Msg.GetCommRank() == 0):
  print ('')
  print(colored('******************** Initialising ********************', "blue"))
  print ('')
  print( '     - Number of processors:', Msg.GetCommSize())
  print( '     - Initial Time (Ti):',  printDate(Ti))
  print( '     - Final Time (Tf):',  printDate(Tf))
  print( '     - Simulation Length (days):',  (Tf-Ti)/3600.0/24.0  )
  print( '     - Reference Time Step:', printTime(dt), ', which is', str(dt), 's')
  print( '     - Number of Iterations:', nbSteps)
  print( '     - Norm of Solution at Ti:',  norm)
  print( '')
  print ('     - Tides:', gbrTide, ' || Wind:', gbrWind, ' || Residual currents:', gbrResidual)
  print( '')
  print( '     - Output Directory:', outputDir)
  print( '')
  print((colored('******************************************************', "blue")))
  print( '')
  
  #Write info file for LPTracker
  simInfo = [str(dt)+'\n', str(nbSteps)+'\n', str(export)+'\n', str(Ti)+'\n', str(outputDir)+'\n', str(filename)]
  simInfoFile = open(outputDir+'/simulationInfo.txt','w')
  simInfoFile.writelines(simInfo)
  simInfoFile.close()


# **********************************************
exporterSol.exportIdx(0, t)
# ****************** Iterate  ******************
t_exportStart = Ti + t_exportStart
startcpu = time.clock()

# OPTIONAL 3/3: if starting from an initial solution, adjust start & end steps accordingly
firstStep = 1
lastStep = 1
if (startFromExistingSolution == 1):
  firstStep = stepNbOfInitialSolution + 1
  lastStep = stepNbOfInitialSolution + nbSteps + 1
else:
  firstStep = 1
  lastStep = nbSteps + 1

for i in range(firstStep, lastStep):
  if(i == lastStep-1):
    dt=Tf-t
  norm = rk.iterate (solution, dt, t)
  t = t +dt
  timeFunction.set(t)
  if ( i%iter == 0 ):
     norm=solution.norm()
     if(Msg.GetCommRank() == 0):
       print('')
       print(colored('|ITER| %d of %d',  "red")%(i,lastStep))
       print(colored('------------------------------------------------------', "red"))
       print('|TIME|', printDate(t))
       print('|TIME ELAPSED|', printTime((t - Ti)))
       print('| DT |','%.2f'%(dt))
       print('|NORM|','%.4f'%(norm))
       print('|CPUT|',printTime(time.clock() - startcpu))
       print(colored('------------------------------------------------------', "red"))
       print('')
  if ( i%export  == 0 and t > t_exportStart ):
    #solution.exportFunctionMsh(currentFunction, outputDir+'/Current/current-%06d'%(i), t, nbExport, '')
    #solution.exportMsh(outputDir+"/Solution/sol-%06d"%(i),t,nbExport)
    exporterSol.exportIdx(i, t)

endcpu=time.clock()
norm=solution.norm()
Msg.Barrier()
if (Msg.GetCommRank() == 0):
  print( '')
  print(colored('********************     End      ********************', "blue"))
  print( '')
  print( '     - Final Time (Tf):',  printDate(t))
  print( '     - Norm of Solution at Tf:',  norm)
  print( '     - Total CPU Time:', endcpu-startcpu)
  print( '')
  print(colored('******************************************************', "blue"))
  print( '')
Msg.Exit(0)
