/*
 * General library of functions for GBR simulation.
 *  Part 1: "Numerical & computational" functions
 *  Part 2: "Physical" functions
 */

#include "fullMatrix.h"
#include "function.h"
#include "dgGroupOfElements.h"
#include "math.h"
#include "polynomialBasis.h"
#include "dgMeshJacobian.h"

static double pOx, pOy, pOz, pPhiX, pPhiY, pPhiZ, pThetaX, pThetaY, pThetaZ, R;
static bool init = false;

static void initialize() {
  double phi = 146.5892/180*M_PI;
  double theta = -17.147569/180*M_PI;
  R = 6.371e6;
  pOx = cos(theta)*cos(phi)*R;
  pOy = cos(theta)*sin(phi)*R;
  pOz = sin(theta)*R;
  pPhiX = -sin(phi);
  pPhiY = cos(phi);
  pPhiZ = 0;
  pThetaX = -sin(theta)*cos(phi);
  pThetaY = -sin(theta)*sin(phi);
  pThetaZ = cos(theta);
  init = true;
}

extern "C" {

//Numerical & computational functions:


// void lonLatToStereo (dataCacheMap *, fullMatrix<double> &lonlat, fullMatrix<double> &stereo) {
// {
//   double pi;
//   pi = acos(-1.0);
//    
//   //geo coords --> S.P.C.
//   double pointTheta = 90.0 - geoLat;
//   double pointPhi = geoLon;
// 
//   //S.P.C. --> Cartesian(on sphere)
//   pointCart[0] = R * sin(pointTheta*pi/180.0) * cos(pointPhi*pi/180.0);
//   pointCart[1] = R * sin(pointTheta*pi/180.0) * sin(pointPhi*pi/180.0);
//   pointCart[2] = R * cos(pointTheta*pi/180.0);
// 
//   //Cartesian(sphere) --> Stereographic
//   pointStereo[0] = (2 * R * pointCart[0]) / (R + pointCart[2]);
//   pointStereo[1] = (2 * R * pointCart[1]) / (R + pointCart[2]);
//   pointStereo[2] = 0;
// }
void stereoToLonLatDegrees(dataCacheMap *, fullMatrix<double> &lonLat, fullMatrix<double> &XYZ){
  if (! init) initialize();
  for (size_t i = 0; i < lonLat.size1(); i++) {
    double xi = XYZ(i,0);
    double zeta = XYZ(i,1);
    double x = 4*R*R*xi/(4*R*R +xi*xi + zeta*zeta);
    double y = 4*R*R*zeta/(4*R*R +xi*xi + zeta*zeta);
    double z = R *(4*R*R-xi*xi-zeta*zeta)/(4*R*R+xi*xi + zeta*zeta);
    double lon = atan2(y,x)*180/M_PI;
    double lat = asin(z/R)*180/M_PI;
    lonLat.set(i,0,lon);
    lonLat.set(i,1,lat);
    lonLat.set(i,2,0.);
  }
}

void stereoToLatLonDegrees(dataCacheMap *, fullMatrix<double> &lonLat, fullMatrix<double> &XYZ){
  if (! init) initialize();
  for (size_t i = 0; i < lonLat.size1(); i++) {
    double xi = XYZ(i,0);
    double zeta = XYZ(i,1);
    double x = 4*R*R*xi/(4*R*R +xi*xi + zeta*zeta);
    double y = 4*R*R*zeta/(4*R*R +xi*xi + zeta*zeta);
    double z = R *(4*R*R-xi*xi-zeta*zeta)/(4*R*R+xi*xi + zeta*zeta);
    double lon = atan2(y,x)*180/M_PI;
    double lat = asin(z/R)*180/M_PI;
    lonLat.set(i,1,lon);
    lonLat.set(i,0,lat);
    lonLat.set(i,2,0.);
  }
}

void stereoToLonLat(dataCacheMap *, fullMatrix<double> &lonLat, fullMatrix<double> &XYZ){
  if (! init) initialize();
  for (size_t i = 0; i < lonLat.size1(); i++) {
    double xi = XYZ(i,0);
    double zeta = XYZ(i,1);
    double x = 4*R*R*xi/(4*R*R +xi*xi + zeta*zeta);
    double y = 4*R*R*zeta/(4*R*R +xi*xi + zeta*zeta);
    double z = R *(4*R*R-xi*xi-zeta*zeta)/(4*R*R+xi*xi + zeta*zeta);
    double lon = atan2(y,x);
    double lat = asin(z/R);
    lonLat.set(i,0,lon);
    lonLat.set(i,1,lat);
    lonLat.set(i,2,0.);
  }
}

void lonLat (dataCacheMap *, fullMatrix<double> &lonlat, fullMatrix<double> &xyz) {
  if (! init) initialize();
  for (size_t i = 0; i< lonlat.size1(); i++) {
    double x = pPhiX * xyz(i,0) + pThetaX * xyz(i,1) + pOx;
    double y = pPhiY * xyz(i,0) + pThetaY * xyz(i,1) + pOy;
    double z = pPhiZ * xyz(i,0) + pThetaZ * xyz(i,1) + pOz;
    double r = sqrt(x*x+y*y+z*z);
    lonlat.set(i, 0, atan2(y,x));
    lonlat.set(i, 1, asin(z/r));
    lonlat.set(i, 2, 0);
  }
}

void latLon (dataCacheMap *, fullMatrix<double> &lonlat, fullMatrix<double> &xyz) {
  if (! init) initialize();
  for (size_t i = 0; i< lonlat.size1(); i++) {
    double x = pPhiX * xyz(i,0) + pThetaX * xyz(i,1) + pOx;
    double y = pPhiY * xyz(i,0) + pThetaY * xyz(i,1) + pOy;
    double z = pPhiZ * xyz(i,0) + pThetaZ * xyz(i,1) + pOz;
    double r = sqrt(x*x+y*y+z*z);
    lonlat.set(i, 0, asin(z/r));
    lonlat.set(i, 1, atan2(y,x));
    lonlat.set(i, 2, 0);
  }
}

void lonLatDegrees (dataCacheMap *, fullMatrix<double> &lonlat, fullMatrix<double> &xyz) {
	if (! init) initialize();
	for (size_t i = 0; i< lonlat.size1(); i++) {
		double x = pPhiX * xyz(i,0) + pThetaX * xyz(i,1) + pOx;
		double y = pPhiY * xyz(i,0) + pThetaY * xyz(i,1) + pOy;
		double z = pPhiZ * xyz(i,0) + pThetaZ * xyz(i,1) + pOz;
		double r = sqrt(x*x+y*y+z*z);
		lonlat.set(i, 0, atan2(y,x)*180/M_PI);
		lonlat.set(i, 1, asin(z/r)*180/M_PI);
		lonlat.set(i, 2, 0);
	}
}

void latLonDegrees (dataCacheMap *, fullMatrix<double> &lonlat, fullMatrix<double> &xyz) {
	if (! init) initialize();
	for (size_t i = 0; i< lonlat.size1(); i++) {
		double x = pPhiX * xyz(i,0) + pThetaX * xyz(i,1) + pOx;
		double y = pPhiY * xyz(i,0) + pThetaY * xyz(i,1) + pOy;
		double z = pPhiZ * xyz(i,0) + pThetaZ * xyz(i,1) + pOz;
		double r = sqrt(x*x+y*y+z*z);
		lonlat.set(i, 1, atan2(y,x)*180/M_PI);
		lonlat.set(i, 0, asin(z/r)*180/M_PI);
		lonlat.set(i, 2, 0);
	}
}

void lonLatVector(dataCacheMap *, fullMatrix<double> &uv, fullMatrix<double> &lonlat, fullMatrix<double> &u, fullMatrix<double> &v){
	for (size_t i = 0; i< lonlat.size1(); i++) {
		double lon=lonlat(i,0);
		double lat=lonlat(i,1);
		double uLon=u(i,0);
		double uLat=v(i,0);
    double lon0=146.5892*M_PI/180;
    double lat0=-17.147569*M_PI/180;
    double u1= -uLat*sin(lat)*cos(lon)-uLon*sin(lon);
    double u2= -uLat*sin(lat)*sin(lon)+uLon*cos(lon);
    double u3= uLat*cos(lat);
    double alpha1= -sin(lon0);
    double alpha2= cos(lon0);
    double alpha3=0;
    double beta1=-sin(lat0)*cos(lon0);
    double beta2=-sin(lat0)*sin(lon0);
    double beta3=cos(lat0);
		uv.set(i, 0, u1*alpha1+u2*alpha2+u3*alpha3);
		uv.set(i, 1, u1*beta1+u2*beta2+u3*beta3);
		uv.set(i, 2,  0);
	}
}

void latLonVector(dataCacheMap *, fullMatrix<double> &uv, fullMatrix<double> &latlon, fullMatrix<double> &u, fullMatrix<double> &v){
	for (size_t i = 0; i< latlon.size1(); i++) {
		double lat=latlon(i,0);
		double lon=latlon(i,1);
		double uLat=u(i,0);
		double uLon=v(i,0);
    double lon0=146.5892*M_PI/180;
    double lat0=-17.147569*M_PI/180;
    double u1= -uLat*sin(lat)*cos(lon)-uLon*sin(lon);
    double u2= -uLat*sin(lat)*sin(lon)+uLon*cos(lon);
    double u3= uLat*cos(lat);
    double alpha1= -sin(lon0);
    double alpha2= cos(lon0);
    double alpha3=0;
    double beta1=-sin(lat0)*cos(lon0);
    double beta2=-sin(lat0)*sin(lon0);
    double beta3=cos(lat0);
		uv.set(i, 0, u1*alpha1+u2*alpha2+u3*alpha3);
		uv.set(i, 1, u1*beta1+u2*beta2+u3*beta3);
		uv.set(i, 2,  0);
	}
}

void transport2velocity(dataCacheMap *,fullMatrix<double> &uv, fullMatrix<double> &UV, fullMatrix<double> &bath, fullMatrix<double> &solution){
  for (size_t i = 0; i< solution.size1(); i++) {
    uv.set(i,0,UV(i,0)/(bath(i,0)+solution(i,0))); 
    uv.set(i,1,UV(i,1)/(bath(i,0)+solution(i,0))); 
    uv.set(i,2,UV(i,2)/(bath(i,0)+solution(i,0))); 
  }
}

void computeEl(dataCacheMap *,fullMatrix<double> &val,fullMatrix<double> &solution) {
  for (size_t i = 0; i< val.size1(); i++) {
    val.set(i, 0, solution(i,0)); 
  }
}

void computeVel(dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &solution){
  for (size_t i = 0; i< val.size1(); i++) {
    val.set(i, 0, solution(i,1)); 
    val.set(i, 1, solution(i,2)); 
  }
}

void computeErrorEl(dataCacheMap *,fullMatrix<double> &err,fullMatrix<double> &solution1, fullMatrix<double> &solution2) {
  for (size_t i = 0; i< err.size1(); i++) {
    err.set(i, 0, solution1(i,0)-solution2(i, 0)); 
  }
}

void computeErrorVel(dataCacheMap *, fullMatrix<double> &err, fullMatrix<double> &solution1, fullMatrix<double> &solution2){
  for (size_t i = 0; i< err.size1(); i++) {
    err.set(i, 0, solution1(i,1)-solution2(i, 1)); 
    err.set(i, 1, solution1(i,2)-solution2(i, 2)); 
  }
}

void merge(dataCacheMap *,fullMatrix<double> &sol,fullMatrix<double> &eta, fullMatrix<double> &uv){
  for (size_t i = 0; i< sol.size1(); i++) {
    sol.set(i,0,eta(i,0)); 
    sol.set(i,1,uv(i,0));
    sol.set(i,2,uv(i,1));
  }
}

void merge2(dataCacheMap *,fullMatrix<double> &sol,fullMatrix<double> &u1, fullMatrix<double> &u2){
  for (size_t i = 0; i < sol.size1(); i++) {
    sol.set(i, 0, u1(i,0)); 
    sol.set(i, 1, u2(i,0)); 
  }
}

void merge3(dataCacheMap *,fullMatrix<double> &sol,fullMatrix<double> &eta, fullMatrix<double> &uv, fullMatrix<double> &currentUV){
  for (size_t i = 0; i< sol.size1(); i++) {
    sol.set(i,0,eta(i,0)); 
    sol.set(i,1,uv(i,0) + currentUV(i,1));
    sol.set(i,2,uv(i,1) + currentUV(i,2)); 
  }
}

//Function to merge shallow (hard) and deep (soft) DCs
void overwrite2(dataCacheMap *,fullMatrix<double> &sol, fullMatrix<double> &bath, fullMatrix<double> &shallowDC, fullMatrix<double> &deepDC, fullMatrix<double> &nbOfShallowReefs){
  for (size_t i = 0; i < sol.size1(); i++) {
    if (shallowDC(i,0) > 0.1) sol.set(i, 0, shallowDC(i,0));
    else {
      if ((deepDC(i,0) > 0.1) && (bath(i,0) > 6.0)) sol.set(i, 0, deepDC(i,0)+nbOfShallowReefs(0,0));
      else sol.set(i, 0, 0.0);
    }
  }
}

//Physical functions:

void wind (dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< sol.size1(); i++) {
    sol.set(i,0,sin(xyz(i,1)/1e6)/1e6);
    sol.set(i,1,0);
  }
}

void coriolis (dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  double theta = -17.147569/180*M_PI;
  for (size_t i = 0; i< sol.size1(); i++) {
    //sol.set(i,0,2*7.292e-5*xyz(i,2)/6.371e6);
    sol.set(i,0,2*7.292e-5*sin(theta));
  }
}

void bottomDrag_old(dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &sol, fullMatrix<double> &bath){
  for(int i=0;  i<val.size1(); i++){
    double H = sol(i, 0)+bath(i, 0);
    double reef = 1.0;
    if (H < 10.0) reef = 10.0;
    val.set(i, 0, 9.81*reef*0.0235*0.0235/(pow(H, 1.333333333333)));
  }
}

void bottomDrag(dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &sol, fullMatrix<double> &bath, fullMatrix<double> &reefs){
  for(int i=0;  i<val.size1(); i++){
    double reef = 1.0;
    double H = sol(i, 0)+bath(i, 0);
    //Verify if there is a reef or not. 
    //  Reef --> multiply normal bottom drag by factor 10x
    int reefYesOrNo = int(reefs(i, 0));
//    printf("i is %d. reefYesOrNo: %d, reefs(i,0): %f\n",i, reefYesOrNo, reefs(i,0));
    if (reefYesOrNo > 0) reef = 10.0;
    val.set(i, 0, 9.81*reef*0.0235*0.0235/(pow(H, 1.333333333333)));
  }
}

void smagorinsky(dataCacheMap *map, fullMatrix<double> &val, fullMatrix<double> &solGradient){
  const dgGroupOfElements *g = map->getGroupOfElements();
  int elNum = map->getElementId();
  const dgMeshJacobian &jaco = map->getJacobians();
  const fullVector<double> &elRadi = jaco.elementInnerRadius(g->elementVectorId());
  double radi = elRadi(elNum);
  for(int i=0;  i<val.size1(); i++){
    double dudx = solGradient(i, 3);
    double dudy = solGradient(i, 4);
    double dvdx = solGradient(i, 6);
    double dvdy = solGradient(i, 7);
    val(i, 0)= pow_int(0.1*radi, 2)*sqrt(2*dudx*dudx+2*dvdy*dvdy+pow_int(dudy+dvdx, 2));
  }
}

void current (dataCacheMap *, fullMatrix<double> &current, fullMatrix<double> &solution) {
  for (size_t i = 0; i< current.size1(); i++) {
    current.set(i, 0, solution(i,1));
    current.set(i, 1, solution(i,2));
    current.set(i, 2, 0);
  }
}

void current2 (dataCacheMap *, fullMatrix<double> &current, fullMatrix<double> &solution) {
  for (size_t i = 0; i< current.size1(); i++) {
    current.set(i, 0, solution(i,0));
    current.set(i, 1, solution(i,1));
    current.set(i, 2, 0.0);
  }
}

void windStress (dataCacheMap *,fullMatrix<double> &windstress,fullMatrix<double> &winduv, fullMatrix<double> &sol, fullMatrix<double> &bath) {
  for (size_t i = 0; i< windstress.size1(); i++) {
    double normWind=sqrt(winduv(i, 0)*winduv(i, 0)+winduv(i, 1)*winduv(i, 1));
    double rho=1025;
    double sol1=0.001*(0.630*normWind+0.066*normWind*normWind)*winduv(i, 0)/(rho*(sol(i, 0)+bath(i, 0)));
    double sol2=0.001*(0.630*normWind+0.066*normWind*normWind)*winduv(i, 1)/(rho*(sol(i, 0)+bath(i, 0)));
    windstress.set(i, 0, sol1);
    windstress.set(i, 1, sol2);
  }
}

void bathVec(dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &bathymetry, fullMatrix<double> &normals, fullMatrix<double> &boundaryData) {
  double vx, vy;
  vx = boundaryData(0,1);
  vy = boundaryData(0,2);
  for (size_t i=0; i<bathymetry.size1(); i++) {
    sol.set(i, 0, bathymetry(i,0) * (vx*normals(i,0) + vy*normals(i,1)) );
  }
}

void SEC_UV(dataCacheMap *, fullMatrix<double> &UVout, fullMatrix<double> &bathInterfaceIntegrals, fullMatrix<double> &normals, fullMatrix<double> &boundaryData) {
  double F, c, vx, vy;
  F = boundaryData(0,0);
  vx = boundaryData(0,1);
  vy = boundaryData(0,2);
  c = F / bathInterfaceIntegrals(0,0);
  for (size_t i=0; i<UVout.size1(); i++) {
  	UVout.set(i, 0, 0);	//Eta
    UVout.set(i, 1, c * vx);
    UVout.set(i, 2, c * vy);
	}
}
}
