//GBR Cartesian coords with extended shelf - Oct 2012
//NB: had to manually remove open sea edge points from Field1 attractor lis (IP + 6550:6630)
Include "gbr_nov2012_data.geo";
Euclidian Coordinates;
Include "Reefs_vertices.geo";
//Field 2 is Reefs_vertices attractor

//Field 51: Islands threshold
Field[51] = Threshold;
Field[51].IField = 1;
Field[51].DistMin = 600;
Field[51].DistMax = 20000;
Field[51].LcMin = 800;
Field[51].LcMax = 8000;

//Field 53: Reef contours threshold
Field[53] = Threshold;
Field[53].IField = 2;
Field[53].DistMin = 200;
Field[53].DistMax = 22000;
Field[53].LcMin = 400;
Field[53].LcMax = 5000;
//Field[53].StopAtDistMax = 1;

//Fields 6-9: Bathymetry
Field[6] = Structured;
Field[6].FileName = "./full.bin";
Field[7] = LonLat;
Field[7].IField = 6;
Field[8] = Threshold;
Field[8].DistMax = -8;
Field[8].DistMin = -200;
Field[8].IField = 7;
Field[8].LcMax = 0;
Field[8].LcMin = 1;
Field[9] = MathEval;
Field[9].F = "640+3000*Sqrt(F8)";

//Field 100: Minimum field
Field[100] = Min;
Field[100].FieldsList = {51, 53, 9};

//Gaussian:
//Euclidian Coordinates;
Field[10] = MathEval;
//Good sma-mesh fit:
//Field[40].F = "10*exp(-((x+5150000)*(x+5150000))/(2*240000*240000) - ((y-3062000)*(y-3062000))/(2*240000*240000) - (z+2188000)*(z+2188000)/(2*500000*500000))";
//Whole SGBR:
Field[10].F = "10*exp(-((x+5150000)*(x+5150000))/(560000*560000) - ((y-2982000)*(y-2982000))/(2*780000*780000) - (z+2188000)*(z+2188000)/(2*600000*600000))";
//Field[10].F = "10";

//Field 11: Normalised Gaussian
Field[11] = Threshold;
Field[11].IField = 10;
Field[11].LcMax = 1.0;
// Field[11].LcMin = 0.16;
Field[11].LcMin = 1.0;
Field[11].DistMax = 7;
Field[11].DistMin = 4;

//Field 500: F100 divided by Gaussian
Field[500] = MathEval;
Field[500].F = "F100/F11";
//Set maximum element size
Field[499] = MathEval;
Field[499].F = "5000";

//Set final field
Field[502] = Min;
Field[502].FieldsList = {500, 499};
Background Field = 502;

Mesh.CharacteristicLengthExtendFromBoundary = 0;
Mesh.CharacteristicLengthFromPoints = 0;
Mesh.LcIntegrationPrecision = 1e-3;
Mesh.Algorithm = 5;
//Mesh.MinimumCurvePoints = 4; //<-- Comment this out to lose small islands

