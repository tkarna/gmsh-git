from dgpy import *
from ProjectMesh import *
import calendar, os, time
from math import sqrt

#================
# 1. CHOOSE MESH
#----------------
#mesh = "2K"
#mesh = "8K_nov2012"
mesh = "460K_nov2012"

#SET RADIUS: 6.37101e6 for 'sma_zoomed', else 6.371e6
R = 6.371e6
#================

#================
# 2. SET FILE NAME & OUTPUT DIRECTORY
#----------------
filename = "gbr"+mesh
outputDir = "output"
#================

#================
#3. SET BATHYMETRY & REEF MAP DIRECTORIES
#----------------
bathdir=outputDir+"/Bath/"+filename+"_bath_smooth"
bathname=outputDir+"/Bath/"+filename+"_bath_smooth"+"/"+filename+"_bath_smooth.idx"

reefMapFile="Data/ReefMap_9200.dat"
#================

#================
#4. SET EXPORT PARAMETERS
#----------------
# Print every "iter" iterations, export every "export" iterations, export from t=t_exportStart
iter = 1
export = 100
export_fnEval = 100
t_exportStart = 0 # 6*24*3600.0
#================

# Mesh Parameters
order = 1
dimension = 2

#================
# 5. SET INITIAL AND FINAL TIME [year, month, day, hour, minute, second]
#----------------
Ti = calendar.timegm([2007, 11, 28, 0, 0, 0])
#Tf = calendar.timegm([2000, 4, 1, 1, 0, 0])
Tf = Ti + 7*24*3600.0
#================

#================
# 6. OPTIONAL: LOAD INITIAL SOLUTION FROM EXISTING FILEi
# NB: must also set, above, Ti = Ti of old simulation
#----------------
# startFromExistingSolution: 0 if no, 1 if yes
startFromExistingSolution = 0

stepNbOfInitialSolution = 53360
solutionFileName = '/gbr-%06d'%( stepNbOfInitialSolution )+'.idx'
old_dt = 1
if (startFromExistingSolution == 1):
  Ti = Ti + stepNbOfInitialSolution*old_dt
  Tf = Ti + 7*24*3600
#================

#================
# 7. OTHER PARAMETERS
#----------------
# Radius of the Earth (for stereo projection)
EarthRadius = 6371e3

# Multirate and Partition Parameters
mL = 1
algo = 1

"""
Number of Refinement Levels for the Multirate Method
"""
mrMl=1000       #SingleRate: set mL = 1


# Physical parameters for bathymetry smoothing
bNu=20    # Diffusion
bT=10000  # Time for smoothing 
#================

#================
# 8. PHYSICAL PARAMETERS FOR GBR SIMULATION
#----------------
"""
-gbrDiff= 0: Constant Diffusion diff || 1: Parametrization of Smagorinsky 
"""
gbrDiff=1
diff=1
#================

#================
# 9. SET FORCINGS
#----------------
"""
-gbrWind= 0: NO wind  ||  1: netCDF data
"""
gbrWind=1

"""
gbrTide= 0: NO tide  ||  1: tpxo data
"""
gbrTide=1

"""
gbrResidual= 0: NO residual current  ||  1: standard NCJ forcing
"""
gbrResidual=0

#Define residual current fluxes (North, Central, South)
F_A = 3.0e5
F_B = -10.0e5
F_C = 7.0e5

#Define current direction vectors at boundary in stereographic coordinates
vx_A_stereo = 0.76
vy_A_stereo = -sqrt(1 - vx_A_stereo*vx_A_stereo)
vx_B_stereo = 0.06
vy_B_stereo = -sqrt(1 - vx_B_stereo*vx_B_stereo)
vx_C_stereo = -0.98
vy_C_stereo = -sqrt(1 - vx_C_stereo*vx_C_stereo)

#Define current direction vectors at boundary
vx_A = 0.0
vy_A = sqrt(1 - vx_A*vx_A)
vx_B = 0.76
vy_B = sqrt(1 - vx_B*vx_B)
vy_C = -0.7
vx_C = sqrt(1 - vy_C*vy_C)
#================

