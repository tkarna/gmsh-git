#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <png.h>

void error() {
  printf("usage : bin2pbm input.bin output.png [vmin] [vmax]\n");
  exit(1);
}

int main(int argc, char *argv[]) {
  int i,j;

  //process arguments
  if (argc < 3) error();
  char* inputFilename = argv[1];
  char* outputFilename = argv[2];
  if(strlen(outputFilename) < 4 || strcmp(outputFilename + strlen(outputFilename) - 4, ".png") != 0) {
    printf("output file extension should be '.png'");
    exit(1);
  }
  char *headerFilename = strdup(outputFilename);
  headerFilename[strlen(headerFilename) - 3] = '\0';
  strcat(headerFilename, "hdr");
  printf("Input file: %s\nOutput file: %s\nHeader: %s\n", inputFilename, outputFilename, headerFilename);

  //read bin file
  FILE *input = fopen(inputFilename, "rb");
  double buf, O[3], d[3];
  int n[3];
  fread(O, sizeof(double), 3, input);
  fread(d, sizeof(double), 3, input);
  fread(n, sizeof(int), 3, input);
  double *data = malloc(sizeof(double) * n[0] * n[1]);
  fread(data, sizeof(double), n[0] * n[1], input);
  fclose(input);

  //get bounds
  double vmin, vmax;
  if (argc > 3) {
    if (sscanf(argv[3], "%le", &vmin) != 1) error();
  }
  else {
    vmin = data[0];
    for (i = 0; i < n[0] * n[1]; ++i)
      vmin = fmin(vmin, data[i]);
  }
  if (argc > 4) {
    if (sscanf(argv[4], "%le", &vmax) != 1) error();
  }
  else {
    vmax = data[0];
    for (i = 0; i < n[0] * n[1]; ++i)
      vmax = fmax(vmax, data[i]);
  }
  printf("vmin = %g, vmax = %g\n", vmin, vmax);

  //convert double to unsigned short int
  unsigned long int NLEVEL = ((unsigned long int)2<<15);
  unsigned short int *outdata = malloc(n[0] * n[1] * sizeof(short int));
  for (i = 0; i < n[0]; ++i) {
    for (j = 0; j < n[1]; ++j) {
      double v = fmin(1., fmax(0., (data[i * n[1] + j] - vmin) / (vmax - vmin)));
      outdata[j * n[0] + i] = v * (NLEVEL - 1);
    }
  }

  //write png
  png_image image = {0};
  image.version = PNG_IMAGE_VERSION;
  image.opaque = NULL;
  image.width = n[0];
  image.height = n[1];
  image.format = PNG_FORMAT_GRAY | PNG_FORMAT_FLAG_LINEAR;
  image.flags = 0;
  png_image_write_to_file(&image, outputFilename, 0, outdata, -n[0], NULL);
  png_image_free(&image);

  //write header
  FILE *header = fopen(headerFilename, "w");
  fprintf(header, "%.16e %.16e %.16e %.16e %.16e %.16e %.16e %.16e\n", O[0], O[1], O[2], d[0], d[1], d[2], vmin, vmax);
  fclose(header);
  return 0;
}
