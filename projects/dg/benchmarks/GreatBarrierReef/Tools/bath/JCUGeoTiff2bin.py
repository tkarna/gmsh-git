#Script to generate a bathymetry file in gmsh format (binary or text) from geographic tiff format.
#Data source: Project 3DGBR (R. Beaman/JCU) geotiff file: http://e-atlas.org.au/content/gbr-jcu-bathymetry-gbr100
#Change tiff filename on line 10

from sys import *
from math import *
from numpy import * 
from osgeo import gdal
from scipy import *
import matplotlib.pyplot as plt

print ('Opening tiff file ...')
ds = gdal.Open("./GBR_JCU_Bathymetry-3DGBR_v1.0.tif", gdal.GA_ReadOnly)
(X, deltaX, rotation, Y, rotation, deltaY) = ds.GetGeoTransform()
srs_wkt = ds.GetProjection()
Nx = ds.RasterXSize
Ny = ds.RasterYSize
print ('X is', X, ', Y is', Y)
print ('dx is ', deltaX, ', dy is ', deltaY)
print ('rotation is', rotation)
print ('')

X = (X*pi)/180.0
Y = (Y*pi)/180.0
deltaX = (deltaX*pi)/180.0
deltaY = (deltaY*pi)/180.0

headerArray1 = array([X, Y+Ny*deltaY, -1.0, deltaX, -deltaY, 2.0],'float64')
headerArray2 = array([Nx, Ny, 1],'int32')

band = ds.GetRasterBand(1)
print ('Band Type=',gdal.GetDataTypeName(band.DataType))
min = band.GetMinimum()
max = band.GetMaximum()
if min is None or max is None:
  (min,max) = band.ComputeRasterMinMax(1)
print ('Min=%.3f, Max=%.3f' % (min,max))

if band.GetOverviewCount() > 0:
  print ('Band has ', band.GetOverviewCount(), ' overviews.')

if not band.GetRasterColorTable() is None:
  print ('Band has a color table with ', \
  band.GetRasterColorTable().GetCount(), ' entries.')

print ('GeoTiff details:')
print (headerArray1)
print (headerArray2)

dataArray = array([],'float64')
print ('')
print ('Now starting the hard part ...')
dataArray = ds.GetRasterBand(1).ReadAsArray(0,0,Nx,Ny)
print ('dataArray is of size:', dataArray.size)
print ('Nx * Ny =', Nx*Ny)
print ('dataArray is of type:', dataArray.dtype)
print ('dataArray shape is:', dataArray.shape)
print ('Flip & convert array to 64-bit ...')

dA2 = dataArray.transpose()
dataArray = flipud(dataArray)
dataArray = dataArray.astype(float64)

dataArray = dataArray.transpose()
print("header 2", headerArray2)
print(dataArray.shape)
#shape = (14989,13010)
#dataArray = dataArray.reshape(shape)
#dataArray = resample(dataArray, len(dataArray)/4.0)

print ('dataArray is of type:', dataArray.dtype)
print ('dataArray shape is:', dataArray.shape)

print ('Write to file ...')
#savetxt('array.txt', dataArray, fmt='%.10g', delimiter=' ', newline='\n')

#Output to binary file:
outFile = open("gbrbath.bin","wb")
headerArray1.tofile(outFile)
headerArray2.tofile(outFile)
dataArray.tofile(outFile)
outFile.close()
print ('Script finished. Thank you and good night.')
