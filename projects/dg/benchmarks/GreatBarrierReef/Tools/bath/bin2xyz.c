#include <stdlib.h>
#include <stdio.h>
#include <proj_api.h>
#include <float.h>

int main(int argc, char **argv)
{
  int N[3];
  int i, j;
  double d[3], O[3];

  FILE *input = fopen(argv[1], "r");

  fread(O, sizeof(double), 3, input);
  fread(d, sizeof(double), 3, input);
  fread(N, sizeof(int), 3, input);

  double *data = malloc(sizeof(double) * N[0] * N[1] * N[2]);
  fread(data, sizeof(double), N[0] * N[1] * N[2], input);
  fprintf(stderr, "%i points\n", N[0] * N[1]);

  projPJ lonlat = pj_init_plus("+proj=latlong +ellps=WGS84");
  projPJ utm = pj_init_plus("+proj=utm +ellps=WGS84 +zone=55");
  double xMin = DBL_MAX, xMax = -DBL_MAX, yMin = DBL_MAX, yMax = -DBL_MAX;

  for (i = 0; i < N[0]; i++) {
    for (j = 0; j < N[1]; j++) {
      double x = O[0] + i * d[0];
      double y = O[1] + j * d[1];
      pj_transform(lonlat, utm, 1, 1, &x, &y, NULL);
      printf("%.16e %.16e %.16e\n", x, y, data[j + i * N[1]]);
      if (x < xMin) xMin = x;
      if (x > xMax) xMax = x;
      if (y < yMin) yMin = y;
      if (y > yMax) yMax = y;
    }
  }

  pj_free(utm);
  pj_free(lonlat);
  free(data);

  fprintf(stderr, "x0 : % 21.16e xwidth : % 21.16e\n", xMin, xMax - xMin);
  fprintf(stderr, "y0 : % 21.16e ywidth : % 21.16e\n", yMin, yMax - yMin);

  return EXIT_SUCCESS;
}
