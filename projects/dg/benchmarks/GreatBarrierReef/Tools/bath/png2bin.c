#include <png.h>
#include <stdlib.h>
#include <string.h>
int main(int argc, char **argv)
{
  //process arguments
  if (argc != 3) {
    fprintf(stderr, "usage : png2bin input.png output.bin");
    exit(1);
  }
  const char *pngfilename = argv[1];
  const char *binfilename = argv[2];
  //read png
  png_image image = {0};
  image.version = PNG_IMAGE_VERSION;
  if (png_image_begin_read_from_file(&image, pngfilename) != 1) {
    fprintf(stderr, "error : cannot read png file %s\n", pngfilename);
    exit(1);
  }
  image.format = PNG_FORMAT_GRAY |PNG_FORMAT_FLAG_LINEAR;
  printf("size = %u %u\n", image.width, image.height);
  unsigned short int *data = malloc(image.width * image.height * sizeof(unsigned short int));
  png_image_finish_read(&image, NULL, data, -image.width, NULL);
  int nx = image.width;
  int ny = image.height;
  png_image_free(&image);

  //read header
  char *headerfilename = strdup(pngfilename);
  headerfilename[strlen(headerfilename) - 3] = '\0';
  strcat(headerfilename, "hdr");
  FILE *header = fopen(headerfilename, "r");
  double O[3], d[3], vmin, vmax;
  if (fscanf(header, "%le %le %le %le %le %le %le %le", &O[0], &O[1], &O[2], &d[0], &d[1], &d[2], &vmin, &vmax) != 8) {
    fprintf(stderr, "invalid header file '%s'\n", headerfilename);
    exit(1);
  }
  fclose(header);

  //convert data
  int i, j;
  double *outdata = malloc(sizeof(double) * nx * ny);
  for(i = 0; i < nx; ++i) {
    for (j = 0; j < ny; ++j) {
      outdata[i * ny + j] = vmin + (vmax - vmin) * (data[j * nx + i] * 1./65535);
    }
  }

  //write bin
  FILE *output = fopen(binfilename, "wb");
  if (!output){
    fprintf(stderr, "cannot open output file '%s'\n", binfilename);
    exit(1);
  }
  fwrite(O, sizeof(double), 3, output);
  fwrite(d, sizeof(double), 3, output);
  int nn[3] = {nx, ny, 1};
  fwrite(nn, sizeof(int), 3, output);
  fwrite(outdata, sizeof(double), nx * ny, output);
  free(data);
  free(outdata);
  return 0;
}
