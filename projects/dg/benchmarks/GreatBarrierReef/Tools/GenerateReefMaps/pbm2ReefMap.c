//#include<netpbm/pam.h>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

bool pbmb_read_data ( ifstream &input, int xsize, int ysize, int *barray );
bool pbmb_read_header ( ifstream &input, int &xsize, int &ysize );
bool s_eqi ( string s1, string s2 );
int s_len_trim ( string s );
void s_word_extract_first ( string s, string &s1, string &s2 );
char ch_cap ( char ch );

int main(int argc, char *argv[]) {
  
  //NB: Set these manually for appropriate reef map:
  double Ox = 140.921706943421;
  double Oy = -25.0026964546776;
  double dx = 0.00162643790263492;
  double dy = 0.00178435689193745;
  
  char* inputFilename = argv[1];
  char* outputFilename = argv[2];
  
  int xsize, ysize;
  printf("HulloHullo.\n");
  if (argc < 3) { printf("Not enough arguments. arg1: input filename, arg2: output filename.\n"); return 1; }
  printf("Input file: %s; Output file: %s\n", inputFilename, outputFilename);
  
  bool error;
  ifstream input;
  input.open (inputFilename, ios::binary);
//   input.open ( "test.pbm", ios::binary );           //!!!!

  FILE *outfile = fopen(outputFilename, "w");
//   FILE *outfile = fopen("test_modified.dat", "w");              //!!!!
  
  if ( !input )  {
    cout << "\n";
    cout << "PBMB_READ: Fatal error!\n";
    cout << "  Cannot open the input file\n";
    return true;
  }
  printf("Opened input file\n");
//  Read the header.
  error = pbmb_read_header ( input, xsize, ysize );
  if ( error )  {
    cout << "\n";
    cout << "PBMB_READ: Fatal error!\n";
    cout << "  PBMB_READ_HEADER failed.\n";
    return true;
  }
  printf("Read header\n");
//  Allocate storage for the data.
  int* data;
  data = new int [xsize * ysize];
  
//   int *data;
//   data = new int[xsize*ysize];
//   data = (int *)malloc(xsize*ysize*sizeof(int*));
  
//   int** data;
//   data = (int **)malloc(xsize*sizeof(int*));
//    for(int i=0;i<xsize;i++)
//      data[i]=(int*)malloc(ysize*sizeof(int));

  printf("Reading data ...\n");
//  Read the data.
  error = pbmb_read_data ( input, xsize, ysize, data );
  if ( error )
  {
    cout << "\n";
    cout << "PBMB_READ: Fatal error!\n";
    cout << "  PBMB_READ_DATA failed.\n";
    return true;
  }
  printf("Read data\n");
//
//  Close the file.
//
  input.close ( );

  
  //Now write data to Reef Map file
  if (! outfile) { printf("Error: Cannot open Reef Map.\n");return 1; }

  fprintf( outfile, "%.15g %.15g 0.0 %.15g %.15g 0.0 %d %d 0\n", Ox, Oy, dx, dy, xsize, ysize);  

  printf( "Initialising Reef Map data with following values (hard-coded except nx, ny): Ox: %e Oy: %e dx: %e dy: %e Nx: %d Ny: %d\n", Ox, Oy, dx, dy, xsize, ysize);

  for (int j=0; j<ysize; j++) {
     for (int i=0; i<xsize; i++) {
//        cout<<"i: "<<i<<", j: "<<j<<", data: "<<data[xsize*j + i]<<endl;
       fprintf( outfile, "%d ", data[xsize*j + i] );
     }
     fprintf( outfile, "\n" );
   }
   
  fclose(outfile);
  
  printf("Done.\n");
  return 0;
}


bool pbmb_read_data ( ifstream &input, int xsize, int ysize, int *barray )
{
  int bit;
  char c;
  unsigned char c2;
  int i;
  int *indexb;
  int j;
  int k;
  int numbyte;

  indexb = barray;
  numbyte = 0;

  for ( j = 0; j < ysize; j++ )
  {
    for ( i = 0; i < xsize; i++ )
    {
      if ( i%8 == 0 )
      {
        input.read ( &c, 1 );
        
        c2 = ( unsigned char ) c;
        
        if ( input.eof ( ) )
        {
          cout << "\n";
          cout << "PBMB_CHECK_DATA - Fatal error!\n";
          cout << "  Failed reading byte " << numbyte << "\n";
          return true;
        }
        numbyte = numbyte + 1;
      }

      k = 7 - i%8;
      bit = ( c2 >> k )%2;
        
//       cout<<"i: "<<i<<", j: "<<j<<", bit: "<<bit<<endl;
      *indexb = bit;
      indexb = indexb + 1;
    }
  }
  return false;
}


bool pbmb_read_header ( ifstream &input, int &xsize, int &ysize )
{
  int count;
  string line;
  string rest;
  int step;
  int width;
  string word;

  step = 0;

  while ( 1 )
  {
    getline ( input, line );
    cout << "Header line: " << line<<endl;
    
    if ( input.eof ( ) )
    {
      cout << "\n";
      cout << "PBMB_READ_HEADER - Fatal error!\n";
      cout << "  End of file.\n";
      return true;
    }

    if ( line[0] == '#' )
    {
      continue;
    }

    if ( step == 0 )
    {
      s_word_extract_first ( line, word, rest );

      if ( s_len_trim ( word ) <= 0 )
      {
        continue;
      }
      
      if ( !s_eqi ( word, "P4" ) )
      {
        cout << "\n";
        cout << "PBMB_READ_HEADER - Fatal error.\n";
        cout << "  Bad magic number = \"" << word << "\".\n";
        return true;
      }
      line = rest;
      step = 1;
    }

    if ( step == 1 )
    {
      s_word_extract_first ( line, word, rest );
 
      if ( s_len_trim ( word ) <= 0 )
      {
        continue;
      }
      xsize = atoi ( word.c_str ( ) );
      line = rest;
      step = 2;
    }

    if ( step == 2 )
    {
      s_word_extract_first ( line, word, rest );
 
      if ( s_len_trim ( word ) <= 0 )
      {
        continue;
      }
      ysize = atoi ( word.c_str ( ) );
      break;
    }

  }
  return false;
}

bool s_eqi ( string s1, string s2 )

//****************************************************************************80
//
//  Purpose:
//
//    S_EQI reports whether two strings are equal, ignoring case.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license. 
//
//  Modified:
//
//    05 July 2009
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, string S1, S2, two strings.
//
//    Output, bool S_EQI, is true if the strings are equal. 
//
{
  int i;
  int nchar;
  int s1_length;
  int s2_length;

  s1_length = s1.length ( );
  s2_length = s2.length ( );

  if ( s1_length < s2_length )
  {
    nchar = s1_length;
  }
  else
  {
    nchar = s2_length;
  }
//
//  The strings are not equal if they differ over their common length.
//
  for ( i = 0; i < nchar; i++ ) 
  {

    if ( ch_cap ( s1[i] ) != ch_cap ( s2[i] ) ) 
    {
      return false;
    }
  }
//
//  The strings are not equal if the longer one includes nonblanks
//  in the tail.
//
  if ( nchar < s1_length ) 
  {
    for ( i = nchar; i < s1_length; i++ ) 
    {
      if ( s1[i] != ' ' ) 
      {
        return false;
      }
    } 
  }
  else if ( nchar < s2_length ) 
  {
    for ( i = nchar; i < s2_length; i++ )
    {
      if ( s2[i] != ' ' ) 
      {
        return false;
      }
    } 
  }

  return true;
}
//****************************************************************************80

int s_len_trim ( string s )

//****************************************************************************80
//
//  Purpose:
//
//    S_LEN_TRIM returns the length of a string to the last nonblank.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license. 
//
//  Modified:
//
//    05 July 2009
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, string S, a string.
//
//    Output, int S_LEN_TRIM, the length of the string to the last nonblank.
//    If S_LEN_TRIM is 0, then the string is entirely blank.
//
{
  int n;

  n = s.length ( );

  while ( 0 < n ) 
  {
    if ( s[n-1] != ' ' )
    {
      return n;
    }
    n = n - 1;
  }

  return n;
}
//****************************************************************************80

void s_word_extract_first ( string s, string &s1, string &s2 )

//****************************************************************************80
//
//  Purpose:
//
//    S_WORD_EXTRACT_FIRST extracts the first word from a string.
//
//  Discussion:
//
//    A "word" is a string of characters terminated by a blank or
//    the end of the string.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license. 
//
//  Modified:
//
//    25 October 2010
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, string S, the string.
//
//    Output, string &S1, the first word (initial blanks removed).
//
//    Output, string &S2, the remainder of the string, after removing
//    the first word (initial blanks removed).
//
{
  int i;
  int mode;
  int s_len;

  s_len = s.length ( );
  s1 = "";
  s2 = "";
  mode = 1;

  for ( i = 0; i < s_len; i++ )
  {
    if ( mode == 1 )
    {
      if ( s[i] != ' ' )
      {
         mode = 2;
      }
    }
    else if ( mode == 2 )
    {
      if ( s[i] == ' ' )
      {
        mode = 3;
      }
    }
    else if ( mode == 3 )
    {
      if ( s[i] != ' ' )
      {
        mode = 4;
      }
    }
    if ( mode == 2 )
    {
      s1 = s1 + s[i];
    }
    else if ( mode == 4 )
    {
      s2 = s2 + s[i];
    }
  }

  return;
}

char ch_cap ( char ch )

//****************************************************************************80
//
//  Purpose:
//
//    CH_CAP capitalizes a single character.
//
//  Discussion:
//
//    This routine should be equivalent to the library "toupper" function.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license. 
//
//  Modified:
//
//    19 July 1998
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, char CH, the character to capitalize.
//
//    Output, char CH_CAP, the capitalized character.
//
{
  if ( 97 <= ch && ch <= 122 ) 
  {
    ch = ch - 32;
  }   

  return ch;
}