/*
 * Function to generate:
 *        .pos file with positions of all polygons in 2D plane co-ords
 *        .pos file with positions of all polygons in Stereo co-ords
 *        .geo file with list of all reef polygon vertices in 3D Cartesian co-ords
 *        .geo file with list of all reef polygon vertices in Stereo co-ords
 *        .dat file with list of all reef polygon vertices (_ContoursList.dat) in 3D Cartesian co-ords
 * 
 * Use this to create all necessary files to generate a mesh.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "shapefil.h"

 void addPoint(double x, double y, int reef_num, int**);
 void line(int x1, int y1, int x2, int y2, int reef_num, int** data);
 int sign (long a);
 void geoCoordToCartesian(double geoLat, double geoLon, double*);
 void cartesianCoordToStereographic(double*, double*);
 void projectPoint(double*, double projParams[3][3], double*);
 void Inverse(double Matrix[3][3], double Inverse[3][3]);

 int main()
 {
   int nEntities, nShapeType, i, j, reefCounter=1;
   double adfMinBound[4], adfMaxBound[4];
   double polySideLength;
   
   // Projection Parameters
   double pi;
   pi = 4.0*atan(1.0);
   printf( "pi is %.15g\n",pi );
   double phi = 146.5892/180.0*pi;
   double theta = -17.147569/180.0*pi;
   double Ox = cos(theta)*cos(phi);
   double Oy = cos(theta)*sin(phi);
   double Oz = sin(theta);
   double eThetaX = -sin(theta)*cos(phi);
   double eThetaY = -sin(theta)*sin(phi);
   double eThetaZ = cos(theta);
   double ePhiX = -sin(phi);
   double ePhiY = cos(phi);
   double ePhiZ = 0.0;
   double projParams[3][3], origin[3];
   projParams[0][0] = ePhiX;
   projParams[0][1] = ePhiY;
   projParams[0][2] = ePhiZ;
   projParams[1][0] = eThetaX;
   projParams[1][1] = eThetaY;
   projParams[1][2] = eThetaZ;

   origin[0] = Ox;
   origin[1] = Oy;
   origin[2] = Oz;
   
   SHPHandle newData;
   FILE *outFile;
   FILE *outFileDat;
   FILE *outFileStereo;
   FILE *outFilePos;
   FILE *outFilePosStereo;

   outFile = fopen("./GBR_DeepReefMap_Vertices_3DCartesian.geo","w");
   outFileStereo = fopen("./GBR_DeepReefMap_Vertices_Stereographic.geo","w");
   outFileDat = fopen("./GBR_DeepReefMap_ContoursList_3DCartesian.dat","w");
   outFilePos = fopen("./GBR_DeepReefMap_2DPlane.pos","w");
   outFilePosStereo = fopen("./GBR_DeepReefMap_Stereo.pos","w");

   fprintf( outFile, "IP = newp;\n");
   fprintf( outFileStereo, "IP = newp;\nIL = newl;\nIS = news;\nIF = newf;");
   fprintf( outFilePos, "View \"Reef map\" {\n");
   fprintf( outFilePosStereo, "View \"Reef map\" {\n");

//   newData = SHPOpen( "/home/cthomas/Data/Coral Reefs Distro 2010/coralreef2010/coralreef2010_GBR", "rb" );
   newData = SHPOpen( "/home/cthomas/GBR_data/GBR_Banks_Layer/Banks_GBR", "rb" );
   SHPGetInfo( newData, &nEntities, &nShapeType, adfMinBound, adfMaxBound );
   printf( "\nNo. of Entities: %d, Shape type: %s\n", nEntities, SHPTypeName( nShapeType ) );
   printf( "X min./max. values: %.15g,%.15g\nY min./max. values: %.15g,%.15g\n\n", adfMinBound[0], adfMaxBound[0], adfMinBound[1], adfMaxBound[1] );
   
   int vertexCounter=0;
   //Loop over all shapes
   for (i=0; i<nEntities; i++) 
   {
      SHPObject *currentShape;
      currentShape = SHPReadObject( newData, i );

      if( currentShape == NULL )
      {
        fprintf( stderr, "Unable to read shape %d, terminating object reading.\n", i );
        break;
      }
      
      //Verify reef is within domain:
//    NB: The following uses a straight-line cutoff criterion
//      if (currentShape->dfXMax < 142.0 || currentShape->dfXMin > 154.396 || currentShape->dfYMax < -25.8724 || currentShape->dfYMin > -11.0 || currentShape->dfXMin + currentShape->dfYMin > 148.8 - 15.4) continue;

//    Test #1: If polygon is in an unwanted area, remove it.
//    NB: The following uses a series of boxes to remove unwanted reefs
      if (currentShape->dfXMax < 142.0 || currentShape->dfXMin > 154.396 || currentShape->dfYMax < -25.8724 || currentShape->dfYMin > -11.0 || (currentShape->dfXMin > 146.8 && currentShape->dfYMin > -18.0) || (currentShape->dfXMin > 145.8 && currentShape->dfYMin > -15.0) || (currentShape->dfXMin > 151.2 && currentShape->dfYMin > -20.0)  || (currentShape->dfXMin > 149.21 && currentShape->dfYMin > -18.25) || (currentShape->dfXMin > 153.36 && currentShape->dfYMin > -22.05) || (currentShape->dfXMin > 153.27 && currentShape->dfYMin < -24.915) 
      ) continue;
        //Zoomed area:
//       || (currentShape->dfYMin - currentShape->dfXMin > -146.1 - 17.4)
//       || (currentShape->dfYMax - currentShape->dfXMax < -150.48 - 22.2) ) continue;
      //North GBR condition:
//       if (currentShape->dfXMax < 142.0 || currentShape->dfXMin > 146.0 || currentShape->dfYMax < -14.738 || currentShape->dfYMin > -12.0  ) continue;
      //Entire Central & South GBR:
//      || (currentShape->dfYMin - currentShape->dfXMin > -145.8 - 16.5) ) continue;
      
      int numVertices;
      numVertices = currentShape->nVertices;
      
      //Test #2: If polygon perimeter < threshold, don't count it.
      double p0_x, p0_y, p1_x, p1_y;
      polySideLength=0.0;
      //Loop over all vertices to see whether shape's big enough to count or not
      //Nb:: padfX: Longitude, padfY: Latitude
      for (j=0; j<numVertices; j++)
      {
        p0_x = currentShape->padfX[j]; //save x-coord
        p0_y = currentShape->padfY[j];
        p1_x = currentShape->padfX[(j+1)%numVertices];
        p1_y = currentShape->padfY[(j+1)%numVertices];
        polySideLength += sqrt( (p1_x-p0_x)*(p1_x-p0_x) + (p1_y-p0_y)*(p1_y-p0_y) );
      }
      if (polySideLength < 0.016) continue;
      
      //Loop over all vertices to write points to file
      for (j=0; j<numVertices; j++)
      {
        p0_x = currentShape->padfX[j];
        p0_y = currentShape->padfY[j];
        p1_x = currentShape->padfX[(j+1)%numVertices];
        p1_y = currentShape->padfY[(j+1)%numVertices];
//         double remaining = sqrt( (p1_x-p0_x)*(p1_x-p0_x) + (p1_y-p0_y)*(p1_y-p0_y) );
        //printf("Remaining is: %.8f\n",remaining);
//         double unit_x = (p1_x-p0_x)/remaining;
//         double unit_y = (p1_y-p0_y)/remaining;
        double pointCartOld[3];
        double pointCart[3];
        
//         while (remaining > 0.0) {
//           geoCoordToCartesian(p0_x, p0_y, pointCart);
//           fprintf( outFile, "Point ( IP + %d ) = {%.14f ,%.14f, %.14f};\n", vertexCounter, pointCart[0], pointCart[1], pointCart[2] );
//           p0_x += unit_x*0.0002;
//           p0_y += unit_y*0.0002;
//           remaining -= 0.0002;
//         }
        //Plot final vertex:
        geoCoordToCartesian(p1_y, p1_x, pointCart);
        geoCoordToCartesian(p0_y, p0_x, pointCartOld);
        
        fprintf( outFile, "Point ( IP + %d ) = {%.14f ,%.14f, %.14f};\n", vertexCounter, pointCart[0], pointCart[1], pointCart[2] );
        fprintf( outFileDat, "%.14f %.14f %.14f\n", pointCart[0], pointCart[1], pointCart[2] );
        
        //Get stereographic coords:
        double pointStereo[3];
        double pointStereoOld[3];
        cartesianCoordToStereographic(pointCart, pointStereo);
        cartesianCoordToStereographic(pointCartOld, pointStereoOld);
        fprintf( outFileStereo, "Point ( IP + %d ) = {%.14f ,%.14f, %.14f};\n", vertexCounter, pointStereo[0], pointStereo[1], pointStereo[2] );
        fprintf( outFilePosStereo, "SL (%.10g,%.10g,0,%.10g,%.10g,0) {%d,%d};\n", pointStereoOld[0],pointStereoOld[1],pointStereo[0],pointStereo[1], vertexCounter, vertexCounter );
        
        //Project onto 2D plane
        projectPoint(pointCartOld, projParams, origin);
        projectPoint(pointCart, projParams, origin);
        fprintf( outFilePos, "SL (%.10g,%.10g,0,%.10g,%.10g,0) {%d,%d};\n", pointCartOld[0],pointCartOld[1],pointCart[0],pointCart[1], vertexCounter, vertexCounter );
        
        vertexCounter++;
      }
      reefCounter++;
      
   }
   printf("Total number of reefs recorded: %d.\n", reefCounter-1);
   printf("Total number of vertex points recorded to file: %d.\n", vertexCounter);
   SHPClose(newData);
   fprintf( outFile, "IFI = newf;\nField[IFI]=Attractor;\nField[IFI].NodesList={IP:IP+%d};", vertexCounter );
   fprintf( outFileStereo, "IFI = newf;\nField[IFI]=Attractor;\nField[IFI].NodesList={IP:IP+%d};", vertexCounter );
   fprintf( outFilePos, "};");
   fprintf( outFilePosStereo, "};");
   
   fclose(outFile);
   fclose(outFileDat);
   fclose(outFileStereo);
   fclose(outFilePos);
   
   exit(0);
 }
 
 void addPoint(double x, double y, int reef_num, int** data) 
 {
//   double extent_lon = 155.884935647662-140.921706943421;
//   double extent_lat = -25.0026964546776+8.58661304885311;
//   x = (x-146.5892)*1647.046/extent_lon;
//   y = (y+8.58661304885311)*1816.823/extent_lat;
   int x_int = (int)(x/10000.0+874.0+0.5);
   int y_int = (int)(y/10000.0+603.0+0.5); //why 10000 not 1000???TODO
//  printf("\tx = %d \ty = %d\n",x_int, y_int);
  data[x_int][y_int]=reef_num;
 }
 
 int sign(long a) 
 {
   if (a > 0) return +1;
   else if (a < 0) return -1;
   else return 0;
 }
 
 void line(int x1, int y1, int x2, int y2, int reef_num, int** data) 
 {
   long x_vec, y_vec,s, dx1, dy1, dx2, dy2, x_length, y_length;
   int i;
   
   x_vec = x2-x1;
   y_vec = y2-y1;
   dx1 = sign(x_vec);
   dy1 = sign(y_vec);
   dx2 = sign(x_vec);
   dy2 = 0;
   x_length = abs(x_vec);
   y_length = abs(y_vec);
   
   if (x_length <= y_length) {
     dx2 = 0;
     dy2 = sign(y_vec);
     y_length = abs(y_vec);
     x_length = abs(x_vec);
   }
   s = (int)(x_length / 2);
   
   for (i=0; i<(int)(x_length+0.5); i++) {
     addPoint(x1, y1, reef_num, data);
     s += y_length;
     if (s >= x_length) {
       s -= x_length;
       x1 += dx1;
       y1 += dy1;
     }
     else {
       x1 += dx2;
       y1 += dy2;
     }
   }
 }
 
void geoCoordToCartesian(double geoLat, double geoLon, double pointCart[3])
{
  //calculate radius at given latitude
//   double a = 6378137.0; //Earth SemiMajor
//   double b = 6356752.3; //Earth SemiMinor
   double pi;
   pi = acos(-1.0);
//   double R = sqrt( ((a*a*(cos(geoLat*pi/180.0)))*(a*a*(cos(geoLat*pi/180.0))) + (b*b*(sin(geoLat*pi/180.0)))*(b*b*(sin(geoLat*pi/180.0)))) / ((a*(cos(geoLat*pi/180.0)))*(a*(cos(geoLat*pi/180.0))) + (b*(sin(geoLat*pi/180.0)))*(b*(sin(geoLat*pi/180.0)))) );
//  printf( "R is %.15g",R );
  double R = 6.371e6;
  //geo coords --> S.P.C.
  double pointTheta = 90.0 - geoLat;
  double pointPhi = geoLon;

  //S.P.C. --> Cartesian(on sphere)
  pointCart[0] = R * sin(pointTheta*pi/180.0) * cos(pointPhi*pi/180.0);
  pointCart[1] = R * sin(pointTheta*pi/180.0) * sin(pointPhi*pi/180.0);
  pointCart[2] = R * cos(pointTheta*pi/180.0);
}

void cartesianCoordToStereographic(double pointCart[3], double pointStereo[3])
{
  double R = 6.371e6;
  //geo coords --> S.P.C.
  pointStereo[0] = (2 * R * pointCart[0]) / (R + pointCart[2]);
  pointStereo[1] = (2 * R * pointCart[1]) / (R + pointCart[2]);
  pointStereo[2] = 0;
}

//takes x,y,z(sphere) as input and overwrites with x,y,z(plane)
void projectPoint(double point[3], double projParams[3][3], double origin[3])
{
  double temp[3][3];
  projParams[2][0] = point[0];
  projParams[2][1] = point[1];
  projParams[2][2] = point[2];
  int i, j;
  Inverse(projParams, temp);
  for (i=0; i<3; i++) {
    for (j=0; j<3; j++) projParams[i][j] = temp[i][j];
  }
  double R = sqrt(point[0]*point[0] + point[1]*point[1] + point[2]*point[2]);
  double alpha = -origin[0]*projParams[0][0] - origin[1]*projParams[1][0] - origin[2] * projParams[2][0];
  double beta = -origin[0]*projParams[0][1] - origin[1]*projParams[1][1] - origin[2] * projParams[2][1];

  point[0] = alpha*R;
  point[1] = beta*R;
  point[2] = 0.0;
//  printf( "x is %.15g, y is %.15g, z is %.15g.\n", point[0]/1000.0, point[1]/1000.0, point[2] );
}

void Inverse(double Matrix[3][3], double Inverse[3][3])
{
  double determinant =    +Matrix[0][0]*(Matrix[1][1]*Matrix[2][2]-Matrix[2][1]*Matrix[1][2])
  -Matrix[0][1]*(Matrix[1][0]*Matrix[2][2]-Matrix[1][2]*Matrix[2][0])
  +Matrix[0][2]*(Matrix[1][0]*Matrix[2][1]-Matrix[1][1]*Matrix[2][0]);
  double invdet = 1.0/determinant;
  Inverse[0][0] =  (Matrix[1][1]*Matrix[2][2]-Matrix[2][1]*Matrix[1][2])*invdet;
  Inverse[1][0] = -(Matrix[0][1]*Matrix[2][2]-Matrix[0][2]*Matrix[2][1])*invdet;
  Inverse[2][0] =  (Matrix[0][1]*Matrix[1][2]-Matrix[0][2]*Matrix[1][1])*invdet;
  Inverse[0][1] = -(Matrix[1][0]*Matrix[2][2]-Matrix[1][2]*Matrix[2][0])*invdet;
  Inverse[1][1] =  (Matrix[0][0]*Matrix[2][2]-Matrix[0][2]*Matrix[2][0])*invdet;
  Inverse[2][1] = -(Matrix[0][0]*Matrix[1][2]-Matrix[1][0]*Matrix[0][2])*invdet;
  Inverse[0][2] =  (Matrix[1][0]*Matrix[2][1]-Matrix[2][0]*Matrix[1][1])*invdet;
  Inverse[1][2] = -(Matrix[0][0]*Matrix[2][1]-Matrix[2][0]*Matrix[0][1])*invdet;
  Inverse[2][2] =  (Matrix[0][0]*Matrix[1][1]-Matrix[1][0]*Matrix[0][1])*invdet;
}
