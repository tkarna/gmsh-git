#Script to compare GBROOS observed currents to SLIM-predicted currents
#1. Load GBROOS data from netCDF file, calculate depth- and time-averaged GBROOS currents and GBROOS residual currents
#2. Load SLIM simulation data, calculate residual currents
#3. Standardise timescales
#4. Plot both datasets

import numpy as np
from scipy.io import netcdf
import matplotlib.pyplot as plt
import matplotlib.dates as dates
import datetime
import os

#=====================
# Define input files:
siteName = [''] * 9
inFileName_GBROOS = [''] * 9
siteName[0]='Lizard Island Slope'
siteName[1]='Lizard Island Shelf'
siteName[2]='Palm Island Passage'
siteName[3]='Myrmidon Reef Slope'
siteName[4]='Elusive Reef'
siteName[5]='Capricorn Channel'
siteName[6]='Heron Island North'
siteName[7]='Heron Island South'
siteName[8]='One Tree Island'
# NOV 2007:
inFileName_GBROOS[0] = '/home/cthomas/GBR_data/GBROOS/ANMN_Moorings/LSL_LizardIslandSlope/IMOS_ANMN-QLD_VATPE_20071103T110000Z_GBRLSL_FV01_GBRLSL-0710-Continental-205_END-20080613T132900Z_C-20120206T014702Z.nc'
inFileName_GBROOS[2] = '/home/cthomas/GBR_data/GBROOS/ANMN_Moorings/PPS_PalmIslandPassage/IMOS_ANMN-QLD_VATE_20071029T103000Z_GBRPPS_FV01_GBRPPS-0710-Workhorse-ADCP-53_END-20080620T104700Z_C-20120206T014748Z_PART1.nc'
inFileName_GBROOS[3] = '/home/cthomas/GBR_data/GBROOS/ANMN_Moorings/MYR_MyrmidonSlope/IMOS_ANMN-QLD_VATPE_20071030T113000Z_GBRMYR_FV01_GBRMYR-0710-Continental-190_END-20080619T081500Z_C-20120206T014728Z.nc'
inFileName_GBROOS[5] = '/home/cthomas/GBR_data/GBROOS/ANMN_Moorings/CCH_CapricornChannel/IMOS_ANMN-QLD_VATPE_20070910T073000Z_GBRCCH_FV01_GBRCCH-0709-Workhorse-ADCP-78_END-20080506T083500Z_C-20120206T043427Z.nc'
inFileName_GBROOS[6] = '/home/cthomas/GBR_data/GBROOS/ANMN_Moorings/HIN_HeronIslandNorth/IMOS_ANMN-QLD_VATE_20070912T093000Z_GBRHIN_FV01_GBRHIN-0709-Workhorse-ADCP-43_END-20080507T134600Z_C-20120206T043525Z.nc'
inFileName_GBROOS[7] = '/home/cthomas/GBR_data/GBROOS/ANMN_Moorings/HIS_HeronIslandSouth/IMOS_ANMN-QLD_VATPE_20070913T090000Z_GBRHIS_FV01_GBRHIS-0709-Workhorse-ADCP-42_END-20080509T163000Z_C-20120206T043720Z.nc'
#04 DEC 2008 onwards:
#inFileName_GBROOS[0] = '/home/cthomas/GBR_data/GBROOS/ANMN_Moorings/LSL_LizardIslandSlope/IMOS_ANMN-QLD_VATPE_20081202T053000Z_GBRLSL_FV01_GBRLSL-0812-Workhorse-ADCP-244_END-20090530T231500Z_C-20120205T150820Z.nc'
#inFileName_GBROOS[2] = '/home/cthomas/GBR_data/GBROOS/ANMN_Moorings/PPS_PalmIslandPassage/IMOS_ANMN-QLD_VATE_20081128T203000Z_GBRPPS_FV01_GBRPPS-0811-Workhorse-ADCP-60_END-20090526T040600Z_C-20120205T150921Z.nc'
#inFileName_GBROOS[3] = '/home/cthomas/GBR_data/GBROOS/ANMN_Moorings/MYR_MyrmidonSlope/IMOS_ANMN-QLD_VATPE_20081128T223000Z_GBRMYR_FV01_GBRMYR-0811-Continental-193_END-20090526T203100Z_C-20120205T150851Z.nc'
#inFileName_GBROOS[4] = '/home/cthomas/GBR_data/GBROOS/ANMN_Moorings/ELR_ElusiveReef/IMOS_ANMN-QLD_VATPE_20081024T083000Z_GBRELR_FV01_GBRELR-0810-Continental-193_END-20090418T214300Z_C-20120205T160754Z.nc'
#inFileName_GBROOS[5] = '/home/cthomas/GBR_data/GBROOS/ANMN_Moorings/CCH_CapricornChannel/IMOS_ANMN-QLD_VATPE_20081102T200000Z_GBRCCH_FV01_GBRCCH-0811-Workhorse-ADCP-81_END-20090419T223200Z_C-20120205T160707Z.nc'
#inFileName_GBROOS[6] = '/home/cthomas/GBR_data/GBROOS/ANMN_Moorings/HIN_HeronIslandNorth/IMOS_ANMN-QLD_VATE_20081027T000000Z_GBRHIN_FV01_GBRHIN-0810-Workhorse-ADCP-41_END-20090420T051200Z_C-20120205T160830Z.nc'
#inFileName_GBROOS[7] = '/home/cthomas/GBR_data/GBROOS/ANMN_Moorings/HIS_HeronIslandSouth/IMOS_ANMN-QLD_VATE_20081029T063000Z_GBRHIS_FV01_GBRHIS-0810-Workhorse-ADCP-44_END-20090423T043000Z_C-20120205T160927Z.nc'
#inFileName_GBROOS[8] = '/home/cthomas/GBR_data/GBROOS/ANMN_Moorings/OTE_OneTreeIsland/IMOS_ANMN-QLD_VATPE_20081028T230000Z_GBROTE_FV01_GBROTE-0810-Workhorse-ADCP-54_END-20090421T042700Z_C-20120205T161040Z.nc'

inFileNameBulk_SLIM = '/home/cthomas/GBR_slim/Saved_Data/Hydro_Calibration/gbr330K_sgbr/2svIn_bDf10_Realistic_4wks/Evaluator/evalElevGBROOS_0'
#inFileNameBulk_SLIM = '/home/cthomas/GBR_slim/Output/Evaluator/evalElevGBROOS_0'
#=====================
# Define SLIM simulation start time:
SLIM_startTime = datetime.datetime(2007, 11, 28, 0, 0, 0)
#=====================
plotOnlyGBROOS = 0

def loadDatasets(inFileName_GBROOS, inFileName_SLIM, SLIM_startTime):
  # 1. Read GBROOS data from netCDF file
  f = netcdf.netcdf_file(inFileName_GBROOS, 'r')
  
  if 'DEPTH' not in f.variables:
    print '* Site has no depth data *'
    return None, None#np.zeros( (2,2) )
  
  time = f.variables['TIME']
  time_units = time.units
  depth = f.variables['DEPTH']
  height = f.variables['HEIGHT_ABOVE_SENSOR']
  lat = f.variables['LATITUDE']
  lon = f.variables['LONGITUDE']
  f.close()
  print '    GBROOS lon/lat:', lon[0], lat[0]
  #print' GBROOS initial time: ', time[0], time_units
  # 1.a) Record depths (minus the average water height)
  GBROOS_data = np.zeros( (len(time[:]), 2) )
  GBROOS_nbSteps = GBROOS_data.shape[0]
  for n in range(0,len(time[:])):
    GBROOS_data[n,0] = time[n]
    GBROOS_data[n,1] = depth[n]
  aveDepth = depth[:].mean()
  for n in range(0,len(time[:])):
    GBROOS_data[n,1] = GBROOS_data[n,1] - aveDepth

  # 1.b) Calculate time-averaged GBROOS depths
  #dt = GBROOS_data[1,0] - GBROOS_data[0,0] #units: days
  #stepsPerSixHrs = int(0.1 + (1.5/24.0)/dt) #+0.1 as int() rounds down
  #print '    GBROOS Data: time steps per averaging period: '+ str(stepsPerSixHrs)+', dt = '+str(dt*24.0)
  #temp_array = np.zeros((GBROOS_nbSteps,1))
  #for n in range(0, GBROOS_nbSteps):
    #if n >= stepsPerSixHrs/2.0 and n < GBROOS_nbSteps-stepsPerSixHrs/2.0:
      #temp_array[n] = GBROOS_data[n-stepsPerSixHrs/2:n+stepsPerSixHrs/2, 1].mean()
  #for n in range(0, GBROOS_nbSteps):
    #if n >= stepsPerSixHrs/2.0 and n < GBROOS_nbSteps-stepsPerSixHrs/2.0:
      #GBROOS_data[n,1] = temp_array[n]

  #----------

  # 2. Read SLIM data
  print inFileName_SLIM
  if not os.path.exists(inFileName_SLIM):
    print inFileName_SLIM + ' does not exist. Skipping it ...'
    return None, None
  SLIM_file = open(inFileName_SLIM, 'r')
  SLIM_nbSteps=0
  for line in SLIM_file.readlines():
    SLIM_nbSteps = SLIM_nbSteps+1
  SLIM_file.close()
  SLIM_file = open(inFileName_SLIM, 'r')
  SLIM_data = np.zeros( (SLIM_nbSteps,2) )
  i=0
  for line in SLIM_file.readlines():
    line = line.split()
    SLIM_data[i,0] = float(line[0])
    SLIM_data[i,1] = float(line[1])
    i=i+1
  print 'SLIM_nbSteps is', SLIM_nbSteps
  #SLIM_data = np.zeros( (1,2) )
  #for line in SLIM_file.readlines():
    #line = line.split()
    #newLine = np.array( [float(line[0]), float(line[1])] )
    #SLIM_data = np.vstack(( SLIM_data, newLine ))
  #SLIM_nbSteps = SLIM_data.shape[0]

  #----------

  # 3. Standardise SLIM and GBROOS timescales (GBROOS: days since 1950-01-01 00:00:00 UTC)
  #  1. Convert both start times to datetime objects:
  GBROOS_startTimeOffset = datetime.timedelta(GBROOS_data[0,0]) 
  #print GBROOS_startTimeOffset
  GBROOS_startTime = datetime.datetime(1950, 1, 1, 0, 0, 0) + GBROOS_startTimeOffset
  #print '  --> GBROOS start time:', GBROOS_startTime
  #print '  --> SLIM start time:', SLIM_startTime
  #  2. Convert both datetime objects to number of days since 0001-01-01 00:00:00 UTC plus one
  GBROOS_startTime = dates.date2num(GBROOS_startTime)
  
  SLIM_startTime = dates.date2num(SLIM_startTime)
  #print '  --> Start times in "dates" format:', GBROOS_startTime, SLIM_startTime
  #  3. Standardise format of time indices of both datasets with number of days since 0001-01-01 00:00:00 UTC plus one
  GBROOS_startTimeOffsetNumeric = GBROOS_data[0,0]
  #for n in range(0, GBROOS_nbSteps):
    #GBROOS_data[n,0] = GBROOS_startTime + (GBROOS_data[n,0] - GBROOS_startTimeOffsetNumeric)  #<-Avoids timedelta arithmetic at each iter
  for n in range(0, GBROOS_nbSteps):
    GBROOS_data[n,0] = dates.date2num( datetime.datetime(1950, 1, 1, 0, 0, 0) + datetime.timedelta(GBROOS_data[n,0]) )
  for n in range (0, SLIM_nbSteps):
    SLIM_data[n,0] = SLIM_startTime + SLIM_data[n,0]/24.0

  #TEMP: subtract 9 hours from GBROOS!!! (Time difference)
  SLIM_dt = SLIM_data[0,1] - SLIM_data[0,0]
  #print dates.num2date(SLIM_data[0,0]), dates.num2date(SLIM_data[1,0])
  for n in range(0, len(SLIM_data[:,0])):
    SLIM_data[n,0] = SLIM_data[n,0] + 9.0/24.0
    
  return GBROOS_data, SLIM_data

# 4. Plot time-averaged GBROOS currents and SLIM currents
print ''
print '** Graphing elevation time-series **'
# NB: for SLIM_data, start plotting from [1] as [0]s=0
fig = plt.figure()
for site in range (0,9):
  if inFileName_GBROOS[site] == '':
    continue
  print '--> Reading GBROOS netCDF data for: ' + siteName[site]
  [GBROOS_data, SLIM_data] = loadDatasets(inFileName_GBROOS[site], inFileNameBulk_SLIM+str(site)+'.dat', SLIM_startTime)
  if GBROOS_data is None:
    continue
  print ' SLIM initial time:', SLIM_startTime, 'GBROOS initial time: ',dates.num2date(GBROOS_data[0,0])
  print ' SLIM_dt = ', SLIM_data[1,0]-SLIM_data[0,0], 'GBROOS_dt = ', GBROOS_data[1,0]-GBROOS_data[0,0]
  ax = fig.add_subplot(3, 3, site, label=siteName[site])
  if plotOnlyGBROOS == 1:
    ax.plot(dates.num2date(GBROOS_data[:,0]), GBROOS_data[:,1])
    ax.tick_params(axis='both', which='major', labelsize=10)
    ax.tick_params(axis='both', which='minor', labelsize=6)
    ax.grid(True,linestyle='-',color='0.75')
    ax.set_title(siteName[site])
    continue
  
  # ----
  
  #Find the the value of GBROOS_data immediately before the start of SLIM_data
  GBROOS_timeAtSLIMInitTime = 0
  GBROOS_timeAtSLIMEndTime = 0
  for n in range(0, len(GBROOS_data[:,0])):
    if GBROOS_data[n,0] > SLIM_data[0,0]:
      GBROOS_timeAtSLIMInitTime = n-1
      break
  for n in range(GBROOS_timeAtSLIMInitTime, len(GBROOS_data[:,0])):
    if GBROOS_data[n,0] > SLIM_data[len(SLIM_data[:,0])-1,0]:
      GBROOS_timeAtSLIMEndTime = n
      break
  
  # ----
  
  #Find mean GBROOS water height (Z0) and subtract from GBROOS elevations, then add SLIM mean elevation to harmonise baseline elevations
  GBROOS_meanDuringSLIMTime = GBROOS_data[GBROOS_timeAtSLIMInitTime+1:GBROOS_timeAtSLIMEndTime-1,1].mean()
  SLIM_mean = SLIM_data[:,1].mean()
  print'Site:', siteName[site], ' SLIM_Z0:', SLIM_mean, ' GBROOS_Z0:', GBROOS_meanDuringSLIMTime
  for n in range(0, len(GBROOS_data[:,0])):
    GBROOS_data[n,1] = GBROOS_data[n,1] - GBROOS_meanDuringSLIMTime + SLIM_mean
  
  #Plot elevation data:
  ax.plot(dates.num2date(GBROOS_data[GBROOS_timeAtSLIMInitTime:GBROOS_timeAtSLIMEndTime,0]), GBROOS_data[GBROOS_timeAtSLIMInitTime:GBROOS_timeAtSLIMEndTime,1], dates.num2date(SLIM_data[:,0]), SLIM_data[:,1])
  #ax.set_xlim( [dates.num2date(SLIM_data[:,0]), dates.num2date(SLIM_data[len(SLIM_data[:,0])-1,0])] )
  ax.set_title(siteName[site])
  ax.grid(True,linestyle='-',color='0.75')
  ax.tick_params(axis='both', which='major', labelsize=10)
  ax.tick_params(axis='both', which='minor', labelsize=6)
fig.subplots_adjust(bottom=0.06)
fig.suptitle('Elevation of GBROOS data (blue) and SLIM output (green)', fontsize=12)
#fig.set_tight_layout(True)
#fig.plot()
plt.show()
