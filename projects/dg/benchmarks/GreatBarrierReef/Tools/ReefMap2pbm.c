//FOR TESTING: use test.dat input file

//#include<netpbm/pam.h>
#include <fstream>
#include<iostream>

using namespace std;

bool pbmb_write_data ( ofstream &output, int xsize, int ysize, int *barray );
bool pbmb_write_header ( ofstream &output, int xsize, int ysize );



int main(int argc, char *argv[]) {
  
  char* inputFilename = argv[1];
  char* outputFilename = argv[2];
  if (argc < 3) { printf("Not enough arguments. arg1: input filename, arg2: output filename.\n"); return 1; }
  printf("Input file: %s; Output file: %s\n", inputFilename, outputFilename);
  
  //Initialise libnetpbm
//  pam_init(&argc, argv);
  printf("Hullo.\n");

  //INPUT FILE
  FILE *input = fopen(inputFilename, "r");
//   FILE *input = fopen("test.dat", "r");
  
  //OUTPUT FILE
  ofstream output;
  output.open (outputFilename, ios::binary);
//     output.open ("test.pbm", ios::binary);
  
  if (! input) printf("Error: Cannot open Reef Map.");

  double buf, Ox, Oy, dx, dy;
  int nx, ny, bufInt;
  fscanf(input, "%lf %lf %lf %lf %lf %lf %d %d %d", &Ox, &Oy, &buf, &dx, &dy, &buf, &nx, &ny, &bufInt);
  printf( "Initialising Reef Map data: Ox: %e Oy: %e dx: %e dy: %e Nx: %d Ny: %d\n", Ox, Oy, dx, dy, nx, ny);

  int *data = new int[nx*ny];
  int row=0, col=0;
  for (row=0; row<ny; row++) {
    for (col=0; col<nx; col++) {
      fscanf(input, "%d", &data[row*nx+col]);
    }
  }
  fclose(input);
  
  printf("Writing output.\n");
  
  bool error;
  if ( !output )
  {
    cout << "\n";
    cout << "PBMB_WRITE: Fatal error!\n";
    cout << "  Cannot open the output file \n";
  }

//  Write the header.
  error = pbmb_write_header( output, nx, ny );
  if ( error )
  {
    cout << "\n";
    cout << "PBMB_WRITE: Fatal error!\n";
    cout << "  PBMB_WRITE_HEADER failed.\n";
    return true;
  }
  
//  Write the data.
  error = pbmb_write_data ( output, nx, ny, data );
  if ( error )
  {
    cout << "\n";
    cout << "PBMB_WRITE: Fatal error!\n";
    cout << "  PBMB_WRITE_DATA failed.\n";
    return true;
  }

//  Close the file.
  output.close ( );
  
  printf("Done.\n");
  return 0;
//  struct pam outpam();
}

bool pbmb_write_data ( ofstream &output, int xsize, int ysize, int *barray ) {
  int bit;
  unsigned char c;
  int i;
  int *indexb;
  int j;
  int k;

  indexb = barray;
  c = 0;

  for ( j = 0; j < ysize; j++ )
  {
    for ( i = 0; i < xsize; i++ )
    {
      k = 7 - i%8;
      bit = (*indexb)%2;
      c = c | ( bit << k );

      indexb = indexb + 1;

      if ( (i+1)%8 == 0 || i == ( xsize - 1 ) )
      {
        output << c;
        c = 0;
      }
    }
  }
  return false;
}

bool pbmb_write_header ( ofstream &output, int xsize, int ysize ) {
  output << "P4" << " "
           << xsize << " "
           << ysize << "\n";
 
  return false;
}