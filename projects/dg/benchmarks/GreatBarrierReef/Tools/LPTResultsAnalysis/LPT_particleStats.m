%Script to merge particle statistics from all CPUs and plot data

fprintf('\nScript to merge particle statistics from all CPUs and show data\n');

folderPath1 = '/home/cthomas/GBR_slim/Saved_Data/Hydro_Realistic/gbr500K_sma_1svIn_bDf10_allT_windOn_28112007_job9062/LPT_Output/';
folderPath2 = 'Agemmifera_RsSs_613558/';

folderPath = [folderPath1, folderPath2];

%Nb of CPUs:
nbCPUs = 1;

%Import 1st CPU (to get array length)
filename = [folderPath, 'CPU1/particleStats.dat'];
newStats = importdata(filename,' ');
particleStats = newStats;

for cpuNb = 2:1:nbCPUs
    filename = [folderPath, 'CPU', int2str(cpuNb), '/particleStats.dat'];
    newStats = importdata(filename,' ');
    particleStats(:,2:4) = particleStats(:,2:4) + newStats(:,2:4);
end

fprintf('Loaded stats.\n');

totPars = sum(particleStats(1,:));
%Set time to days
particleStats(:,1) = particleStats(:,1)./(3600*24);
granotuturco = figure();
plot(particleStats(:,1), particleStats(:,2)./totPars, particleStats(:,1), particleStats(:,3)./totPars, particleStats(:,1), particleStats(:,4)./totPars);
legend('Alive', 'Settled', 'Dead');
title(folderPath2, 'interpreter', 'none');
folderPath = [folderPath, 'particle_stats.png'];
saveas(granotuturco, folderPath);
fprintf('Plot saved to %s', folderPath);

percAl = 100*particleStats(length(particleStats),2)/totPars;
percSet = 100*particleStats(length(particleStats),3)/totPars;
percDe = 100*particleStats(length(particleStats),4)/totPars;
fprintf('%d particles.\nAfter %d days:\n\t\tAlive\tSettled\tDead\n\t\t%d\t%d\t\t%d\n\t\t%.3g%%\t\t%.3g%%\t\t%.3g%%\n', totPars, particleStats(length(particleStats),1)/(3600.0*24.0), particleStats(length(particleStats),2), particleStats(length(particleStats),3), particleStats(length(particleStats),4), percAl, percSet, percDe);

clearvars -except particleStats