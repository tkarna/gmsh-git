#Script to compare GBROOS observed currents to SLIM-predicted currents
#1. Load GBROOS data from netCDF file, calculate depth- and time-averaged GBROOS currents and GBROOS residual currents
#2. Load SLIM simulation data, calculate residual currents
#3. Standardise timescales
#4. Plot both datasets

import numpy as np
from scipy.io import netcdf
import matplotlib.pyplot as plt
import matplotlib.dates as dates
import datetime
import os

#=====================
# Define input files:
siteName = [''] * 9
inFileName_GBROOS = [''] * 9
siteName[0]='Lizard Island Slope'
siteName[1]='Lizard Island Shelf'
siteName[2]='Palm Island Passage'
siteName[3]='Myrmidon Reef Slope'
siteName[4]='Elusive Reef'
siteName[5]='Capricorn Channel'
siteName[6]='Heron Island North'
siteName[7]='Heron Island South'
siteName[8]='One Tree Island'
# NOV 2007:
inFileName_GBROOS[0] = '/home/cthomas/GBR_data/GBROOS/ANMN_Moorings/LSL_LizardIslandSlope/IMOS_ANMN-QLD_VATPE_20071103T110000Z_GBRLSL_FV01_GBRLSL-0710-Continental-205_END-20080613T132900Z_C-20120206T014702Z.nc'
inFileName_GBROOS[2] = '/home/cthomas/GBR_data/GBROOS/ANMN_Moorings/PPS_PalmIslandPassage/IMOS_ANMN-QLD_VATE_20071029T103000Z_GBRPPS_FV01_GBRPPS-0710-Workhorse-ADCP-53_END-20080620T104700Z_C-20120206T014748Z_PART1.nc'
inFileName_GBROOS[3] = '/home/cthomas/GBR_data/GBROOS/ANMN_Moorings/MYR_MyrmidonSlope/IMOS_ANMN-QLD_VATPE_20071030T113000Z_GBRMYR_FV01_GBRMYR-0710-Continental-190_END-20080619T081500Z_C-20120206T014728Z.nc'
inFileName_GBROOS[5] = '/home/cthomas/GBR_data/GBROOS/ANMN_Moorings/CCH_CapricornChannel/IMOS_ANMN-QLD_VATPE_20070910T073000Z_GBRCCH_FV01_GBRCCH-0709-Workhorse-ADCP-78_END-20080506T083500Z_C-20120206T043427Z.nc'
inFileName_GBROOS[6] = '/home/cthomas/GBR_data/GBROOS/ANMN_Moorings/HIN_HeronIslandNorth/IMOS_ANMN-QLD_VATE_20070912T093000Z_GBRHIN_FV01_GBRHIN-0709-Workhorse-ADCP-43_END-20080507T134600Z_C-20120206T043525Z.nc'
inFileName_GBROOS[7] = '/home/cthomas/GBR_data/GBROOS/ANMN_Moorings/HIS_HeronIslandSouth/IMOS_ANMN-QLD_VATPE_20070913T090000Z_GBRHIS_FV01_GBRHIS-0709-Workhorse-ADCP-42_END-20080509T163000Z_C-20120206T043720Z.nc'
#04 DEC 2008 onwards:
#inFileName_GBROOS[0] = '/home/cthomas/GBR_data/GBROOS/ANMN_Moorings/LSL_LizardIslandSlope/IMOS_ANMN-QLD_VATPE_20081202T053000Z_GBRLSL_FV01_GBRLSL-0812-Workhorse-ADCP-244_END-20090530T231500Z_C-20120205T150820Z.nc'
#inFileName_GBROOS[2] = '/home/cthomas/GBR_data/GBROOS/ANMN_Moorings/PPS_PalmIslandPassage/IMOS_ANMN-QLD_VATE_20081128T203000Z_GBRPPS_FV01_GBRPPS-0811-Workhorse-ADCP-60_END-20090526T040600Z_C-20120205T150921Z.nc'
#inFileName_GBROOS[3] = '/home/cthomas/GBR_data/GBROOS/ANMN_Moorings/MYR_MyrmidonSlope/IMOS_ANMN-QLD_VATPE_20081128T223000Z_GBRMYR_FV01_GBRMYR-0811-Continental-193_END-20090526T203100Z_C-20120205T150851Z.nc'
#inFileName_GBROOS[4] = '/home/cthomas/GBR_data/GBROOS/ANMN_Moorings/ELR_ElusiveReef/IMOS_ANMN-QLD_VATPE_20081024T083000Z_GBRELR_FV01_GBRELR-0810-Continental-193_END-20090418T214300Z_C-20120205T160754Z.nc'
#inFileName_GBROOS[5] = '/home/cthomas/GBR_data/GBROOS/ANMN_Moorings/CCH_CapricornChannel/IMOS_ANMN-QLD_VATPE_20081102T200000Z_GBRCCH_FV01_GBRCCH-0811-Workhorse-ADCP-81_END-20090419T223200Z_C-20120205T160707Z.nc'
#inFileName_GBROOS[6] = '/home/cthomas/GBR_data/GBROOS/ANMN_Moorings/HIN_HeronIslandNorth/IMOS_ANMN-QLD_VATE_20081027T000000Z_GBRHIN_FV01_GBRHIN-0810-Workhorse-ADCP-41_END-20090420T051200Z_C-20120205T160830Z.nc'
#inFileName_GBROOS[7] = '/home/cthomas/GBR_data/GBROOS/ANMN_Moorings/HIS_HeronIslandSouth/IMOS_ANMN-QLD_VATE_20081029T063000Z_GBRHIS_FV01_GBRHIS-0810-Workhorse-ADCP-44_END-20090423T043000Z_C-20120205T160927Z.nc'
#inFileName_GBROOS[8] = '/home/cthomas/GBR_data/GBROOS/ANMN_Moorings/OTE_OneTreeIsland/IMOS_ANMN-QLD_VATPE_20081028T230000Z_GBROTE_FV01_GBROTE-0810-Workhorse-ADCP-54_END-20090421T042700Z_C-20120205T161040Z.nc'

inFileNameBulk_SLIM = '/home/cthomas/GBR_slim/Saved_Data/Hydro_Calibration/gbr330K_sgbr/2svIn_bDf10_Realistic_4wks/Evaluator/evalCurrentGBROOS_0'
#inFileNameBulk_SLIM = '/home/cthomas/GBR_slim/Output/Evaluator/evalCurrentGBROOS_0'
#=====================
# Define SLIM simulation start time:
SLIM_startTime = datetime.datetime(2007, 11, 28)
#=====================
# Plot only GBROOS data or also SLIM data?
plotOnlyGBROOS = 0
#=====================
# Plot U or V velocities? U=1, V=2
plotUorV = 1
UorV=''
if plotUorV == 1:
  UorV='U'
else:
  UorV='V'
#=====================
# Average all currents over 24hrs? No=0, Yes=1
averageOver24hrs = 1
#=====================

def loadDatasets(inFileName_GBROOS, inFileName_SLIM, SLIM_startTime):
  # 1. Read GBROOS data from netCDF file
  f = netcdf.netcdf_file(inFileName_GBROOS, 'r')
  #for variable in f.variables:
    #print variable
  
  time = f.variables['TIME']
  ucur = f.variables['UCUR']
  vcur = f.variables['VCUR']
  wcur = f.variables['WCUR']
  height = f.variables['HEIGHT_ABOVE_SENSOR']
  lat = f.variables['LATITUDE']
  lon = f.variables['LONGITUDE']
  f.close()
  print '    -> GBROOS lon, lat: %.4f, %.4f'%( lon[0], lat[0])

  # 1.a) Calc. and record depth-averaged currents
  GBROOS_data = np.zeros( (len(time[:]), 4) )
  GBROOS_nbSteps = GBROOS_data.shape[0]
  for n in range(0,len(time[:])):
    totUCurr = 0.0
    totVCurr = 0.0
    totWCurr = 0.0
    for depthLevel in range(0,len(height[:])):
      if ucur[n][depthLevel][0][0] < 900000:
        totUCurr = totUCurr + ucur[n][depthLevel][0][0]
      if vcur[n][depthLevel][0][0] < 900000:
        totVCurr = totVCurr + vcur[n][depthLevel][0][0]
      if wcur[n][depthLevel][0][0] < 900000:
        totWCurr = totWCurr + wcur[n][depthLevel][0][0]
    GBROOS_data[n,0] = time[n]
    GBROOS_data[n,1] = totUCurr / float(len(height[:]))
    GBROOS_data[n,2] = totVCurr / float(len(height[:]))
    GBROOS_data[n,3] = totWCurr / float(len(height[:]))
  
  # 1.b) Calculate GBROOS residual currents
  GBROOS_res = np.zeros(3)
  for n in range(0, GBROOS_nbSteps):
    for dimension in range(0,3):
      GBROOS_res[dimension] = GBROOS_res[dimension] + GBROOS_data[n,1+dimension]
  for dimension in range(0,3):
    GBROOS_res[dimension] = GBROOS_res[dimension] / float(GBROOS_nbSteps)
  #----------

  ## 2. Read SLIM data
  #SLIM_file = open(inFileName_SLIM, 'r')
  #SLIM_data = np.zeros( (1,3) )
  #for line in SLIM_file.readlines():
    #line = line.split()
    #newLine = np.array( [float(line[0]), float(line[1]), float(line[2])] )
    #SLIM_data = np.vstack(( SLIM_data, newLine ))
  #SLIM_nbSteps = SLIM_data.shape[0]
  
  # 2. Read SLIM data
  print inFileName_SLIM
  if not os.path.exists(inFileName_SLIM):
    print inFileName_SLIM + ' does not exist. Skipping it ...'
    return None, None, None, None
  SLIM_file = open(inFileName_SLIM, 'r')
  SLIM_nbSteps=0
  for line in SLIM_file.readlines():
    SLIM_nbSteps = SLIM_nbSteps+1
  SLIM_file.close()
  SLIM_file = open(inFileName_SLIM, 'r')
  SLIM_data = np.zeros( (SLIM_nbSteps,3) )
  i=0
  for line in SLIM_file.readlines():
    line = line.split()
    SLIM_data[i,0] = float(line[0])
    SLIM_data[i,1] = float(line[1])
    SLIM_data[i,2] = float(line[2])
    i=i+1
  
  # 2.a) Calculate SLIM simulation residual currents
  SLIM_dt = SLIM_data[2,0] - SLIM_data[1,0] #units: hours
  SLIM_itersPerDay = int(24.0/SLIM_dt)
  #print 'SLIM dt:', SLIM_dt, ' so iters per day:', SLIM_itersPerDay
  SLIM_res = np.zeros(2)
  SLIM_res[0] = sum(SLIM_data[SLIM_nbSteps-2*SLIM_itersPerDay: , 1])/(2.0*SLIM_itersPerDay)
  SLIM_res[1] = sum(SLIM_data[SLIM_nbSteps-2*SLIM_itersPerDay: , 2])/(2.0*SLIM_itersPerDay)

  #----------
  
  # 3) Calculate time-averaged currents
  if (averageOver24hrs == 1):
    #1. GBROOS
    dt = GBROOS_data[1,0] - GBROOS_data[0,0] #units: days
    stepsPerDay = int(0.1 + 1.0/dt) #+0.1 as int() rounds down
    print '    -> GBROOS Data: time steps per day: %d, dt = %.6f'%(stepsPerDay, dt*24.0)
    print '    -> SLIM Data: time steps per day: %d, dt = %.6f'%(SLIM_itersPerDay, SLIM_dt)
    GBROOS_U_tA = np.zeros(GBROOS_nbSteps)
    GBROOS_V_tA = np.zeros(GBROOS_nbSteps)
    GBROOS_W_tA = np.zeros(GBROOS_nbSteps)
    for n in range(stepsPerDay/2, GBROOS_nbSteps-stepsPerDay/2 + 1):
      GBROOS_U_tA[n] = GBROOS_data[n-stepsPerDay/2:n+stepsPerDay/2, 1].mean()
      GBROOS_V_tA[n] = GBROOS_data[n-stepsPerDay/2:n+stepsPerDay/2, 2].mean()
      GBROOS_W_tA[n] = GBROOS_data[n-stepsPerDay/2:n+stepsPerDay/2, 3].mean()
    for n in range(0, GBROOS_nbSteps):
      GBROOS_data[n,1] = GBROOS_U_tA[n]
      GBROOS_data[n,2] = GBROOS_V_tA[n]
      GBROOS_data[n,3] = GBROOS_W_tA[n]
    #2. SLIM
    SLIM_U_tA = np.zeros(SLIM_nbSteps)
    SLIM_V_tA = np.zeros(SLIM_nbSteps)
    for n in range(SLIM_itersPerDay/2, SLIM_nbSteps-SLIM_itersPerDay/2 + 1):
      SLIM_U_tA[n] = SLIM_data[n-SLIM_itersPerDay/2:n+SLIM_itersPerDay/2, 1].mean()
      SLIM_V_tA[n] = SLIM_data[n-SLIM_itersPerDay/2:n+SLIM_itersPerDay/2, 2].mean()
    for n in range(0, SLIM_nbSteps):
      SLIM_data[n,1] = SLIM_U_tA[n]
      SLIM_data[n,2] = SLIM_V_tA[n]
      
  #----------

  # 4. Standardise SLIM and GBROOS timescales (GBROOS: days since 1950-01-01 00:00:00 UTC)
  #  1. Convert both start times to datetime objects:
  GBROOS_startTimeOffset = datetime.timedelta(GBROOS_data[0,0]) 
  #print GBROOS_startTimeOffset
  GBROOS_startTime = datetime.datetime(1950, 1, 1) + GBROOS_startTimeOffset
  #print '  --> GBROOS start time:', GBROOS_startTime
  #print '  --> SLIM start time:', SLIM_startTime
  #  2. Convert both datetime objects to number of days since 0001-01-01 00:00:00 UTC plus one
  GBROOS_startTime = dates.date2num(GBROOS_startTime)
  SLIM_startTime = dates.date2num(SLIM_startTime)
  #print '  --> Start times in "dates" format:', GBROOS_startTime, SLIM_startTime
  #  3. Standardise format of time indices of both datasets with number of days since 0001-01-01 00:00:00 UTC plus one
  GBROOS_startTimeOffsetNumeric = GBROOS_data[0,0]
  for n in range(0, GBROOS_nbSteps):
    GBROOS_data[n,0] = GBROOS_startTime + (GBROOS_data[n,0] - GBROOS_startTimeOffsetNumeric)  #<-Avoids timedelta arithmetic at each iter
  for n in range (0, SLIM_nbSteps):
    SLIM_data[n,0] = SLIM_startTime + SLIM_data[n,0]/24.0

  #TEMP: subtract 9 hours from GBROOS!!! (Time difference)
  #print dates.num2date(SLIM_data[0,0]), dates.num2date(SLIM_data[1,0])
  for n in range(0, len(SLIM_data[:,0])):
    SLIM_data[n,0] = SLIM_data[n,0] + 9.0/24.0
  
  return GBROOS_data, SLIM_data, GBROOS_res, SLIM_res

# 4. Plot time-averaged GBROOS currents and SLIM currents
print ''
print '** Graphing time-series **'
# NB: for SLIM_data, start plotting from [1] as [0]s=0
fig = plt.figure()
SLIM_res = [None] * 9
GBROOS_res = [None] * 9
GBROOS_dailyAverages = [None] * 9
SLIM_dailyAverages = [None] * 9
for site in range (0,9):
  if inFileName_GBROOS[site] == '':
    continue
  print '--> Reading GBROOS netCDF data for: ' + siteName[site] + ' <--'
  [GBROOS_data, SLIM_data, GBROOS_res[site], SLIM_res[site]] = loadDatasets(inFileName_GBROOS[site], inFileNameBulk_SLIM+str(site)+'.dat', SLIM_startTime)
  if SLIM_data == None:
    continue
  ax = fig.add_subplot(3, 3, site, label=siteName[site])
  # ----
  if plotOnlyGBROOS == 1:
    ax.plot(dates.num2date(GBROOS_data[:,0]), GBROOS_data[:,plotUorV])
    ax.tick_params(axis='both', which='major', labelsize=10)
    ax.tick_params(axis='both', which='minor', labelsize=8)
    ax.set_title(siteName[site])
    continue
  # ---- 
  # Find the the value of GBROOS_data immediately before the start of SLIM_data
  GBROOS_timeAtSLIMInitTime = 0
  GBROOS_timeAtSLIMEndTime = 0
  for n in range(0, len(GBROOS_data[:,0])):
    if GBROOS_data[n,0] > SLIM_data[1,0]:
      GBROOS_timeAtSLIMInitTime = n-1
      break
  for n in range(GBROOS_timeAtSLIMInitTime, len(GBROOS_data[:,0])):
    if GBROOS_data[n,0] > SLIM_data[len(SLIM_data[:,0])-1,0]:
      GBROOS_timeAtSLIMEndTime = n
      break
  # ----
  # Get daily average values of U and V by 'decimating' the current data (only if we've averaged the currents over 24hrs)
  if averageOver24hrs == 1:
    GBROOS_stepsPerDay = int( 0.1 + 1.0/(GBROOS_data[1,0]-GBROOS_data[0,0]) )
    GBROOS_nbDays = len(GBROOS_data[:,0])/GBROOS_stepsPerDay
    GBROOS_dailyAverages[site] = np.zeros(( GBROOS_nbDays,4 ))
    for n in range(0,GBROOS_nbDays):
      for dimension in range(0,4):
        GBROOS_dailyAverages[site][n,dimension] = GBROOS_data[n*GBROOS_stepsPerDay,dimension]
    SLIM_itersPerDay = int(1.0/(SLIM_data[2,0]-SLIM_data[1,0])) #NB: SLIM_dt is now in days
    SLIM_nbDays = len(SLIM_data[:,0])/SLIM_itersPerDay
    SLIM_dailyAverages[site] = np.zeros(( SLIM_nbDays,3 ))
    print '    -> Time-averages: GBROOS data:', GBROOS_nbDays, 'days, SLIM data:', SLIM_nbDays, 'days.'
    for n in range(0,SLIM_nbDays):
      for dimension in range(0,3):
        SLIM_dailyAverages[site][n,dimension] = SLIM_data[n*SLIM_itersPerDay,dimension]
    # Find the the value of decimated GBROOS_data immediately before the start of SLIM_data
    GBROOS_dA_timeAtSLIMInitTime = 0
    GBROOS_dA_timeAtSLIMEndTime = 0
    for n in range(0, len(GBROOS_dailyAverages[site][:,0])):
      if GBROOS_dailyAverages[site][n,0] > SLIM_dailyAverages[site][0,0]:
        GBROOS_dA_timeAtSLIMInitTime = n-1
        break
    for n in range(GBROOS_dA_timeAtSLIMInitTime, len(GBROOS_dailyAverages[site][:,0])):
      if GBROOS_dailyAverages[site][n,0] > SLIM_dailyAverages[site][len(SLIM_dailyAverages[site][:,0])-1,0]:
        GBROOS_dA_timeAtSLIMEndTime = n
        break
  # ----
  ax.grid(True,linestyle='-',color='0.75')
  ax.plot(dates.num2date(GBROOS_data[GBROOS_timeAtSLIMInitTime:GBROOS_timeAtSLIMEndTime,0]), GBROOS_data[GBROOS_timeAtSLIMInitTime:GBROOS_timeAtSLIMEndTime,plotUorV], dates.num2date(SLIM_data[1:,0]), SLIM_data[1:,plotUorV])
  ax.set_ylabel('Current velocity (m/s)')
  ax.set_title(siteName[site])
  for tick in ax.xaxis.get_ticklabels():
    tick.set_fontsize('12')
    tick.set_rotation(10)
#fig.subplots_adjust(bottom=0.06)
#fig.autofmt_xdate(rotation=10)
fig.subplots_adjust(left=0.06, right=0.94, top=0.9, bottom=0.06, wspace=0.3, hspace=0.4)
fig.suptitle(UorV+' of GBROOS data (blue) and SLIM output (green)', fontsize=14)
#plt.show()

#5. Print residual currents
print''
print'** Residual Currents: **'
print '                      U(obs) U(SLIM) | V(obs) V(SLIM) | percentage diff'
for site in range(0,9):
  if inFileName_GBROOS[site] == '' or SLIM_res[site] == None:
    continue
  siteNameFormatted = '{0: <22}'.format(siteName[site])
  print siteNameFormatted, '%.2f, %.2f | %.2f, %.2f | %.2f, %.2f' % (GBROOS_res[site][0], SLIM_res[site][0], GBROOS_res[site][1], SLIM_res[site][1], 100.0*(SLIM_res[site][0]-GBROOS_res[site][0])/GBROOS_res[site][0], 100.0*(SLIM_res[site][1]-GBROOS_res[site][1])/GBROOS_res[site][1])
print ''

#6. Plot scatter of SLIM and GBROOS currents (only if we've averaged over 24hrs)
if averageOver24hrs == 1:
  figScat = plt.figure()
  for site in range (0,9):
    if inFileName_GBROOS[site] == '' or SLIM_res[site] == None:
      continue
    ax = figScat.add_subplot(3,3,site, label=siteName[site])
    ax.grid(True,linestyle='-',color='0.75')
    ax.scatter(GBROOS_dailyAverages[site][GBROOS_dA_timeAtSLIMInitTime:GBROOS_dA_timeAtSLIMEndTime,2],GBROOS_dailyAverages[site][GBROOS_dA_timeAtSLIMInitTime:GBROOS_dA_timeAtSLIMEndTime,1])
    ax.scatter(SLIM_dailyAverages[site][2:,2],SLIM_dailyAverages[site][2:,1], color='tomato') #Skip first 2 days of SLIM data (spin-up)
    ax.set_xlabel('Easterly current (m/s)')
    ax.set_ylabel('Northerly current (m/s)')
    ax.set_title(siteName[site])
    ax.axhline(y=0, color='k')
    ax.axvline(x=0, color='k')
  figScat.subplots_adjust(left=0.06, right=0.94, top=0.9, bottom=0.06, wspace=0.34, hspace=0.44)
  figScat.suptitle('Scatter graph of North vs East currents (GBROOS: blue, SLIM: red)', fontsize=14)
plt.show()
