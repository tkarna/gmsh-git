/*
 * Writes output to a large Matrix. Range of Matrix (max/min lat/lon) is printed.
 *  Normally:
 *  X(lon) min./max. values: 140.921706943421, 155.884935647662
 *  Y(lat) min./max. values: -25.0026964546776, -8.58661304885311
 *  Range: 14.963228704 (lon), 16.416083406 (lat)
 * 
 * To change resolution, change definitions of I, J.
 * As a guideline, 
 *    ~1km resolution is:
 *    1676(x) x 1860(y)
 *    ~ 200m resolution is:
 *    8380(x) x 9300(y)
 * 
 * To remove unwanted reef areas/reefs scroll to Test #1&2 and change conditions
 * 
 * Function outputs:
 *        .dat file with ZxZ solid matrix of reefs. Use this to seed reefs in SLIM-LPT and/or compute bottomDrag in SLIM-Hydro
 *        .dat ReefMappingId file: This lists in the left column 'Our' reefId number (as used when seeding/tracking reefs), in the right column coralreef2010_GBR.shp's reefId number for the same reef 
 *                                  (can use to identify source/sink reefs with coralreef2010_GBR.shp layer)
 * 
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "shapefil.h"

 void addPointInt(int, int, int reef_num, int**); 
 int intersection(double*,double*,double*,double*,double*);

 int main() 
 {
   int nEntities, nShapeType, i, j;
   int maxX, minX, maxY, minY;
   double adfMinBound[4], adfMaxBound[4];

   int **data;
   int I = 9200+1;
   int J = 9200+1;
//    int I = 14000+1;
//    int J = 14000+1;
   data = (int **)malloc(I*sizeof(int*));
   for(i=0;i<I;i++)
     data[i]=(int*)malloc(J*sizeof(int));
   
   for (i=0; i<I; i++) {
     for (j=0; j<J; j++) data[i][j] = 0;
   }
   
   // Projection Parameters
   double pi;
   pi = 4.0*atan(1.0);
   printf( "pi is %.15g\n",pi );

   //Initialise output files
   SHPHandle newData;
//    FILE *outFile;
   FILE *outFileDat;
//   FILE *datFileCentres;
   FILE *outFileIdMap;
//    outFile = fopen("./Reefs_GBR.pos","a+");
//    fprintf( outFile, "View \"Reef map\" {\n");
//    outFileDat = fopen("./GBR_ReefMap_Solid9200.dat","w");
   outFileDat = fopen("./SGBR_DeepReefMap_2121_Solid9200.dat","w");
//    outFileDat = fopen("./CGBR_ReefMapDeep_Cropped_Sma_1093_Solid9200.dat","w");
   
   //   datFileCentres = fopen("./SGBR_Reefs_Sma_GBR_centres.dat","w");   
//    outFileIdMap = fopen("./GBR_ReefMap_ReefIdMapping_9200.dat","w");
   outFileIdMap = fopen("./SGBR_DeepReefMap_2121_ReefIdMapping.dat","w");
//    outFileIdMap = fopen("./CGBR_ReefMapDeep_Cropped_Sma_1093_ReefIdMapping_9200.dat","w");
   
   //Load Shapefile data
   newData = SHPOpen( "/home/cthomas/Data/Coral Reefs Distro 2010/coralreef2010/coralreef2010_GBR", "rb" );
//    newData = SHPOpen( "/home/cthomas/GBR_data/GBR_Banks_Layer/Banks_GBR", "rb" );
   SHPGetInfo( newData, &nEntities, &nShapeType, adfMinBound, adfMaxBound );
   printf( "\nNo. of Entities: %d, Shape type: %s\n", nEntities, SHPTypeName( nShapeType ) );
   printf( "X min./max. values: %.15g,%.15g\nY min./max. values: %.15g,%.15g\n\n", adfMinBound[0], adfMaxBound[0], adfMinBound[1], adfMaxBound[1] );
   printf( "Total delta_lon = %.15g, delta_lat = %.15g\n", adfMaxBound[0]-adfMinBound[0], adfMaxBound[1]-adfMinBound[1] );
   printf( "dlon = %.15g, dlat = %.15g\n", (adfMaxBound[0]-adfMinBound[0])/(double)(I-1.0), (adfMaxBound[1]-adfMinBound[1])/(double)(J-1.0) );
   
   //Write header to output file
   double dx, dy;
   dx = (adfMaxBound[0]-adfMinBound[0])/(double)(I-1.0);
   dy = (adfMaxBound[1]-adfMinBound[1])/(double)(J-1.0);
   fprintf( outFileDat, "%.15g %.15g 0.0 %.15g %.15g 1.0 %d %d 1\n", adfMinBound[0], adfMinBound[1], dx, dy, I-1, J-1);
   
   //Loop over all shapes
   int reefCounter = 1;
   for (i=0; i<nEntities; i++) 
   {
      maxX=-10000000; minX=10000000; maxY=-10000000; minY=10000000;
      SHPObject *currentShape;
      currentShape = SHPReadObject( newData, i );
//      printf("Entering shape %d. Type: %s\n", i, SHPTypeName(currentShape->nSHPType));
      if ( currentShape == NULL )
      {
        fprintf( stderr, "Unable to read shape %d, terminating object reading.\n", i );
        break;
      }
      
      int numVertices;
      numVertices = currentShape->nVertices;
      double *polyX;
      polyX = (double*)malloc(numVertices*sizeof(double));
      double *polyY;
      polyY = (double*)malloc(numVertices*sizeof(double));
      //Loop over all vertices
      //Nb:: padfX: Longitude, padfY: Latitude
      for (j=0; j<numVertices; j++)
      {
        double pointCoords[3];

        pointCoords[0] = (currentShape->padfX[j]-adfMinBound[0])*(( (double)I-1.0) /(adfMaxBound[0]-adfMinBound[0]));
        pointCoords[1] = (currentShape->padfY[j]-adfMinBound[1])*(( (double)J-1.0) /(adfMaxBound[1]-adfMinBound[1]));
        
        if (maxX < (int)(pointCoords[0] +1.0)) maxX = (int)(pointCoords[0] +1.0); //1.0: give a little leeway
        if (minX > (int)(pointCoords[0] -1.0)) minX = (int)(pointCoords[0] -1.0);
        
        if (maxY < (int)(pointCoords[1] +1.0)) maxY = (int)(pointCoords[1] +1.0);
        if (minY > (int)(pointCoords[1] -1.0)) minY = (int)(pointCoords[1] -1.0);
        
        polyX[j] = pointCoords[0]; //save x-coord
        polyY[j] = pointCoords[1]; //save y-coord
      }
      
      //temp:
//      printf("Units: metres(abs)==> Max/minX: %d, %d / Max/minY: %d, %d\n", maxX, minX, maxY, minY);
//      printf("Units: MatrixPoints(modif'd)==> Max/minX: %d, %d / Max/minY: %d, %d\n", maxX*(int)(scaleFactor/1000.0)+633*(int)scaleFactor, minX*(int)(scaleFactor/1000.0)+633*(int)scaleFactor, maxY*(int)(scaleFactor/1000.0)+913*(int)scaleFactor, minY*(int)(scaleFactor/1000.0)+913*(int)scaleFactor);
      
      //Test #1: If polygon is in an unwanted area, remove it.
      //      NB: The following uses a series of boxes to remove unwanted reefs
      if (currentShape->dfXMax < 142.0 || currentShape->dfXMin > 154.396 || currentShape->dfYMax < -25.8724 || currentShape->dfYMin > -11.0 || (currentShape->dfXMin > 146.8 && currentShape->dfYMin > -18.0) || (currentShape->dfXMin > 145.8 && currentShape->dfYMin > -15.0) || (currentShape->dfXMin > 151.2 && currentShape->dfYMin > -20.0)  || (currentShape->dfXMin > 149.21 && currentShape->dfYMin > -18.25) || (currentShape->dfXMin > 153.36 && currentShape->dfYMin > -22.05) || (currentShape->dfXMin > 153.27 && currentShape->dfYMin < -24.915) 
      ) continue;
        //Larger zoomed area:
//       || (currentShape->dfYMin - currentShape->dfXMin > -146.1 - 17.4)
//       || (currentShape->dfYMax - currentShape->dfXMax < -150.48 - 22.2) ) continue;
      //Smaller zoomed area:
//       || (currentShape->dfYMin - currentShape->dfXMin > -146.708679 - 18.804918)
//       || (currentShape->dfYMax - currentShape->dfXMax < -150.07 - 22.12) ) continue;
      //Entire Central & South GBR:
//      || (currentShape->dfYMin - currentShape->dfXMin > -145.8 - 16.5) ) continue;
      
      //Torres Strait:
//       if (currentShape->dfXMax < 140.0 || currentShape->dfXMin > 145.0 || currentShape->dfYMax < -12.0 || currentShape->dfYMin > -6.0 )continue;
      
      //Test #2: If polygon perimeter < threshold, don't count it.
      double p0_x, p0_y, p1_x, p1_y, polySideLength=0.0;
      for (j=0; j<numVertices; j++)
      {
        p0_x = currentShape->padfX[j]; //save x-coord
        p0_y = currentShape->padfY[j];
        p1_x = currentShape->padfX[(j+1)%numVertices];
        p1_y = currentShape->padfY[(j+1)%numVertices];
        
        polySideLength += sqrt( (p1_x-p0_x)*(p1_x-p0_x) + (p1_y-p0_y)*(p1_y-p0_y) );
      }
//      printf("polySideLength is %.10g\n", polySideLength);
      if (polySideLength < 0.016) continue;
      
      //We are counting this polygon as a reef.
      //Loop over all vertices to calculate centre of polygon
      double xCentre=0, yCentre=0;      
      for (j=0; j<numVertices; j++)
      {
        xCentre += currentShape->padfX[j];
        yCentre += currentShape->padfY[j];
      }
      xCentre = xCentre/numVertices;
      yCentre = yCentre/numVertices;
//      fprintf( datFileCentres, "%d %.20g %.20g\n", reefCounter, xCentre, yCentre );
      //Write polygon's Id and reefCounter Id to ReefId mapping file
      fprintf( outFileIdMap, "%d %d\n", reefCounter, currentShape->nShapeId );
      //Loop over every row of MiniGrid and at every point a) check for intersections, b) fill point if intersSoFar = odd
      int row, nodeX[numVertices], pixelX=0, vertex;
      double p0[2], p1[2], p2[2], p3[2], inter[2];
      
      //Vertical loop over all rows
      for (row=maxY; row>=minY; row--)
      {
        int intersWithRow=0;
        p0[0] = (double)minX;
        p0[1] = (double)row;
        p1[0] = (double)maxX;
        p1[1] = (double)row;
        
        //Loop over all polygon sides
        for (vertex=0; vertex<numVertices; vertex++)
        {
          p2[0] = polyX[vertex];
          p2[1] = polyY[vertex];
          p3[0] = polyX[(vertex+1)%numVertices];
          p3[1] = polyY[(vertex+1)%numVertices];
          
          if (intersection(p0,p1,p2,p3,inter) == 1) {
            //modification:
            if (inter[0] > (double)(maxX+1.0) || inter[0] < (double)(minX-1.0)) {printf("Error #381! Continuing..."); continue;}
            nodeX[intersWithRow] = (int)(inter[0]+0.5);     //Record location of intersection along row
            intersWithRow++;
          }
        }

        
        //We now have a list of all the points of intersections along the row, unordered.
        int m=0, swap, counter=0;
        if (intersWithRow%2!=0 && intersWithRow!=0) {
//          printf("ERROR: Odd number of intersections... something funny's going on!\n intersections: %d\n row:%d\npolygon: %d\nmax/minX & max/minY: %d, %d / %d, %d\n", intersWithRow, row, i, maxX, minX, maxY, minY);
          for (counter=0; counter<intersWithRow; counter++) {
//            printf("Node at x= %d,\n",nodeX[counter]); 
          }
          return 0;
        }
        //  Sort the nodes, via a simple “Bubble” sort.
        m=0;
        while (m<intersWithRow-1) {
          if (nodeX[m]>nodeX[m+1]) {
            swap=nodeX[m]; 
            nodeX[m]=nodeX[m+1]; 
            nodeX[m+1]=swap; 
            if (m) m--; 
          }
          else m++;
        }

        //Nodes sorted.
        //Now fill line:
        int nodeNum;
        for (nodeNum=0; nodeNum<intersWithRow; nodeNum+=2) {
          //If node# is odd (ie. even in C-array counting), loop over all pixels to next node.
          //NB: COUNT the number of errors to file if problems persist/bad quality results!! (record shape, vertices etc.)
          if   (nodeX[nodeNum]>=maxX){ printf("ERROR: nodeX>=maxX");break; }
          if   (nodeX[nodeNum]<minX){ printf("ERROR: nodeX<maxX");break; }
          for (pixelX=nodeX[nodeNum]; pixelX <= nodeX[nodeNum+1]; pixelX++) {
            addPointInt(pixelX, row, reefCounter, data);
          }
        }
//printf(" intersections with this row: %d, row %d of (-)%d\n",intersWithRow, row-maxY, maxY-minY);
      }
//printf(" number of vertices of this shape: %d\n Box bounds: max/minX at %d, %d, max/minY at %d, %d\n", numVertices, maxX, minX, maxY, minY);
   SHPDestroyObject( currentShape );
   reefCounter++;
   }//END: Loop over polygons
   
   SHPClose(newData);
//    fprintf( outFile, "};");
//    fclose(outFile);
   
   for (j=0; j<J-1; j++) {
     for (i=0; i<I-1; i++) {
//        if (data[i][j] > 0) data[i][j] = 1;
       fprintf( outFileDat, "%d ", data[i][j] );
     }
     fprintf( outFileDat, "\n" );
   }
   
   for (i=0; i<I; i++){
     free(data[i]);
   }
//   fclose(datFileCentres);  
   fclose(outFileIdMap);
   free(data);
   printf("Total number of indexed reefs: %d\nThank you and good night.\n", reefCounter-1);
   exit(0);
 }
 
 void addPointInt(int x, int y, int reef_num, int** data) 
 {
   //   double extent_lon = 155.884935647662-140.921706943421;
   //   double extent_lat = -25.0026964546776+8.58661304885311;
   //   x = (x-146.5892)*1647.046/extent_lon;
   //   y = (y+8.58661304885311)*1816.823/extent_lat;
//   printf("-> In addPointInt. x = %.d, y = %.d\n", x, y);

//   printf("\tx_int = %d \ty_int = %d\treef_num = %d\n",x_int, y_int, reef_num);
   data[x][y]=reef_num;

 }


int intersection(double p1[2],double p2[2],double p3[2],double p4[2],double inter[2])
{
  double x1,y1;
  double x2,y2;
  double x3,y3;
  double x4,y4;
  double ua,ub;
  double num_ua;
  double num_ub;
  double denom;
  double e;
  x1 = p1[0];
  y1 = p1[1];
  x2 = p2[0];
  y2 = p2[1];
  x3 = p3[0];
  y3 = p3[1];
  x4 = p4[0];
  y4 = p4[1];
  e = 0.000001;
  denom = (y4-y3)*(x2-x1) - (x4-x3)*(y2-y1);
  if(fabs(denom)<e){
    inter[0]=0.0;
    inter[1]=0.0;
    return 0;
  }
  num_ua = (x4-x3)*(y1-y3) - (y4-y3)*(x1-x3);
  num_ub = (x2-x1)*(y1-y3) - (y2-y1)*(x1-x3);
  ua = num_ua/denom;
  ub = num_ub/denom;
  if(ua<=1.0 && ua>=0.0 && ub<=1.0 && ub>=0.0){
    inter[0] = x1+ua*(x2-x1);
    inter[1] = y1+ua*(y2-y1);
    return 1;
  }
  else{
    inter[0]=0.0;
    inter[1]=0.0;
    return 0;
  }
}
