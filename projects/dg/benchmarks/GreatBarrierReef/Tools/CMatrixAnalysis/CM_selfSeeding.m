% Function which returns the <percentageWanted>% of reefs which have the highest
% self-seeding as a proportion of:   a) larvae seeded by that reef:
%                                       <mostSSbyLarvaeSeeded>
%                                    b) larvae seded by that reef that have
%                                    subsequently recruited:
%                                       <mostSSbyRecruitedLarvae>

function [mostSSbyLarvaeSeeded, mostSSbyRecruitedLarvae] = CM_selfSeeding(CM, seedingArray, reefLocations, reefList, reefListOnlyFlag, percentageWanted)

fprintf('\nFinding reefs with highest proportion of self-seeding.\n');

nbWeeks = length(CM);
mostSSbyRecruitedLarvae = cell(nbWeeks, 1);
mostSSbyLarvaeSeeded = cell(nbWeeks, 1);

nonZeroSeedingReefs = find(seedingArray{1});
seedingArray{1} = seedingArray{1}(nonZeroSeedingReefs);

nbReefsWanted = int16( (percentageWanted/100.0) * length(CM{1}) );
fprintf('Percentage of reefs selected for self-seeding list: %d%% (%d reefs)\n', percentageWanted, nbReefsWanted);

for week = 1:1:nbWeeks
    CMatrix = CM{week};
    
    selfSeedingArray = zeros(length(CMatrix),2);
    reefsSeededRecruited = zeros(length(CMatrix), 1);
    
     for row = 1:1:length(CMatrix);
%         
%         % If reefListOnly flag = 1, only consider reefs in reefList:
%         if reefListOnlyFlag == 1
%             if ismember(row, reefList)~=1
%                 continue;
%             end
%         % If reefListOnly flag = -1, only consider reefs NOT in reefList:    
%         elseif reefListOnlyFlag == -1
%             if ismember(row, reefList)==1
%                 continue;
%             end
%         % Else consider all reefs
%         end
        
        selfSeedingArray(row, 1) = CMatrix(row, row);
        selfSeedingArray(row, 2) = row;
        reefsSeededRecruited(row) = sum(CMatrix(row,:));
    end
    
    % a) List most highly self-seeded reefs by self-seeding as a proportion
    % of total larvae seeded
    selfSeedingArrayNormalised = selfSeedingArray(nonZeroSeedingReefs, :);
    selfSeedingArrayNormalised(:,1) = selfSeedingArray(nonZeroSeedingReefs,1) ./ seedingArray{1};
    selfSeedingArraySorted = sortrows(selfSeedingArrayNormalised, 1);
    mostSSreefs = selfSeedingArraySorted( length(selfSeedingArraySorted)-(nbReefsWanted-1):length(selfSeedingArraySorted), 2 );
    
    reefLonLat = zeros(length(mostSSreefs), 2);
    for i = 1:1:length(mostSSreefs)
        reefLonLat(i,:) = reefLocations(mostSSreefs(i),:);
    end
    mostSSbyLarvaeSeeded{week} = reefLonLat;
    
    
    % b) List most highly self-seeded reefs by self-seeding as a proportion 
    % of larvae seeded from that reef that subsequently recruited
    nonZeroRecruitingReefs = find(reefsSeededRecruited);
    reefsSeededRecruited = reefsSeededRecruited(nonZeroRecruitingReefs);
    
    selfSeedingArrayNormalised = selfSeedingArray(nonZeroRecruitingReefs,:);
    selfSeedingArrayNormalised(:,1) = selfSeedingArray(nonZeroRecruitingReefs,1) ./ reefsSeededRecruited;
    selfSeedingArraySorted = sortrows(selfSeedingArrayNormalised, 1);
    mostSSreefs = selfSeedingArraySorted( length(selfSeedingArraySorted)-(nbReefsWanted-1):length(selfSeedingArraySorted), 2 );
    
    reefLonLat = zeros(length(mostSSreefs), 2);
    for i = 1:1:length(mostSSreefs)
        reefLonLat(i,:) = reefLocations(mostSSreefs(i),:);
    end
    mostSSbyRecruitedLarvae{week} = reefLonLat;
    
end

end