% Function to calculate strength of connectivity at different length scales
% Output:
% -> connArray: (Cell of matrices with) col1: connection strength, col2: distance, of every connection. 
%     Cell index: week
% -> connLengths: (Cell of arrays with) weighted connectivity length for every reef.
%     Cell index: week
% -> connLengthsStdev: (Cell of arrays with) Stdev of weighted connectivity
%                       lengths
% -> connStrengthVsDistance: total strength of all connections against distance (element #)

function [] = CM_shallowDeepInteraction(CM, shallowReefLocations, deepReefLocations, nbShallowReefs, plotAllWeeks)

fprintf('\nINTERACTIONS BETWEEN SHALLOW AND DEEP REEFS:\n');
nbWeeks = length(CM);

for week = 1:1:nbWeeks
    CMatrix = CM{week};
    
    %Select sha-deep interaction matrices
    CM_ShaToDeep = CMatrix(1:nbShallowReefs, nbShallowReefs+1:length(CMatrix));
    CM_DeepToSha = CMatrix(nbShallowReefs+1:length(CMatrix), 1:nbShallowReefs);
    
    %Calc. nb of sha->deep & deep->sha connections as proportion of total
    %connections
    if week == nbWeeks && plotAllWeeks == 0
        fprintf('Proportion of total connections that are:\n Shallow->Shallow: %.4g\n Shallow->Deep: %.4g\n Deep->Deep: %.4g\n Deep->Shallow: %.4g\n\n', 100.0*sum(sum(CMatrix(1:nbShallowReefs,1:nbShallowReefs)))/sum(sum(CMatrix)), 100.0*sum(sum(CM_ShaToDeep))/sum(sum(CMatrix)), 100.0*sum(sum(CMatrix(nbShallowReefs+1:length(CMatrix),nbShallowReefs+1:length(CMatrix))))/sum(sum(CMatrix)), 100.0*sum(sum(CM_DeepToSha))/sum(sum(CMatrix)) );
    end
    for row = 1:1:length(CMatrix)
            for col = 1:1:length(CMatrix)
                if ( CMatrix(row, col) == 0 ) 
                    continue;
                end
                %Continue if this is not a sha-deep interaction element
                if (row <= nbShallowReefs && col <= nbShallowReefs) || (row > nbShallowReefs && col > nbShallowReefs)
                    continue;
                end
            
            end
    end
    
end

end