% Function to calculate strength of connectivity at different length scales
%
%INPUTS:
% -> reefsList_<seeding>/<settling>: CM_scales will only consider
% connections between reefs seeded over reefs in reefsList_seeding and
% settled over reefs in reefsList_settling.
%   NB: if a reefsList=0, then all reefs are considered
%
% Output:
% -> connArray: (Cell of matrices with) col1: connection strength, col2: distance, of every connection. 
%     Cell index: week
% -> connLengths: (Cell of arrays with) weighted connectivity length for every reef.
%     Cell index: week
% -> connLengthsStdev: (Cell of arrays with) Stdev of weighted connectivity
%                       lengths
% -> connStrengthVsDistance: total strength of all connections against distance (element #)

function [connArray, connLengths, connLengthsStdevs, connectionsByDistanceHistc, connStrengthVsDistance] = CM_scales(CM, reefArray, reefsList_Seeding, reefsList_Settling, plotAllWeeks)

fprintf('\nCONNECTIVITY LENGTH SCALE ANALYSIS:\n');

%array measuring connectivity at each scale:
nbWeeks = length(CM);
nbBins = 501;

connArray = cell(nbWeeks, 1);
connStrengthVsDistance = cell(nbWeeks, 1);
connLengths = cell(nbWeeks, 1);
connLengthsStdevs = cell(nbWeeks, 1);
connLengthTotal = zeros(nbWeeks,1);

sum_reefsList_Seeding = sum(reefsList_Seeding);
sum_reefsList_Settling = sum(reefsList_Settling);

for week = 1:1:nbWeeks
    CMatrix = CM{week};

    connArrayNow = zeros(nnz(CMatrix), 2);
    connStrengthVsDistanceNow = zeros(nbBins, 1);
    connLengthsNow = zeros(length(CMatrix),1);
    connLengthsVarianceNow = zeros(length(CMatrix),1);
    totalConnStrengthTimesLength=0;
    totalConnectivity = 0;
    
    connectionCounter=1;
    reefConnStrengthTimesLengthTotal=0;
    
    for row = 1:1:length(CMatrix)
        
        % If reefsList_Seeding is non-null, only consider reefs in the list:
        if sum_reefsList_Seeding > 0
            if ismember(row, reefsList_Seeding)~=1
                continue;
            end
        % Else consider all reefs
        end
        
        % Reset reef connection strength and conn strength*link length counters
        reefConnStrengthTimesLength=0;
        
        for col = 1:1:length(CMatrix)
            % If element is zero, skip it
            if ( CMatrix(row, col) == 0 ) 
                continue;
            end
            
            % If reefsList_Settling is non-null, only consider reefs in the list:
            if sum_reefsList_Settling > 0
                if ismember(col, reefsList_Settling)~=1
                    continue;
                end
            % Else consider all reefs
            end
            
            reefSeparation = getDistanceBetweenPoints(reefArray(row,2), reefArray(row,1), reefArray(col,2), reefArray(col,1));
            
            % Add connection to connArray
            connArrayNow(connectionCounter,1) = CMatrix(row, col);
            connArrayNow(connectionCounter,2) = reefSeparation;
            connectionCounter=connectionCounter+1;
            
            % Add connectivity strength to appropriate connStrengthVsDistance cell
            reefSeparationInt = ceil(reefSeparation);
            if (reefSeparationInt+1) > nbBins
                reefSeparationInt = nbBins-1;
            end
            connStrengthVsDistanceNow(reefSeparationInt+1) = connStrengthVsDistanceNow(reefSeparationInt+1) + CMatrix(row, col);
            
            % Update weighted connectivity length counter for this reef
            reefConnStrengthTimesLength = reefConnStrengthTimesLength + CMatrix(row, col)*reefSeparation;
            
            % Update weighted connectivity length counter for all reefs
            reefConnStrengthTimesLengthTotal = reefConnStrengthTimesLengthTotal + CMatrix(row, col)*reefSeparation;
            
            % Count connection to total connectivity:
            totalConnectivity = totalConnectivity + CMatrix(row, col);
        end
        totalConnStrengthTimesLength = totalConnStrengthTimesLength + reefConnStrengthTimesLength;
        % Record weighted connectivity length for this (row) reef
        totalReefConnectivity = sum(CMatrix(row,:));
        if totalReefConnectivity==0
            connLengthsNow(row) = 0;
            connLengthsVarianceNow(row) = 0;
        else
            connLengthsNow(row) = reefConnStrengthTimesLength/totalReefConnectivity;
            % Calc. variance:
            reefConnLengthVariance=0;
            for col = 1:1:length(CMatrix)
                if ( CMatrix(row, col) == 0 ) 
                    continue;
                end
                reefSeparation = getDistanceBetweenPoints(reefArray(row,2), reefArray(row,1), reefArray(col,2), reefArray(col,1));
                reefConnLengthVariance = CMatrix(row,col)*(reefSeparation - connLengthsNow(row))^2;
            end
            reefConnLengthVariance = reefConnLengthVariance/totalReefConnectivity;
            connLengthsVarianceNow(row) = reefConnLengthVariance;
        end
    end
%    fprintf('Test: reefConnStrengthTimesLengthTotal = %d, totalConnStrengthTimesLength = %d\n',reefConnStrengthTimesLengthTotal, totalConnStrengthTimesLength);
    connLengthTotal(week) = totalConnStrengthTimesLength/totalConnectivity;
    
    connArray{week} = connArrayNow;
    connStrengthVsDistance{week} = connStrengthVsDistanceNow;
    connLengths{week} = connLengthsNow;
    connLengthsStdevs{week} = sqrt(connLengthsVarianceNow);
end

fprintf('Test: Tot. conn. = %d = %d ~=(always) %d\n', totalConnectivity, sum(connArray{nbWeeks}(:,1)), sum(sum(CMatrix)));

%Remove reefs with zero weighted connectivity length
for week = 1:1:nbWeeks
    connLengthsNow = connLengths{week};
    connLengthsStdevNow = connLengthsStdevs{week};
    for row = length(connLengthsNow):-1:1
        if connLengthsNow(row) == 0
            connLengthsNow(row,:) = [];
        end
        if connLengthsStdevNow(row) == 0
            connLengthsStdevNow(row,:) = [];
        end
    end
    connLengths{week} = connLengthsNow;
    connLengthsStdevs{week} = connLengthsStdevNow;
end

% Generate histograms for every week
nbBinsInternal = 500;
connectionsByDistanceHistc = zeros(nbBinsInternal, nbWeeks);
categories = linspace(0, nbBinsInternal+1, nbBinsInternal);
categories(nbBinsInternal) = inf;
categories(1)=-inf;
% Histogram of nb of connections by distance
for week=1:1:nbWeeks
    connArrayNow = connArray{week};
    connectionsByDistanceHistc(:,week) = histc( connArrayNow(:,2), categories );
end

% % Plot of connection strength (y) vs distance (x)
% figure
% %for week=1:1:nbWeeks
%     connArrayNow = connArray{nbWeeks};
%     scatter(connArrayNow(:,2), connArrayNow(:,1));
%     title('Plot of connection strength (y) vs distance (x)')
% %end

% Plot of connectivity strength vs distance
figure
%for week=1:1:nbWeeks
    connStrengthVsDistanceNow = connStrengthVsDistance{nbWeeks};
    bar(connStrengthVsDistanceNow, 'histc');
    title('Histogram showing strength of connections vs distance from source reef');
%end

figure
if plotAllWeeks == 1
    for week=1:1:nbWeeks
        subplot(2,2,week);
        bar(connectionsByDistanceHistc(:,week), 'hist');
    end
else
    bar(connectionsByDistanceHistc(:,nbWeeks), 'hist');
    title('Histogram showing number of connections vs distance from source reef for all connections');
end


fprintf('\nConnectivity after each week compared to end of last week:\n');
for week=1:1:nbWeeks
    connArrayNow = connArray{week};
    connArrayEnd = connArray{nbWeeks};
    b = sum(connArrayNow(:,1));
    c = b / sum(connArrayEnd(:,1));
    fprintf(' week %d:  %.4g %%\n', week, c*100);
end
fprintf('\nProportion of connections =< 0km; 5km; 10km:\n');
for week=1:1:nbWeeks
    connStrengthVsDistanceNow = connStrengthVsDistance{week};
    b = sum(connStrengthVsDistanceNow);
    zero = connStrengthVsDistanceNow(1)/b * 100.0;
    five = sum(connStrengthVsDistanceNow(1:6))/b * 100.0;
    ten = sum(connStrengthVsDistanceNow(1:11))/b * 100.0;
    fprintf(' week %d:  %.4g%%  %.4g%%  %.4g%%\n', week, zero, five, ten);
end
fprintf('\nMean of weighted connectivity lengths (including self-seeding):\n');
for week=1:1:nbWeeks
    b = mean(connLengths{week});
    fprintf(' week %d:  %.4g\n', week, b);
end
fprintf('\nTotal weighted connectivity length (including self-seeding):\n');
for week=1:1:nbWeeks
    b = connLengthTotal(week);
    fprintf(' week %d:  %.4g\n', week, b);
end

% figure
% subplot(1,2,1);
% bar(connLengths{nbWeeks})
% title('Weighted connectivity length scales')
% subplot(1,2,2);
% bar(connLengthsStdevs{nbWeeks})
% title('Weighted connectivity length stdev')

fprintf('\nProportion of long- and very long-distance connectivity (>100km, >250km) after each week:\n');
for week=1:1:nbWeeks
    connStrengthVsDistanceNow = connStrengthVsDistance{week};
    b = sum(connStrengthVsDistanceNow);
    LDc = sum(connStrengthVsDistanceNow(101:nbBins))/b * 100.0;
    VLDc = sum(connStrengthVsDistanceNow(251:nbBins))/b * 100.0;
    fprintf(' week %d:  >100km: %4g %%, >250km: %4g %%\n', week, LDc, VLDc);
end


end
