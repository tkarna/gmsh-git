% Function to return the distance between two points on the Earth in km
% Uses haversine formula (spherical law of cosines commented - identical)
%   -> see: (http://www.movable-type.co.uk/scripts/latlong.html)
%
% Points must be supplied in (lat, lon) format, in degrees

function [distance] = getDistanceBetweenPoints(lat1, lon1, lat2, lon2)

    lon1 = (lon1*pi)/180.0;
    lat1 = (lat1*pi)/180.0;
    lon2 = (lon2*pi)/180.0;
    lat2 = (lat2*pi)/180.0;
    R = 6371.0; %kilometres
%     fprintf('%f, %f, %f, %f', lon1, lat1, lon2, lat2);
%     distance = acos( sin(lat1)*sin(lat2)+cos(lat1)*cos(lat2)*cos(lon2-lon1) )*R;
%     fprintf('%f',distance);

    deltaLat = lat2-lat1;
    deltaLon = lon2-lon1;
    a = sin(deltaLat/2)*sin(deltaLat/2) + cos(lat1)*cos(lat2)*sin(deltaLon/2)*sin(deltaLon/2);
    c = 2*atan2(sqrt(a), sqrt(1-a));
    distance = R*c;
end