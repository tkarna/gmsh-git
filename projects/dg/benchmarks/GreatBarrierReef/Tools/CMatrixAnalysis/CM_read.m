%Function to read connectivity matrices.
% -> Reads from folderPath/CPU<cpu>/ folders, where <cpu> goes from 1 to nbCPUs
% -> Outputs to cell array, w/ each cell containing a CM a week apart
% -> seedingMatrix: (1-element cell): matrix with nb of particles seeded per reef (==diagonal matrix)

function [CM, seedingArray] = CM_read(folderPath, nbWeeks, nbCPUs, oneWeekIters, nbShallowReefs, settleFlag)

fprintf('\n================\nScript starting.\nGrouping together connectivity matrices.\n %d runs.\n %d weeks.\n Output every %d iterations.\n Reading data from:\n   %s.\n', nbCPUs, nbWeeks, oneWeekIters, folderPath);

%Create a cell array that contains 3 elements, each a matrix
CM = cell(nbWeeks,1);

%Cycle through all connectivity matrices
for week = 1:1:nbWeeks
    weekName = sprintf('%06d', week*oneWeekIters);
    for cpuNb = 1:1:nbCPUs
        if (settleFlag == 1)
            filename = [folderPath, 'CPU', int2str(cpuNb), '/settlementMatrix', weekName, '.dat'];
        else
            filename = [folderPath, 'CPU', int2str(cpuNb), '/connectivityMatrix', weekName, '.dat'];
        end
        newCM = importdata(filename,' ');
        %Necessary to initialise matrix size first time around:
        if (cpuNb==1)
            CM{week} = newCM;
        else
            CM{week} = newCM + CM{week};
        end
    end
end

%Remove first (dummy) row & col:
%NB: Only needed for pre-22/03/2012 CMatrices
% for i=1:1:nbWeeks
%     tempCM = CM{i};
%     tempCM(1,:) = [];
%     tempCM(:,1) = [];
%     CM{i} = tempCM;
% end

% Load initial seeding matrices
seedingArray = cell(1, 1);
for cpuNb = 1:1:nbCPUs
    if (settleFlag == 1)
        filename = [folderPath, 'CPU', int2str(cpuNb), '/settlementMatrix_initial.dat'];
    else
        filename = [folderPath, 'CPU', int2str(cpuNb), '/connectivityMatrix_initial.dat'];
    end
    newCM = importdata(filename,' ');
    %Necessary to initialise matrix size first time around:
    if (cpuNb==1)
        seedingArray{1} = newCM;
    else
        seedingArray{1} = newCM + seedingArray{1};
    end
end
seedingArray{1} = diag( seedingArray{1} );

fprintf(' Total nb of particles seeded is %d (%d per cpu)\n', sum(seedingArray{1}), sum(seedingArray{1})/nbCPUs );
seedingVector = seedingArray{1};
fprintf(' ... of which over shallow reefs: %d, deep reefs: %d\n',sum( seedingVector(1:nbShallowReefs) ), sum( seedingVector(nbShallowReefs+1:length(seedingVector)) ) );
end