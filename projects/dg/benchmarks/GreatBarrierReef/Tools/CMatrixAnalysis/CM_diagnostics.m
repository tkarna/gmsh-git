% Script to run diagnostics on connectivity matrices

clear all

%PARAMETERS:
%Folder path:
folderPath1 = '/home/cthomas/GBR_slim/Saved_Data/Hydro_Realistic/gbr500K_sma_1svIn_bDf10_allT_windOn_28112007_job9062/LPT_Output/';
%folderPath1 = '/home/cthomas/GBR_slim/Saved_Data/Hydro_Realistic/gbr500K_sma_1_5svIn_bDf10_allT_windOn_28112007_job43592/LPT_Output/';

folderPath2 = 'Agemmifera_RsSs_613558/';
%folderPath2 = 'SettProg_Mort007/';

folderPath = [folderPath1, folderPath2];

%Nb of weeks:
nbWeeks = 4;
%Nb of CPUs:
nbCPUs = 1;
%LPT dt:
dt = 90;
%Nb of iterations in one week:
oneWeekIters = (7*24*3600)/dt;
%Are we SEEDING over shallow or deep reefs? [ 0: shallow, 1: Deep, 2: Both]
seedingOverWhichReefs = 0;
%Are we SETTLING over shallow or deep reefs? [ 0: shallow, 1: Deep, 2: Both]
settlingOverWhichReefs = 0;
%Location of Reef_centres file:
shallowReefLocations = importdata('/home/cthomas/GBR_data/slim_ReefMaps/CGBR_1093/GBR_ReefCentres_Sma_1093.dat', ' ');
deepReefLocations = importdata('/home/cthomas/GBR_data/slim_ReefMaps/CGBR_1093/CGBR_ReefCentresDeep_1093_LonLat.dat', ' ');
%List of nearshore reefs:
nearshoreReefList = importdata('/home/cthomas/GBR_data/slim_ReefMaps/CGBR_1093/GBR_NearShoreReefIds_Sma_1093.txt', '\n');
%Settlement flag: 1 if settlementMatrix, 0 if connectivityMatrix:
settleFlag = 1;
%Plot all weeks or only 1 week in graphs (1: all, 0: only last):
plotAllWeeks = 0;

%Merge reefLocations Lists
nbShallowReefs = 1091; %TODO: read this from LP_runInfo.txt file
shallowReefLocations = shallowReefLocations(1:nbShallowReefs,:);
reefLocations = [shallowReefLocations, 
    deepReefLocations];
%READ DATA:
[CM, seedingArray] = CM_read(folderPath, nbWeeks, nbCPUs, oneWeekIters, nbShallowReefs, settleFlag);

%CALCULATE SELF-SEEDING:
reefListOnlyFlag = 0;
percentageWanted = 20.0;
%[mostSSbyLarvaeSeeded, mostSSbyRecruitedLarvae] = CM_selfSeeding(CM, seedingArray, reefLocations, nearshoreReefList, reefListOnlyFlag, percentageWanted);

%WRITE DATA TO FILE
%CM_write(CM, mostSSbyLarvaeSeeded, mostSSbyRecruitedLarvae, folderPath);

%CALCULATE SHALLOW-DEEP REEF INTERACTION
if (seedingOverWhichReefs == settlingOverWhichReefs) && (settlingOverWhichReefs ~= 2)
    fprintf('There is no shallow-deep reef interaction\n');
else
    CM_shallowDeepInteraction(CM, shallowReefLocations, deepReefLocations, nbShallowReefs, 0);
end


%PLOT DISTANCE BAR CHARTS:

%PLOT PLUME LENGTH BAR CHARTS:
% 1: only consider reefs in reefList,
% 0: consider all reefs
% -1: only consider reefs NOT in reefsList

% reefListOnlyFlag = 1;
% [nearshoreConnArray, nearshoreConnLengths, nearshoreConnLengthsStdevs, nearshoreConnectionsByDistanceHistc, nearshoreConnStrengthVsDistance] = CM_scales(CM, reefLocations, nearshoreReefList, reefListOnlyFlag, plotAllWeeks);
% [nearshorePlumeLength, nearshorePlumes] = CM_plume(CM, reefLocations, nearshoreReefList, reefListOnlyFlag, plotAllWeeks);
%  
% reefListOnlyFlag = -1;
% offshoreConnArray = CM_scales(CM, reefLocations, nearshoreReefList, reefListOnlyFlag, plotAllWeeks);
% [offshorePlumeLength, offshorePlumes] = CM_plume(CM, reefLocations, nearshoreReefList, reefListOnlyFlag, plotAllWeeks);

fprintf('We are now considering all particles:\n');
reefsList_Seeding = 0;
reefsList_Settling = 0;
[connArray, connLengths, connLengthsStdevs, connectionsByDistanceHistc, connStrengthVsDistance] = CM_scales(CM, reefLocations, reefsList_Seeding, reefsList_Settling, plotAllWeeks);
[plumeLength, allPlumes] = CM_plume(CM, reefLocations, reefsList_Seeding, reefsList_Settling, plotAllWeeks);

%Only consider connectivity for particles seeded over deep reefs & settling over all reefs:
fprintf('------------\n\n');
fprintf('We are now considering particles seeded over deep reefs and settling over all reefs:\n');
deepReefsList = ( nbShallowReefs+1:nbShallowReefs+length(deepReefLocations) )';
shallowReefsList = (1:nbShallowReefs)';
reefsList_Seeding = deepReefsList;
reefsList_Settling = 0;
[connArrayDeep, connLengthsDeep, connLengthsStdevsDeep, connectionsByDistanceHistcDeep, connStrengthVsDistanceDeep] = CM_scales(CM, reefLocations, reefsList_Seeding, reefsList_Settling, plotAllWeeks);
[plumeLengthDeep, allPlumesDeep] = CM_plume(CM, reefLocations, reefsList_Seeding, reefsList_Settling, plotAllWeeks);

% figure
% subplot(1,2,1)
% hist(connLengths{nbWeeks},50);
% title('Histogram of weighted connectivity length scales')
% subplot(1,2,2)
% hist(connLengthsStdevs{nbWeeks},50);
% title('Histogram of weighted connectivity length stdevs')

%NB: to analyse community data, use connectivityMatrix_commSourceSinks
fprintf('---------------------\n\n');
