function [] = CM_write(CM, mostSSbyLarvaeSeeded, mostSSbyRecruitedLarvae, folderPath)

fprintf('\nWriting data to file.\n');

nbWeeks = length(CM);
fprintf(' 1. Writing connectivity matrices.\n');
for week = 1:1:nbWeeks
    CMfilename = sprintf('CM_wk%02d.dat', week);
    CMfilename = [folderPath, CMfilename]; %#ok<AGROW>
    dlmwrite(CMfilename, CM{week}, ' ');
end

fprintf(' 2. Writing connectivity lists.\n');
for week = 1:1:nbWeeks
    listFilename = sprintf('CM_list_wk%02d.dat', week);
    listFilename = [folderPath, listFilename]; %#ok<AGROW>
    [row,col,v] = find(CM{week});
    v(:,3) = v(:,1);
    v(:,1) = row;
    v(:,2) = col;
    v(:,4) = 1;
    dlmwrite(listFilename, v, ' ');
end

fprintf(' 3. Writing self-seeded reef lists.\n');
for week = 1:1:nbWeeks
    listFilenamebySeeded = sprintf('SelfSeeded_byNbSeeded_wk%02d.dat', week);
    listFilenamebySeeded = [folderPath, listFilenamebySeeded]; %#ok<AGROW>
    listFilenamebyRecruited = sprintf('SelfSeeded_byNbRecruited_wk%02d.dat', week);
    listFilenamebyRecruited = [folderPath, listFilenamebyRecruited]; %#ok<AGROW>
    top = [0, 1];
    temp1 = mostSSbyLarvaeSeeded{week};
    temp2 = mostSSbyRecruitedLarvae{week};
    temp1 = [top 
        temp1];
    temp2 = [top 
        temp2];
    mostSSbyLarvaeSeeded{week} = temp1;
    mostSSbyRecruitedLarvae{week} = temp2;
    dlmwrite(listFilenamebySeeded, mostSSbyLarvaeSeeded{week}, ' ');
    dlmwrite(listFilenamebyRecruited, mostSSbyRecruitedLarvae{week}, ' ');
end

fprintf('Finished writing data. Enjoy!\n\n');

end