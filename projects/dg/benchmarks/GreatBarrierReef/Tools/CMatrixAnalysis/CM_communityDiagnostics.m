%Script to find net connectivity balance of communities
%  --> Identify source & sink communities
%
%INPUTS: 
%   A. CMatrix_list from file (adjust filename & folder as required)
%   B. Node map mapping sparse to compact node Id numbers from file
%   C. Node-to-community map from file
%
%OUTPUT:
%   A. commConn: row°: community Id, col1: connectivity IN, col2: connectivity
%                  OUT, col3: self-seeding
%                 NB: conn. IN & OUT do not count self-seeding
%   B. commConnMatrix: community connectivity matrix
%
%PROCESS:
%1. Load list of communities into array (rowId:node, col1:comm)
%2. Set up array of (rowId:comm number, col1:connectivity IN, col2:
%connectivity OUT) and community CMatrix
%3. Cycle over each node and add conn IN & OUT to its community's running
%total (array in 2.)
%

fprintf('\n============\nScript starting.\nFind net connectivity balance of communities.\n============\nOUTPUT:\n')
fprintf('  commConn: connectivity in, out & self-seeding of community\n  commConnMatrix: community connectivity matrix\n')
fprintf('  Output to file: self seeding OF and IN communities.\n')

%Reef positions file (to calc. community centres):
reefCentresFile = '/home/cthomas/GBR_slim/Saved_Data/GBR_ReefCentres_Sma_1093.dat';
reefCentres = importdata(reefCentresFile);

%Folder path:
communityFolder1 = '/home/cthomas/GBR_communities/src/';
communityFolder2 = 'results_wk03_Sett1Mort009/';
communityFolderPath = [communityFolder1, communityFolder2];

%Nb of weeks & week in question:
nbWeeks = 1;
week = 1;
%Gamma value to take for communities:
gamma = 0.06;
%Connectivity data:
connListFilename = [communityFolderPath, 'CM_list_wk03.dat'];
connList = importdata(connListFilename);

%Load list of nodes to communities
% -> First load sparse nodeId to compact nodeId mapping
nodeMapFilename = [communityFolderPath, 'CM_list_wk03.node_map.txt'];
nodeMapRaw = importdata(nodeMapFilename);
    
    %(old) TEMP:
%nodeMapRaw(:,1) = nodeMapRaw(:,1) - 1;
    %END TEMP
    
%nodeMap col1: sparse reef Id
    %NB: row numbering goes 1-->X but compact reefId goes 0-->X-1
nodeMap(:,1) = nodeMapRaw(:,1);

%Then load compact reef to community mapping
weekName = sprintf('%d', week);
gammaName = sprintf('%06f', gamma);
filenameCommsMap = [communityFolderPath, '/results/comm_', gammaName, '_1.000000.txt'];
reefsCommsMap = importdata(filenameCommsMap,' ');
reefsCommsMap(:,2) = reefsCommsMap(:,2)+1; %+1 to make community counting start from 1
nbComms = max(reefsCommsMap(:,2));

%Now merge sparse reefId list with community list
nodeMap(:,2) = reefsCommsMap(:,2);
                                 
%Create nodeMapReverse: row°: sparse reef Id, col1: community Id
highestSparseReefId = max(nodeMap(:,1));
nodeMapReverse = zeros(highestSparseReefId,1);
for compactReefId = 1:1:length(nodeMap)
    sparseReefId = nodeMap(compactReefId,1);
    nodeMapReverse(sparseReefId,1) = nodeMap(compactReefId,2);
end


%Set up list of community connectivity (col1: IN & col2: OUT)
commConn = zeros(nbComms,2);
%For each community, record connectivity into and out of the other
%communities:
% -> row: src comm, col: sink comm.
commConnMatrix = zeros(nbComms,nbComms);
commSelfSeeding = zeros(nbComms,1);    %<-- self-seeding of communities
selfSeedingInComm = zeros(nbComms, 1); %<-- self-seeding of reefs inside communities

%Also calculate communities' centres
%commCentres = zeros(nbComms, 2);
commMaxLonLat = zeros(nbComms, 2);
commMaxLonLat(:,:) = -1000.0;
commMinLonLat = zeros(nbComms, 2);
commMinLonLat(:,:) = 1000.0;
nbReefsPerComm = zeros(nbComms, 1);

%Cycle through list of directed connections and record each one to
%community connectivity list
for connNb = 1:1:length(connList)
    %1. Get src & sink communities for this connection
    srcReefSparseId = connList(connNb,1);
    srcReefComm = nodeMapReverse(srcReefSparseId,1);
    sinkReefSparseId = connList(connNb,2);
    sinkReefComm = nodeMapReverse(sinkReefSparseId,1);
    
    %2. Record connectivity
    %2A. Record connectivity out of source and into sink
    if (srcReefComm ~= sinkReefComm)
        commConn(srcReefComm,2) = commConn(srcReefComm,2) + connList(connNb, 3);
        commConn(sinkReefComm,1) = commConn(sinkReefComm,1) + connList(connNb, 3);
    else
        %NB: if connection is self-seeding, record it in self-seeding array,
        %not in commConn
        commSelfSeeding(srcReefComm) = commSelfSeeding(srcReefComm) + connList(connNb, 3);
    end
    
    if (srcReefSparseId==sinkReefSparseId)
        selfSeedingInComm(srcReefComm) = selfSeedingInComm(srcReefComm) + connList(connNb, 3);
    end
    
    %2B. Update community connectivity matrix
    commConnMatrix(srcReefComm, sinkReefComm) = commConnMatrix(srcReefComm, sinkReefComm) + connList(connNb, 3);
    
    %3A. Update array to calculate community centre of mass
%     commCentres(srcReefComm,1) = commCentres(srcReefComm,1) + reefCentres(srcReefSparseId,1);
%     commCentres(srcReefComm,2) = commCentres(srcReefComm,2) + reefCentres(srcReefSparseId,2);
     nbReefsPerComm(srcReefComm) = nbReefsPerComm(srcReefComm) + 1;
    %3A. Record max lat, lon for each community
    %Lon:
    if ( reefCentres(srcReefSparseId,1) > commMaxLonLat(srcReefComm,1) )
        commMaxLonLat(srcReefComm,1) = reefCentres(srcReefSparseId,1);
    end
    if ( reefCentres(srcReefSparseId,1) < commMinLonLat(srcReefComm,1) )
        commMinLonLat(srcReefComm,1) = reefCentres(srcReefSparseId,1);
    end
    %Lat:
    if ( reefCentres(srcReefSparseId,2) > commMaxLonLat(srcReefComm,2) )
        commMaxLonLat(srcReefComm,2) = reefCentres(srcReefSparseId,2);
    end
    if ( reefCentres(srcReefSparseId,2) < commMinLonLat(srcReefComm,2) )
        commMinLonLat(srcReefComm,2) = reefCentres(srcReefSparseId,2);
    end
end
%3B. Finish calculating communities' centres
% commCentres(:,1) = commCentres(:,1)./nbReefsPerComm;
% commCentres(:,2) = commCentres(:,2)./nbReefsPerComm;
%3B. Finish calculating communities' centres
commCentres = (commMinLonLat+commMaxLonLat)./2.0;

%Print community connectivity summary:
fprintf('\nInter-community connectivity summary:\n');
fprintf('Community\t\tNet balance (+:IN, -:OUT)\tSelf-seeding\n');
for i=1:1:length(commConn)
    fprintf('%d\t\t%d\t\t%d\n', i-1, commConn(i,1)-commConn(i,2), commSelfSeeding(i));
end

totalConnProducedPerComm = sum(commConnMatrix,2);

% Write results to file
filename = sprintf('comm_connSummary_wk%d_gamma%4f.dat', week, gamma);
filename = [communityFolderPath, filename];
commConn(:,3) = commSelfSeeding;
dlmwrite(filename, commConn, ' ');
filename = sprintf('comm_CMatrix_wk%d_gamma%.4f.dat', week, gamma);
filename = [communityFolderPath, filename];
dlmwrite(filename, commConnMatrix, ' ');

filename = sprintf('selfSeedingOFcommunities.dat');
filename = [communityFolderPath, filename];
commSelfSeeding = commSelfSeeding./totalConnProducedPerComm;
commSelfSeeding(:,2) = commCentres(:,1);
commSelfSeeding(:,3) = commCentres(:,2);
commSelfSeeding(:,4) = nbReefsPerComm;
%top = [100 0 1];
%tempCommSelfSeeding = [top
%    commSelfSeeding];
dlmwrite(filename, commSelfSeeding, ' ');
filename = sprintf('selfSeedingINcommunities.dat');
filename = [communityFolderPath, filename];
selfSeedingInComm = selfSeedingInComm./totalConnProducedPerComm;
selfSeedingInComm(:,2) = commCentres(:,1);
selfSeedingInComm(:,3) = commCentres(:,2);
selfSeedingInComm(:,4) = nbReefsPerComm;
%top = [100 0 1];
%tempSelfSeedingInComm = [top
%    selfSeedingInComm];
dlmwrite(filename, selfSeedingInComm, ' ');

%Plot results on chart
%A. Plot raw results
figure
subplot(2,1,1);
bar(commSelfSeeding);
title('Chart showing self-seeding for each reef community');
subplot(2,1,2);
bar(commConn(:,1)-commConn(:,2));
title('Chart showing net connectivity balance for each community (+ve:IN, -ve:OUT)');

%B. Plot normalised results
figure
subplot(2,1,1);
bar(commSelfSeeding(:,1)./totalConnProducedPerComm);
title('Chart showing self-seeding for each reef community, NORMALISED by the total connectivity produced by that community');
subplot(2,1,2);
bar( (commConn(:,1)-commConn(:,2))./totalConnProducedPerComm );
title('Chart showing net connectivity balance for each community (+ve:IN, -ve:OUT), NORMALISED by the total connectivity produced by that community \\ -> ie. >1 ==> community imports (net) more than it produces \\ -> but cannot be <1 as a community cannot export more than it produces');

figure
imagesc(commConnMatrix)
caxis([0 2000])
colormap bone
colormap(flipud(colormap))
title('Community connectivity matrix');

fprintf('================\n');

clearvars -except commConn commConnMatrix commCentres commMinLonLat commMaxLonLat