% Function to calculate lengths of plumes of every reef
% Output:
% A/ Plots histogram of nb reefs vs plume length
% B/ Returns array of nb reefs per plume length

function [plumeLength, allPlumes] = CM_plume(CM, reefArray, reefsList_Seeding, reefsList_Settling, plotAllWeeks)

fprintf('\nPLUME LENGTH SCALE ANALYSIS:\n');

%array measuring connectivity at each scale:
nbWeeks = length(CM);
nbBins = 500;
nbBins=nbBins+1; %To account for self-seeding (plume length=0)
plumeLength = zeros(nbBins,nbWeeks);

% Loop through every element in CM. Skip if =0. 
% For every row (src reef), loop through every non-zero element and calc
% src<-->dest distance. 
% Record largest distance in plumeLength.

% plumeLength: 
%   -> distance is rounded up to nearest integer and placed into that bin+1.
%     --> plumeLength(1): plumes of length 0km (self-seeding only, no conn)
%     --> plumeLength(2) : plumes of lengths of 0-1km
%     --> plumeLength(3) : between 1-2km

% CMatrix: row = source, col = sink

allPlumes = cell(nbWeeks, 1);
sum_reefsList_Seeding = sum(reefsList_Seeding);
sum_reefsList_Settling = sum(reefsList_Settling);

for week = 1:1:nbWeeks
    CMatrix = CM{week};
    allPlumesNow = zeros(length(CMatrix),1);

    for row = 1:1:length(CMatrix)
        
        % If reefsList_Seeding is non-null, only consider reefs in the list:
        if sum_reefsList_Seeding > 0
            if ismember(row, reefsList_Seeding)~=1
                continue;
            end
        % Else consider all reefs
        end 
        
        reefSeparationMax = 0;
        for col = 1:1:length(CMatrix)
            if ( CMatrix(row, col) == 0 ) 
                continue;
            end
            
            % If reefsList_Settling is non-null, only consider reefs in the list:
            if sum_reefsList_Settling > 0
                if ismember(col, reefsList_Settling)~=1
                    continue;
                end
            % Else consider all reefs
            end
            
            reefSeparation = getDistanceBetweenPoints(reefArray(row,2), reefArray(row,1), reefArray(col,2), reefArray(col,1));
            if (reefSeparation > reefSeparationMax)
                reefSeparationMax = reefSeparation;
            end
        end
        reefSeparationMaxInt = ceil(reefSeparationMax);
        %Cap reefSeparationMaxInt at maximum bin size
        if (reefSeparationMaxInt > nbBins-1)
            reefSeparationMaxInt = nbBins-1;
        end
        
        allPlumesNow(row) = reefSeparationMax;
        plumeLength(reefSeparationMaxInt+1, week) = plumeLength(reefSeparationMaxInt+1, week) + 1;
    end
    allPlumes{week} = allPlumesNow;
end

%Remove reefs with zero plume from allPlumes
for week = 1:1:nbWeeks
    allPlumesNow = allPlumes{week};
    for row = length(allPlumesNow):-1:1
        if allPlumesNow(row) == 0
            allPlumesNow(row,:) = [];
        end
    end
    allPlumes{week} = allPlumesNow;
end


% figure
% for week = 1:1:nbWeeks
%     subplot(2,2,week);
%     bar(plumeLength(:,week));
%     tit = sprintf('Week %d', week);
%     title(tit);
%     xlabel('plume length (kms)');
%     ylabel('nb reefs');
% end
% title('Graph showing strength of nb reefs vs plume length');

% figure
% subplot(1,1,1);
% if plotAllWeeks == 1
%     bar(plumeLength, 'histc');
% else
%     bar(plumeLength(:,nbWeeks), 'histc');
% end

%Initialise +/- sign
pm=char(177);


fprintf('\nAverage plume length (for non-zero plumes) after each week (%c stdev):\n', pm);
for week=1:1:nbWeeks
    
    meanPL = mean(allPlumes{week});
    stdevPL = std(allPlumes{week});

    fprintf(' week %d:  %4gkm %c%4g \n', week, meanPL, pm, stdevPL);
end

fprintf('\nProportion of long plumes (>100km) after each week (including non-zero plumes):\n');
for week=1:1:nbWeeks
    b = sum(plumeLength(:,week));
    c = sum(plumeLength(100:nbBins,week))/b * 100.0;
    fprintf(' week %d:  %4g %%\n', week, c);
end

end
