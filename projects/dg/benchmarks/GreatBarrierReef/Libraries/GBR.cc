/*
 * General library of functions for GBR simulation.
 *  Part 1: "Numerical & computational" functions
 *  Part 2: "Physical" functions
 */

#include <stdio.h>
#include "math.h"
#include "fullMatrix.h"
#include "function.h"
#include "dgGroupOfElements.h"
#include "polynomialBasis.h"
#include "dgMeshJacobian.h"
#include "dgConfig.h"

#ifdef HAVE_PROJ
#include <proj_api.h>
static projPJ pj_utm, pj_latlon;
#endif

static double pOx, pOy, pOz, pPhiX, pPhiY, pPhiZ, pThetaX, pThetaY, pThetaZ, R;
static bool init = false;

static void initialize() {
  // Initialise variables for OLD-type projection
  double phi = 146.5892/180*M_PI;
  double theta = -17.147569/180*M_PI;
  R = 6.371e6;
  pOx = cos(theta)*cos(phi)*R;
  pOy = cos(theta)*sin(phi)*R;
  pOz = sin(theta)*R;
  pPhiX = -sin(phi);
  pPhiY = cos(phi);
  pPhiZ = 0;
  pThetaX = -sin(theta)*cos(phi);
  pThetaY = -sin(theta)*sin(phi);
  pThetaZ = cos(theta);
//   init = true;
  
  #ifdef HAVE_PROJ
  // Initialise variables for PROJ-type projection
  if ( !(pj_latlon = pj_init_plus("+proj=latlong +ellps=WGS84")) )  {
    printf("Error init latLon\n");
    exit(1); }

  if ( !(pj_utm = pj_init_plus("+proj=utm +ellps=WGS84 +zone=55 +south")) ) {
    printf("Error init UTM\n");
    exit(1); }
  #endif
    
  init = true;
}

extern "C" {

//Numerical & computational functions:

#ifdef HAVE_PROJ
//Use the following function to get angleFM for UTM vector functions
void getAngleProjLonLat(dataCacheMap *, fullMatrix<double> &angle, fullMatrix<double> &xyz) {
  if (! init) initialize();
  for (size_t i = 0; i < xyz.size1(); i++) {
    double x[2], y[2];
    x[0] = xyz.get(i, 0) - 1000.0;
    x[1] = xyz.get(i, 0) + 1000.0;
    y[0] = xyz.get(i, 1);
    y[1] = xyz.get(i, 1);
//     printf("x[0]_utm: %.12f, x[1]_utm: %.12f\n", x[0], x[1]);
    
    //Convert points to lat/lon:
    int er=0;
    if ( (er = pj_transform( pj_utm, pj_latlon, 2, 1, x, y, NULL )) != 0) {
      printf ("Transform failed: %s\n", pj_strerrno (er));
      exit(1);
    }
//     printf("x[0]_lon: %.12f, x[1]_lon: %.12f\n", x[0], x[1]);
    double dlon = x[1] - x[0];
    double dlat = y[1] - y[0];
//     printf("dlon: %.12f, dlat: %.12f\n", dlon, dlat);
    angle.set(i, 0, atan2(dlat, dlon));
  }
}
  
void latLonDegreesToUTM(dataCacheMap *, fullMatrix<double> &UTM, fullMatrix<double> &latLonDegrees) {
  if (! init) initialize();
  double x, y;
  double pi = 4*atan(1.0);
  for (size_t i = 0; i < UTM.size1(); i++) {
    y = latLonDegrees.get(i,0) * pi / 180.0;
    x = latLonDegrees.get(i,1) * pi / 180.0;

//     printf("Using latLonDegreesToUTM (LIB.cc); lat/lon (degrees): %.2f\t%.2f\n", x*180.0/pi, y*180.0/pi);
    int er=0;
    if ( (er = pj_transform( pj_latlon, pj_utm, 1, 1, &x, &y, NULL )) != 0) {
      printf ("Transform failed: %s\n", pj_strerrno (er));
      exit(1);
    }
    
    UTM.set(i,0, x);
    UTM.set(i,1, y);
    UTM.set(i,2, 0.0);
  }
}
  
void UTMtoLatLonDegrees(dataCacheMap *, fullMatrix<double> &latLonDegrees, fullMatrix<double> &UTM) {
  if (! init) initialize();
  double x, y;
  double pi = 4*atan(1.0);
  for (size_t i = 0; i < latLonDegrees.size1(); i++) {
    x = UTM.get(i,0);
    y = UTM.get(i,1);

//     printf("Using UTMtoLatLonDegrees (LIB.cc); UTM: %.2f\t%.2f\n", x, y);
    
    int er=0;
    if ( (er = pj_transform( pj_utm, pj_latlon, 1, 1, &x, &y, NULL )) != 0) {
      printf ("Transform failed: %s\n", pj_strerrno (er));
      exit(1);
    }
//     printf("lat/lon: %.2f, %.2f\n", y* 180.0 / pi, x* 180.0 / pi);
    latLonDegrees.set(i,0, y * 180.0 / pi);
    latLonDegrees.set(i,1, x * 180.0 / pi);
    latLonDegrees.set(i,2, 0.0);
  }
}

void UTMtoLonLatDegrees(dataCacheMap *, fullMatrix<double> &lonLatDegrees, fullMatrix<double> &UTM) {
  if (! init) initialize();
  double x, y;
  double pi = 4*atan(1.0);
  for (size_t i = 0; i < lonLatDegrees.size1(); i++) {
    x = UTM.get(i,0);
    y = UTM.get(i,1);

//     printf("Using UTMtoLatLonDegrees (LIB.cc); UTM: %.2f\t%.2f\n", x, y);
    
    int er=0;
    if ( (er = pj_transform( pj_utm, pj_latlon, 1, 1, &x, &y, NULL )) != 0) {
      printf ("Transform failed: %s\n", pj_strerrno (er));
      exit(1);
    }
//     printf("Using UTMtoLatLonDegrees (LIB.cc); lon/lat: %.2f\t%.2f\n", x*180.0/pi, y*180.0/pi);
    lonLatDegrees.set(i,0, x * 180.0 / pi);
    lonLatDegrees.set(i,1, y * 180.0 / pi);
    lonLatDegrees.set(i,2, 0.0);
  }
}
  
// Functions to convert vectors:
//Convert vector in lon/lat space to UTM space. u and v give the lon and lat components of vector to be projected.
void lonLatVector_To_UTMVector(dataCacheMap *, fullMatrix<double> &uv, fullMatrix<double> &angle_FM, fullMatrix<double> &u, fullMatrix<double> &v){
  for (size_t i = 0; i < uv.size1(); i++) {
//     printf("Converting vectors from lon/lat to UTM space.\n");
    double dlon = u.get(i,0);
    double dlat = v.get(i,0);
//     printf("lon/lat: u: %.4f, v: %.4f\n", dlon, dlat);
    double angle = angle_FM.get(i,0);
    double dx = dlon*cos(angle) + dlat*sin(angle);
    double dy = dlat*cos(angle) + dlon*sin(angle);
//     printf("utm: u: %.4f, v: %.4f\n", dx, dy);
    uv.set(i, 0, dx);
    uv.set(i, 1, dy);
    uv.set(i, 2, 0.0);
  }
}

//This function is redundant as you can just use the above function and reverse the u and v to get the same thing.
void latLonVector_To_UTMVector(dataCacheMap *, fullMatrix<double> &uv, fullMatrix<double> &angle_FM, fullMatrix<double> &u, fullMatrix<double> &v){
  for (size_t i = 0; i < uv.size1(); i++) {
//     printf("Converting vectors from lon/lat to UTM space.\n");
    double dlat = u.get(i,0);
    double dlon = v.get(i,0);
//     printf("lon/lat: u: %.4f, v: %.4f\n", dlon, dlat);
    double angle = angle_FM.get(i,0);
    double dx = dlon*cos(angle) + dlat*sin(angle);
    double dy = dlat*cos(angle) + dlon*sin(angle);
//     printf("utm: u: %.4f, v: %.4f; \t angle: %.6f\n", dx, dy, angle*180.0/M_PI);
    uv.set(i, 0, dx);
    uv.set(i, 1, dy);
    uv.set(i, 2, 0.0);
  }
}
#endif


void lonLat (dataCacheMap *, fullMatrix<double> &lonlat, fullMatrix<double> &xyz) {
  if (! init) initialize();
  for (size_t i = 0; i< lonlat.size1(); i++) {
    double x = pPhiX * xyz(i,0) + pThetaX * xyz(i,1) + pOx;
    double y = pPhiY * xyz(i,0) + pThetaY * xyz(i,1) + pOy;
    double z = pPhiZ * xyz(i,0) + pThetaZ * xyz(i,1) + pOz;
    double r = sqrt(x*x+y*y+z*z);
    lonlat.set(i, 0, atan2(y,x));
    lonlat.set(i, 1, asin(z/r));
    lonlat.set(i, 2, 0);
  }
}

void latLon (dataCacheMap *, fullMatrix<double> &lonlat, fullMatrix<double> &xyz) {
  if (! init) initialize();
  for (size_t i = 0; i< lonlat.size1(); i++) {
    double x = pPhiX * xyz(i,0) + pThetaX * xyz(i,1) + pOx;
    double y = pPhiY * xyz(i,0) + pThetaY * xyz(i,1) + pOy;
    double z = pPhiZ * xyz(i,0) + pThetaZ * xyz(i,1) + pOz;
    double r = sqrt(x*x+y*y+z*z);
    lonlat.set(i, 0, asin(z/r));
    lonlat.set(i, 1, atan2(y,x));
    lonlat.set(i, 2, 0);
  }
}

void lonLatDegrees (dataCacheMap *, fullMatrix<double> &lonlat, fullMatrix<double> &xyz) {
	if (! init) initialize();
	for (size_t i = 0; i< lonlat.size1(); i++) {
		double x = pPhiX * xyz(i,0) + pThetaX * xyz(i,1) + pOx;
		double y = pPhiY * xyz(i,0) + pThetaY * xyz(i,1) + pOy;
		double z = pPhiZ * xyz(i,0) + pThetaZ * xyz(i,1) + pOz;
		double r = sqrt(x*x+y*y+z*z);
		lonlat.set(i, 0, atan2(y,x)*180/M_PI);
		lonlat.set(i, 1, asin(z/r)*180/M_PI);
		lonlat.set(i, 2, 0);
	}
}

void latLonDegrees (dataCacheMap *, fullMatrix<double> &lonlat, fullMatrix<double> &xyz) {
	if (! init) initialize();
	for (size_t i = 0; i< lonlat.size1(); i++) {
		double x = pPhiX * xyz(i,0) + pThetaX * xyz(i,1) + pOx;
		double y = pPhiY * xyz(i,0) + pThetaY * xyz(i,1) + pOy;
		double z = pPhiZ * xyz(i,0) + pThetaZ * xyz(i,1) + pOz;
		double r = sqrt(x*x+y*y+z*z);
		lonlat.set(i, 1, atan2(y,x)*180/M_PI);
		lonlat.set(i, 0, asin(z/r)*180/M_PI);
		lonlat.set(i, 2, 0);
	}
}

void lonLatVector(dataCacheMap *, fullMatrix<double> &uv, fullMatrix<double> &lonlat, fullMatrix<double> &u, fullMatrix<double> &v){
	for (size_t i = 0; i< lonlat.size1(); i++) {
		double lon=lonlat(i,0);
		double lat=lonlat(i,1);
		double uLon=u(i,0);
		double uLat=v(i,0);
    double lon0=146.5892*M_PI/180;
    double lat0=-17.147569*M_PI/180;
    double u1= -uLat*sin(lat)*cos(lon)-uLon*sin(lon);
    double u2= -uLat*sin(lat)*sin(lon)+uLon*cos(lon);
    double u3= uLat*cos(lat);
    double alpha1= -sin(lon0);
    double alpha2= cos(lon0);
    double alpha3=0;
    double beta1=-sin(lat0)*cos(lon0);
    double beta2=-sin(lat0)*sin(lon0);
    double beta3=cos(lat0);
		uv.set(i, 0, u1*alpha1+u2*alpha2+u3*alpha3);
		uv.set(i, 1, u1*beta1+u2*beta2+u3*beta3);
		uv.set(i, 2,  0);
	}
}

void latLonVector(dataCacheMap *, fullMatrix<double> &uv, fullMatrix<double> &latlon, fullMatrix<double> &u, fullMatrix<double> &v){
	for (size_t i = 0; i< latlon.size1(); i++) {
		double lat=latlon(i,0);
		double lon=latlon(i,1);
		double uLat=u(i,0);
		double uLon=v(i,0);
    double lon0=146.5892*M_PI/180;
    double lat0=-17.147569*M_PI/180;
    double u1= -uLat*sin(lat)*cos(lon)-uLon*sin(lon);
    double u2= -uLat*sin(lat)*sin(lon)+uLon*cos(lon);
    double u3= uLat*cos(lat);
    double alpha1= -sin(lon0);
    double alpha2= cos(lon0);
    double alpha3=0;
    double beta1=-sin(lat0)*cos(lon0);
    double beta2=-sin(lat0)*sin(lon0);
    double beta3=cos(lat0);
		uv.set(i, 0, u1*alpha1+u2*alpha2+u3*alpha3);
		uv.set(i, 1, u1*beta1+u2*beta2+u3*beta3);
		uv.set(i, 2,  0);
	}
}

void transport2velocity(dataCacheMap *,fullMatrix<double> &uv, fullMatrix<double> &UV, fullMatrix<double> &bath, fullMatrix<double> &solution){
  for (size_t i = 0; i< solution.size1(); i++) {
    uv.set(i,0,UV(i,0)/(bath(i,0)+solution(i,0))); 
    uv.set(i,1,UV(i,1)/(bath(i,0)+solution(i,0))); 
    uv.set(i,2,UV(i,2)/(bath(i,0)+solution(i,0))); 
  }
}

void computeEl(dataCacheMap *,fullMatrix<double> &val,fullMatrix<double> &solution) {
  for (size_t i = 0; i< val.size1(); i++) {
    val.set(i, 0, solution(i,0)); 
  }
}

void computeVel(dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &solution){
  for (size_t i = 0; i< val.size1(); i++) {
    val.set(i, 0, solution(i,1)); 
    val.set(i, 1, solution(i,2)); 
  }
}

void computeErrorEl(dataCacheMap *,fullMatrix<double> &err,fullMatrix<double> &solution1, fullMatrix<double> &solution2) {
  for (size_t i = 0; i< err.size1(); i++) {
    err.set(i, 0, solution1(i,0)-solution2(i, 0)); 
  }
}

void computeErrorVel(dataCacheMap *, fullMatrix<double> &err, fullMatrix<double> &solution1, fullMatrix<double> &solution2){
  for (size_t i = 0; i< err.size1(); i++) {
    err.set(i, 0, solution1(i,1)-solution2(i, 1)); 
    err.set(i, 1, solution1(i,2)-solution2(i, 2)); 
  }
}

void merge_3DFunctions(dataCacheMap *,fullMatrix<double> &sol, fullMatrix<double> &funkOne, fullMatrix<double> &funkTwo){
  for (size_t i = 0; i< sol.size1(); i++) {
    sol.set(i, 0, funkOne(i,0) + funkTwo(i,0)); 
    sol.set(i, 1, funkOne(i,1) + funkTwo(i,1));
    sol.set(i, 2, funkOne(i,2) + funkTwo(i,2));
  }
}

void merge(dataCacheMap *,fullMatrix<double> &sol,fullMatrix<double> &eta, fullMatrix<double> &uv){
  for (size_t i = 0; i< sol.size1(); i++) {
    sol.set(i,0, eta(i,0)); 
    sol.set(i,1, uv(i,0));
    sol.set(i,2, uv(i,1));
  }
}

void merge2(dataCacheMap *,fullMatrix<double> &sol, fullMatrix<double> &u1, fullMatrix<double> &u2){
  for (size_t i = 0; i < sol.size1(); i++) {
    sol.set(i, 0, u1(i,0)); 
    sol.set(i, 1, u2(i,0)); 
  }
}

void merge3(dataCacheMap *,fullMatrix<double> &sol, fullMatrix<double> &eta, fullMatrix<double> &uv, fullMatrix<double> &currentUV){
  for (size_t i = 0; i< sol.size1(); i++) {
    sol.set(i,0, eta(i,0)); 
    sol.set(i,1, uv(i,0) + currentUV(i,1));
    sol.set(i,2, uv(i,1) + currentUV(i,2)); 
  }
}

void merge3D_with_UV(dataCacheMap *,fullMatrix<double> &sol, fullMatrix<double> &UVElev1, fullMatrix<double> &UV2){
  for (size_t i = 0; i< sol.size1(); i++) {
    sol.set(i,0, UVElev1(i,0)); 
    sol.set(i,1, UVElev1(i,1) + UV2(i,0));
    sol.set(i,2, UVElev1(i,2) + UV2(i,1)); 
  }
}

//Function to merge shallow (hard) and deep (soft) DCs
void overwrite2(dataCacheMap *,fullMatrix<double> &sol, fullMatrix<double> &bath, fullMatrix<double> &shallowDC, fullMatrix<double> &deepDC, fullMatrix<double> &nbOfShallowReefs){
  for (size_t i = 0; i < sol.size1(); i++) {
    if (shallowDC(i,0) > 0.1) sol.set(i, 0, shallowDC(i,0));
    else {
      if (deepDC(i,0) > 0.1) sol.set(i, 0, deepDC(i,0)+nbOfShallowReefs(0,0));
      else sol.set(i, 0, 0.0);
    }
  }
}

void cutOffMin(dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &ourFunk, fullMatrix<double> &minCutOff){
  double ourValue=0.1;
  for (size_t i = 0; i< sol.size1(); i++) {
    if (ourFunk.get(i,0) < minCutOff(0,0)) ourValue = minCutOff(0,0);
    else ourValue = ourFunk.get(i,0);
    sol.set(i, 0, ourValue);
  }
}

//Physical functions:
/** Calculate water depth */
void getWaterDepth(dataCacheMap *, fullMatrix<double> &fct, fullMatrix<double> &sol, fullMatrix<double> &bath) {
  for (size_t i = 0; i< fct.size1(); i++) {
    double eta = sol.get(i, 0);
    double depth = bath.get(i, 0);
    fct.set(i, 0, eta+depth);
  }
}

/** Constant wind */
void wind (dataCacheMap *, fullMatrix<double> &sol) {
  for (size_t i = 0; i< sol.size1(); i++) {
    sol.set(i,0,0.000005);
    sol.set(i,1,0.000005);
  }
}

/** Coriolis force */
void coriolisLonLatDegrees (dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &lonlat) {
  for (size_t i = 0; i< sol.size1(); i++) {
    sol.set(i,0,2*7.292e-5*sin(lonlat(i,1)/180.0*M_PI));
  }
}

void coriolisConst (dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  double theta = -17.147569/180*M_PI;
  for (size_t i = 0; i< sol.size1(); i++) {
//     sol.set(i,0,2*7.292e-5*xyz(i,2)/6.371e6);
    sol.set(i,0,2*7.292e-5*sin(theta));
  }
}

/** Different bottom drag paramaterisations */
void bottomDrag_old(dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &sol, fullMatrix<double> &bath){
  for(int i=0;  i<val.size1(); i++){
    double H = sol(i, 0)+bath(i, 0);
    double reef = 1.0;
    if (H < 10.0) reef = 10.0;
    val.set(i, 0, 9.81*reef*0.0235*0.0235/(pow(H, 1.333333333333)));
  }
}

void bottomDrag(dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &sol, fullMatrix<double> &bath, fullMatrix<double> &reefs){
  for(int i=0;  i<val.size1(); i++){
    double reef = 1.0;
    double H = sol(i, 0)+bath(i, 0);
    //Verify if there is a reef or not. 
    //  Reef --> multiply normal bottom drag by factor 10x
    int reefYesOrNo = int(reefs(i, 0));
//    printf("i is %d. reefYesOrNo: %d, reefs(i,0): %f\n",i, reefYesOrNo, reefs(i,0));
    if (reefYesOrNo > 0) reef = 10.0;
    val.set(i, 0, 9.81*reef*0.0235*0.0235/(pow(H, 1.333333333333)));
  }
}

void bottomDrag_const_n008(dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &sol, fullMatrix<double> &bath){
  for(int i=0;  i<val.size1(); i++){
    double H = sol(i, 0)+bath(i, 0);
    val.set(i, 0, 9.81*0.008*0.008/(pow(H, 1.333333333333)));
  }
}

void bottomDrag_const_n016(dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &sol, fullMatrix<double> &bath){
  for(int i=0;  i<val.size1(); i++){
    double depthFactor=1.0;
    double H = sol(i, 0)+bath(i, 0);
//     if H > 200 depthFactor = 10.0;
//     if H > 500 depthFactor = 100.0;
    val.set(i, 0, 9.81*depthFactor*0.016*0.016/(pow(H, 1.333333333333)));
  }
}

void bottomDrag_reefs_n016(dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &sol, fullMatrix<double> &bath, fullMatrix<double> &reefs){
  for(int i=0;  i<val.size1(); i++){
    double reef = 1.0;
    double H = sol(i, 0)+bath(i, 0);
    //Verify if there is a reef or not.
    //  Reef --> multiply normal bottom drag by factor 10x
    int reefYesOrNo = int(reefs(i, 0));
    if (reefYesOrNo > 0 && bath(i,0) < 25.0) reef = 10.0;
    val.set(i, 0, 9.81*reef*0.016*0.016/(pow(H, 1.333333333333)));
  }
}

void bottomDrag_reefs_n008(dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &sol, fullMatrix<double> &bath, fullMatrix<double> &reefs){
  for(int i=0;  i<val.size1(); i++){
    double reef = 1.0;
    double H = sol(i, 0)+bath(i, 0);
    //Verify if there is a reef or not.
    //  Reef --> multiply normal bottom drag by factor 10x
    int reefYesOrNo = int(reefs(i, 0));
    if (reefYesOrNo > 0) reef = 10.0;
    val.set(i, 0, 9.81*reef*0.008*0.008/(pow(H, 1.333333333333)));
  }
}

void bottomDrag_const_Smallest(dataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &sol, fullMatrix<double> &bath){
  for(int i=0;  i<val.size1(); i++){
    double H = sol(i, 0)+bath(i, 0);
    val.set(i, 0, 9.81*0.0074*0.0074/(pow(H, 1.333333333333)));
  }
}

/** Smagorinsky term */
void smagorinsky(dataCacheMap *map, fullMatrix<double> &val, fullMatrix<double> &solGradient){
  const dgGroupOfElements *g = map->getGroupOfElements();
  int elNum = map->getElementId();
  const dgMeshJacobian &jaco = map->getJacobians();
  const fullVector<double> &elRadi = jaco.elementInnerRadius(g->elementVectorId());
  double radi = elRadi(elNum);
  for(int i=0;  i<val.size1(); i++){
    double dudx = solGradient(i, 3);
    double dudy = solGradient(i, 4);
    double dvdx = solGradient(i, 6);
    double dvdy = solGradient(i, 7);
    val(i, 0)= pow_int(0.1*radi, 2)*sqrt(2*dudx*dudx+2*dvdy*dvdy+pow_int(dudy+dvdx, 2));
  }
}

void current (dataCacheMap *, fullMatrix<double> &current, fullMatrix<double> &solution) {
  for (size_t i = 0; i< current.size1(); i++) {
    current.set(i, 0, solution(i,1));
    current.set(i, 1, solution(i,2));
    current.set(i, 2, 0);
  }
}

void current2 (dataCacheMap *, fullMatrix<double> &current, fullMatrix<double> &solution) {
  for (size_t i = 0; i< current.size1(); i++) {
    current.set(i, 0, solution(i,0));
    current.set(i, 1, solution(i,1));
    current.set(i, 2, 0.0);
  }
}

/** Wind stress paramaterisations */
void windStress (dataCacheMap *,fullMatrix<double> &windstress,fullMatrix<double> &winduv, fullMatrix<double> &sol, fullMatrix<double> &bath) {
  for (size_t i = 0; i< windstress.size1(); i++) {
    double normWind=sqrt(winduv(i, 0)*winduv(i, 0)+winduv(i, 1)*winduv(i, 1));
    double rho=1025;
    double sol1=0.001*(0.630*normWind+0.066*normWind*normWind)*winduv(i, 0)/(rho*(sol(i, 0)+bath(i, 0)));
    double sol2=0.001*(0.630*normWind+0.066*normWind*normWind)*winduv(i, 1)/(rho*(sol(i, 0)+bath(i, 0)));
    windstress.set(i, 0, sol1);
    windstress.set(i, 1, sol2);
  }
}

void windStressSmithBanke (dataCacheMap *,fullMatrix<double> &windstress,fullMatrix<double> &winduv, fullMatrix<double> &sol, fullMatrix<double> &bath) {
  for (size_t i = 0; i< windstress.size1(); i++) {
    double normWind=sqrt(winduv(i, 0)*winduv(i, 0)+winduv(i, 1)*winduv(i, 1));
    double rho=1025;
    double sol1=1.25*0.001*(0.630*normWind+0.066*normWind*normWind)*winduv(i, 0)/(rho*(sol(i, 0)+bath(i, 0)));
    double sol2=1.25*0.001*(0.630*normWind+0.066*normWind*normWind)*winduv(i, 1)/(rho*(sol(i, 0)+bath(i, 0)));
    windstress.set(i, 0, sol1);
    windstress.set(i, 1, sol2);
  }
}

void windStress_OTT (dataCacheMap *,fullMatrix<double> &windstress,fullMatrix<double> &winduv, fullMatrix<double> &sol, fullMatrix<double> &bath) {
  for (size_t i = 0; i< windstress.size1(); i++) {
    double normWind=sqrt(winduv(i, 0)*winduv(i, 0)+winduv(i, 1)*winduv(i, 1));
    double rho=1025;
    double sol1=1.25*0.001*63.0*normWind*winduv(i, 0)/(rho*(sol(i, 0)+bath(i, 0)));
    double sol2=1.25*0.001*63.0*normWind*winduv(i, 1)/(rho*(sol(i, 0)+bath(i, 0)));
    windstress.set(i, 0, sol1);
    windstress.set(i, 1, sol2);
  }
}

void windStressSmithBankeTimesTen (dataCacheMap *,fullMatrix<double> &windstress,fullMatrix<double> &winduv, fullMatrix<double> &sol, fullMatrix<double> &bath) {
  for (size_t i = 0; i< windstress.size1(); i++) {
    double normWind=sqrt(winduv(i, 0)*winduv(i, 0)+winduv(i, 1)*winduv(i, 1));
    double rho=1025;
    double sol1=1.25*0.01*(0.630*normWind+0.066*normWind*normWind)*winduv(i, 0)/(rho*(sol(i, 0)+bath(i, 0)));
    double sol2=1.25*0.01*(0.630*normWind+0.066*normWind*normWind)*winduv(i, 1)/(rho*(sol(i, 0)+bath(i, 0)));
    windstress.set(i, 0, sol1);
    windstress.set(i, 1, sol2);
  }
}

void windStressSmithBankeTimesOnePointFive (dataCacheMap *,fullMatrix<double> &windstress,fullMatrix<double> &winduv, fullMatrix<double> &sol, fullMatrix<double> &bath) {
  for (size_t i = 0; i< windstress.size1(); i++) {
    double normWind=sqrt(winduv(i, 0)*winduv(i, 0)+winduv(i, 1)*winduv(i, 1));
    double rho=1025;
    double sol1=1.5*1.25*0.001*(0.630*normWind+0.066*normWind*normWind)*winduv(i, 0)/(rho*(sol(i, 0)+bath(i, 0)));
    double sol2=1.5*1.25*0.001*(0.630*normWind+0.066*normWind*normWind)*winduv(i, 1)/(rho*(sol(i, 0)+bath(i, 0)));
    windstress.set(i, 0, sol1);
    windstress.set(i, 1, sol2);
  }
}

void windStressSmithBankeTimesTwo (dataCacheMap *,fullMatrix<double> &windstress,fullMatrix<double> &winduv, fullMatrix<double> &sol, fullMatrix<double> &bath) {
  for (size_t i = 0; i< windstress.size1(); i++) {
    double normWind=sqrt(winduv(i, 0)*winduv(i, 0)+winduv(i, 1)*winduv(i, 1));
    double rho=1025;
    double sol1=2.0*1.25*0.001*(0.630*normWind+0.066*normWind*normWind)*winduv(i, 0)/(rho*(sol(i, 0)+bath(i, 0)));
    double sol2=2.0*1.25*0.001*(0.630*normWind+0.066*normWind*normWind)*winduv(i, 1)/(rho*(sol(i, 0)+bath(i, 0)));
    windstress.set(i, 0, sol1);
    windstress.set(i, 1, sol2);
  }
}

void windStressSmithBankeTimesFour (dataCacheMap *,fullMatrix<double> &windstress,fullMatrix<double> &winduv, fullMatrix<double> &sol, fullMatrix<double> &bath) {
  for (size_t i = 0; i< windstress.size1(); i++) {
    double normWind=sqrt(winduv(i, 0)*winduv(i, 0)+winduv(i, 1)*winduv(i, 1));
    double rho=1025;
    double sol1=4.0*1.25*0.001*(0.630*normWind+0.066*normWind*normWind)*winduv(i, 0)/(rho*(sol(i, 0)+bath(i, 0)));
    double sol2=4.0*1.25*0.001*(0.630*normWind+0.066*normWind*normWind)*winduv(i, 1)/(rho*(sol(i, 0)+bath(i, 0)));
    windstress.set(i, 0, sol1);
    windstress.set(i, 1, sol2);
  }
}

void windStressGarratt (dataCacheMap *,fullMatrix<double> &windstress,fullMatrix<double> &winduv, fullMatrix<double> &sol, fullMatrix<double> &bath) {
  for (size_t i = 0; i< windstress.size1(); i++) {
    double normWind=sqrt(winduv(i, 0)*winduv(i, 0)+winduv(i, 1)*winduv(i, 1));
    double rho=1025;
    double sol1=0.001*(0.75*normWind+0.067*normWind*normWind)*winduv(i, 0)/(rho*(sol(i, 0)+bath(i, 0)));
    double sol2=0.001*(0.75*normWind+0.067*normWind*normWind)*winduv(i, 1)/(rho*(sol(i, 0)+bath(i, 0)));
    windstress.set(i, 0, sol1);
    windstress.set(i, 1, sol2);
  }
}

void windStressOnly (dataCacheMap *, fullMatrix<double> &windstress, fullMatrix<double> &winduv) {
  for (size_t i = 0; i< windstress.size1(); i++) {
    double normWind=sqrt(winduv(i, 0)*winduv(i, 0)+winduv(i, 1)*winduv(i, 1));
    double sol1=0.001*(0.630*normWind+0.066*normWind*normWind)*winduv(i, 0);
    double sol2=0.001*(0.630*normWind+0.066*normWind*normWind)*winduv(i, 1);
    windstress.set(i, 0, sol1);
    windstress.set(i, 1, sol2);
  }
}

/** Function to return time-dependent sine curve (elev only). Arg1: current time, arg2: Ref Time */
void sineFunction (dataCacheMap *, fullMatrix<double> &fct, fullMatrix<double> &timeFunction, fullMatrix<double> &initTime) {
  for (size_t i = 0; i< fct.size1(); i++) {
    fct.set(i, 0, 0.3*sin((timeFunction(0,0)-initTime(0,0))*2*M_PI/3600.0/12.0));
    fct.set(i, 1, 0);
    fct.set(i, 2, 0);
  }
}

/** Boundary current functions */

/** Currents as a function of wind */
void functionMultiply(dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &wind, fullMatrix<double> &multFactor) {
  for (size_t i=0; i<sol.size1(); i++) {
    sol.set(i, 0, wind(i,0) * multFactor(i,0) );
    sol.set(i, 1, wind(i,1) * multFactor(i,1) );
  }
}

void functionCurrentAsFWind(dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &wind, fullMatrix<double> &multFactor, fullMatrix<double> &depthFactor) {
  for (size_t i=0; i<sol.size1(); i++) {
    sol.set(i, 0, wind(i,0) * multFactor(i,0) * depthFactor(i,0) );
    sol.set(i, 1, wind(i,1) * multFactor(i,1) * depthFactor(i,0) );
  }
}

void functionCurrentAsFWind_calcDepthFactor(dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &bath) {
  for (size_t i=0; i<sol.size1(); i++) {
    double factor = 100.0/bath(i,0);
    if (factor > 2.0) factor = 2.0;
    sol.set(i, 0, factor);
  }
}

void functionAdd(dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &wind, fullMatrix<double> &addTerm) {
  for (size_t i=0; i<sol.size1(); i++) {
    sol.set(i, 0, wind(i,0) + addTerm(i,0) );
    sol.set(i, 1, wind(i,1) + addTerm(i,1) );
  }
}

/** Following two functions used to calculate the constant reisdual current imposed at boundaries */
/** return bathymetry * (unitVector (dot) normals)
    so size of return value will be greater when vectors and normals coincide, and when depth is larger*/
void bathVec(dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &bathymetry, fullMatrix<double> &normals, fullMatrix<double> &boundaryData) {
  double vx, vy;
  vx = boundaryData(0,1);
  vy = boundaryData(0,2);
  for (size_t i=0; i<bathymetry.size1(); i++) {
    sol.set(i, 0, bathymetry(i,0) * (vx*normals(i,0) + vy*normals(i,1)) );
  }
}
/** 1. compute the flux per point (by doing Flux / integral along the boundary of the above function)
    2. set the flux as (flux per point (dot) unitVector) ; so the incoming/outgoing flux points along the direction of the unitVector*/
void SEC_UV(dataCacheMap *, fullMatrix<double> &UVout, fullMatrix<double> &bathInterfaceIntegrals, fullMatrix<double> &normals, fullMatrix<double> &boundaryData) {
  double F, c, vx, vy;
  F = boundaryData(0,0);
  vx = boundaryData(0,1);
  vy = boundaryData(0,2);
  c = F / bathInterfaceIntegrals(0,0);
  for (size_t i=0; i<UVout.size1(); i++) {
  	UVout.set(i, 0, 0);	//Eta
    UVout.set(i, 1, c * vx);
    UVout.set(i, 2, c * vy);
	}
}

/** Function which returns: currents * (vector DOT boundary normals) 
 i.e. returns the current velocity only along boundaries perpendicular to "vector" (a unit vector) */
void currentsAlongVecDotNormals(dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &normal, fullMatrix<double> &vector, fullMatrix<double> &currents) {
  double vx, vy;
  vx = vector(0,0);
  vy = vector(0,1);
  for (size_t i=0; i<sol.size1(); i++) {
    sol.set(i, 0, currents(i,0) * (vx*normal(i,0) + vy*normal(i,1)) );
    sol.set(i, 1, currents(i,1) * (vx*normal(i,0) + vy*normal(i,1)) );
  }
}

/** Function which returns: currents DOT vector
 i.e. returns the current velocity along an axis defined by "vector" (a unit vector) */
void currentsDotVector(dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &vector, fullMatrix<double> &currents) {
  double vx, vy;
  vx = vector(0,0);
  vy = vector(0,1);
  for (size_t i=0; i<sol.size1(); i++) {
    sol.set(i, 0, currents(i,0) * vx );
    sol.set(i, 1, currents(i,1) * vy);
  }
}

/** Return Residual current 3 function (vector of size 0.10 spanning range of Open Sea Central) */
void getRes3(dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &lonLatDegrees) {
  for (size_t i=0; i<sol.size1(); i++) {
    if (lonLatDegrees(i,1) > -15.1)  {
      sol.set(i, 0, -0.10);
      sol.set(i, 1, 0.0); 
      continue; }
    if (lonLatDegrees(i,1) < -17.6)  {
      sol.set(i, 0, 0.0);
      sol.set(i, 1, -0.10); 
      continue; }
    sol.set(i, 0, -0.10*sqrt(1+(lonLatDegrees(i,1)+15.1)/(-15.1+17.6)) );
    sol.set(i, 1, -0.10*sqrt(-1.0*(lonLatDegrees(i,1)+15.1)/(-15.1+17.6)) );
  }
}

/** Return elevation as a function of perpendicular wind */
void elevAsFWind(dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &normals, fullMatrix<double> windUV) {
  for (size_t i=0; i<sol.size1(); i++) {
    sol.set(i, 0, 0.1*normals(i,0)*windUV(i,0) );
    sol.set(i, 1, 0.1*normals(i,1)*windUV(i,1) );
  }
}


}
