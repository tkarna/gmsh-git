#"""
#   Simulations on the GBR with multirate explicit Runge-Kutta time-stepping.
#   All the parameters can be found and modified in the file "InpuGBR.py"
#"""

#   A) HOW TO LAUNCH A SIMULATION
#       Simulation on 1 processor  : rundgpy MultirateGBR.py
#       Simulation on n processors : mpirun -np n rundgpy MultirateGBR.py   ( !!! You need MPI !!! )
#
#
#   B) TO START SIMULATION FROM A PRE-EXISTING SOLUTION:
#     1. Set the following in Input.py:
#         -> set flag startFromExistingSolution = 1
#         -> set solutionFileName
#         -> set stepNbOfInitialSolution
#         -> set old_dt
#         -> then, Ti = old Ti + stepNbOfInitialSolution*old_dt
#     2. Set the following in MultirateGBR.py:
#         -> Nothing
#
#         Notes:  Must point to a 'merged' .idx file (use generateIdxFiles.py, and make sure you adjust nb_parts)
#                 Make sure idx file is complete (ie open it and check it looks right) - especially if previous job crashed out!
#
#   C) Warning: this script requires the presence of the proj_api library for C. 
#               --> If not installed, use MultirateGBR_oldProj.py instead
#"""

# Import for python
from dgpy import *
from gmshpy import *
from math import *
from InputGBR import *
from ProjectMesh import *
from DiffuseBathGBR import *
from termcolor import colored
import time, os, sys

if(Msg.GetCommRank() == 0):
  print''
  print(colored('SLIM: Multirate GBR simulation starting. Loading 1:Mesh, 2:Bathymetry 3:Reef Map 4:Conservation law.',"yellow"))
  print''

  print(colored('      ________ ___    ___  ___  ___         ',"red"))
  print(colored('     /  _____//  /   /  / /   \/   \        ',"red"))
  print(colored(' ____\____  //  /___/  /_/__/\__/\  \_____   ',"red"))
  print(colored('/__________/ \_____/__/___________\__\____\         [ Great Barrier Reef ]',"red"))
  print''
  
# Lauch Makefile to generate meshes and download forcings
if(Msg.GetCommRank() == 0):
  try : os.mkdir('./Meshes');
  except: 0;
  try : os.mkdir('./Data');
  except: 0;
  try : os.mkdir('./Bath');
  except: 0;
  try : os.mkdir('./GBR_Functions');
  except: 0;
  try : os.mkdir(outputDir+'/angle');
  except: 0;
  try : os.mkdir(outputDir+'/XYZ');
  except: 0;
  if(not os.path.exists("./Meshes/"+filename+".msh")):
    try : os.system("make "+filename+".msh");
    except: 0;
  if (gbrWind != 0):
    try : os.mkdir(outputDir+'/windUV');
    except: 0;
    try : os.mkdir(outputDir+'/windSourceAcc');
    except: 0;
  if (gbrWindFluxAtBoundaries == 1):
    try : os.mkdir(outputDir+'/fWind_depthFactor');
    except: 0;
  if (gbrWind == 2):
    try : os.system("make forcings_windNCEP")
    except: 0;
  elif (gbrWind == 3):
    try : os.system("make forcings_windCSFR")
    except: 0;
  if (gbrResidual == 2):
    try : os.system("make forcings_blink")
    except: 0;
    try : os.mkdir(outputDir+'/blinkSol');
    except: 0;
  elif (gbrResidual == 3):
    try : os.mkdir('./Data/currentsForcing_MYR_CCH')
    except: 0
    try : os.system("make forcings_GBROOS")
    except: 0;
    try : os.mkdir(outputDir+'/boundaryGBROOS_MYRSol');
    except: 0;
    try : os.mkdir(outputDir+'/boundaryGBROOS_CCHSol');
    except: 0;
  try : os.system("make forcings");
  except: 0;
  try : os.mkdir(outputDir);
  except: 0;
  try : os.mkdir(outputDir+'/Evaluator');
  except: 0;
  try : os.mkdir(outputDir+'/lonLat');
  except: 0;

Msg.Barrier()

# Definition of Python functions
def printTime (t) :
  return "%dh%02d'%02d\"" % (t/3600,  (t%3600)/60,  t%60)

def printDate (t):
  date=time.gmtime(t)
  return "%02d/%02d/%04d %02dh%02d'%02d\""%(date[2], date[1], date[0], date[3], date[4], date[5])

# Compile libraries
if (Msg.GetCommRank() == 0):
  functionC.buildLibraryFromFile ("Libraries/GBR.cc", "Libraries/lib_gbr.so")
glib = "Libraries/lib_gbr.so"
Msg.Barrier()

# Load model
model = GModel()
if(Msg.GetCommSize() == 1):
  print(colored('Loading mesh for 1 processor (unpartitioned) ...',"red"))
  loadAndProjectMeshUTM(model, filename, projCodeLonLat, projCodeMesh)
else:
  if(os.path.exists("./Meshes/"+filename+"_utm_partitioned_"+str(Msg.GetCommSize())+".msh")):
    if(Msg.GetCommRank() == 0):
      print'Loading partitioned mesh: ./Meshes/'+filename+'_utm_partitioned_'+str(Msg.GetCommSize())+'.msh'
    model.load('./Meshes/'+filename+'_utm_partitioned_'+str(Msg.GetCommSize())+'.msh')
  else:
    Msg.Fatal('No valid partitioned mesh file: ./Meshes/'+filename+'_utm_partitioned_[nbProcs].msh ; run PartitionGBR_Standalone.py first.')

# Group collection
groups = dgGroupCollection(model, dimension, order)

# Coordinate system
XYZ = function.getCoordinates()
XYZ_PC = functionPrecomputed(groups, integOrder, 3)
XYZ_PC.compute(XYZ)
XYZ_DC = dgDofContainer(groups, 3)
XYZ_DC.L2Projection(XYZ_PC)
exporterXYZ = dgIdxExporter(XYZ_DC, outputDir+'/XYZ')
exporterXYZ.exportIdx()

# Define functions to project UTM points --> lat/lon
UTMtoLatLonDegrees = functionC(glib,"UTMtoLatLonDegrees",3,[XYZ])
UTMtoLonLatDegrees = functionC(glib,"UTMtoLonLatDegrees",3,[XYZ])

# Place above functions into PrecomputedFunction objects
UTMtoLatLonDegrees_PC = functionPrecomputed(groups, integOrder, 3)
UTMtoLatLonDegrees_PC.compute(UTMtoLatLonDegrees)
UTMtoLonLatDegrees_PC = functionPrecomputed(groups, integOrder, 3)
UTMtoLonLatDegrees_PC.compute(UTMtoLonLatDegrees)

lonLat_DC = dgDofContainer(groups, 3)
lonLat_DC.L2Projection(UTMtoLonLatDegrees)
exporterLL = dgIdxExporter(lonLat_DC, outputDir+'/lonLat')
exporterLL.exportIdx()

angleProj = functionC(glib, "getAngleProjLonLat", 1, [XYZ])
angleProj_PC = functionPrecomputed(groups, 3, 1)
angleProj_PC.compute(angleProj)
angleProj_DC = dgDofContainer(groups, 1);
angleProj_DC.L2Projection(angleProj_PC)
exporterAngleProj = dgIdxExporter(angleProj_DC, outputDir+'/angle')
exporterAngleProj.exportIdx()

# ****************** Cons Law ******************
if (Msg.GetCommRank() == 0):
  print(colored('Setting up conservation law ...',"red"))

claw = dgConservationLawShallowWater2d()
solution = dgDofContainer(groups, claw.getNbFields())

# **********************************************

# ****************** Bathymetry ****************

bathDC = dgDofContainer(groups, 1)
if(os.path.exists("./Bath/"+filename+"_bath_smooth/"+filename+"_bath_smooth.idx")):
  Msg.Info("Smoothed bathymetry already exists.")
  bathDC.readMsh("./Bath/"+filename+"_bath_smooth/"+filename+"_bath_smooth.idx")
  Msg.Info("Smoothed bathymetry read.")
else:
  diffuseBathymetry(groups, UTMtoLonLatDegrees, bathDC, filename)
#if using full.bin: diffuseBathymetry(bathDC, groups, lonLat)

# **********************************************

# ****************** Reefs *********************
if (Msg.GetCommRank() == 0):
  print(colored('Loading reef map ...',"red"))

reefsDC = dgDofContainer(groups,1)

reefsDCfilename = "GBR_Functions/reefsFunction_"+filename+"/reefsFunction_"+filename+".idx"
if(os.path.exists(reefsDCfilename)):
  if (Msg.GetCommRank() == 0):
    print 'Reef Map file found: '+reefsDCfilename+' ; loading this.'
  reefsDC.readMsh(reefsDCfilename)
else:
  if (Msg.GetCommRank() == 0):
    print 'Reef Map file '+reefsDCfilename+' not found. Loading reef map ...'
    #1. Create a reefs object (initialise object and create reefs function)
    reefsObj = reefsSmooth(reefMapFile, UTMtoLonLatDegrees)
    #2. Interpolate the function onto DC
    reefsDC.interpolate(reefsObj)
    exporterReefsDC=dgIdxExporter(reefsDC, 'GBR_Functions/reefsFunction_'+filename)
    exporterReefsDC.exportIdx(0,0)
    #3. Delete reefs object data
    reefsObj.clearReefsData()
Msg.Barrier()
reefsDC.readMsh(reefsDCfilename)

# ************************************************

t = Ti
timeFunction = functionConstant(t)

# ****************** Forcings ******************
if (Msg.GetCommRank() == 0):
  print(colored('Setting up forcings ...',"red"))
# .................. Tides    ..................

# Tpxo tide 
tpxoTide = None
if (gbrTide == 1):
  tideEta = slimFunctionTpxo("./Data/h_tpxo7.2.nc","ha","hp","lon_z","lat_z")
  tideEta.setCoordAndTimeFunctions(UTMtoLonLatDegrees_PC,timeFunction)
  tideU = slimFunctionTpxo("./Data/u_tpxo7.2.nc","Ua","up","lon_u","lat_u")
  tideU.setCoordAndTimeFunctions(UTMtoLonLatDegrees_PC,timeFunction)
  tideV = slimFunctionTpxo("./Data/u_tpxo7.2.nc","Va","vp","lon_v","lat_v")
  tideV.setCoordAndTimeFunctions(UTMtoLonLatDegrees_PC,timeFunction)
  tideUV = functionC(glib, "lonLatVector_To_UTMVector", 3, [angleProj_PC, tideU, tideV])

  tideuv = functionC(glib,"transport2velocity",3,[tideUV, bathDC.getFunction(), solution.getFunction()])
  tpxoTide = functionC(glib, "merge", 3, [tideEta, tideuv])

# .................. Wind     ..................

windInterpolatorU = slimStructDataInterpolatorMultilinear()
windInterpolatorV = slimStructDataInterpolatorMultilinear()
windUcontainer = None
windVcontainer = None
if gbrWind == 1:
  windUcontainer = slimStructDataContainerTemporalSerie("./Data/AIMS_wind_Davies_U.txt")
  windVcontainer = slimStructDataContainerTemporalSerie("./Data/AIMS_wind_Davies_V.txt")
elif gbrWind == 2:
  windUcontainer = slimStructDataContainerNetcdf("./Data/uwind-2000-2011.nc", "uwnd", "lat", "lon")
  windVcontainer = slimStructDataContainerNetcdf("./Data/vwind-2000-2011.nc", "vwnd", "lat", "lon")
elif gbrWind == 3:
  windUcontainer = slimStructDataContainerNetcdf("./Data/wnd10m_csfr_2008-2010.grb2.nc", "U_GRD_L103", "lat", "lon")
  windVcontainer = slimStructDataContainerNetcdf("./Data/wnd10m_csfr_2008-2010.grb2.nc", "V_GRD_L103", "lat", "lon")
windU = slimFunctionStructData(windUcontainer, windInterpolatorU, UTMtoLatLonDegrees_PC)
windV = slimFunctionStructData(windVcontainer, windInterpolatorV, UTMtoLatLonDegrees_PC)
windU.setTimeFunction(timeFunction)
windV.setTimeFunction(timeFunction)

#windU_PostSetTimeFn_DC = dgDofContainer(groups, 1)
#windU_PostSetTimeFn_DC.L2Projection(windU)
#windV_PostSetTimeFn_DC = dgDofContainer(groups, 1)
#windV_PostSetTimeFn_DC.L2Projection(windV)
#windU_PostSetTimeFn_DC.exportMsh('windU_PostSetTimeFn')
#windV_PostSetTimeFn_DC.exportMsh('windV_PostSetTimeFn')

windUV = functionC(glib, "lonLatVector_To_UTMVector", 3, [angleProj_PC, windU, windV])

# .... Define wind-induced current functions
currentFromWind_North = None
currentFromWind_Central = None
currentFromWind_South = None
if gbrWindFluxAtBoundaries == 1 and gbrWind != 0:
  #currentFromWind_North = functionC(glib, "functionMultiply", 2, [windUV, northMultFactor])
  #currentFromWind_Central = functionC(glib, "functionMultiply", 2, [windUV, centralMultFactor])
  #currentFromWind_South = functionC(glib, "functionMultiply", 2, [windUV, southMultFactor])
  
  functionCurrentAsFWind_depthFactor = functionC(glib, "functionCurrentAsFWind_calcDepthFactor", 1, [bathDC.getFunction()])
  functionCurrentAsFWind_depthFactor_PC = functionPrecomputed(groups, integOrder, 1)
  functionCurrentAsFWind_depthFactor_PC.compute(functionCurrentAsFWind_depthFactor)

  depthFactorDC = dgDofContainer(groups, 1)
  depthFactorDC.L2Projection(functionCurrentAsFWind_depthFactor_PC)
  depthFactorExporter = dgIdxExporter(depthFactorDC, outputDir+'/fWind_depthFactor')
  depthFactorExporter.exportIdx(0,0)
  
  currentFromWind_North = functionC(glib, "functionCurrentAsFWind", 2, [windUV, northMultFactor, functionCurrentAsFWind_depthFactor_PC])
  currentFromWind_Central = functionC(glib, "functionCurrentAsFWind", 2, [windUV, centralMultFactor, functionCurrentAsFWind_depthFactor_PC])
  currentFromWind_South = functionC(glib, "functionCurrentAsFWind", 2, [windUV, southMultFactor, functionCurrentAsFWind_depthFactor_PC])
  

# ............ Boundary current functions    .............
boundaryUVElev_North = None
boundaryUVElev_Central = None
boundaryUVElev_South = None
boundaryUVElev_North_WIP = None
boundaryUVElev_Central_WIP = None
boundaryUVElev_South_WIP = None
boundaryUVElevAndTide_North = None
boundaryUVElevAndTide_Central = None
boundaryUVElevAndTide_South = None
boundaryUVElevAndTide_North_WIP = None
boundaryUVElevAndTide_Central_WIP = None
boundaryUVElevAndTide_South_WIP = None

# .... Constant NCJ
if gbrResidual == 1:
  #Put data into functions
  data_A = functionConstant([F_A, vx_A_bulbSouth, vy_A_bulbSouth])
  data_B = functionConstant([F_B, vx_B_bulbSouth, vy_B_bulbSouth])
  data_C = functionConstant([F_C, vx_C_bulbSouth, vy_C_bulbSouth])

  # NCJ Current forcing
  bathTimesVector_A = functionC(glib, "bathVec", 3, [bathDC.getFunction(), function.getNormals(), data_A])
  bathTimesVector_B = functionC(glib, "bathVec", 3, [bathDC.getFunction(), function.getNormals(), data_B])
  bathTimesVector_C = functionC(glib, "bathVec", 3, [bathDC.getFunction(), function.getNormals(), data_C])
  bathIntegralNorthFM = fullMatrixDouble(1,1)
  bathIntegralCentralFM = fullMatrixDouble(1,1)
  bathIntegralSouthFM = fullMatrixDouble(1,1)
  dgFunctionIntegratorInterface(groups, bathTimesVector_A).compute('Open Sea North', bathIntegralNorthFM)
  dgFunctionIntegratorInterface(groups, bathTimesVector_B).compute('Open Sea Central', bathIntegralCentralFM)
  dgFunctionIntegratorInterface(groups, bathTimesVector_C).compute('Open Sea South', bathIntegralSouthFM)
  bathIntegralsFuncNorth = functionConstant(bathIntegralNorthFM(0,0))
  bathIntegralsFuncCentral = functionConstant(bathIntegralCentralFM(0,0))
  bathIntegralsFuncSouth = functionConstant(bathIntegralSouthFM(0,0))

  # Generate SEC UV functions
  boundaryUVElev_North_WIP = functionC(glib, "SEC_UV", 3, [bathIntegralsFuncNorth, function.getNormals(), data_A])
  boundaryUVElev_Central_WIP = functionC(glib, "SEC_UV", 3, [bathIntegralsFuncCentral, function.getNormals(), data_B])
  boundaryUVElev_South_WIP = functionC(glib, "SEC_UV", 3, [bathIntegralsFuncSouth, function.getNormals(), data_C])
  
  if (Msg.GetCommRank() == 0):
    outputVec = open('GBR_Functions/CurrentUnitVectors.pos', 'w')
    outputVec.write('View \"CurrentUnitVectors\" {\n')
    outputVec.write('VP (' + '%.5f'%0 + ',' + '%.5f'%0 + ',0) {' + '%.5f'%vx_A + ',' + '%.5f'%vy_A + ',0};\n')
    outputVec.write('VP (' + '%.5f'%0 + ',' + '%.5f'%0 + ',0) {' + '%.5f'%vx_B + ',' + '%.5f'%vy_B + ',0};\n')
    outputVec.write('VP (' + '%.5f'%0 + ',' + '%.5f'%0 + ',0) {' + '%.5f'%vx_C + ',' + '%.5f'%vy_C + ',0};\n')
    outputVec.write('};')
    outputVec.close()

# .... Bluelink
elif gbrResidual == 2:
  blinkInterpolatorU = slimStructDataInterpolatorMultilinear()
  blinkInterpolatorV = slimStructDataInterpolatorMultilinear()
  blinkInterpolatorEta = slimStructDataInterpolatorMultilinear()
  blinkUContainer = slimStructDataContainerNetcdf("./Data/bluelink_transport_27nov2007_35days.nc", "u", "lat", "lon")
  blinkVContainer = slimStructDataContainerNetcdf("./Data/bluelink_transport_27nov2007_35days.nc", "v", "lat", "lon")
  blinkEtaContainer = slimStructDataContainerNetcdf("./Data/bluelink_eta_27nov2007_35days.nc", "eta", "lat", "lon") 
  blinkU = slimFunctionStructData(blinkUContainer, blinkInterpolatorU, UTMtoLatLonDegrees_PC)
  blinkV = slimFunctionStructData(blinkVContainer, blinkInterpolatorV, UTMtoLatLonDegrees_PC)
  blinkEta = slimFunctionStructData(blinkEtaContainer, blinkInterpolatorEta, UTMtoLatLonDegrees_PC)
  blinkU.setTimeFunction(timeFunction)
  blinkV.setTimeFunction(timeFunction)
  blinkEta.setTimeFunction(timeFunction)
  #blinkEta.setMinValue(-3.0)
  #blinkEta.setMaxValue(3.0)
  blinkUV = functionC(glib, "lonLatVector_To_UTMVector", 3, [angleProj_PC, blinkU, blinkV])
  minFC = functionConstant(-3.0)
  blinkEta_cutOff = functionC(glib, "cutOffMin", 1, [blinkEta, minFC])

  blinkuv = functionC(glib,"transport2velocity",3,[blinkUV, bathDC.getFunction(), solution.getFunction()])
  boundaryUVElev_North_WIP = functionC(glib, "merge", 3, [blinkEta_cutOff, blinkuv])
  boundaryUVElev_Central_WIP = functionC(glib, "merge", 3, [blinkEta_cutOff, blinkuv])
  boundaryUVElev_South_WIP = functionC(glib, "merge", 3, [blinkEta_cutOff, blinkuv])

# .... GBROOS Boundary Currents
#NB: -9 to GBROOS input files if necessary (GMT time offset)
elif gbrResidual == 3:
  #Put data into functions
  data_Central = functionConstant([vx_B, vy_B])
  data_South = functionConstant([vx_C, vy_C])

  boundaryInterpolatorU_MYR = slimStructDataInterpolatorMultilinear()
  boundaryInterpolatorV_MYR = slimStructDataInterpolatorMultilinear()
  boundaryInterpolatorEta_MYR = slimStructDataInterpolatorMultilinear()
  boundaryElevContainer_MYR = slimStructDataContainerTemporalSerie("./Data/currentsForcing_MYR_CCH/MYR_elev_novdec07janfeb08.txt")
  boundaryUcontainer_MYR = slimStructDataContainerTemporalSerie("./Data/currentsForcing_MYR_CCH/MYR_ucur_novdec07janfeb08.txt")
  boundaryVcontainer_MYR = slimStructDataContainerTemporalSerie("./Data/currentsForcing_MYR_CCH/MYR_vcur_novdec07janfeb08.txt")
  boundaryElev_MYR = slimFunctionStructData(boundaryElevContainer_MYR, boundaryInterpolatorEta_MYR, UTMtoLatLonDegrees_PC)
  boundaryU_MYR = slimFunctionStructData(boundaryUcontainer_MYR, boundaryInterpolatorU_MYR, UTMtoLatLonDegrees_PC)
  boundaryV_MYR = slimFunctionStructData(boundaryVcontainer_MYR, boundaryInterpolatorV_MYR, UTMtoLatLonDegrees_PC)
  boundaryElev_MYR.setTimeFunction(timeFunction)
  boundaryU_MYR.setTimeFunction(timeFunction)
  boundaryV_MYR.setTimeFunction(timeFunction)
  boundaryUV_MYR = functionC(glib, "lonLatVector_To_UTMVector", 3, [angleProj_PC, boundaryU_MYR, boundaryV_MYR])
  boundaryUV_modulated_MYR = functionC(glib,"currentsAlongVecDotNormals", 3, [function.getNormals(), data_Central, boundaryUV_MYR])
  boundaryUVElev_Central_WIP = functionC(glib, "merge", 3, [boundaryElev_MYR, boundaryUV_modulated_MYR])

  boundaryInterpolatorU_CCH = slimStructDataInterpolatorMultilinear()
  boundaryInterpolatorV_CCH = slimStructDataInterpolatorMultilinear()
  boundaryInterpolatorEta_CCH = slimStructDataInterpolatorMultilinear()
  boundaryElevContainer_CCH = slimStructDataContainerTemporalSerie("./Data/currentsForcing_MYR_CCH/CCH_elev_novdec07janfeb08.txt")
  boundaryUcontainer_CCH = slimStructDataContainerTemporalSerie("./Data/currentsForcing_MYR_CCH/CCH_ucur_novdec07janfeb08.txt")
  boundaryVcontainer_CCH = slimStructDataContainerTemporalSerie("./Data/currentsForcing_MYR_CCH/CCH_vcur_novdec07janfeb08.txt")
  boundaryElev_CCH = slimFunctionStructData(boundaryElevContainer_CCH, boundaryInterpolatorEta_CCH, UTMtoLatLonDegrees_PC)
  boundaryU_CCH = slimFunctionStructData(boundaryUcontainer_CCH, boundaryInterpolatorU_CCH, UTMtoLatLonDegrees_PC)
  boundaryV_CCH = slimFunctionStructData(boundaryVcontainer_CCH, boundaryInterpolatorV_CCH, UTMtoLatLonDegrees_PC)
  boundaryElev_CCH.setTimeFunction(timeFunction)
  boundaryU_CCH.setTimeFunction(timeFunction)
  boundaryV_CCH.setTimeFunction(timeFunction)
  boundaryUV_CCH = functionC(glib, "lonLatVector_To_UTMVector", 3, [angleProj_PC, boundaryU_CCH, boundaryV_CCH])
  boundaryUV_modulated_CCH = functionC(glib,"currentsAlongVecDotNormals", 3, [function.getNormals(), data_South, boundaryUV_CCH])
  boundaryUVElev_South_WIP = functionC(glib, "merge", 3, [boundaryElev_CCH, boundaryUV_modulated_CCH])
  
  boundaryUVElev_North_WIP = functionConstant([0.0,0.0,0.0])

# .... Constant boundary currents
elif gbrResidual == 4:
  elevFactor = functionConstant([0.0])
  northAddFactor = functionConstant([0.0, 0.0])
  #centralAddFactor = functionConstant([0.0, -0.10])
  centralAddFactor = functionC(glib, "getRes3", 2, [UTMtoLonLatDegrees_PC])
  southAddFactor = functionConstant([0.08, -0.08])
  
  #Res3DC = dgDofContainer(groups, 2)
  #Res3DC.L2Projection(centralAddFactor)
  #Res3DC.exportMsh("Res3DC")
  
  boundaryUVElev_North_WIP = functionC(glib, "merge", 3, [elevFactor, northAddFactor])
  boundaryUVElev_Central_WIP = functionC(glib, "merge", 3, [elevFactor, centralAddFactor])
  boundaryUVElev_South_WIP = functionC(glib, "merge", 3, [elevFactor, southAddFactor])


# .... Add tides to boundary conditions
if gbrTide == 1 and gbrResidual != 0:
  boundaryUVElevAndTide_North_WIP = functionC(glib,"merge_3DFunctions",3,[tpxoTide, boundaryUVElev_North_WIP])
  boundaryUVElevAndTide_Central_WIP = functionC(glib, "merge_3DFunctions", 3, [tpxoTide, boundaryUVElev_Central_WIP])
  boundaryUVElevAndTide_South_WIP = functionC(glib, "merge_3DFunctions", 3, [tpxoTide, boundaryUVElev_South_WIP])

    
# .... Add wind-induced boundary currents: UV = UV_existing + f(windUV)
if gbrWindFluxAtBoundaries == 1 and gbrWind != 0:
  if gbrResidual == 0:
    if gbrTide == 0:
      eta = functionConstant([0.0])
      boundaryUVElev_North = functionC(glib, "merge", 3, [eta, currentFromWind_North])
      boundaryUVElev_Central = functionC(glib, "merge", 3, [eta, currentFromWind_Central])
      boundaryUVElev_South = functionC(glib, "merge", 3, [eta, currentFromWind_South])
    elif gbrTide == 1:
      boundaryUVElevAndTide_North = functionC(glib, "merge3D_with_UV", 3, [tpxoTide, currentFromWind_North])
      boundaryUVElevAndTide_Central = functionC(glib, "merge3D_with_UV", 3, [tpxoTide, currentFromWind_Central])
      boundaryUVElevAndTide_South = functionC(glib, "merge3D_with_UV", 3, [tpxoTide, currentFromWind_South])
  else:
    if gbrTide == 0:
      boundaryUVElev_North = functionC(glib, "merge3D_with_UV", 3, [boundaryUVElev_North_WIP, currentFromWind_North])
      boundaryUVElev_Central = functionC(glib, "merge3D_with_UV", 3, [boundaryUVElev_Central_WIP, currentFromWind_Central])
      boundaryUVElev_South = functionC(glib, "merge3D_with_UV", 3, [boundaryUVElev_South_WIP, currentFromWind_South])
    elif gbrTide == 1:
      boundaryUVElevAndTide_North = functionC(glib,"merge3D_with_UV",3,[boundaryUVElevAndTide_North_WIP, currentFromWind_North])
      boundaryUVElevAndTide_Central = functionC(glib,"merge3D_with_UV",3,[boundaryUVElevAndTide_Central_WIP, currentFromWind_Central])
      boundaryUVElevAndTide_South = functionC(glib,"merge3D_with_UV",3,[boundaryUVElevAndTide_South_WIP, currentFromWind_South])
else:
  if gbrTide == 0:
    boundaryUVElev_North = boundaryUVElev_North_WIP
    boundaryUVElev_Central = boundaryUVElev_Central_WIP
    boundaryUVElev_South = boundaryUVElev_South_WIP
  elif gbrTide == 1:
    boundaryUVElevAndTide_North = boundaryUVElevAndTide_North_WIP
    boundaryUVElevAndTide_Central = boundaryUVElevAndTide_Central_WIP
    boundaryUVElevAndTide_South = boundaryUVElevAndTide_South_WIP

  
# **********************************************

# ** OPTIONAL 1/3: Load an existing DC as the initial solution DC

if (startFromExistingSolution == 1):
  solution.readMsh( solutionFileName )

# **********************************************

ld = functionConstant(0)

#Cd = functionC(glib, "bottomDrag_old", 1, [solution.getFunction(), bathDC.getFunction()])
#Cd = functionC(glib, "bottomDrag", 1, [ solution.getFunction(), bathDC.getFunction(), reefsDC.getFunction() ])
Cd = functionC(glib, "bottomDrag_const_n016", 1, [ solution.getFunction(), bathDC.getFunction() ])
if(gbrDiff == 0):
  Di = functionConstant(diff)
elif(gbrDiff == 1):
  Di = functionC(glib, "smagorinsky", 1, [solution.getFunctionGradient()]) #***
Coriolis = functionC(glib, "coriolisLonLatDegrees", 1, [UTMtoLonLatDegrees])
if(gbrWind == 0):
  source = functionConstant([0.0, 0.0])
else:
  source = functionC(glib, "windStressSmithBanke", 2, [windUV, solution.getFunction(), bathDC.getFunction()])

claw.setCoriolisFactor(Coriolis)
claw.setQuadraticDissipation(Cd)
claw.setLinearDissipation(ld)
claw.setDiffusivity(Di)
claw.setSource(source)
claw.setBathymetry(bathDC.getFunction())
claw.setBathymetryGradient(bathDC.getFunctionGradient())

if(gbrResidual == 0):
  if(gbrTide == 0 and gbrWindFluxAtBoundaries == 0):
    claw.addBoundaryCondition('Open Sea North',claw.newBoundaryWall())
    claw.addBoundaryCondition('Open Sea Central',claw.newBoundaryWall())
    claw.addBoundaryCondition('Open Sea South',claw.newBoundaryWall())
    claw.addBoundaryCondition('Open Sea Neutral',claw.newBoundaryWall())
  elif(gbrTide == 1 and gbrWindFluxAtBoundaries == 0):
    claw.addBoundaryCondition('Open Sea North',claw.newOutsideValueBoundary("Surface", tpxoTide))
    claw.addBoundaryCondition('Open Sea Central',claw.newOutsideValueBoundary("Surface", tpxoTide))
    claw.addBoundaryCondition('Open Sea South',claw.newOutsideValueBoundary("Surface", tpxoTide))
    claw.addBoundaryCondition('Open Sea Neutral',claw.newOutsideValueBoundary("Surface", tpxoTide))
  if(gbrTide == 0 and gbrWindFluxAtBoundaries == 1):
    claw.addBoundaryCondition('Open Sea North',claw.newOutsideValueBoundary("Surface", boundaryUVElev_North))
    claw.addBoundaryCondition('Open Sea Central',claw.newOutsideValueBoundary("Surface", boundaryUVElev_Central))
    claw.addBoundaryCondition('Open Sea South',claw.newOutsideValueBoundary("Surface", boundaryUVElev_South))
    claw.addBoundaryCondition('Open Sea Neutral',claw.newOutsideValueBoundary("Surface", boundaryUVElev_Central))
  elif(gbrTide == 1 and gbrWindFluxAtBoundaries == 1):
    claw.addBoundaryCondition('Open Sea North', claw.newOutsideValueBoundary("Surface", boundaryUVElevAndTide_North))
    claw.addBoundaryCondition('Open Sea Central', claw.newOutsideValueBoundary("Surface", boundaryUVElevAndTide_Central))
    claw.addBoundaryCondition('Open Sea South', claw.newOutsideValueBoundary("Surface", boundaryUVElevAndTide_South))
    claw.addBoundaryCondition('Open Sea Neutral',claw.newOutsideValueBoundary("Surface", boundaryUVElevAndTide_Central))
elif(gbrResidual == 1 or gbrResidual == 2 or gbrResidual == 3 or gbrResidual == 4):
  if(gbrTide == 0):
    claw.addBoundaryCondition('Open Sea North',claw.newOutsideValueBoundary("Surface", boundaryUVElev_North))
    claw.addBoundaryCondition('Open Sea Central',claw.newOutsideValueBoundary("Surface", boundaryUVElev_Central))
    claw.addBoundaryCondition('Open Sea South',claw.newOutsideValueBoundary("Surface", boundaryUVElev_South))
    claw.addBoundaryCondition('Open Sea Neutral',claw.newOutsideValueBoundary("Surface", boundaryUVElev_Central))
  elif(gbrTide == 1):
    claw.addBoundaryCondition('Open Sea North', claw.newOutsideValueBoundary("Surface", boundaryUVElevAndTide_North))
    claw.addBoundaryCondition('Open Sea Central', claw.newOutsideValueBoundary("Surface", boundaryUVElevAndTide_Central))
    claw.addBoundaryCondition('Open Sea South', claw.newOutsideValueBoundary("Surface", boundaryUVElevAndTide_South))
    claw.addBoundaryCondition('Open Sea Neutral',claw.newOutsideValueBoundary("Surface", boundaryUVElevAndTide_Central))

claw.addBoundaryCondition('Coast',claw.newBoundaryWall())
claw.addBoundaryCondition('Island',claw.newBoundaryWall())
claw.addBoundaryCondition('Islands',claw.newBoundaryWall())

# **********************************************
#NB: Must make 1st argument of Cd solution.getFunction() to use the following:
bottomDragDC = dgDofContainer(groups, 1)
bottomDragDC.interpolate(Cd)
exporterQuadDissip = dgIdxExporter(bottomDragDC, 'GBR_Functions/quadDissipation_'+filename)
exporterQuadDissip.exportIdx(0,0)

# ****************** Time Int ******************
#WARNING: in MultiRate, all DofContainers are split into groups
#       ---> Must add every DC used by claw to splitForMultiRate!!!
if (Msg.GetCommRank() == 0):
  print(colored('Setting up time integrator ...',"red"))

rk = dgMultirateERK(groups, claw, RK_TYPE)
dt = rk.splitGroupsForMultirate(mL, solution, [solution, bathDC, reefsDC])
rk.printMultirateInfo(0)

# Recompute Precomputed functions after splitting of element groups
UTMtoLonLatDegrees_PC.compute(UTMtoLonLatDegrees)
UTMtoLatLonDegrees_PC.compute(UTMtoLatLonDegrees)
angleProj_PC.compute(angleProj)
functionCurrentAsFWind_depthFactor_PC.compute(functionCurrentAsFWind_depthFactor)

#OPTIONAL 2/3: if starting from existing solution, set dt = old_dt
if (startFromExistingSolution == 1):
  dt = old_dt
# **********************************************

##Write dissipation term
#dissipationTermFC = functionC(glib, "dissipationTerm", 2, [solution.getFunction(), solution.getFunctionGradient(), Di, bathDC.getFunction(), bathDC.getFunctionGradient()])
#dissipationTermDC = dgDofContainer(groups, 2)
#dissipationTermDC.interpolate(dissipationTermFC)
#exporterDissipationTerm = dgIdxExporter(dissipationTermDC, outputDir+'/DissipationTerm/dissipationTerm')
#exporterDissipationTerm.exportIdx(0,0)

#Write windUV term
windUVDC = None
if (gbrWind != 0):
  windUVDC = dgDofContainer(groups, 3)
  windUVDC.L2Projection(windUV)
  exporterWindUV = dgIdxExporter(windUVDC, outputDir+'/windUV')
  exporterWindUV.exportIdx(0, t)
  
#Write windStress term
windSourceDC = None
if (gbrWind != 0):
  windSourceDC = dgDofContainer(groups, 2)
  windSourceDC.L2Projection(source)
  exporterWindSourceAcc = dgIdxExporter(windSourceDC, outputDir+'/windSourceAcc')
  exporterWindSourceAcc.exportIdx(0, t)
  
#Write blink term
blinkSolDC = None
if(gbrResidual == 2):
  blinkSolDC = dgDofContainer(groups, 3)
  blinkSolDC.L2Projection(boundaryUVElev_Central)
  exporterBlinkSol=dgIdxExporter(blinkSolDC, outputDir+'/blinkSol_boundaryTerm')
  exporterBlinkSol.exportIdx(0, t)
  
##Write GBROOS boundary currents/elev term
#boundaryGBROOS_MYRSol_DC = None
#boundaryGBROOS_CCHSol_DC = None
#if(gbrResidual == 3):
  #boundaryGBROOS_MYRSol_DC = dgDofContainer(groups, 3)
  #boundaryGBROOS_MYRSol_DC.L2Projection(boundaryUVElev_MYR)
  #exporterGBROOS_MYRSol=dgIdxExporter(boundaryGBROOS_MYRSol_DC, outputDir+'/boundaryGBROOS_MYRSol')
  #exporterGBROOS_MYRSol.exportIdx(0, t)
  #boundaryGBROOS_CCHSol_DC = dgDofContainer(groups, 3)
  #boundaryGBROOS_CCHSol_DC.L2Projection(boundaryUVElev_CCH)
  #exporterGBROOS_CCHSol=dgIdxExporter(boundaryGBROOS_CCHSol_DC, outputDir+'/boundaryGBROOS_CCHSol')
  #exporterGBROOS_CCHSol.exportIdx(0, t)

currentFunction = functionC(glib,"current",3,[solution.getFunction()]) #***
if gbrTide != 0:
  tideCurrentFunction = functionC(glib,"current",3,[tpxoTide])

nbSteps = int(ceil((Tf-Ti)/dt))
nbExport = 0

if(Msg.GetCommRank() == 0):
  if(os.path.exists(outputDir+'/mr-gbr')):
    try : os.system("rm -r "+outputDir+'/mr-gbr');
    except: 0;
exporterSol=dgIdxExporter(solution, outputDir+'/mr-gbr')
#exporterSol=dgIdxExporter(currentFunction, outputDir+'/mr-gbr-func')

norm = solution.norm()
waterDepth = functionC(glib, "getWaterDepth", 1, [solution.getFunction(), bathDC.getFunction()])
integrator = dgFunctionIntegrator(groups,  waterDepth,  solution)
totalMass=fullMatrixDouble(1, 1)
totalMassRef=fullMatrixDouble(1, 1)
integrator.compute(totalMassRef, "")


if (Msg.GetCommRank() == 0):
  print ''
  print(colored('******************** Initialising ********************', "blue"))
  print ''
  print '     - Number of processors:', Msg.GetCommSize()
  print '     - Multirate Runge Kutta Scheme:', RK_TYPE
  print '     - Initial Time (Ti):',  printDate(Ti)
  print '     - Final Time (Tf):',  printDate(Tf)
  print '     - Simulation Length (days):',  (Tf-Ti)/3600.0/24.0  
  print '     - Reference Time Step:', printTime(dt), ', which is', str(dt), 's'
  print '     - Number of Iterations:', nbSteps
  print '     - Theoretical Speedup:', rk.speedUp()
  print '     - Norm of Solution at Ti:',  norm
  print '     - Mass of Solution at Ti:',  totalMassRef(0, 0)
  print ''
  print '     - Tides:', gbrTideLabels[gbrTide], ' || Wind:', gbrWindLabels[gbrWind], ' || Wind-induced boundary UV:', gbrWindFluxAtBoundariesLabels[gbrWindFluxAtBoundaries], ' || Residual current:', gbrResidualLabels[gbrResidual]
  print ''
  print '     - Output Directory:', outputDir
  print ''
  print(colored('******************************************************', "blue"))
  print ''
  
  #Write info file for LPTracker
  simInfo = [str(dt)+'\n', str(nbSteps)+'\n', str(export)+'\n', str(Ti)+'\n', str(outputDir)+'\n', str(filename)]
  simInfoFile = open(outputDir+'/simulationInfo.txt','w')
  simInfoFile.writelines(simInfo)
  simInfoFile.close()

#Initialise Function Evaluators
FunctionEval = dgFunctionEvaluator(groups, currentFunction)
BathEval = dgFunctionEvaluator(groups, bathDC.getFunction())
ElevationEval = dgFunctionEvaluator(groups, solution.getFunction())
WindUVEval = None
if (gbrWind != 0):
  WindUVEval = dgFunctionEvaluator(groups, windUV)
res = fullMatrixDouble()

#Evaluate Bathymetry Evaluator
outBathEval = open(outputDir+'/Evaluator/evalBathymetry_StdLocations.dat',"w")
outBathGBROOSEval = open(outputDir+'/Evaluator/evalBathymetry_GBROOSLocations.dat',"w")
for point in range(0,len(evalPointsLatLon)):
  #Only write to file if we are on correct cpu:
  BathEval.compute(evalPointsCart[point][0],evalPointsCart[point][1],evalPointsCart[point][2], res)
  outBathEval.write(str(res(0,0))+'\n')
outBathEval.close()
for point in range(0,len(evalPointsGBROOSLatLon)):
  #Only write to file if we are on correct cpu:
  BathEval.compute(evalPointsGBROOSCart[point][0],evalPointsGBROOSCart[point][1],evalPointsGBROOSCart[point][2], res)
  outBathGBROOSEval.write(str(res(0,0))+'\n')
outBathGBROOSEval.close()

#Create total mass file
if (Msg.GetCommRank() == 0):
  outMass = open(outputDir+'/totalMass.dat',"w")
  outMass.write(str(t/3600-Ti/3600)+'\t'+str(totalMassRef(0,0))+'\n')
  outMass.close()

# **********************************************
exporterSol.exportIdx(0, t)
# ****************** Iterate  ******************
t_exportStart = Ti + t_exportStart
startcpu = time.clock()

##TEST: OUTPUT VALUE OF WIND (PART 1 OF 2)
#if (Msg.GetCommRank() == 0):
  #for siteId in range(0,len(evalPointsGBROOSLatLon)):
    #outWind = open(outputDir+'/Evaluator/WINDUV_%02d'%(evalPointsGBROOSLatLon[siteId][3])+'.dat',"w")
    #outWind.close()

# OPTIONAL 3/3: if starting from an initial solution, adjust start & end steps accordingly
firstStep = 1
lastStep = 1
if (startFromExistingSolution == 1):
  firstStep = stepNbOfInitialSolution + 1
  lastStep = stepNbOfInitialSolution + nbSteps + 1
else:
  firstStep = 1
  lastStep = nbSteps + 1

if (Msg.GetCommRank() == 0):
  print 'Starting simulation from iteration #',firstStep,' ...'
  print ''
for i in range(firstStep, lastStep):
  if(i == lastStep-1):
    dt=Tf-t
  norm = rk.iterate (solution, dt, t)
  t = t +dt
  timeFunction.set(t)
  if ( i%iter == 0 ):
     norm=solution.norm()
     if(Msg.GetCommRank() == 0):
       print''
       print(colored('|ITER| %d of %d',  "red")%(i,lastStep))
       print(colored('------------------------------------------------------', "red"))
       print'|TIME|', printDate(t)
       print'|TIME ELAPSED|', printTime((t - Ti))
       print'| DT |','%.2f'%(dt)
       print'|NORM|','%.4f'%(norm)
       print'|CPUT|',printTime(time.clock() - startcpu)
       print(colored('------------------------------------------------------', "red"))
       print''
  if ( i%export  == 0 and t > t_exportStart ):
    exporterSol.exportIdx(i, t)
  if ( i%export_extraTerms == 0 ):
    #exporterDissipationTerm.exportIdx(i, t)
    if(gbrWind!=0):
      windUVDC.L2Projection(windUV)
      exporterWindUV.exportIdx(i, t)
      windSourceDC.L2Projection(source)
      exporterWindSourceAcc.exportIdx(i, t)
    if(gbrResidual == 2):
      blinkSolDC.L2Projection(blink_VelAndElev)
      exporterBlinkSol.exportIdx(i, t)
    if(gbrResidual == 3):
      boundaryGBROOS_MYRSol_DC.L2Projection(boundaryUVElev_MYR)
      exporterGBROOS_MYRSol.exportIdx(i, t)
      boundaryGBROOS_CCHSol_DC.L2Projection(boundaryUVElev_CCH)
      exporterGBROOS_CCHSol.exportIdx(i, t)
  if ( i%export_fnEval  == 0 ):    
    #Export Function Evaluator results:
    integrator.compute(totalMass, "")
    relMass=totalMass(0, 0)
    if (Msg.GetCommRank() == 0):
      outMass = open(outputDir+'/totalMass.dat',"a")
      outMass.write(str(t/3600-Ti/3600)+'\t'+str(relMass)+'\n')
      outMass.close()
    #*** Current evaluator: ***
    for point in range(0,len(evalPointsCart)):
      #Only write to file if we are on correct cpu:
      evalPointSP = SPoint3(evalPointsCart[point][0],evalPointsCart[point][1],evalPointsCart[point][2])
      evalPointElem = model.getMeshElementByCoord(evalPointSP)
      whichPartition = evalPointElem.getPartition()
      FunctionEval.compute(evalPointsCart[point][0],evalPointsCart[point][1],evalPointsCart[point][2], res)
      #Project velocities onto lonLat space
      res_uv = fullMatrixDouble(1,2)
      #xyVector(res_uv, evalPointsCart[point][0], evalPointsCart[point][1], res)
      if (Msg.GetCommRank()+1 == whichPartition):
        outEval = open(outputDir+'/Evaluator/evalCurrent_%02d'%(point)+'.dat',"a")
        outEval.write(str(t/3600-Ti/3600)+'\t'+str(res.get(0,0))+'\t'+str(res.get(0,1))+'\n')
        outEval.close()
    #*** Elevation evaluator - GBROOS: ***
    for point in range(0,len(evalPointsGBROOSLatLon)):
      #Only write to file if we are on correct cpu:
      evalPointSP = SPoint3(evalPointsGBROOSCart[point][0],evalPointsGBROOSCart[point][1],evalPointsGBROOSCart[point][2])
      evalPointElem = model.getMeshElementByCoord(evalPointSP)
      whichPartition = evalPointElem.getPartition()
      ElevationEval.compute(evalPointsGBROOSCart[point][0],evalPointsGBROOSCart[point][1],evalPointsGBROOSCart[point][2], res)
      res2 = fullMatrixDouble()
      FunctionEval.compute(evalPointsGBROOSCart[point][0],evalPointsGBROOSCart[point][1],evalPointsGBROOSCart[point][2], res2)
      if (Msg.GetCommRank()+1 == whichPartition):
        outElevEval = open(outputDir+'/Evaluator/evalElevGBROOS_%02d'%(evalPointsGBROOSLatLon[point][3])+'.dat',"a")
        outElevEval.write(str(t/3600-Ti/3600)+'\t'+str(res.get(0,0))+'\n')
        outElevEval.close()
        outEval = open(outputDir+'/Evaluator/evalCurrentGBROOS_%02d'%(evalPointsGBROOSLatLon[point][3])+'.dat',"a")
        outEval.write(str(t/3600-Ti/3600)+'\t'+str(res2.get(0,0))+'\t'+str(res2.get(0,1))+'\n')
        outEval.close()
        
      ##TEST: OUTPUT VALUE OF WIND (PART 2 OF 2) (Doesn't work if using Precomputed functions in wind)
      #if gbrWind != 0:
        #res3 = fullMatrixDouble()
        #WindUVEval.compute(evalPointsGBROOSCart[point][0],evalPointsGBROOSCart[point][1],evalPointsGBROOSCart[point][2], res3)
        #if (Msg.GetCommRank()+1 == whichPartition):
          #outWind = open(outputDir+'/Evaluator/WINDUV_%02d'%(evalPointsGBROOSLatLon[point][3])+'.dat',"a")
          #outWind.write(str(t/3600-Ti/3600)+'\t'+str(res3.get(0,0))+'\t'+str(res3.get(0,1))+'\n')
          #outWind.close()
        
    #*** Elevation evaluator - Seafarer: ***
    for point in range(0,len(tidalPointsSeafarerLatLon)):
      #Only write to file if we are on correct cpu:
      evalPointSP = SPoint3(tidalPointsSeafarerCart[point][0],tidalPointsSeafarerCart[point][1],tidalPointsSeafarerCart[point][2])
      evalPointElem = model.getMeshElementByCoord(evalPointSP)
      whichPartition = evalPointElem.getPartition()
      ElevationEval.compute(tidalPointsSeafarerCart[point][0],tidalPointsSeafarerCart[point][1],tidalPointsSeafarerCart[point][2], res)
      if (Msg.GetCommRank()+1 == whichPartition):
        outElevEval = open(outputDir+'/Evaluator/evalElevSeafarer_%02d'%(point)+'.dat',"a")
        outElevEval.write(str(t/3600-Ti/3600)+'\t'+str(res.get(0,0))+'\n')
        outElevEval.close()
    #*** Reef Points: current & elevation evaluator: ***
    for point in range(0,len(reefPointsLatLon)):
      #Only write to file if we are on correct cpu:
      evalPointSP = SPoint3(reefPointsCart[point][0],reefPointsCart[point][1],reefPointsCart[point][2])
      evalPointElem = model.getMeshElementByCoord(evalPointSP)
      whichPartition = evalPointElem.getPartition()
      ElevationEval.compute(reefPointsCart[point][0],reefPointsCart[point][1],reefPointsCart[point][2], res)
      if (Msg.GetCommRank()+1 == whichPartition):
        outEval = open(outputDir+'/Evaluator/evalReefPoint_%02d'%(point)+'.dat',"a")
        outEval.write(str(t/3600-Ti/3600)+'\t'+str(res.get(0,0))+'\t'+str(res.get(0,1))+'\t'+str(res.get(0,2))+'\n')
        outEval.close()

endcpu=time.clock()
norm=solution.norm()
Msg.Barrier()
if (Msg.GetCommRank() == 0):
  print ''
  print(colored('********************     End      ********************', "blue"))
  print ''
  print '     - Final Time (Tf):',  printDate(t)
  print '     - Norm of Solution at Tf:',  norm
  print '     - Total CPU Time:', endcpu-startcpu
  print ''
  print(colored('******************************************************', "blue"))
  print ''
Msg.Exit(0)
