from dgpy import *
from ProjectMesh import *
import calendar, os, time
from math import sqrt

#================
# 1. CHOOSE MESH
#----------------
mesh = "8K_nov2012"
#mesh = "460K_nov2012"

#SET RADIUS: 6.37101e6 for 'sma_zoomed', else 6.371e6
R = 6.371e6
#================

#================
# 2. SET FILE NAME & OUTPUT DIRECTORY
#----------------
filename = "gbr"+mesh
outputDir = "output"
#================

#================
#3. SET BATHYMETRY & REEF MAP DIRECTORIES
#----------------
bathdir=outputDir+"/Bath/"+filename+"_bath_smooth"
bathname=outputDir+"/Bath/"+filename+"_bath_smooth"+"/"+filename+"_bath_smooth.idx"

reefMapFile="Data/ReefMap_9200.dat"
#================

#================
#4. SET EXPORT PARAMETERS
#----------------
# Print every "iter" iterations, export every "export" iterations, export from t=t_exportStart
iter = 1
export = 1000
export_fnEval = 1000
export_extraTerms = 10
t_exportStart = 0 # 6*24*3600.0
#================

# Mesh Parameters
order = 1
dimension = 2

#================
# 5. SET INITIAL AND FINAL TIME [year, month, day, hour, minute, second]
#----------------
#Ti = calendar.timegm([2007, 11, 28, 0, 0, 0])
Ti = calendar.timegm([2008, 12, 18, 1, 0, 0])
Tf = Ti + 2*3600.0#Ti + 36*24*3600.0
#================

#================
# 6. OPTIONAL: LOAD INITIAL SOLUTION FROM EXISTING FILEi
# NB: must also set, above, Ti = Ti of old simulation
#----------------
# startFromExistingSolution: 0 if no, 1 if yes
startFromExistingSolution = 0

stepNbOfInitialSolution = 53360
solutionFileName = '/gbr-%06d'%( stepNbOfInitialSolution )+'.idx'
old_dt = 1
if (startFromExistingSolution == 1):
  Ti = Ti + stepNbOfInitialSolution*old_dt
  Tf = Ti + 7*24*3600
#================

#================
# 7. OTHER PARAMETERS
#----------------
# Radius of the Earth (for stereo projection)
EarthRadius = 6371e3

# Multirate and Partition Parameters
RK_TYPE = ERK_22_C
mL = 1000
algo = 5

"""
Number of Refinement Levels for the Multirate Method
"""
mrMl=1000       #SingleRate: set mL = 1


# Physical parameters for bathymetry smoothing
bNu=20    # Diffusion
bT=10000  # Time for smoothing 
#================

#================
# 8. PHYSICAL PARAMETERS FOR GBR SIMULATION
#----------------
"""
-gbrDiff= 0: Constant Diffusion diff || 1: Parametrization of Smagorinsky 
"""
gbrDiff=1
diff=1
#================

#================
# 9. SET FORCINGS
#----------------
"""
-gbrWind= 0: NO wind  ||  1: 1-point time-series ||  2: NCEP netCDF data ||  3: CSFR netCDF data
"""
gbrWind=3

gbrWindLabels=['NONE', 'Single point', 'NCEP data', 'CSFR data']

"""
gbrTide= 0: NO tide  ||  1: tpxo data
"""
gbrTide=1

gbrTideLabels=['NONE', 'Tpxo data']

"""
gbrResidual= 0: NO residual current  ||  1: standard NCJ forcing || 2: from bluelink at boundaries || 3: from GBROOS currents
"""
gbrResidual=0

gbrResidualLabels=['NONE', 'Const. NCJ forcing', 'Bluelink at boundaries', 'GBROOS at boundaries']

#Define residual current fluxes (North, Central, South)
F_A = 3.0e5
F_B = -10.0e5
F_C = 7.0e5

#Define current direction vectors at boundary in stereographic coordinates
vx_A_stereo = 0.76
vy_A_stereo = -sqrt(1 - vx_A_stereo*vx_A_stereo)
vx_B_stereo = 0.06
vy_B_stereo = -sqrt(1 - vx_B_stereo*vx_B_stereo)
vx_C_stereo = -0.98
vy_C_stereo = -sqrt(1 - vx_C_stereo*vx_C_stereo)

#Define current direction vectors at boundary
vx_A = 0.0
vy_A = sqrt(1 - vx_A*vx_A)
vx_B = 0.76
vy_B = sqrt(1 - vx_B*vx_B)
vy_C = -0.7
vx_C = sqrt(1 - vy_C*vy_C)
#================

#================
#10. Define lat/lon points at which to evaluate current and convert to 2D, projected Cartesian
#----------------
evalPointsLatLon = [ 
[-14.7406, 145.4253, 'Lizard Island', 0],
[-19.6253, 147.9142, 'Cape Upstart', 1],
[-19.4071, 148.0197, 'Old Reef', 2],
[-18.8311, 148.2896, 'near shelf break', 3],
#
[-19.0600, 147.9597, 'Bowden Reef', 4],
[-19.9826, 148.5833, 'Rattray Island', 5],
[-19.9400, 149.1100, 'Hook', 6],
[-20.8900, 150.1600, 'Bushy', 7],
[-21.8200, 151.1400, 'Bell', 8] ]
[-18.241667, 147.366667, 'Myrmidon Reef', 9], #NB: WAS -18.2452, 147.4100

#tidalPointsAbbotLatLon = [ 
#[-19.8187007904, 148.013000488, 'Deepwater West', 0],
#[-19.9050006866, 148.005004883, 'Coastal West', 1],
#[-19.8815994263, 148.074996948, 'Coastal Footprint', 2],
#[-19.8484992981, 148.04699707, 'Inshore Footprint', 3],
#[-19.8791999817, 147.970993042, 'Inshore West', 4],
#[-19.831199646, 148.115005493, 'Deepwater East', 5],
#[-19.9340991974, 148.130004883, 'Coastal East', 6],
#[-19.8999004364, 148.158996582, 'Inshore East', 7] ]

evalPointsGBROOSLatLon = [ 
#[-14.33923333, 145.34375, 'Lizard Island Slope', 0],
[-14.69953333, 145.6436, 'Lizard Island Shelf', 1],
[-18.3127666667, 147.151216667, 'Palm Passage Shelf', 2],
[-18.21963333, 147.343683333, 'Myrmidon Slope', 3],
#[-21.03088333, 152.8793, 'Elusive Reef', 4],
[-22.22648333, 151.804683333, 'Capricorn Channel', 5],
[-23.38263333, 151.986916667, 'Heron Island North', 6],
[-23.51233333, 151.9553, 'Heron Island South', 7],
[-23.48338333, 152.1726333, 'One Tree East', 8],
[-19.306117, 147.620217, 'Yongala', 9]]

tidalPointsSeafarerLatLon = [ 
[-18.466667, 146.866667, 'Rib Reef', 0],
[-19.238889, 146.838889, 'Townsville', 1],
[-18.933333, 148.066667, 'Shrimp Reef', 2],
[-19.283333, 148.066667, 'Stanley Reef', 3],
[-19.85, 148.116667, 'Abbot Point', 4],
[-19.5, 149.143333, 'Kennedy Reef', 5],
[-20.266667,149.066667, 'Haslewood Island', 6],
[-20.083333, 150.3, 'Bugatti Reef', 7],
[-20.533333, 150.366667, 'Creal Reef', 8],
[-20.997222, 149.9, 'Penrith Island', 9] ]

#Points on reefs
reefPointsLatLon = [
[-19.701517,149.380458, 'Reef A', 0], 
[-19.004364,148.100131, 'Reef B', 1],
[-19.419703,148.760181, 'Reef C', 2],
[-19.140206,147.636978, 'Reef D', 3],
[-18.587256,147.100464, 'Passage between reefs', 4],
[-19.921219,149.015717, 'Open sea', 5] ]
#================

if(Msg.GetCommRank() == 0):
  try : os.mkdir(outputDir);
  except: 0;
  try : os.mkdir(outputDir+'/Evaluator');
  except: 0;
  
# **********************************************
if (Msg.GetCommRank() == 0):
  print 'Loading evaluator points ...'
  
# ********* Current Evaluator Points  **********
#if (Msg.GetCommRank() == 0):
#  print 'Current evaluator points.'

evalPointsCart = [ [0]*5 for i in range(len(evalPointsLatLon)) ]
for i in range(0,len(evalPointsLatLon)):
  cartCoords = [0]*3
  evalPointsCart[i][3] = evalPointsLatLon[i][2] #Site name
  evalPointsCart[i][4] = evalPointsLatLon[i][3] #Site Id#
  geoCoordTo3DCartesian(evalPointsLatLon[i][0],evalPointsLatLon[i][1],cartCoords, R)
  projectPoint(cartCoords)
  for j in range(0,2):
    evalPointsCart[i][j] = cartCoords[j]
#  if (Msg.GetCommRank() == 0):
#    print str(evalPointsCart[i][3]),': ', evalPointsLatLon[i][0], '(lat) |', evalPointsLatLon[i][1], '(lon)'
#    print 'x(flat): ', evalPointsCart[i][0]/1000, '(km) | y(flat): ', evalPointsCart[i][1]/1000,'(km) | z(flat): ', evalPointsCart[i][2]/1000, '(km)'
if (Msg.GetCommRank() == 0):
  outEvalPos = open(outputDir+'/Evaluator/Points_evalCurrent.pos',"w")
  outEvalPos.write('View "evalCurrent mooring points" {\n')
  for siteId in range(0,len(evalPointsCart)):
    outEval = open(outputDir+'/Evaluator/evalCurrent_%02d'%(siteId)+'.dat',"w")
    outEval.close()
    outEvalPos.write('SP ('+str(evalPointsCart[siteId][0])+','+str(evalPointsCart[siteId][1])+',0) {'+str(siteId)+'};\n')
  outEvalPos.write('};')
  outEvalPos.close()
  
# ********* Elevation and current Evaluator Points  **********
# *** GBROOS points ***
#if (Msg.GetCommRank() == 0):
#  print ' "GBROOS" Elevation & current evaluator points:'
evalPointsGBROOSCart = [ [0]*5 for i in range(len(evalPointsGBROOSLatLon)) ]
for i in range(0,len(evalPointsGBROOSLatLon)):
  cartCoords = [0]*3
  evalPointsGBROOSCart[i][3] = evalPointsGBROOSLatLon[i][2] #Site name
  evalPointsGBROOSCart[i][4] = evalPointsGBROOSLatLon[i][3] #Site Id#
  geoCoordTo3DCartesian(evalPointsGBROOSLatLon[i][0],evalPointsGBROOSLatLon[i][1],cartCoords, R)
  projectPoint(cartCoords)
  for j in range(0,2):
    evalPointsGBROOSCart[i][j] = cartCoords[j]
#  if (Msg.GetCommRank() == 0):
#    print str(evalPointsGBROOSCart[i][3]),': ', evalPointsGBROOSLatLon[i][0], '(lat) |', evalPointsGBROOSLatLon[i][1], '(lon)'
#    print 'x(flat): ', evalPointsGBROOSCart[i][0]/1000, '(km) | y(flat): ', evalPointsGBROOSCart[i][1]/1000,'(km) | z(flat): ', evalPointsGBROOSCart[i][2]/1000, '(km)'
if (Msg.GetCommRank() == 0):
  outGBROOSPos = open(outputDir+'/Evaluator/Points_GBROOS.pos',"w")
  outGBROOSPos.write('View "GBROOS mooring points" {\n')
  for siteId in range(0,len(evalPointsGBROOSCart)):
    outElevEval = open(outputDir+'/Evaluator/evalElevGBROOS_%02d'%(evalPointsGBROOSLatLon[siteId][3])+'.dat',"w")
    outCurrentEval = open(outputDir+'/Evaluator/evalCurrentGBROOS_%02d'%(evalPointsGBROOSLatLon[siteId][3])+'.dat',"w")
    outElevEval.close()
    outCurrentEval.close()
    outGBROOSPos.write('SP ('+str(evalPointsGBROOSCart[siteId][0])+','+str(evalPointsGBROOSCart[siteId][1])+',0) {'+str(siteId)+'};\n')
  outGBROOSPos.write('};')
  outGBROOSPos.close()

# ********* Elevation Evaluator Points  **********
# *** Seafarer points ***
#if (Msg.GetCommRank() == 0):
#  print ' "Seafarer" Elevation evaluator points:'
tidalPointsSeafarerCart = [ [0]*5 for i in range(len(tidalPointsSeafarerLatLon)) ]
for i in range(0,len(tidalPointsSeafarerLatLon)):
  cartCoords = [0]*3
  tidalPointsSeafarerCart[i][3] = tidalPointsSeafarerLatLon[i][2] #Site name
  tidalPointsSeafarerCart[i][4] = tidalPointsSeafarerLatLon[i][3] #Site Id#
  geoCoordTo3DCartesian(tidalPointsSeafarerLatLon[i][0],tidalPointsSeafarerLatLon[i][1],cartCoords, R)
  projectPoint(cartCoords)
  for j in range(0,2):
    tidalPointsSeafarerCart[i][j] = cartCoords[j]
#  if (Msg.GetCommRank() == 0):
#    print str(tidalPointsSeafarerCart[i][3]),': ', tidalPointsSeafarerLatLon[i][0], '(lat) |', tidalPointsSeafarerLatLon[i][1], '(lon)'
#    print 'x(flat): ', tidalPointsSeafarerCart[i][0]/1000, '(km) | y(flat): ', tidalPointsSeafarerCart[i][1]/1000,'(km) | z(flat): ', tidalPointsSeafarerCart[i][2]/1000, '(km)'
if (Msg.GetCommRank() == 0):
  outElevEvalPos = open(outputDir+'/Evaluator/Points_evalElevSeafarer.pos',"w")
  outElevEvalPos.write('View "Seafarer mooring points" {\n')
  for siteId in range(0,len(tidalPointsSeafarerCart)):
    outElevEval = open(outputDir+'/Evaluator/evalElevSeafarer_%02d'%(siteId)+'.dat',"w")
    outElevEval.close()
    outElevEvalPos.write('SP ('+str(tidalPointsSeafarerCart[siteId][0])+','+str(tidalPointsSeafarerCart[siteId][1])+',0) {'+str(siteId)+'};\n')
  outElevEvalPos.write('};')
  outElevEvalPos.close()

# *** reef points ***
#if (Msg.GetCommRank() == 0):
#  print ' "Reef Point" Current & elevation evaluator points:'
reefPointsCart = [ [0]*5 for i in range(len(reefPointsLatLon)) ]
for i in range(0,len(reefPointsLatLon)):
  cartCoords = [0]*3
  reefPointsCart[i][3] = reefPointsLatLon[i][2] #Site name
  reefPointsCart[i][4] = reefPointsLatLon[i][3] #Site Id#
  geoCoordTo3DCartesian(reefPointsLatLon[i][0],reefPointsLatLon[i][1],cartCoords, R)
  projectPoint(cartCoords)
  for j in range(0,2):
    reefPointsCart[i][j] = cartCoords[j]
#  if (Msg.GetCommRank() == 0):
#    print str(reefPointsCart[i][3]),': ', reefPointsLatLon[i][0], '(lat) |', reefPointsLatLon[i][1], '(lon)'
#    print 'x(flat): ', reefPointsCart[i][0]/1000, '(km) | y(flat): ', reefPointsCart[i][1]/1000,'(km) | z(flat): ', reefPointsCart[i][2]/1000, '(km)'
if (Msg.GetCommRank() == 0):
  outReefPointsPos = open(outputDir+'/Evaluator/Points_evalReefs.pos',"w")
  outReefPointsPos.write('View "Reef points" {\n')
  for siteId in range(0,len(reefPointsCart)):
    outReefPoints = open(outputDir+'/Evaluator/evalReefPoint_%02d'%(siteId)+'.dat',"w")
    outReefPoints.close()
    outReefPointsPos.write('SP ('+str(reefPointsCart[siteId][0])+','+str(reefPointsCart[siteId][1])+',0) {'+str(siteId)+'};\n')
  outReefPointsPos.write('};')
  outReefPointsPos.close()
