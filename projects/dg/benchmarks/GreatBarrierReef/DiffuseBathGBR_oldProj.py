# Script to generate smooth bathymetry. 
#   Filename is set in InputGBR.py
#   CARTESIAN
#NB: bathy class defined in dgParticleTracker2D.h

from dgpy import *
from ProjectMesh import *
import os, sys

#-------------
#filename = "gbr10K"
filename = sys.argv[1]
#-------------
# Physical parameters for bathymetry smoothing
bNu=20    # Diffusion
bT=10000  # Time for smoothing 
#-------------

try : os.mkdir('./Meshes');
except: 0;
try : os.mkdir('./Data');
except: 0;
if(not os.path.exists("./Meshes/"+filename+".msh")):
  try : os.system("make "+filename+".msh");
  except: 0;
try : os.system("make forcings");
except: 0;

print""
print"Generating smooth bathymetry for CARTESIAN PLANE mesh:", filename


#First project mesh into planar space
if(not os.path.exists("./Meshes/"+filename+"_tan.msh")):
  print'Projecting mesh in planar space ...'
  m = GModel()
  m.load("./Meshes/"+filename+".msh")
  projectMeshPlane(m)
  m.save("./Meshes/"+filename+"_tan.msh")
model=GModel()
model.load("Meshes/"+filename+"_tan.msh")

#Compile libraries
if (Msg.GetCommRank() == 0):
  functionC.buildLibraryFromFile ("./Libraries/GBR.cc", "./Libraries/lib_gbr.so")
glib = "./Libraries/lib_gbr.so"

#Get started
if(Msg.GetCommSize() > 1):
  Msg.Fatal("You need to run the script a first time on only one processor before you can run it on more!")
Msg.Info( "Diffuse bathymetry")

#Create output folder
try : os.mkdir('./Bath');
except: 0;
  
XYZ = function.getCoordinates()
lonLatDegrees = functionC(glib,"lonLatDegrees",3,[XYZ])
groups = dgGroupCollection(model,2,1)
bathDC = dgDofContainer(groups,1);

#OLD:
#bathS = functionC(blib, "bath", 1, [stereoToLonLatDegrees])
#bathDC.interpolate(bathS);

#NEW:
#1. Create a bathy object (initialises object and creates bathymetry function)
bathymetryObj = bathy("./Data/BathHR.bin", lonLatDegrees)
#2. Interpolate the function onto DC
bathDC.interpolate(bathymetryObj)
#3. Delete bathymetry object data
bathymetryObj.clearBathData()

#Diffuse & save:
nuB = functionConstant(bNu)
dlaw = dgConservationLawAdvectionDiffusion.diffusionLaw(nuB)

dlaw.addBoundaryCondition("Coast", dlaw.new0FluxBoundary())
dlaw.addBoundaryCondition("Island", dlaw.new0FluxBoundary())
dlaw.addBoundaryCondition("Islands", dlaw.new0FluxBoundary())
dlaw.addBoundaryCondition("Open Sea North", dlaw.new0FluxBoundary())
dlaw.addBoundaryCondition("Open Sea Central", dlaw.new0FluxBoundary())
dlaw.addBoundaryCondition("Open Sea South", dlaw.new0FluxBoundary())

sys = linearSystemPETScDouble()
dof = dgDofManager.newCG (groups, 1, sys)
implicitEuler = dgDIRK(dlaw, dof)
implicitEuler.iterate (bathDC, bT, 0)
exporterBath=dgIdxExporter(bathDC, "./Bath/"+filename+"_bath_smooth");
exporterBath.exportIdx()
sys = 0
dof = 0
Msg.Exit(0)
