# Import for python
from dgpy import *
from gmshpy import *
from math import *
import os

# !!! WARNING: OLD MESHES HAVE R=6.37101e6, NEW MESHES HAVE R=6.371e6 !!! #

# Projection Parameters
phi = 146.5892/180*pi
theta = -17.147569/180*pi
Ox = cos(theta)*cos(phi)
Oy = cos(theta)*sin(phi)
Oz = sin(theta)
eThetaX = -sin(theta)*cos(phi)
eThetaY = -sin(theta)*sin(phi)
eThetaZ = cos(theta)
ePhiX = -sin(phi)
ePhiY = cos(phi)
ePhiZ = 0

# Project 3D Cartesian mesh onto UTM (or other) plane
def projectMeshPROJ(model, lonlatProjName, destProjName):
  def project(entity):
    numVertices = entity.getNumMeshVertices()
    m = fullMatrixDouble (3,3)
    for iV in range(0,numVertices): 
      v = entity.getMeshVertex(iV)
      
      # 3D Cartesian --> lat/lon
      R = sqrt(v.x()*v.x() + v.y()*v.y() + v.z()*v.z())
      lat = asin( v.z()/R ) #* 180.0/pi
      lon = atan2( v.y(), v.x() ) #* 180.0/pi
      
      # Lat/lon --> projection
      pj_latlong = pj_init_plus(lonlatProjName)
      pj_merc = pj_init_plus(destProjName)
      x, y = pj_transform(pj_latlong, pj_merc, lon, lat)
      v.setXYZ(x, y, 0)
      
  gvertices = model.bindingsGetVertices()
  for v in gvertices:
    project(v)
  
  gedges = model.bindingsGetEdges()
  for e in gedges:
    project(e)

  gfaces = model.bindingsGetFaces()
  for f in gfaces:
    project(f)

# Project 3D Cartesian mesh onto tangential plane (old method)
def projectMeshPlane(model):
  print ('Projecting mesh in planar space...')
  def project(entity):
    numVertices = entity.getNumMeshVertices()
    m = fullMatrixDouble (3,3)
    for iV in range(0,numVertices): 
      v = entity.getMeshVertex(iV)
      pEphi = v.x()*ePhiX + v.y()*ePhiY + v.z()*ePhiZ
      pEtheta = v.x()*eThetaX + v.y()*eThetaY + v.z()*eThetaZ
      m.set(0, 0, ePhiX)
      m.set(0, 1, ePhiY)
      m.set(0, 2, ePhiZ)
      m.set(1, 0, eThetaX)
      m.set(1, 1, eThetaY)
      m.set(1, 2, eThetaZ)
      m.set(2, 0, -v.x())
      m.set(2, 1, -v.y())
      m.set(2, 2, -v.z())
      m.invertInPlace()
      R = sqrt(v.x()**2 + v.y()**2+ v.z()**2)
      print (R)
      alpha = -Ox*m.get(0,0) - Oy*m.get(1,0) - Oz * m.get(2,0)
      beta = -Ox*m.get(0,1) - Oy*m.get(1,1) - Oz * m.get(2,1)
      v.setXYZ(alpha*R, beta*R, 0)
      
  gvertices = model.bindingsGetVertices()
  for v in gvertices:
    project(v)
  
  gedges = model.bindingsGetEdges()
  for e in gedges:
    project(e)

  gfaces = model.bindingsGetFaces()
  for f in gfaces:
    project(f)

#takes geo lat/lon and outputs x,y,z(sphere)
def geoCoordTo3DCartesian(geoLat, geoLon, pointCart, R):
  #calculate radius at given latitude
  #a = 6378137.0 #Earth SemiMajor
  #b = 6356752.3 #Earth SemiMinor
  #R = sqrt( ((a*a*(cos(geoLat*pi/180.0)))**2 + (b*b*(sin(geoLat*pi/180.0)))**2) / ((a*(cos(geoLat*pi/180.0)))**2 + (b*(sin(geoLat*pi/180.0)))**2) )
  #R = 6.371e6;
  
  #geo coords --> S.P.C.
  pointTheta = 90 - geoLat
  pointPhi = geoLon
  
  #S.P.C. --> Cartesian(on sphere)
  pointCart[0] = R * sin(pointTheta*pi/180.0) * cos(pointPhi*pi/180.0)
  pointCart[1] = R * sin(pointTheta*pi/180.0) * sin(pointPhi*pi/180.0)
  pointCart[2] = R * cos(pointTheta*pi/180.0)

  
#Takes x,y,z(sphere) as input and overwrites with alpha,beta,0(stereo)
def threeDCartesianToStereo(point, R):
  #R = 6.371e6
  alpha = (2.0 * R * point[0]) / (R + point[2])
  beta = (2.0 * R * point[1]) / (R + point[2])
  point[0] = alpha
  point[1] = beta
  point[2] = 0.0
  
#takes x,y,z(sphere) as input and overwrites with x,y,z(plane)
def projectPoint(point):
  m = fullMatrixDouble (3,3)
  m.set(0, 0, ePhiX)
  m.set(0, 1, ePhiY)
  m.set(0, 2, ePhiZ)
  m.set(1, 0, eThetaX)
  m.set(1, 1, eThetaY)
  m.set(1, 2, eThetaZ)
  m.set(2, 0, -point[0])
  m.set(2, 1, -point[1])
  m.set(2, 2, -point[2])
  m.invertInPlace()
  R = sqrt(point[0]**2 + point[1]**2 + point[2]**2)
  alpha = -Ox*m.get(0,0) - Oy*m.get(1,0) - Oz * m.get(2,0)
  beta = -Ox*m.get(0,1) - Oy*m.get(1,1) - Oz * m.get(2,1)
  point[0] = alpha*R
  point[1] = beta*R
  point[2] = 0
  
#Transform a vector in x,y(plane) space into lon,lat space
def xyVector(uvLonLat, x, y, uv_xy):
  xy = fullMatrixDouble(1,2)
  xy.set(0,0,x);
  xy.set(0,1,y);

  #1. Get (u,v) unit vector in x-y space
  uv_norm = sqrt( uv_xy(0,0)*uv_xy(0,0) + uv_xy(0,1)*uv_xy(0,1) );
  if (uv_norm == 0):
    return
  uv_xy_unit = [0.0, 0.0]
  uv_xy_unit[0] = uv_xy(0,0)/uv_norm;
  uv_xy_unit[1] = uv_xy(0,1)/uv_norm;
#  print 'uv_norm is ', uv_norm
  
  #2. Project either end of unit vector onto lonLat space
  lonLat1 = fullMatrixDouble(1,2)
  lonLat2 = fullMatrixDouble(1,2)
  lonLat_fn(lonLat1, xy);
  xy.set(0,0,uv_xy_unit[0]+xy(0,0))
  xy.set(0,1,uv_xy_unit[1]+xy(0,1))
  lonLat_fn(lonLat2, xy);

  uvLonLat.set( 0, 0, lonLat2(0,0)-lonLat1(0,0) );
  uvLonLat.set( 0, 1, lonLat2(0,1)-lonLat1(0,1) );
  uv_unit_proj_norm = sqrt( uvLonLat(0,0)*uvLonLat(0,0) + uvLonLat(0,1)*uvLonLat(0,1) );
  
  #4. Divide projected unit vector by its norm to make it into a unit vector again, then multiply it by norm of original vector
  sol_u = uvLonLat(0,0)*uv_norm/uv_unit_proj_norm;
  sol_v = uvLonLat(0,1)*uv_norm/uv_unit_proj_norm;
  uvLonLat.set( 0, 0, sol_u );
  uvLonLat.set( 0, 1, sol_v );
  uvLonLat.set( 0, 2, 0.0 );

#Transform a vector in alpha,beta(stereo) space into lon,lat space
def stereoVectorToLonLat(uvLonLat, alpha, beta, uv_stereo):
  xy = fullMatrixDouble(1,2)
  xy.set(0,0,alpha);
  xy.set(0,1,beta);

  #1. Get (u,v) unit vector in x-y space
  uv_norm = sqrt( uv_stereo(0,0)*uv_stereo(0,0) + uv_stereo(0,1)*uv_stereo(0,1) );
  if (uv_norm == 0):
    return
  uv_xy_unit = [0.0, 0.0]
  uv_xy_unit[0] = uv_stereo(0,0)/uv_norm;
  uv_xy_unit[1] = uv_stereo(0,1)/uv_norm;
#  print 'uv_norm is ', uv_norm
  
  #2. Project either end of unit vector onto lonLat space
  lonLat1 = fullMatrixDouble(3,3)
  lonLat2 = fullMatrixDouble(3,3)

  stereoToLonLatPoint(lonLat1, xy)

  xy.set(0,0,uv_xy_unit[0]+xy(0,0))
  xy.set(0,1,uv_xy_unit[1]+xy(0,1))
  stereoToLonLatPoint(lonLat2, xy)

  #3. Calculate projected unit vector
  uvLonLat.set( 0, 0, lonLat2(0,0)-lonLat1(0,0) );
  uvLonLat.set( 0, 1, lonLat2(0,1)-lonLat1(0,1) );

  uv_unit_proj_norm = sqrt( uvLonLat(0,0)*uvLonLat(0,0) + uvLonLat(0,1)*uvLonLat(0,1) );

  #4. Divide projected unit vector by its norm to make it into a unit vector again, then multiply it by norm of original vector
  sol_u = uvLonLat(0,0)*uv_norm/uv_unit_proj_norm;
  sol_v = uvLonLat(0,1)*uv_norm/uv_unit_proj_norm;
  uvLonLat.set( 0, 0, sol_u );
  uvLonLat.set( 0, 1, sol_v );
  uvLonLat.set( 0, 2, 0.0 );
  
def stereoToLonLatPoint(lonLat, xyz, R):
  pi = acos(-1.0)
  #R = 6.371e6
  for i in range(0,lonLat.size1()):
    xi = xyz.get(i,0)
    eta = xyz.get(i,1)
    x = 4*R*R*xi/(4*R*R +xi*xi + eta*eta)
    y = 4*R*R*eta/(4*R*R +xi*xi + eta*eta)
    z = R *(4*R*R-xi*xi-eta*eta)/(4*R*R+xi*xi + eta*eta)
    lonLat.set(i, 0, atan2(y,x)*180/pi)
    lonLat.set(i, 1, asin(z/R)*180/pi)
    lonLat.set(i, 2, 0)
  
def lonLat_fn(lonLat, xyz):
  for i in range(0,lonLat.size1()):
    R = 6.371e6
    x = ePhiX * xyz(i,0) + eThetaX * xyz(i,1) + Ox*R
    y = ePhiY * xyz(i,0) + eThetaY * xyz(i,1) + Oy*R
    z = ePhiZ * xyz(i,0) + eThetaZ * xyz(i,1) + Oz*R
    lonLat.set(i, 0, atan2(y,x));
    lonLat.set(i, 1, asin(z/R));
    lonLat.set(i, 2, 0);

# Function to load projected mesh if it exists, else project it by calling projectMeshPROJ
def loadAndProjectMeshUTM(model, filename, projCodeLonLat, projCodeMesh):
  if(not os.path.exists('./Meshes/'+filename+'_utm.msh')):
    Msg.Info("Projected mesh (UTM) doesn't exist. Projecting it.")
    m = GModel()
    m.load("./Meshes/"+filename+".msh")
    projectMeshPROJ(m, projCodeLonLat, projCodeMesh)
    m.save("./Meshes/"+filename+"_utm.msh")
  model.load('./Meshes/'+filename+'_utm.msh')  
