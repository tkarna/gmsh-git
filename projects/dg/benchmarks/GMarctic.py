#!/usr/bin/env python
# -*- coding: utf-8 -*-
from gmshpy import *
from dgpy import *
from sys import stdout
import time, math

meshFile = "circle.msh"  #carre.msh

model = GModel()
model.load(meshFile)
msg = Msg()

order = 1
groups = dgGroupCollection (model, 3, order)
groups.buildGroupsOfInterfaces()

CCode = """
#include "fullMatrix.h"
#include "dgDataCacheMap.h"
#include "math.h"
//static bool init = false;

extern "C" {
	void lonLat (dgDataCacheMap *, fullMatrix<double> &lonlat, fullMatrix<double> &xyz) {
		//if (! init) initialize();
		double x,y;
		for (size_t i = 0; i< lonlat.size1(); i++) {
			//maille carre.msh
			//lonlat.set(i, 0, xyz(i,1));
			//if (xyz(i,0)<0.5){
			  //lonlat.set(i, 1, xyz(i,0)+360);
			//}
			//else {
			  //lonlat.set(i, 1, xyz(i,0));
			//}
			//lonlat.set(i, 2, 0);
			
			//maille circle4.msh
			lonlat.set(i, 0, 90 -(sqrt(xyz(i,1)*xyz(i,1)+(xyz(i,0)*xyz(i,0)))*10)/1000000);
			lonlat.set(i, 1, 180+atan2(xyz(i,1),xyz(i,0))*180/M_PI);
			if (lonlat(i,1)<0.5){
			  lonlat.set(i, 0, 90 -(sqrt(xyz(i,1)*xyz(i,1)+(xyz(i,0))*(xyz(i,0)))*10)/1000000);
			  lonlat.set(i, 1, 180+atan2(xyz(i,1),xyz(i,0))*180/M_PI+359);
			}
			if (lonlat(i,0)>89.5){
			  lonlat.set(i, 0, 89.5 -(sqrt(xyz(i,1)*xyz(i,1)+(xyz(i,0))*(xyz(i,0)))*10)/1000000);
			  lonlat.set(i, 1, 180+atan2(xyz(i,1),xyz(i,0))*180/M_PI);
			}
			lonlat.set(i, 2, xyz(i,2));
		}
	}
  }
"""
#passage du maillage en latlong
tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CCode, tmpLib)
Msg.Barrier()
XYZ = getFunctionCoordinates()
lonLat=functionC(tmpLib,"lonLat",3,[XYZ])
meshlonlat = dgDofContainer (groups, 3)
meshlonlat.interpolate(lonLat)
meshlonlat.exportMsh('ll',0,0)

#lecture des données et stockage
tempDof = dgDofContainer(groups,1)
dz = [0,10,20,30,50,75,100,125,150,200,250,300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500]
tempData = ncDataDz("ocean_temp_PHC_3_0_prepro.nc","ocean_temp","lat","lon",dz)
tempFunc = functionNcDataDz(tempData,lonLat)
tempDof.interpolate(tempFunc)
tempDof.exportMsh('T',0,0)

salDof=dgDofContainer(groups,1)
salData = ncDataDz("ocean_sal_PHC_3_0.nc","ocean_sal","lat","lon",dz)
salFunc = functionNcDataDz(salData,lonLat)
salDof.interpolate(salFunc)
salDof.exportMsh('S',0,0)

#test de l'interpolation avec des donnees constantes par niveau
#tempDof = dgDofContainer(groups,1)
#dz = [0,10,20,30,50,75,100,125,150,200,250,300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500]
#tempData = ncDataDz("dataCstLevel.nc","ocean_temp","lat","lon",dz)
#tempFunc = functionNcDataDz(tempData,lonLat)
#tempDof.interpolate(tempFunc)
#tempDof.exportMsh('T',0,0)

#test sur plan xy
#tempDof = dgDofContainer(groups,1)
#dz = [0,10,20,30,50,75,100,125,150,200,250,300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500]
#tempData = ncDataDz("datalat.nc","ocean_temp","lat","lon",dz)
#tempFunc = functionNcDataDz(tempData,lonLat)
#tempDof.interpolate(tempFunc)
#tempDof.exportMsh('T',0,0)

#autres fonctions
#----------------

#slimstrucData

#tempDof = dgDofContainer(groups,1)
#tempData = slimStructDataContainerNetcdf("ocean_temp_PHC_3_0_prepro.nc","ocean_temp","lat","lon")
#tempInt = slimStructDataInterpolatorMultilinear()
#tempFunc = slimFunctionStructData(tempData, tempInt, lonLat)
#funcCst = functionConstant([0])
#tempFunc.setTimeFunction(funcCst)
#tempDof.interpolate(tempFunc)
#tempDof.exportMsh('T',0,0)

#netcdfStructData

#tempDof = dgDofContainer(groups,1)
#tempData = netcdfContainer("ocean_temp_PHC_3_0_prepro.nc","ocean_temp","lat","lon")
#tempInt = netcdfInterpolatorMultilinear()
#tempFunc = netcdf3dStructData(tempData, tempInt, lonLat)
#funcCst = functionConstant([0])
#tempFunc.setTimeFunction(funcCst)
#tempDof.interpolate(tempFunc)
#tempDof.exportMsh('T',0,0)

#calcul de la pression hydrostatique
#-----------------------------------
pDof = dgDofContainer(groups,1)
def hydrostaticpressure (p,xyz) :
   for i in range(xyz.size1()) :
     z = xyz.get(i,2)
     p.set(i,0,0.1-0.01*z)
p = functionPython (1,hydrostaticpressure,[XYZ])
pDof.interpolate(p)
pDof.exportMsh('P',0,0)

#calcul de la densité
#--------------------
rho = dgDofContainer(groups,1)

#Passage en celsius
temp=tempDof.getFunction()
def kelvincelsius (t,k) :
   for i in range(k.size1()) :
     oldt = k.get(i,0)
     t.set(i,0,oldt-273.16)
t = functionPython (1,kelvincelsius,[temp])
tempDof.interpolate(t)
tempDof.exportMsh('nT',0,0)

sal=salDof.getFunction()
temp=tempDof.getFunction()
press=pDof.getFunction()
rhoFunc = functionStateEquation(sal,temp,press)
rho.interpolate(rhoFunc)
rho.exportMsh('D',0,0)

#passage en densite potentielle
densi = rho.getFunction()
def denspot (dp,rr) :
   for i in range(rr.size1()) :
     oldr = rr.get(i,0)
     dp.set(i,0,oldr-1000)
dp = functionPython (1,denspot,[densi])
rho.interpolate(dp)
rho.exportMsh('nD',0,0)

#--calcul du gradient de la densite
#----------------------------------
gradrhodEq = dgFEGradient(rho.getFunction())
gradrhoSym = gradrhodEq.newSymmetryBoundary('')
gradrhodEq.addBoundaryCondition('surf',gradrhoSym)
gradrhodEq.addBoundaryCondition('bottom',gradrhoSym)
gradrhodEq.addBoundaryCondition('wall',gradrhoSym)

#creation of dofContainer
gradrhodDof = dgDofContainer (groups, 3)
gradrhodDofTemp = dgDofContainer (groups, 3)

#creation of residual
gradrhodResidu = dgResidual(gradrhodEq)
gradrhodResidu.compute(groups,0,gradrhodDofTemp,gradrhodDof)
gradrhodDof.multiplyByInvMassMatrix()

#export
gradrhodDof.exportMsh('gradrhod', 0, 0)

#le rendre continu
gradrhoDof = dgDofContainer (groups, 3)
cgproj=dgL2ProjectionCG(gradrhodDof)
cg = gradrhodDof.getFunction()
cgproj.project(gradrhoDof,cg)

gradrhoDof.exportMsh('gradrho', 0, 0)

#--calcul de la fonction de courant 'eddy transport' par Ferrari 2010
#--------------------------------------------------------------------
cDof=dgDofContainer (groups,1)
def cste( cc, gradz) :
   for i in range(gradz.size1()) :
      gz = gradz.get(i,2)
      val = math.sqrt(-gz*10/1000)*1000/math.pi
      cc.set(i,0,val)
      if (val < 0.1) :
	cc.set(i,0,0.1)
ccgradz = gradrhoDof.getFunction()
cc = functionPython (1,cste,[ccgradz])
cDof.interpolate(cc)

indDof=dgDofContainer (groups,3)
def termR( indR ,gradz) :
   for i in range(gradz.size1()) :
      gx = gradz.get(i,0)
      gy = gradz.get(i,1)
      indR.set(i,0,gx*10/1000)
      indR.set(i,1,gy*10/1000)
      indR.set(i,2,0)
indR = functionPython (3,termR,[ccgradz])
indDof.interpolate(indR)

NDof=dgDofContainer (groups,1)
def freq(  freqN , gradz):
   for i in range(gradz.size1()) :
      gz = gradz.get(i,2)
      freqN.set(i,0,-gz*10/1000)
freqN= functionPython (1,freq,[ccgradz])
NDof.interpolate(freqN)

#--implicit resolution
#---------------------
constant = cDof.getFunction()
independant = indDof.getFunction()
frequence = NDof.getFunction()
psiEq= dgEddyTransportFlux(constant,independant,frequence)
psiEqBSBnd = functionConstant([0,0,0])
psiEq.addStrongBoundaryCondition(2,"surf",psiEqBSBnd)
psiEq.addStrongBoundaryCondition(2,"bottom",psiEqBSBnd)
psiEq.addStrongBoundaryCondition(2,"wall",psiEqBSBnd)
psiEqZeroBnd = psiEq.new0FluxBoundary()
psiEq.addBoundaryCondition('surf',psiEqZeroBnd)
psiEq.addBoundaryCondition('bottom',psiEqZeroBnd)
psiEq.addBoundaryCondition('wall',psiEqZeroBnd)


fieldspsi = psiEq.getNbFields()
psiDof = dgDofContainer(groups,fieldspsi)

residual = dgDofContainer(groups,fieldspsi)
sys =  linearSystemPETScDouble()
sys.setParameter("petscOption", "-pc_type lu")
dof = dgDofManager.newCG (groups,3,sys)

sol = dgSteady(psiEq,dof)
#sol.setRtol(1e-9)
sol.solve(psiDof)
psiDof.exportMsh('psi',0,0)

#exit(0)
##le rendre continu
psicDof = dgDofContainer (groups, 3)
cgproj=dgL2ProjectionCG(psiDof)
cg = psiDof.getFunction()
cgproj.project(psicDof,cg)

psicDof.exportMsh('psic', 0, 0)

##--Creation de la vitesse maintenant
##----------------------------------
uGMDof = dgDofContainer(groups,3)
gradPDof = dgDofContainer(groups,9)
psigrad = psicDof.getFunctionGradient()
gradPDof.interpolate(psigrad)
gradPDof.exportMsh('gradp', 0, 0)


def velocityGM (h,gradpsi) :
   for i in range(gradpsi.size1()) :
     gradXx = gradpsi.get(i,0)
     gradXz = gradpsi.get(i,2)
     gradYy = gradpsi.get(i,4)
     gradYz = gradpsi.get(i,5)
     h.set(i,0,gradXz)
     h.set(i,1,gradYz)
     h.set(i,2,(-gradXx-gradYy))
gradslope = gradPDof.getFunction()
h = functionPython (3,velocityGM,[gradslope])
uGMDof.interpolate(h)

#export
uGMDof.exportMsh('uGM', 0, 0)

uprefix= 'U'
velocity = uGMDof.getFunction()
streamu=functionExtractCompNew(velocity,0)
streamv=functionExtractCompNew(velocity,1)
streamw=functionExtractCompNew(velocity,2)
#streamu=functionExtractCompNew(uGMDof.getFunction(),0)
#streamv=functionExtractCompNew(uGMDof.getFunction(),1)
#streamw=functionExtractCompNew(uGMDof.getFunction(),2)
streamuvw=functionCatCompNew([streamu,streamv,streamw])
#streamuvw=functionCatCompNew([functionExtractCompNew(uGMDof.getFunction(),0),functionExtractCompNew(uGMDof.getFunction(),1),functionExtractCompNew(uGMDof.getFunction(),2)])
uGMDof.exportFunctionMsh(streamuvw,uprefix, 0, 0,'U')

#Test de consistence
#-------------------
#traceur constant
trac = dgDofContainer(groups, 1)
#constfunc2 = functionConstant([2])
#trac.interpolate(constfunc2)

#traceur lineaire
def traceur( tr, xz) :
  for i in range(xz.size1()) :
      x = xz.get(i,0)
      tr.set(i,0,0.01*x)
tr = functionPython (1,traceur,[XYZ])
trac.interpolate(tr)

#equation du traceur
#streamuv = functionCatCompNew([streamu,streamv])
streamuv = functionCatCompNew([functionExtractCompNew(uGMDof.getFunction(),0),functionExtractCompNew(uGMDof.getFunction(),1)])
tracEq = dgConservationLawSW3dTracer(streamuv,streamw)
constfunc1 = functionConstant([1])
tracEq.setBath(constfunc1)
#conditions frontières pr traceur constant
#tracBnCst = tracEq.newInFluxBoundary(constfunc2)
#tracEq.addBoundaryCondition('surf',tracBnCst)
#tracEq.addBoundaryCondition('bottom',tracBnCst)
#tracEq.addBoundaryCondition('wall',tracBnCst)
#conditions frontières pr traceur lineaire
traclin = trac.getFunction()
tracBnlin = tracEq.newInFluxBoundary(traclin)
tracEq.addBoundaryCondition('surf',tracBnlin)
tracEq.addBoundaryCondition('bottom',tracBnlin)
tracEq.addBoundaryCondition('wall',tracBnlin)

#Runge-Kutta run
rk=dgRungeKutta()

dt = 1000 #p1: rk2: 60 rk3: 90 (75) rk4: 85

_time = 0
export_all_the_time = False
#export_all_the_time = true
export_interval = 100000#750 --2*3600 --1e6
end_time = 1000000 #7*24*3600 --20*24*3600 -- 150000
odir = 'GentMc/'
tracPrefix = 'tr'

#main loop
matrix=fullMatrixDouble(1,1)
tracfunc = trac.getFunction()
inte=dgFunctionIntegrator(tracfunc)
result=dgDofContainer(groups,1)
inte.compute(result,matrix)
next_export_time = export_interval
export_count = 0
normtr = trac.norm()
iter = 0
t0 = 0
t1 = 0
t2 = 0

def export() :
    global t1,_time,export_count,iter,normtr,dt,sol
    t2 = time.clock()
    cpuTime = t2-t1
    t1 = t2
    if Msg.GetCommRank()==0 :
      if iter == 0 :
	sol = matrix.get(0,0)
        stdout.write('Export iter   t           nT       Integral     dt   Tcpu\n')
      stdout.write("%3d %4d %6.2f %.16e %.16e %3.3f  %.2f\n" % (export_count,iter,_time,normtr,sol,dt,cpuTime) )
    istr = "_%05d" % (export_count)
    trac.exportMsh(odir + tracPrefix + istr, _time , export_count)
    
export()

#for RK

trac_old = dgDofContainer (groups, 1)
trac_new= dgDofContainer (groups, 1)
trac_K= dgDofContainer (groups, 1)
trac_resd= dgDofContainer (groups, 1)


tracResidu = dgResidual(tracEq)

t0 = time.clock()
t1 = t0
for ii in range (100000):
  iter = iter + 1
  #diagonal Runge-Kutta iteration --
   #a = {0} 
   #b = {1}
   #a = {0,1} 
   #b = {0.5,0.5}
  a = [0, 1./3, 2./3]
  b = [1./4,0, 3./4]
  #a = {0,0.5,0.5,1} 
  #b = {1./6, 1./3, 1./3, 1./6}
  trac_old.copy(trac)
  trac_new.copy(trac)
  trac_K.copy(trac)
  for iRK in range (len(a)): 
    #compute solutions at fractional time step
    if iRK>1 :
      trac.copy(trac_old)
      trac.axpy(trac_K,a[iRK]*dt)
    #compute residuals
    tracResidu.compute(groups,_time+a[iRK]*dt,trac,trac_resd)
    #update K
    trac_resd.multiplyByInvMassMatrix()
    trac_K.copy(trac_resd)
    #update new solutions
    trac_new.axpy(trac_K,b[iRK]*dt)
  # update final solutions
  trac.copy(trac_new)
  normtr = trac.norm()
  tracfunc = trac.getFunction()
  inte=dgFunctionIntegrator(tracfunc)
  result=dgDofContainer(groups,1)
  inte.compute(result,matrix)

  _time = _time +dt
  if (export_all_the_time or _time >= next_export_time or (not (normtr<1e10))):
     export_count = export_count + 1;
     next_export_time = next_export_time + export_interval
     export()
  if _time >= end_time:
    break
  if not (normtr<1e10) :
    stdout.write('Exploded?\n')
    export_count = export_count + 1;
    export()
    break

t2 = time.clock()
print('in main loop. ' + str(t2-t0))