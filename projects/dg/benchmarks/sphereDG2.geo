lc = .5;
r = 1;
Point(1) = {0.0,0.0,0.0,lc};
Point(2) = {r,0.0,0.0,lc};
Point(3) = {0,r,0.0,lc};
Circle(1) = {2,1,3};
Point(4) = {-r,0,0.0,lc};
Point(5) = {0,-r,0.0,lc};
Circle(2) = {3,1,4};
Circle(3) = {4,1,5};
Circle(4) = {5,1,2};
Point(6) = {0,0,-r,lc};
Point(7) = {0,0,r,lc};
Circle(5) = {3,1,6};
Circle(6) = {6,1,5};
Circle(7) = {5,1,7};
Circle(8) = {7,1,3};
Circle(9) = {2,1,7};
Circle(10) = {7,1,4};
Circle(11) = {4,1,6};
Circle(12) = {6,1,2};
Line Loop(13) = {2,8,-10};
Ruled Surface(14) = {13};
Line Loop(15) = {10,3,7};
Ruled Surface(16) = {15};
Line Loop(17) = {-8,-9,1};
Ruled Surface(18) = {17};
Line Loop(19) = {-11,-2,5};
Ruled Surface(20) = {19};
Line Loop(21) = {-5,-12,-1};
Ruled Surface(22) = {21};
Line Loop(23) = {-3,11,6};
Ruled Surface(24) = {23};
Line Loop(25) = {-7,4,9};
Ruled Surface(26) = {25};
Line Loop(27) = {-4,12,-6};
Ruled Surface(28) = {27};
Surface Loop(29) = {28,26,16,14,20,24,22,18};
Volume(30) = {29};

lc2 = 1.5*5;
R = 15;
Point(8) =  {-R, R,-R,lc2};
Point(9) =  { R, R,-R,lc2};
Point(10) = {-R,-R,-R,lc2};
Point(11) = { R,-R,-R,lc2};
Line(31) = {8,9};
Line(32) = {9,11};
Line(33) = {11,10};
Line(34) = {10,8};
Line Loop(35) = {31,32,33,34};
Plane Surface(36) = {35};
Extrude {0,0,3*R} {
  Surface{36};
}
Surface Loop(59) = {49,36,45,58,53,57};
Volume(60) = {59,29};
Delete {
  Volume{30};
}

Y = 6.*r;
Field[1] = Box;
Field[1].VIn = 3*lc;
Field[1].VOut = lc2;
Field[1].XMax = Y;
Field[1].XMin = -Y;
Field[1].YMax = Y;
Field[1].YMin = -Y;
Field[1].ZMax = 3*Y;
Field[1].ZMin = -Y;
Y2 = 3.*r;
Field[2] = Box;
Field[2].VIn = lc;
Field[2].VOut = lc2;
Field[2].XMax = Y2;
Field[2].XMin = -Y2;
Field[2].YMax = Y2;
Field[2].YMin = -Y2;
Field[2].ZMax = 2*Y2;
Field[2].ZMin = -Y2;

Field[3] = Min;
Field[3].FieldsList = {1,2};

Background Field = 3;
Physical Surface("Box") = {45, 49, 58, 53, 57, 36};
Physical Surface("Wall") = {14, 16, 18, 20, 24, 26, 22, 28};
Physical Volume("Fluid") = {60};
