#!/usr/bin/env python
# -*- coding: utf-8 -*-
from gmshpy import *
from dgpy import *
from sys import stdout
from scipy import *			#ADDED FOR ???
from scipy.sparse import *		#ADDED FOR CSR
from matplotlib.pyplot import *		#ADDED FOR PLOT
from numpy.linalg import *		#ADDED FOR EIG
import time, math

meshFile = "circle200fineH.msh"  #carre.msh

model = GModel()
model.load(meshFile)
msg = Msg()

order = 1
groups = dgGroupCollection (model, 3, order)
groups.buildGroupsOfInterfaces()

CCode = """
#include "fullMatrix.h"
#include "dgDataCacheMap.h"
#include "math.h"
//static bool init = false;

extern "C" {
	void lonLat (dgDataCacheMap *, fullMatrix<double> &lonlat, fullMatrix<double> &xyz) {
		//if (! init) initialize();
		double x,y;
		for (size_t i = 0; i< lonlat.size1(); i++) {
			//maille carre.msh
			//lonlat.set(i, 0, xyz(i,1));
			//if (xyz(i,0)<0.5){
			  //lonlat.set(i, 1, xyz(i,0)+360);
			//}
			//else {
			  //lonlat.set(i, 1, xyz(i,0));
			//}
			//lonlat.set(i, 2, 0);
			
			//maille circle4.msh
			lonlat.set(i, 0, 90 -(sqrt(xyz(i,1)*xyz(i,1)+(xyz(i,0)*xyz(i,0)))*10)/1000000);
			lonlat.set(i, 1, 180+atan2(xyz(i,1),xyz(i,0))*180/M_PI);
			if (lonlat(i,1)<0.5){
			  lonlat.set(i, 0, 90 -(sqrt(xyz(i,1)*xyz(i,1)+(xyz(i,0))*(xyz(i,0)))*10)/1000000);
			  lonlat.set(i, 1, 180+atan2(xyz(i,1),xyz(i,0))*180/M_PI+359);
			}
			if (lonlat(i,0)>89.5){
			  lonlat.set(i, 0, 89.5 -(sqrt(xyz(i,1)*xyz(i,1)+(xyz(i,0))*(xyz(i,0)))*10)/1000000);
			  lonlat.set(i, 1, 180+atan2(xyz(i,1),xyz(i,0))*180/M_PI);
			}
			lonlat.set(i, 2, xyz(i,2));
		}
	}
	void AlphaBeta (dataCacheMap *, fullMatrix<double> &alphaBeta,fullMatrix<double> &S,fullMatrix<double> &Th,fullMatrix<double> &P) {
		for(int i=0; i< S.size1(); i++) {
			double a[15]={0.665157e-1,0.170907e-1,-0.203814e-3,0.298357e-5,-0.255019e-7,0.27811e-2,-0.84696e-4,-0.164759e-6,-0.25152e-6,-0.678662e-5,0.380374e-4,-0.933746e-6,0.791325e-8,0.512857e-12,-0.302285e-13};
			alphaBeta.set(i,0,a[0]+Th(i,0)*a[1]+Th(i,0)*Th(i,0)*a[2]+Th(i,0)*Th(i,0)*Th(i,0)*a[3]+Th(i,0)*Th(i,0)*Th(i,0)*Th(i,0)*a[4]+(S(i,0)-35)*(a[5]+Th(i,0)*a[6]+P(i,0)*a[7]+P(i,0)*P(i,0)*a[8])+(S(i,0)-35)*(S(i,0)-35)*a[9]+P(i,0)*(a[10]+Th(i,0)*a[11]+Th(i,0)*Th(i,0)*a[12])+P(i,0)*P(i,0)*Th(i,0)*Th(i,0)*a[13]+P(i,0)*P(i,0)*P(i,0)*a[14]);
			double b[15]={0.785567e-3,-0.301985e-5,0.555579e-7,-0.415613e-9,-0.356603e-6,0.788212e-8,-0.408195e-10,-0.6002281e-15,0.515032e-8,-0.121555e-7,0.192867e-9,-0.213127e-11,0.176621e-12,-0.175379e-14,0.121551e-17};
			alphaBeta.set(i,1,b[0]+Th(i,0)*b[1]+Th(i,0)*Th(i,0)*b[2]+Th(i,0)*Th(i,0)*Th(i,0)*b[3]+(S(i,0)-35)*(b[4]+Th(i,0)*b[5]+P(i,0)*b[6]+P(i,0)*P(i,0)*b[7])+(S(i,0)-35)*(S(i,0)-35)*b[8]+P(i,0)*(b[9]+Th(i,0)*b[10]+Th(i,0)*Th(i,0)*b[11])+P(i,0)*P(i,0)*(b[12]+Th(i,0)*b[13])+P(i,0)*P(i,0)*P(i,0)*b[14]);
		}
	}
  }
"""

#passage du maillage en latlong
tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CCode, tmpLib)
Msg.Barrier()
XYZ = getFunctionCoordinates()
lonLat=functionC(tmpLib,"lonLat",3,[XYZ])
meshlonlat = dgDofContainer (groups, 3)
meshlonlat.interpolate(lonLat)
meshlonlat.exportMsh('ll',0,0)

#lecture des données et stockage
tempDof = dgDofContainer(groups,1)
dz = [0,10,20,30,50,75,100,125,150,200,250,300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500]
tempData = ncDataDz("ocean_temp_PHC_3_0_prepro.nc","ocean_temp","lat","lon",dz)
tempFunc = functionNcDataDz(tempData,lonLat)
tempDof.interpolate(tempFunc)
tempDof.exportMsh('T',0,0)

#Passage en celsius
temp=tempDof.getFunction()
def kelvincelsius (t,k) :
   for i in range(k.size1()) :
     oldt = k.get(i,0)
     t.set(i,0,oldt-273.16)
t = functionPython (1,kelvincelsius,[temp])
tempDof.interpolate(t)
tempDof.exportMsh('nT',0,0)

salDof=dgDofContainer(groups,1)
salData = ncDataDz("ocean_sal_PHC_3_0.nc","ocean_sal","lat","lon",dz)
salFunc = functionNcDataDz(salData,lonLat)
salDof.interpolate(salFunc)
salDof.exportMsh('S',0,0)

#calcul de la pression hydrostatique
#-----------------------------------
pDof = dgDofContainer(groups,1)
def hydrostaticpressure (p,xyz) :
   for i in range(xyz.size1()) :
     z = xyz.get(i,2)
     p.set(i,0,0.1-0.01*z)
p = functionPython (1,hydrostaticpressure,[XYZ])
pDof.interpolate(p)
pDof.exportMsh('P',0,0)

#calcul de la densité
#--------------------
rho = dgDofContainer(groups,1)
sal=salDof.getFunction()
temp=tempDof.getFunction()
press=pDof.getFunction()
rhoFunc = functionStateEquation(sal,temp,press)
rho.interpolate(rhoFunc)
rho.exportMsh('D',0,0)


#--calcul du gradient de la densite
#----------------------------------
gradrhodEq = dgFEGradient(rho.getFunction())
gradrhoSym = gradrhodEq.newSymmetryBoundary('')
gradrhodEq.addBoundaryCondition('surf',gradrhoSym)
gradrhodEq.addBoundaryCondition('bottom',gradrhoSym)
gradrhodEq.addBoundaryCondition('wall',gradrhoSym)

#creation of dofContainer
gradrhodDof = dgDofContainer (groups, 3)
gradrhodDofTemp = dgDofContainer (groups, 3)

#creation of residual
gradrhodResidu = dgResidual(gradrhodEq)
gradrhodResidu.compute(groups,0,gradrhodDofTemp,gradrhodDof)
gradrhodDof.multiplyByInvMassMatrix()

#export
gradrhodDof.exportMsh('gradrhod', 0, 0)

#le rendre continu
gradrhoDof = dgDofContainer (groups, 3)
cgproj=dgL2ProjectionCG(gradrhodDof)
cg = gradrhodDof.getFunction()
cgproj.project(gradrhoDof,cg)
gradrhoDof.exportMsh('gradrho', 0, 0)

#rendre la densite stable
#-------------------------

#frequence de Brunt-vaisala
NDof=dgDofContainer(groups,1)
def freq(freqN , gradz):
   for i in range(gradz.size1()) :
      gz = gradz.get(i,2)
      freqN.set(i,0,-gz*10/1000)
freqN= functionPython (1,freq,[cg])
NDof.interpolate(freqN)
NDof.exportMsh('freq',0,0)

#calcul de la diffusion
N2 = NDof.getFunction()
nuDof = dgDofContainer(groups,1)
def nuFunction(nu,freq):
  for i in range(freq.size1()):
    f = freq.get(i,0)
    nu.set(i,0,(1-math.tanh(10*f)))
nu = functionPython(1,nuFunction,[N2])
nuDof.interpolate(nu)
nuDof.exportMsh('nu',0,0)

#diffusion
n = nuDof.getFunction()
rhoEq = dgConservationLawAdvectionDiffusion.diffusionLaw(n)
rhoBnlin = rhoEq.new0FluxBoundary()
rhoEq.addBoundaryCondition('surf',rhoBnlin)
rhoEq.addBoundaryCondition('bottom',rhoBnlin)
rhoEq.addBoundaryCondition('wall',rhoBnlin)
residual = dgDofContainer(groups, rhoEq.getNbFields())
sys = linearSystemPETScBlockDouble ()
sys.setParameter("petscOption", "-pc_type lu -ksp_monitor")
dof = dgDofManager.newDGBlock (groups, 1, sys)
implicitRK2 = dgDIRK(rhoEq, dof,1)
implicitRK2.getNewton().setRtol(1e-6)
implicitRK2.getNewton().setVerb(10)
export_interval = 10000
next_export_time = export_interval
dt = 10000
t = 0.0
for i in range (1,2) :
  t = t+dt
  implicitRK2.iterate(rho, dt, t)
  if ( t >= next_export_time):
     next_export_time = next_export_time + export_interval
     print('|ITER|',i,'|DT|',dt, '|t|', t)
     rho.exportMsh("Rho-%06d" % (i), i*dt, i)

gradrhodEq = dgFEGradient(rho.getFunction())
gradrhoSym = gradrhodEq.newSymmetryBoundary('')
gradrhodEq.addBoundaryCondition('surf',gradrhoSym)
gradrhodEq.addBoundaryCondition('bottom',gradrhoSym)
gradrhodEq.addBoundaryCondition('wall',gradrhoSym)

#creation of residual
gradrhodResidu = dgResidual(gradrhodEq)
gradrhodResidu.compute(groups,0,gradrhodDofTemp,gradrhodDof)
gradrhodDof.multiplyByInvMassMatrix()

#export
gradrhodDof.exportMsh('gradrhod', 0, 0)

#le rendre continu
cgproj=dgL2ProjectionCG(gradrhodDof)
cg = gradrhodDof.getFunction()
cgproj.project(gradrhoDof,cg)
gradrhoDof.exportMsh('gradrho', 0, 0)

#frequence de Brunt-vaisala
NDof=dgDofContainer(groups,1)
def freq(freqN , gradz):
   for i in range(gradz.size1()) :
      gz = gradz.get(i,2)
      freqN.set(i,0,-gz*10/1000)
freqN= functionPython (1,freq,[cg])
NDof.interpolate(freqN)
NDof.exportMsh('freq2',0,0)

#calcul de la pente
#------------------
SlopedDof = dgDofContainer(groups,2)
def slopefunction (sl,grxyz) :
   for i in range(grxyz.size1()) :
     grx = grxyz.get(i,0)
     gry = grxyz.get(i,1)
     grz = grxyz.get(i,2)
     if (grz == 0):
       Sx = 0
       Sy = 0
     else :
       Sx = -grx/grz
       Sy = -gry/grz
     Slim = 0.01
     if (Sx > Slim):
       Sx = Slim
     if (Sx < -Slim):
       Sx = -Slim
     if (Sy > Slim):
       Sy = Slim
     if (Sy < -Slim):
       Sy = -Slim
     sl.set(i,0,Sx)
     sl.set(i,1,Sy)
grad = gradrhoDof.getFunction()
sl = functionPython (2,slopefunction,[grad])
SlopedDof.interpolate(sl)
SlopedDof.exportMsh('slope', 0, 0)

SlopeDof = dgDofContainer (groups, 2)
cgproj=dgL2ProjectionCG(SlopedDof)
cg = SlopedDof.getFunction()
cgproj.project(SlopeDof,cg)
SlopeDof.exportMsh('slopeC', 0, 0)

#traceur et diffusion isopycnale
#-------------------------------
#traceur constant
trac = dgDofContainer(groups, 1)
def traceur( tr, xz) :
  for i in range(xz.size1()) :
      x = xz.get(i,0)
      y = xz.get(i,1)
      z = xz.get(i,2)
      val = 0
      if ((z<-50) and (z>-100)) :
         val = math.exp(-((y*y)+(x*x))/(2*10000000000))
      tr.set(i,0,val)
tr = functionPython (1,traceur,[XYZ])
trac.interpolate(tr)
trac.exportMsh('TR', 0, 0)


#vitesse
UDof = dgDofContainer(groups, 3)
def velocity( vel, xz) :
  for i in range(xz.size1()) :
      vel.set(i,0,0)
      vel.set(i,1,0)
      vel.set(i,2,0)
vel = functionPython (3,velocity,[XYZ])
UDof.interpolate(vel)

#equation du traceur
u = functionExtractCompNew(UDof.getFunction(),0)
v = functionExtractCompNew(UDof.getFunction(),1)
streamuv = functionCatCompNew([u,v])
streamw = functionExtractCompNew(UDof.getFunction(),2)
tracEq = dgConservationLawSW3dTracer(streamuv,streamw)
constfunc1 = functionConstant([1])
tracEq.setBath(constfunc1)
slo = SlopeDof.getFunction()
diff = functionConstant([1000])
#approximation small slope
tracEq.setIsoDiff(slo,diff)
#full matrix
#tracEq.setIsoDiffFull(slo,diff)
tracBnlin = tracEq.new0FluxBoundary()
tracEq.addBoundaryCondition('surf',tracBnlin)
tracEq.addBoundaryCondition('bottom',tracBnlin)
tracEq.addBoundaryCondition('wall',tracBnlin)

#implicit
#-------
residual = dgDofContainer(groups, tracEq.getNbFields())
sys = linearSystemPETScBlockDouble () #Block
sys.setParameter("petscOption", "-pc_type lu -ksp_monitor")
dof = dgDofManager.newDGBlock(groups, 1, sys) #Block
implicitRK2 = dgDIRK(tracEq, dof,1)
implicitRK2.getNewton().setRtol(1e-6)
implicitRK2.getNewton().setAtol(1e-11)
implicitRK2.getNewton().setVerb(10)

export_interval = 1000000
next_export_time = export_interval
dt = 1000000
t = 0.0
for i in range (1,30) :
  t = t+dt
  implicitRK2.iterate(trac, dt, t)
  #implicitRK2.getNewton().assembleSystem(0.0,None,0.0)
  #rows = sys.getRowPointers()
  #n = len(rows)-1
  #A = csr_matrix( (array(sys.getData()),array(sys.getColumnsIndices()),array(rows)), shape=(n,n) )
  #s,w = eig(A.todense())
  #if (min(s) < -0.000001):
    #figure()
    #plot(real(s),imag(s),"x")
    #draw()
    #show()
    #print "done :o)"
    #value = raw_input()
  if ( t >= next_export_time):
     next_export_time = next_export_time + export_interval
     print('|ITER|',i,'|DT|',dt, '|t|', t)
     trac.exportMsh("GentMc/TR-%06d" % (i), i*dt, i)
     
exit(0)