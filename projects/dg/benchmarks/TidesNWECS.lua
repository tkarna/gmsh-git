-- Include colored prints
dofile("coloredPrints.lua")
outputDir = string.format("NWECS")

function projectMesh (model)
  function project(entity)
    local numVertices = entity:getNumMeshVertices()
    local m = fullMatrix (3,3)
    for iV = 0,numVertices-1 do
      local v = entity:getMeshVertex(iV)
      local pEphi = v:x()*ePhiX + v:y()*ePhiY + v:z()*ePhiZ
      local pEtheta = v:x()*eThetaX + v:y()*eThetaY + v:z()*eThetaZ
      m:set(0, 0, ePhiX)
      m:set(0, 1, ePhiY)
      m:set(0, 2, ePhiZ)
      m:set(1, 0, eThetaX)
      m:set(1, 1, eThetaY)
      m:set(1, 2, eThetaZ)
      m:set(2, 0, -v:x())
      m:set(2, 1, -v:y())
      m:set(2, 2, -v:z())
      m:invertInPlace()
      R = math.sqrt(v:x()^2 + v:y()^2+ v:z()^2)
      local alpha = -Ox*m:get(0,0) - Oy*m:get(1,0) - Oz * m:get(2,0)
      local beta = -Ox*m:get(0,1) - Oy*m:get(1,1) - Oz * m:get(2,1)
      v:setXYZ(alpha*R, beta*R, 0)
      --local pEphi = v:x()*ePhiX + v:y()*ePhiY + v:z()*ePhiZ
      --local pEtheta = v:x()*eThetaX + v:y()*eThetaY + v:z()*eThetaZ
      --v:setXYZ(pEphi, pEtheta, 0)
    end
  end

  local gvertices = model:getVertices()
  for i,v in ipairs(gvertices) do
    project(v)
  end
  local gedges = model:getEdges()
  for i,e in ipairs(gedges) do
    project(e)
  end
  local gfaces = model:getFaces()
  for i,f in ipairs(gfaces) do
    project(f)
  end
end

theta = 54.5/180*math.pi
phi = 0.45/180*math.pi
Ox = math.cos(theta)*math.cos(phi)
Oy = math.cos(theta)*math.sin(phi)
Oz = math.sin(theta)
eThetaX = -math.sin(theta)*math.cos(phi)
eThetaY = -math.sin(theta)*math.sin(phi)
eThetaZ = math.cos(theta)
ePhiX = -math.sin(phi)
ePhiY = math.cos(phi)
ePhiZ = 0

m = GModel()
m:load("NWECS_1.msh")
projectMesh(m)
m:save("NWECS_1_tan.msh")
m:save("NWECS_1_tan.msh")
m:delete()

model = GModel()
model:load("NWECS_1_tan.msh")
order = 1
dimension = 2

CCode =[[
#include "fullMatrix.h"
#include "dgDataCacheMap.h"
#include "math.h"
static double pOx, pOy, pOz, pPhiX, pPhiY, pPhiZ, pThetaX, pThetaY, pThetaZ;
static bool init = false;

static void initialize() {
  double theta = 54.5/180*M_PI;
  double phi = 0.45/180*M_PI;
  double R = 6.37101e6;
  pOx = cos(theta)*cos(phi)*R;
  pOy = cos(theta)*sin(phi)*R;
  pOz = sin(theta)*R;
  pPhiX = -sin(phi);
  pPhiY = cos(phi);
  pPhiZ = 0;
  pThetaX = -sin(theta)*cos(phi);
  pThetaY = -sin(theta)*sin(phi);
  pThetaZ = cos(theta);
  init = true;
}
extern "C" {
	void lonLat (dgDataCacheMap *, fullMatrix<double> &lonlat, fullMatrix<double> &xyz) {
		if (! init) initialize();
		for (size_t i = 0; i< lonlat.size1(); i++) {
			double x = pPhiX * xyz(i,0) + pThetaX * xyz(i,1) + pOx;
			double y = pPhiY * xyz(i,0) + pThetaY * xyz(i,1) + pOy;
			double z = pPhiZ * xyz(i,0) + pThetaZ * xyz(i,1) + pOz;
			double r = sqrt(x*x+y*y+z*z);
			lonlat.set(i, 0, atan2(y,x));
			lonlat.set(i, 1, asin(z/r));
			lonlat.set(i, 2, 0);
		}
	}
	void lonLatDegrees (dgDataCacheMap *, fullMatrix<double> &lonlat, fullMatrix<double> &xyz) {
		if (! init) initialize();
		for (size_t i = 0; i< lonlat.size1(); i++) {
			double x = pPhiX * xyz(i,0) + pThetaX * xyz(i,1) + pOx;
			double y = pPhiY * xyz(i,0) + pThetaY * xyz(i,1) + pOy;
			double z = pPhiZ * xyz(i,0) + pThetaZ * xyz(i,1) + pOz;
			double r = sqrt(x*x+y*y+z*z);
			lonlat.set(i, 0, atan2(y,x)*180/M_PI);
			lonlat.set(i, 1, asin(z/r)*180/M_PI);
			lonlat.set(i, 2, 0);
		}
	}
	void lonLatVector(dgDataCacheMap *, fullMatrix<double> &uv, fullMatrix<double> &lonlat, fullMatrix<double> &u, fullMatrix<double> &v){
		for (size_t i = 0; i< lonlat.size1(); i++) {
			double lon=lonlat(i,0);
			double lat=lonlat(i,1);
			double uLat=u(i,0);
			double uLon=v(i,0);
			uv.set(i, 0,  uLon);
			uv.set(i, 1, -uLat);
			uv.set(i, 2, 0);
			//uv.set(i, 0, -uLat*sin(lat)*cos(lon)-uLon*cos(lat)*sin(lon));
			//uv.set(i, 1, -uLat*sin(lat)*sin(lon)+uLon*cos(lat)*cos(lon));
			//uv.set(i, 2,  0*uLat*cos(lat));
		}
  }
 void vectorFromComponents(dgDataCacheMap *, fullMatrix<double> &v, fullMatrix<double> &v1, fullMatrix<double> &v2){
    for (size_t i = 0; i< v.size1(); i++){
      v.set(i,0,v1(i,0));
      v.set(i,1,v2(i,0)); 
    }
  }
 void coriolis(dgDataCacheMap *,fullMatrix<double> &factor, fullMatrix<double> &lonlat){
	  double omega = 2*M_PI/86400;
    for (size_t i = 0; i< lonlat.size1(); i++) {
      factor.set(i,0,2*omega*sin(lonlat(i,1))); 
    }
  }
	void transport2velocity(dgDataCacheMap *,fullMatrix<double> &uv, fullMatrix<double> &UV, fullMatrix<double> &bath, fullMatrix<double> &solution){
    for (size_t i = 0; i< solution.size1(); i++) {
      uv.set(i,0,UV(i,0)/(bath(i,0)+solution(i,0))); 
      uv.set(i,1,UV(i,1)/(bath(i,0)+solution(i,0))); 
      uv.set(i,2,UV(i,2)/(bath(i,0)+solution(i,0))); 
    }
	}
	void getComp(dgDataCacheMap *,fullMatrix<double> &u,fullMatrix<double> &uv, int comp){
    for (size_t i = 0; i< u.size1(); i++) {
      uv.set(i,0,uv(i,comp)); 
    }
	}
	void merge(dgDataCacheMap *,fullMatrix<double> &sol,fullMatrix<double> &eta, fullMatrix<double> &uv){
    for (size_t i = 0; i< sol.size1(); i++) {
      sol.set(i,0,eta(i,0)); 
      sol.set(i,1,uv(i,0)); 
      sol.set(i,2,uv(i,1)); 
    }
	}
	void inverseSign(dgDataCacheMap *,fullMatrix<double> &u,fullMatrix<double> &v){
    for (size_t i = 0; i< u.size1(); i++) {
      u.set(i,0,-v(i,0)); 
    }
	}
	void smoothBath(dgDataCacheMap *,fullMatrix<double> &bathIn,fullMatrix<double> &bathOut){
    for (size_t i = 0; i< bathIn.size1(); i++) {
			double b=bathIn(i,0);
    	if (b<0) b=5;
    	else if (b<100) b = b + 5*exp(-b/5.0);
    	else b = b - 10*exp((b-210.)/10.);
			bathOut.set(i,0,b); 
    }
	}
  void bottomDrag(dgDataCacheMap *, fullMatrix<double> &val, fullMatrix<double> &sol, fullMatrix<double> &bath){
    for(int i=0;  i<val.size1(); i++){
      double H = sol(i, 0)+bath(i, 0);
      val.set(i, 0, -9.81*0.0235*0.0235/(pow(H, 1.333333333333)));
    }
  }
  void current (dgDataCacheMap *, fullMatrix<double> &current, fullMatrix<double> &solution) {
    for (size_t i = 0; i< current.size1(); i++) {
      current.set(i, 0, solution(i,1));
      current.set(i, 1, solution(i,2));
      current.set(i, 2, 0);
    }
  }
}
]]

print '.... Compiling Libraries ....'
tmpLib = "tmp.dylib"
if (Msg.getCommRank() == 0 ) then
  functionC.buildLibrary (CCode, tmpLib);
end
Msg.barrier()


-- time
initialTime=os.time{year=2000, month=1, day=1, hour=0, sec=0}
time=initialTime
function timeF( FCT ) 
  for i=0,FCT:size1()-1 do
    FCT:set(i,0,time); 
  end
end
endTime=os.time{year=2000, month=2, day=1, hour=0, sec=0}
timeFunction = functionLua(1, 'timeF', {} )
function tide (f)
  for i=0,f:size1()-1 do
    f:set(i, 0, math.sin((time-initialTime)*2*math.pi/3600/12))
    f:set(i, 1, 0)
    f:set(i, 2, 0)
  end
end

-- Coordinate systems
XYZ = functionCoordinates.get();
lonLatDegrees=functionC(tmpLib,"lonLatDegrees",3,{XYZ})
lonLat=functionC(tmpLib,"lonLat",3,{XYZ})
--lonLatDegrees=slimFunctionXYZ2LatLon("lonLatDegrees",XYZ)
--lonLat=slimFunctionXYZ2LatLon("lonLat",XYZ)

-- Bath NWECS
print '.... Define, Interpolate and Smooth Bathymetry ....'
groups = dgGroupCollection(model, dimension, order)
bathDC = dgDofContainer(groups, 1);
bathData=slimFunctionGebco("DataNWECS/gebco/gebco_NWECS.nc")
bathData:setCoordFunction(lonLatDegrees);
bathData:setMax(-10.)
bath=functionC(tmpLib,"inverseSign",1,{bathData})
bathDC:interpolate(bath);
bathDC:exportMsh("NWECS/bath", 0, 0);
groups:buildGroupsOfInterfaces()

--diffuse bath
dlaw = dgConservationLawAdvectionDiffusion.diffusionLaw(functionConstant({20}))
dlaw:addBoundaryCondition("ShelfBreak", dlaw:new0FluxBoundary())
dlaw:addBoundaryCondition("Cattegat", dlaw:new0FluxBoundary())
dlaw:addBoundaryCondition("none", dlaw:new0FluxBoundary())
sys = linearSystemPETSc()
dof = dgDofManager.newCG (groups, 1, sys)
dgSolver.implicitEuler (bathDC, dlaw, dof, 100000, 0)
bathDC:exportMsh("NWECS/bath_smooth", 0, 0);
sys:delete()
dof:delete()

-- Tides
tideEta=slimFunctionTpxo("DataNWECS/tpxo/hf.ES2008.nc","ha","hp","lon_z","lat_z")
tideEta:setCoordAndTimeFunctions(lonLatDegrees,timeFunction)
tideU=slimFunctionTpxo("DataNWECS/tpxo/uv.ES2008.nc","Ua","up","lon_u","lat_u")
tideU:setCoordAndTimeFunctions(lonLatDegrees,timeFunction)
tideV=slimFunctionTpxo("DataNWECS/tpxo/uv.ES2008.nc","Va","vp","lon_v","lat_v")
tideV:setCoordAndTimeFunctions(lonLatDegrees,timeFunction)
tideUV=functionC(tmpLib,"lonLatVector",3,{lonLat,tideU,tideV})

print '.... Define Conservation Law,  Set Parameters,  Add Boundary Conditions ....'
claw = dgConservationLawShallowWater2d()
solution = dgDofContainer(groups, claw:getNbFields())
Cd = functionC(tmpLib, "bottomDrag", 1, {solution:getFunction(), bathDC:getFunction()})
claw:setCoriolisFactor(functionC(tmpLib,"coriolis",1,{lonLat}))
claw:setQuadraticDissipation(Cd)
claw:setLinearDissipation(functionConstant({0}))
claw:setDiffusivity(functionConstant({20}))
claw:setBathymetry(bathDC:getFunction())
claw:setBathymetryGradient(bathDC:getFunctionGradient())
tideuv=functionC(tmpLib,"transport2velocity",3,{tideUV,bathDC:getFunction(),solution:getFunction()});
tpxoTide=functionC(tmpLib,"merge",3,{tideEta,tideuv});

claw:addBoundaryCondition('ShelfBreak',claw:newOutsideValueBoundary("Surface",tpxoTide))
claw:addBoundaryCondition('none',claw:newBoundaryWall())
claw:addBoundaryCondition('Cattegat',claw:newBoundaryWall())

print '.... Define Multirate Scheme To Use, Compute the Reference Time Step ....'
groups:deleteGroupsOfInterfaces()
rk=dgRungeKuttaMultirateConservative.new2a(groups, claw)
mL=1000
dt=rk:splitForMultirate(mL, solution, {solution, bathDC})
groups:buildGroupsOfInterfaces()
solution:exportGroupIdMsh()
solution:exportMultirateGroupMsh()

currentFunction = functionC(tmpLib,"current",3,{solution:getFunction()})
tideCurrentFunction = functionC(tmpLib,"current",3,{tpxoTide})

------------------------------------------------------
--solution:L2Projection(tpxoTide)
pc=10
nbExport = 0
print '-------------------- Initializing --------------------'
print ''
print('Reference Time Step:', dt)
print('Norm of Solution At T0:',  solution:norm())
print ''
print '------------------------------------------------------'
solution:exportMsh(string.format(outputDir..'/NWECS-%06d',0), time, nbExport)
solution:exportFunctionMsh(tideEta,string.format(outputDir..'/tideEta-%06d',0), time, nbExport,"")
solution:exportFunctionMsh(tideU,string.format(outputDir..'/tideU-%06d',0), time, nbExport,"U")
solution:exportFunctionMsh(tideV,string.format(outputDir..'/tideV-%06d',0), time, nbExport,"V")
solution:exportFunctionMsh(tideCurrentFunction,string.format(outputDir..'/tideUV-%06d',0), time, nbExport,"")

nbExport=nbExport + 1
startcpu = os.clock()
nbSteps = math.ceil((endTime-initialTime)/dt)

print ('.... Start Iterations(',nbSteps,') ....')
for i=1,nbSteps do
	norm = rk:iterate(time,dt,solution)
  time = time +dt
  if ( i%pc ==0 ) then
   	print(string.format(os.date("%c", time)))
    print('|ITER|',i,'|NORM|',norm,'|DT|',dt)
    print('|PROC|',Msg:getCommRank(),'|CPU|',os.clock() - startcpu)
    print'-------------------------------------------------------------'
    solution:exportMsh(string.format(outputDir..'/NWECS-%06d',i), time, nbExport)
    nbExport = nbExport + 1
  end
end
endcpu=os.clock()

print'*************************************'
print('|TOTAL CPU|',    endcpu - startcpu)
print'*************************************'
print'.... DONE ....'

