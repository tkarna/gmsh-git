# -*- coding: utf-8 -*-
# Import for python
from dgpy import *
from gmshpy import *
from scipy import *                     
from scipy.sparse import *              
from scipy.sparse.linalg import *       
from matplotlib.pyplot import *         
from numpy.linalg import *
from numpy import matrix
import time, math, os

meshFile =   "circle.msh" #"boxConv.msh"
meshFile2d = "circle2d.msh"

model = GModel()
model.load(meshFile)
msg = Msg()
order = 1
dimension = 3
groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByVerticalLayer(["bottom"])
groups.buildGroupsOfInterfaces()

model2d = GModel()
model2d.load (meshFile2d)
groups2d = dgGroupCollection(model2d, dimension-1, order)
groups2d.buildGroupsOfInterfaces()

columnInfo = dgSW3dVerticalColumns(groups,groups2d,["bottom"])

#Fonctions
#---------

#transformation xyz en lat-lon
def lonLat(coord,xyz):
  for i in range(xyz.size1()) :
     x = xyz.get(i,0)
     y = xyz.get(i,1)
     z = xyz.get(i,2)
     sol1 = 90 - (math.sqrt(y*y+x*x)*10)/1000000
     sol2 = 180 + math.atan2(y,x)*180/math.pi
     if (sol2<0.5):
        sol2 = 180 + math.atan2(y,x)*180/math.pi + 359
     if (sol1>89.5):
        sol1 = 89.5 -(math.sqrt(y*y+x*x)*10)/1000000
     coord.set(i, 0, sol1)
     coord.set(i, 1, sol2)
     coord.set(i, 2, z)
     
#passage en celcius
def kelvincelsius (t,k) :
   for i in range(k.size1()) :
     oldt = k.get(i,0)
     t.set(i,0,oldt-273.16)

#calcul de la pression hydrostatique
def hydrostaticpressure (p,xyz) :
   for i in range(xyz.size1()) :
     z = xyz.get(i,2)
     p.set(i,0,-z)
     
#calcul de la pente
def slopefunction (sl,grxyz,nn) :
   for i in range(grxyz.size1()) :
     grx = grxyz.get(i,0)
     gry = grxyz.get(i,1)
     grz = -nn.get(i,0)*1027/9.81
     Sx = -grx/grz
     Sy = -gry/grz
     Slim = 0.01
     if (Sx<0) :
       absSx = -Sx
     else :
       absSx = Sx 
     if (Sy<0) :
       absSy = -Sy
     else :
       absSy = Sy
     sl.set(i,0,0.5*(1+math.tanh((0.01-absSx)/0.001))*Sx)
     sl.set(i,1,0.5*(1-math.tanh((0.01-absSy)/0.001))*Sy)
     
#Eddy Ferrari
def cste( cc, NN) :
   for i in range(NN.size1()):
      n2 = NN.get(i,0)
      if (n2<0):
        n2 = 0
      val = math.sqrt(n2)*1027/math.pi
      cc.set(i,0,val)
      if (val < 0.1) :
        cc.set(i,0,0.1)
def termR(indR ,gradb) :
   for i in range(gradb.size1()):
      gx = gradb.get(i,0)
      gy = gradb.get(i,1)
      indR.set(i,0,gx*9.81/1027)
      indR.set(i,1,gy*9.81/1027)
      indR.set(i,2,0)

#Diffusion normale (//GOTM)
def diffu(d,xyz):
  for i in range(xyz.size1()):
    z = xyz.get(i,2)
    d.set(i,0,0.0001/2*(1.1+0.9*cos(z*math.pi/200)))
#Diffusion verticale
def mixing(Kc,n2):
   for i in range(n2.size1()) :
    n = n2.get(i,0)
    Kac = 0.0
    if ((-1027/9.81*n)>0) :
      Kac = 1
    Kc.set(i,0,Kac)
#Sommation des diffusions
def diffutot(di,nuN,nuK):
  for i in range(nuN.size1()):
    di.set(i,0,nuN.get(i,0)+nuK.get(i,0))

#Vitesse de GM
def velocityGM (h,gradpsi) :
  for i in range(gradpsi.size1()):
    gradXx = gradpsi.get(i,0)
    gradXz = gradpsi.get(i,2)
    gradYy = gradpsi.get(i,4)
    gradYz = gradpsi.get(i,5)
    h.set(i,0,gradXz)
    h.set(i,1,gradYz)
    h.set(i,2,(-gradXx-gradYy))

#Programme de base
#----------------

#maillage
XYZ = getFunctionCoordinates()
coord = functionPython (3,lonLat,[XYZ])

#creation de la diffusivité normale (après par GOTM)
nuDof = dgDofContainer(groups,1)
d = functionPython(1,diffu,[XYZ])
nuDof.interpolate(d)
nuDof.exportMsh('allin/nuN',0,0)

#lecture des données et stockage
nnDof = dgDofContainer(groups, 1)
N2 = nnDof.getFunction()
SDof = dgDofContainer(groups, 1)
TDof = dgDofContainer(groups,1)
dz = [0,10,20,30,50,75,100,125,150,200,250,300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500]
tempData = ncDataDz("ocean_temp_PHC_3_0_prepro.nc","ocean_temp","lat","lon",dz)
tempFunc = functionNcDataDz(tempData,coord)
temp = functionPython (1,kelvincelsius,[tempFunc])
TDof.interpolate(temp)
T = TDof.getFunction()

salData = ncDataDz("ocean_sal_PHC_3_0.nc","ocean_sal","lat","lon",dz)
salFunc = functionNcDataDz(salData,coord)
SDof.interpolate(salFunc)
S = SDof.getFunction()

p = functionPython (1,hydrostaticpressure,[XYZ])

#calcul de la densité
rhoFunc = functionStateEquation(S,T,p)
rhoDof = dgDofContainer(groups,1)
rhoDof.interpolate(rhoFunc)
rhoDof.exportMsh('allin/R_0',0,0)

#Containers et definitions necessaires pour la diffusion
nn = fullMatrixDouble(1,1)
M = fullMatrixDouble(1,1)
nuV = dgDofContainer(groups,1)
#calcul de la diffusivité verticale supplémentaire
Kc = functionPython(1,mixing,[nnDof.getFunction()])
#Combinaison des diffusivités
nuCDof = dgDofContainer(groups,1)
di = functionPython(1,diffutot,[d,Kc])
nuCDof.interpolate(di)
nuCDof.exportMsh('allin/nuI',0,0)

#creation of dofContainer
gradrhodDof = dgDofContainer (groups, 3)
gradrhodDofTemp = dgDofContainer (groups, 3)
gradrhoDof = dgDofContainer (groups, 3)


#calcul du gradient de la densite
gradrhodEq = dgFEGradient(rhoFunc)
gradrhoSym = gradrhodEq.newSymmetryBoundary('')
gradrhodEq.addBoundaryCondition('surf',gradrhoSym)
gradrhodEq.addBoundaryCondition('bottom',gradrhoSym)
gradrhodEq.addBoundaryCondition('wall',gradrhoSym)
#creation of residual
gradrhodResidu = dgResidual(gradrhodEq)
gradrhodResidu.compute(groups,0,gradrhodDofTemp,gradrhodDof)
gradrhodDof.multiplyByInvMassMatrix() 
#le rendre continu
cgproj=dgL2ProjectionCG(gradrhodDof)
cg = gradrhodDof.getFunction()
cgproj.project(gradrhoDof,cg)
#calcul de la pente
grad = gradrhoDof.getFunction()
  
sl = functionPython (2,slopefunction,[grad,N2])

#--calcul de la fonction de courant 'eddy transport' par Ferrari 2010
#--------------------------------------------------------------------

cc = functionPython (1,cste,[N2])
indR = functionPython (3,termR,[grad])

psiEq= dgEddyTransportFlux(cc,indR,N2)
psiEqBSBnd = functionConstant([0,0,0])
psiEq.addStrongBoundaryCondition(2,"surf",psiEqBSBnd)
psiEq.addStrongBoundaryCondition(2,"bottom",psiEqBSBnd)
psiEq.addStrongBoundaryCondition(2,"wall",psiEqBSBnd)
psiEqZeroBnd = psiEq.new0FluxBoundary()
psiEq.addBoundaryCondition('surf',psiEqZeroBnd)
psiEq.addBoundaryCondition('bottom',psiEqZeroBnd)
psiEq.addBoundaryCondition('wall',psiEqZeroBnd)

fieldspsi = psiEq.getNbFields()
psiDof = dgDofContainer(groups,fieldspsi)

residualP = dgDofContainer(groups,fieldspsi)
sysP =  linearSystemPETScDouble()
sysP.setParameter("petscOption", "-pc_type lu")
dofP = dgDofManager.newCG (groups,3,sysP)

#dgSolver.setVerb(10)
sol = dgSteady(psiEq,dofP)

#le rendre continu
psicDof = dgDofContainer (groups, 3)
cgprojP=dgL2ProjectionCG(psiDof)
cgP = psiDof.getFunction()

#--Creation de la vitesse maintenant
#----------------------------------
uGMDof = dgDofContainer(groups,3)
gradPDof = dgDofContainer(groups,9)
psigrad = psicDof.getFunctionGradient()
gradPDof.interpolate(psigrad)
gradslope = gradPDof.getFunction()
h = functionPython (3,velocityGM,[gradslope])

streamu=functionExtractCompNew(uGMDof.getFunction(),0)
streamv=functionExtractCompNew(uGMDof.getFunction(),1)
streamw=functionExtractCompNew(uGMDof.getFunction(),2)
streamuvw=functionCatCompNew([streamu,streamv,streamw])
streamuv = functionCatCompNew([functionExtractCompNew(uGMDof.getFunction(),0),functionExtractCompNew(uGMDof.getFunction(),1)])

law = dgConservationLawSW3dTracer(streamuv,streamw)
constfunc1 = functionConstant([200])
law.setBath(constfunc1)
law.setKappaV(di)
diff = functionConstant([100])
law.setIsoDiff(sl,diff)
# Boundary condition
v_bc = functionConstant(0)
law.addBoundaryCondition('surf',law.new0FluxBoundary())
law.addBoundaryCondition('wall',law.new0FluxBoundary())
law.addBoundaryCondition('bottom',law.new0FluxBoundary())
nbFields = law.getNbFields()

#implicit
#-------
residual = dgDofContainer(groups, law.getNbFields())
sys = linearSystemPETScBlockDouble () #Block
sys.setParameter("petscOption", "-pc_type lu -ksp_monitor")
dof = dgDofManager.newDGBlock(groups, 1, sys) #Block
implicitRK2 = dgDIRK(law, dof,1)
implicitRK2.getNewton().setRtol(1e-6)
implicitRK2.getNewton().setAtol(1e-11)
implicitRK2.getNewton().setVerb(10)

export_interval = 20*86400
next_export_time = export_interval
dt = 86400
#Frequence de Brunt-Vaisala
zFunc = functionExtractCompNew(XYZ,2)
vmod = dgSW3dVerticalModel(columnInfo)
vmod.setComputeNNWithEOS(True)
vmod.setZFunction(zFunc)
vmod.setTContainer(TDof)
vmod.setSContainer(SDof)
t = 0
#conservation globale
matrixS=fullMatrixDouble(1,1)
matrixT=fullMatrixDouble(1,1)
inteS=dgFunctionIntegrator(groups,S,SDof)
inteT=dgFunctionIntegrator(groups,T,TDof)
inteS.compute(matrixS,"")
inteT.compute(matrixT,"")
S0 = matrixS.get(0,0)
T0 = matrixT.get(0,0)
print '%.16e %.16e' %(matrixS.get(0,0), matrixT.get(0,0))
for j in range(1,2000):
  #Frequence de Brunt-Vaisala
  vmod.computeHeights()
  vmod.computeNN()
  vmod.exportNNData(nnDof)
  nnDof.minmax(nn,M)
  print 'jour', j, 'N2 = ', nn(0,0)

  #calcul de la diffusivité verticale supplémentaire
  nuV.interpolate(Kc)
  #nuV.exportMsh('allin/Kc_%2d'%(j),t,j)
  
  #Combinaison des diffusivités
  nuCDof.interpolate(di)
  #nuCDof.exportMsh('allin/nuT_%2d'%(j),t,j)
  
  #vitesse de G-M
  sol.solve(psiDof)
  cgprojP.project(psicDof,cgP)
  gradPDof.interpolate(psigrad)
  uGMDof.interpolate(h)
  
  #calcul du gradient de la densite
  t += dt
  implicitRK2.iterate(SDof, dt, t)
  implicitRK2.iterate(TDof, dt, t)
  rhoDof.interpolate(rhoFunc)
  
  gradrhodResidu.compute(groups,0,gradrhodDofTemp,gradrhodDof)
  gradrhodDof.multiplyByInvMassMatrix() 
  cgproj.project(gradrhoDof,cg)
 
  if ( t >= next_export_time):
     next_export_time = next_export_time + export_interval
     uGMDof.exportFunctionMsh(streamuvw,'allin/U_%2d'%(j), t, j,'U')
     TDof.exportMsh('allin/Tm_%2d'%(j),t,j)
     SDof.exportMsh('allin/Sm_%2d'%(j),t,j)
     rhoDof.exportMsh('allin/R_%2d'%(j),t,j)
     #conservation
     inteS.compute(matrixS,"")
     inteT.compute(matrixT,"")
     print '%.16e %.16e' %((S0-matrixS.get(0,0))/S0,(T0-matrixT.get(0,0))/T0)