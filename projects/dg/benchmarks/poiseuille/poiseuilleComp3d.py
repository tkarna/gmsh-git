from gmshpy import *
from dgpy import *
import os
import time
import math

MACH = .05;
RHO  = 1.0;
PRES = 1./(MACH*RHO*RHO*1.4*1.4) 
V = 2.
MU = 0.02
KAPPA = 0.01

t = 0;

def free_stream(FCT,  XYZ) :
  v = V
  for i in range (0,XYZ.size1()) :
    FCT.set(i,0,RHO) 
    FCT.set(i,1,0.0) 
    FCT.set(i,2,0.0) 
    FCT.set(i,3,RHO*v) 
    FCT.set(i,4, 0.5*RHO*v*v+PRES/0.4) 

def outlet_stream(FCT,  sol) :
  v = V
  for i in range (0,sol.size1()) :
    FCT.set(i,0,RHO) 
    FCT.set(i,1,sol.get(i,1)) 
    FCT.set(i,2,sol.get(i,2)) 
    FCT.set(i,3,sol.get(i,3)) 
    FCT.set(i,4, 0.5*RHO*v*v+PRES/0.4) 


xyz = getFunctionCoordinates()
FS = functionPython(5, free_stream, [xyz])
outletS = functionPython(5, outlet_stream, [xyz])

order = 3
dimension = 3

print'*** Loading the mesh and the model ***'
model   = GModel  ()
model.load('box.msh')
groups = dgGroupCollection(model, dimension, order)
groups.buildGroupsOfInterfaces()

law=dgPerfectGasLaw(dimension)
muF = functionConstant(MU)
kappaF = functionConstant(KAPPA)
law.setViscosityAndThermalConductivity(muF, kappaF);

slipWallBoundary = law.newSlipWallBoundary()
nonSlipWallBoundary = law.newNonSlipWallBoundary()

outsideBoundary = law.newOutsideValueBoundary("",FS)
outsideBoundaryOutlet = law.newOutsideValueBoundary("",outletS)
law.addBoundaryCondition('inlet',outsideBoundary)
law.addBoundaryCondition('outlet',outsideBoundaryOutlet)
law.addBoundaryCondition('symmetry',slipWallBoundary)
law.addBoundaryCondition('wall',nonSlipWallBoundary)
#law.addBoundaryCondition('symmetry',outsideBoundary)
#law.addBoundaryCondition('wall',outsideBoundary)

print'*** setting the initial solution ***'
solution = dgDofContainer(groups, law.getNbFields())
solution.L2Projection(FS)

print'*** solve ***'
print('*** Reynolds= %g' % (RHO*V*1/MU))
dt = 0.1

#sys = linearSystemPETScDouble ()
#sys.setParameter("petscOptions", "-pc_type lu")
#dof = dgDofManager.newDG (groups, law.getNbFields(), sys)
##sys.setParameter("petscOptions", "-pc_type bjacobi -pc_bjacobi_blocks  %i -sub_pc_type lu -ksp_rtol 1e-3 -ksp_max_it 50" % groups.getElementGroup(0).getNbElements())
#nbElements = groups.getElementGroup(0).getNbElements() +groups.getElementGroup(1).getNbElements() 
#sys.setParameter("petscOptions", "-pc_type bjacobi -pc_bjacobi_blocks  %i -sub_pc_type lu -ksp_rtol 1e-3 -ksp_max_it 50" % nbElements)

#sys = linearSystemPETScBlockDouble ()
#sys.setParameter("petscOptions", "-pc_type lu")

## matrix free bjacobi
sys = dgLinearSystemMatrixFreeBJacobi (law, groups)
dof = dgDofManager.newDGBlock (groups, law.getNbFields(), sys)
#am = dgAdamsMoulton (4, solution, law, dof, dt, t)
am = dgDIRK (law, dof, 1)
am.getNewton().setAtol(1e-12)
am.getNewton().setRtol(1e-9)
am.getNewton().setVerb(10)
solution.exportMsh('output/init-%05i' % (0), 0, 0)

for i in range (0, 100):
  t = t + dt
  print("Iter %i t=%.2f" % (i,t))
  am.iterate(solution, dt, t)
  if (i % 1 == 0) :
    solution.exportMsh('output/sphere-%05i' % (i), t, i)

