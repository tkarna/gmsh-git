from gmshpy import *
from dgpy import *
import os
import time
import math

GAMMA = 1.4
MU = 1e-5
R = 287
RHO = 1.2
T = 293.15

C = math.sqrt(GAMMA * R * T)

PRANDTL = 0.72    # mu c_p / k
PRANDTL = 2.8

CP = GAMMA * R / (GAMMA - 1)
KAPPA = MU / PRANDTL * CP
#CV = R / (GAMMA - 1)


MACH = 0.005       # v / c
L = 0.0004

V = MACH * C
#P = C * C * RHO / GAMMA
P = 1./(MACH*RHO*RHO*1.4*1.4)
print("my P = %e", RHO * R * T)

EI = P/RHO/(GAMMA-1)

EK = V*V/2

print ("EI = %e vs %e" % (EI, R*T/(GAMMA-1)))

REYNOLDS = L * RHO * V / MU 


print("v = %e, mu = %e, k = %e, p = %e, C = %e, rho = %e" % (V, MU, KAPPA, P, C, RHO))

dt = 0.001
print("dt = %e" %(dt))

t = 0;

def free_stream(f) :
  for i in range (0,f.size1()) :
    f.set(i, 0, RHO) 
    f.set(i, 1, RHO * V) 
    f.set(i, 2, 0.) 
    f.set(i, 3, (EI + EK) * RHO) 

def outlet_stream(f,  sol) :
  for i in range (0,sol.size1()) :
    rho = sol.get(i, 0)
    u = sol.get(i, 1) / rho
    v = sol.get(i, 2) / rho
    f.set(i, 0, rho) 
    f.set(i, 1, rho * u) 
    f.set(i, 2, rho * v) 
    f.set(i, 3, (v * v + u * u) * rho / 2  + EI * RHO) 

def velocity(f,  sol) :
  for i in range (0,sol.size1()) :
    rho = sol.get(i, 0)
    u = sol.get(i, 1) / rho
    v = sol.get(i, 2) / rho
    f.set(i, 0, u) 
    f.set(i, 1, v) 
    f.set(i, 2, 0.) 

def temperature(f,  sol) :
  for i in range (0,sol.size1()) :
    rho = sol.get(i, 0)
    u = sol.get(i, 1) / rho
    v = sol.get(i, 2) / rho
    e = sol.get(i, 3) / rho
    T = (e - (u*u + v*v) / 2)/R
    f.set(i, 0, T) 

xyz = getFunctionCoordinates()
FS = functionPython(4, free_stream, [])
velocityF = functionPython(3, velocity, [function.getSolution()])
temperatureF = functionPython(1, temperature, [function.getSolution()])
outletS = functionPython(4, outlet_stream, [function.getSolution()])

order = 3
dimension = 2

print'*** Loading the mesh and the model ***'
model   = GModel  ()
model.load('rectangle.msh')
groups = dgGroupCollection(model, dimension, order)
groups.buildGroupsOfInterfaces()

law=dgPerfectGasLaw(dimension)
muF = functionConstant(MU)
kappaF = functionConstant(KAPPA)
law.setViscosityAndThermalConductivity(muF, kappaF);
law.setGasConstantAndGamma(R, GAMMA)

slipWallBoundary = law.newSlipWallBoundary()
nonSlipWallBoundary = law.newNonSlipWallBoundary()

outsideBoundary = law.newOutsideValueBoundary("",FS)
outsideBoundaryOutlet = law.newOutsideValueBoundary("",outletS)
law.addBoundaryCondition('inlet',outsideBoundary)
law.addBoundaryCondition('outlet',outsideBoundaryOutlet)
law.addBoundaryCondition('symmetry',slipWallBoundary)
law.addBoundaryCondition('wall',nonSlipWallBoundary)
#law.addBoundaryCondition('symmetry',outsideBoundary)
#law.addBoundaryCondition('wall',outsideBoundary)

print'*** setting the initial solution ***'
solution = dgDofContainer(groups, law.getNbFields())
solution.L2Projection(FS)

print'*** solve ***'
#sys = linearSystemPETScDouble ()
#sys.setParameter("petscOptions", "-pc_type lu")
#dof = dgDofManager.newDG (groups, law.getNbFields(), sys)
##sys.setParameter("petscOptions", "-pc_type bjacobi -pc_bjacobi_blocks  %i -sub_pc_type lu -ksp_rtol 1e-3 -ksp_max_it 50" % groups.getElementGroup(0).getNbElements())
#nbElements = groups.getElementGroup(0).getNbElements() +groups.getElementGroup(1).getNbElements() 
#sys.setParameter("petscOptions", "-pc_type bjacobi -pc_bjacobi_blocks  %i -sub_pc_type lu -ksp_rtol 1e-3 -ksp_max_it 50" % nbElements)

sys = linearSystemPETScBlockDouble ()
sys.setParameter("petscOptions", "-pc_type lu")

## matrix free bjacobi
#sys = dgLinearSystemMatrixFreeBJacobi (law, groups)
dof = dgDofManager.newDGBlock (groups, law.getNbFields(), sys)
#am = dgAdamsMoulton (4, solution, law, dof, dt, t)
am = dgDIRK (law, dof, 1)
am.getNewton().setAtol(1e-10)
am.getNewton().setRtol(1e-9)
am.getNewton().setVerb(10)
solution.exportMsh('output/init-%05i' % (0), 0, 0)

for i in range (0, 1000):
  t = t + dt
  print("Iter %i t=%.2f" % (i,t))
  am.iterate(solution, dt, t)
  if (i % 1 == 0) :
    solution.exportMsh('output/poiseuille-%05i' % (i), t, i)
    solution.exportFunctionMsh(velocityF, 'output/velocity-%05i' % (i), t, i, "velocity")
    solution.exportFunctionMsh(temperatureF, 'output/temperature-%05i' % (i), t, i, "temperature")

