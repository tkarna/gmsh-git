%feature("autodoc", "1");
%module ann

%{
  #define SWIG_FILE_WITH_INIT
  #include "ANN/ANN.h"
  class kdtree 
  {
    ANNpointArray coords;
    ANNkd_tree *tree;
    public:
    kdtree(double *pa, int m, int n){
      coords = annAllocPts(m, n);
      for (int i = 0; i < m; i++)
        for (int j = 0; j < n; j++)
          coords[i][j] = pa[i * n + j];
      tree = new ANNkd_tree(coords, m, n);
    }
    ~kdtree(){
      annDeallocPts(coords);
      delete tree;
    }
    void search(double *v, int n, int *idx, int nidx, double *dist, int ndist){
      tree->annkSearch(v, nidx, idx, dist);
    }
  };
%}

%include "numpy.i"
%init %{
import_array();
%}

%apply (double* IN_ARRAY2, int DIM1, int DIM2) {(double* pa, int m, int n)}
%apply (double* IN_ARRAY1, int DIM1) {(double* v, int n)}
%apply (double* INPLACE_ARRAY1, int DIM1) {(double* dist, int ndist)}
%apply (int* INPLACE_ARRAY1, int DIM1) {(int* idx, int nidx)}
class kdtree {
  ANNkd_tree *tree;
  public:
  kdtree(double *pa, int m, int n);
  void search(double *v, int n, int *idx, int nidx, double *dist, int ndist);
};
