from extrude import *
import sys

if (len(sys.argv) < 3) :
  print 'please write the input and output mesh files !'
  exit(-1)
else :
  meshInput = sys.argv[1]
  meshOutput = sys.argv[2]

zLayers = [0, 50, 100, 150, 200, 250, 315, 400, 510, 655, 850, 1100, 1400, 1700, 2000, 2300, 2600, 2900, 3200, 3500, 3800, 4100, 4400, 4700, 5000, 5300, 5600, 5900, 6200, 6500, 6800, 7100, 7400, 7700, 8000, 8300, 8600, 8900, 9200, 9500, 9800, 10100, 10400, 10700, 11000, 11300, 11600, 11900, 12200]

model = GModel()
model.load(meshInput)
groups = dgGroupCollection(model, 2, 1)
bath = dgDofContainer(groups, 1);
##bath.importMsh("bath")
filename = "world"
outputDir = "bath"
bathname= outputDir+"/"+filename+"_bath_smooth"+"/"+filename+"_bath_smooth.idx"
bath.readMsh(bathname)
def getLayers (x, y, iGroup, iElement, iVertex) :
  h = bath.getGroupProxy(iGroup)(iVertex, iElement);
  MinDiff = 1e10
  for i in range(len(zLayers)) :
    diff = abs(h - zLayers[i])
    if diff < MinDiff :
      MinDiff = diff
      MinI = i
  MinI = max(1, MinI)
  return zLayers[0:(MinI + 1)]

print "Info : extrude " + meshInput
extrude(mesh(meshInput), getLayers).write(meshOutput)
