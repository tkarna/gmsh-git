#!/usr/bin/python
# -*-coding:Utf-8 -*

##
# @file svg2geo.py
# @brief Convert a SVG file into a GEO file.
# @author Lambrecht, Jonathan
# @author Vallaeys, Valentin
# @author Le Bars, Yoann
# @version 1.3
# @date 2013/09/03
# @date 2013/09/04
# @date 2013/09/05
# @date 2013/09/06
# @date 2013/09/17
# @date 2013/10/11
# @date 2013/10/24
# @date 2013/10/25
# @date 2013/10/28

##
# @mainpage
# Svg2geo is a tool which converts a SVG file into a GEO file.
#
# Command:
#   svg2geo.py [-h] [-s SCALE] ifile ofile
#
# positional arguments:
#  ifile                 Name of the SVG file to be readed.
#  ofile                 Name of the GEO file for output.
#
#optional arguments:
#  -h, --help            show this help message and exit
#  -s SCALE, --scale SCALE
#                        Scale factor (default 1.).

#check inkscape options before editing :
# snap to point
# do not preserve path on remove

import math
import xml.sax.handler
import svgpathparser
import numpy
import argparse
from svg2geo_config import *
import os.path

class SVGHandler(xml.sax.handler.ContentHandler):
  def __init__(self, ofile):
    self.inTitle = False
    self.ofile = ofile
    self.title =""
    self.ip = 2
    self.il = 1
    self.physicalLines = {}
    self.connectingNodes = []
    self.pointToLine = {}
#    self.groupdx = 0.
#    self.groupdy = 0.
    # Matrix stack.
    self.transformationStack = [numpy.identity(3)]
    ofile.write("IP = newp;\nIL = newl;\nIS = news;\nIF = newf;\nILL = newll;\n");
    if writeStereoHeader :
      ofile.write("Point(IP + 0) = {0, 0, 0};\nPoint(IP + 1) = {0, 0, %e};\nPolarSphere(IS) = {IP + 0, IP + 1};\n" % R);

  ##
  # @brief Apply transformations.
  # @param self Current object.
  # @param transformString String containing transformations descriptions.
  # @param transformMatrix Transformation matrix, will be modified.
  def parseTransform (self, transformString, transformMatrix):
    """
    Apply transformation to the current path.
    """
#    transfo = transformString.split('(')
#    if (transfo[0] == "translate"):
#      dx = float(transfo[1].split(',')[0])
#      dy = float(transfo[1].split(',')[1].strip(')'))
#    translateMatrix = numpy.identity(3)
#    translateMatrix[0, 2] = dx
#    translateMatrix[1, 2] = dy
#    transformMatrix[:, :] = transformMatrix.dot(translateMatrix)

    # Array which contains each transformation to be applied.
    transformations = transformString.split(' ')
    for transfo in transformations:
      # Value of the current transformation.
      value = transfo.split('(')
      # Current transformation matrix.
      transformation = numpy.identity(3)

      if (value[0] == "translate"):
        # Deplacement according abscissa.
        dx = float(value[1].split(',')[0])
        # Deplacement according ordinate.
        dy = float(value[1].split(',')[1].strip(')'))
        transformation[0, 2] = dx
        transformation[1, 2] = dy
        transformMatrix[:, :] = transformMatrix.dot(transformation)

      if (value[0] == "scale"):
        # Scaling according abscissa.
        sx = float(value[1].split(',')[0])
        # Scaling according ordinate.
        sy = float(value[1].split(',')[1].strip(')'))
        transformation[0, 0] = sx
        transformation[1, 1] = sy
        transformMatrix[:, :] = transformMatrix.dot(transformation)

      if (value[0] == "rotate"):
        # Rotation angle.
        a = (float(value[1].split(',')[0]) * math.pi) / 180.
        # Rotation centre abscissa.
        cx = float(value[1].split(',')[1])
        # Rotation centre ordinate.
        cy = float(value[1].split(',')[2]).strip(')')
        # Pre-translation matrix.
        preRot = numpy.identity(3)
        preRot[0, 2] = -cx
        preRot[1, 2] = -cy
        # Post-translation matrix.
        postRot = numpy.identity(3)
        postRot[0, 2] = cx
        postRot[1, 2] = cy
        # Rotation matrix.
        rotate = numpy.identity(3)
        rotate[0, 0] = math.cos(a)
        rotate[1, 0] = math.sin(a)
        rotate[0, 1] = -rotate[1, 0]
        rotate[1, 1] = rotate[0, 0]
        transformMatrix[:, :] = transformMatrix.dot(preRot.dot(rotate.dot(postRot)))

      if (value[0] == "skewX"):
        # Skew angle.
        a = (float(value[1].strip(')')) * math.pi) / 180.
        # Skew matrix.
        skew = numpy.identity(3)
        skew[0, 1] = math.tan(a)
        transformMatrix[:, :] = transformMatrix.dot(skew)

      if (value[0] == "skewY"):
        # Skew angle.
        a = (float(value[1].strip(')')) * math.pi) / 180.
        # Skew matrix.
        skew = numpy.identity(3)
        skew[1, 0] = math.tan(a)
        transformMatrix[:, :] = transformMatrix.dot(skew)

      if (value[0] == "matrix"):
        # Transformation matrix.
        matrix = numpy.identity(3)
        matrix[0, 0] = float(value[1].split(',')[0])
        matrix[1, 0] = float(value[1].split(',')[1])
        matrix[0, 1] = float(value[1].split(',')[2])
        matrix[1, 1] = float(value[1].split(',')[3])
        matrix[0, 2] = float(value[1].split(',')[4])
        matrix[1, 2] = float(value[1].split(',')[5].strip(')'))
        transformMatrix[:, :] = transformMatrix.dot(matrix)

  def startElement(self, name, attributes):
    if name == "g":
      self.transformationStack.append(self.transformationStack[-1].copy())
      if attributes.has_key("transform") :
#        transform = attributes["transform"]
        self.parseTransform(attributes["transform"], self.transformationStack[-1])
#        print("group transfo", transform)
#        transfo = transform.split('(')
#        if (transfo[0] == "translate"):
#          self.groupdx = float(transfo[1].split(',')[0])
#          self.groupdy = float(transfo[1].split(',')[1].strip(')'))
    if name == "path":
      self.transformationStack.append(self.transformationStack[-1].copy())
      self.title = ""
#      self.dx = self.groupdx
#      self.dy = self.groupdy
      if attributes.has_key("transform") :
        self.parseTransform(attributes["transform"], self.transformationStack[-1])
#        transform = attributes["transform"]
#        transfo = transform.split('(')
#        if (transfo[0] == "translate"):
#          self.dx += float(transfo[1].split(',')[0])
#          self.dy += float(transfo[1].split(',')[1].strip(')'))
      self.path = attributes["d"]
    elif name == "title":
      self.inTitle = True
  def characters(self, data):
    if self.inTitle:
      if data =='\n':
        self.inTitle = False
      else :
        self.title += data
  def endElement(self, name):
    if name == "g":
#      self.groupdx = 0.
#      self.groupdy = 0.
      self.transformationStack.pop()
    if name == "path":
      self.inTitle = False
      if self.title == "":
        self.title = defaultPhysicalTag
      self.writePath(self.path)
      self.transformationStack.pop()
  def writeNode(self, node):
    [x, y, z] = outputCoordinates(*node)
    #ofile.write("Point(IP + %i) = {%.16e, %.16e, 0.};\n" % (self.ip, u, v));
    ofile.write("Point(IP + %i) = {%.16e, %.16e, %.16e};\n" % (self.ip, x, y, z));

    self.ip = self.ip + 1
  def getConnectingNode(self, node):
    for i, cnode in enumerate(self.connectingNodes) :
      d = [cnode[0] - node[0], cnode[1] - node[1], cnode[2] - node[2]]
      if (d[0] * d[0] + d[1] * d[1] + d[2] * d[2]) < (samePointDistance) ** 2 :
        del self.connectingNodes[i]
        return cnode[3]
    self.connectingNodes.insert(0, [node[0], node[1], node[2], self.ip])
    self.writeNode(node)
    return self.ip - 1
  def addLine(self, ltype, points) :
    beginp = self.getConnectingNode(points[0])
    endp = self.getConnectingNode(points[-1])
    if beginp in self.pointToLine:
      self.pointToLine[beginp].append((self.il, beginp, endp))
    else :
      self.pointToLine[beginp] = [(self.il, beginp, endp)]
    if endp in self.pointToLine:
      self.pointToLine[endp].append((self.il, beginp, endp))
    else :
      self.pointToLine[endp] = [(self.il, beginp, endp)]
    points.pop(0)
    points.pop(-1)
    firstp = self.ip
    for node in points:
      self.writeNode(node)
    if points:
      ofile.write("%s(IL + %i) = {IP + %i, IP + %i : IP + %i, IP + %i};\n" % (ltype, self.il, beginp, firstp, self.ip - 1, endp))
    else:
      ofile.write("%s(IL + %i) = {IP + %i, IP + %i};\n" % (ltype, self.il, beginp, endp))
    if (self.title != ""):
      if (self.title in self.physicalLines):
        self.physicalLines[self.title].append(self.il)
      else:
        self.physicalLines[self.title] = [self.il]
    self.il = self.il + 1;
  def writePath(self, path):
    lines = svgpathparser.parse_path(path)
    i = 0
    while i < len(lines) :
      ltype = lines[i][0]
      points = []
      points += lines[i][1]
      i += 1
      while (i < len(lines) and ltype == lines[i][0]) :
        points += lines[i][1][1:]
        i += 1
#      self.addLine(ltype, [inputCoordinates(p.real + self.dx, p.imag + self.dy) for p in points])
      xypoints = []
      for p in points:
        x, y = self.transformationStack[-1].dot([p.real, p.imag, 1.])[0:2]
        xypoints.append(inputCoordinates(x, y))
      self.addLine(ltype, xypoints)
      #apply transformMatrix[-1]
#      self.addLine(ltype, [inputCoordinates(p.real, p.imag) for p in points])
  def endDocument(self):
    for tag, lines in self.physicalLines.items():
      self.ofile.write("Physical Line (\"%s\") = {IL + %i" % (tag, lines[0]));
      for i in range(1, len(lines)):
        self.ofile.write(", IL + %i" % lines[i]);
      self.ofile.write("};\n");
    self.ofile.write("Field[IF + 0] = Attractor;\nField[IF + 0].NodesList = {IP + 2 : IP + %i};\n" % (self.ip -1));
    ill = 1
    while self.pointToLine :
      # no better way to access any value of the dictionnary
      for lines in self.pointToLine.values() :
        break
      currentline = lines[0][0]
      firstp = lines[0][1]
      lastp = lines[0][2]
      loop = [currentline]
      del self.pointToLine[firstp]
      while lastp != firstp :
        if not lastp in self.pointToLine:
          break
        lines = self.pointToLine[lastp]
        del self.pointToLine[lastp]
        if len(lines) != 2 :
          break
        currentline, cfirstp, clastp = lines[1] if lines[0][0] == currentline else lines[0]
        if cfirstp == lastp :
          lastp = clastp
          loop.append(currentline)
        elif clastp == lastp :
          loop.append(-currentline)
          lastp = cfirstp
        else :
          break
      txt = ", ".join([("IL + " + str(l) if l > 0 else "-(IL + " + str(-l) + ")")for l in loop])
      if lastp == firstp :
        self.ofile.write("Line Loop (ILL + " + str(ill) + ") = {" + txt + "};\n");
        ill += 1
      else :
        print("Invalid line loop, skipping : " + txt + "(from point %i to point %i)" % (firstp, lastp))
    self.ofile.write("Plane Surface(IS + 1) = {ILL + 1 : ILL + %i};\n" % (ill - 1))
    self.ofile.write("Physical Surface(\"sea\") = {IS + 1};\n")

# Command line parser.
argumentParser = argparse.ArgumentParser()
argumentParser.add_argument("ifile",
                            help = "Name of the SVG file to be readed.")
argumentParser.add_argument("ofile",
                            help = "Name of the GEO file for output.")
argumentParser.add_argument("-f", "--force",
                            help = "Force overwriting GEO file.")
argumentParser.add_argument("-s", "--scale",
                            help = "Scale factor (default 1.).",
                            type = float, default = 1.)
argumentParser.add_argument("-w", "--worldfile", help = "World file name",
                            type = str)
# Arguments in the command line.
args = argumentParser.parse_args()
# Scale factor to be applied.
scale = args.scale

if (args.worldfile != None):
  # World file: file containing the projection matrix.
  worldFile = open(args.worldfile, "r")
  for j in range(0, 2):
    for i in range(0, 2):
      T[i, j] = worldFile.readline()
  for i in range(0, 2):
    T[i, 2] = worldFile.readline()

# SVG parser.
parser = xml.sax.make_parser()
# Name of the output file.
oName = args.ofile
#if (os.path.exists(oName) and not(args.force)):
#  # User's choice for overwriting.
#  choix = input('"' + oName + '" exists, overwrite it?')
# File in which output result.
ofile = open(oName, "w")
# Handler for output file.
handler = SVGHandler(ofile)
parser.setContentHandler(handler)
# Name of the file to be parsed.
iName = args.ifile
parser.parse(iName)
