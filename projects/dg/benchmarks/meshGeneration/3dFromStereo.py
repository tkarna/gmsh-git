from gmshpy import *
import sys

if (len(sys.argv) < 3) :
  print 'please write the input and output mesh files !'
  exit(-1)
else :
  meshInput = sys.argv[1]
  meshOutput = sys.argv[2]


def projectMesh (model):
  def project(entity):
    numVertices = entity.getNumMeshVertices()
    for iV in range(0,numVertices): 
      v = entity.getMeshVertex(iV)
      alpha = v.x()
      beta = v.y()
      R = 6371220
      sqrtJ = 4 * R ** 2 / (4 * R ** 2 + alpha ** 2 + beta ** 2)
      x = sqrtJ * alpha
      y = sqrtJ * beta
      z = (4 * R ** 2 - alpha ** 2 - beta ** 2) * sqrtJ / (4 * R)
      p = (20000 + v.z())/ 20000
      v.setXYZ(x * p, y * p, z * p)

  gvertices = model.bindingsGetVertices()
  for v in gvertices:
    project(v)
  
  gedges = model.bindingsGetEdges()
  for e in gedges:
    project(e)

  gfaces = model.bindingsGetFaces()
  for f in gfaces:
    project(f)

  gregions = model.bindingsGetRegions()
  for r in gregions:
    project(r)

g = GModel()
g.load(meshInput)
projectMesh(g)
g.save(meshOutput)
