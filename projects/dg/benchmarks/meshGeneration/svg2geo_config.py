##
# @file svg2geo_config
# @brief Configuration settings for Svg2geo.
# @author Le Bars, Yoann
# @author Lambrechts, Jonathan
# @version 1.0
# @date 2013/10/11
# @date 2013/10/28

import math, proj
import numpy

defaultPhysicalTag = "coast"

# wheter or not gmsh should interpret point as on stereographic surface
writeStereoHeader = False
# only used in gmsh stereo header
R = 6.371e6

# minimum distance between 2 different nodes on a radius 1 sphere
samePointDistance = 1.

# inverse of the transformation defined in "svgOutputCoordinates" in gshhs2geo
# input from the svg file, output in cartesian coordinates on a radius 1 sphere
lonlat = proj.system("+proj=latlong +ellps=WGS84")
utm = proj.system("+proj=utm +ellps=WGS84 +zone=32")

# Scale factor. Do not modify the value here, it is set in the command line.
scale = 1.

# Matrix for geo-referencement. Do not modify here, it is set in the command
# line.
T = numpy.eye(3, 3)

def inputCoordinates (u, v):
  ## stereographic coordinates
  #u = u / 50 - 10
  #v = v / 50 - 10
  #z =  (1 - u*u - v*v) / (1 + u*u + v*v)
  #x = -u * (1 + z)
  #y = -v * (1 + z)
  ## Projected coordinates.
  # Original coordinates.
  X = [u, v, 1.]
  # Geo-referenced coordinates.
  Y = T.dot(X)
  # Longitude in radian.
#  lon = (Y[0] * math.pi) / (180. * scale)
  # Latitude in radian.
#  lat = -(Y[1] * math.pi) / (180. * scale)
  # Coordinates in selected projection.
#  x, y = proj.transform(lonlat, utm, lon, lat)
#  z = 0
  #x = math.cos(lat) * math.cos(lon)
  #y = math.cos(lat) * math.sin(lon)
  #z = math.sin(lat)
#  return lon, lat, 0.
#  return x, y, z
  return Y[0], Y[1], 0.
  
# input coordinates on a sphere of radius 1 output in stereographic coordinates 
# for gmsh or whatever you want to debug
def outputCoordinates (x, y, z) :
  return x, y, z
  # stereo (gmsh) : u = 2 R x / (R + z) ; v = 2 R y / (R + z) ; 0


#  return - x / (1 + z), - y / (1 + z), 0
#  return R*x, R*y, R*z
