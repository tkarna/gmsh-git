from gmshpy import *
from dgpy import *
import time, math, os
import os.path
import os
import sys


if (len(sys.argv) == 1) :
  print 'please write the input mesh file !'
  exit(-1)
else :
  meshInput = sys.argv[1]

model = GModel()
model.load(meshInput)
os.putenv("BATHFILE", "/Users/Phil/Desktop/world3d/etopo/etopo2.bin")
if (Msg.GetCommRank()==0):
  functionC.buildLibraryFromFile ("BATH.cc", "BATH.so");
stereo = dgSpaceTransformSpherical(6371220.)
groups = dgGroupCollection(model, 2, 1, stereo)
XYZ = groups.getFunctionCoordinates();
bath = dgDofContainer(groups, 1);
bathS = functionC("BATH.so", "bath", 1, [XYZ])
bath.interpolate(bathS);
bath.exportMsh("bath", 0, 0);
nuB = functionConstant(1e6)
dlaw = dgConservationLawAdvectionDiffusion.diffusionLaw(nuB)
dlaw.addBoundaryCondition("coast", dlaw.new0FluxBoundary())
sys = linearSystemPETScDouble()
dof = dgDofManager.newCG (groups, 1, sys)
implicitEuler = dgDIRK(dlaw, dof)
implicitEuler.iterate (bath, 1, 0)
bath.bezierCrop(10, 2e6)
bath.exportMsh("bath_crop", 0, 0);
bathP = functionC("BATH.so", "diff", 1, [function.getSolution(), bath.getFunction()])
nuB = functionConstant(0)
dlaw = dgConservationLawAdvectionDiffusion.diffusionLaw(nuB)
dlaw.addBoundaryCondition("coast", dlaw.new0FluxBoundary())
dlaw.setSource(bathP)
sys = linearSystemPETScDouble()
sys.setParameter("petscOptions", "-pc_type lu");
dof = dgDofManager.newCG (groups, 1, sys)
solver = dgSteady(dlaw, dof)
solver.getNewton().setVerb(10)
solver.getNewton().setMaxIt(6)
solver.solve(bath)
#bath.exportMsh("bath_smooth", 0, 0);
filename = "world"
outputDir = "bath"
odir = 'bath'
if not os.path.exists(odir) :
   os.mkdir(odir)
bathdir=outputDir+"/"+filename+"_bath_smooth"
bathname=outputDir+"/"+filename+"_bath_smooth"+"/"+filename+"_bath_smooth.idx"
exporterBath = dgIdxExporter(bath,  bathdir)
exporterBath.exportIdx()


Msg.Exit(0)
