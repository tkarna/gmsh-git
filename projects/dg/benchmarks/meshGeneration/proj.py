import ctypes

libproj = ctypes.cdll.LoadLibrary("libproj.so")
nullptr = ctypes.POINTER(ctypes.c_int)()

class system :
  def __init__(self, descr) :
    self.pj = libproj.pj_init_plus(ctypes.c_char_p(descr.encode()))
    if not self.pj :
      print("proj system initialization failed : %s" % descr)
  def __del__(self) :
    if self.pj :
      libproj.pj_free(self.pj)

def transform(systemFrom, systemTo, a, b) :
  pa = ctypes.c_double(a)
  pb = ctypes.c_double(b)
  libproj.pj_transform(systemFrom.pj, systemTo.pj, 1, 1, ctypes.byref(pa), ctypes.byref(pb), nullptr)
  return pa.value, pb.value
