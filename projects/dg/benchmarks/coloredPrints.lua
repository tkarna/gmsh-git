--[[
  Lua File to include in other lua file in order to have colored prints:
  - Classical colors: printColor("string1", var1, "string2", var2, ..., "stringN", varN)
  - Colors in function of which proccessor is working: MPIprint("string1", var1, "string2", var2, ..., "stringN", varN)
--]]

black = '\27[30m'
red = '\27[31m'
green = '\27[32m'
yellow = '\27[33m'
blue = '\27[34m'
magenta = '\27[35m'
cyan = '\27[36m'
white = '\27[37m'

function MPIprint(...)
  if Msg:getCommRank()==0 then
    io.write(red)
  elseif Msg:getCommRank()==1 then
    io.write(blue)
  elseif Msg:getCommRank()==2 then
    io.write(green)
  elseif Msg:getCommRank()==3 then
    io.write(magenta)
  elseif Msg:getCommRank()==4 then
    io.write(yellow)
  elseif Msg:getCommRank()==5 then
    io.write(cyan)
  else 
    io.write(black)
  end
  for j=1, #arg do
   io.write(arg[j]..'\t')
  end
  io.write(black..'\n')
  j=0
end


function printBlue(...)
  io.write(blue)
  for i=1, #arg do
   io.write(arg[i]..'\t')
  end
  io.write(black..'\n')
  i=0
end

function printRed(...)
  io.write(red)
  for i=1, #arg do
   io.write(arg[i]..'\t')
  end
  io.write(black..'\n')
  i=0
end

function printGreen(...)
  io.write(green)
  for i=1, #arg do
   io.write(arg[i]..'\t')
  end
  io.write(black..'\n')
  i=0
end

function printYellow(...)
  io.write(yellow)
  for i=1, #arg do
   io.write(arg[i]..'\t')
  end
  io.write(black..'\n')
  i=0
end

function printCyan(...)
  io.write(cyan)
  for i=1, #arg do
   io.write(arg[i]..'\t')
  end
  io.write(black..'\n')
  i=0
end

function printMagenta(...)
  io.write(magenta)
  for i=1, #arg do
   io.write(arg[i]..'\t')
  end
  io.write(black..'\n')
  i=0
end
