from gmshpy import *
from dgpy import *
import time
import math

model = GModel()
model.load ('aquaplanet2.msh')
order = 3
dimension = 2
spherical = true
groups = dgGroupCollection(model, dimension, order, spherical)
groups.splitGroupsByPhysicalTag();


def initial_condition(FCT,XYZ):
  thetac = 0
  lambdc = 3*math.pi/2.0
  alpha = 0.0
  R = 6371220
  for i in range(0,XYZ.size1()):
    xi = XYZ.get(i,0)
    beta = XYZ.get(i,1)
    x = 4*R*R*xi/(4*R*R+xi*xi + beta*beta)
    y = 4*R*R*beta/(4*R*R+xi*xi + beta*beta)
    z = R *(4*R*R-xi*xi-beta*beta)/(4*R*R+xi*xi + beta*beta)
    #theta = math.acos(z/math.sqrt(x*x + y*y + z*z))
    theta = math.asin(z/R)
    lambd = math.atan2(y,x)
    a = R/3
    r = R*math.acos(math.sin(thetac)*math.sin(theta)+math.cos(thetac)*math.cos(theta)*math.cos(lambd-lambdc))
    #if (FCT.getDataCacheMap().asDgDataCacheMap().getGroupOfElements().getPhysicalTag() == "Top"):
    if(r < a):
      print r
      #FCT.set(i,0, math.exp(-10*((x/6371220)*(x/6371220)+(y/6371220)*(y/6371220))))
      FCT.set(i,0,500*(1+math.cos(math.pi*r/a)))
    else:
      FCT.set(i,0, 0)
      
    #else:
      #FCT.set(i,0, 0)

    FCT.set(i,1, 0)
    FCT.set(i,2, 0)


claw = dgConservationLawShallowWater2d()
print math.pi
solution = dgDofContainer(groups, claw.getNbFields())
XYZ = getFunctionCoordinates();
f4 = functionConstant([1000])
FS2 = functionPython(3, initial_condition, [XYZ])
solution.L2Projection(FS2)
claw.setIsLinear(0)
claw.setBathymetry(f4)
f2 = functionConstant([0])
claw.setLinearDissipation(f2)
f5 = functionConstant([0,0,0])
claw.setBathymetryGradient(f5)
#bndWall = claw.newBoundaryWall()
#claw.addBoundaryCondition('Wall', bndWall)
groups.buildGroupsOfInterfaces()
solution.exportMsh('init0.5', 0., 0)
rk=dgRungeKutta()


t=0
nbExport = 0
tic = time.clock()
for i in range (1,50000) :
  dt = 100
  norm = rk.iterate44ThreeEight(claw,t,dt,solution)
  t = t +dt
  print i
  if ( i%10 ==0 ):
     print('|ITER|',i,'|NORM|',norm,'|DT|',dt, '|T|',t)
     solution.exportMsh("solution0.5-%06d" % (i), t, nbExport)
     nbExport = nbExport + 1
print (time.clock()-tic)
Msg.Exit(0)
