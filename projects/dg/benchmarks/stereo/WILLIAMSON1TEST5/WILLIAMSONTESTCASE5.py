from gmshpy import *
from dgpy import *
import time
import math

model = GModel()
model.load ('aquaplanet2.msh')
order = 3
dimension = 2
groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag();


def initial_condition(FCT,XYZ):
  thetac = math.pi/6
  lambdc = -math.pi/2.0
  Omega = 7.292e-5
  alpha = 0.0
  h0 = 2.94e4/9.81
  R = 6371220
  a = math.pi/9
  u0 = 2*math.pi*R/(3600*24*12)
  rev = 1;
  for i in range(0,XYZ.size1()):
    xi = XYZ.get(i,0)
    beta = XYZ.get(i,1)
    x = 4*R*R*xi/(4*R*R+xi*xi + beta*beta)
    y = 4*R*R*beta/(4*R*R+xi*xi + beta*beta)
    if (FCT.getDataCacheMap().asDgDataCacheMap().getGroupOfElements().getPhysicalTag() == "Bottom"):
     rev = -1;
    z = R *(4*R*R-xi*xi-beta*beta)/(4*R*R+xi*xi + beta*beta) * rev;
    theta = math.asin(z/R)
    lambd = math.atan2(y,x)
    r = math.sqrt(a**2)
    if (r > math.sqrt((lambd-lambdc)**2 + (theta+thetac)**2)):
      r = math.sqrt((lambd-lambdc)**2 + (theta+thetac)**2)
      
    FCT.set(i,0,-(R*Omega*u0 + u0*u0/2) / 9.81* (math.sin(theta))**2)
    FCT.set(i,1, -math.sin(lambd)*u0*math.cos(theta))
    FCT.set(i,2,math.cos(lambd)*u0*math.cos(theta))
    
def coriolis(FCT,XYZ):
  alpha = 0
  Omega = 7.292e-5
  R = 6371220
  for i in range(0,XYZ.size1()):
    xi = XYZ.get(i,0)
    beta = XYZ.get(i,1)
    x = 4*R*R*xi/(4*R*R+xi*xi + beta*beta)
    y = 4*R*R*beta/(4*R*R+xi*xi + beta*beta)
    z = R *(4*R*R-xi*xi-beta*beta)/(4*R*R+xi*xi + beta*beta)
    #if (FCT.getDataCacheMap().asDgDataCacheMap().getGroupOfElements().getPhysicalTag() == "Bottom"):
     #rev = -1;
    theta = math.asin(z/R)
    lambd = math.atan2(y,x)
    FCT.set(i,0,2*Omega*math.sin(theta))
    
def bath(FCT,XYZ):
  thetac = math.pi/6
  lambdc = -math.pi/2.0
  alpha = 0
  Omega = 7.292e-5
  h0 = 2.94e4/9.81
  R = 6371220
  a = math.pi/9
  u0 = 2*math.pi*R/(3600*24*12)
  rev = 1;
  for i in range(0,XYZ.size1()):
    xi = XYZ.get(i,0)
    beta = XYZ.get(i,1)
    x = 4*R*R*xi/(4*R*R+xi*xi + beta*beta)
    y = 4*R*R*beta/(4*R*R+xi*xi + beta*beta)
    z = R *(4*R*R-xi*xi-beta*beta)/(4*R*R+xi*xi + beta*beta)
    if (FCT.getDataCacheMap().asDgDataCacheMap().getGroupOfElements().getPhysicalTag() == "Bottom"):
     rev = -1;
    z = R *(4*R*R-xi*xi-beta*beta)/(4*R*R+xi*xi + beta*beta) * rev;
    theta = math.asin(z/R)
    lambd = math.atan2(y,x)
    r = math.sqrt(a**2)
    if (r > math.sqrt((lambd-lambdc)**2 + (theta+thetac)**2)):
      r = math.sqrt((lambd-lambdc)**2 + (theta+thetac)**2)
    FCT.set(i,0,5400 - 2000 * (1-r/a))


claw = dgConservationLawShallowWater2d()
solution = dgDofContainer(groups, claw.getNbFields())
XYZ = getFunctionCoordinates();
f4 = functionPython(1, bath, [XYZ])
f10 = functionConstant([0,0,0]) 
FS2 = functionPython(3, initial_condition, [XYZ])
solution.L2Projection(FS2)
f0 = functionPython(1, coriolis, [XYZ])
claw.setCoriolisFactor(f0)
claw.setIsLinear(0)
claw.setBathymetry(f4)
f2 = functionConstant([0])
claw.setLinearDissipation(f2)
f5 = functionConstant([0,0,0])
claw.setBathymetryGradient(f5)
#bndWall = claw.newBoundaryWall()
#claw.addBoundaryCondition('Wall', bndWall)
groups.buildGroupsOfInterfaces()
solution.exportMsh('init2', 0., 0)
rk=dgRungeKutta()


t=0
nbExport = 0
tic = time.clock()
#steady = dgSteady(claw, dof)
#steady.solve(solution)
for i in range (1,5000) :
  #print math.pi
  print i
  dt = 200
  norm = rk.iterate44ThreeEight(claw,t,dt,solution)
  t = t +dt
  if ( i%10 ==0 ):
   print('|ITER|',i,'|NORM|',norm,'|DT|',dt, '|T|',t)
   solution.exportMsh("solution2-%06d" % (i), t, nbExport)
   nbExport = nbExport + 1
print (time.clock()-tic)
Msg.Exit(0)
