#!/usr/bin/env python
from dgpy import *
from gmshpy import *
import math
def extrudeVerticesAndMElements(entity, entityn) :
  numVertices = entity.getNumMeshVertices()
  for iV in range(numVertices) :
    v = entity.getMeshVertex(iV)
    x = v.x()
    y = v.y()
    z = v.z()
    r = math.sqrt(x * x + y * y + z * z)
    vn = MVertex(math.atan2(y,x), math.asin(z/r), 0)
    vn.thisown = 0
    vn.setPolynomialOrder(v.getPolynomialOrder())
    entityn.addMeshVertex(vn)
    mvertex2mvertex[v.getNum()] = vn
  numElements = entity.getNumMeshElements()
  for iE in range(numElements) :
    a = 0
    e = entity.getMeshElement(iE)
    partition = e.getPartition()
    numVertices = e.getNumVertices()
    verticesn = []
    eType = e.getType()
    order = e.getPolynomialOrder()
    v0 = mvertex2mvertex[e.getVertex(0).getNum()]
    v1 = mvertex2mvertex[e.getVertex(1).getNum()]
    v2 = mvertex2mvertex[e.getVertex(2).getNum()]
      
    j = ((v1.x()-v0.x())*(v2.y()-v0.y()) - (v1.y()-v0.y())*(v2.x()-v0.x()));
    if j > 0:
        for iVV in range(numVertices) :
          newv2 = mvertex2mvertex[e.getVertex(iVV).getNum()]
          if(newv2.x() > 0):
            a = a+1
    #print j
    #print a
    for iV in range(numVertices) :
      newv = mvertex2mvertex[e.getVertex(iV).getNum()]
      if(j > 0 and a >= numVertices/2 and newv.x() < 0):
        newv = MVertex(newv.x() + 2 * math.pi, newv.y(), 0)
        newv.thisown = 0
        newv.setPolynomialOrder(e.getVertex(iV).getPolynomialOrder())
        entityn.addMeshVertex(newv)
        print (v0.x(),v1.x(),v2.x())
      if(j > 0 and a < numVertices/2 and newv.x() > 0):
        newv = MVertex(newv.x() - 2 * math.pi, newv.y(), 0)
        newv.thisown = 0
        newv.setPolynomialOrder(e.getVertex(iV).getPolynomialOrder())
        entityn.addMeshVertex(newv)
        print (v0.x(),v1.x(),v2.x())
      verticesn.append(newv)
          
    if eType == 3 :
      if order == 1:
        newTri = MTriangle(verticesn, e.getNum())
      elif order == 2:
        newTri = MTriangle6(verticesn, e.getNum())
      else:
        newTri = MTriangleN(verticesn, order, e.getNum())
      newTri.setPartition(partition)
      newTri.thisown = 0
      entityn.addTriangle(newTri)

model = GModel()
model.load("aquaplanet.geo")
model.load("aquasphere.msh")
emodel = GModel()
vertex2vertex = {}
edge2edge = {}
mvertex2mvertex = {}

gvertices = model.bindingsGetVertices()
gedges = model.bindingsGetEdges()
gfaces = model.bindingsGetFaces()

for i in range(len(gvertices)) :
  v = gvertices[i]
  vn = emodel.addVertex(v.x(), v.y(), v.z(), 1)
  vn.addPhysicalEntity(1)
  vertex2vertex[v.tag()] = vn
  extrudeVerticesAndMElements(v, vn)
for i in range(len(gedges)) :
  e = gedges[i]
  en = emodel.addLine(vertex2vertex[e.getBeginVertex().tag()], vertex2vertex[e.getEndVertex().tag()])
  en.addPhysicalEntity(2)
  edge2edge[e.tag()] = en
  extrudeVerticesAndMElements(e, en)
for i in range(len(gfaces)) :
  f = gfaces[i]
  edges = f.edges()
  edgesn = []
  f.addPhysicalEntity(3)
  for j in range(len(edges)) :
    e = edges[j]
    edgesn.append(edge2edge[e.tag()])
  fn = emodel.addPlanarFace([edgesn])
  extrudeVerticesAndMElements(f, fn)

print(emodel.getNumMeshVertices())
print(emodel.getNumMeshElements())
emodel.writeMSH("aquasphere2_lonlat.msh", 2.2, False, True)
print("ok")
