from gmshpy import *

def projectMesh (model):
  def project(entity):
    numVertices = entity.getNumMeshVertices()
    tag = entity.getPhysicalEntities()[0]
    dim = entity.dim()
    if (g.getPhysicalName(dim, tag) == "Bottom") :
      rev = -1
    else:
      rev = 1
    for iV in range(0,numVertices): 
      R = 6371220
      v = entity.getMeshVertex(iV)
      alpha = v.x()
      beta = v.y()
      R = R + v.z()
      sqrtJ = 2*R*2*R / (2*R*2*R + alpha ** 2 + beta ** 2)
      x = sqrtJ * alpha
      y = sqrtJ * beta
      z = (2*R*2*R - alpha ** 2 - beta ** 2) * sqrtJ / (4*R)
      v.setXYZ(x, y, z * rev)

  gvertices = model.bindingsGetVertices()
  for v in gvertices:
    project(v)
  
  gedges = model.bindingsGetEdges()
  for e in gedges:
    project(e)

  gfaces = model.bindingsGetFaces()
  for f in gfaces:
    project(f)

  gregions = model.bindingsGetRegions()
  for h in gregions:
    project(h)
