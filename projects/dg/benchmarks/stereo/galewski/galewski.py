from dgpy import *
import time
from math import *
import time,os,sys
from Project import *

model = GModel()
order = 5
dimension = 2
clscale=0.3
R = 6371220

#if (Msg.GetCommRank()==0):
#  m = GModel()
#  m.load("aquaplanet.geo")
#  GmshSetOption('Mesh','CharacteristicLengthFactor', clscale)
#  GmshSetOption('Mesh','Algorithm', "frontal")
#  m.mesh(dimension)
#  m.setOrderN(order,False,False)
#  m.save("aquaplanet.msh")
#  m.load("aquaplanet.msh")
#  projectMesh(m)
#  m.save("aquasphere.msh")
Msg.Barrier()
model.load("aquaplanet.msh")



groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsByPhysicalTag();
XYZ = groups.getFunctionCoordinates();
claw = dgConservationLawShallowWater2d()
solution = dgDofContainer(groups, claw.getNbFields())

try : os.mkdir("output")
except: 0;

#We're on the sphere
claw.setIsSpherical(R)

#Bathymetry
bath = functionConstant([10000])
claw.setBathymetry(bath)
grad_bath = functionConstant([0,0,0])
claw.setBathymetryGradient(grad_bath)

#Coriolis
if (Msg.GetCommRank()==0):
  functionC.buildLibraryFromFile ("ccode.cc", "ccode.so");
Msg.Barrier()
coriolis = functionC("ccode.so", "coriolis", 1, [XYZ])
claw.setCoriolisFactor(coriolis)
initial = functionC("ccode.so", "galewski", 3, [XYZ])
solution.interpolate(initial)

#Export initial condition
def u_cross_r_3D(F, vel3d, XYZ):
    for i in range(0,vel3d.size1()):
        rev=1
        xi = XYZ.get(i,0)
        beta = XYZ.get(i,1)
        x = 4*R*R*xi/(4*R*R+xi*xi + beta*beta)
        y = 4*R*R*beta/(4*R*R+xi*xi + beta*beta)
        if (F.getDataCacheMap().getGroupOfElements().getPhysicalTag() == "Bottom"):
            rev = -1;
        z = R *(4*R*R-xi*xi-beta*beta)/(4*R*R+xi*xi + beta*beta) * rev

        u = vel3d.get(i,0)
        v = vel3d.get(i,1)
        w = vel3d.get(i,2)

        a=sqrt(x*x+y*y+z*z);

        F.set(i,0,v*z/a-w*y/a)
        F.set(i,1,w*x/a-u*z/a)
        F.set(i,2,u*y/a-v*x/a)    
def u_cross_r_st(F, sol, grad_sol, XYZ):
  for i in range(0,grad_sol.size1()):
    xi = XYZ(i,0)
    eta = XYZ(i,1)
    J = 4*R*R/(4*R*R+xi*xi+eta*eta)
    dudy = grad_sol.get(i, 4)
    dvdx = grad_sol.get(i, 6)
    x = XYZ.get(i, 0)
    y = XYZ.get(i, 1)
    u = sol.get(i, 1)
    v = sol.get(i, 2)
    F.set(i,0,(grad_sol.get(i,6)-grad_sol.get(i,4))/J + (x * v - y * u) / (2 * R * R))
    
def vorticity(F, u_cross_r_grad, XYZ):
    for i in range(0,u_cross_r_grad.size1()):
        xi = XYZ(i,0)
        eta = XYZ(i,1)
        J = 4*R*R/(4*R*R+xi*xi+eta*eta)
        d1dxi=u_cross_r_grad.get(i,0)/J
        d2deta=u_cross_r_grad.get(i,4)/J
        div=d1dxi+d2deta
        F.set(i,0,div/J)
#        div=div-(alphabeta_3D(i,0) xi*/(2.0*R*R)
#      F.set(i,0,vorticity_grad.get(i,2)/J)

def elevation(F, sol):
    for i in range(0,sol.size1()):
        F.set(i,0,sol(i,0))
def velocity3D(F, sol, XYZ):
    R = 6371220
    rev = 1
    for i in range(0,sol.size1()):
        if (F.getDataCacheMap().getGroupOfElements().getPhysicalTag() == "Bottom"):
            rev = -1;
        xi = XYZ(i,0)
        eta = XYZ(i,1)
        vxi = sol(i,1)
        veta = sol(i,2)
        ux = (1-(2*xi**2)/(4*R*R+xi*xi + eta*eta)) * vxi - 2*eta*xi/(4*R*R+xi*xi + eta*eta) * veta
        uy = - 2*eta*xi/(4*R*R+xi*xi + eta*eta) * vxi + (1-(2*eta**2)/(4*R*R+xi*xi + eta*eta)) * veta
        uz = -4*R*xi*rev/(4*R*R+xi*xi + eta*eta) * vxi - 4*R*eta*rev/(4*R*R+xi*xi + eta*eta) * veta
        F.set(i,0,ux)
        F.set(i,1,uy)
        F.set(i,2,uz)
#def alphabeta3D(F, XYZ):
#    R = 6371220
#    rev = 1
#    for i in range(0,sol.size1()):
#        if (F.getDataCacheMap().getGroupOfElements().getPhysicalTag() == "Bottom"):
#            rev = -1;
#        xi = XYZ(i,0)
#        eta = XYZ(i,1)
#        vxi = xi
#        veta = eta
#        ux = (1-(2*xi**2)/(4*R*R+xi*xi + eta*eta)) * vxi - 2*eta*xi/(4*R*R+xi*xi + eta*eta) * veta
#        uy = - 2*eta*xi/(4*R*R+xi*xi + eta*eta) * vxi + (1-(2*eta**2)/(4*R*R+xi*xi + eta*eta)) * veta
#        uz = -4*R*xi*rev/(4*R*R+xi*xi + eta*eta) * vxi - 4*R*eta*rev/(4*R*R+xi*xi + eta*eta) * veta
#        F.set(i,0,ux)
#        F.set(i,1,uy)
#        F.set(i,2,uz)
vel3d = functionPython(3, velocity3D, [ solution.getFunction(), XYZ])
eta = functionPython(1, elevation, [ solution.getFunction()])
u_cross_r = functionPython(3,u_cross_r_3D,[ vel3d, XYZ])
u_cross_rDof = dgDofContainer (groups, 3)
u_cross_rDof.interpolate(u_cross_r)
#alphabeta_3D = functionPython(3, alphabeta3D, [XYZ])
vort = functionPython(1, vorticity, [ u_cross_rDof.getFunctionGradient(),  XYZ])

vortST = functionPython(1,u_cross_r_st,[solution.getFunction(), solution.getFunctionGradient(), XYZ])

petscIm = linearSystemPETScBlockDouble()
dofIm = dgDofManager.newDGBlock(groups, claw.getNbFields(), petscIm)
petscEx = linearSystemPETScBlockDouble()
dofEx = dgDofManager.newDGBlock(groups, claw.getNbFields(), petscEx)

#timeIter = dgERK(claw, dofEx, DG_ERK_44)
timeIter = dgIMEXRK(claw, dofIm, 2, dofEx)     #timeorder

claw.setLinearFilterMode(True)
dt=claw.getMinOfTimeSteps(solution)

Ti=0
Tf=518400
nbSteps = int(ceil((Tf-Ti)/dt))
nbExport=0
t=0
if (Msg.GetCommRank()==0):
  print "Time step:",dt
  print "Number of steps:",nbSteps
for i in range (0,nbSteps) :
    if (i==nbSteps-1):
        dt=Tf-t
    if ( i==0 or i%100 ==0 ):
      if (Msg.GetCommRank()==0):
        print "\nExport",nbExport,"at time",t,"and step",i,"over",nbSteps
      solution.exportFunctionMsh(vel3d,"output/vel_%i"% (nbExport), t, i, "velocity") 
      solution.exportFunctionMsh(eta,"output/eta_%i"% (nbExport), t, i, "eta") 
      u_cross_rDof.interpolate(u_cross_r)
      solution.exportFunctionMsh(vort,"output/vort_%i"% (nbExport), t, i, "vorticity") 
      solution.exportFunctionMsh(vortST,"output/vortSt_%i"% (nbExport), t, i, "vorticitySt") 
      nbExport = nbExport + 1
    timeIter.iterate(solution,dt,t)
    t = t +dt
    if (Msg.GetCommRank()==0):
      sys.stdout.write('.')
    sys.stdout.flush()

if (Msg.GetCommRank()==0):
  print "\n---------------------------"
  print "| Program ended correctly |"
  print "---------------------------"

