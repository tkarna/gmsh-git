//Written in a strange way. Imported from MUSE with expression template stuff

#include "fullMatrix.h"
#include "function.h"
#include "math.h"
#include "dgGroupOfElements.h"
extern "C" {
#include <stdlib.h>

  double R = 6371220.;
  double pi=M_PI;
  double Omega = 7.292e-5;

  double coriolis_lat(double lat){
    return 2*Omega*sin(pi/180*lat);
  }

  void coriolis (dataCacheMap *, fullMatrix<double> &FCT, fullMatrix<double> &stereo) {
    double alpha = 0;
    for (size_t i = 0; i< FCT.size1(); i++) {
      double xi = stereo(i,0);
      double beta = stereo(i,1);
      FCT.set(i,0,2*Omega*(4*R*R-xi*xi-beta*beta)/(4*R*R+xi*xi + beta*beta));
    }
  }
  void galewski_vel_coord(double lambda,double phi, double phi0, double phi1, double umax, double en, double &u_lambda, double &u_phi) {
    double local_d;
    u_lambda=(pi/180*phi-phi0);
    u_lambda=u_lambda*(pi/180*phi-phi1);
    u_lambda=1.0/u_lambda;
    u_lambda=exp(u_lambda);
    u_lambda=umax/en*u_lambda;
    if (phi*pi/180 <= phi0 || phi*pi/180 >= phi1)
      u_lambda=0.0;
    u_phi=0;
  }

  void galewski (dataCacheMap *m, fullMatrix<double> &FCT, fullMatrix<double> &stereo) {
    double phi0,phi1,umax,en,h0,a,phi2,alpha,beta_gw,h_hat;
    phi0=pi/7;
    phi1=pi/2-phi0;
    umax=80;
    en=exp(-4.0/((phi1-phi0)*(phi1-phi0)));
    h0=10158.2950420088891;
    a=6.37122e6;
   
    alpha=1.0/3.0;
    beta_gw=1.0/15.0;
    h_hat=120.0;

    int nx;
    double lat0, lat1, dlat;
    nx=5000;
    lat0=phi0*180/pi;
    lat1=phi1*180/pi;
    dlat=(lat1-lat0)/(nx-1);
    double *height_line=(double*)malloc(nx * sizeof(double));

    //Computing the height along a line, from phi0 to phi1
    height_line[0]=h0;
    double integrand[2];
    integrand[0]=0;
    double lat_pt=lat0;
    for (int i=1; i<nx; ++i){
      lat_pt=lat0+i*dlat;
      //Compute coriolis with the given latitude
      double coriolis=coriolis_lat(lat_pt);
      //Compute velocity with the given latitude
      double u_lambda,u_phi;
      galewski_vel_coord( 0.0, lat_pt, phi0, phi1, umax, en, u_lambda, u_phi);
      //Integrate
      integrand[1]=a*u_lambda*(coriolis+tan(lat_pt*pi/180.0)/a*u_lambda);
      height_line[i]=height_line[i-1]-(integrand[0]+integrand[1])/2*dlat*pi/180.0/9.80616;
      integrand[0]=integrand[1];
      //printf("i%i\n",height_line[i]);
    }

    for (size_t i = 0; i< FCT.size1(); i++) {
      double u_lambda,u_phi;
      int rev = 1;
      //coordinates
      double xi =stereo(i,0);
      double beta =stereo(i,1);
      double x = 4*R*R*xi/(4*R*R+xi*xi + beta*beta);
      double y = 4*R*R*beta/(4*R*R+xi*xi + beta*beta);
      const dgGroupOfElements *group = m->getGroupOfElements();
       if (group->getPhysicalTag() == "Bottom")
	rev = -1;
      double z = R *(4*R*R-xi*xi-beta*beta)/(4*R*R+xi*xi + beta*beta) * rev;
           
      //Change the coordinates
      double lambda=180.0/pi*(atan2(y,x));
      double phi=180/pi*asin(z/sqrt(x*x+y*y+z*z)); // asin(z/r)*180/pi

      double height;
      //Elevation
      if (phi<lat0)
	height=h0;
      else if (phi>lat1)
	height=height_line[nx-1];
      else{
	int index=floor((phi-lat0)/dlat);
	double phi_idx1=lat0+index*dlat;
	double phi_idx2=lat0+(index+1)*dlat;
	double h_idx1=height_line[index];
	double h_idx2=height_line[index+1];
	//printf("lat %e h1 %e h2 %e p1 %e p2 %e\n",phi, h_idx1,h_idx2,phi_idx1,phi_idx2);
	height=h_idx1+(h_idx2-h_idx1)*(phi-phi_idx1)/(phi_idx2-phi_idx1);
      }
      bool have_bump=true;
      double e1,e2,bump;
      phi2=pi/4.0;
      if (have_bump){
      	e1=lambda*(pi/180.0/alpha)*lambda*(-pi/180.0/alpha); //-(lambda/alpha)^2
      	e1=exp(e1); //exp(-(lambda/alpha)^2)
      	e2=phi*(pi/180.0);
      	e2=(phi2-e2)*(1.0/beta_gw); // (phi2-phi)/beta
      	e2=e2*e2*(-1.0); // -[(phi2-phi)/beta]^2
      	e2=exp(e2); //exp(-[(phi2-phi)/beta]^2)
      	bump=phi*(pi/180.0);
      	bump=cos(bump); //cos(phi)
      	bump=h_hat*bump*e1*e2;
      	height=height+bump;
      }
      //Remove mean height
      height=height-10000;
      
      FCT.set(i,0,height);

      //Velocity
      lambda=lambda+180;
      galewski_vel_coord( lambda, phi, phi0, phi1, umax, en, u_lambda, u_phi);

      //To stereographic coordinates
      lambda=lambda/180*pi;
      phi=phi/180*pi;
      FCT.set(i,1,sin(lambda)*u_lambda - (cos(lambda)*sin(phi)+x*rev*cos(phi)/(R+z*rev))*u_phi);
      FCT.set(i,2,-cos(lambda)*u_lambda - (sin(lambda)*sin(phi)+y*rev*cos(phi)/(R+z*rev))*u_phi);
    }

    free(height_line);
  }
  
  
  
}
