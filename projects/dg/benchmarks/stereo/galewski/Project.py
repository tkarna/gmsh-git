from gmshpy import *

def projectMesh (model):
  def project(entity):
    rev = 1
    numVertices = entity.getNumMeshVertices()
    if len(entity.getPhysicalEntities()) != 0:
      tag = entity.getPhysicalEntities()[0]
      dim = entity.dim()
      if (model.getPhysicalName(dim, tag) == "Bottom") :
        rev = -1
    m = fullMatrixDouble (3,3)
    for iV in range(0,numVertices): 
      v = entity.getMeshVertex(iV)
      alpha = v.x()
      beta = v.y()
      sqrtJ = 2*6371220*2*6371220 / (2*6371220*2*6371220 + alpha ** 2 + beta ** 2)
      x = sqrtJ * alpha
      y = sqrtJ * beta
      z = (2*6371220*2*6371220 - alpha ** 2 - beta ** 2) * sqrtJ / (4*6371220)
      v.setXYZ(x, y, z * rev)
    #if (rev == -1) :
    #  numElements = entity.getNumMeshElements()
    #  for iE in range(0,numElements) :
    #    el = entity.getMeshElement(iE)
    #    if (el.getDim() == 2) :
    #      el.revert()

  gvertices = model.bindingsGetVertices()
  for v in gvertices:
    project(v)
  
  gedges = model.bindingsGetEdges()
  for e in gedges:
    project(e)

  gfaces = model.bindingsGetFaces()
  for f in gfaces:
    project(f)

g = GModel()
g.load("aquaplanet.msh")
projectMesh(g)
g.save("aquasphere.msh")
