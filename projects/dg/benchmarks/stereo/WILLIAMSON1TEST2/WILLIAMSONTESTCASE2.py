from gmshpy import *
from dgpy import *
import time
import math

model = GModel()
model.load ('aquaplanet1.msh')
order = 3
dimension = 2
groups = dgGroupCollection(model, dimension, order)


groups.splitGroupsByPhysicalTag();


def error(F, analyticalSolution, solution):
  for i in range(0,solution.size1()):
    F.set(i,0,analyticalSolution(i,0)-solution(i,0))


def velocity3D(F, sol, XYZ):
  R = 6371220
  rev = 1;
  for i in range(0,sol.size1()):
    if (F.getDataCacheMap().asDgDataCacheMap().getGroupOfElements().getPhysicalTag() == "Bottom"):
     rev = -1;
    xi = XYZ(i,0);
    eta = XYZ(i,1);
    x = 4*R*R*xi/(4*R*R+xi*xi + eta*eta)
    y = 4*R*R*eta/(4*R*R+xi*xi + eta*eta)
    z = R *(4*R*R-xi*xi-eta*eta)/(4*R*R+xi*xi + eta*eta) * rev;
    ux = sol(i,1)*1/R*(z*rev+y*y/(R+z*rev)) - sol(i,2)*(x*y)/R/(R+z*rev)
    uy = -sol(i,1)*(x*y)/R/(R+z*rev) + sol(i,2)*1/R*(z*rev+x*x/(R+z*rev))
    uz = - sol(i,1)*(x/R)- sol(i,2)*(y/R)
    lambd = math.atan2(y,x)
    theta = math.asin(z/R)
    F.set(i,0,math.sqrt(ux**2 + uy**2 + uz**2))
    #F.set(i,1,uy)
    #F.set(i,2,uz)
    

def initial_condition(FCT,XYZ):
  Omega = 7.292e-5
  R = 6371220
  for i in range(0,XYZ.size1()):
    rev = 1
    xi = XYZ.get(i,0)
    beta = XYZ.get(i,1)
    x = 4*R*R*xi/(4*R*R+xi*xi + beta*beta)
    y = 4*R*R*beta/(4*R*R+xi*xi + beta*beta)
    if(FCT.getDataCacheMap().asDgDataCacheMap().getGroupOfElements().getPhysicalTag()== "Bottom"):
     rev = -1;
    z = R *(4*R*R-xi*xi-beta*beta)/(4*R*R+xi*xi + beta*beta) * rev;
    u0 = 2*math.pi*R/(3600*24*12)#*2*R/(R+z*rev)
    theta = math.asin(z/R)
    lambd = math.atan2(y,x)
    FCT.set(i,0,-(R*Omega*u0 + u0*u0/2) / 9.80616* (math.sin(theta))**2)
    FCT.set(i,1,  -math.sin(lambd)*u0*math.cos(theta))
    FCT.set(i,2, math.cos(lambd)*u0*math.cos(theta))
    
def coriolis(FCT,XYZ):
  alpha = 0
  Omega = 7.292e-5
  R = 6371220
  for i in range(0,XYZ.size1()):
    xi = XYZ.get(i,0)
    beta = XYZ.get(i,1)
    x = 4*R*R*xi/(4*R*R+xi*xi + beta*beta)
    y = 4*R*R*beta/(4*R*R+xi*xi + beta*beta)
    if(FCT.getDataCacheMap().asDgDataCacheMap().getGroupOfElements().getPhysicalTag()== "Bottom"):
     rev = -1;
    z = R *(4*R*R-xi*xi-beta*beta)/(4*R*R+xi*xi + beta*beta)
    theta = math.asin(z/R)
    lambd = math.atan2(y,x)
    FCT.set(i,0,2*Omega*math.sin(theta))
    


claw = dgConservationLawShallowWater2d()
solution = dgDofContainer(groups, claw.getNbFields())
XYZ = getFunctionCoordinates();
f9 = functionConstant([2.94e4/9.80616]) 
f10 = functionConstant([0,0,0]) 
FS2 = functionPython(3, initial_condition, [XYZ])
Ferror = functionPython(1, error, [FS2, solution.getFunction()])
Fvel = functionPython(1, velocity3D, [solution.getFunction(), XYZ])
solution.interpolate(FS2)
f0 = functionPython(1, coriolis, [XYZ])
claw.setCoriolisFactor(f0)
claw.setIsLinear(1)
claw.setIsSpherical(1)
claw.setBathymetry(f9)
f2 = functionConstant([0])
claw.setLinearDissipation(f2)
f5 = functionConstant([0,0,0])
claw.setBathymetryGradient(f5)
print "TEST1"
#groups.buildGroupsOfInterfaces()
print "TEST2"
solution.exportMsh('init1', 0., 0)
solution.exportFunctionMsh(Fvel,"initvel", 0.,0,"vel") 
solution.exportFunctionMsh(Ferror,"initerr", 0.,0,"errorl") 
rk=dgRungeKutta()
print "TEST"
t=0
nbExport = 0
tic = time.clock()
for i in range (1,5000) :
  dt = 100
  print i
  norm = rk.iterate44ThreeEight(claw,t,dt,solution)
  t = t +dt
  if ( i%10 ==0 ):
   print('|ITER|',i,'|NORM|',norm,'|DT|',dt, '|T|',t)
   solution.exportMsh("solution1-%06d" % (i), t, nbExport)
   solution.exportFunctionMsh(Ferror,"error-%06d" % (i), t, nbExport, "error") 
   solution.exportFunctionMsh(Fvel,"vel-%06d" % (i), t, nbExport, "vel") 
   nbExport = nbExport + 1
print (time.clock()-tic)
Msg.Exit(0)

