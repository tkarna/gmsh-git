from dgpy import *
from InputCongo import *
from termcolor import colored
import time, math, os, sys

def loadAndPartitionMesh(model, fileName, ng, algo) :
  if (Msg.GetCommSize() > 1) :
    if(Msg.GetCommRank() == 0) :
      print ''
      print(colored('********** Partition mesh with algorithm %d **********', "red")%(algo))
      print ''
      m2 = GModel()
      m2.load(fileName+'_tan' + '.msh')
      pOpt = meshPartitionOptions()
      pOpt.setNumOfPartitions(Msg.GetCommSize())

      groups = dgGroupCollection(m2, dimension, order, True)
      xyz = groups.getFunctionCoordinates();
      lonLatDegrees = functionC(glib,"lonLatDegrees",3,[xyz])
      lonLat = functionC(glib,"lonLat",3,[xyz])
      bath = dgDofContainer(groups, 1);
      if(not os.path.exists(bathname)):
        Msg.Fatal('No valid bathymetry file :' + bathname + ' found,  please launch "rundgpy DiffuseBathymetryTsunami.py"')

      bath.readMsh(bathname)
      # Define Conservation Law
      claw = dgConservationLawShallowWater2d()
      solution = dgDofContainer(groups, claw.getNbFields())

      ld = functionConstant(0)
      Cd = functionC(glib, "bottomDrag", 1, [solution.getFunction(), bath.getFunction()])
      if(gbrDiff == 0):
        Di = functionConstant(diff)
      elif(gbrDiff == 1):
        Di = functionC(glib, "smagorinsky", 1, [solution.getFunctionGradient()])
      Coriolis = functionC(glib, "coriolis", 1, [xyz])

      claw.setCoriolisFactor(Coriolis)
      claw.setQuadraticDissipation(Cd)
      claw.setLinearDissipation(ld)
      claw.setDiffusivity(Di)
      claw.setBathymetry(bath.getFunction())
      claw.setBathymetryGradient(bath.getFunctionGradient())

      
      rk = dgMultirateERK(groups, claw, RK_TYPE)
      dt = rk.splitGroupsForMultirate(ng, solution, [solution, bath])
      rk.printMultirateInfo()

      rk.computeOptimalMeshPartition(pOpt, algo)
      PartitionMesh(m2, pOpt)
      rk.printOptimalPartitions(Msg.GetCommSize())

      m2.save(fileName + '_partitioned_tan.msh')
      print ''
      print(colored('**********       Partition Done            **********', "red"))
      print ''
    Msg.Barrier()
    model.load(fileName + '_partitioned_tan.msh')
  else :
    model.load(fileName + '_tan.msh')
