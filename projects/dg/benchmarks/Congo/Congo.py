"""
    Simulations on the GBR with multirate explicit Runge-Kutta time-stepping.
    All the parameters can be found and modified in the file "InpuGBR.py"
"""

"""
    *** How to launch the testcase ***

    Simulation on 1 processor  : rundgpy MultirateGBR.py
    Simulation on n processors : mpirun -np n rundgpy MultirateGBR.py   ( !!! You need MPI !!! )

    **********************************
"""

# Import for python
from dgpy import *
from math import *
from ProjectMesh import *
from PartitionCongo import *
from InputCongo import *
from termcolor import colored
import calendar, time, os, sys

model = GModel()
loadAndPartitionMesh(model,  filename,  mL, algo)

# Coordinate system
XYZ = function.getCoordinates();
lonLatDegrees = functionC(clib,"lonLatDegrees",3,[XYZ])
lonLat = functionC(clib,"lonLat",3,[XYZ])

latLonDegrees = functionC(clib,"latLonDegrees",3,[XYZ])
latLon = functionC(clib,"latLon",3,[XYZ])

# Mesh Parameters
order = 1
dimension = 2

groups = dgGroupCollection(model, dimension, order)

if(not os.path.exists(bathname)):
  Msg.Fatal('No valid bathymetry file :' + bathname + ' found,  please launch "rundgpy DiffuseBathCongo.py"')

bath = dgDofContainer(groups, 1);
bath.readMsh(bathname)

# ************************************************

# Initial & Final Time [year, month, hour, minutes, seconds]
Ti = calendar.timegm([2011, 12, 1, 0, 0, 0])
Tf = calendar.timegm([2011, 12, 11, 0, 0, 0])

t = Ti
timeFunction = functionConstant(t)

# ****************** Forcings ******************

# .................. Tides    ..................

tideEta = slimFunctionTpxo("h_tpxo7.2.nc","ha","hp","lon_z","lat_z")
tideEta.setCoordAndTimeFunctions(lonLatDegrees,timeFunction)
tideU = slimFunctionTpxo("u_tpxo7.2.nc","Ua","up","lon_u","lat_u")
tideU.setCoordAndTimeFunctions(lonLatDegrees,timeFunction)
tideV = slimFunctionTpxo("u_tpxo7.2.nc","Va","vp","lon_v","lat_v")
tideV.setCoordAndTimeFunctions(lonLatDegrees,timeFunction)
tideUV = functionC(clib,"latLonVector",3,[latLon,tideU,tideV])

# ..............................................

# .................. Wind     ..................

windInterpolator = slimStructDataInterpolatorMultilinear()
windUcontainer = slimStructDataContainerNetcdf("wind.nc", "U_GRD_L103", "lat", "lon")
windVcontainer = slimStructDataContainerNetcdf("wind.nc", "V_GRD_L103", "lat", "lon")
windU = slimFunctionStructData(windUcontainer, windInterpolator, latLonDegrees)
windV = slimFunctionStructData(windVcontainer, windInterpolator, latLonDegrees)
windU.setTimeFunction(timeFunction)
windV.setTimeFunction(timeFunction)
windUV = functionC(clib,"latLonVector",3,[latLon,windU,windV])

# ..............................................

# **********************************************


# ****************** Cons Law ******************

claw = dgConservationLawShallowWater2d()
claw.setIsLinear(1)
solution = dgDofContainer(groups, claw.getNbFields())

Cd = functionC(clib, "bottomDrag", 1, [function.getSolution(), bath.getFunction()])
# Diffusion (Smagorinsky)
Di = functionC(clib, "smagorinsky", 1, [function.getSolutionGradient()])
Coriolis = functionC(clib, "coriolis", 1, [XYZ])
# wind from netcdf
source = functionC(clib, "windStress", 2, [windUV, function.getSolution(), bath.getFunction()])


#claw.setCoriolisFactor(Coriolis)
#claw.setQuadraticDissipation(Cd)
#claw.setDiffusivity(Di)
claw.setSource(source)
claw.setBathymetry(bath.getFunction())
claw.setBathymetryGradient(bath.getFunctionGradient())

tideuv = functionC(clib,"transport2velocity",3,[tideUV,bath.getFunction(),function.getSolution()]);
tpxoTide = functionC(clib,"merge",3,[tideEta,tideuv]);

CCode = """
#include "fullMatrix.h"
#include "function.h"

extern "C" {

  void fluxToVelocity(dataCacheMap *m,  fullMatrix<double> &sol,  fullMatrix<double> &normals,  fullMatrix<double> &bathymetryIntegral,  fullMatrix<double> &flux) {
          
  double c = flux(0, 0)/bathymetryIntegral(0, 0);
  for (size_t i=0; i<sol.size1(); i++) {
    sol.set(i,  0,  0); //Eta
    sol.set(i,  1,  -c * normals(i, 0));
    sol.set(i,  2,  -c * normals(i, 1));
  }
 }
	void gaussian(dataCacheMap *,fullMatrix<double> &f,fullMatrix<double> &lonlat){
		for (size_t i=0; i<lonlat.size1(); i++){
			f.set(i,0,1*exp(-0.5*((lonlat(i,0)-0.213)*(lonlat(i,0)-0.213)+(lonlat(i,1)+0.12)*(lonlat(i,1)+0.12))/1e-6));
			f.set(i,1,0);
			f.set(i,2,0);
		}
	}
}
"""
tmpCLib = "tempCLib.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CCode,  tmpCLib)

riverDischargeFunction = functionConstant(10000.0)

bathIntegralFM = fullMatrixDouble(1, 1)
dgFunctionIntegratorInterface(groups,  bath.getFunction()).compute('source1',  bathIntegralFM)
#print bathIntegralFM(0, 0)
bathIntegralsFunc = functionConstant(bathIntegralFM(0, 0))
currentUV = functionC(tmpCLib,  "fluxToVelocity",  3,  [function.getNormals(),  bathIntegralsFunc,  riverDischargeFunction])

# Tide data from tpxo

#claw.addBoundaryCondition('ocean',claw.newOutsideValueBoundary("Surface", tpxoTide))
#claw.addBoundaryCondition('source1', claw.newOutsideValueBoundary("Surface", currentUV))
claw.addBoundaryCondition('coast',claw.newBoundaryWall())
claw.addBoundaryCondition('ocean',claw.newBoundaryWall())
claw.addBoundaryCondition('source1',claw.newBoundaryWall())

#solutionInit = functionC(tmpCLib, "gaussian", 3, [lonLat])
#solution.interpolate(solutionInit)

windExp = dgDofContainer(groups, 3)
windExp.interpolate(windUV)
exporterWind=dgIdxExporter(windExp, outputDir+'/wind')


# **********************************************


# ****************** Time Int ******************
rk = dgERK(claw, None, DG_ERK_44)
dt = 10#rk.splitGroupsForMultirate(mL, solution, [solution, bath])
#rk.printMultirateInfo(0)

# **********************************************

currentFunction = functionC(clib,"current",3,[function.getSolution()])
#tideCurrentFunction = functionC(clib,"current",3,[tpxoTide])

nbSteps = int(ceil((Tf-Ti)/dt))
nbExport = 0

if(Msg.GetCommRank() == 0):
  if(os.path.exists(outputDir+'/mr-gbr')):
    try : os.system("rm -r "+outputDir+'/mr-gbr');
    except: 0;
exporterSol=dgIdxExporter(solution, outputDir+'/mr-gbr')

norm=solution.norm()

mass=functionPython(1,  massIntegrator,  [functionSolution.get(), bath.getFunction()])
integrator = dgFunctionIntegrator(groups,  mass,  solution)
totalMass=fullMatrixDouble(1, 1)
totalMassRef=fullMatrixDouble(1, 1)
#integrator.compute(totalMassRef, "")

if (Msg.GetCommRank() == 0):
  print ''
  print(colored('******************** Initializing ********************', "blue"))
  print ''
  print '     - Number of processors:', Msg.GetCommSize()
  print '     - Multirate Runge Kutta Scheme:', RK_TYPE
  print '     - Initial Time (Ti):',  printDate(Ti)
  print '     - Final Time (Tf):',  printDate(Tf)
  print '     - Reference Time Step:', printTime(dt)
  print '     - Number of Iterations:', nbSteps
  print '     - Norm of Solution at Ti:',  norm
  print '     - Mass of Solution at Ti:',  totalMassRef(0, 0)
  print ''
  print(colored('******************************************************', "blue"))
  print ''

# ****************** Iterate  ******************

iter = 100
export = 100

startcpu = time.clock()
for i in range(0, nbSteps):
  if(i == nbSteps - 1):
    dt=Tf-t
  norm = rk.iterate (solution, dt, t)
  t = t +dt
#print t
  timeFunction.set(t)
  if ( i%iter == 0 ):
    norm=solution.norm()
    integrator.compute(totalMass, "")
    relMass=totalMass(0, 0)
    if(Msg.GetCommRank() == 0):
      print(colored('|ITER| %d',  "red")%(i))
      print(colored('------------------------------------------------------', "red"))
      print'|TIME|', printDate(t)
      print'|NORM|',norm
      print'|MASS|',relMass
      print'|CPUT|',printTime(time.clock() - startcpu)
      print(colored('------------------------------------------------------', "red"))
      print''
  if ( i%export  == 0 ):
    exporterSol.exportIdx(i/iter, t)
    windExp.interpolate(windUV)
    exporterWind.exportIdx(i/iter, t)
    #solution.exportFunctionMsh(currentFunction, outputDir+'/mr_gbr_current-%06d'%(i), t, nbExport, '')
    nbExport = nbExport + 1

endcpu=time.clock()
norm=solution.norm()
integrator.compute(totalMass, "")
relMass=totalMass(0, 0)
Msg.Barrier()
if (Msg.GetCommRank() == 0):
  print ''
  print(colored('********************     End      ********************', "blue"))
  print ''
  print '     - Final Time (Tf):',  printDate(t)
  print '     - Norm of Solution at Tf:',  norm
  print '     - Mass of Solution at Tf:', relMass
  print '     - Total CPU Time:', endcpu-startcpu
  print ''
  print(colored('******************************************************', "blue"))
  print ''
Msg.Exit(0)

