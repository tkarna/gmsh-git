#include "function.h"
#include "fullMatrix.h"
#include "math.h"

static double Ox, Oy, dx, dy;
static int nx, ny;
static double *data;
static bool init = false;

static void initialize() {
  FILE *input = fopen("bath.bin", "r");
  double buf;
  fread (&Ox, sizeof(double), 1, input);
  fread (&Oy, sizeof(double), 1, input);
  fread (&buf, sizeof(double), 1, input);
  fread (&dx, sizeof(double), 1, input);
  fread (&dy, sizeof(double), 1, input);
  fread (&buf, sizeof(double), 1, input);
  fread (&nx, sizeof(int), 1, input);
  fread (&ny, sizeof(int), 1, input);
  fread (&buf, sizeof(int), 1, input);
  data = new double[nx*ny];
  fread (data, sizeof(double), nx*ny, input);
  init = true;
}
extern "C" {

void bath (dataCacheMap *, fullMatrix<double> &bath, fullMatrix<double> &lonlat) {
  if (! init) initialize();
  for (size_t i = 0; i < bath.size1(); i++) {
    double rx = (lonlat(i,0)-Ox);
    double ry = (lonlat(i,1)-Oy);
    int iX = rx/dx;
    int iY = ry/dy;
    double alpha = (rx - iX*dx) / dx;
    double beta = (ry - iY*dy) / dy;
    double b = 
      (1-alpha)*(1-beta)*data[iX*ny+iY]
      +alpha*(1-beta)*data[(iX+1)*ny+iY]
      +alpha*beta*data[(iX+1)*ny+(iY+1)]
      +(1-alpha)*beta*data[iX*ny+(iY+1)];
    b = -b;

    if (b < 0){ 
      b = 5;
    }
    else if (b < 100){
      b = b + 5*exp(-b/5.0);
    }
    else if (b < 210){ 
      b = b - 10*exp((b-210.)/10.);
    }
    else{
      b = 200;
    }
		
		double di;
		double d = sqrt((-5.83 - (-6.07)) * (-5.83 - (-6.07)) + (13.44 - 12.4) * (13.44 - 12.4));
		
		if (lonlat(i,1)>-6.23*M_PI/180 && lonlat(i,0)>12.42*M_PI/180){
			di =  sqrt((lonlat(i,1)*180/M_PI - (-6.07)) * (lonlat(i,1)*180/M_PI - (-6.07)) + (lonlat(i,0)*180/M_PI - 12.4) * (lonlat(i,0)*180/M_PI - 12.4));
			b = (di/d)*(-9) + 4;
		}
		
		b = b + 9;

    bath.set (i, 0, b);
  }
}

}
