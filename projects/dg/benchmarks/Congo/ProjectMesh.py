# Import for python
from dgpy import *
from gmshpy import *
from math import *

# Projection Parameters
phi = 12.9802/180*pi
theta = -5.9022/180*pi
Ox = cos(theta)*cos(phi)
Oy = cos(theta)*sin(phi)
Oz = sin(theta)
eThetaX = -sin(theta)*cos(phi)
eThetaY = -sin(theta)*sin(phi)
eThetaZ = cos(theta)
ePhiX = -sin(phi)
ePhiY = cos(phi)
ePhiZ = 0


def projectMesh (model):
  def project(entity):
    numVertices = entity.getNumMeshVertices()
    m = fullMatrixDouble (3,3)
    for iV in range(0,numVertices): 
      v = entity.getMeshVertex(iV)
      pEphi = v.x()*ePhiX + v.y()*ePhiY + v.z()*ePhiZ
      pEtheta = v.x()*eThetaX + v.y()*eThetaY + v.z()*eThetaZ
      m.set(0, 0, ePhiX)
      m.set(0, 1, ePhiY)
      m.set(0, 2, ePhiZ)
      m.set(1, 0, eThetaX)
      m.set(1, 1, eThetaY)
      m.set(1, 2, eThetaZ)
      m.set(2, 0, -v.x())
      m.set(2, 1, -v.y())
      m.set(2, 2, -v.z())
      m.invertInPlace()
      R = sqrt(v.x()**2 + v.y()**2+ v.z()**2)
      alpha = -Ox*m.get(0,0) - Oy*m.get(1,0) - Oz * m.get(2,0)
      beta = -Ox*m.get(0,1) - Oy*m.get(1,1) - Oz * m.get(2,1)
      v.setXYZ(alpha*R, beta*R, 0)

  gvertices = model.bindingsGetVertices()
  for v in gvertices:
    project(v)
  
  gedges = model.bindingsGetEdges()
  for e in gedges:
    project(e)

  gfaces = model.bindingsGetFaces()
  for f in gfaces:
    project(f)

