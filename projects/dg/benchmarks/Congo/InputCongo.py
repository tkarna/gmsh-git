from dgpy import *
from ProjectMesh import *
import calendar, os, time

# File Name
filename = "congotest2"
outputDir = "output"
bathdir="Bath/"+filename+"_bath_smooth"
bathname="Bath/"+filename+"_bath_smooth"+"/"+filename+"_bath_smooth.idx"



# Multirate and Partition Parameters
RK_TYPE = ERK_22_C
mL = 1000
algo = 5



#a faire !!!

# Lauch Makefile to generate meshes and download forcings
#if(Msg.GetCommRank() == 0):
  #try : os.system("gmsh -2 "+filename+".geo");
  #except: 0;
  #try : os.system("make forcings");
  #except: 0;
  #try : os.mkdir(outputDir);
  #except: 0;
#Msg.Barrier()

# Project mesh in planar space
if(not os.path.exists(filename+"_tan"+".msh")):
  m = GModel()
  m.load(filename+".msh")
  projectMesh(m)
  m.save(filename+"_tan"+".msh")
Msg.Barrier()

if (Msg.GetCommRank() == 0):
  functionC.buildLibraryFromFile ("BATH.cc", "lib_bath.so");
  functionC.buildLibraryFromFile ("Congo.cc", "lib_congo.so");

Msg.Barrier()

clib = "lib_congo.so"
blib= "lib_bath.so"

# Definition of Python functions
def printTime (t) :
  return "%dh%02d'%02d\"" % (t/3600,  (t%3600)/60,  t%60)

def printDate (t):
  date=time.gmtime(t)
  return "%02d/%02d/%04d %02dh%02d'%02d\""%(date[2], date[1], date[0], date[3], date[4], date[5])


def massIntegrator(fct,  sol, bath):
  for i in range(0,fct.size1()):
    s=sol.get(i, 0)
    depth=bath.get(i, 0)
    fct.set(i, 0, s+depth)
