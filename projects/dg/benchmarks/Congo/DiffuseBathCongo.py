from dgpy import *
from InputCongo import *
from ProjectMesh import *
import os

if(Msg.GetCommSize() > 1):
  Msg.Fatal("You need to run the script a first time on only one processor before you run it on more!!")
model=GModel()
model.load(filename+"_tan"+".msh")

bNu=20    # Diffusion
bT=10000  # Time for smoothing

# Mesh Parameters
order = 1
dimension = 2

groups = dgGroupCollection(model, dimension, order)
XYZ = function.getCoordinates()
bathDC = dgDofContainer(groups, 1);
lonLat = functionC(clib, "lonLat", 3, [XYZ])
bathS = functionC(blib, "bath", 1, [lonLat])
bathDC.interpolate(bathS);
nuB = functionConstant(bNu)
dlaw = dgConservationLawAdvectionDiffusion.diffusionLaw(nuB)
dlaw.addBoundaryCondition("coast", dlaw.new0FluxBoundary())
dlaw.addBoundaryCondition("ocean", dlaw.new0FluxBoundary())
dlaw.addBoundaryCondition("source1", dlaw.new0FluxBoundary())
sys = linearSystemPETScDouble()
dof = dgDofManager.newCG (groups, 1, sys)
implicitEuler = dgDIRK(dlaw, dof)
implicitEuler.iterate (bathDC, bT, 0)
sys = 0
dof = 0
exporterBath = dgIdxExporter(bathDC, bathdir)
exporterBath.exportIdx()
Msg.Exit(0)
