#!/usr/bin/env rundgpy
from dgpy import *;
import math, sys, os

def mem(size="rss"):
  return int(os.popen('ps -p %d -o %s | tail -1' % (os.getpid(), size)).read())
 
def rss():
  """Return ps -o rss (resident) memory in kB."""
  return mem("rss")
 
def rsz():
  """Return ps -o rsz (resident + text) memory in kB."""
  return mem("rsz")
 
def vsz():
  """Return ps -o vsz (virtual) memory in kB."""
  return mem("vsz")


os.makedirs("meshes", exist_ok=True)

CCode = """
#include "fullMatrix.h"
#include "function.h"
extern "C" {
void CM(dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< sol.size1(); i++) {
    double x = (xyz(i, 0));
    double y = (xyz(i, 1));
    double alpha = 1e-3;
    double beta = 1e-8;
    double tau = 5;
    double CM = exp(-beta/tau * (x * x + y * y / alpha)) / sqrt(4 * M_PI * tau * alpha);
    sol(i, 0) = CM;
  }
}

void gradCM(dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< sol.size1(); i++) {
    double x = (xyz(i, 0));
    double y = (xyz(i, 1));
    double alpha = 1e-3;
    double beta = 1e-8;
    double tau = 5;
    double CM = exp(-beta/tau * (x * x + y * y / alpha)) / sqrt(4 * M_PI * tau * alpha);
    sol(i, 0) = -beta/tau * CM * 2 * x;
    sol(i, 1) = -beta/tau * CM * 2 * y / alpha;
    sol(i, 2) = 0;
  }
}

void S(dataCacheMap *, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (size_t i = 0; i< sol.size1(); i++) {
    double x = (xyz(i, 0));
    double y = (xyz(i, 1));
    double alpha = 1e-3;
    double beta = 1e-8;
    double tau = 5;
    double bt = beta/tau;
    double CM = exp(-bt * (x * x + y * y / alpha)) / sqrt(4 * M_PI * tau * alpha);
    sol(i, 0) = -4 * bt * ((x * x + y * y / alpha) * bt - 1) * CM;
  }
}

void sqError(dataCacheMap *, fullMatrix<double> &sqerr, fullMatrix<double> &sol, fullMatrix<double> &CM)
{
  for (size_t i = 0; i< sol.size1(); i++) {
    double e = sol(i, 0) - CM(i, 0);
    sqerr(i, 0) = e * e;
  }
}
void error(dataCacheMap *, fullMatrix<double> &err, fullMatrix<double> &sol, fullMatrix<double> &CM)
{
  for (size_t i = 0; i< sol.size1(); i++) {
    err(i, 0) = sol(i, 0) - CM(i, 0);
  }
}
}
"""

functionC.buildLibrary (CCode, "libtmp.so");
S = functionC("./libtmp.so", "S",1, [function.getCoordinates()])
CM = functionC("./libtmp.so", "CM",1, [function.getCoordinates()])
gradCM = functionC("./libtmp.so", "gradCM", 3, [function.getCoordinates()])

kappaMax = 1
kappaMin = 1e-3
def genMesh(N, alpha) :
  meshfilename = "meshes/square_%i_%i.msh" % (alpha, N)
  fparam = open("param.geo", "w")
  fparam.write("N = %i;\n" % N);
  fparam.write("Alpha = %.16e;\n" % (math.pi * alpha / 180.));
  fparam.close()
  g = GModel()
  g.load("param.geo")
  os.remove("param.geo")
  g.load("square.geo")
  g.mesh(2)
  g.save(meshfilename)
  return g

def run(name, outdir, groups, IP, SIP):
  print("MEMORY USAGE =", rss()/1024)
  law = dgConservationLawDiffusionSIP(kappaMax, 0, kappaMin, S);
  law.setBoundaryFunction(CM, gradCM);
  law.setSymmetricIP(SIP)
  law.setIPFactor(IP)
  solution = dgDofContainer(groups, 1);
  sys = linearSystemPETScBlockDouble();
  sys.setParameter("petscOptions", "-pc_type lu")
  #sys.setParameter("petscOptions", "-ksp_monitor")
  dof = dgDofManager.newDGBlock(groups, 1, sys);
  dof.thisown=True
  steady = dgSteady(law, dof)
  steady.getNewton().setRtol(1e-8)
  steady.getNewton().setVerb(10)
  steady.solve(solution)
  sys.printMatlab("%s/%s_matrix.mat" % (outdir, name))
  err = functionC("./libtmp.so", "error",1, [solution.getFunction(), CM])
  errdof = dgDofContainer(groups, 1)
  errdof.interpolate(err)
  zero = functionConstant(0)
  sqerr = functionC("./libtmp.so", "sqError",1, [solution.getFunction(), CM])
  #sqerr = functionC("./libtmp.so", "sqError",1, [errdof.getFunction(), zero])
  integ = dgFunctionIntegrator(groups, sqerr)
  e = math.sqrt(integ.compute("", 10))
  allerrors.setdefault(name, []).append(e)
  solution.exportMsh("%s/%s" % (outdir, name))
  solution.exportFunctionMsh(err, "%s/error_%s" % (outdir, name))
  allip.setdefault(name, []).append(IP)

def projectionError(groups, outdir) :
  sol = dgDofContainer(groups, 1)
  sol.L2Projection(CM)
  err = functionC("./libtmp.so", "error",1, [sol.getFunction(), CM])
  sqerr = functionC("./libtmp.so", "sqError",1, [sol.getFunction(), CM])
  integ = dgFunctionIntegrator(groups, sqerr)
  e = math.sqrt(integ.compute("", 10))
  allerrors.setdefault("projection", []).append(e)
  sol.exportFunctionMsh(sol.getFunction(), "%s/projection" % (outdir))
  sol.exportFunctionMsh(err, "%s/error_projection" % (outdir))

def printDict(f, data) :
  f.write("%20s" %"N")
  for N in allN :
    f.write("%12i" %N)
  f.write("\n")
  for t, err in sorted(data.items()) :
    f.write("%20s" %t)
    for e in err :
      f.write("%12.3e" % e)
    f.write("\n")

order = 1
alpha = 0
if len(sys.argv) > 1:
  order = int(sys.argv[1])
if len(sys.argv) > 2 :
  alpha = float(sys.argv[2])

allN = []
allerrors = {}
allip = {}
for j in range(2, 5) : #2^j elements in x
  N = 2**j
  L = 1e5 / N
  l = L / 33;
  allN.append(N)
  model = genMesh(N, alpha)
  groups = dgGroupCollection(model,2, order);
  outdir = "output_%i_%g/%i" % (order, alpha, N)
  os.makedirs(outdir, exist_ok=True)
  projectionError(groups, outdir)
  run("nKn", outdir, groups, -1, True)
  run("nKn_NSIP", outdir, groups, -1, False)
  ## original Riviere (wrong in the paper)
  IP = (order + 1) * (order + 2) / 2. / 2. * (2 * L + 2 * l) / (l * L) * kappaMax**2 / kappaMin
  run("Rivière", outdir, groups, IP, False)
  ## improved Riviere
  IP = (order + 1) * (order + 2) / 2. / 2. * (2 * l * kappaMax**2 + 2 * L * kappaMin**2) / (l * L * kappaMin) # invalid for alpha != 0
  run("Rivière_Improved", outdir, groups, IP, True)

  f = open("log_%i_%g" % (order, alpha), "w")
  f.write("alpha = %g order = %i\n" % (alpha, order))
  f.write("errrors : \n")
  printDict(f, allerrors)
  f.close()
  os.system("cat log_%i_%g" % (order, alpha))
