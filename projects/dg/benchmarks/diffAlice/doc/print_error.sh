base=$(basename $1 .png)
arrIN=(${base//-/ })
NAME=${arrIN[1]}
ORDER=${arrIN[2]}
ALPHA=${arrIN[3]}
N=${arrIN[4]}
echo ../meshes/square_${ALPHA}_${N}.msh ../output_${ORDER}_${ALPHA}/${N}/error_${NAME}.msh print.opt print.geo
gmsh ../meshes/square_${ALPHA}_${N}.msh ../output_${ORDER}_${ALPHA}/${N}/error_${NAME}.msh print.opt print.geo
mv out.png $1
