import sys
import matplotlib.pyplot as plt
plt.figure(figsize=(8, 11))
for fname in ["log_1_0", "log_2_0", "log_3_0", "log_4_0"] :
  f = open(fname)
  lines = [line.split() for line in f.readlines()[:8]]
  order = int(lines[0][5])
  lines = lines[2:]
  labels = [line[0] for line in lines[1:]]
  values = [[float(i) for i in line[1:]] for line in lines[1:]]
  N = [float(i) for i in lines[0][1:]]
  l = [1e5/i for i in N]
  ma=['o','*','.','.','.']
  #col=[(0, 0, 1),(0, 0.5, 0),(0, 0, 1),(0, 0.7, 0.7),(0.7, 0.7, 0)]
  col = ["b", "g", "r", "c", "m"]
  for i in range(len(values)):
    plt.loglog(l, values[i],'--' if labels[i] == "projection" else '-', marker=ma[i],markeredgewidth=0, color=col[i])
  xm = l[-1]
  xM = l[0]
  ym = min([min(v) for v in values])
  yM = max([max(v) for v in values])
  plt.plot([xm, xM], [0.9 * ym, 0.9 * ym * (xM/xm)**(order + 1)],'k:',label=u'$\\alpha x^%i$' % (order + 1))
  plt.text(xm, ym*3, "$p=%i$"%order, ha = "center")
plt.axis(xmin=xm * 0.9, xmax = xM * 1.1)#, ymin = ym * 0.9, ymax = yM * 1.1)
plt.xlabel('$l_x$')
plt.ylabel(r'$\sqrt{\int_\Omega e^2 d\omega}$')
plt.legend(labels + [r"$\alpha x^{p + 1}$"], loc='best', frameon=False)
if len(sys.argv) > 1 :
  plt.savefig(sys.argv[1],bbox_inches="tight")
else :
  plt.show()

