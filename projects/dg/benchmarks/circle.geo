  ip = newp;
    PI=3.1415926535897931;
    alpha = 80*PI/180;
    r=1000000;
    N = 1000;
    Point(ip)={0,0,0};
    il = newl;
    For i In {0:N}
      beta = i*2*PI/N;
      Point (ip+i+1) = {r*Sin(beta), r*Cos(beta), 0};
    EndFor
    Spline (il) = {ip+1:ip+N+1, ip+1};
Line Loop(2) = {1};
Plane Surface(3) = {2};
Physical Surface('surf') = {3};
h=1500;
out[] = Extrude {0,0,-h}{
  Surface{3};
  Layers{{5,5,9},{0.01,0.1,1}};
  Recombine;
};
Mesh.Algorithm3D=4;
Physical Volume('vol') = {out[1]};
Physical Surface("bottom") = {out[0]};
Physical Surface("wall") = {out[2]};
Field[1] = MathEval;
Field[1].F = "100000";
Background Field = 1;
