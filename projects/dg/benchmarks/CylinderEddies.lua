MACH = .05;
RHO  = 1.0;
PRES = 1./(MACH*RHO*RHO*1.4*1.4) 
V = 2
SOUND = V/MACH

--[[ 
     Function for initial conditions
--]]
function free_stream( XYZ, FCT )
  for i=0,XYZ:size1()-1 do
    FCT:set(i,0,RHO) 
    FCT:set(i,1,RHO*V) 
    FCT:set(i,2,0.0) 
    FCT:set(i,3, 0.5*RHO*V*V+PRES/0.4) 
  end
end

function rot(sol_grad, f)
  for i=0,sol_grad:size1()-1 do
    dudy = sol_grad:get(i,1*3+1)
    dvdx = sol_grad:get(i,2*3+0)
    f:set(i,0,dudy-dvdx) 
  end
end

xyz = functionCoordinates.get()
print("Coordinates : ",xyz)
FS = functionLua(4, 'free_stream', {xyz})

--[[ 
     Example of a lua program driving the DG code
--]]

order = 3
dimension = 2

print'*** Loading the mesh and the model ***'
model   = GModel  ()
model:load ('cyl2.geo')	
model:load('cyl2.msh')
--gmshOptions.numberSet("Mesh", 0, "ElementOrder", order)
--model:mesh(1)
--model:mesh(2)
--model:save ('cyl2.msh')

groups = dgGroupCollection(model, dimension, order)
groups:buildGroupsOfInterfaces()

law=dgPerfectGasLaw(dimension)
const1 = functionConstant({0.025})
const2 = functionConstant({0.01})
law:setViscosityAndThermalConductivity(const1, const2);
print("FS = ",FS)

wallBoundary = law:newNonSlipWallBoundary()
outsideBoundary = law:newOutsideValueBoundary("",FS)
law:addBoundaryCondition('Cylinder',wallBoundary)
law:addBoundaryCondition('Box'     ,outsideBoundary)

print'*** setting the initial solution ***'
solution = dgDofContainer(groups, law:getNbFields())
proj = dgDofContainer(groups, 1)

rotF = functionLua(1, "rot", {solution:getFunctionGradient()})

solution:L2Projection(FS)
solution:exportMsh('output/cyl_0', 0,0)

print'*** solve ***'
rk=dgRungeKutta()

t = 0;
CFL = 10
x = os.clock()
nbExport = 0
for i=1,100000 do
   dt = CFL * rk:computeInvSpectralRadius(law,solution);
   norm = rk:iterateEuler(law, t, dt, solution)
   t = t + dt
   if (i % 100 == 0) then 
      print('|ITER|',i,'|NORM|',norm,'|DT|',dt, '|T|',t, '|CPU|',os.clock() - x)
      solution:exportMsh(string.format("output/cyl-%06d", i), t, nbExport) 
      proj:L2Projection(rotF)
      proj:exportMsh(string.format("output/rot-%06d", i), t, nbExport) 
      solution:exportFunctionMsh(law:getVelocity(), string.format('output/vel-%06d', i), t, nbExport)
      nbExport = nbExport + 1
   end
end
print'*** done ***'
