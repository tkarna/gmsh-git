## update particles positions and velocities with a time step DT
import numpy

def createInitialDistributionOfParticles (N) :
    n_particles = N*N
    dx = 1./N
    n_info = 13
    
    particles = numpy.zeros ( [n_particles , n_info], 'd' )
    volume_particle = 0.000018
    k = 0
    for i in range(0,N) :
        for j in range(0,N) :
            X =  -0.5 + i *dx
            Y =  -1 + j *dx
            particles[ k , 0] =X  # x position
            particles[ k , 1] =Y  # y position
            particles[ k , 2] =0  # z position
            particles[ k , 3] =0  # x velocity
            particles[ k , 4] =0  # y velocity
            particles[ k , 5] =0  # z velocity
            particles[ k , 6] =volume_particle  # volume
            particles[ k , 7] =0  # x darcy velocity
            particles[ k , 8] =0  # y darcy velocity
            particles[ k , 9] =0  # z darcy velocity
            k = k + 1

    return particles


def updateParticlesPositionsAndVelocities (particles, DT) :
  for i in range(0,particles.shape[0]):
    X = particles[i,0]
    Y = particles[i,1]
    Z = particles[i,2]
    vX = particles[i,3]
    vY = particles[i,4]
    vZ = particles[i,5]
    vol = particles[i,6]
    density = 1.0
    mass  = vol * density

    FX = particles[i,10]
    FY = - 9.81 * mass + particles[i,11]
    FZ = particles[i,12]

#    print 'F',  particles[i,11], 'G',  - 9.81 * mass

    accelX = FX / mass
    accelY = FY / mass
    accelZ = FZ / mass
    vxNew = vX + DT * accelX  
    vyNew = vY + DT * accelY  
    vzNew = vZ + DT * accelZ  
    xNew = X + DT * vxNew  
    yNew = Y + DT * vyNew  
    zNew = Z + DT * vzNew

    if (yNew < -4) :
        yNew = -3.99
        vxNew = 0
        vxNew = 0
        vyNew = 0
    if (xNew > 1.5) :
        xNew = 1.499
        vxNew = 0
        vyNew = 0
    if (xNew < -1.5) :
        xNew = -1.499
        vxNew = 0
        vyNew = 0
    
    particles[ i , 0] =xNew  # x position
    particles[ i , 1] =yNew  # y position
    particles[ i , 2] =zNew  # z position
    particles[ i , 3] =vxNew  # x velocity
    particles[ i , 4] =vyNew  # y velocity
    particles[ i , 5] =vzNew  # z velocity
