order=1
dimension=1

model = GModel()
cmd = string.format("dg river.geo -%d -order %d", dimension, order)
os.execute(cmd)
model:load ('river.msh')

CCode =[[
#include "fullMatrix.h"
#include "dgDataCacheMap.h"
#include "math.h"
extern "C" {
  double pow2(double value){
  	return value*value;
  }
	void gaussian(dgDataCacheMap *,fullMatrix<double> &u,fullMatrix<double> &xyz){
    for (size_t i = 0; i< u.size1(); i++) {
      u.set(i,0,exp(-0.5*pow2(xyz(i,0)-5e5)/pow2(3e5))); 
    }
	}
	void chezyManningDissipation(dgDataCacheMap *,fullMatrix<double> &C,fullMatrix<double> &section, fullMatrix<double> &width){
    for (size_t i = 0; i< C.size1(); i++) {
			double Heff=section(i,0)/width(i,0);
			double manning=0.023;
      C.set(i,0,pow2(manning)/Heff); 
    }
	}
	void merge(dgDataCacheMap *,fullMatrix<double> &uv,fullMatrix<double> &u, fullMatrix<double> &v){
    for (size_t i = 0; i< uv.size1(); i++) {
      uv.set(i,0,u(i,0)); 
      uv.set(i,1,v(i,0)); 
    }
	}
	void widthData(dgDataCacheMap *,fullMatrix<double> &w,fullMatrix<double> &bath, fullMatrix<double> &eta){
    for (size_t i = 0; i< w.size1(); i++) {
      w.set(i,0,100+(100-50)/bath(i,0)*eta(i,0)); 
      //w.set(i,0,100); 
    }
	}
	void sectionData(dgDataCacheMap *,fullMatrix<double> &s,fullMatrix<double> &bath, fullMatrix<double> &eta){
    for (size_t i = 0; i< s.size1(); i++) {
      s.set(i,0, 100*(eta(i,0)+bath(i,0))+(100-50)/bath(i,0)*(pow2(eta(i,0))-pow2(bath(i,0)))/2  ); 
      //s.set(i,0, 100*(eta(i,0)+bath(i,0))); 
    }
	}
}
]]
tmpLib = "tmp.dylib"
if (Msg.getCommRank() == 0 ) then
  functionC.buildLibrary (CCode, tmpLib);
end
Msg.barrier()
g = 9.81;
t = 0;

function tideF(FCT)
  for i=0,FCT:size1()-1 do
  	eta = math.sin(2*3.1416/44700*t)
  	FCT:set(0,0,eta)
	end
end
function timeF( FCT ) 
  for i=0,FCT:size1()-1 do
    	FCT:set(i,0,t); 
  end
end
--[[ 
     Example of a ShallowWater 1d code
--]]

groups = dgGroupCollection(model, dimension, order)
groups:buildGroupsOfInterfaces()

law = dgConservationLawShallowWater1d()

law:setElevationInterp()
etaInterp=law:getElevationInterp()

bath=functionConstant({10.})
widthData = functionC(tmpLib,'widthData',1,{bath,etaInterp})
sectionData = functionC(tmpLib,'sectionData',1,{bath,etaInterp})
law:setWidthData(widthData)
law:setSectionData(sectionData)

etaF = law:getElevation()
width= law:getWidth()
sol = functionSolution:get()
section = functionExtractComp(sol,0)
velocity = functionExtractComp(sol,1)

law:setQuadraticDissipation(functionC(tmpLib,'chezyManningDissipation',1,{section,width}))

etaGaussian = functionC(tmpLib,'gaussian',1,{functionCoordinates.get()})
sectionGaussian = functionC(tmpLib,'sectionData',1,{bath,etaGaussian})
initialSol = functionC(tmpLib,'merge',2,{sectionGaussian ,functionConstant({0.})})
law:addBoundaryCondition('Upstream',law:newBoundaryWall())
law:addBoundaryCondition('Downstream',law:newBoundaryWall())

--Iterate
solution = dgDofContainer(groups, law:getNbFields())
solution:L2Projection(initialSol)

rk=dgRungeKutta()
limiter = dgSlopeLimiter(law)
rk:setLimiter(limiter) 
limiter:apply(solution)

print'*** print initial sol ***'
solution:exportMsh('tidalRiver/init_limit', 0, 0)
solution:exportFunctionMsh(width,'tidalRiver/width', 0, 0)  
solution:exportFunctionMsh(etaF,'tidalRiver/eta', 0, 0)  
solution:exportFunctionMsh(section,'tidalRiver/section', 0, 0)  

print'*** solve ***'
t=0
CFL = 0.1
nbExport = 0
for i=1,1000 do 
    dt = 100
    norm = rk:iterate44(law,t, dt,solution)
    t = t+dt
    if (i % 100 == 0) then 
       print(string.format('Export %03d  |  time:%03.2f days  |  dt:%03.3f  |  norm:%.3e',nbExport,t/86400,dt,norm))
       solution:exportMsh(string.format('tidalRiver/sol-%06d', i), t, nbExport)  
       solution:exportFunctionMsh(etaF,string.format('tidalRiver/eta-%06d', i), t, nbExport)  
       nbExport  = nbExport  + 1
    end
end
