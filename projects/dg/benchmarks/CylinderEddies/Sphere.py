from dgpy import *
from gmshpy import *
import os
import time
import math

t=0
order = 3
dimension = 3
print'*** Loading the mesh and the model ***'
model   = GModel  ()
model.load('sphereDG2.msh')
groups = dgGroupCollection(model, dimension, order)
groups.buildGroupsOfInterfaces()


# Geometry
D = 1

R = 287.
Gamma = 1.4

T = 293.15
P = 101325.

Prandtl = 0.7
Reynolds = 300
Mach = 0.05

c = math.sqrt(Gamma * R * T)
V = c * Mach
RHO = P / (R * T)
mu = RHO * V * D / Reynolds # dynamic viscosity
Cp = Gamma * R / (Gamma - 1) # capacite thermique massique
k = mu * Cp / Prandtl # thermal conductivity


dt = 0.2*D/V/5.
dtinit = dt*5
Tmax = 2000 # 500s = time to have Karman vortex developed
NbStep = int(Tmax/dt)
  

# Adimensional numbers
Prandtl = mu * Cp / k
Mach = V/c
Re = RHO*V*D/mu

Msg.Info('Re = %8g  Pr = %8g  Ma = %8g' % (Re, Prandtl, Mach))
Msg.Info('mu = %8g  k  = %8g  Cp = %8g  D  = %8g' % (mu, k , Cp, D))
Msg.Info('T  = %8g  P  = %8g  RHO= %8g  V  = %8g' % (T, P, RHO, V))
Msg.Info('Vortices Period = %.2g    dt = %.2g' % (0.2*D/V, dt))


def free_stream(FCT,  XYZ) :
  slope = dtinit * 10
  v = V
  if t < slope :
    v = V*t/slope
  for i in range (0,XYZ.size1()) :
    FCT.set(i,0,RHO) 
    FCT.set(i,1,0.0) 
    FCT.set(i,2,0.0) 
    FCT.set(i,3,RHO*v) 
    FCT.set(i,4, 0.5*RHO*v*v+P/(Gamma-1)/RHO) 

"""
def rot(f, sol_grad) :
  for i in range(0,sol_grad.size1()) :
    dudy = sol_grad.get(i,1*3+1)
    dvdx = sol_grad.get(i,2*3+0)
    f.set(i,0,dudy-dvdx) 
"""

xyz = groups.getFunctionCoordinates()
FS = functionPython(5, free_stream, [xyz])

def computeDragAndLift(surface, time, iter, fScale, flowDir, spanDir1, spanDir2):
  
  print "---- Computing lift and drag ----"
  fun = open("LiftAndDrag.dat","a")
  if (iter == 0):
    fun.write("#(1)iter (2)time (3)lift1 (4)lift2 (5)drag \n")
  lift1 = 0.0
  lift2 = 0.0
  drag = 0.0
  
  integ=dgFunctionIntegratorInterface(groups, law.getTotalForceSurface(), solution)
  force=fullMatrixDouble(3,1)
  integ.compute(surface, force)
  
  # project force onto directions
  for j in range(0,3):
    drag = drag + force.get(j,0)*flowDir[j]
    lift1 = lift1 + force.get(j,0)*spanDir1[j]
    lift2 = lift2 + force.get(j,0)*spanDir2[j]
    
  drag = drag/fScale
  lift1 = lift1/fScale
  lift2 = lift2/fScale
  
  #save in file
  print >>fun, iter,' ',time,' ',lift1,' ',lift2,' ',drag
  fun.close()
  print "Lift1: %f Lift2: %f Drag: %f" %(lift1, lift2, drag)



law=dgPerfectGasLaw(dimension)
law.setGasConstantAndGamma(R, Gamma)
muF = functionConstant(mu)
kF = functionConstant(k)
law.setViscosityAndThermalConductivity(muF, kF);

#slipWallBoundary = law.newSlipWallBoundary()
wallBoundary = law.newNonSlipWallBoundary()
outsideBoundary = law.newOutsideValueBoundary("",FS)
law.addBoundaryCondition('Sphere', wallBoundary)
law.addBoundaryCondition('Box'     , outsideBoundary)

print'*** setting the initial solution ***'
solution = dgDofContainer(groups, law.getNbFields())
proj = dgDofContainer(groups, 1)

#rotF = functionPython(1, rot, [solution.getFunctionGradient()])

solution.L2Projection(FS)

sys = dgLinearSystemMatrixFreeBJacobi (law, groups)
sys.setTolerance(1e-3, 1e-13, 30)
dof = dgDofManager.newDGBlock (groups, law.getNbFields(), sys)
am = dgDIRK (law, dof, 2)
am.getNewton().setRtol(10**-3.5)
am.getNewton().setMaxIt(6)
am.getNewton().setVerb(10)

flowDir=[0., 0., 1.]
spanDir1=[1., 0., 0.]
spanDir2=[0., 1., 0.]
D = 1.

fScale = 0.5*RHO*V*V*D
t0 = time.clock()
for i in range (0, NbStep):
  dt2 = dt
  if i < 20 :
    dt2 = dtinit
  t = t + dt2
  am.iterate(solution, dt2, t)
  Msg.Info("Iter %i t=%.2f cpu=%.2f" % (i,t, time.clock()-t0))
  if (i % 10 == 0) :
    solution.exportMsh('output/sphere-%05i' % (i), t, i)
  computeDragAndLift('Sphere', t, i, fScale, flowDir, spanDir1, spanDir2)

Msg.Exit(0)
