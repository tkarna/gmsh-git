radius = 0.5;
W = 30 * radius;
Lb = 30 * radius;
La = 150 * radius;

radiusBoundaryLayer = 1.35 * radius;

Point(1) = {0, 0, 0};

nlayers = 3;
np = 6;
For n In {0:nlayers}
  r= radius + (radiusBoundaryLayer-radius)*n/nlayers;
  Point(n*10+2) = {r,0,0};
  Point(n*10+3) = {0,-r,0};
  Point(n*10+4) = {-r,0,0};
  Point(n*10+5) = {0,r,0};

  Circle(n*10+2) = {n*10+2,1,n*10+3};
  Circle(n*10+3) = {n*10+3,1,n*10+4};
  Circle(n*10+4) = {n*10+4,1,n*10+5};
  Circle(n*10+5) = {n*10+5,1,n*10+2};

  Transfinite Line (n*10+2) = np;
  Transfinite Line (n*10+3) = np;
  Transfinite Line (n*10+4) = np;
  Transfinite Line (n*10+5) = np;

  If (n>0)
    Line (n*10+7) = {(n-1)*10+2, n*10+2};
    Line (n*10+8) = {(n-1)*10+3, n*10+3};
    Line (n*10+9) = {(n-1)*10+4, n*10+4};
    Line (n*10+10) = {(n-1)*10+5, n*10+5};

    Line Loop (n*10+2) = {n*10+2, -n*10-8, -(n-1)*10-2, n*10+7};
    Plane Surface(n*10+2) = {n*10+2};
    Transfinite Surface (n*10+2) = {n*10+2, n*10+3, (n-1)*10+3, (n-1)*10+2};
    Line Loop (n*10+3) = {n*10+3, -n*10-9, -(n-1)*10-3, n*10+8};
    Plane Surface(n*10+3) = {n*10+3};
    Transfinite Surface (n*10+3) = {n*10+3, n*10+4, (n-1)*10+4, (n-1)*10+3};
    Line Loop (n*10+4) = {n*10+4, -n*10-10, -(n-1)*10-4, n*10+9};
    Plane Surface(n*10+4) = {n*10+4};
    Transfinite Surface (n*10+4) = {n*10+4, n*10+5, (n-1)*10+5, (n-1)*10+4};
    Line Loop (n*10+5) = {n*10+5, -n*10-7, -(n-1)*10-5, n*10+10};
    Plane Surface(n*10+5) = {n*10+5};
    Transfinite Surface (n*10+5) = {n*10+5, n*10+2, (n-1)*10+2, (n-1)*10+5};

    //Recombine Surface {n*10+2, n*10+3, n*10+4,n*10+5};
  EndIf
EndFor

Point(6) = {-Lb*radius, -W, 0};
Point(7) = {-Lb*radius, W, 0};
Point(8) = {La*radius, W, 0};
Point(9) = {La*radius, -W, 0};

Line (1000) = {6,7};
Line (1001) = {7,8};
Line (1002) = {8,9};
Line (1003) = {9,6};


Line Loop(1)={1000:1003};
Line Loop(2)={-nlayers*10-5,-nlayers*10-4,-nlayers*10-3,-nlayers*10-2};
Line Loop(3)= {2,3,4,5};
Plane Surface(1)={1,2};
//Recombine Surface (1);

Physical Line("Lateral") = {1001,1003};
Physical Line("Box") = {1000,1002};
Physical Line("Cylinder") = {2,3,4,5};

Physical Surface("BoundaryLayer") = {2:10000};
Physical Surface("Air") = {1};

Mesh.CharacteristicLengthExtendFromBoundary=1;
Mesh.CharacteristicLengthFromPoints=1;
Mesh.RecombinationAlgorithm = 1;
//Mesh.RecombinationAlgorithm = 0;
//Mesh.SmoothInternalEdges = 1;
Mesh.SecondOrderIncomplete=0;

Field[1] = MathEval;
Field[1].F = "Fabs(y)";
Field[2] = Threshold;
Field[2].IField = 1;
Field[2].LcMin = radius;
Field[2].LcMax = radius * 3;
Field[2].DistMin = radius * 2;
Field[2].DistMax = radius * 10;
Field[3] = MathEval;
Field[3].F = "Sqrt(x * x / 2 + y * y * 1.5)";
Field[4] = Threshold;
Field[4].IField = 3;
Field[4].LcMin = radius / 2;
Field[4].LcMax = radius * 3;
Field[4].DistMin = radius * 2;
Field[4].DistMax = radius * 10;
Field[5] = Min;
Field[5].FieldsList = {4, 2};
Background Field = 5;
