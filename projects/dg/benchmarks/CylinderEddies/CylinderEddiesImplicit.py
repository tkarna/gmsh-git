from gmshpy import *
from dgpy import *
import os
import time
import math

Mach = 0.05
Reynolds = 100
Prandtl = 0.7

D = 1
P = 101325
R = 287.1
T = 293.15
Gamma = 1.4

Cp = Gamma * R / (Gamma-1)
rho = P / ( R * T)
c = math.sqrt(Gamma * R * T)
V = Mach * c
mu  = rho * V * D / Reynolds
k = mu * Cp / Prandtl

t = 0

Strouhal = 0.181
T = D / (Strouhal * V)
dtreg = T / 100
dtinit = dtreg * 10
ndtinit = 200

os.system("rm LiftAndDrag.dat")
print('Reynolds= %g\nPrandtl = %g\nMach = %g\n' % (rho*V*D/mu, Prandtl, V/c))
print("v = %g\nT=%g\n" %(V,T))

def free_stream(FCT,  XYZ) :
  for i in range (0,XYZ.size1()) :
    FCT.set(i,0,rho) 
    FCT.set(i,1,rho*V) 
    FCT.set(i,2,0.0) 
    FCT.set(i,3, 0.5*rho*V*V+P/(Gamma-1)/rho) 

order = 4
dimension = 2

print'*** Loading the mesh and the model ***'
model   = GModel  ()
model.load('cyl2_p2.msh')
groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsRandom(2)
#groups.buildGroupsOfInterfaces()

xyz = groups.getFunctionCoordinates()
FS = functionPython(4, free_stream, [xyz])

law=dgPerfectGasLaw(dimension)
law.setGasConstantAndGamma(R, Gamma)
muF = functionConstant(mu)
kF = functionConstant(k)
law.setViscosityAndThermalConductivity(muF, kF);

wallBoundary = law.newNonSlipWallBoundary()
slipWallBoundary = law.newSlipWallBoundary()
outsideBoundary = law.newOutsideValueBoundary("",FS)
law.addBoundaryCondition('Cylinder',wallBoundary)
law.addBoundaryCondition('Lateral',slipWallBoundary)
law.addBoundaryCondition('Box'     ,outsideBoundary)

print'*** setting the initial solution ***'
solution = dgDofContainer(groups, law.getNbFields())
proj = dgDofContainer(groups, 1)
solution.L2Projection(FS)
solution.exportMsh('output/cyl_init', 0, 0)
 
print'*** solve ***'

#sys = linearSystemPETScDouble ()
#sys.setParameter("petscOptions", "-pc_type lu")
#dof = dgDofManager.newDG (groups, law.getNbFields(), sys)
##sys.setParameter("petscOptions", "-pc_type bjacobi -pc_bjacobi_blocks  %i -sub_pc_type lu -ksp_rtol 1e-3 -ksp_max_it 50" % groups.getElementGroup(0).getNbElements())
#nbElements = groups.getElementGroup(0).getNbElements() +groups.getElementGroup(1).getNbElements() 
#sys.setParameter("petscOptions", "-pc_type bjacobi -pc_bjacobi_blocks  %i -sub_pc_type lu -ksp_rtol 1e-3 -ksp_max_it 50" % nbElements)

## matrix free bjacobi
sys = dgLinearSystemMatrixFreeBJacobi (law, groups, True)
sys.setTolerance(1e-3, 1e-13, 15)
dof = dgDofManager.newDGBlock (groups, law.getNbFields(), sys)
#am = dgAdamsMoulton (2, solution, law, dof, dt, t)
dirk = dgDIRK (law, dof, 2)
dirk.getNewton().setAtol(1e-12)
dirk.getNewton().setRtol(1e-4)
dirk.getNewton().setMaxIt(10)
dirk.getNewton().setVerb(10)

def computeDragAndLift(surface, time):
  flowDir=[1., 0., 0.]
  spanDir=[0., 1., 0.]
  fScale = 0.5*rho*V*V*D
  fun = open("LiftAndDrag.dat","a")
  lift = 0.0
  drag = 0.0
  integ=dgFunctionIntegratorInterface(groups, law.getTotalForceSurface(), solution)
  force=fullMatrixDouble(3,1)
  integ.compute(surface, force)
  for j in range(0,3):
    drag = drag + force.get(j,0)*flowDir[j]
    lift = lift + force.get(j,0)*spanDir[j]
  drag = drag/fScale
  lift = lift/fScale
  print >>fun, time,' ',lift,' ',drag
  fun.close()
  print "Lift: %f Drag: %f" %(lift, drag)

buf = dgDofContainer(groups, law.getNbFields())
def safe_iter (ts, sol, buf, dt, t, msg = "") :
  if (msg != "") :
    Msg.Info("subiter " + msg)
  buf.copy(sol)
  if (not ts.iterate(sol, dt, t)) :
    sol.copy(buf)
    safe_iter(ts, sol, buf, dt/2, t, msg + "0")
    safe_iter(ts, sol, buf, dt/2, t + dt/2, msg + "1")
tic = 0
tic = time.clock()
for i in range (0, 100000):
  dt = dtreg
  if (i < ndtinit):
    dt = dtinit
  t = t + dt
  safe_iter(dirk, solution, buf, dt, t)
  computeDragAndLift('Cylinder', t)
  solution.exportMsh('output/cyl2-%05i' % (i), t, i)
  Msg.Info("Iter %i t=%.2g dt=%.2g cpu=%.2f" % (i,t,dt,time.clock()-tic))
print(time.clock()-tic)
Msg.Exit(0)
