from gmshpy import *
from dgpy import *
import os
import time
import math

# Geometry
D = 1

# Material characteristics of air (at 300K)
RHO  = 1.18 # 1.18 kg/m^3
Cp = 1006. # 1006. J/kg/K do not remove the dot
k = 0.025 # 0.026 W/m/K
mu = 0.0002 # 1.85e-5 kg/m/s

V = 0.2
P = 10. 

dt = 0.25
Tmax = 4 # 500s = time to have Karman vortex developed
NbStep = int(Tmax/dt)
  
R = 287.
Gamma = Cp/(Cp-R)
c = math.sqrt(Gamma*P/RHO)

# Adimensional numbers
Prandtl = mu * Cp / k
Mach = V/c
Re = RHO*V*D/mu

print('\nRe = %g ; Pr = %g ; Ma = %g ; ' % (Re, Prandtl, Mach))
print("v = %g\tT=%g" %(V,P/RHO/R))
if raw_input("\nPress any Key to continue, 'a' to abort: ") == "a":
    sys.exit();


t=0
order = 1
dimension = 3

def free_stream(FCT,  XYZ) :
  slope = dt * 10
  v = V*t/slope if t < slope else V
  for i in range (0,XYZ.size1()) :
    FCT.set(i,0,RHO) 
    FCT.set(i,1,RHO*v) 
    FCT.set(i,2,0.0) 
    FCT.set(i,3,0.0) 
    FCT.set(i,4, 0.5*RHO*v*v+P/(Gamma-1)/RHO) 

"""
def rot(f, sol_grad) :
  for i in range(0,sol_grad.size1()) :
    dudy = sol_grad.get(i,1*3+1)
    dvdx = sol_grad.get(i,2*3+0)
    f.set(i,0,dudy-dvdx) 
"""

xyz = getFunctionCoordinates()
FS = functionPython(5, free_stream, [xyz])


print'*** Loading the mesh and the model ***'
model   = GModel  ()
model.load('cyl_extruded.msh')
groups = dgGroupCollection(model, dimension, order)
groups.buildGroupsOfInterfaces()

law=dgPerfectGasLaw(dimension)
law.setGasConstantAndGamma(R, Gamma)
muF = functionConstant(mu)
kF = functionConstant(k)
law.setViscosityAndThermalConductivity(muF, kF);

slipWallBoundary = law.newSlipWallBoundary()
wallBoundary = law.newNonSlipWallBoundary()
outsideBoundary = law.newOutsideValueBoundary("",FS)
law.addBoundaryCondition('Cylinder', wallBoundary)
law.addBoundaryCondition('Box'     , outsideBoundary)
law.addBoundaryCondition('Symmetry', slipWallBoundary)

print'*** setting the initial solution ***'
solution = dgDofContainer(groups, law.getNbFields())
proj = dgDofContainer(groups, 1)

#rotF = functionPython(1, rot, [solution.getFunctionGradient()])

solution.L2Projection(FS)

print'*** solve ***'

#sys = linearSystemPETScDouble ()
#sys.setParameter("petscOptions", "-pc_type lu")
#dof = dgDofManager.newDG (groups, law.getNbFields(), sys)
##sys.setParameter("petscOptions", "-pc_type bjacobi -pc_bjacobi_blocks  %i -sub_pc_type lu -ksp_rtol 1e-3 -ksp_max_it 50" % groups.getElementGroup(0).getNbElements())
#nbElements = groups.getElementGroup(0).getNbElements() +groups.getElementGroup(1).getNbElements() 
#sys.setParameter("petscOptions", "-pc_type bjacobi -pc_bjacobi_blocks  %i -sub_pc_type lu -ksp_rtol 1e-3 -ksp_max_it 50" % nbElements)

## matrix free bjacobi
sys = dgLinearSystemMatrixFreeBJacobi (law, groups)
dof = dgDofManager.newDGBlock (groups, law.getNbFields(), sys)
am = dgAdamsMoulton (4, solution, law, dof, dt, t)
am.getNewton().setAtol(1e-12)
am.getNewton().setRtol(1e-7)
am.getNewton().setVerb(10)

for i in range (0, NbStep):
  t = t + dt
  print("Iter %i t=%.2f" % (i,t))
  am.iterate()
  if (i % 10 == 0) :
    solution.exportMsh('output/cyl-%05i' % (i), t, i)

