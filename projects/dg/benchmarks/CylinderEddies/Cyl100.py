import sys
from dgpy import *
from gmshpy import *
sys.path.append("../incompressible")
from Incompressible import *
from math import *

os.system("rm LiftAndDragIncomp.dat")
#-------------------------------------------------
#-- Cylinder at Reynolds 100
#-------------------------------------------------

#-------------------------------------------------
#-- Parameters
#-------------------------------------------------
P = 101325
R = 287.1
T = 293.15
RHO = P / (R * T)
MU  = 0.361
UGlob = 30
meshName = "Cyl100"

print('*** Reynolds= %g' % (RHO*UGlob*1/MU))

#-------------------------------------------------
#-- Initial function
#-------------------------------------------------
def initF(FCT, XYZ):
	for i in range(0,FCT.size1()):
		FCT.set(i,0, UGlob) #u
		FCT.set(i,1, 0.0) #v
		FCT.set(i,2, 0.0) #p

#-------------------------------------------------
#-- Case specific functions
#-------------------------------------------------
def VelBC(FCT, XYZ ):
	vmax = 2.0;
	for i in range(0,FCT.size1()):
		FCT.set(i,0, UGlob)
		FCT.set(i,1, 0.0)
		FCT.set(i,2, 0.0)
		FCT.set(i,3, fixed)
		FCT.set(i,4, fixed)
		FCT.set(i,5, free)

#-------------------------------------------------
#-- Initialization
#-------------------------------------------------
ns = Incompressible(2)
rhoF = functionConstant(RHO)
muF = functionConstant(MU)
ns.initializeIncomp(meshName, initF, rhoF, muF , UGlob)
#-------------------------------------------------
#-- Boundary Conditions
#-------------------------------------------------

VEL = functionPython(6, VelBC, [ns.XYZ])
ns.strongBoundaryConditionLine('Inlet', VEL)
ns.strongBoundaryConditionLine('Cylinder', ns.WALL)
ns.weakPresBoundaryCondition('Outlet', ns.PRESZERO)
ns.weakSlipBoundaryCondition('WallBottom')
ns.weakSlipBoundaryCondition('WallTop')

#-------------------------------------------------
#-- TimeStepping
#-------------------------------------------------

timeOrder = 2
ATol = 1.e-5
RTol = 1.e-8
Verb = 2
nbExport = 10
petscOptions =""
 
ns.initSolver(timeOrder, ATol,RTol, Verb, petscOptions, nbExport)

flowDir=[1., 0., 0.]
spanDir=[0., 1., 0.]
D = 1.0
fScale = 0.5*RHO*UGlob*UGlob*D

t = 0
tic = time.clock()
dt = 0.05
for i in range (0,30000) :
        if (i==80):
          dt = 0.005
	ns.doOneIteration(dt, i)
        Msg.Info("Iter %i t=%.2g dt=%.2g cpu=%.2f" % (i,t,dt,time.clock()-tic))
        t = t + dt
       	ns.computeDragAndLift('Cylinder', t, i, fScale, flowDir, spanDir, 'LiftAndDragIncomp.dat')
	
#-------------------------------------------------
#-- Validation 
#-------------------------------------------------


