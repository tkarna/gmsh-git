from dgpy import *
import gmshPartition
import time

def free_stream() :
  M = .05          # Mach
  a = 1            # Speed of sound
  rho = 1.4
  p = a*a*rho/1.4 
  v = M*a
  return [rho, rho*v, 0., 0.5*rho*v*v+p/0.4]

def rot(f, sol_grad, x) :
  for i in range(0,sol_grad.size1()) :
    dudy = sol_grad.get(i,1*3+1)
    dvdx = sol_grad.get(i,2*3+0)
    f.set(i,0,dudy-dvdx) 

xyz = function.getCoordinates()
#FS = functionPython(4, free_stream, [xyz])
FS = functionConstant(free_stream());

print('*** Loading the mesh and the model ***')
model   = GModel  ()
model.load (gmshPartition.simple('cyl_p2.msh'))
groups = dgGroupCollection(model)

law=dgPerfectGasLaw(2)
law.setGasConstantAndGamma(287.058, 1.4)
mu = functionConstant(0.0007)
kappa = functionConstant(0.025)
law.setViscosityAndThermalConductivity(mu, kappa);

wallBoundary = law.newNonSlipWallBoundary()
outsideBoundary = law.newOutsideValueBoundary("",FS)
law.addBoundaryCondition('Cylinder',wallBoundary)
#law.addBoundaryCondition('Lateral'     ,outsideBoundary)
#law.addBoundaryCondition('Box'     ,outsideBoundary)
law.addBoundaryCondition('WallBottom'     ,outsideBoundary)
law.addBoundaryCondition('WallTop'     ,outsideBoundary)
law.addBoundaryCondition('Inlet'     ,outsideBoundary)
law.addBoundaryCondition('Outlet'     ,outsideBoundary)

print('*** setting the initial solution ***')
solution = dgDofContainer(groups, law.getNbFields())
proj = dgDofContainer(groups, 1)

rotF = functionPython(1, rot, [solution.getFunctionGradient(), xyz])

solution.L2Projection(FS)
solution.exportMsh('output/cyl_0', 0,0)

print('*** solve ***')
rk=dgERK(law, None, DG_ERK_22)

t = 0;
#CFL = 10
x = time.clock()
nbExport = 0
#dt = CFL * rk.computeInvSpectralRadius(solution);
#dt = 0.015 # p=1, Cyl100
#dt = 0.0075 # p=2, Cyl100
dt = 0.002 # p=2, cyl
print("dt = %g" % dt)
for i in range(1,100000) :
   rk.iterate(solution, dt, t)
   t = t + dt
   if (i % 100 == 0) :
      norm = solution.norm()
      if(Msg.GetCommRank() == 0):
        print('|ITER|',i,'|NORM|',norm, '|DT|',dt, '|T|',t, '|CPU|',time.clock() - x)
      solution.exportMsh("output/cyl-%06d"% (i),t, nbExport) 
      proj.L2Projection(rotF)
      proj.exportMsh("output/rot-%06d" %(i),t, nbExport) 
      solution.exportFunctionMsh(law.getVelocity(), 'output/vel-%06d'%(i), t, nbExport)
      nbExport = nbExport + 1
