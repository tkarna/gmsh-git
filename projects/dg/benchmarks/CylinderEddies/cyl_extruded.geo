L = 2;
radius = 0.5;
a = radius;
b = radius*3;
radiusBoundaryLayer = 0.7;
Point(1) = {0, 0, 0, a};

nlayers=1;
np=3;
np2=2;


For n In {0:nlayers}
  r= radius + (radiusBoundaryLayer-radius)*n/nlayers;
  Point(n*10+2) = {r,0,0,a};
  Point(n*10+3) = {0,-r,0,a};
  Point(n*10+4) = {-r,0,0,a};
  Point(n*10+5) = {0,r,0,a};

  Circle(n*10+2) = {n*10+2,1,n*10+3};
  Circle(n*10+3) = {n*10+3,1,n*10+4};
  Circle(n*10+4) = {n*10+4,1,n*10+5};
  Circle(n*10+5) = {n*10+5,1,n*10+2};

  Transfinite Line (n*10+2) = np;
  Transfinite Line (n*10+3) = np;
  Transfinite Line (n*10+4) = np;
  Transfinite Line (n*10+5) = np;

  If (n>0)
    Line (n*10+7) = {(n-1)*10+2, n*10+2};
    Line (n*10+8) = {(n-1)*10+3, n*10+3};
    Line (n*10+9) = {(n-1)*10+4, n*10+4};
    Line (n*10+10) = {(n-1)*10+5, n*10+5};

    Transfinite Line (n*10+7) = np2;
    Transfinite Line (n*10+8) = np2;
    Transfinite Line (n*10+9) = np2;
    Transfinite Line (n*10+10) = np2;

    Line Loop (n*10+2) = {n*10+2, -n*10-8, -(n-1)*10-2, n*10+7};
    Plane Surface(n*10+2) = {n*10+2};
    Transfinite Surface (n*10+2) = {n*10+2, n*10+3, (n-1)*10+3, (n-1)*10+2};
    Line Loop (n*10+3) = {n*10+3, -n*10-9, -(n-1)*10-3, n*10+8};
    Plane Surface(n*10+3) = {n*10+3};
    Transfinite Surface (n*10+3) = {n*10+3, n*10+4, (n-1)*10+4, (n-1)*10+3};
    Line Loop (n*10+4) = {n*10+4, -n*10-10, -(n-1)*10-4, n*10+9};
    Plane Surface(n*10+4) = {n*10+4};
    Transfinite Surface (n*10+4) = {n*10+4, n*10+5, (n-1)*10+5, (n-1)*10+4};
    Line Loop (n*10+5) = {n*10+5, -n*10-7, -(n-1)*10-5, n*10+10};
    Plane Surface(n*10+5) = {n*10+5};
    Transfinite Surface (n*10+5) = {n*10+5, n*10+2, (n-1)*10+2, (n-1)*10+5};

    //Recombine Surface {n*10+2, n*10+3, n*10+4,n*10+5};
    extCyl[] = Extrude {0,0,L} {
      Surface {n*10+2, n*10+3, n*10+4,n*10+5};
      //Layers{2};
      //Recombine;
    };
    For i In {0:3}
      sTop[(n-1)*4+i] = n*10+2+i;
      sBottom[(n-1)*4+i] = extCyl[i*6];
      volAir[(n-1)*4+i] = extCyl[i*6+1];
      If (n == 1)
        sCylinder[i] = extCyl[4+i*6];
      EndIf
    EndFor
  EndIf
EndFor

Point(6) = {-5, -5, 0, b};
Point(7) = {-5, 5, 0, b};
Point(8) = {35, 5, 0, b};
Point(9) = {35, -5, 0, b};

Line(10001) = {7, 8};
Line(10002) = {8, 9};
Line(10003) = {9, 6};
Line(10004) = {6, 7};

Line Loop(1)={10001,10002,10003,10004};
Line Loop(2)={-nlayers*10-5,-nlayers*10-4,-nlayers*10-3,-nlayers*10-2};
Line Loop(3)= {2,3,4,5};
Plane Surface(1)={1,2};
//Recombine Surface (1);
ext[] = Extrude {0,0,L} {
  Surface {1};
  //Layers{2};
  //Recombine;
};

Physical Surface ("Box") = {ext[{2:5}]};
Physical Surface ("Symmetry") = {1, ext[0], sTop[{0:4*nlayers-1}], sBottom[{0:4*nlayers-1}]};
Physical Surface ("Cylinder") = {sCylinder[{0:3}]};
Physical Volume ("Air") = {ext[1], volAir[{0:4*nlayers-1}]};

Mesh.CharacteristicLengthExtendFromBoundary=1;
Mesh.CharacteristicLengthFromPoints=1;
//Mesh.RecombinationAlgorithm = 1;
//Mesh.SmoothInternalEdges = 1;
Mesh.SecondOrderIncomplete=0;
