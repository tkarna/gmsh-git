from dgpy import *
from gmshpy import *
import os
import time
import math

os.system("rm LiftAndDrag.dat")

#-------------------------------------------------
#-- Defs
#-------------------------------------------------
P = 101325
mu = 0.361
R = 287.1
Gamma = 1.4
T = 293.15
c = math.sqrt(Gamma * R * T)
rho = P / ( R * T)
V = 30
k = 254
D = 1
Cp = Gamma * R / (Gamma-1)
Prandtl = (mu * Cp) / k
dt = 0.05
print("\n Reynolds= %g\nPrandtl = %g\n Mach = %g\n" % (rho*V*D/mu, Prandtl, V/c))
print("v = %g\n T=%g\n" %(V,T))

t = 0

#-------------------------------------------------
#-- General functions
#-------------------------------------------------
def free_stream(FCT,  XYZ) :
  if (t < dt*10):
    v = V * t/(dt*10)
  else:
    v = V
  for i in range (0,XYZ.size1()) :
    FCT.set(i,0,rho) 
    FCT.set(i,1,rho*v) 
    FCT.set(i,2,0.0) 
    FCT.set(i,3, 0.5*rho*v*v+P/(Gamma-1)/rho) 

def velocity (f, sol) :
  for i in range(0, f.size1()):
    f.set(i,0,sol.get(i,1))
    f.set(i,1,sol.get(i,2))
    f.set(i,2,0)

def rot(f, sol_grad) :
  for i in range(0,sol_grad.size1()) :
    dudy = sol_grad.get(i,1*3+1)
    dvdx = sol_grad.get(i,2*3+0)
    f.set(i,0,dudy-dvdx) 

#-------------------------------------------------
#-- Postpro
#-------------------------------------------------
def computeDragAndLift(surface, time, iter, fScale, flowDir, spanDir):
  
  print "---- Computing lift and drag ----"
  fun = open("LiftAndDrag.dat","a")
  if (iter == 0):
    fun.write("#(1)iter (2)time (3)lift (4)drag \n")
  lift = 0.0
  drag = 0.0
  
  integ=dgFunctionIntegratorInterface(groups, law.getTotalForceSurface(), solution)
  force=fullMatrixDouble(3,1)
  integ.compute(surface, force)
  
  # project force onto directions
  for j in range(0,3):
    drag = drag + force.get(j,0)*flowDir[j]
    lift = lift + force.get(j,0)*spanDir[j]
    
  drag = drag/fScale
  lift = lift/fScale
  
  #save in file
  print >>fun, iter,' ',time,' ',lift,' ',drag
  fun.close()
  print "Lift: %f Drag: %f" %(lift, drag)

#-------------------------------------------------
#-- problem setup
#-------------------------------------------------
order = 1
dimension = 2

print'*** Loading the mesh and the model ***'
model   = GModel  ()
#model.load('cyl2.msh')
model.load('Cyl100.msh')#mesh P1
groups = dgGroupCollection(model, dimension, order)
groups.splitGroupsRandom(2)
groups.buildGroupsOfInterfaces()

xyz = groups.getFunctionCoordinates()
FS = functionPython(4, free_stream, [xyz])

law=dgPerfectGasLaw(dimension)
law.setGasConstantAndGamma(R, Gamma)
muF = functionConstant(mu)
kF = functionConstant(k)
law.setViscosityAndThermalConductivity(muF, kF);

wallBoundary = law.newNonSlipWallBoundary()
slipWallBoundary = law.newSlipWallBoundary()
outsideBoundary = law.newOutsideValueBoundary("",FS)
law.addBoundaryCondition('Cylinder',wallBoundary)
law.addBoundaryCondition('WallBottom',slipWallBoundary)
law.addBoundaryCondition('WallTop',slipWallBoundary)
law.addBoundaryCondition('Inlet'     ,outsideBoundary)
law.addBoundaryCondition('Outlet'     ,outsideBoundary)

print'*** setting the initial solution ***'
solution = dgDofContainer(groups, law.getNbFields())
solution.L2Projection(FS)

#proj = dgDofContainer(groups, 1)
rotF = functionPython(1, rot, [solution.getFunctionGradient()])
velF = functionPython(3, velocity, [solution.getFunction()])

print'*** solve ***'

sys = linearSystemPETScBlockDouble ()
sys.setParameter("petscOptions", "-ksp_rtol 1e-3")
#sys.setParameter("petscOptions", "-pc_type lu")
#dof = dgDofManager.newDG (groups, law.getNbFields(), sys)
#sys.setParameter("petscOptions", "-pc_type bjacobi -pc_bjacobi_blocks  %i -sub_pc_type lu -ksp_rtol 1e-3 -ksp_max_it 50" % groups.getElementGroup(0).getNbElements())
#nbElements = groups.getElementGroup(0).getNbElements() +groups.getElementGroup(1).getNbElements() 
#sys.setParameter("petscOptions", "-pc_type bjacobi -pc_bjacobi_blocks  %i -sub_pc_type lu -ksp_rtol 1e-3 -ksp_max_it 50" % nbElements)

## matrix free bjacobi
#sys = dgLinearSystemMatrixFreeBJacobi (law, groups, True)
#sys.setTolerance(5e-3, 1e-13, 100)
dof = dgDofManager.newDGBlock (groups, law.getNbFields(), sys)
#am = dgAdamsMoulton (2, solution, law, dof, dt, t)
dirk = dgDIRK (law, dof, 2)
dirk.getNewton().setAtol(1e-5)
dirk.getNewton().setRtol(1e-6)
#dirk.getNewton().setMaxIt(6)
dirk.getNewton().setVerb(10)

#init = 10
#solution.importMsh('output/cyl-00010');
#t = dt * init;

integ=dgFunctionIntegratorInterface(groups, law.getTotalForceSurface(), solution)
force=fullMatrixDouble(3,1)
integ.compute('Cylinder', force)

flowDir=[1., 0., 0.]
spanDir=[0., 1., 0.]
D = 1.
fScale = 0.5*rho*V*V*D

tic = time.clock()
#solution.importMsh('ref/output/cyl2-00200')
#t = 4
for i in range (0, 30000):
  if (i == 80):
    dt = 0.005
  t = t + dt
  dirk.iterate(solution,dt,t)
  Msg.Info("Iter %i t=%.2g dt=%.2g cpu=%.2f" % (i,t,dt,time.clock()-tic))
  if (i % 10 == 0) :
    solution.exportMsh('output/cyl2-%05i' % (i), t, i)
    solution.exportFunctionMsh(velF, 'output/vel-%05i' % (i), t, i,"velocity")
  computeDragAndLift('Cylinder', t, i, fScale, flowDir, spanDir)
print(time.clock()-tic)
Msg.Exit(0)

