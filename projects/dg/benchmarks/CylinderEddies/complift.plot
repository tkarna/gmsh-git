#!/usr/bin/gnuplot
#set term pdfcairo font ",4"
#set output "cyl.pdf"
set multiplot;
set size 1,0.5;
set key left
set origin 0.0,0.5;
plot [:][:]'LiftAndDrag.dat' using ($1):(-$3) title 'drag comp DG' with lines lt 3 lw 2, 1.48 title '' ls 9
set origin 0.0,0.0;
plot [:][:]'LiftAndDrag.dat' using ($1):(-$2) title 'lift comp DG' with lines lt 3 lw 2, 0.34 title '' ls 9, -0.34 title '' ls 9
unset multiplot;
