r = 0.5;
//biggest element
lc = r*22;

Point(1) = {0.0,0.0,0.0};
Point(2) = {r,0.0,0.0};
Point(3) = {0,r,0.0};
Circle(1) = {2,1,3};
Point(4) = {-r,0,0.0};
Point(5) = {0,-r,0.0};
Circle(2) = {3,1,4};
Circle(3) = {4,1,5};
Circle(4) = {5,1,2};
Point(6) = {0,0,-r};
Point(7) = {0,0,r};
Circle(5) = {3,1,6};
Circle(6) = {6,1,5};
Circle(7) = {5,1,7};
Circle(8) = {7,1,3};
Circle(9) = {2,1,7};
Circle(10) = {7,1,4};
Circle(11) = {4,1,6};
Circle(12) = {6,1,2};
Line Loop(13) = {2,8,-10};
Ruled Surface(14) = {13};
Line Loop(15) = {10,3,7};
Ruled Surface(16) = {15};
Line Loop(17) = {-8,-9,1};
Ruled Surface(18) = {17};
Line Loop(19) = {-11,-2,5};
Ruled Surface(20) = {19};
Line Loop(21) = {-5,-12,-1};
Ruled Surface(22) = {21};
Line Loop(23) = {-3,11,6};
Ruled Surface(24) = {23};
Line Loop(25) = {-7,4,9};
Ruled Surface(26) = {25};
Line Loop(27) = {-4,12,-6};
Ruled Surface(28) = {27};
Surface Loop(29) = {28,26,16,14,20,24,22,18};
Volume(30) = {29};

R = 15*r;
Point(8) =  {-R, R,-R};
Point(9) =  { R, R,-R};
Point(10) = {-R,-R,-R};
Point(11) = { R,-R,-R};
Line(31) = {8,9};
Line(32) = {9,11};
Line(33) = {11,10};
Line(34) = {10,8};
Line Loop(35) = {31,32,33,34};
Plane Surface(36) = {35};
Extrude {0,0,3*R} {
  Surface{36};
}
Surface Loop(59) = {49,36,45,58,53,57};
Volume(60) = {59,29};
Delete {
  Volume{30};
}

Physical Surface("Box") = {45, 49, 58, 53, 57, 36};
Physical Surface("Sphere") = {14, 16, 18, 20, 24, 26, 22, 28};
Physical Volume("Fluid") = {60};

// refine using a global factor in a cylinder region
Field[4] = MathEval;
Field[4].F = "Sqrt(x*x + y*y)";
Field[5] = Threshold;
Field[5].IField = 4;
Field[5].LcMin = lc/5;
Field[5].LcMax = lc;
Field[5].DistMax = r * 20;
Field[5].DistMin = r * 4;


/*
Field[11] = Threshold;
Field[11].DistMax = 2;
Field[11].DistMin = 0.5;
Field[11].IField = 4;
Field[11].LcMax = 1;
Field[11].LcMin = 0.3;
*/


//refine in the z direction
Field[6] = MathEval;
Field[6].F = "z";
//after the sphere
Field[7] = Threshold;
Field[7].IField = 6;
Field[7].DistMax = 40 * r;
Field[7].DistMin = 2 * r;
Field[7].LcMax = 1;
Field[7].LcMin = 0.25;
//before the sphere
Field[9] = Threshold;
Field[9].IField = 6;
Field[9].DistMin = -6*r;
Field[9].DistMax = -2 *r;
Field[9].LcMin = 1;
Field[9].LcMax = 0.25;


//boundary layer on the sphere
Field[12] = MathEval;
Field[12].F = "Sqrt(x*x + y*y + z*z)";
Field[13] = Threshold;
Field[13].IField = 12;
Field[13].DistMin = lc /10;
Field[13].DistMax = lc / 5;
Field[13].LcMin = 0.3;
Field[13].LcMax = 1;

//global combinaison
Field[10] = Max;
Field[10].FieldsList = {7, 9};
Field[8] = MathEval;
Field[8].F = "F10*F5*F13";
Background Field = 8;
