#check inkscape options before editing :
# snap to point
# do not preserve path on remove
# no relative coordinates in export

import math
import xml.sax.handler

R = 6.371e6

########################################################################################
## parameters
########################################################################################

# inverse of the transformation defined in "svgOutputCoordinates" in gshhs2geo
# input from the svg file, output in cartesian coordinates on a sphere of radius 1
def inputCoordinates (u, v):
  ## stereographic coordinates
  u = u / 50 - 10
  v = v / 50 - 10
  z =  (1 - u*u - v*v) / (1 + u*u + v*v)
  x = -u * (1 + z)
  y = -v * (1 + z)
  ## lon lat
  #f = 1000 / (2 * math.pi)
  #lon = (u / f - math.pi)
  #lat = math.pi / 2 - v / f
  #x = math.cos(lat) * math.cos(lon)
  #y = math.cos(lat) * math.sin(lon)
  #z = math.sin(lat)
  return x, y, z 
  
########################################################################################
## parameters
########################################################################################

class SVGHandler(xml.sax.handler.ContentHandler):
  def __init__(self, ofile):
    self.inTitle = False
    self.ofile = ofile
    self.title =""
    self.ip = 2
    self.il = 0
    self.physicalLines = {}
    self.connectingNodes = [] 
    ofile.write("IP = newp;\nIL = newl;\nIS = news;IF = newf;\n");
    ofile.write("Point(IP + 0) = {0, 0, 0};\nPoint(IP + 1) = {0, 0, %e};\nPolarSphere(IS) = {IP + 0, IP + 1};\n" % R);
  def startElement(self, name, attributes):
    if name == "path":
      self.title = ""
      self.path = attributes["d"]
    elif name == "title":
      self.inTitle = True
  def characters(self, data):
    if self.inTitle:
      if data =='\n':
        self.inTitle = False
      else :
        self.title += data
  def endElement(self, name):
    if name == "path":
      self.inTitle = False
      self.writePath(self.path)
  def writeNode(self, node):
    [x, y, z] = node
    u = -x / (1 + z)
    v = -y / (1 + z)
    ofile.write("Point(IP + %i) = {%.16e, %.16e, 0.};\n" % (self.ip, u, v));
    self.ip = self.ip + 1
  def getConnectingNode(self, node):
    for cnode in self.connectingNodes :
      d = [cnode[0] - node[0], cnode[1] - node[1], cnode[2] - node[2]]
      if (d[0] * d[0] + d[1] * d[1] + d[2] * d[2]) < (1000 / R) ** 2 :
        return cnode[3]
    self.connectingNodes.append([node[0], node[1], node[2], self.ip])
    self.writeNode(node)
    return self.ip - 1
  def writePath(self, path):
    nodes = path.split()
    closed = (nodes[-1] == "z" or nodes[-1] == "Z")
    nodesonly = []
    for snode in nodes :
      if (len(snode.split(",")) == 2) :
        coords = snode.split(",")
        u = float(coords[0])
        v = float(coords[1])
        nodesonly.append(inputCoordinates(u, v))
      elif (snode != "M" and snode != "Z" and snode != "z"):
        print("skipping invalid path %s (%s)" % (path, snode))
        return
    if len(nodesonly) < 2 :
      return
    if not closed :
      beginp = self.getConnectingNode(nodesonly[0])
      endp = self.getConnectingNode(nodesonly[-1])
      nodesonly.pop(0)
      nodesonly.pop(-1)
    firstp = self.ip
    for node in nodesonly:
      self.writeNode(node)
    if len(nodesonly) > 2:
      if closed :
        ofile.write("BSpline(IL + %i) = {IP + %i : IP + %i, IP + %i};\n" % (self.il, firstp, self.ip - 1, firstp))
      else:
        ofile.write("BSpline(IL + %i) = {IP + %i, IP + %i : IP + %i, IP + %i};\n" % (self.il, beginp, firstp, self.ip - 1, endp))
    else:
      ofile.write("Line(IL + %i) = {IP + %i, IP + %i};\n" % (self.il, beginp, endp))
    if (self.title != ""):
      if (self.title in self.physicalLines):
        self.physicalLines[self.title].append(self.il)
      else:
        self.physicalLines[self.title] = [self.il]
    self.il = self.il + 1;
  def endDocument(self):
    for tag, lines in self.physicalLines.items():
      self.ofile.write("Physical Line (\"%s\") = {IL + %i" % (tag, lines[0]));
      for i in range(1, len(lines)):
        self.ofile.write(", IL + %i" % lines[i]);
      self.ofile.write("};\n");
    self.ofile.write("Field[IF + 0] = Attractor;\nField[IF + 0].NodesList = {IP + 2 : IP + %i};\n" % (self.ip -1));

parser = xml.sax.make_parser()
ofile = open("world2.geo", "w")
handler = SVGHandler(ofile)
parser.setContentHandler(handler)
parser.parse("world.svg")
