import sys

if(len(sys.argv) > 1):
  size = int(sys.argv[1]);
else:
  size = 100

f = open('diGiammarco_quads' + str(size) + 'm.msh','w')

print >>f, '''$MeshFormat
2.2 0 8
$EndMeshFormat
$PhysicalNames
7
1 3 "sidedown"
1 4 "down"
1 5 "sideup"
1 6 "up"
1 7 "outlet"
2 1 "slope"
2 2 "channel"
$EndPhysicalNames
$Nodes'''


nbx = 800 / size
nby = 1000 / size

nbxc = 1
nbyc = nby

nbPtX = nbx+nbxc+1

print >>f, (nbx+1)*(nby+1) + nbyc+1

# points => coordinates

i = 1
for iy in range(nby+1):
  for ix in range(nbx+1):
    print >>f, i, ix * size, iy * size, 1. + (nbx-ix) * 40./nbx + iy * 20./nby
    i=i+1
  print >>f, i, ix * size + 10, iy * size, 1. + iy * 20./nby
  i=i+1

print >>f, '$EndNodes\n$Elements'
print >>f, nbx*nby + nbyc + 2*nbx+2 + nby + nbyc

# quads

i = 1
for iy in range(nby):
  for ix in range(nbx):
    print >>f, i, 3, 2, 1, 1, iy*nbPtX + ix+1, iy*nbPtX + ix+2, (iy+1)*nbPtX + ix+2, (iy+1)*nbPtX + ix+1
    i=i+1
  print >>f, i, 3, 2, 2, 2, iy*nbPtX + nbx+1, iy*nbPtX + nbx+2, (iy+1)*nbPtX + nbx+2, (iy+1)*nbPtX + nbx+1
  i=i+1

# lines sides

for ix in range(nbx):
  print >>f, i, 1, 2, 3, 3, ix+1, ix+2
  i=i+1
  print >>f, i, 1, 2, 5, 5, nby*nbPtX + ix+2, nby*nbPtX + ix+1
  i=i+1
print >>f, i, 1, 2, 7, 7, nbx+1, nbx+2
i=i+1
print >>f, i, 1, 2, 5, 5, nby*nbPtX + nbx+2, nby*nbPtX + nbx+1
i=i+1

# lines up/down

for iy in range(nby):
  print >>f, i, 1, 2, 6, 6, (iy+1)*nbPtX + 1, iy*nbPtX + 1
  i=i+1
  print >>f, i, 1, 2, 4, 4, iy*nbPtX + nbx+2, (iy+1)*nbPtX + nbx+2
  i=i+1

print >>f, '$EndElements'

