from SWNI import *
from utilities import *
import time

#############
# FUNCTIONS #
#############


CFunctions = ''' 
#include "fullMatrix.h"
#include "function.h"
extern "C" {

void analyt (dataCacheMap *m, fullMatrix<double> &out, fullMatrix<double> &xyz) {
  for (size_t i = 0; i < out.size1(); i++) {
    double x = xyz(i, 0);
    out.set(i, 0, 1.5 * exp(-1.5 * (x-2) * (x-2)) );
  }
}

}
'''

tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CFunctions, tmpLib);
Msg.Barrier()

##################
# INITIALISATION #
##################

# definition of land surface

lsm = LandSurfaceMap()
surface = landSurfManning(1.)
lsm.add("domain", surface)

# construction of the law

Soptions = SWNIOptions(isImplicit=True, controlVolumeFE=True, setVerb=1, DMax=10, setMaxIt=50)
S = SWNI('meshes/line_converging.msh', finalizeInitialisation=False, options=Soptions)
S.finalizeInit(lsm, source=None)

# boundary conditions

C0 = functionConstant(0)
S.bndNeumann(C0, ['lB', 'rB'])

# initial conditions

analytic = functionC(tmpLib, "analyt", 1, [S.XYZ])
S.setH(analytic)

# time and exports

h = slimIterateHelper()
h.setTime("0:.002:5")
h.setExport(".2")
h.setFileName("output/A2_newconvergingPlanes")
h.addExport(S.H, "newh")
#h.addExport(S.H, mean_h, "mean_h")
h.addExport(S.H, analytic, "analytic")
h.addExport(S.H, S.nl, "newnlTerm")
h.exportBin(True)
h.setNbMaxIter(1000000)
printEach = 100
i=0; x = time.clock(); t=0; dt=1; norm=0; normH=0; nbIter=1
nbIMin = 1000; nbIMax = 0; nbIMean = 0

#h.importAll(28, 2.8)

glbMass = S.getGlobalMass()
if (Msg.GetCommRank()==0) :
  print '|IT|', 0, '|DT|', dt, '|t|', 0, '|CPU|', '%.1e' % (0),
  print '|Mass|', '%0.16g' % (glbMass), '|NORM|', "--"

while (not h.isTheEnd()):
  i = i + 1
#  CFL = rk.computeInvSpectralRadius(law, solution_h)
#  print 'cfl = ', CFL
#  h.setMaxDt(min(CFL * factCFL, 10))
  dt = h.getNextTimeStepAndExport()
  t = h.getCurrentTime()

  nbIter = S.iterate(dt, t)

  nbIMin = min(nbIMin, nbIter)
  nbIMax = max(nbIMax, nbIter)
  nbIMean = nbIMean + nbIter

  if (i % printEach == 0) :
    glbMass = S.getGlobalMass()
    nbIMean = float(nbIMean) / printEach;
    if (Msg.GetCommRank() == 0) :
      print '|IT|', '%6d' % (i), '|DT|', dt, '|t|','%.2e' % (t), '|CPU|', '%.1e' % (time.clock()-x),
      print '|Mass|', '%21.16g' % (glbMass), '|NbIter|', '%4g<%4g<%4g' % (nbIMin, nbIMean, nbIMax)
    nbIMin = 1000; nbIMax = 0; nbIMean = 0

  if (isnan(nbIter) or not nbIter) :
    glbMass = S.getGlobalMass()
    if(Msg.GetCommRank() == 0) :
      if(isnan(nbIter)):
        print('NAN, abording . last iter is')
      else:
        print('not converged, abording . Last iter is')
      print '|IT|', i, '|DT|', dt, '|t|', t, '|CPU|', '%.1e' % (time.clock()-x),
      print '|Mass|', '%21.16g' % (glbMass), '|NORM|', '%21.16g' % (norm)
    h.exportAll()
    break

glbMass = S.getGlobalMass()
if (Msg.GetCommRank()==0) :
  print '|IT|', '%6d' % (i), '|DT|', dt, '|t|','%.2e' % (t), '|CPU|', '%.1e' % (time.clock()-x),
  print '|Mass|', '%21.16g' % (glbMass), '|NORM|', '%21.16g' % (norm)


