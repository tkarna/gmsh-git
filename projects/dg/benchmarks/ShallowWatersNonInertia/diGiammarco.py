from SWNI import *
import time


# V-catchment
# 3.0e-6 m/s for 5400 s

CFunctions = ''' 
#include "fullMatrix.h"
#include "function.h"
extern "C" {
  
  void rain (dataCacheMap *m, fullMatrix<double> &out, fullMatrix<double> &time) {
    for(size_t i = 0; i < out.size1(); i++) {
      if(time(0, 0) <= 5400.)
        out.set(i, 0, 3e-6);
      else
        out.set(i, 0, 0.);
    }
  }

  void ini (dataCacheMap *m, fullMatrix<double> &out) {
    for(size_t i = 0; i < out.size1(); i++) {
      out.set(i, 0, 0.0001);
    }
  }
}
'''
tmpLib = u.makeLib(CFunctions, "tmp.dylib")

TIME = function.getTime();

rain = functionC(tmpLib, "rain", 1, [TIME])

#mesh= 'meshes/diGiammarco_fromGW.msh' # quads 
#mesh= 'meshes/diGiammarco_GWmod.msh' # quads mod
#mesh= 'meshes/diGiammarco_mod.msh' # triangles mod
#mesh= 'meshes/diGiammarco_triangles.msh' # triangles
#mesh = 'meshes/oneQuad.msh'
#mesh = 'meshes/twoQuads.msh'
mesh = 'meshes/diGiammarco_quads10m.msh'

#Qc = quads10m pure CVFE
#Qd = quads20m pure CVFE
#Qe = quads40m pure CVFE
#Qf = quads100m pure CVFE
#Qg = quads100m CVFE maxed
#Qh = quads20m CVFE maxed
#Qi = quads10m CVFE maxed

lsm = LandSurfaceMap()
surface = landSurfManning(0.015) #0.015
channel = landSurfManning(0.15) #0.15
lsm.add("slope", surface)
lsm.add("channel", channel)


Soptions = SWNIOptions(residualSat=1e-10, mobileWaterDepth=1e-10, microTopo=1e-10, setMaxIt=15, setVerb=0, controlVolumeFE=True, controlVolumeFEinterface=True, isDG=False, DMax=10000000)
S = SWNI(mesh, lsm, source=rain, options=Soptions)#, tags=["channel"])

C0 = functionConstant(0)
S.bndNeumann(C0, ['sidedown', 'sideup', 'up', 'down'])
S.bndCriticalDepth('outlet')
#S.bndNeumann(C0, 'outlet')
#S.bndNeumann(C0, 'none')

INI = functionC(tmpLib, "ini", 1, [])
S.setH(INI)

f = open('outputdata/diGiammarco_outlet_fluxQi','w')

h = slimIterateHelper()
h.setInitialDate(0)
h.setFinalDate(10000)
h.setExport('500:5,100,200,300')
h.setFileName("output/diGiammarcoQi")
h.addExport(S.H, "sH")
h.exportBin(True)
h.printExport(True)
h.setNbMaxIter(1000000)
printEach = 100

i = 0; dt=1; t = 0;
nbIMin = 1000; nbIMax = 0; nbIMean = 0

glbMass = S.getGlobalMass()
if (Msg.GetCommRank()==0) :
  print '|IT|', 0, '|DT|', dt, '|t|', t, '|CPU|', '%.1e' % (0),
  print '|Mass|', '%0.16g' % (glbMass)

x = time.clock()
h.setMaxDt(dt)

while (not h.isTheEnd()):
  i = i + 1
  dt = h.getNextTimeStepAndExport()
  t = h.getCurrentTime()

  nbIter = S.iterate(dt, t)

  nbIMin = min(nbIMin, nbIter)
  nbIMax = max(nbIMax, nbIter)
  nbIMean = nbIMean + nbIter

  if (i % printEach == 0) :
    glbMass = S.getGlobalMass()
    nbIMean = float(nbIMean) / printEach;
    bndFlux = S.getBoundaryFlux('outlet')
    if (Msg.GetCommRank() == 0) :
      print '|IT|', '%6d' % i, '|DT|', dt, '|t|','%.2e' % (t), '|CPU|', '%.1e' % (time.clock()-x),
      print '|Mass|', '%21.16g' % glbMass, '|Flux|', '%12.6e' % bndFlux, '|NbIter|', '%4g<%4g<%4g' % (nbIMin, nbIMean, nbIMax)
      print >>f, '%6.3e' % (t), '%21.16e' % (bndFlux)
    nbIMin = 1000; nbIMax = 0; nbIMean = 0

