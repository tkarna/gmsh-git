//GMSH geometry file

Mesh.Algorithm = 1;

lb = 25.00;	// External boundary
lc = 12;	// Internal boundary
li = 1;	// Island boundary
lt = 1;	// Tip of island

// External boundary
Point(0) = {-25.0, -300, 0.0, lb};
Point(1) = {125.0, -300, 0.0, lb};
Point(2) = {125.0, 400, 0.0, lb};
Point(3) = {-25.0, 400, 0.0, lb};

// Region of interest around boundary
Point(4)  = {50, 50, 0.0, lc};
Point(5)  = {50, 0, 0.0, lc};
Point(6) = {5, 50, 0.0, lc};
Point(7) = {50, 100, 0.0, lc};
Point(8) = {95, 50, 0.0, lc};

// Island boundary
Point(10) = {50, 50, 0.0, lt};
Point(11) = {50, 45, 0.0, li};
Point(12) = {60, 50, 0.0, li};
Point(13) = {50, 55, 0.0, li};
Point(14) = {40, 50, 0.0, li};

// Exterior contour
Line(0) = {0,1};
Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,0};

// Island
Ellipse(10) = {11,10,12,12};
Ellipse(11) = {12,10,13,13};
Ellipse(12) = {13,10,14,14};
Ellipse(13) = {14,10,11,11};

// Internal region
Ellipse(5) = {6,4,5,5};
Ellipse(6) = {5,4,8,8};
Ellipse(7) = {8,4,7,7};
Ellipse(8) = {7,4,6,6};

Line Loop(4) = {0,1,2,3};
Line Loop(5) = {10,11,12,13};
Line Loop(6) = {5,6,7,8};


Plane Surface(7) = {4,6};
Plane Surface(8) = {6,5};

Physical Line("lateral")={1, 3};
Physical Line("input")={2};
Physical Line("output")={0};
Physical Line("island")={10,11,12,13};

Physical Surface("domain")={7, 8};
