from dgpy import *
from math import *
import os
try : os.mkdir("output"); 
except: 0;

ref=3
Tf=0.5

order=1
FileName="island"
fct=1

def initial(FCT, XYZ):
  for i in range(0,XYZ.size1()):
    y = (XYZ(i,1)/225) 
    FCT.set(i, 0, 0.05 * exp(-((y-1)*8)**2));
    FCT.set(i, 1, 0);
    FCT.set(i, 2, 0);

def bathymetry(FCT, XYZ):
  for i in range(0,XYZ.size1()):
    x = (XYZ(i,0)/50) 
    y = (XYZ(i,1)/50) 
    FCT.set(i, 0, 10-5 * exp(-((x-1)*1.25)**2-((y-1)*2.5)**2));
    #FCT.set(i, 0, 10-5 * exp(-((x-1)*2.5)**2-((y-1)*1.25)**2));

CCode = """
#include "fullMatrix.h"
#include "function.h"
extern "C" {
void bottomDrag(dataCacheMap *,  fullMatrix<double> &val,  fullMatrix<double> &sol,  fullMatrix<double> &bath){
  for(int i=0;  i<val.size1(); i++){
    double H = sol(i,  0)+bath(i,  0);
    val.set(i,  0,  9.81*0.0235*0.0235/(pow(H,  1.333333333333)));
  }
}
void Initial(dataCacheMap *,  fullMatrix<double> &val,  fullMatrix<double> &sol){
  for(int i=0;  i<val.size1(); i++){
    double s = sol(i,  0);
    val.set(i,  0,  s);
    val.set(i,  1,  0);
    val.set(i,  2,  0);
  }
}
}
"""

tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CCode, tmpLib);

def loadAndPartitionMesh(model,  fileName) :
  if (Msg.GetCommSize() > 1) :
    Msg.Info("partition mesh")
    if(Msg.GetCommRank()==0) :
      m2 = GModel()
      m2.load("output/"+fileName + '.msh')
      pOpt = meshPartitionOptions()
      pOpt.setNumOfPartitions(Msg.GetCommSize())
      pOpt.partition(m2)
      m2.save("output/"+fileName + '_partitioned.msh')
    Msg.Barrier()
    Msg.Info("partition done")
    model.load("output/"+fileName + '_partitioned.msh')
  else :
    model.load("output/"+fileName + '.msh')




g2 = GModel()
if(Msg.GetCommRank()==0):
  gm = GModel()
  #gm.load (FileName+'.geo')
  #gm.mesh(2)
  gm.load(FileName+'.msh')
  for i in range(1, ref+1):
    gm.refineMesh(True)
  gm.save("output/"+FileName+"-ref-%i"'.msh' % ref)

loadAndPartitionMesh(g2, FileName+"-ref-%i" % ref)

groups = dgGroupCollection(g2, 2, order)
groups.buildGroupsOfInterfaces()

theta=-17.147569/180*pi
claw = dgConservationLawShallowWater2d()
dof = dgDofContainer(groups, claw.getNbFields())
bathC = dgDofContainer(groups, 1)
dof1 = dgDofContainer(groups, 1)
claw.addBoundaryCondition('lateral', claw.newBoundaryWall())
claw.addBoundaryCondition('input', claw.newBoundaryWall())
claw.addBoundaryCondition('output', claw.newBoundaryWall())
claw.addBoundaryCondition('island', claw.newBoundaryWall())
XYZ = groups.getFunctionCoordinates();
#init = functionPython(1, initial, [XYZ])
init = functionPython(3, initial, [XYZ])
f0 = functionConstant([ 2*7.292e-5*sin(theta)])
claw.setCoriolisFactor(f0)
#f1 = functionConstant(0)
f1 = functionC(tmpLib,  'bottomDrag',  1,  [dof.getFunction(),bathC.getFunction()])
claw.setQuadraticDissipation(f1)
claw.setIsLinear(1)
f2 = functionConstant([0])
claw.setLinearDissipation(f2)
#f4 = functionConstant([10])
#claw.setBathymetry(f4)

f4 = functionPython(1, bathymetry, [XYZ])
ls=linearSystemPETScDouble()
ls.setParameter("petscOptions",  "-ksp_monitor_true_residual -ksp_rtol 1e-15 -ksp_atol 0")
df=dgDofManager.newCG(groups,  1,  ls)
l2p=L2ProjectionContinuous(df)
l2p.apply(bathC, f4)
claw.setBathymetry(bathC.getFunction())
claw.setBathymetryGradient(bathC.getFunctionGradient())

#f5=functionConstant([0.01])
#claw.setDiffusivity(f5)

dof.L2Projection(init)


groups.deleteGroupsOfInterfaces()
bathC.exportMsh("output/bath", 0, 0)

mL=1
rkmr=dgRungeKuttaMultirateConservative.new44(groups,claw)

dt=rkmr.splitForMultirate(mL, dof, [dof, bathC])
dt=dt/fct
#rk = dgERK(claw,   None,   DG_ERK_44)
rk = dgERK(claw,   None,   DG_ERK_44)


groups.buildGroupsOfInterfaces()

t=0.
print dt
nbSteps=int(ceil(Tf/dt))

exporterInit=dgIdxExporter(dof, "output/dof-ref-%i-init" % (ref))
exporterEnd=dgIdxExporter(dof, "output/dof-ref-%i-end" % (ref))
exporterInit.exportIdx()
for it in range (1, nbSteps+1):
  if(Tf-t<dt):
    dt=Tf-t
  rk.iterate(dof,   dt,  t)
  t=t+dt
  print 'it=', it, 'Time=', t, 'Norm=', dof.norm()
exporterEnd.exportIdx()

