from dgpy import *
from math import *
import time
import os
try : os.mkdir("output"); 
except: 0;

FileName="island"

order=1

MR=1
RK=43

Tf=200

def massIntegrator(fct,  sol, XYZ):
  for i in range(0,XYZ.size1()):
    x = (XYZ(i,0)/50) 
    y = (XYZ(i,1)/50) 
    s=sol.get(i, 0)
    depth=10-5 * exp(-((x-1)*2.5)**2-((y-1)*1.25)**2)
    fct.set(i, 0, s+depth)

def initial(FCT, XYZ):
  for i in range(0,XYZ.size1()):
    y = (XYZ(i,1)/225) 
    FCT.set(i, 0, 0.05 * exp(-((y-1)*8)**2));
    FCT.set(i, 1, 0);
    FCT.set(i, 2, 0);

def bathymetry(FCT, XYZ):
  for i in range(0,XYZ.size1()):
    x = (XYZ(i,0)/50) 
    y = (XYZ(i,1)/50) 
    FCT.set(i, 0, 10-5 * exp(-((x-1)*2.5)**2-((y-1)*1.25)**2));

CCode = """
#include "fullMatrix.h"
#include "function.h"
extern "C" {
void bottomDrag(dataCacheMap *,  fullMatrix<double> &val,  fullMatrix<double> &sol,  fullMatrix<double> &bath){
  for(int i=0;  i<val.size1(); i++){
    double H = sol(i,  0)+bath(i,  0);
    val.set(i,  0,  9.81*0.0235*0.0235/(pow(H,  1.333333333333)));
  }
}
}
"""

tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CCode, tmpLib);

g = GModel()
g.load(FileName+".msh")

groups = dgGroupCollection(g, 2, order)
groups.buildGroupsOfInterfaces()

theta=-17.147569/180*pi
claw = dgConservationLawShallowWater2d()
dof = dgDofContainer(groups, claw.getNbFields())
bathC = dgDofContainer(groups, 1)
dof1 = dgDofContainer(groups, 1)
claw.addBoundaryCondition('lateral', claw.newBoundaryWall())
claw.addBoundaryCondition('input', claw.newBoundaryWall())
claw.addBoundaryCondition('output', claw.newBoundaryWall())
claw.addBoundaryCondition('island', claw.newBoundaryWall())
XYZ = groups.getFunctionCoordinates();
#init = functionPython(1, initial, [XYZ])
init = functionPython(3, initial, [XYZ])
f0 = functionConstant([ 2*7.292e-5*sin(theta)])
claw.setCoriolisFactor(f0)
#f1 = functionConstant(0)
f1 = functionC(tmpLib,  'bottomDrag',  1,  [dof.getFunction(),bathC.getFunction()])
claw.setQuadraticDissipation(f1)
claw.setIsLinear(1)
f2 = functionConstant([0])
claw.setLinearDissipation(f2)

f4 = functionPython(1, bathymetry, [XYZ])
ls=linearSystemPETScDouble()
ls.setParameter("petscOptions",  "-ksp_monitor_true_residual -ksp_rtol 1e-15 -ksp_atol 0")
df=dgDofManager.newCG(groups,  1,  ls)
l2p=L2ProjectionContinuous(df)
l2p.apply(bathC, f4)
claw.setBathymetry(bathC.getFunction())
claw.setBathymetryGradient(bathC.getFunctionGradient())

#f5=functionConstant([0.01])
#claw.setDiffusivity(f5)

dof.L2Projection(init)

# ls2=linearSystemPETScDouble()
# ls2.setParameter("petscOptions",  "-ksp_monitor_true_residual -ksp_rtol 1e-15 -ksp_atol 0")
# df2=dgDofManager.newCG(groups,  1,  ls2)
# l2p2=L2ProjectionContinuous(df2)
# l2p2.apply(dof1, init)
# initial2 = functionC(tmpLib,  'Initial',  3,  [dof1.getFunction()])
# dof.L2Projection(initial2)

groups.deleteGroupsOfInterfaces()

if(MR==0):
  mL=1
else:
  mL=1000


if(RK==22):
  rk=dgRungeKuttaMultirateConservative.new2a(groups,claw)
elif(RK==33):
  rk=dgRungeKuttaMultirateConservative.new33(groups,claw)
elif(RK==44):
  rk=dgRungeKuttaMultirateConservative.new44(groups,claw)
elif(RK==43):
  rk=dgRungeKuttaMultirate43(groups,claw)

dt=rk.splitForMultirate(mL, dof, [dof, bathC])

groups.buildGroupsOfInterfaces()

mass=functionPython(1,  massIntegrator,  [functionSolution.get(), XYZ])
integrator = dgFunctionIntegrator(groups,  mass,  dof)
totalMass=fullMatrixDouble(1, 1)
totalMassRef=fullMatrixDouble(1, 1)
integrator.compute(totalMassRef, "")

t=0.
nbSteps=int(ceil(Tf/dt))
file=open(str(MR)+'_'+str(RK)+"_conservation.txt", "w")
for it in range (1, nbSteps+1):
  if(Tf-t<dt):
    dt=Tf-t
  rk.iterate(t, dt, dof)
  t=t+dt
  integrator.compute(totalMass, "")
  file.write("(%f \t ,  %.17e ) \n"%(t, (totalMass.get(0,  0)-totalMassRef.get(0,  0))/totalMassRef.get(0,  0)))
  print 'T', t, 'MASS',(totalMass.get(0,   0)-totalMassRef.get(0,   0))/totalMassRef.get(0, 0)

file.close()
Msg.Exit(0)

