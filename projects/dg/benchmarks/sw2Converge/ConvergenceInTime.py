from dgpy import *
from math import *
import time
import os
try : os.mkdir("output"); 
except: 0;

FileName="island"

order=1

MR=1
RK=43
ME=1000

nstage = 3
Tf=0.5


ref=0
nPartRef=10

def initial(FCT, XYZ):
  for i in range(0,XYZ.size1()):
    y = (XYZ(i,1)/225) 
    FCT.set(i, 0, 0.05 * exp(-((y-1)*8)**2));
    FCT.set(i, 1, 0);
    FCT.set(i, 2, 0);

def bathymetry(FCT, XYZ):
  for i in range(0,XYZ.size1()):
    x = (XYZ(i,0)/50) 
    y = (XYZ(i,1)/50) 
    FCT.set(i, 0, 10-5 * exp(-((x-1)*2.5)**2-((y-1)*1.25)**2));

CCode = """
#include "fullMatrix.h"
#include "function.h"
extern "C" {
void bottomDrag(dataCacheMap *,  fullMatrix<double> &val,  fullMatrix<double> &sol,  fullMatrix<double> &bath){
  for(int i=0;  i<val.size1(); i++){
    double H = sol(i,  0)+bath(i,  0);
    val.set(i,  0,  9.81*0.0235*0.0235/(pow(H,  1.333333333333)));
  }
}
void Initial(dataCacheMap *,  fullMatrix<double> &val,  fullMatrix<double> &sol){
  for(int i=0;  i<val.size1(); i++){
    double s = sol(i,  0);
    val.set(i,  0,  s);
    val.set(i,  1,  0);
    val.set(i,  2,  0);
  }
}
}
"""

tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CCode, tmpLib);

g2 = GModel()
#g2.load(FileName+".geo")
#g2.mesh(2)
#g2.save("output/"+FileName+".msh")
g2.load(FileName+".msh")
conv = []
cpu=[]
alldt=[]
su=[]

for i in range(0,nstage + 1):
  g2.save("output/"+FileName+"-%i.msh" % 0)

  g = GModel()
  g.load("output/"+FileName+"-%i.msh" % 0)
  groups = dgGroupCollection(g, 2, order)
  groups.buildGroupsOfInterfaces()

  theta=-17.147569/180*pi
  claw = dgConservationLawShallowWater2d()
  dof = dgDofContainer(groups, claw.getNbFields())
  bathC = dgDofContainer(groups, 1)
  dof1 = dgDofContainer(groups, 1)
  claw.addBoundaryCondition('lateral', claw.newBoundaryWall())
  claw.addBoundaryCondition('input', claw.newBoundaryWall())
  claw.addBoundaryCondition('output', claw.newBoundaryWall())
  claw.addBoundaryCondition('island', claw.newBoundaryWall())
  XYZ = groups.getFunctionCoordinates();
  #init = functionPython(1, initial, [XYZ])
  init = functionPython(3, initial, [XYZ])
  f0 = functionConstant([ 2*7.292e-5*sin(theta)])
  claw.setCoriolisFactor(f0)
  #f1 = functionConstant(0)
  f1 = functionC(tmpLib,  'bottomDrag',  1,  [dof.getFunction(),bathC.getFunction()])
  claw.setQuadraticDissipation(f1)
  claw.setIsLinear(1)
  f2 = functionConstant([0])
  claw.setLinearDissipation(f2)

  f4 = functionPython(1, bathymetry, [XYZ])
  ls=linearSystemPETScDouble()
  ls.setParameter("petscOptions",  "-ksp_monitor_true_residual -ksp_rtol 1e-15 -ksp_atol 0")
  df=dgDofManager.newCG(groups,  1,  ls)
  l2p=L2ProjectionContinuous(df)
  l2p.apply(bathC, f4)
  claw.setBathymetry(bathC.getFunction())
  claw.setBathymetryGradient(bathC.getFunctionGradient())

  #f5=functionConstant([0.01])
  #claw.setDiffusivity(f5)

  dof.L2Projection(init)

 # ls2=linearSystemPETScDouble()
 # ls2.setParameter("petscOptions",  "-ksp_monitor_true_residual -ksp_rtol 1e-15 -ksp_atol 0")
 # df2=dgDofManager.newCG(groups,  1,  ls2)
 # l2p2=L2ProjectionContinuous(df2)
 # l2p2.apply(dof1, init)
 # initial2 = functionC(tmpLib,  'Initial',  3,  [dof1.getFunction()])
 # dof.L2Projection(initial2)

  groups.deleteGroupsOfInterfaces()

  if(MR==0):
    mL=1
  else:
    mL=ME



  if(RK==22):
    rk=dgRungeKuttaMultirateConservative.new2a(groups,claw)
  elif(RK==33):
    rk=dgRungeKuttaMultirateConservative.new33(groups,claw)
  elif(RK==44):
    rk=dgRungeKuttaMultirateConservative.new44(groups,claw)
  elif(RK==43):
    rk=dgRungeKuttaMultirate43(groups,claw)

  fct=pow(2, i)

  dt=rk.splitForMultirate(mL, dof, [dof, bathC])
  dt=dt/fct
  #rknew = dgERK(claw,   None,   DG_ERK_44)

  groups.buildGroupsOfInterfaces()
  alldt.append(dt)
  su.append(groups.getMRspeedUp())

  t=0.
  tic=time.clock()
  nbSteps=int(ceil(Tf/dt))
  #dof.exportMsh("output/dof-%i-init" % (i), 0 *dt, 0)
  for it in range (1, nbSteps+1):
    if(Tf-t<dt):
      dt=Tf-t
    #rknew.iterate(dof,   dt,  t)
    rk.iterate(t, dt, dof)
    t=t+dt
    print t
    #dof.exportMsh("output/dof-%i-%05i" % (i,it), it *dt, it)
  toc=time.clock()
  cpu.append(toc-tic)
  dof.exportMsh("output/dof-%i-end" % (i))
  print 'norm=', dof.norm()

gRef = GModel()
gRef.load("output/"+FileName+"-ref-%i.msh" % ref)
groupsRef = dgGroupCollection(gRef, 2, order)
dofRef = dgDofContainer(groupsRef, 3)
dofRef.importMsh("output/dof-ref-%i-end" % ref)
XYZRef = groupsRef.getFunctionCoordinates()
solRef = dofRef.getFunction()

def compareE(FCT, F1, F2):
  for i in range(0,F1.size1()):
    FCT.set(i, 0, (F1(i,0) -F2(i,0))** 2);

def compareV(FCT, F1, F2):
  for i in range(0,F1.size1()):
    FCT.set(i, 0, (F1(i,1) -F2(i,1))** 2+(F1(i,2) -F2(i,2))**2) ;

def difference(FCT, F1, F2):
  for i in range(0,F1.size1()):
    FCT.set(i, 0, (F1(i,0) -F2(i,0)));
    FCT.set(i, 1, (F1(i,1) -F2(i,1)));
    FCT.set(i, 2, (F1(i,2) -F2(i,2)));

convE = []
convV = []
convL = []

error = 0
for i in range(0, nstage+1):
  g = GModel()
  g.load("output/"+FileName+"-%i.msh" % 0)
  groups = dgGroupCollection(g, 2, order)
  dof = dgDofContainer(groups, 3)
  dof.importMsh("output/dof-%i-end" % i)

  evaluator = dgFunctionEvaluator(groups, dof.getFunction())
  fsol = evaluator.newFunction(XYZRef)


  fdifference = functionPython(3, difference, [fsol, solRef]);
  L2error=dgDofContainer(groupsRef, 3)
  L2error.L2Projection(fdifference)

  fcompareE = functionPython(1, compareE, [fsol, solRef]);
  fcompareV = functionPython(1, compareV, [fsol, solRef]);
  integratorE = dgFunctionIntegrator(groupsRef, fcompareE)
  integratorV = dgFunctionIntegrator(groupsRef, fcompareV)
  rE = fullMatrixDouble(1,1)
  rV = fullMatrixDouble(1,1)
  integratorE.compute(rE)
  integratorV.compute(rV)
  convE.append((rE(0,0))**0.5)
  convV.append((rV(0,0))**0.5)
  convL.append(L2error.norm())

print '*** ELEVATION ***'
for i in range(0, nstage+1):
  if (convE[0]/convE[i] < (2**i) * 0.9):
    error = 1
  print("%.12e %.12e (%.12e,  %.12e) (%.1f)" % (alldt[i], su[i], cpu[i], convE[i], convE[0] / convE[i]))
 
print '*** VELOCITY ***'
for i in range(0, nstage+1):
  if (convV[0]/convV[i] < (2**i) * 0.9):
    error = 1
  print("%.12e %.12e (%.12e,  %.12e) (%.1f)" % (alldt[i], su[i], cpu[i], convV[i], convV[0] / convV[i]))

print '*** L2ERROR ***'
for i in range(0, nstage+1):
  if (convL[0]/convL[i] < (2**i) * 0.9):
    error = 1
  print("%.12e %.12e (%.12e,  %.12e) (%.1f)" % (alldt[i], su[i], cpu[i], convL[i], convL[0] / convL[i]))
