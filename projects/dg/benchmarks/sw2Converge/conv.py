from dgpy import *
import math
import os
try : os.mkdir("output"); 
except: 0;

nstage = 3

def initial(FCT, XYZ):
  for i in range(0,XYZ.size1()):
    x = (XYZ(i,0)/1e3) 
    FCT.set(i, 0, 0.05 * math.exp(-((x-0.5)*8)**2));
    FCT.set(i, 1, 0);
    FCT.set(i, 2, 0);

g2 = GModel()
g2.load("square.geo")
g2.mesh(2)
g2.save("output/square.msh")
conv = []

for i in range(0,nstage + 1):
  if (i != 0):
    g2.refineMesh(1)
  g2.save("output/square-%i.msh" % i)
  g = GModel()
  g.load("output/square-%i.msh" % i)
  groups = dgGroupCollection(g, 2, 1)
  groups.buildGroupsOfInterfaces()

  claw = dgConservationLawShallowWater2d()
  claw.addBoundaryCondition('Border', claw.newBoundaryWall())
  XYZ = groups.getFunctionCoordinates();
  f0 = functionPython(3, initial, [XYZ])
  f1 = functionConstant(0)
  claw.setQuadraticDissipation(f1)
  claw.setIsLinear(0)
  f2 = functionConstant([0])
  claw.setLinearDissipation(f2)
  f4 = functionConstant([10])
  claw.setBathymetry(f4)
  f5 = functionConstant([0,0,0])
  claw.setBathymetryGradient(f5)
  
  rk22 = dgERK(claw, None, DG_ERK_22)

  dof = dgDofContainer(groups, 3)
  dof.L2Projection(f0)
  dt = 1./(2**i)
  exporterInit=dgIdxExporter(dof, "output/dof-%i-init" % (i))
  exporterEnd=dgIdxExporter(dof, "output/dof-%i-end" % (i))
  exporterInit.exportIdx()
  for it in range (2**i * 7):
    rk22.iterate(dof, dt, 0)
  exporterEnd.exportIdx();

gRef = GModel()
gRef.load("output/square-%i.msh" % nstage)
groupsRef = dgGroupCollection(gRef, 2, 1)
dofRef = dgDofContainer(groupsRef, 3)
dofRef.readMsh("output/dof-%i-end/dof-%i-end.idx" % (nstage, nstage))
XYZRef = groupsRef.getFunctionCoordinates()
solRef = dofRef.getFunction()

def compareE(FCT, F1, F2):
  for i in range(0,F1.size1()):
    FCT.set(i, 0, (F1(i,0) -F2(i,0))** 2);


conv = []

error = 0
for i in range(0, nstage):
  g = GModel()
  g.load("output/square-%i.msh" % i)
  groups = dgGroupCollection(g, 2, 1)
  dof = dgDofContainer(groups, 3)
  dof.readMsh("output/dof-%i-end/dof-%i-end.idx" % (i, i))

  evaluator = dgFunctionEvaluator(groups, dof.getFunction())
  fsol = evaluator.newFunction(XYZRef)

  fcompare = functionPython(1, compareE, [fsol, solRef]);
  integrator = dgFunctionIntegrator(groupsRef, fcompare)
  r = fullMatrixDouble(1,1)
  integrator.compute(r)
  conv.append((r(0,0))**0.5)
  
for i in range(0, nstage):
  if (conv[0]/conv[i] < (2**i) * 0.9):
    error = 1
  print("%.1e (%.1f)" % (conv[i], conv[0] / conv[i]))
 
exit(error)
