from dgpy import *
import time
from math import *

# Mesh and model
clscale=1
order = 1
dimension = 2
FileName="IslandInBath"

MR=1
RK=2
nbSteps=10000

model = GModel()
tmodel = GModel()
tmodel.load (FileName+'.geo')
GmshSetOption('Mesh','CharacteristicLengthFactor',   clscale*1.0)
tmodel.mesh(dimension) 
if order > 1:
  tmodel.setOrderN(order, False, False)
tmodel.save(FileName+str(order)+'.msh')
model.load(FileName+str(order)+'.msh')

cx=50
cy=50

dx=40
dy=30

def massIntegrator(fct,  sol, XYZ):
  for i in range(0,XYZ.size1()):
    x = XYZ.get(i,0)
    y = XYZ.get(i,1)
    z = XYZ.get(i,2)
    s=sol.get(i, 0)
    depth=5-10*(-1+exp(-((x-cx)**2/dx**2+(y-cy)**2/dy**2)))
    fct.set(i, 0, s+depth)

def bathymetry(f, XYZ):
  for i in range(0,XYZ.size1()):
    x = XYZ.get(i,0)
    y = XYZ.get(i,1)
    z = XYZ.get(i,2)
    val=5-10*(-1+exp(-((x-cx)**2/dx**2+(y-cy)**2/dy**2)))
    f.set(i, 0, val)

def initial_condition(f, XYZ):
  for i in range(0,XYZ.size1()):
    x = XYZ.get(i,0)
    y = XYZ.get(i,1)
    z = XYZ.get(i,2)
    
    f.set(i,0,(0.01*y-0.5)/7) 
    f.set(i,1,0) 
    f.set(i,2,0) 

def current(f, SOL):
  for i in range(0,SOL.size1()):
    f.set(i,0,SOL.get(i, 1)) 
    f.set(i,1,SOL.get(i, 2)) 
    f.set(i,2,0) 

CCode = """
#include "fullMatrix.h"
#include "dgDataCacheMap.h"
extern "C" {
void bottomDrag(dgDataCacheMap *,  fullMatrix<double> &val,  fullMatrix<double> &sol,  fullMatrix<double> &bath){
  for(int i=0;  i<val.size1(); i++){
    double H = sol(i,  0)+bath(i,  0);
    val.set(i,  0,  9.81*0.0235*0.0235/(pow(H,  1.333333333333)));
  }
}
}
"""

tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CCode, tmpLib);

theta=-17.147569/180*pi
groups = dgGroupCollection(model, dimension, order)
XYZ = groups.getFunctionCoordinates()
claw = dgConservationLawShallowWater2d()
solution = dgDofContainer(groups, claw.getNbFields())
bath = dgDofContainer(groups,1)
f0 = functionConstant([ 2*7.292e-5*sin(theta)])
claw.setCoriolisFactor(f0)
f2 = functionConstant([0])
claw.setLinearDissipation(f2)
f4 = functionPython(1, bathymetry, [XYZ])
bath.L2Projection(f4)
claw.setBathymetry(f4)
f1 = functionC(tmpLib,  "bottomDrag",  1,  [solution.getFunction(),bath.getFunction()])
claw.setQuadraticDissipation(f1)

bndWall = claw.newBoundaryWall()
claw.addBoundaryCondition('lateral', bndWall)
claw.addBoundaryCondition('island', bndWall)
claw.addBoundaryCondition('input', bndWall)
claw.addBoundaryCondition('output', bndWall)


if MR==1:
  ML=10000
elif MR==0:
  ML=1

if RK == 1:
  rk=dgRungeKuttaMultirateConservative.new2a(groups,claw)
elif RK == 2:
  rk=dgRungeKuttaMultirateConservative.new2b(groups,claw)
elif RK == 3:
  rk=dgRungeKuttaMultirateConservative.new33(groups,claw)
elif RK == 4:
  rk=dgRungeKuttaMultirateConservative.new43(groups,claw)
elif RK == 5:
  rk=dgRungeKuttaMultirateConservative.new44(groups,claw)
elif RK == 6:
  rk=dgRungeKuttaMultirate43(groups,claw)

dt=rk.splitForMultirate(ML,   solution, [solution, bath])

groups.buildGroupsOfInterfaces()
solution.exportGroupIdMsh()

FS = functionPython(3, initial_condition, [XYZ])
CR = functionPython(3, current, [solution.getFunction()])
solution.L2Projection(FS)
bath.L2Projection(f4)

solution.exportMsh('output/island_in_bath_init', 0., 0)
bath.exportMsh('output/island_in_bath_bathymetry', 0., 0)

mass=functionPython(1,  massIntegrator,  [functionSolution.get(), XYZ])
integrator = dgFunctionIntegrator(groups,  mass,  solution)
totalMass=fullMatrixDouble(1, 1)
totalMassRef=fullMatrixDouble(1, 1)
integrator.compute(totalMassRef, "")

t=0
nbExport = 0
file=open(str(MR)+'_'+str(RK)+"_conservation_iib.txt", "w")
print ''
print '----------------------------------------------'
print 'NUMBER OF TIME STEPS : ', nbSteps
print 'DT                   : ', dt
print 'NORM OF SOLUTION     : ', solution.norm()
print 'TOTAL MASS           : ', totalMassRef.get(0, 0)
print 'MULTIRATE SPEED-UP   : ', groups.getMRspeedUp()
print '----------------------------------------------'
print ''

tic = time.time()
for i in range (1,nbSteps+1) :
  rk.iterate(t, dt, solution);
  norm = solution.norm()
  t = t +dt
  if ( i%10 ==0 ):
     integrator.compute(totalMass, "")
     print '|ITER|',i,'|NORM|',norm,'|MASS|',(totalMass.get(0, 0)-totalMassRef.get(0, 0))/totalMassRef.get(0, 0) , '|DT|',dt, '|T|',t, '|CPU|',time.time()-tic 
     file.write("%d \t %f \t %f \t %f \t %f \n"%(i, t, (totalMass.get(0,  0)-totalMassRef.get(0,  0))/totalMassRef.get(0,  0), norm, time.time()-tic))
     solution.exportMsh("output/island_in_bath-%06d" % (i), t, nbExport)
     solution.exportFunctionMsh(CR,'output/island_in_bath-%06d'%(i),  t,  nbExport,  '')
     nbExport = nbExport + 1
print (time.time()-tic)
file.close()
Msg.Exit(0)

