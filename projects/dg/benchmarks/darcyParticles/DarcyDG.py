from dgpy import *
import numpy

RADIUS = 0.05

# store particles information at mesh elements

#----------------------------------------------------------------
#-- CLASS DEFINITION
#-- Darcy's Law :
#  - \nabla \cdot {K \over \mu} \grad P = - \nabla \cdot v_p
#    Alternative form
#  - \nabla \cdot {{K \phi} \over {\mu + \beta \kappa}} \grad P = - \nabla \cdot v_p
#
#   K is the permeability of the bulk that depends on its compacity [m^2]
#   \mu is the dynamic viscosity of the fluid [Pa sec ]
#   v_p is the "mesh" velocity of the particles [m/sec]
#   P is the pressure [Pa]
#   \phi is the local porosity of the medium = 1 - C []
#   C is the compacity = V_particles / V_all [] 
#   \beta =  { 150 \mu C^2} \over {d_p^2 \phi} is the viscous friction coefficient [N sec]
#   Permeability is K = {d_p^2 \phi^3} \over {150 C^2}
#----------------------------------------------------------------
def phi(compacity) :
  return numpy.minimum(1. -compacity, 0.75)

def perm(dp, compacity) :
  phiV = phi(compacity)
  return dp**2/150. * (phiV**3) / (1 - phiV)**2

def volumeForce(cmap, val, compacity, gradp) :
  f = 1. / (1 - phi(compacity))
  val[:, :] = - f.reshape(-1, 1) * gradp

def minusDivergence(cmap, val, gradv) :
  val[:] = -(gradv[:, 0] + gradv[:, 4] + gradv[:, 8])

def permeability(cmap, val, dp, compacity) :
  val[:] = perm(dp, compacity)

def diffusivity(cmap, val, mu, dp, compacity) :
  val[:] = perm(dp, compacity) / mu


class Darcy(object):
  def __init__(self, nameMesh, dimension, volumeParticle):
    self.dim = dimension
    self.order = 1
    self.computeParameters(volumeParticle)
    self.model = GModel()
    self.model.load(nameMesh+'.geo')
    self.model.load(nameMesh+'.msh')
    self.groups = dgGroupCollection(self.model, self.dim, self.order)
    self.particleMesh = particleMesh(self.model, ["Domain"]);
    mu = 1.e-3 # water
    rho = 1000 # water
    self.rho = functionConstant(rho) # water
    self.mu = functionConstant(mu) # water
    self.dp = functionConstant(self.d_p)
    self.compacityDof = dgDofContainer(self.groups, 1)
    self.velocityDof = dgDofContainer(self.groups, 3)
    self.pressureDof = dgDofContainer(self.groups,1)
    # specific for darcy
    self.K   = functionNumpy(1, diffusivity, [self.mu, self.dp, self.compacityDof.getFunction()])
    self.law = dgConservationLawAdvectionDiffusion.diffusionLaw(self.K)
    self.RHS = functionNumpy(1, minusDivergence, [self.velocityDof.getFunctionGradient()])
    self.law.setSource(self.RHS)
    self.law.addBoundaryCondition(['Wall', 'ZeroPressure'],self.law.new0FluxBoundary())
    self.const0 = functionConstant(0.0)
    self.law.addStrongBoundaryCondition(0,'CornerBottomLeft',self.const0)
    self.law.addStrongBoundaryCondition(1,'ZeroPressure',self.const0)
    #specific for navier stokes brinkman
    self.sys = linearSystemPETScDouble ()
    self.sys.setParameter("petscOptions", "-ksp_rtol 1.e-13 -pc_type lu -pc_factor_levels 0")
    #self.sys.setParameter("petscOptions", "-ksp_rtol 1.e-5 -pc_type ilu -pc_factor_levels 0 -ksp_monitor")
    self.dof = dgDofManager.newCG (self.groups,  1,  self.sys)
    self.steady = dgSteady(self.law, self.dof);
    self.steady.getNewton().setRtol(10e+7)

  def computeParameters(self, volume) :
    """compute average particle diameter and other properties"""
    if (self.dim == 2) :
      self.d_p = numpy.mean((volume / numpy.pi)**(0.5))
    else :
      self.d_p = numpy.mean((0.75 * volume / numpy.pi)**(1./3))
    print('Average of the areas is %g' % self.d_p)

  def particlesToMesh (self, position, velocity, volume) :
    self.particleOnMesh = particleList(self.particleMesh, position)
    self.particleVolume = numpy.reshape(volume, [-1, 1])

    #smoothing
    def genSamples(NT, NR) :
      thetas = numpy.linspace(2*numpy.pi/NT, 2*numpy.pi, NT)
      rs = numpy.linspace(RADIUS/NR, RADIUS, NR)
      samples = numpy.zeros([NT * NR + 1, 3])
      samples[1:, 0] = numpy.outer(rs, numpy.cos(thetas)).flat
      samples[1:, 1] = numpy.outer(rs, numpy.sin(thetas)).flat
      return samples
    self.samples = genSamples(4, 1)
    N = position.shape[0]
    NS = self.samples.shape[0]
    coordSmooth = position.repeat(NS, axis = 0) + numpy.tile(self.samples, [N, 1])
    volumeSmooth = (self.particleVolume / NS).repeat(NS, axis = 0)
    velocitySmooth = velocity.repeat(NS, axis = 0)
    particleOnMeshSmooth = particleList(self.particleMesh, coordSmooth)

    vertexMomentum = particleOnMeshSmooth.particle2mesh(velocitySmooth * volumeSmooth)
    vertexVolume = numpy.maximum(1e-12, particleOnMeshSmooth.particle2mesh(volumeSmooth))
    self.vertexVelocity = vertexMomentum / vertexVolume
    self.vertexCompacity = vertexVolume / self.particleMesh.vertexVolume()

    particleMesh2DGDof(self.particleMesh, self.vertexVelocity, self.velocityDof)
    particleMesh2DGDof(self.particleMesh, self.vertexCompacity, self.compacityDof)
    compacity = self.particleOnMesh.mesh2particle(self.vertexCompacity).reshape([-1])
    return compacity

  def computeForces (self) :
    fVolumeForce = functionNumpy(3, volumeForce, [self.compacityDof.getFunction(), self.pressureDof.getFunctionGradient()])
    return self.particleOnMesh.function2particle(self.groups, fVolumeForce) * self.particleVolume

  def solve (self, iteration, T) :
    """solve the continuous equation for the pressure and return the pressure gradient at the particle positions"""
    self.steady.solve(self.pressureDof)
    gradp = self.particleOnMesh.function2particle(self.groups, self.pressureDof.getFunctionGradient())
    return gradp

  def saveParticlesOnMeshElem (self, iteration, T, what) :
    if what == 1 :
      permeabilityF = functionNumpy(1, permeability, [self.dp, self.compacityDof.getFunction()])
      self.groups.exportFunctionMsh(permeabilityF, "output/permeability-%d" % iteration, T, iteration);
    elif what == 2 :
      self.groups.exportFunctionMsh(self.compacityDof.getFunction(), "output/compacity-%d" % iteration, T, iteration);
    elif what == 4 :
      self.groups.exportFunctionMsh(self.velocityDof.getFunction(), "output/velocity-%d" % iteration, T, iteration);

  def genVertexMapping(self, verticesIdx = None) :
    if not verticesIdx :
      verticesIdx = range(1, self.model.getNumMeshVertices() + 1)
    return [self.particleMesh.vertexIndex(self.model.getMeshVertexByTag(i)) for i in verticesIdx]

  def getVertexVelocity(self, mapping) :
    return self.vertexVelocity[mapping]

