from dgpy import *
from LMGC90Particles import *
from DarcyDG import *
from math import *

def saveParticles (particles, iteration, T) :
  f = open("output/particles%d.pos"%(iteration), 'w')
  f.write("View \"\"{\n")
  for i in range(0,particles.shape[0]):
    X = particles[i,0]
    Y = particles[i,1]
    Z = particles[i,2]
    gradpX = particles[i,7]
    gradpY = particles[i,8]
    gradpZ = particles[i,9]
    fX = particles[i,10]
    fY = particles[i,11]
    fZ = particles[i,12]
    compacity = particles[i,13]
    vol = particles[i,6]
    density = 1.0
    mass  = vol * density
    f.write("VP(%g,%g,%g){%g,%g,%g};\n"%(X,Y,Z,fX,fY,fZ))
    #    f.write("VP(%g,%g,%g){%g,%g,%g};\n"%(X,Y,Z,gradpX,gradpY,gradpZ))
    #    f.write("SP(%g,%g,%g){%g};\n"%(X,Y,Z,compacity))
    
  f.write("};\n")
  f.close()


######### MAIN PROGRAM ##################################################

os.system("mkdir output")
os.system("mkdir DATBOX")


DT = 5e-3 
T = 0.
name = 'darcy'
ITMAX = 100
density_particles = 2700.
density_fluid = 1000.

CreateParticlesSample (density_particles - density_fluid)

particles = InitializeParticlesSolver(DT)

darcySolver = Darcy(name, 2, particles[:, 6])
vertexMapping = darcySolver.genVertexMapping()

for iteration in range (1,ITMAX+1) :

  print ' '
  print '-----'
  print '## STEP ', iteration, 'Time = ', T, '##'

  ## transfer particles info to the mesh
  particles[:,13] = darcySolver.particlesToMesh(particles[:,0:3], particles[:, 3:6], particles[:, 6])

  ## compute Darcy
  particles[:,7:10] = darcySolver.solve(iteration, T)

  ## compute force on particles due to fluid
  particles[:,10:13] = darcySolver.computeForces()

  ## save particle positions and velocities
  if (iteration % 10 == 0) :
    saveParticles(particles, iteration, T) 
    darcySolver.saveParticlesOnMeshElem(iteration, T, 1) 
    darcySolver.saveParticlesOnMeshElem(iteration, T, 2) 
    darcySolver.saveParticlesOnMeshElem(iteration, T, 4) 

  ## update Particles Positions and Velocities using LMGC90
  V=darcySolver.getVertexVelocity(vertexMapping)
  ComputeOneStepParticlesSolver(particles, DT, V[:, 0], V[:, 1]) 

  ## increase time step
  T += DT

FinalizeParticlesSolver()

