from dgpy import *
from Incompressible import *

RHO = 1

def initF(FCT, XYZ):
  for i in range(0,FCT.size1()):
    FCT.set(i,0, 0) #u
    FCT.set(i,1, 0) #v
    FCT.set(i,2, 0) #p

def POne(FCT):
  for i in range(0,FCT.size1()):
    FCT.set(i, 2, 1.)
    FCT.set(i,3, free)
    FCT.set(i,4, free)
    FCT.set(i,5, fixed)

nameMesh = "darcy"
xyz = function.getCoordinates();

invK  = functionConstant(1000)
GradV = functionConstant([0, 0, 0, 0, 0, 0])
ns = Incompressible(nameMesh, 2)
rhoF = functionConstant(RHO)
mu = functionConstant(100)
ns.initializeIncompDarcy(initF, rhoF, mu, 1, invK, GradV)

#ns.strongBoundaryConditionLine('Wall', ns.VELX)
ns.weakSlipBoundaryCondition("Wall")
ns.strongBoundaryConditionLine('Left', ns.PZERO)
POneF = functionPython(6, POne, [])
ns.strongBoundaryConditionLine('Right', POneF)
petscOptions ="-pc_type lu"
ns.steadySolve(4 , petscOptions)
ns.solution.exportFunctionMsh(ns.law.getPressure(), 'output/darcy', 0, 0, "pre-test-%d"%(0))
