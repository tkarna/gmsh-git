lc = 1000;

w = 1500;
L = 13800;
l1 = 2400;
l2 = 3600;
l3 = 4800;
l4 = 6000;

Point (1)  = {0 , -w, 0, lc};
Point (2)  = {l1, -w, 0, lc};
Point (3)  = {l2, -w, 0, lc};
Point (4)  = {l3, -w, 0, lc};
Point (5)  = {l4, -w, 0, lc};
Point (6)  = {L , -w, 0, lc};
Point (7)  = {L ,  w, 0, lc};
Point (8)  = {l4,  w, 0, lc};
Point (9)  = {l3,  w, 0, lc};
Point (10) = {l2,  w, 0, lc};
Point (11) = {l1,  w, 0, lc};
Point (12) = {0 ,  w, 0, lc};

Line (1)  = { 1, 2};
Line (2)  = { 2, 3};
Line (3)  = { 3, 4};
Line (4)  = { 4, 5};
Line (5)  = { 5, 6};
Line (6)  = { 6, 7};
Line (7)  = { 7, 8};
Line (8)  = { 8, 9};
Line (9)  = { 9,10};
Line (10) = {10,11};
Line (11) = {11,12};
Line (12) = {12, 1};
Line (21) = { 2, 11};
Line (31) = { 3, 10};
Line (41) = { 4, 9};
Line (51) = { 5, 8};

Physical Line ('Tide') = {6};
Physical Line ('Wall') = {1,2,3,4,5,7,8,9,10,11,12};

Line Loop (101) = {1, 21, 11, 12};
Line Loop (102) = {2, 31, 10, -21};
Line Loop (103) = {3, 41, 9, -31};
Line Loop (104) = {4, 51, 8, -41};
Line Loop (105) = {5, 6, 7, -51};

Plane Surface (111) = {101};
Plane Surface (112) = {102};
Plane Surface (113) = {103};
Plane Surface (114) = {104};
Plane Surface (115) = {105};

Physical Surface (3) = {111,112,113,114,115};
