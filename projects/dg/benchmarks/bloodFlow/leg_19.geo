pi=3.14159;
lc=1;
//nb=11*10;

L1 = 3;
L2 = 1.0;
L3 = 30.0;
L4 = 1.0;
 
C10 = Cos(pi/18);
S10 = Sin(pi/18);

C20 = Cos(pi/9);
S20 = Sin(pi/9);

x1= 0;
y1= 0;

x2= 0;
y2= -L1;

x3= x2+L2*S20;
y3= y2-L2*C20;

x4= 0;
y4= y2-L3;

x5= 0;
y5= y4-L4;


Point(1) = {x1, y1, 0,lc};
Point(2) = {x2, y2, 0,lc};
Point(3) = {x3, y3, 0,lc};
Point(4) = {x4, y4, 0,lc};
Point(5) = {x5, y5, 0,lc};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {2,4};
Line(4) = {4,5};

//Transfinite Line {1,2,3,4} = nb;

Physical Line("ComFem")={1};
Physical Line("DeepFem")={2};
Physical Line("Bypass")={3};
Physical Line("Popliteal")={4};

Physical Point("Inlet")={1};
Physical Point("Outlet_DF")={3};
Physical Point("Outlet_PO")={5};

