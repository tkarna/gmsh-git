from Bloodflow import *
import os
 
os.system("rm output/*")


# Simulation arteries of the leg 
# Data computed from measurements in patient 19

#----------------------------------------------------------------  
#-- Blood Parameters
#----------------------------------------------------------------  
meshName = "leg_19"
order = 4
Flux = 'UPW' 

rho  = 1.06 
ViscDyn = 0.04
ViscLaw = 'PoiseuilleProfile' 

#-- Diastolic pressure(=61 mmHg)  
p0 = 81300

# Diameter and pulse wave velocity for each artery: (D[cm],c[cm/s])
data_Dc = {"ComFem": [0.615, 580.0] ,"DeepFem": [0.43, 890.] , "Bypass": [0.43, 1500.] , "Popliteal": [0.375, 720.0]}

data_Leakage = {"ComFem": 0.0,  "DeepFem": 0.0 , "Bypass": 0.0 , "Popliteal": 0.0}

#----------------------------------------------------------------  
#-- Initialization
#----------------------------------------------------------------  

(model, groups, law, solution) = initializeIncomp( meshName, order, Flux, rho, ViscDyn, ViscLaw, p0,  data_Dc, data_Leakage )

#----------------------------------------------------------------  
#-- Boundary Conditions
#----------------------------------------------------------------
InletFileName = "data/19_FCom_MAXdiv2_DELAY01_CorrMean_cycle_FWD_600.dat"
os.putenv("INLETFILE", InletFileName)

inletAbsorbingVelocityBoundaryCondition(law,'Inlet',VELFILE)

#-- 0D model, outlet param DeepFem & Pop
R_DF = 7.49e4
C_DF = 1.58e-5
Pout_DF = 26660.0

R_Pop = 3.93e4
C_Pop = 4.32e-5
Pout_Pop = 26660.0

#-- OUTLET
MyWK_DF = functionConstant([R_DF,C_DF, Pout_DF])
outletWindkesselBoundaryCondition(law,'Outlet_DF',MyWK_DF)

MyWK_Pop = functionConstant([R_Pop,C_Pop, Pout_Pop])
outletWindkesselBoundaryCondition(law,'Outlet_PO',MyWK_Pop)


#----------------------------------------------------------------  
#-- Solvers for NS
#----------------------------------------------------------------  
Tc = 0.875
tFinal =  Tc*3.0
dt_export = 0.001; 

explicit = True
outDir = 'output'
outList = ['ComFem', 'DeepFem', 'Bypass', 'Popliteal']

unsteadySolve(explicit, model, groups, law, solution, tFinal, dt_export, outDir, outList)




