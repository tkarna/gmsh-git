L=15;
lc= 0.9;

Point(1) = {0,0,0,lc};
Point(2) = {L/3,0, 0,lc};
Point(3) = {2*L/3,0, 0,lc};
Point(4) = {L,0, 0,lc};

Line(14) = {1,2};
Line(15) = {2,3};
Line(16) = {3,4};

Physical Line("aorta")={14};
Physical Line("stent")={15};
Physical Line("aorta2")={16};
Physical Point("Inlet")={1};
Physical Point("Outlet")={4};

