from Bloodflow import *
import os
 
os.system("rm output/*")

#----------------------------------------------------------------  
#-- Blood Parameters
#----------------------------------------------------------------  
meshName = "aorta"
order = 2
Flux = 'UPW' # 'LAX' # 'HLL' ### ROE KO!!! 

rho  = 1.06 
ViscDyn = 0.04
ViscLaw = 'PoiseuilleProfile' # ='FlatProfile'
p0 = 61328 #46 mmHg

data_Dc = {"aorta":[2.0 , 614.0]}
data_Leakage = {"aorta": 0.}

#----------------------------------------------------------------  
#-- Initialization
#----------------------------------------------------------------  

(model, groups, law, solution) = initializeIncomp( meshName, order, Flux, rho, ViscDyn, ViscLaw, p0,  data_Dc, data_Leakage )

#----------------------------------------------------------------  
#-- Boundary Conditions
#----------------------------------------------------------------
InletFileName = "BloodVel_Profile.dat"
os.putenv("INLETFILE", InletFileName)

#inletAbsorbingPressureBoundaryCondition(law,'Inlet',inlet_pres)
inletReflectingVelocityBoundaryCondition(law,'Inlet',inlet_vel2)
#inletAbsorbingVelocityBoundaryCondition(law,'Inlet',VELFILE)

#-- 0D model, outlet param
Rt = 0.0
R = 1890.0
C = 6.31e-4
Pout = 13333.0
MyWK = functionConstant([R,C, Pout])
outletWindkesselBoundaryCondition(law,'Outlet', MyWK)
#MyRt = functionConstant([Rt])
#outletTerminalResistanceBoundaryCondition(law,'Outlet',MyRt) 

#----------------------------------------------------------------  
#-- Solvers for NS
#----------------------------------------------------------------  
Tc = 0.8
tFinal =  Tc*9.0
dt_export = Tc/100 

explicit = True
outDir = 'output'
outList = ['aorta']

unsteadySolve(explicit, model, groups, law, solution, tFinal, dt_export, outDir, outList)


