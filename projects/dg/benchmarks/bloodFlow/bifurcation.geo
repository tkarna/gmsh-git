L=20;
lc=0.5;

pi = 3.14;
C45 = Cos(pi/5);
S45 = Sin(pi/5);

Point(1) = {-L,0,0,lc};
Point(2) = {0,0, 0,lc};
Point(3) = {L*C45, L*S45, 0,lc};
Point(4) = {L*C45, -L*S45, 0,lc};

Line(11) = {1,2};
Line(12) = {2,3};
Line(13) = {2,4};

Physical Line("aorta")={11};
Physical Line("iliac1")={12};
Physical Line("iliac2")={13};
Physical Point("Inlet")={1};
Physical Point("Outlet1")={3};
Physical Point("Outlet2")={4};
