L=20;
lc=1;
//nb=11*10;

Point(1) = {0,0,0,lc};
Point(2) = {L,0, 0,lc};
Point(3) = {2*L,0,0,lc};

Line(11) = {1,2};
Line(12) = {2,3};
//Transfinite Line {11,12} = nb;

Physical Line("aorta")={11};
Physical Line("disc")={12};
Physical Point("Inlet")={1};
Physical Point("Outlet")={3};

