import subprocess
from gmshpy import *
from dgpy import *
import math
import time


#-------------------------------------------------
#-- Global Parameters
#-------------------------------------------------
geo = ".geo"
msh = ".msh"

#-- Parameter for reading of inlet file
global numCycle
numCycle = 0

global init
init = False

#-------------------------------------------------
#-- General functions
#-------------------------------------------------
#-- Reading of file (InletFile)  
def ReadFile(InletFileName):
    print 'Info : Inlet file is ',InletFileName
    VelFile= open(InletFileName,"r")
    global MyTime
    global MyVel
    global T
    MyTime = []
    MyVel = []
    lines = VelFile.read().splitlines()
    VelFile.close()
    for line in lines[:-1] :
        lineSep = line.split(" ")
        MyTime.append(float(lineSep[0]))
        MyVel.append(float(lineSep[1]))
    T = MyTime[-1]



#-- Compute the Viscosity coefficient KR 
def Compute_KR(ViscDyn,ViscLaw, rho): 
    if (ViscLaw == 'PoiseuilleProfile') :
        KR = 8*math.pi*ViscDyn/rho
    elif (ViscLaw == 'FlatProfile') :
        KR = 22*math.pi*ViscDyn/rho    
    return KR


#--Inlet functions
def inletPres(FCT):
    T = 0.8 #TODO: param to put in arguments
    Pmax = 20000
    heaviside = 1.0
    if (t > T/2) :
        heaviside = 0.0	

    p = Pmax*math.sin(2*math.pi*t/T)*heaviside	
    FCT.set(0,0,p)


def inletVel( FCT):
    global t
    u = 0.5*math.exp(-5000.0*(t-0.05)*(t-0.05))
    FCT.set(0,0,u)


def inletVel2( FCT ):
    T = 0.8 #TODO: param to put in arguments
    global t
    heaviside = 1.0;
    if ( (t-math.floor(t/T)*T) > 0.25 ):
        heaviside = 0.0	
    u = 100*math.sin(3.1415*(t-math.floor(t/T)*T)/.25)*heaviside
    FCT.set(0,0,u)



def inletVelFile( FCT ):
    InletFileName = "BloodVel_Profile.dat" #TODO:to put in arguments!
#    InletFileName = "data/19_FCom_MAXdiv2_DELAY01_CorrMean_cycle_FWD_600.dat"
    global init
    global numCycle
    global MyTime
    global MyVel
    global T
    global t
    global ind
    if not init :
        ReadFile(InletFileName) 
        print 'Info : Cardiac cycle duration:', T
        init = True
    tstar = (t - math.floor(t/T)*T)
    if (t >= T*numCycle) :
        numCycle = numCycle+1
        ind = 0
    if (tstar >= MyTime[ind+1]) :
        ind =ind+1
    dat1 = MyVel[ind]
    dat2 = MyVel[ind+1]
    t1 = MyTime[ind]
    t2 = MyTime[ind+1]
    u = dat1+(dat2-dat1)/(t2-t1)*(tstar-t1)
    FCT.set(0,0,u)



inlet_pres   = functionPython(1, inletPres   , [])
inlet_vel    = functionPython(1, inletVel    , [])
inlet_vel2   = functionPython(1, inletVel2   , [])
inlet_velFile= functionPython(1, inletVelFile, [])


tmpLib = "tmp.dylib"
#if (Msg.getCommRank() == 0 ) :
functionC.buildLibraryFromFile("BloodFlowC.c", tmpLib)
#Msg.barrier()


def exportTxtData(model, groups, solution, outDir, tcurrent, evaluator, iiter, fileName, outList):
    
    for k in range(len(outList)):
        name = '%s/%s-%s.txt' % (outDir, fileName, outList[k])
        fun = open(name,"a")
        fun2 = open('%s/plot%s-%s.gnu' % (outDir, fileName, outList[k]),"a")
        if (iiter == 0):
            fun.write("#(1) Time (2) Proximal Value (3) Medial Value (4) Distal Value\n")
            fun2.write("set xlabel 'time (s)'\n")
            fun2.write("set ylabel '%s' \n" % fileName)
            fun2.write("plot '%s' u 1:($2) w l t 'P', '%s' u 1:($3) w l t 'M', '%s' u 1:($4) w l t 'D'" %(name, name, name))
        fun.write("%g "% tcurrent)

 
            
        myEval = dgFunctionEvaluator(groups, evaluator, solution)
        res = fullMatrixDouble()
           
        nums = model.getEdgesByStringTag(outList[k]) 
        edge = model.getEdgeByTag(nums[0])
        umin = edge.getLowerBound()
        umax = edge.getUpperBound()
        pOld = edge.point(umin)
        s = 0.0
        N = 2
        for i in range (0,N+1) :
            u = umin + (i*1.)/(N*1.) * (umax - umin)
            p = edge.point(u)
            #s = s + math.sqrt((p.x()-pOld.x())*(p.x()-pOld.x())+ (p.y()-pOld.y())*(p.y()-pOld.y()) + (p.z()-pOld.z())*(p.z()-pOld.z()))/(umax-umin)
            myEval.compute(p.x(),p.y(), p.z(), res)
            pOld = p
            fun.write("%g " % (res.get(0,0)))
            #print 's' , s, 'VAL  = ', res.get(0,0)
                
        fun.write("\n")
        fun.close()

#-------------------------------------------------
#-- Initialization of problem
#-------------------------------------------------
def initializeIncomp(order, meshName, rho, ViscDyn, ViscLaw, p0, Flux, data_Dc, data_Leakage):
    KR = Compute_KR(ViscDyn,ViscLaw, rho)
    dimension = 1
    model = GModel()
    #subprocess.Popen("gmsh %s.geo -%d -order %d"%(meshName, dimension, order), shell=True )
    model.load (meshName+geo)
    model.load (meshName+msh)
    groups = dgGroupCollection(model, dimension, order)
    groups.splitGroupsByPhysicalTag()
    groups.buildGroupsOfInterfaces()
    law = dgConservationLawNavierStokes1d()
    law.setRiemannSolver(Flux)
    MyKR = functionConstant([KR])
    MyKR.thisown = 0
    law.setLinearDissipation(MyKR)
    #--Source term for mass conservation
    MyS = functionConstantByTag(data_Leakage)
    MyS.thisown = 0
    law.setSourceMass(MyS)
    law.setBloodFormulation()
    law.setDensity(rho)
    law.setEquilibriumPressure(p0)
    #--Area & Elasticity of arteries
    #AData = functionConstantByTag(data_Area)
    #AData.thisown = 0
    #law.setBathymetry(AData)
    #BData = functionConstantByTag(data_Beta)
    #BData.thisown = 0
    #law.setParamPressureLaw(BData)
    #--Diameter & pulse wave velocity of arteries
    DcData = functionConstantByTag(data_Dc)
    DcData.thisown = 0
    law.setDiameterAndCelerity(DcData)
    INIT = functionConstant( [0., 0. ])
    INIT.thisown = 0
    solution = dgDofContainer(groups, law.getNbFields())
    solution.L2Projection(INIT)
    solution.exportGroupIdMsh()
    return (model, groups, law, solution)
    
    
#-------------------------------------------------
#-- Functions for boundary conditions
#-------------------------------------------------

def inletAbsorbingVelocityBoundaryCondition(law, nameBC, fctLUA):
    Tmp = law.newInletAbsorbingVelocity(fctLUA)
    law.addBoundaryCondition(nameBC, Tmp)

def inletReflectingVelocityBoundaryCondition(law,nameBC, fctLUA):
    law.addBoundaryCondition(nameBC, law.newInletReflectingVelocity(fctLUA))

def inletAbsorbingPressureBoundaryCondition(law,nameBC, fctLUA):
    law.addBoundaryCondition(nameBC, law.newInletAbsorbingPressure(fctLUA))

def wallBoundaryCondition(law,nameBC):
    law.addBoundaryCondition(nameBC, newBoundaryWall())

def outletTerminalResistanceBoundaryCondition(law,nameBC, fctLUA):
    law.addBoundaryCondition(nameBC, law.newOutletTerminalResistance(fctLUA))
   
def outletWindkesselBoundaryCondition(law,nameBC, fctLUA):
    law.addBoundaryCondition(nameBC, law.newOutletWindkessel(fctLUA))


#-------------------------------------------------
#-- Solvers for NS
#-- Example of a Bloodflow Code with Runge Kutta
#-------------------------------------------------


def unsteadySolve(model, groups, law, solution, nbTimeSteps, nbExport, outDir, outList):

    rk=dgRungeKutta()
    #--limiter = dgSlopeLimiter(law)
    #--rk:setLimiter(limiter) 

    print('*** print initial sol in /{0}/  ***'.format(outDir))
    solution.exportMsh('{0}/init'.format(outDir), 0, 0)

    print'*** solve ***'
    CFL = 0.5
    x =  time.clock()
    global t
    t=0
    for i in range(0,nbTimeSteps):
        dt = CFL * rk.computeInvSpectralRadius(law,solution) #*10
        norm = rk.iterate44(law,t, dt,solution)
        if (i % 200  == 0) : 
            print('|ITER|',i,'|NORM|',norm,'|DT|',dt, '|t|', t, '|CPU|', time.clock()-x)
            solution.exportMsh('%s/sol-%06d' % (outDir, i), t, nbExport)
            solution.exportFunctionMsh(law.getPressure(), '%s/pres-%06d' % (outDir, i), t, nbExport, '')       
            solution.exportFunctionMsh(law.getArea(),     '%s/area-%06d' % (outDir, i), t, nbExport, '')
            solution.exportFunctionMsh(law.getFlowRate(), '%s/flow-%06d' % (outDir, i), t, nbExport, '')

            exportTxtData(model, groups, solution, outDir,  t, law.getPressure(), i, 'pressure', outList)
            exportTxtData(model, groups, solution, outDir, t, law.getArea()    , i, 'area', outList)
            exportTxtData(model, groups, solution, outDir, t, law.getFlowRate(), i, 'flowrate', outList)
                    
            nbExport = nbExport + 1

        t = t+dt
        

