from Bloodflow import *
import os
 
os.system("rm output/*")

#----------------------------------------------------------------  
#-- Blood Parameters
#----------------------------------------------------------------  
meshName = "stent"
order = 4
Flux = 'LAX'

rho  = 1.0
ViscDyn = 0.04
ViscLaw = 'PoiseuilleProfile'
p0 = 0.0 

# Diameter and pulse wave velocity for each artery:
data_Dc = {"aorta":[1.12 , 475.0], "stent":[1.12 , 4750.], "aorta2":[1.12, 475.]}
data_Leakage = {"aorta": 0. , "stent": 0. , "aorta2":0.}

#----------------------------------------------------------------  
#-- Initialization
#----------------------------------------------------------------  

(model, groups, law, solution) = initializeIncomp( meshName, order, Flux, rho, ViscDyn, ViscLaw, p0,  data_Dc, data_Leakage )

#----------------------------------------------------------------  
#-- Boundary Conditions
#----------------------------------------------------------------
#-- INLET
inletAbsorbingPressureBoundaryCondition(law,'Inlet',inlet_pres)
#inletAbsorbingVelocityBoundaryCondition(law,'Inlet',inlet_vel2)

#-- OUTLET
Rt = 0.0
MyRt = functionConstant([Rt])
outletTerminalResistanceBoundaryCondition(law,'Outlet',MyRt) 

#----------------------------------------------------------------  
#-- Solvers for NS
#----------------------------------------------------------------  
Tc = 0.25
tFinal = 0.25
dt_export = Tc/100

explicit = True
outDir = 'output'
outList = ['aorta', 'stent', 'aorta2']

unsteadySolve(explicit, model, groups, law, solution, tFinal, dt_export, outDir, outList)

