from Bloodflow import *
import os
 
os.system("rm output/*")

#----------------------------------------------------------------  
#-- Blood Parameters
#----------------------------------------------------------------  
meshName = "discontinuity"
order = 4
Flux =  'UPW'    #'HLL' #'LAX'   ### ROE KO!!!

rho  = 1.06 
ViscDyn = 0.0
ViscLaw = 'PoiseuilleProfile' # ='FlatProfile'
p0 = 0  #-- Diastolic pressure(=46 mmHg)

# Diameter and pulse wave velocity for each artery:
#data_Dc = {"aorta":[0.7 , 2000.] , "disc":[.5 , 1200.]} # D et c varient : semble ok à dt_export = 0.001!?! 

# Pour les cas test suivants (uniquement PWV change), si dt_export = 0.001, on capte des oscillations 
# MAIS dans Argo, aucune oscillation n'est observée (meme si dt_export = 0.001) 
#data_Dc = {"aorta":[1.0 , 175.0] , "disc":[1.0 , 175.]} # idem
#data_Dc = {"aorta":[1.0 , 1750.0], "disc":[1.0 , 175.]} # <
data_Dc = {"aorta":[1.0 , 175.0] , "disc":[1.0 , 300.]} # >
#data_Dc = {"aorta":[1.0 , 120.]  , "disc":[1.0 , 1200.]}# >

data_Leakage = {"aorta": 0. , "disc": 0.}

#----------------------------------------------------------------  
#-- Initialization
#----------------------------------------------------------------  

(model, groups, law, solution) = initializeIncomp( meshName, order, Flux, rho, ViscDyn, ViscLaw, p0,  data_Dc, data_Leakage )

#----------------------------------------------------------------  
#-- Boundary Conditions
#----------------------------------------------------------------
#-- INLET
InletFileName = "BloodVel_Profile.dat"
os.putenv("INLETFILE", InletFileName)

inletAbsorbingPressureBoundaryCondition(law,'Inlet',inlet_pres)
#inletAbsorbingVelocityBoundaryCondition(law,'Inlet',inlet_vel2)
#inletAbsorbingVelocityBoundaryCondition(law,'Inlet',VELFILE)


#-- OUTLET
Rt = 0.0
R = 1890.0
C = 6.31e-4
Pout = 13333.0

MyRt = functionConstant([Rt])
outletTerminalResistanceBoundaryCondition(law,'Outlet',MyRt) 
#MyWK = functionConstant([R,C, Pout])
#outletWindkesselBoundaryCondition(law,'Outlet',MyWK)


#----------------------------------------------------------------  
#-- Solvers for NS
#----------------------------------------------------------------  
Tc = 1.0 
tFinal = 0.8
dt_export = 0.01 #Attention, si dt trop petit (0.001), on capte des oscillations! ! ! 

explicit = True
outDir = 'output'
outList = ['aorta', 'disc']

unsteadySolve(explicit, model, groups, law, solution, tFinal, dt_export, outDir, outList)

