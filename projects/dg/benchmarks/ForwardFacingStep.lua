MACH = 3.0;
GAMMA = 1.4;
U = 3.0
V = 0.0 
RHO  = 1.4;

PRES = RHO*U*U/(GAMMA*MACH*MACH)
--PRES = 1;
--PRES = ./(MACH*RHO*RHO*GAMMA*GAMMA) 

SOUND = math.sqrt(U*U+V*V)/MACH

--[[ 
     Function for initial conditions
--]]
function free_stream(xyz, f )
 for i=0,xyz:size1()-1 do
    f:set(i,0,RHO) 
    f:set(i,1,RHO*U) 
    f:set(i,2,RHO*V) 
    f:set(i,3, 0.5*RHO*(U*U+V*V)+PRES/(GAMMA-1)) 
  end
end

xyz = functionCoordinates.get()
FS = functionLua(4, 'free_stream', {xyz})

--[[ 
     Example of a lua program driving the DG code
--]]

order = 1
dimension = 2

print'*** Loading the mesh and the model ***'
model   = GModel  ()
cmd = string.format("gmsh step.geo -%d -order %d", dimension, order)
os.execute(cmd)
model:load ('step.msh')

groups = dgGroupCollection(model, dimension, order)
groups:buildGroupsOfInterfaces()

law=dgPerfectGasLaw(dimension)
zero = functionConstant({0.})
law:setViscosityAndThermalConductivity(zero,zero);
law:addBoundaryCondition('Walls',law:newNonSlipWallBoundary())
law:addBoundaryCondition('LeftRight',law:newOutsideValueBoundary(FS))
--law:addBoundaryCondition('Walls',law:newOutsideValueBoundary(FS))

rk=dgRungeKutta()
limiter = dgSlopeLimiter(law)
rk:setLimiter(limiter) 

print'*** setting the initial solution ***'
solution = dgDofContainer(groups, law:getNbFields())
solution:L2Projection(FS)
solution:exportMsh('output/init', 0, 0)
limiter:apply(solution)
solution:exportMsh('output/init_limit', 0, 0)

print'*** solve ***'
t = 0.0
CFL = 0.9
nbExport = 0
local x = os.clock()
for i=1,1.e4 do
    dt = CFL * rk:computeInvSpectralRadius(law,solution); 
    --norm = rk:iterate44(law,t, dt,solution)
    norm = rk:iterateEuler(law,t, dt,solution)
    t = t+dt 
    if (i % 10 == 0) then 
       print('|ITER|',i,'|NORM|',norm,'|DT|',dt,'|CPU|',os.clock() - x)
    end
    if (i % 100 == 0) then 
       solution:exportMsh(string.format('output/sol-%06d', i), t, nbExport) 
       nbExport = nbExport + 1
    end
end

print'*** done ***'


