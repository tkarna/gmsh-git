from Incompressible import *
from levelset import *
from dgpy import *
from math import *
import sys
import os

TIME = function.getTime()
os.system("rm output/*")
os.system("rm bubble_*.msh")
os.system("rm Area.dat")
x0 = time.clock()

#-------------------------------------------------
#-- Initial function
#-------------------------------------------------
def initF(FCT, XYZ):
	for i in range(0,FCT.size1()):
  		y = XYZ.get(i,1)
		FCT.set(i,0, 0.0) #u
		FCT.set(i,1, 0.0) #v
		FCT.set(i,2, 0.0) #p

def initLS(FCT, XYZ):
	for i in range(0,FCT.size1()):
          	x = XYZ.get(i,0)
                y = XYZ.get(i,1)
                eta = 1.0 + 0.01*sin(pi*(0.5-x))
		FCT.set(i,0, eta -y)

#-------------------------------------------------
#-- Parameters Common
#-------------------------------------------------
meshLS = "sloshing"
meshNS = "sloshing"
dt = 1.e-1
nbExport = 1

#-------------------------------------------------
#-- Parameters NS
#-------------------------------------------------
rhoMin = 1.0
rhoPlus  = 10.0
muMin  = 3.16/100.
muPlus = 3.16
UGlob = 1.0

print('*** Reynolds= %g' % (rhoPlus*UGlob*0.5/muMin))

#-------------------------------------------------
#-- Parameters LS
#-------------------------------------------------
doAdapt = False
doFEM = False
doREInit = False

order = 1
dimension = 2
ls = levelset(meshLS, order, dimension, doAdapt, doFEM, doREInit)
ns = Incompressible(meshNS, dimension)

#-------------------------------------------------
#-- Navier Stokes
#-------------------------------------------------
ls.initLevelset(initLS)

evalPHI = dgFunctionEvaluator(ls.groups[0], ls.solutions[0].getFunction(), ls.solutions[0])
PHINEW = evalPHI.newFunction(ns.XYZ)
rhoF = functionLevelsetSmoothNew(PHINEW, rhoMin, rhoPlus, 4.*ns.maxRadius)
muF = functionLevelsetSmoothNew(PHINEW, muMin, muPlus, 4.*ns.maxRadius)
ns.initializeIncomp(initF, rhoF, muF , UGlob)

GRAV = functionConstant([0., -9.81, 0.])
ns.law.setSource(GRAV)

ns.strongBoundaryConditionLine('left', ns.VELY)
ns.strongBoundaryConditionLine('right', ns.VELY)
ns.strongBoundaryConditionLine('bottom', ns.VELX)
ns.strongBoundaryConditionLine('top', ns.VELX)
ns.strongBoundaryConditionPoint('corner', ns.PZERO)

#-------------------------------------------------
#-- Levelset
#-------------------------------------------------

ls.setVelocityFromNS(ns)

ls.law.addBoundaryCondition('bottom',ls.law.new0FluxBoundary())
ls.law.addBoundaryCondition('top',ls.law.new0FluxBoundary())
ls.law.addBoundaryCondition('left',ls.law.new0FluxBoundary())
ls.law.addBoundaryCondition('right',ls.law.new0FluxBoundary())

#-------------------------------------------------
#-- Iterate
#-------------------------------------------------
timeOrder  = 2
ATol = 1.e-8
RTol = 1.e-6
Verb = 2
nsPetsc = "-pc_type lu" #"-ksp_rtol 1.e-4 -pc_type ilu -pc_factor_levels 2"
lsPetsc = "-pc_type lu"#"-ksp_rtol 1.e-2 -pc_type lu -pc_factor_levels 0"
ns.initSolver(timeOrder, ATol,RTol, Verb, nsPetsc, nbExport)
ls.initSolver(timeOrder, ATol,RTol, Verb, lsPetsc, nbExport)

for i in range (0,100) :
#if (i % 5 == 0):
#		print 'ReInitialize :-)'
#		ls.solutions[0].reInitialize(0.3)
	ns.doOneIteration(dt, i)
	ls.doOneIteration(dt, i)

