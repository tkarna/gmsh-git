from dgpy import *
import os
import time
from math import *

TIME = function.getTime()
os.system("rm output/*")

#-------------------------------------------------
#-- Case specific functions
#-------------------------------------------------
def initF(FCT, XYZ):
	for i in range(0,FCT.size1()):
          	x = XYZ.get(i,0)
                y = XYZ.get(i,1)
                phi = sqrt((x-.5)*(x-.5) + (y-.75)*(y-.75))-.15
		FCT.set(i,0, phi)

CCode = """
#include "fullMatrix.h"
#include "dgDataCacheMap.h"
extern "C" {
void velocityC (dgDataCacheMap *, fullMatrix<double> &fct, fullMatrix<double> &xyz, fullMatrix<double> &time) {
double t = time(0,0);
for (size_t i = 0; i< fct.size1(); i++) {
   double x = xyz(i,0);
   double y = xyz(i,1);
   double pi = 3.14;
   double u = sin(2.*pi*y)*sin(pi*x)*sin(pi*x); //*cos(pi*t/8);
   double v = -sin(2.*pi*x)*sin(pi*y)*sin(pi*y); //*cos(pi*t/8);
   fct.set(i,0,u);
   fct.set(i,1,v);
   fct.set(i,2,0.0);
  }
}
}
"""
tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CCode, tmpLib);


"""                
def velocityVortex(FCT, XYZ ):
	for i in range(0,FCT.size1()):
          	x = XYZ.get(i,0)
                y = XYZ.get(i,1)
                u = sin(2.*pi*y)*sin(pi*x)*sin(pi*x) #*cos(pi*t/8)
                v = -sin(2.*pi*x)*sin(pi*y)*sin(pi*y) #*cos(pi*t/8)
		FCT.set(i,0, u)
		FCT.set(i,1, v)
		FCT.set(i,2, 0.0)
VEL = functionPython(3, velocityVortex, [XYZ])
"""

#-------------------------------------------------
#-- Initialize level set problem
#-------------------------------------------------

print'---- Load Mesh & Geometry'
meshName = "box"
order = 4
dimension = 2

model = GModel()
p = os.system("gmsh_dynamic %s.geo -%d -order %d"%(meshName, dimension, order))
model.load (meshName+".geo")
model.load(meshName+".msh")

print'---- Build Groups'
groups = dgGroupCollection(model, dimension, order)
groups.buildGroupsOfInterfaces()
XYZ = groups.getFunctionCoordinates()

print'---- Load Conservation law'
VELC = functionC(tmpLib,"velocityC",3,[XYZ, TIME])
law = dgConservationLawAdvectionDiffusion.advectionLaw(VELC)
analytical = dgDofContainer(groups, 3)
analytical.L2Projection(VELC)
analytical.exportFunctionMsh(law.getVelocity(), 'output/Ana', 0, 0, 'vel')

print'---- Initial condition'
INIT = functionPython(1, initF, [XYZ])
solution = dgDofContainer(groups,law.getNbFields())
solution.L2Projection(INIT)
solution.exportMsh('output/LS-00000', 0., 0)

print'---- Load Boundary conditions'
#outsideValue = functionConstant([0,0,0])
#law.addBoundaryCondition('bottom',law.newOutsideValueBoundary("bottom",outsideValue))
law.addBoundaryCondition('top'   ,law.newSymmetryBoundary("top"))
law.addBoundaryCondition('bottom' , law.newSymmetryBoundary("bottom"))
law.addBoundaryCondition('left' , law.newSymmetryBoundary("left"))
law.addBoundaryCondition('right' ,law.newSymmetryBoundary("right"))

#-------------------------------------------------
#-- Integration Tools
#-------------------------------------------------

def areaF(FCT, sol):
	for i in range(0,FCT.size1()):
          	phi = sol.get(i,0)
		value = 0.0
		if (phi < 0):
			value = 1.0
		FCT.set(i,0, value)
AREA = functionPython(1, areaF, [solution.getFunction()])
integ=dgFunctionIntegrator(groups, AREA)
localArea=fullMatrixDouble(1,1)

integ.compute(localArea)

os.system("rm Area.dat")
fun = open("Area.dat","a")
A0  = localArea.get(0,0)
print >>fun, ' ', 0, ' ',A0, ' ', 0
print'Area_0: ', A0, 'Loss: ', 0

#-------------------------------------------------
#-- Run
#-------------------------------------------------

print'---- Solver Definition'
implicit = False
sys = {}
solver = {}
if (implicit) :
  sys = linearSystemPETScBlockDouble()
  sys.setParameter("petscOptions", "-pc_type lu")
  dof = dgDofManager.newDGBlock(groups, law.getNbFields(), sys)
  solver = dgDIRK(law, dof, 2)
  solver.getNewton().setVerb(10)
  dt = 0.02
  nbExport = 1
else:
  solver = dgERK(law, None, DG_ERK_44)
  nbExport = 20

print'---- Solve'
t = 0
x = time.clock()
CFL = 0.8
tMax = 4.5
i=0
#nbIter = 400
#for i in range(0,nbIter): 
while (t<tMax):
  norm = 0.0
  if (implicit):
    solver.iterate(solution, dt , t)
    norm = solver.getNewton().getFirstResidual()
  else:
    dt = CFL*solver.computeInvSpectralRadius(solution);  
    solver.iterate(solution, dt, t)
    norm = solution.norm()
  t = t + dt
  i = i+1
  if (i % nbExport == 0):
     print'|ITER|',i,'|NORM|',norm,'|DT|',dt, '|T|',t, '|CPU|',time.clock() - x
     solution.exportMsh("output/LS-%05d" % (i), t, i/nbExport)
     integ.compute(localArea)
     Aloc = localArea.get(0,0)
     print 'Area: ', Aloc, 'Loss: ', (Aloc-A0)/A0
     print >>fun, ' ', t, ' ',Aloc, ' ', (Aloc-A0)/A0
     	
fun.close()
