from dgpy import *
from Incompressible import *
from levelset import *
from Common import *
from math import *
import sys
import os

os.system("rm output/*")
os.system("rm bubble_adapt*.msh")
os.system("rm bubbleAdapt_*.msh")
os.system("rm Area.dat")
x0 = time.clock()

#-------------------------------------------------
#-- Initial function
#-------------------------------------------------
def initF(FCT, XYZ):
	for i in range(0,FCT.size1()):
  		y = XYZ.get(i,1)
    	        #FCT.set(i,0, vmax*(1-(y/0.5)*(y/0.5)))
		FCT.set(i,0, UGlob) #u
		FCT.set(i,1, 0.0) #v
		FCT.set(i,2, 0.0) #p

def VelBC(FCT, XYZ ):
	for i in range(0,FCT.size1()):
		y = XYZ.get(i,1)
		FCT.set(i,0, vmax*(1-((y-0.5)/0.5)*((y-0.5)/0.5)))
		#FCT.set(i,0, 1.0)
		FCT.set(i,1, 0.0)
		FCT.set(i,2, 0.0)
		FCT.set(i,3, fixed)
		FCT.set(i,4, fixed)
		FCT.set(i,5, free)

def initLS(FCT, XYZ):
	for i in range(0,FCT.size1()):
          	x = XYZ.get(i,0)
                y = XYZ.get(i,1)
                phi = sqrt((x-.5)*(x-.5) + (y-.5)*(y-.5))-.15
	
		if (fabs(phi) < E) :
			phiFilter = 2*E/pi*sin(0.5*pi*phi/E)
		else:
			sign = 1.0 if phi > 0.0 else -1.0
			phiFilter = 2*E/pi*sign
		#print 'phi = ', phiFilter
		FCT.set(i,0, phi) #Filter)

INITLS = functionPython(1, initLS, [XYZ])
VEL = functionPython(6, VelBC, [XYZ])

#-------------------------------------------------
#-- Parameters Common
#-------------------------------------------------
meshName = "bubble"
dt = 1.e-2
nbExport = 1
tMax = 2.0

#-------------------------------------------------
#-- Parameters NS
#-------------------------------------------------
RHO = 1.0
MU  = 0.1
vmax = 1.0
UGlob = 2./3.*vmax
print('*** Reynolds= %g' % (RHO*UGlob*1/MU))

#-------------------------------------------------
#-- Parameters LS
#-------------------------------------------------
order = 1
dimension = 2
ls = levelset(order, dimension)

doAdapt = False
doFEM = False
E = 0.07 # width filter level set

if (doAdapt):
	N = 15 
	lcMin = E/N
	lcMax = 3.e-1
	eps  = 1.e-2
	typeOfMetric = "frey" # grad and hession
	#typeOfMetric = "coupez" # distance with N and E
	ls.initAdapt(typeOfMetric, lcMin, lcMax, eps, N, E)
	
#-------------------------------------------------
#-- Navier Stokes
#-------------------------------------------------
ns = Incompressible(2) 
rhoF = functionConstant(RHO)
muF = functionConstant(MU)
ns.initializeIncomp(meshName, initF, rhoF, muF , UGlob)

ns.strongBoundaryConditionLine('left', VEL)
ns.strongBoundaryConditionLine('top', ns.WALL)
ns.strongBoundaryConditionLine('bottom', ns.WALL)
ns.weakPresBoundaryCondition('right', ns.PRESZERO)

petscOptions ="-ksp_rtol 1.e-3 -pc_type lu -pc_factor_levels 1"
nbTimeSteps = 20
dt0 = 1000
ATol = 1.e-8
RTol = 1.e-5
Verb = 2
ns.pseudoTimeSteppingSolve(nbTimeSteps, dt0, ATol, RTol, Verb, petscOptions)

x1 = time.clock()
print '|TOTAL CPU|',x1 - x0

#-------------------------------------------------
#-- Levelset
#-------------------------------------------------
ls.initLevelset(meshName, doAdapt, doFEM, INITLS)

def funbc(ls):
	outsideValue = functionConstant([0])
	ls.law.addBoundaryCondition('bottom',ls.law.new0FluxBoundary())
	ls.law.addBoundaryCondition('top',ls.law.new0FluxBoundary())
	ls.law.addBoundaryCondition('left',ls.law.newOutsideValueBoundary("left",INITLS))
	ls.law.addBoundaryCondition('right',ls.law.newOutsideValueBoundary("right",outsideValue))

ls.setVelocityFromNS(ns, funbc)

#-------------------------------------------------
#-- Run
#-------------------------------------------------
timeOrder  = 2
ATol = 1.e-8
RTol = 1.e-5
petscOptions = "-pc_type lu"
ls.initSolver(timeOrder, ATol,RTol, petscOptions, nbExport)

i = 0
while (ls.t < tMax):
	ls.doOneIteration(dt, i)
	i = i + 1

print '|TOTAL CPU|',time.clock() - x1
