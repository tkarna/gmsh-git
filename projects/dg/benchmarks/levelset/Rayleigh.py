from dgpy import *
from Incompressible import *
from levelset import *
from math import *
import sys
import os

TIME = function.getTime()
os.system("rm output/*")
os.system("rm rayleigh_*.msh")
os.system("rm Area.dat")
x0 = time.clock()
GmshSetOption('General', 'Verbosity', 1.0)

#-------------------------------------------------
#-- Initial function
#-------------------------------------------------
def initF(FCT, XYZ):
	for i in range(0,FCT.size1()):
  		y = XYZ.get(i,1)
		FCT.set(i,0, 0.0) #u
		FCT.set(i,1, 0.0) #v
		FCT.set(i,2, 0.0) #p

def initLS(FCT, XYZ):
	for i in range(0,FCT.size1()):
          	x = XYZ.get(i,0)
                y = XYZ.get(i,1)
                phi = y - 2.0- 0.05*cos(2*pi*x) 	
		FCT.set(i,0, phi)

#-------------------------------------------------
#-- Parameters Common
#-------------------------------------------------
meshLS = "rayleighNS"
meshNS = "rayleighNS"
dt = 1.e-2
nbExport = 5

#-------------------------------------------------
#-- Parameters NS
#-------------------------------------------------
rhoMin = 1.0
rhoPlus  = 1.5
muMin  = 2.e-3
muPlus = 2.e-3
UGlob = 1.0

print('*** Atwood = %g' % ((rhoPlus-rhoMin)/(rhoPlus+rhoMin)))

#-------------------------------------------------
#-- Parameters LS
#-------------------------------------------------
doAdapt = False
doFEM = False
doREInit = False

order = 1
dimension = 2
ls = levelset(meshLS, order, dimension, doAdapt, doFEM, doREInit)
ls.initLevelset(initLS)

if (doAdapt):
	lcMin = ls.E/10
	lcMax = 0.1
	eps  = 1.e-1
	#typeOfMetric = "frey" # grad and hession
	typeOfMetric = "coupez" # distance with E	
	ls.initAdapt(typeOfMetric, lcMin, lcMax, eps)
	
#-------------------------------------------------
#-- Navier Stokes
#-------------------------------------------------
ns = Incompressible(meshNS, dimension)

evalPHI = dgFunctionEvaluator(ls.groups[0], ls.solutions[0].getFunction())
PHINEW = evalPHI.newFunction(ns.XYZ)
#rhoF = functionLevelsetNew(PHINEW, rhoMin, rhoPlus)
#muF = functionLevelsetNew(PHINEW, muMin, muPlus)
rhoF = functionLevelsetSmoothNew(PHINEW, rhoMin, rhoPlus, 1.5*ns.maxRadius)
muF = functionLevelsetSmoothNew(PHINEW, muMin, muPlus, 1.5*ns.maxRadius)
ns.initializeIncomp(initF, rhoF, muF , UGlob)

GRAV = functionConstant([0., -9.81, 0.])
ns.law.setSource(GRAV)

def funbc_ns(ns):
	ns.strongBoundaryConditionLine('left', ns.VELY)
	ns.strongBoundaryConditionLine('right', ns.VELY)
	ns.strongBoundaryConditionLine('bottom', ns.VELX)
	ns.strongBoundaryConditionLine('top', ns.PZERO)

funbc_ns(ns)

#-------------------------------------------------
#-- Levelset
#-------------------------------------------------
def funbc(ls):
	ls.law.addBoundaryCondition('bottom',ls.law.new0FluxBoundary())
	ls.law.addBoundaryCondition('top',ls.law.new0FluxBoundary())
	ls.law.addBoundaryCondition('left',ls.law.new0FluxBoundary())
	ls.law.addBoundaryCondition('right',ls.law.new0FluxBoundary())
	
ls.setVelocityFromNS(ns,funbc)

#-------------------------------------------------
#-- Iterate
#-------------------------------------------------
timeOrder  = 2
explicit = True
ATol = 1.e-8
RTol = 1.e-5
Verb = 1
nsPetsc = "-pc_type lu"
lsPetsc = "-pc_type lu"#"-ksp_rtol 1.e-2 -pc_type lu -pc_factor_levels 0"

ns.initSolver(timeOrder, ATol, RTol, Verb, nsPetsc, nbExport)
ls.initSolver(explicit, timeOrder, ATol, RTol, Verb, lsPetsc, nbExport)

for i in range (0,300) :
	ls.solutions[0].reInitialize(-1.0)
	ns.doOneIteration(dt, i)
	ls.doOneIteration(dt, i)    
	
	if (doAdapt):
		ns2 = Incompressible("current", dimension)	
		evalPHI = dgFunctionEvaluator(ls.groups[0], ls.solutions[0].getFunction())
		PHINEW = evalPHI.newFunction(ns2.XYZ)
		rhoF = functionLevelsetSmoothNew(PHINEW, rhoMin, rhoPlus, 3.0*ns2.maxRadius)
		muF = functionLevelsetSmoothNew(PHINEW, muMin, muPlus, 3.0*ns2.maxRadius)
		ns2.initializeIncomp(initF, rhoF, muF , UGlob)
		
		GRAV = functionConstant([0., -9.81, 0.])
		ns2.law.setSource(GRAV)
		funbc_ns(ns2)

		pm = dgMesh2MeshProjection(ns.solution,ns2.solution)
		pm.projectFromTo(ns.solution,ns2.solution)
		ns2.initSolver(timeOrder, ATol,RTol, Verb, nsPetsc, nbExport)

		ns = ns2
		
		ls.setVelocityFromNS(ns,funbc)

		outputstr = "output/nsDT-%06d" % (i)
		ns.solution.exportMsh(outputstr, 0, 0)
