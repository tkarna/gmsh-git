Mesh.CharacteristicLengthExtendFromBoundary=1;

lc = 1/20;
p = 32;
L= 4;
h = 2.0;

Point(1) = {0.0, 0.0, 0, lc};
Point(2) = {L, 0.0, 0, lc};
Point(3) = {L, h, 0, lc};
Point(4) = {0, h, 0, lc};
Line(1) = {4, 3};
Line(2) = {3, 2};
Line(3) = {2, 1};
Line(4) = {1, 4};
Line Loop(5) = {2, 3, 4, 1};
Plane Surface(6) = {5};
//Transfinite Line {1, 3} = 33 Using Progression 1;
//Transfinite Line {2, 4} = 266 Using Progression 1;
//Transfinite Surface {6};

Physical Line("top") = {1};
Physical Line("bottom") = {3};
Physical Line("left") = {4};
Physical Line("right") = {2};
Physical Surface("inside") = {6};
Physical Point("corner") ={1};

