Mesh.Algorithm = 3;
Mesh.CharacteristicLengthExtendFromBoundary=1;

lc = 1/10;
lc2  = 1/40;
p = 2.5;
L=2.5; 
h = 1.5;

Point(1) = {0.0, 0.0, 0, lc2};
Point(2) = {L, 0.0, 0, lc2};
Point(3) = {L, h, 0, lc};
Point(4) = {0, h, 0, lc};
Line(1) = {4, 3};
Line(2) = {3, 2};
Line(3) = {2, 1};
Line(4) = {1, 4};
Line Loop(5) = {2, 3, 4, 1};
Plane Surface(6) = {5};
//Transfinite Line {1, 3} = (2*p*10)+1 Using Progression 1;
//Transfinite Line {2, 4} = (p*10)+1 Using Progression Bump 0.05;
//Transfinite Surface {6};

Physical Point("corner") ={1};
Physical Line("top") = {1};
Physical Line("bottom") = {3};
Physical Line("left") = {4};
Physical Line("right") = {2};
Physical Surface("inside") = {6};


