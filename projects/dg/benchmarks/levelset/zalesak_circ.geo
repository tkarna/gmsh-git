lc = 0.8;

Point(5) = {50,50,0.0,lc};
Point(6) = {0.,50,0.0,lc};
Point(7) = {50.,0.,0.0,lc};
Point(8) = {50.,100.,0.0,lc};
Point(9) = {100.,50.,0.0,lc};

Circle(100) = {8,5,6};
Circle(101) = {6,5,7};
Circle(102) = {7,5,9};
Circle(103) = {9,5,8};


Line Loop(104) = {103,100,101,102};
Plane Surface(105) = {104};

Physical Line("wall") = {103,100,101,102};
Physical Surface("all") = {105};
