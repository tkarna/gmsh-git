from dgpy import *
import os
import time
from math import *

TIME = function.getTime()
os.system("rm output/*")

#-------------------------------------------------
#-- Case specific functions
#-------------------------------------------------
def initF(FCT, XYZ):
	for i in range(0,FCT.size1()):
          	x = XYZ.get(i,0)
                y = XYZ.get(i,1)
                circle = sqrt((x-50.)*(x-50.0) + (y-75.)*(y-75.))-15.0
		FCT.set(i,0, rect)

CCode = """
#include "fullMatrix.h"
#include "dgDataCacheMap.h"
extern "C" {
void velocityC (dgDataCacheMap *, fullMatrix<double> &fct, fullMatrix<double> &xyz, fullMatrix<double> &time) {
double t = time(0,0);
for (size_t i = 0; i< fct.size1(); i++) {
   double x = xyz(i,0);
   double y = xyz(i,1);
   double pi = 3.14;
   double u = (pi/314.0)*(50-y); 
   double v = (pi/314.0)*(x-50);
   fct.set(i,0,u);
   fct.set(i,1,v);
   fct.set(i,2,0.0);
  }
}
void initC (dgDataCacheMap *, fullMatrix<double> &fct, fullMatrix<double> &xyz) {
for (size_t i = 0; i< fct.size1(); i++) {
  double x = xyz(i,0);
  double y = xyz(i,1);
  double pi = 3.14;
  double circle = sqrt((x-50.)*(x-50.0) + (y-75.)*(y-75.))-15.0;
  double x1= 47.5;
  double y1= 40.;
  double x2= 52.5;
  double y2 = 85.;
  double rect = 0.0;
  if ( x>x1 && x<x2 && y>y1 && y<y2) rect = std::min(std::min(x-x1,x2-x), std::min(y-y1,y2-y));
  else if ( x>x2 && y>y1 && y<y2)  rect = -1.0*(x-x2);
  else if ( x<x1 && y>y1 && y<y2)  rect = -1.0*(x1-x);
  else if ( y<y1 && x>x1 && x<x2 ) rect = -1.0*(y1-y);
  else if ( y>y2 && x>x1 && x<x2 ) rect = -1.0*(y-y2);
  else if ( x>x2 && y > y2) rect = -1.0*sqrt((x-x2)*(x-x2) + (y-y2)*(y-y2));
  else if ( x<x1 && y < y1) rect = -1.0*sqrt((x-x1)*(x-x1) + (y-y1)*(y-y1));
  else if ( x>x2 && y < y1) rect = -1.0*sqrt((x-x2)*(x-x2) + (y-y1)*(y-y1));
  else if ( x<x1 && y > y2) rect = -1.0*sqrt((x-x1)*(x-x1) + (y-y2)*(y-y2));
    
  double phi = 0.0;
  if(rect<0.0 && circle<0.0){  //dans notched disk(--) ->dist negative
  phi = std::max(rect,circle); 
  }
  else if (rect<0.0 && circle>0.0){//en dehors carre et en dehors cercle(-+) -->dist cercle
    phi = circle;
  }
  else if (rect>0.0 && circle<0.0){//dans carre dans le cercle(+-) ->dist carre
     phi = rect;
  }
  else{ //dans carre en dehors du cercle(++) ->plus grande distance cercle carre
     phi =  std::max(rect,circle); 
  }
  double E= 5.0;
  double phiFilter = 0.0;
  double sign = (phi< 0.0) ? -1.0: 1.0;
  if (fabs(phi) < E)  phiFilter = 2.*E/pi*sin(0.5*pi*phi/E);
  else phiFilter = 2.*E/pi*sign;
  
  fct.set(i,0,phiFilter);
  }
}
}
"""
tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CCode, tmpLib);

#-------------------------------------------------
#-- Initialize level set problem
#-------------------------------------------------

print'---- Load Mesh & Geometry'
meshName = "zalesak_circ"
order = 1
dimension = 2
CG = True
timeOrder = 4
nbExport = 10

#GmshSetOption('Mesh', 'ElementOrder', float(order))
#model.mesh(2)
#model.save(meshName+".msh")
#cmd = "gmsh %s.geo -%d -order %d " % (meshName,dimension, order)
#os.system(cmd)

model = GModel()
p = os.system("gmsh %s.geo -%d -order %d"%(meshName, dimension, order))
model.load (meshName+".geo")
model.load(meshName+".msh")

print'---- Build Groups'
groups = dgGroupCollection(model, dimension, order)
groups.buildGroupsOfInterfaces()
XYZ = groups.getFunctionCoordinates()

print'---- Load Conservation law'
VELC = functionC(tmpLib,"velocityC",3,[XYZ, TIME])
if (CG):
	velDOF = dgDofContainer(groups, 3)
	velDOF.L2Projection(VELC)
	law = dgConservationLawAdvectionDiffusionSUPG.advectionLaw(velDOF.getFunction(), velDOF.getFunctionGradient())
	solutionPREV = dgDofContainer(groups, 1)
	law.setPreviousSolution(solutionPREV.getFunction())
	law.setPreviousSolutionGradient(solutionPREV.getFunctionGradient())
else:
	law = dgConservationLawAdvectionDiffusion.advectionLaw(VELC)

analytical = dgDofContainer(groups, 3)
analytical.L2Projection(VELC)
analytical.exportFunctionMsh(law.getVelocity(), 'output/Ana', 0, 0, 'vel')

print'---- Initial condition'
INIT = functionC(tmpLib,"initC",1,[XYZ, TIME])
#INIT = functionPython(1, initF, [XYZ])
solution = dgDofContainer(groups,law.getNbFields())
solution.L2Projection(INIT)
solution.exportMsh('output/LS-00000', 0., 0)

print'---- Load Boundary conditions'
law.addBoundaryCondition('wall'   ,law.newSymmetryBoundary("wall"))
#law.addBoundaryCondition('top'   ,law.newSymmetryBoundary("top"))
#law.addBoundaryCondition('bottom' , law.newSymmetryBoundary("bottom"))
#law.addBoundaryCondition('left' , law.newSymmetryBoundary("left"))
#law.addBoundaryCondition('right' ,law.newSymmetryBoundary("right"))

#-------------------------------------------------
#-- Integration Tools
#-------------------------------------------------
def areaF(FCT, sol):
	for i in range(0,FCT.size1()):
          	phi = sol.get(i,0)
		value = 0.0
		if (phi < 0):
			value = 1.0
		FCT.set(i,0, value)
AREA = functionPython(1, areaF, [solution.getFunction()])
integ=dgFunctionIntegrator(groups, AREA)
localArea=fullMatrixDouble(1,1)

integ.compute(localArea)

os.system("rm Area.dat")
fun = open("Area.dat","a")
A0  = localArea.get(0,0)
print >>fun, ' ', 0, ' ',A0, ' ', 0
print'Area_0: ', A0, 'Loss: ', 0

#-------------------------------------------------
#-- Run
#-------------------------------------------------

print'---- Solver Definition'
dof = None
if (CG):
   petsc =  linearSystemPETScDouble()
   dof = dgDofManager.newCG(groups, 1, petsc)
   petsc.setParameter("petscOptions", "-pc_type lu")
   petsc.setParameter("matrix_reuse", "same_matrix")
solver = dgERK(law, dof, timeOrder)

print'---- Solve'
t = 0
x = time.clock()
CFL = 0.8
tMax = 628
i=0
#nbIter = 400
#for i in range(0,nbIter): 
while (t<tMax):
	if (CG):
		solutionPREV.copy(solution)
		groups.buildStreamwiseLength(law.getVelocity())
	dt = CFL * solver.computeInvSpectralRadius(solution)*10;  
	solver.iterate(solution, dt, t);
	norm = solution.norm()
	t = t + dt
	i = i+1
	if (i % nbExport == 0):
		print'|ITER|',i,'|NORM|',norm,'|DT|',dt, '|T|',t, '|CPU|',time.clock() - x
		solution.exportMsh("output/LS-%05d" % (i), t, i/nbExport)
		integ.compute(localArea)
		Aloc = localArea.get(0,0)
		print 'Area: ', Aloc, 'Loss: ', (Aloc-A0)/A0
		print >>fun, ' ', t, ' ',Aloc, ' ', (Aloc-A0)/A0
		
fun.close()
