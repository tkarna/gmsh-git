from Incompressible import *
from levelset import *
from dgpy import *
from math import *
import sys
import os

TIME = function.getTime()
os.system("rm output/*")
os.system("rm bubble_*.msh")
os.system("rm Area.dat")
x0 = time.clock()

#-------------------------------------------------
#-- Initial function
#-------------------------------------------------
def initF(FCT, XYZ):
	for i in range(0,FCT.size1()):
  		x = XYZ.get(i,0)
		FCT.set(i,0, 0.0) #u
		FCT.set(i,1, 0.0) #v
		FCT.set(i,2, 0.0) #p

def initLS(FCT, XYZ):
	for i in range(0,FCT.size1()):
          	x = XYZ.get(i,0)
                y = XYZ.get(i,1)
		phi = 1.0 - y
		FCT.set(i,0, phi)
"""
def VelBC(FCT, XYZ ):
	for i in range(0,FCT.size1()):
		x = XYZ.get(i,0)
		y = XYZ.get(i,1)
		d = 1.0
		H = 2.0
		v1 = dp*(d*(muPlus-muMin*(1-d))*x**2-H*(muPlus-muMin*(1-d**2))*x)/(2*muMin*d*(muPlus-muMin*(1-d)))
		v2 = dp*(d*(muPlus-muMin*(1-d))*x**2-H*(muPlus-muMin*(1-d**2))*x - H*H*(muPlus*d+muMin*(1-d)))/(2*muPlus*d*(muPlus-muMin*(1-d)))
		FCT.set(i,1, 0.0)
		FCT.set(i,2, 0.0)
		FCT.set(i,3, fixed)
		FCT.set(i,4, fixed)
		FCT.set(i,5, free)
VEL = functionPython(6, VelBC, [XYZ])
"""

#-------------------------------------------------
#-- Parameters Common
#-------------------------------------------------
meshName = "twoPhase"
dt = 1.e-3
nbExport = 1

#-------------------------------------------------
#-- Parameters NS
#-------------------------------------------------
rhoMin = 1.0
rhoPlus  = 1.0
muMin  = 0.1
muPlus = 0.1
dp = -0.212435
UGlob = 1.0

print('*** Reynolds= %g' % (rhoPlus*UGlob*0.5/muMin))

#-------------------------------------------------
#-- Parameters LS
#-------------------------------------------------
order = 1
dimension = 2
ls = levelset(order, dimension)

doAdapt = False
doFEM = False
doREInit = False
tMax = 2.0

#-------------------------------------------------
#-- Navier Stokes
#-------------------------------------------------
ls.initLevelset(meshName, initLS, doAdapt, doFEM, doREInit)
evalPHI = dgFunctionEvaluator(ls.groups[0], ls.solutions[0].getFunction(), ls.solutions[0])
PHINEW = evalPHI.newFunction(XYZ)

ns = Incompressible(2)
rhoF = functionLevelsetSmoothNew(PHINEW, rhoMin, rhoPlus, ls.E)
muF = functionLevelsetSmoothNew(PHINEW, muMin, muPlus, ls.E)
ns.initializeIncomp(meshName, initF, rhoF, muF , UGlob)

test = dgDofContainer(ns.groups, 1);
test.interpolate(rhoF)
test.exportMsh('output/RHO', 0., 0)

GRAV = functionConstant([0., -9.81, 0.])
ns.law.setSource(GRAV)

PTWO = functionConstant([2.0])
ns.strongBoundaryConditionLine('left', PTWO)
ns.strongBoundaryConditionLine('right', ns.PZERO)
ns.strongBoundaryConditionLine('bottom', ns.WALL)
ns.strongBoundaryConditionLine('top', ns.WALL)

#-------------------------------------------------
#-- Levelset
#-------------------------------------------------

evalNS = dgFunctionEvaluator(ns.groups, ns.law.getVelocity(), ns.solution)
VELNEW = evalNS.newFunction(XYZ)
ls.setLaw(VELNEW)

ls.law.addBoundaryCondition('bottom',ls.law.new0FluxBoundary())
ls.law.addBoundaryCondition('top',ls.law.new0FluxBoundary())
ls.law.addBoundaryCondition('left',ls.law.new0FluxBoundary())
ls.law.addBoundaryCondition('right',ls.law.new0FluxBoundary())

#-------------------------------------------------
#-- Iterate
#-------------------------------------------------
timeOrder  = 2
ATol = 1.e-8
RTol = 1.e-6
Verb = 2
nsPetsc = "-pc_type lu"
lsPetsc = "-pc_type lu"#"-ksp_rtol 1.e-2 -pc_type lu -pc_factor_levels 0"
ns.initSolver(timeOrder, ATol,RTol, Verb, nsPetsc, nbExport)
ls.initSolver(timeOrder, ATol,RTol, Verb, lsPetsc, nbExport)

ns.pseudoTimeSteppingSolve(10, dt, ATol, RTol, Verb, nsPetsc)
exit(1)

for i in range (0,200) :
	if (i % 5 == 0):
		print 'ReInitialize :-)'
		ls.solutions[0].reInitialize(0.3)
	ns.doOneIteration(dt, i)
	ls.doOneIteration(dt, i)

	test = dgDofContainer(ns.groups, 1);
	test.interpolate(rhoF)
	out4 = "output/rho%d" % i
	test.exportMsh(out4, 0., 0)
