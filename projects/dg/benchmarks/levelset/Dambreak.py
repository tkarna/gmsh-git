from dgpy import *
from Incompressible import *
from levelset import *
from math import *
import sys
import os

TIME = function.getTime()
os.system("rm output/*")
os.system("rm dambreak_adapt*.msh")
os.system("rm Area.dat")
x0 = time.clock()

#-------------------------------------------------
#-- Initial function
#-------------------------------------------------
def initF(FCT, XYZ):
	for i in range(0,FCT.size1()):
		x = XYZ.get(i,0)
  		y = XYZ.get(i,1)
		if (x< 1 and y < 1):
			p = rhoMin*9.81*1.5+rhoPlus*9.81*(1-y)
		else:
			p = rhoMin*9.81*(2.5-y)
		FCT.set(i,0, 0.0) #u
		FCT.set(i,1, 0.0) #v
		FCT.set(i,2, p) #p

def initLS(FCT, XYZ):
	for i in range(0,FCT.size1()):
          	x = XYZ.get(i,0)
                y = XYZ.get(i,1)
		x0 = 1.0
		y0 = 1.0
		if ((x>x0) and (y<y0) ):
			phi = (x0-x)
		else:
			if ((y>y0) and (x<x0) ):
				phi = (y0-y)
			else:
				if (  (x<x0) and (y<y0) ):
					if ( (x0-x) <  (y0-y) ):
						phi = (x0-x)
					else:
						phi = (y0-y)
				else:
					phi = -math.sqrt((x-x0)*(x-x0)+(y-y0)*(y-y0))	
		if (math.fabs(phi) < ls.E) :
			phiFilter = 2*ls.E/math.pi*sin(0.5*math.pi*phi/ls.E)
		else:
			sign = 1.0 if phi > 0.0 else -1.0
			phiFilter = 2*ls.E/math.pi*sign
		#FCT.set(i,0, phiFilter)
		FCT.set(i,0, phi)

#-------------------------------------------------
#-- Parameters Common
#-------------------------------------------------
meshNS = "dambreak"
meshLS = "dambreak"
dt =  0.01 #sqrt(h/g)

#-------------------------------------------------
#-- Parameters NS
#-------------------------------------------------
rhoMin   = 1.0
rhoPlus  = 10.0 
muMin  = 1.e-3
muPlus = 1.e-3
UGlob = 1.0

#-------------------------------------------------
#-- Parameters LS
#-------------------------------------------------
doAdapt = False
doFEM =  False
doREInit = False

order = 1
dimension = 2
ls = levelset(meshLS, order, dimension, doAdapt, doFEM, doREInit)
ns = Incompressible(meshNS, dimension)

if (doAdapt):
	ls.E = 0.05
	lcMin = ls.E/10
	lcMax = 0.1
	eps  = 1.e-1
	#typeOfMetric = "frey" # grad and hession
	typeOfMetric = "coupez" # distance with E	
	ls.initAdapt(typeOfMetric, lcMin, lcMax, eps)
	
#-------------------------------------------------
#-- Navier Stokes
#-------------------------------------------------
ls.initLevelset(initLS)

evalPHI = dgFunctionEvaluator(ls.groups[0], ls.solutions[0].getFunction(), ls.solutions[0])
PHINEW = evalPHI.newFunction(ns.XYZ)
#rhoF = functionLevelsetNew(PHINEW, rhoMin, rhoPlus)
#muF = functionLevelsetNew(PHINEW, muMin, muPlus)
rhoF = functionLevelsetSmoothNew(PHINEW, rhoMin, rhoPlus, 3.*ns.maxRadius)
muF = functionLevelsetSmoothNew(PHINEW, muMin, muPlus, 3.*ns.maxRadius)

ns.initializeIncomp(initF, rhoF, muF , UGlob)

GRAV = functionConstant([0., -9.81, 0.])
ns.law.setSource(GRAV)

ns.strongBoundaryConditionLine('left', ns.VELY)
ns.strongBoundaryConditionLine('right', ns.VELY)
#ns.weakSlipBoundaryCondition('bottom')
ns.strongBoundaryConditionLine('bottom', ns.VELX)
#ns.strongBoundaryConditionLine('top', ns.VELX)
ns.strongBoundaryConditionLine('top', ns.PZERO)
#ns.strongBoundaryConditionPoint('corner', ns.PZERO)

#-------------------------------------------------
#-- Levelset
#-------------------------------------------------
def funbc(ls):
	ls.law.addBoundaryCondition('bottom',ls.law.new0FluxBoundary())
	ls.law.addBoundaryCondition('top',ls.law.new0FluxBoundary())
	ls.law.addBoundaryCondition('left',ls.law.new0FluxBoundary())
	ls.law.addBoundaryCondition('right',ls.law.new0FluxBoundary())

ls.setVelocityFromNS(ns, funbc)

#-------------------------------------------------
#-- Iterate
#-------------------------------------------------
timeOrder  = 1
ATol = 1.e-5
RTol = 1.e-5
Verb = 0
nsPetsc = "-ksp_rtol 1.e-5 -pc_type ilu -pc_factor_levels 2"
lsPetsc = "-pc_type lu"
explicit = False

print '--- init solvers'
nbExport = 10
ns.initSolver(timeOrder, ATol, RTol, Verb, nsPetsc, nbExport)
ls.initSolver(explicit, timeOrder, ATol, RTol, Verb, lsPetsc, nbExport)

rk = dgERK(ls.law,None,1)
for i in range (0,10000) :

	tStep = ls.t + dt
	while (ls.t < tStep):
		dt_loc = min(dt, 5.*rk.computeInvSpectralRadius(ls.solutions[0]), tStep-ls.t)
		ns.doOneIteration(dt_loc, i)
		ls.doOneIteration(dt_loc, i)
		ls.solutions[0].reInitialize(-1.0)

	print "***************** T = ", ls.t
	outputstr = "output/lsDT-%06d" % (i)
	ls.solutions[0].exportMsh(outputstr, ls.t, i)
	outputstr2 = "output/nsDT-%06d" % (i)
	ns.solution.exportFunctionMsh(ns.law.getVelocity(), outputstr2, ns.t, i, 'vel')
	ns.solution.exportFunctionMsh(ns.law.getPressure(), outputstr2, ns.t, i, 'pres')

        test = dgDofContainer(ns.groups, 1);
	test.interpolate(rhoF)
	out4 = "output/rho%d" % i
	test.exportMsh(out4, 0., 0)
	
