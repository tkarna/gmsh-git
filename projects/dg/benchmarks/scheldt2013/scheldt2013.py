# Import for python
import sys
sys.path.insert(0, './input')
from dgpy import *
from math import *
from ProjectMesh import *
import gmshPartition
import time, os, calendar

# mesh
mshName = 'full2'
glib = './input/lib_gbr.so'
if Msg.GetCommRank() == 0:
  functionC.buildLibraryFromFile('./input/GBR.cc', glib);
  modelO = GModel()
  modelO.load('./input/' + mshName + '.msh')
  projectMesh(modelO)
  modelO.save('./input/' + mshName + '_tan.msh')
Msg.Barrier()
model = GModel()
model.load(gmshPartition.simple('./input/' + mshName + '_tan.msh'))

# Coordinate system
XYZ = function.getCoordinates();
lonLatDegrees = functionC(glib,"lonLatDegrees",3,[XYZ])
latLonDegrees = functionC(glib,"latLonDegrees",3,[XYZ])
latLon = functionC(glib,"latLon",3,[XYZ])
lonLat = functionC(glib,"lonLat",3,[XYZ])

# DG properties
dimension = 2
order = 1
groups = dgGroupCollection(model, dimension, order)


# Time
Ti = calendar.timegm([2000, 4, 1, 0, 0, 0]) # initial time
Tf = calendar.timegm([2000, 4, 6, 0, 0, 0]) # final time
t = Ti																			# current time
timeFunction = functionConstant(t)
dt = 900																		# time step

# Conservation law
claw = dgConservationLawShallowWater2d()
solution = dgDofContainer(groups, claw.getNbFields())

# bathymetry
bath = dgDofContainer(groups, 1)
def interpBath ():
  #bathF = slimFunctionETOPO1("./input/etopo1_bed_g_i2.bin", lonLatDegrees, 5, 10000)
  #bathF = slimFunctionBathyScheldtDelft("./input/bathyScheldtDelftGimpSaeftingeP50cm.bin", lonLatDegrees, 5, 200)
  bathF = slimFunctionBathyScheldtDelftAndETOPO1("./input/bathyScheldtDelftGimpSaeftingeP50cm.bin", "./input/etopo1_bed_g_i2.bin", lonLatDegrees, 5, 10000)
  bath.interpolate(bathF)
  bath.scatter()
interpBath()
claw.setBathymetry(bath.getFunction())
claw.setBathymetryGradient(bath.getFunctionGradient())

# wind
windInterpolatorU = slimStructDataInterpolatorMultilinear()
windInterpolatorV = slimStructDataInterpolatorMultilinear()
windUcontainer = slimStructDataContainerNetcdf("./input/uwnd_1998-2012.nc", "uwnd", "lat", "lon")
windVcontainer = slimStructDataContainerNetcdf("./input/vwnd_1998-2012.nc", "vwnd", "lat", "lon")
windU = slimFunctionStructData(windUcontainer, windInterpolatorU, latLonDegrees)
windV = slimFunctionStructData(windVcontainer, windInterpolatorV, latLonDegrees)
windU.setTimeFunction(timeFunction)
windV.setTimeFunction(timeFunction)
windUV = functionC(glib, "latLonVector", 3, [latLon, windV, windU])
wind = functionC(glib, "windStress", 2, [windUV, solution.getFunction(), bath.getFunction()])
claw.setSource(wind)

# Coriolis
Coriolis = functionC(glib, "coriolis", 1, [lonLatDegrees])
claw.setCoriolisFactor(Coriolis)

# Bottom Stress
Cd = functionC(glib, "bottomDrag", 1, [function.getSolution(), bath.getFunction()])
claw.setQuadraticDissipation(Cd)

# Diffusion
Di = functionC(glib, "smagorinsky", 1, [function.getSolutionGradient()])
claw.setDiffusivity(Di)

# TPXO tides
tideEta = slimFunctionTpxo("./input/h_tpxo7.2.nc", "ha", "hp", "lon_z", "lat_z")
tideEta.setCoordAndTimeFunctions(lonLatDegrees, timeFunction)
tideU = slimFunctionTpxo("./input/u_tpxo7.2.nc", "Ua", "up", "lon_u", "lat_u")
tideU.setCoordAndTimeFunctions(lonLatDegrees, timeFunction)
tideV = slimFunctionTpxo("./input/u_tpxo7.2.nc", "Va", "vp", "lon_v", "lat_v")
tideV.setCoordAndTimeFunctions(lonLatDegrees, timeFunction)
tideUV = functionC(glib, "latLonVector", 3, [latLon, tideV, tideU])
tideuv = functionC(glib, "transport2velocity", 3, [tideUV, bath.getFunction(), function.getSolution()])
tpxoTide = functionC(glib, "merge", 3, [tideEta,tideuv])

# boundary conditions
claw.addBoundaryCondition('Shelf Break', claw.newOutsideValueBoundary("Surface",  tpxoTide))
claw.addBoundaryCondition('Ghent-Terneuzen Canal', claw.newBoundaryWall())
claw.addBoundaryCondition('Hansweert Canal', claw.newBoundaryWall())
claw.addBoundaryCondition('Zandvlietsluis North', claw.newBoundaryWall())
claw.addBoundaryCondition('Zandvlietsluis South', claw.newBoundaryWall())
claw.addBoundaryCondition('BoudewijnSluis North', claw.newBoundaryWall())
claw.addBoundaryCondition('BoudewijnSluis South', claw.newBoundaryWall())
claw.addBoundaryCondition('Kallo Sluis', claw.newBoundaryWall())
claw.addBoundaryCondition('Upstream', claw.newBoundaryWall())
claw.addBoundaryCondition('Meuse', claw.newBoundaryWall())
claw.addBoundaryCondition('Rhine', claw.newBoundaryWall())
claw.addBoundaryCondition('Thames', claw.newBoundaryWall())
claw.addBoundaryCondition('Seine', claw.newBoundaryWall())
claw.addBoundaryCondition('Baltic', claw.newBoundaryWall())
claw.addBoundaryCondition('Bath Canal', claw.newBoundaryWall())
claw.addBoundaryCondition('coast', claw.newBoundaryWall())
claw.addBoundaryCondition('Coast', claw.newBoundaryWall())

# outputs
outputDir = 'output/scheldt/'
if(Msg.GetCommRank() == 0):
  if(os.path.exists(outputDir)):
    try : os.system("rm -r " + outputDir);
    except: 0;
exporterSol=dgIdxExporter(solution, outputDir)
bath.exportMsh(outputDir + "bath", 0, 1)

# implicit solver
linsys = linearSystemPETScBlockDouble()
# linsys.setParameter("petscOptions", "-ksp_monitor -ksp_rtol 1e-3 -ksp_max_it 20")
dof = dgDofManager.newDGBlock(groups, 3, linsys)
solver = dgDIRK(claw, dof, 2)
solver.getNewton().setVerb(1000)
solver.getNewton().setRtol(1e-6)
nb = int((Tf-Ti)/dt)
startcpu = time.clock()
for i in range(0,nb):
  norm = solver.iterate(solution, dt, t)
  t = t+dt
  timeFunction.set(t)
  if ( i%1  == 0 ):
    exporterSol.exportIdx(i, t)
    if Msg.GetCommRank() == 0:
      print((t-Ti)/3600., time.clock() - startcpu)
endcpu=time.clock()