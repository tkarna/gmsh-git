%module "linearSystem"

%import(module="dgpy.dgCommon") "dgCommon.i"

%{
#undef HAVE_DLOPEN
#include "dgBlockMatrix.h"
#include "GmshConfig.h"
#include "linearSystem.h"
#include "dgDofManager.h"
#include "dgLinearSystemBJacobi.h"
#include "dgLinearSystemMassMatrix.h"
#include "dgLinearSystemExtrusion.h"
%}

%include "dgBlockMatrix.h"
%include "GmshConfig.h"
%include "linearSystem.h"
%include "dgDofManager.h"
%template(LinearSystemFullMatrixDouble) linearSystem<fullMatrix<double> >;
%include "dgLinearSystemBJacobi.h"
%include "dgLinearSystemMassMatrix.h"
%include "dgLinearSystemExtrusion.h"
