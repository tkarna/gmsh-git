#ifndef _DG_BLOCK_MATRIX_H
#define _DG_BLOCK_MATRIX_H

#include <vector>
#include <map>

#include "linearSystem.h"
#include "GmshMessage.h"
class dgDofContainer;
class dgGroupCollection;
template<class t>
class dgFullMatrix;
template<class t>
class fullMatrix;

class dgBlockMatrix : public linearSystem<fullMatrix<double> > {
  std::vector<std::map<size_t, std::vector<size_t> > > _nonEmptyRows;
  std::vector<std::map<size_t, std::vector<size_t> > > _nonEmptyCols;
  std::vector<std::map<size_t, fullMatrix<double> > > _matrix;
  std::vector<size_t> _dof2Group;
  std::vector<size_t> _dof2Element;
  const dgGroupCollection *_groups;
  size_t _firstBlock,_nbFields, _nbSplit;
  bool _isAllocated;
 public:
  dgBlockMatrix(int nbFIelds,const dgGroupCollection *groups);
  void addToMatrix(int row, int col, const fullMatrix<double> &val);
  void zeroMatrix();
  //y=A*x
  void mult(const dgDofContainer &x, dgDofContainer &y);
  //A=invM*A
  void invMmult();
  //Removes zero lines and columns in blocks
  void reduce(bool split=false, double epsilon=0.0);
  void setIdentity();
  void print(); 
  void getFromMatrix(int _row, int _col, fullMatrix<double> &val) const { Msg::Error("getFromMatrix not implemented in dgBlockMatrix");}
  void addToRightHandSide(int _row, const fullMatrix<double> &val) { Msg::Error("addToRightHandSide not implemented in dgBlockMatrix");}
  void getFromRightHandSide(int _row, fullMatrix<double> &val) const { Msg::Error("getFromRightHandSide not implemented in dgBlockMatrix");}
  void getFromSolution(int _row, fullMatrix<double> &val) const { Msg::Error("getFromSolution not implemented in dgBlockMatrix");}
  void addToSolution(int _row, const fullMatrix<double> &val) { Msg::Error("addToSolution not implemented in dgBlockMatrix");}
  virtual bool isAllocated() const { return _isAllocated;}
  virtual void allocate(int nbRows) { _isAllocated=true; }
  virtual void clear() { Msg::Error("clear not implemented in dgBlockMatrix");}
  virtual void zeroRightHandSide() { Msg::Error("zeroRightHandSide not implemented in dgBlockMatrix");}
  virtual void zeroSolution() { Msg::Error("zeroSolution not implemented in dgBlockMatrix");}
  virtual int systemSolve() { Msg::Error("systemSolve not implemented in dgBlockMatrix"); return -1;}
  virtual double normInfRightHandSide() const { Msg::Error("normInfRightHandSide not implemented in dgBlockMatrix"); return -1;}

};

#endif
