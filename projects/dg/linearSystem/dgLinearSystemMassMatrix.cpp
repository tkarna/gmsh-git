#include "dgLinearSystemMassMatrix.h"
#include "dgGroupOfElements.h"
#include "dgFullMatrix.h"
#include "dgDofContainer.h"
#include "dgMPI.h"

dgLinearSystemMassMatrix::dgLinearSystemMassMatrix(const dgGroupCollection &groups, bool sameMass):
  _sameMass(sameMass),
  _groups(groups)
{
  _solved = false;
  _isAllocated = false;
}

bool dgLinearSystemMassMatrix::_getId(int row, int &groupId, int &elId) const
{
  row -= _firstBlock;
  if (row < 0 || (size_t) row >= _dof2Group.size())
    return false;
  groupId = _dof2Group[row];
  elId = _dof2Element[row];
  return true;
}

void dgLinearSystemMassMatrix::allocate(int nbRows)
{
  if (_isAllocated)
    Msg::Fatal("system already allocated");
  _matrix.resize(_groups.getNbElementGroups());
  _nField = strtol (_parameters["nField"].c_str(), NULL, 10);
  if (_nField == 0)
    Msg::Error ("'nField' parameters must be set for linearSystemMassMatrix");
  int nMassField = _sameMass ? 1 : _nField;
  if(nMassField > 1) Msg::Fatal("dgLinearSystemMassMatrix not tested for multi mass fields");
  int nLocalElement = 0;
  for (int i = 0; i < _groups.getNbElementGroups(); ++i) {
    const dgGroupOfElements &group = *_groups.getElementGroup(i);
    _matrix[i].resize(group.getNbNodes(), group.getNbNodes(), group.getNbElements() * nMassField, true);
    nLocalElement += group.getNbElements();
  }
  _dof2Group.resize(nLocalElement);
  _dof2Element.resize(nLocalElement);
  dgMPI::sumPrev(nLocalElement, _firstBlock);
  nLocalElement = 0;
  for (int i = 0; i < _groups.getNbElementGroups(); ++i) {
    const dgGroupOfElements &group = *_groups.getElementGroup(i);
    for (size_t j = 0; j < group.getNbElements(); ++j) {
      _dof2Group[j + nLocalElement] = i;
      _dof2Element[j + nLocalElement] = j;
    }
    nLocalElement += group.getNbElements();
  }
  _rhs = new dgDofContainer(_groups, _nField);
  _solution = new dgDofContainer(_groups, _nField);
  _isAllocated = true;
}

void dgLinearSystemMassMatrix::addToMatrix(int row, int col, const fullMatrix<double> &val)
{
  if (_solved)
    Msg::Fatal("zero this matrix before adding to it");
  int iGroup, iElement;
  if(row != col)
    Msg::Error("row != col");
  if (row != col || !_getId(row, iGroup, iElement))
    return;
  int nNode = _groups.getElementGroup(iGroup)->getNbNodes();
  fullMatrix<double> m;
  int nMassField = _matrix[iGroup].size2() / (nNode * _groups.getElementGroup(iGroup)->getNbElements());
  for (int iField = 0; iField < nMassField; ++iField)
  {
    _matrix[iGroup].getBlockProxy(iElement * nMassField + iField, m);
    int bs = iField * nNode;
    for (int i = 0; i < nNode; ++i) {
      for (int j = 0; j < nNode; ++j) {
        m(i, j) += val(bs + i, bs + j);
      }
    }
  }
}

void dgLinearSystemMassMatrix::zeroMatrix()
{
  for (size_t i = 0; i < _matrix.size(); ++i) {
    _matrix[i].setAll(0.);
  }
  _solved = false;
}

void dgLinearSystemMassMatrix::getFromMatrix(int row, int col, fullMatrix<double> &val) const
{
  Msg::Fatal("not implemented");
}

void dgLinearSystemMassMatrix::addToRightHandSide(int row, const fullMatrix<double> &val)
{
  int iGroup, iElement;
  if (!_getId(row, iGroup, iElement))
    return;
  fullMatrix<double> &r = _rhs->getGroupProxy(iGroup);
  int nNode = _groups.getElementGroup(iGroup)->getNbNodes();
  for (int j = 0; j < _nField; ++j) {
    for (int i = 0; i < nNode; ++i) {
      r(i, _nField * iElement + j) += val(j * nNode + i, 0);
    }
  }
}

void dgLinearSystemMassMatrix::getFromRightHandSide(int row, fullMatrix<double> &val) const
{
  int iGroup, iElement;
  if (!_getId(row, iGroup, iElement))
    return;
  const fullMatrix<double> &r = _rhs->getGroupProxy(iGroup);
  int nNode = _groups.getElementGroup(iGroup)->getNbNodes();
  for (int j = 0; j < _nField; ++j) {
    for (int i = 0; i < nNode; ++i) {
       val(j * nNode + i, 0) = r(i, _nField * iElement + j);
    }
  }
}

void dgLinearSystemMassMatrix::getFromSolution(int row, fullMatrix<double> &val) const
{
  int iGroup, iElement;
  if (!_getId(row, iGroup, iElement))
    return;
  const fullMatrix<double> &r = _solution->getGroupProxy(iGroup);
  int nNode = _groups.getElementGroup(iGroup)->getNbNodes();
  for (int j = 0; j < _nField; ++j) {
    for (int i = 0; i < nNode; ++i) {
       val(j * nNode + i, 0) = r(i, _nField * iElement + j);
    }
  }
}

void dgLinearSystemMassMatrix::addToSolution(int row, const fullMatrix<double> &val)
{
  int iGroup, iElement;
  if (!_getId(row, iGroup, iElement))
    return;
  fullMatrix<double> &r = _solution->getGroupProxy(iGroup);
  int nNode = _groups.getElementGroup(iGroup)->getNbNodes();
  for (int j = 0; j < _nField; ++j) {
    for (int i = 0; i < nNode; ++i) {
      r(i, _nField * iElement + j) += val(j * nNode + i, 0);
    }
  }
}

bool dgLinearSystemMassMatrix::isAllocated() const { return _isAllocated;}

void dgLinearSystemMassMatrix::clear()
{
  if (_isAllocated) {
    delete _rhs;
    delete _solution;
    _matrix.clear();
    _isAllocated = false;
  }
}

void dgLinearSystemMassMatrix::zeroRightHandSide()
{
  _rhs->setAll(0.);
}

void dgLinearSystemMassMatrix::zeroSolution()
{
  _solution->setAll(0.);
}

int dgLinearSystemMassMatrix::systemSolve()
{
  fullMatrix<double> m;
  if (!_solved) {
    for (size_t i = 0; i < _matrix.size(); ++i) {
      size_t nBlock = _matrix[i].size2() / _matrix[i].getBlockSize();
      for (size_t j = 0; j < nBlock; ++j) {
        _matrix[i].getBlockProxy(j, m);
        m.invertInPlace();
      }
    }
    _solved = true;
  }
  if (_sameMass) {
    fullMatrix<double> m;
    fullMatrix<double> r, s;
    for (size_t i = 0; i < _matrix.size(); ++i) {
      size_t nBlock = _matrix[i].size2() / _matrix[i].getBlockSize();
      for (size_t j = 0; j < nBlock; ++j) {
        _matrix[i].getBlockProxy(j, m);
        _rhs->getGroupProxy(i).getBlockProxy(j, r);
        _solution->getGroupProxy(i).getBlockProxy(j, s);
        m.mult(r, s);
      }
    }
  }
  else {
    fullMatrix<double> m;
    for (size_t i = 0; i < _matrix.size(); ++i) {
      size_t nBlock = _matrix[i].size2() / _matrix[i].getBlockSize();
      for (size_t j = 0; j < nBlock; ++j) {
        _matrix[i].getBlockProxy(j, m);
        fullMatrix<double> r(_rhs->getGroupProxy(i), j, 1); //TODO not 1 -> _nbFields ?
        fullMatrix<double> s(_solution->getGroupProxy(i), j, 1);
        m.mult(r, s);
      }
    }
  }
  return 0;
}

double dgLinearSystemMassMatrix::normInfRightHandSide() const
{
  return _rhs->norm0();
}

dgLinearSystemMassMatrix::~dgLinearSystemMassMatrix()
{
  clear();
}
