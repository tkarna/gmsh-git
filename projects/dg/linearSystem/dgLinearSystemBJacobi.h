#ifndef _DG_LINEAR_SYSTEM_PETSC_MIXED_H_
#define _DG_LINEAR_SYSTEM_PETSC_MIXED_H_

#include "GmshConfig.h"
#include <vector>
#include <map>

#if defined(HAVE_PETSC)
#include "linearSystemPETSc.h"
class dgGroupCollection;
class dgDofContainer;
class dgConservationLaw;
template<class t>
class dgFullMatrix;
template<class t>
class fullMatrix;

class dgLinearSystemMatrixFreeBJacobi : public linearSystem<fullMatrix<double> > {
  std::vector<dgFullMatrix<double> > _blocks;
  bool _inverted;
  dgGroupCollection *_groups;
  const dgDofContainer *_xRef;
  dgDofContainer *_residualRef;
  dgConservationLaw *_law;
  double _alpha, _t, _epsilon;
  double _aTol, _rTol;
  int _maxIt;
  KSP _ksp;
  Vec _b, _x;
  Mat _a;
  bool _isAllocated, _kspAllocated;
  std::vector<int> _dof2Group;
  std::vector<int> _dof2Element;
  std::vector<int> _groupStart;
  int _nbRows, _firstBlock;
  bool _matrixFree;
  std::vector<std::map<int, fullMatrix<double> > > _matrix;
  public:
  void allocate(int nbRows);
  void clear();
  inline dgGroupCollection *getGroups() const {return _groups;}
  inline dgConservationLaw *getLaw() const {return _law;}
  void addToMatrix(int row, int col, const fullMatrix<double> &val);
  void addToRightHandSide(int row, const fullMatrix<double> &val);
  void getFromRightHandSide(int row, fullMatrix<double> &val) const;
  void getFromSolution(int row, fullMatrix<double> &val) const;
  void zeroMatrix();
  void zeroSolution(); 
  void addToSolution(int _row, const fullMatrix<double> &val);
  void zeroRightHandSide();
  void getFromMatrix(int row, int col, fullMatrix<double> &val) const
  {
    Msg::Fatal("getFromMatrix not implemented for PETSc");
  }
  virtual double normInfRightHandSide() const;
  int systemSolve();
  void setRef(const dgDofContainer  *xRef,const dgDofContainer *residualRef, double alpha);
  int mult(const dgDofContainer &x, dgDofContainer &y);
  int precondition(const dgDofContainer &X, dgDofContainer &Y);
  void setTolerance(double rtol, double atol, int maxit);
  void setEpsilon(double epsilon);
  dgLinearSystemMatrixFreeBJacobi (dgConservationLaw *law, dgGroupCollection *groups, bool matrixFree = true);
  ~dgLinearSystemMatrixFreeBJacobi();
  bool isAllocated() const { return _isAllocated; }
};

#endif
#endif
