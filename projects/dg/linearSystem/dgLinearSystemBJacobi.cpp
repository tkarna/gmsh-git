#include "GmshConfig.h"

#if defined(HAVE_PETSC)
#include "dgLinearSystemBJacobi.h"
#include "dgConservationLaw.h"
#include "dgGroupOfElements.h"
#include "dgDofContainer.h"
#include "fullMatrix.h"
#include "petsc.h"

double dgLinearSystemMatrixFreeBJacobi::normInfRightHandSide() const
{
  PetscReal nor;
  VecNorm(_b, NORM_INFINITY, &nor);
  return nor;
}

dgLinearSystemMatrixFreeBJacobi::dgLinearSystemMatrixFreeBJacobi (dgConservationLaw *law, dgGroupCollection *groups, bool matrixFree)
{
  _law = law;
  _matrixFree = matrixFree;
  _groups = groups;
  _residualRef = new dgDofContainer (*groups, law->getNbFields());
  _t = 0;
  _aTol = 1e-8;
  _rTol = 1e-3;
  _maxIt = 50;
  _epsilon = 1e-8;
  _isAllocated = false;
  _kspAllocated = false;
  //create a dof2DG
  int groupStart = 0;
  for (int i = 0; i < groups->getNbElementGroups() + groups->getNbGhostGroups(); i++) {
    dgGroupOfElements *group = groups->getElementGroup(i);
    _groupStart.push_back(groupStart);
    groupStart += group->getNbElements() * group->getNbNodes() * _law->getNbFields();
    _dof2Group.reserve(_dof2Group.size() + group->getNbElements());
    _dof2Element.reserve(_dof2Element.size() + group->getNbElements());
    for (size_t j = 0; j < group->getNbElements(); j++) {
      _dof2Group.push_back(i);
      _dof2Element.push_back(j);
    }
  }
  if (!_matrixFree) {
    int nbElements = 0;
    for (int i = 0; i < groups->getNbElementGroups(); i++) {
      nbElements += groups->getElementGroup(i)->getNbElements();
    }
    _matrix.resize(nbElements);
  }
  _blocks.resize(_groups->getNbElementGroups());
  _nbRows = 0;
  int nbBlocks = 0;
  for (size_t i = 0; i < _blocks.size(); i++) {
    dgGroupOfElements *group = _groups->getElementGroup(i);
    int blockSize = group->getNbNodes() * _law->getNbFields();
    _blocks[i].resize (blockSize, blockSize, group->getNbElements(), true);
    _nbRows += blockSize * group->getNbElements();
    nbBlocks += group->getNbElements();
  }
  MPI_Status status;
  if (Msg::GetCommRank() == 0)
    _firstBlock = 0;
  else
    MPI_Recv (&_firstBlock, 1, MPI_INT, Msg::GetCommRank()-1, 0, MPI_COMM_WORLD, &status);
  int lastBlock = _firstBlock + nbBlocks;
  if (Msg::GetCommRank() != Msg::GetCommSize()-1)
    MPI_Send (&lastBlock, 1, MPI_INT, Msg::GetCommRank()+1, 0, MPI_COMM_WORLD);
}
void dgLinearSystemMatrixFreeBJacobi::setTolerance(double rtol, double atol, int maxit)
{
  _maxIt = maxit;
  _aTol = atol;
  _rTol = rtol;
  if (_kspAllocated)
    KSPSetTolerances(_ksp, _rTol, _aTol, 1e8, _maxIt);
}
void dgLinearSystemMatrixFreeBJacobi::setEpsilon(double epsilon)
{
  _epsilon = epsilon;
}

void dgLinearSystemMatrixFreeBJacobi::setRef (const dgDofContainer *x,const dgDofContainer *res, double alpha) {
  _xRef = x;
  _residualRef->copy(*res);
  _alpha = alpha;
}

void dgLinearSystemMatrixFreeBJacobi::addToMatrix(int row, int col, const fullMatrix<double> &val)
{
  if (!_matrixFree) {
    fullMatrix<double> &entry = _matrix[row - _firstBlock][col];
    if (entry.size1() == 0) {
      entry = val;
    } else {
      entry.axpy (val);
    }
  }
  if (row != col) return;
  if (_inverted)
    Msg::Fatal("cannot add to the system before resetting the matrix");
  dgFullMatrix<double> &blockGroup = _blocks[_dof2Group[row - _firstBlock]];
  fullMatrix<double> block; 
  blockGroup.getBlockProxy(_dof2Element[row - _firstBlock], block);
  block.axpy(val);
}

void dgLinearSystemMatrixFreeBJacobi::getFromRightHandSide(int row, fullMatrix<double> &val) const
{
  row -= _firstBlock;
  int iGroup = _dof2Group[row];
  int iElement = _dof2Element[row];
  int blockSize = _blocks[iGroup].size1();
  int blockStart = iElement * blockSize + _groupStart[iGroup];
  PetscScalar *bv;
  VecGetArray(_b, &bv);
  for (int i = 0; i < blockSize; i++) {
    int ii = blockStart + i;
    #ifdef PETSC_USE_COMPLEX
    val(i,0) = bv[ii].real();
    #else
    val(i,0) = bv[ii];
    #endif
  }
  VecRestoreArray(_b, &bv);
}

void dgLinearSystemMatrixFreeBJacobi::getFromSolution(int row, fullMatrix<double> &val) const {
  row -= _firstBlock;
  int iGroup = _dof2Group[row];
  int iElement = _dof2Element[row];
  int blockSize = _blocks[iGroup].size1();
  int blockStart = iElement * blockSize + _groupStart[iGroup];
  PetscScalar *xv;
  VecGetArray(_x, &xv);
  for (int i = 0; i < blockSize; i++) {
    int ii = blockStart + i;
    #ifdef PETSC_USE_COMPLEX
    val(i,0) = xv[ii].real();
    #else
    val(i,0) = xv[ii];
    #endif
  }
  VecRestoreArray(_x, &xv);
}

void dgLinearSystemMatrixFreeBJacobi::addToRightHandSide(int row, const fullMatrix<double> &val)
{
  row -= _firstBlock;
  int iGroup = _dof2Group[row];
  int iElement = _dof2Element[row];
  int blockSize = _blocks[iGroup].size1();
  int blockStart = iElement * blockSize + _groupStart[iGroup];
  PetscScalar *bv;
  VecGetArray(_b, &bv);
  for (int i = 0; i < blockSize; i++) {
    int ii = blockStart + i;
    #ifdef PETSC_USE_COMPLEX
    bv[ii].real() +=  val(i,0);
    #else
    bv[ii] += val(i,0);
    #endif
  }
  VecRestoreArray(_b, &bv);
}

void dgLinearSystemMatrixFreeBJacobi::zeroRightHandSide()
{
  if (_isAllocated) {
    VecAssemblyBegin(_b);
    VecAssemblyEnd(_b);
    VecZeroEntries(_b);
  }
}

void dgLinearSystemMatrixFreeBJacobi::zeroSolution()
{
  if (_isAllocated) {
    VecAssemblyBegin(_x);
    VecAssemblyEnd(_x);
    VecZeroEntries(_x);
  }
} 
void dgLinearSystemMatrixFreeBJacobi::addToSolution(int row, const fullMatrix<double> &val)
{
  row -= _firstBlock;
  int iGroup = _dof2Group[row];
  int iElement = _dof2Element[row];
  int blockSize = _blocks[iGroup].size1();
  int blockStart = iElement * blockSize + _groupStart[iGroup];
  PetscScalar *bv;
  VecGetArray(_x, &bv);
  for (int i = 0; i < blockSize; i++) {
    int ii = blockStart + i;
    #ifdef PETSC_USE_COMPLEX
    bv[ii].real() =  val(i,0);
    #else
    bv[ii] =  val(i,0);
    #endif
  }
  VecRestoreArray(_x, &bv);
}

void dgLinearSystemMatrixFreeBJacobi::zeroMatrix()
{
  _inverted = false;
  for (size_t i = 0; i < _blocks.size(); i++)
    _blocks[i].setAll(0.);
  if (!_matrixFree) {
    for (size_t i = 0; i < _matrix.size(); i++) {
      for (std::map<int, fullMatrix<double> >::iterator it = _matrix[i].begin(); it != _matrix[i].end(); ++it) {
        it->second.setAll(0.);
      }
    }
  }
}

// y = A x
int dgLinearSystemMatrixFreeBJacobi::mult(const dgDofContainer &x, dgDofContainer &y)
{
  if (_matrixFree) {
    double epsilon = _epsilon / (x.norm() / x.norm0());
    //y = (residualRef -_residual.compute(xRef + epsilon x))/epsilon + alpha M x;
    dgDofContainer tmpX(x), tmpRes(x);
    tmpX.copy (*_xRef);
    tmpX.axpy (x, epsilon);
    tmpRes.setAll(0.);
    _law->computeAllTerms(_t, tmpX, tmpRes);
    y.setAll(0.);
    tmpRes.axpy(*_residualRef, -1);
    y.axpy (tmpRes, -1./epsilon);
    tmpX.copy(x);
    tmpX.multiplyByMassMatrix();
    y.axpy (tmpX, _alpha);
  } else {
    y.setAll(0.);
    fullMatrix<double> px, py;
    for (size_t i = 0; i < _matrix.size(); ++i) {
      for (std::map<int, fullMatrix<double> >::iterator it = _matrix[i].begin(); it != _matrix[i].end(); ++it) {
        int j = it->first;
        const fullMatrix<double> &m = it->second;
        y.getGroupProxy(_dof2Group[i]).getBlockProxy(_dof2Element[i], py);
        x.getGroupProxy(_dof2Group[j]).getBlockProxy(_dof2Element[j], px);
        px.reshape(px.size1() * px.size2(), 1);
        py.reshape(py.size1() * py.size2(), 1);
        py.gemm(m, px);
      }
    }
  }
  return 0;
}

int dgLinearSystemMatrixFreeBJacobi::precondition(const dgDofContainer &X, dgDofContainer &Y)
{
  fullMatrix<double> p, x, y, xS, yS;
  for (int iGroup = 0; iGroup < _groups->getNbElementGroups(); iGroup++) {
    dgGroupOfElements *group = _groups->getElementGroup(iGroup);
    const dgFullMatrix<double> &blockGroup = _blocks[iGroup];
    const dgFullMatrix<double> &xGroup = X.getGroupProxy(iGroup);
    const dgFullMatrix<double> &yGroup = Y.getGroupProxy(iGroup);
    for (size_t iElement = 0; iElement < group->getNbElements(); iElement ++) {
      xGroup.getBlockProxy(iElement, x);
      yGroup.getBlockProxy(iElement, y);
      blockGroup.getBlockProxy(iElement, p);
      x.reshape(blockGroup.size1(), 1);
      y.reshape(blockGroup.size1(), 1);
      if (!_inverted)
        p.invertInPlace();
      p.mult(x, y);
    }
  }
  _inverted = true;
  return 0;
}

#if (PETSC_VERSION_MAJOR==3 && PETSC_VERSION_MINOR==0)
static PetscErrorCode pcCallback(void *_sys, Vec x, Vec y)
{
  dgLinearSystemMatrixFreeBJacobi *sys = (dgLinearSystemMatrixFreeBJacobi*) _sys;
#else
static PetscErrorCode pcCallback(PC pc, Vec x, Vec y)
{
  dgLinearSystemMatrixFreeBJacobi *sys;
  PCShellGetContext (pc, (void**) &sys);
#endif
  dgDofContainer dofX (*sys->getGroups(), sys->getLaw()->getNbFields());
  dgDofContainer dofY (*sys->getGroups(), sys->getLaw()->getNbFields());
  PetscScalar *X, *Y;
  VecGetArray(x, &X);
  VecGetArray(y, &Y);
  for (int i = 0; i < dofX.getVector().size(); i++) {
    #ifdef PETSC_USE_COMPLEX
    dofX.getVector()(i) = X[i].real();
    #else
    dofX.getVector()(i) = X[i];
    #endif
  }
  PetscErrorCode r = sys->precondition(dofX, dofY);
  for (int i = 0; i < dofY.getVector().size(); i++) {
    #ifdef PETSC_USE_COMPLEX
    Y[i].real() = dofY.getVector()(i);
    #else
    Y[i] = dofY.getVector()(i);
    #endif
  }
  VecRestoreArray(x, &X);
  VecRestoreArray(y, &Y);
  return r;
}

static PetscErrorCode multCallback(Mat A, Vec x, Vec y)
{
  dgLinearSystemMatrixFreeBJacobi *sys;
  MatShellGetContext (A, (void**) &sys);
  dgDofContainer dofX (*sys->getGroups(), sys->getLaw()->getNbFields());
  dgDofContainer dofY (*sys->getGroups(), sys->getLaw()->getNbFields());
  PetscScalar *X, *Y;
  VecGetArray(x, &X);
  VecGetArray(y, &Y);
  for (int i = 0; i < dofX.getVector().size(); i++) {
    #ifdef PETSC_USE_COMPLEX
    dofX.getVector()(i) = X[i].real();
    #else
    dofX.getVector()(i) = X[i];
    #endif
  }
  PetscErrorCode r = sys->mult(dofX, dofY);
  for (int i = 0; i < dofY.getVector().size(); i++) {
    #ifdef PETSC_USE_COMPLEX
    Y[i].real() = dofY.getVector()(i);
    #else
    Y[i] = dofY.getVector()(i);
    #endif
  }
  VecRestoreArray(x, &X);
  VecRestoreArray(y, &Y);
  return r;
}

int dgLinearSystemMatrixFreeBJacobi::systemSolve()
{
  if (!_kspAllocated){
    KSPCreate(PETSC_COMM_WORLD, &_ksp);
    KSPSetType(_ksp, KSPGMRES);
    KSPSetOperators(_ksp, _a, _a, DIFFERENT_NONZERO_PATTERN);
    KSPSetFromOptions(_ksp);
    KSPSetTolerances(_ksp, _rTol, _aTol, 1e8, _maxIt);
    PC pc;
    KSPGetPC(_ksp, &pc);
    PCSetType(pc, PCSHELL);
    PCShellSetApply(pc, pcCallback);
    PCShellSetContext(pc, this);
    _kspAllocated= true;
  }
  VecAssemblyBegin(_b);
  VecAssemblyEnd(_b);
  KSPSolve(_ksp, _b, _x);
  return 1;
}

void dgLinearSystemMatrixFreeBJacobi::allocate(int nbRows)
{
  clear();
  MatCreateShell(PETSC_COMM_WORLD, _nbRows, _nbRows, PETSC_DETERMINE, PETSC_DETERMINE, this, &_a);
  MatShellSetOperation(_a, MATOP_MULT, (void(*)()) multCallback);
  VecCreate(PETSC_COMM_WORLD, &_x);
  VecSetSizes(_x, _nbRows, PETSC_DETERMINE);
  VecSetFromOptions(_x);
  VecDuplicate(_x, &_b);
  _isAllocated = true;
}

void dgLinearSystemMatrixFreeBJacobi::clear()
{
  if(_isAllocated) {		      
    MatDestroy(&_a);
    VecDestroy(&_x);
    VecDestroy(&_b);
  }
  _isAllocated = false;
}

dgLinearSystemMatrixFreeBJacobi::~dgLinearSystemMatrixFreeBJacobi()
{
  clear();
  if (_kspAllocated) {
    KSPDestroy(&_ksp);
  }
}

#endif
