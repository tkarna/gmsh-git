#ifndef _DG_LINEAR_SYSTEM_MASS_MATRIX_H
#define _DG_LINEAR_SYSTEM_MASS_MATRIX_H

#include <vector>
#include <map>

#include "linearSystem.h"
#include "GmshMessage.h"
class dgDofContainer;
class dgGroupCollection;
template<class t>
class dgFullMatrix;
template<class t>
class fullMatrix;

class dgLinearSystemMassMatrix : public linearSystem<fullMatrix<double> > {
  std::vector<dgFullMatrix<double> > _matrix;
  std::vector<int> _dof2Group;
  std::vector<int> _dof2Element;
  int _nField;
  bool _sameMass;
  bool _solved;
  const dgGroupCollection &_groups;
  dgDofContainer *_rhs, *_solution;
  int _firstBlock;
  bool _isAllocated;
  bool _getId(int row, int &groupId, int &elId) const;
 public:
  dgLinearSystemMassMatrix(const dgGroupCollection &groups, bool sameMass);
  void addToMatrix(int row, int col, const fullMatrix<double> &val);
  void zeroMatrix();
  void getFromMatrix(int _row, int _col, fullMatrix<double> &val) const;
  void addToRightHandSide(int _row, const fullMatrix<double> &val);
  void getFromRightHandSide(int _row, fullMatrix<double> &val) const;
  void getFromSolution(int _row, fullMatrix<double> &val) const;
  void addToSolution(int _row, const fullMatrix<double> &val);
  bool isAllocated() const;
  void allocate(int nbRows);
  void clear();
  void zeroRightHandSide();
  void zeroSolution();
  int systemSolve();
  double normInfRightHandSide() const;
  ~dgLinearSystemMassMatrix();
};

#endif
