#include "dgLinearSystemExtrusion.h"

#include "dgGroupOfElements.h"
#include "dgConservationLaw.h"
#include "dgExtrusion.h"
#include "linearSystemPETSc.h"
#include <sstream>

#if defined(HAVE_PETSC)
#include "mpi.h"

dgLinearSystemExtrusion::dgLinearSystemExtrusion(dgConservationLaw* law, dgGroupCollection* groups, dgExtrusion* columns)
{
  _groups = groups;
  _columns = columns;
  _law = law;
  //create a dof2DG
  int groupStart = 0;
  for (int i = 0; i < groups->getNbElementGroups(); i++) {
    dgGroupOfElements *group = groups->getElementGroup(i);
    _groupStart.push_back(groupStart);
    groupStart += group->getNbElements() * group->getNbNodes() * _law->getNbFields();
    _dof2Group.reserve(_dof2Group.size() + group->getNbElements());
    _dof2Element.reserve(_dof2Element.size() + group->getNbElements());
    for (size_t j = 0; j < group->getNbElements(); j++) {
      _dof2Group.push_back(i);
      _dof2Element.push_back(j);
    }
  }
  _nbDofs = _dof2Element.size();
  _procDofStartIndex = 0;
#if defined(HAVE_MPI)
  int nbProcs = Msg::GetCommSize();
  if ( nbProcs > 1 ) {
    int rank = Msg::GetCommRank();
    std::vector<int> nbDofsPerProcToSend(nbProcs);
    nbDofsPerProcToSend[rank] = _nbDofs;
    std::vector<int> nbDofsPerProc(nbProcs);
    int err = MPI_Allreduce ( &nbDofsPerProcToSend[0], &nbDofsPerProc[0], nbProcs, 
                    MPI_INT, MPI_MAX, MPI_COMM_WORLD );
    if (err)
      Msg::Error("MPI allreduce failed");
    for (int i = 0; i < rank; i++)
      _procDofStartIndex += nbDofsPerProc[i];
  }
#endif
  _isAllocated = false;
}

void dgLinearSystemExtrusion::allocate(int nbRows)
{
  // nbRows ingnored
  int nbCols = _columns->getNbColumns();
  _linSystems.resize(nbCols,linearSystemPETSc<fullMatrix<double> >(PETSC_COMM_SELF));
  std::ostringstream bsize;
  bsize<<_law->getNbFields()*_groups->getElementGroup(0)->getNbNodes();
  for ( int i = 0; i < nbCols; i++ ) {
//     printf("blockSize %d nbRows %d\n",_law->getNbFields()*_groups->getElementGroup(0)->getNbNodes(),_columns->getNbElemsInColumn(i));
    _linSystems[i].setParameter ("blockSize", bsize.str());
    _linSystems[i].setParameter ("sequentialMatrix", "true");
//     _linSystems[i].setParameter ("petscOptions","-pc_type ilu");
    _linSystems[i].allocate(_columns->getNbElemsInColumn(i));
  }
  _isAllocated = true;
  Msg::Barrier();
}

void dgLinearSystemExtrusion::addToRightHandSide(int row, const fullMatrix< double >& val)
{
  size_t iGroup = _globalDof2Group(row);
  size_t iElement = _globalDof2Elem(row);
  size_t iColumn,iLayer;
  _columns->mapElementID2ColumnPosition(iGroup,iElement,iColumn,iLayer);
  _linSystems[iColumn].addToRightHandSide(iLayer,val);
}

void dgLinearSystemExtrusion::addToMatrix(int row, int col, const fullMatrix< double >& val)
{
  size_t iColumn,iLayerRow;
  _columns->mapElementID2ColumnPosition(_globalDof2Group(row),_globalDof2Elem(row),iColumn,iLayerRow);
  size_t iColumn2,iLayerCol;
  _columns->mapElementID2ColumnPosition(_globalDof2Group(col),_globalDof2Elem(col),iColumn2,iLayerCol);
  if (iColumn != iColumn2) {
//     Msg::Fatal("Trying to add an element between two different linear systems: %d %d", iColumn, iColumn2);
    return;
  }
  _linSystems[iColumn].addToMatrix(iLayerRow,iLayerCol,val);
}

void dgLinearSystemExtrusion::insertInSparsityPattern(int row, int col)
{
  if (row < _procDofStartIndex || row > _procDofStartIndex + _nbDofs ||
      col < _procDofStartIndex || col > _procDofStartIndex + _nbDofs)
    return;
  size_t iColumn,iLayerRow;
  _columns->mapElementID2ColumnPosition(_globalDof2Group(row),_globalDof2Elem(row),iColumn,iLayerRow);
  size_t iColumn2,iLayerCol;
  _columns->mapElementID2ColumnPosition(_globalDof2Group(col),_globalDof2Elem(col),iColumn2,iLayerCol);
  if (iColumn != iColumn2) {
//     printf("%d: Ignored sp pattern: %d %d (%d %d)\n",Msg::GetCommRank(), iColumn, iColumn2,row,col);
    return;
  }
  _linSystems[iColumn].insertInSparsityPattern(iLayerRow,iLayerCol);
}

void dgLinearSystemExtrusion::getFromRightHandSide(int row, fullMatrix< double >& val) const
{
  size_t iColumn,iLayer;
  _columns->mapElementID2ColumnPosition(_globalDof2Group(row),_globalDof2Elem(row),iColumn,iLayer);
  _linSystems[iColumn].getFromRightHandSide(iLayer,val);
}

void dgLinearSystemExtrusion::getFromSolution(int row, fullMatrix< double >& val) const
{
  size_t iColumn,iLayer;
  _columns->mapElementID2ColumnPosition(_globalDof2Group(row),_globalDof2Elem(row),iColumn,iLayer);
  _linSystems[iColumn].getFromSolution(iLayer,val);
}

void dgLinearSystemExtrusion::zeroSolution()
{
  for (size_t iCol = 0; iCol < _linSystems.size(); iCol++)
    _linSystems[iCol].zeroSolution();

}

void dgLinearSystemExtrusion::addToSolution(int row, const fullMatrix< double >& val)
{
  size_t iColumn,iLayer;
  _columns->mapElementID2ColumnPosition(_globalDof2Group(row),_globalDof2Elem(row),iColumn,iLayer);
  _linSystems[iColumn].addToSolution(iLayer,val);

}


void dgLinearSystemExtrusion::getFromMatrix(int row, int col, fullMatrix< double >& val) const
{
  Msg::Error("getFromMatrix not implemented for PETSc");
//   int iColumn,iLayer;
//   _columns->findElementColumnPosition(_globalDof2Group(row),_globalDof2Elem(row),iColumn,iLayer);
//   _linSystems[iColumn].getFromMatrix(iLayer,val);
}

void dgLinearSystemExtrusion::clear()
{
  for (size_t iCol = 0; iCol < _linSystems.size(); iCol++)
    _linSystems[iCol].clear();
}

int dgLinearSystemExtrusion::systemSolve()
{
  int output = 1;
  for (size_t iCol = 0; iCol < _linSystems.size(); iCol++){
    output = std::min( output, _linSystems[iCol].systemSolve() );
  }
  return output;
}

void dgLinearSystemExtrusion::zeroMatrix()
{
  for (size_t iCol = 0; iCol < _linSystems.size(); iCol++)
    _linSystems[iCol].zeroMatrix();
}

void dgLinearSystemExtrusion::zeroRightHandSide()
{
  for (size_t iCol = 0; iCol < _linSystems.size(); iCol++)
    _linSystems[iCol].zeroRightHandSide();
}

dgLinearSystemExtrusion::~dgLinearSystemExtrusion()
{
  _linSystems.clear(); // is this needed ?
}

double dgLinearSystemExtrusion::normInfRightHandSide() const
{
  double normInf = 0;
  for (size_t iCol = 0; iCol < _linSystems.size(); iCol++)
    normInf = std::max( normInf, _linSystems[iCol].normInfRightHandSide() );
  return normInf;
}
#endif
