#include "dgDofManager.h"
#include "dgGroupOfElements.h"
#include "MElement.h"
#include "dgConservationLaw.h"
#include "dgDofContainer.h"
#include "GModel.h"
#include "dgLinearSystemBJacobi.h"
#include "linearSystem.h"
#include "dgFullMatrix.h"
#include "GmshConfig.h"
#include "dgMPI.h"
#include "dgHOMesh.h"
#include "dgTerm.h"

dgDofManager::dgDofManager (const dgGroupCollection *groups) {
  _groups = groups;
  _nbFields = 0;
  _changedSolution = _nsupra = _const1changedsol = _const2changedsol = NULL;
}

double dgDofManager::getNormInfRHS() {
  return _lsysBase->normInfRightHandSide();
}

void dgDofManager::zeroRHS() {
  _lsysBase->zeroRightHandSide();
}

void dgDofManager::zeroMatrix() {
  _lsysBase->zeroMatrix();
}

void dgDofManager::setMatrixFreeParameters(const dgDofContainer &sol, const dgDofContainer &F, double alpha) {
#ifdef HAVE_PETSC
  dgLinearSystemMatrixFreeBJacobi *sysBJacobi = dynamic_cast<dgLinearSystemMatrixFreeBJacobi*>(_lsysBase);
  if (sysBJacobi) {
    sysBJacobi->setRef(&sol, &F, alpha);
  }
#endif
}

class dgDofManagerDG:public dgDofManager 
{
  linearSystem<double> *_sys;
  std::vector<int> _groupStart;
  int _firstBlock;
  std::vector<std::vector<int> > _ghostId;
  
public:
  
  void solve()
  {
    _lsysBase->systemSolve();
  }
  
  int getId(int iGroup, size_t iElement, int iNode, int iField)
  {
    int nNode = _groups->getElementGroup(iGroup)->getNbNodes();
    if (iGroup < _groups->getNbElementGroups()) {
      return _groupStart[iGroup] + (iElement * _nbFields + iField) * nNode + iNode;
    }
    else {
      return _ghostId[iGroup - _groups->getNbElementGroups()][iElement] + iField * nNode + iNode;
    }
  }
  
  dgDofManagerDG(const dgGroupCollection *groups, int nbFields, linearSystem<double > *lsys) :
    dgDofManager (groups)
  {
    _lsysBase = lsys;
    _sys = lsys;
    _nbFields = nbFields;
    char tmp[256];
    sprintf(tmp,"%d",nbFields);
    lsys->setParameter ("nField", tmp);
    size_t nDof = 0;
    _groupStart.resize(groups->getNbElementGroups());
    for (int iGroup = 0; iGroup < groups->getNbElementGroups(); iGroup++) {
      _groupStart[iGroup] = nDof;
      nDof += groups->getElementGroup(iGroup)->getNbElements() * groups->getElementGroup(iGroup)->getNbNodes() * _nbFields;
    }
    dgMPI::sumPrev(nDof, _firstBlock);
    for (size_t i = 0; i < _groupStart.size(); ++i) {
      _groupStart[i] += _firstBlock;
    }
    std::map<int, std::vector<int> >ghostToSend, ghostToRec;
    for (int i = 0; i < Msg::GetCommSize(); ++i) {
      if (groups->getNbImageElementsOnPartition(i, false) > 0) {
        std::vector<int> &toSend = ghostToSend[i];
        toSend.resize(groups->getNbImageElementsOnPartition(i, false));
        for (int j = 0; j < groups->getNbImageElementsOnPartition(i, false); ++j) {
          int gId = groups->getImageElementGroup(i, j, false);
          int eId = groups->getImageElementPositionInGroup(i, j , false);
          int nNode = groups->getElementGroup(gId)->getNbNodes();
          toSend[j] = _groupStart[gId] +  nNode * _nbFields * eId;
        }
      }
    }
    dgMPI::allAllV(ghostToSend, ghostToRec, true);
    _ghostId.resize(groups->getNbGhostGroups());
    for (int i = 0; i < groups->getNbGhostGroups(); ++i) {
      _ghostId[i].resize(groups->getGhostGroup(i)->getNbElements());
    }
    for (std::map<int, std::vector<int> >::const_iterator it = ghostToRec.begin(); it != ghostToRec.end(); ++it) {
      for (size_t j = 0; j < it->second.size(); ++j) {
        int iGroup = groups->getGhostElementGroup(it->first, j, false);
        int iEl = groups->getGhostElementPositionInGroup(it->first, j, false);
        _ghostId[iGroup - groups->getNbElementGroups()][iEl] = it->second[j];
      }
    }
    _sys->allocate(nDof);
  }
  
  void assembleRHSVector(int iGroup, size_t iElement, fullMatrix<double> &rhs)
  {
    for (int i = 0; i < rhs.size1(); ++i)
      for (int j = 0; j < rhs.size2(); ++j)
	_sys->addToRightHandSide(getId(iGroup, iElement, i, j), rhs(i, j));
  }
  
  void assembleLHSMatrix(int iGroup0, size_t iElement0, int iGroup1, size_t iElement1, fullMatrix<double> &lhs)
  {
    int nNodeI = _groups->getElementGroup(iGroup0)->getNbNodes();
    int nNodeJ = _groups->getElementGroup(iGroup1)->getNbNodes();
    for (int iNode = 0; iNode < nNodeI; ++iNode)
      for (int iField = 0; iField < _nbFields; ++iField)
        for (int jNode = 0; jNode < nNodeJ; ++jNode)
          for (int jField = 0; jField < _nbFields; ++jField)
            _sys->addToMatrix(getId(iGroup0, iElement0, iNode, iField), getId(iGroup1, iElement1, jNode, jField), lhs(iField * nNodeI + iNode, jField * nNodeJ + jNode));
  }
  
  void insertInSparsityPattern(int iGroup0, size_t iElement0, int iGroup1, size_t iElement1)
  {
    int nNodeI = _groups->getElementGroup(iGroup0)->getNbNodes();
    int nNodeJ = _groups->getElementGroup(iGroup1)->getNbNodes();
    for (int iNode = 0; iNode < nNodeI; ++iNode)
      for (int iField = 0; iField < _nbFields; ++iField)
        for (int jNode = 0; jNode < nNodeJ; ++jNode)
          for (int jField = 0; jField < _nbFields; ++jField)
	    _sys->insertInSparsityPattern(getId(iGroup0, iElement0, iNode, iField), getId(iGroup1, iElement1, jNode, jField));
  }
  
  void getSolution(int iGroup, size_t iElement, fullMatrix<double> &sol)
  {
    for (int i = 0; i < sol.size1(); ++i)
      for (int j = 0; j < sol.size2(); ++j)
        _sys->getFromSolution(getId(iGroup, iElement, i, j), sol(i, j));
  }
  
  bool isContinuous () {return false;}
};

class dgDofManagerDGBlock:public dgDofManager 
{
  linearSystem<fullMatrix<double> > *_sys;
  std::vector<int> _groupStart;
  int _firstBlock;
  std::vector<std::vector<int> > _ghostId;
  
public:
  
  void solve() {
    _lsysBase->systemSolve();
  }
  
  int getId(int iGroup, size_t iElement) {
    if (iGroup < _groups->getNbElementGroups()) {
      return _groupStart[iGroup] + iElement;
    }
    else {
      return _ghostId[iGroup - _groups->getNbElementGroups()][iElement];
    }
  }
  
  dgDofManagerDGBlock (const dgGroupCollection *groups, int nbFields, linearSystem<fullMatrix<double> > *lsys) :
    dgDofManager (groups)
  {
    _lsysBase = lsys;
    _sys = lsys;
    _nbFields = nbFields;
    char tmp[256]; // don't use ostringstream here: gcc4.4 does not like it
    sprintf(tmp,"%d",nbFields*groups->getElementGroup(0)->getNbNodes());
    lsys->setParameter ("blockSize", tmp);
    sprintf(tmp,"%d",nbFields);
    lsys->setParameter ("nField", tmp);
    size_t nElement = 0;
    _groupStart.resize(groups->getNbElementGroups());
    for (int iGroup = 0; iGroup < groups->getNbElementGroups(); iGroup++) {
      _groupStart[iGroup] = nElement;
      nElement += groups->getElementGroup(iGroup)->getNbElements();
    }
    dgMPI::sumPrev(nElement, _firstBlock);
    for (size_t i = 0; i < _groupStart.size(); ++i) {
      _groupStart[i] += _firstBlock;
    }
    std::map<int, std::vector<int> >ghostToSend, ghostToRec;
    for (int i = 0; i < Msg::GetCommSize(); ++i) {
      if (groups->getNbImageElementsOnPartition(i, false) > 0) {
        std::vector<int> &toSend = ghostToSend[i];
        toSend.resize(groups->getNbImageElementsOnPartition(i, false));
        for (int j = 0; j < groups->getNbImageElementsOnPartition(i, false); ++j) {
          toSend[j] = _groupStart[groups->getImageElementGroup(i, j, false)] + groups->getImageElementPositionInGroup(i, j , false);
        }
      }
    }
    dgMPI::allAllV(ghostToSend, ghostToRec, true);
    _ghostId.resize(groups->getNbGhostGroups());
    for (int i = 0; i < groups->getNbGhostGroups(); ++i) {
      _ghostId[i].resize(groups->getGhostGroup(i)->getNbElements());
    }
    for (std::map<int, std::vector<int> >::const_iterator it = ghostToRec.begin(); it != ghostToRec.end(); ++it) {
      for (size_t j = 0; j < it->second.size(); ++j) {
        int iGroup = groups->getGhostElementGroup(it->first, j, false);
        int iEl = groups->getGhostElementPositionInGroup(it->first, j, false);
        _ghostId[iGroup - groups->getNbElementGroups()][iEl] = it->second[j];
      }
    }
    _sys->allocate(nElement);
  }
  
  void assembleRHSVector(int iGroup, size_t iElement, fullMatrix<double> &rhs)
  {
    _sys->addToRightHandSide(getId(iGroup, iElement), rhs);
  }
  
  void assembleLHSMatrix(int iGroup0, size_t iElement0, int iGroup1, size_t iElement1, fullMatrix<double> &lhs)
  {
    _sys->addToMatrix(getId(iGroup0, iElement0), getId(iGroup1, iElement1), lhs);
  }
  
  void insertInSparsityPattern(int iGroup0, size_t iElement0, int iGroup1, size_t iElement1)
  {
    _sys->insertInSparsityPattern(getId(iGroup0, iElement0), getId(iGroup1, iElement1));
  }
  
  void getSolution(int iGroup, size_t iElement, fullMatrix<double> &sol)
  {
    _sys->getFromSolution(getId(iGroup, iElement), sol);
  }
  
  bool isContinuous () {return false;}
};


void dgDofManager::fillSparsityPattern(bool hasInterfaceTerms, const dgConservationLaw *claw)
{
  for (int iGroup = 0; iGroup < _groups->getNbElementGroups(); iGroup++) {
    dgGroupOfElements *group = _groups->getElementGroup(iGroup);
    for (size_t iElement = 0 ; iElement < group->getNbElements(); iElement++) {
      insertInSparsityPattern(iGroup, iElement, iGroup, iElement);
    }
  }
  if (hasInterfaceTerms){
    for (int iGroup = 0; iGroup < _groups->getNbFaceGroups(); iGroup++) {
      dgGroupOfFaces *group = _groups->getFaceGroup(iGroup);
      if (claw == NULL || claw->haveFaceTerm(*group)) {
        for (int iConn = 0; iConn < group->nConnection(); iConn++) {
          int iGroup = _groups->getElementGroupId(&group->elementGroup(iConn));
          for (int jConn = 0; jConn < group->nConnection(); jConn++) {
            if (iConn == jConn) 
              continue;
            int jGroup = _groups->getElementGroupId(&group->elementGroup(jConn));
            for (size_t iFace = 0 ; iFace < group->size(); iFace++) {
              insertInSparsityPattern(iGroup, group->elementId(iFace, iConn), jGroup, group->elementId(iFace, jConn));
            }
          }
        }
      }
    } 
  }
  finalizeSparsity();
}


dgDofManager *dgDofManager::newDGBlock (const dgGroupCollection *groups, int nbFields, linearSystem<fullMatrix<double> > *lsys) 
{
  return new dgDofManagerDGBlock(groups, nbFields, lsys);
}

dgDofManager *dgDofManager::newDG (const dgGroupCollection *groups, int nbFields, linearSystem<double> *lsys) 
{
  return new dgDofManagerDG(groups, nbFields, lsys);
}

static double dzOnda_invbeta(double a, double b, double c, double n)
{ double z=0;
  if (n==1) z=1;
  else z= pow(a*a+b*b+c*c,(n-1)/2)+(n-1)*a*a*pow(a*a+b*b+c*c,(n-3)/2);
  return z;
}

void dgDofManager::reorderAndAssembleGroupJacobian(const dgGroupOfElements &group, const fullMatrix<double> &jacobianK) {
  if (_nsupra) {
    int nbNodes = group.getNbNodes();
    int iGroup = group.getGroupCollection()->getElementGroupId(&group);
    
    fullMatrix<double> jacobianE(nbNodes * _nbFields, nbNodes * _nbFields), jacobianKE;
    fullMatrix<double> solE(nbNodes, _nbFields), changedsolE(nbNodes, _nbFields), nsupraE(nbNodes, _nbFields);
    fullMatrix<double> const1changedsolE(nbNodes, _nbFields), const2changedsolE(nbNodes, _nbFields);
    solE.setAll(0);
    for (size_t iElement = 0 ; iElement < group.getNbElements(); iElement++) {
      jacobianKE.setAsProxy(jacobianK, iElement * _nbFields * _nbFields, _nbFields * _nbFields);
      _changedSolution->getGroupProxy(&group).getBlockProxy(iElement, changedsolE);
      _const1changedsol->getGroupProxy(&group).getBlockProxy(iElement, const1changedsolE);
      _const2changedsol->getGroupProxy(&group).getBlockProxy(iElement, const2changedsolE);
      _nsupra->getGroupProxy(&group).getBlockProxy(iElement, nsupraE);
      for (int k = 0; k < _nbFields; k++) for (int l = 0; l < _nbFields; l++) {
        for (int i = 0; i < nbNodes; i++) for (int j = 0; j < nbNodes; j++) {    
          jacobianE(k * nbNodes + i, l * nbNodes + j) = -jacobianKE(i * nbNodes + j, k * _nbFields + l);
          jacobianE(k * nbNodes + i, l * nbNodes + j) *= dzOnda_invbeta(changedsolE(j,l), const1changedsolE(j,l), const2changedsolE(j,l), nsupraE(j,l));
        }
      }
      assembleLHSMatrix(iGroup, iElement, iGroup, iElement, jacobianE);
    }
  }
  else {
    int iGroup = group.getGroupCollection()->getElementGroupId(&group);
    int nbNodes = group.getNbNodes();
    fullMatrix<double> jacobianE(nbNodes * _nbFields, nbNodes * _nbFields), jacobianKE;
    for (size_t iElement = 0 ; iElement < group.getNbElements(); iElement++) {
      jacobianKE.setAsProxy(jacobianK, iElement * _nbFields * _nbFields, _nbFields * _nbFields);
      for (int k = 0; k < _nbFields; k++) for (int l = 0; l < _nbFields; l++) {
        for (int i = 0; i < nbNodes; i++) for (int j = 0; j < nbNodes; j++) {
          jacobianE(k * nbNodes + i, l * nbNodes + j) = -jacobianKE(i * nbNodes + j, k * _nbFields + l);
        }
      }
      assembleLHSMatrix(iGroup, iElement, iGroup, iElement, jacobianE);
    }
  }
}

void dgDofManager::setSupra(dgDofContainer &solution, dgDofContainer &betaOfCurrentSolution, dgDofContainer &nSupra, dgDofContainer &const1Var_in_beta_law, dgDofContainer &const2Var_in_beta_law)
{ 
  _changedSolution = &solution;
  //_changedSolution = &betaOfCurrentSolution;
  _nsupra = &nSupra;
  _const1changedsol = &const1Var_in_beta_law;
  _const2changedsol = &const2Var_in_beta_law;
}

void dgDofManager::reorderAndAssembleFaceGroupJacobian(const dgGroupOfFaces &faces, const int nbFields, std::vector<fullMatrix<double> > &jacobianSol, std::vector<fullMatrix<double> > &jacobianGradSol) 
{
  bool haveSomething = false;
  int nbConnectionsInFlux = 0;
  int nbf2 = nbFields * nbFields;
  for (size_t i = 0; i < jacobianSol.size(); ++i){
    haveSomething |= (jacobianSol[i].size1() != 0);
    if (jacobianSol[i].size1() != 0) {
      nbConnectionsInFlux = jacobianSol[i].size2() / (faces.size() * nbf2);
    }
  }
  for (size_t i = 0; i < jacobianGradSol.size(); ++i) {
    haveSomething |= (jacobianGradSol[i].size1() != 0);
    if (jacobianGradSol[i].size1() != 0) {
      nbConnectionsInFlux = jacobianGradSol[i].size2() / (faces.size() * nbf2);
    }
  }
  if (!haveSomething)
    return;
  fullMatrix<double> jacobianKE;
  int nbConnections = faces.nConnection();
  int nbInterfaceNodes = faces.getNodalBasis()->points.size1();
  fullMatrix<double> jacobianE2;

  for (int d = 0; d < nbConnections; d++) {
    int nbNodesD = faces.elementGroup(d).getNbNodes(); // all the node of adjacent elmts influe on the gradient at interface
    int iGroupD = faces.elementGroup(d).getGroupCollection()->getElementGroupId(&faces.elementGroup(d));

    for (int c = 0; c < nbConnectionsInFlux; c++) {
      bool assembleL = (faces.elementGroup(c).getGhostPartition() < 0);
      bool assembleR = (nbConnections ==2 && nbConnectionsInFlux == 1 && faces.elementGroup(1).getGhostPartition() < 0);
      if (! assembleL && ! assembleR)
        continue;
      int iGroupC = faces.elementGroup(c).getGroupCollection()->getElementGroupId(&faces.elementGroup(c));
      int iGroup1 = -1;
      if (assembleR)
        iGroup1 = faces.elementGroup(1).getGroupCollection()->getElementGroupId(&faces.elementGroup(1));
      int nbNodesC = faces.elementGroup(c).getNbNodes();
      fullMatrix<double> jacobianE(nbNodesC * nbFields, nbNodesD * nbFields);

      for (size_t iFace = 0 ; iFace < faces.size(); ++iFace) {
        const std::vector<int> &closureC = faces.closure(iFace, c);
        jacobianE.setAll(0);

        if (jacobianGradSol[d].size1() != 0) {
          jacobianKE.setAsProxy(jacobianGradSol[d], (iFace * nbConnectionsInFlux + c) * nbf2, nbf2);
          const std::vector<int> &closureDFull = faces.fullClosure(iFace, d);
          for (int l = 0; l < nbFields; l++) {
            for (int k = 0; k < nbFields; k++) {
              for (int j = 0; j < nbNodesD; j++) {
                for (int i = 0; i < nbInterfaceNodes; i++) {
                  jacobianE (k * nbNodesC + closureC[i], l * nbNodesD + closureDFull[j]) -= jacobianKE(i * nbNodesD + j, k * nbFields + l);
                }
              }
            }
          }
        }
        if (jacobianSol[d].size1() != 0) {
          int ibloc = iFace * nbConnectionsInFlux + c;
          jacobianKE.setAsProxy(jacobianSol[d], ibloc * nbf2, nbf2);
          const std::vector<int> &closureD = faces.closure(iFace, d);
          for (int l = 0; l < nbFields; l++) {
            for (int k = 0; k < nbFields; k++) {
              for (int j = 0; j < nbInterfaceNodes; j++) {
                for (int i = 0; i < nbInterfaceNodes; i++) {
                  jacobianE(k * nbNodesC + closureC[i], l * nbNodesD + closureD[j]) -= jacobianKE(i * nbInterfaceNodes + j, k * nbFields + l);
                }
              }
            }
          }
        }
        if (assembleL)
          assembleLHSMatrix(iGroupC, faces.elementId(iFace, c), iGroupD, faces.elementId(iFace, d), jacobianE);
        if (assembleR) {
          int nbNodes1 = faces.elementGroup(1).getNbNodes();
          jacobianE2.resize(nbFields * nbNodes1, nbFields * nbNodes1);
          jacobianE2.setAll(0.);
          const std::vector<int> &closure1 = faces.closure(iFace, 1);
          for (int l = 0; l < nbFields * nbNodesD; l++) for (int k=0; k<nbFields;k++)  for(int i=0; i<nbInterfaceNodes; i++) {
          	jacobianE2 (k * nbNodes1 + closure1[i], l) = -jacobianE(k * nbNodesC + closureC[i], l);
          }
          assembleLHSMatrix(iGroup1, faces.elementId(iFace, 1), iGroupD, faces.elementId(iFace, d), jacobianE2);
        }
      }
    }
  }
}


class dgDofManagerCG:public dgDofManager 
{
  std::map<int, std::set<std::pair<int, int> > > _ghostSparsity;
  std::map<int, std::vector<double> > _send, _recv;

  dgHOMesh _mesh;
  std::vector<int> _fixedId;
  std::vector<double> _fixedValues;
  std::vector<double> _ghostValues;
  linearSystem<double> *_sys;
  void _scatter() {
    for(std::map<int, std::vector<int> >::const_iterator it = _mesh.scatterSendId().begin(); it != _mesh.scatterSendId().end(); ++it) {
      std::vector<double> &send = _send[it->first];
      send.resize(it->second.size() * _nbFields);
      for (size_t i = 0; i < it->second.size(); ++i)
        for (int iField = 0; iField < _nbFields; ++iField)
          _sys->getFromSolution(_mesh.vertexGlobalId(it->second[i]) * _nbFields + iField, send[i * _nbFields + iField]);
    }
    dgMPI::allAllV(_send, _recv, true);
    _ghostValues.resize(_mesh.nGhostVertices() * _nbFields);
    std::map<int, std::vector<double> >::iterator itv = _recv.begin();
    for (std::map<int, std::vector<int> >::const_iterator it = _mesh.scatterReceiveId().begin(); it != _mesh.scatterReceiveId().end(); ++it) {
      for (size_t i = 0; i < it->second.size(); ++i) {
        for (int j = 0; j < _nbFields; ++j) {
          _ghostValues[(it->second[i] - _mesh.nVertices()) * _nbFields + j] = itv->second[i * _nbFields + j];
        }
      }
      itv ++;
    }
  }
  
  void solve() {
    for (int i = 0; i < _mesh.nVertices(); ++i) {
      for (int j = 0; j < _nbFields; ++j) {
        if (_fixedId[i * _nbFields + j] >= 0) {
          int gid = _mesh.vertexGlobalId(i) * _nbFields + j;
          _sys->addToRightHandSide(gid, _fixedValues[_fixedId[i * _nbFields + j]]);
          _sys->addToMatrix(gid, gid, 1.);
        }
      }
    }
    _lsysBase->systemSolve();
    _scatter();
  }
  
public:
  dgDofManagerCG (const dgGroupCollection *groups,int nbFields, linearSystem<double> *lsys):
    dgDofManager (groups), _mesh(*groups)
  {
    _lsysBase = lsys;
    _sys = lsys;
    _nbFields = nbFields;
    _sys->allocate(_mesh.nVertices() * _nbFields);
    _fixedId.resize((_mesh.nVertices() + _mesh.nGhostVertices()) * _nbFields, -1);
  }
  
  void fixStrongBC (int vertexId, int fieldId, double value)
  {
    int id = vertexId * _nbFields + fieldId;
    if (_fixedId[id] < 0) {
      _fixedId[id] = _fixedValues.size();
      _fixedValues.push_back(value);
    }
  }

  void clearStrongBC()
  {
    _fixedValues.resize(0);
    _fixedId.clear();
    _fixedId.resize((_mesh.nVertices() + _mesh.nGhostVertices()) * _nbFields, -1);
  }
  const dgHOMesh *getHOMesh() const
  {
    return &_mesh;
  }

  void assembleRHSVector(int iGroup, size_t iElement, fullMatrix<double> &rhs) 
  {
    const std::vector<int> &vertices = _mesh.vertices(iGroup, iElement);
    for (size_t i = 0; i < vertices.size(); ++i) {
      int gid = _mesh.vertexGlobalId(vertices[i]);
      for (int j = 0; j < _nbFields; ++j) {
        if (_fixedId[vertices[i] * _nbFields + j] < 0)
          _sys->addToRightHandSide(gid * _nbFields + j, rhs(i, j));
      }
    }
  }
  void assembleLHSMatrix(int iGroup0, size_t iElement0, int iGroup1, size_t iElement1, fullMatrix<double> &lhs)
  {
    const std::vector<int> &vertices0 = _mesh.vertices(iGroup0, iElement0);
    const std::vector<int> &vertices1 = _mesh.vertices(iGroup1, iElement1);
    for (size_t i0 = 0; i0 < vertices0.size(); ++i0) {
      int gid0 = _mesh.vertexGlobalId(vertices0[i0]);
      for (size_t i1 = 0; i1 < vertices1.size(); ++i1) {
        int gid1 = _mesh.vertexGlobalId(vertices1[i1]);
        for (int j0 = 0; j0 < _nbFields; ++j0) {
          if (_fixedId[vertices0[i0] * _nbFields + j0] >= 0)
            continue;
          for (int j1 = 0; j1 < _nbFields; ++j1) {
            _sys->addToMatrix(gid0 * _nbFields + j0, gid1 * _nbFields + j1, lhs(j0 * vertices0.size() + i0, j1 * vertices1.size() + i1));
          }
        }
      }
    }
  }
  
  void insertInSparsityPattern(int iGroup0, size_t iElement0, int iGroup1, size_t iElement1)
  {
    const std::vector<int> &vertices0 = _mesh.vertices(iGroup0, iElement0);
    const std::vector<int> &vertices1 = _mesh.vertices(iGroup1, iElement1);
    for (size_t i0 = 0; i0 < vertices0.size(); ++i0) {
      int gid0 = _mesh.vertexGlobalId(vertices0[i0]);
      int vPartition = _mesh.vertexPartition(vertices0[i0]);
      if (vPartition != Msg::GetCommRank()) {
        for (size_t i1 = 0; i1 < vertices1.size(); ++i1) {
          int gid1 = _mesh.vertexGlobalId(vertices1[i1]);
          for (int j0 = 0; j0 < _nbFields; ++j0) {
            if (_fixedId[vertices0[i0] * _nbFields + j0] >= 0){
              _ghostSparsity[vPartition].insert(std::make_pair(gid0 * _nbFields + j0, gid0 * _nbFields + j0));
            } else {
              for (int j1 = 0; j1 < _nbFields; ++j1) {
                _ghostSparsity[vPartition].insert(std::make_pair(gid0 * _nbFields + j0, gid1 * _nbFields + j1));
              }
            }
          }
        }
      }
      else {
        for (size_t i1 = 0; i1 < vertices1.size(); ++i1) {
          int gid1 = _mesh.vertexGlobalId(vertices1[i1]);
          for (int j0 = 0; j0 < _nbFields; ++j0) {
            if (_fixedId[vertices0[i0] * _nbFields + j0] >= 0){
              _sys->insertInSparsityPattern(gid0 * _nbFields + j0, gid0 * _nbFields + j0);
            }
            else {
              for (int j1 = 0; j1 < _nbFields; ++j1) {
                _sys->insertInSparsityPattern(gid0 * _nbFields + j0, gid1 * _nbFields + j1);
              }
            }
          }
        }
      }
    }
  }
  void finalizeSparsity()
  {
    std::map<int, std::vector<int> > send, recv;
    for (std::map<int, std::set<std::pair<int, int> > >::iterator it = _ghostSparsity.begin(); it != _ghostSparsity.end(); ++it)  {
      std::vector<int> &isend = send[it->first];
      isend.reserve(it->second.size() * 2);
      for(std::set<std::pair<int, int> > ::iterator it2 = it->second.begin(); it2 != it->second.end(); ++it2) {
        isend.push_back(it2->first);
        isend.push_back(it2->second);
      }
    }
    dgMPI::allAllV(send, recv, true);
    for (std::map<int, std::vector<int> >::iterator it = recv.begin(); it != recv.end(); ++it) {
      std::vector<int> &irecv = it->second;
      for (size_t i = 0; i < irecv.size() / 2; ++i) {
        _sys->insertInSparsityPattern(irecv[i * 2], irecv[i * 2 + 1]);
      }
    }
  }
  
  void getSolution(int iGroup, size_t iElement, fullMatrix<double> &sol) {
    const std::vector<int> &vertices = _mesh.vertices(iGroup, iElement);
    for (size_t i = 0; i < vertices.size(); ++i) {
      int gid = _mesh.vertexGlobalId(vertices[i]);
      for (int j = 0; j < _nbFields; ++j) {
        if (vertices[i] < _mesh.nVertices())
          _sys->getFromSolution(gid * _nbFields + j, sol(j * vertices.size() + i, 0));
        else 
          sol(j * vertices.size() + i, 0) = _ghostValues[(vertices[i] - _mesh.nVertices()) * _nbFields + j];
      }
    }
  }
  
  bool isContinuous () {return true;}
};

dgDofManager *dgDofManager::newCG (const dgGroupCollection *groups, int nbFields, linearSystem<double> *lsys) 
{
  return new dgDofManagerCG (groups, nbFields, lsys);
}
