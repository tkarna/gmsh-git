#include "GmshConfig.h"

#if defined(HAVE_PETSC)
#include "petsc.h"
#include "linearSystemPETSc.h"
static void  _try(int ierr) 
{
  CHKERRABORT(PETSC_COMM_WORLD, ierr);
}
class dgLinearSystemPETScShellDemo : public linearSystemPETSc<double>{

  class mixedShell{
    public:
  };
  private:
  mixedShell _shell;
  void _kspCreate()
  {
    //  needed because both mother and daughter classes are template !!!
    _try(KSPCreate(PETSC_COMM_WORLD, &_ksp));
    _try(KSPSetType(_ksp, KSPGMRES));
    _try(KSPSetFromOptions(_ksp));
    PC pc;
    _try(KSPGetPC(_ksp, &pc));
    _try(PCSetType(pc, PCNONE));//SHELL));
    //    _try(PCShellSetApply(pc,mixedShellPCapply)); // preconditioning function (PCSetTYPE PCSHELL)
    _try(PCShellSetContext(pc,&_shell)); // object we can retrieve in the shell function
    _kspAllocated = true;
  }

  public:
  virtual void allocate(int nbRows)
  {
    clear();
    _try(MatCreateShell(PETSC_COMM_WORLD, nbRows, nbRows, nbRows, nbRows, &_shell, &_a));
    _try(MatShellSetOperation(_a, MATOP_MULT, (void(*)()) mixedShellMult));
    _try(MatShellSetOperation(_a, MATOP_MULT_ADD, (void(*)()) mixedShellMultAdd));
    _try(VecCreate(PETSC_COMM_WORLD, &_x));
    _try(VecSetSizes(_x, PETSC_DECIDE, nbRows));
    _try(VecSetFromOptions(_x));
    _try(VecDuplicate(_x, &_b));
    _isAllocated = true;
  }

  virtual void clear()
  {
    if(_isAllocated) {		      
      linearSystemPETSc<double>::clear();
    }
    _isAllocated = false;
  }
  virtual void addToMatrix(int row, int col, const double &val);
  virtual void zeroMatrix();
  static PetscErrorCode mixedShellMult(Mat mat,Vec x,Vec y);
  static PetscErrorCode mixedShellMultAdd(Mat mat,Vec x,Vec y,Vec z);
};


void dgLinearSystemPETScShellDemo::addToMatrix(int row, int col, const double &val)
{
}

void dgLinearSystemPETScShellDemo::zeroMatrix()
{
}

PetscErrorCode dgLinearSystemPETScShellDemo::mixedShellMult(Mat A,Vec x,Vec y){ // y = A x
  mixedShell *shell;
  _try(MatShellGetContext (A,(void**) &shell));		// should I use _try here ?
  _try(VecCopy(x,y));
  return 0;
}

PetscErrorCode dgLinearSystemPETScShellDemo::mixedShellMultAdd(Mat A,Vec x,Vec y,Vec z) { // z = y + A x
  // Get shell
  mixedShell *shell;
  _try(MatShellGetContext (A,(void**) &shell));
  _try(VecCopy(y, z));
  _try(VecAXPY(z, 1.0, x));
  return 0;
}
#endif // HAVE_PETSC
