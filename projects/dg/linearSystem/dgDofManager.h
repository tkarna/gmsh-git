#ifndef _DG_DOFMANAGER_H_
#define _DG_DOFMANAGER_H_

#include<vector>
#include<cstdlib>
class dgGroupCollection;
class dgGroupOfElements;
class dgGroupOfFaces;
template <class t> class fullMatrix;
template <class t> class dgFullMatrix;
class linearSystemBase;
template <class t> class linearSystem;
class dgConservationLaw;
class dgDofContainer;
class dgHOMesh;

/** A class to map degrees of freedom from a FEformulation (ie by element) to a linear system.
 * 3 implementations are available :
 *  CG (continuous Galerkin), 
 *  DG (discontinuous Galerkin) 
 *  and DGBlock (discontinuous Galerkin using block linear system)*/
class dgDofManager {
  protected:
  const dgGroupCollection *_groups;
  linearSystemBase *_lsysBase;
  int _nbFields;
  dgDofManager (const dgGroupCollection *groups);
  dgDofContainer *_changedSolution, *_const1changedsol, *_const2changedsol, *_nsupra;
  virtual void finalizeSparsity() {};
  public:  

  void fillSparsityPattern(bool hasInterfaceTerms, const dgConservationLaw *claw = NULL);
  //pure virtual
  virtual void assembleRHSVector (int iGroup, size_t iElement, fullMatrix<double> &rhs)=0;
  virtual void assembleLHSMatrix (int iGroup0, size_t iElement0, int iGroup1, size_t iElement1, fullMatrix<double> &lhs)=0;
  virtual void getSolution (int iGroup, size_t iElement, fullMatrix<double> &sol)=0;
  virtual void insertInSparsityPattern(int iGroup0, size_t iElement0, int iGroup1, size_t iElement1) = 0;
  virtual bool isContinuous() = 0;
  //relevant only in CG
  virtual void clearStrongBC() {};
  virtual const dgHOMesh *getHOMesh() const {return NULL;}
  virtual void fixStrongBC (int vertexId, int fieldId, double value) {};

  inline const dgGroupCollection &getGroups() const {return *_groups;}

  // all access from the non linear solver to the linearSystem are done through the dof manager
  virtual void solve() = 0;
  void zeroRHS();
  void zeroMatrix();
  double getNormInfRHS();
  void setMatrixFreeParameters(const dgDofContainer &sol, const dgDofContainer &F, double alpha);

  inline int getNbFields() { return _nbFields; }

  /** nbNodes nbNodes X nbFields nbFields => nbNodes nbFields x nbNodes nbFields */
  void setSupra(dgDofContainer &solution, dgDofContainer &betaOfCurrentSolution, dgDofContainer &nSupra, dgDofContainer &const1Var_in_beta_law, dgDofContainer &const2Var_in_beta_law);
  //void setSupra(dgDofContainer &betaOfCurrentSolution, dgDofContainer &nSupra, dgDofContainer &const1Var_in_beta_law, dgDofContainer &const2Var_in_beta_law);
  void reorderAndAssembleGroupJacobian(const dgGroupOfElements &group, const fullMatrix<double> &jacobianK);
  void reorderAndAssembleFaceGroupJacobian(const dgGroupOfFaces &faces, const int nbFields, std::vector<fullMatrix<double> >&jacobianSol, std::vector<fullMatrix<double> > &jacobianGradSol);
  /**Constructor for the block Discontinuous Galerkin formulation */
  static dgDofManager * newDGBlock(const dgGroupCollection*, int nbFields, linearSystem<fullMatrix<double> > *_lsys);
  /**Constructor for the Discontinuous Galerkin formulation */
  static dgDofManager * newDG(const dgGroupCollection*, int nbFields, linearSystem<double> *_lsys);
  /**Constructor for the Continuous Galerkin formulation with weak boundary conditions */
  static dgDofManager * newCG(const dgGroupCollection*, int nbFields, linearSystem<double> *_lsys);
  virtual ~dgDofManager(){}
};

#endif
