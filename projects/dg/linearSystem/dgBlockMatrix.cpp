#include "GmshConfig.h"
#ifdef HAVE_MPI
#include "mpi.h"
#endif

#include <stdlib.h>
#include <sstream>  

#include "dgBlockMatrix.h"
#include "dgGroupOfElements.h"
#include "dgDofContainer.h"


dgBlockMatrix::dgBlockMatrix(int nbFields, const dgGroupCollection *groups):
  _groups(groups),_nbFields(nbFields),_nbSplit(1),_isAllocated(false){
  for (int i = 0; i < groups->getNbElementGroups() + groups->getNbGhostGroups(); i++) {
    dgGroupOfElements *group = groups->getElementGroup(i);
    _dof2Group.reserve(_dof2Group.size() + group->getNbElements());
    _dof2Element.reserve(_dof2Element.size() + group->getNbElements());
    for (size_t j = 0; j < group->getNbElements(); j++) {
      _dof2Group.push_back(i);
      _dof2Element.push_back(j);
    }
  }
  int nbBlocks = 0;
  for (int i = 0; i < _groups->getNbElementGroups(); i++) {
    nbBlocks += _groups->getElementGroup(i)->getNbElements();
  }
  _matrix.resize(nbBlocks);
  #ifdef HAVE_MPI
  MPI_Status status;
  if (Msg::GetCommRank() == 0)
    _firstBlock = 0;
  else
    MPI_Recv (&_firstBlock, 1, MPI_INT, Msg::GetCommRank()-1, 0, MPI_COMM_WORLD, &status);
  int lastBlock = _firstBlock + nbBlocks;
  if (Msg::GetCommRank() != Msg::GetCommSize()-1)
    MPI_Send (&lastBlock, 1, MPI_INT, Msg::GetCommRank()+1, 0, MPI_COMM_WORLD);
  #else
    _firstBlock = 0;
  #endif
}

void dgBlockMatrix::addToMatrix(int row, int col, const fullMatrix<double> &val)
{
  if (_nonEmptyRows.size()!=0){
    Msg::Fatal("Cannot apply addToMatrix to a reduced matrix");
  }

  fullMatrix<double> &entry = _matrix[row - _firstBlock][col];
  if (entry.size1() == 0) {
    entry = val;
  } else {
    entry.axpy (val);
  }
}

void dgBlockMatrix::zeroMatrix()
{
 if (_nonEmptyRows.size()!=0){
    Msg::Fatal("Cannot apply zeroMatrix to a reduced matrix");
  }
 for (size_t i = 0; i < _matrix.size(); i++) {
    for (std::map<size_t, fullMatrix<double> >::iterator it = _matrix[i].begin(); it != _matrix[i].end(); ++it) {
      it->second.setAll(0.);
    }
  }
}

void dgBlockMatrix::setIdentity()
{
  if (_nonEmptyRows.size()!=0){
    Msg::Fatal("Cannot apply setIdentity to a reduced matrix");
  }
  for (size_t i = 0; i < _matrix.size(); i++) {
    for (std::map<size_t, fullMatrix<double> >::iterator it = _matrix[i].begin(); it != _matrix[i].end(); ++it) {
      it->second.setAll(0.);
      if (it->first==i){
	for (int d=0; d<it->second.size1(); d++)
	  it->second.set(d,d,1);
      }
    }
  }
}

void dgBlockMatrix::print()
{
  for (size_t i = 0; i < _matrix.size(); i++) {
    int iSplit=0;
    for (std::map<size_t, fullMatrix<double> >::iterator it = _matrix[i].begin(); it != _matrix[i].end(); ++it) {
      std::stringstream blockName;
      blockName << "Block " << i << ", Split " << iSplit << "\n"; 
      it->second.print(blockName.str().c_str());
      iSplit++;
    }
  }
}

// y = A x
void dgBlockMatrix::mult(const dgDofContainer &x, dgDofContainer &y)
{
  y.setAll(0.);
  fullMatrix<double> px, py, pxRed, pyRed;
  std::vector<std::map<size_t, fullMatrix<double> >::iterator> mSplitIt(_nbSplit);
  std::vector<std::map<size_t, std::vector<size_t> >::iterator> nonEmptyRowsIt(_nbSplit);
  std::vector<std::map<size_t, std::vector<size_t> >::iterator> nonEmptyColsIt(_nbSplit);
  for (size_t i = 0; i < _matrix.size()/_nbSplit; ++i) {
    if (_nonEmptyRows.size()!=0){
      for (size_t iSplit=0; iSplit<_nbSplit; iSplit++){
        mSplitIt[iSplit]=_matrix[_matrix.size()/_nbSplit*iSplit+i].begin();
        nonEmptyRowsIt[iSplit]=_nonEmptyRows[_matrix.size()/_nbSplit*iSplit+i].begin();
        nonEmptyColsIt[iSplit]=_nonEmptyCols[_matrix.size()/_nbSplit*iSplit+i].begin();
      }
    }
    for (std::map<size_t, fullMatrix<double> >::iterator it = _matrix[i].begin(); it != _matrix[i].end(); ++it) {
      int j = it->first;
      y.getGroupProxy(_dof2Group[i]).getBlockProxy(_dof2Element[i], py);
      x.getGroupProxy(_dof2Group[j]).getBlockProxy(_dof2Element[j], px);
      px.reshape(px.size1() * px.size2(), 1);
      py.reshape(py.size1() * py.size2(), 1);
      if (_nonEmptyRows.size()!=0){
	size_t nbNodesRow=py.size1()/_nbFields;
	for (size_t iSplit=0; iSplit<_nbSplit; iSplit++){
	  const fullMatrix<double> &m = mSplitIt[iSplit]++->second;
	  std::vector<size_t> &nonEmptyRowsBlock=nonEmptyRowsIt[iSplit]++->second;
	  std::vector<size_t> &nonEmptyColsBlock=nonEmptyColsIt[iSplit]++->second;
	  if (m.size1()>0){
	    pxRed.resize(nonEmptyColsBlock.size(), 1, false);
	    pyRed.resize(nonEmptyRowsBlock.size(), 1, true);
	    for (size_t iCol=0; iCol<nonEmptyColsBlock.size(); iCol++) {
	      pxRed(iCol,0)=px(nonEmptyColsBlock[iCol],0);
	    }
	    //	    m.print("Mat","%5.2g");
	    pyRed.gemm(m, pxRed);
	    for (size_t iRow=0; iRow<nonEmptyRowsBlock.size(); iRow++) {
	      py(iSplit*nbNodesRow+nonEmptyRowsBlock[iRow],0)+=pyRed(iRow,0);
	    }
	  }
	}
      }else{
	const fullMatrix<double> &m = it->second;
	py.gemm(m, px);
      }
    }
  }
}

void dgBlockMatrix::invMmult(){
  if (_nonEmptyRows.size()!=0){
    Msg::Fatal("Cannot apply invMmult to a reduced matrix");
  }
  for (size_t i=0; i<_matrix.size(); i++){
    fullMatrix<double> invMElem;
    _groups->getElementGroup(_dof2Group[i])->getInverseMassMatrix().getBlockProxy(_dof2Element[i],invMElem);
    for (std::map<size_t, fullMatrix<double> >::iterator it = _matrix[i].begin(); it != _matrix[i].end(); ++it) {
      fullMatrix<double>&m=it->second;
      size_t nbNodesRow=m.size1()/_nbFields;
      size_t nbNodesCol=m.size2()/_nbFields;
      fullMatrix<double> mField(nbNodesRow,nbNodesCol);
      fullMatrix<double> mFieldMInv(nbNodesRow,nbNodesCol);
      for (size_t iField=0; iField<_nbFields; iField++){
	for (size_t jField=0; jField<_nbFields; jField++){
	  for (size_t iRow=0; iRow<nbNodesRow; iRow++){
	    for (size_t iCol=0; iCol<nbNodesCol; iCol++){
	      mField(iRow, iCol) = m(iField * nbNodesRow + iRow, jField * nbNodesCol + iCol);
	    }
	  }
	  invMElem.mult(mField,mFieldMInv);	 
	  for (size_t iRow=0; iRow<nbNodesRow; iRow++){
	    for (size_t iCol=0; iCol<nbNodesCol; iCol++){
	      m(iField * nbNodesRow + iRow, jField * nbNodesCol + iCol) = mFieldMInv(iRow, iCol);
	    }
	  }
	}
      }
    }
  }
}

void dgBlockMatrix::reduce(bool split, double epsilon){
  if (_nonEmptyRows.size() !=0)
    Msg::Fatal("Matrix already reduced");
  if (split && _nbFields>1){
    _nbSplit=_nbFields;
    _matrix.resize(_nbSplit*_matrix.size());
    for (size_t i=0; i<_matrix.size()/_nbSplit; i++){
      for (std::map<size_t, fullMatrix<double> >::iterator it = _matrix[i].begin(); it != _matrix[i].end(); ++it) {
	fullMatrix<double>&m=it->second;
	size_t nbNodesRow=m.size1()/_nbFields;
	for (size_t iSplit=1; iSplit<_nbSplit; iSplit++){
	  fullMatrix<double> &mSplit = _matrix[iSplit*_matrix.size()/_nbSplit+i][it->first];
	  mSplit.resize(nbNodesRow,m.size2());
	  for (size_t iRow=0; iRow<nbNodesRow; iRow++){
	    for (int iCol=0; iCol<m.size2(); iCol++){
	      mSplit(iRow,iCol)=m(iSplit*nbNodesRow+iRow,iCol);
	    }
	  }
	}
	//The first sub-matrix is written in place of the matrix
	fullMatrix<double> mSplit(nbNodesRow,m.size2());
	for (size_t iRow=0; iRow<nbNodesRow; iRow++){
	  for (int iCol=0; iCol<m.size2(); iCol++){
	    mSplit(iRow,iCol)=m(iRow,iCol);
	  }
	}
	m.resize(nbNodesRow,m.size2());
	for (size_t iRow=0; iRow<nbNodesRow; iRow++){
	  for (int iCol=0; iCol<m.size2(); iCol++){
	    m(iRow,iCol)=mSplit(iRow,iCol);
	  }
	}
      }
    }
  }
  
  _nonEmptyRows.resize(_matrix.size());
  _nonEmptyCols.resize(_matrix.size());
  for (size_t i=0; i<_matrix.size(); i++){
    for (std::map<size_t, fullMatrix<double> >::iterator it = _matrix[i].begin(); it != _matrix[i].end(); ++it) {
      fullMatrix<double>&m=it->second;
      //Zero rows
      for (int iRow=0; iRow<m.size1(); iRow++){
	bool zero=true;
	for (int iCol=0; iCol<m.size2(); iCol++)
	  if (fabs(m(iRow,iCol))>epsilon)
	    if (m(iRow,iCol)!=0)
	      zero=false;
	if (!zero){
	  _nonEmptyRows[i][it->first].push_back(iRow);	    
	}
      }
      //Zero cols
      for (int iCol=0; iCol<m.size2(); iCol++){
	bool zero=true;
	for (int iRow=0; iRow<m.size1(); iRow++)
	  if (fabs(m(iRow,iCol))>epsilon)
	    if (m(iRow,iCol)!=0)
	      zero=false;
	if (!zero)
	  _nonEmptyCols[i][it->first].push_back(iCol);	    
      }
      std::vector<size_t> &nonEmptyRowsBlock=_nonEmptyRows[i][it->first];
      std::vector<size_t> &nonEmptyColsBlock=_nonEmptyCols[i][it->first];
      if (nonEmptyRowsBlock.size() != (size_t) m.size1() || nonEmptyColsBlock.size() != (size_t) m.size2()){
	fullMatrix<double> buf(m);
	m.resize(nonEmptyRowsBlock.size(),nonEmptyColsBlock.size());
	for (size_t iRow=0; iRow<nonEmptyRowsBlock.size(); iRow++){
	  for (size_t iCol=0; iCol<nonEmptyColsBlock.size(); iCol++){
	    m.set(iRow,iCol,buf(nonEmptyRowsBlock[iRow],nonEmptyColsBlock[iCol]));
	  }
	}
      }
      if ((size_t) m.size2() != nonEmptyColsBlock.size() || (size_t) m.size1() != nonEmptyRowsBlock.size()){
        Msg::Fatal("s1 %i s2 %i row %lu col %lu",m.size1(),m.size2(), (unsigned long int)nonEmptyRowsBlock.size(), (unsigned long int)nonEmptyColsBlock.size());
      }
    }
  }
}


