#include <vector>

#include "GmshConfig.h"
#include "linearSystem.h"
#include "fullMatrix.h"

class dgGroupCollection;
class dgExtrusion;
class dgConservationLaw;
template<class scalar>
class linearSystemPETSc;

#if defined(HAVE_PETSC)
/** Linear system class for solving a separate system for each element column */
class dgLinearSystemExtrusion : public linearSystem<fullMatrix<double> > {
  dgGroupCollection *_groups;
  dgExtrusion *_columns;
  dgConservationLaw *_law;
  std::vector<linearSystemPETSc<fullMatrix<double> > >_linSystems;
  std::vector<int> _dof2Group;
  std::vector<int> _dof2Element;
  std::vector<int> _groupStart;
  int _procDofStartIndex, _nbDofs;
  bool _isAllocated;
  inline int _globalDof2Elem(int row) const{ return _dof2Element[row-_procDofStartIndex]; };
  inline int _globalDof2Group(int row) const{ return _dof2Group[row-_procDofStartIndex]; };
  public:
  bool isAllocated() const { return _isAllocated; }
  void allocate(int nbRows);
  void clear();
  void zeroMatrix();
  void zeroRightHandSide();
  int systemSolve();
  void insertInSparsityPattern(int row, int col);
  virtual double normInfRightHandSide() const;
  void addToMatrix(int row, int col, const fullMatrix<double> &val);
  void getFromMatrix(int row, int col, fullMatrix<double> &val) const;
  void addToRightHandSide(int row, const fullMatrix<double> &val);
  void getFromRightHandSide(int row, fullMatrix<double> &val) const;
  void getFromSolution(int row, fullMatrix<double> &val) const;
  void zeroSolution();
  void addToSolution(int row, const fullMatrix< double > &val);
  inline dgGroupCollection *getGroups() const {return _groups;}
  inline dgConservationLaw *getLaw() const {return _law;}
//   void setRef(const dgDofContainer  *xRef,const dgDofContainer *residualRef, double alpha);
//   int mult(const dgDofContainer &x, dgDofContainer &y);
//   int precondition(const dgDofContainer &X, dgDofContainer &Y);
//   void setTolerance(double rtol, double atol, int maxit);
//   void setEpsilon(double epsilon);
  dgLinearSystemExtrusion (dgConservationLaw *law, dgGroupCollection *groups, dgExtrusion* columns);
  ~dgLinearSystemExtrusion();
};
#endif
