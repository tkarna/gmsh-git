#include "dgMPI.h"
#include "GmshConfig.h"
#include "GmshMessage.h"
#ifdef HAVE_MPI
#include "mpi.h"
#endif

void dgMPI::sumPrev(int local, int &prev)
{
  prev = 0;
#ifdef HAVE_MPI
  for (int i = 0; i < Msg::GetCommSize() - 1; ++i) {
    Msg::Barrier();
    int end = prev + local;
    if (i == Msg::GetCommRank())
      MPI_Send((void*) &end, 1, MPI_INT, i + 1, 0, MPI_COMM_WORLD);
    if (i + 1 == Msg::GetCommRank())
      MPI_Recv((void*) &prev, 1, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    Msg::Barrier();
  }
#endif
}

#ifdef HAVE_MPI
template<class t>
static MPI_Datatype get_MPIType();
template<>
MPI_Datatype get_MPIType<double>() { return MPI_DOUBLE;}
template<>
MPI_Datatype get_MPIType<int>() { return MPI_INT;}

template <class t>
static void mpiAllAllV(const std::map<int, std::vector<t> > &send, std::map<int, std::vector<t> > &recv, bool initRecv)
{
  if (initRecv) {
    std::vector<int> sendSize(Msg::GetCommSize()), recvSize(Msg::GetCommSize());
    for (typename std::map<int, std::vector<t> >::const_iterator it = send.begin(); it != send.end(); ++it) {
      sendSize[it->first] = it->second.size();
    }
    MPI_Alltoall((void*)&sendSize[0], 1, MPI_INT, (void*)&recvSize[0], 1, MPI_INT, MPI_COMM_WORLD);
    for (int i = 0; i < Msg::GetCommSize(); ++i)
      if(recvSize[i] > 0)
        recv[i].resize(recvSize[i]);
  }
  std::vector<MPI_Request> requests(recv.size() + send.size(), MPI_REQUEST_NULL);
  int i = 0;
  for(typename std::map<int, std::vector<t> >::iterator it = recv.begin(); it != recv.end(); ++it) {
    MPI_Irecv((void*)&it->second[0], it->second.size(), get_MPIType<t>(), it->first, 0, MPI_COMM_WORLD, &requests[i++]);
  }
  for(typename std::map<int, std::vector<t> >::const_iterator it = send.begin(); it != send.end(); ++it)  {
    MPI_Isend((void*)&it->second[0], it->second.size(), get_MPIType<t>(), it->first, 0, MPI_COMM_WORLD, &requests[i++]);
  }
  MPI_Waitall(requests.size(), &requests[0], MPI_STATUSES_IGNORE);
}
#else
template <class t>
static void mpiAllAllV(const std::map<int, std::vector<t> > &send, std::map<int, std::vector<t> > &recv, bool initRecv)
{
  recv = send;
}
#endif

void dgMPI::allAllV(const std::map<int, std::vector<double> > &send, std::map<int, std::vector<double> > &recv, bool initRecv)
{
  mpiAllAllV(send, recv, initRecv);
}

void dgMPI::allAllV(const std::map<int, std::vector<int> > &send, std::map<int, std::vector<int> > &recv, bool initRecv)
{
  mpiAllAllV(send, recv, initRecv);
}
