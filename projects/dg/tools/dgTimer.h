#ifndef _DG_TIMER_H_
#define _DG_TIMER_H_

#include <time.h>
#include <string>
#include <map>

class dgTimer {
  static dgTimer _root;
  timespec _saved;
  bool _paused;
  std::string _name;
  dgTimer *_parent;
  std::map<const std::string, dgTimer *> _children;
  dgTimer(const char *name, dgTimer *parent);
  void printFullSerial(int level, double total, double rootTime=-1);
  dgTimer *searchFirstPtr(const char *name);
 public :
  void reset();
  double get();
  void start(bool onlyIfParentIsRunning=false);
  void stop();
  dgTimer &child(const char *name);
  dgTimer &searchFirst(const char *name);
  static inline dgTimer &root() {return _root;}
  ~dgTimer();
  void print();
  void printFull(int level = 0, double total = -1);
};

#endif
