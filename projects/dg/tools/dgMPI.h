#ifndef _DG_MPI_H_
#define _DG_MPI_H_
#include <map>
#include <vector>
class dgMPI {
  public:
  static void sumPrev(int local, int &prev);
  static void allAllV(const std::map<int, std::vector<int> > &send, std::map<int, std::vector<int> > &recv, bool initRecv);
  static void allAllV(const std::map<int, std::vector<double> > &send, std::map<int, std::vector<double> > &recv, bool initRecv);
};
#endif
