%module dgCommon

%include std_string.i
%include std_vector.i
%include std_map.i
%include std_pair.i

%rename("_print") "print";
%import("module=gmshpy") "fullMatrix.h";
%import "gmshtypemaps.i"

namespace std {
  %template(IntVector) vector<int>;
  %template(DoubleVector) vector<double, std::allocator<double> >;
  %template(StringVector) vector<std::string, std::allocator<std::string> >;
  %template(MapStringVectorDouble) map<std::string, std::vector<double> >;
  %template(MapStringDouble) map<std::string, double>;
  %template(MapIntInt) std::map< int,int,std::less< int >,std::allocator< std::pair< int const,int > > >;
  %template(PairIntInt) pair<int, int>;
  %template(VectorPairIntInt) vector<pair<int, int> >;
}
