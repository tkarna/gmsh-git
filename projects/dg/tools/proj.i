%include "cstring.i"

%module proj
%import("module=gmshpy") "fullMatrix.h";
%import "gmshtypemaps.i"
%template (fullMatrixDouble) fullMatrix<double>;


%{
  #include "proj_api.h"
  #include "fullMatrix.h"
  double pj_transform( projPJ src, projPJ dst, double &x, double &y) {
    pj_transform(src, dst, 1, 1, &x, &y, NULL);
    return x;
  }
  fullMatrix<double> pjTransform(projPJ src, projPJ dst,const fullMatrix<double> &coord) {
    fullMatrix<double> out = coord;
    pj_transform(src, dst, coord.size1(), 1, &out(0, 0), &out(0, 1), &out(0, 2));
    return out;
  }
  void pjTransformInPlace(projPJ src, projPJ dst,fullMatrix<double> &coord) {
    pj_transform(src, dst, coord.size1(), 1, &coord(0, 0), &coord(0, 1), &coord(0, 2));
  }
%}

typedef void *projPJ;
projPJ pj_init_plus(const char *);
void pj_free(projPJ);

fullMatrix<double> pjTransform(projPJ src, projPJ dst, const fullMatrix<double> &coord);
%apply double *INOUT {double &x, double &y};
void pj_transform(projPJ src, projPJ dst, double &x, double &y);
void pjTransformInPlace(projPJ src, projPJ dst,fullMatrix<double> &coord);
