#include <map>
#include "fullMatrix.h"
#include "dgIntegrationMatrices.h"
#include "GaussIntegration.h"
#include "BasisFactory.h"
#include "Numeric.h"

static std::map<std::pair<int, int>, dgIntegrationMatrices*> _map;

const dgIntegrationMatrices& dgIntegrationMatrices::get(int elementTag, int integrationOrder)
{
  dgIntegrationMatrices* &mat = _map[std::make_pair(elementTag, integrationOrder)];
  if (mat == NULL)
    mat = new dgIntegrationMatrices(elementTag, integrationOrder);
  return *mat;
}

static std::map<std::pair<std::pair<int, int>, int>, dgIntegrationMatrices*> _mapClosure;

void dgIntegrationMatrices::clear()
{
  _map.clear();
  _mapClosure.clear();
}

const dgIntegrationMatrices &dgIntegrationMatrices::get(int elementType, int integrationOrder, int closureId)
{
  dgIntegrationMatrices* &mat = _mapClosure
    [std::make_pair(std::make_pair(elementType, integrationOrder), closureId)];
  if (mat == NULL)
    mat = new dgIntegrationMatrices(elementType, integrationOrder, closureId);
  return *mat;
  
}

void dgIntegrationMatrices::_implicitComplete()
{
  int nbQP = _psi.size1();
  int dim = _dPsi.size1() / nbQP;
  int nbNodesPsi = _psi.size2();
  int nbNodesDPsi = _dPsi.size2();
  _psiDPsiW.resize(nbNodesPsi * nbNodesDPsi, nbQP * dim);
  _dPsiPsiW.resize(nbNodesDPsi * nbNodesPsi, nbQP * dim);
  _dPsiDPsiW.resize(nbNodesDPsi * nbNodesDPsi, nbQP * dim * dim);
  for (int g = 0; g < nbQP; ++ g) {
    const double w = _weight(g);
    for (int i = 0; i< nbNodesDPsi; ++ i) {
      for (int a = 0; a < dim; ++ a) {
        for (int j = 0; j < nbNodesDPsi; ++ j) {
          for (int b = 0; b < dim; ++ b) {
            _dPsiDPsiW (i * nbNodesDPsi + j, (a * dim + b) * nbQP + g) = _dPsi(g * dim + a, i) * _dPsi(g * dim + b, j) * w;
          }
        }
      }
    }
    for (int i = 0; i< nbNodesPsi; ++ i) {
      for (int j = 0; j < nbNodesDPsi; ++ j) {
        for (int a = 0; a < dim; ++ a) {
          _psiDPsiW (j * nbNodesPsi + i, g * dim + a) = _dPsi(g * dim + a, j) * _psi(g, i) * w;
          _dPsiPsiW (i * nbNodesDPsi + j, a * nbQP + g) = _dPsi(g * dim + a, j) * _psi(g, i) * w;
        }
      }
    }
  }
  _implicitCompleted = true;
}

//complete everything based on xi, weigth, psi and dpsi
void dgIntegrationMatrices::_complete()
{
  int nbQP = _psi.size1();
  int dim = _dPsi.size1() / nbQP;
  int nbNodesPsi = _psi.size2();
  int nbNodesDPsi = _dPsi.size2();
  _psiPsiW.resize(nbNodesPsi * nbNodesPsi, nbQP);
  _psiW.resize(nbNodesPsi, nbQP);
  _dPsiW.resize(nbNodesDPsi, nbQP * dim);
  for (int g = 0; g < nbQP; ++ g) {
    const double w = _weight(g);
    for (int i = 0; i< nbNodesDPsi; ++ i) {
      for (int a = 0; a < dim; ++ a) {
        _dPsiW(i, g * dim + a) = _dPsi(g * dim + a, i) * w;
      }
    }
    for (int i = 0; i < nbNodesPsi; ++ i) {
      _psiW(i, g) = _psi(g, i) * w;
      for (int j = 0; j < nbNodesPsi; ++ j) {
        _psiPsiW(i * nbNodesPsi + j, g) = _psi(g, i) * _psi(g, j) * w;
      }
    }
  }
}

void dgIntegrationMatrices::_massComplete()
{
  int nbQP = _psi.size1();
  int nbNodesPsi = _psi.size2();
  _mass.resize(nbNodesPsi, nbNodesPsi);
  _massInverse.resize(nbNodesPsi, nbNodesPsi);
  for (int g = 0; g < nbQP; ++ g) {
    const double w = _weight(g);
    for (int i = 0; i < nbNodesPsi; ++ i) {
       for (int j = 0; j < nbNodesPsi; ++ j) {
        _mass(i, j) += _psi(g, i) * _psi(g, j) * w;
       }
    }
  }
  _mass.invert(_massInverse);
  _massCompleted = true;
}

dgIntegrationMatrices::dgIntegrationMatrices(int elementType, int integrationOrder)
{
  _elementType = elementType;
  _implicitCompleted = false;
  _massCompleted = false;
  _integrationOrder = integrationOrder;
  const nodalBasis &fs = *BasisFactory::getNodalBasis(elementType);
  gaussIntegration::get(fs.parentType, integrationOrder, _xi, _weight);
  int nbNodes = fs.points.size1();
  int nbQP = _xi.size1();
  int dim = fs.dimension;
  _psi.resize(nbQP, nbNodes);
  fs.f(_xi, _psi);
  _dPsi.resize(nbQP * dim, nbNodes);
  double df[256][3];
  for (int g = 0; g < nbQP; ++ g) {
    fs.df(_xi(g, 0), _xi(g, 1), _xi(g, 2), df);
    for (int i=0; i<nbNodes; ++ i) {
      for (int a = 0; a < dim; ++ a) {
        _dPsi(g * dim + a, i) = df[i][a];
      }
    }
  }
  _complete();
}

static double _computeDeterminant(int dim, double jac[3][3])
{
  switch (dim) {
    case 0: return 1.;
    case 1: return sqrt(SQU(jac[0][0]) + SQU(jac[0][1]) + SQU(jac[0][2]));
    case 2: return sqrt(SQU(jac[0][0] * jac[1][1] - jac[0][1] * jac[1][0]) +
            SQU(jac[0][2] * jac[1][0] - jac[0][0] * jac[1][2]) +
            SQU(jac[0][1] * jac[1][2] - jac[0][2] * jac[1][1]));
    case 3: return fabs(jac[0][0] * jac[1][1] * jac[2][2] + jac[0][2] * jac[1][0] * jac[2][1] +
            jac[0][1] * jac[1][2] * jac[2][0] - jac[0][2] * jac[1][1] * jac[2][0] -
            jac[0][0] * jac[1][2] * jac[2][1] - jac[0][1] * jac[1][0] * jac[2][2]);
  }
  Msg::Fatal("cannot compute determinant in dim %i\n", dim);
  return 0.;
}


dgIntegrationMatrices::dgIntegrationMatrices(int elementType, int integrationOrder, int closureId)
{
  _elementType = elementType;
  _implicitCompleted = false;
  _massCompleted = false;
  _integrationOrder = integrationOrder;
  const nodalBasis &fsElem = *BasisFactory::getNodalBasis(elementType);
  const nodalBasis::closure &closure = fsElem.closures[closureId];
  const nodalBasis &fsFace = *BasisFactory::getNodalBasis(closure.type);
  const dgIntegrationMatrices &intMatricesFace = dgIntegrationMatrices::get(fsFace.type, integrationOrder);
  int nbQP = intMatricesFace.integrationPoints().size1();
  _xi.resize(nbQP, 3);
  _xi.setAll(0);
  _weight.resize(nbQP);
  for (int xi = 0; xi < nbQP; ++xi) {
    double jac[3][3] = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
    for(size_t j = 0; j < closure.size(); j++) {
      for(int k = 0;k < fsElem.points.size2(); k++){
        _xi(xi, k) += intMatricesFace.psi()(xi, j) * fsElem.points(closure[j], k);
      }
      for (int l = 0; l < fsFace.dimension; ++l) {
        for (int m = 0; m < fsElem.points.size2(); ++m) {
          jac[l][m] += intMatricesFace.dPsi()(xi * fsFace.dimension + l, j) * fsElem.points(closure[j], m);
        }
      }
    }
    _weight(xi) = intMatricesFace.integrationWeights()(xi) * _computeDeterminant(fsFace.dimension, jac);
  }

  const int dimUVW = fsElem.dimension;
  const int nbQPElem = fsElem.points.size1();
  _psi = intMatricesFace.psi();
  _dPsi.resize(nbQP * dimUVW, nbQPElem, false);
  double gg[1256][3];
  for (int q = 0; q < _xi.size1(); q++) {
    fsElem.df(_xi(q, 0), _xi(q, 1), _xi(q, 2), gg);
    for (int beta = 0; beta < dimUVW; beta++)
      for (int j = 0; j < nbQPElem; j++)
        _dPsi(q * dimUVW + beta, j) = gg[j][beta];
  }
  _complete();
}

