%module tools

%{
#undef HAVE_DLOPEN
#include "process.h"
#include "processPython.h"
#include "dgTimer.h"
#include "dgFullMatrix.h"
#include "dgIntegrationMatrices.h"
#include "dgMesh2MeshProjection.h"
%}

%import(module="dgpy.dgCommon") "dgCommon.i"

%include "process.h"
%include "processPython.h"
%include "dgTimer.h"
%include "dgFullMatrix.h"
%template(fullMatrixDouble) fullMatrix<double>;
%template(dgFullMatrixDouble) dgFullMatrix<double>;
%include "dgIntegrationMatrices.h"
%include "dgMesh2MeshProjection.h"

