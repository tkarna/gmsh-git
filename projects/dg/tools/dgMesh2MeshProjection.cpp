#include "GmshConfig.h"
#include "GModel.h"
#include "MElement.h"
#include "MElementOctree.h"
#include "dgDofContainer.h"
#include "dgGroupOfElements.h"
#include "dgIntegrationMatrices.h"
#include "dgMesh2MeshProjection.h"
#include "linearSystemFull.h"
#include "linearSystemPETSc.h"
#ifdef HAVE_MPI
#include "mpi.h"
#else
#include "string.h"
#endif
#include "dgMesh.h"
#include "dgMeshJacobian.h"

dgMesh2MeshProjection::dgMesh2MeshProjection()
{
  _projectionMatrixIsBuilt = false;
  _numInDofs = _numOutDofs = _inMaxDim = _outMaxDim = 0;
  _inDofPattern = _outDofPattern = NULL;
}

dgMesh2MeshProjection::dgMesh2MeshProjection(dgDofContainer* donor, dgDofContainer* receiver)
{
  _projectionMatrixIsBuilt = false;
  _numInDofs = _numOutDofs = 0;
  _inDofPattern = _outDofPattern = NULL;
  _inDofPattern = new dgDofStoragePattern(donor);
  _outDofPattern = new dgDofStoragePattern(receiver);
  _inMaxDim = donor->getGroups()->getModel()->getDim();
  _outMaxDim = receiver->getGroups()->getModel()->getDim();
  _dGroups = donor->getGroups();
  _rGroups = receiver->getGroups();
  _sameGroupCollection = false;
  if (_dGroups == _rGroups) _sameGroupCollection = true;
  _buildProjectionMatrix();
}

void dgMesh2MeshProjection::rebuild()
{
  if(_projectionMatrixIsBuilt) {
    delete(_projectionMatrix);
  }
  //TODO: optimize
  _buildProjectionMatrix();
}


dgMesh2MeshProjection::~dgMesh2MeshProjection()
{
  if(_projectionMatrixIsBuilt) {
    delete(_projectionMatrix);
    delete _outDofPattern;
    delete _inDofPattern;
  }
}

dgDofStoragePattern::dgDofStoragePattern(dgDofContainer* dofContainer)
{
  _nbGroups = dofContainer->getGroups()->getNbElementGroups();
  _nbFields = dofContainer->getNbFields();
  _nbElems.resize(_nbGroups);
  _nbNodes.resize(_nbGroups);
  _groupFirstProjectionDofId.resize(_nbGroups);

  int offset = 0;
  for (int i = 0; i < _nbGroups; i++) {
    int nbElems = dofContainer->getGroups()->getElementGroup(i)->getNbElements();
    int nbNodes = dofContainer->getGroups()->getElementGroup(i)->getNbNodes();
    _nbElems[i] = nbElems;
    _nbNodes[i] = nbNodes;
    _groupFirstProjectionDofId[i] = offset;
    offset += nbElems*nbNodes;
  }
  _nbProjDofs = offset;
}

dgDofStoragePattern::~dgDofStoragePattern() {}

bool dgDofStoragePattern::equalTo(const dgDofStoragePattern& other) const
{
  if (this->_nbFields != other._nbFields)
    return false;
  return (*this == other);
}

bool dgDofStoragePattern::operator==(const dgDofStoragePattern& other) const
{
  if ( this->_nbGroups != other._nbGroups )
    return false;
  for ( int i =0; i < this->_nbGroups; i++) {
    if ( this->_nbElems[i] != other._nbElems[i] )
      return false;
    if ( this->_nbNodes[i] != other._nbNodes[i] )
      return false;
  }
  return true;
}

bool dgDofStoragePattern::operator!=(const dgDofStoragePattern& other) const
{
  return !operator==(other);
}

bool dgMesh2MeshProjection::_checkTranspose(dgDofContainer* donor, dgDofContainer* receiver)
{
  // check if transpose is necessary
  dgDofStoragePattern dDofPattern = dgDofStoragePattern(donor);
  dgDofStoragePattern rDofPattern = dgDofStoragePattern(receiver);
  bool transpose=false;
  if (dDofPattern == *_inDofPattern && rDofPattern == *_outDofPattern)
    transpose = false;
  else if (dDofPattern == *_outDofPattern && rDofPattern == *_inDofPattern)
    transpose = true;
  else
    Msg::Fatal("the DOF storage patterns do not match the projection matrix.");
  return transpose;
}

void dgMesh2MeshProjection::projectFromTo(dgDofContainer* donor, dgDofContainer* receiver)
{
  if(!_projectionMatrixIsBuilt)
    _buildProjectionMatrix();
  _applyProjection(donor,receiver,_checkTranspose(donor,receiver));
}

void dgMesh2MeshProjection::projectFieldFromTo(dgDofContainer* donor, dgDofContainer* receiver,int donorField, int receiverField)
{
  Msg::Warning("projectFieldFromTo is broken!");
  if(!_projectionMatrixIsBuilt)
    _buildProjectionMatrix();
  _applyProjection(donor,receiver,_checkTranspose(donor,receiver),donorField,receiverField);
}

// TODO: clean copy, face copy dof

// matrix that projects data from one dgDofContainer to another
void dgMesh2MeshProjection::_buildProjectionMatrix()
{
  if (_outMaxDim > _inMaxDim)
    Msg::Fatal("Cannot build projection matrix from lower dim to higher dim. Swap donor and receiver.");

  int jProj = 0; // indices in projection matrix
  int totalNbOutDofs = 0;
  for (int i = 0; i<_rGroups->getNbElementGroups(); i++)
    totalNbOutDofs += _rGroups->getElementGroup(i)->getNbElements()*_rGroups->getElementGroup(i)->getNbNodes();
  _projectionMatrix = new linearSystemCSRGmm<double>;
  _projectionMatrix->allocate(totalNbOutDofs);

  // HACK MG
  const dgGroupCollection* buf = _rGroups;
  _rGroups = _dGroups;
  _dGroups = buf;
  dgDofStoragePattern* dofPattern = _inDofPattern; // _outDofPattern without hack
//   dgDofStoragePattern* dofPattern = _outDofPattern; // without hack
  // integration over donor function space
  for (int jGroup=0;jGroup<_dGroups->getNbElementGroups();jGroup++) {// for donor groups
    dgGroupOfElements* dGroup = _dGroups->getElementGroup(jGroup);
    const nodalBasis &dfs = dGroup->getFunctionSpace();
    const dgIntegrationMatrices &dIntMatrices = dgIntegrationMatrices::get(dfs.type, dfs.order * 2 + 1);
    const dgMeshJacobian &djacobian = _dGroups->_mesh->getJacobian( dfs.order * 2 + 1);
    const fullMatrix<double> &ddetJElement = djacobian.detJElement(jGroup);
    for (size_t jElement=0 ; jElement<dGroup->getNbElements() ;++jElement) {// for elements
      double rShapeFun[256];
      GModel *rModel = _rGroups->getModel();
      MElement* dElem = dGroup->getElement(jElement);
      for (int jPt =0; jPt< dIntMatrices.integrationPoints().size1(); jPt++) {
        int iGroup,iElement;
        dgGroupOfElements* rGroup = dGroup;
        if (!_sameGroupCollection) {
          double x=0,y=0,z=0;
          for (int iVer=0; iVer < dElem->getNumVertices(); iVer++) {
            x += dElem->getVertex(iVer)->x()*dIntMatrices.psi()(jPt,iVer);
            y += dElem->getVertex(iVer)->y()*dIntMatrices.psi()(jPt,iVer);
            z += dElem->getVertex(iVer)->z()*dIntMatrices.psi()(jPt,iVer);
          }
          if (_inMaxDim==3 && _outMaxDim == 2) {
            z = 0; // dummy projection to 2d mesh
          }
          // find p in receiver mesh
          SPoint3 p(x,y,z);
          MElement *rElem;
//           rElem = _rGroups->getModel()->getMeshElementByCoord(p,_rGroups->getElementGroup(0)->getDimUVW());
          rElem = rModel->getMeshElementByCoord(p,rModel->getDim());
          _rGroups->find(rElem,iGroup,iElement);
          if (iElement == -1)
            Msg::Fatal("Integration point (%g,%g,%g) not found in receiver mesh",p.x(),p.y(),p.z());
          rGroup = _rGroups->getElementGroup(iGroup);
          double U[3],X[3]={x,y,z};
          rElem->xyz2uvw(X,U);
          rGroup->getFunctionSpace().f(U[0],U[1],U[2],rShapeFun);
        } else {
          iElement = jElement;
          iGroup = jGroup;
          rGroup = dGroup;
        }
        const double detJ = ddetJElement(jPt, jElement);
        int iProj = dofPattern->getProjDofId(iGroup,iElement,0); // ************
        for (int jNode=0;jNode<dGroup->getNbNodes();jNode++){
          for (int iNode=0;iNode<rGroup->getNbNodes();iNode++){
            double val;
            if (!_sameGroupCollection)
              val = rShapeFun[iNode]*dIntMatrices.psiW()(jNode, jPt)*detJ;
            else
              val = dIntMatrices.psi()(jPt,iNode)*dIntMatrices.psiW()(jNode, jPt)*detJ;
//HACK MG   _projectionMatrix->addToMatrix(iProj+iNode,jProj+jNode,val);
            _projectionMatrix->addToMatrix(jProj+jNode,iProj+iNode,val);
//             _projectionMatrix->addToMatrix(iProj+iNode,jProj+jNode,val);
          }
        }
      }
      jProj += dGroup->getNbNodes();
    }
  }

// HACK MG
  _dGroups = _rGroups;
  _rGroups = buf;

  _multiplyByInverseMassMatrix();
  _projectionMatrixIsBuilt = true;
}

// could be improved by using a better matrix representation and an efficient M*P routine
void dgMesh2MeshProjection::_multiplyByInverseMassMatrix()
{
  int *startIndex;
  int *columns;
  double *values;
  _projectionMatrix->getMatrix(startIndex,columns,values);

  int iProj = 0;
  for (int iGroup=0;iGroup<_rGroups->getNbElementGroups();iGroup++) {
    const dgGroupOfElements &rGroup = *_rGroups->getElementGroup(iGroup);
    for (size_t iElement=0 ; iElement<rGroup.getNbElements() ;++iElement) {
      fullMatrix<double> buffer = fullMatrix<double> (rGroup.getNbNodes(),startIndex[iProj+1]-startIndex[iProj]);
      fullMatrix<double> buffer2 = fullMatrix<double> (rGroup.getNbNodes(),startIndex[iProj+1]-startIndex[iProj]);
      fullMatrix<double> iMassEl;
      for (int iNode = 0 ; iNode<rGroup.getNbNodes() ;++iNode) {
        for (int i = startIndex[iProj+iNode]; i < startIndex[iProj+iNode+1] ; i++)
          buffer(iNode,i-startIndex[iProj+iNode]) = values[i];
      }
      rGroup.getInverseMassMatrix().getBlockProxy(iElement, iMassEl);
      buffer2.gemm(iMassEl,buffer);
      for (int iNode = 0 ; iNode<rGroup.getNbNodes() ;++iNode) {
        for (int i = startIndex[iProj+iNode]; i < startIndex[iProj+iNode+1] ; i++)
          values[i] = buffer2(iNode,i-startIndex[iProj+iNode]);
      }
      iProj+=rGroup.getNbNodes();
    }
  }
}

// could be improved by using a better matrix representation and an efficient M*V routine
void dgMesh2MeshProjection::_applyProjection(dgDofContainer* donor, dgDofContainer* receiver, bool transpose, int donorField, int receiverField)
{
  dgDofContainer* dDof = donor;
  dgDofContainer* rDof = receiver;
  if (transpose) {
    Msg::Warning("using transpose of the projection matrix, this may be buggy!");
    rDof = donor;
    dDof = receiver;
  }
  // dDof is the parent dof where integration is performed
  const dgGroupCollection* dGroups = dDof->getGroups();
  const dgGroupCollection* rGroups = rDof->getGroups();
  int nbRFields = receiver->getNbFields();
  int nbDFields = donor->getNbFields();
  
  if (receiverField == -1) {
    receiver->setAll(0.0);
  } else {
    for (int iGroup=0;iGroup<receiver->getGroups()->getNbElementGroups();iGroup++) {
      const dgGroupOfElements &rGroup = *receiver->getGroups()->getElementGroup(iGroup);
      for (size_t iElement=0 ; iElement<rGroup.getNbElements() ;++iElement) {
        for (int iNode = 0 ; iNode<rGroup.getNbNodes() ;++iNode) {
          receiver->getGroupProxy(iGroup)(iNode,iElement*nbRFields+receiverField) = 0.0;
        }
      }
    }
  }

  int iProj = 0; // indices in projection matrix
  std::vector<int> dGroupsStartIGlobal(dGroups->getNbElementGroups() + 1);
  dGroupsStartIGlobal[0] = 0;
  for (size_t i = 1; i<dGroupsStartIGlobal.size(); i++) {
    int nbNodes = dGroups->getElementGroup(i-1)->getNbNodes();
    int nbElems = dGroups->getElementGroup(i-1)->getNbElements();
    dGroupsStartIGlobal[i] = dGroupsStartIGlobal[i-1] + nbElems*nbNodes;
  }
  int *PMstartIndex;
  int *PMcolumns;
  double *PMValues;
  _projectionMatrix->getMatrix(PMstartIndex,PMcolumns,PMValues);

  std::vector<int> receiverFieldVect, donorFieldVect;
  if (receiverField == -1 || donorField == -1) {
    // project all fields
    if (nbRFields != nbDFields)
      Msg::Fatal("Number of fields in donor and receiver dofContainer must match");
    for (int iF = 0; iF< nbRFields; iF++) {
      receiverFieldVect.push_back(iF);
      donorFieldVect.push_back(iF);
    }
  } else {
    receiverFieldVect.push_back(receiverField);
    donorFieldVect.push_back(donorField);
  }
  for (int iGroup=0;iGroup<rGroups->getNbElementGroups();iGroup++) {
    const dgGroupOfElements &rGroup = *rGroups->getElementGroup(iGroup);
    for (size_t iElement=0 ; iElement<rGroup.getNbElements() ;++iElement) {
      for (int iNode = 0 ; iNode<rGroup.getNbNodes() ;++iNode) {
        int jGroup = 0;
        for (int i = PMstartIndex[iProj++]; i < PMstartIndex[iProj] ; i++){
          int jProj = PMcolumns[i];
          while (jProj > dGroupsStartIGlobal[jGroup+1]) {
            jGroup++;
          }
          const dgGroupOfElements &dGroup = *dGroups->getElementGroup(jGroup);
          int jElement = (jProj-dGroupsStartIGlobal[jGroup])/dGroup.getNbNodes();
          int jNode = jProj-dGroupsStartIGlobal[jGroup]-jElement*dGroup.getNbNodes();
          double val = PMValues[i];
          for (size_t kField = 0; kField < receiverFieldVect.size(); kField++){
            int rField = receiverFieldVect[kField];
            int dField = donorFieldVect[kField];
            if (transpose)
              receiver->getGroupProxy(jGroup)(jNode,jElement*nbRFields+rField) += val*donor->getGroupProxy(iGroup)(iNode,iElement*nbDFields+dField);
            else
              receiver->getGroupProxy(iGroup)(iNode,iElement*nbRFields+rField) += val*donor->getGroupProxy(jGroup)(jNode,jElement*nbDFields+dField);
          }
        }
      }
    }
  }
  // send ghost data to others
  receiver->scatter();
}

void dgL2ProjectionCG::rebuild()
{
  // build mass matrix (same for all fields)
  if (_matrixIsBuilt)
    _dofManager->zeroMatrix();
  fullMatrix<double> elemMassMatrix;
  for (int iGroup=0;iGroup<_groups->getNbElementGroups();iGroup++) {
    const dgGroupOfElements &group = *_groups->getElementGroup(iGroup);
    for (size_t iElement = 0 ; iElement < group.getNbElements(); iElement++){
      group.getMassMatrix().getBlockProxy(iElement, elemMassMatrix);
      _dofManager->assembleLHSMatrix(iGroup, iElement, iGroup, iElement, elemMassMatrix);
    }
  }
  _matrixIsBuilt = true;
}


dgL2ProjectionCG::dgL2ProjectionCG(dgDofContainer* dof) : _dofPattern(dof)
{
  _matrixIsBuilt = false;
#if defined(HAVE_PETSC)
  _lsys = new linearSystemPETSc<double>();
  _lsys->setParameter("matrix_reuse","same_matrix");
//   _lsys->setParameter("matrix_reuse","same_sparsity");
  _lsys->setParameter("petscPrefix","dgL2Projection-");
//   _lsys->setParameter("petscOptions","-dgL2Projection-pc_type lu");
   //_lsys->setParameter("petscOptions","-dgL2Projection-ksp_monitor");
  _lsys->setParameter("petscOptions","-dgL2Projection-pc_type ilu -dgL2Projection-ksp_rtol 1.e-16");
#else
  Msg::Fatal("dgL2Projection requires PETSc");
//   _lsys = new linearSystemFull<double>();
#endif
  _groups = dof->getGroups();
  // build dofManager for one field
  _dofManager = dgDofManager::newCG(_groups, 1, (linearSystem<double>*)_lsys);
  rebuild();
}

dgL2ProjectionCG::~dgL2ProjectionCG()
{
  delete _lsys;
  delete _dofManager;
}

void dgL2ProjectionCG::project(dgDofContainer* dof, const function *f){
  dgDofStoragePattern inDofPattern = dgDofStoragePattern(dof);
  if (inDofPattern != _dofPattern)
    Msg::Fatal("Cannot apply L2 projection on the given dgDofContainer, storage pattern does not match");

  int nbFields = dof->getNbFields();
  if (nbFields != f->getNbCol())
    Msg::Fatal("Cannot apply L2 projection: Number of fiels and function dimension do not match.");
  
  const dgGroupCollection* groups = dof->getGroups();
  // project one field at a time, reuse lhs
  int nbFieldsInLoop = 1;
  dgDofContainer rhs(*groups, nbFieldsInLoop);
  for (int kField = 0; kField < nbFields; kField++) {
    // clear rhs
    _dofManager->zeroRHS();
    rhs.scale(0.);
    for (int iGroup=0;iGroup<groups->getNbElementGroups();iGroup++) {
      const dgGroupOfElements &group = *groups->getElementGroup(iGroup);
      const nodalBasis &fs = group.getFunctionSpace();
      const dgIntegrationMatrices &intMatrices = dgIntegrationMatrices::get(fs.type, fs.order * 2 + 1);
      const fullMatrix<double> &detJ = groups->_mesh->getJacobian(fs.order * 2 + 1).detJElement(iGroup);
      int nbQP = intMatrices.integrationPoints().size1();
      // source at integration points
      dgFullMatrix<double> Source(nbQP, nbFieldsInLoop, group.getNbElements(), false);
      dataCacheMap cacheMap(dataCacheMap::INTEGRATION_GROUP_MODE, groups);
      dataCacheDouble &sourceTerm = *cacheMap.get(f);
      fullMatrix<double> source;
      cacheMap.setGroup(&group);
      const fullMatrix<double> &sourceTermMatrix = sourceTerm.get();
      for (size_t iElement=0 ; iElement<group.getNbElements() ;++iElement) {
        Source.getBlockProxy(iElement, source);
        for (int iPt =0; iPt< nbQP; iPt++) {
          source(iPt,0) = sourceTermMatrix(iElement * nbQP + iPt,kField)*detJ(iPt, iElement);
        }
      }
      // assemble source to rhs
      rhs.getGroupProxy(iGroup).gemm(intMatrices.psiW(), Source);
      fullMatrix<double> rhsData;
      for (size_t iElement = 0 ; iElement < group.getNbElements(); iElement++) {
        rhs.getGroupProxy(iGroup).getBlockProxy(iElement, rhsData);
        _dofManager->assembleRHSVector(iGroup, iElement, rhsData);
      }
    }
    // solve dof manager
    _dofManager->solve();
    // copy
    fullMatrix<double> v;
    for (int iGroup = 0; iGroup < groups->getNbElementGroups(); iGroup++){
      dgGroupOfElements *group = groups->getElementGroup(iGroup);
      fullMatrix<double> & solgroup = dof->getGroupProxy(group);
      for (size_t iElement = 0 ; iElement < group->getNbElements(); iElement++){
        v.resize (group->getNbNodes() * nbFieldsInLoop, 1);
        _dofManager->getSolution(iGroup, iElement, v);
        for ( int i = 0; i < group->getNbNodes(); i++) {
            solgroup (i, iElement*nbFields + kField) = v (i, 0);
        }
      }
    }
  }
}
