#include "dgTimer.h"
#include "GmshMessage.h"
#include "dgConfig.h"
#include <sys/time.h>
#include <time.h>
#include <cstdio>
#include <vector>  
#include <algorithm>  

dgTimer dgTimer::_root("root", NULL);


static void getClock(timespec &t) 
{
#if (defined HAVE_REALTIME && _POSIX_TIMERS > 0)

  #if defined CLOCK_MONOTONIC_RAW
    clock_gettime(CLOCK_MONOTONIC_RAW, &t);
  #else
    clock_gettime(CLOCK_MONOTONIC, &t);
  #endif
#else
  struct timeval tv;
  gettimeofday(&tv, NULL);
  t.tv_sec = tv.tv_sec;
  t.tv_nsec = tv.tv_usec*1000;
#endif

}

dgTimer::dgTimer(const char * name, dgTimer *parent)
{
  _paused = true;
  _name = name;
  reset();
  if(parent) {
    _parent = parent;
    _parent->_children[name] = this;
  }
}

dgTimer::~dgTimer()
{
  for(std::map<const std::string, dgTimer *>::iterator it = _children.begin(); it != _children.end(); it++) {
    it->second->_parent = NULL;
    delete it->second;
  }
  if (_parent)
    _parent->_children.erase(_name);
}

dgTimer &dgTimer::child(const char *name)
{
  dgTimer *&c = _children[name];
  if (!c)
    c =  new dgTimer(name, this);
  return *c;
}

dgTimer *dgTimer::searchFirstPtr(const char *name){
  std::map<const std::string, dgTimer*>::iterator find = _children.find(name);
  if (find != _children.end())
    return find->second;
  dgTimer *found = NULL;
  for (std::map<const std::string, dgTimer*>::iterator child = _children.begin(); child != _children.end(); ++child){
    found = child->second->searchFirstPtr(name);
    if (found)
      break;
  }
  return found;
}

dgTimer &dgTimer::searchFirst(const char *name)
{
  dgTimer *found = searchFirstPtr(name);
  if (!found)
    Msg::Fatal("Timer %s not found\n",name);
  return *found;
}

void dgTimer::print() {
  double v = get();
  printf("%.3g %s", v, _name.c_str());
  if (!_children.empty()) {
    printf(" [ ");
    for (std::map<const std::string, dgTimer*>::iterator child = _children.begin(); child != _children.end(); ++child)
      printf("% 2.0f%% %s ", child->second->get() / v * 100, child->first.c_str());
    printf("]");
  }
  printf("\n");
}

struct moreTime {
  typedef std::pair<std::string, dgTimer*> timerVecEl;
  bool operator ()(timerVecEl const& a, timerVecEl const& b) const {
    return a.second->get() > b.second->get();
  }
};
void dgTimer::printFullSerial(int level, double total, double rootTime){ 
  double v = get();
  double rootTotal = rootTime < 0 ? v : rootTime;
  printf("(%04.1f%%) ", v / rootTotal * 100);
  for (int i = 0; i < level; i++) {
    printf("   ");
  }
  if (total > 0)
    printf("%04.1f%% ", v / total * 100);
  printf("%.3g %s\n", v, _name.c_str());
  std::vector<std::pair<std::string, dgTimer*> > childrenVec;
  for (std::map<const std::string, dgTimer*>::iterator child = _children.begin(); child != _children.end(); ++child)
    childrenVec.push_back(std::pair<std::string, dgTimer*>(child->first,child->second));
  std::sort(childrenVec.begin(), childrenVec.end(), moreTime());
  for (std::vector<std::pair<std::string, dgTimer*> >::iterator child = childrenVec.begin(); child != childrenVec.end(); ++child)
    child->second->printFullSerial(level + 1, v, rootTotal);
}

void dgTimer::printFull(int level, double total) {
  for (int i = 0; i < Msg::GetCommSize(); i++){
    if (Msg::GetCommRank() == i){
      if (Msg::GetCommSize() > 1)
	printf("Rank %i\n",i);
      printFullSerial(level, total);
    }
    fflush(stdout);
    Msg::Barrier();
  }
}

void dgTimer::reset() {
  if (_paused) {
    _saved.tv_sec = 0;
    _saved.tv_nsec = 0;
  }
  else {
    getClock(_saved);
  }
  for(std::map<const std::string, dgTimer *>::iterator it = _children.begin(); it != _children.end(); it++){
    it->second->reset();
  }
}

void dgTimer::start(bool onlyIfParentIsRunning)

{
  if (onlyIfParentIsRunning && _parent && _parent->_paused){
    if (_parent == &root() && _parent->_paused)
      _parent->start();
    else
      return;
  }
  if (_paused) {
    timespec t;
    getClock(t);
    _saved.tv_sec = t.tv_sec - _saved.tv_sec;
    _saved.tv_nsec = t.tv_nsec - _saved.tv_nsec;
  }
  _paused = false;
  if (_parent && _parent->_paused && _parent != &root()) {
    Msg::Fatal("cannot start timer %s if its parent is not running",_name.c_str());
  }
}

void dgTimer::stop()
{
  if (!_paused) {
    timespec t;
    getClock(t);
    _saved.tv_sec = t.tv_sec - _saved.tv_sec;
    _saved.tv_nsec = t.tv_nsec - _saved.tv_nsec;
  }
  _paused = true;
}

double dgTimer::get()
{
  if (_paused) {
    return _saved.tv_sec  + _saved.tv_nsec * 1e-9;
  }
  else {
    timespec t;
    getClock(t);
    return t.tv_sec - _saved.tv_sec + (t.tv_nsec - _saved.tv_nsec) * 1e-9;
  }
}
