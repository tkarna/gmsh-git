#ifndef _PROCESS_PYTHON_H_
#define _PROCESS_PYTHON_H_
#include "Python.h"
#include "GmshMessage.h"

class processPython : private process {
  PyObject *_pycallback, *_pyargs;
  //std::vector<PyObject *> _swigA;
 public:
  processPython(PyObject *callback/*, std::vector<PyObject *> args*/) : process(), _pycallback(callback)/*, _swigA(args)*/ {
    /*switch(_swigA.size()) {
      case 0 : _pyargs = Py_BuildValue("()"); break;
      case 1 : _pyargs = Py_BuildValue("(O)", _swigA[0]); break;
      case 2 : _pyargs = Py_BuildValue("(OO)", _swigA[0], _swigA[1]); break;
      case 3 : _pyargs = Py_BuildValue("(OOO)", _swigA[0], _swigA[1], _swigA[2]); break;
      case 4 : _pyargs = Py_BuildValue("(OOOO)", _swigA[0], _swigA[1], _swigA[2], _swigA[3]); break;
      case 5 : _pyargs = Py_BuildValue("(OOOOO)", _swigA[0], _swigA[1], _swigA[2], _swigA[3], _swigA[4]); break;
      case 6 : _pyargs = Py_BuildValue("(OOOOOO)", _swigA[0], _swigA[1], _swigA[2], _swigA[3], _swigA[4], _swigA[5]); break;
      case 7 : _pyargs = Py_BuildValue("(OOOOOOO)", _swigA[0], _swigA[1], _swigA[2], _swigA[3], _swigA[4], _swigA[5], _swigA[6]); break;
      case 8 : _pyargs = Py_BuildValue("(OOOOOOOO)", _swigA[0], _swigA[1], _swigA[2], _swigA[3], _swigA[4], _swigA[5], _swigA[6], _swigA[7]); break;
      default:Msg::Error("python function not implemented for more than 8 arguments");
    }*/
    _pyargs = Py_BuildValue("()");
  }
  void call() {
    PyObject *result = PyEval_CallObject(_pycallback, _pyargs);
    if (result) {
      Py_DECREF(result); //delete result
    }
    else {
      PyErr_Print();
      Msg::Fatal("An error occurs in the python function.");
    }
  }
};
#endif

