#include "dgFullMatrix.h"

template <class scalar>
dgFullMatrix<scalar>::dgFullMatrix()
{
  _blockSize = 0;
}

template <class scalar>
dgFullMatrix<scalar>::dgFullMatrix(int nbRows, int blockSize, int nbBlocks, bool reset)
{
  resize (nbRows, blockSize, nbBlocks, reset);
}

template <class scalar>
dgFullMatrix<scalar>::dgFullMatrix(scalar *data, int nbRows, int blockSize, int nbBlocks):
  fullMatrix<scalar>(data, nbRows, blockSize * nbBlocks)
{
  _blockSize = blockSize;
}

template <class scalar>
inline void dgFullMatrix<scalar>::resize (int nbRows, int blockSize, int nbBlocks, bool reset)
{
  _blockSize = blockSize;
  fullMatrix<scalar>::resize (nbRows, blockSize * nbBlocks, reset);
}

template <class scalar>
void dgFullMatrix<scalar>::setAsProxy(const dgFullMatrix<scalar> &orig)
{
  fullMatrix<scalar>::setAsProxy(orig);
  _blockSize = orig._blockSize;
}

template <class scalar>
void dgFullMatrix<scalar>::setAsProxy(double *v, int nbRows, int blockSize, int nbBlocks)
{
  _blockSize = blockSize;
  fullMatrix<scalar>::setAsProxy(v, nbRows, blockSize * nbBlocks);
}

template <class scalar>
void dgFullMatrix<scalar>::gemmBlock(const dgFullMatrix<scalar>&A, const dgFullMatrix<scalar> &B, double alpha, double beta)
{
  fullMatrix<scalar> p, Ap, Bp;
  size_t n = nBlock();
  for (size_t i = 0; i < n; ++i)
  {
    getBlockProxy(i, p);
    A.getBlockProxy(i, Ap);
    B.getBlockProxy(i, Bp);
    p.gemm(Ap, Bp, alpha, beta);
  }
}

template <class scalar>
void dgFullMatrix<scalar>::reshape(int nbRows, int blockSize, int nbBlocks )
{
  if (nbRows * blockSize * nbBlocks !=  fullMatrix<scalar>::size1() * fullMatrix<scalar>::size2() )
    Msg::Fatal("Invalid reshape, total number of entries must be equal");
  fullMatrix<scalar>::reshape(nbRows, blockSize * nbBlocks);
  _blockSize = blockSize;
}

template class dgFullMatrix<double>;
