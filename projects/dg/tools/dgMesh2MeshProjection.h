#ifndef _DG_MESH_2_MESH_PROJECTION_H
#define _DG_MESH_2_MESH_PROJECTION_H

#include "linearSystemCSR.h"
class dgGroupCollection;
class MElementOctree;
class MElement;
class SPoint3;
class dgDofContainer;
class dgDofManager;
class function;

// Contains the number of groups, elements and nodes to check if DOF containers are mutually compatible
class dgDofStoragePattern {
  private:
    int _nbGroups, _nbFields, _nbProjDofs;
    std::vector<int> _nbElems, _nbNodes, _groupFirstProjectionDofId;
  public:
    dgDofStoragePattern(dgDofContainer* dofContainer);
    ~dgDofStoragePattern();
    // checks if two storage patterns are identical
    bool equalTo(const dgDofStoragePattern& other) const;
    // checks if two storage patterns are identical ignoring number of fields
    bool operator == (const dgDofStoragePattern& other) const;
    bool operator != (const dgDofStoragePattern& other) const;
    // indices needed when accessing the projection matrix
    inline int getProjDofId (int groupId, int elementId, int nodeId) const {
      // this is different from dgDofContainer::getDofId() because nbFields is ignored
      return _groupFirstProjectionDofId[groupId]+elementId*_nbNodes[groupId]+nodeId;
    }
};

/**A class for projecting a function on dgDofContainer using continuous Galerkin spatial discretization. */
class dgL2ProjectionCG {
  private:
  dgDofStoragePattern _dofPattern;
  linearSystem <double>* _lsys;
  dgDofManager* _dofManager;
  const dgGroupCollection* _groups;
  bool _matrixIsBuilt;
  public:
  /**Build a projection instance for a given dgDofContainer.
   * Same instance can be used for any container with similar storage pattern, number of fields can be different. */
   dgL2ProjectionCG(dgDofContainer* dof);
   void rebuild();
   ~dgL2ProjectionCG();
  /**Projects the source function to the dofContainer using continuous Galerkin shape functions. 
   * The dimension of the function and the dofContainer must match. */
   void project(dgDofContainer* dof, const function *f);
};

// TODO: better representation of matrices, faster matrix-vector matrix-matrix products
// projecting 3D integration points to 2D plane, instead of assuming z=0
// possibility to use spesific integration rules
/**The dgMesh2MeshProjection class provides methods for projecting fields from one mesh on another. */
class dgMesh2MeshProjection {
public:
protected:
  bool _projectionMatrixIsBuilt, _useDofContainerOctree, _sameGroupCollection;
  MElementOctree* _octree;
  int _numInDofs, _numOutDofs, _inMaxDim, _outMaxDim;
  dgDofStoragePattern *_inDofPattern, *_outDofPattern;
  const dgGroupCollection *_rGroups, *_dGroups;
  linearSystemCSRGmm<double>* _projectionMatrix;
  void _buildProjectionMatrix();
  void _multiplyByInverseMassMatrix();
  void _applyProjection(dgDofContainer* donor, dgDofContainer* receiver, bool transpose, int donorField=-1, int receiverField=-1);
  bool _checkTranspose(dgDofContainer* donor,dgDofContainer* receiver);
  void _buildReceiverOctree(dgDofContainer* receiver);
  MElement* _getReceiverElementByCoord(SPoint3 point);
public:
  dgMesh2MeshProjection();
  ~dgMesh2MeshProjection();
  /**Build a projection instance between two dgDofContainers. Integration is performend in the donor Dof function space. */
  dgMesh2MeshProjection(dgDofContainer* donor,dgDofContainer* receiver);
  /**Reconstructs the projection matrix. */
  void rebuild();
  /**Projects all the fields in one container to the other */
  void projectFromTo(dgDofContainer* donor,dgDofContainer* receiver);
  /**Projects a certain field in one container to the other */
  void projectFieldFromTo(dgDofContainer* donor, dgDofContainer* receiver,int donorField, int receiverField);
};
#endif
