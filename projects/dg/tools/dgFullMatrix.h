#ifndef _DG_FULL_MATRIX_H_
#define _DG_FULL_MATRIX_H_
#include "fullMatrix.h"
template <class scalar>
class dgFullMatrix : public fullMatrix<scalar> {
  int _blockSize;
public :
  dgFullMatrix();
  dgFullMatrix(int nbRows, int blockSize, int nbBlocks, bool reset);
  dgFullMatrix(scalar *data, int nbRows, int blockSize, int nbBlocks);
  void resize (int nbRows, int blockSize, int nbBlocks, bool reset);
  void getBlockProxy(int iBlock, fullMatrix<scalar> &proxy) const
  {
    proxy.setAsProxy(*this, iBlock * _blockSize, _blockSize);
  }
  void setAsProxy(const dgFullMatrix<scalar> &orig);
  void setAsProxy(double *v, int nbRows, int blockSize, int nbBlocks);
  void gemmBlock(const dgFullMatrix<scalar>&A, const dgFullMatrix<scalar> &B, double alpha, double beta);
  inline int getBlockSize() const {return _blockSize;}
  inline size_t nBlock() const { return fullMatrix<scalar>::size2() / _blockSize;}
  void reshape(int nbRows, int blockSize, int nbBlocks );
};
#endif
