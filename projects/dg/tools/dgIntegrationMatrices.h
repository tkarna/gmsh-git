#ifndef _DG_INTEGRATION_MATRICES_H_
#define _DG_INTEGRATION_MATRICES_H_

#include "fullMatrix.h"

class dgIntegrationMatrices {

  bool _implicitCompleted, _massCompleted;
  void _implicitComplete(), _massComplete();
  int _elementType;
  inline void _checkIComplete() const { if (!_implicitCompleted)const_cast<dgIntegrationMatrices*>(this)->_implicitComplete();}
  inline void _checkMComplete() const { if (!_massCompleted)const_cast<dgIntegrationMatrices*>(this)->_massComplete();}
  void _complete();
  dgIntegrationMatrices(int elementType, int integrationOrder);
  dgIntegrationMatrices(int elementType, int integrationOrder, int closureId);
  int _integrationOrder;

  fullVector<double> _weight;
  fullMatrix<double>
    _xi,
    _psi, _dPsi,
    _psiW, _dPsiW,
    _psiPsiW, _psiDPsiW, _dPsiPsiW, _dPsiDPsiW,
    _mass, _massInverse;

public:

  static void clear();
  static const dgIntegrationMatrices &get(int elementType, int integrationOrder);
  static const dgIntegrationMatrices &get(int elementType, int integrationOrder, int closureId);

  inline int elementType() const {return _elementType;}
  inline int getIntegrationOrder() const {return _integrationOrder;}
  inline int getNbIntegrationPoints() const {return _xi.size1();}
  inline const fullMatrix<double> &integrationPoints() const {return _xi;}
  inline const fullVector<double> &integrationWeights() const {return _weight;}
  inline const fullMatrix<double> &psi() const {return _psi;}
  inline const fullMatrix<double> &dPsi() const {return _dPsi;}
  inline const fullMatrix<double> &psiW() const {return _psiW;}
  inline const fullMatrix<double> &dPsiW() const {return _dPsiW;}
  inline const fullMatrix<double> &psiPsiW() const {return _psiPsiW;}
  inline const fullMatrix<double> &psiDPsiW() const {_checkIComplete(); return _psiDPsiW;}
  inline const fullMatrix<double> &dPsiPsiW() const {_checkIComplete(); return _dPsiPsiW;}
  inline const fullMatrix<double> &dPsiDPsiW() const {_checkIComplete(); return _dPsiDPsiW;}
  inline const fullMatrix<double> &mass() const {_checkMComplete(); return _mass;}
  inline const fullMatrix<double> &massInverse() const {_checkMComplete(); return _massInverse;}
};
#endif
