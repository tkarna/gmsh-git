Mesh.CharacteristicLengthMin = 30;
Mesh.CharacteristicLengthMax = 30;
Mesh.RemeshParametrization=1;
Merge "capot.brep";
Compound Surface(100) = {1, 8, 15, 17, 16, 18, 9, 2, 3, 10, 7, 14, 11, 4, 12, 5, 6, 13} ;
Compound Line(1000) = {47,50};
Compound Line(1001) = {44,46};
Recombine Surface {100};
