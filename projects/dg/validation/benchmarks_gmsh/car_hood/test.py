#
# Testing 2D meshers on conformal reparametrization
#

from gmshpy import *
import time

def capot( mesher, filename,quad,MINELE,MAXELE ) :
    GmshSetOption('Mesh', 'Algorithm', mesher) 
    gm = GModel()
    gm.load(filename)
    gm.mesh(2)

            
    gf = gm.getFaceByTag(100)

    if (gf.getNumMeshElements() > MAXELE):
        exit(-1)
    if (gf.getNumMeshElements() <  MINELE):
        exit(-2)
    
    minqual = 1.
    maxqual = 0.
    avgqual = 0.
    for i in range(gf.getNumMeshElements() ):
        e = gf.getMeshElement(i)
        qual = 0
        if (quad):
             qual = e.etaShapeMeasure()
        else :
            qual = e.gammaShapeMeasure()
        if (qual < minqual):
            minqual = qual
        if (qual > maxqual):
            maxqual = qual
        avgqual = avgqual + qual

    avgqual = avgqual / gf.getNumMeshElements()

    print 'NELE = ',gm.getNumMeshElements(), 'MINQUAL = ', minqual, 'AVGQUAL = ', avgqual

    if (minqual < .3):
        exit(-3)
    if (avgqual < .7):
        exit(-4)

# test all 2D meshers
t1 = time.time()
capot (1.0,'capot.geo', False, 2000,5000) # meshadapt
t2 = time.time()
capot (5.0,'capot.geo', False, 2000,5000) # delaunay ref
t3 = time.time()
capot (6.0,'capot.geo', False, 2000,5000) # frontal
t4 = time.time()
capot (8.0,'capotQuad.geo', True, 1000,2500) # delquad 
t5 = time.time()
tMA = t2 - t1
tDR = t3 - t2
tFR = t4 - t3
tDQ = t5 - t4

#if (tMA > 6.):exit(-5)
#if (tDR > 3.):exit(-6)
#if (tFR > 3.):exit(-7)
#if (tDQ > 6.):exit(-8)

exit(0)

