#
# Testing 2D meshers with boundary layers
#

from gmshpy import *
import time

def cascade( mesher, filename,MINELE,MAXELE ) :
    GmshSetOption('Mesh', 'LcIntegrationPrecision', 1.e-5) 
    GmshSetOption('Mesh', 'Algorithm', mesher) 
    GmshSetOption('Mesh', 'SmoothRatio', 1.3) 
    gm = GModel()
    gm.load(filename)
    gm.mesh(2)
            
#    if (gm.getNumMeshElements() > MAXELE):
#        exit(-1)
#    if (gm.getNumMeshElements() <  MINELE):
#        exit(-2)
    if(mesher == 6) : gm.save("cascade_mtu_frontal.msh")
    if(mesher == 2) : gm.save("cascade_mtu_delaunay.msh")


t1 = time.time()
cascade (6.0,'cascade_mtu.geo', 5800,6200) # frontal
cascade (2.0,'cascade_mtu.geo', 6000,6400) # delaunay
exit(0)

