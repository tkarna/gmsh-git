#
# Testing mesh matching
#

from gmshpy import *

def mesh_matching(geo_filename, mesh_filename):
    GmshSetOption('Geometry', 'MatchGeomAndMesh', 1)
    matcher = GeomMeshMatcher_instance()
    gm_geo = GModel()
    gm_geo.load(geo_filename)
    gm_mesh = GModel()
    gm_mesh.load(mesh_filename)
    return matcher.match(gm_geo, gm_mesh)

if mesh_matching('3compwing-split.step', '3compwing.msh') != 1:
    print 'FAILURE'
    exit(-1)

print 'SUCCESS'
exit(0)
