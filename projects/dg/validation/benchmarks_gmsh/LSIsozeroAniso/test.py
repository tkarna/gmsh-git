#05/2/2012 shamrock2D

from dgpy import *
from Common import *
import os
import time
import math

# Parameters
L=1 # domain size
lc = 0.2*L # initial element size

# Level sets definition
circle_radius = 0.25*L
shamrock_radius = 0.25*L
shamrock_radius_variation = 0.15*L
lscircle = gLevelsetShamrock(0.5*L,0.5*L,0.,circle_radius,0.,1)
lsshamrock = gLevelsetShamrock(0.5*L,0.5*L,0.,shamrock_radius, shamrock_radius_variation,3)

nbElementsOnE = 5 # number of elements in the band of size E
Np = 12 #number of elements in tangential direction to describe a circle - for curvature-based metric

# first level set adapt parameters
Eplus = 2.e-3*L
hmin = Eplus/nbElementsOnE
hmax = 0.2*L

# second level set adapt parameters
Epluscircle = 8e-3*L # band width in positive part of the ls
Emoinscircle = -2.e-3*L # band width in positive part of the ls
hmincircle = Epluscircle/nbElementsOnE
hmaxcircle = 0.2*L

# Creating initial mesh
print'---- Load Mesh & Geometry'
meshName0  = "shamrock0_2D"
meshName   = "shamrock_2D"
tempmeshName   = "temp"

cmd = "sed -e 's/XXXX/%g/g' %s > %s" % (lc, meshName0+geo, tempmeshName+geo)
cmd2 = "sed -e 's/YYYY/%g/g' %s > %s" % (L, tempmeshName+geo, meshName+geo)
cmdrm = "rm %s" % (tempmeshName+geo)
os.system(cmd)
os.system(cmd2)
os.system(cmdrm)

GmshSetOption('Mesh', 'Algorithm', 4.0) 

g = GModel()

# stuff for the adaptation loop, to be able to capture a band width E <<< initial element size or hmax
# for shamrock ls
Estart = 0.5*hmax
hminstart = Estart/nbElementsOnE
g.load(meshName+geo)
currenthmin = hminstart
currentE = Estart
# for circle ls
Ecirclestart = Estart*Epluscircle/Eplus;
hmincirclestart = hminstart*hmincircle/hmin;
currentEcircle = Ecirclestart 
currenthmincircle =hmincirclestart 

counter=1

while(currentE>Eplus):
#  print 'current hmin %g  hmax %g  currentE %g '% (currenthmin,hmax,currentE)
#  cmdsys = "echo 'current hmin %g  hmax %g  currentE %g  counter %g  ' > %d.txt " % (currenthmin,hmax,currentE,counter,counter)
#  os.system(cmdsys)
  print '---- Adapt mesh with ls ********************  loop on E %d ' % (counter)
  g.adaptMesh([4,4], [lscircle,lsshamrock], [[currentEcircle, currenthmincircle, hmaxcircle, Np, Emoinscircle],[currentE, currenthmin, hmax, Np]], 20, False)
#  tempname = "temp_%d.msh" % (counter)
#  g.save(tempname)
  counter += 1
  currentE = max(currentE - currenthmin*(nbElementsOnE-1),Eplus);
  currenthmin = currentE/nbElementsOnE;
  currentEcircle = currentE*Epluscircle/Eplus;
  currenthmincircle = currenthmin*hmincircle/hmin;

#outputs - final mesh and ls's

g.save("GModelEnd.msh")
g2 = GModel()
g2.load("GModelEnd.msh");
grouppps = dgGroupCollection(g2) 
malevelsetDG = dgDofContainer(grouppps,1) 
malevelsetDG.interpolate(functionLevelsetValueNew(lsshamrock)) 
malevelsetDG.exportMsh('mylevelsetshamrock') 
malevelsetDG.interpolate(functionLevelsetValueNew(lscircle)) 
malevelsetDG.exportMsh('mylevelsetcircle') 


gf = g2.getFaceByTag(6)
finalNbElements = gf.getNumMeshElements()

print 'NELE = ',finalNbElements

if (finalNbElements<5500 or finalNbElements>8000):exit(-1)

exit(0)
