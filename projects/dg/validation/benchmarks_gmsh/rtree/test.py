from gmshpy import *
from dgpy import *
import time

t1 = time.time()

GmshSetOption('Mesh','Algorithm',9.0)
GmshSetOption('Mesh','Algorithm3D',9.0)
GmshSetOption('Mesh','Smoothing',0.0)

model = GModel()
model.load('mesh1.geo')

model.mesh(3)

model.save('out.msh')

num = model.getNumMeshVertices()

t2 = time.time()
t = t2-t1

print 'number of seconds : {0}'.format(t)
print 'number of vertices : {0}'.format(num)

if(num<250 or num>4000):exit(-1)

exit(0)