from gmshpy import *
from dgpy import *
import time

t1 = time.time()

model = GModel()
model.load('essai1.geo')
model.mesh(2)

s = smoothing(10,6)
s.optimize_model()

model.save('out.msh')

num = model.getNumMeshElements()

t2 = time.time()
t = t2-t1

print 'number of seconds : {0}'.format(t)
print 'number of elements : {0}'.format(num)

if(num<250 or num>4000):exit(-1)

exit(0)