Point(1) = {0, 0, 0, 0.05};
Point(2) = {1, 0, 0, 0.05};
Point(3) = {0, 0, 1, 0.05};
Point(4) = {0, 1, 0, 0.05};
Point(5) = {1, 1, 0, 0.05};
Point(6) = {0, 1, 1, 0.05};
Circle(1) = {2, 1, 3};
Circle(2) = {6, 4, 5};
Line(3) = {6, 3};
Line(4) = {2, 5};
Line Loop(5) = {2, -4, 1, -3};
Ruled Surface(6) = {5};

Mesh.RemeshParametrization=1; //(0) harmonic (1) conformal 
Mesh.RemeshAlgorithm=0; //(0) nosplit (1) automatic (2) split metis

//Compound Line(100)={1,2,3,4};
Compound Surface(200)={6};

Physical Surface(1001)={200};
