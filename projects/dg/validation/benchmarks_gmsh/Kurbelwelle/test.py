#
# Testing 3D meshers
#

from gmshpy import *
import time

def MESH( mesher, filename,MINELE,MAXELE,LC,MINQ ) :

    GmshSetOption('Mesh', 'CharacteristicLengthMin', LC) 
    GmshSetOption('Mesh', 'CharacteristicLengthMax', LC) 
    gm = GModel()
    gm.load(filename)
    GmshSetOption('Mesh', 'Algorithm', mesher) 
    GmshSetOption('Mesh', 'Optimize', 1.0) 
    gm.mesh(3)

            
    gf = gm.getRegionByTag(1)
    
    minqual = 1.
    maxqual = 0.
    avgqual = 0.
    for i in range(gf.getNumMeshElements() ):
        e = gf.getMeshElement(i)
        qual = e.gammaShapeMeasure()
        if (qual < minqual):
            minqual = qual
        if (qual > maxqual):
            maxqual = qual
        avgqual = avgqual + qual

    avgqual = avgqual / gf.getNumMeshElements()

    print 'NELE = ',gf.getNumMeshElements(), 'MINQUAL = ', minqual, 'AVGQUAL = ', avgqual

#    if (gf.getNumMeshElements() > MAXELE):
#        exit(-1)
#    if (gf.getNumMeshElements() <  MINELE):
#        exit(-2)
    if (minqual < MINQ):
        exit(-3)
    if (avgqual < .5):
        exit(-4)

# test all 2D meshers
t1 = time.time()

MESH (1.0,'Kurbelwelle.geo', 8900,9300, 2.0, 0.01) # meshadapt
#MESH (4.0,'Kurbelwelle.geo', 8000,14000, 2.0, 0.01) # meshadapt
MESH (1.0,'Kurbelwelle.geo', 23000,27000, 1.0, 0.01) # meshadapt
MESH (1.0,'Kurbelwelle.geo', 150000,160000, 0.5, 0.01) # meshadapt
MESH (1.0,'Kurbelwelle.geo', 1100000,1200000, 0.25, 0.01) # meshadapt
print 'DONE'
exit(0)

