#
# Testing Extrusion
#

from gmshpy import *
import time

def durufle( mesher, filename,MINELE,MAXELE ) :
    GmshSetOption('Mesh', 'Algorithm', mesher) 
    gm = GModel()
    gm.load(filename)
    gm.mesh(3)

            
    if (gm.getNumMeshElements() > MAXELE):
        exit(-1)
    if (gm.getNumMeshElements() <  MINELE):
        exit(-2)
    gm.save("mesh.msh")

durufle (1.0,'durufle.geo', 1000,29000) 
print  'SUCCESS'
exit(0)

