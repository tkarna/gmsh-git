from gmshpy import *
from dgpy import *
import time
import os.path

t1 = time.time()

model = GModel()
model.load('mesh1.geo')
model.mesh(3)

f = voroMetal3D()
f.execute(0.1)

model.save('out.msh')

size = os.path.getsize('MicrostructurePolycrystal3D.geo')

t2 = time.time()
t = t2-t1

print 'number of seconds : {0}'.format(t)
print 'file size of cells.geo : {0}'.format(size)

if(size<1000 or size>1000000):exit(-1)

exit(0)