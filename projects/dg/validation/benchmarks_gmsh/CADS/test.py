#
# Testing SAMTECH cads
#

from gmshpy import *

def cad( mesher, filename, MINELE, MAXELE) :
    GmshSetOption('Mesh', 'LcIntegrationPrecision', 1.e-5) 
    GmshSetOption('Mesh', 'Algorithm', mesher) 
    GmshSetOption('Mesh', 'SmoothRatio', 1.3) 
    gm = GModel()
    gm.load(filename)
    gm.mesh(2)
    if (gm.getNumMeshElements() > MAXELE):exit(-1)
    if (gm.getNumMeshElements() <  MINELE):exit(-2)
    
            
cad (8.0,'silly_face.geo',1000,1100) # meshadapt
print 'DONE WITH SUCCESS!!!'
exit(0)

