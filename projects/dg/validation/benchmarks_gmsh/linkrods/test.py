#
# Testing 3D meshers
#

from gmshpy import *
import time

def MESH( mesher, filename,MINELE,MAXELE,MINQ ) :

    gm = GModel()
    gm.load(filename)
    GmshSetOption('Mesh', 'Algorithm', mesher) 
    GmshSetOption('Mesh', 'Optimize', 1.0) 
    gm.mesh(3)

            
    gf = gm.getRegionByTag(1)
    
    minqual = 1.
    maxqual = 0.
    avgqual = 0.
    for i in range(gf.getNumMeshElements() ):
        e = gf.getMeshElement(i)
        qual = e.gammaShapeMeasure()
        if (qual < minqual):
            minqual = qual
        if (qual > maxqual):
            maxqual = qual
        avgqual = avgqual + qual

    avgqual = avgqual / gf.getNumMeshElements()

    print 'NELE = ',gf.getNumMeshElements(), 'MINQUAL = ', minqual, 'AVGQUAL = ', avgqual

#    if (gf.getNumMeshElements() > MAXELE):
#        exit(-1)
#    if (gf.getNumMeshElements() <  MINELE):
#        exit(-2)
#    if (minqual < MINQ):
#        exit(-3)
#    if (avgqual < .5):
#        exit(-4)

# test all 2D meshers
t1 = time.time()

MESH (1.0,'linkrods.geo', 300000,350000, 0.05) 
print 'DONE'
exit(0)

