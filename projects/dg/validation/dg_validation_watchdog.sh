#!/bin/bash
PIDFILE=/tmp/dg-validation.pid
export LC_ALL=C

if [ -e "${PIDFILE}" ] && (ps -a -f |grep "[ ]$(cat ${PIDFILE})[ ]"); then
  echo "already running"
  exit 0;
fi
echo $$ > ${PIDFILE}

while true; do
  if [ -e public/results/global/global/last_tested/info ]; then
    old_rev=`awk '/svn_revision/{print $3}' public/results/global/global/last_tested/info`
  else
    old_rev=-1
  fi
  svn up ../../../
  rev=`svn info ../../../ |awk '/Revision/{print $2}'`
  echo "rev = $rev, old_rev = $old_rev";
  if [ $rev -gt $old_rev ]; then
    ./dg_validation.sh $rev
  else
    sleep 20
  fi
done
