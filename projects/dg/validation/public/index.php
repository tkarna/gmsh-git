<?php
if($_SERVER["argc"]>1)
  $server_path=$_SERVER["argv"][1];
else
  $server_path=".";
  
echo <<<END
<html><head><title>DG validation</title></head>
<style>

END;
include("benchmarks.css");
echo <<<END
</style>
<body>
<h3><a href="$server_path/index.php">DG automatic testing report</a></h3>
<table class="slim_validation_table">
<tr class="odd_line">
  <th class="first_col">test name</th>
  <th>status</th>
  <th>last tested</th>
  <th>last passed</th>
  <th>first failed</th>
  <th>time</th>
  <th>diff</th>
</tr>
END;
  function safe_parse_ini_file($file){
    if (file_exists($file)) {
      return parse_ini_file($file);
    }
    else {
      return array(0);
    }
  }
  function print_with_log($array,$key,$log){
    global $server_path;
    if(array_key_exists($key, $array)) {
      if(file_exists("results/".$log)){
        echo "  <td><a href=".$server_path."/results/browse.php?loc=".urlencode($log).">$array[$key]</a></td>\n";
      }
      else{
        echo "  <td>$array[$key]</td>\n";
      }
    }
    else {
      echo "  <td></td>\n";
    }
  }
  $all_dir=file($server_path."/dir_list", FILE_IGNORE_NEW_LINES);
  foreach ($all_dir as $dir) {
    echo"<tr class=\"title_line\">\n";
    echo "  <td colspan=\"7\">$dir</td>\n";
    echo "</tr>\n";
    $all_test=scandir("results/$dir");
    natcasesort($all_test);
    $i = 0;
    foreach ($all_test as $test) {
      if($test=="." || $test=="..")
        continue;
      if($i%2==0)
        echo"<tr class=\"even_line\">\n";
      else
        echo"<tr class=\"odd_line\">\n";
      echo "  <td class=\"first_col\">$test</td>\n";
      $i = $i + 1;
      $test_dir="results/".$dir."/".$test;
      $test_dir_browse = $dir."/".$test;
      $info_tested = safe_parse_ini_file($test_dir."/last_tested/info");
      $info_passed = safe_parse_ini_file($test_dir."/last_passed/info");
      $info_failed = safe_parse_ini_file($test_dir."/first_failed/info");
      echo "  <td class=\"slim_benchmark_{$info_tested["status"]}\">{$info_tested["status"]}</td>\n";
      print_with_log($info_tested, "svn_revision", "$test_dir_browse/last_tested/");
      print_with_log($info_passed, "svn_revision", "$test_dir_browse/last_passed");
      print_with_log($info_failed, "svn_revision", "$test_dir_browse/first_failed");
      print_with_log($info_tested, "time", "none");
      echo "<td>\n";
      if(array_key_exists("svn_revision",$info_failed) && array_key_exists("svn_revision", $info_passed)) {
        echo "<a href = \"https://geuz.org/trac/gmsh/changeset?new={$info_failed["svn_revision"]}%40trunk&old={$info_passed["svn_revision"]}%40trunk\">{$info_passed["svn_revision"]}:{$info_failed["svn_revision"]}</a>";
      }
      echo "</td>\n";
      echo "</tr>\n";
    }
  }
  
echo <<<END
</table>
</body>
</html>
END
?>
