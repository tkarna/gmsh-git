<?php

error_reporting(1);
$this_script = basename(__FILE__);
if(isset($_GET['loc'])){
  $location = $_GET['loc'];
}
else {
  $location =".";
}

if(strpos(realpath("$location"), realpath(".")) === 0){
 $location = substr(realpath("$location"), strlen(realpath("."))+1);
 if(strlen($location) == 0)
   $location = ".";
}
else {
  $location =".";
}

$file_list = array();
$folder_list = array();

if ($handle = opendir($location))
{
  while (false !== ($file = readdir($handle)))
  {
    if (($file[0] != "." || $file =="..") && $file != $this_script)
    {
      $item['lname'] =  strtolower($file);
      $item['name']  =  $file;
      $item['size']  =  bytes_to_string(filesize($location."/".$file),1);
      $item['mtime'] =  strftime("%x %X", filemtime($location."/".$file));
      if(is_dir("$location/$file"))
        array_push($folder_list, $item);
      else
        array_push($file_list, $item);
      clearstatcache();
    }
  }
  closedir($handle);
}
if($folder_list)
  sort($folder_list);
if($file_list)
  sort($file_list);

/**
 *  @ http://us3.php.net/manual/en/function.filesize.php#84652
 */
function bytes_to_string($size, $precision = 0) {
  $sizes = array('YB', 'ZB', 'EB', 'PB', 'TB', 'GB', 'MB', 'KB', 'B');
  $total = count($sizes);
  while($total-- && $size > 1024) $size /= 1024;
  $return['num'] = round($size, $precision);
  $return['str'] = $sizes[$total];
  return $return;
}

?>

<?
function odd_line_style($i)
{
  if (($i%2)==0) {
    return "odd_line";
  }
  else {
    return "even_line";
  }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>DG automatic testing : <?=$location ?></title>
<link rel="stylesheet" type="text/css" href="browse.css" media="screen" />
</head>

<body>
<h3><a href="..">DG automatic testing</a> : <?=$location ?></h3>
<table cellpadding="2" cellspacing="1">
<? $i = 0; ?>
<? if($folder_list):?>
<tr class="title_line"> <td colspan="3">Folders</td> </tr>
<? foreach($folder_list as $item) : ?>
<? $i = $i + 1; ?>
    <tr class=<?=odd_line_style($i)?> >
      <td class="file_name"><a href="?loc=<?=urlencode($location.'/'.$item['name'])?>"><?=$item['name']?></a></td>
      <td class="file_size"></td>
      <td class="file_mtime"><?=$item['mtime']?></td>
    </tr>
<? endforeach; ?>
<? endif; ?>
<? if($file_list): ?>
<tr class="title_line"> <td colspan="3"> Files</td> </tr>
<? foreach($file_list as $item) : ?>
<? $i = $i + 1; ?>
    <tr class=<?=odd_line_style($i)?> >
      <td class="file_name"><a href="<?=$location.'/'.$item['name']?>"><?=$item['name']?></a></td>
      <td class="file_size"><?=$item['size']['num']?> <?=$item['size']['str']?></span></td>
      <td class="file_mtime"><?=$item['mtime']?></td>
    </tr>
<? endforeach; ?>
<? endif; ?>
</table>
</body>
</html>
