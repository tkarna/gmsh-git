#!/bin/bash

rev=${1--1}


if [ "$rev" -ne "-1" ]; then
  source ~/.bashrc
  source password.sh
fi
PIDFILE=/tmp/dg-validation.pid

export LC_ALL=C
SYNC_DIR=$PWD

# $1 command $2 name $3 rev $4 dir
sync() {
  if [ "$rev" -eq "-1" ]; then
    return;
  fi
  cd $SYNC_DIR
  rm -rf tmp_mnt
  mkdir tmp_mnt
  echo $PASSWORD | sshfs wwwimmc@webhost-test.uclouvain.be:/mema/dg_validation tmp_mnt -o password_stdin
  rsync -rtL --chmod=o+rX --delete public/ tmp_mnt/
  fusermount -u tmp_mnt
  rmdir tmp_mnt
  cd -
}

sync_result_dir() {
  if [ "$rev" -eq "-1" ]; then
    return;
  fi
  cd $SYNC_DIR
  rm -rf tmp_mnt
  mkdir tmp_mnt
  echo $PASSWORD | sshfs wwwimmc@webhost-test.uclouvain.be:/mema/dg_validation tmp_mnt -o password_stdin
  rsync -rtL --chmod=o+rX --delete public/results/$1/ tmp_mnt/results/$1/
  fusermount -u tmp_mnt
  rmdir tmp_mnt
  cd -
}

send_mail() {
  if [ "$rev" -eq "-1" ]; then
    return;
  fi
  cd public
  php5 -q index.php http://sites-test.uclouvain.be/immc/mema/dg_validation | mail -a "MIME-Version: 1.0" -a "Content-type: text/html; charset=iso-8859-1" -a "From: dg benchmarks <no-reply@dg.dg>" -s "dg testing report $1" `cat ../mail_list`
  cd ..
}

run_test() {
  ulimit -S -t 600
  echo -ne "$2 running\r"
  rdir="$RESULT_DIR/$4/$2"
  dir="$rdir/last_tested"
  rm -rf $dir
  mkdir -p $dir
  echo "status = \"running\"" > "$dir/info"
  echo "svn_revision = $3" >> "$dir/info"
  sync_result_dir $4/$2
  start_time=`date "+%s"`
  if $1  &>"$dir/log" ; then
    res="passed"
    echo -e "$2 passed\033[0K"
  else
    res="failed"
    echo -e "$2 failed\033[0K"
  fi
  end_time=`date "+%s"`
  echo "status = \"$res\"" > "$dir/info"
  echo "time = $(($end_time-$start_time))" >> "$dir/info"
  echo "svn_revision = $3" >> "$dir/info"
  if [ "$DG_VALIDATION_EXPORT_DIR" ]; then
    if [ $DG_VALIDATION_EXPORT_DIR -eq 1 ]; then
      rm -rf $dir/work
      cp -r . $dir/work
    fi
  fi
  if [ "$res" = "passed" ]; then
    rm -rf "$rdir/last_passed"
    rm -rf "$rdir/first_failed"
    cp -r $dir "$rdir/last_passed"
  else 
    if [ ! -d "$rdir/first_failed" ]; then
      cp -r $dir "$rdir/first_failed"
    fi
    let nfailed=$nfailed+1
    if [ ! -e  no_mail ]; then
      let nmail=$nmail+1
    fi
  fi
  sync_result_dir $4/$2
  ulimit -t unlimited
  if [ "$res" = "passed" ]; then
    return 0
  else
    return 1
  fi
}

run_test_dir() {
  if [ ! -d $1 ]; then return; fi
  for name in `ls --color=never "$1" | sort -f`; do
    pdir=$PWD
    rm -rf work
    cp -r "$1/$name" work
    cd work
    DG_VALIDATION_EXPORT_DIR=1
    if [ -e test.py ]; then
      run_test "../build/rundgpy test.py" "$name" $rev $2
    else
      run_test "./test.sh" "$name" $rev $2
    fi
    unset DG_VALIDATION_EXPORT_DIR
    cd $pdir
  done
  #remove old test case's directories 
  for i in public/results/$2/*; do
    if [ -e $i/last_tested ]
    then
      lasttested=`awk '/svn_revision/{print $3}' $i/last_tested/info`
    else
      lasttested=-1
    fi
    if [ ! $lasttested -eq $rev ]; then
      rm -rf $i
    fi
  done
}

RESULT_DIR=$PWD/public/results
start_time_global=`date "+%s"`
nfailed=0
nmail=0
rm -rf "public/results/global/global/last_tested"
mkdir -p "public/results/global/global/last_tested"
echo 'status = "running"' > "public/results/global/global/last_tested/info"
echo "svn_revision = $rev" >> "public/results/global/global/last_tested/info"

ALLDIRS=`awk '{print $1}' build/test_directories`
echo "compilation" > public/dir_list
for i in $ALLDIRS; do
  echo $i >> public/dir_list
done
echo gmsh >> public/dir_list

#test compilation
run_test "cmake build" "cmake" $rev compilation 
  run_test "make -j2 -C build/gmsh" "gmshpy" $rev compilation && \
  run_test "make -j2 -C build _dgpy" "dgpy" $rev compilation && \
  run_test "make -j2 -C build gmsh" "gmsh" $rev compilation
COMPILATION_SUCCEED=$?

mkdir -p build/bin
ln -sf  $PWD/build/gmsh/gmsh build/bin/gmsh
export PATH=$PWD/build/bin:$PATH

if [ $COMPILATION_SUCCEED -eq 0 ]; then #sucess
  for i in $ALLDIRS; do
    run_test_dir ../$i $i
  done
  run_test_dir "benchmarks_gmsh" "gmsh"
  #test benchmarks
  #find ../../../ -name '*.gcda' | xargs rm  
  #lcov --directory ../../../ --zero-counters
  #rm -f gmsh.info
  #lcov --directory ../../../ --capture --output-file gmsh.info
  #rm -rf public/coverage
  #genhtml gmsh.info --output-directory public/coverage
  sync
fi
echo "global" >> public/dir_list

#generate _global entry
if [ $nmail -gt 0 ];then
  run_test false "global" $rev public/results/global
  send_mail $rev
fi
end_time_global=`date "+%s"`
