#include "dgCoupling.h"
#include "dgIntegrationMatrices.h"
#include <algorithm>

class dgCoupling_SW1D_SW2D::computeToIntegrate1:public function{
  public:
  fullMatrix<double> _solution;
  computeToIntegrate1(const function *solution):function(3){
    setArgument(_solution, solution);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val){
    for( int i=0; i<val.size1(); i++){
      val(i,0) = _solution(i,0);
      val(i,1) = _solution(i,1); 
      val(i,2) = 0;
    }
  }
};
function *dgCoupling_SW1D_SW2D::computeToIntegrate1New(const function *solution){
  return new computeToIntegrate1(solution);
}

class dgCoupling_SW1D_SW2D::computeToIntegrate2:public function{
  public:
  fullMatrix<double> _solution, _bath, normals;
  computeToIntegrate2(const function *solution, const function *bath):function(3){
    setArgument(_solution, solution);
    setArgument(_bath, bath);
    setArgument(normals,function::getNormals());
  }
  void call(dataCacheMap *m, fullMatrix<double> &val){
    for( int i=0; i<val.size1(); i++){
      val(i,0) = _solution(i, 0) + _bath(i, 0);
      val(i,1) = (_solution(i, 1) * normals(i,0) + _solution(i, 2) * normals(i,1)) / val(i, 0); 
      val(i,2) = (_solution(i, 1) * (-normals(i,1)) + _solution(i, 2) * normals(i,0)) / val(i, 0); 
    }
  }
};
function *dgCoupling_SW1D_SW2D::computeToIntegrate2New(const function *solution, const function *bath){
  return new computeToIntegrate2(solution, bath);
}

class dgCoupling_SW1D_SW2D::computeOutsideSolution1:public function{
  public:
  fullMatrix<double> _integrals;
  double _section0, _width;
  computeOutsideSolution1(const function *integrals, const double section0, const double width):function(3){
    setArgument(_integrals, integrals);
    _section0 = section0;
    _width = width;
  }
  void call(dataCacheMap *m, fullMatrix<double> &val){
    for( int i=0; i<val.size1(); i++){
      val(i,0) = (_integrals(0, 0) - _section0) / _width; // = elevation = (S - int h dx) /   
      val(i,1) = _integrals(0, 1); //velocity (integral on one point change nothing)
      val(i,2) = 0;
    }
  }
};
function *dgCoupling_SW1D_SW2D::computeOutsideSolution1New(const function *integrals, const double section0, const double width){
  return new computeOutsideSolution1(integrals, section0, width);
}

class dgCoupling_SW1D_SW2D::computeOutsideSolution2:public function{
  public:
  fullMatrix<double> _integrals;
  computeOutsideSolution2(const function *integrals):function(3){
    setArgument(_integrals, integrals);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val){
    for( int i=0; i<val.size1(); i++){
      val(i,0) = _integrals(0, 0) ;
      val(i,1) = _integrals(0, 1) / _integrals(0, 0); 
      val(i,2) = _integrals(0, 2) / _integrals(0, 0);
    }
  }
};
function *dgCoupling_SW1D_SW2D::computeOutsideSolution2New(const function *integrals){
  return new computeOutsideSolution2(integrals);
}

void dgDofOverAnotherCollectionFunction::call(dataCacheMap *m, dataCacheDouble *d, fullMatrix<double> &val) {
  int ig, ie;

  switch(m->getMode()) {
    case dataCacheMap::INTEGRATION_GROUP_MODE:
    {
      fullMatrix<double> dof;
      const dgGroupOfElements &group = *m->getGroupOfElements();
      int nbFields = _dofC->getNbFields();
      dof.resize(group.getNbNodes(), group.getNbElements() * nbFields);
      for (size_t i = 0; i < group.getNbElements(); ++i) {
        _dofC->getGroups()->find(m->element(i), ig, ie);
        if(m->getGroupOfInterfaces()) {
          _dofC->getGroups()->find(m->getGroupOfInterfaces()->meshInterfaceElement(i), ig, ie);
        } else {
          _dofC->getGroups()->find(m->element(i), ig, ie); //TODO optim possible with mapping
        }
        for (int j = 0; j < group.getNbNodes(); ++j) {
          for (int k = 0; k < nbFields; ++k) {
            dof(j, k * group.getNbElements() + i) = _dofC->getGroupProxy(ig)(j, ie * nbFields + k);
          }
        }
      }
      val.resize(m->getIntegrationMatrices().getNbIntegrationPoints(), dof.size2(), false);
      m->getIntegrationMatrices().psi().mult(dof, val);
      val.reshape(group.getNbElements() * val.size1(), nbFields);
      return;
    }
    case dataCacheMap::NODE_MODE:
    {
      if(m->getGroupOfInterfaces()) {
        _dofC->getGroups()->find(m->getGroupOfInterfaces()->meshInterfaceElement(m->interfaceId(0)), ig, ie);
      } else {
        _dofC->getGroups()->find(m->element(0), ig, ie); //TODO optim possible with mapping
      }
      _dofC->getGroupProxy(ig).getBlockProxy(ie, val);
      return;
    }
    default:
    {
      Msg::Error("only INTEGRATION_GROUP_MODE and NODE_MODE are implemented in dgDofOverAnotherCollectionFunction !");
    }
  }
}

dgCoupling::dgCoupling(dgFunctionCoupled * f, dataCacheMap::mode m) : _f(f) {
  GModel * model = NULL;
  // check if the same GModel is used
  for(int i = 0; i < _f->nFunction(); ++i) {
    if(model && model != _f->_funcs[i].second->getModel()) Msg::Error("the GModel used by group collections should be identic");
    model = _f->_funcs[i].second->getModel();
  }
  int minDim = 4, maxDim = 0;
  for(int i = 0; i < _f->nFunction(); ++i) {
    const dgGroupCollection * gc = _f->getGroupCollection(i);
    std::map<const dgGroupCollection *, dataCacheMap *>::iterator it = _cacheMaps.find(gc);
    if(it == _cacheMaps.end())
      _cacheMaps[gc] = new dataCacheMap(m, gc);
    _cacheDoubles.push_back(_cacheMaps[gc]->get(_f->getFunction(i)));
    int dim = _f->getGroupCollection(i)->getDim();
    minDim = std::min(minDim, dim);
    maxDim = std::max(maxDim, dim);
  }
  if(maxDim - minDim > 1)
    Msg::Error("this coupling is only between 1D-2D or 2D-3D models");
  _dimFace = minDim;
  for(int i = 0; i < _f->nFunction(); ++i) {
    if(_f->getGroupCollection(i)->getDim() == minDim) {
      _coupledField = new dgDofContainer(*_f->getGroupCollection(i), _f->nFields());
      break;
    }
  }
}

void dgCoupling::compute(std::vector<std::string> physicalTags) {
  
  const dgGroupCollection &mainGC = *_coupledField->getGroups();
  fullMatrix<double> proxy;

  //loop over groups
  for(int iG = 0; iG < mainGC.getNbElementGroups(); ++iG) {
    const dgGroupOfElements &mainG = *mainGC.getElementGroup(iG);

    //select physical tags
    if(! physicalTags.empty() && std::find(physicalTags.begin(), physicalTags.end(), mainG.getPhysicalTag()) == physicalTags.end() )
      continue;

    // set group
    MElement * firstFace = mainG.getElement(0); //TODO robust enough ?
    for(std::map<const dgGroupCollection *, dataCacheMap *>::const_iterator it = _cacheMaps.begin(); it != _cacheMaps.end(); ++it) {
      const dgGroupCollection &gc = *it->first;
      int ig, ie;
      if(gc.getDim() == _dimFace) { // surface (1D or 2D)
        gc.find(firstFace, ig, ie);
        if(ig == -1) Msg::Error("the group collection dont possess specified physical tag");
        it->second->setGroup(gc.getElementGroup(ig));
      } else { // volume (2D or 3D)
        gc.findFace(firstFace, ig, ie); //TODO optimize with static mapping: constant vs nlog(n)
        if(ig == -1) Msg::Error("the group collection dont possess specified physical tag");
        it->second->setInterfaceGroup(gc.getFaceGroup(ig));
      }
    }

    dgFullMatrix<double> &groupProxy = _coupledField->getGroupProxy(iG);

    //loop over elements of the group
    for(size_t iE = 0; iE < mainG.getNbElements(); ++iE) {

      // set element
      MElement * face = mainG.getElement(iE);
      for(std::map<const dgGroupCollection *, dataCacheMap *>::const_iterator it = _cacheMaps.begin(); it != _cacheMaps.end(); ++it) {
        const dgGroupCollection &gc = *it->first;
        int ig, ie;
        if(gc.getDim() == _dimFace) { // surface (1D or 2D)
          gc.find(face, ig, ie);
          if(ig == -1) Msg::Error("the element was not found (illogical?)");
          it->second->setElement(ie);
        } else { // volume (2D or 3D)
          gc.findFace(face, ig, ie); //TODO optimize with static mapping: constant vs nlog(n)
          if(ig == -1) Msg::Error("the element was not found (illogical?)");
          it->second->setInterface(ie);
        }
      }

      // link input results
      for(int iF = 0; iF < _f->nFunction(); ++iF)
        _f->_args[iF].setAsProxy(_cacheDoubles[iF]->get());

      // compute coupling function
      groupProxy.getBlockProxy(iE, proxy);
      _f->call(proxy);
    }
  }
}

const function * dgCoupling::getFunction(const dgGroupCollection * gc) {
  if(gc == _coupledField->getGroups())
    return _coupledField->getFunction();
  std::map<const dgGroupCollection *, function *>::iterator it = _vFunc.find(gc);
  if(it != _vFunc.end())
    return it->second;
  _vFunc[gc] = new dgDofOverAnotherCollectionFunction(_coupledField);
  return _vFunc[gc];
}

//TODO try to merge with functionC (difficulty: _args are evaluated != )
#if defined(HAVE_DLOPEN)
#include <dlfcn.h>
#endif
dgFunctionCoupledC::dgFunctionCoupledC(std::string file, std::string symbol, int nbFields,
                                       std::vector<const function *> f, std::vector<const dgGroupCollection *> g) :
                                       dgFunctionCoupled(f, g, nbFields) {
#if defined(HAVE_DLOPEN)
  void *dlHandler = dlopen(file.c_str(), RTLD_NOW);
  if (!dlHandler)
    Msg::Fatal(dlerror());
  _callback = (void(*)(void))(size_t)dlsym(dlHandler, symbol.c_str());
  if(!_callback)
    Msg::Error("Cannot get the callback to the compiled C function: %s in file '%s'", symbol.c_str(), file.c_str());
#else
  Msg::Error("Cannot construct dgFunctionCoupledC without dlopen");
#endif
}

void dgFunctionCoupledC::call (fullMatrix<double> &val) {
  switch (_args.size()) {
    case 0 : 
      ((void (*)(fullMatrix<double> &))(_callback))(val);
      break;
    case 1 : 
      ((void (*)(fullMatrix<double> &, const fullMatrix<double>&))
       (_callback)) (val, _args[0]);
      break;
    case 2 : 
      ((void (*)(fullMatrix<double> &, const fullMatrix<double>&, 
                 const fullMatrix<double> &))
       (_callback)) (val, _args[0], _args[1]);
      break;
    case 3 : 
      ((void (*)(fullMatrix<double> &, const fullMatrix<double>&, 
                 const fullMatrix<double>&, const fullMatrix<double>&))
       (_callback)) (val, _args[0], _args[1], _args[2]);
      break;
    case 4 : 
      ((void (*)(fullMatrix<double> &, const fullMatrix<double>&,
                 const fullMatrix<double>&, const fullMatrix<double>&,
                 const fullMatrix<double>&))
       (_callback)) (val, _args[0], _args[1], _args[2], _args[3]);
      break;
    case 5 : 
      ((void (*)(fullMatrix<double> &, const fullMatrix<double>&, 
                 const fullMatrix<double>&, const fullMatrix<double>&,
                 const fullMatrix<double>&, const fullMatrix<double>&))
       (_callback)) (val, _args[0], _args[1], _args[2], _args[3], _args[4]);
      break;
    case 6 : 
      ((void (*)(fullMatrix<double> &, const fullMatrix<double>&,
                 const fullMatrix<double>&, const fullMatrix<double>&,
                 const fullMatrix<double>&, const fullMatrix<double>&, 
                 const fullMatrix<double>&))
       (_callback)) (val, _args[0], _args[1], _args[2], _args[3], _args[4], _args[5]);
      break;
    case 7 : 
      ((void (*)(fullMatrix<double> &, const fullMatrix<double>&,
                 const fullMatrix<double>&, const fullMatrix<double>&,
                 const fullMatrix<double>&, const fullMatrix<double>&, 
                 const fullMatrix<double>&, const fullMatrix<double>&))
       (_callback)) (val, _args[0], _args[1], _args[2], _args[3], _args[4], _args[5], _args[6]);
      break;
    case 8 : 
      ((void (*)(fullMatrix<double> &, const fullMatrix<double>&,
                 const fullMatrix<double>&, const fullMatrix<double>&,
                 const fullMatrix<double>&, const fullMatrix<double>&, 
                 const fullMatrix<double>&, const fullMatrix<double>&, 
                 const fullMatrix<double>&))
       (_callback)) (val, _args[0], _args[1], _args[2], _args[3], _args[4], _args[5], _args[6], _args[7]);
      break;
    case 9 : 
      ((void (*)(fullMatrix<double> &, const fullMatrix<double>&,
                 const fullMatrix<double>&, const fullMatrix<double>&,
                 const fullMatrix<double>&, const fullMatrix<double>&, 
                 const fullMatrix<double>&, const fullMatrix<double>&, 
                 const fullMatrix<double>&, const fullMatrix<double>&))
       (_callback)) (val, _args[0], _args[1], _args[2], _args[3], _args[4], _args[5], _args[6], _args[7], _args[8]);
      break;
    default :
      Msg::Error("C _callback not implemented for %i argurments", _args.size());
  }
}


