#ifndef _DG_CONSERVATION_LAW_FUNCTION_H_
#define _DG_CONSERVATION_LAW_FUNCTION_H_

#include "dgConservationLaw.h"

class dataCacheMap;
class function;
class dgConservationLawFunction;
class dgDofContainer;
class dgDofManager;
class dgBoundaryCondition;
class dgTerm;
class dgTermAssembler;
class dgFaceTermAssembler;
class dgInterfacePsiTerm;
class dgIntegrationMatrices;
class dgExtrusion;
class function;
template <class t> class fullMatrix;


#include "GmshMessage.h"
#include <map>

/**
 * A conservation law is defined a convective flux (f), a diffusive flux(g), a source term(r)
 * and a set of boundary conditions.\n
 * \f[\partial_t L(u) = \nabla \cdot (\mathbf{f}(u,\nabla u,forcings)) 
 * + r(u,forcings))\f]
 */
class dgStrongBoundaryCondition{
  public:
  int dim;
  std::string physicalTag;
  const function *f;
  dgStrongBoundaryCondition (int dim_, const std::string physicalTag_, const function *f_): dim(dim_),physicalTag(physicalTag_), f(f_){};
};

class dgConservationLawFunction :public dgConservationLaw{
 public:
  typedef std::map<const std::string, const function*> termMap;
 private:
  std::vector<dgStrongBoundaryCondition> _strongBoundaryConditions;
  typedef std::map<const std::string, dgBoundaryCondition*> bcMap;
  bcMap _boundaryConditions;
  bool _built; //cfr checkSetup
 protected :
  std::vector<int> temporal; //0: untemporal; 1:temporal; 2:mass-lumped
  int _nbf;
  termMap _volumeTerm0;    // DG-term for source
  termMap _volumeTerm1;    // DG-term for convective and diffusive flux
  termMap _interfaceTerm0;
  termMap _interfaceTerm1N;
  termMap _bifurcationTerm0;
  termMap _bifurcationTerm1N;
  termMap _maximumConvectiveSpeed;
  termMap _diffusivity;
  termMap _clipToPhysics;
  std::map<const std::string, std::pair<const function*, bool> > _massFactor; // the boolean indicates if the term is constant in time
  static inline const function * functionForTag(const termMap &map, const std::string tag)
  {
    termMap::const_iterator it = map.find(tag);
    if (it == map.end()) it = map.find("");
    if (it == map.end()) return NULL;
    return it->second;
  }
 public:

  int isTemporal(int iField) const { return temporal[iField]; }

  // Constructor - Setup - Destructor
  dgConservationLawFunction(int nbf) {
    _nbf = nbf;
    _built = false;
    temporal.resize(nbf);
    for (size_t k = 0; k < temporal.size(); k++) temporal[k] = true;
  }
  virtual void setup() {}
  virtual ~dgConservationLawFunction() {}

  // Check setup
  inline void checkSetup () const {if(!_built) { const_cast<dgConservationLawFunction*>(this)->setup(); const_cast<dgConservationLawFunction*>(this)->_built = true;}}
  inline void checkNonSetup () {if(_built) Msg::Fatal("this conservation law is already _built, you cannot set this parameter now");}

  // Get information
  /** Return the number of fields composing the unknowns of this conservation law.
   * For vectorial fields, each components is counted as one field.
   */
  int getNbFields() const {return _nbf;}
  const function *getClipToPhysics(const std::string tag) const;
  const function *getInterfaceTerm0(const std::string tag) const;
  const function *getdiffusivity(const std::string tag) const;
  const function *getMaximumConvectiveSpeed(const std::string tag) const;

  /** Associate a mesh tag with a boundary condition.*/
  void addBoundaryCondition(std::string tag, dgBoundaryCondition *condition);
  void addBoundaryCondition(const std::vector<std::string> tags, dgBoundaryCondition *condition);
  dgBoundaryCondition *getBoundaryCondition(const std::string tag) const;
  virtual const function *getOutsideValue(const dgGroupOfFaces*, const function *in) const;
  bool haveFaceTerm(const dgGroupOfFaces &faces) const;

  /** Add a strong BC to a physical entity.
   * Give tag and dim and constant function (value_field0, value_field1,..., flag_field0, flag_field1,...). 
   * Fields with flags >0 are fixed
   * Strong boundary condition are not implemented in Discontinuous Galerkin
   */
  void addStrongBoundaryCondition (int dim, const std::string tag, const function *f);

  /**
   * Create a new boundary condition based on external data.
   * The interface term with the provided tag computes the fluxes using the provided values as external solution");
  */
  dgBoundaryCondition *newOutsideValueBoundary(const std::string tag, const function *outsideValueFunction);
  dgBoundaryCondition *newOutsideValueBoundary(const function *outsideValueFunction)
    {return newOutsideValueBoundary("", outsideValueFunction);}

  /**
   * Create a new boundary condition based on external data. Generic version
   * The interface term with the provided tag computes the fluxes using the provided values as external solution");
  */
  dgBoundaryCondition *newOutsideValueBoundaryGeneric(const std::string tag, const std::vector<const function*> toReplace,
                                                                             const std::vector<const function*> replaceBy);
  dgBoundaryCondition *newOutsideValueBoundaryGeneric(const std::vector<const function*> toReplace, const std::vector<const function*> replaceBy)
    {return newOutsideValueBoundaryGeneric("", toReplace, replaceBy);}

  /**
   * Create a new boundary condition based on external data. Generic version, with the 2 which could depend on the 1
   * The interface term with the provided tag computes the fluxes using the provided values as external solution");
  */
  dgBoundaryCondition *newOutsideValueBoundaryGeneric(const std::string tag,
                                                      const std::vector<const function*> toReplace1,
                                                      const std::vector<const function*> replaceBy1,
                                                      const std::vector<const function*> toReplace2,
                                                      const std::vector<const function*> replaceBy2);
  dgBoundaryCondition *newOutsideValueBoundaryGeneric(const std::string tag,
                                                      const std::map<const function*, const function*> *map,
                                                      const std::map<const function*, const function*> *map2=NULL);
  /**
   * Create a new boundary condition with a given flux (no other fluxes will be computed)
   */
  dgBoundaryCondition *newNeumannBoundary(const function *fluxFunction);

  /**
  * Create a new boundary condition which set to 0 all the fluxes through the interfaces.
  */
  dgBoundaryCondition *new0FluxBoundary(const std::string tag="");

  /**
  * Create a new boundary condition using the values computed inside the domain as boundary values. The interface term with the provided tag computes the fluxes using the internal values as internal AND external values.
  */
  dgBoundaryCondition *newSymmetryBoundary(const std::string tag = "");

public:
  virtual dgTermAssembler *getTerm(const dgGroupOfElements &group) const;
  virtual dgFaceTermAssembler *getFaceTerm(const dgGroupOfFaces &group) const;
  
  /** Return a fullMatrix list of the group's elements' innerRadii. */
  static fullMatrix<double> innerRadius(dataCacheMap *m);
  /** Return a fullMatrix list of the group's elements' maxEdges. */
  static fullMatrix<double> maxEdge(dataCacheMap *m);
  static fullMatrix<double> muFactor(dataCacheMap *m);
  void computeElementMass(const dgGroupOfElements &group, double t, dgDofContainer *du, dgDofContainer *rhs, double alpha, dgDofManager *dof) const;
  void computeElement(const dgGroupOfElements &group, double t, dgDofContainer &solution, dgDofContainer &residual, dgDofManager *dofManager, dgDofContainer *variable = NULL) const;
  void computeFace(const dgGroupOfFaces &faces, double t, dgDofContainer &solutionDof, dgDofContainer &residual,  dgDofManager *jacDof, dgDofContainer *variable = NULL) const;
  void computeFaceMass(const dgGroupOfFaces &faces, double t, dgDofContainer *du, dgDofContainer *rhs, double alpha, dgDofManager *dof) const;
  void computeElementaryTimeSteps (const dgGroupOfElements & group, // the group
                                          dgDofContainer *solution, // the solution 
                                          std::vector<double> & DT, // elementary time steps
                                          dgExtrusion *extrusion = NULL) const;
  /** Maximum global explicit time step associated with the CFL condition. 
      Parameter extrusion should be provided when the mesh was built from an extrusion 
      of a 2d mesh and horizontal/vertical phenomena have different timescales. */
  double getMinOfTimeSteps(dgDofContainer *solution, dgExtrusion *extrusion = NULL) const;
  void fixStrongBC(dgDofManager &dof, dgDofContainer *oldSolution = NULL, double alpha = 1.) const;
  const function * getMassFactor(const std::string tag, bool &constMass) const;
  massFactorType haveMassFactor(const dgGroupOfElements &group) const;
};


/**
 * A weak boundary condition. 
 * Boundary conditions should be associated with tag using dgConservationLawFunction::addBoundaryCondition.
 */
class dgBoundaryCondition {
  protected :
  dgConservationLawFunction *_claw;
  const function *_term0, *_term1N;
  std::string _type;
  std::map<const function*,const function*> _outsideValue;
 public: 

  // Constructor - Destructor
  dgBoundaryCondition () {
    _term0 = NULL;
    _term1N = NULL;
    _type = "undefined";
  }
  virtual ~dgBoundaryCondition () {}

  // Get information
  virtual const function * getTerm0 () { return _term0; }
  virtual const function * getTerm1N () { return _term1N; }
  virtual const function * getOutsideValue (const function* f) { return _outsideValue[f]; } 
  inline std::string getType() const { return _type; }
};

/*==============================================================================
 * IP Term
 *============================================================================*/

function *dgNewIpTerm (int nbFields, const function *diffusiveFlux, const function *diffusivity, bool _useSip=true);
function *dgNewIpTermAnisotropic(int nbFields, const function *diffusiveFlux, const function *ipPenalty);
function *dgNewIpTermErnRef(int nbFields, const function *diffusiveFlux,const function *ipPenalty);
function *dgNewSymmetricIpTerm (int nbFields, const function *diffusivity);
function *dgNewSymIpTerm3d(int nbFields, const function *diffusivity);
#endif










