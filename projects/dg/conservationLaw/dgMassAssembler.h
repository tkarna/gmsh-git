#ifndef _DG_MASS_ASSEMBLER_H
#define _DG_MASS_ASSEMBLER_H

class dgMassAssembler
{
  const dgConservationLaw &_claw;
  dgDofContainer *_Rhs, *_dU;
  dgDofManager &_dof;
  double _alpha;
  bool _addToLhs;
 public:
  dgMassAssembler(const dgConservationLaw &claw, dgDofContainer *Rhs, dgDofContainer *dU, dgDofManager &dof, double alpha, bool addToLhs);
  //Rhs -= M * dU, dof += M * alpha
  void assemble(const dgGroupOfElements &groupI, const dgGroupOfElements &groupJ, int iElem, int jElem, dgFullMatrix<double> &M);
  // mass is of size nbGroupNodes*nbGroupNodes, nbFields, nbFaces * nbCon * nbCon
  // blocks are ordered as : iFace * nbCon * nbCon + iCon * nbCon + jCon;
  void assembleFaces(const dgGroupOfFaces &faces, dgFullMatrix<double> &mass);
};

#endif
