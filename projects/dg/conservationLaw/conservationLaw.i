%module conservationLaw
%include "std_vector.i";
%import(module="dgpy.dgCommon") "dgCommon.i"

%{
  #undef HAVE_DLOPEN
  #include "dgConservationLaw.h"
  #include "dgConservationLawFunction.h"
  #include "dgCoupling.h"
%}

%import(module="dgpy.dgFunction") "function.h";
namespace std {
%template(VectorFunctionConst) vector<const function*, std::allocator<const function*> >;
}


%rename(_in) "in";
%include "dgCoupling.h"
%include "dgConservationLaw.h"
%include "dgConservationLawFunction.h"
