#include "dgConservationLaw.h"
#include "dgGroupOfElements.h"
#include "dgDofContainer.h"

void dgConservationLaw::computeAllTerms(double t, dgDofContainer &solution, dgDofContainer &residual, bool scatterSolutionDof) const
{
  if ( scatterSolutionDof )
    solution.scatterBegin();
  residual.setAll(0.);
  const dgGroupCollection &groups = *residual.getGroups();
  for (int i=0; i < groups.getNbElementGroups(); i++) {
    const dgGroupOfElements &group = *groups.getElementGroup(i);
    computeElement(group, t, solution, residual, NULL);
  }
  for(int i=0;i < groups.getNbFaceGroups() ; i++) {
    dgGroupOfFaces &group = *groups.getFaceGroup(i);
    bool oneGhost = false;
    for (int j = 0; j < group.nConnection(); ++ j) {
      oneGhost |= (group.elementGroup(j).getGhostPartition() >= 0);
    }
    if (!oneGhost)
      computeFace(group, t, solution, residual, NULL);
  }
  if ( scatterSolutionDof )
    solution.scatterEnd();
  for(int i = 0; i < groups.getNbFaceGroups(); i++) {
    dgGroupOfFaces &group = *groups.getFaceGroup(i);
    bool oneGhost = false;
    for (int j = 0; j < group.nConnection(); ++ j) {
      oneGhost |= (group.elementGroup(j).getGhostPartition() >= 0);
    }
    if (oneGhost){
      computeFace(group, t, solution, residual, NULL);
    }
  }
}

