#ifndef _DG_TERM_H_
#define _DG_TERM_H_
#define EPSILON_FINITE_DIFFERENCE 1e-8
#include "fullMatrix.h"
#include "dgFullMatrix.h"
#include "functionDerivator.h"
#include "dgGroupOfElements.h"
class dgDofManager;

class dgTerm {
 public:
  virtual void computeElement(dataCacheMap &cache, fullMatrix<double> &residual, fullMatrix<double> *jac = NULL) = 0;
  virtual ~dgTerm();
};

class dgInterfaceTerm {
 public:
  virtual void computeFace(dataCacheMap &cache, fullMatrix<double> &residual, std::vector<fullMatrix<double> > *jacSol = NULL, std::vector<fullMatrix<double> > *jacGradSol = NULL) = 0;
  virtual ~dgInterfaceTerm();
};

class dgIntegralTerm {
 public :
  static dgTerm *newPsiTerm(const function *f, int nbFields, bool isLinear);
  static dgTerm *newGradPsiTerm(const function *f, int nbFields, bool isLinear);
  // linearize f before integration (same as interpolate)
  static dgTerm *newPsiTermNodal(const function *f, int nbFields, bool isLinear);
  static dgTerm *newGradPsiTermNodal(const function *f, int nbFields, bool isLinear);
  static dgTerm *newLowOrderFPsiTerm(const function *f, int nbFields, bool isLinear, int order, bool onlyVertical);
  static dgInterfaceTerm *newInterfacePsiTerm(const function *f, bool isLinear);
};



/*class dgInterfacePsiTerm {
 private:
  std::vector<functionDerivator*> _derivatorSol, _derivatorGradSol;
  std::vector<const dgIntegrationMatrices*> _clIntMatrices;

  //For lower order terms
  std::vector<int> _orders;
  std::vector< fullMatrix<double> > _highIntegToLowInteg, _dFlm_dFhk;
  fullMatrix<double> _toNodesMatSolLO, _psiW_Jk, _dFlm_dFhk_psiW_Jk, _dSol;
  dgFullMatrix<double> _termJac;
  int _nbOrders;
  void _copyTerm(const fullMatrix<double> &in, fullMatrix<double> &out);

  const fullMatrix<double>  *_toNodesMatSol;
  fullMatrix<double>  _proxy;
  dataCacheDouble *_term;
  dataCacheMap *_cacheMap;
  int _nbFields;
  int _nbConnections, _nbConnectionsInFlux;
  dgFullMatrix<double> _termAtQP;
  const dgGroupOfFaces *_group;
  bool _isLinear;
  const function *_f;
  public :
  std::vector<dgFullMatrix<double> > _dGradSolAtQP;
  dgFullMatrix<double> _dSolAtQP;
  int getNbConnectionsInFlux();
  dgInterfacePsiTerm(const function *f, int nbFields, bool isLinear, const std::vector<int>& orders = std::vector<int>());
  virtual void initGroup(dataCacheMap &cacheMap, const dgGroupOfFaces &group, bool jacobianNeeded);
  void computeFace(int iFace);
  void redistributeGroup(fullMatrix<double> &residual);
  void redistributeGroup(fullMatrix<double> &residual, dgFullMatrix<double> &jacSol, std::vector<dgFullMatrix<double> > &jacGradSol);
  virtual ~dgInterfacePsiTerm();
};*/

class dgMassAssembler;
class dgTermAssembler {
  public:
  virtual void compute(const dgGroupOfElements &elements, double t, dgDofContainer &solution, dgDofContainer &residual, dgDofManager *jacDof = NULL, dgFullMatrix<double> *mass = NULL, dgDofContainer *variable = NULL) = 0;
  virtual ~dgTermAssembler(){}
};

class dgFaceTermAssembler {
 public:
  static void mapFromInterface (const dgGroupOfFaces &faces, int nFields, const fullMatrix<double> &v, std::vector< fullMatrix<double> *> &proxies);
  virtual void compute(const dgGroupOfFaces &faces, double t, dgDofContainer &solution, dgDofContainer &residual, dgDofManager *jacDof = NULL, dgDofContainer *variable = NULL) = 0;
  virtual ~dgFaceTermAssembler(){}
};

class dgTermAssemblerIntegral : public dgTermAssembler {
  std::vector<dgTerm*> _terms;
public:
  void compute(const dgGroupOfElements &elements, double t, dgDofContainer &solution, dgDofContainer &residual, dgDofManager *jacDof, dgFullMatrix<double> *mass, dgDofContainer *variable);
  dgTermAssemblerIntegral(std::vector<dgTerm*> terms): _terms(terms){}
  ~dgTermAssemblerIntegral();
};

class dgFaceTermAssemblerIntegral : public dgFaceTermAssembler {
  dgInterfaceTerm *_term;
  int _nbFields;
  public:
  void compute(const dgGroupOfFaces &faces, double t, dgDofContainer &solution, dgDofContainer &residual, dgDofManager *jacDof, dgDofContainer *variable);
  dgFaceTermAssemblerIntegral(dgInterfaceTerm *term, int nbFields);
  ~dgFaceTermAssemblerIntegral();
};

// ====================================================================================

class dgTermAssemblerControlVolumeFE : public dgTermAssembler {
  const function *_nodal, *_volume, *_height;
  dgTermAssembler* _classicTermAssembler;
  int _nbf;

  static void computeResidual(dataCacheMap &cache, const fullMatrix<double> &termI, const fullMatrix<double> &termN,
                                            const fullMatrix<double> &var, const fullMatrix<double> &height, fullMatrix<double> &out);
  static void getIntegralPart(dataCacheMap &cache, int iElement, const fullMatrix<double> &termQP, fullMatrix<double> &termN);
 public:
  void compute(const dgGroupOfElements &elements, double t, dgDofContainer &solution, dgDofContainer &residual, dgDofManager *jacDof, dgFullMatrix<double> *mass, dgDofContainer *variable);
  dgTermAssemblerControlVolumeFE(int nbFields, const function * nodal, const function * volume, const function * height, dgTermAssembler * termAssembler) : 
      _nodal(nodal), _volume(volume), _height(height), _classicTermAssembler(termAssembler), _nbf(nbFields) {}
  ~dgTermAssemblerControlVolumeFE() {}
};

class dgFaceTermAssemblerControlVolumeFE : public dgFaceTermAssembler {
  const function *_term;
  int _nbf;
  static void computeResidual(dataCacheMap &cache, const fullMatrix<double> &termN, fullMatrix<double> &term);
  static void getIntegralPart(dataCacheMap &cache, int iFace, fullMatrix<double> &termN);
 public:
  void compute(const dgGroupOfFaces &faces, double t, dgDofContainer &solution, dgDofContainer &residual, dgDofManager *jacDof, dgDofContainer *variable); 
  dgFaceTermAssemblerControlVolumeFE(const function *term, int nbFields) : _term(term), _nbf(nbFields) {}
  ~dgFaceTermAssemblerControlVolumeFE() {}
};

#endif
