#include "dgTerm.h"
#include "dgMeshJacobian.h"
#include "function.h"
#include "dgMesh.h"
#include "dgInterfaceVector.h"
#include "dgIntegrationMatrices.h"
#include "dgDofContainer.h"
#include "dgDofManager.h"
#include "dgMassAssembler.h"
#include "functionGeneric.h"
#include "MElement.h"
#include "legendrePolynomials.h"
#include "ElementType.h"



// A+= B * C, memory of A is allocated if A is empty;
static void gemmAndAllocate(fullMatrix<double> &A, const fullMatrix<double> &B, const fullMatrix<double> &C, int innerSize = -1)
{
  if (innerSize == -1)
    innerSize = B.size2();
  fullMatrix<double> pB;
  pB.setAsProxy(B);
  pB.reshape(B.size1() * B.size2() / innerSize, innerSize);
  fullMatrix<double> pC;
  pC.setAsProxy(C);
  pC.reshape(innerSize, C.size1() * C.size2() /innerSize);
  if (A.size1() == 0) {
    A.resize(pB.size1(), pC.size2(), false);
    A.gemm(pB, pC, 1., 0.);
  }
  else {
    fullMatrix<double> pA;
    pA.setAsProxy(A);
    pA.reshape(pB.size1(), pC.size2());
    pA.gemm(pB, pC, 1., 1.);
  }
}

dgTerm::~dgTerm() {}
dgInterfaceTerm::~dgInterfaceTerm() {}

class dgPsiTerm : public dgTerm {
  bool _isLinear;
  const function *_f;
public:
  dgPsiTerm(const function *f, bool isLinear) {
    _f = f;
    _isLinear = isLinear;
  }
  void computeElement(dataCacheMap &cache, fullMatrix<double> &residual, fullMatrix<double> *jac)
  {
    dataCacheDouble &term = *cache.get(_f);
    const dgIntegrationMatrices &integ = cache.getIntegrationMatrices();
    const dgMeshJacobian &meshj = cache.getJacobians();
    residual.gemm(integ.psiW(), meshj.multJElement(cache.getGroupId(), term.get()));
    if (jac && term.doIDependOn(function::getSolution())) {
      functionDerivator derivator(term, *cache.get(function::getSolution()), _isLinear ? 1 : EPSILON_FINITE_DIFFERENCE);
      gemmAndAllocate(*jac, integ.psiPsiW(), meshj.multJElement(cache.getGroupId(), derivator.compute()));
    }
    if (jac && term.doIDependOn(function::getSolutionGradient())) {
      functionDerivator derivatorGradSol(term, *cache.get(function::getSolutionGradient()), _isLinear ? 1 : EPSILON_FINITE_DIFFERENCE);
      gemmAndAllocate(*jac, integ.dPsiPsiW(), meshj.multDXiDXJRightElement(cache.getGroupId(), derivatorGradSol.compute()));
    }
  }
};

class dgGradPsiTerm : public dgTerm {
  bool _isLinear;
  const function *_f;
  public:
  dgGradPsiTerm(const function *f, bool isLinear) {
    _f = f;
    _isLinear = isLinear;
  }
  void computeElement(dataCacheMap &cache, fullMatrix<double> &residual, fullMatrix<double> *jac)
  {
    dataCacheDouble &term = *cache.get(_f);
    const dgIntegrationMatrices &integ = cache.getIntegrationMatrices();
    const dgMeshJacobian &meshj = cache.getJacobians();
    residual.gemm(integ.dPsiW(), meshj.multDXiDXJElement(cache.getGroupId(), term.get()));
    if (jac && term.doIDependOn(function::getSolution())) {
      functionDerivator derivator(term, *cache.get(function::getSolution()), _isLinear ? 1 : EPSILON_FINITE_DIFFERENCE);
      gemmAndAllocate(*jac, integ.psiDPsiW(),meshj.multDXiDXJElement(cache.getGroupId(), derivator.compute()));
    }
    if (jac && term.doIDependOn(function::getSolutionGradient())) {
      functionDerivator derivatorGradSol(term, *cache.get(function::getSolutionGradient()), _isLinear ? 1 : EPSILON_FINITE_DIFFERENCE);
      gemmAndAllocate(*jac, integ.dPsiDPsiW(), meshj.multDXiDXDXiDXJElement(cache.getGroupId(), derivatorGradSol.compute()));
    }
  }
};

class dgInterfacePsiTerm:public dgInterfaceTerm {
  const function *_f;
  bool _isLinear;
  public:
  dgInterfacePsiTerm(const function *f, bool isLinear) {
    _isLinear = isLinear;
    _f = f;
  }
  void computeFace(dataCacheMap &cache, fullMatrix<double> &residual, std::vector<fullMatrix<double> >*jacSol = NULL, std::vector<fullMatrix<double> > *jacGradSol = NULL)
  {
    dataCacheDouble &term = *cache.get(_f);
    const dgGroupOfFaces &group = *cache.getGroupOfInterfaces();
    const dgMeshJacobian &meshj = cache.getJacobians();
    gemmAndAllocate(residual, cache.getIntegrationMatrices().psiW(), meshj.multJInterface(group.interfaceVectorId(), group.interfaceId(), term.get()));
    if (jacSol) {
      jacSol->resize(group.nConnection());
      for (int i = 0; i < group.nConnection(); i++) {
        if (term.doIDependOn(function::getSolution(), i)) {
          functionDerivator derivator(term, *cache.getSecondaryCache(i)->get(function::getSolution()), _isLinear ? 1 : EPSILON_FINITE_DIFFERENCE);
          const fullMatrix<double> &dSolAtQP = meshj.multJInterface(group.interfaceVectorId(),group.interfaceId(), derivator.compute());
          gemmAndAllocate((*jacSol)[i], cache.getIntegrationMatrices().psiPsiW(), dSolAtQP);
        }
      }
    }
    if (jacGradSol) {
      jacGradSol->resize(group.nConnection());
      for (int i = 0; i < group.nConnection(); i++) {
        if (term.doIDependOn(function::getSolutionGradient(), i)) {
          functionDerivator derivator(term, *cache.getSecondaryCache(i)->get(function::getSolutionGradient()), _isLinear ? 1 : EPSILON_FINITE_DIFFERENCE);
          const fullMatrix<double> &dGradSolAtQP = meshj.multDXiDXJRightInterface(group.interfaceVectorId(), i, group.interfaceId(), derivator.compute());
          gemmAndAllocate((*jacGradSol)[i], cache.getSecondaryCache(i)->getIntegrationMatrices().dPsiPsiW(), dGradSolAtQP);
        }
      }
    }
  }
};

/*class dgPsiTermNodal : public dgPsiTerm {
  public:
    virtual void computeElement(int iElement) {
      const dgIntegrationMatrices &intMatrices = _cacheMap->getIntegrationMatrices();
      _termAtQP.getBlockProxy(iElement, _proxy);
      intMatrices.psi().mult(_term->get(), _proxy);
      //toParametricR(iElement, _proxy, _proxy);
      if (_derivatorSol) {
        _dSolAtQP.getBlockProxy(iElement, _proxy);
        intMatrices.psi().mult(_derivatorSol->compute(), _proxy);
        //toParametricJSol(iElement, _proxy, _proxy);
      }
      if (_derivatorGradSol) {
        _dGradSolAtQP.getBlockProxy(iElement, _proxy);
        intMatrices.psi().mult(_derivatorGradSol->compute(), _proxy);
        //toParametricJGrad(iElement, _proxy, _proxy);
      }
    }
    dgPsiTermNodal(const function *f, int nbFields, bool isLinear) : dgPsiTerm(f, nbFields, isLinear) {}
};*/

/*class dgGradPsiTermNodal : public dgGradPsiTerm {
  public:
  virtual void computeElement(int iElement) {
  const dgIntegrationMatrices &intMatrices = _cacheMap->getIntegrationMatrices();
  _termAtQP.getBlockProxy(iElement, _proxy);
  intMatrices.psi().mult(_term->get(), _proxy);
//toParametricR(iElement, _proxy, _proxy);
if (_derivatorSol) {
_dSolAtQP.getBlockProxy(iElement, _proxy);
intMatrices.psi().mult(_derivatorSol->compute(), _proxy);
//toParametricJSol(iElement, _proxy, _proxy);
}
if (_derivatorGradSol) {
_dGradSolAtQP.getBlockProxy(iElement, _proxy);
intMatrices.psi().mult(_derivatorGradSol->compute(), _proxy);
//toParametricJGrad(iElement, _proxy, _proxy);
}
}
dgGradPsiTermNodal(const function *f, int nbFields, bool isLinear) : dgGradPsiTerm(f, nbFields, isLinear) {}
};*/

class dgLowOrderFPsiTerm : public dgTerm {
  protected:
    bool _onlyVertical;
    int _order;
    const function *_f;
    int _nbFields;
    bool _isLinear;

  fullMatrix<double> buildLowOrderMatrix(const dgIntegrationMatrices &intMatricesIn)
  {
    int elementBaseType = ElementType::ParentTypeFromTag(intMatricesIn.elementType());
    int orderFull = ElementType::OrderFromTag(intMatricesIn.elementType());
    int dim = ElementType::DimensionFromTag(intMatricesIn.elementType());
    int lowOrderElementType = ElementType::getTag(elementBaseType, _order, false);
    int nQP = intMatricesIn.getNbIntegrationPoints();

    fullMatrix<double> *invMassOut;
    fullMatrix<double> *psiOut;
    fullMatrix<double> *psiWOut;
    if (!_onlyVertical){
      const dgIntegrationMatrices &intMatricesOut = dgIntegrationMatrices::get(lowOrderElementType, intMatricesIn.getIntegrationOrder());
      invMassOut = (fullMatrix<double> *)&intMatricesOut.massInverse();
      psiOut = (fullMatrix<double> *)&intMatricesOut.psi();
      psiWOut = (fullMatrix<double> *)&intMatricesOut.psiW();
    }else{
      if (elementBaseType != TYPE_QUA && elementBaseType != TYPE_HEX){
        Msg::Fatal("Vertical low order terms currently only implemented for quads and hexahedrons");
      }
      const fullMatrix<double> &xi = intMatricesIn.integrationPoints();
      const fullVector<double> &w = intMatricesIn.integrationWeights();
      LegendrePolynomials leBasis(orderFull);

      //Computation of the Legendre basis functions
      double *basisVal[3];
      int nbI=1, nbJ=1, nbK=1;
      switch (dim) {
        case 2:
          nbI = orderFull+1;
          nbJ = _order + 1;
          break;
        case 3:
          nbI = orderFull+1;
          nbJ = orderFull+1;
          nbK = _order + 1;
          break;
        default:
          Msg::Fatal("Low order vertical term not implemented for dimension %i",dim);
          break;
      }
      psiOut = new fullMatrix<double>(nQP, nbI*nbJ*nbK);
      psiWOut = new fullMatrix<double>(nbI*nbJ*nbK, nQP);
      for (int iDim = 0; iDim < 3; iDim++){
        basisVal[iDim] = new double[psiOut->size2()];
        for (int i = 0; i < psiOut->size2(); i++){
          basisVal[iDim][i] = 1;
        }
      }
      for (int iPt = 0; iPt < psiOut->size1(); iPt++){
        for (int iDim = 0; iDim < dim; iDim++){
          leBasis.f(xi(iPt,iDim),basisVal[iDim]);
        }
        for (int i = 0; i < nbI; i++){
          for (int j = 0; j < nbJ; j++){
            for (int k = 0; k < nbK; k++){
              psiOut->set(iPt, i + nbI * j + nbI * nbJ * k, basisVal[0][i] * basisVal[1][j] * basisVal[2][k]);
              psiWOut->set(i + nbI * j + nbI * nbJ * k, iPt, (*psiOut)(iPt, i + nbI * j + nbI * nbJ * k) * w(iPt));
            }
          }
        }
      }
      for (int iDim = 0; iDim < 3; iDim++){
        delete[] basisVal[iDim];
      }

      //Computation of the mass matrices. We neglect the jacobian of the transformation here
      invMassOut = new fullMatrix<double>(psiOut->size2(), psiOut->size2());
      invMassOut->setAll(0);
      for (int iPt = 0; iPt < psiOut->size1(); iPt++){
        for (int i = 0; i < psiOut->size2(); i++){
          for (int j = 0; j < psiOut->size2(); j++){
            (*invMassOut)(i,j) += (*psiOut)(iPt, i) * (*psiOut)(iPt, j) * w(iPt);
          }
        }
      }
      invMassOut->invertInPlace();
    }

    // Matrix of the L2 projection
    fullMatrix<double> invMassPsiWOut(invMassOut->size1(), psiWOut->size2());
    invMassOut->mult(*psiWOut, invMassPsiWOut);
    fullMatrix<double> highIntegToLowInteg(nQP, invMassPsiWOut.size2());
    psiOut->mult(invMassPsiWOut, highIntegToLowInteg);

    if (_onlyVertical){
      delete psiOut;
      delete psiWOut;
      delete invMassOut;
    }
    return highIntegToLowInteg;
  }

  fullMatrix<double> buildLowOrderJacobianMatrix(const dgIntegrationMatrices &intMatricesIn, const fullMatrix<double> &highIntegToLowInteg)
  {
    const fullMatrix<double> &psi = intMatricesIn.psi();
    int nPsi = psi.size2();
    int nQP = intMatricesIn.getNbIntegrationPoints();
    fullMatrix<double> highIntegToLowIntegPsi(nQP * nPsi, nQP);
    for (int iQP = 0; iQP < nQP; ++iQP) {
      for (int iPsi = 0; iPsi < nPsi; ++iPsi) {
        for (int jQP = 0; jQP < nQP; ++jQP) {
          highIntegToLowIntegPsi(iQP + iPsi * nQP, jQP) = highIntegToLowInteg(iQP, jQP) * psi(jQP, iPsi);
        }
      }
    }
    return highIntegToLowIntegPsi;
  }

  void computeElement(dataCacheMap &cache, fullMatrix<double> &residual, fullMatrix<double> *jac) {
    fullMatrix<double> highIntegToLowInteg = buildLowOrderMatrix(cache.getIntegrationMatrices());

    dataCacheDouble &term = *cache.get(_f);
    {
      fullMatrix<double> termAtQP;
      gemmAndAllocate(termAtQP, highIntegToLowInteg, term.get(), highIntegToLowInteg.size2());
      termAtQP.reshape(-1, term.get().size2());
      residual.gemm(cache.getIntegrationMatrices().psiW(), cache.getJacobians().multJElement(cache.getGroupId(), termAtQP));
    }

    if (jac &&term.doIDependOn(function::getSolution())) {
      fullMatrix<double> highIntegToLowIntegPsi = buildLowOrderJacobianMatrix(cache.getIntegrationMatrices(), highIntegToLowInteg);
      functionDerivator derivator(term, *cache.get(function::getSolution()), _isLinear ? 1 : EPSILON_FINITE_DIFFERENCE);
      fullMatrix<double> termAtQP;
      gemmAndAllocate(termAtQP, highIntegToLowIntegPsi, derivator.compute(), highIntegToLowInteg.size2());
      termAtQP.reshape(-1, _nbFields * _nbFields);
      fullMatrix<double> tmp;
      gemmAndAllocate(tmp, cache.getIntegrationMatrices().psiW(), cache.getJacobians().multJElement(cache.getGroupId(), termAtQP));
      int nQP = highIntegToLowInteg.size1();
      int nPsi = highIntegToLowIntegPsi.size1() / nQP;
      tmp.reshape(nPsi * nPsi, -1);
      jac->add(tmp);
    }
    if (term.doIDependOn(function::getSolutionGradient())) {
      Msg::Fatal("integralLowOrderFTerm should not be used with terms depending on solution gradient");
    }
  }
  public:
    dgLowOrderFPsiTerm(const function *f, int nbFields, bool isLinear, int order, bool onlyVertical)
  {
    _order = order;
    _nbFields = nbFields;
    _f = f;
    _isLinear = isLinear;
    _onlyVertical = onlyVertical;
  }
};

dgTerm *dgIntegralTerm::newLowOrderFPsiTerm(const function *f, int nbFields, bool isLinear, int order, bool onlyVertical){
  return new dgLowOrderFPsiTerm(f, nbFields, isLinear, order, onlyVertical);
}


dgTerm *dgIntegralTerm::newPsiTerm(const function *f, int nbFields, bool isLinear)
{
  return new dgPsiTerm(f, isLinear);
}

dgInterfaceTerm *dgIntegralTerm::newInterfacePsiTerm(const function *f, bool isLinear)
{
  return new dgInterfacePsiTerm(f, isLinear);
}

dgTerm *dgIntegralTerm::newGradPsiTerm(const function *f, int nbFields, bool isLinear)
{
  return new dgGradPsiTerm(f, isLinear);
}

dgTerm *dgIntegralTerm::newPsiTermNodal(const function *f, int nbFields, bool isLinear)
{
  return NULL;//new dgPsiTermNodal(f, nbFields, isLinear);
}

dgTerm *dgIntegralTerm::newGradPsiTermNodal(const function *f, int nbFields, bool isLinear)
{
  return NULL;//new dgGradPsiTermNodal(f, nbFields, isLinear);
}




void dgTermAssemblerIntegral::compute(const dgGroupOfElements &group, double t, dgDofContainer &solution, dgDofContainer &residual, dgDofManager *jacDof, dgFullMatrix<double> *massMatrix, dgDofContainer *variable)
{
  if (massMatrix)
    Msg::Fatal("mass matrix is no more in dgTermAssemblerIntegral");
  //create and populate the CacheMap
  dataCacheMap cacheMap(dataCacheMap::INTEGRATION_GROUP_MODE, residual.getGroups());
  cacheMap.setSolutionFunction(solution.getFunction(), solution.getFunctionGradient());
  if(variable)
    cacheMap.setVariableFunction(variable->getFunction(), variable->getFunctionGradient());
  function::getTime()->set(t);
  cacheMap.setGroup(&group);
  if (jacDof) {
    dgFullMatrix<double> jacobianK;
    for (size_t i = 0; i < _terms.size(); ++i)
      _terms[i]->computeElement(cacheMap, residual.getGroupProxy(&group), &jacobianK);
    if (jacobianK.size1() != 0){
      jacDof->reorderAndAssembleGroupJacobian(group, jacobianK);
    }
  }
  else {
    for (size_t i = 0; i < _terms.size(); ++i)
      _terms[i]->computeElement(cacheMap, residual.getGroupProxy(&group));
  }
}


// size func either 1 so symetric, either prop nbConnections
void dgFaceTermAssembler::mapFromInterface (const dgGroupOfFaces &faces, int nFields, const fullMatrix<double> &v, std::vector< fullMatrix<double> *> &proxies)
{
  const int nbConnections = faces.nConnection();
  if (nbConnections == 2 && (size_t)v.size2() == (size_t)nFields * faces.size()) { // symmetric flux
    for (size_t i = 0; i < faces.size(); ++i) {
      const std::vector<int> &closure0 = faces.closure(i, 0);
      const std::vector<int> &closure1 = faces.closure(i, 1);
      const int eid0 = faces.elementId(i, 0);
      const int eid1 = faces.elementId(i, 1);
      for (int iField = 0; iField < nFields; ++iField){
        double *data0 = &(*proxies[0])(0, eid0 * nFields + iField);
        double *data1 = &(*proxies[1])(0, eid1 * nFields + iField);
        for(size_t j = 0; j < closure0.size(); ++j)  {
          const double  flux = v(j, i * nFields + iField);
          data0[closure0[j]] += flux;
          data1[closure1[j]] -= flux;
        }
      }
    }
  }
  else {
    for (size_t i=0; i < faces.size(); i++) {
      for (int iConnection = 0; iConnection < nbConnections; iConnection++) {
        const std::vector<int> &closure = faces.closure(i, iConnection);
        for (int iField=0; iField<nFields; iField++){
          for(size_t j =0; j < closure.size(); j++)
            (*proxies[iConnection])(closure[j], faces.elementId(i, iConnection)*nFields + iField) +=
              v(j, (i*nbConnections+iConnection)*nFields + iField);
        }
      }
    }
  }
}



void dgFaceTermAssemblerIntegral::compute(const dgGroupOfFaces &faces, double t, dgDofContainer &solution, dgDofContainer &residual, dgDofManager *jacDof, dgDofContainer *variable)
{
  if (! _term)
    return;
  int nbConnections = faces.nConnection();
  dataCacheMap cacheMap(dataCacheMap::INTEGRATION_GROUP_MODE, residual.getGroups(), nbConnections - 1);
  function::getTime()->set(t);
  cacheMap.setInterfaceGroup(&faces);
  cacheMap.setSolutionFunction(solution.getFunction(), solution.getFunctionGradient());
  if(variable)
    cacheMap.setVariableFunction(variable->getFunction(), variable->getFunctionGradient());
  fullMatrix<double> residualFace;
  if (jacDof) {
    std::vector<fullMatrix<double> > jacobianSol;
    std::vector<fullMatrix<double> > jacobianGradSol(nbConnections);
    _term->computeFace(cacheMap, residualFace, &jacobianSol, &jacobianGradSol);
    jacDof->reorderAndAssembleFaceGroupJacobian(faces, _nbFields, jacobianSol, jacobianGradSol);
  }
  else
    _term->computeFace(cacheMap, residualFace);
  std::vector<fullMatrix<double> *> residualProxies(nbConnections);
  for (int i=0; i<nbConnections; i++)
    residualProxies[i] = &residual.getGroupProxy(&faces.elementGroup(i));
  mapFromInterface(faces, _nbFields, residualFace, residualProxies);
}

dgTermAssemblerIntegral::~dgTermAssemblerIntegral()
{
  for (size_t i = 0; i < _terms.size(); ++i) {
    delete _terms[i];
  }
}

dgFaceTermAssemblerIntegral::dgFaceTermAssemblerIntegral(dgInterfaceTerm *term, int nbFields): _term(term), _nbFields(nbFields){
}

dgFaceTermAssemblerIntegral::~dgFaceTermAssemblerIntegral()
{
  delete _term;
}

