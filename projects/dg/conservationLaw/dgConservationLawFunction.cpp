#include "dgConservationLawFunction.h"
#include "dgGroupOfElements.h"
#include "function.h"
#include "functionGeneric.h"
#include "GModel.h"
#include "dgTerm.h"
#include "MElement.h"
#include "dgMeshJacobian.h"
#include "dgDofContainer.h"
#include "dgMassAssembler.h"
#include "dgIntegrationMatrices.h"
#include "dgHOMesh.h"
#include <limits>
#include "dgExtrusion.h"
#include "GmshConfig.h"
#ifdef HAVE_MPI
#include "mpi.h"
#endif

dgFullMatrix<double> computeMassInterface (const fullVector<int> &VIntMass, dataCacheMap &cacheMap)
{
  int nbFields = VIntMass.size();
  const dgGroupOfFaces &group = *cacheMap.getGroupOfInterfaces();
  const dgIntegrationMatrices &intMatrices = cacheMap.getIntegrationMatrices();
  int nbCon = group.nConnection();
  int nQP = intMatrices.getNbIntegrationPoints();
  dgFullMatrix<double> nzJk(nQP, nbFields, group.size() * group.nConnection() * group.nConnection(), true);

  const fullMatrix<double> &detJ = cacheMap.getJacobians().detJInterface(group.interfaceVectorId());
  const fullMatrix<double> &normals = cacheMap.getJacobians().normal(group.interfaceVectorId(), 0.);
  for(size_t iFace = 0; iFace < group.size(); ++iFace) {
    std::vector <fullMatrix<double> > nzJkElem(nbCon * nbCon, fullMatrix<double>());
    for (int iCon = 0; iCon < nbCon; iCon++){
      for (int jCon = 0; jCon < nbCon; jCon++){
        nzJk.getBlockProxy(iFace * nbCon * nbCon + iCon * nbCon + jCon, nzJkElem[iCon * nbCon + jCon]);
      }
    }
    for (int iVar = 0; iVar < nbFields; ++iVar) {
      if (VIntMass(iVar) == 1){
        for (int iPt = 0; iPt < nQP; ++iPt) {
          double nz = normals(iFace * nQP + iPt, group.elementGroup(0).getDimUVW() - 1);
          if (nz == 0.) continue;
          bool lowerSide = nz > 0; // Positive normal
          //Face 0 (i=0)
          //Normal is positive, assembled in the element (i=0, j=0)
          if (lowerSide){
            nzJkElem[0](iPt, iVar) = nz * detJ(iPt, group.interfaceId(iFace));
          }else if (nbCon == 2){ //Normal is negative, assembled with j from the neighbor (i=0, j=1)
            nzJkElem[1](iPt, iVar) = nz * detJ(iPt, group.interfaceId(iFace));
          }
          if (nbCon == 2){
            nzJkElem[3](iPt, iVar) = - nzJkElem[1](iPt, iVar);
            nzJkElem[2](iPt, iVar) = - nzJkElem[0](iPt, iVar);
          }
        }
      }
    }
  }
  dgFullMatrix<double> mass(intMatrices.psiPsiW().size1(), nbFields, group.size() * group.nConnection() * group.nConnection(), true);
  mass.gemm(intMatrices.psiPsiW(), nzJk);
  return mass;
}

static dgFullMatrix<double> computeMassElement(const fullVector<int> *VIntMass, const function *massFactor, bool constantMass, const dgGroupOfElements &group, const dgIntegrationMatrices &intMatrices, dataCacheMap &cacheMap)
{
  dataCacheDouble *term =  massFactor ? cacheMap.get(massFactor) : NULL;
  dgFullMatrix<double> massMatrix;
  if (!term && !VIntMass) {
    massMatrix.setAsProxy(group.getMassMatrix());
  }
  if (term && !VIntMass){
    massMatrix.resize(group.getNbNodes() * group.getNbNodes(), massFactor->getNbCol(), group.getNbElements(), false);
    massMatrix.gemm(intMatrices.psiPsiW(), cacheMap.getJacobians().multJElement(cacheMap.getGroupId(), term->get()), 1., 0.);
    massMatrix.reshape(group.getNbNodes(), group.getNbNodes(), massFactor->getNbCol() * group.getNbElements());
  }
  if (VIntMass) {
    dgFullMatrix<double>  mass, massInt;
    fullMatrix<double> dPsidzPsiW;
    const dgIntegrationMatrices &intMatrices = cacheMap.getIntegrationMatrices();
    int nbQP = intMatrices.getNbIntegrationPoints();
    int size = VIntMass->size();
    mass.resize(nbQP, size, group.getNbElements(), true);
    massInt.resize(nbQP, size, group.getNbElements(), true);
    dPsidzPsiW.resize(intMatrices.psiPsiW().size1(), intMatrices.psiPsiW().size2());
    int dim = intMatrices.dPsi().size1() / nbQP;
    int nbNodes = intMatrices.psi().size2();
    for (int i = 0; i < nbNodes; i++){
      for (int j = 0; j < nbNodes; j++){
        for (int iPt = 0; iPt < nbQP; iPt++){
          dPsidzPsiW(i * nbNodes + j, iPt) = intMatrices.dPsiPsiW()(i * nbNodes + j, (dim - 1) * nbQP + iPt);
        }
      }
    }
    const fullMatrix<double> &detJ = cacheMap.getJacobians().detJElement(group.elementVectorId());
    const fullMatrix<double> *termV = term ? &term->get(): NULL;
    for (size_t iEl = 0; iEl < group.getNbElements(); ++iEl) {
      for (int iPt = 0; iPt < cacheMap.nPointByElement(); iPt++) {
        fullMatrix<double> dXidX_k;
        cacheMap.getJacobians().dXiDXElement(group.elementVectorId(), iEl, iPt, dXidX_k);
        int dim = group.getDimUVW();
        for (int iVar = 0; iVar < size; iVar++) {
          if ((*VIntMass)(iVar)==1){
            massInt(iPt, size * iEl + iVar) = - (term ? ((*termV)(iEl * nbQP + iPt, iVar)) : 1) * detJ(iPt, iEl) * dXidX_k(dim-1, dim-1);
          }else{
            mass(iPt, size * iEl + iVar) = term ? ((*termV)(iEl * nbQP + iPt, iVar) * detJ(iPt, iEl)) : detJ(iPt, iEl);
          }
        }
      }
    }
    massMatrix.resize(group.getNbNodes() * group.getNbNodes() , size, group.getNbElements(), true);
    massMatrix.gemm(intMatrices.psiPsiW(), mass);
    massMatrix.gemm(dPsidzPsiW, massInt);
    massMatrix.reshape(group.getNbNodes(), group.getNbNodes(), group.getNbElements() * size);
  }
  return massMatrix;
}

fullMatrix<double> dgConservationLawFunction::innerRadius(dataCacheMap *m)
{
  fullMatrix<double> val(1, m->nPoint());
  int nP = m->nPointByElement();
  const dgGroupOfElements *g = m->getGroupOfElements();
  for (int iE = 0; iE < m->nElement(); ++iE) {
    const double radi = m->getJacobians().elementInnerRadius(g->elementVectorId())(m->elementId(iE));
    for (int iP = 0; iP < nP; iP++) {
      val(0, nP * iE + iP) = radi;
    }
  }
  return val;
}

fullMatrix<double> dgConservationLawFunction::maxEdge(dataCacheMap *m)
{
  fullMatrix<double> val(1, m->nPoint());
  int nP = m->nPointByElement();
  const dgGroupOfElements *g = m->getGroupOfElements();
  double magnusEdge;
  for (int iE = 0; iE < m->nElement(); ++iE) {
    MElement *el = g->getElement(m->elementId(iE));
    magnusEdge = el->maxEdge();
    for (int iP = 0; iP < nP; iP++) {
      val(0, nP * iE + iP) = magnusEdge;
    }
  }
  return val;
}

fullMatrix<double> dgConservationLawFunction::muFactor(dataCacheMap *m)
{
  fullMatrix<double> val(m->nPoint(), 2);
  const dgGroupOfFaces &iGroup = *m->getGroupOfInterfaces();
  if (!&iGroup)
    Msg::Fatal("dgConservationLawFunction::muFactor can only be used on interfaces");
  const dgGroupOfElements &eGroupL = *m->getGroupOfElements();
  const dgGroupOfElements &eGroupR = *m->getSecondaryCache(1)->getGroupOfElements();
  int nP = m->nPointByElement();
  for (int iFace = 0; iFace < m->nElement(); ++iFace) {
    int iElementL = m->elementId(iFace);
    int iElementR = m->getSecondaryCache(1)->elementId(iFace);
    int p = eGroupL.getOrder();
    int dim = eGroupL.getDimUVW();
    double minl = std::min(eGroupL.elementVolume(m->getJacobians(), iElementL), eGroupR.elementVolume(m->getJacobians(), iElementR)) / iGroup.interfaceSurface(m->getJacobians(), m->interfaceId(iFace));
    int n0 = std::max(eGroupL.getFunctionSpace().numFaces, eGroupR.getFunctionSpace().numFaces);
    for (int iP = 0; iP < nP; iP++) {
      val(nP * iFace + iP, 0) = (p + 1) * (p + dim) / (dim * minl) * n0 / 2;
      val(nP * iFace + iP, 1) = minl;
    }
  }
  return val;
}

/*==============================================================================
 * Generic boundary conditions
 *============================================================================*/

typedef std::map<const function*, const function*> mapFunction;

class dgBoundaryConditionOutsideGeneric : public dgBoundaryCondition {
  dgConservationLawFunction *_claw;
  mapFunction _replaceMap, _replaceMap2;
  std::string _tag;
  class termF : public function {
    std::vector<fullMatrix <double> > toReplaceIn, toReplaceOut, replaceBy, secondLevel;
    fullMatrix<double> interfaceTerm;
    functionReplace outReplace, inReplace;
    int sizeFirst, sizeSec;
    int _nbFields;
   public:
    termF(dgConservationLawFunction *claw, const mapFunction &replace_map, const std::string tag, const mapFunction *replace_map2) :
      function((claw->getInterfaceTerm0(tag)->getNbCol() > 2 * claw->getNbFields())? claw->getInterfaceTerm0(tag)->getNbCol()/2 : claw->getNbFields()) {
      _nbFields = claw->getNbFields();
      outReplace.addChild();
      inReplace.addChild();
      addFunctionReplace (outReplace);
      addFunctionReplace (inReplace);
      sizeFirst = replace_map.size();
      sizeSec = replace_map2 ? replace_map2->size() : 0 ;
      toReplaceIn.resize(sizeFirst);
      toReplaceOut.resize(sizeFirst + sizeSec);
      replaceBy.resize(sizeFirst);
      secondLevel.resize(sizeSec);
      int i = 0;
      for (std::map<const function*, const function*>::const_iterator it = replace_map.begin(); it != replace_map.end(); it++) {
        if(it->first != function::getSolution() && it->first != function::getSolutionGradient()
           && it->first->getNbCol() != it->second->getNbCol())
          Msg::Fatal("in outside boundary condition: verify functions in/out have to be of the same size");
        outReplace.replace (toReplaceOut[i], it->first, 1);
        inReplace.replace (toReplaceIn[i], it->first, 1);
        setArgument (replaceBy[i], it->second);
        i++;
      }
      if(replace_map2) {
        for (std::map<const function*, const function*>::const_iterator it = replace_map2->begin(); it != replace_map2->end(); it++) {
          if(it->first != function::getSolution() && it->first != function::getSolutionGradient()
             && it->first->getNbCol() != it->second->getNbCol())
            Msg::Fatal("in outside boundary condition: verify functions"
                       " in/out have to be of the same size (%d vs %d)", 
                       it->first->getNbCol(), it->second->getNbCol());
          outReplace.replace (toReplaceOut[i], it->first, 1);
          inReplace.get (secondLevel[i-sizeFirst], it->second, 1);
          i++;
        }
      }
      outReplace.get(interfaceTerm, claw->getInterfaceTerm0(tag));
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      if(sizeSec) {
        for(int k = 0; k < sizeFirst; k ++)
        toReplaceIn[k].setAll(replaceBy[k]);
        inReplace.compute(); //compute secondLevels
        for(int k = sizeFirst; k < sizeFirst+sizeSec; k ++)
          toReplaceOut[k].setAll(secondLevel[k-sizeFirst]);
      }
      for(int k = 0; k < sizeFirst; k ++) {
        for (int i = 0; i < toReplaceOut[k].size1(); ++i) {
          for (int j = 0; j < toReplaceOut[k].size2(); ++j) {
            toReplaceOut[k](i, j) = replaceBy[k](i, j);
          }
        }
      }
      outReplace.compute(); //compute interfaceTerm
      for(int i = 0; i < val.size1(); i++)
        for(int j = 0; j < _nbFields; j++)
          for(int iOrder = 0; iOrder < val.size2()/_nbFields; iOrder++)
            val(i, j + iOrder * _nbFields) = interfaceTerm(i, j + iOrder * _nbFields * 2);
    }
  };
  termF *_term;
 public:
  const function * getTerm0 () {
    if (_term)
      delete _term;
    _term = new termF(_claw, _replaceMap, _tag, _replaceMap2.empty() ? NULL : &_replaceMap2);
    return _term;
  }
  const function * getOutsideValue(const function *f) {
    mapFunction::const_iterator it = _replaceMap.find(f);
    if (it != _replaceMap.end())
      return it->second;
    return NULL;
  }
  ~dgBoundaryConditionOutsideGeneric() {
    if (_term)
      delete _term;
  }
  dgBoundaryConditionOutsideGeneric(dgConservationLawFunction *claw, const mapFunction &replace_map,
                                    const std::string tag, const mapFunction *replace_map2 = NULL)
                                    {
    _replaceMap = replace_map;
    _outsideValue = _replaceMap;
    if (replace_map2) {
      _replaceMap2 = *replace_map2;
    }
    _tag = tag;
    _claw = claw;
    _type = "outsideValue";
    _term = NULL;
  }
};

dgBoundaryCondition *dgConservationLawFunction::newOutsideValueBoundaryGeneric(const std::string tag,
                                                                       const std::vector<const function*> toReplace,
                                                                       const std::vector<const function*> replaceBy) {
  if(toReplace.size() != replaceBy.size())
    Msg::Fatal("toReplace must be of the same size as replaceBy in outsideValueBoundaryGeneric");
  mapFunction replace_map;
  for(size_t i = 0; i < toReplace.size(); i ++)
    replace_map[toReplace[i]] = replaceBy[i];
  return new dgBoundaryConditionOutsideGeneric(this, replace_map, tag);
}
dgBoundaryCondition *dgConservationLawFunction::newOutsideValueBoundaryGeneric(const std::string tag,
                                                                       const mapFunction *map,
                                                                       const mapFunction *map2) {
  return new dgBoundaryConditionOutsideGeneric(this, *map, tag, map2);
}

dgBoundaryCondition *dgConservationLawFunction::newOutsideValueBoundaryGeneric(const std::string tag,
                                                                       const std::vector<const function*> toReplace,
                                                                       const std::vector<const function*> replaceBy,
                                                                       const std::vector<const function*> toReplace2,
                                                                       const std::vector<const function*> replaceBy2) {
  if(toReplace.size() != replaceBy.size())
    Msg::Fatal("toReplace must be of the same size as replaceBy in outsideValueBoundaryGeneric");
  mapFunction replace_map, replace_map2;
  for(size_t i = 0; i < toReplace.size(); i ++) {
    replace_map [toReplace [i]] = replaceBy [i];
  }
  for(size_t i = 0; i < toReplace2.size(); i ++) {
    replace_map2[toReplace2[i]] = replaceBy2[i];
  }
  return new dgBoundaryConditionOutsideGeneric(this, replace_map, tag, &replace_map2);
}

dgBoundaryCondition *dgConservationLawFunction::newOutsideValueBoundary(const std::string tag, const function *outsideValueFunction) {
  mapFunction replace_map;
  replace_map[function::getSolution()] = outsideValueFunction;
  return new dgBoundaryConditionOutsideGeneric(this, replace_map, tag);
}

// BC : Neumann

class dgBoundaryConditionNeumann : public dgBoundaryCondition {
 public:
  dgBoundaryConditionNeumann(const function *fluxFunction) {
    _term0 = fluxFunction;
    _type = "Neumann";
  }
};

dgBoundaryCondition *dgConservationLawFunction::newNeumannBoundary(const function *fluxFunction) {
  return new dgBoundaryConditionNeumann(fluxFunction);
}

// BC : Neumann (0-flux)

class dgBoundaryCondition0Flux : public dgBoundaryCondition {
 public:
  dgBoundaryCondition0Flux(dgConservationLawFunction *claw, std::string tag) {
    std::vector<double> v0((claw->getInterfaceTerm0(tag)->getNbCol() > 2 * claw->getNbFields())? claw->getInterfaceTerm0(tag)->getNbCol()/2 : claw->getNbFields());
    for (int i = 0; i < claw->getNbFields() ; i++) {
      v0 [i] = 0;
    }
    _term0 = new functionConstant(v0);
    _type = "zero-flux";
  }
  ~dgBoundaryCondition0Flux() {
    delete _term0;
  }
};

dgBoundaryCondition *dgConservationLawFunction::new0FluxBoundary(std::string tag) {
  return new dgBoundaryCondition0Flux(this, tag);
}

// BC : Symmetry condition

class dgBoundaryConditionSymmetry : public dgBoundaryCondition {
  class termF : public function {
    fullMatrix<double> interfaceTerm; 
    functionReplace outReplace;
    int _nbFields;
   public:
    termF(dgConservationLawFunction *claw, const std::string tag):
      function((claw->getInterfaceTerm0(tag)->getNbCol() > 2 * claw->getNbFields())? claw->getInterfaceTerm0(tag)->getNbCol()/2 : claw->getNbFields()) { 
      _nbFields = claw->getNbFields();
      outReplace.addChild();
      addFunctionReplace (outReplace);
      outReplace.get(interfaceTerm, claw->getInterfaceTerm0(tag));
    }
    void call(dataCacheMap *m, fullMatrix<double> &val) {
      outReplace.compute();
      for(int i = 0; i < val.size1(); i++)
        for(int j = 0; j < _nbFields; j++)
          for(int iOrder = 0; iOrder < val.size2()/_nbFields; iOrder++)
            val(i, j + iOrder * _nbFields) = interfaceTerm(i, j + iOrder * _nbFields * 2);
    }
  };
  termF _termF;
 public:
  dgBoundaryConditionSymmetry(dgConservationLawFunction *claw, const std::string tag): _termF(claw, tag)
  { 
    _term0 = &_termF;
    _type = "symmetry";
  }
};

dgBoundaryCondition *dgConservationLawFunction::newSymmetryBoundary(const std::string tag) {
  return new dgBoundaryConditionSymmetry(this, tag);
}
void dgConservationLawFunction::addStrongBoundaryCondition (int dim, const std::string tag, const function *f)
{
  if (f->getNbCol() != _nbf && f->getNbCol() != _nbf * 2)
    Msg::Fatal("invalid size for strong boundary condition %d %d %d %d",f->getNbCol(),_nbf,f->getNbCol() , _nbf * 2);
 _strongBoundaryConditions.push_back(dgStrongBoundaryCondition(dim,tag,f));
}

/*==============================================================================
 * IP Term
 *============================================================================*/

class dgIPTerm : public function {
  fullMatrix<double> diffusiveFluxL, diffusiveFluxR, diffusivityL, diffusivityR, solutionL, solutionR, normal;
  int _nbFields;
  bool _useSip;
  public:
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    int dimXYZ = diffusiveFluxR.size2() / _nbFields;

    fullMatrix<double> ipf = dgConservationLawFunction::muFactor(m);
    for (int iPt = 0; iPt < m->nPoint(); ++iPt) {
      double minl = ipf(iPt, 1);
      double mufactor = ipf(iPt, 0);
      for (int k = 0; k < _nbFields; k++) {
        double meanNormalFlux = 0;
        for (int iDim = 0; iDim < dimXYZ; iDim ++)
          meanNormalFlux += (diffusiveFluxL(iPt, k + _nbFields * iDim) + diffusiveFluxR(iPt, k + _nbFields * iDim)) * normal(iPt, iDim) / 2.;

        double nu = 0;
        double solutionJumpPenalty = 0;
        if (diffusivityR.size2() == _nbFields)
        {
          nu = std::max(diffusivityR(iPt, k), diffusivityL(iPt, k));
          solutionJumpPenalty = (solutionL(iPt, k) - solutionR(iPt, k)) / 2. * mufactor * nu;
          if (!_useSip) {
            double wm = diffusivityL(iPt, k) / (diffusivityL(iPt, k) + diffusivityR(iPt, k));
            double wp = diffusivityR(iPt, k) / (diffusivityL(iPt, k) + diffusivityR(iPt, k));
            mufactor = std :: max (mufactor, sqrt(minl) * (wm * wm * diffusivityL(iPt, k) + wp * wp * diffusivityR(iPt, k))/ minl);
            solutionJumpPenalty = (solutionL(iPt, k) - solutionR(iPt, k)) * mufactor;
          }
        } 
        else if (diffusivityL.size2() == 3) { //TODO CHANGE THIS SOON!!!
          double nx = normal(iPt, 0);
          double ny = normal(iPt, 1);
          double nuL = diffusivityL(iPt, 0) * nx * nx + 2 * diffusivityL(iPt, 1) * nx * ny + diffusivityL(iPt, 2) * ny * ny;  
          double nuR = diffusivityR(iPt, 0) * nx * nx + 2 * diffusivityR(iPt, 1) * nx * ny + diffusivityR(iPt, 2) * ny * ny;  
          nu = std::max(nuL, nuR);
          solutionJumpPenalty = (solutionL(iPt, k) - solutionR(iPt, k)) / 2. * mufactor * nu;
        }
        val(iPt, k)             = - (meanNormalFlux + solutionJumpPenalty);
        val(iPt, k + _nbFields) =   (meanNormalFlux + solutionJumpPenalty);
      }
    }
  }
  dgIPTerm (int nbFields, const function *diffusiveFlux, const function *diffusivity, bool useSip): function(2 * nbFields), _nbFields(nbFields) {
    setArgument(diffusiveFluxL, diffusiveFlux, 0);
    setArgument(diffusiveFluxR, diffusiveFlux, 1);
    setArgument(diffusivityL, diffusivity, 0);
    setArgument(diffusivityR, diffusivity, 1);
    setArgument(solutionL, function::getSolution(), 0);
    setArgument(solutionR, function::getSolution(), 1);
    setArgument(normal, function::getNormals(), 0);
    _useSip = useSip;
  }
};

class dgIpTermAnisotropic : public function {
  fullMatrix<double>  diffusiveFluxL, diffusiveFluxR, ipPenaltyL, ipPenaltyR, solutionL, solutionR, normal;
  int _nbFields;
 public:
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    const dgGroupOfFaces &iGroup = *m->getGroupOfInterfaces();
    const dgGroupOfElements &eGroupL = *m->getGroupOfElements();
    const dgGroupOfElements &eGroupR = *m->getSecondaryCache(1)->getGroupOfElements();
    int nP = m->nPointByElement();
    for (int iFace = 0; iFace < m->nElement(); ++iFace) {
      for (int iP = 0; iP < nP; iP++) {
        int iElementL = m->elementId(iFace);
        int iElementR = m->getSecondaryCache(1)->elementId(iFace);
        int p = eGroupL.getOrder();
        int iPt = iFace * nP + iP;
        int dim = eGroupL.getDimUVW();
        double minl = std::min(eGroupL.elementVolume(m->getJacobians(), iElementL), eGroupR.elementVolume(m->getJacobians(), iElementR)) / iGroup.interfaceSurface(m->getJacobians(), m->interfaceId(iFace));
        double mufactor = (p + 1) * (p + dim) / (dim * minl);
        int dimXYZ = diffusiveFluxR.size2() / _nbFields;
        for (int k = 0; k < _nbFields; k++) {
          double meanNormalFlux = 0;
          for (int iDim = 0; iDim < dimXYZ; iDim ++)
            meanNormalFlux += ((diffusiveFluxL(iPt, k + _nbFields * iDim) + diffusiveFluxR(iPt, k + _nbFields * iDim)) * normal(iPt, iDim)) / 2;
          double ipFactor = std::max(ipPenaltyR(iPt, k), ipPenaltyL(iPt, k));
          //printf("\n%e",ipPenaltyR(iPt,k));
          if (ipFactor<1.0)
            ipFactor = 1.0;
          double solutionJumpPenalty = (solutionL(iPt, k) - solutionR(iPt, k)) / 2 * mufactor * ipFactor;
          val(iPt,k) = - (meanNormalFlux + solutionJumpPenalty);
          val(iPt,k+_nbFields) = (meanNormalFlux + solutionJumpPenalty);
        }
      }
    }
  }
  dgIpTermAnisotropic (int nbFields, const function *diffusiveFlux, const function *ipPenalty):function(2*nbFields), _nbFields(nbFields) {
    setArgument(ipPenaltyL,ipPenalty,0);
    setArgument(ipPenaltyR,ipPenalty,1);
    setArgument(diffusiveFluxL,diffusiveFlux,0);
    setArgument(diffusiveFluxR,diffusiveFlux,1);
    setArgument(solutionL,function::getSolution(),0);
    setArgument(solutionR,function::getSolution(),1);
    setArgument(normal,function::getNormals(),0);
  }
};

class dgIpTermErnRef : public function {
  fullMatrix<double>  diffusiveFluxL, diffusiveFluxR, ipPenaltyL, ipPenaltyR, solutionL, solutionR, normal;
  int _nbFields;
 public:
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    const dgGroupOfElements &eGroupL = *m->getGroupOfElements();
    int p = eGroupL.getOrder();
    int dim = eGroupL.getDimUVW();
    //double mufactor = (p + 1) * (p + dim) / (dim * minl);
    double mufactor = (p + 1) * (p + dim) / (2*dim);
    //printf(" \n%e",mufactor);
    int dimXYZ = diffusiveFluxR.size2() / _nbFields;
    
  for (int iPt = 0; iPt < val.size1(); iPt++) {
      for (int k = 0; k < _nbFields; k++) {
        double meanNormalFlux = 0;
        for (int iDim = 0; iDim < dimXYZ; iDim ++)
          meanNormalFlux += ((diffusiveFluxL(iPt, k + _nbFields * iDim) + diffusiveFluxR(iPt, k + _nbFields * iDim)) * normal(iPt, iDim)) / 2;
        //printf("\n%e",ipPenaltyR(iPt,k));
        double wm = ipPenaltyR(iPt, k)/ (ipPenaltyL(iPt, k) + ipPenaltyR(iPt, k));
        double wp = ipPenaltyL(iPt, k)/ (ipPenaltyL(iPt, k) + ipPenaltyR(iPt, k));
        double ipfactor = std::max(0.,  (wm * wm * ipPenaltyR(iPt, k) + wp * wp * ipPenaltyL(iPt, k)));
        //double ipfactor = std::max(ipPenaltyR(iPt, k), ipPenaltyL(iPt, k))/2;
        //double ipfactor = ipPenaltyR(iPt, k)+ ipPenaltyL(iPt, k)/4;
        //printf("\n%e",ipfactor);
        double solutionJumpPenalty = (solutionL(iPt, k) - solutionR(iPt, k))*ipfactor*mufactor; 
        //printf("\n%e",solutionJumpPenalty);
        val(iPt,k) = - (meanNormalFlux + solutionJumpPenalty);
        val(iPt,k+_nbFields) = (meanNormalFlux + solutionJumpPenalty);
      }
    }
  }
  dgIpTermErnRef (int nbFields, const function *diffusiveFlux, const function *ipPenalty):function(2*nbFields), _nbFields(nbFields) {
    setArgument(ipPenaltyL,ipPenalty,0);
    setArgument(ipPenaltyR,ipPenalty,1);
    setArgument(diffusiveFluxL,diffusiveFlux,0);
    setArgument(diffusiveFluxR,diffusiveFlux,1);
    setArgument(solutionL,function::getSolution(),0);
    setArgument(solutionR,function::getSolution(),1);
    setArgument(normal,function::getNormals(),0);
  }
};

class dgSymmetricIPTerm : public function {
  fullMatrix<double> diffusivityL, diffusivityR, solutionL, solutionR, normal;
  int _nbFields;
 public:
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    for (int iPt = 0; iPt < val.size1(); iPt++) {
      for (int k = 0; k < _nbFields; k++) { 
        const double nu = std::max(diffusivityR(iPt,k), diffusivityL(iPt,k));
        const double jump = (solutionL(iPt,k)-solutionR(iPt,k))/2;
        const double nx = normal(iPt, 0);
        const double ny = normal(iPt, 1);
        const double nz = normal(iPt, 2);
        val(iPt,k * 3 + 0) = - jump * nu * nx;
        val(iPt,k * 3 + 1) = - jump * nu * ny;
        val(iPt,k * 3 + 2) = - jump * nu * nz;
        val(iPt,(k + _nbFields) * 3 + 0) = - jump * nu * nx;
        val(iPt,(k + _nbFields) * 3 + 1) = - jump * nu * ny;
        val(iPt,(k + _nbFields) * 3 + 2) = - jump * nu * nz;
      }
    }
  }
  dgSymmetricIPTerm (int nbFields, const function *diffusivity):function(6*nbFields), _nbFields(nbFields) {
    setArgument(diffusivityL,diffusivity,0);
    setArgument(diffusivityR,diffusivity,1);
    setArgument(solutionL,function::getSolution(),0);
    setArgument(solutionR,function::getSolution(),1);
    setArgument(normal,function::getNormals(),0);
  }
};

class dgSymIPTerm3d : public function {
  fullMatrix<double> _diffusivityL,_diffusivityR, solutionL, solutionR, normal;
  int _nbFields;
 public:
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    for (int iPt = 0; iPt < val.size1(); iPt++) {
      for (int k = 0; k < _nbFields; k++) {
        const double jump = (solutionL(iPt,k)-solutionR(iPt,k))/2;
        const double nuX = std::max(_diffusivityR(iPt,0), _diffusivityL(iPt,0));
        const double nuY = std::max(_diffusivityR(iPt,1), _diffusivityL(iPt,1));
        const double nuZ = std::max(_diffusivityR(iPt,2), _diffusivityL(iPt,2));
        val(iPt,k * 3 + 0) = - nuX * jump ;
        val(iPt,k * 3 + 1) = - nuY * jump ;
        val(iPt,k * 3 + 2) = - nuZ * jump ;
        val(iPt,(k + _nbFields) * 3 + 0) = - nuX * jump ;
        val(iPt,(k + _nbFields) * 3 + 1) = - nuY * jump ;
        val(iPt,(k + _nbFields) * 3 + 2) = - nuZ * jump ;
      }
    }
  }
  dgSymIPTerm3d (int nbFields, const function *diffusivity):function(6*nbFields), _nbFields(nbFields) {
    setArgument(_diffusivityL,diffusivity,0);
    setArgument(_diffusivityR,diffusivity,1);
    setArgument(solutionL,function::getSolution(),0);
    setArgument(solutionR,function::getSolution(),1);
    setArgument(normal,function::getNormals(),0);
  }
};

function *dgNewIpTerm (int nbFields, const function *diffusiveFlux, const function *diffusivity, bool useSip) {
  return new dgIPTerm (nbFields, diffusiveFlux, diffusivity, useSip);
}

function *dgNewIpTermAnisotropic(int nbFields, const function *diffusiveFlux,  const function *ipPenalty){
  return new dgIpTermAnisotropic(nbFields, diffusiveFlux, ipPenalty);
}
function *dgNewIpTermErnRef(int nbFields, const function *diffusiveFlux, const function *ipPenalty){
  return new dgIpTermErnRef(nbFields, diffusiveFlux, ipPenalty);
}
function *dgNewSymmetricIpTerm (int nbFields, const function *diffusivity)
{
  return new dgSymmetricIPTerm (nbFields, diffusivity);
}

function *dgNewSymIpTerm3d (int nbFields, const function *diffusivity)
{
  return new dgSymIPTerm3d (nbFields, diffusivity);
}

dgTermAssembler *dgConservationLawFunction::getTerm(const dgGroupOfElements &group) const
{
  checkSetup();
  std::vector<dgTerm*> terms;
  const function *vf0 = functionForTag(_volumeTerm0, group.getPhysicalTag());
  const function *vf1 = functionForTag(_volumeTerm1, group.getPhysicalTag());
  if (vf0)
    terms.push_back(dgIntegralTerm::newPsiTerm(vf0, _nbf, isLinear()));
  if (vf1)
    terms.push_back(dgIntegralTerm::newGradPsiTerm(vf1, _nbf, isLinear()));
  return new dgTermAssemblerIntegral(terms);
}

dgFaceTermAssembler *dgConservationLawFunction::getFaceTerm(const dgGroupOfFaces &group) const
{
  checkSetup();
  const function *normalFluxF = NULL;
  switch (group.nConnection()) {
    case 1 :
      normalFluxF = getBoundaryCondition(group.physicalTag())->getTerm0();
      break;
    case 2 :
      normalFluxF = functionForTag(_interfaceTerm0, group.physicalTag());
      break;
    case 3 :
       normalFluxF = functionForTag(_bifurcationTerm0, group.physicalTag());
       break;
    default : Msg::Fatal("Number of connections invalid: %d", group.nConnection());
  }
  if (normalFluxF)
    return new dgFaceTermAssemblerIntegral(dgIntegralTerm::newInterfacePsiTerm(normalFluxF, isLinear()), _nbf);
  else
    return NULL;
}

const function *dgConservationLawFunction::getClipToPhysics(const std::string tag) const
{
  checkSetup();
  return functionForTag(_clipToPhysics, tag);
}

const function *dgConservationLawFunction::getInterfaceTerm0(const std::string tag) const
{
  checkSetup();
  return functionForTag(_interfaceTerm0, tag);
}

const function *dgConservationLawFunction::getdiffusivity(const std::string tag) const
{
  checkSetup();
  return functionForTag(_diffusivity,tag);
}

const function *dgConservationLawFunction::getMaximumConvectiveSpeed(const std::string tag) const
{
  checkSetup();
  return functionForTag(_maximumConvectiveSpeed,tag);
}

void dgConservationLawFunction::addBoundaryCondition(std::string tag, dgBoundaryCondition *condition)
{
  if(_boundaryConditions.find(tag) != _boundaryConditions.end())
    Msg::Fatal("The boundary condition with tag '%s' is already defined !", tag.c_str());
  _boundaryConditions[tag] = condition;
}

void dgConservationLawFunction::addBoundaryCondition(const std::vector<std::string> tags, dgBoundaryCondition *condition)
{
  for(size_t i = 0; i < tags.size(); ++i) {
    addBoundaryCondition(tags[i], condition);
  }
}

dgBoundaryCondition *dgConservationLawFunction::getBoundaryCondition(const std::string tag) const
{
  bcMap::const_iterator it = _boundaryConditions.find(tag);
  if(it==_boundaryConditions.end()) {
    Msg::Fatal("no boundary condition defined with tag '%s' (%d BCs)", tag.c_str(),_boundaryConditions.size());
  }
  return it->second;
}

void dgConservationLawFunction::computeFace(const dgGroupOfFaces &faces, double t, dgDofContainer &solutionDof, dgDofContainer &residual,  dgDofManager *jacDof, dgDofContainer *variable) const
{
  dgFaceTermAssembler *faceTerm = getFaceTerm(faces);
  if (faceTerm) {
    faceTerm->compute(faces, t, solutionDof, residual, jacDof, variable);
    delete faceTerm;
  }
}



void dgConservationLawFunction::computeFaceMass(const dgGroupOfFaces &faces, double t, dgDofContainer *du, dgDofContainer *rhs, double alpha, dgDofManager *dof) const
{
  fullVector<int> *VIntMass = NULL;
  if (VIntMass) {
    dgMassAssembler assembler(*this, rhs, du, *dof, alpha, dof != NULL);
    int nbConnections = faces.nConnection();
    dataCacheMap cacheMap(dataCacheMap::INTEGRATION_GROUP_MODE, faces.elementGroup(0).getGroupCollection(), nbConnections - 1);
    function::getTime()->set(t);
    cacheMap.setInterfaceGroup(&faces);
    dgFullMatrix<double> mass = computeMassInterface(*VIntMass, cacheMap);
    assembler.assembleFaces(faces, mass);
  }
}

void dgConservationLawFunction::computeElementMass(const dgGroupOfElements &group, double t, dgDofContainer *du, dgDofContainer *rhs, double alpha, dgDofManager *dof) const
{
  dgMassAssembler assembler(*this, rhs, du, *dof, alpha, dof != NULL);
  dataCacheMap cacheMap(dataCacheMap::INTEGRATION_GROUP_MODE, group.getGroupCollection());
  function::getTime()->set(t);
  cacheMap.setGroup(&group);
  const dgIntegrationMatrices &intMatrices = cacheMap.getIntegrationMatrices();
  bool constantMass;
  const function *massFactor = getMassFactor(group.getPhysicalTag(), constantMass);
  dgFullMatrix<double> massMatrix = computeMassElement(NULL, massFactor, constantMass, group, intMatrices, cacheMap);
  int size = massMatrix.nBlock() / group.getNbElements();
  massMatrix.reshape(group.getNbNodes() * group.getNbNodes() , size, group.getNbElements());
  dgFullMatrix<double> massE;
  for (size_t iElement = 0; iElement < group.getNbElements(); ++iElement) {
    massMatrix.getBlockProxy(iElement, massE);
    massE.reshape(group.getNbNodes(), group.getNbNodes() , size);
    assembler.assemble(group, group, iElement, iElement, massE);
  }
}

void dgConservationLawFunction::computeElement(const dgGroupOfElements &group, double t, dgDofContainer &solution, dgDofContainer &residual, dgDofManager *jacDof, dgDofContainer *variable) const
{
  dgTermAssembler *term = getTerm(group);
  if (term) {
    term->compute(group, t, solution, residual, jacDof, NULL, variable);
    delete term;
  }
}

void dgConservationLawFunction::computeElementaryTimeSteps(const dgGroupOfElements & group, dgDofContainer *solution, std::vector<double> & DT, dgExtrusion *extrusion) const
{ 
  dataCacheMap cacheMap(dataCacheMap::INTEGRATION_GROUP_MODE, group.getGroupCollection());
  cacheMap.setSolutionFunction(solution->getFunction(), solution->getFunctionGradient());
  const std::string tag = group.getPhysicalTag();
  dataCacheDouble *maxConvectiveSpeed = getMaximumConvectiveSpeed(tag) ? cacheMap.get(getMaximumConvectiveSpeed(tag)) : NULL;
  dataCacheDouble *diffusivity = getdiffusivity(tag) ? cacheMap.get(getdiffusivity(tag)) : NULL;
  // provided dataCache
  /* This is an estimate on how lengths changes with p 
     It is merely the smallest distance between gauss 
     points at order p + 1 */
  const double p   = group.getOrder();
  const double Cip = 3 * (p + 1) * (p + group.getDimUVW()) ;
  double l_red = 1./3. * p * p + 7./6. * p + 1.0;
  double l_red_sq = l_red * l_red;
  DT.resize(group.getNbElements());

  cacheMap.setGroup(&group);
  int nP = cacheMap.nPointByElement();
  for (size_t iElement = 0 ; iElement < group.getNbElements(); ++iElement) {
    double LH,LV;
    if (extrusion){
      LH = 2.0 * extrusion->innerRadius2d(group.elementVectorId(), iElement);
      //We suppose LV smaller than LH
      LV = 2.0 * cacheMap.getJacobians().elementInnerRadius(group.elementVectorId())(iElement);
    }else{
      LH = 2.0 * cacheMap.getJacobians().elementInnerRadius(group.elementVectorId())(iElement);
      LV = LH;
    }
    double L=std::min(LH, LV);
    double spectralRadius = 0.0;
    if (diffusivity){
      const fullMatrix<double> &nuV = diffusivity->get();
      double nu = nuV(iElement * nP, 0);
      for (int j = 0; j < nP; j++)
        for (int k = 0; k < nuV.size2(); k++)
          nu = std::max(nuV(iElement * nP + j, k), nu);
      spectralRadius += group.getDimUVW() * nu / (L * L) * std::max(l_red_sq, 6. * l_red * Cip);
      spectralRadius += 4.0 * nu * l_red * Cip / (L * L);
    }
    if (maxConvectiveSpeed) {
      const fullMatrix<double> &maxVelocity = maxConvectiveSpeed->get();
      if (maxVelocity.size2() == 1){
        double c = maxVelocity(iElement * nP, 0);
        for (int k = 1; k < nP; k++)
          c = std::max(maxVelocity(iElement * nP + k, 0), c);
        spectralRadius += 4.0 * c * l_red / L;
      }else{
        double cH = maxVelocity(iElement * nP, 0);
        double cV = maxVelocity(iElement * nP, 1);
        for (int k = 1; k < nP; k++){
          cH = std::max(maxVelocity(iElement * nP + k, 0), cH);
          cV = std::max(maxVelocity(iElement * nP + k, 1), cV);
        }
        spectralRadius += 4.0 * l_red * std::max(cH / LH, cV / LV);
      }
    }
    DT[iElement] = 1. / spectralRadius;
  }
}

double dgConservationLawFunction::getMinOfTimeSteps(dgDofContainer *solution, dgExtrusion *extrusion) const
{
  std::vector<std::vector<double> >localDt;
  const dgGroupCollection &groups = *solution->getGroups();
  // Compute the time step constrain per element
  double dtMin = std::numeric_limits<double>::max();
  localDt.resize(groups.getNbElementGroups());
  for (int i=0;i< groups.getNbElementGroups();i++){
    computeElementaryTimeSteps(*groups.getElementGroup(i), solution, localDt[i], extrusion);
    for (size_t k=0;k<localDt[i].size();k++){
      dtMin = std::min(dtMin,localDt[i][k]);
    }
  }
  // If HAVE_MPI take the min of the mins of each partition
  double dtMinGlobal = dtMin;
#ifdef HAVE_MPI
  MPI_Allreduce((void *)&dtMin, (void *)&dtMinGlobal, 1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
#endif
  return dtMinGlobal;
}


const function *dgConservationLawFunction::getMassFactor(const std::string tag, bool &constant) const
{
  checkSetup();
  std::map<const std::string, std::pair<const function*, bool> >::const_iterator it = _massFactor.find(tag);
  if (it == _massFactor.end())
    it = _massFactor.find("");
  if (it == _massFactor.end())
    return NULL;
  constant = it->second.second;
  return it->second.first;
}

dgConservationLaw::massFactorType dgConservationLawFunction::haveMassFactor(const dgGroupOfElements &group) const
{
  bool constantMassFactor;
  const function *mf = getMassFactor(group.getPhysicalTag(), constantMassFactor);
  return ((!mf) ? MASS_FACTOR_NO : constantMassFactor ? MASS_FACTOR_CONSTANT : MASS_FACTOR_TIME_DEPENDENT);
}

const function *dgConservationLawFunction::getOutsideValue(const dgGroupOfFaces* faces, const function *in) const
{
  dgBoundaryCondition *bc = getBoundaryCondition(faces->physicalTag());
  if (! bc) return NULL;
  return bc->getOutsideValue(in);
}

void dgConservationLawFunction::fixStrongBC(dgDofManager &dof, dgDofContainer *oldSolution, double alpha) const
{
  const dgHOMesh *mesh = dof.getHOMesh();
  if (!mesh) return;
  dof.clearStrongBC();
  std::map<int, std::vector<GEntity*> > physicalGroups[4];
  const dgGroupCollection &groups = dof.getGroups();
  groups.getModel()->getPhysicalGroups(physicalGroups);
  dataCacheMap cache(dataCacheMap::NODE_MODE, &groups);
  dataCacheDouble *oldC = oldSolution ? cache.get(oldSolution->getFunction()) : NULL;
  std::map<int, std::list<dataCacheDouble*> > verticesBC;
  for (size_t i = 0; i < _strongBoundaryConditions.size(); i++) {
    const dgStrongBoundaryCondition &bc = _strongBoundaryConditions[i];
    std::vector<GEntity*> entities = physicalGroups[bc.dim][groups.getModel()->getPhysicalNumber(bc.dim, bc.physicalTag)];
    dataCacheDouble *valC = cache.get(bc.f);
    for (size_t j = 0; j < entities.size(); j++) {
      GEntity *entity = entities[j];
      for (size_t k = 0; k < entity->getNumMeshElements(); k++) {
        std::vector<int> eVertices = mesh->vertices(entity->getMeshElement(k));
        for (size_t l = 0; l < eVertices.size(); l++) {
          verticesBC[eVertices[l]].push_back(valC);
        }
      }
    }
  }
  for (int i = 0; i < groups.getNbFaceGroups(); i++) {
    dgGroupOfFaces &faces = *groups.getFaceGroup(i);
    cache.setInterfaceGroup(&faces, 0);
    int groupId = groups.getElementGroupId(&faces.elementGroup(0));
    for (size_t j = 0; j < faces.size(); j++) {
      const std::vector<int> &closure =  faces.closure(j, 0);
      cache.setInterface(j);
      const fullMatrix<double> *oldV = oldC ? &oldC->get() : NULL;
      const std::vector<int> &elementVertices = mesh->vertices(groupId, faces.elementId(j, 0));
      for (size_t iVertex = 0; iVertex < closure.size(); iVertex++) {
        int v = elementVertices[closure[iVertex]];
        std::list<dataCacheDouble*> &vertexBC = verticesBC[v];
        for (std::list<dataCacheDouble*>::iterator it = vertexBC.begin(); it != vertexBC.end(); ++it) {
          const fullMatrix<double> &val = (*it)->get();
          for (int iField = 0; iField < _nbf; ++iField) {
            if (val.size2() == _nbf || (val(iVertex, _nbf + iField) > 0.0)) {
              double oldValue = oldV ? (*(oldV))(iVertex, iField) : 0;
              dof.fixStrongBC(v, iField, (val(iVertex, iField) - oldValue) * alpha);
            }
          }
        }
      }
    }
  }
}

bool dgConservationLawFunction::haveFaceTerm(const dgGroupOfFaces &faces) const
{
  dgFaceTermAssembler* term = getFaceTerm(faces);
  if (term) {
    delete term;
    return true;
  }
  return false;
}

