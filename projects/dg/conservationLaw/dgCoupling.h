#ifndef _DG_COUPLING_H_
#define _DG_COUPLING_H_

#include "function.h"
#include "dgFunctionIntegratorInterface.h"
#include "functionGeneric.h"
#include "dgDofContainer.h"

class dgCouplingIntegral0D{
  public:
  const function *_toIntegrate1;
  const function *_toIntegrate2;
  dgDofContainer *_solution1;
  dgDofContainer *_solution2;
  const function *_outsideSolution1;
  const function *_outsideSolution2;
  function *_solutionF1;
  function *_solutionF2;

  const dgGroupCollection *_groups1;
  const dgGroupCollection *_groups2;

  functionConstant *integrals1;
  functionConstant *integrals2;

  fullMatrix<double> integrals1_fm;
  fullMatrix<double> integrals2_fm;

  std::string _tag1, _tag2;

  dgFunctionIntegratorInterface *integrator1;
  dgFunctionIntegratorInterface *integrator2;

  int dim;

  dgCouplingIntegral0D(const dgGroupCollection *groups1, const dgGroupCollection *groups2,  dgDofContainer *solution1, dgDofContainer *solution2, std::string tag1, std::string tag2){
    _groups1 = groups1;
    _groups2 = groups2;
    _solution1 = solution1;
    _solution2= solution2;
    _solutionF1 = solution1->getFunction();
    _solutionF2 = solution1->getFunction();
    _tag1 = tag1;
    _tag2 = tag2;
    dim  = std::max(solution1->getNbFields(),solution2->getNbFields());
    integrals1 = new functionConstant(std::vector<double>(dim,0));
    integrals2 = new functionConstant(std::vector<double>(dim,0));
    integrals1_fm.resize(1, dim, true);
    integrals2_fm.resize(1, dim, true);
  }

  void setIntegrator(){
    integrator1 = new dgFunctionIntegratorInterface(_groups1, _toIntegrate1, _solution1); 
    integrator2 = new dgFunctionIntegratorInterface(_groups2, _toIntegrate2, _solution2); 
  }

  inline const function *getOutsideSolution1(){
    return _outsideSolution1;
  }
  inline const function *getOutsideSolution2(){
    return _outsideSolution2;
  }

  void updateIntegrals(){
    integrator1->compute(_tag1, integrals1_fm);
    integrals1->set(integrals1_fm);
    integrator2->compute(_tag2, integrals2_fm);
    integrals2->set(integrals1_fm);
  }

  ~dgCouplingIntegral0D(){
    delete integrator1;
    delete integrator2;
    delete integrals1;
    delete integrals2;
    delete _toIntegrate1;
    delete _toIntegrate2;
    delete _outsideSolution1;
    delete _outsideSolution2;
  }
};


class dgCoupling_SW1D_SW2D:public dgCouplingIntegral0D{
  class computeOutsideSolution1;
  class computeOutsideSolution2;
  class computeToIntegrate1;
  class computeToIntegrate2;
  public:

  const function *_bath;
  double width, section0;

  /** class to compute the integrals and the outside value when coupling interfaces of different models */
  dgCoupling_SW1D_SW2D(const dgGroupCollection *groups1, const dgGroupCollection *groups2, dgDofContainer *solution1, dgDofContainer *solution2, const function *bath, const std::string tag1, const std::string tag2):dgCouplingIntegral0D(groups1, groups2, solution1, solution2, tag1, tag2){
    _bath = bath;

    //compute connection width
    fullMatrix<double> integrals2_0(1,1);
    function *fone = new functionConstant(1.);
    dgFunctionIntegratorInterface integrator_1(groups2, fone, solution2);
    integrator_1.compute(_tag2, integrals2_0); 
    width = integrals2_0(0,0);
    
    //compute connection section at rest
    dgFunctionIntegratorInterface integrator_bath(groups2, bath, solution2);
    integrator_bath.compute(_tag2, integrals2_0);
    section0 = integrals2_0(0,0);
    
    delete fone;

    _toIntegrate1 = computeToIntegrate1New(_solutionF1);
    _toIntegrate2 = computeToIntegrate2New(_solutionF2, _bath);
    setIntegrator();
    _outsideSolution1 = computeOutsideSolution1New(integrals1, section0, width);
    _outsideSolution2 = computeOutsideSolution2New(integrals2);
  };

  inline double getSection0(){
    return section0;
  }
  inline double getWidth(){
    return width;
  }
  ~dgCoupling_SW1D_SW2D(){
  }
  function *computeToIntegrate1New(const function *solution);
  function *computeToIntegrate2New(const function *solution, const function *bath);
  function *computeOutsideSolution1New(const function *integrals, const double section0, const double width);
  function *computeOutsideSolution2New(const function *integrals);
};

class dgDofOverAnotherCollectionFunction : public function {
  const dgDofContainer * _dofC;
 public:
  dgDofOverAnotherCollectionFunction(const dgDofContainer * dofC) : function(dofC->getNbFields()), _dofC(dofC) {}
  void call(dataCacheMap *m, dataCacheDouble *d, fullMatrix<double> &val);
};

class dgFunctionCoupled {
  friend class dgCoupling;
 protected:
  std::vector<std::pair<const function *, const dgGroupCollection *> > _funcs;
  std::vector<fullMatrix<double> > _args;
  int _nbFields;
  void init() {
    if(_funcs.size() < 2) Msg::Error("nb GroupCollection should be greater or equal to 2");
    _args.resize(_funcs.size());
  }

 public:
  dgFunctionCoupled(std::vector<std::pair<const function *, const dgGroupCollection *> > f, int nbFields=1):
    _funcs(f), _nbFields(nbFields) { init(); }
  dgFunctionCoupled(std::vector<const function *> f, std::vector<const dgGroupCollection *> g, int nbFields=1): _nbFields(nbFields) {
    if(f.size() != g.size()) Msg::Error("size(f) != size(g) in dgFunctionCoupled !");
    for(size_t i = 0; i < f.size(); ++i)  _funcs.push_back(std::make_pair(f[i], g[i]));
    init();
  }
  virtual void call(/*dataCacheMap *m, */fullMatrix<double> &val) = 0;
  inline const fullMatrix<double> & get(int idVar) { return _args[idVar]; }
  inline const int nFunction() { return _funcs.size(); }
  inline const int nFields() { return _nbFields; }
  inline const function * getFunction(int idVar) { return _funcs[idVar].first; }
  inline const dgGroupCollection * getGroupCollection(int idVar) { return _funcs[idVar].second; }
  virtual ~dgFunctionCoupled(){};
};

class dgFunctionCoupledC : public dgFunctionCoupled {
  void (*_callback)(void);
 public:
  dgFunctionCoupledC(std::string file, std::string symbol, int nbFields,
                     std::vector<const function *> f, std::vector<const dgGroupCollection *> g);
  virtual void call (fullMatrix<double> &val);
};

//coupling between 1D-2D or 2D-3D

class dgCoupling {
  dgDofContainer * _coupledField;
  dgFunctionCoupled * _f;
  std::map<const dgGroupCollection *, dataCacheMap *> _cacheMaps;
  std::vector<dataCacheDouble *> _cacheDoubles;
  std::map<const dgGroupCollection *, function *> _vFunc;
  int _dimFace;
 public:
  dgCoupling(dgFunctionCoupled * f, dataCacheMap::mode m = dataCacheMap::INTEGRATION_GROUP_MODE);
  void compute(std::vector<std::string> physicalTags=std::vector<std::string>());

  /* get dgFunctionCoupled in the required group collection */
  const function * getFunction(const dgGroupCollection * gc);

  ~dgCoupling() {
    for(std::map<const dgGroupCollection *, dataCacheMap *>::const_iterator it = _cacheMaps.begin(); it != _cacheMaps.end(); ++it)
      delete it->second;
    for(std::map<const dgGroupCollection *, function *>::const_iterator it = _vFunc.begin(); it != _vFunc.end(); ++it)
      delete it->second;
    delete _coupledField;
  }
};


#endif
