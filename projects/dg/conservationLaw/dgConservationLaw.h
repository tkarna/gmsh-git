#ifndef _DG_CONSERVATION_LAW_H_
#define _DG_CONSERVATION_LAW_H_

class dgDofContainer;
class dgDofManager;
class dgGroupOfElements;
class dgGroupOfFaces;

/** @todo dgHOMesh should maybe not be exposed outside dgDofManager */
class dgHOMesh;
template<class t> class dgFullMatrix;

#include <string>
#include <vector>

//@TODO : function and dgExtrusion should not be needed
class function;
class dgExtrusion;
class dgMassAssembler;
class dgConservationLaw {
  public :
  enum imexMode {IMEX_IMPLICIT, IMEX_ALL};
  enum massFactorType {MASS_FACTOR_NO, MASS_FACTOR_CONSTANT, MASS_FACTOR_TIME_DEPENDENT};
  virtual int getNbFields() const = 0;
  virtual bool isConstantJac() const {return false;}
  virtual bool isLinear() const {return false;}
  virtual int isTemporal(int iField) const {return 1;}
  virtual void setImexMode(imexMode mode) {}
  virtual massFactorType haveMassFactor(const dgGroupOfElements&) const {return MASS_FACTOR_NO;}
  // @todo access to dgResidual should be done through dgDofManager

  /** Compute RHS and jacobian for elements. Compute the RHS and the jacobian for each element of the group.
   *  dofManager=NULL means no jacobian matrix is needed. @todo ask Thomas for what is variable 
   */
  virtual void computeElement(const dgGroupOfElements &group, double t, dgDofContainer &solution, dgDofContainer &residual, dgDofManager *dofManager, dgDofContainer *variable = NULL) const = 0;
  /** rhs -= M * dU *alpha; dof += M * alpha */
  virtual void computeElementMass(const dgGroupOfElements &group, double t, dgDofContainer *du, dgDofContainer *rhs, double alpha, dgDofManager *dof) const = 0;
  /** Equivalent of {@link #computeElement} for faces. */
  virtual void computeFace(const dgGroupOfFaces &faces, double t, dgDofContainer &solutionDof, dgDofContainer &residual,  dgDofManager *jacDof, dgDofContainer *variable = NULL) const = 0;
  /** Equivalent of {@link #computeElementMass} for faces. */
  virtual void computeFaceMass(const dgGroupOfFaces &faces, double t, dgDofContainer *du, dgDofContainer *rhs, double alpha, dgDofManager *dof) const = 0;
  /** Maximum explicit time step associated with the CFL condition. 
      The output DT is a vector of the same size as the number of elements in the group.  
      Parameter extrusion should be provided when the mesh was built from an extrusion 
      of a 2d mesh and horizontal/vertical phenomena have different timescales.
  */
  virtual void computeElementaryTimeSteps (const dgGroupOfElements & group, dgDofContainer *solution, std::vector<double> & DT, dgExtrusion *extrusion = NULL) const = 0;
  void computeAllTerms(double t, dgDofContainer &solution, dgDofContainer &residual, bool scatterSolutionDof = true) const;
  virtual void fixStrongBC(dgDofManager &dof, dgDofContainer *oldSolution = NULL, double alpha = 1.) const {};
  virtual ~dgConservationLaw() {};

  /** @todo this should be here
   * virtual void fillSparsityPattern(dgDofManager *dof, bool hasInterfaceTerm) const = 0;
   * and we can remove this
   */
   virtual bool haveFaceTerm(const dgGroupOfFaces &faces) const {return true;}
   

  /** @todo everything bellow still relies on functions (but is optional) => should be adapted:
   *for dgLimiter
   */
  virtual const function *getOutsideValue(const dgGroupOfFaces*, const function *in) const {return NULL;}
  virtual const function *getClipToPhysics(const std::string tag) const {return NULL;}
};

#endif
