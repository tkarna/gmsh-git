
#include <dgFullMatrix.h>
#include <dgDofManager.h>
#include <dgDofContainer.h>
#include <dgMassAssembler.h>
#include <dgConservationLaw.h>
#include <dgGroupOfElements.h>

dgMassAssembler::dgMassAssembler(const dgConservationLaw &claw, dgDofContainer *Rhs, dgDofContainer *dU, dgDofManager &dof, double alpha, bool addToLhs):_claw(claw),_Rhs(Rhs),_dU(dU),_dof(dof),_alpha(alpha),_addToLhs(addToLhs){}

//Rhs -= M * dU * alpha, dof += M * alpha
void dgMassAssembler::assemble(const dgGroupOfElements &groupI, const dgGroupOfElements &groupJ, int iElem, int jElem, dgFullMatrix<double> &M)
{
  if (_alpha != 0){
    fullMatrix<double> dUE, RhsE, MField;
    int iGroupI = groupI.getGroupCollection()->getElementGroupId(&groupI);
    int iGroupJ = groupJ.getGroupCollection()->getElementGroupId(&groupJ);
    if (_Rhs && _dU){
      _Rhs->getGroupProxy(iGroupI).getBlockProxy(iElem, RhsE);
      _dU->getGroupProxy(iGroupJ).getBlockProxy(jElem, dUE);
    }
    int nField = _claw.getNbFields();
    int nNodeI = groupI.getNbNodes();
    int nNodeJ = groupJ.getNbNodes();
    fullMatrix<double> JE(nField * nNodeI, nField * nNodeJ);
    int nFieldInMass = M.size2() / nNodeJ;
    if (nFieldInMass == 1)
      M.getBlockProxy(0, MField);
    for (int iVar = 0; iVar < nField; iVar++) {
      if (!_claw.isTemporal(iVar)) continue;
      if (_claw.isTemporal(iVar) > 1 && iElem != jElem) Msg::Fatal("Mass lumping cannot be used with off-diagonal blocks");
      if (nFieldInMass == nField)
	M.getBlockProxy(iVar, MField);
      if (_Rhs && _dU) {
	if (_claw.isTemporal(iVar) > 1)  //Mass-lumping
	  for (int i = 0; i < nNodeI; i++)
	    for (int j = 0; j < nNodeJ; j++)
	      RhsE (i, iVar) -= MField(i, j) * dUE(i, iVar) * _alpha;
	else
	  for (int i = 0; i < nNodeI; i++)
	    for (int j = 0; j < nNodeJ; j++)
	      RhsE (i, iVar) -= MField(i, j) * dUE(j, iVar) * _alpha;
      }
      if (_addToLhs) {
	if (_claw.isTemporal(iVar) > 1)  //Mass-lumping
	  for (int i = 0; i < nNodeI; i++)
	    for (int j = 0; j < nNodeJ; j++)
	      JE(iVar*nNodeI+i, iVar*nNodeI+i) = MField (i, j) * _alpha;
	else
	  for (int i = 0; i < nNodeI; i++)
	    for (int j = 0; j < nNodeJ; j++)
	      JE(iVar*nNodeI+i, iVar*nNodeJ+j) = MField (i, j) * _alpha;
      }
    }
    if(_addToLhs) {
      _dof.assembleLHSMatrix(iGroupI, iElem, iGroupJ, jElem, JE);
    }
  }
}

void dgMassAssembler::assembleFaces(const dgGroupOfFaces &faces, dgFullMatrix<double> &mass)
{
  if (mass.size1() == 0 || mass.size2() == 0) return;
  fullMatrix<double> massKE;
  int nbCon = faces.nConnection();
  int nbInterfaceNodes = faces.getNodalBasis()->points.size1();


  int nbFields = mass.size2() / faces.size() / nbCon / nbCon;
  for (int iCon = 0; iCon < nbCon; iCon++) {

    if (faces.elementGroup(iCon).getGhostPartition() >= 0)
      continue;


    int nbNodesElemI = faces.elementGroup(iCon).getNbNodes();
    for (int jCon = 0; jCon < nbCon; jCon++) {

      if (faces.elementGroup(jCon).getGhostPartition() >= 0)
	continue;

  

      int nbNodesElemJ = faces.elementGroup(jCon).getNbNodes(); // all the node of adjacent elmts have contribution on the modified mass
      dgFullMatrix<double> massE(nbNodesElemI, nbNodesElemJ, nbFields, false);
      fullMatrix<double> massEVar; 
      for (size_t iFace = 0 ; iFace < faces.size(); ++iFace) {
	const std::vector<int> &closureI = faces.closure(iFace, iCon);
	massE.setAll(0);
	for (int iVar = 0; iVar < nbFields; iVar++){
	  massE.getBlockProxy(iVar, massEVar);
	  int ibloc = iFace * nbCon * nbCon + iCon * nbCon + jCon;
	  mass.getBlockProxy(ibloc, massKE);
	  const std::vector<int> &closureJ = faces.closure(iFace, jCon);
	  for (int jNode = 0; jNode < nbInterfaceNodes; jNode++) {
	    for (int iNode = 0; iNode < nbInterfaceNodes; iNode++) {
	      massEVar(closureI[iNode], closureJ[jNode]) = massKE(iNode * nbInterfaceNodes + jNode, iVar);
	    }
	  }
	}
	assemble(faces.elementGroup(iCon), faces.elementGroup(jCon), faces.elementId(iFace, iCon), faces.elementId(iFace, jCon), massE);
      }
    }
  }
}
