#include "dgLimiter.h" 
#include "dgGroupOfElements.h"   
#include "nodalBasis.h"
#include "dgCGStructure.h"
#include "function.h"
#include "dgDofContainer.h"
#include "dgConservationLaw.h"
#include "SVector3.h"
#include "MVertex.h"
#include "GModel.h"
#ifdef HAVE_MPI
#include "mpi.h"
#endif
#include <limits>
#include "dgMeshJacobian.h"
#include "dgMesh.h"
#include "dgIntegrationMatrices.h"
#include "MElement.h"
#include "legendrePolynomials.h"

//----------------------------------------------------------------------------------   
int dgSlopeLimiter::applyForGroup (dgDofContainer *solution, dgGroupOfElements *goe)
{
  const dgGroupCollection &groups = *solution->getGroups();
  int groupId = groups.getElementGroupId(goe);
  solution->scatter(); //FIXME really needed ?? not good for scalability...
  int nbFields = solution->getNbFields();

  // first compute max and min of all fields for all stencils 
  //----------------------------------------------------------
  dgDofContainer MIN(groups, nbFields);
  dgDofContainer MAX(groups, nbFields);

  MIN.setAll ( 1.e22);
  MAX.setAll (-1.e22);

  int iElementL, iElementR, fSize=0;
  fullMatrix<double> TempL, TempR;

  for( int iGFace = 0; iGFace < groups.getNbFaceGroupsOfGroup(groupId); iGFace++) {
    dgGroupOfFaces* group = groups.getFaceGroupOfGroup(groupId, iGFace);

    if (group->nConnection() == 2) { // interior Faces

      const dgGroupOfElements *groupLeft = &group->elementGroup(0);
      const dgGroupOfElements *groupRight = &group->elementGroup(1);

      dgFullMatrix<double> &solleft = solution->getGroupProxy(groupLeft);
      dgFullMatrix<double> &solright = solution->getGroupProxy(groupRight);
      dgFullMatrix<double> &MINLeft = MIN.getGroupProxy(groupLeft);
      dgFullMatrix<double> &MAXLeft = MAX.getGroupProxy(groupLeft);
      dgFullMatrix<double> &MINRight = MIN.getGroupProxy(groupRight);
      dgFullMatrix<double> &MAXRight = MAX.getGroupProxy(groupRight);

      for(size_t iFace=0 ; iFace < group->size(); iFace++) {

        iElementL = group->elementId(iFace, 0);
        iElementR = group->elementId(iFace, 1);
        solleft.getBlockProxy(iElementL, TempL);
        solright.getBlockProxy(iElementR, TempR);
        fSize = TempL.size1();
        for (int k = 0; k < nbFields; ++k) {
          double AVGL = 0;
          double AVGR = 0;
          for (int i = 0; i < fSize; ++i) {
            AVGL += TempL(i,k);
            AVGR += TempR(i,k);
          }
          AVGL /= (double) fSize;
          AVGR /= (double) fSize;
          MINLeft  (0, iElementL * nbFields + k) = std::min ( AVGR , MINLeft  (0, iElementL * nbFields + k ) );
          MAXLeft  (0, iElementL * nbFields + k) = std::max ( AVGR , MAXLeft  (0, iElementL * nbFields + k ) );
          MINRight (0, iElementR * nbFields + k) = std::min ( AVGL , MINRight (0, iElementR * nbFields + k ) );
          MAXRight (0, iElementR * nbFields + k) = std::max ( AVGL , MAXRight (0, iElementR * nbFields + k ) );
        }
      }

    } else if(group->nConnection() == 1) { //exterior Faces

      const dgGroupOfElements *groupIn = &group->elementGroup(0);

      const function *bcF = _claw->getOutsideValue(group, function::getSolution());

      dataCacheMap cacheMap(dataCacheMap::NODE_MODE, &groups);
      cacheMap.setSolutionFunction(solution->getFunction(), solution->getFunctionGradient());
      const dgMeshJacobian & jacobians = cacheMap.getJacobians();

      dgFullMatrix<double> &MINIn = MIN.getGroupProxy(groupIn);
      dgFullMatrix<double> &MAXIn = MAX.getGroupProxy(groupIn);

      if(bcF) { // outside value boundary
        dataCacheDouble &bcE = *cacheMap.get(bcF, NULL);
        cacheMap.setInterfaceGroup(group,0);
        for(size_t iFace = 0; iFace < group->size(); iFace++) {
          cacheMap.setInterface(iFace);
          iElementL = group->elementId(iFace, 0);
          const fullMatrix<double> &bcM = bcE.get();

          fSize = bcM.size1();
          for (int k = 0; k < nbFields; ++k) {
            double AVG = 0;
            for (int i = 0; i < fSize; ++i) {
              AVG += bcM(i,k);
            }
            AVG /= (double) fSize;
            MINIn (0, iElementL * nbFields + k) = std::min ( AVG , MINIn  (0, iElementL * nbFields + k ) );
            MAXIn (0, iElementL * nbFields + k) = std::max ( AVG , MAXIn  (0, iElementL * nbFields + k ) );
          }
        }
      } // else { // not outside value boundary
        //3D ! P1 ! Plane faces !

        fullMatrix<double> normals;
      
        dataCacheDouble &solE  = *cacheMap.get(solution->getFunction(), NULL);
        dataCacheDouble &gsolE = *cacheMap.get(solution->getFunctionGradient(), NULL);

        cacheMap.setGroup(groupIn);

        for(size_t iFace = 0; iFace < group->size(); iFace++) {
          //1) find element and bnd face in element
          iElementL = group->elementId(iFace, 0);
          cacheMap.setElement(iElementL);
          MElement * elem = cacheMap.element(0);
          const fullMatrix<double> & sol = solE.get();
          const fullMatrix<double> &gsol = gsolE.get();

          //2) separe internal and bnd points of the element
          const std::vector<int> &id_pt_face = group->closure(iFace, 0);
          std::map<int, SPoint3> pt_face;
          std::map<int, SPoint3> pt_interior;
          for(int i = 0; i < elem->getNumVertices(); i++) {
            std::vector<int>::const_iterator it = find(id_pt_face.begin(), id_pt_face.end(), i);
            if(it != id_pt_face.end())
              pt_face[i]     = elem->getVertex(i)->point();
            else
              pt_interior[i] = elem->getVertex(i)->point();
          }

          //3) get the analytical plane (ax+by+cz+d = 0)
	        group->normal(jacobians, iFace, 0, normals);
          SVector3 v_normal(normals(0, 0),  //a
                            normals(0, 1),  //b
                            normals(0, 2)); //c
          double d = - dot(SVector3(pt_face.begin()->second), v_normal);

          //4) compute average of virtual external symetric element
          for (int k = 0; k < nbFields; ++k) {
            double AVG = 0;
            for (std::map<int, SPoint3>::iterator it = pt_face.begin(); it != pt_face.end(); it++) {
              AVG += sol(it->first, k);
            }
            for (std::map<int, SPoint3>::iterator it = pt_interior.begin(); it != pt_interior.end(); it++) {
              SVector3 v_gsol(gsol(it->first, 0), gsol(it->first, 1), gsol(it->first, 2)); //FOR P1 !
              double normal_gradient = dot(v_normal, v_gsol);
              //perp distances of internal pnts to bnd plane
              double l = fabs( dot(v_normal, SVector3(it->second)) + d ) ; // / norm(v_normal)
              AVG += 2. * l * normal_gradient + sol(it->first, k);
            }
            AVG /= (double) (elem->getNumVertices());
            MINIn (0, iElementL * nbFields + k) = std::min ( AVG , MINIn  (0, iElementL * nbFields + k ) );
            MAXIn (0, iElementL * nbFields + k) = std::max ( AVG , MAXIn  (0, iElementL * nbFields + k ) );
          }
        }
      //}//
    }// else continue; // skip bifurcations
  }

  //----------------------------------------------------------   
  // then limit the solution  
  //----------------------------------------------------------   
  //
  dgFullMatrix<double> &solGroup = solution->getGroupProxy(groupId);
  dgFullMatrix<double> &MAXG = MAX.getGroupProxy(groupId);
  dgFullMatrix<double> &MING = MIN.getGroupProxy(groupId);
  fullMatrix<double> Temp;
  dataCacheMap cacheMap(dataCacheMap::NODE_MODE, &groups);
  cacheMap.setSolutionFunction(solution->getFunction(), solution->getFunctionGradient());
  dataCacheDouble *detector = (_detectorFunction ? cacheMap.get(_detectorFunction, NULL) : NULL);
  if (_detectorFunction) cacheMap.setGroup (goe);
  for (size_t iElement = 0 ; iElement < goe->getNbElements(); ++iElement)  {
    solGroup.getBlockProxy(iElement, Temp);
    if (_detectorFunction) {
      cacheMap.setElement(iElement);
    }
    for (int k = 0; k < nbFields; ++k)
    {
      if (_detectorFunction && detector->get()(0,k) < 0)
        continue;
      double AVG = 0.;
      double locMax = -1.e22;
      double locMin =  1.e22;
      double neighMax = MAXG (0, iElement*nbFields+k);
      double neighMin = MING (0, iElement*nbFields+k);
      for (int i = 0; i < fSize; ++i)
      {
        AVG += Temp(i,k);
        locMax = std::max (locMax, Temp (i,k));
        locMin = std::min (locMin, Temp (i,k));
      }
      AVG /= (double) fSize;

      //SLOPE LIMITING DG
      //-------------------
      double slopeLimiterValue = 1.0;
      if (locMax > neighMax && locMax != AVG) slopeLimiterValue = (neighMax-AVG) / (locMax-AVG);
      if (locMin < neighMin && locMin != AVG) slopeLimiterValue = std::min ( slopeLimiterValue , (AVG-neighMin) / (AVG-locMin) );
      if (AVG < neighMin) slopeLimiterValue = 0;
      if (AVG > neighMax) slopeLimiterValue = 0;

      if(slopeLimiterValue != 1.0)
        for (int i = 0; i < fSize; ++i)
          Temp(i,k) = AVG + (Temp(i,k) - AVG) * slopeLimiterValue;
    }
  }
  //  --- CLIPPING: check unphysical values
  fullMatrix<double> limitedSol;
  //dgFullMatrix<double> &solGroup = solution->getGroupProxy(groupId);

  //dataCacheMap cacheMap(dataCacheMap::NODE_MODE); //POINT_MODE ?
  cacheMap.setSolutionFunction(solution->getFunction(), solution->getFunctionGradient());
  cacheMap.setGroup (goe);

  dataCacheDouble *solutionEClipped = _claw->getClipToPhysics(goe->getPhysicalTag()) ?
                                          cacheMap.get(_claw->getClipToPhysics(goe->getPhysicalTag())) : NULL;
  if (solutionEClipped){
    for (size_t iElement=0; iElement<goe->getNbElements(); ++iElement) {
      cacheMap.setElement(iElement);
      solGroup.getBlockProxy(iElement, limitedSol);
      limitedSol.setAll(solutionEClipped->get());
    }
  }
  return 1; 
}

void dgSlopeLimiter::apply ( dgDofContainer *solution)
{
  const dgGroupCollection &groups = *solution->getGroups();
  solution->scatter(); //FIXME really needed ?? not good for scalability...
  int nbFields = solution->getNbFields();
  
  // first compute max and min of all fields for all stencils 
  //----------------------------------------------------------
  dgDofContainer MIN(groups, nbFields);
  dgDofContainer MAX(groups, nbFields);
  
  MIN.setAll ( 1.e22);
  MAX.setAll (-1.e22);
  
  int iElementL, iElementR, fSize=0;
  fullMatrix<double> TempL, TempR;
  
  for( int iGFace = 0; iGFace < groups.getNbFaceGroups(); iGFace++) {
    dgGroupOfFaces* group = groups.getFaceGroup(iGFace);
    
    if (group->nConnection() == 2) { // interior Faces
      
      const dgGroupOfElements *groupLeft = &group->elementGroup(0);
      const dgGroupOfElements *groupRight = &group->elementGroup(1);
      
      dgFullMatrix<double> &solleft = solution->getGroupProxy(groupLeft);
      dgFullMatrix<double> &solright = solution->getGroupProxy(groupRight);
      dgFullMatrix<double> &MINLeft = MIN.getGroupProxy(groupLeft);
      dgFullMatrix<double> &MAXLeft = MAX.getGroupProxy(groupLeft);
      dgFullMatrix<double> &MINRight = MIN.getGroupProxy(groupRight);
      dgFullMatrix<double> &MAXRight = MAX.getGroupProxy(groupRight);
      
      for(size_t iFace=0 ; iFace < group->size(); iFace++) {
	
        iElementL = group->elementId(iFace, 0);
        iElementR = group->elementId(iFace, 1);
        solleft.getBlockProxy(iElementL, TempL);
        solright.getBlockProxy(iElementR, TempR);
        fSize = TempL.size1();
        for (int k = 0; k < nbFields; ++k) {
          double AVGL = 0;
          double AVGR = 0;
          for (int i = 0; i < fSize; ++i) {
            AVGL += TempL(i,k);
            AVGR += TempR(i,k);
          }
          AVGL /= (double) fSize;
          AVGR /= (double) fSize;
          MINLeft  (0, iElementL * nbFields + k) = std::min ( AVGR , MINLeft  (0, iElementL * nbFields + k ) );
          MAXLeft  (0, iElementL * nbFields + k) = std::max ( AVGR , MAXLeft  (0, iElementL * nbFields + k ) );
          MINRight (0, iElementR * nbFields + k) = std::min ( AVGL , MINRight (0, iElementR * nbFields + k ) );
          MAXRight (0, iElementR * nbFields + k) = std::max ( AVGL , MAXRight (0, iElementR * nbFields + k ) );
        }
      }
      
    } else if(group->nConnection() == 1) { //exterior Faces
      
      const dgGroupOfElements *groupIn = &group->elementGroup(0);
      const function *bcF = _claw->getOutsideValue(group, function::getSolution()); //TODO: variable
      
      dataCacheMap cacheMap(dataCacheMap::NODE_MODE, &groups);
      cacheMap.setSolutionFunction(solution->getFunction(), solution->getFunctionGradient());
      
      dgFullMatrix<double> &MINIn = MIN.getGroupProxy(groupIn);
      dgFullMatrix<double> &MAXIn = MAX.getGroupProxy(groupIn);
      
      if(bcF) { // outside value boundary
        dataCacheDouble &bcE = *cacheMap.get(bcF, NULL);
        cacheMap.setInterfaceGroup(group,0);
        for(size_t iFace = 0; iFace < group->size(); iFace++) {
          cacheMap.setInterface(iFace);
          iElementL = group->elementId(iFace, 0);
          const fullMatrix<double> &bcM = bcE.get();
	  
          fSize = bcM.size1();
          for (int k = 0; k < nbFields; ++k) {
            double AVG = 0;
            for (int i = 0; i < fSize; ++i) {
              AVG += bcM(i,k);
            }
            AVG /= (double) fSize;
            MINIn (0, iElementL * nbFields + k) = std::min ( AVG , MINIn  (0, iElementL * nbFields + k ) );
            MAXIn (0, iElementL * nbFields + k) = std::max ( AVG , MAXIn  (0, iElementL * nbFields + k ) );
          }
        }
      } // else { // not outside value boundary
        //3D ! P1 ! Plane faces !
      
      
      dataCacheDouble &solE  = *cacheMap.get(solution->getFunction(), NULL);
      dataCacheDouble &gsolE = *cacheMap.get(solution->getFunctionGradient(), NULL);
      
      cacheMap.setGroup(groupIn);
      const dgMeshJacobian & jacobians = cacheMap.getJacobians();
      
      fullMatrix<double> normals;
      
      for(size_t iFace = 0; iFace < group->size(); iFace++) {
	//1) find element and bnd face in element
	iElementL = group->elementId(iFace, 0);
	cacheMap.setElement(iElementL);
	MElement * elem = cacheMap.element(0);
	const fullMatrix<double> & sol =  solE.get();
	const fullMatrix<double> &gsol = gsolE.get();
	
	//2) separe internal and bnd points of the element
	const std::vector<int> &id_pt_face = group->closure(iFace, 0);
	std::map<int, SPoint3> pt_face;
	std::map<int, SPoint3> pt_interior;
	for(int i = 0; i < elem->getNumVertices(); i++) {
	  std::vector<int>::const_iterator it = find(id_pt_face.begin(), id_pt_face.end(), i);
	  if(it != id_pt_face.end())
	    pt_face[i]     = elem->getVertex(i)->point();
	  else
	    pt_interior[i] = elem->getVertex(i)->point();
	}
	
	group->normal(jacobians, iFace, 0, normals);
	//3) get the analytical plane (ax+by+cz+d = 0)
	SVector3 v_normal(normals(0, 0),  //a
			  normals(0, 1),  //b
			  normals(0, 2)); //c
	double d = - dot(SVector3(pt_face.begin()->second), v_normal);
	
	//4) compute average of virtual external symetric element
	for (int k = 0; k < nbFields; ++k) {
	  double AVG = 0;
	  for (std::map<int, SPoint3>::iterator it = pt_face.begin(); it != pt_face.end(); it++) {
	    AVG += sol(it->first, k);
	  }
	  for (std::map<int, SPoint3>::iterator it = pt_interior.begin(); it != pt_interior.end(); it++) {
	    SVector3 v_gsol(gsol(it->first, 0), gsol(it->first, 1), gsol(it->first, 2)); //FOR P1 !
	    double normal_gradient = dot(v_normal, v_gsol);
	    //perp distances of internal pnts to bnd plane
	    double l = fabs( dot(v_normal, SVector3(it->second)) + d ) ; // / norm(v_normal)
	    AVG += 2. * l * normal_gradient + sol(it->first, k);
	  }
	  AVG /= (double) (elem->getNumVertices());
	  MINIn (0, iElementL * nbFields + k) = std::min ( AVG , MINIn  (0, iElementL * nbFields + k ) );
	  MAXIn (0, iElementL * nbFields + k) = std::max ( AVG , MAXIn  (0, iElementL * nbFields + k ) );
	}
      }
      //}//
    }// else continue; // skip bifurcations
  }
  
  //----------------------------------------------------------   
  // then limit the solution  
  //----------------------------------------------------------   
  
  for (int iGroup = 0 ; iGroup < groups.getNbElementGroups() ; iGroup++) {
    dgGroupOfElements &group = *groups.getElementGroup(iGroup);
    dgFullMatrix<double> &solGroup = solution->getGroupProxy(iGroup);
    dgFullMatrix<double> &MAXG = MAX.getGroupProxy(iGroup);
    dgFullMatrix<double> &MING = MIN.getGroupProxy(iGroup);
    fullMatrix<double> Temp;
    dataCacheMap cacheMap(dataCacheMap::NODE_MODE, &groups);
    cacheMap.setSolutionFunction(solution->getFunction(), solution->getFunctionGradient());
    dataCacheDouble *detector = (_detectorFunction ? cacheMap.get(_detectorFunction, NULL) : NULL);
    if (_detectorFunction) cacheMap.setGroup (&group);
    for (size_t iElement = 0 ; iElement < group.getNbElements(); ++iElement)  {
      solGroup.getBlockProxy(iElement, Temp);
      if (_detectorFunction) {
        cacheMap.setElement(iElement);
      }
      for (int k = 0; k < nbFields; ++k)
	{
	  if (_detectorFunction && detector->get()(0,k) < 0)
	    continue;
	  double AVG = 0.;
	  double locMax = -1.e22;
	  double locMin =  1.e22;
	  double neighMax = MAXG (0, iElement*nbFields+k);
	  double neighMin = MING (0, iElement*nbFields+k);
	  for (int i = 0; i < fSize; ++i)
	    {
	      AVG += Temp(i,k);
	      locMax = std::max (locMax, Temp (i,k));
	      locMin = std::min (locMin, Temp (i,k));
	    }
	  AVG /= (double) fSize;
	  
	  //SLOPE LIMITING DG
	  //-------------------
	  double slopeLimiterValue = 1.0;
	  if (locMax > neighMax && locMax != AVG) slopeLimiterValue = (neighMax-AVG) / (locMax-AVG);
	  if (locMin < neighMin && locMin != AVG) slopeLimiterValue = std::min ( slopeLimiterValue , (AVG-neighMin) / (AVG-locMin) );
	  if (AVG < neighMin) slopeLimiterValue = 0;
	  if (AVG > neighMax) slopeLimiterValue = 0;
	  
	  if(slopeLimiterValue != 1.0)
	    for (int i = 0; i < fSize; ++i)
	      Temp(i,k) = AVG + (Temp(i,k) - AVG) * slopeLimiterValue;
	}
    }
  }
  //  --- CLIPPING: check unphysical values
  fullMatrix<double> limitedSol;
  for (int iG = 0; iG < groups.getNbElementGroups(); iG++) {
    dgGroupOfElements* egroup = groups.getElementGroup(iG);
    dgFullMatrix<double> &solGroup = solution->getGroupProxy(iG);
    
    dataCacheMap cacheMap(dataCacheMap::NODE_MODE, &groups);
    cacheMap.setSolutionFunction(solution->getFunction(), solution->getFunctionGradient());
    cacheMap.setGroup (egroup);
    
    dataCacheDouble *solutionEClipped = _claw->getClipToPhysics(egroup->getPhysicalTag()) ?
      cacheMap.get(_claw->getClipToPhysics(egroup->getPhysicalTag())) : NULL;
    if (solutionEClipped){
      for (size_t iElement=0; iElement<egroup->getNbElements(); ++iElement) {
        cacheMap.setElement(iElement);
        solGroup.getBlockProxy(iElement, limitedSol);
        limitedSol.setAll(solutionEClipped->get());
      }
    }
  }
}

dgSlopeLimiterVertex::dgSlopeLimiterVertex(dgConservationLaw* claw, dgGroupCollection* groups, dgDofContainer* nodalVolume, bool useOptimalLimiter): dgLimiter(claw)
{
  _groups = groups;
  _CGStruct = new dgCGStructure(*_groups);
  _nodalVolume = nodalVolume;
  _optimal = useOptimalLimiter;
  
  if(nodalVolume == NULL) {
    _nodalVolume = new dgDofContainer(*_groups, 1);
    dgNodalVolume::compute(_nodalVolume);
    _ownNodalVolume = true;
  }
  else _ownNodalVolume = false;
}

bool dgSlopeLimiterVertex::boundaryValues(const dgGroupOfFaces * faces, dgDofContainer * solution, fullMatrix<double> & valueOnFaces,
                               const function * secondary) {
  if((size_t)valueOnFaces.size1() != faces->size() || valueOnFaces.size2() != _claw->getNbFields())
    Msg::Fatal("bad matrix sizes in slopeLimiter boundaryValue !");
  const function * variable = (secondary)? secondary : solution->getFunction();
  
  const dgGroupOfElements *groupIn = &faces->elementGroup(0);
  const function *bcF = _claw->getOutsideValue(faces, secondary);
  if (!bcF)
    bcF = _claw->getOutsideValue(faces, solution->getFunction());
  dataCacheMap cacheMap(dataCacheMap::NODE_MODE, solution->getGroups());
  cacheMap.setSolutionFunction(solution->getFunction(), solution->getFunctionGradient());
  
  if(bcF) { // Dirichlet BC => the given value is the min/max accepted
    dataCacheDouble &bcE = *cacheMap.get(bcF, NULL);
    cacheMap.setInterfaceGroup(faces, 0);
    for(size_t iFace = 0; iFace < faces->size(); iFace++) {
      cacheMap.setInterface(iFace);
      const fullMatrix<double> &bcM = bcE.get();
      for (int k = 0; k < bcM.size2(); k++) {
        fullVector<double> nVal;
        nVal.setAsProxy(bcM, k);
        valueOnFaces(iFace, k) = meanOnElement(nVal, *faces->getNodalBasis()/*, connectionIn.getElementId(iFace),*/);
        //printf("%e\n", valueOnFaces(iFace, k));        
      }
    }
  } else {
    dataCacheDouble &solE = *cacheMap.get(variable, NULL);
    dataCacheDouble &volE = *cacheMap.get(_nodalVolume->getFunction(), NULL);
    cacheMap.setGroup(groupIn);
    for(size_t iFace = 0; iFace < faces->size(); iFace++) {
      cacheMap.setElement(faces->elementId(iFace, 0));
      const std::vector<int> &closure = faces->closure(iFace, 0);
      const fullMatrix<double> &sol = solE.get();
      const fullMatrix<double> &vol = volE.get();
      double elementVolume = 0, faceVolume = 0;
      std::vector<double> elementMean(sol.size2()), faceMean(sol.size2());
      for (int i = 0; i < vol.size1(); ++i) {
        elementVolume += vol(i, 0);
        for (int k = 0; k < sol.size2(); ++k) {
          elementMean[k] += sol(i, k) * vol(i, 0);
        }
      }
      for (size_t i = 0; i < closure.size(); ++i) {
        faceVolume += vol(closure[i], 0);
        for (int k = 0; k < sol.size2(); ++k) {
          faceMean[k] += sol(closure[i], k) * vol(closure[i], 0);
        }
      }
      for (int k = 0; k < sol.size2(); ++k) {
        elementMean[k] /= elementVolume;
        faceMean[k] /= faceVolume;
        valueOnFaces(iFace, k) = faceMean[k] * 2 - elementMean[k];
      }
    }
  }
  return true;
}

dgSlopeLimiterVertex::~dgSlopeLimiterVertex()
{
  delete _CGStruct;
  if(_ownNodalVolume) delete _nodalVolume;
}


void checkViolation(fullVector<double> &w, fullVector<double> &x,
                    fullVector<double> &xMin,fullVector<double> &xMax,
                    double &totDev, double &devLB, double &devUB,
                    fullVector<double> &fixedLB,fullVector<double> &fixedUB ) {
  devLB = 0;
  devUB = 0;
  int n = w.size();
  double TOL = 1e-16;
  for ( int i = 0; i < n; i++ ) {
    if ( x(i) - xMin(i) < -TOL ) {
      fixedLB(i) = 1;
      devLB = devLB + (x(i) - xMin(i))*w(i);
    } else {
      fixedLB(i) = 0;
    }
    if ( x(i) - xMax(i) > TOL ) {
      fixedUB(i) = 1;
      devUB = devUB + (x(i) - xMax(i))*w(i);
    } else {
      fixedUB(i) = 0;
    }
  }
  totDev = devLB + devUB;
}

void fixBound(fullVector<double> &w,fullVector<double> &x,
              fullVector<double> &fixed,fullVector<double> &xBound, double deviation) {
  double wFree = 0;
  // set bounds
  int n = w.size();
  for ( int i = 0; i < n; i++ ) {
    if ( fixed(i) == 1 ) {
      x( i ) =  xBound( i );
    } else {
      wFree = wFree + w(i);
    }
  }
  // correct other to conserve mass
  double correction = deviation/wFree;
  for ( int i = 0; i < n; i++ ) {
    if ( fixed(i) == 0 )
      x( i ) =  x( i ) + correction;
  }
}

void solveMajorBounds(fullVector<double> &w, fullVector<double> &x,
                      fullVector<double> &xMin,fullVector<double> &xMax, int &iter) {
  fullVector<double> fixedLB(w.size()), fixedUB(w.size()), fixed(w.size());
  double totDev,devLB,devUB;
  int n = w.size();
  // check if solution is within bounds
  checkViolation(w,x,xMin,xMax,totDev,devLB,devUB,fixedLB,fixedUB);
  //     printf("totDev %f\n",totDev);
  //     printf("devLB %f\n",devLB);
  //     printf("devUB %f\n",devUB);
  //     fixedLB.print("fixedLB");
  //     fixedUB.print("fixedUB");
  //     x.print("x");
  if ( devUB == 0 && devLB == 0 ) {
    //     printf("Solved: %d\n",iter);
    return;
  } else if ( totDev > 0 ) {
    // fix the upper bounds
    fixBound(w,x,fixedUB,xMax,devUB);
    fixed.setAll(fixedUB);
  } else {
    // fix the lower bounds
    fixBound(w,x,fixedLB,xMin,devLB);
    fixed.setAll(fixedLB);
  }
  iter++;
  // re-check
  checkViolation(w,x,xMin,xMax,totDev,devLB,devUB,fixedLB,fixedUB);
  //     printf("totDev %f\n",totDev);
  //     printf("devLB %f\n",devLB);
  //     printf("devUB %f\n",devUB);
  //     fixedLB.print("fixedLB");
  //     fixedUB.print("fixedUB");
  //     x.print("x");
  if ( devUB == 0 && devLB == 0 ) {
    //     printf("Solved: %d\n",iter);
    return;
  } else {
    // recursion on free nodes !
    int nFree = 0;
    for ( int i = 0; i < n; i++ )
      if ( fixed(i) == 0 )
        nFree++;
    fullVector<double> wRec(nFree), xRec(nFree), xMinRec(nFree), xMaxRec(nFree);
    int iFree = 0;
    for ( int i = 0; i < n; i++ ) {
      if ( fixed(i) == 0 ) {
        wRec(iFree) = w(i);
        xRec(iFree) = x(i);
        xMaxRec(iFree) = xMax(i);
        xMinRec(iFree) = xMin(i);
        iFree++;
      }
    }
    // solve the sub problem
    solveMajorBounds(wRec,xRec,xMinRec,xMaxRec,iter);
    // copy back
    iFree = 0;
    for ( int i = 0; i < n; i++ ) {
      if ( fixed(i) == 0 ) {
        x(i) = xRec(iFree);
        iFree++;
      }
    }
  }
}  

/** Finds optimal limited solution for one element
 *  (x - x_0)^T D (x - x_0) = min!,  D = diag(w)
 *  s.t. w^T (x - x0) = 0     (mass conservation)
 *  and    xMin <= x <= xMax  (limits)                 */
void solveQPLimiter(fullVector<double> &w, fullVector<double> &x0,
                    fullVector<double> &xMin, fullVector<double> &xMax, int &iter) {
  solveMajorBounds(w,x0,xMin,xMax,iter);
}


void dgSlopeLimiterVertex::apply(dgDofContainer* solution)
{ 
  if (!_nodalVolume)
    Msg::Fatal("nodalVolume must be given");
  int nbFields = solution->getNbFields();
  dgDofContainer meanVals(*_groups, nbFields);
  dgDofContainer maxVals(*_groups, nbFields);
  dgDofContainer minVals(*_groups, nbFields);
  // find allowed extrema values for each vertex
  //  i.e. the min/max at the center of all surrounding elements
  // compute the mean for each element and store it in dof
  for (int iG = 0; iG < _groups->getNbElementGroups(); iG++) {
    dgGroupOfElements *group = _groups->getElementGroup(iG);
    int nbNodes = group->getNbNodes();
    const dgFullMatrix<double> &groupProxyData = solution->getGroupProxy(iG);
    dgFullMatrix<double> &groupProxyMean = meanVals.getGroupProxy(iG);
    dgFullMatrix<double> &groupProxyVol = _nodalVolume->getGroupProxy(iG);
    for (size_t iElem =0; iElem < group->getNbElements(); iElem++) {
      fullMatrix<double> elemData, elemMean, elemVol;
      groupProxyData.getBlockProxy(iElem, elemData);
      groupProxyMean.getBlockProxy(iElem, elemMean);
      groupProxyVol.getBlockProxy(iElem, elemVol);
      for ( int iField = 0; iField < nbFields; iField++ ) {
        double meanVal = 0;
        double vol = 0;
        for (int iNode = 0; iNode < nbNodes; iNode++ ) {
          meanVal += elemData(iNode, iField)*elemVol(iNode,0);
          vol += elemVol(iNode,0);
        }
        meanVal /= vol;
        for (int iNode = 0; iNode < nbNodes; iNode++ ) {
          elemMean(iNode, iField) = meanVal;
        }
      }
    }
  }
  maxVals.copy(meanVals);
  minVals.copy(meanVals);
  // on the boundary, compute the mean of the face elements and update min/max
  for( int iGFace = 0; iGFace < _groups->getNbFaceGroups(); iGFace++) {
    dgGroupOfFaces* faceGroup = _groups->getFaceGroup(iGFace);
    if(faceGroup->nConnection() == 1) {
      const dgGroupOfElements &groups = faceGroup->elementGroup(0);
      dgFullMatrix<double> &groupProxyMax = maxVals.getGroupProxy(&groups);
      dgFullMatrix<double> &groupProxyMin = minVals.getGroupProxy(&groups);
      const dgFullMatrix<double> &groupProxyData = solution->getGroupProxy(&groups); // Test
      
      //      int nbFaceNodes = faceGroup->getNbNodes();
      //      const dgFullMatrix<double> &groupProxyData = solution->getGroupProxy(&groups);
      //      for(int iFace = 0; iFace < faceGroup->size(); iFace++) {
      //        const std::vector<int> &closure = faceGroup->closure(iFace, 0);
      //        int iElem = faceGroup->elementId(iFace, 0);
      //        fullMatrix<double> elemData, elemMax, elemMin;
      //        groupProxyData.getBlockProxy(iElem, elemData);
      //        groupProxyMax.getBlockProxy(iElem, elemMax);
      //        groupProxyMin.getBlockProxy(iElem, elemMin);
      //        for (int iF = 0; iF < nbFields; ++iF) {
      //          double faceMeanVal = 0;
      //          for (int iNode = 0; iNode < nbFaceNodes; ++iNode) {
      //            faceMeanVal += elemData(closure[iNode], iF);
      //          }
      //          faceMeanVal /= (double) nbFaceNodes;
      //          for (int iNode = 0; iNode < nbFaceNodes; ++iNode) {
      //            elemMax(closure[iNode], iF) = std::max( faceMeanVal, elemMax(closure[iNode], iF) );
      //            elemMin(closure[iNode], iF) = std::min( faceMeanVal, elemMin(closure[iNode], iF) );
      //          }
      //        }
      //      }
      fullMatrix<double> valuesOnFaces(faceGroup->size(), nbFields);
      boundaryValues(faceGroup, solution, valuesOnFaces);
      for (size_t iFace = 0; iFace < faceGroup->size(); iFace++) {
        const std::vector<int> &closure = faceGroup->closure(iFace, 0);
        int iElem = faceGroup->elementId(iFace, 0);
        fullMatrix<double> elemMax, elemMin, elemData;
        groupProxyMax.getBlockProxy(iElem, elemMax);
        groupProxyMin.getBlockProxy(iElem, elemMin);
        groupProxyData.getBlockProxy(iElem, elemData);
        for (int iF = 0; iF < nbFields; iF++) {
          for (int iNode = 0; iNode < faceGroup->getNodalBasis()->points.size1(); iNode++) {
            elemMin(closure[iNode], iF) = std::min( elemMin(closure[iNode], iF), valuesOnFaces(iFace, iF) );
            elemMax(closure[iNode], iF) = std::max( elemMax(closure[iNode], iF), valuesOnFaces(iFace, iF) );
            //elemMin(closure[iNode], iF) = std::min( elemMin(closure[iNode], iF), elemData(closure[iNode], iF) );
            //elemMax(closure[iNode], iF) = std::max( elemMax(closure[iNode], iF), elemData(closure[iNode], iF) );
          }
        }
      }
    }
  }
  maxVals.scatter();
  minVals.scatter();
  // apply dgCGStructure.min/max to get min/max values for each CG node
  _CGStruct->max(&maxVals, &maxVals);
  _CGStruct->min(&minVals, &minVals);
  // limit slopes in each elem
  // 1D optimization: lambda = 1 -> original solution, lambda = 0 -> mean solution 
  for (int iG = 0; iG < _groups->getNbElementGroups(); iG++) {
    dgGroupOfElements *group = _groups->getElementGroup(iG);
    int nbNodes = group->getNbNodes();
    const dgFullMatrix<double> &groupProxyData = solution->getGroupProxy(iG);
    dgFullMatrix<double> &groupProxyMean = meanVals.getGroupProxy(iG);
    dgFullMatrix<double> &groupProxyMax = maxVals.getGroupProxy(iG);
    dgFullMatrix<double> &groupProxyMin = minVals.getGroupProxy(iG);
    dgFullMatrix<double> &groupProxyVol = _nodalVolume->getGroupProxy(iG);
    for (size_t iE =0; iE < group->getNbElements(); iE++) {
      fullMatrix<double> elemData, elemMean, elemMax, elemMin, elemVol;
      groupProxyData.getBlockProxy(iE, elemData);
      groupProxyMean.getBlockProxy(iE, elemMean);
      groupProxyMax.getBlockProxy(iE, elemMax);
      groupProxyMin.getBlockProxy(iE, elemMin);
      groupProxyVol.getBlockProxy(iE, elemVol);
      for ( int iF = 0; iF < nbFields; iF++ ) {
        if (_optimal) {
          // QP optimal limiter
          fullVector<double> x(nbNodes),w(nbNodes),xMin(nbNodes),xMax(nbNodes);
          for (int i = 0; i < nbNodes; i++ ) {
            x(i) = elemData(i,iF);
            xMin(i) = elemMin(i,iF);
            xMax(i) = elemMax(i,iF);
            w(i) = elemVol(i, 0);
          }
	  //         double w0[6] = {1.04, 0.97, 1.02, 0.98, 1.00, 0.96};
	  //         double x0[6] = {1.0, 2.0, 3.0, 3.0, 4.0, 5.0};
	  //         double xMin0[6] = {2,2.4,1,1,1,1};
	  //         double xMax0[6] = {5,5,3.0,5,5,3.43};
	  //         for (int i = 0; i < nbNodes; i++ ) {
	  //           x(i) = x0[i];
	  //           xMin(i) = xMin0[i];
	  //           xMax(i) = xMax0[i];
	  //           w(i) = w0[i];
	  //         }
          int iter = 0;
          solveQPLimiter(w,x,xMin,xMax,iter);
          for (int i = 0; i < nbNodes; i++ ) {
            elemData(i,iF) = x(i);
          }
        } else {
          // find maximum allowed lambda
          double elemLambda = 1.0;
          for (int iNode = 0; iNode < nbNodes; iNode++ ) {
            double lambda = 1.0;
            double nodalDeviation = elemData(iNode, iF) - elemMean(iNode,iF);
            double TOL = 1e-20;
            if ( nodalDeviation > TOL )
              lambda = ( elemMax(iNode, iF) - elemMean(iNode,iF) ) / nodalDeviation;
            else if ( nodalDeviation < -TOL )
              lambda = ( elemMin(iNode, iF) - elemMean(iNode,iF) ) / nodalDeviation;
            elemLambda = std::min(elemLambda, lambda);
          }
          // apply limiter
          if(elemLambda < 1.0)
            for (int iNode = 0; iNode < nbNodes; iNode++ ) {
              elemData(iNode, iF) = elemLambda * elemData(iNode, iF) + (1. - elemLambda) * elemMean(iNode, iF);
	          }
        }
      }
    }
  }
  solution->scatter();
}

//Same as previous function, adding the local properties behavior
void dgSlopeLimiterField::apply ( dgDofContainer *solution)
{
  const dgGroupCollection *groups = solution->getGroups();
  solution->scatter(); //FIXME really needed ?? not good for scalability...
  int nbFields = solution->getNbFields();
  
  // first compute max and min of all fields for all stencils 
  //----------------------------------------------------------
  dgDofContainer MIN(*groups, nbFields);
  dgDofContainer MAX(*groups, nbFields);
  
  MIN.setAll ( 1.e22);
  MAX.setAll (-1.e22);
  
  int iElementL=0, iElementR, fSize;
  
  for( int iGFace = 0; iGFace < groups->getNbFaceGroups(); iGFace++) {
    dgGroupOfFaces* group = groups->getFaceGroup(iGFace);
    
    if (group->nConnection() == 2) { // interior Faces
      
      const dgGroupOfElements *groupLeft = &group->elementGroup(0);
      const dgGroupOfElements *groupRight = &group->elementGroup(1);
      
      dataCacheMap cacheMapL(dataCacheMap::NODE_MODE, groups);
      dataCacheMap cacheMapR(dataCacheMap::NODE_MODE, groups);
      cacheMapL.setSolutionFunction(solution->getFunction(), NULL);
      cacheMapR.setSolutionFunction(solution->getFunction(), NULL);
      dataCacheDouble &solEL = *cacheMapL.get(solution->getFunction(), NULL);
      dataCacheDouble &solER = *cacheMapR.get(solution->getFunction(), NULL);
      cacheMapL.setGroup(groupLeft);
      cacheMapR.setGroup(groupRight);
      
      dgFullMatrix<double> &MINLeft  = MIN.getGroupProxy(groupLeft);
      dgFullMatrix<double> &MAXLeft  = MAX.getGroupProxy(groupLeft);
      dgFullMatrix<double> &MINRight = MIN.getGroupProxy(groupRight);
      dgFullMatrix<double> &MAXRight = MAX.getGroupProxy(groupRight);
      
      if(groupLeft->getPhysicalTag() == groupRight->getPhysicalTag()) { //same tag: nothing special
	
        for(size_t iFace = 0 ; iFace < group->size(); iFace++) {
          iElementL = group->elementId(iFace, 0);
          iElementR = group->elementId(iFace, 1);
          cacheMapL.setElement(iElementL);
          cacheMapR.setElement(iElementR);
	  
          const fullMatrix<double> & TempR = solER.get();
          const fullMatrix<double> & TempL = solEL.get();
	  
          fSize = TempL.size1();
          for (int k = 0; k < nbFields; ++k) {
            double AVGL = 0;
            double AVGR = 0;
            for (int i = 0; i < fSize; ++i) {
              AVGL += TempL(i, k);
              AVGR += TempR(i, k);
            }
            AVGL /= (double) fSize;
            AVGR /= (double) fSize;
            MINLeft  (0, iElementL * nbFields + k) = std::min ( AVGR , MINLeft  (0, iElementL * nbFields + k ) );
            MAXLeft  (0, iElementL * nbFields + k) = std::max ( AVGR , MAXLeft  (0, iElementL * nbFields + k ) );
            MINRight (0, iElementR * nbFields + k) = std::min ( AVGL , MINRight (0, iElementR * nbFields + k ) );
            MAXRight (0, iElementR * nbFields + k) = std::max ( AVGL , MAXRight (0, iElementR * nbFields + k ) );
          }
        }
      } else { // different tag ! Take left mat for right calc & vice versa
	
        fullMatrix<double> tempYL, tempYR;
        fullMatrix<double> TempL(cacheMapL.nPoint(), 1);
        fullMatrix<double> TempR(cacheMapR.nPoint(), 1);
	
        dataCacheDouble &solYL = *cacheMapL.get(_dofY->getFunction(), NULL);
        dataCacheDouble &solYR = *cacheMapR.get(_dofY->getFunction(), NULL);
        dataCacheDouble &xToYL = *cacheMapL.get(_xToY, NULL);
        dataCacheDouble &xToYR = *cacheMapR.get(_xToY, NULL);
        //dataCacheDouble &yToXL = *cacheMapL.get(_yToX, NULL);
        //dataCacheDouble &yToXR = *cacheMapR.get(_yToX, NULL);
	
        for(size_t iFace = 0 ; iFace < group->size(); iFace++) {
          iElementL = group->elementId(iFace, 0);
          iElementR = group->elementId(iFace, 1);
          cacheMapL.setElement(iElementL);
          cacheMapR.setElement(iElementR);
	  
          // stock values in H field
          fullMatrix<double> & YL = solYL.set();
          fullMatrix<double> & YR = solYR.set();
          tempYL = YL; //copies
          tempYR = YR;
          YL.setAll(xToYL.get());
          YR.setAll(xToYR.get());
	  
          // compute the modified values of Th 
          // use implicitely H in the function, and reverse the relation with other parameters
          //warn : cacheMap used only for the tags, otherwise unexpected problems possible...
          /*
	    std::vector<dataCacheDouble*> & depsL = yToXL.getDirectDependencies(); // HACK of the _eval() function
	    std::vector<dataCacheDouble*> & depsR = yToXR.getDirectDependencies();
	    for(size_t i = 0; i < depsL.size(); i++) {
            _yToX->arguments[i].val->setAsProxy(depsL[i]->get()); // get only the H field already _valid
	    }
	    _yToX->call(&cacheMapR, TempL); // inv !
	    for(size_t i = 0; i < depsR.size(); i++) {
            _yToX->arguments[i].val->setAsProxy(depsR[i]->get());
	    }
	    _yToX->call(&cacheMapL, TempR); // inv !
          */
          Msg::Error("this section has to be re-implemented");
	  
          // normal computation
          fSize = TempL.size1();
          for (int k = 0; k < nbFields; ++k) {
            double AVGL = 0;
            double AVGR = 0;
            for (int i = 0; i < fSize; ++i) {
              AVGL += TempL(i, k);
              AVGR += TempR(i, k);
            }
            AVGL /= (double) fSize;
            AVGR /= (double) fSize;
            MINLeft  (0, iElementL * nbFields + k) = std::min ( AVGR , MINLeft  (0, iElementL * nbFields + k ) );
            MAXLeft  (0, iElementL * nbFields + k) = std::max ( AVGR , MAXLeft  (0, iElementL * nbFields + k ) );
            MINRight (0, iElementR * nbFields + k) = std::min ( AVGL , MINRight (0, iElementR * nbFields + k ) );
            MAXRight (0, iElementR * nbFields + k) = std::max ( AVGL , MAXRight (0, iElementR * nbFields + k ) );
          }
          //restore the original H values
          YL.setAll(tempYL);
          YR.setAll(tempYR);
        }
      }
    } else if(group->nConnection() == 1) { //exterior Faces
      
      const dgGroupOfElements *groupIn = &group->elementGroup(0);
      
      const function *bcF = _claw->getOutsideValue(group, function::getSolution());
      
      dataCacheMap cacheMap(dataCacheMap::NODE_MODE, groups);
      cacheMap.setSolutionFunction(solution->getFunction(), solution->getFunctionGradient());
      
      dgFullMatrix<double> &MINIn = MIN.getGroupProxy(groupIn);
      dgFullMatrix<double> &MAXIn = MAX.getGroupProxy(groupIn);
      
      if(bcF) { // outside value boundary
        dataCacheDouble &bcE = *cacheMap.get(bcF, NULL);
        cacheMap.setInterfaceGroup(group,0);
        for(size_t iFace = 0; iFace < group->size(); iFace++) {
          cacheMap.setInterface(iFace);
          iElementL = group->elementId(iFace, 0);
          const fullMatrix<double> &bcM = bcE.get();
	  
          fSize = bcM.size1();
          for (int k = 0; k < nbFields; ++k) {
            double AVG = 0;
            for (int i = 0; i < fSize; ++i) {
              AVG += bcM(i,k);
            }
            AVG /= (double) fSize;
            MINIn (0, iElementL * nbFields + k) = std::min ( AVG , MINIn  (0, iElementL * nbFields + k ) );
            MAXIn (0, iElementL * nbFields + k) = std::max ( AVG , MAXIn  (0, iElementL * nbFields + k ) );
          }
        }
      } // else { // not outside value boundary
        //3D ! P1 ! Plane faces !
      
      dataCacheDouble &solE  = *cacheMap.get(solution->getFunction(), NULL);
      
      cacheMap.setGroup(groupIn);
      
      for(size_t iFace = 0; iFace < group->size(); iFace++) {
	//1) find element and bnd face in element
	iElementL = group->elementId(iFace, 0);
	cacheMap.setElement(iElementL);
	MElement * elem = cacheMap.element(0);
	const fullMatrix<double> & sol =  solE.get();
	//const fullMatrix<double> &gsol = gsolE.get();
	
	//2) separe internal and bnd points of the element
	const std::vector<int> &id_pt_face = group->closure(iFace, 0);
	std::map<int, SPoint3> pt_face;
	std::map<int, SPoint3> pt_interior;
	for(int i = 0; i < elem->getNumVertices(); i++) {
	  std::vector<int>::const_iterator it = find(id_pt_face.begin(), id_pt_face.end(), i);
	  if(it != id_pt_face.end())
	    pt_face[i]     = elem->getVertex(i)->point();
	  else
	    pt_interior[i] = elem->getVertex(i)->point();
	}
	
	fullVector<double> phantom(elem->getNumVertices());
	for(int k = 0; k < nbFields; ++k) {
	  int i_ph = 0;
	  switch(elem->getTypeForMSH()) {
	  case MSH_TRI_3:
	    //                Msg::Warning("Limiter not tested for this type of element (triangle)");
	  case MSH_TET_4:
	    {
	      int i_base = pt_interior.begin()->first;
	      phantom(i_ph++) = sol(i_base, k);
	      for(std::map<int, SPoint3>::iterator it = pt_face.begin(); it != pt_face.end(); it++) {
		double diff = sol(it->first, k) - sol(i_base, k);
		phantom(i_ph++) = sol(i_base, k) + diff * 2.;
	      }
	      break;
	    }
	  case MSH_PRI_6:
	    {
	      //                Msg::Warning("Limiter not tested for this type of element (triangle)");
	      for(std::map<int, SPoint3>::iterator it = pt_interior.begin(); it != pt_interior.end(); it++)
		phantom(i_ph++) = sol(it->first, k);
	      if(pt_face.size() == 3) { //faces 0-1-2 or 3-4-5
		int idFace = (pt_face.begin()->first > 2)? 3 : 0;
		int idNotFace = 3 - idFace;
		for(int i = 0; i < 3; i++) {
		  double diff = sol(i + idFace, k) - sol(i + idNotFace, k);
		  phantom(i_ph++) = sol(i + idNotFace, k) + diff * 2.;
		}
	      }
	      else { //faces 0-1-3-4 or 1-2-4-5 or 0-2-3-5
		int idHigh = pt_interior.begin()->first + ((pt_interior.begin()->first > 2)? 0 : 3) ;
		int idLow  = idHigh - 3;
		for(std::map<int, SPoint3>::iterator it = pt_face.begin(); it != pt_face.end(); it++) {
		  int idIn = (it->first > 2)? idHigh : idLow;
		  double diff = sol(it->first, k) - sol(idIn, k);
		  phantom(i_ph++) = sol(idIn, k) + diff * 2.;
		}
	      }
	      break;
	    }
	  case MSH_QUA_4: //faces 0-1 or 1-2 or 2-3 or 3-0
	    Msg::Warning("Limiter not tested for this type of element (quadrangle)");              
	    {
	      static const int cycle = 4 - 1; //(beg at 0)
	      int idHigh = 0;
	      int idLow = 1e9; // 0-1(3) or 1-2(3) or 2-3(1) or 3-0(2)
	      for(std::map<int, SPoint3>::iterator it = pt_interior.begin(); it != pt_interior.end(); it++) {
		idHigh = std::max(idHigh, it->first);
		idLow  = std::min(idLow,  it->first);
		phantom(i_ph++) = sol(it->first, k);
	      }
	      if(idHigh == cycle && idLow == 0) { idLow = cycle; idHigh = 0; }
	      for(std::map<int, SPoint3>::iterator it = pt_face.begin(); it != pt_face.end(); it++) {
		int idIn = (it->first - 1 == idHigh)? idHigh : (
								(it->first - 2 == idHigh)? idLow  : (
												     (it->first + cycle == idHigh)? idHigh : idLow  ));
		double diff = sol(it->first, k) - sol(idIn, k);
		phantom(i_ph++) = sol(idIn, k) + diff * 2.;
	      }
	      break;
	    }
	  case MSH_HEX_8:
	    //Msg::Warning("Limiter not tested for this type of element (hexahedron)");              
	    {
	      int idsFace[4], idsIn[4]; // it should be sorted as maps should containt sorted elms
	      int i = 0;
	      for(std::map<int, SPoint3>::iterator it = pt_interior.begin(); it != pt_interior.end(); it++) {
		phantom(i_ph++) = sol(it->first, k);
		idsIn[i++] = it->first;
	      }
	      // map of linked nodes
	      if(idsIn[0] == 0) {
		if(idsIn[1] == 1) {
		  if(idsIn[2] == 2)  { idsFace[0]=4; idsFace[1]=5; idsFace[2]=6; idsFace[3]=7; }
		  else               { idsFace[0]=3; idsFace[1]=2; idsFace[2]=7; idsFace[3]=6; }
		}
		else                 { idsFace[0]=1; idsFace[1]=2; idsFace[2]=5; idsFace[3]=6; }
	      }
	      else if(idsIn[0] == 1) { idsFace[0]=0; idsFace[1]=3; idsFace[2]=4; idsFace[3]=7; }
	      else if(idsIn[0] == 2) { idsFace[0]=1; idsFace[1]=0; idsFace[2]=5; idsFace[3]=4; }
	      else                   { idsFace[0]=0; idsFace[1]=1; idsFace[2]=2; idsFace[3]=3; }
	      
	      for(i = 0; i < 4; i++) {
		double diff = sol(idsFace[i], k) - sol(idsIn[i], k);
		phantom(i_ph++) = sol(idsIn[i], k) + diff * 2.;
	      }
	      break;
	    }
	  case MSH_PYR_5:
	    Msg::Warning("Limiter not tested for this type of element (pyramid)");              
	    {
	      if(pt_face.size() == 3) {
		for(std::map<int, SPoint3>::iterator it = pt_interior.begin(); it != pt_interior.end(); it++)
		  phantom(i_ph++) = sol(it->first, k);
		//map of linked nodes
		int idsIn[2], idsFace[2]; // the 3rd idFace is always 4
		idsIn[0] = pt_interior.begin()->first; // it should be sorted as maps should containt sorted elms
		idsFace[0] = pt_face.begin()->first;
		if(idsFace[0] == 0) {
		  if(idsIn[0] == 2)      { idsFace[1]=1; idsIn[0]=3; idsIn[1]=2; }
		  else                   { idsFace[1]=3; idsIn[0]=1; idsIn[1]=2; }
		}
		else if(idsFace[0] == 1) { idsFace[1]=2; idsIn[1]=3; }
		else                     { idsFace[1]=3; idsIn[0]=1; idsIn[1]=0; }
		
		for(int i = 0; i < 2; i++) {
		  double diff = sol(idsFace[i], k) - sol(idsIn[i], k);
		  phantom(i_ph++) = sol(idsIn[i], k) + diff * 2.;
		  diff = sol(4, k) - sol(idsIn[i], k);
		  phantom(i_ph++) = sol(idsIn[i], k) + diff * 2.;
		}
	      }
	      else {
		phantom(i_ph++) = sol(4, k);
		for(int i = 0; i < 4; i++) {
		  double diff = sol(i, k) - sol(4, k);
		  phantom(i_ph++) = sol(4, k) + diff * 2.;
		}
	      }
	    }
	  default:
	    Msg::Fatal("limiters are not implemented for this element type: %d", elem->getTypeForMSH() );
	    break;
	  }
	  
	  // mean value of phantom
	  double AVG = meanOnElement(phantom, *group->getNodalBasis()/*group, iElementL, phantom*/);
	  
	  MINIn (0, iElementL * nbFields + k) = std::min ( AVG , MINIn  (0, iElementL * nbFields + k ) );
	  MAXIn (0, iElementL * nbFields + k) = std::max ( AVG , MAXIn  (0, iElementL * nbFields + k ) );
	}
      }
    }// else continue; // skip bifurcations
  }
  
  //----------------------------------------------------------   
  // then limit the solution  
  //----------------------------------------------------------   
  
  for (int iGroup = 0 ; iGroup < groups->getNbElementGroups(); iGroup++) {
    dgGroupOfElements &group = *groups->getElementGroup(iGroup);
    dgFullMatrix<double> &solGroup = solution->getGroupProxy(iGroup);
    dgFullMatrix<double> &MAXG = MAX.getGroupProxy(iGroup);
    dgFullMatrix<double> &MING = MIN.getGroupProxy(iGroup);
    fullMatrix<double> Temp;
    
    dataCacheMap cacheMap(dataCacheMap::NODE_MODE, groups);
    cacheMap.setSolutionFunction(solution->getFunction(), solution->getFunctionGradient());
    dataCacheDouble *detector = (_detectorFunction ? cacheMap.get(_detectorFunction, NULL) : NULL);
    
    const nodalBasis &fs = group.getFunctionSpace();
    const dgIntegrationMatrices &intMatrices = dgIntegrationMatrices::get(fs.type, fs.order * 2 + 1);
    int nbQP = intMatrices.integrationPoints().size1();
    fullMatrix<double> &solProxy = solution->getGroupProxy(&group);
    fullMatrix<double> solutionQP(nbQP,group.getNbElements() * nbFields);
    intMatrices.psi().mult(solProxy, solutionQP);
    const fullMatrix<double> &detJ = cacheMap.getJacobians().detJElement(iGroup);
    
    if (_detectorFunction)
      cacheMap.setGroup (&group);
    
    for (size_t iElement = 0 ; iElement < group.getNbElements(); ++iElement)  {
      solGroup.getBlockProxy(iElement, Temp);
      fSize = Temp.size1();
      if (_detectorFunction)
        cacheMap.setElement(iElement);
      for (int k = 0; k < nbFields; ++k)
	{
	  if (_detectorFunction && detector->get()(0, k) < 0)
	    continue;
	  double neighMax = MAXG (0, iElement * nbFields + k);
	  double neighMin = MING (0, iElement * nbFields + k);
	  double locMax = -1.e22;
	  double locMin =  1.e22;
	  double AVG = 0., surf = 0.;
	  for (int i = 0; i < fSize; ++i)
	    {
	      //          AVG2 += Temp(i, k);
	      locMax = std::max (locMax, Temp(i, k));
	      locMin = std::min (locMin, Temp(i, k));
	    }
	  //        AVG2 /= (double) fSize;
	  for(int iPt = 0; iPt < nbQP; iPt++) {
	    const double w = intMatrices.integrationWeights()(iPt);
	    AVG  += solutionQP(iPt, iElement * nbFields + k) * detJ(iPt, iElementL) * w;
	    surf += detJ(iPt, iElementL) * w;
	  }
	  AVG /= surf;
	  
	  //        printf("AVG - AVG2 = %e\n", AVG - AVG2);
	  
	  
	  // SLOPE LIMITING DG
	  //-------------------
	  double slopeLimiterValue = 1.0;
	  if (locMax > neighMax && locMax != AVG) slopeLimiterValue = (neighMax - AVG) / (locMax - AVG);
	  if (locMin < neighMin && locMin != AVG) slopeLimiterValue = std::min ( slopeLimiterValue , (AVG - neighMin) / (AVG - locMin) );
	  if (AVG < neighMin) slopeLimiterValue = 0;
	  if (AVG > neighMax) slopeLimiterValue = 0;
	  
	  //slopeLimiterValue = 1.0;
	  if(slopeLimiterValue != 1.0)
	    for (int i = 0; i < fSize; ++i)
	      Temp(i,k) = AVG + (Temp(i,k) - AVG) * slopeLimiterValue; // ! dont work with bilinear forms ! :(
	}
    }
  }
  
  //  --- CLIPPING: check unphysical values
  fullMatrix<double> limitedSol;
  for (int iG = 0; iG < groups->getNbElementGroups(); iG++) {
    dgGroupOfElements* egroup = groups->getElementGroup(iG);
    dgFullMatrix<double> &solGroup = solution->getGroupProxy(iG);
    
    dataCacheMap cacheMap(dataCacheMap::NODE_MODE, groups); //POINT_MODE ?
    cacheMap.setSolutionFunction(solution->getFunction(), solution->getFunctionGradient());
    dataCacheDouble *solutionEClipped = _claw->getClipToPhysics(egroup->getPhysicalTag()) ?
      cacheMap.get(_claw->getClipToPhysics(egroup->getPhysicalTag())) : NULL;
    cacheMap.setGroup (egroup);
    if (solutionEClipped){
      for (size_t iElement = 0; iElement < egroup->getNbElements(); ++iElement) {
        cacheMap.setElement(iElement);
        solGroup.getBlockProxy(iElement, limitedSol);
        limitedSol.setAll(solutionEClipped->get());
      }
    }
  }
}



//Same as previous function, adding the local properties behavior
class dgSlopeLimiterField2::replacedFunction : public function {
  functionReplace yReplace;
  fullMatrix<double> toReplace, & _replaceBy, solution;
public:
  replacedFunction(dgConservationLaw *claw, const function *toRepF, fullMatrix<double> &replaceBy, const function *f) : 
    function(claw->getNbFields()), _replaceBy(replaceBy) {
    yReplace.addChild();
    addFunctionReplace(yReplace);
    yReplace.replace(toReplace, toRepF);
    yReplace.get(solution, f);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    for(int i = 0; i < toReplace.size1(); i ++)
      for(int j = 0; j < toReplace.size2(); j ++)
	toReplace(i, j) = _replaceBy(i, j);
    yReplace.compute();
    for(int i = 0; i < val.size1(); i++)
      for(int j = 0; j < val.size2(); j++)
	val(i, j) = solution(i, j);
  }
};

dgSlopeLimiterField2::dgSlopeLimiterField2 (dgConservationLaw *claw, dgGroupCollection* groups, function *xToY, function *y,
                                            function *yToX) : dgLimiter (claw), _xToY(xToY), _barycenter(false) {
  _yToX = new replacedFunction(claw, y, _replaceBy, yToX);
  _CGStruct = new dgCGStructure(*groups);
  _nodalVolume = new dgDofContainer(*groups, 1);
  dgNodalVolume::compute(_nodalVolume);
}
dgSlopeLimiterField2::~dgSlopeLimiterField2() {
  if(_yToX) delete _yToX;
  delete _CGStruct;
  delete _nodalVolume;
}

void dgSlopeLimiterField2::apply(dgDofContainer* solution)
{
  const dgGroupCollection* groups = solution->getGroups();
  
  // create a fullmatrix(nVertex, nfields) of min, max per physical label (string)
  /*  int nbVertices = groups->getModel()->getNumMeshVertices();
      int nbFields = solution->getNbFields();
      fullMatrix<double> vertexMin(nbVertices, nbFields);
      fullMatrix<double> vertexMax(nbVertices, nbFields);
      vertexMax.setAll(-std::numeric_limits<double>::max());
      vertexMin.setAll( std::numeric_limits<double>::max());*/
  
  int nbFields = solution->getNbFields();
  dgDofContainer meanVals(*groups, nbFields);
  dgDofContainer maxVals (*groups, nbFields);
  dgDofContainer minVals (*groups, nbFields);
  
  // find allowed extrema values for each vertex
  //  i.e. the min/max at the center of all surrounding elements
  
  if(_barycenter) {
    dataCacheMap cacheMapMean(dataCacheMap::POINT_MODE, groups);
    cacheMapMean.setSolutionFunction(solution->getFunction(), solution->getFunctionGradient());
    dataCacheDouble &solValCache = *cacheMapMean.get(_xToY);
    
    for (int iG = 0; iG < groups->getNbElementGroups(); iG++) {
      
      dgGroupOfElements *group = groups->getElementGroup(iG);
      int nbNodes = group->getNbNodes();
      dgFullMatrix<double> &groupProxyMean = meanVals.getGroupProxy(iG);
      
      for (size_t iE = 0; iE < group->getNbElements(); iE++) {
	
        const fullMatrix<double> &elemMeanG = solValCache.get();
        fullMatrix<double> elemMean;
        groupProxyMean.getBlockProxy(iE, elemMean);
	
	//        MElement* elem = group->getElement(iE);
        cacheMapMean.setPointBarycenter(group, iE);
	
        for ( int iF = 0; iF < nbFields; iF++ ) {
	  
          const double elemMeanF = elemMeanG(0, iF);
	  
          for (int iNode = 0; iNode < nbNodes; iNode++ ) {
	    //            int vertexID = elem->getVertex(iNode)->getNum() - 1;
	    //            vertexMin(vertexID, iF) = std::min( vertexMin(vertexID, iF), elemMeanF - 1e-10 );
	    //            vertexMax(vertexID, iF) = std::max( vertexMax(vertexID, iF), elemMeanF + 1e-10 );
            elemMean(iNode, iF) = elemMeanF;
          }
        }
      }
    }
    
  } else { // normal mean (not barycenter)
    
    dataCacheMap cacheMapMean(dataCacheMap::NODE_MODE, groups);
    cacheMapMean.setSolutionFunction(solution->getFunction(), solution->getFunctionGradient());
    dataCacheDouble &solValCache = *cacheMapMean.get(_xToY);
    
    for (int iG = 0; iG < groups->getNbElementGroups(); iG++) {
      
      const dgFullMatrix<double> &groupProxyVol = _nodalVolume->getGroupProxy(iG);
      dgFullMatrix<double> &groupProxyMean = meanVals.getGroupProxy(iG);
      dgGroupOfElements *group = groups->getElementGroup(iG);
      int nbNodes = group->getNbNodes();
      cacheMapMean.setGroup(group);
      
      for (size_t iE = 0; iE < group->getNbElements(); iE++) {
	
        fullMatrix<double> elemMean, elemVol;
        groupProxyMean.getBlockProxy(iE, elemMean);
        groupProxyVol.getBlockProxy (iE, elemVol);
	
        cacheMapMean.setElement(iE);
        const fullMatrix<double> &elemData = solValCache.get();
	
        double vol = 0.;
        for (int iNode = 0; iNode < nbNodes; iNode++ )
          vol += elemVol(iNode, 0);
	
        for ( int iF = 0; iF < nbFields; iF++ ) {
	  
          double meanVal = 0.;
	  
          for (int iNode = 0; iNode < nbNodes; iNode++ ) {
            //vertexMin(vertexID, iF) = std::min( vertexMin(vertexID, iF), elemMean - 1e-10 );
            //vertexMax(vertexID, iF) = std::max( vertexMax(vertexID, iF), elemMean + 1e-10 );
            meanVal += elemData(iNode, iF) * elemVol(iNode, 0);
          }
          meanVal /= vol;
	  
          for (int iNode = 0; iNode < nbNodes; iNode++ ) {
            elemMean(iNode, iF) = meanVal;
          }
        }
      }
    }
  }
  maxVals.copy(meanVals);
  minVals.copy(meanVals);
  
  // boundaries
  for (int iGFace = 0; iGFace < groups->getNbFaceGroups(); iGFace++) {
    dgGroupOfFaces* faces = groups->getFaceGroup(iGFace);
    if(faces->nConnection() != 1)
      continue; // only boundaries
    
    // old code
    /*    const dgGroupOfElements &groups = faces->elementGroup(0);
	  int nbFaceNodes = faces->getNbNodes();
	  dataCacheDouble &solValCache = *cacheMapMean.get(_xToY);
	  dataCacheDouble &solValCacheTmp = *cacheMapMean.get(solution->getFunction());
	  cacheMapMean.setInterfaceGroup(faces);
	  //const dgFullMatrix<double> &groupProxyData = solution->getGroupProxy(&groups);
	  for(int iFace = 0; iFace < faces->size(); iFace++) {
	  cacheMapMean.setInterface(iFace);
	  const std::vector<int> &closure = faces->closure(iFace, 0);
	  int iElem = faces->elementId(iFace, 0);
	  MElement& el = faces->element(iFace, 0);
	  //fullMatrix<double> elemData;
	  //groupProxyData.getBlockProxy(iElem, elemData);
	  for (int iF = 0; iF < nbFields; ++iF) {
	  //double faceMeanVal = 0.;
	  //for (int iNode = 0; iNode < nbFaceNodes; ++iNode) {
	  //  faceMeanVal += elemData(closure[iNode], iF);
	  //}
	  //faceMeanVal /= (double) nbFaceNodes;
	  double faceMeanVal = solValCache.get()(0, iF);
	  for (int iNode = 0; iNode < nbFaceNodes; ++iNode) {
          int vertexID = el.getVertex(closure[iNode])->getNum() - 1;
          vertexMax(vertexID, iF) = std::max( faceMeanVal, vertexMax(vertexID, iF) );
          vertexMin(vertexID, iF) = std::min( faceMeanVal, vertexMin(vertexID, iF) );
	  }
	  }
	  }*/
    
    // new code : cancel the limiter "only" in the direction perpendicular to the plane
    const dgGroupOfElements &elems = faces->elementGroup(0);
    dgFullMatrix<double> &groupProxyMax = maxVals.getGroupProxy(&elems);
    dgFullMatrix<double> &groupProxyMin = minVals.getGroupProxy(&elems);
    
    fullMatrix<double> valuesOnFaces(faces->size(), nbFields);
    boundaryValues(faces, solution, valuesOnFaces, _xToY);
    for (size_t iFace = 0; iFace < faces->size(); iFace++) {
      const std::vector<int> closure = faces->closure(iFace, 0);
      const int iElem = faces->elementId(iFace, 0);
      fullMatrix<double> elemMax, elemMin;
      groupProxyMax.getBlockProxy(iElem, elemMax);
      groupProxyMin.getBlockProxy(iElem, elemMin);
      //      MElement& el = faces->element(iFace, 0);
      for (int iF = 0; iF < nbFields; iF++) {
        for (size_t iNode = 0; iNode < closure.size(); iNode++) {
	  //          int vertexID = el.getVertex(closure[iPt])->getNum() - 1;
          elemMin(closure[iNode], iF) = std::min( elemMin(closure[iNode], iF), valuesOnFaces(iFace, iF) );
          elemMax(closure[iNode], iF) = std::max( elemMax(closure[iNode], iF), valuesOnFaces(iFace, iF) );
        }
      }
    }
  }
  maxVals.scatter();
  minVals.scatter();
  // apply dgCGStructure.min/max to get min/max values for each CG node
  _CGStruct->max(&maxVals, &maxVals);
  _CGStruct->min(&minVals, &minVals);
  
  //printf("vertexmin: ");
  //vertexMin.print();
  //printf("vertexmax: ");
  //vertexMax.print();
  //double max = -1e100;
  //for(int i=0; i < vertexMax.size1(); i++)
  //  max = std::max(max, vertexMax(i, 0));
  //printf("vertexmax: %e\n", max);
  
  // limit slopes in each elem
  // 1D optimization: lambda = 1 -> original solution, lambda = 0 -> mean solution 
  dataCacheMap cacheMap(dataCacheMap::NODE_MODE, groups);
  cacheMap.setSolutionFunction(solution->getFunction(), solution->getFunctionGradient());
  dataCacheDouble &yToXCD = *cacheMap.get(_yToX);
  for (int iG = 0; iG < groups->getNbElementGroups(); iG++) {
    dgGroupOfElements *group = groups->getElementGroup(iG);
    int nbNodes = group->getNbNodes();
    cacheMap.setGroup(group);
    const dgFullMatrix<double> &groupProxyData = solution->getGroupProxy(iG);
    const dgFullMatrix<double> &groupProxyMean = meanVals.getGroupProxy(iG);
    const dgFullMatrix<double> &groupProxyMax = maxVals.getGroupProxy(iG);
    const dgFullMatrix<double> &groupProxyMin = minVals.getGroupProxy(iG);
    const dgFullMatrix<double> &groupProxyVol = _nodalVolume->getGroupProxy(iG);
    
    _replaceBy.resize(nbNodes, nbFields);
    
    for (size_t iE = 0; iE < group->getNbElements(); iE++) {
      //      MElement* elem = group->getElement(iE);
      cacheMap.setElement(iE);
      fullMatrix<double> elemData, elemMean, elemMax, elemMin, elemVol;
      groupProxyData.getBlockProxy(iE, elemData);
      groupProxyMean.getBlockProxy(iE, elemMean);
      groupProxyMax.getBlockProxy(iE, elemMax);
      groupProxyMin.getBlockProxy(iE, elemMin);
      groupProxyVol.getBlockProxy(iE, elemVol);
      
      // use of funtionReplace to transfer nodal values to the discontinuous formulation
      _replaceBy.setAll(elemMin);
      /*      for (int iNode = 0; iNode < nbNodes; iNode++ ) {
      //        int vertexID = elem->getVertex(iNode)->getNum() - 1;
      for ( int iF = 0; iF < nbFields; iF++ ) {
      _replaceBy(iNode, iF) = elemMin(iNode, iF); //vertexMin(vertexID, iF);
      }
      }*/
      const fullMatrix<double> proxyMin = yToXCD.get(); //copy operator !
      cacheMap.setElement(iE); // TODO necessary to reinit replace ?? or .get() sufficient ? unvalidate ?
      _replaceBy.setAll(elemMax);
      /*      for (int iNode = 0; iNode < nbNodes; iNode++ ) {
      //        int vertexID = elem->getVertex(iNode)->getNum() - 1;
      for ( int iF = 0; iF < nbFields; iF++ ) {
      _replaceBy(iNode, iF) = elemMax(iNode, iF); //vertexMax(vertexID, iF);
      }
      }*/
      const fullMatrix<double> &proxyMax = yToXCD.get(); //not copy operator
      
      for ( int iF = 0; iF < nbFields; iF++ ) {
        // find maximum allowed lambda
        double elemLambda = 1.;
        for (int iNode = 0; iNode < nbNodes; iNode++ ) {
          double lambda = 1.;
          //if(_replaceBy(iNode, iF) > 0.)  // linked to Richards equation !! zero-slope=bugs
          //  nodalDeviation = 0;
          //else
          //  nodalDeviation = elemData(iNode, iF) - elemMean(iNode, iF);
	  
          //if(_replaceBy(iNode, iF) > 0.)
          //  printf("%e\n", proxyMax(iNode, iF));
	  
          double nodalDeviation = elemData(iNode, iF) - elemMean(iNode, iF);
          if ( fabs(nodalDeviation) > 1e-14 * elemMean(iNode, iF) ) {
            if ( nodalDeviation > 0 )
              lambda = ( proxyMax(iNode, iF) - elemMean(iNode, iF) ) / nodalDeviation;
            else
              lambda = ( proxyMin(iNode, iF) - elemMean(iNode, iF) ) / nodalDeviation;
            elemLambda = std::max(0., std::min(elemLambda, lambda)); // 0<L<1 for instabilities in ret curve
          }
        }
	
        // apply limiter
        if(elemLambda < 1.) // if ==1, no change !
          for (int iNode = 0; iNode < nbNodes; iNode++ )
            elemData(iNode, iF) = elemLambda * elemData(iNode, iF) + (1. - elemLambda) * elemMean(iNode, iF);
      }
    }
  }
}

double dgLimiter::meanOnElement (const fullVector<double> nodalValues, const nodalBasis &fs) {
  double AVG = 0.;
  if (fs.order < 2) {
    for(int i = 0; i < nodalValues.size(); i++)
      AVG += nodalValues(i);
  } else  Msg::Fatal("not implemented");
  /*const dgIntegrationMatrices &intMatrices = dgIntegrationMatrices::get(fs.type, fs.order * 2 + 1);
    const fullMatrix<double> &detJ = group->getGroupCollection()->_mesh->getJacobian(fs.order * 2 + 1).detJElement(group->getGroupCollection()->getElementGroupId(group));
    int nbQPint = intMatrices.integrationPoints().size1();
    fullVector<double> solQP(nbQPint);
    intMatrices.psi().mult(nodalValues, solQP); // projection on integration points
    double surf = 0.;
    for(int iPt = 0; iPt < nbQPint; iPt++) {
    AVG += solQP(iPt) * detJ(iPt, iElement) * intMatrices.integrationWeights()(iPt, 0);
    surf += detJ(iPt, iElement) * intMatrices.integrationWeights()(iPt, 0);
    }
    return AVG / surf;*/
  return AVG / nodalValues.size();
}

bool dgLimiter::boundaryValues(const dgGroupOfFaces * faces, dgDofContainer * solution, fullMatrix<double> & valueOnFaces,
                               const function * secondary) {
  if((size_t)valueOnFaces.size1() != faces->size() || valueOnFaces.size2() != _claw->getNbFields())
    Msg::Fatal("bad matrix sizes in slopeLimiter boundaryValue !");
  const function * variable = (secondary)? secondary : solution->getFunction();
  
  const dgGroupOfElements *groupIn = &faces->elementGroup(0);
  const function *bcF = _claw->getOutsideValue(faces, secondary);
  if (!bcF)
    bcF = _claw->getOutsideValue(faces, solution->getFunction());
  dataCacheMap cacheMap(dataCacheMap::NODE_MODE, solution->getGroups());
  cacheMap.setSolutionFunction(solution->getFunction(), solution->getFunctionGradient());
  
  if(bcF) { // Dirichlet BC => the given value is the min/max accepted
    dataCacheDouble &bcE = *cacheMap.get(bcF, NULL);
    cacheMap.setInterfaceGroup(faces, 0);
    for(size_t iFace = 0; iFace < faces->size(); iFace++) {
      cacheMap.setInterface(iFace);
      const fullMatrix<double> &bcM = bcE.get();
      for (int k = 0; k < bcM.size2(); k++) {
        fullVector<double> nVal;
        nVal.setAsProxy(bcM, k);
        valueOnFaces(iFace, k) = meanOnElement(nVal, *faces->getNodalBasis()/*, connectionIn.getElementId(iFace),*/);
        //printf("%e\n", valueOnFaces(iFace, k));        
      }
    }
  } else {
    dataCacheDouble &solE = *cacheMap.get(variable, NULL);
    cacheMap.setGroup(groupIn);
    for(size_t iFace = 0; iFace < faces->size(); iFace++) {
      cacheMap.setElement(faces->elementId(iFace, 0));
      MElement * elem = cacheMap.element(0);
      const fullMatrix<double> & sol =  solE.get();

      // separe points inside the domain and bnd points
      const std::vector<int> &id_pt_face = faces->closure(iFace, 0);
      std::map<int, SPoint3> pt_face;
      std::map<int, SPoint3> pt_interior;
      for(int i = 0; i < elem->getNumVertices(); i++) {
        std::vector<int>::const_iterator it = find(id_pt_face.begin(), id_pt_face.end(), i);
        if(it != id_pt_face.end())
          pt_face[i]     = elem->getVertex(i)->point();
        else
          pt_interior[i] = elem->getVertex(i)->point();
      }
      
      // function of the element type, compute a virtual extension of the solution outside the domain
      fullVector<double> phantom(elem->getNumVertices());
      for(int k = 0; k < _claw->getNbFields(); ++k) {
        switch(elem->getTypeForMSH()) {
	case MSH_LIN_2:
          {
            int i_int = pt_interior.begin()->first;
            int i_ext = pt_face.begin()->first;
            phantom(i_int) = sol(i_int, k);
            double diff = sol(i_ext, k) - sol(i_int, k);
            phantom(i_ext) = sol(i_ext, k) + diff * 2.;
	    //            if(i_int==1) {printf("%e %e\n",sol(i_int,0),sol(i_ext,0)); phantom.print(); }
            break;
          }
	  
	case MSH_TRI_3:
	case MSH_TET_4:
          {
            int i_base = pt_interior.begin()->first;
            phantom(i_base) = sol(i_base, k);
            for(std::map<int, SPoint3>::iterator it = pt_face.begin(); it != pt_face.end(); it++) {
              double diff = sol(it->first, k) - sol(i_base, k);
              phantom(it->first) = sol(i_base, k) + diff * 2.;
            }
            break;
          }
	case MSH_PRI_6:
          {
            for(std::map<int, SPoint3>::iterator it = pt_interior.begin(); it != pt_interior.end(); it++)
              phantom(it->first) = sol(it->first, k);
            if(pt_face.size() == 3) { //face 0-1-2 or 3-4-5
              int idFace = (pt_face.begin()->first > 2)? 3 : 0;
              int idNotFace = 3 - idFace;
              for(int i = 0; i < 3; i++) {
                double diff = sol(i + idFace, k) - sol(i + idNotFace, k);
                phantom(i + idFace) = sol(i + idNotFace, k) + diff * 2.;
              }
            }
            else { //face 0-1-3-4 or 1-2-4-5 or 0-2-3-5
              int idHigh = pt_interior.begin()->first + ((pt_interior.begin()->first > 2)? 0 : 3) ;
              int idLow  = idHigh - 3;
              for(std::map<int, SPoint3>::iterator it = pt_face.begin(); it != pt_face.end(); it++) {
                int idIn = (it->first > 2)? idHigh : idLow;
                double diff = sol(it->first, k) - sol(idIn, k);
                phantom(it->first) = sol(idIn, k) + diff * 2.;
              }
            }
            break;
          }
	case MSH_QUA_4: //faces 0-1 or 1-2 or 2-3 or 3-0
	  Msg::Warning("Limiter not tested for this type of element (quadrangle)");              
          {
            static const int cycle = 4 - 1; //(beg at 0)
            int idHigh = 0;
            int idLow = 1e9; // 0-1(3) or 1-2(3) or 2-3(1) or 3-0(2)
            for(std::map<int, SPoint3>::iterator it = pt_interior.begin(); it != pt_interior.end(); it++) {
              idHigh = std::max(idHigh, it->first);
              idLow  = std::min(idLow,  it->first);
              phantom(it->first) = sol(it->first, k);
            }
            if(idHigh == cycle && idLow == 0) { idLow = cycle; idHigh = 0; }
            for(std::map<int, SPoint3>::iterator it = pt_face.begin(); it != pt_face.end(); it++) {
              int idIn = (it->first - 1 == idHigh)? idHigh : (
							      (it->first - 2 == idHigh)? idLow  : (
												   (it->first + cycle == idHigh)? idHigh : idLow  ));
              double diff = sol(it->first, k) - sol(idIn, k);
              phantom(it->first) = sol(idIn, k) + diff * 2.;
            }
            break;
          }
	case MSH_HEX_8:
	  //Msg::Warning("Limiter not tested for this type of element (hexahedron)");              
          {
            int idsFace[4], idsIn[4]; // it should be sorted as maps should containt sorted elms
            int i = 0;
            for(std::map<int, SPoint3>::iterator it = pt_interior.begin(); it != pt_interior.end(); it++) {
              phantom(it->first) = sol(it->first, k);
              idsIn[i++] = it->first;
            }
            // map of linked nodes
            if(idsIn[0] == 0) {
              if(idsIn[1] == 1) {
                if(idsIn[2] == 2)  { idsFace[0]=4; idsFace[1]=5; idsFace[2]=6; idsFace[3]=7; }
                else               { idsFace[0]=3; idsFace[1]=2; idsFace[2]=7; idsFace[3]=6; }
              }
              else                 { idsFace[0]=1; idsFace[1]=2; idsFace[2]=5; idsFace[3]=6; }
            }
            else if(idsIn[0] == 1) { idsFace[0]=0; idsFace[1]=3; idsFace[2]=4; idsFace[3]=7; }
            else if(idsIn[0] == 2) { idsFace[0]=1; idsFace[1]=0; idsFace[2]=5; idsFace[3]=4; }
            else                   { idsFace[0]=0; idsFace[1]=1; idsFace[2]=2; idsFace[3]=3; }
	    
            for(i = 0; i < 4; i++) {
              double diff = sol(idsFace[i], k) - sol(idsIn[i], k);
              phantom(idsFace[i]) = sol(idsIn[i], k) + diff * 2.;
            }
            break;
          }
	case MSH_PYR_5:
	  Msg::Warning("Limiter not tested for this type of element (pyramid)");              
          {
            if(pt_face.size() == 3) {
              for(std::map<int, SPoint3>::iterator it = pt_interior.begin(); it != pt_interior.end(); it++)
                phantom(it->first) = sol(it->first, k);
              //map of linked nodes
              int idsIn[2], idsFace[2]; // the 3rd idFace is always 4
              idsIn[0] = pt_interior.begin()->first; // it should be sorted as maps should containt sorted elms
              idsFace[0] = pt_face.begin()->first;
              if(idsFace[0] == 0) {
                if(idsIn[0] == 2)      { idsFace[1]=1; idsIn[0]=3; idsIn[1]=2; }
                else                   { idsFace[1]=3; idsIn[0]=1; idsIn[1]=2; }
              }
              else if(idsFace[0] == 1) { idsFace[1]=2; idsIn[1]=3; }
              else                     { idsFace[1]=3; idsIn[0]=1; idsIn[1]=0; }
	      
              for(int i = 0; i < 2; i++) {
                double diff = sol(idsFace[i], k) - sol(idsIn[i], k);
                phantom(idsFace[i]) = sol(idsIn[i], k) + diff * 2.;
                diff = sol(4, k) - sol(idsIn[i], k);
                phantom(4) = sol(idsIn[i], k) + diff * 2.;
              }
            }
            else {
              phantom(4) = sol(4, k);
              for(int i = 0; i < 4; i++) {
                double diff = sol(i, k) - sol(4, k);
                phantom(i) = sol(4, k) + diff * 2.;
              }
            }
          }
	default:
	  Msg::Fatal("limiters are not implemented for this element type: %d", elem->getTypeForMSH() );
	  break;
        }
        double AVG = meanOnElement(phantom, /*groupIn,*/ groupIn->getFunctionSpace() /*, connectionIn.getElementId(iFace), phantom*/);
        valueOnFaces(iFace, k) = AVG;
      } // foreach field k
    } // foreach face iFace
  } // if not Dirichlet
  return true;
}

void dgLimiterClip::apply (dgDofContainer *solution)
{    
  const dgGroupCollection * groups=solution->getGroups();
  solution->scatter();
  fullMatrix<double> limitedSol;
  for (int iG = 0; iG < groups->getNbElementGroups(); iG++){
    dgGroupOfElements * egroup = groups->getElementGroup(iG);
    dgFullMatrix<double> &solGroup = solution->getGroupProxy(iG);
    
    dataCacheMap cacheMap(dataCacheMap::NODE_MODE, groups);
    cacheMap.setSolutionFunction(solution->getFunction(), solution->getFunctionGradient());
    //dataCacheDouble &solutionE = *cacheMap.get(function::getSolution(), NULL);
    const function * clipping = _claw->getClipToPhysics(egroup->getPhysicalTag());
    if (clipping) {
      dataCacheDouble * solutionEClipped = cacheMap.get(clipping);
      cacheMap.setGroup (egroup);
      for (size_t iElement=0; iElement<egroup->getNbElements(); ++iElement) {
        cacheMap.setElement(iElement);
        solGroup.getBlockProxy(iElement, limitedSol);
        limitedSol.setAll(solutionEClipped->get());
      }
    }
  }  
}

dgNodalVolume::dgNodalVolume(dgGroupCollection* groups)
{
  _groups = groups;
  _nodalVolume = new dgDofContainer(*_groups,1);
  compute(_nodalVolume);
}

dgNodalVolume::~dgNodalVolume()
{
  delete _nodalVolume;
}

void dgNodalVolume::compute(dgDofContainer * nodalVolume)
{
  // compute weights
  const dgGroupCollection * groups = nodalVolume->getGroups();
  for ( int iG = 0; iG < groups->getNbElementGroups(); iG++ ) {
    dgGroupOfElements *group = groups->getElementGroup(iG);
    const nodalBasis &fs = group->getFunctionSpace();
    const fullMatrix<double> &detJ = groups->_mesh->getJacobian(fs.order * 2 + 1).detJElement(iG);
    const dgIntegrationMatrices &matrices = dgIntegrationMatrices::get(fs.type, fs.order * 2 + 1);
    const fullMatrix<double> &psiW = matrices.psiW();
    int nbQP = matrices.getNbIntegrationPoints();
    dgFullMatrix<double> &groupData = nodalVolume->getGroupProxy(iG);
    for ( size_t iE = 0; iE < group->getNbElements(); iE++ ) {
      fullMatrix<double> elemData;
      groupData.getBlockProxy(iE,elemData);
      int nbNodes = group->getNbNodes();
      for ( int iN = 0; iN < nbNodes; iN++ ) {
        elemData(iN, 0) = 0.;
        for ( int iQP = 0; iQP < nbQP; iQP++ )
          elemData(iN, 0) += psiW(iN, iQP) * detJ(iQP, iE);
      }
    }
  }
}

dgJumpDiffusion::dgJumpDiffusion(const dgGroupCollection &groups, int fieldId, int integrationOrder, double c, double u, dgDofContainer *ext):
  dgLimiter(NULL),
  _groups(groups),
  _fieldId(fieldId),
  _integrationOrder(integrationOrder),
  _c(c),
  _u(u),
  _ext(ext)
{
  _dim = _groups.getElementGroup(0)->getDimUVW();
  _sizeNu =  (_dim * _dim - _dim) / 2 + _dim;
  _nuField = new dgDofContainer(groups, _sizeNu);
}

dgJumpDiffusion::~dgJumpDiffusion()
{
  delete _nuField;
}

void dgJumpDiffusion::apply(dgDofContainer *_sol)
{
  const dgDofContainer &sol = *_sol;
  _nuField->setAll(0.);
  int nbFields = sol.getNbFields();
  const dgMeshJacobian &jac = _groups._mesh->getJacobian(_integrationOrder);
  
  for (int ig = 0; ig < _groups.getNbFaceGroups(); ++ig) {
    const dgGroupOfFaces &gf = *_groups.getFaceGroup(ig);
    if (gf.nConnection() == 2) {
      const dgGroupOfElements &gLeft = gf.elementGroup(0);
      const dgGroupOfElements &gRight = gf.elementGroup(1);
      const dgFullMatrix<double> &solGroupLeft = sol.getGroupProxy(&gLeft);
      const dgFullMatrix<double> &solGroupRight = sol.getGroupProxy(&gRight);
      dgFullMatrix<double> *nu[2] = {&_nuField->getGroupProxy(&gLeft), &_nuField->getGroupProxy(&gRight)};
      for (unsigned int iface = 0; iface < gf.size(); ++iface) {
	int iElem[2] = {gf.elementId(iface, 0), gf.elementId(iface, 1)};
        const std::vector<int> &clLeft = gf.closure(iface, 0);
        const std::vector<int> &clRight = gf.closure(iface, 1);
        for (unsigned int i = 0; i < clLeft.size(); ++i) {
          double jumpLocal = fabs(solGroupLeft(clLeft[i], iElem[0] * nbFields + _fieldId) - solGroupRight(clRight[i], iElem[1] * nbFields + _fieldId)) / _u;
 	  double dx = std::max(gLeft.elementVolume(jac, iElem[0]), gRight.elementVolume(jac, iElem[1])) / gf.interfaceSurface(jac, iface);
          double nuLocal = _c * dx * jumpLocal;
	  
	  for (int iCon = 0; iCon < 2; iCon++){
	    fullMatrix<double> nuElem;
	    nu[iCon]->getBlockProxy(iElem[iCon], nuElem);
	    fullMatrix<double> normal;
	    gf.normal(jac, iface, iCon, normal);
	    
	    double nx = normal(i, 0);
	    double ny = normal(i, 1);
	    double K = nuElem(0, 0) * nx * nx + 2 * nuElem(0, 1) * nx * ny + nuElem(0, 2) * ny * ny;
	    
	    double alpha = nuLocal - K;
	    
	    if (alpha > 0){
	      nuElem(0, 0) += alpha * nx * nx;
	      nuElem(0, 1) += alpha * nx * ny;
	      nuElem(0, 2) += alpha * ny * ny;
	    }
	  }
	  
        }
      }
    }
  }
  
  for (int ig = 0; ig < _groups.getNbFaceGroups(); ++ig) {
    const dgGroupOfFaces &gf = *_groups.getFaceGroup(ig);
    if (gf.nConnection() == 1 && _ext) {
      if (gf.physicalTag() == "Sides")
	continue;
      
      const dgGroupOfElements &gLeft = gf.elementGroup(0);
      const dgFullMatrix<double> &eLeft = sol.getGroupProxy(&gLeft);
      const dgFullMatrix<double> &extLeft = _ext->getGroupProxy(&gLeft);
      dgFullMatrix<double> &nuLeft = _nuField->getGroupProxy(&gLeft);
      for (unsigned int iface = 0; iface < gf.size(); ++iface) {
	int iLeft = gf.elementId(iface, 0);
	const std::vector<int> &clLeft = gf.closure(iface, 0);
	fullMatrix<double> normalLeft;
	gf.normal(jac, iface, 0, normalLeft);
	for (unsigned int i = 0; i < clLeft.size(); ++i) {
	  double jumpLocal = fabs(eLeft(clLeft[i], iLeft * nbFields + _fieldId) - extLeft(clLeft[i], iLeft * nbFields + _fieldId)) / _u;
	  double dx = gLeft.elementVolume(jac, iLeft) / gf.interfaceSurface(jac, iface);
	  double nuLocal = _c * dx * jumpLocal;
	  
	  fullMatrix<double> nuElem;
	  nuLeft.getBlockProxy(iLeft, nuElem);
	  fullMatrix<double> normal;
	  gf.normal(jac, iface, 0, normal);
	  
	  double nx = normal(i, 0);
	  double ny = normal(i, 1);
	  double K = nuElem(0, 0) * nx * nx + 2 * nuElem(0, 1) * nx * ny + nuElem(0, 2) * ny * ny;
	  
	  double alpha = nuLocal - K;
	  
	  if (alpha > 0){
	    nuElem(0, 0) += alpha * nx * nx;
	    nuElem(0, 1) += alpha * nx * ny;
	    nuElem(0, 2) += alpha * ny * ny;
	  }
	}
      }
    }
  }
  //Compute the average of nodes values
  for (int ig = 0; ig < _groups.getNbElementGroups(); ++ig) {
    const dgGroupOfElements &ge = *_groups.getElementGroup(ig);
    dgFullMatrix<double> &nu = _nuField->getGroupProxy(&ge);
    for (unsigned int iElem = 0; iElem < ge.getNbElements(); ++iElem) {
      fullMatrix<double> nuElem;
      nu.getBlockProxy(iElem, nuElem);
      for (int i = 1; i < ge.getNbNodes(); i++){
	nuElem(i, 0) = nuElem(0, 0);
	nuElem(i, 1) = nuElem(0, 1);
	nuElem(i, 2) = nuElem(0, 2);
      }
    }
  }
  
  //  //Back to (bi-)linear
  //  for (int iter = 0; iter < 200; iter++){
  //    for (int ig = 0; ig < _groups.getNbFaceGroups(); ++ig) {
  //      const dgGroupOfFaces &gf = *_groups.getFaceGroup(ig);
  //      if (gf.nConnection() == 2) {
  //	const dgGroupOfElements &gLeft = gf.elementGroup(0);
  //	const dgGroupOfElements &gRight = gf.elementGroup(1);
  //	dgFullMatrix<double> *nu[2] = {&_nuField->getGroupProxy(&gLeft), &_nuField->getGroupProxy(&gRight)};
  //	for (unsigned int iface = 0; iface < gf.size(); ++iface) {
  //	  int iElem[2] = {gf.elementId(iface, 0), gf.elementId(iface, 1)};
  //	  const std::vector<int> &clLeft = gf.closure(iface, 0);
  //	  const std::vector<int> &clRight = gf.closure(iface, 1);
  //	  for (unsigned int i = 0; i < clLeft.size(); ++i) {
  //	    fullMatrix<double> nuElemL;
  //	    nu[0]->getBlockProxy(iElem[0], nuElemL);
  //	    fullMatrix<double> nuElemR;
  //	    nu[1]->getBlockProxy(iElem[1], nuElemR);
  //	    
  //	    nuElemL(clLeft[i],0) = (nuElemL(clLeft[i],0) + nuElemR(clRight[i],0)) / 2;
  //	    nuElemL(clLeft[i],1) = (nuElemL(clLeft[i],1) + nuElemR(clRight[i],1)) / 2;
  //	    nuElemL(clLeft[i],2) = (nuElemL(clLeft[i],2) + nuElemR(clRight[i],2)) / 2;
  //	    
  //	    
  //	    nuElemR(clRight[i],0) = nuElemL(clLeft[i],0);
  //	    nuElemR(clRight[i],1) = nuElemL(clLeft[i],1);
  //	    nuElemR(clRight[i],2) = nuElemL(clLeft[i],2);
  //	  }
  //        }
  //      }
  //    }
  //  }
  
}



dgArtificialDiffusion::dgArtificialDiffusion(const dgGroupCollection &groups, function *sensor, int integrationOrder, double kappa, double H_over_V):
  dgLimiter(NULL),
  _groups(groups),
  _sensor(sensor),
  _integrationOrder(integrationOrder),
  _kappa(kappa)
  {
  
  _LegendreToLagrangeDof.resize(groups.getNbElementGroups());
  _LagrangeToLegendreDof.resize(groups.getNbElementGroups());
  for(int iGroup = 0; iGroup < groups.getNbElementGroups(); iGroup++) {
    dgGroupOfElements &group = *groups.getElementGroup(iGroup);
    
    if (group.getElement(0)->getType() != TYPE_QUA && group.getElement(0)->getType() != TYPE_HEX){
      Msg::Fatal("Artificial diffusion only implemented for quads and hexahedrons");
    }
    dataCacheMap cacheMap(dataCacheMap::INTEGRATION_GROUP_MODE, group.getGroupCollection());
    cacheMap.setGroup(&group);
    const dgIntegrationMatrices &intMatrices = cacheMap.getIntegrationMatrices();
    const fullMatrix<double> &xi = intMatrices.integrationPoints();
    const fullVector<double> &w = intMatrices.integrationWeights();
    const fullMatrix<double> &psiLa = intMatrices.psi();
    int order = group.getFunctionSpace().order;
    LegendrePolynomials leBasis(order);
    
    //Computation of the Legendre basis functions
    _psiLe.resize(psiLa.size1(), psiLa.size2());
    double *basisVal[3];
    int dim = group.getDimUVW();
    int nbI = order+1;
    int nbJ = (dim >= 2) ? (order+1) : 1;
    int nbK = (dim >= 3) ? (order+1) : 1;
    for (int iDim = 0; iDim < 3; iDim++){
      basisVal[iDim] = new double[_psiLe.size2()];
      for (int i = 0; i < _psiLe.size2(); i++){
	basisVal[iDim][i] = 1;
      }
    }
    for (int iPt = 0; iPt < _psiLe.size1(); iPt++){
      for (int iDim = 0; iDim < dim; iDim++){
	leBasis.f(xi(iPt,iDim),basisVal[iDim]);
      }
      for (int i = 0; i < nbI; i++){
	for (int j = 0; j < nbJ; j++){
	  for (int k = 0; k < nbK; k++){
	    _psiLe(iPt, i + nbI * j + nbI * nbJ * k) = basisVal[0][i] * basisVal[1][j] * basisVal[2][k];
	  }
	}
      }
    }
    for (int iDim = 0; iDim < 3; iDim++){
      delete[] basisVal[iDim];
    }
    
    //Computation of the mass matrices. We neglect the jacobian of the transformation here
    fullMatrix<double> intPsiLePsiLa(_psiLe.size2(), _psiLe.size2());
    fullMatrix<double> invMassLe(_psiLe.size2(), _psiLe.size2());
    intPsiLePsiLa.setAll(0);
    invMassLe.setAll(0);
    for (int iPt = 0; iPt < _psiLe.size1(); iPt++){
      for (int i = 0; i < _psiLe.size2(); i++){
	for (int j = 0; j < _psiLe.size2(); j++){
	  intPsiLePsiLa(i,j) += _psiLe(iPt, i) * psiLa(iPt, j) * w(iPt);
	  invMassLe(i,j) += _psiLe(iPt, i) * _psiLe(iPt, j) * w(iPt);
	}
      }
    }
    invMassLe.invertInPlace();
    _LagrangeToLegendreDof[iGroup].resize(_psiLe.size2(), _psiLe.size2());
    invMassLe.mult(intPsiLePsiLa, _LagrangeToLegendreDof[iGroup]);
    _LegendreToLagrangeDof[iGroup].resize(_psiLe.size2(), _psiLe.size2());
    _LagrangeToLegendreDof[iGroup].invert(_LegendreToLagrangeDof[iGroup]);
  }
  _nuhField = new dgDofContainer(groups, 1);
  _nuvField = new dgDofContainer(groups, 1);
  _nuhField->setAll(0);
  _nuvField->setAll(0);
}

void dgArtificialDiffusion::apply(dgDofContainer *_sol){
  const dgMeshJacobian &jac = _groups._mesh->getJacobian(_integrationOrder);
  dataCacheMap cacheMap(dataCacheMap::INTEGRATION_GROUP_MODE, &_groups, 0, _integrationOrder);
  dataCacheMap cacheMapNode(dataCacheMap::NODE_MODE, &_groups);
  dataCacheDouble &Sensor = *cacheMapNode.get(_sensor);

  cacheMapNode.setSolutionFunction(_sol->getFunction(), _sol->getFunctionGradient());
  


  for(int iGroup = 0; iGroup < _groups.getNbElementGroups(); iGroup++) {
    dgGroupOfElements &group = *_groups.getElementGroup(iGroup);

    cacheMap.setGroup(&group);
    cacheMapNode.setGroup(&group);
    const dgFullMatrix<double> &nuhGroup = _nuhField->getGroupProxy(&group);    
    const dgFullMatrix<double> &nuvGroup = _nuvField->getGroupProxy(&group);    

    int order = group.getFunctionSpace().order;
    int dim = group.getDimUVW();
    int nbI = order+1;
    int nbJ = (dim >= 2) ? (order+1) : 1;
    int nbK = (dim >= 3) ? (order+1) : 1;
      
    for (unsigned int iElem = 0; iElem < group.getNbElements(); ++iElem) {
      cacheMapNode.setElement(iElem);
      fullMatrix<double> sensorElem(_LagrangeToLegendreDof[iGroup].size1(), Sensor.get().size2());
      _LagrangeToLegendreDof[iGroup].mult(Sensor.get(), sensorElem);

      fullMatrix<double> nuhElem;
      nuhGroup.getBlockProxy(iElem, nuhElem);
      fullMatrix<double> nuvElem;
      nuvGroup.getBlockProxy(iElem, nuvElem);
      int p = order;


      const dgIntegrationMatrices &intMatrices = cacheMap.getIntegrationMatrices();
      const fullVector<double> &w = intMatrices.integrationWeights();

      double intSensorFull = 0, intSensorDiffH = 0, intSensorDiffV = 0;
      for (int iPt = 0; iPt < w.size(); iPt++){

	double sensorTruncH = 0;
	for (int i = 0; i < nbI-1; i++){
	  for (int j = 0; j < nbJ-1; j++){
	    for (int k = 0; k < nbK; k++){
	      sensorTruncH += _psiLe(iPt, i + nbI * j + nbI * nbJ * k) * sensorElem(i + nbI * j + nbI * nbJ * k, 0);
	    }
	  }
	}
	double sensorTruncV = 0;
	for (int i = 0; i < nbI; i++){
	  for (int j = 0; j < nbJ; j++){
	    for (int k = 0; k < nbK-1; k++){
	      sensorTruncV +=  _psiLe(iPt, i + nbI * j + nbI * nbJ * k) * sensorElem(i + nbI * j + nbI * nbJ * k, 0);
	    }
	  }
	}
	double sensorFull = 0;
	for (int i = 0; i < nbI; i++){
	  for (int j = 0; j < nbJ; j++){
	    for (int k = 0; k < nbK; k++){
	      sensorFull += _psiLe(iPt, i + nbI * j + nbI * nbJ * k) * sensorElem(i + nbI * j + nbI * nbJ * k, 0);
	    }
	  }
	}

	intSensorDiffH += (sensorFull - sensorTruncH)*(sensorFull - sensorTruncH) * w(iPt);
	intSensorDiffV += (sensorFull - sensorTruncV)*(sensorFull - sensorTruncV) * w(iPt);
	intSensorFull += sensorFull * sensorFull * w(iPt);
      }
      

      double SeH = intSensorDiffH / intSensorFull;
      double SeV = intSensorDiffV / intSensorFull;
      if (fabs(intSensorFull) < DBL_MIN){
	SeH = 1;
	SeV = 1;
      }

      //We suppose hV much smaller than hH
      //vertical dimension is not subject to spaceTransform so can use innerRadius
      //horizontal dimension cannot
      double hV = 2.0 * cacheMap.getJacobians().elementInnerRadius(group.elementVectorId())(iElem);
      double hH = sqrt(group.elementVolume(jac, iElem) / hV);

      //      double hH = pow(group.elementVolume(jac, iElem) * _H_over_V, 1./3);
      //      double hV = pow(group.elementVolume(jac, iElem) / _H_over_V / _H_over_V, 1./3);
      double epsilon0H = hH / p;
      double epsilon0V = hV / p;
      double s0 = .8 / (p*p*p*p);
      double seH = SeH;//log10(SeH);
      double seV = SeV;//log10(SeV);

      double epsiloneH;
      if (seH < s0 - _kappa){
	epsiloneH = 0;
      }
      else if (s0 - _kappa <= seH && s0 + _kappa >= seH){
	epsiloneH = epsilon0H/2 * (1. + sin(M_PI * (seH-s0) / (2. * _kappa)));
      }
      else{
	epsiloneH = epsilon0H;
      }
      double epsiloneV;
      if (seV < s0 - _kappa){
	epsiloneV = 0;
	//	printf("dow (seV %e s0 %e kappa %e p %i)\n",seV,s0,_kappa,p);
      }
      else if (s0 - _kappa <= seV && s0 + _kappa >= seV){
	epsiloneV = epsilon0V/2 * (1. + sin(M_PI * (seV-s0) / (2. * _kappa)));
	//	printf("mid (seV %e s0 %e kappa %e p %i)\n",seV,s0,_kappa,p);
      }
      else{
	epsiloneV = epsilon0V;
	//	printf("up  (seV %e s0 %e kappa %e p %i)\n",seV,s0,_kappa,p);
      }


      for (int i = 0; i < nuhElem.size1(); i++){
	nuhElem(i, 0) = 3.e-1*epsiloneH;
	nuvElem(i, 0) = 3.e-1*epsiloneV;
      }
    }
  }
}

dgArtificialDiffusion::~dgArtificialDiffusion()
{
  delete _nuhField;
  delete _nuvField;
}

bool dgSlopeLimiterOriginal::boundaryValues(const dgGroupOfFaces * faces, dgDofContainer * solution, fullMatrix<double> & valueOnFaces,
                               const function * secondary) {
  if((size_t)valueOnFaces.size1() != faces->size() || valueOnFaces.size2() != _claw->getNbFields())
    Msg::Fatal("bad matrix sizes in slopeLimiter boundaryValue !");
  
  //const dgGroupOfElements *groupIn = &faces->elementGroup(0);
  const function *bcF = _claw->getOutsideValue(faces, secondary);
  if (!bcF)
    bcF = _claw->getOutsideValue(faces, function::getSolution());
  dataCacheMap cacheMap(dataCacheMap::NODE_MODE, solution->getGroups());
  cacheMap.setSolutionFunction(solution->getFunction(), solution->getFunctionGradient());
  
  if(bcF) { // Dirichlet BC => the given value is the min/max accepted
    dataCacheDouble &bcE = *cacheMap.get(bcF, NULL);
    cacheMap.setInterfaceGroup(faces, 0);
    for(size_t iFace = 0; iFace < faces->size(); iFace++) {
      cacheMap.setInterface(iFace);
      const fullMatrix<double> &bcM = bcE.get();
      for (int k = 0; k < bcM.size2(); k++) {
        fullVector<double> nVal;
        nVal.setAsProxy(bcM, k);
      }
    }
    return true;
  } else {
    //check
    /*dataCacheDouble &solE = *cacheMap.get(variable, NULL);
    cacheMap.setGroup(groupIn);
    for(size_t iFace = 0; iFace < faces->size(); iFace++) {
      cacheMap.setElement(faces->elementId(iFace, 0));
      const fullMatrix<double> &sol = solE.get();
      for (int k = 0; k < sol.size2(); ++k) {
        valueOnFaces(iFace, k) = 0;
      }
    }
    return true;*/
    //endcheck
    
    return false; // No value given if there is no information on the boundary
  }
}

dgSlopeLimiterOriginal::dgSlopeLimiterOriginal(dgConservationLaw* claw, dgGroupCollection* groups): dgLimiter(claw)
{
  _groups = groups;
  
  _nodalVolume = new dgDofContainer(*_groups, 1);
  dgNodalVolume::compute(_nodalVolume);
}


dgSlopeLimiterOriginal::~dgSlopeLimiterOriginal()
{
  delete _nodalVolume;
}

void dgSlopeLimiterOriginal::apply(dgDofContainer* solution)
{
  dgNodalVolume::compute(_nodalVolume); // not very efficient 
  int nFields = solution->getNbFields();
  int nGroups = _groups->getNbElementGroups() + _groups->getNbGhostGroups();
  std::vector<std::vector<double> > meanVals(nGroups), maxVals(nGroups), minVals(nGroups), meanValsVertical(nGroups);
  // compute the mean for each element and store it in dof
  for (int iG = 0; iG < nGroups; iG++) {
    fullMatrix<double> elemData, eVol;
    const dgGroupOfElements &group = *_groups->getElementGroup(iG);
    meanVals[iG].resize(group.getNbElements() * nFields);
    int n2dNodes = group.getNbNodes()/2;
    meanValsVertical[iG].resize(group.getNbElements() * nFields * n2dNodes);
    minVals[iG].resize(group.getNbElements() * nFields, 1e20);
    maxVals[iG].resize(group.getNbElements() * nFields, -1e20);
    for (size_t iElem =0; iElem < group.getNbElements(); iElem++) {
      solution->getGroupProxy(iG).getBlockProxy(iElem, elemData);
      _nodalVolume->getGroupProxy(iG).getBlockProxy(iElem, eVol);
      for ( int iField = 0; iField < nFields; iField++ ) {
        double meanVal = 0;
        double vol = 0;
        for (int iNode = 0; iNode < group.getNbNodes(); iNode++ ) {
          meanVal += elemData(iNode, iField) * eVol(iNode,0);
          vol += eVol(iNode,0);
        }
        meanVals[iG][iElem * nFields + iField] = meanVal / vol;
        for (int iVert = 0; iVert < n2dNodes; iVert++ ) {
          meanValsVertical[iG][(iElem * nFields + iField)*n2dNodes + iVert] = (elemData(iVert, iField) * eVol(iVert,0) + elemData(iVert+n2dNodes, iField) * eVol(iVert+n2dNodes,0)) / (eVol(iVert,0) + eVol(iVert + n2dNodes,0));
        }
      }
    }
  }
  for( int iGFace = 0; iGFace < _groups->getNbFaceGroups(); iGFace++) {
    dgGroupOfFaces* faceGroup = _groups->getFaceGroup(iGFace);
    if(faceGroup->nConnection() == 1) {
      int iGroup = _groups->getElementGroupId(&faceGroup->elementGroup(0));
      fullMatrix<double> valuesOnFaces(faceGroup->size(), nFields);
      if (!boundaryValues(faceGroup, solution, valuesOnFaces))
        continue;
      for (size_t iFace = 0; iFace < faceGroup->size(); iFace++) {
        int iElem = faceGroup->elementId(iFace, 0);
        for (int iF = 0; iF < nFields; iF++) {
          minVals[iGroup][iElem * nFields + iF] = std::min(minVals[iGroup][iElem * nFields + iF], valuesOnFaces(iFace, iF));
          maxVals[iGroup][iElem * nFields + iF] = std::max(maxVals[iGroup][iElem * nFields + iF], valuesOnFaces(iFace, iF));
        }
      }
    }
    for (int iConnection = 0; iConnection < faceGroup->nConnection(); ++iConnection) {
      int iGroup = _groups->getElementGroupId(&faceGroup->elementGroup(iConnection));
      for (int jConnection = 0; jConnection < faceGroup->nConnection(); ++jConnection) {
        if (iConnection == jConnection) continue;
        int jGroup = _groups->getElementGroupId(&faceGroup->elementGroup(jConnection));
        for (size_t i = 0; i < faceGroup->size(); ++i) {
          int iElem = faceGroup->elementId(i, iConnection);
          int jElem = faceGroup->elementId(i, jConnection);
          for (int iF = 0; iF < nFields; iF++) {
            minVals[iGroup][iElem * nFields + iF] = std::min(minVals[iGroup][iElem * nFields + iF], meanVals[jGroup][jElem * nFields + iF]);
            maxVals[iGroup][iElem * nFields + iF] = std::max(maxVals[iGroup][iElem * nFields + iF], meanVals[jGroup][jElem * nFields + iF]);
          }
        }
      }
    }
  }
  // limit slopes in each elem
  // 1D optimization: lambda = 1 -> original solution, lambda = 0 -> mean solution 
  for (int iG = 0; iG < _groups->getNbElementGroups(); iG++) {
    dgGroupOfElements *group = _groups->getElementGroup(iG);
    int nbNodes = group->getNbNodes();
    int n2dNodes = nbNodes/2;
    const dgFullMatrix<double> &groupProxyData = solution->getGroupProxy(iG);
    for (size_t iE =0; iE < group->getNbElements(); iE++) {
      fullMatrix<double> elemData, eVol;
      groupProxyData.getBlockProxy(iE, elemData);
      _nodalVolume->getGroupProxy(iG).getBlockProxy(iE, eVol);
      for ( int iF = 0; iF < nFields; iF++ ) {
        // apply limiter
        bool _optimal = false;
        if (_optimal){
          fullVector<double> x(nbNodes),w(nbNodes),xMin(nbNodes),xMax(nbNodes);
          for (int i = 0; i < nbNodes; i++ ) {
            x(i) = elemData(i,iF);
            xMin(i) = minVals[iG][iE*nFields + iF];
            xMax(i) = maxVals[iG][iE*nFields + iF];
            w(i) = eVol(i, 0);
          }
          int iter = 0;
          solveQPLimiter(w,x,xMin,xMax,iter);
          for (int iNode = 0; iNode < nbNodes; iNode++ ) {
            elemData(iNode,iF) = x(iNode);
          }
        }
        else{
          double TOL = 1e-20;
          /* Struct vertical first */
          for (int iVert = 0; iVert < n2dNodes; iVert++ ) {
            double vertLambda = 1.0;
            double meanVert = meanValsVertical[iG][(iE * nFields + iF)*n2dNodes + iVert];
            for (int iNode = 0; iNode < 2; iNode++ ) {
              double lambda = 1.0;
              double nodeV = elemData(iVert + iNode * n2dNodes, iF);
              if (nodeV - maxVals[iG][iE * nFields + iF] > TOL)
                lambda = (maxVals[iG][iE * nFields + iF] - meanVert) / (nodeV - meanVert);
              else if (minVals[iG][iE * nFields + iF] - nodeV > TOL )
                lambda = (meanVert - minVals[iG][iE * nFields + iF]) / (meanVert - nodeV);
              vertLambda = std::min(vertLambda, lambda);
            }
            if(vertLambda < 1.0) {
              vertLambda = std::max(vertLambda, 0.);
              for (int iNode = 0; iNode < 2; iNode++ ) {
                elemData(iVert + iNode * n2dNodes, iF) = vertLambda * elemData(iVert + iNode * n2dNodes, iF) + (1. - vertLambda) * meanVert;
              }
            }
          }
          /* end struct vertical */
          // find maximum allowed lambda
          double elemLambda = 1.0;
          double mean = meanVals[iG][iE * nFields + iF];
          for (int iNode = 0; iNode < nbNodes; iNode++ ) {
            double lambda = 1.0;
            double nodeV = elemData(iNode, iF);
            if (nodeV - maxVals[iG][iE * nFields + iF] > TOL)
              lambda = (maxVals[iG][iE * nFields + iF] - mean) / (nodeV - mean);
            else if (minVals[iG][iE * nFields + iF] - nodeV > TOL )
              lambda = (mean - minVals[iG][iE * nFields + iF]) / (mean - nodeV);
            elemLambda = std::min(elemLambda, lambda);
          }
          if(elemLambda < 1.0) {
            elemLambda = std::max(elemLambda, 0.);
            for (int iNode = 0; iNode < nbNodes; iNode++ ) {
              elemData(iNode, iF) = elemLambda * elemData(iNode, iF) + (1. - elemLambda) * mean;
            }
          }
        }
      }
    }
  }
  solution->scatter();
}

bool dgSlopeLimiterUV::boundaryValues(const dgGroupOfFaces * faces, dgDofContainer * solution, fullMatrix<double> & valueOnFaces,
                               const function * secondary) {
  if((size_t)valueOnFaces.size1() != faces->size() || valueOnFaces.size2() != _claw->getNbFields())
    Msg::Fatal("bad matrix sizes in slopeLimiter boundaryValue !");
  const function * variable = (secondary)? secondary : solution->getFunction();
  
  const dgGroupOfElements *groupIn = &faces->elementGroup(0);
  const function *bcF = _claw->getOutsideValue(faces, secondary);
  if (!bcF)
    bcF = _claw->getOutsideValue(faces, function::getSolution());
  dataCacheMap cacheMap(dataCacheMap::NODE_MODE, solution->getGroups());
  cacheMap.setSolutionFunction(solution->getFunction(), solution->getFunctionGradient());
  
  if(bcF) { // Dirichlet BC => the given value is the min/max accepted
    dataCacheDouble &bcE = *cacheMap.get(bcF, NULL);
    cacheMap.setInterfaceGroup(faces, 0);
    for(size_t iFace = 0; iFace < faces->size(); iFace++) {
      cacheMap.setInterface(iFace);
      const fullMatrix<double> &bcM = bcE.get();
      for (int k = 0; k < bcM.size2(); k++) {
        fullVector<double> nVal;
        nVal.setAsProxy(bcM, k);
      }
    }
    return true;
  } else {
    dataCacheDouble &solE = *cacheMap.get(variable, NULL);
    dataCacheDouble &volE = *cacheMap.get(_nodalVolume->getFunction(), NULL);
    cacheMap.setGroup(groupIn);
    for(size_t iFace = 0; iFace < faces->size(); iFace++) {
      cacheMap.setElement(faces->elementId(iFace, 0));
      const std::vector<int> &closure = faces->closure(iFace, 0);
      const fullMatrix<double> &sol = solE.get();
      const fullMatrix<double> &vol = volE.get();
      double elementVolume = 0, faceVolume = 0;
      std::vector<double> elementMean(sol.size2()), faceMean(sol.size2());
      for (int i = 0; i < vol.size1(); ++i) {
        elementVolume += vol(i, 0);
        for (int k = 0; k < sol.size2(); ++k) {
          elementMean[k] += sol(i, k) * vol(i, 0);
        }
      }
      for (size_t i = 0; i < closure.size(); ++i) {
        faceVolume += vol(closure[i], 0);
        for (int k = 0; k < sol.size2(); ++k) {
          faceMean[k] += sol(closure[i], k) * vol(closure[i], 0);
        }
      }
      for (int k = 0; k < sol.size2(); ++k) {
        elementMean[k] /= elementVolume;
        faceMean[k] /= faceVolume;
        valueOnFaces(iFace, k) = faceMean[k];// * 2 - elementMean[k];
      }
    }
    return true;
  }
}

dgSlopeLimiterUV::dgSlopeLimiterUV(dgConservationLaw* claw, dgGroupCollection* groups): dgLimiter(claw)
{
  _groups = groups;
  
  _nodalVolume = new dgDofContainer(*_groups, 1);
  dgNodalVolume::compute(_nodalVolume);
}


dgSlopeLimiterUV::~dgSlopeLimiterUV()
{
  delete _nodalVolume;
}

void dgSlopeLimiterUV::apply(dgDofContainer* solution)
{
  dgNodalVolume::compute(_nodalVolume); // not very efficient 
  int nFields = solution->getNbFields();
  int nGroups = _groups->getNbElementGroups() + _groups->getNbGhostGroups();
  std::vector<std::vector<double> > meanVals(nGroups), maxVals(nGroups), minVals(nGroups), meanValsVertical(nGroups);
  // compute the mean for each element and store it in dof
  for (int iG = 0; iG < nGroups; iG++) {
    fullMatrix<double> elemData, eVol;
    const dgGroupOfElements &group = *_groups->getElementGroup(iG);
    meanVals[iG].resize(group.getNbElements() * nFields);
    int n2dNodes = group.getNbNodes()/2;
    meanValsVertical[iG].resize(group.getNbElements() * nFields * n2dNodes);
    minVals[iG].resize(group.getNbElements() * nFields, 1e20);
    maxVals[iG].resize(group.getNbElements() * nFields, -1e20);
    for (size_t iElem =0; iElem < group.getNbElements(); iElem++) {
      solution->getGroupProxy(iG).getBlockProxy(iElem, elemData);
      _nodalVolume->getGroupProxy(iG).getBlockProxy(iElem, eVol);
      for ( int iField = 0; iField < nFields; iField++ ) {
        double meanVal = 0;
        double vol = 0;
        for (int iNode = 0; iNode < group.getNbNodes(); iNode++ ) {
          meanVal += elemData(iNode, iField) * eVol(iNode,0);
          vol += eVol(iNode,0);
        }
        meanVals[iG][iElem * nFields + iField] = meanVal / vol;
        for (int iVert = 0; iVert < n2dNodes; iVert++ ) {
          meanValsVertical[iG][(iElem * nFields + iField)*n2dNodes + iVert] = (elemData(iVert, iField) * eVol(iVert,0) + elemData(iVert+n2dNodes, iField) * eVol(iVert+n2dNodes,0)) / (eVol(iVert,0) + eVol(iVert + n2dNodes,0));
        }
      }
    }
  }
  for( int iGFace = 0; iGFace < _groups->getNbFaceGroups(); iGFace++) {
    dgGroupOfFaces* faceGroup = _groups->getFaceGroup(iGFace);
    if(faceGroup->nConnection() == 1) {
      int iGroup = _groups->getElementGroupId(&faceGroup->elementGroup(0));
      fullMatrix<double> valuesOnFaces(faceGroup->size(), nFields);
      if (!boundaryValues(faceGroup, solution, valuesOnFaces))
        continue;
      for (size_t iFace = 0; iFace < faceGroup->size(); iFace++) {
        int iElem = faceGroup->elementId(iFace, 0);
        for (int iF = 0; iF < nFields; iF++) {
          minVals[iGroup][iElem * nFields + iF] = std::min(minVals[iGroup][iElem * nFields + iF], valuesOnFaces(iFace, iF));
          maxVals[iGroup][iElem * nFields + iF] = std::max(maxVals[iGroup][iElem * nFields + iF], valuesOnFaces(iFace, iF));

        }
      }
    }
    for (int iConnection = 0; iConnection < faceGroup->nConnection(); ++iConnection) {
      int iGroup = _groups->getElementGroupId(&faceGroup->elementGroup(iConnection));
      for (int jConnection = 0; jConnection < faceGroup->nConnection(); ++jConnection) {
        if (iConnection == jConnection) continue;
        int jGroup = _groups->getElementGroupId(&faceGroup->elementGroup(jConnection));
        for (size_t i = 0; i < faceGroup->size(); ++i) {
          int iElem = faceGroup->elementId(i, iConnection);
          int jElem = faceGroup->elementId(i, jConnection);
          for (int iF = 0; iF < nFields; iF++) {
            minVals[iGroup][iElem * nFields + iF] = std::min(minVals[iGroup][iElem * nFields + iF], meanVals[jGroup][jElem * nFields + iF]);
            maxVals[iGroup][iElem * nFields + iF] = std::max(maxVals[iGroup][iElem * nFields + iF], meanVals[jGroup][jElem * nFields + iF]);
          }
        }
      }
    }
  }
  // limit slopes in each elem
  // 1D optimization: lambda = 1 -> original solution, lambda = 0 -> mean solution 
  for (int iG = 0; iG < _groups->getNbElementGroups(); iG++) {
    dgGroupOfElements *group = _groups->getElementGroup(iG);
    int nbNodes = group->getNbNodes();
    //int n2dNodes = nbNodes/2;
    const dgFullMatrix<double> &groupProxyData = solution->getGroupProxy(iG);
    for (size_t iE =0; iE < group->getNbElements(); iE++) {
      fullMatrix<double> elemData, eVol;
      groupProxyData.getBlockProxy(iE, elemData);
      _nodalVolume->getGroupProxy(iG).getBlockProxy(iE, eVol);
      for ( int iF = 0; iF < nFields; iF++ ) {
        // apply limiter
        bool _optimal = false;
        if (_optimal){
          fullVector<double> x(nbNodes),w(nbNodes),xMin(nbNodes),xMax(nbNodes);
          for (int i = 0; i < nbNodes; i++ ) {
            x(i) = elemData(i,iF);
            xMin(i) = minVals[iG][iE*nFields + iF];
            xMax(i) = maxVals[iG][iE*nFields + iF];
            w(i) = eVol(i, 0);
          }
          int iter = 0;
          solveQPLimiter(w,x,xMin,xMax,iter);
          for (int iNode = 0; iNode < nbNodes; iNode++ ) {
            elemData(iNode,iF) = x(iNode);
          }
        }
        else{
          double TOL = 1e-20;
          // find maximum allowed lambda
          double elemLambda = 1.0;
          double mean = meanVals[iG][iE * nFields + iF];
          for (int iNode = 0; iNode < nbNodes; iNode++ ) {
            double lambda = 1.0;
            double nodeV = elemData(iNode, iF);
            if (nodeV - maxVals[iG][iE * nFields + iF] > TOL)
              lambda = (maxVals[iG][iE * nFields + iF] - mean) / (nodeV - mean);
            else if (minVals[iG][iE * nFields + iF] - nodeV > TOL )
              lambda = (mean - minVals[iG][iE * nFields + iF]) / (mean - nodeV);
            elemLambda = std::min(elemLambda, lambda);
          }
          if(elemLambda < 1.0) {
            elemLambda = std::max(elemLambda, 0.);
            for (int iNode = 0; iNode < nbNodes; iNode++ ) {
              elemData(iNode, iF) = elemLambda * elemData(iNode, iF) + (1. - elemLambda) * mean;
            }
          }
        }
      }
    }
  }
  solution->scatter();
}

bool dgSlopeLimiterOldVals::boundaryValues(const dgGroupOfFaces * faces, dgDofContainer * solution, fullMatrix<double> & valueOnFaces,
                               const function * secondary) {
  if((size_t)valueOnFaces.size1() != faces->size() || valueOnFaces.size2() != _claw->getNbFields())
    Msg::Fatal("bad matrix sizes in slopeLimiter boundaryValue !");
  const function * variable = (secondary)? secondary : solution->getFunction();
  
  const dgGroupOfElements *groupIn = &faces->elementGroup(0);
  const function *bcF = _claw->getOutsideValue(faces, secondary);
  if (!bcF)
    bcF = _claw->getOutsideValue(faces, function::getSolution());
  dataCacheMap cacheMap(dataCacheMap::NODE_MODE, solution->getGroups());
  cacheMap.setSolutionFunction(solution->getFunction(), solution->getFunctionGradient());
  
  if(bcF) { // Dirichlet BC => the given value is the min/max accepted
    dataCacheDouble &bcE = *cacheMap.get(bcF, NULL);
    cacheMap.setInterfaceGroup(faces, 0);
    for(size_t iFace = 0; iFace < faces->size(); iFace++) {
      cacheMap.setInterface(iFace);
      const fullMatrix<double> &bcM = bcE.get();
      for (int k = 0; k < bcM.size2(); k++) {
        fullVector<double> nVal;
        nVal.setAsProxy(bcM, k);
        valueOnFaces(iFace, k) = meanOnElement(nVal, *faces->getNodalBasis()/*, connectionIn.getElementId(iFace),*/);
      }
    }
    return true;
  } else {
    
    dataCacheDouble &solE = *cacheMap.get(variable, NULL);
    dataCacheDouble &volE = *cacheMap.get(_nodalVolume->getFunction(), NULL);
    cacheMap.setGroup(groupIn);
    for(size_t iFace = 0; iFace < faces->size(); iFace++) {
      cacheMap.setElement(faces->elementId(iFace, 0));
      const std::vector<int> &closure = faces->closure(iFace, 0);
      const fullMatrix<double> &sol = solE.get();
      const fullMatrix<double> &vol = volE.get();
      double faceVolume = 0;
      std::vector<double> faceMean(sol.size2());
      for (size_t i = 0; i < closure.size(); ++i) {
        faceVolume += vol(closure[i], 0);
        for (int k = 0; k < sol.size2(); ++k) {
          faceMean[k] += sol(closure[i], k) * vol(closure[i], 0);
        }
      }
      for (int k = 0; k < sol.size2(); ++k) {
        faceMean[k] /= faceVolume;
        valueOnFaces(iFace, k) = faceMean[k];
      }
    }
    return true;
  }
}

dgSlopeLimiterOldVals::dgSlopeLimiterOldVals(dgConservationLaw* claw, dgGroupCollection* groups, dgDofContainer* oldVals): dgLimiter(claw)
{
  _groups = groups;
  
  _nodalVolume = new dgDofContainer(*_groups, 1);
  dgNodalVolume::compute(_nodalVolume);
  _oldVals = oldVals; 
}


dgSlopeLimiterOldVals::~dgSlopeLimiterOldVals()
{
  delete _nodalVolume;
}

void dgSlopeLimiterOldVals::apply(dgDofContainer* solution)
{
  dgNodalVolume::compute(_nodalVolume); // not very efficient 
  int nFields = solution->getNbFields();
  int nGroups = _groups->getNbElementGroups() + _groups->getNbGhostGroups();
  std::vector<std::vector<double> > meanVals(nGroups), maxVals(nGroups), minVals(nGroups);
  // compute the mean for each element and store it in dof
  for (int iG = 0; iG < nGroups; iG++) {
    fullMatrix<double> elemData, eVol;
    const dgGroupOfElements &group = *_groups->getElementGroup(iG);
    meanVals[iG].resize(group.getNbElements() * nFields);
    minVals[iG].resize(group.getNbElements() * nFields, 1e20);
    maxVals[iG].resize(group.getNbElements() * nFields, -1e20);
    for (size_t iElem =0; iElem < group.getNbElements(); iElem++) {
      solution->getGroupProxy(iG).getBlockProxy(iElem, elemData);
      _nodalVolume->getGroupProxy(iG).getBlockProxy(iElem, eVol);
      for ( int iField = 0; iField < nFields; iField++ ) {
        double meanVal = 0;
        double vol = 0;
        for (int iNode = 0; iNode < group.getNbNodes(); iNode++ ) {
          meanVal += elemData(iNode, iField) * eVol(iNode,0);
          vol += eVol(iNode,0);
        }
        meanVals[iG][iElem * nFields + iField] = meanVal / vol;
      }
    }
  }
  for( int iGFace = 0; iGFace < _groups->getNbFaceGroups(); iGFace++) {
    dgGroupOfFaces* faceGroup = _groups->getFaceGroup(iGFace);
    if(faceGroup->nConnection() == 1) {
      int iGroup = _groups->getElementGroupId(&faceGroup->elementGroup(0));
      fullMatrix<double> valuesOnFaces(faceGroup->size(), nFields);
      if (!boundaryValues(faceGroup, solution, valuesOnFaces, _oldVals->getFunction()))
        continue;
      for (size_t iFace = 0; iFace < faceGroup->size(); iFace++) {
        int iElem = faceGroup->elementId(iFace, 0);
        for (int iF = 0; iF < nFields; iF++) {
          minVals[iGroup][iElem * nFields + iF] = std::min(minVals[iGroup][iElem * nFields + iF], valuesOnFaces(iFace, iF));
          maxVals[iGroup][iElem * nFields + iF] = std::max(maxVals[iGroup][iElem * nFields + iF], valuesOnFaces(iFace, iF));

        }
      }
    }
    for (int iConnection = 0; iConnection < faceGroup->nConnection(); ++iConnection) {
      int iGroup = _groups->getElementGroupId(&faceGroup->elementGroup(iConnection));
      for (int jConnection = 0; jConnection < faceGroup->nConnection(); ++jConnection) {
        if (iConnection == jConnection) continue;
        int jGroup = _groups->getElementGroupId(&faceGroup->elementGroup(jConnection));
        for (size_t i = 0; i < faceGroup->size(); ++i) {
          int iElem = faceGroup->elementId(i, iConnection);
          int jElem = faceGroup->elementId(i, jConnection);
          for (int iF = 0; iF < nFields; iF++) {
            minVals[iGroup][iElem * nFields + iF] = std::min(minVals[iGroup][iElem * nFields + iF], meanVals[jGroup][jElem * nFields + iF]);
            maxVals[iGroup][iElem * nFields + iF] = std::max(maxVals[iGroup][iElem * nFields + iF], meanVals[jGroup][jElem * nFields + iF]);
          }
        }
      }
    }
  }
  // limit slopes in each elem
  // 1D optimization: lambda = 1 -> original solution, lambda = 0 -> mean solution 
  for (int iG = 0; iG < _groups->getNbElementGroups(); iG++) {
    dgGroupOfElements *group = _groups->getElementGroup(iG);
    int nbNodes = group->getNbNodes();
    const dgFullMatrix<double> &groupProxyData = solution->getGroupProxy(iG);
    for (size_t iE =0; iE < group->getNbElements(); iE++) {
      fullMatrix<double> elemData;
      groupProxyData.getBlockProxy(iE, elemData);
      for ( int iF = 0; iF < nFields; iF++ ) {
        // find maximum allowed lambda
        double elemLambda = 1.0;
        double mean = meanVals[iG][iE * nFields + iF];
        for (int iNode = 0; iNode < nbNodes; iNode++ ) {
          double lambda = 1.0;
          double nodeV = elemData(iNode, iF);
          double TOL = 1e-20;
          if (nodeV - maxVals[iG][iE * nFields + iF] > TOL)
            lambda = (maxVals[iG][iE * nFields + iF] - mean) / (nodeV - mean);
          else if (minVals[iG][iE * nFields + iF] - nodeV > TOL )
            lambda = (mean - minVals[iG][iE * nFields + iF]) / (mean - nodeV);
          elemLambda = std::min(elemLambda, lambda);
        }
        // apply limiter
        if(elemLambda < 1.0) {
          elemLambda = std::max(elemLambda, 0.);
          for (int iNode = 0; iNode < nbNodes; iNode++ ) {
            elemData(iNode, iF) = elemLambda * elemData(iNode, iF) + (1. - elemLambda) * mean;
          }
        }
      }
    }
  }
  solution->scatter();
}
