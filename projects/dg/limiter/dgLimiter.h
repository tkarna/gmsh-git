#ifndef _DG_LIMITER_H_
#define _DG_LIMITER_H_
#include "fullMatrix.h"
#include <vector>
#include "dgGroupOfElements.h"

class dgDofContainer;
class dgGroupCollection;
class dgConservationLaw;
class dgCGStructure;
class function;

/**Parent class for limiters */
class dgLimiter{
protected:
  dgConservationLaw *_claw;
  double meanOnElement (const fullVector<double> nodalValues, const nodalBasis &fs);

public:
  dgLimiter (dgConservationLaw *claw) : _claw(claw) {}
  /**apply the limiter on the solution */
  virtual void apply ( dgDofContainer *sol)=0;
  virtual int applyForGroup(dgDofContainer *sol, dgGroupOfElements *goe){
    Msg::Fatal("This kind of limiter is not implemented to be applied for a single group");
    return 0;
  };
  virtual bool boundaryValues(const dgGroupOfFaces * faces, dgDofContainer * solution,
                      fullMatrix<double> & valueOnFaces, const function * secondary=NULL);
  virtual ~dgLimiter(){};
};

/**The usual DG slope limiter: keep the mean, reduces uniformly the slope until it does not overshoot the neighbors' mean */
class dgSlopeLimiter : public dgLimiter{
  function *_detectorFunction;
public :
  dgSlopeLimiter (dgConservationLaw *claw) : dgLimiter (claw), _detectorFunction(NULL){}
  virtual void apply ( dgDofContainer *solution);
  virtual int applyForGroup ( dgDofContainer *solution, dgGroupOfElements *goe);
  /**the limiter will be applied only if this function return a value > 0 */
  inline void setDetector (function * detector) { _detectorFunction = detector; }
};

/** A vertex based slope limiter.
 * For each node, a nodal value must not exceed the mean of elements touching that node.
 * This is achieved by redistributing mass in each element sufficiently.
 * For 3D prisms, the mean must be computed as a weighted average, using the nodal volume as weights.
 * Two limiters exists: convex combination between fully mixed and original state (useOptimalLimiter=false)
 * and optimal (Quadratic Programming) limiter: nodal values are changed as little as possible (useOptimalLimiter=true) */
class dgSlopeLimiterVertex : public dgLimiter{
  dgGroupCollection *_groups;
  dgCGStructure* _CGStruct;
  dgDofContainer* _nodalVolume;
  bool _ownNodalVolume, _optimal;
public :
  bool boundaryValues(const dgGroupOfFaces * faces, dgDofContainer * solution,
                      fullMatrix<double> & valueOnFaces, const function * secondary=NULL);
  dgSlopeLimiterVertex (dgConservationLaw *claw, dgGroupCollection *groups, dgDofContainer* nodalVolume=NULL, bool useOptimalLimiter=false);
  virtual ~dgSlopeLimiterVertex();
  virtual void apply (dgDofContainer *solution);
};

/** original limiter weighted by the volume */
class dgSlopeLimiterOriginal : public dgLimiter{
  dgGroupCollection *_groups;
  dgDofContainer* _nodalVolume;
public :
  virtual bool boundaryValues(const dgGroupOfFaces * faces, dgDofContainer * solution,
                      fullMatrix<double> & valueOnFaces, const function * secondary=NULL);
  dgSlopeLimiterOriginal (dgConservationLaw *claw, dgGroupCollection *groups);
  virtual ~dgSlopeLimiterOriginal();
  virtual void apply (dgDofContainer *solution);
};

/** similar to original limiter. The difference is that for boundary values, the mean is computed on the interface */
class dgSlopeLimiterUV : public dgLimiter{
  dgGroupCollection *_groups;
  dgDofContainer* _nodalVolume;
public :
  virtual bool boundaryValues(const dgGroupOfFaces * faces, dgDofContainer * solution,
                      fullMatrix<double> & valueOnFaces, const function * secondary=NULL);
  dgSlopeLimiterUV (dgConservationLaw *claw, dgGroupCollection *groups);
  virtual ~dgSlopeLimiterUV();
  virtual void apply (dgDofContainer *solution);
};

/** similar to original limiter. The only difference is that on the boundaries where no outside value is given, the old values are used to find the min/max*/
class dgSlopeLimiterOldVals : public dgLimiter{
  dgGroupCollection *_groups;
  dgDofContainer* _nodalVolume;
  dgDofContainer* _oldVals;
public :
  virtual bool boundaryValues(const dgGroupOfFaces * faces, dgDofContainer * solution,
                      fullMatrix<double> & valueOnFaces, const function * secondary=NULL);
  dgSlopeLimiterOldVals (dgConservationLaw *claw, dgGroupCollection *groups, dgDofContainer *oldVals);
  virtual ~dgSlopeLimiterOldVals();
  virtual void apply (dgDofContainer *solution);
};

/** The usual DG slope limiter, but the field could be discontinuous with the tag.
 *  Jumps are done in extending the physical tag area with a layer of elms of the same tag.
 *  Needing of a field in _dofY which corresponds to a continuous representation of the 
 *  disontinuous variable. Two functions make the transform between the main field and
 *  the _dof (direct xToY and reverse yToX transforms)
 **/
class dgSlopeLimiterField : public dgLimiter {
  dgDofContainer *_dofY;
  function *_detectorFunction, *_xToY;
public :
  dgSlopeLimiterField (dgConservationLaw *claw, function *xToY, dgDofContainer *dofY, function *yToX) : 
  dgLimiter (claw), _dofY(dofY), _detectorFunction(NULL), _xToY(xToY) {}
  virtual void apply ( dgDofContainer *solution);
  /**the limiter will be applied only if this function return a value > 0 */
  inline void setDetector (function * detector) { _detectorFunction = detector; }
};

class dgSlopeLimiterField2 : public dgLimiter {
  class replacedFunction;
  function *_xToY;
  replacedFunction *_yToX;
  fullMatrix<double> _replaceBy;
  dgDofContainer* _nodalVolume;
  dgCGStructure* _CGStruct;
  bool _barycenter;
public :
  dgSlopeLimiterField2 (dgConservationLaw *claw, dgGroupCollection* groups, function *xToY, function *y, function *yToX);
  virtual void apply ( dgDofContainer *solution);
  void setBarycenter(bool yn) { _barycenter = yn; }
  ~dgSlopeLimiterField2();
};

/**call the clipToPhysics function of the conservation law */
class dgLimiterClip : public dgLimiter{
public :
  /**A new explicit clipper */
  dgLimiterClip (dgConservationLaw *claw) : dgLimiter (claw) {}
  virtual void apply ( dgDofContainer *solution);
};

/** A class that computes the volume associated to each node. */
class dgNodalVolume {
  dgGroupCollection * _groups;
  dgDofContainer * _nodalVolume;
public:
  /** creates a new instance */
  dgNodalVolume(dgGroupCollection *groups);
  ~dgNodalVolume();
  /** updates the information if mesh has changed */
  void update() { compute(_nodalVolume); }
  /** compute the volumes on the given dofContainer */
  static void compute(dgDofContainer * nodalVolume);
  /** return the data as a dofcontainer */
  inline dgDofContainer* get() { return _nodalVolume; };
};

class dgJumpDiffusion : public dgLimiter {
  const dgGroupCollection &_groups;
  dgDofContainer *_nuField;
  int _dim, _sizeNu;
  int _fieldId;
  int _integrationOrder;
  double _c, _u;
  dgDofContainer *_ext;
 public:
  const dgDofContainer &diffusivity() {return *_nuField;}
  dgJumpDiffusion(const dgGroupCollection &groups, int fieldId, int integrationOrder, double c, double u, dgDofContainer *ext=NULL);
  ~dgJumpDiffusion();
  virtual void apply(dgDofContainer *sol);
};

class dgArtificialDiffusion : public dgLimiter {
  const dgGroupCollection &_groups;
  std::vector< fullMatrix<double> > _LagrangeToLegendreDof, _LegendreToLagrangeDof;
  fullMatrix<double> _psiLe;
  dgDofContainer *_nuhField;
  dgDofContainer *_nuvField;
  function *_sensor;
  int _integrationOrder;
  double _kappa;
 public:
  const dgDofContainer &nuh() {return *_nuhField;}
  const dgDofContainer &nuv() {return *_nuvField;}
  dgArtificialDiffusion(const dgGroupCollection &groups, function *sensor, int integrationOrder, double kappa, double H_over_V);
  ~dgArtificialDiffusion();
  virtual void apply(dgDofContainer *sol);
};


#endif
