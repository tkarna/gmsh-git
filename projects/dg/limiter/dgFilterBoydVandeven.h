#ifndef _DG_FILTER_BOYD_VANDEVEN_H_
#define _DG_FILTER_BOYD_VANDEVEN_H_

#include <vector>
#include <list>
#include "fullMatrix.h"

class dgGroupOfElements;
class dgGroupCollection;
class dgDofContainer;

class dgFilterBoydVandevenGroup {
  const dgGroupOfElements &_group;
  bool _filterHorizontal, _filterVertical;
  fullMatrix<double> _LagrangeToLegendreDof, _LegendreToLagrangeDof;
  fullVector<double> _Sigma;
 public:
  dgFilterBoydVandevenGroup(const dgGroupOfElements &group, double s, double mu, bool filterHorizontal, bool filterVertical);
  void apply(dgDofContainer &solution);
};

class dgFilterBoydVandeven {
  std::list<dgFilterBoydVandevenGroup> _filters;
 public:
  dgFilterBoydVandeven(const dgGroupCollection &groups, const std::string tag, double s, double mu, bool filterHorizontal, bool filterVertical);
  void apply(dgDofContainer &solution);
};

#endif
