%module limiter

%include std_string.i

%{
  #undef HAVE_DLOPEN
  #include "dgFilterBoydVandeven.h"
  #include "dgLimiter.h"
%}

%include "dgFilterBoydVandeven.h"
%include "dgLimiter.h"
