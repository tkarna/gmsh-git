#include <cmath>

#include "dgFilterBoydVandeven.h"
#include "legendrePolynomials.h"
#include "dgIntegrationMatrices.h"
#include "dgGroupOfElements.h"
#include "dgDofContainer.h"
#include "function.h"
#include "MElement.h"

static void sigmaBV1d(int order, double s, fullVector<double> &Sigma){
  s -= 1;
  Sigma.setAll(1);
  if (Sigma.size() > 1){
    for (int k = (int) ceil(s); k < Sigma.size(); k++){
      double theta = (k - s) / (order + 1 - s);
      double thetaBar = std::fabs(theta) - 0.5;
      double sqFac = (fabs(thetaBar)>1e-12? std::sqrt(-std::log(1 - 4 * thetaBar * thetaBar) / (4 * thetaBar * thetaBar)) : 1);
      Sigma(k) = 0.5 * erfc( 2 * std::sqrt((double) order) * thetaBar * sqFac);
    }
  }
}

dgFilterBoydVandevenGroup::dgFilterBoydVandevenGroup(const dgGroupOfElements &group, double s, double mu, bool filterHorizontal, bool filterVertical)
  : _group(group)
{
  _filterHorizontal=filterHorizontal;
  _filterVertical=filterVertical;
  if (group.getElement(0)->getType() != TYPE_QUA && group.getElement(0)->getType() != TYPE_HEX){
    Msg::Fatal("BoydVandeven filter only implemented for quads and hexahedrons");
  }
  dataCacheMap cacheMap(dataCacheMap::INTEGRATION_GROUP_MODE, group.getGroupCollection());
  cacheMap.setGroup(&group);
  const dgIntegrationMatrices &intMatrices = cacheMap.getIntegrationMatrices();
  const fullMatrix<double> &xi = intMatrices.integrationPoints();
  const fullVector<double> &w = intMatrices.integrationWeights();
  const fullMatrix<double> &psiLa = intMatrices.psi();
  int order = group.getFunctionSpace().order;
  LegendrePolynomials leBasis(order);

  //Computation of the Legendre basis functions
  fflush(stdout);
  fullMatrix<double> psiLe(psiLa.size1(), psiLa.size2());
  double *basisVal[3];
  int dim = group.getDimUVW();
  int nbI = order+1;
  int nbJ = (dim >= 2) ? (order+1) : 1;
  int nbK = (dim >= 3) ? (order+1) : 1;
  for (int iDim = 0; iDim < 3; iDim++){
    basisVal[iDim] = new double[psiLe.size2()];
    for (int i = 0; i < psiLe.size2(); i++){
      basisVal[iDim][i] = 1;
    }
  }
  for (int iPt = 0; iPt < psiLe.size1(); iPt++){
    for (int iDim = 0; iDim < dim; iDim++){
      leBasis.f(xi(iPt,iDim),basisVal[iDim]);
    }
    for (int i = 0; i < nbI; i++){
      for (int j = 0; j < nbJ; j++){
	for (int k = 0; k < nbK; k++){
	  psiLe(iPt, i + nbI * j + nbI * nbJ * k) = basisVal[0][i] * basisVal[1][j] * basisVal[2][k];
	}
      }
    }
  }
  for (int iDim = 0; iDim < 3; iDim++){
    delete[] basisVal[iDim];
  }

  //Computation of the mass matrices. We neglect the jacobian of the transformation here
  fullMatrix<double> intPsiLePsiLa(psiLe.size2(), psiLe.size2());
  fullMatrix<double> invMassLe(psiLe.size2(), psiLe.size2());
  intPsiLePsiLa.setAll(0);
  invMassLe.setAll(0);
  for (int i = 0; i < psiLe.size2(); i++){
    for (int j = 0; j < psiLe.size2(); j++){
      for (int iPt = 0; iPt < psiLe.size1(); iPt++){
	intPsiLePsiLa(i,j) += psiLe(iPt, i) * psiLa(iPt, j) * w(iPt);
	invMassLe(i,j) += psiLe(iPt, i) * psiLe(iPt, j) * w(iPt);
      }
    }
  }
  invMassLe.invertInPlace();
  _LagrangeToLegendreDof.resize(psiLe.size2(), psiLe.size2());
  invMassLe.mult(intPsiLePsiLa, _LagrangeToLegendreDof);
  _LegendreToLagrangeDof.resize(psiLe.size2(), psiLe.size2());
  _LagrangeToLegendreDof.invert(_LegendreToLagrangeDof);

  //Boyd-Vandeven filter
  _Sigma.resize(nbI*nbJ*nbK);

  fullVector<double> SigmaI(nbI);
  fullVector<double> SigmaJ(nbJ);
  fullVector<double> SigmaK(nbK);
  SigmaI.setAll(1);
  SigmaJ.setAll(1);
  SigmaK.setAll(1);

  switch (dim) {
  case 2:
    if (_filterHorizontal){
      sigmaBV1d(order, s, SigmaI);
    }
    if (_filterVertical){
      sigmaBV1d(order, s, SigmaJ);
    }
    break;
  case 3:
    if (_filterHorizontal){
      sigmaBV1d(order, s, SigmaI);
      sigmaBV1d(order, s, SigmaJ);
    }
    if (_filterVertical){
      sigmaBV1d(order, s, SigmaK);
    }
    break;
  default:
    Msg::Fatal("Boyd-Vandeven filter not implemented for dimension %i",dim);
    break;
  }
  for (int i = 0; i < nbI; i++){
    for (int j = 0; j < nbJ; j++){
      for (int k = 0; k < nbK; k++){
	double Sigma = SigmaI(i) * SigmaJ(j) * SigmaK(k);
	_Sigma(i + nbI * j + nbI * nbJ * k) = (1.0 - mu) + mu * Sigma;
      }
    }
  }
}

void dgFilterBoydVandevenGroup::apply(dgDofContainer &solution){
  solution.multiplyByLocalMatrix(_group, _LagrangeToLegendreDof);
  solution.multiplyByDiagonalVector(_group, _Sigma);
  solution.multiplyByLocalMatrix(_group, _LegendreToLagrangeDof);
}

dgFilterBoydVandeven::dgFilterBoydVandeven(const dgGroupCollection &groups, const std::string tag, double s, double mu, bool filterHorizontal, bool filterVertical){
  for(int k = 0; k < groups.getNbElementGroups(); k++) {
    dgGroupOfElements *group = groups.getElementGroup(k);
    if (group->getPhysicalTag() == tag || tag == ""){
      _filters.push_back(dgFilterBoydVandevenGroup(*group, s, mu, filterHorizontal, filterVertical));
    }
  }
}

void dgFilterBoydVandeven::apply(dgDofContainer &solution){
  for (std::list<dgFilterBoydVandevenGroup>::iterator it = _filters.begin(); it != _filters.end(); it++){
    it->apply(solution);
  }
}
