#ifndef _DG_ELEMENT_VECTOR_
#define _DG_ELEMENT_VECTOR_

#include <vector>
#include <string>
#include "dgFullMatrix.h"

class MElement;
class nodalBasis;

/** A vector of MElement which all have the same type. */
class dgElementVector {
  std::vector<MElement*> _elements;
  std::string _physicalTag;
  const nodalBasis *_functionSpace;
  bool _sequentialLoad;
  dgFullMatrix<double> _coordinates;
 public:
  dgFullMatrix<double> &coordinates() {return _coordinates;};
  const dgFullMatrix<double> &coordinates() const {return _coordinates;};
  dgElementVector(const std::vector<MElement*> &elements, const std::string physicalTag, bool sequentialLoad);
  inline MElement &element(int i) const {return *_elements[i];}
  inline int size() const {return _elements.size();}
  inline const std::string physicalTag() const {return _physicalTag;}
  inline const nodalBasis &functionSpace() const {return *_functionSpace;}
  int originalPartition() const;
};

#endif
