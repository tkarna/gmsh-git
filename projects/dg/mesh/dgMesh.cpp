#include "dgMesh.h"
#include "dgElementVector.h"
#include "dgInterfaceVector.h"

#include "GModel.h"
#include "MElement.h"
#include "nodalBasis.h"
#include <stdio.h>

#include <algorithm>

#include "dgMeshJacobian.h"

//transitional
#include "dgGroupOfElements.h"

typedef struct {
  int groupId;
  int elementId;
  size_t closureId;
  int closureRef;
} _tmpConnection;

class _tmpInterface {
  public:
  std::vector<int> vertices;
  std::vector<_tmpConnection> connections;
  std::string physicalTag;
  MElement *element;
  _tmpInterface() {
    element = NULL;
  }
  bool operator < (const _tmpInterface &o) const {
    if (connections.size() < o.connections.size())
      return true;
    if (connections.size() > o.connections.size())
      return false;
    if (physicalTag < o.physicalTag)
      return true;
    if (physicalTag > o.physicalTag)
      return false;
    for (size_t i = 0; i < connections.size(); ++i) {
      if (connections[i].groupId < o.connections[i].groupId)
        return true;
      if (connections[i].groupId > o.connections[i].groupId)
        return false;
    }
    for (size_t i = 0; i < connections.size(); ++i) {
      if (connections[i].closureRef < o.connections[i].closureRef)
        return true;
      if (connections[i].closureRef > o.connections[i].closureRef)
        return false;
    }
    return false;
  }
};

static void buildInterfaces(const dgMesh &mesh, std::vector<dgInterfaceVector *> &interfaces, std::map<int, std::pair<int, int> > &meshInterfaceMap, std::map<int, int> &periodicMapVertex, bool sequentialLoad) {

  if (mesh.nElementVector() == 0)
    return;
  int dimension = mesh.elementVector(0).functionSpace().dimension;
  std::map<std::vector<int>, _tmpInterface> interfaceMap;
  std::map<int, std::vector<int> > meshElementIdToVerticesMap;
  std::vector<GEntity*> entities;
  mesh.gModel()->getEntities(entities);
  for (size_t i = 0; i < entities.size(); i++) {
    GEntity *entity = entities[i];
    std::string entityPhysicalTag = entity->physicals.empty() ? "" : mesh.gModel()->getPhysicalName(entity->dim(), entity->physicals[0]);
    if (entity->dim() == dimension - 1) {
      for (size_t iel = 0; iel < entity->getNumMeshElements(); iel++) {
        MElement &e = *entity->getMeshElement(iel);
        std::vector<int> verticesId;
        verticesId.reserve(e.getNumVertices());
        for (int l = 0; l < e.getNumVertices(); ++l) {
          verticesId.push_back(e.getVertex(l)->getNum());
        }
        std::vector<int> sortedVerticesId = verticesId;
        std::sort(sortedVerticesId.begin(), sortedVerticesId.end());
        meshElementIdToVerticesMap[e.getNum()] = sortedVerticesId;
        _tmpInterface &interface = interfaceMap[sortedVerticesId];
        interface.physicalTag = entityPhysicalTag;
        interface.element = &e;
        interface.vertices = verticesId;
      }
    }
  }
  
  for (int iev = 0; iev < mesh.nElementVector() + mesh.nGhostElementVector(); ++iev) {
    const dgElementVector &ev =mesh.elementVector(iev);
    const nodalBasis &fs = ev.functionSpace();
    for (int ie = 0; ie < ev.size(); ++ie) {
      MElement &e = ev.element(ie);
      for (size_t ic = 0; ic< fs.closures.size(); ++ic) {
        std::vector<int> verticesId;
        if(fs.closures[ic].empty())
          continue;
        verticesId.reserve(fs.closures[ic].size());
        for (size_t l = 0; l < fs.closures[ic].size(); ++l) {
          verticesId.push_back(e.getVertex(fs.closures[ic][l])->getNum());
          periodicMapVertex[e.getVertex(fs.closures[ic][l])->getNum()] = e.getVertex(fs.closures[ic][l])->getNum();
        }
        std::vector<int> sortedVerticesId = verticesId;
        std::sort(sortedVerticesId.begin(), sortedVerticesId.end());
        _tmpInterface &interface = interfaceMap[sortedVerticesId];
        if (interface.vertices.empty())
          interface.vertices = verticesId;
        if (interface.vertices == verticesId) {
          _tmpConnection connection = {iev, ie, ic, fs.closureRef[ic]};
          interface.connections.push_back(connection);
        }
      }
    }
  }  
  
  /** Reconnect elements for periodic mesh **/
  /* Read the periodic connections map and create perEdgeMap */
  std::map<int, std::pair<int, std::map<int, int> > > perEdgeMap;
  std::map<int, std::pair<int, std::map<int, int> > >::iterator perEdgeMapIt;
  const char* filename = "periodicMesh.txt";
  std::vector<int> verticesId;
  FILE* file;
  file = fopen(filename, "r");
  if (file !=NULL) {
    int perBndNum, perBndEdgeNum, vertexNum, tmp, edge0Id, edge1Id;
    if(!fscanf(file, "%d", &perBndNum)) return;
    for (int iPerioBnd = 0; iPerioBnd < perBndNum; ++iPerioBnd) {
      if(!fscanf(file, "%d", &perBndEdgeNum)) return;
      for (int iEdge = 0; iEdge < perBndEdgeNum; ++iEdge){
        if(!fscanf(file, "%d %d", &edge0Id, &edge1Id)) return;
        if(!fscanf(file, "%d", &vertexNum)) return;
        perEdgeMap[edge0Id].first = edge1Id;
        verticesId.clear();
        verticesId.reserve(vertexNum);
        for (int l = 0; l < vertexNum; ++l) {
          if(!fscanf(file, "%d", &tmp)) return;
          verticesId.push_back(tmp);
        }
        if(!fscanf(file, "%d", &vertexNum)) return;
        for (int l = 0; l < vertexNum; ++l) {
          if(!fscanf(file, "%d", &tmp)) return;
          perEdgeMap[edge0Id].second[tmp] = verticesId[l];
          periodicMapVertex[tmp] = verticesId[l];
        }
      }
    }
    fclose(file);
    
    /* Reconnect the elements */
    std::map<std::vector<int>, _tmpInterface>::iterator interfaceMapIt = interfaceMap.begin(); 
    while ( interfaceMapIt != interfaceMap.end() ){
      if (interfaceMapIt->second.connections.size() == 1 && !interfaceMapIt->second.physicalTag.empty()){
        if(interfaceMapIt->second.element == NULL)
          Msg::Fatal("The interface should have an element\n");
        perEdgeMapIt = perEdgeMap.find(interfaceMapIt->second.element->getNum());
        if (perEdgeMapIt != perEdgeMap.end()){
          _tmpConnection connection0 = interfaceMapIt->second.connections[0];
          const dgElementVector &ev0 = mesh.elementVector(connection0.groupId);
          const nodalBasis &fs0 = ev0.functionSpace();
          MElement &e0 = ev0.element(connection0.elementId);
          std::vector<int> sortedVerticesId = meshElementIdToVerticesMap[perEdgeMapIt->second.first];
          if (interfaceMap.find(sortedVerticesId) == interfaceMap.end()){
            interfaceMap.erase(interfaceMapIt++);
            continue;
          }
          if(interfaceMap[sortedVerticesId].connections.empty()){
            interfaceMap.erase(interfaceMapIt++);
            continue;
          }
          _tmpConnection connection1 = interfaceMap[sortedVerticesId].connections[0];
          if (std::min(connection0.groupId, connection1.groupId) >= mesh.nElementVector()){
            interfaceMap.erase(interfaceMapIt++);
            interfaceMap.erase(sortedVerticesId);
            continue;
          }
          const dgElementVector &ev1 = mesh.elementVector(connection1.groupId);
          const nodalBasis &fs1 = ev1.functionSpace();
          MElement &e1 = ev1.element(connection1.elementId);
          verticesId.clear();
          verticesId.reserve(fs1.closures[connection1.closureId].size());
          for (size_t l = 0; l < fs1.closures[connection1.closureId].size(); ++l) {
            verticesId.push_back(e1.getVertex(fs1.closures[connection1.closureId][l])->getNum()); 
          }
          bool haveFound = false;
          for (size_t ic = 0; ic< fs0.closures.size(); ++ic) {
            if(fs0.closures[ic].empty())
              continue;
            bool match = true;
            for (size_t l = 0; l < fs0.closures[ic].size(); ++l) {
              if (perEdgeMapIt->second.second[verticesId[l]] != e0.getVertex(fs0.closures[ic][l])->getNum()){
                match = false;
                break;
              }
            }
            if (match == true){
              connection0.closureId = ic;
              connection0.closureRef = fs0.closureRef[ic];
              haveFound = true;
              break;
            }
          }
          if (haveFound == false)
            Msg::Fatal("There is no match between the periodic boundaries %d\n", Msg::GetCommRank());
          interfaceMap[sortedVerticesId].connections.push_back(connection0); 
          perEdgeMap.erase(perEdgeMapIt);
          interfaceMap.erase(interfaceMapIt++);
        }
        else{
          ++interfaceMapIt;
        }
      }
      else {
        ++interfaceMapIt;
      }
    }
    
    interfaceMapIt = interfaceMap.begin(); 
    while ( interfaceMapIt != interfaceMap.end() ){
      if (interfaceMapIt->second.connections.size() == 1){
        if (interfaceMapIt->second.physicalTag == "paste"){
          //std::cout << "physical Tag " << interfaceMapIt->second.physicalTag << " proc : " << Msg::GetCommRank() << "\n";
          interfaceMap.erase(interfaceMapIt++);
        }
        else
          ++interfaceMapIt;
      }
      else{
        ++interfaceMapIt;
      }
    }
    
  
    Msg::Info("Done reconnecting periodic elements");
  }
  /** end reconnect interfaces for periodic mesh **/
 

  std::vector<_tmpInterface> allInterfaceVector;
  allInterfaceVector.reserve(interfaceMap.size());
  for (std::map<std::vector<int>, _tmpInterface>::iterator it = interfaceMap.begin(); it != interfaceMap.end(); ++it) {
    allInterfaceVector.push_back(it->second);
  }
  sort(allInterfaceVector.begin(), allInterfaceVector.end());
  interfaces.clear();
  meshInterfaceMap.clear();
  int vectorStart = 0;

  int iGroup = 0;
  for (size_t i = 1; i <= allInterfaceVector.size(); ++i) {
    if (i == allInterfaceVector.size() || allInterfaceVector[i] < allInterfaceVector[i - 1] || allInterfaceVector[i - 1] < allInterfaceVector[i]) {
      int nConnections = allInterfaceVector[vectorStart].connections.size();
      std::vector<const dgElementVector*> elV(nConnections);
      std::vector<MElement *> mel(i - vectorStart, (MElement *)NULL);
      std::vector<std::vector<std::pair<int, int> > > el(nConnections, std::vector<std::pair<int, int> >(i - vectorStart));
      for (int k = 0; k < nConnections; ++k) {
        elV[k] = &mesh.elementVector(allInterfaceVector[vectorStart].connections[k].groupId);
        for (size_t j = vectorStart; j < i; ++j) {
          _tmpConnection &c = allInterfaceVector[j].connections[k];
          el[k][j - vectorStart] = std::make_pair(c.elementId, c.closureId);
          mel[j - vectorStart] = allInterfaceVector[j].element;
        }
      }
      const std::string physicalTag = allInterfaceVector[vectorStart].physicalTag;
      bool onlyGhost = !sequentialLoad;
      for (size_t j = 0; j < elV.size(); ++j) {
        onlyGhost &= (elV[j]->originalPartition() != Msg::GetCommRank());
      }
      if (!onlyGhost && !elV.empty()) {
        interfaces.push_back(new dgInterfaceVector(elV, el, mel, physicalTag));
        for (size_t j = vectorStart; j < i; ++j) {
          if (allInterfaceVector[j].element) {
            meshInterfaceMap[allInterfaceVector[j].element->getNum()] = std::make_pair(iGroup, j - vectorStart);
          }
        }
        iGroup++;
      }
      vectorStart = i;
    }
  }
}

dgMesh::dgMesh(GModel *g, int dimension, const std::vector<std::string> physicalTags, bool sequentialLoad)
{
  _spaceTransform = NULL;
  //
  // ElementVector
  //
  _gmodel = g;
  if (dimension == 0)
    dimension = g->getDim();
  sequentialLoad |= (Msg::GetCommSize() == 1);
  std::vector<GEntity*> entities;
  g->getEntities(entities);
  std::map<std::pair<int, std::string>, std::vector<MElement *> > localElements;
  int nbPartitions = std::max((int)g->getMeshPartitions().size(), 1 );
  if (!sequentialLoad && Msg::GetCommSize() != nbPartitions )
    Msg::Error("Mesh partitions do not match to number of processes %d %d", nbPartitions, Msg::GetCommSize());
  std::vector<std::map<int, std::vector<MElement *> > > ghostElements(sequentialLoad ? 1 : Msg::GetCommSize());// [partition][elementType]
  std::multimap<MElement*, short> &ghostsCells = g->getGhostCells();
  // take the elements (real and ghost) belonging to the current process
  for(size_t i = 0; i < entities.size(); i++) {
    GEntity *entity = entities[i];
    std::string entityPhysicalTag = entity->physicals.empty() ? "" : g->getPhysicalName(dimension, entity->physicals[0]);
    // we add only the elements with the requested physicalTags
    bool correctPhysicalTag = false;
    if( physicalTags.empty() ) {
      correctPhysicalTag = true;
    }
    else {
      for(size_t iPhy = 0; iPhy < entity->physicals.size(); iPhy++) {
        for(size_t jPhy = 0; jPhy < physicalTags.size(); jPhy++) {
          if (g->getPhysicalName(dimension, entity->physicals[iPhy]) == physicalTags[jPhy] ) {
            correctPhysicalTag = true;
          }
        }
      }
    }

    if(entity->dim() == dimension && correctPhysicalTag) {
      for (size_t iel = 0; iel < entity->getNumMeshElements(); iel++) {
        MElement *el = entity->getMeshElement(iel);
        // if belong to the local partition => add the element
        if(el->getPartition() == Msg::GetCommRank() + 1 || sequentialLoad) {
          localElements[std::make_pair(el->getType(), entityPhysicalTag)].push_back(el);
        } else { // belong to - maybe - neightbour partition => catch the right ghost elems
          std::multimap<MElement*, short>::iterator ghost = ghostsCells.lower_bound(el);
          //todo give physical tags to ghost
          while(ghost != ghostsCells.end() && ghost->first == el) {
            if(abs(ghost->second) - 1 == Msg::GetCommRank()) {
              ghostElements[el->getPartition() - 1][el->getType()].push_back(el);
            }
            ghost++;
          }
        }
      }
    }
  }
  if(localElements.size() == 0)
    Msg::Fatal("no element loaded in dgMesh !");

  int iGroup = 0;
  for (std::map<std::pair<int, std::string>, std::vector<MElement *> >::iterator it = localElements.begin(); it != localElements.end() ; ++it) {
    _elements.push_back(new dgElementVector(it->second, it->first.second, sequentialLoad));
    for (size_t i = 0; i < it->second.size(); ++i) {
      _elementMap[it->second[i]->getNum()] = std::make_pair(iGroup, i);
    }
    iGroup++;
  }
  //create ghost groups
  if (! sequentialLoad) {
    for(int i = 0; i <Msg::GetCommSize(); i++) {
      for (std::map<int, std::vector<MElement *> >::iterator it = ghostElements[i].begin(); it != ghostElements[i].end() ; ++it) {
        _ghostElements.push_back(new dgElementVector(it->second, "", sequentialLoad));
        for (size_t i = 0; i < it->second.size(); ++i) {
          _elementMap[it->second[i]->getNum()] = std::make_pair(iGroup, i);
        }
        iGroup++;
      }
    }
  }
  buildInterfaces(*this, _interfaces, _interfaceMap, _periodicMap, sequentialLoad);
}

void dgMesh::findElement(int mElementId, int &iGroup, int &iElement) const
{
  std::map<int,std::pair<int,int> >::const_iterator it = _elementMap.find(mElementId);
  if (it == _elementMap.end() ) {
    iGroup = iElement = -1;
  } else {
    iGroup = it->second.first;
    iElement = it->second.second;
  }
}

void dgMesh::findInterface(int mElementId, int &iGroup, int &iFace) const
{
  std::map<int,std::pair<int,int> >::const_iterator it = _interfaceMap.find(mElementId);
  if (it == _interfaceMap.end() ) {
    iGroup = iFace = -1;
  } else {
    iGroup = it->second.first;
    iFace = it->second.second;
  }
}

const dgMeshJacobian &dgMesh::getJacobian(int integrationOrder) const
{
  dgMeshJacobian *&j = _jacobian[integrationOrder];
  if (!j) {
    j = new dgMeshJacobian(*this, integrationOrder);
  }
  return *j;
}

void dgMesh::freeJacobian(int integrationOrder) const
{
  if(integrationOrder > 0) {
    std::map<int, dgMeshJacobian*>::iterator it = _jacobian.find(integrationOrder);
    if (it != _jacobian.end()) {
      delete it->second;
      _jacobian.erase(it);
    }
  }
  else {
    for (std::map<int, dgMeshJacobian*>::iterator it = _jacobian.begin(); it != _jacobian.end(); ++it) {
      delete it->second;
    }
  }
  _jacobian.clear();
}

dgMesh::~dgMesh()
{
  for (std::map<int, dgMeshJacobian*>::iterator it = _jacobian.begin(); it != _jacobian.end(); ++it) {
    delete it->second;
  }
  for (int i = 0; i < nElementVector(); ++i)
    delete _elements[i];
  for (int i = 0; i < nInterfaceVector(); ++i)
    delete _interfaces[i];
}

//transitional constructor
dgMesh::dgMesh(const dgGroupCollection &groups) {
  _gmodel = groups.getModel();
  _spaceTransform = groups.getSpaceTransform();
  for (int iGroup = 0; iGroup < groups.getNbElementGroups() + groups.getNbGhostGroups(); ++iGroup) {
    const dgGroupOfElements &group = *groups.getElementGroup(iGroup);
    std::vector<MElement*> tmpVec(group.getNbElements());
    for (size_t i = 0; i < group.getNbElements(); ++i) {
      tmpVec[i] = group.getElement(i);
      _elementMap[group.getElement(i)->getNum()] = std::make_pair(iGroup, i);
    }
    if (iGroup < groups.getNbElementGroups())
      _elements.push_back(new dgElementVector(tmpVec, group.getPhysicalTag(), groups.getIsDefinedOnOneProcessor()));
    else
      _ghostElements.push_back(new dgElementVector(tmpVec, group.getPhysicalTag(), false));
  }
  buildInterfaces(*this, _interfaces, _interfaceMap, _periodicMap, groups.getIsDefinedOnOneProcessor());
}

void dgMesh::setSpaceTransform(dgSpaceTransform &spaceTransform, dgGroupCollection &groups){
  _spaceTransform=&spaceTransform;
  freeJacobian();
  for (int i=0; i<groups.getNbElementGroups()+groups.getNbGhostGroups(); i++){
    groups.getElementGroup(i)->computeMatrices();
  }
}
