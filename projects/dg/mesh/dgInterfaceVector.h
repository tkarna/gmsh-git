#ifndef _DG_INTERFACE_VECTOR_
#define _DG_INTERFACE_VECTOR_

#include <string>
#include <vector>

#include "dgElementVector.h"

class MElement;

/**
 * Vector of interfaces.
 *  As part of the dgMesh, the position of the interfaces in this vector implicitely defined their id (as interfaceVectorId, id)
 */
class dgInterfaceVector {
  std::vector<const dgElementVector*> _elementVector;
  std::vector<MElement*> _mElement; // only for element present in the mesh
  std::vector<int> _closureRef;
  std::vector<std::vector<std::pair<int, int> > > _element; //elementId, closureId
  std::string _physicalTag;
 public:
  dgInterfaceVector(const std::vector<const dgElementVector *> elementVector,  const std::vector<std::vector<std::pair<int, int> > > element, const std::vector<MElement *> mElement, const std::string physicalTag);
  inline int nConnection() const {return _elementVector.size();}
  inline int size() const {return _element[0].size();}
  inline int closureRef(int iConnection) const {return _closureRef[iConnection];}
  inline const dgElementVector &elementVector(int iConnection) const {return *_elementVector[iConnection];}
  /** id of the element on the iConnection'th side of the interface */
  inline int elementId(int iInterface, int iConnection) const {return _element[iConnection][iInterface].first;}
  inline int closureId(int iInterface, int iConnection) const {return _element[iConnection][iInterface].second;}
  inline MElement &element(int iInterface, int iConnection) const {return _elementVector[iConnection]->element(elementId(iInterface, iConnection));}
  inline const std::string &physicalTag() const {return _physicalTag;}
  inline MElement * faceElement(int iInterface) const {return _mElement[iInterface];}
};

#endif
