%module dgMesh

%include std_vector.i
%import(module="dgpy.dgCommon") "dgCommon.i";
%typedef int size_t;

%{
#undef HAVE_DLOPEN
#include "dgGroupOfElements.h"
#include "dgElementVector.h"
#include "dgInterfaceVector.h"
#include "dgMeshJacobian.h"
#include "dgCGStructure.h"
#include "dgMesh.h"
#include "dgHOMesh.h"
#include "dgNeighbourMap.h"
#include "dgSpaceTransform.h"
#include "dgCircleTransform.h"
#include "dgTransformNodalValue.h"
#include "function.h"
#include "dgExtrusion.h"
%}

%import(module="dgpy.dgFunction") "function.h"

%include "dgGroupOfElements.h"
%apply int &OUTPUT {int &iVertical, int &iLevel}
%apply int &OUTPUT {int &iGroup2, int &iElement2, int &iNode2}
%apply int &OUTPUT {int &iGroup2d, int &iElem2d, int &iNode2d}
%include "dgExtrusion.h"
%include "dgElementVector.h"
%include "dgInterfaceVector.h"
%include "dgMeshJacobian.h"
%include "dgCGStructure.h"

%apply int &OUTPUT {int &iGroup, int &iElement}
%apply int &OUTPUT {int &iGroup, int &iInterface}
%include "dgMesh.h"
%include "dgHOMesh.h"

%apply int &OUTPUT { int &jGroup, int &jFace, int &jConnection}
%include "dgNeighbourMap.h"

%apply double &OUTPUT { double &xOut, double &yOut, double &zOut}
%include "dgSpaceTransform.h"
%include "dgCircleTransform.h"
%include "dgTransformNodalValue.h"

%template(VectorGroupCollection) std::vector<const dgGroupCollection*>;

