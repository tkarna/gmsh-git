#include <set>
#include <list>

#include "function.h"
#include "dgExtrusion.h"
#include "dgNeighbourMap.h"
#include "dgGroupOfElements.h"
#include "MElement.h"
#ifdef HAVE_MPI
#include "mpi.h"
#endif

void dgExtrusion::_check2d3dParametricMatch() const
{
  for (int iG = 0; iG < _groups->getNbElementGroups(); ++iG) {
    const dgGroupOfElements &g3 = *_groups->getElementGroup(iG);
    for (size_t iE = 0; iE < g3.getNbElements(); ++iE) {
      size_t iG2, iE2;
      map3d2d(iG, iE, iG2, iE2);
      MElement *e3 = g3.getElement(iE);
      MElement *e2 = _groups2d->getElementGroup(iG2)->getElement(iE2);
      
      if (g3.getDimUVW() == 3){
	double D = hypot(e3->getVertex(0)->x() - e3->getVertex(1)->x(), e3->getVertex(0)->y() - e3->getVertex(1)->y());
	for (int iN = 0; iN < e3->getNumPrimaryVertices(); ++iN) {
	  double u, v, w;
	  e3->getNode(iN, u, v, w);
	  SPoint3 p3, p2; 
	  e3->pnt(u, v, w, p3);
	  e2->pnt(u, v, 0, p2);
	  if (hypot(p3.x() - p2.x(), p3.y() - p2.y()) > D * 1e-7)
	    Msg::Fatal("parametric coordinates of 3d element %i does not match parametric coordinates of 2d element %i ([%.15e, %.15e] != [%.15e, %.15e])\n", e3->getNum(), e2->getNum(),p2.x(),p2.y(),p3.x(),p3.y());
	}
      }else if (g3.getDimUVW() == 2){
	double D = fabs(e3->getVertex(0)->x() - e3->getVertex(1)->x());
	for (int iN = 0; iN < e3->getNumPrimaryVertices(); ++iN) {
	  double u, v, w;
	  e3->getNode(iN, u, v, w);
	  SPoint3 p3, p2; 
	  e3->pnt(u, v, 0, p3);
	  e2->pnt(u, 0, 0, p2);
	  if (fabs(p3.x() - p2.x()) > D * 1e-10)
	    Msg::Fatal("parametric coordinates of 2d element %i does not match parametric coordinates of 1d element %i (%e != %e)\n", e3->getNum(), e2->getNum(),p2.x(),p3.x());
	}
      }
    }
  }
}

dgExtrusion::dgExtrusion(dgGroupCollection* groups3d, dgGroupCollection* groups2d, std::vector<std::string> topLevelTags)
{
  _groups = groups3d;
  _groups2d = groups2d;
  _topLevelTags = topLevelTags;
  dgNeighbourMap neighbourMap(*_groups);

  size_t nbCols = 0;
  // find faces with correct tags
  for (int iFaceGroup = 0; iFaceGroup < _groups->getNbFaceGroups(); iFaceGroup++) {
    dgGroupOfFaces *faces = _groups->getFaceGroup(iFaceGroup);
    bool isTopFaceGroup = false;
    for (size_t i=0;i<_topLevelTags.size();i++) {
      if(_topLevelTags[i] == faces->physicalTag()) {
        if (faces->nConnection() != 1) {
          Msg::Fatal("More that one connection found, not bnd face! (%i connections, tag \"%s\")", faces->nConnection(), faces->physicalTag().c_str());
        }
        isTopFaceGroup = true;
      }
    }
    if (! isTopFaceGroup)
      continue;
    nbCols += faces->size();
    _topFaceGroups.push_back(faces);
    size_t groupId = _groups->getElementGroupId(&faces->elementGroup(0));
    for (size_t iFace = 0; iFace < faces->size(); iFace++) {
      std::set<size_t> lastVertices;
      std::vector<std::pair<size_t, size_t> > column;
      if (_groups2d) {
        int iGroup2d, iElem2d;
        _groups2d->find(faces->meshInterfaceElement(iFace), iGroup2d, iElem2d);
        _col2Elem2d.push_back(std::make_pair(iGroup2d, iElem2d));
      }
      int gId = groupId;
      int eId = faces->elementId(iFace, 0);
      column.push_back(std::make_pair(gId, eId));
      MElement &elFace = *faces->meshInterfaceElement(iFace);
      for (int i = 0; i < elFace.getNumPrimaryVertices(); ++i) {
        lastVertices.insert(elFace.getVertex(i)->getNum());
      }
      bool neighbourFound = true;
      while(neighbourFound) {
        neighbourFound = false;
        for (int iFace = 0; iFace < neighbourMap.getNbFaces(gId); iFace++) {
          int faceGroupId, faceId, connectionId;
          neighbourMap.getFace(gId, eId, iFace, faceGroupId, faceId, connectionId);
          dgGroupOfFaces &faceGroup = *_groups->getFaceGroup(faceGroupId);
          if (faceGroup.nConnection() != 2)
            continue;
          MElement &el = *_groups->getElementGroup(gId)->getElement(eId);
          std::vector<int> closure = el.getFunctionSpace(1)->closures[faceGroup.closureId(faceId, connectionId)];
          bool shareVertex = false;
          for (size_t i = 0; i < closure.size(); ++i) {
            shareVertex |= (lastVertices.count(el.getVertex(closure[i])->getNum()) > 0);
          }
          if (!shareVertex) {
            neighbourMap.getNeighbour(gId, eId, iFace, gId, eId);
            column.push_back(std::make_pair(gId, eId));
            neighbourFound = true;
            lastVertices.clear();
            for (size_t i = 0; i < closure.size(); ++i) {
              lastVertices.insert(el.getVertex(closure[i])->getNum());
            }
          }
        }
      }
      _colPos2Elem.push_back(column);
    }
  }
  if (nbCols == 0) {
    std::string errMsg = "No boundary faces found with the given physical tag(s): ";
    for (size_t i = 0; i < _topLevelTags.size(); i++)
      errMsg += _topLevelTags.at(i) += " ";
    Msg::Fatal(errMsg.c_str());
  }
  // the link with gotm requires the column to be numbered from bottom to top
  for (size_t iCol = 0; iCol < nbCols; iCol++)
    std::reverse(_colPos2Elem[iCol].begin(), _colPos2Elem[iCol].end());
  
  _nbResidentCols = _colPos2Elem.size();
#ifdef HAVE_MPI
  // exchange ghost information via MPI
  if (_groups2d) {
    // loop over all 2d image elements
    // send column information as a list of 3d element numbers
    for (int partID=0; partID<Msg::GetCommSize(); partID++) {
      for (int i=0; i<_groups2d->getNbImageElementsOnPartition(partID); i++) {
        int iG2d = _groups2d->getImageElementGroup(partID,i);
        int iE2d = _groups2d->getImageElementPositionInGroup(partID,i);
        // find column id
        size_t iCol=0;
        for (; iCol<_col2Elem2d.size(); iCol++) {
          int ig = _col2Elem2d[iCol].first;
          int ie = _col2Elem2d[iCol].second;
          if ((iG2d==ig) & (iE2d==ie))
            break;
        }
        // pack to buffer
        std::vector<int> buffer;
        for (size_t k=0; k<_colPos2Elem[iCol].size();k++) {
          int num = _groups->getElementGroup(_colPos2Elem[iCol][k].first)->getElement(_colPos2Elem[iCol][k].second)->getNum();
          buffer.push_back(num);
        }
        // send to target process with elem2d num as tag
        int tag = _groups2d->getElementGroup(iG2d)->getElement(iE2d)->getNum();
        int nBuf = buffer.size();
        // first send the length of the buffer
        MPI_Send(&nBuf, 1, MPI_INT, partID, tag+1e6, MPI_COMM_WORLD);
        MPI_Send(buffer.data(), nBuf, MPI_INT, partID, tag, MPI_COMM_WORLD);
        std::vector<std::pair<size_t,size_t> > column = _colPos2Elem[iCol];
      }
      // receive 3d elem numbers from other processes, assemble correctly with ghost group IDs
      for (int i=0; i<_groups2d->getNbGhostElementsFromPartition(partID); i++) {
        int iG2d = _groups2d->getGhostElementGroup(partID,i);
        int iE2d = _groups2d->getGhostElementPositionInGroup(partID,i);
        int tag = _groups2d->getElementGroup(iG2d)->getElement(iE2d)->getNum();
        int nBuf;
        MPI_Recv(&nBuf, 1, MPI_INT, partID, tag+1e6, MPI_COMM_WORLD,MPI_STATUS_IGNORE);
        std::vector<int> buffer(nBuf,-1);
        MPI_Recv(buffer.data(), nBuf, MPI_INT, partID, tag, MPI_COMM_WORLD,MPI_STATUS_IGNORE);
        // map received elem num to ghost iGroup,iElement
        std::vector<std::pair<size_t, size_t> > column;
        for (size_t k=0; k<buffer.size(); k++) {
          for (int j=0; j<_groups->getNbGhostElementsFromPartition(partID);j++) {
            int iG = _groups->getGhostElementGroup(partID,j);
            int iE = _groups->getGhostElementPositionInGroup(partID,j);
            int num = _groups->getElementGroup(iG)->getElement(iE)->getNum();
            if (num==buffer[k]) {
              column.push_back(std::make_pair(iG, iE));
              continue;
            }
          }
        }
        // append to arrays
        _col2Elem2d.push_back(std::make_pair(iG2d, iE2d));
        _colPos2Elem.push_back(column);
      }
    }
  }
#endif
  // build reverse map
  _elem2ColPos.resize(_groups->getNbElementGroups()+_groups->getNbGhostGroups());
  for (int iG = 0; iG < _groups->getNbElementGroups()+_groups->getNbGhostGroups(); iG++) {
    _elem2ColPos[iG].resize(_groups->getElementGroup(iG)->getNbElements(), std::make_pair(-1, -1));
  }
  for (size_t iCol = 0; iCol < _colPos2Elem.size(); iCol++) {
    for (size_t iLev = 0; iLev < _colPos2Elem[iCol].size(); iLev ++ ) {
      size_t gId = _colPos2Elem[iCol][iLev].first;
      size_t eId = _colPos2Elem[iCol][iLev].second;
      _elem2ColPos[gId][eId].first = iCol;
      _elem2ColPos[gId][eId].second = iLev;
    }
  }
  if (_groups2d){
    _buildVertexMap();
    _buildVertexMap2d();
    _computeInRadiusAndArea2d();
    _check2d3dParametricMatch();
  }
}

class fdNode{
  private:
    static double _epsilon; // tolerance for x,y,z comparison
    double _x, _y, _z;
    SPoint3 _elemCenter;
    size_t _iGroup,_iElem,_iNode,_vertexNum;
  public:
    fdNode(SPoint3 p, SPoint3 elemCenter, size_t iG, size_t iE, size_t iN) :
      _x(p.x()),_y(p.y()),_z(p.z()),_elemCenter(elemCenter),
      _iGroup(iG),_iElem(iE), _iNode(iN),_vertexNum(-1){};
    fdNode(MElement* e, size_t iG, size_t iE, size_t iN):
      _iGroup(iG), _iElem(iE),_iNode(iN)
    {
      MVertex* v = e->getVertex(iN);
      SPoint3 p = v->point();
      p.getPosition(&_x,&_y,&_z);
      _elemCenter = e->barycenter();
      _vertexNum = v->getNum();
    }
    fdNode() {_x = _y = _z = 0; _iGroup = _iElem = _iNode = _vertexNum = -1; }
    inline size_t groupId() const { return _iGroup;};
    inline size_t elemId() const { return _iElem;};
    inline size_t nodeId() const { return _iNode;};
    inline SPoint3 elemCenter() const { return _elemCenter;};
    inline double x() const { return _x;};
    inline double y() const { return _y;};
    inline double z() const { return _z;};
    inline double num() const { return _vertexNum;};
    inline static void setEpsilon(double eps) { fdNode::_epsilon = eps; };
    static double getEpsilon() { return _epsilon; };
};
double fdNode::_epsilon = 1e-10;

static bool fdNodeSmallerThan(fdNode a, fdNode b)
{
  if (a.x() + a.getEpsilon() < b.x()) return true;
  if (a.x() - a.getEpsilon() > b.x()) return false;
  if (a.y() + a.getEpsilon() < b.y()) return true;
  if (a.y() - a.getEpsilon() > b.y()) return false;
  if (a.z() + a.getEpsilon() < b.z()) return true;
  // test the element centers
  if (a.elemCenter().z() + a.getEpsilon() < b.elemCenter().z()) return true;
  return false;
}

void dgExtrusion::_buildVertexMap()
{
  _maxNbLayers = 10000;
  for (int iG = 0; iG < _groups->getNbElementGroups()+_groups->getNbGhostGroups(); iG++) {
    const dgGroupOfElements *group = _groups->getElementGroup(iG);
    std::vector<std::vector<size_t> > elemVect(group->getNbElements(), std::vector<size_t> (group->getNbNodes(), -1));
    _dofToVEdgeCG.push_back(elemVect);
    _dofToVEdgeDG.push_back(elemVect);
  }
  // build face vertex to 3d vertex map
  for (size_t iElemCol=0; iElemCol < _colPos2Elem.size(); iElemCol++ ) {
    std::list<fdNode> nodeList;
    // find all nodes in all elems in this column and sort them in x/y/z --> store to list as x,y,z with iG,iE,iNode
    for (size_t iElemLayer = 0; iElemLayer < _colPos2Elem[iElemCol].size(); iElemLayer++) {
      size_t iG = _colPos2Elem[iElemCol][iElemLayer].first;
      size_t iE = _colPos2Elem[iElemCol][iElemLayer].second;
      dgGroupOfElements *group = _groups->getElementGroup(iG);
      //elem->getNumVertices() should be groups->getNbNodes() but fdNode needs to be modified!
      for (int iNode = 0; iNode < group->getElement(iE)->getNumVertices(); iNode ++)
        nodeList.push_back(fdNode(group->getElement(iE) ,iG, iE, iNode));
    }
    nodeList.sort(fdNodeSmallerThan);

    double previousZ = nodeList.front().z() + 1.;
    for (std::list<fdNode>::iterator n = nodeList.begin(); n != nodeList.end(); ++n) {
      if (previousZ > n->z()) {
        _faceVertices.push_back((size_t)n->num());
        _nbCGLevelsInVertical.push_back(1);
        _nbDGLevelsInVertical.push_back(1);
      } else {
        _nbDGLevelsInVertical.back()++;
        if (previousZ != n->z())
          _nbCGLevelsInVertical.back()++;
      }
      previousZ = n->z();
      _dofToVEdgeCG[n->groupId()][n->elemId()][n->nodeId()] = hashVEdgeNode(_faceVertices.size() - 1, _nbCGLevelsInVertical.back() - 1);
      _dofToVEdgeDG[n->groupId()][n->elemId()][n->nodeId()] = hashVEdgeNode(_faceVertices.size() - 1, _nbDGLevelsInVertical.back() - 1);
    }
    if (iElemCol==_nbResidentCols-1)
      _nbResidentVerticals = _faceVertices.size();
  }
/*
  printf("nbCGLevelsInVert\n",getNbVerticals());
  for (int i = 0; i < getNbVerticals(); i++ )
    printf("%d\n",_nbCGLevelsInVertical[i]);
  printf("nbDGLevelsInVert\n",getNbVerticals());
  for (int i = 0; i < getNbVerticals(); i++ )
    printf("%d\n",_nbDGLevelsInVertical[i]);
  printf("\n_femNode2CGPoint\n",getNbVerticals());
  for (int iG = 0; iG < _groups->getNbElementGroups(); iG++) {
    for (int iE = 0; iE < _groups->getElementGroup(iG)->getNbElements(); iE++)
      for (int iN = 0; iN < _groups->getElementGroup(iG)->getElement(iE)->getNumVertices(); iN++) {
        int iV,iL;
        unhashVEdgeNode(_dofToVEdgeCG[iG][iE][iN],iV,iL);
        printf("%d %d %d -> %d\t-> ( %d %d )\n",iG,iE,iN,_dofToVEdgeCG[iG][iE][iN],iV,iL);
      }
  }
  printf("\n_femNode2DGPoint\n",getNbVerticals());
  for (int iG = 0; iG < _groups->getNbElementGroups(); iG++) {
    for (int iE = 0; iE < _groups->getElementGroup(iG)->getNbElements(); iE++)
      for (int iN = 0; iN < _groups->getElementGroup(iG)->getElement(iE)->getNumVertices(); iN++) {
        int iV,iL;
        unhashVEdgeNode(_dofToVEdgeDG[iG][iE][iN],iV,iL);
        printf("%d %d %d -> %d\t-> ( %d %d )\n",iG,iE,iN,_dofToVEdgeDG[iG][iE][iN],iV,iL);
      }
  }
  */
}

void dgExtrusion::_buildVertexMap2d()
{
  _maxNb2dElems = _maxNb2dNodes = 0;
  for (int iG = 0; iG < _groups2d->getNbElementGroups(); iG++) {
    _maxNb2dElems = std::max(_maxNb2dElems, _groups2d->getElementGroup(iG)->getNbElements());
    _maxNb2dNodes = std::max(_maxNb2dNodes, (size_t)_groups2d->getElementGroup(iG)->getNbNodes());
  }
  _verticalTo2DNode.resize(getNbVerticals(false),-1);
  size_t nbColumns = getNbColumns(false);
  for (size_t iCol = 0; iCol < nbColumns; iCol++ ) {
    size_t iGroup3d, iElem3d;
    size_t topLayer = getNbElemsInColumn(iCol)-1;
    mapColumnPosition2ElementID(iCol,topLayer,iGroup3d,iElem3d);
    MElement* prism = _groups->getElementGroup(iGroup3d)->getElement(iElem3d);
    SPoint3 center = prism->barycenter();
    center.setPosition(center.x(),center.y(),0.0); // dummy projection to 2d
    // find center in 2D mesh
    size_t iGroup2d = _col2Elem2d[iCol].first, iElem2d = _col2Elem2d[iCol].second;
    MElement *triangle = _groups2d->getElementGroup(iGroup2d)->getElement(iElem2d);
    for (int iNode3d = 0; iNode3d < prism->getNumVertices(); iNode3d++) {
      SPoint3 v3d = prism->getVertex(iNode3d)->point();
      v3d[2] = 0.; //dummy projection to 2d
      bool nodeFound = false;
      int iNode2d = 0;
      // find corresponding 2d node
      for (iNode2d = 0; iNode2d < triangle->getNumVertices(); iNode2d++) {
        SPoint3 v2d = triangle->getVertex(iNode2d)->point();
        v2d[2] = 0.; //dummy projection to 2d
        if ( v3d.distance( v2d ) < 1e-10) {
          nodeFound = true;
          break;
        }
      }
      if (!nodeFound)
        continue;
      size_t iVertical, iLevel;
      mapDofToVEdgeDG(iGroup3d,iElem3d,iNode3d,iVertical,iLevel);
      _verticalTo2DNode[iVertical] = _hash2dNode(iGroup2d,iElem2d,iNode2d);
    }
  }
}

void dgExtrusion::_computeInRadiusAndArea2d(){
  _inRadius.clear();
  _area2d.clear();
  _inRadius.resize(_groups2d->getNbElementGroups());
  _area2d.resize(_groups2d->getNbElementGroups());
  dataCacheMap cacheMap(dataCacheMap::NODE_MODE, _groups2d);
  dataCacheDouble &xyzCache = *cacheMap.get(_groups2d->getFunctionCoordinates());
  for (int iG = 0; iG < _groups2d->getNbElementGroups(); iG++) {
    dgGroupOfElements &group2d = *_groups2d->getElementGroup(iG);
    cacheMap.setGroup(&group2d);
    _inRadius[iG].resize(group2d.getNbElements());
    _area2d[iG].resize(group2d.getNbElements());
    for (size_t iElem2d = 0; iElem2d < group2d.getNbElements(); iElem2d++ ) {
      MElement &elem2d=*group2d.getElement(iElem2d);
      cacheMap.setElement(iElem2d);
      const fullMatrix<double> &xyz = xyzCache.get();
      std::vector<double> origX, origY, origZ;
      for (int iVertex = 0; iVertex < elem2d.getNumVertices(); iVertex++) {
        MVertex &vertex = *elem2d.getVertex(iVertex);
        origX.push_back(vertex.x());
        origY.push_back(vertex.y());
        origZ.push_back(vertex.z());
        vertex.setXYZ(xyz(iVertex, 0), xyz(iVertex, 1), xyz(iVertex, 2));    
      }
      _inRadius[iG][iElem2d] = elem2d.getInnerRadius();
      _area2d[iG][iElem2d] = elem2d.getVolume();
      for (int iVertex = 0; iVertex < elem2d.getNumVertices(); iVertex++) {
        MVertex &vertex = *elem2d.getVertex(iVertex);
        vertex.setXYZ(origX[iVertex], origY[iVertex], origZ[iVertex]);
      }
    }
  }
}

void dgExtrusion::mapDofToVEdgeCG(size_t iGroup, size_t iElement, size_t iNode, size_t& iVertical, size_t& iLevel) const
{
  size_t vEdgeHash = _dofToVEdgeCG[iGroup][iElement][iNode];
  unhashVEdgeNode(vEdgeHash, iVertical, iLevel);
  if (iVertical > getNbVerticals(false)) {
    Msg::Fatal("bad hash! %d %d %d -> %d -> ( %d %d )\n",
               iGroup,iElement,iNode,vEdgeHash,iVertical,iLevel);
  }
}

size_t dgExtrusion::getFirstVertex(size_t iVertical) const
{
  return _faceVertices[iVertical];
}

dgExtrusion::~dgExtrusion()
{
}

void dgExtrusion::mapColumnPosition2ElementID(size_t iColumn, size_t iLayer, size_t& iGroup, size_t& iElem) const
{
  iGroup = _colPos2Elem[iColumn][iLayer].first;
  iElem = _colPos2Elem[iColumn][iLayer].second;
}

void dgExtrusion::mapElementID2ColumnPosition(size_t iGroup, size_t iElem, size_t& iColumn, size_t& iLayer) const
{
  iColumn = _elem2ColPos[iGroup][iElem].first;
  iLayer = _elem2ColPos[iGroup][iElem].second;
}

void dgExtrusion::mapElementID2Element2dID(size_t iGroup, size_t iElem, size_t &iGroup2d, size_t &iElem2d) const
{
  iGroup2d = _col2Elem2d[_elem2ColPos[iGroup][iElem].first].first;
  iElem2d = _col2Elem2d[_elem2ColPos[iGroup][iElem].first].second;
}

size_t dgExtrusion::getNbElemsInColumn(size_t iCol) const
{
 return _colPos2Elem[iCol].size();
}

size_t dgExtrusion::getNbColumns(bool residentOnly) const
{
  if (residentOnly)
    return _nbResidentCols;
  return _colPos2Elem.size();
}

double dgExtrusion::innerRadius2d(size_t iGroup, size_t iElement) const
{
  size_t iGroup2d, iElem2d;
  mapElementID2Element2dID(iGroup, iElement, iGroup2d, iElem2d);
  return _inRadius[iGroup2d][iElem2d];
}

double dgExtrusion::area2d(size_t iGroup, size_t iElement) const
{
  size_t iGroup2d, iElem2d;
  mapElementID2Element2dID(iGroup, iElement, iGroup2d, iElem2d);
  return _area2d[iGroup2d][iElem2d];
}

