#include "dgSpaceTransform.h"
#include "dgCircleTransform.h"
#include "dgGroupOfElements.h"


#include <stdio.h>
// one D
void dgSpaceTransform1DCurvilinear::applyJacobian(const MElement &elem, double xyz[3], double uvw[3], double jac[3][3], double &detJ, int closureId) const
{
  jac[0][0] = detJ * (closureId == 1 ? -1 : 1);
  jac[1][1] = jac[2][2] = 1.;
  jac[0][1] = jac[0][2] = jac[1][0] = jac[1][2] = jac[2][0] = jac[2][1] = 0.0 ;
}


// stereographic coordinates with circle transform
dgSpaceTransformSpherical::dgSpaceTransformSpherical(double R, dgGroupCollection *groups)
{
  _R = R;
  _circleTransform = new dgCircleTransform(*groups);
}
// stereographic coordinates
dgSpaceTransformSpherical::dgSpaceTransformSpherical(double R)
{
  _R = R;
  _circleTransform = NULL;
      
}

double dgSpaceTransformSpherical::getRmz(double xyz[3]) const
{
  double R = getRadius();
  return 4 * R * (R + xyz[2]) / (4 * R * R + xyz[0] * xyz[0] + xyz[1] * xyz[1]);
}

void dgSpaceTransformSpherical::applyJacobian(const MElement &elem, double xyz[3], double uvw[3], double jac[3][3], double &detJ, int closureId) const
{  
  double rmz;
  if (_circleTransform){
    transformToCircle transformCoord(*_circleTransform);
    double xyzCircle[3];
    transformCoord.getCircleCoordinates(elem, uvw, closureId, xyz, xyzCircle);
    _circleTransform->applyJacobian(elem, xyz, uvw, jac, detJ, closureId);
    rmz = getRmz(xyzCircle);
    /*if( fabs((xyz[0]* xyz[0] +  xyz[1]* xyz[1]) - 4*getRadius()*getRadius()) < 5e11)
       printf("%e %e, %e %e \n", xyz[0], xyzCircle[0], xyz[1], xyzCircle[1]);*/
  }else{
    rmz = getRmz(xyz);
  }

  jac[0][0] *= rmz;
  jac[0][1] *= rmz;
  jac[1][0] *= rmz;
  jac[1][1] *= rmz;
  jac[2][0] *= rmz;
  jac[2][1] *= rmz;
  
  detJ = jac[0][0] * (jac[1][1] * jac[2][2] - jac[1][2] * jac[2][1])
    - jac[0][1] * (jac[1][0] * jac[2][2] - jac[1][2] * jac[2][0])
    + jac[0][2] * (jac[1][0] * jac[2][1] - jac[1][1] * jac[2][0]);
  
}

void dgSpaceTransformSpherical::rotateCartX(double x, double y, double z, double Alpha, double &xr, double &yr, double &zr){
  double rot[3][3] = {{1, 0, 0}, {0, cos(Alpha), -sin(Alpha)}, {0, sin(Alpha), cos(Alpha)}};
  xr = rot[0][0]*x + rot[0][1]*y + rot[0][2]*z;
  yr = rot[1][0]*x + rot[1][1]*y + rot[1][2]*z;
  zr = rot[2][0]*x + rot[2][1]*y + rot[2][2]*z;
}

void dgSpaceTransformSpherical::rotateCartXVec(double x, double y, double z, double Alpha, double v1, double v2, double v3, double &v1r, double &v2r, double &v3r){
  double rot[3][3] = {{1, 0, 0}, {0, cos(Alpha), -sin(Alpha)}, {0, sin(Alpha), cos(Alpha)}};
  v1r = rot[0][0]*v1 + rot[0][1]*v2 + rot[0][2]*v3;
  v2r = rot[1][0]*v1 + rot[1][1]*v2 + rot[1][2]*v3;
  v3r = rot[2][0]*v1 + rot[2][1]*v2 + rot[2][2]*v3;
}

void dgSpaceTransformSpherical::stereo2Cart(double xi, double beta, double Gamma, dataCacheMap *cacheMap, double &x, double &y, double &z){
  double fa = (_R+Gamma)/_R;
  x = fa * 4.0*_R*_R*xi/(4*_R*_R+xi*xi + beta*beta);
  y = fa * 4.0*_R*_R*beta/(4*_R*_R+xi*xi + beta*beta);
  double rev = 1;
  if (cacheMap->getGroupOfElements()->getPhysicalTag() == "volume_Bottom")
    rev = -1;
  z = fa * _R *(4.0*_R*_R-xi*xi-beta*beta)/(4.0*_R*_R+xi*xi + beta*beta) * rev;
}

void dgSpaceTransformSpherical::cart2LonLat(double x, double y, double z, double &lambd, double &phi, double &r){
  lambd = atan2(y,x)+M_PI;
  phi = asin(z / sqrt(x*x+y*y+z*z) );
  r = sqrt(x*x+y*y+z*z) - _R;
}

void dgSpaceTransformSpherical::lonLatToCartVec(double lon, double lat, double r, double v1, double v2, double v3, double &v1c, double &v2c, double &v3c){
  v1c = sin(lon) * v1 + cos(lon)*sin(lat) * v2 - cos(lon)*cos(lat) * v3;
  v2c = -cos(lon) * v1 + sin(lon)*sin(lat) * v2 - cos(lat)*sin(lon) * v3;
  v3c = cos(lat) * v2 + sin(lat) * v3;
}

void dgSpaceTransformSpherical::cartToStereoVec(double xst, double yst, double zst, double v1, double v2, double v3, dataCacheMap *cacheMap, double &v1s, double &v2s, double &v3s){
  double rev = 1;
  if (cacheMap->getGroupOfElements()->getPhysicalTag() == "volume_Bottom")
    rev = -1;
  v1s = (4*_R*_R - xst*xst + yst*yst)/(4*_R*_R + xst*xst + yst*yst) * v1  -(2*xst*yst)/(4*_R*_R + xst*xst + yst*yst)* v2  -(4*_R*rev*xst)/(4*_R*_R + xst*xst + yst*yst) * v3;
  v2s = -(2*xst*yst)/(4*_R*_R + xst*xst + yst*yst) * v1 + (4*_R*_R + xst*xst - yst*yst)/(4*_R*_R + xst*xst + yst*yst) * v2  -(4*_R*rev*yst)/(4*_R*_R + xst*xst + yst*yst) * v3;
  v3s = (4*_R*xst)/(4*_R*_R + xst*xst + yst*yst) * v1 +  (4*_R*yst)/(4*_R*_R + xst*xst + yst*yst) * v2 - (rev*(- 4*_R*_R + xst*xst + yst*yst))/(4*_R*_R + xst*xst + yst*yst) * v3;
}


