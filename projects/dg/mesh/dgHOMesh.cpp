#include "dgHOMesh.h"
#include "dgDofContainer.h"
#include "MElement.h"
#include "GModel.h"
#include "dgMPI.h"
#include "BasisFactory.h"
#include <limits>

class dgHOMeshElement {
  public:
  std::vector<int> hoNodes, p1Nodes;
  int partition;
  size_t ownedStart;
  dgHOMeshElement(): partition(0) {}
};

static const dgHOMeshElement *rereadNoOwnedVertices(std::map<std::vector<int>, dgHOMeshElement> &mesh, const std::vector<int> &p1Nodes, const nodalBasis &fs, const nodalBasis &fs1)
{
  std::vector<int> sortedP1Nodes = p1Nodes;
  std::sort(sortedP1Nodes.begin(), sortedP1Nodes.end());
  dgHOMeshElement &hoEl = mesh[sortedP1Nodes];
  if (hoEl.p1Nodes != p1Nodes)
    return NULL;
  if (fs.dimension != 0) {
    for (size_t iClosure = 0; iClosure < fs.closures.size(); ++iClosure) {
      const std::vector<int> cl1 = fs1.getClosure(iClosure);
      if (cl1.size() == 0)
        continue;
      const std::vector<int> cl = fs.getClosure(iClosure);
      std::vector<int> faceP1Nodes(cl1.size());
      for (size_t iV = 0; iV < cl1.size(); ++iV)
        faceP1Nodes[iV] = p1Nodes[cl1[iV]];
      const nodalBasis &faceFS1 = *BasisFactory::getNodalBasis(fs1.getClosureType(iClosure));
      const nodalBasis &faceFS = *BasisFactory::getNodalBasis(fs.getClosureType(iClosure));
      const dgHOMeshElement *face =rereadNoOwnedVertices(mesh, faceP1Nodes, faceFS, faceFS1);
      if (face) {
        for (size_t iV = 0; iV < face->hoNodes.size(); ++iV) {
          hoEl.hoNodes[cl[iV]] = face->hoNodes[iV];
        }
      }
    }
  }
  return &hoEl;
}

static dgHOMeshElement *addFsVertices(std::map<std::vector<int>, dgHOMeshElement> &mesh, const std::vector<int> &p1Nodes, const nodalBasis &fs, const nodalBasis &fs1, int &numVertices)
{
  std::vector<int> sortedP1Nodes = p1Nodes;
  std::sort(sortedP1Nodes.begin(), sortedP1Nodes.end());
  dgHOMeshElement &hoEl = mesh[sortedP1Nodes];
  if (!hoEl.hoNodes.empty()) {
    if (hoEl.p1Nodes == p1Nodes)
      return &hoEl;
    else
      return NULL;
  }
  hoEl.p1Nodes = p1Nodes;
  hoEl.hoNodes.resize(fs.points.size1(), -1);
  if (fs.dimension != 0) {
    for (size_t iClosure = 0; iClosure < fs.closures.size(); ++iClosure) {
      const std::vector<int> cl1 = fs1.getClosure(iClosure);
      if (cl1.size() == 0)
        continue;
      const std::vector<int> cl = fs.getClosure(iClosure);
      std::vector<int> faceP1Nodes(cl1.size());
      for (size_t iV = 0; iV < cl1.size(); ++iV)
        faceP1Nodes[iV] = p1Nodes[cl1[iV]];
      const nodalBasis &faceFS1 = *BasisFactory::getNodalBasis(fs1.getClosureType(iClosure));
      const nodalBasis &faceFS = *BasisFactory::getNodalBasis(fs.getClosureType(iClosure));
      const dgHOMeshElement *face = addFsVertices(mesh, faceP1Nodes, faceFS, faceFS1, numVertices);
      if (face)
        for (size_t iV = 0; iV < face->hoNodes.size(); ++iV)
          hoEl.hoNodes[cl[iV]] = face->hoNodes[iV];
    }
  }
  hoEl.ownedStart = hoEl.hoNodes.size();
  for (size_t iV = 0; iV < hoEl.hoNodes.size(); ++iV) {
    if (hoEl.hoNodes[iV] == -1) {
      hoEl.hoNodes[iV] = numVertices++;
      hoEl.ownedStart = std::min(iV, hoEl.ownedStart);
    }
  }
  return &hoEl;
}

static void setFsPartition(std::map<std::vector<int>, dgHOMeshElement> &mesh, const std::vector<int> &p1Nodes, const nodalBasis &fs1, int partition, std::vector<int> &partitionV)
{
  std::vector<int> sortedP1Nodes = p1Nodes;
  std::sort(sortedP1Nodes.begin(), sortedP1Nodes.end());
  std::map<std::vector<int>, dgHOMeshElement>::iterator it = mesh.find(sortedP1Nodes);
  if (it != mesh.end() && it->second.partition < partition) {
    dgHOMeshElement &el = it->second;
    el.partition = partition;
    for (size_t i = el.ownedStart; i < el.hoNodes.size(); ++i) {
      partitionV[el.hoNodes[i]] = partition;
    }
  }
  if (fs1.dimension != 0) {
    for (size_t iClosure = 0; iClosure < fs1.closures.size(); ++iClosure) {
      const std::vector<int> cl1 = fs1.getClosure(iClosure);
      if (cl1.size() == 0)
        continue;
      std::vector<int> faceP1Nodes(cl1.size());
      for (size_t iV = 0; iV < cl1.size(); ++iV)
        faceP1Nodes[iV] = p1Nodes[cl1[iV]];
      const nodalBasis &faceFS1 = *BasisFactory::getNodalBasis(fs1.getClosureType(iClosure));
      setFsPartition(mesh, faceP1Nodes, faceFS1, partition, partitionV);
    }
  }
}


void dgHOMesh::_comm(std::vector<double> &value, dgHOMesh::MPIOperation op, int nbFields)
{
  std::map<int, std::vector<double> > send, recv;
  std::map<int, std::vector<int> > &sendId = (op == SCATTER ? _scatterSendId : _scatterReceiveId);
  std::map<int, std::vector<int> > &recvId = (op == SCATTER ? _scatterReceiveId : _scatterSendId);
  for (std::map<int, std::vector<int> > ::iterator it = sendId.begin(); it != sendId.end(); ++it) {
    std::vector<double> &sendit = send[it->first];
    sendit.resize(it->second.size() * nbFields);
    for (size_t i = 0; i < it->second.size(); ++i) {
      for (int f = 0; f < nbFields; ++f) {
        sendit[i * nbFields + f] = value[it->second[i] * nbFields + f];
      }
    }
  }
  dgMPI::allAllV(send, recv, true);
  for (std::map<int, std::vector<int> > ::iterator it = recvId.begin(); it != recvId.end(); ++it) {
    std::vector<double> &recvit = recv[it->first];
    switch (op) {
      case SCATTER:
        for (size_t i = 0; i < it->second.size(); ++i) {
          for (int f = 0; f < nbFields; ++f) {
            value[it->second[i] * nbFields + f] = recvit[i * nbFields + f];
          }
        }
        break;
      case GATHER_SUM:
        for (size_t i = 0; i < it->second.size(); ++i) {
          for (int f = 0; f < nbFields; ++f) {
            value[it->second[i] * nbFields + f] += recvit[i * nbFields + f]; 
          }
        }
        break;
      case GATHER_MIN:
        for (size_t i = 0; i < it->second.size(); ++i) {
          for (int f = 0; f < nbFields; ++f) {
            value[it->second[i] * nbFields + f] = std::min(value[it->second[i] * nbFields + f], recvit[i * nbFields + f]);
          }
        }
        break;
      case GATHER_MAX:
        for (size_t i = 0; i < it->second.size(); ++i) {
          for (int f = 0; f < nbFields; ++f) {
            value[it->second[i] * nbFields + f] = std::max(value[it->second[i] * nbFields + f], recvit[i * nbFields + f]);
          }
        }
        break;
    }
  }
}

void dgHOMesh::cgOperation(const dgDofContainer *from, dgDofContainer *to, CGOperation op)
{
  int nbFields = from->getNbFields();
  const dgGroupCollection &groups = *from->getGroups();
  std::vector<double> value((nVertices() + nGhostVertices()) * nbFields, ((op == SUM) ? 0. : ((op == MIN) ? std::numeric_limits<double>::max() : -std::numeric_limits<double>::max())));
  for (int iGroup = 0; iGroup < groups.getNbElementGroups(); ++iGroup){
    const dgGroupOfElements &group = *groups.getElementGroup(iGroup);
    const fullMatrix<double> &proxy = from->getGroupProxy(iGroup);
    for (size_t iElement = 0; iElement < group.getNbElements(); ++iElement) {
      const std::vector<int> vert = vertices(iGroup, iElement);
      switch(op) {
        case (SUM) :
          for (size_t iV = 0; iV < vert.size(); ++iV) {
            for (int f = 0; f < nbFields; ++f){
              value[vert[iV] * nbFields + f ] += proxy(iV, iElement * nbFields + f);
            } 
          }
          break;
        case (MIN) :
          for (size_t iV = 0; iV < vert.size(); ++iV) {
            for (int f = 0; f < nbFields; ++f){
              value[vert[iV] * nbFields + f] = std::min(proxy(iV, iElement * nbFields + f), value[vert[iV] * nbFields + f]);
            }
          }
          break;
        case (MAX) :
          for (size_t iV = 0; iV < vert.size(); ++iV) {
            for (int f = 0; f < nbFields; ++f){
              value[vert[iV] * nbFields + f] = std::max(proxy(iV, iElement * nbFields + f), value[vert[iV] * nbFields + f]);
            }
          }
          break;
      }
    }
  }
  _comm(value, op==SUM ? GATHER_SUM : op == MAX ? GATHER_MAX : GATHER_MIN, nbFields);
  _comm(value, SCATTER, nbFields);
  for (int iGroup = 0; iGroup < groups.getNbElementGroups(); ++iGroup){
    const dgGroupOfElements &group = *groups.getElementGroup(iGroup);
    fullMatrix<double> &proxy = to->getGroupProxy(iGroup);
    for (size_t iElement = 0; iElement < group.getNbElements(); ++iElement) {
      const std::vector<int> vert = vertices(iGroup, iElement);
      for (size_t iV = 0; iV < vert.size(); ++iV) {
        for (int f = 0; f < nbFields; ++f){
          proxy(iV, iElement * nbFields + f) = value[vert[iV] * nbFields + f];
        }
      }
    }
  }
}


dgHOMesh::dgHOMesh(const dgGroupCollection &groups)
{
  _groups = &groups;
  std::map<std::vector<int>, dgHOMeshElement > mesh;
  std::vector<std::vector<dgHOMeshElement*> > groupMap;
  //create the local HO nodes
  groupMap.resize(groups.getNbElementGroups());
  _numVertices = 0;
  for (int iGroup = 0; iGroup < groups.getNbElementGroups(); iGroup++) {
    const dgGroupOfElements &group = *groups.getElementGroup(iGroup);
    groupMap[iGroup].resize(group.getNbElements());
    const nodalBasis &fs = group.getFunctionSpace();
    const nodalBasis &fs1 = *group.getElement(0)->getFunctionSpace(1);
    for (size_t iElement = 0; iElement < group.getNbElements(); ++iElement) {
      MElement *e = group.getElement(iElement);
      std::vector<int> p1Nodes(e->getNumPrimaryVertices());
      for (size_t iV = 0; iV < p1Nodes.size(); ++iV){
        p1Nodes[iV] = groups._mesh->periodicMap(e->getVertex(iV)->getNum());
      }
      groupMap[iGroup][iElement] = addFsVertices(mesh, p1Nodes, fs, fs1, _numVertices);
    }
  }
  std::vector<int> vertexPartition(_numVertices, Msg::GetCommRank());

  //assign a partition to each hoElement and each vertex
  GModel *gm = groups.getModel();
  std::vector<GEntity*> entities;
  gm->getEntities(entities);
  std::map<int, int> ghostVertices;
  for (size_t iEntity = 0; iEntity < entities.size(); ++ iEntity) {
    GEntity *entity = entities[iEntity];
    if (entity->dim() != gm->getDim())
      continue;
    for (size_t iElement = 0; iElement < entity->getNumMeshElements(); ++ iElement) {
      MElement *element = entity->getMeshElement(iElement);
      std::vector<int> p1Vertices(element->getNumPrimaryVertices());
      for (size_t i = 0; i < p1Vertices.size(); ++i)
        p1Vertices[i] = groups._mesh->periodicMap(element->getVertex(i)->getNum());
      setFsPartition(mesh, p1Vertices, *element->getFunctionSpace(1), element->getPartition() - 1, vertexPartition);
    }
  }
  //renumber local vertices to put the ghosts at the end (like petsc vector)
  _numLocalVertices = 0;
  int nGhost = 0;
  std::vector<int> globalId(_numVertices);
  for (int i = 0; i < _numVertices; ++i) {
    globalId[i] = (vertexPartition[i] == Msg::GetCommRank()) ? _numLocalVertices ++ : - (++ nGhost);
  }
  dgMPI::sumPrev(_numLocalVertices, _startId);
  for(std::map<std::vector<int>, dgHOMeshElement>::iterator it = mesh.begin(); it != mesh.end(); ++it) {
    dgHOMeshElement &el = it->second;
    for (size_t i = 0; i < el.hoNodes.size(); ++i)
      el.hoNodes[i] = globalId[el.hoNodes[i]] + _startId;
  }
  //request globalId of ghost vertices
  std::map<int, std::vector<int> > ask, asked;
  for (std::map<std::vector<int>, dgHOMeshElement > ::iterator it = mesh.begin(); it != mesh.end(); ++it) {
    const dgHOMeshElement &el = it->second;
    if (el.partition != Msg::GetCommRank() && el.ownedStart < el.hoNodes.size()) {
      std::vector<int> &a = ask[el.partition];
      a.push_back(el.p1Nodes.size());
      for (size_t i = 0; i < el.p1Nodes.size(); ++i)
        a.push_back(el.p1Nodes[i]);
      std::sort(&a[a.size() - el.p1Nodes.size()], &a.back() + 1);
    }
  }
  dgMPI::allAllV(ask, asked, true);
  std::map<int, std::vector<int> > reply, replied;
  for(std::map<int, std::vector<int> >::iterator it = asked.begin(); it != asked.end(); ++it) {
    std::vector<int> &askedi = it->second;
    std::vector<int> &replyi = reply[it->first];
    size_t c = 0;
    while (c < askedi.size()) {
      std::vector<int> elem(askedi[c++]);
      for (size_t j = 0; j < elem.size(); ++j)
        elem[j] = askedi[c++];
      const dgHOMeshElement &el = mesh[elem];
      for (size_t k =0; k < el.p1Nodes.size(); ++k)
        replyi.push_back(el.p1Nodes[k]);
      for (size_t k = el.ownedStart; k < el.hoNodes.size(); ++k)
        replyi.push_back(el.hoNodes[k]);
    }
  }
  dgMPI::allAllV(reply, replied, true);
  std::map<int, std::vector<int> >::iterator itAsk = ask.begin();
  std::map<int, int> ghostPartitionMap;
  for(std::map<int, std::vector<int> >::iterator it = replied.begin(); it != replied.end(); ++it) {
    std::vector<int> &aski = itAsk->second;
    std::vector<int> &repliedi = it->second;
    size_t c = 0, d = 0;
    while (c < aski.size()) {
      std::vector<int> elem(aski[c++]);
      for (size_t j = 0; j < elem.size(); ++j)
        elem[j] = aski[c++];
      dgHOMeshElement &el = mesh[elem];
      for (size_t k = 0; k < el.p1Nodes.size(); ++k)
        el.p1Nodes[k] = repliedi[d++];
      for (size_t k = el.ownedStart; k < el.hoNodes.size(); ++k) {
        el.hoNodes[k] = repliedi[d++];
        ghostPartitionMap[el.hoNodes[k]] = it->first;
      }
    }
    itAsk++;
  }
  for (int iGroup = 0; iGroup < groups.getNbElementGroups(); iGroup++) {
    const dgGroupOfElements &group = *groups.getElementGroup(iGroup);
    const nodalBasis &fs = group.getFunctionSpace();
    const nodalBasis &fs1 = *group.getElement(0)->getFunctionSpace(1);
    for (size_t iElement = 0; iElement < group.getNbElements(); ++iElement)
      rereadNoOwnedVertices(mesh, groupMap[iGroup][iElement]->p1Nodes, fs, fs1);
  }
  //create scatterSendId and scatterReceiveId Maps
  for (std::map<int, int>::const_iterator it = ghostPartitionMap.begin(); it != ghostPartitionMap.end(); ++it)
    _scatterReceiveId[it->second].push_back(it->first);
  dgMPI::allAllV(_scatterReceiveId, _scatterSendId, true);

  //create the local numbering
  std::map<int, int> ghostLocalId;
  _ghostGlobalId.reserve(ghostPartitionMap.size());
  _ghostPartition.reserve(ghostPartitionMap.size());
  for( std::map<std::vector<int>, dgHOMeshElement > ::iterator it = mesh.begin(); it != mesh.end(); ++it) {
    dgHOMeshElement &el = it->second;
    std::vector<int> &elNew = _sortedP1NodesToAllNodes[it->first];
    elNew.reserve(it->second.hoNodes.size());
    for (size_t k = 0; k <el.hoNodes.size(); ++k) {
      if (el.hoNodes[k] < _startId || el.hoNodes[k] >= _startId + _numLocalVertices) {
        if (ghostLocalId.find(el.hoNodes[k]) == ghostLocalId.end()){
          ghostLocalId[el.hoNodes[k]] = _ghostGlobalId.size() + _numLocalVertices;
          _ghostGlobalId.push_back(el.hoNodes[k]);
          _ghostPartition.push_back(ghostPartitionMap[el.hoNodes[k]]);
        }
        elNew.push_back(ghostLocalId[el.hoNodes[k]]);
      }
      else {
        elNew.push_back(el.hoNodes[k] - _startId);
      }
    }
  }
  for(std::map<int, std::vector<int> >::iterator  it = _scatterReceiveId.begin(); it  != _scatterReceiveId.end(); ++it) {
    for(size_t i = 0; i < it->second.size(); ++i) {
      it->second[i] = ghostLocalId[it->second[i]];
    }
  }
  for(std::map<int, std::vector<int> >::iterator  it = _scatterSendId.begin(); it  != _scatterSendId.end(); ++it) {
    for(size_t i = 0; i < it->second.size(); ++i) {
      it->second[i] -= _startId;
    }
  }
  _groupIdElIdToAllNodes.resize(groupMap.size());
  for(int i = 0; i < groups.getNbElementGroups(); ++i) {
    const dgGroupOfElements &group = *groups.getElementGroup(i);
    _groupIdElIdToAllNodes[i].resize(group.getNbElements());
    for(size_t j = 0; j < group.getNbElements(); ++j) {
      _groupIdElIdToAllNodes[i][j] = &vertices(group.getElement(j));
    }
  }
}

int dgHOMesh::vertexPartition(int iVertex) const
{
  return iVertex >= _numLocalVertices ? _ghostPartition[iVertex - _numLocalVertices] : Msg::GetCommRank();
}

const std::vector<int> &dgHOMesh::vertices(MElement *e) const
{
  std::vector<int> p1Nodes(e->getNumPrimaryVertices());
  for (size_t iV = 0; iV < p1Nodes.size(); ++iV) {
    p1Nodes[iV] = _groups->_mesh->periodicMap(e->getVertex(iV)->getNum());
  }
  static std::vector<int> empty;
  std::sort(p1Nodes.begin(), p1Nodes.end());
  std::map<std::vector<int>, std::vector<int> >::const_iterator it = _sortedP1NodesToAllNodes.find(p1Nodes);
  if(it == _sortedP1NodesToAllNodes.end())
    return empty;
    //Msg::Fatal("element not in high order mesh");
  return it->second;
}
