#ifndef _DH_HO_MESH_H_
#define _DH_HO_MESH_H_
#include <map>
#include <vector>
#include "dgGroupOfElements.h"
#include "dgMesh.h"

class dgGroupCollection;
class dgDofContainer;
class MElement;

class dgHOMesh {
  std::map<std::vector<int>, std::vector<int> > _sortedP1NodesToAllNodes;
  std::vector<std::vector<const std::vector<int> * > > _groupIdElIdToAllNodes;
  std::vector<int> _ghostGlobalId, _ghostPartition;
  int _numVertices, _numLocalVertices, _startId;
  const dgGroupCollection *_groups;
  std::map<int, std::vector<int> > _scatterSendId, _scatterReceiveId;
  typedef enum {SCATTER, GATHER_MIN, GATHER_MAX, GATHER_SUM} MPIOperation;
  void _comm(std::vector<double> &localValues, MPIOperation op, int nbFields);
 public :
  dgHOMesh(const dgGroupCollection &groups);
  inline int nVertices() const { return _numLocalVertices; }
  const std::vector<int> &vertices(int iGroup, int iElement) const {return *_groupIdElIdToAllNodes[iGroup][iElement];}
  const std::vector<int> &vertices(MElement *e) const;
  int vertexPartition(int localId) const;
  inline int vertexGlobalId(int localId) const {return localId < _numLocalVertices ? localId + _startId :_ghostGlobalId[localId - _numLocalVertices];}
  inline int nGhostVertices() const {return _ghostPartition.size();}
  inline const std::map<int, std::vector<int> > &scatterSendId() const {return _scatterSendId;}
  inline const std::map<int, std::vector<int> > &scatterReceiveId() const {return _scatterReceiveId;}
  typedef enum {SUM, MIN, MAX} CGOperation;
  void cgOperation(const dgDofContainer *from, dgDofContainer *to, CGOperation op);
};
#endif
