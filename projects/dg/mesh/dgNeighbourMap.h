#include<vector>
class dgGroupCollection;
class MElement;
class dgNeighbourMap {
   std::vector<std::vector<int> > _faces;
   std::vector<int> _nbFaces;
   bool _isValid(int iGroup, int iElem, int iNeigh) const;
   const dgGroupCollection * _groups;
   public:
   dgNeighbourMap(const dgGroupCollection &groups);
   void getFace(int iGroup, int iElem, int iNeigh, int &jGroup, int &jFace, int &jConnection) const;
   int getNbFaces(int iGroup) const {return _nbFaces[iGroup];}
   void getNeighbour(int iGroup, int iElem, int iNeigh, int &jGroup, int &jElem) const;
   std::vector<std::pair<int, int> > getNeighbours(int iGroup, int iElement, int iNeigh) const;
   //simple temporary interface so that christopher can work during my hollydays :-)
   MElement *getNeighbour(const MElement *e, int iNeigh) const;
};
