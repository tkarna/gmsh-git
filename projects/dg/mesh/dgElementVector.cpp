#include "dgElementVector.h"
#include "MElement.h"

dgElementVector::dgElementVector(const std::vector<MElement*> &elements, const std::string physicalTag, bool sequentialLoad)
{
  _elements = elements;
  _physicalTag = physicalTag;
  _sequentialLoad = sequentialLoad;
  if(size() == 0) {
    Msg::Fatal("cannot create a vector of 0 elements");
  }
  _functionSpace = _elements[0]->getFunctionSpace();
  int type = _elements[0]->getTypeForMSH();
  for (size_t i = 0; i < elements.size(); ++ i) {
    if (_elements[i]->getTypeForMSH() != type) {
      Msg::Fatal("group composed of element of different type");
    }
  }
  _coordinates.resize(_functionSpace->points.size1(), 3, elements.size(), false);
  fullMatrix<double> ecoord;
  for (size_t i = 0; i < _elements.size(); ++i) {
    _coordinates.getBlockProxy(i, ecoord);
    for (int j = 0; j < ecoord.size1(); ++j) {
      MVertex *v = _elements[i]->getVertex(j);
      ecoord(j, 0) = v->x();
      ecoord(j, 1) = v->y();
      ecoord(j, 2) = v->z();
    }
  }
}

int dgElementVector::originalPartition() const
{
  return _sequentialLoad ? 0 : _elements[0]->getPartition() - 1;
}

