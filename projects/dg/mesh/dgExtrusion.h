#ifndef _DG_EXTRUSION_H_
#define _DG_EXTRUSION_H_

#include <vector>
#include <string>

class dgGroupCollection;
class dgGroupOfFaces;

/**Stores information about vertical column stucture.
 * Each 3d element belongs to a certain column of stacked elements.
 * */
class dgExtrusion {
  private:
    dgGroupCollection *_groups, *_groups2d;
    // stores the elements in each vertical: verticalColumnInfo[iCol][iLayer] = pair<iGroup,iElem>
    std::vector<std::pair<size_t,size_t> > _col2Elem2d;
    std::vector< std::vector< std::pair<size_t,size_t> > > _colPos2Elem;
    std::vector< std::vector< std::pair<size_t,size_t> > > _elem2ColPos;
    std::vector<std::vector<double> > _inRadius, _area2d;
    std::vector<dgGroupOfFaces*> _topFaceGroups;
    // vertex num of first node in each vertical
    std::vector<size_t> _faceVertices;
    // maps extruded 3D nodes (iG,iE,iN) to 1D vertical arrays (iVertical, iLevel)
    std::vector<std::vector<std::vector<size_t> > > _dofToVEdgeCG, _dofToVEdgeDG;
    // nb of CG and DG points in each
    std::vector<size_t> _nbCGLevelsInVertical, _nbDGLevelsInVertical;
    // maps each vertical to a (hashed) node in 2d mesh
    std::vector<size_t> _verticalTo2DNode;
    // generates unique int for FD nodes
    size_t _maxNbLayers;
    size_t _nbResidentCols;
    size_t _nbResidentVerticals;
    inline size_t hashVEdgeNode(size_t iVertical, size_t iLevel) const {
      return iVertical*_maxNbLayers + iLevel;
    };
    inline void unhashVEdgeNode(size_t iFD, size_t &iVertical, size_t &iLevel) const {
      iVertical = iFD / _maxNbLayers;
      iLevel = iFD - iVertical*_maxNbLayers;
    };
    size_t _maxNb2dElems,_maxNb2dNodes;
    inline size_t _hash2dNode(size_t iGroup2d, size_t iElem2d, size_t iNode2d) {
      return _maxNb2dNodes*_maxNb2dElems*iGroup2d + _maxNb2dElems*iNode2d + iElem2d;
    };
    inline void _unhash2dNode(size_t iHash, size_t &iGroup2d, size_t &iElem2d, size_t &iNode2d) const {
      iGroup2d = iHash / (_maxNb2dNodes*_maxNb2dElems);
      size_t reminder = iHash - iGroup2d *_maxNb2dNodes*_maxNb2dElems;
      iNode2d = reminder / _maxNb2dElems;
      iElem2d = reminder - iNode2d * _maxNb2dElems;
    };

    void _buildVertexMap();
    void _buildVertexMap2d();
    void _computeInRadiusAndArea2d();
    std::vector<std::string> _topLevelTags;
    //check that the asumbption we make when evaluating a 2d dof in 3d are correct
    void _check2d3dParametricMatch() const;

  public:
    /** Builds vertical columns of elements for a 3D mesh.
     *  Element columns are built recursively, starting from face(s) identified by the physical tag(s).
     *  Elements adjacent to these faces form the first layer (iLayer=0).
     *  Also builds vertical arrays of nodes. For clarity term "column" corresponds to
     *  element columns while "vertical" is used for node columns.*/
    dgExtrusion(dgGroupCollection* groups, dgGroupCollection* groups2d, std::vector<std::string> topLevelTags);
    ~dgExtrusion();
    /** Returns (iGroup, iElem) indices of an element on given layer in vertical iColumn. */
    void mapColumnPosition2ElementID(size_t iColumn, size_t iLayer, size_t &iGroup, size_t &iElem) const; 
    /** Returns (iColumn, iLayer) indices of an element (iGroup, iElem). */
    void mapElementID2ColumnPosition(size_t iGroup, size_t iElem, size_t &iColumn, size_t &iLayer) const;
    /** Returns (iGroup2d, iEleme2d) indices of the 2d mesh element which is base of the extruded element (iGroup, iElem) */
    void mapElementID2Element2dID(size_t iGroup, size_t iElem, size_t &iGroup2d, size_t &iElem2d) const;
    /** Returns 3d dgGroupCollection */
    inline dgGroupCollection& getGroups3d() const {return *_groups;};
    /** Returns 2d dgGroupCollection */
    inline dgGroupCollection& getGroups2d() const {return *_groups2d;};
    /** Returns number of vertical element columns.*/
    size_t getNbColumns(bool residentOnly=true) const;
    /** Returns number of elements in given column.*/
    size_t getNbElemsInColumn(size_t iCol) const;
    /** Returns number of face groups that form the */
    size_t getNbTopFaceGroups() const {return _topFaceGroups.size();};
    /** Returns a given faceGroup.*/
    dgGroupOfFaces* getTopFaceGroup(size_t iFG) const {return _topFaceGroups[iFG];};
    /** Return a number of vertical node arrays. */
    inline size_t getNbVerticals(bool residentOnly=true) const { 
      if (residentOnly)
        return _nbResidentVerticals;
      return _faceVertices.size();
    };
    /** Return a number of nodes in each vertical edge. */
    inline size_t getNbDGPointsInVertical(size_t iVertical) const { return _nbDGLevelsInVertical[iVertical];};
    /** Return a number of distinct nodes in each vertical edge.
     * This ignores the discontinuities in DG */
    inline size_t getNbCGPointsInVertical(size_t iVertical) const { return _nbCGLevelsInVertical[iVertical];};
    /** Return max number of DG nodes in any vertical edge **/
    size_t getMaxNbDGPoints() const {
      size_t maxLev = 0;
      for (size_t i = 0; i < getNbVerticals(false); i++)
        maxLev = std::max(maxLev,getNbDGPointsInVertical(i));
      return maxLev;
    };
    /** Return max number of CG nodes in any vertical edge **/
    size_t getMaxNbCGPoints() const {
      size_t maxLev = 0;
      for (size_t i = 0; i < getNbVerticals(false); i++)
        maxLev = std::max(maxLev,getNbCGPointsInVertical(i));
      return maxLev;
    };
    /** Find 2d node for given 1D vertical.
     * Given node iVertical returns (iGroup2d,iElem2d,iNode2d) corresponding to the 2d element group. */
    inline void mapVEdgeTo2dNode(size_t iVertical, size_t &iGroup2d, size_t &iElem2d, size_t &iNode2d) const {_unhash2dNode(_verticalTo2DNode[iVertical],iGroup2d,iElem2d,iNode2d); };
    /** Returns vertex tag (num) of the first vertex in a vertical.*/
    size_t getFirstVertex(size_t iVertical) const;
    /** map 3D node (iG,iE,iN) to vertical edge node (iV,iL) */
    inline void mapDofToVEdgeDG(size_t iGroup,size_t iElement,size_t iNode,size_t &iVertical,size_t &iLevel) const { unhashVEdgeNode( _dofToVEdgeDG[iGroup][iElement][iNode] , iVertical, iLevel); };
    /** map 3D node (iG,iE,iN) to vertical edge node (iV,iL) ignoring discontinuity */
    void mapDofToVEdgeCG(size_t iGroup,size_t iElement,size_t iNode,size_t &iVertical,size_t &iLevel) const;
    /** Returns the inner radius of the 2d base of the extrusion */
    double innerRadius2d(size_t iGroup, size_t iElement) const;
    double area2d(size_t iGroup, size_t iElement) const;
    /** map 3D node to 2D node  (DG) **/
    inline void map3d2d(size_t iGroup3, size_t iElement3, size_t iNode3, size_t &iGroup2, size_t &iElement2, size_t &iNode2) const
    {
      size_t iVertical, iLevel;
      mapDofToVEdgeDG(iGroup3, iElement3, iNode3, iVertical, iLevel);
      mapVEdgeTo2dNode(iVertical, iGroup2, iElement2, iNode2);
    }
    /** map 3D element to 2D element  (DG) **/
    inline void map3d2d(size_t iGroup3, size_t iElement3, size_t &iGroup2, size_t &iElement2) const
    {
      size_t iColumn = _elem2ColPos[iGroup3][iElement3].first;
      iGroup2 = _col2Elem2d[iColumn].first;
      iElement2 = _col2Elem2d[iColumn].second;
    }
};
#endif
