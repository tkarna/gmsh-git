#include "dgCircleTransform.h"
#include <math.h>
#include "dgGroupOfElements.h"
#include "dgMesh.h"
#include "MElement.h"
#include "Numeric.h"
#include "dgMPI.h"
#include "BasisFactory.h"


static double _hDist(const MVertex *v1, const MVertex *v2){
  return sqrt((v1->x()-v2->x())*(v1->x()-v2->x())+(v1->y()-v2->y())*(v1->y()-v2->y()));
}

static void getParametricCoordinatesInReference(const MElement &elem, const std::vector<int> &closureSeam, double uvw[3], double A[2], double V[4][2]){
  const nodalBasis &fsElem1 = *elem.getFunctionSpace(1);
  const fullMatrix<double> &p = fsElem1.points;
  double f[8];
  fsElem1.f(uvw[0], uvw[1], uvw[2], f);
  // U: Parametric coordinates in reference element 
  // (defined with the closure of the seam)
  double U[3] = {0., 0., 0.};
  int hIdx[4] = {0,0,0,0};
  for (int j = 0; j < p.size2(); ++j) {
    U[j] = 0;
    for (int k = 0; k < p.size1(); ++k) {
      U[j] += p(k, j) * f[closureSeam[k]];
    }
  }  
  if (elem.getType()==TYPE_TRI || elem.getType()==TYPE_PRI) {
    A[0] = 1 - U[0] - U[1];
    A[1] = U[0];
    hIdx[0] = 0; hIdx[1] = 1; hIdx[2] = 2; hIdx[3] = 2;
  }
  else if (elem.getType()==TYPE_QUA) {
    A[0] = U[0];
    A[1] = U[1];
    hIdx[0] = 0; hIdx[1] = 1; hIdx[2] = 2; hIdx[3] = 3;
  } else if (elem.getType()==TYPE_HEX){
    // We choose the face which is horizontal
    if (_hDist(elem.getVertex(closureSeam[1]), elem.getVertex(closureSeam[0])) < _hDist(elem.getVertex(closureSeam[4]), elem.getVertex(closureSeam[0]))){
      A[0] = U[1];
      A[1] = U[2];
      hIdx[0] = 0; hIdx[1] = 3; hIdx[2] = 7; hIdx[3] = 4;
    }
    else {
      printf("v1 %.15e %.15e %.15e\nv2 %.15e %.15e %.15e\n",elem.getVertex(closureSeam[1])->x(),elem.getVertex(closureSeam[1])->y(),elem.getVertex(closureSeam[1])->z(),
	     elem.getVertex(closureSeam[0])->x(),elem.getVertex(closureSeam[0])->y(),elem.getVertex(closureSeam[0])->z());
      A[0] = -U[0];
      A[1] = U[2];
      hIdx[0] = 1; hIdx[1] = 0; hIdx[2] = 4; hIdx[3] = 5;
    }
  } else Msg::Fatal("Element not supported %i\n",elem.getType());  
  for (int i=0; i<4; i++){
    V[i][0] = elem.getVertex(closureSeam[hIdx[i]])->x();
    V[i][1] = elem.getVertex(closureSeam[hIdx[i]])->y();
  }
}


static void getParametricCoordinatesDerivInReference(const MElement &elem, const std::vector<int> &closureSeam, double uvw[3], fullMatrix<double> &dAdU){
  switch (elem.getType()) {
  case TYPE_TRI:
  case TYPE_PRI:
    dAdU(0, 0) = -1;
    dAdU(0, 1) = -1;
    dAdU(1, 0) = 1;
    dAdU(1, 1) = 0;
    break;
  case TYPE_QUA:
    dAdU(0, 0) = 1;
    dAdU(1, 1) = 1;
    break;
  case TYPE_HEX:
    // We choose the face which is horizontal
    if (_hDist(elem.getVertex(closureSeam[1]), elem.getVertex(closureSeam[0])) < _hDist(elem.getVertex(closureSeam[4]), elem.getVertex(closureSeam[0]))){
      dAdU(0, 1) = 1;  
      dAdU(1, 2) = 1;
    }
    else {
      dAdU(0, 0) = -1;
      dAdU(1, 2) = 1;
    }
    break;
  default: Msg::Fatal("Element not supported %i\n",elem.getType());
  }
}

inline static double _C1(double V[4][2], int j, double s)
{
  return V[1][j] + s * (V[2][j] - V[1][j]);
}
inline static double _C2(double V[4][2], int j, double s)
{
  return V[2][j] + s * (V[0][j] - V[2][j]);
}
inline static double _C0(double V[4][2], int j, double s)
{
  double R = hypot(V[0][0], V[0][1]);
  double u[2] = {
    V[0][0] + s * (V[1][0] - V[0][0]),
    V[0][1] + s * (V[1][1] - V[0][1])
  };
  double normU = hypot(u[0], u[1]);
  return u[j] * R / normU;
}
inline static double _dC1ds(double V[4][2], int j, double s)
{
  return V[2][j] - V[1][j];
}
inline static double _dC2ds(double V[4][2], int j, double s)
{
  return V[0][j] - V[2][j];
}
inline static double _dC0ds(double V[4][2], int j, double s)
{
  double R = hypot(V[0][0], V[0][1]);
  double u[2] = {
    V[0][0] + s * (V[1][0] - V[0][0]),
    V[0][1] + s * (V[1][1] - V[0][1])
  };
  double normU = hypot(u[0], u[1]);
  double uHat[2] = { u[0] / normU, u[1] / normU}; 
  return R / normU * (V[1][j] - V[0][j] - uHat[j] * ((V[1][0]-V[0][0]) * uHat[0] + (V[1][1]-V[0][1]) * uHat[1]));
}


static void transformCoordinates(const MElement &elem, double A[2], double V[4][2], double *xyz){
  double R = hypot(V[0][0], V[0][1]);
  double xi = A[0];
  double eta = A[1];

  switch (elem.getType()) {
  case TYPE_TRI:
  case TYPE_PRI: {
    for (int j = 0; j < 2; j++) {
      xyz[j] = (1 - xi - eta) * V[2][j] + eta * _C0(V, j, 1 - xi) + xi * _C0(V, j, eta);
    }
  }
    break;
  case TYPE_QUA:
  case TYPE_HEX: {
    double u[2] = {
      ((1 + xi) * V[1][0] + (1 - xi) * V[0][0]) / 2, 
      ((1 + xi) * V[1][1] + (1 - xi) * V[0][1]) / 2 
    };
    double normU = hypot(u[0], u[1]);
    double uHat[2] = {u[0] / normU, u[1] / normU};
    double E[2] = {
      ((1 + xi) * V[2][0] + (1 - xi) * V[3][0]) / 2, 
      ((1 + xi) * V[2][1] + (1 - xi) * V[3][1]) / 2 
    };
    xyz[0] = (1 - eta) / 2 * R * uHat[0] + (1 + eta) / 2 * E[0];
    xyz[1] = (1 - eta) / 2 * R * uHat[1] + (1 + eta) / 2 * E[1];
  }
    break;
  default:
    Msg::Fatal("Element not supported %i\n",elem.getType());
  }
}

static void transformdXdXi(const MElement &elem, double A[2], double V[4][2], double dXdXi[2][2]){
  double R = hypot(V[0][0], V[0][1]);
  double xi = A[0];
  double eta = A[1];

  switch (elem.getType()) {
  case TYPE_TRI:
  case TYPE_PRI: {
    for (int j = 0; j < 2; j++) {


      dXdXi[j][0] = - V[2][j] - eta * _dC0ds(V, j, 1 - xi) + _C0(V, j, eta);
      dXdXi[j][1] = - V[2][j] + _C0(V, j, 1 - xi) + xi * _dC0ds(V, j, eta) ;
    }
  }
    break;
  case TYPE_QUA: 
  case TYPE_HEX: {
    double u[2] = {
      ((1 + xi) * V[1][0] + (1 - xi) * V[0][0]) / 2, 
      ((1 + xi) * V[1][1] + (1 - xi) * V[0][1]) / 2 
    };
    double normU = hypot(u[0], u[1]);
    double uHat[2] = {u[0] / normU, u[1] / normU};
    for (int j = 0; j < 2; j++){
      dXdXi[j][0] = (1 - eta) / 2 * R / normU * ((V[1][j] - V[0][j]) / 2 - uHat[j] * ((V[1][0] - V[0][0]) / 2 * uHat[0] + (V[1][1] - V[0][1]) / 2 * uHat[1]))
	+ (1 + eta) / 2 * (V[2][j] - V[3][j]) / 2;
      dXdXi[j][1] = - uHat[j] * R / 2 + ((1 + xi) * V[2][j] + (1 - xi) * V[3][j]) / 4;
    }
  } 
  }
}



dgCircleTransform::dgCircleTransform (dgGroupCollection &groups)
{
  dgMesh &mesh = *groups._mesh;
  for (int i=0; i< mesh.nInterfaceVector(); i++){
    const dgInterfaceVector &iv = mesh.interfaceVector(i);
    if (iv.nConnection() != 2)
      continue;
    for (int iFace=0; iFace < iv.size(); iFace++){
      const dgElementVector &ev0 = iv.elementVector(0);
      const dgElementVector &ev1 = iv.elementVector(1);
      if (ev0.physicalTag() != ev1.physicalTag()){
        _elemToSeamClosure[iv.element(iFace, 0).getNum()]=iv.closureId(iFace, 0);
        _elemToSeamClosure[iv.element(iFace, 1).getNum()]=iv.closureId(iFace, 1);
      }
    }
  }
  std::map<int, std::vector<int> >ghostToSend, ghostToRec;
  for (int i = 0; i < Msg::GetCommSize(); ++i) {
    if (groups.getNbImageElementsOnPartition(i, true) > 0) {
      std::vector<int> &toSend = ghostToSend[i];
      toSend.resize(groups.getNbImageElementsOnPartition(i, true));
      for (int j = 0; j < groups.getNbImageElementsOnPartition(i, true); ++j) {
	int gId = groups.getImageElementGroup(i, j, true);
	int eId = groups.getImageElementPositionInGroup(i, j , true);
	std::map<int,int>::const_iterator it = _elemToSeamClosure.find(groups.getElementGroup(gId)->getElement(eId)->getNum());
	toSend[j] = (it == _elemToSeamClosure.end()) ? -1 : it->second; 
      }
    }
  }
  dgMPI::allAllV(ghostToSend, ghostToRec, true);
  for (std::map<int, std::vector<int> >::const_iterator it = ghostToRec.begin(); it != ghostToRec.end(); ++it) {
    for (size_t j = 0; j < it->second.size(); ++j) {
      int iGroup = groups.getGhostElementGroup(it->first, j, true);
      int iEl = groups.getGhostElementPositionInGroup(it->first, j, true);
      if (it->second[j] != -1)
	_elemToSeamClosure[groups.getElementGroup(iGroup)->getElement(iEl)->getNum()] = it->second[j];
    }
  }
}


void dgCircleTransform::applyJacobian(const MElement &elem, double xyz[3], double uvw[3], double jac[3][3], double &detJ, int closureId) const
{
  std::map<int,int>::const_iterator it = _elemToSeamClosure.find(elem.getNum());
  if (it == _elemToSeamClosure.end())
    return;
  
  const nodalBasis &fsElem1 = *elem.getFunctionSpace(1);
  const fullMatrix<double> &p = fsElem1.points;
  const std::vector<int> &closure = fsElem1.getFullClosure(it->second);
  int dim = elem.getDim();
  
  
  double V[4][2];
  double A[2] = {0, 0}; //A = [alpha, beta]; U=[u v]
  double f[8], g[8][3];
  fsElem1.f(uvw[0], uvw[1], uvw[2], f);
  fsElem1.df(uvw[0], uvw[1], uvw[2], g);
  
  fullMatrix<double> dAdU(2, dim);
  fullMatrix<double> dUdUcl(dim, dim);
  double U[3] = {0., 0., 0.};
  for (int k = 0; k < p.size1(); ++k) {
    for (int i = 0; i < dim; ++i) {
      U[i] += p(k, i) * f[closure[k]];
      for (int j = 0; j < dim; ++j) {
        dUdUcl(i, j) += p(k, i) * g[closure[k]][j];
      }
    }
  }
  
  getParametricCoordinatesInReference(elem, closure, uvw, A, V);
  getParametricCoordinatesDerivInReference(elem, closure, uvw, dAdU);
  
  fullMatrix<double> dAdUcl(2, dim);
  dAdU.mult(dUdUcl, dAdUcl);
  
  if (closureId >= 0){
    fullMatrix<double> dAdU2(2, dim), dU2dU(dim, dim), dUdU2(dim, dim);
    const std::vector<int> &closure2 = fsElem1.getFullClosure(closureId);
    for (int i = 0; i < p.size1(); i++){
      for (int j = 0; j < dim; j++) {
        for (int k = 0; k < dim; k++) {
          dU2dU(j, k) += p(i,j) * g[closure2[i]][k];
        }
      }
    }
    dU2dU.invert(dUdU2);
    dAdUcl.mult(dUdU2, dAdU2);
    dAdUcl = dAdU2;
  }
  
  double dXdXi[2][2];
  transformdXdXi(elem, A, V, dXdXi);
  for (int i = 0; i < 2; i++){
    for (int j=0; j<dim; j++){
      //jac must be transposed!
      jac[j][i] = dXdXi[i][0] * dAdUcl(0, j) + dXdXi[i][1] * dAdUcl(1, j);
    }
  }
  detJ = jac[0][0] * (jac[1][1] * jac[2][2] - jac[1][2] * jac[2][1])
    - jac[0][1] * (jac[1][0] * jac[2][2] - jac[1][2] * jac[2][0])
    + jac[0][2] * (jac[1][0] * jac[2][1] - jac[1][1] * jac[2][0]);
}



void transformToCircle::getCircleCoordinates (const MElement &elem, double uvw[3], int closureId, double xyzIn[3], double xyzOut[3]){
  std::map<int,int>::const_iterator it = _elemToSeamClosure.find(elem.getNum());
  if (it == _elemToSeamClosure.end()){
    for (int i = 0; i < 3; i++)
      xyzOut[i]=xyzIn[i];
    return;
  }
  const nodalBasis &fsElem1 = *elem.getFunctionSpace(1);
  const std::vector<int> &closure = fsElem1.getFullClosure(it->second);
  
  double V[4][2];
  double A[2] = {0., 0.};
  getParametricCoordinatesInReference(elem, closure, uvw, A, V);
  transformCoordinates(elem, A, V, xyzOut);
  xyzOut[2] = xyzIn[2]; 
}

transformToCircle::transformToCircle(dgCircleTransform &transform):function(3), _elemToSeamClosure(transform.elemToSeamClosure()){
  setArgument(_xyz, function::getCoordinates());
}

void transformToCircle::call (dataCacheMap *cmap, fullMatrix<double> &val){
  const fullMatrix<double> &uvw = cmap->parametricCoordinates();
  int nP = cmap->nPointByElement();
  for (int iEl = 0; iEl < cmap->nElement(); ++iEl) {
    MElement &elem=*cmap->element(iEl);
    std::map<int,int>::const_iterator it = _elemToSeamClosure.find(elem.getNum());
    if (it == _elemToSeamClosure.end()){
      for (int i = 0; i < nP; ++i) {
        val(iEl * nP + i, 0) =_xyz(iEl * nP + i, 0);
        val(iEl * nP + i, 1) =_xyz(iEl * nP + i, 1);
        val(iEl * nP + i, 2) =_xyz(iEl * nP + i, 2);
      }
    }
    else {
      const nodalBasis &fsElem1 = *elem.getFunctionSpace(1);
      const fullMatrix<double> &p = fsElem1.points;
      const std::vector<int> &closure = fsElem1.getFullClosure(it->second);
      for (int i = 0; i < nP; i++){
        double coord[3] = {0, 0, 0};
        if(cmap->getGroupOfInterfaces()) {
          // Coordinates from the parametric space of the face to the one of the element
          int clId = cmap->getGroupOfInterfaces()->closureId(cmap->interfaceId(iEl), cmap->connectionId());
          const nodalBasis &fsFace1 = *BasisFactory::getNodalBasis(fsElem1.getClosureType(clId));
          const std::vector<int> &cl = fsElem1.getClosure(clId);
          double f[8];
          fsFace1.f(uvw(i, 0), uvw(i, 1), uvw(i, 2), f);
          for (size_t iPsi = 0; iPsi < cl.size(); iPsi++) {
            for (int d = 0; d < p.size2(); d++) {
              coord[d] += f[iPsi] * p(cl[iPsi], d);
            }
          }
        }
        else {
          coord[0] = uvw(i, 0);
          coord[1] = uvw(i, 1);
          coord[2] = uvw.size2() > 2 ? uvw(i, 2) : 0;
        }

        double A[2];
        double V[4][2];
        getParametricCoordinatesInReference(elem, closure, coord, A, V);
        double xyz[2];
        transformCoordinates(elem, A, V, xyz);
        val(iEl * nP + i, 0) = xyz[0];
        val(iEl * nP + i, 1) = xyz[1];
        val(iEl * nP + i, 2) = _xyz(iEl * nP + i, 2);
      }
    }
  }
}
