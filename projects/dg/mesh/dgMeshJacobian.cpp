#include "dgMeshJacobian.h"

#include "dgMesh.h"
#include "dgElementVector.h"
#include "dgInterfaceVector.h"
#include "dgFullMatrix.h"
#include "dgIntegrationMatrices.h"

#include "nodalBasis.h"
#include "MElement.h"
#include "Numeric.h"
#include "dgSpaceTransform.h"

class elementCoordMod {
  fullMatrix<double> _svCoord;
  MElement &_e;
  public:
  elementCoordMod(MElement &e, fullMatrix<double> &newCoord):
    _e(e)
  {
    _svCoord.resize(newCoord.size1(), newCoord.size2());
    if(_e.getNumVertices() != newCoord.size1() || newCoord.size2() != 3) {
      Msg::Fatal("invalid coord matrix size");
    }
    for (int i = 0; i < _e.getNumVertices(); ++i) {
      MVertex &v = *_e.getVertex(i);
      _svCoord(i, 0) = v.x();
      _svCoord(i, 1) = v.y();
      _svCoord(i, 2) = v.z();
      v.x() = newCoord(i, 0);
      v.y() = newCoord(i, 1);
      v.z() = newCoord(i, 2);
    }
  }
  ~elementCoordMod()
  {
    for (int i = 0; i < _e.getNumVertices(); ++i) {
      MVertex &v = *_e.getVertex(i);
      v.x() = _svCoord(i, 0);
      v.y() = _svCoord(i, 1);
      v.z() = _svCoord(i, 2);
    }
  }
};

static void _computeElementVector(const dgElementVector &ev, int integrationOrder, dgFullMatrix<double> &dXiDX, fullMatrix<double> &detJ, fullVector<double> &elementVolume, fullVector<double> &elementInnerRadius, const dgSpaceTransform *st)
{
  const nodalBasis &fs = ev.functionSpace();
  if (integrationOrder == -1)
    integrationOrder = fs.order * 2 + 1;
  const dgIntegrationMatrices &intMatrices = dgIntegrationMatrices::get(fs.type, integrationOrder);
  const fullMatrix<double> &integration = intMatrices.integrationPoints();
  int nbQP = integration.size1();
  int nbNodes = fs.points.size1();
  dXiDX.resize(fs.dimension, nbQP * 3, ev.size(), false);
  detJ.resize(nbQP, ev.size());
  dgFullMatrix<double> allGGeo(nbNodes, fs.dimension, nbQP, false);
  elementVolume.resize(ev.size());
  elementInnerRadius.resize(ev.size());
  for (int i = 0; i < ev.size(); ++i) {
    MElement &e = ev.element(i);
    elementInnerRadius(i) = e.getInnerRadius(); 
  }
  fullMatrix<double> gGeo;
  for (int xi = 0; xi < nbQP; ++xi) {
    allGGeo.getBlockProxy(xi, gGeo);
    for (int i = 0; i < nbNodes; ++i) {
      for (int d = 0; d < fs.dimension; ++d) {
        gGeo(i, d) = intMatrices.dPsi()(xi * fs.dimension + d, i);
      }
    }
  }
  fullMatrix<double> coordEl;
  for (int i = 0; i < ev.size(); ++i) {
    MElement &e = ev.element(i);
    ev.coordinates().getBlockProxy(i, coordEl);
    elementCoordMod tmpMod = elementCoordMod(e, coordEl);
    elementInnerRadius(i) = e.getInnerRadius();
    double jac[3][3],ijac[3][3];
    for (int xi = 0; xi < nbQP; ++xi) {
      allGGeo.getBlockProxy(xi, gGeo);
      detJ(xi, i) = fabs(e.getJacobian(gGeo, jac));
      if (st) {
        double uvw[3] = {integration(xi, 0), integration(xi, 1), integration(xi, 2)};
        SPoint3 p;
        e.pnt(uvw[0], uvw[1], uvw[2], p);
        double xyz[3] = {p.x(), p.y(), p.z()};
        st->applyJacobian(e, xyz, uvw, jac, detJ(xi, i));
      }
      inv3x3(jac, ijac);
      for (int alpha = 0; alpha < fs.dimension; ++alpha) {
        for (int a = 0; a < 3; a++) {
          dXiDX (alpha, (i * nbQP + xi) * 3 + a) = ijac[a][alpha];
        }
      }
      elementVolume(i) += detJ(xi, i) * intMatrices.integrationWeights()(xi);
    }
  }
}

static void _computeInterfaceVector(const dgInterfaceVector &iv, int integrationOrder, fullMatrix<double> &detJ, std::vector<dgFullMatrix<double> > &dXiDXV, std::vector<dgFullMatrix<double> > &normalsV, fullVector<double> &interfaceSurface, const dgSpaceTransform *st)
{
  dXiDXV.resize(iv.nConnection());
  normalsV.resize(iv.nConnection());
  for (int iConn = 0; iConn < iv.nConnection(); ++iConn) {
    const nodalBasis &fsElem = iv.elementVector(iConn).functionSpace();
    int closureRef = iv.closureRef(iConn);
    const dgIntegrationMatrices &intMatrices = dgIntegrationMatrices::get(fsElem.type, integrationOrder, closureRef);
    int nbPsi = fsElem.points.size1();
    int nbQP = intMatrices.integrationPoints().size1();
    int size = iv.size();
    int dimUVW = fsElem.dimension;
    fullMatrix<double> &dXiDX = dXiDXV[iConn];
    dgFullMatrix<double> &normals = normalsV[iConn];
    dXiDX.resize (dimUVW, size * nbQP * 3, false);
    if(iConn == 0) {
      detJ.resize(nbQP, size);
      interfaceSurface.resize(iv.size());
    }
    normals.resize(nbQP, 3, size, false);
    fullMatrix<double> gReorder(nbPsi, fsElem.dimension);
    fullMatrix<double> nuvw(3, nbQP);
    const std::vector<int> &closure = fsElem.getClosure(closureRef);
    fullVector<double> parentJac(nbQP);
    for (int xi = 0; xi < nbQP; xi++) {
      for (size_t k = 0; k < closure.size(); k++) {
        int inode = closure[k];
        for (int d = 0; d < fsElem.dimension; d++) {
          nuvw(d, xi) += intMatrices.dPsi()(xi * fsElem.dimension + d, inode);
        }
      }
      double nn = 0;
      for (int d = 0; d < fsElem.dimension; d++)
        nn += nuvw(d, xi) * nuvw(d, xi);
      nn = sqrt(nn);
      for (int d = 0; d < fsElem.dimension; d++)
        nuvw(d, xi) /= nn;
    }
    fullMatrix<double> normalFace, coordEl;
    for (int i = 0; i < size; i++) {
      normals.getBlockProxy(i, normalFace);
      MElement &element = iv.element(i, iConn);
      iv.elementVector(iConn).coordinates().getBlockProxy(iv.elementId(i, iConn), coordEl);
      elementCoordMod tmpMod = elementCoordMod(element, coordEl);
      double jac[3][3], ijac[3][3];
      int closureId = iv.closureId(i, iConn);
      for (int xi = 0; xi < nbQP; xi++){
        double coord[3] = {0, 0, 0}; 
        for (int iPsi = 0; iPsi < nbPsi; iPsi++) {
          for (int d = 0; d < fsElem.dimension; d++){
            gReorder(fsElem.fullClosures[closureId][iPsi], d) = intMatrices.dPsi()(xi * fsElem.dimension + d, iPsi);
          }
        }
        for (size_t iPsi = 0; iPsi < fsElem.closures[closureId].size(); iPsi++) {  	 	 
          for (int d = 0; d < fsElem.dimension; d++) {  	 	 
            coord[d] += intMatrices.psi()(xi, iPsi) * fsElem.points(fsElem.closures[closureId][iPsi], d);  	 	 
          }  	 	 
        } 
        double j = element.getJacobian(gReorder, jac);
        if (st) {
          SPoint3 p;
          element.pnt(coord[0], coord[1], coord[2], p);
          double xyz[3] = {p.x(), p.y(), p.z()};
          st->applyJacobian(element, xyz, coord, jac, j, closureId);
        }
        j = fabs(j);
        inv3x3(jac, ijac);
        for (int alpha = 0; alpha < dimUVW; alpha++)
          for (int a = 0; a < 3; a++)
            dXiDX (alpha, (i * nbQP + xi) * 3 + a) = ijac[a][alpha];
        normalFace(xi, 0) = nuvw(0, xi)*ijac[0][0]+nuvw(1, xi)*ijac[0][1]+nuvw(2, xi)*ijac[0][2];
        normalFace(xi, 1) = nuvw(0, xi)*ijac[1][0]+nuvw(1, xi)*ijac[1][1]+nuvw(2, xi)*ijac[1][2];
        normalFace(xi, 2) = nuvw(0, xi)*ijac[2][0]+nuvw(1, xi)*ijac[2][1]+nuvw(2, xi)*ijac[2][2];
        double norm = sqrt(normalFace(xi, 0) * normalFace(xi, 0) + normalFace(xi, 1) * normalFace(xi, 1) + normalFace(xi, 2) * normalFace(xi, 2));
        normalFace(xi, 0) /= norm;
        normalFace(xi, 1) /= norm;
        normalFace(xi, 2) /= norm;
        if (iConn == 0) {
          detJ(xi, i) = j * norm;
          interfaceSurface(i) += detJ(xi, i) * intMatrices.integrationWeights()(xi);
        }
      }
    }
  }
}

dgMeshJacobian::dgMeshJacobian(const dgMesh &mesh, int integrationOrder):
  _mesh(&mesh), _integrationOrder(integrationOrder)
{
  int nTotalElement = _mesh->nElementVector() + _mesh->nGhostElementVector();
  _elementVolume.resize(nTotalElement);
  _elementInnerRadius.resize(nTotalElement);
  _elementJacobian.resize(nTotalElement);
  _elementJacobianDeterminant.resize(nTotalElement);
  for (int i = 0; i < _mesh->nElementVector(); ++i) {
    _computeElementVector(_mesh->elementVector(i), _integrationOrder, _elementJacobian[i], _elementJacobianDeterminant[i], _elementVolume[i], _elementInnerRadius[i], _mesh->spaceTransform());
  }
  for (int i = 0; i < _mesh->nGhostElementVector(); ++i) {
    int j = i + _mesh->nElementVector();
    _computeElementVector(_mesh->ghostElementVector(i), _integrationOrder, _elementJacobian[j], _elementJacobianDeterminant[j], _elementVolume[j], _elementInnerRadius[j], _mesh->spaceTransform());
  }
  _interfaceJacobianDeterminant.resize(_mesh->nInterfaceVector());
  _interfaceNormals.resize(_mesh->nInterfaceVector());
  _interfaceDXiDX.resize(_mesh->nInterfaceVector());
  _interfaceSurface.resize(_mesh->nInterfaceVector());
  for (int i = 0; i < _mesh->nInterfaceVector(); ++i) {
    _computeInterfaceVector(_mesh->interfaceVector(i), _integrationOrder, _interfaceJacobianDeterminant[i], _interfaceDXiDX[i], _interfaceNormals[i], _interfaceSurface[i], _mesh->spaceTransform());
  }
}

dgMeshJacobian::~dgMeshJacobian()
{
}

static void _multDXiDXUnrolled (int iElement, const fullMatrix<double> &dXiDX, const fullMatrix<double> &vXi, fullMatrix<double> &vX) {
  int nbFields = vXi.size2();
  int nbIntegrationPoints = vX.size1();
  int dimUVW = vXi.size1()/nbIntegrationPoints;
  switch (dimUVW) {
    case 1 :
      {
        for (int iField = 0; iField < nbFields; iField ++) {
          for (int iPt = 0; iPt < nbIntegrationPoints; iPt++) {
            const int jj = (iElement * nbIntegrationPoints + iPt) * 3;
            const double dudxi = vXi(iPt + 0, iField);
            vX(iPt, 3*iField + 0) =  dudxi* dXiDX(0, jj+0);
            vX(iPt, 3*iField + 1) =  dudxi* dXiDX(0, jj+1);
            vX(iPt, 3*iField + 2) =  dudxi* dXiDX(0, jj+2);
          }
        }
      }
      break;
    case 2 :
      {
        for (int iField = 0; iField < nbFields; iField ++) {
          for (int iPt = 0; iPt < nbIntegrationPoints; iPt++) {
            const int jj = (iElement * nbIntegrationPoints + iPt) * 3;
            const double dudxi  = vXi(iPt * 2 + 0, iField);
            const double dudeta = vXi(iPt * 2 + 1, iField);
            vX(iPt, 3*iField + 0) =  dudxi* dXiDX(0, jj+0) + dudeta * dXiDX(1, jj+0);
            vX(iPt, 3*iField + 1) =  dudxi* dXiDX(0, jj+1) + dudeta * dXiDX(1, jj+1);
            vX(iPt, 3*iField + 2) =  dudxi* dXiDX(0, jj+2) + dudeta * dXiDX(1, jj+2);
          }
        }
      }
      break;
    case 3 :
      {
        for (int iField = 0; iField < nbFields; iField ++) {
          for (int iPt = 0; iPt < nbIntegrationPoints; iPt++) {
            const int jj = (iElement * nbIntegrationPoints + iPt) * 3;
            const double dudxi   = vXi(iPt * 3 + 0, iField);
            const double dudeta  = vXi(iPt * 3 + 1, iField);
            const double dudzeta = vXi(iPt * 3 + 2, iField);
            vX(iPt, 3*iField + 0) =  dudxi * dXiDX(0, jj + 0) + dudeta * dXiDX(1, jj + 0) + dudzeta * dXiDX(2, jj + 0);
            vX(iPt, 3*iField + 1) =  dudxi * dXiDX(0, jj + 1) + dudeta * dXiDX(1, jj + 1) + dudzeta * dXiDX(2, jj + 1);
            vX(iPt, 3*iField + 2) =  dudxi * dXiDX(0, jj + 2) + dudeta * dXiDX(1, jj + 2) + dudzeta * dXiDX(2, jj + 2);
          }
        }
      }
      break;
  }
}

static void _multDXiDXJParentUnrolled (int iElement, const fullMatrix<double> &dXiDX, const fullMatrix<double> &det, const fullMatrix<double> &vXi, fullMatrix<double> &vX) {
  int nbFields = vXi.size2();
  int nbIntegrationPoints = vX.size1();
  int dimUVW = vXi.size1()/nbIntegrationPoints;
  switch (dimUVW) {
    case 1 :
      {
	for (int iPt = 0; iPt < nbIntegrationPoints; iPt++) {
	  const double detJ = det(iPt, iElement);
	  const int jj = (iElement * nbIntegrationPoints + iPt) * 3;
	  for (int iField = 0; iField < nbFields; iField ++) {
            const double dudxi = vXi(iPt + 0, iField);
            vX(iPt, iField + 0 * nbFields) =  dudxi* dXiDX(0, jj+0) * detJ;
            vX(iPt, iField + 1 * nbFields) =  dudxi* dXiDX(0, jj+1) * detJ;
            vX(iPt, iField + 2 * nbFields) =  dudxi* dXiDX(0, jj+2) * detJ;
          }
        }
      }
      break;
    case 2 :
      {
	for (int iPt = 0; iPt < nbIntegrationPoints; iPt++) {
	  const double detJ = det(iPt, iElement);
	  const int jj = (iElement * nbIntegrationPoints + iPt) * 3;
	  for (int iField = 0; iField < nbFields; iField ++) {
            const double dudxi  = vXi(iPt * 2 + 0, iField);
            const double dudeta = vXi(iPt * 2 + 1, iField);
            vX(iPt, iField + 0 * nbFields) =  (dudxi* dXiDX(0, jj+0) + dudeta * dXiDX(1, jj+0)) * detJ;
            vX(iPt, iField + 1 * nbFields) =  (dudxi* dXiDX(0, jj+1) + dudeta * dXiDX(1, jj+1)) * detJ;
            vX(iPt, iField + 2 * nbFields) =  (dudxi* dXiDX(0, jj+2) + dudeta * dXiDX(1, jj+2)) * detJ;
          }
        }
      }
      break;
    case 3 :
      {
	for (int iPt = 0; iPt < nbIntegrationPoints; iPt++) {
          const double detJ = det(iPt, iElement);
	  const int jj = (iElement * nbIntegrationPoints + iPt) * 3;
	  for (int iField = 0; iField < nbFields; iField ++) {
            const double dudxi   = vXi(iPt * 3 + 0, iField);
            const double dudeta  = vXi(iPt * 3 + 1, iField);
            const double dudzeta = vXi(iPt * 3 + 2, iField);
            vX(iPt, iField + 0 * nbFields) =  (dudxi * dXiDX(0, jj + 0) + dudeta * dXiDX(1, jj + 0) + dudzeta * dXiDX(2, jj + 0)) * detJ;
            vX(iPt, iField + 1 * nbFields) =  (dudxi * dXiDX(0, jj + 1) + dudeta * dXiDX(1, jj + 1) + dudzeta * dXiDX(2, jj + 1)) * detJ;
            vX(iPt, iField + 2 * nbFields) =  (dudxi * dXiDX(0, jj + 2) + dudeta * dXiDX(1, jj + 2) + dudzeta * dXiDX(2, jj + 2)) * detJ;
          }
        }
      }
      break;
  }
}

fullMatrix<double> dgMeshJacobian::multDXiDXJElement(int iGroup, const fullMatrix<double> &vX) const
{
  const fullMatrix<double> &det = getElementJacobianDeterminant(iGroup);
  const fullMatrix<double> &dXiDX = getElementJacobian(iGroup);
  const int dim = _mesh->elementVector(iGroup).functionSpace().dimension;
  const int nP = det.size1();
  const int nE = det.size2();
  const int nField = vX.size2() / 3;
  fullMatrix<double> vXi;
  vXi.resize(nP * dim, nE * nField, false);
  //vXi.resize(nP * dim, nE * nField, false);
  switch (dim) {
    case 1 :
      for (int iE = 0; iE < nE; ++iE) {
        for (int iField = 0; iField < nField; iField ++) {
          for (int iPt =0; iPt< nP; iPt++) {
            int jj = (iE*nP + iPt)*3;
            int ii = iE * nP + iPt;
            const double detJ = det(iPt, iE);
            double dudx = vX(ii, iField);
            double dudy = vX(ii, iField + nField);
            double dudz = vX(ii, iField + nField +nField);
            vXi(iPt, iE * nField + iField) = (dudx * dXiDX(0, jj+0) + dudy * dXiDX(0, jj+1) + dudz * dXiDX(0, jj+2)) * detJ;
          }
        }
      }
      break;
    case 2 :
      for (int iE = 0; iE < nE; ++iE) {
        for (int iField = 0; iField < nField; iField ++) {
          for (int iPt =0; iPt< nP; iPt++) {
            int jj = (iE*nP + iPt)*3;
            //int ii = iE * nP + iPt;
            const double detJ = det(iPt, iE);
            double dudx = vX(iE * nP + iPt, iField );
            double dudy = vX(iE * nP + iPt, iField + nField );
            double dudz = vX(iE * nP + iPt, iField + nField + nField);
            vXi(iPt*2+0, iE * nField + iField) = (dudx * dXiDX(0, jj+0) + dudy * dXiDX(0, jj+1) + dudz * dXiDX(0, jj+2)) * detJ;
            vXi(iPt*2+1, iE * nField + iField) = (dudx * dXiDX(1, jj+0) + dudy * dXiDX(1, jj+1) + dudz * dXiDX(1, jj+2)) * detJ;
          }
        }
      }
      break;
    case 3 :
      for (int iE = 0; iE < nE; ++iE) {
        for (int iField = 0; iField < nField; iField ++) {
          for (int iPt =0; iPt< nP; iPt++) {
            int jj = (iE*nP + iPt)*3;
            int ii = iE * nP + iPt;
            const double detJ = det(iPt, iE);
            double dudx = vX(ii, iField );
            double dudy = vX(ii, iField + nField );
            double dudz = vX(ii, iField + nField +nField);
            vXi(iPt*3+0, iE * nField + iField) = (dudx * dXiDX(0, jj+0) + dudy * dXiDX(0, jj+1) + dudz * dXiDX(0, jj+2)) * detJ;
            vXi(iPt*3+1, iE * nField + iField) = (dudx * dXiDX(1, jj+0) + dudy * dXiDX(1, jj+1) + dudz * dXiDX(1, jj+2)) * detJ;
            vXi(iPt*3+2, iE * nField + iField) = (dudx * dXiDX(2, jj+0) + dudy * dXiDX(2, jj+1) + dudz * dXiDX(2, jj+2)) * detJ;
          }
        }
      }
      break;
  }
  return vXi;
}

void dgMeshJacobian::multDXiDXJParentElement (int iGroup, int iElement, const fullMatrix<double> & vXi, fullMatrix<double> &vX) const
{
  _multDXiDXJParentUnrolled(iElement, getElementJacobian(iGroup), getElementJacobianDeterminant(iGroup), vXi, vX);
}

fullMatrix<double> dgMeshJacobian::multDXiDXJRightElement(int iGroup, const fullMatrix<double> & vX) const
{
  const fullMatrix<double> &DetJ = getElementJacobianDeterminant(iGroup);
  const int nP = DetJ.size1();
  const int dim = _mesh->elementVector(iGroup).functionSpace().dimension;
  const int nbFields = vX.size2() / 3;
  const int nbElements = vX.size1() / nP;
  const dgFullMatrix<double> &dXiDX = getElementJacobian(iGroup);
  fullMatrix<double> vXi(nP * dim, nbFields * nbElements);
  switch (dim) {
    case 1 :
      for (int iElement = 0; iElement < nbElements; ++iElement) {
        for (int iField = 0; iField < nbFields; iField ++) {
          for (int iPt =0; iPt< nP; iPt++) {
            int jj = (iElement * nP + iPt) * 3;
            double detJ = DetJ(iPt, iElement);
            double dudx = vX(iPt + iElement * nP, iField * 3 );
            double dudy = vX(iPt + iElement * nP, iField * 3 + 1);
            double dudz = vX(iPt + iElement * nP, iField * 3 + 2);
            vXi(iPt, iField + iElement * nbFields) = (dudx * dXiDX(0, jj+0) + dudy * dXiDX(0, jj+1) + dudz * dXiDX(0, jj+2)) * detJ;
          }
        }
      }
      break;
    case 2 :
      for (int iElement = 0; iElement < nbElements; ++iElement) {
        for (int iField = 0; iField < nbFields; iField ++) {
          for (int iPt =0; iPt< nP; iPt++) {
            int jj = (iElement * nP + iPt) * 3;
            double detJ = DetJ(iPt, iElement);
            double dudx = vX(iPt + iElement * nP, iField * 3 );
            double dudy = vX(iPt + iElement * nP, iField * 3 + 1);
            double dudz = vX(iPt + iElement * nP, iField * 3 + 2);
            vXi(       iPt, iField + iElement * nbFields) = (dudx * dXiDX(0, jj+0) + dudy * dXiDX(0, jj+1) + dudz * dXiDX(0, jj+2)) * detJ;
            vXi(nP + iPt, iField + iElement * nbFields) = (dudx * dXiDX(1, jj+0) + dudy * dXiDX(1, jj+1) + dudz * dXiDX(1, jj+2)) * detJ;
          }
        }
      }
      break;
    case 3 :
      for (int iElement = 0; iElement < nbElements; ++iElement) {
        for (int iField = 0; iField < nbFields; iField ++) {
          for (int iPt =0; iPt < nP; iPt++) {
            int jj = (iElement * nP + iPt) * 3;
            double detJ = DetJ(iPt, iElement);
            double dudx = vX(iPt + iElement * nP, iField * 3 );
            double dudy = vX(iPt + iElement * nP, iField * 3 + 1);
            double dudz = vX(iPt + iElement * nP, iField * 3 + 2);
            vXi(           iPt, iField + iElement * nbFields) = (dudx * dXiDX(0, jj+0) + dudy * dXiDX(0, jj+1) + dudz * dXiDX(0, jj+2)) * detJ;
            vXi(    nP + iPt, iField + iElement * nbFields) = (dudx * dXiDX(1, jj+0) + dudy * dXiDX(1, jj+1) + dudz * dXiDX(1, jj+2)) * detJ;
            vXi(2 * nP + iPt, iField + iElement * nbFields) = (dudx * dXiDX(2, jj+0) + dudy * dXiDX(2, jj+1) + dudz * dXiDX(2, jj+2)) * detJ;
          }
        }
      }
      break;
  }
  return vXi;
}

fullMatrix<double> dgMeshJacobian::multDXiDXDXiDXJElement(int iGroup, const fullMatrix<double> &vX) const {
  const int dim = _mesh->elementVector(iGroup).functionSpace().dimension;
  const int nbFields = vX.size2() / 9;
  const fullMatrix<double> &DetJ = getElementJacobianDeterminant(iGroup);
  const int nP = DetJ.size1();
  const int nbElements = vX.size1() / nP;
  const dgFullMatrix<double> &dXiDX = getElementJacobian(iGroup);
  fullMatrix<double> vXi(dim * dim * nP, nbFields * nbElements);
  switch (dim) {
    case 1 :
      for (int iElement = 0; iElement < nbElements; ++iElement) {
        for (int k = 0; k < nbFields; k++) {
          const int kk0 = k * 3;
          const int kk1 = (k + nbFields) * 3;
          const int kk2 = (k + 2 * nbFields) * 3;
          for (int iPt = 0; iPt < nP; iPt++) {
            double detJ = DetJ(iPt, iElement);
            const int jj = (iElement * nP + iPt) * 3;
            const int ii = (iElement * nP + iPt);
            const int kk = (iElement * nbFields + k);
            const double ddd00 = vX(ii, kk0) * dXiDX(0, jj) + vX(ii, kk0 + 1) * dXiDX(0, jj + 1) + vX(ii, kk0 + 2) * dXiDX(0, jj + 2);
            const double ddd10 = vX(ii, kk1) * dXiDX(0, jj) + vX(ii, kk1 + 1) * dXiDX(0, jj + 1) + vX(ii, kk1 + 2) * dXiDX(0, jj + 2);
            const double ddd20 = vX(ii, kk2) * dXiDX(0, jj) + vX(ii, kk2 + 1) * dXiDX(0, jj + 1) + vX(ii, kk2 + 2) * dXiDX(0, jj + 2);
            vXi(iPt, kk) = (ddd00 * dXiDX(0, jj + 0) + ddd10 * dXiDX(0, jj + 1) + ddd20 * dXiDX(0, jj + 2)) * detJ;
          }
        }
      }
      break;
    case 2 :
      for (int iElement = 0; iElement < nbElements; ++iElement) {
        for (int k = 0; k < nbFields; k++) {
          const int kk0 = k * 3;
          const int kk1 = (k + nbFields) * 3;
          const int kk2 = (k + 2 * nbFields) * 3;
          for (int iPt = 0; iPt < nP; iPt++) {
            double detJ = DetJ(iPt, iElement);
            const int jj = (iElement * nP + iPt) * 3;
            const int ii = (iElement * nP + iPt);
            const int kk = (iElement * nbFields + k);
            const double ddd00 = vX(ii, kk0) * dXiDX(0, jj) + vX(ii, kk0 + 1) * dXiDX(0, jj + 1) + vX(ii, kk0 + 2) * dXiDX(0, jj + 2);
            const double ddd10 = vX(ii, kk1) * dXiDX(0, jj) + vX(ii, kk1 + 1) * dXiDX(0, jj + 1) + vX(ii, kk1 + 2) * dXiDX(0, jj + 2);
            const double ddd20 = vX(ii, kk2) * dXiDX(0, jj) + vX(ii, kk2 + 1) * dXiDX(0, jj + 1) + vX(ii, kk2 + 2) * dXiDX(0, jj + 2);
            const double ddd01 = vX(ii, kk0) * dXiDX(1, jj) + vX(ii, kk0 + 1) * dXiDX(1, jj + 1) + vX(ii, kk0 + 2) * dXiDX(1, jj + 2);
            const double ddd11 = vX(ii, kk1) * dXiDX(1, jj) + vX(ii, kk1 + 1) * dXiDX(1, jj + 1) + vX(ii, kk1 + 2) * dXiDX(1, jj + 2);
            const double ddd21 = vX(ii, kk2) * dXiDX(1, jj) + vX(ii, kk2 + 1) * dXiDX(1, jj + 1) + vX(ii, kk2 + 2) * dXiDX(1, jj + 2);
            vXi(iPt, kk)          = (ddd00 * dXiDX(0, jj + 0) + ddd10 * dXiDX(0, jj + 1) + ddd20 * dXiDX(0, jj + 2)) * detJ;
            vXi(nP + iPt, kk)   = (ddd01 * dXiDX(0, jj + 0) + ddd11 * dXiDX(0, jj + 1) + ddd21 * dXiDX(0, jj + 2)) * detJ;
            vXi(2*nP + iPt, kk) = (ddd00 * dXiDX(1, jj + 0) + ddd10 * dXiDX(1, jj + 1) + ddd20 * dXiDX(1, jj + 2)) * detJ;
            vXi(3*nP + iPt, kk) = (ddd01 * dXiDX(1, jj + 0) + ddd11 * dXiDX(1, jj + 1) + ddd21 * dXiDX(1, jj + 2)) * detJ;
          }
        }
      }
      break;
    case 3 :
      for (int iElement = 0; iElement < nbElements; ++iElement) {
        for (int k = 0; k < nbFields; k++) {
          const int kk0 = k * 3;
          const int kk1 = (k + nbFields) * 3;
          const int kk2 = (k + 2 * nbFields) * 3;
          for (int iPt = 0; iPt < nP; iPt++) {
            double detJ = DetJ(iPt, iElement);
            const int jj = (iElement * nP + iPt) * 3;
            const int ii = (iElement * nP + iPt);
            const int kk = (iElement * nbFields + k);
            const double ddd00 = vX(ii, kk0) * dXiDX(0, jj) + vX(ii, kk0 + 1) * dXiDX(0, jj + 1) + vX(ii, kk0 + 2) * dXiDX(0, jj + 2);
            const double ddd10 = vX(ii, kk1) * dXiDX(0, jj) + vX(ii, kk1 + 1) * dXiDX(0, jj + 1) + vX(ii, kk1 + 2) * dXiDX(0, jj + 2);
            const double ddd20 = vX(ii, kk2) * dXiDX(0, jj) + vX(ii, kk2 + 1) * dXiDX(0, jj + 1) + vX(ii, kk2 + 2) * dXiDX(0, jj + 2);
            const double ddd01 = vX(ii, kk0) * dXiDX(1, jj) + vX(ii, kk0 + 1) * dXiDX(1, jj + 1) + vX(ii, kk0 + 2) * dXiDX(1, jj + 2);
            const double ddd11 = vX(ii, kk1) * dXiDX(1, jj) + vX(ii, kk1 + 1) * dXiDX(1, jj + 1) + vX(ii, kk1 + 2) * dXiDX(1, jj + 2);
            const double ddd21 = vX(ii, kk2) * dXiDX(1, jj) + vX(ii, kk2 + 1) * dXiDX(1, jj + 1) + vX(ii, kk2 + 2) * dXiDX(1, jj + 2);
            const double ddd02 = vX(ii, kk0) * dXiDX(2, jj) + vX(ii, kk0 + 1) * dXiDX(2, jj + 1) + vX(ii, kk0 + 2) * dXiDX(2, jj + 2);
            const double ddd12 = vX(ii, kk1) * dXiDX(2, jj) + vX(ii, kk1 + 1) * dXiDX(2, jj + 1) + vX(ii, kk1 + 2) * dXiDX(2, jj + 2);
            const double ddd22 = vX(ii, kk2) * dXiDX(2, jj) + vX(ii, kk2 + 1) * dXiDX(2, jj + 1) + vX(ii, kk2 + 2) * dXiDX(2, jj + 2);
            vXi(iPt, kk)          = (ddd00 * dXiDX(0, jj + 0) + ddd10 * dXiDX(0, jj + 1) + ddd20 * dXiDX(0, jj + 2)) * detJ;
            vXi(nP + iPt, kk)   = (ddd01 * dXiDX(0, jj + 0) + ddd11 * dXiDX(0, jj + 1) + ddd21 * dXiDX(0, jj + 2)) * detJ;
            vXi(2*nP + iPt, kk) = (ddd02 * dXiDX(0, jj + 0) + ddd12 * dXiDX(0, jj + 1) + ddd22 * dXiDX(0, jj + 2)) * detJ;
            vXi(3*nP + iPt, kk) = (ddd00 * dXiDX(1, jj + 0) + ddd10 * dXiDX(1, jj + 1) + ddd20 * dXiDX(1, jj + 2)) * detJ;
            vXi(4*nP + iPt, kk) = (ddd01 * dXiDX(1, jj + 0) + ddd11 * dXiDX(1, jj + 1) + ddd21 * dXiDX(1, jj + 2)) * detJ;
            vXi(5*nP + iPt, kk) = (ddd02 * dXiDX(1, jj + 0) + ddd12 * dXiDX(1, jj + 1) + ddd22 * dXiDX(1, jj + 2)) * detJ;
            vXi(6*nP + iPt, kk) = (ddd00 * dXiDX(2, jj + 0) + ddd10 * dXiDX(2, jj + 1) + ddd20 * dXiDX(2, jj + 2)) * detJ;
            vXi(7*nP + iPt, kk) = (ddd01 * dXiDX(2, jj + 0) + ddd11 * dXiDX(2, jj + 1) + ddd21 * dXiDX(2, jj + 2)) * detJ;
            vXi(8*nP + iPt, kk) = (ddd02 * dXiDX(2, jj + 0) + ddd12 * dXiDX(2, jj + 1) + ddd22 * dXiDX(2, jj + 2)) * detJ;
          }
        }
      }
      break;
  }
  return vXi;
}

fullMatrix<double> dgMeshJacobian::multJInterface(int iVector, const std::vector<int> &faceId, const fullMatrix<double> &vX) const
{
  const fullMatrix<double> &DetJ = detJInterface(iVector);
  const int nP = DetJ.size1();
  const size_t nElement = faceId.size();
  const int nField = vX.size2();
  fullMatrix<double> vXi(nP, nElement * nField);
  for (size_t iElement = 0; iElement < nElement; ++iElement) {
    for (int iField = 0; iField < nField; iField ++) {
      for (int iP = 0; iP < nP; iP++) {
        vXi(iP, iElement* nField+ iField) = vX(iElement * nP + iP, iField) * DetJ(iP, faceId[iElement]);
      }
    }
  }
  return vXi;
}
fullMatrix<double> dgMeshJacobian::multJElement(int iGroup, const fullMatrix<double> &vX) const
{
  const fullMatrix<double> &DetJ = getElementJacobianDeterminant(iGroup);
  const int nP = DetJ.size1();
  const int nElement = DetJ.size2();
  const int nnP = vX.size1() / nP / nElement;
  const int nField = vX.size2();
  fullMatrix<double> vXi(nP * nnP, nElement * nField);
  for (int iiP = 0; iiP < nnP; iiP++) {
    for (int iElement = 0; iElement < nElement; ++iElement) {
      for (int i = 0; i < nField; ++i) {
        for (int iP = 0; iP < nP; iP++) {
          vXi(iP * nnP + iiP, iElement * nField + i) = vX((iElement * nP + iP) * nnP + iiP, i) * DetJ(iP, iElement);
        }
      }
    }
  }
  return vXi;
}

void dgMeshJacobian::dXiDXElement (int iGroup, int iElement, int iGaussPoint, fullMatrix<double> &jac) const
{
  int nbQP = _elementJacobianDeterminant[iGroup].size1();
  jac.setAsProxy(_elementJacobian[iGroup], (iElement * nbQP + iGaussPoint) * 3, 3);
}

void dgMeshJacobian::dXiDXInterface (int iGroup, int iConnection, int iFace, int iGaussPoint, fullMatrix<double> &jac) const
{
  int nbQP = _interfaceJacobianDeterminant[iGroup].size1();
  jac.setAsProxy(_interfaceDXiDX[iGroup][iConnection], (iFace * nbQP + iGaussPoint) * 3, 3);
}

void dgMeshJacobian::multDXiDXInterface(int iGroup, int iConn, int iFace, const fullMatrix<double> & vXi, fullMatrix<double> &vX) const {
  _multDXiDXUnrolled(iFace, _interfaceDXiDX[iGroup][iConn], vXi, vX);
}

void dgMeshJacobian::multDXiDXJInterface(int iGroup, int iConn, int iFace, const fullMatrix<double> &vX, fullMatrix<double> &vXi, const int d) const
{
  const int nbFields = vXi.size2();
  const int nbIntegrationPoints = vX.size1();
  const int dim = _mesh->interfaceVector(iGroup).elementVector(iConn).functionSpace().dimension;
  const fullMatrix<double> &dXiDX = _interfaceDXiDX[iGroup][iConn];
  const fullMatrix<double> &DetJ = _interfaceJacobianDeterminant[iGroup];
  switch (dim) {
    case 1 :
      for (int iField = 0; iField < nbFields; iField ++) {
        for (int iPt =0; iPt< nbIntegrationPoints; iPt++) {
          int j = iFace * nbIntegrationPoints + iPt;
          int j3 = j * 3;
          const double detJ = DetJ(iPt, iFace);
          const double fx = vX(iPt, iField + (d * 3) *  nbFields);
          const double fy = vX(iPt, iField + (d * 3 + 1) *  nbFields);
          const double fz = vX(iPt, iField + (d * 3 + 2) *  nbFields);
          vXi(iPt, iField) =  detJ * (fx * dXiDX(0, j3 + 0) + fy * dXiDX(0, j3 + 1) + fz * dXiDX(0, j3 + 2));
        }
      }
      break;
    case 2 :
      for (int iField = 0; iField < nbFields; iField ++) {
        for (int iPt =0; iPt< nbIntegrationPoints; iPt++) {
          int j = iFace * nbIntegrationPoints + iPt;
          int j3 = j * 3;
          const double detJ = DetJ(iPt, iFace);
          const double fx = vX(iPt, iField + (d * 3) *  nbFields);
          const double fy = vX(iPt, iField + (d * 3 + 1) *  nbFields);
          const double fz = vX(iPt, iField + (d * 3 + 2) *  nbFields);
          vXi(iPt * 2 + 0, iField) = detJ * (fx * dXiDX(0, j3 + 0) + fy * dXiDX(0, j3 + 1) + fz * dXiDX(0, j3 + 2));
          vXi(iPt * 2 + 1, iField) = detJ * (fx * dXiDX(1, j3 + 0) + fy * dXiDX(1, j3 + 1) + fz * dXiDX(1, j3 + 2));
        }
      }
      break;
    case 3 :
      for (int iField = 0; iField < nbFields; iField ++) {
        for (int iPt =0; iPt< nbIntegrationPoints; iPt++) {
          const int j3 = (iFace * nbIntegrationPoints + iPt) * 3;
          const double detJ = DetJ(iPt, iFace);
          const double fx = vX(iPt, iField + (d * 3) *  nbFields);
          const double fy = vX(iPt, iField + (d * 3 + 1) *  nbFields);
          const double fz = vX(iPt, iField + (d * 3 + 2) *  nbFields);
          vXi(iPt * 3 + 0, iField) = detJ * (fx * dXiDX(0, j3 + 0) + fy * dXiDX(0, j3 + 1) + fz * dXiDX(0, j3 + 2));
          vXi(iPt * 3 + 1, iField) = detJ * (fx * dXiDX(1, j3 + 0) + fy * dXiDX(1, j3 + 1) + fz * dXiDX(1, j3 + 2));
          vXi(iPt * 3 + 2, iField) = detJ * (fx * dXiDX(2, j3 + 0) + fy * dXiDX(2, j3 + 1) + fz * dXiDX(2, j3 + 2));
        }
      }
      break;
  }
}

#include "dgTimer.h"
fullMatrix<double> dgMeshJacobian::multDXiDXJRightInterface(int iGroup, int iConnection, const std::vector<int> &faceId, const fullMatrix<double> &vX) const {
  const fullMatrix<double> &dXiDX = _interfaceDXiDX[iGroup][iConnection];
  const fullMatrix<double> &DetJ = _interfaceJacobianDeterminant[iGroup];
  const int dim = _mesh->interfaceVector(iGroup).elementVector(iConnection).functionSpace().dimension;
  const size_t nFace = faceId.size();
  const int nbQP = DetJ.size1();
  const int nbFields = vX.size2() / 3;
  fullMatrix<double> vXi(dim * nbQP , nFace * nbFields);
  static dgTimer &timer = dgTimer::root().child("multDXiDXJRightInterface");
  timer.start();
  switch (dim) {
    case 1 :
      for (size_t iFace = 0; iFace < nFace; ++iFace) {
        int id = faceId[iFace];
        for (int iField = 0; iField < nbFields; iField ++) {
          for (int iPt = 0; iPt < nbQP; iPt ++) {
            const int j3 = (id * nbQP + iPt) * 3;
            const double detJ = DetJ(iPt, id);
            const double fx = vX(iFace * nbQP + iPt, iField * 3);
            const double fy = vX(iFace * nbQP + iPt, iField * 3 + 1);
            const double fz = vX(iFace * nbQP + iPt, iField * 3 + 2);
            vXi(iPt           , iFace * nbFields + iField) = detJ * (dXiDX(0, j3 + 0) * fx + dXiDX(0, j3 + 1) * fy + dXiDX(0, j3 + 2) * fz);
          }
        }
      }
    break;
    case 2 :
      for (size_t iFace = 0; iFace < nFace; ++iFace) {
        int id = faceId[iFace];
        for (int iField = 0; iField < nbFields; iField ++) {
          for (int iPt = 0; iPt < nbQP; iPt ++) {
            const int j3 = (id * nbQP + iPt) * 3;
            const double detJ = DetJ(iPt, id);
            const double fx = vX(iFace * nbQP + iPt, iField * 3);
            const double fy = vX(iFace * nbQP + iPt, iField * 3 + 1);
            const double fz = vX(iFace * nbQP + iPt, iField * 3 + 2);
            vXi(iPt           , iFace * nbFields +  iField) = detJ * (dXiDX(0, j3 + 0) * fx + dXiDX(0, j3 + 1) * fy + dXiDX(0, j3 + 2) * fz);
            vXi(nbQP + iPt    , iFace * nbFields +  iField) = detJ * (dXiDX(1, j3 + 0) * fx + dXiDX(1, j3 + 1) * fy + dXiDX(1, j3 + 2) * fz);
          }
        }
      }
    break;
    case 3 :
      for (size_t iFace = 0; iFace < nFace; ++iFace) {
        for (int iField = 0; iField < nbFields; iField ++) {
          for (int iPt = 0; iPt < nbQP; iPt ++) {
            int id = faceId[iFace];
            const int j3 = (id * nbQP + iPt) * 3;
            const double detJ = DetJ(iPt, id);
            const double fx = vX(iFace * nbQP + iPt, iField * 3);
            const double fy = vX(iFace * nbQP + iPt, iField * 3 + 1);
            const double fz = vX(iFace * nbQP + iPt, iField * 3 + 2);
            vXi(iPt           , iFace * nbFields + iField) = detJ * (dXiDX(0, j3 + 0) * fx + dXiDX(0, j3 + 1) * fy + dXiDX(0, j3 + 2) * fz);
            vXi(nbQP + iPt    , iFace * nbFields + iField) = detJ * (dXiDX(1, j3 + 0) * fx + dXiDX(1, j3 + 1) * fy + dXiDX(1, j3 + 2) * fz);
            vXi(2 * nbQP + iPt, iFace * nbFields + iField) = detJ * (dXiDX(2, j3 + 0) * fx + dXiDX(2, j3 + 1) * fy + dXiDX(2, j3 + 2) * fz);
          }
        }
      }
    break;
  }
  timer.stop();
  return vXi;
}

void dgMeshJacobian::multDXiDXElement (int iGroup, int iElement, const fullMatrix<double> & vXi, fullMatrix<double> &vX) const
{
  _multDXiDXUnrolled(iElement, getElementJacobian(iGroup), vXi, vX);
}

void dgMeshJacobian::multJElement (int iGroup, int iElement, const fullMatrix<double> &vX, fullMatrix<double> &vXi) const
{
  const int nbFields = vXi.size2();
  const int nbQP = vXi.size1();
  const fullMatrix<double> &DetJ = getElementJacobianDeterminant(iGroup);
  for (int iField = 0; iField < nbFields; iField ++) {
    for (int iPt = 0; iPt < nbQP; iPt++) {
      vXi(iPt, iField) = vX(iPt, iField) * DetJ(iPt, iElement);
    }
  }
}
