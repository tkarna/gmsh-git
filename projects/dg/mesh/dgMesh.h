#ifndef _DG_MESH_H_
#define _DG_MESH_H_

#include <string>
#include <vector>
#include <map>

class dgElementVector;
class dgInterfaceVector;

class dgMeshJacobian;
class dgSpaceTransform;

class GModel;

//transitional
class dgGroupCollection;
#include "dgFullMatrix.h"

// only mesh topology

class dgMesh {
  GModel *_gmodel;
  std::vector<dgElementVector*> _elements;
  std::vector<dgElementVector*> _ghostElements;
  std::vector<dgInterfaceVector*> _interfaces;

  std::map<int, std::pair<int, int> > _elementMap; // Num -> (elementVectorId, elementId)
  std::map<int, std::pair<int, int> > _interfaceMap; // Num -> (elementVectorId, elementId) (only or interface elements present in the mesh)
  std::map<int, int> _periodicMap;

  dgSpaceTransform *_spaceTransform;

  mutable std::map<int, dgMeshJacobian*> _jacobian;
 public:
  ~dgMesh();
  dgMesh(GModel *g, int dimension = 0,  const std::vector<std::string> physicalTags = std::vector<std::string>(), bool sequentialLoad = false);
  //transitional constructor
  dgMesh(const dgGroupCollection &groups);
  inline const int nElementVector() const {return _elements.size();}
  inline const int nInterfaceVector() const {return _interfaces.size();}
  inline const int nGhostElementVector() const {return _ghostElements.size();}
  inline dgElementVector &elementVector(size_t i) const {return i < _elements.size() ? *_elements[i] : *_ghostElements[i - _elements.size()];}
  inline const dgElementVector &ghostElementVector(int i) const {return *_ghostElements[i];}
  inline const dgInterfaceVector &interfaceVector(int i) const {return *_interfaces[i];}
  void findInterface(int mElementId, int &iGroup, int &iInterface) const;
  void findElement(int mElementId, int &iGroup, int &iElement) const;
  const dgMeshJacobian &getJacobian(int integrationOrder) const;
  void freeJacobian(int integrationOrder = -1) const;
  inline GModel *gModel() const {return _gmodel;}
  inline const dgSpaceTransform * spaceTransform() const {return _spaceTransform;}
  void setSpaceTransform(dgSpaceTransform &spaceTransform, dgGroupCollection &groups);
  inline int periodicMap(int vertexId) {return _periodicMap[vertexId];}
  
};

#endif
