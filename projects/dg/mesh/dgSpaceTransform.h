#include <cstddef>

#ifndef _DG_SPACE_TRANSFORM_H

#define _DG_SPACE_TRANSFORM_H

#include "MElement.h"
#include "function.h"
class dgCircleTransform;
class dgGroupCollection;


class dgSpaceTransform {
  public:
  // the closureId will be removed when we have a cleaner definition of the 1D space
  virtual void applyJacobian(const MElement &elem,  double xyz[3], double uvw[3], double jac[3][3], double &detJ, int closureId = -1) const = 0;
  virtual ~dgSpaceTransform(){};
};

class dgSpaceTransform1DCurvilinear : public dgSpaceTransform {
  public :
  void applyJacobian(const MElement &elem, double xyz[3], double uvw[3], double jac[3][3], double &detJ, int closureId = -1) const;
};

class dgSpaceTransformSpherical : public dgSpaceTransform {
  double _R;
  dgCircleTransform *_circleTransform;
  public :
  dgSpaceTransformSpherical (double R);
  dgSpaceTransformSpherical (double R, dgGroupCollection *groups);
  double getRadius() const {return _R;};
  double getRmz(double xyz[3]) const;
  void applyJacobian(const MElement &elem, double xyz[3], double uvw[3], double jac[3][3], double &detJ, int closureId = -1) const;

  void rotateCartX(double x, double y, double z, double Alpha, double &xOut, double &yOut, double &zOut);
  void rotateCartXVec(double x, double y, double z, double Alpha, double v1, double v2, double v3, double &xOut, double &yOut, double &zOut);
  void stereo2Cart(double xi, double beta, double Gamma, dataCacheMap *cacheMap, double &xOut, double &yOut, double &zOut);
  void cart2LonLat(double x, double y, double z, double &xOut, double &yOut, double &zOut);
  void lonLatToCartVec(double lon, double lat, double r, double v1, double v2, double v3, double &xOut, double &yOut, double &zOut);
  void cartToStereoVec(double xst, double yst, double zst, double v1, double v2, double v3, dataCacheMap *cacheMap, double &xOut, double &yOut, double &zOut);
};
#endif
