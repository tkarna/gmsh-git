#include "dgNeighbourMap.h"
#include "dgGroupOfElements.h"
dgNeighbourMap::dgNeighbourMap(const dgGroupCollection &groups) {
  _groups = &groups;
  const int nbGroups = groups.getNbElementGroups() + groups.getNbGhostGroups();
  _faces.resize(nbGroups);
  _nbFaces.resize(nbGroups);
  for (int i = 0; i < nbGroups; ++i) {
    const dgGroupOfElements &g = *groups.getElementGroup(i);
    _nbFaces[i] = g.getFunctionSpace().numFaces;
    _faces[i].resize(g.getNbElements() * _nbFaces[i] * 3, -1);
  }
  for (int igf = 0; igf < groups.getNbFaceGroups(); ++igf) {
    const dgGroupOfFaces &f = *groups.getFaceGroup(igf);
    for (int igc = 0; igc < f.nConnection(); ++igc) {
      const dgGroupOfElements &e = f.elementGroup(igc);
      int gid = groups.getElementGroupId(&e);
      const int nbFaces = _nbFaces[gid];
      for (size_t iface = 0; iface < f.size(); ++iface) {
        int iClosure, iNeigh, iSign, iRot;
        iClosure = f.closureId(iface, igc);
        e.getFunctionSpace().breakClosureId(iClosure, iNeigh, iSign, iRot);
        int k = (f.elementId(iface, igc) * nbFaces + iNeigh) * 3;
        _faces[gid][k] = igf;
        _faces[gid][k + 1] = iface;
        _faces[gid][k + 2] = igc;
      }
    }
  }
}

bool dgNeighbourMap::_isValid(int iGroup, int iElem, int iNeigh) const {
  return  !(
    iGroup < 0 || (size_t)iGroup >= _faces.size() ||
    iElem < 0 || (size_t)iElem  >= _faces[iGroup].size() / (3 * _nbFaces[iGroup]) ||
    iNeigh < 0 || iNeigh > _nbFaces[iGroup]
    );
}

void dgNeighbourMap::getFace(int iGroup, int iElem, int iNeigh, int &jGroup, int &jFace, int &jConnection) const {
  if (!_isValid(iGroup, iElem, iNeigh)) {
    jGroup = -1;
    jFace = -1;
    jConnection = -1;
    //Msg::Fatal("Invalid request in dgNeighbourMap");
    return;
  }
  const int nbfaces = _nbFaces[iGroup];
  const int k = (iElem * nbfaces + iNeigh) * 3;
  jGroup = _faces[iGroup][k];
  jFace = _faces[iGroup][k + 1];
  jConnection = _faces[iGroup][k + 2];
}

void dgNeighbourMap::getNeighbour(int iGroup, int iElem, int iNeigh, int &jGroup, int &jElem) const {
  int igf, iface, ic;
  getFace(iGroup, iElem, iNeigh, igf, iface, ic);
  if (igf == -1) {
    jGroup = -1;
    jElem = -1;
    return;
  }
  const dgGroupOfFaces &gf = *_groups->getFaceGroup(igf);
  if (gf.nConnection() != 2) {
    jGroup = -1;
    jElem = -1;
    return;
  }
  jGroup = _groups->getElementGroupId(&gf.elementGroup(1 - ic));
  jElem = gf.elementId(iface, 1 - ic);
}

MElement *dgNeighbourMap::getNeighbour(const MElement *e, int iNeigh) const {
  int gid, eid;
  _groups->find(e, gid, eid);
  if (eid == -1)
    return NULL;
  getNeighbour(gid, eid, iNeigh, gid, eid);
  return eid == -1 ? NULL : _groups->getElementGroup(gid)->getElement(eid);
}

std::vector<std::pair<int, int> > dgNeighbourMap::getNeighbours(int iGroup, int iElement, int iNeigh) const
{
  std::vector<std::pair<int, int> > neighbours;
  int igf, iface, ic;
  getFace(iGroup, iElement, iNeigh, igf, iface, ic);
  if (igf == -1) {
    return neighbours;
  }
  const dgGroupOfFaces &gf = *_groups->getFaceGroup(igf);
  neighbours.reserve(gf.nConnection() - 1);
  for (int iConnection = 0; iConnection < gf.nConnection(); ++iConnection) {
    if (iConnection == ic)
      continue;
    neighbours.push_back(std::make_pair(_groups->getElementGroupId(&gf.elementGroup(iConnection)), gf.elementId(iface, iConnection)));
  }
  return neighbours;
}
