#include "dgInterfaceVector.h"
#include "dgElementVector.h"

#include "GmshMessage.h"
#include "nodalBasis.h"

dgInterfaceVector::dgInterfaceVector(const std::vector<const dgElementVector *> elementVector,  const std::vector<std::vector<std::pair<int, int> > > element, const std::vector<MElement *> mElement, const std::string physicalTag)
{
  _elementVector = elementVector;
  _element = element;
  _mElement = mElement;
  _physicalTag = physicalTag;
  if (_elementVector.size() != _element.size()) {
    Msg::Fatal("inconsistant number of connections");
  }
  for (size_t i = 1; i < element.size(); ++i) {
    if (element[i].size() != element[0].size()) {
      Msg::Fatal("inconsistant number of connections");
    }
  }
  if(nConnection() == 0 || size() == 0) {
    Msg::Fatal("empty interface vectors are not allowed");
  }
  if (_physicalTag.empty() && nConnection() == 1)
    _physicalTag = "none";
  if (_physicalTag.empty() && nConnection() != 1)
    _physicalTag = elementVector[0]->physicalTag();
  _closureRef.resize(nConnection());
  for (int iConnection = 0; iConnection < nConnection(); ++iConnection) {
    _closureRef[iConnection] = elementVector[iConnection]->functionSpace().closureRef[element[iConnection][0].second];
    for (int i = 1; i < size(); ++i) {
      if (_closureRef[iConnection] != elementVector[iConnection]->functionSpace().closureRef[element[iConnection][i].second])
        Msg::Fatal("inconsistant closureRef");
    }
  }
}
