RStereo = 2;
Point(1) = {0,0,0};
Point(2) = {RStereo,0,0};
Point(3) = {0,RStereo,0};
Point(4) = {-RStereo,0,0};
Point(5) = {0,-RStereo,0};
Circle (1) = {2,1,3};
Circle (2) = {3,1,4};
Circle (3) = {4,1,5};
Circle (4) = {5,1,2};
Line Loop (1) = {1, 2, 3, 4};
//Line Loop (2) = {-1, -4, -3, -2};
Field[1] = MathEval;
Field[1].F = "1";
Background Field = 1;
Plane Surface (1) = {1};
Plane Surface (2) = {1};
Mesh.RecombinationAlgorithm = 1;
//Mesh.Algorithm = 8;
Mesh.CharacteristicLengthExtendFromBoundary = 0;
Mesh.CharacteristicLengthFromPoints = 0;
Physical Surface ("Top") = {1};
Physical Surface ("Bottom") = {2};
Physical Line("Seam") = {1, 2, 3, 4};
