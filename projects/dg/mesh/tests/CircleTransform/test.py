from extrude import *
from dgpy import *
from math import *
import time, os, sys
import gmshPartition
from partitionThenExtrude import *


allOrder = [1, 1, 3, 3]
allDimension = [2, 3, 2, 3]
allNames = ["Triangles", "Prisms", "Quadrangles", "Hexahedrons"]
allMeshes = ["aquaplanet_tri", "aquaplanet_tri", "aquaplanet_qua", "aquaplanet_qua"]

R=2
integOrder=25
dataCacheMap.setDefaultIntegrationOrder(integOrder)
integrals = []
for i in range(4):
   order=allOrder[i]
   dimension=allDimension[i]
   
   model = GModel()

   model.load(allMeshes[i]+".geo")
   model.mesh(2)
#   model.setOrderN(allOrder[i], True, False)
   model.save(allMeshes[i]+".msh")

   if (dimension == 3):
     partitionThenExtrude(allMeshes[i], 1)
     name=allMeshes[i]+"_3d.msh"
     model = GModel()
     model.load(name)
     H=3
   else:
     name=allNames[i]
     H=1
   
   groups = dgGroupCollection(model, dimension, order)
   groups.splitGroupsByPhysicalTag();
   circleTransform = dgCircleTransform(groups)
   groups._mesh.setSpaceTransform(circleTransform, groups)
   
   dof = dgDofContainer(groups, 1) 
   
   XYZ = groups.getFunctionCoordinates()
   
   def toIntegrate(FCT, XYZ) :
     for i in range (0,XYZ.size1()) :
       FCT.set(i,0,1)
   
   def toIntegrate2(FCT, XYZ) :
     for i in range (0,XYZ.size1()) :
       FCT.set(i,0,sqrt(XYZ(i,0)**2+XYZ(i,1)**2))
   
   xyzCircle = transformToCircle(circleTransform)
   xyzCircleInteg = functionPrecomputed(groups, integOrder, 3)
   xyzCircleInteg.compute(xyzCircle)
   
   toInteg=functionPython(1, toIntegrate, [xyzCircleInteg])
   integrator = dgFunctionIntegratorInterface(groups, toInteg)
   integral = fullMatrixDouble(1,1)
   integrator.compute("Seam",integral)
   
   toInteg2=functionPython(1, toIntegrate2, [xyzCircleInteg])
   integrator2 = dgFunctionIntegrator(groups, toInteg2)
   integral2 = fullMatrixDouble(1,1)
   integrator2.compute(integral2)
   
   integrator3 = dgFunctionIntegrator(groups, toInteg)
   integral3 = fullMatrixDouble(1,1)
   integrator3.compute(integral3)

   integrator4 = dgFunctionIntegratorInterface(groups, toInteg2)
   integral4 = fullMatrixDouble(1,1)
   integrator4.compute("Seam",integral4)

   integrals.append([])
   integrals[i].append(integral(0,0))
   integrals[i].append(integral2(0,0))
   integrals[i].append(integral3(0,0))
   integrals[i].append(integral4(0,0))
   
   
   def exportFct(FCT,XYZ,xyzCircle):
       for i in range(0,FCT.size1()):
         FCT.set(i,0,XYZ(i,0))
         FCT.set(i,1,XYZ(i,1))
         FCT.set(i,2,XYZ(i,2))
         FCT.set(i,3,xyzCircle(i,0))
         FCT.set(i,4,xyzCircle(i,1))
         FCT.set(i,5,xyzCircle(i,2))
   
   nCompExp=[3,3]
   namesExp=["xyz","xyzCircle"]
   exportFunction=functionPython(sum(nCompExp), exportFct, [XYZ, xyzCircle])
   dof.exportFunctionVtk(exportFunction,'output', 0, 0,"solution",nCompExp,namesExp, xyzCircle)
   
   print ("-------------------------------------------------------")
   print ("%s of order %i. Radius %e" % (allNames[i], order, R))
   print ("Perimeter/Area of seam is",integrals[i][0])
   print ("Analytical value: ",pi*2*R*H)
   print ("Integral of function on domain is",integrals[i][1]/2)
   print ("Analytical value: ",2*pi*R*R*R/3*H)
   print ("Area/volume of the domain is",integrals[i][2]/2)
   print ("Analytical value: ",pi*R*R*H)
   print ("Integral of function on perimter/area is",integrals[i][3])
   print ("Analytical value: ",2*pi*R*R*H)
   print ("-------------------------------------------------------")

error = []
error.append([abs(integrals[0][0]-pi*2*R)/(pi*2*R), abs(integrals[0][1]/2-2*pi*R*R*R/3)/(2*pi*R*R*R/3), abs(integrals[0][2]/2-pi*R*R)/(pi*R*R), abs(integrals[0][3]-2 * pi*R**2)/(2*pi*R**2)])
error.append([abs(integrals[1][0]-pi*2*R*3)/(pi*2*R*3), abs(integrals[1][1]/2-2*pi*R*R*R*3/3)/(2*pi*R*R*R*3/3), abs(integrals[1][2]/2-pi*R*R*3)/(pi*R*R*3), abs(integrals[1][3]-2 * pi*R**2 * 3)/(2*pi*R**2*3)])
error.append([abs(integrals[2][0]-pi*2*R)/(pi*2*R), abs(integrals[2][1]/2-2*pi*R*R*R/3)/(2*pi*R*R*R/3), abs(integrals[2][2]/2-pi*R*R)/(pi*R*R), abs(integrals[2][3]-2 * pi*R**2)/(2*pi*R**2)])
error.append([abs(integrals[3][0]-pi*2*R*3)/(pi*2*R*3), abs(integrals[3][1]/2-2*pi*R*R*R*3/3)/(2*pi*R*R*R*3/3), abs(integrals[3][2]/2-pi*R*R*3)/(pi*R*R*3), abs(integrals[3][3]-2 * pi*R**2*3)/(2*pi*R**2*3)])
print ("Relative error:")

("%s of order %i. Radius %e" % (allNames[i], order, R))

print ("2d order %i triangles    :" % (allOrder[0]), error[0])
print ("3d order %i prisms       :" % (allOrder[1]), error[1])
print ("2d order %i quads        :" % (allOrder[2]), error[2])
print ("3d order %i hexahedrons  :" % (allOrder[3]), error[3])

for i in range(4):
  if (error[i][0] > 1e-13):
     Msg.Fatal("Error too high on perimeter/area integration")
  if (error[i][3] > 1e-13):
     Msg.Fatal("Error too high on function integration on the seam")

for i in range(2):
  if (error[i][1] > 1e-7 or error[i+2][1] > 1e-7):
    Msg.Fatal("Error too high on function integration")
  if (error[i][2] > 1e-12 or error[i+2][2] > 1e-12):
    Msg.Fatal("Error too high on domain integration")
print("Test case successfully passed");
Msg.Exit(0)

