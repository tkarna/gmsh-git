from partition1d import *
from dgpy import *
from extrude import *
from rotateZtoY import *

def partitionThenExtrude(name, nbPart):
   nbLayers = 2
   H = 3.
   
   res=H/nbLayers
   
   def getLayersSigma (element, vertex) :
     z=0
     zTab=[z]
     while (-z)<H:
       z=z-res
       zTab.append(z)
     return zTab
   
   if nbPart>1:
     partStr='_part_%i' % nbPart
   else:
     partStr=''
   partition1d(name+'.msh', name + partStr + '.msh', nbPart)
   extrude(mesh(name + partStr + '.msh'), getLayersSigma).write(name + partStr + '_3d.msh')
