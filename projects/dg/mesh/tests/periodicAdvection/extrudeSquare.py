from extrudePeriodic import *
import sys

mesh2d = "squareXY_4b.msh"
mesh3d = "squareXY3d_4.msh"

h = 0.5
nbLayers = 10

def getLayers(e,v) :
  return [z * h /nbLayers for z in  range(nbLayers + 1)]

extrudePeriodic(mesh(mesh2d), getLayers, "periodicMesh.txt","periodicMesh.txt").write(mesh3d)

