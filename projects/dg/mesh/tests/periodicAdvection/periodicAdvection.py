# Import for python
from dgpy import *
from math import *
import time, os


# File name and extensions
FileName = "squareXY3d_4.msh"
outputDir = "output"




try : os.mkdir(outputDir);
except: 0;
print ('Output Directory : ', outputDir)

# Physical Parameters
Vx=0.1     # Advective Speed In x-direction
Vy=0.1     # Advective Speed In y-direction
Vz=0.      # Advective Speed in z-direction

""" 
     Function for Initial Condition
"""

def InitialCondition(ic, xyz):
  for i in range(0,xyz.size1()):
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    z = xyz.get(i,2)
    val=exp(-(x*x+y*y)*2) * exp(-((z+0.25)*(z+0.25)) * 50)
    ic.set(i,0,val) 

""" 
     Function for Advection parameters
"""

def Velocity(vel, xyz):
  for i in range(0,xyz.size1()): 
    x = xyz.get(i,0)
    y = xyz.get(i,1)
    vel.set(i,0,Vx)
    vel.set(i,1,Vy) 
    vel.set(i,2,Vz) 

print('*** Loading the Mesh and the Model ***')
model = GModel()
model.load(FileName)
order=1
dim=3

print( '*** Create Group Collection ***')
groups=dgGroupCollection(model, dim, order)

print ('*** Define Conservation Law ***')
xyz = groups.getFunctionCoordinates()
vel = functionPython(3, Velocity, [xyz])
nu  = functionConstant([0])
law = dgConservationLawAdvectionDiffusion(vel, nu)

print ('*** Add Boundary Conditions ***')
#law.addBoundaryCondition('Wall', law.new0FluxBoundary())
#law.addBoundaryCondition('cut', law.new0FluxBoundary())
#law.addBoundaryCondition('paste', law.new0FluxBoundary())
law.addBoundaryCondition('top_Surface', law.new0FluxBoundary())
law.addBoundaryCondition('bottom_Surface', law.new0FluxBoundary())

Init = functionPython(1, InitialCondition, [xyz])

print ('*** Set initial condition ***')
solution = dgDofContainer(groups, 1)
solution.L2Projection(Init)

initSol = dgDofContainer(groups, 1)
initSol.copy(solution)

sys = linearSystemPETScBlockDouble()
dof = dgDofManager.newDGBlock (groups, 3, sys)
rk44 = dgERK(law, None, DG_ERK_44)

t=0
dt = 0.05
nbExport = 0 
tic = time.clock()
norm = solution.norm()
if Msg.GetCommRank() == 0 : 
  print('Iter %i Norm %.16e    DT%4.2g   T%8.2g   cpu %6.2g' % (0, norm, dt, t, time.clock() - tic))
solution.exportMsh(outputDir + "/solution-%06d" % (0), t, nbExport)
for i in range (1,401) :
  rk44.iterate(solution, dt, t); 
  norm = solution.norm()
  t = t +dt 
  if ( i%400 ==0 ):
    if Msg.GetCommRank() == 0 : 
      print('Iter %i Norm %.16e    DT%4.2g   T%8.2g   cpu %6.2g' % (i, norm, dt, t, time.clock() - tic))
    nbExport = nbExport + 1 
    solution.exportMsh(outputDir + "/solution-%06d" % (i), t, nbExport)

initSol.axpy(solution,-1)
norm = initSol.norm()
print( "Norm %.16e" % norm)
print (time.clock()-tic)
if Msg.GetCommRank() == 0 :
  f = open('checkNorm.txt', 'w')
  f.write('%.16e' % norm)
  f.close()
Msg.Exit(0)
