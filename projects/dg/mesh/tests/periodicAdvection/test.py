import os, sys

os.system('rm *.msh')
os.system('gmsh -2 -part 4 squareXY.geo -o squareXY_4.msh')
os.system('rundgpy periodicMappingXY.py')
os.system('rundgpy extrudeSquare.py')

os.system('mpirun -np 4 rundgpy periodicAdvection.py')

f = open('checkNorm.txt', 'r')
n = float(f.readline())

error = (n > 2)

if error :
  print("exit with failure")
else :
  print("exit with success")

sys.exit(error)
