L = 1;
Point(1) = {-L, -L, 0};
Point(2) = {L, -L, 0};
Point(3) = {L, L, 0};
Point(4) = {-L, L, 0};
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line Loop(1) = {3, 4, 1, 2};
Plane Surface(1) = {1};

Transfinite Line{1} = 8;
Transfinite Line{2} = 6;
Transfinite Line{3} = 8;
Transfinite Line{4} = 6;
Transfinite Surface{1} = {1,2,3,4};

//Recombine Surface{1};

Physical Surface("Surface")={1};
Physical Line("cutX") = {2};
Physical Line("pasteX") = {4};
Physical Line("cutY") = {3};
Physical Line("pasteY") = {1};
