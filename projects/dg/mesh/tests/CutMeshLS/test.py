from dgpy import *
import os
import time
import math

TIME = function.getTime()
os.system("rm output/*")

def initF(FCT, XYZ):
  for i in range(0,FCT.size1()):
    x = XYZ.get(i,0)
    y = XYZ.get(i,1)
    FCT.set(i, 0, x+y-0.5) 

#-------------------------------------------------
#-- Initialize level set problem
#-------------------------------------------------

print('---- Load Mesh & Geometry')
meshName = "square"
dim = 2
order = 2 #(max for DI Integration)
cut = True
quad = False

if (quad):
     GmshSetOption('Mesh', 'Algorithm', 8.0) 
     GmshSetOption('Mesh','RecombineAll', 1.0)
else:
     GmshSetOption('Mesh', 'Algorithm', 4.0) 
gm=GModel()
gm.load (meshName+".geo")
gm.mesh(dim)
gm.setOrderN(order, True, False)
gm.save(meshName+".msh")

g = GModel()
g.load(meshName+".msh")

groups = dgGroupCollection(g, dim, order)
XYZ = groups.getFunctionCoordinates()
INIT = functionPython(1, initF, [XYZ])
solution = dgDofContainer(groups,1)
solution.interpolate(INIT) 
eval1 = dgFunctionEvaluator(groups, solution.getFunction())
eval2 = dgFunctionEvaluator(groups, INIT)

ls1 = gLevelsetPlane(-1.,0.,0.,0.5)
ls2 = gLevelsetMathEval("x^2+y^2-0.6")

ls3 = gLevelsetEvaluator(eval1)
ls4 = gLevelsetEvaluator(eval2)

i1 = gLevelsetIntersection([ls1,ls2])
i2 = gLevelsetUnion([ls1,ls2])

g2 = g.buildCutGModel(ls2, cut, True)

meshName = "squareMYCUT"
g2.save(meshName+".msh")

#FlGui.instance();
#FlGui.run();

exit(0) //succes

