Mesh.CharacteristicLengthExtendFromBoundary=1;

lc = 0.2;
p = 4;

Point(1) = {0.0, 0.0, 0, lc};
Point(2) = {1, 0.0, 0, lc};
Point(3) = {1, 1, 0, lc};
Point(4) = {0, 1, 0, lc};
Line(1) = {4, 3};
Line(2) = {3, 2};
Line(3) = {2, 1};
Line(4) = {1, 4};
Line Loop(5) = {2, 3, 4, 1};
Plane Surface(6) = {5};

Transfinite Line {1, 3}= p+1 Using Progression 1;
Transfinite Line {2, 4} = p+1 Using Progression 1;
Transfinite Surface {6} Left;
//Recombine Surface(6);

//Physical Line("top") = {1};
//Physical Line("bottom") = {3};
//Physical Line("left") = {4};
//Physical Line("right") = {2};
//Physical Surface("inside") = {6};


