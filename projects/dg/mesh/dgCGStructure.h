#ifndef _DG_CG_STRUCTURE_H
#define _DG_CG_STRUCTURE_H

#include<vector>

class dgGhostStructure;
class dgDofContainer;
class dgGroupCollection;

class dgCGStructure {
  private:
  std::vector<std::vector<int> > _iGroupiElementiVertex2localId;
  std::vector<int> _vertexMultiplicity;
  int _nCGVertices;
  dgGhostStructure *_ghostStructure;
  const dgGroupCollection *_groups;
  void _scatter(std::vector<double> &cg, dgDofContainer *to);
  public :
  dgCGStructure(const dgGroupCollection &groups);
  // to and from can point to the same dgDofContainer
  void mean(dgDofContainer *orig, dgDofContainer *dest);
  void weightedMean(dgDofContainer *orig, dgDofContainer *dest, dgDofContainer *weights);
  void min(dgDofContainer *orig, dgDofContainer *dest);
  void max(dgDofContainer *orig, dgDofContainer *dest);
  void sum(dgDofContainer *orig, dgDofContainer *dest);
  void writeMshVertices(const char *filename);
  void writeCG(dgDofContainer *dof, const char *filename);
  ~dgCGStructure();
};
#endif
