#include <algorithm>
#include "GmshConfig.h"
#include "GmshMessage.h"
#include "dgGroupOfElements.h"
#include "dgDofContainer.h"
#include "MElement.h"
#include "nodalBasis.h"
#include "Numeric.h"
#include "MTriangle.h"
#include "MQuadrangle.h"
#include "MLine.h"
#include "MPoint.h"
#include "GModel.h"
#include "limits.h"
#include "functionGeneric.h"
#ifdef HAVE_MESH
  #include "meshMetric.h"
#endif
#include "dgFunctionEvaluator.h"
#include "dgSpaceTransform.h"
#include "dgIntegrationMatrices.h"
#include "dgMesh.h"
#include "dgMeshJacobian.h"
#include "dgInterfaceVector.h"
#include "dgElementVector.h"
#include "dgExtrusion.h"
#ifdef HAVE_MPI
  #include "mpi.h"
#else
  #include <string.h>
#endif
#include <sstream>
#include <vector>

void dgGroupOfElements::computeMatrices()
{
  _imass.setAll (0.0);
  _mass.setAll (0.0);
  _maxInnerRadius = 0.0;
  _minInnerRadius = 1e20;
  const dgMeshJacobian &meshJacobian = _groups->_mesh->getJacobian(_fs.order * 2 + 1 );
  const dgIntegrationMatrices &_matrices = dgIntegrationMatrices::get(_fs.type, _fs.order * 2 + 1);
  int nbQP = _matrices.integrationPoints().size1();
  int nbNodes = getNbNodes();
  fullMatrix<double> detJacs(nbQP, _elements.size());
  int iGroup = _groups->getElementGroupId(this);
  for (int xi = 0; xi < nbQP; xi ++) {
    for (size_t i = 0; i < _elements.size(); i++) {
      detJacs(xi, i) = meshJacobian.detJElement(iGroup)(xi, i);
    }
  }
  fullMatrix<double> massShape, massProxy, imassProxy;
  massShape.setAsProxy(_mass);
  massShape.reshape(nbNodes * nbNodes, _elements.size());
  massShape.gemm(_matrices.psiPsiW(), detJacs);
  for (size_t i = 0; i < _elements.size(); i++){
    _mass.getBlockProxy(i, massProxy);
    _imass.getBlockProxy(i, imassProxy);
    massProxy.invert (imassProxy);
    _maxInnerRadius = std::max(_maxInnerRadius, meshJacobian.elementInnerRadius(elementVectorId())(i));
    _minInnerRadius = std::min(_minInnerRadius, meshJacobian.elementInnerRadius(elementVectorId())(i));
  }
}

dgGroupOfElements::dgGroupOfElements(dgGroupCollection *groups,
                                     const std::vector<MElement*> &e, 
                                     int polyOrder,
                                     int ghostPartition) :
  _elements(e), 
   _fs(*_elements[0]->getFunctionSpace(polyOrder)),
   _groups(groups),
   _ghostPartition(ghostPartition)
{
  _layerId = -1;
  int nbNodes = _fs.getNumShapeFunctions();
  _imass.resize(nbNodes, nbNodes, e.size(), false);
  _mass.resize(nbNodes, nbNodes, e.size(), false);
  _multirateTag = 0;
  _maxInnerRadius = 0.0;
  _minInnerRadius = 1e20;
  _streamwiseLength.resize(e.size(),1);
  _streamwiseLength.setAll(-1.0);
  _pspgStabilisation.resize(e.size(),1);
  _pspgStabilisation.setAll(-1.0);
  _laplacianOfFunction.resize(e.size(), 6);
  _laplacianOfFunction.setAll(-1000.0);
 
}

// streamwise length
// h = 2 / Sum_i=0^n abs( u/normU dot gradPsi_i)
void dgGroupOfElements::buildStreamwiseLength(int iG, function * fun, dgDofContainer *solution)
{

  int nbNodes = getNbNodes();
  fullMatrix<double> dPsiDx;
  dataCacheMap cacheMap(dataCacheMap::NODE_MODE, _groups);
  if(solution){
//    fullMatrix<double> &solGroup = solution->getGroupProxy(iG);
    cacheMap.setSolutionFunction(solution->getFunction(), solution->getFunctionGradient());
  }
  const dgIntegrationMatrices &_matrices = dgIntegrationMatrices::get(_fs.type, _fs.order * 2 + 1);
  int groupid = _groups->getElementGroupId(this);

  dataCacheDouble &value = *cacheMap.get(fun);
  cacheMap.setGroup(this);
  const dgMeshJacobian &jacobians = cacheMap.getJacobians();
  fullMatrix<double> dXiDX;
  for (size_t iElement=0; iElement<_elements.size() ;iElement++){

    cacheMap.setElement(iElement);
    const fullMatrix<double> &valueMatrix = value.get();
    int nbNodesM = valueMatrix.size1();
   
    //get for each element mean velocity direction
    double u=0.0,v=0.0,w=0.0;
    for (int k=0;k<nbNodesM;++k) {
      u += valueMatrix(k,0);
      v += valueMatrix(k,1);
      w += valueMatrix(k,2);
    }
    u/=nbNodesM;
    v/=nbNodesM;
    w/=nbNodesM;
    double normVel = sqrt(u*u+v*v+w*w);

    double length = -1.0;
    double sum = 0.0;
    if (normVel != 0.0){
      u/=normVel;
      v/=normVel;
      w/=normVel;
  
      //project the velocity onto the gradient of the shape functions
      //HACK: for P1 elements gradient is constant, so take only first QP
      for (int i=0;i<nbNodes;++i) {
	int xi = 0;
	const fullMatrix<double> & dPsi = _matrices.dPsi();
  jacobians.dXiDXElement(groupid, iElement, xi, dXiDX);

	double dPsiDX =  dPsi (0,i) * dXiDX(0, 0) + dPsi (1,i) * dXiDX (1, 0);
	double dPsiDY =  dPsi (0,i) * dXiDX(0, 1) + dPsi (1,i) * dXiDX (1, 1);
	double dPsiDZ =  dPsi (0,i) * dXiDX(0, 2) + dPsi (1,i) * dXiDX (1, 2);
	if(_fs.dimension  == 3){
	 dPsiDX += dPsi (2,i) * dXiDX (2, 0);
	 dPsiDY += dPsi (2,i) * dXiDX (2, 1);
	 dPsiDZ += dPsi (2,i) * dXiDX (2, 2);
	}
	//printf("i shape =%d grad = %g %g %g \n",i,dPsiDX, dPsiDY, dPsiDZ);
	sum += fabs(u*dPsiDX+v*dPsiDY+w*dPsiDZ);
      }
  
      length = 2./sum ;
    }
    //printf("SL length elem %d = %g \n",iElement, length);

    _streamwiseLength(iElement,0) = length;
   
  }

}

void dgGroupOfElements::buildLaplacianOfFunction(int iG, function *fun, dgGroupCollection *groups)
{
#ifdef HAVE_MESH
  _laplacianOfFunction.resize(_elements.size(), fun->getNbCol());
  _laplacianOfFunction.setAll(0.0);

  for (int iF=0; iF< fun->getNbCol(); iF++){
    function *iFun = functionExtractCompNew(fun,iF); 
    dgFunctionEvaluator *evalFunc = new dgFunctionEvaluator(groups, iFun, NULL, true);
    std::vector<double> parameters;
    parameters.push_back(1.e-4);
    meshMetric* metric = new meshMetric(this->getElements());
    metric->addMetric(2, evalFunc, parameters);
  
    for (size_t iElement=0; iElement<_elements.size() ;iElement++){
      MElement *e = _elements[iElement]; 
      double laplElem = 0.0;
      for (int j=0; j< e->getNumVertices(); j++){
	MVertex *v = e->getVertex(j);
	double laplVertex = metric->getLaplacian(v);
	laplElem += laplVertex;
      }
      laplElem /= e->getNumVertices();
     
     _laplacianOfFunction(iElement,iF) = laplElem;
    }
  }
#endif
}

void dgGroupOfElements::copyPrivateDataFrom(const dgGroupOfElements *from) {
  // multirate
  _multirateTag = from->_multirateTag;
  // tag
  _physicalTag = from->_physicalTag;
  _ghostPartition = from->_ghostPartition;
}

const function *dgGroupCollection::getFunctionCoordinates() const
{
  return _coordinatesDofFunction;
}

// this function creates groups of elements
// First, groups of faces are created on every physical group
// of dimension equal to the dimension of the problem minus one
// Then, groups of elements are created 
//  -) Elements of different types are separated
//  -) Then those groups are split into partitions
void dgGroupCollection::buildGroupsOfElements(GModel *model, int dim, int order, const std::vector<std::string> physicalTags)
{
  _dim = (dim == -1 ? model->getDim() : dim);
  _order = order;
  _model = model;
      
  std::vector<GEntity*> entities;
  model->getEntities(entities);
  std::map<int, std::vector<MElement *> > localElements;
  if ((size_t) Msg::GetCommSize() !=  model->getMeshPartitions().size() && !_groupsDefinedOnOneProcessor)
    Msg::Error("Mesh partitions do not match to number of processes %d %d", model->getMeshPartitions().size(), Msg::GetCommSize() );
  std::vector<std::map<int, std::vector<MElement *> > > ghostElements;
  if (!_groupsDefinedOnOneProcessor)
    ghostElements.resize(Msg::GetCommSize());
  std::multimap<MElement*, short> &ghostsCells = _model->getGhostCells();

  // take the elements (real and ghost) belonging to the current process
  for(size_t i = 0; i < entities.size(); i++) {
    GEntity *entity = entities[i];

    // we add only the elements with the requested physicalTags
    bool correctPhysicalTag = physicalTags.empty();
    for(size_t iPhy = 0; ! correctPhysicalTag && iPhy < entity->physicals.size(); iPhy++) {
      const std::string phy = _model->getPhysicalName(_dim, entity->physicals[iPhy]);
      correctPhysicalTag |= std::find(physicalTags.begin(), physicalTags.end(), phy) != physicalTags.end();
    }

    if(entity->dim() == _dim && correctPhysicalTag) {
      for (size_t iel = 0; iel < entity->getNumMeshElements(); iel++) {
        MElement *el = entity->getMeshElement(iel);
        if (order == -1)
          order = el->getPolynomialOrder();
        // if belong to the local partition => add the element
        if(el->getPartition() == Msg::GetCommRank() + 1 || _groupsDefinedOnOneProcessor) {
          localElements[el->getType()].push_back(el); //(keep only one by type)
        } else { // belong to - maybe - neightbour partition => catch the right ghost elems
          std::multimap<MElement*, short>::iterator ghost = ghostsCells.lower_bound(el);
          while(ghost != ghostsCells.end() && ghost->first == el) {
            if(abs(ghost->second) - 1 == Msg::GetCommRank()) {
              ghostElements[el->getPartition() - 1][el->getType()].push_back(el);
            }
            ghost++;
          }
        }
      }
    }
  }
  if(localElements.size() == 0)
    Msg::Fatal("mesh not loaded !");

  std::vector<dgGroupOfElements *> eGroups, gGroups;
  // do a group of elements for every element type in the mesh
  for (std::map<int, std::vector<MElement *> >::iterator it = localElements.begin(); it != localElements.end() ; ++it) {
    dgGroupOfElements *newGroup = new dgGroupOfElements(this, it->second, order);
    eGroups.push_back(newGroup);
  }
  //create ghost groups
  for(size_t i = 0; i < ghostElements.size(); i++) {
    for (std::map<int, std::vector<MElement *> >::iterator it = ghostElements[i].begin(); it != ghostElements[i].end() ; ++it) {
      dgGroupOfElements *gof = new dgGroupOfElements(this, it->second, order, i);
      if (gof->getNbElements())
        gGroups.push_back(gof);
      else
        delete gof;
    }
  }
  setGroups(eGroups, gGroups);
}

class dgMiniConnection 
{
  public:
  int iGroup, iElement, iClosure;
  dgMiniConnection (int iGroup_, int iElement_, int iClosure_)
  {
    iGroup = iGroup_;
    iElement = iElement_;
    iClosure = iClosure_;
  }
  bool operator < (const dgMiniConnection b) const 
  {
    if (iGroup < b.iGroup) return true;
    if (iGroup > b.iGroup) return false;
    if (iElement < b.iElement) return true;
    if (iElement > b.iElement) return false;
    if (iClosure < b.iClosure) return true;
    return false;
  }
};

class dgMiniInterface 
{
  public:
  MElement *originalElement;
  int physicalTag;
  int numVertices;
  std::vector<dgMiniConnection> connections;
  dgMiniInterface()
  {
    physicalTag = -1;
    originalElement = NULL;
  }
  bool isFullGhost(dgGroupCollection &groups)
  {
    bool fullGhost = true;
    for (size_t i = 0; i<connections.size(); i++) {
      fullGhost &= (connections[i].iGroup >= groups.getNbElementGroups());
    }
    return fullGhost;
  }
};

static std::vector<dgMiniInterface> *_createMiniInterfaces(dgGroupCollection &groups) 
{
  std::vector<GEntity*> entities;
  groups.getModel()->getEntities(entities);
  std::map<MVertex*, dgMiniInterface> vertexInterfaces;
  std::map<MEdge, dgMiniInterface, Less_Edge> edgeInterfaces;
  std::map<MFace, dgMiniInterface, Less_Face> faceInterfaces;
  int dim = groups.getDim(); //groups.getElementGroup(0)->getElement(0)->getDim(); //TODO check validity
  // 1) get tag of existing interfaces
  for(size_t i = 0; i < entities.size(); i++){
    GEntity *entity = entities[i];
    if(entity->dim() == dim-1){
      for(size_t j = 0; j < entity->physicals.size(); j++){
        int physicalTag = entity->physicals[j];
        for (size_t k = 0; k < entity->getNumMeshElements(); k++) {
          MElement *element = entity->getMeshElement(k);
          dgMiniInterface *interface;
          switch(dim) {
            case 1: interface = &vertexInterfaces[element->getVertex(0)]; break;
            case 2: interface = &edgeInterfaces[element->getEdge(0)]; break;
            case 3: interface = &faceInterfaces[element->getFace(0)]; break;
            default : throw;
          }
          interface->physicalTag = physicalTag;
          interface->originalElement = element;
        }
      }
    }
  }
  // 2) build new interfaces
  int iClosure;
  for (int iGroup = 0; iGroup < groups.getNbElementGroups()+groups.getNbGhostGroups(); iGroup++) {
    dgGroupOfElements &group = *groups.getElementGroup(iGroup);
    for (size_t iElement = 0; iElement < group.getNbElements(); iElement++) {
      MElement &element = *group.getElement(iElement);
      switch(dim) {
        case 1: 
          for (int iVertex = 0; iVertex < element.getNumPrimaryVertices(); iVertex++) {
            iClosure = element.getFunctionSpace(-1)->getClosureId(iVertex);
            vertexInterfaces[element.getVertex(iVertex)].connections.push_back(dgMiniConnection(iGroup,iElement,iClosure));
          }
          break;
        case 2:
          for (int iEdge = 0; iEdge < element.getNumEdges(); iEdge++) {
            std::map<MEdge, dgMiniInterface, Less_Edge>::iterator it =
              edgeInterfaces.insert(std::pair<MEdge,dgMiniInterface>(element.getEdge(iEdge),dgMiniInterface())).first;
            int iEdge_, sign;
            element.getEdgeInfo(it->first, iEdge_, sign);
            iClosure = element.getFunctionSpace(-1)->getClosureId(iEdge,sign);
            it->second.connections.push_back(dgMiniConnection(iGroup,iElement,iClosure));
          }
          break;
        case 3:
          for (int iFace = 0; iFace < element.getNumFaces(); iFace++) {
            std::map<MFace, dgMiniInterface, Less_Face>::iterator it =
             faceInterfaces.insert(std::pair<MFace,dgMiniInterface>(element.getFace(iFace),dgMiniInterface())).first;
            int iFace_, sign, rotation;
            element.getFaceInfo(it->first, iFace_, sign, rotation);
            iClosure = element.getFunctionSpace (-1)->getClosureId (iFace, sign, rotation);
            it->second.connections.push_back(dgMiniConnection(iGroup,iElement,iClosure));
          }
          break;
        default : throw;
      }
    }
  }
  std::vector<dgMiniInterface> *interfaces = new std::vector<dgMiniInterface>;
  switch(dim) {
    case 1: 
      interfaces->reserve(vertexInterfaces.size());
      for(std::map<MVertex*, dgMiniInterface>::iterator it = vertexInterfaces.begin(); it != vertexInterfaces.end(); it++) {
        it->second.numVertices = 1;
        interfaces->push_back(it->second);
      }
      break;
    case 2: 
      interfaces->reserve(edgeInterfaces.size());
      for(std::map<MEdge, dgMiniInterface, Less_Edge>::iterator it = edgeInterfaces.begin(); it != edgeInterfaces.end(); it++) {
        it->second.numVertices = it->first.getNumVertices();
        interfaces->push_back(it->second);
      }
      break;
    case 3: 
      interfaces->reserve(faceInterfaces.size());
      for(std::map<MFace, dgMiniInterface,Less_Face>::iterator it = faceInterfaces.begin(); it != faceInterfaces.end(); it++) {
        it->second.numVertices = it->first.getNumVertices();
        interfaces->push_back(it->second);
      }
      break;
  }
  for (size_t i = 0; i < interfaces->size(); i++) {
    std::sort((*interfaces)[i].connections.begin(), (*interfaces)[i].connections.end());
  }
  return interfaces;
}

class dgGroupKey : public std::binary_function<dgMiniInterface, dgMiniInterface, bool> {
  const dgGroupCollection &_groups;
  public:
  dgGroupKey (const dgGroupCollection &groups) : _groups(groups){}
  bool operator ()(const dgMiniInterface &i1, const dgMiniInterface &i2) const
  {
    // 1 sort by number of connections
    if (i1.connections.size() < i2.connections.size()) return true;
    if (i1.connections.size() > i2.connections.size()) return false;
    // 2 sort by physical tag
    if (i1.physicalTag < i2.physicalTag) return true;
    if (i1.physicalTag > i2.physicalTag) return false;
    // 3 sort by groups of elements
    for (size_t i = 0; i < i1.connections.size(); i++) {
      if (i1.connections[i].iGroup < i2.connections[i].iGroup) return true;
      if (i1.connections[i].iGroup > i2.connections[i].iGroup) return false;
    }
    // 4 sort by reference Closure
    if (i1.connections.size()>0) {
      const dgMiniConnection &c1 = i1.connections[0];
      const dgMiniConnection &c2 = i2.connections[0];
      int closureRef1 = _groups.getElementGroup(c1.iGroup)->getFunctionSpace().closureRef[c1.iClosure];
      int closureRef2 = _groups.getElementGroup(c2.iGroup)->getFunctionSpace().closureRef[c2.iClosure];
      if (closureRef1 < closureRef2) return true;
      if (closureRef1 > closureRef2) return false;
    }
    return false;
  }
};

double dgGroupCollection::maxInnerRadius() const
{
  double maxRadius = 0.0;
  for (size_t iG=0; iG < _elementGroups.size(); iG++){
    dgGroupOfElements* gof = _elementGroups[iG];
    double rad = gof->maxInnerRadius();
    maxRadius = std::max(maxRadius,  rad);
  }
  return maxRadius;
}

double dgGroupCollection::minInnerRadius() const
{
  double minRadius = 1e20;
  for (size_t iG=0; iG < _elementGroups.size(); iG++){
    dgGroupOfElements* gof = _elementGroups[iG];
    double rad = gof->minInnerRadius();
    minRadius = std::min(minRadius,  rad);
  }
  return minRadius;
}

void dgGroupCollection::buildStreamwiseLength(function * fun, dgDofContainer *solution)
{
  for (size_t iG=0; iG < _elementGroups.size(); iG++){
    dgGroupOfElements* gElem = _elementGroups[iG];
    gElem->buildStreamwiseLength(iG, fun, solution);
  }
}

void dgGroupCollection::buildLaplacianOfFunction(function *fun)
{
  for (size_t iG=0; iG < _elementGroups.size(); iG++){
    dgGroupOfElements* gElem = _elementGroups[iG];
    gElem->buildLaplacianOfFunction(iG, fun, this);
  }
}

dgGroupOfFaces::dgGroupOfFaces (dgGroupCollection &groups, int interfaceVector, int pOrder, int id, const std::vector<int>& interfacesSubset) :
  _interfaceVector(groups._mesh->interfaceVector(interfaceVector))
{
  _iInterfaceVector = interfaceVector;
  _id = id;
  for (int i = 0; i < _interfaceVector.nConnection(); i++) {
    int iGroup, iEl;
    groups._mesh->findElement(_interfaceVector.elementVector(i).element(_interfaceVector.elementId(0, i)).getNum(), iGroup, iEl);
    _elementGroup.push_back(groups.getElementGroup(iGroup));
  }
  MElement &el = _interfaceVector.elementVector(0).element(_interfaceVector.elementId(0, 0));
  _fsFace = BasisFactory::getNodalBasis(el.getFunctionSpace(pOrder)->closures[_interfaceVector.closureId(0, 0)].type);
  _orientationTag = ORIENTATION_UNDEFINED;
  if (interfacesSubset.size()==0){
    for (int i = 0; i < _interfaceVector.size(); i++) {
      _interfacesSubset.push_back(i);
    }
  }else{
    _interfacesSubset=interfacesSubset;
  }
}

double dgGroupOfElements::elementVolume(const dgMeshJacobian &jac, int iElement) const
{
  return jac.elementVolume(elementVectorId())(iElement);
}

double dgGroupOfFaces::interfaceSurface(const dgMeshJacobian &jac, int iFace) const
{
  return jac.interfaceSurface(interfaceVectorId())(interfaceId(iFace));
}

void dgGroupOfFaces::normal(const dgMeshJacobian &jac, int iFace, int iConnection, fullMatrix<double> &n) const
{
  jac.normal(interfaceVectorId(), iConnection).getBlockProxy(interfaceId(iFace), n);
}

void dgGroupCollection::_buildParallelStructure()
{
  std::vector<std::vector<bool> > isDGGhost(getNbGhostGroups());
  for (int i = 0; i< getNbGhostGroups(); ++i) {
    isDGGhost[i].resize(getGhostGroup(i)->getNbElements(), false);
  }
  for (int i = 0; i < getNbFaceGroups(); ++i) {
    const dgGroupOfFaces &faces = *getFaceGroup(i);
    for (int iC = 0; iC < faces.nConnection(); ++ iC) {
      if (faces.elementGroup(iC).getGhostPartition() < 0)
        continue;
      int groupId = getElementGroupId(&faces.elementGroup(iC)) - getNbElementGroups();
      for (size_t iF = 0; iF < faces.size(); ++iF) {
        isDGGhost[groupId][faces.elementId(iF, iC)] = true;
      }
    }
  }

  // build the ghosts structure
  int *nGhostElements = new int[Msg::GetCommSize()];
  int *nParentElements = new int[Msg::GetCommSize()];
  for (int i=0;i<Msg::GetCommSize();i++) {
    nGhostElements[i] = 0;
    nParentElements[i] = 0;
  }
  _elementsToReceive.clear();
  _elementsToReceive.resize(Msg::GetCommSize());
  _elementsToReceiveDG.clear();
  _elementsToReceiveDG.resize(Msg::GetCommSize());
  for (int i = 0; i< getNbGhostGroups(); i++){
    const dgGroupOfElements &group = *getGhostGroup(i);
    const int partId = group.getGhostPartition();
    nGhostElements[partId] += group.getNbElements();
    for (size_t j = 0; j < group.getNbElements(); ++j) {
      if (isDGGhost[i][j]) {
        _elementsToReceiveDG[partId].push_back(std::make_pair(getNbElementGroups() + i, j));
      }
      _elementsToReceive[partId].push_back(std::make_pair(getNbElementGroups() + i, j));
    }
  }
  #ifdef HAVE_MPI
  if(_groupsDefinedOnOneProcessor){
    nParentElements[0] = nGhostElements[0];
  }
  else{
    MPI_Alltoall(nGhostElements,1,MPI_INT,nParentElements,1,MPI_INT,MPI_COMM_WORLD); 
  }
  #else
  nParentElements[0] = nGhostElements[0];
  #endif
  int *shiftSend = new int[Msg::GetCommSize()];
  int *shiftRecv = new int[Msg::GetCommSize()];
  int *curShiftSend = new int[Msg::GetCommSize()];
  int totalSend = 0,totalRecv = 0;
  for(int i= 0; i<Msg::GetCommSize(); i++){
    shiftSend[i] = (i==0 ? 0 : shiftSend[i-1] + nGhostElements[i-1]);
    curShiftSend[i] = shiftSend[i];
    shiftRecv[i] = (i==0 ? 0 : shiftRecv[i-1] + nParentElements[i-1]);
    totalSend += nGhostElements[i];
    totalRecv += nParentElements[i];
  }

  int *idSend = new int[totalSend];
  int *idRecv = new int[totalRecv];
  for (int i = 0; i<getNbGhostGroups(); i++){
    dgGroupOfElements *group = getGhostGroup(i);
    const int part = group->getGhostPartition();
    for (size_t j = 0; j < group->getNbElements(); j++){
      int id = group->getElement(j)->getNum() + 1;
      if (isDGGhost[i][j])
        id = -id;
      idSend[curShiftSend[part]++] = id;
    }
  }
  #ifdef HAVE_MPI
  if(_groupsDefinedOnOneProcessor){
    memcpy(idRecv,idSend,nParentElements[0]*sizeof(int));
  }
  else{
    MPI_Alltoallv(idSend,nGhostElements,shiftSend,MPI_INT,idRecv,nParentElements,shiftRecv,MPI_INT,MPI_COMM_WORLD);
  }
  #else
  memcpy(idRecv,idSend,nParentElements[0]*sizeof(int));
  #endif
  //create a Map elementNum :: group, position in group
  _elementsToSend.clear();
  _elementsToSendDG.clear();
  _elementsToSend.resize(Msg::GetCommSize());
  _elementsToSendDG.resize(Msg::GetCommSize());
  for(int i = 0; i<Msg::GetCommSize();i++){
    _elementsToSend[i].resize(nParentElements[i]);
    _elementsToSendDG[i].reserve(nParentElements[i]);
    for(int j=0;j<nParentElements[i];j++){
      int num = idRecv[shiftRecv[i]+j];
      bool dgGhost = (num < 0);
      std::pair<int, int> elementRef = _elementMap[abs(num) - 1];
      _elementsToSend[i][j] = elementRef;
      if (dgGhost)
        _elementsToSendDG[i].push_back(elementRef);
    }
  }
  delete []idRecv;
  delete []idSend;
  delete []curShiftSend;
  delete []shiftSend;
  delete []shiftRecv;
  delete []nGhostElements;
  delete []nParentElements;
}

void dgGroupCollection::setGroups(std::vector<dgGroupOfElements*> newGroups)
{
  setGroups(newGroups, _ghostGroups);
}

void dgGroupCollection::setGroups(std::vector<dgGroupOfElements*> newGroups, std::vector<dgGroupOfElements*> newGhostGroups)
{
  for(size_t i=0;i<_elementGroups.size();i++) {
    if (std::find(newGroups.begin(), newGroups.end(), _elementGroups[i]) == newGroups.end())
      delete _elementGroups[i];
  }
  for(size_t i=0;i<_ghostGroups.size();i++) {
    if (std::find(newGhostGroups.begin(), newGhostGroups.end(), _ghostGroups[i]) == newGhostGroups.end())
      delete _ghostGroups[i];
  }
  _elementGroups = newGroups;
  _ghostGroups = newGhostGroups;
  _groupId.clear();
  for (size_t i=0; i < _elementGroups.size(); i++)
    _groupId[_elementGroups[i]] = i;
  for (size_t i=0; i < _ghostGroups.size(); i++)
    _groupId[_ghostGroups[i]] = i + _elementGroups.size();
  _elementMap.clear();
  for (int iG=0; iG < getNbElementGroups() + getNbGhostGroups(); iG++) {
    const dgGroupOfElements &group = *getElementGroup(iG);
    for (size_t iE=0; iE < group.getNbElements(); iE++)
      _elementMap[group.getElement(iE)->getNum()] = std::make_pair(iG,iE);
  }
  if (_mesh)
    delete _mesh;
  _mesh = new dgMesh(*this);
  for (int i = 0; i < getNbElementGroups() + getNbGhostGroups(); ++i) {
    getElementGroup(i)->setElementVectorId(i);
    getElementGroup(i)->computeMatrices();
  }
  // interfaces
  for(size_t i = 0; i < _faceGroups.size(); ++i)
    delete _faceGroups[i];
	_faceGroups.clear();
  _interfaceGroupsForGroup.clear();
  _faceMap.clear();
  int pOrder = _elementGroups[0]->getOrder();
  for (int i = 0; i< _mesh->nInterfaceVector(); i++) {
    _faceGroups.push_back (new dgGroupOfFaces (*this, i, pOrder, i) );
  }
  _interfaceGroupsForGroup.resize(_elementGroups.size());
  for(int iGroup=0;iGroup<getNbFaceGroups();iGroup++){
    dgGroupOfFaces *gf=getFaceGroup(iGroup);
    int newId;
    int oldId=INT_MAX;
    for(int i=0;i<gf->nConnection();i++) {
      const dgGroupOfElements *ge = &gf->elementGroup(i);
      newId = getElementGroupId(ge);
      if(ge->getGhostPartition()==-1 && newId!=oldId) _interfaceGroupsForGroup[newId].push_back(gf);
      oldId=newId;
    }
  }
  for (size_t iG=0; iG < _faceGroups.size(); iG++) {
    for (size_t iF=0; iF < _faceGroups[iG]->size(); iF++){
      MElement *o = _faceGroups[iG]->meshInterfaceElement(iF);
      if (o) {
        _faceMap[o->getNum()] = std::make_pair(iG, iF);
      }
    }
  }
  _buildParallelStructure();
  if(_coordinatesDof)
    delete _coordinatesDof;
  _coordinatesDof = new dgDofContainer(*this, 3);
  copyVectorCoordinates();
  _coordinatesDofFunction->setDof(_coordinatesDof);
}

void dgGroupCollection::copyVectorCoordinates() {
  fullMatrix<double> coord;
  for (int i = 0; i < getNbElementGroups() + getNbGhostGroups(); i++) {
    dgGroupOfElements *group = getElementGroup(i);
    const nodalBasis &fsGeo = *group->getElement(0)->getFunctionSpace();
    const nodalBasis &fs = group->getFunctionSpace();
    fullMatrix<double> phiGeo(fsGeo.points.size1(), fs.points.size1());
    for (int j = 0; j < fs.points.size1(); ++j) {
      fsGeo.f(fs.points(j, 0), fs.points.size2() > 1 ? fs.points(j, 1) : 0, fs.points.size2() > 2 ? fs.points(j, 2) : 0, &phiGeo(0, j));
    }
    dgFullMatrix<double> &groupProxy = _coordinatesDof->getGroupProxy(i);
    const dgFullMatrix<double> &coordEV = _mesh->elementVector(group->elementVectorId()).coordinates();
    fullMatrix<double> coordEVp;
    for (size_t j = 0; j < group->getNbElements(); ++ j) {
      groupProxy.getBlockProxy(j, coord);
      coordEV.getBlockProxy(j, coordEVp);
      coord.setAll(0.);
      for (int k = 0; k < coordEVp.size1(); ++ k) {
        for (int l = 0; l < fs.points.size1(); ++l) {
          coord(l,0) += coordEVp(k, 0) * phiGeo(k, l);
          coord(l,1) += coordEVp(k, 1) * phiGeo(k, l);
          coord(l,2) += coordEVp(k, 2) * phiGeo(k, l);
        }
      }
    }
  }
}

void dgGroupCollection::updateCoordinates(const function *xyzF)
{
  dataCacheMap cache(dataCacheMap::NODE_MODE, this);
  for (int i = 0; i < getNbElementGroups(); ++i) {
    const dgGroupOfElements &group = *getElementGroup(i);
    int nNode = group.getNbNodes();
    cache.setGroup(&group);
    dgFullMatrix<double> &coordVector = _mesh->elementVector(group.elementVectorId()).coordinates();
    dataCacheDouble &coordCache = *cache.get(xyzF);
    for (size_t j = 0; j < group.getNbElements(); ++j) {
      cache.setElement(j);
      const fullMatrix<double> &coordF = coordCache.get();
      for (int k = 0 ; k < nNode; ++k) {
        coordVector(k, j * 3 + 0) =  coordF(k, 0);
        coordVector(k, j * 3 + 1) =  coordF(k, 1);
        coordVector(k, j * 3 + 2) =  coordF(k, 2);
      }
    }
  }
  copyVectorCoordinates();
  _mesh->freeJacobian(-1);
  for (int i = 0; i < getNbElementGroups() + getNbGhostGroups(); ++i) {
    getElementGroup(i)->computeMatrices();
  }
}

void dgGroupCollection::updateCoordinates(dgDofContainer* zCoordDof)
{
  zCoordDof->scatter();
  fullMatrix<double> groupCoords, newCoords;
  for (int i = 0; i < getNbElementGroups() + getNbGhostGroups(); i++) {
    dgGroupOfElements *group = getElementGroup(i);
    dgFullMatrix<double> &groupCProxy = _coordinatesDof->getGroupProxy(i);
    dgFullMatrix<double> &newCProxy = zCoordDof->getGroupProxy(i);
    const dgFullMatrix<double> &evV = _mesh->elementVector(i).coordinates();
    fullMatrix<double> evVProxy;
    for (size_t j = 0; j < group->getNbElements(); ++ j) {
      groupCProxy.getBlockProxy(j, groupCoords);
      newCProxy.getBlockProxy(j, newCoords);
      evV.getBlockProxy(j, evVProxy);
      MElement *e = group->getElement(j);
      for (int iN = 0; iN < e->getNumVertices(); ++ iN) {
//         groupCoords(iN,0) = v->x();
//         groupCoords(iN,1) = v->y();
        groupCoords(iN,2) = newCoords(iN,0);
        evVProxy(iN, 2) = newCoords(iN,0);
      }
    }
  //}
  }

  // recompute matrices
  _mesh->freeJacobian(-1);
  for (int i = 0; i < getNbElementGroups() + getNbGhostGroups(); ++i) {
    getElementGroup(i)->computeMatrices();
  }
}

void dgGroupCollection::setCoordinates(dgDofContainer* xyzCoordDof)
{
  if(xyzCoordDof->getNbFields() != 6)
    Msg::Error("dgGroupCollection::setCoordinates require a dof of 6 fields. The original mesh nodes "
               "are modified as f0 * x + f1, f2 * y + f3, f4 * z + f5.");
  fullMatrix<double> coord, mod;
  for (int i = 0; i < getNbElementGroups() + getNbGhostGroups(); i++) {
    dgGroupOfElements *group = getElementGroup(i);
    const nodalBasis &fsGeo = *group->getElement(0)->getFunctionSpace();
    const nodalBasis &fs = group->getFunctionSpace();
    fullMatrix<double> phiGeo(fsGeo.points.size1(), fs.points.size1());
    for (int j = 0; j < fs.points.size1(); ++j) {
      fsGeo.f(fs.points(j, 0), fs.points.size2() > 1 ? fs.points(j, 1) : 0, fs.points.size2() > 2 ? fs.points(j, 2) : 0, &phiGeo(0, j));
    }
    dgFullMatrix<double> &groupProxyOriginal = _coordinatesDof->getGroupProxy(i);
    dgFullMatrix<double> &groupProxyModified = xyzCoordDof->getGroupProxy(i);
    const dgFullMatrix<double> &coordEV = _mesh->elementVector(group->elementVectorId()).coordinates();
    fullMatrix<double> coordEVp;
    for (size_t iEl = 0; iEl < group->getNbElements(); ++ iEl) {
      groupProxyOriginal.getBlockProxy(iEl, coord);
      groupProxyModified.getBlockProxy(iEl, mod);
      coordEV.getBlockProxy(iEl, coordEVp);
      coord.setAll(0.);
      for (int l = 0; l < fs.points.size1(); ++l) {
/*        for (int k = 0; k < coordEVp.size1(); ++ k) {
          coord(l,0) += coord(k, 0) * phiGeo(k, l);
          coord(l,1) += coord(k, 1) * phiGeo(k, l);
          coord(l,2) += coord(k, 2) * phiGeo(k, l);
        }*/
        coord(l,0) = coordEVp(l,0) * mod(l,0) + mod(l,1);
        coordEVp(l,0) = coord(l,0);
        coord(l,1) = coordEVp(l,1) * mod(l,2) + mod(l,3);
        coordEVp(l,1) = coord(l,1);
        coord(l,2) = coordEVp(l,2) * mod(l,4) + mod(l,5);
        coordEVp(l,2) = coord(l,2);
      }
    }
  }

  // recompute matrices
  _mesh->freeJacobian(-1);
  for (int i = 0; i < getNbElementGroups() + getNbGhostGroups(); ++i) {
    getElementGroup(i)->computeMatrices();
  }
}


// Split the groups of elements into vertical layers 
void dgGroupCollection::splitGroupsByVerticalLayer(dgExtrusion extrusion, std::vector<std::string> botLevelTags, std::vector<std::string> topLevelTags) {
  Msg::Info("Splitting Groups per vertical layers");
  std::vector<dgMiniInterface> *miniInterfaceV=_createMiniInterfaces(*this);
  std::vector< dgGroupOfElements* >newGroups;// indexed by newGroupId
  std::vector<std::vector<bool> >isProcessed;
  isProcessed.resize(getNbElementGroups());
  std::vector<std::vector<std::vector<std::pair<int,int> > > >elementToNeighbors;
  elementToNeighbors.resize(getNbElementGroups());
  for(int iGroup=0;iGroup<getNbElementGroups();iGroup++){
    isProcessed[iGroup].assign(getElementGroup(iGroup)->getNbElements(),0);
    elementToNeighbors[iGroup].resize(getElementGroup(iGroup)->getNbElements());
  }
  // Build elementToNeighbors table (needed to have random access to the neighbors)
  for(size_t iInterface=0;iInterface<miniInterfaceV->size();iInterface++){
    dgMiniInterface &interface=miniInterfaceV->at(iInterface);
    for(size_t iConn=0;iConn<interface.connections.size();iConn++){
      int gIdi=interface.connections[iConn].iGroup;
      int eIdi=interface.connections[iConn].iElement;
      if (gIdi >= getNbElementGroups()) // ignore ghost groups
        continue;
      for(size_t jConn=0;jConn<iConn;jConn++){
        int gIdj=interface.connections[jConn].iGroup;
        int eIdj=interface.connections[jConn].iElement;
        if (gIdj >= getNbElementGroups())
          continue;
        size_t iCol0, iLayer0, iCol1, iLayer1;
        extrusion.mapElementID2ColumnPosition(gIdi, eIdi, iCol0, iLayer0);
        extrusion.mapElementID2ColumnPosition(gIdj, eIdj, iCol1, iLayer1);
        if (iCol0 != iCol1){
          continue;
        }
        elementToNeighbors[gIdi][eIdi].push_back(std::pair<int,int>(gIdj,eIdj));
        elementToNeighbors[gIdj][eIdj].push_back(std::pair<int,int>(gIdi,eIdi));
      }
    }
  }
  std::vector< std::pair<int,int> > oldGroupElemIds;
  std::vector< std::pair<int,int> > newGroupElemIds;
  std::vector< MElement* > newGroupElems;
  int currentNewGroupId=0;
  int parentGroupId = -1; // the old group that is being split
  while(true) {
    newGroupElemIds.clear();
    newGroupElems.clear();
    if(currentNewGroupId==0) {
      // create first element groups
      for(size_t iInterface = 0; iInterface < miniInterfaceV->size(); iInterface++){
        dgMiniInterface interface = (*miniInterfaceV)[iInterface];
        for(size_t iConn=0;iConn<interface.connections.size();iConn++){
          int gId=interface.connections[iConn].iGroup;
          int eId=interface.connections[iConn].iElement;
          bool botLevelTagFound = false;
          for (size_t i = 0; i < botLevelTags.size(); i++)
            if (_model->getPhysicalName(2, interface.physicalTag) == botLevelTags.at(i) ) {
              botLevelTagFound = true;
              break;
            }
          if (botLevelTagFound && gId < getNbElementGroups()) { // ignore ghost groups
            newGroupElems.push_back(getElementGroup(gId)->getElement(eId));
            newGroupElemIds.push_back(std::pair<int,int>(gId,eId));
            isProcessed[gId][eId]=true;
            parentGroupId = gId;
          }
        }
      }
      if (newGroupElems.empty()) {
        std::string errMsg = "No boundary faces found with the given physical tag(s): ";
        for (size_t i = 0; i < botLevelTags.size(); i++)
          errMsg += botLevelTags.at(i) += " ";
        Msg::Fatal(errMsg.c_str());
      }
    }
    else {
      // find neighbors in next level
      for (size_t iElems = 0; iElems<oldGroupElemIds.size();iElems++ ) {
        int gId = oldGroupElemIds.at(iElems).first;
        int eId = oldGroupElemIds.at(iElems).second;
        for (size_t iNeighbors = 0; iNeighbors < elementToNeighbors[gId][eId].size(); iNeighbors++) {
          int nGId = elementToNeighbors[gId][eId].at(iNeighbors).first;
          int nEId = elementToNeighbors[gId][eId].at(iNeighbors).second;
          if (!isProcessed[nGId][nEId]) {
            if (nGId != parentGroupId)
              Msg::Fatal("Next element belongs to another group (%d) than the previous (%d)",nGId,parentGroupId);
            newGroupElemIds.push_back(std::pair<int,int>(nGId,nEId));
            newGroupElems.push_back(getElementGroup(nGId)->getElement(nEId));
            isProcessed[nGId][nEId]=true;
          }
        }
      }
    }
    if (newGroupElems.empty())
      break;
    // assuming that single group is being split
    // otherwise ghost groups need to be regenerated
    dgGroupOfElements* oldGroup = getElementGroup(parentGroupId); // only for one original group
    dgGroupOfElements* newGroup = new dgGroupOfElements(this, newGroupElems,oldGroup->getOrder());
    newGroup->copyPrivateDataFrom(oldGroup);
    newGroups.push_back(newGroup);
    newGroup->_layerId = currentNewGroupId;
    currentNewGroupId++;
    oldGroupElemIds = newGroupElemIds;
  }
  
  _maxGroupLayerId=currentNewGroupId-1;
  delete miniInterfaceV;
  setGroups(newGroups);
  // hack
  for (size_t i = 0; i < _faceGroups.size(); ++i){
    if (_faceGroups[i]->nConnection() == 2){
      if (_faceGroups[i]->elementGroup(0).elementVectorId() != _faceGroups[i]->elementGroup(1).elementVectorId()){
        _faceGroups[i]->setOrientationTag(dgGroupOfFaces::ORIENTATION_HORIZONTAL);
      }
      else{
        _faceGroups[i]->setOrientationTag(dgGroupOfFaces::ORIENTATION_VERTICAL);
      }
    }
    else
    {
      bool hBndTagFound = false;
      for (size_t j = 0; j < topLevelTags.size(); j++){
        if (_faceGroups[i]->physicalTag() == topLevelTags.at(j)) {
          hBndTagFound = true;
          break;
        }
      }
      for (size_t j = 0; j < botLevelTags.size(); j++){
        if (_faceGroups[i]->physicalTag() == botLevelTags.at(j)) {
          hBndTagFound = true;
          break;
        }
      }
      if (hBndTagFound){
        _faceGroups[i]->setOrientationTag(dgGroupOfFaces::ORIENTATION_HORIZONTAL);
      }
      else{
        _faceGroups[i]->setOrientationTag(dgGroupOfFaces::ORIENTATION_VERTICAL);
      }
    }
  }
}

void dgGroupCollection::splitGroupsByPhysicalTag()
{
  std::map<int,int> num2tag;
  int dim = _dim;
  std::map<int, std::vector<GEntity*> > groups[4];
  _model->getPhysicalGroups(groups);

  for (std::map<int, std::vector<GEntity*> >::iterator it = groups[dim].begin(); it != groups[dim].end(); it++) {
    std::vector<GEntity*> entities = it->second;
    for (size_t iEnt = 0; iEnt < entities.size(); iEnt++) {
      GEntity* entity = entities[iEnt];
      for(size_t iElem = 0; iElem < entity->getNumMeshElements(); iElem++){
        MElement *e = entity->getMeshElement(iElem);
        num2tag [e->getNum()] = it->first;
      }
    }
  }
  std::vector<dgGroupOfElements*> newGroups;
  std::vector<dgGroupOfElements*> newGhostGroups;
  for (int iGroup = 0; iGroup < getNbElementGroups() + getNbGhostGroups() ; iGroup++) {
    dgGroupOfElements &group = *getElementGroup(iGroup);
    std::map<int, std::vector<MElement *> > subGroups;
    for (size_t iElement = 0; iElement < group.getNbElements(); iElement ++) {
      MElement *el = group.getElement(iElement);
      subGroups[num2tag[el->getNum()]].push_back(el);
    }
    for (std::map<int, std::vector<MElement*> >::iterator it = subGroups.begin(); it!=subGroups.end(); it++) { 
      dgGroupOfElements *newGroup = new dgGroupOfElements(this, it->second, group.getOrder());
      newGroup->copyPrivateDataFrom (&group);
      newGroup->_physicalTag = getModel()->getPhysicalName(dim, it->first);
      if(iGroup < getNbElementGroups())
        newGroups.push_back(newGroup);
      else
        newGhostGroups.push_back(newGroup);
    }
  }
  setGroups(newGroups, newGhostGroups);
}

void dgGroupCollection::splitGroupsRandom(int nSubGroups) 
{
  std::vector<dgGroupOfElements*> newGroups;
  for (int iGroup = 0; iGroup<getNbElementGroups(); iGroup++) {
    dgGroupOfElements *group = getElementGroup(iGroup);
    std::vector<std::vector<MElement*> > subGroups (nSubGroups);
    for (size_t iElement = 0; iElement < group->getNbElements(); iElement ++) {
      MElement *el = group->getElement(iElement);
      subGroups[rand()%nSubGroups].push_back(el);
    }
    for (std::vector<std::vector<MElement*> >::iterator it = subGroups.begin(); it!=subGroups.end(); it++) { 
      dgGroupOfElements *newGroup = new dgGroupOfElements(this, *it, group->getOrder());
      newGroup->copyPrivateDataFrom (group);
      newGroups.push_back(newGroup);
    }
  }
  setGroups(newGroups);
}

/**Split the face groups between vertical and horizontal. The hLevelTags indicates boundary horizontal faces.*/
void dgGroupCollection::splitFaceGroupsByOrientation(dgExtrusion extrusion, std::vector<std::string> hLevelTags) 
{
  std::vector<int> facesH, facesV;
  int oldNbFaceGroups = getNbFaceGroups();
  int newId = 0;
  std::vector<dgGroupOfFaces*> newFaceGroups;
  for (int iGroup = 0; iGroup < oldNbFaceGroups; iGroup++){
    facesH.clear();
    facesV.clear();
    dgGroupOfFaces &faces = *getFaceGroup(iGroup);
    if (faces.nConnection()==2){
      for (size_t iInterface = 0; iInterface < faces.size(); iInterface++){
	size_t iCol0, iLayer0, iCol1, iLayer1;
	extrusion.mapElementID2ColumnPosition(faces.elementGroup(0).elementVectorId(), faces.elementId(iInterface, 0), iCol0, iLayer0);
	extrusion.mapElementID2ColumnPosition(faces.elementGroup(1).elementVectorId(), faces.elementId(iInterface, 1), iCol1, iLayer1);
	if (iCol0==iCol1){
	  facesH.push_back(faces.interfaceId(iInterface));
	}else{
	  facesV.push_back(faces.interfaceId(iInterface));
	}
      }
    }else{
      bool hBndTagFound = false;
      for (size_t i = 0; i < hLevelTags.size(); i++){
	if (faces.physicalTag() == hLevelTags.at(i)) {
	  hBndTagFound = true;
	  break;
	}
      }
      if (hBndTagFound)
	for (size_t iInterface = 0; iInterface < faces.size(); iInterface++)
	  facesH.push_back(faces.interfaceId(iInterface));
      else
	for (size_t iInterface = 0; iInterface < faces.size(); iInterface++)
	  facesV.push_back(faces.interfaceId(iInterface));
    }
    int pOrder = _elementGroups[0]->getOrder();
    if (facesH.size() != 0){
      newFaceGroups.push_back (new dgGroupOfFaces (*this, faces.interfaceVectorId(), pOrder, newId++, facesH));
      newFaceGroups.back()->setOrientationTag(dgGroupOfFaces::ORIENTATION_HORIZONTAL);
    }
    if (facesV.size() != 0){
      newFaceGroups.push_back (new dgGroupOfFaces (*this, faces.interfaceVectorId(), pOrder, newId++, facesV));
      newFaceGroups.back()->setOrientationTag(dgGroupOfFaces::ORIENTATION_VERTICAL);
    }
    delete &faces;
  }
  _faceGroups = newFaceGroups;
}

void dgGroupCollection::_init(bool np)
{
  _mesh = NULL;
  _spaceTransform = NULL;
  _coordinatesDof = NULL;
  _extrusion = NULL;
  _maxGroupLayerId = -1;
  _groupsDefinedOnOneProcessor = np || (Msg::GetCommSize() == 1);
  _coordinatesDofFunction = new dgDofFunction;
}

/*dgGroupCollection::dgGroupCollection(bool np)
{
  _init(np);
}*/

dgGroupCollection::dgGroupCollection(GModel *model, int dimension, int order, bool np)
{
  _init(np);
  buildGroupsOfElements(model,dimension,order);
}

dgGroupCollection::dgGroupCollection(GModel *model, int dimension, int order, const std::vector<std::string> physicalTags, bool np)
{
  _init(np);
  buildGroupsOfElements(model, dimension, order, physicalTags);
}

dgGroupCollection::dgGroupCollection(GModel *model, int dimension, int order, dgSpaceTransform* st, const std::vector<std::string> physicalTags, bool np)
{
  _init(np);
  _spaceTransform = st;
  buildGroupsOfElements(model, dimension, order, physicalTags);
}


dgGroupCollection::dgGroupCollection(GModel *model, int dimension, int order, dgSpaceTransform* st, bool np)
{
  _init(np);
  _spaceTransform = st;
  buildGroupsOfElements(model,dimension,order);
}

dgGroupCollection* dgGroupCollection::newByTag(GModel *model, int dimension, int order, std::vector<std::string> tags){
  Msg::Info("Creating %dD groupCollection by tags.", dimension);
  dgGroupCollection* subCollection = new dgGroupCollection(model, dimension, order, tags);
  if (subCollection->getNbElementGroups() == 0)
    Msg::Warning("groupCollection is empty.");
  return subCollection;
}

dgGroupCollection::~dgGroupCollection() 
{
  for (size_t i=0; i< _elementGroups.size(); i++)
    delete _elementGroups[i];
  for (size_t i=0; i< _faceGroups.size(); i++)
    delete _faceGroups[i];
  for (size_t i=0; i< _ghostGroups.size(); i++)
    delete _ghostGroups[i];
  if(_coordinatesDof)
    delete _coordinatesDof;
  delete _coordinatesDofFunction;
  delete _mesh;
}

void dgGroupCollection::find (const MElement*e, int &iGroup, int &iElem)const
{
  std::map<int,std::pair<int,int> >::const_iterator it = _elementMap.find(e->getNum());
  if (it == _elementMap.end() ) {
    iGroup = iElem = -1;
  } else {
    iGroup = it->second.first;
    iElem = it->second.second;
  }
}

void dgGroupCollection::findFace (const MElement*e, int &iGroup, int &iFace)const
{
  std::map<int,std::pair<int,int> >::const_iterator it = _faceMap.find(e->getNum());
  if (it == _faceMap.end() ) {
    iGroup = iFace = -1;
  } else {
    iGroup = it->second.first;
    iFace = it->second.second;
  }
}

static void  writeMshInterpolationSchemes(FILE *f, const std::string &name, const dgGroupCollection &groups)
{
  std::set<const nodalBasis*> allPb;
  for (int i=0; i<groups.getNbElementGroups(); ++i)
    allPb.insert(&groups.getElementGroup(i)->getFunctionSpace());
  fprintf(f, "$InterpolationScheme\n");
  fprintf(f, "\"interp_%s\"\n",name.c_str());
  fprintf(f, "%lu\n", (unsigned long int) allPb.size());
  for (std::set<const nodalBasis*>::iterator it = allPb.begin(); it != allPb.end(); ++it) {
    const polynomialBasis &pb = *(polynomialBasis*)*it;
    fprintf(f, "%i 2\n", pb.parentType);
    fprintf(f, "%i %i\n", pb.coefficients.size1(), pb.coefficients.size2());
    for (int i=0; i<pb.coefficients.size1(); ++i) {
      for (int j=0; j<pb.coefficients.size2(); ++j)
        fprintf(f, "%.16g ", pb.coefficients(i ,j));
      fprintf(f, "\n");
    }
    fprintf(f, "%i %i\n", pb.monomials.size1(), pb.monomials.size2());
    for (int i=0; i<pb.monomials.size1(); ++i) {
      for (int j=0; j<pb.monomials.size2(); ++j)
        fprintf(f, "%.16g ", pb.monomials(i ,j));
      fprintf(f, "\n");
    }
  }
  fprintf(f, "$EndInterpolationScheme\n");
}

void dgGroupCollection::exportFunctionMsh (const function * fun, const std::string name, double time, int step, std::string fieldname, const dgDofContainer *solutionDof) const
{
  int nbExport = fun->getNbCol();
  bool vector = false;
  bool tensor = false;
  if (fun->getNbCol() == 3) {
    nbExport = 1;
    vector = true;
  } else if (fun->getNbCol() == 9) {
    nbExport = 1;
    tensor = true;
  }

  for (int ICOMP = 0; ICOMP < nbExport;++ICOMP){
    std::ostringstream name_oss, name_view;
    if (fieldname != ""){
      name_view<<fieldname;
      name_oss<<name<<"_"<<fieldname<<".msh";
    }
    else if (vector){
      name_view<<"vector";
      name_oss<<name<<".msh";
    }
    else if (tensor) {
      name_view<<"tensor";
      name_oss<<name<<".msh";
    }
    else if (!vector && nbExport > 1){
      name_view<<"comp_"<<ICOMP;
      name_oss<<name<<"_COMP_"<<ICOMP<<".msh";
    }
    else{
      name_view<<"function";
      name_oss<<name<<".msh";
    }
    if(Msg::GetCommSize()>1)
      name_oss<<"_"<<Msg::GetCommRank();
    FILE *f = fopen (name_oss.str().c_str(),"w");
    if(!f){
      Msg::Fatal("Unable to open export file '%s'", name.c_str());
    }

    int COUNT = 0;
    for (int i=0;i < getNbElementGroups() ;i++){
      COUNT += getElementGroup(i)->getNbElements();
    }

    fprintf(f,"$MeshFormat\n2.1 0 8\n$EndMeshFormat\n");
    writeMshInterpolationSchemes(f, name_view.str(), *this);
    fprintf(f,"$ElementNodeData\n");
    fprintf(f,"2\n");
    fprintf(f,"\"%s\"\n",name_view.str().c_str());
    fprintf(f,"\"interp_%s\"\n",name_view.str().c_str());
    fprintf(f,"1\n");
    fprintf(f,"%f\n", time);
    fprintf(f,"%d\n", Msg::GetCommSize() > 1 ? 4 : 3);
    fprintf(f,"%d\n", step);
    if (vector)  fprintf(f,"3\n");
    else if (tensor) fprintf (f, "9\n");
    else  fprintf(f,"1\n");
    fprintf(f,"%d\n", COUNT);
    if(Msg::GetCommSize() > 1) fprintf(f,"%d\n", Msg::GetCommRank());
    dataCacheMap cacheMap(dataCacheMap::NODE_GROUP_MODE, this);
    if(solutionDof)
      cacheMap.setSolutionFunction (solutionDof->getFunction(), solutionDof->getFunctionGradient());
    dataCacheDouble &value = *cacheMap.get(fun);
    for (int iG=0; iG < getNbElementGroups(); iG++){
      dgGroupOfElements *group = getElementGroup(iG);
      cacheMap.setGroup(group);
      int nNode = group->getNbNodes();
      const fullMatrix<double> &valueMatrix = value.get();
      for (size_t iElement=0 ; iElement< group->getNbElements() ;++iElement) {
        int num = group->getElement(iElement)->getNum();
        fprintf(f, "%d %d", num, nNode);
        if (vector || tensor){
          for (int k = 0; k < nNode; ++k)
            for (int j = 0; j < valueMatrix.size2(); ++j)
              fprintf(f, " %.16E ", valueMatrix(k + iElement * nNode, j));
        }
        else{
          for (int k = 0; k < nNode; ++k)
            fprintf(f, " %.16E ", valueMatrix(k + iElement * nNode, ICOMP));
        }
        fprintf(f,"\n");
      }
    }
    fprintf(f,"$EndElementNodeData\n");
    fclose(f);
  }
}
