#ifndef _DG_GROUP_OF_ELEMENTS_
#define _DG_GROUP_OF_ELEMENTS_

#include <vector>
#include "dgFullMatrix.h"
#include "nodalBasis.h"

#include "dgInterfaceVector.h"
#include "dgElementVector.h"

class dgExtrusion;
class nodalBasis;
class GModel;

class dgConservationLawFunction;
class dgDofContainer;

class dgMiniInterface;
class dgGroupCollection;
class dgSpaceTransform;
class dgMesh;
class dgMeshJacobian;

class function;
class dgDofFunction;
class dgDofFunctionGradient;

/**Stores topological and geometrical data for 1 group for 1 discretisation.
 * A group of element contains N elements
 * that are of the same type and of the same order. All element have the same
 * number of integration points Np, the same number of vertices Nv.
*/

class dgGroupOfElements {
  std::vector<MElement*> _elements;
  friend class dgGroupCollection;
  const nodalBasis &_fs;
  dgFullMatrix<double> _mass, _imass;
  // polynomial order of the interpolation
  
  double _maxInnerRadius, _minInnerRadius;

  // element length in the direction of the a given vector field
  fullMatrix<double> _streamwiseLength;
  // elementary PSPG stabilisation
  mutable fullMatrix<double> _pspgStabilisation;
  // elementary laplacian
  mutable fullMatrix<double> _laplacianOfFunction;
  dgGroupCollection *_groups;
  int _elementVectorId;
protected:
  std::string _physicalTag;
  int _ghostPartition; // -1 : this is not a ghosted group, otherwise the id of the parent partition
  int _multirateTag;
  int _layerId;
public:
  inline int elementVectorId() const {return _elementVectorId;}
  inline void setElementVectorId(int id) {_elementVectorId = id;}
  dgGroupOfElements (dgGroupCollection *groups, const std::vector<MElement*> &e, int pOrder, int ghostPartition=-1);
  inline dgGroupCollection *getGroupCollection() const {return _groups;}
  //base
  inline std::vector<MElement*> getElements() {return _elements;}
  inline size_t getNbElements() const {return _elements.size();}
  inline MElement* getElement (int iElement) const {return _elements[iElement];}
  void computeMatrices();
  inline const nodalBasis & getFunctionSpace () const {return _fs;}
  inline int getNbNodes() const {return _fs.points.size1();}
  inline int getDimUVW () const {return _fs.dimension;}
  inline int getOrder() const {return _fs.order;}

  //group data => not sure, keep it ?
  inline std::string getPhysicalTag() const {return _physicalTag;}
  inline int getMultirateTag() const {return _multirateTag;}
  inline void setMultirateTag(int tag) {_multirateTag = tag;}
  inline int getLayerId() const {return _layerId;}
  inline int getGhostPartition() const {return _ghostPartition;}
  void copyPrivateDataFrom(const dgGroupOfElements *from);
  //inverse mass matrix in a specific solver
  inline const dgFullMatrix<double> &getInverseMassMatrix () const {return _imass;}
  // remove massMatrix ?
  inline const dgFullMatrix<double> &getMassMatrix () const {return _mass;}
  inline double maxInnerRadius()const {return _maxInnerRadius;}
  inline double minInnerRadius()const {return _minInnerRadius;}
  //element data => move to dgElement or create a dgElementData
  void buildStreamwiseLength (int iG, function *fun, dgDofContainer *solution);
  void buildLaplacianOfFunction (int iG, function *fun, dgGroupCollection *groups);
  inline double getStreamwiseLength(int iElement)const {return _streamwiseLength(iElement,0);}
  inline double getPSPGStabilisation(int iElement)const {return _pspgStabilisation(iElement,0);}
  inline void   setPSPGStabilisation(int iElement, double tau_P)const { _pspgStabilisation(iElement,0) = tau_P;};
  inline double getLaplacianOfFunction(int iElement, int idx)const {return _laplacianOfFunction(iElement,idx);}

  //Helpers jacobian
  double elementVolume(const dgMeshJacobian &jac, int iElement) const;
};

/**A group of faces.
 * All faces of this group are either interior of the same group of elements or at the boundary between two group of elements. */
class dgGroupOfFaces {
 public:
  typedef enum {ORIENTATION_UNDEFINED, ORIENTATION_HORIZONTAL, ORIENTATION_VERTICAL} orientationType;
 private:
  const nodalBasis *_fsFace;
  const dgInterfaceVector &_interfaceVector;
  std::vector<int> _interfacesSubset;
  std::vector<dgGroupOfElements*> _elementGroup;
  int _id;
  orientationType _orientationTag;
  int _iInterfaceVector;
public:
  dgGroupOfFaces (dgGroupCollection &groups, int interfaceVector, int pOrder, int id, const std::vector<int>& interfacesSubset = std::vector<int>());

  inline const nodalBasis * getNodalBasis() const {return _fsFace;}
  inline int id() const {return _id;}
  inline const std::string &physicalTag() const {return _interfaceVector.physicalTag();}
  inline int nConnection() const {return _interfaceVector.nConnection();}
  inline size_t size() const {return _interfacesSubset.size();}
  inline int getNbNodes() const {return _fsFace->points.size1();}
  inline int interfaceVectorId() const {return _iInterfaceVector;}
  inline const dgGroupOfElements &elementGroup(int iConnection) const {return *_elementGroup[iConnection];}
  inline int elementId(int iInterface, int iConnection) const {return _interfaceVector.elementId(_interfacesSubset[iInterface], iConnection);}
  inline MElement &element(int iInterface, int iConnection) const {return _interfaceVector.elementVector(iConnection).element(elementId(iInterface, iConnection));}
  inline int closureId(int iInterface, int iConnection) const {return _interfaceVector.closureId(_interfacesSubset[iInterface], iConnection);}
  inline MElement * meshInterfaceElement(int iInterface) const {return _interfaceVector.faceElement(_interfacesSubset[iInterface]);}
  inline const std::vector<int>& closure(int iInterface, int iConnection) const {return _elementGroup[iConnection]->getFunctionSpace().closures[closureId(iInterface, iConnection)];}
  inline const std::vector<int>& fullClosure(int iInterface, int iConnection) const {return _elementGroup[iConnection]->getFunctionSpace().fullClosures[closureId(iInterface, iConnection)];}
  inline int interfaceId(int localIndex) const {return _interfacesSubset[localIndex];}
  inline const std::vector<int> &interfaceId() const {return _interfacesSubset;}
  inline orientationType orientationTag() const {return _orientationTag;}
  inline void setOrientationTag(orientationType tag) {_orientationTag = tag;}

  double interfaceSurface(const dgMeshJacobian &jac, int iFace) const;
  void normal(const dgMeshJacobian &jac, int iFace, int iConnection, fullMatrix<double> &n) const;
};

/** The GroupCollection class let you access to group partitioning functions */
class dgGroupCollection {
  GModel *_model;
  int _dim;
  int _order;
  std::vector<dgGroupOfElements*> _elementGroups; //volume
  std::vector<dgGroupOfFaces*> _faceGroups; //interface
  std::vector<dgGroupOfElements*> _ghostGroups; //ghost volume
  dgDofContainer *_coordinatesDof;
  //we do not use _coordinatesDof->function because we want to be able to change the dof (on split) while keeping the functions
  dgDofFunction *_coordinatesDofFunction;

  std::vector<std::vector<dgGroupOfFaces*> > _interfaceGroupsForGroup; // list of face group for each group of elements

  //{group,id} of the elements to send to each partition for a scatter operation
  std::vector< std::vector<std::pair<int,int> > >_elementsToSend, _elementsToSendDG, _elementsToReceive, _elementsToReceiveDG;

  // maps all elements  to pair {groupId,elementId}
  std::map<int ,std::pair<int,int> > _elementMap, _faceMap;
  bool _groupsDefinedOnOneProcessor;

  int _maxGroupLayerId;
  std::map<const dgGroupOfElements*,int> _groupId;
  void _buildParallelStructure();
  dgSpaceTransform *_spaceTransform;
  dgExtrusion *_extrusion;
  void _init(bool np);
public:
  dgMesh *_mesh;
  const dgDofContainer &coordinatesDof() const {return *_coordinatesDof;};
  inline void setSpaceTransform(dgSpaceTransform *st) {_spaceTransform = st;}
  inline dgSpaceTransform *getSpaceTransform() const {return _spaceTransform;}
  inline int getMaxGroupLayerId() const {return _maxGroupLayerId;}
  inline bool getIsDefinedOnOneProcessor() const {return _groupsDefinedOnOneProcessor;}
  inline GModel* getModel() const {return _model;}
  /**Return the number of dgGroupOfElements */
  inline int getNbElementGroups() const {return _elementGroups.size();}
  inline int getNbElements() const {
    int nbElems = 0;
    for (int i= 0; i < getNbElementGroups() ; i++) 
      nbElems += _elementGroups[i]->getNbElements();
    return nbElems;
  }
  inline int getDim() const { return _dim; }
  inline int getNbGhostGroups() const {return _ghostGroups.size();}
  /**Return the number of dgGroupOfFaces*/
  inline int getNbFaceGroups() const {return _faceGroups.size();}
  inline int getNbFaceGroupsOfGroup(int i) const {return _interfaceGroupsForGroup[i].size();}
  /**Get one group of elements. */
  inline dgGroupOfElements *getElementGroup(int i) const{return i<getNbElementGroups()?_elementGroups[i]:_ghostGroups[i-getNbElementGroups()];} 
  inline dgGroupOfElements *getElementGroup(std::string tag) const { for(int i=0; i<getNbElementGroups(); i++) if(_elementGroups[i]->getPhysicalTag() == tag) return _elementGroups[i]; return NULL; }
  /**Get one group of faces. */
  inline dgGroupOfFaces *getFaceGroup(int i) const {return _faceGroups[i];}
  inline dgGroupOfFaces *getFaceGroupOfGroup(int i, int j) const {return _interfaceGroupsForGroup[i][j];}
  inline dgGroupOfElements *getGhostGroup(int i) const {return _ghostGroups[i];}
  inline int getElementGroupId(const dgGroupOfElements* g) const { return _groupId.find(g)->second;}

  inline int getNbImageElementsOnPartition(int partId, bool nodalGhost = true) const {return nodalGhost ? _elementsToSend[partId].size() : _elementsToSendDG[partId].size();}
  inline int getImageElementGroup(int partId, int i, bool nodalGhost = true) const {return nodalGhost ? _elementsToSend[partId][i].first : _elementsToSendDG[partId][i].first;}
  inline int getImageElementPositionInGroup(int partId, int i, bool nodalGhost = true) const {return nodalGhost ? _elementsToSend[partId][i].second : _elementsToSendDG[partId][i].second;}
  inline int getNbGhostElementsFromPartition(int partId, bool nodalGhost = true) const {return nodalGhost ? _elementsToReceive[partId].size() : _elementsToReceiveDG[partId].size();}
  inline int getGhostElementGroup(int partId, int i, bool nodalGhost = true) const {return nodalGhost ? _elementsToReceive[partId][i].first : _elementsToReceiveDG[partId][i].first;}
  inline int getGhostElementPositionInGroup(int partId, int i, bool nodalGhost = true) const {return nodalGhost ? _elementsToReceive[partId][i].second : _elementsToReceiveDG[partId][i].second;}
  inline void setExtrusion(dgExtrusion *extrusion) {_extrusion = extrusion;}
  inline dgExtrusion *getExtrusion() const {return _extrusion;}
  double maxInnerRadius() const;
  double minInnerRadius() const;
  void buildGroupsOfElements (GModel *model,int dimension, int order, const std::vector<std::string> physicalTags = std::vector<std::string>());
  void setGroups(std::vector<dgGroupOfElements*> newGroups, std::vector<dgGroupOfElements*> newGhostGroups);
  void setGroups(std::vector<dgGroupOfElements*> newGroups);
  void copyVectorCoordinates();
  /** build an elementary mesh size in a given streamwise direction */
  void buildStreamwiseLength (function *fun, dgDofContainer *solution=NULL);
  /**Build the laplacian of the components of a function */
  void buildLaplacianOfFunction (function *fun);
  /**Split the groups according vertical layer structure. The first is defined by the topLevelTags.*/
  void splitGroupsByVerticalLayer(dgExtrusion extrusion, std::vector<std::string> botLevelTags, std::vector<std::string> topLevelTags);
  /**Splits the groups according to their physical tags. */
  void splitGroupsByPhysicalTag();
  /**Splits the groups randomly (debugging purpose). */
  void splitGroupsRandom (int nGroups);
/**Split the face groups between vertical and horizontal. The hLevelTags indicates boundary horizontal faces.*/
  void splitFaceGroupsByOrientation(dgExtrusion extrusion, std::vector<std::string> hLevelTags);


  void find (const MElement *elementToFind, int &iGroup, int &ithElementOfGroup) const;
  void findFace (const MElement *elementToFind, int &iGroup, int &ithFaceOfGroup) const;
  /**Builds the group of elements, separated by element type and element order.
   * Additional partitioning can be done using splitGroupsForXXX specific functions */
  dgGroupCollection(GModel *model, int dimension =-1, int order =-1, bool np=false);
  dgGroupCollection(GModel *model, int dimension, int order, dgSpaceTransform* st, bool np=false);
  dgGroupCollection(GModel *model, int dimension, int order, const std::vector<std::string> physicalTags, bool np=false);
  dgGroupCollection(GModel *model, int dimension, int order, dgSpaceTransform* st, const std::vector<std::string> physicalTags, bool np=false);
  //dgGroupCollection(bool np=false);
  /**Creates a group collection of elements associated with given physical tag(s). */
  static dgGroupCollection* newByTag(GModel* model, int dimension, int order, std::vector<std::string> tags);
  ~dgGroupCollection();

  const function *getFunctionCoordinates() const;
  void updateCoordinates(const function *xyzF);
  void updateCoordinates(dgDofContainer* zCoordDof);
  /*Allows the edition of the mesh coordinates. Warn: is reinit when setGroups() is called*/
  void setCoordinates(dgDofContainer* xyzCoordDof); // 6 fields required: scale * original + shift: scx, shx, scy, shy, scz, shz
  void exportFunctionMsh (const function * fun, const std::string name, double time=0.0, int step=0, std::string fieldname="", const dgDofContainer *solutionDof = NULL) const;
};
#endif
