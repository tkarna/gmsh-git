#ifndef _DG_CIRCLE_TRANSFORM_H
#define _DG_CIRCLE_TRANSFORM_H

#include <map>
#include <math.h>

#include "dgSpaceTransform.h"
#include "function.h"

class dgGroupCollection;

class dgCircleTransform : public dgSpaceTransform {
 private:
  std::map<int,int> _elemToSeamClosure;
 public :
  std::map<int,int> &elemToSeamClosure(){return _elemToSeamClosure;}
  dgCircleTransform (dgGroupCollection &groups);
  void applyJacobian(const MElement &elem, double xyz[3], double uvw[3], double jac[3][3], double &detJ, int closureId = -1) const;
};

class transformToCircle : public function {
  std::map<int,int> &_elemToSeamClosure;
  fullMatrix<double> _xyz;
 public:
  void getCircleCoordinates (const MElement &elem, double uvw[3], int closureId, double xyzIn[3], double xyzOut[3]);
  transformToCircle(dgCircleTransform &transform);
  void call (dataCacheMap *m, fullMatrix<double> &val);
};
#endif
