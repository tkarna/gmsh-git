#include "dgCGStructure.h"
#include "dgGhostStructure.h"
#include "dgDofContainer.h"
#include "dgGroupOfElements.h"
#include "MVertex.h"
#include "MElement.h"
#include "GModel.h"
#include <limits>
dgCGStructure::dgCGStructure(const dgGroupCollection &groups):
  _groups(&groups)
{
  std::map<int, int> localVertexIdMap; // vertex num -> local num
  _iGroupiElementiVertex2localId.resize(_groups->getNbElementGroups());
  { // create a local numbering of the vertices
    _nCGVertices = 0;
    for (int iG = 0; iG < _groups->getNbElementGroups(); iG++) {
      const dgGroupOfElements *group = _groups->getElementGroup(iG);
      int nbNodes = group->getNbNodes();
      std::vector<int> &iElementiVertex2LocalId = _iGroupiElementiVertex2localId[iG];
      iElementiVertex2LocalId.resize(nbNodes * group->getNbElements());
      for (size_t iE =0; iE < group->getNbElements(); iE++) {
        MElement* e = group->getElement(iE);
        for (int iN = 0; iN < nbNodes; iN++ ) {
          MVertex *v = e->getVertex(iN); 
          int id = localVertexIdMap.insert(std::make_pair(v->getNum(), _nCGVertices)).first->second;
          if (id == _nCGVertices) {
            ++_nCGVertices;
          }
          iElementiVertex2LocalId[iE * nbNodes + iN] = id;
        }
      }
    }
  }
  std::map<int, std::vector<int> > scatterSend;
  { // collect the ghost vertices
    //create a map of all ghosted vertices of the mesh
    GModel *gm = _groups->getModel();
    std::vector<GEntity*> entities;
    gm->getEntities(entities);
    std::map<MVertex *, int > ghostVertices; // vertex 2 partition (only correct for node that touch an edge on the interface between the partitions)
    for (size_t iEntity = 0; iEntity < entities.size(); ++ iEntity) {
      GEntity *entity = entities[iEntity];
      for (size_t iElement = 0; iElement < entity->getNumMeshElements(); ++ iElement) {
        MElement *element = entity->getMeshElement(iElement);
        if (element->getPartition() - 1 > Msg::GetCommRank() && Msg::GetCommSize() != 1) {
          for (int iVertex = 0; iVertex < element->getNumVertices(); ++ iVertex) {
            int &partition = ghostVertices[element->getVertex(iVertex)];
            partition = std::max(partition, element->getPartition() - 1);
          }
        }
      }
    }
    //number the relevant ghost
    for (int iGroup = 0; iGroup < _groups->getNbFaceGroups(); iGroup++) {
      const dgGroupOfFaces &fGroup = *_groups->getFaceGroup(iGroup);
      for (int iConn = 0; iConn < fGroup.nConnection(); ++iConn) {
        for (size_t iF = 0; iF < fGroup.size(); ++iF) {
          MElement &e = fGroup.element(iF, iConn);
          const std::vector<int> &cl = fGroup.closure(iF, iConn);
          for (size_t i = 0; i< cl.size(); i++) {
            MVertex *v = e.getVertex(cl[i]);
            std::map<MVertex *, int>::iterator it = ghostVertices.find(v);
            if (it != ghostVertices.end() && it->second >= 0) {
              scatterSend[it->second].push_back(v->getNum());
              ghostVertices.erase(it);
            }
          }
        }
      }
    }
  }
  _ghostStructure = new dgGhostStructure(scatterSend, localVertexIdMap);
}

void dgCGStructure::_scatter(std::vector<double> &cg, dgDofContainer *to)
{
  size_t nbFields = to->getNbFields();
  if (cg.size()/_nCGVertices != nbFields){
    Msg::Error("wrong number of fields in destination dgDofContainer");
    return;
  }
  _ghostStructure->communicate(dgGhostStructure::SCATTER, cg, nbFields);
  for (int i = 0; i < _groups->getNbElementGroups(); ++i) {
    const dgGroupOfElements &group = *_groups->getElementGroup(i);
    std::vector<int> iElementiVertex2LocalId = _iGroupiElementiVertex2localId[i];
    fullMatrix<double> &proxy = to->getGroupProxy(i);
    for (size_t j = 0; j < group.getNbElements(); ++j) {
      for (int k = 0; k < group.getNbNodes(); ++k) {
        for (size_t f = 0; f < nbFields; ++f) {
          proxy(k, j * nbFields + f) = cg[iElementiVertex2LocalId[j * group.getNbNodes() + k] * nbFields + f];
        }
      }
    }
  }
  to->scatter();
}

void dgCGStructure::mean(dgDofContainer *from, dgDofContainer *to)
{
  int nbFields = from->getNbFields();
  std::vector<double> cg(_nCGVertices * nbFields);
  for (int i = 0; i < _groups->getNbElementGroups(); ++i){
    const dgGroupOfElements &group = *_groups->getElementGroup(i);
    std::vector<int> iElementiVertex2LocalId = _iGroupiElementiVertex2localId[i];
    const fullMatrix<double> &proxy = from->getGroupProxy(i);
    for (size_t j = 0; j < group.getNbElements(); ++j) {
      for (int k = 0; k < group.getNbNodes(); ++k) {
        for (int f = 0; f < nbFields; ++f)
          cg[iElementiVertex2LocalId[j * group.getNbNodes() + k] * nbFields + f] += proxy(k, j * nbFields + f);
      }
    }
  }
  if (_vertexMultiplicity.empty()) {
    // compute local vertex multiplicity
    _vertexMultiplicity.resize(_nCGVertices);
    for (int i = 0; i < _groups->getNbElementGroups(); ++i){
      const dgGroupOfElements &group = *_groups->getElementGroup(i);
      std::vector<int> iElementiVertex2LocalId = _iGroupiElementiVertex2localId[i];
      for (size_t j = 0; j < group.getNbElements(); ++j) {
        for (int k = 0; k < group.getNbNodes(); ++k) {
          ++_vertexMultiplicity[iElementiVertex2LocalId[j * group.getNbNodes() + k]];
        }
      }
    }
    _ghostStructure->communicate(dgGhostStructure::GATHER_SUM, _vertexMultiplicity, 1);
    _ghostStructure->communicate(dgGhostStructure::SCATTER, _vertexMultiplicity, 1);
  }

  for (int i = 0; i < _nCGVertices; ++i){
    for (int j = 0; j < nbFields; ++j){
      cg[i * nbFields + j] /= _vertexMultiplicity[i];
    }
  }
  _ghostStructure->communicate(dgGhostStructure::GATHER_SUM, cg, nbFields);
  _scatter(cg, to);
}

void dgCGStructure::weightedMean(dgDofContainer *from, dgDofContainer *to, dgDofContainer *weights)
{
  int nbFields = from->getNbFields();
  std::vector<double> cg(_nCGVertices * nbFields);
  std::vector<double> sumOfWeights(_nCGVertices,0);
  for (int i = 0; i < _groups->getNbElementGroups(); ++i){
    const dgGroupOfElements &group = *_groups->getElementGroup(i);
    std::vector<int> iElementiVertex2LocalId = _iGroupiElementiVertex2localId[i];
    const fullMatrix<double> &sourceProxy = from->getGroupProxy(i);
    const fullMatrix<double> &wProxy = weights->getGroupProxy(i);
    for (size_t j = 0; j < group.getNbElements(); ++j) {
      for (int k = 0; k < group.getNbNodes(); ++k) {
        double w = wProxy(k, j);
        sumOfWeights[iElementiVertex2LocalId[j * group.getNbNodes() + k]] += w;
        for (int f = 0; f < nbFields; ++f) {
          double val = sourceProxy(k, j * nbFields + f);
          cg[iElementiVertex2LocalId[j * group.getNbNodes() + k] * nbFields + f] += val*w;
        }
      }
    }
  }
  _ghostStructure->communicate(dgGhostStructure::GATHER_SUM, sumOfWeights, 1);
  _ghostStructure->communicate(dgGhostStructure::SCATTER, sumOfWeights, 1);

  for (int i = 0; i < _nCGVertices; ++i){
    for (int j = 0; j < nbFields; ++j){
      cg[i * nbFields + j] /= sumOfWeights[i];
    }
  }
  _ghostStructure->communicate(dgGhostStructure::GATHER_SUM, cg, nbFields);
  _scatter(cg, to);
}

void dgCGStructure::min(dgDofContainer *from, dgDofContainer *to)
{
  int nbFields = from->getNbFields();
  std::vector<double> cg(_nCGVertices * nbFields, std::numeric_limits<double>::max());
  for (int i = 0; i < _groups->getNbElementGroups(); ++i){
    const dgGroupOfElements &group = *_groups->getElementGroup(i);
    std::vector<int> iElementiVertex2LocalId = _iGroupiElementiVertex2localId[i];
    const fullMatrix<double> &proxy = from->getGroupProxy(i);
    for (size_t j = 0; j < group.getNbElements(); ++j) {
      for (int k = 0; k < group.getNbNodes(); ++k) {
        for (int f = 0; f < nbFields; ++f){
          double &v = cg[iElementiVertex2LocalId[j * group.getNbNodes() + k] * nbFields + f];
          v = std::min(v, proxy(k, j * nbFields + f));
        }
      }
    }
  }
  _ghostStructure->communicate(dgGhostStructure::GATHER_MIN, cg, nbFields);
  _scatter(cg, to);
}

void dgCGStructure::max(dgDofContainer *from, dgDofContainer *to)
{
  int nbFields = from->getNbFields();
  std::vector<double> cg(_nCGVertices * nbFields, -std::numeric_limits<double>::max());
  for (int i = 0; i < _groups->getNbElementGroups(); ++i){
    const dgGroupOfElements &group = *_groups->getElementGroup(i);
    std::vector<int> iElementiVertex2LocalId = _iGroupiElementiVertex2localId[i];
    const fullMatrix<double> &proxy = from->getGroupProxy(i);
    for (size_t j = 0; j < group.getNbElements(); ++j) {
      for (int k = 0; k < group.getNbNodes(); ++k) {
        for (int f = 0; f < nbFields; ++f){
          double &v = cg[iElementiVertex2LocalId[j * group.getNbNodes() + k] * nbFields + f];
          v = std::max(v, proxy(k, j * nbFields + f));
        }
      }
    }
  }
  _ghostStructure->communicate(dgGhostStructure::GATHER_MAX, cg, nbFields);
  _scatter(cg, to);
}

void dgCGStructure::sum(dgDofContainer *from, dgDofContainer *to)
{
  int nbFields = from->getNbFields();
  std::vector<double> cg(_nCGVertices * nbFields);
  for (int i = 0; i < _groups->getNbElementGroups(); ++i){
    const dgGroupOfElements &group = *_groups->getElementGroup(i);
    std::vector<int> iElementiVertex2LocalId = _iGroupiElementiVertex2localId[i];
    const fullMatrix<double> &proxy = from->getGroupProxy(i);
    for (size_t j = 0; j < group.getNbElements(); ++j) {
      for (int k = 0; k < group.getNbNodes(); ++k) {
        for (int f = 0; f < nbFields; ++f){
          cg[iElementiVertex2LocalId[j * group.getNbNodes() + k] * nbFields + f] +=  proxy(k, j * nbFields + f);
        }
      }
    }
  }
  _ghostStructure->communicate(dgGhostStructure::GATHER_SUM, cg, nbFields);
  _scatter(cg, to);
}

dgCGStructure::~dgCGStructure()
{
  delete _ghostStructure;
}

void dgCGStructure::writeCG(dgDofContainer *dof, const char *filename)
{
  int nbFields = dof->getNbFields();
  std::vector<double> cg(_nCGVertices * nbFields);
  for (int i = 0; i < _groups->getNbElementGroups(); ++i){
    const dgGroupOfElements &group = *_groups->getElementGroup(i);
    std::vector<int> iElementiVertex2LocalId = _iGroupiElementiVertex2localId[i];
    const fullMatrix<double> &proxy = dof->getGroupProxy(i);
    for (size_t j = 0; j < group.getNbElements(); ++j) {
      for (int k = 0; k < group.getNbNodes(); ++k) {
        for (int f = 0; f < nbFields; ++f){
          cg[iElementiVertex2LocalId[j * group.getNbNodes() + k] * nbFields + f] =  proxy(k, j * nbFields + f);
        }
      }
    }
  }
  for (int iRank = 0; iRank < Msg::GetCommSize(); ++iRank) {
    Msg::Barrier();
    if (Msg::GetCommRank() != iRank) continue;
    FILE *out = fopen(filename, Msg::GetCommRank() == 0 ? "w" : "a");
    if (!out) Msg::Fatal("cannot open %s.", filename);
    for (int i = 0; i < _nCGVertices; ++i) {
      for (int j = 0; j < nbFields; ++j) {
        fprintf(out, "%.4e ", cg[i * nbFields + j]);
      }
      fprintf(out, "\n");
    }
    fclose(out);
  }
  Msg::Barrier();
}

void dgCGStructure::writeMshVertices(const char *filename)
{
  std::vector<int> cgIdx(_nCGVertices);
  std::vector<double> coord(_nCGVertices * 3);
  for (int i = 0; i < _groups->getNbElementGroups(); ++i){
    const dgGroupOfElements &group = *_groups->getElementGroup(i);
    std::vector<int> iElementiVertex2LocalId = _iGroupiElementiVertex2localId[i];
    for (size_t j = 0; j < group.getNbElements(); ++j) {
      MElement *e = group.getElement(j);
      if(e->getNumVertices() != group.getNbNodes())
        Msg::Fatal("writeMshIdx can only be used in iso-parametric");
      for (int k = 0; k < group.getNbNodes(); ++k) {
        int iCG = iElementiVertex2LocalId[j * group.getNbNodes() + k];
        MVertex *v = e->getVertex(k);
        cgIdx[iCG] = v->getNum();
        coord[iCG * 3 + 0] = v->x();
        coord[iCG * 3 + 1] = v->y();
        coord[iCG * 3 + 2] = v->z();
      }
    }
  }
  for (int iRank = 0; iRank < Msg::GetCommSize(); ++iRank) {
    Msg::Barrier();
    if (Msg::GetCommRank() != iRank) continue;
    FILE *out = fopen(filename, Msg::GetCommRank() == 0 ? "w" : "a");
    if (!out) Msg::Fatal("cannot open %s.", filename);
    for (size_t i = 0; i < cgIdx.size(); ++i)
      fprintf(out, "%i %.16e %.16e %.16e\n", cgIdx[i], coord[i * 3 + 0], coord[i * 3 + 1], coord[i * 3 + 2]);
    fclose(out);
  }
  Msg::Barrier();
}
