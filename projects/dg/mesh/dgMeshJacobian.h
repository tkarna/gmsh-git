#ifndef _DG_MESH_JACOBIAN_H_
#define _DG_MESH_JACOBIAN_H_

#include <vector>
#include <dgFullMatrix.h>

class dgMesh;
class dgDofContainer;

class dgMeshJacobian {
  const dgMesh *_mesh;
  int _integrationOrder;
  std::vector<dgFullMatrix<double> > _elementJacobian;
  std::vector<fullMatrix<double> > _elementJacobianDeterminant;
  std::vector<fullMatrix<double> > _interfaceJacobianDeterminant;
  std::vector<std::vector<dgFullMatrix<double> > > _interfaceDXiDX;
  std::vector<std::vector<dgFullMatrix<double> > > _interfaceNormals;
  std::vector<fullVector<double> > _interfaceSurface;
  std::vector<fullVector<double> > _elementVolume;
  std::vector<fullVector<double> > _elementInnerRadius;
 public:
  const dgFullMatrix<double> &getElementJacobian(int iVector) const {return _elementJacobian[iVector];}
  const fullMatrix<double> &getElementJacobianDeterminant(int iVector) const {return _elementJacobianDeterminant[iVector];}
  dgMeshJacobian(const dgMesh &mesh, int integrationOrder = -1);
  ~dgMeshJacobian();
  fullMatrix<double> multDXiDXJElement(int iGroup, const fullMatrix<double> & vX) const;
  void multDXiDXJParentElement (int iGroup, int iElement, const fullMatrix<double> & vX, fullMatrix<double> &vXi) const;
  fullMatrix<double> multDXiDXJRightElement(int iGroup, const fullMatrix<double> & vX) const;
  fullMatrix<double> multDXiDXDXiDXJElement(int iGroup, const fullMatrix<double> & vX) const;
  fullMatrix<double> multJElement(int iGroup, const fullMatrix<double> &vX) const;
  fullMatrix<double> multJInterface(int iVector, const std::vector<int> &faceId, const fullMatrix<double> &vX) const;
  void multJElement (int iGroup, int iElement, const fullMatrix<double> &vX, fullMatrix<double> &vXi) const;
  void multDXiDXElement (int iGroup, int iElement, const fullMatrix<double> & vXi, fullMatrix<double> &vX) const;
  void multDXiDXInterface(int iGroup, int iConn, int iFace, const fullMatrix<double> & vXi, fullMatrix<double> &vX) const;
  void multDXiDXJInterface(int iGroup, int iConn, int iFace, const fullMatrix<double> & vX, fullMatrix<double> &vXi, int d) const;
  fullMatrix<double> multDXiDXJRightInterface(int iGroup, int iConnection, const std::vector<int> &faceId, const fullMatrix<double> &vX) const;
  inline const fullMatrix<double>& detJElement(int iGroup) const {return _elementJacobianDeterminant[iGroup];}
  inline const fullMatrix<double>& detJInterface(int iGroup) const {return _interfaceJacobianDeterminant[iGroup];}
  inline double invJElement (int iGroup, int iElement, int iGaussPoint, int iUVW, int jXYZ) const {return _elementJacobian[iGroup](iUVW, (iElement * _elementJacobianDeterminant[iGroup].size1()+iGaussPoint)*3+jXYZ);}
  void dXiDXElement (int iGroup, int iElement, int iGaussPoint, fullMatrix<double> &jac) const;
  void dXiDXInterface (int iGroup, int iConnection, int iFace, int iGaussPoint, fullMatrix<double> &jac) const;
  inline const dgFullMatrix<double> &normal(int iGroup, int iConnection) const {return _interfaceNormals[iGroup][iConnection];}
  inline const fullVector<double> &interfaceSurface(int iGroup) const {return _interfaceSurface[iGroup];}
  inline const fullVector<double> &elementVolume(int iGroup) const {return _elementVolume[iGroup];}
  inline const fullVector<double> &elementInnerRadius(int iGroup)const {return _elementInnerRadius[iGroup];}
};

#endif
