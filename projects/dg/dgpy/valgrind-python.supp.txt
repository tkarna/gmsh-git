{
   PythonAddr4
   Memcheck:Addr4
   obj:*/python*
   ...
}

{
   PythonValue8
   Memcheck:Value8
   obj:*/python*
   ...
}

{
   PythonCond
   Memcheck:Cond
   obj:*/python*
   ...
}

{
  OpenMPIInitAddr8
  Memcheck:Addr8
  ...
  fun:opal_init
  ...
}

{
  OpenMPIInitParamsched_setaffinity
  Memcheck:Param
  sched_setaffinity(mask)
  ...
  fun:opal_init
  ...
}

{
  OpenMPIInitParamWritev
   Memcheck:Param
   writev(vector[...])
   fun:writev
   ...
   fun:PMPI_Init
   ...
}

{
  PythonProg
  Memcheck:Addr8
  ...
  fun:PySys_SetPath
  ...
}

{
  PythonProg
  Memcheck:Addr8
  ...
  fun:Py_GetProgramFullPath
  ...
}
