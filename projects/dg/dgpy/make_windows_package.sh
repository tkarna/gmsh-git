PACK=dg-win

rm -rf $PACK
mkdir $PACK

mkdir $PACK/lib

cp libDG.dll gmsh/libGmsh.dll @GFORTRAN_DLL@ @GCC_DLL@ @QUADMATH_DLL@ $PACK/lib

mkdir $PACK/gmshpy
cp gmsh/wrappers/gmshpy/__init__.py $PACK/gmshpy
for module in gmshCommon gmshGeo gmshMesh gmshNumeric gmshPost gmshSolver; do
  cp gmsh/wrappers/gmshpy/{${module}.py,_${module}.pyd} $PACK/gmshpy
done

mkdir $PACK/dgpy
cp dgpy/__init__.py $PACK/dgpy
for module in dgCore dgConservationLaws dgSlim proj; do
  cp dgpy/{${module}.py,_${module}.pyd} $PACK/dgpy
done
cp -r dgpy/scripts $PACK

cp dgpy/{rundgpy.py,idle.py} $PACK
zip -r $PACK $PACK
