import sys, os, subprocess, threading, signal, select
if sys.version_info[0] == 2 :
  from Tkinter import *
  import tkFileDialog
else :
  from tkinter import *
  import tkinter.filedialog as tkFileDialog

def getAppDataFile(APPNAME) :
  if sys.platform == 'darwin':
    from AppKit import NSSearchPathForDirectoriesInDomains
    return os.path.join(NSSearchPathForDirectoriesInDomains(14, 1, True)[0], APPNAME)
  elif sys.platform == 'win32':
    return os.path.join(os.environ['APPDATA'], APPNAME)
  else:
    return os.path.expanduser(os.path.join("~", "." + APPNAME))

class messagewindow:
    def __init__(self, parent, message):
        self.top = Toplevel(parent)
        self.top.title("")
        Label(self.top, text=message).pack()
        Button(self.top, text="OK", command=self.ok).pack(pady=5)
        self.top.focus_set()
        self.top.grab_set()
        self.top.transient(parent)
        self.top.wait_window(self.top)

    def ok(self):
        self.top.destroy()

class Application(Frame):
  def chooseFile(self):
    try :
      filePath = tkFileDialog.askopenfilename(initialfile=self.filename.get(), filetypes = [("python scripts", ".py")], title = "run dg" )
    except :
      filePath = tkFileDialog.askopenfilename(filetypes = [("python scripts", ".py")], title = "run dg" )
    if filePath :
      self.filename.set(filePath)
  def updateStatus(self):
    if self.process and self.process.poll() != None :
	self.process = None
    if self.process :
      self.runBtn["text"] = "Kill";
      self.threadEntry.config(state = "disabled")
      self.fileBtn.config(state = "disabled")
      self.threadLabel.config(state = "disabled")
      self.fileLabel.config(state = "disabled")
    else :
      self.runBtn["text"] = "Run";
      self.threadEntry.config(state = "normal")
      self.fileBtn.config(state = "normal")
      self.threadLabel.config(state = "normal")
      self.fileLabel.config(state = "normal")

  def logAppend(self, text, tag = None):
    atEnd =  self.log.yview()[1] == 1.
    self.log.config(state = NORMAL)
    self.log.insert(END, text, tag)
    self.log.config(state = DISABLED)
    if atEnd :
      self.log.yview(END)

  def logClear(self) :
    self.log.config(state = NORMAL)
    self.log.delete("1.0", END)
    self.log.config(state = DISABLED)

  def edit(self):
      command = [sys.executable, os.path.join(dgroot, "idle.py"), self.filename.get()]
      subprocess.Popen(command)
  def run(self):
    if self.process :
      self.process.terminate()
    else :
      if os.getenv("PYTHONPATH") :
        os.environ["PYTHONPATH"] += os.pathsep + dgroot
      else:
        os.environ["PYTHONPATH"] = dgroot
      os.environ["PYTHONPATH"] += os.pathsep + os.path.join(dgroot, "scripts")
      os.environ["PATH"] += os.pathsep + os.path.join(dgroot, "lib")
      def readThread(pipe, tag) :
	line = pipe.readline()
        while True :
          if not line :
            break
          self.logAppend(line, tag)
          line = pipe.readline()
        self.updateStatus()
            
      os.chdir(os.path.dirname(self.filename.get()))
      command = ["mpiexec",  "-n", str(self.nproc.get()), sys.executable, self.filename.get()]
      self.logClear()
      self.process = subprocess.Popen(command, bufsize = 1, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
      self.thread = threading.Thread(target = readThread, args = (self.process.stdout, "")).start()
      self.thread = threading.Thread(target = readThread, args = (self.process.stderr, "error")).start()
      self.updateStatus()

  def save(self) :
    filePath = tkFileDialog.asksaveasfilename(filetypes = [("text files", ".txt")], title = "save log as ..." )
    if filePath :
      try :
        f = open(filePath, "w")
        f.write(self.log.get("1.0", END))
        f.close()
      except :
        messagewindow(self, "cannot write to '%s'" % filePath)

  def createWidgets(self):
    self.fileLabel = Label(self, text="File : ")
    self.fileLabel.grid(row = 0, column = 0, sticky = E)
    self.fileBtn = Button(self, justify = LEFT, textvariable = self.filename, command = self.chooseFile)
    self.fileBtn.grid(row = 0, column = 1, sticky = E + W, columnspan = 3)
    self.threadLabel = Label(self, text="Number of threads : ")
    self.threadLabel.grid(row = 1, column = 0, sticky = E + N + S, pady = (5, 0))
    self.threadEntry = Spinbox(self, from_ = 1, to = 10, increment = 1, width=5, justify = CENTER, textvariable=self.nproc)
    self.threadEntry.grid(row = 1, column = 1, sticky = W + N + S, pady = (5, 0))
    frame = Frame(self)
    frame.grid(row = 1, column = 3)
    self.runBtn = Button(frame, text="Run", command = self.run)
    self.runBtn.pack(side = RIGHT, pady = (5, 0), padx=(5, 0))
    self.editBtn = Button(frame, text="Edit", command = self.edit)
    self.editBtn.pack(side = RIGHT, padx = (5, 0), pady=(5, 0))
    framelog = Frame(self)
    framelog.grid(row = 2, column = 0, columnspan = 4, sticky = E + W + S + N, pady=(5, 0))
    self.log = Text(framelog, state = DISABLED)
    self.log.tag_config("error", foreground= "red")
    #self.log.grid(row = 2, column = 0, columnspan = 4, sticky = E + W + S + N, pady=(5, 0))
    self.log.pack(side = LEFT, expand = True, fill = BOTH)
    scrollbar = Scrollbar(framelog)
    self.log.config(yscrollcommand=scrollbar.set)
    scrollbar.config(command=self.log.yview)
    scrollbar.pack(side = RIGHT, fill = Y)
    self.saveBtn = Button(self, text="Save log", command = self.save)
    self.saveBtn.grid(row = 3, column = 3, stick = E + S, pady = (5, 0))
    self.columnconfigure(2, weight = 1)
    self.rowconfigure(2, weight = 1)
    
  
  def quit(self) :
    if self.process :
      d = messagewindow(self, "Kill the job before closing the window.")
      return
    data = open(getAppDataFile("dg"), "w")
    data.write(self.filename.get() + "\n")
    data.write(str(self.nproc.get()) + "\n")
    self.master.quit()

  def __init__(self, master=None):
    Frame.__init__(self, master, padx = 5, pady = 5)
    self.grid(sticky = E + W + N + S)
    top=self.winfo_toplevel()
    top.columnconfigure(0, weight=1)
    top.rowconfigure(0, weight=1)
    self.nproc = IntVar()
    self.filename = StringVar()
    self.nproc.set(1)
    self.process = None
    try :
      data = open(getAppDataFile("dg"), "r").readlines()
      self.filename.set(data[0][:-1])
      self.nproc.set(int(data[1]))
    except :
      pass
    self.createWidgets()
    self.master.protocol("WM_DELETE_WINDOW", self.quit)

dgroot=os.path.abspath(os.path.dirname(__file__))
root = Tk()
root.title("DG")
root.minsize(400, 50)
root.iconbitmap(bitmap = None)
app = Application(master=root)
app.mainloop()
root.destroy()
