from dgpy import *
import time
import os

#python version 2.4 : not isnan and not .format (% things)
def isnan(num):
  return num != num

def printOnce(strg):
  if (Msg.GetCommRank() == 0):
    print (strg)

init_clock_cpu = 0.

def initClock():
  init_clock_cpu = time.clock()

def printInfos(it, dt, t, strg):
  if (Msg.GetCommRank() == 0):
    print ('|IT| %6d' % (it), '|DT| %g' % (dt), '|t| %8g' % (t), '|CPU|', '%.3e' % (time.clock()-init_clock_cpu), strg)

def makeLib(code, libName):
  makeDir("dylibs");
  newname = "dylibs/" + libName
  if (Msg.GetCommRank() == 0):
    functionC.buildLibrary(code, newname)
  Msg.Barrier()
  return newname

def makeDir(dirName):
  if(os.path.isdir(dirName)):
    return
  if (Msg.GetCommRank() == 0):
    try :
      os.mkdir(dirName)
    except :
      print ("failed creation of directory " + dirName + " !")
      
