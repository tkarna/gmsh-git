from dgpy import *
from Common import *
import math
import time
import os

#-------------------------------------------------
#-- Generalfunctions
#-------------------------------------------------
def areaF(FCT, sol):
  for i in range(0,FCT.size1()):
    phi = sol.get(i,0)
    value = 0.0
    if (phi < 0.0):
      value = 1.0
    FCT.set(i,0, value)
    
#-------------------------------------------------
#-- CLASS DEFINITION
#-------------------------------------------------
class levelset(object):
  def __init__(self, meshName, order, dimension, doAdapt, doFEM, doREInit):
    self.nbFields = 1
    self.order = order
    self.dimension = dimension
    self.models = []
    self.groups = []
    self.solutions = []
    self.A0 = 0.0
    self.iAdapt = 0
    self.typeOfMetric = "coupez"

    self.meshName = meshName
    self.currentName = self.meshName
    self.modelName = self.meshName+"Adapt"
    self.doAdapt = doAdapt
    self.doFEM = doFEM
    if (doFEM and order > 1):
      print ("Error: FEM simulation only with order 1 ! Change the order in your python file.")
      exit(1)
    self.doREInit = doREInit

    genMesh(self.meshName, dimension, order)
    self.models.append(GModel())
    self.models[0].load(self.meshName+msh)
    
    self.groups.append(dgGroupCollection(self.models[0], self.dimension, self.order)) 
    self.XYZ = function.getCoordinates()
    
#-------------------------------------------------
#-- Initialization of problem
#-------------------------------------------------

  def initLevelset(self, initF):
    self.solutions.append(dgDofContainer(self.groups[0],self.nbFields))
    self.maxRadius = self.groups[0].maxInnerRadius()
    self.E = 10.0*self.maxRadius
    self.INIT = functionPython(1, initF, [self.XYZ])
    self.solutions[0].interpolate(self.INIT)
    self.solutions[0].exportMsh('output/initLS', 0., 0)
    self.A0 = self.computeArea(0.0)

  def setVelocityFromNS(self, ns, funBC):
    self.evalNS = dgFunctionEvaluator(ns.groups, ns.law.getVelocity(), ns.solution)
    self.VELNEW = self.evalNS.newFunction(self.XYZ)
    self.setLaw(self.VELNEW, funBC)

  def setLaw(self, VELC, funBC):
    self.VELC = VELC
    self.FUNBC = funBC
    if (self.doFEM):
      #velDOF = dgDofContainer(self.groups[0], 3)
      #velDOF.interpolate(VELC)
      self.law = dgConservationLawAdvectionDiffusionSUPG.advectionLaw(VELC, VELC) #velDOF.getFunction(), velDOF.getFunctionGradient()) # #
      self.solutionPREV = dgDofContainer(self.groups[0], self.nbFields)
      self.law.setPreviousSolution(self.solutionPREV.getFunction())
      self.law.setPreviousSolutionGradient(self.solutionPREV.getFunctionGradient())
      if (self.doREInit):
        self.law.setDistance(self.E)
    else:
      self.law = dgConservationLawAdvectionDiffusion.advectionLaw(self.VELC)
    funBC(self)

  def initAdapt(self, typeOfMetric, lcMin, lcMax, eps):
    self.lcMin = lcMin
    self.lcMax = lcMax
    self.eps = eps
    self.typeOfMetric = typeOfMetric
  
#-------------------------------------------------
#-- Adapt mesh
#-------------------------------------------------
  def adaptMesh(self, jAdapt):
  
    self.currentName = self.modelName+"_%d" % (jAdapt+1)
    GmshSetOption('Mesh', 'Algorithm', 7.0) #7=bamg
    GmshSetOption('Mesh', 'LcIntegrationPrecision', 1.e-4)
    GmshSetOption('Mesh', 'CharacteristicLengthMin', self.lcMin)
    GmshSetOption('Mesh', 'CharacteristicLengthMax', self.lcMax)
    modelAdapt = GModel()
    modelAdapt.load(self.modelName+geo)
    evaluatorOfSolution = dgFunctionEvaluator(self.groups[jAdapt], self.solutions[jAdapt].getFunction())
    metric = dgMeshMetric ()
    if (self.typeOfMetric == "hessian"):
      metric.setHessian(self.groups[jAdapt], evaluatorOfSolution, self.eps)
    elif (self.typeOfMetric == "frey"):
      metric.setLevelsetFrey(self.groups[jAdapt], evaluatorOfSolution, self.eps)
    elif (self.typeOfMetric == "coupez"):
      metric.setLevelsetEval(self.groups[jAdapt], evaluatorOfSolution,  self.E)
    metric.setAsBackgroundMesh(modelAdapt)
    
    modelAdapt.mesh(self.dimension)
    modelAdapt.save(self.currentName+msh)
    
    self.models.append( GModel())
    self.models[jAdapt+1].load(self.currentName+msh)
    self.groups.append(dgGroupCollection(self.models[jAdapt+1], self.dimension, self.order))
    self.solutions.append(dgDofContainer(self.groups[jAdapt+1],self.nbFields))
    if (self.t == 0.0):
      self.solutions[jAdapt+1].interpolate(self.INIT)
    else:
      pm = dgMesh2MeshProjection(self.solutions[jAdapt],self.solutions[jAdapt+1])
      pm.projectFromTo(self.solutions[jAdapt],self.solutions[jAdapt+1])

    nNew = float(self.models[jAdapt+1].getNumMeshElements())
    nOld = float(self.models[jAdapt].getNumMeshElements())
    print ("---------- ADAPT %d (nbElems %d)----------" % (jAdapt+1, nNew))
    if ( math.fabs(nNew-nOld)/nOld > 0.06 and jAdapt < 4):
      self.adaptMesh(jAdapt+1)
      
#-------------------------------------------------
#-- Clean Adapt mesh
#-------------------------------------------------
  def cleanAdapt(self):
    kEnd = len(self.solutions)-1
    self.solutions[0] = self.solutions[kEnd]
    self.groups[0] = self.groups[kEnd]
    self.models[0] = self.models[kEnd]
    del self.solutions[1:kEnd+1]
    del self.groups[1:kEnd+1]
    del self.models[1:kEnd+1]
    
    cmd = "mv "+self.currentName+".msh "+"current.msh"
    os.system(cmd)
    self.currentName = "current"
    
    self.setLaw(self.VELC, self.FUNBC)
    self.initSolver(self.explicit, self.timeOrder, self.ATol,self.RTol, self.Verb, self.solverOptions, self.nbExport)
    
        
#-------------------------------------------------
#-- Area cmputation
#-------------------------------------------------

  def computeArea(self, t):
    fun = open("Area.dat","a")
    self.AREA = functionPython(1, areaF, [self.solutions[0].getFunction()])
    integ=dgFunctionIntegrator(self.groups[0], self.AREA)
    localArea=fullMatrixDouble(1,1)
    integ.compute(localArea)
    Aloc = localArea.get(0,0)
    ALoss = 0.0
    if (t > 0.0 and self.A0 != 0.0):
      ALoss = (Aloc-self.A0)/self.A0
    print ('Area: ', Aloc, 'Loss: ', ALoss)
    fun.write(' %g %g %g\n'% (t, Aloc, ALoss))
    fun.close()
    return Aloc
        
#-------------------------------------------------
#-- Unsteady solver
#-------------------------------------------------

  def initSolver(self, explicit, timeOrder, ATol,RTol, Verb, solverOptions, nbExport):
    self.t = 0.0
    self.explicit = explicit
    self.timeOrder = timeOrder
    self.ATol = ATol
    self.RTol = RTol
    self.Verb = Verb
    self.solverOptions = solverOptions
    self.nbExport = nbExport
    if (self.doFEM):
      self.petsc =  linearSystemPETScDouble()
      self.petsc.setParameter("matrix_reuse", "same_matrix")
      self.petsc.setParameter("petscOptions", solverOptions)
      self.dof = dgDofManager.newCG(self.groups[0], self.nbFields, self.petsc)
    else:
      self.petsc = linearSystemPETScBlockDouble()
      self.petsc.setParameter("petscOptions", solverOptions)
      self.dof = dgDofManager.newDGBlock(self.groups[0], self.nbFields, self.petsc)

    self.nbExport = nbExport
    if (self.explicit):
      self.solver = dgERK(self.law, self.dof, timeOrder)
    else:
      self.solver = dgDIRK(self.law, self.dof, timeOrder)
      self.solver.getNewton().setAtol(ATol)
      self.solver.getNewton().setRtol(RTol)
      self.solver.getNewton().setVerb(Verb)

  def doOneIterationDT(self, dt):
    self.t = self.t+dt
    if (self.doFEM):
      self.solutionPREV.copy(self.solutions[0]) #function.getSolution()) #self.solutions[0])
      self.groups[0].buildStreamwiseLength(self.law.getVelocity())

    self.solver.iterate(self.solutions[0], dt, self.t)

    if (self.explicit):
      res = self.solutions[0].norm()
    else:
      res = self.solver.getNewton().getFirstResidual()

    return res
          
  def doOneIteration(self, dt, i):
    tFinal = self.t +dt
    norm = 0.0
    if (self.doAdapt):
      while (self.t < tFinal):
        tOld = self.t

        self.adaptMesh(0)
        self.cleanAdapt()

        self.t = tOld
        
        minV=fullMatrixDouble(3,1)
        maxV=fullMatrixDouble(3,1)
        vel = dgDofContainer(self.groups[0], 3)
        vel.interpolate(self.VELC)
        vel.minmax(minV, maxV)
        VMax = math.sqrt(maxV(0,0)*maxV(0,0)+maxV(1,0)*maxV(1,0));
        dt_adapt = tFinal - self.t
        if (VMax != 0.0):
          dt_adapt = min(0.9*self.E/VMax, dt, tFinal-self.t)
        norm = self.doOneIterationDT(dt_adapt)
        self.iAdapt = self.iAdapt+1
    else:
      if (self.explicit):
        tStep = self.t + dt
        while (self.t < tStep):
          dt_loc = min(dt, 0.9*self.solver.computeInvSpectralRadius(self.solutions[0]), tStep-self.t)
          norm = self.doOneIterationDT(dt_loc)
      else:
        norm = self.doOneIterationDT(dt)
      

    if (i % self.nbExport == 0):
      if (self.doAdapt):
        simuName = self.meshName+"_simu%d"%self.iAdapt+".msh"
        cmd = "cp "+self.currentName+".msh "+simuName
        os.system(cmd)  
      print('--LS-- |ITER|', i, '|MESH|', self.iAdapt, '|NORM|',norm,'|DT|',dt, '|T|', self.t)
      outputstr = "output/ls-mesh%d-%06d" % (self.iAdapt,i/self.nbExport)
      self.solutions[0].exportMsh(outputstr, self.t, i//self.nbExport)
      Aloc  = self.computeArea(self.t)


  def doOneExplicitIteration(self, i):
  
    dt = 0.8 * self.solver.computeInvSpectralRadius(self.solutions[0])*10;  
    norm = self.solver.iterate(self.solutions[0], dt, self.t) 
    self.t = self.t+dt
    
    if (i % self.nbExport == 0):
      print('--LS-- |ITER|', i, '|NORM|',norm,'|DT|',dt, '|T|', self.t)
      outputstr = "output/ls-mesh%d%06d" % (self.iAdapt,i/self.nbExport)
      self.solutions[0].exportMsh(outputstr, self.t, i//self.nbExport)    
      Aloc  = self.computeArea(self.t)


