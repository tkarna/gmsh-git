from extrude import *
import os.path

# Does not work for order != 1

def readPeriodicBoundaries(filename) :
  mappingPer = {}
  mappingEdge2Nodes = {}
  f = open(filename, "r")
  perBndNum = int(f.readline())
  for iPerBnd in range(perBndNum) :
    perBndEdgeNum = int(f.readline())
    for iPerBndEdge in range(perBndEdgeNum) :
      l = f.readline().split()
      mappingPer[int(l[0])] = [int(l[1]), iPerBnd]
      mappingEdge2Nodes[int(l[0])] = []
      n1 = int(l[2])
      for i in range(n1) :
        mappingEdge2Nodes[int(l[0])].append(int(l[3+i]))
      mappingEdge2Nodes[int(l[1])] = []
      n2 = int(l[3+n1])
      for i in range(n2) :
        mappingEdge2Nodes[int(l[1])].append(int(l[4+n1+i]))
  return (mappingPer, mappingEdge2Nodes, perBndNum)

def extrudePeriodic(fileName, getLayers, inputPeriodicFilename, outputPeriodicFilename) :
  extrudedMesh = extrude(fileName, getLayers)
  
  if not os.path.isfile(inputPeriodicFilename) :
    print("Error : There is no file : %s " % inputPeriodicFilename)
  (mapPer, mapEdge2Nodes, perBndNum) = readPeriodicBoundaries(inputPeriodicFilename)
  numPerNeighb = [0 for i in range(perBndNum)]
  

  for k, v in mapPer.items() :
    numPerNeighb[v[1]] += len(extrudedMesh.columns[k])

  f = open(outputPeriodicFilename, "w")
  f.write("%i\n" % perBndNum)
  for iPerBndNum in range(perBndNum) :
    f.write("%i\n" % numPerNeighb[iPerBndNum])
    for k, v in mapPer.items() :
      if v[1] == iPerBndNum : 
        for i in range(len(extrudedMesh.columns[k])) :
          f.write("%i %i " % (extrudedMesh.columns[k][i].tag, extrudedMesh.columns[v[0]][i].tag))
          numNodes = len(mapEdge2Nodes[k]) * (1 if i==0 else 2)
          f.write("%i " % numNodes)
          for j in range(numNodes) : 
            f.write("%i " % extrudedMesh.verticals[mapEdge2Nodes[k][j%len(mapEdge2Nodes[k])]][i-(j // len(mapEdge2Nodes[k]))][-1])
          numNodes = len(mapEdge2Nodes[v[0]]) * (1 if i==0 else 2)
          f.write("%i " % numNodes)
          for j in range(numNodes) : 
            f.write("%i " % extrudedMesh.verticals[mapEdge2Nodes[v[0]][j%len(mapEdge2Nodes[k])]][i-(j // len(mapEdge2Nodes[k]))][-1])
          f.write("\n")
  
  return extrudedMesh
