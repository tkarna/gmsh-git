import sys

def hsv2rgb(h, s, v) :
  v = v * 255
  if s == 0 :
    return [v, v, v]
  ti = int(h/60) % 6
  f = float(h)/60 - ti
  l = round(v * (1 - s))
  m = round(v * (1 - s * f))
  n = round(v * (1 - s * (1 - f)))
  if ti == 0 :
    return [v, n, l]
  elif ti == 1 :
    return [m, v, l]
  elif ti == 2 :
    return [l, v, n]
  elif ti == 3 :
    return [l, m, v]
  elif ti == 4 :
    return [n, l, v]
  elif ti == 5 :
    return [v, l, m]

out = sys.stdout
out.write("View[0].ColorTable = {")
nsample = 512
for i in range(nsample) :
  f = i / float(nsample - 1)
  if  f < 0.5 :
    rgb = hsv2rgb(240, 1, f * 2)
  else :
    rgb = hsv2rgb(240, 2 * (1 - f), 1)
  out.write("{%d, %d, %d}" % (rgb[0], rgb[1], rgb[2]))
  if i != nsample - 1 :
    out.write(", ")
out.write("};\n")

