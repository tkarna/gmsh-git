from  mesh import *
import gmshType
import sys
import collections


def extrude(mesh2d, getLayers) :
  extrudedMesh = mesh()

  class extrudedVertical(list) :
    def v(self, z, ho = 0):
      return self[z * order + ho]

  def newExtrBnd(nodes, z, extrBnd, partition) :
    if (extrBnd.dimension == 1):
      return newLineV(nodes, z, extrBnd, partition)
    else:
      return newQuadrangleV(nodes, z, extrBnd, partition)

  def newElementH(t, nodes, z, entity, partition):
    return extrudedMesh.newElement(t, partition, entity, [node.v(z) for node in nodes])
    
  def newLineV(nodes, z, edge, partition) :
    return extrudedMesh.newElement(gmshType.TYPE_LIN.mshType[order], partition, edge, [nodes[0].v(z), nodes[0].v(z+1)] + [nodes[0].v(z, i + 1) for i in range(order - 1)])

  def addLine(rnodes, ix, iz, jx, jz):
    n=max(abs(jx - ix),abs(jz - iz))
    dx = (jx - ix) // n
    dz = (jz - iz) // n
    return [rnodes[ix + dx * i].v(z, iz + dz * i) for i in range(1,n)]

  def addQuad(rnodes, j = 0, shift = 0):
    order = len(rnodes) - 1
    allp = [(j, j + shift), (order - j, j + shift), (order - j, order - j + shift), (j, order - j + shift)]
    if j * 2 == order :
      return [rnodes[order // 2].v(z, order // 2 + shift)]
    v = [rnodes[p[0]].v(z, p[1]) for p in allp]
    for i in range(4):
      v += addLine(rnodes, allp[i][0], allp[i][1], allp[(i + 1) % 4][0], allp[(i + 1) % 4][1])
    if (j < order // 2):
      v += addQuad(rnodes, j + 1, shift)
    return v

  def addHex(rnodes, shift = 0):
    def reverseQuad(v) :
      iorder = int(len(v)**(0.5)) - 1
      if iorder > 0 :
        v[1], v[3] = v[3], v[1]
        v[4 : 4 * iorder] = reversed(v[4 : 4 * iorder])
        v[4 * iorder :] = reverseQuad(v[4 * iorder :])
      return v
    def rotateQuad(v) :
      iorder = int(len(v)**(0.5)) - 1
      if iorder > 0 :
        v[: 4] = v[1 : 4] + [v[0]]
        v[4 : 4 * iorder] = v[3 + iorder :  4 * iorder] + v[4 : 3 + iorder]
        v[4 * iorder :] = rotateQuad(v[4 * iorder :])
      return v
    iorder = int(len(rnodes)**(0.5)) - 1
    if iorder == 0 :
      return [rnodes[0].v(z, shift)]
    v = [rnodes[p[0]].v(z, shift + p[1] * iorder) for p in [(0, 0), (1, 0), (2, 0), (3, 0), (0, 1), (1, 1), (2, 1), (3, 1)]]
    if iorder > 1 :
      n = iorder - 1
      v += [rnodes[4 + i].v(z, shift) for i in range(n)]                                #8
      v += [rnodes[4 + 4 * n - 1 - i].v(z, shift) for i in range(n)]                    #9
      v += [rnodes[0].v(z, shift + i + 1) for i in range(n)]                            #10
      v += [rnodes[4 + i + n].v(z, shift) for i in range(n)]                            #11
      v += [rnodes[1].v(z, shift + i + 1) for i in range(n)]                            #12
      v += [rnodes[4 + i + 2 * n].v(z, shift) for i in range(n)]                        #13
      v += [rnodes[2].v(z, shift + i + 1) for i in range(n)]                            #14
      v += [rnodes[3].v(z, shift + i + 1) for i in range(n)]                            #15
      v += [rnodes[4 + i].v(z, shift + iorder) for i in range(n)]                       #16
      v += [rnodes[4 + 4 * n - i - 1].v(z, shift + iorder) for i in range(n)]           #17        
      v += [rnodes[4 + n + i].v(z, shift + iorder) for i in range(n)]                   #18
      v += [rnodes[4 + 2 * n + i].v(z, shift + iorder) for i in range(n)]               #19
      v += reverseQuad([rnodes[4 + 4 * n + i].v(z, shift) for i in range (n * n)])      #20
      v += addQuad([rnodes[4 + i] for i in range(n)], 0, shift + 1)                     #21
      v += rotateQuad(addQuad([rnodes[4 + 3 * n + i] for i in range(n)], 0, shift + 1)) #22
      v += addQuad([rnodes[4 + i + n] for i in range(n)], 0, shift + 1)                 #23
      v += addQuad([rnodes[4 + i + 2 * n] for i in range(n)], 0, shift + 1)             #24
      v += [rnodes[4 + 4 * n + i].v(z, shift + iorder) for i in range (n * n)]          #25
      v += addHex(rnodes[4* iorder:], shift + 1)                                        #26
    return v;
    
  def newQuadrangleV(nodes, z, face, partition) :
    return extrudedMesh.newElement(gmshType.TYPE_QUA.mshType[order], partition, face, addQuad([nodes[0]] + [nodes[i + 2] for i in range (order - 1)] + [nodes[1]]))

  def newHexahedron(nodes, z, face, partition) :
    return extrudedMesh.newElement(gmshType.TYPE_HEX.mshType[order], partition, face, addHex(nodes))

  def newPrism(nodes, z, region, partition) :
    if (order == 1):
      return  extrudedMesh.newElement(gmshType.MSH_PRI_6, partition, region, (nodes[0].v(z), nodes[1].v(z), nodes[2].v(z), nodes[0].v(z + 1), nodes[1].v(z + 1), nodes[2].v(z + 1)))
    elif (order == 2):
      return extrudedMesh.newElement(gmshType.MSH_PRI_18, partition, region, (nodes[0].v(z), nodes[1].v(z), nodes[2].v(z), nodes[0].v(z+1), nodes[1].v(z+1), nodes[2].v(z+1), nodes[3].v(z), nodes[5].v(z), nodes[0].v(z, 0), nodes[4].v(z), nodes[1].v(z, 0), nodes[2].v(z,0), nodes[3].v(z+1), nodes[5].v(z+1), nodes[4].v(z+1), nodes[3].v(z, 1), nodes[5].v(z, 1), nodes[4].v(z, 1)))
    print("Prism extrusion not implemented for order > 2")

  def newExtrudedElement(eType, nodes, z, gExtr, partition):
    if (eType == gmshType.TYPE_LIN):
      return newQuadrangleV(nodes, z, gExtr, partition)
    elif (eType == gmshType.TYPE_TRI):
      return newPrism(nodes, z, gExtr, partition)
    elif (eType == gmshType.TYPE_QUA):
      return newHexahedron(nodes, z, gExtr, partition)
    else:
      print("cannot extrude element of type : %i" % eType)

  dim2d = max(e.dimension for e in mesh2d.entities)
  order = max(max(e.etype.order for e in entity.elements) for entity in mesh2d.entities)

  extrudedMesh.useFormat3 = mesh2d.useFormat3
  extrudedMesh.verticals = {}
  extrudedMesh.columns = {}
  verticals = extrudedMesh.verticals
  columns = {}
  mesh2d.genFaces(dim2d, "vertical_bottom")
  flayers = collections.defaultdict(list)
  for entity in mesh2d.entities :
    if entity.dimension == dim2d :
      tag = ("_" + mesh2d.getPhysicalName(dim2d, entity.physicals[0])) if entity.physicals else ""
      if tag == "_" : tag = ""
      extrudedEntity = extrudedMesh.newEntity(dim2d + 1, "volume" + tag)
      topEntity = extrudedMesh.newEntity(dim2d, "top" + tag)
      bottomEntity = extrudedMesh.newEntity(dim2d, "bottom" + tag)
      for e in entity.elements :
        nLayersMin = sys.maxsize
        for v in e.vertices :
          layers = getLayers(e, v)
          if not v[3] in verticals :
            verticals[v[3]] = extrudedVertical()
            verticals[v[3]].append(extrudedMesh.newVertex(v[0], v[1], -layers[0]))
          extruded = verticals[v[3]]
          for i in range((len(extruded) - 1) // order, len(layers) - 1) :
            for j in range(1, order + 1) :
              extruded.append(extrudedMesh.newVertex(v[0], v[1], -(layers[i] * (order - j) + layers[i + 1] * j) * 1./ order))
          nLayersMin = min(len(layers) - 1, nLayersMin)
        if (nLayersMin < 1) :
          print("error : at least 2 vertices are needed")
        for f in e.faces :
          flayers[f.tag].append(nLayersMin)
        nodes = [verticals[v[3]] for v in e.vertices]
        col = []
        col.append(newElementH(e.etype, nodes, 0, topEntity, e.partition))
        for z in range(nLayersMin):
          col.append(newExtrudedElement(e.etype.baseType, nodes, z, extrudedEntity, e.partition))
        col.append(newElementH(e.etype, nodes, nLayersMin, bottomEntity, e.partition))
        extrudedMesh.columns[e.tag] = col
  for entity in mesh2d.entities :
    if entity.dimension == dim2d - 1:
      tag = (mesh2d.getPhysicalName(dim2d - 1, entity.physicals[0])) if entity.physicals else ""
      extrudedEntity = extrudedMesh.newEntity(dim2d, tag)
      topEntity = extrudedMesh.newEntity(dim2d - 1, tag)
      for f in entity.elements :
        layers = flayers[f.tag]
        zmin = min(layers) if tag == "vertical_bottom" else 0
        zmax = max(layers)
        nodes = [verticals[v[3]] for v in f.vertices]
        col = []
        if tag != "vertical_bottom" :
          col.append(newElementH(f.etype, nodes, 0, topEntity, f.partition))
        for z in range(zmin, zmax):
          col.append(newExtrBnd(nodes, z, extrudedEntity, f.partition))
        if tag != "vertical_bottom" :
          extrudedMesh.columns[f.tag] = col
  return extrudedMesh
