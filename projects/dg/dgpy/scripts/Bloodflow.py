from dgpy import *
from Common import *
import math
import time
import subprocess
import os
import sys


#-------------------------------------------------
#-- Global Parameters
#-------------------------------------------------
TIME = function.getTime()

#-------------------------------------------------
#-- General functions
#-------------------------------------------------
def Compute_KR(ViscDyn,ViscLaw, rho): 
    if (ViscLaw == 'PoiseuilleProfile') :
        KR = 8*math.pi*ViscDyn/rho
    elif (ViscLaw == 'FlatProfile') :
        KR = 22*math.pi*ViscDyn/rho    
    return KR

def exportTxtData(model, groups, solution, outDir, tcurrent, evaluator, iiter, fileName, outList):
    
    for k in range(len(outList)):
        name = '%s/%s-%s.txt' % (outDir, fileName, outList[k])
        fun = open(name,"a")
        fun2 = open('%s/plot%s-%s.gnu' % (outDir, fileName, outList[k]),"a")
        if (iiter == 0):
            fun.write("#(1) Time (2) Proximal Value (3) Medial Value (4) Distal Value\n")
            fun2.write("set xlabel 'time (s)'\n")
            fun2.write("set ylabel '%s' \n" % fileName)
            fun2.write("plot '%s' u 1:($2) w l t 'P', '%s' u 1:($3) w l t 'M', '%s' u 1:($4) w l t 'D'" %(name, name, name))
        fun.write("%g "% tcurrent)
            
        myEval = dgFunctionEvaluator(groups, evaluator, solution)
        res = fullMatrixDouble()
           
        nums = model.getEdgesByStringTag(outList[k]) 
        edge = model.getEdgeByTag(nums[0])
        umin = edge.getLowerBound()
        umax = edge.getUpperBound()
        pOld = edge.point(umin)
        s = 0.0
        N = 2
        for i in range (0,N+1) :
            u = umin + (i*1.)/(N*1.) * (umax - umin)
            p = edge.point(u)
            myEval.compute(p.x(),p.y(), p.z(), res)
            pOld = p
            fun.write("%g " % (res.get(0,0)))
                
        fun.write("\n")
        fun.close()


#---------------------------------------------------
#-- Fonction C++
#---------------------------------------------------

tmpLib = "./tmp.dylib"
functionC.buildLibraryFromFile(os.getenv("DG_SCRIPTS_DIR") + "/BloodC.c", tmpLib)

VELFILE      = functionC(tmpLib,"velocityFileC",1, [TIME])
inlet_pres   = functionC(tmpLib,"inletPresC",   1, [TIME])
inlet_vel    = functionC(tmpLib,"inletVelC",    1, [TIME])
inlet_vel2   = functionC(tmpLib,"inletVel2C",   1, [TIME])

#-------------------------------------------------
#-- Initialization of problem
#-------------------------------------------------

def initializeIncomp(meshName,order, Flux, rho, ViscDyn, ViscLaw, p0, data_Dc, data_Leakage ):
    dimension = 1

    genMesh(meshName, dimension,order)
    model = GModel()
    model.load (meshName+geo)
    model.load (meshName+msh)
        
    curv1D = dgSpaceTransform1DCurvilinear()
    curv1D.thisown= 0
    groups = dgGroupCollection(model, dimension, order, curv1D)
    groups.splitGroupsByPhysicalTag()

    law = dgConservationLawNavierStokes1d()
    law.setRiemannSolver(Flux)

    KR = Compute_KR(ViscDyn,ViscLaw, rho)
    MyKR = functionConstant([KR])
    MyKR.thisown = 0
    law.setLinearDissipation(MyKR)
    
    MyS = functionConstantByTag(data_Leakage)
    MyS.thisown = 0
    law.setSourceMass(MyS)

    law.setBloodFormulation()
    law.setDensity(rho)
    law.setEquilibriumPressure(p0)

    DcData = functionConstantByTag(data_Dc)
    DcData.thisown = 0
    law.setDiameterAndCelerity(DcData)

    INIT = functionConstant( [0., 0. ])
    INIT.thisown = 0
    solution = dgDofContainer(groups, law.getNbFields())
    solution.interpolate(INIT)
    solution.exportGroupIdMsh()

    return (model, groups, law, solution)
    
#-------------------------------------------------
#-- Functions for boundary conditions
#-------------------------------------------------

def inletAbsorbingVelocityBoundaryCondition(law, nameBC, fct):
    Tmp = law.newInletAbsorbingVelocity(fct)
    law.addBoundaryCondition(nameBC, Tmp)
    
def inletReflectingVelocityBoundaryCondition(law,nameBC, fct):
    law.addBoundaryCondition(nameBC, law.newInletReflectingVelocity(fct))

def inletAbsorbingPressureBoundaryCondition(law,nameBC, fct):
    law.addBoundaryCondition(nameBC, law.newInletAbsorbingPressure(fct))

def wallBoundaryCondition(law,nameBC):
    law.addBoundaryCondition(nameBC, newBoundaryWall())

def outletTerminalResistanceBoundaryCondition(law,nameBC, fct):
    law.addBoundaryCondition(nameBC, law.newOutletTerminalResistance(fct))
   
def outletWindkesselBoundaryCondition(law,nameBC, fct):
    law.addBoundaryCondition(nameBC, law.newOutletWindkessel(fct))

#-------------------------------------------------
#-- Solvers for NS
#-------------------------------------------------

def unsteadySolve(explicit, model, groups, law, solution, tFinal, dt_export, outDir, outList):

    print('*** solve ***')

    if (explicit):
        solver = dgERK(law, None, DG_ERK_44)
    else:
        print ('WARNING: you CAN NOT use implicit')
        print ('Windkessel solver makes assumption we have RK44 ')
        print ('Change conservation law (WK solver) in order to use implicit')
        exit(1)
        solverOptions = "-ksp_rtol 1.e-3 -pc_type ilu -pc_factor_levels 2"
        petsc = linearSystemPETScBlockDouble()
        petsc.setParameter("petscOptions", solverOptions)
        dof = dgDofManager.newDGBlock(groups, law.getNbFields(), petsc)
        solver = dgDIRK(law, dof, 2)     #timeorder
        solver.getNewton().setAtol(1.e-5)#ATol
        solver.getNewton().setRtol(1.e-8)#Rtol
        solver.getNewton().setVerb(1)    #Verbosity
        
    i = 0 
    tic =  time.clock()
    t=0
    norm = 0.0
    CFL = 0.5
    while (t < tFinal):
        if (explicit):
            tStep = t + dt_export
            while (t < tStep):
                dt_loc = min(dt_export, CFL*solver.computeInvSpectralRadius(solution), tStep-t)
                solver.iterate(solution, dt_loc, t)
                norm = solution.norm()
                t = t + dt_loc
        else:
            solver.iterate(solution, dt_export , t)
            norm = solver.getNewton().getFirstResidual()
            t = t + dt_export

        print ('|ITER|',i,'|NORM|',norm, '|T|', t, '|CPU|',time.clock() - tic)
        #solution.exportMsh('%s/sol-%06d' % (outDir, i), t, i)
        groups.exportFunctionMsh(law.getPressure(),'%s/pres-%06d' % (outDir, i), t, i, '', solution)
        groups.exportFunctionMsh(law.getArea(),'%s/area-%06d' % (outDir, i), t, i, '', solution)
        #groups.exportFunctionMsh(law.getFlowRate(),'%s/flow-%06d' % (outDir, i), t, i, '', solution)
        
        exportTxtData(model, groups, solution, outDir, t, law.getPressure(), i, 'pressure', outList)
        exportTxtData(model, groups, solution, outDir, t, law.getVelocity(), i, 'velocity', outList)
        exportTxtData(model, groups, solution, outDir, t, law.getArea()    , i, 'area',     outList)
        exportTxtData(model, groups, solution, outDir, t, law.getFlowRate(), i, 'flowrate', outList)
        
        i = i + 1
        

