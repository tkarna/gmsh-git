from SWNI import *
from Richards import *
import os
import utilities as u
import gmshPartition as partition

# Shallow water-Richards coupling
# Legend : C=container, F=function of container, G=grad of container, R=richards, S=Shallow-water

class SWGWOptions(object):
  def __init__(self, ROptions=None, SOptions=None, rewrite2DMesh=False, fluxType=0, RichardsExplicit=True, surfaceImposed=None,
                     RichardsImplicitAdaptive=True, VDKSurfaceFlux=None, couplingLength=1e-4, meanCplgFlux=False, scale=1.):
    self.rewrite2DMesh = rewrite2DMesh
    self.fluxType = fluxType # -1=desactivate coupling, 0=home-made, 1=vanderkwaak 2=vanderkwaak implicit
    if(fluxType==2): print "warning: bad behavior in sub iterations of h !"
    self.couplingLength = couplingLength # for vanderkwaak flux
    self.RichardsExplicit = RichardsExplicit
    self.surfaceImposed = surfaceImposed
    self.RichardsImplicitAdaptive = RichardsImplicitAdaptive
    self.meanCplgFlux = meanCplgFlux
    self.R = ROptions if ROptions else RichardsOptions()
    self.S = SOptions if SOptions else SWNIOptions()
    self.scale = scale

class SWGW(object):

  # constructor: set the mesh
  # init could be split mainly to catch S.XYZ coordinates to define source terms (i.e. rain)

  def __init__(self, MeshName3D, soilMap=None, landMap=None, options=None, rainFlux=None,
                    surfaceTags=['surface'], finalizeInitialisation=True):

    self.options = options if options else SWGWOptions()
    self.gravitation = functionConstant([0,0,1]) if not self.options.R.gravitation else self.options.R.gravitation

    self.model = GModel()
    if(os.path.exists(MeshName3D + '.geo')):
      self.model.load(MeshName3D + '.geo')
    MeshName3D = partition.simple(MeshName3D)
    self.model.load(MeshName3D)
    self.model.scaleMesh(self.options.scale)
    

    # Laws #
    ########

    if(self.options.RichardsExplicit):
      self.R = RichardsExplicit(MeshName3D, soilMap, self.options.R, finalizeInitialisation, self.model)
    else:
      self.R = RichardsImplicit(MeshName3D, soilMap, self.options.R, finalizeInitialisation, self.model)
    dim = self.R.model.getDim()

    '''
    MeshName2D = MeshName3D.replace('.msh', '_surf.msh')
    if self.options.rewrite2DMesh or not os.path.isfile(MeshName2D) or os.stat(MeshName3D).st_mtime > os.stat(MeshName2D).st_mtime:
      if(dim == 2):
        sModel = extractSurfaceLine(self.R.model, surfaceTags);
      elif(dim == 3):
        sModel = extractSurface(self.R.model, surfaceTags);
      else:
        print "error: dim cannot be", dim
        exit()
      if(Msg.GetCommSize() > 1):
        partitionOpt = meshPartitionOptions()
        partitionOpt.setNumOfPartitions(Msg.GetCommSize())
        PartitionMesh(sModel, partitionOpt)
      sModel.save(MeshName2D)
    '''
    self.surfaceTags = surfaceTags

    self.S = SWNI("", landMap, None, self.options.S, finalizeInitialisation, self.model, self.surfaceTags)

    self.minl = functionConstant(getMinL(self.S.gc))

    if(self.options.S.isImplicit and self.options.S.clip):
      self.limiterS = dgLimiterClip(self.S.law)

    self.nbSubH = 1
    self.nbSubW = 1

    # generic functions
  
    self.areaModC     = dgDofContainer(self.S.gc, 1)
    self.copySurfHC   = dgDofContainer(self.S.gc, 1)
    self.areaModF     = self.areaModC.getFunction()
    self.copySurfHF   = self.copySurfHC.getFunction()

    self.DT = function.getDT()
    self.DT_coupled = functionConstant(1)
    self.TIME = function.getTime()

    self.C0 = functionConstant(0.)

    self.libName = u.makeLib(self.CCode(), "tmpSWGW.dylib")

    #flatArea = functionC(self.libName, "flatArea", 1, []) # flat surf area !! (not physical area)
    #areaMod = functionQuotientNew(self.S.areaF, flatArea)
    areaMod = functionC(self.libName, "modArea", 1, [])
    self.areaModC.interpolate(areaMod)

#    self.areaModC.exportMsh("output/debugAreaMod", 1., 1, "AreaMod")

    if(finalizeInitialisation):
      self.finalizeInit(rainFlux)
    else:
      if(rainFlux or self.options.surfaceImposed):
        Msg.Warning("rainFlux or surfaceImposed not taken into account: give them to the method finalize")

  def finalize(self, soilMap, landMap=None, rainFlux=None):
    self.R.finalizeInit(soilMap)
    if(not self.options.surfaceImposed):
      if(landMap==None):
        Msg.Fatal("define landMap in SWGW")
      self.S.finalizeInit(landMap, source=None)
    self.finalizeInit(rainFlux)

  def finalizeInit(self, rainFlux=None):
    if(rainFlux):
      self.rain = rainFlux
    else:
      self.rain = self.C0
    if(not self.options.RichardsExplicit and self.options.RichardsImplicitAdaptive):
      # replace the cpling algo to add the coupling computation
      if(self.options.R.constantKlin):
        self.R.preProc = processPython(preProcFuncCplgNoLinK)
      else:
        self.R.preProc = processPython(preProcFuncCplg)
      self.R.newtonKrabbenhoft.setPreProc(self.R.preProc)
      
    # Coupling #
    ############

    if(self.options.surfaceImposed and rainFlux):
      self.massAvailS = functionC(self.libName, "massAvailSurfImposed", 1, [self.S.HF, self.DT_coupled, self.rain])
    else:
      self.massAvailS = functionC(self.libName, "massAvail", 1, [self.S.HF, self.DT_coupled, self.rain])

    self.ksat  = self.R.sm.getFunction("Ks")
    self.massAvailModRS = functionQuotientNew(self.massAvailS, self.areaModF)

    self.cplgFunction = None

    if self.options.fluxType == 0:  #home-made coupling (DG-type)
      if(self.options.meanCplgFlux):
        Msg.Fatal("meanCplgFlux not implemented for fluxType=0")

      # stock constants
      self._normals = functionC(self.libName, "normals", 3, [self.S.bathG, self.gravitation]) #TODO cleaner
      self._normalsC = dgDofContainer(self.S.gc, 3);
      self._normalsC.L2Projection(self._normals)
      self._normalsS = self._normalsC.getFunction()
      self._muFactor = functionC(self.libName, "muFactor", 1, [self.minl])
      self._muFactorC = dgDofContainer(self.S.gc, 1);
      self._muFactorC.fromElsewhereL2Projection(self._muFactor, self.R.gc, [])
      self._muFactorS = self._muFactorC.getFunction()

      self._richFlux = functionC(self.libName, "richardsFlux", 3, [self.R.HG, self.gravitation, self.ksat])
      self._nu = functionExtractCompNew(self.ksat, 8) #TODO tmp

      self._cplgFunction = dgFunctionCoupledC(self.libName, "flux_dg", 1,
                      [self.R.HF, self.S.HF, self._normalsS, self._nu,  self._richFlux, self._muFactorS, self.massAvailModRS],
                      [self.R.gc, self.S.gc, self.S.gc,      self.R.gc, self.R.gc,      self.S.gc      , self.S.gc])

      if(self.options.surfaceImposed and rainFlux):
        self.cplgFunction = functionMaxNew(self._cplgFunction, 0.)
      else:
        self.cplgFunction = self._cplgFunction
      '''
      if(self.options.RichardsExplicit):
        #self.WSurfR = self.R.sm.getFunction("retention", ["pressure"], [self.R.HF])
        #self.cplgFluxWR = self.R.laws['D'].newSurfaceFlux(self.normalsR, self.WSurfR)
        self.cplgFluxWR = functionConstant(0)
        self.cplgFluxR = functionSumNew(self.cplgFluxHR, self.cplgFluxWR)
      else:
        self.cplgFluxR = self.cplgFluxHR '''

    elif self.options.fluxType == 1:  # vanderkwaak coupling type

      self._couplLenght = functionConstant(self.options.couplingLength)
      if(self.options.S.microTopo):
        self._kro = self.S._kro
      else:
        self._kro = functionConstant(1.)
      #self.cplgFunction = dgFunctionCoupledC(self.libName, "flux_dummy", 1, [self.C0, self.C0], [self.S.gc, self.R.gc])
      
      if(not self.options.meanCplgFlux):
        self.cplgFunction = dgFunctionCoupledC(self.libName, "flux_vdk", 1,
              [self.R.KlinF, self._couplLenght, self.ksat, self.R.HF, self.S.HF, self._kro, self.massAvailModRS],
              [self.R.gc,    self.S.gc,         self.R.gc, self.R.gc, self.S.gc, self.S.gc, self.S.gc])
      else:
        self.cplgFunction = dgFunctionCoupledC(self.libName, "flux_vdk_mean", 1,
              [self.R.KlinF, self._couplLenght, self.ksat, self.R.HF, self.S.HF, self._kro, self.massAvailModRS],
              [self.R.gc,    self.S.gc,         self.R.gc, self.R.gc, self.S.gc, self.S.gc, self.S.gc])

    elif self.options.fluxType == 2:  # vanderkwaak coupling type, implicit (debug !)

      Msg.Fatal("not re-implemented !")
      '''
      self._couplLenght = functionConstant(self.options.couplingLength)
      if(self.options.S.microTopo):
        self._mt = self.S._mt
        self._rh = self.S._rh
      else:
        self._mt = self.C0
        self._rh = self.C0
      self.cplgFunction = functionC(self.libName, "vdkwkImplicit", 1,
            [self.R.KlinF, self._couplLenght, self.ksat, self.R.HF, self.S.HF, self._mt, self._rh, self.rain, self.DT_coupled])
      '''

    elif self.options.fluxType > 2:
      Msg.Fatal("fluxType not recognised : " + str(self.options.fluxType))

    if not self.cplgFunction : #coupling desactivated (neg values of fluxType)
      Msg.Warning("coupling flux desactivated !")
      self.cplg = None
      self.cplgFluxR = self.C0
      self.cplgFluxS = self.C0
    else:
      self.cplg = dgCoupling(self.cplgFunction)
      self.cplgFluxR = self.cplg.getFunction(self.R.gc)
      self.cplgFluxS = functionProdNew(self.cplg.getFunction(self.S.gc), self.areaModF)


    # coupling by boundary condition for Richards

    self.R.laws['H'].addBoundaryCondition(self.surfaceTags, self.R.laws['H'].newNeumannBoundary(self.cplgFluxR))
    if(self.options.RichardsExplicit):
      self.R.laws['W'].addBoundaryCondition(self.surfaceTags, self.R.laws['W'].newNeumannBoundary(self.cplgFluxR))
      self.R.laws['D'].addBoundaryCondition(self.surfaceTags, self.R.laws['D'].newNeumannBoundary(self.C0)) # to be improved ?

    # coupling by source term for shallow water

    self.sourceF = functionMinusNew(self.rain, self.cplgFluxS)
    self.S.law.setSource(self.sourceF)

  def bndNeumannS(self, flux, tags):
    self.S.bndNeumann(flux, tags)
  def bndDirichletS(self, value, tags):
    self.S.bndDirichlet(value, tags)
  def bndSlopeS(self, value, tags):
    self.S.bndSlope(value, tags)
  def bndCriticalDepthS(self, tags):
    self.S.bndCriticalDepth(tags)
  def bndNeumannR(self, flux, tags):
    self.R.bndNeumann(flux, tags)
  def bndDirichletR(self, value, tags):
    self.R.bndDirichlet(value, tags)
  def bndSlopeR(self, value, tags):
    self.R.bndSlope(value, tags)

  def setRain(self, rainFlux):
    print "setRain not implemented"
    exit()

  def seth(self, h, H):
    self.R.seth(h)
    self.S.setH(H)

  # restore the export "it" at time "t" if exists corresp binary for the fields R.H and S.H

  def restore(self, h, it, t=None):
    if(t):
      h.importAll(it, t)
    else:
      h.importAll(it)
    self.R._linK(self.R.H)
    #if R.W not loaded
    #self.R.seth(self.R.HF)

  def display(self, it, dt, t, text=""):
    massR  = self.R.getGlobalMass()
    massS  = self.S.getGlobalMass()
    massStr = '|MassS| %.16g\t|MassR| %.16g\t|totMass| %.16g' % (massS, massR, massS+massR)
    if(self.options.RichardsExplicit and self.R.activeEstimator):
      (errorL1, errorL2) = self.R.getEstimateErrors()
      (errorMax, errorMaxSat) = self.R.getMaxError()
      u.printInfos(it, dt, t, massStr + '\t|L1| %.3e |L2| %.3e |max| %.3e %.3e\t' % (errorL1, errorL2, errorMax, errorMaxSat) + text )
    else:
      u.printInfos(it, dt, t, massStr + '\t' + text)

  def iterate_old(self, dt, time):
    self.DT_coupled.set(dt)

    # compute flux and store it

    if(self.cplg):  self.cplg.compute()

    # Richards
    
    nbSubR = self.R.iterate(dt, time)

    # Shallow waters

    nbSubS = self.S.iterate(dt, time)
    if(self.options.S.isImplicit and self.options.S.clip):
      self.limiterS.apply(self.S.H)

    return nbSubR, nbSubS
    
  def iterate(self, dt, time):

    if(self.options.surfaceImposed):
      self.TIME.set(time)
      self.S.H.interpolate(self.options.surfaceImposed)
      self.S.H.scatter()
      nbSubS = 1

    if(self.options.RichardsExplicit):

      #self.copySurfHC.copy(self.S.H)
      self.DT_coupled.set(dt / self.R.nbSubH)

      # iterate on H in Richards
      for j in range(self.R.nbSubH - 1):
#        if(self.R.activeEstimator): #TEMP
#          self.R.lastH.copy(self.R.H) #TEMP
#          self.R.nbSubIters.set(self.R.nbSubH) #TEMP
        if(self.cplg):  self.cplg.compute()
        self.R.RK_H.iterate(self.R.H, dt / self.R.nbSubH, time + dt / self.R.nbSubH * j)
        if(not self.R.options.isCG):
          self.R.limiterH.apply(self.R.H)
        self.R._linK(self.R.H)
        #if(not self.options.surfaceImposed):
        #  self.S.iterate(dt / self.R.nbSubH, time + dt / self.R.nbSubH * j)
#        print 'errmax=%e\t' % (self.R.getMaxError()), 'L1=%e\tL2=%e' % self.R.getEstimateErrors()  #TEMP
      if(self.R.activeEstimator):
        self.R.lastH.copy(self.R.H)
        self.R.nbSubIters.set(self.R.nbSubH)
      #print 'h iter ', self.R.nbSubH
      if(self.cplg):  self.cplg.compute()
      self.R.RK_H.iterate(self.R.H, dt / self.R.nbSubH, time + dt - dt / self.R.nbSubH)
      if(not self.R.options.isCG):
        self.R.limiterH.apply(self.R.H)
      self.R._linK(self.R.H)
      #if(not self.options.surfaceImposed):
      #  #self.S.H.copy(self.copySurfHC)
      #  self.S.iterate(dt / self.R.nbSubH, time + dt - dt / self.R.nbSubH)

      self.DT_coupled.set(dt)
      self.DT.set(dt) # as H use != DT to set eta


#      self.R.KlinC.exportMsh("output/debugKlin", 1., 1, "Klin")
#      self.R.H.exportMsh("output/debugHg", 1., 1, "h")
#      self.R.H.exportFunctionMsh(self._couplLenght, "output/debugLc",1.,1,"lc")
#      self.R.H.exportFunctionMsh(self.heigStoR, "output/debugHs",1.,1,"Hs")
#      self.R.H.exportFunctionMsh(self._kro, "output/debugKro",1.,1,"Kro")

      # compute flux and store it
      if(self.cplg):  self.cplg.compute()

      # Richards
      nbSubR = self.R.iterateW(dt, time)

      # Shallow waters
      if(not self.options.surfaceImposed):
        nbSubS = self.S.iterate(dt, time)
        #if(self.options.S.isImplicit and self.options.S.clip and self.options.S.isDG):
        #  self.limiterS.apply(self.S.H) # mandatory ?
      return nbSubR, nbSubS


    else: #Richards implicit

      ## !!! not tested !!!

      self.DT_coupled.set(dt)
      if(self.options.RichardsImplicitAdaptive):
        SWGW.current = self
      else: #constant flux during the time step
        if(self.cplg):  self.cplg.compute()
      nbSubR = self.R.iterate(dt, time)
      

      # Shallow waters

      if(not self.options.surfaceImposed):
        nbSubS = self.S.iterate(dt, time)
        if(self.options.S.isDG):
          self.limiterS.apply(self.S.H) # mandatory ?

      return nbSubR, nbSubS

  def CCode(self):
    return '''
#include "fullMatrix.h"
#include "SVector3.h"
#include "function.h"
#include "dgGroupOfElements.h"
#include "MElement.h"
#include "dgConservationLaw.h"
#include "dgMeshJacobian.h"
extern "C" {

  void flatArea(dataCacheMap *m, fullMatrix<double> &out) {
    // quick P1 computation of the triangle area
    const dgGroupOfElements &eGroup = *m->getGroupOfElements();
    int iElement = m->getElementId();
    MElement * e = eGroup.getElement(iElement);
    switch(e->getTypeForMSH()) {
      /*case MSH_TRI_3:
      {
        SPoint3 p0 = e->getVertex(0)->point();
        SPoint3 p1 = e->getVertex(1)->point();
        SPoint3 p2 = e->getVertex(2)->point();
        SVector3 v1(p0, p1), v2(p0, p2);
        double area = norm(crossprod(v1,v2)) / 2.;
        double areabis = e->getVolume();
        printf("areas: %e : %e\\n", area, areabis);
        for (size_t i = 0; i < out.size1(); i++) {
          out.set(i, 0, area);
        }
        break;
      }*/
      case MSH_LIN_2:
      {
        SPoint3 p0 = e->getVertex(0)->point();
        SPoint3 p1 = e->getVertex(1)->point();
        double area = p0.distance(p1);
        for (size_t i = 0; i < out.size1(); i++) {
          out.set(i, 0, area);
        }
        break;
      }
      default:
      {
        double area = e->getVolume();
        for (size_t i = 0; i < out.size1(); i++) {
          out.set(i, 0, area);
        }
        //printf("elemt not checked in surfaceArea\\n");
        //exit(1);
      }
    }
  }

  void modArea(dataCacheMap *m, fullMatrix<double> &out) {
    int iElement = m->getElementId();
    int iGroup = m->getGroupCollection()->getElementGroupId(m->getGroupOfElements());
    double modifiedVolume = m->getJacobians().elementVolume(iGroup)(iElement);
    double originalVolume = m->getGroupOfElements()->getElement(iElement)->getVolume();
    double areaMod = originalVolume / modifiedVolume;
    for (size_t i = 0; i < out.size1(); i++) {
      out.set(i, 0, areaMod);
    }
  }

  void heightSurf(dataCacheMap *m, fullMatrix<double> &out, fullMatrix<double> &hSurf,
                  fullMatrix<double> &hRain) {
    for (size_t i = 0; i < out.size1(); i++) {
      //double H = hSurf(i, 0) + hRain(i, 0) / 2.;
      //out.set(i, 0, H);
      out.set(i, 0, hSurf(i, 0) );
    }
  }

  #define ALPHA_F1 1e-5 // ~min value for minAtm = -500

  void f1(dataCacheMap *, fullMatrix<double> &out, fullMatrix<double> &hSurf, fullMatrix<double> &hRain) {
    const double alpha_sq = ALPHA_F1 * ALPHA_F1;
    const double minAtmHead = -500.;
    const double minAtmHeight = .5 * (minAtmHead + sqrt(minAtmHead * minAtmHead + alpha_sq));

    for (size_t i = 0; i < out.size1(); i++) {
      double H = std::max(hSurf(i, 0) /*+ hRain(i, 0) / 2.*/, minAtmHeight);
      double h = H - alpha_sq / 4 / H;
      //printf("H=%e + R=%e/2 -> h=%e\\n", hSurf(i,0), hRain(i,0), h);
      //printf("H=%e -> h=%e\\n", hSurf(i,0), h);
      out.set(i, 0, h);
    }
  }

  void f1_inv(dataCacheMap *, fullMatrix<double> &out, fullMatrix<double> &hSoil) {
    const double alpha_sq = ALPHA_F1 * ALPHA_F1;
    for (size_t i = 0; i < out.size1(); i++) {
      double h = hSoil(i, 0);
      //printf("head %e -> height %e\\n", h,  (h + sqrt(h*h + alpha_sq)) / 2. );
      out.set(i, 0, (h + sqrt(h*h + alpha_sq)) / 2. );
    }
  }

  #define ALPHA_F2 1e-16

  void f2(dataCacheMap *, fullMatrix<double> &out, fullMatrix<double> &flux, fullMatrix<double> &massS) {
    const double alpha_sq = ALPHA_F2 * ALPHA_F2;
    for (size_t i = 0; i < out.size1(); i++) {
      double x = flux(i, 0);
      double q = std::max(0., massS(i, 0)); // 0 avoid pumping => elmts with h<0
//      printf("%e\\n", q);
//      double x_lim = (x + q - sqrt((x - q) * (x - q) + alpha_sq)) / 2.;
      out.set(i, 0, std::min(x,q)); //x_lim);
    }
  }

  #define DEVIATION_COORD 1e-5 //until better coupling procedure set, sensible parameter !

  void oneTwoAndThree(dataCacheMap *, fullMatrix<double> &out, fullMatrix<double> &OneTwo, fullMatrix<double> &Three) {
    /* expected
    for (size_t i = 0; i < out.size1(); i++) {
      out.set(i, 0, OneTwo(i, 0) );
      out.set(i, 1, OneTwo(i, 1) );
      out.set(i, 2, Three (i, 0) );
    }
    */

    // modification: taking points inside the elements close to element nodes
    // goal: as not element-by-element, but coordinate-to-element, wrong elmts
    //       could be considered otherwise

    double xmoy = 0, ymoy = 0, zmoy = 0;
    for (size_t i = 0; i < out.size1(); i++) {
      xmoy += OneTwo(i, 0);
      ymoy += OneTwo(i, 1);
      zmoy += Three(i, 0);
    }
    xmoy /= (double) out.size1();
    ymoy /= (double) out.size1();
    zmoy /= (double) out.size1();

    for (size_t i = 0; i < out.size1(); i++) {
      double x = OneTwo(i, 0) + (xmoy - OneTwo(i, 0)) * DEVIATION_COORD;
      double y = OneTwo(i, 1) + (ymoy - OneTwo(i, 1)) * DEVIATION_COORD;
      double z = Three (i, 0) + (zmoy - Three (i, 0)) * DEVIATION_COORD;
//      printf("%e -> %e      %e -> %e     %e -> %e\\n", OneTwo(i,0), x, OneTwo(i,1), y, Three(i,0), z);
      out.set(i, 0, x );
      out.set(i, 1, y );
      out.set(i, 2, z );
    }
  }

  void normals(dataCacheMap *, fullMatrix<double> &out, fullMatrix<double> &gBath, fullMatrix<double> &g) {
    for (size_t i = 0; i < out.size1(); i++) {
      SVector3 v1(1, 0, gBath(i,0));
      SVector3 v2(0, 1, gBath(i,1));
      SVector3 v3 = crossprod(v1, v2);
      v3.normalize();
/*      double sign = v3.x() * g(i, 0) + v3.y() * g(i, 1) + v3.z() * g(i, 2);
      sign = (sign > 0) - (sign < 0);
//      printf("%e  (%e,%e,%e)\\n", sign, v3.x(), v3.y(), v3.z() );
      out.set(i, 0, v3.x() * sign);
      out.set(i, 1, v3.y() * sign);
      out.set(i, 2, v3.z() * sign);*/
      out.set(i, 0, v3.x());
      out.set(i, 1, v3.y());
      out.set(i, 2, v3.z());
    }
  }

  void massAvail(dataCacheMap *, fullMatrix<double> &out, fullMatrix<double> &HS, //fullMatrix<double> &HR, 
                                 fullMatrix<double> &DT, fullMatrix<double> &rain) {
    for (size_t i = 0; i < out.size1(); i++) {
      out.set(i, 0, HS(i, 0) / DT(0, 0) + rain(i, 0) );
      //out.set(i, 0, HS(i, 0) + rain(i,0) * DT(0, 0) );
    }
  }

  void massAvailSurfImposed(dataCacheMap *, fullMatrix<double> &out, fullMatrix<double> &HS,
                                        fullMatrix<double> &DT, fullMatrix<double> &rain) {
    for (size_t i = 0; i < out.size1(); i++) {
      double avail = HS(i, 0);
      if(avail > 0.)
        avail = avail / DT(0, 0) + rain(i, 0) ;
        //avail = avail + rain(i, 0) * DT(0, 0) ;
      out.set(i, 0, avail);
    }
  }

  void cplgFluxImposed(dataCacheMap *, fullMatrix<double> &out, fullMatrix<double> &cfH, fullMatrix<double> &cfW) {
    for (size_t i = 0; i < out.size1(); i++) {
      out.set(i, 0, std::max(0., cfH(i,0) + cfW(i,0)) );
    }    
  }

  void vdkwk(dataCacheMap *, fullMatrix<double> &out, fullMatrix<double> &K, fullMatrix<double> &couplingLength,
             fullMatrix<double> &ksat, fullMatrix<double> &H, fullMatrix<double> &H0, fullMatrix<double> &kro) {
    for (size_t i = 0; i < out.size1(); i++) {
      double hs = H0(i, 0); //std::max(0., H0(i, 0));
      double hg = H(i, 0);
      double kog, gradH;

      if(hs <= hg) {
        kog = K(i, 8);
        gradH = /*- 1. +*/ (hs - hg) / couplingLength(i, 0);
      } else {
        kog = kro(i, 0) * ksat(i, 8);
        gradH = (hs - hg) / couplingLength(i, 0);
      }

      double flux = kog * gradH;
      out.set(i, 0, flux);
    }
  }

  struct paramsVdkw {
    double l, ks, hg, mtopo, Sr, hold, rain, DT;
  };
  
  double vdkwFunction(double hs, paramsVdkw & p) {
    double gradH = /*-1. +*/ (hs - p.hg) / p.l;
    if(p.mtopo > 0.) {
      double Sw  = std::min(1., std::max(0., hs / p.mtopo ));
      double Se  = std::max(0., (Sw - p.Sr) / (1. - p.Sr) );
      double kog = pow(Se, 2. * (1. - Se) );
      return gradH * kog;
    }
    else
      return gradH;
  }

  double vdkwMassConservationResidual(double h, paramsVdkw &p) {
    return (p.rain - vdkwFunction(h, p) ) * p.DT + p.hold - h ;
  }

  void stopSpecifyingState(std::string str, paramsVdkw &p) {
    printf("%s\\n", str.c_str());
    printf("l, ks, hg, mtopo, Sr, hold, rain, DT=(%e, %e, %e, %e, %e, %e, %e, %e)\\n", 
            p.l, p.ks, p.hg, p.mtopo, p.Sr, p.hold, p.rain, p.DT);
    exit(1);
  }

  double smallNewtonSolverVdkw(double guess, paramsVdkw & p) {
    const int nbItMax = 100;
    const double atol = 1e-8;
    const double rtol = 1e-6;
    const double eps = 1e-8;

    double h = guess;
    double Rini = vdkwMassConservationResidual(h, p);
    if(Rini == 0.) return h;
    double R = Rini;

    for(int i=0; i < nbItMax; i++) {
      double Rp = vdkwMassConservationResidual(h+eps, p);
      double fp = (Rp - R) / eps;
      if(fp == 0) { stopSpecifyingState("error: null derivative !", p); }
      h = h - R / fp ;
      R = vdkwMassConservationResidual(h, p);
      //if(h > p.mtopo*p.Sr)
      //printf("h=%e  R=%e  Rp=%e, fp=%e\\n", h, R, Rp, fp);
      if( fabs(R) < atol || fabs(R / Rini) < rtol )
        return h;
      if(i == 51) printf("up to 50 newton iterations\\n");
      if(i > 50) printf("%e, %e, %e, %e, %e, %e, %e, %e, %e\\n", h, p.l, p.ks, p.hg, p.mtopo, p.Sr, p.hold, p.rain, p.DT);
    }
    stopSpecifyingState("warn: newton method reached nbItMax=100 !", p);
  }

  double smallSecantSolverVdkw(double guess, paramsVdkw & p) {
    const int nbItMax = 100;
    const double atol = 1e-8;
    const double rtol = 1e-6;
    const double eps = 1e-8;

    double ho = guess;
    double Rini = vdkwMassConservationResidual(ho, p);
    if(Rini == 0.) return ho;
    double Ro = Rini;
    double hn = guess + eps;
    double Rn = vdkwMassConservationResidual(hn, p);

    for(int i=0; i < nbItMax; i++) {
      if(Rn-Ro==0.) { stopSpecifyingState("error: null derivative !", p); }
      double h = hn - Rn * (hn - ho) / (Rn - Ro);
      Ro = Rn;
      Rn = vdkwMassConservationResidual(h, p);
      if( fabs(Rn) < atol || fabs(Rn / Rini) < rtol )
        return h;
      ho = hn;
      hn = h;
    }
    stopSpecifyingState("warn: secant method reached nbItMax=100 !", p);
  }



  void vdkwkImplicit(dataCacheMap *m, fullMatrix<double> &out, fullMatrix<double> &K, fullMatrix<double> &l, fullMatrix<double> &ks,
                     fullMatrix<double> &hg, fullMatrix<double> &hs, fullMatrix<double> &mtopo, fullMatrix<double> &Sr,
                     fullMatrix<double> &rain, fullMatrix<double> &DT) {
    int iElem = m->getElementId();
    paramsVdkw p;
    p.DT = DT(0, 0);
    for (size_t i = 0; i < out.size1(); i++) {
      if(hs(i,0) < hg(i,0)) {
        double gradH = -1. + ( hs(i,0) - hg(i,0) ) / l(i,0);
        out.set(i, 0, gradH * K(i,8) ); // !grav = 001
      }
      else {
        p.l = l(i, 0);
        p.ks = ks(i, 8); // !grav = 001
        p.hg = hg(i, 0);
        p.mtopo = mtopo(i, 0);
        p.Sr = Sr(i, 0);
        p.hold = hs(i, 0);
        p.rain = rain(i, 0);
        double hsol = smallNewtonSolverVdkw(p.hold, p);
        //double hsol = smallSecantSolverVdkw(p.hold, p);
        //printf("hsol=%e, flux=%e\\n", hsol, vdkwFunction(hsol, p));
        double hdump = hsol ;//* 1.01 - p.hold * .01;
        if(iElem==5 && i==0)
          printf("%e, %e, %e, %e, %e, %e, %e, %e, %e\\n", hdump, p.l, p.ks, p.hg, p.mtopo, p.Sr, p.hold, p.rain, p.DT);
        out.set(i, 0, vdkwFunction( std::max(0., hdump) , p ));
      }
    }
  }

  void muFactor(dataCacheMap *m, fullMatrix<double> &out, fullMatrix<double> &minl) {
    double p = m->getGroupOfElements()->getOrder();
    double dim = m->getGroupOfElements()->getDimUVW();
    out.setAll((p + 1) * (p + dim) / (dim * minl(0,0)));
  }

  void richardsFlux(dataCacheMap *m, fullMatrix<double> &out, fullMatrix<double> &gradH, fullMatrix<double> &g, fullMatrix<double> &K) {
    for (size_t i = 0; i < out.size1(); i++) {
      out.set(i, 0, (g(i, 0) + gradH(i, 0)) * K(i, 1) + (g(i, 1) + gradH(i, 1)) * K(i, 1) + (g(i, 2) + gradH(i, 2)) * K(i, 2));
      out.set(i, 1, (g(i, 0) + gradH(i, 0)) * K(i, 4) + (g(i, 1) + gradH(i, 1)) * K(i, 4) + (g(i, 2) + gradH(i, 2)) * K(i, 5));
      out.set(i, 2, (g(i, 0) + gradH(i, 0)) * K(i, 6) + (g(i, 1) + gradH(i, 1)) * K(i, 7) + (g(i, 2) + gradH(i, 2)) * K(i, 8));
    }
  }

  void flux_dg(fullMatrix<double> &out, fullMatrix<double> &hR, fullMatrix<double> &hS, fullMatrix<double> &n, fullMatrix<double> &nu,
               fullMatrix<double> &richardsFlux, fullMatrix<double> &muFactor, fullMatrix<double> &massAvail) {

    for (size_t i = 0; i < out.size1(); i++) {
      double flux = richardsFlux(i, 0) * n(i, 0) + richardsFlux(i, 1) * n(i, 1) + richardsFlux(i, 2) * n(i, 2);
      double jump = (hR(i, 0) - hS(i, 0)) / 2. * muFactor(0, 0) * nu(i, 0);
      //TODO nu => ksat if hs>hr else k(h),  limited as other intf term !
//      printf("jump factor = %e*%e/2 = %e\\n", muFactor(0, 0), nu(i,0), muFactor(0, 0) * nu(i, 0) / 2.);

      out.set(i, 0, std::min(std::max(0., massAvail(i,0)), flux - jump));
    }
  }

  void flux_vdk(fullMatrix<double> &out, fullMatrix<double> &K, fullMatrix<double> &cl, fullMatrix<double> &ksat,
              fullMatrix<double> &HR, fullMatrix<double> &HS, fullMatrix<double> &kro, fullMatrix<double> &massAvail) {

    for (size_t i = 0; i < out.size1(); i++) {
      double hs = std::max(0., HS(i, 0));
      double hg = HR(i, 0);
      double kog, gradH;

      if(hs <= hg) {
        kog = K(i, 8); //TODO mod: only valid if grav=001 !
        gradH = /*- 1. +*/ (hs - hg) / cl(i, 0);
//    if(kog / cl(i, 0) > 5e-05)
//      printf("vdk< factor = %e/%e = %e\\n", kog, cl(i, 0), kog / cl(i, 0));
//  kog = 0.;
      } else {
        kog = kro(i, 0) * ksat(i, 8);
        gradH = /*- 1. +*/ (hs - hg) / cl(i, 0);
//    if(kog / cl(i, 0) > 5e-05)
//      printf("vdk> factor = %e/%e = %e\\n", kog, cl(i, 0), kog / cl(i, 0));
      }

      double flux = std::min(kog * gradH, std::max(0., massAvail(i, 0)));
      out.set(i, 0, flux);
    }
  }

    void flux_vdk_mean(fullMatrix<double> &out, fullMatrix<double> &K, fullMatrix<double> &cl, fullMatrix<double> &ksat,
              fullMatrix<double> &HR, fullMatrix<double> &HS, fullMatrix<double> &kro, fullMatrix<double> &massAvail) {
    double fluxMean = 0., massMean = 0.;
    for (size_t i = 0; i < out.size1(); i++) {
      double hs = HS(i, 0);
      double hg = HR(i, 0);
      double kog, gradH;

      if(hs <= hg) {
        kog = K(i, 8); //TODO mod: only valid if grav=001 !
        gradH = /*- 1. +*/ (hs - hg) / cl(i, 0);
      } else {
        kog = kro(i, 0) * ksat(i, 8);
        gradH = /*- 1. +*/ (hs - hg) / cl(i, 0);
      }

      fluxMean += kog * gradH;
      massMean += massAvail(i, 0);
    }
    fluxMean /= (double) out.size1();
    massMean /= (double) out.size1();
    fluxMean = std::min(fluxMean, massMean);
    for (size_t i = 0; i < out.size1(); i++)
      out.set(i, 0, fluxMean);
  }

  void flux_dummy(fullMatrix<double> &out, fullMatrix<double> &, fullMatrix<double> &) {
    for (size_t i = 0; i < out.size1(); i++) {
      out.set(i, 0, 1e-7);
    }
  }
}
'''

def preProcFuncCplg():
  curSol = RichardsImplicit.current.newtonKrabbenhoft.getCurrentSolution()
  RichardsImplicit.current._linK(curSol)
  if(SWGW.current.cplg): SWGW.current.cplg.compute()

def preProcFuncCplgNoLinK():
  if(SWGW.current.cplg): SWGW.current.cplg.compute()

class vertexContainer(object):
  def __init__(self):
    self.mapv = {}
    self.lastexists = False
  def __getitem__(self, v):
    num = v.getNum()
    if(num in self.mapv.keys()):
      self.lastexists = True
      return self.mapv[num]
    else:
      self.lastexists = False
      eV = MVertex(v.x(), v.y(), v.z())
      eV.thisown = 0
      self.mapv[num] = eV
      return eV
  def exists(self, v):
    return num in self.mapv.keys()
  def lastExists(self):
    return self.lastexists

# from a 3D mesh, extract 2D mesh which can be used into the SWNI model

def extractSurface (model, facesNames) :
  # initial 3d mesh must be generated
  # 3d mesh must have Physical Lines bounding it
  # facesNames is a vector of strings designing face tags

  eModel = GModel()
  eModel.thisown = 0

  # for each mentionned face, add the Gvertices and Gedges

  gfaces = model.bindingsGetFaces()
  vertexMap = vertexContainer()

  for face in gfaces:
    numericalTagsOfFace = face.getPhysicalEntities()
    if(len(numericalTagsOfFace)==0):
      print "numerically untagged face, skipping"
      continue

    tagOnFace = model.getPhysicalName(2, numericalTagsOfFace[0])
    if(tagOnFace in facesNames):
      print "copying face", tagOnFace
      fedges = face.edges()
      if(len(fedges) == 0):
        print "error: face.edges() empty !! Try to load .geo first"
        exit()

      tagBoundEdges = []

      # edges

      for edge in fedges:

        eV1 = discreteVertex(eModel, eModel.getMaxElementaryNumber(0) + 1)
        eV1.thisown = 0
        eModel.add(eV1)

        eV2 = discreteVertex(eModel, eModel.getMaxElementaryNumber(0) + 1)
        eV2.thisown = 0
        eModel.add(eV2)

        eE = discreteEdge(eModel, eModel.getMaxElementaryNumber(1) + 1, eV1, eV2)
        eE.thisown = 0
        tagBoundEdges.append(eE.tag())
        eModel.add(eE)

        numericalTagsOfEdge = edge.getPhysicalEntities()
        if(len(numericalTagsOfEdge)!=0):
          eE.addPhysicalEntity(numericalTagsOfEdge[0])
          eModel.setPhysicalName(model.getPhysicalName(1, numericalTagsOfEdge[0]), 1, numericalTagsOfEdge[0])

        for iL in range(edge.getNumMeshElements()):
          l = edge.getMeshElement(iL)
          listV = []
          for iV in range(l.getNumVertices()):
            v = l.getVertex(iV)
            eV = vertexMap[v]
            listV.append(eV)
            if(not vertexMap.lastExists()):
              eE.addMeshVertex(eV)
          if(l.getTypeForMSH() == MSH_LIN_2):
            newLine = MLine(listV[0], listV[1])
            newLine.thisown = 0
            eE.addLine(newLine)
          else:
            print "error: 2D element mesh type not recognised"

      # face

      eF = discreteFace(eModel, eModel.getMaxElementaryNumber(2) + 1)
      eF.thisown = 0
      eF.setBoundEdges(eModel, tagBoundEdges)
      eModel.add(eF)
      eF.addPhysicalEntity(numericalTagsOfFace[0])
      eModel.setPhysicalName(tagOnFace, 2, numericalTagsOfFace[0])

      numMElements = face.getNumMeshElements()
      for iE in range(numMElements):
        e = face.getMeshElement(iE)
        listV = []
        numMVertices = e.getNumVertices()
        for iV in range(numMVertices):
          v = e.getVertex(iV)
          eV = vertexMap[v]
          listV.append(eV)
          if(not vertexMap.lastExists()):
            eF.addMeshVertex(eV)
        if(e.getTypeForMSH() == MSH_TRI_3):
          newTri = MTriangle(listV[0],listV[1],listV[2])
          newTri.thisown = 0
          eF.addTriangle(newTri)
        elif(e.getTypeForMSH() == MSH_QUA_4):
          newQuad = MQuadrangle(newV[0],newV[1],newV[2],newV[3])
          newQuad.thisown = 0
          eF.addQuadrangle(newQuad)
        else:
          print "error: 2D element mesh type not recognised"

    else:
      print "face", tagOnFace, "ignored"
  return eModel

def extractSurfaceLine (model, facesNames):
  # initial 2d mesh must be generated
  # 1d mesh must have Physical Points bounding it
  # facesNames is a vector of strings designing face tags

  eModel = GModel()
  eModel.thisown = 0

  # for each mentionned face (here face is 1D, edge is 0D), add the 2 Gvertices

  gfaces = model.bindingsGetEdges()
  vertexMap = {}
  for face in gfaces:
    numericalTagsOfFace = face.getPhysicalEntities()
    if(len(numericalTagsOfFace)==0):
      print "numerically untagged face"
      continue
    tagOnFace = model.getPhysicalName(1, numericalTagsOfFace[0])
    if(tagOnFace in facesNames):
      print "copying face", tagOnFace
      fedges = [face.getBeginVertex(), face.getEndVertex()]
      boundEdges = []
      if(not len(fedges[0].getPhysicalEntities()) or not len(fedges[1].getPhysicalEntities())):
        print "error: Try to load .geo first and put tags on vertices"
        exit()

      for edge in fedges:
        eE = discreteVertex(eModel, eModel.getMaxElementaryNumber(0) + 1)
        eE.thisown = 0
        eModel.add(eE)
        boundEdges.append(eE)
        numericalTagsOfEdge = edge.getPhysicalEntities()
        if(len(numericalTagsOfEdge)!=0):
          eE.addPhysicalEntity(numericalTagsOfEdge[0])
          eModel.setPhysicalName(model.getPhysicalName(0, numericalTagsOfEdge[0]), 0, numericalTagsOfEdge[0])

        numMVertices = edge.getNumMeshVertices()
        for iV in range(numMVertices):
          v = edge.getMeshVertex(iV)
          if(v.getNum() in vertexMap.keys()):
            eV = vertexMap[v.getNum()]
          else:
            eV = MVertex(v.x(), v.y(), v.z())
            eV.thisown = 0
            vertexMap[v.getNum()] = eV
          eE.addMeshVertex(eV)
          newPoint = MPoint(eV)
          newPoint.thisown = 0
          eE.addPoint(newPoint)

      eF = discreteEdge(eModel, eModel.getMaxElementaryNumber(1) + 1, fedges[0], fedges[1])
      eF.thisown = 0
      eModel.add(eF)
      eF.addPhysicalEntity(numericalTagsOfFace[0])
      eModel.setPhysicalName(tagOnFace, 1, numericalTagsOfFace[0])

      numMVertices = face.getNumMeshVertices()
      for iV in range(numMVertices):
        v = face.getMeshVertex(iV)
        if(v.getNum() in vertexMap.keys()):
          eV = vertexMap[v.getNum()]
        else:
          eV = MVertex(v.x(), v.y(), v.z())
          eV.thisown = 0
          vertexMap[v.getNum()] = eV
        eF.addMeshVertex(eV)

      numMElements = face.getNumMeshElements()
      for iE in range(numMElements):
        e = face.getMeshElement(iE)
        listV = []
        numMVertices = e.getNumVertices()
        for iV in range(numMVertices):
          v = e.getVertex(iV)
          eV = vertexMap[v.getNum()]
          listV.append(eV)
        if(e.getTypeForMSH() == MSH_LIN_2):
          newLine = MLine(listV[0],listV[1])
          newLine.thisown = 0
          eF.addLine(newLine)
        else:
          print "error: 1D element mesh type not recognised"

    else:
      print "face", tagOnFace, "ignored"
  return eModel


