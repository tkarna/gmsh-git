#include "fullMatrix.h"
#include "function.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define PI 3.14159265

static bool init = false;
static float *dataT;
static float *dataV; 
static float T;
static int numCycle = 0;
static int ind = 0;


static void initialize() {
  // Read inlet file and store the time in dataT, the velocity in dataV.
  const char * InletFileName = getenv("INLETFILE");
  if (!InletFileName || std::string(InletFileName) == "") {
    fprintf(stderr,"INLETFILE env variable not defined. Use os.putenv(\"INLETFILE\", InletFileName) in python.\n");
    exit(0);
  }
  printf("Info : Inlet file: %s\n", InletFileName);
  FILE *input = fopen(InletFileName, "r");
  //First check the length of the file to assign dataT and dataV
  int j = 0;
  float t,v;
  int o1,o2;
  while (!feof(input)){
    o1 = fscanf(input, "%f", &t);
    o2 = fscanf(input, "%f", &v);
    j=j+1;
  }
  rewind(input);
  dataT = new float[j];
  dataV = new float[j];
  int i = 0;
  
  while (!feof(input)){
    o1 = fscanf(input, "%f", &t);
    o2 = fscanf(input, "%f", &v);
    dataT[i] = t;
    dataV[i] = v;
    i = i+1;
  }
  T = dataT[i-1];
  printf("Info : Cardiac cycle duraction: %f\n", T);
  fclose (input);
  init = true;
}

extern "C" {

  void velocityFileC (dataCacheMap *, fullMatrix<double> &fct, fullMatrix<double> &time ) {
    if (! init) initialize();
    float t = time(0,0);
    float tstar = (t - floor(t/T)*T);
    if (t >= T*numCycle) {
      numCycle = numCycle+1;
      ind = 0;
    }
    if (tstar >= dataT[ind+1])  ind =ind+1;
    float dat1 = dataV[ind];
    float dat2 = dataV[ind+1];
    float t1 = dataT[ind];
    float t2 = dataT[ind+1];
    double velFile = dat1+(dat2-dat1)/(t2-t1)*(tstar-t1); 
    fct.set(0,0, velFile);
  }
   
  void inletPresC(dataCacheMap *, fullMatrix<double> &fct, fullMatrix<double> &time ){
    double Tc = 0.8;// 0.33;
    double Pmax = 20000.0;
    double heaviside = 1.0;
    double tt = time(0,0);
    if (tt > Tc/2) heaviside = 0.0;	
    double p = Pmax*sin(2*PI*tt/Tc)*heaviside;
    fct.set(0,0,p);
  }
  
  void inletVelC( dataCacheMap *, fullMatrix<double> &fct, fullMatrix<double> &time ){
    double tt = time(0,0);
    double u = 0.5*exp(-5000.0*(tt-0.05)*(tt-0.05));
    fct.set(0,0,u);
  }
  
  void inletVel2C(dataCacheMap *, fullMatrix<double> &fct, fullMatrix<double> &time ){
    double Tc = 0.8;
    double tt = time(0,0);
    double heaviside = 1.0;
    if ( (tt-floor(tt/Tc)*Tc) > 0.25 )   heaviside = 0.0;
    double u = 100*sin(PI*(tt-floor(tt/Tc)*Tc)/.25)*heaviside;
    fct.set(0,0,u);
  }

}





