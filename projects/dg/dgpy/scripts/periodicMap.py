from math import *
import sys

# only work for triangles and quads in order 1 !!!

class Mesh:
  #we store everything, not efficient in term of memory but simple to use
  #neighbours = [] #[NElements][3] => (elementId, closureId)
  def readMsh(self, filename, cut, paste) :
    fin = open(filename, "r");
    l = fin.readline()
    while l != "" :
      if l == "$PhysicalNames\n" :
        n = int(fin.readline())
        for i in range(n) :
          (i, j, st) = fin.readline().split()
          for iPerBound in range(self.perBoundNum) :
            if st == "\"" + cut[iPerBound] + "\"" :
              self.cutBnd[iPerBound] = int(j)
            if st == "\"" + paste[iPerBound] + "\"" :
              self.pasteBnd[iPerBound] = int(j)
      if l == "$Nodes\n" :
        n = int(fin.readline())
        for i in range(n) :
          (j, x, y, z) = fin.readline().split()
          self.nodes.append([float(x), float(y), float(z)])
        self.node2Point = [-1 for n in self.nodes]
      if l == "$Elements\n" :
        n = int(fin.readline())
        for i in range(n) :
          l = fin.readline().split()
          t = int(l[1])
          nf = int(l[2])
          if nf > 2 :
            self.meshPartitionned = True
          if t == 2 :
            self.elements.append([int(l[3 + nf + a]) - 1 for a in range(3)])
            self.elementId.append(int(l[0]))
            #self.elementTag.append(int(l[4]))
            if self.meshPartitionned :
              self.entityPartition[int(l[0])] = int(l[6])
              nGhost = int(l[5]) - 1
              self.ghosts[int(l[0])] = [int(l[7+a]) for a in range(nGhost)]
              for j in range(3) :
                if int(l[3+nf+j]) in self.node2Elements.keys() :
                  self.node2Elements[int(l[3+nf+j])].append(int(l[0]))
                else :
                  self.node2Elements[int(l[3+nf+j])] = [int(l[0])]
            self.elementType.append(t)
          elif t == 3 :
            self.elements.append([int(l[3 + nf + a]) - 1 for a in range(4)])
            self.elementId.append(int(l[0]))
            #self.elementTag.append(int(l[4]))
            if self.meshPartitionned :
              self.entityPartition[int(l[0])] = int(l[6])
              nGhost = int(l[5]) - 1
              self.ghosts[int(l[0])] = [int(l[7+a]) for a in range(nGhost)]
              for j in range(4) :
                if int(l[3+nf+j]) in self.node2Elements.keys() :
                  self.node2Elements[int(l[3+nf+j])].append(int(l[0]))
                else :
                  self.node2Elements[int(l[3+nf+j])] = [int(l[0])]
            self.elementType.append(t)
          elif t == 1 :
            n0, n1 = (int(l[3 + nf +0]) -1, int(l[3 + nf + 1]) -1)
            self.bnd[(min(n0, n1), max(n0, n1))] = [int(l[3]), int(l[4])]
            self.edges.append((min(n0, n1), max(n0, n1)))
            self.edgesId[(min(n0, n1), max(n0, n1))] = int(l[0])
            if self.meshPartitionned :
              self.entityPartition[int(l[0])] = int(l[6])
              self.ghosts[int(l[0])] = []
              for j in range(2) :
                if int(l[3+nf+j]) in self.node2Elements.keys() :
                  self.node2Elements[int(l[3+nf+j])].append(int(l[0]))
                else :
                  self.node2Elements[int(l[3+nf+j])] = [int(l[0])]
          elif t == 15 :
            p = int(l[3 + nf])
            self.node2Point[int(l[3 + nf]) - 1] = int(l[4]) - 1
      l = fin.readline()
    self.buildNeighbours()

  def __init__(self, filename, perBoundNum, cut, paste) :
    self.nodes = [] #[Nnodes][3] => xyz
    self.elements = [] #[NElements][3] => nodeId
    #self.elementTag = []
    self.elementId = []
    self.elementType = []
    self.entityPartition = {}
    self.edges = []
    self.edgesId = {}
    self.node2Elements = {}
    self.ghosts = {}
    self.bnd = {}
    self.perBoundNum = perBoundNum
    self.cutBnd = [-1 for i in range(perBoundNum)]
    self.pasteBnd = [-1 for i in range(perBoundNum)]
    self.periodicNeighbours = [[] for i in range(perBoundNum)]
    self.meshPartitionned = False
    self.readMsh(filename, cut, paste)
    print ("mesh \"%s\" loaded : %i nodes %i elements" % (filename,len(self.nodes), len(self.elements)))

  def match(self, ie0, ie1, iPerBound, shiftOperation) :
    tol  = 1e-2
    e0 = self.edges[ie0]
    e1 = self.edges[ie1]
    ne0 = [self.nodes[e0[0]], self.nodes[e0[1]]]
    shiftedNe00 = shiftOperation(ne0[0],iPerBound)
    shiftedNe01 = shiftOperation(ne0[1],iPerBound)
    for i in range(2) :
      ne1 = [self.nodes[e1[i]], self.nodes[e1[(i+1)%2]]]
      checkNe0 = abs(ne1[0][0] - shiftedNe00[0])  + abs(ne1[0][1] - shiftedNe00[1]) +  abs(ne1[0][2] - shiftedNe00[2])
      checkNe1 = abs(ne1[1][0] - shiftedNe01[0])  + abs(ne1[1][1] - shiftedNe01[1]) +  abs(ne1[1][2] - shiftedNe01[2])
      if checkNe0 + checkNe1 < tol :
        for j in range(3) :
          self.nodes[e0[0]][j] += ne1[0][j] - shiftedNe00[j]
          self.nodes[e0[1]][j] += ne1[1][j] - shiftedNe01[j]
        return i
    return -1
  
  def buildNeighbours(self) :
    emap = [{} for e in self.nodes]
    self.neighbours = [[(-1, -1)] * 4 for e in self.elements]
    for ie in range(len(self.elements)) :
      el = self.elements[ie]
      type = self.elementType[ie]
      if type == 2 :
        nEdge = 3
      else :
        nEdge = 4
      for i in range (nEdge) :
        j = (i + 1) % nEdge
        enodes = (min(el[i], el[j]), max(el[i], el[j]))
        if enodes in self.bnd.keys():
          self.neighbours[ie][i] = (-1, self.bnd[enodes][1])
          #self.edge2Elements[enodes] = (ie, i)
        if not enodes[1] in emap[enodes[0]] :
          emap[enodes[0]][enodes[1]] = (ie, i)
        else :
          n = emap[enodes[0]][enodes[1]]
          self.neighbours[ie][i] = n
          self.neighbours[n[0]][n[1]] = (ie, i)
  
  def buildPeriodicNeighboursGhosts(self, ie0, ie1, match) :
    e0 = self.edges[ie0]
    e1 = self.edges[ie1]
    e = [e0, e1]
    for iedge in range(2) :
      for inode in range(2) :
        for iElement in self.node2Elements[e[iedge][inode] + 1] :
          for jElement in self.node2Elements[e[(iedge+1)%2][(inode+match) % 2] + 1] :
            if self.entityPartition[iElement] != self.entityPartition[jElement] :
              newGhost = True
              for iGhost in self.ghosts[iElement] :
                if self.entityPartition[jElement] == - iGhost :
                  newGhost = False
              if newGhost :  
                self.ghosts[iElement].append(-self.entityPartition[jElement])

  def buildPeriodicNeighbours(self, shiftOperation) : 
    for ie0 in range(len(self.edges)) : 
      for ie1 in range(ie0 + 1, len(self.edges)) :
        for iPerBound in range(self.perBoundNum) :
          if self.bnd[self.edges[ie0]][0] == self.cutBnd[iPerBound] and self.bnd[self.edges[ie1]][0] == self.pasteBnd[iPerBound] :
            match = self.match(ie0, ie1, iPerBound, shiftOperation) 
            if match >= 0 :
              if self.meshPartitionned : 
                self.buildPeriodicNeighboursGhosts(ie0, ie1, match)
              ne0 = [self.edges[ie0][0], self.edges[ie0][1]]
              ne1 = [self.edges[ie1][match], self.edges[ie1][(match+1)%2]]
              self.periodicNeighbours[iPerBound].append([self.edgesId[self.edges[ie0]], self.edgesId[self.edges[ie1]], ne0[0] + 1, ne0[1] + 1, ne1[0] + 1, ne1[1] + 1])
          elif self.bnd[self.edges[ie1]][0] == self.cutBnd[iPerBound] and self.bnd[self.edges[ie0]][0] == self.pasteBnd[iPerBound] :
            match = self.match(ie1, ie0, iPerBound, shiftOperation)
            if match >= 0 :
              if self.meshPartitionned : 
                self.buildPeriodicNeighboursGhosts(ie0, ie1, match)
              ne0 = [self.edges[ie0][0], self.edges[ie0][1]]
              ne1 = [self.edges[ie1][match], self.edges[ie1][(match+1)%2]]
              self.periodicNeighbours[iPerBound].append([self.edgesId[self.edges[ie1]], self.edgesId[self.edges[ie0]], ne1[0] + 1, ne1[1] + 1, ne0[0] + 1, ne0[1] + 1])

  #def writePeriodicNeighbours(self, name, filename = None) : 
  def writePeriodicNeighbours(self, filename) : 
    #if filename == None :
    #  filename = name
    f = open(filename, "w")
    f.write("%i\n" % self.perBoundNum)
    for iPerBound in range(self.perBoundNum) :
      f.write("%i\n" % len(self.periodicNeighbours[iPerBound]))
      if len(self.periodicNeighbours[iPerBound]) == 0 :
        sys.exit("Error : No periodic boundaries found !!")
      for i in range(len(self.periodicNeighbours[iPerBound])) :
        for j in range(len(self.periodicNeighbours[iPerBound][i])) :
          if j == 2 or j == 4 :
            f.write("2 ")
          f.write("%i " % self.periodicNeighbours[iPerBound][i][j])
        f.write("\n")
    print ("Done writing  : '%s'" % filename)

  def reWriteMshFile(self, filename0, filename1) : 
    f0 = open(filename0, "r");
    f1 = open(filename1, "w")
    l = f0.readline()
    f1.write("%s" % l)
    while l != "" :
      l = f0.readline()
      f1.write("%s" % l)
      if l == "$Nodes\n" :
        n = int(f0.readline())
        f1.write("%d\n" % n)
        for i in range(n) :
          l = f0.readline()
          f1.write("%d %.16g %.16g 0\n" % ((i+1), self.nodes[i][0], self.nodes[i][1]))
      if l == "$Elements\n" :
        n = int(f0.readline())
        f1.write("%d\n" % n)
        if self.meshPartitionned == True :
          for i in range(1,n+1) :
            l = f0.readline().split()
            for j in range(2) :
              f1.write("%s " % l[j])
            nGhosts = 0
            if (int(l[1])!=15):
              nGhosts=len(self.ghosts[int(l[0])])
            f1.write("%d " % (4 +  nGhosts))
            for j in range(2) :
              f1.write("%s " % l[3+j])
            f1.write("%d %d " % (nGhosts + 1, int(l[6])))
            for j in  range(nGhosts):
              f1.write("%d " % self.ghosts[int(l[0])][j])
            if int(l[1]) == 15 :
              nNodes = 1
            elif int(l[1]) == 1 :
              nNodes = 2
            elif int(l[1]) == 2 :
              nNodes = 3
            elif int(l[1]) == 3 :
              nNodes = 4
            nOldGhost = int(l[5])
            for j in range(nNodes) : 
              f1.write("%s " % l[6+nOldGhost+j])
            f1.write("\n")
        else :
          for i in range(1,n+1) :
            l = f0.readline()
            f1.write("%s" % l)
    print ("Done writing  : '%s'" % filename1)

# -------------------- #
def periodicMap(inputMeshFilename, outputMeshFilename, periodicBoundaryNumber, shiftOperation, cutTags, pasteTags, mapFilename) :

  #for i in range(len(cutTags)) :
  #  cutTags[i] = "\"" + cutTags[i] + "\"" 
  #  pasteTags[i] = "\"" + pasteTags[i] + "\"" 

  m = Mesh(inputMeshFilename, periodicBoundaryNumber, cutTags, pasteTags)
  m.buildPeriodicNeighbours(shiftOperation)
  m.writePeriodicNeighbours(mapFilename)
  m.reWriteMshFile(inputMeshFilename, outputMeshFilename)
