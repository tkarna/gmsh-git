"""
Python file to include using: from Incompressible import *
This file defines the main functions and global parameters to be used for an 
incompressible flow problem.
"""

from dgpy import *
from Common import *
import subprocess
import math
import time
import os
import numpy
from numpy import linalg as LA

#-------------------------------------------------
#-- Generalfunctions
#-------------------------------------------------

#-------------------------------------------------
#-- Wall
#-------------------------------------------------
def WallBC_2D(FCT, XYZ):
  nbFields = FCT.size2()//2
#  print 'nbfields =',nbFields
  for i in range(0,FCT.size1()):    
    for j in range(0,nbFields):        
      FCT.set(i,j, 0.0) 
    FCT.set(i,nbFields  , fixed)
    FCT.set(i,nbFields+1, fixed)
    FCT.set(i,nbFields+2, free)
    if (nbFields == 4):
      FCT.set(i,nbFields+3, fixed) # nuTilda fixed to zero on walls
#      print 'coucou wall 2d'
    
#-------------------------------------------------
#-- Adiabatic Wall
#-------------------------------------------------

def AdiabaticWallBC_2D(FCT, XYZ):
  for i in range(0,FCT.size1()):
    FCT.set(i,0, 0.0) 
    FCT.set(i,1, 0.0) 
    FCT.set(i,2, 0.0)
    FCT.set(i,3, 0.0)
    FCT.set(i,4, fixed)
    FCT.set(i,5, fixed)
    FCT.set(i,6, free)
    FCT.set(i,7, free)

#-------------------------------------------------
#-- Wall with temperature fixed
#-------------------------------------------------


def PresBC_2D(FCT, XYZ):
  for i in range(0,FCT.size1()):
    FCT.set(i,0, .0)
    FCT.set(i,1, .0)
    FCT.set(i,2, .0) 
    FCT.set(i,3, free)
    FCT.set(i,4, free)
    FCT.set(i,5, fixed)

def NormVEL_2D(FCT, sol):
  for i in range (0,sol.size1()):
    vx = sol.get(i,0)
    vy = sol.get(i,1)
    FCT.set(i,0,math.sqrt(vx*vx+vy*vy))

def VelX_2D(FCT, XYZ ):
  nbFields = FCT.size2()//2
  for i in range(0,FCT.size1()):
    for j in range(0,nbFields):        
      FCT.set(i,j, 0.0) 
    FCT.set(i,nbFields  , free)
    FCT.set(i,nbFields+1, fixed)
    FCT.set(i,nbFields+2, free)
    if (nbFields == 4):
      FCT.set(i,nbFields+3, free) # nuTilda free at symmetry

def VelY_2D(FCT, XYZ ):
  for i in range(0,FCT.size1()):
    FCT.set(i,0, 0.0)
    FCT.set(i,1, 0.0)
    FCT.set(i,2, 0.0)
    FCT.set(i,3, fixed)
    FCT.set(i,4, free)
    FCT.set(i,5, free)

def PZero_2D(FCT, XYZ ):
  nbFields = FCT.size2()//2
  for i in range(0,FCT.size1()):
    for j in range(0,nbFields):        
      FCT.set(i,j, 0.0) 
    FCT.set(i,nbFields  , free)
    FCT.set(i,nbFields+1, free)
    FCT.set(i,nbFields+2, fixed)
    if (nbFields == 4):
      FCT.set(i,nbFields+3, free) # nuTilda free at outlet

def WallBC_3D(FCT, XYZ):
  for i in range(0,FCT.size1()):
    FCT.set(i,0, 0.0) 
    FCT.set(i,1, 0.0) 
    FCT.set(i,2, 0.0)
    FCT.set(i,3, 0.0)
    FCT.set(i,4, fixed)
    FCT.set(i,5, fixed)
    FCT.set(i,6, fixed)
    FCT.set(i,7, free)

def PZero_3D(FCT, XYZ):
  for i in range(0,FCT.size1()):
    FCT.set(i,0, .0)
    FCT.set(i,1, .0)
    FCT.set(i,2, .0) 
    FCT.set(i,3, .0) 
    FCT.set(i,4, free)
    FCT.set(i,5, free)
    FCT.set(i,6, free)
    FCT.set(i,7, fixed)

def NormVEL_3D(FCT, sol):
  for i in range (0,sol.size1()):
    vx = sol.get(i,0)
    vy = sol.get(i,1)
    vz = sol.get(i,2)
    FCT.set(i,0,math.sqrt(vx*vx+vy*vy+vz*vz)) 

#-------------------------------------------------
#-- CLASS DEFINITION
#-------------------------------------------------

class Incompressible(object):

  def __init__(self, nameMesh, dimension, domain = [""]):
    self.order = 1
    self.dimension = dimension
    self.nameMesh = nameMesh
    self.heat = False
    self.spallart_almaras = False
    
    self.model = GModel()
    #self.model.load(nameMesh+geo)
    self.model.load(nameMesh+msh)

    if (domain[0] == ""):
      self.groups = dgGroupCollection(self.model, self.dimension, self.order)
    else:
      self.groups = dgGroupCollection(self.model, self.dimension, self.order, domain)    
    self.groups.splitGroupsByPhysicalTag()
    self.XYZ = function.getCoordinates()
    
    self.maxRadius = self.groups.maxInnerRadius()

    print ("---- Initializing incompressible solver with %d elems ----" % (self.groups.getElementGroup(0).getNbElements()))

#-------------------------------------------------
#-- Initialization of problem
#-------------------------------------------------
  def initializeIncomp(self, initF, rhoF, muF, UGlob, distanceFunction = None):
    if (self.dimension == 2): 
      self.law = dgConservationLawNavierStokesIncomp2d(rhoF, muF, UGlob, None , None, None, 293,1000,distanceFunction)
    elif (self.dimension == 3):
      self.law = dgConservationLawNavierStokesIncomp3d(rhoF, muF, UGlob)

    self.explicit = False # if explicit do artificial compressibility in claw

    self.solution = dgDofContainer(self.groups, self.law.getNbFields())
    self.solutionPREV = dgDofContainer(self.groups, self.law.getNbFields())
    self.law.setPreviousSolution(self.solutionPREV.getFunction())
    self.law.setPreviousSolutionGradient(self.solutionPREV.getFunctionGradient())

    nbFields = self.dimension + 1
    if (distanceFunction) :
      self.spallart_almaras = True
      nbFields = self.dimension + 2

    if (self.dimension == 2):
      self.WALL = functionPython(2*nbFields, WallBC_2D, [self.XYZ])
      self.VELX = functionPython(2*nbFields, VelX_2D, [self.XYZ])
      self.VELY= functionPython(2*nbFields, VelY_2D, [self.XYZ])
      self.PZERO = functionPython(2*nbFields, PZero_2D, [self.XYZ])
    elif (self.dimension == 3):
      self.WALL = functionPython(2*nbFields, WallBC_3D, [self.XYZ])
      self.PZERO = functionPython(2*nbFields, PZero_3D, [self.XYZ])
      
    INIT = functionPython(nbFields, initF, [self.XYZ])
    self.solution.interpolate(INIT)
    self.solution.exportMsh('output/initNS', 0, 0)
    
#-------------------------------------------------
#-- Initialization of problem (Darcy)
#-------------------------------------------------
  def initializeIncompDarcy(self, initF, rhoF, muF, UGlob, invK, gradVs):
    if (self.dimension == 2): 
      self.law = dgConservationLawNavierStokesIncomp2d(rhoF, muF, UGlob, None , None, None, 293, 1000, None, invK, gradVs)
    elif (self.dimension == 3):
      self.law = dgConservationLawNavierStokesIncomp3d(rhoF, muF, UGlob)

    self.explicit = False # if explicit do artificial compressibility in claw

    self.solution = dgDofContainer(self.groups, self.law.getNbFields())
    self.solutionPREV = dgDofContainer(self.groups, self.law.getNbFields())
    self.law.setPreviousSolution(self.solutionPREV.getFunction())
    self.law.setPreviousSolutionGradient(self.solutionPREV.getFunctionGradient())

    nbFields = self.dimension + 1

    if (self.dimension == 2):
      self.WALL = functionPython(2*nbFields, WallBC_2D, [self.XYZ])
      self.VELX = functionPython(2*nbFields, VelX_2D, [self.XYZ])
      self.VELY= functionPython(2*nbFields, VelY_2D, [self.XYZ])
      self.PZERO = functionPython(2*nbFields, PZero_2D, [self.XYZ])
    elif (self.dimension == 3):
      self.WALL = functionPython(2*nbFields, WallBC_3D, [self.XYZ])
      self.PZERO = functionPython(2*nbFields, PZero_3D, [self.XYZ])
      
    INIT = functionPython(nbFields, initF, [self.XYZ])
    self.solution.interpolate(INIT)
    self.solution.exportMsh('output/initNS', 0, 0)
    



#-------------------------------------------------
#-- Initialization of problem with heat
#-------------------------------------------------

  def initializeIncompBoussinesq(self, initF, rhoF, muF, UGlob, heatConductivityF,heatCapacityF, heatExpansionCoefficientF, T_REF):
    
    if (self.dimension == 2): 
      self.law = dgConservationLawNavierStokesIncomp2d(rhoF, muF, UGlob,heatConductivityF,heatCapacityF, heatExpansionCoefficientF, T_REF)
      self.heat = True
    elif (self.dimension == 3):
      print ('Boussinesq Approximation not yet available in 3D')
      sys.exit(1)

    self.explicit = False # if explicit do artificial compressibility in claw

    self.solution = dgDofContainer(self.groups, self.law.getNbFields())
    self.solutionPREV = dgDofContainer(self.groups, self.law.getNbFields())
    self.law.setPreviousSolution(self.solutionPREV.getFunction())
    self.law.setPreviousSolutionGradient(self.solutionPREV.getFunctionGradient())

    if (self.dimension == 2):
      self.ADIABATIC_WALL = functionPython(8, AdiabaticWallBC_2D, [self.XYZ])
      
    INIT = functionPython(self.dimension+2, initF, [self.XYZ])
    self.solution.interpolate(INIT)
    self.solution.exportMsh('output/initNS', 0, 0)

  
#-------------------------------------------------
#-- Functions for boundary conditions
#-------------------------------------------------

  def strongBoundaryConditionPoint(self,nameBC, fctPython):
    self.law.addStrongBoundaryCondition(0,nameBC, fctPython)
    
  def strongBoundaryConditionLine(self, nameBC, fctPython):
    self.law.addBoundaryCondition(nameBC, self.law.newOutsideValueBoundary("",fctPython))
    self.law.addStrongBoundaryCondition(1, nameBC, fctPython)

  def strongBoundaryConditionSurface(self, nameBC, fctPython):
    self.law.addStrongBoundaryCondition(2,nameBC, fctPython)
    self.law.addBoundaryCondition(nameBC, self.law.newOutsideValueBoundary("",fctPython)) 

  def weakPresBoundaryCondition(self,nameBC, fctPython):
    self.law.addBoundaryCondition(nameBC, self.law.newBoundaryPressure(fctPython))

  def weakSlipBoundaryCondition(self,nameBC):
    self.law.addBoundaryCondition(nameBC, self.law.newBoundarySlipWall())

  def weakVelBoundaryCondition(self,nameBC, fctPython):
    self.law.addBoundaryCondition(nameBC, self.law.newBoundaryVelocity(fctPython))

  def weakSymmetryBoundaryCondition(self,nameBC):
    self.law.addBoundaryCondition(nameBC, self.law.newSymmetryBoundary(nameBC))

#-------------------------------------------------
#-- Solvers for NS
#-------------------------------------------------

  def steadySolve(self, maxIter, solverOptions):
    print ("---- Computing Steady solution----")
    petsc =  linearSystemPETScDouble()
    petsc.setParameter("petscOptions", solverOptions)
    dof = dgDofManager.newCG (self.groups, self.law.getNbFields(), petsc)
    self.groups.buildStreamwiseLength(self.law.getVelocity(), self.solution) 
    #self.groups.buildLaplacianOfFunction(self.solution.getFunction())
    steady = dgSteady(self.law, dof)
    steady.getNewton().setVerb(2)
    steady.getNewton().setMaxIt(maxIter)
    steady.solve(self.solution)
    self.groups.exportFunctionMsh(self.law.getVelocity(),'output/ns',0., 0, "vel", self.solution)
    self.groups.exportFunctionMsh(self.law.getPressure(),'output/ns',0., 0, "pres", self.solution)
    if (self.heat):
      self.groups.exportFunctionMsh(self.law.getTemperature(),'output/ns',0., 0, "temperature", self.solution)

#-------------------------------------------------
#-- Solvers for NS
#-------------------------------------------------

  def pseudoTimeSteppingSolve(self,timeOrder, nbTimeSteps, dt0, ATol, RTol, Verb, solverOptions, exponent = 1.5):
    print ("---- Computing steady solution with pseudo time steps %d ---"  % nbTimeSteps )
    petsc =  linearSystemPETScDouble()
    petsc.setParameter("petscOptions", solverOptions)
    dof = dgDofManager.newCG (self.groups, self.law.getNbFields(), petsc)
    t = 0.0
    dt = dt0
    implicitEuler = dgDIRK(self.law, dof, timeOrder)
    implicitEuler.getNewton().setAtol(ATol)
    implicitEuler.getNewton().setRtol(RTol)
    implicitEuler.getNewton().setVerb(Verb)

    rinit = 1.0
    for i in range (1,nbTimeSteps+1):

      t = t+dt
      print("|ITER| %d |DT| %g |T(sec)| %g" %(i, dt, t))
      self.solutionPREV.copy(self.solution)
      
      self.groups.buildStreamwiseLength(self.law.getVelocity(), self.solution) 
      #self.groups.buildLaplacianOfFunction(self.solution.getFunction())
      implicitEuler.iterate(self.solution, dt, t)
      res = implicitEuler.getNewton().getFirstResidual()

      outputstr = "output/ns%06d" % i
      self.groups.exportFunctionMsh(self.law.getVelocity(),'outputstr', t, i, "vel", self.solution)
      self.groups.exportFunctionMsh(self.law.getPressure(),'outputstr', t, i, "pres", self.solution)
      if (self.heat):
        self.groups.exportFunctionMsh(self.law.getTemperature(),'outputstr', t, i, "temperature", self.solution)
      if (self.spallart_almaras) :
        self.groups.exportFunctionMsh(self.law.getTurbulentViscosity(),'outputstr', t, i, "nuTilda", self.solution)

      if (i == 2) :
        rinit = res
        if (rinit == 0.0) :
          rinit = 1.e-22
          break
      if (i > 2) :
        print ("CONVERGENCE : %g" % (res/rinit))
        dt = dt0 * math.pow(rinit/res,exponent)
        if (dt > 60000) : 
          dt = 60000
        if (res/rinit < 1.e-8) :
          break

    return i
  
#-------------------------------------------------
#-- Solvers for NS
#-------------------------------------------------
  def initSolver(self, timeOrder, ATol, RTol, Verb, solverOptions, nbExport): 

    self.t = 0.0
    self.petsc =  linearSystemPETScDouble()
    self.petsc.setParameter("petscOptions", solverOptions)
    self.dof = dgDofManager.newCG (self.groups, self.law.getNbFields(), self.petsc)

    self.nbExport = nbExport

    if (self.explicit):
      self.solver = dgERK(self.law, self.dof, timeOrder)
    else:
      self.solver = dgDIRK(self.law, self.dof, timeOrder)
      self.solver.getNewton().setAtol(ATol)
      self.solver.getNewton().setRtol(RTol)
      self.solver.getNewton().setVerb(Verb)

  def doOneIterationDT(self, dt):
    self.t = self.t+dt
    self.solutionPREV.copy(self.solution)
    self.groups.buildStreamwiseLength(self.law.getVelocity(), self.solution) 
    #self.groups.buildLaplacianOfFunction(self.solution.getFunction())

    self.solver.iterate(self.solution, dt, self.t)
    if (self.explicit):
      res = self.solution.norm()
    else:
      res = self.solver.getNewton().getFirstResidual()

    return res

  def doOneIteration(self, dt, i):
    if (self.explicit):
      tStep = self.t + dt
      while (self.t < tStep):
        dt_loc = min(dt, 0.9*self.solver.computeInvSpectralRadius(self.solution), tStep-self.t)
        res = self.doOneIterationDT(dt_loc)
    else:
      res = self.doOneIterationDT(dt)

    if (i % self.nbExport == 0) :
      print("**NS** |ITER| %d |NORM| %g |DT| %g |T(sec)| %g" %(i, res, dt, self.t))
      outputstr = "output/ns%06d" % (i/self.nbExport)
      self.groups.exportFunctionMsh(self.law.getVelocity(),'outputstr', self.t, i//self.nbExport, "vel", self.solution)
      self.groups.exportFunctionMsh(self.law.getPressure(),'outputstr', self.t, i//self.nbExport, "pres", self.solution)


    
#-------------------------------------------------
#-- mesh Adaptation
#-------------------------------------------------

  def adaptMesh(self, nameGeo, adaptedMeshName, eps, lcMin, lcMax, nbIter, allDim):

    if (self.dimension == 2):
      self.NORMVEL = functionPython(1, NormVEL_2D, [self.solution.getFunction()])
    elif (self.dimension == 3):
      self.NORMVEL = functionPython(1, NormVEL_3D, [self.solution.getFunction()])
    myEval = dgFunctionEvaluator(self.groups, self.NORMVEL)

    #velu=functionExtractCompNew(velocity,0)
    #evalu = dgFunctionEvaluator(self.groups, velu)
    #velv=functionExtractCompNew(velocity,1)
    #evalv = dgFunctionEvaluator(self.groups, velv)
    #if (self.dimension == 3):
    #  velw=functionExtractCompNew(velocity,2)
    #  evalw = dgFunctionEvaluator(self.groups, velw)

    #geoModel = GModel()
    #geoModel.load(nameGeo+geo)
    #meshModel = GModel()
    #meshModel.load(self.nameMesh+msh)
    #myMatcher = GeomMeshMatcher()
    #myMatcher.match(geoModel, meshModel)
    #modelAdapt = GModel()
    #modelAdapt.load(geoModel)
    
    modelAdapt = GModel()
    modelAdapt.load(nameGeo+geo)
    modelAdapt.load(self.nameMesh+msh)

    modelAdapt.adaptMesh([2], [myEval], [[eps, lcMin, lcMax]], nbIter, allDim)
    #if (self.dimension == 2):
          #    modelAdapt.adaptMesh([2,2], [evalu, evalv], [[eps, lcMin, lcMax],[eps, lcMin, lcMax]], nbIter, True)
    #elif (self.dimension == 3):
    #  modelAdapt.adaptMesh([2,2,2], [evalu, evalv,evalw], [[eps, lcMin, lcMax],[eps, lcMin, lcMax],[eps, lcMin, lcMax]], nbIter, True)
            
    modelAdapt.save(self.nameMesh+msh)

    modelAdapt.save(adaptedMeshName+msh)

  #-------------------------------------------------
  #-- post processing
  #-------------------------------------------------

  def computeDragAndLift(self, surface, itime, iiter, fScale, flowDir, spanDir, fileName, nbFD = 0, hFD = 0.):
    print ("---- Computing lift and drag ----")
    fun = open(fileName,"a")
    if (iiter == 0):
      fun.write("#(1)iter (2)time (3)lift (4)drag \n")
    lift = 0.0
    lift_visc = 0.0
    lift_pres = 0.0
    drag = 0.0
    drag_visc = 0.0
    drag_pres = 0.0
    
    integ=dgFunctionIntegratorInterface(self.groups, self.law.getTotalForce(), self.solution)
    integ_pres=dgFunctionIntegratorInterface(self.groups, self.law.getInviscousNormalStress(), self.solution)
    
    force=fullMatrixDouble(3,1)
    force_pres=fullMatrixDouble(3,1)
    integ.compute(surface, force)
    integ_pres.compute(surface, force_pres)

    if (nbFD == 0) :
      integ_visc=dgFunctionIntegratorInterface(self.groups, self.law.getViscousNormalStress(), self.solution)  
      force_visc=fullMatrixDouble(3,1)
      integ_visc.compute(surface, force_visc)
    else :
      force_visc=self.integrateWallShearStressFD(surface, hFD, nbFD)
      
    # project force onto directions
    for j in range(0,3):
      drag = drag + force.get(j,0)*flowDir[j]
      drag_visc = drag_visc + force_visc.get(j,0)*flowDir[j]
      drag_pres = drag_pres + force_pres.get(j,0)*flowDir[j]
      lift = lift + force.get(j,0)*spanDir[j]
      lift_visc = lift_visc + force_visc.get(j,0)*spanDir[j]
      lift_pres = lift_pres + force_pres.get(j,0)*spanDir[j]
    drag = drag/fScale
    drag_visc = drag_visc/fScale
    drag_pres = drag_pres/fScale
    lift = lift/fScale
    lift_visc = lift_visc/fScale
    lift_pres = lift_pres/fScale

    #save in file
    fun.write(' %i %g %g %g\n' % (iiter, itime, lift, drag))
    
    fun.close()
    print ("Lift: %f (visc= %f, pres=%f)" %(lift, lift_visc, lift_pres))
    print ("Drag: %f (visc= %f, pres=%f)" %(drag, drag_visc, drag_pres))
    print ("Latex: & $%d$ & $%.4f$ & $%.4f$ & $%.4f$ & $%.4f$ &$%.4f$ & $%.4f$ \\" %(self.groups.getElementGroup(0).getNbElements(), drag, drag_visc, drag_pres,lift, lift_visc, lift_pres))

  def integrateWallShearStressFD(self, name, hFD, nbFD) : 

    if (nbFD > 5):
      print ("FD WSS evaluation up to 4th order only !")
      exit(1)
      
    OUTVAL = -1.e200

    evalSol = dgFunctionEvaluator(self.groups, self.law.getVelocity(), self.solution)
    evalSol.setDefaultValue(OUTVAL)
    resSol = fullMatrixDouble(3,1)
    evalVisc = dgFunctionEvaluator(self.groups, self.law.getViscosity())
    evalVisc.setDefaultValue(OUTVAL)
    resVisc = fullMatrixDouble(1,1)

    #Get the edges
    #############################
    nums = self.model.getEdgesByStringTag(name)
    for k in range(len(nums)):
      edge = self.model.getEdgeByTag(nums[k])
      N = edge.getNumMeshElements()

      integ = fullMatrixDouble(3,1)
      integ.setAll(0.)

      # Loop over points and get the normal direction to the edge
      for i in range (0,N) :
        
        e = edge.getMeshElement(i)
        p0 = e.getVertex(0)
        p1 = e.getVertex(1)
        p = SPoint3(0.5*(p1.x()+p0.x()), 0.5*(p1.y()+p0.y()), 0.)
        l = SVector3(p1.x()-p0.x(), p1.y()-p0.y(), 0.)
        lnorm = l.norm()
        t = SVector3(l.x()/lnorm, l.y()/lnorm, 0.)
        n = SVector3(t.y(), -t.x(), 0.)                 # Not sure normal is always well oriented

        # Revert normal if point does not exist (necessary?)
        pTest = SPoint3(p.x()+hFD*n.x(),p.y()+hFD*n.y(),0.)
        evalSol.compute(pTest.x(), pTest.y(), 0., resSol)
        if (resSol(0,0) == OUTVAL) :
          n = SVector3(-n.x(), -n.y(), 0.);

        evalVisc.compute(p.x(), p.y(), p.z(), resVisc)
        mu = resVisc(0,0)
        
        #evaluate tangent velocity at different points in normal direction
        vt = [None]*nbFD
        for j in range (0,nbFD) :
          p2x = p.x() + (j*hFD)*n.x()
          p2y = p.y() + (j*hFD)*n.y()
          p2z = p.z() + (j*hFD)*n.z()
          evalSol.compute(p2x, p2y, p2z, resSol)
          vel = SVector3(resSol.get(0,0), resSol.get(1,0), 0.)
          vn = dot(vel,n)
          vel_t = SVector3(vel.x()-vn*n.x(), vel.y()-vn*n.y(), vel.z()-vn*n.z())
          vt[j] = dot(vel_t,t)
        
        #FD computations of the derivative
        if (nbFD == 2) :
          dvtdn = (vt[1]-vt[0])/hFD
        elif (nbFD == 3) :
          dvtdn = (-vt[2] + 4.*vt[1] - 3*vt[0])/(2.*hFD)
        elif (nbFD == 4) :
          dvtdn = (1./3.*vt[3] - 3./2.*vt[2] + 3.*vt[1] - 11./6.*vt[0])/hFD
        elif (nbFD == 5) :
          dvtdn = (-1./4.*vt[4] + 4./3.*vt[3] - 3.*vt[2] + 4.*vt[1] - 25./12.*vt[0])/hFD
        
        minusTau = mu*dvtdn
        integ.set(0,0,integ(0,0)+minusTau*l.x())
        integ.set(1,0,integ(1,0)+minusTau*l.y())
   
    return integ           

#-------------------------------------------------
#-- post processing
#-------------------------------------------------

  def computeWallShearStressL2FE(self, name) :

    print ("---- Computing FE with L2 projection Wall Shear Stresses ----")
    
    pet =  linearSystemPETScDouble()
    nbFields = 9
    if (self.dimension == 3):
      nbFields = 12
    dof = dgDofManager.newCG(self.groups, nbFields, pet)
    smoothGRAD = dgDofContainer (self.groups, nbFields)
    
    l2 = L2ProjectionContinuous(dof)
    l2.apply(smoothGRAD, self.solution.getFunctionGradient())
    #smoothGRAD.interpolate(self.solution.getFunctionGradient(), self.solution);

    WSS = self.law.newWallShearStressSmooth(smoothGRAD.getFunction())  
    self.solution.exportFunctionSurf(WSS, 'output/surface', 0, 0, 'WSS_L2FE', [name])

  def computeWallShearStressFD(self, model, dirPerp, hFD, nbFD, MU, name, fScale = 1.0, NParam = 0) : 

    print ("---- Computing Finite Difference Wall Shear Stresses ----")

    if (nbFD > 5):
      print ("FD WSS evaluation up to 4th order only !")
      exit(1)

    fun = open("WSS_FD.txt","w")
    fun.write("#x y z wss \n")
    eval2 = dgFunctionEvaluator(self.groups, self.law.getVelocity(), self.solution)
    eval2.setDefaultValue(-10000.0)
    res = fullMatrixDouble(3,1)
    res.setAll(0.)

    #Get the edges
    #############################
    nums = model.getEdgesByStringTag(name)
    for k in range(len(nums)):
        edge = model.getEdgeByTag(nums[k])

        #compute number of points for evaluation
        if (NParam > 0):
          N = NParam + 1
          umin = edge.getLowerBound()
          umax = edge.getUpperBound()
          pOld = edge.point(umin)
        else:
          N =  nbV = edge.getNumMeshElements()

        #loop over points and get the normal direction to the edge
        for i in range (0,N) :
          if (NParam > 0):
            u = umin + (i*1.)/(N*1.) * (umax - umin)
            p = edge.point(u)
            der = edge.firstDer(u);
            perp = SVector3(0.0, 0.0, 1.0)
            nn = crossprod(der, perp)
            n = SVector3(nn.x()/norm(nn), nn.y()/norm(nn),nn.z()/norm(nn));
          else:
            e = edge.getMeshElement(i)
            p0 = e.getVertex(0)
            p1 = e.getVertex(1)
            p = GPoint(0.5*(p1.x()+p0.x()),0.5*(p1.y()+p0.y()),0.5*(p1.z()+p0.z()))
            der = SVector3(p1.x()-p0.x(), p1.y()-p0.y(), p1.z()-p0.z())
            nn = crossprod(der, dirPerp)
            n = SVector3(nn.x()/norm(nn), nn.y()/norm(nn),nn.z()/norm(nn));
                  
          #revert normal if point does not exist
          pTest = SPoint3(p.x()+hFD*n.x(),p.y()+hFD*n.y(),p.z()+hFD*n.z())
          me = self.model.getMeshElementByCoord(pTest)
          eval2.compute(pTest.x(), pTest.y(), pTest.z(), res)
          if ((me == None) or (res.get(0,0) == -10000.0)):
            n = SVector3(-1.*n.x(), -1.*n.y(),-1.*n.z());

          #evaluate tangent velocity at different points in normal direction
          d = [None]*nbFD
          for j in range (0,nbFD) :
            p2x = p.x() + (j*hFD)*n.x()
            p2y = p.y() + (j*hFD)*n.y()
            p2z = p.z() + (j*hFD)*n.z()
            eval2.compute(p2x, p2y, p2z, res)
            #get the wall velocity parallel to the wall
            vel = SVector3(res.get(0,0),res.get(1,0),  res.get(2,0))
            vn = dot(vel,n)
            vel_t = SVector3(vel.x() - vn*n.x(),vel.y() - vn*n.y(),vel.z() - vn*n.z())
            d[j] = norm(vel_t)
                                        
          #FD computations of the derivative
          myDer  = 0.0
          if (nbFD == 2) :
            myDer = (d[1]-d[0])/hFD
          elif (nbFD == 3) :
            myDer = (-d[2] + 4.*d[1] - 3*d[0])/(2.*hFD)
          elif (nbFD == 4) :
            myDer = (1./3.*d[3] - 3./2.*d[2] + 3.*d[1] - 11./6.*d[0])/hFD
          elif (nbFD == 5) :
            myDer = (-1./4.*d[4] + 4./3.*d[3] - 3.*d[2] + 4.*d[1] - 25./12.*d[0])/hFD
               
          #WSS computations
          WSS = MU*myDer/fScale
          fun.write("%g %g %g %g \n" % (p.x(),p.y(), p.z(), fabs(WSS)))

  
        fun.write("\n")

  def computeWallShearStressLEAST_SQUARES(self, MU, name, fScale = 1.0) : 

    print ("---- Computing Least Squares Wall Shear Stresses ----")

    fun = open("WSS_LEAST_SQUARES.txt","w")
    fun.write("#x y z wss \n")
    
    velu=functionExtractCompNew(self.solution.getFunction(),0)
    evalu = dgFunctionEvaluator(self.groups, velu)
    velv=functionExtractCompNew(self.solution.getFunction(),1)
    evalv = dgFunctionEvaluator(self.groups, velv)
    
    metricU = meshMetric(self.model)
    metricU.addMetric(2, evalu, [1000, 0.1, 1.0])
    
    metricV = meshMetric(self.model)
    metricV.addMetric(2, evalv, [1000, 0.1, 1.0])
    
    #Get the edges
    #############################
    nums = self.model.getEdgesByStringTag(name)
    for k in range(len(nums)):
      edge = self.model.getEdgeByTag(nums[k])
      N =  nbV = edge.getNumMeshElements()
      for i in range (0,N) :
        e = edge.getMeshElement(i)
        p0 = e.getVertex(0)
        p1 = e.getVertex(1)
        der = SVector3(p1.x()-p0.x(), p1.y()-p0.y(), p1.z()-p0.z())
        perp = SVector3(0.0, 0.0, 1.0)
        nn = crossprod(der, perp)
        n = SVector3(nn.x()/norm(nn), nn.y()/norm(nn),nn.z()/norm(nn));
        gradu = metricU.getGradient(p0)
        gradv = metricV.getGradient(p0)
        t1 = MU* (2.*gradu.x()*n.x()     + (gradu.y()+gradv.x())*n.y());
        t2 = MU* ((gradv.x()+gradu.y())*n.x() + 2.*gradv.y()*n.y());
        tn = t1*n.x()+t2*n.y();
        wss_x = t1 - tn*n.x();
        wss_y = t2 - tn*n.y();
        wss_norm = sqrt(wss_x*wss_x+wss_y*wss_y)
        fun.write("%g %g %g %g \n" % (p0.x(),p0.y(), p0.z(), wss_norm))


    fun.write("\n")

  def computeWallShearStressEIGEN(self, MU, name, fScale = 1.0) : 

    print ("---- Computing EigenValue Wall Shear Stresses ----")

    fun = open("WSS_EIGEN.txt","w")
    fun.write("#x y z wss \n")
    eval2 = dgFunctionEvaluator(self.groups, self.law.getViscousStress(), self.solution)
    eval2.setDefaultValue(-10000.0)
    res = fullMatrixDouble(9,1)

    velu=functionExtractCompNew(self.solution.getFunction(),0)
    evalu = dgFunctionEvaluator(self.groups, velu)
    velv=functionExtractCompNew(self.solution.getFunction(),1)
    evalv = dgFunctionEvaluator(self.groups, velv)
    
    metricU = meshMetric(self.model)
    metricU.addMetric(2, evalu, [1000, 0.1, 1.0])
    
    metricV = meshMetric(self.model)
    metricV.addMetric(2, evalv, [1000, 0.1, 1.0])

    #Get the edges
    #############################
    nums = self.model.getEdgesByStringTag(name)
    for k in range(len(nums)):
        edge = self.model.getEdgeByTag(nums[k])
        N =  nbV = edge.getNumMeshVertices()

        #loop over points and get the normal direction to the edge
        for i in range (0,N) :
          ver = edge.getMeshVertex(i)

          #compute FE derivative
          eps = 0.0 #1.e-5;
          eval2.compute(ver.x(), ver.y()*(1.+eps), ver.z(), res)
          w, v = LA.eig(numpy.array( [[ res(0,0), res(1,0) , 0.0], [res(3,0), res(4,0), 0.0] , [0.0, 0.0, 0.0] ]))
          
          #compute LS derivative
          #gradu = metricU.getGradient(ver)
          #gradv = metricV.getGradient(ver)
          #w, v = LA.eig(numpy.array( [[ 2.*gradu.x(), (gradu.y()+gradv.x()), 0.0], [(gradv.x()+gradu.y()), 2.*gradv.y(), 0.0] , [0.0, 0.0, 0.0] ]))

          #WSS computations
          maxV = w.max()
          WSS = MU*maxV/fScale
          fun.write("%g %g %g %g \n" % (ver.x(),ver.y(), ver.z(), fabs(WSS)))

  
        fun.write("\n")
