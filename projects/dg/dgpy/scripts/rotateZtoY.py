import sys
from gmshpy import *

def rotateZtoY(filename,namerotate):
  inFile = open(filename,'r')
  outFile = open(namerotate,'w')
  line=inFile.readline()
  outFile.write(line)
  while ( line != "$Nodes\n"):
    line=inFile.readline()
    outFile.write(line)
  nbNodes=int(inFile.readline())
  outFile.write("%i\n"%(nbNodes))
  for iNodes in range(nbNodes):
    line=inFile.readline().split()
    outFile.write("%s %s %s %s\n" % (line[0], line[1], line[3], line[2]))
  line=inFile.readline()
  outFile.write(line)
  while line != "":
    line=inFile.readline()
    outFile.write(line)
    
  
  inFile.close()
  outFile.close()

  Msg.Info("Done writing '"+namerotate+"'")



