from dgpy import *
import os
import math
from mpi4py import MPI
import utilities as u

# Richards equation for 1-2-3D domains
# the implicit case is the implementation of Krabbenhoft (2007)
# the explicit case use the false transient method in the saturated areas


# ==================
# Richards : OPTIONS
# ==================

class RichardsOptions(object):
  def __init__(self, isCG=False, interpolator=0, scale=1., gravitation=None, constantEta=True, petscOptions="",
                     constantKlin=True, homogeneousLimiter=False):
    self.isCG = isCG
    self.interpolator = interpolator
    self.scale = scale
    self.gravitation = gravitation
    self.constantEta = constantEta
    self.petscOptions = petscOptions
    self.constantKlin = constantKlin
    self.homogeneousLimiter = homogeneousLimiter

# =========================
# Richards : ABSTRACT CLASS
# =========================

class Richards(object):

  # initial conditions

  def seth(self, funcINI, initialDT=1):
    self.H.interpolate(funcINI)
    self.H.scatter()
    #self.W.L2Projection(self.hToTh)
    self.W.interpolate(self.hToTh)
    self.W.scatter()
    self._linK(self.H)
    self.DT.set(initialDT)

  # boundary conditions

  def bndNeumann(self, flux, tags):
    for name,law in self.laws.items():
      flux_ = self.C0 if name == 'D' else flux
      law.addBoundaryCondition(tags, law.newNeumannBoundary(flux_))

  def bndDirichlet(self, value, tags):
    for name,law in self.laws.items():
      law.addBoundaryCondition(tags, law.newOutsideValueBoundaryGeneric("",
                               [self.thToH, self.HF], [value, value],
                               [self.KlinF, self.WF], [self.K, self.hToTh]))
  def bndSlope(self, value, tags):
    #Msg.Fatal("bndSlope to test in Richards")
    for name,law in self.laws.items():
      if name == 'D' or name == 'W' : # 'W' TEMP
        law.addBoundaryCondition(tags, law.newNeumannBoundary(self.C0)) # temporary
      else:
        law.addBoundaryCondition(tags, law.newOutsideValueBoundaryGeneric("", [self.HG], [value]))

  def bndEvaporation(self, flux, atmWP, tags):
    self.evapStateOld[tags] = dgDofContainer(self.gc, 1)
    self.evapState[tags] = dgConservationLawRichardsCommon.newEvaporationBoundaryState(
                                          flux_, self.HF, atmWP, self.evapStateOld.getFunction())
    for name,law in self.laws.items():
      flux_ = self.C0 if name == 'D' else flux
      law.addBoundaryCondition(tags, dgConservationLawRichardsCommon.newEvaporationBoundary(
                                     law, "", flux_, self.evapState[name],
                                     [self.HF, self.thToH], [atmWP, atmWP],
                                     [self.KlinF, self.WF], [self.K, self.hToTh]))
  # get global mass

  def getGlobalMass(self):
    globalMass = fullMatrixDouble(1, 1)
    self.globalMass.compute(globalMass)
    return globalMass(0, 0)

  def getGlobalError(self):
    globalError = fullMAtrixDouble(1, 1)
    self.globalError.compute(globalError)
    return globalError(0, 0)

  # PRIVATE (i.e. dont touch !)
  # -------

  def __init__(self):
    print ("abstract class: use RichardsImplicit or RichardsExplicit instead !")

  # private method: load the mesh

  def mesh(self, mesh, model):
    if(not model):
      self.model = GModel()
      if(os.path.exists(mesh.replace('.msh','.geo'))):
        self.model.load(mesh.replace('.msh','.geo'))
      self.model.load(mesh)
      self.model.scaleMesh(self.options.scale)
    else:
      self.model = model
     
    self.gc = dgGroupCollection(self.model, self.model.getDim(), 1) # dim, order
    self.gc.splitGroupsByPhysicalTag()

  # private method: init functions

  def initBasics(self, soilMap):

    self.laws = {}

    # basic functions
    self.C0 = functionConstant(0.)
    self.C001 = functionConstant([0.,0.,1.])
    self.DT = function.getDT()
    self.TIME = function.getTime()
    self.XYZ = self.gc.getFunctionCoordinates()

    # degrees of freedom (fields)
    self.H = dgDofContainer(self.gc, 1)
    self.HF = self.H.getFunction()
    self.HG = self.H.getFunctionGradient()
    self.W = dgDofContainer(self.gc, 1)
    self.WF = self.W.getFunction()
    self.KlinC = dgDofContainer(self.gc, 9)
    self.KlinF = self.KlinC.getFunction()

    # soil-specific fonctions
    self.sm = soilMap
    self.sm.addDefaultInput("pressure", self.HF)
    #self.sm.addDefaultInput("water_content", self.WF)
    if(self.options.interpolator):
      nbIntPoints = 20
      if(isinstance(self.options.interpolator, int)):
        nbIntPoints = max(self.options.interpolator, 10)
      print ("nbInterpolationPoints:", nbIntPoints)
      self.interpolator = functionInterpolatorLinear()
      self.K1    = self.sm.getFunctionLT("conductivity1", nbIntPoints, self.gc, self.interpolator)
      self.K     = functionCatCompNew([self.K1, self.C0, self.C0, self.C0, self.K1, self.C0, self.C0, self.C0, self.K1])
      self.hToTh = self.sm.getFunctionLT("retention", nbIntPoints, self.gc, self.interpolator)
      self.C     = self.sm.getFunctionLT_deriv("retention", nbIntPoints, self.gc, self.interpolator)
      self.thToH = self.sm.getFunctionLT_invert("retention", nbIntPoints, self.gc, self.interpolator, self.WF)
      self.thToK = self.sm.getFunctionLT("conductivity1", nbIntPoints, self.gc, self.interpolator, ["pressure"], [self.thToH])
      self.thToC = self.sm.getFunctionLT_deriv("retention", nbIntPoints, self.gc, self.interpolator, ["pressure"], [self.thToH])
    else:
      self.K     = self.sm.getFunction("conductivity")
      self.C     = self.sm.getFunction("dRetention")
      self.hToTh = self.sm.getFunction("retention")
      self.thToH = self.sm.getFunction("retention_inv", ["water_content"], [self.WF])
      self.thToK = self.sm.getFunction("conductivity1", ["pressure"], [self.thToH])
      self.thToC = self.sm.getFunction("dRetention", ["pressure"], [self.thToH])

    #self.KlinF = dgFunctionEInterpolationNew(self.K)

    self.globalMass = dgFunctionIntegrator(self.gc, self.WF, self.W)

    self.evapState = {}
    self.evapStateOld = {}
    
  # private method: linearize K with nodal values (same as 1st order Legendre)

  def _linK(self, curSol):
    self.KlinC.interpolate(self.K, curSol, self.H)
    self.KlinC.scatter()



# =================
# Richards IMPLICIT
# =================

class RichardsImplicit(Richards):

  # constructor: set the mesh
  # init could be split mainly to catch XYZ coordinates to define source terms

  def __init__(self, mesh, soilMap=None, options=None, finalizeInitialisation=True, model=None):
    self.options = options if options else RichardsOptions()
    self.mesh(mesh, model)

    if(finalizeInitialisation):
      if(soilMap==None):
        Msg.Fatal("give a soil map to Richards constructor or use finalizeInitialisation=False")
      self.initBasics(soilMap)
      self.init()

  # finalize the initialization: to use when finalizeInitialisation=False in the constructor

  def finalizeInit(self, soilMap):
    self.initBasics(soilMap)
    self.init()

  # iterate to the next time step

  def iterate(self, dt, time):
    RichardsImplicit.current = self
    if(self.options.constantKlin):
      self._linK(self.H)
    nbNewtonSub = self.IE_H.iterate(self.H, dt, time)
    self.W.interpolate(self.hToTh)
    #self.W.L2Projection(self.hToTh)
    self.W.scatter()
    return nbNewtonSub

  # PRIVATE (i.e. dont touch !)
  # -------

  current = None # pointer to the active instance of R.I.

  # private method: create the law and the solver

  def init(self):
    self.ths = self.sm.getFunction("ths")
    self.gravitation = self.C001 if not self.options.gravitation else self.options.gravitation

    self.laws['H'] = dgConservationLawRichardsHform(self.KlinF, self.gravitation, self.C0, self.C)
    self.laws['H'].setParam("lumping", 1)
    self.laws['H'].setParam("mixed", 1)

    self.linSysSolver = linearSystemPETScDouble ()

    if(self.options.petscOptions != ""):
      self.linSysSolver.setParameter("petscOptions", self.options.petscOptions)
    elif(Msg.GetCommSize() > 1):
      self.linSysSolver.setParameter("petscOptions", "-pc_type bjacobi -sub_pc_type ilu")
    else:
      self.linSysSolver.setParameter("petscOptions", "-pc_type ilu")
    if(self.options.isCG):
      self.dofManager = dgDofManager.newCG(self.gc, 1, self.linSysSolver)
    else:
      self.dofManager = dgDofManager.newDG(self.gc, 1, self.linSysSolver)
    self.thTol = functionScaleNew(self.ths, 0.95)
    self.newtonKrabbenhoft = dgNewtonKrabbenhoft(self.dofManager, self.laws['H'], self.hToTh, self.thToH,
                                                 self.C, self.thTol, self.WF)
    self.IE_H = dgDIRK(self.newtonKrabbenhoft, 1)
    self.IE_H.getNewton().setRtol(1e-6)
    self.IE_H.getNewton().setAtol(1e-12)
    self.IE_H.getNewton().setMaxIt(50)
    self.IE_H.getNewton().setVerb(1)

    self.preProc = processPython(preProcFunc)
    if(not self.options.constantKlin):
      self.newtonKrabbenhoft.setPreProc(self.preProc)

# =================
# Richards EXPLICIT
# =================

class RichardsExplicit(Richards):

  # constructor: set the mesh
  # init could be split mainly to catch XYZ coordinates to define source terms

  def __init__(self, mesh="", soilMap=None, options=None, finalizeInitialisation=True, model=None):
    self.options = options if options else RichardsOptions()
    self.mesh(mesh, model)
    self.activeEstimator = False

    if(finalizeInitialisation):
      if(soilMap==None):
        Msg.Fatal("give a soil map to Richards constructor")
      self.initBasics(soilMap)
      self.init()

  # finalize the initialization: to use when finalizeInitialisation=False in the constructor

  def finalizeInit(self, soilMap):
    self.initBasics(soilMap)
    self.init()

  # set the number of sub-iterations on H (false transient) and on W (conservative corrector)

  def setNbIters(self, nbSubH, nbSubW):
    self.nbSubH = nbSubH
    self.nbSubW = nbSubW

  # iterate to the next time step

  def iterate(self, dt, time, nbSubHTemp=-1, nbSubWTemp=-1):

    nbSubH = nbSubHTemp if nbSubHTemp >= 0 else self.nbSubH
    nbSubW = nbSubWTemp if nbSubWTemp >= 0 else self.nbSubW

    #hmean = dgFunctionEMeanNew(self.H.getFunction())

    #i=0
    #self.H.exportMsh("output/debug%d" % (i), i, i, "h")
    #self.H.exportFunctionMsh(hmean, "output/debugM%d" % (i), i, i, "hmean"); i=i+1
    for j in range(nbSubH - 1):
      #print 'iter', j+1
      self.RK_H.iterate(self.H, dt / nbSubH, time + dt / nbSubH * j)
      #self.H.exportMsh("output/debug%d" % (i), i, i, "h")
      #self.H.exportFunctionMsh(hmean, "output/debugM%d" % (i), i, i, "hmean"); i=i+1
      if(not self.options.isCG):
        self.limiterH.apply(self.H)
      #self.H.exportMsh("output/debug%d" % (i), i, i, "h")
      #self.H.exportFunctionMsh(hmean, "output/debugM%d" % (i), i, i, "hmean"); i=i+1
      self._linK(self.H)
    if(self.activeEstimator):
      self.lastH.copy(self.H)
      self.nbSubIters.set(nbSubH)
    #print 'iter', nbSubH
    self.RK_H.iterate(self.H, dt / nbSubH, time + dt - dt / nbSubH)
    #self.H.exportMsh("output/debug%d" % (i), i, i, "h")
    #self.H.exportFunctionMsh(hmean, "output/debugM%d" % (i), i, i, "hmean"); i=i+1
    if(not self.options.isCG):
      self.limiterH.apply(self.H)
    #self.H.exportMsh("output/debug%d" % (i), i, i, "h")
    #self.H.exportFunctionMsh(hmean, "output/debugM%d" % (i), i, i, "hmean"); i=i+1
    self._linK(self.H)

    self.RK_W.iterate(self.W, dt, time)
    ###self.W.L2Projection(self.hToTh) #hack !!! no mass conservative !!
    if(self.options.isCG):
      self.L2projcont.apply(self.H, self.hProj)
    else:
      self.limiterW.apply(self.W)
      self.H.L2Projection(self.hProj)
    self.H.scatter()
    self._linK(self.H)

    self.fH.L2Projection(self.hToTh)
    self.fH.scatter()
    for j in range(nbSubW):
      self.RK_D.iterate(self.W, dt, time)
    #self.limiterW.apply(self.W) # optional: reduces (removes?) non-monotonicity

    return 1

  # execute this before using getEstimateErrors

  def setEstimator(self): #TODO make a C-function !
    self.activeEstimator = True
    self.lastH = dgDofContainer(self.gc, 1)
    self.lastHF = self.lastH.getFunction()
    self.nbSubIters = functionConstant(1)
    self.Cmod = self.laws['H'].getAppliedC()

    #self.diffC = functionMinusNew(self.C, self.Cmod)
    #self.diffH = functionMinusNew(self.HF, self.lastHF)
    #self.variationH = functionQuotientNew(self.diffH, self.DT)
    #self.variationH_sub = functionProdNew(self.variationH, self.nbSubIters)
    #self.errorOnHApprox = functionProdNew(self.diffC, self.variationH_sub)
    #self.absErrorOnApprox = functionAbsNew(self.errorOnHApprox)
    self.absErrorOnApprox = functionC(self.libName, "errorEstimator", 1, [self.C, self.Cmod, self.HF, self.lastHF,
                                                                          self.nbSubIters, self.DT])
    self.satErrorOnApprox = functionProdNew(self.absErrorOnApprox, self.state) # state=1
    self.sqErrorOnApprox = functionProdNew(self.absErrorOnApprox, self.absErrorOnApprox)
    self.globalOpL1 = dgFunctionGlobalOp(self.gc, self.absErrorOnApprox)
    self.globalOpL2 = dgFunctionGlobalOp(self.gc, self.sqErrorOnApprox)
    self.globalOpSt = dgFunctionGlobalOp(self.gc, self.satErrorOnApprox)

  # get a good approximation of the errors done on the mass fluxes

  def getEstimateErrors(self):
    if(self.activeEstimator == False):
      Msg.Fatal("first use setEstimator() befor the iteration !")
    errorL1 = self.globalOpL1.computeIntegral()
    errorL2 = self.globalOpL2.computeIntegral()

    if(errorL2(0,0) < 0):
      Msg.Fatal("hin ? L2err %e < 0 ?" % (errorL2(0,0)) ) # TODO check: this has happend and should not !
    return (errorL1(0,0), math.sqrt(errorL2(0,0)))

  def getMaxError(self):
    if(self.activeEstimator == False):
      Msg.Fatal("first use setEstimator() befor the iteration !")
    errorMax    = self.globalOpL1.computeMax()
    errorMaxSat = self.globalOpSt.computeMax()
    return (errorMax(0,0), errorMaxSat(0,0))

  # PRIVATE (i.e. dont touch !)
  # ------- 
  cppCode = '''
    #include "dgDofContainer.h"
    #include "dgMeshJacobian.h"
    #include "function.h"
    extern "C" {
      void h_projection (dataCacheMap *, fullMatrix<double> &out,   fullMatrix<double> &hmat, 
                                         fullMatrix<double> &thToHmat, fullMatrix<double> &state) {
        if(state(0,0) >= 0.) { // saturated
          for(size_t i = 0; i < out.size1(); i++) {
            out.set(i, 0, hmat(i,0) );
          }
        } else {             // unsaturated
          for(size_t i = 0; i < out.size1(); i++) {
if(thToHmat(i,0) > 0) printf("gub : h=%e, th=%e, st=%e\\n", hmat(i,0), thToHmat(i,0), state(0,0));
            out.set(i, 0, thToHmat(i,0) );
          }
        }
      }

      void KoverC (dataCacheMap *, fullMatrix<double> &out, fullMatrix<double> &Klin, fullMatrix<double> &C,
                                   fullMatrix<double> &KTh, fullMatrix<double> &CTh) {
        for(size_t i = 0; i < out.size1(); i++) {
          const double KlinMax = std::max(Klin(i, 0), std::max(Klin(i, 4), Klin(i, 8)));
          const double KoC = KlinMax / C(i, 0);
          const double KoCTh = KTh(i, 0) / CTh(i, 0);
          out.set(i, 0, std::max(KoC, KoCTh) );
        }
      }

      void size2fact (dataCacheMap *m, fullMatrix<double> &out, fullMatrix<double> &stabFact) {
        double elmtSize = m->getJacobians().elementInnerRadius(m->getGroupOfElements()->elementVectorId())(m->getElementId()) * 2.;
        double sizesizefact = elmtSize * elmtSize * stabFact(0,0);
        for(size_t i = 0; i < out.size1(); i++) {
          out.set(i, 0, sizesizefact);
        }
      }

      void errorEstimator (dataCacheMap *, fullMatrix<double> &out, fullMatrix<double> &C, fullMatrix<double> &Cmod,
                                           fullMatrix<double> &H, fullMatrix<double> &lastH, fullMatrix<double> &nbIter,
                                           fullMatrix<double> &DT) {
        for(size_t i = 0; i < out.size1(); i++) {
          double diffC = C(i, 0) - Cmod(i, 0);
          double varH  = ( H(i, 0) - lastH(i, 0) ) / DT(0, 0); //* nbIter(0, 0)
          double error = diffC * varH;
          out.set(i, 0, fabs(error));
        }
      }
    }
  '''

  # private method: create the law and the solver

  def init(self):

    self.libName = u.makeLib(self.cppCode, "tmpRichards.dylib")

    # Stabilization
    if(self.model.getDim() == 1):
      fact = 1/5. # theory diff EE: / 2    1/5 sufficient with diff on theta ? 1/25 needed without
    elif(self.model.getDim() == 2):
      fact = 1/9. # honestly, should be greater (>5^2) ! (works for abdul & gilham)
    else: # 3D
      fact = 1/15. #15. # 10< <?  #rem: for both prism/tets
    if(self.options.constantEta):
      minL = getMinL(self.gc)
      self.minL2 = functionConstant(minL * minL * fact)
      self.eta = functionQuotientNew(self.minL2, self.DT)
    else:
      self.stabFact = functionConstant(fact)
      self.L2fact_eval = functionC(self.libName, "size2fact", 1, [self.stabFact])
      self.L2factC = dgDofContainer(self.gc, 1)
      self.L2factC.interpolate(self.L2fact_eval)
      self.L2factC.scatter()
      self.eta = functionQuotientNew(self.L2factC.getFunction(), self.DT)
    self.nbSubH = 1
    self.nbSubW = 1

    # Functions
    self.fH = dgDofContainer(self.gc, 1)
    self.fHF = self.fH.getFunction()
    self.fHG = self.fH.getFunctionGradient()
    self.ID = self.sm.getIDFunction()
    self.KoC = functionC(self.libName, "KoverC", 1, [self.KlinF, self.C, self.thToK, self.thToC])
#    self.state = self.sm.getFunction("state_eta2", ["eta", "old_state", "conductivity", "capillary_capacity"],
#                                                   [self.eta, self.C0, self.KlinF, self.C])
    self.state = self.sm.getFunction("state_eta3", ["eta", "conductivity_over_capacity"],
                                                   [self.eta, self.KoC])
    self.hProj = functionC(self.libName, "h_projection", 1, [self.HF, self.thToH, self.state])

    # Conservation laws
    self.gravitation = self.C001 if not self.options.gravitation else self.options.gravitation

    self.laws['W'] = dgConservationLawRichardsExplicitH.newForced(self.KlinF, self.gravitation, self.state,
                                                                  None, self.C, self.eta, self.HF, self.HG, False)
    self.laws['H'] = dgConservationLawRichardsExplicitH(self.KlinF, self.gravitation, self.state, None, self.C, self.eta)
    self.laws['D'] = dgConservationLawRichardsExplicitThDiff(self.state, self.eta, self.fHF, self.fHG, self.thToH, self.HF)
#    self.laws['H'].setParam("lumping", 0)
#    self.laws['W'].setParam("lumping", 0)
    #self.laws['W'].setParam("upwind", 1)
    #self.laws['H'].setParam("upwind", 1)

    # Solvers
    if(self.options.isCG):
      self.linSysSolverW = linearSystemPETScDouble()
      self.linSysSolverH = linearSystemPETScDouble()
      self.linSysSolverD = linearSystemPETScDouble()
    else:
      self.linSysSolverW = dgLinearSystemMassMatrix(self.gc, True)
      self.linSysSolverH = dgLinearSystemMassMatrix(self.gc, False)
      self.linSysSolverD = dgLinearSystemMassMatrix(self.gc, True)

    if(Msg.GetCommSize() > 1):
      self.linSysSolverW.setParameter("petscOptions", "-pc_type bjacobi -sub_pc_type ilu")
      self.linSysSolverH.setParameter("petscOptions", "-pc_type bjacobi -sub_pc_type ilu")
      self.linSysSolverD.setParameter("petscOptions", "-pc_type bjacobi -sub_pc_type ilu")
    else:
      self.linSysSolverW.setParameter("petscOptions", "-pc_type lu") #ilu
      self.linSysSolverH.setParameter("petscOptions", "-pc_type lu") #ilu
      self.linSysSolverD.setParameter("petscOptions", "-pc_type lu") #ilu

    if(self.options.isCG):
      self.dofManagerW = dgDofManager.newCG(self.gc, 1, self.linSysSolverW) # TODO true ? not DG for W ?
      self.dofManagerH = dgDofManager.newCG(self.gc, 1, self.linSysSolverH)
      self.dofManagerD = dgDofManager.newCG(self.gc, 1, self.linSysSolverD)
      self.L2projcont = L2ProjectionContinuous(self.dofManagerW)
    else:
#      self.dofManagerW = None #dgDofManager.newDG(self.gc, 1, self.linSysSolverW)
#      self.dofManagerH = None
#      self.dofManagerD = None
      #self.dofManagerH = dgDofManager.newDG(self.gc, 1, self.linSysSolverH)
      #self.dofManagerD = dgDofManager.newDG(self.gc, 1, self.linSysSolverD)
      self.dofManagerW = dgDofManager.newDGBlock(self.gc, 1, self.linSysSolverW)
      self.dofManagerH = dgDofManager.newDGBlock(self.gc, 1, self.linSysSolverH)
      self.dofManagerD = dgDofManager.newDGBlock(self.gc, 1, self.linSysSolverD)
    self.RK_W = dgERK(self.laws['W'], self.dofManagerW, DG_ERK_EULER)
    self.RK_H = dgERK(self.laws['H'], self.dofManagerH, DG_ERK_EULER)
    self.RK_D = dgERK(self.laws['D'], self.dofManagerD, DG_ERK_EULER)
    
    # Limiters
    self.limiterH = dgSlopeLimiter2(self.laws['H'], self.gc)
    if(self.options.homogeneousLimiter):
      self.limiterW = dgSlopeLimiter2(self.laws['W'], self.gc) #only works if homogen !
    else:
      self.limiterW = dgSlopeLimiterField2(self.laws['W'], self.gc, self.thToH, self.HF, self.hToTh)

  # private method: split of the algorithm; only H

  def iterateH(self, dt, time, nbSubHTemp=-1):
    nbSubH = nbSubHTemp if nbSubHTemp >= 0 else self.nbSubH
    for j in range(nbSubH - 1):
      self.RK_H.iterate(self.H, dt / nbSubH, time + dt / nbSubH * j)
      if(not self.options.isCG):
        self.limiterH.apply(self.H)
      self._linK(self.H)
    if(self.activeEstimator):
      self.lastH.copy(self.H)
      self.nbSubIters.set(nbSubH)
    self.RK_H.iterate(self.H, dt / nbSubH, time + dt - dt / nbSubH)
    if(not self.options.isCG):
      self.limiterH.apply(self.H)
    self._linK(self.H)
    return 1

  # private method: split of the algorithm; all after H

  def iterateW(self, dt, time, nbSubWTemp=-1):
    nbSubW = nbSubWTemp if nbSubWTemp >= 0 else self.nbSubW

#    self.H.scatter()
    self._linK(self.H)
    self.RK_W.iterate(self.W, dt, time)

    if(self.options.isCG):
      self.L2projcont.apply(self.H, self.hProj)
    else:
      self.limiterW.apply(self.W)
      self.H.L2Projection(self.hProj)
      #self.H.interpolate(self.hProj)
    self.H.scatter()
    self._linK(self.H)

    self.fH.L2Projection(self.hToTh)
    #self.fH.interpolate(self.hToTh)
    self.fH.scatter()
    for j in range(nbSubW):
      self.RK_D.iterate(self.W, dt, time)
    #self.limiterW.apply(self.W) # optional: reduces (removes?) non-monotonicity
    return 1


# private method: linearize K during implicit sub-time steps

def preProcFunc():
  curSol = RichardsImplicit.current.newtonKrabbenhoft.getCurrentSolution()
  RichardsImplicit.current._linK(curSol)

# private method: get the size of the smallest element

def getMinL(gc):
  minL = 1e50
  for groupID in range(gc.getNbElementGroups()):
    group = gc.getElementGroup(groupID)
    for elementID in range(group.getNbElements()):
      minL = min(2. * group.getElement(elementID).getInnerRadius(), minL)
  if(Msg.GetCommSize() > 1):
    minL = MPI.COMM_WORLD.allreduce(minL, op=MPI.MIN)
  return minL

