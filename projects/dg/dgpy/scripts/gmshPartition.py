from gmshpy import *
from partition1d import *
import os

def simple(filename, keepIfExisting=True, takeExternalPartitionning=True, NPart=Msg.GetCommSize()):
  filename = filename.replace(".msh", "")

  if(NPart == 1):
    return filename + '.msh'
  
  # take an already existing partitionning if it exists (format = filename_part_x_EXT.msh)
  if takeExternalPartitionning:
    namefull = filename + "_part_%i_EXT.msh" % NPart
    if(os.path.isfile(namefull)):
      if Msg.GetCommRank() == 0 :
        print ("take externally pre-partitioned mesh")
      return namefull

  namefull = filename + "_part_%i.msh" % NPart
  # keep the already generated partitionning if .msh has not changed
  if keepIfExisting and os.path.isfile(namefull) and os.stat(namefull).st_mtime > os.stat(filename+'.msh').st_mtime:
    if Msg.GetCommRank() == 0 :
      Msg.Info("Partitioned mesh found : keeping existing mesh")
    return namefull
  if(Msg.GetCommRank() == 0):
    modelS = GModel()
    modelS.load(filename+'.msh')
    if (modelS.getDim()==1):
      partition1d(filename+'.msh', namefull, NPart)
    else:
      opt = meshPartitionOptions()
      opt.setNumOfPartitions(NPart)
      opt.createAllDims = 1
      PartitionMesh(modelS, opt)
      #modelS.createPartitionBoundaries(1, 1)
      modelS.writeMSH(namefull)
  Msg.Barrier()
  return namefull

def getNbPart(filename):
  model = GModel()
  model.load(filename)
  return model.getMeshPartitions().size();

