import sys
from gmshpy import *

def partition1d(filename,namefull,nbParts):
  if (nbParts==1):
      return filename
  inFile = open(filename,'r')
  outFile = open(namefull,'w')
  line=inFile.readline()
  outFile.write(line)
  while ( line != "$Elements\n"):
      line=inFile.readline()
      outFile.write(line)
  nbElems=int(inFile.readline())
  Nodes=[]
  Edges=[]
  for iElem in range(nbElems):
     elem = [int(x) for x in inFile.readline().split()]
     if elem[1]==15:
         Nodes.append(elem)
     else:
         Edges.append(elem)
  division=len(Edges)/nbParts
  residual=len(Edges)%nbParts
  partSize=[]
  for iPart in range (nbParts):
      if (residual>0):
          partSize.append(division+1)
          residual=residual-1
      else:
          partSize.append(division)
  
  partEdges=[]
  partNodes=[]
  iEdge=0
  for iPart in range (nbParts) :
      for iPartEdge in range(partSize[iPart]) :
          newEdge=[Edges[iEdge][i] for i in range(Edges[iEdge][2]+3)]
          ghostsPart=[]
          if iPartEdge==0 and iPart != 0 :
              ghostsPart.append(-iPart)
          if iPartEdge==partSize[iPart]-1 and iPart != nbParts-1 :
              ghostsPart.append(-(iPart+2))
          newEdge[Edges[iEdge][2]]=newEdge[Edges[iEdge][2]]+len(ghostsPart)+2
          newEdge.append(len(ghostsPart)+1)
          newEdge.append(iPart+1)
          for i in range(len(ghostsPart)) :
              newEdge.append(ghostsPart[i]) 
          for edgeN in Edges[iEdge][Edges[iEdge][2]+3:] :
              newEdge.append(edgeN)
          for node in Nodes:
              if node[node[2]+3] in newEdge[newEdge[2]+3:] :
                  newNode=node[0:node[2]+3]
                  newNode[2]=newNode[2]+2
                  newNode.append(1)
                  newNode.append(iPart+1)
                  for nodeN in node[node[2]+3:] :
                      newNode.append(nodeN)
                  partNodes.append(newNode)
          partEdges.append(newEdge)
          iEdge=iEdge+1
  
  outFile.write("%i\n"%(len(partNodes)+len(partEdges)))
  for node in partNodes :
      for val in node :
         outFile.write("%i "%val)
      outFile.write("\n")
  for edge in partEdges :
      for val in edge :
         outFile.write("%i "%val)
      outFile.write("\n")
  outFile.write("$EndElements\n")
  
  
  inFile.close()
  outFile.close()



