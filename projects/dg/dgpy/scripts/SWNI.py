from dgpy import *
import utilities as u
import gmshPartition as partition

# shallow water with the non-inertia approx (also named diffusive wave apx.)
# the mesh have to be 1D or 2D, to have the gravity in his z-axis and the topography included (z-coordinate)


class SWNIOptions(object):
  def __init__(self, isImplicit=True, scale=1., nlConstantByElement=False, microTopo=0., residualSat=0., mobileWaterDepth=0.,
                     clip = False, nlUpwindedByElement=False, constantGH=False, constantNL=False, isDG=True, kinematicWave=False,
                     picard = False, nodalNL = False, setMaxIt = -1, setAtol = -1, setRtol = -1, setVerb = -1000, controlVolumeFE = True,
                     controlVolumeFEinterface = True, DMax = -1):
    self.isImplicit = isImplicit
    self.isDG = isDG
    self.scale = scale
    self.DMax = DMax
    self.clip = clip  # only used for explicit
    self.nlConstantByElement = nlConstantByElement
    self.nlUpwindedByElement = nlUpwindedByElement
    self.mobileWaterDepth = mobileWaterDepth    
    self.microTopo = microTopo
    self.residualSat = residualSat
    self.constantGH = constantGH
    self.constantNL = constantNL
    self.kinematicWave = kinematicWave
    self.picard = picard
    self.nodalNL = nodalNL
    self.controlVolumeFE = controlVolumeFE
    self.controlVolumeFEinterface = controlVolumeFEinterface

    self.setMaxIt = setMaxIt
    self.setAtol = setAtol
    self.setRtol = setRtol
    self.setVerb = setVerb

class SWNI(object):

  # constructor: set the mesh
  # init could be split mainly to catch XYZ coordinates to define source terms
  # need to transform the topographic surface into a plane one and to extract slopes, areas and normals.

  def __init__(self, meshName="", landMap=None, source=None, options=None, finalizeInitialisation=True, model=None, tags=[]):
    self.options = options if options else SWNIOptions()

    # 1) find local slope (gravity in z direction)
    # --------------------------------------------

    if(not model):
      self.model = GModel()
      meshName = partition.simple(meshName)
      self.model.load(meshName)
      self.model.scaleMesh(self.options.scale)
      self.gc = dgGroupCollection(self.model, self.model.getDim(), 1, tags)
    else:
      self.model = model
      self.gc = dgGroupCollection(model, model.getDim()-1, 1, tags) #the model is then given for soils
    self.gc.splitGroupsByPhysicalTag()

    # init bath matrix

    size = 0
    for iG in range(self.gc.getNbElementGroups()):
      group = self.gc.getElementGroup(iG)
      size += group.getNbNodes() * group.getNbElements()
    bathM = fullMatrixDouble(size, 1)
    areaM = fullMatrixDouble(size, 1)

    # fill bath matrix

    i = 0
    XYZ = self.gc.getFunctionCoordinates()
    areaF = dgFunctionEAreaNew()
    cacheMap = dataCacheMap(dataCacheMap.NODE_MODE, self.gc)
    xyzCDouble = cacheMap.get(XYZ)
    areaCDouble = cacheMap.get(areaF)
    proxy = fullMatrixDouble()
    for iG in range(self.gc.getNbElementGroups()):
      group = self.gc.getElementGroup(iG)
      cacheMap.setGroup(group)
      for iE in range(0, group.getNbElements()):
        cacheMap.setElement(iE)
        xyzLocMat = xyzCDouble.get()
        areaLocMat = areaCDouble.get()
        for iPt in range(0, group.getNbNodes()):
          bathM.set(i, 0, xyzLocMat(iPt, 2) )  # storing only z
          areaM.set(i, 0, areaLocMat(iPt, 0) )
          i += 1

    # 2) project on the z=0 plane
    # ---------------------------

    '''
    gvertices = self.model.bindingsGetVertices()
    for v in gvertices:
      projectOnZ(v)
    gedges = self.model.bindingsGetEdges()
    for e in gedges:
      projectOnZ(e)
    gfaces = self.model.bindingsGetFaces()
    for f in gfaces:
      projectOnZ(f)
    '''
    print "erease z coord"
    xyzDof = dgDofContainer(self.gc, 6)
    xyzDof.setAll([1., 0., 1., 0., 0., 0.])
    self.gc.setCoordinates(xyzDof)

    # 3) remake all and store bathymetry
    # ----------------------------------

    # init modified gc

    #self.gc = dgGroupCollection(self.model, self.model.getDim(), 1)
    #self.gc.splitGroupsByPhysicalTag()

    # set bathymetry in the new gc

    self.bathC = dgDofContainer(self.gc, 1)
    self.areaC = dgDofContainer(self.gc, 1)
    i = 0
    for iG in range(0, self.gc.getNbElementGroups()):
      group = self.gc.getElementGroup(iG)
      BFullData = self.bathC.getGroupProxy(iG)
      AFullData = self.areaC.getGroupProxy(iG)
      for iE in range(0, group.getNbElements()):
        BLocalData = fullMatrixDouble()
        ALocalData = fullMatrixDouble()
        BFullData.getBlockProxy(iE, BLocalData)
        AFullData.getBlockProxy(iE, ALocalData)
        for iPt in range(0, group.getNbNodes()):
          BLocalData.set(iPt, 0, bathM(i, 0))
          ALocalData.set(iPt, 0, areaM(i, 0))
          i += 1
    self.bathF = self.bathC.getFunction()
    self.bathG = self.bathC.getFunctionGradient()
    self.areaF = self.areaC.getFunction()

    self.XYZ = self.gc.getFunctionCoordinates()
#    self.bathC.exportFunctionMsh(XYZ, "output/debugXYZ", 0, 0, "xyz")
    self.bathC.exportMsh("output/debugBath", 0, 0, "bath")

    # compilation of some special functions

    self.libName = u.makeLib(self.CCode(), "tmpSWNI.dylib")

    # fields

    self.H = dgDofContainer(self.gc, 1)
    self.HF = self.H.getFunction()
    self.HG = self.H.getFunctionGradient()

    if(self.options.nodalNL):
      self.NLlinC = dgDofContainer(self.gc, 1)
      self.NLlinF = self.NLlinC.getFunction()

    self.hasSolver = False

    if(finalizeInitialisation):
      if(landMap==None):
        Msg.Fatal("specify a landMap in SWNI !")
      self.finalizeInit(landMap, source)

    self.globalMass = dgFunctionIntegrator(self.gc, self.HF, self.H)
    self.BC = {} 

  # definition of surface properties and initialization of law and solver
  # the source term represents rain and/or soil exfiltration and is not mandatory

  def finalizeInit(self, landMap, source=None):

    self.landMap = landMap
    self.source = source

    if(self.options.constantGH):
      self.constantGHC = dgDofContainer(self.gc, 3)
      self.constantGHF = self.constantGHC.getFunction()
      #self.scalarizedGH = functionC(self.libName, "scalarize2D", 1, [self.HG, self.bathG])
    else:
      #self.constantGHF = functionC(self.libName, "scalarize2D", 1, [self.HG, self.bathG])
      self.constantGHF = None

    if(self.options.kinematicWave):
      self.law = dgConservationLawSWkinematic(self.bathG, None, self.source)
    else:
      self.law = dgConservationLawSWnonInertia(self.bathG, None, self.source, self.constantGHF)
    self.law.setDmax(self.options.DMax if self.options.DMax>-1 else 100)
    if(self.options.clip): self.law.setClipped(True)


    if(self.options.isImplicit and self.options.picard):
      self.setSolver()
      self.linHF = self.nlSolver.getLinearizedSolution()
      self.linHG = self.nlSolver.getLinearizedSolutionGradient()
    else:
      self.linHF = self.HF
      self.linHG = self.HG

    # compute the non linear term from landMap (diverses possible options)
    if(self.options.nlConstantByElement):
      self._H   = functionMeanP1New(self.linHF, self.linHG)
      self._Hnl = functionMeanP1New(self.HF, self.HG)
    elif(self.options.nlUpwindedByElement):
      self._H   = functionC(self.libName, "nlUpwindedByElement", 1, [self.linHF, self.XYZ])
      self._Hnl = functionC(self.libName, "nlUpwindedByElement", 1, [self.HF, self.XYZ])
    else:
      self._H   = self.linHF
      self._Hnl = self.HF
    if(self.options.mobileWaterDepth):
      self._mwd = functionConstant(self.options.mobileWaterDepth)
      self.mobileWD = functionC(self.libName, "mobileWaterDepth", 1, [self._H, self._mwd])
      self.mobileWDnl = functionC(self.libName, "mobileWaterDepth", 1, [self._Hnl, self._mwd])
      self._nl = self.landMap.getFunction("nlFactor", ["water_height", "water_height_nonlinear"], [self.mobileWD, self.mobileWDnl])
      self.law.setMobileWaterDepth(self._mwd)
    else:
      self.mobileWD = self._Hnl
      self._nl = self.landMap.getFunction("nlFactor", ["water_height", "water_height_nonlinear"], [self._H, self._Hnl])
    if(self.options.constantNL):
      self.linSysSolverContinuous = linearSystemPETScDouble()
      self.dofManagerContinuous = dgDofManager.newCG(self.gc, 1, self.linSysSolverContinuous)
      self.L2projCont = L2ProjectionContinuous(self.dofManagerContinuous)
      self._nlC = dgDofContainer(self.gc, 1)
      self._nlF = functionC(self.libName, "minZero", 1, [self._nlC.getFunction()])
    else:
      self._nlF = self._nl
    if(self.options.microTopo):
      self._mt = functionConstant(self.options.microTopo)
      self._rh = functionConstant(self.options.residualSat)
      self._kro = functionC(self.libName, "obstructAndRillStorageExclusion", 1, [self._mt, self._rh, self.HF])
      self.__nl = functionProdNew(self._nlF, self._kro)
    else:
      self.__nl = self._nlF
    if(self.options.nodalNL):
      self.nl = self.NLlinF
    else:
      self.nl = self.__nl

    self.law.setNLTerm(self.nl)
    if(self.options.controlVolumeFE):
      self.C1 = functionConstant(1.)
      self.law.setControlVolumes(self.nl, self.C1, self.bathF, self.options.controlVolumeFEinterface)
      '''
      if(self.options.microTopo):
#        self.law.setControlVolumes(self._kro, self._nlF, self.bathF) # as in GW
        self.C1 = functionConstant(1.)
        self.law.setControlVolumes(self.__nl, self.C1, self.bathF) # all nodal
      else:
        self.C1 = functionConstant(1.)
        #self.law.setControlVolumes(self.C1, self.nl, self.bathF)
        self.law.setControlVolumes(self.nl, self.C1, self.bathF)
        '''
    else:
      self.options.controlVolumeFEinterface = False

    #self.interfaceTermBnd = self.law.getNewInterfaceTermBnd(self.bathG, self.nl)
    self.boundaryFlux = {} #dgFunctionIntegratorInterface(self.gc, self.interfaceTermBnd, self.H)
    self._bndIntFluxSlope = {}

    # function for the Critical Depth BC

    self.criticalDepthFlux = functionC(self.libName, "criticalDepthBnd", 1, [self.mobileWD])

  def setSolver(self):
    if(self.options.isImplicit):
      self.linSysSolver = linearSystemPETScDouble()
      if(Msg.GetCommSize() > 1):
        self.linSysSolver.setParameter("petscOptions", "-pc_type bjacobi -sub_pc_type ilu")
      else:
        self.linSysSolver.setParameter("petscOptions", "-pc_type ilu")
      if(self.options.isDG):
        self.dofI = dgDofManager.newDG (self.gc, 1, self.linSysSolver)
      else:
        self.dofI = dgDofManager.newCG (self.gc, 1, self.linSysSolver)
      if(self.options.picard):
        self.nlSolver = dgPicard(self.dofI, self.law)
      else:
        self.nlSolver = dgNewton(self.dofI, self.law)
      self.solver = dgDIRK(self.nlSolver, 1)
      self.solver.getNewton().setRtol(1e-6 if self.options.setRtol<0 else self.options.setRtol)
      self.solver.getNewton().setMaxIt(50 if self.options.setMaxIt<0 else self.options.setMaxIt)
      if self.options.setAtol>0: self.solver.getNewton().setAtol(self.options.setAtol)
      if self.options.setVerb>-1000: self.solver.getNewton().setVerb(self.options.setVerb)
      self.solver.getNewton().returnZeroIfNotConverged(True)
    else:
      self.solver = dgERK(self.law, None, DG_ERK_EULER)
    self.hasSolver = True
    if(self.options.isImplicit and self.options.nodalNL):
      self.preProc = processPython(preProcLinNL)
      self.nlSolver.setPreProc(self.preProc)


  # initial condition

  def setH(self, funcINI):
    self.H.interpolate(funcINI)
    self.H.scatter()
    if(self.options.nodalNL):
      self._linNL(self.H)
    if(not self.hasSolver):
      self.setSolver()

  def copy(self, otherSWNI):
    self.H.copy(otherSWNI.H)
    if(self.options.nodalNL):
      self._linNL(self.H)
    if(not self.hasSolver):
      self.setSolver()

  # boundary conditions

  def bndNeumann(self, flux, tags):
    self.law.addBoundaryCondition(tags, self.law.newNeumannBoundary(flux))
    self._addBC(tags, flux, 'N')

  def bndDirichlet(self, value, tags):
    self.law.addBoundaryCondition(tags, self.law.newOutsideValueBoundaryGeneric("", [self.HF], [value]))
    self._addBC(tags, value, 'D')

  def bndSlope(self, value, tags):
    self.law.addBoundaryCondition(tags, self.law.newOutsideValueBoundaryGeneric("", [self.HG], [value]))
    self._addBC(tags, value, 'S')

  def bndCriticalDepth(self, tags):
    self.law.addBoundaryCondition(tags, self.law.newNeumannBoundary(self.criticalDepthFlux) )
    self._addBC(tags, 0, 'C')

  # set the maximal diffusion (if it does not converges, decrease it!). Default=100

  def setDMax(self, num):
    self.law.setDmax(num)

  # mass informations

  def getGlobalMass(self):
    globalMass = fullMatrixDouble(1, 1)
    self.globalMass.compute(globalMass)
    return globalMass(0, 0)

  def getBoundaryFlux(self, tag):
    if tag not in self.boundaryFlux:
      (typeBC, fct) = self.BC[tag]
      if(typeBC == 'N'):
        self.boundaryFlux[tag] = dgFunctionIntegratorInterface(self.gc, fct, self.H)
      elif(typeBC == 'C'):
        self.boundaryFlux[tag] = dgFunctionIntegratorInterface(self.gc, self.criticalDepthFlux, self.H)
      elif(typeBC == 'S'):
        self._bndIntFluxSlope[tag] = self.law.getNewInterfaceSlopeBnd(self.bathG, self.nl, fct)
        self.boundaryFlux[tag] = dgFunctionIntegratorInterface(self.gc, self._bndIntFluxSlope[tag], self.H)
      else:
        Msg.Error("this type of BC is not implemented in getbndflux :-)")
    flux = fullMatrixDouble(1, 1)
    if self.options.controlVolumeFE and self.options.controlVolumeFEinterface:
      self.boundaryFlux[tag].computeCV(tag, flux)
    else:
      self.boundaryFlux[tag].compute(tag, flux)
    return flux(0, 0)

  current = None # pointer to the active instance of SWNI

  # iterate to the next time step

  def iterate(self, dt, time):
    SWNI.current = self
    if(self.options.constantGH):
      #self.constantGHC.L2Projection(self.scalarizedGH)
      #self.constantGHC.L2Projection(self.HG)
      self.constantGHC.interpolate(self.HG)
      self.constantGHC.scatter()
    if(self.options.constantNL):
      self._nlC.interpolate(self._nl)
      #self.L2projCont.apply(self._nlC, self._nl)
      self._nlC.scatter()
      #self._nlC.exportMsh("output/debugNL.msh", 1, 1, "NL")
    return self.solver.iterate(self.H, dt, time)

  def _addBC(self, tags, fct, bctype):
    if isinstance(tags, str):
      self.BC[tags] = (bctype, fct)
    else:
      for tag in tags:
        self.BC[tag] = (bctype, fct)

  def _linNL(self, curSol):
    self.NLlinC.interpolate(self.__nl, curSol, self.H)
    self.NLlinC.scatter()

  def CCode(self):
    return '''
#include "fullMatrix.h"
#include "SVector3.h"
#include "function.h"
#include "dgGroupOfElements.h"
#include "MElement.h"
extern "C" {
  void criticalDepthBnd(dataCacheMap *m, fullMatrix<double> &out, fullMatrix<double> &hSurf) {
    for (size_t i = 0; i < out.size1(); i++) {
      double h = std::max(0., hSurf(i, 0) );
      double crDpth = sqrt(9.81 * h * h * h);
      out.set(i, 0, - crDpth );
    }
  }

  void mobileWaterDepth(dataCacheMap *m, fullMatrix<double> &out, fullMatrix<double> &hs, fullMatrix<double> &mwd) {
    for (size_t i = 0; i < out.size1(); i++)
      out.set(i, 0, std::max(0., hs(i,0) - mwd(i,0)) );
  }

  void obstructAndRillStorageExclusion(dataCacheMap *m, fullMatrix<double> &out, fullMatrix<double> &microtopo,
                                       fullMatrix<double> &residualSat, fullMatrix<double> &H) {
    for (size_t i = 0; i < out.size1(); i++) {
      double Sws = H(i, 0) / microtopo(i, 0);
      double Sws_r = residualSat(i, 0);
      if(Sws >= 1.)
        out.set(i, 0, 1.);
      else if(Sws <= Sws_r)
        out.set(i, 0, 0.);
      else {
        double Ses = (Sws - Sws_r) / (1. - Sws_r);
        out.set(i, 0, pow(Ses, 2. * (1. - Ses) ) );
      }
    }
  }

  //TODO generalize to grav != 001
  void nlUpwindedByElement (dataCacheMap *m, fullMatrix<double> &out, fullMatrix<double> &Hmat, fullMatrix<double> &XYZ) {
    double H = Hmat(0, 0) + XYZ(0, 2);
    double h = Hmat(0, 0);
    for (size_t i = 1; i < out.size1(); i++) {
      double Hi = Hmat(i, 0) + XYZ(i, 2);
      if(Hi > H) {
        H = Hi;
        h = Hmat(i, 0);
      }
    }
    for (size_t i = 0; i < out.size1(); i++)
      out.set(i, 0, h);
  }

  void scalarize2D (dataCacheMap *m, fullMatrix<double> &out, fullMatrix<double> &gH, fullMatrix<double> &gB) {
    for (size_t i = 0; i < out.size1(); i++) {
      double gx = gH(i, 0) + gB(i, 0);
      double gy = gH(i, 1) + gB(i, 1);
      out.set(i, 0, sqrt(gx * gx + gy * gy) );
    }
  }

  void minZero (dataCacheMap *m, fullMatrix<double> &out, fullMatrix<double> &x) {
    for (size_t i = 0; i < out.size1(); i++) {
      out.set(i, 0, std::max(1e-10, x(i, 0)) );
    }
  }
}
'''

def projectOnZ(entity):
  for iV in range(0, entity.getNumMeshVertices()):
    v = entity.getMeshVertex(iV)
    v.setXYZ(v.x(), v.y(), 0.)

# private method: linearize K during implicit sub-time steps

def preProcLinNL():
  curSol = SWNI.current.nlSolver.getCurrentSolution()
  SWNI.current._linNL(curSol)

