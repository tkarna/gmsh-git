#include "functionLookupTable.h"

static std::vector<double> getPointValues(double x, dataCacheDouble *dcdX, dataCacheDouble *dcdF) {
  fullMatrix<double> &xMat = dcdX->set();
  xMat(0, 0) = x;
  const fullMatrix<double> &fMat = dcdF->get();
  std::vector<double> v(fMat.size2());
  for(int k = 0; k < fMat.size2(); k++)
    v[k] = fMat(0, k);
  return v;
}

/*
static std::vector<double> getPointValues(double *x, dataCacheDouble *dcdX, dataCacheDouble *dcdF) {
  fullMatrix<double> &xMat = dcdX->set();
  for(int k = 0; k < xMat.size2(); k++)
    xMat(0, k) = x[k];
  const fullMatrix<double> &fMat = dcdF->get();
  std::vector<double> v(fMat.size2());
  for(int k = 0; k < fMat.size2(); k++)
    v[k] = fMat(0, k);
  return v;
}*/


functionLT::functionLT(const function *f, const function *x, const dgGroupCollection * gc, functionInterpolator * ip, int nbSubPoly, const function *df, dgGroupOfElements *group):
  function(f->getNbCol()),
  _ip(ip),
  _selfIP(false),
  _x(x),
  _f(f),
  _df(df),
  _nbSubPoly(nbSubPoly),
  _sizeF(f->getNbCol()),
  _sizeX(x->getNbCol()),
  _dcm(dataCacheMap::POINT_MODE, gc)
{
  ini(group);
  setArgument(_variable, x);
}

functionLT::functionLT(const functionLT *fLT, const function *y, dgGroupOfElements *group) :
  function(fLT->_f->getNbCol()),
  _ip(fLT->_ip),
  _selfIP(false),
  _x(fLT->_x),
  _f(fLT->_f),
  _df(fLT->_df),
  _nbSubPoly(fLT->_nbSubPoly),
  _sizeF(fLT->_f->getNbCol()),
  _sizeX(fLT->_x->getNbCol()),
  _dcm(dataCacheMap::POINT_MODE, fLT->_dcm.getGroupCollection())
{
  ini(group);
  setArgument(_variable, y);
}

functionLT::functionLT(const functionLT& copy) :
  function(copy._f->getNbCol()),
  _ip(copy._ip), 
  _selfIP(false),
  _x(copy._x),
  _f(copy._f),
  _df(copy._df),
  _nbSubPoly(copy._nbSubPoly),
  _sizeF(copy._f->getNbCol()),
  _sizeX(copy._x->getNbCol()),
  _dcm(dataCacheMap::POINT_MODE, copy._dcm.getGroupCollection())
{
  ini((dgGroupOfElements*) copy._dcm.getGroupOfElements());
  setArgument(_variable, _x);
  for(int i = 0; i < _nbSubPoly * _nbCol * _ip->getOrder(); i++)
    _data[i] = copy._data[i];
}
functionLT::~functionLT() {
  if(_data) delete [] _data;
  if(_selfIP) delete _ip;
}
void functionLT::ini(dgGroupOfElements *group) {
  _dcdF = _dcm.get(_f);
  _dcdX = _dcm.get(_x);
  _dcdDF = _df ? _dcm.get(_df) : NULL;
  if(group) //to set the group physical tag
    _dcm.setPoint(group, 0, 0., 0., 0.);
  _data = new double[_ip->getOrder() * _nbSubPoly * _nbCol];
  setArgument(_coordinates, function::getCoordinates());
}
void functionLT::derivate() {
  functionInterpolator *tmpip = _ip;
  int order = _ip->getOrder() - 1;
  switch(order) {
    case 0:
      Msg::Fatal("can't derive zero-order function");
    case 1:
      _ip = new functionInterpolatorConstant(_ip->getInterpMethod());
      break;
    case 2:
      _ip = new functionInterpolatorLinear(_ip->getInterpMethod());
      break;
    case 3:
      _ip = new functionInterpolatorQuadratic(_ip->getInterpMethod());
      break;
    case 4:
      _ip = new functionInterpolatorCubic(_ip->getInterpMethod());
      break;
    case 5:
      _ip = new functionInterpolatorPolynom(order, _ip->getInterpMethod());
  }
  if(_selfIP) delete tmpip;
  _selfIP = true;
  double * tmpdata = _data;
  _data = new double[order * _nbSubPoly * _nbCol];
  for(int i = 0; i < _nbSubPoly; i++)
    for(int k = 0; k < _nbCol; k++)
      for(int p = 0; p < order; p++)
        _data[i * _nbCol * order + k * order + p] = tmpdata[i * _nbCol * (order+1) + k * (order+1) + p+1] * (p+1);
  relinkData();
  delete []tmpdata;
}



functionLTClassic::functionLTClassic(const function *f, const function *x, const dgGroupCollection * gc, functionInterpolator *ip, double begin, double end, 
                                     int steps, const function *df) : functionLT(f, x, gc, ip, steps, df), _begin(begin), _end(end) {
  if(x->getNbCol() != 1)
    Msg::Fatal("this lookup table works only with a variable of size 1");
  double step = (_end - _begin) / steps;
  std::set<double> nexus;
  for (int i = 0; i < steps; i++)
    nexus.insert(_begin + step * i);
  nexus.insert(_end);
  _1ostep = 1. / step;
  _ip->set(this, nexus, _data);
}

void functionLTClassic::call(dataCacheMap *m, fullMatrix<double> &val) {
  for (int i = 0; i < val.size1(); i++) {
    double x = _variable(i, 0);
    if(x < _end && x > _begin) {
      int nbCol = val.size2();
      int order = _ip->getOrder();
      int nbColxOrder = nbCol * order;
      for (int k = 0; k < nbCol; k++)
        val.set(i, k, _ip->get(x, _data + int( (x-_begin)*_1ostep) * nbColxOrder + k * order) );
    } else { //worse case: not in the given range
      _dcm.setPoint((dgGroupOfElements*) m->getGroupOfElements(), m->elementId(i/m->nPointByElement()), _coordinates(i, 0), _coordinates(i, 1), _coordinates(i, 2));
      std::vector<double> f = getPointValues(x, _dcdX, _dcdF);
      for (int k = 0; k < val.size2(); k++)
        val.set(i, k, f[k]);
    }
  }
}

functionLTClassicOne::functionLTClassicOne(const function *f, const function *x, const dgGroupCollection * gc, functionInterpolator *ip, 
                       double begin, double end, int steps, const function *df) : functionLT(f, x, gc, ip, steps, df), _begin(begin), _end(end) {
  if(x->getNbCol() != 1 || f->getNbCol() != 1)
    Msg::Fatal("this lookup table works only with a variable of size 1 and function of size 1");
  double step = (_end - _begin) / steps;
  std::set<double> nexus;
  for (int i = 0; i < steps; i++)
    nexus.insert(_begin + step * i);
  nexus.insert(_end);
  _1ostep = 1. / step;
  _ip->set(this, nexus, _data);
}
void functionLTClassicOne::call(dataCacheMap *m, fullMatrix<double> &val) {
  for (int i = 0; i < val.size1(); i++) {
    int id = int( (_variable(i, 0) - _begin) * _1ostep );
    id = std::max(0, std::min(_nbSubPoly-1, id));
    val.set(i, 0, _ip->get(_variable(i, 0), _data + id * _ip->getOrder()) );
  }
}


functionLTClassicOneCubic::functionLTClassicOneCubic(const function *f, const function *x, const dgGroupCollection * gc, 
                       double begin, double end, int steps, const function *df) : functionLT(f, x, gc, NULL, steps, df), _begin(begin), _end(end) {
  if(x->getNbCol() != 1 || f->getNbCol() != 1)
    Msg::Fatal("this lookup table works only with a variable of size 1 and function of size 1");
  Msg::Fatal("disabled: not-initial ip has to be taken into account"); //maybe more convenient with templates ?
  _ip = new functionInterpolatorCubic("");
  double step = (_end - _begin) / steps;
  std::set<double> nexus;
  for (int i = 0; i < steps; i++)
    nexus.insert(_begin + step * i);
  nexus.insert(_end);
  _1ostep = 1. / step;
  _ip->set(this, nexus, _data);
}
functionLTClassicOneCubic::~functionLTClassicOneCubic() {
  if(_ip) delete _ip;
}
void functionLTClassicOneCubic::call(dataCacheMap *m, fullMatrix<double> &val) {
  for (int i = 0; i < val.size1(); i++)
    val.set(i, 0, _ip->get(_variable(i, 0), _data + int( (_variable(i, 0)-_begin)*_1ostep ) * _ip->getOrder()) );
}

/*void functionLTClassicMulti::call(dataCacheMap *m, fullMatrix<double> &val) {
  for (int i = 0; i < val.size1(); i++) {
    bool allInRange = true;
    for(int k = 0; k < val.size2(); k++)
      allInRange &= _variable(i, k) < _end && _variable(i, k) > _begin;
    if(allInRange) {
      for (int k = 0; k < val.size2(); k++)
        val.set(i, k, _ip->get(_variable(i, k), _data + int( (x-_begin)*_1ostep ) * _ip->getOrder()) );
    } else { //worse case: not in the given range
      _dcm.setPoint((dgGroupOfElements*) m->getGroupOfElements(), m->getElementId(), _coordinates(i, 0), _coordinates(i, 1), _coordinates(i, 2));
      fullMatrix<double> &xMat = _dcdX->set();
      for (int k = 0; k < val.size2(); k++)
        xMat(0, k) = x;
      const fullMatrix<double> &f = _dcdF->get();
      for (int k = 0; k < val.size2(); k++)
        val.set(i, k, f(0, k));
    }
  }
}*/

functionLTTree::functionLTTree(const function *f, const function *x, const dgGroupCollection * gc, functionInterpolator *ip, std::set<double> &nexus,
                               const function *df, dgGroupOfElements *group) : functionLT(f, x, gc, ip, nexus.size()-1, df, group) {
  if(x->getNbCol() != 1 || f->getNbCol() != 1)
    Msg::Fatal("this lookup table works only with a variable of size 1 and function of size 1");
  _ip->set(this, nexus, _data);
  iniTree(nexus);
}
void functionLTTree::iniTree(std::set<double> nexus) {
  _begin = *(nexus.begin());
  _end   = *(nexus.rbegin());
  std::set<double>::iterator it = nexus.begin();
  it++;
  for(int i = 0; i < _nbSubPoly; i++)
    _tree[*it++] = _data + i * _f->getNbCol() * _ip->getOrder();
}
void functionLTTree::relinkData() {
  std::map<double,double*>::iterator it = _tree.begin();
  for(size_t i = 0; i < _tree.size(); i++) {
    (it++)->second = _data + i * _sizeF * _ip->getOrder();
  }
}
functionLTTree::functionLTTree(const functionLTTree *fLT, const function *y, std::set<double> nexus, dgGroupOfElements *group) :
        functionLT(fLT, y, group) {
  std::set<double> newNexus = _ip->setInverse((functionLTTree *) fLT, nexus, _data);
  iniTree(newNexus);
}
functionLTTree * functionLTTree::newInverse(const function *y, std::set<double> originalNexus, dgGroupOfElements *group) {
  if(y->getNbCol() != 1)
    Msg::Fatal("this lookup table works only with a variable of size 1 and function of size 1");
  functionLTTree * fLTTree = new functionLTTree(this, y, originalNexus, group);
  return fLTTree;
}

/*void functionLTTree::call(dataCacheMap *m, fullMatrix<double> &val) {
  for (int i = 0; i < val.size1(); i++) {
    double x = _variable(i, 0);
    if(x < _end && x > _begin) {
      for (int k = 0; k < _sizeF; k++)
        val.set(i, k, _ip->get(x, (*_tree.upper_bound(x)).second) );
    } else { //worse case: not in the given range
      std::vector<double> f = getPointValues(x, _dcdX, _dcdF);
      for (int k = 0; k < val.size2(); k++)
        val.set(i, k, f[k]);
    }
  }
}*/
void functionLTTree::call(dataCacheMap *m, fullMatrix<double> &val) {
  for (int i = 0; i < val.size1(); i++) {
    double x = _variable(i, 0);
    for (int k = 0; k < _sizeF; k++) {
      std::map<double, double*>::iterator it = _tree.upper_bound(x);
      if(it == _tree.end()) it--; //avoid extreme upper bound
      val.set(i, k, _ip->get(x, (*it).second) );
    }
  }
}






void functionInterpolatorConstant::set(functionLT *fLT, std::set<double> &nexus, double *data) {
  if(_interpMethod == "" || _interpMethod == "nodalValues") {
    std::set<double>::iterator it = nexus.begin();
    double x0 = *it++;
    for(size_t i = 0; i < nexus.size() - 1; i++) {
      double x1 = *it++;
      std::vector<double> f0 = getPointValues(x0, fLT->getdcdX(), fLT->getdcdF());
      std::vector<double> f1 = getPointValues(x1, fLT->getdcdX(), fLT->getdcdF());
      for(size_t k = 0; k < f0.size(); k++)
        data[i * f0.size() + k] = (f0[k] + f1[k]) * .5;
      x0 = x1;
    }
  }
  else
    Msg::Fatal("interpolation method unknown. Available: nodalValues (default)");
}

void functionInterpolatorLinear::set(functionLT *fLT, std::set<double> &nexus, double *data) {
  if(_interpMethod == "" || _interpMethod == "nodalValues") {
    std::set<double>::iterator it = nexus.begin();  
    double x0 = *it++;
    for(size_t i = 0; i < nexus.size() - 1; i++) {
      double x1 = *it++;
      if(fLT->getSizeX() == 1) {
        std::vector<double> f0 = getPointValues(x0, fLT->getdcdX(), fLT->getdcdF());
        std::vector<double> f1 = getPointValues(x1, fLT->getdcdX(), fLT->getdcdF());
        std::vector<double> p(f0.size());
        for(size_t k = 0; k < f0.size(); k++)
          p[k] = (f1[k] - f0[k]) / (x1 - x0);
        for(size_t k = 0; k < f0.size(); k++) {
          data[i * f0.size() * 2 + k * 2    ] = f0[k] - x0 * p[k];
          data[i * f0.size() * 2 + k * 2 + 1] = p[k];
        }
      } else {
        Msg::Error("linear interp multi not implemented");
        /*for(int k = 0; k < fLT->getSizeX(); k++)
          xMat(0, k) = nexus[i];
        fullMatrix<double> f0Mat = *(fLT->_dcdF->get()); //copy
        for(int k = 0; k < fLT->getSizeX(); k++)
          xMat(0, k) = nexus[i+1];
        fullMatrix<double &f1Mat = fLT->_dcdF->get();
        std::vector<double> p(f0Mat.size2());
        for(int k = 0; k < fLT->getSizeX(); k++)
          data[i * fMat.size2() + k] = fMat(0, k);*/
      }
      x0 = x1;
    }
  }
  else
    Msg::Fatal("interpolation method unknown. Available: nodalValues (default)");
}
std::set<double>::iterator getLastElement(std::set<double> set) {
  std::set<double>::iterator it = set.end();
  if(it != set.begin())
    return --it;
  return set.begin();
}
std::set<double> functionInterpolatorLinear::setInverse(functionLT *fLT, std::set<double> &nexus, double *data) {
  std::set<double> newNexus;
  if(_interpMethod == "" || _interpMethod == "nodalValues") {
    set(fLT, nexus, data);

    std::set<double>::iterator it = nexus.begin();
    newNexus.insert(data[0] + data[1] * *it++);
    for(size_t i = 0; i < nexus.size() - 1; i++) {
      int id = i * fLT->getNbCol() * 2;
      double nextNexus = data[id] + data[id+1] * *it++;
      if(newNexus.find(nextNexus) == newNexus.end()) {
        newNexus.insert(getLastElement(newNexus), nextNexus);
        for(int k = 0; k < fLT->getNbCol(); k++) {
          int idk = id + k * 2;
          data[idk  ] = - data[idk] / data[idk+1] ;
          data[idk+1] = 1. / data[idk+1];
        }
      }
      else { // if inf-slope 
        newNexus.insert(getLastElement(newNexus), *getLastElement(newNexus) + 1e-8);
        for(int k = 0; k < fLT->getNbCol(); k++) {
          int idk = id + k * 2;
          data[idk  ] = -data[idk] / 1e-16;
          data[idk+1] = 1. / 1e-16;
        }
      }
    }
  }
  else
    Msg::Fatal("interpolation method unknown. Available: nodalValues (default)");  
  return newNexus;
}

void functionInterpolatorQuadratic::set(functionLT *fLT, std::set<double> &nexus, double *data) {
  if(_interpMethod == "" || _interpMethod == "nodalAndMiddleValues") {
    if(fLT->getSizeX() > 1)
      Msg::Error("quadratic interp not implemented with vectorial variable");
    std::set<double>::iterator it = nexus.begin();
    double x0 = *it++;
    for(size_t i = 0; i < nexus.size() - 1; i++) {
      double x2 = *it++;
      double x1 = (x0 + x2) * .5;
      std::vector<double> f0 = getPointValues(x0, fLT->getdcdX(), fLT->getdcdF());
      std::vector<double> f1 = getPointValues(x1, fLT->getdcdX(), fLT->getdcdF());
      std::vector<double> f2 = getPointValues(x2, fLT->getdcdX(), fLT->getdcdF());
      for(size_t k = 0; k < f0.size(); k++) {
        double Y0_A = f0[k] / (x0-x1) / (x0-x2);
        double Y1_B = f1[k] / (x1-x0) / (x1-x2);
        double Y2_C = f2[k] / (x2-x0) / (x2-x1);
        data[i * f0.size() * 3 + k * 3    ] =  (x1*x2) * Y0_A + (x0*x2) * Y1_B + (x0*x1) * Y2_C;
        data[i * f0.size() * 3 + k * 3 + 1] = -(x1+x2) * Y0_A - (x0+x2) * Y1_B - (x0+x1) * Y2_C;
        data[i * f0.size() * 3 + k * 3 + 2] = Y0_A + Y1_B + Y2_C;
      }
      x0 = x2;
    }
  }
  else
    Msg::Fatal("interpolation method unknown. Available: nodalAndMiddleValues (default)");
}

#define EPSILON_DERIVATIVE_INTERP 1e-8

void functionInterpolatorCubic::set(functionLT *fLT, std::set<double> &nexus, double *data) {
  if(_interpMethod == "" || _interpMethod == "nodalValuesAndDerivatives") {
    if(fLT->getSizeX() > 1)
      Msg::Error("quadratic interp not implemented with vectorial variable");
    int nbCol = fLT->getSizeF();
    std::set<double>::iterator it = nexus.begin();
    double x0 = *it++;
    for(size_t i = 0; i < nexus.size() - 1; i++) {
      double x1 = *it++;
      std::vector<double> f0 = getPointValues(x0, fLT->getdcdX(), fLT->getdcdF());
      std::vector<double> f1 = getPointValues(x1, fLT->getdcdX(), fLT->getdcdF());
      std::vector<double> p0, p1;
      if(fLT->getdcdDF()) {
        p0 = getPointValues(x0, fLT->getdcdX(), fLT->getdcdDF());
        p1 = getPointValues(x1, fLT->getdcdX(), fLT->getdcdDF());
      } else {
        std::vector<double> f0a = getPointValues(x0 - EPSILON_DERIVATIVE_INTERP * .5, fLT->getdcdX(), fLT->getdcdF());
        std::vector<double> f0b = getPointValues(x0 + EPSILON_DERIVATIVE_INTERP * .5, fLT->getdcdX(), fLT->getdcdF());
        std::vector<double> f1a = getPointValues(x1 - EPSILON_DERIVATIVE_INTERP * .5, fLT->getdcdX(), fLT->getdcdF());
        std::vector<double> f1b = getPointValues(x1 + EPSILON_DERIVATIVE_INTERP * .5, fLT->getdcdX(), fLT->getdcdF());
        p0.resize(nbCol);
        p1.resize(nbCol);
        for(int k = 0; k < nbCol; k++) {
          p0[k] = (f0b[k] - f0a[k]) / EPSILON_DERIVATIVE_INTERP;
          p1[k] = (f1b[k] - f1a[k]) / EPSILON_DERIVATIVE_INTERP;
        }
      }                      
      for(int k = 0; k < nbCol; k++) {
        double dx = x1 - x0, dy = f1[k] - f0[k];
        double dx2 = dx*dx, x02 = x0*x0;
        double tmp  = (3. * dy - dx * (2. * p0[k] + p1[k]) ) / dx2;
        double tmp2 = (dx * (p0[k] + p1[k]) - 2. * dy) / dx2 / dx;
        data[i * f0.size() * 4 + k * 4    ] = f0[k] - p0[k] * x0 +      tmp * x02 -      tmp2 * x02*x0;
        data[i * f0.size() * 4 + k * 4 + 1] =         p0[k]      - 2. * tmp * x0  + 3. * tmp2 * x02;
        data[i * f0.size() * 4 + k * 4 + 2] =                           tmp       - 3. * tmp2 * x0;
        data[i * f0.size() * 4 + k * 4 + 3] =                                            tmp2;
      }
      x0 = x1;
    }
  }
  else
    Msg::Fatal("interpolation method unknown. Available: nodalValuesAndDerivatives (default)");
}


