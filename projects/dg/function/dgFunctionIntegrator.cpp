#include "function.h"
#include "dgIntegrationMatrices.h"
#include "dgFunctionIntegrator.h"
#include "dgDofContainer.h"
#include "fullMatrix.h"
#include "dgGroupOfElements.h"
#include "dgMeshJacobian.h"
#ifdef HAVE_MPI
#include "mpi.h"
#else
#include "string.h"
#endif
#include <stdio.h>

dgFunctionIntegrator::dgFunctionIntegrator(const dgGroupCollection *groups, const function *f, dgDofContainer *sol):_f(f), _groups(groups), _sol(sol){}

void dgFunctionIntegrator::setSolution(dgDofContainer *sol){
  _sol = sol;
}

double dgFunctionIntegrator::compute(std::string tag, int integrationOrder)
{
  fullMatrix<double> result(1, 1);
  compute(result, tag, integrationOrder);
  return result(0, 0);
}

void dgFunctionIntegrator::compute(fullMatrix<double> &result, std::string tag, int integrationOrder)
{
  dataCacheMap cacheMap(dataCacheMap::INTEGRATION_GROUP_MODE, _groups, 0, integrationOrder);
  if(_sol){
    cacheMap.setSolutionFunction(_sol->getFunction(), _sol->getFunctionGradient());
  }
  dataCacheDouble &F = *cacheMap.get(_f);
  int nbRowResult = result.size1();
  std::vector<double> _localResult(nbRowResult, 0.);

  for(int iGroup = 0; iGroup < _groups->getNbElementGroups(); iGroup++) {
    dgGroupOfElements &group = *_groups->getElementGroup(iGroup);
    if (tag.empty() || group.getPhysicalTag() == tag ) { // if we want all the domain => tag=""
      cacheMap.setGroup(&group);
      const fullMatrix<double> &FMatrix = F.get();
      const dgIntegrationMatrices &intMatrices = cacheMap.getIntegrationMatrices();
      int nbQP = intMatrices.integrationPoints().size1();
      const dgMeshJacobian &jacobian = cacheMap.getJacobians();
      const fullMatrix<double> &detJElement = jacobian.detJElement(iGroup);
      for (size_t iElement = 0 ; iElement < group.getNbElements(); ++iElement) {
        for (int iPt = 0; iPt < nbQP; iPt++) {
          const double detJ = detJElement(iPt, iElement);
          for (int k = 0; k < nbRowResult; k++) {
	    //	    printf("detJ = %12.5E %12.5E %12.5E\n",detJ, FMatrix(iPt, k), intMatrices.integrationWeights()(iPt) );
            _localResult[k] += FMatrix(iElement * nbQP + iPt, k) * detJ * intMatrices.integrationWeights()(iPt);
          }
        }
      }
    }
  }

#ifdef HAVE_MPI
  std::vector<double> _result(nbRowResult);
  MPI_Allreduce(&_localResult[0], &_result[0], nbRowResult, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  for (int k = 0; k < nbRowResult; k++)
    result(k, 0) = _result[k];
#else
  for (int k = 0; k < nbRowResult; k++)
    result(k, 0) = _localResult[k];
#endif
}


