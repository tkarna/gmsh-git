#include "function.h"
#include "SPoint3.h"
#include "GModel.h"
#include "dgIntegrationMatrices.h"
#include "nodalBasis.h"
#include "dgGroupOfElements.h"
#include "dgSpaceTransform.h"
#include "Numeric.h"
#include "dgMesh.h"
#include "dgMeshJacobian.h"
#include "dgInterfaceVector.h"
#include "MElement.h"

// function

void function::addFunctionReplace(functionReplace &fr) 
{
  fr._master = this;
  _functionReplaces.push_back(&fr);
}

void function::setArgument(fullMatrix<double> &v, const function *f, int iMap)
{
  if (f == NULL)
    throw;
  arguments.push_back(argument(v, iMap, f));
  dependencies.insert(dependency(iMap, f));
  for (std::set<dependency>::const_iterator it = f->dependencies.begin(); 
       it != f->dependencies.end(); it++) {
    if (it->iMap > 0 && iMap > 0)
      Msg::Error("Consecutive secondary caches");
    dependencies.insert(dependency(iMap + it->iMap, it->f));
  }
/*  for (size_t i = 0; i < _functionReplaces.size(); i++) {
    functionReplace &replace = *_functionReplaces[i];
    for (std::set<dependency>::iterator it = replace._fromParent.begin(); 
         it != replace._fromParent.end(); it++) {
      if (it->iMap > 0 && iMap > 0)
        Msg::Error("Consecutive secondary caches");
      dependencies.insert(dependency(iMap + it->iMap, it->f));
    }
  }*/ //REM: do nothing as replace._fromParent is empty
}

// functionReplace

functionReplace::functionReplace() 
{
  _nChildren = 0;
}

void functionReplace::get(fullMatrix<double> &v, const function *f, int iMap) 
{
  bool allDepFromParent = true; // mean no replacement done on the deps of f
  for (std::set<function::dependency>::const_iterator itDep = f->dependencies.begin();
       itDep != f->dependencies.end(); itDep++){
    bool depFromParent = (_replaced.count(*itDep) == 0);
    for (std::set<function::dependency>::const_iterator itDep2 = itDep->f->dependencies.begin(); 
         itDep2 != itDep->f->dependencies.end() && depFromParent; itDep2++)
      depFromParent &= (_replaced.count(*itDep2) == 0); //why this second level ??
    if(depFromParent)
      _master->dependencies.insert(*itDep);
    else
      allDepFromParent = false;
  }
  function::dependency asDep(iMap, f);
  if (allDepFromParent && _replaced.count(asDep) == 0)
    _master->dependencies.insert(asDep); // mean no replacement done at all
  _toCompute.push_back(function::argument(v, iMap, f));
}

void functionReplace::replace(fullMatrix<double> &v, const function *f, int iMap) 
{
  _replaced.insert(function::dependency(iMap, f));
  _toReplace.push_back(function::argument(v, iMap, f));
}

void functionReplace::addChild() 
{
  _nChildren++;
}

void functionReplace::compute() 
{
  for (unsigned i = 0; i < _toReplace.size(); i++)
    currentCache->toReplace[i]->set(); // validates toReplace caches
  for (unsigned i = 0; i < _toCompute.size(); i++)
    _toCompute[i].val->setAsProxy(currentCache->toCompute[i]->get());
}

void dataCacheMap::addDataCacheDouble(dataCacheDouble *data, bool invalidatedOnElement)
{
  _allDataCaches.insert(data);
  if(invalidatedOnElement)
    _toInvalidateOnElement.push_back(data);
}

// dataCacheDouble 

dataCacheDouble::dataCacheDouble(dataCacheMap *m, function *f):
  _function(f), _cacheMap(*m), _value(m->nPoint(), f->getNbCol(), false)
{
  m->addDataCacheDouble(this, f->isInvalitedOnElement());
  _valid = false;
}

void dataCacheDouble::resize(int nrow) 
{
  _value.resize(nrow, _value.size2(), false);
}

void dataCacheDouble::_eval() 
{
  for(size_t i = 0; i < _directDependencies.size(); i++){
    _function->arguments[i].val->setAsProxy(_directDependencies[i]->get()); // recursive computation of all the sub-sub..dependencies
  }
  for (unsigned i = 0; i < _function->_functionReplaces.size(); i++) {
    _function->_functionReplaces[i]->currentCache = &functionReplaceCaches[i];
    for (unsigned j = 0; j < functionReplaceCaches[i].toReplace.size(); j++){
      _function->_functionReplaces[i]->_toReplace[j].val->setAsProxy
        ((*functionReplaceCaches[i].toReplace[j])._value);
    }
  }
  _function->call(&_cacheMap, this, _value);
  _valid = true;
}

const function * dataCacheMap::_translate(const function *f) const 
{
  //special case
  if (f == function::getSolution() || f == _functionVariable) {
    f = _functionSolution;
    if (f == NULL) {
      dataCacheMap *parent = _parent;
      while (parent) {
        f = parent->_functionSolution;
        if (f) break;
        parent = parent->_parent;
      }
      if (f == NULL) 
        Msg::Error ("solution function has not been set");
    }
  } else if (f == function::getSolutionGradient() || f == _functionVariableGradient) {
    f = _functionSolutionGradient;
    if (f == NULL) {
      dataCacheMap *parent = _parent;
      while (parent) {
        f = parent->_functionSolutionGradient;
        if (f) break;
        parent = parent->_parent;
      }
      if (f == NULL) 
      Msg::Error ("solution function gradient has not been set");
    }
  }
  if (f == function::getCoordinates()) {
    f = _functionCoordinates;
    if (f == NULL) {
      dataCacheMap *parent = _parent;
      while (parent) {
        f = parent->_functionCoordinates;
        if (f) break;
        parent = parent->_parent;
      }
      if (f == NULL)
        Msg::Error ("function coordinates has not been set");
    }
  }
  return f;
}

dataCacheDouble *dataCacheMap::get(const function *f, dataCacheDouble *caller, bool createIfNotPresent)
{
  f = _translate(f);
  // do I have a cache for this function ?
  dataCacheDouble *&r = _cacheDoubleMap[f];
  // can I use the cache of my parent ?
  if(_parent && r == NULL) {
    bool okFromParent = true;
    for (std::set<function::dependency>::const_iterator it = f->dependencies.begin(); 
         it != f->dependencies.end(); it++) {
      if (it->iMap > _parent->_secondaryCaches.size())
        okFromParent = false;
      dataCacheMap *m = getSecondaryCache(it->iMap);
      if (m->_cacheDoubleMap.find(_translate(it->f)) != m->_cacheDoubleMap.end()) {
        okFromParent = false;
        break;
      }
    }
    if (okFromParent)
      r = _parent->get (f, caller);
  }
  // no cache found, create a new one
  if (r == NULL) {
    if (!createIfNotPresent)
      return NULL;
    r = new dataCacheDouble (this, (function*)(f));
    r->_directDependencies.resize (f->arguments.size());
    for (size_t i = 0; i < f->arguments.size(); i++) {
      r->_directDependencies[i] = 
        getSecondaryCache(f->arguments[i].iMap)->get(f->arguments[i].f, r);
    }
    for (unsigned i = 0; i < f->_functionReplaces.size(); i++) {
      functionReplaceCache replaceCache;
      functionReplace *replace = f->_functionReplaces[i];
      dataCacheMap *rMap = newChild();
      for (unsigned i = 0; i < _secondaryCaches.size(); i++)
        rMap->addSecondaryCache (getSecondaryCache(i+1)->newChild());
      for (int i = 0; i < replace->_nChildren; i++)
        rMap->addSecondaryCache (rMap->newChild());
      for (std::vector<function::argument>::iterator it = replace->_toReplace.begin();
          it != replace->_toReplace.end(); it++) {
        dataCacheMap *m = rMap->getSecondaryCache(it->iMap);
        dataCacheDouble *s = new dataCacheDouble(m, (function*) _translate(it->f));
        m->_cacheDoubleMap[_translate(it->f)] = s;
        replaceCache.toReplace.push_back(s);
        if(_functionVariable && (it->f == function::getSolution() || it->f == _functionVariable))
          m->_cacheDoubleMap[_functionVariable] = s;
        if((_functionVariableGradient && it->f == function::getSolutionGradient()) || it->f == _functionVariableGradient)
          m->_cacheDoubleMap[_functionVariableGradient] = s;
      }
      for (std::vector<function::argument>::iterator it = replace->_toCompute.begin();
          it!= replace->_toCompute.end(); it++ ) {
        dataCacheDouble * toComputeCache = rMap->getSecondaryCache(it->iMap)->get(it->f, r);
        replaceCache.toCompute.push_back(toComputeCache);
      }
      replaceCache.map = rMap; // useful ?
      r->functionReplaceCaches.push_back (replaceCache);
    }
  }

  // update the dependency tree
  if (caller) {
    r->_dependOnMe.insert(caller);
    caller->_iDependOn.insert(r);
    for(std::set<dataCacheDouble*>::iterator it = r->_iDependOn.begin();
        it != r->_iDependOn.end(); it++) {
      (*it)->_dependOnMe.insert(caller);
      caller->_iDependOn.insert(*it);
    }
  }
  return r;
}

// dataCacheMap

dataCacheMap::~dataCacheMap()
{
  for (std::set<dataCacheDouble*>::iterator it = _allDataCaches.begin();
      it != _allDataCaches.end(); it++) {
    delete *it;
  }
  for (std::list<dataCacheMap*>::iterator it = _children.begin();
       it != _children.end(); it++) {
    delete *it;
  }
}

void dataCacheMap::setNbEvaluationPoints(int nbEvaluationPoints, int nbEvaluationPointsByElement)
{
  for(std::list<dataCacheMap*>::iterator it = _children.begin(); it != _children.end(); it++) {
    (*it)->setNbEvaluationPoints(nbEvaluationPoints, nbEvaluationPointsByElement);
  }
  for(std::vector<dataCacheMap*>::iterator it = _secondaryCaches.begin(); it != _secondaryCaches.end(); it++) {
    (*it)->setNbEvaluationPoints(nbEvaluationPoints, nbEvaluationPointsByElement);
  }
  for(std::set<dataCacheDouble*>::iterator it = _allDataCaches.begin(); it != _allDataCaches.end(); it++) {
    (*it)->_valid = false;
    (*it)->_groupValue.resize(0, 0, 0, false);
  }
  if (_nbEvaluationPoints != nbEvaluationPoints) {
    for(std::set<dataCacheDouble*>::iterator it = _allDataCaches.begin();
        it != _allDataCaches.end(); it++){
      (*it)->resize(nbEvaluationPoints);
    }
  }
  _nbEvaluationPoints = nbEvaluationPoints;
  _nbEvaluationPointsByElement = nbEvaluationPointsByElement;
}

void dataCacheMap::setGroup(const dgGroupOfElements *group) {
  if (_mode != NODE_MODE && _mode != INTEGRATION_GROUP_MODE && _mode != NODE_GROUP_MODE)
    Msg::Fatal("dataCacheMap::setGroup can only be used in NODE_MODE or INTEGRATION_GROUP_MODE");
  _groupOfElements = group;
  _groupOfInterfaces = NULL;
  _face = NULL;
  const nodalBasis &fs = group->getFunctionSpace();
  int integrationOrder = getIntegrationOrder(fs.order);
  _intMatrices = &dgIntegrationMatrices::get(fs.type, integrationOrder);
  _jacobians = &_groups->_mesh->getJacobian(integrationOrder);
  
  if (_mode == INTEGRATION_GROUP_MODE) {
    setNbEvaluationPoints(_intMatrices->integrationPoints().size1() * group->getNbElements(), _intMatrices->integrationPoints().size1());
    _parametricPoints = _intMatrices->integrationPoints();
    
  }
  else if (_mode == NODE_GROUP_MODE) {
    setNbEvaluationPoints(fs.points.size1() * group->getNbElements(), fs.points.size1());
    _parametricPoints = fs.points;
  }
  else {
    setNbEvaluationPoints(fs.points.size1(), fs.points.size1());
    _parametricPoints = fs.points;
  }
  for(std::list<dataCacheMap*>::iterator it = _children.begin(); it!= _children.end(); it++) {
    (*it)->setGroup(group);
  }
}

void dataCacheMap::setElement(int id) {
  if (_mode == INTEGRATION_GROUP_MODE || _mode == NODE_GROUP_MODE) {
    Msg::Fatal("dataCacheMap::setElement does dot make sense in INTEGRATION_GROUP_MODE and NODE_GROUP_MODE");
  }
  _elementId = id;
  _element = _groupOfElements->getElement(_elementId);
  for(std::vector<dataCacheDouble*>::iterator it = _toInvalidateOnElement.begin(); it != _toInvalidateOnElement.end(); it++) {
    (*it)->_valid = false;
  }
  for(std::list<dataCacheMap*>::iterator it = _children.begin(); it != _children.end(); it++) {
    (*it)->setElement(_elementId);
  }
}

void dataCacheMap::setInterfaceGroup (const dgGroupOfFaces *group) {
  if(_ownedSecondaryCaches.size() + 1 != (size_t)group->nConnection())
    Msg::Fatal("the number of connections does not correspond to the number of secondary caches");
  setInterfaceGroup(group, 0);
  for (size_t i = 0; i < _ownedSecondaryCaches.size(); i++) {
    _ownedSecondaryCaches[i].setInterfaceGroup(group, i + 1);
  }
}

void dataCacheMap::setInterfaceGroup (const dgGroupOfFaces *group, int iConnection) {
  if (_mode != NODE_MODE && _mode != INTEGRATION_GROUP_MODE && _mode != NODE_GROUP_MODE)
    Msg::Fatal("dataCacheMap::setInterfaceGroup can only be used in INTEGRATION_GROUP_MODE, NODE_MODE and NODE_GROUP_MODE.");
  _connectionId = iConnection;
  _groupOfInterfaces = group;
  _groupOfElements = &group->elementGroup(iConnection);
  const nodalBasis *fs = &_groupOfElements->getFunctionSpace();
  
  int integrationOrder = getIntegrationOrder(fs->order);
  _intMatrices = &dgIntegrationMatrices::get(fs->type, integrationOrder, _groups->_mesh->interfaceVector(group->interfaceVectorId()).closureRef(iConnection));
  _jacobians = &_groups->_mesh->getJacobian(integrationOrder);
  for(std::list<dataCacheMap*>::iterator it = _children.begin(); it!= _children.end(); it++) {
    (*it)->setInterfaceGroup(group, iConnection);
  }
  if(_mode == NODE_MODE) {
    _parametricPoints = group->getNodalBasis()->points;
    setNbEvaluationPoints(_parametricPoints.size1(), _parametricPoints.size1());
  }
  else if (_mode == NODE_GROUP_MODE) {
    _parametricPoints = group->getNodalBasis()->points;
    setNbEvaluationPoints(_parametricPoints.size1() * group->size(), _parametricPoints.size1());
  }
  else if (_mode == INTEGRATION_GROUP_MODE) {
    _parametricPoints = dgIntegrationMatrices::get(group->getNodalBasis()->type, integrationOrder).integrationPoints();
    setNbEvaluationPoints(_parametricPoints.size1() * group->size(), _parametricPoints.size1());
  }
  else {
    setNbEvaluationPoints(_parametricPoints.size1(), _parametricPoints.size1());
  }
}

void dataCacheMap::setInterface(int id) 
{
  if (_mode == INTEGRATION_GROUP_MODE || _mode == NODE_GROUP_MODE) {
    Msg::Fatal("dataCacheMap::setInterface does dot make sense in INTEGRATION_GROUP_MODE and NODE_GROUP_MODE");
  }
  _interfaceId = id;
  _elementId = _groupOfInterfaces->elementId(_interfaceId, _connectionId);
  _element = _groupOfElements->getElement(_elementId);
  _face = _groupOfInterfaces->meshInterfaceElement(_interfaceId);
  for(std::vector<dataCacheDouble*>::iterator it = _toInvalidateOnElement.begin(); it!= _toInvalidateOnElement.end(); it++) {
    (*it)->_valid = false;
  }
  for(std::list<dataCacheMap*>::iterator it = _children.begin(); it != _children.end(); it++) {
    (*it)->setInterface(_interfaceId);
  }
  for (size_t i = 0; i < _ownedSecondaryCaches.size(); i++)
    _ownedSecondaryCaches[i].setInterface(id);
}

void dataCacheMap::setMultiPoint(const dgGroupOfElements &group, std::vector<int> &elid, const fullMatrix<double> &xi)
{
  setNbEvaluationPoints(elid.size(), 1);
  for(std::list<dataCacheMap*>::iterator it = _children.begin(); it != _children.end(); it++) {
    (*it)->setMultiPoint(group, elid, xi);
  }
  _groupOfElements = &group;
  _elementIds = elid;
  _face = NULL;
  _parametricPoints = xi;
  int integrationOrder = getIntegrationOrder(group.getFunctionSpace().order);
  _intMatrices = &dgIntegrationMatrices::get(group.getFunctionSpace().type, integrationOrder);
  _jacobians = &_groups->_mesh->getJacobian(integrationOrder);
  for(std::vector<dataCacheDouble*>::iterator it = _toInvalidateOnElement.begin(); it != _toInvalidateOnElement.end(); it++)
    (*it)->_valid=false;
}


MElement *dataCacheMap::setPoint(dgGroupCollection &groups, double x, double y, double z, MElement *guess) {

  SPoint3 xyz(x, y, z), uvw;
  MElement *e = NULL;
  if (guess == NULL)
    guess = _lastElement;
  if (guess) {
    guess->xyz2uvw(xyz, uvw);
    if (guess->isInside(uvw[0], uvw[1], uvw[2]))
      e = guess;
  }
  if (!e) {
    e = groups.getModel()->getMeshElementByCoord(xyz, groups.getDim());
  }
  if (e) {
    int iGroup, iElement;
    groups.find(e, iGroup, iElement);
    if (iElement == -1) {
      return NULL;
    }
    e->xyz2uvw(xyz, uvw);
    setPoint(groups.getElementGroup(iGroup), iElement, uvw[0], uvw[1], uvw[2]);
  }
  _lastElement = e;
  return e;
}

void dataCacheMap::setPointBarycenter(dgGroupOfElements *group, int id)
{
  SPoint3 bc;
  bc = group->getElement(id)->barycenterUVW();
  setPoint(group, id, bc.x(), bc.y(), bc.z());
}

void dataCacheMap::setPoint(dgGroupOfElements *group, int id, double xi0, double xi1, double xi2)
{
  for(std::list<dataCacheMap*>::iterator it = _children.begin(); it != _children.end(); it++) {
    (*it)->setPoint(group, id, xi0, xi1, xi2);
  }
  _groupOfElements = group;
  _elementId = id;
  _element = _groupOfElements->getElement(id);
  _face = NULL;
  _parametricPoints.resize(1, 3);
  _parametricPoints(0, 0) = xi0;
  _parametricPoints(0, 1) = xi1;
  _parametricPoints(0, 2) = xi2;
  int integrationOrder = getIntegrationOrder(group->getFunctionSpace().order);
  _intMatrices = &dgIntegrationMatrices::get(group->getFunctionSpace().type, integrationOrder);
  _jacobians = &_groups->_mesh->getJacobian(integrationOrder);
  for(std::vector<dataCacheDouble*>::iterator it = _toInvalidateOnElement.begin(); it != _toInvalidateOnElement.end(); it++)
    (*it)->_valid=false;
}

int dataCacheMap::_defaultIntegrationOrder;

dataCacheMap::dataCacheMap(mode m,const dgGroupCollection *groups, int nbSecondaryCaches, int integrationOrder){ 
  _functionSolution = _functionSolutionGradient = _functionCoordinates = _functionVariable = _functionVariableGradient = NULL;
  _integrationOrder = integrationOrder;
  _nbEvaluationPoints = 0;
  _parent = NULL;
  _groups = groups;
  _mode = m;
  _groupOfInterfaces = NULL;
  _groupOfElements = NULL;
  _element = _face = NULL;
  _lastFace = _lastElement = NULL;
  _functionCoordinates = groups->getFunctionCoordinates();
  if(nbSecondaryCaches != 0)
    _ownedSecondaryCaches.resize(nbSecondaryCaches, dataCacheMap(m, groups, 0));
  for (int i = 0; i < nbSecondaryCaches; ++i) {
    addSecondaryCache(&_ownedSecondaryCaches[i]);
  }
  _firstCall = true;
  if (m == POINT_MODE)
    setNbEvaluationPoints(1, 1);
}

dataCacheMap *dataCacheMap::newChild() {
  dataCacheMap *m = new dataCacheMap(_mode, _groups);
  m->_parent = this;
  _children.push_back(m);
  m->_nbEvaluationPoints = 0;
  if (_groupOfInterfaces)
    m->setInterfaceGroup(_groupOfInterfaces, _connectionId);
  else if (_groupOfElements)
    m->setGroup(_groupOfElements);
  return m;
} 

int dataCacheMap::getGroupId() const
{
//  return _groupOfElements->getGroupCollection()->getElementGroupId(_groupOfElements);
  return _groups->getElementGroupId(_groupOfElements);
}

int dataCacheMap::getIntegrationOrder() const
{
  return getIntegrationOrder(_groupOfElements->getFunctionSpace().order);
}

MElement *dataCacheMap::element(int iEl) const {
  if (_mode == INTEGRATION_GROUP_MODE || _mode == NODE_GROUP_MODE)
    return _groupOfInterfaces ? &_groupOfInterfaces->element(iEl, _connectionId) : _groupOfElements->getElement(iEl);
  if (_mode == MULTIPOINT_MODE)
    return _groupOfElements->getElement(_elementIds[iEl]);
  return _element;
}

int dataCacheMap::elementId(int iEl) const
{
  if (_mode == INTEGRATION_GROUP_MODE || _mode == NODE_GROUP_MODE) return _groupOfInterfaces ? _groupOfInterfaces->elementId(iEl, _connectionId) : iEl;
  if (_mode == MULTIPOINT_MODE)
    return _elementIds[iEl];
  return _elementId;
}

int dataCacheMap::interfaceId(int iFace) const
{
  if (!_groupOfInterfaces) Msg::Fatal("dataCacheMap::interfaceId(int) can only be called  on faces");
  if (_mode == INTEGRATION_GROUP_MODE) return iFace;
  return _interfaceId;
}

bool dataCacheDouble::doIDependOn(const dataCacheDouble &other) const
{
  return (_iDependOn.find((dataCacheDouble*)&other)!=_iDependOn.end());
}

bool dataCacheDouble::doIDependOn(const function *f, int iMap) const
{
  if (!f) return false;
  dataCacheDouble *other = _cacheMap.getSecondaryCache(iMap)->get(f, NULL, false);
  if (!other) return false;
  return (_iDependOn.find(other)!=_iDependOn.end());
}
