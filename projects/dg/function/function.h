#ifndef _FUNCTION_H_
#define _FUNCTION_H_

#include "fullMatrix.h"
#include <map>
#include <set>
#include <list>
#include <string>
#include <vector>

class dataCacheDouble;
class dataCacheMap;
class function;
class functionConstant;
class functionReplace;
class functionReplaceCache;
class MElement;
class dgGroupOfElements;
class dgGroupCollection;
class dgGroupOfFaces;
class dgDofFunction;
class dgDofFunctionGradient;
class dgIntegrationMatrices;
#include "dgFullMatrix.h"
class dgMeshJacobian;

// An abstract interface to functions
class function {
  friend class functionReplaceCache;
  friend class functionReplace;
  friend class dataCacheDouble;
  friend class dataCacheMap;
  protected:
  // additionnal types
  class dependency {
   public:
    unsigned iMap;
    const function *f;
    dependency(int iMap_, const function *f_) {
       iMap = iMap_;
       f = f_;
    }
    bool operator < (const dependency &d) const {
      return (d.iMap <iMap || d.f < f);
    }
  };
  class argument {
   public:
    int iMap; // id of the dataCacheMap, e.g. on interfaces
    const function *f;
    fullMatrix<double> *val;
    argument(fullMatrix<double> &v, int iMap_, const function *f_) {
      val = &v;
      iMap = iMap_;
      f = f_;
    }
  };

  // data
  int _nbCol;
  bool _invalidatedOnElement;
  std::vector<functionReplace*> _functionReplaces;
  std::vector<argument> arguments;
  std::set<dependency> dependencies;

 private:
  static functionConstant *_timeFunction;
  static functionConstant *_dtFunction;
  static functionConstant *_dtSubFunction;

 public:
  function(int nbCol, bool invalidatedOnElement = true)
    : _nbCol(nbCol), _invalidatedOnElement(invalidatedOnElement) {}
  virtual ~function(){}

  void addFunctionReplace(functionReplace &fr);
  void setArgument(fullMatrix<double> &v, const function *f, int iMap = 0);

  #define setArgumentWrapped(v, f, expectedNbCol, funcName, iMap) { \
    if(f != function::getSolution() && f != function::getSolutionGradient() && f != function::getNormals()) { \
      if(f->getNbCol() != expectedNbCol) { \
        Msg::Fatal("Wrong size for %s (%s in %s:%i : arg. size %d, but %d expected)", \
		   funcName, __func__, __FILE__, __LINE__, f->getNbCol(), expectedNbCol); \
      } \
    } \
    setArgument(v, f, iMap); \
  }
  virtual void call(dataCacheMap *m, dataCacheDouble *d, fullMatrix<double> &res){
    call (m, res);
  };
  virtual void call(dataCacheMap *m, fullMatrix<double> &res){};

  // get or print information
  inline bool isInvalitedOnElement() { return _invalidatedOnElement; }
  inline int getNbCol() const        { if(_nbCol == 0) Msg::Error("Cannot ask nbCol of functionSolution"); return _nbCol; }
  static functionConstant *getTime();
  static functionConstant *getDT();
  static functionConstant *getSubDT();
  static function *getSolution();
  static function *getCoordinates();
  static function *getSolutionGradient();
  static function *getNormals();
  void printDep()
  {
    for (std::set<dependency>::iterator it = dependencies.begin();
         it != dependencies.end(); it++)
      Msg::Info("%i %p", it->iMap, (void*)it->f);
  }
};


// functionConstant (constant values copied over each line)
class functionConstant : public function {
 public:
  fullMatrix<double> _source;
  void call(dataCacheMap *m, fullMatrix<double> &val);
  functionConstant(std::vector<double> source);
  functionConstant(double source);
  void set(double val); 
  const fullMatrix<double> &get() const;
  void set(const std::vector<double> &val);
  void set(const fullMatrix<double> &val);
};


// Additionnal class to get solution
class functionSolution : public function {
  static functionSolution *_instance;
  // constructor is private only 1 instance can exists, call get to
  // access the instance
  functionSolution() : function(0) {}; 
 public:
  void call(dataCacheMap *m, fullMatrix<double> &sol)
  {
    Msg::Error("A function requires the solution but this algorithm does "
               "not provide the solution");
    throw;
  }
  void setNbFields(int nbFields){ _nbCol = nbFields; }
  static functionSolution *get()
  {
    if(!_instance)
      _instance = new functionSolution();
    return _instance;
  }
};

class functionSolutionGradient : public function {
  static functionSolutionGradient *_instance;
  // constructor is private only 1 instance can exists, call get to
  // access the instance
  functionSolutionGradient();
 public:
  void call(dataCacheMap *m, fullMatrix<double> &sol);
  static function *get();
};

class functionReplaceCache {
  public:
  dataCacheMap *map;
  std::vector <dataCacheDouble*> toReplace;
  std::vector <dataCacheDouble*> toCompute;
};

class functionReplace {
  friend class function;
  friend class dataCacheDouble;
  friend class dataCacheMap;
  std::set <function::dependency> _replaced;
//  std::set <function::dependency> _fromParent;
protected:
  std::vector <function::argument> _toReplace;
  std::vector <function::argument> _toCompute;
  int _nChildren;
  function *_master;
  functionReplaceCache *currentCache;
 public:
  functionReplace();
  void get(fullMatrix<double> &v, const function *, int iMap = 0);
  void replace(fullMatrix<double> &v, const function *, int iMap = 0);
  void compute();
  void addChild();
};

// A dataCache when the value is a matrix of double. The user should
// provide the number of rows by evaluating points and the number of
// columns; then the size of the matrix is automatically adjusted
class dataCacheDouble {
  friend class dataCacheMap;
  friend class functionReplace;
  std::vector<dataCacheDouble*> _directDependencies;
  function *_function;
  dataCacheMap &_cacheMap;
  std::vector<functionReplaceCache> functionReplaceCaches;
 protected:
  // pointers to all of the dataCache depending on me
  std::set<dataCacheDouble*> _dependOnMe;
  std::set<dataCacheDouble*> _iDependOn;
  fullMatrix<double> _value;
  bool _valid;
  dataCacheDouble(dataCacheMap *,function *f);
  // do the actual computation and put the result into _value
  void _eval();
  void resize(int nrow);
 public:
  dgFullMatrix<double> _groupValue;
  inline function * getFunction() {return _function;}
  //set the value (without computing it by _eval) and invalidate the
  // dependencies this function is needed to be able to pass the
  // _value to functions like gemm or mult but you cannot keep the
  // reference to the _value, you should always use the set function
  // to modify the _value take care if you use this to set a proxy you
  // must ensure that the value pointed to are not modified without
  // further call to set because the dependencies won't be invalidate
  inline fullMatrix<double> &set()
  {
    if(_valid) {
      for(std::set<dataCacheDouble*>::iterator it = _dependOnMe.begin();
          it!=_dependOnMe.end(); it++)
        (*it)->_valid = false;
    }
    _valid = true;
    return _value;
  }
  // access _value and compute it if necessary
  inline const fullMatrix<double> &get()
  {
    if(!_valid)
      _eval();
    return _value;
  }
  // dataCacheMap is the only one supposed to call this
  inline bool somethingDependOnMe() { return !_dependOnMe.empty(); }
  bool doIDependOn(const dataCacheDouble &other) const;
  bool doIDependOn(const function *f, int iMap = 0) const;
  inline std::vector<dataCacheDouble*> & getDirectDependencies() { return _directDependencies; }
  dataCacheMap &getDataCacheMap() const {return _cacheMap;}
};

class dataCacheMap {
  const function *_functionSolution, *_functionSolutionGradient, *_functionCoordinates, *_functionVariable, *_functionVariableGradient;
  //handle function solution and function solution gradient
  const function * _translate (const function *) const;
  void setNbEvaluationPoints(int nbEvaluationPoints, int nbEvaluationPointsByElement);
  double _xi0, _xi1, _xi2;
 public:
  dataCacheMap  *_parent;
  std::list<dataCacheMap*> _children;
  std::vector<dataCacheMap*> _secondaryCaches;
  int _nbEvaluationPoints, _nbEvaluationPointsByElement;
  std::map<const function*, dataCacheDouble*> _cacheDoubleMap;
  std::set<dataCacheDouble*> _allDataCaches;
  std::vector<dataCacheDouble*> _toInvalidateOnElement;
  MElement *_element, *_face;
  ~dataCacheMap();
  void addDataCacheDouble(dataCacheDouble *data, bool invalidatedOnElement);
  dataCacheMap *getSecondaryCache(int i)
  {
    if (i==0)
      return this;
    return _secondaryCaches[i-1];
  }
  void addSecondaryCache(dataCacheMap *s)
  {
    _secondaryCaches.push_back(s);
  }
  dataCacheDouble *get(const function *f, dataCacheDouble *caller=0, bool createIfNotPresent = true);
  inline void setSolutionFunction(const function *functionSolution, const function *functionSolutionGradient) {
    _functionSolution = functionSolution;
    _functionSolutionGradient = functionSolutionGradient;
    for(std::vector<dataCacheMap*>::iterator it = _secondaryCaches.begin(); it != _secondaryCaches.end(); it++) {
      (*it)->setSolutionFunction(functionSolution, functionSolutionGradient);
    }
  }
  inline void setVariableFunction(const function *functionVariable, const function *functionVariableGradient) {
    _functionVariable = functionVariable;
    _functionVariableGradient = functionVariableGradient;
    for(std::vector<dataCacheMap*>::iterator it = _secondaryCaches.begin(); it != _secondaryCaches.end(); it++) {
      (*it)->setVariableFunction(functionVariable, functionVariableGradient);
    }
    for(std::list<dataCacheMap*>::iterator it = _children.begin(); it != _children.end(); it++) {
      (*it)->setVariableFunction(functionVariable, functionVariableGradient);
    }
  }


  inline int nPoint() const { return _nbEvaluationPoints; }
  inline int nPointByElement() const { return _nbEvaluationPointsByElement;}
  inline int nElement() const {return nPoint() / nPointByElement();}
  int elementId(int iEl) const;
  MElement *element(int iEl) const;
  int interfaceId(int iFace) const;
  inline int connectionId() { return _connectionId;}


  typedef enum {INTEGRATION_GROUP_MODE, NODE_GROUP_MODE, NODE_MODE, POINT_MODE, MULTIPOINT_MODE} mode;
 private:
  static int _defaultIntegrationOrder;
  int _integrationOrder;
  std::vector<dataCacheMap> _ownedSecondaryCaches;
  const dgGroupOfElements *_groupOfElements;
  const dgGroupOfFaces *_groupOfInterfaces;
  int _elementId,_interfaceId,_connectionId;
  std::vector<int> _elementIds;
  //temporary matrices that we do not want to reallocate at each setElement
  const dgMeshJacobian *_jacobians;
  mode _mode;
  MElement *_lastFace, *_lastElement;
  const dgGroupCollection *_groups;
  const dgIntegrationMatrices *_intMatrices;
  fullMatrix<double> _parametricPoints;
 public :
  bool _firstCall;
  static void setDefaultIntegrationOrder(int io) {_defaultIntegrationOrder = io;}
  inline int getIntegrationOrder(int fsOrder) const { return _integrationOrder > 0 ? _integrationOrder
                                                      : (_defaultIntegrationOrder > 0 ? _defaultIntegrationOrder
                                                      : fsOrder * 2 + 1); }
  int getIntegrationOrder() const;
  inline const dgIntegrationMatrices &getIntegrationMatrices() {return *_intMatrices;}
  inline const dgMeshJacobian &getJacobians() const {return *_jacobians;}
  void setInterfaceGroup (const dgGroupOfFaces *group);
  void setInterfaceGroup (const dgGroupOfFaces *group, int iConnections);
  void setGroup(const dgGroupOfElements *group);
  void setElement (int id);
  MElement *setPoint(dgGroupCollection &groups, double x, double y, double z, MElement *guess = NULL);
  void setPoint (dgGroupOfElements *element, int id, double xi0, double xi1, double xi2);
  void setMultiPoint(const dgGroupOfElements &group, std::vector<int> &elid, const fullMatrix<double> &xi);
  void setPointBarycenter (dgGroupOfElements *group, int iE);
  void setInterface (int id);
  dataCacheMap(mode m, const dgGroupCollection * groups, int nbSecondaryCaches = 0, int integrationOrder = -1);
  dataCacheMap *newChild();
  inline mode getMode() {return _mode;}
  inline const fullMatrix<double> &parametricCoordinates() const {return _parametricPoints;}
  int getGroupId() const;
  inline const dgGroupOfFaces *getGroupOfInterfaces() { return _groupOfInterfaces; }
  inline const dgGroupOfElements *getGroupOfElements() const { return _groupOfElements; }
  inline const function * getFunctionSolution() { return _functionSolution; }
  inline const function * getFunctionSolutionGradient() { return _functionSolutionGradient; }
  inline const function * getFunctionVariable() { return _functionVariable; }
  inline const function * getFunctionVariableGradient() { return _functionVariableGradient; }
  inline const dgGroupCollection * getGroupCollection() const { return _groups; }
};
#endif

