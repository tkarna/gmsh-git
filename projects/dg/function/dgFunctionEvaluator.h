#ifndef _DGFUNCTIONEVALUATOR
#define _DGFUNCTIONEVALUATOR
#include <string>
#include "gmshLevelset.h"
class dgDofContainer;
class function;
class MElement;
class dgGroupCollection;
class dataCacheMap;
#include "function.h"
#include "simpleFunction.h"

/**Evaluate a function, using the compute function */
class dgFunctionEvaluator : public simpleFunction<double> {
  dgGroupCollection *_groups;
  const function *_f;
  mutable dataCacheMap _cacheMap;
  dataCacheDouble *_F;
  dgDofContainer *_sol;
  bool _serial;
  bool _haveDefaultValue, _defaultValue;
  class F : public function{
    dgFunctionEvaluator *_evaluator;
    fullMatrix<double> _coordinates, _values;
    public :
    F(dgFunctionEvaluator *eval, function *coordinate);
    void call(dataCacheMap *m, fullMatrix<double> &res);
  };
  public:
  /**a new dgFunctionEvaluator, get the function evaluation using the compute method */
  dgFunctionEvaluator(dgGroupCollection *_groups, const function *f,  dgDofContainer *sol=NULL, bool serial = false);
  ~dgFunctionEvaluator();
  /**compute the function at point x,y,z */
  double operator() (const double x, const double y, const double z) const;
  void compute(double x, double y, double z, fullMatrix<double> &result);
  void computeElem(int iGroup, int ithElementOfGroup, double u, double v, double w , fullMatrix<double> &result);
  void setDefaultValue(double defaultValue);
  inline dgGroupCollection *getGroups() { return _groups; }
  inline const function *getFunction() const {return _f;}
  inline void setGroups(dgGroupCollection * groups) { _groups = groups; }
  function *newFunction(function *coordinate){return new F(this, coordinate);}
};

class gLevelsetEvaluator: public gLevelsetPrimitive
{
  dgFunctionEvaluator *_eval;
public:
  gLevelsetEvaluator(dgFunctionEvaluator  *eval, int tag=1);
  double operator () (const double x, const double y, const double z) const;
  int type() const { return UNKNOWN; }
};

#endif
