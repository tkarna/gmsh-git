#ifndef _DG_FUNCTION_INTEGRATOR_INTERFACE_H_
#define _DG_FUNCTION_INTEGRATOR_INTERFACE_H_

#include <string>
class dgDofContainer;
class function;
class dgGroupCollection;
/**Integrates a function, using the compute function */
template<class t>
class fullMatrix;
class dgFunctionIntegratorInterface {
  const function *_f;
  const dgGroupCollection *_groups;
  dgDofContainer *_sol;
  public:
  /**a new dgFunctionIntegratorInterface, get the solution using the compute method */
  dgFunctionIntegratorInterface(const dgGroupCollection *groups, const function *f, dgDofContainer *sol=NULL);
  /**set the solution (if needed by the function)*/
  void setSolution(dgDofContainer *sol);
  /**compute the integral of the function */
  void compute( std::string tag,  fullMatrix<double> &result);
  void computeCV( std::string tag,  fullMatrix<double> &result);
};
#endif
