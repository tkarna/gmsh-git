#ifndef _FUNCTION_PYTHON1_H_
#define _FUNCTION_PYTHON1_H_
#include "Python.h"

class functionPython1 : public function {
  PyObject *_pycallback;
  std::vector<fullMatrix<double> > args;
 public:
  void call (dataCacheMap *m, fullMatrix<double> &res) 
  {
    for (int i = 0; i < res.size1(); ++i) {
      PyObject *pyArgs = Py_BuildValue("(d)", args[0](i, 0));
      PyObject *result = PyEval_CallObject(_pycallback, pyArgs);
      Py_DECREF(pyArgs);
      if (result) {
        Py_DECREF(result);
      }
      else {
        PyErr_Print();
        Msg::Fatal("An error occurs in the python function.");
      }
    }
  }
  ~functionPython1()
  {
    Py_DECREF(_pycallback);
  }
  functionPython1 (int nbCol, PyObject *callback, std::vector<const function*> dependencies)
    : function(nbCol), _pycallback(callback)
  {
    args.resize(dependencies.size());
    for (int i = 0; i < dependencies.size(); i++) {
      setArgument(args[i], dependencies[i]);
    }
    Py_INCREF(_pycallback);
  }
  functionPython1 (int nbCol, PyObject *callback, std::vector<std::pair<const function*, int> > dependencies)
    : function(nbCol), _pycallback(callback)
  {
    args.resize(dependencies.size());
    for (int i = 0; i < dependencies.size(); i++) {
      setArgument(args[i], dependencies[i].first, dependencies[i].second);
    }
    Py_INCREF(_pycallback);
  }
};
#endif
