
#include "dgFunctionGlobalOp.h"
#include "function.h"
#include "dgIntegrationMatrices.h"
#include "dgDofContainer.h"
#include "fullMatrix.h"
#include "dgGroupOfElements.h"
#include "dgMeshJacobian.h"
#ifdef HAVE_MPI
#include "mpi.h"
#else
#include "string.h"
#endif
#include <stdio.h>


dgFunctionGlobalOp::dgFunctionGlobalOp(dgGroupCollection *groups, const function *f, dgDofContainer *sol) :
    _f(f), _groups(groups), _sol(sol) {}


fullMatrix<double> dgFunctionGlobalOp::computeIntegral(std::string tag) {
  dataCacheMap cacheMap(dataCacheMap::INTEGRATION_GROUP_MODE, _groups);
  if(_sol)
    cacheMap.setSolutionFunction(_sol->getFunction(), _sol->getFunctionGradient());
  dataCacheDouble &F = *cacheMap.get(_f);
  int nbRowResult = _f->getNbCol(); //result.size1();
  std::vector<double> _localResult(nbRowResult, 0.);
  fullMatrix<double> result(nbRowResult, 1);

  for(int iGroup = 0; iGroup < _groups->getNbElementGroups(); iGroup++) {
    dgGroupOfElements &group = *_groups->getElementGroup(iGroup);
    const nodalBasis &fs = group.getFunctionSpace();
    const dgIntegrationMatrices &intMatrices = dgIntegrationMatrices::get(fs.type, fs.order * 2 + 1);
    int nbQP = intMatrices.integrationPoints().size1();
    if (tag.empty() || group.getPhysicalTag() == tag ) { // if we want all the domain => tag=""
      cacheMap.setGroup(&group);
      const fullMatrix<double> &FMatrix = F.get();
      const dgMeshJacobian &jacobian = cacheMap.getJacobians();
      const fullMatrix<double> &detJElement = jacobian.detJElement(iGroup);
      for (size_t iElement = 0 ; iElement < group.getNbElements(); ++iElement) {
        cacheMap.setElement(iElement);
        for (int iPt = 0; iPt < nbQP; iPt++) {
          const double detJ = detJElement(iPt, iElement);
          for (int k = 0; k < nbRowResult; k++) {
            _localResult[k] += FMatrix(iElement * nbQP + iPt, k) * detJ * intMatrices.integrationWeights()(iPt);
          }
        }
      }
    }
  }
#ifdef HAVE_MPI
  std::vector<double> _result(nbRowResult);
  MPI_Allreduce(&_localResult[0], &_result[0], nbRowResult, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  for (int k = 0; k < nbRowResult; k++)
    result(k, 0) = _result[k];
#else
  for (int k = 0; k < nbRowResult; k++)
    result(k, 0) = _localResult[k];
#endif
  return result;
}

fullMatrix<double> dgFunctionGlobalOp::computeMean(std::string tag) {
  dataCacheMap cacheMap(dataCacheMap::INTEGRATION_GROUP_MODE, _groups);
  if(_sol)
    cacheMap.setSolutionFunction(_sol->getFunction(), _sol->getFunctionGradient());
  dataCacheDouble &F = *cacheMap.get(_f);
  int nbRowResult = _f->getNbCol(); //result.size1();
  std::vector<double> _localResult(nbRowResult+1, 0.);
  fullMatrix<double> result(nbRowResult, 1);

  for(int iGroup = 0; iGroup < _groups->getNbElementGroups(); iGroup++) {
    dgGroupOfElements &group = *_groups->getElementGroup(iGroup);
    const nodalBasis &fs = group.getFunctionSpace();
    const dgIntegrationMatrices &intMatrices = dgIntegrationMatrices::get(fs.type, fs.order * 2 + 1);
    int nbQP = intMatrices.integrationPoints().size1();
    if (tag.empty() || group.getPhysicalTag() == tag ) { // if we want all the domain => tag=""
      cacheMap.setGroup(&group);
      const fullMatrix<double> &FMatrix = F.get();
      const dgMeshJacobian &jacobian = cacheMap.getJacobians();
      const fullMatrix<double> &detJElement = jacobian.detJElement(iGroup);
      for (size_t iElement = 0 ; iElement < group.getNbElements(); ++iElement) {
        for (int iPt = 0; iPt < nbQP; iPt++) {
          const double detJ = detJElement(iPt, iElement);
          for (int k = 0; k < nbRowResult; k++) {
            _localResult[k] += FMatrix(iElement * nbQP + iPt, k) * detJ * intMatrices.integrationWeights()(iPt);
          }
          _localResult[nbRowResult] += detJ * intMatrices.integrationWeights()(iPt); //volume
        }
      }
    }
  }
#ifdef HAVE_MPI
  std::vector<double> _result(nbRowResult);
  MPI_Allreduce(&_localResult[0], &_result[0], nbRowResult+1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  for (int k = 0; k < nbRowResult; k++)
    result(k, 0) = _result[k] / _result[nbRowResult];
#else
  for (int k = 0; k < nbRowResult; k++)
    result(k, 0) = _localResult[k];
#endif
  return result;
}

//TODO max for high-order functions (here for L1)
fullMatrix<double> dgFunctionGlobalOp::computeMax(std::string tag) {
  dataCacheMap cacheMap(dataCacheMap::NODE_MODE, _groups);
  if(_sol)
    cacheMap.setSolutionFunction(_sol->getFunction(), _sol->getFunctionGradient());
  dataCacheDouble &F = *cacheMap.get(_f);
  int nbRowResult = _f->getNbCol(); //result.size1();
  std::vector<double> _localResult(nbRowResult, -1e100);
  fullMatrix<double> result(nbRowResult, 1);

  for(int iGroup = 0; iGroup < _groups->getNbElementGroups(); iGroup++) {
    dgGroupOfElements &group = *_groups->getElementGroup(iGroup);
    if (tag.empty() || group.getPhysicalTag() == tag ) { // if we want all the domain => tag=""
      cacheMap.setGroup(&group);
      for (size_t iElement = 0 ; iElement < group.getNbElements(); ++iElement) {
        cacheMap.setElement(iElement);
        const fullMatrix<double> &FMatrix = F.get();
        for (int iPt = 0; iPt < FMatrix.size1(); iPt++) {
          for (int k = 0; k < nbRowResult; k++) {
            _localResult[k] = std::max(FMatrix(iPt, k), _localResult[k]);
          }
        }
      }
    }
  }
#ifdef HAVE_MPI
  std::vector<double> _result(nbRowResult);
  MPI_Allreduce(&_localResult[0], &_result[0], nbRowResult, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
  for (int k = 0; k < nbRowResult; k++)
    result(k, 0) = _result[k];
#else
  for (int k = 0; k < nbRowResult; k++)
    result(k, 0) = _localResult[k];
#endif
  return result;
}

//TODO min for high-order functions (here for L1)
fullMatrix<double> dgFunctionGlobalOp::computeMin(std::string tag) {
  dataCacheMap cacheMap(dataCacheMap::NODE_MODE, _groups);
  if(_sol)
    cacheMap.setSolutionFunction(_sol->getFunction(), _sol->getFunctionGradient());
  dataCacheDouble &F = *cacheMap.get(_f);
  int nbRowResult = _f->getNbCol(); //result.size1();
  std::vector<double> _localResult(nbRowResult, 1e100);
  fullMatrix<double> result(nbRowResult, 1);

  for(int iGroup = 0; iGroup < _groups->getNbElementGroups(); iGroup++) {
    dgGroupOfElements &group = *_groups->getElementGroup(iGroup);
    if (tag.empty() || group.getPhysicalTag() == tag ) { // if we want all the domain => tag=""
      cacheMap.setGroup(&group);
      for (size_t iElement = 0 ; iElement < group.getNbElements(); ++iElement) {
        cacheMap.setElement(iElement);
        const fullMatrix<double> &FMatrix = F.get();
        for (int iPt = 0; iPt < FMatrix.size1(); iPt++) {
          for (int k = 0; k < nbRowResult; k++) {
            _localResult[k] = std::min(FMatrix(iPt, k), _localResult[k]);
          }
        }
      }
    }
  }
#ifdef HAVE_MPI
  std::vector<double> _result(nbRowResult);
  MPI_Allreduce(&_localResult[0], &_result[0], nbRowResult, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
  for (int k = 0; k < nbRowResult; k++)
    result(k, 0) = _result[k];
#else
  for (int k = 0; k < nbRowResult; k++)
    result(k, 0) = _localResult[k];
#endif
  return result;
}

