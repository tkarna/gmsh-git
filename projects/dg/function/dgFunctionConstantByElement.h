#ifndef _DG_FUNCTIONCONSTANTBYELEMENT_H_
#define _DG_FUNCTIONCONSTANTBYELEMENT_H_

#include "function.h"
class dgGroupCollection;

class dgFunctionConstantByElement : public function {
private:
  std::vector< std::vector< double > > _data;
protected:
  dgGroupCollection *_groups;
public:
  dgFunctionConstantByElement(dgGroupCollection* groups);
  void call (dataCacheMap *m, fullMatrix<double> &val);
  void set(int iGroup, int iElem, double value);
  double get(int iGroup, int iElem) { return _data[iGroup][iElem]; };
};

class dgFunctionElementVolume : public dgFunctionConstantByElement {
public:
  dgFunctionElementVolume(dgGroupCollection* groups) : dgFunctionConstantByElement(groups) {};
  void update();
};

class dgFunctionMaximumByElement: public dgFunctionConstantByElement{
  const function *_f;
  public:
   dgFunctionMaximumByElement(dgGroupCollection* groups, const function *f) : dgFunctionConstantByElement(groups), _f(f) {};
   void update();
};

class dgFunctionIpEigenValue: public dgFunctionConstantByElement{
  public:
   dgFunctionIpEigenValue(dgGroupCollection* groups, const function *f, const function *kedd): dgFunctionConstantByElement(groups){};
   void update();
};

class dgFunctionIpErn: public dgFunctionConstantByElement{
  public:
   dgFunctionIpErn(dgGroupCollection* groups, const function *f, const function *kedd): dgFunctionConstantByElement(groups){};
   void update();
};

#endif
