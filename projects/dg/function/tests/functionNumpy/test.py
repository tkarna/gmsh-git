from dgpy import *
import time
import math
import numpy
import gmshPartition

coriolis = {}
wind = {}

XYZ = function.getCoordinates()

#### NUMPY
def coriolisNumpy(cmap, value, xyz):
  value[:] = 1e-4 + 2e-11 * xyz[:,1]

def windNumpy(cmap, value, xyz):
  value[:,0] = numpy.sin(xyz[:,1]/1e6)/1e6
  value[:,1] = 0.

coriolis["NUMPY"] = functionNumpy(1, coriolisNumpy, [XYZ])
wind["NUMPY"] = functionNumpy(2, windNumpy, [XYZ])

#### PYTHON
def coriolisPython(value, xyz):
  for i in range(xyz.size1()):
    value.set(i, 0, 1e-4 + 2e-11 * xyz(i,1))

def windPython(value, xyz):
  for i in range(xyz.size1()):
    value.set(i, 0, math.sin(xyz(i,1)/1e6)/1e6)
    value.set(i, 1, 0.)

coriolis["PYTHON"] = functionPython(1, coriolisPython, [XYZ])
wind["PYTHON"] = functionPython(2, windPython, [XYZ])

### C
CCode = """
#include "fullMatrix.h"
#include "function.h"
extern "C" {
void coriolis (dataCacheMap *m, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (int i = 0; i< sol.size1(); i++) {
    sol.set(i,0,1e-4+2e-11*xyz(i,1));
  }
}
void wind (dataCacheMap *m, fullMatrix<double> &sol, fullMatrix<double> &xyz) {
  for (int i = 0; i< sol.size1(); i++) {
    sol.set(i,0,sin(xyz(i,1)/1e6)/1e6);
    sol.set(i,1,0);
  }
}
}
"""

tmpLib = "tmp.dylib"
if (Msg.GetCommRank() == 0 ) :
  functionC.buildLibrary (CCode, tmpLib);
Msg.Barrier()

coriolis["C"] = functionC(tmpLib,"coriolis",1,[XYZ])
wind["C"] = functionC(tmpLib,"wind",2,[XYZ])
####

model = GModel()
model.load("stommel_square.geo")
model.mesh(2)
model.writeMSH("stommel_square.msh", 3.0)
#model = GModel()
#model.load("stommel_square.msh")

order = 1
dimension = 2
groups = dgGroupCollection(model, dimension, order)

results = {}
print("%10s %10s %20s" % ("Mode", "Time", "Solution norm"))
for mode in ["C", "NUMPY", "PYTHON"] :
  claw = dgConservationLawShallowWater2d()
  solution = dgDofContainer(groups, claw.getNbFields())
  solution.setAll(0.)
  claw.setCoriolisFactor(coriolis[mode])
  claw.setSource(wind[mode])
  fLinearDissipation = functionConstant(1e-6)
  claw.setLinearDissipation(fLinearDissipation)
  fBath = functionConstant(1000)
  claw.setBathymetry(fBath)
  claw.addBoundaryCondition('Wall',  claw.newBoundaryWall())
  rk44 = dgERK(claw, None, DG_ERK_44)
  t=0
  dt = 50.*(3./(2.*order+1)/2)
  tic = time.clock()
  for i in range (100) :
    rk44.iterate(solution, dt, t);
    t = t +dt
  cput = time.clock()-tic
  norm = solution.norm()
  results[mode] = [cput, norm]
  print ("%10s %10.2g %20.16g" %(mode, cput, norm))
  solution.exportMsh(mode)

if results["NUMPY"][0] > results["C"][0] * 1.1 :
  print("error : NUMPY is more than 10% slower than C", results["NUMPY"][0] * 0.95, results["C"][0])
  Msg.Exit(1)
for (cpu, norm) in results.values() :
  if abs(norm - results["C"][1]) > 1e-14 :
    print("error : all norms are not identical")
    Msg.Exit(1)
print("exit with success")
Msg.Exit(0)
