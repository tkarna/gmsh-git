#include "dgFunctionIntegratorInterface.h"
#include "dgDofContainer.h"
#include "fullMatrix.h"
#include "dgGroupOfElements.h"
#include "GmshConfig.h"
#ifdef HAVE_MPI
#include "mpi.h"
#endif
#include "dgMeshJacobian.h"
#include "dgMesh.h"
#include "dgInterfaceVector.h"
#include "dgIntegrationMatrices.h"

dgFunctionIntegratorInterface::dgFunctionIntegratorInterface(const dgGroupCollection *groups, const function *f, dgDofContainer *sol):
  _f(f), _groups(groups), _sol(sol)
{
}

void dgFunctionIntegratorInterface::setSolution(dgDofContainer *sol)
{
  _sol = sol;
}

void dgFunctionIntegratorInterface::compute(std::string tag, fullMatrix<double> &result)
{
  fullMatrix<double> localResult(1, _f->getNbCol());
  result.resize(1, _f->getNbCol());
  for(int iFaceG = 0; iFaceG < _groups->getNbFaceGroups(); iFaceG++){
    dgGroupOfFaces &group = *_groups->getFaceGroup(iFaceG);
    if (group.physicalTag() != tag)
      continue;

    dataCacheMap cache(dataCacheMap::INTEGRATION_GROUP_MODE, _groups, group.nConnection() - 1);
    cache.setInterfaceGroup(&group);
    if (_sol)
      cache.setSolutionFunction(_sol->getFunction(), _sol->getFunctionGradient());
    dataCacheDouble &F = *cache.get(_f);
    cache.setInterfaceGroup(&group);
    const fullMatrix<double> &detJ = cache.getJacobians().detJInterface(group.interfaceVectorId());
    const dgIntegrationMatrices &intMatrices = cache.getIntegrationMatrices();
    for (size_t iFace = 0; iFace < group.size(); ++iFace) {
      const fullMatrix<double> &FMatrix = F.get();
      for (int iPt = 0; iPt < intMatrices.getNbIntegrationPoints(); iPt++) {
        for (int k = 0; k < FMatrix.size2(); ++k){
          localResult(0, k) += FMatrix(iFace *intMatrices.getNbIntegrationPoints() + iPt, k) * detJ(iPt, group.interfaceId(iFace)) * intMatrices.integrationWeights()(iPt);
        }
      }
    }
  }
#ifdef HAVE_MPI	
  MPI_Allreduce(&localResult(0, 0), &result(0, 0), result.size2(), MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
#else
  result = localResult;
#endif
}

void dgFunctionIntegratorInterface::computeCV(std::string tag, fullMatrix<double> &result)
{
  fullMatrix<double> localResult(1, _f->getNbCol());
  result.resize(1, _f->getNbCol());
  for(int iFaceG = 0; iFaceG < _groups->getNbFaceGroups(); iFaceG++) {
    dgGroupOfFaces &group = *_groups->getFaceGroup(iFaceG);
    if (group.physicalTag() != tag )
      continue;
    std::vector<dataCacheMap *> caches(group.nConnection());
    for (int i = 0; i < group.nConnection(); ++i) {
      caches[i] = new dataCacheMap(dataCacheMap::NODE_MODE, _groups);
      if (i != 0)
        caches[0]->addSecondaryCache(caches[i]);
      if(_sol)
        caches[i]->setSolutionFunction(_sol->getFunction(), _sol->getFunctionGradient());
    }
    dataCacheDouble &F = *caches[0]->get(_f);
    for (int i = 0; i < group.nConnection(); ++i)
      caches[i]->setInterfaceGroup(&group, i);
    const fullMatrix<double> &detJ = caches[0]->getJacobians().detJInterface(group.interfaceVectorId());
    const dgIntegrationMatrices &intMatrices = caches[0]->getIntegrationMatrices();
    const fullMatrix<double> &psi = intMatrices.psi();
    const fullVector<double> &w = intMatrices.integrationWeights();
    const int nbQP = intMatrices.getNbIntegrationPoints() ;
    for (size_t iFace=0; iFace < group.size(); ++iFace) {
      for (int i = 0; i < group.nConnection(); ++i)
        caches[i]->setInterface(iFace);
      const fullMatrix<double> &FMatrix = F.get();
      for (int iNode = 0; iNode < FMatrix.size1(); iNode++) {
        for (int k = 0; k < FMatrix.size2(); ++k) {
          double area = 0.;
          for (int iQP = 0; iQP < nbQP; iQP++)
            area += psi(iQP, iNode) * detJ(iQP, group.interfaceId(iFace)) * w(iQP);
          localResult(0, k) += FMatrix(iNode, k) * area;
        }
      }
    }
    for (int i = 0; i < group.nConnection(); ++i) {
      delete caches[i];
    }
  }
#ifdef HAVE_MPI	
  MPI_Allreduce(&localResult(0, 0), &result(0, 0), result.size2(), MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
#else
  result = localResult;
#endif
}
