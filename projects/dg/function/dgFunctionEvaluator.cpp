#include "GModel.h"
#include "function.h"
#include "dgFunctionEvaluator.h"
#include "dgDofContainer.h"
#include "fullMatrix.h"
#include "dgGroupOfElements.h"
#ifdef HAVE_MPI
#include "mpi.h"
#else
#include "string.h"
#endif
#include <stdio.h>
#include <limits>

dgFunctionEvaluator::~dgFunctionEvaluator() 
{
}

dgFunctionEvaluator::dgFunctionEvaluator(dgGroupCollection *groups, const function *f, dgDofContainer *sol, bool serial):_groups(groups), _f(f), _cacheMap(dataCacheMap::POINT_MODE, groups), _sol(sol)
{
  _serial = serial;
  _haveDefaultValue = false;
  if(_sol){
    _cacheMap.setSolutionFunction(_sol->getFunction(), _sol->getFunctionGradient()); 
  }
  _F = _cacheMap.get(_f);
}

void dgFunctionEvaluator::computeElem(int iGroup, int ithElementOfGroup, double u, double v, double w , fullMatrix<double> &result)
{
  dgGroupOfElements &group = *_groups->getElementGroup(iGroup);
  _cacheMap.setPoint (&group, ithElementOfGroup, u, v, w);
  const fullMatrix<double> &FMatrix = _F->get();
  result = FMatrix;
}

double dgFunctionEvaluator::operator() (const double x, const double y, const double z) const {
   fullMatrix<double> res;
   (const_cast<dgFunctionEvaluator*>(this))->compute(x,y,z,res);
   return res(0,0);
}

void dgFunctionEvaluator::compute(double x, double y, double z, fullMatrix<double> &result) {
  result.resize(1, _f->getNbCol(), false);
  result.setAll(-std::numeric_limits<double>::max());
  MElement *el = _cacheMap.setPoint(*_groups, x, y, z);
  if (el && (el->getPartition() - 1 == Msg::GetCommRank() || _serial || Msg::GetCommSize() == 1)) {
    const fullMatrix<double> &FMatrix = _F->get();
    result = FMatrix;
  }
  if (! _serial) {
#ifdef HAVE_MPI	
    for (int k = 0; k < result.size2(); k++) {
      double _loc = result(0, k);
      MPI_Allreduce(&_loc, &result(0, k), 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    }
#endif
  }
  for (int k = 0; k < result.size2(); k++) {
    if (result(0, k) == -std::numeric_limits<double>::max() ) {
      if (_haveDefaultValue)
        result(0, k) = _defaultValue;
      else{
        result(0, k) = 0.0;
        Msg::Warning("FunctionEvaluator: point not found %e %e %e", x, y, z);
      }
    }
  }
}
void dgFunctionEvaluator::setDefaultValue(double defaultValue)
{
  _haveDefaultValue = true;
  _defaultValue = defaultValue;
}

dgFunctionEvaluator::F::F(dgFunctionEvaluator *eval, function *fCoordinate) : function(eval->getFunction()->getNbCol())
{
  _evaluator = eval;
  setArgument(_coordinates, fCoordinate);
  _values.resize(1, eval->getFunction()->getNbCol()); 
}

void dgFunctionEvaluator::F::call(dataCacheMap *m, fullMatrix<double> &res)
{
  for (int i = 0; i < res.size1(); ++i) {
    _evaluator->compute(_coordinates(i,0), _coordinates(i,1), _coordinates(i,2), _values);
    for (int j = 0; j < _values.size2(); ++j) {
      res(i, j) = _values(0, j);
    }
  }
}

gLevelsetEvaluator::gLevelsetEvaluator(dgFunctionEvaluator* eval, int tag) : gLevelsetPrimitive(tag) {
    _eval = eval;
}

double gLevelsetEvaluator::operator() (const double x, const double y, const double z) const {
  fullMatrix<double> result;
  _eval->compute(x,y,z,result);
  return result(0,0);
}

