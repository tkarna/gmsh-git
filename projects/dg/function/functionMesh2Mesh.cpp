#include "dgDofContainer.h"
#include "dgGroupOfElements.h"

// function that enables to interpolate a DG solution using
// geometrical search in a mesh 

class functionMesh2Mesh : public function {
  dgDofContainer *_dofContainer;
public:
  functionMesh2Mesh(dgDofContainer *dofc) : function(dofc->getNbFields()), _dofContainer(dofc){}
  void call( dataCacheMap *m, const fullMatrix<double> &xyz, fullMatrix<double> &val) {
    int nP =xyz.size1();
    val.setAll(0.0);
    double fs[256];
    fullMatrix<double> solEl;
    GModel *model = _dofContainer->getGroups()->getModel();
    for (int i=0;i<val.size1();i++){
      const double x = xyz(i,0);
      const double y = xyz(i,1);
      const double z = xyz(i,2);
      SPoint3 p(x,y,z);
      MElement *e = model->getMeshElementByCoord(p);
      int ig,index;
      _dofContainer->getGroups()->find (e,ig,index);
      dgGroupOfElements *group =  _dofContainer->getGroups()->getElementGroup(ig);      
      double U[3],X[3]={x,y,z};
      e->xyz2uvw (X,U);
      group->getFunctionSpace().f(U[0],U[1],U[2],fs);      
      dgFullMatrix<double> &sol = _dofContainer->getGroupProxy(ig);
      sol.getBlockProxy(index, solEl);
      int fSize = group->getNbNodes();
      for (int k=0;k<val.size2();k++){
        val(i,k) = 0.0; 	
        for (int j=0;j<fSize;j++){
          val(i,k) += solEl(j,k)*fs[j];
        }
      }
    }
  }
};

/*
  cb = b->addClass<functionMesh2Mesh>("functionMesh2Mesh");
  cb->setDescription("A function that can be used to interpolate into a given mesh");
  mb = cb->setConstructor<functionMesh2Mesh,dgDofContainer*>();
  mb->setArgNames("solution",NULL);
  mb->setDescription("A solution.");
  cb->setParentClass<function>();
*/
