#include "functionGeneric.h"
#include "SPoint3.h"
#include "GmshConfig.h"
#include "OS.h"
#include "dgGroupOfElements.h"
#include "dgIntegrationMatrices.h"
#include "dgSpaceTransform.h"
#include <fstream>
#include <cstdlib>
#include "MElement.h"
#include "dgExtrusion.h"
#include "Field.h"
#if defined(HAVE_DLOPEN)
#include <dlfcn.h>
#endif

// Some examples of functions

// functionConstant (constant values copied over each line)
const fullMatrix<double> & functionConstant::get() const
{
  return _source;
}

void functionConstant::set(const std::vector<double> &val){
  if((size_t)getNbCol() != val.size())
    Msg::Error ("set vector on a constant function: mismatched dimensions");
  for (size_t i=0; i<val.size(); i++){
    _source(i, 0) = val[i];
  }
}

void functionConstant::set(const fullMatrix<double> &val){
  if(getNbCol() != val.size2())
    Msg::Error ("set fullMatrix on a constant function: mismatched dimensions, nbCol = %d size1 =%d size2= %d\n",getNbCol(),val.size1(),val.size2());
  for (int i=0; i<val.size2(); i++){
    _source(i, 0) = val(0,i);
  }
}
void functionConstant::set(double val) 
{
  if(getNbCol() != 1)
    Msg::Error ("set scalar value on a vectorial constant function");
  _source(0, 0) = val;
}

void functionConstant::call(dataCacheMap *m, fullMatrix<double> &val)
{
  for (int i = 0; i < val.size1(); i++)
    for (int j = 0; j < _source.size1(); j++)
      val(i, j) = _source(j, 0);
}

functionConstant::functionConstant(std::vector<double> source) : function(source.size(), false)
{
  _source = fullMatrix<double>(source.size(), 1);
  for (size_t i = 0; i < source.size(); i++)
    _source(i, 0) = source[i];
}

functionConstant::functionConstant(double source) : function(1, false)
{
  _source.resize(1, 1);
  _source(0, 0) = source;
}

// functionSum

class functionSum : public function {
 public:
  fullMatrix<double> _f0, _f1;
  void call(dataCacheMap *m, fullMatrix<double> &val) 
  {
    for (int i = 0; i < val.size1(); i++)
      for (int j = 0; j < val.size2(); j++)
        val(i, j)= _f0(i, j) + _f1(i, j);
  }
  functionSum(const function *f0, const function *f1) : function(f0->getNbCol()) 
  {
    if (f0->getNbCol() != f1->getNbCol()) {
      Msg::Error("trying to sum 2 functions of different sizes: %d %d\n",
                 f0->getNbCol(), f1->getNbCol());
      throw;
    }
    setArgument (_f0, f0);
    setArgument (_f1, f1);
  }
};

function *functionSumNew(const function *f0, const function *f1) 
{
  return new functionSum (f0, f1);
}

// functionMinus

class functionMinus : public function {
 public:
  fullMatrix<double> _f0, _f1;
  void call(dataCacheMap *m, fullMatrix<double> &val) 
  {
    if (_f0.size2() != _f1.size2()) {
      Msg::Error("trying to substract 2 functions of different sizes: %d - %d\n",
                 _f0.size2(), _f1.size2());
      throw;
    }
    for (int i = 0; i < val.size1(); i++)
      for (int j = 0; j < val.size2(); j++)
        val(i, j)= _f0(i, j) - _f1(i, j);
  }
  functionMinus(const function *f0, const function *f1) : function(f0->getNbCol()) 
  {
/*    if (f0->getNbCol() != f1->getNbCol()) {
      Msg::Error("trying to substract 2 functions of different sizes: %d - %d\n",
                 f0->getNbCol(), f1->getNbCol());
      throw;
    }*/
    setArgument (_f0, f0);
    setArgument (_f1, f1);
  }
};

function *functionMinusNew(const function *f0, const function *f1) 
{
  return new functionMinus (f0, f1);
}


// functionField

class functionField : public function {
 public:
  Field *_field;
  fullMatrix<double> _coord;
  void call(dataCacheMap *m, fullMatrix<double> &val) 
  {
    for (int i = 0; i < _coord.size1(); i++)
      val(i, 0)= (*_field)(_coord(i, 0), _coord(i, 1), _coord(i, 2));
  }
  functionField(Field &field, const function *coord) : function(1) 
  {
    _field = &field;
    setArgument (_coord, coord);
  }
};

function *functionFieldNew(Field &field, const function *coord) 
{
  return new functionField (field, coord);
}

// functionLevelset

class functionLevelset : public function {
 public:
  fullMatrix<double> _f0;
  double _valMin, _valPlus;
  void call(dataCacheMap *m, fullMatrix<double> &val) 
  {
    for (int i = 0; i < val.size1(); i++)
      for (int j = 0; j < val.size2(); j++){
	val(i, j)= _valPlus;
	if (_f0(i,j) < 0.0)
	  val(i,j) = _valMin;
      }
  }
  functionLevelset(const function *f0, const double valMin, const double valPlus) : function(f0->getNbCol()) 
  {
    setArgument (_f0, f0);
    _valMin  = valMin;
    _valPlus = valPlus;
  }
};

function *functionLevelsetNew(const function *f0, const double valMin, const double valPlus) 
{
  return new functionLevelset (f0, valMin, valPlus);
}

class functionLevelsetSmooth : public function {
 public:
  fullMatrix<double> _f0;
  double _valMin, _valPlus, _E;
  void call(dataCacheMap *m, fullMatrix<double> &val) 
  {
    for (int i = 0; i < val.size1(); i++)
      for (int j = 0; j < val.size2(); j++){
        double phi = _f0(i,j);

        //double Heps = 0.5 * (1 + phi / _E + 1. / M_PI * sin(M_PI * phi / _E));
	//double Heps = 0.5 + 1./32.*(45.*phi/_E - 50.*pow(phi/_E,3.) + 21.*pow(phi/_E,5.)  );
	double Heps = 0.5*(1+tanh(M_PI*phi/_E));
	//double Heps = 0.75 * (phi/_E - 0.33*pow(phi/_E,3.0)) + 0.5;

        //if (fabs(phi) < _E)  val(i, j) = 1./(Heps * ivalPlus + (1 - Heps) * ivalMin);
        //else if (phi >  _E)  val(i, j) = 1./ivalPlus;
        //else if (phi < -_E)  val(i, j) = 1./ivalMin;

        if (fabs(phi) < _E)  val(i, j) = (Heps * _valPlus + (1 - Heps) * _valMin);
        else if (phi >  _E)  val(i, j) = _valPlus;
        else if (phi < -_E)  val(i, j) = _valMin;

      }
  }
  functionLevelsetSmooth(const function *f0, const double valMin, const double valPlus, const double E) : function(f0->getNbCol()) 
  {
    setArgument (_f0, f0);
    _valMin  = valMin;
    _valPlus = valPlus;
    _E = E;
  }
};

function *functionLevelsetSmoothNew(const function *f0, const double valMin, const double valPlus, const double E) 
{
  return new functionLevelsetSmooth (f0, valMin, valPlus, E);
}


// functionProd

class functionProd : public function {
 public:
  fullMatrix<double> _f0, _f1;
  void call(dataCacheMap *m, fullMatrix<double> &val) 
  {
    for (int i = 0; i < val.size1(); i++)
      for (int j = 0; j < val.size2(); j++)
        val(i, j)= _f0(i, j) * _f1(i, j);
  }
  functionProd(const function *f0, const function *f1) : function(f0->getNbCol()) 
  {
    if (f0->getNbCol() != f1->getNbCol()) {
      Msg::Error("trying to compute product of 2 functions of different sizes: %d %d\n",
                 f0->getNbCol(), f1->getNbCol());
      throw;
    }
    setArgument (_f0, f0);
    setArgument (_f1, f1);
  }
};

function *functionProdNew(const function *f0, const function *f1) 
{
  return new functionProd (f0, f1);
}

// functionQuotient

class functionQuotient : public function {
 public:
  fullMatrix<double> _f0, _f1;
  void call(dataCacheMap *m, fullMatrix<double> &val) 
  {
    for (int i = 0; i < val.size1(); i++)
      for (int j = 0; j < val.size2(); j++)
        val(i, j)= _f0(i, j) / _f1(i, j);
  }
  functionQuotient(const function *f0, const function *f1) : function(f0->getNbCol()) 
  {
    if (f0->getNbCol() != f1->getNbCol()) {
      Msg::Error("trying to compute product of 2 functions of different sizes: %d %d\n",
                 f0->getNbCol(), f1->getNbCol());
      throw;
    }
    setArgument (_f0, f0);
    setArgument (_f1, f1);
  }
};

function *functionQuotientNew (const function *f0, const function *f1)
{
  return new functionQuotient (f0, f1);
}


// functionMin

class functionMin : public function {
 public:
  fullMatrix<double> _f0, _f1;
  void call(dataCacheMap *m, fullMatrix<double> &val) 
  {
    for (int i = 0; i < val.size1(); i++)
      for (int j = 0; j < val.size2(); j++)
        val(i, j)= std::min( _f0(i, j) , _f1(i, j) );
  }
  functionMin(const function *f0, const function *f1) : function(f0->getNbCol()) 
  {
    if (f0->getNbCol() != f1->getNbCol()) {
      Msg::Error("trying to compute product of 2 functions of different sizes: %d %d\n",
                 f0->getNbCol(), f1->getNbCol());
      throw;
    }
    setArgument (_f0, f0);
    setArgument (_f1, f1);
  }
};

function *functionMinNew(const function *f0, const function *f1) 
{
  return new functionMin (f0, f1);
}

class functionMinConst : public function {
  double _min;
 public:
  fullMatrix<double> _f0;
  void call(dataCacheMap *m, fullMatrix<double> &val) 
  {
    for (int i = 0; i < val.size1(); i++)
      for (int j = 0; j < val.size2(); j++)
        val(i, j)= std::min( _f0(i, j) , _min );
  }
  functionMinConst(const function *f0, const double s) : function(f0->getNbCol()) 
  {
    setArgument (_f0, f0);
    _min = s;
  }
};

function *functionMinNew(const function *f0, const double s) 
{
  return new functionMinConst (f0, s);
}

// functionMax

class functionMax : public function {
 public:
  fullMatrix<double> _f0, _f1;
  void call(dataCacheMap *m, fullMatrix<double> &val) 
  {
    for (int i = 0; i < val.size1(); i++)
      for (int j = 0; j < val.size2(); j++)
        val(i, j)= std::max( _f0(i, j) , _f1(i, j) );
  }
  functionMax(const function *f0, const function *f1) : function(f0->getNbCol()) 
  {
    if (f0->getNbCol() != f1->getNbCol()) {
      Msg::Error("trying to compute product of 2 functions of different sizes: %d %d\n",
                 f0->getNbCol(), f1->getNbCol());
      throw;
    }
    setArgument (_f0, f0);
    setArgument (_f1, f1);
  }
};

function *functionMaxNew(const function *f0, const function *f1) 
{
  return new functionMax (f0, f1);
}

class functionMaxConst : public function {
  double _max;
 public:
  fullMatrix<double> _f0;
  void call(dataCacheMap *m, fullMatrix<double> &val) 
  {
    for (int i = 0; i < val.size1(); i++)
      for (int j = 0; j < val.size2(); j++)
        val(i, j)= std::max( _f0(i, j) , _max );
  }
  functionMaxConst(const function *f0, const double s) : function(f0->getNbCol()) 
  {
    setArgument (_f0, f0);
    _max = s;
  }
};

function *functionMaxNew(const function *f0, const double s) 
{
  return new functionMaxConst (f0, s);
}

// functionExtractComp

class functionExtractComp : public function {
  public:
  fullMatrix<double> _f0;
  std::vector<int> _iComp;
  void call(dataCacheMap *m, fullMatrix<double> &val) 
  {
    for (int i = 0; i < val.size1(); i++)
      for (size_t j = 0; j < _iComp.size(); j++)
        val(i, j) = _f0(i, _iComp[j]);
  }
  functionExtractComp(const function *f0, const int iComp) : function(1) 
  {
    setArgument (_f0, f0);
    _iComp.push_back(iComp);
  }
  functionExtractComp(const function *f0, std::vector<int> iComp) : function( iComp.size() ) 
  {
    setArgument (_f0, f0);
    for (size_t i = 0; i < iComp.size(); i++)
      _iComp.push_back(iComp[i]);
  }
};

function *functionExtractCompNew(const function *f0, const int iComp) 
{
  return new functionExtractComp (f0, iComp);
}
function *functionExtractCompNew(const function *f0, std::vector<int> iComp) 
{
  return new functionExtractComp (f0, iComp);
}

// functionCatComp

class functionCatComp : public function {
  public:
  int _nComp;
  std::vector<fullMatrix<double> > _fMatrix;
  void call(dataCacheMap *m, fullMatrix<double> &val)
  {
    for (int i = 0; i<val.size1(); i++)
      for (int comp = 0; comp < _nComp; comp++)
        val(i,comp) = _fMatrix[comp](i, 0);
  }
  functionCatComp(std::vector<const function *> fArray) : function(fArray.size())
  {
    _nComp = fArray.size();
    _fMatrix.resize(_nComp);
    for (int i = 0; i < _nComp; i++)
      setArgument (_fMatrix[i], fArray[i]);
  }
};

function *functionCatCompNew(std::vector<const function *> fArray)
{
  return new functionCatComp (fArray);
}

// functionScale

class functionScale : public function {
 public:
  fullMatrix<double> _f0;
  double _s;
  void call(dataCacheMap *m, fullMatrix<double> &val) 
  {
    for(int i = 0; i < val.size1(); i++)
      for(int j = 0; j < val.size2(); j++)
        val(i, j) = _f0(i, j)*_s;
  }
  functionScale(const function *f0, const double s) : function(f0->getNbCol()) 
  {
    setArgument (_f0, f0);
    _s = s;
  }
};

function *functionScaleNew(const function *f0, const double s) {
  return new functionScale (f0, s);
}

// functionAbs

class functionAbs : public function {
 public:
  fullMatrix<double> _f0;
  void call(dataCacheMap *m, fullMatrix<double> &val) 
  {
    for(int i = 0; i < val.size1(); i++)
      for(int j = 0; j < val.size2(); j++)
        val(i, j) = fabs(_f0(i, j));
  }
  functionAbs(const function *f0) : function(f0->getNbCol())
  {
    setArgument (_f0, f0);
  }
};

function *functionAbsNew (const function *f0) {
  return new functionAbs (f0);
}



// functionMean
class functionMeanP1 : public function {
  fullMatrix<double> _f, _df, _xyz;
  public:
  functionMeanP1(const function *f, const function *df) : function(f->getNbCol()) {
    setArgument (_f, f);
    setArgument (_df, df);
    setArgument (_xyz, function::getCoordinates());
  }
  void call(dataCacheMap *m, fullMatrix<double> &val)
  {
    int nP = m->nPointByElement();
    for (int iEl = 0; iEl < m->nElement(); ++iEl) {
      SPoint3 center = m->element(iEl)->barycenter();
      for(int j = 0; j < val.size2(); j++) {
        for(int i = 0; i < val.size1(); i++) {
          val(iEl * nP + i, j) = _f(iEl * nP + i, j)
            + _df(iEl * nP + i, 3 * j + 0) * ( center.x() - _xyz(iEl * nP + i, 0) )
            + _df(iEl * nP + i, 3 * j + 1) * ( center.y() - _xyz(iEl * nP + i, 1) )
            + _df(iEl * nP + i, 3 * j + 2) * ( center.z() - _xyz(iEl * nP + i, 2) );
        }
      }
    }
  }
};

function *functionMeanP1New(const function *f, const function *df) {
  return new functionMeanP1 (f, df);
}

// functionStructuredGridFile
class functionStructuredGridFile : public function {
  fullMatrix<double> coord;
 public:
  int n[3];
  double d[3], o[3];
  double get(int i, int j, int k) 
  {
    return v[(i * n[1] + j) * n[2] + k];
  }
  double *v;
  void call(dataCacheMap *m, fullMatrix<double> &val) 
  {
    for (int pt = 0; pt < val.size1(); pt++) {
      double xi[3];
      int id[3];
      for(int i = 0; i < 3; i++){
        id[i] = (int)((coord(pt, i) - o[i]) / d[i]);
        id[i] = std::max(0, std::min(n[i] - 1, id[i]));
        xi[i] = (coord(pt,i) - o[i] - id[i] * d[i]) / d[i];
        xi[i] = std::min(1., std::max(0., xi[i]));
      }
      val(pt, 0) =
        get(id[0]  , id[1]  , id[2]   ) * (1-xi[0]) * (1-xi[1]) * (1-xi[2]) +
        get(id[0]  , id[1]  , id[2]+1 ) * (1-xi[0]) * (1-xi[1]) * (  xi[2]) +
        get(id[0]  , id[1]+1, id[2]   ) * (1-xi[0]) * (  xi[1]) * (1-xi[2]) +
        get(id[0]  , id[1]+1, id[2]+1 ) * (1-xi[0]) * (  xi[1]) * (  xi[2]) +
        get(id[0]+1, id[1]  , id[2]   ) * (  xi[0]) * (1-xi[1]) * (1-xi[2]) +
        get(id[0]+1, id[1]  , id[2]+1 ) * (  xi[0]) * (1-xi[1]) * (  xi[2]) +
        get(id[0]+1, id[1]+1, id[2]   ) * (  xi[0]) * (  xi[1]) * (1-xi[2]) +
        get(id[0]+1, id[1]+1, id[2]+1 ) * (  xi[0]) * (  xi[1]) * (  xi[2]);
    }
  }
  functionStructuredGridFile(const std::string filename, const function *coordFunction)
    : function(1) 
  {
    setArgument(coord, coordFunction);
    std::ifstream input(filename.c_str());
    if(!input)
      Msg::Error("cannot open file : %s",filename.c_str());
    if(filename.substr(filename.size()-4, 4) != ".bin") {
      input >> o[0] >> o[1] >> o[2] >> d[0] >> d[1] >> d[2] >> n[0] >> n[1] >> n[2];
      int nt = n[0] * n[1] * n[2];
      v = new double [nt];
      for (int i = 0; i < nt; i++)
        input >> v[i];
    } else {
      input.read((char *)o, 3 * sizeof(double));
      input.read((char *)d, 3 * sizeof(double));
      input.read((char *)n, 3 * sizeof(int));
      int nt = n[0] * n[1] * n[2];
      v = new double[nt];
      input.read((char *)v, nt * sizeof(double));
    }
  }
  ~functionStructuredGridFile() 
  {
    delete []v;
  }
};

// functionC
void functionC::buildLibraryFromFile(const std::string cfilename, const std::string libfilename, bool keepTmpFiles) {
  std::string tmpFile = cfilename + "_tmpMake";
  std::string tmpOutputFile = cfilename + "_tmpMakeOut";
  FILE *tmpMake = fopen(tmpFile.c_str(), "w");
  fprintf(tmpMake, 
      "include $(DG_BUILD_DIR)/CMakeFiles/dgshared.dir/flags.make\n"
      "include $(DG_BUILD_DIR)/dgpy/external_libraries.make\n"
      "%s : %s\n"
      "\tg++ $< -fPIC -shared -o $@ $(CXX_FLAGS) -L$(DG_BUILD_DIR) -lDG $(CXX_DEFINES) ${EXTERNAL_LIBRARIES}\n",
      libfilename.c_str(), cfilename.c_str());
  fclose(tmpMake);
  if(system(("make -f " + tmpFile + " > " + tmpOutputFile).c_str())) {
    if (system(("cat " + tmpOutputFile).c_str())) Msg::Fatal("\ncat command failed");
    Msg::Fatal("\nmake command failed");
  }
  if(! keepTmpFiles){
    UnlinkFile(tmpFile.c_str());
    UnlinkFile(tmpOutputFile.c_str());
  }
}

void functionC::buildLibrary(std::string code, std::string filename, bool keepTmpFiles) 
{
  std::string tmpFile = filename + "_tmp.cpp";
  FILE *tmpSrc = fopen(tmpFile.c_str(), "w");
  fprintf(tmpSrc, "%s\n",code.c_str());
  fclose(tmpSrc);
  buildLibraryFromFile(tmpFile, filename, keepTmpFiles);
  if (!keepTmpFiles)
    UnlinkFile(tmpFile);
}
void functionC::call (dataCacheMap *m, fullMatrix<double> &val)
{
  switch (_args.size()) {
    case 0 : 
      ((void (*)(dataCacheMap*, fullMatrix<double> &))(_callback))(m, val);
      break;
    case 1 : 
      ((void (*)(dataCacheMap*, fullMatrix<double> &, const fullMatrix<double>&))
       (_callback)) (m, val, _args[0]);
      break;
    case 2 : 
      ((void (*)(dataCacheMap*, fullMatrix<double> &, const fullMatrix<double>&, 
                 const fullMatrix<double> &))
       (_callback)) (m, val, _args[0], _args[1]);
      break;
    case 3 : 
      ((void (*)(dataCacheMap*, fullMatrix<double> &, const fullMatrix<double>&, 
                 const fullMatrix<double>&, const fullMatrix<double>&))
       (_callback)) (m, val, _args[0], _args[1], _args[2]);
      break;
    case 4 : 
      ((void (*)(dataCacheMap*, fullMatrix<double> &, const fullMatrix<double>&,
                 const fullMatrix<double>&, const fullMatrix<double>&,
                 const fullMatrix<double>&))
       (_callback)) (m, val, _args[0], _args[1], _args[2], _args[3]);
      break;
    case 5 : 
      ((void (*)(dataCacheMap*, fullMatrix<double> &, const fullMatrix<double>&, 
                 const fullMatrix<double>&, const fullMatrix<double>&,
                 const fullMatrix<double>&, const fullMatrix<double>&))
       (_callback)) (m, val, _args[0], _args[1], _args[2], _args[3], _args[4]);
      break;
    case 6 : 
      ((void (*)(dataCacheMap*, fullMatrix<double> &, const fullMatrix<double>&,
                 const fullMatrix<double>&, const fullMatrix<double>&,
                 const fullMatrix<double>&, const fullMatrix<double>&, 
                 const fullMatrix<double>&))
       (_callback)) (m, val, _args[0], _args[1], _args[2], _args[3], _args[4], _args[5]);
      break;
    case 7 : 
      ((void (*)(dataCacheMap*, fullMatrix<double> &, const fullMatrix<double>&,
                 const fullMatrix<double>&, const fullMatrix<double>&,
                 const fullMatrix<double>&, const fullMatrix<double>&, 
                 const fullMatrix<double>&, const fullMatrix<double>&))
       (_callback)) (m, val, _args[0], _args[1], _args[2], _args[3], _args[4], _args[5], _args[6]);
      break;
    case 8 : 
      ((void (*)(dataCacheMap*, fullMatrix<double> &, const fullMatrix<double>&,
                 const fullMatrix<double>&, const fullMatrix<double>&,
                 const fullMatrix<double>&, const fullMatrix<double>&, 
                 const fullMatrix<double>&, const fullMatrix<double>&, 
                 const fullMatrix<double>&))
       (_callback)) (m, val, _args[0], _args[1], _args[2], _args[3], _args[4], _args[5], _args[6], _args[7]);
      break;
    case 9 : 
      ((void (*)(dataCacheMap*, fullMatrix<double> &, const fullMatrix<double>&,
                 const fullMatrix<double>&, const fullMatrix<double>&,
                 const fullMatrix<double>&, const fullMatrix<double>&, 
                 const fullMatrix<double>&, const fullMatrix<double>&, 
                 const fullMatrix<double>&, const fullMatrix<double>&))
       (_callback)) (m, val, _args[0], _args[1], _args[2], _args[3], _args[4], _args[5], _args[6], _args[7], _args[8]);
      break;
    default :
      Msg::Error("C _callback not implemented for %i argurments", _args.size());
  }
}
functionC::functionC (std::string file, std::string symbol, int nbCol, 
    std::vector<const function *> dependencies):
  function(nbCol)
{
#if defined(HAVE_DLOPEN)
  _args.resize(dependencies.size());
  for(size_t i = 0; i < dependencies.size(); i++) {
    setArgument(_args[i], dependencies[i]);
  }
  void *dlHandler = dlopen(file.c_str(), RTLD_NOW);
  if (!dlHandler)
    Msg::Fatal(dlerror());
  _callback = (void(*)(void))(size_t)dlsym(dlHandler, symbol.c_str());
  if(!_callback)
    Msg::Error("Cannot get the callback to the compiled C function: %s in file '%s'", symbol.c_str(), file.c_str());
#else
  Msg::Error("Cannot construct functionC without dlopen");
#endif
  //dlclose(dlHandler);
}

class functionCoordinates : public function {
  static functionCoordinates *_instance;
  functionCoordinates() : function(3) {}; 
 public:
  void call(dataCacheMap *m, fullMatrix<double> &sol)
  {
    Msg::Error("A function requires the coordinates but this algorithm does "
               "not provide the coordinates");
    throw;
  }
  static functionCoordinates *get()
  {
    if(!_instance)
      _instance = new functionCoordinates();
    return _instance;
  }
};
functionCoordinates *functionCoordinates::_instance = NULL;

function *function::getCoordinates() 
{
  return functionCoordinates::get();
}

// function based on levelset value (scalar)
#include "gmshLevelset.h"
class functionLevelsetValue : public function{
  const gLevelset *_levelset;
  fullMatrix<double> xyz;
  public:
  functionLevelsetValue(const gLevelset *levelset): function(1){
     _levelset = levelset;
     setArgument(xyz, function::getCoordinates());
  }
  void call(dataCacheMap *m, fullMatrix<double> &val)
  {
    for (int i = 0; i < val.size1(); i++) {
      val(i, 0) = (*_levelset)(xyz(i, 0), xyz(i, 1), xyz(i, 2));
    }
  }
};

function *functionLevelsetValueNew(const gLevelset *levelset)
{
  return new functionLevelsetValue(levelset);
}

functionConstantByTag::functionConstantByTag(std::map<std::string, std::vector<double> >source) : function(source.begin()->second.size()){
  for (std::map<std::string, std::vector<double> >::iterator it = source.begin(); it != source.end(); it++) {
    fullMatrix<double> &mat = _source[it->first];
    std::vector<double> &vec = it->second;
    mat.resize(vec.size(), 1);
    for (size_t i = 0; i < vec.size(); i ++) {
      mat(i,0) = vec[i];
    }
  }
}
functionConstantByTag::functionConstantByTag(std::map<std::string, double>source) : function(1)
{
  for (std::map<std::string, double>::iterator it = source.begin(); it != source.end(); it++) {
    fullMatrix<double> &mat = _source[it->first];
    mat.resize(1, 1);
    mat(0,0) = it->second;
  }
}
void functionConstantByTag::call(dataCacheMap *m, fullMatrix<double> &val)
{
  fullMatrix<double> &s = _source[m->getGroupOfElements()->getPhysicalTag()];
  for (int i=0; i<val.size1(); i++)
    for (int j=0; j<s.size1(); j++)
      val(i,j)=s(j,0);
}

functionConstant *function::_timeFunction = NULL;
functionConstant *function::_dtFunction = NULL;
functionConstant *function::_dtSubFunction = NULL;

functionConstant *function::getTime()
{
  if (! _timeFunction)
    _timeFunction = new functionConstant(0.);
  return _timeFunction;
}

functionConstant *function::getDT()
{
  if (! _dtFunction)
    _dtFunction = new functionConstant(0.);
  return _dtFunction;
}
functionConstant *function::getSubDT()
{
  if (! _dtSubFunction)
    _dtSubFunction = new functionConstant(0.);
  return _dtSubFunction;
}

functionSolution *functionSolution::_instance = NULL;

function *function::getSolution() 
{
  return functionSolution::get();
}


// Get Solution Gradient + Additionnal class

functionSolutionGradient::functionSolutionGradient():function(0){} 
void functionSolutionGradient::call(dataCacheMap *m, fullMatrix<double> &sol)
{
  Msg::Error("a function requires the gradient of the solution but "
      "this algorithm does not provide the gradient of the solution");
  throw;
}
function *functionSolutionGradient::get()
{
  if(!_instance)
    _instance = new functionSolutionGradient();
  return _instance;
}

functionSolutionGradient *functionSolutionGradient::_instance = NULL;

function *function::getSolutionGradient() 
{
  return functionSolutionGradient::get();
}

// Get Normals + Additionnal class

class functionNormals : public function {
  static functionNormals *_instance;
  // constructor is private only 1 instance can exists, call get to
  // access the instance
  functionNormals() : function(3){} //TODO TOFIX : set to zero, but not display the error (in function.h)
 public:
  void call(dataCacheMap *m, fullMatrix<double> &sol) 
  {
    const dgGroupOfFaces *faces = m->getGroupOfInterfaces();
    if (faces == NULL)
      Msg::Fatal("normals are not available if groupOfFaces in not set");
    fullMatrix<double> normalLocal;
    switch(m->getMode()) {
      case dataCacheMap::INTEGRATION_GROUP_MODE :
        for (size_t id = 0; id < faces->size(); ++id) {
          faces->normal(m->getJacobians(), id, m->connectionId(), normalLocal);
          for (int i = 0; i < normalLocal.size1(); ++i) {
            for (int j = 0; j < 3; ++j) {
              sol(id * normalLocal.size1() + i, j) = normalLocal(i, j);
            }
          }
        }
        break;
      case dataCacheMap::NODE_MODE :
        {
          // this is really sub-obtimal but I think there are verry few use-case
          // compute parametric coordinates on the element
          const nodalBasis &fsE = m->getGroupOfElements()->getFunctionSpace();
          const fullMatrix<double> &points = fsE.points;
          int dimUVW = points.size2();
          for (size_t id = 0; id  < faces->size(); ++id) {
            const std::vector<int> &closure = m->getGroupOfInterfaces()->closure(id, m->connectionId());
            const MElement &element = faces->element(id, m->connectionId());
            int nbNodesInterfaces = closure.size();
            fullMatrix<double> gE;
            fullMatrix<double> xiE(1, 3);
            fullMatrix<double> dVdXi(dimUVW, 1);
            for(int j = 0; j < nbNodesInterfaces; j++) {
              for (int k = 0; k < dimUVW; k++) {
                xiE(0, k) = points(closure[j], k);
              }
              fsE.df(xiE, gE);
              double jac[3][3],ijac[3][3], detJac;
              element.getJacobian(gE, jac);
              if (m->getGroupCollection()->getSpaceTransform()) {
                double uvw[3] = {xiE(0,0), xiE(0,1), xiE(0,2)};
                const MVertex *v = element.getVertex(closure[j]);
                double xyz[3] = {v->x(), v->y(), v->z()};
                m->getGroupCollection()->getSpaceTransform()->applyJacobian(element, xyz, uvw, jac, detJac);
              }
              inv3x3(jac, ijac);
              //normals
              double nuvw[3] = {0., 0., 0.};
              for (size_t i = 0; i < closure.size(); i++){
                nuvw[0] += gE(closure[i], 0);
                nuvw[1] += gE(closure[i], 1);
                nuvw[2] += gE(closure[i], 2);
              }
              double nx = nuvw[0] * ijac[0][0] + nuvw[1] * ijac[0][1] + nuvw[2] * ijac[0][2];
              double ny = nuvw[0] * ijac[1][0] + nuvw[1] * ijac[1][1] + nuvw[2] * ijac[1][2];
              double nz = nuvw[0] * ijac[2][0] + nuvw[1] * ijac[2][1] + nuvw[2] * ijac[2][2];
              double norm = sqrt(nx * nx + ny * ny + nz * nz);
              sol(id * m->nPointByElement() + j, 0) = nx / norm;
              sol(id * m->nPointByElement() + j, 1) = ny / norm;
              sol(id * m->nPointByElement() + j, 2) = nz / norm;
            }
          }
        }
        break;
      case dataCacheMap::POINT_MODE:
        Msg::Fatal("normals are not available in point mode");
        break;
      default :
        Msg::Fatal("unknown dataCacheMap mode");
    }
  }
  static function *get() 
  {
    if(!_instance)
      _instance = new functionNormals();
    return _instance;
  }
};

functionNormals *functionNormals::_instance = NULL;

function *function::getNormals() 
{
  return functionNormals::get();
}

functionPrecomputed::functionPrecomputed (const dgGroupCollection &groups, int integrationOrder, int nbCols):function(nbCols)
{
  _groups = &groups;
  _integrationOrder = integrationOrder;
  _elementData.resize(groups.getNbElementGroups() + groups.getNbGhostGroups());
  _faceData.resize(groups.getNbFaceGroups());
  _elementDataNode.resize(groups.getNbElementGroups() + groups.getNbGhostGroups());
  _faceDataNode.resize(groups.getNbFaceGroups());
}

void functionPrecomputed::clear()
{
  _elementData.clear();
  _faceData.clear();
  _elementDataNode.clear();
  _faceDataNode.clear();
}

void functionPrecomputed::call(dataCacheMap *m, fullMatrix<double> &sol)
{
  if (m->getMode() != dataCacheMap::NODE_MODE && m->getMode() != dataCacheMap::INTEGRATION_GROUP_MODE) {
    Msg::Fatal("functionPrecomputedQP can only be called in integration mode and node mode");
  }
  if (m->getMode() == dataCacheMap::INTEGRATION_GROUP_MODE && m->getIntegrationMatrices().getIntegrationOrder() != _integrationOrder) {
    Msg::Fatal("This functionPrecomputedQP is pre-computed for integration order %i, not %i", _integrationOrder, m->getIntegrationMatrices().getIntegrationOrder());
  }
  const dgGroupOfFaces *faces = m->getGroupOfInterfaces();
  if (faces) {
    std::vector<dgFullMatrix<double> > &face = (m->getMode() == dataCacheMap::INTEGRATION_GROUP_MODE) ? _faceData : _faceDataNode;
    if (face[faces->id()].size1() == 0) {
      Msg::Fatal("This functionPrecomputedQP is not pre-computed for interface group %i", faces->id());
    }
    if (m->getMode() ==dataCacheMap::INTEGRATION_GROUP_MODE)
      sol.setAsProxy(face[faces->id()]);
    else
      face[faces->id()].getBlockProxy(m->interfaceId(0), sol);
  }
  else {
    std::vector<dgFullMatrix<double> > &element = (m->getMode() == dataCacheMap::INTEGRATION_GROUP_MODE) ? _elementData : _elementDataNode;
    if (element[m->getGroupId()].size1() == 0) {
      Msg::Fatal("This functionPrecomputedQP is not pre-computed for element group %i", m->getGroupId());
    }
    if (m->getMode() ==dataCacheMap::INTEGRATION_GROUP_MODE)
      sol.setAsProxy(element[m->getGroupId()]);
    else
      element[m->getGroupId()].getBlockProxy(m->elementId(0), sol);
  }
}

void functionPrecomputed::compute(const function &f, const dgGroupOfElements &group, dataCacheMap::mode m)
{
  dataCacheMap map(m, _groups);
  dataCacheDouble &fcache = *map.get(&f);
  size_t gid = _groups->getElementGroupId(&group);
  std::vector<dgFullMatrix<double> > &element = (m == dataCacheMap::INTEGRATION_GROUP_MODE) ? _elementData : _elementDataNode;
  if (element.size() <= gid)
    element.resize(gid + 1);
  dgFullMatrix<double> &data = element[gid];
  fullMatrix<double> proxy;
  map.setGroup(&group);
  if (m == dataCacheMap::INTEGRATION_GROUP_MODE) {
    data.resize(map.nPoint(), getNbCol(), 1, false);
    data.setAll(fcache.get());
  }
  else {
    data.resize(map.nPoint(), getNbCol(), group.getNbElements(), false);
    for (size_t iE = 0; iE < group.getNbElements(); ++iE) {
      map.setElement(iE);
      data.getBlockProxy(iE, proxy);
      proxy.setAll(fcache.get());
    }
  }
}

void functionPrecomputed::compute(const function &f, const dgGroupOfFaces &group, dataCacheMap::mode m)
{
  dataCacheMap map(m, _groups, group.nConnection() - 1);
  dataCacheDouble &fcache = *map.get(&f);
  std::vector<dgFullMatrix<double> > &face = (m == dataCacheMap::INTEGRATION_GROUP_MODE) ? _faceData : _faceDataNode;
  if (face.size() <= (size_t)group.id())
    face.resize(group.id() + 1);
  dgFullMatrix<double> &data = face[group.id()];
  fullMatrix<double> proxy;
  map.setInterfaceGroup(&group);
  if (m == dataCacheMap::INTEGRATION_GROUP_MODE) {
    data.resize(map.nPoint(), getNbCol(), 1, false);
    data.setAll(fcache.get());
  }
  else {
    data.resize(map.nPoint(), getNbCol(), group.size(), false);
    for (size_t iE = 0; iE < group.size(); ++iE) {
      map.setInterface(iE);
      data.getBlockProxy(iE, proxy);
      proxy.setAll(fcache.get());
    }
  }
}

void functionPrecomputed::compute(const function &f, dataCacheMap::mode m)
{
  _elementData.resize(_groups->getNbElementGroups() + _groups->getNbGhostGroups());
  _faceData.resize(_groups->getNbFaceGroups());
  _elementDataNode.resize(_groups->getNbElementGroups() + _groups->getNbGhostGroups());
  _faceDataNode.resize(_groups->getNbFaceGroups());
  for (int i = 0; i < _groups->getNbElementGroups() + _groups->getNbGhostGroups(); ++i)
    compute(f, *_groups->getElementGroup(i), m);
  for (int i = 0; i < _groups->getNbFaceGroups(); ++i)
    compute(f, *_groups->getFaceGroup(i), m);
}

void functionPrecomputedExtrusion::call(dataCacheMap *m, fullMatrix<double> &sol)
{
  if (m->getGroupCollection() == &_extrusion.getGroups3d())
    _f3d.call(m, sol);
  else if (m->getGroupCollection() == &_extrusion.getGroups2d())
    _f2d.call(m, sol);
  else 
    Msg::Fatal("functionPrecomputedExtrusion called on an unknown groupCollection");
}

functionPrecomputedExtrusion::functionPrecomputedExtrusion(const dgExtrusion &extrusion, int integrationOrder, const function &f):
  function(f.getNbCol()),
  _extrusion(extrusion),
  _f2d(extrusion.getGroups2d(), integrationOrder, f.getNbCol()),
  _f3d(extrusion.getGroups3d(), integrationOrder, f.getNbCol())
{
  compute(f, dataCacheMap::INTEGRATION_GROUP_MODE);
  compute(f, dataCacheMap::NODE_MODE);
}

functionPrecomputedExtrusion::functionPrecomputedExtrusion(const dgExtrusion &extrusion, int integrationOrder, int nbCols):
  function(nbCols),
  _extrusion(extrusion),
  _f2d(extrusion.getGroups2d(), integrationOrder, nbCols),
  _f3d(extrusion.getGroups3d(), integrationOrder, nbCols)
{
}

void functionPrecomputedExtrusion::compute(const function &f, dataCacheMap::mode m)
{
  _f2d.compute(f, m);
  _f3d.compute(f, m);
}

void functionPrecomputedExtrusion::clear(){
  _f2d.clear();
  _f3d.clear();
}
