#ifndef _DGFUNCTIONGLOBALOP
#define _DGFUNCTIONGLOBALOP

//TODO remove dgFunctionIntegrator (replaced by this class)

#include <string>
#include "dgFullMatrix.h"
class dgDofContainer;
class function;
class dgGroupCollection;

class dgFunctionGlobalOp {
  const function *_f;
  dgGroupCollection *_groups;
  dgDofContainer *_sol;
 public:
  //groups = group of the sub-groups, f=function to evaluate, sol=replace getSolution if used in the function
  dgFunctionGlobalOp(dgGroupCollection *groups, const function *f, dgDofContainer *sol=NULL);
 
  //if not defined in the constructor...
  void setSolution(dgDofContainer *sol) { _sol = sol; }

  //where = specific tag (or nothing = global)
  fullMatrix<double> computeIntegral(std::string where="");
  fullMatrix<double> computeMean(std::string where="");
  fullMatrix<double> computeMax(std::string where="");
  fullMatrix<double> computeMin(std::string where="");

};


#endif
