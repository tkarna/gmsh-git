#ifndef _DG_FUNCTION_INTEGRATOR_H_
#define _DG_FUNCTION_INTEGRATOR_H_
#include <string>
class dgDofContainer;
class function;
class dgGroupCollection;
/**Integrates a function, using the compute function */
class dgFunctionIntegrator {
  const function *_f;
  const dgGroupCollection *_groups;
   dgDofContainer *_sol;
  public:
  /**a new dgFunctionIntegrator, get the solution using the compute method */
  dgFunctionIntegrator(const dgGroupCollection *groups, const function *f, dgDofContainer *sol=NULL);
  /**set the solution (if needed by the function)*/
  void setSolution(dgDofContainer *sol);
  /**compute the integral of the function */
  void compute(fullMatrix<double> &result, std::string tag="", int integrationOrder = -1);
  double compute(std::string tag="", int integrationOrder = -1);
};

#endif
