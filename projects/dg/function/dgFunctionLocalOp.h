#ifndef _DGFUNCTIONLOCALOP
#define _DGFUNCTIONLOCALOP

// operations depending on the elements

class function;


function * dgFunctionEMeanNew(const function *f);
function * dgFunctionEAreaNew();
function * dgFunctionEInterpolationNew(const function *f);
function * dgFunctionEL2ProjectionNew(const function *f);

#endif

