#include "dgFunctionLocalOp.h"
#include "function.h"
#include "dgGroupOfElements.h"
#include "dgIntegrationMatrices.h"
#include "dgMeshJacobian.h"

class dgFunctionMean : public function {
  fullMatrix<double> _f, _xyz;
 public:
  dgFunctionMean(const function *f) : function(f->getNbCol()) {
    setArgument (_f, f);
//    setArgument (_xyz, function::getCoordinates());
  }
  void call(dataCacheMap *m, fullMatrix<double> &val)
  {
    const dgGroupOfElements &eGroup = *m->getGroupOfElements();
    const nodalBasis &fs = eGroup.getFunctionSpace();
    const dgIntegrationMatrices &intMatrices = dgIntegrationMatrices::get(fs.type, fs.order * 2 + 1);
    if (m->getMode() == m->NODE_MODE || m->getMode() == m->POINT_MODE) {
      for(int j = 0; j < val.size2(); j++) {
        double AVG = 0.;
        for(int i = 0; i < _f.size1(); i++)
          AVG += _f(i,j);
        AVG /= (double) _f.size1();
        for(int i = 0; i < val.size1(); i++)
          val(i, j) = AVG;
      }
    }
    else {
      int nP = m->nPointByElement();
      for(int j = 0; j < val.size2(); j++) {
        for (int iE = 0; iE < m->nElement(); ++iE) {
          double AVG = 0., surf = 0.;
          for(int iPt = 0; iPt < nP; iPt++) {
            const double detJ = m->getJacobians().detJElement(m->getGroupId())(iPt, m->elementId(iE));
            const double w = intMatrices.integrationWeights()(iPt);
            AVG  += _f(iE * nP + iPt, j) * detJ * w;
            surf += detJ * w;
          }
          AVG /= surf;
          for(int i = 0; i < m->nPointByElement(); i++)
            val(iE * nP + i, j) = AVG;
        }
      }
    }
  }
};
function * dgFunctionEMeanNew(const function *f) {
  return new dgFunctionMean (f);
}


function * dgFunctionEAreaNew() {
  Msg::Fatal("dgFunctionEArea does not exist anymore, use the values stored on the mesh jacobian instead");
  return NULL;
}



#if 0
class dgFunctionEInterpolation : public function {
  const function * _fct;
  dataCacheMap * _cacheMap;
  dataCacheDouble * _cacheDouble;
 public:
  dgFunctionEInterpolation(const function *f) :function(f->getNbCol()),  _fct(f) { 
    _cacheMap = NULL;
    _cacheDouble = NULL;
  }

  void call(dataCacheMap *m, fullMatrix<double> &val) { //TODO optimize (how?)

    //bool ismod = false;
    if(!_cacheMap || _cacheMap->getGroupCollection() != m->getGroupCollection()) {
      if(_cacheMap) delete _cacheMap;
      _cacheMap = new dataCacheMap(dataCacheMap::NODE_MODE, m->getGroupCollection());
      //ismod = true;
    }
    if(_cacheMap->getFunctionSolution() != m->getFunctionSolution()) {
      _cacheMap->setSolutionFunction(m->getFunctionSolution(), m->getFunctionSolutionGradient());
      //ismod = true;
    }
    if(_cacheMap->getFunctionVariable() != m->getFunctionVariable()) {
      _cacheMap->setVariableFunction(m->getFunctionVariable(), m->getFunctionVariableGradient());
      //ismod = true;
    }

    //if(!_cacheDouble || ismod)
    dataCacheDouble * cacheDouble = _cacheMap->get(_fct);

    int type;
    if(m->getGroupOfInterfaces() == NULL) {
      if(_cacheMap->getGroupOfElements() != m->getGroupOfElements())
        _cacheMap->setGroup(m->getGroupOfElements());
      _cacheMap->setElement(m->getElementId());
      type = m->getGroupOfElements()->getFunctionSpace().type;
    }
    else {
      if(_cacheMap->getGroupOfInterfaces() != m->getGroupOfInterfaces() || _cacheMap->getConnectionId() != m->getConnectionId())
        _cacheMap->setInterfaceGroup(m->getGroupOfInterfaces(), m->getConnectionId());
      _cacheMap->setInterface(m->getInterfaceId());
      type = m->getGroupOfInterfaces()->getNodalBasis()->type;
    }
    const fullMatrix<double> &valuesMatrix = cacheDouble->get();
    const dgIntegrationMatrices &intMat = dgIntegrationMatrices::get(type, m->getIntegrationMatrices().getIntegrationOrder());
    //printf("valMat : %d/%d  , val : %d/%d , psi : %d/%d\n", valuesMatrix.size1(), valuesMatrix.size2(), val.size1(), val.size2(), intMat.psi().size1(), intMat.psi().size2());
    intMat.psi().mult(valuesMatrix, val);
/*    for(int k = 0; k < val.size2(); k++)
      for(int i = 0; i < val.size1(); i++)
         val(i, k) = valuesMatrix(i, k);*/



    //one cache map witch is deleted (listed?) if change of gc 
/*    dataCacheMap cacheMap(dataCacheMap::NODE_MODE, m->getGroupCollection());
    cacheMap.setSolutionFunction(m->getFunctionSolution(), m->getFunctionSolutionGradient());
    cacheMap.setVariableFunction(m->getFunctionVariable(), m->getFunctionVariableGradient());
    dataCacheDouble * values = cacheMap.get(_fct);
    if(m->getGroupOfInterfaces() == NULL) {
      cacheMap.setGroup(m->getGroupOfElements());
      cacheMap.setElement(m->getElementId());
    }
    else {
      cacheMap.setInterfaceGroup(m->getGroupOfInterfaces(), m->getConnectionId());
      cacheMap.setInterface(m->getInterfaceId());
    }
    const fullMatrix<double> &valuesMatrix = values->get();
    for(int k = 0; k < val.size2(); k++)
      for(int i = 0; i < val.size1(); i++)
         val(i, k) = valuesMatrix(i, k);

*/
  }
};
#endif

function * dgFunctionEInterpolationNew(const function *f) {
  Msg::Fatal("sorry, I've removed dgFunctionEInterpolationNew for now because I can't understand it's meaning. In my opinion, NODE_MODE and GROUP_INTEGRATION_MODE cannot be mixed.");
  return NULL;
  //return new dgFunctionEInterpolation (f);
}


class dgFunctionEL2Projection : public function {
  fullMatrix<double> _f;
 public:
  dgFunctionEL2Projection(const function *f) : function(f->getNbCol()) {
    setArgument(_f, f);
  }
  void call(dataCacheMap *m, fullMatrix<double> &val) {
    Msg::Fatal("dgFunctionEL2Projection to implement ! (sorry :p)");
  }
};
function * dgFunctionEL2ProjectionNew(const function *f) {
  return new dgFunctionEL2Projection (f);
}

