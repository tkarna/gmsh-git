%module dgFunction

%include std_vector.i
%include std_pair.i
%rename (_print) print;
%import(module="dgpy.dgCommon")"dgCommon.i";

%{
  #undef HAVE_DLOPEN
  #include "dgConfig.h"
  #include "GmshConfig.h"
  #include "function.h"
  #include "functionGeneric.h"
  #include "functionDerivator.h"
  #include "functionLookupTable.h"
  #include "functionPython.h"
  //#include "functionPython1.h"
  #include "functionNumpy.h"

  #include "dgFunctionIntegrator.h"
  #include "dgFunctionIntegratorInterface.h"
  #include "dgFunctionGlobalOp.h"
  #include "dgFunctionLocalOp.h"
  #include "dgFunctionEvaluator.h"
  #include "dgFunctionConstantByElement.h"

  #include "simpleFunction.h"
  #include "gmshLevelset.h"
%}

namespace std {
%template(VectorFunctionConst) vector<const function*, std::allocator<const function*> >;
}

%include "simpleFunction.h"
%template(simpleFunctionDouble) simpleFunction<double>;
%template(fullMatrixDouble) fullMatrix<double>;
%include "gmshLevelset.h"

%rename(_in) in;

%include "function.h"
%include "functionGeneric.h"
%include "functionDerivator.h"
%include "functionLookupTable.h"
%include "functionPython.h"
//%include "functionPython1.h"
%include "functionNumpy.h"

%include "dgFunctionIntegrator.h"
%include "dgFunctionIntegratorInterface.h"
%include "dgFunctionGlobalOp.h"
%include "dgFunctionLocalOp.h"
%include "dgFunctionEvaluator.h"
%include "dgFunctionConstantByElement.h"

