#ifndef _FUNCTION_NUMPY_H_
#define _FUNCTION_NUMPY_H_
#include "GmshConfig.h"
#ifdef HAVE_NUMPY
#include "Python.h"
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include "numpy/arrayobject.h"
#if NPY_API_VERSION < 0x00000007
#define NPY_ARRAY_FARRAY NPY_FARRAY
#endif

class functionNumpy : public function {
  PyObject *_pycallback;
  std::vector<fullMatrix<double> > args;
  static PyObject *M2A(fullMatrix<double> &m) {
    npy_intp n[2] = {m.size1(), m.size2()};
    return PyArray_New(&PyArray_Type, m.size2() > 1 ? 2 : 1, n, NPY_DOUBLE, NULL, &m(0, 0), 0, NPY_ARRAY_FARRAY, NULL);
  }
  static void _checkImported() {
    static bool _arrayImported = false;
    if (! _arrayImported){
      _import_array();
      _arrayImported = true;
    }
  }
public:
  void call (dataCacheMap *m, fullMatrix<double> &res) 
  {
    PyObject *pyargs;
    PyObject *cmpy = SWIG_NewPointerObj((void*) m, SWIGTYPE_p_dataCacheMap, 0);
    switch(args.size()) {
      case 0 : pyargs = Py_BuildValue("(NN)", cmpy, M2A(res)); break;
      case 1 : pyargs = Py_BuildValue("(NNN)", cmpy, M2A(res), M2A(args[0])); break;
      case 2 : pyargs = Py_BuildValue("(NNNN)", cmpy, M2A(res), M2A(args[0]), M2A(args[1])); break;
      case 3 : pyargs = Py_BuildValue("(NNNNN)", cmpy, M2A(res), M2A(args[0]), M2A(args[1]), M2A(args[2])); break;
      case 4 : pyargs = Py_BuildValue("(NNNNNN)", cmpy, M2A(res), M2A(args[0]), M2A(args[1]), M2A(args[2]), M2A(args[3])); break;
      case 5 : pyargs = Py_BuildValue("(NNNNNNN)", cmpy, M2A(res), M2A(args[0]), M2A(args[1]), M2A(args[2]), M2A(args[3]), M2A(args[4])); break;
      case 6 : pyargs = Py_BuildValue("(NNNNNNNN)", cmpy, M2A(res), M2A(args[0]), M2A(args[1]), M2A(args[2]), M2A(args[3]), M2A(args[4]), M2A(args[5])); break;
      case 7 : pyargs = Py_BuildValue("(NNNNNNNNN)", cmpy, M2A(res), M2A(args[0]), M2A(args[1]), M2A(args[2]), M2A(args[3]), M2A(args[4]), M2A(args[5]), M2A(args[6])); break;
      case 8 : pyargs = Py_BuildValue("(NNNNNNNNNN)", cmpy, M2A(res), M2A(args[0]), M2A(args[1]), M2A(args[2]), M2A(args[3]), M2A(args[4]), M2A(args[5]), M2A(args[6]), M2A(args[7])); break;
      default:Msg::Error("python function not implemented for more than 8 arguments");
    }
    PyObject *result = PyEval_CallObject(_pycallback, pyargs);
    if (result) {
      Py_DECREF(result);
    }
    else {
      PyErr_Print();
      Msg::Fatal("An error occurs in the python function.");
    }
    Py_DECREF(pyargs);
  }
  functionNumpy (int nbCol, PyObject *callback, std::vector<const function*> dependencies)
    : function(nbCol), _pycallback(callback)
  {
    args.resize(dependencies.size());
    for (int i = 0; i < dependencies.size(); i++) {
      setArgument(args[i], dependencies[i]);
    }
    _checkImported();
  }
  functionNumpy (int nbCol, PyObject *callback, std::vector<std::pair<const function*, int> > dependencies)
    : function(nbCol), _pycallback(callback)
  {
    args.resize(dependencies.size());
    for (int i = 0; i < dependencies.size(); i++) {
      setArgument(args[i], dependencies[i].first, dependencies[i].second);
    }
    _checkImported();
  }
};
#endif
#endif
