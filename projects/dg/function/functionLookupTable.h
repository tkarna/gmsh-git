#ifndef _FUNCTION_LOOKUP_TABLE_H_
#define _FUNCTION_LOOKUP_TABLE_H_

#include "function.h"


/**
 * HELP: functionInterpolator creates parameter sets for arbritary polynomial sets between some nexus.
 *                            e.g.   *----*--*  =>  3 nexus, 2 polynoms => 2 parameter sets
 *                            it return, for a given subset of param, the value for x in the polynom
 *                            e.g.   *-x--*     => first parameter set, position x
 *       functionLT (Lookup Table) maps the subpolynoms, contains the parameter sets and calls the good one.
 */

class functionInterpolator;

/**
 * Lookup table abstract class
 */
class functionLT : public function {
 public:
 protected:
  functionInterpolator *_ip;
  bool _selfIP;
  fullMatrix<double> _variable, _coordinates;
  double *_data;
  const function *_x, *_f, *_df;
  int _nbSubPoly, _sizeF, _sizeX;
  dataCacheMap _dcm;
  dataCacheDouble *_dcdF, *_dcdX, *_dcdDF;
  inline void setX(fullMatrix<double> var) { _variable.setAsProxy(var); } //shortcut for functions !
  functionLT(const functionLT *fLT, const function *x, dgGroupOfElements *group);
  functionLT(const functionLT&);
  void ini(dgGroupOfElements *group);
  virtual void relinkData() {}
 public:
  functionLT(const function *f, const function *x, const dgGroupCollection * gc, functionInterpolator * ip, int nbSubPoly,
             const function *df=NULL, dgGroupOfElements *group=NULL);
  ~functionLT();
  inline dataCacheDouble * getdcdX()  { return _dcdX;  }
  inline dataCacheDouble * getdcdF()  { return _dcdF;  }
  inline dataCacheDouble * getdcdDF() { return _dcdDF; }
  inline int getSizeF() { return _sizeF; }
  inline int getSizeX() { return _sizeX; }
  inline int getNbSubPoly() { return _nbSubPoly; }
  virtual functionLT * newDerivative() = 0;
  void derivate();
  inline void call2(dataCacheMap *m, fullMatrix<double> &out, fullMatrix<double> &in) { _variable.setAsProxy(in); call(m, out); }
  virtual void call(dataCacheMap *m, fullMatrix<double> &val) = 0;
};

/**
 * Lookup table, constant step size.
 *
 *  rem: if multi-dimentional, begin/end forms a cubic domain (to be improved if needed)
 */
class functionLTClassic : public functionLT {
  double _begin, _end, _1ostep;
 public:
  functionLTClassic(const function *f, const function *x, const dgGroupCollection * gc, functionInterpolator *ip, 
                    double begin, double end, int steps, const function *df=NULL); 
  void call(dataCacheMap *m, fullMatrix<double> &val);
  functionLT * newDerivative() { functionLTClassic * f = new functionLTClassic(*this); f->derivate(); return f; }
};
// optimized method, without checks on out-of-bound values
class functionLTClassicOne : public functionLT {
  double _begin, _end, _1ostep;
 public:
  functionLTClassicOne(const function *f, const function *x, const dgGroupCollection * gc, functionInterpolator *ip, 
                       double begin, double end, int steps, const function *df=NULL);
  void call(dataCacheMap *m, fullMatrix<double> &val);
  functionLT * newDerivative() { functionLTClassicOne * f = new functionLTClassicOne(*this); f->derivate(); return f; }
};
// very optimized method, with all included
class functionLTClassicOneCubic : public functionLT {
  double _begin, _end, _1ostep;
 public:
  functionLTClassicOneCubic(const function *f, const function *x, const dgGroupCollection * gc, double begin, double end, 
                            int steps, const function *df=NULL);
  ~functionLTClassicOneCubic();
  void call(dataCacheMap *m, fullMatrix<double> &val);
  functionLT * newDerivative() { Msg::Fatal("one cubic not derivable"); return NULL;}
};
// for x as vector
/*class functionLTClassicMulti : public functionLT {
 public:
  functionLTClassicMulti(const function *f, const function *x, const dgGroupCollection * gc, functionInterpolator *ip, 
                         double begin, double end, int steps, const function *df=NULL) {}
  void call(dataCacheMap *m, fullMatrix<double> &val) {}
};*/


/**
 * Lookup table, variable step size, with tree structure containing each subinterpolation
 */
class functionLTTree : public functionLT {
  std::map<double, double*> _tree;
  double _begin, _end;
  void iniTree(std::set<double> nexus);
  functionLTTree(const functionLTTree *fLT, const function *x, std::set<double> nexus, dgGroupOfElements *group); //construct for inverse
 public:
  functionLTTree(const function *f, const function *x, const dgGroupCollection * gc, functionInterpolator *ip, std::set<double> &nexus,
                               const function *df=NULL, dgGroupOfElements *group=NULL);
  static functionLTTree * newFunction(const function *f, const function *x, const dgGroupCollection * gc, functionInterpolator *ip, 
                                            std::vector<double> nexus, const function *df=NULL, dgGroupOfElements *group=NULL) {
    std::set<double> nexusS(nexus.begin(), nexus.end());
    return new functionLTTree(f, x, gc, ip, nexusS, df, group);
  }
  functionLTTree * newInverse(const function *x, std::set<double> originalNexus, dgGroupOfElements *group);
  void call(dataCacheMap *m, fullMatrix<double> &val);
  functionLT * newDerivative() { functionLTTree * f = new functionLTTree(*this); f->derivate(); return f; }
  virtual void relinkData();  
};

/**
 * Lookup table, variable step size, following a functional distribution
 */
class functionLTShaped : public functionLT {
 public:
  functionLTShaped() : functionLT(NULL,NULL,NULL,NULL,0) { Msg::Fatal("shaped not implemented!");}
  void call(dataCacheMap *m, fullMatrix<double> &val) {}
  functionLTShaped * newDerivative() { Msg::Fatal("shaped not implemented!"); return NULL;}  
};


//==========//==========//==========//==========//==========//==========//==========//==========
//==========//==========//==========//==========//==========//==========//==========//==========


class functionInterpolator {
 protected:
  int _order; // = order + 1  (in fact, number of parameters)
  std::string _interpMethod;
 public:
  functionInterpolator(int order, std::string interpMethod="") : _order(order), _interpMethod(interpMethod) {}
  virtual void set(functionLT *fLT, std::set<double> &nexus, double *data) = 0;
  virtual std::set<double> setInverse(functionLT *fLT, std::set<double> &nexus, double *data) { Msg::Fatal("setInv not implemented in this interpolator"); return std::set<double>();}
  virtual double get(double &x, double *param) = 0;
  virtual double getD(double &x, double *param) = 0;
  inline const int getOrder() { return _order; }
  inline const std::string getInterpMethod() { return _interpMethod; }
  virtual ~functionInterpolator(){}
};

class functionInterpolatorConstant : public functionInterpolator {
 public:
  functionInterpolatorConstant(std::string interpMethod="") : functionInterpolator(1, interpMethod) {}
  inline double get (double &x, double *param) { return *param; }
  inline double getD(double &x, double *param) { return 0.; }
  void set(functionLT *fLT, std::set<double> &nexus, double *data);
  virtual std::set<double> setInverse(functionLT *fLT, std::set<double> &nexus, double *data) { Msg::Fatal("setInv not possible for const"); return std::set<double>();}
};

class functionInterpolatorLinear : public functionInterpolator {
 public:
  functionInterpolatorLinear(std::string interpMethod="") : functionInterpolator(2, interpMethod) {}
  inline double get (double &x, double *param) { return *param + param[1] * x; }
  inline double getD(double &x, double *param) { return param[1]; }
  void set(functionLT *fLT, std::set<double> &nexus, double *data);
  virtual std::set<double> setInverse(functionLT *fLT, std::set<double> &nexus, double *data);
};

class functionInterpolatorQuadratic : public functionInterpolator {
 public:
  functionInterpolatorQuadratic(std::string interpMethod="") : functionInterpolator(3, interpMethod) {}
  inline double get (double &x, double *param) { return *param + param[1] * x + param[2] * x*x; }
  inline double getD(double &x, double *param) { return param[1] + param[2] * 2. * x; }
  void set(functionLT *fLT, std::set<double> &nexus, double *data);
};

class functionInterpolatorCubic : public functionInterpolator {
 public:
  functionInterpolatorCubic(std::string interpMethod="") : functionInterpolator(4, interpMethod) {}
  inline double get (double &x, double *param) { double x2 = x*x; return *param + param[1] * x + param[2] * x2 + param[3] * x2*x; }
  inline double getD(double &x, double *param) { return param[1] + param[2] * 2. * x + param[3] * x*x; }
  void set(functionLT *fLT, std::set<double> &nexus, double *data);
};

class functionInterpolatorPolynom : public functionInterpolator {
 public:
  functionInterpolatorPolynom(int order, std::string interpMethod="") : functionInterpolator(order+1, interpMethod) {}
  inline double get (double &x, double *param) {
    double sol = param[_order-1];
    for(int i=_order-2; i>=0; i--) sol = param[i] + sol * x;
    return sol;
  }
  inline double getD(double &x, double *param) {
    double dsol = (_order-1) * param[_order-1];
    for(int i=_order-2; i>0; i--) dsol = i * param[i] + dsol * x;
    return dsol;
  }
    // TODO vandermonde/Lagrange or chebychev
  void set(functionLT *fLT, std::set<double> &nexus, double *data) {}
};


#endif
