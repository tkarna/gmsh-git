#include"dgFunctionConstantByElement.h"
#include "dgMeshJacobian.h"
#include "dgGroupOfElements.h"

dgFunctionConstantByElement::dgFunctionConstantByElement(dgGroupCollection* groups) : function(1)
{
  _groups = groups;
  int nbElemGroups = _groups->getNbElementGroups();
  std::vector<double> tempVec;
  _data.resize(nbElemGroups,tempVec);
  for ( int iG = 0; iG < nbElemGroups; iG++ ) {
    _data[iG].resize(groups->getElementGroup(iG)->getNbElements(),0);
  }
}

void dgFunctionConstantByElement::set(int iGroup, int iElem, double value)
{
  _data[iGroup][iElem] = value;
}

void dgFunctionConstantByElement::call(dataCacheMap* m, fullMatrix< double >& val)
{
  dataCacheMap* dgMap = m;
  int iGroup = _groups->getElementGroupId(dgMap->getGroupOfElements());
  for (int iEl = 0; iEl < m->nElement(); iEl++)
    for (int i = 0; i < m->nPointByElement(); ++i)
      for (int j = 0; j < val.size2(); j++)
        val(iEl * m->nPointByElement() + i ,j) = _data[iGroup][m->elementId(iEl)];
}

void dgFunctionElementVolume::update()
{
  Msg::Error("This function should be removed");
 // int nbElemGroups = _groups->getNbElementGroups();
 // for ( int iG = 0; iG < nbElemGroups; iG ++ ) {
 //   dgGroupOfElements* group = _groups->getElementGroup(iG);
 //   int nbElements = group->getNbElements();
 //   for ( int iE = 0; iE < nbElements; iE++ ) {
 //     set(iG,iE, group->getElementVolume(iE));
 //   }
 // }
}

void dgFunctionMaximumByElement::update()
{
  dataCacheMap cacheMap(dataCacheMap::NODE_MODE, _groups);
  dataCacheDouble* fCache = cacheMap.get(_f,NULL);
  for (int iG = 0; iG < _groups->getNbElementGroups(); iG++) {
    dgGroupOfElements *group = _groups->getElementGroup(iG);
    int nbNodes = group->getNbNodes();
    cacheMap.setGroup(group);
    for (size_t iE =0; iE < group->getNbElements(); iE++) {
      cacheMap.setElement(iE);
      double maxVal = -1e20;
      for (int iNode = 0; iNode < nbNodes; iNode++ ) {
        maxVal = std::max( fCache->get()(iNode,0), maxVal );
      }
      set(iG, iE, maxVal );
    }
  }
}

void dgFunctionIpEigenValue::update()
{
  Msg::Error("dgFunctionIpEigenValue has to be implemented");
  #if 0
  dataCacheMap cacheMap(dataCacheMap::INTEGRATION_MODE, _groups);
  dataCacheDouble* fCache = cacheMap.get(_f,NULL);
  dataCacheDouble* keddCache = cacheMap.get(_kedd,NULL);
  double invJac[3][3];
  double K[3][3];
  double lambdaM, lambdam;
  fullMatrix <double> Kp(3,3), eigVR(3,3), eigVI(3,3), Kjb(3,3);
  fullVector <double>  lambdaR(3), lambdaI(3), Kj(3);
  double alpha;
  //alpha = 0.00000001;
  alpha = 1.e-5;
  const dgMeshJacobian &jacobian = cacheMap.getJacobians();
  for (int iGF = 0; iGF < _groups->getNbFaceGroups(); iGF++){
    dgGroupOfFaces *groupF = _groups->getFaceGroup(iGF);
    int nbConn = groupF->nConnection();
    int nbFaceElems = groupF->getNbElements();
    for (int iC = 0; iC < nbConn; iC++){
      cacheMap.setInterfaceGroup(groupF,iC);
      const dgGroupOfConnections& connGroup = groupF->getGroupOfConnections(iC);
      const dgGroupOfElements& elemGroup = connGroup.getGroupOfElements();
      const int iG = _groups->getElementGroupId(&elemGroup);
      for (int iFace = 0; iFace < nbFaceElems; iFace++) {
        cacheMap.setInterface(iFace);
        double maxVal = -1e20;
        double detF = groupF->getInterfaceSurface(iFace);
        double n[3];
        int iE = connGroup.getElementId(iFace);
        int nbIntegrationPts = fCache->get().size1();
        for (int iQP=0; iQP<nbIntegrationPts;iQP++){
          Kjb.scale(0);
          n[0] = connGroup.getNormal(iFace,iQP,0);
          n[1] = connGroup.getNormal(iFace,iQP,1);
          n[2] = connGroup.getNormal(iFace,iQP,2);
          K[0][0] = keddCache->get()(iQP,0);
          K[0][1] = 0;
          K[0][2] = keddCache->get()(iQP,0)*fCache->get()(iQP,0);
          K[1][0] = 0;
          K[1][1] = keddCache->get()(iQP,0);
          K[1][2] = keddCache->get()(iQP,0)*fCache->get()(iQP,1);
          K[2][0] = keddCache->get()(iQP,0)*fCache->get()(iQP,0);
          K[2][1] = keddCache->get()(iQP,0)*fCache->get()(iQP,1);
          K[2][2] = keddCache->get()(iQP,0)*(alpha +fCache->get()(iQP,1)*fCache->get()(iQP,1)+fCache->get()(iQP,0)*fCache->get()(iQP,0));
          for (int l=0; l<3;l++){
             for (int m=0;m<3;m++){
                invJac[l][m]= connGroup.getDXiDX(iFace,iQP,l,m);
             }
           }
           for (int s=0;s<3;s++){
             Kj(s)=0;
             for (int l=0;l<3;l++){
               for (int m=0;m<3;m++){
                  Kj(s)+=detF*K[s][m]*invJac[m][l]*n[l];
               }
             }
           }
            lambdaM=std::max(std::max(Kj(0),Kj(1)),Kj(2));
            maxVal = std::max( lambdaM, maxVal );
        }
        double currentMax = get(iG, iE);
        set(iG, iE, std::max( currentMax, maxVal ) );
      }
    }
  }
  for (int iG = 0; iG < _groups->getNbElementGroups(); iG++) {
    dgGroupOfElements *group = _groups->getElementGroup(iG);
    int nbNodes = group->getNbNodes();
    cacheMap.setGroup(group);
    for (int iE =0; iE < group->getNbElements(); iE++) {
      cacheMap.setElement(iE);
      double minVal = 1e20;
      int nbIntegrationPts = fCache->get().size1();
      for (int iQP = 0; iQP < nbNodes; iQP++ ) {
        Kp.scale(0);
        double det = jacobian.getDetJElement(iG, iE, iQP);
        K[0][0] = keddCache->get()(iQP,0);
        K[0][1] = 0;
        K[0][2] = keddCache->get()(iQP,0)*fCache->get()(iQP,0);
        K[1][0] = 0;
        K[1][1] = keddCache->get()(iQP,0);
        K[1][2] = keddCache->get()(iQP,0)*fCache->get()(iQP,1);
        K[2][0] = keddCache->get()(iQP,0)*fCache->get()(iQP,0);
        K[2][1] = keddCache->get()(iQP,0)*fCache->get()(iQP,1);
        K[2][2] = keddCache->get()(iQP,0)*(alpha +fCache->get()(iQP,1)*fCache->get()(iQP,1)+fCache->get()(iQP,0)*fCache->get()(iQP,0));
        for (int l=0; l<3;l++){
          for (int m=0;m<3;m++){
            invJac[l][m]= group->getInvJ(iE,iQP,l,m);
          }
        }
        for (int s=0; s<3;s++){
           for (int t=0;t<3;t++){
             Kp(s,t)=0;
              for (int l=0; l<3;l++){
                  for (int m=0;m<3;m++){
                     Kp(s,t) += det*invJac[s][l]*K[l][m]*invJac[t][m];
                  }
              }
           }
        }
        Kp.eig(lambdaR,lambdaI,eigVR, eigVI);
        lambdam=std::min(std::min(lambdaR(0),lambdaR(1)),lambdaR(2));
        
        minVal = std::min( lambdam, minVal );
      }
      double max = get(iG, iE);
      set(iG, iE, max*max/minVal);
    }
  }
  #endif
}

void dgFunctionIpErn::update()
{
  #if 0
  dataCacheMap cacheMap(dataCacheMap::INTEGRATION_MODE, _groups);
  dataCacheDouble* fCache = cacheMap.get(_f,NULL);
  dataCacheDouble* keddCache = cacheMap.get(_kedd,NULL);
  double invJac[3][3];
  double K[3][3];
  fullMatrix <double>  Kp(3,3);
  fullVector <double>  Kv(1), Kj(3);
  double alpha;
  alpha = 1.e-7;
  for (int iGF = 0; iGF < _groups->getNbFaceGroups(); iGF++){
    dgGroupOfFaces *groupF = _groups->getFaceGroup(iGF);
    int nbConn = groupF->nConnection();
    int nbFaceElems = groupF->getNbElements();
    for (int iC = 0; iC < nbConn; iC++){
      cacheMap.setInterfaceGroup(groupF,iC);
      const dgGroupOfConnections& connGroup = groupF->getGroupOfConnections(iC);
      const dgGroupOfElements& elemGroup = connGroup.getGroupOfElements();
      const int iG = _groups->getElementGroupId(&elemGroup);
      for (int iFace = 0; iFace < nbFaceElems; iFace++) {
        cacheMap.setInterface(iFace);
        double val = -1e20;
        double detF = groupF->getInterfaceSurface(iFace);
        double n[3];
        int iE = connGroup.getElementId(iFace);
        int nbIntegrationPts = fCache->get().size1();
        for (int iQP=0; iQP<nbIntegrationPts;iQP++){
          Kj.scale(0);
          n[0] = connGroup.getNormal(iFace,iQP,0);
          n[1] = connGroup.getNormal(iFace,iQP,1);
          n[2] = connGroup.getNormal(iFace,iQP,2);
          K[0][0] = keddCache->get()(iQP,0);
          K[0][1] = 0;
          K[0][2] = keddCache->get()(iQP,0)*fCache->get()(iQP,0);
          K[1][0] = 0;
          K[1][1] = keddCache->get()(iQP,0);
          K[1][2] = keddCache->get()(iQP,0)*fCache->get()(iQP,1);
          K[2][0] = keddCache->get()(iQP,0)*fCache->get()(iQP,0);
          K[2][1] = keddCache->get()(iQP,0)*fCache->get()(iQP,1);
          K[2][2] = keddCache->get()(iQP,0)*(alpha +fCache->get()(iQP,1)*fCache->get()(iQP,1)+fCache->get()(iQP,0)*fCache->get()(iQP,0));
          for (int l=0; l<3;l++){
             for (int m=0;m<3;m++){
                invJac[l][m]= connGroup.getDXiDX(iFace,iQP,l,m);
             }
           }
           Kv.scale(0);
           Kj.scale(0);
           Kp.scale(0);
           for (int s=0;s<3;s++){
             for (int t=0;t<3;t++){
                for (int l=0;l<3;l++){
                  for (int m=0;m<3;m++){
                      Kp(s,t)+=invJac[s][l]*K[l][m]*invJac[m][t];
                  }
                }
                Kj(t)+=Kp(s,t)*n[t];
             }
             Kv(1)+=Kj(s)*n[s];
           }
            val = std::max( Kv(1), val );
        }
        double currentMax = get(iG, iE);
        set(iG, iE, std::max( currentMax, val ));
      }
    }
  }
  #endif
}
