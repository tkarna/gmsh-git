#ifndef _FUNCTIONS_H_
#define _FUNCTIONS_H_
#include "function.h"
class functionConstantByTag : public function {
 public:
  std::map<std::string, fullMatrix<double> > _source;
  void call(dataCacheMap *m, fullMatrix<double> &val);
  functionConstantByTag(std::map<std::string, std::vector<double> >source);
  functionConstantByTag(std::map<std::string, double>source);
};

class functionC : public function {
  std::vector<fullMatrix<double> > _args;
  void (*_callback)(void);
 public:
  static void buildLibrary(std::string code, std::string filename, bool keepTmpFiles=false) ;
	static void buildLibraryFromFile(const std::string cfilename, const std::string libfilename, bool keepTmpFiles=false);
  void call (dataCacheMap *m, fullMatrix<double> &val) ;
  functionC (std::string file, std::string symbol, int nbCol, 
             std::vector<const function *> dependencies);
};

class functionPrecomputed : public function {
  const dgGroupCollection *_groups;
  std::vector<dgFullMatrix<double> > _elementData, _faceData, _elementDataNode, _faceDataNode;
  int _integrationOrder;
 public:
  void call(dataCacheMap *m, fullMatrix<double> &sol);
  functionPrecomputed(const dgGroupCollection &groups, int integrationOrder, int nbCols);
  void compute(const function &f, const dgGroupOfElements &elements, dataCacheMap::mode m = dataCacheMap::INTEGRATION_GROUP_MODE);
  void compute(const function &f, const dgGroupOfFaces &faces, dataCacheMap::mode m = dataCacheMap::INTEGRATION_GROUP_MODE);
  void compute(const function &f, dataCacheMap::mode m = dataCacheMap::INTEGRATION_GROUP_MODE);
  void clear();
};

class dgExtrusion;
class functionPrecomputedExtrusion : public function {
  const dgExtrusion &_extrusion;
  functionPrecomputed _f2d, _f3d;
  public :
  void call(dataCacheMap *m, fullMatrix<double> &sol);
  functionPrecomputedExtrusion(const dgExtrusion &extrusion, int integrationOrder, int nbCols);
  functionPrecomputedExtrusion(const dgExtrusion &extrusion, int integrationOrder, const function &f);
  void compute(const function &f, dataCacheMap::mode m = dataCacheMap::INTEGRATION_GROUP_MODE);
  void clear();
};

class gLevelset;
class Field;
function *functionLevelsetValueNew (const gLevelset *levelset);
function *functionLevelsetNew (const function *f0, const double valMin, const double valPlus);
function *functionLevelsetSmoothNew (const function *f0, const double valMin, const double valPlus, const double E);
function *functionSumNew (const function *f0, const function *f1);
function *functionMinusNew (const function *f0, const function *f1);
function *functionFieldNew (Field &field, const function *coord);
function *functionProdNew (const function *f0, const function *f1);
function *functionQuotientNew (const function *f0, const function *f1);
function *functionScaleNew (const function *f0, const double s);
function *functionAbsNew (const function *f0);
function *functionMinNew (const function *f0, const function *f1);
function *functionMaxNew (const function *f0, const function *f1);
function *functionMinNew (const function *f0, const double s);
function *functionMaxNew (const function *f0, const double s);
function *functionExtractCompNew (const function *f0, const int iComp);
function *functionExtractCompNew (const function *f0, std::vector<int> iComp);
function *functionCatCompNew(std::vector<const function *> fArray);
function *functionMeanP1New(const function *f, const function *df);
#endif
