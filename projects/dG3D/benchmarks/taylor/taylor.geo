// creat a cube with fiber

// characteristic size for fibe
R= 0.0032;                          // fiber radius
L= 0.0324;

// characteristic Size for the volum
x = R;
y = R;
z = L;

sq2 = Sqrt(2.0);


// Characteristic length
Lc1=R/4;


// definition of points***********************************************
Point(5) = {  R ,  y , 0.0 , Lc1};
Point(6) = {  0.0  , y-R , 0.0 , Lc1};
Point(8) = { 0.0 ,  y  , 0.0 , Lc1};

Point(14) = { R/sq2 ,  y-R/sq2  , 0.0 , Lc1};
Point(15) = { R/(2.0*sq2) , y-R/(2.0*sq2), 0.0 , Lc1};
Point(16) = { R/(2.0*sq2) ,  y  , 0.0 , Lc1};
Point(17) = { 0.0 ,  y-R/(2.0*sq2) , 0.0 , Lc1};

Line(14) = {5,16};
Line(15) = {16,8};
Line(16) = {8,17};
Line(17) = {17,6};
Line(18) = {14,15};
Line(19) = {16,15};
Line(20) = {15,17};

Circle(21) = {6,8,14};
Circle(22) = {14,8,5};

// from surface

Line Loop(8) = {14,19,-18,22};
Line Loop(9) = {15,16,-20,-19};
Line Loop(10) = {17,21,18,20};

Plane Surface(8) = {8};
Plane Surface(9) = {9};
Plane Surface(10) = {10};

Transfinite Line {14,15,16,17,18,19,20,21,22} = 3;
Transfinite Surface {8,9,10};
//Recombine Surface {8,9,10};

my_volume[] = Extrude {0.0 , 0.0 , z} {Surface{8,9,10}; Layers{7}; };




Transfinite Volume {1};
Transfinite Volume {2};
Transfinite Volume {3};

Physical Volume(102) ={1,2,3};

// fix x
Physical Surface(1) = {57,75};

// fix y
Physical Surface(2) = {53,31};

// fix z
Physical Surface(3) = {8,9,10};



