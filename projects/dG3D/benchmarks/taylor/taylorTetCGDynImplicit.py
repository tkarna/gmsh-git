#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch beam problem with a python script

# material law
lawnum = 1 # unique number of law
rho   = 8930
K = 130.e9
G = 43.333e9
young = 9*K*G/(3*K+G)
print young
nu    = (3*K-2*G)/(2*(3*K+G))
print nu
sy0   = 400.e6
h     = 100.e6


# geometry
meshfile="taylorTet.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 4 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 250   # number of step
ftime =80.e-6   # Final time 
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
tol = 1e-4
fullDg = 0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)


#  compute solution and BC (given directly to the solver
# creation of law
law1 = J2LinearDG3DMaterialLaw(lawnum,rho,young,nu,sy0,h)

# creation of ElasticField
nfield = 102 # number of the field (physical number of surface)
myfield1 = dG3DDomain(1000,nfield,space1,lawnum,fullDg)
#myfield1.matrixByPerturbation(1,1,1,1e-8)

# creation of Solver

alpham  = 0
alphaf = 0

gamma = 0.5-alpham+alphaf
beta = 0.25*(1-alpham+alphaf)*(1-alpham+alphaf)


mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
#mysolver.implicitData(beta,gamma,alpham,alphaf)
mysolver.implicitSpectralRadius(0.5)
mysolver.stepBetweenArchiving(nstepArch)
# BC
mysolver.displacementBC("Face",1,0,0.)
mysolver.displacementBC("Face",2,1,0.)
mysolver.displacementBC("Face",3,2,0.)
mysolver.initialBC("Volume","Velocity",102,2,-227.)

mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 3, 2)

mysolver.solve()

