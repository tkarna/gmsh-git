// creat a cube with fiber

//defination of unit
mm = 0.001;

// volum fraction

f=0.6;

// characteristic size for fibe
R= 0.0052*mm;                          // fiber radius
ly=2.0*R*Sqrt(Pi/(2.0*Sqrt(3.0)*f));  // distance between fibers' centers
lx=ly*Sqrt(3.0)/2.0;

// characteristic Size for the volum
x = 4*ly;
y = 4*lx;
z = R/2.0;


// Characteristic length
Lc1=lx/10.0;
Lc2=lx/10.0;

// definition of points***********************************************
Point(1) = { 0.0 , 0.0 , 0.0 , Lc1};
Point(2) = {  x  , 0.0 , 0.0 , Lc1};
Point(3) = {  x  ,  y  , 0.0 , Lc1};
Point(4) = { 0.0 ,  y  , 0.0 , Lc1};

// Line between points
Line(1) = {1,2};
Line(2) = {3,4};


//define line Loop list for fibers
t=-1;
//fiber*************************************************************

For j In {0:2:2}
For i In {0:3}

t=t+1;
  
//define new point
p1 = newp; Point(p1) = {i*ly+ly/2.0 , j*lx+lx/2.0 ,0.0 ,Lc2};
p2 = newp; Point(p2) = {i*ly+ly/2.0-R,j*lx+lx/2.0 ,0.0 ,Lc2}; 
p3 = newp; Point(p3) = {i*ly+ly/2.0 , j*lx+lx/2.0-R ,0.0 ,Lc2};
p4 = newp; Point(p4) = {i*ly+ly/2.0+R,j*lx+lx/2.0 ,0.0 ,Lc2}; 
p5 = newp; Point(p5) = {i*ly+ly/2.0 , j*lx+lx/2.0+R ,0.0 ,Lc2}; 


//define new circles

c1 = newreg; Circle(c1) = {p2,p1,p3};
c2 = newreg; Circle(c2) = {p3,p1,p4};
c3 = newreg; Circle(c3) = {p4,p1,p5};
c4 = newreg; Circle(c4) = {p5,p1,p2};


theloop[t] = newreg; Line Loop(theloop[t]) = {c1,c2,c3,c4}; 

thesurface[t] = news;
Plane Surface( thesurface[t]) = {theloop[t]};

EndFor
EndFor

//****************************************************************
For j In {1:3:2}
For i In {1:3}
  
t=t+1;

//define new point
p1 = newp; Point(p1) = {i*ly , j*lx+lx/2.0 ,0.0 ,Lc2};
p2 = newp; Point(p2) = {i*ly-R,j*lx+lx/2.0 ,0.0 ,Lc2}; 
p3 = newp; Point(p3) = {i*ly , j*lx+lx/2.0-R ,0.0 ,Lc2};
p4 = newp; Point(p4) = {i*ly+R,j*lx+lx/2.0 ,0.0 ,Lc2}; 
p5 = newp; Point(p5) = {i*ly , j*lx+lx/2.0+R ,0.0 ,Lc2}; 


//define new circles

c1 = newreg; Circle(c1) = {p2,p1,p3};
c2 = newreg; Circle(c2) = {p3,p1,p4};
c3 = newreg; Circle(c3) = {p4,p1,p5};
c4 = newreg; Circle(c4) = {p5,p1,p2};


theloop[t] = newreg; Line Loop(theloop[t]) = {c1,c2,c3,c4}; 

thesurface[t] = news;
Plane Surface( thesurface[t]) = {theloop[t]};

EndFor
EndFor

//***********************************************************
// define new point

t=-1;
For j In {1:3:2}

t=t+1;

p1 = newp; Point(p1) = {0.0, j*lx+lx/2.0 ,0.0 ,Lc2};
p2 = newp; Point(p2) = {0.0, j*lx+lx/2.0-R ,0.0 ,Lc2}; 
p3 = newp; Point(p3) = { R , j*lx+lx/2.0 ,0.0 ,Lc2};
p4 = newp; Point(p4) = {0.0, j*lx+lx/2.0+R ,0.0 ,Lc2}; 

//define new circles

c1 = newreg; Circle(c1) = {p2,p1,p3};
c2 = newreg; Circle(c2) = {p3,p1,p4};
l3 = newreg; Line (l3) = {p4,p2};


loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {c1,c2,l3}; 

surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

EndFor

//***********************************************************
// define new point

For j In {1:3:2}

t=t+1;

p1 = newp; Point(p1) = {x, j*lx+lx/2.0 ,0.0 ,Lc2};
p2 = newp; Point(p2) = {x, j*lx+lx/2.0-R ,0.0 ,Lc2}; 
p3 = newp; Point(p3) = {x-R, j*lx+lx/2.0 ,0.0 ,Lc2};
p4 = newp; Point(p4) = {x, j*lx+lx/2.0+R ,0.0 ,Lc2}; 

//define new circles

c1 = newreg; Circle(c1) = {p4,p1,p3};
c2 = newreg; Circle(c2) = {p3,p1,p2};
l3 = newreg; Line (l3) = {p2,p4};


loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {c1,c2,l3}; 

surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

EndFor

//define the line loop for matrix surface****************************

l1 = newreg; Line (l1) = {2,84};
l2 = newreg; Line (l2) = {86,88};
l3 = newreg; Line (l3) = {90,3};
l4 = newreg; Line (l4) = {4,82};
l5 = newreg; Line (l5) = {80,78};
l6 = newreg; Line (l6) = {76,1};

// Surface definition
lineloop_matix = newreg;

Line Loop(lineloop_matix) = {1,l1,-98,-97,l2,-103,-102,l3,2,l4,-93,-92,l5,-88,-87,l6};

// define the surface for matrix
MaS=news;
Plane Surface(MaS) = {lineloop_matix,theloop[]};


//VOlume of fiber and matrix ******************************************************
my_fiberV[] = Extrude {0.0 , 0.0 , z} {Surface{thesurface[],surface_edg[]};Layers{1};};
my_mtrixV[] = Extrude {0.0 , 0.0 , z} {Surface {MaS}; Layers{1};};



// Physical objects to applied BC **********************************
//fix x displacement
Physical Surface(101) = {603,615,627,438,455};

//add x displacement
Physical Surface(102) = {595,583,571,472,489};

//fix y displacement
Physical Surface(111) = {567};
Physical Surface(112) = {599};

//fix z displacement
Physical Surface(121) = {MaS,thesurface[],surface_edg[]};
Physical Surface(122) = {852,136,158,180,202,224,246,268,290,312,334,356,378,400,422,439,456,473,490};



// Physical objects to applied material*********************
Physical Volume(1001) ={my_mtrixV[]};
Physical Volume(1002) ={my_fiberV[]};


//define transfinite mesh***************************************
Transfinite Line {l3,l4} = 2;










