// creat a cube with fiber

//defination of unit
mm = 0.001;

// volum fraction

f=0.6;

// characteristic size for fibe
R= 0.0052*mm;                          // fiber radius
ly=2.0*R*Sqrt(Pi/(2.0*Sqrt(3.0)*f));  // distance between fibers' centers
lx=ly*Sqrt(3.0)/2.0;

// characteristic Size for the volume
x = 4*ly;
y = 4*lx;
z = R/2.0;


// Characteristic length
Lc1=lx/10.0;
Lc2=lx/10.0;

// definition of points  at the corners of volume ******************************************
Point(1) = { 0.0 , 0.0 , 0.0 , Lc1};
Point(2) = {  x  , 0.0 , 0.0 , Lc1};
Point(3) = {  x  ,  y  , 0.0 , Lc1};
Point(4) = { 0.0 ,  y  , 0.0 , Lc1};

//half fiber *********************************
Point(5) = { 0.7324*x, 0.0 , 0.0 , Lc1};
Point(6) = {  x , 0.4908*y , 0.0 , Lc1};
Point(7) = {  0.95*x , 0.8*y  , 0.0 , Lc1};
Point(8) = { 0.5475*x, 1.0420*y  , 0.0 , Lc1};
Point(9) = { 0.0390*x, 0.6428*y , 0.0 , Lc1};
Point(10) = { -0.05*x, 0.40*y , 0.0 , Lc1};

Point(11) = { 0.7324*x-R, 0.0 , 0.0 , Lc1};
Point(12) = { 0.7324*x, R , 0.0 , Lc1};
Point(13) = { 0.7324*x+R, 0.0 , 0.0 , Lc1};

Point(14) = {  x ,  0.4908*y-R , 0.0 , Lc1};
Point(15) = {  x-R ,  0.4908*y , 0.0 , Lc1};
Point(16) = {  x ,  0.4908*y+R , 0.0 , Lc1};

Point(17) = {  x , 0.8*y-Sqrt(R*R-0.0025*x*x) , 0.0 , Lc1};
Point(18) = { 0.95*x-R , 0.8*y  , 0.0 , Lc1};
Point(19) = {  x , 0.8*y+Sqrt(R*R-0.0025*x*x) , 0.0 , Lc1};

Point(20) = { 0.5475*x+Sqrt(R*R-0.042*0.042*y*y), y  , 0.0 , Lc1};
Point(21) = { 0.5475*x-Sqrt(R*R-0.042*0.042*y*y), y  , 0.0 , Lc1};

Point(22) = { 0.0, 0.6428*y+Sqrt(R*R-0.039*0.039*x*x) , 0.0 , Lc1};
Point(23) = { 0.0390*x+R, 0.6428*y , 0.0 , Lc1};
Point(24) = { 0.0, 0.6428*y-Sqrt(R*R-0.039*0.039*x*x) , 0.0 , Lc1};

Point(25) = { 0, 0.40*y+Sqrt(R*R-0.0025*x*x) , 0.0 , Lc1};
Point(26) = { 0, 0.40*y-Sqrt(R*R-0.0025*x*x) , 0.0 , Lc1};


Line(1) = {1,11};
Circle(2) = {11,5,12};
Circle(3) = {12,5,13};
Line(4) = {13,2};

Line(5) = {2,14};
Circle(6) = {14,6,15};
Circle(7) = {15,6,16};
Line(8) = {16,17};
Circle(9) = {17,7,18};
Circle(10) = {18,7,19};
Line(11) = {19,3};

Line(12) = {3,20};
Circle(13) = {20,8,21};
Line(14) = {21,4};

Line(15) = {4,22};
Circle(16) = {22,9,23};
Circle(17) = {23,9,24};
Line(18) = {24,25};
Circle(19) = {25,10,26};
Line(20) = {26,1};

Line(21) = {11,13};
Line(22) = {14,16};
Line(23) = {17,19};
Line(24) = {20,21};
Line(25) = {22,24};
Line(26) = {25,26};

//define fibers at boundary*************************************
t=0;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-3,-2,21}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-7,-6,22}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-10,-9,23}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-13,24}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-17,-16,25}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-19,26}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

// definition of points  

xc[]={0.2457, 0.3696, 0.5181, 0.7808, 0.5424, 0.7488, 0.6109, 0.3861, 0.7026, 0.3408, 0.1293, 0.1234, 0.88};
yc[]={0.3704, 0.8525, 0.4568, 0.6286, 0.7033, 0.3774, 0.1976, 0.1302, 0.8725, 0.5943, 0.1459, 0.8677,0.175};

For i In {0:12}

t=t+1;
x0 = xc[i]*x;
y0 = yc[i]*y;
p1 = newp; Point(p1) = {x0, y0, 0.0 ,Lc2};
p2 = newp; Point(p2) = {x0, y0-R ,0.0 ,Lc2}; 
p3 = newp; Point(p3) = {x0+R, y0 ,0.0 ,Lc2};
p4 = newp; Point(p4) = {x0, y0+R ,0.0 ,Lc2}; 
p5 = newp; Point(p5) = {x0-R, y0 ,0.0 ,Lc2}; 

//define new circles

c1 = newreg; Circle(c1) = {p2,p1,p3};
c2 = newreg; Circle(c2) = {p3,p1,p4};
c3 = newreg; Circle(c3) = {p4,p1,p5};
c4 = newreg; Circle(c4) = {p5,p1,p2};

theloop[i] = newreg; Line Loop(theloop[i]) = {c1,c2,c3,c4}; 

surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {theloop[i]};

EndFor

// Surface definition
lineloop_matix = newreg;
Line Loop(lineloop_matix) = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};

// define the surface for matrix
MaS=news;
Plane Surface(MaS) = {lineloop_matix,theloop[]};

//Volume of fiber and matrix ******************************************************
my_fiberV[] = Extrude {0.0 , 0.0 , z} {Surface{surface_edg[]};Layers{1};};
my_mtrixV[] = Extrude {0.0 , 0.0 , z} {Surface {MaS}; Layers{1};};



// Physical objects to applied BC **********************************

// Physical objects to applied material*********************
Physical Volume(1001) ={my_mtrixV[]};
Physical Volume(1002) ={my_fiberV[]};


//fix x displacement
Physical Surface(101) = {197,629,641,209,649};
//add x displacement
Physical Surface(102) = {589,151,601,168,613};

//fix y displacement
Physical Surface(111) = {134,573,585};
Physical Surface(112) = {180,617,625};

//fix z displacement
Physical Surface(121) = {MaS,surface_edg[]};
Physical Surface(122) = {474,254,181,408,169,298,320,152,276,342,496,135,364,386,452,232,430,198,210,858};


//define transfinite mesh***************************************
//Transfinite Line {l3,l4} = 2;










