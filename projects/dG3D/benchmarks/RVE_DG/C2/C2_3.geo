// creat a cube with fiber

//defination of unit
mm = 0.001;

// volum fraction

f=0.6;

// characteristic size for fibe
R= 0.0052*mm;                          // fiber radius
ly=2.0*R*Sqrt(Pi/(2.0*Sqrt(3.0)*f));  // distance between fibers' centers
lx=ly*Sqrt(3.0)/2.0;

// characteristic Size for the volume
x = 4*ly;
y = 4*lx;
z = R/2.0;


// Characteristic length
Lc1=lx/10.0;
Lc2=lx/10.0;

// definition of points  at the corners of volume ******************************************
Point(1) = { 0.0 , 0.0 , 0.0 , Lc1};
Point(2) = {  x  , 0.0 , 0.0 , Lc1};
Point(3) = {  x  ,  y  , 0.0 , Lc1};
Point(4) = { 0.0 ,  y  , 0.0 , Lc1};

//half fiber *********************************
Point(5) = { 0.6705*x, 0.0 , 0.0 , Lc1};
Point(6) = {  x ,  0.3805*y , 0.0 , Lc1};
Point(7) = {  0.4684*x , y  , 0.0 , Lc1};
Point(8) = { 0.0, 0.8419*y  , 0.0 , Lc1};

Point(9) = { 0.6705*x-R, 0.0 , 0.0 , Lc1};
Point(10) = { 0.6705*x, R , 0.0 , Lc1};
Point(11) = { 0.6705*x+R, 0.0 , 0.0 , Lc1};

Point(12) = {  x ,  0.3805*y-R , 0.0 , Lc1};
Point(13) = {  x-R ,  0.3805*y , 0.0 , Lc1};
Point(14) = {  x ,  0.3805*y+R , 0.0 , Lc1};

Point(15) = {  0.4684*x+R , y  , 0.0 , Lc1};
Point(16) = {  0.4684*x , y-R  , 0.0 , Lc1};
Point(17) = {  0.4684*x-R , y  , 0.0 , Lc1};

Point(18) = { 0.0, 0.8419*y+R  , 0.0 , Lc1};
Point(19) = { 0.0+R, 0.8419*y  , 0.0 , Lc1};
Point(20) = { 0.0, 0.8419*y-R  , 0.0 , Lc1};

Line(1) = {1,9};
Circle(2) = {9,5,10};
Circle(3) = {10,5,11};
Line(4) = {11,2};

Line(5) = {2,12};
Circle(6) = {12,6,13};
Circle(7) = {13,6,14};
Line(8) = {14,3};

Line(9) = {3,15};
Circle(10) = {15,7,16};
Circle(11) = {16,7,17};
Line(12) = {17,4};

Line(13) = {4,18};
Circle(14) = {18,8,19};
Circle(15) = {19,8,20};
Line(16) = {20,1};

Line(17) = {9,11};
Line(18) = {12,14};
Line(19) = {15,17};
Line(20) = {18,20};

//define fibers at boundary*************************************
t=0;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-3,-2,17}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-7,-6,18}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-11,-10,19}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-15,-14,20}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

// definition of points  

xc[]={0.3190, 0.8571, 0.4164, 0.3272, 0.7562, 0.5323, 0.1327, 0.1164, 0.6533, 0.5415, 0.2135, 0.880, 0.121,0.878};
yc[]={0.5032, 0.5954, 0.7398, 0.1784, 0.3571, 0.4568, 0.3729, 0.6269, 0.7496, 0.2030, 0.8450, 0.138, 0.13, 0.864};

For i In {0:13}

t=t+1;
x0 = xc[i]*x;
y0 = yc[i]*y;
p1 = newp; Point(p1) = {x0, y0, 0.0 ,Lc2};
p2 = newp; Point(p2) = {x0, y0-R ,0.0 ,Lc2}; 
p3 = newp; Point(p3) = {x0+R, y0 ,0.0 ,Lc2};
p4 = newp; Point(p4) = {x0, y0+R ,0.0 ,Lc2}; 
p5 = newp; Point(p5) = {x0-R, y0 ,0.0 ,Lc2}; 

//define new circles

c1 = newreg; Circle(c1) = {p2,p1,p3};
c2 = newreg; Circle(c2) = {p3,p1,p4};
c3 = newreg; Circle(c3) = {p4,p1,p5};
c4 = newreg; Circle(c4) = {p5,p1,p2};

theloop[i] = newreg; Line Loop(theloop[i]) = {c1,c2,c3,c4}; 

surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {theloop[i]};

EndFor

// Surface definition
lineloop_matix = newreg;
Line Loop(lineloop_matix) = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};

// define the surface for matrix
MaS=news;
Plane Surface(MaS) = {lineloop_matix,theloop[]};

//Volume of fiber and matrix ******************************************************
my_fiberV[] = Extrude {0.0 , 0.0 , z} {Surface{surface_edg[]};Layers{1};};
my_mtrixV[] = Extrude {0.0 , 0.0 , z} {Surface {MaS}; Layers{1};};



// Physical objects to applied BC **********************************

// Physical objects to applied material*********************
Physical Volume(1001) ={my_mtrixV[]};
Physical Volume(1002) ={my_fiberV[]};


//fix x displacement
Physical Surface(101) = {181,615,627};
//add x displacement
Physical Surface(102) = {147,595,583};

//fix y displacement
Physical Surface(111) = {130,567,579};
Physical Surface(112) = {164,599,611};

//fix z displacement
Physical Surface(121) = {MaS,surface_edg[]};
Physical Surface(122) = {148,402,468,380,226,314,424,446,204,131,490,248,336,292,358,165,182,270,852};


//define transfinite mesh***************************************
//Transfinite Line {l3,l4} = 2;










