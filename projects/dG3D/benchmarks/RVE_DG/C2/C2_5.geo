// creat a cube with fiber

//defination of unit
mm = 0.001;

// volum fraction

f=0.6;

// characteristic size for fibe
R= 0.0052*mm;                          // fiber radius
ly=2.0*R*Sqrt(Pi/(2.0*Sqrt(3.0)*f));  // distance between fibers' centers
lx=ly*Sqrt(3.0)/2.0;

// characteristic Size for the volume
x = 4*ly;
y = 4*lx;
z = R/2.0;


// Characteristic length
Lc1=lx/10.0;
Lc2=lx/10.0;

// definition of points  at the corners of volume ******************************************
Point(1) = { 0.0 , 0.0 , 0.0 , Lc1};
Point(2) = {  x  , 0.0 , 0.0 , Lc1};
Point(3) = {  x  ,  y  , 0.0 , Lc1};
Point(4) = { 0.0 ,  y  , 0.0 , Lc1};

//half fiber *********************************
Point(5) = { 1.04*x , 0.14*y , 0.0 , Lc1};
Point(6) = { 0.04*x, 0.85*y , 0.0 , Lc1};

Point(7) = { x , 0.14*y-Sqrt(R*R-0.04*0.04*x*x) , 0.0 , Lc1};
Point(8) = { x , 0.14*y+Sqrt(R*R-0.04*0.04*x*x) , 0.0 , Lc1};

Point(9) = { 0.0, 0.85*y+Sqrt(R*R-0.04*0.04*x*x) , 0.0 , Lc1};
Point(10) = {0.04*x+R, 0.85*y , 0.0 , Lc1};
Point(11) = { 0.0, 0.85*y-Sqrt(R*R-0.04*0.04*x*x) , 0.0 , Lc1};


Line(1) = {1,2};

Line(2) = {2,7};
Circle(3) = {7,5,8};
Line(4) = {8,3};

Line(5) = {3,4};

Line(6) = {4,9};
Circle(7) = {9,6,10};
Circle(8) = {10,6,11};
Line(9) = {11,1};


Line(10) = {7,8};
Line(11) = {9,11};


//define fibers at boundary*************************************
t=0;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-3,10}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-8,-7,11}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

// definition of points  

xc[]={0.8775, 0.3397, 0.5236, 0.3243, 0.5294, 0.6659, 0.8783, 0.1356, 0.4567,  0.7328, 0.7596, 0.6718,0.115,0.12,0.30};
yc[]={0.6668, 0.6023, 0.1335, 0.2138, 0.8264, 0.3582, 0.4162, 0.3614, 0.4071, 0.1355, 0.8759, 0.6524,0.122,0.63,0.84};

For i In {0:14}

t=t+1;
x0 = xc[i]*x;
y0 = yc[i]*y;
p1 = newp; Point(p1) = {x0, y0, 0.0 ,Lc2};
p2 = newp; Point(p2) = {x0, y0-R ,0.0 ,Lc2}; 
p3 = newp; Point(p3) = {x0+R, y0 ,0.0 ,Lc2};
p4 = newp; Point(p4) = {x0, y0+R ,0.0 ,Lc2}; 
p5 = newp; Point(p5) = {x0-R, y0 ,0.0 ,Lc2}; 

//define new circles

c1 = newreg; Circle(c1) = {p2,p1,p3};
c2 = newreg; Circle(c2) = {p3,p1,p4};
c3 = newreg; Circle(c3) = {p4,p1,p5};
c4 = newreg; Circle(c4) = {p5,p1,p2};

theloop[i] = newreg; Line Loop(theloop[i]) = {c1,c2,c3,c4}; 

surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {theloop[i]};

EndFor

//Surface definition
lineloop_matix = newreg;
Line Loop(lineloop_matix) = {1,2,3,4,5,6,7,8,9};

// define the surface for matrix
MaS=news;
Plane Surface(MaS) = {lineloop_matix,theloop[]};

//Volume of fiber and matrix ******************************************************
my_fiberV[] = Extrude {0.0 , 0.0 , z} {Surface{surface_edg[]};Layers{1};};
my_mtrixV[] = Extrude {0.0 , 0.0 , z} {Surface {MaS}; Layers{1};};



// Physical objects to applied BC **********************************

// Physical objects to applied material*********************
Physical Volume(1001) ={my_mtrixV[]};
Physical Volume(1002) ={my_fiberV[]};


//fix x displacement
Physical Surface(101) = {560,135,572};
//add x displacement
Physical Surface(102) = {118,544,552};

//fix y displacement
Physical Surface(111) = {540};
Physical Surface(112) = {556};

//fix z displacement
Physical Surface(121) = {MaS,surface_edg[]};
Physical Surface(122) = {136,466,246,378,158,400,180,444,312,334,268,290,422,224,202,356,119,813};


//define transfinite mesh***************************************
//Transfinite Line {l3,l4} = 2;










