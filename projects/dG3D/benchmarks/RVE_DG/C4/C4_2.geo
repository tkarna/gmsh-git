// creat a cube with fiber

//defination of unit
mm = 0.001;

// volum fraction

f=0.6;

// characteristic size for fibe
R= 0.0052*mm;                          // fiber radius
ly=2.0*R*Sqrt(Pi/(2.0*Sqrt(3.0)*f));  // distance between fibers' centers
lx=ly*Sqrt(3.0)/2.0;

// characteristic Size for the volume
x = 8*ly;
y = 8*lx;
z = R/2.0;


// Characteristic length
Lc1=lx/10.0;
Lc2=lx/10.0;

// definition of points  at the corners of volume ******************************************
Point(1) = { 0.0 , 0.0 , 0.0 , Lc1};
Point(2) = {  x  , 0.0 , 0.0 , Lc1};
Point(3) = {  x  ,  y  , 0.0 , Lc1};
Point(4) = { 0.0 ,  y  , 0.0 , Lc1};

//half fiber *********************************  
Point(5) = { 0.1866*x, 0.028*y , 0.0 , Lc1};
Point(6) = { 0.42*x, -0.03*y , 0.0 , Lc1};
Point(7) = { 0.9305*x, 0.03*y , 0.0 , Lc1};

Point(8) = {  1.02*x , 0.14*y , 0.0 , Lc1};
Point(9) = {  1.0100*x ,  0.6514*y , 0.0 , Lc1};
Point(10) = { 1.000*x ,  0.8823*y , 0.0 , Lc1};

Point(11) = {  0.76933*x , y  , 0.0 , Lc1};
Point(12) = {  0.5380*x , y  , 0.0 , Lc1};
Point(13) = {  0.25*x , 1.028*y  , 0.0 , Lc1};

Point(14) = { 0.02*x, 0.931*y  , 0.0 , Lc1};
Point(15) = { 0.0, 0.3159*y  , 0.0 , Lc1};
Point(16) = { 0.01*x , 0.0708*y  , 0.0 , Lc1};
//****************
Point(17) = { 0.1866*x-Sqrt(R*R-0.028*0.028*y*y), 0.0 , 0.0 , Lc1};
Point(18) = { 0.1866*x, 0.028*y+R , 0.0 , Lc1};
Point(19) = { 0.1866*x+Sqrt(R*R-0.028*0.028*y*y), 0.0 , 0.0 , Lc1};

Point(20) = { 0.42*x-Sqrt(R*R-0.03*0.03*y*y), 0.0 , 0.0 , Lc1};
Point(21) = { 0.42*x, R-0.03*y , 0.0 , Lc1};
Point(22) = { 0.42*x+Sqrt(R*R-0.03*0.03*y*y), 0.0 , 0.0 , Lc1};

Point(23) = { 0.9305*x-Sqrt(R*R-0.03*0.03*y*y), 0.0 , 0.0 , Lc1};
Point(24) = { 0.9305*x, R+0.03*y , 0.0 , Lc1};
Point(25) = { 0.9305*x+Sqrt(R*R-0.03*0.03*y*y), 0.0 , 0.0 , Lc1};
//*******************
Point(26) = {  x ,  0.14*y-Sqrt(R*R-0.0004*x*x) , 0.0 , Lc1};
Point(27) = {  1.02*x-R ,  0.14*y , 0.0 , Lc1};
Point(28) = {  x ,  0.14*y+Sqrt(R*R-0.0004*x*x)  , 0.0 , Lc1};

Point(29) = {  x ,   0.6514*y-Sqrt(R*R-0.0001*x*x) , 0.0 , Lc1};
Point(30) = {  1.0100*x-R ,   0.6514*y , 0.0 , Lc1};
Point(31) = {  x ,   0.6514*y+Sqrt(R*R-0.0001*x*x)  , 0.0 , Lc1};

Point(32) = {  x ,  0.8823*y-R , 0.0 , Lc1};
Point(33) = {  x-R , 0.8823*y , 0.0 , Lc1};
Point(34) = {  x ,  0.8823*y+R , 0.0 , Lc1};

//********************
Point(35) = {  0.76933*x+R , y  , 0.0 , Lc1};
Point(36) = {  0.76933*x , y-R  , 0.0 , Lc1};
Point(37) = {  0.76933*x-R , y  , 0.0 , Lc1};

Point(38) = {  0.5380*x+R , y  , 0.0 , Lc1};
Point(39) = {  0.5380*x , y-R  , 0.0 , Lc1};
Point(40) = {  0.5380*x-R , y  , 0.0 , Lc1};

Point(41) = { 0.25*x+Sqrt(R*R-0.028*0.028*y*y), y  , 0.0 , Lc1};
Point(42) = { 0.25*x, 1.028*y-R  , 0.0 , Lc1};
Point(43) = { 0.25*x-Sqrt(R*R-0.028*0.028*y*y), y  , 0.0 , Lc1};

//********************
Point(44) = { 0.0, 0.931*y+Sqrt(R*R-0.0004*x*x)  , 0.0 , Lc1};
Point(45) = { 0.02*x+R, 0.931*y  , 0.0 , Lc1};
Point(46) = { 0.0, 0.931*y-Sqrt(R*R-0.0004*x*x)  , 0.0 , Lc1};

Point(47) = { 0.0, 0.3159*y+R  , 0.0 , Lc1};
Point(48) = { 0.0+R, 0.3159*y  , 0.0 , Lc1};
Point(49) = { 0.0, 0.3159*y-R  , 0.0 , Lc1};

Point(50) = { 0.0, 0.0708*y+Sqrt(R*R-0.0001*x*x)  , 0.0 , Lc1};
Point(51) = { 0.01*x+R, 0.0708*y  , 0.0 , Lc1};
Point(52) = { 0.0, 0.0708*y-Sqrt(R*R-0.0001*x*x)  , 0.0 , Lc1};

//*************************************************
Line(1) = {1,17};
Circle(2) = {17,5,18};
Circle(3) = {18,5,19};
Line(4) = {19,20};
Circle(5) = {20,6,21};
Circle(6) = {21,6,22};
Line(7) = {22,23};
Circle(8) = {23,7,24};
Circle(9) = {24,7,25};
Line(10) = {25,2};

Line(11) = {2,26};
Circle(12) = {26,8,27};
Circle(13) = {27,8,28};
Line(14) = {28,29};
Circle(15) = {29,9,30};
Circle(16) = {30,9,31};
Line(17) = {31,32};
Circle(18) = {32,10,33};
Circle(19) = {33,10,34};
Line(20) = {34,3};

Line(21) = {3,35};
Circle(22) = {35,11,36};
Circle(23) = {36,11,37};
Line(24) = {37,38};
Circle(25) = {38,12,39};
Circle(26) = {39,12,40};
Line(27) = {40,41};
Circle(28) = {41,13,42};
Circle(29) = {42,13,43};
Line(30) = {43,4};

Line(31) = {4,44};
Circle(32) = {44,14,45};
Circle(33) = {45,14,46};
Line(34) = {46,47};
Circle(35) = {47,15,48};
Circle(36) = {48,15,49};
Line(37) = {49,50};
Circle(38) = {50,16,51};
Circle(39) = {51,16,52};
Line(40) = {52,1};

Line(41) = {17,19};
Line(42) = {20,22};
Line(43) = {23,25};
Line(44) = {26,28};
Line(45) = {29,31};
Line(46) = {32,34};
Line(47) = {35,37};
Line(48) = {38,40};
Line(49) = {41,43};
Line(50) = {44,46};
Line(51) = {47,49};
Line(52) = {50,52};

//define fibers at boundary*************************************
t=0;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-3,-2,41}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-6,-5,42}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-9,-8,43}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-13,-12,44}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-16,-15,45}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-19,-18,46}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-23,-22,47}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-26,-25,48}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-29,-28,49}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-33,-32,50}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-36,-35,51}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-39,-38,52}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

// definition of points  

xc[]={0.6485, 0.8344, 0.5792, 0.1956, 0.3827, 0.6659, 0.2962, 0.5114, 0.4872, 0.9080,
   0.9259, 0.1017, 0.7278, 0.3646, 0.3201, 0.4874, 0.1976, 0.2120, 0.7968, 0.0638, 0.4273, 0.2378, 0.5709, 0.9373, 0.6080, 0.8641, 0.0932, 0.2268,
   0.3120, 0.7786, 0.8999, 0.7395, 0.2873, 0.0785, 0.4670, 0.6894, 0.1606, 0.4695, 0.8840, 0.5890, 0.9415, 0.8300, 0.6244, 0.3991, 0.3154, 0.7139,
   0.0803, 0.7277, 0.3933, 0.8133, 0.5055, 0.5950, 0.7825, 0.1640, 0.68, 0.312, 0.1103, 0.5416};
yc[]={0.0638, 0.4694, 0.8845, 0.2001, 0.4497, 0.5680, 0.3262, 0.4196, 0.7551, 0.7901,
    0.3964, 0.1304, 0.3031, 0.8159, 0.7041, 0.1751, 0.5264, 0.4039, 0.5991, 0.7884, 0.6495, 0.8349, 0.6269, 0.2381, 0.4651, 0.1438, 0.2526, 0.6458,
    0.5405, 0.8729, 0.6537, 0.1762, 0.0807, 0.5212, 0.5345, 0.8067, 0.9217, 0.9080, 0.9303, 0.2139, 0.5243, 0.2776, 0.3363, 0.2969, 0.2017, 0.6793, 
    0.6609, 0.4366, 0.0923, 0.7366, 0.2957, 0.7493, 0.0647, 0.7427, 0.93, 0.928, 0.378, 0.07};

For i In {0:57}

t=t+1;
x0 = xc[i]*x;
y0 = yc[i]*y;
p1 = newp; Point(p1) = {x0, y0, 0.0 ,Lc2};
p2 = newp; Point(p2) = {x0, y0-R ,0.0 ,Lc2}; 
p3 = newp; Point(p3) = {x0+R, y0 ,0.0 ,Lc2};
p4 = newp; Point(p4) = {x0, y0+R ,0.0 ,Lc2}; 
p5 = newp; Point(p5) = {x0-R, y0 ,0.0 ,Lc2}; 

//define new circles

c1 = newreg; Circle(c1) = {p2,p1,p3};
c2 = newreg; Circle(c2) = {p3,p1,p4};
c3 = newreg; Circle(c3) = {p4,p1,p5};
c4 = newreg; Circle(c4) = {p5,p1,p2};
theloop[i] = newreg; Line Loop(theloop[i]) = {c1,c2,c3,c4}; 

surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {theloop[i]};

EndFor

// Surface definition
lineloop_matix = newreg;
Line Loop(lineloop_matix) = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,
27,28,29,30,31,32,33,34,35,36,37,38,39,40};

// define the surface for matrix
MaS=news;
Plane Surface(MaS) = {lineloop_matix,theloop[]};

//Volume of fiber and matrix ******************************************************
my_fiberV[] = Extrude {0.0 , 0.0 , z} {Surface{surface_edg[]};Layers{1};};
my_mtrixV[] = Extrude {0.0 , 0.0 , z} {Surface {MaS}; Layers{1};};



// Physical objects to applied BC **********************************

// Physical objects to applied material*********************
Physical Volume(1001) ={my_mtrixV[]};
Physical Volume(1002) ={my_fiberV[]};


//fix x displacement
Physical Surface(101) = {2303,595,2315,612,2327,629,2339};
//add x displacement0.0
Physical Surface(102) = {2223,493,2235,510,2247,527,2259};

//fix y displacement
Physical Surface(111) = {2183,442,2195,459,2207,476,2219};
Physical Surface(112) = {2263,544,2275,561,2287,578,2299};

//fix z displacement
Physical Surface(121) = {MaS,surface_edg[]};
Physical Surface(122) = {596,1444,1114,579,1862,938,1466,562,696,1422,1774,828,1092,1136,960,1246,1818,1070,1664,
                         762,1180,806,1400,740,1268,1026,1004,1378,1884,613,1224,718,784,1620,1598,1752,982,1510,
                         1576,652,1906,1708,1356,443,894,630,460,545,1840,1488,528,1290,1422,850,1730,1642,1312,511,
                         1048,1532,674,872,1686,916,1554,1158,1334,1202,1796,477,494,3268};

//define transfinite mesh***************************************
//Transfinite Line {l3,l4} = 2;










