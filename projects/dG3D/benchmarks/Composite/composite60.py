#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch beam problem with a python script

# material law
lawnumCF = 1 # unique number of law
rhoCF   = 1750.
youngCF    = 40.e9
youngACF   = 230.e9;
nuCF       = 0.2 #
nu_minorCF = 0.256*youngCF/youngACF 
GACF       = 24.e9
Ax       = 0.; #direction of anisotropy
Ay       = 0.;
Az       = 1.;

lawnumEP = 2 
rhoEP   = 1000.
youngEP = 3.2e9
nuEP    = 0.3
sy0EP   = 25.e6
hEP     = 7.1e9


lawcnumCF =3 
GcCF    = 100.  # fracture energy [J/m2]
sigmacCF=3600.e6 # fracture limit in tension [Pa]
beta =0.87 # ratio KII/KI
mu =-1. # no fracture in compression 0.41 # friction coefficient ??

lawcnumEP = 4
GcEP   =  78.
sigmacEP = 83.e6

lawcnumInt = 5
GcInt   = 100.
sigmaInt = 65.e6

fsmin = 0.8
fsmax = 1.2

# geometry
meshfile="c50_2-48.msh" # name of mesh file
#meshfile="composite.msh" 

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 2 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1000   # number of step (used only if soltype=1)
ftime =1.e-5   # Final time (used only if soltype=1)
tol=1.e-5   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=5000 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 1 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1  = 30

#  compute solution and BC (given directly to the solver
# creation of law
law1EP = J2LinearDG3DMaterialLaw(lawnumEP,rhoEP,youngEP,nuEP,sy0EP,hEP)
law2EP = LinearCohesive3DLaw(lawcnumEP,GcEP,sigmacEP,beta,mu,fsmin,fsmax)
law3EP = FractureByCohesive3DLaw(6,lawnumEP,lawcnumEP)

law1CF = TransverseIsotropicDG3DMaterialLaw(lawnumCF,rhoCF,youngCF,nuCF,youngACF,GACF,nu_minorCF,Ax,Ay,Az)
law2CF = LinearCohesive3DLaw(lawcnumCF,GcCF,sigmacCF,beta,mu,fsmin,fsmax)
law3CF = FractureByCohesive3DLaw(7,lawnumCF,lawcnumCF)

law2Int = LinearCohesive3DLaw(lawcnumInt,GcInt,sigmaInt,beta,mu,fsmin,fsmax)

# creation of ElasticField
nfieldEP = 61 # number of the field (physical number of surface)
myfieldEP = dG3DDomain(1000,nfieldEP,space1,6,fullDg)
nfieldCF = 62
myfieldCF = dG3DDomain(1000,nfieldCF,space1,7,fullDg)
#myfieldEP.matrixByPerturbation(0,1,1,1e-15)
myfieldEP.stabilityParameters(beta1)
myfieldCF.stabilityParameters(beta1)
#in // we need to create the ininterdomain after adding the domains
myinterfield = interDomainBetween3D(1000,myfieldEP,myfieldCF,lawcnumInt)
myinterfield.stabilityParameters(beta1)


# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfieldEP)
mysolver.addDomain(myfieldCF)
mysolver.addMaterialLaw(law1EP)
mysolver.addMaterialLaw(law2EP)
mysolver.addMaterialLaw(law3EP)
mysolver.addMaterialLaw(law1CF)
mysolver.addMaterialLaw(law2CF)
mysolver.addMaterialLaw(law3CF)
mysolver.addDomain(myinterfield)
mysolver.addMaterialLaw(law2Int)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
#mysolver.snlData(nstep,ftime,tol)
mysolver.explicitSpectralRadius(ftime,0.2,0.)
#mysolver.dynamicRelaxation(0.1, ftime, 1.e-3,2)
mysolver.explicitTimeStepEvaluation(nstep)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.energyComputation(nstepArch/10)
#mysolver.snlManageTimeStep(50,2, 2., 15);
# BC
mysolver.displacementBC("Face",101,0,0.)
mysolver.displacementBC("Face",111,1,0.)
mysolver.displacementBC("Volume",nfieldEP,2,0.)
mysolver.displacementBC("Volume",nfieldCF,2,0.)
#mysolver.constraintBC("Face",112,1)

d = 3.
#cyclicFunction1=cycleFunctionTime(ftime/2., d*0.8, 3.*ftime/4., d*0.9,ftime, d);

mysolver.displacementBC("Face",102,0,d)


mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 102, 0, nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 101, 0, nstepArch)

mysolver.solve()

