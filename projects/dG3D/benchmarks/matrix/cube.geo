// Test case a SCB with a vertical load at its free extremity
// Size

//definition of unit
mm = 1e-03;

// volum fraction

y = 0.025*mm; 
x = 0.1*mm;
z = 0.1*mm;


// Characteristic length
Lc1=mm/2.5;

// definition of points
Point(1) = { 0.0 , 0.0 , 0.0 , Lc1};
Point(2) = {  x  , 0.0 , 0.0 , Lc1};
Point(3) = {  x  ,  y  , 0.0 , Lc1};
Point(4) = { 0.0 ,  y  , 0.0 , Lc1};
Point(5) = { 0.0 , 0.0 , z , Lc1};
Point(6) = {  x  , 0.0 , z , Lc1};
Point(7) = {  x  ,  y  , z , Lc1};
Point(8) = { 0.0 ,  y  , z , Lc1};

// Line between points
Line(51) = {1,2};
Line(52) = {2,3};
Line(53) = {3,4};
Line(54) = {4,1};
Line(55) = {5,6};
Line(56) = {6,7};
Line(57) = {7,8};
Line(58) = {8,5};
Line(59) = {1,5};
Line(60)= {2,6};
Line(61)= {3,7};
Line(62)= {4,8};

// Surface definition
Line Loop(100) = {51,52,53,54};
Line Loop(200) = {55,56,57,58};
Line Loop(300) = {51,60,-55,-59};
Line Loop(400) = {52,61,-56,-60};
Line Loop(500) = {53,62,-57,-61};
Line Loop(600) = {54,59,-58,-62};

Plane Surface(100) = {100};
Plane Surface(200) = {200};
Plane Surface(300) = {300};
Plane Surface(400) = {400};
Plane Surface(500) = {500};
Plane Surface(600) = {600};

//VOlume

Surface Loop(7) = {100,200,300,400,500,600};
Volume(10) = {7};

// Physical objects to applied BC and material
Physical Surface(121) = {100};
Physical Surface(122) = {200};
Physical Surface(111) = {300};
Physical Surface(102) = {400};
Physical Surface(112) = {500};
Physical Surface(101) = {600};

Physical Volume(10) ={10};

// define transfinite mesh
Transfinite Line {51,59,53,60,55,61,57,62} = 7;
Transfinite Line {52,54,56,58} = 2;
Transfinite Surface {100,200,300,400,500,600} ;
//Recombine Surface {100,200,300,400,500,600} ;
Transfinite Volume {10};

