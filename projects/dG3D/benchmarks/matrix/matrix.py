#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch beam problem with a python script

# material law
lawnumEP = 2 
rhoEP   = 1000.
youngEP = 2.89e9
nuEP    = 0.3
sy0EP   = 35.e6
hEP     = 73.e6*60

lawcnumEP = 4
GcEP   =  780.
sigmacEP = 60e6

beta =0.87 # ratio KII/KI
mu =0.41 # friction coefficient ??

fsmin = 0.9
fsmax = 1.1

# geometry
meshfile="cube.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 2 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 100   # number of step (used only if soltype=1)
ftime =2.e-3   # Final time (used only if soltype=1)
tol=1.e-5   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=100 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 1 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1  = 30

#  compute solution and BC (given directly to the solver
# creation of law
law1EP = J2LinearDG3DMaterialLaw(lawnumEP,rhoEP,youngEP,nuEP,sy0EP,hEP)
law2EP = LinearCohesive3DLaw(lawcnumEP,GcEP,sigmacEP,beta,mu,fsmin,fsmax)
law3EP = FractureByCohesive3DLaw(6,lawnumEP,lawcnumEP)

# creation of ElasticField
nfieldEP = 10 # number of the field (physical number of surface)
myfieldEP = dG3DDomain(1000,nfieldEP,space1,6,fullDg)
#myfieldEP.matrixByPerturbation(0,1,1,1e-15)
myfieldEP.stabilityParameters(beta1)


# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfieldEP)
mysolver.addMaterialLaw(law1EP)
mysolver.addMaterialLaw(law2EP)
mysolver.addMaterialLaw(law3EP)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
#mysolver.snlData(nstep,ftime,tol)
#mysolver.explicitSpectralRadius(ftime,0.8,1.)
mysolver.dynamicRelaxation(0.8, ftime, 1.e-3,2)
mysolver.explicitTimeStepEvaluation(nstep)
mysolver.stepBetweenArchiving(nstepArch)
#mysolver.snlManageTimeStep(50,2, 2., 15);
# BC
mysolver.displacementBC("Face",121,2,0.)
mysolver.displacementBC("Face",102,0,0.)
mysolver.displacementBC("Face",111,1,0.)
mysolver.displacementBC("Face",112,1,0.)
mysolver.constraintBC("Face",101,0)

d = 0.02
#cyclicFunction1=cycleFunctionTime(ftime/2., d*0.7, 3.*ftime/4., d*0.5,ftime, d);

mysolver.displacementBC("Face",122,2,d)

mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 122, 2, nstepArch)

mysolver.solve()

