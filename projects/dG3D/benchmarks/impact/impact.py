#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch beam problem with a python script

# material law
lawnum = 1 # unique number of law
rho   = 2700
young = 72.e9
nu    = 0.233 
# 5083 H111
#sy0   = 148e6
#h     = 850e6
# 6082 T6
sy0   = 265e6
h     = 850e6
# 7076 T6
#sy0   = 445e6
#h     = 850e6
# geometry
meshfile="impactSmall.msh" # name of mesh file

explicit = 0 # 1 explicit 0 implicit
# solver
soltype = 1
if explicit:
    soltype = 2
    ftime = 1.e-6
    nstep = 50
    nstepArch=1000
else:
    ftime = 1
    nstep = 1000
    nstepArch = 10
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
#for implicit
tol=1.e-4   # relative tolerance for NR scheme (used only if soltype=1)
fullDg = 0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)


#  compute solution and BC (given directly to the solver
# creation of law
law1 = J2LinearDG3DMaterialLaw(lawnum,rho,young,nu,sy0,h)

# creation of ElasticField
nfield = 51 # number of the field (physical number of surface)
myfield1 = dG3DDomain(1000,nfield,space1,lawnum,fullDg)
myfield1.stabilityParameters(40)
#myfield1.matrixByPerturbation(1,1,1,1e-8)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
if explicit:
    mysolver.explicitTimeStepEvaluation(nstep)
    mysolver.dynamicRelaxation(0.1, ftime, 1.e-3,2)
else:
    mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.energyComputation(nstepArch)

# BC
mysolver.displacementBC("Face",101,1,0.)
mysolver.displacementBC("Face",201,0,0.)
mysolver.displacementBC("Face",201,1,0.)
mysolver.displacementBC("Face",201,2,0.)
#mysolver.displacementBC("Face",102,0,-0.005)
F = -1.3e9
if explicit:
    cyclicFunction1=cycleFunctionTime(ftime/2., F, 3*ftime/4., F,ftime, F);
else:
    cyclicFunction1=cycleFunctionTime(ftime/2., F/2, 3*ftime/4., 3.*F/4., ftime, F);
mysolver.forceBC("Face",102,0,cyclicFunction1);

mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)

mysolver.archivingForceOnPhysicalGroup("Face", 102, 0,nstepArch)

mysolver.solve()

