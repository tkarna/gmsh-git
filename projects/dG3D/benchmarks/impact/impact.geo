//impactor

mm = 1.e-3;

Ri  = 2*mm;    // radius of impactor
hf  = 4*mm;    // height of the flange
hb  = 22*mm;     // height of the block
tf  = 2*mm;     // thickness of the flange
L1f = 4*mm;       // first part of the length of the flange
L2f = 2*L1f;    // second part of the length of the flange
tb = 19*mm;     //thickness of the whole block
rr = 0.5*mm;      // radius
t1 = tb-tf-rr;  //thickness of the extruded part


CLRi = 0.5*mm;    //characteristic length at impactor
CLL1 = 1.*mm;   //characteristic length at mid length
CLL2 = 1.2*mm;   //characteristic length at full length
CLep = 0.5*mm;    //characteristic length at radius

//mid face
Point (1)  = { 0.0 , 0.0 , 0.0,   CLRi};
Point (2)  = { 0.0 , Ri ,  0.0,   CLRi};
Point (28) = { 0.0 , Ri/Sqrt(2) ,  -Ri/Sqrt(2),   CLRi};
Point (3) = { 0.0 , 0.0,  -Ri,   CLRi};
Point (4) = { 0.0 , L1f,  0.0 ,  CLL1};
Point (5) = { 0.0 , L1f,  -hf ,  CLep};
Point (6) = { 0.0 , 0.0,  -hf ,  CLep};
Point (7) = { 0.0 , L1f+L2f,  0.0 ,  CLL2};
Point (8) = { 0.0 , L1f+L2f,  -hf ,  CLL2};
Point (9) = {  rr*(1.-Sqrt(2)/2.), L1f+L2f,  -hf-rr/Sqrt(2),  CLep};
Point (10) = {  rr*(1.-Sqrt(2)/2.), L1f,  -hf-rr/Sqrt(2),  CLep};
Point (11) = {  rr*(1.-Sqrt(2)/2.), 0,  -hf-rr/Sqrt(2),  CLep};
Point (12) = { rr , L1f+L2f,  -hf-rr,  CLep};
Point (13) = { rr , L1f,  -hf-rr,  CLep};
Point (14) = { rr , 0,  -hf-rr,  CLep};
Point (15) = { rr , L1f+L2f,  -hf, CLL2}; 
Point (16) = { rr , L1f,  -hf, CLL2}; 
Point (17) = { rr , 0.,  -hf, CLep}; 
Point (18) = { rr, L1f+L2f,  -hf-hb,  CLL2};
Point (19) = { rr, L1f,  -hf-hb,  CLL2};
Point (20) = { rr, 0,  -hf-hb,  CLL2};


Line(1)    = {1, 2};
Circle (2) = {2, 1, 28};
Line(3)    = {3, 1};
Line(4)    = {2, 4};
Line(5)    = {4, 5};
Line(6)    = {5, 6};
Line(7)    = {6, 3};
Line(8)    = {4, 7};
Line(9)    = {7, 8};
Line(10)   = {8, 5};
Circle(11)  = {8, 15, 9};
Line(12)   = {9, 10};
Circle(13)  = {10, 16, 5};
Line(14)   = {10, 11};
Circle(15)  = {11, 17, 6};
Circle(16)  = {9, 15, 12};
Line(17)   = {12, 13};
Circle(18)  = {13, 16, 10};
Line(19)   = {13, 14};
Circle(20)  = {14, 17, 11};
Line(21)   = {5,  28};
Circle(22) = {28, 1, 3};
Line(23)   = {28,1};

Line(24)   = {12, 18};
Line(25)   = {18, 19};
Line(26)   = {19, 13};
Line(27)   = {19, 20};
Line(28)   = {20, 14};

Line Loop(1) = {1,2,23};
Line Loop(2) = {22,3,-23};
Line Loop(3) = {4,5,21,-2};
Line Loop(4) = {6,7,-22,-21};
Line Loop(5) = {8,9,10,-5};

Plane Surface(1) = {1};
Plane Surface(2) = {2};
Plane Surface(3) = {3};
Plane Surface(4) = {4};
Plane Surface(5) = {5};

Line Loop(6) = {-6, -13, 14, 15};
Line Loop(7) = {-10, 11, 12, 13};
Line Loop(8) = {-14, -18,19, 20};
Line Loop(9) = {-12, 16,17, 18};

Ruled Surface(6) = {6};
Ruled Surface(7) = {7};
Ruled Surface(8) = {8};
Ruled Surface(9) = {9};


//back face
Point (101) = { -tf , 0.0 , 0.0,   CLRi};
Point (102) = { -tf , Ri ,  0.0,   CLRi};
Point (128) = { -tf , Ri/Sqrt(2) ,  -Ri/Sqrt(2),   CLRi};
Point (103) = { -tf , 0.0,  -Ri,   CLRi};
Point (104) = { -tf , L1f,  0.0 ,  CLL1};
Point (105) = { -tf , L1f,  -hf ,  CLL1};
Point (106) = { -tf , 0.0,  -hf ,  CLep};
Point (107) = { -tf , L1f+L2f,  0.0 ,  CLL2};
Point (108) = { -tf , L1f+L2f,  -hf ,  CLL2};
Point (109) = { -tf , L1f+L2f,  -hf-4*rr,  CLL1};
Point (110) = { -tf , L1f,  -hf-4*rr,  CLL1};
Point (111) = { -tf , 0,  -hf-4*rr,  CLL1};
Point (112) = { -tf , L1f+L2f,  -hf-8*rr,  CLL1};
Point (113) = { -tf , L1f,  -hf-8*rr,  CLL1};
Point (114) = { -tf , 0,  -hf-8*rr,  CLL1};
Point (118) = { -tf , L1f+L2f,  -hf-hb,  CLL2};
Point (119) = { -tf , L1f,  -hf-hb,  CLL2};
Point (120) = { -tf , 0,  -hf-hb,  CLL2};

Line(101)    = {101, 102};
Circle (102) = {102, 101, 128};
Line(103)    = {103, 101};
Line(104)    = {102, 104};
Line(105)    = {104, 105};
Line(106)    = {105, 106};
Line(107)    = {106, 103};
Line(108)    = {104, 107};
Line(109)    = {107, 108};
Line(110)   = {108, 105};
Line(111)   = {108, 109};
Line(112)   = {109, 110};
Line(113)   = {110, 105};
Line(114)   = {110, 111};
Line(115)   = {111, 106};
Line(116)   = {109, 112};
Line(117)   = {112, 113};
Line(118)   = {113, 110};
Line(119)   = {113, 114};
Line(120)   = {114, 111};
Line(121)   = {105,  128};
Circle(122) = {128, 101, 103};
Line(123)   = {128,101};
Line(124)   = {112,118};
Line(125)   = {118,119};
Line(126)   = {119,113};
Line(127)   = {119,120};
Line(128)   = {120,114};


Line Loop(101) = {101,102,123};
Line Loop(102) = {122,103,-123};
Line Loop(103) = {104,105,121,-102};
Line Loop(104) = {106,107,-122,-121};
Line Loop(105) = {108,109,110,-105};

Plane Surface(101) = {101};
Plane Surface(102) = {102};
Plane Surface(103) = {103};
Plane Surface(104) = {104};
Plane Surface(105) = {105};


Line Loop(106) = {-106, -113, 114, 115};
Line Loop(107) = {-110, 111, 112, 113};
Line Loop(108) = {-114, -118,119, 120};
Line Loop(109) = {-112, 116,117, 118};

Plane Surface(106) = {106};
Plane Surface(107) = {107};
Plane Surface(108) = {108};
Plane Surface(109) = {109};

//side back to mid
For i In {1:14}
  Line(1000+i) = {i,100+i};
EndFor
For i In {18:20}
  Line(1000+i) = {i,100+i};
EndFor
Line(1028) = {28,128};

Line Loop(1001) = {1,1002,-101,-1001};
Line Loop(1002) = {2,1028,-102,-1002};
Line Loop(1003) = {103,-1001,-3,1003};
Line Loop(1004) = {4, 1004, -104,-1002};
Line Loop(1005) = {5, 1005, -105,-1004};
Line Loop(1006) = {21, 1028, -121,-1005};
Line Loop(1007) = {6, 1006, -106,-1005};
Line Loop(1008) = {-7, 1006, 107,-1003};
Line Loop(1010) = {22,1003,-122,-1028};
Line Loop(1011) = {23,1001,-123,-1028};
Line Loop(1012) = {9, 1008, -109, -1007};
Line Loop(1013) = {10, 1005, -110, -1008};
Line Loop(1014) = {8, 1007, -108, -1004};


Plane Surface(1001) = {1001};
Ruled Surface(1002) = {1002};
Plane Surface(1003) = {1003};
Plane Surface(1004) = {1004};
Plane Surface(1005) = {1005};
Plane Surface(1006) = {1006};
Plane Surface(1007) = {1007};
Plane Surface(1008) = {1008};
Ruled Surface(1010) = {1010};
Plane Surface(1011) = {1011};
Plane Surface(1012) = {1012};
Plane Surface(1013) = {1013};
Plane Surface(1014) = {1014};

Line Loop (1021) = {1008,111,-1009,-11};
Line Loop (1022) = {1005,-113,-1010,13};
Line Loop (1023) = {1006,-115,-1011,15};
Line Loop (1024) = {12,1010,-112,-1009};
Line Loop (1025) = {14,1011, -114, -1010};
Line Loop (1026) = {17,1013, -117, -1012};
Line Loop (1027) = {19,1014, -119, -1013};
Line Loop (1028) = {1009,116, -1012, -16};
Line Loop (1029) = {1010,-118, -1013, 18};
Line Loop (1030) = {1011,-120, -1014, 20};

Line Loop(1031) = {-1014, -28, 1020, 128};
Line Loop(1032) = {-1013, -26, 1019, 126};
Line Loop(1033) = {1012, -24, -1018, 124};
Line Loop(1034) = {25, 26, -17, 24};
Line Loop(1035) = {125, 126, -117, 124};
Line Loop(1036) = {27, 28, -19, -26};
Line Loop(1037) = {127, 128, -119, -126};
Line Loop(1038) = {25, 1019,-125,-1018};
Line Loop(1039) = {27, 1020,-127,-1019};


//Front face
Point (212) = { rr+t1 , L1f+L2f,  -hf-rr,  CLL2};
Point (213) = { rr+t1 , L1f,  -hf-rr,  CLL2};
Point (214) = { rr+t1 , 0,  -hf-rr,  CLL2};
Point (218) = { rr+t1, L1f+L2f,  -hf-hb,  CLL2};
Point (219) = { rr+t1, L1f,  -hf-hb,  CLL2};
Point (220) = { rr+t1, 0,  -hf-hb,  CLL2};

Line(217)   = {212, 213};
Line(219)   = {213, 214};
Line(224)   = {212, 218};
Line(225)   = {218, 219};
Line(226)   = {219, 213};
Line(227)   = {219, 220};
Line(228)   = {220, 214};

For i In {12:14}
  Line(1200+i) = {i,200+i};
EndFor
For i In {18:20}
  Line(1200+i) = {i,200+i};
EndFor

Line Loop(1040) = {1214,-228,-1220,28};
Line Loop(1041) = {1213,-226,-1219,26};
Line Loop(1042) = {1212,224,-1218,-24};
Line Loop(1043) = {225, 226, -217, 224};
Line Loop(1044) = {227, 228, -219, -226};
Line Loop(1045) = {1212,217,-1213,-17};
Line Loop(1046) = {1213,219,-1214,-19};
Line Loop(1047) = {1218,225,-1219,-25};
Line Loop(1048) = {1219,227,-1220,-27};

For i In {1021:1048}
  Plane Surface (i) = {i};
EndFor

Surface Loop(1) = {1011,1,101,1002,1001};
Surface Loop(2) = {1011,102,2,1010,1003};
Surface Loop(3) = {1004, 1005, 1006, 1002,3,103};
Surface Loop(4) = {1006, 1007, 1008, 1010,4,104};
Surface Loop(5) = {1005, 1014, 1012, 1013,5,105};

Surface Loop(6) = {1023,6,1022,106,1007,1025};
Surface Loop(7) = {1030,8,1029,108,1025,1027};
Surface Loop(8) = {1022,7,1021,107,1013,1024};
Surface Loop(9) = {1029,9,1028,109,1024,1026};

Surface Loop(10) = {1033, 1034, 1032, 1035, 1038, 1026};
Surface Loop(11) = {1032, 1036, 1031, 1037, 1039, 1027};

Surface Loop(12) = {1040,1044,1041,1036,1048,1046};
Surface Loop(13) = {1041,1043,1042,1034,1047,1045};

//Symmetry face
Physical Surface(101) = {1003,1008,1023,1030,1031,1040};

//fixed face
Physical Surface(201) = {1012,1021,1028,1033,1042};

//Impacted face
Physical Surface(102) = {1,2};


Volume(1) = {1};
//Transfinite Volume {1};
Volume(2) = {2};
//Transfinite Volume {2};
Volume(3) = {3};
Volume(4) = {4};
Volume(5) = {5};
Volume(6) = {6};
Volume(7) = {7};
Volume(8) = {8};
Volume(9) = {9};
Volume(10) = {10};
Volume(11) = {11};
Volume(12) = {12};
Volume(13) = {13};

Physical Volume (51) = {1,2,3,4,5,6,7,8,9,10,11,12,13};



