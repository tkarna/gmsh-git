// creat a cube with fiber

//defination of unit
mm = 0.001;

// volum fraction

f=0.525;

// characteristic size for fibe
R= 0.0052*mm;                          // fiber radius 5.2e-6 m
ly=2.0*R*Sqrt(Pi/(2.0*Sqrt(3.0)*f));  // distance between fibers' centers
lx=ly*Sqrt(3.0)/2.0;

// characteristic Size for the volum
x = 2*ly;
y = 2*lx;
z = R/4.0;


// Characteristic length
Lc1=lx/4.0/2.5;
Lc2=Lc1;
//0.03*mm/2.2;

// definition of points***********************************************
Point(1) = { 0.0 , 0.0 , 0.0 , Lc1};
Point(2) = {  x  , 0.0 , 0.0 , Lc1};
Point(3) = {  x  ,  y  , 0.0 , Lc1};
Point(4) = { 0.0 ,  y  , 0.0 , Lc1};

// Line between points
Line(1) = {1,2};
Line(2) = {3,4};


//define line Loop list for fibers
t=0;
//fiber*************************************************************

//fiber1
x1=0.6*ly;
y1=0.5*lx;

//define new point
p1 = newp; Point(p1) = {x1,y1 ,0.0 ,Lc2};
p2 = newp; Point(p2) = {x1-R,y1 ,0.0 ,Lc2};
p3 = newp; Point(p3) = {x1,y1-R ,0.0 ,Lc2};
p4 = newp; Point(p4) = {x1+R,y1 ,0.0 ,Lc2};
p5 = newp; Point(p5) = {x1,y1+R ,0.0 ,Lc2};

//define new circles

c1 = newreg; Circle(c1) = {p2,p1,p3};
c2 = newreg; Circle(c2) = {p3,p1,p4};
c3 = newreg; Circle(c3) = {p4,p1,p5};
c4 = newreg; Circle(c4) = {p5,p1,p2};


theloop[t] = newreg; Line Loop(theloop[t]) = {c1,c2,c3,c4}; 

thesurface[t] = news;
Plane Surface( thesurface[t]) = {theloop[t]};

//****************************************************************
t=t+1;

//fiber2
x1=1.45*ly;
y1=0.75*lx;

//define new point
p1 = newp; Point(p1) = {x1,y1 ,0.0 ,Lc2};
p2 = newp; Point(p2) = {x1-R,y1 ,0.0 ,Lc2};
p3 = newp; Point(p3) = {x1,y1-R ,0.0 ,Lc2};
p4 = newp; Point(p4) = {x1+R,y1 ,0.0 ,Lc2};
p5 = newp; Point(p5) = {x1,y1+R ,0.0 ,Lc2};


//define new circles

c1 = newreg; Circle(c1) = {p2,p1,p3};
c2 = newreg; Circle(c2) = {p3,p1,p4};
c3 = newreg; Circle(c3) = {p4,p1,p5};
c4 = newreg; Circle(c4) = {p5,p1,p2};


theloop[t] = newreg; Line Loop(theloop[t]) = {c1,c2,c3,c4}; 

thesurface[t] = news;
Plane Surface( thesurface[t]) = {theloop[t]};

//****************************************************************
t=t+1;

//fiber3
x1=0.85*ly;
y1=1.45*lx;

//define new point
p1 = newp; Point(p1) = {x1,y1 ,0.0 ,Lc2};
p2 = newp; Point(p2) = {x1-R,y1 ,0.0 ,Lc2};
p3 = newp; Point(p3) = {x1,y1-R ,0.0 ,Lc2};
p4 = newp; Point(p4) = {x1+R,y1 ,0.0 ,Lc2};
p5 = newp; Point(p5) = {x1,y1+R ,0.0 ,Lc2};


//define new circles

c1 = newreg; Circle(c1) = {p2,p1,p3};
c2 = newreg; Circle(c2) = {p3,p1,p4};
c3 = newreg; Circle(c3) = {p4,p1,p5};
c4 = newreg; Circle(c4) = {p5,p1,p2};


theloop[t] = newreg; Line Loop(theloop[t]) = {c1,c2,c3,c4}; 

thesurface[t] = news;
Plane Surface( thesurface[t]) = {theloop[t]};


//***********************************************************
// define new point

t=0;
x1=0.0;
y1=1.15*lx;


p1 = newp; Point(p1) = {x1, y1 ,0.0 ,Lc2};
p2 = newp; Point(p2) = {0.0, y1-R ,0.0 ,Lc2}; 
p3 = newp; Point(p3) = { R , y1 ,0.0 ,Lc2};
p4 = newp; Point(p4) = {0.0, y1+R ,0.0 ,Lc2}; 

//define new circles

c1 = newreg; Circle(c1) = {p2,p1,p3};
c2 = newreg; Circle(c2) = {p3,p1,p4};
l3 = newreg; Line (l3) = {p4,p2};


loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {c1,c2,l3}; 

surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};



//***********************************************************
// define new point

j=1;

t=t+1;

p1 = newp; Point(p1) = {x, j*lx+lx/2.0 ,0.0 ,Lc2};
p2 = newp; Point(p2) = {x, j*lx+lx/2.0-R ,0.0 ,Lc2}; 
p3 = newp; Point(p3) = {x-R, j*lx+lx/2.0 ,0.0 ,Lc2};
p4 = newp; Point(p4) = {x, j*lx+lx/2.0+R ,0.0 ,Lc2}; 

//define new circles

c1 = newreg; Circle(c1) = {p4,p1,p3};
c2 = newreg; Circle(c2) = {p3,p1,p2};
l3 = newreg; Line (l3) = {p2,p4};


loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {c1,c2,l3}; 

surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};


//define the line loop for matrix surface****************************

l1 = newreg; Line (l1) = {2,25};
l2 = newreg; Line (l2) = {27,3};
l3 = newreg; Line (l3) = {4,23};
l4 = newreg; Line (l4) = {21,1};


// Surface definition
lineloop_matix = newreg;

Line Loop(lineloop_matix) = {1,l1,-27,-26,l2,2,l3,-22,-21,l4};

// define the surface for matrix
MaS=news;
Plane Surface(MaS) = {lineloop_matix,theloop[]};


//VOlume of fiber and matrix ******************************************************
my_fiberV[] = Extrude {0.0 , 0.0 , z} {Surface{thesurface[],surface_edg[]};}; Layers{1};};
my_mtrixV[] = Extrude {0.0 , 0.0 , z} {Surface {MaS};}; Layers{1};



// Physical objects to applied BC **********************************
//fix x displacement
Physical Surface(101) = {199,118,187};

//add x displacement
Physical Surface(102) = {167,135,179};

//fix y displacement
Physical Surface(111) = {163};
Physical Surface(112) = {183};

//fix z displacement
Physical Surface(121) = {MaS,thesurface[],surface_edg[]};
Physical Surface(122) = {248,80,58,102,119,136};



// Physical objects to applied material*********************
Physical Volume(1001) ={my_mtrixV[]};
Physical Volume(1002) ={my_fiberV[]};


//define transfinite mesh***************************************
//Transfinite Line {l3,l4} = 2;










