cl = 0.5;
Point( 1 ) = { -1.500000 ,  1.500000 , 0, cl};
Point( 2 ) = { -1.500000 ,  1.019117 , 0, cl};
Point( 3 ) = { -0.168761 ,  -0.360342 , 0, cl};
Point( 4 ) = { 0.685644 ,  -0.955796 , 0, cl};
Point( 5 ) = { 1.500000 ,  -0.638387 , 0, cl};
Point( 6 ) = { 1.500000 ,  1.044271 , 0, cl};
Point( 7 ) = { -0.208460 ,  0.215856 , 0, cl};
Point( 8 ) = { 1.500000 ,  -1.500000 , 0, cl};
Point( 9 ) = { 0.572515 ,  -1.500000 , 0, cl};
Point( 10 ) = { -1.006935 ,  -1.500000 , 0, cl};
Point( 11 ) = { 1.500000 ,  1.500000 , 0, cl};
Point( 12 ) = { -1.500000 ,  -1.500000 , 0, cl};
Line( 1 ) = { 3 ,  4 };
Line( 2 ) = { 4 ,  5 };
Line( 3 ) = { 5 ,  6 };
Line( 4 ) = { 6 ,  7 };
Line( 5 ) = { 7 ,  3 };
Line( 6 ) = { 9 ,  10 };
Line( 7 ) = { 10 ,  3 };
Line( 8 ) = { 3 ,  4 };
Line( 9 ) = { 4 ,  9 };
Line( 10 ) = { 9 ,  4 };
Line( 11 ) = { 4 ,  5 };
Line( 12 ) = { 5 ,  8 };
Line( 13 ) = { 8 ,  9 };
Line( 14 ) = { 1 ,  11 };
Line( 15 ) = { 11 ,  6 };
Line( 16 ) = { 6 ,  7 };
Line( 17 ) = { 7 ,  2 };
Line( 18 ) = { 2 ,  1 };
Line( 19 ) = { 12 ,  10 };
Line( 20 ) = { 10 ,  3 };
Line( 21 ) = { 3 ,  7 };
Line( 22 ) = { 7 ,  2 };
Line( 23 ) = { 2 ,  12 };
Line Loop( 1 ) = {1 , 2 , 3 , 4 , 5};
Plane Surface( 1 ) = { 1 };
Line Loop( 2 ) = {6 , 7 , 8 , 9};
Plane Surface( 2 ) = { 2 };
Line Loop( 3 ) = {10 , 11 , 12 , 13};
Plane Surface( 3 ) = { 3 };
Line Loop( 4 ) = {14 , 15 , 16 , 17 , 18};
Plane Surface( 4 ) = { 4 };
Line Loop( 5 ) = {19 , 20 , 21 , 22 , 23};
Plane Surface( 5 ) = { 5 };
Extrude {0,0, 0.600000 } {
 Surface{ 1 };
 }
Extrude {0,0, 0.600000 } {
 Surface{ 2 };
 }
Extrude {0,0, 0.600000 } {
 Surface{ 3 };
 }
Extrude {0,0, 0.600000 } {
 Surface{ 4 };
 }
Extrude {0,0, 0.600000 } {
 Surface{ 5 };
 }

Physical Volume(10) ={1,2,3,4,5};


Physical Surface(110) = {120,147};
Physical Surface(120) = {131,59,93};
Physical Surface(101) = {108,41,89};
Physical Surface(102) = {104};
Physical Surface(130) = {1,2,3,4,5};
Physical Surface(103) = {121,148,72,94,50};
Show "*";
