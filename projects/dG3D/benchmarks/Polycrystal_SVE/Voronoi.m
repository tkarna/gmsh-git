%   Copyright (C) 2006 Antoine Jerusalem
clear all;

% CREATION OF THE VORONOI DIAGRAM

% Inputs
%  GrainSize = 1.;
GrainSize = 1.5;
h_x = 3.0; % Length of the grid along x
h_y = 3.0; % Length of the grid 4long y
h_z = min(h_x,h_y)/h_x;
%  printf('%f',h_z);
perturbation_max = 0.25; % Perturbation in term of unit of grid subdivision, do NOT put it at 0!
dist2bound=  0; % h/(2*(n-1)); % Distance min between the boundary and the seeds
mesh_size = 0.2; % Check manually that it's not bigger than the grid subdivisions! (h/(n-1)) and bigger than 0!
factor_mesh = 1.;
FlagCheckLength = 1;

% Automated second parameters and initialization

n_x = ceil(h_x/GrainSize)+2; % Number of horizontal grid points (along x) (the +2 ensures that outside seeds perturbed can be in)
pertu_x = perturbation_max * GrainSize;
pertu_y = perturbation_max * GrainSize * 2/sqrt(3);
Voronoi_polygone_length = 0;
tolerance_1 = mesh_size * 1.e-12;
tolerance_2 = mesh_size * 1.e-5;
n_y = 1+ceil( (n_x-1) * h_y/h_x * 2/sqrt(3) ) +2; %(the +2 ensures that outside seeds perturbed can be in)
numberOfGrains = floor(h_x*h_y/GrainSize^2);
printf('%i\n',numberOfGrains);
numberOfGrainsTemp = 0;

%%%%%%%%%%%% DO NOT CHANGE THESE %%%%%%%%%%%%
FlagCheckGrainSize = 0;
FlagEraseSeeds = 0; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ( mod(n_y,2) == 0 ) % n_y is even
    nb_seeds = n_x*n_y - n_y/2;
else % n_y is odd
    nb_seeds = n_x*n_y - (n_y-1)/2;
end
if (FlagEraseSeeds == 1)
    proba_nothing = (nb_seeds-numberOfGrains)/(nb_seeds); % Probability to have no point on the grid
else
    proba_nothing = 0;
end

% Condition to keep the Voronoi diagram based on having polygons sides > mesh size
while ((Voronoi_polygone_length < mesh_size) | (numberOfGrainsTemp ~= numberOfGrains))

    clear C_new;
    clear C_new_temp_1;
    clear C_new_temp_2;
    
    % Create an initial grid with perturbation and out-of-the-box points (the ones we will skip)
    grid = zeros(n_x*n_y,2);
    for i=1:n_x
        for j=1:n_y
          if (rand > proba_nothing)      
            grid( (i-1)*n_y+j , 1 ) = -h_x/2 + (i-2)*GrainSize + (2*rand-1)*pertu_x + mod(j,2)*GrainSize/2.;
            grid( (i-1)*n_y+j , 2 ) = -h_y/2 + (j-2)*GrainSize + (2*rand-1)*pertu_y;
          else
            grid( (i-1)*n_y+j , 1 ) = 2*max(h_x,h_y); % Trick to get rid of them at the next step
          end
        end
    end

    % Create the seeds for the Voronoi diagram by getting rid of the out-of-the-box points and those too close to the boundary
    k=1;
    seeds = zeros(1,2);
    for i=1:(n_x*n_y)
        if ( ( abs(grid(i,1)) <= (h_x/2-dist2bound) )  &  ( abs(grid(i,2)) <= (h_y/2-dist2bound) ) )
            seeds(k,:) = grid(i,:);
          k = k+1;
        end
    end
    
    
    % Create the 8 mirrors of the seeds (in order to have boundaries created)
    seeds_mirror_1 = [-seeds(:,1)+h_x*ones(k-1,1),seeds(:,2)];
    seeds_mirror_2 = [-seeds(:,1)+h_x*ones(k-1,1),-seeds(:,2)+h_y*ones(k-1,1)];
    seeds_mirror_3 = [-seeds(:,1)+h_x*ones(k-1,1),-seeds(:,2)-h_y*ones(k-1,1)];
    seeds_mirror_4 = [-seeds(:,1)-h_x*ones(k-1,1),seeds(:,2)];
    seeds_mirror_5 = [-seeds(:,1)-h_x*ones(k-1,1),-seeds(:,2)+h_y*ones(k-1,1)];
    seeds_mirror_6 = [-seeds(:,1)-h_x*ones(k-1,1),-seeds(:,2)-h_y*ones(k-1,1)];
    seeds_mirror_7 = [seeds(:,1),-seeds(:,2)+h_y*ones(k-1,1)];
    seeds_mirror_8 = [seeds(:,1),-seeds(:,2)-h_y*ones(k-1,1)];

    % Assemble all the seeds
    seeds_total = [ seeds ; seeds_mirror_1 ; seeds_mirror_2 ; seeds_mirror_3 ; seeds_mirror_4 ; seeds_mirror_5 ; seeds_mirror_6 ; seeds_mirror_7 ; seeds_mirror_8 ];
    
 %   voronoi(seeds_total(:,1),seeds_total(:,2));
    
    % Create the associated Voronoi diagram
    [V,C] = voronoin(seeds_total);
    
    % Get rid of double nodes by finding them and the ones out-of-the-box
    found_flag = 0;
    counter = 0;
    V_correspondance = zeros(size(V,1),1);
    for i=2:size(V,1)
        
        found_flag = 0;
        if ( ( abs(V(i,1)) > (h_x/2 + tolerance_2) )  |  ( abs(V(i,2)) > (h_y/2 + tolerance_2) ) ) % out-of-the-box nodes
            found_flag = 1;
        else 
            if (counter > 0.5) % Condition for the definition of "size(V_new,1)"
                for j=1:size(V_new,1)
                    if ( norm(V(i,:) - V_new(j,:) ) <  tolerance_2 ) % already found
                        found_flag = 1;
                        V_correspondance(i) =  j; % give the found node the same new number as the already existing one
                        break;
                    end
                end
            end   
        end
        
        if ( found_flag < 0.5 ) % Adding new nodes in-the-box
            if ( counter < 0.5) % First found one
                V_new = V(i,:); 
                counter = 1;
            else
                V_new = [ V_new ; V(i,:) ];
                counter = counter + 1;
            end
            V_correspondance(i) =  counter;
        end
        
    end
    
    % Get rif of out-of-the-box polygons and find the polygons with twice or more the same node
    counter_i = 1;
    for i=1:size(C,1)
        counter_j = 1;
        clear C_new_temp_1;
        for j=1:size(C{i},2)
            if ( V_correspondance( C{i}(j) ) > 0.5 ) % found one polygone with at least one node in-the-box
                C_new_temp_1{1}(counter_j) = V_correspondance( C{i}(j) );
                counter_j = counter_j + 1;
            end
            
        end  
        if (counter_j > 2.5) % Get rid of two-points "polygons"
            C_new_temp_2{counter_i} = C_new_temp_1{1};
            counter_i = counter_i + 1;
        end
    end
    
    
    
    % Get rid of "flat" elements 
    counter_i = 1;
    for i=1:size(C_new_temp_2,2)
        
        % Get rid of double nodes
        clear C_new_temp_1;
        C_new_temp_1{1}(1) = C_new_temp_2{i}(1);
        counter_j = 2;
        for j=2:size(C_new_temp_2{i},2)
            found_flag = 0;
            for k=1:size(C_new_temp_1{1},2)
                if ( abs(C_new_temp_1{1}(k) - C_new_temp_2{i}(j)) < tolerance_2 )
                    found_flag = 1;
                    break;
                end
            end
            if ( found_flag < 0.5 )
                C_new_temp_1{1}(counter_j) = C_new_temp_2{i}(j);
                counter_j = counter_j + 1;
            end
        end
        
        % Get rid of two-points polygones left
        if ( size(C_new_temp_1{1},2) > 2.5 )
            C_new{counter_i} = C_new_temp_1{1};
            counter_i = counter_i + 1;
        end
              
    end

    count_adj_cell = zeros(size(V_new,1),1);
    for i=1:size(C_new,2)
       for j=1:size(C_new{i},2)
	   C_now = C_new{i}(j);
	   count_adj_cell(C_now)++;
       end
    end
            
    increment = 0.001;
    Voronoi_polygone_length = max(h_x,h_y);
    for ii=1:1000
      printf('--> ii = %d\n', ii);
       % Calculate the smallest side       
      correction = 0;
      for i=1:size(C_new,2)
	  for j=1:size(C_new{i},2)
	      C_now = C_new{i}(j);
	      if (j==1)
		  C_previous = C_new{i}(size(C_new{i},2));
	      else
		  C_previous = C_new{i}(j-1);
	      end
	      Voronoi_polygone_length_temp = sqrt( (V_new(C_now,1)-V_new(C_previous,1))^2 + (V_new(C_now,2)-V_new(C_previous,2))^2 );
%                  if ( Voronoi_polygone_length_temp < Voronoi_polygone_length )
%                      Voronoi_polygone_length = Voronoi_polygone_length_temp;
%                  end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
              % Added by Vincent Peron
	      if ( Voronoi_polygone_length_temp < mesh_size )		 
%  		 printf('length init --> %f\n', Voronoi_polygone_length_temp);
%  		 printf('--> %f %f\n', V_new(C_now,1), V_new(C_now,2));
		 vec(1) = V_new(C_now,1)-V_new(C_previous,1);
		 vec(2) = V_new(C_now,2)-V_new(C_previous,2);
                 norm = sqrt(vec(1)^2+vec(2)^2);
                 vec(1) /= norm;
		 vec(2) /= norm;
                 if (((count_adj_cell(C_now) == 3) & (count_adj_cell(C_previous) == 3)) | 
		     ((count_adj_cell(C_now) == 2) & (count_adj_cell(C_previous) == 2)))
		   V_new(C_now,1) += vec(1)*increment;
		   V_new(C_now,2) += vec(2)*increment;
		   V_new(C_previous,1) -= vec(1)*increment;
		   V_new(C_previous,2) -= vec(2)*increment;
		 end
                 if (((count_adj_cell(C_now) == 3) & (count_adj_cell(C_previous) == 2)) |
		     ((count_adj_cell(C_now) == 2) & (count_adj_cell(C_previous) == 1)))
		   V_new(C_now,1) += vec(1)*increment;
		   V_new(C_now,2) += vec(2)*increment;
		 end
                 if (((count_adj_cell(C_now) == 2) & (count_adj_cell(C_previous) == 3)) |
		     ((count_adj_cell(C_now) == 1) & (count_adj_cell(C_previous) == 2)))
		   V_new(C_previous,1) -= vec(1)*increment;
		   V_new(C_previous,2) -= vec(2)*increment;
		 end
		 correction++;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  		 Voronoi_polygone_length_temp = sqrt( (V_new(C_now,1)-V_new(C_previous,1))^2 + (V_new(C_now,2)-V_new(C_previous,2))^2 );
%  		 printf('--> %f %f\n', V_new(C_now,1), V_new(C_now,2));
%  		 printf('length final --> %f\n', Voronoi_polygone_length_temp);
	      end
	  end
      end
      printf('correction(s) --> %i\n', correction);
      if (correction == 0)
	break;
      end
    end
    printf('--> %f\n',Voronoi_polygone_length);
    if (FlagCheckGrainSize == 0)
        numberOfGrainsTemp = numberOfGrains;
    else
        numberOfGrainsTemp = size(C_new,2);
	printf('%i\n',numberOfGrainsTemp);
    end
end % End of the While loop

printf('exit while !!\n');

%  voronoi(seeds_total(:,1),seeds_total(:,2));
%  axis([-h_x/2 h_x/2 -h_y/2 h_y/2]);

%pause;

fid = fopen('polycrystal.geo','w');

fprintf(fid,'%s\n', 'cl = 0.1;');

for i=1:size(V_new,1)
    fprintf(fid,'%s %i %s %f %s %f %s\n','Point(', i,') = {',V_new(i,1),', ',V_new(i,2), ', 0, cl};');
end

count = 1
for i=1:size(C_new,2)
    for j=1:size(C_new{i},2)
        C_now = C_new{i}(j);
        if ( j == size(C_new{i},2) )
            C_next = C_new{i}(1);
        else
            C_next = C_new{i}(j+1);
        end
        fprintf(fid,'%s %i %s %i %s %i %s\n','Line(', count, ') = {',C_now,', ',C_next,'};');
	count = count +1;
    end
end

count = 1
printf('%i\n',size(C_new,2))
for i=1:size(C_new,2)
    fprintf(fid,'%s %i %s', 'Line Loop(', i, ') = {');
    for j=1:size(C_new{i},2)
        C_now = C_new{i}(j);
        if ( j == size(C_new{i},2) )
            C_next = C_new{i}(1);
        else
            C_next = C_new{i}(j+1);
        end

        if ( j == size(C_new{i},2) )
            fprintf(fid,'%i', count);
        else
            fprintf(fid,'%i %s', count,', ');
        end

	count = count +1;
    end
    fprintf(fid,'%s\n', '};');
    fprintf(fid,'%s %i %s %i %s\n', 'Plane Surface(', i, ') = {', i, '};');
end

for i=1:size(C_new,2)
    fprintf(fid,'%s %f %s\n %s %i %s\n %s\n', 'Extrude {0,0,',0.3,'} {', 'Surface{', i, '};', '}');
%      fprintf(fid,'%s\n %s %i %s\n %s\n', 'Extrude {0,0,1} {', 'Surface{', i, '};', '}');
end

fclose(fid);

exit;


%  fid = fopen('PolycrystalScriptForAbaqus.py','w');
%  
%  fprintf(fid,'%s\n','"""');
%  fprintf(fid,'%s\n','PolycrystalScriptForAbaqus.py');
%  fprintf(fid,'%s\n','');
%  fprintf(fid,'%s\n','Copyright (C) Antoine Jerusalem 2006');
%  fprintf(fid,'%s\n','Creation of the polycrystalline Mesh: next steps are partitionning and meshing on Abaqus');
%  fprintf(fid,'%s\n','"""');
%  fprintf(fid,'%s\n','');
%  fprintf(fid,'%s\n','from abaqus import *');
%  fprintf(fid,'%s\n','from abaqusConstants import *');
%  fprintf(fid,'%s\n','import sketch');
%  fprintf(fid,'%s\n','import part');
%  fprintf(fid,'%s\n','import regionToolset');
%  fprintf(fid,'%s\n','import material');
%  fprintf(fid,'%s\n','import section');
%  fprintf(fid,'%s\n','');
%  fprintf(fid,'%s %f %s\n','s = mdb.models[''Model-1''].Sketch(name=''__profile__'', sheetSize=',max(2*h_x,2*h_y),')');
%  fprintf(fid,'%s\n','g, v, d = s.geometry, s.vertices, s.dimensions');
%  fprintf(fid,'%s %f %s %f %s\n','s.sketchOptions.setValues(sheetSize=',max(2*h_x,2*h_y),', gridSpacing=',max(2*h_x,2*h_y)/10,', grid=ON, ');
%  fprintf(fid,'%s\n','    gridFrequency=2, constructionGeometry=ON, dimensionTextHeight=5.0, ');
%  fprintf(fid,'%s\n','    decimalPlaces=2)');
%  fprintf(fid,'%s\n','s.setPrimaryObject(option=STANDALONE)');
%  fprintf(fid,'%s %f %s %f %s %f %s %f %s\n','s.rectangle(point1=(',-h_x/2,', ',h_y/2,'), point2=(',h_x/2,', ',-h_y/2,'))');
%  fprintf(fid,'%s\n','p = mdb.models[''Model-1''].Part(name=''Plate'', dimensionality=THREE_D, ');
%  fprintf(fid,'%s\n','    type=DEFORMABLE_BODY)');
%  fprintf(fid,'%s\n','p = mdb.models[''Model-1''].parts[''Plate'']');
%  fprintf(fid,'%s %f %s\n','p.BaseSolidExtrude(sketch=s, depth=',h_z,')');
%  fprintf(fid,'%s\n','s.unsetPrimaryObject()');
%  fprintf(fid,'%s\n','p = mdb.models[''Model-1''].parts[''Plate'']');
%  fprintf(fid,'%s\n','session.viewports[''Viewport: 1''].setValues(displayedObject=p)');
%  fprintf(fid,'%s\n','del mdb.models[''Model-1''].sketches[''__profile__'']');
%  fprintf(fid,'%s\n','p1 = mdb.models[''Model-1''].parts[''Plate'']');
%  fprintf(fid,'%s\n','f, e, d1 = p1.faces, p1.edges, p1.datums');
%  fprintf(fid,'%s\n','t = p1.MakeSketchTransform(sketchPlane=f[4], sketchUpEdge=e[4], ');
%  fprintf(fid,'%s %f %s\n','    sketchPlaneSide=SIDE1, origin=(0.0, 0.0, ',h_z,'))');
%  fprintf(fid,'%s %f %s\n','s1 = mdb.models[''Model-1''].Sketch(name=''__profile__'', sheetSize=',max(2*h_x,2*h_y),', ');
%  fprintf(fid,'%s %f %s\n','    gridSpacing=',max(2*h_x,2*h_y)/10,', transform=t)');
%  fprintf(fid,'%s\n','g, v, d = s1.geometry, s1.vertices, s1.dimensions');
%  fprintf(fid,'%s %f %s %f %s\n','s1.sketchOptions.setValues(sheetSize=',max(2*h_x,2*h_y),', gridSpacing=',max(2*h_x,2*h_y)/10,', grid=ON, ');
%  fprintf(fid,'%s\n','    gridFrequency=2, constructionGeometry=ON, dimensionTextHeight=5.0, ');
%  fprintf(fid,'%s\n','    decimalPlaces=2)');
%  fprintf(fid,'%s\n','s1.setPrimaryObject(option=SUPERIMPOSE)');
%  fprintf(fid,'%s\n','p1 = mdb.models[''Model-1''].parts[''Plate'']');
%  fprintf(fid,'%s\n','p1.projectReferencesOntoSketch(sketch=s1, filter=COPLANAR_EDGES)');
%  fprintf(fid,'%s\n','r, r1 = s1.referenceGeometry, s1.referenceVertices');
%  
%  for i=1:size(C_new,2)
%      for j=1:size(C_new{i},2)
%          C_now = C_new{i}(j);
%          if ( j == size(C_new{i},2) )
%              C_next = C_new{i}(1);
%          else
%              C_next = C_new{i}(j+1);
%          end
%          fprintf(fid,'%s %f %s %f %s %f %s %f %s\n','s1.Line(point1=(',V_new(C_now,1),', ',V_new(C_now,2),'), point2=(',V_new(C_next,1),', ',V_new(C_next,2),'))');
%      end
%  end
%      
%  fprintf(fid,'%s %f %s %f %s\n','s1.constraintReferences(vertex1=r1.findAt((',-h_x/2,', ',h_y/2,'), 1))');
%  fprintf(fid,'%s\n','p1 = mdb.models[''Model-1''].parts[''Plate'']');
%  fprintf(fid,'%s\n','f, e, d1 = p1.faces, p1.edges, p1.datums');
%  fprintf(fid,'%s\n','faces =(f[4], )');
%  fprintf(fid,'%s\n','p1.PartitionFaceBySketch(sketchUpEdge=e[4], faces=faces, sketch=s1)');
%  fprintf(fid,'%s\n','s1.unsetPrimaryObject()');
%  fprintf(fid,'%s\n','del mdb.models[''Model-1''].sketches[''__profile__'']');
%  
%  for i=1:size(C_new,2)
%      fprintf(fid,'%s%i%s\n','mdb.models[''Model-1''].Material(name=''Material-',i,''')');
%  end
%  
%  for i=1:size(C_new,2)
%      fprintf(fid,'%s%i%s\n','mdb.models[''Model-1''].HomogeneousSolidSection(name=''Section-',i,''', ');
%      fprintf(fid,'%s%i%s\n','    material=''Material-',i,''', thickness=1.0)');
%  end
%  
%  fprintf(fid,'%s\n','myViewport = session.Viewport(name=''Viewport for Model Polycrystal'',');
%  fprintf(fid,'%s\n','    origin=(10, 10), width=150, height=100)');
%  fprintf(fid,'%s\n','myViewport.setValues(displayedObject=p1)');
%  fprintf(fid,'%s\n','myViewport.partDisplay.setValues(renderStyle=SHADED)');
%  
%  fclose(fid);
