#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch beam problem with a python script

# material law
lawnum = 1 # unique number of law
rho   = 7850.
E     = 220000.
nu    = 0.3
mu    = 0.5 * E / (1.+nu)
Ex=E;	Ey=E/10;	Ez=E/10;
Vxy=nu;	Vxz=nu;	Vyz=nu;
MUxy=mu;MUxz=mu;MUyz=mu;
alpha = 0.
beta  = 0.
gamma = 0.

lawnum2 = 2 # unique number of law
Ex2=E;	Ey2=E/10;	Ez2=E/10;
Vxy2=nu;	Vxz2=nu;	Vyz2=nu;
MUxy2=mu;MUxz2=mu;MUyz2=mu;
alpha2 = 45.
beta2  = 45.
gamma2 = 45.


# geometry
meshfile="polycrystal.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1  = 30


#  compute solution and BC (given directly to the solver
# creation of law
law1 = AnisotropicDG3DMaterialLaw(lawnum,rho,Ex,Ey,Ez,Vxy,Vxz,Vyz,MUxy,MUxz,MUyz,alpha,beta,gamma)
law2 = AnisotropicDG3DMaterialLaw(lawnum2,rho,Ex2,Ey2,Ez2,Vxy2,Vxz2,Vyz2,MUxy2,MUxz2,MUyz2,alpha2,beta2,gamma2)

# creation of ElasticField
nfield = 10 # number of the field (physical number of surface)
myfield1 = dG3DDomain(1000,nfield,space1,lawnum,fullDg)
#myfield1.stabilityParameters(beta1)
#nfield2 = 11 # number of the field (physical number of surface)
#myfield2 = dG3DDomain(1000,nfield2,space1,lawnum2,fullDg)
#myfield2.stabilityParameters(beta1)
#myfield1.matrixByPerturbation(1,1,1,1e-8)
myinterfield1 = interDomainBetween3D(1000,myfield1,myfield2,lawnum)
myinterfield1.stabilityParameters(beta1)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
#mysolver.addDomain(myfield2)
mysolver.addMaterialLaw(law1)
#mysolver.addMaterialLaw(law2)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
# BC
mysolver.displacementBC("Face",101,0,0.)
mysolver.displacementBC("Face",110,0,0.03)
mysolver.displacementBC("Face",102,1,0.)
mysolver.displacementBC("Face",103,2,0.)


mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("eps_xx",IPField.STRAIN_XX, 1, 1)
mysolver.internalPointBuildView("eps_yy",IPField.STRAIN_YY, 1, 1)
mysolver.internalPointBuildView("eps_zz",IPField.STRAIN_ZZ, 1, 1)
mysolver.internalPointBuildView("eps_xy",IPField.STRAIN_XY, 1, 1)
mysolver.internalPointBuildView("eps_yz",IPField.STRAIN_YZ, 1, 1)
mysolver.internalPointBuildView("eps_xz",IPField.STRAIN_XZ, 1, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 1111, 2)
mysolver.archivingForceOnPhysicalGroup("Face", 3333, 2)

mysolver.solve()

