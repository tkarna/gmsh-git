#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*
#script to launch beam problem with a python script

# material law
lawnum = 1 # unique number of law
rho   = 10000. #7850
young = 10000.e6 #71.e9
nu    = 0. 
sy0   = 5000000000.0e6  # elastic
h     = 400e6

# geometry
meshfile="beam2.msh" # name of mesh file

# solver
sol = 0  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 2 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 50   # number of step (used only if soltype=1)
ftime =0.005   # Final time (used only if soltype=1)
tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=50 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
gamma_s = 0.666666667
rhoinfty=1.

#  compute solution and BC (given directly to the solver
# creation of law
law1 = J2LinearDG3DMaterialLaw(lawnum,rho,young,nu,sy0,h)

# creation of ElasticField
nfield = 99 # number of the field (physical number of surface)
myfield1 = dG3DDomain(1000,nfield,space1,lawnum,fullDg)
#myfield1.matrixByPerturbation(1,1,1,1e-8)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.explicitSpectralRadius(ftime,gamma_s,rhoinfty)
mysolver.stepBetweenArchiving(nstepArch)
# BC
mysolver.displacementBC("Face",305,2,0.)
mysolver.displacementBC("Face",305,1,0.)
mysolver.displacementBC("Face",305,0,0.)
#mysolver.displacementBC("Volume",99,2,0.)
#mysolver.displacementBC("Volume",99,1,0.)
mysolver.initialBC("Volume",'Velocity',99,0,1.)

mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 305, 0)
mysolver.archivingForceOnPhysicalGroup("Face", 305, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 305, 2)
mysolver.archivingNodeDisplacement(101,0)
mysolver.archivingNodeVelocity(101,0)

mysolver.solve()

