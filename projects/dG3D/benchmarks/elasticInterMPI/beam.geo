
Lc = 0.01;
L = 1;
b = 0.1;
h = 0.1;

// bottom
Point(1) = {  0.  ,  0.  , 0.  ,Lc};
Point(2) = { L/2. ,  0.  , 0.  ,Lc};
Point(3) = {  L   ,  0.  , 0.  ,Lc};
Point(4) = {  L   ,  b   , 0.  ,Lc};
Point(5) = { L/2. ,  b   , 0.  ,Lc};
Point(6) = {  0.  ,  b   , 0.  ,Lc};

// top
Point(11) = {  0.  ,  0.  , h  ,Lc};
Point(12) = { L/2. ,  0.  , h  ,Lc};
Point(13) = {  L   ,  0.  , h  ,Lc};
Point(14) = {  L   ,  b   , h  ,Lc};
Point(15) = { L/2. ,  b   , h  ,Lc};
Point(16) = {  0.  ,  b   , h  ,Lc};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,5};
Line(5) = {5,6};
Line(6) = {6,1};
Line(7) = {2,5};

Line(11) = {11,12};
Line(12) = {12,13};
Line(13) = {13,14};
Line(14) = {14,15};
Line(15) = {15,16};
Line(16) = {16,11};
Line(17) = {12,15};

Line(21) = {1,11};
Line(22) = {2,12};
Line(23) = {3,13};
Line(24) = {4,14};
Line(25) = {5,15};
Line(26) = {6,16};

Line Loop(1) = {1,7,5,6};
Line Loop(2) = {2,3,4,-7};
Line Loop(11) = {11,17,15,16};
Line Loop(12) = {12,13,14,-17};
Line Loop(3) = {1,22,-11,-21};
Line Loop(4) = {2,23,-12,-22};
Line Loop(5) = {3,24,-13,-23};
Line Loop(6) = {4,25,-14,-24};
Line Loop(7) = {5,26,-15,-25};
Line Loop(8) = {6,21,-16,-26};
Line Loop(9) = {7,25,-17,-22};

Plane Surface(1) = {1};
Plane Surface(2) = {2};
Plane Surface(3) = {3};
Plane Surface(4) = {4};
Plane Surface(5) = {5};
Plane Surface(6) = {6};
Plane Surface(7) = {7};
Plane Surface(8) = {8};
Plane Surface(9) = {9};
Plane Surface(11) = {11};
Plane Surface(12) = {12};

Surface Loop(1) = {3,9,7,8,1,-11};
Surface Loop(2) = {4,5,6,9,2,-12};
Volume(1) = {1};
Volume(2) = {2};

Physical Volume(99) = {1,2};
Physical Surface(305) = {5};
Physical Surface(308) = {8};
Physical Point(101) = {1};

// Mesh
Transfinite Line{6,7,3,16,17,13,21,22,23,24,25,26} = 2;
Transfinite Line{1,2,4,5,11,12,14,15} = 4;
Transfinite Surface{1,2,3,4,5,6,7,8,9,11,12};
Recombine Surface{1,2,3,4,5,6,7,8,9,11,12};
Transfinite Volume{1,2}; 


