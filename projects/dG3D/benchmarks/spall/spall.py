#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*

#script to launch beam problem with a python script

# material law
lawnum = 1 # unique number of law
rho   = 3690.
young = 260.e9
nu    = 0. 
sy0   = 500.e10
h     = 400.e6

lawcnum =2
Gc=34.  # fracture energy [J/m2]
sigmac=390.e6 # fracture limit in tension [Pa]
beta =1. # ratio KII/KI
mu =1. # friction coefficient ??

vz= 6.086 

# geometry
meshfile="spall.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 2 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 100   # number of step (used only if soltype=1)
ftime = 2.e-6   # Final time (used only if soltype=1)
tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=100 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 1 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1  = 40

#  compute solution and BC (given directly to the solver
# creation of law
law1 = J2LinearDG3DMaterialLaw(lawnum,rho,young,nu,sy0,h)
law2 = LinearCohesive3DLaw(lawcnum,Gc,sigmac,beta,mu)
law3 = FractureByCohesive3DLaw(3,lawnum,lawcnum)


# creation of ElasticField
nfield = 10 # number of the field (physical number of surface)
myfield1 = dG3DDomain(1000,nfield,space1,3,fullDg)
myfield1.matrixByPerturbation(0,1,1,1e-8)
myfield1.stabilityParameters(beta1)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.addMaterialLaw(law2)
mysolver.addMaterialLaw(law3)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.explicitSpectralRadius(ftime,0.8,0.7)
mysolver.explicitTimeStepEvaluation(nstep)
mysolver.stepBetweenArchiving(nstepArch)
# BC
#shearing
#mysolver.displacementBC("Face",1234,0,0.)
#mysolver.displacementBC("Face",1234,1,0.)
#mysolver.displacementBC("Face",1234,2,0.)
#mysolver.displacementBC("Face",5678,1,0.00)
#mysolver.displacementBC("Face",5678,0,0.00001)
#mysolver.forceBC("Face",2376,2,100000000)
#mysolver.forceBC("Face",4158,2,-100000000)
#mysolver.forceBC("Face",5678,0,100000000)
#tension along z
dz= vz
mysolver.displacementBC("Face",1234,2,-dz)
mysolver.displacementBC("Face",5678,2,dz)
mysolver.displacementBC("Face",2376,0,0.)
mysolver.displacementBC("Face",1265,1,0.)


mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 1234, 2)
mysolver.archivingForceOnPhysicalGroup("Face", 5678, 2)

mysolver.solve()

