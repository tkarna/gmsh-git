#coding-Utf-8-*-
from gmshpy import *
#from dG3DpyDebug import*
from dG3Dpy import*

#script to launch composite problem with a python script

# material law
lawnonlocal1     = 1 # unique number of lawlawnonlocal1     = 1 # unique number of law
lawnonlocalcoh1  = 2 # unique number of law
lawnonlocalfrac1 = 3 # unique number of law


rho   = 7850
young = 210.e9
nu    = 0.3
sy0   = 630e6
h     = 0.1*young 

# Gurson
q1    = 1.5
q2    = 1.
q3    = 1.5
fC     = 0.15
ff     = 0.4
ffstar = 0.5
fn     = 0.;
sn     = 0.1;
epsilonn = 0.3;
A0      = 0.1;

fVinitial = 0.05; #095;


Gc = 1.44e4
Dc = 0.95
sigmacCF = 60.42e7 # fracture limit in tension [Pa]
beta=1

#Gc = 0.879e4
#Dc = 0.99
#sigmacCF = 176.7e7 # fracture limit in tension [Pa]
#beta=1.

# geometry
meshfile="DNT.msh" # name of mesh file


# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 3 # StaticLinear=0 (default) StaticNonLinear=1 Multi=3
nstepImpl = 600  # number of step (used only if soltype=1)
nstepExpl = 600 #1000
ftime =1.e-2   # Final time (used only if soltype=1)
tol=1.e-3   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=10000 # Number of step between 2 archiving (used only if soltype=1)
fulldg = 1   # 1 use dg inside a domain
dgnl = 1     # 1 use dg for non local epl inside a domain - only if fulldg
beta1 = 40   # stability parameter for dg
fsmin = 0.95
fsmax = 1.
eqRatio = 1.e8


#  compute solution and BC (given directly to the solver
# creation of law
harden = PowerLawJ2IsotropicHardening(1,sy0, h, 1.)
cl     = IsotropicCLengthLaw(1, 2e-6)

law1   = NonLocalDamageGursonDG3DMaterialLaw(lawnonlocal1,rho,young,nu,q1,q2,q3,fC, ff, ffstar,A0,fn,sn,epsilonn, fVinitial, harden,cl)
lawcoh1  = NonLocalDamageLinearCohesive3DLaw(lawnonlocalcoh1,Gc,sigmacCF,Dc,beta,fsmin,fsmax)
lawfrac1 = FractureByCohesive3DLaw(lawnonlocalfrac1,lawnonlocal1,lawnonlocalcoh1)



# creation of ElasticField
nfield1 = 51 # number of the field (physical number of surface)

space1 = 0 # function space (Lagrange=0)

myfield1 = nonLocalDamageDG3DDomain(1000,nfield1,space1,lawnonlocal1,fulldg,eqRatio)
myfield1.stabilityParameters(beta1)  #stabilty parameter on displacement
myfield1.nonLocalStabilityParameters(beta1,dgnl) #stabilty parameter on non local epl


#myfield1.matrixByPerturbation(1,1,1,1e-8)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)

mysolver.addMaterialLaw(law1)
mysolver.addMaterialLaw(lawcoh1)
mysolver.addMaterialLaw(lawfrac1)

mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstepImpl,ftime,tol)
mysolver.snlManageTimeStep(500, 3, 2, 10)
mysolver.explicitSpectralRadius(ftime,0.5,0.)
mysolver.explicitTimeStepEvaluation(nstepExpl)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.addSystem(3,2)
mysolver.addSystem(1,1)
# BC
#tension along x

d1=0.6

mysolver.displacementBC("Face",101,0,0.)
mysolver.displacementBC("Face",1012,0,0.)
mysolver.displacementBC("Face",1012,1,0.)
mysolver.displacementBC("Face",201,1,d1)
mysolver.displacementBC("Face",301,2,0.)
mysolver.initialBC("Volume","Position",10,3,fVinitial)


#mysolver.constraintBC("Face",112,1)

mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("eps_xx",IPField.STRAIN_XX, 1, 1)
mysolver.internalPointBuildView("eps_yy",IPField.STRAIN_YY, 1, 1)
mysolver.internalPointBuildView("eps_zz",IPField.STRAIN_ZZ, 1, 1)
mysolver.internalPointBuildView("eps_xy",IPField.STRAIN_XY, 1, 1)
mysolver.internalPointBuildView("eps_yz",IPField.STRAIN_YZ, 1, 1)
mysolver.internalPointBuildView("eps_xz",IPField.STRAIN_XZ, 1, 1)
mysolver.internalPointBuildView("damage",IPField.DAMAGE, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
#mysolver.archivingForceOnPhysicalGroup("Edge", 101, 1, nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 201, 1, nstepArch)

mysolver.archivingIPOnPhysicalGroup("Volume",nfield1, IPField.PLASTICSTRAIN,IPField.MIN_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield1, IPField.PLASTICSTRAIN,IPField.MAX_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield1, IPField.PLASTICSTRAIN,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield1, IPField.DAMAGE,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield1, IPField.DAMAGE,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield1, IPField.DAMAGE,IPField.MEAN_VALUE, nstepArch);

#mysolver.archivingIPOnPhysicalGroup("Volume",nfield2, IPField.PLASTICSTRAIN,IPField.MIN_VALUE,nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",nfield2, IPField.PLASTICSTRAIN,IPField.MAX_VALUE,nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",nfield2, IPField.PLASTICSTRAIN,IPField.MEAN_VALUE, nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",nfield2, IPField.DAMAGE,IPField.MIN_VALUE, nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",nfield2, IPField.DAMAGE,IPField.MAX_VALUE, nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",nfield2, IPField.DAMAGE,IPField.MEAN_VALUE, nstepArch);

#mysolver.archivingNodeIP(21, IPField.STRAIN_XX, IPField.MAX_VALUE, nstepArch);
#mysolver.archivingNodeIP(21, IPField.STRAIN_YY, IPField.MAX_VALUE, nstepArch);
#mysolver.archivingNodeIP(21, IPField.DAMAGE, IPField.MAX_VALUE, nstepArch);
#mysolver.archivingNodeIP(23, IPField.STRAIN_XX, IPField.MAX_VALUE, nstepArch);
#mysolver.archivingNodeIP(23, IPField.STRAIN_YY, IPField.MAX_VALUE, nstepArch);
#mysolver.archivingNodeIP(23, IPField.DAMAGE, IPField.MAX_VALUE, nstepArch);


#mysolver.archivingNodeDisplacement(21,0)
#mysolver.archivingNodeDisplacement(23,0)

mysolver.solve()
