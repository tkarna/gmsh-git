// creat a cube with fiber

//defination of unit
mm = 0.001;

//basic parameters
a=5*10.0*mm;
r1geers=5*2.0*mm;
r2geers=5*2.5*mm;
rc=5*1.0*mm;
r1=Sqrt((r1geers-rc)*(r1geers-rc)+rc*rc); //2.0*mm; 
r2=Sqrt((r2geers-rc)*(r2geers-rc)+rc*rc); //2.5*mm; 

t=5*1.0*mm;


// Characteristic length for mesh
Lc1=0.18*a;
Lc2=0.3*r1;
Lc3=0.3*r1;
Lc4=0.12*r1;


// definition of points
Point(1) = { 0.0 , 0.0 , 0.0 , Lc2};
Point(2) = { a , 0.0 , 0.0 , Lc1};
Point(3) = { a , a , 0.0 , Lc2};
Point(4) = { 0.0, a , 0.0 , Lc1};
Point(5) = { rc , rc , 0.0 , Lc2};
Point(6) = { a-rc , a-rc , 0.0 , Lc2};

Point(7) = { rc+r2*Cos(0.025) , rc+r2*Sin(0.025) , 0.0 , Lc4};
Point(8) = { rc+Sqrt(r2*r2-rc*rc) , 0.0 , 0.0 , Lc3};
Point(9) = { 0.0, rc+Sqrt(r2*r2-rc*rc) , 0.0 ,  Lc2};

Point(10) = { a-rc-r1*Cos(1.55) , a-rc-r1*Sin(1.55) , 0.0 , Lc4};
Point(11) = { a-rc-Sqrt(r1*r1-rc*rc) , a , 0.0 , Lc3};
Point(12) = { a, a-rc-Sqrt(r1*r1-rc*rc) , 0.0 , Lc2};

Point(13) = { 1.9*a/3.0, 1.1*a/3.0 , 0.0 , Lc4};

Point(14) = { rc+r2*Cos(1.55) , rc+r2*Sin(1.55) , 0.0 , Lc4};
Point(15) = { rc+Sqrt(r2*r2-rc*rc) , 0.0 , 0.0 , Lc3};
Point(16) = { 0.0, rc+Sqrt(r2*r2-rc*rc) , 0.0 ,  Lc2};

Point(17) = { a-rc-r1*Cos(0.025) , a-rc-r1*Sin(0.025) , 0.0 , Lc4};
Point(18) = { a-rc-Sqrt(r1*r1-rc*rc) , a , 0.0 , Lc3};
Point(19) = { a, a-rc-Sqrt(r1*r1-rc*rc) , 0.0 , Lc2};

Point(20) = { 1.1*a/3.0, 1.9*a/3.0 , 0.0 , Lc4};



// Line between points
Line(1) = {8,2};
Line(2) = {2,12};
Circle(3) = {12,6,10};
Circle(4) = {10,6,17};
Circle(13) = {17,6,11};

Line(5) = {11,4};
Line(6) = {4,9};
Circle(7) = {9,5,14};
Circle(14) = {14,5,7};
Circle(8) = {7,5,8};

Line(9) = {13,7};
Line(10) = {13,2};
Line(11) = {13,10};
Line(12) = {13,20};
Line(15) = {20,4};
Line(16) = {17,20};
Line(17) = {20,14};


Line Loop(1) = {1,-10,9,8};
Line Loop(2) = {2,3,-11,10};
Line Loop(3) = {4,16,-12,11};
Line Loop(4) = {13,5,-15,-16};
Line Loop(5) = {6,7,-17,15};
Line Loop(6) = {14,-9,12,17};

Plane Surface(1) = {1};
Plane Surface(2) = {2};
Plane Surface(3) = {3};
Plane Surface(4) = {4};
Plane Surface(5) = {5};
Plane Surface(6) = {6};

//VOlume

my_mat1_1[] = Extrude {0.0 , 0.0 , t/2.0} {Surface{1};Surface{2};Surface{3};Surface{4};Surface{5};Surface{6}; Layers{1};};


//physical volume for material properties
Physical Volume(51) ={my_mat1_1[]};


// Physical objects to applied BC and material


//fix x
Physical Surface(101) = {114};

//fix xy
Physical Surface(1012) = {26,48};

//fix Z
//Physical Surface(301) = {104};
Physical Surface(301) = {1,2,3,4,5,6};

//loading y
Physical Surface(201) = {96};
/*
Transfinite Line {1,6,3,2,13} = 21;
Transfinite Line {12,11,10} = 21;
Transfinite Line {5,7,8,9,4} = 17 Using progression 0.95;

Transfinite Surface {1,2,3,4} ;

Recombine Surface {1,2,3,4} ;
*/





