#coding-Utf-8-*-
from gmshpy import *
from dG3DpyDebug import*
import math

#script to launch beam problem with a python script

# material law
lawnum = 1 # unique number of law
rho   = 7850
lamb  = 121154e6
mu    = 80769e6 
young = (mu*(3.*lamb+2.*mu)/(lamb+mu))
nu    = (lamb/2./(lamb+mu)) 
sy0   = 400e6
h     = 100e6

# Gurson
q1    = 1.5
q2    = 1.
q3    = 1.
fC     = 0.1
ff     = 0.18
ffstar = 0.5
fn     = 0.1;
sn     = 0.1;
epsilonn = 0.3;
A0      = 0.;

fVinitial = 0.; #095;

# geometry
meshfile="cube.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 5000   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=10 # Number of step between 2 archiving (used only if soltype=1)
fullDg = 0 #O = CG, 1 = DG
space1 = 0 # function space (Lagrange=0)
beta1  = 10

#  compute solution and BC (given directly to the solver
# creation of law
#law1 = J2LinearDG3DMaterialLaw(lawnum,rho,young,nu,sy0,h)
harden = PowerLawJ2IsotropicHardening(1,sy0, h, 1.)
#cl     = AnisotropicCLengthLaw(1, 8.45e-11, 8.45e-11, 2e-6, 0, 90, 0)
cl     = IsotropicCLengthLaw(1, 2e-6)

gdnexp    = ExponentialGursonDamageNucleation(1, fn, sn, epsilonn);
gdnlin    = LinearGursonDamageNucleation(2, A0);

law1   = NonLocalDamageGursonDG3DMaterialLaw(lawnum,rho,young,nu,q1,q2,q3,fC, ff, ffstar,fVinitial, harden,cl, gdnexp)

# creation of ElasticField
nfield = 10 # number of the field (physical number of surface)
#myfield1 = dG3DDomain(1000,nfield,space1,lawnum,fullDg)
myfield1 = nonLocalDamageDG3DDomain(1000,nfield,space1,lawnum,fullDg,1.e6)

#myfield1.matrixByPerturbation(1,1,1,1e-8)
myfield1.stabilityParameters(beta1)
myfield1.nonLocalStabilityParameters(beta1,1)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
# BC
#shearing
#mysolver.displacementBC("Face",1234,0,0.)
#mysolver.displacementBC("Face",1234,1,0.)
#mysolver.displacementBC("Face",1234,2,0.)
#mysolver.displacementBC("Face",5678,1,0.00)
#mysolver.displacementBC("Face",5678,0,0.00001)
#mysolver.forceBC("Face",2376,2,100000000)
#mysolver.forceBC("Face",4158,2,-100000000)
#mysolver.forceBC("Face",5678,0,100000000)
#tension along z
mysolver.displacementBC("Face",1234,2,0.)
mysolver.displacementBC("Face",5678,2,0.0004)
mysolver.displacementBC("Face",2376,0,0.)
mysolver.displacementBC("Face",1265,1,0.)
mysolver.initialBC("Volume","Position",10,3,fVinitial)


mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 1234, 2)
mysolver.archivingForceOnPhysicalGroup("Face", 5678, 2)
mysolver.internalPointBuildView("damage",IPField.DAMAGE,1,1)
mysolver.archivingNodeIP(1, IPField.PLASTICSTRAIN,1);
mysolver.archivingIPOnPhysicalGroup("Volume",10, IPField.PLASTICSTRAIN,IPField.MIN_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",10, IPField.PLASTICSTRAIN,IPField.MAX_VALUE);
mysolver.archivingIPOnPhysicalGroup("Volume",10, IPField.PLASTICSTRAIN,IPField.MEAN_VALUE);


mysolver.solve()

