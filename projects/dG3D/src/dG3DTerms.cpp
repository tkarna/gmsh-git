//
// C++ Interface: terms
//
// Description: Class of terms for dg non linear shell
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "dG3DTerms.h"

void dG3DForceBulk::get(MElement *ele,int npts,IntPt *GP,fullVector<double> &vFor) const
{

  // Initialization of some data
  int nbdof = this->space1.getNumKeys(ele);
  int nbFF = ele->getNumShapeFunctions();
  vFor.resize(nbdof);
  vFor.scale(0.);
  SVector3 BT;

  // get gauss' point data
  _ipf->getIPv(ele,vipv,vipvprev);
  // grads value
  //nlsFunctionSpaceXYZ<double>* sp1 = static_cast<nlsFunctionSpaceXYZ<double>*>(&(this->space1));
  //std::vector<GaussPointSpaceValues<double>*> vall;
  //sp1->get(ele,npts,GP,vall);

  bool hessianflag = false;
  bool thirdflag = false;
  if (_mlaw->isHighOrder()) {
    hessianflag = true;
    if (_fullDg) thirdflag = true;
  };

  double averagePressure = 0.;
  if((ele->getTypeForMSH() == MSH_HEX_8 or ele->getTypeForMSH() == MSH_QUA_4) and !_fullDg) //not correct in large deformations
  {
    for (int i = 0; i < npts; i++)
    {
      const STensor3 *PK1 = &(vipv[i]->getConstRefToFirstPiolaKirchhoffStress());
      double pres = (PK1->operator()(0,0)+PK1->operator()(1,1)+PK1->operator()(2,2))/3.;
      averagePressure += pres/npts;
    }
  }
  for (int i = 0; i < npts; i++)
  {
//    double u = GP[i].pt[0]; double v = GP[i].pt[1]; double w = GP[i].pt[2];
    // Weight of Gauss' point i
    double weight = GP[i].weight;
 //   double detJ = ele->getJacobian(u, v, w, jac);
 //   double detJ = vall[i]->_detJ;
    double & detJ = vipv[i]->getJacobianDeterminant(&space1,ele,GP[i],hessianflag,thirdflag);
    const STensor3 *PK1 = &(vipv[i]->getConstRefToFirstPiolaKirchhoffStress());
    double ratio = detJ*weight;
    double pres = (PK1->operator()(0,0)+PK1->operator()(1,1)+PK1->operator()(2,2))/3.;

//    Grads.clear();
//    space1.gradf(ele,u, v, w, Grads); // a optimiser

 //   std::vector<TensorialTraits<double>::GradType> &Grads = vall[i]->_vgrads;
    std::vector<TensorialTraits<double>::GradType> &Grads = vipv[i]->gradf(&space1,ele,GP[i],hessianflag,thirdflag);
    for(int j=0;j<nbFF; j++)
    {
      BT(0) = Grads[j+0*nbFF][0];
      BT(1) = Grads[j+0*nbFF][1];
      BT(2) = Grads[j+0*nbFF][2];
      // x y z
      for(int kk=0;kk<3;kk++)
      {
        if((ele->getTypeForMSH() == MSH_HEX_8 or ele->getTypeForMSH() == MSH_QUA_4) and !_fullDg)
        {
          for(int m=0; m<3; m++)
          {
	      vFor(j+kk*nbFF) += ratio*(BT(m)*(PK1->operator()(kk,m)));
          }
          vFor(j+kk*nbFF) -= ratio*(BT(kk)*(pres-averagePressure));
        }
        else
        {
          for(int m=0; m<3; m++)
          {
            vFor(j+kk*nbFF) += ratio*(BT(m)*PK1->operator()(kk,m));
          }
        }
      }

    }
  }
  // vFor.print("interF");
}

void dG3DStiffnessBulk::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &mStiff) const
{
  if (sym)
  {
    // Initialization of some data
    int nbdof = BilinearTerm<double,double>::space1.getNumKeys(ele);
    int nbFF = ele->getNumShapeFunctions();
    mStiff.resize(nbdof, nbdof,true); // true --> setAll(0.)
//    mStiff.setAll(0.); // done by resize!

    // get ipvariables
    _ipf->getIPv(ele,vipv);
    // Value of grad and _detJ at all Gauss points
    //nlsFunctionSpaceXYZ<double>* sp1 = static_cast<nlsFunctionSpaceXYZ<double>*>(&(this->space1));
    //std::vector<GaussPointSpaceValues<double>*> vall;
    //sp1->get(ele,npts,GP,vall);

    bool hessianflag = false;
    bool thirdflag = false;
    if (_mlaw->isHighOrder()) {
      hessianflag = true;
      if (_fullDg) thirdflag = true;
    };
    // sum on Gauss' points
    SVector3 B;
    SVector3 BT;
    std::vector<SVector3> HBaverage(nbFF);
    SVector3 Bipg(0.);

   if((ele->getTypeForMSH() == MSH_HEX_8 or ele->getTypeForMSH() == MSH_QUA_4) and !_fullDg)
   {
      for (int ipg = 0; ipg < npts; ipg++)
      {
        //double uipg = GP[ipg].pt[0]; double vipg = GP[ipg].pt[1]; double wipg = GP[ipg].pt[2];
        //Gradsipg.clear();
        //space1.gradf(ele,uipg,vipg,wipg,Gradsipg);
        //std::vector<TensorialTraits<double>::GradType> &Gradsipg = vall[ipg]->_vgrads;
        std::vector<TensorialTraits<double>::GradType>& Gradsipg = vipv[ipg]->gradf(&space1,ele,GP[ipg],hessianflag,thirdflag);

        const STensor43 *Hipg =&vipv[ipg]->getConstRefToTangentModuli();
        for(int k=0;k<nbFF;k++){
          SVector3 HBaverageTmp(0.);
          Bipg(0) = Gradsipg[k][0];
          Bipg(1) = Gradsipg[k][1];
          Bipg(2) = Gradsipg[k][2];
          for(int ll=0; ll<3; ll++)
          {
            for(int n=0; n<3; n++)
            {
              HBaverageTmp(ll) += (Hipg->operator()(0,0,ll,n)+Hipg->operator()(1,1,ll,n)+Hipg->operator()(2,2,ll,n))*Bipg(n)/3./npts;
            }
          }
          HBaverage[k]+=HBaverageTmp; //.push_back(HBaverageTmp);
        }
      }
    }
    for (int i = 0; i < npts; i++)
    {
        // Coordonate of Gauss' point i
//      double u = GP[i].pt[0]; double v = GP[i].pt[1]; double w = GP[i].pt[2];
      // Weight of Gauss' point i
      double weight = GP[i].weight;
//      double detJ = ele->getJacobian(u, v, w, jac);
//      double detJ = vall[i]->_detJ;
      double& detJ = vipv[i]->getJacobianDeterminant(&space1,ele,GP[i],hessianflag,thirdflag);
      double ratio = detJ*weight;
      // x, y, z
      const STensor43 *H =&vipv[i]->getConstRefToTangentModuli();
//      Grads.clear();
//      space1.gradf(ele,u,v,w,Grads);
  //    std::vector<TensorialTraits<double>::GradType> &Grads = vall[i]->_vgrads;
      std::vector<TensorialTraits<double>::GradType> &Grads = vipv[i]->gradf(&space1,ele,GP[i],hessianflag,thirdflag);
      for(int j=0;j<nbFF; j++)
      {
        BT(0) = Grads[j][0];
        BT(1) = Grads[j][1];
        BT(2) = Grads[j][2];
	    for(int k=0;k<nbFF;k++){
	      // x y z
          B(0) = Grads[k][0];
          B(1) = Grads[k][1];
          B(2) = Grads[k][2];

          for(int kk=0;kk<3;kk++)
          {
             for(int ll=0; ll<3; ll++)
             {
               if((ele->getTypeForMSH() == MSH_HEX_8 or ele->getTypeForMSH() == MSH_QUA_4) and !_fullDg)
               {
                 for(int m=0; m<3; m++)
                 {
                   for(int n=0; n<3; n++)
                   {
                     mStiff(j+kk*nbFF,k+ll*nbFF) += ratio*(BT(m)*H->operator()(kk,m,ll,n)*B(n));
                   }
                   mStiff(j+kk*nbFF,k+ll*nbFF) -= ratio*(BT(kk)*(H->operator()(0,0,ll,m)+H->operator()(1,1,ll,m)+H->operator()(2,2,ll,m))*B(m))/3.;
                 }
                 mStiff(j+kk*nbFF,k+ll*nbFF) += ratio*(BT(kk)*HBaverage[k](ll));
               }
               else
               {
                 for(int m=0; m<3; m++)
                 {
                   for(int n=0; n<3; n++)
                   {
                     mStiff(j+kk*nbFF,k+ll*nbFF) += ratio*(BT(m)*H->operator()(kk,m,ll,n)*B(n));
                   }
                 }
               }
            }
          }
        }
      }
    }
   //mStiff.print("bulk");
  }
  else
    printf("not implemented\n");



}

void dG3DForceInter::get(MElement *ele, int npts, IntPt *GP, fullVector<double> &m)const
{
  const fullVector<double> &datafield = *_data;
  MInterfaceElement *ie = dynamic_cast<MInterfaceElement*>(ele);
  const int nbFFm = ie->getElem(0)->getNumVertices();
  const int nbFFp = ie->getElem(1)->getNumVertices();
  const int nbdofm = _minusSpace->getNumKeys(ie->getElem(0));
  const int nbdofp = _plusSpace->getNumKeys(ie->getElem(1));
  m.resize(nbdofm+nbdofp);
  m.scale(0.);
 if(_fullDg)
 {
    // characteristic size
    const double hs = ie->getCharacteristicSize();
    const double b1hs = _beta1/hs;

    // get value at gauss's point
    AllIPState::ipstateElementContainer *vips = _ipf->getAips()->getIPstate(ie->getNum());
    // Values on minus and plus elements
    IntPt *GPm; IntPt *GPp;
    _interQuad->getIntPoints(ie,GP,&GPm,&GPp);
    // val grads and detJ at all Gauss points
    //std::vector<std::vector<TensorialTraits<double>::GradType> > vgradsuvw;
    //std::vector<GaussPointSpaceValues<double>*> vallm;
    //std::vector<GaussPointSpaceValues<double>*> vallp;
    //_minusSpace->getgradfuvw(ele,npts,GP,vgradsuvw);  //use a special functional space ??
    //_minusSpace->get(ie->getElem(0),npts,GPm,vallm);
    //_plusSpace->get(ie->getElem(1),npts,GPp,vallp);

    double jac[3][3];

    bool hessianflagMinus = false;
    bool thirdflagMinus = false;
    if (_mlawMinus->isHighOrder()) {
      hessianflagMinus = true;
      if (_fullDg) thirdflagMinus = true;
    };

    bool hessianflagPlus = false;
    bool thirdflagPlus = false;
    if (_mlawPlus->isHighOrder()) {
      hessianflagPlus = true;
      if (_fullDg) thirdflagPlus = true;
    };

    // loop on Gauss Point
    for(int i=0;i<npts; i++)
    {
      // Coordonate of Gauss' point i
      const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
      // Weight of Gauss' point i and Jacobian's value at this point
      const double weight = GP[i].weight;
      double detJ = ele->getJacobian(u, v, w, jac);
 //     double detJ = ele->getJacobian(vgradsuvw[i], jac);

      // IP
      const IPStateBase *ipsm        = (*vips)[i];
      const dG3DIPVariableBase *ipvm     = static_cast<const dG3DIPVariableBase*>(ipsm->getState(IPStateBase::current));
      const dG3DIPVariableBase *ipvmprev = static_cast<const dG3DIPVariableBase*>(ipsm->getState(IPStateBase::previous));
      const dG3DIPVariableBase *ipvm0    = static_cast<const dG3DIPVariableBase*>(ipsm->getState(IPStateBase::initial));

      const SVector3 &referenceOutwardNormalm = ipvm->getConstRefToReferenceOutwardNormal();

      const IPStateBase *ipsp        = (*vips)[i+npts];
      const dG3DIPVariableBase *ipvp     = static_cast<const dG3DIPVariableBase*>(ipsp->getState(IPStateBase::current));
      const dG3DIPVariableBase *ipvp0    = static_cast<const dG3DIPVariableBase*>(ipsp->getState(IPStateBase::initial));

      const SVector3 &referenceOutwardNormalp = ipvp->getConstRefToReferenceOutwardNormal();
      const SVector3 &jump = ipvp->getConstRefToJump();


      // Compute of Hessian of SF. Each component of the vector is link to a shape function.
//      Gradsm.resize(0); Gradsp.resize(0);
//      double uem = GPm[i].pt[0]; double vem = GPm[i].pt[1]; double wem = GPm[i].pt[2];
//      double uep = GPp[i].pt[0]; double vep = GPp[i].pt[1]; double wep = GPp[i].pt[2];
       //Msg::Error("New metho u %e %e v %e %e w %e %e\n",uem, uep, vem, vep, wem, wep);
      //ie->getuvwOnElem(u,v,uem,vem,wem,uep,vep,wep);
       //Msg::Error("Old metho u %e %e v %e %e w %e %e\n",uem, uep, vem, vep, wem, wep);
//      _minusSpace->gradf(ie->getElem(0),uem, vem, wem, Gradsm);
//      _plusSpace->gradf(ie->getElem(1),uep, vep, wep, Gradsp);
  //    std::vector<TensorialTraits<double>::GradType> &Gradsm = vallm[i]->_vgrads;
  //    std::vector<TensorialTraits<double>::GradType> &Gradsp = vallp[i]->_vgrads;

      std::vector<TensorialTraits<double>::GradType> &Gradsm = ipvm->gradf(_minusSpace,ie->getElem(0),GPm[i],hessianflagMinus,thirdflagMinus);
      std::vector<TensorialTraits<double>::GradType> &Gradsp = ipvp->gradf(_plusSpace,ie->getElem(1),GPp[i],hessianflagPlus,thirdflagPlus);
      // possibility of fracture
      bool broken = false;
//    bool prevbroken = false;
      if(_mlawMinus->getType() == materialLaw::fracture )
      {
        const FractureCohesive3DIPVariable *ipvwf = static_cast<const FractureCohesive3DIPVariable*>(ipvm); // broken via minus (OK broken on both sides)
        const FractureCohesive3DIPVariable *ipvwfprev = static_cast<const FractureCohesive3DIPVariable*>(ipvmprev);
        broken = ipvwfprev->isbroken(); // use continuity principle at fracture initialization to avoid instability
//      prevbroken = false; //ipvwfprev->broken();
      }
      const double wJ = detJ* weight;
      if(!broken)
      {
        SVector3 nu = referenceOutwardNormalm*(1./referenceOutwardNormalm.norm());

        // shape functions (no rotation ?)
//        Valsm.clear(); Valsp.clear();
//        _minusSpace->fuvw(ie->getElem(0),uem, vem, wem, Valsm);
//        _plusSpace->fuvw(ie->getElem(1),uep, vep, wep, Valsp);
     //   std::vector<TensorialTraits<double>::ValType> &Valsm = vallm[i]->_vvals;
      //  std::vector<TensorialTraits<double>::ValType> &Valsp = vallp[i]->_vvals;
        std::vector<TensorialTraits<double>::ValType> &Valsm = ipvm->f(_minusSpace,ie->getElem(0),GPm[i],hessianflagMinus,thirdflagMinus);
        std::vector<TensorialTraits<double>::ValType> &Valsp = ipvp->f(_plusSpace,ie->getElem(1),GPp[i],hessianflagPlus,thirdflagPlus);
        // consistency \mean{P}
        SVector3 meanPN;
        for(int k =0; k<3; k++)
        {
          for(int l =0; l<3; l++)
          {
            meanPN[k] += (ipvp->getConstRefToFirstPiolaKirchhoffStress()(k,l)+ipvm->getConstRefToFirstPiolaKirchhoffStress()(k,l))*(nu(l)/2.);
          }
        }
        // Stability  N \mean{H} (jump N)
        SVector3 NMeanHJumpNBetasc;
        STensor43 meanH = (ipvp->getConstRefToElasticTangentModuli()+ipvm->getConstRefToElasticTangentModuli())*(1./2.);
        for(int k = 0; k <3; k++)
        {
          for(int l = 0; l <3; l++)
          {
            for(int m = 0; m <3; m++)
            {
              for(int n = 0; n <3; n++)
              {
                NMeanHJumpNBetasc(k) += meanH(k,l,m,n)*jump(m)*nu(n)*b1hs*nu(l);
              }
            }
          }
        }
        // Assembly consistency + stability (if not broken)
        for(int j=0;j<nbFFm;j++){
          for(int k=0;k<3;k++){
            m(j+k*nbFFm) -= (meanPN[k]+NMeanHJumpNBetasc[k])*(Valsm[j+0*nbFFm]*wJ);
          }
        }
        for(int j=0;j<nbFFp;j++){
          for(int k=0;k<3;k++){
            m(j+k*nbFFp+nbdofm) += (meanPN[k]+NMeanHJumpNBetasc[k])*(Valsp[j+0*nbFFp]*wJ);
          }
        }
        // compatibility membrane (4 terms to assembly)
        STensor3 HmJumpN;
        STensor3 HpJumpN;
        for(int k = 0; k <3; k++)
        {
          for(int l = 0; l <3; l++)
          {
            for(int m = 0; m <3; m++)
            {
              for(int n = 0; n <3; n++)
              {
                HmJumpN(k,l) += ipvm->getConstRefToElasticTangentModuli()(m,n,k,l)*jump(m)*nu(n);
                HpJumpN(k,l) += ipvp->getConstRefToElasticTangentModuli()(m,n,k,l)*jump(m)*nu(n);
              }
            }
          }
        }
        // Assembly (loop on shape function)
        for(int j=0;j<nbFFm;j++)
        {
          for(int k=0;k<3;k++)
          {
            for(int l=0;l<3;l++)
            {
              m(j+k*nbFFm) += (HmJumpN(k,l)*Gradsm[j+0*nbFFm][l])*(wJ/2.);
            }
          }
        }
        for(int j=0;j<nbFFm;j++)
        {
          for(int k=0;k<3;k++)
          {
            for(int l=0;l<3;l++)
            {
              m(j+k*nbFFp+nbdofm) += (HpJumpN(k,l)*Gradsp[j+0*nbFFp][l])*(wJ/2.);
            }
          }
        }
      }
      else //broken case
      {
        const SVector3 &interfaceForce = ipvm->getConstRefToInterfaceForce();
//        Valsm.clear(); Valsp.clear();
//        _minusSpace->fuvw(ie->getElem(0),uem, vem, wem, Valsm);
//        _plusSpace->fuvw(ie->getElem(1),uep, vep, wep, Valsp);
//        std::vector<TensorialTraits<double>::ValType> &Valsm = vallm[i]->_vvals;
//        std::vector<TensorialTraits<double>::ValType> &Valsp = vallp[i]->_vvals;
        std::vector<TensorialTraits<double>::ValType> &Valsm = ipvm->f(_minusSpace,ie->getElem(0),GPm[i],hessianflagMinus,thirdflagMinus);
        std::vector<TensorialTraits<double>::ValType> &Valsp = ipvp->f(_plusSpace,ie->getElem(1),GPp[i],hessianflagPlus,thirdflagPlus);

        // Assembly consistency + stability (if not broken)
        for(int j=0;j<nbFFm;j++){
          for(int k=0;k<3;k++){
            m(j+k*nbFFm) -= interfaceForce[k]*(Valsm[j+0*nbFFm]*wJ);
          }
        }
        for(int j=0;j<nbFFp;j++){
          for(int k=0;k<3;k++){
            m(j+k*nbFFp+nbdofm) += interfaceForce[k]*(Valsp[j+0*nbFFp]*wJ);
          }
        }
      }
    }
  }
}

void dG3DStiffnessInter::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &stiff) const
{
  MInterfaceElement *ie = dynamic_cast<MInterfaceElement*>(ele);
  const int nbFFm = ie->getElem(0)->getNumVertices();
  const int nbFFp = ie->getElem(1)->getNumVertices();
  const int nbdofm = _minusSpace->getNumKeys(ie->getElem(0));
  const int nbdofp = _plusSpace->getNumKeys(ie->getElem(1));
  stiff.resize(nbdofm+nbdofp,nbdofm+nbdofp);
  stiff.scale(0.);
  double jac[3][3];
  if(_fullDg)
  {
    // characteristic size
    const double hs = ie->getCharacteristicSize();
    const double b1hs = _beta1/hs;

    // get value at gauss's point
    AllIPState::ipstateElementContainer *vips = _ipf->getAips()->getIPstate(ie->getNum());
    // Values on minus and plus elements
    IntPt *GPm; IntPt *GPp;
    _interQuad->getIntPoints(ie,GP,&GPm,&GPp);
    // val grad and detJ
    //std::vector<std::vector<TensorialTraits<double>::GradType> >vgradsuvw;
    //std::vector<GaussPointSpaceValues<double>*> vallm;
    //std::vector<GaussPointSpaceValues<double>*> vallp;
    //_minusSpace->getgradfuvw(ele,npts,GP,vgradsuvw); // create a specific functional space ??
    //_minusSpace->get(ie->getElem(0),npts,GPm,vallm);
    //_plusSpace->get(ie->getElem(1),npts,GPp,vallp);

    bool hessianflagMinus = false;
    bool thirdflagMinus = false;
    if (_mlawMinus->isHighOrder()) {
      hessianflagMinus = true;
      if (_fullDg) thirdflagMinus = true;
    };

    bool hessianflagPlus = false;
    bool thirdflagPlus = false;
    if (_mlawPlus->isHighOrder()) {
      hessianflagPlus = true;
      if (_fullDg) thirdflagPlus = true;
    };

//    double jac[3][3];

    // loop on Gauss Point
    for(int i=0;i<npts; i++)
    {
      // Coordonate of Gauss' point i
      const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
      // Weight of Gauss' point i and Jacobian's value at this point
      const double weight = GP[i].weight;
      double detJ = ele->getJacobian(u, v, w, jac);
//      double detJ = ele->getJacobian(vgradsuvw[i], jac);

      // IP
      const IPStateBase *ipsm        = (*vips)[i];
      const dG3DIPVariableBase *ipvm     = static_cast<const dG3DIPVariableBase*>(ipsm->getState(IPStateBase::current));
      const dG3DIPVariableBase *ipvmprev = static_cast<const dG3DIPVariableBase*>(ipsm->getState(IPStateBase::previous));
      const dG3DIPVariableBase *ipvm0    = static_cast<const dG3DIPVariableBase*>(ipsm->getState(IPStateBase::initial));

      const SVector3 &referenceOutwardNormalm = ipvm->getConstRefToReferenceOutwardNormal();

      const IPStateBase *ipsp        = (*vips)[i+npts];
      const dG3DIPVariableBase *ipvp     = static_cast<const dG3DIPVariableBase*>(ipsp->getState(IPStateBase::current));
      const dG3DIPVariableBase *ipvp0    = static_cast<const dG3DIPVariableBase*>(ipsp->getState(IPStateBase::initial));

      const SVector3 &referenceOutwardNormalp = ipvp->getConstRefToReferenceOutwardNormal();
      const SVector3 &jump = ipvp->getConstRefToJump();


      // Compute of Hessian of SF. Each component of the vector is link to a shape function.
//      Gradsm.resize(0); Gradsp.resize(0);
//      double uem = GPm[i].pt[0]; double vem = GPm[i].pt[1]; double wem = GPm[i].pt[2];
//      double uep = GPp[i].pt[0]; double vep = GPp[i].pt[1]; double wep = GPp[i].pt[2];
      //ie->getuvwOnElem(u,v,uem,vem,wem,uep,vep,wep);
//      _minusSpace->gradf(ie->getElem(0),uem, vem, wem, Gradsm);
//      _plusSpace->gradf(ie->getElem(1),uep, vep, wep, Gradsp);
//      std::vector<TensorialTraits<double>::GradType> &Gradsm = vallm[i]->_vgrads;
//      std::vector<TensorialTraits<double>::GradType> &Gradsp = vallp[i]->_vgrads;
      std::vector<TensorialTraits<double>::GradType> &Gradsm = ipvm->gradf(_minusSpace,ie->getElem(0),GPm[i],hessianflagMinus,thirdflagMinus);
      std::vector<TensorialTraits<double>::GradType> &Gradsp = ipvp->gradf(_plusSpace,ie->getElem(1),GPp[i],hessianflagPlus,thirdflagPlus);


      // possibility of fracture
      bool broken = false;
//    bool prevbroken = false;
      if(_mlawMinus->getType() == materialLaw::fracture )
      {
        const FractureCohesive3DIPVariable *ipvwf = static_cast<const FractureCohesive3DIPVariable*>(ipvm); // broken via minus (OK broken on both sides)
        const FractureCohesive3DIPVariable *ipvwfprev = static_cast<const FractureCohesive3DIPVariable*>(ipvmprev);
        broken = ipvwfprev->isbroken(); // use continuity principle at fracture initialization to avoid instability
//      prevbroken = false; //ipvwfprev->broken();
      }
      const double wJ = detJ* weight;
      SVector3 nu = referenceOutwardNormalm*(1./referenceOutwardNormalm.norm());

      // shape functions (no rotation ?)
//      Valsm.clear(); Valsp.clear();
//      _minusSpace->fuvw(ie->getElem(0),uem, vem, wem, Valsm);
//      _plusSpace->fuvw(ie->getElem(1),uep, vep, wep, Valsp);
 //     std::vector<TensorialTraits<double>::ValType> &Valsm = vallm[i]->_vvals;
  //    std::vector<TensorialTraits<double>::ValType> &Valsp = vallp[i]->_vvals;
      std::vector<TensorialTraits<double>::ValType> &Valsm = ipvm->f(_minusSpace,ie->getElem(0),GPm[i],hessianflagMinus,thirdflagMinus);
      std::vector<TensorialTraits<double>::ValType> &Valsp = ipvp->f(_plusSpace,ie->getElem(1),GPp[i],hessianflagPlus,thirdflagPlus);

      if(broken)
      {
        const STensor3 &DInterfaceForceDjump = ipvm->getConstRefToDInterfaceForceDjump();
        // Assembly (if broken)
	    for(int j=0;j<nbFFm;j++)
	    {
	      for(int k=0;k<3;k++)
	      {
	        for(int l=0;l<nbFFm;l++)
	        {
	          for(int m=0;m<3;m++)
	          {
	            stiff(j+k*nbFFm,l+m*nbFFm) += DInterfaceForceDjump(k,m)*(Valsm[j+0*nbFFm]*Valsm[l+0*nbFFm]*wJ);
	          }
	        }
	        for(int l=0;l<nbFFp;l++)
	        {
	          for(int m=0;m<3;m++)
	          {
	            stiff(j+k*nbFFm,l+m*nbFFp+nbdofm) -= DInterfaceForceDjump(k,m)*(Valsm[j+0*nbFFm]*Valsp[l+0*nbFFp]*wJ);
	          }
	        }
	      }
	    }
	    for(int j=0;j<nbFFp;j++)
	    {
	      for(int k=0;k<3;k++)
	      {
	        for(int l=0;l<nbFFm;l++)
	        {
	          for(int m=0;m<3;m++)
	          {
	            stiff(j+k*nbFFp+nbdofm,l+m*nbFFm) -= DInterfaceForceDjump(k,m)*(Valsp[j+0*nbFFp]*Valsm[l+0*nbFFm]*wJ);
	          }
	        }
	        for(int l=0;l<nbFFp;l++)
	        {
              for(int m=0;m<3;m++)
	          {
	            stiff(j+k*nbFFp+nbdofm,l+m*nbFFp+nbdofm) += DInterfaceForceDjump(k,m)*(Valsp[j+0*nbFFp]*Valsp[l+0*nbFFp]*wJ);
	          }
	        }
	      }
	    }
      }
      else
      {
        // consistency D\mean{P}DF
        const STensor43 &Hp = ipvp->getConstRefToTangentModuli();
        const STensor43 &Hm = ipvm->getConstRefToTangentModuli();
        const STensor43 &eHp = ipvp->getConstRefToElasticTangentModuli();
        const STensor43 &eHm = ipvm->getConstRefToElasticTangentModuli();
        // Assembly consistency
        for(int j=0;j<nbFFm;j++)
        {
          for(int k=0;k<3;k++)
          {
            for(int l=0;l<nbFFm;l++)
            {
              for(int m=0;m<3;m++)
              {
                for(int p = 0; p< 3; p++)
                {
                  for(int q = 0; q< 3; q++)
                  {
                    stiff(j+k*nbFFm,l+m*nbFFm) -= Hm(k,q,m,p)*(Gradsm[l+0*nbFFm][p]*Valsm[j+0*nbFFm]*wJ/2.*nu(q));
                  }
                }
              }
            }
            for(int l=0;l<nbFFp;l++)
            {
              for(int m=0;m<3;m++)
              {
                for(int p = 0; p< 3; p++)
                {
                  for(int q = 0; q< 3; q++)
                  {
                    stiff(j+k*nbFFm,l+m*nbFFp+nbdofm) -= Hp(k,q,m,p)*(Gradsp[l+0*nbFFp][p]*Valsm[j+0*nbFFm]*wJ/2.*nu(q));
                  }
                }
              }
            }
          }
        }
        for(int j=0;j<nbFFp;j++)
        {
          for(int k=0;k<3;k++)
          {
            for(int l=0;l<nbFFm;l++)
            {
              for(int m=0;m<3;m++)
              {
                for(int p = 0; p< 3; p++)
                {
                  for(int q = 0; q< 3; q++)
                  {
                    stiff(j+k*nbFFp+nbdofm,l+m*nbFFm) += Hm(k,q,m,p)*(Gradsm[l+0*nbFFm][p]*Valsp[j+0*nbFFp]*wJ/2.*nu(q));
                  }
                }
              }
            }
            for(int l=0;l<nbFFp;l++)
            {
              for(int m=0;m<3;m++)
              {
                for(int p = 0; p< 3; p++)
                {
                  for(int q = 0; q< 3; q++)
                  {
                    stiff(j+k*nbFFp+nbdofm,l+m*nbFFp+nbdofm) += Hp(k,q,m,p)*(Gradsp[l+0*nbFFp][p]*Valsp[j+0*nbFFp]*wJ/2.*nu(q));
                  }
                }
              }
            }
          }
        }

        // Stability  N \mean{H} (N)
        STensor3 NMeanHNBetasc;
        for(int k = 0; k <3; k++)
        {
          for(int l = 0; l <3; l++)
          {
            for(int m = 0; m <3; m++)
            {
              for(int n = 0; n <3; n++)
              {
                NMeanHNBetasc(k,m) += (eHp(k,l,m,n)+eHm(k,l,m,n))*nu(n)*b1hs*nu(l)/2.;
              }
            }
          }
        }
        // Assembly stability (if not broken)
        for(int j=0;j<nbFFm;j++)
        {
          for(int k=0;k<3;k++)
          {
            for(int l=0;l<nbFFm;l++)
            {
              for(int m=0;m<3;m++)
              {
                stiff(j+k*nbFFm,l+m*nbFFm) += NMeanHNBetasc(k,m)*(Valsm[j+0*nbFFm]*Valsm[l+0*nbFFm]*wJ);
              }
            }
            for(int l=0;l<nbFFp;l++)
            {
              for(int m=0;m<3;m++)
              {
                stiff(j+k*nbFFm,l+m*nbFFp+nbdofm) -= NMeanHNBetasc(k,m)*(Valsm[j+0*nbFFm]*Valsp[l+0*nbFFp]*wJ);
              }
            }
          }
        }
        for(int j=0;j<nbFFp;j++)
        {
          for(int k=0;k<3;k++)
          {
            for(int l=0;l<nbFFm;l++)
            {
              for(int m=0;m<3;m++)
              {
                stiff(j+k*nbFFp+nbdofm,l+m*nbFFm) -= NMeanHNBetasc(k,m)*(Valsp[j+0*nbFFp]*Valsm[l+0*nbFFm]*wJ);
              }
            }
            for(int l=0;l<nbFFp;l++)
            {
              for(int m=0;m<3;m++)
              {
                stiff(j+k*nbFFp+nbdofm,l+m*nbFFp+nbdofm) += NMeanHNBetasc(k,m)*(Valsp[j+0*nbFFp]*Valsp[l+0*nbFFp]*wJ);
              }
            }
          }
        }
        // compatibility membrane (4 terms to assembly)
        STensor3 HmJumpN;
        STensor3 HpJumpN;
        for(int k = 0; k <3; k++)
        {
          for(int l = 0; l <3; l++)
          {
            for(int m = 0; m <3; m++)
            {
              for(int n = 0; n <3; n++)
              {
                HmJumpN(k,l) += eHm(m,n,k,l)*jump(m)*nu(n);
                HpJumpN(k,l) += eHp(m,n,k,l)*jump(m)*nu(n);
              }
            }
          }
        }
        // Assembly (loop on shape function)
        for(int j=0;j<nbFFm;j++)
        {
          for(int k=0;k<3;k++)
          {
            for(int l=0;l<nbFFm;l++)
            {
              for(int m=0;m<3;m++)
              {
                for(int p =0; p < 3; p++)
                {
                  for(int q =0; q < 3; q++)
                  {
                    stiff(j+k*nbFFm,l+m*nbFFm) -= (eHm(m,p,k,q)*Gradsm[j+0*nbFFm][q])*(nu(p)*wJ/2.*Valsm[l+0*nbFFm]);
                  }
                }
              }
            }
            for(int l=0;l<nbFFp;l++)
            {
              for(int m=0;m<3;m++)
              {
                for(int p =0; p < 3; p++)
                {
                  for(int q =0; q < 3; q++)
                  {
                    stiff(j+k*nbFFm,l+m*nbFFp+nbdofm) += (eHm(m,p,k,q)*Gradsm[j+0*nbFFm][q])*(nu(p)*wJ/2.*Valsp[l+0*nbFFp]);
                  }
                }
              }
            }
          }
        }
        for(int j=0;j<nbFFp;j++)
        {
          for(int k=0;k<3;k++)
          {
            for(int l=0;l<nbFFm;l++)
            {
              for(int m=0;m<3;m++)
              {
                for(int p =0; p < 3; p++)
                {
                  for(int q =0; q < 3; q++)
                  {
                    stiff(j+k*nbFFp+nbdofm,l+m*nbFFm) -= (eHp(m,p,k,q)*Gradsp[j+0*nbFFp][q])*(nu(p)*wJ/2.*Valsm[l+0*nbFFm]);
                  }
                }
              }
            }
            for(int l=0;l<nbFFp;l++)
            {
              for(int m=0;m<3;m++)
              {
                for(int p =0; p < 3; p++)
                {
                  for(int q =0; q < 3; q++)
                  {
                    stiff(j+k*nbFFp+nbdofm,l+m*nbFFp+nbdofm) += (eHp(m,p,k,q)*Gradsp[j+0*nbFFp][q])*(nu(p)*wJ/2.*Valsp[l+0*nbFFp]);
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

void nonLocalMassG3D::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const{
  mass3D::get(ele,npts,GP,m);
}


void nonLocalDG3DForceBulk::get(MElement *ele,int npts,IntPt *GP,fullVector<double> &vFor) const
{

  // xyz Part
  dG3DForceBulk::get(ele,npts,GP,vFor);

  int nbdof = this->space1.getNumKeys(ele);
  int nbFF = ele->getNumShapeFunctions();

//  double jac[3][3];
  SVector3 BTepsbar;
  SVector3 Bepsbar;

  // get gauss' point data
  _ipf->getIPv(ele,vipv,vipvprev);
  // grads and jacobian at all Gauss points
  //std::vector<GaussPointSpaceValues<double>*> vall;
  //nlsFunctionSpaceXYZ<double>* sp1 = static_cast<nlsFunctionSpaceXYZ<double>*>(&(this->space1));
  //sp1->get(ele,npts,GP,vall);
  bool hessianflag = false;
  bool thirdflag = false;
  if (_mlaw->isHighOrder()) {
    hessianflag = true;
    if (_fullDg) thirdflag = true;
  };
  for (int i = 0; i < npts; i++)
  {
//    double u = GP[i].pt[0]; double v = GP[i].pt[1]; double w = GP[i].pt[2];
    // Weight of Gauss' point i
    double weight = GP[i].weight;
//    double detJ = ele->getJacobian(u, v, w, jac);
//    double detJ = vall[i]->_detJ;
    double &detJ = vipv[i]->getJacobianDeterminant(&space1,ele,GP[i],hessianflag,thirdflag);

    const STensor3 *PK1 = &(vipv[i]->getConstRefToFirstPiolaKirchhoffStress());
    const STensor3 *cg = &(static_cast <const nonLocalDamageDG3DIPVariableBase * > (vipv[i])) -> 	
				getConstRefToCharacteristicLengthMatrix();           // by Wu Ling
    double ratio = detJ*weight;
    double eqRatio = _nonLocalEqRatio;                    //by Wu Ling

    double effectivePlasticStrain = (static_cast <const nonLocalDamageDG3DIPVariableBase * > (vipv[i]))->
                                          getEffectivePlasticStrain();
    const SVector3 &gradepl = ((static_cast <const nonLocalDamageDG3DIPVariableBase * > (vipv[i]))->
                                          getConstRefToGradNonLocalPlasticStrain());

//    Grads.clear();
//    space1.gradf(ele,u, v, w, Grads); // a optimiser
//    Vals.clear();
//    space1.fuvw(ele,u, v, w, Vals);
//    std::vector<TensorialTraits<double>::GradType> &Grads = vall[i]->_vgrads;
//    std::vector<TensorialTraits<double>::ValType> &Vals = vall[i]->_vvals;
    std::vector<TensorialTraits<double>::GradType> &Grads = vipv[i]->gradf(&space1,ele,GP[i],hessianflag,thirdflag);
    std::vector<TensorialTraits<double>::ValType> &Vals = vipv[i]->f(&space1,ele,GP[i],hessianflag,thirdflag);

    const int threeTimesNbFF = 3*nbFF;
    for(int j=0;j<nbFF; j++)
    {
      BTepsbar(0) = Grads[j+threeTimesNbFF][0];
      BTepsbar(1) = Grads[j+threeTimesNbFF][1];
      BTepsbar(2) = Grads[j+threeTimesNbFF][2];
      // epsbar
      /*for(int k=0;k<nbFF; k++)
      {
        Bepsbar(0) = Grads[k+threeTimesNbFF][0];
        Bepsbar(1) = Grads[k+threeTimesNbFF][1];
	    Bepsbar(2) = Grads[k+threeTimesNbFF][2];
        double BtB = 0.;
        for(int m=0; m<3; m++)
        {
            for(int n=0; n<3; n++)
            {
             BtB += BTepsbar(m)*(cg->operator()(m,n))*Bepsbar(n);    // by Wu Ling
            }
        }
        vFor(j+threeTimesNbFF) += eqRatio*ratio*(Vals[j+threeTimesNbFF]*Vals[k+threeTimesNbFF]+ BtB)*_data->operator()(k+threeTimesNbFF);
      }*/
      vFor(j+3*nbFF) += eqRatio*ratio*(Vals[j+3*nbFF]*effectivePlasticStrain);
      for(int m=0; m<3; m++)
      {
        for(int n=0; n<3; n++)
        {
          vFor(j+3*nbFF) += eqRatio*ratio*(BTepsbar(m)*cg->operator()(m,n)*gradepl(n));
        }
      }
      vFor(j+threeTimesNbFF) -= eqRatio*ratio*(Vals[j+threeTimesNbFF]*
                              (static_cast <const nonLocalDamageDG3DIPVariableBase * > (vipv[i]))->
                                              getCurrentPlasticStrain());
    }
  }
  // vFor.print("interF");
}

void nonLocalDG3DStiffnessBulk::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &mStiff) const
{
  dG3DStiffnessBulk::get(ele,npts,GP,mStiff);
  if (sym)
  {
    bool hessianflag = false;
    bool thirdflag = false;
    if (_mlaw->isHighOrder()) {
      hessianflag = true;
      if (_fullDg) thirdflag = true;
    };
    // Initialization of some data
    int nbdof = BilinearTerm<double,double>::space1.getNumKeys(ele);
    int nbFF = ele->getNumShapeFunctions();

    // get ipvariables
    _ipf->getIPv(ele,vipv);
    // val, grad and detJ at all Gauss points
    //nlsFunctionSpaceXYZ<double> *sp1 = static_cast<nlsFunctionSpaceXYZ<double>*>(&(this->space1));
    //std::vector<GaussPointSpaceValues<double>*> vall;
    //sp1->get(ele,npts,GP,vall);

    // sum on Gauss' points
//    double jac[3][3];
    SVector3 Bepsbar(3);
    SVector3 BTepsbar(3);
    SVector3 newB,newBT;

    for (int i = 0; i < npts; i++)
    {
        // Coordonate of Gauss' point i
//      double u = GP[i].pt[0]; double v = GP[i].pt[1]; double w = GP[i].pt[2];
      // Weight of Gauss' point i
      double weight = GP[i].weight;
//      double detJ = ele->getJacobian(u, v, w, jac);
//      double detJ = vall[i]->_detJ;
      double &detJ = vipv[i]->getJacobianDeterminant(&space1,ele,GP[i],hessianflag,thirdflag);
      // x, y, z
      const STensor43 *H =&vipv[i]->getConstRefToTangentModuli();

      const STensor3  *dpds = &(static_cast <const nonLocalDamageDG3DIPVariableBase * > (vipv[i]))
				->getConstRefToDLocalPlasticStrainDStrain();
      const STensor3  *dsdp = &(static_cast <const nonLocalDamageDG3DIPVariableBase * > (vipv[i]))
				->getConstRefToDStressDNonLocalPlasticStrain();
      double          SpBar = (static_cast <const nonLocalDamageDG3DIPVariableBase * > (vipv[i]))
				->getConstRefToDLocalPlasticStrainDNonLocalPlasticStrain();
      const STensor3 *cg = &(static_cast <const nonLocalDamageDG3DIPVariableBase * > (vipv[i])) -> 	
				getConstRefToCharacteristicLengthMatrix();           // by Wu Ling

      //double length = _mlaw->getLength();
      double eqRatio = _nonLocalEqRatio;               //by Wu Ling
//      Grads.clear();
//      space1.gradf(ele,u,v,w,Grads);
//      Vals.clear();
//      space1.fuvw(ele,u, v, w, Vals);
 //     std::vector<TensorialTraits<double>::GradType> &Grads = vall[i]->_vgrads;
 //     std::vector<TensorialTraits<double>::ValType> &Vals = vall[i]->_vvals;

      std::vector<TensorialTraits<double>::GradType> &Grads = vipv[i]->gradf(&space1,ele,GP[i],hessianflag,thirdflag);
      std::vector<TensorialTraits<double>::ValType> &Vals = vipv[i]->f(&space1,ele,GP[i],hessianflag,thirdflag);

      const int threeTimesNbFF = 3*nbFF;
      for(int j=0;j<nbFF; j++)
      {
        newBT(0) = Grads[j][0];
        newBT(1) = Grads[j][1];
        newBT(2) = Grads[j][2];
        BTepsbar(0) = Grads[j+threeTimesNbFF][0];
        BTepsbar(1) = Grads[j+threeTimesNbFF][1];
	    BTepsbar(2) = Grads[j+threeTimesNbFF][2];
        for(int k=0;k<nbFF;k++){
          double ratio = detJ*weight;
	      // x y z
          newB(0) = Grads[k][0];
          newB(1) = Grads[k][1];
          newB(2) = Grads[k][2];
          Bepsbar(0) = Grads[k+threeTimesNbFF][0];
          Bepsbar(1) = Grads[k+threeTimesNbFF][1];
	      Bepsbar(2) = Grads[k+threeTimesNbFF][2];

          // epsbar
	      double BtB = 0.;
	      for(int m=0; m<3; m++)
          {
             for(int n=0; n<3; n++)
            {
             BtB += BTepsbar(m)*(cg->operator()(m,n))*Bepsbar(n);    // by Wu Ling
            }
          }
          mStiff(j+threeTimesNbFF,k+threeTimesNbFF) += eqRatio*ratio*(Vals[j+threeTimesNbFF]*Vals[k+threeTimesNbFF]*(1.-SpBar)+ BtB);
          // cross terms to check
          for(int kk=0;kk<3;kk++)
          {
            for(int m=0; m<3; m++)
            {
              mStiff(j+kk*nbFF,k+threeTimesNbFF) += ratio*(newBT(m)*dsdp->operator()(kk,m)*Vals[k+threeTimesNbFF]);
              mStiff(j+threeTimesNbFF,k+kk*nbFF) -= eqRatio*ratio*(Vals[j+threeTimesNbFF]*dpds->operator()(kk,m)*newBT(m));
            }
          }
        }
      }
    }
   //mStiff.print("bulk");
  }
  else
    printf("not implemented\n");
}

void nonLocalDG3DForceInter::get(MElement *ele, int npts, IntPt *GP, fullVector<double> &m)const
{
  dG3DForceInter::get(ele, npts, GP,m);
  const fullVector<double> &datafield = *_data;
  MInterfaceElement *ie = dynamic_cast<MInterfaceElement*>(ele);
  const int nbFFm = ie->getElem(0)->getNumVertices();
  const int nbFFp = ie->getElem(1)->getNumVertices();
  const int nbdofm = _minusSpace->getNumKeys(ie->getElem(0));
  const int nbdofp = _plusSpace->getNumKeys(ie->getElem(1));
  double jac[3][3];
  if(_fullDg)
  {
    bool hessianflagMinus = false;
    bool thirdflagMinus = false;
    if (_mlawMinus->isHighOrder()) {
      hessianflagMinus = true;
      if (_fullDg) thirdflagMinus = true;
    };

    bool hessianflagPlus = false;
    bool thirdflagPlus = false;
    if (_mlawPlus->isHighOrder()) {
      hessianflagPlus = true;
      if (_fullDg) thirdflagPlus = true;
    };
    // characteristic size
    const double hs = ie->getCharacteristicSize();
    const double nbetahs = _nonLocalBeta/hs;
    if( _continuity)
    {
      // get value at gauss's point
      AllIPState::ipstateElementContainer *vips = _ipf->getAips()->getIPstate(ie->getNum());
      // value on minus and plus elements
      IntPt *GPm; IntPt *GPp;
      _interQuad->getIntPoints(ie,GP,&GPm,&GPp);
      // Val, grad and detJ at all Gauss points
      std::vector<std::vector<TensorialTraits<double>::GradType> >vgradsuvw;
      //std::vector<GaussPointSpaceValues<double>*> vallm;
      //std::vector<GaussPointSpaceValues<double>*> vallp;
      _minusSpace->getgradfuvw(ele,npts,GP,vgradsuvw); // create a special function space ??
      //_minusSpace->get(ie->getElem(0),npts,GPm,vallm);
      //_plusSpace->get(ie->getElem(1),npts,GPp,vallp);
//      double jac[3][3];

      // loop on Gauss Point
      for(int i=0;i<npts; i++)
      {
        // Coordonate of Gauss' point i
//        const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
        // Weight of Gauss' point i and Jacobian's value at this point
        const double weight = GP[i].weight;
//        double detJ = ele->getJacobian(u, v, w, jac);
        double detJ = ele->getJacobian(vgradsuvw[i], jac);

        // IP
        const IPStateBase *ipsm        = (*vips)[i];
        const IPStateBase *ipsp        = (*vips)[i+npts];
        const nonLocalDamageDG3DIPVariableBase *ipvm;
        const nonLocalDamageDG3DIPVariableBase *ipvp;
        if(_mlawMinus->getType() == materialLaw::fracture )
        {
          FractureCohesive3DIPVariable* ipvtmp = static_cast<FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::current));
          ipvm = static_cast<nonLocalDamageDG3DIPVariableBase*>(ipvtmp->getIPvBulk());
          ipvtmp = static_cast<FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::current));
          ipvp = static_cast<nonLocalDamageDG3DIPVariableBase*>(ipvtmp->getIPvBulk());
        }
        else
        {
          ipvm = static_cast<const nonLocalDamageDG3DIPVariableBase*>(ipsm->getState(IPStateBase::current));
          ipvp = static_cast<const nonLocalDamageDG3DIPVariableBase*>(ipsp->getState(IPStateBase::current));
        }

//        const nonLocalDamageDG3DIPVariable *ipvmprev = static_cast<const nonLocalDamageDG3DIPVariable*>(ipsm->getState(IPStateBase::previous));
//        const nonLocalDamageDG3DIPVariable *ipvm0    = static_cast<const nonLocalDamageDG3DIPVariable*>(ipsm->getState(IPStateBase::initial));

        const SVector3 &referenceOutwardNormalm = ipvm->getConstRefToReferenceOutwardNormal();



 //       const nonLocalDamageDG3DIPVariable *ipvp0    = static_cast<const nonLocalDamageDG3DIPVariable*>(ipsp->getState(IPStateBase::initial));

        const SVector3 &referenceOutwardNormalp = ipvp->getConstRefToReferenceOutwardNormal();
        const double &epljump = ipvp->getConstRefToNonLocalJump();

        const STensor3  *cgm = &(ipvm->getConstRefToCharacteristicLengthMatrix());           // by Wu Ling
        const STensor3  *cgp = &(ipvp->getConstRefToCharacteristicLengthMatrix());           // by Wu Ling
        const SVector3  *gradm = &(ipvm->getConstRefToGradNonLocalPlasticStrain());
        const SVector3  *gradp = &(ipvp->getConstRefToGradNonLocalPlasticStrain());

        //double length = _mlaw->getLength();
        double eqRatio = _nonLocalEqRatio;               //by Wu Ling

        SVector3 cgGradm(0.);
        SVector3 cgGradp(0.);
        for(int k =0; k<3; k++)
        {
          for(int l =0; l<3; l++)
          {
            cgGradm[k] += cgm->operator()(k,l)*gradm->operator()(l);
            cgGradp[k] += cgp->operator()(k,l)*gradp->operator()(l);
          }
        }

        //std::vector<TensorialTraits<double>::GradType> &Gradsm = vallm[i]->_vgrads;
        //std::vector<TensorialTraits<double>::GradType> &Gradsp = vallp[i]->_vgrads;
        std::vector<TensorialTraits<double>::GradType> &Gradsm = ipvm->gradf(_minusSpace,ie->getElem(0),GPm[i],hessianflagMinus,thirdflagMinus);
        std::vector<TensorialTraits<double>::GradType> &Gradsp = ipvp->gradf(_plusSpace,ie->getElem(1),GPp[i],hessianflagPlus,thirdflagPlus);

        // possibility of fracture
        bool broken = false;
        if(_mlawMinus->getType() == materialLaw::fracture )
        {
          const FractureCohesive3DIPVariable *ipvwf = static_cast<const FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::current)); // broken via minus (OK broken on both sides)
          const FractureCohesive3DIPVariable *ipvwfprev = static_cast<const FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::previous));
          broken = ipvwfprev->isbroken(); // use continuity principle at fracture initialization to avoid instability
        }
        double wJ = detJ* weight;
        wJ*=eqRatio;
        if(!broken)
        {
          SVector3 nu = referenceOutwardNormalm*(1./referenceOutwardNormalm.norm());

          // shape functions (no rotation ?)
          //std::vector<TensorialTraits<double>::ValType> &Valsm = vallm[i]->_vvals;
          //std::vector<TensorialTraits<double>::ValType> &Valsp = vallp[i]->_vvals;

          std::vector<TensorialTraits<double>::ValType> &Valsm = ipvm->f(_minusSpace,ie->getElem(0),GPm[i],hessianflagMinus,thirdflagMinus);
          std::vector<TensorialTraits<double>::ValType> &Valsp = ipvp->f(_plusSpace,ie->getElem(1),GPp[i],hessianflagPlus,thirdflagPlus);


          // consistency \mean{cg*grad eps}
          double meanCgGradN =0.;
          for(int k =0; k<3; k++)
          {
            meanCgGradN += (cgGradm(k)+cgGradp(k))*(nu(k)/2.);
          }
          // Stability  N \mean{H} (jump N)
          double NMeanCgJumpNBetasc = 0.;
          STensor3 meanCg = ((*cgm)+(*cgp))*(1./2.);
          for(int l = 0; l <3; l++)
          {
            for(int n = 0; n <3; n++)
            {
              NMeanCgJumpNBetasc += meanCg(l,n)*epljump*nu(n)*nbetahs*nu(l);
            }
          }
          // Assembly consistency + stability (if not broken)
          for(int j=0;j<nbFFm;j++)
          {
            m(j+3*nbFFm) -= (meanCgGradN+NMeanCgJumpNBetasc)*(Valsm[j+3*nbFFm]*wJ);
          }
          for(int j=0;j<nbFFp;j++)
          {
            m(j+3*nbFFp+nbdofm) += (meanCgGradN+NMeanCgJumpNBetasc)*(Valsp[j+3*nbFFp]*wJ);
          }
          // compatibility membrane (4 terms to assembly)
          SVector3 CgmJumpN(0.);
          SVector3 CgpJumpN(0.);
          for(int k = 0; k <3; k++)
          {
            for(int m = 0; m <3; m++)
            {
              CgmJumpN(k) += cgm->operator()(k,m)*epljump*nu(m);
              CgpJumpN(k) += cgp->operator()(k,m)*epljump*nu(m);
            }
          }
          // Assembly (loop on shape function)
          for(int j=0;j<nbFFm;j++)
          {
            for(int l=0;l<3;l++)
            {
              m(j+3*nbFFm) += (CgmJumpN(l)*Gradsm[j+3*nbFFm][l])*(wJ/2.);
            }
          }
          for(int j=0;j<nbFFm;j++)
          {
            for(int l=0;l<3;l++)
            {
              m(j+3*nbFFp+nbdofm) += (CgpJumpN(l)*Gradsp[j+3*nbFFp][l])*(wJ/2.);
            }
          }
        }
        else //broken case
        {

        }
      }
    }
  }
}

void nonLocalDG3DStiffnessInter::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &stiff) const
{
  dG3DStiffnessInter::get(ele,npts,GP,stiff);

  MInterfaceElement *ie = dynamic_cast<MInterfaceElement*>(ele);
  const int nbFFm = ie->getElem(0)->getNumVertices();
  const int nbFFp = ie->getElem(1)->getNumVertices();
  const int nbdofm = _minusSpace->getNumKeys(ie->getElem(0));
  const int nbdofp = _plusSpace->getNumKeys(ie->getElem(1));
  if(_fullDg)
  {
    bool hessianflagMinus = false;
    bool thirdflagMinus = false;
    if (_mlawMinus->isHighOrder()) {
      hessianflagMinus = true;
      if (_fullDg) thirdflagMinus = true;
    };

    bool hessianflagPlus = false;
    bool thirdflagPlus = false;
    if (_mlawPlus->isHighOrder()) {
      hessianflagPlus = true;
      if (_fullDg) thirdflagPlus = true;
    };
    // characteristic size
    const double hs = ie->getCharacteristicSize();
    const double nbetahs = _nonLocalBeta/hs;
    if( _continuity)
    {
      // get value at gauss's point
      AllIPState::ipstateElementContainer *vips = _ipf->getAips()->getIPstate(ie->getNum());
      // values of Gauss points on minus and plus elements
      IntPt *GPm; IntPt *GPp;
      _interQuad->getIntPoints(ie,GP,&GPm,&GPp);
      // val, grad and detJ at all Gauss points
      std::vector<std::vector<TensorialTraits<double>::GradType> >vgradsuvw;
      //std::vector<GaussPointSpaceValues<double>*> vallm;
      //std::vector<GaussPointSpaceValues<double>*> vallp;
      _minusSpace->getgradfuvw(ele,npts,GP,vgradsuvw); // do a special function space ??
      //_minusSpace->get(ie->getElem(0),npts,GPm,vallm);
      //_plusSpace->get(ie->getElem(1),npts,GPp,vallp);

      double jac[3][3];

      // loop on Gauss Point
      for(int i=0;i<npts; i++)
      {
        // Coordonate of Gauss' point i
//        const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
        // Weight of Gauss' point i and Jacobian's value at this point
        const double weight = GP[i].weight;
//        double detJ = ele->getJacobian(u, v, w, jac);
        double detJ = ele->getJacobian(vgradsuvw[i], jac);

        // IP
        const IPStateBase *ipsm        = (*vips)[i];
        const IPStateBase *ipsp        = (*vips)[i+npts];
        const nonLocalDamageDG3DIPVariableBase *ipvm;     //= static_cast<const nonLocalDamageDG3DIPVariable*>(ipsm->getState(IPStateBase::current));
        const nonLocalDamageDG3DIPVariableBase *ipvp;     // = static_cast<const nonLocalDamageDG3DIPVariable*>(ipsp->getState(IPStateBase::current));
        if(_mlawMinus->getType() == materialLaw::fracture )
        {
          FractureCohesive3DIPVariable* ipvtmp = static_cast<FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::current));
          ipvm = static_cast<nonLocalDamageDG3DIPVariableBase*>(ipvtmp->getIPvBulk());
          ipvtmp = static_cast<FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::current));
          ipvp = static_cast<nonLocalDamageDG3DIPVariableBase*>(ipvtmp->getIPvBulk());
        }
        else
        {
          ipvm = static_cast<const nonLocalDamageDG3DIPVariableBase*>(ipsm->getState(IPStateBase::current));
          ipvp = static_cast<const nonLocalDamageDG3DIPVariableBase*>(ipsp->getState(IPStateBase::current));
        }

//        const nonLocalDamageDG3DIPVariable *ipvmprev = static_cast<const nonLocalDamageDG3DIPVariable*>(ipsm->getState(IPStateBase::previous));
//        const nonLocalDamageDG3DIPVariable *ipvm0    = static_cast<const nonLocalDamageDG3DIPVariable*>(ipsm->getState(IPStateBase::initial));

        const SVector3 &referenceOutwardNormalm = ipvm->getConstRefToReferenceOutwardNormal();



//        const nonLocalDamageDG3DIPVariable *ipvp0    = static_cast<const nonLocalDamageDG3DIPVariable*>(ipsp->getState(IPStateBase::initial));

        const SVector3 &referenceOutwardNormalp = ipvp->getConstRefToReferenceOutwardNormal();
        const SVector3 &jump = ipvp->getConstRefToJump();

        //std::vector<TensorialTraits<double>::GradType> &Gradsm = vallm[i]->_vgrads;
        //std::vector<TensorialTraits<double>::GradType> &Gradsp = vallp[i]->_vgrads;

        std::vector<TensorialTraits<double>::GradType> &Gradsm = ipvm->gradf(_minusSpace,ie->getElem(0),GPm[i],hessianflagMinus,thirdflagMinus);
        std::vector<TensorialTraits<double>::GradType> &Gradsp = ipvp->gradf(_plusSpace,ie->getElem(1),GPp[i],hessianflagPlus,thirdflagPlus);

        // possibility of fracture
        bool broken = false;
        if(_mlawMinus->getType() == materialLaw::fracture )
        {
          const FractureCohesive3DIPVariable *ipvwf = static_cast<const FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::current)); // broken via minus (OK broken on both sides)
          const FractureCohesive3DIPVariable *ipvwfprev = static_cast<const FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::previous));
          broken = ipvwfprev->isbroken(); // use continuity principle at fracture initialization to avoid instability
        }
        double wJ = detJ* weight;
        SVector3 nu = referenceOutwardNormalm*(1./referenceOutwardNormalm.norm());
        const double &epljump = ipvp->getConstRefToNonLocalJump();

        //std::vector<TensorialTraits<double>::ValType> &Valsm = vallm[i]->_vvals;
        //std::vector<TensorialTraits<double>::ValType> &Valsp = vallp[i]->_vvals;

        std::vector<TensorialTraits<double>::ValType> &Valsm = ipvm->f(_minusSpace,ie->getElem(0),GPm[i],hessianflagMinus,thirdflagMinus);
        std::vector<TensorialTraits<double>::ValType> &Valsp = ipvp->f(_plusSpace,ie->getElem(1),GPp[i],hessianflagPlus,thirdflagPlus);


        if(broken)
        {

        }
        else
        {
          const STensor3  *cgm = &(ipvm->getConstRefToCharacteristicLengthMatrix());           // by Wu Ling
          const STensor3  *cgp = &(ipvp->getConstRefToCharacteristicLengthMatrix());           // by Wu Ling
          const SVector3  *gradm = &(ipvm->getConstRefToGradNonLocalPlasticStrain());
          const SVector3  *gradp = &(ipvp->getConstRefToGradNonLocalPlasticStrain());
          const STensor3  *dsdpp = &(ipvp->getConstRefToDStressDNonLocalPlasticStrain());
          const STensor3  *dsdpm = &(ipvm->getConstRefToDStressDNonLocalPlasticStrain());

          //double length = _mlaw->getLength();
          double eqRatio = _nonLocalEqRatio;               //by Wu Ling
          wJ*=eqRatio;



          // Assembly consistency (from derivative of sigma with tilde{p}
          for(int j=0;j<nbFFm;j++)
          {
            for(int k=0;k<3;k++)
            {
              for(int l=0;l<nbFFm;l++)
              {
                for(int q = 0; q< 3; q++)
                {
                  stiff(j+k*nbFFm,l+3*nbFFm) -= dsdpm->operator()(k,q)*(Valsm[l+3*nbFFm]*Valsm[j+0*nbFFm]*wJ/2.*nu(q)/eqRatio);
                }
              }
              for(int l=0;l<nbFFp;l++)
              {
                for(int q = 0; q< 3; q++)
                {
                  stiff(j+k*nbFFm,l+3*nbFFp+nbdofm) -= dsdpp->operator()(k,q)*(Valsp[l+3*nbFFp]*Valsm[j+0*nbFFm]*wJ/2.*nu(q)/eqRatio);
                }
              }
            }
          }
          for(int j=0;j<nbFFp;j++)
          {
            for(int k=0;k<3;k++)
            {
              for(int l=0;l<nbFFm;l++)
              {
                for(int q = 0; q< 3; q++)
                {
                  stiff(j+k*nbFFp+nbdofm,l+3*nbFFm) += dsdpm->operator()(k,q)*(Valsm[l+3*nbFFm]*Valsp[j+0*nbFFp]*wJ/2.*nu(q)/eqRatio);
                }
              }
              for(int l=0;l<nbFFp;l++)
              {
                for(int q = 0; q< 3; q++)
                {
                  stiff(j+k*nbFFp+nbdofm,l+3*nbFFp+nbdofm) += dsdpp->operator()(k,q)*(Valsp[l+3*nbFFp]*Valsp[j+0*nbFFp]*wJ/2.*nu(q)/eqRatio);
                }
              }
            }
          }
          // consistency D\mean{cG grad epl} Depl
          // Assembly consistency
          for(int j=0;j<nbFFm;j++)
          {
            for(int l=0;l<nbFFm;l++)
            {
              for(int p = 0; p< 3; p++)
              {
                for(int q = 0; q< 3; q++)
                {
                  stiff(j+3*nbFFm,l+3*nbFFm) -= cgm->operator()(p,q)*(Gradsm[l+3*nbFFm][p]*Valsm[j+3*nbFFm]*wJ/2.*nu(q));
                }
              }
            }
            for(int l=0;l<nbFFp;l++)
            {
              for(int p = 0; p< 3; p++)
              {
                for(int q = 0; q< 3; q++)
                {
                  stiff(j+3*nbFFm,l+3*nbFFp+nbdofm) -= cgp->operator()(p,q)*(Gradsp[l+3*nbFFp][p]*Valsm[j+3*nbFFm]*wJ/2.*nu(q));
                }
              }
            }
          }
          for(int j=0;j<nbFFp;j++)
          {
            for(int l=0;l<nbFFm;l++)
            {
              for(int p = 0; p< 3; p++)
              {
                for(int q = 0; q< 3; q++)
                {
                  stiff(j+3*nbFFp+nbdofm,l+3*nbFFm) += cgm->operator()(p,q)*(Gradsm[l+3*nbFFm][p]*Valsp[j+3*nbFFp]*wJ/2.*nu(q));
                }
              }
            }
            for(int l=0;l<nbFFp;l++)
            {
              for(int p = 0; p< 3; p++)
              {
                for(int q = 0; q< 3; q++)
                {
                  stiff(j+3*nbFFp+nbdofm,l+3*nbFFp+nbdofm) += cgp->operator()(p,q)*
                                  (Gradsp[l+3*nbFFp][p]*Valsp[j+3*nbFFp]*wJ/2.*nu(q));
                }
              }
            }
          }

          // Stability  N \mean{cg} (N)
          double NMeanCgNBetasc=0.;
          for(int l = 0; l <3; l++)
          {
            for(int n = 0; n <3; n++)
            {
              NMeanCgNBetasc += (cgp->operator()(l,n)+cgm->operator()(l,n))*nu(n)*nbetahs*nu(l)/2.;
            }
          }
          // Assembly stability (if not broken)
          for(int j=0;j<nbFFm;j++)
          {
            for(int l=0;l<nbFFm;l++)
            {
              stiff(j+3*nbFFm,l+3*nbFFm) += NMeanCgNBetasc*(Valsm[j+3*nbFFm]*Valsm[l+3*nbFFm]*wJ);
            }
            for(int l=0;l<nbFFp;l++)
            {
              stiff(j+3*nbFFm,l+3*nbFFp+nbdofm) -= NMeanCgNBetasc*(Valsm[j+3*nbFFm]*Valsp[l+3*nbFFp]*wJ);
            }
          }
          for(int j=0;j<nbFFp;j++)
          {
            for(int l=0;l<nbFFm;l++)
            {
              stiff(j+3*nbFFp+nbdofm,l+3*nbFFm) -= NMeanCgNBetasc*(Valsp[j+3*nbFFp]*Valsm[l+3*nbFFm]*wJ);
            }
            for(int l=0;l<nbFFp;l++)
            {
              stiff(j+3*nbFFp+nbdofm,l+3*nbFFp+nbdofm) += NMeanCgNBetasc*(Valsp[j+3*nbFFp]*Valsp[l+3*nbFFp]*wJ);
            }
          }
          // compatibility membrane (4 terms to assembly)
          //SVector3 cgmJumpN;
          //SVector3 cgpJumpN;
          //for(int k = 0; k <3; k++)
          //{
           // for(int m = 0; m <3; m++)
            //{
             // cgmJumpN(k) += cgm->operator()(k,m)*epljump*nu(m);
              //cgpJumpN(k) += cgp->operator()(k,m)*epljump*nu(m);
            //}
          //}
          // Assembly (loop on shape function)
          for(int j=0;j<nbFFm;j++)
          {
            for(int l=0;l<nbFFm;l++)
            {
              for(int p =0; p < 3; p++)
              {
                for(int q =0; q < 3; q++)
                {
                  stiff(j+3*nbFFm,l+3*nbFFm) -= (cgm->operator()(p,q)*Gradsm[j+3*nbFFm][q])*(nu(p)*wJ/2.*Valsm[l+3*nbFFm]);
                }
              }
            }
            for(int l=0;l<nbFFp;l++)
            {
              for(int p =0; p < 3; p++)
              {
                for(int q =0; q < 3; q++)
                {
                  stiff(j+3*nbFFm,l+3*nbFFp+nbdofm) += (cgm->operator()(p,q)*Gradsm[j+3*nbFFm][q])*(nu(p)*
                                                                wJ/2.*Valsp[l+3*nbFFp]);
                }
              }
            }
          }
          for(int j=0;j<nbFFp;j++)
          {
            for(int l=0;l<nbFFm;l++)
            {
              for(int p =0; p < 3; p++)
              {
                for(int q =0; q < 3; q++)
                {
                  stiff(j+3*nbFFp+nbdofm,l+3*nbFFm) -= (cgp->operator()(p,q)*Gradsp[j+3*nbFFp][q])*(nu(p)*wJ/2.
                                                                *Valsm[l+3*nbFFm]);
                }
              }
            }
            for(int l=0;l<nbFFp;l++)
            {
              for(int p =0; p < 3; p++)
              {
                for(int q =0; q < 3; q++)
                {
                  stiff(j+3*nbFFp+nbdofm,l+3*nbFFp+nbdofm) += (cgp->operator()(p,q)*Gradsp[j+3*nbFFp][q])*(nu(p)*wJ/2.
                                                                   *Valsp[l+3*nbFFp]);
                }
              }
            }
          }
        }
      }
    }
  }
  //stiff.print("dginter");
}
  
  
  
    
void ThermoMechanicsMassG3D::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const
{
    mass3D::get(ele,npts,GP,m);
    Msg::Error("We should fill the 4th degree of freedom for the mass matrix");
}


void ThermoMechanicsDG3DForceBulk::get(MElement *ele,int npts,IntPt *GP,fullVector<double> &vFor) const
{

  // xyz Part
  dG3DForceBulk::get(ele,npts,GP,vFor);

  int nbdof = this->space1.getNumKeys(ele);
  int nbFF = ele->getNumShapeFunctions();

//  double jac[3][3];
  SVector3 BTepsbar;
  SVector3 Bepsbar;

  // get gauss' point data
  _ipf->getIPv(ele,vipv,vipvprev);
  bool hessianflag = false;
  bool thirdflag = false;
  if (_mlaw->isHighOrder()) {
    hessianflag = true;
    if (_fullDg) thirdflag = true;
  };
  for (int i = 0; i < npts; i++)
  {
    // Weight of Gauss' point i
    double weight = GP[i].weight;
    double &detJ = vipv[i]->getJacobianDeterminant(&space1,ele,GP[i],hessianflag,thirdflag);

    
    // CHANGE FROMM HERE
    
    const STensor3 *PK1 = &(vipv[i]->getConstRefToFirstPiolaKirchhoffStress());
    double ratio = detJ*weight;
    double eqRatio = _ThermoMechanicsEqRatio;                    //by Wu Ling

    const SVector3 &tflux = ((static_cast <const ThermoMechanicsDG3DIPVariableBase * > (vipv[i]))->
                                         getConstRefTothermalFlux()); 
    std::vector<TensorialTraits<double>::GradType> &Grads = vipv[i]->gradf(&space1,ele,GP[i],hessianflag,thirdflag);

    const int threeTimesNbFF = 3*nbFF;
    for(int j=0;j<nbFF; j++)
    {
      BTepsbar(0) = Grads[j+threeTimesNbFF][0];
      BTepsbar(1) = Grads[j+threeTimesNbFF][1];
      BTepsbar(2) = Grads[j+threeTimesNbFF][2];
      for(int m=0; m<3; m++)
      {
	 vFor(j+3*nbFF) += eqRatio*ratio*(BTepsbar(m)*tflux(m));
      }
    }
  }
  // vFor.print("interF");
}

void ThermoMechanicsDG3DStiffnessBulk::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &mStiff) const
{
  dG3DStiffnessBulk::get(ele,npts,GP,mStiff);
  if (sym)
  {
    bool hessianflag = false;
    bool thirdflag = false;
    if (_mlaw->isHighOrder()) {
      hessianflag = true;
      if (_fullDg) thirdflag = true;
    };
    // Initialization of some data
    int nbdof = BilinearTerm<double,double>::space1.getNumKeys(ele);
    int nbFF = ele->getNumShapeFunctions();

    // get ipvariables
    _ipf->getIPv(ele,vipv);
 
    SVector3 Bepsbar(3);
    SVector3 BTepsbar(3);
    SVector3 newB,newBT;

    for (int i = 0; i < npts; i++)
    {

      double weight = GP[i].weight;

      double &detJ = vipv[i]->getJacobianDeterminant(&space1,ele,GP[i],hessianflag,thirdflag);
      // x, y, z
      const STensor3 &dthermalFluxdgradT = ((static_cast <const ThermoMechanicsDG3DIPVariableBase * > (vipv[i]))->getConstRefTodthermalFluxdgradT()); 
      const STensor3 &dPdT = ((static_cast <const ThermoMechanicsDG3DIPVariableBase * > (vipv[i]))->getConstRefTodPdT());

      double eqRatio = _ThermoMechanicsEqRatio;               //by Wu Ling

      std::vector<TensorialTraits<double>::GradType> &Grads = vipv[i]->gradf(&space1,ele,GP[i],hessianflag,thirdflag);
      std::vector<TensorialTraits<double>::ValType> &Vals = vipv[i]->f(&space1,ele,GP[i],hessianflag,thirdflag);

      const int threeTimesNbFF = 3*nbFF;
      for(int j=0;j<nbFF; j++)
      {
        newBT(0) = Grads[j][0];
        newBT(1) = Grads[j][1];
        newBT(2) = Grads[j][2];
        BTepsbar(0) = Grads[j+threeTimesNbFF][0];
        BTepsbar(1) = Grads[j+threeTimesNbFF][1];
	 BTepsbar(2) = Grads[j+threeTimesNbFF][2];
        for(int k=0;k<nbFF;k++){
          double ratio = detJ*weight;
	      // x y z
          newB(0) = Grads[k][0];
          newB(1) = Grads[k][1];
          newB(2) = Grads[k][2];
          Bepsbar(0) = Grads[k+threeTimesNbFF][0];
          Bepsbar(1) = Grads[k+threeTimesNbFF][1];
	  Bepsbar(2) = Grads[k+threeTimesNbFF][2];

          // pure thermal
          for(int m=0; m<3; m++)
          {
             for(int n=0; n<3; n++)
            {
	      mStiff(j+threeTimesNbFF,k+threeTimesNbFF)+=eqRatio*ratio*(BTepsbar(m)*dthermalFluxdgradT.operator()(m,n)*Bepsbar(n));
            }
          }
          // cross terms to check
          for(int kk=0;kk<3;kk++)
          {
            for(int m=0; m<3; m++)
            {
	      //should be this one ?? mStiff(j+kk*nbFF,k+threeTimesNbFF) += ratio*(BTepsbar(m)*dPdT->operator()(kk,m)*Vals[k+threeTimesNbFF]);
              mStiff(j+kk*nbFF,k+threeTimesNbFF) += ratio*(newBT(m)*dPdT.operator()(kk,m)*Vals[k+threeTimesNbFF]);
            }
          }
        }
      }
    }
   //mStiff.print("bulk");
  }
  else
    printf("not implemented\n");
}


void ThermoMechanicsDG3DForceInter::get(MElement *ele, int npts, IntPt *GP, fullVector<double> &m)const
{
  dG3DForceInter::get(ele, npts, GP,m);
  const fullVector<double> &datafield = *_data;
  MInterfaceElement *ie = dynamic_cast<MInterfaceElement*>(ele);
  const int nbFFm = ie->getElem(0)->getNumVertices();
  const int nbFFp = ie->getElem(1)->getNumVertices();
  const int nbdofm = _minusSpace->getNumKeys(ie->getElem(0));
  const int nbdofp = _plusSpace->getNumKeys(ie->getElem(1));
  double jac[3][3];
  if(_fullDg)
  {
    bool hessianflagMinus = false;
    bool thirdflagMinus = false;
    if (_mlawMinus->isHighOrder()) {
      hessianflagMinus = true;
      if (_fullDg) thirdflagMinus = true;
    };

    bool hessianflagPlus = false;
    bool thirdflagPlus = false;
    if (_mlawPlus->isHighOrder()) {
      hessianflagPlus = true;
      if (_fullDg) thirdflagPlus = true;
    };
    // characteristic size
    const double hs = ie->getCharacteristicSize();
    const double nbetahs = _ThermoMechanicsBeta/hs;
    if( _continuity)
    {
      // get value at gauss's point
      AllIPState::ipstateElementContainer *vips = _ipf->getAips()->getIPstate(ie->getNum());
      // value on minus and plus elements
      IntPt *GPm; IntPt *GPp;
      _interQuad->getIntPoints(ie,GP,&GPm,&GPp);
      // Val, grad and detJ at all Gauss points
      std::vector<std::vector<TensorialTraits<double>::GradType> >vgradsuvw;

      _minusSpace->getgradfuvw(ele,npts,GP,vgradsuvw); // create a special function space ??
      // loop on Gauss Point
      for(int i=0;i<npts; i++)
      {

        const double weight = GP[i].weight;

        double detJ = ele->getJacobian(vgradsuvw[i], jac);
        // IP
        const IPStateBase *ipsm        = (*vips)[i];
        const IPStateBase *ipsp        = (*vips)[i+npts];
        const ThermoMechanicsDG3DIPVariableBase *ipvm;
        const ThermoMechanicsDG3DIPVariableBase *ipvp;
        if(_mlawMinus->getType() == materialLaw::fracture )
        {
          FractureCohesive3DIPVariable* ipvtmp = static_cast<FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::current));
          ipvm = static_cast<ThermoMechanicsDG3DIPVariableBase*>(ipvtmp->getIPvBulk());
          ipvtmp = static_cast<FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::current));
          ipvp = static_cast<ThermoMechanicsDG3DIPVariableBase*>(ipvtmp->getIPvBulk());
        }
        else
        {
          ipvm = static_cast<const ThermoMechanicsDG3DIPVariableBase*>(ipsm->getState(IPStateBase::current));
          ipvp = static_cast<const ThermoMechanicsDG3DIPVariableBase*>(ipsp->getState(IPStateBase::current));
        }

        const SVector3 &referenceOutwardNormalm = ipvm->getConstRefToReferenceOutwardNormal();


        const SVector3 &referenceOutwardNormalp = ipvp->getConstRefToReferenceOutwardNormal();
        const double &tempjump = ipvp->getConstRefToTemperatureJump();
	

        const STensor3  *k0m = &(ipvm->getConstRefTolinearK());           // by Wu Ling
        const STensor3  *k0p = &(ipvp->getConstRefTolinearK());           // by Wu Ling

	const SVector3  &tfluxm = (ipvm->getConstRefTothermalFlux());
        const SVector3  &tfluxp = (ipvp->getConstRefTothermalFlux());

        //double length = _mlaw->getLength();
        double eqRatio = _ThermoMechanicsEqRatio;               //by Wu Ling

        std::vector<TensorialTraits<double>::GradType> &Gradsm = ipvm->gradf(_minusSpace,ie->getElem(0),GPm[i],hessianflagMinus,thirdflagMinus);
        std::vector<TensorialTraits<double>::GradType> &Gradsp = ipvp->gradf(_plusSpace,ie->getElem(1),GPp[i],hessianflagPlus,thirdflagPlus);

        // possibility of fracture
        bool broken = false;
        if(_mlawMinus->getType() == materialLaw::fracture )
        {
          const FractureCohesive3DIPVariable *ipvwf = static_cast<const FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::current)); // broken via minus (OK broken on both sides)
          const FractureCohesive3DIPVariable *ipvwfprev = static_cast<const FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::previous));
          broken = ipvwfprev->isbroken(); // use continuity principle at fracture initialization to avoid instability
        }
        double wJ = detJ* weight;
        wJ*=eqRatio;
        if(!broken)
        {
          SVector3 nu = referenceOutwardNormalm*(1./referenceOutwardNormalm.norm());

          std::vector<TensorialTraits<double>::ValType> &Valsm = ipvm->f(_minusSpace,ie->getElem(0),GPm[i],hessianflagMinus,thirdflagMinus);
          std::vector<TensorialTraits<double>::ValType> &Valsp = ipvp->f(_plusSpace,ie->getElem(1),GPp[i],hessianflagPlus,thirdflagPlus);


          // consistency \mean{cg*grad eps}
          double meantflux =0.;
          for(int k =0; k<3; k++)
          {
             meantflux += (tfluxm(k)+tfluxp(k))*(nu(k)/2.);
          }
          
    
          double NMeank0JumpNBetasc = 0.;
          STensor3 meank = ((*k0m)+(*k0p))*(1./2.);
          for(int l = 0; l <3; l++)
          {
            for(int n = 0; n <3; n++)
            {
              NMeank0JumpNBetasc += meank(l,n)*tempjump*nu(n)*nbetahs*nu(l);
            }
          }

          for(int j=0;j<nbFFm;j++)
	
          {
            m(j+3*nbFFm) -= ( meantflux+NMeank0JumpNBetasc)*(Valsm[j+3*nbFFm]*wJ);
          }
          for(int j=0;j<nbFFp;j++)
          {
            m(j+3*nbFFp+nbdofm) += ( meantflux+NMeank0JumpNBetasc)*(Valsp[j+3*nbFFp]*wJ);
          }
                
          SVector3 k0pJumpN(0.);
          SVector3 k0mJumpN(0.);
          for(int k = 0; k <3; k++)
          {
            for(int m = 0; m <3; m++)
            {
              k0mJumpN(k) += k0m->operator()(k,m)*tempjump*nu(m);
              k0pJumpN(k) += k0p->operator()(k,m)*tempjump*nu(m);
            }
          }
          // Assembly (loop on shape function)
          
    
          
          for(int j=0;j<nbFFm;j++)
          {
            for(int l=0;l<3;l++)
            {
              m(j+3*nbFFm) += ( k0mJumpN(l)*Gradsm[j+3*nbFFm][l])*(wJ/2.);
            }
          }
          for(int j=0;j<nbFFm;j++) // shouldn't it be nbFFp
          {
            for(int l=0;l<3;l++)
            {
              m(j+3*nbFFp+nbdofm) += ( k0pJumpN(l)*Gradsp[j+3*nbFFp][l])*(wJ/2.);
            }
          }
	}
        
        else //broken case
        {

        }
      }
    }
  }
}


void ThermoMechanicsDG3DStiffnessInter::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &stiff) const
{
  dG3DStiffnessInter::get(ele,npts,GP,stiff);

  MInterfaceElement *ie = dynamic_cast<MInterfaceElement*>(ele);
  const int nbFFm = ie->getElem(0)->getNumVertices();
  const int nbFFp = ie->getElem(1)->getNumVertices();
  const int nbdofm = _minusSpace->getNumKeys(ie->getElem(0));
  const int nbdofp = _plusSpace->getNumKeys(ie->getElem(1));
  if(_fullDg)
  {
    bool hessianflagMinus = false;
    bool thirdflagMinus = false;
    if (_mlawMinus->isHighOrder()) {
      hessianflagMinus = true;
      if (_fullDg) thirdflagMinus = true;
    };

    bool hessianflagPlus = false;
    bool thirdflagPlus = false;
    if (_mlawPlus->isHighOrder()) {
      hessianflagPlus = true;
      if (_fullDg) thirdflagPlus = true;
    };
    // characteristic size
    const double hs = ie->getCharacteristicSize();
    const double nbetahs = _ThermoMechanicsBeta/hs;
    if( _continuity)
    {
      // get value at gauss's point
      AllIPState::ipstateElementContainer *vips = _ipf->getAips()->getIPstate(ie->getNum());
      // values of Gauss points on minus and plus elements
      IntPt *GPm; IntPt *GPp;
      _interQuad->getIntPoints(ie,GP,&GPm,&GPp);
      // val, grad and detJ at all Gauss points
      std::vector<std::vector<TensorialTraits<double>::GradType> >vgradsuvw;
      //std::vector<GaussPointSpaceValues<double>*> vallm;
      //std::vector<GaussPointSpaceValues<double>*> vallp;
      _minusSpace->getgradfuvw(ele,npts,GP,vgradsuvw); // do a special function space ??
      //_minusSpace->get(ie->getElem(0),npts,GPm,vallm);
      //_plusSpace->get(ie->getElem(1),npts,GPp,vallp);

      double jac[3][3];

      // loop on Gauss Point
      for(int i=0;i<npts; i++)
      {
        // Coordonate of Gauss' point i
//        const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
        // Weight of Gauss' point i and Jacobian's value at this point
        const double weight = GP[i].weight;
//        double detJ = ele->getJacobian(u, v, w, jac);
        double detJ = ele->getJacobian(vgradsuvw[i], jac);

        // IP
        const IPStateBase *ipsm        = (*vips)[i];
        const IPStateBase *ipsp        = (*vips)[i+npts];
        const ThermoMechanicsDG3DIPVariableBase *ipvm;     //= static_cast<const nonLocalDamageDG3DIPVariable*>(ipsm->getState(IPStateBase::current));
        const ThermoMechanicsDG3DIPVariableBase *ipvp;     // = static_cast<const nonLocalDamageDG3DIPVariable*>(ipsp->getState(IPStateBase::current));
        if(_mlawMinus->getType() == materialLaw::fracture )
        {
          FractureCohesive3DIPVariable* ipvtmp = static_cast<FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::current));
          ipvm = static_cast<ThermoMechanicsDG3DIPVariableBase*>(ipvtmp->getIPvBulk());
          ipvtmp = static_cast<FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::current));
          ipvp = static_cast<ThermoMechanicsDG3DIPVariableBase*>(ipvtmp->getIPvBulk());
        }
        else
        {
          ipvm = static_cast<const ThermoMechanicsDG3DIPVariableBase*>(ipsm->getState(IPStateBase::current));
          ipvp = static_cast<const ThermoMechanicsDG3DIPVariableBase*>(ipsp->getState(IPStateBase::current));
        }

//        const nonLocalDamageDG3DIPVariable *ipvmprev = static_cast<const nonLocalDamageDG3DIPVariable*>(ipsm->getState(IPStateBase::previous));
//        const nonLocalDamageDG3DIPVariable *ipvm0    = static_cast<const nonLocalDamageDG3DIPVariable*>(ipsm->getState(IPStateBase::initial));

        const SVector3 &referenceOutwardNormalm = ipvm->getConstRefToReferenceOutwardNormal();



//        const nonLocalDamageDG3DIPVariable *ipvp0    = static_cast<const nonLocalDamageDG3DIPVariable*>(ipsp->getState(IPStateBase::initial));

        const SVector3 &referenceOutwardNormalp = ipvp->getConstRefToReferenceOutwardNormal();
	const double &tempjump = ipvp->getConstRefToTemperatureJump();

        //std::vector<TensorialTraits<double>::GradType> &Gradsm = vallm[i]->_vgrads;
        //std::vector<TensorialTraits<double>::GradType> &Gradsp = vallp[i]->_vgrads;

        std::vector<TensorialTraits<double>::GradType> &Gradsm = ipvm->gradf(_minusSpace,ie->getElem(0),GPm[i],hessianflagMinus,thirdflagMinus);
        std::vector<TensorialTraits<double>::GradType> &Gradsp = ipvp->gradf(_plusSpace,ie->getElem(1),GPp[i],hessianflagPlus,thirdflagPlus);

        // possibility of fracture
        bool broken = false;
        if(_mlawMinus->getType() == materialLaw::fracture )
        {
          const FractureCohesive3DIPVariable *ipvwf = static_cast<const FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::current)); // broken via minus (OK broken on both sides)
          const FractureCohesive3DIPVariable *ipvwfprev = static_cast<const FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::previous));
          broken = ipvwfprev->isbroken(); // use continuity principle at fracture initialization to avoid instability
        }
        double wJ = detJ* weight;
        SVector3 nu = referenceOutwardNormalm*(1./referenceOutwardNormalm.norm());

        //std::vector<TensorialTraits<double>::ValType> &Valsm = vallm[i]->_vvals;
        //std::vector<TensorialTraits<double>::ValType> &Valsp = vallp[i]->_vvals;

        std::vector<TensorialTraits<double>::ValType> &Valsm = ipvm->f(_minusSpace,ie->getElem(0),GPm[i],hessianflagMinus,thirdflagMinus);
        std::vector<TensorialTraits<double>::ValType> &Valsp = ipvp->f(_plusSpace,ie->getElem(1),GPp[i],hessianflagPlus,thirdflagPlus);


        if(broken)
        {

        }
        else
        {
          const STensor3  *k0m = &(ipvm->getConstRefTolinearK());       
          const STensor3  *k0p = &(ipvp->getConstRefTolinearK());       
          const STensor3  *dqdgradTm = &(ipvm->getConstRefTodthermalFluxdgradT()); 
          const STensor3  *dqdgradTp = &(ipvp->getConstRefTodthermalFluxdgradT()); 
          const STensor3  *dPdTm = &(ipvm->getConstRefTodPdT()); 
          const STensor3  *dPdTp = &(ipvp->getConstRefTodPdT()); 

          double eqRatio = _ThermoMechanicsEqRatio;
          wJ*=eqRatio;

          // Assembly consistency (from derivative of sigma with tilde{p}
	  for(int j=0;j<nbFFm;j++)
          {
            for(int k=0;k<3;k++)
            {
              for(int l=0;l<nbFFm;l++)
              {
                for(int q = 0; q< 3; q++)
                {
                  stiff(j+k*nbFFm,l+3*nbFFm) -= dPdTm->operator()(k,q)*(Valsm[l+3*nbFFm]*Valsm[j+0*nbFFm]*wJ/2.*nu(q)/eqRatio);
                }
              }
              for(int l=0;l<nbFFp;l++)
              {
                for(int q = 0; q< 3; q++)
                {
                  stiff(j+k*nbFFm,l+3*nbFFp+nbdofm) -= dPdTp->operator()(k,q)*(Valsp[l+3*nbFFp]*Valsm[j+0*nbFFm]*wJ/2.*nu(q)/eqRatio);
                }
              }
            }
          }
          for(int j=0;j<nbFFp;j++)
          {
            for(int k=0;k<3;k++)
            {
              for(int l=0;l<nbFFm;l++)
              {
                for(int q = 0; q< 3; q++)
                {
                  stiff(j+k*nbFFp+nbdofm,l+3*nbFFm) += dPdTm->operator()(k,q)*(Valsm[l+3*nbFFm]*Valsp[j+0*nbFFp]*wJ/2.*nu(q)/eqRatio);
                }
              }
              for(int l=0;l<nbFFp;l++)
              {
                for(int q = 0; q< 3; q++)
                {
                  stiff(j+k*nbFFp+nbdofm,l+3*nbFFp+nbdofm) += dPdTp->operator()(k,q)*(Valsp[l+3*nbFFp]*Valsp[j+0*nbFFp]*wJ/2.*nu(q)/eqRatio);
                }
              }
            }
          }

         
          // consistency D\mean{q} DT
          // Assembly consistency
          for(int j=0;j<nbFFm;j++)
          {
            for(int l=0;l<nbFFm;l++)
            {
              for(int p = 0; p< 3; p++)
              {
                for(int q = 0; q< 3; q++)
                {
                  stiff(j+3*nbFFm,l+3*nbFFm) -= dqdgradTm->operator()(q,p)*(Gradsm[l+3*nbFFm][p]*Valsm[j+3*nbFFm]*wJ/2.*nu(q));
                }
              }
            }
            for(int l=0;l<nbFFp;l++)
            {
              for(int p = 0; p< 3; p++)
              {
                for(int q = 0; q< 3; q++)
                {
                  stiff(j+3*nbFFm,l+3*nbFFp+nbdofm) -= dqdgradTp->operator()(q,p)*(Gradsp[l+3*nbFFp][p]*Valsm[j+3*nbFFm]*wJ/2.*nu(q));
                }
              }
            }
          }
          for(int j=0;j<nbFFp;j++)
          {
            for(int l=0;l<nbFFm;l++)
            {
              for(int p = 0; p< 3; p++)
              {
                for(int q = 0; q< 3; q++)
                {
                  stiff(j+3*nbFFp+nbdofm,l+3*nbFFm) += dqdgradTm->operator()(q,p)*(Gradsm[l+3*nbFFm][p]*Valsp[j+3*nbFFp]*wJ/2.*nu(q));
                }
              }
            }
            for(int l=0;l<nbFFp;l++)
            {
              for(int p = 0; p< 3; p++)
              {
                for(int q = 0; q< 3; q++)
                {
                  stiff(j+3*nbFFp+nbdofm,l+3*nbFFp+nbdofm) += dqdgradTp->operator()(q,p)*(Gradsp[l+3*nbFFp][p]*Valsp[j+3*nbFFp]*wJ/2.*nu(q));
                }
              }
            }
          }

          // Stability  N \mean{k0} (N)
          double NMeankNBetasc=0.;
          for(int l = 0; l <3; l++)
          {
            for(int n = 0; n <3; n++)
            {
              NMeankNBetasc += (k0p->operator()(l,n)+k0m->operator()(l,n))*nu(n)*nbetahs*nu(l)/2.;
            }
          }
          // Assembly stability (if not broken)
          for(int j=0;j<nbFFm;j++)
          {
            for(int l=0;l<nbFFm;l++)
            {
              stiff(j+3*nbFFm,l+3*nbFFm) += NMeankNBetasc*(Valsm[j+3*nbFFm]*Valsm[l+3*nbFFm]*wJ);
            }
            for(int l=0;l<nbFFp;l++)
            {
              stiff(j+3*nbFFm,l+3*nbFFp+nbdofm) -= NMeankNBetasc*(Valsm[j+3*nbFFm]*Valsp[l+3*nbFFp]*wJ);
            }
          }
          for(int j=0;j<nbFFp;j++)
          {
            for(int l=0;l<nbFFm;l++)
            {
              stiff(j+3*nbFFp+nbdofm,l+3*nbFFm) -= NMeankNBetasc*(Valsp[j+3*nbFFp]*Valsm[l+3*nbFFm]*wJ);
            }
            for(int l=0;l<nbFFp;l++)
            {
              stiff(j+3*nbFFp+nbdofm,l+3*nbFFp+nbdofm) += NMeankNBetasc*(Valsp[j+3*nbFFp]*Valsp[l+3*nbFFp]*wJ);
            }
          }
          // compatibility 
          // Assembly (loop on shape function)
          for(int j=0;j<nbFFm;j++)
          {
            for(int l=0;l<nbFFm;l++)
            {
              for(int p =0; p < 3; p++)
              {
                for(int q =0; q < 3; q++)
                {
                  stiff(j+3*nbFFm,l+3*nbFFm) -= (k0m->operator()(p,q)*Gradsm[j+3*nbFFm][q])*(nu(p)*wJ/2.*Valsm[l+3*nbFFm]);
                }
              }
            }
            for(int l=0;l<nbFFp;l++)
            {
              for(int p =0; p < 3; p++)
              {
                for(int q =0; q < 3; q++)
                {
                  stiff(j+3*nbFFm,l+3*nbFFp+nbdofm) += (k0m->operator()(p,q)*Gradsm[j+3*nbFFm][q])*(nu(p)*wJ/2.*Valsp[l+3*nbFFp]);
                }
              }
            }
          }
          for(int j=0;j<nbFFp;j++)
          {
            for(int l=0;l<nbFFm;l++)
            {
              for(int p =0; p < 3; p++)
              {
                for(int q =0; q < 3; q++)
                {
                  stiff(j+3*nbFFp+nbdofm,l+3*nbFFm) -= (k0p->operator()(p,q)*Gradsp[j+3*nbFFp][q])*(nu(p)*wJ/2.*Valsm[l+3*nbFFm]);
                }
              }
            }
            for(int l=0;l<nbFFp;l++)
            {
              for(int p =0; p < 3; p++)
              {
                for(int q =0; q < 3; q++)
                {
                  stiff(j+3*nbFFp+nbdofm,l+3*nbFFp+nbdofm) += (k0p->operator()(p,q)*Gradsp[j+3*nbFFp][q])*(nu(p)*wJ/2.*Valsp[l+3*nbFFp]);
                }
              }
            }
          }
        }
      }
    }
  }
  //stiff.print("dginter");
}

