//
// C++ Interface: partDomain
//
// Description: Interface class for dg 3D
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "nonLinearMechSolver.h"
#include "dG3DTerms.h"
#include "dG3DDomain.h"
#include "dG3DFilterDofComponent.h"
#include "IFace.h"
#include "GaussLobatto2DQuadratureRules.h"
#include "numericalMaterial.h"
#include "IEdge.h"
#include "MInterfaceLine.h"
static double determinantSTensor3(const STensor3 &a)
{
	  return (a(0,0) * (a(1,1) * a(2,2) - a(1,2) * a(2,1)) -
			            a(0,1) * (a(1,0) * a(2,2) - a(1,2) * a(2,0)) +
				              a(0,2) * (a(1,0) * a(2,1) - a(1,1) * a(2,0)));
}

dG3DDomain::dG3DDomain (const int tag,const int phys, const int ws, const int lnum,
                                          const int fdg, int dim) : dgPartDomain(tag,phys,ws,fdg), _lnum(lnum),
                                                            _beta1(10.),_sts(1.), _isHighOrder(false), _interQuad(NULL),
                                                            _space(NULL), _spaceMinus(NULL), _spacePlus(NULL), _dim(dim),
                                                            _gqt(Gauss), _gaussorderbulk(-1), _gaussorderbound(-1), _evalStiff(false)
{
  switch(_wsp)
  {
   case functionSpaceType::Lagrange:
    if(_fullDg)
      _space = new dG3DLagrangeFunctionSpace(_phys,3);
    else
      _space = new g3DLagrangeFunctionSpace(_phys,3);
    _spaceInterUVW = new IsoparametricLagrangeFunctionSpace(_phys); // 3 composantes
    break;
   case functionSpaceType::Inter :
    _spaceInterUVW = new IsoparametricLagrangeFunctionSpace(_phys); // 3 composantes
    break; // InterfaceDomain set function space later
   default:
    Msg::Error("Function space type is unknown on domain %d",_phys);
  }
  _spaceMinus = _space;
  _spacePlus = _space;

  #if defined(HAVE_MPI)
  _distributedOnRoot = true;
  #endif
}
dG3DDomain::dG3DDomain(const dG3DDomain &source) : dgPartDomain(source), _lnum(source._lnum),
                                                                               _mlaw(source._mlaw), _beta1(source._beta1),
                                                                               _sts(source._sts), _space(source._space),
                                                                               _spaceMinus(source._spaceMinus),
                                                                               _spacePlus(source._spacePlus),
                                                                               _spaceInterUVW(source._spaceInterUVW),
                                                                               _gqt(source._gqt),
                                                                               _gaussorderbulk(source._gaussorderbulk),
                                                                               _gaussorderbound(source._gaussorderbound),
                                                                               _interQuad(source._interQuad), _dim(source._dim),
									       _evalStiff(source._evalStiff),_isHighOrder(source._isHighOrder){
  #if defined(HAVE_MPI)
  _distributedOnRoot = source._distributedOnRoot;
  #endif
}

void dG3DDomain::distributeOnRootRank(const int flg){
  #if defined(HAVE_MPI)
  _distributedOnRoot = flg;
  #endif
};

void dG3DDomain::setMaterialLaw(const std::map<int,materialLaw*> maplaw)
{
  if(!setmaterial){
    for(std::map<int,materialLaw*>::const_iterator it=maplaw.begin(); it!=maplaw.end(); ++it){
      if(it->first == _lnum){
        _mlaw = it->second;
        _mlaw->initLaws(maplaw);
        _isHighOrder = _mlaw->isHighOrder();
      }
    }
    setmaterial = true;
  }
}

void dG3DDomain::stabilityParameters(const double b1)
{
  _beta1 = b1;
  _sts = 1./sqrt(_beta1);
}

const double dG3DDomain::stabilityParameter(const int i) const
{
  switch(i)
  {
   case 1:
    return _beta1;
   default:
    Msg::Error("This domain has only 3 stability parameters !");
    return 0.;
  }
}

void dG3DDomain::matrixByPerturbation(const int ibulk, const int iinter, const int ivirt, const double eps)
{
  this->setBulkMatrixByPerturbation(ibulk,eps);
  iinter == 0 ? _interByPert = false : _interByPert = true;
  ivirt == 0 ? _virtByPert = false : _virtByPert = true;
}

FunctionSpaceBase* dG3DDomain::getSpaceForBC(const nonLinearBoundaryCondition &boundary, const groupOfElements *groupBC,const int domcomp) const
{
  FunctionSpaceBase* spacebc;
  if(boundary.getType() == nonLinearBoundaryCondition::DIRICHLET)
  {
    const nonLinearDirichletBC* diri = static_cast<const nonLinearDirichletBC*>(&boundary);
    switch(_wsp){
     case functionSpaceType::Lagrange:
      if(_fullDg){
        if (getDim() == 3){
          if(boundary.onWhat !=nonLinearBoundaryCondition::ON_VOLUME)
            spacebc= new dG3DBoundaryConditionLagrangeFunctionSpace(_phys,domcomp,diri->_comp,groupBC,gi,giv,_groupGhostMPI);
          else
            spacebc = new dG3DLagrangeFunctionSpace(_phys,domcomp,diri->_comp);
        }
        else if (getDim() == 2){
          if(boundary.onWhat !=nonLinearBoundaryCondition::ON_FACE)
            spacebc= new dG3DBoundaryConditionLagrangeFunctionSpace(_phys,domcomp,diri->_comp,groupBC,gi,giv,_groupGhostMPI);
          else
            spacebc = new dG3DLagrangeFunctionSpace(_phys,domcomp,diri->_comp);

        }

      }
      else{
        // For MPI a check if the BC is applied on element or ghost element
        // the BC being applied on the element of the BC with maybe element->getPartition()==0 !!
       #if defined(HAVE_MPI)
        if(Msg::GetCommSize()>1)
        {
	      int found =0;
	      int limitFound = 3;
	      if(g->size()==0) // No element on this domain --> the BC is ghost !
          {
            spacebc = new g3DDirichletBoundaryConditionLagrangeFunctionSpaceGhost(_phys,3,diri->_comp);
            return spacebc;
          }
	      MElement *e = *(g->begin());
          if(boundary.onWhat == nonLinearBoundaryCondition::ON_VOLUME) Msg::Error("Cannot use volume in this function");
	      if(e->getTypeForMSH() == MSH_TET_10)
	      {
	        if(boundary.onWhat == nonLinearBoundaryCondition::ON_FACE) limitFound = 3;
            if(boundary.onWhat == nonLinearBoundaryCondition::ON_EDGE) limitFound = 2;
	        if(boundary.onWhat == nonLinearBoundaryCondition::ON_VERTEX) limitFound = 1;
	      }
	      else if(e->getTypeForMSH() == MSH_HEX_27)
	      {
            if(boundary.onWhat == nonLinearBoundaryCondition::ON_FACE) limitFound = 4;
	        if(boundary.onWhat == nonLinearBoundaryCondition::ON_EDGE) limitFound = 3;
	        if(boundary.onWhat == nonLinearBoundaryCondition::ON_VERTEX) limitFound = 1;
          }
	      else if(e->getTypeForMSH() == MSH_HEX_8)
	      {
            if(boundary.onWhat == nonLinearBoundaryCondition::ON_FACE) limitFound = 3;
	        if(boundary.onWhat == nonLinearBoundaryCondition::ON_EDGE) limitFound = 2;
	        if(boundary.onWhat == nonLinearBoundaryCondition::ON_VERTEX) limitFound = 1;
          }
          for(groupOfElements::vertexContainer::const_iterator itv=g->vbegin(); itv!=g->vend();++itv)
          {
            MVertex* ver = *itv;
            for(groupOfElements::vertexContainer::const_iterator it = groupBC->vbegin(); it != groupBC->vend(); it++)
	        {
              if(ver->getNum() == (*it)->getNum()) found ++;
            }
          }
	      if(found >= limitFound ) // BC is on element
          {
            spacebc = new g3DDirichletBoundaryConditionLagrangeFunctionSpace(_phys,domcomp,diri->_comp);
          }
          else // BC is on ghost element
          {
            spacebc = new g3DDirichletBoundaryConditionLagrangeFunctionSpaceGhost(_phys,domcomp,diri->_comp);
          }
        }
        else
        #endif //HAVE_MPI
        {
          spacebc = new g3DDirichletBoundaryConditionLagrangeFunctionSpace(_phys,domcomp,diri->_comp);
        }
      }
      break;
     default :
       Msg::Error("Unknown function space type for dirichlet BC %d on domain %d",boundary._tag,_phys);
    }
  }
  else if(boundary.getType() == nonLinearBoundaryCondition::NEUMANN)
  {
    const nonLinearNeumannBC *neu = static_cast<const nonLinearNeumannBC*>(&boundary);
    switch(_wsp){
     case functionSpaceType::Lagrange :
      if(_fullDg){
        if (getDim()==3){
          if((boundary.onWhat != nonLinearBoundaryCondition::ON_VOLUME) and (boundary.onWhat != nonLinearBoundaryCondition::PRESSURE))
            spacebc = new dG3DBoundaryConditionLagrangeFunctionSpace(_phys,domcomp,neu->_comp,groupBC,gi,giv,NULL); // no ghostMPI for Neumann BC
          else
            spacebc = new dG3DLagrangeFunctionSpace(_phys,domcomp);
        }
        else if (getDim()==2){
          if((boundary.onWhat != nonLinearBoundaryCondition::ON_FACE) and (boundary.onWhat != nonLinearBoundaryCondition::PRESSURE))
            spacebc = new dG3DBoundaryConditionLagrangeFunctionSpace(_phys,domcomp,neu->_comp,groupBC,gi,giv,NULL); // no ghostMPI for Neumann BC
          else
            spacebc = new dG3DLagrangeFunctionSpace(_phys,domcomp);
        }
      }
      else
        if (getDim()==3){
          if((neu->onWhat != nonLinearBoundaryCondition::ON_VOLUME) and (boundary.onWhat != nonLinearBoundaryCondition::PRESSURE))
            spacebc = new g3DNeumannBoundaryConditionLagrangeFunctionSpace(_phys,domcomp,neu->_comp);
          else
            spacebc = new g3DNeumannBoundaryConditionLagrangeFunctionSpace(_phys,domcomp);
        }
        else if (getDim()==2){
          if((neu->onWhat != nonLinearBoundaryCondition::ON_FACE) and (boundary.onWhat != nonLinearBoundaryCondition::PRESSURE))
            spacebc = new g3DNeumannBoundaryConditionLagrangeFunctionSpace(_phys,domcomp,neu->_comp);
          else
            spacebc = new g3DNeumannBoundaryConditionLagrangeFunctionSpace(_phys,domcomp);
        }

     break;
      default :
      Msg::Error("Unknown function space type on neumman BC %d on domain %d",boundary._tag,_phys);
    }
  }
  else if(boundary.getType() == nonLinearBoundaryCondition::INITIAL)
  {
    switch(_wsp){
     case functionSpaceType::Lagrange:
      if(_fullDg){
        if (getDim()==3){
          if(boundary.onWhat !=nonLinearBoundaryCondition::ON_VOLUME)
            spacebc = new dG3DBoundaryConditionLagrangeFunctionSpace(_phys,domcomp,groupBC,gi,giv,_groupGhostMPI);
          else
            spacebc = new dG3DLagrangeFunctionSpace(_phys,domcomp);
        }
        else if (getDim()==2){
          if(boundary.onWhat !=nonLinearBoundaryCondition::ON_FACE)
            spacebc = new dG3DBoundaryConditionLagrangeFunctionSpace(_phys,domcomp,groupBC,gi,giv,_groupGhostMPI);
          else
            spacebc = new dG3DLagrangeFunctionSpace(_phys,domcomp);
        }
      }
      else{
        spacebc = new g3DDirichletBoundaryConditionLagrangeFunctionSpace(_phys,domcomp);
      }
      break;
     default :
       Msg::Error("Unknown function space type for dirichlet BC %d on domain %d",boundary._tag,_phys);
    }
  }
  return spacebc;
}

QuadratureBase* dG3DDomain::getQuadratureRulesForNeumannBC(nonLinearNeumannBC &neu) const
{
  QuadratureBase *integ;
  if(neu.onWhat == nonLinearBoundaryCondition::ON_VOLUME)
  {
    GaussQuadrature *integgauss = static_cast<GaussQuadrature*>(integBulk);
    integ = new GaussQuadrature(*integgauss);
  }
  else{
    if(_gqt == Gauss){
      GaussQuadrature *intgauss = static_cast<GaussQuadrature*>(integBound);
      integ = new GaussQuadrature(*intgauss);
    }
    else{
      GaussLobatto2DQuadrature *intgauss = static_cast<GaussLobatto2DQuadrature*>(integBound);
      integ = new GaussLobatto2DQuadrature(*intgauss);
    }
  }
  return integ;
}

void dG3DDomain::createTerms(unknownField *uf,IPField*ip, std::vector<nonLinearNeumannBC> &vNeumann)
{
  FunctionSpace<double>* dgspace = static_cast<FunctionSpace<double>*>(_space);
  ltermBulk = new dG3DForceBulk(*dgspace,_mlaw,_fullDg,ip);
  if(_fullDg)
    ltermBound = new dG3DForceInter(*dgspace,dgspace,_mlaw,_mlaw,_interQuad,_beta1,_fullDg,ip);
  else
     ltermBound = new nonLinearTermVoid();
  if(_bmbp)
    btermBulk = new BilinearTermPerturbation<double>(ltermBulk,*dgspace,*dgspace,uf,ip,this,_eps);
  else
   btermBulk = new dG3DStiffnessBulk(*dgspace,_mlaw,_fullDg,ip);
  if(_fullDg)
  {
    if(_interByPert)
      btermBound = new BilinearTermPerturbation<double>(ltermBound,*dgspace,*dgspace,uf,ip,this,_eps);
    else
      btermBound = new dG3DStiffnessInter(*dgspace,*dgspace,_mlaw,_mlaw,_interQuad,_beta1,ip,uf,_fullDg);
  }
  else
    btermBound = new BiNonLinearTermVoid();

  this->btermVirtBound = new BiNonLinearTermVoid();
  this->ltermVirtBound = new nonLinearTermVoid();


  massterm = new mass3D(*dgspace,_mlaw);

   // term of neumannBC
   for(int i=0;i<vNeumann.size();i++){
    nonLinearNeumannBC &neu = vNeumann[i];
    for(int j=0;j<neu._vspace.size();j++){
      if(neu._vdom[j] == this)
      {
//      if(neu._vterm[j] != NULL) delete neu._vterm[j];
        FunctionSpace<double> *spneu = static_cast<FunctionSpace<double>*>(neu._vspace[j]);
        neu._vterm[j] = new g3DLoadTerm<double>(*spneu,neu._f);
      }
    }
   }

}

void dG3DDomain::initializeTerms(unknownField *uf,IPField*ip, std::vector<nonLinearNeumannBC> &vNeumann)
{
  createTerms(uf,ip, vNeumann);

   /* same value for fracture strength on both side */
   // not very elegant put very efficient to put here
   if(_mlaw->getType() == materialLaw::fracture)
   {
     // loop on ipvariable
     IntPt *GP;
     for(groupOfElements::elementContainer::iterator it=gi->begin(); it!=gi->end(); ++it)
     {
       MElement *ele = *it;
       int npts_inter=integBound->getIntPoints(ele,&GP);
       AllIPState::ipstateElementContainer *vips = ip->getAips()->getIPstate(ele->getNum());
       for(int j=0;j<npts_inter;j++){ // npts_inter points on both sides
         IPStateBase* ipsm = (*vips)[j];
         IPStateBase* ipsp = (*vips)[j+npts_inter];
         FractureCohesive3DIPVariable *ipvm = static_cast<FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::initial));
         FractureCohesive3DIPVariable *ipvp = static_cast<FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::initial));
         ipvp->setFractureStrengthFactor(ipvm->fractureStrengthFactor());
         ipvm = static_cast<FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::previous));
         ipvp = static_cast<FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::previous));
         ipvp->setFractureStrengthFactor(ipvm->fractureStrengthFactor());
         ipvm = static_cast<FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::current));
         ipvp = static_cast<FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::current));
         ipvp->setFractureStrengthFactor(ipvm->fractureStrengthFactor());
       }
     }
   }
}

void dG3DDomain::computeAllIPStrain(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, const bool virt){
  IntPt *GP;
  fullVector<double> dispm;
  fullVector<double> dispp;
  std::vector<Dof> R;
  // interface elements
  partDomain *dom = static_cast<partDomain*>(this);
  for(groupOfElements::elementContainer::const_iterator it=gi->begin(); it!=gi->end();++it){
    MInterfaceElement *ie = dynamic_cast<MInterfaceElement*>(*it);
    R.clear();
    _spaceMinus->getKeys(ie->getElem(0),R);
    dispm.resize(R.size());
    ufield->get(R,dispm);
    R.clear();
    _spacePlus->getKeys(ie->getElem(1),R);
    dispp.resize(R.size());
    ufield->get(R,dispp);
    int npts = integBound->getIntPoints(*it,&GP);
    this->computeStrain(aips,ie, GP,ws,dom,dom,dispm,dispp,virt);
  }
  // bulk
  for (groupOfElements::elementContainer::const_iterator it = g->begin(); it != g->end(); ++it){
    MElement *e = *it;
    R.clear();
    _space->getKeys(e,R);
    dispm.resize(R.size());
    ufield->get(R,dispm);
    this->computeStrain(aips,e,ws,dispm);
  }
};


void dG3DDomain::computeIPVariable(AllIPState *aips,const unknownField *ufield, const IPStateBase::whichState ws, bool stiff)
{
  _evalStiff = false; // directly access to computeIPv is for matrix by perturbation otherwise compute ipvarible thanks to this function
  #if defined(HAVE_MPI)
  if (_mlaw->getType()==materialLaw::numeric and _otherRanks.size()>0){
    this->computeIPVariableMPI(aips,ufield,ws,stiff);
  }
  else
  #endif //HAVE_MPI
  {
    if(ws == IPStateBase::initial){
      this->initialIPVariable(aips,ufield,ws);
    }
    else{

      this->computeAllIPStrain(aips,ufield,ws,false);
      IntPt *GP;
      double uem,vem,uep,vep;
      fullVector<double> dispm;
      fullVector<double> dispp;
      std::vector<Dof> R;
      // interface elements
      partDomain *dom = static_cast<partDomain*>(this);
      for(groupOfElements::elementContainer::const_iterator it=gi->begin(); it!=gi->end();++it){
        MInterfaceElement *ie = dynamic_cast<MInterfaceElement*>(*it);
        R.clear();
        _spaceMinus->getKeys(ie->getElem(0),R);
        dispm.resize(R.size());
        ufield->get(R,dispm);
        R.clear();
        _spacePlus->getKeys(ie->getElem(1),R);
        dispp.resize(R.size());
        ufield->get(R,dispp);
        int npts = integBound->getIntPoints(*it,&GP);
        this->computeIpv(aips,ie, GP,ws,dom,dom,_mlaw,_mlaw,dispm,dispp,false,stiff);
      }
      // bulk
      for (groupOfElements::elementContainer::const_iterator it = g->begin(); it != g->end(); ++it){
        MElement *e = *it;
        R.clear();
        _space->getKeys(e,R);
        dispm.resize(R.size());
        ufield->get(R,dispm);
        this->computeIpv(aips,e,ws,_mlaw,dispm,stiff);
      }
    }
  }
  _evalStiff = true;
}

void dG3DDomain::computeStrain(AllIPState *aips,MElement *e, IPStateBase::whichState ws,
                                        fullVector<double> &disp)
{
  //nlsFunctionSpaceXYZ<double> *sp = static_cast<nlsFunctionSpaceXYZ<double>*>(_space);
  IntPt *GP;
  int npts_bulk=integBulk->getIntPoints(e,&GP);
  AllIPState::ipstateElementContainer *vips = aips->getIPstate(e->getNum());
  // Gert values of shape function (and gradient)
  //std::vector<std::vector<TensorialTraits<double>::GradType> > vgrads;
  //sp->getgradf(e,npts_bulk,GP,vgrads);

  bool hessianFlag = false;
  bool thirdFlag = false;
  dG3DMaterialLaw* law = dynamic_cast<dG3DMaterialLaw*>(_mlaw);
  if (law->isHighOrder()){
    hessianFlag = true;
    if (_fullDg)
      thirdFlag = true;
  }

  int nbFF = e->getNumShapeFunctions();
  for(int j=0;j<npts_bulk;j++){
    //grad value at gauss point
    //sp->gradf(e,GP[j].pt[0],GP[j].pt[1],GP[j].pt[2],Grads);
    //std::vector<TensorialTraits<double>::GradType> &Grads = vgrads[j];  // CHECK HERE WHAT WE NEED
    IPStateBase* ips = (*vips)[j];

    dG3DIPVariableBase *ipv = static_cast<dG3DIPVariableBase*>(ips->getState(ws));
    dG3DIPVariableBase *ipvprev = static_cast<dG3DIPVariableBase*>(ips->getState(IPStateBase::previous));
    // get grad of shape function from Gauss point
    std::vector<TensorialTraits<double>::GradType> &Grads = ipv->gradf(_space,e,GP[j],hessianFlag,thirdFlag);
    //x y z
    STensor3 *deformationGradient = &(ipv->getRefToDeformationGradient());
    deformationGradient->operator=(0.);
    deformationGradient->operator()(0,0) = 1.;
    deformationGradient->operator()(1,1) = 1.;
    deformationGradient->operator()(2,2) = 1.;
    for (int i = 0; i < nbFF; i++)
    {
        //x y z: take compoenent x arbitrarily
        // xx yy zz xy xz yz
        deformationGradient->operator()(0,0) += Grads[i+0*nbFF][0]*disp(i);
        deformationGradient->operator()(0,1) += Grads[i+0*nbFF][1]*disp(i);
        deformationGradient->operator()(0,2) += Grads[i+0*nbFF][2]*disp(i);
        deformationGradient->operator()(1,0) += Grads[i+0*nbFF][0]*disp(i+nbFF);
        deformationGradient->operator()(1,1) += Grads[i+0*nbFF][1]*disp(i+nbFF);
        deformationGradient->operator()(1,2) += Grads[i+0*nbFF][2]*disp(i+nbFF);
        deformationGradient->operator()(2,0) += Grads[i+0*nbFF][0]*disp(i+2*nbFF);
        deformationGradient->operator()(2,1) += Grads[i+0*nbFF][1]*disp(i+2*nbFF);
        deformationGradient->operator()(2,2) += Grads[i+0*nbFF][2]*disp(i+2*nbFF);
    }
    if(determinantSTensor3(*deformationGradient) < 1.e-15) Msg::Error("Negative Jacobian");
  }
}
void dG3DDomain::computeIpv(AllIPState *aips,MElement *e, IPStateBase::whichState ws,
                                        materialLaw *mlaw__,fullVector<double> &disp, bool stiff)
{
  dG3DMaterialLaw *mlaw;
  if(mlaw__->getType() == materialLaw::fracture)
    mlaw = static_cast< dG3DMaterialLaw* >((static_cast< FractureByCohesive3DLaw* > (mlaw__) )->getBulkLaw());
  else{
    mlaw= static_cast<dG3DMaterialLaw*>(mlaw__);
  }
  if(_evalStiff) { this->computeStrain(aips,e,ws,disp);}

  //nlsFunctionSpaceXYZ<double> *sp = static_cast<nlsFunctionSpaceXYZ<double>*>(_space);
  IntPt *GP;
  int npts_bulk=integBulk->getIntPoints(e,&GP);
  AllIPState::ipstateElementContainer *vips = aips->getIPstate(e->getNum());
  // Get grad value of shape functions
  //std::vector<std::vector<TensorialTraits<double>::GradType> >vgrads;
  //sp->getgradf(e,npts_bulk,GP,vgrads);
  //int nbFF = e->getNumShapeFunctions();
  for(int j=0;j<npts_bulk;j++){
    //std::vector<TensorialTraits<double>::GradType> &Grads = vgrads[j];
    IPStateBase* ips = (*vips)[j];
    dG3DIPVariableBase *ipv = static_cast<dG3DIPVariableBase*>(ips->getState(ws));
    dG3DIPVariableBase *ipvprev = static_cast<dG3DIPVariableBase*>(ips->getState(IPStateBase::previous));
    mlaw->stress(ipv,ipvprev, stiff);
  }
}

void dG3DDomain::computeStrain(AllIPState *aips,MInterfaceElement *ie, IntPt *GP,const IPStateBase::whichState ws,
                                        partDomain* efMinus, partDomain *efPlus, fullVector<double> &dispm,
                                        fullVector<double> &dispp,const bool virt)
{
  if(!virt){
    //nlsFunctionSpaceXYZ<double> *_spaceminus = static_cast<nlsFunctionSpaceXYZ<double>*>(efMinus->getFunctionSpace());
    FunctionSpaceBase* _spaceminus = efMinus->getFunctionSpace();
    MElement *ele = dynamic_cast<MElement*>(ie);

    int npts=integBound->getIntPoints(ele,&GP);
    // vector with nodal displacement of interface
    // take from minus displacement vector
    //nlsFunctionSpaceXYZ<double> *_spaceplus = static_cast<nlsFunctionSpaceXYZ<double>*>(efPlus->getFunctionSpace());
    FunctionSpaceBase* _spaceplus = efPlus->getFunctionSpace();
    MElement *em = ie->getElem(0);
    MElement *ep = ie->getElem(1);

     // gauss point
    int nbdofm = _spaceminus->getNumKeys(em);
    int nbdofp = _spaceplus->getNumKeys(ep);
    int nbFFm = em->getNumVertices();
    int nbFFp = ep->getNumVertices();
    // Get Gauss points values on minus and plus elements
    IntPt *GPm; IntPt *GPp;
    _interQuad->getIntPoints(ie,GP,&GPm,&GPp);
    // Get the grad value at all Gauss points
    //std::vector<std::vector<TensorialTraits<double>::GradType> > vgradm;
    //std::vector<std::vector<TensorialTraits<double>::GradType> > vgradp;
    //_spaceminus->getgradf(ie->getElem(0),npts,GPm,vgradm);
    //_spaceplus->getgradf(ie->getElem(1),npts,GPp,vgradp);

     fullVector<double> dispinter;
    int nbdofinter = _space->getNumKeys(ele);
    int nbvertexInter = ie->getNumVertices();
    std::vector<int> vn(nbvertexInter);
    ie->getLocalVertexNum(0,vn);
    dispinter.resize(nbdofinter);
    for(int i=0;i<nbvertexInter;i++){
      dispinter(i) = dispm(vn[i]);
      dispinter(i+nbvertexInter) = dispm(vn[i]+nbFFm);
      dispinter(i+2*nbvertexInter) = dispm(vn[i]+2*nbFFm);
    }


    nlsFunctionSpaceUVW<double> *spinter = static_cast<nlsFunctionSpaceUVW<double>*>(_spaceInterUVW);
    // Get grad shape functions values at all Gauss points
    std::vector<std::vector<TensorialTraits<double>::GradType> > vgrads;
    spinter->getgradf(ele,npts,GP,vgrads);


    bool hessianFlagMinus = false;
    bool thirdFlagMinus = false;
    dG3DMaterialLaw* lawminus = dynamic_cast<dG3DMaterialLaw*>(efMinus->getMaterialLaw());
    if (lawminus->isHighOrder()){
      hessianFlagMinus = true;
      if (_fullDg)
        thirdFlagMinus = true;
    }

    bool hessianFlagPlus = false;
    bool thirdFlagPlus = false;
    dG3DMaterialLaw* lawplus = dynamic_cast<dG3DMaterialLaw*>(efPlus->getMaterialLaw());
    if (lawplus->isHighOrder()){
      hessianFlagPlus = true;
      if (_fullDg)
        thirdFlagPlus = true;
    }

    AllIPState::ipstateElementContainer *vips = aips->getIPstate(ie->getNum());
    for(int j=0;j<npts;j++){
      // key of gauss point grad value at gauss point
//      Gradm.clear(); Gradp.clear();
//      _spaceminus->gradf(em,uem,vem,wem,Gradm);
//      _spaceplus->gradf(ep,uep,vep,wep,Gradp);

      //std::vector<TensorialTraits<double>::GradType> &Gradm = vgradm[j];
      //std::vector<TensorialTraits<double>::GradType> &Gradp = vgradp[j];

      IPStateBase* ipsm = (*vips)[j];
      IPStateBase* ipsp = (*vips)[j+npts];

      dG3DIPVariableBase* ipvm = static_cast<dG3DIPVariableBase*>(ipsm->getState(ws));
      dG3DIPVariableBase* ipvp = static_cast<dG3DIPVariableBase*>(ipsp->getState(ws));
      dG3DIPVariableBase* ipvmprev = static_cast<dG3DIPVariableBase*>(ipsm->getState(IPStateBase::previous));
      dG3DIPVariableBase* ipvpprev = static_cast<dG3DIPVariableBase*>(ipsp->getState(IPStateBase::previous));

      // get shape function gradients
      std::vector<TensorialTraits<double>::GradType> &Gradm = ipvm->gradf(_spaceminus,em,GPm[j],hessianFlagMinus,thirdFlagMinus);
      std::vector<TensorialTraits<double>::GradType> &Gradp = ipvp->gradf(_spaceplus,ep,GPp[j],hessianFlagPlus,thirdFlagPlus);

      STensor3 *deformationGradientm = &(ipvm->getRefToDeformationGradient());
      deformationGradientm->operator=(0);
      deformationGradientm->operator()(0,0) = 1.;
      deformationGradientm->operator()(1,1) = 1.;
      deformationGradientm->operator()(2,2) = 1.;
      for (int i = 0; i < nbFFm; i++)
      {
        //x y z: take compoenent x arbitrarily
        // xx yy zz xy xz yz
        deformationGradientm->operator()(0,0) += Gradm[i+0*nbFFm][0]*dispm(i);
        deformationGradientm->operator()(0,1) += Gradm[i+0*nbFFm][1]*dispm(i);
        deformationGradientm->operator()(0,2) += Gradm[i+0*nbFFm][2]*dispm(i);
        deformationGradientm->operator()(1,0) += Gradm[i+0*nbFFm][0]*dispm(i+nbFFm);
        deformationGradientm->operator()(1,1) += Gradm[i+0*nbFFm][1]*dispm(i+nbFFm);
        deformationGradientm->operator()(1,2) += Gradm[i+0*nbFFm][2]*dispm(i+nbFFm);
        deformationGradientm->operator()(2,0) += Gradm[i+0*nbFFm][0]*dispm(i+2*nbFFm);
        deformationGradientm->operator()(2,1) += Gradm[i+0*nbFFm][1]*dispm(i+2*nbFFm);
        deformationGradientm->operator()(2,2) += Gradm[i+0*nbFFm][2]*dispm(i+2*nbFFm);
     }
     if(determinantSTensor3(*deformationGradientm) < 1.e-15) Msg::Error("Negative Jacobian");
     STensor3 *deformationGradientp = &(ipvp->getRefToDeformationGradient());
     deformationGradientp->operator=(0);
     deformationGradientp->operator()(0,0) = 1.;
     deformationGradientp->operator()(1,1) = 1.;
     deformationGradientp->operator()(2,2) = 1.;
     for (int i = 0; i < nbFFp; i++)
     {
        //x y z: take compoenent x arbitrarily
        // xx yy zz xy xz yz
        deformationGradientp->operator()(0,0) += Gradp[i+0*nbFFp][0]*dispp(i);
        deformationGradientp->operator()(0,1) += Gradp[i+0*nbFFp][1]*dispp(i);
        deformationGradientp->operator()(0,2) += Gradp[i+0*nbFFp][2]*dispp(i);
        deformationGradientp->operator()(1,0) += Gradp[i+0*nbFFp][0]*dispp(i+nbFFp);
        deformationGradientp->operator()(1,1) += Gradp[i+0*nbFFp][1]*dispp(i+nbFFp);
        deformationGradientp->operator()(1,2) += Gradp[i+0*nbFFp][2]*dispp(i+nbFFp);
        deformationGradientp->operator()(2,0) += Gradp[i+0*nbFFp][0]*dispp(i+2*nbFFp);
        deformationGradientp->operator()(2,1) += Gradp[i+0*nbFFp][1]*dispp(i+2*nbFFp);
        deformationGradientp->operator()(2,2) += Gradp[i+0*nbFFp][2]*dispp(i+2*nbFFp);
     }
     if(determinantSTensor3(*deformationGradientp) < 1.e-15) Msg::Error("Negative Jacobian");

     // key of gauss point grad value at gauss point
//      double uem = GPm[j].pt[0]; double vem = GPm[j].pt[1]; double wem = GPm[j].pt[2];
//      double uep = GPp[j].pt[0]; double vep = GPp[j].pt[1]; double wep = GPp[j].pt[2];
      //ie->getuvwOnElem(GP[j].pt[0],GP[j].pt[1],uem,vem,wem,uep,vep,wep);
//      Grads.clear();
//      _spaceminus->gradfuvw(ele,GP[j].pt[0],GP[j].pt[1],0.,Grads); //use for normal evaluation

      std::vector<TensorialTraits<double>::GradType> &Grads = vgrads[j];
      // minus outward normal
      SVector3 referencePhi0[2];
      SVector3 currentPhi0[2];

      if (efMinus->getDim() == 3){
        for(int k=0;k<nbvertexInter;k++)
        {
          double x = ele->getVertex(k)->x();
          double y = ele->getVertex(k)->y();
          double z = ele->getVertex(k)->z();
          //evaulate displacements at interface
          for(int i=0;i<2;i++)
          {
           referencePhi0[i](0) += Grads[k](i)*x;
           referencePhi0[i](1) += Grads[k+nbvertexInter](i)*y;
           referencePhi0[i](2) += Grads[k+nbvertexInter+nbvertexInter](i)*z;
           currentPhi0[i](0) += Grads[k](i)*(x+dispinter(k));
           currentPhi0[i](1) += Grads[k+nbvertexInter](i)*(y+dispinter(k+nbvertexInter));
           currentPhi0[i](2) += Grads[k+nbvertexInter+nbvertexInter](i)*(z+dispinter(k+2*nbvertexInter));
          }
        }
      }
      else if (efMinus->getDim() == 2){
        for(int k=0;k<nbvertexInter;k++)
        {
          double x = ele->getVertex(k)->x();
          double y = ele->getVertex(k)->y();
          double z = ele->getVertex(k)->z();
           referencePhi0[0](0) += Grads[k](0)*x;
           referencePhi0[0](1) += Grads[k+nbvertexInter](0)*y;
           referencePhi0[0](2) += Grads[k+nbvertexInter+nbvertexInter](0)*z;
           currentPhi0[0](0) += Grads[k](0)*(x+dispinter(k));
           currentPhi0[0](1) += Grads[k+nbvertexInter](0)*(y+dispinter(k+nbvertexInter));
           currentPhi0[0](2) += Grads[k+nbvertexInter+nbvertexInter](0)*(z+dispinter(k+2*nbvertexInter));
        }

        int nbFF = em->getNumVertices();
        SVector3 phi0[2];
        for(int k=0;k<nbFF;k++){
          MVertex* ver = em->getVertex(k);
          double x = ver->x();
          double y = ver->y();
          double z = ver->z();
          for(int i=0;i<2;i++){
            phi0[i](0) += Gradm[k](i)*x;
            phi0[i](1) += Gradm[k+nbFF](i)*y;
            phi0[i](2) += Gradm[k+nbFF+nbFF](i)*z;
          }
        }
        currentPhi0[1] = crossprod(phi0[0],phi0[1]);
        referencePhi0[1] = crossprod(phi0[0],phi0[1]);
      }


      (ipvm->getRefToCurrentOutwardNormal())   = crossprod(currentPhi0[0],currentPhi0[1]);
      (ipvm->getRefToReferenceOutwardNormal()) = crossprod(referencePhi0[0],referencePhi0[1]);

      (ipvp->getRefToCurrentOutwardNormal())   = crossprod(currentPhi0[0],currentPhi0[1]);
      (ipvp->getRefToReferenceOutwardNormal()) = crossprod(referencePhi0[0],referencePhi0[1]);


      SVector3 ujump;
      // compute the jump
      //std::vector<TensorialTraits<double>::ValType> &Valm = vvalm[j];
      //_spaceminus->fuvw(ie->getElem(0),uem, vem, wem, Valm);
      //std::vector<TensorialTraits<double>::ValType> &Valp = vvalp[j];
//      _spaceplus->fuvw(ie->getElem(1),uep, vep, wep, Valp);

      std::vector<TensorialTraits<double>::ValType> &Valm = ipvm->f(_spaceminus,em, GPm[j],hessianFlagMinus,thirdFlagMinus);
      std::vector<TensorialTraits<double>::ValType> &Valp = ipvp->f(_spaceplus,ep, GPp[j],hessianFlagPlus,thirdFlagPlus);

      computeJump(Valm,nbFFm,Valp,nbFFp,dispm,dispp,ujump);
      ipvm->getRefToJump() = ujump;
      ipvp->getRefToJump() = ujump;


    }
  }
  else
  {  // Virtual interface element
     Msg::Error("No virtual interface for 3D");

  }
}
void dG3DDomain::computeIpv(AllIPState *aips,MInterfaceElement *ie, IntPt *GP,const IPStateBase::whichState ws,
                                        partDomain* efMinus, partDomain *efPlus,materialLaw *mlawmi,
                                        materialLaw *mlawp,fullVector<double> &dispm,
                                        fullVector<double> &dispp,const bool virt, bool stiff,const bool checkfrac)
{
  if(_evalStiff){ this->computeStrain(aips,ie, GP,ws,efMinus,efPlus,dispm,dispp,virt);}
  if(!virt){
    //nlsFunctionSpaceXYZ<double> *_spaceminus = static_cast<nlsFunctionSpaceXYZ<double>*>(efMinus->getFunctionSpace());
    //FunctionSpaceBase* _spaceminus = efMinus->getFunctionSpace();
    //nlsFunctionSpaceUVW<double> *spinter = static_cast<nlsFunctionSpaceUVW<double>*>(_spaceInterUVW);
    MElement *ele = dynamic_cast<MElement*>(ie);
    int npts=integBound->getIntPoints(ele,&GP);
    // Get grad shape functions values at all Gauss points
    //std::vector<std::vector<TensorialTraits<double>::GradType> > vgrads;
    //spinter->getgradf(ele,npts,GP,vgrads);
    // Gauss points values on minus and plus elements
    //IntPt *GPm; IntPt *GPp;
    //_interQuad->getIntPoints(ie,GP,&GPm,&GPp);
    // shape functions values at all Gauss points
    //std::vector<std::vector<TensorialTraits<double>::ValType> > vvalm;
    //_spaceminus->get(ie->getElem(0),npts,GPm,vvalm);
    dG3DMaterialLaw *mlawminus = static_cast<dG3DMaterialLaw*>(mlawmi);
    dG3DMaterialLaw *mlawplus = static_cast<dG3DMaterialLaw*>(mlawp);
  // vector with nodal displacement of interface
  // take from minus displacement vector

    //nlsFunctionSpaceXYZ<double> *_spaceplus = static_cast<nlsFunctionSpaceXYZ<double>*>(efPlus->getFunctionSpace());
    /*FunctionSpaceBase* _spaceplus = efPlus->getFunctionSpace();
    dG3DMaterialLaw *mlawplus = static_cast<dG3DMaterialLaw*>(mlawp);
    MElement *em = ie->getElem(0);
    MElement *ep = ie->getElem(1);
    //std::vector<std::vector<TensorialTraits<double>::ValType> > vvalp;
    _spaceplus->get(ep,npts,GPp,vvalp);
    // gauss point
    int nbdofm = _spaceminus->getNumKeys(em);
    int nbdofp = _spaceplus->getNumKeys(ep);
    int nbFFm = em->getNumVertices();
    int nbFFp = ep->getNumVertices();
    fullVector<double> dispinter;
    int nbdofinter = _space->getNumKeys(ele);
    int nbvertexInter = ie->getNumVertices();
    std::vector<int> vn(nbvertexInter);
    ie->getLocalVertexNum(0,vn);
    dispinter.resize(nbdofinter);
    for(int i=0;i<nbvertexInter;i++){
      dispinter(i) = dispm(vn[i]);
      dispinter(i+nbvertexInter) = dispm(vn[i]+nbFFm);
      dispinter(i+2*nbvertexInter) = dispm(vn[i]+2*nbFFm);
    }

    bool hessianFlagMinus = false;
    bool thirdFlagMinus = false;
    if (mlawminus->isHighOrder()){
      hessianFlagMinus = true;
      if (_fullDg)
        thirdFlagMinus = true;
    }

    bool hessianFlagPlus = false;
    bool thirdFlagPlus = false;
    if (mlawminus->isHighOrder()){
      hessianFlagPlus = true;
      if (_fullDg)
        thirdFlagPlus = true;
    }
    */

    AllIPState::ipstateElementContainer *vips = aips->getIPstate(ie->getNum());
    for(int j=0;j<npts;j++){
      // key of gauss point grad value at gauss point
//      double uem = GPm[j].pt[0]; double vem = GPm[j].pt[1]; double wem = GPm[j].pt[2];
//      double uep = GPp[j].pt[0]; double vep = GPp[j].pt[1]; double wep = GPp[j].pt[2];
      //ie->getuvwOnElem(GP[j].pt[0],GP[j].pt[1],uem,vem,wem,uep,vep,wep);
//      Grads.clear();
//      _spaceminus->gradfuvw(ele,GP[j].pt[0],GP[j].pt[1],0.,Grads); //use for normal evaluation

      //std::vector<TensorialTraits<double>::GradType> &Grads = vgrads[j];

      IPStateBase* ipsm = (*vips)[j];
      IPStateBase* ipsp = (*vips)[j+npts];

      dG3DIPVariableBase* ipvm = static_cast<dG3DIPVariableBase*>(ipsm->getState(ws));
      dG3DIPVariableBase* ipvp = static_cast<dG3DIPVariableBase*>(ipsp->getState(ws));
      dG3DIPVariableBase* ipvmprev = static_cast<dG3DIPVariableBase*>(ipsm->getState(IPStateBase::previous));
      dG3DIPVariableBase* ipvpprev = static_cast<dG3DIPVariableBase*>(ipsp->getState(IPStateBase::previous));
      /*
      // minus outward normal
      SVector3 referencePhi0[2];
      SVector3 currentPhi0[2];
      for(int k=0;k<nbvertexInter;k++)
      {
        double x = ele->getVertex(k)->x();
        double y = ele->getVertex(k)->y();
        double z = ele->getVertex(k)->z();
        //evaulate displacements at interface
        for(int i=0;i<2;i++)
        {
         referencePhi0[i](0) += Grads[k](i)*x;
         referencePhi0[i](1) += Grads[k+nbvertexInter](i)*y;
         referencePhi0[i](2) += Grads[k+nbvertexInter+nbvertexInter](i)*z;
         currentPhi0[i](0) += Grads[k](i)*(x+dispinter(k));
         currentPhi0[i](1) += Grads[k+nbvertexInter](i)*(y+dispinter(k+nbvertexInter));
         currentPhi0[i](2) += Grads[k+nbvertexInter+nbvertexInter](i)*(z+dispinter(k+2*nbvertexInter));
        }
      }



      (ipvm->getRefToCurrentOutwardNormal())   = crossprod(currentPhi0[0],currentPhi0[1]);
      (ipvm->getRefToReferenceOutwardNormal()) = crossprod(referencePhi0[0],referencePhi0[1]);

      (ipvp->getRefToCurrentOutwardNormal())   = crossprod(currentPhi0[0],currentPhi0[1]);
      (ipvp->getRefToReferenceOutwardNormal()) = crossprod(referencePhi0[0],referencePhi0[1]);

      SVector3 ujump;
      // compute the jump
      //std::vector<TensorialTraits<double>::ValType> &Valm = vvalm[j];
      //_spaceminus->fuvw(ie->getElem(0),uem, vem, wem, Valm);
      //std::vector<TensorialTraits<double>::ValType> &Valp = vvalp[j];
//      _spaceplus->fuvw(ie->getElem(1),uep, vep, wep, Valp);

      std::vector<TensorialTraits<double>::ValType> &Valm = ipvm->f(_spaceminus,em, GPm[j],hessianFlagMinus,thirdFlagMinus);
      std::vector<TensorialTraits<double>::ValType> &Valp = ipvp->f(_spaceplus,ep, GPp[j],hessianFlagPlus,thirdFlagPlus);

      computeJump(Valm,nbFFm,Valp,nbFFp,dispm,dispp,ujump);

      ipvm->getRefToJump() = ujump;
      ipvp->getRefToJump() = ujump;
      */

      //if(ws != IPStateBase::initial) // CHANGE THIS (COMPUTE INITIAL IPV ON nonLinearInterDomain)
      SVector3 ujump = ipvm->getRefToJump();

      mlawminus->stress(ipvm,ipvmprev, stiff, checkfrac);
      mlawplus->stress(ipvp,ipvpprev, stiff, checkfrac);


      if(mlawminus->getType() == materialLaw::fracture){
        FractureCohesive3DIPVariable *ipvfprevm = static_cast<FractureCohesive3DIPVariable*>(ipvmprev);
        FractureCohesive3DIPVariable *ipvfprevp = static_cast<FractureCohesive3DIPVariable*>(ipvpprev);
        FractureCohesive3DIPVariable *ipvfm = static_cast<FractureCohesive3DIPVariable*>(ipvm);
        FractureCohesive3DIPVariable *ipvfp = static_cast<FractureCohesive3DIPVariable*>(ipvp);
        // minus element
        if( (ipvfprevm->isbroken()) or ( (!checkfrac) and ( (ipvfm->isbroken()) or (ipvfp->isbroken()) ) ) ){
          // if previous broken plus and minus are broken. Second condition is for perturbating for the stiffness matrix computation
//          ipvfm->setFracture(ipvfprevm,ujump,ipvfprevm->getRefToCurrentOutwardNormal());
//          if(checkfrac)
//            Msg::Info("Opening deltamax %e deltan %e deltat %e delta %e",ipvcm->getDeltamax(),ipvcm->getDeltan(),ipvcm->getDeltat(),ipvcm->getDelta());
//          ipvfp->setFracture(ipvfprevp,ujump,ipvfprevm->getRefToCurrentOutwardNormal());
        }
        // check for fracture initialization
        else if(ipvfm->isbroken()){ // previous not broken --> initialization of jump
          if(!ipvfp->isbroken()){ // At this time ipvfp has to be initialized
            const FractureByCohesive3DLaw * mflaw = static_cast<const FractureByCohesive3DLaw*>(mlawminus);
            const LinearCohesive3DLaw *mclaw = static_cast<const LinearCohesive3DLaw*>(mflaw->getFractureLaw());

            ipvfp->setDataFromLaw(ipvfm->getSigmac(),ipvfm->getSNor0(), ipvfm->getTau0(), mclaw->getGc(),ipvfm->getBeta(),ipvfm->ifTension());
            ipvfp->operator =(static_cast < const IPVariable & >(*ipvfm));
            ipvfp->broken();
          }
          // The two are broken --> mean values
          // use previous normal for stiffness matrix
          ipvfm->initmean(ipvfp);
          ipvfm->initfrac(ujump,ipvmprev->getConstRefToCurrentOutwardNormal());
          ipvfp->initfrac(ujump,ipvpprev->getConstRefToCurrentOutwardNormal());
        }
        else if(ipvfp->isbroken()){ // previous not broken -->initialization of jump
          // here we are sure that ipvfm is not broken otherwise previous else if
          const FractureByCohesive3DLaw * mflaw = static_cast<const FractureByCohesive3DLaw*>(mlawminus);
          const LinearCohesive3DLaw *mclaw = static_cast<const LinearCohesive3DLaw*>(mflaw->getFractureLaw());
          // same value than plus element*/
          ipvfm->setDataFromLaw(ipvfp->getSigmac(),ipvfp->getSNor0(), ipvfp->getTau0(),mclaw->getGc(),ipvfp->getBeta(),ipvfp->ifTension());
          ipvfm->operator =(static_cast < const IPVariable & >(*ipvfp));
          ipvfm->broken();  // has to be after computation of reduction element otherwise nalpha = malpha = 0
          // The two are broken --> mean values
          ipvfp->initmean(ipvfm);
          ipvfm->initfrac(ujump,ipvmprev->getConstRefToCurrentOutwardNormal());
          ipvfp->initfrac(ujump,ipvpprev->getConstRefToCurrentOutwardNormal());
        }
      }
    }
  }
  else
  {  // Virtual interface element
     Msg::Error("No virtual interface for 3D");

  }
}

void dG3DDomain::initialIPVariable(AllIPState *aips,const unknownField *ufield,
                                               const IPStateBase::whichState ws)
{
  fullVector<double> unknowns_m;
  fullVector<double> unknowns_p;
  fullVector<double> unknowns_inter;
  std::vector<Dof> R;
  //nlsFunctionSpaceXYZ<double> *spminus = static_cast<nlsFunctionSpaceXYZ<double>*>(_spaceMinus);
  //nlsFunctionSpaceXYZ<double> *spplus = static_cast<nlsFunctionSpaceXYZ<double>*>(_spacePlus);
  nlsFunctionSpaceUVW<double> *spinter = static_cast<nlsFunctionSpaceUVW<double>*>(_spaceInterUVW);
  IntPt *GP;

  // loop on interface
  for(groupOfElements::elementContainer::iterator it=gi->begin(); it!=gi->end(); ++it){
    MInterfaceElement *ie = dynamic_cast<MInterfaceElement*>(*it);
    MElement *ele = *it; //dynamic_cast<MElement*>(ie);
    MElement *em = ie->getElem(0);
    MElement *ep = ie->getElem(1);
    // get displacement
    R.clear();
    _spaceMinus->getKeys(em,R);
    unknowns_m.resize(R.size());
    ufield->get(R,unknowns_m);
    R.clear();
    _spacePlus->getKeys(ep,R);
    unknowns_p.resize(R.size());
    ufield->get(R,unknowns_p);
    int nbdofinter = _space->getNumKeys(ele);
    int nbvertexInter = ie->getNumVertices();
    int nbFFm = ie->getElem(0)->getNumVertices();
    std::vector<int> vn(nbvertexInter);
    ie->getLocalVertexNum(0,vn);
    unknowns_inter.resize(nbdofinter);
    for(int i=0;i<nbvertexInter;i++){
      unknowns_inter(i) = unknowns_m(vn[i]);
      unknowns_inter(i+nbvertexInter) = unknowns_m(vn[i]+nbFFm);
      unknowns_inter(i+2*nbvertexInter) = unknowns_m(vn[i]+2*nbFFm);
    }

    AllIPState::ipstateElementContainer *vips = aips->getIPstate(ie->getNum());
    int npts = integBound->getIntPoints(*it,&GP);
    // Grads values on all Gauss points;
    std::vector<std::vector<TensorialTraits<double>::GradType> > vgrads;
    spinter->getgradf(ele,npts,GP,vgrads);
    // Get Gauss points on minus and plus elements
    //IntPt *GPm; IntPt *GPp;
    //_interQuad->getIntPoints(ie,GP,&GPm,&GPp);
    // Grads values on all Gauss points;
    //std::vector<std::vector<TensorialTraits<double>::GradType> > vgradm;
    //spminus->getgradf(em,npts,GPm,vgradm);
    //std::vector<std::vector<TensorialTraits<double>::GradType> > vgradp;
    //spplus->getgradf(ep,npts,GPp,vgradp);
    for(int j=0;j<npts;j++){
      // key of gauss point grad value at gauss point
//      double uem = GPm[j].pt[0];  double vem = GPm[j].pt[1];  double wem = GPm[j].pt[2];
//      double uep = GPp[j].pt[0];  double vep = GPp[j].pt[1];  double wep = GPp[j].pt[2];
      //ie->getuvwOnElem(GP[j].pt[0],GP[j].pt[1],uem,vem,wem,uep,vep,wep);
//      Grads.clear(); Gradm.clear(); Gradp.clear();
//      spminus->gradfuvw(ele,GP[j].pt[0],GP[j].pt[1],0.,Grads);
//      spminus->gradf(em,uem,vem,wem,Gradm);
//      spplus->gradf(ep,uep,vep,wep,Gradp);
      std::vector<TensorialTraits<double>::GradType> &Grads = vgrads[j];
      //std::vector<TensorialTraits<double>::GradType> &Gradm = vgradm[j];
      //std::vector<TensorialTraits<double>::GradType> &Gradp = vgradp[j];
      // local basis on element is needed to compute the local basis on interfaceelement (normal)
      IPStateBase* ipsm = (*vips)[j];
      IPStateBase* ipsp = (*vips)[j+npts];

      dG3DIPVariableBase* ipvm = static_cast<dG3DIPVariableBase*>(ipsm->getState(IPStateBase::initial));
      dG3DIPVariableBase* ipvp = static_cast<dG3DIPVariableBase*>(ipsp->getState(IPStateBase::initial));
      // set initial Jacobian value in other ipv (change this HOW ??)
      dG3DIPVariableBase* ipvmprev = static_cast<dG3DIPVariableBase*>(ipsm->getState(IPStateBase::previous));
      dG3DIPVariableBase* ipvmcur = static_cast<dG3DIPVariableBase*>(ipsm->getState(IPStateBase::current));
      dG3DIPVariableBase* ipvpprev = static_cast<dG3DIPVariableBase*>(ipsp->getState(IPStateBase::previous));
      dG3DIPVariableBase* ipvpcur = static_cast<dG3DIPVariableBase*>(ipsp->getState(IPStateBase::current));

      // minus outward normal
      SVector3 referencePhi0[2];
      SVector3 currentPhi0[2];
      for(int k=0;k<nbvertexInter;k++)
      {
        double x = ele->getVertex(k)->x();
        double y = ele->getVertex(k)->y();
        double z = ele->getVertex(k)->z();
        //evaulate displacements at interface
        for(int i=0;i<2;i++)
        {
          referencePhi0[i](0) += Grads[k](i)*x;
          referencePhi0[i](1) += Grads[k+nbvertexInter](i)*y;
          referencePhi0[i](2) += Grads[k+nbvertexInter+nbvertexInter](i)*z;
          currentPhi0[i](0) += Grads[k](i)*(x+unknowns_inter(k));
          currentPhi0[i](1) += Grads[k+nbvertexInter](i)*(y+unknowns_inter(k+nbvertexInter));
          currentPhi0[i](2) += Grads[k+nbvertexInter+nbvertexInter](i)*(z+unknowns_inter(k+2*nbvertexInter));
        }
      }
      (ipvm->getRefToCurrentOutwardNormal())       = crossprod(currentPhi0[0],currentPhi0[1]);
      (ipvm->getRefToReferenceOutwardNormal())     = crossprod(referencePhi0[0],referencePhi0[1]);
      (ipvmprev->getRefToCurrentOutwardNormal())   = crossprod(currentPhi0[0],currentPhi0[1]);
      (ipvmprev->getRefToReferenceOutwardNormal()) = crossprod(referencePhi0[0],referencePhi0[1]);
      (ipvmcur->getRefToCurrentOutwardNormal())    = crossprod(currentPhi0[0],currentPhi0[1]);
      (ipvmcur->getRefToReferenceOutwardNormal())  = crossprod(referencePhi0[0],referencePhi0[1]);
      /*ipvm->initialize();
      ipvmprev->initialize();
      ipvmcur->initialize();

      ipvp->initialize();
      ipvpprev->initialize();
      ipvpcur->initialize();*/
      (ipvp->getRefToCurrentOutwardNormal())       = crossprod(currentPhi0[0],currentPhi0[1]);
      (ipvp->getRefToReferenceOutwardNormal())     = crossprod(referencePhi0[0],referencePhi0[1]);
      (ipvpprev->getRefToCurrentOutwardNormal())   = crossprod(currentPhi0[0],currentPhi0[1]);
      (ipvpprev->getRefToReferenceOutwardNormal()) = crossprod(referencePhi0[0],referencePhi0[1]);
      (ipvpcur->getRefToCurrentOutwardNormal())    = crossprod(currentPhi0[0],currentPhi0[1]);
      (ipvpcur->getRefToReferenceOutwardNormal())  = crossprod(referencePhi0[0],referencePhi0[1]);

    }

  }
  // loop on element
  fullVector<double> disp;
  for(groupOfElements::elementContainer::iterator it=g->begin(); it!=g->end(); ++it)
  {
    MElement *ele = *it;
    // get displacement of element to known the current element configuration
    R.clear();
    //nlsFunctionSpaceXYZ<double> *sp = static_cast<nlsFunctionSpaceXYZ<double>*>(_space);
    _space->getKeys(ele,R);
    disp.resize(R.size());
    ufield->get(R,disp);
    // loop on Gauss's point
//    IntPt *GP;
    int npts = integBulk->getIntPoints(ele,&GP);
    AllIPState::ipstateElementContainer *vips = aips->getIPstate(ele->getNum());
    // grads values at all Gauss points
    //std::vector<std::vector<TensorialTraits<double>::GradType> > vgrads;
    //sp->getgradf(ele,npts,GP,vgrads);
    for(int j=0;j<npts;j++){
      //grads.clear();
      //sp->gradf(ele,GP[j].pt[0],GP[j].pt[1],GP[j].pt[2],grads);
      //std::vector<TensorialTraits<double>::GradType> &grads = vgrads[j];

      dG3DIPVariableBase *ipv = static_cast<dG3DIPVariableBase*>((*vips)[j]->getState(IPStateBase::initial));
      // set initial jacobian in other ipv (Change this HOW ??)
      dG3DIPVariableBase *ipvcur = static_cast<dG3DIPVariableBase*>((*vips)[j]->getState(IPStateBase::current));
      dG3DIPVariableBase *ipvprev = static_cast<dG3DIPVariableBase*>((*vips)[j]->getState(IPStateBase::previous));

      /*ipv->initialize(Gradm);
      ipvprev->initialize(Gradm);
      ipvcur->initialize(Gradm);*/
    }
  }
}
void dG3DDomain::setGaussIntegrationRule(){
  if (this->g->size()>0){
    groupOfElements::elementContainer::iterator it = g->begin();
    MElement *e = *it;
    // default case bulk
    if(_gaussorderbulk == -1)
    {
      if(e->getTypeForMSH() == MSH_TET_10)
      {
        integBulk = new GaussQuadrature(2); // under integration
      }
      else if(e->getTypeForMSH() == MSH_HEX_27)
      {
        integBulk = new GaussQuadrature(4); // full integration
      }
      else if(e->getTypeForMSH() == MSH_HEX_8)
      {
        integBulk = new GaussQuadrature(2); // under integration
      }
      else if (e->getTypeForMSH() == MSH_QUA_9){
        integBulk = new GaussQuadrature(2);
      }
      else if (e->getTypeForMSH() == MSH_QUA_4){
        integBulk = new GaussQuadrature(2);
      }
      else if (e->getTypeForMSH() == MSH_TRI_6){
        integBulk = new GaussQuadrature(2);
      }
      else if (e->getTypeForMSH() == MSH_TRI_3){
        integBulk = new GaussQuadrature(2);
      }
    }
    else{
      integBulk = new GaussQuadrature(_gaussorderbulk);
    }
    // default case bound
    if(_gaussorderbound == -1)
    {
      if(e->getTypeForMSH() == MSH_TET_10)
      {
        if( _gqt == Gauss){
          integBound = new GaussQuadrature(4);
        }
        else
          integBound = new GaussLobatto2DQuadrature(2);
        _interQuad = new interface2DQuadrature(4);  // same number of Gauss point Gauss & Lobato = 2 ??
      }
      else if(e->getTypeForMSH() == MSH_HEX_27)
      {
        if( _gqt == Gauss)
          integBound = new GaussQuadrature(4);
        else
          integBound = new GaussLobatto2DQuadrature(3);
        _interQuad = new interface2DQuadrature(4);  // same number of Gauss point Gauss & Lobato = 2 ?? Why 3
      }
      else if(e->getTypeForMSH() == MSH_HEX_8)
      {
        if( _gqt == Gauss)
          integBound = new GaussQuadrature(3);
        else
          integBound = new GaussLobatto2DQuadrature(2);
      _interQuad = new interface2DQuadrature(3);  // same number of Gauss point Gauss & Lobato = 3 ??
      }
      else if (e->getTypeForMSH() == MSH_QUA_9)
      {
        integBound = new GaussQuadrature(5); // 3 pts
        _interQuad = new interface1DQuadrature(5);
      }
      else if (e->getTypeForMSH() == MSH_QUA_4)
      {
        integBound = new GaussQuadrature(5); // 3 pts
        _interQuad = new interface1DQuadrature(5);
      }
      else if( e->getTypeForMSH() == MSH_TRI_6)
      {
        integBound = new GaussQuadrature(5); // 3 pts
        _interQuad = new interface1DQuadrature(5);
      }
      else if( e->getTypeForMSH() == MSH_TRI_3)
      {
        integBound = new GaussQuadrature(5); // 3 pts
        _interQuad = new interface1DQuadrature(5);
      }
    }
    else{
      if(getDim()==3){
        _gqt == Gauss ? integBound = new GaussQuadrature(_gaussorderbound) : integBound = new GaussLobatto2DQuadrature(_gaussorderbound);
        _interQuad = new interface2DQuadrature(_gaussorderbound);
      }
      else if (getDim() == 2){
        integBound = new GaussQuadrature(_gaussorderbound);
        _interQuad = new interface1DQuadrature(_gaussorderbound);
      }
    }
  }
  else{
    integBulk = new QuadratureVoid();
    integBound = new QuadratureVoid();
    _interQuad = new interfaceQuadratureVoid();
  }
}

void dG3DDomain::allocateInterfaceMPI(const int rankOtherPart, const groupOfElements &groupOtherPart,dgPartDomain** dgdom)
{
  // domain creation has it function create a domain with a part of the same domain contained on an other part,
  // the type of domain is known
  if(*dgdom==NULL){ // domain doesn't exist --> creation
    (*dgdom) = new interDomainBetween3D(this->getTag(),this,this,0);
    // set the stability parameters
    dG3DDomain* newInterDom3D = static_cast< dG3DDomain* >(*dgdom);
    newInterDom3D->stabilityParameters(this->_beta1);
  }
}


// copy paste of linear change this !!
void dG3DDomain::createInterfaceMPI(const int rankOtherPart, const groupOfElements &groupOtherPart,dgPartDomain* &dgdom)
{
  // domain creation has it function create a domain with a part of the same domain contained on an other part,
  // the type of domain is known
  allocateInterfaceMPI(rankOtherPart, groupOtherPart,&dgdom);
  // create interface (no need to use an interface builder ??)

  if (getDim() ==2){
    std::set<IEdge> map_edge;// the type Iedge is known
    // create interface with groupOtherPart (no match possible match has to be done with dom->gib)
    for(groupOfElements::elementContainer::const_iterator it = groupOtherPart.begin(); it != groupOtherPart.end(); ++it)
    {
      MElement *e=*it;
      int nedge = e->getNumEdges();
      for(int j=0;j<nedge;j++){
        std::vector<MVertex*> vv;
        e->getEdgeVertices(j,vv);
          map_edge.insert(IEdge(vv,e,this));
      }
    }
    // now use interface created on boundary to identify the interface element
    for(groupOfElements::elementContainer::iterator it=giv->begin(); it!=giv->end(); ++it) // loop on gib too (normally no need)
    {
      MInterfaceElement *interele = dynamic_cast<MInterfaceElement*>(*it);
      MElement *e = interele->getElem(0); // on boundary interface geetElem(0) == getElem(1)
      int nedge = e->getNumEdges();
      for(int j=0;j<nedge;j++){
      std::vector<MVertex*> vv;
      e->getEdgeVertices(j,vv);
      IEdge ie = IEdge(vv,e,this);
      const interfaceKeys& key = ie.getkey();
        std::set<IEdge>::iterator it_edge;
        bool findedge=false;
        for(it_edge = map_edge.begin(); it_edge != map_edge.end();++it_edge)
        {
          if(  it_edge->getkey() == key)
          {
            findedge = true;
            break;
          }
        }
        if(findedge){
          MElement *interel;
          // the partition with the lowest number is choosen by convention like the one corresponding to minus element
          if(dgdom->getPhysical() == _phys){
            if(Msg::GetCommRank() < rankOtherPart){
              interel =  new MInterfaceLine(vv, 0, Msg::GetCommRank()+1, e, it_edge->getElement());
            }
            else{
              // vv has to be oriented has the minus element --> get the
              std::vector<MVertex*> vvother = it_edge->getVertices();
              interel = new MInterfaceLine(vvother,0,Msg::GetCommRank()+1,it_edge->getElement(),e);
            }
            _groupGhostMPI->insert(it_edge->getElement());
          }
          else{ // interDomain defined between 2 partitions. The minus element has to be included in the minus domain
            groupOfElements::elementContainer::iterator itfound = dgdom->getMinusDomain()->g->end();
            for(groupOfElements::elementContainer::iterator ite=dgdom->getMinusDomain()->g->begin(); ite!=dgdom->getMinusDomain()->g->end();++ite)
            {
              if(e->getNum()==(*ite)->getNum())
              {
                itfound = ite;
                break;
              }
            }
            if(itfound!=dgdom->getMinusDomain()->g->end())
            {
              interel =  new MInterfaceLine(vv, 0, Msg::GetCommRank()+1, e, it_edge->getElement());
              // add element of other partition to prescribed Dirichlet BC later
              if(e->getPartition() != Msg::GetCommRank()+1)
                 dgdom->getMinusDomain()->_groupGhostMPI->insert(e);
              else
                 dgdom->getPlusDomain()->_groupGhostMPI->insert(it_edge->getElement());

            }
            else{ // e is included in the plus Domain
              // vv has to be oriented has the minus element --> get the
              std::vector<MVertex*> vvother = it_edge->getVertices();
              interel = new MInterfaceLine(vvother,0,Msg::GetCommRank()+1,it_edge->getElement(),e);
              // add element of other partition to prescribed Dirichlet BC later
              if(e->getPartition() != Msg::GetCommRank()+1)
                 dgdom->getPlusDomain()->_groupGhostMPI->insert(e);
              else
                 dgdom->getMinusDomain()->_groupGhostMPI->insert(it_edge->getElement());
            }
          }
          dgdom->gi->insert(interel);
          // add element of other partition to prescribed Dirichlet BC later
          map_edge.erase(it_edge);
        }
      }
    }
  }
  else if (getDim() == 3) {
    std::set<IFace> map_face;// the type Iedge is known
    // create interface with groupOtherPart (no match possible match has to be done with dom->gib)
    for(groupOfElements::elementContainer::const_iterator it = groupOtherPart.begin(); it != groupOtherPart.end(); ++it)
    {
      MElement *e=*it;
      int nface = e->getNumFaces();
      for(int j=0;j<nface;j++){
        std::vector<MVertex*> vv;
        /*
        if(e->getTypeForMSH() == MSH_TET_10)
        {
           std::vector<MVertex*> vvv;
           e->getFaceVertices(j,vvv);
           vv.push_back(vvv[0]);
           vv.push_back(vvv[1]);
           vv.push_back(vvv[2]);
             if(j==0)
             {
               vv.push_back(vvv[5]);
               vv.push_back(vvv[4]);
               vv.push_back(vvv[3]);
             }
             else if(j==1)
             {
               vv.push_back(vvv[3]);
               vv.push_back(vvv[5]);
               vv.push_back(vvv[4]);
             }
             else if(j==2)
             {
               vv.push_back(vvv[4]);
               vv.push_back(vvv[5]);
               vv.push_back(vvv[3]);
             }
             else
             {
               vv.push_back(vvv[5]);
               vv.push_back(vvv[3]);
               vv.push_back(vvv[4]);
             }
          }
          else
          */
            e->getFaceVertices(j,vv);

          map_face.insert(IFace(vv,e,this));
      }
    }
    // now use interface created on boundary to identify the interface element
    for(groupOfElements::elementContainer::iterator it=giv->begin(); it!=giv->end(); ++it) // loop on gib too (normally no need)
    {
      MInterfaceElement *interele = dynamic_cast<MInterfaceElement*>(*it);
      MElement *e = interele->getElem(0); // on boundary interface geetElem(0) == getElem(1)
      int nface = e->getNumFaces();
      for(int j=0;j<nface;j++)
      {
        std::vector<MVertex*> vv;
        /*
        if(e->getTypeForMSH() == MSH_TET_10)
        {
           std::vector<MVertex*> vvv;
           e->getFaceVertices(j,vvv);
           vv.push_back(vvv[0]);
           vv.push_back(vvv[1]);
           vv.push_back(vvv[2]);
           if(j==0)
           {
             vv.push_back(vvv[5]);
             vv.push_back(vvv[4]);
             vv.push_back(vvv[3]);
           }
           else if(j==1)
           {
             vv.push_back(vvv[3]);
             vv.push_back(vvv[5]);
             vv.push_back(vvv[4]);
           }
           else if(j==2)
           {
             vv.push_back(vvv[4]);
             vv.push_back(vvv[5]);
             vv.push_back(vvv[3]);
           }
           else
           {
             vv.push_back(vvv[5]);
             vv.push_back(vvv[3]);
             vv.push_back(vvv[4]);
           }
        }
        else
        */
        e->getFaceVertices(j,vv);
        IFace ie = IFace(vv,e,this);
        const interfaceKeys& key = ie.getkey();
        std::set<IFace>::iterator it_face;
        bool findface=false;
        for(it_face = map_face.begin(); it_face != map_face.end();++it_face)
        {
          if(  it_face->getkey() == key)
          {
            findface = true;
            break;
          }
        }
        if(findface){
          MElement *interel;
          // the partition with the lowest number is choosen by convention like the one corresponding to minus element
          if(_phys == dgdom->getPhysical())
          {
            if(Msg::GetCommRank() < rankOtherPart)
            {
              if(e->getTypeForMSH() == MSH_TET_10)
              {
                interel =  new MInterfaceTriangleN(vv, 2, 0, Msg::GetCommRank()+1, e, it_face->getElement());
              }
              else if(e->getTypeForMSH() == MSH_HEX_8)
              {
                interel =  new MInterfaceQuadrangleN(vv, 1, 0, Msg::GetCommRank()+1, e, it_face->getElement());
              }
              else if(e->getTypeForMSH() == MSH_HEX_27)
              {
                interel =  new MInterfaceQuadrangleN(vv, 2, 0, Msg::GetCommRank()+1, e, it_face->getElement());
              }
              else
              {
                Msg::Error("dG3DDomain::createInterfaceMPI");
              }
            }
            else
            {
              // vv has to be oriented has the minus element --> get the
              std::vector<MVertex*> vvother = it_face->getVertices();
              if(it_face->getElement()->getTypeForMSH() == MSH_TET_10)
              {
                interel =  new MInterfaceTriangleN(vvother,2, 0,Msg::GetCommRank()+1,it_face->getElement(),e);
              }
              else if(it_face->getElement()->getTypeForMSH() == MSH_HEX_8)
              {
                interel =  new MInterfaceQuadrangleN(vvother,1,0,Msg::GetCommRank()+1,it_face->getElement(),e);
              }
              else if(it_face->getElement()->getTypeForMSH() == MSH_HEX_27)
              {
                interel =  new MInterfaceQuadrangleN(vvother,2,0,Msg::GetCommRank()+1,it_face->getElement(),e);
              }
              else
              {
                Msg::Error("dG3DDomain::createInterfaceMPI");
              }
            }
            // add element of other partition to prescribed Dirichlet BC later
            _groupGhostMPI->insert(it_face->getElement());
          }
          else{ // interDomain defined between 2 partitions the minus element has to be included in the minus domain
                 // idem when filling _groupGhsotMPI
            groupOfElements::elementContainer::iterator itfound = dgdom->getMinusDomain()->g->end();
            for(groupOfElements::elementContainer::iterator ite=dgdom->getMinusDomain()->g->begin(); ite!=dgdom->getMinusDomain()->g->end();++ite)
            {
              if(e->getNum()==(*ite)->getNum())
              {
                itfound = ite;
                break;
              }
            }
            if(itfound!=dgdom->getMinusDomain()->g->end())
            {
              if(e->getTypeForMSH() == MSH_TET_10)
              {
                interel =  new MInterfaceTriangleN(vv, 2, 0, Msg::GetCommRank()+1, e, it_face->getElement());
              }
              else if(e->getTypeForMSH() == MSH_HEX_8)
              {
                interel =  new MInterfaceQuadrangleN(vv, 1, 0, Msg::GetCommRank()+1, e, it_face->getElement());
              }
              else if(e->getTypeForMSH() == MSH_HEX_27)
              {
                interel =  new MInterfaceQuadrangleN(vv, 2, 0, Msg::GetCommRank()+1, e, it_face->getElement());
              }
              else
              {
                Msg::Error("dG3DDomain::createInterfaceMPI");
              }
              // add element of other partition to prescribed Dirichlet BC later
              if(e->getPartition() != Msg::GetCommRank()+1)
                 dgdom->getMinusDomain()->_groupGhostMPI->insert(e);
              else
                 dgdom->getPlusDomain()->_groupGhostMPI->insert(it_face->getElement());

            }
            else
            {
               // vv has to be oriented has the minus element --> get the
              std::vector<MVertex*> vvother = it_face->getVertices();
              if(it_face->getElement()->getTypeForMSH() == MSH_TET_10)
              {
                interel =  new MInterfaceTriangleN(vvother,2, 0,Msg::GetCommRank()+1,it_face->getElement(),e);
              }
              else if(it_face->getElement()->getTypeForMSH() == MSH_HEX_8)
              {
                interel =  new MInterfaceQuadrangleN(vvother,1,0,Msg::GetCommRank()+1,it_face->getElement(),e);
              }
              else if(it_face->getElement()->getTypeForMSH() == MSH_HEX_27)
              {
                interel =  new MInterfaceQuadrangleN(vvother,2,0,Msg::GetCommRank()+1,it_face->getElement(),e);
              }
              else
              {
                Msg::Error("dG3DDomain::createInterfaceMPI");
              }
              // add element of other partition to prescribed Dirichlet BC later
              if(e->getPartition() != Msg::GetCommRank()+1)
                 dgdom->getPlusDomain()->_groupGhostMPI->insert(e);
              else
                 dgdom->getMinusDomain()->_groupGhostMPI->insert(it_face->getElement());
            }
          }
          dgdom->gi->insert(interel);
          map_face.erase(it_face);
        }
      }
    }
  }
}



FilterDof* dG3DDomain::createFilterDof(const int comp) const
{
  FilterDof* fdof = new DG3DFilterDofComponent(comp);
  return fdof;

}

void dG3DDomain::gaussIntegration(const GaussType gqt,const int orderbulk_, const int orderbound_){
  _gqt = gqt;
  _gaussorderbulk = orderbulk_;
  _gaussorderbound= orderbound_;
}


void dG3DDomain::createInterface(manageInterface &maninter)
{
// create all interface even if CG to allow CG/DG mixed formulation
// if CG interface are not added in vector
//  if(_fullDg)
//  {
    for (groupOfElements::elementContainer::const_iterator it = g->begin(); it != g->end(); ++it)
    {
      MElement *e=*it;
      if (e->getDim() == 3){
        int nface = e->getNumFaces();
        for(int j=0;j<nface;j++){
          std::vector<MVertex*> vv;
          /*
          if(e->getTypeForMSH() == MSH_TET_10)
          {
             std::vector<MVertex*> vvv;
             e->getFaceVertices(j,vvv);
             vv.push_back(vvv[0]);
             vv.push_back(vvv[1]);
             vv.push_back(vvv[2]);
             if(j==0)
             {
               vv.push_back(vvv[5]);
               vv.push_back(vvv[4]);
               vv.push_back(vvv[3]);
             }
             else if(j==1)
             {
               vv.push_back(vvv[3]);
               vv.push_back(vvv[5]);
               vv.push_back(vvv[4]);
             }
             else if(j==2)
             {
               vv.push_back(vvv[4]);
               vv.push_back(vvv[5]);
               vv.push_back(vvv[3]);
             }
             else
             {
               vv.push_back(vvv[5]);
               vv.push_back(vvv[3]);
               vv.push_back(vvv[4]);
             }
          }
          else
          */
          e->getFaceVertices(j,vv);
          IElement *ie = new IFace(vv,e,this);
          maninter.insert(ie,this);
        }
      }
      else if (e->getDim() == 2){
        int nedge = e->getNumEdges();
        for(int j=0;j<nedge;j++){
          std::vector<MVertex*> vv;
          e->getEdgeVertices(j,vv);
          IElement *ie = new IEdge(vv,e,this);
          maninter.insert(ie,this);
        }
      }
    }
//  }
}

MElement* dG3DDomain::createVirtualInterface(IElement *ie) const
{
  std::vector<MVertex*> Mv = ie->getVertices();
  MElement *ele;
  if(ie->getElement()->getTypeForMSH() == MSH_TET_10)
  {
    ele = new MInterfaceTriangleN(Mv,2,0,0,ie->getElement(),ie->getElement());
  }
  else if(ie->getElement()->getTypeForMSH() == MSH_HEX_8)
  {
    ele = new MInterfaceQuadrangleN(Mv,1,0,0,ie->getElement(),ie->getElement());
  }
  else if(ie->getElement()->getTypeForMSH() == MSH_HEX_27)
  {
    ele = new MInterfaceQuadrangleN(Mv,2,0,0,ie->getElement(),ie->getElement());
  }
  else if (ie->getElement()->getDim() == 2){
    ele = new MInterfaceLine(Mv,0,0,ie->getElement(),ie->getElement());
  }
  else
  {
    Msg::Error("missing case in dG3DDomain::createVirtualInterface");
  }
  return ele;
}
MElement* dG3DDomain::createInterface(IElement *ie1, IElement *ie2) const
{
  #if defined(HAVE_MPI)
  if(Msg::GetCommSize() != 1){ // with mpi special attention is needed for creation of
                               // interface on interface domain
                               // minus element == element with the lowest part number (if they are different)
    int part1 = ie1->getElement()->getPartition();
    int part2 = ie2->getElement()->getPartition();
    MElement *nelem;
    // HERE WE USE DG FOR
    //         1° 2 domains -> if phys1 != phys2
    //         2° Parallel -> if part1 != part2
    //         3° if choice -> _full dg

    if(ie2->getPhys() > ie1->getPhys() )
    {
      std::vector<MVertex*> vv = ie1->getVertices();
      if(ie1->getElement()->getTypeForMSH() == MSH_TET_10)
      {
        nelem = new MInterfaceTriangleN(vv, 2, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
      }
      else if(ie1->getElement()->getTypeForMSH() == MSH_HEX_8)
      {
        nelem = new MInterfaceQuadrangleN(vv, 1, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
      }
      else if(ie1->getElement()->getTypeForMSH() == MSH_HEX_27)
      {
        nelem = new MInterfaceQuadrangleN(vv, 2, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
      }
      else if (ie1->getElement()->getDim()==2){
        nelem = new MInterfaceLine(vv,0,Msg::GetCommRank()+1,ie1->getElement(),ie2->getElement());
      }
      else
      {
        Msg::Error("missing case in dG3DDomain::createInterface");
        nelem = NULL;
      }
    }
    else if(ie2->getPhys() < ie1->getPhys() )
    {
      std::vector<MVertex*> vv = ie2->getVertices();
      if(ie2->getElement()->getTypeForMSH() == MSH_TET_10)
      {
        nelem = new MInterfaceTriangleN(vv, 2, 0,Msg::GetCommRank()+1,ie2->getElement(),ie1->getElement());
      }
      else if(ie2->getElement()->getTypeForMSH() == MSH_HEX_8)
      {
        nelem = new MInterfaceQuadrangleN(vv, 1, 0,Msg::GetCommRank()+1,ie2->getElement(),ie1->getElement());
      }
      else if(ie2->getElement()->getTypeForMSH() == MSH_HEX_27)
      {
        nelem = new MInterfaceQuadrangleN(vv, 2, 0,Msg::GetCommRank()+1,ie2->getElement(),ie1->getElement());
      }
      else if (ie2->getElement()->getDim()==2){
        nelem  = new MInterfaceLine(vv,0,Msg::GetCommRank()+1,ie2->getElement(),ie1->getElement());
      }
      else
      {
        Msg::Error("missing case in dG3DDomain::createInterface");
        nelem=NULL;
      }
    }
    else if(part2 > part1){
      std::vector<MVertex*> vv = ie1->getVertices();
      if(ie1->getElement()->getTypeForMSH() == MSH_TET_10)
      {
        nelem = new MInterfaceTriangleN(vv, 2, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
      }
      else if(ie1->getElement()->getTypeForMSH() == MSH_HEX_8)
      {
        nelem = new MInterfaceQuadrangleN(vv, 1, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
      }
      else if(ie1->getElement()->getTypeForMSH() == MSH_HEX_27)
      {
        nelem = new MInterfaceQuadrangleN(vv, 2, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
      }
      else if (ie1->getElement()->getDim()==2){
        nelem = new MInterfaceLine(vv,0,Msg::GetCommRank()+1,ie1->getElement(),ie2->getElement());
      }
      else
      {
        Msg::Error("missing case in dG3DDomain::createInterface");
      }
    }
    else if(part1 > part2){
      std::vector<MVertex*> vv = ie2->getVertices();
      if(ie2->getElement()->getTypeForMSH() == MSH_TET_10)
      {
        nelem = new MInterfaceTriangleN(vv, 2, 0,Msg::GetCommRank()+1,ie2->getElement(),ie1->getElement());
      }
      else if(ie2->getElement()->getTypeForMSH() == MSH_HEX_8)
      {
        nelem = new MInterfaceQuadrangleN(vv, 1, 0,Msg::GetCommRank()+1,ie2->getElement(),ie1->getElement());
      }
      else if(ie2->getElement()->getTypeForMSH() == MSH_HEX_27)
      {
        nelem = new MInterfaceQuadrangleN(vv, 2, 0,Msg::GetCommRank()+1,ie2->getElement(),ie1->getElement());
      }
      else if (ie2->getElement()->getDim()==2){
        nelem = new MInterfaceLine(vv,0,Msg::GetCommRank()+1,ie2->getElement(),ie1->getElement());
      }
      else
      {
        Msg::Error("missing case in dG3DDomain::createInterface");
        nelem = NULL;
      }
    }
    else if(_fullDg or _isHighOrder) //arbitrary choice
    {
      std::vector<MVertex*> vv = ie1->getVertices();
      if(ie1->getElement()->getTypeForMSH() == MSH_TET_10)
      {
        nelem = new MInterfaceTriangleN(vv, 2, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
      }
      else if(ie1->getElement()->getTypeForMSH() == MSH_HEX_8)
      {
        nelem = new MInterfaceQuadrangleN(vv, 1, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
      }
      else if(ie1->getElement()->getTypeForMSH() == MSH_HEX_27)
      {
        nelem = new MInterfaceQuadrangleN(vv, 2, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
      }
      else if (ie1->getElement()->getDim()==2){
        nelem = new MInterfaceLine(vv,0,Msg::GetCommRank()+1,ie1->getElement(),ie2->getElement());
      }
      else
      {
        Msg::Error("missing case in dG3DDomain::createInterface");
      }
    }
    else
    {
      nelem = NULL;
    }
    if((part1 != Msg::GetCommRank() + 1) and (part1 != 0)) // add ghost interface for Dirichlet BC
    {
      ie1->getDomain()->_groupGhostMPI->insert(ie1->getElement());
    }
    if((part2 != Msg::GetCommRank() + 1) and (part2 != 0)) // add ghost interface for Dirichlet BC
    {
      ie2->getDomain()->_groupGhostMPI->insert(ie2->getElement());
    }
    return nelem;
  }
  else
 #endif // HAVE_MPI
  {
    if(_isHighOrder or _fullDg or (ie1->getPhys() != ie2->getPhys())){
      if(ie1->getPhys() <= ie2->getPhys())
      {
        std::vector<MVertex*> vv = ie1->getVertices();
        MElement *nelem;
        if(ie1->getElement()->getTypeForMSH() == MSH_TET_10)
        {
          nelem = new MInterfaceTriangleN(vv, 2, 0, 0, ie1->getElement(), ie2->getElement());
        }
        else if(ie1->getElement()->getTypeForMSH() == MSH_HEX_8)
        {
          nelem = new MInterfaceQuadrangleN(vv, 1, 0, 0, ie1->getElement(), ie2->getElement());
        }
        else if(ie1->getElement()->getTypeForMSH() == MSH_HEX_27)
        {
          nelem = new MInterfaceQuadrangleN(vv, 2, 0, 0, ie1->getElement(), ie2->getElement());
        }
        else if (ie1->getElement()->getDim()==2){
          nelem = new MInterfaceLine(vv,0,0,ie1->getElement(),ie2->getElement());
        }
        else
        {
          Msg::Error("missing case in dG3DDomain::createInterface");
        }
        return nelem;
      }
      else
      {
        std::vector<MVertex*> vv = ie2->getVertices();
        MElement *nelem;
        if(ie2->getElement()->getTypeForMSH() == MSH_TET_10)
        {
          nelem = new MInterfaceTriangleN(vv, 2, 0, 0, ie2->getElement(), ie1->getElement());
        }
        else if(ie2->getElement()->getTypeForMSH() == MSH_HEX_8)
        {
          nelem = new MInterfaceQuadrangleN(vv, 1, 0, 0, ie2->getElement(), ie1->getElement());
        }
        else if(ie2->getElement()->getTypeForMSH() == MSH_HEX_27)
        {
          nelem = new MInterfaceQuadrangleN(vv, 2, 0, 0, ie2->getElement(), ie1->getElement());
        }
        else if (ie2->getElement()->getDim()==2){
          nelem = new MInterfaceLine(vv,0,0,ie2->getElement(),ie1->getElement());
        }
        else
        {
          Msg::Error("missing case in dG3DDomain::createInterface");
        }
        return nelem;
      }
    }
    else{
      return NULL;
    }
  }
}


void dG3DDomain::createIPState(AllIPState::ipstateContainer& map, const bool *state) const{
  #if defined(HAVE_MPI)
  if (this->getMaterialLawMinus()->getType()== materialLaw::numeric and  _otherRanks.size()>0){
    if (Msg::GetCommRank() == _rootRank){
      // get all IP number
      std::vector<int> allRanks(_otherRanks.begin(),_otherRanks.end());
      if (_distributedOnRoot)
        allRanks.push_back(_rootRank);
      std::vector<int> allIPBulk, allIPMinus,allIPPlus;
      IntPt* GP;
      for (groupOfElements::elementContainer::iterator it = this->g->begin(); it != this->g->end(); ++it){
        MElement *ele = *it;
        int npts_bulk=this->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
        for(int i=0;i<npts_bulk;i++){
          int num = numericalMaterial::createTypeWithTwoInts(ele->getNum(),i);
          allIPBulk.push_back(num);
        }
      }
      numericalMaterial::distributeMacroIP(allIPBulk,allRanks,_mapIPBulk);
      allIPBulk.clear();

      for(groupOfElements::elementContainer::const_iterator it=this->gi->begin(); it!=this->gi->end();++it){
        MElement *ele = *it;
        MInterfaceElement* iele = dynamic_cast<MInterfaceElement*>(ele);
      // 2* because IP is duplicated for DG formulation
        int npts_inter=2*this->getInterfaceGaussIntegrationRule()->getIntPoints(ele,&GP);
        for(int i=0;i<npts_inter/2;i++){
          int num = numericalMaterial::createTypeWithTwoInts(ele->getNum(),i);
          allIPMinus.push_back(num);
        }
        for(int i=npts_inter/2;i<npts_inter;i++){
          int num = numericalMaterial::createTypeWithTwoInts(ele->getNum(),i);
          allIPPlus.push_back(num);
        }
      }
      numericalMaterial::distributeMacroIP(allIPMinus,allRanks,_mapIPInterfaceMinus);
      allIPMinus.clear();
      numericalMaterial::distributeMacroIP(allIPPlus,allRanks,_mapIPInterfacePlus);
      allIPPlus.clear();

      // Send data for bulk elements
      for (std::map<int,std::set<int> >::iterator it = _mapIPBulk.begin(); it!= _mapIPBulk.end(); it++){
        int rank = it->first;
        std::set<int>& numElOnRank = _mapIPBulk[rank];
        // at root rank
        if (rank == _rootRank){
          _domainIPBulk = numElOnRank;
        }
        // at other ranks
        else{
          int bufferSize = numElOnRank.size();
          MPI_Send(&bufferSize,1,MPI_INT,rank,numericalMaterial::createTypeWithTwoInts(getPhysical(),0),MPI_COMM_WORLD);
          if (bufferSize>0){
            int* buffer = new int[bufferSize];
            int idex = 0;
            for (std::set<int>::iterator its = numElOnRank.begin(); its != numElOnRank.end(); its++){
              buffer[idex] = *its;
              idex++;
            };
            MPI_Send(buffer,bufferSize,MPI_INT,rank,numericalMaterial::createTypeWithTwoInts(getPhysical(),1),MPI_COMM_WORLD);
            delete[] buffer;
          }
        }
      };

      // send data for minus interface IP
      for (std::map<int,std::set<int> >::iterator it = _mapIPInterfaceMinus.begin();
                                      it!= _mapIPInterfaceMinus.end(); it++){
        int rank = it->first;
        std::set<int>& numElOnRank = _mapIPInterfaceMinus[rank];
        // at root rank
        if (rank == _rootRank){
          _domainIPInterfaceMinus = numElOnRank;
        }
        // at other ranks
        else{
          int bufferSize = numElOnRank.size();
          MPI_Send(&bufferSize,1,MPI_INT,rank,numericalMaterial::createTypeWithTwoInts(getPhysical(),2),MPI_COMM_WORLD);
          if (bufferSize>0){
            int* buffer = new int[bufferSize];
            int idex = 0;
            for (std::set<int>::iterator its = numElOnRank.begin(); its != numElOnRank.end(); its++){
              buffer[idex] = *its;
              idex++;
            };
            MPI_Send(buffer,bufferSize,MPI_INT,rank,numericalMaterial::createTypeWithTwoInts(getPhysical(),3),MPI_COMM_WORLD);
            delete[] buffer;
          }
        }
      };

       // send data for plus interface IP
      for (std::map<int,std::set<int> >::iterator it = _mapIPInterfacePlus.begin();
                                      it!= _mapIPInterfacePlus.end(); it++){
        int rank = it->first;
        std::set<int>& numElOnRank = _mapIPInterfacePlus[rank];
        // at root rank
        if (rank == _rootRank){
          _domainIPInterfacePlus = numElOnRank;
        }
        // at other ranks
        else{
          int bufferSize = numElOnRank.size();
          MPI_Send(&bufferSize,1,MPI_INT,rank,numericalMaterial::createTypeWithTwoInts(getPhysical(),4),MPI_COMM_WORLD);
          if (bufferSize>0){
            int* buffer = new int[bufferSize];
            int idex = 0;
            for (std::set<int>::iterator its = numElOnRank.begin(); its != numElOnRank.end(); its++){
              buffer[idex] = *its;
              idex++;
            };
            MPI_Send(buffer,bufferSize,MPI_INT,rank,numericalMaterial::createTypeWithTwoInts(getPhysical(),5),MPI_COMM_WORLD);
            delete[] buffer;
          }
        }
      };

      // create IP State
      const materialLaw* mlaw = this->getMaterialLaw();
      for (groupOfElements::elementContainer::const_iterator it = this->g->begin(); it != this->g->end(); ++it){
        MElement *ele = *it;
        int npts_bulk=this->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
        AllIPState::ipstateElementContainer tp(npts_bulk);
        for(int i=0;i<npts_bulk;i++){
          int num = numericalMaterial::createTypeWithTwoInts(ele->getNum(),i);
          if (_domainIPBulk.find(num) != _domainIPBulk.end())
            mlaw->createIPState(tp[i],state,ele,ele->getNumVertices(),i);
          else{
            const numericalMaterial* nummat = dynamic_cast<const numericalMaterial*>(mlaw);
            if (nummat)
              nummat->createIPState(false,tp[i],state,ele,ele->getNumVertices(),i);
            else
              Msg::Fatal("Fatal error at creating the bulk IPState");
          }
        }
        map.insert(AllIPState::ipstatePairType(ele->getNum(),tp));
      }
      const materialLaw *mlawMinus = this->getMaterialLawMinus();
      const materialLaw *mlawPlus = this->getMaterialLawPlus();
      // loop
      for(groupOfElements::elementContainer::const_iterator it=this->gi->begin(); it!=this->gi->end();++it){
        MElement *ele = *it;
        MInterfaceElement* iele = dynamic_cast<MInterfaceElement*>(ele);
      // 2* because IP is duplicated for DG formulation
        int npts_inter=2*this->getInterfaceGaussIntegrationRule()->getIntPoints(ele,&GP);
        AllIPState::ipstateElementContainer tp(npts_inter);
        for(int i=0;i<npts_inter/2;i++){
          int num = numericalMaterial::createTypeWithTwoInts(ele->getNum(),i);
          if (_domainIPInterfaceMinus.find(num) != _domainIPInterfaceMinus.end())
            mlawMinus->createIPState(tp[i],state,ele,iele->getElem(0)->getNumVertices(),i);
          else{
            const numericalMaterial* numatMinus = dynamic_cast<const numericalMaterial*>(mlawMinus);
            if (numatMinus)
              numatMinus->createIPState(false,tp[i],state,ele,ele->getNumVertices(),i);
            else
              Msg::Fatal("Fatal error at creating the negative IPState");
          }
        }
        for(int i=npts_inter/2;i<npts_inter;i++){
          int num = numericalMaterial::createTypeWithTwoInts(ele->getNum(),i);
          if (_domainIPInterfacePlus.find(num) != _domainIPInterfacePlus.end())
            mlawPlus->createIPState(tp[i],state,ele,iele->getElem(1)->getNumVertices(),i);
          else{
            const numericalMaterial* numatPlus = dynamic_cast<const numericalMaterial*>(mlawPlus);
            if (numatPlus)
              numatPlus->createIPState(false,tp[i],state,ele,ele->getNumVertices(),i);
            else
              Msg::Fatal("Fatal error at creating the positive IPState");
          }
        }
        map.insert(AllIPState::ipstatePairType(iele->getNum(),tp));
      }
    }
    else if (_otherRanks.find(Msg::GetCommRank()) != _otherRanks.end()) {
      // receive data from root for bulk elements
      MPI_Status status;
      int bufferSize;
      MPI_Recv(&bufferSize,1,MPI_INT,_rootRank,numericalMaterial::createTypeWithTwoInts(getPhysical(),0),MPI_COMM_WORLD,&status);
      if (bufferSize>0){
        int* buffer = new int[bufferSize];
        MPI_Recv(buffer,bufferSize,MPI_INT,_rootRank,numericalMaterial::createTypeWithTwoInts(getPhysical(),1),MPI_COMM_WORLD,&status);
        _domainIPBulk.clear();
        for (int i=0; i<bufferSize; i++)
          _domainIPBulk.insert(buffer[i]);
        delete[] buffer;
      }

      MPI_Recv(&bufferSize,1,MPI_INT,_rootRank,numericalMaterial::createTypeWithTwoInts(getPhysical(),2),MPI_COMM_WORLD,&status);
      if (bufferSize>0){
        int* buffer = new int[bufferSize];
        MPI_Recv(buffer,bufferSize,MPI_INT,_rootRank,numericalMaterial::createTypeWithTwoInts(getPhysical(),3),MPI_COMM_WORLD,&status);
        _domainIPInterfaceMinus.clear();
        for (int i=0; i<bufferSize; i++)
          _domainIPInterfaceMinus.insert(buffer[i]);
        delete[] buffer;
      }

      MPI_Recv(&bufferSize,1,MPI_INT,_rootRank,numericalMaterial::createTypeWithTwoInts(getPhysical(),4),MPI_COMM_WORLD,&status);
      if (bufferSize>0){
        int* buffer = new int[bufferSize];
        MPI_Recv(buffer,bufferSize,MPI_INT,_rootRank,numericalMaterial::createTypeWithTwoInts(getPhysical(),5),MPI_COMM_WORLD,&status);
        _domainIPInterfacePlus.clear();
        for (int i=0; i<bufferSize; i++)
          _domainIPInterfacePlus.insert(buffer[i]);
        delete[] buffer;
      }
      //
      const materialLaw *mlaw = this->getMaterialLaw();
      for (std::set<int>::iterator it = _domainIPBulk.begin(); it != _domainIPBulk.end(); it++){
        int num = *it;
        int elnum, gnum;
        numericalMaterial::getTwoIntsFromType(num,elnum,gnum);
        MElement* ele = new MPoint(NULL,elnum,0);
        AllIPState::ipstateElementContainer tp(1);
        mlaw->createIPState(tp[0],state,ele,ele->getNumVertices(),gnum);
        delete ele;
        map.insert(AllIPState::ipstatePairType(num,tp));
      };

      const materialLaw *mlawMinus = this->getMaterialLawMinus();
      for (std::set<int>::iterator it = _domainIPInterfaceMinus.begin(); it != _domainIPInterfaceMinus.end(); it++){
        int num = *it;
        int elnum, gnum;
        numericalMaterial::getTwoIntsFromType(num,elnum,gnum);
        MElement* ele = new MPoint(NULL,elnum,0);
        AllIPState::ipstateElementContainer tp(1);
        mlawMinus->createIPState(tp[0],state,ele,ele->getNumVertices(),gnum);
        delete ele;
        map.insert(AllIPState::ipstatePairType(num,tp));
      };

      const materialLaw *mlawPlus = this->getMaterialLawPlus();
      for (std::set<int>::iterator it = _domainIPInterfacePlus.begin(); it != _domainIPInterfacePlus.end(); it++){
        int num = *it;
        int elnum, gnum;
        numericalMaterial::getTwoIntsFromType(num,elnum,gnum);
        MElement* ele = new MPoint(NULL,elnum,0);
        AllIPState::ipstateElementContainer tp(1);
        mlawPlus->createIPState(tp[0],state,ele,ele->getNumVertices(),gnum);
        delete ele;
        map.insert(AllIPState::ipstatePairType(num,tp));
      };
    }
  }
  else
  #endif //HAVE_MPI
  {
    dgPartDomain::createIPState(map,state);
  }
};

partDomain* dG3DDomain::clone() const{
  return new dG3DDomain(*this);
};

#if defined(HAVE_MPI)
void dG3DDomain::computeIPVariableMPI(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, bool stiff){
  IntPt *GP;
  MPI_Status status;
  if (Msg::GetCommRank() == _rootRank){
    // compute all IP strain on root rank
    this->computeAllIPStrain(aips,ufield,ws,false);
    // send strain to other procs
    // for bulk elements
    for (std::set<int>::iterator it = _otherRanks.begin(); it!= _otherRanks.end(); it++){
      int otherRank = *it;
      std::set<int>& IPBulkonRank = _mapIPBulk[otherRank];
      for (std::set<int>::iterator its = IPBulkonRank.begin(); its!= IPBulkonRank.end(); its++){
        int num = *its;
        int elnum,gnum;
        numericalMaterial::getTwoIntsFromType(num,elnum,gnum);
        AllIPState::ipstateElementContainer *vips = aips->getIPstate(elnum);
        IPStateBase* ips = (*vips)[gnum];
        ipFiniteStrain* ipv =dynamic_cast<ipFiniteStrain*>(ips->getState(ws));
        int bufferSize = ipv->getMacroNumberElementDataSendToMicroProblem(); // already known
        double *buffer = new double[bufferSize];
        ipv->getMacroDataSendToMicroProblem(buffer);
        MPI_Send(buffer,bufferSize,MPI_DOUBLE,otherRank,num,MPI_COMM_WORLD);
        delete[] buffer;
      };

      std::set<int>& IPInterfaceMinusonRank = _mapIPInterfaceMinus[otherRank];
      for (std::set<int>::iterator its = IPInterfaceMinusonRank.begin(); its!= IPInterfaceMinusonRank.end(); its++){
        int num = *its;
        int elnum,gnum;
        numericalMaterial::getTwoIntsFromType(num,elnum,gnum);
        AllIPState::ipstateElementContainer *vips = aips->getIPstate(elnum);
        IPStateBase* ips = (*vips)[gnum];
        ipFiniteStrain* ipv =dynamic_cast<ipFiniteStrain*>(ips->getState(ws));
        int bufferSize = ipv->getMacroNumberElementDataSendToMicroProblem(); // already known
        double *buffer = new double[bufferSize];
        ipv->getMacroDataSendToMicroProblem(buffer);
        MPI_Send(buffer,bufferSize,MPI_DOUBLE,otherRank,num,MPI_COMM_WORLD);
        delete[] buffer;
      };

      std::set<int>& IPInterfacePlusonRank = _mapIPInterfacePlus[otherRank];
      for (std::set<int>::iterator its = IPInterfacePlusonRank.begin(); its!= IPInterfacePlusonRank.end(); its++){
        int num = *its;
        int elnum,gnum;
        numericalMaterial::getTwoIntsFromType(num,elnum,gnum);
        AllIPState::ipstateElementContainer *vips = aips->getIPstate(elnum);
        IPStateBase* ips = (*vips)[gnum];
        ipFiniteStrain* ipv =dynamic_cast<ipFiniteStrain*>(ips->getState(ws));
        int bufferSize = ipv->getMacroNumberElementDataSendToMicroProblem(); // already known
        double *buffer = new double[bufferSize];
        ipv->getMacroDataSendToMicroProblem(buffer);
        MPI_Send(buffer,bufferSize,MPI_DOUBLE,otherRank,num,MPI_COMM_WORLD);
        delete[] buffer;
      };
    }
  }
  else if (_otherRanks.find(Msg::GetCommRank()) != _otherRanks.end()){
    for (std::set<int>::iterator it = _domainIPBulk.begin(); it != _domainIPBulk.end(); it++){
      int num = *it;
      int elnum,gnum;
      numericalMaterial::getTwoIntsFromType(num,elnum,gnum);
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(num);
      IPStateBase* ips = (*vips)[0];
      ipFiniteStrain* ipv = dynamic_cast<ipFiniteStrain*>( ips->getState(ws));
      int bufferSize = ipv->getMacroNumberElementDataSendToMicroProblem();
      double *buffer = new double[bufferSize];
      MPI_Recv(buffer,bufferSize,MPI_DOUBLE,_rootRank,num,MPI_COMM_WORLD,&status);
      ipv->setReceivedMacroDataToMicroProblem(buffer);
      delete [] buffer;
    };

    for (std::set<int>::iterator it = _domainIPInterfaceMinus.begin(); it != _domainIPInterfaceMinus.end(); it++){
      int num = *it;
      int elnum, phys, gnum;
      numericalMaterial::getTwoIntsFromType(num,elnum,gnum);
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(num);
      IPStateBase* ips = (*vips)[0];
      ipFiniteStrain* ipv = dynamic_cast<ipFiniteStrain*>( ips->getState(ws));
      int bufferSize = ipv->getMacroNumberElementDataSendToMicroProblem();
      double *buffer = new double[bufferSize];
      MPI_Recv(buffer,bufferSize,MPI_DOUBLE,_rootRank,num,MPI_COMM_WORLD,&status);
      ipv->setReceivedMacroDataToMicroProblem(buffer);
      delete [] buffer;
    };

    for (std::set<int>::iterator it = _domainIPInterfacePlus.begin(); it != _domainIPInterfacePlus.end(); it++){
      int num = *it;
      int elnum,gnum;
      numericalMaterial::getTwoIntsFromType(num,elnum,gnum);
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(num);
      IPStateBase* ips = (*vips)[0];
      ipFiniteStrain* ipv = dynamic_cast<ipFiniteStrain*>( ips->getState(ws));
      int bufferSize = ipv->getMacroNumberElementDataSendToMicroProblem();
      double *buffer = new double[bufferSize];
      MPI_Recv(buffer,bufferSize,MPI_DOUBLE,_rootRank,num,MPI_COMM_WORLD,&status);
      ipv->setReceivedMacroDataToMicroProblem(buffer);
      delete [] buffer;
    };
  };

  for (std::set<int>::iterator it = _domainIPBulk.begin(); it != _domainIPBulk.end(); it++){
    int num = *it;
    int elnum, gnum;
    numericalMaterial::getTwoIntsFromType(num,elnum,gnum);
    IPStateBase* ips = NULL;
    if (Msg::GetCommRank() == _rootRank){
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(elnum);
      ips = (*vips)[gnum];
    }
    else if (_otherRanks.find(Msg::GetCommRank()) != _otherRanks.end()){
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(num);
      ips = (*vips)[0];
    }
    IPVariable *ipv = ips->getState(ws);
    IPVariable *ipvprev = ips->getState(IPStateBase::previous);
    dG3DMaterialLaw *mlaw = dynamic_cast<dG3DMaterialLaw*>(this->getMaterialLaw());
    mlaw->stress(ipv,ipvprev,stiff,false);
  };

  for (std::set<int>::iterator it = _domainIPInterfaceMinus.begin(); it != _domainIPInterfaceMinus.end(); it++){
    int num = *it;
    int elnum, gnum;
    numericalMaterial::getTwoIntsFromType(num,elnum,gnum);
    IPStateBase* ips = NULL;
    if (Msg::GetCommRank() == _rootRank){
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(elnum);
      ips = (*vips)[gnum];
    }
    else if (_otherRanks.find(Msg::GetCommRank()) != _otherRanks.end()){
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(num);
      ips = (*vips)[0];
    }
    IPVariable *ipv = ips->getState(ws);
    IPVariable *ipvprev = ips->getState(IPStateBase::previous);
    dG3DMaterialLaw *mlawMinus = dynamic_cast<dG3DMaterialLaw*>(this->getMaterialLawMinus());
    mlawMinus->stress(ipv,ipvprev,stiff,false);
  };

  for (std::set<int>::iterator it = _domainIPInterfacePlus.begin(); it != _domainIPInterfacePlus.end(); it++){
    int num = *it;
    int elnum,gnum;
    numericalMaterial::getTwoIntsFromType(num,elnum,gnum);
    IPStateBase* ips = NULL;
    if (Msg::GetCommRank() == _rootRank){
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(elnum);
      ips = (*vips)[gnum];
    }
    else if (_otherRanks.find(Msg::GetCommRank()) != _otherRanks.end()){
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(num);
      ips = (*vips)[0];
    }
    IPVariable *ipv = ips->getState(ws);
    IPVariable *ipvprev = ips->getState(IPStateBase::previous);
    dG3DMaterialLaw *mlawPlus= dynamic_cast<dG3DMaterialLaw*>(this->getMaterialLawPlus());
    mlawPlus->stress(ipv,ipvprev,stiff,false);
  };

  if (_otherRanks.find(Msg::GetCommRank()) != _otherRanks.end()){
    for (std::set<int>::iterator it = _domainIPBulk.begin(); it != _domainIPBulk.end(); it++){
      int num = *it;
      int elnum,gnum;
      numericalMaterial::getTwoIntsFromType(num,elnum,gnum);
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(num);
      IPStateBase* ips = (*vips)[0];
      ipFiniteStrain* ipv = dynamic_cast<ipFiniteStrain*>(ips->getState(ws));
      int bufferSize = ipv->getMicroNumberElementDataSendToMacroProblem();
      double *buffer = new double[bufferSize];
      ipv->getMicroDataToMacroProblem(buffer);
      MPI_Send(buffer,bufferSize,MPI_DOUBLE,_rootRank,num,MPI_COMM_WORLD);
      delete [] buffer;
    };
    for (std::set<int>::iterator it = _domainIPInterfaceMinus.begin(); it != _domainIPInterfaceMinus.end(); it++){
      int num = *it;
      int elnum,gnum;
      numericalMaterial::getTwoIntsFromType(num,elnum,gnum);
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(num);
      IPStateBase* ips = (*vips)[0];
      ipFiniteStrain* ipv = dynamic_cast<ipFiniteStrain*>(ips->getState(ws));
      int bufferSize = ipv->getMicroNumberElementDataSendToMacroProblem();
      double *buffer = new double[bufferSize];
      ipv->getMicroDataToMacroProblem(buffer);
      MPI_Send(buffer,bufferSize,MPI_DOUBLE,_rootRank,num,MPI_COMM_WORLD);
      delete [] buffer;
    };
    for (std::set<int>::iterator it = _domainIPInterfacePlus.begin(); it != _domainIPInterfacePlus.end(); it++){
      int num = *it;
      int elnum,gnum;
      numericalMaterial::getTwoIntsFromType(num,elnum,gnum);
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(num);
      IPStateBase* ips = (*vips)[0];
      ipFiniteStrain* ipv = dynamic_cast<ipFiniteStrain*>(ips->getState(ws));
      int bufferSize = ipv->getMicroNumberElementDataSendToMacroProblem();
      double *buffer = new double[bufferSize];
      ipv->getMicroDataToMacroProblem(buffer);
      MPI_Send(buffer,bufferSize,MPI_DOUBLE,_rootRank,num,MPI_COMM_WORLD);
      delete [] buffer;
    };
  }
  else if (Msg::GetCommRank() == _rootRank){
    for (std::set<int>::iterator it = _otherRanks.begin(); it!= _otherRanks.end(); it++){
      int otherRank = *it;
      std::set<int>& IPBulkonRank = _mapIPBulk[otherRank];
      for (std::set<int>::iterator its = IPBulkonRank.begin(); its!= IPBulkonRank.end(); its++){
        int num = *its;
        int elnum,gnum;
        numericalMaterial::getTwoIntsFromType(num,elnum,gnum);
        AllIPState::ipstateElementContainer *vips = aips->getIPstate(elnum);
        IPStateBase* ips = (*vips)[gnum];
        ipFiniteStrain* ipv = dynamic_cast<ipFiniteStrain*>(ips->getState(ws));
        int bufferSize = ipv->getMicroNumberElementDataSendToMacroProblem();
        double *buffer = new double[bufferSize];
        MPI_Recv(buffer,bufferSize,MPI_DOUBLE,otherRank,num,MPI_COMM_WORLD,&status);
        ipv->setReceivedMicroDataToMacroProblem(buffer);
        dG3DMaterialLaw* mlaw = dynamic_cast<dG3DMaterialLaw*>(this->getMaterialLaw());
        mlaw->setElasticStiffness(ipv);
        delete[] buffer;
      };
      std::set<int>& IPInterfaceMinusonRank = _mapIPInterfaceMinus[otherRank];
      for (std::set<int>::iterator its = IPInterfaceMinusonRank.begin(); its!= IPInterfaceMinusonRank.end(); its++){
        int num = *its;
        int elnum,gnum;
        numericalMaterial::getTwoIntsFromType(num,elnum,gnum);
        AllIPState::ipstateElementContainer *vips = aips->getIPstate(elnum);
        IPStateBase* ips = (*vips)[gnum];
        ipFiniteStrain* ipv = dynamic_cast<ipFiniteStrain*>(ips->getState(ws));
        int bufferSize = ipv->getMicroNumberElementDataSendToMacroProblem();
        double *buffer = new double[bufferSize];
        MPI_Recv(buffer,bufferSize,MPI_DOUBLE,otherRank,num,MPI_COMM_WORLD,&status);
        ipv->setReceivedMicroDataToMacroProblem(buffer);
        dG3DMaterialLaw* mlawMinus = dynamic_cast<dG3DMaterialLaw*>(this->getMaterialLawMinus());
        mlawMinus->setElasticStiffness(ipv);
        delete[] buffer;
      };

      std::set<int>& IPInterfacePlusonRank = _mapIPInterfacePlus[otherRank];
      for (std::set<int>::iterator its = IPInterfacePlusonRank.begin(); its!= IPInterfacePlusonRank.end(); its++){
        int num = *its;
        int elnum,gnum;
        numericalMaterial::getTwoIntsFromType(num,elnum,gnum);
        AllIPState::ipstateElementContainer *vips = aips->getIPstate(elnum);
        IPStateBase* ips = (*vips)[gnum];
        ipFiniteStrain* ipv = dynamic_cast<ipFiniteStrain*>(ips->getState(ws));
        int bufferSize = ipv->getMicroNumberElementDataSendToMacroProblem();
        double *buffer = new double[bufferSize];
        MPI_Recv(buffer,bufferSize,MPI_DOUBLE,otherRank,num,MPI_COMM_WORLD,&status);
        ipv->setReceivedMicroDataToMacroProblem(buffer);
        dG3DMaterialLaw* mlawPlus = dynamic_cast<dG3DMaterialLaw*>(this->getMaterialLawPlus());
        mlawPlus->setElasticStiffness(ipv);
        delete[] buffer;
      };
    }
  };
};
#endif

interDomainBase::interDomainBase(partDomain *dom1,partDomain *dom2) :  _mlawMinus(NULL), _mlawPlus(NULL)
{
  // By convention the domain with the small number is the domMinus
  // For MPI in domMinus->getPhysical() = domPlus->getPhysical() --> domMinus = domain on the partion with the lowest rank

  // look for element (INTERDOMAIN HAVE TO BE CREATED AFTER addDomain to solver)
  if(dom2->getPhysical() > dom1->getPhysical())
  {
    _domMinus = dom1;
    _domPlus = dom2;
  }
  else if(dom1->getPhysical() > dom2->getPhysical())
  {
    _domMinus = dom2;
    _domPlus = dom1;
  }
#if defined(HAVE_MPI)
  else if((Msg::GetCommSize() > 1) and (dom1->getPhysical() == dom2->getPhysical()))
  {
    _domMinus = dom1; // same --> no matter
    _domPlus = dom2;
  }
  else if ( (Msg::GetCommSize() > 1) and ((dom1->g->size() !=0) and (dom2->g->size() !=0))){
    // Check if the two domains are added to solver
    groupOfElements *g1 = dom1->g;
    groupOfElements *g2 = dom2->g;

    groupOfElements::elementContainer::iterator it1 = g1->begin();
    MElement *ele1 = *it1;
    groupOfElements::elementContainer::iterator it2 = g2->begin();
    MElement *ele2 = *it2;
    if(ele1->getPartition() < ele2->getPartition())
    {
      _domMinus = dom1;
      _domPlus = dom2;
    }
    else if(ele2->getPartition() < ele1->getPartition())
    {
      _domMinus = dom2;
      _domPlus = dom1;
    }
  }
 #endif // HAVE_MPI
  else  // same partition choose in function of physical number
  {
    Msg::Error("Impossible to known the partion of domain --> no inter domain created between %d and %d",dom1->getPhysical(),dom2->getPhysical());
  }

}

interDomainBase::interDomainBase(const interDomainBase &source) : _domMinus(source._domMinus),
                                                                  _domPlus(source._domPlus),
                                                                  _mlawMinus(source._mlawMinus),
                                                                  _mlawPlus(source._mlawPlus)
{

}

void interDomainBase::initializeFractureBase(unknownField *uf,IPField*ip, std::vector<nonLinearNeumannBC> &vNeumann,
                                         groupOfElements *gi, QuadratureBase* integBound)
{

  // same strength fracture value on both side (to avoid discontinuities)
  // no very elegant to put here but very efficient
  // take the value of minus element by choice
     if(_mlawMinus->getType() == materialLaw::fracture and _mlawPlus->getType() == materialLaw::fracture)
     {
       // loop on ipvariable
       IntPt *GP;
       for(groupOfElements::elementContainer::iterator it=gi->begin(); it!=gi->end(); ++it)
       {
         MElement *ele = *it;
         int npts_inter=integBound->getIntPoints(ele,&GP);
         AllIPState::ipstateElementContainer *vips = ip->getAips()->getIPstate(ele->getNum());
         for(int j=0;j<npts_inter;j++){ // npts_inter points on both sides
           IPStateBase* ipsm = (*vips)[j];
           IPStateBase* ipsp = (*vips)[j+npts_inter];
           FractureCohesive3DIPVariable *ipvm = static_cast<FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::initial));
           FractureCohesive3DIPVariable *ipvp = static_cast<FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::initial));
           ipvp->setFractureStrengthFactor(ipvm->fractureStrengthFactor());
           ipvm = static_cast<FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::previous));
           ipvp = static_cast<FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::previous));
           ipvp->setFractureStrengthFactor(ipvm->fractureStrengthFactor());
           ipvm = static_cast<FractureCohesive3DIPVariable*>(ipsm->getState(IPStateBase::current));
           ipvp = static_cast<FractureCohesive3DIPVariable*>(ipsp->getState(IPStateBase::current));
           ipvp->setFractureStrengthFactor(ipvm->fractureStrengthFactor());
         }
       }
     }
}

void interDomainBase::setGaussIntegrationRuleBase(groupOfElements *gi, QuadratureBase** integBound, QuadratureBase** integBulk,
                                                  interfaceQuadratureBase** interQuad, int _gaussorderbound, dG3DDomain::GaussType _gqt)
{
 #if defined(HAVE_MPI)
   // InterDomain defined by user can be empty on a partition !!
  if(gi->size() == 0)
  {
    *integBound = new QuadratureVoid();
    *interQuad = new interface2DQuadrature(0);
  }
  else
 #endif //HAVE_MPI
  {
    groupOfElements::elementContainer::const_iterator it = gi->begin();
    MElement *e = *it;
    if(_gaussorderbound == -1)
    {
      if(e->getTypeForMSH() == MSH_TET_10 or e->getTypeForMSH() == MSH_TRI_6)
      {
        if( _gqt == dG3DDomain::Gauss)
              *integBound = new GaussQuadrature(4);
        else
              *integBound = new GaussLobatto2DQuadrature(2);
        *interQuad = new interface2DQuadrature(4);  // same number of Gauss point Gauss & Lobato = 2 ??
      }
      else if(e->getTypeForMSH() == MSH_HEX_27 or e->getTypeForMSH() == MSH_QUA_9)
      {
        if( _gqt == dG3DDomain::Gauss)
              *integBound = new GaussQuadrature(4);
        else
              *integBound = new GaussLobatto2DQuadrature(3);
        *interQuad = new interface2DQuadrature(4);  // same number of Gauss point Gauss & Lobato = 2 ??
      }
      else if(e->getTypeForMSH() == MSH_HEX_8 or e->getTypeForMSH() == MSH_QUA_4)
      {
        if( _gqt == dG3DDomain::Gauss)
              *integBound = new GaussQuadrature(3);
        else
              *integBound = new GaussLobatto2DQuadrature(2);
        *interQuad = new interface2DQuadrature(3);  // same number of Gauss point Gauss & Lobato = 2 ??
      }
      else if (e->getTypeForMSH() == MSH_LIN_2){
        *integBound = new GaussQuadrature(5);
        *interQuad = new interface1DQuadrature(5);
      }
      else if (e->getTypeForMSH() == MSH_LIN_3){
        *integBound = new GaussQuadrature(5);
        *interQuad = new interface1DQuadrature(5);
      }
    }
    else{
      if (_domMinus->getDim()==3){
        _gqt == dG3DDomain::Gauss ? *integBound = new GaussQuadrature(_gaussorderbound) : *integBound = new GaussLobatto2DQuadrature(_gaussorderbound);
        *interQuad = new interface2DQuadrature(_gaussorderbound);
      }
      else if (_domMinus->getDim()==2){
        *integBound = new GaussQuadrature(_gaussorderbound);
        *interQuad = new interface1DQuadrature(_gaussorderbound);
      }

    }
   }
}


interDomainBetween3D::interDomainBetween3D(const int tag, partDomain *dom1,partDomain *dom2,const int lnum) :
                                  interDomainBase(dom1,dom2),
                                  dG3DDomain(tag,manageInterface::getKey(dom1->getPhysical(),
                                                                    dom2->getPhysical()),10000,lnum,true,dom1->getDim())
{

  // create empty groupOfElements
  g = new groupOfElements();
  // set functionalSpace
  _spaceMinus = interDomainBase::_domMinus->getFunctionSpace();
  _spacePlus = interDomainBase::_domPlus->getFunctionSpace();
  if((_domMinus->getFunctionSpaceType() == functionSpaceType::Lagrange) and
     (_domPlus->getFunctionSpaceType() == functionSpaceType::Lagrange)){
    if(_space != NULL) delete _space;
    _space = new dG3DLagrangeBetween2DomainsFunctionSpace(3,_spaceMinus,_spacePlus);
    _wsp = functionSpaceType::Lagrange;
  }
  else{
    Msg::Error("Impossible to create functional space on interDomainBetweenShell %d",_phys);
  }
}


interDomainBetween3D::interDomainBetween3D(const interDomainBetween3D &source) : interDomainBase(source), dG3DDomain(source)
{}

void interDomainBetween3D::createTerms(unknownField *uf,IPField*ip, std::vector<nonLinearNeumannBC> &vNeumann)
{
  // void term (as they have to exist)
  this->ltermBulk = new nonLinearTermVoid();
  this->btermBulk = new BiNonLinearTermVoid();

  this->btermVirtBound = new BiNonLinearTermVoid();
  this->ltermVirtBound = new nonLinearTermVoid();
  this->massterm = new BiNonLinearTermVoid();

  FunctionSpace<double>* dgspace = static_cast<FunctionSpace<double>*>(_space);
  FunctionSpace<double>* dgspace1 = static_cast<FunctionSpace<double>*>(_spaceMinus);
  FunctionSpace<double>* dgspace2 = static_cast<FunctionSpace<double>*>(_spacePlus);
  dG3DDomain* dgldomMinus = static_cast<dG3DDomain*>(_domMinus);
  dG3DDomain* dgldomPlus = static_cast<dG3DDomain*>(_domPlus);
  ltermBound = new dG3DForceInter(*dgspace1,dgspace2,_mlawMinus,_mlawPlus,_interQuad,_beta1,true,ip);
  if(_interByPert)
    btermBound = new BilinearTermPerturbation<double>(ltermBound,*dgspace1,*dgspace2,uf,ip,this,_eps);
  else
    btermBound = new dG3DStiffnessInter(*dgspace1,*dgspace2,_mlawMinus,_mlawPlus,_interQuad,_beta1,ip,uf,true);
}
void interDomainBetween3D::initializeTerms(unknownField *uf,IPField*ip, std::vector<nonLinearNeumannBC> &vNeumann)
{

  createTerms(uf,ip, vNeumann);
  initializeFractureBase(uf,ip, vNeumann,gi, integBound);
}

void interDomainBetween3D::setGaussIntegrationRule()
{
  if (gi->size()>0)
    setGaussIntegrationRuleBase(gi, &integBound, &integBulk, &_interQuad, _gaussorderbound, _gqt);
  else{
    integBound = new QuadratureVoid();
    integBulk = NULL;
    _interQuad = new interfaceQuadratureVoid();
  }
}

void interDomainBetween3D::computeAllIPStrain(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, const bool virt){
  IntPt *GP;
  fullVector<double> dispm;
  fullVector<double> dispp;
  std::vector<Dof> R;

  for(groupOfElements::elementContainer::iterator it=gi->begin(); it!=gi->end(); ++it){
    MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(*it);
    R.clear();
    _spaceMinus->getKeys(iele->getElem(0),R);
    dispm.resize(R.size());
    ufield->get(R,dispm);
    R.clear();
    _spacePlus->getKeys(iele->getElem(1),R);
    dispp.resize(R.size());
    ufield->get(R,dispp);
    this->computeStrain(aips,iele,GP,ws,_domMinus,_domPlus,dispm,dispp,virt);
  }
};

void interDomainBetween3D::computeIPVariable(AllIPState *aips,const unknownField *ufield,
                                                const IPStateBase::whichState ws, bool stiff)
{
  #if defined(HAVE_MPI)
  if (_mlawMinus->getType()==materialLaw::numeric and this->_otherRanks.size()>0){
    this->computeIPVariableMPI(aips,ufield,ws,stiff);
  }
  else
  #endif //HAVE_MPI
  {
    this->computeAllIPStrain(aips,ufield,ws,false);
    IntPt *GP;
    fullVector<double> dispm;
    fullVector<double> dispp;
    std::vector<Dof> R;
    dG3DMaterialLaw *mlawMinus = static_cast<dG3DMaterialLaw*>(_mlawMinus);
    dG3DMaterialLaw *mlawPlus = static_cast<dG3DMaterialLaw*>(_mlawPlus);
    for(groupOfElements::elementContainer::iterator it=gi->begin(); it!=gi->end(); ++it){
      MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(*it);
      int npts_inter= integBound->getIntPoints(*it,&GP);
      R.clear();
      _spaceMinus->getKeys(iele->getElem(0),R);
      dispm.resize(R.size());
      ufield->get(R,dispm);
      R.clear();
      _spacePlus->getKeys(iele->getElem(1),R);
      dispp.resize(R.size());
      ufield->get(R,dispp);
      this->computeIpv(aips,iele,GP,ws,_domMinus,_domPlus,mlawMinus,mlawPlus,dispm,dispp,false,stiff);
    }
  }
}

void interDomainBetween3D::setMaterialLaw(const std::map<int,materialLaw*> maplaw)
{
  // oterwise the law is already set
  if(!setmaterial){
    _domMinus->setMaterialLaw(maplaw);
    _domPlus->setMaterialLaw(maplaw);
    _mlaw = NULL;
    if(_lnum == 0){ // no fracture law prescribed --> materialLawMinus and Plus are given via domain
      _mlawMinus = _domMinus->getMaterialLaw();
      _mlawPlus = _domPlus->getMaterialLaw();
      if(_domMinus->getPhysical() == _domPlus->getPhysical())
      {
         _mlaw = _mlawMinus; // same law on both
      }
      else if(_mlawMinus->getNum() == _mlawPlus->getNum())
      {
        _mlaw = _mlawMinus; // same law on both
      }
      else
      {
        Msg::Warning("Cannot set the interface law on interDomain3D");
      }
    }
    else{
      // create law (use lnum with negative number same number for the two but it doesn't matter)
      // these law don't appear in map with all laws
      // check if minus and plus laws are already fracture by 2 laws;
      int numlawBulkMinus, numlawBulkPlus;
      if(_domMinus->getMaterialLaw()->getType() != materialLaw::fracture){
        numlawBulkMinus = _domMinus->getLawNum();
      }
      else{
        materialLaw2LawsInitializer *m2law = dynamic_cast<materialLaw2LawsInitializer*>(_domMinus->getMaterialLaw());
        numlawBulkMinus = m2law->bulkLawNumber();
      }
      if(_domPlus->getMaterialLaw()->getType() != materialLaw::fracture){
        numlawBulkPlus = _domPlus->getLawNum();
      }
      else{
        materialLaw2LawsInitializer *m2law = dynamic_cast<materialLaw2LawsInitializer*>(_domPlus->getMaterialLaw());
        numlawBulkPlus = m2law->bulkLawNumber();
      }
      _mlawMinus = new FractureByCohesive3DLaw(-numlawBulkMinus,numlawBulkMinus,_lnum);
      _mlawPlus = new FractureByCohesive3DLaw(-numlawBulkPlus,numlawBulkPlus,_lnum);
      if(numlawBulkMinus == numlawBulkPlus)
      {
        _mlaw = _mlawMinus;
      }
      else
      {
        Msg::Warning("Cannot set the interface law on interDomain3D");
      }
    }
    _mlawMinus->initLaws(maplaw);
    _mlawPlus->initLaws(maplaw);
    setmaterial=true;
  }
}


void interDomainBetween3D::stabilityParameters(const double b1)
{
  _beta1 = b1;
}

void interDomainBetween3D::matrixByPerturbation(const int iinter, const double eps)
{
  _interByPert = iinter;
  _eps = eps;

}

void computeJump(const std::vector<TensorialTraits<double>::ValType> &Val_m,const int n_m,
                      const std::vector<TensorialTraits<double>::ValType> &Val_p,
                      const int n_p,const fullVector<double> & dispm,const fullVector<double> & dispp,SVector3 &ujump)
{
  for(int i=0;i<3;i++){
    ujump(i) =0.;
    for(int j=0;j<n_m;j++)
      ujump(i)-=Val_m[j]*dispm(j+i*n_m);
    for(int j=0;j<n_p;j++)
      ujump(i)+=Val_p[j]*dispp(j+i*n_p);
  }
}

void computeNonLocalJump(const std::vector<TensorialTraits<double>::ValType> &Val_m,const int n_m,
                      const std::vector<TensorialTraits<double>::ValType> &Val_p,
                      const int n_p,const fullVector<double> & dispm,const fullVector<double> & dispp, double &epsjump)
{
  epsjump =0.;
  for(int j=0;j<n_m;j++)
    epsjump-=Val_m[j]*dispm(j+3*n_m);
  for(int j=0;j<n_p;j++)
    epsjump+=Val_p[j]*dispp(j+3*n_p);
}

void computeThermoMechanicsJump(const std::vector<TensorialTraits<double>::ValType> &Val_m,const int n_m,
                      const std::vector<TensorialTraits<double>::ValType> &Val_p,
                      const int n_p,const fullVector<double> & dispm,const fullVector<double> & dispp, double &temperaturejump)
{
  temperaturejump =0.;
  for(int j=0;j<n_m;j++)
    temperaturejump-=Val_m[j]*dispm(j+3*n_m);
  for(int j=0;j<n_p;j++)
    temperaturejump+=Val_p[j]*dispp(j+3*n_p);
}


nonLocalDamageDG3DDomain::nonLocalDamageDG3DDomain (const int tag,const int phys, const int ws, const int lnum,
                                          const int fdg, const double nonLocalEqRatio, const int dim) : dG3DDomain(tag,phys,ws,lnum,fdg,dim), _nonLocalEqRatio(nonLocalEqRatio)
{
  if(_space!=NULL) delete _space;   // A space is created in dG3DDomain remove it!
  switch(_wsp)
  {
   case functionSpaceType::Lagrange:
    if(_fullDg)
      _space = new dG3DLagrangeFunctionSpace(_phys,4);
    else
      _space = new g3DLagrangeFunctionSpace(_phys,4);
    break;
   case functionSpaceType::Inter :
    break; // InterfaceDomain set function space later
   default:
    Msg::Error("Function space type is unknown on domain %d",_phys);
  }
  _spaceMinus = _space;
  _spacePlus = _space;
}

nonLocalDamageDG3DDomain::nonLocalDamageDG3DDomain(const nonLocalDamageDG3DDomain &source) : dG3DDomain(source)
{
     _nonLocalBeta = source._nonLocalBeta;
     _continuity = source._continuity;
     _nonLocalEqRatio = source._nonLocalEqRatio;
}

void nonLocalDamageDG3DDomain::createTerms(unknownField *uf,IPField*ip, std::vector<nonLinearNeumannBC> &vNeumann)
{
  FunctionSpace<double>* dgspace = static_cast<FunctionSpace<double>*>(_space);
  ltermBulk = new nonLocalDG3DForceBulk(*dgspace,_mlaw,_fullDg,ip,getNonLocalEqRatio());
  if(_fullDg)
    ltermBound = new nonLocalDG3DForceInter(*dgspace,dgspace,_mlaw,_mlaw,_interQuad,_beta1,_fullDg,ip,_nonLocalBeta,_continuity, getNonLocalEqRatio());
  else
     ltermBound = new nonLinearTermVoid();
  if(_bmbp)
    btermBulk = new BilinearTermPerturbation<double>(ltermBulk,*dgspace,*dgspace,uf,ip,this,_eps);
  else
   btermBulk = new nonLocalDG3DStiffnessBulk(*dgspace,_mlaw,_fullDg,ip,getNonLocalEqRatio());
  if(_fullDg)
  {
    if(_interByPert)
      btermBound = new BilinearTermPerturbation<double>(ltermBound,*dgspace,*dgspace,uf,ip,this,_eps);
    else
      btermBound = new nonLocalDG3DStiffnessInter(*dgspace,*dgspace,_mlaw,_mlaw,_interQuad,_beta1,ip,uf,_fullDg,_nonLocalBeta,_continuity,getNonLocalEqRatio());
  }
  else
    btermBound = new BiNonLinearTermVoid();

  this->btermVirtBound = new BiNonLinearTermVoid();
  this->ltermVirtBound = new nonLinearTermVoid();


  massterm = new nonLocalMassG3D(*dgspace,_mlaw);

   // term of neumannBC
   for(int i=0;i<vNeumann.size();i++){
    nonLinearNeumannBC &neu = vNeumann[i];
    for(int j=0;j<neu._vspace.size();j++){
      if(neu._vdom[j] == this)
      {
//      if(neu._vterm[j] != NULL) delete neu._vterm[j];
        FunctionSpace<double> *spneu = static_cast<FunctionSpace<double>*>(neu._vspace[j]);
        neu._vterm[j] = new nonLocalG3DLoadTerm<double>(*spneu,neu._f);
      }
    }
   }

}

void nonLocalDamageDG3DDomain::computeStrain(AllIPState *aips,MElement *e, IPStateBase::whichState ws,
                                        fullVector<double> &disp)
{
  dG3DDomain::computeStrain(aips,e, ws,disp);
  nlsFunctionSpaceXYZ<double> *sp = static_cast<nlsFunctionSpaceXYZ<double>*>(_space);
  IntPt *GP;
  int npts_bulk=integBulk->getIntPoints(e,&GP);
  AllIPState::ipstateElementContainer *vips = aips->getIPstate(e->getNum());
  // get values of shape functions and derivatives at all Gauss points
  std::vector<GaussPointSpaceValues<double>*> valls;
  sp->get(e,npts_bulk,GP,valls);
  int nbFF = e->getNumShapeFunctions();
  for(int j=0;j<npts_bulk;j++){

    std::vector<TensorialTraits<double>::ValType> Vals = valls[j]->_vvals;
    std::vector<TensorialTraits<double>::GradType> Grads = valls[j]->_vgrads;

    IPStateBase* ips = (*vips)[j];
    nonLocalDamageDG3DIPVariableBase *ipv = static_cast<nonLocalDamageDG3DIPVariableBase*>(ips->getState(ws));
    nonLocalDamageDG3DIPVariableBase *ipvprev = static_cast<nonLocalDamageDG3DIPVariableBase*>(ips->getState(IPStateBase::previous));
    double *effectivePlasticStrain = &(ipv->getRefToEffectivePlasticStrain());
    SVector3 *gradepl = &(ipv->getRefToGradNonLocalPlasticStrain());
    *effectivePlasticStrain = 0.;
    gradepl->operator=(0.);
    for (int i = 0; i < nbFF; i++)
    {
      double ptilde = disp(i+3*nbFF);
      //epsbar: take compoenent epsbar
      *effectivePlasticStrain += Vals[i+3*nbFF]*ptilde;
      gradepl->operator()(0) += Grads[i+3*nbFF][0]*ptilde;
      gradepl->operator()(1) += Grads[i+3*nbFF][1]*ptilde;
      gradepl->operator()(2) += Grads[i+3*nbFF][2]*ptilde;
    }
  }
}

bool nonLocalDamageDG3DDomain::considerForTimeStep(AllIPState *aips,MElement *e)
{
  nlsFunctionSpaceXYZ<double> *sp = static_cast<nlsFunctionSpaceXYZ<double>*>(_space);
  IntPt *GP;
  int npts_bulk=integBulk->getIntPoints(e,&GP);
  AllIPState::ipstateElementContainer *vips = aips->getIPstate(e->getNum());
  bool cons = false;
  for(int j=0;j<npts_bulk;j++){
    IPStateBase* ips = (*vips)[j];
    nonLocalDamageDG3DIPVariableBase *ipv = static_cast<nonLocalDamageDG3DIPVariableBase*>(ips->getState(IPStateBase::current));
    nonLocalDamageDG3DIPVariableBase *ipvprev = static_cast<nonLocalDamageDG3DIPVariableBase*>(ips->getState(IPStateBase::previous));
    if(ipv->get(IPField::DAMAGE)<0.9)
    {
       cons = true;
       break;
    }
  }
  return cons;
}

void nonLocalDamageDG3DDomain::computeStrain(AllIPState *aips,MInterfaceElement *ie, IntPt *GP,const IPStateBase::whichState ws,
                                        partDomain* efMinus, partDomain *efPlus, fullVector<double> &dispm,
                                        fullVector<double> &dispp,const bool virt)
{
  dG3DDomain::computeStrain(aips,ie, GP,ws,efMinus,efPlus, dispm,dispp,virt);

  nlsFunctionSpaceXYZ<double> *_spaceminus = static_cast<nlsFunctionSpaceXYZ<double>*>(efMinus->getFunctionSpace());
  MElement *ele = dynamic_cast<MElement*>(ie);
  int npts=integBound->getIntPoints(ele,&GP);
  // Get Gauss points values on minus and plus elements
  IntPt *GPm; IntPt *GPp;
  _interQuad->getIntPoints(ie,GP,&GPm,&GPp);
  // Get values of shapes functions and derivative at all Gauss points
  std::vector<GaussPointSpaceValues<double>*> vallm;
  _spaceminus->get(ie->getElem(0),npts,GPm,vallm);
  // vector with nodal displacement of interface
  // take from minus displacement vector
  if(!virt){
    nlsFunctionSpaceXYZ<double> *_spaceplus = static_cast<nlsFunctionSpaceXYZ<double>*>(efPlus->getFunctionSpace());
    MElement *em = ie->getElem(0);
    MElement *ep = ie->getElem(1);
    // Get values of shapes functions and derivative at all Gauss points
    std::vector<GaussPointSpaceValues<double>*> vallp;
    _spaceplus->get(ie->getElem(1),npts,GPp,vallp);

    int nbdofm = _spaceminus->getNumKeys(em);
    int nbdofp = _spaceplus->getNumKeys(ep);
    int nbFFm = em->getNumVertices();
    int nbFFp = ep->getNumVertices();

    AllIPState::ipstateElementContainer *vips = aips->getIPstate(ie->getNum());
    for(int j=0;j<npts;j++){
      std::vector<TensorialTraits<double>::ValType> &Valsm = vallm[j]->_vvals;
      std::vector<TensorialTraits<double>::ValType> &Valsp = vallp[j]->_vvals;
      std::vector<TensorialTraits<double>::GradType> &Gradsm = vallm[j]->_vgrads;
      std::vector<TensorialTraits<double>::GradType> &Gradsp = vallp[j]->_vgrads;

      IPStateBase* ipsm = (*vips)[j];
      IPStateBase* ipsp = (*vips)[j+npts];

      nonLocalDamageDG3DIPVariableBase* ipvm; //= static_cast<nonLocalDamageDG3DIPVariable*>(ipsm->getState(ws));
      nonLocalDamageDG3DIPVariableBase* ipvp; //= static_cast<nonLocalDamageDG3DIPVariable*>(ipsp->getState(ws));
//      nonLocalDamageDG3DIPVariable* ipvmprev = static_cast<nonLocalDamageDG3DIPVariable*>(ipsm->getState(IPStateBase::previous));
//      nonLocalDamageDG3DIPVariable* ipvpprev = static_cast<nonLocalDamageDG3DIPVariable*>(ipsp->getState(IPStateBase::previous));
      // check if fracture or not via minus if true for minus --> true for plus too
      FractureCohesive3DIPVariable *ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipsm->getState(ws));
      if(ipvtmp!=NULL)
      {
        ipvm = static_cast<nonLocalDamageDG3DIPVariableBase*>(ipvtmp->getIPvBulk());
        ipvtmp = static_cast<FractureCohesive3DIPVariable*>(ipsp->getState(ws)); // here sure cast OK
        ipvp = static_cast<nonLocalDamageDG3DIPVariableBase*>(ipvtmp->getIPvBulk());
      }
      else
      {
        ipvm = static_cast<nonLocalDamageDG3DIPVariableBase*>(ipsm->getState(ws));
        ipvp = static_cast<nonLocalDamageDG3DIPVariableBase*>(ipsp->getState(ws));
      }
      double *effectivePlasticStrainm = &(ipvm->getRefToEffectivePlasticStrain());
      *effectivePlasticStrainm = 0.;

      SVector3 *gradeplm = &(ipvm->getRefToGradNonLocalPlasticStrain());
      gradeplm->operator=(0.);

      for (int i = 0; i < nbFFm; i++)
      {
	 double ptilde = dispm(i+3*nbFFm);
         //epsbar: take compoenent epsbar
        *effectivePlasticStrainm += Valsm[i+3*nbFFm]*ptilde;
        gradeplm->operator()(0) += Gradsm[i+3*nbFFm][0]*ptilde;
        gradeplm->operator()(1) += Gradsm[i+3*nbFFm][1]*ptilde;
        gradeplm->operator()(2) += Gradsm[i+3*nbFFm][2]*ptilde;
      }
      double *effectivePlasticStrainp = &(ipvp->getRefToEffectivePlasticStrain());
      *effectivePlasticStrainp = 0.;
      SVector3 *gradeplp = &(ipvp->getRefToGradNonLocalPlasticStrain());
      gradeplp->operator=(0.);

      for (int i = 0; i < nbFFp; i++)
      {
	 double ptilde = dispp(i+3*nbFFp);

         //epsbar: take compoenent epsbar
        *effectivePlasticStrainp += Valsp[i+3*nbFFp]*ptilde;
        gradeplp->operator()(0) += Gradsp[i+3*nbFFp][0]*ptilde;
        gradeplp->operator()(1) += Gradsp[i+3*nbFFp][1]*ptilde;
        gradeplp->operator()(2) += Gradsp[i+3*nbFFp][2]*ptilde;
      }
      double epsjump=0.;
      // compute the jump
      computeNonLocalJump(Valsm,nbFFm,Valsp,nbFFp,dispm,dispp,epsjump);
      ipvm->getRefToNonLocalJump() = epsjump;
      ipvp->getRefToNonLocalJump() = epsjump;
    }
  }
  else
  {  // Virtual interface element
     Msg::Error("No virtual interface for 3D");

  }
}

void nonLocalDamageDG3DDomain::allocateInterfaceMPI(const int rankOtherPart, const groupOfElements &groupOtherPart,dgPartDomain**  dgdom)
{
  // domain creation has it function create a domain with a part of the same domain contained on an other part,
  // the type of domain is known
  if(*dgdom==NULL){ // domain doesn't exist --> creation
    *dgdom = new nonLocalInterDomainBetween3D(this->getTag(),this,this,this->getNonLocalEqRatio(),0); // TO CHANGE HERE
    // set the stability parameters
    nonLocalDamageDG3DDomain* newInterDom3D = static_cast< nonLocalDamageDG3DDomain* >(*dgdom);
    newInterDom3D->stabilityParameters(this->_beta1);
    newInterDom3D->nonLocalStabilityParameters(this->_nonLocalBeta,this->_continuity);
  }
}

nonLocalInterDomainBetween3D::nonLocalInterDomainBetween3D(const int tag, partDomain *dom1,partDomain *dom2,const double nonLocalEqRatio, const int lnum) :
                                  interDomainBase(dom1,dom2),
                                  nonLocalDamageDG3DDomain(tag,manageInterface::getKey(dom1->getPhysical(),
                                                                    dom2->getPhysical()),10000,lnum,true,_nonLocalEqRatio,dom1->getDim()), _nonLocalEqRatio(nonLocalEqRatio)
{

  // create empty groupOfElements
  g = new groupOfElements();
  // set functionalSpace
  _spaceMinus = _domMinus->getFunctionSpace();
  _spacePlus = _domPlus->getFunctionSpace();
  if((_domMinus->getFunctionSpaceType() == functionSpaceType::Lagrange) and
     (_domPlus->getFunctionSpaceType() == functionSpaceType::Lagrange)){
    if(_space != NULL) delete _space;
    _space = new dG3DLagrangeBetween2DomainsFunctionSpace(4,_spaceMinus,_spacePlus);
    _wsp = functionSpaceType::Lagrange;
  }
  else{
    Msg::Error("Impossible to create functional space on nonLocalIInterDomain3D %d",_phys);
  }
}


nonLocalInterDomainBetween3D::nonLocalInterDomainBetween3D(const nonLocalInterDomainBetween3D &source) : interDomainBase(source), nonLocalDamageDG3DDomain(source)
{
  _nonLocalEqRatio = source._nonLocalEqRatio;
}

void nonLocalInterDomainBetween3D::createTerms(unknownField *uf,IPField*ip, std::vector<nonLinearNeumannBC> &vNeumann)
{
  // void term (as they have to exist)
  this->ltermBulk = new nonLinearTermVoid();
  this->btermBulk = new BiNonLinearTermVoid();

  this->btermVirtBound = new BiNonLinearTermVoid();
  this->ltermVirtBound = new nonLinearTermVoid();
  this->massterm = new BiNonLinearTermVoid();

  FunctionSpace<double>* dgspace = static_cast<FunctionSpace<double>*>(_space);
  FunctionSpace<double>* dgspace1 = static_cast<FunctionSpace<double>*>(_spaceMinus);
  FunctionSpace<double>* dgspace2 = static_cast<FunctionSpace<double>*>(_spacePlus);
  dG3DDomain* dgldomMinus = static_cast<dG3DDomain*>(_domMinus);
  dG3DDomain* dgldomPlus = static_cast<dG3DDomain*>(_domPlus);
  ltermBound = new nonLocalDG3DForceInter(*dgspace1,dgspace2,_mlawMinus,_mlawPlus,_interQuad,_beta1,true,ip,_nonLocalBeta,_continuity,getNonLocalEqRatio());
  if(_interByPert)
    btermBound = new BilinearTermPerturbation<double>(ltermBound,*dgspace1,*dgspace2,uf,ip,this,_eps);
  else
    btermBound = new nonLocalDG3DStiffnessInter(*dgspace1,*dgspace2,_mlawMinus,_mlawPlus,_interQuad,_beta1,ip,uf,true,_nonLocalBeta,_continuity,getNonLocalEqRatio());
}
void nonLocalInterDomainBetween3D::initializeTerms(unknownField *uf,IPField*ip, std::vector<nonLinearNeumannBC> &vNeumann)
{

  createTerms(uf,ip, vNeumann);
  initializeFractureBase(uf,ip, vNeumann,gi, integBound);
}

void nonLocalInterDomainBetween3D::setGaussIntegrationRule()
{
  setGaussIntegrationRuleBase(gi, &integBound, &integBulk,&_interQuad, _gaussorderbound, _gqt);
}

void nonLocalInterDomainBetween3D::computeAllIPStrain(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, const bool virt){
  IntPt *GP;
  fullVector<double> dispm;
  fullVector<double> dispp;
  std::vector<Dof> R;
  for(groupOfElements::elementContainer::iterator it=gi->begin(); it!=gi->end(); ++it){
    MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(*it);
    R.clear();
    _spaceMinus->getKeys(iele->getElem(0),R);
    dispm.resize(R.size());
    ufield->get(R,dispm);
    R.clear();
    _spacePlus->getKeys(iele->getElem(1),R);
    dispp.resize(R.size());
    ufield->get(R,dispp);
    this->computeStrain(aips,iele,GP,ws,_domMinus,_domPlus,dispm,dispp,virt);
  }
};


void nonLocalInterDomainBetween3D::computeIPVariable(AllIPState *aips,const unknownField *ufield,
                                                const IPStateBase::whichState ws, bool stiff)
{
  #if defined(HAVE_MPI)
  if (_mlawMinus->getType()==materialLaw::numeric and this->_otherRanks.size()>0){
    this->computeIPVariableMPI(aips,ufield,ws,stiff);
  }
  else
  #endif //HAVE_MPI
  {
    this->computeAllIPStrain(aips,ufield,ws,false);
    IntPt *GP;
    fullVector<double> dispm;
    fullVector<double> dispp;
    std::vector<Dof> R;
    dG3DMaterialLaw *mlawMinus = static_cast<dG3DMaterialLaw*>(_mlawMinus);
    dG3DMaterialLaw *mlawPlus = static_cast<dG3DMaterialLaw*>(_mlawPlus);
    for(groupOfElements::elementContainer::iterator it=gi->begin(); it!=gi->end(); ++it){
      MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(*it);
      int npts_inter= integBound->getIntPoints(*it,&GP);
      R.clear();
      _spaceMinus->getKeys(iele->getElem(0),R);
      dispm.resize(R.size());
      ufield->get(R,dispm);
      R.clear();
      _spacePlus->getKeys(iele->getElem(1),R);
      dispp.resize(R.size());
      ufield->get(R,dispp);
      this->computeIpv(aips,iele,GP,ws,_domMinus,_domPlus,mlawMinus,mlawPlus,dispm,dispp,false,stiff);
    }
  }
}

void nonLocalInterDomainBetween3D::setMaterialLaw(const std::map<int,materialLaw*> maplaw)
{
  // oterwise the law is already set
  if(!setmaterial){
    _domMinus->setMaterialLaw(maplaw);
    _domPlus->setMaterialLaw(maplaw);
    _mlaw = NULL;
    if(_lnum == 0){ // no fracture law prescribed --> materialLawMinus and Plus are given via domain
      _mlawMinus = _domMinus->getMaterialLaw();
      _mlawPlus = _domPlus->getMaterialLaw();
      if(_domMinus->getPhysical() == _domPlus->getPhysical())
      {
         _mlaw = _mlawMinus; // same law on both
      }
      else if(_mlawMinus->getNum() == _mlawPlus->getNum())
      {
        _mlaw = _mlawMinus; // same law on both
      }
      else
      {
        Msg::Error("Cannot set the interface law on interDomain3D");
      }
    }
    else{
      // create law (use lnum with negative number same number for the two but it doesn't matter)
      // these law don't appear in map with all laws
      // check if minus and plus laws are already fracture by 2 laws;
      int numlawBulkMinus, numlawBulkPlus;
      if(_domMinus->getMaterialLaw()->getType() != materialLaw::fracture){
        numlawBulkMinus = _domMinus->getLawNum();
      }
      else{
        materialLaw2LawsInitializer *m2law = dynamic_cast<materialLaw2LawsInitializer*>(_domMinus->getMaterialLaw());
        numlawBulkMinus = m2law->bulkLawNumber();
      }
      if(_domPlus->getMaterialLaw()->getType() != materialLaw::fracture){
        numlawBulkPlus = _domPlus->getLawNum();
      }
      else{
        materialLaw2LawsInitializer *m2law = dynamic_cast<materialLaw2LawsInitializer*>(_domPlus->getMaterialLaw());
        numlawBulkPlus = m2law->bulkLawNumber();
      }
      _mlawMinus = new FractureByCohesive3DLaw(-numlawBulkMinus,numlawBulkMinus,_lnum);
      _mlawPlus = new FractureByCohesive3DLaw(-numlawBulkPlus,numlawBulkPlus,_lnum);
    }
    _mlawMinus->initLaws(maplaw);
    _mlawPlus->initLaws(maplaw);
    setmaterial=true;
  }
}


void nonLocalInterDomainBetween3D::stabilityParameters(const double b1)
{
  _beta1 = b1;
}

void nonLocalInterDomainBetween3D::matrixByPerturbation(const int iinter, const double eps)
{
  _interByPert = iinter;
  _eps = eps;

}












ThermoMechanicsDG3DDomain::ThermoMechanicsDG3DDomain (const int tag,const int phys, const int ws, const int lnum,
                                          const int fdg, const double ThermoMechanicsEqRatio, const int dim) : dG3DDomain(tag,phys,ws,lnum,fdg,dim), _ThermoMechanicsEqRatio(ThermoMechanicsEqRatio)
{
  if(_space!=NULL) delete _space;   // A space is created in dG3DDomain remove it!
  switch(_wsp)
  {
   case functionSpaceType::Lagrange:
    if(_fullDg)
      _space = new dG3DLagrangeFunctionSpace(_phys,4);
    else
      _space = new g3DLagrangeFunctionSpace(_phys,4);
    break;
   case functionSpaceType::Inter :
    break; // InterfaceDomain set function space later
   default:
    Msg::Error("Function space type is unknown on domain %d",_phys);
  }
  _spaceMinus = _space;
  _spacePlus = _space;
}

ThermoMechanicsDG3DDomain::ThermoMechanicsDG3DDomain(const ThermoMechanicsDG3DDomain &source) : dG3DDomain(source)
{
     _ThermoMechanicsBeta = source._ThermoMechanicsBeta;
     _continuity = source._continuity;
     _ThermoMechanicsEqRatio = source._ThermoMechanicsEqRatio;
}

void ThermoMechanicsDG3DDomain::createTerms(unknownField *uf,IPField*ip, std::vector<nonLinearNeumannBC> &vNeumann)
{
  FunctionSpace<double>* dgspace = static_cast<FunctionSpace<double>*>(_space);
  ltermBulk = new ThermoMechanicsDG3DForceBulk(*dgspace,_mlaw,_fullDg,ip,getThermoMechanicsEqRatio());
  if(_fullDg)
    ltermBound = new ThermoMechanicsDG3DForceInter(*dgspace,dgspace,_mlaw,_mlaw,_interQuad,_beta1,_fullDg,ip,_ThermoMechanicsBeta,_continuity, getThermoMechanicsEqRatio());
  else
     ltermBound = new nonLinearTermVoid();
  if(_bmbp)
    btermBulk = new BilinearTermPerturbation<double>(ltermBulk,*dgspace,*dgspace,uf,ip,this,_eps);
  else
   btermBulk = new ThermoMechanicsDG3DStiffnessBulk(*dgspace,_mlaw,_fullDg,ip,getThermoMechanicsEqRatio());
  if(_fullDg)
  {
    if(_interByPert)
      btermBound = new BilinearTermPerturbation<double>(ltermBound,*dgspace,*dgspace,uf,ip,this,_eps);
    else
      btermBound = new ThermoMechanicsDG3DStiffnessInter(*dgspace,*dgspace,_mlaw,_mlaw,_interQuad,_beta1,ip,uf,_fullDg,_ThermoMechanicsBeta,_continuity,getThermoMechanicsEqRatio());
  }
  else
    btermBound = new BiNonLinearTermVoid();

  this->btermVirtBound = new BiNonLinearTermVoid();
  this->ltermVirtBound = new nonLinearTermVoid();


  massterm = new ThermoMechanicsMassG3D(*dgspace,_mlaw);

   // term of neumannBC
   for(int i=0;i<vNeumann.size();i++){
    nonLinearNeumannBC &neu = vNeumann[i];
    for(int j=0;j<neu._vspace.size();j++){
      if(neu._vdom[j] == this)
      {
//      if(neu._vterm[j] != NULL) delete neu._vterm[j];
        FunctionSpace<double> *spneu = static_cast<FunctionSpace<double>*>(neu._vspace[j]);
        neu._vterm[j] = new ThermoMechanicsG3DLoadTerm<double>(*spneu,neu._f);
      }
    }
   }

}

void ThermoMechanicsDG3DDomain::computeStrain(AllIPState *aips,MElement *e, IPStateBase::whichState ws,
                                        fullVector<double> &disp)
{
  dG3DDomain::computeStrain(aips,e, ws,disp);
  nlsFunctionSpaceXYZ<double> *sp = static_cast<nlsFunctionSpaceXYZ<double>*>(_space);
  IntPt *GP;
  int npts_bulk=integBulk->getIntPoints(e,&GP);
  AllIPState::ipstateElementContainer *vips = aips->getIPstate(e->getNum());
  // get values of shape functions and derivatives at all Gauss points
  std::vector<GaussPointSpaceValues<double>*> valls;
  sp->get(e,npts_bulk,GP,valls);
  int nbFF = e->getNumShapeFunctions();
  for(int j=0;j<npts_bulk;j++){

    std::vector<TensorialTraits<double>::ValType> Vals = valls[j]->_vvals;
    std::vector<TensorialTraits<double>::GradType> Grads = valls[j]->_vgrads;

    IPStateBase* ips = (*vips)[j];
    ThermoMechanicsDG3DIPVariableBase *ipv = static_cast<ThermoMechanicsDG3DIPVariableBase*>(ips->getState(ws));
    ThermoMechanicsDG3DIPVariableBase *ipvprev = static_cast<ThermoMechanicsDG3DIPVariableBase*>(ips->getState(IPStateBase::previous));
    double *temperature = &(ipv->getRefTotemperature());
    SVector3 *gradT = &(ipv->getRefTogradT());
    *temperature= 0.;
    gradT->operator=(0.);
    for (int i = 0; i < nbFF; i++)
    {
      double temp = disp(i+3*nbFF);
      //epsbar: take compoenent epsbar
      *temperature += Vals[i+3*nbFF]*temp;
      gradT->operator()(0) += Grads[i+3*nbFF][0]*temp;
      gradT->operator()(1) += Grads[i+3*nbFF][1]*temp;
      gradT->operator()(2) += Grads[i+3*nbFF][2]*temp;
    }
  }
}


void ThermoMechanicsDG3DDomain::computeStrain(AllIPState *aips,MInterfaceElement *ie, IntPt *GP,const IPStateBase::whichState ws,
                                        partDomain* efMinus, partDomain *efPlus, fullVector<double> &dispm,
                                        fullVector<double> &dispp,const bool virt)
{
  dG3DDomain::computeStrain(aips,ie, GP,ws,efMinus,efPlus, dispm,dispp,virt);

  nlsFunctionSpaceXYZ<double> *_spaceminus = static_cast<nlsFunctionSpaceXYZ<double>*>(efMinus->getFunctionSpace());
  MElement *ele = dynamic_cast<MElement*>(ie);
  int npts=integBound->getIntPoints(ele,&GP);
  // Get Gauss points values on minus and plus elements
  IntPt *GPm; IntPt *GPp;
  _interQuad->getIntPoints(ie,GP,&GPm,&GPp);
  // Get values of shapes functions and derivative at all Gauss points
  std::vector<GaussPointSpaceValues<double>*> vallm;
  _spaceminus->get(ie->getElem(0),npts,GPm,vallm);
  // vector with nodal displacement of interface
  // take from minus displacement vector
  if(!virt){
    nlsFunctionSpaceXYZ<double> *_spaceplus = static_cast<nlsFunctionSpaceXYZ<double>*>(efPlus->getFunctionSpace());
    MElement *em = ie->getElem(0);
    MElement *ep = ie->getElem(1);
    // Get values of shapes functions and derivative at all Gauss points
    std::vector<GaussPointSpaceValues<double>*> vallp;
    _spaceplus->get(ie->getElem(1),npts,GPp,vallp);

    int nbdofm = _spaceminus->getNumKeys(em);
    int nbdofp = _spaceplus->getNumKeys(ep);
    int nbFFm = em->getNumVertices();
    int nbFFp = ep->getNumVertices();

    AllIPState::ipstateElementContainer *vips = aips->getIPstate(ie->getNum());
    for(int j=0;j<npts;j++){
      std::vector<TensorialTraits<double>::ValType> &Valsm = vallm[j]->_vvals;
      std::vector<TensorialTraits<double>::ValType> &Valsp = vallp[j]->_vvals;
      std::vector<TensorialTraits<double>::GradType> &Gradsm = vallm[j]->_vgrads;
      std::vector<TensorialTraits<double>::GradType> &Gradsp = vallp[j]->_vgrads;

      IPStateBase* ipsm = (*vips)[j];
      IPStateBase* ipsp = (*vips)[j+npts];

      ThermoMechanicsDG3DIPVariableBase* ipvm; //= static_cast<nonLocalDamageDG3DIPVariable*>(ipsm->getState(ws));
      ThermoMechanicsDG3DIPVariableBase* ipvp; //= static_cast<nonLocalDamageDG3DIPVariable*>(ipsp->getState(ws));
//      nonLocalDamageDG3DIPVariable* ipvmprev = static_cast<nonLocalDamageDG3DIPVariable*>(ipsm->getState(IPStateBase::previous));
//      nonLocalDamageDG3DIPVariable* ipvpprev = static_cast<nonLocalDamageDG3DIPVariable*>(ipsp->getState(IPStateBase::previous));
      // check if fracture or not via minus if true for minus --> true for plus too
      FractureCohesive3DIPVariable *ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipsm->getState(ws));
      if(ipvtmp!=NULL)
      {
        ipvm = static_cast<ThermoMechanicsDG3DIPVariableBase*>(ipvtmp->getIPvBulk());
        ipvtmp = static_cast<FractureCohesive3DIPVariable*>(ipsp->getState(ws)); // here sure cast OK
        ipvp = static_cast<ThermoMechanicsDG3DIPVariableBase*>(ipvtmp->getIPvBulk());
      }
      else
      {
        ipvm = static_cast<ThermoMechanicsDG3DIPVariableBase*>(ipsm->getState(ws));
        ipvp = static_cast<ThermoMechanicsDG3DIPVariableBase*>(ipsp->getState(ws));
      }
      double *temperaturem = &(ipvm->getRefTotemperature());
      *temperaturem = 0.;

      SVector3 *gradTm = &(ipvm->getRefTogradT());
      gradTm->operator=(0.);

      for (int i = 0; i < nbFFm; i++)
      {
	 double temp = dispm(i+3*nbFFm);
         //epsbar: take compoenent epsbar
        *temperaturem += Valsm[i+3*nbFFm]*temp;
        gradTm->operator()(0) += Gradsm[i+3*nbFFm][0]*temp;
        gradTm->operator()(1) += Gradsm[i+3*nbFFm][1]*temp;
        gradTm->operator()(2) += Gradsm[i+3*nbFFm][2]*temp;
      }
      double *temperaturep = &(ipvp->getRefTotemperature());
      *temperaturep = 0.;
      SVector3 *gradTp = &(ipvp->getRefTogradT());
      gradTp->operator=(0.);

      for (int i = 0; i < nbFFp; i++)
      {
	 double temp = dispp(i+3*nbFFp);

         //epsbar: take compoenent epsbar
        *temperaturep += Valsp[i+3*nbFFp]*temp;
        gradTp->operator()(0) += Gradsp[i+3*nbFFp][0]*temp;
        gradTp->operator()(1) += Gradsp[i+3*nbFFp][1]*temp;
        gradTp->operator()(2) += Gradsp[i+3*nbFFp][2]*temp;
      }
      double tempjump=0.;
      // compute the jump
      computeThermoMechanicsJump(Valsm,nbFFm,Valsp,nbFFp,dispm,dispp,tempjump);
      ipvm->getRefToTemperatureJump() = tempjump;
      ipvp->getRefToTemperatureJump() = tempjump;
    }
  }
  else
  {  // Virtual interface element
     Msg::Error("No virtual interface for 3D");

  }
}

void ThermoMechanicsDG3DDomain::allocateInterfaceMPI(const int rankOtherPart, const groupOfElements &groupOtherPart,dgPartDomain**  dgdom)
{
  // domain creation has it function create a domain with a part of the same domain contained on an other part,
  // the type of domain is known
  if(*dgdom==NULL){ // domain doesn't exist --> creation
    *dgdom = new ThermoMechanicsInterDomainBetween3D(this->getTag(),this,this,this->getThermoMechanicsEqRatio(),0); // TO CHANGE HERE
    // set the stability parameters
    ThermoMechanicsDG3DDomain* newInterDom3D = static_cast< ThermoMechanicsDG3DDomain* >(*dgdom);
    newInterDom3D->stabilityParameters(this->_beta1);
    newInterDom3D->ThermoMechanicsStabilityParameters(this->_ThermoMechanicsBeta,this->_continuity);
  }
}

ThermoMechanicsInterDomainBetween3D::ThermoMechanicsInterDomainBetween3D(const int tag, partDomain *dom1,partDomain *dom2,const double ThermoMechanicsEqRatio, const int lnum) :
                                  interDomainBase(dom1,dom2),
                                  ThermoMechanicsDG3DDomain(tag,manageInterface::getKey(dom1->getPhysical(),
                                                                    dom2->getPhysical()),10000,lnum,true,_ThermoMechanicsEqRatio,dom1->getDim()), _ThermoMechanicsEqRatio(ThermoMechanicsEqRatio)
{

  // create empty groupOfElements
  g = new groupOfElements();
  // set functionalSpace
  _spaceMinus = _domMinus->getFunctionSpace();
  _spacePlus = _domPlus->getFunctionSpace();
  if((_domMinus->getFunctionSpaceType() == functionSpaceType::Lagrange) and
     (_domPlus->getFunctionSpaceType() == functionSpaceType::Lagrange)){
    if(_space != NULL) delete _space;
    _space = new dG3DLagrangeBetween2DomainsFunctionSpace(4,_spaceMinus,_spacePlus);
    _wsp = functionSpaceType::Lagrange;
  }
  else{
    Msg::Error("Impossible to create functional space on ThermoMechanicsIInterDomain3D %d",_phys);
  }
}


ThermoMechanicsInterDomainBetween3D::ThermoMechanicsInterDomainBetween3D(const ThermoMechanicsInterDomainBetween3D &source) : interDomainBase(source), ThermoMechanicsDG3DDomain(source)
{
  _ThermoMechanicsEqRatio = source._ThermoMechanicsEqRatio;
}

void ThermoMechanicsInterDomainBetween3D::createTerms(unknownField *uf,IPField*ip, std::vector<nonLinearNeumannBC> &vNeumann)
{
  // void term (as they have to exist)
  this->ltermBulk = new nonLinearTermVoid();
  this->btermBulk = new BiNonLinearTermVoid();

  this->btermVirtBound = new BiNonLinearTermVoid();
  this->ltermVirtBound = new nonLinearTermVoid();
  this->massterm = new BiNonLinearTermVoid();

  FunctionSpace<double>* dgspace = static_cast<FunctionSpace<double>*>(_space);
  FunctionSpace<double>* dgspace1 = static_cast<FunctionSpace<double>*>(_spaceMinus);
  FunctionSpace<double>* dgspace2 = static_cast<FunctionSpace<double>*>(_spacePlus);
  dG3DDomain* dgldomMinus = static_cast<dG3DDomain*>(_domMinus);
  dG3DDomain* dgldomPlus = static_cast<dG3DDomain*>(_domPlus);
  ltermBound = new ThermoMechanicsDG3DForceInter(*dgspace1,dgspace2,_mlawMinus,_mlawPlus,_interQuad,_beta1,true,ip,_ThermoMechanicsBeta,_continuity,getThermoMechanicsEqRatio());
  if(_interByPert)
    btermBound = new BilinearTermPerturbation<double>(ltermBound,*dgspace1,*dgspace2,uf,ip,this,_eps);
  else
    btermBound = new ThermoMechanicsDG3DStiffnessInter(*dgspace1,*dgspace2,_mlawMinus,_mlawPlus,_interQuad,_beta1,ip,uf,true,_ThermoMechanicsBeta,_continuity,getThermoMechanicsEqRatio());
}
void ThermoMechanicsInterDomainBetween3D::initializeTerms(unknownField *uf,IPField*ip, std::vector<nonLinearNeumannBC> &vNeumann)
{

  createTerms(uf,ip, vNeumann);
  initializeFractureBase(uf,ip, vNeumann,gi, integBound);
}

void ThermoMechanicsInterDomainBetween3D::setGaussIntegrationRule()
{
  setGaussIntegrationRuleBase(gi, &integBound, &integBulk,&_interQuad, _gaussorderbound, _gqt);
}

void ThermoMechanicsInterDomainBetween3D::computeAllIPStrain(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, const bool virt){
  IntPt *GP;
  fullVector<double> dispm;
  fullVector<double> dispp;
  std::vector<Dof> R;
  for(groupOfElements::elementContainer::iterator it=gi->begin(); it!=gi->end(); ++it){
    MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(*it);
    R.clear();
    _spaceMinus->getKeys(iele->getElem(0),R);
    dispm.resize(R.size());
    ufield->get(R,dispm);
    R.clear();
    _spacePlus->getKeys(iele->getElem(1),R);
    dispp.resize(R.size());
    ufield->get(R,dispp);
    this->computeStrain(aips,iele,GP,ws,_domMinus,_domPlus,dispm,dispp,virt);
  }
};


void ThermoMechanicsInterDomainBetween3D::computeIPVariable(AllIPState *aips,const unknownField *ufield,
                                                const IPStateBase::whichState ws, bool stiff)
{
  #if defined(HAVE_MPI)
  if (_mlawMinus->getType()==materialLaw::numeric and this->_otherRanks.size()>0){
    this->computeIPVariableMPI(aips,ufield,ws,stiff);
  }
  else
  #endif //HAVE_MPI
  {
    this->computeAllIPStrain(aips,ufield,ws,false);
    IntPt *GP;
    fullVector<double> dispm;
    fullVector<double> dispp;
    std::vector<Dof> R;
    dG3DMaterialLaw *mlawMinus = static_cast<dG3DMaterialLaw*>(_mlawMinus);
    dG3DMaterialLaw *mlawPlus = static_cast<dG3DMaterialLaw*>(_mlawPlus);
    for(groupOfElements::elementContainer::iterator it=gi->begin(); it!=gi->end(); ++it){
      MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(*it);
      int npts_inter= integBound->getIntPoints(*it,&GP);
      R.clear();
      _spaceMinus->getKeys(iele->getElem(0),R);
      dispm.resize(R.size());
      ufield->get(R,dispm);
      R.clear();
      _spacePlus->getKeys(iele->getElem(1),R);
      dispp.resize(R.size());
      ufield->get(R,dispp);
      this->computeIpv(aips,iele,GP,ws,_domMinus,_domPlus,mlawMinus,mlawPlus,dispm,dispp,false,stiff);
    }
  }
}

void ThermoMechanicsInterDomainBetween3D::setMaterialLaw(const std::map<int,materialLaw*> maplaw)
{
  // oterwise the law is already set
  if(!setmaterial){
    _domMinus->setMaterialLaw(maplaw);
    _domPlus->setMaterialLaw(maplaw);
    _mlaw = NULL;
    if(_lnum == 0){ // no fracture law prescribed --> materialLawMinus and Plus are given via domain
      _mlawMinus = _domMinus->getMaterialLaw();
      _mlawPlus = _domPlus->getMaterialLaw();
      if(_domMinus->getPhysical() == _domPlus->getPhysical())
      {
         _mlaw = _mlawMinus; // same law on both
      }
      else if(_mlawMinus->getNum() == _mlawPlus->getNum())
      {
        _mlaw = _mlawMinus; // same law on both
      }
      else
      {
        Msg::Error("Cannot set the interface law on interDomain3D");
      }
    }
    else{
      // create law (use lnum with negative number same number for the two but it doesn't matter)
      // these law don't appear in map with all laws
      // check if minus and plus laws are already fracture by 2 laws;
      int numlawBulkMinus, numlawBulkPlus;
      if(_domMinus->getMaterialLaw()->getType() != materialLaw::fracture){
        numlawBulkMinus = _domMinus->getLawNum();
      }
      else{
        materialLaw2LawsInitializer *m2law = dynamic_cast<materialLaw2LawsInitializer*>(_domMinus->getMaterialLaw());
        numlawBulkMinus = m2law->bulkLawNumber();
      }
      if(_domPlus->getMaterialLaw()->getType() != materialLaw::fracture){
        numlawBulkPlus = _domPlus->getLawNum();
      }
      else{
        materialLaw2LawsInitializer *m2law = dynamic_cast<materialLaw2LawsInitializer*>(_domPlus->getMaterialLaw());
        numlawBulkPlus = m2law->bulkLawNumber();
      }
      _mlawMinus = new FractureByCohesive3DLaw(-numlawBulkMinus,numlawBulkMinus,_lnum);
      _mlawPlus = new FractureByCohesive3DLaw(-numlawBulkPlus,numlawBulkPlus,_lnum);
    }
    _mlawMinus->initLaws(maplaw);
    _mlawPlus->initLaws(maplaw);
    setmaterial=true;
  }
}


void ThermoMechanicsInterDomainBetween3D::stabilityParameters(const double b1)
{
  _beta1 = b1;
}

void ThermoMechanicsInterDomainBetween3D::matrixByPerturbation(const int iinter, const double eps)
{
  _interByPert = iinter;
  _eps = eps;

}
