//
// C++ Interface: ipvariable
//
// Description: Class with definition of ipvarible for dG3D
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef DG3DIPVARIABLE_H_
#define DG3DIPVARIABLE_H_
#include "ipvariable.h"
#include "STensor43.h"
#include "ipJ2linear.h"
#include "ipNonLocalDamageJ2Hyper.h"
#include "ipNonLocalDamageGurson.h"
#include "ipVUMATinterface.h"
#include "ipTransverseIsotropic.h"
#include "ipAnisotropic.h"
#include "ipNonLocalDamage.h"
#include "ipFiniteStrain.h"
#include "ipLinearThermoMechanics.h"
#include "mlawJ2linear.h"
#include "mlawNonLocalDamageJ2Hyper.h"
#include "mlawLinearThermoMechanics.h"
#include "mlawNonLocalDamageGurson.h"

class dG3DIPVariableBase : public ipFiniteStrain
{
 public:
  dG3DIPVariableBase() : ipFiniteStrain(){}
  dG3DIPVariableBase(const dG3DIPVariableBase &source);
  virtual dG3DIPVariableBase& operator=(const IPVariable &source);
  virtual ~dG3DIPVariableBase(){}

  virtual const bool isInterface() const=0;

  // Archiving data
  virtual double get(const int i) const=0;
  virtual double defoEnergy() const=0;
  virtual double plasticEnergy() const=0;
  virtual double vonMises() const=0;
  virtual void getCauchyStress(STensor3 &cauchy) const=0;
  virtual double getJ() const=0;

  virtual const STensor3 &getConstRefToDeformationGradient() const=0;
  virtual STensor3 &getRefToDeformationGradient()=0;

  virtual const STensor3 &getConstRefToFirstPiolaKirchhoffStress() const=0;
  virtual STensor3 &getRefToFirstPiolaKirchhoffStress()=0;

  virtual const STensor43 &getConstRefToTangentModuli() const=0;
  virtual STensor43 &getRefToTangentModuli()=0;

  virtual const STensor43 &getConstRefToElasticTangentModuli() const=0;
  virtual void setRefToElasticTangentModuli(const STensor43 &eT)=0;

  virtual const SVector3 &getConstRefToReferenceOutwardNormal() const=0;
  virtual SVector3 &getRefToReferenceOutwardNormal()=0;

  virtual const SVector3 &getConstRefToCurrentOutwardNormal() const=0;
  virtual SVector3 &getRefToCurrentOutwardNormal()=0;

  virtual const SVector3 &getConstRefToInterfaceForce() const=0;
  virtual SVector3 &getRefToInterfaceForce()=0;

  virtual const STensor3 &getConstRefToDInterfaceForceDjump() const=0;
  virtual STensor3 &getRefToDInterfaceForceDjump()=0;

  virtual const SVector3 &getConstRefToJump() const=0;
  virtual SVector3 &getRefToJump()=0;

  virtual const double &getConstRefToNonLocalJump() const=0;
  virtual double &getRefToNonLocalJump()=0;

  virtual const SVector3 &getConstRefToGradNonLocalPlasticStrain() const=0;
  virtual SVector3 &getRefToGradNonLocalPlasticStrain()=0;
};

class dG3DIPVariable : public dG3DIPVariableBase
{
 private:
  STensor3                     deformationGradient;
  STensor3                     firstPiolaKirchhoffStress;
  STensor43                    tangentModuli; //include geometric terms
  const STensor43                   *elasticTangentModuli;

  // for dG
  bool _oninter;
  SVector3                     referenceOutwardNormal;
  SVector3                     currentOutwardNormal;
  SVector3                     interfaceForce;
  STensor3                     DInterfaceForceDjump;

  SVector3                     jump;

 public:
  dG3DIPVariable(const bool oninter=false); // A empty constructor is mandatory to build FractureCohesive3DIPVariable OK like this as the fracture is on interface??
  dG3DIPVariable(const dG3DIPVariable &source);
  dG3DIPVariable &operator = (const IPVariable &_source);
  virtual ~dG3DIPVariable()
  {

  }
  virtual const bool isInterface() const {return _oninter;}

  // Archiving data
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual double vonMises() const;
  virtual void getCauchyStress(STensor3 &cauchy) const;
  virtual double getJ() const;

  virtual const STensor3 &getConstRefToDeformationGradient() const {return deformationGradient;}
  virtual STensor3 &getRefToDeformationGradient() {return deformationGradient;}

  virtual const STensor3 &getConstRefToFirstPiolaKirchhoffStress() const {return firstPiolaKirchhoffStress;}
  virtual STensor3 &getRefToFirstPiolaKirchhoffStress() {return firstPiolaKirchhoffStress;}

  virtual const STensor43 &getConstRefToTangentModuli() const {return tangentModuli;}
  virtual STensor43 &getRefToTangentModuli() {return tangentModuli;}

  virtual const STensor43 &getConstRefToElasticTangentModuli() const {return *elasticTangentModuli;}
  virtual void setRefToElasticTangentModuli(const STensor43 &eT) {elasticTangentModuli=&eT;}

  virtual const SVector3 &getConstRefToReferenceOutwardNormal() const {return referenceOutwardNormal;}
  virtual SVector3 &getRefToReferenceOutwardNormal() {return referenceOutwardNormal;}

  virtual const SVector3 &getConstRefToCurrentOutwardNormal() const {return currentOutwardNormal;}
  virtual SVector3 &getRefToCurrentOutwardNormal() {return currentOutwardNormal;}

  virtual const SVector3 &getConstRefToInterfaceForce() const {return interfaceForce;}
  virtual SVector3 &getRefToInterfaceForce() {return interfaceForce;}

  virtual const STensor3 &getConstRefToDInterfaceForceDjump() const {return DInterfaceForceDjump;}
  virtual STensor3 &getRefToDInterfaceForceDjump() {return DInterfaceForceDjump;}

  virtual const SVector3 &getConstRefToJump() const {return jump;}
  virtual SVector3 &getRefToJump() {return jump;}

  virtual const double &getConstRefToNonLocalJump() const
                             {Msg::Error("getConstRefToNonLocalJump not defined"); }
  virtual double &getRefToNonLocalJump() {Msg::Error("getRefToNonLocalJump not defined"); }

  virtual const SVector3 &getConstRefToGradNonLocalPlasticStrain() const
                   {Msg::Error("getConstRefToGradNonLocalPlasticStrain( not defined"); }
  virtual SVector3 &getRefToGradNonLocalPlasticStrain()
                   {Msg::Error("getConstRefToGradNonLocalPlasticStrain( not defined"); }

};

class J2LinearDG3DIPVariable : public dG3DIPVariable // or store data in a different way
{
 protected:
  IPJ2linear *_j2ipv;
 public:
  J2LinearDG3DIPVariable(const mlawJ2linear &_j2law, const bool oninter=false);
  J2LinearDG3DIPVariable(const J2LinearDG3DIPVariable &source);
  J2LinearDG3DIPVariable& operator=(const IPVariable &source);
  ~J2LinearDG3DIPVariable(){if(_j2ipv!=NULL) delete _j2ipv;};

 /* specific function */
  IPJ2linear* getIPJ2linear(){return _j2ipv;}
  const IPJ2linear* getIPJ2linear() const{return _j2ipv;}
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
};

class VUMATinterfaceDG3DIPVariable : public dG3DIPVariable
{
 protected:
  IPVUMATinterface *_vumatipv;
 public:
  VUMATinterfaceDG3DIPVariable(int _nsdv, double _size, const bool oninter=false);
  VUMATinterfaceDG3DIPVariable(const VUMATinterfaceDG3DIPVariable &source);
  VUMATinterfaceDG3DIPVariable& operator=(const IPVariable &source);

 /* specific function */
  IPVUMATinterface* getIPVUMATinterface(){return _vumatipv;}
  const IPVUMATinterface* getIPVUMATinterface() const{return _vumatipv;}
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual ~VUMATinterfaceDG3DIPVariable()
  {
     delete _vumatipv;
  }
};

class nonLocalDamageDG3DIPVariableBase : public dG3DIPVariable
{

 protected:

  // for coupling
  STensor3                     dLocalPlasticStrainDStrain;
  STensor3                     dStressDNonLocalPlasticStrain;
  double                       dLocalPlasticStrainDNonLocalPlasticStrain;

 // for dg: jump in non local epl
  double                      nonLocalJump;
  SVector3                     gradNonLocalPlasticStrain;

 public:
  nonLocalDamageDG3DIPVariableBase(const bool oninter=false);
  nonLocalDamageDG3DIPVariableBase(const nonLocalDamageDG3DIPVariableBase &source);
  nonLocalDamageDG3DIPVariableBase& operator=(const IPVariable &source);

  virtual ~nonLocalDamageDG3DIPVariableBase()
  {
  }
  // Archiving data
  virtual double getEffectivePlasticStrain() const=0;
  virtual double getCurrentPlasticStrain() const=0;
  virtual double &getRefToEffectivePlasticStrain()=0;
  virtual double &getRefToCurrentPlasticStrain()=0;
  virtual double get(const int i) const=0;

  virtual const STensor3 &getConstRefToDLocalPlasticStrainDStrain() const {return dLocalPlasticStrainDStrain;}
  virtual STensor3 &getRefToDLocalPlasticStrainDStrain() {return dLocalPlasticStrainDStrain;}

  virtual const STensor3 &getConstRefToDStressDNonLocalPlasticStrain() const {return dStressDNonLocalPlasticStrain;}
  virtual STensor3 &getRefToDStressDNonLocalPlasticStrain() {return dStressDNonLocalPlasticStrain;}

  virtual const double &getConstRefToDLocalPlasticStrainDNonLocalPlasticStrain() const
                             {return dLocalPlasticStrainDNonLocalPlasticStrain;}
  virtual double &getRefToDLocalPlasticStrainDNonLocalPlasticStrain() {return dLocalPlasticStrainDNonLocalPlasticStrain;}

  virtual const double &getConstRefToNonLocalJump() const
                             {return nonLocalJump;}
  virtual double &getRefToNonLocalJump() {return nonLocalJump;}
  virtual const SVector3 &getConstRefToGradNonLocalPlasticStrain() const {return gradNonLocalPlasticStrain;}
  virtual SVector3 &getRefToGradNonLocalPlasticStrain() {return gradNonLocalPlasticStrain;}

  virtual double defoEnergy() const=0;
  virtual double plasticEnergy() const=0;

  virtual const STensor3 &getConstRefToCharacteristicLengthMatrix() const =0;

};

class nonLocalDamageDG3DIPVariable : public nonLocalDamageDG3DIPVariableBase
{

 protected:
  IPNonLocalDamage *_nldipv;

 public:
  nonLocalDamageDG3DIPVariable(int _nsdv, const bool oninter=false);
  nonLocalDamageDG3DIPVariable(const nonLocalDamageDG3DIPVariable &source);
  nonLocalDamageDG3DIPVariable& operator=(const IPVariable &source);

 /* specific function */
  IPNonLocalDamage* getIPNonLocalDamage(){return _nldipv;}
  const IPNonLocalDamage* getIPNonLocalDamage() const{return _nldipv;}
  virtual ~nonLocalDamageDG3DIPVariable()
  {
     delete _nldipv;
  }
  virtual double getEffectivePlasticStrain() const;
  virtual double getCurrentPlasticStrain() const;
  virtual double &getRefToEffectivePlasticStrain();
  virtual double &getRefToCurrentPlasticStrain();
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual const STensor3 &getConstRefToCharacteristicLengthMatrix() const 
  {
    return getIPNonLocalDamage()->_nldCharacteristicLengthMatrix;
  }

};

class nonLocalDamageJ2HyperDG3DIPVariable : public nonLocalDamageDG3DIPVariableBase
{

 protected:
  IPNonLocalDamageJ2Hyper *_nldJ2Hyperipv;

 public:
  nonLocalDamageJ2HyperDG3DIPVariable(const mlawNonLocalDamageJ2Hyper &_j2law, const bool oninter=false);
  nonLocalDamageJ2HyperDG3DIPVariable(const nonLocalDamageJ2HyperDG3DIPVariable &source);
  nonLocalDamageJ2HyperDG3DIPVariable& operator=(const IPVariable &source);

 /* specific function */
  IPNonLocalDamageJ2Hyper* getIPNonLocalDamageJ2Hyper(){return _nldJ2Hyperipv;}
  const IPNonLocalDamageJ2Hyper* getIPNonLocalDamageJ2Hyper() const{return _nldJ2Hyperipv;}
  virtual ~nonLocalDamageJ2HyperDG3DIPVariable()
  {
     delete _nldJ2Hyperipv;
  }
  virtual double getEffectivePlasticStrain() const;
  virtual double getCurrentPlasticStrain() const;
  virtual double &getRefToEffectivePlasticStrain();
  virtual double &getRefToCurrentPlasticStrain();
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual const STensor3 &getConstRefToCharacteristicLengthMatrix() const 
  {
    return _nldJ2Hyperipv->getConstRefToCharacteristicLength();
  }


};

class nonLocalDamageGursonDG3DIPVariable : public nonLocalDamageDG3DIPVariableBase
{

 protected:
  IPNonLocalDamageGurson *_nldGursonipv;

 public:
  nonLocalDamageGursonDG3DIPVariable(const mlawNonLocalDamageGurson &_gursonlaw, const bool oninter=false);
  nonLocalDamageGursonDG3DIPVariable(const nonLocalDamageGursonDG3DIPVariable &source);
  nonLocalDamageGursonDG3DIPVariable& operator=(const IPVariable &source);

 /* specific function */
  IPNonLocalDamageGurson* getIPNonLocalDamageGurson(){return _nldGursonipv;}
  const IPNonLocalDamageGurson* getIPNonLocalDamageGurson() const {return _nldGursonipv;}
  virtual ~nonLocalDamageGursonDG3DIPVariable()
  {
     delete _nldGursonipv;
  }
  virtual double getEffectivePlasticStrain() const;
  virtual double getCurrentPlasticStrain() const;
  virtual double &getRefToEffectivePlasticStrain();
  virtual double &getRefToCurrentPlasticStrain();
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual const STensor3 &getConstRefToCharacteristicLengthMatrix() const 
  {
    return _nldGursonipv->getConstRefToCharacteristicLength();
  }


};

class TransverseIsotropicDG3DIPVariable : public dG3DIPVariable // or store data in a different way
{
 protected:
  IPTransverseIsotropic _tiipv;
 public:
  TransverseIsotropicDG3DIPVariable(const bool oninter=false);
  TransverseIsotropicDG3DIPVariable(const TransverseIsotropicDG3DIPVariable &source);
  TransverseIsotropicDG3DIPVariable& operator=(const IPVariable &source);

 /* specific function */
  IPTransverseIsotropic* getIPTransverseIsotropic(){return &_tiipv;}
  const IPTransverseIsotropic* getIPTransverseIsotropic() const{return &_tiipv;}
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
};

class AnisotropicDG3DIPVariable : public dG3DIPVariable // or store data in a different way
{
 protected:
  IPAnisotropic _tiipv;
 public:
  AnisotropicDG3DIPVariable(const bool oninter=false);
  AnisotropicDG3DIPVariable(const AnisotropicDG3DIPVariable &source);
  AnisotropicDG3DIPVariable& operator=(const IPVariable &source);

 /* specific function */
  IPAnisotropic* getIPAnisotropic(){return &_tiipv;}
  const IPAnisotropic* getIPAnisotropic() const{return &_tiipv;}
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
};

class LinearCohesive3DIPVariable : public IPVariableMechanics{
 protected :
  // variables use by cohesive law
  bool tension;
  double delta, deltan, deltac, deltamax, sigmac;
  SVector3 ujump0, deltat, DDeltaDjump, DDeltanDjump;
  STensor3 DDeltatDjump;
  double beta;
  double snor0;
  double tau0;

 public :
  double _fractureEnergy, _tfactor; // for fracture energy computation
  // no initial fracture (for initial fracture use ipfield)
  LinearCohesive3DIPVariable();// no initial fracture (for initial fracture use ipfield)
  ~LinearCohesive3DIPVariable(){}
  LinearCohesive3DIPVariable(const LinearCohesive3DIPVariable &source);
  virtual LinearCohesive3DIPVariable &operator = (const IPVariable &source);
  void setFracture(const LinearCohesive3DIPVariable *ipvprev,const SVector3 &ujump_, const SVector3 &n);
  void initfrac(const SVector3 &ujump_, const SVector3 &norm);
  void initmean(LinearCohesive3DIPVariable *ipv2);
  // get variable
  double getBeta() const {return beta;}
  double getDeltac() const{return deltac;}
  double getDeltan() const {return deltan;}
  const SVector3 &getDeltat() const {return deltat;}
  double getDeltamax() const{return deltamax;}
  void setDeltamax(double dm) {deltamax =dm;}
  double getDelta() const{return delta;}
  bool ifTension() const{return tension;}
  const double getSigmac() const {return sigmac;}
  const double getSNor0() const {return snor0;}
  const double getTau0() const {return tau0;}
  const SVector3 &getUjump0() const { return ujump0;}
  const SVector3 &getDDeltaDjump() const { return DDeltaDjump;}
  const SVector3 &getDDeltanDjump() const { return DDeltanDjump;}
  const STensor3 &getDDeltatDjump() const { return DDeltatDjump;}

  void setDataFromLaw(const double smax, const double snor, const double tau, const double Gc, const double beta_, const bool ift)
  {
    sigmac = smax;
    snor0  = snor;
    tau0  = tau;
    deltac = 2*Gc/sigmac;
    beta = beta_;
    tension = ift;
  }
  virtual int fractureEnergy(double* arrayEnergy) const{arrayEnergy[0]=_fractureEnergy; return 1;}
};

class NonLocalDamageLinearCohesive3DIPVariable : public LinearCohesive3DIPVariable{
 protected :
  // variables use by cohesive law

 public :
  NonLocalDamageLinearCohesive3DIPVariable();// no initial fracture (for initial fracture use ipfield)
  ~NonLocalDamageLinearCohesive3DIPVariable(){}
  NonLocalDamageLinearCohesive3DIPVariable(const NonLocalDamageLinearCohesive3DIPVariable &source);
  virtual NonLocalDamageLinearCohesive3DIPVariable &operator = (const IPVariable &source);
  void setFracture(const NonLocalDamageLinearCohesive3DIPVariable *ipvprev,const SVector3 &ujump_, const SVector3 &n);
  void initfrac(const SVector3 &ujump_, const SVector3 &norm);
  void initmean(NonLocalDamageLinearCohesive3DIPVariable *ipv2);

  void setDataFromLaw(const double smax, const double snor, const double tau, const double Gc, const double beta_, const bool ift);
};



class FractureCohesive3DIPVariable : public IPVariable2ForFracture<dG3DIPVariableBase,LinearCohesive3DIPVariable>
{
 protected:
  double _facSigmac;
 public:
  FractureCohesive3DIPVariable(const double facSigmac=1.);
  FractureCohesive3DIPVariable(const FractureCohesive3DIPVariable &source);
  virtual FractureCohesive3DIPVariable& operator=(const IPVariable &source);
  virtual ~FractureCohesive3DIPVariable(){}
  virtual const double fractureStrengthFactor() const{return _facSigmac;}
  virtual void setFractureStrengthFactor(const double fsf){_facSigmac = fsf;}

  // but you can communicate values by filling these functions for your IPVariable
  virtual int numberValuesToCommunicateMPI()const{return 1;}
  virtual void fillValuesToCommunicateMPI(double *arrayMPI)const{arrayMPI[0]=_facSigmac;}
  virtual void getValuesFromMPI(const double *arrayMPI){_facSigmac=arrayMPI[0];}

// access to bulk
  virtual const bool isInterface() const{return _ipvbulk->isInterface();}
  virtual double vonMises() const{return _ipvbulk->vonMises();}
  virtual double get(const int i) const{return _ipvbulk->get(i);}
  virtual double defoEnergy() const{return _ipvbulk->defoEnergy();}
  virtual double plasticEnergy() const{return _ipvbulk->plasticEnergy();}
  virtual int fractureEnergy(double* arrayEnergy) const{return _ipvfrac->fractureEnergy(arrayEnergy);}

  virtual void getCauchyStress(STensor3 &cauchy) const { return _ipvbulk->getCauchyStress(cauchy); }
  virtual double getJ() const { return _ipvbulk->getJ(); };


  virtual const STensor3 &getConstRefToDeformationGradient() const {return _ipvbulk->getConstRefToDeformationGradient();}
  virtual STensor3 &getRefToDeformationGradient() {return _ipvbulk->getRefToDeformationGradient();}

  virtual const STensor3 &getConstRefToFirstPiolaKirchhoffStress() const {return _ipvbulk->getConstRefToFirstPiolaKirchhoffStress();}
  virtual STensor3 &getRefToFirstPiolaKirchhoffStress() {return _ipvbulk->getRefToFirstPiolaKirchhoffStress();}

  virtual const STensor43 &getConstRefToTangentModuli() const {return _ipvbulk->getConstRefToTangentModuli();}
  virtual STensor43 &getRefToTangentModuli() {return _ipvbulk->getRefToTangentModuli();}

  virtual const STensor43 &getConstRefToElasticTangentModuli() const {return _ipvbulk->getConstRefToElasticTangentModuli();}
  virtual void setRefToElasticTangentModuli(const STensor43 &eT) {return _ipvbulk->setRefToElasticTangentModuli(eT);}

  virtual const SVector3 &getConstRefToReferenceOutwardNormal() const {return _ipvbulk->getConstRefToReferenceOutwardNormal();}
  virtual SVector3 &getRefToReferenceOutwardNormal() {return _ipvbulk->getRefToReferenceOutwardNormal();}

  virtual const SVector3 &getConstRefToCurrentOutwardNormal() const {return _ipvbulk->getConstRefToCurrentOutwardNormal();}
  virtual SVector3 &getRefToCurrentOutwardNormal() {return _ipvbulk->getRefToCurrentOutwardNormal();}

  virtual const SVector3 &getConstRefToInterfaceForce() const {return _ipvbulk->getConstRefToInterfaceForce();}
  virtual SVector3 &getRefToInterfaceForce() {return _ipvbulk->getRefToInterfaceForce();}

  virtual const STensor3 &getConstRefToDInterfaceForceDjump() const {return _ipvbulk->getConstRefToDInterfaceForceDjump();}
  virtual STensor3 &getRefToDInterfaceForceDjump() {return _ipvbulk->getRefToDInterfaceForceDjump();}

  virtual const SVector3 &getConstRefToJump() const {return _ipvbulk->getConstRefToJump();}
  virtual SVector3 &getRefToJump() {return _ipvbulk->getRefToJump();}

  virtual const double &getConstRefToNonLocalJump() const
                             {return _ipvbulk->getConstRefToNonLocalJump(); }
  virtual double &getRefToNonLocalJump() {return _ipvbulk->getRefToNonLocalJump(); }

  virtual const SVector3 &getConstRefToGradNonLocalPlasticStrain() const {
                              return _ipvbulk->getConstRefToGradNonLocalPlasticStrain();}
  virtual SVector3 &getRefToGradNonLocalPlasticStrain() {return _ipvbulk->getRefToGradNonLocalPlasticStrain();}


// access to cohesive law

  virtual void setFracture(const FractureCohesive3DIPVariable *ipvprev,const SVector3 &ujump_, const SVector3 &n) { _ipvfrac->setFracture(ipvprev->getIPvFrac(), ujump_, n); }
  void initfrac(const SVector3 &ujump_, const SVector3 &norm) {  _ipvfrac->initfrac(ujump_, norm);}
  void initmean(FractureCohesive3DIPVariable  *ipv2) {  _ipvfrac->initmean(ipv2->getIPvFrac());}
  // get variable
  double getBeta() const {return _ipvfrac->getBeta();}
  double getDeltac() const{return  _ipvfrac->getDeltac();}
  double getDeltan() const  {return  _ipvfrac->getDeltan();}
  const SVector3 &getDeltat() const  {return  _ipvfrac->getDeltat();}
  double getDeltamax() const  {return  _ipvfrac->getDeltamax();}
  void setDeltamax(double dm)  {  _ipvfrac->setDeltamax(dm);}
  double getDelta() const  {  return _ipvfrac->getDelta();}
  const SVector3 &getUjump0() const { return _ipvfrac->getUjump0();}
  const SVector3 &getDDeltaDjump() const { return _ipvfrac->getDDeltaDjump();}
  const SVector3 &getDDeltanDjump() const { return _ipvfrac->getDDeltanDjump();}
  const STensor3 &getDDeltatDjump() const { return _ipvfrac->getDDeltatDjump();}
  double& getTfactor(){ return _ipvfrac->_tfactor;}
  const double getTfactor() const { return _ipvfrac->_tfactor;}
  double& getFractureEnergy(){ return _ipvfrac->_fractureEnergy;}
  const double getFractureEnergy() const { return _ipvfrac->_fractureEnergy;}
  bool ifTension() const {  return _ipvfrac->ifTension();}
  const double getSigmac() const  { return _ipvfrac->getSigmac();}
  const double getSNor0() const {return _ipvfrac->getSNor0();}
  const double getTau0() const {return _ipvfrac->getTau0();}

  void setDataFromLaw(const double smax, const double snor, const double stau, const double Gc, const double beta_, const bool ift) {  _ipvfrac->setDataFromLaw(smax, snor, stau, Gc, beta_, ift);}

};





class ThermoMechanicsDG3DIPVariableBase : public dG3DIPVariable
{

 protected:

  double                       temperature;
  SVector3                     gradT;
  SVector3                     thermalFlux;
  STensor3                     dPdT;
  STensor3                     dthermalFluxdgradT;
  const STensor3               *linearK;
 // for dg: jump in non local epl
  double                       temperatureJump;
  SVector3                     interfaceFlux;

 public:
  ThermoMechanicsDG3DIPVariableBase(const bool oninter=false);
  ThermoMechanicsDG3DIPVariableBase(const ThermoMechanicsDG3DIPVariableBase &source);
  ThermoMechanicsDG3DIPVariableBase& operator=(const IPVariable &source);

  virtual ~ThermoMechanicsDG3DIPVariableBase()
  {
  }
  virtual double defoEnergy() const=0;
  virtual double plasticEnergy() const=0;
  virtual double get(const int i) const=0;

  // Archiving data to be changed
    virtual const double &getConstRefTotemperature() const {return  temperature;}
  virtual double &getRefTotemperature() {return  temperature;}

  virtual const SVector3 &getConstRefTogradT() const {return gradT;}
  virtual SVector3 &getRefTogradT() {return gradT;}

  virtual const SVector3 &getConstRefTothermalFlux() const {return thermalFlux;}
  virtual SVector3 &getRefTothermalFlux() {return thermalFlux;}
  
  virtual const  STensor3 &getConstRefTodPdT() const {return dPdT;}
  virtual  STensor3 &getRefTodPdT() {return dPdT;}
  
  virtual const  STensor3 &getConstRefTodthermalFluxdgradT() const {return  dthermalFluxdgradT;}
  virtual  STensor3 &getRefTodthermalFluxdgradT() {return  dthermalFluxdgradT;}
  
 
  //question   is it right ??
  virtual const STensor3 &getConstRefTolinearK() const {return *linearK;}
 virtual void setRefTolinearK(const STensor3 &eT) {linearK=&eT;}




  virtual const double &getConstRefToTemperatureJump() const {return temperatureJump;}
  virtual double &getRefToTemperatureJump() {return  temperatureJump;}
  
  virtual const SVector3 &getConstRefTointerfaceFlux() const {return interfaceFlux;}
  virtual SVector3 &getRefTointerfaceFlux() {return interfaceFlux;}

};


class LinearThermoMechanicsDG3DIPVariable : public ThermoMechanicsDG3DIPVariableBase
{

 protected:
  IPLinearThermoMechanics _linearTMIP;

 public:
  LinearThermoMechanicsDG3DIPVariable(const bool oninter=false);
  LinearThermoMechanicsDG3DIPVariable(const LinearThermoMechanicsDG3DIPVariable &source);
  LinearThermoMechanicsDG3DIPVariable& operator=(const IPVariable &source);

 /* specific function */
  IPLinearThermoMechanics* getIPLinearThermoMechanics(){return &_linearTMIP;}
  const IPLinearThermoMechanics* getIPLinearThermoMechanics() const{return &_linearTMIP;}
  virtual ~LinearThermoMechanicsDG3DIPVariable()
  {
  }
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;


};
#endif //DG3DIPVARIABLE
