//
//
// Description: Class of materials for non linear dg shell
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef _DG3DMATERIALLAW_H_
#define _DG3DMATERIALLAW_H_
#ifndef SWIG
#include "dG3DIPVariable.h"
#include "mlawJ2linear.h"
#include "mlawNonLocalDamageJ2Hyper.h"
#include "mlawNonLocalDamageGurson.h"
#include "mlawVUMATinterface.h"
#include "mlawTransverseIsotropic.h"
#include "mlawAnisotropic.h"
#include "mlawNonLocalDamage.h"
#include "nonLocalDamageLaw.h"
#include "j2IsotropicHardening.h"
#include "GursonDamageNucleation.h"
#include "mlawLinearThermoMechanics.h"
#endif
// non linear shell material law must have specific function. (eg ensure plane stress by newton-raphson iteration)
class dG3DMaterialLaw : public materialLaw{
 public:

  STensor43 elasticStiffness;

protected:
  double _rho;

#ifndef SWIG
 public:
  dG3DMaterialLaw(const int num, const double rho,
                   const bool init=true) : materialLaw(num,init), _rho(rho)
  {

  }
  dG3DMaterialLaw(const dG3DMaterialLaw &source) : materialLaw(source),
                                                      _rho(source._rho), elasticStiffness(source.elasticStiffness)
  {
     
  }
  ~dG3DMaterialLaw()
  {
  }
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const=0;
  // To allow initialization of bulk ip in case of fracture
  virtual void createIPVariable(IPVariable* &ipv,const MElement *ele=NULL, const int nbFF_=0) const=0;
  virtual void calledBy2lawFracture(){}
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw)=0;
  virtual double density() const{return _rho;}
  // As to be defined for explicit scheme. If law for implicit only
  // please define this function with return 0
  virtual double soundSpeed() const=0;
  // If law enable fracture the bool checkfrac allow to detect fracture. This bool is unused otherwise
  // The bool is necessery for computation of matrix by perturbation in this case the check of fracture
  // has not to be performed.
  virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true)=0;

  virtual void fillElasticStiffness(double E, double nu, STensor43 &tangent) const;
  virtual void setElasticStiffness(IPVariable* ipv){}
#endif // SWIG
};

class Cohesive3DLaw : public materialLaw{
 protected :
  // All cohesive law must have at least these four variable
  const double _Gc;
  const double _sigmac;
  const double _beta;
  const double _mu;
  const double _fscmin,_fscmax; // Boundary value for uncertainty on sigma_c (default min = max = 1)
#ifndef SWIG
 public :
  Cohesive3DLaw(const int num, const double Gc, const double sigmac, const double beta, const double mu,
                   const double fscmin=1.,const double fscmax=1.,const bool init=true);
  Cohesive3DLaw(const Cohesive3DLaw &source);
  ~Cohesive3DLaw(){}
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw)=0;
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const=0;
  virtual void createIPVariable(IPVariable* &ipv,const MElement *ele=NULL, const int nbFF_=0) const=0;
  // get operation
  double getGc() const{return _Gc;}
  double getSigmac() const{return _sigmac;}
  double getBeta() const{return _beta;}
  double getMu() const{return _mu;}
  double getFractureStrengthFactorMin() const{return _fscmin;}
  double getFractureStrengthFactorMax() const{return _fscmax;}
  // onto this form to compute easily matrix by perturbation
  virtual matname getType() const{return materialLaw::cohesiveLaw;}
  virtual const bool checkBroken(dG3DIPVariableBase *ipvcur, const dG3DIPVariableBase *ipvprev) const=0;
  virtual double soundSpeed() const
  {
    Msg::Warning("Use a cohesive law to determine sound speed ?? value =1");
    return 1;
  }
  // default for cohesive law full broken if delta > delta_c
  virtual bool fullBroken(const LinearCohesive3DIPVariable *ipv) const
  {
    if(ipv->getDelta() >= ipv->getDeltac() )  return true;
    return false;
  }
  virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true)=0;

#endif
};

class LinearCohesive3DLaw : public Cohesive3DLaw
{
 protected:
  const double _betaexpm2;
 public:
  LinearCohesive3DLaw(const int num, const double Gc, const double sigmac, const double beta, const double mu,
                         const double fractureStrengthFactorMin = 1., const double fractureStrengthFactorMax = 1.);
#ifndef SWIG
  LinearCohesive3DLaw(const LinearCohesive3DLaw &source);
  ~LinearCohesive3DLaw(){}
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){} // nothing to do for this law
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv,const MElement *ele=NULL, const int nbFF_=0) const;
  virtual const bool checkBroken(dG3DIPVariableBase *ipvcur, const dG3DIPVariableBase *ipvprev) const;
  virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true);

#endif
};


class J2LinearDG3DMaterialLaw : public dG3DMaterialLaw
{
 protected:
  mlawJ2linear _j2law;
  //double _E,_nu;
 private: // cache data
  //mutable STensor43 _tangent3D;
  //const STensor3 _ISTensor3;
  //mutable STensor3 tau,PKxyz,PKsh,invF,tauXYZ,Fp1,Fp0;
 public:
  J2LinearDG3DMaterialLaw(const int num,const double rho,
                   double E,const double nu,const double sy0,const double h,const double tol=1.e-6);
  J2LinearDG3DMaterialLaw(const int num,const double rho,
                   double E,const double nu,const J2IsotropicHardening &_j2IH,const double tol=1.e-6);
 #ifndef SWIG
  J2LinearDG3DMaterialLaw(const J2LinearDG3DMaterialLaw &source);
  virtual ~J2LinearDG3DMaterialLaw()
  {
    
  }
  // set the time of _j2law
   virtual void setTime(const double t,const double dtime){
    this->_timeStep = dtime;
    this->_currentTime = t;
    _j2law.setTime(t,dtime);
  }
  virtual materialLaw::matname getType() const {return _j2law.getType();}
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv,const MElement *ele=NULL, const int nbFF_=0) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const{return _j2law.soundSpeed();} // or change value ??
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true);

 #endif // SWIG
};


class VUMATinterfaceDG3DMaterialLaw : public dG3DMaterialLaw
{

 protected:
  mlawVUMATinterface _vumatlaw;

 public:
  VUMATinterfaceDG3DMaterialLaw(const int num, const double rho,
                                const char *propName);
  ~VUMATinterfaceDG3DMaterialLaw() {};

#ifndef SWIG
  VUMATinterfaceDG3DMaterialLaw(const VUMATinterfaceDG3DMaterialLaw &source);
  // set the time of _vumatlaw
  virtual void setTime(const double t,const double dtime){
    this->_timeStep = dtime;
    this->_currentTime = t;
    _vumatlaw.setTime(t,dtime);
  }
  virtual materialLaw::matname getType() const {return _vumatlaw.getType();}
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv,const MElement *ele=NULL, const int nbFF_=0) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const{return _vumatlaw.soundSpeed();}
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true);
#endif
};

class TransverseIsotropicDG3DMaterialLaw : public dG3DMaterialLaw
{
 protected:
  mlawTransverseIsotropic _tilaw;
  double _E,_nu, _EA, _GA, _nu_minor;
 private: // cache data
  mutable STensor43 _tangent3D;
  const STensor3 _ISTensor3;
  mutable STensor3 tau,PKxyz,PKsh,invF,tauXYZ,Fp1,Fp0;
 public:
  TransverseIsotropicDG3DMaterialLaw(const int num,const double rho,
                   double E,const double nu, const double EA,
		   const double GA, const double nu_minor,
                   const double Ax, const double Ay, const double Az);
 #ifndef SWIG
  TransverseIsotropicDG3DMaterialLaw(const TransverseIsotropicDG3DMaterialLaw &source);
  virtual ~TransverseIsotropicDG3DMaterialLaw(){}
  // set the time of _tilaw
   virtual void setTime(const double t,const double dtime){
    this->_timeStep = dtime;
    this->_currentTime = t;
    _tilaw.setTime(t,dtime);
  }
  virtual materialLaw::matname getType() const {return _tilaw.getType();}
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv,const MElement *ele=NULL, const int nbFF_=0) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const{return _tilaw.soundSpeed();} // or change value ??
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true);

 #endif // SWIG
};


class AnisotropicDG3DMaterialLaw : public dG3DMaterialLaw
{
 protected:
  mlawAnisotropic _tilaw;
  std::vector<double> _ElasticityTensor;
 private: // cache data
  mutable STensor43 _tangent3D;
  const STensor3 _ISTensor3;
  mutable STensor3 tau,PKxyz,PKsh,invF,tauXYZ,Fp1,Fp0;
 public:
  AnisotropicDG3DMaterialLaw(const int num,const double rho,
				const double Ex,const double Ey,const double Ez,
				const double Vxy,const double Vxz,const double Vyz,
				const double MUxy,const double MUxz,const double MUyz,
                   		const double alpha, const double beta, const double gamma);
 #ifndef SWIG
  AnisotropicDG3DMaterialLaw(const AnisotropicDG3DMaterialLaw &source);
  virtual ~AnisotropicDG3DMaterialLaw(){}
  // set the time of _tilaw
   virtual void setTime(const double t,const double dtime){
    this->_timeStep = dtime;
    this->_currentTime = t;
    _tilaw.setTime(t,dtime);
  }
  virtual materialLaw::matname getType() const {return _tilaw.getType();}
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv,const MElement *ele=NULL, const int nbFF_=0) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const{return _tilaw.soundSpeed();} // or change value ??
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true);

 #endif // SWIG
};


class NonLocalDamageDG3DMaterialLaw : public dG3DMaterialLaw
{

 protected:
  materialLawWithDamage *_nldlaw; // pointer to allow to choose between LLN style or Gmsh Style. The choice is performed by the constructor (2 constructors with != arguments)

 public:
  NonLocalDamageDG3DMaterialLaw(const int num, const double rho,
                                const char *propName);
  ~NonLocalDamageDG3DMaterialLaw() {delete _nldlaw;};

#ifndef SWIG
  NonLocalDamageDG3DMaterialLaw(const NonLocalDamageDG3DMaterialLaw &source);
  // set the time of _nldlaw
  virtual void setTime(const double t,const double dtime){
    this->_timeStep = dtime;
    this->_currentTime = t;
    _nldlaw->setTime(t,dtime);
  }
  virtual materialLaw::matname getType() const {return _nldlaw->getType();}
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv,const MElement *ele=NULL, const int nbFF_=0) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const{return _nldlaw->soundSpeed();} // or change value ??
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true);
#endif
};

class NonLocalDamageJ2HyperDG3DMaterialLaw : public dG3DMaterialLaw
{

 protected:
  mlawNonLocalDamageJ2Hyper *_nldJ2Hyperlaw; // pointer to allow to choose between LLN style or Gmsh Style. The choice is performed by the constructor (2 constructors with != arguments)

 public:
  NonLocalDamageJ2HyperDG3DMaterialLaw(const int num,const double rho,
                   double E,const double nu,const J2IsotropicHardening &_j2IH,
                   const CLengthLaw &_cLLaw,const DamageLaw &_damLaw,const double tol=1.e-6);
  ~NonLocalDamageJ2HyperDG3DMaterialLaw() {delete _nldJ2Hyperlaw;};

#ifndef SWIG
  NonLocalDamageJ2HyperDG3DMaterialLaw(const NonLocalDamageJ2HyperDG3DMaterialLaw &source);
  // set the time of _nldlaw
  virtual void setTime(const double t,const double dtime){
    this->_timeStep = dtime;
    this->_currentTime = t;
    _nldJ2Hyperlaw->setTime(t,dtime);
  }
  virtual materialLaw::matname getType() const {return _nldJ2Hyperlaw->getType();}
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv,const MElement *ele=NULL, const int nbFF_=0) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const{return _nldJ2Hyperlaw->soundSpeed();} // or change value ??
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true);
#endif
};

class NonLocalDamageGursonDG3DMaterialLaw : public dG3DMaterialLaw
{

 protected:
  mlawNonLocalDamageGurson *_nldGursonlaw; // pointer to allow to choose between LLN style or Gmsh Style. The choice is performed by the constructor (2 constructors with != arguments)

 public:
  NonLocalDamageGursonDG3DMaterialLaw(const int num,const double rho,
                   const double E,const double nu,
		   const double q1, const double q2, const double q3, const double fC, 
                   const double ff, const double ffstar, const double fVinitial,
                   const J2IsotropicHardening &_j2IH,
                   const CLengthLaw &_cLLaw, const GursonDamageNucleation &_gdn,const double tol=1.e-6);

  ~NonLocalDamageGursonDG3DMaterialLaw() {delete _nldGursonlaw;};

#ifndef SWIG
  NonLocalDamageGursonDG3DMaterialLaw(const NonLocalDamageGursonDG3DMaterialLaw &source);
  // set the time of _nldlaw
  virtual void setTime(const double t,const double dtime){
    this->_timeStep = dtime;
    this->_currentTime = t;
    _nldGursonlaw->setTime(t,dtime);
  }
  virtual materialLaw::matname getType() const {return _nldGursonlaw->getType();}
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv,const MElement *ele=NULL, const int nbFF_=0) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const{return _nldGursonlaw->soundSpeed();} // or change value ??
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true);
#endif
};


class NonLocalDamageLinearCohesive3DLaw : public LinearCohesive3DLaw
{
 protected:
  const double _Dc;
 public:
  NonLocalDamageLinearCohesive3DLaw(const int num, const double Gc, const double sigmac, const double Dc, const double beta, 
                         const double fractureStrengthFactorMin = 1., const double fractureStrengthFactorMax = 1.);
#ifndef SWIG
  NonLocalDamageLinearCohesive3DLaw(const NonLocalDamageLinearCohesive3DLaw &source);
  ~NonLocalDamageLinearCohesive3DLaw(){}
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){} // nothing to do for this law
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv,const MElement *ele=NULL, const int nbFF_=0) const;
  virtual const bool checkBroken(dG3DIPVariableBase *ipvcur, const dG3DIPVariableBase *ipvprev) const;
  virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true);

#endif
};

class DelaminationNonLocalDamageLinearCohesive3DLaw : public NonLocalDamageLinearCohesive3DLaw
{
  public:
    DelaminationNonLocalDamageLinearCohesive3DLaw(const int num, const double Gc, const double Dc, const double beta,
			                             const double fractureStrengthFactorMin = 1., const double fractureStrengthFactorMax = 1.);
#ifndef SWIG
    DelaminationNonLocalDamageLinearCohesive3DLaw(const DelaminationNonLocalDamageLinearCohesive3DLaw &source);
   ~DelaminationNonLocalDamageLinearCohesive3DLaw(){}
   virtual const bool checkBroken(dG3DIPVariableBase *ipvcur, const dG3DIPVariableBase *ipvprev) const;
#endif
};



class FractureByCohesive3DLaw : public dG3DMaterialLaw, public  fractureBy2Laws<dG3DMaterialLaw,Cohesive3DLaw>
{
 public:
  FractureByCohesive3DLaw(const int num,const int nbulk, const int ncoh);
#ifndef SWIG
  FractureByCohesive3DLaw(const FractureByCohesive3DLaw &source);
  ~FractureByCohesive3DLaw(){}
  virtual matname getType() const{return materialLaw::fracture;}
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv,const MElement *ele=NULL, const int nbFF_=0) const;
  virtual void initialBroken(IPStateBase *ips) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw)
  {
    if(!_initialized){
      fractureBy2Laws<dG3DMaterialLaw,Cohesive3DLaw>::initLaws(maplaw);
      _initialized = true;
    }
    _mbulk->calledBy2lawFracture();
  }
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true);
  virtual bool fullBroken(const IPVariable *ipv) const
  {
    const LinearCohesive3DIPVariable *ipvc = (static_cast<const FractureCohesive3DIPVariable*>(ipv))->getIPvFrac();
    if(ipvc !=NULL)  return _mfrac->fullBroken(ipvc); // not initialized --> no fracture
    return false;
  }
  virtual double density() const{return _mbulk->density();}
  virtual double soundSpeed() const{return _mbulk->soundSpeed();}
#endif // SWIG
};


class LinearThermoMechanicsDG3DMaterialLaw : public dG3DMaterialLaw // public materialLaw
{

 protected:
  mlawLinearThermoMechanics *_lawLinearTM; // pointer to allow to choose between LLN style or Gmsh Style. The choice is performed by the constructor (2 constructors with != arguments)

  STensor3                  linearK;
 public:
  LinearThermoMechanicsDG3DMaterialLaw(const int num,const double rho, const double Ex, const double Ey, const double Ez, 
						  const double Vxy, const double Vxz, const double Vyz,
						  const double MUxy, const double MUxz, const double MUyz,
			 			  const double alpha, const double beta, const double gamma  ,
			                   const double cv,const double t0,const double Kx,const double Ky,
			                  const double Kz,const double alphax,const double alphay,const double alphaz);
  ~LinearThermoMechanicsDG3DMaterialLaw() {delete _lawLinearTM;};

#ifndef SWIG
 LinearThermoMechanicsDG3DMaterialLaw(const LinearThermoMechanicsDG3DMaterialLaw &source);
  // set the time of _nldlaw
  virtual void setTime(const double t,const double dtime){
    this->_timeStep = dtime;
    this->_currentTime = t;
    _lawLinearTM->setTime(t,dtime);
  }
  virtual materialLaw::matname getType() const {return _lawLinearTM->getType();}
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv,const MElement *ele=NULL, const int nbFF_=0) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const{return _lawLinearTM->soundSpeed();} // or change value ??
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true, const bool checkfrac=true);
#endif
};

#endif
