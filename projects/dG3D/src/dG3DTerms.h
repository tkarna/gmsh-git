//
// C++ Interface: terms
//
// Description: Class of terms for dg
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef DG3DTERMS_H
#define DG3DTERMS_H
#include "SVector3.h"
#include <string>
#include "dG3DFunctionSpace.h"
#include "dG3DMaterialLaw.h"
#include "simpleFunction.h"
#include "unknownField.h"
#include "ipField.h"
#include "terms.h"
#include "interfaceQuadrature.h"



template<class T1,class T2> class g3DBilinearTerm : public BiNonLinearTermBase
{
 protected :
  FunctionSpace<T1>& space1;
  FunctionSpace<T2>& space2;
 public :
  g3DBilinearTerm(FunctionSpace<T1>& space1_,FunctionSpace<T2>& space2_) : space1(space1_),space2(space2_) {}
  virtual ~g3DBilinearTerm() {}
};

template<class T1> class g3DLinearTerm : public nonLinearTermBase<double>
{
 protected :
  FunctionSpace<T1>& space1;
 public :
  g3DLinearTerm(FunctionSpace<T1>& space1_) : space1(space1_){}
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me get by integration point massC0DgShell");
  }

  virtual ~g3DLinearTerm() {}
};

template<class T1> class g3DLoadTerm : public LinearTerm<T1>
{
 protected:
  simpleFunction<typename TensorialTraits<T1>::ValType> *Load;
  // Data of get function (Allocated once)
  mutable SPoint3 p;
  mutable typename TensorialTraits<T1>::ValType load;
 public :
  g3DLoadTerm(FunctionSpace<T1>& space1_,
               simpleFunction<typename TensorialTraits<T1>::ValType> *Load_) : LinearTerm<T1>(space1_),Load(Load_)
  {

  }
  virtual ~g3DLoadTerm() {}

  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &mv) const
  {
    Msg::Error("Define me get by integration point g3DLoadTerm");
  }
  virtual void get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const
  {
    const int nbdof=this->space1.getNumKeys(ele);
    m.resize(nbdof);
    m.scale(0.);
    // Get shape functions values and gradients at all Gauss points
    nlsFunctionSpaceXYZ<double>* sp1 = static_cast<nlsFunctionSpaceXYZ<double>*>(&(this->space1));
    std::vector<std::vector<TensorialTraits<double>::ValType> >vvals;
    std::vector<std::vector<TensorialTraits<double>::GradType> >vgradsuvw;
    sp1->get(ele,npts,GP,vvals);
    sp1->getgradfuvw(ele,npts,GP,vgradsuvw);
    double jac[3][3];
    for (int i = 0; i < npts; i++)
    {
      const double weight = GP[i].weight;
 //     Vals.clear();
 //     LinearTerm<T1>::space1.f(ele,u, v, w, Vals);
      std::vector<typename TensorialTraits<T1>::ValType> &Vals = vvals[i];
      // not optimal as evertything is computed (especially if hessian and vthird are true but it should be false for Neumann space) but better than before...
      const double detJ = ele->getJacobian(vgradsuvw[i],jac);
      ele->pnt(Vals,p);
      load=Load->operator()(p.x(),p.y(),p.z());
      double loadvalue = load*weight*detJ;
      for (int j = 0; j < nbdof ; ++j)
        m(j)+=Vals[j]*loadvalue;
    }
  }
  virtual LinearTermBase<double>* clone () const
  {
    return new g3DLoadTerm(this->space1,Load);
  }
};

class dG3DForceBulk : public g3DLinearTerm<double>
{
 protected:
  const dG3DMaterialLaw* _mlaw;
  const IPField *_ipf;
  const bool _fullDg;
  mutable const dG3DIPVariableBase* vipv[256]; // max 256 Gauss' point ??
  mutable const dG3DIPVariableBase* vipvprev[256];

 public:
  dG3DForceBulk(FunctionSpace<double>& space1_,const materialLaw* mlaw,bool FullDG,
                                       const IPField *ip) : g3DLinearTerm<double>(space1_),
                                                                                 _fullDg(FullDG),
                                                                                 _ipf(ip),
                                                                                 _mlaw(static_cast<const dG3DMaterialLaw*>(mlaw))
  {

  }
  virtual ~dG3DForceBulk(){}
  virtual void get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &mv) const
  {
    Msg::Error("Define me get by integration point dG3DForceBulk");
  }

  virtual const bool isData() const {return false;}
  virtual void set(const fullVector<double> *datafield){}
  virtual LinearTermBase<double>* clone () const
  {
    return new dG3DForceBulk(space1,_mlaw,_fullDg,_ipf);
  }
};

class dG3DStiffnessBulk : public BilinearTerm<double,double>
{
 protected:
  const dG3DMaterialLaw* _mlaw;
  const IPField *_ipf;
  const bool _fullDg;
  bool sym;
  mutable const dG3DIPVariableBase* vipv[256]; // max 256 Gauss' point ??
 public:
  dG3DStiffnessBulk(FunctionSpace<double>& space1_,FunctionSpace<double>& space2_,materialLaw* mlaw,
                                  bool FullDG, IPField *ip) : BilinearTerm<double,double>(space1_,space2_),
                                                            _fullDg(FullDG), _ipf(ip),
                                                            _mlaw(static_cast<dG3DMaterialLaw*>(mlaw))

  {
    sym=(&space1_==&space2_);
  }
  dG3DStiffnessBulk(FunctionSpace<double>& space1_, const materialLaw* mlaw,
                                  bool FullDG,const IPField *ip) : BilinearTerm<double,double>(space1_,space1_),
                                                             _fullDg(FullDG), _ipf(ip),
                                                             _mlaw(static_cast<const dG3DMaterialLaw*>(mlaw))
  {
    sym=true;
  }
  virtual ~dG3DStiffnessBulk(){}
  virtual void get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me get by gauss point dG3DStiffnessBulk");
  }
  virtual BilinearTermBase* clone () const
  {
    return new dG3DStiffnessBulk(space1,_mlaw,_fullDg,_ipf);
  }
};

class dG3DForceInter : public g3DLinearTerm<double>
{
 protected:
  // To get displacement
  const fullVector<double> *_data;
  nlsFunctionSpaceXYZ<double> *_minusSpace;
  nlsFunctionSpaceXYZ<double> *_plusSpace;
  const dG3DMaterialLaw *_mlawMinus;
  const dG3DMaterialLaw *_mlawPlus;
  interfaceQuadratureBase *_interQuad;
  const IPField *_ipf;
  double _beta1;
  const bool _fullDg;
 public:
  dG3DForceInter(FunctionSpace<double>& space1_, FunctionSpace<double> *space2_,
                                       const materialLaw *mlawMinus, const materialLaw *mlawPlus,interfaceQuadratureBase *iquad,
                                       double beta1_,
                                       bool fdg, const IPField *ip) : g3DLinearTerm<double>(space1_),
                                                                _minusSpace(static_cast<nlsFunctionSpaceXYZ<double>*>(&space1_)),
                                                                _plusSpace(static_cast<nlsFunctionSpaceXYZ<double>*>(space2_)),
                                                                _beta1(beta1_),
                                                                _ipf(ip),
                                                                _mlawMinus(static_cast<const dG3DMaterialLaw*>(mlawMinus)),
                                                                _mlawPlus(static_cast<const dG3DMaterialLaw*>(mlawPlus)),
                                                                _interQuad(iquad),_fullDg(fdg)
  {
  }
  virtual ~dG3DForceInter() {}
  virtual const bool isData() const{return true;}
  virtual void set(const fullVector<double> *datafield){_data = datafield;}
  virtual void get(MElement *ele, int npts, IntPt *GP, fullVector<double> &v)const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &mv) const
  {
    Msg::Error("Define me get by integration point dG3DForceInter");
  }
  virtual LinearTermBase<double>* clone () const
  {
    return new dG3DForceInter(*_minusSpace,_plusSpace,_mlawMinus,_mlawPlus,_interQuad,_beta1,_fullDg,_ipf);
  }
};

class dG3DStiffnessInter : public BilinearTerm<double,double>
{
 protected:
  double _beta1;
  const fullVector<double> *_data;
  nlsFunctionSpaceXYZ<double> *_minusSpace;
  nlsFunctionSpaceXYZ<double> *_plusSpace;
  const dG3DMaterialLaw *_mlawMinus;
  const dG3DMaterialLaw *_mlawPlus;
  interfaceQuadratureBase *_interQuad;
  unknownField *_ufield; // used for fracture. In that case the matrix is computed by perturbation
  const IPField *_ipf;
  const bool _fullDg;
  const double _perturbation;
 public:
  dG3DStiffnessInter(FunctionSpace<double>& space1_,FunctionSpace<double>& space2_,
                                    const materialLaw* mlawMinus, const materialLaw* mlawPlus,interfaceQuadratureBase *iquad,
                                    double beta1_, const IPField *ip, unknownField *uf,
                                    bool fulldg,double eps=1.e-6) : BilinearTerm<double,double>(space1_,space2_),
                                                                     _minusSpace(static_cast<nlsFunctionSpaceXYZ<double>*>(&space1_)),
                                                                     _plusSpace(static_cast<nlsFunctionSpaceXYZ<double>*>(&space2_)),
                                                                     _beta1(beta1_),
                                                                     _ipf(ip), _ufield(uf),
                                                                     _mlawMinus(static_cast<const dG3DMaterialLaw*>(mlawMinus)),
                                                                     _mlawPlus(static_cast<const dG3DMaterialLaw*>(mlawPlus)),
                                                                     _interQuad(iquad),_fullDg(fulldg), _perturbation(eps){}
  virtual ~dG3DStiffnessInter() {}
  virtual void get(MElement *ele,int npts,IntPt *GP, fullMatrix<double> &m)const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me ?? get by integration point dG3DStiffnessInter");
  }
  virtual BilinearTermBase* clone () const
  {
    return new dG3DStiffnessInter(space1,space2,_mlawMinus,_mlawPlus,_interQuad,_beta1,
                                              _ipf,_ufield,_fullDg,_perturbation);
  }
};

template<class T1> class nonLocalG3DLoadTerm : public g3DLoadTerm<T1>
{
 public :
  nonLocalG3DLoadTerm(FunctionSpace<T1>& space1_,
               simpleFunction<typename TensorialTraits<T1>::ValType> *Load_) : g3DLoadTerm<T1>(space1_,Load_)
  {

  }
  virtual ~nonLocalG3DLoadTerm() {}

  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &mv) const
  {
    Msg::Error("Define me get by integration point nonLocalG3DLoadTerm");
  }
  virtual void get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const
  {
    const int nbdof=this->space1.getNumKeys(ele);
    int nbFF = ele->getNumShapeFunctions();
    m.resize(nbdof);
    m.scale(0.);
    // get the values of shape functions and detJ at all Gauss points
    nlsFunctionSpaceXYZ<double> *sp1 = static_cast<nlsFunctionSpaceXYZ<double>*>(&(this->space1));
    std::vector<std::vector<TensorialTraits<double>::ValType> > vvals;
    std::vector<std::vector<TensorialTraits<double>::GradType> > vgradsuvw;
    sp1->get(ele,npts,GP,vvals);
    sp1->getgradfuvw(ele,npts,GP,vgradsuvw);
    double jac[3][3];
    for (int i = 0; i < npts; i++)
    {
      const double weight = GP[i].weight;
      //g3DLoadTerm<T1>::Vals.clear();
      //LinearTerm<T1>::space1.f(ele,u,v,w, g3DLoadTerm<T1>::Vals);
      std::vector<TensorialTraits<double>::ValType> & Vals = vvals[i];
      const double detJ = ele->getJacobian(vgradsuvw[i],jac); //ele->getJacobian(u,v,w,jac);
      ele->pnt(Vals,g3DLoadTerm<T1>::p);
      g3DLoadTerm<T1>::load=g3DLoadTerm<T1>::Load->operator()(g3DLoadTerm<T1>::p.x(),g3DLoadTerm<T1>::p.y(),
                                                               g3DLoadTerm<T1>::p.z());
      double loadvalue = g3DLoadTerm<T1>::load*weight*detJ;
      for (int j = 0; j < nbdof ; ++j)
         m(j)+=Vals[j]*loadvalue;
	     
      //for (int j = 0; j < nbFF ; ++j)
      //{
        //m(j) +=Vals[j]*loadvalue;
        //m(j+nbFF)+=Vals[j]*loadvalue;
        //m(j+2*nbFF)+=Vals[j]*loadvalue;
        //m(j+3*nbFF)+=0;
      //}
    }
  }
  virtual LinearTermBase<double>* clone () const
  {
    return new nonLocalG3DLoadTerm(this->space1,g3DLoadTerm<T1>::Load);
  }
};

// elementary mass matrix
class nonLocalMassG3D : public mass3D{
 public:
  nonLocalMassG3D(FunctionSpace<double> &space1_,
                materialLaw *mlaw): mass3D(space1_,mlaw)
  {
  }
  nonLocalMassG3D(FunctionSpace<double> &space1_, FunctionSpace<double> &space2_,
                 materialLaw *mlaw): mass3D(space1_,space2_,mlaw)
  {
  }
  nonLocalMassG3D(FunctionSpace<double> &space1_, FunctionSpace<double> &space2_,
                 double rho): mass3D(space1_,space2_,rho)
  {
  }
  virtual void get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me get by integration point nonLocalMassG3D");
  }
  virtual BilinearTermBase* clone () const
  {
    return new nonLocalMassG3D(space1,space2,_rho);
  }
};

class nonLocalDG3DForceBulk : public dG3DForceBulk
{
 protected:
  const fullVector<double> *_data;
  double _nonLocalEqRatio;

 public:
  nonLocalDG3DForceBulk(FunctionSpace<double>& space1_,const materialLaw* mlaw,bool FullDG,
                                       const IPField *ip, const double nonLocalEqRatio) : dG3DForceBulk(space1_,mlaw,FullDG,ip), _nonLocalEqRatio(nonLocalEqRatio)
  {

  }
  virtual ~nonLocalDG3DForceBulk(){}
  virtual void get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const;
  virtual const bool isData() const {return true;} //bareps appears in internal forces, so should be perturbated
  virtual void set(const fullVector<double> *datafield){_data = datafield;}
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &mv) const
  {
    Msg::Error("Define me get by integration point nonLocalDG3DForceBulk");
  }

  virtual LinearTermBase<double>* clone () const
  {
    return new nonLocalDG3DForceBulk(space1,_mlaw,_fullDg,_ipf,_nonLocalEqRatio);
  }
};

class nonLocalDG3DStiffnessBulk : public dG3DStiffnessBulk
{
  double _nonLocalEqRatio;
 public:
  nonLocalDG3DStiffnessBulk(FunctionSpace<double>& space1_,FunctionSpace<double>& space2_,materialLaw* mlaw,
                                  bool FullDG, IPField *ip, const double nonLocalEqRatio) : dG3DStiffnessBulk(space1_,space2_,mlaw,FullDG,ip), _nonLocalEqRatio(nonLocalEqRatio)

  {

  }
  nonLocalDG3DStiffnessBulk(FunctionSpace<double>& space1_, const materialLaw* mlaw,
                                  bool FullDG,const IPField *ip, const double nonLocalEqRatio) : dG3DStiffnessBulk(space1_,mlaw,FullDG,ip), _nonLocalEqRatio(nonLocalEqRatio)
  {
  }
  virtual ~nonLocalDG3DStiffnessBulk(){}
  virtual void get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me get by gauss point nonLocalDG3DStiffnessBulk");
  }
  virtual BilinearTermBase* clone () const
  {
    return new nonLocalDG3DStiffnessBulk(space1,_mlaw,_fullDg,_ipf,_nonLocalEqRatio);
  }
};

class nonLocalDG3DForceInter : public dG3DForceInter
{
 protected:
  double _nonLocalBeta;
  bool _continuity;
  double _nonLocalEqRatio;
 public:
  nonLocalDG3DForceInter(FunctionSpace<double>& space1_, FunctionSpace<double> *space2_,
                                       const materialLaw *mlawMinus, const materialLaw *mlawPlus,interfaceQuadratureBase *iquad,
                                       double beta1_,
                                       bool fdg, const IPField *ip, double nonLocalBeta_,
                                       bool continuity_, double nonLocalEqRatio ) : dG3DForceInter( space1_, space2_,
                                       mlawMinus, mlawPlus,iquad,beta1_, fdg, ip), _nonLocalBeta(nonLocalBeta_),
                                       _continuity(continuity_), _nonLocalEqRatio(nonLocalEqRatio)
  {
  }
  virtual ~nonLocalDG3DForceInter() {}
  virtual void get(MElement *ele, int npts, IntPt *GP, fullVector<double> &v)const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &mv) const
  {
    Msg::Error("Define me get by integration point nonLocalDG3DForceInter");
  }
  virtual LinearTermBase<double>* clone () const
  {
    return new nonLocalDG3DForceInter(*_minusSpace,_plusSpace,_mlawMinus,_mlawPlus,_interQuad,_beta1,_fullDg,_ipf,
						 _nonLocalBeta,_continuity,_nonLocalEqRatio);
  }
};

class nonLocalDG3DStiffnessInter : public dG3DStiffnessInter
{
  double _nonLocalBeta;
  bool _continuity;
  double _nonLocalEqRatio;
 public:
  nonLocalDG3DStiffnessInter(FunctionSpace<double>& space1_,FunctionSpace<double>& space2_,
                                    const materialLaw* mlawMinus, const materialLaw* mlawPlus,interfaceQuadratureBase *iquad,
                                    double beta1_, const IPField *ip, unknownField *uf,
                                    bool fulldg,  double nonLocalBeta_, bool continuity_, double nonLocalEqRatio, double eps=1.e-6) :
                                    dG3DStiffnessInter(space1_,space2_,
                                          mlawMinus, mlawPlus,iquad,beta1_, ip, uf,fulldg,eps), _nonLocalBeta(nonLocalBeta_),
                                       _continuity(continuity_), _nonLocalEqRatio(nonLocalEqRatio)
  {

  }
  virtual ~nonLocalDG3DStiffnessInter() {}
  virtual void get(MElement *ele,int npts,IntPt *GP, fullMatrix<double> &m)const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me ?? get by integration point nonLocalDG3DStiffnessInter");
  }
  virtual BilinearTermBase* clone () const
  {
    return new nonLocalDG3DStiffnessInter(space1,space2,_mlawMinus,_mlawPlus,_interQuad,_beta1,
                                              _ipf,_ufield,_fullDg,_perturbation,_nonLocalBeta,_nonLocalBeta,_nonLocalEqRatio);
  }
};  
 
 

 template<class T1> class ThermoMechanicsG3DLoadTerm : public g3DLoadTerm<T1>
{
 public :
  ThermoMechanicsG3DLoadTerm(FunctionSpace<T1>& space1_,
               simpleFunction<typename TensorialTraits<T1>::ValType> *Load_) : g3DLoadTerm<T1>(space1_,Load_)
  {

  }
  virtual ~ThermoMechanicsG3DLoadTerm() {}

  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &mv) const
  {
    Msg::Error("Define me get by integration point ThermoMechanicsG3DLoadTerm");
  }
  virtual void get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const
  {
    const int nbdof=this->space1.getNumKeys(ele);
    int nbFF = ele->getNumShapeFunctions();
    m.resize(nbdof);
    m.scale(0.);
    // get the values of shape functions and detJ at all Gauss points
    nlsFunctionSpaceXYZ<double> *sp1 = static_cast<nlsFunctionSpaceXYZ<double>*>(&(this->space1));
    std::vector<std::vector<TensorialTraits<double>::ValType> > vvals;
    std::vector<std::vector<TensorialTraits<double>::GradType> > vgradsuvw;
    sp1->get(ele,npts,GP,vvals);
    sp1->getgradfuvw(ele,npts,GP,vgradsuvw);
    double jac[3][3];
    for (int i = 0; i < npts; i++)
    {
      const double weight = GP[i].weight;
      //g3DLoadTerm<T1>::Vals.clear();
      //LinearTerm<T1>::space1.f(ele,u,v,w, g3DLoadTerm<T1>::Vals);
      std::vector<TensorialTraits<double>::ValType> & Vals = vvals[i];
      const double detJ = ele->getJacobian(vgradsuvw[i],jac); //ele->getJacobian(u,v,w,jac);
      ele->pnt(Vals,g3DLoadTerm<T1>::p);
      g3DLoadTerm<T1>::load=g3DLoadTerm<T1>::Load->operator()(g3DLoadTerm<T1>::p.x(),g3DLoadTerm<T1>::p.y(),
                                                               g3DLoadTerm<T1>::p.z());
      double loadvalue = g3DLoadTerm<T1>::load*weight*detJ;
      for (int j = 0; j < nbdof ; ++j)
         m(j)+=Vals[j]*loadvalue;
	     
      //for (int j = 0; j < nbFF ; ++j)
      //{
        //m(j) +=Vals[j]*loadvalue;
        //m(j+nbFF)+=Vals[j]*loadvalue;
        //m(j+2*nbFF)+=Vals[j]*loadvalue;
        //m(j+3*nbFF)+=0;
      //}
    }
  }
  virtual LinearTermBase<double>* clone () const
  {
    return new ThermoMechanicsG3DLoadTerm(this->space1,g3DLoadTerm<T1>::Load);
  }
};

// elementary mass matrix
class ThermoMechanicsMassG3D : public mass3D{
 public:
  ThermoMechanicsMassG3D(FunctionSpace<double> &space1_,
                materialLaw *mlaw): mass3D(space1_,mlaw)
  {
  }
  ThermoMechanicsMassG3D(FunctionSpace<double> &space1_, FunctionSpace<double> &space2_,
                 materialLaw *mlaw): mass3D(space1_,space2_,mlaw)
  {
  }
 ThermoMechanicsMassG3D(FunctionSpace<double> &space1_, FunctionSpace<double> &space2_,
                 double rho): mass3D(space1_,space2_,rho)
  {
  }
  virtual void get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me get by integration point nonLocalMassG3D");
  }
  virtual BilinearTermBase* clone () const
  {
    return new ThermoMechanicsMassG3D(space1,space2,_rho);
  }
};

class ThermoMechanicsDG3DForceBulk : public dG3DForceBulk
{
 protected:
  const fullVector<double> *_data;
  double _ThermoMechanicsEqRatio;

 public:
 ThermoMechanicsDG3DForceBulk(FunctionSpace<double>& space1_,const materialLaw* mlaw,bool FullDG,
                                       const IPField *ip, const double ThermoMechanicsEqRatio) : dG3DForceBulk(space1_,mlaw,FullDG,ip), _ThermoMechanicsEqRatio(ThermoMechanicsEqRatio)
  {

  }
  virtual ~ThermoMechanicsDG3DForceBulk(){}
  virtual void get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const;
  virtual const bool isData() const {return true;} //bareps appears in internal forces, so should be perturbated
  virtual void set(const fullVector<double> *datafield){_data = datafield;}
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &mv) const
  {
    Msg::Error("Define me get by integration point ThermoMechanicsDG3DForceBulk");
  }

  virtual LinearTermBase<double>* clone () const
  {
    return new ThermoMechanicsDG3DForceBulk(space1,_mlaw,_fullDg,_ipf,_ThermoMechanicsEqRatio);
  }
};

class ThermoMechanicsDG3DStiffnessBulk : public dG3DStiffnessBulk
{
  double _ThermoMechanicsEqRatio;
 public:
  ThermoMechanicsDG3DStiffnessBulk(FunctionSpace<double>& space1_,FunctionSpace<double>& space2_,materialLaw* mlaw,
                                  bool FullDG, IPField *ip, const double ThermoMechanicsEqRatio) : dG3DStiffnessBulk(space1_,space2_,mlaw,FullDG,ip), _ThermoMechanicsEqRatio(ThermoMechanicsEqRatio)

  {

  }
  ThermoMechanicsDG3DStiffnessBulk(FunctionSpace<double>& space1_, const materialLaw* mlaw,
                                  bool FullDG,const IPField *ip, const double ThermoMechanicsEqRatio) : dG3DStiffnessBulk(space1_,mlaw,FullDG,ip), _ThermoMechanicsEqRatio(ThermoMechanicsEqRatio)
  {
  }
  virtual ~ThermoMechanicsDG3DStiffnessBulk(){}
  virtual void get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me get by gauss point ThermoMechanicsDG3DStiffnessBulk");
  }
  virtual BilinearTermBase* clone () const
  {
    return new ThermoMechanicsDG3DStiffnessBulk(space1,_mlaw,_fullDg,_ipf,_ThermoMechanicsEqRatio);
  }
};

class ThermoMechanicsDG3DForceInter : public dG3DForceInter
{
 protected:
  double _ThermoMechanicsBeta;
  bool _continuity;
  double _ThermoMechanicsEqRatio;
 public:
  ThermoMechanicsDG3DForceInter(FunctionSpace<double>& space1_, FunctionSpace<double> *space2_,
                                       const materialLaw *mlawMinus, const materialLaw *mlawPlus,interfaceQuadratureBase *iquad,
                                       double beta1_,
                                       bool fdg, const IPField *ip, double ThermoMechanicsBeta_,
                                       bool continuity_, double ThermoMechanicsEqRatio ) : dG3DForceInter( space1_, space2_,
                                       mlawMinus, mlawPlus,iquad,beta1_, fdg, ip), _ThermoMechanicsBeta(ThermoMechanicsBeta_),
                                       _continuity(continuity_), _ThermoMechanicsEqRatio(ThermoMechanicsEqRatio)
  {
  }
  virtual ~ThermoMechanicsDG3DForceInter() {}
  virtual void get(MElement *ele, int npts, IntPt *GP, fullVector<double> &v)const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &mv) const
  {
    Msg::Error("Define me get by integration point ThermoMechanicsDG3DForceInter");
  }
  virtual LinearTermBase<double>* clone () const
  {
    return new ThermoMechanicsDG3DForceInter(*_minusSpace,_plusSpace,_mlawMinus,_mlawPlus,_interQuad,_beta1,_fullDg,_ipf,
						 _ThermoMechanicsBeta,_continuity,_ThermoMechanicsEqRatio);
  }
};

class ThermoMechanicsDG3DStiffnessInter : public dG3DStiffnessInter
{
  double _ThermoMechanicsBeta;
  bool _continuity;
  double _ThermoMechanicsEqRatio;
 public:
  ThermoMechanicsDG3DStiffnessInter(FunctionSpace<double>& space1_,FunctionSpace<double>& space2_,
                                    const materialLaw* mlawMinus, const materialLaw* mlawPlus,interfaceQuadratureBase *iquad,
                                    double beta1_, const IPField *ip, unknownField *uf,
                                    bool fulldg,  double ThermoMechanicsBeta_, bool continuity_, double ThermoMechanicsEqRatio, double eps=1.e-6) :
                                    dG3DStiffnessInter(space1_,space2_,
                                          mlawMinus, mlawPlus,iquad,beta1_, ip, uf,fulldg,eps), _ThermoMechanicsBeta(ThermoMechanicsBeta_),
                                       _continuity(continuity_), _ThermoMechanicsEqRatio(ThermoMechanicsEqRatio)
  {

  }
  virtual ~ThermoMechanicsDG3DStiffnessInter() {}
  virtual void get(MElement *ele,int npts,IntPt *GP, fullMatrix<double> &m)const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me ?? get by integration point ThermoMechanicsDG3DStiffnessInter");
  }
  virtual BilinearTermBase* clone () const
  {
    return new ThermoMechanicsDG3DStiffnessInter(space1,space2,_mlawMinus,_mlawPlus,_interQuad,_beta1,
                                              _ipf,_ufield,_fullDg,_perturbation,_ThermoMechanicsBeta,_ThermoMechanicsBeta,_ThermoMechanicsEqRatio);
  }
  
  
  
};

#endif // DG3DTERMS_H_
