//
// C++ Interface: materialLaw
//
// Description: Class with definition of materialLaw for shell
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "dG3DMaterialLaw.h"
#include "ipstate.h"
#include "MInterfaceElement.h"
#include "ipField.h"
#include "mlawLinearThermoMechanics.h"


void dG3DMaterialLaw::fillElasticStiffness(double E, double nu, STensor43 &K_) const
{
  double mu = 0.5*E/(1.+nu);
  double lambda = (E*nu)/(1.+nu)/(1.-2.*nu);
  double twicemu = mu+mu;
  K_*=0.;
  K_(0,0,0,0) = lambda + twicemu;
  K_(1,1,0,0) = lambda;
  K_(2,2,0,0) = lambda;
  K_(0,0,1,1) = lambda;
  K_(1,1,1,1) = lambda + twicemu;
  K_(2,2,1,1) = lambda;
  K_(0,0,2,2) = lambda;
  K_(1,1,2,2) = lambda;
  K_(2,2,2,2) = lambda + twicemu;

  K_(1,0,1,0) = mu;
  K_(2,0,2,0) = mu;
  K_(0,1,0,1) = mu;
  K_(2,1,2,1) = mu;
  K_(0,2,0,2) = mu;
  K_(1,2,1,2) = mu;

  K_(0,1,1,0) = mu;
  K_(0,2,2,0) = mu;
  K_(1,0,0,1) = mu;
  K_(1,2,2,1) = mu;
  K_(2,0,0,2) = mu;
  K_(2,1,1,2) = mu;
}

Cohesive3DLaw::Cohesive3DLaw(const int num, const double Gc, const double sigmac,
                                   const double beta, const double mu,const double fscmin, const double fscmax,
                                   const bool init) : materialLaw(num,init), _Gc(Gc), _sigmac(sigmac),
                                                       _beta(beta), _mu(mu), _fscmin(fscmin), _fscmax(fscmax)
{

}
Cohesive3DLaw::Cohesive3DLaw(const Cohesive3DLaw &source) : materialLaw(source), _Gc(source._Gc),
                                                                      _sigmac(source._sigmac),
                                                                      _beta(source._beta), _mu(source._mu),
                                                                      _fscmin(source._fscmin), _fscmax(source._fscmax)
{

}

LinearCohesive3DLaw::LinearCohesive3DLaw(const int num, const double Gc, const double sigmac,
                                               const double beta,const double mu,
                                               const double fractureStrengthFactorMin,
                                               const double fractureStrengthFactorMax) : Cohesive3DLaw(num,Gc,sigmac,beta,mu,fractureStrengthFactorMin,
                                                                                                          fractureStrengthFactorMax,true),
                                                                                         _betaexpm2(1./(_beta*_beta))
{

}

LinearCohesive3DLaw::LinearCohesive3DLaw(const LinearCohesive3DLaw &source) : Cohesive3DLaw(source),
                                                                                        _betaexpm2(source._betaexpm2)
{

}
void LinearCohesive3DLaw::createIPState(IPStateBase* &ips,const bool* state_,
                                          const MElement *ele, const int nbFF_, const int gpt) const
{
  IPVariable *ipvi = new LinearCohesive3DIPVariable();
  IPVariable *ipv1 = new LinearCohesive3DIPVariable();
  IPVariable *ipv2 = new LinearCohesive3DIPVariable();
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void LinearCohesive3DLaw::createIPVariable(IPVariable* &ipv,const MElement *ele,const int nbFF) const
{
  ipv = new LinearCohesive3DIPVariable();
}


void LinearCohesive3DLaw::stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff, const bool checkfrac)
{
  FractureCohesive3DIPVariable *fipv = static_cast < FractureCohesive3DIPVariable *> (ipv);
  const FractureCohesive3DIPVariable *fipvprev = static_cast < const FractureCohesive3DIPVariable *> (ipvprev);

  SVector3 normDir = fipvprev->getConstRefToCurrentOutwardNormal(); //so derivative cste
  normDir.normalize();
  fipv->setFracture(fipvprev,fipv->getConstRefToJump(), normDir);

  SVector3 tgDir = fipv->getDeltat();
  tgDir.normalize();

  fipv->getRefToInterfaceForce() = 0.;
  // monotonic decreasing cohesive law (Pandolfi & Ortiz 1999)
  if(fipv->ifTension()) // tension case
  {
      if(fipv->getDelta()<=fipv->getDeltac() && fipvprev->getDeltamax() <fipv->getDeltac()) // and fipv->getDeltan() >=0.)
      {
	SVector3 DtDjump = 0.;
        double t =1.;
        if(fipv->getDelta() >= fipvprev->getDeltamax() or fipvprev->getDeltamax() == 0) // loading case
        {
          t = 1.-fipv->getDelta()/fipv->getDeltac();
          fipv->setDeltamax(fipv->getDelta());
          DtDjump = (-1./fipv->getDeltac()) * fipv->getDDeltaDjump();
          fipv->getTfactor() = t;
        }
        else  //unloading case
        {
          if(fipvprev->getDeltamax()>1.e-10*fipv->getDeltac())
          {
            t = fipv->getDelta()/fipvprev->getDeltamax() - fipv->getDelta()/fipv->getDeltac();
            DtDjump = fipv->getDDeltaDjump()*((-fipv->getDeltan()/(fabs(fipv->getDeltan())))/(fipvprev->getDeltamax()-fipv->getDeltac()));
          }
          else
          {
            t=1.- fipvprev->getDeltamax()/fipv->getDeltac();
            DtDjump = 0.;
          }
        }
        double facn=1.;
        double fact=0.;
	if(fipv->getSigmac()> 0. && fipv->getSNor0()<fipv->getSigmac())
	{
	  facn = fabs(fipv->getSNor0()/fipv->getSigmac());
          fact = sqrt(1-facn*facn);
	}
	SVector3 DfacnDjump = 0.;
	SVector3 DfactDjump = 0.;
        if(fabs(fipv->getDelta()/fipv->getDeltac()) > 1.e-12)
        {
          if(fabs(fipv->getDeltan())<fabs(fipv->getDelta()))
          {
            facn = fabs(fipv->getDeltan()/fipv->getDelta());
	    if(fipv->getDeltan()>0)
	      DfacnDjump = (fipv->getDDeltanDjump()*(1./fipv->getDelta()) - (fipv->getDeltan()/(fipv->getDelta()*fipv->getDelta()))*fipv->getDDeltaDjump());
	    else
	      DfacnDjump = (fipv->getDDeltanDjump()*(-1./fipv->getDelta()) + (fipv->getDeltan()/(fipv->getDelta()*fipv->getDelta()))*fipv->getDDeltaDjump());
            fact = sqrt(1-facn*facn);
            if(fact >0.)
 	    	DfactDjump = DfacnDjump*(-1.*facn/fact);
            else
                DfactDjump = 0.;
	  }
        }
	//fact = 0.;
	//DfactDjump =0.;


        fipv->getRefToInterfaceForce() = (fipv->getSigmac()*t)*((facn * normDir) + (fact*tgDir));
	//Msg::Error("Fracture in tension: Delta %f Deltan %f Delta t %f  Deltac %f, t %f, facn %f fact %f, sc %f, Delta max %f", fipv->getDelta(), fipv->getDeltan(), fipv->getDeltat().norm(), fipv->getDeltac(), t, facn, fact, fipv->getSigmac(), fipv->getDeltamax());
	STensor3 &DFDU = fipv->getRefToDInterfaceForceDjump();
	DFDU=0.;
	for (int i= 0; i <3 ; i++)
	{
	  for(int j=0; j < 3; j++)
          {
	    DFDU(i,j) += (fipv->getSigmac()*((facn * normDir(i)) + (fact*tgDir(i)))) * DtDjump(j);
	    DFDU(i,j) += (fipv->getSigmac()*t)*((DfacnDjump(j) * normDir(i)) + (DfactDjump(j)*tgDir(i)));
          }
        }
      }
      else
      {
        fipv->getRefToInterfaceForce() = 0.;
	fipv->getRefToDInterfaceForceDjump() =0.;
      }
  }
  else
  { // compression case
    double fact = 1.;
    if(fabs(fipv->getDelta())<=fabs(fipv->getDeltac()))
    {
      SVector3 DcDjump = 0.;
      double c=1.;
      if(fabs(fipv->getDelta()) >= fabs(fipvprev->getDeltamax())) // loading case
      {
        c = 1.- fipv->getDelta()/fipv->getDeltac();
	DcDjump = (-1./fipv->getDeltac()) * fipv->getDDeltaDjump();
        fipv->setDeltamax(fipv->getDelta());
      }
      else //unloading case
      {
        if(fipvprev->getDeltamax()>1.e-12*fipv->getDeltac())
	  c = fipv->getDelta()/fipvprev->getDeltamax() - fipv->getDelta()/fipv->getDeltac();
	else
	  c=0.;
	DcDjump = (-1./(fipvprev->getDeltamax()-fipv->getDeltac()))*fipv->getDDeltaDjump();
      }
      fipv->getRefToInterfaceForce() = (getSigmac()*c*fact) * tgDir;
      STensor3 &DFDU = fipv->getRefToDInterfaceForceDjump();
      DFDU=0.;
      for (int i= 0; i <3 ; i++)
      {
        for(int j=0; j < 3; j++)
	{
	  DFDU(i,j) += (fipv->getSigmac()*(fact*tgDir(i))) * DcDjump(j);
	}
      }
     //Msg::Error("Fracture in compression: Delta %f Deltan %f Delta t %f  Deltac %f, c %f, fact %f, sc %f, Delta max %f", fipv->getDelta(), fipv->getDeltan(), fipv->getDeltat().norm(), fipv->getDeltac(), c, fact, fipv->getSigmac(), fipv->getDeltamax());

    }
    else
    {
      fipv->getRefToInterfaceForce() = 0.;
      fipv->getRefToDInterfaceForceDjump() =0.;
    }
  }
  fipv->getFractureEnergy() = fipvprev->getFractureEnergy();
  // compute the released energy HAS TO BE CHECKED !!
  if(fipv->getDelta() > fipv->getDeltamax()) // otherwise no increment of released energy
  {
    fipv->getFractureEnergy() += 0.5*fipv->getSigmac()*(fipv->getTfactor()+fipvprev->getTfactor())*(fipv->getDelta()-fipvprev->getDelta());
  }
  else
  {
    fipv->getTfactor() = fipvprev->getTfactor();
  }
}

const bool LinearCohesive3DLaw::checkBroken(dG3DIPVariableBase *ipvcur, const dG3DIPVariableBase *ipvprev) const
{
  FractureCohesive3DIPVariable *ipvfcur = static_cast<FractureCohesive3DIPVariable*>(ipvcur);

  SVector3 normDir = ipvprev->getConstRefToCurrentOutwardNormal()*(1./ipvprev->getConstRefToCurrentOutwardNormal().norm()); //so derivative cste
  SVector3 tmp(1.,1.,3.);
  SVector3 tgDir = crossprod(normDir,tmp);
  if(tgDir.norm() < 1.e-7)
  {
     SVector3 tmp2(-1.,1.,3.);
     tgDir = crossprod(normDir,tmp);
  }
  tgDir = tgDir*(1./tgDir.norm());

  /* value of sigma_c which depends on random factor */
  const double effsigc = ipvfcur->fractureStrengthFactor() * _sigmac;

  // Look upper fiber first
  STensor3 cauchy;
  ipvcur->getCauchyStress(cauchy);

  double snor =0.,tau =0.,traction=0.;
  for (int i=0;i < 3; i++)
  {
    for (int j=0; j < 3; j++)
    {
      snor += normDir(i)*cauchy(i,j)*normDir(j);
      traction  += normDir(i)*cauchy(i,j)*normDir(i)*cauchy(i,j);
    }
  }
  traction = sqrt(traction);
  tau = sqrt(fabs(traction*traction-snor*snor));
  // Normal and tangential components
  bool ift;
  double smax;
  double seff;
  // sigma eff (Camacho and Ortiz)
  if(snor>=0.){
    seff = sqrt(snor*snor+_betaexpm2*tau*tau);
    smax = snor;
    ift = true;
  }
  else{
    double temp = fabs(tau)-_mu*fabs(snor);
    if (temp >0)
      seff = 1./_beta*temp;
    else
      seff = 0.;
    //no fracture in compression if mu<0
    if(_mu< 0) seff =0.;
    smax = tau;
    ift=false;
  }
  if(seff> effsigc){
//    Msg::Info("sc %e",seff);
    ipvfcur->broken();
  }
  if(ipvfcur->isbroken()){
    // create ipvariable for fracture (Allocation --> previous too)
    const FractureCohesive3DIPVariable *ipvfprev= static_cast<const FractureCohesive3DIPVariable*>(ipvprev);
    /* if ipv have been created before (previous iteration delete first) CHANGE THIS just reset the function
    if(ipvccur !=NULL){delete ipvccur;}
    if(ipvcprev!=NULL){delete ipvcprev;}
    IPVariable* ipvccur2 = dynamic_cast<IPVariable*>(ipvccur);
    IPVariable* ipvcprev2 = dynamic_cast<IPVariable*>(ipvcprev);
    this->createIPVariable(ipvccur2);
    this->createIPVariable(ipvcprev2);
    ipvfcur->setIPvFrac(ipvccur2);
    ipvfprev->setIPvFrac(ipvcprev2);*/
    ipvfcur->setDataFromLaw(traction, snor,tau,_Gc,_beta,ift);
    // needed for stiffness matrix in non linear
    //ipvcprev = ipvfcur->getIPvFrac();
    //ipvcprev->setDataFromLaw(smax,_Gc,_beta,ift);
  }
  return ipvfcur->isbroken();
}


J2LinearDG3DMaterialLaw::J2LinearDG3DMaterialLaw(const int num,const double rho,
                                   double E,const double nu,const double sy0,const double h,
                                   const double tol) : dG3DMaterialLaw(num,rho,true),
                                                             _j2law(num,E,nu,rho,sy0,h,tol)
{
  fillElasticStiffness(E, nu, elasticStiffness);
}

J2LinearDG3DMaterialLaw::J2LinearDG3DMaterialLaw(const int num,const double rho,
                                   double E, const double nu, const J2IsotropicHardening &_j2IH,
                                   const double tol) : dG3DMaterialLaw(num,rho,true), _j2law(num,E,nu,rho,_j2IH,tol)
{
  fillElasticStiffness(E, nu, elasticStiffness);
}

J2LinearDG3DMaterialLaw::J2LinearDG3DMaterialLaw(const J2LinearDG3DMaterialLaw &source) : dG3DMaterialLaw(source), _j2law(source._j2law)
{

}

void J2LinearDG3DMaterialLaw::createIPState(IPStateBase* &ips,const bool* state_,const MElement *ele, const int nbFF_, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  J2LinearDG3DIPVariable(_j2law,inter);
  IPVariable* ipv1 = new  J2LinearDG3DIPVariable(_j2law,inter);
  IPVariable* ipv2 = new  J2LinearDG3DIPVariable(_j2law,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void J2LinearDG3DMaterialLaw::createIPVariable(IPVariable* &ipv,const MElement *ele, const int nbFF) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  J2LinearDG3DIPVariable(_j2law,inter);
}


void J2LinearDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac)
{
  /* get ipvariable */
  J2LinearDG3DIPVariable* ipvcur;
  const J2LinearDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<J2LinearDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const J2LinearDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<J2LinearDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const J2LinearDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPJ2linear* q1 = ipvcur->getIPJ2linear();
  const IPJ2linear* q0 = ipvprev->getIPJ2linear();

  /* compute stress */
  _j2law.constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),stiff);

  ipvcur->setRefToElasticTangentModuli(this->elasticStiffness);
}

VUMATinterfaceDG3DMaterialLaw::VUMATinterfaceDG3DMaterialLaw(const int num, const double rho, const char *propName) :
                                                              dG3DMaterialLaw(num,rho,true),
                                                             _vumatlaw(num,rho,propName)
{
  double nu = 0.3; // NOTE: This is to get an order of magnitude; another way of doing it would be to
                   //       change vumat_soundspeed to vumat_mu_nu which would give mu and nu
                   //       then reconstruct mlawVUMATinterface::soundSpeed to use this instead
                   //       and create mlawVUMATinterface::poissonRation/shearModulus using it too
  double soundSpeed = _vumatlaw.soundSpeed();
  double lambda_plus_two_mu = soundSpeed*soundSpeed*rho;
  double E = lambda_plus_two_mu * ( ((1+nu)*(1-2*nu)) / (1-nu) );
  fillElasticStiffness(E, nu, elasticStiffness);
}

VUMATinterfaceDG3DMaterialLaw::VUMATinterfaceDG3DMaterialLaw(const VUMATinterfaceDG3DMaterialLaw &source) :
                                                             dG3DMaterialLaw(source), _vumatlaw(source._vumatlaw)
{

}

void VUMATinterfaceDG3DMaterialLaw::createIPState(IPStateBase* &ips,const bool* state_,const MElement *ele, const int nbFF_, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  int nsdv = _vumatlaw.getNsdv();
  double size = 2.*(( const_cast<MElement*>(ele) )->getInnerRadius());
  IPVariable* ipvi = new  VUMATinterfaceDG3DIPVariable(nsdv,size,inter);
  IPVariable* ipv1 = new  VUMATinterfaceDG3DIPVariable(nsdv,size,inter);
  IPVariable* ipv2 = new  VUMATinterfaceDG3DIPVariable(nsdv,size,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void VUMATinterfaceDG3DMaterialLaw::createIPVariable(IPVariable* &ipv,const MElement *ele, const int nbFF) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  VUMATinterfaceDG3DIPVariable(_vumatlaw.getNsdv(),2.*(( const_cast<MElement*>(ele) )->getInnerRadius()),inter);
}

void VUMATinterfaceDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac)
{
  /* get ipvariable */
  VUMATinterfaceDG3DIPVariable* ipvcur;
  const VUMATinterfaceDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<VUMATinterfaceDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const VUMATinterfaceDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<VUMATinterfaceDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const VUMATinterfaceDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for VUMAT law */
  IPVUMATinterface* q1 = ipvcur->getIPVUMATinterface();
  const IPVUMATinterface* q0 = ipvprev->getIPVUMATinterface();

  /* compute stress */
  ipvcur->getRefToFirstPiolaKirchhoffStress()=( const_cast<VUMATinterfaceDG3DIPVariable*>(ipvprev) )->getRefToFirstPiolaKirchhoffStress();
  _vumatlaw.constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),stiff);

  ipvcur->setRefToElasticTangentModuli(this->elasticStiffness);
}


NonLocalDamageDG3DMaterialLaw::NonLocalDamageDG3DMaterialLaw(const int num, const double rho, const char *propName) :
                                                              dG3DMaterialLaw(num,rho,true)
{
  _nldlaw = new mlawNonLocalDamage(num,rho,propName);
  double nu = _nldlaw->poissonRatio();
  double mu = _nldlaw->shearModulus();
  double E = 2.*mu*(1.+nu);
  fillElasticStiffness(E, nu, elasticStiffness);
}

NonLocalDamageDG3DMaterialLaw::NonLocalDamageDG3DMaterialLaw(const NonLocalDamageDG3DMaterialLaw &source) :
                                                             dG3DMaterialLaw(source), _nldlaw(source._nldlaw)
{

}

void NonLocalDamageDG3DMaterialLaw::createIPState(IPStateBase* &ips,const bool* state_,const MElement *ele, const int nbFF_, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  int nsdv = _nldlaw->getNsdv();
  IPVariable* ipvi = new  nonLocalDamageDG3DIPVariable(nsdv,inter);
  IPVariable* ipv1 = new  nonLocalDamageDG3DIPVariable(nsdv,inter);
  IPVariable* ipv2 = new  nonLocalDamageDG3DIPVariable(nsdv,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
  _nldlaw->createIPState((static_cast <nonLocalDamageDG3DIPVariable*> (ipvi))->getIPNonLocalDamage(),
                         (static_cast <nonLocalDamageDG3DIPVariable*> (ipv1))->getIPNonLocalDamage(),
             			 (static_cast <nonLocalDamageDG3DIPVariable*> (ipv2))->getIPNonLocalDamage());

}

void NonLocalDamageDG3DMaterialLaw::createIPVariable(IPVariable* &ipv,const MElement *ele, const int nbFF) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  nonLocalDamageDG3DIPVariable(_nldlaw->getNsdv(),inter);
  IPNonLocalDamage * ipvnl = static_cast <nonLocalDamageDG3DIPVariable*>(ipv)->getIPNonLocalDamage();
  _nldlaw->createIPVariable(ipvnl,ele,nbFF);
}


void NonLocalDamageDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac)
{
  /* get ipvariable */
  nonLocalDamageDG3DIPVariable* ipvcur; //= static_cast < nonLocalDamageDG3DIPVariable *> (ipv);
  const nonLocalDamageDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<nonLocalDamageDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const nonLocalDamageDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<nonLocalDamageDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const nonLocalDamageDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPNonLocalDamage* q1 = ipvcur->getIPNonLocalDamage();
  const IPNonLocalDamage* q0 = ipvprev->getIPNonLocalDamage();

  /* compute stress */
  _nldlaw->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),
                        ipvcur->getRefToDLocalPlasticStrainDStrain(),ipvcur->getRefToDStressDNonLocalPlasticStrain(),
                        ipvcur->getRefToDLocalPlasticStrainDNonLocalPlasticStrain(),stiff);

  ipvcur->setRefToElasticTangentModuli(this->elasticStiffness);

}
//
NonLocalDamageJ2HyperDG3DMaterialLaw::NonLocalDamageJ2HyperDG3DMaterialLaw(const int num,const double rho,
                   double E,const double nu,const J2IsotropicHardening &_j2IH,
                    const CLengthLaw &_cLLaw,const DamageLaw &_damLaw,const double tol) :
                                                              dG3DMaterialLaw(num,rho,true)
{
  _nldJ2Hyperlaw = new mlawNonLocalDamageJ2Hyper(num,E,nu,rho,_j2IH,_cLLaw,_damLaw,tol);
  fillElasticStiffness(E, nu, elasticStiffness);
}

NonLocalDamageJ2HyperDG3DMaterialLaw::NonLocalDamageJ2HyperDG3DMaterialLaw(const NonLocalDamageJ2HyperDG3DMaterialLaw &source) :
                                                             dG3DMaterialLaw(source)
{
  _nldJ2Hyperlaw = new mlawNonLocalDamageJ2Hyper(*(source._nldJ2Hyperlaw));
}

void NonLocalDamageJ2HyperDG3DMaterialLaw::createIPState(IPStateBase* &ips,const bool* state_,const MElement *ele, const int nbFF_, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  nonLocalDamageJ2HyperDG3DIPVariable(*_nldJ2Hyperlaw,inter);
  IPVariable* ipv1 = new  nonLocalDamageJ2HyperDG3DIPVariable(*_nldJ2Hyperlaw,inter);
  IPVariable* ipv2 = new  nonLocalDamageJ2HyperDG3DIPVariable(*_nldJ2Hyperlaw,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
  _nldJ2Hyperlaw->createIPState((static_cast <nonLocalDamageJ2HyperDG3DIPVariable*> (ipvi))->getIPNonLocalDamageJ2Hyper(),
                         (static_cast <nonLocalDamageJ2HyperDG3DIPVariable*> (ipv1))->getIPNonLocalDamageJ2Hyper(),
             			 (static_cast <nonLocalDamageJ2HyperDG3DIPVariable*> (ipv2))->getIPNonLocalDamageJ2Hyper());

}

void NonLocalDamageJ2HyperDG3DMaterialLaw::createIPVariable(IPVariable* &ipv,const MElement *ele, const int nbFF) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  nonLocalDamageJ2HyperDG3DIPVariable(*_nldJ2Hyperlaw,inter);
  IPNonLocalDamageJ2Hyper * ipvnl = static_cast <nonLocalDamageJ2HyperDG3DIPVariable*>(ipv)->getIPNonLocalDamageJ2Hyper();
  _nldJ2Hyperlaw->createIPVariable(ipvnl,ele,nbFF);
}

void NonLocalDamageJ2HyperDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac)
{
  /* get ipvariable */
  nonLocalDamageJ2HyperDG3DIPVariable* ipvcur; //= static_cast < nonLocalDamageDG3DIPVariable *> (ipv);
  const nonLocalDamageJ2HyperDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<nonLocalDamageJ2HyperDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const nonLocalDamageJ2HyperDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<nonLocalDamageJ2HyperDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const nonLocalDamageJ2HyperDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPNonLocalDamageJ2Hyper* q1 = ipvcur->getIPNonLocalDamageJ2Hyper();
  const IPNonLocalDamageJ2Hyper* q0 = ipvprev->getIPNonLocalDamageJ2Hyper();

  /* compute stress */
  _nldJ2Hyperlaw->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),
                        ipvcur->getRefToDLocalPlasticStrainDStrain(),ipvcur->getRefToDStressDNonLocalPlasticStrain(),
                        ipvcur->getRefToDLocalPlasticStrainDNonLocalPlasticStrain(),stiff);

  ipvcur->setRefToElasticTangentModuli(this->elasticStiffness);

}
//
NonLocalDamageGursonDG3DMaterialLaw::NonLocalDamageGursonDG3DMaterialLaw(const int num,const double rho,
                    const double E,const double nu,
		    const double q1, const double q2, const double q3, const double fC, 
                    const double ff, const double ffstar, const double fVinitial,
                    const J2IsotropicHardening &_j2IH,
                    const CLengthLaw &_cLLaw, const GursonDamageNucleation &_gdn, const double tol) :
                                                              dG3DMaterialLaw(num,rho,true)
{
  _nldGursonlaw = new mlawNonLocalDamageGurson(num,E,nu,rho,q1, q2, q3, fC, 
                                                              ff, ffstar, fVinitial,_j2IH,_cLLaw,_gdn,tol);
  fillElasticStiffness(E, nu, elasticStiffness);
}

NonLocalDamageGursonDG3DMaterialLaw::NonLocalDamageGursonDG3DMaterialLaw(const NonLocalDamageGursonDG3DMaterialLaw &source) :
                                                             dG3DMaterialLaw(source)
{
  _nldGursonlaw = new mlawNonLocalDamageGurson(*(source._nldGursonlaw));
}

void NonLocalDamageGursonDG3DMaterialLaw::createIPState(IPStateBase* &ips,const bool* state_,const MElement *ele, const int nbFF_, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  nonLocalDamageGursonDG3DIPVariable(*_nldGursonlaw,inter);
  IPVariable* ipv1 = new  nonLocalDamageGursonDG3DIPVariable(*_nldGursonlaw,inter);
  IPVariable* ipv2 = new  nonLocalDamageGursonDG3DIPVariable(*_nldGursonlaw,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
  _nldGursonlaw->createIPState((static_cast <nonLocalDamageGursonDG3DIPVariable*> (ipvi))->getIPNonLocalDamageGurson(),
                         (static_cast <nonLocalDamageGursonDG3DIPVariable*> (ipv1))->getIPNonLocalDamageGurson(),
             			 (static_cast <nonLocalDamageGursonDG3DIPVariable*> (ipv2))->getIPNonLocalDamageGurson());

}

void NonLocalDamageGursonDG3DMaterialLaw::createIPVariable(IPVariable* &ipv,const MElement *ele, const int nbFF) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  nonLocalDamageGursonDG3DIPVariable(*_nldGursonlaw,inter);
  IPNonLocalDamageGurson * ipvnl = static_cast <nonLocalDamageGursonDG3DIPVariable*>(ipv)->getIPNonLocalDamageGurson();
  _nldGursonlaw->createIPVariable(ipvnl,ele,nbFF);
}

void NonLocalDamageGursonDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac)
{
  /* get ipvariable */
  nonLocalDamageGursonDG3DIPVariable* ipvcur; //= static_cast < nonLocalDamageDG3DIPVariable *> (ipv);
  const nonLocalDamageGursonDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<nonLocalDamageGursonDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const nonLocalDamageGursonDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<nonLocalDamageGursonDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const nonLocalDamageGursonDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPNonLocalDamageGurson* q1 = ipvcur->getIPNonLocalDamageGurson();
  const IPNonLocalDamageGurson* q0 = ipvprev->getIPNonLocalDamageGurson();

  /* compute stress */
  _nldGursonlaw->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),
                        ipvcur->getRefToDLocalPlasticStrainDStrain(),ipvcur->getRefToDStressDNonLocalPlasticStrain(),
                        ipvcur->getRefToDLocalPlasticStrainDNonLocalPlasticStrain(),stiff);

  ipvcur->setRefToElasticTangentModuli(this->elasticStiffness);

}

//
NonLocalDamageLinearCohesive3DLaw::NonLocalDamageLinearCohesive3DLaw(const int num, const double Gc, const double sigmac, const double Dc,
                                               const double beta,
                                               const double fractureStrengthFactorMin,
                                               const double fractureStrengthFactorMax) : 
								LinearCohesive3DLaw(num,Gc,sigmac,beta,0.,fractureStrengthFactorMin,
                                                                                                          fractureStrengthFactorMax),
                                                                                         _Dc(Dc)
{

}

NonLocalDamageLinearCohesive3DLaw::NonLocalDamageLinearCohesive3DLaw(const NonLocalDamageLinearCohesive3DLaw &source) : LinearCohesive3DLaw(source),
                                                                                        _Dc(source._Dc)
{

}
void NonLocalDamageLinearCohesive3DLaw::createIPState(IPStateBase* &ips,const bool* state_,
                                          const MElement *ele, const int nbFF_, const int gpt) const
{
  IPVariable *ipvi = new NonLocalDamageLinearCohesive3DIPVariable();
  IPVariable *ipv1 = new NonLocalDamageLinearCohesive3DIPVariable();
  IPVariable *ipv2 = new NonLocalDamageLinearCohesive3DIPVariable();
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void NonLocalDamageLinearCohesive3DLaw::createIPVariable(IPVariable* &ipv,const MElement *ele,const int nbFF) const
{
  ipv = new NonLocalDamageLinearCohesive3DIPVariable();
}


void NonLocalDamageLinearCohesive3DLaw::stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff, const bool checkfrac)
{
  FractureCohesive3DIPVariable *fipv = static_cast < FractureCohesive3DIPVariable *> (ipv);
  const FractureCohesive3DIPVariable *fipvprev = static_cast < const FractureCohesive3DIPVariable *> (ipvprev);

  SVector3 normDir = fipvprev->getConstRefToCurrentOutwardNormal(); //so derivative cste
  normDir.normalize();
  fipv->setFracture(fipvprev,fipv->getConstRefToJump(), normDir);

  SVector3 tgDir = fipv->getDeltat();
  tgDir.normalize();

  fipv->getRefToInterfaceForce() = 0.;
  // monotonic decreasing cohesive law (Pandolfi & Ortiz 1999)
  if(fipv->ifTension()) // tension case
  {
      if(fipv->getDelta()<=fipv->getDeltac() && fipvprev->getDeltamax() < fipv->getDeltac()) // and fipv->getDeltan() >=0.)
      {
	SVector3 DtDjump = 0.;
        double t =1.;
        if(fipv->getDelta() >= fipvprev->getDeltamax() or fipvprev->getDeltamax() == 0) // loading case
        {
          t = 1.-fipv->getDelta()/fipv->getDeltac();
          fipv->setDeltamax(fipv->getDelta());
          DtDjump = (-1./fipv->getDeltac()) * fipv->getDDeltaDjump();
          fipv->getTfactor() = t;
        }
        else  //unloading case
        {
          if(fipvprev->getDeltamax()>1.e-10*fipv->getDeltac())
          {
            t = fipv->getDelta()/fipvprev->getDeltamax() - fipv->getDelta()/fipv->getDeltac();
            DtDjump = fipv->getDDeltaDjump()*((-fipv->getDeltan()/(fabs(fipv->getDeltan())))/(fipvprev->getDeltamax()-fipv->getDeltac()));
          }
          else
          {
            t=1.- fipvprev->getDeltamax()/fipv->getDeltac();
            DtDjump = 0.;
          }
        }
	double facn=1.;
        double fact=0.;
        if(fipv->getSigmac()> 0. && fipv->getSNor0()<fipv->getSigmac())
        {
          facn = fabs(fipv->getSNor0()/fipv->getSigmac());
          fact = sqrt(1-facn*facn);
        }
	SVector3 DfacnDjump = 0.;
	SVector3 DfactDjump = 0.;
        if(fabs(fipv->getDelta()/fipv->getDeltac()) > 1.e-12)
        {
          if(fabs(fipv->getDeltan())<fabs(fipv->getDelta()))
          {
            facn = fabs(fipv->getDeltan()/fipv->getDelta());
	    if(fipv->getDeltan()>0)
	      DfacnDjump = (fipv->getDDeltanDjump()*(1./fipv->getDelta()) - (fipv->getDeltan()/(fipv->getDelta()*fipv->getDelta()))*fipv->getDDeltaDjump());
	    else
	      DfacnDjump = (fipv->getDDeltanDjump()*(-1./fipv->getDelta()) + (fipv->getDeltan()/(fipv->getDelta()*fipv->getDelta()))*fipv->getDDeltaDjump());
            fact = sqrt(1-facn*facn);
            if(fact >0.)
 	    	DfactDjump = DfacnDjump*(-1.*facn/fact);
            else
                DfactDjump = 0.;
	  }
        }
	//fact = 0.;
	//DfactDjump =0.;


        fipv->getRefToInterfaceForce() = (fipv->getSigmac()*t)*((facn * normDir) + (fact*tgDir));
	//Msg::Error("Fracture in tension: Delta %f Deltan %f Delta t %f  Deltac %f, t %f, facn %f fact %f, sc %f, Delta max %f", fipv->getDelta(), fipv->getDeltan(), fipv->getDeltat().norm(), fipv->getDeltac(), t, facn, fact, fipv->getSigmac(), fipv->getDeltamax());
	STensor3 &DFDU = fipv->getRefToDInterfaceForceDjump();
	DFDU=0.;
	for (int i= 0; i <3 ; i++)
	{
	  for(int j=0; j < 3; j++)
          {
	    DFDU(i,j) += (fipv->getSigmac()*((facn * normDir(i)) + (fact*tgDir(i)))) * DtDjump(j);
	    DFDU(i,j) += (fipv->getSigmac()*t)*((DfacnDjump(j) * normDir(i)) + (DfactDjump(j)*tgDir(i)));
          }
        }
      }
      else
      {
        fipv->getRefToInterfaceForce() = 0.;
	fipv->getRefToDInterfaceForceDjump() =0.;
      }
  }
  else
  { // compression case
    double fact = 1.;
    if(fabs(fipv->getDelta())<=fabs(fipv->getDeltac()))
    {
      SVector3 DcDjump = 0.;
      double c=1.;
      if(fabs(fipv->getDelta()) >= fabs(fipvprev->getDeltamax())) // loading case
      {
        c = 1.- fipv->getDelta()/fipv->getDeltac();
	DcDjump = (-1./fipv->getDeltac()) * fipv->getDDeltaDjump();
        fipv->setDeltamax(fipv->getDelta());
      }
      else //unloading case
      {
        if(fipvprev->getDeltamax()>1.e-12*fipv->getDeltac())
	  c = fipv->getDelta()/fipvprev->getDeltamax() - fipv->getDelta()/fipv->getDeltac();
	else
	  c=0.;
	DcDjump = (-1./(fipvprev->getDeltamax()-fipv->getDeltac()))*fipv->getDDeltaDjump();
      }
      fipv->getRefToInterfaceForce() = (getSigmac()*c*fact) * tgDir;
      STensor3 &DFDU = fipv->getRefToDInterfaceForceDjump();
      DFDU=0.;
      for (int i= 0; i <3 ; i++)
      {
        for(int j=0; j < 3; j++)
	{
	  DFDU(i,j) += (fipv->getSigmac()*(fact*tgDir(i))) * DcDjump(j);
	}
      }
     //Msg::Error("Fracture in compression: Delta %f Deltan %f Delta t %f  Deltac %f, c %f, fact %f, sc %f, Delta max %f", fipv->getDelta(), fipv->getDeltan(), fipv->getDeltat().norm(), fipv->getDeltac(), c, fact, fipv->getSigmac(), fipv->getDeltamax());

    }
    else
    {
      fipv->getRefToInterfaceForce() = 0.;
      fipv->getRefToDInterfaceForceDjump() =0.;
    }
  }
  fipv->getFractureEnergy() = fipvprev->getFractureEnergy();
  // compute the released energy HAS TO BE CHECKED !!
  if(fipv->getDelta() > fipv->getDeltamax()) // otherwise no increment of released energy
  {
    fipv->getFractureEnergy() += 0.5*fipv->getSigmac()*(fipv->getTfactor()+fipvprev->getTfactor())*(fipv->getDelta()-fipvprev->getDelta());
  }
  else
  {
    fipv->getTfactor() = fipvprev->getTfactor();
  }
}

const bool NonLocalDamageLinearCohesive3DLaw::checkBroken(dG3DIPVariableBase *ipvcur, const dG3DIPVariableBase *ipvprev) const
{
  FractureCohesive3DIPVariable *ipvfcur = static_cast<FractureCohesive3DIPVariable*>(ipvcur);

  SVector3 normDir = ipvprev->getConstRefToCurrentOutwardNormal()*(1./ipvprev->getConstRefToCurrentOutwardNormal().norm()); //so derivative cste
  SVector3 tmp(1.,1.,3.);
  SVector3 tgDir = crossprod(normDir,tmp);
  if(tgDir.norm() < 1.e-7)
  {
     SVector3 tmp2(-1.,1.,3.);
     tgDir = crossprod(normDir,tmp);
  }
  tgDir = tgDir*(1./tgDir.norm());

  double remainingG = _Gc;
  // value of sigma_c which depends on random factor
  const double effDc = ipvfcur->fractureStrengthFactor() * _Dc;
  const double effsigc = ipvfcur->fractureStrengthFactor() * _sigmac;
  //get damage
  double damage = ipvcur->get(IPField::DAMAGE);

  // Look upper fiber first
  STensor3 cauchy;
  ipvcur->getCauchyStress(cauchy);

  double snor =0.,tau =0.,traction=0.;
  for (int i=0;i < 3; i++)
  {
    for (int j=0; j < 3; j++)
    {
      snor += normDir(i)*cauchy(i,j)*normDir(j);
      traction  += normDir(i)*cauchy(i,j)*normDir(i)*cauchy(i,j);
    }
  }
  traction = sqrt(traction);
  tau = sqrt(fabs(traction*traction-snor*snor));
    
  // Normal and tangential components
  bool ift;
  double smax;
  double seff;
  // sigma eff (Camacho and Ortiz)
  if(snor>=0.){
    seff = sqrt(snor*snor+_betaexpm2*tau*tau);
    smax = snor;
    ift = true;
  }
  else
  {
    double temp = fabs(tau)-_mu*fabs(snor);
    if (temp >0)
      seff = 1./_beta*temp;
    else
      seff = 0.;
    //no fracture in compression if mu<0
    if(_mu< 0) seff =0.;
    smax = tau;
    ift=false;
  }
  //broken check according to effective sttress (Ling Wu)
   seff = seff/(1.0-damage);

 /* if(damage > effDc && ift){
    Msg::Info("broken with damage %e, smax %e, traction %d",damage, smax,ift);*/
  if(seff> effsigc && ift){ 
    Msg::Info("broken with effective stress %e, damage %e, smax %e, traction %d", seff, damage, smax, ift);
    ipvfcur->broken();
  }
  if(ipvfcur->isbroken()){
    // create ipvariable for fracture (Allocation --> previous too)
    const FractureCohesive3DIPVariable *ipvfprev= static_cast<const FractureCohesive3DIPVariable*>(ipvprev);
    ipvfcur->setDataFromLaw(traction,snor,tau,remainingG,_beta,ift);
  }
  return ipvfcur->isbroken();
}
DelaminationNonLocalDamageLinearCohesive3DLaw::DelaminationNonLocalDamageLinearCohesive3DLaw(const int num, const double Gc, const double Dc,
		                                  const double beta, const double fractureStrengthFactorMin, const double fractureStrengthFactorMax) : 
	                                          NonLocalDamageLinearCohesive3DLaw(num,Gc,0,Dc,beta,fractureStrengthFactorMin,fractureStrengthFactorMax)
{

}

DelaminationNonLocalDamageLinearCohesive3DLaw::DelaminationNonLocalDamageLinearCohesive3DLaw(const DelaminationNonLocalDamageLinearCohesive3DLaw &source) : NonLocalDamageLinearCohesive3DLaw(source)
{

}

const bool DelaminationNonLocalDamageLinearCohesive3DLaw::checkBroken(dG3DIPVariableBase *ipvcur, const dG3DIPVariableBase *ipvprev) const
{
  FractureCohesive3DIPVariable *ipvfcur = static_cast<FractureCohesive3DIPVariable*>(ipvcur);

  SVector3 normDir = ipvprev->getConstRefToCurrentOutwardNormal()*(1./ipvprev->getConstRefToCurrentOutwardNormal().norm()); //so derivative cste
  SVector3 tmp(1.,1.,3.);
  SVector3 tgDir = crossprod(normDir,tmp);
  if(tgDir.norm() < 1.e-7)
  {
     SVector3 tmp2(-1.,1.,3.);
     tgDir = crossprod(normDir,tmp);
  }
  tgDir = tgDir*(1./tgDir.norm());

  double remainingG = _Gc;
  // value of sigma_c which depends on random factor
  const double effDc = ipvfcur->fractureStrengthFactor() * _Dc;
  const double effsigc = ipvfcur->fractureStrengthFactor() * _sigmac;
  //get damage
  double damage = ipvcur->get(IPField::DAMAGE);

  // Look upper fiber first
  STensor3 cauchy;
  ipvcur->getCauchyStress(cauchy);

  double snor =0.,tau =0.,traction=0.;
  for (int i=0;i < 3; i++)
  {
    for (int j=0; j < 3; j++)
    {
      snor += normDir(i)*cauchy(i,j)*normDir(j);
      traction  += normDir(i)*cauchy(i,j)*normDir(i)*cauchy(i,j);
    }
  }
  traction = sqrt(traction);
  tau = sqrt(fabs(traction*traction-snor*snor));
    
  // Normal and tangential components
  bool ift;
  double smax;
  double seff;
  // sigma eff (Camacho and Ortiz)
  if(snor>=0.){
    seff = sqrt(snor*snor+_betaexpm2*tau*tau);
    smax = snor;
    ift = true;
  }
  else
  {
    double temp = fabs(tau)-_mu*fabs(snor);
    if (temp >0)
      seff = 1./_beta*temp;
    else
      seff = 0.;
    //no fracture in compression if mu<0
    if(_mu< 0) seff =0.;
    smax = tau;
    ift=false;
  }
  //broken check according to effective sttress (Ling Wu)
   seff = seff/(1.0-damage);

  if(damage > effDc){
    Msg::Info("broken with damage %e, smax %e, traction %d",damage, smax,ift);
    ipvfcur->broken();
  }
  if(ipvfcur->isbroken()){
    // create ipvariable for fracture (Allocation --> previous too)
    const FractureCohesive3DIPVariable *ipvfprev= static_cast<const FractureCohesive3DIPVariable*>(ipvprev);
    ipvfcur->setDataFromLaw(traction,snor,tau,remainingG,_beta,ift);
  }
  return ipvfcur->isbroken();
}



TransverseIsotropicDG3DMaterialLaw::TransverseIsotropicDG3DMaterialLaw(
                                   const int num,const double rho,
                                   double E,const double nu,
                                   const double EA, const double GA, const double nu_minor,
                                   const double Ax, const double Ay, const double Az) :
                                                             dG3DMaterialLaw(num,rho,true),
                                                             _tilaw(num,E,nu,rho,EA,GA,nu_minor, Ax,Ay,Az), _E(E), _nu(nu), _nu_minor(nu_minor), _EA(EA), _GA(GA),
                                                             _ISTensor3(1.)
{
  fillElasticStiffness(E, nu, elasticStiffness);
}

TransverseIsotropicDG3DMaterialLaw::TransverseIsotropicDG3DMaterialLaw(const TransverseIsotropicDG3DMaterialLaw &source) : dG3DMaterialLaw(source), _tilaw(source._tilaw),
                                                                     _E(source._E), _nu(source._nu), _EA(source._EA),
                                                                     _GA(source._GA), _nu_minor(source._nu_minor),
                                                                     _ISTensor3(source._ISTensor3)
{

}

void TransverseIsotropicDG3DMaterialLaw::createIPState(IPStateBase* &ips,const bool* state_,const MElement *ele, const int nbFF_, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  TransverseIsotropicDG3DIPVariable(inter);
  IPVariable* ipv1 = new  TransverseIsotropicDG3DIPVariable(inter);
  IPVariable* ipv2 = new  TransverseIsotropicDG3DIPVariable(inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void TransverseIsotropicDG3DMaterialLaw::createIPVariable(IPVariable* &ipv,const MElement *ele, const int nbFF) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  TransverseIsotropicDG3DIPVariable(inter);
}


void TransverseIsotropicDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac)
{
  /* get ipvariable */
  TransverseIsotropicDG3DIPVariable* ipvcur;
  const TransverseIsotropicDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<TransverseIsotropicDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const TransverseIsotropicDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<TransverseIsotropicDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const TransverseIsotropicDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPTransverseIsotropic* q1 = ipvcur->getIPTransverseIsotropic();
  const IPTransverseIsotropic* q0 = ipvprev->getIPTransverseIsotropic();

  /* compute stress */
  _tilaw.constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),stiff);

  ipvcur->setRefToElasticTangentModuli(this->elasticStiffness);

}

AnisotropicDG3DMaterialLaw::AnisotropicDG3DMaterialLaw(
                                   const int num,const double rho,
				   const double Ex,const double Ey,const double Ez,
				   const double Vxy,const double Vxz,const double Vyz,
				   const double MUxy,const double MUxz,const double MUyz,
                                   const double alpha, const double beta, const double gamma) :
                                     	dG3DMaterialLaw(num,rho,true),
                                     	_tilaw(num,rho,Ex,Ey,Ez,Vxy,Vxz,Vyz,
                                            MUxy,MUxz,MUyz,alpha,beta,gamma),
				     	_ISTensor3(1.)
{
  double nu = _tilaw.poissonRatio();
  double mu = _tilaw.shearModulus();
  double E = 2.*mu*(1.+nu); //Where does this come from? To Have the E corresponding to the Vmax, MUmax
		            //In the linear & homogeneous & isotropic case ?
  fillElasticStiffness(E, nu, elasticStiffness); // WTF here? not sure to get it ...
}

AnisotropicDG3DMaterialLaw::AnisotropicDG3DMaterialLaw(const AnisotropicDG3DMaterialLaw &source) : 
								     dG3DMaterialLaw(source), 
								     _tilaw(source._tilaw),
                                                                     _ElasticityTensor(source._ElasticityTensor),
                                                                     _ISTensor3(source._ISTensor3)
{

}

void AnisotropicDG3DMaterialLaw::createIPState(IPStateBase* &ips,const bool* state_,const MElement *ele, const int nbFF_, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  AnisotropicDG3DIPVariable(inter);
  IPVariable* ipv1 = new  AnisotropicDG3DIPVariable(inter);
  IPVariable* ipv2 = new  AnisotropicDG3DIPVariable(inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void AnisotropicDG3DMaterialLaw::createIPVariable(IPVariable* &ipv,const MElement *ele, const int nbFF) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  AnisotropicDG3DIPVariable(inter);
}


void AnisotropicDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac)
{
  /* get ipvariable */
  AnisotropicDG3DIPVariable* ipvcur;
  const AnisotropicDG3DIPVariable* ipvprev;
  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<AnisotropicDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const AnisotropicDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<AnisotropicDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const AnisotropicDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPAnisotropic* q1 = ipvcur->getIPAnisotropic();
  const IPAnisotropic* q0 = ipvprev->getIPAnisotropic();

  /* compute stress */
  _tilaw.constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),stiff);

  ipvcur->setRefToElasticTangentModuli(this->elasticStiffness);

}


// function to generate random factor for fracture strength
// srand initiated in constructor of NonLinearMechSolver
static double frand(const double a,const double b){
  return (rand()/(double)RAND_MAX) * (b-a) + a;
}

FractureByCohesive3DLaw::FractureByCohesive3DLaw(const int num,const int nbulk,
                                                       const int ncoh) : dG3DMaterialLaw(num,0,false),
                                                                          fractureBy2Laws<dG3DMaterialLaw,Cohesive3DLaw>(num,nbulk,ncoh)

{
}
FractureByCohesive3DLaw::FractureByCohesive3DLaw(const FractureByCohesive3DLaw &source) :
                                                                          dG3DMaterialLaw(source),
                                                                          fractureBy2Laws<dG3DMaterialLaw,Cohesive3DLaw>(source)

{
}

void FractureByCohesive3DLaw::createIPState(IPStateBase* &ips,const bool* state_,
                                          const MElement *ele, const int nbFF_, const int gpt) const
{
  // FractureCohesive3DIPVariable are created only on interface element. On bulk element bulk IPvariable are created
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele!=NULL)
  {
    const double fscmin = _mfrac->getFractureStrengthFactorMin();
    const double fscmax = _mfrac->getFractureStrengthFactorMax();
    double fsc = frand(fscmin,fscmax);
    IPVariable* ipvi = new FractureCohesive3DIPVariable(fsc);
    IPVariable* ipv1 = new FractureCohesive3DIPVariable(fsc);
    IPVariable* ipv2 = new FractureCohesive3DIPVariable(fsc);
    FractureCohesive3DIPVariable *ipvf = static_cast<FractureCohesive3DIPVariable*>(ipvi);
    IPVariable* ipv=NULL;
    _mbulk->createIPVariable(ipv,ele,nbFF_);
    ipvf->setIPvBulk(ipv);
    ipvf = static_cast<FractureCohesive3DIPVariable*>(ipv1);
    ipv=NULL;
    _mbulk->createIPVariable(ipv,ele,nbFF_);
    ipvf->setIPvBulk(ipv);
    ipvf = static_cast<FractureCohesive3DIPVariable*>(ipv2);
    ipv=NULL;
    _mbulk->createIPVariable(ipv,ele,nbFF_);
    ipvf->setIPvBulk(ipv);

    // Why cohesive IPvariable are created here ?
    ipvf = static_cast<FractureCohesive3DIPVariable*>(ipvi);
    IPVariable* ipvc=NULL;
    _mfrac->createIPVariable(ipvc,ele,nbFF_);
    ipvf->setIPvFrac(ipvc);
    ipvf = static_cast<FractureCohesive3DIPVariable*>(ipv1);
    ipvc=NULL;
    _mfrac->createIPVariable(ipvc,ele,nbFF_);
    ipvf->setIPvFrac(ipvc);
    ipvf = static_cast<FractureCohesive3DIPVariable*>(ipv2);
    ipvc=NULL;
    _mfrac->createIPVariable(ipvc,ele,nbFF_);
    ipvf->setIPvFrac(ipvc);

    if(ips != NULL) delete ips;
    ips = new IP3State(state_,ipvi,ipv1,ipv2);
  }
  else
  {
    _mbulk->createIPState(ips,state_,ele,nbFF_,gpt);
  }
}

void FractureByCohesive3DLaw::createIPVariable(IPVariable* &ipv,const MElement *ele,const int nbFF_) const
{
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele!=NULL) // No fracture on bulk points
  {
    if(ipv != NULL) delete ipv;
    ipv = new FractureCohesive3DIPVariable();
    FractureCohesive3DIPVariable *ipvf = static_cast<FractureCohesive3DIPVariable*>(ipv);
    IPVariable* ipv2;
    _mbulk->createIPVariable(ipv2,ele,nbFF_);
    ipvf->setIPvBulk(ipv2);
    IPVariable* ipv3;
    _mfrac->createIPVariable(ipv3,ele,nbFF_);
    ipvf->setIPvFrac(ipv3);
  }
  else
  {
    _mbulk->createIPVariable(ipv,ele,nbFF_);
  }
}

void FractureByCohesive3DLaw::initialBroken(IPStateBase *ips) const
{
  FractureCohesive3DIPVariable *ipvfcur = static_cast<FractureCohesive3DIPVariable*>(ips->getState(IPStateBase::current));
  FractureCohesive3DIPVariable *ipvfprev = static_cast<FractureCohesive3DIPVariable*>(ips->getState(IPStateBase::previous));
  FractureCohesive3DIPVariable *ipvfinit = static_cast<FractureCohesive3DIPVariable*>(ips->getState(IPStateBase::initial));
  ipvfcur->broken();
  ipvfinit->broken();
  ipvfprev->broken();
  ipvfcur->setDataFromLaw(_mfrac->getSigmac(),_mfrac->getSigmac(),0.,_mfrac->getGc(),_mfrac->getBeta(),true);
  ipvfprev->setDataFromLaw(_mfrac->getSigmac(),_mfrac->getSigmac(),0.,_mfrac->getGc(),_mfrac->getBeta(),true);
  ipvfinit->setDataFromLaw(_mfrac->getSigmac(),_mfrac->getSigmac(),0.,_mfrac->getGc(),_mfrac->getBeta(),true);
}

void FractureByCohesive3DLaw::stress(IPVariable*ipv, const IPVariable*ipvp, const bool stiff, const bool checkfrac)
{
  _mbulk->stress(ipv, ipvp, stiff, checkfrac);
  FractureCohesive3DIPVariable *ipvcur = static_cast<FractureCohesive3DIPVariable*>(ipv);
  const FractureCohesive3DIPVariable *ipvprev = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
  // Now check for fracture on interface
  if((ipvcur->isInterface()) and (checkfrac))
  {
    if(!ipvprev->isbroken())
    { // no check if already broken
      bool isb = _mfrac->checkBroken(ipvcur,ipvprev);
      // Compute fracture data if broken SHOULD BE SOMEWHERE ELSE ??
      if(isb){
         //_mfrac->stress(ipv, ipvp, stiff, checkfrac);
      }
      else{
        ipvcur->nobroken();
      }
    }
    else
    {
      ipvcur->broken();
      _mfrac->stress(ipv, ipvp, stiff, checkfrac);
    }
  }
}






LinearThermoMechanicsDG3DMaterialLaw::LinearThermoMechanicsDG3DMaterialLaw(const int num,const double rho, 
						const double Ex, const double Ey, const double Ez, const double Vxy, const double Vxz,
						const double Vyz, const double MUxy, const double MUxz, const double MUyz,
			 		       const double alpha, const double beta, const double gamma , const double cv,
					       const double t0,const double Kx,const double Ky, const double Kz,const double alphax,
					       const double alphay,const double alphaz) :
                                                              dG3DMaterialLaw(num,rho,true)
{
  _lawLinearTM = new mlawLinearThermoMechanics(num,rho,Ex,Ey,Ez,Vxy,Vxz,Vyz,MUxy,MUxz,MUyz,alpha,beta,gamma,cv,t0,Kx,Ky,Kz,alphax,alphay,alphaz);

  double nu = _lawLinearTM->poissonRatio();
  double mu = _lawLinearTM->shearModulus();
  double E = 2.*mu*(1.+nu); //Where does this come from? To Have the E corresponding to the Vmax, MUmax
		            //In the linear & homogeneous & isotropic case ?
  
  linearK     = _lawLinearTM->getConductivityTensor();
  fillElasticStiffness(E, nu, elasticStiffness);
}

LinearThermoMechanicsDG3DMaterialLaw::LinearThermoMechanicsDG3DMaterialLaw(const LinearThermoMechanicsDG3DMaterialLaw &source) :
                                                             dG3DMaterialLaw(source)
{
_lawLinearTM  = new mlawLinearThermoMechanics(*(source._lawLinearTM));
 // _linearTM = new mlawLinearThermoMechanics(*(source._lawLinearTM));  or this
  linearK   = source.linearK;
  
}

void LinearThermoMechanicsDG3DMaterialLaw::createIPState(IPStateBase* &ips,const bool* state_,const MElement *ele, const int nbFF_, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new  LinearThermoMechanicsDG3DIPVariable(inter);
  IPVariable* ipv1 = new  LinearThermoMechanicsDG3DIPVariable(inter);
  IPVariable* ipv2 = new  LinearThermoMechanicsDG3DIPVariable(inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void LinearThermoMechanicsDG3DMaterialLaw::createIPVariable(IPVariable* &ipv,const MElement *ele, const int nbFF) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  LinearThermoMechanicsDG3DIPVariable(inter);
}


void LinearThermoMechanicsDG3DMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff, const bool checkfrac)
{
  /* get ipvariable */
  LinearThermoMechanicsDG3DIPVariable* ipvcur; //= static_cast < nonLocalDamageDG3DIPVariable *> (ipv);
  const LinearThermoMechanicsDG3DIPVariable* ipvprev; //= static_cast <const  nonLocalDamageDG3DIPVariable *> (ipvp);

  FractureCohesive3DIPVariable* ipvtmp = dynamic_cast<FractureCohesive3DIPVariable*>(ipv);
  if(ipvtmp !=NULL)
  {

    ipvcur = static_cast<LinearThermoMechanicsDG3DIPVariable*>(ipvtmp->getIPvBulk());
    const FractureCohesive3DIPVariable *ipvtmp2 = static_cast<const FractureCohesive3DIPVariable*>(ipvp);
    ipvprev = static_cast<const LinearThermoMechanicsDG3DIPVariable*>(ipvtmp2->getIPvBulk());
   }
  else
  {
    ipvcur = static_cast<LinearThermoMechanicsDG3DIPVariable*>(ipv);
    ipvprev = static_cast<const LinearThermoMechanicsDG3DIPVariable*>(ipvp);
  }

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();
  double temperature = ipvcur->getConstRefTotemperature();
  const SVector3& gradT= ipvcur->getConstRefTogradT();
        STensor3& dPdT=ipvcur->getRefTodPdT();
        SVector3& fluxT=ipvcur->getRefTothermalFlux();
        STensor3& dqdgradT=ipvcur->getRefTodthermalFluxdgradT();
  //const STensor3 *linearK()=ipvcur-> getConstRefTolinearK();
  
        STensor3& P=ipvcur->getRefToFirstPiolaKirchhoffStress();
        STensor43& Tangent=ipvcur->getRefToTangentModuli();
  
  /* data for J2 law */
  IPLinearThermoMechanics* q1 = ipvcur->getIPLinearThermoMechanics();
  const IPLinearThermoMechanics* q0 = ipvprev->getIPLinearThermoMechanics();
  
  
  /*_lawLinearTM->constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli())
                       ipvcur->getRefToDLocalPlasticStrainDStrain(),ipvcur->getRefToDStressDThermoMechanicsPlasticStrain(),
                      ipvcur->getRefToDLocalPlasticStrainDThermoMechanicsPlasticStrain(),stiff);*/
 // _lawLinearTM->constitutive(  F0,F1, q0,  q1,temperature,gradT, fluxT,dPdT,dqdgradT,stiff   ) ;// do I need  ,ipvcur->getRefToTangentModuli()
 // _lawLinearTM->constitutive( F0, Fn,P,  q0,q1,Tangent, stiff);
       
 _lawLinearTM->	constitutive(F0,F1, P,q0, q1,Tangent,temperature, gradT,fluxT,dPdT,dqdgradT,stiff);
  ipvcur->setRefToElasticTangentModuli(this->elasticStiffness);
  ipvcur->setRefTolinearK(this->linearK);
}

