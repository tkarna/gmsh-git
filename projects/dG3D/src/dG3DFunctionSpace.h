//
// C++ Interface: Function Space
//
// Description: FunctionSpace used (scalar lagragian function space in 3D)
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef DG3DFUNCTIONSPACE_H_
#define DG3DFUNCTIONSPACE_H_
#include "GModel.h"
#include "MElement.h"
#include "functionSpace.h"
#include "MInterfaceElement.h"
#include "dG3DDof3IntType.h"
#include "groupOfElements.h"
#include "GmshConfig.h"
#include "interFunctionSpace.h"
#include "MInterfaceTriangleN.h"
#include "MInterfaceQuadrangleN.h"
#include "ThreeDLagrangeFunctionSpace.h"

class g3DLagrangeFunctionSpace : public ThreeDLagrangeFunctionSpace{
 public :
  g3DLagrangeFunctionSpace(int id, int ncomp) : ThreeDLagrangeFunctionSpace(id, ncomp,false,false){};
  g3DLagrangeFunctionSpace(int id, int ncomp, int comp1) : ThreeDLagrangeFunctionSpace(id,ncomp,comp1,false,false){}
  g3DLagrangeFunctionSpace(int id, int ncomp, int comp1,int comp2) : ThreeDLagrangeFunctionSpace(id,ncomp, comp1,comp2,false,false){}
  g3DLagrangeFunctionSpace(int id, int ncomp, int comp1,int comp2,int comp3) : ThreeDLagrangeFunctionSpace(id,ncomp,comp1,comp2,comp3,false,false){}
  g3DLagrangeFunctionSpace(int id, int ncomp, int comp1,int comp2,int comp3,int comp4) : ThreeDLagrangeFunctionSpace(id,ncomp,comp1,comp2,comp3,comp4,false,false){}
  virtual ~g3DLagrangeFunctionSpace(){};

 protected :
  virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys)
  {
    int nk=ele->getNumVertices(); // return the number of vertices
    // negative type in mpi if the Dof are not located on this partition
    #if defined(HAVE_MPI) // small duplication to avoid multiple if in a loop
    if( (ele->getPartition() != 0) and (ele->getPartition() != Msg::GetCommRank() +1))
    {
      for (int j=0;j<_ncomp;++j)
        for (int i=0;i<nk;++i)
          keys.push_back(Dof(ele->getVertex(i)->getNum(),-dG3DDof3IntType::createTypeWithThreeInts(comp[j],_ifield,ele->getPartition())));
    }
    else
    #endif // HAVE_MPI
    {
      for (int j=0;j<_ncomp;++j)
        for (int i=0;i<nk;++i)
          keys.push_back(Dof(ele->getVertex(i)->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],_ifield,ele->getPartition())));
    }
  }
 public:
  // warning MPI used ??
  //virtual void getKeys(MVertex *ver, std::vector<Dof> &keys){
  //  for(int j=0;j<_ncomp;j++)
  //    keys.push_back(Dof(ver->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],_ifield)));
  //}
};

class dG3DLagrangeFunctionSpace : public ThreeDLagrangeFunctionSpace{
 protected:
  virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys){
    int nk=ele->getNumVertices(); // return the number of vertices
    #if defined(HAVE_MPI) // small duplication to avoid multiple if in a loop
    if( (ele->getPartition() != 0)and (ele->getPartition() != Msg::GetCommRank() +1))
    {
      for (int j=0;j<_ncomp;++j)
        for (int i=0;i<nk;i++)
          keys.push_back(Dof(ele->getNum(),-dG3DDof3IntType::createTypeWithThreeInts(comp[j],_ifield,i)));
    }
    else
    #endif // HAVE_MPI
    {
      for (int j=0;j<_ncomp;++j)
        for (int i=0;i<nk;i++)
          keys.push_back(Dof(ele->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],_ifield,i)));
    }
  }
 public :
  dG3DLagrangeFunctionSpace(int id, int ncomp) : ThreeDLagrangeFunctionSpace(id,ncomp,false,false){};
  dG3DLagrangeFunctionSpace(int id, int ncomp, int comp1) : ThreeDLagrangeFunctionSpace(id,ncomp,comp1,false,false){}
  dG3DLagrangeFunctionSpace(int id, int ncomp,int comp1,int comp2) : ThreeDLagrangeFunctionSpace(id,ncomp,comp1,comp2,false,false){}
  dG3DLagrangeFunctionSpace(int id, int ncomp,int comp1,int comp2, int comp3) : ThreeDLagrangeFunctionSpace(id,ncomp,comp1,comp2,comp3,false,false){}
  dG3DLagrangeFunctionSpace(int id, int ncomp,int comp1,int comp2, int comp3, int comp4) : ThreeDLagrangeFunctionSpace(id,ncomp,comp1,comp2,comp3,comp4,false,false){}
  virtual ~dG3DLagrangeFunctionSpace(){};
};

// for high order

class g3DhoDGFunctionSpace : public ThreeDLagrangeFunctionSpace{
/*
  protected:
    virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys){
      int nver = ele->getNumVertices();
      for (int i=0; i<_ncomp; i++){
        for (int j=0; j<nver; j++){
          keys.push_back(Dof(ele->getVertex(j)->getNum(),Dof::createTypeWithTwoInts(this->comp[i],_ifield)));
        };
      };
    };
*/
 public:
  g3DhoDGFunctionSpace(int id, int ncomp) : ThreeDLagrangeFunctionSpace(id,ncomp,true,true){};
  g3DhoDGFunctionSpace(int id, int ncomp, int comp1) : ThreeDLagrangeFunctionSpace(id,ncomp,comp1,true,true){};
  g3DhoDGFunctionSpace(int id, int ncomp, int comp1, int comp2) : ThreeDLagrangeFunctionSpace(id,ncomp,comp1,comp2,true,true){};
  g3DhoDGFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3) : ThreeDLagrangeFunctionSpace(id,ncomp,comp1,comp2,comp3,true,true){};
  virtual ~g3DhoDGFunctionSpace(){};
 protected :
  virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys)
  {
    int nk=ele->getNumVertices(); // return the number of vertices
    // negative type in mpi if the Dof are not located on this partition
    #if defined(HAVE_MPI) // small duplication to avoid multiple if in a loop
    if( (ele->getPartition() != 0) and (ele->getPartition() != Msg::GetCommRank() +1))
    {
      for (int j=0;j<_ncomp;++j)
        for (int i=0;i<nk;++i)
          keys.push_back(Dof(ele->getVertex(i)->getNum(),-dG3DDof3IntType::createTypeWithThreeInts(comp[j],_ifield,ele->getPartition())));
    }
    else
    #endif // HAVE_MPI
    {
      for (int j=0;j<_ncomp;++j)
        for (int i=0;i<nk;++i)
          keys.push_back(Dof(ele->getVertex(i)->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],_ifield,ele->getPartition())));
    }
  }
 public:
  // warning MPI used ?? Seems unused
  //virtual void getKeys(MVertex *ver, std::vector<Dof> &keys){
  //  for(int j=0;j<_ncomp;j++)
  //    keys.push_back(Dof(ver->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],_ifield,ele->getPartition())));
  //}
};

class dG3DhoDGFunctionSpace : public ThreeDLagrangeFunctionSpace{
 public:
  dG3DhoDGFunctionSpace(int id, int ncomp) : ThreeDLagrangeFunctionSpace(id,ncomp,true,true){};
  dG3DhoDGFunctionSpace(int id, int ncomp, int comp1) : ThreeDLagrangeFunctionSpace(id,ncomp,comp1,true,true){};
  dG3DhoDGFunctionSpace(int id, int ncomp, int comp1, int comp2) : ThreeDLagrangeFunctionSpace(id,ncomp,comp1,comp2,true,true){};
  dG3DhoDGFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3) : ThreeDLagrangeFunctionSpace(id,ncomp,comp1,comp2,comp3,true,true){};
  virtual ~dG3DhoDGFunctionSpace(){};
 protected: // avoid duplication of the following functions with dG3DLagrangeFunctionSpace HOW ??
  virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys){
    int nk=ele->getNumVertices(); // return the number of vertices
    #if defined(HAVE_MPI) // small duplication to avoid multiple if in a loop
    if( (ele->getPartition() != 0)and (ele->getPartition() != Msg::GetCommRank() +1))
    {
      for (int j=0;j<_ncomp;++j)
        for (int i=0;i<nk;i++)
          keys.push_back(Dof(ele->getNum(),-dG3DDof3IntType::createTypeWithThreeInts(comp[j],_ifield,i)));
    }
    else
    #endif // HAVE_MPI
    {
      for (int j=0;j<_ncomp;++j)
        for (int i=0;i<nk;i++)
          keys.push_back(Dof(ele->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],_ifield,i)));
    }
  }
};


#if 0
class g3DBoundaryConditionLagrangeFunctionSpace : public g3DLagrangeFunctionSpace{
 public:
  g3DBoundaryConditionLagrangeFunctionSpace(int id, int ncomp) : g3DLagrangeFunctionSpace(id, ncomp){}
  g3DBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1) : g3DLagrangeFunctionSpace(id,ncomp,comp1){}
  g3DBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2) : g3DLagrangeFunctionSpace(id, ncomp,comp1,comp2){}
  g3DBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3) : g3DLagrangeFunctionSpace(id, ncomp, comp1,comp2, comp3){}
  g3DBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3, int comp4) : g3DLagrangeFunctionSpace(id, ncomp, comp1,comp2, comp3, comp4){}
  virtual ~g3DBoundaryConditionLagrangeFunctionSpace(){};

  virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys)
  {
    int nk=ele->getNumVertices(); // return the number of vertices
    // negative type in mpi if the Dof are not located on this partition
    #if defined(HAVE_MPI) // small duplication to avoid multiple if in a loop
    if( (ele->getPartition() != 0) and (ele->getPartition() != Msg::GetCommRank() +1))
    {
      for (int j=0;j<_ncomp;++j)
        for (int i=0;i<nk;++i)
          keys.push_back(Dof(ele->getVertex(i)->getNum(),-dG3DDof3IntType::createTypeWithThreeInts(comp[j],_ifield,ele->getPartition())));
    }
    else
    #endif // HAVE_MPI
    {
      for (int j=0;j<_ncomp;++j)
        for (int i=0;i<nk;++i)
          keys.push_back(Dof(ele->getVertex(i)->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],_ifield,ele->getPartition())));
    }
  }
};
#endif //0

class g3DDirichletBoundaryConditionLagrangeFunctionSpace : public g3DLagrangeFunctionSpace{
 public:
 g3DDirichletBoundaryConditionLagrangeFunctionSpace(int id, int ncomp) : g3DLagrangeFunctionSpace(id, ncomp){}
  g3DDirichletBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1) : g3DLagrangeFunctionSpace(id,ncomp,comp1){}
  g3DDirichletBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2) : g3DLagrangeFunctionSpace(id, ncomp,comp1,comp2){}
  g3DDirichletBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3) : g3DLagrangeFunctionSpace(id, ncomp, comp1,comp2, comp3){}
  g3DDirichletBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3, int comp4) : g3DLagrangeFunctionSpace(id, ncomp, comp1,comp2, comp3, comp4){}
  virtual ~g3DDirichletBoundaryConditionLagrangeFunctionSpace(){};

  virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys)
  {
    int nk=ele->getNumVertices();
    // negative type in mpi if the Dof are not located on this partition
    // For BC on edge in the case of CG in // ele->getPartition() is equal to 0.
    // Therefore generate more keys to be sure to fix the dofs (There will be extra fixed dof but normally it doesn't matter)
    #if defined(HAVE_MPI) // small duplication to avoid multiple if in a loop
    if( (ele->getPartition() != 0) and (ele->getPartition() != Msg::GetCommRank() +1))
    {
      if(ele->getPartition()!=0){ // Sure than it will match
        for (int j=0;j<_ncomp;++j)
          for (int i=0;i<nk;++i)
            keys.push_back(Dof(ele->getVertex(i)->getNum(),-dG3DDof3IntType::createTypeWithThreeInts(comp[j],_ifield,ele->getPartition())));
      }
      else{ // match is not ensured --> generate keys for each partitions
        for(int p=0;p<=Msg::GetCommSize();p++){
          for (int j=0;j<_ncomp;++j)
            for (int i=0;i<nk;++i)
              keys.push_back(Dof(ele->getVertex(i)->getNum(),-dG3DDof3IntType::createTypeWithThreeInts(comp[j],_ifield,p)));
         }
      }
    }
    else
    #endif // HAVE_MPI
    {
      for (int j=0;j<_ncomp;++j)
        for (int i=0;i<nk;++i)
          keys.push_back(Dof(ele->getVertex(i)->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],_ifield,ele->getPartition())));
      if(ele->getPartition()==0) // Add the keys corresponding to this partition
      {
        for (int j=0;j<_ncomp;++j)
          for (int i=0;i<nk;++i)
            keys.push_back(Dof(ele->getVertex(i)->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],_ifield,Msg::GetCommRank()+1)));
      }
    }
  }
};

// It is tricky to account for CG parallel case especially for the msch project
// in which non partiotioned mesh are considered with Msg::GetCommSize()>1
// This works as long as only 1 GModel exists (Normally OK). Otherwise the Good Gmodel as
// to be selected before prescribing the BCs
class g3DNeumannBoundaryConditionLagrangeFunctionSpace : public g3DLagrangeFunctionSpace{
 protected:
   bool _parallelMesh;
   void setParallelMesh()
   {
    #if defined(HAVE_MPI)
     int partMax = 0;
     std::set<int>& mshPart = GModel::current()->getMeshPartitions();
     for(std::set<int>::const_iterator it = mshPart.begin(); it!=mshPart.end(); ++it)
     {
       if((*it) > partMax) partMax = (*it);
     }
     if(partMax>0)
     {
       _parallelMesh = true;
     }
     else
    #endif // HAVE_MPI
     {
       _parallelMesh = false;
     }
   }
 public:
  g3DNeumannBoundaryConditionLagrangeFunctionSpace(int id, int ncomp) : g3DLagrangeFunctionSpace(id, ncomp){this->setParallelMesh();}
  g3DNeumannBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1) : g3DLagrangeFunctionSpace(id,ncomp,comp1){this->setParallelMesh();}
  g3DNeumannBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2) : g3DLagrangeFunctionSpace(id, ncomp,comp1,comp2){this->setParallelMesh();}
  g3DNeumannBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3) : g3DLagrangeFunctionSpace(id, ncomp, comp1,comp2, comp3){this->setParallelMesh();}
  g3DNeumannBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3, int comp4) : g3DLagrangeFunctionSpace(id, ncomp, comp1,comp2, comp3, comp4){this->setParallelMesh();}
  virtual ~g3DNeumannBoundaryConditionLagrangeFunctionSpace(){};

  virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys)
  {
     // For Neumann BC we sure that the BC is applied on element that are on this partition.
     // Indeed Neumann BC are never applied on ghost element
     int p = (_parallelMesh) ? Msg::GetCommRank()+1 : 0;
     int nk=ele->getNumVertices();
     for (int j=0;j<_ncomp;++j)
       for (int i=0;i<nk;++i)
         keys.push_back(Dof(ele->getVertex(i)->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],_ifield,p)));
  }
};


class g3DDirichletBoundaryConditionLagrangeFunctionSpaceGhost : public g3DDirichletBoundaryConditionLagrangeFunctionSpace
{
 public:
  g3DDirichletBoundaryConditionLagrangeFunctionSpaceGhost(int id, int ncomp) : g3DDirichletBoundaryConditionLagrangeFunctionSpace(id, ncomp){}
  g3DDirichletBoundaryConditionLagrangeFunctionSpaceGhost(int id, int ncomp, int comp1) : g3DDirichletBoundaryConditionLagrangeFunctionSpace(id,ncomp,comp1){}
  g3DDirichletBoundaryConditionLagrangeFunctionSpaceGhost(int id, int ncomp, int comp1, int comp2) : g3DDirichletBoundaryConditionLagrangeFunctionSpace(id, ncomp,comp1,comp2){}
  g3DDirichletBoundaryConditionLagrangeFunctionSpaceGhost(int id, int ncomp, int comp1, int comp2, int comp3) : g3DDirichletBoundaryConditionLagrangeFunctionSpace(id, ncomp, comp1,comp2, comp3){}
  g3DDirichletBoundaryConditionLagrangeFunctionSpaceGhost(int id, int ncomp, int comp1, int comp2, int comp3, int comp4) : g3DDirichletBoundaryConditionLagrangeFunctionSpace(id, ncomp, comp1,comp2, comp3, comp4){}
  virtual ~g3DDirichletBoundaryConditionLagrangeFunctionSpaceGhost(){};

  // be sure that the type is negative !!
  virtual void getKeys(MElement *ele, std::vector<Dof> &keys){
    int nk=ele->getNumVertices();
    if(ele->getPartition()!=0){ // Sure than it will match
      for (int j=0;j<_ncomp;++j)
        for (int i=0;i<nk;++i)
          keys.push_back(Dof(ele->getVertex(i)->getNum(),-dG3DDof3IntType::createTypeWithThreeInts(comp[j],_ifield,ele->getPartition())));
    }
    else{ // match is not ensured --> generate keys for each partitions
      for(int p=0;p<=Msg::GetCommSize();p++){
        for (int j=0;j<_ncomp;++j)
          for (int i=0;i<nk;++i)
            keys.push_back(Dof(ele->getVertex(i)->getNum(),-dG3DDof3IntType::createTypeWithThreeInts(comp[j],_ifield,p)));
        }
    }
  }
};

class dG3DBoundaryConditionLagrangeFunctionSpace : public dG3DLagrangeFunctionSpace{
 protected:
  std::map<MElement*,std::pair<std::vector<int>,std::vector<int> > > mapLocalVertex;
  std::map<MElement*,std::pair<MElement*,MElement*> > mapInter;
 private:
  // constructor function
  void __init__(const groupOfElements *g, groupOfElements *gi, groupOfElements *vinter,groupOfElements *ghostMPI)
  {
    for(groupOfElements::elementContainer::iterator it=g->begin(); it!=g->end(); ++it)
    {
      MElement *e = *it;
      bool InternalEdge=false;
      std::vector<MVertex*> tabV;
      int nv=e->getNumVertices();
      for (int i=0;i<nv;++i) tabV.push_back(e->getVertex(i));
      MInterfaceElement *ielem=NULL;
      // Find the interface element linked with e
      int ind;
      std::vector<int> posfind;
      for(groupOfElements::elementContainer::const_iterator it2=vinter->begin();it2!=vinter->end();++it2)
      {
//debug
//        Msg::Error("Rank %d, face %d %d %d", Msg::GetCommRank(), (*it2)->getVertex(0)->getNum(),(*it2)->getVertex(1)->getNum(),(*it2)->getVertex(2)->getNum());
// end
        int nn = (*it2)->getNumVertices();
        if (e->getDim()==0)  //BC applied on a vertex
        {
          for(int i=0;i<nn;i++)
          {
            if(tabV[0]==(*it2)->getVertex(i))
            {
              ielem=dynamic_cast<MInterfaceElement*>(*it2);
              posfind.clear();
              posfind.push_back(i);
            }
          }
        }
        else // on a face of edge
        {
          if ((*it2)->getType() == TYPE_LIN){
            MVertex* v0 = (*it2)->getVertex(0);
            MVertex* v1 = (*it2)->getVertex(1);
            if (e->getDim() == 1){
              if ((tabV[0] == v0 and tabV[1] == v1) or
                  (tabV[0] == v1 and tabV[1] == v0)){
                ielem = dynamic_cast<MInterfaceElement*>(*it2);
                posfind.clear();
                for (int i=0;i<nn;++i)
                {
	          for(int j=0;j<nv;j++)
                  {
                    if(tabV[j] == (*it2)->getVertex(i))
                     posfind.push_back(i);
                  }
                }
              }
            }
          }
          else if((*it2)->getType() == TYPE_TRI)
          {
            MVertex* v0 = (*it2)->getVertex(0);
            MVertex* v1 = (*it2)->getVertex(1);
            MVertex* v2 = (*it2)->getVertex(2);
            int nn = (*it2)->getNumVertices();
            if( e->getDim()==1) // BC on an edge
            {
              if( (tabV[0] == v0 and tabV[1] == v1) or
                  (tabV[0] == v1 and tabV[1] == v2) or
                  (tabV[0] == v2 and tabV[1] == v0) or
                  (tabV[0] == v0 and tabV[1] == v2) or
                  (tabV[0] == v1 and tabV[1] == v0) or
                  (tabV[0] == v2 and tabV[1] == v1))
              {
                ielem=dynamic_cast<MInterfaceElement*>(*it2);
                posfind.clear();
                for (int i=0;i<nn;++i)
                {
	          for(int j=0;j<nv;j++)
                  {
                    if(tabV[j] == (*it2)->getVertex(i))
                     posfind.push_back(i);
                  }
                }
              }
            }
            else if (e->getDim()==2) // BC on a face
            {
                if( (tabV[0] == v0 and tabV[1] == v1 and tabV[2] == v2) or
                  (tabV[1] == v0 and tabV[2] == v1 and tabV[0] == v2) or
                  (tabV[2] == v0 and tabV[0] == v1 and tabV[1] == v2) or
                  (tabV[0] == v0 and tabV[1] == v2 and tabV[2] == v1) or
                  (tabV[1] == v0 and tabV[2] == v2 and tabV[0] == v1) or
                  (tabV[2] == v0 and tabV[0] == v2 and tabV[1] == v1) )
              {
                ielem=dynamic_cast<MInterfaceElement*>(*it2);
                posfind.clear();
                for (int i=0;i<nn;++i)
                {
	          for(int j=0;j<nv;j++)
                  {
                    if(tabV[j] == (*it2)->getVertex(i))
                     posfind.push_back(i);
                  }
                }
              }
            }
            else
              Msg::Error("dG3DBoundaryConditionLagrangeFunctionSpace: configuration not allowed");
          }
          else if((*it2)->getType() == TYPE_QUA)
          {
            MVertex* v0 = (*it2)->getVertex(0);
            MVertex* v1 = (*it2)->getVertex(1);
            MVertex* v2 = (*it2)->getVertex(2);
            MVertex* v3 = (*it2)->getVertex(3);
            int nn = (*it2)->getNumVertices();
            if(e->getDim()==1) // BC on an edge
          {
            if( (tabV[0] == v0 and tabV[1] == v1) or
                  (tabV[0] == v1 and tabV[1] == v2) or
                  (tabV[0] == v2 and tabV[1] == v3) or
                  (tabV[0] == v3 and tabV[1] == v0) or
                  (tabV[0] == v0 and tabV[1] == v3) or
                  (tabV[0] == v1 and tabV[1] == v0) or
                  (tabV[0] == v2 and tabV[1] == v1) or
                  (tabV[0] == v3 and tabV[1] == v2))
              {
                ielem=dynamic_cast<MInterfaceElement*>(*it2);
                posfind.clear();
                for (int i=0;i<nn;++i)
                {
	          for(int j=0;j<nv;j++)
                  {
                    if(tabV[j] == (*it2)->getVertex(i))
                     posfind.push_back(i);
                  }
                }
              }
            }
            else if (e->getDim()==2) // BC on a face
            {
              if( (tabV[0] == v0 and tabV[1] == v1 and tabV[2] == v2) or
                  (tabV[1] == v0 and tabV[2] == v1 and tabV[3] == v2) or
                  (tabV[2] == v0 and tabV[3] == v1 and tabV[0] == v2) or
                  (tabV[3] == v0 and tabV[0] == v1 and tabV[1] == v2) or
                  (tabV[0] == v0 and tabV[1] == v3 and tabV[2] == v2) or
                  (tabV[1] == v0 and tabV[2] == v3 and tabV[3] == v2) or
                  (tabV[2] == v0 and tabV[3] == v3 and tabV[0] == v2) or
                  (tabV[3] == v0 and tabV[0] == v3 and tabV[1] == v2) )
              {
                ielem=dynamic_cast<MInterfaceElement*>(*it2);
                posfind.clear();
                for (int i=0;i<nn;++i)
                {
	          for(int j=0;j<nv;j++)
                  {
                    if(tabV[j] == (*it2)->getVertex(i))
                     posfind.push_back(i);
                  }
                }
              }
            }
            else
              Msg::Error("dG3DBoundaryConditionLagrangeFunctionSpace: configuration not allowed");
          }
          else
            Msg::Error("dG3DBoundaryConditionLagrangeFunctionSpace: configuration not allowed");
        }
      }
      // If not find on external edge. Look in internal edge
      if(ielem==NULL)
      {
        for(groupOfElements::elementContainer::const_iterator it2=gi->begin();it2!=gi->end();++it2)
        {
//debug
//        Msg::Error("Rank %d, face inter %d %d %d", Msg::GetCommRank(), (*it2)->getVertex(0)->getNum(),(*it2)->getVertex(1)->getNum(),(*it2)->getVertex(2)->getNum());
// end
          int nn = (*it2)->getNumVertices();
          if (e->getDim()==0)  //BC applied on a vertex
          {
	    for(int i=0;i<nn;i++)
            {
              if(tabV[0]==(*it2)->getVertex(i))
              {
                ielem=dynamic_cast<MInterfaceElement*>(*it2);
                posfind.clear();
                posfind.push_back(i);
                InternalEdge=true;
              }
            }
          }
          else // on a face of edge
          {
            if ((*it2)->getType() == TYPE_LIN){
              MVertex* v0 = (*it2)->getVertex(0);
              MVertex* v1 = (*it2)->getVertex(1);
              if (e->getDim() == 1){
                if( (tabV[0] == v0 and tabV[1] == v1) or
                    (tabV[0] == v1 and tabV[1] == v0)){
                  ielem=dynamic_cast<MInterfaceElement*>(*it2);
                  posfind.clear();
                  for (int i=0;i<nn;++i)
                  {
	            for(int j=0;j<nv;j++)
                    {
                      if(tabV[j] == (*it2)->getVertex(i))
                       posfind.push_back(i);
                    }
                  }
                  InternalEdge=true;
                }
              }
            }
            else if((*it2)->getType() == TYPE_TRI)
            {
              MVertex* v0 = (*it2)->getVertex(0);
              MVertex* v1 = (*it2)->getVertex(1);
              MVertex* v2 = (*it2)->getVertex(2);
              int nn = (*it2)->getNumVertices();
              if(e->getDim()==1) // BC on an edge
	      {
	        if( (tabV[0] == v0 and tabV[1] == v1) or
                    (tabV[0] == v1 and tabV[1] == v2) or
                    (tabV[0] == v2 and tabV[1] == v0) or
                    (tabV[0] == v0 and tabV[1] == v2) or
                    (tabV[0] == v1 and tabV[1] == v0) or
                    (tabV[0] == v2 and tabV[1] == v1))
                {
                  ielem=dynamic_cast<MInterfaceElement*>(*it2);
                  posfind.clear();
                  for (int i=0;i<nn;++i)
                  {
	            for(int j=0;j<nv;j++)
                    {
                      if(tabV[j] == (*it2)->getVertex(i))
                       posfind.push_back(i);
                    }
                  }
                  InternalEdge=true;
                }
              }
              else if (e->getDim()==2) // BC on a face
              {
	        if( (tabV[0] == v0 and tabV[1] == v1 and tabV[2] == v2) or
                    (tabV[1] == v0 and tabV[2] == v1 and tabV[0] == v2) or
                    (tabV[2] == v0 and tabV[0] == v1 and tabV[1] == v2) or
                    (tabV[0] == v0 and tabV[1] == v2 and tabV[2] == v1) or
                    (tabV[1] == v0 and tabV[2] == v2 and tabV[0] == v1) or
                    (tabV[2] == v0 and tabV[0] == v2 and tabV[1] == v1) )
                {
                  ielem=dynamic_cast<MInterfaceElement*>(*it2);
                  posfind.clear();
                  for (int i=0;i<nn;++i)
                  {
	            for(int j=0;j<nv;j++)
                    {
                      if(tabV[j] == (*it2)->getVertex(i))
                       posfind.push_back(i);
                    }
                  }
                  InternalEdge=true;
                }
              }
              else
                Msg::Error("dG3DBoundaryConditionLagrangeFunctionSpace: configuration not allowed");
            }
            else if((*it2)->getType() == TYPE_QUA)
            {
              MVertex* v0 = (*it2)->getVertex(0);
              MVertex* v1 = (*it2)->getVertex(1);
              MVertex* v2 = (*it2)->getVertex(2);
              MVertex* v3 = (*it2)->getVertex(3);
              int nn = (*it2)->getNumVertices();
              if( e->getDim()==1 ) // BC on an edge
	      {
	        if( (tabV[0] == v0 and tabV[1] == v1) or
                    (tabV[0] == v1 and tabV[1] == v2) or
                    (tabV[0] == v2 and tabV[1] == v3) or
                    (tabV[0] == v3 and tabV[1] == v0) or
                    (tabV[0] == v0 and tabV[1] == v3) or
                    (tabV[0] == v1 and tabV[1] == v0) or
                    (tabV[0] == v2 and tabV[1] == v1) or
                    (tabV[0] == v3 and tabV[1] == v2))
                {
                  ielem=dynamic_cast<MInterfaceElement*>(*it2);
                  posfind.clear();
                  for (int i=0;i<nn;++i)
                  {
	            for(int j=0;j<nv;j++)
                    {
                      if(tabV[j] == (*it2)->getVertex(i))
                       posfind.push_back(i);
                    }
                  }
                  InternalEdge=true;
                }
              }
              else if (e->getDim()==2) // BC on a face
              {
	        if( (tabV[0] == v0 and tabV[1] == v1 and tabV[2] == v2) or
                    (tabV[1] == v0 and tabV[2] == v1 and tabV[3] == v2) or
                    (tabV[2] == v0 and tabV[3] == v1 and tabV[0] == v2) or
                    (tabV[3] == v0 and tabV[0] == v1 and tabV[1] == v2) or
                    (tabV[0] == v0 and tabV[1] == v3 and tabV[2] == v2) or
                    (tabV[1] == v0 and tabV[2] == v3 and tabV[3] == v2) or
                    (tabV[2] == v0 and tabV[3] == v3 and tabV[0] == v2) or
                    (tabV[3] == v0 and tabV[0] == v3 and tabV[1] == v2) )
                {
                  ielem=dynamic_cast<MInterfaceElement*>(*it2);
                  posfind.clear();
                  for (int i=0;i<nn;++i)
                  {
	            for(int j=0;j<nv;j++)
                    {
                      if(tabV[j] == (*it2)->getVertex(i))
                       posfind.push_back(i);
                    }
                  }
                  InternalEdge=true;
                }
              }
              else
                Msg::Error("dG3DBoundaryConditionLagrangeFunctionSpace: configuration not allowed");
            }
            else
              Msg::Error("dG3DBoundaryConditionLagrangeFunctionSpace: configuration not allowed");
          }
        }
      }
      if(ielem == NULL)
      {
        // not found yet look on ghost element
        bool ghostfind = false;
        if(ghostMPI != NULL) // NULL for Neumman BC
        {
          for(groupOfElements::elementContainer::const_iterator itE=ghostMPI->begin(); itE!=ghostMPI->end();++itE)
          {
//debug
//        Msg::Error("Rank %d, face ghostMPI %d %d %d", Msg::GetCommRank(), (*itE)->getVertex(0)->getNum(),(*itE)->getVertex(1)->getNum(),(*itE)->getVertex(2)->getNum());
// end
            MElement *gele = *itE;
            if(e->getNum() == gele->getNum()) // BC of same dim as domain's element
            {
              std::vector<int> vernum;
              int nvi = gele->getNumVertices();
              vernum.resize(nvi);
              for(int kk=0;kk<nvi;kk++){
                vernum[kk] = kk; // local number of vertex (and we take all vertices...)
              }
              mapLocalVertex[e] = std::pair<std::vector<int>,std::vector<int> >(vernum,vernum);
              mapInter[e] = std::pair<MElement*,MElement*>(gele,gele);
              ghostfind = true;
              break;
            }
            else if(e->getDim() == 0) // BC applied on a vertex
            {
              // look if there is a match between the vertex
              int nvi = gele->getNumVertices();
              for(int kk=0;kk<nvi;kk++)
              {
                if(e->getNum() == gele->getVertex(kk)->getNum())
                {
                  std::vector<int> vernum(1);
                  vernum.push_back(kk);
                  mapLocalVertex[e] = std::pair<std::vector<int>,std::vector<int> >(vernum,vernum);
                  mapInter[e] = std::pair<MElement*,MElement*>(gele,gele);
                  ghostfind = true;
                  break;
                }
                if(ghostfind) break;
              }
            }
            else if (e->getDim()==1 )// BC on an edge
            {
              int nedge = gele->getNumEdges();
              std::vector<MVertex*> verEdge;
              for(int kk=0;kk<nedge;kk++)
              {
                gele->getEdgeVertices(kk,verEdge);
                int nver = verEdge.size();
                std::vector<int> vernum;
                vernum.resize(nver);
                if((e->getVertex(0) == verEdge[0] and e->getVertex(1) == verEdge[1]) or
                   (e->getVertex(0) == verEdge[1] and e->getVertex(1) == verEdge[0]))
                {
                  if (gele->getDim() == 2){
                    switch(kk){
                     case 0 :
                      vernum[0] = 0;
                      vernum[1] = 1;
                      break;
                     case 1 :
                      vernum[0] = 1;
                      vernum[1] = 2;
                      break;
                     case 2 :
                      if(gele->getType()==TYPE_TRI){vernum[0]=2;vernum[1]=0;}
                      else{vernum[0]=2;vernum[1]=3;}
                      break;
                     case 3 :
                      vernum[0] = 3;
                      vernum[1] = 0;
                      break;
                     default : Msg::Error("Impossible to get local vertex number in this case");
                    }
                    // interior edge node
                    for(int j=2;j<vernum.size();j++)
                      vernum[j]=gele->getNumEdges()+kk*(gele->getPolynomialOrder()-1)+(j-2);
                  }
                  // find local vertex num (only tet and hex !!)
                  else if(gele->getType() == TYPE_TET)
                  {
                    switch(kk){
                      case 0 :
                        vernum[0] = 0;
                        vernum[1] = 1;
                        break;
                      case 1 :
                        vernum[0] = 1;
                        vernum[1] = 2;
                        break;
                      case 2 :
                        vernum[0] = 2;
                        vernum[1] = 0;
                        break;
                      case 3 :
                        vernum[0] = 3;
                        vernum[1] = 0;
                        break;
                      case 4 :
                        vernum[0] = 3;
                        vernum[1] = 2;
                        break;
                      case 5 :
                        vernum[0] = 3;
                        vernum[1] = 1;
                        break;
                      default : Msg::Error("Impossible to get local vertex number in this case");
                    }
                    // interior edge node
                    if( gele->getTypeForMSH() == MSH_TET_4)
                    {
                      //nothing
                    }
                    else if( gele->getTypeForMSH() == MSH_TET_10)
                    {
                      switch(kk){
                        case 0 :
                          vernum[2] = 4;
                          break;
                        case 1 :
                          vernum[2] = 5;
                          break;
                        case 2 :
                          vernum[2] = 6;
                          break;
                        case 3 :
                          vernum[2] = 7;
                          break;
                        case 4 :
                          vernum[2] = 8;
                          break;
                        case 5 :
                          vernum[2] = 9;
                          break;
                        default : Msg::Error("Impossible to get local vertex number in this case");
                      }
                    }
                    else
			Msg::Error("dG3DBoundaryConditionLagrangeFunctionSpace: Impossible to get local vertex number in this case");
                  }
                  else if(gele->getType() == TYPE_HEX)
                  {
                    switch(kk){
                      case 0 :
                        vernum[0] = 0;
                        vernum[1] = 1;
                        break;
                      case 1 :
                        vernum[0] = 0;
                        vernum[1] = 3;
                        break;
                      case 2 :
                        vernum[0] = 0;
                        vernum[1] = 4;
                        break;
                      case 3 :
                        vernum[0] = 1;
                        vernum[1] = 2;
                        break;
                      case 4 :
                        vernum[0] = 1;
                        vernum[1] = 5;
                        break;
                      case 5 :
                        vernum[0] = 2;
                        vernum[1] = 3;
                        break;
                      case 6 :
                        vernum[0] = 2;
                        vernum[1] = 6;
                        break;
                      case 7 :
                        vernum[0] = 3;
                        vernum[1] = 7;
                        break;
                      case 8 :
                        vernum[0] = 4;
                        vernum[1] = 5;
                        break;
                       case 9 :
                        vernum[0] = 4;
                        vernum[1] = 7;
                        break;
                      case 10 :
                        vernum[0] = 5;
                        vernum[1] = 6;
                        break;
                      case 11 :
                        vernum[0] = 6;
                        vernum[1] = 7;
                        break;
                      default : Msg::Error("Impossible to get local vertex number in this case");
                    }
                    // interior edge node
                    if( gele->getTypeForMSH() == MSH_HEX_8)
                    {
                      //nothing
                    }
                    else if( gele->getTypeForMSH() == MSH_HEX_27)
                    {
                      switch(kk){
                        case 0 :
                          vernum[2] = 8;
                          break;
                        case 1 :
                          vernum[2] = 9;
                          break;
                        case 2 :
                          vernum[2] = 10;
                          break;
                        case 3 :
                          vernum[2] = 11;
                          break;
                        case 4 :
                          vernum[2] = 12;
                          break;
                        case 5 :
                          vernum[0] = 13;
                          break;
                        case 6 :
                          vernum[2] = 14;
                          break;
                        case 7 :
                          vernum[2] = 15;
                          break;
                        case 8 :
                          vernum[2] = 16;
                          break;
                        case 9 :
                          vernum[2] = 17;
                          break;
                        case 10 :
                          vernum[2] = 18;
                          break;
                        case 11 :
                          vernum[2] = 19;
                          break;
                        default : Msg::Error("Impossible to get local vertex number in this case");
                      }
                    }
                    else
                      Msg::Error("dG3DBoundaryConditionLagrangeFunctionSpace: Impossible to get local vertex number in this case");
                  }
		              else
		                Msg::Error("dG3DBoundaryConditionLagrangeFunctionSpace: Impossible to get local vertex number in this case");
                  mapLocalVertex[e] = std::pair<std::vector<int>,std::vector<int> >(vernum,vernum);
                  mapInter[e] = std::pair<MElement*,MElement*>(gele,gele);
                  ghostfind = true;
                  break;
                }
                if(ghostfind) break;
              }
            }
            else if (e->getDim()==2 )// BC on a face
            {
              int nface = gele->getNumFaces();
              std::vector<MVertex*> verFace;
              for(int kk=0;kk<nface;kk++)
              {
                verFace.resize(0);
                /*
                if(gele->getTypeForMSH() == MSH_TET_10)
                {
                   std::vector<MVertex*> vvv;
                   gele->getFaceVertices(kk,vvv);
                   verFace.push_back(vvv[0]);
                   verFace.push_back(vvv[1]);
                   verFace.push_back(vvv[2]);
                   if(kk==0)
                   {
                     verFace.push_back(vvv[5]);
                     verFace.push_back(vvv[4]);
                     verFace.push_back(vvv[3]);
                   }
                   else if(kk==1)
                   {
                     verFace.push_back(vvv[3]);
                     verFace.push_back(vvv[5]);
                     verFace.push_back(vvv[4]);
                   }
                   else if(kk==2)
                   {
                     verFace.push_back(vvv[4]);
                     verFace.push_back(vvv[5]);
                     verFace.push_back(vvv[3]);
                   }
                   else
                   {
                     verFace.push_back(vvv[5]);
                     verFace.push_back(vvv[3]);
                     verFace.push_back(vvv[4]);
                   }
                }
                else
                */

                gele->getFaceVertices(kk,verFace);
                int nver = verFace.size();
                std::vector<int> vernum;
                vernum.resize(nver);
                if(gele->getType() == TYPE_TET && e->getType() == TYPE_TRI)
                {
                  if((e->getVertex(0) == verFace[0] and e->getVertex(1) == verFace[1] and e->getVertex(2) == verFace[2]) or
                     (e->getVertex(0) == verFace[1] and e->getVertex(1) == verFace[2] and e->getVertex(2) == verFace[0]) or
                     (e->getVertex(0) == verFace[2] and e->getVertex(1) == verFace[0] and e->getVertex(2) == verFace[1]) or
		                 (e->getVertex(0) == verFace[0] and e->getVertex(1) == verFace[2] and e->getVertex(2) == verFace[1]) or
		                 (e->getVertex(0) == verFace[1] and e->getVertex(1) == verFace[0] and e->getVertex(2) == verFace[2]) or
		                 (e->getVertex(0) == verFace[2] and e->getVertex(1) == verFace[1] and e->getVertex(2) == verFace[0]))
                  {
                    // find local vertex num (only tet and hex !!)
                    switch(kk){
                      case 0 :
                        vernum[0] = 0;
                        vernum[1] = 2;
                        vernum[2] = 1;
                        break;
                      case 1 :
                        vernum[0] = 0;
                        vernum[1] = 1;
                        vernum[2] = 3;
                        break;
                      case 2 :
                        vernum[0] = 0;
                        vernum[1] = 3;
                        vernum[2] = 2;
                        break;
                      case 3 :
                        vernum[0] = 3;
                        vernum[1] = 1;
                        vernum[2] = 2;
                        break;
                      default : Msg::Error("Impossible to get local vertex number in this case");
                    }
                    // interior edge node
                    if( gele->getTypeForMSH() == MSH_TET_4)
                    {
                      //nothing
                    }
                    else if( gele->getTypeForMSH() == MSH_TET_10)
                    {
                      switch(kk){
                        case 0 :
                          vernum[3] = 6;
                          vernum[4] = 5;
                          vernum[5] = 4;
                          break;
                        case 1 :
                          vernum[3] = 4;
                          vernum[4] = 9;
                          vernum[5] = 7;
                          break;
                        case 2 :
                          vernum[3] = 7;
                          vernum[4] = 8;
                          vernum[5] = 6;
                          break;
                        case 3 :
                          vernum[3] = 9;
                          vernum[4] = 5;
                          vernum[5] = 8;
                          break;
                        default : Msg::Error("Impossible to get local vertex number in this case");
                      }
                    }
                    else
			                Msg::Error("dG3DBoundaryConditionLagrangeFunctionSpace: Impossible to get local vertex number in this case");
                    mapLocalVertex[e] = std::pair<std::vector<int>,std::vector<int> >(vernum,vernum);
                    mapInter[e] = std::pair<MElement*,MElement*>(gele,gele);
                    ghostfind = true;
                    break;
                  }
                }
		            else if(gele->getType() == TYPE_HEX && e->getType() == TYPE_QUA)
                {
                  if((e->getVertex(0) == verFace[0] and e->getVertex(1) == verFace[1] and e->getVertex(2) == verFace[2]) or
                     (e->getVertex(0) == verFace[1] and e->getVertex(1) == verFace[2] and e->getVertex(2) == verFace[3]) or
                     (e->getVertex(0) == verFace[2] and e->getVertex(1) == verFace[3] and e->getVertex(2) == verFace[0]) or
		                 (e->getVertex(0) == verFace[3] and e->getVertex(1) == verFace[0] and e->getVertex(2) == verFace[1]) or
		                 (e->getVertex(0) == verFace[0] and e->getVertex(1) == verFace[3] and e->getVertex(2) == verFace[2]) or
                     (e->getVertex(0) == verFace[1] and e->getVertex(1) == verFace[0] and e->getVertex(2) == verFace[3]) or
		                 (e->getVertex(0) == verFace[2] and e->getVertex(1) == verFace[1] and e->getVertex(2) == verFace[0]) or
                     (e->getVertex(0) == verFace[3] and e->getVertex(1) == verFace[2] and e->getVertex(2) == verFace[1]))
                  {
                    switch(kk){
                      case 0 :
                        vernum[0] = 0;
                        vernum[1] = 3;
                        vernum[2] = 2;
                        vernum[3] = 1;
                        break;
                      case 1 :
                        vernum[0] = 0;
                        vernum[1] = 1;
                        vernum[2] = 5;
                        vernum[3] = 4;
                        break;
                      case 2 :
                        vernum[0] = 0;
                        vernum[1] = 4;
                        vernum[2] = 7;
                        vernum[3] = 3;
                        break;
                      case 3 :
                        vernum[0] = 1;
                        vernum[1] = 2;
                        vernum[2] = 6;
                        vernum[3] = 5;
                        break;
                      case 4 :
                        vernum[0] = 2;
                        vernum[1] = 3;
                        vernum[2] = 7;
                        vernum[3] = 6;
                        break;
                      case 5 :
                        vernum[0] = 4;
                        vernum[1] = 5;
                        vernum[2] = 6;
                        vernum[3] = 7;
                        break;
                      default : Msg::Error("Impossible to get local vertex number in this case");
                    }
                    // interior edge node
                    if( gele->getTypeForMSH() == MSH_HEX_8)
                    {
                      //nothing
                    }
                    else if( gele->getTypeForMSH() == MSH_HEX_27)
                    {
                      switch(kk){
                        case 0 :
                          vernum[4] = 9;
                          vernum[5] = 13;
                          vernum[6] = 11;
                          vernum[7] = 8;
                          vernum[8] = 20;
                          break;
                        case 1 :
                          vernum[4] = 8;
                          vernum[5] = 12;
                          vernum[6] = 16;
                          vernum[7] = 10;
                          vernum[8] = 21;
                          break;
                        case 2 :
                          vernum[4] = 10;
                          vernum[5] = 17;
                          vernum[6] = 15;
                          vernum[7] = 9;
                          vernum[8] = 22;
                          break;
                        case 3 :
                          vernum[4] = 11;
                          vernum[5] = 14;
                          vernum[6] = 18;
                          vernum[7] = 12;
                          vernum[8] = 23;
                          break;
                        case 4 :
                          vernum[4] = 13;
                          vernum[5] = 15;
                          vernum[6] = 19;
                          vernum[7] = 14;
                          vernum[8] = 24;
                          break;
                        case 5 :
                          vernum[4] = 16;
                          vernum[5] = 18;
                          vernum[6] = 19;
                          vernum[7] = 17;
                          vernum[8] = 25;
                          break;
                        default : Msg::Error("Impossible to get local vertex number in this case");
                      }
                    }
                    else
			                Msg::Error("dG3DBoundaryConditionLagrangeFunctionSpace: Impossible to get local vertex number in this case");
                    mapLocalVertex[e] = std::pair<std::vector<int>,std::vector<int> >(vernum,vernum);
                    mapInter[e] = std::pair<MElement*,MElement*>(gele,gele);
                    ghostfind = true;
                    break;
                  }
                }
                else
		              Msg::Error("dG3DBoundaryConditionLagrangeFunctionSpace: Impossible to get local vertex number in this case");
                if(ghostfind) break;
              }
            }
          }
        }
        if(!ghostfind)
        {
          Msg::Error("Impossible to fix dof for element %d on rank %d",e->getNum(),Msg::GetCommRank());
          Msg::Error("I am on Proc  %d", Msg::GetCommRank());
          Msg::Error("Vertex %d", e->getVertex(0)->getNum());
          Msg::Error("Vertex %d", e->getVertex(1)->getNum());
          Msg::Error("Vertex %d", e->getVertex(2)->getNum());
       }
      }
      else{
        int nvi=ielem->getNumVertices();
        std::vector<int> vernum;
        std::vector<int> vernumtmp;
        vernumtmp.resize(nvi);
        ielem->getLocalVertexNum(0,vernumtmp);
        for(int i=0; i< posfind.size(); i++)
        {
          vernum.push_back(vernumtmp[posfind[i]]);
        }
        //if(nv == 1){ // if boundary condition is applied on a vertex
        //  int temp = vernum[posfind];
        //  vernum.clear();
        //  vernum.push_back(temp);
        //}
        if(InternalEdge){
          std::vector<int> vernum2;
          std::vector<int> vernum2tmp;
          vernum2tmp.resize(nvi);
          ielem->getLocalVertexNum(1,vernum2tmp);
          for(int i=0; i< posfind.size(); i++)
          {
            vernum.push_back(vernumtmp[posfind[i]]);
          }
          //if(nv == 1){ // if boundary condition is applied on a vertex
          //  int temp = vernum2[posfind];
          //  vernum2.clear();
          //  vernum2.push_back(temp);
          //}
          mapLocalVertex[e] = std::pair<std::vector<int>,std::vector<int> >(vernum,vernum2);
          mapInter[e] = std::pair<MElement*,MElement*>(ielem->getElem(0),ielem->getElem(1));
        }
        else{
          mapLocalVertex[e] = std::pair<std::vector<int>,std::vector<int> >(vernum,vernum);
          mapInter[e] = std::pair<MElement*,MElement*>(ielem->getElem(0),ielem->getElem(0));
        }
      }
    }
  }
 public:

  virtual ~dG3DBoundaryConditionLagrangeFunctionSpace(){}
  // Copy the constructor function
  dG3DBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, const groupOfElements *g, groupOfElements *gi,
                                          groupOfElements *vinter, groupOfElements *ghostMPI) : dG3DLagrangeFunctionSpace(id, ncomp)
  {
    this->__init__(g,gi,vinter,ghostMPI);
  }
  dG3DBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, const groupOfElements *g, groupOfElements *gi,
                                          groupOfElements *vinter, groupOfElements *ghostMPI) : dG3DLagrangeFunctionSpace(id, ncomp,comp1)
  {
    this->__init__(g,gi,vinter,ghostMPI);
  }
  dG3DBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2, const groupOfElements *g, groupOfElements *gi,
                                          groupOfElements *vinter, groupOfElements *ghostMPI) : dG3DLagrangeFunctionSpace(id, ncomp,comp1,comp2)
  {
    this->__init__(g,gi,vinter,ghostMPI);
  }
  dG3DBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3, const groupOfElements *g, groupOfElements *gi,
                                          groupOfElements *vinter, groupOfElements *ghostMPI) : dG3DLagrangeFunctionSpace(id, ncomp,comp1,comp2,comp3)
  {
    this->__init__(g,gi,vinter,ghostMPI);
  }
  dG3DBoundaryConditionLagrangeFunctionSpace(int id, int ncomp, int comp1, int comp2, int comp3, int comp4, const groupOfElements *g, groupOfElements *gi,
                                          groupOfElements *vinter, groupOfElements *ghostMPI) : dG3DLagrangeFunctionSpace(id, ncomp,comp1,comp2,comp3,comp4)
  {
    this->__init__(g,gi,vinter,ghostMPI);
  }

  virtual void getKeys(MInterfaceElement *ielem, std::vector<Dof> &keys){
    Msg::Error("Impossible to get keys on interface element for a Dirichlet Boundary Conditions");
  }
  virtual void getKeys(MElement *e, std::vector<Dof> &keys)
  {
    std::map<MElement*,std::pair<MElement*,MElement*> >::iterator itele=mapInter.find(e);
    MElement *ele1 = itele->second.first;
    MElement *ele2 = itele->second.second;
    std::map<MElement*,std::pair<std::vector<int>,std::vector<int> > >::iterator itvec=mapLocalVertex.find(e);
    int nbcomp=comp.size();
    #if defined(HAVE_MPI) // small duplication to avoid multiple if in a loop
    if( (ele1->getPartition() != 0)and (ele1->getPartition() != Msg::GetCommRank() +1))
    {
      for (int j=0;j<nbcomp;++j)
        for (int i=0;i<itvec->second.first.size();i++)
        {
          keys.push_back(Dof(ele1->getNum(),-dG3DDof3IntType::createTypeWithThreeInts(comp[j],this->getId(),itvec->second.first[i])));
        }
    }
    else
    #endif // HAVE_MPI
    {
      for (int j=0;j<nbcomp;++j)
        for (int i=0;i<itvec->second.first.size();i++)
        {
          keys.push_back(Dof(ele1->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],this->getId(),itvec->second.first[i])));
      }
    }
    // Keys of plus element in case of a internal boundary conditions
    if(ele1 != ele2){
      #if defined(HAVE_MPI) // idem
      if( (ele2->getPartition() != 0)and (ele2->getPartition() != Msg::GetCommRank() +1))
      {
        for (int j=0;j<nbcomp;++j)
          for (int i=0;i<itvec->second.second.size();i++)
          {
            keys.push_back(Dof(ele2->getNum(),-dG3DDof3IntType::createTypeWithThreeInts(comp[j],this->getId(),itvec->second.second[i])));
          }
      }
      else
      #endif
      {
        for (int j=0;j<nbcomp;++j)
          for (int i=0;i<itvec->second.second.size();i++)
          {
            keys.push_back(Dof(ele2->getNum(),dG3DDof3IntType::createTypeWithThreeInts(comp[j],this->getId(),itvec->second.second[i])));
          }
      }
    }
  }
};

// Space of the interface with acces to the two function space
class dG3DLagrangeBetween2DomainsFunctionSpace : public ThreeDLagrangeFunctionSpace, public interFunctionSpace{
 protected:
  FunctionSpaceBase *spaceMinus;
  FunctionSpaceBase *spacePlus;
  virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys){
    Msg::Error("Impossible to get key for an element on a interface Domain");
  }
 public:
  dG3DLagrangeBetween2DomainsFunctionSpace(int ncomp, FunctionSpaceBase*sp1, FunctionSpaceBase *sp2) :
                                            ThreeDLagrangeFunctionSpace(0, ncomp,false,false), spaceMinus(sp1), spacePlus(sp2){}
  virtual ~dG3DLagrangeBetween2DomainsFunctionSpace(){}

  virtual void getKeys(MInterfaceElement *iele, std::vector<Dof> &keys){
    spaceMinus->getKeys(iele->getElem(0),keys);
    spacePlus->getKeys(iele->getElem(1),keys);
  }
  virtual void getKeys(MElement *ele, std::vector<Dof> &keys){
    // As all element are interface element on an interface domain
    MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(ele);
    this->getKeys(iele,keys);
  }
  // special function of interFunctionSpace
  virtual FunctionSpaceBase* getMinusSpace()const {return spaceMinus;}
  virtual FunctionSpaceBase* getPlusSpace() const {return spacePlus;}
  virtual void getNumKeys(MInterfaceElement *ele, int &numMinus, int &numPlus) const
  {
    numMinus = spaceMinus->getNumKeys(ele->getElem(0));
    numPlus = spacePlus->getNumKeys(ele->getElem(1));
  }
  virtual void getKeys(MInterfaceElement *ele, std::vector<Dof> &Rminus,std::vector<Dof> &Rplus) const
  {
    spaceMinus->getKeys(ele->getElem(0),Rminus);
    spacePlus->getKeys(ele->getElem(1),Rplus);
  }
};

#endif // DG3DFUNCTIONSPACE_H_
