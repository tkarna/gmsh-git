//
// C++ Interface: ipvariable
//
// Description: Class with definition of ipvarible for dG3D
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "dG3DIPVariable.h"
#include "ipstate.h"
#include "ipField.h"

dG3DIPVariableBase::dG3DIPVariableBase(const dG3DIPVariableBase &source) : ipFiniteStrain(source)
{}

dG3DIPVariableBase & dG3DIPVariableBase::operator = (const IPVariable &source)
{
  ipFiniteStrain::operator=(source);
  return *this;
}


dG3DIPVariable::dG3DIPVariable(const bool oninter) : dG3DIPVariableBase()
{
  _oninter = oninter;
  deformationGradient(0,0) = 1.;
  deformationGradient(1,1) = 1.;
  deformationGradient(2,2) = 1.;
}

dG3DIPVariable::dG3DIPVariable(const dG3DIPVariable &source) : dG3DIPVariableBase(source)
{
    _oninter                  = source._oninter;
    deformationGradient       = source.deformationGradient;
    firstPiolaKirchhoffStress = source.firstPiolaKirchhoffStress;
    tangentModuli             = source.tangentModuli;
    elasticTangentModuli      = source.elasticTangentModuli;
    referenceOutwardNormal    = source.referenceOutwardNormal;
    currentOutwardNormal      = source.currentOutwardNormal;
    interfaceForce            = source.interfaceForce;
    DInterfaceForceDjump      = source.DInterfaceForceDjump;
    jump                      = source.jump;
}
dG3DIPVariable & dG3DIPVariable::operator = (const IPVariable &_source)
{
    dG3DIPVariableBase::operator=(_source);
    const dG3DIPVariable *source=dynamic_cast<const dG3DIPVariable *> (&_source);
    if(source!=NULL)
    {
      _oninter                  = source->_oninter;
      deformationGradient       = source->deformationGradient;
      firstPiolaKirchhoffStress = source->firstPiolaKirchhoffStress;
      tangentModuli             = source->tangentModuli;
      elasticTangentModuli      = source->elasticTangentModuli;
      referenceOutwardNormal    = source->referenceOutwardNormal;
      currentOutwardNormal      = source->currentOutwardNormal;
      interfaceForce            = source->interfaceForce;
      DInterfaceForceDjump      = source->DInterfaceForceDjump;
      jump                      = source->jump;
    }
    return *this;
}

double dG3DIPVariable::getJ() const
{
  double mat[3][3];
  for(int i = 0; i < 3; i++)
    for(int j = 0; j < 3; j++)
      mat[i][j] = getConstRefToDeformationGradient()(i,j);
  return det3x3(mat);

}
void dG3DIPVariable::getCauchyStress(STensor3 &cauchy) const
{

  cauchy=0.;
  double detJ = getJ();
  if (detJ < 1.e-12) Msg::Error("Negative Jacobian");
  for (int i = 0; i< 3; i ++)
  {
    for (int j = 0; j< 3; j ++)
    {
       for(int k =0; k <3; k++)
       {
         cauchy(i,j) += getConstRefToFirstPiolaKirchhoffStress()(i,k)*getConstRefToDeformationGradient()(j,k)/detJ;
       }
    }
  }
}

double dG3DIPVariable::get(const int comp) const
{
  STensor3 cauchy;
  getCauchyStress(cauchy);
  if(comp == IPField::SIG_XX)
    return cauchy(0,0);
  else if(comp == IPField::SIG_YY)
    return cauchy(1,1);
  else if(comp == IPField::SIG_ZZ)
    return cauchy(2,2);
  else if(comp == IPField::SIG_XY)
    return cauchy(0,1);
  else if(comp == IPField::SIG_YZ)
    return cauchy(1,2);
  else if(comp == IPField::SIG_XZ)
    return cauchy(0,2);
  else if(comp == IPField::SVM) // von Mises
  {
    return vonMises();
  }
  else if(comp == IPField::STRAIN_XX)
    return getConstRefToDeformationGradient()(0,0)-1.;
  else if(comp == IPField::STRAIN_YY)
    return getConstRefToDeformationGradient()(1,1)-1.;
  else if(comp == IPField::STRAIN_ZZ)
    return getConstRefToDeformationGradient()(2,2)-1.;
  else if(comp == IPField::STRAIN_XY)
    return (getConstRefToDeformationGradient()(0,1)+getConstRefToDeformationGradient()(1,0))/2.;
  else if(comp == IPField::STRAIN_YZ)
    return (getConstRefToDeformationGradient()(1,2)+getConstRefToDeformationGradient()(2,1))/2.;
  else if(comp == IPField::STRAIN_XZ)
    return (getConstRefToDeformationGradient()(0,2)+getConstRefToDeformationGradient()(2,0))/2.;
  else
     return 0.;

}
double dG3DIPVariable::defoEnergy() const
{
  double ene=0;
  double detJ = getJ();
  if (detJ < 1.e-12) Msg::Error("Negative Jacobian");
  // default implementation for small deformations
  STensor3 cauchy;
  getCauchyStress(cauchy);
  STensor3 E;
  for (int i = 0; i< 3; i ++)
  {
    for (int j = 0; j< 3; j ++)
    {
     E(i,j)  = getConstRefToDeformationGradient()(i,j)/2.+getConstRefToDeformationGradient()(j,i)/2.;
     if(i == j) E(i,j)-=1.;
    }
  }
  for (int i = 0; i< 3; i ++)
  {
    for (int j = 0; j< 3; j ++)
    {
       ene += cauchy(i,j)*E(i,j)/2.;
    }
  }
  return ene;
}
double dG3DIPVariable::plasticEnergy() const
{
  return 0.;
}
double dG3DIPVariable::vonMises() const
{
  STensor3 cauchy;
  getCauchyStress(cauchy);
  double svm= (cauchy(0,0)-cauchy(1,1))*(cauchy(0,0)-cauchy(1,1))+(cauchy(0,0)-cauchy(2,2))*(cauchy(0,0)-cauchy(2,2))+
               (cauchy(2,2)-cauchy(1,1))*(cauchy(2,2)-cauchy(1,1));
     svm += 6.*(cauchy(0,1)*cauchy(1,0)+cauchy(0,2)*cauchy(2,0)+cauchy(2,1)*cauchy(1,2));
     svm /= 2.;
     svm = sqrt(svm);
     return svm;
}

J2LinearDG3DIPVariable::J2LinearDG3DIPVariable(const mlawJ2linear &_j2law, const bool oninter) : dG3DIPVariable(oninter)
{
  _j2ipv = new   IPJ2linear(_j2law.getJ2IsotropicHardening());
}
J2LinearDG3DIPVariable::J2LinearDG3DIPVariable(const J2LinearDG3DIPVariable &source) : dG3DIPVariable(source)
{
  _j2ipv->operator=(*source._j2ipv);
}
J2LinearDG3DIPVariable& J2LinearDG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const J2LinearDG3DIPVariable* src = dynamic_cast<const J2LinearDG3DIPVariable*>(&source);
  if(src != NULL)
    _j2ipv->operator=(*src->_j2ipv);
  return *this;
}
double J2LinearDG3DIPVariable::get(const int comp) const
{
  if(comp == IPField::PLASTICSTRAIN)
  {
    return _j2ipv->_j2lepspbarre;
  }
  else
  {
    return dG3DIPVariable::get(comp);
  }
}
double  J2LinearDG3DIPVariable::defoEnergy() const
{
  return getIPJ2linear()->defoEnergy();
}
double  J2LinearDG3DIPVariable::plasticEnergy() const
{
  return getIPJ2linear()->plasticEnergy();
}

VUMATinterfaceDG3DIPVariable::VUMATinterfaceDG3DIPVariable(int _nsdv, double _size, const bool oninter) : dG3DIPVariable(oninter)
{
  _vumatipv = new IPVUMATinterface(_nsdv,_size);
}
VUMATinterfaceDG3DIPVariable::VUMATinterfaceDG3DIPVariable(const VUMATinterfaceDG3DIPVariable &source) : dG3DIPVariable(source)
{
  _vumatipv = source._vumatipv;
}
VUMATinterfaceDG3DIPVariable& VUMATinterfaceDG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const VUMATinterfaceDG3DIPVariable* src = dynamic_cast<const VUMATinterfaceDG3DIPVariable*>(&source);
  if(src != NULL)
    _vumatipv->operator= (*(src->getIPVUMATinterface()));
  return *this;
}
double VUMATinterfaceDG3DIPVariable::get(const int comp) const
{
  if(comp >= 100) // Internal variables
  {
    return getIPVUMATinterface()->get(comp);
  }
  else
  {
    return dG3DIPVariable::get(comp);
  }
}

double  VUMATinterfaceDG3DIPVariable::defoEnergy() const
{
  return getIPVUMATinterface()->defoEnergy();
}
double  VUMATinterfaceDG3DIPVariable::plasticEnergy() const
{
  return getIPVUMATinterface()->plasticEnergy();
}

nonLocalDamageDG3DIPVariableBase::nonLocalDamageDG3DIPVariableBase(const bool oninter) : dG3DIPVariable(oninter),
                                                                                              nonLocalJump(0.)
{
}

nonLocalDamageDG3DIPVariableBase::nonLocalDamageDG3DIPVariableBase(const nonLocalDamageDG3DIPVariableBase &source) :
                                                                        dG3DIPVariable(source)
{

  dLocalPlasticStrainDStrain                = source.dLocalPlasticStrainDStrain;
  dStressDNonLocalPlasticStrain             = source.dStressDNonLocalPlasticStrain;
  dLocalPlasticStrainDNonLocalPlasticStrain = source.dLocalPlasticStrainDNonLocalPlasticStrain;
  nonLocalJump                              = source.nonLocalJump;
  gradNonLocalPlasticStrain                 = source.gradNonLocalPlasticStrain;

}

nonLocalDamageDG3DIPVariableBase& nonLocalDamageDG3DIPVariableBase::operator=(const IPVariable &source)
{
  IPVariableMechanics::operator=(source);
  const nonLocalDamageDG3DIPVariableBase* src = dynamic_cast<const nonLocalDamageDG3DIPVariableBase*>(&source);
  if(src != NULL)
  {
    dLocalPlasticStrainDStrain                = src->dLocalPlasticStrainDStrain;
    dStressDNonLocalPlasticStrain             = src->dStressDNonLocalPlasticStrain;
    dLocalPlasticStrainDNonLocalPlasticStrain = src->dLocalPlasticStrainDNonLocalPlasticStrain;
    nonLocalJump                              = src->nonLocalJump;
    gradNonLocalPlasticStrain                 = src->gradNonLocalPlasticStrain;
  }
  return *this;
}

//
nonLocalDamageDG3DIPVariable::nonLocalDamageDG3DIPVariable(int _nsdv, const bool oninter) : nonLocalDamageDG3DIPVariableBase(oninter)
{
  _nldipv = new IPNonLocalDamage(_nsdv);
}

nonLocalDamageDG3DIPVariable::nonLocalDamageDG3DIPVariable(const nonLocalDamageDG3DIPVariable &source) :
                                                                   nonLocalDamageDG3DIPVariableBase(source)
{
  _nldipv = new IPNonLocalDamage((int)source.getIPNonLocalDamage()->getNsdv());
  _nldipv->operator= (*(source.getIPNonLocalDamage()));
}

nonLocalDamageDG3DIPVariable& nonLocalDamageDG3DIPVariable::operator=(const IPVariable &source)
{
  nonLocalDamageDG3DIPVariableBase::operator=(source);
  const nonLocalDamageDG3DIPVariable* src = dynamic_cast<const nonLocalDamageDG3DIPVariable*>(&source);
  if(src != NULL)
  {
    _nldipv->operator= (*(src->getIPNonLocalDamage()));
  }
  return *this;
}
double nonLocalDamageDG3DIPVariable::get(const int i) const
{
  return getIPNonLocalDamage()->get(i);
}
double nonLocalDamageDG3DIPVariable::defoEnergy() const
{
  return getIPNonLocalDamage()->defoEnergy();
}
double nonLocalDamageDG3DIPVariable::plasticEnergy() const
{
  return getIPNonLocalDamage()->plasticEnergy();
}
double nonLocalDamageDG3DIPVariable::getEffectivePlasticStrain() const
{
  return getIPNonLocalDamage()->_nldEffectivePlasticStrain;
}
double nonLocalDamageDG3DIPVariable::getCurrentPlasticStrain() const
{
  return getIPNonLocalDamage()->_nldCurrentPlasticStrain;
}
double &nonLocalDamageDG3DIPVariable::getRefToEffectivePlasticStrain()
{
  return getIPNonLocalDamage()->_nldEffectivePlasticStrain;
}
double &nonLocalDamageDG3DIPVariable::getRefToCurrentPlasticStrain()
{
  return getIPNonLocalDamage()->_nldCurrentPlasticStrain;
}
nonLocalDamageJ2HyperDG3DIPVariable::nonLocalDamageJ2HyperDG3DIPVariable(const mlawNonLocalDamageJ2Hyper &_j2law, const bool oninter) : 
                                   nonLocalDamageDG3DIPVariableBase(oninter)
{
  _nldJ2Hyperipv = new IPNonLocalDamageJ2Hyper(_j2law.getJ2IsotropicHardening(),_j2law.getCLengthLaw(),_j2law.getDamageLaw());
}

nonLocalDamageJ2HyperDG3DIPVariable::nonLocalDamageJ2HyperDG3DIPVariable(const nonLocalDamageJ2HyperDG3DIPVariable &source) : nonLocalDamageDG3DIPVariableBase(source)
{
  _nldJ2Hyperipv = new IPNonLocalDamageJ2Hyper(*source.getIPNonLocalDamageJ2Hyper());
}

nonLocalDamageJ2HyperDG3DIPVariable& nonLocalDamageJ2HyperDG3DIPVariable::operator=(const IPVariable &source)
{
  nonLocalDamageDG3DIPVariableBase::operator=(source);
  const nonLocalDamageJ2HyperDG3DIPVariable* src = dynamic_cast<const nonLocalDamageJ2HyperDG3DIPVariable*>(&source);
  if(src != NULL)
  {
    _nldJ2Hyperipv->operator= (*(src->getIPNonLocalDamageJ2Hyper()));
  }
  return *this;
}
double nonLocalDamageJ2HyperDG3DIPVariable::get(const int i) const
{
  if(i == IPField::PLASTICSTRAIN)
  {
    return getIPNonLocalDamageJ2Hyper()->getCurrentPlasticStrain();
  }
  else if(i == IPField::DAMAGE)
  {
    return getIPNonLocalDamageJ2Hyper()->getDamage();
  }
  else
  {
    return dG3DIPVariable::get(i);
  }
}
double nonLocalDamageJ2HyperDG3DIPVariable::defoEnergy() const
{
  return getIPNonLocalDamageJ2Hyper()->defoEnergy();
}
double nonLocalDamageJ2HyperDG3DIPVariable::plasticEnergy() const
{
  return getIPNonLocalDamageJ2Hyper()->plasticEnergy();
}
double nonLocalDamageJ2HyperDG3DIPVariable::getEffectivePlasticStrain() const
{
  return getIPNonLocalDamageJ2Hyper()->getEffectivePlasticStrain();
}
double nonLocalDamageJ2HyperDG3DIPVariable::getCurrentPlasticStrain() const
{
  return getIPNonLocalDamageJ2Hyper()->getCurrentPlasticStrain();
}
double &nonLocalDamageJ2HyperDG3DIPVariable::getRefToEffectivePlasticStrain()
{
  return getIPNonLocalDamageJ2Hyper()->getRefToEffectivePlasticStrain();
}
double &nonLocalDamageJ2HyperDG3DIPVariable::getRefToCurrentPlasticStrain()
{
  return getIPNonLocalDamageJ2Hyper()->getRefToCurrentPlasticStrain();
}
//
nonLocalDamageGursonDG3DIPVariable::nonLocalDamageGursonDG3DIPVariable(const mlawNonLocalDamageGurson &_law, const bool oninter) : 
                                   nonLocalDamageDG3DIPVariableBase(oninter)
{
  _nldGursonipv = new IPNonLocalDamageGurson(_law.getInitialPorosity(),_law.getJ2IsotropicHardening(),_law.getCLengthLaw(),_law.getGursonDamageNucleation());
}

nonLocalDamageGursonDG3DIPVariable::nonLocalDamageGursonDG3DIPVariable(const nonLocalDamageGursonDG3DIPVariable &source) : nonLocalDamageDG3DIPVariableBase(source)
{
  _nldGursonipv = new IPNonLocalDamageGurson(*source.getIPNonLocalDamageGurson());
}

nonLocalDamageGursonDG3DIPVariable& nonLocalDamageGursonDG3DIPVariable::operator=(const IPVariable &source)
{
  nonLocalDamageDG3DIPVariableBase::operator=(source);
  const nonLocalDamageGursonDG3DIPVariable* src = dynamic_cast<const nonLocalDamageGursonDG3DIPVariable*>(&source);
  if(src != NULL)
  {
    _nldGursonipv->operator= (*(src->getIPNonLocalDamageGurson()));
  }
  return *this;
}
double nonLocalDamageGursonDG3DIPVariable::get(const int i) const
{
  if(i == IPField::PLASTICSTRAIN)
  {
    return getIPNonLocalDamageGurson()->getMatrixPlasticStrain();
  }
  else if(i == IPField::DAMAGE)
  {
    return getIPNonLocalDamageGurson()->getNonLocalCorrectedPorosity();
  }
  else
  {
    return dG3DIPVariable::get(i);
  }
}
double nonLocalDamageGursonDG3DIPVariable::defoEnergy() const
{
  return getIPNonLocalDamageGurson()->defoEnergy();
}
double nonLocalDamageGursonDG3DIPVariable::plasticEnergy() const
{
  return getIPNonLocalDamageGurson()->plasticEnergy();
}
double nonLocalDamageGursonDG3DIPVariable::getEffectivePlasticStrain() const
{
  // for the non local gurson approach this is tilde fVstar
  return getIPNonLocalDamageGurson()->getNonLocalCorrectedPorosity();
}
double nonLocalDamageGursonDG3DIPVariable::getCurrentPlasticStrain() const
{
  // for the non local gurson approach this is fVstar
  return getIPNonLocalDamageGurson()->getLocalCorrectedPorosity();
}
double &nonLocalDamageGursonDG3DIPVariable::getRefToEffectivePlasticStrain()
{
  return getIPNonLocalDamageGurson()->getRefToNonLocalCorrectedPorosity();
}
double &nonLocalDamageGursonDG3DIPVariable::getRefToCurrentPlasticStrain()
{
  return getIPNonLocalDamageGurson()->getRefToLocalCorrectedPorosity();
}

//
TransverseIsotropicDG3DIPVariable::TransverseIsotropicDG3DIPVariable(const bool oninter) : dG3DIPVariable(oninter)
{

}
TransverseIsotropicDG3DIPVariable::TransverseIsotropicDG3DIPVariable(const TransverseIsotropicDG3DIPVariable &source) : dG3DIPVariable(source)
{
   _tiipv = source._tiipv;
}
TransverseIsotropicDG3DIPVariable& TransverseIsotropicDG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const TransverseIsotropicDG3DIPVariable* src = dynamic_cast<const TransverseIsotropicDG3DIPVariable*>(&source);
  if(src != NULL)
    _tiipv = src->_tiipv;
  return *this;
}
double TransverseIsotropicDG3DIPVariable::get(const int comp) const
{
  if(comp == IPField::PLASTICSTRAIN)
  {
    return 0.;
  }
  else
  {
    return dG3DIPVariable::get(comp);
  }
}
double  TransverseIsotropicDG3DIPVariable::defoEnergy() const
{
  return getIPTransverseIsotropic()->defoEnergy();
}
double  TransverseIsotropicDG3DIPVariable::plasticEnergy() const
{
  return getIPTransverseIsotropic()->plasticEnergy();
}


AnisotropicDG3DIPVariable::AnisotropicDG3DIPVariable(const bool oninter) : dG3DIPVariable(oninter)
{

}
AnisotropicDG3DIPVariable::AnisotropicDG3DIPVariable(const AnisotropicDG3DIPVariable &source) : dG3DIPVariable(source)
{
   _tiipv = source._tiipv;
}
AnisotropicDG3DIPVariable& AnisotropicDG3DIPVariable::operator=(const IPVariable &source)
{
  dG3DIPVariable::operator=(source);
  const AnisotropicDG3DIPVariable* src = dynamic_cast<const AnisotropicDG3DIPVariable*>(&source);
  if(src != NULL)
    _tiipv = src->_tiipv;
  return *this;
}
double AnisotropicDG3DIPVariable::get(const int comp) const
{
  if(comp == IPField::PLASTICSTRAIN)
  {
    return 0.;
  }
  else
  {
    return dG3DIPVariable::get(comp);
  }
}
double  AnisotropicDG3DIPVariable::defoEnergy() const
{
  return getIPAnisotropic()->defoEnergy();
}
double  AnisotropicDG3DIPVariable::plasticEnergy() const
{
  return getIPAnisotropic()->plasticEnergy();
}

LinearCohesive3DIPVariable::LinearCohesive3DIPVariable() : IPVariableMechanics(), deltan(0.), delta(0.),
                                                 deltat(0.), deltamax(0.),
                                                 ujump0(0.),
                                                 deltac(0.), sigmac(0.), snor0(0.), tau0(0),
                                                 beta(1.), tension(true),
						                         DDeltaDjump(0.), DDeltanDjump(0.), DDeltatDjump(0.), _fractureEnergy(0.), _tfactor(0.)
{
}

LinearCohesive3DIPVariable::LinearCohesive3DIPVariable(const LinearCohesive3DIPVariable &source): IPVariableMechanics(source)
{

  ujump0= source.ujump0;
  deltan = source.deltan;
  deltat = source.deltat;
  deltac = source.deltac;
  deltamax = source.deltamax;
  delta = source.delta;
  sigmac = source.sigmac;
  snor0   = source.snor0;
  tau0    = source.tau0;
  beta = source.beta;
  tension = source.tension;
  DDeltaDjump = source.DDeltaDjump;
  DDeltanDjump = source.DDeltanDjump;
  DDeltatDjump = source.DDeltatDjump;
  _fractureEnergy = source._fractureEnergy;
  _tfactor = source._tfactor;

}
LinearCohesive3DIPVariable& LinearCohesive3DIPVariable::operator = (const IPVariable &source)
{
  IPVariableMechanics::operator=(source);
  const LinearCohesive3DIPVariable *src = static_cast<const LinearCohesive3DIPVariable*>(&source);
  if(src != NULL)
  {
    ujump0= src->ujump0;
    deltan = src->deltan;
    deltat = src->deltat;
    deltac = src->deltac;
    deltamax = src->deltamax;
    delta = src->delta;
    sigmac = src->sigmac;
    snor0  = src->snor0;
    tau0   = src->tau0;
    beta = src->beta;
    tension = src->tension;
    DDeltaDjump = src->DDeltaDjump;
    DDeltanDjump = src->DDeltanDjump;
    DDeltatDjump = src->DDeltatDjump;
    _fractureEnergy = src->_fractureEnergy;
    _tfactor = src->_tfactor;
  }
  return *this;
}

void LinearCohesive3DIPVariable::initfrac(const SVector3 &du, const SVector3 &n)
{
  SVector3 normalizedN = n;
  normalizedN.normalize();
  // compute initial value
  ujump0  = du;
}

void LinearCohesive3DIPVariable::initmean(LinearCohesive3DIPVariable *ipv2)
{
  if(tension!= ipv2->ifTension())
    Msg::Error("Initial fracture with one element in tension and the other in compression!");
  else if(tension)
    Msg::Error("Fracture in tension");
  else
    Msg::Error("Fracture in compression");
  sigmac += ipv2->getSigmac();
  sigmac*=0.5;
  deltac += ipv2->getDeltac();
  deltac*=0.5;
  ipv2->sigmac = sigmac;
  ipv2->deltac = deltac;
}

void LinearCohesive3DIPVariable::setFracture(const LinearCohesive3DIPVariable*ipvprev,const SVector3 &ujump_, const SVector3 &n)
{
  ujump0 = ipvprev->ujump0;
  sigmac = ipvprev->sigmac;
  snor0  = ipvprev->snor0;
  tau0   = ipvprev->tau0;
  deltac = ipvprev->deltac;
  beta = ipvprev->beta;
  tension = ipvprev->tension;
  SVector3 normalizedN = n;
  normalizedN.normalize();
  // compute initial value
  deltan = dot(ujump_-ujump0,normalizedN);
  if(deltan<0.) deltan = 0.;
  deltat = (ujump_-ujump0) - (deltan*normalizedN);
  //
  DDeltanDjump = normalizedN;
  DDeltatDjump = 0.;

  for(int i =0; i<3; i++)
  {
    for(int j=0; j<3; j++)
    {
      if(i==j) DDeltatDjump(i,j) = 1.-normalizedN(i)*normalizedN(j);
      else DDeltatDjump(i,j) = -normalizedN(i)*normalizedN(j);
    }
  }
  DDeltaDjump = 0.;

  if(ifTension())
  {
    delta = sqrt(deltan*deltan+beta*beta*dot(deltat,deltat));
    if(delta>1.e-15)
    {
      DDeltaDjump = (deltan/delta)*DDeltanDjump;
      for (int i = 0; i< 3; i++)
      {
        for (int j =0; j <3; j++)
        {
          DDeltaDjump(i) += (beta*beta/delta)*deltat(j)*DDeltatDjump(j,i);
	}
      }
    }
  }
  else
  {
    delta = beta*deltat.norm();
    if(delta>1.e-15)
    {
      for (int i = 0; i< 3; i++)
      {
	for (int j =0; j <3; j++)
	{
	  DDeltaDjump(i) += (beta*beta/delta)*deltat(j)*DDeltatDjump(j,i);
	}
      }
    }
  }
  // Update deltamax
  deltamax = ipvprev->deltamax;
  if(fabs(delta)>fabs(deltamax)) deltamax = delta;
}


NonLocalDamageLinearCohesive3DIPVariable::NonLocalDamageLinearCohesive3DIPVariable() : LinearCohesive3DIPVariable()
{
}

NonLocalDamageLinearCohesive3DIPVariable::NonLocalDamageLinearCohesive3DIPVariable(const NonLocalDamageLinearCohesive3DIPVariable &source): LinearCohesive3DIPVariable(source)
{


}
NonLocalDamageLinearCohesive3DIPVariable& NonLocalDamageLinearCohesive3DIPVariable::operator = (const IPVariable &source)
{
  LinearCohesive3DIPVariable::operator=(source);
  const NonLocalDamageLinearCohesive3DIPVariable *src = static_cast<const NonLocalDamageLinearCohesive3DIPVariable*>(&source);
  if(src != NULL)
  {
  }
  return *this;
}

void NonLocalDamageLinearCohesive3DIPVariable::initfrac(const SVector3 &du, const SVector3 &n)
{
  LinearCohesive3DIPVariable::initfrac(du,n);
}

void NonLocalDamageLinearCohesive3DIPVariable::initmean(NonLocalDamageLinearCohesive3DIPVariable *ipv2)
{
  if(tension!= ipv2->ifTension())
    Msg::Error("Initial fracture with one element in tension and the other in compression!");
  else if(tension)
    Msg::Error("Fracture in tension");
  else
    Msg::Error("Fracture in compression");
  sigmac += ipv2->getSigmac();
  sigmac*=0.5;
  deltac += ipv2->getDeltac();
  deltac*=0.5;
  snor0 +=ipv2->snor0;
  snor0 *=0.5;
  tau0  +=ipv2->tau0;
  tau0  *=0.5;
  ipv2->sigmac = sigmac;
  ipv2->deltac = deltac;
  ipv2->snor0  = snor0;
  ipv2->tau0   = tau0;
}

void NonLocalDamageLinearCohesive3DIPVariable::setFracture(const NonLocalDamageLinearCohesive3DIPVariable*ipvprev,const SVector3 &ujump_, const SVector3 &n)
{
  LinearCohesive3DIPVariable::setFracture(ipvprev,ujump_,n);
}

void NonLocalDamageLinearCohesive3DIPVariable::setDataFromLaw(const double smax, const double snor, const double tau, const double Gc, const double beta_, const bool ift)
{
  LinearCohesive3DIPVariable::setDataFromLaw(smax, snor, tau, Gc, beta_, ift);
}



FractureCohesive3DIPVariable::FractureCohesive3DIPVariable(const double facSigmac) :
                                                                           IPVariable2ForFracture<dG3DIPVariableBase,LinearCohesive3DIPVariable>(),
                                                                           _facSigmac(facSigmac){}
FractureCohesive3DIPVariable::FractureCohesive3DIPVariable(const FractureCohesive3DIPVariable &source) :
                                                                  IPVariable2ForFracture<dG3DIPVariableBase,LinearCohesive3DIPVariable>(source),
                                                                  _facSigmac(source._facSigmac){}
FractureCohesive3DIPVariable& FractureCohesive3DIPVariable::operator=(const IPVariable &source)
{
  IPVariable2ForFracture<dG3DIPVariableBase,LinearCohesive3DIPVariable>::operator=(source);
  const FractureCohesive3DIPVariable *src = dynamic_cast<const FractureCohesive3DIPVariable*>(&source);
  if(src != NULL)
    _facSigmac = src->_facSigmac;
  return *this;
}




ThermoMechanicsDG3DIPVariableBase::ThermoMechanicsDG3DIPVariableBase( const bool oninter) : dG3DIPVariable(oninter),temperature(0.), gradT(0.), thermalFlux(0.),
                                                                   dPdT(0.), dthermalFluxdgradT(0.), temperatureJump(0.),interfaceFlux(0.)//,linearK(0.)
											
{
  
}

ThermoMechanicsDG3DIPVariableBase::ThermoMechanicsDG3DIPVariableBase(const ThermoMechanicsDG3DIPVariableBase &source) :
                                                                        dG3DIPVariable(source)
{
  linearK   = source.linearK;
  temperature = source.temperature;
  gradT = source.gradT;
  thermalFlux = source.thermalFlux; 
  dPdT = source. dPdT;
  dthermalFluxdgradT = source.dthermalFluxdgradT ;
  temperatureJump= source. temperatureJump;
  interfaceFlux= source.interfaceFlux;
 
}

ThermoMechanicsDG3DIPVariableBase& ThermoMechanicsDG3DIPVariableBase::operator=(const IPVariable &source)
{
  IPVariableMechanics::operator=(source);
  const ThermoMechanicsDG3DIPVariableBase* src = dynamic_cast<const ThermoMechanicsDG3DIPVariableBase*>(&source);
  if(src != NULL)
  {
    linearK   = src->linearK;
    temperature=src->temperature;
    gradT=src->gradT;
    thermalFlux=src->thermalFlux;
    dPdT=src-> dPdT;
    dthermalFluxdgradT=src->dthermalFluxdgradT;
    temperatureJump=src->temperatureJump;
    interfaceFlux=src-> interfaceFlux;
  }
  return *this;
}

LinearThermoMechanicsDG3DIPVariable::LinearThermoMechanicsDG3DIPVariable( const bool oninter) : 
                                   ThermoMechanicsDG3DIPVariableBase(oninter)
{
  
  
}


LinearThermoMechanicsDG3DIPVariable::LinearThermoMechanicsDG3DIPVariable(const LinearThermoMechanicsDG3DIPVariable &source) : 
                                                        ThermoMechanicsDG3DIPVariableBase(source)
{
    _linearTMIP = source._linearTMIP;

}

LinearThermoMechanicsDG3DIPVariable& LinearThermoMechanicsDG3DIPVariable::operator=(const IPVariable &source)
{
  ThermoMechanicsDG3DIPVariableBase::operator=(source);
  const LinearThermoMechanicsDG3DIPVariable* src = dynamic_cast<const LinearThermoMechanicsDG3DIPVariable*>(&source);
  if(src != NULL)
  {
    _linearTMIP = src->_linearTMIP;
  }
  return *this;
}
double LinearThermoMechanicsDG3DIPVariable::get(const int i) const
{
  if(i == IPField::TEMPERATURE)
  {
    return temperature;
  }
  else if(i == IPField::THERMALFLUX_X)
  { 
    return thermalFlux(0); 
  }
  else if(i == IPField::THERMALFLUX_Y)
  { 
    return thermalFlux(1); 
  }
  else if(i == IPField::THERMALFLUX_Z)
  { 
    return thermalFlux(2); 
  }
  else 
  {
    return dG3DIPVariable::get(i);
  }
}
double LinearThermoMechanicsDG3DIPVariable::defoEnergy() const
{
  return getIPLinearThermoMechanics()->defoEnergy();
}
double LinearThermoMechanicsDG3DIPVariable::plasticEnergy() const
{
  return getIPLinearThermoMechanics()->plasticEnergy();
}
