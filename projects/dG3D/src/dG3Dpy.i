%feature("autodoc","1");
#ifdef _DEBUG
  %module dG3DpyDebug
#else
  %module dG3Dpy
#endif
%include std_string.i
%include std_vector.i
%include std_map.i

%include nlmechsolpy.i

%{
  #include "dG3DMaterialLaw.h"
  #include "dG3DDomain.h"
%}
%nodefaultctor dG3DMaterialLaw;
%nodefaultctor Cohesive3DLaw;
%nodefaultctor fractureBy2Laws<dG3DMaterialLaw,Cohesive3DLaw>;

%template(fractureBy2LawsDG3DMaterialLawCohesive3DLaw) fractureBy2Laws<dG3DMaterialLaw,Cohesive3DLaw>;
%include "dG3DMaterialLaw.h"
%include "dG3DDomain.h"

