//
// C++ Interface: partDomain
//
// Description: Interface class for dg 3D
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef DG3DDOMAIN_H_
#define DG3DDOMAIN_H_
#ifndef SWIG
#include "dG3DFunctionSpace.h"
#include "dG3DMaterialLaw.h"
#include "dG3DMaterialLaw.h"
#include "dG3DIPVariable.h"
#include "partDomain.h"
#include "interfaceQuadrature.h"
#endif
class dG3DDomain : public dgPartDomain{
 public :
  enum GaussType { Gauss=0, Lobatto=1};
 protected:
  materialLaw *_mlaw;
  const int _lnum;
  int _dim; //dimension
  double _beta1;
  double _sts; // variable to scale the explicit time step by 1/sqrt(beta) for dg stabilization
  bool _evalStiff; // = true when compute matrix by perturbation
  bool _isHighOrder; // = true if highorder used
  FunctionSpaceBase *_space;
  FunctionSpaceBase *_spaceMinus;
  FunctionSpaceBase *_spacePlus;
  FunctionSpaceBase *_spaceInterUVW; // needed to compute the normal at interface
  int _gaussorderbulk, _gaussorderbound; // To allow user to change the number of Gauss Point default =-1 (not used) if != -1 user's value
  interfaceQuadratureBase *_interQuad;
  GaussType _gqt; // classic or Lobatto
  #if defined(HAVE_MPI)
  bool _distributedOnRoot;
  mutable std::set<int> _domainIPBulk,  // bulk integration points lie on this domain
                          _domainIPInterfacePlus, // plus intergration points lie on this domain
                          _domainIPInterfaceMinus; //  minus integration points lie on this domain
  mutable std::map<int,std::set<int> > _mapIPBulk, // map IP for bulk elements
                          _mapIPInterfacePlus, // map IP for positive part of interface elements
                          _mapIPInterfaceMinus; // map IP for negative part of interface elements
  #endif //HAVE_MPI

   virtual void initialIPVariable(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws);
 public:
  virtual void stabilityParameters(const double b1=10.);
  virtual void matrixByPerturbation(const int ibulk, const int iinter, const int ivirt, const double eps=1e-8);
  dG3DDomain(const int tag, const int phys, const int sp_, const int lnum, const int fdg, const int dim =3);
  dG3DDomain(const dG3DDomain &source);
  ~dG3DDomain(){delete g;} //If I delete mat here plante why ?? delete g ??
  virtual void gaussIntegration(const GaussType gqt,const int orderbulk, const int orderbound);
  virtual void distributeOnRootRank(const int flg);
#ifndef SWIG
  virtual int getDim() const {return _dim;}
  virtual void computeStrain(AllIPState *aips,MElement *e, IPStateBase::whichState ws,
                                        fullVector<double> &disp);
  virtual void computeIpv(AllIPState *aips,MElement *e, IPStateBase::whichState ws,
                                  materialLaw *mlaw,fullVector<double> &disp, bool stiff);
  virtual void computeStrain(AllIPState *aips,MInterfaceElement *ie, IntPt *GP,const IPStateBase::whichState ws,
                                        partDomain* efMinus, partDomain *efPlus, fullVector<double> &dispm,
                                        fullVector<double> &dispp,const bool virt);
  virtual void computeIpv(AllIPState *aips,MInterfaceElement *ie, IntPt *GP,const IPStateBase::whichState ws,
                                  partDomain* efMinus, partDomain *efPlus,materialLaw *mlawminus,
                                  materialLaw *mlawplus,fullVector<double> &dispm,
                                  fullVector<double> &dispp,const bool virt, bool stiff,const bool checkfrac=true);
  virtual void computeAllIPStrain(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, const bool virt);
  virtual void computeIPVariable(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, bool stiff);
  virtual void setMaterialLaw(const std::map<int,materialLaw*> maplaw);
  virtual materialLaw* getMaterialLaw(){return _mlaw;}
  virtual const materialLaw* getMaterialLaw() const{return _mlaw;}
  virtual materialLaw* getMaterialLawMinus(){return _mlaw;}
  virtual const materialLaw* getMaterialLawMinus() const{return _mlaw;}
  virtual materialLaw* getMaterialLawPlus(){return _mlaw;}
  virtual const materialLaw* getMaterialLawPlus() const{return _mlaw;}
  virtual int getLawNum() const{return _lnum;}
  virtual int getMinusLawNum() const{return _lnum;}
  virtual int getPlusLawNum() const{return _lnum;}
  virtual void setGaussIntegrationRule();
  virtual void createTerms(unknownField *uf,IPField*ip, std::vector<nonLinearNeumannBC> &vNeumann);
  virtual void initializeTerms(unknownField *uf,IPField *ip, std::vector<nonLinearNeumannBC> &vNeumann);
  virtual double scaleTimeStep() const {return _sts;}
  virtual FunctionSpaceBase* getFunctionSpace() const{return _space;}
  virtual FunctionSpaceBase* getFunctionSpaceMinus() const{return _spaceMinus;}
  virtual FunctionSpaceBase* getFunctionSpacePlus() const{return _spacePlus;}
  virtual const partDomain* getMinusDomain() const{return this;}
  virtual const partDomain* getPlusDomain() const{return this;}
  virtual partDomain* getMinusDomain() {return this;}
  virtual partDomain* getPlusDomain() {return this;}
  virtual void createInterface(manageInterface &maninter);
  virtual MElement* createVirtualInterface(IElement *ie) const;
  virtual MElement* createInterface(IElement *ie1, IElement *ie2) const;
  virtual const double stabilityParameter(const int i) const;
  virtual void allocateInterfaceMPI(const int rankOtherPart, const groupOfElements &groupOtherPart,dgPartDomain** dgdom);
  virtual void createInterfaceMPI(const int rankOtherPart, const groupOfElements &groupOtherPart,dgPartDomain* &dgdom);
  // Add for BC
  virtual FilterDof* createFilterDof(const int comp) const;
  virtual FunctionSpaceBase* getSpaceForBC(const nonLinearBoundaryCondition &boundary, const groupOfElements *groupBC,const int domcomp) const;
  virtual FunctionSpaceBase* getSpaceForBC(const nonLinearBoundaryCondition &boundary, const groupOfElements *groupBC) const
  {
      return this->getSpaceForBC(boundary,groupBC,3);
  }
  virtual QuadratureBase* getQuadratureRulesForNeumannBC(nonLinearNeumannBC &neu) const;
  virtual void createIPState(AllIPState::ipstateContainer& map, const bool *state) const;
  virtual partDomain* clone() const;
  #if defined(HAVE_MPI)
  virtual void computeIPVariableMPI(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, bool stiff);
  #endif
#endif
};

class nonLocalDamageDG3DDomain : public dG3DDomain{
 protected:
  double _nonLocalBeta;
  bool _continuity;
  double _nonLocalEqRatio;

 public:
  nonLocalDamageDG3DDomain(const int tag, const int phys, const int sp_, const int lnum, const int fdg, const double nonLocalEqRatio, const int dim =3);
  nonLocalDamageDG3DDomain(const nonLocalDamageDG3DDomain &source);
  ~nonLocalDamageDG3DDomain(){}
  virtual void nonLocalStabilityParameters(const double b2=10., bool continuity=true)
  {
     _nonLocalBeta = b2;
     _continuity = continuity;
  }
  virtual double getNonLocalStabilityParameter() { return _nonLocalBeta;}
  virtual bool   getNonLocalContinuity() { return _continuity;}
  virtual double getNonLocalEqRatio() const { return _nonLocalEqRatio;}
  virtual bool considerForTimeStep(AllIPState *aips,MElement *e);


#ifndef SWIG
  virtual FunctionSpaceBase* getSpaceForBC(const nonLinearBoundaryCondition &boundary, const groupOfElements *groupBC) const
  {
    return dG3DDomain::getSpaceForBC(boundary,groupBC,4);
  }
  virtual void createTerms(unknownField *uf,IPField*ip, std::vector<nonLinearNeumannBC> &vNeumann);
  virtual void computeStrain(AllIPState *aips,MElement *e, IPStateBase::whichState ws,
                                        fullVector<double> &disp);

  virtual void computeStrain(AllIPState *aips,MInterfaceElement *ie, IntPt *GP,const IPStateBase::whichState ws,
                                        partDomain* efMinus, partDomain *efPlus, fullVector<double> &dispm,
                                        fullVector<double> &dispp,const bool virt);

  virtual void allocateInterfaceMPI(const int rankOtherPart, const groupOfElements &groupOtherPart,dgPartDomain** dgdom);

#endif
};

class interDomainBase {
 protected :
  BiNonLinearTermBase *bterm;
  nonLinearTermBase<double> *lterm;
  partDomain *_domMinus;
  partDomain *_domPlus;
  materialLaw* _mlawMinus;
  materialLaw* _mlawPlus;
 public :
  interDomainBase(partDomain *dom1,partDomain *dom2);
  interDomainBase(const interDomainBase &source);
  interDomainBase(){}
#ifndef SWIG
  void initializeFractureBase(unknownField *uf,IPField*ip, std::vector<nonLinearNeumannBC> &vNeumann,
                                         groupOfElements *gi, QuadratureBase* integBound);
  void setGaussIntegrationRuleBase(groupOfElements *gi, QuadratureBase** integBound, QuadratureBase** integBulk,
                                   interfaceQuadratureBase** interQuad,int _gaussorderbound, dG3DDomain::GaussType _gqt);
#endif

};

class interDomainBetween3D : public interDomainBase, public dG3DDomain{
 public:
  interDomainBetween3D(const int tag, partDomain *dom1, partDomain *dom2, const int lnum=0);
  interDomainBetween3D(const interDomainBetween3D &source);
  ~interDomainBetween3D(){}
  virtual void stabilityParameters(const double b1=10.);
  virtual void matrixByPerturbation(const int iinter, const double eps=1e-8);
#ifndef SWIG
  //void initializeTerms(FunctionSpaceBase* space1_,FunctionSpaceBase* space2_,unknownField *uf,IPField*ip,
  //                     std::vector<nonLinearNeumannBC> &vNeumann);
  virtual void setGaussIntegrationRule();
  virtual void computeAllIPStrain(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, const bool virt);
  virtual void computeIPVariable(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws,
                                  bool stiff=true);

  virtual void setMaterialLaw(const std::map<int,materialLaw*> maplaw);
  // FOLLOWING FUNCTION SHOULD STAY HERE BECAUSE OF THE DOUBLE DERIVATION
  virtual materialLaw* getMaterialLaw(){return _mlaw;}
  virtual const materialLaw* getMaterialLaw() const{return _mlaw;}
  virtual materialLaw* getMaterialLawMinus(){return _mlawMinus;}
  virtual const materialLaw* getMaterialLawMinus() const{return _mlawMinus;}
  virtual materialLaw* getMaterialLawPlus(){return _mlawPlus;}
  virtual const materialLaw* getMaterialLawPlus() const{return _mlawPlus;}
  virtual int getLawNum() const{return _lnum;}
  // FOLLOWING FUNCTION SHOULD STAY HERE BECAUSE OF THE DOUBLE DERIVATION
  virtual int getMinusLawNum() const{return _domMinus->getLawNum();}
  virtual int getPlusLawNum() const{return _domPlus->getLawNum();}
  virtual const partDomain* getMinusDomain() const{return _domMinus;}
  virtual const partDomain* getPlusDomain() const{return _domPlus;}
  virtual partDomain* getMinusDomain(){return _domMinus;}
  virtual partDomain* getPlusDomain() {return _domPlus;}
  void initializeTerms(unknownField *uf,IPField *ip, std::vector<nonLinearNeumannBC> &vNeumann);

  virtual void createTerms(unknownField *uf,IPField*ip, std::vector<nonLinearNeumannBC> &vNeumann);
  // as there is no elerment on a interDomain empty function. The interface element on an interDomain
  // has to be created via its two domains.
  virtual void createInterface(manageInterface &maninter){}

#endif
};

class nonLocalInterDomainBetween3D : public interDomainBase, public nonLocalDamageDG3DDomain{
 protected:
  double _nonLocalEqRatio;
 public:
  nonLocalInterDomainBetween3D(const int tag, partDomain *dom1, partDomain *dom2, const double nonLocalEqRatio, const int lnum=0);
  nonLocalInterDomainBetween3D(const nonLocalInterDomainBetween3D &source);
  ~nonLocalInterDomainBetween3D(){}
  virtual double getNonLocalEqRatio() const { return _nonLocalEqRatio;}
  virtual void stabilityParameters(const double b1=10.);
  virtual void nonLocalStabilityParameters(const double b2=10., bool continuity=true)
  {
     _nonLocalBeta = b2;
     _continuity = continuity;
  }
  virtual void matrixByPerturbation(const int iinter, const double eps=1e-8);
#ifndef SWIG
  //void initializeTerms(FunctionSpaceBase* space1_,FunctionSpaceBase* space2_,unknownField *uf,IPField*ip,
  //                     std::vector<nonLinearNeumannBC> &vNeumann);
  virtual void setGaussIntegrationRule();
  virtual void computeAllIPStrain(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, const bool virt);
  virtual void computeIPVariable(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws,
                                  bool stiff=true);

  virtual void setMaterialLaw(const std::map<int,materialLaw*> maplaw);
  // FOLLOWING FUNCTION SHOULD STAY HERE BECAUSE OF THE DOUBLE DERIVATION
  virtual materialLaw* getMaterialLaw(){return _mlaw;}
  virtual const materialLaw* getMaterialLaw() const{return _mlaw;}
  virtual materialLaw* getMaterialLawMinus(){return _mlawMinus;}
  virtual const materialLaw* getMaterialLawMinus() const{return _mlawMinus;}
  virtual materialLaw* getMaterialLawPlus(){return _mlawPlus;}
  virtual const materialLaw* getMaterialLawPlus() const{return _mlawPlus;}
  virtual int getLawNum() const{return _lnum;}
  // FOLLOWING FUNCTION SHOULD STAY HERE BECAUSE OF THE DOUBLE DERIVATION
  virtual int getMinusLawNum() const{return _domMinus->getLawNum();}
  virtual int getPlusLawNum() const{return _domPlus->getLawNum();}
  virtual const partDomain* getMinusDomain() const{return _domMinus;}
  virtual const partDomain* getPlusDomain() const{return _domPlus;}
  virtual partDomain* getMinusDomain(){return _domMinus;}
  virtual partDomain* getPlusDomain() {return _domPlus;}
  void initializeTerms(unknownField *uf,IPField *ip, std::vector<nonLinearNeumannBC> &vNeumann);

  virtual void createTerms(unknownField *uf,IPField*ip, std::vector<nonLinearNeumannBC> &vNeumann);
  // as there is no elerment on a interDomain empty function. The interface element on an interDomain
  // has to be created via its two domains.
  virtual void createInterface(manageInterface &maninter){}

#endif
};

class ThermoMechanicsDG3DDomain : public dG3DDomain{
 protected:
  double _ThermoMechanicsBeta;
  bool _continuity;
  double _ThermoMechanicsEqRatio;

 public:
  ThermoMechanicsDG3DDomain(const int tag, const int phys, const int sp_, const int lnum, const int fdg, const double ThermoMechanicsEqRatio, const int dim =3);
  ThermoMechanicsDG3DDomain(const ThermoMechanicsDG3DDomain &source);
  ~ThermoMechanicsDG3DDomain(){}
  virtual void ThermoMechanicsStabilityParameters(const double b2=10., bool continuity=true)
  {
     _ThermoMechanicsBeta = b2;
     _continuity = continuity;
  }
  virtual double getThermoMechanicsStabilityParameter() { return _ThermoMechanicsBeta;}
  virtual bool   getThermoMechanicsContinuity() { return _continuity;}
  virtual double getThermoMechanicsEqRatio() const { return _ThermoMechanicsEqRatio;}

#ifndef SWIG
  virtual FunctionSpaceBase* getSpaceForBC(const nonLinearBoundaryCondition &boundary, const groupOfElements *groupBC) const
  {
    return dG3DDomain::getSpaceForBC(boundary,groupBC,4);
  }
  virtual void createTerms(unknownField *uf,IPField*ip, std::vector<nonLinearNeumannBC> &vNeumann);
  virtual void computeStrain(AllIPState *aips,MElement *e, IPStateBase::whichState ws,
                                        fullVector<double> &disp);

  virtual void computeStrain(AllIPState *aips,MInterfaceElement *ie, IntPt *GP,const IPStateBase::whichState ws,
                                        partDomain* efMinus, partDomain *efPlus, fullVector<double> &dispm,
                                        fullVector<double> &dispp,const bool virt);

  virtual void allocateInterfaceMPI(const int rankOtherPart, const groupOfElements &groupOtherPart,dgPartDomain** dgdom);

#endif
};

class ThermoMechanicsInterDomainBetween3D : public interDomainBase, public ThermoMechanicsDG3DDomain{
 protected:
  double _ThermoMechanicsEqRatio;
 public:
  ThermoMechanicsInterDomainBetween3D(const int tag, partDomain *dom1, partDomain *dom2, const double ThermoMechanicsEqRatio, const int lnum=0);
  ThermoMechanicsInterDomainBetween3D(const ThermoMechanicsInterDomainBetween3D &source);
  ~ThermoMechanicsInterDomainBetween3D(){}
  virtual double getThermoMechanicsEqRatio() const { return _ThermoMechanicsEqRatio;}
  virtual void stabilityParameters(const double b1=10.);
  virtual void ThermoMechanicsStabilityParameters(const double b2=10., bool continuity=true)
  {
     _ThermoMechanicsBeta = b2;
     _continuity = continuity;
  }
  virtual void matrixByPerturbation(const int iinter, const double eps=1e-8);
#ifndef SWIG
  //void initializeTerms(FunctionSpaceBase* space1_,FunctionSpaceBase* space2_,unknownField *uf,IPField*ip,
  //                     std::vector<nonLinearNeumannBC> &vNeumann);
  virtual void setGaussIntegrationRule();
  virtual void computeAllIPStrain(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, const bool virt);
  virtual void computeIPVariable(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws,
                                  bool stiff=true);

  virtual void setMaterialLaw(const std::map<int,materialLaw*> maplaw);
  // FOLLOWING FUNCTION SHOULD STAY HERE BECAUSE OF THE DOUBLE DERIVATION
  virtual materialLaw* getMaterialLaw(){return _mlaw;}
  virtual const materialLaw* getMaterialLaw() const{return _mlaw;}
  virtual materialLaw* getMaterialLawMinus(){return _mlawMinus;}
  virtual const materialLaw* getMaterialLawMinus() const{return _mlawMinus;}
  virtual materialLaw* getMaterialLawPlus(){return _mlawPlus;}
  virtual const materialLaw* getMaterialLawPlus() const{return _mlawPlus;}
  virtual int getLawNum() const{return _lnum;}
  // FOLLOWING FUNCTION SHOULD STAY HERE BECAUSE OF THE DOUBLE DERIVATION
  virtual int getMinusLawNum() const{return _domMinus->getLawNum();}
  virtual int getPlusLawNum() const{return _domPlus->getLawNum();}
  virtual const partDomain* getMinusDomain() const{return _domMinus;}
  virtual const partDomain* getPlusDomain() const{return _domPlus;}
  virtual partDomain* getMinusDomain(){return _domMinus;}
  virtual partDomain* getPlusDomain() {return _domPlus;}
  void initializeTerms(unknownField *uf,IPField *ip, std::vector<nonLinearNeumannBC> &vNeumann);

  virtual void createTerms(unknownField *uf,IPField*ip, std::vector<nonLinearNeumannBC> &vNeumann);
  // as there is no elerment on a interDomain empty function. The interface element on an interDomain
  // has to be created via its two domains.
  virtual void createInterface(manageInterface &maninter){}

#endif
};

void computeJump(const std::vector<TensorialTraits<double>::ValType> &Val_m,const int n_m,
                      const std::vector<TensorialTraits<double>::ValType> &Val_p,
                      const int n_p,const fullVector<double> & dispm,const fullVector<double> & dispp,SVector3 &ujump);

void computeNonLocalJump(const std::vector<TensorialTraits<double>::ValType> &Val_m,const int n_m,
                      const std::vector<TensorialTraits<double>::ValType> &Val_p,
                      const int n_p,const fullVector<double> & dispm,const fullVector<double> & dispp, double &epsjump);
 
void computeThermoMechanicsJump(const std::vector<TensorialTraits<double>::ValType> &Val_m,const int n_m,
                      const std::vector<TensorialTraits<double>::ValType> &Val_p,
                      const int n_p,const fullVector<double> & dispm,const fullVector<double> & dispp, double &temperaturejump);

#endif // DG3DDOMAIN_H_
