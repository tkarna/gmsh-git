#-*-coding:Utf-8-*-

# script python to regroup crackpath files at the end of a MPI simulation
from os import system,getcwd,listdir

Lext = 0.0001064718085995719 
Surf = 6.147152735606913e-05*5.323590429978592e-06
print Surf

fileD2 = open("NodalDisplacement1comp0.csv",'r')
fileD3 = open("NodalDisplacement2comp0.csv",'r')
fileF3 = open("force102comp0.csv",'r')

fileStressMtxMean = open("IPVolume51val1Mean.csv",'r')
fileStressMtxMax = open("IPVolume51val1Max.csv",'r')
fileStressMtxMin = open("IPVolume51val1Min.csv",'r')
fileStressIncMean = open("IPVolume52val1Mean.csv",'r')
fileStressIncMax = open("IPVolume52val1Max.csv",'r')
fileStressIncMin = open("IPVolume52val1Min.csv",'r')
fileSvmMtxMean = open("IPVolume51val0Mean.csv",'r')
fileSvmMtxMax = open("IPVolume51val0Max.csv",'r')
fileSvmMtxMin = open("IPVolume51val0Min.csv",'r')
filePMtxMean = open("IPVolume51val8Mean.csv",'r')
filePMtxMax = open("IPVolume51val8Max.csv",'r')
filePMtxMin = open("IPVolume51val8Min.csv",'r')

system("rm -f strainstress.csv")
outfile = open("strainstress.csv",'w')

while 1:
    tempo2 = fileD2.readline()[:-1]
    if(tempo2 == ''):
        break
    tempo3 = fileD3.readline()[:-1]
    tempoF3 = fileF3.readline()[:-1]
    tempoStressMtxMean = fileStressMtxMean.readline()[:-1]
    tempoStressMtxMax = fileStressMtxMax.readline()[:-1]
    tempoStressMtxMin = fileStressMtxMin.readline()[:-1]
    tempoStressIncMean = fileStressIncMean.readline()[:-1]
    tempoStressIncMax = fileStressIncMax.readline()[:-1]
    tempoStressIncMin = fileStressIncMin.readline()[:-1]
    tempoSvmMtxMean = fileSvmMtxMean.readline()[:-1]
    tempoSvmMtxMax = fileSvmMtxMax.readline()[:-1]
    tempoSvmMtxMin = fileSvmMtxMin.readline()[:-1]
    tempoPMtxMean = filePMtxMean.readline()[:-1]
    tempoPMtxMax = filePMtxMax.readline()[:-1]
    tempoPMtxMin = filePMtxMin.readline()[:-1]
    lsttmp2 = tempo2.split(';')
    lsttmp3 = tempo3.split(';')
    lsttmpF3 = tempoF3.split(';')
    lsttmpSMtxMean = tempoStressMtxMean.split(';')
    lsttmpSMtxMax = tempoStressMtxMax.split(';')
    lsttmpSMtxMin = tempoStressMtxMin.split(';')
    lsttmpSIncMean = tempoStressIncMean.split(';')
    lsttmpSIncMax = tempoStressIncMax.split(';')
    lsttmpSIncMin = tempoStressIncMin.split(';')
    lsttmpSvmMtxMean = tempoSvmMtxMean.split(';')
    lsttmpSvmMtxMax = tempoSvmMtxMax.split(';')
    lsttmpSvmMtxMin = tempoSvmMtxMin.split(';')
    lsttmpPMtxMean = tempoPMtxMean.split(';')
    lsttmpPMtxMax = tempoPMtxMax.split(';')
    lsttmpPMtxMin = tempoPMtxMin.split(';')
    eps = (float(lsttmp3[1])-float(lsttmp2[1]))/Lext
    sig = (float(lsttmpF3[1])/Surf)
    sigMtxMean = (float(lsttmpSMtxMean[1]))
    sigMtxMax = (float(lsttmpSMtxMax[1]))
    sigMtxMin = (float(lsttmpSMtxMin[1]))
    sigIncMean = (float(lsttmpSIncMean[1]))
    sigIncMax = (float(lsttmpSIncMax[1]))
    sigIncMin = (float(lsttmpSIncMin[1]))
    svmMtxMean = (float(lsttmpSvmMtxMean[1]))
    svmMtxMax = (float(lsttmpSvmMtxMax[1]))
    svmMtxMin = (float(lsttmpSvmMtxMin[1]))
    pMtxMean = (float(lsttmpPMtxMean[1]))
    pMtxMax = (float(lsttmpPMtxMax[1]))
    pMtxMin = (float(lsttmpPMtxMin[1]))
    outfile.write("%e"%eps+";"+"%e"%sig+";"+"%e"%sigMtxMean+";"+"%e"%sigMtxMax+";"+"%e"%sigMtxMin+";"+"%e"%sigIncMean+";"+"%e"%sigIncMax+";"+"%e"%sigIncMin+";"+"%e"%pMtxMean+";"+"%e"%pMtxMax+";"+"%e"%pMtxMin+";"+"%e"%svmMtxMean+";"+"%e"%svmMtxMax+";"+"%e\n"%svmMtxMin)

fileD2.close()
fileD3.close()
fileF3.close()
outfile.close()

print "extract OK"
