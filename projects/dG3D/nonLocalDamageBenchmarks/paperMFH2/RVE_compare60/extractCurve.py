#-*-coding:Utf-8-*-

# script python to regroup crackpath files at the end of a MPI simulation
from os import system,getcwd,listdir

Lext =0.0001064718085995719
Surf = 6.147152735606913e-05*2.661795214989296e-06
print Surf

fileD2 = open("NodalDisplacement1comp0.csv",'r')
fileD3 = open("NodalDisplacement2comp0.csv",'r')
fileF3 = open("force2376comp0.csv",'r')

fileStressComp = open("IPVolume100val1Mean.csv",'r')
fileStressMtx = open("IPVolume100val93Mean.csv",'r')
fileStressInc = open("IPVolume100val100Mean.csv",'r')
fileSvmMtx = open("IPVolume100val92Mean.csv",'r')
fileEpMtx = open("IPVolume100val8Mean.csv",'r')

system("rm -f strainstress.csv")
outfile = open("strainstress.csv",'w')

while 1:
    tempo2 = fileD2.readline()[:-1]
    if(tempo2 == ''):
        break
    tempo3 = fileD3.readline()[:-1]
    tempoF3 = fileF3.readline()[:-1]
    tempoStressComp = fileStressComp.readline()[:-1]
    tempoStressMtx = fileStressMtx.readline()[:-1]
    tempoStressInc = fileStressInc.readline()[:-1]
    tempoSvmMtx = fileSvmMtx.readline()[:-1]
    tempoEpMtx = fileEpMtx.readline()[:-1]
    lsttmp2 = tempo2.split(';')
    lsttmp3 = tempo3.split(';')
    lsttmpF3 = tempoF3.split(';')
    lsttmpSComp = tempoStressComp.split(';')
    lsttmpSMtx = tempoStressMtx.split(';')
    lsttmpSInc = tempoStressInc.split(';')
    lsttmpSvmMtx = tempoSvmMtx.split(';')
    lsttmpEpMtx = tempoEpMtx.split(';')
    eps = (float(lsttmp3[1])-float(lsttmp2[1]))/Lext
    sig = (float(lsttmpF3[1])/Surf)
    sigComp = (float(lsttmpSComp[1]))
    sigMtx = (float(lsttmpSMtx[1]))
    sigInc = (float(lsttmpSInc[1]))
    svmMtx = (float(lsttmpSvmMtx[1]))
    epMtx = (float(lsttmpEpMtx[1]))
    outfile.write("%e"%eps+";"+"%e"%sig+";"+"%e"%sigComp+";"+"%e"%sigMtx+";"+"%e"%sigInc+";"+"%e"%epMtx+";"+"%e\n"%svmMtx)

fileD2.close()
fileD3.close()
fileF3.close()
outfile.close()

print "extract OK"
