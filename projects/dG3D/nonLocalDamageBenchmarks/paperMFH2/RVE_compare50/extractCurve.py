#-*-coding:Utf-8-*-

# script python to regroup crackpath files at the end of a MPI simulation
from os import system,getcwd,listdir

Lext =0.0001166340226167161
Surf = 6.733868435442992e-05*2.915850565417902e-06
S2 = 6.733868435442992e-05*0.0001166340226167161

print Surf

fileD2 = open("NodalDisplacement1comp0.csv",'r')
fileD3 = open("NodalDisplacement2comp0.csv",'r')
fileF3 = open("force2376comp0.csv",'r')
fileF32= open("force5678comp2.csv",'r')

fileStressComp = open("IPVolume100val1Mean.csv",'r')
fileStressMtx = open("IPVolume100val94Mean.csv",'r')
fileStressMtx2 = open("IPVolume100val96Mean.csv",'r')
fileStressInc = open("IPVolume100val101Mean.csv",'r')
fileStressInc2 = open("IPVolume100val103Mean.csv",'r')
fileSvmMtx = open("IPVolume100val93Mean.csv",'r')
fileEpMtx = open("IPVolume100val8Mean.csv",'r')

system("rm -f strainstress.csv")
outfile = open("strainstress.csv",'w')

while 1:
    tempo2 = fileD2.readline()[:-1]
    if(tempo2 == ''):
        break
    tempo3 = fileD3.readline()[:-1]
    tempoF3 = fileF3.readline()[:-1]
    tempoF32= fileF32.readline()[:-1]
    tempoStressComp = fileStressComp.readline()[:-1]
    tempoStressMtx = fileStressMtx.readline()[:-1]
    tempoStressInc = fileStressInc.readline()[:-1]
    tempoStressMtx2 = fileStressMtx2.readline()[:-1]
    tempoStressInc2 = fileStressInc2.readline()[:-1]
    tempoSvmMtx = fileSvmMtx.readline()[:-1]
    tempoEpMtx = fileEpMtx.readline()[:-1]
    lsttmp2 = tempo2.split(';')
    lsttmp3 = tempo3.split(';')
    lsttmpF3 = tempoF3.split(';')
    lsttmpF32 = tempoF32.split(';')
    lsttmpSComp = tempoStressComp.split(';')
    lsttmpSMtx = tempoStressMtx.split(';')
    lsttmpSInc = tempoStressInc.split(';')
    lsttmpSMtx2 = tempoStressMtx2.split(';')
    lsttmpSInc2 = tempoStressInc2.split(';')
    lsttmpSvmMtx = tempoSvmMtx.split(';')
    lsttmpEpMtx = tempoEpMtx.split(';')
    eps = (float(lsttmp3[1])-float(lsttmp2[1]))/Lext
    sig = (float(lsttmpF3[1])/Surf)
    sig33 = (float(lsttmpF32[1])/S2)
    sigComp = (float(lsttmpSComp[1]))
    sigMtx = (float(lsttmpSMtx[1]))
    sigInc = (float(lsttmpSInc[1]))
    sigMtx33 = (float(lsttmpSMtx2[1]))
    sigInc33 = (float(lsttmpSInc2[1]))
    svmMtx = (float(lsttmpSvmMtx[1]))
    epMtx = (float(lsttmpEpMtx[1]))
    outfile.write("%e"%eps+";"+"%e"%sig+";"+"%e"%sigComp+";"+"%e"%sigMtx+";"+"%e"%sigInc+";"+"%e"%epMtx+";"+"%e"%svmMtx+";"+"%e"%sig33+";"+"%e"%sigMtx33+";"+"%e\n"%sigInc33)

fileD2.close()
fileD3.close()
fileF3.close()
fileF32.close()


outfile.close()

print "extract OK"
