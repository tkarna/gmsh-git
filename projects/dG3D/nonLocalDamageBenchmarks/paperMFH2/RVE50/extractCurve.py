#-*-coding:Utf-8-*-

# script python to regroup crackpath files at the end of a MPI simulation
from os import system,getcwd,listdir

Lext = 0.0001166340226167161 
Surf = 6.733868435442992e-05*2.915850565417902e-06
S2   = 0.0001166340226167161*6.733868435442992e-05
print Surf

fileD2 = open("NodalDisplacement1comp0.csv",'r')
fileD3 = open("NodalDisplacement7comp0.csv",'r')
fileF3 = open("force102comp0.csv",'r')
fileF32 = open("force122comp2.csv",'r')

fileStressMtxMean = open("IPVolume51val1Mean.csv",'r')
fileStressMtxMean2 = open("IPVolume51val3Mean.csv",'r')
fileStressMtxMax = open("IPVolume51val1Max.csv",'r')
fileStressMtxMin = open("IPVolume51val1Min.csv",'r')
fileStressIncMean = open("IPVolume52val1Mean.csv",'r')
fileStressIncMean2 = open("IPVolume52val3Mean.csv",'r')
fileStressIncMax = open("IPVolume52val1Max.csv",'r')
fileStressIncMin = open("IPVolume52val1Min.csv",'r')
fileSvmMtxMean = open("IPVolume51val0Mean.csv",'r')
fileSvmMtxMax = open("IPVolume51val0Max.csv",'r')
fileSvmMtxMin = open("IPVolume51val0Min.csv",'r')
filePMtxMean = open("IPVolume51val8Mean.csv",'r')
filePMtxMax = open("IPVolume51val8Max.csv",'r')
filePMtxMin = open("IPVolume51val8Min.csv",'r')

system("rm -f strainstress.csv")
outfile = open("strainstress.csv",'w')

while 1:
    tempo2 = fileD2.readline()[:-1]
    if(tempo2 == ''):
        break
    tempo3 = fileD3.readline()[:-1]
    tempoF3 = fileF3.readline()[:-1]
    tempoF32 = fileF32.readline()[:-1]
    tempoStressMtxMean = fileStressMtxMean.readline()[:-1]
    tempoStressMtxMean2 = fileStressMtxMean2.readline()[:-1]
    tempoStressMtxMax = fileStressMtxMax.readline()[:-1]
    tempoStressMtxMin = fileStressMtxMin.readline()[:-1]
    tempoStressIncMean = fileStressIncMean.readline()[:-1]
    tempoStressIncMean2 = fileStressIncMean2.readline()[:-1]
    tempoStressIncMax = fileStressIncMax.readline()[:-1]
    tempoStressIncMin = fileStressIncMin.readline()[:-1]
    tempoSvmMtxMean = fileSvmMtxMean.readline()[:-1]
    tempoSvmMtxMax = fileSvmMtxMax.readline()[:-1]
    tempoSvmMtxMin = fileSvmMtxMin.readline()[:-1]
    tempoPMtxMean = filePMtxMean.readline()[:-1]
    tempoPMtxMax = filePMtxMax.readline()[:-1]
    tempoPMtxMin = filePMtxMin.readline()[:-1]
    lsttmp2 = tempo2.split(';')
    lsttmp3 = tempo3.split(';')
    lsttmpF3 = tempoF3.split(';')
    lsttmpF32 = tempoF32.split(';')
    lsttmpSMtxMean = tempoStressMtxMean.split(';')
    lsttmpSMtxMean2 = tempoStressMtxMean2.split(';')
    lsttmpSMtxMax = tempoStressMtxMax.split(';')
    lsttmpSMtxMin = tempoStressMtxMin.split(';')
    lsttmpSIncMean = tempoStressIncMean.split(';')
    lsttmpSIncMean2 = tempoStressIncMean2.split(';')
    lsttmpSIncMax = tempoStressIncMax.split(';')
    lsttmpSIncMin = tempoStressIncMin.split(';')
    lsttmpSvmMtxMean = tempoSvmMtxMean.split(';')
    lsttmpSvmMtxMax = tempoSvmMtxMax.split(';')
    lsttmpSvmMtxMin = tempoSvmMtxMin.split(';')
    lsttmpPMtxMean = tempoPMtxMean.split(';')
    lsttmpPMtxMax = tempoPMtxMax.split(';')
    lsttmpPMtxMin = tempoPMtxMin.split(';')
    eps = (float(lsttmp3[1])-float(lsttmp2[1]))/Lext
    sig = (float(lsttmpF3[1])/Surf)
    sig33 = (float(lsttmpF32[1])/S2)
    sigMtxMean = (float(lsttmpSMtxMean[1]))
    sigMtxMean33 = (float(lsttmpSMtxMean2[1]))
    sigMtxMax = (float(lsttmpSMtxMax[1]))
    sigMtxMin = (float(lsttmpSMtxMin[1]))
    sigIncMean = (float(lsttmpSIncMean[1]))
    sigIncMean33 = (float(lsttmpSIncMean2[1]))
    sigIncMax = (float(lsttmpSIncMax[1]))
    sigIncMin = (float(lsttmpSIncMin[1]))
    svmMtxMean = (float(lsttmpSvmMtxMean[1]))
    svmMtxMax = (float(lsttmpSvmMtxMax[1]))
    svmMtxMin = (float(lsttmpSvmMtxMin[1]))
    pMtxMean = (float(lsttmpPMtxMean[1]))
    pMtxMax = (float(lsttmpPMtxMax[1]))
    pMtxMin = (float(lsttmpPMtxMin[1]))
    outfile.write("%e"%eps+";"+"%e"%sig+";"+"%e"%sigMtxMean+";"+"%e"%sigMtxMax+";"+"%e"%sigMtxMin+";"+"%e"%sigIncMean+";"+"%e"%sigIncMax+";"+"%e"%sigIncMin+";"+"%e"%pMtxMean+";"+"%e"%pMtxMax+";"+"%e"%pMtxMin+";"+"%e"%svmMtxMean+";"+"%e"%svmMtxMax+";"+"%e"%svmMtxMin+";"+"%e"%sig33+";"+"%e"%sigMtxMean33+";"+"%e\n"%sigIncMean33)

fileD2.close()
fileD3.close()
fileF3.close()
outfile.close()

print "extract OK"
