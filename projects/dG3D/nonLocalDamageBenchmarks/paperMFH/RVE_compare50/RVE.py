#coding-Utf-8-*-
from gmshpy import *
from dG3Dpy import*
#from dG3DpyDebug import*

#script to launch composite problem with a python script

# material law
lawnonlocal = 1 # unique number of law
rho = 7850. # Bulk mass

# geometry
meshfile="RVE.msh" # name of mesh file
propertiesLC="lemaitreChabocheMFH.i01" #properties file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 3000   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=10 # Number of step between 2 archiving (used only if soltype=1)
fulldg = 0
eqRatio  =1.e6

#  compute solution and BC (given directly to the solver
# creation of law
law1 = NonLocalDamageDG3DMaterialLaw(lawnonlocal,rho,propertiesLC)

# creation of ElasticField
nfield = 100 # number of the field (physical number of surface)
space1 = 0 # function space (Lagrange=0)
myfield1 = nonLocalDamageDG3DDomain(1000,nfield,space1,lawnonlocal,fulldg,eqRatio)
#myfield1.matrixByPerturbation(1,1e-8)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
# BC
#shearing
#mysolver.displacementBC("Face",1234,0,0.)
#mysolver.displacementBC("Face",1234,1,0.)
#mysolver.displacementBC("Face",1234,2,0.)
#mysolver.displacementBC("Face",5678,1,0.00)
#mysolver.displacementBC("Face",5678,0,0.00001)
#mysolver.forceBC("Face",2376,2,100000000)
#mysolver.forceBC("Face",4158,2,-100000000)
#mysolver.forceBC("Face",5678,0,100000000)
#tension along z
#mysolver.displacementBC("Face",1234,2,0.)
#mysolver.displacementBC("Face",5678,2,0.0001)
#mysolver.displacementBC("Face",4158,0,0.)
#mysolver.displacementBC("Face",1265,1,0.)
#tension along x

#d1=0.00002567;
d1=0.000014
d2=0.000021
d3=0.0

cyclicFunction1=cycleFunctionTime(ftime/4., d1, 3.*ftime/4., d2,ftime, d3); 
#fct1 = simpleFunctionTimeDouble(d1)
mysolver.displacementBC("Face",4158,0,0.)
mysolver.displacementBC("Face",2376,0,cyclicFunction1)
#mysolver.displacementBC("Face",2376,0,d1)
mysolver.displacementBC("Face",1234,2,0.)
mysolver.displacementBC("Face",5678,2,0.)
mysolver.displacementBC("Face",1265,1,0.)


mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("damage",IPField.DAMAGE, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 4158, 0, nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 2376, 0, nstepArch)
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.PLASTICSTRAIN,IPField.MIN_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.PLASTICSTRAIN,IPField.MAX_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.PLASTICSTRAIN,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.DAMAGE,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.DAMAGE,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield, IPField.DAMAGE,IPField.MEAN_VALUE, nstepArch);


mysolver.solve()

