// creat a cube with fiber

//defination of unit
mm = 0.001;

// volum fraction

f=0.5;

// characteristic size for fibe
R= 0.05*mm;                          // fiber radius
ly=2.0*R*Sqrt(Pi/(2.0*Sqrt(3.0)*f));  // distance between fibers' centers
lx=ly*Sqrt(3.0)/2.0;

// characteristic Size for the volume
x = 6*ly;
y = 6*lx;
z = R/2.0;


// Characteristic length
Lc1=lx/5.0;
Lc2=lx/5.0;

// definition of points  at the corners of volume ******************************************
Point(1) = { 0.0 , 0.0 , 0.0 , Lc1};
Point(2) = {  x  , 0.0 , 0.0 , Lc1};
Point(3) = {  x  ,  y  , 0.0 , Lc1};
Point(4) = { 0.0 ,  y  , 0.0 , Lc1};

//half fiber *********************************
Point(5) = { 0.1115*x, 0.0 , 0.0 , Lc1};
Point(6) = { 0.322*x, 0.0 , 0.0 , Lc1};
Point(7) = { 0.8612*x, 0.0 , 0.0 , Lc1};

Point(8) = {  0.98*x ,  0.3645*y , 0.0 , Lc1};
Point(9) = {  x ,  0.6587*y , 0.0 , Lc1};
Point(10) = {  1.02*x ,  0.856*y , 0.0 , Lc1};

Point(11) = {  0.7792*x , y  , 0.0 , Lc1};
Point(12) = {  0.5671*x , y  , 0.0 , Lc1};

Point(13) = { 0.0, 0.7691*y  , 0.0 , Lc1};
Point(14) = { 0.0, 0.2538*y  , 0.0 , Lc1};
//****************
Point(15) = { 0.1115*x-R, 0.0 , 0.0 , Lc1};
Point(16) = { 0.1115*x, R , 0.0 , Lc1};
Point(17) = { 0.1115*x+R, 0.0 , 0.0 , Lc1};

Point(18) = { 0.322*x-R, 0.0 , 0.0 , Lc1};
Point(19) = { 0.322*x, R , 0.0 , Lc1};
Point(20) = { 0.322*x+R, 0.0 , 0.0 , Lc1};

Point(21) = { 0.8612*x-R, 0.0 , 0.0 , Lc1};
Point(22) = { 0.8612*x, R , 0.0 , Lc1};
Point(23) = { 0.8612*x+R, 0.0 , 0.0 , Lc1};
//*******************
Point(24) = {  x ,  0.3645*y-Sqrt(R*R-0.0004*x*x) , 0.0 , Lc1};
Point(25) = {  0.98*x-R ,  0.3645*y , 0.0 , Lc1};
Point(26) = {  x ,  0.3645*y+Sqrt(R*R-0.0004*x*x)  , 0.0 , Lc1};

Point(27) = {  x ,   0.6587*y-R , 0.0 , Lc1};
Point(28) = {  x-R ,   0.6587*y , 0.0 , Lc1};
Point(29) = {  x ,   0.6587*y+R  , 0.0 , Lc1};

Point(30) = {  x ,  0.856*y-Sqrt(R*R-0.0004*x*x) , 0.0 , Lc1};
Point(31) = {  1.02*x-R ,  0.856*y , 0.0 , Lc1};
Point(32) = {  x ,  0.856*y+Sqrt(R*R-0.0004*x*x)  , 0.0 , Lc1};

//********************
Point(33) = {  0.7792*x+R , y  , 0.0 , Lc1};
Point(34) = {  0.7792*x , y-R  , 0.0 , Lc1};
Point(35) = {  0.7792*x-R , y  , 0.0 , Lc1};

Point(36) = {  0.5671*x+R , y  , 0.0 , Lc1};
Point(37) = {  0.5671*x , y-R  , 0.0 , Lc1};
Point(38) = {  0.5671*x-R , y  , 0.0 , Lc1};
//********************
Point(39) = { 0.0, 0.7691*y+R  , 0.0 , Lc1};
Point(40) = { 0.0+R, 0.7691*y  , 0.0 , Lc1};
Point(41) = { 0.0, 0.7691*y-R  , 0.0 , Lc1};

Point(42) = { 0.0, 0.2538*y+R  , 0.0 , Lc1};
Point(43) = { 0.0+R, 0.2538*y  , 0.0 , Lc1};
Point(44) = { 0.0, 0.2538*y-R  , 0.0 , Lc1};

Line(1) = {1,15};
Circle(2) = {15,5,16};
Circle(3) = {16,5,17};
Line(4) = {17,18};
Circle(5) = {18,6,19};
Circle(6) = {19,6,20};
Line(7) = {20,21};
Circle(8) = {21,7,22};
Circle(9) = {22,7,23};
Line(10) = {23,2};

Line(11) = {2,24};
Circle(12) = {24,8,25};
Circle(13) = {25,8,26};
Line(14) = {26,27};
Circle(15) = {27,9,28};
Circle(16) = {28,9,29};
Line(17) = {29,30};
Circle(18) = {30,10,31};
Circle(19) = {31,10,32};
Line(20) = {32,3};

Line(21) = {3,33};
Circle(22) = {33,11,34};
Circle(23) = {34,11,35};
Line(24) = {35,36};
Circle(25) = {36,12,37};
Circle(26) = {37,12,38};
Line(27) = {38,4};

Line(28) = {4,39};
Circle(29) = {39,13,40};
Circle(30) = {40,13,41};
Line(31) = {41,42};
Circle(32) = {42,14,43};
Circle(33) = {43,14,44};
Line(34) = {44,1};

Line(35) = {15,17};
Line(36) = {18,20};
Line(37) = {21,23};
Line(38) = {24,26};
Line(39) = {27,29};
Line(40) = {30,32};
Line(41) = {33,35};
Line(42) = {36,38};
Line(43) = {39,41};
Line(44) = {42,44};

//define fibers at boundary*************************************
t=0;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-3,-2,35}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-6,-5,36}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-9,-8,37}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-13,-12,38}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-16,-15,39}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-19,-18,40}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-23,-22,41}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-26,-25,42}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-30,-29,43}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-33,-32,44}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};


// definition of points  

xc[]={0.125, 0.2185, 0.0812, 0.4117, 0.8144, 0.7363, 0.1262, 0.8591, 0.3484, 0.6405, 0.2222, 0.4529, 0.3005, 0.8530, 0.8948, 0.5673, 0.2797, 0.5569, 0.4534, 0.2250, 0.9170, 0.7618, 0.1265, 0.6256, 0.4271, 0.7633, 0.5002, 0.6798, 0.4259, 0.3161, 0.6192};
yc[]={0.882, 0.3121, 0.4378, 0.7498, 0.3704, 0.7911, 0.6354, 0.8697, 0.4012, 0.4889, 0.7574, 0.5696, 0.1652, 0.6977, 0.1823, 0.2403, 0.9088, 0.8039, 0.0894, 0.5005, 0.5102, 0.1340, 0.1636, 0.6559, 0.2517, 0.5658, 0.3868, 0.3319, 0.9153, 0.6244, 0.0851};

For i In {0:30}

t=t+1;
x0 = xc[i]*x;
y0 = yc[i]*y;
p1 = newp; Point(p1) = {x0, y0, 0.0 ,Lc2};
p2 = newp; Point(p2) = {x0, y0-R ,0.0 ,Lc2}; 
p3 = newp; Point(p3) = {x0+R, y0 ,0.0 ,Lc2};
p4 = newp; Point(p4) = {x0, y0+R ,0.0 ,Lc2}; 
p5 = newp; Point(p5) = {x0-R, y0 ,0.0 ,Lc2}; 

//define new circles

c1 = newreg; Circle(c1) = {p2,p1,p3};
c2 = newreg; Circle(c2) = {p3,p1,p4};
c3 = newreg; Circle(c3) = {p4,p1,p5};
c4 = newreg; Circle(c4) = {p5,p1,p2};
theloop[i] = newreg; Line Loop(theloop[i]) = {c1,c2,c3,c4}; 

surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {theloop[i]};

EndFor

// Surface definition
lineloop_matix = newreg;
Line Loop(lineloop_matix) = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34};

// define the surface for matrix
MaS=news;
Plane Surface(MaS) = {lineloop_matix,theloop[]};

//Volume of fiber and matrix ******************************************************
my_fiberV[] = Extrude {0.0 , 0.0 , z} {Surface{surface_edg[]};Layers{1};};
my_mtrixV[] = Extrude {0.0 , 0.0 , z} {Surface {MaS}; Layers{1};};



// Physical objects to applied BC **********************************

// Physical objects to applied material*********************
Physical Volume(51) ={my_mtrixV[]};
Physical Volume(52) ={my_fiberV[]};


//fix x displacement
Physical Surface(101) = {1375,404,1387,421,1399};
//add x displacement
Physical Surface(102) = {1307,319,1319,336,1331,353,1343};

//fix y displacement
Physical Surface(111) = {1267,268,1279,285,1291,302,1303};
Physical Surface(112) = {1347,370,1359,387,1371};

//fix z displacement
Physical Surface(121) = {MaS,surface_edg[]};
Physical Surface(122) = {354,337,320,752,303,884,598,730,532,906,994,554,371,950,642,1038,1104,774,818,388,1016,840,972,686,510,1060,1082,620,796,664,862,466,708,286,269,928,422,488,576,444,405,1896};

//define transfinite mesh***************************************
//Transfinite Line {l3,l4} = 2;










