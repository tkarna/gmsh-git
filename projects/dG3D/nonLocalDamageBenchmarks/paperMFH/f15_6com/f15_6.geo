// creat a cube with fiber

//defination of unit
mm = 0.001;

// volum fraction

f=0.15;

// characteristic size for fibe
R= 0.05*mm;                          // fiber radius
ly=2.0*R*Sqrt(Pi/(2.0*Sqrt(3.0)*f));  // distance between fibers' centers
lx=ly*Sqrt(3.0)/2.0;

// characteristic Size for the volum
x = 2*ly;
y = 2*lx;
z = R/2.0;


// Characteristic length
Lc1=lx/4.0;
Lc2=0.03*mm;

// definition of points***********************************************
Point(1) = { 0.0 , 0.0 , 0.0 , Lc1};
Point(2) = {  x  , 0.0 , 0.0 , Lc1};
Point(3) = {  x  ,  y  , 0.0 , Lc1};
Point(4) = { 0.0 ,  y  , 0.0 , Lc1};

// Line between points
Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};


//define line Loop list for fibers
t=0;
//fiber*************************************************************

//fiber1
x1=0.55*ly;
y1=0.5*lx;

//define new point
p1 = newp; Point(p1) = {x1,y1 ,0.0 ,Lc2};
p2 = newp; Point(p2) = {x1-R,y1 ,0.0 ,Lc2};
p3 = newp; Point(p3) = {x1,y1-R ,0.0 ,Lc2};
p4 = newp; Point(p4) = {x1+R,y1 ,0.0 ,Lc2};
p5 = newp; Point(p5) = {x1,y1+R ,0.0 ,Lc2};

//define new circles

c1 = newreg; Circle(c1) = {p2,p1,p3};
c2 = newreg; Circle(c2) = {p3,p1,p4};
c3 = newreg; Circle(c3) = {p4,p1,p5};
c4 = newreg; Circle(c4) = {p5,p1,p2};

Transfinite Line {c1,c2,c3,c4} = 4;

theloop[t] = newreg; Line Loop(theloop[t]) = {c1,c2,c3,c4}; 

thesurface[t] = news;
Plane Surface( thesurface[t]) = {theloop[t]};

//****************************************************************
t=t+1;

//fiber2
x1=1.6*ly;
y1=0.7*lx;

//define new point
p1 = newp; Point(p1) = {x1,y1 ,0.0 ,Lc2};
p2 = newp; Point(p2) = {x1-R,y1 ,0.0 ,Lc2};
p3 = newp; Point(p3) = {x1,y1-R ,0.0 ,Lc2};
p4 = newp; Point(p4) = {x1+R,y1 ,0.0 ,Lc2};
p5 = newp; Point(p5) = {x1,y1+R ,0.0 ,Lc2};


//define new circles

c1 = newreg; Circle(c1) = {p2,p1,p3};
c2 = newreg; Circle(c2) = {p3,p1,p4};
c3 = newreg; Circle(c3) = {p4,p1,p5};
c4 = newreg; Circle(c4) = {p5,p1,p2};

Transfinite Line {c1,c2,c3,c4} = 4;

theloop[t] = newreg; Line Loop(theloop[t]) = {c1,c2,c3,c4}; 

thesurface[t] = news;
Plane Surface( thesurface[t]) = {theloop[t]};

//****************************************************************
t=t+1;

//fiber3
x1=1.5*ly;
y1=1.6*lx;

//define new point
p1 = newp; Point(p1) = {x1,y1 ,0.0 ,Lc2};
p2 = newp; Point(p2) = {x1-R,y1 ,0.0 ,Lc2};
p3 = newp; Point(p3) = {x1,y1-R ,0.0 ,Lc2};
p4 = newp; Point(p4) = {x1+R,y1 ,0.0 ,Lc2};
p5 = newp; Point(p5) = {x1,y1+R ,0.0 ,Lc2};


//define new circles

c1 = newreg; Circle(c1) = {p2,p1,p3};
c2 = newreg; Circle(c2) = {p3,p1,p4};
c3 = newreg; Circle(c3) = {p4,p1,p5};
c4 = newreg; Circle(c4) = {p5,p1,p2};

Transfinite Line {c1,c2,c3,c4} = 4;

theloop[t] = newreg; Line Loop(theloop[t]) = {c1,c2,c3,c4}; 

thesurface[t] = news;
Plane Surface( thesurface[t]) = {theloop[t]};

//****************************************************************
t=t+1;

//fiber4
x1=0.65*ly;
y1=1.3*lx;

//define new point
p1 = newp; Point(p1) = {x1,y1 ,0.0 ,Lc2};
p2 = newp; Point(p2) = {x1-R,y1 ,0.0 ,Lc2};
p3 = newp; Point(p3) = {x1,y1-R ,0.0 ,Lc2};
p4 = newp; Point(p4) = {x1+R,y1 ,0.0 ,Lc2};
p5 = newp; Point(p5) = {x1,y1+R ,0.0 ,Lc2};


//define new circles

c1 = newreg; Circle(c1) = {p2,p1,p3};
c2 = newreg; Circle(c2) = {p3,p1,p4};
c3 = newreg; Circle(c3) = {p4,p1,p5};
c4 = newreg; Circle(c4) = {p5,p1,p2};

Transfinite Line {c1,c2,c3,c4} = 4;

theloop[t] = newreg; Line Loop(theloop[t]) = {c1,c2,c3,c4}; 

thesurface[t] = news;
Plane Surface( thesurface[t]) = {theloop[t]};



//define the line loop for matrix surface****************************

// Surface definition
lineloop_matix = newreg;

Line Loop(lineloop_matix) = {1,2,3,4};

// define the surface for matrix
MaS=news;
Plane Surface(MaS) = {lineloop_matix,theloop[]};


//VOlume of fiber and matrix ******************************************************
my_fiberV[] = Extrude {0.0 , 0.0 , z} {Surface{thesurface[]}; Layers{1};};
my_mtrixV[] = Extrude {0.0 , 0.0 , z} {Surface {MaS}; Layers{1};};



// Physical objects to applied BC **********************************
//fix x displacement
Physical Surface(101) = {155};

//add x displacement
Physical Surface(102) = {147};

//fix y displacement
Physical Surface(111) = {143};
Physical Surface(112) = {151};

//fix z displacement
Physical Surface(121) = {MaS,thesurface[]};
Physical Surface(122) = {220,52,74,96,118};



// Physical objects to applied material*********************
Physical Volume(51) ={my_mtrixV[]};
Physical Volume(52) ={my_fiberV[]};


//define transfinite mesh***************************************
//Transfinite Line {1,l4} = 2;










