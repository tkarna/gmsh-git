// creat a cube with fiber

//defination of unit
mm = 0.001;

// volum fraction

f=0.15;

// characteristic size for fibe
R= 0.05*mm;                          // fiber radius
ly=2.0*R*Sqrt(Pi/(2.0*Sqrt(3.0)*f));  // distance between fibers' centers
lx=ly*Sqrt(3.0)/2.0;

// characteristic Size for the volume
x = 6*ly;
y = 6*lx;
z = R/2.0;


// Characteristic length
Lc1=lx/3.0;
Lc2=lx/3.0;

// definition of points  at the corners of volume ******************************************
Point(1) = { 0.0 , 0.0 , 0.0 , Lc1};
Point(2) = {  x  , 0.0 , 0.0 , Lc1};
Point(3) = {  x  ,  y  , 0.0 , Lc1};
Point(4) = { 0.0 ,  y  , 0.0 , Lc1};

//half fiber *********************************
Point(5) = { 0.161*x, 0.0*y , 0.0 , Lc1};
Point(6) = { 0.7850*x, 0.0 , 0.0 , Lc1};

Point(7) = {  1.0*x ,  0.1101*y , 0.0 , Lc1};
Point(8) = {  1.0*x ,  0.7384*y , 0.0 , Lc1};

Point(9) = {  0.8955*x , 1.0*y  , 0.0 , Lc1};
Point(10) = { 0.0884*x , y  , 0.0 , Lc1};

Point(11) = { 0.0*x, 0.8271*y  , 0.0 , Lc1};
Point(12) = { 0.0*x, 0.1088*y  , 0.0 , Lc1};
//****************
Point(13) = { 0.161*x-R, 0.0 , 0.0 , Lc1};
Point(14) = { 0.161*x, R, 0.0 , Lc1};
Point(15) = { 0.161*x+R, 0.0 , 0.0 , Lc1};

Point(16) = { 0.7850*x-R, 0.0 , 0.0 , Lc1};
Point(17) = { 0.7850*x, R , 0.0 , Lc1};
Point(18) = { 0.7850*x+R, 0.0 , 0.0 , Lc1};

//*******************
Point(19) = {  1.0*x ,  0.1101*y-R, 0.0 , Lc1};
Point(20) = {  1.0*x-R ,  0.1101*y , 0.0 , Lc1};
Point(21) = {  1.0*x ,  0.1101*y+R  , 0.0 , Lc1};

Point(22) = {  1.0*x ,   0.7384*y-R , 0.0 , Lc1};
Point(23) = {  1.0*x-R , 0.7384*y , 0.0 , Lc1};
Point(24) = {  1.0*x ,   0.7384*y+R  , 0.0 , Lc1};
//********************
Point(25) = {  0.8955*x+R , y  , 0.0 , Lc1};
Point(26) = {  0.8955*x , y-R  , 0.0 , Lc1};
Point(27) = {  0.8955*x-R, y  , 0.0 , Lc1};

Point(28) = {  0.0884*x+R , y  , 0.0 , Lc1};
Point(29) = {  0.0884*x , y-R  , 0.0 , Lc1};
Point(30) = {  0.0884*x-R , y  , 0.0 , Lc1};
//********************
Point(31) = { 0.0, 0.8271*y+R  , 0.0 , Lc1};
Point(32) = { 0.0+R, 0.8271*y  , 0.0 , Lc1};
Point(33) = { 0.0, 0.8271*y-R  , 0.0 , Lc1};

Point(34) = { 0.0, 0.1088*y+R  , 0.0 , Lc1};
Point(35) = { 0.0*x+R, 0.1088*y  , 0.0 , Lc1};
Point(36) = { 0.0, 0.1088*y-R , 0.0 , Lc1};

Line(1) = {1,13};
Circle(2) = {13,5,14};
Circle(3) = {14,5,15};
Line(4) = {15,16};
Circle(5) = {16,6,17};
Circle(6) = {17,6,18};
Line(7) = {18,2};

Line(8) = {2,19};
Circle(9) = {19,7,20};
Circle(10) = {20,7,21};
Line(11) = {21,22};
Circle(12) = {22,8,23};
Circle(13) = {23,8,24};
Line(14) = {24,3};

Line(15) = {3,25};
Circle(16) = {25,9,26};
Circle(17) = {26,9,27};
Line(18) = {27,28};
Circle(19) = {28,10,29};
Circle(20) = {29,10,30};
Line(21) = {30,4};

Line(22) = {4,31};
Circle(23) = {31,11,32};
Circle(24) = {32,11,33};
Line(25) = {33,34};
Circle(26) = {34,12,35};
Circle(27) = {35,12,36};
Line(28) = {36,1};

Line(29) = {13,15};
Line(30) = {16,18};
Line(31) = {19,21};
Line(32) = {22,24};
Line(33) = {25,27};
Line(34) = {28,30};
Line(35) = {31,33};
Line(36) = {34,36};

//define fibers at boundary*************************************
t=0;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-3,-2,29}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-6,-5,30}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-10,-9,31}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-13,-12,32}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-17,-16,33}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-20,-19,34}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-24,-23,35}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

t=t+1;
loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {-27,-26,36}; 
surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};


// definition of points  

xc[]={0.3477, 0.6378, 0.3761, 0.1785, 0.5532, 0.8417, 0.2756, 0.9172, 0.7591, 0.7910, 0.4097, 0.8502, 0.6842, 0.5445, 0.4870, 0.5257, 0.2302, 0.5013, 0.6977, 0.3460, 0.1203, 0.7319, 0.7717, 0.1378, 0.0757, 0.6039, 0.9113, 0.4106, 0.0859, 0.6508, 0.27, 0.178};
yc[]={0.4881, 0.4022, 0.3019, 0.5619, 0.6726, 0.7856, 0.7277, 0.5602, 0.2842, 0.6235, 0.6976, 0.1501, 0.7558, 0.0893, 0.5281, 0.9003, 0.2814, 0.3676, 0.1211, 0.9044, 0.7093, 0.9104, 0.4459, 0.1353, 0.4535, 0.2383, 0.3469, 0.1282, 0.2919, 0.5595, 0.09, 0.88};

For i In {0:31}

t=t+1;
x0 = xc[i]*x;
y0 = yc[i]*y;
p1 = newp; Point(p1) = {x0, y0, 0.0 ,Lc2};
p2 = newp; Point(p2) = {x0, y0-R ,0.0 ,Lc2}; 
p3 = newp; Point(p3) = {x0+R, y0 ,0.0 ,Lc2};
p4 = newp; Point(p4) = {x0, y0+R ,0.0 ,Lc2}; 
p5 = newp; Point(p5) = {x0-R, y0 ,0.0 ,Lc2}; 

//define new circles

c1 = newreg; Circle(c1) = {p2,p1,p3};
c2 = newreg; Circle(c2) = {p3,p1,p4};
c3 = newreg; Circle(c3) = {p4,p1,p5};
c4 = newreg; Circle(c4) = {p5,p1,p2};
theloop[i] = newreg; Line Loop(theloop[i]) = {c1,c2,c3,c4}; 

surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {theloop[i]};

EndFor

// Surface definition
lineloop_matix = newreg;
Line Loop(lineloop_matix) = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28};

// define the surface for matrix
MaS=news;
Plane Surface(MaS) = {lineloop_matix,theloop[]};

//Volume of fiber and matrix ******************************************************
my_fiberV[] = Extrude {0.0 , 0.0 , z} {Surface{surface_edg[]};Layers{1};};
my_mtrixV[] = Extrude {0.0 , 0.0 , z} {Surface {MaS}; Layers{1};};



// Physical objects to applied BC **********************************

// Physical objects to applied material*********************
Physical Volume(51) ={my_mtrixV[]};
Physical Volume(52) ={my_fiberV[]};


//fix x displacement
Physical Surface(101) = {1331,364,1343,381,1355};
//add x displacement
Physical Surface(102) = {1275,296,1287,313,1299};

//fix y displacement
Physical Surface(111) = {1247,262,1259,279,1271};
Physical Surface(112) = {1303,330,1315,347,1327};

//fix z displacement
Physical Surface(121) = {MaS,surface_edg[]};
Physical Surface(122) = {1086,365,976,844,348,514,954,668,331,932,646,734,314,602,822,800,624,404,558,1020,866,536,448,756,470,1042,778,382,426,910,888,998,580,263,690,492,1064,712,280,297,1868};

//define transfinite mesh***************************************
//Transfinite Line {l3,l4} = 2;










