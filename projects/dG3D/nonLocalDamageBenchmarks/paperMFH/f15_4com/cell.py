#coding-Utf-8-*-
from gmshpy import *
#from dG3DpyDebug import*
from dG3Dpy import*

#script to launch composite problem with a python script

# material law
lawnonlocal = 1 # unique number of law
lawlinear = 2 # unique number of law
rho = 7850. # Bulk mass



# geometry
#meshfile="cell6.msh" # name of mesh file
meshfile="cell.msh" # name of mesh file
propertiesLC="lemaitreChaboche.i01" #properties file
propertiesLin="linear.i01" #properties file
#propertiesNoDamage="noDamage.i01" #properties file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 3000   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-4   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=10 # Number of step between 2 archiving (used only if soltype=1)
fulldg = 0
beta1 = 40
eqRatio = 1.e6

#  compute solution and BC (given directly to the solver
# creation of law
law1 = NonLocalDamageDG3DMaterialLaw(lawnonlocal,rho,propertiesLC)
law2 = NonLocalDamageDG3DMaterialLaw(lawlinear,rho,propertiesLin)

# creation of ElasticField
Mfield = 51 # number of the field (physical number of Mtrix)
Ffield = 52 # number of the field (physical number of Fiber)

space1 = 0 # function space (Lagrange=0)

Matixfield = nonLocalDamageDG3DDomain(1000,Mfield,space1,lawnonlocal,fulld,eqRatio)
Fiberfield = nonLocalDamageDG3DDomain(1000,Ffield,space1,lawlinear,fulldg,eqRatio)
Matixfield.stabilityParameters(beta1)
Matixfield.nonLocalStabilityParameters(30,1)
Fiberfield.stabilityParameters(beta1)
Fiberfield.nonLocalStabilityParameters(30,1)

#myfield1.matrixByPerturbation(1,1e-8)

#in // we need to create the ininterdomain after adding the domains
myinterfield = nonLocalInterDomainBetween3D(1000,Matixfield,Fiberfield,eqRatio,0) #law = 0
myinterfield.stabilityParameters(beta1)
myinterfield.nonLocalStabilityParameters(beta1,1)  #interface is continous

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(Matixfield)
mysolver.addDomain(Fiberfield)
mysolver.addDomain(myinterfield)
mysolver.addMaterialLaw(law1)
mysolver.addMaterialLaw(law2)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)

# BC
# fixed face
mysolver.displacementBC("Face",101,0,0.)
mysolver.displacementBC("Face",111,1,0.)
mysolver.constraintBC("Face",112,1)
mysolver.displacementBC("Face",121,2,0.)
mysolver.displacementBC("Face",122,2,0.)


# displacement
#15
d1=0.000032332
d2=0.000048497
d3=0.0
#30
#d1=0.000020785
#d2=0.000034641
#d3=0
#50
#d1=0.000016166
#d2=0.000025403
#d3=0.0


cyclicFunction1=cycleFunctionTime(ftime/4., d1, 3.*ftime/4., d2,ftime, d3);  

#mysolver.displacementBC("Face",6,0,cyclicFunction1)
#mysolver.displacementBC("Face",7,0,cyclicFunction1)
#mysolver.displacementBC("Face",8,0,cyclicFunction1)
#mysolver.displacementBC("Face",9,0,cyclicFunction1)
mysolver.displacementBC("Face",102,0,cyclicFunction1)

#mysolver.displacementBC("Face",102,0,d1)



# needed resultes
mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("damage",IPField.DAMAGE, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 101, 0, nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 102, 0, nstepArch)
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.PLASTICSTRAIN,IPField.MIN_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.PLASTICSTRAIN,IPField.MAX_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.PLASTICSTRAIN,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.DAMAGE,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.DAMAGE,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",Mfield, IPField.DAMAGE,IPField.MEAN_VALUE, nstepArch);


mysolver.solve()

