#-*-coding:Utf-8-*-

# script python to regroup crackpath files at the end of a MPI simulation
from os import system,getcwd,listdir

Lext = 0.05
Surf = 0.02519*4*0.0011
print Surf

fileD2 = open("NodalDisplacement2comp0.csv",'r')
fileD3 = open("NodalDisplacement3comp0.csv",'r')
fileF3 = open("force302comp0.csv",'r')
fileDam= open("IPVolume101val7Mean.csv",'r')
fileEp = open("IPVolume101val8Mean.csv",'r')

system("rm -f strainstress.csv")
outfile = open("strainstress.csv",'w')

while 1:
    tempo2 = fileD2.readline()[:-1]
    if(tempo2 == ''):
        break
    tempo3 = fileD3.readline()[:-1]
    tempoF3 = fileF3.readline()[:-1]
    tempoDam= fileDam.readline()[:-1]
    tempoEp = fileEp.readline()[:-1]
    lsttmp2 = tempo2.split(';')
    lsttmp3 = tempo3.split(';')
    lsttmpF3 = tempoF3.split(';')
    lsttmpDam= tempoDam.split(';')
    lsttmpEp = tempoEp.split(';')
    eps = (float(lsttmp3[1])-float(lsttmp2[1]))/Lext
    sig = (float(lsttmpF3[1])/Surf)
    dam = (float(lsttmpDam[1]))
    ep = (float(lsttmpEp[1]))
    outfile.write("%e"%eps+";"+"%e"%sig+";"+"%e"%(eps*100)+";"+"%e"%(sig/1.e6)+";"+"%e"%dam+";"+"%e\n"%ep)

fileD2.close()
fileD3.close()
fileF3.close()
fileDam.close()
fileEp.close()
outfile.close()

print "extract OK"
