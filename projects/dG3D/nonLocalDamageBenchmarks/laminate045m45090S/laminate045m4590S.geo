// creat a cube with fiber

//defination of unit
mm = 0.001;

ly  = 25.19*mm;
lx  = 250*mm;
lxe = 50*mm;
ep  = 0.304*mm;

Lc1 = 1*mm;
Lc2 = 2*mm;

// definition of points
Point(1) = { 0.0 , 0.0 , 0.0 , Lc2};
Point(2) = {  (lx-lxe)/2., 0.0 , 0.0 , Lc1};
Point(3) = {  (lx+lxe)/2., 0.0 , 0.0, Lc1};
Point(4) = {  lx  , 0.0 , 0.0 , Lc2};
Point(5) = {  lx  , ly  , 0.0 , Lc2};
Point(6) = {  (lx+lxe)/2., ly, 0.0, Lc1};
Point(7) = {  (lx-lxe)/2., ly , 0.0 , Lc1};
Point(8) = { 0.0 , ly , 0.0 , Lc2};

// Line between points
Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,5};
Line(5) = {5,6};
Line(6) = {6,7};
Line(7) = {7,8};
Line(8) = {1,8};
Line(9) = {2,7};
Line(10)= {3,6};


// Surface definition
Line Loop(1) = {1,9,7,-8};
Line Loop(2) = {2,10,6,-9};
Line Loop(3) = {3,4,5,-10};

Plane Surface(1) = {1};
Plane Surface(2) = {2};
Plane Surface(3) = {3};

// define transfinite mesh
Transfinite Line {1,3,5,7} = 9;
Transfinite Line {2,6} = 16;
Transfinite Line {4,8,9,10} = 9;

Transfinite Surface {1,2,3} ; 
Recombine Surface {1,2,3};
//VOlume

vol1_1[] = Extrude {0.0 , 0.0 , ep} {Surface{1}; Layers{1}; Recombine;};
vol1_2[] = Extrude {0.0 , 0.0 , ep} {Surface{2}; Layers{1}; Recombine;};
vol1_3[] = Extrude {0.0 , 0.0 , ep} {Surface{3}; Layers{1}; Recombine;};
Recombine  Surface {vol1_1[0],vol1_2[0],vol1_3[0]};
Transfinite Volume {vol1_1[1],vol1_2[1],vol1_3[1]};

vol2_1[] = Extrude {0.0 , 0.0 , ep} {Surface{vol1_1[0]}; Layers{1}; Recombine;};
vol2_2[] = Extrude {0.0 , 0.0 , ep} {Surface{vol1_2[0]}; Layers{1}; Recombine;};
vol2_3[] = Extrude {0.0 , 0.0 , ep} {Surface{vol1_3[0]}; Layers{1}; Recombine;};
Recombine  Surface {vol2_1[0],vol2_2[0],vol2_3[0]};
Transfinite Volume {vol2_1[1],vol2_2[1],vol2_3[1]};

vol3_1[] = Extrude {0.0 , 0.0 , ep} {Surface{vol2_1[0]}; Layers{1}; Recombine;};
vol3_2[] = Extrude {0.0 , 0.0 , ep} {Surface{vol2_2[0]}; Layers{1}; Recombine;};
vol3_3[] = Extrude {0.0 , 0.0 , ep} {Surface{vol2_3[0]}; Layers{1}; Recombine;};
Recombine  Surface {vol3_1[0],vol3_2[0],vol3_3[0]};
Transfinite Volume {vol3_1[1],vol3_2[1],vol3_3[1]};

vol4_1[] = Extrude {0.0 , 0.0 , ep} {Surface{vol3_1[0]}; Layers{1}; Recombine;};
vol4_2[] = Extrude {0.0 , 0.0 , ep} {Surface{vol3_2[0]}; Layers{1}; Recombine;};
vol4_3[] = Extrude {0.0 , 0.0 , ep} {Surface{vol3_3[0]}; Layers{1}; Recombine;};
Recombine  Surface {vol4_1[0],vol4_2[0],vol4_3[0]};
Transfinite Volume {vol4_1[1],vol4_2[1],vol4_3[1]};

vol5_1[] = Extrude {0.0 , 0.0 , ep} {Surface{vol4_1[0]}; Layers{1}; Recombine;};
vol5_2[] = Extrude {0.0 , 0.0 , ep} {Surface{vol4_2[0]}; Layers{1}; Recombine;};
vol5_3[] = Extrude {0.0 , 0.0 , ep} {Surface{vol4_3[0]}; Layers{1}; Recombine;};
Recombine  Surface {vol5_1[0],vol5_2[0],vol5_3[0]};
Transfinite Volume {vol5_1[1],vol5_2[1],vol5_3[1]};

vol6_1[] = Extrude {0.0 , 0.0 , ep} {Surface{vol5_1[0]}; Layers{1}; Recombine;};
vol6_2[] = Extrude {0.0 , 0.0 , ep} {Surface{vol5_2[0]}; Layers{1}; Recombine;};
vol6_3[] = Extrude {0.0 , 0.0 , ep} {Surface{vol5_3[0]}; Layers{1}; Recombine;};
Recombine  Surface {vol6_1[0],vol6_2[0],vol6_3[0]};
Transfinite Volume {vol6_1[1],vol6_2[1],vol6_3[1]};

vol7_1[] = Extrude {0.0 , 0.0 , ep} {Surface{vol6_1[0]}; Layers{1}; Recombine;};
vol7_2[] = Extrude {0.0 , 0.0 , ep} {Surface{vol6_2[0]}; Layers{1}; Recombine;};
vol7_3[] = Extrude {0.0 , 0.0 , ep} {Surface{vol6_3[0]}; Layers{1}; Recombine;};
Recombine  Surface {vol7_1[0],vol7_2[0],vol7_3[0]};
Transfinite Volume {vol7_1[1],vol7_2[1],vol7_3[1]};

vol8_1[] = Extrude {0.0 , 0.0 , ep} {Surface{vol7_1[0]}; Layers{1}; Recombine;};
vol8_2[] = Extrude {0.0 , 0.0 , ep} {Surface{vol7_2[0]}; Layers{1}; Recombine;};
vol8_3[] = Extrude {0.0 , 0.0 , ep} {Surface{vol7_3[0]}; Layers{1}; Recombine;};
Recombine  Surface {vol8_1[0],vol8_2[0],vol8_3[0]};
Transfinite Volume {vol8_1[1],vol8_2[1],vol8_3[1]};

vol9_1[] = Extrude {0.0 , 0.0 , ep} {Surface{vol8_1[0]}; Layers{1}; Recombine;};
vol9_2[] = Extrude {0.0 , 0.0 , ep} {Surface{vol8_2[0]}; Layers{1}; Recombine;};
vol9_3[] = Extrude {0.0 , 0.0 , ep} {Surface{vol8_3[0]}; Layers{1}; Recombine;};
Recombine  Surface {vol9_1[0],vol9_2[0],vol9_3[0]};
Transfinite Volume {vol9_1[1],vol9_2[1],vol9_3[1]};

vol10_1[] = Extrude {0.0 , 0.0 , ep} {Surface{vol9_1[0]}; Layers{1}; Recombine;};
vol10_2[] = Extrude {0.0 , 0.0 , ep} {Surface{vol9_2[0]}; Layers{1}; Recombine;};
vol10_3[] = Extrude {0.0 , 0.0 , ep} {Surface{vol9_3[0]}; Layers{1}; Recombine;};
Recombine  Surface {vol10_1[0],vol10_2[0],vol10_3[0]};
Transfinite Volume {vol10_1[1],vol10_2[1],vol10_3[1]};


// Physical objects to applied BC and material
//X=0
Physical Surface(301) = {97,31,229,163,295,361,427,493,559,625};
//X=L
Physical Surface(302) = {133,67,265,199,331,397,463,529,595,661};

//FOr extensometer
Physical Point(402) = {2};
Physical Point(403) = {3};

//0
Physical Volume(101) ={vol1_1[1],vol1_2[1],vol1_3[1],vol4_1[1],vol4_2[1],vol4_3[1],vol7_1[1],vol7_2[1],vol7_3[1],vol10_1[1],vol10_2[1],vol10_3[1]};
//45
Physical Volume(102) ={vol2_1[1],vol2_2[1],vol2_3[1],vol9_1[1],vol9_2[1],vol9_3[1]};
//-45
Physical Volume(103) ={vol3_1[1],vol3_2[1],vol3_3[1],vol8_1[1],vol8_2[1],vol8_3[1]};
//90
Physical Volume(104) ={vol5_1[1],vol5_2[1],vol5_3[1],vol6_1[1],vol6_2[1],vol6_3[1]};












