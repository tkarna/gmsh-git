#-*-coding:Utf-8-*-

# script python to regroup crackpath files at the end of a MPI simulation
from os import system,getcwd,listdir

Lext = 0.05
Surf = 0.02519*10*0.000304
print Surf

fileD2 = open("NodalDisplacement2comp0.csv",'r')
fileD3 = open("NodalDisplacement3comp0.csv",'r')
fileF3 = open("force302comp0.csv",'r')
fileDam0= open("IPVolume101val7Mean.csv",'r')
fileEp0 = open("IPVolume101val8Mean.csv",'r')
fileDam45= open("IPVolume102val7Mean.csv",'r')
fileEp45 = open("IPVolume102val8Mean.csv",'r')
fileDamm45= open("IPVolume103val7Mean.csv",'r')
fileEpm45 = open("IPVolume103val8Mean.csv",'r')
fileDam90= open("IPVolume104val7Mean.csv",'r')
fileEp90 = open("IPVolume104val8Mean.csv",'r')


system("rm -f strainstress.csv")
outfile = open("strainstress.csv",'w')

while 1:
    tempo2 = fileD2.readline()[:-1]
    if(tempo2 == ''):
        break
    tempo3 = fileD3.readline()[:-1]
    tempoF3 = fileF3.readline()[:-1]
    tempoDam0= fileDam0.readline()[:-1]
    tempoEp0 = fileEp0.readline()[:-1]
    tempoDam45= fileDam45.readline()[:-1]
    tempoEp45 = fileEp45.readline()[:-1]
    tempoDamm45= fileDamm45.readline()[:-1]
    tempoEpm45 = fileEpm45.readline()[:-1]
    tempoDam90= fileDam90.readline()[:-1]
    tempoEp90 = fileEp90.readline()[:-1]
    lsttmp2 = tempo2.split(';')
    lsttmp3 = tempo3.split(';')
    lsttmpF3 = tempoF3.split(';')
    lsttmpDam0= tempoDam0.split(';')
    lsttmpEp0 = tempoEp0.split(';')
    lsttmpDam45= tempoDam45.split(';')
    lsttmpEp45 = tempoEp45.split(';')
    lsttmpDamm45= tempoDamm45.split(';')
    lsttmpEpm45 = tempoEpm45.split(';')
    lsttmpDam90= tempoDam90.split(';')
    lsttmpEp90 = tempoEp90.split(';')
    eps = (float(lsttmp3[1])-float(lsttmp2[1]))/Lext
    sig = (float(lsttmpF3[1])/Surf)
    dam0 = (float(lsttmpDam0[1]))
    ep0 = (float(lsttmpEp0[1]))
    dam45 = (float(lsttmpDam45[1]))
    ep45 = (float(lsttmpEp45[1]))
    damm45 = (float(lsttmpDamm45[1]))
    epm45 = (float(lsttmpEpm45[1]))
    dam90 = (float(lsttmpDam90[1]))
    ep90 = (float(lsttmpEp90[1]))
    outfile.write("%e"%eps+";"+"%e"%sig+";"+"%e"%(eps*100)+";"+"%e"%(sig/1.e6)+";"+"%e"%dam0+";"+"%e"%dam45+";"+"%e"%damm45+";"+"%e\n"%dam90)

fileD2.close()
fileD3.close()
fileF3.close()
fileDam0.close()
fileEp0.close()
fileDam45.close()
fileEpm45.close()
fileDamm45.close()
fileEpm45.close()
fileDam90.close()
fileEp90.close()
outfile.close()

print "extract OK"
