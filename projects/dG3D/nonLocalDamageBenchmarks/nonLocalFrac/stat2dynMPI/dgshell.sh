#!/bin/bash

#SBATCH --job-name=plateHole3D
#
#SBATCH --mail-user=gauthier.becker@ulg.ac.be
#SBATCH --mail-type=ALL
#SBATCH --time=120:0:0
#SBATCH --mem-per-cpu=1000
#SBATCH --output="dgshell.out"

#SBATCH --ntasks=12
# copying mesh file
SCRATCH=/scratch/$USER/$SLURM_JOB_ID
WORKDIR=$HOME/workspace/nonLocalPlateHoleSwitch
MYMPIDIR=/usr/local/openmpi/1.5.3-gcc-4.4.4/
PYFILE=plateHole.py
MSHFILE=halfPlateHole.msh
FC1=properties_an1.i01 #properties file
FC2=properties_an2.i01 #properties file

echo Creating scratch dir $SCRATCH on $HOSTNAME
srun mkdir -p $SCRATCH || exit $?
srun cp $WORKDIR/$PYFILE $SCRATCH
srun cp $WORKDIR/$MSHFILE $SCRATCH
srun cp $WORKDIR/$FC1 $SCRATCH
srun cp $WORKDIR/$FC2 $SCRATCH
cd $SCRATCH
$MYMPIDIR/bin/mpiexec python $PYFILE

srun cp $SCRATCH/* $WORKDIR

#echo Removing $SCRATCH on $HOSTNAME
#srun rm -rf $SCRATCH || exit $?

