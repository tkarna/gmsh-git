#coding-Utf-8-*-
from gmshpy import *
#from dG3DpyDebug import*
from dG3Dpy import*

#script to launch composite problem with a python script

# material law
lawnonlocal1 = 1 # unique number of law
lawnonlocal2 = 2 # unique number of law
rho = 7850. # Bulk mass


# geometry
meshfile="halfPlateHole.msh" # name of mesh file
propertiesC1="properties_an1.i01" #properties file
propertiesC2="properties_an2.i01" #properties file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 3 # StaticLinear=0 (default) StaticNonLinear=1 Multi=3
nstepImpl = 15 #75  # number of step (used only if soltype=1)
#nstepExpl = 1000 #1000
ftime =1. #2.e-3   # Final time (used only if soltype=1)
tol=1.e-4   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch= 3 #1500 # Number of step between 2 archiving (used only if soltype=1)
fulldg = 1   # 1 use dg inside a domain
dgnl = 1     # 1 use dg for non local epl inside a domain - only if fulldg
beta1 = 20   # stability parameter for dg
dgnlinter = 0  # 1 use dg for non local epl between laminate
eqRatio = 1.e6

#  compute solution and BC (given directly to the solver
# creation of law
law1 = NonLocalDamageDG3DMaterialLaw(lawnonlocal1,rho,propertiesC1)
law2 = NonLocalDamageDG3DMaterialLaw(lawnonlocal2,rho,propertiesC2)


# creation of ElasticField
nfield1 = 51 # number of the field (physical number of surface)
nfield2 = 52 

space1 = 0 # function space (Lagrange=0)

myfield1 = nonLocalDamageDG3DDomain(1000,nfield1,space1,lawnonlocal1,fulldg,eqRatio)
myfield1.stabilityParameters(beta1)  #stabilty parameter on displacement
myfield1.nonLocalStabilityParameters(beta1,dgnl) #stabilty parameter on non local epl
myfield2 = nonLocalDamageDG3DDomain(1000,nfield2,space1,lawnonlocal2,fulldg,eqRatio)
myfield2.stabilityParameters(beta1)  #stabilty parameter on displacement
myfield2.nonLocalStabilityParameters(beta1,dgnl) #stabilty parameter on non local epl

myinterfield = nonLocalInterDomainBetween3D(1000,myfield1,myfield2,eqRatio) #law = 0
myinterfield.stabilityParameters(beta1)
myinterfield.nonLocalStabilityParameters(beta1,dgnlinter)  #interface not continous

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addDomain(myfield2)
mysolver.addDomain(myinterfield) # interface domain
mysolver.addMaterialLaw(law1)
mysolver.addMaterialLaw(law2)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstepImpl,ftime,tol)
#mysolver.explicitSpectralRadius(ftime,0.866666,1.)
#mysolver.explicitTimeStepEvaluation(nstepExpl)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.addSystem(3,1)
mysolver.addSystem(1,1)

# BC
#tension along x

d1=0.0004
d1impl=0.15*d1 # 15% in static (no damage)

mysolver.displacementBC("Face",101,0,0.)
mysolver.displacementBC("Face",102,0,d1impl)
mysolver.displacementBC("Face",122,2,0.)
mysolver.displacementBC("Face",112,1,0.)

#mysolver.constraintBC("Face",112,1)
nstepArchIP = nstepArch
mysolver.internalPointBuildView("svm",IPField.SVM, 1, nstepArchIP)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, nstepArchIP)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, nstepArchIP)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, nstepArchIP)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, nstepArchIP)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, nstepArchIP)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, nstepArchIP)
mysolver.internalPointBuildView("eps_xx",IPField.STRAIN_XX, 1, nstepArchIP)
mysolver.internalPointBuildView("eps_yy",IPField.STRAIN_YY, 1, nstepArchIP)
mysolver.internalPointBuildView("eps_zz",IPField.STRAIN_ZZ, 1, nstepArchIP)
mysolver.internalPointBuildView("eps_xy",IPField.STRAIN_XY, 1, nstepArchIP)
mysolver.internalPointBuildView("eps_yz",IPField.STRAIN_YZ, 1, nstepArchIP)
mysolver.internalPointBuildView("eps_xz",IPField.STRAIN_XZ, 1, nstepArchIP)
mysolver.internalPointBuildView("damage",IPField.DAMAGE, 1, nstepArchIP)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, nstepArchIP)
mysolver.archivingForceOnPhysicalGroup("Face", 101, 0, nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 102, 0, nstepArch)

mysolver.archivingIPOnPhysicalGroup("Volume",nfield1, IPField.PLASTICSTRAIN,IPField.MIN_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield1, IPField.PLASTICSTRAIN,IPField.MAX_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield1, IPField.PLASTICSTRAIN,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield1, IPField.DAMAGE,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield1, IPField.DAMAGE,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield1, IPField.DAMAGE,IPField.MEAN_VALUE, nstepArch);

mysolver.archivingIPOnPhysicalGroup("Volume",nfield2, IPField.PLASTICSTRAIN,IPField.MIN_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield2, IPField.PLASTICSTRAIN,IPField.MAX_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield2, IPField.PLASTICSTRAIN,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield2, IPField.DAMAGE,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield2, IPField.DAMAGE,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield2, IPField.DAMAGE,IPField.MEAN_VALUE, nstepArch);

mysolver.archivingNodeIP(21, IPField.STRAIN_XX, IPField.MAX_VALUE, nstepArch);
mysolver.archivingNodeIP(21, IPField.STRAIN_YY, IPField.MAX_VALUE, nstepArch);
mysolver.archivingNodeIP(21, IPField.DAMAGE, IPField.MAX_VALUE, nstepArch);
mysolver.archivingNodeIP(23, IPField.STRAIN_XX, IPField.MAX_VALUE, nstepArch);
mysolver.archivingNodeIP(23, IPField.STRAIN_YY, IPField.MAX_VALUE, nstepArch);
mysolver.archivingNodeIP(23, IPField.DAMAGE, IPField.MAX_VALUE, nstepArch);


#mysolver.archivingNodeDisplacement(21,0)
mysolver.archivingNodeDisplacement(23,0)
tendimpl = mysolver.solve()

# switch to explicit
nstepImpl = 600 # number of step (used only if soltype=1)
nstepExpl = 300 
oldarch = nstepArch
nstepArch= 300  
ftime = 0.0006375 # 85% in explicit with a 4x factor on the loading rate #0.000375 # all exp ftime = 1.5e-3 div 4 here as only 1/4 in explicit

mysolver.snlData(nstepImpl,ftime,tol)
mysolver.explicitSpectralRadius(ftime,0.866666,1.)
mysolver.explicitTimeStepEvaluation(nstepExpl)
mysolver.switchSystem(0,2) # switch displacement to explicit
mysolver.stepBetweenArchiving(nstepArch)
mysolver.setFactorOnArchivingFiles(nstepArch/oldarch)

d1expl = (d1 - d1impl)/ftime
functionDisp = simpleFunctionTimeWithConstantDouble(d1expl,1,1.,d1impl)
mysolver.resetBoundaryConditions()
mysolver.displacementBC("Face",101,0,0.)
mysolver.displacementBC("Face",102,0,functionDisp)
mysolver.displacementBC("Face",122,2,0.)
mysolver.displacementBC("Face",112,1,0.)

mysolver.solve()
