// creat a cube with fiber

//defination of unit
mm = 0.001;

lx=6.5*mm;

// characteristic Size for the volum
x = 50*mm;
y = 6.*lx;    //ly/2;
z = 1.1*mm;

R=6.5*mm;

x1=R*x/Sqrt(x*x+y*y);
y1=R*y/Sqrt(x*x+y*y);

// Characteristic length
Lc1=z;

// definition of points
Point(1) = { 0.0 , 0.0-y/2 , 0.0 , Lc1};
Point(2) = {  x/2.0  , 0.0-y/2 , 0.0 , Lc1};
Point(3) = {  x  , 0.0-y/2 , 0.0 , Lc1};
Point(4) = { x , y/2.-y/2 , 0.0 , Lc1};
Point(5) = {  x/2.0  , y/2.-y/2 , 0.0 , Lc1};
Point(6) = {  0.0  , y/2.-y/2 , 0.0 , Lc1};
Point(7) = {  x/2.0-x1  , y/2.0-y1-y/2 , 0.0 , Lc1};
Point(8) = {  x/2.0  , y/2.0-R-y/2 , 0.0 , Lc1};
Point(9) = {  x/2.0+x1  , y/2.0-y1-y/2 , 0.0 , Lc1};
Point(10) = {  x/2.0+R  , y/2.0-y/2 , 0.0 , Lc1};
Point(11) = {  x/2.0-R  , y/2.0-y/2 , 0.0 , Lc1};

// Line between points
Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,10};
Line(5) = {6,11};
Line(6) = {6,1};
Line(7) = {1,7};
Line(8) = {2,8};
Line(9) = {3,9};

Circle(10) = {7,5,11};
Circle(11) = {8,5,7};
Circle(12) = {9,5,8};
Circle(13) = {10,5,9};

// Surface definition
Line Loop(1) = {6,7,10,-5};
Line Loop(2) = {1,8,11,-7};
Line Loop(3) = {2,9,12,-8};
Line Loop(4) = {3,4,13,-9};

Plane Surface(1) = {1};
Plane Surface(2) = {2};
Plane Surface(3) = {3};
Plane Surface(4) = {4};

sym[]={
  Symmetry {0,1,0,0}
  {Duplicata { Surface{1,2,3,4}; } }
};


Transfinite Line {1,6,3,2,13,20,25,15,30} = 18;
Transfinite Line {12,11,10,17,22,27,32} = 18;
Transfinite Line {5,7,8,9,4,16,21,26} = 17 Using Progression 0.95;

Transfinite Surface {1,2,3,4,sym[0],sym[1],sym[2],sym[3]} ;

Recombine Surface {1,2,3,4,sym[0],sym[1],sym[2],sym[3]} ;

//VOlume

my_mat1_1[] = Extrude {0.0 , 0.0 , z} {Surface{1}; Layers{1};Recombine;};
my_mat1_2[] = Extrude {0.0 , 0.0 , z} {Surface{2}; Layers{1};Recombine;};
my_mat1_3[] = Extrude {0.0 , 0.0 , z} {Surface{3}; Layers{1};Recombine;};
my_mat1_4[] = Extrude {0.0 , 0.0 , z} {Surface{4}; Layers{1};Recombine;};
my_mat1_5[] = Extrude {0.0, 0.0,   z} {Surface{sym[0]}; Layers{1};Recombine;};
my_mat1_6[] = Extrude {0.0, 0.0,   z} {Surface{sym[1]}; Layers{1};Recombine;};
my_mat1_7[] = Extrude {0.0, 0.0,   z} {Surface{sym[2]}; Layers{1};Recombine;};
my_mat1_8[] = Extrude {0.0, 0.0,   z} {Surface{sym[3]}; Layers{1};Recombine;};
Recombine  Surface {my_mat1_1[0],my_mat1_2[0],my_mat1_3[0],my_mat1_4[0],my_mat1_5[0],my_mat1_6[0],my_mat1_7[0],my_mat1_8[0]};
Transfinite Volume {my_mat1_1[1],my_mat1_2[1],my_mat1_3[1],my_mat1_4[1],my_mat1_5[1],my_mat1_6[1],my_mat1_7[1],my_mat1_8[1]};

my_mat2_1[] = Extrude {0.0 , 0.0 , 2*z} {Surface{my_mat1_1[0]}; Layers{2};Recombine;};
my_mat2_2[] = Extrude {0.0 , 0.0 , 2*z} {Surface{my_mat1_2[0]}; Layers{2};Recombine;};
my_mat2_3[] = Extrude {0.0 , 0.0 , 2*z} {Surface{my_mat1_3[0]}; Layers{2};Recombine;};
my_mat2_4[] = Extrude {0.0 , 0.0 , 2*z} {Surface{my_mat1_4[0]}; Layers{2};Recombine;};
my_mat2_5[] = Extrude {0.0 , 0.0 , 2*z} {Surface{my_mat1_5[0]}; Layers{2};Recombine;};
my_mat2_6[] = Extrude {0.0 , 0.0 , 2*z} {Surface{my_mat1_6[0]}; Layers{2};Recombine;};
my_mat2_7[] = Extrude {0.0 , 0.0 , 2*z} {Surface{my_mat1_7[0]}; Layers{2};Recombine;};
my_mat2_8[] = Extrude {0.0 , 0.0 , 2*z} {Surface{my_mat1_8[0]}; Layers{2};Recombine;};
Recombine  Surface {my_mat2_1[0],my_mat2_2[0],my_mat2_3[0],my_mat2_4[0],my_mat2_5[0],my_mat2_6[0],my_mat2_7[0],my_mat2_8[0]};
Transfinite Volume {my_mat2_1[1],my_mat2_2[1],my_mat2_3[1],my_mat2_4[1],my_mat2_5[1],my_mat2_6[1],my_mat2_7[1],my_mat2_8[1]};

my_mat3_1[] = Extrude {0.0 , 0.0 , z} {Surface{my_mat2_1[0]}; Layers{1};Recombine;};
my_mat3_2[] = Extrude {0.0 , 0.0 , z} {Surface{my_mat2_2[0]}; Layers{1};Recombine;};
my_mat3_3[] = Extrude {0.0 , 0.0 , z} {Surface{my_mat2_3[0]}; Layers{1};Recombine;};
my_mat3_4[] = Extrude {0.0 , 0.0 , z} {Surface{my_mat2_4[0]}; Layers{1};Recombine;};
my_mat3_5[] = Extrude {0.0 , 0.0 , z} {Surface{my_mat2_5[0]}; Layers{1};Recombine;};
my_mat3_6[] = Extrude {0.0 , 0.0 , z} {Surface{my_mat2_6[0]}; Layers{1};Recombine;};
my_mat3_7[] = Extrude {0.0 , 0.0 , z} {Surface{my_mat2_7[0]}; Layers{1};Recombine;};
my_mat3_8[] = Extrude {0.0 , 0.0 , z} {Surface{my_mat2_8[0]}; Layers{1};Recombine;};
Recombine  Surface {my_mat3_1[0],my_mat3_2[0],my_mat3_3[0],my_mat3_4[0],my_mat3_5[0],my_mat3_6[0],my_mat3_7[0],my_mat3_8[0]};
Transfinite Volume {my_mat3_1[1],my_mat3_2[1],my_mat3_3[1],my_mat3_4[1],my_mat3_5[1],my_mat3_6[1],my_mat3_7[1],my_mat3_8[1]};


//outside
Physical Volume(51) ={my_mat1_1[1],my_mat1_2[1],my_mat1_3[1],my_mat1_4[1],my_mat3_1[1],my_mat3_2[1],my_mat3_3[1],my_mat3_4[1],my_mat1_5[1],my_mat1_6[1],my_mat1_7[1],my_mat1_8[1],my_mat3_5[1],my_mat3_6[1],my_mat3_7[1],my_mat3_8[1]};
//inside
Physical Volume(52) ={my_mat2_1[1],my_mat2_2[1],my_mat2_3[1],my_mat2_4[1],my_mat2_5[1],my_mat2_6[1],my_mat2_7[1],my_mat2_8[1]};

// Physical objects to applied BC and material

//x=0
Physical Surface(101) = {41,217,393,129,305,481};
//x=L
Physical Surface(102) = {107,283,459,195,371,547};
//z=0
Physical Surface(121) = {1,2,3,4,sym[0],sym[1],sym[2],sym[3]};

Physical Point(21) ={1};
Physical Point(23) ={3};






