// creat a cube with fiber

//defination of unit
mm = 0.001;

//basic parameters
w=50.0*mm;
a=10.0*mm;
t=3.8*mm;

R=0.125*w;


// Characteristic length for mesh
Lc1=0.25*w;
Lc2=0.2*a;
Lc3=0.4*R;

// definition of points
Point(1) = { 0.0 , 0.0 , 0.0 , Lc1};
Point(2) = { 1.25*w , 0.0 , 0.0 , Lc1};
Point(3) = { 1.25*w , 0.6*w , 0.0 , Lc2/2};
Point(4) = { 1.25*w , 1.2*w , 0.0 , Lc1};
Point(5) = { 0.0 , 1.2*w , 0.0 , Lc1};
Point(6) = { 0.0 , 0.6*w+w/100.0 , 0.0 , Lc1};
Point(7) = { 0.25*w+a , 0.6*w+w/100.0 , 0.0 , Lc2/2};
Point(8) = { 0.25*w+a ,0.6*w-w/100.0  , 0.0 , Lc2/2};

Point(9) = { 0.0 , 0.6*w-w/100.0  , 0.0 , Lc1};

Point(10) = { 0.25*w , 0.275*w , 0.0 , Lc3};
Point(11) = { 0.25*w , (1.2-0.275)*w , 0.0 , Lc3};

Point(12) = { 0.25*w , 0.275*w-R , 0.0 , Lc3};
Point(13) = { 0.25*w+R , 0.275*w , 0.0 , Lc3};
Point(14) = { 0.25*w , 0.275*w+R , 0.0 , Lc3};
Point(15) = { 0.25*w-R , 0.275*w , 0.0 , Lc3};

Point(16) = { 0.25*w , (1.2-0.275)*w-R , 0.0 , Lc3};
Point(17) = { 0.25*w+R, (1.2-0.275)*w , 0.0 , Lc3};
Point(18) = { 0.25*w , (1.2-0.275)*w+R , 0.0 , Lc3};
Point(19) = { 0.25*w-R , (1.2-0.275)*w , 0.0 , Lc3};


//add new points for loading

Point(20) = { 0.25*w+R*Sin(45) , 0.275*w-R*Cos(45) , 0.0 , Lc3};
Point(21) = { 0.25*w-R*Sin(45) , 0.275*w-R*Cos(45) , 0.0 , Lc3};

Point(22) = { 0.25*w+R*Sin(45) , (1.2-0.275)*w+R*Cos(45) , 0.0 , Lc3};
Point(23) = { 0.25*w-R*Sin(45) , (1.2-0.275)*w+R*Cos(45) , 0.0 , Lc3};

Point(24)= { 0.25*w+a ,0.6*w, 0.0 , Lc2/2};

// Line between points
Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,5};
Line(5) = {5,6};
Line(6) = {6,7};
Line(7) = {7,24};
Line(8) = {8,9};
Line(9) = {9,1};



Circle(10) = {12,10,20};
Circle(11) = {20,10,13};
Circle(12) = {13,10,14};
Circle(13) = {14,10,15};
Circle(14) = {15,10,21};
Circle(15) = {21,10,12};



Circle(16) = {16,11,17};
Circle(17) = {17,11,22};
Circle(18) = {22,11,18};
Circle(19) = {18,11,23};
Circle(20) = {23,11,19};
Circle(21) = {19,11,16};

//add new line for loading
Line(22) = {10,15};
Line(23) = {21,10};
Line(24) = {10,20};
Line(25) = {13,10};
Line(26) = {11,17};
Line(27) = {22,11};
Line(28) = {11,23};
Line(29) = {19,11};

Line(30) = {24,8};

// Surface definition
Line Loop(1) = {1,2,3,4,5,6,7,30,8,9};
Line Loop(2) = {10,11,12,13,14,15};
Line Loop(3) = {16,17,18,19,20,21};


//new surface for loading
Line Loop(4) = {15,10,-24,-23};
Line Loop(5) = {11,25,24};
Line Loop(6) = {14,23,22};

Line Loop(7) = {18,19,-28,-27};
Line Loop(8) = {17,27,26};
Line Loop(9) = {20,29,28};

Plane Surface(1) = {1,2,3};

Plane Surface(2) = {4};
Plane Surface(3) = {7};
//Plane Surface(4) = {5};
//Plane Surface(5) = {6};
//Plane Surface(6) = {8};
//Plane Surface(7) = {9};


//VOlume

my_mat1_1[] = Extrude {0.0 , 0.0 , t/2.0} {Surface{1}; Layers{1};};

mat_Load_1[] = Extrude {0.0 , 0.0 , t/2.0} {Surface{2,3}; Layers{1};};
//mat_Load_2[] = Extrude {0.0 , 0.0 , t/2.0} {Surface{4,5,6,7}; Layers{1};};


//physical volume for material properties
Physical Volume(51) ={my_mat1_1[]};
Physical Volume(61) ={mat_Load_1[]};
//Physical Volume(62) ={mat_Load_2[]};


// Physical objects to applied BC and material

//fix Z
Physical Surface(301) = {1,2,3}; //,4,5,6,7};

//Loading Line 1
Physical Line(101) = {158};

//Loading Line 2
Physical Line(102) = {180};




/*
Transfinite Line {1,6,3,2,13} = 21;
Transfinite Line {12,11,10} = 21;
Transfinite Line {5,7,8,9,4} = 17 Using progression 0.95;

Transfinite Surface {1,2,3,4} ;

Recombine Surface {1,2,3,4} ;
*/





