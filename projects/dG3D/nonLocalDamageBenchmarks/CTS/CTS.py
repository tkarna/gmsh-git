#coding-Utf-8-*-
from gmshpy import *
#from dG3DpyDebug import*
from dG3Dpy import*

#script to launch composite problem with a python script

# material law
lawnonlocal1     = 1 # unique number of lawlawnonlocal1     = 1 # unique number of law
lawnonlocal4     = 4 # unique number of law
lawnonlocal5     = 5 # unique number of law
lawnonlocalcoh1  = 2 # unique number of law
lawnonlocalfrac1 = 3 # unique number of law
rho = 7850. # Bulk mass

Gc = 0.879e4
Dc = 0.99
sigmacCF = 176.7e7 # fracture limit in tension [Pa]
beta=1

# geometry
meshfile="CTS.msh" # name of mesh file
propertiesC1="prop_ep.i01" #properties file
propertiesC4="prop_el1.i01" #properties file
propertiesC5="prop_el2.i01" #properties file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 3 # StaticLinear=0 (default) StaticNonLinear=1 Multi=3
nstepImpl = 200 #500   # number of step (used only if soltype=1)
nstepExpl = 200 #1000
ftime =1.e-1   # Final time (used only if soltype=1)
tol=1.e-3   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=5000 # Number of step between 2 archiving (used only if soltype=1)
fulldg = 1   # 1 use dg inside a domain
dgnl = 1     # 1 use dg for non local epl inside a domain - only if fulldg
beta1 = 40   # stability parameter for dg
fsmin = 0.95
fsmax = 1.
eqRatio = 1.e6

#  compute solution and BC (given directly to the solver
# creation of law
law1     = NonLocalDamageDG3DMaterialLaw(lawnonlocal1,rho,propertiesC1)
law4     = NonLocalDamageDG3DMaterialLaw(lawnonlocal4,rho,propertiesC4)
law5     = NonLocalDamageDG3DMaterialLaw(lawnonlocal5,rho,propertiesC5)
lawcoh1  = NonLocalDamageLinearCohesive3DLaw(lawnonlocalcoh1,Gc,sigmacCF,Dc,beta,fsmin,fsmax)
lawfrac1 = FractureByCohesive3DLaw(lawnonlocalfrac1,lawnonlocal1,lawnonlocalcoh1)



# creation of ElasticField
nfield1 = 51 # number of the field (physical number of surface)
nfield2 = 61 # number of the field (physical number of surface)
nfield3 = 62 # number of the field (physical number of surface)

space1 = 0 # function space (Lagrange=0)

myfield1 = nonLocalDamageDG3DDomain(1000,nfield1,space1,lawnonlocalfrac1,fulldg,eqRatio)
myfield1.stabilityParameters(beta1)  #stabilty parameter on displacement
myfield1.nonLocalStabilityParameters(beta1,dgnl) #stabilty parameter on non local epl

myfield2 = nonLocalDamageDG3DDomain(1000,nfield2,space1,lawnonlocal4,0,eqRatio)
myfield2.stabilityParameters(beta1)  #stabilty parameter on displacement
myfield2.nonLocalStabilityParameters(beta1,0) #stabilty parameter on non local epl

#myfield3 = nonLocalDamageDG3DDomain(1000,nfield3,space1,lawnonlocal5,1,eqRatio)
#myfield3.stabilityParameters(beta1)  #stabilty parameter on displacement
#myfield3.nonLocalStabilityParameters(beta1,0) #stabilty parameter on non local epl

myinterfield = nonLocalInterDomainBetween3D(1000,myfield1,myfield2,eqRatio,1) #law = 0
myinterfield.stabilityParameters(beta1)
myinterfield.nonLocalStabilityParameters(beta1,dgnl)  #interface continous
#myinterfield1 = nonLocalInterDomainBetween3D(1000,myfield1,myfield3,eqRatio,1) #law = 0
#myinterfield1.stabilityParameters(beta1)
#myinterfield1.nonLocalStabilityParameters(beta1,dgnl)  #interface continous
#myinterfield2 = nonLocalInterDomainBetween3D(1000,myfield2,myfield3,eqRatio,1) #law = 0
#myinterfield2.stabilityParameters(beta1)
#myinterfield2.nonLocalStabilityParameters(beta1,dgnl)  #interface continous


#myfield1.matrixByPerturbation(1,1,1,1e-8)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addDomain(myfield2)
#mysolver.addDomain(myfield3)
mysolver.addDomain(myinterfield)
#mysolver.addDomain(myinterfield1)
#mysolver.addDomain(myinterfield2)
mysolver.addMaterialLaw(law1)
mysolver.addMaterialLaw(lawcoh1)
mysolver.addMaterialLaw(lawfrac1)
mysolver.addMaterialLaw(law4)
mysolver.addMaterialLaw(law5)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstepImpl,ftime,tol)
mysolver.snlManageTimeStep(500, 3, 2, 10)
mysolver.explicitSpectralRadius(ftime,0.5,0.)
mysolver.explicitTimeStepEvaluation(nstepExpl)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.addSystem(3,2)
mysolver.addSystem(1,1)
# BC
#tension along x

d1=0.15

mysolver.displacementBC("Edge",101,0,0.)
mysolver.displacementBC("Edge",101,1,-d1)
mysolver.displacementBC("Edge",102,0,0.)
mysolver.displacementBC("Edge",102,1,d1)
mysolver.displacementBC("Face",301,2,0.)


#mysolver.constraintBC("Face",112,1)

mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("eps_xx",IPField.STRAIN_XX, 1, 1)
mysolver.internalPointBuildView("eps_yy",IPField.STRAIN_YY, 1, 1)
mysolver.internalPointBuildView("eps_zz",IPField.STRAIN_ZZ, 1, 1)
mysolver.internalPointBuildView("eps_xy",IPField.STRAIN_XY, 1, 1)
mysolver.internalPointBuildView("eps_yz",IPField.STRAIN_YZ, 1, 1)
mysolver.internalPointBuildView("eps_xz",IPField.STRAIN_XZ, 1, 1)
mysolver.internalPointBuildView("damage",IPField.DAMAGE, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.archivingForceOnPhysicalGroup("Edge", 101, 1, nstepArch)
mysolver.archivingForceOnPhysicalGroup("Edge", 102, 1, nstepArch)

mysolver.archivingIPOnPhysicalGroup("Volume",nfield1, IPField.PLASTICSTRAIN,IPField.MIN_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield1, IPField.PLASTICSTRAIN,IPField.MAX_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield1, IPField.PLASTICSTRAIN,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield1, IPField.DAMAGE,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield1, IPField.DAMAGE,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield1, IPField.DAMAGE,IPField.MEAN_VALUE, nstepArch);

#mysolver.archivingIPOnPhysicalGroup("Volume",nfield2, IPField.PLASTICSTRAIN,IPField.MIN_VALUE,nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",nfield2, IPField.PLASTICSTRAIN,IPField.MAX_VALUE,nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",nfield2, IPField.PLASTICSTRAIN,IPField.MEAN_VALUE, nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",nfield2, IPField.DAMAGE,IPField.MIN_VALUE, nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",nfield2, IPField.DAMAGE,IPField.MAX_VALUE, nstepArch);
#mysolver.archivingIPOnPhysicalGroup("Volume",nfield2, IPField.DAMAGE,IPField.MEAN_VALUE, nstepArch);

#mysolver.archivingNodeIP(21, IPField.STRAIN_XX, IPField.MAX_VALUE, nstepArch);
#mysolver.archivingNodeIP(21, IPField.STRAIN_YY, IPField.MAX_VALUE, nstepArch);
#mysolver.archivingNodeIP(21, IPField.DAMAGE, IPField.MAX_VALUE, nstepArch);
#mysolver.archivingNodeIP(23, IPField.STRAIN_XX, IPField.MAX_VALUE, nstepArch);
#mysolver.archivingNodeIP(23, IPField.STRAIN_YY, IPField.MAX_VALUE, nstepArch);
#mysolver.archivingNodeIP(23, IPField.DAMAGE, IPField.MAX_VALUE, nstepArch);


#mysolver.archivingNodeDisplacement(21,0)
#mysolver.archivingNodeDisplacement(23,0)

mysolver.solve()
