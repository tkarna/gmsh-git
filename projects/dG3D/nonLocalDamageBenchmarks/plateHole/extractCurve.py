#-*-coding:Utf-8-*-

# script python to regroup crackpath files at the end of a MPI simulation
from os import system,getcwd,listdir

Lext = 0.05
Surf = 0.039*4*0.0011
print Surf
rate = 0.00075

fileD2 = open("IP21val10Max.csv",'r')
fileD3 = open("IP23val10Max.csv",'r')
fileF3 = open("force102comp0.csv",'r')
fileDE = open("IPVolume51val7Max.csv",'r')
fileDI = open("IPVolume52val7Max.csv",'r')

system("rm -f strainstress.csv")
outfile = open("strainstress.csv",'w')

while 1:
    tempo2 = fileD2.readline()[:-1]
    if(tempo2 == ''):
        break
    tempo3 = fileD3.readline()[:-1]
    tempoF3 = fileF3.readline()[:-1]
    tempoDE = fileDE.readline()[:-1]
    tempoDI = fileDI.readline()[:-1]
    lsttmp2 = tempo2.split(';')
    lsttmp3 = tempo3.split(';')
    lsttmpF3 = tempoF3.split(';')
    lsttmpDE = tempoDE.split(';')
    lsttmpDI = tempoDI.split(';')
    tim = (float(lsttmpF3[0]))
    eps = (float(lsttmpF3[0]))/Lext*rate
    hf  = (float(lsttmpF3[1]))
    sig = (float(lsttmpF3[1])/Surf*2)
    exx2= (float(lsttmp2[1]))
    exx3= (float(lsttmp3[1]))
    dE  = (float(lsttmpDE[1]))
    dI  = (float(lsttmpDI[1]))
    outfile.write("%e"%tim+";"+"%e"%hf+";"+"%e"%(eps*100)+";"+"%e"%(sig/1.e6)+";"+"%e"%dE+";"+"%e"%dI+";"+"%e"%(exx2)+";"+"%e"%(exx2*100)+";"+"%e"%exx3+";"+"%e\n"%(exx3*100))

fileD2.close()
fileD3.close()
fileF3.close()
fileDE.close()
fileDI.close()
outfile.close()

print "extract OK"
