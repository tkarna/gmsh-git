// creat a cube with fiber

//defination of unit
mm = 0.001;

lx=6*mm;

// characteristic Size for the volum
x = 8.*lx;
y = 6.*lx;    //ly/2;
z = 0.3*mm;

R=6.0*mm;

x1=R*x/Sqrt(x*x+y*y);
y1=R*y/Sqrt(x*x+y*y);

// Characteristic length
Lc1=z;

// definition of points
Point(1) = { 0.0 , 0.0 , 0.0 , Lc1};
Point(2) = {  x/2.0  , 0.0 , 0.0 , Lc1};
Point(3) = {  x  , 0.0 , 0.0 , Lc1};
Point(4) = { x , y/2.0 , 0.0 , Lc1};
Point(5) = {  x/2.0  , y/2.0 , 0.0 , Lc1};
Point(6) = {  0.0  , y/2.0 , 0.0 , Lc1};
Point(7) = {  x/2.0-x1  , y/2.0-y1 , 0.0 , Lc1};
Point(8) = {  x/2.0  , y/2.0-R , 0.0 , Lc1};
Point(9) = {  x/2.0+x1  , y/2.0-y1 , 0.0 , Lc1};
Point(10) = {  x/2.0+R  , y/2.0 , 0.0 , Lc1};
Point(11) = {  x/2.0-R  , y/2.0 , 0.0 , Lc1};

// Line between points
Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,10};
Line(5) = {11,6};
Line(6) = {6,1};
Line(7) = {1,7};
Line(8) = {2,8};
Line(9) = {3,9};

Circle(10) = {7,5,11};
Circle(11) = {8,5,7};
Circle(12) = {9,5,8};
Circle(13) = {10,5,9};

// Surface definition
Line Loop(1) = {6,7,10,5};
Line Loop(2) = {1,8,11,-7};
Line Loop(3) = {2,9,12,-8};
Line Loop(4) = {3,4,13,-9};

Plane Surface(1) = {1};
Plane Surface(2) = {2};
Plane Surface(3) = {3};
Plane Surface(4) = {4};



//VOlume

my_mat1[] = Extrude {0.0 , 0.0 , z} {Surface{1,2,3,4};};// Layers{1};Recombine;};
my_mat2[] = Extrude {0.0 , 0.0 , 2.0*z} {Surface{35,57,79,101};};// Layers{1};Recombine;};
my_mat1[] = Extrude {0.0 , 0.0 , z} {Surface{123,145,167,189};};// Layers{1};Recombine;};


// Physical objects to applied BC and material
Physical Surface(111) = {44,132,220,66,154,242};
Physical Surface(112) = {34,122,210,92,180,268};
Physical Surface(101) = {22,110,198};
Physical Surface(102) = {88,176,264};
Physical Surface(121) = {1,2,3,4};
Physical Surface(122) = {211,233,255,277};
//Physical Line(12) = {1};
//Physical Line(23) = {2};

//Physical Point(1) ={1};
//Physical Point(2) ={2};


Physical Volume(51) ={1,2,3,4,9,10,11,12};
Physical Volume(52) ={5,6,7,8};

// define transfinite mesh
Transfinite Line {1,6,3,37,125,213,2,59,147,235,8,81,169,257,13,83,15} = 37;
Transfinite Line {171,259,12,61,149,237,11,39,127,215,10,17,105,193,103,191} = 37;
Transfinite Line {5,18,106,194,7,16,104,192,214,8,38,126,236,148,60,9,4,82,170,258} = 35;

Transfinite Line {20,196,29,205,25,201,47,223,245,69,267,91,87,263,65,241,43,219,21,197} = 2;

Transfinite Line {108,117,113,135,157,179,175,153,131,109} = 3;

Transfinite Surface {1,2,3,4,35,57,79,101,123,145,167,189,211,233,255,277} ;
Transfinite Surface {44,132,220,66,154,242,34,122,210,92,180,268,22,110,198,88,176,264} ;
Transfinite Surface {206,118,30,52,140,228,74,162,250,96,184,272} ;
Transfinite Surface {70,158,246,48,136,224,26,114,202} ;

Recombine Surface {1,2,3,4,35,57,79,101,123,145,167,189,211,233,255,277} ;
Recombine Surface {44,132,220,66,154,242,34,122,210,92,180,268,22,110,198,88,176,264} ;
Recombine Surface {206,118,30,52,140,228,74,162,250,96,184,272} ;
Recombine Surface {70,158,246,48,136,224,26,114,202} ;

Transfinite Volume {1,2,3,4,5,6,7,8,9,10,11,12};





