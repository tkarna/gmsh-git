#coding-Utf-8-*-
from gmshpy import *
#from dG3DpyDebug import*
from dG3Dpy import*

#script to launch composite problem with a python script

# material law
lawnonlocal1 = 1 # unique number of law
lawnonlocal2 = 2 # unique number of law
lawnonlocal3 = 3 # unique number of law
lawnonlocal4 = 4 # unique number of law
lawnonlocal5 = 5 # unique number of law
rho = 7850. # Bulk mass


# geometry
meshfile="fullPlateHole.msh" # name of mesh file
propertiesC1="properties_an0.i01" #properties file
propertiesC2="properties_an90.i01" #properties file
propertiesC3="properties_anm45.i01" #properties file
propertiesC4="properties_an45.i01" #properties file
propertiesC5="properties_an90.i01" #properties file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2

#soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
#nstepImpl = 500   # number of step (used only if soltype=1)
#nstepArch=2 # Number of step between 2 archiving (used only if soltype=1)

soltype = 3 # StaticLinear=0 (default) StaticNonLinear=1 Multi=3
nstepImpl = 1500 #500   # number of step (used only if soltype=1)
nstepExpl = 1000 #1000
nstepArch= 3000 #1500 # Number of step between 2 archiving (used only if soltype=1)

ftime =1.e-3   # Final time (used only if soltype=1)
tol=1.e-2   # relative tolerance for NR scheme (used only if soltype=1)


fulldg = 0   # 1 use dg inside a domain
dgnl = 1     # 1 use dg for non local epl inside a domain - only if fulldg
beta1 = 40   # stability parameter for dg
dgnlinter = 0  # 1 use dg for non local epl between laminate
eqRatio = 1.e6

#  compute solution and BC (given directly to the solver
# creation of law
law1 = NonLocalDamageDG3DMaterialLaw(lawnonlocal1,rho,propertiesC1)
law2 = NonLocalDamageDG3DMaterialLaw(lawnonlocal2,rho,propertiesC2)
law3 = NonLocalDamageDG3DMaterialLaw(lawnonlocal3,rho,propertiesC3)
law4 = NonLocalDamageDG3DMaterialLaw(lawnonlocal4,rho,propertiesC4)
law5 = NonLocalDamageDG3DMaterialLaw(lawnonlocal5,rho,propertiesC5)


# creation of ElasticField
nfield1 = 51 # number of the field (physical number of surface)
nfield2 = 52 
nfield3 = 53
nfield4 = 54
nfield5 = 55

space1 = 0 # function space (Lagrange=0)

myfield1 = nonLocalDamageDG3DDomain(1000,nfield1,space1,lawnonlocal1,fulldg,eqRatio)
myfield1.stabilityParameters(beta1)  #stabilty parameter on displacement
myfield1.nonLocalStabilityParameters(beta1,dgnl) #stabilty parameter on non local epl
myfield2 = nonLocalDamageDG3DDomain(1000,nfield2,space1,lawnonlocal2,fulldg,eqRatio)
myfield2.stabilityParameters(beta1)  #stabilty parameter on displacement
myfield2.nonLocalStabilityParameters(beta1,dgnl) #stabilty parameter on non local epl
myfield3 = nonLocalDamageDG3DDomain(1000,nfield3,space1,lawnonlocal3,fulldg,eqRatio)
myfield3.stabilityParameters(beta1)  #stabilty parameter on displacement
myfield3.nonLocalStabilityParameters(beta1,dgnl) #stabilty parameter on non local epl
myfield4 = nonLocalDamageDG3DDomain(1000,nfield4,space1,lawnonlocal4,fulldg,eqRatio)
myfield4.stabilityParameters(beta1)  #stabilty parameter on displacement
myfield4.nonLocalStabilityParameters(beta1,dgnl) #stabilty parameter on non local epl
myfield5 = nonLocalDamageDG3DDomain(1000,nfield5,space1,lawnonlocal5,fulldg,eqRatio)
myfield5.stabilityParameters(beta1)  #stabilty parameter on displacement
myfield5.nonLocalStabilityParameters(beta1,dgnl) #stabilty parameter on non local epl


#in // we need to create the ininterdomain after adding the domains
myinterfield1 = nonLocalInterDomainBetween3D(1000,myfield1,myfield2,eqRatio,0) #law = 0
myinterfield1.stabilityParameters(beta1)
myinterfield1.nonLocalStabilityParameters(beta1,dgnlinter)  #interface not continous
myinterfield2 = nonLocalInterDomainBetween3D(1000,myfield2,myfield3,eqRatio,0) #law = 0
myinterfield2.stabilityParameters(beta1)
myinterfield2.nonLocalStabilityParameters(beta1,dgnlinter)  #interface not continous
myinterfield3 = nonLocalInterDomainBetween3D(1000,myfield3,myfield4,eqRatio,0) #law = 0
myinterfield3.stabilityParameters(beta1)
myinterfield3.nonLocalStabilityParameters(beta1,dgnlinter)  #interface not continous
myinterfield4 = nonLocalInterDomainBetween3D(1000,myfield4,myfield5,eqRatio,0) #law = 0
myinterfield4.stabilityParameters(beta1)
myinterfield4.nonLocalStabilityParameters(beta1,dgnlinter)  #interface not continous



#myfield1.matrixByPerturbation(1,1e-8)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addDomain(myfield2)
mysolver.addDomain(myfield3)
mysolver.addDomain(myfield4)
mysolver.addDomain(myfield5)
mysolver.addDomain(myinterfield1) # interface domain
mysolver.addDomain(myinterfield2)
mysolver.addDomain(myinterfield3)
mysolver.addDomain(myinterfield4)
mysolver.addMaterialLaw(law1)
mysolver.addMaterialLaw(law2)
mysolver.addMaterialLaw(law3)
mysolver.addMaterialLaw(law4)
mysolver.addMaterialLaw(law5)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
#mysolver.lineSearch(1)
mysolver.snlData(nstepImpl,ftime,tol)
mysolver.snlManageTimeStep(250, 3, 2, 10)
mysolver.stepBetweenArchiving(nstepArch)

mysolver.explicitSpectralRadius(ftime,0.3,0.)
mysolver.explicitTimeStepEvaluation(nstepExpl)
mysolver.addSystem(3,2)
mysolver.addSystem(1,1)

# BC
#tension along x

#d1=0.00075
d1=0.1

mysolver.displacementBC("Face",101,0,0.)
mysolver.displacementBC("Face",102,0,d1)
mysolver.displacementBC("Face",121,2,0.)
mysolver.displacementBC("Node",21,1,0.)
mysolver.displacementBC("Node",23,1,0.)


#mysolver.constraintBC("Face",112,1)

mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("eps_xx",IPField.STRAIN_XX, 1, 1)
mysolver.internalPointBuildView("eps_yy",IPField.STRAIN_YY, 1, 1)
mysolver.internalPointBuildView("eps_zz",IPField.STRAIN_ZZ, 1, 1)
mysolver.internalPointBuildView("eps_xy",IPField.STRAIN_XY, 1, 1)
mysolver.internalPointBuildView("eps_yz",IPField.STRAIN_YZ, 1, 1)
mysolver.internalPointBuildView("eps_xz",IPField.STRAIN_XZ, 1, 1)
mysolver.internalPointBuildView("damage",IPField.DAMAGE, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 101, 0, nstepArch)
mysolver.archivingForceOnPhysicalGroup("Face", 102, 0, nstepArch)

mysolver.archivingIPOnPhysicalGroup("Volume",nfield1, IPField.PLASTICSTRAIN,IPField.MIN_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield1, IPField.PLASTICSTRAIN,IPField.MAX_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield1, IPField.PLASTICSTRAIN,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield1, IPField.DAMAGE,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield1, IPField.DAMAGE,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield1, IPField.DAMAGE,IPField.MEAN_VALUE, nstepArch);

mysolver.archivingIPOnPhysicalGroup("Volume",nfield2, IPField.PLASTICSTRAIN,IPField.MIN_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield2, IPField.PLASTICSTRAIN,IPField.MAX_VALUE,nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield2, IPField.PLASTICSTRAIN,IPField.MEAN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield2, IPField.DAMAGE,IPField.MIN_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield2, IPField.DAMAGE,IPField.MAX_VALUE, nstepArch);
mysolver.archivingIPOnPhysicalGroup("Volume",nfield2, IPField.DAMAGE,IPField.MEAN_VALUE, nstepArch);

mysolver.archivingNodeIP(21, IPField.STRAIN_XX, IPField.MAX_VALUE, nstepArch);
mysolver.archivingNodeIP(21, IPField.STRAIN_YY, IPField.MAX_VALUE, nstepArch);
mysolver.archivingNodeIP(21, IPField.DAMAGE, IPField.MAX_VALUE, nstepArch);
mysolver.archivingNodeIP(23, IPField.STRAIN_XX, IPField.MAX_VALUE, nstepArch);
mysolver.archivingNodeIP(23, IPField.STRAIN_YY, IPField.MAX_VALUE, nstepArch);
mysolver.archivingNodeIP(23, IPField.DAMAGE, IPField.MAX_VALUE, nstepArch);


#mysolver.archivingNodeDisplacement(21,0)
#mysolver.archivingNodeDisplacement(23,0)

mysolver.solve()

