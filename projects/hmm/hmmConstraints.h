// C++ Interface: hmmConstraints
//
// Description: Classes used for boundary conditions (only Dirichlet boundary conditions 
// have been implemented so far but possible extensions will include Neumann BCs).  
//
// Copyright: See COPYING file that comes with this distribution
//
// bugs and problems to the public mailing list <gmsh@geuz.org>.
//

#ifndef _HMMCONSTRAINTS_H_
#define _HMMCONSTRAINTS_H_

#include <stdio.h>
#include <iostream>
#include <vector>
#include "simpleFunction.h"
#include "hmmDomain.h"

class hmmConstraintsBase 
{
protected:
  std::string _tagCons;
  hmmGroupOfDomains *_dCons;
public:
  hmmConstraintsBase(std::string tagCons) {
    _dCons   = NULL;
    _tagCons = tagCons;
  }
  hmmConstraintsBase(std::string tagCons, hmmGroupOfDomains &dCons) {
    _dCons   = &dCons;
    _tagCons = tagCons;
  }
  virtual ~hmmConstraintsBase() {}
  inline std::string getTag() const{ return _tagCons;}
  inline hmmGroupOfDomains *getAllDomains() const { return _dCons;}
  virtual void addDomain(hmmDomainBase &domain) = 0;
};

class hmmDirichlet : public hmmConstraintsBase 
{
protected:
  int _comp;
  simpleFunction<double> *_imposedValue;
  double _value;
public:
  hmmDirichlet(std::string tagCons, double imposedValue, int comp) : hmmConstraintsBase(tagCons) {
    _value = imposedValue;
    _comp = comp; 
    _imposedValue        = new simpleFunction<double>(imposedValue);
  }

  hmmDirichlet(hmmGroupOfDomains &dCons, std::string tagCons, int comp, double imposedValue) : hmmConstraintsBase(tagCons, dCons) {
    _value = imposedValue;
    _comp = comp; 
    _imposedValue = new simpleFunction<double>(imposedValue);
  }
  virtual ~hmmDirichlet() {}
  inline void addDomain(hmmDomainBase &domain) { _dCons->addDomain(domain);}
  virtual inline double getInitialValue() const { return _value;}
  virtual inline simpleFunction<double> *getSimpleFunction() const { return _imposedValue;}
  virtual inline void setSimpleFunction(double imposedValue) { 
    simpleFunction<double> *newSF = new simpleFunction<double>(imposedValue);
    _imposedValue = newSF;
  }
  virtual inline void setNewDirichletBC(double multFactor) {
    double newValue = multFactor * getInitialValue();
    this->setSimpleFunction(newValue);
  }
};

#endif //_HMMCONSTRAINTS_H_
