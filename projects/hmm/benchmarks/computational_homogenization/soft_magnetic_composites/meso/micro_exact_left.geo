
//min     = 5;
//cal1    = Ceil(ratio * ppp_x);
//cal2    = ppp_x-cal1;
//n1_1 = (cal1<min)?min:cal1;
//n1_2 = (cal2<min)?min:cal2;

// Define left part of the domain
//===============================

// 2nd case - (x_gauss - 0.5 * lx, y_gauss - 0.5 * ly) is at the begining of Omega1
//=================================================================================

If( (d == 0 ))

P_BL_1_x = P_BL_x;
P_BL_1_y = P_BL_y;
P_BL_1 = newp; Point(P_BL_1) =  { P_BL_1_x , P_BL_1_y, 0.0};

P_BL_2_x = P_BL_x;
P_BL_2_y = P_BL_y + eps_yy;
P_BL_2 = newp; Point(P_BL_2) =  { P_BL_2_x , P_BL_2_y, 0.0};

P_BL_3_x = P_BL_x;
P_BL_3_y = P_BL_y + eps_y;
P_BL_3 = newp; Point(P_BL_3) =  { P_BL_3_x , P_BL_3_y, 0.0};

P_BR_1_x = P_BR_x;
P_BR_1_y = P_BR_y;
P_BR_1 = newp; Point(P_BR_1) =  { P_BR_1_x , P_BR_1_y, 0.0};


P_BR_2_x = P_BR_x;
P_BR_2_y = P_BR_y + eps_yy;
P_BR_2 = newp; Point(P_BR_2) =  { P_BR_2_x , P_BR_2_y, 0.0};

P_BR_3_x = P_BR_x;
P_BR_3_y = P_BR_y + eps_y;
P_BR_3 = newp; Point(P_BR_3) =  { P_BR_3_x , P_BR_3_y, 0.0};

vec_P_BL_1[] += P_BL_1;
vec_P_BR_1[] += P_BR_1;
vec_P_BL_2[] += P_BL_2;
vec_P_BR_2[] += P_BR_2;
vec_P_BL_3[] += P_BL_3;
vec_P_BR_3[] += P_BR_3;

// Creating lines
//===============
l1_B = newl ; Line(l1_B)  = {P_BL_3,P_BR_3};
l2_B = newl ; Line(l2_B)  = {P_BR_3,P_BR_2};
l3_B = newl ; Line(l3_B)  = {P_BR_2,P_BL_2};
l4_B = newl ; Line(l4_B)  = {P_BL_2,P_BL_3};
l5_B = newl ; Line(l5_B)  = {P_BL_2,P_BL_1};
l6_B = newl ; Line(l6_B)  = {P_BL_1,P_BR_1};
l7_B = newl ; Line(l7_B)  = {P_BR_1,P_BR_2};
//====================
vec_l1_B[]  +={l1_B} ;
vec_l2_B[]  +={l2_B} ;
vec_l3_B[]  +={l3_B} ;
vec_l4_B[]  +={l4_B} ;
vec_l5_B[]  +={l5_B} ;
vec_l6_B[]  +={l6_B} ;
vec_l7_B[]  +={l7_B} ;
//====================

/////////////////////////////////////
bas_vec[]    += {l6_B};
gauche_vec[] += {l4_B,l5_B};
droite_vec[] += {l2_B,l7_B};
////////////////////////////////////

ll10 = newll; Line Loop(ll10)         = {l1_B,l2_B,l3_B,l4_B} ;
s11  = news ; Plane Surface(s11)      = {-ll10} ;
ll12 = newll; Line Loop(ll12)         = {-l3_B,-l7_B,-l6_B,-l5_B} ;
s13  = news ; Plane Surface(s13)      = {-ll12} ;
Transfinite Line{l1_B,l3_B,l6_B}      = n2+1;
Transfinite Line{l2_B,l4_B}           = n1_1+1;
Transfinite Line{l5_B,l7_B}           = n1_2+1;
Transfinite Surface{s11,s13}; Recombine Surface{s11,s13} ;
inno2[] += {s11};
inno1[] += {s13};

EndIf 

// 2nd case - (x_gauss - 0.5 * lx, y_gauss - 0.5 * ly) in Omega1
//==============================================================

If( (d < eps_yy) && (d > 0) ) 

P_BL_1_x = P_BL_x;
P_BL_1_y = P_BL_y;
P_BL_1 = newp; Point(P_BL_1) =  { P_BL_1_x , P_BL_1_y, 0.0};

P_BL_2_x = P_BL_x;
P_BL_2_y = P_BL_y + (eps_yy-d);
P_BL_2 = newp; Point(P_BL_2) =  { P_BL_2_x , P_BL_2_y, 0.0};

P_BL_3_x = P_BL_x;
P_BL_3_y = P_BL_y + (eps_y-d);
P_BL_3 = newp; Point(P_BL_3) =  { P_BL_3_x , P_BL_3_y, 0.0};

P_BR_1_x = P_BR_x;
P_BR_1_y = P_BR_y;
P_BR_1 = newp; Point(P_BR_1) =  { P_BR_1_x , P_BR_1_y, 0.0};


P_BR_2_x = P_BR_x;
P_BR_2_y = P_BR_y + (eps_yy-d);
P_BR_2 = newp; Point(P_BR_2) =  { P_BR_2_x , P_BR_2_y, 0.0};

P_BR_3_x = P_BR_x;
P_BR_3_y = P_BR_y + (eps_y-d);
P_BR_3 = newp; Point(P_BR_3) =  { P_BR_3_x , P_BR_3_y, 0.0};

vec_P_BL_1[] += P_BL_1;
vec_P_BR_1[] += P_BR_1;
vec_P_BL_2[] += P_BL_2;
vec_P_BR_2[] += P_BR_2;
vec_P_BL_3[] += P_BL_3;
vec_P_BR_3[] += P_BR_3;

// Creating lines
//===============
l1_B = newl ; Line(l1_B)  = {P_BL_3,P_BR_3};
l2_B = newl ; Line(l2_B)  = {P_BR_3,P_BR_2};
l3_B = newl ; Line(l3_B)  = {P_BR_2,P_BL_2};
l4_B = newl ; Line(l4_B)  = {P_BL_2,P_BL_3};
l5_B = newl ; Line(l5_B)  = {P_BL_2,P_BL_1};
l6_B = newl ; Line(l6_B)  = {P_BL_1,P_BR_1};
l7_B = newl ; Line(l7_B)  = {P_BR_1,P_BR_2};
//====================
vec_l1_B[]  +={l1_B} ;
vec_l2_B[]  +={l2_B} ;
vec_l3_B[]  +={l3_B} ;
vec_l4_B[]  +={l4_B} ;
vec_l5_B[]  +={l5_B} ;
vec_l6_B[]  +={l6_B} ;
vec_l7_B[]  +={l7_B} ;
//====================

/////////////////////////////////////
bas_vec[]    += {l6_B};
gauche_vec[] += {l4_B,l5_B};
droite_vec[] += {l2_B,l7_B};
////////////////////////////////////

ll10 = newll; Line Loop(ll10)         = {l1_B,l2_B,l3_B,l4_B} ;
s11  = news ; Plane Surface(s11)      = {-ll10} ;
ll12 = newll; Line Loop(ll12)         = {-l3_B,-l7_B,-l6_B,-l5_B} ;
s13  = news ; Plane Surface(s13)      = {-ll12} ;
Transfinite Line{l1_B,l3_B,l6_B}      = n2+1;
Transfinite Line{l2_B,l4_B} = n1_1+1;
//n1_2 = Floor(n1_2 * (eps_yy - d) / eps_yy);
Transfinite Line{l5_B,l7_B} = n1_2+1;
Transfinite Surface{s11,s13}; Recombine Surface{s11,s13} ;
inno2[] += {s11};
inno1[] += {s13};

EndIf 

// 3trd case - (x_gauss - 0.5 * lx, y_gauss - 0.5 * ly) at the boundary between Omega1 andOmega2
//==============================================================================================

If(d == eps_yy)

P_BL_2_x = P_BL_x;
P_BL_2_y = P_BL_y;
P_BL_2 = newp; Point(P_BL_2) =  { P_BL_2_x , P_BL_2_y, 0.0};

P_BL_3_x = P_BL_x;
P_BL_3_y = P_BL_y + (eps_y-d);
P_BL_3 = newp; Point(P_BL_3) =  { P_BL_3_x , P_BL_3_y, 0.0};

P_BR_2_x = P_BR_x;
P_BR_2_y = P_BR_y;
P_BR_2 = newp; Point(P_BR_2) =  { P_BR_2_x , P_BR_2_y, 0.0};

P_BR_3_x = P_BR_x;
P_BR_3_y = P_BR_y + (eps_y-d);
P_BR_3 = newp; Point(P_BR_3) =  { P_BR_3_x , P_BR_3_y, 0.0};

vec_P_BL_2[] += P_BL_2;
vec_P_BR_2[] += P_BR_2;
vec_P_BL_3[] += P_BL_3;
vec_P_BR_3[] += P_BR_3;
// Creating lines
//===============
l1_B = newl ; Line(l1_B)  = {P_BL_3,P_BR_3};
l2_B = newl ; Line(l2_B)  = {P_BR_3,P_BR_2};
l3_B = newl ; Line(l3_B)  = {P_BR_2,P_BL_2};
l4_B = newl ; Line(l4_B)  = {P_BL_2,P_BL_3};
//====================
vec_l1_B[]  +={l1_B} ;
vec_l2_B[]  +={l2_B} ;
vec_l3_B[]  +={l3_B} ;
vec_l4_B[]  +={l4_B} ;
//====================

/////////////////////////////////////
bas_vec[]    += {l3_B};
gauche_vec[] += {l4_B};
droite_vec[] += {l2_B};
////////////////////////////////////

ll10 = newll; Line Loop(ll10)         = {l1_B,l2_B,l3_B,l4_B} ;
s11  = news ; Plane Surface(s11)      = {-ll10} ;
Transfinite Line{l1_B,l3_B}           = n2+1;
Transfinite Line{l2_B,l4_B}           = n1_1+1;
Transfinite Surface{s11}; Recombine Surface{s11} ;
inno2[] += {s11};

EndIf 

// 4th case - (x_gauss - 0.5 * lx, y_gauss - 0.5 * ly) is a point of Omega2
//=========================================================================

If( (d > eps_yy) && !(d == eps_y)  )

P_BL_2_x = P_BL_x;
P_BL_2_y = P_BL_y;
P_BL_2 = newp; Point(P_BL_2) =  { P_BL_2_x , P_BL_2_y, 0.0};

P_BL_3_x = P_BL_x;
P_BL_3_y = P_BL_y + (eps_y-d);
P_BL_3 = newp; Point(P_BL_3) =  { P_BL_3_x , P_BL_3_y, 0.0};

P_BR_2_x = P_BR_x;
P_BR_2_y = P_BR_y;
P_BR_2 = newp; Point(P_BR_2) =  { P_BR_2_x , P_BR_2_y, 0.0};

P_BR_3_x = P_BR_x;
P_BR_3_y = P_BR_y + (eps_y-d);
P_BR_3 = newp; Point(P_BR_3) =  { P_BR_3_x , P_BR_3_y, 0.0};

vec_P_BL_2[] += P_BL_2;
vec_P_BR_2[] += P_BR_2;
vec_P_BL_3[] += P_BL_3;
vec_P_BR_3[] += P_BR_3;

// Creating lines
//===============
l1_B = newl ; Line(l1_B)  = {P_BL_3,P_BR_3};
l2_B = newl ; Line(l2_B)  = {P_BR_3,P_BR_2};
l3_B = newl ; Line(l3_B)  = {P_BR_2,P_BL_2};
l4_B = newl ; Line(l4_B)  = {P_BL_2,P_BL_3};
//====================
vec_l1_B[]  +={l1_B} ;
vec_l2_B[]  +={l2_B} ;
vec_l3_B[]  +={l3_B} ;
vec_l4_B[]  +={l4_B} ;
//====================

/////////////////////////////////////
bas_vec[]    += {l3_B};
gauche_vec[] += {l4_B};
droite_vec[] += {l2_B};
////////////////////////////////////

ll10 = newll; Line Loop(ll10)         = {l1_B,l2_B,l3_B,l4_B} ;
s11  = news ; Plane Surface(s11)      = {-ll10} ;
Transfinite Line{l1_B,l3_B}           = n2+1;
//n1_1 = Floor(n1_1 * (eps_y - d)/(eps_y - eps_yy));
Transfinite Line{l2_B,l4_B}           = n1_1+1;
Transfinite Surface{s11}; Recombine Surface{s11} ;
inno2[] += {s11};

EndIf

