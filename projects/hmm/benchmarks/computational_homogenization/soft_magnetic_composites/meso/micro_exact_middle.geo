
//min     = 5;
//cal1    = Ceil(ratio * ppp_x);
//cal2    = ppp_x-cal1;
//n1_1 = (cal1<min)?min:cal1;
//n1_2 = (cal2<min)?min:cal2;

If(nop_y >= 2)

For i In{0:nop_y-2}

P_L_2_x = P_BL_x;
P_L_2_y = P_BL_3_y + (i) * eps_y + (eps_yy);
P_L_2 = newp; Point(P_L_2) =  { P_L_2_x , P_L_2_y, 0.0};

P_L_3_x = P_BL_x;
P_L_3_y = P_BL_3_y + (i + 1) * eps_y;
P_L_3 = newp; Point(P_L_3) =  { P_L_3_x , P_L_3_y, 0.0};

P_R_2_x = P_BR_x;
P_R_2_y = P_BR_3_y + (i) * eps_y + (eps_yy);
P_R_2 = newp; Point(P_R_2) =  { P_R_2_x , P_R_2_y, 0.0};

P_R_3_x = P_BR_x;
P_R_3_y = P_BR_3_y + (i + 1) * eps_y;
P_R_3 = newp; Point(P_R_3) =  { P_R_3_x , P_R_3_y, 0.0};

vec_P_L_2[] += P_L_2;
vec_P_R_2[] += P_R_2;
vec_P_L_3[] += P_L_3;
vec_P_R_3[] += P_R_3;

// Creating lines
//===============

If(i == 0)
l1 = newl ; Line(l1)  = {vec_P_BR_3[0],vec_P_R_2[i]}  ;
l3 = newl ; Line(l3)  = {vec_P_L_2[i]  ,vec_P_BL_3[0]};
EndIf

If(i > 0)
l1 = newl ; Line(l1)  = {vec_P_R_3[i-1],vec_P_R_2[i]}  ;
l3 = newl ; Line(l3)  = {vec_P_L_2[i]  ,vec_P_L_3[i-1]};
EndIf

l2 = newl ; Line(l2)  = {vec_P_R_2[i]  ,vec_P_L_2[i]}  ;
l4 = newl ; Line(l4)  = {vec_P_L_2[i]  ,vec_P_L_3[i]}  ;
l5 = newl ; Line(l5)  = {vec_P_L_3[i]  ,vec_P_R_3[i] } ;
l6 = newl ; Line(l6)  = {vec_P_R_3[i]  ,vec_P_R_2[i]   };

//================
vec_l1[]  +={l1} ;
vec_l2[]  +={l2} ;
vec_l3[]  +={l3} ;
vec_l4[]  +={l4} ;
vec_l5[]  +={l5} ;
vec_l6[]  +={l6} ;
//================

/////////////////////////////////////
gauche_vec[] += {l3,l4};
droite_vec[] += {l1,l6};
////////////////////////////////////

If(i == 0)
ll10 = newll; Line Loop(ll10)         = {-vec_l1_B[i],-l1,-l2,-l3} ;
s11  = news ; Plane Surface(s11)      = {-ll10} ;
EndIf

If(i > 0)
ll10 = newll; Line Loop(ll10)         = {-vec_l5[i-1],-l1,-l2,-l3} ;
s11  = news ; Plane Surface(s11)      = {-ll10} ;
EndIf

ll12 = newll; Line Loop(ll12)         = {l2,l4,l5,l6} ;
s13  = news ; Plane Surface(s13)      = {-ll12} ;

Transfinite Line{l2,l5}      = n2+1;
Transfinite Line{l1,l3}      = n1_2+1;
Transfinite Line{l4,l6}      = n1_1+1;
Transfinite Surface{s11,s13}; Recombine Surface{s11,s13} ;

If(d == 0)
haut_vec[] += {l5};
EndIf

inno1[] += {s11};
inno2[] += {s13};

EndFor

EndIf
