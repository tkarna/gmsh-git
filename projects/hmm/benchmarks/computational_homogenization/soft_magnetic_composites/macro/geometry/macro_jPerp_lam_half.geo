Include "../../../reference_solutions/reference_solution_lam/a-v_formulation/macro_jPerp_lam_half.dat";

Geometry.OldNewReg = 1; // Use old newreg definition for geometrical transformations (compatibility option for old Gmsh geometries)

Mesh.Algorithm = 1 ; // 2D mesh algorithm (1=MeshAdapt, 2=Automatic, 5=Delaunay, 6=Frontal, 7=bamg, 8=delquad)
Mesh.CharacteristicLengthFactor = 1 ;

Flag_Transfinite = 1 ;

lclam            = 6 * L_X/40 ;
lclam2           = L_X/40  ; //finer
lcair            = R1/10 ; // inner circle
lcair2           = R2/10 ; // outer circle

nwl              = 16 ;//number of divisions along thickness of stack (with possible Bump)
//nll              = 10 * 3*Ceil[Lsrc/lclam] +1 ;
//nllm             = 3*Ceil[(L_X-2*Lsrc)/lclam] +1 ;

nll              = 6 ;
nllm             = 6 ;

fprog = 0.85 ;
fbump2 = 0.5 ; // near middle points

//============================================================
// Lamination stack center at (0,0)
// Corner of stack at (-L_X/2, L_Y/2, 0)
//============================================================

x0 = -L_X/2 ;
y0 = 0 ;
z0 = 0 ;

pp[] += newp; Point(pp[0]) = { x0,      y0,   z0, lclam};
pp[] += newp; Point(pp[1]) = { x0+Lsrc, y0,   z0, lclam2};
pp[] += newp; Point(pp[2]) = {-x0-Lsrc, y0,   z0, lclam2};
pp[] += newp; Point(pp[3]) = {-x0,      y0,   z0, lclam};

pp[] += newp; Point(pp[4]) = {-x0,      y0+L_Y/2, z0, lclam};
pp[] += newp; Point(pp[5]) = {-x0-Lsrc, y0+L_Y/2, z0, lclam2};
pp[] += newp; Point(pp[6]) = { x0+Lsrc, y0+L_Y/2, z0, lclam2};
pp[] += newp; Point(pp[7]) = { x0,      y0+L_Y/2, z0, lclam};


For k In {0:7}
  llam[] += newl ; Line(newl) = {pp[k],pp[((k==7)?0:k+1)]};
EndFor

lllam[] += newll ; Line Loop(newll) = {llam[]};
slam[]+=news ; Plane Surface(news) = lllam[0];

If(Flag_Transfinite)
Transfinite Line{llam[{3}], -llam[{7}]}   = nwl Using Progression fprog ;
  Transfinite Line{llam[{0}],-llam[{2}]}  = nll Using Bump fbump2 ;
  Transfinite Line{llam[{1}],llam[{5}]}   = nllm Using Bump fbump2 ;
  Transfinite Line{llam[{4}], -llam[{6}]} = nll Using Bump fbump2 ;
Transfinite Surface{slam[]}=pp[{0,3,4,7}]; Recombine Surface{slam[]};
EndIf

// ===================================================================
// Adding an inductor and air around
// airgap between inductor and laminations...
pgap[] += newp; Point(newp) = { x0+Lsrc, L_Y/2+dist_ind_lam, z0, lclam2};
pgap[] += newp; Point(newp) = {-x0-Lsrc, L_Y/2+dist_ind_lam, z0, lclam2};

// inductor
lcind = wind/4 ;
pi[] += newp; Point(newp) = { x0+Lsrc, L_Y/2+dist_ind_lam+wind, z0, lcind};
pi[] += newp; Point(newp) = {-x0-Lsrc, L_Y/2+dist_ind_lam+wind, z0, lcind};

//up
lind_[] += newl ; Line(newl) = {pgap[0],pgap[1]};
lind_[] += newl ; Line(newl) = {pgap[1],pi[1]};
lind_[] += newl ; Line(newl) = {pi[1],pi[0]};
lind_[] += newl ; Line(newl) = {pi[0],pgap[0]};

llind[] += newll ;  Line Loop(newll) = {lind_[]};
sind[]+=news; Plane Surface(news) = llind[];

Transfinite Line{lind_[{0}], lind_[{2}]} = 10;
Transfinite Line{lind_[{1}], lind_[{3}]} = 8;
Transfinite Surface{sind[]} = {pi[{0}],pi[{1}],pgap[{1}],pgap[{0}]}; Recombine Surface{sind[]};



// Air around everything
cen = newp ; Point(cen) = { 0,  0, z0, lcair};

pa[] += newp; Point(pa[0]) = { R1,  0, z0, lcair};
pa[] += newp; Point(pa[1]) = { 0,  R1, z0, lcair};
pa[] += newp; Point(pa[2]) = {-R1, 0, z0, lcair};

For k In {0:1}
 lair[] += newl ; Circle(newl) = {pa[k],cen,pa[k+1]};
EndFor

laxis[] += newl ; Line(newl) = {pp[3],pa[0]};
laxis[] += newl ; Line(newl) = {pp[0],pa[2]};

Line Loop(newll) = {19, 17, 18, -20, -8, -7, -6, -5, -4};
sair = news ; Plane Surface(sair) = {llind[0], newll-1};


// air infinity
pa_[] += newp; Point(pa_[0]) = { R2,  0, z0, lcair2};
pa_[] += newp; Point(pa_[1]) = { 0,  R2, z0, lcair2};
pa_[] += newp; Point(pa_[2]) = {-R2, 0, z0, lcair2};

For k In {0:1}
 lair2[] += newl ; Circle(newl) = {pa_[k],cen,pa_[k+1]};
EndFor

laxis[] += newl ; Line(newl) = {pa[0],pa_[0]};
laxis[] += newl ; Line(newl) = {pa[2],pa_[2]};

llair2=newll; Line Loop(newll) = {25, 23, 24, -26, -18, -17};
sair2 = news ; Plane Surface(sair2) = {llair2};

//================================================================================
// Physical regions
//================================================================================
GAMMA_INF = 10;
CONDUCTOR = 11;
AIR       = 12;
INDUCTOR1 = 13;
INDUCTOR2 = 14;


Physical Line(GAMMA_INF)       = {lair2[], llam[{0,1,2}],laxis[] };
//Physical Line(SYMMETRY)     = {llam[{0,1,2}],laxis[] };
Physical Surface(CONDUCTOR)   = {slam[]} ;
Physical Surface(AIR)         = {sair, sair2} ;
//Physical Surface(AIR_INF)   = {sair2} ;
Physical Surface(INDUCTOR1)   = {sind[]} ;
Physical Surface(INDUCTOR2)   = {} ;
