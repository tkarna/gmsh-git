from gmshpy import *
from hmmpy  import *
import hmmpy 
import math
import sys
import os
import time

dir(hmmpy)

print'---- Lamination stack problem'
geoFile  = '../macro/geometry/square2d_.geo'
meshFile = '../macro/geometry/square2d_.msh'
macromodel = GModel()
macromodel.load(geoFile)
macromodel.mesh(2)
macromodel.save(meshFile)

# Physical regions
#=================
GAMMA_INF = 10
CONDUCTOR = 11
AIR       = 12
INDUCTOR1 = 13
INDUCTOR2 = 14
INDUCTOR_R = 15;
INDUCTOR_L = 16;

# Solver input data
#==================
mm          = 1e-3
micron      = 1e-6
d           = 0.5 * mm
d_c         = 45  * micron # thickness of the conductor.
d_i         = 2.5 * micron # thickness of the isolation.
e           = d_c + 2 * d_i # size of the periodic square. 
num         = 10 # number of cell over the length.
_LX         = num * e
FF          = 0.9
_lx         = e
_ly         = _lx
_nx         = 5
_ny         = _nx
problemType = 0
maxNumIter  = 4
tol         = 5e-5 
gmres       = 0
nonlinear   = 0
hysteresis  = 1
mesoSolver  = hysteresis 
#mesoSolver  = nonlinear 

# Constructor
#============
hmmprob = hmmProblemBase()
hmmprob.setLX(_lx)
hmmprob.setLY(_ly)
hmmprob.setNX(_ny)
hmmprob.setNY(_ny)
hmmprob.setProblemType(problemType)

# 1. Set some parameters
#=======================
hmmprob.setLinsysAndDofManager()
hmmprob.setModel(macromodel)
hmmprob.setMaxNumIter(maxNumIter)
hmmprob.setNumIter(0)
hmmprob.setTolerance(tol)
hmmprob.setGmresValue(gmres)

# 2. hmmGroupOfDomains
#=====================
tagGroup_GammaINF = "Gamma_Inf"
hmmprob.addGroupOfDomains(tagGroup_GammaINF)
hmmprob.addDomainToGroupOfDomains(tagGroup_GammaINF, 1, GAMMA_INF)
print'Group_GammaINF is tagged', tagGroup_GammaINF

tagGroup_Air = "Air"
hmmprob.addGroupOfDomains(tagGroup_Air)
hmmprob.addDomainToGroupOfDomains(tagGroup_Air, 2, AIR)
print'Group_Air is tagged', tagGroup_Air

tagGroup_Indu1 = "Inductor1"
hmmprob.addGroupOfDomains(tagGroup_Indu1)
hmmprob.addDomainToGroupOfDomains(tagGroup_Indu1, 2, INDUCTOR1)
print'Group_Dom is tagged', tagGroup_Indu1

tagGroup_Indu2 = "Inductor2"
hmmprob.addGroupOfDomains(tagGroup_Indu2)
hmmprob.addDomainToGroupOfDomains(tagGroup_Indu2, 2, INDUCTOR2)
print'Group_Dom is tagged', tagGroup_Indu2

tagGroup_InduR = "Inductor_R"
hmmprob.addGroupOfDomains(tagGroup_InduR)
hmmprob.addDomainToGroupOfDomains(tagGroup_InduR, 2, INDUCTOR_R)
print'Group_Dom is tagged', tagGroup_InduR

tagGroup_InduL = "Inductor_L"
hmmprob.addGroupOfDomains(tagGroup_InduL)
hmmprob.addDomainToGroupOfDomains(tagGroup_InduL, 2, INDUCTOR_L)
print'Group_Dom is tagged', tagGroup_InduL

tagGroup_Cond = "Conductor"
hmmprob.addGroupOfDomains(tagGroup_Cond)
hmmprob.addDomainToGroupOfDomains(tagGroup_Cond, 2, CONDUCTOR)
print'Group_Dom is tagged', tagGroup_Cond

tagGroup_All = "All"
hmmprob.addGroupOfDomains(tagGroup_All)
hmmprob.addDomainToGroupOfDomains(tagGroup_All, 2, AIR)
hmmprob.addDomainToGroupOfDomains(tagGroup_All, 2, INDUCTOR1)
hmmprob.addDomainToGroupOfDomains(tagGroup_All, 2, INDUCTOR2)
hmmprob.addDomainToGroupOfDomains(tagGroup_All, 2, INDUCTOR_R)
hmmprob.addDomainToGroupOfDomains(tagGroup_All, 2, INDUCTOR_L)
hmmprob.addDomainToGroupOfDomains(tagGroup_All, 2, CONDUCTOR)
print'Group_All is tagged', tagGroup_All
hmmprob.getDomainsInfo()


# 4. hmmConstraints
#==================
imposedValue_Corner = 0.0
tagDiriInf = "diriInf"
hmmprob.addDirichletBC(tagGroup_GammaINF, tagDiriInf, 1, imposedValue_Corner)
print'The boundary condition at corner is tagged ', tagDiriInf
hmmprob.getConstraintsInfo()

# 5. Quadratures
#===============
tagQuad = "Grad"
hmmprob.addQuad(tagQuad)
print'The added quadrature rule is tagged ', tagQuad
hmmprob.getQuadInfo()

# 6. hmmFunctionSpace
#====================
tagSpace = "BF_Perp"
iField = 0
funcSpacType = Form1P
basisFunctionType = BF_PerpendicularEdge
hmmprob.addFunctionSpace(tagSpace, tagGroup_All, iField, funcSpacType, basisFunctionType)
hmmprob.addConstraintToFunctionSpace(tagSpace, tagDiriInf)
print'The added function space is tagged ', tagSpace
hmmprob.getFunctionSpaceInfo()

# 3. hmmFunction
#===============
GMSHVerbosity  = "0 "
GMSHGeoFileName = "../meso/micro_InsuCondInsu.geo "
GetDPVerbosity = "0 "
if mesoSolver == nonlinear :
    GetDPProFileName = "../meso/micro_vecA_dyn_nlin.pro "
    GetDPExactProFileName = "../meso/micro_vecA_dyn_nlin.pro "
else : # if hysteresis
    GetDPProFileName = "../meso/micro_vecA_dyn_hys.pro "
    GetDPExactProFileName = "../meso/micro_vecA_dyn_hys.pro "
GetDPResolutionName = "a_NR"
GetDPPostPro = "mean_1 mean_2 mean_3 cuts_field_1 "
GMSHExactGeoFileName = "micro_exact.geo "
microProblemFileName = ""

pi = 3.14159
e1 = 0.5/0.52
e2 = 1.0 - e1
nu_Insu = 1e0 * 1.0/(4.0 * pi * 1.0e-7) 
nu_Iron = 1e-3 * nu_Insu
nu_para = nu_Iron * e1 + nu_Insu * e2
nu_perp = nu_Iron * nu_Insu / (e1 * nu_Insu + e2 * nu_Iron)

#====================================================================
initTime = 0.0
#====================================================================

tagNLML_hb = "_hb_NLMatLaw"
tagLML_hb = "_hb_MatLaw"

hmmprob.add2ScaleNonLinearMaterialLawInno(tagNLML_hb, tagGroup_Cond, tagSpace, tagQuad, GMSHVerbosity, GMSHGeoFileName, GetDPVerbosity, GetDPProFileName, GetDPResolutionName, GetDPPostPro, GMSHExactGeoFileName, GetDPExactProFileName, microProblemFileName)
hmmprob.Init2ScaleNonLinearMLInno(tagNLML_hb, tagGroup_Cond, nu_para, 0.0, 0.0, 0.0, nu_para, 0.0, 0.0, 0.0, nu_para)
hmmprob.Init2ScaleNonLinearMap(tagNLML_hb, tagGroup_Cond, 0.0)
hmmprob.Init2ScaleNonLinearHLMap(tagQuad, tagNLML_hb, tagGroup_Cond, initTime, 0.0)

tagElecEnergy            = "JouleLosses"
tagMagEnergy             = "MagEnergy"
tagTotalEnergy           = "TotalEnergy"
tagTotalEnergyDifference = "TotalEnergyDifference"
HL                       = 0.0

hmmprob.Init2ScaleNonLinearGQMap(tagQuad, tagNLML_hb, tagGroup_Cond)

hmmprob.addGQTo2ScaleNonLinearGQMap(tagElecEnergy, tagNLML_hb)
hmmprob.InitGQMap(tagElecEnergy, tagNLML_hb, initTime, HL)

hmmprob.addGQTo2ScaleNonLinearGQMap(tagMagEnergy, tagNLML_hb)
hmmprob.InitGQMap(tagMagEnergy, tagNLML_hb, initTime, HL)

hmmprob.addGQTo2ScaleNonLinearGQMap(tagTotalEnergy, tagNLML_hb)
hmmprob.InitGQMap(tagTotalEnergy, tagNLML_hb, initTime, HL)

hmmprob.addGQTo2ScaleNonLinearGQMap(tagTotalEnergyDifference, tagNLML_hb)
hmmprob.InitGQMap(tagTotalEnergyDifference, tagNLML_hb, initTime, HL)


print "The added material law is tagged ", tagNLML_hb
hmmprob.add1ScaleLinearMaterialLawInno(tagLML_hb, tagGroup_Air, nu_Insu, 0.0, 0.0, 0.0, nu_Insu, 0.0, 0.0, 0.0, nu_Insu)
print "The added material law is tagged ", tagLML_hb
hmmprob.add1ScaleLinearMaterialLawInno(tagLML_hb, tagGroup_Indu1, nu_Insu, 0.0, 0.0, 0.0, nu_Insu, 0.0, 0.0, 0.0, nu_Insu)
print "The added material law is tagged ", tagLML_hb
hmmprob.add1ScaleLinearMaterialLawInno(tagLML_hb, tagGroup_Indu2, nu_Insu, 0.0, 0.0, 0.0, nu_Insu, 0.0, 0.0, 0.0, nu_Insu)
print "The added material law is tagged ", tagLML_hb
hmmprob.add1ScaleLinearMaterialLawInno(tagLML_hb, tagGroup_InduR, nu_Insu, 0.0, 0.0, 0.0, nu_Insu, 0.0, 0.0, 0.0, nu_Insu)
print "The added material law is tagged ", tagLML_hb
hmmprob.add1ScaleLinearMaterialLawInno(tagLML_hb, tagGroup_InduL, nu_Insu, 0.0, 0.0, 0.0, nu_Insu, 0.0, 0.0, 0.0, nu_Insu)
print "The added material law is tagged ", tagLML_hb

tagLML_sigma = "_sigma_MatLaw"
sigma = 5e6
sigma1 = sigma 
sigma2 = sigma
sigma_para = sigma1 * e1 + sigma2 * e2
sigma_perp = sigma1 * sigma2 / (e1 * sigma2 + e2 * sigma1)
hmmprob.add1ScaleLinearMaterialLawInno(tagLML_sigma, tagGroup_Cond, sigma_para, 0.0, 0.0, 0.0, sigma_perp, 0.0, 0.0, 0.0, 0.0)
print "The added material law is tagged ", tagLML_sigma
hmmprob.getFunctionInfo()

# 7. hmmFormulation
#==================
tagForm1 = "a_Hyst_ML"
hmmprob.addFormulation(tagForm1)
hmmprob.addBilinearTermToFormulation(tagForm1, tagSpace, tagLML_hb   , tagGroup_Indu1, GBF, tagQuad)
hmmprob.addBilinearTermToFormulation(tagForm1, tagSpace, tagLML_hb   , tagGroup_Indu2, GBF, tagQuad)
hmmprob.addBilinearTermToFormulation(tagForm1, tagSpace, tagLML_hb   , tagGroup_InduR, GBF, tagQuad)
hmmprob.addBilinearTermToFormulation(tagForm1, tagSpace, tagLML_hb   , tagGroup_InduL, GBF, tagQuad)
hmmprob.addBilinearTermToFormulation(tagForm1, tagSpace, tagLML_hb   , tagGroup_Air  , GBF, tagQuad)
hmmprob.addBilinearTermToFormulation(tagForm1, tagSpace, tagNLML_hb  , tagGroup_Cond , GBF, tagQuad)
hmmprob.addBilinearTermToFormulation(tagForm1, tagSpace, tagLML_sigma, tagGroup_Cond , SBF, tagQuad)

divFactor = 1.
if mesoSolver == nonlinear :
    sourceCurrentValue     = (4.e9)/divFactor
    sourceCurrentValue_RL  = (4.e9)/divFactor
elif mesoSolver == hysteresis :
    sourceCurrentValue     = (3.5e9)/divFactor
    sourceCurrentValue_RL  = (3.5e9)/divFactor
else :
    sourceCurrentValue     = (3.5e9)/divFactor
    sourceCurrentValue_RL  = (3.5e9)/divFactor

tagSource_Ind1 = 1
tagSource_Ind2 = 2
tagSource_IndR = 3
tagSource_IndL = 4
hmmprob.addSourceTermToFormulation(tagForm1, tagSpace, tagGroup_Indu1, tagQuad, 0.0, 0.0,  sourceCurrentValue, tagSource_Ind1)
hmmprob.addSourceTermToFormulation(tagForm1, tagSpace, tagGroup_Indu2, tagQuad, 0.0, 0.0, -sourceCurrentValue, tagSource_Ind2)
hmmprob.addSourceTermToFormulation(tagForm1, tagSpace, tagGroup_InduR, tagQuad, 0.0, 0.0,  sourceCurrentValue_RL, tagSource_IndR)
hmmprob.addSourceTermToFormulation(tagForm1, tagSpace, tagGroup_InduL, tagQuad, 0.0, 0.0,  sourceCurrentValue_RL, tagSource_IndL)

print'The added formulation is tagged ', tagForm1
hmmprob.getFormulationInfo()

# 8. hmmResolution
#=================
x = time.clock()
if mesoSolver == nonlinear :
    n = 100
    freq = 50000
elif mesoSolver == hysteresis :
    n = 200
    freq = 2500
else :
    n = 200
    freq = 2500

withNoHamonics = false
if withNoHarmonics :
    period = 1.0/freq
    nbRT = 2.0
    endTime = nbRT * period
    dTime = (period)/n
    theta = 1.0
else :
    Harmonics = 5.;
    period    = 1/(Harmonics *freq) ;
    nbRT      = 2. ;
    endTime   = nbrT * (period * Harmonics) ;
    dTime     = period/nbrstep ;
    theta     = 1.0;

print 'initTime', initTime
print 'endTime', endTime
print 'dTime', dTime
print 'theta', theta
tagRes1 = "a_Hyst_Res"
tagPostPro1 = "a_Hyst_Post"
hmmprob.addSolver(tagRes1, timeDomSolver)
hmmprob.addPostPro(tagPostPro1)

#=============================================================================================
hmmprob.addPoint2MapOfPoints(tagNLML_hb, tagPostPro1, 699994, 0.50 * e, 0.5 * e , 0.0)
hmmprob.addPoint2MapOfPoints(tagNLML_hb, tagPostPro1, 699995, 0.50 * e, 3.5 * e , 0.0)
hmmprob.addPoint2MapOfPoints(tagNLML_hb, tagPostPro1, 699996, 0.50 * e, 6.5 * e , 0.0)
hmmprob.addPoint2MapOfPoints(tagNLML_hb, tagPostPro1, 699997, 0.50 * e, 9.5 * e , 0.0)

hmmprob.addPoint2MapOfPoints(tagNLML_hb, tagPostPro1, 799994, 3.5 * e, 0.5 * e , 0.0)
hmmprob.addPoint2MapOfPoints(tagNLML_hb, tagPostPro1, 799995, 3.5 * e, 3.5 * e , 0.0)
hmmprob.addPoint2MapOfPoints(tagNLML_hb, tagPostPro1, 799996, 3.5 * e, 6.5 * e , 0.0)
hmmprob.addPoint2MapOfPoints(tagNLML_hb, tagPostPro1, 799997, 3.5 * e, 9.5 * e , 0.0)

hmmprob.addPoint2MapOfPoints(tagNLML_hb, tagPostPro1, 899994, 6.5 * e, 0.5 * e , 0.0)
hmmprob.addPoint2MapOfPoints(tagNLML_hb, tagPostPro1, 899995, 6.5 * e, 3.5 * e , 0.0)
hmmprob.addPoint2MapOfPoints(tagNLML_hb, tagPostPro1, 899996, 6.5 * e, 6.5 * e , 0.0)
hmmprob.addPoint2MapOfPoints(tagNLML_hb, tagPostPro1, 899997, 6.5 * e, 9.5 * e , 0.0)

hmmprob.addPoint2MapOfPoints(tagNLML_hb, tagPostPro1, 999994, 9.5 * e, 0.5 * e , 0.0)
hmmprob.addPoint2MapOfPoints(tagNLML_hb, tagPostPro1, 999995, 9.5 * e, 3.5 * e , 0.0)
hmmprob.addPoint2MapOfPoints(tagNLML_hb, tagPostPro1, 999996, 9.5 * e, 6.5 * e , 0.0)
hmmprob.addPoint2MapOfPoints(tagNLML_hb, tagPostPro1, 999997, 9.5 * e, 9.5 * e , 0.0)
#=============================================================================================

hmmprob.addParameters(tagRes1, initTime, endTime, dTime, theta, freq)
hmmprob.addOp2Solver(tagRes1, tagForm1, InitSolLin, hmmprob.getPostPro(tagPostPro1) )
hmmprob.addOp2Solver(tagRes1, tagForm1, Sol,    hmmprob.getPostPro(tagPostPro1) )
hmmprob.addOp2Solver(tagRes1, tagForm1, SolJac, hmmprob.getPostPro(tagPostPro1) )

nameOfQtyJouleLosses           = "JouleLosses_Last"
nameOfQtyMagEnergy             = "MagEnergy_Last"
nameOfQtyTotalEnergy           = "TotalEnergy_Last"
nameOfQtyTotalEnergyDifference = "TotalEnergyDifference_Last"
hmmprob.computeGQOverTime(tagElecEnergy           , tagNLML_hb, nameOfQtyJouleLosses          )
hmmprob.computeGQOverTime(tagMagEnergy            , tagNLML_hb, nameOfQtyMagEnergy            )
hmmprob.computeGQOverTime(tagTotalEnergy          , tagNLML_hb, nameOfQtyTotalEnergy          )
hmmprob.computeGQOverTime(tagTotalEnergyDifference, tagNLML_hb, nameOfQtyTotalEnergyDifference)

# Computing Hysteresis losses
if mesoSolver == hysteresis :
    cycleNum = 1
    hmmprob.computeTotalHysteresisLossesOverCycle(tagNLML_hb, (period-dTime), dTime, freq, cycleNum)

print'|CPU|',time.clock() - x
