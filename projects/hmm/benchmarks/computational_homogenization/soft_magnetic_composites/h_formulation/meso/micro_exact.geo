Include "micro_exact.dat";

//Defining the F_Period function
//==============================

min     = 5;
cal1    = Ceil(ratio * ppp_x);
cal2    = ppp_x-cal1;
n1_1 = (cal1<min)?min:cal1;
n1_2 = (cal2<min)?min:cal2;

P_BL_x = x_gauss-0.5*lx;
P_BL_y = y_gauss-0.5*ly;
P_BR_x = x_gauss+0.5*lx;
P_BR_y = y_gauss-0.5*ly;
d = Fmod[P_BL_y,eps_y] + (P_BL_y < 0 ? eps_y : 0);

Printf("d before is equal to = %.16g ", d);
Printf("eps_y is equal to = %.16g", eps_y);

If( Fabs(d - eps_y) <= 0.0000000000000001)
d = 0.0;
EndIf

Printf("d after is equal to = %.16g", d);
Include "micro_exact_left.geo";
Include "micro_exact_middle.geo";
Include "micro_exact_right.geo";
Physical Line(GAMMA_LEFT) = {gauche_vec[]} ;
Physical Line(GAMMA_RIGHT) = {droite_vec[]} ;
Physical Line(GAMMA_DOWN)    = {bas_vec[]} ;
Physical Line(GAMMA_UP)   = {haut_vec[]} ;
Physical Surface(IRON) = {inno1[]} ;
Physical Surface(AIR) = {inno2[]} ;

