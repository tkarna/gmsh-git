Include "meso.dat";
Include Sprintf("../output/mesoproblems/computational/gaussPointPosition_%g.dat", thisThreadNum);
//Include "../output/mesoproblems/computational/gaussPointPosition.dat";

//Include Sprintf("../output/mesoproblems/computational/gaussPointPosition_%g.dat", thisNum);
//Include "../output/mesoproblems/computational/gaussPointPosition.dat"; 

Group {
  GammaCornerFix = Region[ {GAMMA_MIDDLE_POINT } ];
  GammaCornerUL = Region[ {GAMMA_CORNER3 } ];

  GammaLeft      = Region[ {GAMMA_LEFT } ];
  GammaRight     = Region[ {GAMMA_RIGHT} ];
  GammaUp        = Region[ {GAMMA_UP} ];
  GammaDown      = Region[ {GAMMA_DOWN} ];
  GammaDirichlet = Region[ {GAMMA_UP, GAMMA_DOWN} ];
  GammaDirichlet_Yindi = Region[ {GAMMA_LEFT, GAMMA_RIGHT} ];

  Omega_NL       = Region[ {IRON} ] ;
  Omega_L        = Region[ {AIR} ] ;
  Omega_C        = Region[ {Omega_NL, Omega_L} ] ;
  Omega_CC       = Region[ {} ] ;
  Omega          = Region[ {Omega_NL, Omega_L} ] ;
}

Function {
  XYZ_Gauss[] = Vector[x_gauss, y_gauss, 0.0];
  mu0         = 4.e-7 * Pi ;
  nu0         = 1/mu0 ;
  sigmaIron   = 5e6;
  rho_Iron    = 1./sigmaIron;
  rho_Other   = 10./sigmaIron;

  //Gyselink paper: Incorporation of a Jiles-Atherton vector hysteresis model in 2D FE magnetic field computations
  //================================================================================================================
  Freq = 2500; 
  T= 1/Freq ; 
  nbrT = 1 ; 
  nbrstep = 20 ;
  t0 = 0 ; 
  tmax = nbrT*T ; 
  dt = T/nbrstep ; 
  dtt= 1.0 * dt;
  theta = 1.0;


  //eM[]   = -(1.0/dt) * Vector[0.0, 0.0, _dtFunc];// this is "e_M = -\partial_t aM = -(aM-aM_prev)(dt)"
  //dt_bM[]   = (1.0/dt) * Vector[_dtGradFunc_2, -_dtGradFunc_1, 0.0];

  hM[]        = Vector[0.0, 0.0, _func];
  curl_hM[]   = Vector[_gradFunc_2, -_gradFunc_1, 0.0];

  //aM_prev[]        = Vector[0.0, 0.0, _func_prev];
  //grad_aM_prev[]   = Vector[_gradFunc_prev_2, -_gradFunc_prev_1, 0.0];


  NbrMaxIter = 50; 
  Eps = 1e-4; 
  Relax = 1.0;
  factor = 1.0;
  //Nbr_SubProblems = 1;
  Nbr_SubProblems = 4;
  epsilon = 1e-4;
  Pert~{1}[] = Vector[0, 0, 0];
  Pert~{2}[] = epsilon * Vector[1.0, 0.0, 0.0];
  Pert~{3}[] = epsilon * Vector[0.0, 1.0, 0.0];
  Pert~{4}[] = epsilon * Vector[0.0, 0.0, 1.0];

  ti = t0 + currentTimeStep * dt ;
  tf = ti + dt;
  Printf("Inno =============> step = %g ti = %g lx = %g exactMicroGeo = %g", currentTimeStep, ti, lx, exactMicroGeo);

  //a_macro[]     = Vector[ 0., 0., ScalarField[XYZ[]]{1}] ; // not used
  //a_tprevious[] = (currentTimeStep==0) ? Vector[0.,0.,0.] : Vector[ 0., 0., ScalarField[XYZ[]]{0}] ;

  h_macro[]     = Vector[ 0., 0., ScalarField[XYZ[]]{1}] ; // not used
  h_tprevious[] = (currentTimeStep==0) ? Vector[0.,0.,0.] : Vector[ 0., 0., ScalarField[XYZ[]]{0}] ;

  //Parameters of the Brauer nonlinear constitutive law
  
  gamma = 388.; alpha = 0.3774; beta = 2.97;
  nu_a[] = gamma + alpha * Exp[beta*$1] ;
  dnudb2_a[] = alpha * beta* Exp[beta*$1] ;

  Flag_NL = 0;	

  nu_sat = gamma + alpha * Exp[beta * 1.29 * 1.29] ;
  mur_sat = 1/(mu0*nu_sat) ;   mur = mur_sat ;
  Printf("nu with saturation %g mur %g", gamma + alpha * Exp[beta * 1.29 * 1.29], mur_sat) ;


  rho[Omega_NL]       = rho_Iron;
  rho[Omega_L]        = rho_Other;

  mu_Mag              = 2500 * mu0; 

  If(!Flag_NL)
    mu[Omega_L]       = TensorDiag[mu0, mu0, mu0];//mu0;
    mu[Omega_NL]      = TensorDiag[mu_Mag, mu_Mag, mu_Mag]; //mu0;
    dbdh_NL[Omega]    = TensorDiag[0., 0., 0.] ;
  EndIf
    If(Flag_NL)// 
    mu[Omega_L]       = 1. * mu0;
    mu[Omega_NL]      = 1./nu_a[SquNorm[$1]] ;
    dbdh_NL[Omega_NL] = 2*dnudb2_a[SquNorm[$1]] * SquDyadicProduct[$1] ;
  EndIf
}

Constraint {
 { Name h_Micro ;
   Case {
     { Region GammaRight; Type Link; RegionRef GammaLeft;
       Coefficient 1.; Function Vector[$X-lx, $Y, $Z]; }
     { Region GammaUp; Type Link; RegionRef GammaDown;
       Coefficient 1.; Function Vector[$X, $Y-ly, $Z]; }
     { Region GammaCornerFix; Type Assign; Value 0.0; }//??????????????????
   }
 }
 { Name Current_2D ;
   Case {
     { Region Omega_C; Type Assign; Value 0.0; } // forcing net current in each lamination to be equal to zero
   }
 }
 { Name h_Micro_Init ;
   Case {
     { Type InitFromResolution ; Region Omega ; NameOfResolution h_Init ; }
   }
 }
}

Jacobian {
  { Name Vol ; Case { { Region All ; Jacobian Vol ; } } }
  { Name Sur ; Case { { Region All ; Jacobian Sur ; } } }
}

Integration {
  { Name II ; Case { 
      {Type Gauss ; 
        Case {
	  { GeoElement Line        ; NumberOfPoints  4 ; }
          { GeoElement Triangle    ; NumberOfPoints  4 ; }
          { GeoElement Quadrangle  ; NumberOfPoints  4 ; } 
        } 
      } 
    }
  }
}

FunctionSpace{
  For iP In {1:Nbr_SubProblems}
  { Name HCurl_a_Proj~{iP} ; Type Form1P ;
    BasisFunction{
      { Name sn; NameOfCoef hn; Function BF_PerpendicularEdge; Support Omega; Entity NodesOf[ All ]; }
    }
  }
  { Name HCurl_a_Micro~{iP} ; Type Form1P ;
    BasisFunction{
      { Name sn; NameOfCoef hn; Function BF_PerpendicularEdge; Support Omega; Entity NodesOf[ All ]; }
    }
    Constraint {
      { NameOfCoef hn; EntityType NodesOf; NameOfConstraint h_Micro; }
      { NameOfCoef hn; EntityType NodesOf; NameOfConstraint h_Micro_Init; }
    }
  }

//   // Gradient of Electric scalar potential (2D)
//   //===========================================
//   { Name Hregion_u_2D~{iP} ; Type Form1P ;
//     BasisFunction {
//       { Name sr ; NameOfCoef ur ; Function BF_RegionZ ;
//         Support Omega_C ; Entity Omega_C ; }
//     }
//     GlobalQuantity {
//       { Name U ; Type AliasOf        ; NameOfCoef ur ; }
//       { Name I ; Type AssociatedWith ; NameOfCoef ur ; }
//     }
//     Constraint {
//       { NameOfCoef U ;
//         EntityType Region ; NameOfConstraint Voltage_2D ; }
//       { NameOfCoef I ;
//         EntityType Region ; NameOfConstraint Current_2D ; }
//     }
//   }

  { Name HCurl_a_Micro_t0~{iP} ; Type Form1P ;
    BasisFunction{
      { Name sn; NameOfCoef hn; Function BF_PerpendicularEdge; Support Omega; Entity NodesOf[ All ]; }
    }
  }
  EndFor
}

Formulation {
  For iP In {1:Nbr_SubProblems}
  { Name Init_PreviousTimeStep~{iP} ; Type FemEquation ;
    Quantity{
      { Name h  ; Type Local  ; NameOfSpace HCurl_a_Micro~{iP} ; }
    }
    Equation{
      Galerkin { [     Dof{h} , {h} ]   ; In Omega; Jacobian Vol; Integration II; }
      Galerkin { [ -h_tprevious[], {h} ]; In Omega; Jacobian Vol; Integration II; }
    }
  }

  { Name h_NR~{iP} ; Type FemEquation ;
    Quantity {

      { Name h  ; Type Local ; NameOfSpace HCurl_a_Micro~{iP} ; }
      //{ Name ur ; Type Local  ; NameOfSpace Hregion_u_2D~{iP} ; }
      //{ Name I  ; Type Global ; NameOfSpace Hregion_u_2D~{iP}[I] ; }
      //{ Name U  ; Type Global ; NameOfSpace Hregion_u_2D~{iP}[U] ; }

    }
    Equation {

      Galerkin { DtDof[ mu[] * Dof{h} , {h} ] ; 
        In Omega; Jacobian Vol; Integration II; }
      Galerkin { [ - mu[] * ( factor * (XYZ[]-XYZ_Gauss[]) /\ curl_hM[] ) , {h} ]; 
        In Omega; Jacobian Vol; Integration II; }
      Galerkin { [ mu[] * hM[] , {h} ]; 
        In Omega; Jacobian Vol; Integration II; }
      Galerkin { [ mu[] * Pert~{iP}[] , {h} ]; 
        In Omega; Jacobian Vol; Integration II; }

      Galerkin { [ rho[] * Dof{d h} , {d h} ] ;   
        In Omega_C; Jacobian Vol; Integration II; }
      Galerkin { [ rho[] * curl_hM[] , {d h} ] ;   
        In Omega_C; Jacobian Vol; Integration II; }
      //Galerkin { [ rho[] * Pert~{iP}[] , {d h} ];   
      //  In Omega_C; Jacobian Vol; Integration II; }

      //Galerkin { DtDof[ sigma[] * Dof{a} , {a} ] ; 
      //  In Omega_C; Jacobian Vol; Integration II; }
      //Galerkin { [ - sigma[] * ( factor * (XYZ[]-XYZ_Gauss[]) /\ dt_bM[] ) , {a} ]; 
      //  In Omega_C; Jacobian Vol; Integration II; }
      //Galerkin { [   sigma[] * Dof{ur}      , {a} ]  ; In Omega_C; Jacobian Vol; Integration II; }
      
      //Galerkin { DtDof [ sigma[] * Dof{a} , {ur} ]; In Omega_C ; Jacobian Vol; Integration II ; }
      //Galerkin { [ sigma[] * Dof{ur}      , {ur} ]; In Omega_C ; Jacobian Vol ; Integration II ; }    
      //GlobalTerm { [ Dof{I}               , {U}  ]; In Omega_C ; } 

      //Galerkin { [ nu[ {d a}+curl_aM[]+Pert~{iP}[] ] * Dof{d a} , {d a} ]   ;   In Omega; Jacobian Vol; Integration II; }
      //Galerkin { [ nu[ {d a}+curl_aM[]+Pert~{iP}[] ] * curl_aM[], {d a} ]      ;   In Omega; Jacobian Vol; Integration II; }
      //Galerkin { [ nu[ {d a}+curl_aM[]+Pert~{iP}[] ] * Pert~{iP}[] , {d a} ];   In Omega; Jacobian Vol; Integration II; }
      //Galerkin { JacNL[ dhdb_NL[{d a}+curl_aM[]+Pert~{iP}[] ] * Dof{d a}, {d a} ]; In Omega_NL; Jacobian Vol; Integration II; }
    }
  }
  EndFor
}

Resolution {
  { Name h_Init ;
    System {
      For iP In {1:Nbr_SubProblems}
      { Name AH~{iP}   ; NameOfFormulation Init_PreviousTimeStep~{iP}; DestinationSystem Micro~{iP}; }
      EndFor
    }
    Operation {
       For iP In {1:Nbr_SubProblems}
       If(currentTimeStep == 0)
         Generate[AH~{iP}]; Solve[AH~{iP}]; TransferSolution[AH~{iP}];
       EndIf
         If(currentTimeStep != 0)
           If(exactMicroGeo == 0 )
             GmshRead[Sprintf("../output/mesoproblems/computational/h_pert_p1_cs%g_gp%g.pos", currentTimeStep-1, numEle)];
           EndIf
           If( (exactMicroGeo != 0) && (iP == 1) )
             GmshRead[Sprintf("../output/mesoproblems/exact/h_pert_p1_cs%g_gp%g.pos", currentTimeStep-1, numEle)];
           EndIf
         Generate[AH~{iP}]; Solve[AH~{iP}]; TransferSolution[AH~{iP}];
       EndIf
     EndFor
    }
  }
  { Name h_NR ;
    System {
      For iP In {1:Nbr_SubProblems}
      { Name Micro~{iP}; NameOfFormulation h_NR~{iP} ;}
      EndFor
    }
    Operation {
      SetTime[ti];
      For iP In {1:Nbr_SubProblems}
      InitSolution[Micro~{iP} ];
      TimeLoopTheta[ ti, tf, dtt, 1.]{
        IterativeLoop[NbrMaxIter, Eps, Relax]{
          GenerateJac[Micro~{iP}] ; SolveJac[Micro~{iP}] ;
        }
      }
      SaveSolution[Micro~{iP}];
      If(iP == 1)
        PostOperation[map_field_1];
      EndIf
      If(exactMicroGeo && (iP == 1) )
        PostOperation[cuts_field_1];
      EndIf
      EndFor
    }
  }
}

PostProcessing {
  For iP In {1:Nbr_SubProblems}
  { Name h_Micro_NR~{iP} ; NameOfFormulation h_NR~{iP} ; NameOfSystem Micro~{iP} ;
     PostQuantity {
       { Name vol         ; Value { Integral { [ 1. ] ;        In Omega ; Jacobian Vol ; Integration II ; } } }  // stored in register #12
       { Name h_pert      ; Value { Term { [ CompZ[ {h} ] ] ;  In Omega ; Jacobian Vol; } } }
       { Name h_proj      ; Value { 
           Term { [ CompZ[ ( hM[] ) ] ] ;                      In Omega ; Jacobian Vol;  }
           Term { [ CompZ[ ( - factor * (XYZ[] - XYZ_Gauss[]) /\ curl_hM[] ) ] ] ;  In Omega ; Jacobian Vol;  } } }
       { Name h_tot       ; Value { 
           Term { [ CompZ[ {h}] ] ;   In Omega ; Jacobian Vol; }
           Term { [ CompZ[ hM[] ] ] ; In Omega ; Jacobian Vol; }
           Term { [ CompZ[ ( - factor * (XYZ[] - XYZ_Gauss[]) /\ curl_hM[] ) ] ] ; In Omega ; Jacobian Vol; } } }
       { Name h_tot_mean ; Value {// stored in #21
           Integral { [ ({d h} + curl_hM[] + Pert~{iP}[])/#12 ] ; In Omega ; Jacobian Vol; Integration II ; } } }
       { Name j_pert      ; Value { Term { [ {d h} ] ; In Omega_C ; Jacobian Vol;  } } }
       { Name j_proj      ; Value { Term { [ curl_hM[] ] ; In Omega_C ; Jacobian Vol;  } } }
       { Name j_tot       ; Value { 
           Term { [ {d h}]    ; In Omega_C ; Jacobian Vol ; } 
           Term { [ curl_hM[]  ] ; In Omega_C ; Jacobian Vol ; }
           Term { [ Pert~{iP}[]] ; In Omega_C ; Jacobian Vol ; } } }
       { Name j_tot_mean  ; Value { Integral { [ ({d h} + curl_hM[] + Pert~{iP}[])/#12 ] ; In Omega_C ; Jacobian Vol ; Integration II ; } } }
       { Name b_tot ; Value {
           Term { [ mu[{d h} + curl_hM[] + Pert~{iP}[]] * ({d h} + curl_hM[] + Pert~{iP}[]) ]; In Omega; Jacobian Vol;} } }
       { Name b_tot_mean ; Value {// stored in #22
           Integral { [ mu[ {d h} + curl_hM[] + Pert~{iP}[]] * ({d h} + curl_hM[] + Pert~{iP}[])/#12 ] ; In Omega ; Jacobian Vol; Integration II ; } } }
       { Name e_pert      ; Value { 
           Term { [ rho[] * {d h} ]   ; In Omega_C ; Jacobian Vol;  } } } 
       { Name e_proj      ; Value { Term { [ factor * rho[] * ( (XYZ[] - XYZ_Gauss[]) /\ curl_hM[] ) ] ; In Omega_C ; Jacobian Vol;  } } }
       { Name e_tot       ; Value { 
           Term { [ rho[] * {d h} ] ; In Omega_C ; Jacobian Vol;  }
           Term { [ factor * rho[] * ( (XYZ[] - XYZ_Gauss[]) /\ curl_hM[] ) ] ; In Omega_C ; Jacobian Vol;  } } }
       { Name JL_tot_mean  ; Value{ // stored in #25
           Integral { [ ( rho[] * SquNorm[ {d h} + curl_hM[] + Pert~{iP}[] ] )/#12 ] ; In Omega_C ; Jacobian Vol; Integration II ; } } }
       { Name MagLosses_tot_mean  ; Value{ // stored in #27. Contribution of the current time step to the integral \oint (h db) 
           Integral { [  mu[ {d h} + curl_hM[] + Pert~{iP}[]] * SquNorm[ ({d h} + curl_hM[] + Pert~{iP}[]) ]/#12 ] ; In Omega ; Jacobian Vol; Integration II ; } } }
       { Name MagEnergy_tot_mean  ; Value{ // stored in #27. Contribution of the current time step to the integral \oint (h db) 
           //Integral { [ ( nu[ {d a} + curl_aM[] + Pert~{iP}[]] * ({d a} + curl_aM[] + Pert~{iP}[] ) * (Dt[{d a}] + dt_bM[]) )/#12 ] ; In Omega ; Jacobian Vol; Integration II ; } } }
           Integral { [ mu[]/#12 ] ; In Omega ; Jacobian Vol; Integration II ; } } }
       { Name TotalEnergy_tot_mean ; Value{ // stored in #27. Contribution of the current time step to the integral \oint (h db) 
           Integral { [ ( rho[])/#12 ] ; In Omega_C ; Jacobian Vol; Integration II ; }
           //Integral { [ ( sigma[] * SquNorm[ Dt[{a}] + {ur} - eM[] - factor * ( (XYZ[] - XYZ_Gauss[]) /\ dt_bM[] ) ] )/#12 ] ; In Omega_C ; Jacobian Vol; Integration II ; }
           //Integral { [ ( {h} * (Dt[{d a}] + dt_bM[]) )/#12 ] ; In Omega_NL ; Jacobian Vol; Integration II ; }
           //Integral { [ ( nu[ {d a} + curl_aM[] + Pert~{iP}[]] * ({d a} + curl_aM[] + Pert~{iP}[] ) * (Dt[{d a}] + dt_bM[]) )/#12 ] ; In Omega_L ; Jacobian Vol; Integration II ; } 
         } }
       { Name TotalEnergyDifference_tot_mean ; Value{ // stored in #27. Contribution of the current time step to the integral \oint (h db) 
           Integral { [ ( rho[])/#12 ] ; In Omega_C ; Jacobian Vol; Integration II ; }
           //Integral { [ ( sigma[] * SquNorm[ Dt[{a}] + {ur} - eM[] - factor * ( (XYZ[] - XYZ_Gauss[]) /\ dt_bM[] ) ] )/#12 ] ; In Omega_C ; Jacobian Vol; Integration II ; }
           //Integral { [ ( -{h} * (Dt[{d a}]  + dt_bM[]) )/#12 ] ; In Omega_NL ; Jacobian Vol; Integration II ; }
           //Integral { [ ( -nu[ {d a} + curl_aM[] + Pert~{iP}[]] * ({d a} + curl_aM[] + Pert~{iP}[] ) * (Dt[{d a}] + dt_bM[]) )/#12 ] ; In Omega_L ; Jacobian Vol; Integration II ; } 
         } }
     }
   }
  EndFor
}



If(exactMicroGeo)
nameDirRes = "../output/mesoproblems/exact/";
EndIf
If(!exactMicroGeo)
nameDirRes = "../output/mesoproblems/computational/";
EndIf

PostOperation {
For iP In {1:Nbr_SubProblems}
 { Name mean~{iP} ; NameOfPostProcessing h_Micro_NR~{iP};
   Operation{
     Print[ vol[Omega]       , OnGlobal, Format Table, Store 12, File StrCat[nameDirRes, "vol.txt"], LastTimeStepOnly ] ;
     Print[ b_tot_mean[Omega], OnGlobal, Format Table, Store 21, File StrCat[nameDirRes, Sprintf("j%g_%g.txt",   iP, numEle) ], LastTimeStepOnly] ;
     Print[ h_tot_mean[Omega], OnGlobal, Format Table, Store 22, File StrCat[nameDirRes, Sprintf("e%g_%g.txt",   iP, numEle) ], LastTimeStepOnly] ;
     Print[ j_tot_mean[Omega_C], OnGlobal, Format Table, Store 23, File StrCat[nameDirRes, Sprintf("j_tot_mean%g_%g.txt",   iP, numEle) ], LastTimeStepOnly] ;
     Print[ JL_tot_mean[Omega_C], OnGlobal, Format Table, Store 25, File StrCat[nameDirRes, Sprintf("JL%g_%g.txt",   iP, numEle) ], LastTimeStepOnly] ;
     Print[ MagLosses_tot_mean[Omega_C], OnGlobal, Format Table, Store 27, File StrCat[nameDirRes, Sprintf("MagLosses%g_%g.txt",   iP, numEle) ], LastTimeStepOnly ] ;
     Print[ MagEnergy_tot_mean[Omega], OnGlobal, Format Table, Store 28, File StrCat[nameDirRes, Sprintf("MagEnergy%g_%g.txt",   iP, numEle) ], LastTimeStepOnly ] ;
     Print[ TotalEnergy_tot_mean[Omega], OnGlobal, Format Table, Store 29, File StrCat[nameDirRes, Sprintf("TotalEnergy%g_%g.txt",   iP, numEle) ], LastTimeStepOnly ] ;
     Print[ TotalEnergyDifference_tot_mean[Omega], OnGlobal, Format Table, Store 30, File StrCat[nameDirRes, Sprintf("TotalEnergyDifference%g_%g.txt",   iP, numEle) ], LastTimeStepOnly ] ;
   }
 }
 { Name cuts_field~{iP} ; NameOfPostProcessing h_Micro_NR~{iP};
   Operation{
     Print[ h_pert, OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("h_pert%g_%g.pos", iP, numEle) ], LastTimeStepOnly];
     Print[ h_proj, OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("h_proj%g_%g.pos", iP, numEle) ], LastTimeStepOnly];
     Print[ h_tot,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("h_tot%g_%g.pos" , iP, numEle) ], LastTimeStepOnly];
     Print[ j_pert, OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("j_pert%g_%g.pos" , iP, numEle) ], LastTimeStepOnly];
     Print[ j_proj, OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("j_proj%g_%g.pos" , iP, numEle) ], LastTimeStepOnly];
     Print[ j_tot,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("j_tot%g_%g.pos" , iP, numEle) ], LastTimeStepOnly];
     Print[ b_tot,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("b_tot%g_%g.pos" , iP, numEle) ], LastTimeStepOnly];
     Print[ e_pert,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("e_pert%g_%g.pos" , iP, numEle) ], LastTimeStepOnly];
     Print[ e_proj,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("e_proj%g_%g.pos" , iP, numEle) ], LastTimeStepOnly];
     Print[ e_tot,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("e_tot%g_%g.pos" , iP, numEle) ], LastTimeStepOnly];
   }
 }
 { Name map_field~{iP} ; NameOfPostProcessing h_Micro_NR~{iP};
   Operation {
     Print[ h_pert, OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("h_pert_p%g_cs%g_gp%g.pos", iP, currentTimeStep, numEle) ], Format Gmsh, LastTimeStepOnly ];
     Print[ h_proj, OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("h_proj_p%g_cs%g_gp%g.pos", iP, currentTimeStep, numEle) ], Format Gmsh, LastTimeStepOnly ];
     Print[ h_tot,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("h_tot_p%g_cs%g_gp%g.pos" , iP, currentTimeStep, numEle) ], Format Gmsh, LastTimeStepOnly ];

     Print[ j_pert, OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("j_pert_p%g_cs%g_gp%g.pos" , iP, currentTimeStep, numEle) ], Format Gmsh, LastTimeStepOnly ];
     Print[ j_tot,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("j_tot_p%g_cs%g_gp%g.pos" , iP, currentTimeStep, numEle) ], Format Gmsh, LastTimeStepOnly ];
     Print[ j_proj,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("j_proj_p%g_cs%g_gp%g.pos" , iP, currentTimeStep, numEle) ], Format Gmsh, LastTimeStepOnly ];

     Print[ e_tot ,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("e_tot_p%g_cs%g_gp%g.pos" , iP, currentTimeStep, numEle) ], Format Gmsh, LastTimeStepOnly ];
     Print[ e_pert,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("e_pert_p%g_cs%g_gp%g.pos" , iP, currentTimeStep, numEle) ], Format Gmsh, LastTimeStepOnly ];
     Print[ e_proj,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("e_proj_p%g_cs%g_gp%g.pos" , iP, currentTimeStep, numEle) ], Format Gmsh, LastTimeStepOnly ];

     Print[ b_tot,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("b_tot_p%g_cs%g_gp%g.pos" , iP, currentTimeStep, numEle) ], Format Gmsh, LastTimeStepOnly ];
   }
 }
 { Name map_field_all~{iP} ; NameOfPostProcessing h_Micro_NR~{iP};
   Operation {
     Print[ h_tot,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("h_tot_p%g_s%g_%g.pos" , iP, currentTimeStep, numEle) ], Format Gmsh, LastTimeStepOnly ];
     Print[ j_tot,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("j_tot_p%g_s%g_%g.pos" , iP, currentTimeStep, numEle) ], Format Gmsh, LastTimeStepOnly ];
     Print[ b_tot,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("b_tot_p%g_s%g_%g.pos" , iP, currentTimeStep, numEle) ], Format Gmsh, LastTimeStepOnly ];
   }
 }
 EndFor
}
