from gmshpy import *
from hmmpy  import *
import hmmpy 
import math
import sys
import os
import time

dir(hmmpy)

print'---- Lamination stack problem'
geoFile  = '../macro/geometry/square2d_Iso.geo'
meshFile = '../macro/geometry/square2d_Iso.msh'
macromodel = GModel()
macromodel.load(geoFile)
macromodel.mesh(2)
macromodel.save(meshFile)

# Physical regions
#=================
GAMMA_INF = 10
CONDUCTOR = 11
AIR       = 12

# Solver input data
#==================
mm          = 1e-3
micron      = 1e-6
d           = 0.5 * mm
d_c         = 45  * micron # thickness of the conductor.
d_i         = 2.5 * micron # thickness of the isolation.
e           = d_c + 2 * d_i # size of the periodic square. 
num         = 10 # number of cell over the length.
_lx         = e
_ly         = _lx
_nx         = 5
_ny         = _nx
problemType = 0
maxNumIter  = 4
tol         = 5e-12
gmres       = 0
nonlinear   = 0
hysteresis  = 1
#mesoSolver  = hysteresis 
mesoSolver  = nonlinear 

# Constructor
#============
hmmprob = hmmProblemBase()
hmmprob.setLX(_lx)
hmmprob.setLY(_ly)
hmmprob.setNX(_ny)
hmmprob.setNY(_ny)
hmmprob.setProblemType(problemType)

# 1. Set some parameters
#=======================
hmmprob.setLinsysAndDofManager()
hmmprob.setModel(macromodel)
hmmprob.setMaxNumIter(maxNumIter)
hmmprob.setNumIter(0)
hmmprob.setTolerance(tol)
hmmprob.setGmresValue(gmres)

# 2. hmmGroupOfDomains
#=====================
tagGroup_GammaINF = "Gamma_Inf"
hmmprob.addGroupOfDomains(tagGroup_GammaINF)
hmmprob.addDomainToGroupOfDomains(tagGroup_GammaINF, 1, GAMMA_INF)
print'Group_GammaINF is tagged', tagGroup_GammaINF

tagGroup_Air = "Air"
hmmprob.addGroupOfDomains(tagGroup_Air)
hmmprob.addDomainToGroupOfDomains(tagGroup_Air, 2, AIR)
print'Group_Air is tagged', tagGroup_Air

tagGroup_Cond = "Conductor"
hmmprob.addGroupOfDomains(tagGroup_Cond)
hmmprob.addDomainToGroupOfDomains(tagGroup_Cond, 2, CONDUCTOR)
print'Group_Dom is tagged', tagGroup_Cond

tagGroup_All = "All"
hmmprob.addGroupOfDomains(tagGroup_All)
hmmprob.addDomainToGroupOfDomains(tagGroup_All, 2, AIR)
hmmprob.addDomainToGroupOfDomains(tagGroup_All, 2, CONDUCTOR)
print'Group_All is tagged', tagGroup_All
hmmprob.getDomainsInfo()


# 4. hmmConstraints
#==================
imposedValue = 350.0
tagDiriInf = "diriInf"
hmmprob.addDirichletBC(tagGroup_GammaINF, tagDiriInf, 1, imposedValue)
print'The boundary condition at corner is tagged ', tagDiriInf
hmmprob.getConstraintsInfo()

# 5. Quadratures
#===============
tagQuad = "Grad"
hmmprob.addQuad(tagQuad)
print'The added quadrature rule is tagged ', tagQuad
hmmprob.getQuadInfo()

# 6. hmmFunctionSpace
#====================
tagSpace = "BF_Perp"
iField = 0
funcSpacType = Form1P
basisFunctionType = BF_PerpendicularEdge
hmmprob.addFunctionSpace(tagSpace, tagGroup_All, iField, funcSpacType, basisFunctionType)
hmmprob.addConstraintToFunctionSpace(tagSpace, tagDiriInf)
print'The added function space is tagged ', tagSpace
hmmprob.getFunctionSpaceInfo()

# 3. hmmFunction
#===============
GMSHVerbosity  = "0 "
GMSHGeoFileName = "../meso/micro_InsuCondInsu.geo "
GetDPVerbosity = "0 "
if mesoSolver == nonlinear :
    GetDPProFileName = "../meso/micro_vecH_dyn_nlin.pro "
    GetDPExactProFileName = "../meso/micro_vecH_dyn_nlin.pro "
else : # if hysteresis
    GetDPProFileName = "../meso/micro_vecH_dyn_hys.pro "
    GetDPExactProFileName = "../meso/micro_vecH_dyn_hys.pro "
GetDPResolutionName = "h_NR"
GetDPPostPro = "mean_1 mean_2 mean_3 mean_4 cuts_field_1 "
GMSHExactGeoFileName = "micro_exact.geo "
microProblemFileName = ""

pi = 3.14159
e1 = 0.45/0.5
e2 = e1
mu_Insu = 4.0 * pi * 1.0e-7 
mu_Iron = 1e3 * mu_Insu
mu_para = mu_Iron * e1 + mu_Insu * e2
mu_perp = mu_Iron * mu_Insu / (e1 * mu_Insu + e2 * mu_Iron)

#====================================================================
initTime = 0.0
#====================================================================

tagNLML_bh = "_bh_NLMatLaw"
tagLML_bh = "_bh_MatLaw"

hmmprob.add2ScaleNonLinearMaterialLawInno(tagNLML_bh, tagGroup_Cond, tagSpace, tagQuad, GMSHVerbosity, GMSHGeoFileName, GetDPVerbosity, GetDPProFileName, GetDPResolutionName, GetDPPostPro, GMSHExactGeoFileName, GetDPExactProFileName, microProblemFileName)
hmmprob.Init2ScaleNonLinearMLInno(tagNLML_bh, tagGroup_Cond, mu_para, 0.0, 0.0, 0.0, mu_para, 0.0, 0.0, 0.0, mu_para)
#hmmprob.Init2ScaleNonLinearMap(tagNLML_bh, tagGroup_Cond, 0.0)
#hmmprob.Init2ScaleNonLinearHLMap(tagQuad, tagNLML_bh, tagGroup_Cond, initTime, 0.0)

#tagElecEnergy            = "JouleLosses"
#tagMagEnergy             = "MagEnergy"
#tagTotalEnergy           = "TotalEnergy"
#tagTotalEnergyDifference = "TotalEnergyDifference"
#HL                       = 0.0

#hmmprob.Init2ScaleNonLinearGQMap(tagQuad, tagNLML_bh, tagGroup_Cond)

#hmmprob.addGQTo2ScaleNonLinearGQMap(tagElecEnergy, tagNLML_bh)
#hmmprob.InitGQMap(tagElecEnergy, tagNLML_bh, initTime, HL)

#hmmprob.addGQTo2ScaleNonLinearGQMap(tagMagEnergy, tagNLML_hb)
#hmmprob.InitGQMap(tagMagEnergy, tagNLML_hb, initTime, HL)

#hmmprob.addGQTo2ScaleNonLinearGQMap(tagTotalEnergy, tagNLML_hb)
#hmmprob.InitGQMap(tagTotalEnergy, tagNLML_hb, initTime, HL)

#hmmprob.addGQTo2ScaleNonLinearGQMap(tagTotalEnergyDifference, tagNLML_hb)
#hmmprob.InitGQMap(tagTotalEnergyDifference, tagNLML_hb, initTime, HL)


print "The added material law is tagged ", tagNLML_bh
hmmprob.add1ScaleLinearMaterialLawInno(tagLML_bh, tagGroup_Air, mu_Insu, 0.0, 0.0, 0.0, mu_Insu, 0.0, 0.0, 0.0, mu_Insu)
print "The added material law is tagged ", tagLML_bh

tagLML_rho = "_rho_MatLaw"
sigma      = 2405327
rho        = 1./sigma
rho1       = rho 
rho2       = rho
rho_para   = rho1 * e1 + rho2 * e2
rho_perp   = rho1 * rho2 / (e1 * rho2 + e2 * rho1)
rho_Iso    = 1.e-0 

hmmprob.add1ScaleLinearMaterialLawInno(tagLML_rho, tagGroup_Cond, rho1, 0.0, 0.0, 0.0, rho2, 0.0, 0.0, 0.0, 0.0)
hmmprob.add1ScaleLinearMaterialLawInno(tagLML_rho, tagGroup_Air, rho_Iso, 0.0, 0.0, 0.0, rho_Iso, 0.0, 0.0, 0.0, rho_Iso)
print "The added material law is tagged ", tagLML_rho
hmmprob.getFunctionInfo()

# 7. hmmFormulation
#==================
tagForm1 = "h_NL_From"
hmmprob.addFormulation(tagForm1)
hmmprob.addBilinearTermToFormulation(tagForm1, tagSpace, tagLML_bh , tagGroup_Air , SBF, tagQuad)
hmmprob.addBilinearTermToFormulation(tagForm1, tagSpace, tagNLML_bh, tagGroup_Cond, SBF, tagQuad)
hmmprob.addBilinearTermToFormulation(tagForm1, tagSpace, tagLML_rho, tagGroup_Air, GBF, tagQuad)
hmmprob.addBilinearTermToFormulation(tagForm1, tagSpace, tagLML_rho, tagGroup_Cond, GBF, tagQuad)

print'The added formulation is tagged ', tagForm1
hmmprob.getFormulationInfo()

# 8. hmmResolution
#=================
x = time.clock()
if mesoSolver == nonlinear :
    n = 20
    freq = 2500
elif mesoSolver == hysteresis :
    n = 200
    freq = 2500
else :
    n = 200
    freq = 2500

withNoHarmonics = True
if withNoHarmonics :
    period = 1.0/freq
    nbRT = 1.0
    endTime = nbRT * period
    dTime = (period)/n
    theta = 1.0
else :
    Harmonics = 5.;
    period    = 1/(Harmonics *freq) ;
    nbRT      = 1. ;
    endTime   = nbrT * (period * Harmonics) ;
    dTime     = period/nbrstep ;
    theta     = 1.0;

print 'initTime', initTime
print 'endTime', endTime
print 'dTime', dTime
print 'theta', theta
tagRes1 = "h_NL_Res"
tagPostPro1 = "h_NL_Post"
hmmprob.addSolver(tagRes1, timeDomSolver)
hmmprob.addPostPro(tagPostPro1)

#=============================================================================================
hmmprob.addPoint2MapOfPoints(tagNLML_bh, tagPostPro1, 699994, 0.50 * e, 0.5 * e , 0.0)
hmmprob.addPoint2MapOfPoints(tagNLML_bh, tagPostPro1, 699995, 0.50 * e, 3.5 * e , 0.0)
hmmprob.addPoint2MapOfPoints(tagNLML_bh, tagPostPro1, 699996, 0.50 * e, 6.5 * e , 0.0)
hmmprob.addPoint2MapOfPoints(tagNLML_bh, tagPostPro1, 699997, 0.50 * e, 9.5 * e , 0.0)

hmmprob.addPoint2MapOfPoints(tagNLML_bh, tagPostPro1, 799994, 3.5 * e, 0.5 * e , 0.0)
hmmprob.addPoint2MapOfPoints(tagNLML_bh, tagPostPro1, 799995, 3.5 * e, 3.5 * e , 0.0)
hmmprob.addPoint2MapOfPoints(tagNLML_bh, tagPostPro1, 799996, 3.5 * e, 6.5 * e , 0.0)
hmmprob.addPoint2MapOfPoints(tagNLML_bh, tagPostPro1, 799997, 3.5 * e, 9.5 * e , 0.0)

hmmprob.addPoint2MapOfPoints(tagNLML_bh, tagPostPro1, 899994, 6.5 * e, 0.5 * e , 0.0)
hmmprob.addPoint2MapOfPoints(tagNLML_bh, tagPostPro1, 899995, 6.5 * e, 3.5 * e , 0.0)
hmmprob.addPoint2MapOfPoints(tagNLML_bh, tagPostPro1, 899996, 6.5 * e, 6.5 * e , 0.0)
hmmprob.addPoint2MapOfPoints(tagNLML_bh, tagPostPro1, 899997, 6.5 * e, 9.5 * e , 0.0)

hmmprob.addPoint2MapOfPoints(tagNLML_bh, tagPostPro1, 999994, 9.5 * e, 0.5 * e , 0.0)
hmmprob.addPoint2MapOfPoints(tagNLML_bh, tagPostPro1, 999995, 9.5 * e, 3.5 * e , 0.0)
hmmprob.addPoint2MapOfPoints(tagNLML_bh, tagPostPro1, 999996, 9.5 * e, 6.5 * e , 0.0)
hmmprob.addPoint2MapOfPoints(tagNLML_bh, tagPostPro1, 999997, 9.5 * e, 9.5 * e , 0.0)
#=============================================================================================

hmmprob.addParameters(tagRes1, initTime, endTime, dTime, theta, freq)
hmmprob.addOp2Solver(tagRes1, tagForm1, InitSolLin_H_Form, hmmprob.getPostPro(tagPostPro1) )
hmmprob.addOp2Solver(tagRes1, tagForm1, Sol,    hmmprob.getPostPro(tagPostPro1) )
#hmmprob.addOp2Solver(tagRes1, tagForm1, SolJac, hmmprob.getPostPro(tagPostPro1) )
hmmprob.addOp2Solver(tagRes1, tagForm1, SolJac_H_Form, hmmprob.getPostPro(tagPostPro1) )

#nameOfQtyJouleLosses           = "JouleLosses_Last"
#nameOfQtyMagEnergy             = "MagEnergy_Last"
#nameOfQtyTotalEnergy           = "TotalEnergy_Last"
#nameOfQtyTotalEnergyDifference = "TotalEnergyDifference_Last"
#hmmprob.computeGQOverTime(tagElecEnergy           , tagNLML_hb, nameOfQtyJouleLosses          )
#hmmprob.computeGQOverTime(tagMagEnergy            , tagNLML_hb, nameOfQtyMagEnergy            )
#hmmprob.computeGQOverTime(tagTotalEnergy          , tagNLML_hb, nameOfQtyTotalEnergy          )
#hmmprob.computeGQOverTime(tagTotalEnergyDifference, tagNLML_hb, nameOfQtyTotalEnergyDifference)

# Computing Hysteresis losses
#if mesoSolver == hysteresis :
#    cycleNum = 1
#    hmmprob.computeTotalHysteresisLossesOverCycle(tagNLML_hb, (period-dTime), dTime, freq, cycleNum)

print'|CPU|',time.clock() - x
