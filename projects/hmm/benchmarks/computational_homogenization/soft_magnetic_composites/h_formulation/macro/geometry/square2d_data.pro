SymmetryFactor = 2; // Symmetry, half model (ring2d.geo or ring2d_new.geo)
AxialLength    = 1; // 1 m is the default value
micron         = 1e-6;
d              = 45*micron;
e              = 50*micron;   // lamination thickness + isolation
lambda         = d/e ;
nlam           = SymmetryFactor*10;//10
h              = nlam * e; // width lamination stack
L_X            = h/2;
L_Y            = h/2;
lc_smc_iso     = L_X/10;
lc_smc_cond    = L_X/10;
rla_ind        = 10 * e ;
xlam           = h/2 ;
ylam           = h/2 ;

// Physical numbers
//============================
GAMMA_INF      = 10;
CONDUCTOR      = 11; // first lamination or  complete block (if homog)
AIR            = 12;
IND            = 13;
