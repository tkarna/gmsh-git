Include "square2d_data.pro" ;

Mesh.Algorithm = 1; // 2D mesh algorithm (1=MeshAdapt, 2=Automatic, 5=Delaunay, 6=Frontal, 7=bamg, 8=delquad)
Geometry.CopyMeshingMethod = 1; // Copy meshing method when duplicating geometrical entities?

n = 13;
m = 13;
L_int = 0.5 * L_X;


p1 = newp;   Point(p1) = {-L_X, -L_Y, 0.0, lc_smc_iso};
p2 = newp;   Point(p2) = { L_X, -L_Y, 0.0, lc_smc_iso};
p3 = newp;   Point(p3) = { L_X,  L_Y, 0.0, lc_smc_iso};
p4 = newp;   Point(p4) = {-L_X,  L_Y, 0.0, lc_smc_iso};

//=====================================================
p5 = newp;   Point(p5) = {-L_int, -L_int, 0.0, lc_smc_iso};
p6 = newp;   Point(p6) = { L_int, -L_int, 0.0, lc_smc_iso};
p7 = newp;   Point(p7) = { L_int,  L_int, 0.0, lc_smc_iso};
p8 = newp;   Point(p8) = {-L_int,  L_int, 0.0, lc_smc_iso};
//=====================================================

l1 = newl ; Line(l1)  = {p1,p2} ;
l2 = newl ; Line(l2)  = {p2,p3} ;
l3 = newl ; Line(l3)  = {p3,p4} ;
l4 = newl ; Line(l4)  = {p4,p1} ;

//=======================================================
l5 = newl ; Line(l5)  = {p5,p6} ;
l6 = newl ; Line(l6)  = {p6,p7} ;
l7 = newl ; Line(l7)  = {p7,p8} ;
l8 = newl ; Line(l8)  = {p8,p5} ;
l9 = newl ; Line(l9)  = {p1,p5} ;
l10 = newl ; Line(l10)  = {p2,p6} ;
l11 = newl ; Line(l11)  = {p3,p7} ;
l12 = newl ; Line(l12)  = {p4,p8} ;
//=======================================================


ll1 = newll; Line Loop(ll1)   = {l5,l6,l7,l8} ;
s_cond1 = news ; Plane Surface(news) = {ll1} ;
Transfinite Line{l5,l7} = n ; 
Transfinite Line{l6,l8} = n ; 
Transfinite Surface{s_cond1}; Recombine Surface{s_cond1};

ll2 = newll; Line Loop(ll2)   = {l4,l9,-l8,-l12} ;
s_cond2 = news ; Plane Surface(news) = {ll2} ;
Transfinite Line{l4,l8} = n  Using Bump 0.2;
Transfinite Line{l9,l12} = m Using Progression 1.2; 
Transfinite Surface{s_cond2}; Recombine Surface{s_cond2};

ll3 = newll; Line Loop(ll3)   = {l1,l10,-l5,-l9} ;
s_cond3 = news ; Plane Surface(news) = {ll3} ;
Transfinite Line{l1,l5} = n  Using Bump 0.2;
Transfinite Line{l9,l10} = m Using Progression 1.2; 
Transfinite Surface{s_cond3}; Recombine Surface{s_cond3};

ll4 = newll; Line Loop(ll4)   = {l2,l11,-l6,-l10} ;
s_cond4 = news ; Plane Surface(news) = {ll4} ;
Transfinite Line{l2,l6} = n  Using Bump 0.2;
Transfinite Line{l10,l11} = m Using Progression 1.2; 
Transfinite Surface{s_cond4}; Recombine Surface{s_cond4};

ll5 = newll; Line Loop(ll5)   = {l3,l12,-l7,-l11} ;
s_cond5 = news ; Plane Surface(news) = {ll5} ;
Transfinite Line{l3,l7} = n  Using Bump 0.2;
Transfinite Line{l11,l12} = m Using Progression 1.2; 
Transfinite Surface{s_cond5}; Recombine Surface{s_cond5};

// ll1 = newll; Line Loop(ll1)   = {l1,l2,l3,l4} ;
// s_cond = news ; Plane Surface(news) = {ll1} ;
// Transfinite Line{l1,l3} = 11 ; // 10
// Transfinite Line{l2,l4} = 11 ; // 10
// Transfinite Surface{s_cond}; Recombine Surface{s_cond};

// Inductor 
//=========
phi = Atan2[ylam, xlam] ;
dla_ind_x = rla_ind * Cos[phi] ;
dla_ind_y = rla_ind * Sin[phi] ;

xind1 = xlam + dla_ind_x;
yind1 = ylam + dla_ind_x;

c_1 = newp; Point(newp) = {xlam , ylam , 0, lc_smc_iso};
c_2 = newp; Point(newp) = {-xlam, ylam , 0, lc_smc_iso};
c_3 = newp; Point(newp) = {-xlam, -ylam, 0, lc_smc_iso};
c_4 = newp; Point(newp) = {xlam , -ylam, 0, lc_smc_iso};

pi_1 =newp ; Point(newp) = { xind1 , ylam  ,  0, lc_smc_iso};
pi_2 =newp ; Point(newp) = { xlam  , yind1 ,  0, lc_smc_iso};
pi_3 =newp ; Point(newp) = { -xlam , yind1 ,  0, lc_smc_iso};
pi_4 =newp ; Point(newp) = { -xind1, ylam  ,  0, lc_smc_iso};
pi_5 =newp ; Point(newp) = { -xind1, -ylam ,  0, lc_smc_iso};
pi_6 =newp ; Point(newp) = { -xlam , -yind1,  0, lc_smc_iso};
pi_7 =newp ; Point(newp) = { xlam  , -yind1,  0, lc_smc_iso};
pi_8 =newp ; Point(newp) = { xind1 , -ylam ,  0, lc_smc_iso};

li_1 =newl ; Line(newl) = {pi_8,pi_1};
ci_1 =newl ; Circle(newl) = {pi_1,c_1, pi_2};
li_2 =newl ; Line(newl) = {pi_2,pi_3};
ci_2 =newl ; Circle(newl) = {pi_3, c_2,pi_4};
li_3 =newl ; Line(newl) = {pi_4,pi_5};
ci_3 =newl ; Circle(newl) = {pi_5, c_3,pi_6};
li_4 =newl ; Line(newl) = {pi_6,pi_7};
ci_4 =newl ; Circle(newl) = {pi_7, c_4,pi_8};

ll_ext = newll; Line Loop(ll_ext) = {li_1, ci_1, li_2, ci_2, li_3, ci_3, li_4, ci_4, l1, l2, l3, l4};
s_air_added = news; Plane Surface(s_air_added) = {ll_ext};

// Physical regions
//=================

Physical Surface(AIR)       = {s_air_added};
Physical Surface(IND)       = {};
Physical Line(GAMMA_INF)     = {li_1, ci_1, li_2, ci_2, li_3, ci_3, li_4, ci_4};
Physical Surface(CONDUCTOR) = {s_cond1, s_cond2, s_cond3, s_cond4, s_cond5};
