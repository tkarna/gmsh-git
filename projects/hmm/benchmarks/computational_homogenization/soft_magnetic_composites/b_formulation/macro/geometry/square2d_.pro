// Flag_Fine = 1 ; //==> laminated domain
// getdp square2d_.pro -msh square2d_.msh -sol MagDyn_a_t_NL -pos Dyn_NL -v 1
// getdp square2d_.pro -msh square2d_.msh -sol MagDyn_a_t_Hyst -pos Dyn_Hyst -v 1

// Flag_Fine = 0 ; //==> homogenized domain
// getdp square2d_.pro -msh homog.msh -sol MagDyn_a_t_Homo_NL -pos Dyn_Homo_NL -v 1
// getdp square2d_.pro -msh homog.msh -sol MagDyn_a_t_Homo_Hyst -pos Dyn_Homo_Hyst -v 1

Include "square2d_data.pro" ;

//==========================================================
Include "current_Flags.pro" ; 
//==========================================================

SymmetryFactor = 4 ;
AxialLength = 1; //1 m is the default

N = (Flag_Fine)?nlam/2: 1;

DefineConstant[ Flag_NL_law = { 1, Choices{
      0="linear",
      1="analytical",
      2="anhysteretic part of JA-model",
      3="Jiles-Atherton hysteresis model"}, Label "Nonlinear BH curve"} ];

DefineConstant[ ORDER = { 2, Choices{
      1="zero",
      2="2nd",
      3="4rd",
      4="6th",
      5="8th"}, Label "Order of homogenization approximation"} ];

ORDERMAX = 5;

Printf("==============> Flag_Fine = %g", Flag_Fine);
Printf("==============> Flag_NL_law = %g", Flag_NL_law);
Printf("==============> ORDER = %g", ORDER);


Dir="res/";
If(Flag_Fine)
  ExtGmsh    = Str[ Sprintf("_nl%g_f%g.pos", Flag_NL_law, Freq) ];
  ExtGnuplot = Str[ Sprintf("_nl%g_f%g.dat", Flag_NL_law, Freq) ];
EndIf
If(!Flag_Fine)
  ExtGmsh    = Str[ Sprintf("_nl%g_f%g_n%g.pos", Flag_NL_law, Freq, ORDER) ];
  ExtGnuplot = Str[ Sprintf("_nl%g_f%g_n%g.dat", Flag_NL_law, Freq, ORDER) ];
EndIf


Group {
  For k In {1:N}
    Iron~{k} = #{(IRON+k-1)};
    Iron += Region[{Iron~{k}}];
    SkinIron += Region[{(SKINIRON+k-1)}];
  EndFor

  Isolation = #ISOL;
  Air  = #AIR;
  OuterBnd = #OUTERBND;

  SurfaceGe0 = #{MIDDLE, MIDDLE_IND};
  SurfacesDirichletBC = Region[{SurfaceGe0, OuterBnd}];

  Ind  = #IND;

  DomainS0 = Region[ {Ind} ];

  DomainL   = Region[ {Air, Ind, Isolation} ] ;
  DomainNL  = Region[ {Iron} ];

  DomainCC  = Region[ {Air, Ind, Isolation} ] ;
  DomainC   = Region[ {Iron} ];

  Domain    = Region[ {DomainC, DomainCC}] ;
}

Include "matlab/BH_anhysteretic.pro";

Function {
  T = 1./Freq;
  Omega = 2*Pi*Freq;

  mu0 = 4.e-7*Pi;
  nu0 = 1./mu0;

  sigmaIron = 5e6;
  sigma[]   = sigmaIron;

  la = lambda; // Fill factor
  sigmaH[] = sigmaIron/lambda ;

  mur = 2500; // linear case (testing purposes)

  nu[#{Air, Ind, Isolation}] = 1./mu0 ;
  nuIron = nu0/mur ;

  // Parameters for Jiles-Atherton hysteresis model
  //Gyselink's paper: Incorporation of a Jiles-Atherton vector hysteresis model in 2D FE magnetic field computations
  Msat = 1145220; aaa = 59.5; kkk = 99.2; ccc = 0.54; alpha = 1.3e-4 ;
  hyst_FeSi = { Msat, aaa, kkk, ccc, alpha};

  If(Flag_NL_law==0) // For testing purposes
    nu[#{Iron}] = nuIron * (Flag_Fine ? 1.: lambda) ;
    h[] = $1*nuIron ;
    dhdb[]    = TensorDiag[1., 1., 1.] * nuIron ;
    dhdb_NL[] = TensorDiag[0., 0., 0.] ;
  EndIf

  // Example of analytical nonlinear law
  aa = 100; bb = 10;     cc = 1.80; // Ruth's values
  //aa = 388; bb = 0.3774; cc = 2.97; // Inno's values
  nu_1a[]     = aa + bb * Exp[cc*SquNorm[$1]] ; // $1 => b ={d a}
  dnudb2_1a[] = bb *cc* Exp[cc*SquNorm[$1]] ;
  h_1a[]      = nu_1a[$1#1] * #1 ;
  dhdb_1a[]   = TensorDiag[1,1,1] * nu_1a[$1#1] + 2*dnudb2_1a[#1] * SquDyadicProduct[#1]  ;
  dhdb_1a_NL[]= 2*dnudb2_1a[$1#1] * SquDyadicProduct[#1]  ;

  If (Flag_NL_law==1)  // Using analytical law
    h[]         = h_1a[$1];
    dhdb[]      = dhdb_1a[$1];
    nu[#{Iron}] = nu_1a[$1];
    dhdb_NL[]   = dhdb_1a_NL[$1];
  EndIf
  If (Flag_NL_law==2) //Using anhysteretic curve from Jiles-Atherton model
    h[]         = h_anhys[$1];
    dhdb[]      = dhdb_anhys[$1];
    nu[#{Iron}] = nu_anhys[$1];
    dhdb_NL[]   = dhdb_anhys_NL[$1];
  EndIf

  // Source
  Ns[#{Ind}]       = 2000;
  Sc[#{Ind}] = SurfaceArea[]{IND};

  js0[] = Ns[]/Sc[]*Vector[0,0,1] ;
  js[]  = js0[]*F_Sin_wt_p[]{2*Pi*Freq, 0.};

  NbT = 1/2 ;
  NbSteps = 100; //500, 1e3
  time0 = 0. ; timemax = T*NbT ; dtime = T/NbSteps ;
  theta_value = 1; // Time discretisation scheme: 1==Euler implicit; 0.5==Cranck-Nicolson

  Nb_max_iter = 30;
  reltol = 1e-4;
  abstol = 1e-6;
  relaxation_factor[] = ($Iteration < Nb_max_iter/2) ? 1: 0.3;

  // Legendre Polynomials (used in post-processing)
  // Watch out: yy[] implies even number of laminations ==> FIX ME!
  //yy[] = (Fmod[Fabs[$Y],e]-e/2)*2/d ; // y=z/(d/2)
  yy[] = ((Fmod[Fabs[$Y],e]-e/2)*2/d) ; // y=z/(d/2)
  alpha_0[] = 1 ; // zero order
  alpha_2[] = 1/2*(3*yy[]^2-1) ; //2nd order
  alpha_4[] = 1/8*(35*yy[]^4-30*yy[]^2+3); //4th order
  alpha_6[] = 1/16*(231*yy[]^6-315*yy[]^4+105*yy[]^2-5);
  alpha_8[] = 1/128*(6435*yy[]^8-12012*yy[]^6+6930*yy[]^4-1260*yy[]^2+35);

  // dy = 1/(d/2)*dz = (2/d)*dz
  dalpha_0[] = 0. ;
  dalpha_2[] = 3*yy[]*2/d;
  dalpha_4[] = 1/2*(35*yy[]^3-15*yy[])*2/d; //4th order
  dalpha_6[] = 1/8*(231*3*yy[]^5-315*2*yy[]^3+105*yy[])*2/d;
  dalpha_8[] = 1/64*(6435*4*yy[]^7-12012*3*yy[]^5+6930*2*yy[]^3-1260*yy[])*2/d;

  // dz = (d/2)*dy
  ialpha_0[] = yy[]*d/2 ;
  ialpha_2[] = 1/2*(yy[]^3-yy[])*d/2;
  ialpha_4[] = 1/8*(7*yy[]^5-10*yy[]^3+3*yy[])*d/2;
  ialpha_6[] = 1/16*(33*yy[]^7-63*yy[]^5+35*yy[]^3-5*yy[])*d/2 ;
  ialpha_8[] = 1/128*(6435*yy[]^9/9-12012*yy[]^7/7+6930*yy[]^5/5-1260*yy[]^3/3+35*yy[])*d/2;
}

Include "matlab/PQ.pro";
Include Sprintf("matlab/gausspoints_Ngp%g.pro", 5); // integration along the thickness of the lamination

Jacobian {
  { Name JVol ;
    Case {
      { Region All ; Jacobian Vol ; }
    }
  }
  { Name JSur ; Case { { Region All ; Jacobian Sur; } } }
}

Integration {
  { Name I ; Case {
      { Type Gauss ;
        Case {
          { GeoElement Triangle    ; NumberOfPoints  6 ; }
          { GeoElement Quadrangle  ; NumberOfPoints  7 ; }
          { GeoElement Tetrahedron ; NumberOfPoints  4 ; }
        }
      }
    }
  }
}

Constraint {

  { Name MVP  ; Type Assign ;
    Case {
      { Region SurfacesDirichletBC ; Value 0. ; }
    }
  }

  { Name MVP_aux  ; Type Assign ;
    Case {
      { Region SurfacesDirichletBC ; Value 0. ; }
    }
  }


  { Name Current_2D  ; Type Assign ; // Zero net current in each lamination
    Case {
      For k In {1:N}
        { Region Iron~{k} ; Value 0. ; }
      EndFor
    }
  }

  { Name Voltage_2D  ; Type Assign ;
    Case {
    }
  }

}

FunctionSpace {

  { Name Hcurl_a ; Type Form1P ;
    BasisFunction {
      { Name se  ; NameOfCoef ae  ; Function BF_PerpendicularEdge ;
        Support Domain ; Entity NodesOf[All] ; }
     }
    Constraint {
      { NameOfCoef ae ; EntityType NodesOf ; NameOfConstraint MVP ; }
    }
  }

  // Gradient of Electric scalar potential (2D)
  { Name Hregion_u_2D ; Type Form1P ;
    BasisFunction {
      { Name sr ; NameOfCoef ur ; Function BF_RegionZ ;
        Support DomainC ; Entity DomainC ; }
    }
    GlobalQuantity {
      { Name U ; Type AliasOf        ; NameOfCoef ur ; }
      { Name I ; Type AssociatedWith ; NameOfCoef ur ; }
    }
    Constraint {
      { NameOfCoef U ; EntityType Region ; NameOfConstraint Voltage_2D ; }
      { NameOfCoef I ; EntityType Region ; NameOfConstraint Current_2D ; }
    }
  }

  For i In {2:ORDERMAX}
    { Name H_b~{2*i-2} ; Type Form1 ;
      BasisFunction {
        { Name se  ; NameOfCoef ae  ; Function BF_Edge ; Support DomainNL ; Entity EdgesOf[ All ] ; }
      }
    }

    { Name Hcurl_a~{2*i-2} ; Type Form1P ;
      BasisFunction {
        { Name se  ; NameOfCoef ae  ; Function BF_PerpendicularEdge ;
          Support DomainNL ; Entity NodesOf[All] ; }
      }
      Constraint {
        { NameOfCoef ae ; EntityType NodesOf ; NameOfConstraint MVP_aux ; }
      }
    }

  EndFor


  { Name H_hysteresis ; Type Vector;
    BasisFunction {
      { Name sex ; NameOfCoef aex ; Function BF_VolumeX ; Support DomainNL ; Entity VolumesOf[ All ] ; }
      { Name sey ; NameOfCoef aey ; Function BF_VolumeY ; Support DomainNL ; Entity VolumesOf[ All ] ; }
      { Name sez ; NameOfCoef aez ; Function BF_VolumeZ ; Support DomainNL ; Entity VolumesOf[ All ] ; }
    }
  }

  // Store hystory for every integration point along thickness
  NbGaussPnts = Nqp;
  For i In {1:NbGaussPnts}
  { Name H_hysteresis~{i} ; Type Vector;
    BasisFunction {
      { Name sex ; NameOfCoef aex ; Function BF_VolumeX ; Support DomainNL ; Entity VolumesOf[ All ] ; }
      { Name sey ; NameOfCoef aey ; Function BF_VolumeY ; Support DomainNL ; Entity VolumesOf[ All ] ; }
      { Name sez ; NameOfCoef aez ; Function BF_VolumeZ ; Support DomainNL ; Entity VolumesOf[ All ] ; }
    }
  }
  EndFor

}


Formulation {
  // ====================================================================
  // NONLINEAR - single value b-h curve
  // ====================================================================

  { Name MagDyn_a_NL ; Type FemEquation ;
    Quantity {
      { Name a  ; Type Local  ; NameOfSpace Hcurl_a ; }
      { Name ur ; Type Local  ; NameOfSpace Hregion_u_2D ; }
      { Name I  ; Type Global ; NameOfSpace Hregion_u_2D [I] ; }
      { Name U  ; Type Global ; NameOfSpace Hregion_u_2D [U] ; }
    }
    Equation {
      Galerkin { [ nu[{d a}] * Dof{d a} , {d a} ]  ; In Domain ; Jacobian JVol ; Integration I ; }
      Galerkin { JacNL[ dhdb_NL[{d a}]* Dof{d a} , {d a} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }

      Galerkin { DtDof[ sigma[] * Dof{a} , {a} ]   ; In DomainC ; Jacobian JVol ; Integration I ; }
      Galerkin { [      sigma[] * Dof{ur}, {a} ]   ; In DomainC ; Jacobian JVol ; Integration I ; }

      Galerkin { DtDof[ sigma[] * Dof{a} , {ur} ]  ; In DomainC ; Jacobian JVol ; Integration I ; }
      Galerkin { [      sigma[] * Dof{ur}, {ur} ]  ; In DomainC ; Jacobian JVol ; Integration I ; }
      GlobalTerm { [ Dof{I} , {U} ]                ; In DomainC ; }

      Galerkin { [ -js[] , {a} ] ; In DomainS0     ; Jacobian JVol; Integration I ; }
    }
  }


   // ====================================================================
  // Homogenization and nonlinearity
  // ====================================================================
  { Name MagDyn_a_Homo_NL_0 ; Type FemEquation ;
    Quantity {
      { Name a ; Type Local ; NameOfSpace Hcurl_a ; }
    }
    Equation {
      Galerkin { [ nu[] * Dof{d a} , {d a} ] ; In DomainL  ; Jacobian JVol ; Integration I ; }
      Galerkin { [ -js[] , {a} ]             ; In DomainS0 ; Jacobian JVol ; Integration I ; }

      Galerkin { DtDof[ sigmaH[]*d^2*q_0_0 * Dof{d a}, {d a} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }

      Galerkin { [ h[{d a}/la] , {d a} ] ;  In DomainNL ; Jacobian JVol ; Integration I ; }
      Galerkin { JacNL[ 1/la*dhdb[{d a}/la] * Dof{d a} , {d a} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
    }
  }

  { Name MagDyn_a_Homo_NL_2 ; Type FemEquation ;
    Quantity {
      { Name a ; Type Local ; NameOfSpace Hcurl_a ; }
      For i In {2:ORDERMAX}
        //{ Name b~{2*i-2} ; Type Local ; NameOfSpace H_b~{2*i-2} ; }
        { Name b~{2*i-2} ; Type Local ; NameOfSpace Hcurl_a~{2*i-2} ; } // b is now an mvp
      EndFor
    }
    Equation {
      Galerkin { [ nu[] * Dof{d a} , {d a} ] ; In DomainL ; Jacobian JVol ; Integration I ; }
      Galerkin { [ -js[] , {a} ] ; In DomainS0 ; Jacobian JVol ; Integration I ; }

      //ORDER = 0
      Galerkin { DtDof[ sigmaH[]*d^2*q_0_0 * Dof{d a}, {d a} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }

      // ORDER 2
      Galerkin { DtDof[ sigmaH[]*d^2*q_0_2 * Dof{d b_2}, {d a} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }
      Galerkin { DtDof[ sigmaH[]*d^2*q_0_2 * Dof{d a}, {d b_2} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }
      Galerkin { DtDof[ sigmaH[]*d^2*q_2_2 * Dof{d b_2}, {d b_2} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }

      For i In{1:NbGaussPnts}
        Galerkin { [ h[ (({d a}+a_2~{i}*{d b_2})/la)#77 ]#10     * w~{i} , {d a} ] ; // h_0
          In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { [                               #10 * a_2~{i} * w~{i} , {d b_2} ] ; // h_2
          In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ (1/la * dhdb[ #77 ]  *  w~{i})#11  * Dof{d a} , {d a} ] ;
          In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_00
        Galerkin { JacNL[            #11 *           a_2~{i} * Dof{d b_2} , {d a} ] ;
          In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_02
        Galerkin { JacNL[            #11 * a_2~{i} * a_2~{i} * Dof{d b_2} , {d b_2} ] ;
          In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_22
        Galerkin { JacNL[            #11 * a_2~{i} *           Dof{d a} , {d b_2} ] ;
          In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_20
      EndFor
    }
  }

  { Name MagDyn_a_Homo_NL_4 ; Type FemEquation ;
    Quantity {
      { Name a   ; Type Local ; NameOfSpace Hcurl_a ; }
      For i In {2:ORDERMAX}
        //{ Name b~{2*i-2} ; Type Local ; NameOfSpace H_b~{2*i-2} ; }
        { Name b~{2*i-2} ; Type Local ; NameOfSpace Hcurl_a~{2*i-2} ; } // b is now an mvp
      EndFor
    }
    Equation {
      Galerkin { [ nu[] * Dof{d a} , {d a} ] ; In DomainL ; Jacobian JVol ; Integration I ; }
      Galerkin { [ -js[] , {a} ] ; In DomainS0 ; Jacobian JVol ; Integration I ; }

      //ORDER = 1 ==> 0
      Galerkin { DtDof[ sigmaH[]*d^2 * q_0_0 * Dof{d a}, {d a} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }

      // ORDER 2 ==> 2
      Galerkin { DtDof[ sigmaH[]*d^2 * q_0_2 * Dof{d b_2}, {d a} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_0_2 * Dof{d a}, {d b_2} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_2_2 * Dof{d b_2}, {d b_2} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }

      //ORDER = 3 ==> 4
      Galerkin { DtDof[ sigmaH[]*d^2 * q_2_4 * Dof{d b_4}, {d b_2} ];
        In DomainNL; Jacobian JVol ; Integration I; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_2_4 * Dof{d b_2}, {d b_4} ];
        In DomainNL; Jacobian JVol ; Integration I; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_4_4 * Dof{d b_4}, {d b_4} ];
        In DomainNL; Jacobian JVol ; Integration I; }

      // for NR iterations
      For i In{1:NbGaussPnts}
        Galerkin { [ h[ (({d a} + a_2~{i}*{d b_2}+ a_4~{i}*{d b_4})/la)#77 ]#10  * w~{i} , {d a} ]  ; // h_0
          In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { [                               #10 * a_2~{i} * w~{i} , {d b_2} ]  ; // h_2
          In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { [                               #10 * a_4~{i} * w~{i} , {d b_4} ]  ; // h_4
          In DomainNL ; Jacobian JVol ; Integration I ; }

        Galerkin{ JacNL[ (1/la * dhdb[ #77 ] * w~{i})#11      * Dof{d a} , {d a} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_00
        Galerkin{ JacNL[              #11 *          a_2~{i} * Dof{d b_2} , {d a} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_02
        Galerkin{ JacNL[              #11 *          a_4~{i} * Dof{d b_4} , {d a} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_04

        Galerkin{ JacNL[              #11 * a_2~{i} *           Dof{d a} , {d b_2} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_02
        Galerkin{ JacNL[              #11 * a_2~{i} * a_2~{i} * Dof{b_2} , {d b_2} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_22
        Galerkin{ JacNL[              #11 * a_2~{i} * a_4~{i} * Dof{b_4} , {d b_2} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_24

        Galerkin{ JacNL[              #11 * a_4~{i} *          Dof{d a} , {d b_4} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_04
        Galerkin{ JacNL[              #11 * a_4~{i} * a_4~{i} * Dof{d b_4} , {d b_4} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_44
        Galerkin{ JacNL[              #11 * a_4~{i} * a_2~{i} * Dof{d b_2} , {d b_4} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_24
      EndFor
    }
  }

  { Name MagDyn_a_Homo_NL_6 ; Type FemEquation ;
    Quantity {
      { Name a   ; Type Local ; NameOfSpace Hcurl_a ; }
      For i In {2:ORDERMAX}
        //{ Name b~{2*i-2} ; Type Local ; NameOfSpace H_b~{2*i-2} ; }
        { Name b~{2*i-2} ; Type Local ; NameOfSpace Hcurl_a~{2*i-2} ; }
      EndFor
    }
    Equation {
      Galerkin { [ nu[] * Dof{d a} , {d a} ] ; In DomainL ; Jacobian JVol ; Integration I ; }
      Galerkin { [ -js[] , {a} ] ; In DomainS0 ; Jacobian JVol ; Integration I ; }

      //ORDER = 1 ==> 0
      Galerkin { DtDof[ sigmaH[]*d^2 * q_0_0 * Dof{d a}, {d a} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }

      // ORDER 2 ==> 2
      Galerkin { DtDof[ sigmaH[]*d^2 * q_0_2 * Dof{d b_2}, {d a} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_0_2 * Dof{d a}, {d b_2} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_2_2 * Dof{d b_2}, {d b_2} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }

      //ORDER = 3 ==> 4
      Galerkin { DtDof[ sigmaH[]*d^2 * q_2_4 * Dof{d b_4}, {d b_2} ];
        In DomainNL; Jacobian JVol ; Integration I; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_2_4 * Dof{d b_2}, {d b_4} ];
        In DomainNL; Jacobian JVol ; Integration I; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_4_4 * Dof{d b_4}, {d b_4} ];
        In DomainNL; Jacobian JVol ; Integration I; }

      //ORDER = 4 ==> 6
      Galerkin { DtDof[ sigmaH[]*d^2 * q_4_6 * Dof{d b_6}, {d b_4} ];
        In DomainNL; Jacobian JVol ; Integration I; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_4_6 * Dof{d b_4}, {d b_6} ];
        In DomainNL; Jacobian JVol ; Integration I; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_6_6 * Dof{d b_6}, {d b_6} ];
        In DomainNL; Jacobian JVol ; Integration I; }

      // for NR iterations
      For i In{1:NbGaussPnts}
        Galerkin { [ h[ (({d a}+a_2~{i}*{d b_2}+a_4~{i}*{d b_4}+a_6~{i}*{d b_6})/la)#77 ]#10  * w~{i} , {d a} ]  ; // h_0
          In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { [                               #10 * a_2~{i} * w~{i} , {d b_2} ]  ; // h_2
          In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { [                               #10 * a_4~{i} * w~{i} , {d b_4} ]  ; // h_4
          In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { [                               #10 * a_6~{i} * w~{i} , {d b_6} ]  ; // h_6
          In DomainNL ; Jacobian JVol ; Integration I ; }

        Galerkin{ JacNL[ (1/la * dhdb[ #77 ] * w~{i})#11     * Dof{d a} , {d a} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_00
        Galerkin{ JacNL[              #11 *          a_2~{i} * Dof{d b_2} , {d a} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_02
        Galerkin{ JacNL[              #11 *          a_4~{i} * Dof{d b_4} , {d a} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_04
        Galerkin{ JacNL[              #11 *          a_6~{i} * Dof{d b_6} , {d a} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_06

        Galerkin{ JacNL[              #11 * a_2~{i} * a_2~{i} * Dof{d b_2} , {d b_2} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_22
        Galerkin{ JacNL[              #11 * a_2~{i} *           Dof{d a} , {d b_2} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_02
        Galerkin{ JacNL[              #11 * a_2~{i} * a_4~{i} * Dof{d b_4} , {d b_2} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_24
        Galerkin{ JacNL[              #11 * a_2~{i} * a_6~{i} * Dof{d b_6} , {d b_2} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_26

        Galerkin{ JacNL[              #11 * a_4~{i} * a_4~{i} * Dof{d b_4} , {d b_4} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_44
        Galerkin{ JacNL[              #11 * a_4~{i} *           Dof{d a} , {d b_4} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_04
        Galerkin{ JacNL[              #11 * a_4~{i} * a_2~{i} * Dof{d b_2} , {d b_4} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_24
        Galerkin{ JacNL[              #11 * a_4~{i} * a_6~{i} * Dof{d b_6} , {d b_4} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_46

        Galerkin{ JacNL[              #11 * a_6~{i} * a_6~{i} * Dof{d b_6} , {d b_6} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_66
        Galerkin{ JacNL[              #11 * a_6~{i} *           Dof{d a} , {d b_6} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_06
        Galerkin{ JacNL[              #11 * a_6~{i} * a_2~{i} * Dof{d b_2} , {d b_6} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_26
        Galerkin{ JacNL[              #11 * a_6~{i} * a_4~{i} * Dof{d b_4} , {d b_6} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_46
      EndFor
    }
  }

  { Name MagDyn_a_Homo_NL_8 ; Type FemEquation ;
    Quantity {
      { Name a   ; Type Local ; NameOfSpace Hcurl_a ; }
      For i In {2:ORDERMAX}
        //{ Name b~{2*i-2} ; Type Local ; NameOfSpace H_b~{2*i-2} ; }
        { Name b~{2*i-2} ; Type Local ; NameOfSpace Hcurl_a~{2*i-2} ; }
      EndFor
    }
    Equation {
      Galerkin { [ nu[] * Dof{d a} , {d a} ] ; In DomainL ; Jacobian JVol ; Integration I ; }
      Galerkin { [ -js[] , {a} ] ; In DomainS0 ; Jacobian JVol ; Integration I ; }

      //ORDER = 1 ==> 0
      Galerkin { DtDof[ sigmaH[]*d^2 * q_0_0 * Dof{d a}, {d a} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }

      // ORDER 2 ==> 2
      Galerkin { DtDof[ sigmaH[]*d^2 * q_0_2 * Dof{d b_2}, {d a} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_0_2 * Dof{d a}, {d b_2} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_2_2 * Dof{d b_2}, {d b_2} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }

      //ORDER = 3 ==> 4
      Galerkin { DtDof[ sigmaH[]*d^2 * q_2_4 * Dof{d b_4}, {d b_2} ];
        In DomainNL; Jacobian JVol ; Integration I; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_2_4 * Dof{d b_2}, {d b_4} ];
        In DomainNL; Jacobian JVol ; Integration I; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_4_4 * Dof{d b_4}, {d b_4} ];
        In DomainNL; Jacobian JVol ; Integration I; }

      //ORDER = 4 ==> 6
      Galerkin { DtDof[ sigmaH[]*d^2 * q_4_6 * Dof{d b_6}, {d b_4} ];
        In DomainNL; Jacobian JVol ; Integration I; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_4_6 * Dof{d b_4}, {d b_6} ];
        In DomainNL; Jacobian JVol ; Integration I; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_6_6 * Dof{d b_6}, {d b_6} ];
        In DomainNL; Jacobian JVol ; Integration I; }

      //ORDER = 5 ==> 8
      Galerkin { DtDof[ sigmaH[]*d^2 * q_6_8 * Dof{d b_8}, {d b_6} ];
        In DomainNL; Jacobian JVol ; Integration I; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_6_8 * Dof{d b_6}, {d b_8} ];
        In DomainNL; Jacobian JVol ; Integration I; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_8_8 * Dof{d b_8}, {d b_8} ];
        In DomainNL; Jacobian JVol ; Integration I; }

      // for NR iterations
      For i In{1:NbGaussPnts}
        Galerkin { [ h[ (({d a} + a_2~{i}*{d b_2}+a_4~{i}*{d b_4}+a_6~{i}*{d b_6}+a_8~{i}*{d b_8})/la)#77 ]#10  * w~{i} , {d a} ]  ; // h_0
          In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { [                               #10 * a_2~{i} * w~{i} , {d b_2} ]  ; // h_2
          In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { [                               #10 * a_4~{i} * w~{i} , {d b_4} ]  ; // h_4
          In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { [                               #10 * a_6~{i} * w~{i} , {d b_6} ]  ; // h_6
          In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { [                               #10 * a_8~{i} * w~{i} , {d b_8} ]  ; // h_8
          In DomainNL ; Jacobian JVol ; Integration I ; }


        Galerkin{ JacNL[ (1/la * dhdb[ #77 ] * w~{i})#11     * Dof{d a} , {d a} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_00
        Galerkin{ JacNL[              #11 *          a_2~{i} * Dof{d b_2} , {d a} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_02
        Galerkin{ JacNL[              #11 *          a_4~{i} * Dof{d b_4} , {d a} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_04
        Galerkin{ JacNL[              #11 *          a_6~{i} * Dof{d b_6} , {d a} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_06
        Galerkin{ JacNL[              #11 *          a_8~{i} * Dof{d b_8} , {d a} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_08

        Galerkin{ JacNL[              #11 * a_2~{i} * a_2~{i} * Dof{d b_2} , {d b_2} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_22
        Galerkin{ JacNL[              #11 * a_2~{i} *          Dof{d a} , {d b_2} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_02
        Galerkin{ JacNL[              #11 * a_2~{i} * a_4~{i} * Dof{d b_4} , {d b_2} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_24
        Galerkin{ JacNL[              #11 * a_2~{i} * a_6~{i} * Dof{d b_6} , {d b_2} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_26
        Galerkin{ JacNL[              #11 * a_2~{i} * a_8~{i} * Dof{d b_8} , {d b_2} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_28

        Galerkin{ JacNL[              #11 * a_4~{i} * a_4~{i} * Dof{d b_4} , {d b_4} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_44
        Galerkin{ JacNL[              #11 * a_4~{i} *          Dof{d a} , {d b_4} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_04
        Galerkin{ JacNL[              #11 * a_4~{i} * a_2~{i} * Dof{d b_2} , {d b_4} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_24
        Galerkin{ JacNL[              #11 * a_4~{i} * a_6~{i} * Dof{d b_6} , {d b_4} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_46
        Galerkin{ JacNL[              #11 * a_4~{i} * a_8~{i} * Dof{d b_8} , {d b_4} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_48

        Galerkin{ JacNL[              #11 * a_6~{i} * a_6~{i} * Dof{d b_6} , {d b_6} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_66
        Galerkin{ JacNL[              #11 * a_6~{i} *           Dof{d a} , {d b_6} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_06
        Galerkin{ JacNL[              #11 * a_6~{i} * a_2~{i} * Dof{d b_2} , {d b_6} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_26
        Galerkin{ JacNL[              #11 * a_6~{i} * a_4~{i} * Dof{d b_4} , {d b_6} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_46
        Galerkin{ JacNL[              #11 * a_6~{i} * a_8~{i} * Dof{d b_8} , {d b_6} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_68

        Galerkin{ JacNL[              #11 * a_8~{i} * a_8~{i} * Dof{d b_8} , {d b_8} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_88
        Galerkin{ JacNL[              #11 * a_8~{i} *           Dof{d a} , {d b_8} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_08
        Galerkin{ JacNL[              #11 * a_8~{i} * a_2~{i} * Dof{d b_2} , {d b_8} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_28
        Galerkin{ JacNL[              #11 * a_8~{i} * a_4~{i} * Dof{d b_4} , {d b_8} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_48
        Galerkin{ JacNL[              #11 * a_8~{i} * a_6~{i} * Dof{d b_6} , {d b_8} ] ; In DomainNL ; Jacobian JVol ; Integration I ; } //dhdb_68
      EndFor
    }
  }

  // ====================================================================
  // HYSTERESIS
  // ====================================================================
  { Name MagDyn_a_Hyst ; Type FemEquation ;
    Quantity {
      { Name a  ; Type Local  ; NameOfSpace Hcurl_a ; }
      { Name ur ; Type Local  ; NameOfSpace Hregion_u_2D ; }
      { Name I  ; Type Global ; NameOfSpace Hregion_u_2D [I] ; }
      { Name U  ; Type Global ; NameOfSpace Hregion_u_2D [U] ; }
      { Name h  ; Type Local  ; NameOfSpace H_hysteresis ; }
    }
    Equation {
      Galerkin { [ nu[] * Dof{d a} , {d a} ]     ; In DomainL ; Jacobian JVol ; Integration I ; }

      Galerkin { DtDof[ sigma[] * Dof{a} , {a} ] ; In DomainC ; Jacobian JVol ; Integration I ; }
      Galerkin { [ sigma[] * Dof{ur} , {a} ]     ; In DomainC ; Jacobian JVol ; Integration I ; }

      Galerkin { DtDof [ sigma[] * Dof{a} , {ur} ] ; In DomainC ; Jacobian JVol ; Integration I ; }
      Galerkin { [ sigma[] * Dof{ur}, {ur} ]       ; In DomainC ; Jacobian JVol ; Integration I ; }
      GlobalTerm { [ Dof{I} , {U} ]                ; In DomainC ; }

      Galerkin { [ -js[], {a} ] ; In DomainS0     ; Jacobian JVol; Integration I ; }

      Galerkin { [ h_Jiles[ {h}[1], {d a}[1], {d a} ]{List[hyst_FeSi]}#7, {d a} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }
      Galerkin { JacNL[ dhdb_Jiles[{h}, {d a}, {h}-{h}[1] ]{List[hyst_FeSi]} * Dof{d a} , {d a} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }

      Galerkin { [ Dof{h}, {h} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }
      Galerkin { [    -#7, {h} ]  ;
        In DomainNL ; Jacobian JVol ; Integration I ; }
    }
  }

  // ====================================================================
  // Homogenization and hysteresis
  // ====================================================================
  { Name MagDyn_a_Homo_Hyst_0 ; Type FemEquation ;
    Quantity {
      { Name a ; Type Local ; NameOfSpace Hcurl_a ; }
      { Name h ; Type Local ; NameOfSpace H_hysteresis ; }
    }
    Equation {
      Galerkin { [ nu[] * Dof{d a} , {d a} ] ; In DomainL ; Jacobian JVol ; Integration I ; }
      Galerkin { [ -js[] , {a} ] ; In DomainS0 ; Jacobian JVol ; Integration I ; }

      Galerkin { DtDof[ sigmaH[]*d^2 * q_0_0 * Dof{d a}, {d a} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; } //ORDER = 0

      Galerkin { JacNL[ 1/la*dhdb_Jiles[{h}, {d a}/la, {h}-{h}[1] ]{List[hyst_FeSi]} * Dof{d a} , {d a} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }
      Galerkin { [ h_Jiles[{h}[1], {d a}[1]/la, {d a}/la]{List[hyst_FeSi]}#77 , {d a} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }

      Galerkin { [ Dof{h} , {h} ]  ;
        In DomainNL ; Jacobian JVol ; Integration I ; }
      Galerkin { [    -#77 , {h} ]  ;
        In DomainNL ; Jacobian JVol ; Integration I ; }
    }
  }

  { Name MagDyn_a_Homo_Hyst_2 ; Type FemEquation ;
    Quantity {
      { Name a ; Type Local ; NameOfSpace Hcurl_a ; }
      For i In {1:NbGaussPnts}
        { Name h~{i} ; Type Local ; NameOfSpace H_hysteresis~{i} ; }
      EndFor
      For i In {2:ORDERMAX}
        { Name b~{2*i-2} ; Type Local ; NameOfSpace H_b~{2*i-2} ; }
      EndFor
    }
    Equation {
      Galerkin { [ nu[] * Dof{d a} , {d a} ] ; In DomainL ; Jacobian JVol ; Integration I ; }
      Galerkin { [ -js[] , {a} ] ; In DomainS0 ; Jacobian JVol ; Integration I ; }

      //ORDER = 0
      Galerkin { DtDof[ sigmaH[]*d^2 * q_0_0 * Dof{d a}, {d a} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }

      // ORDER 2
      Galerkin { DtDof[ sigmaH[]*d^2 * q_0_2 * Dof{b_2}, {d a} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_0_2 * Dof{d a}, {b_2} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_2_2 * Dof{b_2}, {b_2} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }

      // for NR iterations
      // History of h to be kept for every quadrature point along lamination thickness
      For i In{1:NbGaussPnts}
        Galerkin { [ Dof{h~{i}} , {h~{i}} ] ;
          In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { [ -h_Jiles[{h~{i}}[1],
                              ({d a}[1]+ a_2~{i}*{b_2}[1])/la,
                              ({d a}   + a_2~{i}*{b_2}   )/la]{List[hyst_FeSi]}#77 , {h~{i}} ] ;
          In DomainNL ; Jacobian JVol ; Integration I ; }

        Galerkin { [ Dof{h~{i}} * w~{i}         , {d a} ]  ; // h_Jiles0
          In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { [ Dof{h~{i}} * a_2~{i} * w~{i}, {b_2} ]  ; // h_Jiles2
          In DomainNL ; Jacobian JVol ; Integration I ; }
      EndFor

      For i In{1:NbGaussPnts} //dhdb_Jiles00, dhdb_Jiles02, dhdb_Jiles22, dhdb_Jiles20
        Galerkin { JacNL[ (1/la*dhdb_Jiles[{h~{i}},
                                           ({d a}+a_2~{i}*{b_2})/la,
                                           {h~{i}}-{h~{i}}[1]]{List[hyst_FeSi]}*w~{i})#22 * Dof{d a} , {d a} ] ;
          In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #22 * a_2~{i}           * Dof{b_2} , {d a} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #22 * a_2~{i} * a_2~{i} * Dof{b_2} , {b_2} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #22           * a_2~{i} * Dof{d a} , {b_2} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }
      EndFor
    }
  }

  { Name MagDyn_a_Homo_Hyst_4 ; Type FemEquation ;
    Quantity {
      { Name a ; Type Local ; NameOfSpace Hcurl_a ; }
      For i In {1:NbGaussPnts}
        { Name h~{i} ; Type Local ; NameOfSpace H_hysteresis~{i} ; }
      EndFor
      For i In {2:ORDERMAX}
        { Name b~{2*i-2} ; Type Local ; NameOfSpace H_b~{2*i-2} ; }
      EndFor
    }
    Equation {
      Galerkin { [ nu[] * Dof{d a} , {d a} ] ; In DomainL ; Jacobian JVol ; Integration I ; }
      Galerkin { [ -js[] , {a} ] ; In DomainS0 ; Jacobian JVol ; Integration I ; }

      //ORDER = 1 ==> 0
      Galerkin { DtDof[ sigmaH[]*d^2 * q_0_0 * Dof{d a}, {d a} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }

      // ORDER 2 ==> 2
      Galerkin { DtDof[ sigmaH[]*d^2 * q_0_2 * Dof{b_2}, {d a} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_0_2 * Dof{d a}, {b_2} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_2_2 * Dof{b_2}, {b_2} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }

      //ORDER = 3 ==> 4
      Galerkin { DtDof[ sigmaH[]*d^2 * q_2_4 * Dof{b_4}, {b_2} ];
        In DomainNL; Jacobian JVol ; Integration I; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_2_4 * Dof{b_2}, {b_4} ];
        In DomainNL; Jacobian JVol ; Integration I; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_4_4 * Dof{b_4}, {b_4} ];
        In DomainNL; Jacobian JVol ; Integration I; }

      // for NR iterations
      For i In{1:NbGaussPnts}
        Galerkin { [ Dof{h~{i}} , {h~{i}} ] ;
          In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { [ -h_Jiles[{h~{i}}[1],
                              ({d a}[1]+a_2~{i}*{b_2}[1]+a_4~{i}*{b_4}[1])/la,
                              ({d a}+   a_2~{i}*{b_2}   +a_4~{i}*{b_4}   )/la]{List[hyst_FeSi]}#77 , {h~{i}} ] ;
          In DomainNL ; Jacobian JVol ; Integration I ; }

        Galerkin { [ Dof{h~{i}}           * w~{i}, {d a} ]  ; // h_Jiles0
          In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { [ Dof{h~{i}} * a_2~{i} * w~{i}, {b_2} ]  ; // h_Jiles2
          In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { [ Dof{h~{i}} * a_4~{i} * w~{i}, {b_4} ]  ; // h_Jiles4
          In DomainNL ; Jacobian JVol ; Integration I ; }
      EndFor

      For i In{1:NbGaussPnts}
      //dhdb_Jiles 00, 02, 04, 22, 20, 24, 44, 40, 42
        Galerkin { JacNL[ (1/la*dhdb_Jiles[{h~{i}},
                                           ({d a}+a_2~{i}*{b_2}+a_4~{i}*{b_4})/la,
                                           {h~{i}}-{h~{i}}[1]]{List[hyst_FeSi]}*w~{i})#44 * Dof{d a} , {d a} ] ;
          In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #44 * a_2~{i}          * Dof{b_2} , {d a} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #44 * a_4~{i}          * Dof{b_4} , {d a} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }

        Galerkin { JacNL[ #44 * a_2~{i} * a_2~{i} * Dof{b_2} , {b_2} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #44           * a_2~{i} * Dof{d a} , {b_2} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #44 * a_4~{i} * a_2~{i} * Dof{b_4} , {b_2} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }

        Galerkin { JacNL[ #44 * a_4~{i} * a_4~{i} * Dof{b_4} , {b_4} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #44           * a_4~{i} * Dof{d a} , {b_4} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #44 * a_2~{i} * a_4~{i} * Dof{b_2} , {b_4} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
      EndFor
    }
  }

  { Name MagDyn_a_Homo_Hyst_6 ; Type FemEquation ;
    Quantity {
      { Name a   ; Type Local ; NameOfSpace Hcurl_a ; }
      For i In {1:NbGaussPnts}
        { Name h~{i} ; Type Local ; NameOfSpace H_hysteresis~{i} ; }
      EndFor
      For i In {2:ORDERMAX}
        { Name b~{2*i-2} ; Type Local ; NameOfSpace H_b~{2*i-2} ; }
      EndFor
    }
    Equation {
      Galerkin { [ nu[] * Dof{d a} , {d a} ] ; In DomainL ; Jacobian JVol ; Integration I ; }
      Galerkin { [ -js[] , {a} ] ; In DomainS0 ; Jacobian JVol ; Integration I ; }


      //ORDER = 1 ==> 0
      Galerkin { DtDof[ sigmaH[]*d^2 * q_0_0 * Dof{d a}, {d a} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }

      // ORDER 2 ==> 2
      Galerkin { DtDof[ sigmaH[]*d^2 * q_0_2 * Dof{b_2}, {d a} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_0_2 * Dof{d a}, {b_2} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_2_2 * Dof{b_2}, {b_2} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }

      //ORDER = 3 ==> 4
      Galerkin { DtDof[ sigmaH[]*d^2 * q_2_4 * Dof{b_4}, {b_2} ];
        In DomainNL; Jacobian JVol ; Integration I; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_2_4 * Dof{b_2}, {b_4} ];
        In DomainNL; Jacobian JVol ; Integration I; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_4_4 * Dof{b_4}, {b_4} ];
        In DomainNL; Jacobian JVol ; Integration I; }

      //ORDER = 4 ==> 6
      Galerkin { DtDof[ sigmaH[]*d^2 * q_4_6 * Dof{b_6}, {b_4} ];
        In DomainNL; Jacobian JVol ; Integration I; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_4_6 * Dof{b_4}, {b_6} ];
        In DomainNL; Jacobian JVol ; Integration I; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_6_6 * Dof{b_6}, {b_6} ];
        In DomainNL; Jacobian JVol ; Integration I; }

      // for NR iterations
      For i In {1:NbGaussPnts}
        Galerkin { [ Dof{h~{i}} , {h~{i}} ] ;
          In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { [ -h_Jiles[{h~{i}}[1],
                              ({d a}[1]+a_2~{i}*{b_2}[1]+a_4~{i}*{b_4}[1]+a_6~{i}*{b_6}[1])/la,
                              ({d a}+ a_2~{i}*{b_2}+a_4~{i}*{b_4}+a_6~{i}*{b_6})/la]{List[hyst_FeSi]}#77 , {h~{i}} ] ;
          In DomainNL ; Jacobian JVol ; Integration I ; }

        Galerkin { [ #77           * w~{i}, {d a} ]  ; // h_Jiles0
          In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { [ #77 * a_2~{i} * w~{i}, {b_2} ]  ; // h_Jiles2
          In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { [ #77 * a_4~{i} * w~{i}, {b_4} ]  ; // h_Jiles4
          In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { [ #77 * a_6~{i} * w~{i}, {b_6} ]  ; // h_Jiles6
          In DomainNL ; Jacobian JVol ; Integration I ; }
      EndFor
      For i In{1:NbGaussPnts}
      //dhdb_Jiles 00, 02, 04, 06, 22, 20, 24, 26, 44, 40, 42, 46, 66, 60, 62, 64
        Galerkin { JacNL[ (1/la*dhdb_Jiles[{h~{i}},
                                           ({d a}+a_2~{i}*{b_2}+a_4~{i}*{b_4}+a_6~{i}*{b_6})/la,
                                           {h~{i}}-{h~{i}}[1]]{List[hyst_FeSi]}*w~{i})#66 * Dof{d a} , {d a} ] ;
          In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66 * a_2~{i}          * Dof{b_2} , {d a} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66 * a_4~{i}          * Dof{b_4} , {d a} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66 * a_6~{i}          * Dof{b_6} , {d a} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }

        Galerkin { JacNL[ #66 * a_2~{i} * a_2~{i} * Dof{b_2} , {b_2} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66           * a_2~{i} * Dof{d a} , {b_2} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66 * a_4~{i} * a_2~{i} * Dof{b_4} , {b_2} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66 * a_6~{i} * a_2~{i} * Dof{b_6} , {b_2} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }

        Galerkin { JacNL[ #66 * a_4~{i} * a_4~{i} * Dof{b_4} , {b_4} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66           * a_4~{i} * Dof{d a} , {b_4} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66 * a_2~{i} * a_4~{i} * Dof{b_2} , {b_4} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66 * a_6~{i} * a_4~{i} * Dof{b_6} , {b_4} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }

        Galerkin { JacNL[ #66 * a_4~{i} * a_6~{i} * Dof{b_4} , {b_6} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66           * a_6~{i} * Dof{d a} , {b_6} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66 * a_2~{i} * a_6~{i} * Dof{b_2} , {b_6} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66 * a_6~{i} * a_6~{i} * Dof{b_6} , {b_6} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
      EndFor
    }
  }

  { Name MagDyn_a_Homo_Hyst_8 ; Type FemEquation ;
    Quantity {
      { Name a   ; Type Local ; NameOfSpace Hcurl_a ; }
      For i In {1:NbGaussPnts}
        { Name h~{i} ; Type Local ; NameOfSpace H_hysteresis~{i} ; }
      EndFor
      For i In {2:ORDERMAX}
        { Name b~{2*i-2} ; Type Local ; NameOfSpace H_b~{2*i-2} ; }
      EndFor
    }
    Equation {
      Galerkin { [ nu[] * Dof{d a} , {d a} ] ; In DomainL ; Jacobian JVol ; Integration I ; }
      Galerkin { [ -js[] , {a} ] ; In DomainS0 ; Jacobian JVol ; Integration I ; }


      //ORDER = 1 ==> 0
      Galerkin { DtDof[ sigmaH[]*d^2 * q_0_0 * Dof{d a}, {d a} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }

      // ORDER 2 ==> 2
      Galerkin { DtDof[ sigmaH[]*d^2 * q_0_2 * Dof{b_2}, {d a} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_0_2 * Dof{d a}, {b_2} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_2_2 * Dof{b_2}, {b_2} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }

      //ORDER = 3 ==> 4
      Galerkin { DtDof[ sigmaH[]*d^2 * q_2_4 * Dof{b_4}, {b_2} ];
        In DomainNL; Jacobian JVol ; Integration I; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_2_4 * Dof{b_2}, {b_4} ];
        In DomainNL; Jacobian JVol ; Integration I; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_4_4 * Dof{b_4}, {b_4} ];
        In DomainNL; Jacobian JVol ; Integration I; }

      //ORDER = 4 ==> 6
      Galerkin { DtDof[ sigmaH[]*d^2 * q_4_6 * Dof{b_6}, {b_4} ];
        In DomainNL; Jacobian JVol ; Integration I; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_4_6 * Dof{b_4}, {b_6} ];
        In DomainNL; Jacobian JVol ; Integration I; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_6_6 * Dof{b_6}, {b_6} ];
        In DomainNL; Jacobian JVol ; Integration I; }

      //ORDER = 5 ==> 8
      Galerkin { DtDof[ sigmaH[]*d^2 * q_6_8 * Dof{b_8}, {b_6} ];
        In DomainNL; Jacobian JVol ; Integration I; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_6_8 * Dof{b_6}, {b_8} ];
        In DomainNL; Jacobian JVol ; Integration I; }
      Galerkin { DtDof[ sigmaH[]*d^2 * q_8_8 * Dof{b_8}, {b_8} ];
        In DomainNL; Jacobian JVol ; Integration I; }

      // for NR iterations
      For i In {1:NbGaussPnts}
        Galerkin { [ Dof{h~{i}} , {h~{i}} ] ;
          In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { [ -h_Jiles[{h~{i}}[1],
                              ({d a}[1]+a_2~{i}*{b_2}[1]+a_4~{i}*{b_4}[1]+a_6~{i}*{b_6}[1]+a_8~{i}*{b_8}[1])/la,
                              ({d a}+ a_2~{i}*{b_2}+a_4~{i}*{b_4}+a_6~{i}*{b_6}+a_8~{i}*{b_8})/la]{List[hyst_FeSi]} , {h~{i}} ] ;
          In DomainNL ; Jacobian JVol ; Integration I ; }

        Galerkin { [ Dof{h~{i}}           * w~{i}, {d a} ]  ; // h_Jiles0
          In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { [ Dof{h~{i}} * a_2~{i} * w~{i}, {b_2} ]  ; // h_Jiles2
          In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { [ Dof{h~{i}} * a_4~{i} * w~{i}, {b_4} ]  ; // h_Jiles4
          In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { [ Dof{h~{i}} * a_6~{i} * w~{i}, {b_6} ]  ; // h_Jiles6
          In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { [ Dof{h~{i}} * a_8~{i} * w~{i}, {b_8} ]  ; // h_Jiles8
          In DomainNL ; Jacobian JVol ; Integration I ; }
      EndFor
      For i In{1:NbGaussPnts}
      //dhdb_Jiles 00, 02, 04, 06, 08, 22, 20, 24, 26, 28, 44, 40, 42, 46, 48, 66, 60, 62, 64, 68, 88, 80, 82, 84, 86
        Galerkin { JacNL[ (1/la*dhdb_Jiles[{h~{i}},
                                           ({d a}+a_2~{i}*{b_2}+a_4~{i}*{b_4}+a_6~{i}*{b_6}+a_8~{i}*{b_8})/la,
                                           {h~{i}}-{h~{i}}[1]]{List[hyst_FeSi]}*w~{i})#66 * Dof{d a} , {d a} ] ;
          In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66 * a_2~{i}          * Dof{b_2} , {d a} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66 * a_4~{i}          * Dof{b_4} , {d a} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66 * a_6~{i}          * Dof{b_6} , {d a} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66 * a_8~{i}          * Dof{b_8} , {d a} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }

        Galerkin { JacNL[ #66 * a_2~{i} * a_2~{i} * Dof{b_2} , {b_2} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66           * a_2~{i} * Dof{d a} , {b_2} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66 * a_4~{i} * a_2~{i} * Dof{b_4} , {b_2} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66 * a_6~{i} * a_2~{i} * Dof{b_6} , {b_2} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66 * a_8~{i} * a_2~{i} * Dof{b_8} , {b_2} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }

        Galerkin { JacNL[ #66 * a_4~{i} * a_4~{i} * Dof{b_4} , {b_4} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66           * a_4~{i} * Dof{d a} , {b_4} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66 * a_2~{i} * a_4~{i} * Dof{b_2} , {b_4} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66 * a_6~{i} * a_4~{i} * Dof{b_6} , {b_4} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66 * a_8~{i} * a_4~{i} * Dof{b_8} , {b_4} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }

        Galerkin { JacNL[ #66 * a_6~{i} * a_6~{i} * Dof{b_6} , {b_6} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66           * a_6~{i} * Dof{d a} , {b_6} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66 * a_2~{i} * a_6~{i} * Dof{b_2} , {b_6} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66 * a_4~{i} * a_6~{i} * Dof{b_4} , {b_6} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66 * a_8~{i} * a_6~{i} * Dof{b_8} , {b_6} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }

        Galerkin { JacNL[ #66 * a_8~{i} * a_8~{i} * Dof{b_8} , {b_8} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66           * a_8~{i} * Dof{d a} , {b_8} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66 * a_2~{i} * a_8~{i} * Dof{b_2} , {b_8} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66 * a_4~{i} * a_8~{i} * Dof{b_4} , {b_8} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
        Galerkin { JacNL[ #66 * a_6~{i} * a_8~{i} * Dof{b_6} , {b_8} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
      EndFor
    }
  }

}


Resolution {

  { Name MagDyn_a_t_NL ;
    System {
      { Name A ; NameOfFormulation MagDyn_a_NL ; }
    }
    Operation {
      InitSolution[A] ; SaveSolution[A] ;
      TimeLoopTheta[ time0, timemax, dtime, theta_value]{
        IterativeLoop[Nb_max_iter, reltol, relaxation_factor[]] {
          //IterativeLoopN[ Nb_max_iter, relaxation_factor[], System{ {A, reltol, abstol, Residual MeanL2Norm} }]{
          // Solution, Residual, RecalcResidual
          // L1Norm, MeanL1Norm, L2Norm, MeanL2Norm, LinfNorm
          GenerateJac[A]; SolveJac[A];
        }
        SaveSolution[A];
      }
    }
  }

  { Name MagDyn_a_t_Homo_NL ;
    System {
      { Name A ; NameOfFormulation MagDyn_a_Homo_NL~{2*ORDER-2} ; }
    }
    Operation {
      InitSolution[A] ; SaveSolution[A] ;
      TimeLoopTheta[ time0, timemax, dtime, theta_value]{
        IterativeLoop[Nb_max_iter, reltol, relaxation_factor[]] {
          //IterativeLoopN[ Nb_max_iter, relaxation_factor[],
          //System{ {A, reltol, abstol, Solution MeanL2Norm} }
          //PostOperation { { b_Homo_NL, reltol, abstol, MeanL1Norm } } ]{
          GenerateJac[A]; SolveJac[A];
        }
        SaveSolution[A];
      }
    }
  }

  /*
  { Name MagDyn_a_t_Hyst ;
    System {
      { Name A ; NameOfFormulation MagDyn_a_Hyst ; }
    }
    Operation {
      InitSolution[A] ; SaveSolution[A] ;
      TimeLoopTheta[ time0, timemax, dtime, theta_value]{
        IterativeLoop[Nb_max_iter, reltol, relaxation_factor[]] {
          //IterativeLoopN[ Nb_max_iter, relaxation_factor[], System{ {A, reltol, abstol, Solution MeanL2Norm} }]{
          GenerateJac[A]; SolveJac[A];
        }
        SaveSolution[A];
      }
    }
  }

  { Name MagDyn_a_t_Homo_Hyst ;
    System {
      { Name A ; NameOfFormulation MagDyn_a_Homo_Hyst~{2*ORDER-2} ; }
    }
    Operation {
      InitSolution[A] ; SaveSolution[A] ;
      TimeLoopTheta[ time0, timemax, dtime, theta_value]{
        IterativeLoop[Nb_max_iter, reltol, relaxation_factor[]] {
          //IterativeLoopN[ Nb_max_iter, relaxation_factor[], System{ {A, reltol, abstol, Solution MeanL2Norm} }]{
          GenerateJac[A]; SolveJac[A];
        }
       SaveSolution[A];
      }
    }
  }
  */
}


PostProcessing {
  //=================================================
  // NONLINEAR
  //=================================================
  { Name MagDyn_a_NL ; NameOfFormulation MagDyn_a_NL ;
    PostQuantity {
      { Name a  ; Value { Local { [ {a} ] ; In Domain ; Jacobian JVol; } } }
      { Name az ; Value { Local { [ CompZ[{a}] ] ; In Domain ; Jacobian JVol; } } }

      { Name b ; Value { Local { [ {d a} ] ; In Domain ; Jacobian JVol; } } }
      { Name h ; Value { Local { [ nu[{d a}]*{d a} ] ; In Domain  ; Jacobian JVol; } } }
      { Name hb ; Value { Term { [ TensorSym[
                CompX[nu[{d a}]*{d a}], CompY[nu[{d a}]*{d a}], CompZ[nu[{d a}]*{d a}],
                CompX[{d a}],           CompY[{d a}],           CompZ[{d a}]] ]; In DomainNL ; Jacobian JVol ; } } }

      { Name j  ; Value { Local { [ -sigma[]*(Dt[{a}]+{ur}) ] ; In DomainC ; Jacobian JVol; } } }
      { Name jz ; Value { Local { [ CompZ[-sigma[]*(Dt[{a}]+{ur})] ] ; In DomainC ; Jacobian JVol; } } }
      { Name js  ; Value { Local { [ js[] ] ; In DomainS0 ; Jacobian JVol; } } }

      { Name U ; Value { Term { [ {U} ] ; In DomainC ; } } }
      { Name I ; Value { Term { [ {I} ] ; In DomainC ; } } }

      { Name JouleLosses ; Value {
          Integral { [ sigma[] * SquNorm[Dt[{a}]] ] ; In DomainC ; Jacobian JVol ; Integration I ; } } }
      { Name MagEnergy ; Value {
          Integral { [ nu[{d a}]*{d a}*Dt[{d a}] ] ; In DomainC ; Jacobian JVol ; Integration I ; } } }

      { Name Flux ; Value { Integral { [ SymmetryFactor*AxialLength*js0[]*{a} ] ; In DomainS0 ; Jacobian JVol ; Integration I ; } } }
    }
  }

  { Name MagDyn_a_Homo_NL ; NameOfFormulation MagDyn_a_Homo_NL~{2*ORDER-2}  ;
    PostQuantity {
      { Name yy  ; Value { Local { [ yy[] ]  ; In Domain ; Jacobian JVol ; } } }
      { Name alpha2  ; Value { Local { [ alpha_2[] ]  ; In Domain ; Jacobian JVol ; } } }

      { Name a  ; Value { Local { [ {a} ]  ; In Domain ; Jacobian JVol ; } } }
      { Name az  ; Value { Local { [ CompZ[{a}] ]  ; In Domain ; Jacobian JVol ; } } }
      { Name js ; Value { Local { [ js[] ] ; In DomainS0 ; Jacobian JVol ; } } }
      { Name b  ; Value {
          Local { [ {d a}    ] ; In DomainL ; Jacobian JVol ; }
          Local { [ {d a}/la ] ; In DomainNL ; Jacobian JVol ; }
          For k In {2:ORDER}
            //Local { [ alpha~{2*k-2}[]*{b~{2*k-2}}/la ] ; In DomainNL ; Jacobian JVol ; }
            Local { [ alpha~{2*k-2}[]*{d b~{2*k-2}}/la ] ; In DomainNL ; Jacobian JVol ; }
          EndFor
        }
      }

      { Name h  ; Value {
          Local { [ nu[]*{d a} ] ; In DomainL  ; Jacobian JVol ; }
          If (ORDER==1)
            Local { [ h[{d a}/la ] ]; In DomainNL ; Jacobian JVol ; }
          EndIf
          If (ORDER==2)
            //Local { [ h[ ({d a}+alpha~{2}[]*{b~{2}})/la ] ] ; In DomainNL ; Jacobian JVol ; }
            Local { [ h[ ({d a}+alpha~{2}[]*{d b~{2}})/la ] ] ; In DomainNL ; Jacobian JVol ; }
          EndIf
          If (ORDER==3)
            //Local { [ h[ ({d a}+alpha~{2}[]*{b~{2}}+alpha~{4}[]*{b~{4}})/la] ] ; In DomainNL ; Jacobian JVol ; }
            Local { [ h[ ({d a}+alpha~{2}[]*{d b~{2}}+alpha~{4}[]*{d b~{4}})/la] ] ; In DomainNL ; Jacobian JVol ; }
          EndIf
          If (ORDER==4)
            //Local { [ h[ ({d a}+alpha~{2}[]*{b~{2}}+alpha~{4}[]*{b~{4}}+alpha~{6}[]*{b~{6}})/la] ] ;
            Local { [ h[ ({d a}+alpha~{2}[]*{d b~{2}}+alpha~{4}[]*{d b~{4}}+alpha~{6}[]*{d b~{6}})/la] ] ;
              In DomainNL ; Jacobian JVol ; }
          EndIf
          If (ORDER==5)
            //Local { [ h[ ({d a}+alpha~{2}[]*{b~{2}}+alpha~{4}[]*{b~{4}}+alpha~{6}[]*{b~{6}}+alpha~{8}[]*{b~{8}})/la] ] ;
            Local { [ h[ ({d a}+alpha~{2}[]*{d b~{2}}+alpha~{4}[]*{d b~{4}}+alpha~{6}[]*{d b~{6}}+alpha~{8}[]*{d b~{8}})/la] ] ;
              In DomainNL ; Jacobian JVol ; }
          EndIf
        }
      }

      { Name hb   ; Value {
          If (ORDER==1)
            Term { [ TensorSym[
                  CompX[ h[{d a}/la] ], CompY[ h[{d a}/la] ], CompZ[ h[{d a}/la] ],
                  CompX[ {d a}/la ],    CompY[ {d a}/la ],    CompZ[ {d a}/la] ] ]; In DomainNL ; Jacobian JVol ; }
          EndIf
          If (ORDER==2)
            Term { [ TensorSym[
                  CompX[ h[ ({d a}+alpha~{2}[]*{d b~{2}})/la] ],
                  CompY[ h[ ({d a}+alpha~{2}[]*{d b~{2}})/la] ],
                  CompZ[ h[ ({d a}+alpha~{2}[]*{d b~{2}})/la] ],
                  CompX[ ({d a}+alpha~{2}[]*{d b~{2}})/la ],
                  CompY[ ({d a}+alpha~{2}[]*{d b~{2}})/la ],
                  CompZ[ ({d a}+alpha~{2}[]*{d b~{2}})/la ] ] ]; In DomainNL ; Jacobian JVol ; }
          EndIf
          If (ORDER==3)
            Term { [ TensorSym[
                  CompX[ h[ ({d a}+alpha~{2}[]*{d b~{2}}+alpha~{4}[]*{d b~{4}})/la] ],
                  CompY[ h[ ({d a}+alpha~{2}[]*{d b~{2}}+alpha~{4}[]*{d b~{4}})/la] ],
                  CompZ[ h[ ({d a}+alpha~{2}[]*{d b~{2}}+alpha~{4}[]*{d b~{4}})/la] ],
                  CompX[ ({d a}+alpha~{2}[]*{d b~{2}}+alpha~{4}[]*{d b~{4}})/la ],
                  CompY[ ({d a}+alpha~{2}[]*{d b~{2}}+alpha~{4}[]*{d b~{4}})/la ],
                  CompZ[ ({d a}+alpha~{2}[]*{d b~{2}}+alpha~{4}[]*{d b~{4}})/la ] ]] ;
              In DomainNL ; Jacobian JVol ; }
          EndIf
          If (ORDER==4)
            Term { [ TensorSym[
                  CompX[ h[ ({d a}+alpha~{2}[]*{d b~{2}}+alpha~{4}[]*{d b~{4}}+alpha~{6}[]*{d b~{6}})/la] ],
                  CompY[ h[ ({d a}+alpha~{2}[]*{d b~{2}}+alpha~{4}[]*{d b~{4}}+alpha~{6}[]*{d b~{6}})/la] ],
                  CompZ[ h[ ({d a}+alpha~{2}[]*{d b~{2}}+alpha~{4}[]*{d b~{4}}+alpha~{6}[]*{d b~{6}})/la] ],
                  CompX[ ({d a}+alpha~{2}[]*{d b~{2}}+alpha~{4}[]*{d b~{4}}+alpha~{6}[]*{d b~{6}})/la ],
                  CompY[ ({d a}+alpha~{2}[]*{d b~{2}}+alpha~{4}[]*{d b~{4}}+alpha~{6}[]*{d b~{6}})/la ],
                  CompZ[ ({d a}+alpha~{2}[]*{d b~{2}}+alpha~{4}[]*{d b~{4}}+alpha~{6}[]*{d b~{6}})/la ] ]] ;
              In DomainNL ; Jacobian JVol ; }
          EndIf
          If (ORDER==5)
            Term { [ TensorSym[
                  CompX[ h[ ({d a}+alpha~{2}[]*{d b~{2}}+alpha~{4}[]*{d b~{4}}+alpha~{6}[]*{d b~{6}}+alpha~{8}[]*{d b~{8}})/la] ],
                  CompY[ h[ ({d a}+alpha~{2}[]*{d b~{2}}+alpha~{4}[]*{d b~{4}}+alpha~{6}[]*{d b~{6}}+alpha~{8}[]*{d b~{8}})/la] ],
                  CompZ[ h[ ({d a}+alpha~{2}[]*{d b~{2}}+alpha~{4}[]*{d b~{4}}+alpha~{6}[]*{d b~{6}}+alpha~{8}[]*{d b~{8}})/la] ],
                  CompX[ ({d a}+alpha~{2}[]*{d b~{2}}+alpha~{4}[]*{d b~{4}}+alpha~{6}[]*{d b~{6}}+alpha~{8}[]*{d b~{8}})/la ],
                  CompY[ ({d a}+alpha~{2}[]*{d b~{2}}+alpha~{4}[]*{d b~{4}}+alpha~{6}[]*{d b~{6}}+alpha~{8}[]*{d b~{8}})/la ],
                  CompZ[ ({d a}+alpha~{2}[]*{d b~{2}}+alpha~{4}[]*{d b~{4}}+alpha~{6}[]*{d b~{6}}+alpha~{8}[]*{d b~{8}})/la ] ]] ;
              In DomainNL ; Jacobian JVol ; }
          EndIf
        }
      }

      { Name j  ; Value { // Using Faraday law - check vector
          Local { [ sigmaH[]*ialpha_0[] * Dt[{d a}]/\Vector[0,1,0] ] ; In DomainNL ; Jacobian JVol ; }
          For k In {2:ORDER}
            //            Local { [ sigmaH[]*ialpha~{2*k-2}[]*Dt[{b~{2*k-2}}]/\Vector[0,1,0] ] ; In DomainNL ; Jacobian JVol ; }
            Local { [ sigmaH[]*ialpha~{2*k-2}[]*Dt[{d b~{2*k-2}}]/\Vector[0,1,0] ] ; In DomainNL ; Jacobian JVol ; }
          EndFor
        }
      }

      { Name JouleLosses ;
        Value {
          Integral { [ d^2*sigmaH[]*q_0_0*SquNorm[Dt[{d a}]] ] ;
            In DomainNL ; Jacobian JVol ; Integration I ; }
          If (ORDER>1)
            //Integral { [ 2*d^2*sigmaH[]*q_0_2*Dt[{d a}]*Dt[{b_2}] ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
            Integral { [ 2*d^2*sigmaH[]*q_0_2*Dt[{d a}]*Dt[{d b_2}] ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
          EndIf
          For k In {2:ORDER}
            //Integral { [ d^2*sigmaH[]*q~{2*k-2}~{2*k-2}*SquNorm[Dt[{b~{2*k-2}}]] ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
            Integral { [ d^2*sigmaH[]*q~{2*k-2}~{2*k-2}*SquNorm[Dt[{d b~{2*k-2}}]] ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
            If( ORDER > 2 && k < ORDER)
              //Integral { [ 2*d^2*sigmaH[]*q~{2*k-2}~{2*k}*Dt[{b~{2*k-2}}]*Dt[{b~{2*k}}] ] ;
              Integral { [ 2*d^2*sigmaH[]*q~{2*k-2}~{2*k}*Dt[{d b~{2*k-2}}]*Dt[{d b~{2*k}}] ] ;
                In DomainNL ; Jacobian JVol ; Integration I ; }
            EndIf
          EndFor
        }
      }

      { Name MagEnergy ;
        Value {
          Integral { [ nu[{d a}] * {d a} * Dt[{d a}] ] ;
            In DomainNL ; Jacobian JVol ; Integration I ; }
          For k In {2:ORDER}
            //Integral { [ p~{2*k-2}*nu[]*{b~{2*k-2}}*Dt[{b~{2*k-2}}] ] ;
            Integral { [ p~{2*k-2}*nu[]*{d b~{2*k-2}}*Dt[{d b~{2*k-2}}] ] ;
              In DomainNL ; Jacobian JVol ; Integration I ; }
          EndFor
        } }

      { Name Flux ; Value { Integral { [ SymmetryFactor*AxialLength*js0[] * {a} ] ;
            In DomainS0 ; Jacobian JVol ; Integration I ; } } }
    }
  }

  /*
  //=================================================
  // HYSTERESIS
  //=================================================
  { Name MagDyn_a_Hyst ; NameOfFormulation MagDyn_a_Hyst ;
    PostQuantity {
      { Name a  ; Value { Local { [ {a} ]   ; In Domain ; Jacobian JVol; } } }
      { Name az  ; Value { Local { [ CompZ[{a}] ]   ; In Domain ; Jacobian JVol; } } }
      { Name b  ; Value { Local { [ {d a} ] ; In Domain ; Jacobian JVol; } } }
      { Name j  ; Value { Local { [ -sigma[] * (Dt[{a}]+{ur}) ] ; In DomainC ; Jacobian JVol; } } }
      { Name jz  ; Value { Local { [ CompZ[-sigma[] * (Dt[{a}]+{ur})] ] ; In DomainC ; Jacobian JVol; } } }

      { Name js  ; Value { Local { [ js[] ] ; In DomainS0 ; Jacobian JVol; } } }

      { Name U ; Value { Term { [ {U} ]   ; In DomainC ; } } }
      { Name I ; Value { Term { [ {I} ]   ; In DomainC ; } } }

      { Name h    ; Value {
          Local { [ nu[]*{d a} ] ; In DomainL  ; Jacobian JVol; }
          Local { [ {h} ]   ; In DomainNL ; Jacobian JVol; }
        }
      }
      { Name hb   ; Value { Term { [
              TensorSym[ CompX[{h}],CompY[{h}],CompZ[{h}],
                CompX[{d a}],CompY[{d a}],CompZ[{d a}]] ]; In DomainNL ; Jacobian JVol ; } }
      }
      { Name hdb  ; Value {
          Local { [ {h}*({d a}-{d a}[1]) ] ; In DomainNL  ; Jacobian JVol ; }
        }
      }

     { Name JouleLosses ;
        Value { Integral { [ sigma[] * SquNorm[Dt[{a}]] ] ; In DomainC ;
            Jacobian JVol ; Integration I ; }
        }
      }
      { Name MagEnergy ;
        Value { Integral { [ nu[] * {d a} * Dt[{d a}] ] ; In DomainC ;
            Jacobian JVol ; Integration I ; }
        }
      }

      { Name Flux ; Value { Integral { [ SymmetryFactor*AxialLength*js0[] * {a} ] ; In DomainS0 ; Jacobian JVol ; Integration I ; } } }
    }
  }

  { Name MagDyn_a_Homo_Hyst ; NameOfFormulation MagDyn_a_Homo_Hyst~{2*ORDER-2}  ;
    PostQuantity {
      { Name a  ; Value { Local { [ {a} ]  ; In Domain ; Jacobian JVol ; } } }
      { Name js ; Value { Local { [ js[] ] ; In DomainS0 ; Jacobian JVol ; } } }
      { Name b  ; Value {
          Local { [ {d a}/la ]        ; In Domain ; Jacobian JVol ; }
          For k In {2:ORDER}
            Local { [ alpha~{2*k-2}[]*{b~{2*k-2}}/la ] ; In DomainNL ; Jacobian JVol ; }
          EndFor
      } }

      { Name j  ; Value { // Using Faraday law -- to check
          Local { [ sigmaH[] * ialpha_0[] * Dt[{d a}]/\Vector[0,1,0] ] ; In DomainNL ; Jacobian JVol ; }
          For k In {2:ORDER}
            Local { [ sigmaH[] * ialpha~{2*k-2}[]*Dt[{b~{2*k-2}}]/\Vector[0,1,0] ] ; In DomainNL ; Jacobian JVol ; }
          EndFor
       } }

      If (ORDER==1)
        { Name h  ; Value {
            Local { [ nu[]*{d a} ] ; In DomainL  ; Jacobian JVol ; }
            Local { [ {h} ]        ; In DomainNL ; Jacobian JVol ; }
          } }
      { Name hb   ; Value {
          Term { [ TensorSym[ CompX[{h}],   CompY[{h}],   CompZ[{h}],
                              CompX[{d a}/la], CompY[{d a}/la], CompZ[{d a}/la]] ]; In DomainNL ; Jacobian JVol ; }
        } }
      EndIf
      If (ORDER>1)
        For i In {1:NbGaussPnts}
          { Name h~{i}    ; Value { Local { [ {h~{i}} ] ; In DomainNL ; Jacobian JVol ; } } }
          { Name hb~{i}   ; Value {
              Term { [ TensorSym[ CompX[{h~{i}}],  CompY[{h~{i}}],   CompZ[{h~{i}}],
                                  CompX[{d a}/la], CompY[{d a}/la], CompZ[{d a}/la]] ]; In DomainNL ; Jacobian JVol ; }
              For k In {2:ORDER}
              Term { [ TensorSym[ 0,  0,  0,
                                  CompX[alpha~{2*k-2}[]*{b~{2*k-2}}/la],
                                  CompY[alpha~{2*k-2}[]*{b~{2*k-2}}/la],
                                  CompZ[alpha~{2*k-2}[]*{b~{2*k-2}}/la] ] ]; In DomainNL ; Jacobian JVol ; }
              EndFor
        } }
        EndFor
     EndIf

     { Name JouleLosses ; // To check...
        Value {
          Integral { [ d^2*sigmaH[]*q_0_0*SquNorm[Dt[{d a}]] ] ;
                     In DomainNL ; Jacobian JVol ; Integration I ; }
          If (ORDER>1)
            Integral { [ 2*d^2*sigmaH[]*q_0_2*Dt[{d a}]*Dt[{b_2}] ] ;
                       In DomainNL ; Jacobian JVol ; Integration I ; }
          EndIf
          For k In {2:ORDER}
            Integral { [ d^2*sigmaH[]*q~{2*k-2}~{2*k-2}*SquNorm[ Dt[{b~{2*k-2}}]] ] ;
              In DomainNL ; Jacobian JVol ; Integration I ; }
            If( ORDER > 2 && k < ORDER)
            Integral { [ 2*d^2*sigmaH[]*q~{2*k-2}~{2*k}*Dt[{b~{2*k-2}}]*Dt[{b~{2*k}}] ] ;
              In DomainNL ; Jacobian JVol ; Integration I ; }
            EndIf
          EndFor
        } }

      { Name Flux ; Value { Integral { [ SymmetryFactor*AxialLength*js0[]*{a} ] ; In DomainS0 ; Jacobian JVol ; Integration I ; } } }
    }
  }

  */
}



PostOperation {
  L_X = h ; L_Y=h;
  x0 = 0 ; // Center
  x1 = 0.25*L_X; // must be < 0.5*L_X

  lam_init = 0 ;
  lam_end  = N-1 ;

  { Name Dyn_NL ; NameOfPostProcessing MagDyn_a_NL ;
    Operation {
      Print[ az, OnElementsOf Domain,  File StrCat[Dir,StrCat["az",ExtGmsh]] ];
      Print[ j,  OnElementsOf DomainC, File StrCat[Dir,StrCat["j",ExtGmsh]] ];
      Print[ b,  OnElementsOf Domain,  File StrCat[Dir,StrCat["b",ExtGmsh]] ];
      //Print[ h,  OnElementsOf Domain , File StrCat[Dir,StrCat["h",ExtGmsh]] ];

      For i In {lam_init:lam_init}
        For k In {1:NbGaussPnts}
          Print[ hb, OnPoint {x0 , L_Y/2-d/2+y~{k}*d-i*e, 0}, Format TimeTable,
            File StrCat[Dir,Sprintf("hbx0_f%g_l%g_gp%g.dat", Freq, i, k)] ];
          Print[ hb, OnPoint {x1 , L_Y/2-d/2+y~{k}*d-i*e, 0}, Format TimeTable,
            File StrCat[Dir,Sprintf("hbx1_f%g_l%g_gp%g.dat", Freq, i, k)] ];
          Print[ j,  OnPoint {x0 , L_Y/2-d/2+y~{k}*d-i*e, 0}, Format TimeTable,
            File StrCat[Dir,Sprintf("jx0_f%g_l%g_gp%g.dat", Freq, i, k)] ];
          Print[ j,  OnPoint {x1 , L_Y/2-d/2+y~{k}*d-i*e, 0}, Format TimeTable,
            File StrCat[Dir,Sprintf("jx1_f%g_l%g_gp%g.dat", Freq, i, k)] ];
        EndFor
      EndFor

      Print[ Flux[Ind], OnGlobal, Format TimeTable, File StrCat[Dir, Sprintf("Flux_nl%g_f%g.dat", Flag_NL_law, Freq)] ] ;
      Print[ JouleLosses[Iron], OnGlobal, Format TimeTable, File StrCat[Dir, Sprintf("JL_nl%g_f%g.dat", Flag_NL_law, Freq)] ] ;
    }
  }

  { Name Dyn_Homo_NL ; NameOfPostProcessing MagDyn_a_Homo_NL ;
    Operation {
      /*
      // yy[], alpha_xxx[] and so on... seem to be correct
      Print[ yy , OnElementsOf Iron,  File StrCat[Dir,StrCat["yy", ExtGmsh]], TimeStep{1} ];
       For i In {8:8}
        For k In {1:NbGaussPnts}
          Print[ alpha2 , OnPoint {x0, L_Y/2-d/2+y~{k}*d-i*e, 0},  Format TimeTable, TimeStep{1} ];
          Print[ alpha2 , OnPoint {x0, L_Y/2-d/2-y~{k}*d-i*e, 0},  Format TimeTable, TimeStep{1} ];
        EndFor
      EndFor
      */
      Print[ b, OnElementsOf Domain,   File StrCat[Dir,StrCat["b", ExtGmsh]] ];
      Print[ j, OnElementsOf DomainC,  File StrCat[Dir,StrCat["j", ExtGmsh]] ];
      //Print[ h, OnElementsOf DomainNL, File StrCat["res/h", ExtGmsh]] ;

      For i In {lam_init:lam_init}
        For k In {1:NbGaussPnts}
          Print[ hb, OnPoint {x0, L_Y/2-d/2+y~{k}*d-i*e, 0}, Format TimeTable,
            File StrCat[Dir, Sprintf("hbx0_f%g_n%g_l%g_gp%g.dat", Freq, ORDER, i, k)] ];
          Print[ hb, OnPoint {x1, L_Y/2-d/2+y~{k}*d-i*e, 0}, Format TimeTable,
            File StrCat[Dir, Sprintf("hbx1_f%g_n%g_l%g_gp%g.dat", Freq, ORDER, i, k)] ];
          Print[ j,  OnPoint {x0, L_Y/2-d/2+y~{k}*d-i*e, 0}, Format TimeTable,
            File StrCat[Dir, Sprintf("jx0_f%g_n%g_l%g_gp%g.dat", Freq, ORDER, i, k)] ];
          Print[ j,  OnPoint {x1, L_Y/2-d/2+y~{k}*d-i*e, 0}, Format TimeTable,
            File StrCat[Dir, Sprintf("jx1_f%g_n%g_l%g_gp%g.dat", Freq, ORDER, i, k)] ];
        EndFor
      EndFor

      Print[ Flux[Ind], OnGlobal, Format TimeTable, File StrCat[Dir,StrCat["Flux",ExtGnuplot]] ] ;
      Print[ JouleLosses[Iron], OnGlobal, Format TimeTable, File StrCat[Dir,StrCat["JL",ExtGnuplot]] ];
    }
  }
  /*
 { Name Dyn_Hyst ; NameOfPostProcessing MagDyn_a_Hyst ;
    Operation {

      //Print[ b,  OnElementsOf Domain, File StrCat[Dir,StrCat["b",ExtGmsh]] ];
      //Print[ h,  OnElementsOf DomainNL, File StrCat[Dir,StrCat["h",ExtGmsh]] ];
      //Print[ j,  OnElementsOf DomainC, File StrCat[Dir,StrCat["j",ExtGmsh]] ];


      For i In {lam_init:lam_init}
        For k In {1:NbGaussPnts}
          Print[ hb,  OnPoint {x0 , L_Y/2-d/2+y~{k}*d-i*e, 0},
            File StrCat[Dir, Sprintf("hbx0_f%g_l%g_gp%g.dat", Freq, i, k)], Format TimeTable] ;
          Print[ hb,  OnPoint {x1 , L_Y/2-d/2+y~{k}*d-i*e, 0},
            File StrCat[Dir, Sprintf("hbx1_f%g_l%g_gp%g.dat", Freq, i, k)], Format TimeTable] ;
          Print[ j,  OnPoint {x0 , L_Y/2-d/2+y~{k}*d-i*e, 0},
            File StrCat[Dir, Sprintf("jx0_f%g_l%g_gp%g.dat", Freq, i, k)], Format TimeTable] ;
          Print[ j,  OnPoint {x1 , L_Y/2-d/2+y~{k}*d-i*e, 0},
            File StrCat[Dir, Sprintf("jx1_f%g_l%g_gp%g.dat", Freq, i, k)], Format TimeTable] ;
        EndFor
      EndFor

      Print[ Flux[Ind], OnGlobal, Format TimeTable,     File StrCat["res/Flux",ExtGnuplot]] ;
      Print[ JouleLosses[Iron], OnGlobal, File StrCat[Dir, StrCat["JL",ExtGnuplot]], Format TimeTable];
    }
  }

  { Name Dyn_Homo_Hyst ; NameOfPostProcessing MagDyn_a_Homo_Hyst ;
    Operation {
      Print[ b, OnElementsOf Domain, File StrCat[Dir,StrCat["b",ExtGmsh]] ] ;
      Print[ j, OnElementsOf Domain, File StrCat[Dir,StrCat["j",ExtGmsh]] ] ;

      If(ORDER==1) // Constant per lamination: enough to do the postpro in one point
        For i In {lam_init:lam_init}
          Print[ hb,  OnPoint {x0, L_Y/2-d/2+y~{1}*d-i*e, 0},
            File StrCat[Dir,Sprintf("res/hbx0_f%g_n%g_l%g_gp%g.dat", Freq, ORDER, i, 1)], Format TimeTable] ;
          Print[ hb,  OnPoint {x1, L_Y/2-d/2+y~{1}*d-i*e, 0},
            File StrCat[Dir,Sprintf("res/hbx1_f%g_n%g_l%g_gp%g.dat", Freq, ORDER, i, 1)], Format TimeTable] ;
        EndFor
      EndIf
      If(ORDER>1)
        For i In {lam_init:lam_init}
          For k In {1:NbGaussPnts}
            Print[ hb~{k},  OnPoint {x0 , L_Y/2-d/2+y~{k}*d-i*e, 0},
              File StrCat[Dir,Sprintf("hbx0_f%g_n%g_l%g_gp%g.dat", Freq, ORDER, i, k)], Format TimeTable] ;
            Print[ hb~{k},  OnPoint {x1 , L_Y/2-d/2+y~{k}*d-i*e, 0},
              File StrCat[Dir,Sprintf("hbx1_f%g_n%g_l%g_gp%g.dat", Freq, ORDER, i, k)], Format TimeTable] ;
          EndFor
        EndFor
      EndIf

      Print[ Flux[Ind], OnGlobal, Format TimeTable,     File StrCat["res/Flux",ExtGnuplot]] ;
      Print[ JouleLosses[Iron], OnGlobal, File StrCat[Dir, StrCat["JL",ExtGnuplot]], Format TimeTable];
    }
  }
  */
}

