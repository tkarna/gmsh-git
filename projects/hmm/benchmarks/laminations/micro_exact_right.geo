
//min     = 5;
//cal1    = Ceil(ratio * ppp_x);
//cal2    = ppp_x-cal1;
//n1_1 = (cal1<min)?min:cal1;
//n1_2 = (cal2<min)?min:cal2;

// Define the right side of the domain
//===================================

If(d ==  0)

EndIf


If( (d < eps_xx) && (d > 0) ) 

P_TL_2_x = P_BL_x;
P_TL_2_y = P_BL_3_y + (nop_y - 1) * eps_y + (d);
P_TL_2 = newp; Point(P_TL_2) =  { P_TL_2_x , P_TL_2_y, 0.0};

P_TR_2_x = P_BR_x;
P_TR_2_y = P_BR_3_y + (nop_y - 1) * eps_y + (d);
P_TR_2 = newp; Point(P_TR_2) =  { P_TR_2_x , P_TR_2_y, 0.0};

vec_P_TL_2[] += P_TL_2;
vec_P_TR_2[] += P_TR_2;

If(nop_y == 1)
l2_T = newl ; Line(l2_T)  = {P_BL_3,P_TL_2};
l3_T = newl ; Line(l3_T)  = {P_TL_2,P_TR_2};
l4_T = newl ; Line(l4_T)  = {P_TR_2,P_BR_3};
EndIf

If(nop_y >= 2)
l2_T = newl ; Line(l2_T)  = {vec_P_L_3[nop_y - 2],P_TL_2};
l3_T = newl ; Line(l3_T)  = {P_TL_2,P_TR_2};
l4_T = newl ; Line(l4_T)  = {P_TR_2,vec_P_R_3[nop_y - 2]};
EndIf

//================
vec_l2[]  +={l2_T} ;
vec_l3[]  +={l3_T} ;
vec_l4[]  +={l4_T} ;
//================

/////////////////////////////////////
haut_vec[]   += {l3_T};
gauche_vec[] += {l2_T};
droite_vec[] += {l4_T};
////////////////////////////////////

If(nop_y == 1)
ll10 = newll; Line Loop(ll10)         = {-vec_l1_B[0],l2_T,l3_T,l4_T} ;
s11  = news ; Plane Surface(s11)      = {-ll10} ;
EndIf

If(nop_y >= 2)
ll10 = newll; Line Loop(ll10)         = {-vec_l5[nop_y-2],l2_T,l3_T,l4_T} ;
s11  = news ; Plane Surface(s11)      = {-ll10} ;
EndIf

Transfinite Line{l3_T}      = n2+1;
//n1_2 = Floor(n1_2 * (d)/eps_yy);
Transfinite Line{l2_T,l4_T} = n1_2+1;
Transfinite Surface{s11}; Recombine Surface{s11} ;

inno1[] += {s11};

EndIf

If(d == eps_yy) 

P_TL_1_x = P_BL_x;
P_TL_1_y = P_BL_y + (nop_y - 1) * eps_y;
P_TL_1 = newp; Point(P_TL_1) =  { P_TL_1_x , P_TL_1_y, 0.0};

P_TL_2_x = P_BL_x;
P_TL_2_y = P_BL_3_y + (nop_y - 1) * eps_y + (eps_y - d);
P_TL_2 = newp; Point(P_TL_2) =  { P_TL_2_x , P_TL_2_y, 0.0};

P_TR_1_x = P_BR_x;
P_TR_1_y = P_BR_3_y + (nop_y - 1) * eps_y;
P_TR_1 = newp; Point(P_TR_1) =  { P_TR_1_x , P_TR_1_y, 0.0};

P_TR_2_x = P_BR_x;
P_TR_2_y = P_BR_y + (nop_y - 1) * eps_y + (eps_y - d);
P_TR_2 = newp; Point(P_TR_2) =  { P_TR_2_x , P_TR_2_y, 0.0};

vec_P_TL_1[] += P_TL_1;
vec_P_TR_1[] += P_TR_1;
vec_P_TL_2[] += P_TL_2;
vec_P_TR_2[] += P_TR_2;

// Creating lines
//===============
l1_T = newl ; Line(l1_T)  = {P_TR_1,P_TL_1};
l2_T = newl ; Line(l2_T)  = {P_TL_1,P_TL_2};
l3_T = newl ; Line(l3_T)  = {P_TL_2,P_TR_2};
l4_T = newl ; Line(l4_T)  = {P_TR_2,P_TR_1};

//==================
vec_l1[]  +={l1_T} ;
vec_l2[]  +={l2_T} ;
vec_l3[]  +={l3_T} ;
vec_l4[]  +={l4_T} ;
//==================

/////////////////////////////////////
haut_vec[]   += {l3_T};
gauche_vec[] += {l2_T};
droite_vec[] += {l4_T};
////////////////////////////////////

ll10 = newll; Line Loop(ll10)         = {l1_T,l2_T,l3_T,l4_T} ;
s11  = news ; Plane Surface(s11)      = {-ll10} ;

Transfinite Line{l1_T,l3_T}           = n2+1;
Transfinite Line{l2_T,l4_T}           = n1_2+1;
Transfinite Surface{s11}; Recombine Surface{s11} ;

inno1[] += {s11};

EndIf

If(d > eps_yy)

P_TL_2_x = P_BL_x;
P_TL_2_y = P_BR_3_y + (nop_y - 1) * eps_y + eps_yy  ;
P_TL_2 = newp; Point(P_TL_2) =  { P_TL_2_x , P_TL_2_y, 0.0};


P_TL_3_x = P_BL_x;
P_TL_3_y = P_BL_3_y + (nop_y - 1) * eps_y + d;
P_TL_3 = newp; Point(P_TL_3) =  { P_TL_3_x , P_TL_3_y, 0.0};

P_TR_2_x = P_BR_x;
P_TR_2_y = P_BR_3_y + (nop_y - 1) * eps_y + eps_yy ;
P_TR_2 = newp; Point(P_TR_2) =  { P_TR_2_x , P_TR_2_y, 0.0};

P_TR_3_x = P_BR_x;
P_TR_3_y = P_BR_3_y + (nop_y - 1) * eps_y + d;
P_TR_3 = newp; Point(P_TR_3) =  { P_TR_3_x , P_TR_3_y, 0.0};

vec_P_TL_2[] += P_TL_2;
vec_P_TR_2[] += P_TR_2;
vec_P_TL_3[] += P_TL_3;
vec_P_TR_3[] += P_TR_3;

// Creating lines
//===============

If(nop_y >= 2)
l2_T = newl ; Line(l2_T)  = {vec_P_L_3[nop_y - 2],P_TL_2};
l4_T = newl ; Line(l4_T)  = {P_TR_2,vec_P_R_3[nop_y - 2]};
EndIf

If(nop_y == 1)
l2_T = newl ; Line(l2_T)  = {vec_P_BL_3[0],P_TL_2};
l4_T = newl ; Line(l4_T)  = {P_TR_2,vec_P_BR_3[0]};
EndIf

l3_T = newl ; Line(l3_T)  = {P_TL_2,P_TR_2};
l5_T = newl ; Line(l5_T)  = {P_TR_2,P_TR_3};
l6_T = newl ; Line(l6_T)  = {P_TR_3,P_TL_3};
l7_T = newl ; Line(l7_T)  = {P_TL_3,P_TL_2};
//==================
vec_l2[]  +={l2_T} ;
vec_l3[]  +={l3_T} ;
vec_l4[]  +={l4_T} ;
vec_l5[]  +={l5_T} ;
vec_l6[]  +={l6_T} ;
vec_l7[]  +={l7_T} ;
//==================

/////////////////////////////////////
haut_vec[]   += {l6_T};
gauche_vec[] += {l2_T,l7_T};
droite_vec[] += {l4_T,l5_T};
////////////////////////////////////

If(nop_y == 1)
ll10 = newll; Line Loop(ll10)         = {-vec_l1_B[0],l2_T,l3_T,l4_T} ;
s11  = news ; Plane Surface(s11)      = {-ll10} ;
EndIf

If(nop_y >= 2)
ll10 = newll; Line Loop(ll10)         = {-vec_l5[nop_y - 2],l2_T,l3_T,l4_T} ;
s11  = news ; Plane Surface(s11)      = {-ll10} ;
EndIf

ll12 = newll; Line Loop(ll12)         = {l3_T,l7_T,l6_T,l5_T} ;
s13  = news ; Plane Surface(s13)      = {ll12} ;

Transfinite Line{l3_T,l6_T}      = n2+1;
Transfinite Line{l2_T,l4_T} = n1_2+1;
//n1_1 = Floor(n1_1 * (d - eps_yy)/(eps_y - eps_yy));
Transfinite Line{l5_T,l7_T} = n1_1+1;
Transfinite Surface{s11,s13}; Recombine Surface{s11,s13} ;

inno1[] += {s11};
inno2[] += {s13};

EndIf
