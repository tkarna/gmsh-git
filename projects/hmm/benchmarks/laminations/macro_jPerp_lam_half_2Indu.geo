//Include "../reference_solutions/reference_solution_lam/VecA_Form/macro_jPerp_lam_half_2Indu.dat";

Include "../reference_solutions/reference_solution_lam/a-v_formulation/macro_jPerp_lam_half_2Indu.dat";

Geometry.OldNewReg = 1; // Use old newreg definition for geometrical transformations (compatibility option for old Gmsh geometries)

Mesh.Algorithm = 1 ; // 2D mesh algorithm (1=MeshAdapt, 2=Automatic, 5=Delaunay, 6=Frontal, 7=bamg, 8=delquad)
Mesh.CharacteristicLengthFactor = 1 ;

Flag_Transfinite = 1 ;

lclam            = 6 * L_X/160 ;
lclam2           = L_X/40  ; //finer
lcair            = R1/40 ; // inner circle
lcair2           = R2/40 ; // outer circle

nwl              = 16 ;//number of divisions along thickness of stack (with possible Bump)
//nll              = 10 * 3*Ceil[Lsrc/lclam] +1 ;
//nllm             = 3*Ceil[(L_X-2*Lsrc)/lclam] +1 ;

nll              = 6 ;
nllm             = 6 ;

fprog = 0.9 ;
fbump2 = 0.5 ; // near middle points

//============================================================
// Lamination stack center at (0,0)
// Corner of stack at (-L_X/2, L_Y/2, 0)
//============================================================

x0 = -L_X/2 ;
y0 = 0 ;
z0 = 0 ;

pp[] += newp; Point(pp[0]) = { x0,      y0,   z0, lclam};
pp[] += newp; Point(pp[1]) = { x0+Lsrc, y0,   z0, lclam2};
pp[] += newp; Point(pp[2]) = {-x0-Lsrc, y0,   z0, lclam2};
pp[] += newp; Point(pp[3]) = {-x0,      y0,   z0, lclam};

pp[] += newp; Point(pp[4]) = {-x0,      y0+L_Y/2, z0, lclam};
pp[] += newp; Point(pp[5]) = {-x0-Lsrc, y0+L_Y/2, z0, lclam2};
pp[] += newp; Point(pp[6]) = { x0+Lsrc, y0+L_Y/2, z0, lclam2};
pp[] += newp; Point(pp[7]) = { x0,      y0+L_Y/2, z0, lclam};


For k In {0:7}
  llam[] += newl ; Line(newl) = {pp[k],pp[((k==7)?0:k+1)]};
EndFor

lllam[] += newll ; Line Loop(newll) = {llam[]};
slam[]+=news ; Plane Surface(news) = lllam[0];

If(Flag_Transfinite)
Transfinite Line{llam[{3}], -llam[{7}]}   = nwl Using Progression fprog ;
  Transfinite Line{llam[{0}],-llam[{2}]}  = nll Using Bump fbump2 ;
  Transfinite Line{llam[{1}],llam[{5}]}   = nllm Using Bump fbump2 ;
  Transfinite Line{llam[{4}], -llam[{6}]} = nll Using Bump fbump2 ;
Transfinite Surface{slam[]}=pp[{0,3,4,7}]; Recombine Surface{slam[]};
EndIf

// ===================================================================
// Adding an inductor and air around
// airgap between inductor and laminations...
pgap[] += newp; Point(newp) = { x0+Lsrc, L_Y/2+dist_ind_lam, z0, lclam2};
pgap[] += newp; Point(newp) = {-x0-Lsrc, L_Y/2+dist_ind_lam, z0, lclam2};

// inductor
lcind = wind/4 ;
pi[] += newp; Point(newp) = { x0+Lsrc, L_Y/2+dist_ind_lam+wind, z0, lcind};
pi[] += newp; Point(newp) = {-x0-Lsrc, L_Y/2+dist_ind_lam+wind, z0, lcind};

//up
lind_[] += newl ; Line(newl) = {pgap[0],pgap[1]};
lind_[] += newl ; Line(newl) = {pgap[1],pi[1]};
lind_[] += newl ; Line(newl) = {pi[1],pi[0]};
lind_[] += newl ; Line(newl) = {pi[0],pgap[0]};

llind[] += newll ;  Line Loop(newll) = {lind_[]};
sind[]+=news; Plane Surface(news) = llind[];

Transfinite Line{lind_[{0}], lind_[{2}]} = 10;
Transfinite Line{lind_[{1}], lind_[{3}]} = 8;
Transfinite Surface{sind[]} = {pi[{0}],pi[{1}],pgap[{1}],pgap[{0}]}; Recombine Surface{sind[]};


// Defining the two lateral inductors
//===================================

// Right inductor
//=============== 
p1 = newp; Point(p1) = {0.5 * L_X + hgap          , vgap         , 0.0, lcind};
p2 = newp; Point(p2) = {0.5 * L_X + hgap+thickness, vgap         , 0.0, lcind};
p3 = newp; Point(p3) = {0.5 * L_X + hgap+thickness, vgap + height, 0.0, lcind};
p4 = newp; Point(p4) = {0.5 * L_X + hgap          , vgap + height, 0.0, lcind};

l1 = newl; Line(l1) = {p1, p2}; 
l2 = newl; Line(l2) = {p2, p3}; 
l3 = newl; Line(l3) = {p3, p4}; 
l4 = newl; Line(l4) = {p4, p1}; 

ll1 = newll; Line Loop(ll1) = {l1, l2, l3, l4};
s1 = news; Plane Surface(s1) = ll1;
Transfinite Line{l1, l3} = 10;
Transfinite Line{l2, l4} = 16;
Transfinite Surface{s1} = {p1, p2, p3, p4}; Recombine Surface{s1};

// Left inductor
//============== 
p5 = newp; Point(p5) = {-(0.5 * L_X + hgap+thickness), vgap         , 0.0, lcind};
p6 = newp; Point(p6) = {-(0.5 * L_X + hgap)          , vgap         , 0.0, lcind};
p7 = newp; Point(p7) = {-(0.5 * L_X + hgap)          , vgap + height, 0.0, lcind};
p8 = newp; Point(p8) = {-(0.5 * L_X + hgap+thickness), vgap + height, 0.0, lcind};

l5 = newl; Line(l5) = {p5, p6}; 
l6 = newl; Line(l6) = {p6, p7}; 
l7 = newl; Line(l7) = {p7, p8}; 
l8 = newl; Line(l8) = {p8, p5}; 

ll2 = newll; Line Loop(ll2) = {l5, l6, l7, l8};
s2 = news; Plane Surface(s2) = ll2;

Transfinite Line{l5, l7} = 10;
Transfinite Line{l6, l8} = 16;
Transfinite Surface{s2} = {p5, p6, p7, p8}; Recombine Surface{s2};


// Air around everything

cen = newp ; Point(cen) = { 0,  0, z0, lcair};

pa[] += newp; Point(pa[0]) = { R1,  0, z0, lcair};
pa[] += newp; Point(pa[1]) = { 0,  R1, z0, lcair};
pa[] += newp; Point(pa[2]) = {-R1, 0, z0, lcair};

For k In {0:1}
 lair[] += newl ; Circle(newl) = {pa[k],cen,pa[k+1]};
EndFor

laxis[] += newl ; Line(newl) = {pp[3],pa[0]};
laxis[] += newl ; Line(newl) = {pp[0],pa[2]};
lllair =newll; Line Loop(lllair) = {laxis[{0}], lair[{0}], lair[{1}], -laxis[{1}], -llam[{7}], -llam[{6}], -llam[{5}], -llam[{4}], -llam[{3}]};
//Plane Surface(news) = lllair;
//Line Loop(newll) = {19, 17, 18, -20, -8, -7, -6, -5, -4, -l1, -l2, -l3, -l4, -l5, -l6, -l7, -l8};
//sair = news ; Plane Surface(sair) = {llind[0], newll-1};
sair = news ; Plane Surface(sair) = {lllair, -llind[], -ll1, -ll2};


// air infinity
pa_[] += newp; Point(pa_[0]) = { R2,  0, z0, lcair2};
pa_[] += newp; Point(pa_[1]) = { 0,  R2, z0, lcair2};
pa_[] += newp; Point(pa_[2]) = {-R2, 0, z0, lcair2};

For k In {0:1}
 lair2[] += newl ; Circle(newl) = {pa_[k],cen,pa_[k+1]};
EndFor

laxis2[] += newl ; Line(newl) = {pa[0],pa_[0]};
laxis2[] += newl ; Line(newl) = {pa[2],pa_[2]};

llair2=newll; Line Loop(newll) = {laxis2[{0}], lair2[{0}], lair2[{1}], -laxis2[{1}], -lair[{1}], -lair[{0}]};
sair2 = news ; Plane Surface(sair2) = {llair2};

//================================================================================
// Physical regions
//================================================================================
GAMMA_INF  = 10;
CONDUCTOR  = 11;
AIR        = 12;
INDUCTOR1  = 13;
INDUCTOR2  = 14;
INDUCTOR_R = 15;
INDUCTOR_L = 16;


Physical Line(GAMMA_INF)       = {lair2[], llam[{0,1,2}],laxis[], laxis2[] };
//Physical Line(SYMMETRY)     = {llam[{0,1,2}],laxis[] };
Physical Surface(CONDUCTOR)   = {slam[]} ;
Physical Surface(AIR)         = {sair, sair2} ;
//Physical Surface(AIR_INF)   = {sair2} ;
Physical Surface(INDUCTOR1)   = {sind[]} ;
Physical Surface(INDUCTOR2)   = {} ;
Physical Surface(INDUCTOR_R)  = {s1}; 
Physical Surface(INDUCTOR_L)  = {s2}; 
