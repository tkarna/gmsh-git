Mesh.Algorithm = 1; // 2D mesh algorithm (1=MeshAdapt, 2=Automatic, 5=Delaunay, 6=Frontal, 7=bamg, 8=delquad)
Mesh.CharacteristicLengthFactor = 4 ;
Include "macro_jPerp_lam.dat";
s = 100;
lc2 = 0.2/s;
lc3 = 0.2/s;
Delta_1  = L_X/6;
Delta_78 = 0.5 * (1 - Delta_1);
p1 = newp; Point(p1) = {- 0.5 * L_X, - 0.5 * L_Y, 0.0,lc3};
p2 = newp; Point(p2) = {  0.5 * L_X, - 0.5 * L_Y, 0.0,lc3} ;
p3 = newp; Point(p3) = {  0.5 * L_X,   0.5 * L_Y, 0.0,lc3} ;
p4 = newp; Point(p4) = {- 0.5 * L_X,   0.5 * L_Y, 0.0,lc3} ;
l1 = newl; Line(l1) = {p1,p2} ;
l2 = newl; Line(l2) = {p2,p3} ;
l3 = newl; Line(l3) = {p3,p4} ;
l4 = newl; Line(l4) = {p4,p1} ;
ll5 = newll; Line Loop(ll5) = {l1,l2,l3,l4} ;
s6 = news; Plane Surface(s6) = {ll5} ;

fbumpX = 2.;
fbumpY = 0.05;

If(Flag_TransFinite)
Transfinite Line{l1,l3}      = NX Using Bump fbumpX;
Transfinite Line{l2,l4}     = NY Using Bump fbumpY;
Transfinite Surface{s6}      = {p1,p2,p3,p4}; Recombine Surface{s6} ;
EndIf

// Inductors (up)
//=================

p9  = newp; Point(p9)  = {0.5 *(- dx), 0.5 * L_Y + delta + dy   , 0.0, l_Indu};
p10 = newp; Point(p10) = {0.5 *(- dx), 0.5 * L_Y + delta        , 0.0, l_Indu};
p11 = newp; Point(p11) = {0.5 *(  dx), 0.5 * L_Y + delta        , 0.0, l_Indu};
p12 = newp; Point(p12) = {0.5 *(  dx), 0.5 * L_Y + delta + dy   , 0.0, l_Indu};

l9  = newl; Line(l9)  = {p9, p10};
l10 = newl; Line(l10) = {p10, p11};
l11 = newl; Line(l11) = {p11, p12};
l12 = newl; Line(l12) = {p12, p9};

ll6 = newll; Line Loop(ll6) = {l9, l10, l11, l12};
s7 = news; Plane Surface(s7) = {ll6};

Transfinite Line{l10,l12}     = nx;
Transfinite Line{l9 ,l11}     = ny;
Transfinite Surface{s7} = {p9,p10,p11,p12}; Recombine Surface{s7} ;

// Inductors (down)
//=================

p29 = newp; Point(p29) = {0.5 * (- dx), - 0.5 * L_Y - delta     , 0.0, l_Indu};
p30 = newp; Point(p30) = {0.5 * (- dx), - 0.5 * L_Y - delta - dy, 0.0, l_Indu};
p31 = newp; Point(p31) = {0.5 * (  dx), - 0.5 * L_Y - delta - dy, 0.0, l_Indu};
p32 = newp; Point(p32) = {0.5 * (  dx), - 0.5 * L_Y  - delta     , 0.0, l_Indu};

l29 = newl; Line(l29) = {p29,  p30};
l30 = newl; Line(l30) = {p30, p31};
l31 = newl; Line(l31) = {p31, p32};
l32 = newl; Line(l32) = {p32,  p29};

ll7 = newll; Line Loop(ll7) = {l29, l30, l31, l32};
s8  = news ; Plane Surface(s8) = {ll7};

Transfinite Line{l30,l32}     = nx;
Transfinite Line{l29,l31}     = ny;
Transfinite Surface{s8} = {p29,p30,p31,p32}; Recombine Surface{s8} ;


// Outer circle (C1)
//==================
lc_Circ1 /= 4;
p16 = newp; Point(p16) = {c_x,    c_y,    0.0};
p17 = newp; Point(p17) = {r+c_x,  c_y,    0.0, lc_Circ1};
p18 = newp; Point(p18) = {c_x,    r+c_y,  0.0, lc_Circ1};
p19 = newp; Point(p19) = {-r+c_x, c_y,    0.0, lc_Circ1};
p20 = newp; Point(p20) = {c_x,    -r+c_y, 0.0, lc_Circ1};

c1 = newl; Circle(c1) = {p17, p16, p18};
c2 = newl; Circle(c2) = {p18, p16, p19};
c3 = newl; Circle(c3) = {p19, p16, p20};
c4 = newl; Circle(c4) = {p20, p16, p17};

ll17 = newll; Line Loop(ll17) = {c1, c2, c3, c4} ;
s19 = news  ; Plane Surface(s19) = {ll17, ll5, ll6, ll7} ;

// Physical 
//=========
Physical Line(GAMMA_INF)    = {c1, c2, c3, c4} ;
Physical Surface(AIR)       = {s19} ;
Physical Surface(INDUCTOR1) = {s7} ;
Physical Surface(INDUCTOR2) = {s8} ;
Physical Surface(CONDUCTOR) = {s6} ;
