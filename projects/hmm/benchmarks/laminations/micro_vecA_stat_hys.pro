Include "micro.dat";
//Include "micro_exact.dat";

Group {
  GammaCorner = Region[ {GAMMA_CORNER } ];

  GammaLeft   = Region[ {GAMMA_LEFT } ];
  GammaRight  = Region[ {GAMMA_RIGHT} ];
  GammaUp     = Region[ {GAMMA_UP} ];
  GammaDown   = Region[ {GAMMA_DOWN} ];
  GammaDirichlet = Region[ {GAMMA_UP, GAMMA_DOWN} ];

  Omega_NL    = Region[ {IRON} ] ;
  Omega_L     = Region[ {AIR} ] ;
  Omega       = Region[ {Omega_NL, Omega_L} ] ;
}

Function {
  mu0 = 4.e-7 * Pi ;
  nu0 = 1/mu0 ;
  sigmaIron = 6e7;

  sigma[Omega_NL] = sigmaIron;
  nu[Omega_L] = nu0 ; 

  //Gyselink's paper: Incorporation of a Jiles-Atherton vector hysteresis model in 2D FE magnetic field computations
  //================================================================================================================
  Msat = 1145220; aaa = 59.5; kkk = 99.2; ccc = 0.54; alpha = 1.3e-4 ;
  hyst_FeSi = { Msat, aaa, kkk, ccc, alpha};

  Freq = 50. ; T= 1/Freq ; nbrT = 1 ; nbrstep = 120 ;
  t0 = 0 ; tmax = nbrT*T ; dt = T/nbrstep ; theta = 1.0;

  NbrMaxIter = 50; Eps = 1e-4; Relax = 1.0;

  Nbr_SubProblems = 3;
  epsilon = 1e-6;
  Pert~{1}[] = Vector[0, 0, 0];
  Pert~{2}[] = epsilon * Vector[1.0, 0.0, 0.0];
  Pert~{3}[] = epsilon * Vector[0.0, 1.0, 0.0];

  //currentTimeStep = 2;///
  //Nb_MicroProb = 76;
  ti = t0 + currentTimeStep * dt ;
  Printf("=============> step = %g ti = %g lx = %g exactMicroGeo = %g Nb_MicroProb = %g", currentTimeStep, ti, lx, exactMicroGeo, Nb_MicroProb);

  a_macro[]     = Vector[ 0., 0., ScalarField[XYZ[]]{0}] ;
  a_tprevious[] = (currentTimeStep==0) ? Vector[0.,0.,0.] : Vector[ 0., 0., ScalarField[XYZ[]]{1}] ;
  h_tprevious[] = (currentTimeStep==0) ? Vector[0.,0.,0.] : VectorField[XYZ[]]{2} ;
}

Constraint {
 { Name a_Micro ;
   Case {
     { Region GammaDirichlet ; Value 0. ; }
     { Region GammaRight ; Type Link ; RegionRef GammaLeft ;
       Coefficient 1. ; Function Vector[$X-lx, $Y, $Z] ; }
   }
 }
}

Jacobian {
  { Name Vol ; Case { { Region All ; Jacobian Vol ; } } }
  { Name Sur ; Case { { Region All ; Jacobian Sur ; } } }
}

Integration {
  { Name II ; Case { 
      {Type Gauss ; 
        Case {
	  { GeoElement Line        ; NumberOfPoints  4 ; }
          { GeoElement Triangle    ; NumberOfPoints  4 ; }
          { GeoElement Quadrangle  ; NumberOfPoints  4 ; } 
        } 
      } 
    }
  }
}


FunctionSpace{
  { Name HCurl_a_Proj ; Type Form1P ;
    BasisFunction{
      { Name sn; NameOfCoef an; Function BF_PerpendicularEdge; Support Omega; Entity NodesOf[ All ]; }
    }
  }
  { Name HCurl_a_Micro ; Type Form1P ;
    BasisFunction{
      { Name sn; NameOfCoef an; Function BF_PerpendicularEdge; Support Omega; Entity NodesOf[ All ]; }
      { Name sf; NameOfCoef af; Function BF_RegionZ; Support Omega; Entity Omega ; }
    }
    GlobalQuantity{
      { Name Af_ct ; Type AliasOf ; NameOfCoef af ; }
    }
    SubSpace{
      { Name An ; NameOfBasisFunction {sn} ; } //fluctuations au niveau de la microstructure
      { Name Af ; NameOfBasisFunction {sf} ; } //composante globale du potentiel
    }
    Constraint {
      { NameOfCoef an; EntityType NodesOf; NameOfConstraint a_Micro; }
    }
  }
  { Name H_hysteresis; Type Form1 ;
    BasisFunction {
      { Name sex; NameOfCoef aex; Function BF_VolumeX; Support Omega; Entity VolumesOf[ Omega ]; }
      { Name sey; NameOfCoef aey; Function BF_VolumeY; Support Omega; Entity VolumesOf[ Omega ]; }
      { Name sez; NameOfCoef aez; Function BF_VolumeZ; Support Omega; Entity VolumesOf[ Omega ]; }
    }
  }

  { Name HCurl_a_Micro_t0 ; Type Form1P ;
    BasisFunction{
      { Name sn; NameOfCoef an; Function BF_PerpendicularEdge; Support Omega; Entity NodesOf[ All ]; }
    }
  }
  { Name H_hysteresis_t0 ; Type Vector;
    BasisFunction {
      { Name sex ; NameOfCoef aex ; Function BF_VolumeX ; Support Omega ; Entity VolumesOf[ Omega ] ; }
      { Name sey ; NameOfCoef aey ; Function BF_VolumeY ; Support Omega ; Entity VolumesOf[ Omega ] ; }
      { Name sez ; NameOfCoef aez ; Function BF_VolumeZ ; Support Omega ; Entity VolumesOf[ Omega ] ; }
    }
  }
}

Formulation {
  { Name Proj_Macro ; Type FemEquation ;
    Quantity{
      { Name ap ; Type Local ; NameOfSpace HCurl_a_Proj ; }
    }
    Equation{
      Galerkin { [    Dof{ap}, {ap} ]; In Omega; Jacobian Vol; Integration II; }
      Galerkin { [ -a_macro[], {ap} ]; In Omega; Jacobian Vol; Integration II; }
    }
  }
  { Name Init_PreviousTimeStep ; Type FemEquation ;
    Quantity{
      { Name a  ; Type Local  ; NameOfSpace HCurl_a_Micro_t0 ; }
      { Name h  ; Type Local  ; NameOfSpace H_hysteresis_t0 ; }
    }
    Equation{
      Galerkin { [     Dof{a} , {a} ]   ; In Omega; Jacobian Vol; Integration II; }
      Galerkin { [ -a_tprevious[], {a} ]; In Omega; Jacobian Vol; Integration II; }
      Galerkin { [     Dof{h} , {h} ]   ; In Omega; Jacobian Vol; Integration II; }
      Galerkin { [ -h_tprevious[], {h} ]; In Omega; Jacobian Vol; Integration II; }
    }
  }

  For iP In {1:Nbr_SubProblems}
  { Name a_NR~{iP} ; Type FemEquation ;
    Quantity {
      { Name ap ; Type Local ; NameOfSpace HCurl_a_Proj ; }

      { Name a  ; Type Local ; NameOfSpace HCurl_a_Micro ; }
      { Name an ; Type Local ; NameOfSpace HCurl_a_Micro[An] ; }
      { Name af ; Type Local ; NameOfSpace HCurl_a_Micro[Af] ; }
      { Name h ; Type Local  ; NameOfSpace H_hysteresis ; }

      { Name a0 ; Type Local ; NameOfSpace HCurl_a_Micro_t0 ; } 
      { Name h0 ; Type Local ; NameOfSpace H_hysteresis_t0 ; } 
    }
    Equation {
      Galerkin { [ nu[] * Dof{d a} , {d a} ]   ; In Omega_L; Jacobian Vol; Integration II; }
      Galerkin { [ nu[] * {d ap}, {d a} ]      ; In Omega_L; Jacobian Vol; Integration II; }
      Galerkin { [ nu[] * Pert~{iP}[] , {d a} ]; In Omega_L; Jacobian Vol; Integration II; }
      Galerkin { [ h_Jiles[{h0}, {d a0}, {d a}+{d ap}+Pert~{iP}[] ]{List[hyst_FeSi]}#4 , {d a} ]; 
        In Omega_NL; Jacobian Vol; Integration II; }
      Galerkin { JacNL[ dhdb_Jiles[ {h}, {d a}+{d ap}+Pert~{iP}[], {h}-{h0} ]{List[hyst_FeSi]} * Dof{d a} , {d a} ];
        In Omega_NL ; Jacobian Vol ; Integration II ; }
      Galerkin { JacNL[ dhdb_Jiles[ {h}, {d a}+{d ap}+Pert~{iP}[], {h}-{h0} ]{List[hyst_FeSi]} * {d ap} , {d a} ];
        In Omega_NL ; Jacobian Vol ; Integration II ; }
      Galerkin { JacNL[ dhdb_Jiles[ {h}, {d a}+{d ap}+Pert~{iP}[], {h}-{h0} ]{List[hyst_FeSi]} * Pert~{iP}[], {d a} ];
        In Omega_NL ; Jacobian Vol ; Integration II ; }
      Galerkin { [ Dof{h}, {h} ]; In Omega_NL ; Jacobian Vol ; Integration II ; }
      Galerkin { [   -#4 , {h} ]; In Omega_NL ; Jacobian Vol ; Integration II ; }
      Galerkin { [ Dof{a} , {af} ]; In Omega; Jacobian Vol; Integration II; }
    }
  }
  EndFor
    }

Resolution {
  { Name a_NR ;
    System {
      { Name PMacro ; NameOfFormulation Proj_Macro ; } // Data coming from Macro problem
      { Name AH     ; NameOfFormulation Init_PreviousTimeStep ; }
      For iP In {1:Nbr_SubProblems}
        { Name Micro~{iP} ; NameOfFormulation a_NR~{iP} ;}
      EndFor
    }
    Operation {
      SetTime[ti];
      GmshRead["output/a_CurrentTimeStep_0.pos"] ;
      Generate[PMacro] ; Solve[PMacro] ; SaveSolution[PMacro];

      If(currentTimeStep == 0)
        InitSolution[AH] ;
      EndIf
      If(currentTimeStep != 0)
        GmshRead[Sprintf("output/microproblems/a_tot_p1_cs%g_gp%g.pos", currentTimeStep-1, numEle)];
        GmshRead[Sprintf("output/microproblems/h_tot_p1_cs%g_gp%g.pos", currentTimeStep-1, numEle)];
        Generate[AH]; Solve[AH]; SaveSolution[AH];
      EndIf
      For iP In {1:Nbr_SubProblems}
      TimeLoopTheta[ ti, ti+dt, dt, 1.]{
        IterativeLoop[NbrMaxIter, Eps, Relax]{
          GenerateJac[Micro~{iP}] ; SolveJac[Micro~{iP}] ;
        }
      }
      SaveSolution[Micro~{iP}];
      If(iP == 1)
        PostOperation[map_field_1];
      EndIf
      If(exactMicroGeo && (iP == 1) )
        PostOperation[cuts_field_1];
      EndIf
      EndFor
    }
  }
}

PostProcessing {
  For iP In {1:Nbr_SubProblems}
  { Name a_Micro_NR~{iP} ; NameOfFormulation a_NR~{iP} ; NameOfSystem Micro~{iP} ;
     PostQuantity {
       { Name vol         ; Value { Integral { [ 1. ] ;In Omega ; Jacobian Vol ; Integration II ; } } }  // stored in register #12
       { Name a           ; Value { Term { [ CompZ[ {a} ] ] ; In Omega ; Jacobian Vol; } } }
       { Name a_proj      ; Value { Term { [ CompZ[ {ap} ] ] ;  In Omega ; Jacobian Vol;  } } }
       { Name a_tot       ; Value { Term { [ CompZ[ {a} +{ap} ] ] ; In Omega ; Jacobian Vol; } } }
       { Name b           ; Value { Term { [ {d a} ] ; In Omega ; Jacobian Vol;  } } }
       { Name b_proj      ; Value { Term { [ {d ap} ] ; In Omega ; Jacobian Vol;  } } }
       { Name b_tot       ; Value { Term { [ {d a}+{d ap}+Pert~{iP}[] ] ; In Omega ; Jacobian Vol ; } } }
       { Name b_tot_mean; Value { Integral { [ ({d a}+{d ap}+Pert~{iP}[])/#12 ] ; In Omega ; Jacobian Vol ; Integration II ; } } }
       { Name h_tot ; Value {
           Term { [ nu[] * ({d a} + {d ap}+ Pert~{iP}[]) ]; In Omega_L; Jacobian Vol;}
           Term {  [ {h} ]; In Omega_NL;  Jacobian Vol; } } }
       { Name h ; Value { Term {  [ {h} ]; In Omega_NL; Jacobian Vol; } } }
       { Name h_tot_mean ; Value {// stored in #22
           Integral { [ nu[] * ({d a} + {d ap}+ Pert~{iP}[])/#12 ] ; In Omega_L ; Jacobian Vol; Integration II ; }
           Integral {  [ {h}/#12 ]   ; In Omega_NL ;  Jacobian Vol; Integration II ; } } }
     }
   }
  EndFor
}

If(exactMicroGeo)
nameDirRes = "output/micro_exact/";
EndIf
If(!exactMicroGeo)
nameDirRes = "output/";
EndIf

PostOperation {
For iP In {1:Nbr_SubProblems}
 { Name mean~{iP} ; NameOfPostProcessing a_Micro_NR~{iP};
   Operation{
     Print[ vol[Omega]       , OnGlobal, Format Table, Store 12, File StrCat[nameDirRes, "vol.txt"], LastTimeStepOnly ] ;
     Print[ h_tot_mean[Omega], OnGlobal, Format Table, Store 22, File StrCat[nameDirRes, Sprintf("j%g.txt", iP) ], LastTimeStepOnly ] ;
     Print[ b_tot_mean[Omega], OnGlobal, Format Table, Store 21, File StrCat[nameDirRes, Sprintf("e%g.txt", iP) ], LastTimeStepOnly ] ;
   }
 }
 { Name cuts_field~{iP} ; NameOfPostProcessing a_Micro_NR~{iP};
   Operation{
     Print[ a,      OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("a_pert%g.pos", iP) ], LastTimeStepOnly ];
     Print[ a_proj, OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("a_proj%g.pos", iP) ], LastTimeStepOnly ];
     Print[ a_tot,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("a_tot%g.pos", iP) ] , LastTimeStepOnly ];
     Print[ b_tot,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("b_tot%g.pos", iP) ] , LastTimeStepOnly ];
     Print[ h_tot,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("h_tot%g.pos", iP) ] , LastTimeStepOnly ];
   }
 }
 { Name map_field~{iP} ; NameOfPostProcessing a_Micro_NR~{iP};
   Operation{
     Print[ a_tot,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("microproblems/a_tot_p%g_cs%g_gp%g.pos" , iP, currentTimeStep, numEle) ], Format Gmsh, LastTimeStepOnly ];
     Print[ b_tot,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("microproblems/b_tot_p%g_cs%g_gp%g.pos" , iP, currentTimeStep, numEle) ], Format Gmsh, LastTimeStepOnly ];
     Print[ h_tot,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("microproblems/h_tot_p%g_cs%g_gp%g.pos" , iP, currentTimeStep, numEle) ], Format Gmsh, LastTimeStepOnly ];
   }
 }
 { Name map_field_all~{iP} ; NameOfPostProcessing a_Micro_NR~{iP};
   Operation {
     Print[ a_tot,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("microproblems/a_tot_p%g_s%g.pos" , iP, currentTimeStep) ], Format Gmsh, LastTimeStepOnly ];
     Print[ b_tot,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("microproblems/b_tot_p%g_s%g.pos" , iP, currentTimeStep) ], Format Gmsh, LastTimeStepOnly ];
     Print[ h_tot,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("microproblems/h_tot_p%g_s%g.pos" , iP, currentTimeStep) ], Format Gmsh, LastTimeStepOnly ];
   }
 }
 EndFor
}
