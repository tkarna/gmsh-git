Include "micro_CondInsuCond.dat";

Group {
  GammaLeft   = Region[ {GAMMA_LEFT } ];
  GammaRight  = Region[ {GAMMA_RIGHT} ];
  GammaDown   = Region[ {GAMMA_DOWN } ];
  GammaUp     = Region[ {GAMMA_UP   } ];

  Omega       = Region[ {IRON, AIR} ] ;
  Omega_NL    = Region[ {IRON} ] ;
  Omega_L     = Region[ {AIR} ] ;
  Omega_C     = Region[ {IRON} ] ;
  Omega_CC    = Region[ {AIR} ] ;
}

Function {
  NbrMaxIter = 100; Eps = 1e-4; Relax = 1.0;
  a_interpolation[]  = Vector[ 0., 0., ScalarField[XYZ[]] ];
  mu0 = 4.e-7 * Pi ; murIron = 10; muIron = mu0 * murIron ;
  mu_1 = muIron; mu_2 = mu0;
  nu_1 = 1.0/mu_1;  nu_2 = 1.0/mu_2;
  Freq = 50. ; T= 1/Freq ; nbrT = 1 ; nbrstep = 20 ;
  t0 = 0 ; tmax = nbrT*T ; dt = T/nbrstep ; theta = 1.0;
  
  sigmaIron = 2.7e3; sigma[Omega_C] = sigmaIron;

  //Parameters of the Brauer nonlinear constitutive law
  gamma = 388.; alpha = 0.3774; beta = 2.97;
  nu_a[] = gamma + alpha * Exp[beta*$1] ;
  dnudb2_a[] = alpha * beta* Exp[beta*$1] ;

  Nbr_SubProblems = 3; epsilon = 1e-6;
  Pert~{1}[] = Vector[0, 0, 0];
  Pert~{2}[] = epsilon * Vector[1.0, 0.0, 0.0];
  Pert~{3}[] = epsilon * Vector[0.0, 1.0, 0.0];

  Flag_NL = 0;

  nu_sat = gamma + alpha * Exp[beta * 1.29 * 1.29] ;
  mur_sat = 1/(mu0*nu_sat) ;   mur = mur_sat ;
  Printf("nu with saturation %g mur %g", gamma + alpha * Exp[beta * 1.29 * 1.29], mur_sat) ;
  nu[Omega_L] = 1. * nu_sat;
  If(!Flag_NL)
    nu[Omega_NL] = 1. * nu_sat ;
  dhdb_NL[Omega] = TensorDiag[0., 0., 0.] ;
  EndIf
    If(Flag_NL)
    nu[Omega_NL]      = nu_a[SquNorm[$1]] ;
  dhdb_NL[Omega_NL] = 2*dnudb2_a[SquNorm[$1]] * SquDyadicProduct[$1] ;
  EndIf
    }

Constraint {
  { Name a_Micro ;
    Case {
      { Region GammaDown ; Value 0. ; }
      { Region GammaUp    ; Value 0. ; }
      { Region GammaRight ; Type Link ; RegionRef GammaLeft ;
        Coefficient 1. ; Function Vector[$X-lx, $Y, $Z] ; }
    }
  }
}

Jacobian {
  { Name Vol ;
    Case  {
        { Region All ; Jacobian Vol ; }
    }
  }
  { Name Sur ;
    Case {
      { Region All ; Jacobian Sur ; }
    }
  }
}

Integration {
  { Name II ; Case { {Type Gauss ; Case {
	  { GeoElement Line        ; NumberOfPoints  4 ; }
          { GeoElement Triangle    ; NumberOfPoints  4 ; }
          { GeoElement Quadrangle  ; NumberOfPoints  4 ; }
          { GeoElement Tetrahedron ; NumberOfPoints  4 ; }
          { GeoElement Hexahedron  ; NumberOfPoints  6 ; }
          { GeoElement Prism       ; NumberOfPoints  9 ; } }
      }
    }
  }
}

FunctionSpace{
  
  { Name Hgrad_a_Proj ; Type Form1P ;
    BasisFunction{
      { Name sn ; NameOfCoef an ; Function BF_PerpendicularEdge ;
        Support Omega ; Entity NodesOf[ All ] ; }
    }
  }

  { Name Hgrad_a_Micro ; Type Form1P ;
    BasisFunction{
      { Name sn ; NameOfCoef an ; Function BF_PerpendicularEdge ;
        Support Omega ; Entity NodesOf[ All ] ; }
      { Name sf ; NameOfCoef af ; Function BF_RegionZ ;
        Support Omega ; Entity Omega ; }
    }
    GlobalQuantity{
      { Name Af_ct ; Type AliasOf ; NameOfCoef af ; }
    }
    SubSpace{
      { Name An ; NameOfBasisFunction {sn} ; } //fluctuations au niveau de la microstructure
      { Name Af ; NameOfBasisFunction {sf} ; } //composante globale du potentiel
    }
    Constraint{
      { NameOfCoef an ; EntityType NodesOf ; NameOfConstraint a_Micro ; }
    }
  }
}

Formulation {
  { Name a_Proj ; Type FemEquation ;
    Quantity
      {
        { Name ap ; Type Local ; NameOfSpace Hgrad_a_Proj ; }
      }
    Equation{
        Galerkin { [ Dof{ap} , {ap} ] ;
          In Omega ; Jacobian Vol ; Integration II ; }
        Galerkin { [ - a_interpolation[], {ap} ] ;
          In Omega ; Jacobian Vol ; Integration II ; }
      }
  }

  For iSub In {1:Nbr_SubProblems}
  { Name a_NR~{iSub} ; Type FemEquation ;
    Quantity {
      { Name a  ; Type Local ; NameOfSpace Hgrad_a_Micro ; }
      { Name an ; Type Local ; NameOfSpace Hgrad_a_Micro[An] ; }
      { Name af ; Type Local ; NameOfSpace Hgrad_a_Micro[Af] ; }
      { Name ap ; Type Local ; NameOfSpace Hgrad_a_Proj; }
    }
    Equation {
      Galerkin { [ nu[ {d a}+{d ap}+Pert~{iSub}[] ] * Dof{d a} , {d a} ] ;
        In Omega ; Jacobian Vol ; Integration II ; }
      Galerkin { [ nu[ {d a}+{d ap}+Pert~{iSub}[] ] * {d ap}, {d a} ] ;
        In Omega ; Jacobian Vol ; Integration II ; }
      Galerkin { [ nu[ {d a}+{d ap}+Pert~{iSub}[] ] * Pert~{iSub}[] , {d a} ] ;
        In Omega ; Jacobian Vol ; Integration II ; }
      Galerkin { JacNL[ dhdb_NL[{d a}+{d ap}+Pert~{iSub}[] ] * Dof{d a}, {d a} ] ;
        In Omega_NL ; Jacobian Vol ; Integration II ; }
      Galerkin { [ Dof{a} , {af} ] ;
        In Omega ; Jacobian Vol ; Integration II ; }
    }
  }

  { Name a_Dyn_NR~{iSub} ; Type FemEquation ;
    Quantity {
      { Name a  ; Type Local ; NameOfSpace Hgrad_a_Micro ; }
      { Name an ; Type Local ; NameOfSpace Hgrad_a_Micro[An] ; }
      { Name af ; Type Local ; NameOfSpace Hgrad_a_Micro[Af] ; }
      { Name ap ; Type Local ; NameOfSpace Hgrad_a_Proj; }
    }
    Equation {
      Galerkin { [ nu[ {d a}+{d ap}+Pert~{iSub}[{d a}+{d ap}] ] * Dof{d a} , {d a} ] ;
        In Omega ; Jacobian Vol ; Integration II ; }
      Galerkin { [ nu[ {d a}+{d ap}+Pert~{iSub}[{d a}+{d ap}] ] * {d ap}, {d a} ] ;
        In Omega ; Jacobian Vol ; Integration II ; }
      Galerkin { [ nu[ {d a}+{d ap}+Pert~{iSub}[{d a}+{d ap}] ] * Pert~{iSub}[{d a}+{d ap}] , {d a} ] ;
        In Omega ; Jacobian Vol ; Integration II ; }
      Galerkin { DtDof[ sigma[] * Dof{a} , {a} ] ;
        In Omega_C ; Jacobian Vol ; Integration II ; }
      Galerkin { DtDof[ sigma[] * {ap} , {a} ] ;
        In Omega_C ; Jacobian Vol ; Integration II ; }
      Galerkin { JacNL[ dhdb_NL[{d a}+{d ap}+Pert~{iSub}[{d a}+{d ap}] ] * Dof{d a}, {d a} ] ;
        In Omega_NL ; Jacobian Vol ; Integration II ; }
      Galerkin { [ Dof{a} , {af} ] ;
        In Omega ; Jacobian Vol ; Integration II ; }
    }
  }
  EndFor
    }

Resolution {
  { Name a_NR ;
    System {
      { Name P ; NameOfFormulation a_Proj         ;}
      For iSub In {1:Nbr_SubProblems}
      { Name Micro~{iSub} ; NameOfFormulation a_NR~{iSub} ;}
      EndFor
        }
    Operation {
      GmshRead["output/a_0.pos"];
      //GmshRead["output/a_p1_s0.004.pos"];
      Generate [P] ; Solve [P] ; SaveSolution [P] ;
      For iSub In {1:Nbr_SubProblems}
      InitSolution[Micro~{iSub}];
      IterativeLoop[NbrMaxIter, Eps, Relax]{
        GenerateJac[Micro~{iSub}] ; SolveJac[Micro~{iSub}] ;
      }
      SaveSolution[Micro~{iSub}];
      EndFor
        }
  }
  { Name a_Dyn_NR ;
    System {
      { Name P ; NameOfFormulation a_Proj         ;}
      For iSub In {1:Nbr_SubProblems}
      { Name Micro~{iSub} ; NameOfFormulation a_Dyn_NR~{iSub} ;}
      EndFor
        }
    Operation {
      GmshRead["output/a_0.pos"];
      Generate [P] ; Solve [P] ; SaveSolution [P] ;
      For iSub In {1:Nbr_SubProblems}
      InitSolution[Micro~{iSub}];
      TimeLoopTheta[t0, tmax, dt, theta]{
        IterativeLoop[NbrMaxIter, Eps, Relax]{
          GenerateJac[Micro~{iSub}] ; SolveJac[Micro~{iSub}] ;
        }
        SaveSolution[Micro~{iSub}];
      }
      EndFor
        }
  }
}

PostProcessing {
  For iSub In {1:Nbr_SubProblems}
  { Name a_Micro_NR~{iSub} ; NameOfFormulation a_NR~{iSub} ;
    PostQuantity{
      { Name vol         ; Value { Integral { [ 1. ] ;In Omega ; Jacobian Vol ; Integration II ; } } }  // stored in register #12
      { Name a           ; Value { Term { [ CompZ[ {a} ] ] ; In Omega ; Jacobian Vol; } } }
      { Name a_proj      ; Value { Term { [ CompZ[ {ap} ] ] ;  In Omega ; Jacobian Vol;  } } }
      { Name a_tot       ; Value { 
          Term { [ CompZ[ {a} ] ] ; In Omega ; Jacobian Vol; }
          Term { [ CompZ[ {ap} ] ] ; In Omega ; Jacobian Vol;  } } }
      { Name b           ; Value { Term { [ {d a} ] ; In Omega ; Jacobian Vol;  } } }
      { Name b_proj      ; Value { Term { [ {d ap} ] ; In Omega ; Jacobian Vol;  } } }
      { Name b_tot       ; Value {
          Term { [ {d a}+{d ap}+Pert~{iSub}[+{d a}+{d ap} ] ] ; In Omega ; Jacobian Vol ; Integration II ; } } }
      { Name  b_tot_mean; Value {
          Integral { [ ( {d a}+{d ap}+Pert~{iSub}[+{d a}+{d ap} ] ) / #12 ] ; In Omega ; Jacobian Vol ; Integration II ; } } }
      { Name h_tot ; Value  {// stored in #22
          Term { [ nu[ {d a}+{d ap}+Pert~{iSub}[+{d a}+{d ap}] ] * {d a}  ] ; In Omega ; Jacobian Vol ;}
          Term { [ nu[ {d a}+{d ap}+Pert~{iSub}[+{d a}+{d ap}] ] * {d ap} ] ; In Omega ; Jacobian Vol ;}
          Term { [ nu[ {d a}+{d ap}+Pert~{iSub}[+{d a}+{d ap}] ] * Pert~{iSub}[+{d a}+{d ap} ] ] ; In Omega ; Jacobian Vol ;} } }
      { Name h_tot_mean ; Value {// stored in #22
          Integral { [ ( nu[{d a}+{d ap}+Pert~{iSub}[+{d a}+{d ap}] ] * {d a} )/#12 ] ; In Omega ; Jacobian Vol ; Integration II ;}
          Integral { [ ( nu[{d a}+{d ap}+Pert~{iSub}[+{d a}+{d ap}] ] * {d ap} )/#12 ] ; In Omega ; Jacobian Vol ; Integration II ;}
          Integral { [ ( nu[{d a}+{d ap}+Pert~{iSub}[+{d a}+{d ap}] ] * Pert~{iSub}[+{d a}+{d ap} ] )/#12 ] ; In Omega ; Jacobian Vol ; Integration II ;} } }
    }
  }
  { Name a_Dyn_Micro_NR~{iSub} ; NameOfFormulation a_Dyn_NR~{iSub} ;
    PostQuantity{
      { Name vol         ; Value { Integral { [ 1. ] ;In Omega ; Jacobian Vol ; Integration II ; } } }  // stored in register #12
      { Name a           ; Value { Term { [ CompZ[ {a} ] ] ; In Omega ; Jacobian Vol; } } }
      { Name a_proj      ; Value { Term { [ CompZ[ {ap} ] ] ;  In Omega ; Jacobian Vol;  } } }
      { Name a_tot       ; Value { 
          Term { [ CompZ[ {a} ] ] ; In Omega ; Jacobian Vol; }
          Term { [ CompZ[ {ap} ] ] ; In Omega ; Jacobian Vol;  } } }
      { Name b           ; Value { Term { [ {d a} ] ; In Omega ; Jacobian Vol;  } } }
      { Name b_proj      ; Value { Term { [ {d ap} ] ; In Omega ; Jacobian Vol;  } } }
      { Name b_tot       ; Value {
          Term { [ {d a}+{d ap}+Pert~{iSub}[+{d a}+{d ap} ] ] ; In Omega ; Jacobian Vol ; Integration II ; } } }
      { Name  b_tot_mean; Value {
          Integral { [ ( {d a}+{d ap}+Pert~{iSub}[+{d a}+{d ap} ] ) / #12 ] ; In Omega ; Jacobian Vol ; Integration II ; } } }
      { Name h_tot ; Value  {// stored in #22
          Term { [ nu[ {d a}+{d ap}+Pert~{iSub}[+{d a}+{d ap}] ] * {d a}  ] ; In Omega ; Jacobian Vol ;}
          Term { [ nu[ {d a}+{d ap}+Pert~{iSub}[+{d a}+{d ap}] ] * {d ap} ] ; In Omega ; Jacobian Vol ;}
          Term { [ nu[ {d a}+{d ap}+Pert~{iSub}[+{d a}+{d ap}] ] * Pert~{iSub}[+{d a}+{d ap} ] ] ; In Omega ; Jacobian Vol ;} } }
      { Name h_tot_mean ; Value {// stored in #22
          Integral { [ ( nu[{d a}+{d ap}+Pert~{iSub}[+{d a}+{d ap}] ] * {d a} )/#12 ] ; In Omega ; Jacobian Vol ; Integration II ;}
          Integral { [ ( nu[{d a}+{d ap}+Pert~{iSub}[+{d a}+{d ap}] ] * {d ap} )/#12 ] ; In Omega ; Jacobian Vol ; Integration II ;}
          Integral { [ ( nu[{d a}+{d ap}+Pert~{iSub}[+{d a}+{d ap}] ] * Pert~{iSub}[+{d a}+{d ap} ] )/#12 ] ; In Omega ; Jacobian Vol ; Integration II ;} } }
      { Name j_tot ; Value  {// stored in #22
          Term { [ sigma[] * {d a}  ]                      ; In Omega_C ; Jacobian Vol ;}
          Term { [ sigma[] * {d ap} ]                      ; In Omega_C ; Jacobian Vol ;}
          Term { [ sigma[] * Pert~{iSub}[+{d a}+{d ap} ] ] ; In Omega_C ; Jacobian Vol ;} } }
    }
  }
  EndFor
    }

nameDirRes = "output/";
nameDirRes_ref = "output/";

For iSub In {1:Nbr_SubProblems}
fileExt~{iSub} = Str[ Sprintf("%g", iSub) ];
fileExt_txt~{iSub} = StrCat[fileExt~{iSub}, ".txt"];
fileExt_pos~{iSub} = StrCat[fileExt~{iSub}, ".pos"];
EndFor

PostOperation {
  For iSub In {1:Nbr_SubProblems}

  { Name mean~{iSub} ; NameOfPostProcessing a_Micro_NR~{iSub};
    Operation{
      Print[ vol[Omega], OnGlobal, Format Table, Store 12, File "output/vol.txt" ] ;
      Print[ h_tot_mean[Omega], OnGlobal, Format Table, Store 22, File StrCat[StrCat[nameDirRes, "j"]     , fileExt_txt~{iSub}] ] ;
      Print[ b_tot_mean[Omega], OnGlobal, Format Table, Store 21, File StrCat[StrCat[nameDirRes, "e"]     , fileExt_txt~{iSub}] ] ;
    }
  }

  { Name mean_Dyn~{iSub} ; NameOfPostProcessing a_Dyn_Micro_NR~{iSub};
    Operation{
      Print[ vol[Omega], OnGlobal, Format Table, Store 12, File "output/vol.txt" ] ;
      Print[ h_tot_mean[Omega], OnGlobal, Format Table, Store 22, File StrCat[StrCat[nameDirRes, "j"]     , fileExt_txt~{iSub}] ] ;
      Print[ b_tot_mean[Omega], OnGlobal, Format Table, Store 21, File StrCat[StrCat[nameDirRes, "e"]     , fileExt_txt~{iSub}] ] ;
    }
  }

 EndFor

 For iSub In {1:Nbr_SubProblems}

 { Name cuts_field~{iSub} ; NameOfPostProcessing a_Micro_NR~{iSub};
   Operation{
     Print[ a, OnElementsOf Omega, File StrCat[StrCat[nameDirRes_ref, "v_pert"], fileExt_pos~{iSub}] ];
     Print[ a_proj, OnElementsOf Omega, File StrCat[StrCat[nameDirRes_ref, "v_proj"], fileExt_pos~{iSub}] ];
     Print[ a_tot, OnElementsOf Omega, File StrCat[StrCat[nameDirRes_ref, "v_tot"], fileExt_pos~{iSub}] ];
     Print[ b_tot, OnElementsOf Omega, File StrCat[StrCat[nameDirRes_ref, "e_tot"], fileExt_pos~{iSub}] ];
     Print[ h_tot, OnElementsOf Omega, File StrCat[StrCat[nameDirRes_ref, "j_tot"], fileExt_pos~{iSub}] ];
   }
 }

 { Name cuts_field_Dyn~{iSub} ; NameOfPostProcessing a_Dyn_Micro_NR~{iSub};
   Operation{
     Print[ a, OnElementsOf Omega, File StrCat[StrCat[nameDirRes_ref, "a_pert"], fileExt_pos~{iSub}] ];
     Print[ a_proj, OnElementsOf Omega, File StrCat[StrCat[nameDirRes_ref, "a_proj"], fileExt_pos~{iSub}] ];
     Print[ a_tot, OnElementsOf Omega, File StrCat[StrCat[nameDirRes_ref, "a_tot"], fileExt_pos~{iSub}] ];
     Print[ b_tot, OnElementsOf Omega, File StrCat[StrCat[nameDirRes_ref, "b_tot"], fileExt_pos~{iSub}] ];
     Print[ h_tot, OnElementsOf Omega, File StrCat[StrCat[nameDirRes_ref, "h_tot"], fileExt_pos~{iSub}] ];
     Print[ j_tot, OnElementsOf Omega, File StrCat[StrCat[nameDirRes_ref, "j_tot"], fileExt_pos~{iSub}] ];
   }
 }

 { Name map_field~{iSub} ; NameOfPostProcessing a_Micro_NR~{iSub};
   Operation{
     Print[ a_tot, OnElementsOf Omega, File > Sprintf("output/a_p%g_s%g.pos", iSub, currentTimeStep),  Format Gmsh  ];
     Print[ h_tot, OnElementsOf Omega, File > Sprintf("output/h_p%g_s%g.pos", iSub, currentTimeStep),  Format Gmsh  ];
   }
 }
 EndFor
   }
