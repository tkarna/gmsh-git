Include "micro.dat";

// Transfinite mesh data
n1 = 5;// * ratio_Ll ;// 
n2 = 2;
n3 = 30;

fbumpX = 1;
fbumpY = 0.15;
d_i = e - d;


// micro geometry centered at the gauss point
d_i = e - d;
p1 = newp; Point(p1) = {x_gauss - e/2 , y_gauss - e/2, 0.0};
p2 = newp; Point(p2) = {x_gauss       , y_gauss - e/2, 0.0};
p3 = newp; Point(p3) = {x_gauss + e/2 , y_gauss - e/2, 0.0};

p4 = newp; Point(p4) = {x_gauss - e/2 , y_gauss - d/2 - d_i/4, 0.0};
p5 = newp; Point(p5) = {x_gauss       , y_gauss - d/2 - d_i/4 , 0.0};
p6 = newp; Point(p6) = {x_gauss + e/2 , y_gauss - d/2 - d_i/4, 0.0};

p7 = newp; Point(p7) = {x_gauss - e/2 , y_gauss - d/2, 0.0};
p8 = newp; Point(p8) = {x_gauss       , y_gauss - d/2, 0.0};
p9 = newp; Point(p9) = {x_gauss + e/2 , y_gauss - d/2, 0.0};

p10 = newp; Point(p10) = {x_gauss - e/2 , y_gauss + d/2, 0.0};
p11 = newp; Point(p11) = {x_gauss       , y_gauss + d/2, 0.0};
p12 = newp; Point(p12) = {x_gauss + e/2 , y_gauss + d/2, 0.0};

p13 = newp; Point(p13) = {x_gauss - e/2 , y_gauss + d/2 + d_i/4, 0.0};
p14 = newp; Point(p14) = {x_gauss       , y_gauss + d/2 + d_i/4, 0.0};
p15 = newp; Point(p15) = {x_gauss + e/2 , y_gauss + d/2 + d_i/4, 0.0};

p16 = newp; Point(p16) = {x_gauss - e/2 , y_gauss + e/2, 0.0};
p17 = newp; Point(p17) = {x_gauss       , y_gauss + e/2, 0.0};
p18 = newp; Point(p18) = {x_gauss + e/2 , y_gauss + e/2, 0.0};

p19 = newp; Point(p19) = {x_gauss       , y_gauss      , 0.0};


l1 = newl; Line(l1) = {p1,p2};
l2 = newl; Line(l2) = {p2,p3};
l3 = newl; Line(l3) = {p4,p5};
l4 = newl; Line(l4) = {p5,p6};
l5 = newl; Line(l5) = {p7,p8};
l6 = newl; Line(l6) = {p8,p9};
l7 = newl; Line(l7) = {p10,p11};
l8 = newl; Line(l8) = {p11,p12};
l9 = newl; Line(l9) = {p13,p14};
l10 = newl; Line(l10) = {p14,p15};
l11 = newl; Line(l11) = {p16,p17};
l12 = newl; Line(l12) = {p17,p18};

l13 = newl; Line(l13) = {p1,p4};
l14 = newl; Line(l14) = {p2,p5};
l15 = newl; Line(l15) = {p3,p6};
l16 = newl; Line(l16) = {p4,p7};
l17 = newl; Line(l17) = {p5,p8};
l18 = newl; Line(l18) = {p6,p9};
l19 = newl; Line(l19) = {p7,p10};
l20 = newl; Line(l20) = {p8,p11};
l21 = newl; Line(l21) = {p9,p12};
l22 = newl; Line(l22) = {p10,p13};
l23 = newl; Line(l23) = {p11,p14};
l24 = newl; Line(l24) = {p12,p15};
l25 = newl; Line(l25) = {p13,p16};
l26 = newl; Line(l26) = {p14,p17};
l27 = newl; Line(l27) = {p15,p18};

ll1 = newll; Line Loop(ll1) = {l1,l14,-l3,-l13};
ps1 = news; Plane Surface(ps1) = {ll1};

ll2 = newll; Line Loop(ll2) = {l2,l15,-l4,-l14};
ps2 = news; Plane Surface(ps2) = {ll2};

ll3 = newll; Line Loop(ll3) = {l3,l17,-l5,-l16};
ps3 = news; Plane Surface(ps3) = {ll3};

ll4 = newll; Line Loop(ll4) = {l4,l18,-l6,-l17};
ps4 = news; Plane Surface(ps4) = {ll4};

ll5 = newll; Line Loop(ll5) = {l5,l20,-l7,-l19};
ps5 = news; Plane Surface(ps5) = {ll5};

ll6 = newll; Line Loop(ll6) = {l6,l21,-l8,-l20};
ps6 = news; Plane Surface(ps6) = {ll6};

ll7 = newll; Line Loop(ll7) = {l7,l23,-l9,-l22};
ps7 = news; Plane Surface(ps7) = {ll7};

ll8 = newll; Line Loop(ll8) = {l8,l24,-l10,-l23};
ps8 = news; Plane Surface(ps8) = {ll8};

ll9 = newll; Line Loop(ll9) = {l9,l26,-l11,-l25};
ps9 = news; Plane Surface(ps9) = {ll9};

ll10 = newll; Line Loop(ll10) = {l10,l27,-l12,-l26};
ps10 = news; Plane Surface(ps10) = {ll10};

Transfinite Line{l1 , l2 , l3 , l4 , l5 , l6 , l7 , l8 , l9 , l10, l11, l12} = n1 + 1 ;
Transfinite Line{l13, l14, l15, l16, l17, l18, l22, l23, l24, l25, l26, l27} = n2 + 1 Using Bump fbumpX;
Transfinite Line{l19, l20, l21}          = n3+1 Using Bump fbumpY;
Transfinite Surface '*';
Recombine Surface '*';


// Physical groups
//================
Physical Point(GAMMA_MIDDLE_POINT)  = {p5};
Physical Point(GAMMA_MIDDLE_POINT_U)  = {p14};
Physical Point(GAMMA_CORNER1)  = {p1};
Physical Point(GAMMA_CORNER2)  = {p3};
Physical Point(GAMMA_CORNER3)  = {p16};
Physical Point(GAMMA_CORNER4)  = {p18};

Physical Line(GAMMA_LEFT)      = {l13, l16, l19, l22, l25 };
Physical Line(GAMMA_RIGHT)     = {l15, l18, l21, l24, l27};
Physical Line(GAMMA_DOWN)      = {l1, l2};
Physical Line(GAMMA_UP)        = {l11, l12};

Physical Surface(AIR)          = {ps1, ps2, ps3, ps4, ps7, ps8, ps9, ps10};
Physical Surface(IRON)         = {ps5, ps6};
