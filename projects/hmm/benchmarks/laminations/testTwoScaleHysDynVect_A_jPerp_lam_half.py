from gmshpy import *
from hmmpy  import *
import hmmpy 
import math
import sys
import os
import time

dir(hmmpy)

print'---- Lamination stack problem'

geoFile  = 'macro_jPerp_lam_half.geo'
meshFile = 'macro_jPerp_lam_half.msh'

macromodel = GModel()
macromodel.load(geoFile)
macromodel.mesh(2)
macromodel.save(meshFile)

# Physical regions
#=================
GAMMA_INF = 10
CONDUCTOR = 11
AIR       = 12
INDUCTOR1 = 13
INDUCTOR2 = 14


# Solver input data
#==================
mm          = 1e-3
d           = 0.5 * mm
#llambda     = 0.9
e           = 0.55 * mm #d/llambda
numLam      = 10
_LX         = d * (numLam + 1) + (e - d) * numLam
FF          = 0.9
_lx         = e
_ly         = _lx
_nx         = 5
_ny         = _nx
problemType = 0
maxNumIter  = 6
tol         = 5e-5 
gmres       = 0
mesoSolver  = 0 # 0 for the nonlinear mesoscale solver, 1 for the hysteresis mesoscale solver. 

# Constructor
#============
hmmprob = hmmProblemBase()
hmmprob.setLX(_lx)
hmmprob.setLY(_ly)
hmmprob.setNX(_ny)
hmmprob.setNY(_ny)
hmmprob.setProblemType(problemType)


# 1. Set some parameters
#=======================
hmmprob.setLinsysAndDofManager()
hmmprob.setModel(macromodel)
hmmprob.setMaxNumIter(maxNumIter)
hmmprob.setNumIter(0)
hmmprob.setTolerance(tol)
hmmprob.setGmresValue(gmres)

# 2. hmmGroupOfDomains
#=====================
tagGroup_GammaINF = "Gamma_Inf"
hmmprob.addGroupOfDomains(tagGroup_GammaINF)
hmmprob.addDomainToGroupOfDomains(tagGroup_GammaINF, 1, GAMMA_INF)
print'Group_GammaINF is tagged', tagGroup_GammaINF

tagGroup_Air = "Air"
hmmprob.addGroupOfDomains(tagGroup_Air)
hmmprob.addDomainToGroupOfDomains(tagGroup_Air, 2, AIR)
print'Group_Air is tagged', tagGroup_Air

tagGroup_Indu1 = "Inductor1"
hmmprob.addGroupOfDomains(tagGroup_Indu1)
hmmprob.addDomainToGroupOfDomains(tagGroup_Indu1, 2, INDUCTOR1)
print'Group_Dom is tagged', tagGroup_Indu1

tagGroup_Indu2 = "Inductor2"
hmmprob.addGroupOfDomains(tagGroup_Indu2)
hmmprob.addDomainToGroupOfDomains(tagGroup_Indu2, 2, INDUCTOR2)
print'Group_Dom is tagged', tagGroup_Indu2

tagGroup_Cond = "Conductor"
hmmprob.addGroupOfDomains(tagGroup_Cond)
hmmprob.addDomainToGroupOfDomains(tagGroup_Cond, 2, CONDUCTOR)
print'Group_Dom is tagged', tagGroup_Cond

tagGroup_All = "All"
hmmprob.addGroupOfDomains(tagGroup_All)
hmmprob.addDomainToGroupOfDomains(tagGroup_All, 2, AIR)
hmmprob.addDomainToGroupOfDomains(tagGroup_All, 2, INDUCTOR1)
hmmprob.addDomainToGroupOfDomains(tagGroup_All, 2, INDUCTOR2)
hmmprob.addDomainToGroupOfDomains(tagGroup_All, 2, CONDUCTOR)
print'Group_All is tagged', tagGroup_All
hmmprob.getDomainsInfo()


# 4. hmmConstraints
#==================
imposedValue_Corner = 0.0
tagDiriInf = "diriInf"
hmmprob.addDirichletBC(tagGroup_GammaINF, tagDiriInf, 1, imposedValue_Corner)
print'The boundary condition at corner is tagged ', tagDiriInf
hmmprob.getConstraintsInfo()

# 5. Quadratures
#===============
tagQuad = "Grad"
hmmprob.addQuad(tagQuad)
print'The added quadrature rule is tagged ', tagQuad
hmmprob.getQuadInfo()

# 6. hmmFunctionSpace
#====================
tagSpace = "BF_Perp"
iField = 0
funcSpacType = Form1P
basisFunctionType = BF_PerpendicularEdge
hmmprob.addFunctionSpace(tagSpace, tagGroup_All, iField, funcSpacType, basisFunctionType)
#hmmprob.addFunctionSpace(tagSpace, tagGroup_Air, iField, funcSpacType, basisFunctionType)
hmmprob.addConstraintToFunctionSpace(tagSpace, tagDiriInf)
print'The added function space is tagged ', tagSpace
hmmprob.getFunctionSpaceInfo()

# 3. hmmFunction
#===============
GMSHVerbosity  = "0"
GMSHGeoFileName = "micro_InsuCondInsu.geo"
GetDPVerbosity = "0"
if mesoSolver == 0 :
    GetDPProFileName = "micro_vecA_dyn_nlin.pro "
    GetDPExactProFileName = "micro_vecA_dyn_nlin.pro -msh micro_exact.msh"
else : 
    GetDPProFileName = "micro_vecA_dyn_hys.pro "
    GetDPExactProFileName = "micro_vecA_dyn_hys.pro -msh micro_exact.msh"
GetDPResolutionName = "a_NR"
GetDPPostPro = "mean_1 mean_2 mean_3 cuts_field_1 "
GMSHExactGeoFileName = "micro_exact.geo "
microProblemFileName = ""

pi = 3.14159
e1 = 0.5/0.55
e2 = 1.0 - e1
nu_Insu = 1e0 * 1.0/(4.0 * pi * 1.0e-7) 
nu_Iron = 1e-3 * nu_Insu
nu_para = nu_Iron * e1 + nu_Insu * e2
nu_perp = nu_Iron * nu_Insu / (e1 * nu_Insu + e2 * nu_Iron)

tagNLML_hb = "_hb_NLMatLaw"
tagLML_hb = "_hb_MatLaw"

hmmprob.add2ScaleNonLinearMaterialLawInno(tagNLML_hb, tagGroup_Cond, tagSpace, tagQuad, GMSHVerbosity, GMSHGeoFileName, GetDPVerbosity, GetDPProFileName, GetDPResolutionName, GetDPPostPro, GMSHExactGeoFileName, GetDPExactProFileName, microProblemFileName)
hmmprob.Init2ScaleNonLinearMLInno(tagNLML_hb, tagGroup_Cond, nu_para, 0.0, 0.0, 0.0, nu_para, 0.0, 0.0, 0.0, nu_para)
#hmmprob.Init2ScaleNonLinearMLInno(tagNLML_hb, tagGroup_Cond, nu_Iron, 0.0, 0.0, 0.0, nu_Iron, 0.0, 0.0, 0.0, nu_Iron)
print "The added material law is tagged ", tagNLML_hb

#hmmprob.add1ScaleLinearMaterialLawInno(tagLML_hb, tagGroup_Cond, 994, 0.0, 0.0, 0.0, 159792, 0.0, 0.0, 0.0, nu_Iron)
#print "The added material law is tagged ", tagLML_hb

#hmmprob.add1ScaleLinearMaterialLawInno(tagLML_hb, tagGroup_Cond, nu_Iron, 0.0, 0.0, 0.0, nu_Iron, 0.0, 0.0, 0.0, nu_Iron)
#print "The added material law is tagged ", tagLML_hb

hmmprob.add1ScaleLinearMaterialLawInno(tagLML_hb, tagGroup_Air, nu_Insu, 0.0, 0.0, 0.0, nu_Insu, 0.0, 0.0, 0.0, nu_Insu)
print "The added material law is tagged ", tagLML_hb

hmmprob.add1ScaleLinearMaterialLawInno(tagLML_hb, tagGroup_Indu1, nu_Insu, 0.0, 0.0, 0.0, nu_Insu, 0.0, 0.0, 0.0, nu_Insu)
print "The added material law is tagged ", tagLML_hb

hmmprob.add1ScaleLinearMaterialLawInno(tagLML_hb, tagGroup_Indu2, nu_Insu, 0.0, 0.0, 0.0, nu_Insu, 0.0, 0.0, 0.0, nu_Insu)
print "The added material law is tagged ", tagLML_hb

#hmmprob.getMaterialLaw(tagLML_hb).printMsg()





tagLML_sigma = "_sigma_MatLaw"
sigma = 5e6

sigma1 = sigma 
sigma2 = sigma
sigma_para = sigma1 * e1 + sigma2 * e2
sigma_perp = sigma1*sigma2/(e1 * sigma2 + e2 * sigma1)

#hmmprob.add1ScaleLinearMaterialLawInno(tagLML_sigma, tagGroup_Cond, sigma_para, 0.0, 0.0, 0.0, sigma_perp, 0.0, 0.0, 0.0, sigma_para)
hmmprob.add1ScaleLinearMaterialLawInno(tagLML_sigma, tagGroup_Cond, sigma_para, 0.0, 0.0, 0.0, sigma_perp, 0.0, 0.0, 0.0, 0.0)
print "The added material law is tagged ", tagLML_sigma

#hmmprob.add1ScaleLinearMaterialLaw(tagLML_sigma, tagGroup_Indu1, sigma_para, 0.0, 0.0, 0.0, sigma_perp, 0.0, 0.0, 0.0, sigma_para)
#print "The added material law is tagged ", tagLML_sigma

#hmmprob.add1ScaleLinearMaterialLaw(tagLML_sigma, tagGroup_Indu2, sigma_para, 0.0, 0.0, 0.0, sigma_perp, 0.0, 0.0, 0.0, sigma_para)
#print "The added material law is tagged ", tagLML_sigma

hmmprob.getFunctionInfo()

# 7. hmmFormulation
#==================
tagForm1 = "a_Hyst_ML"
hmmprob.addFormulation(tagForm1)
hmmprob.addBilinearTermToFormulation(tagForm1, tagSpace, tagLML_hb  , tagGroup_Indu1, GBF, tagQuad)
hmmprob.addBilinearTermToFormulation(tagForm1, tagSpace, tagLML_hb  , tagGroup_Indu2, GBF, tagQuad)
hmmprob.addBilinearTermToFormulation(tagForm1, tagSpace, tagLML_hb  , tagGroup_Air, GBF, tagQuad)
hmmprob.addBilinearTermToFormulation(tagForm1, tagSpace, tagNLML_hb  , tagGroup_Cond, GBF, tagQuad)
hmmprob.addBilinearTermToFormulation(tagForm1, tagSpace, tagLML_sigma, tagGroup_Cond, SBF, tagQuad) #?????
#hmmprob.addBilinearTermToFormulation(tagForm1, tagSpace, tagLML_hb  , tagGroup_Cond, GBF, tagQuad)
#hmmprob.addBilinearTermToFormulation(tagForm1, tagSpace, tagLML_sigma, tagGroup_Indu1, SBF, tagQuad) #?????
#hmmprob.addBilinearTermToFormulation(tagForm1, tagSpace, tagLML_sigma, tagGroup_Indu2, SBF, tagQuad) #?????
sourceCurrentValue  = 2.5e7
hmmprob.addSourceTermToFormulation(tagForm1, tagSpace, tagGroup_Indu1, tagQuad, 0.0, 0.0,  sourceCurrentValue)
hmmprob.addSourceTermToFormulation(tagForm1, tagSpace, tagGroup_Indu2, tagQuad, 0.0, 0.0, -sourceCurrentValue)
print'The added formulation is tagged ', tagForm1
hmmprob.getFormulationInfo()

# 8. hmmResolution
#=================
x = time.clock()
freq = 0.5
period = 1.0/freq
nbRT = 1./1.
n = 20
initTime = 0.0
endTime = nbRT * period
dTime = (period)/n
#dTime = (endTime-initTime)/n
theta = 1.0
print 'initTime', initTime
print 'endTime', endTime
print 'dTime', dTime
print 'theta', theta
tagRes1 = "a_Hyst_Res"
tagPostPro1 = "a_Hyst_Post"
hmmprob.addSolver(tagRes1, timeDomSolver)
hmmprob.addPostPro(tagPostPro1)

#===================================================================================
hmmprob.addPoint2MapOfPoints(tagPostPro1, 0.0, 0.0, 0.0)
#===================================================================================

hmmprob.addParameters(tagRes1, initTime, endTime, dTime, theta, freq)
hmmprob.addOp2Solver(tagRes1, tagForm1, InitSolLin, hmmprob.getPostPro(tagPostPro1) )
hmmprob.addOp2Solver(tagRes1, tagForm1, Sol,    hmmprob.getPostPro(tagPostPro1) )
hmmprob.addOp2Solver(tagRes1, tagForm1, SolJac, hmmprob.getPostPro(tagPostPro1) )

print'|CPU|',time.clock() - x
