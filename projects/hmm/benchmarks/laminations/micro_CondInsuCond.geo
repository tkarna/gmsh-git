Include "micro_CondInsuCond.dat";

// Transfinite mesh data
n1 = 15 * ratio_Ll ;
n  = 30 ;
n2 = Floor(n * (1-ratio))/2;
n3 = 2*(n-2*n2);

// micro geometry centered at the gauss point

p1 = newp; Point(p1) = {x_gauss-lx/2 , y_gauss-ly/2          , 0};
p2 = newp; Point(p2) = {x_gauss+lx/2 , y_gauss-ly/2          , 0};
p3 = newp; Point(p3) = {x_gauss-lx/2 , y_gauss-ly/2+eps_yy/2 , 0};
p4 = newp; Point(p4) = {x_gauss+lx/2 , y_gauss-ly/2+eps_yy/2 , 0};
p5 = newp; Point(p5) = {x_gauss-lx/2 , y_gauss+ly/2-eps_yy/2 , 0};
p6 = newp; Point(p6) = {x_gauss+lx/2 , y_gauss+ly/2-eps_yy/2 , 0};
p7 = newp; Point(p7) = {x_gauss-lx/2 , y_gauss+ly/2          , 0};
p8 = newp; Point(p8) = {x_gauss+lx/2 , y_gauss+ly/2          , 0};
p9 = newp; Point(p9) = {x_gauss , y_gauss , 0};


l1 = newl; Line(l1) = {p1,p2};
l2 = newl; Line(l2) = {p3,p4};
l3 = newl; Line(l3) = {p5,p6};
l4 = newl; Line(l4) = {p7,p8};
l5 = newl; Line(l5) = {p1,p3};
l6 = newl; Line(l6) = {p2,p4};
l7 = newl; Line(l7) = {p3,p5};
l8 = newl; Line(l8) = {p4,p6};
l9 = newl; Line(l9) = {p5,p7};
l10 = newl; Line(l10) = {p6,p8};

ll1 = newll; Line Loop(ll1) = {l1,l6,-l2,-l5};
ps1 = news; Plane Surface(ps1) = {ll1};

ll2 = newll; Line Loop(ll2) = {l2,l8,-l3,-l7};
ps2 = news; Plane Surface(ps2) = {ll2};

ll3 = newll; Line Loop(ll3) = {l3,l10,-l4,-l9};
ps3 = news; Plane Surface(ps3) = {ll3};

Transfinite Line{l1, l2, l3, l4}  = n1+1;
Transfinite Line{l5, l6, l9, l10} = n2+1;
Transfinite Line{l7, l8}          = n3+1;
Transfinite Surface '*';
Recombine   Surface '*';


// Physical groups
//================
Physical Point(GAMMA_CORNER1)  = {p1};
Physical Point(GAMMA_CORNER2)  = {p2};
Physical Point(GAMMA_CORNER3)  = {p7};
Physical Point(GAMMA_CORNER4)  = {p8};

Physical Line(GAMMA_LEFT)  = {l5, l7, l9 };
Physical Line(GAMMA_RIGHT) = {l6, l8, l10};
Physical Line(GAMMA_DOWN)  = {l1};
Physical Line(GAMMA_UP)    = {l4};

Physical Surface(IRON)     = {ps1, ps3};
Physical Surface(AIR)      = {ps2};
