Include "micro.dat";
Include "output/gaussPointPosition.dat";
//Include "micro_exact.dat";

Group {
  GammaCornerFix = Region[ {GAMMA_MIDDLE_POINT } ];
  GammaCornerFixU = Region[ {GAMMA_MIDDLE_POINT_U } ];
  GammaCornerUL = Region[ {GAMMA_CORNER3 } ];

  GammaLeft      = Region[ {GAMMA_LEFT } ];
  GammaRight     = Region[ {GAMMA_RIGHT} ];
  GammaUp        = Region[ {GAMMA_UP} ];
  GammaDown      = Region[ {GAMMA_DOWN} ];
  GammaDirichlet = Region[ {GAMMA_UP, GAMMA_DOWN} ];
  GammaDirichlet_Yindi = Region[ {GAMMA_LEFT, GAMMA_RIGHT} ];

  Omega_NL       = Region[ {IRON} ] ;
  Omega_L        = Region[ {AIR} ] ;
  Omega_C        = Region[ {Omega_NL} ] ;
  Omega_CC        = Region[ {Omega_L} ] ;
  Omega          = Region[ {Omega_NL, Omega_L} ] ;
}

Function {
  XYZ_Gauss[] = Vector[x_gauss, y_gauss, 0.0];
  mu0 = 4.e-7 * Pi ;
  nu0 = 1/mu0 ;
  sigmaIron = 5e6;

  //Gyselink's paper: Incorporation of a Jiles-Atherton vector hysteresis model in 2D FE magnetic field computations
  //================================================================================================================
  Msat = 1145220; aaa = 59.5; kkk = 99.2; ccc = 0.54; alpha = 1.3e-4 ;
  hyst_FeSi = { Msat, aaa, kkk, ccc, alpha};

  Freq    = 20 ; 
  T       = 1/Freq ; 
  nbrT    = 2 ; 
  nbrstep = 120 ;
  t0      = 0 ; 
  tmax    = nbrT*T ; 
  dt      = T/nbrstep ; 
  dtt     = dt;
  theta   = 1.0;


  dt_aM[]     = (1.0/dt) * Vector[0.0, 0.0, 0.0];
  //dt_aM[]     = (1.0/dt) * Vector[0.0, 0.0, _dtFunc];
  dt_bM[]   = (1.0/dt) * Vector[_dtGradFunc_2, -_dtGradFunc_1, 0.0];

  aM[]        = Vector[0.0, 0.0, _func];
  curl_aM[]   = Vector[_gradFunc_2, -_gradFunc_1, 0.0];

  aM_prev[]        = Vector[0.0, 0.0, _func_prev];
  grad_aM_prev[]   = Vector[_gradFunc_prev_2, -_gradFunc_prev_1, 0.0];


  NbrMaxIter = 50; 
  Eps        = 1e-4; 
  Relax      = 1.0;
  factor     = 1.0;

  Nbr_SubProblems = 3;
  epsilon = 1e-6;
  Pert~{1}[] = Vector[0, 0, 0];
  Pert~{2}[] = epsilon * Vector[1.0, 0.0, 0.0];
  Pert~{3}[] = epsilon * Vector[0.0, 1.0, 0.0];

  ti = t0 + currentTimeStep * dt ;
  tf = ti + dt;

  //Printf("=============> step = %g ti = %g lx = %g exactMicroGeo = %g Nb_MicroProb = %g", currentTimeStep, ti, lx, exactMicroGeo, Nb_MicroProb);

  sigma[Omega_C]  = sigmaIron;
  sigma[Omega_CC] = 1.e-12;
  nu[Omega_L]     = nu0; 

  //a_macro[]     = Vector[ 0., 0., ScalarField[XYZ[]]{0}] ;
  a_pert_previous[] = (currentTimeStep==0) ? Vector[0.,0.,0.] : Vector[ 0., 0., ScalarField[XYZ[]]{0}] ;
  a_tot_previous[]  = (currentTimeStep==0) ? Vector[0.,0.,0.] : Vector[ 0., 0., ScalarField[XYZ[]]{1}] ;
  h_tot_previous[]  = (currentTimeStep==0) ? Vector[0.,0.,0.] : VectorField[XYZ[]]{2} ;
}

Constraint {
 { Name a_Micro ;
   Case {
     { Region GammaRight; Type Link; RegionRef GammaLeft;
       Coefficient 1.; Function Vector[$X-lx, $Y, $Z]; }
     { Region GammaUp; Type Link; RegionRef GammaDown;
       Coefficient 1.; Function Vector[$X, $Y-ly, $Z]; }
     { Region GammaCornerFix; Type Assign; Value 0.0; }
     //{ Region GammaCornerFixU; Type Assign; Value 0.0; }
   }
 }
 { Name Current_2D ;
   Case {
     { Region Omega_C; Type Assign; Value 0.0; } // forcing net current in each lamination to be equal to zero
   }
 }
 { Name a_Micro_Init ;
   Case {
     { Type InitFromResolution ; Region Omega ; NameOfResolution a_Init ; }
   }
 }
}

Jacobian {
  { Name Vol ; Case { { Region All ; Jacobian Vol ; } } }
  { Name Sur ; Case { { Region All ; Jacobian Sur ; } } }
}

Integration {
  { Name II ; Case { 
      {Type Gauss ; 
        Case {
	  { GeoElement Line        ; NumberOfPoints  4 ; }
          { GeoElement Triangle    ; NumberOfPoints  4 ; }
          { GeoElement Quadrangle  ; NumberOfPoints  4 ; } 
        } 
      } 
    }
  }
}

FunctionSpace{
  For iP In {1:Nbr_SubProblems}
  { Name HCurl_a_Proj~{iP} ; Type Form1P ;
    BasisFunction{
      { Name sn; NameOfCoef an; Function BF_PerpendicularEdge; Support Omega; Entity NodesOf[ All ]; }
    }
  }
  { Name HCurl_a_Micro~{iP} ; Type Form1P ;
    BasisFunction{
      { Name sn; NameOfCoef an; Function BF_PerpendicularEdge; Support Omega; Entity NodesOf[ All ]; }
    }
    Constraint {
      { NameOfCoef an; EntityType NodesOf; NameOfConstraint a_Micro; }
      { NameOfCoef an; EntityType NodesOf; NameOfConstraint a_Micro_Init; }
    }
  }

  // Gradient of Electric scalar potential (2D)
  { Name Hregion_u_2D~{iP} ; Type Form1P ;
    BasisFunction {
      { Name sr ; NameOfCoef ur ; Function BF_RegionZ ;
        Support Omega_C ; Entity Omega_C ; }
    }
    GlobalQuantity {
      { Name U ; Type AliasOf        ; NameOfCoef ur ; }
      { Name I ; Type AssociatedWith ; NameOfCoef ur ; }
    }
    Constraint {
      { NameOfCoef U ;
        EntityType Region ; NameOfConstraint Voltage_2D ; }
      { NameOfCoef I ;
        EntityType Region ; NameOfConstraint Current_2D ; }
    }
  }
  { Name H_hysteresis~{iP} ; Type Form1 ;
    BasisFunction {
      { Name sex; NameOfCoef aex; Function BF_VolumeX; Support Omega; Entity VolumesOf[ Omega ]; }
      { Name sey; NameOfCoef aey; Function BF_VolumeY; Support Omega; Entity VolumesOf[ Omega ]; }
      { Name sez; NameOfCoef aez; Function BF_VolumeZ; Support Omega; Entity VolumesOf[ Omega ]; }
    }
  }
  { Name HCurl_a_Micro_t0~{iP} ; Type Form1P ;
    BasisFunction{
      { Name sn; NameOfCoef an; Function BF_PerpendicularEdge; Support Omega; Entity NodesOf[ All ]; }
    }
  }
  { Name H_hysteresis_t0~{iP} ; Type Vector;
    BasisFunction {
      { Name sex ; NameOfCoef aex ; Function BF_VolumeX ; Support Omega ; Entity VolumesOf[ Omega ] ; }
      { Name sey ; NameOfCoef aey ; Function BF_VolumeY ; Support Omega ; Entity VolumesOf[ Omega ] ; }
      { Name sez ; NameOfCoef aez ; Function BF_VolumeZ ; Support Omega ; Entity VolumesOf[ Omega ] ; }
    }
  }
  EndFor
}

Formulation {
  For iP In {1:Nbr_SubProblems}
  { Name Init_PreviousTimeStep~{iP} ; Type FemEquation ;
    Quantity{
      { Name a  ; Type Local  ; NameOfSpace HCurl_a_Micro_t0~{iP} ; }
      { Name h  ; Type Local  ; NameOfSpace H_hysteresis_t0~{iP} ; }
    }
    Equation{
      Galerkin { [     Dof{a} , {a} ]   ; In Omega; Jacobian Vol; Integration II; }
      Galerkin { [ -a_tot_previous[], {a} ]; In Omega; Jacobian Vol; Integration II; }
      Galerkin { [     Dof{h} , {h} ]   ; In Omega; Jacobian Vol; Integration II; }
      Galerkin { [ -h_tot_previous[], {h} ]; In Omega; Jacobian Vol; Integration II; }
    }
  }

  { Name Init_PertFromPreviousTimeStep~{iP} ; Type FemEquation ;
    Quantity{
      { Name a  ; Type Local  ; NameOfSpace HCurl_a_Micro~{iP} ; }
    }
    Equation{
      Galerkin { [     Dof{a} , {a} ]   ; In Omega; Jacobian Vol; Integration II; }
      Galerkin { [ -a_pert_previous[], {a} ]; In Omega; Jacobian Vol; Integration II; }
    }
  }
  EndFor
  
  For iP In {1:Nbr_SubProblems}
  { Name a_NR~{iP} ; Type FemEquation ;
    Quantity {
      { Name a  ; Type Local ; NameOfSpace HCurl_a_Micro~{iP} ; }
      { Name h ; Type Local  ; NameOfSpace H_hysteresis~{iP} ; }
      { Name ur ; Type Local  ; NameOfSpace Hregion_u_2D~{iP} ; }
      { Name I  ; Type Global ; NameOfSpace Hregion_u_2D~{iP}[I] ; }
      { Name U  ; Type Global ; NameOfSpace Hregion_u_2D~{iP}[U] ; }
      { Name a0 ; Type Local ; NameOfSpace HCurl_a_Micro_t0~{iP} ; } 
      { Name h0 ; Type Local ; NameOfSpace H_hysteresis_t0~{iP} ; } 
    }
    Equation {
       Galerkin { DtDof[ sigma[] * Dof{a} , {a} ] ; In Omega_C; Jacobian Vol; Integration II; }
       Galerkin { [ -sigma[] * ( factor * (XYZ[]-XYZ_Gauss[]) /\ dt_bM[] ) , {a} ]; In Omega_C; Jacobian Vol; Integration II; }
       Galerkin { [ sigma[] * Dof{ur}      , {a} ]  ; In Omega_C; Jacobian Vol; Integration II; }
       
       Galerkin { DtDof [ sigma[] * Dof{a} , {ur} ]; In Omega_C ; Jacobian Vol; Integration II ; }
       Galerkin { [ sigma[] * Dof{ur}      , {ur} ]; In Omega_C ; Jacobian Vol ; Integration II ; }    
       GlobalTerm { [ Dof{I}               , {U}  ]; In Omega_C ; } 
       
       Galerkin { [ nu[ {d a} + curl_aM[] + Pert~{iP}[] ] * Dof{d a} , {d a} ]   ;   In Omega_L; Jacobian Vol; Integration II; }
       Galerkin { [ nu[ {d a} + curl_aM[] + Pert~{iP}[] ] * curl_aM[], {d a} ]      ;   In Omega_L; Jacobian Vol; Integration II; }
       Galerkin { [ nu[ {d a} + curl_aM[] + Pert~{iP}[] ] * Pert~{iP}[] , {d a} ];   In Omega_L; Jacobian Vol; Integration II; }
       Galerkin { [ h_Jiles[{h0}, {d a0}, {d a}+curl_aM[]+Pert~{iP}[] ]{List[hyst_FeSi]}#4 , {d a} ]; 
         In Omega_NL; Jacobian Vol; Integration II; }
       Galerkin { JacNL[ dhdb_Jiles[ {h}, {d a}+curl_aM[]+Pert~{iP}[], {h}-{h0} ]{List[hyst_FeSi]} * Dof{d a} , {d a} ];
         In Omega_NL ; Jacobian Vol ; Integration II ; }
       Galerkin { JacNL[ dhdb_Jiles[ {h}, {d a}+curl_aM[]+Pert~{iP}[], {h}-{h0} ]{List[hyst_FeSi]} * curl_aM[] , {d a} ];
         In Omega_NL ; Jacobian Vol ; Integration II ; }
       Galerkin { JacNL[ dhdb_Jiles[ {h}, {d a}+curl_aM[]+Pert~{iP}[], {h}-{h0} ]{List[hyst_FeSi]} * Pert~{iP}[], {d a} ];
         In Omega_NL ; Jacobian Vol ; Integration II ; }
       
       Galerkin { [ Dof{h}  , {h} ]; In Omega_NL ; Jacobian Vol ; Integration II ; }
       Galerkin { [   -#4   , {h} ]; In Omega_NL ; Jacobian Vol ; Integration II ; }
    }
  }
  EndFor
}

Resolution {
 { Name a_Init ;
    System {
      For iP In {1:Nbr_SubProblems}
      { Name AHHH~{iP}   ; NameOfFormulation Init_PertFromPreviousTimeStep~{iP}; DestinationSystem Micro~{iP}; }
      EndFor
    }
    Operation {
       For iP In {1:Nbr_SubProblems}
       If(currentTimeStep == 0)
         Generate[AHHH~{iP}]; Solve[AHHH~{iP}]; TransferSolution[AHHH~{iP}];
       EndIf
       If(currentTimeStep != 0)
         If(exactMicroGeo == 0 )
           GmshRead[Sprintf("output/microproblems/a_pert_p1_cs%g_gp%g.pos", currentTimeStep-1, numEle)];
         EndIf
         If( (exactMicroGeo != 0) && (iP == 1) )
           GmshRead[Sprintf("output/micro_exact/microproblems/a_pert_p1_cs%g_gp%g.pos", currentTimeStep-1, numEle)];
         EndIf
         //GmshRead[Sprintf("output/microproblems/a_pert_p1_s%g_gp%g.pos", currentTimeStep-1, numEle)];
         Generate[AHHH~{iP}]; Solve[AHHH~{iP}]; TransferSolution[AHHH~{iP}];
       EndIf
       EndFor
    }
  }

  { Name a_NR ;
    System {
      For iP In {1:Nbr_SubProblems}
      { Name AH~{iP}     ; NameOfFormulation Init_PreviousTimeStep~{iP} ; }
      { Name Micro~{iP} ; NameOfFormulation a_NR~{iP} ;}
      EndFor
    }
    Operation {
      SetTime[ti];
      For iP In {1:Nbr_SubProblems}
      If(currentTimeStep == 0)
        InitSolution[AH~{iP}] ;
      EndIf
      If(currentTimeStep != 0)
        If(exactMicroGeo == 0 )
          GmshRead[Sprintf("output/microproblems/a_tot_p1_cs%g_gp%g.pos", currentTimeStep-1, numEle)];
          GmshRead[Sprintf("output/microproblems/h_tot_p1_cs%g_gp%g.pos", currentTimeStep-1, numEle)];
        EndIf
        If( (exactMicroGeo != 0) && (iP == 1) )
          GmshRead[Sprintf("output/micro_exact/microproblems/a_tot_p1_cs%g_gp%g.pos", currentTimeStep-1, numEle)];
          GmshRead[Sprintf("output/micro_exact/microproblems/h_tot_p1_cs%g_gp%g.pos", currentTimeStep-1, numEle)];
        EndIf
        Generate[AH~{iP}]; Solve[AH~{iP}]; SaveSolution[AH~{iP}];
      EndIf
      EndFor
      For iP In {1:Nbr_SubProblems}
      InitSolution[Micro~{iP} ];
      TimeLoopTheta[ ti, tf, dtt, 1.]{
        IterativeLoop[NbrMaxIter, Eps, Relax]{
          GenerateJac[Micro~{iP}] ; SolveJac[Micro~{iP}] ;
        }
      }
      SaveSolution[Micro~{iP}];
      If(iP == 1)
        PostOperation[map_field_1];
      EndIf
      If(exactMicroGeo && (iP == 1) )
        PostOperation[cuts_field_1];
      EndIf
      EndFor
    }
  }
}


PostProcessing {
  For iP In {1:Nbr_SubProblems}
  { Name a_Micro_NR~{iP} ; NameOfFormulation a_NR~{iP} ; NameOfSystem Micro~{iP} ;
     PostQuantity {
       { Name vol         ; Value { Integral { [ 1. ] ;        In Omega ; Jacobian Vol ; Integration II ; } } }  // stored in register #12
       { Name a_pert      ; Value { Term { [ CompZ[ {a} ] ] ;  In Omega ; Jacobian Vol; } } }
       { Name a_proj      ; Value { 
           Term { [ CompZ[ ( aM[] ) ] ] ;                      In Omega ; Jacobian Vol;  }
           Term { [ CompZ[ ( - factor * (XYZ[] - XYZ_Gauss[]) /\ curl_aM[] ) ] ] ;  In Omega ; Jacobian Vol;  } } }
       { Name a_tot       ; Value { 
           Term { [ CompZ[ {a}] ] ;   In Omega ; Jacobian Vol; }
           Term { [ CompZ[ aM[] ] ] ; In Omega ; Jacobian Vol; }
           Term { [ CompZ[ ( - factor * (XYZ[] - XYZ_Gauss[]) /\ curl_aM[] ) ] ] ; In Omega ; Jacobian Vol; } } }
       { Name b_pert      ; Value { Term { [ {d a} ] ; In Omega ; Jacobian Vol;  } } }
       { Name b_proj      ; Value { Term { [ curl_aM[] ] ; In Omega ; Jacobian Vol;  } } }
       { Name b_tot       ; Value { 
           Term { [ {d a}]    ; In Omega ; Jacobian Vol ; } 
           Term { [ curl_aM[]  ] ; In Omega ; Jacobian Vol ; }
           Term { [ Pert~{iP}[]] ; In Omega ; Jacobian Vol ; } } }
       { Name b_tot_mean  ; Value { Integral { [ ({d a} + curl_aM[] + Pert~{iP}[])/#12 ] ; In Omega ; Jacobian Vol ; Integration II ; } } }
       { Name dt_bM       ; Value { Term { [ dt_bM[] ] ;     In Omega ; Jacobian Vol;  } } }
       { Name h_tot ; Value {
           Term { [ nu[{d a} + curl_aM[] + Pert~{iP}[]] * ({d a} + curl_aM[] + Pert~{iP}[]) ]; In Omega_L; Jacobian Vol;}
           Term { [ {h} ]; In Omega_NL; Jacobian Vol;} } }
       { Name h_tot_mean ; Value {// stored in #22
           Integral { [ nu[ {d a} + curl_aM[] + Pert~{iP}[]] * ({d a} + curl_aM[] + Pert~{iP}[])/#12 ] ; In Omega_L ; Jacobian Vol; Integration II ; }
           Integral { [ {h} ] ; In Omega_NL; Jacobian Vol; Integration II ; } } }
       { Name e_pert      ; Value { 
           Term { [ -Dt[{a}] ]    ; In Omega_C ; Jacobian Vol;  } 
           Term { [ - {ur} ]      ; In Omega_C ; Jacobian Vol;  } } }
       { Name e_proj      ; Value { Term { [ factor * ( (XYZ[] - XYZ_Gauss[]) /\ dt_bM[] ) ] ; In Omega_C ; Jacobian Vol;  } } }
       { Name e_tot       ; Value { 
           Term { [ - Dt[{a}] ]   ; In Omega_C ; Jacobian Vol;  }
           Term { [ - {ur} ]      ; In Omega_C ; Jacobian Vol;  }
           Term { [ factor * ( (XYZ[] - XYZ_Gauss[]) /\ dt_bM[] ) ] ; In Omega_C ; Jacobian Vol;} } }
       { Name j_pert      ; Value { 
           Term { [ - sigma[] * Dt[{a}] ]   ; In Omega_C ; Jacobian Vol;  } 
           Term { [ - sigma[] * {ur} ]      ; In Omega_C ; Jacobian Vol;  } } }
       { Name j_proj      ; Value { Term { [ factor * sigma[] * ( (XYZ[] - XYZ_Gauss[]) /\ dt_bM[] ) ] ; In Omega_C ; Jacobian Vol;  } } }
       { Name j_tot       ; Value { 
           Term { [ - sigma[] * Dt[{a}] ] ; In Omega_C ; Jacobian Vol;  }
           Term { [ - sigma[] * {ur} ] ; In Omega_C ; Jacobian Vol;  }
           Term { [   factor * sigma[] * ( (XYZ[] - XYZ_Gauss[]) /\ dt_bM[] ) ] ; In Omega_C ; Jacobian Vol;  } } }
       { Name j_tot_mean  ; Value{ 
           Integral { [ (- sigma[] * Dt[{a}] ) ] ; In Omega_C ; Jacobian Vol; Integration II ; } 
           Integral { [ (- sigma[] * {ur} ) ]    ; In Omega_C ; Jacobian Vol; Integration II ; } } }
     }
   }
  EndFor
}



If(exactMicroGeo)
nameDirRes = "output/micro_exact/";
EndIf
If(!exactMicroGeo)
nameDirRes = "output/";
EndIf

PostOperation {
For iP In {1:Nbr_SubProblems}
 { Name mean~{iP} ; NameOfPostProcessing a_Micro_NR~{iP};
   Operation{
     Print[ vol[Omega]       , OnGlobal, Format Table, Store 12, File StrCat[nameDirRes, "vol.txt"], LastTimeStepOnly ] ;
     Print[ h_tot_mean[Omega], OnGlobal, Format Table, Store 22, File StrCat[nameDirRes, Sprintf("j%g.txt",   iP) ], LastTimeStepOnly ] ;
     Print[ b_tot_mean[Omega], OnGlobal, Format Table, Store 21, File StrCat[nameDirRes, Sprintf("e%g.txt",   iP) ], LastTimeStepOnly ] ;
     Print[ j_tot_mean[Omega_C], OnGlobal, Format Table, Store 21, File StrCat[nameDirRes, Sprintf("j_tot_mean%g.txt",   iP) ], LastTimeStepOnly ] ;

   }
 }
 { Name cuts_field~{iP} ; NameOfPostProcessing a_Micro_NR~{iP};
   Operation{
     Print[ a_pert, OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("a_pert%g.pos", iP) ], LastTimeStepOnly ];
     Print[ a_proj, OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("a_proj%g.pos", iP) ], LastTimeStepOnly ];
     Print[ a_tot,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("a_tot%g.pos" , iP) ], LastTimeStepOnly ];
     Print[ b_pert, OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("b_pert%g.pos" , iP) ], LastTimeStepOnly ];
     Print[ b_proj, OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("b_proj%g.pos" , iP) ], LastTimeStepOnly ];
     Print[ b_tot,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("b_tot%g.pos" , iP) ], LastTimeStepOnly ];
     Print[ h_tot,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("h_tot%g.pos" , iP) ], LastTimeStepOnly ];
     Print[ e_pert,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("e_pert%g.pos" , iP) ], LastTimeStepOnly ];
     Print[ e_proj,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("e_proj%g.pos" , iP) ], LastTimeStepOnly ];
     Print[ e_tot,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("e_tot%g.pos" , iP) ], LastTimeStepOnly ];
     Print[ j_pert,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("j_pert%g.pos" , iP) ], LastTimeStepOnly ];
     Print[ j_proj,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("j_proj%g.pos" , iP) ], LastTimeStepOnly ];
     Print[ j_tot,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("j_tot%g.pos" , iP) ], LastTimeStepOnly ];
   }
 }
{ Name map_field~{iP} ; NameOfPostProcessing a_Micro_NR~{iP};
   Operation {
     Print[ a_pert, OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("microproblems/a_pert_p%g_cs%g_gp%g.pos", iP, currentTimeStep, numEle) ], Format Gmsh, LastTimeStepOnly ];
     Print[ a_proj, OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("microproblems/a_proj_p%g_cs%g_gp%g.pos", iP, currentTimeStep, numEle) ], Format Gmsh, LastTimeStepOnly ];
     Print[ a_tot,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("microproblems/a_tot_p%g_cs%g_gp%g.pos" , iP, currentTimeStep, numEle) ], Format Gmsh, LastTimeStepOnly ];

     Print[ b_pert, OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("microproblems/b_pert_p%g_cs%g_gp%g.pos" , iP, currentTimeStep, numEle) ], Format Gmsh, LastTimeStepOnly ];
     Print[ b_tot,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("microproblems/b_tot_p%g_cs%g_gp%g.pos" , iP, currentTimeStep, numEle) ], Format Gmsh, LastTimeStepOnly ];
     Print[ b_proj,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("microproblems/b_proj_p%g_cs%g_gp%g.pos" , iP, currentTimeStep, numEle) ], Format Gmsh, LastTimeStepOnly ];

     Print[ e_tot ,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("microproblems/e_tot_p%g_cs%g_gp%g.pos" , iP, currentTimeStep, numEle) ], Format Gmsh, LastTimeStepOnly ];
     Print[ e_pert,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("microproblems/e_pert_p%g_cs%g_gp%g.pos" , iP, currentTimeStep, numEle) ], Format Gmsh, LastTimeStepOnly ];
     Print[ e_proj,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("microproblems/e_proj_p%g_cs%g_gp%g.pos" , iP, currentTimeStep, numEle) ], Format Gmsh, LastTimeStepOnly ];

     Print[ j_tot ,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("microproblems/j_tot_p%g_cs%g_gp%g.pos" , iP, currentTimeStep, numEle) ], Format Gmsh, LastTimeStepOnly ];
     Print[ j_pert,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("microproblems/j_pert_p%g_cs%g_gp%g.pos" , iP, currentTimeStep, numEle) ], Format Gmsh, LastTimeStepOnly ];
     Print[ j_proj,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("microproblems/j_proj_p%g_cs%g_gp%g.pos" , iP, currentTimeStep, numEle) ], Format Gmsh, LastTimeStepOnly ];

     Print[ h_tot,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("microproblems/h_tot_p%g_cs%g_gp%g.pos" , iP, currentTimeStep, numEle) ], Format Gmsh, LastTimeStepOnly ];
   }
 }
 { Name map_field_all~{iP} ; NameOfPostProcessing a_Micro_NR~{iP};
   Operation {
     Print[ a_tot,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("microproblems/a_tot_p%g_s%g.pos" , iP, currentTimeStep) ], Format Gmsh, LastTimeStepOnly ];
     Print[ b_tot,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("microproblems/b_tot_p%g_s%g.pos" , iP, currentTimeStep) ], Format Gmsh, LastTimeStepOnly ];
     Print[ h_tot,  OnElementsOf Omega, File StrCat[nameDirRes, Sprintf("microproblems/h_tot_p%g_s%g.pos" , iP, currentTimeStep) ], Format Gmsh, LastTimeStepOnly ];
   }
 }
 EndFor
}
