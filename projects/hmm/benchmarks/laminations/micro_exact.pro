Include "micro_exact.dat";
Include "output/grid.txt"
Include "BH.pro" // Data for non linear material

Group {
  Omega_Micro = Region[ {SURF1,SURF2} ] ;
  Omega_Micro1 = Region[ {SURF1} ] ;
  Omega_Micro2 = Region[ {SURF2} ] ;

  GammaLeft_Micro = Region[ {GAUCHE} ];
  GammaRight_Micro = Region[ {DROITE} ];
  GammaBottom_Micro = Region[ {BAS} ];
  GammaTop_Micro = Region[ {HAUT} ];
  DefineGroup[ DomainInf ] ; DefineVariable[ Val_Rint, Val_Rext ] ;
}

Function {
  NbrMaxIter = 100;
  Eps        = 1e-4;
  v_interpolation[]  = InterpolationBilinear[$1,$2] {List[List_XY_potential]};
  sigma_0         = 60e6;
  e_s             = 0.5;// threshold value of electric field for non linearities
  a               = 2;
  b               = 1.5;
  c               = 1.;
  d               = 0.5;
  e               = 0.5;
  g               = 5;
  eps             = lx;
  epsilon         = 1e-8;

  mu0  = 4.e-7 * Pi ;
  murIron = 1000; // For linear cases
  muIron  = mu0 * murIron ;
  mu_1 = muIron;
  mu_2 = mu0;
  K = 1.;
  m = 0.3;
  sigmaIron = 5e6 ;
  Nbr_SubProblems = 3;
  Pert~{1}[] = Vector[0, 0, 0];
  Pert~{2}[] = epsilon * Vector[1.0, 0.0, 0];
  Pert~{3}[] = epsilon * Vector[0.0, 1.0, 0];
  mu_XY[Omega_Micro1]  = mu_1;
  mu_XY[Omega_Micro2]  = mu_2;
  mu_XY_Tensor[Omega_Micro1]  = mu_1 * TensorSym[1.0, 0.0, 0.0, 1.0, 0.0, 1.0];
  mu_XY_Tensor[Omega_Micro2]  = mu_2 * TensorSym[1.0, 0.0, 0.0, 1.0, 0.0, 1.0];
  Flag_NL = 0; //for linear(=0)/nonlinear(!0) problems

  If(Flag_NL)
  j_Micro_NR[Omega_Micro1]    = mu_1 * 1/( (1 + K * SquNorm[$1#3] )^m) * #3 ;
  dj_de_Micro_NR[Omega_Micro1] = mu_1 * ( 1/( (1 + K * SquNorm[$1#2] )^m) * TensorDiag[1,1,1] - m * K / ( (1 + K * SquNorm[#2] )^(m+1) ) * SquDyadicProduct[#2] );
  j_Micro_NR[Omega_Micro2] = mu_XY[] * $1;
  dj_de_Micro_NR[Omega_Micro2] = mu_XY[] * TensorDiag[1.,1.,1.];
  mu_Micro[] = mu_XY[] * TensorDiag[1., 1., 1.] *  1/( (1 + K * SquNorm[$1#3] )^m) ;
  EndIf

  If(!Flag_NL)   //Linear constitutive law (testing convergence)
  j_Micro_NR[] = mu_XY_Tensor[] * $1;
  dj_de_Micro_NR[] = mu_XY_Tensor[];
  mu_Micro[] = mu_XY_Tensor[] ;
  EndIf
    }

Constraint{
  { Name v_Micro ; Case{
      { Region GammaBottom_Micro ; Value 0. ; }
      { Region GammaTop_Micro    ; Value 0. ; }
      { Region GammaRight_Micro ; Type Link ; RegionRef GammaLeft_Micro ;
        Coefficient 1. ; Function Vector[$X-lx, $Y, $Z] ; }
    }
  }

  { Name v_Interpolated; Type Assign;
    Case{
      { Region Omega_Micro; Value v_interpolation[ X[], Y[] ]; }
    }
  }
}

// ----------------------------- Jacobian ------------------------------------
Jacobian {
  { Name Vol ;
    Case  {
        { Region DomainInf ; Jacobian VolSphShell {Val_Rint, Val_Rext} ; }
        { Region All ; Jacobian Vol ; }
    }
  }
  { Name Sur ;
    Case {
      { Region All ; Jacobian Sur ; }
    }
  }
}
// ------------------------------------------------------------------------------
// ----------------------------- Integration ------------------------------------
Integration {
  { Name II ; Case { {Type Gauss ; Case {
          { GeoElement Triangle    ; NumberOfPoints  4 ; }
          { GeoElement Quadrangle  ; NumberOfPoints  nb_gauss_quad ; }
          { GeoElement Tetrahedron ; NumberOfPoints  4 ; }
          { GeoElement Hexahedron  ; NumberOfPoints  6 ; }
          { GeoElement Prism       ; NumberOfPoints  9 ; } }
      }
    }
  }
}

// ------------------------------------------------------------------------------

FunctionSpace{
  { Name Hgrad_v_Interpolated ; Type Form0 ;
    BasisFunction{
      { Name sn ; NameOfCoef vn ; Function BF_Node ;
        Support Omega_Micro ; Entity NodesOf[ All ] ; }
    }
    Constraint{
      { NameOfCoef vn; EntityType NodesOf; NameOfConstraint v_Interpolated; }
    }
  }

  { Name Hgrad_v_Proj ; Type Form0 ;
    BasisFunction{
      { Name sn ; NameOfCoef vn ; Function BF_Node ;
        Support Omega_Micro ; Entity NodesOf[ All ] ; }
    }
  }

  { Name Hgrad_v_Micro ; Type Form0 ;
    BasisFunction{
      { Name sn ; NameOfCoef vn ; Function BF_Node ;
        Support Omega_Micro ; Entity NodesOf[ All ] ; }
      { Name sf ; NameOfCoef vf ; Function BF_Region ;
        Support Omega_Micro ; Entity Omega_Micro ; }
    }
    GlobalQuantity{
      { Name Vf_ct ; Type AliasOf ; NameOfCoef vf ; }
    }
    SubSpace{
      { Name Vn ; NameOfBasisFunction {sn} ; } //fluctuations au niveau de la microstructure
      { Name Vf ; NameOfBasisFunction {sf} ; } //composante globale du potentiel
    }
    Constraint{
      { NameOfCoef vn ; EntityType NodesOf ; NameOfConstraint v_Micro ; }
    }
  }

}

Formulation {
  { Name v_Interpolated ; Type FemEquation ;
    Quantity{
      { Name vi ; Type Local ; NameOfSpace Hgrad_v_Interpolated ; }
    }
    Equation{
      Galerkin { [ Dof{vi} , {vi} ] ;
        In Omega_Micro ; Jacobian Vol ; Integration II ; }
    }
  }

  { Name v_Proj ; Type FemEquation ;
    Quantity
      {
        { Name vp ; Type Local ; NameOfSpace Hgrad_v_Proj ; }
        { Name vi ; Type Local ; NameOfSpace Hgrad_v_Interpolated;}
      }
    Equation{
        Galerkin { [ Dof{vp} , {vp} ] ;
          In Omega_Micro ; Jacobian Vol ; Integration II ; }
        Galerkin { [ - {vi}, {vp} ] ;
          In Omega_Micro ; Jacobian Vol ; Integration II ; }
      }
  }

  For iSub In {1:Nbr_SubProblems}

  { Name v_Micro_NR~{iSub} ; Type FemEquation ;
    Quantity {
      { Name v  ; Type Local ; NameOfSpace Hgrad_v_Micro ; }
      { Name vn ; Type Local ; NameOfSpace Hgrad_v_Micro[Vn] ; }
      { Name vf ; Type Local ; NameOfSpace Hgrad_v_Micro[Vf] ; }
      { Name vp ; Type Local; NameOfSpace Hgrad_v_Proj; }
    }
    Equation{
      Galerkin { [ -j_Micro_NR[-{d v}-{d vp} + Pert~{iSub}[-{d v}-{d vp}]  ] , {d v} ] ;
        In Omega_Micro ; Jacobian Vol ; Integration II ; }
      Galerkin { JacNL[ dj_de_Micro_NR[-{d v}-{d vp} + Pert~{iSub}[-{d v}-{d vp}] ] * Dof{d v} , {d v} ] ;
        In Omega_Micro ; Jacobian Vol ; Integration II ; }
      Galerkin { [ Dof{v} , {vf} ] ;
        In Omega_Micro ; Jacobian Vol ; Integration II ; }
    }
  }

  { Name v_Micro_Picard~{iSub} ; Type FemEquation ;
    Quantity{
      { Name v  ; Type Local ; NameOfSpace Hgrad_v_Micro ; }
      { Name vn ; Type Local ; NameOfSpace Hgrad_v_Micro[Vn] ; }
      { Name vf ; Type Local ; NameOfSpace Hgrad_v_Micro[Vf] ; }
      { Name vp ; Type Local ; NameOfSpace Hgrad_v_Proj; }
    }
    Equation {
      Galerkin { [ mu_Micro[ -{d v} - {d vp} + Pert~{iSub}[- {d v} - {d vp}] ] * Dof{d v} , {d v} ] ;
        In Omega_Micro ; Jacobian Vol ; Integration II ; }
      Galerkin { [ mu_Micro[ -{d v} - {d vp} + Pert~{iSub}[- {d v} - {d vp}] ] * {d vp} , {d v} ] ;
        In Omega_Micro ; Jacobian Vol ; Integration II ; }
      Galerkin { [ Dof{v} , {vf} ] ;
        In Omega_Micro ; Jacobian Vol ; Integration II ; }
    }
  }
  EndFor
}

Resolution {
  { Name Electrostatics_NR ;
    System {
      { Name I ; NameOfFormulation v_Interpolated ;}
      { Name P ; NameOfFormulation v_Proj         ;}
      For iSub In {1:Nbr_SubProblems}
        { Name Micro~{iSub} ; NameOfFormulation v_Micro_NR~{iSub} ;}
      EndFor
    }
    Operation {
      InitSolution [I];
      Generate [P] ; Solve [P] ; SaveSolution [P] ;
      For iSub In {1:Nbr_SubProblems}
      InitSolution[Micro~{iSub}];
      IterativeLoop[NbrMaxIter, Eps, 1.]{
        GenerateJac[Micro~{iSub}] ; SolveJac[Micro~{iSub}] ;
      }
      SaveSolution[Micro~{iSub}];
      EndFor
    }
  }

  { Name Electrostatics_Picard ;
    System {
      { Name I ; NameOfFormulation v_Interpolated ;}
      { Name P ; NameOfFormulation v_Proj         ;}
      For iSub In {1:Nbr_SubProblems}
        { Name Micro~{iSub}    ; NameOfFormulation v_Micro_Picard~{iSub} ;}
      EndFor
    }
    Operation {
      InitSolution [I];
      Generate [P] ; Solve [P] ; SaveSolution [P] ;
      For iSub In {1:Nbr_SubProblems}
        InitSolution[Micro~{iSub}];
        IterativeLoop[NbrMaxIter, Eps, 1.]{
          GenerateJac[Micro~{iSub}] ; SolveJac[Micro~{iSub}] ;
        }
        SaveSolution[Micro~{iSub}];
        EndFor
          }
  }
}

PostProcessing{

  { Name v_Proj ; NameOfFormulation v_Proj ;
    PostQuantity {
      { Name v_Interpolated  ; Value { Term { [ {vi} ] ; In Omega_Micro ; Jacobian Vol; } } }
      { Name v_proj          ; Value { Term { [ {vp} ] ; In Omega_Micro ; Jacobian Vol; } } }
    }
  }

  For iSub In {1:Nbr_SubProblems}
  { Name v_Micro_NR~{iSub} ; NameOfFormulation v_Micro_NR~{iSub} ;
    PostQuantity{
      { Name j_Micro     ; Value { Term { [ j_Micro_NR[-{d v}-{d vp}+Pert~{iSub}[-{d v}-{d vp}] ]] ; In Omega_Micro ; Jacobian Vol;} } }
      { Name mu     ; Value { Term { [ mu_XY[ ]] ; In Omega_Micro ; Jacobian Vol;} } }
      { Name dj_de_Micro ; Value { Term { [ dj_de_Micro_NR[-{d v}-{d vp}+Pert~{iSub}[-{d v}-{d vp}] ]] ; In Omega_Micro ; Jacobian Vol;} } }
      { Name vol         ; Value { Integral { [ 1. ] ;In Omega_Micro ; Jacobian Vol ; Integration II ; } } }  // stored in register #12
      { Name v           ; Value { Term { [ {v} ] ; In Omega_Micro ; Jacobian Vol; } } }
      { Name v_proj      ; Value { Term { [ {vp} ] ;  In Omega_Micro ; Jacobian Vol;  } } }
      { Name v_tot       ; Value {
          Term { [ {v} ] ; In Omega_Micro ; Jacobian Vol; }
          Term { [ {vp} ] ; In Omega_Micro ; Jacobian Vol;  } } }
      { Name e           ; Value { Term { [ -{d v} ] ; In Omega_Micro ; Jacobian Vol;  } } }
      { Name e_proj      ; Value { Term { [ -{d vp} ] ; In Omega_Micro ; Jacobian Vol;  } } }
      { Name e_tot       ; Value {
          Term { [(-{d v}) ] ; In Omega_Micro ; Jacobian Vol ; Integration II ; }
          Term { [ (Pert~{iSub}[-{d v}-{d vp}]) ] ; In Omega_Micro ; Jacobian Vol ; Integration II ; }
          Term { [ -{d vp} ] ; In Omega_Micro ; Jacobian Vol ; Integration II ; } } }
      { Name e_tot_mean ; Value {
          Integral { [ (-{d v})/#12 ] ; In Omega_Micro ; Jacobian Vol ; Integration II ; }
          Integral { [ (Pert~{iSub}[-{d v}-{d vp}])/#12 ] ; In Omega_Micro ; Jacobian Vol ; Integration II ; }
          Integral { [ (-{d vp})/#12 ] ; In Omega_Micro ; Jacobian Vol ; Integration II ; } } }
      { Name j_tot ; Value {
            Term { [ (j_Micro_NR[-{d v}-{d vp} + Pert~{iSub}[-{d v}-{d vp}] ] ) ] ; In Omega_Micro ; Jacobian Vol ; Integration II ; } } }
      { Name j_tot_mean ; Value {// stored in #22
          Integral { [ (j_Micro_NR[-{d v}-{d vp}+Pert~{iSub}[-{d v}-{d vp}] ])/#12 ] ; In Omega_Micro ; Jacobian Vol ; Integration II ;} } }


      { Name Energy_tot_mean ; Value { // stored in #20
          Integral { [j_Micro_NR[-{d v}-{d vp} + Pert~{iSub}[-{d v}-{d vp}] ] * (-{d v } - {d vp} + Pert~{iSub}[-{d v}-{d vp}]) / #12] ;
            In Omega_Micro ; Jacobian Vol ; Integration II ; } } }
      { Name SquareMeanE ; Value { Term { Type Global; [ Norm[#21]^2 ] ; In Omega_Micro ; } } } // (e_tot_mean)^2 ( stored in #21)
      { Name sigma_homo_MeanEnergyOnSquareMeanE ; Value { Term { Type Global; [ #20/Norm[#21]^2 ] ; In Omega_Micro ; } } } // Energy stored in #20, e in #21

      }
  }

  { Name v_Micro_Picard~{iSub} ; NameOfFormulation v_Micro_Picard~{iSub} ;
    PostQuantity{
      { Name mu_Micro ; Value { Term { [ mu_Micro[-{d v}-{d vp} + Pert~{iSub}[{d v}+{d vp}] ] ] ; In Omega_Micro ; Jacobian Vol;} } }
      //{ Name dsigma_de_Micro ; Value { Term { [ dsigma_de_Micro[- {d v} - {d vp} + Pert~{iSub}[-{d v} - {d vp}] ] ] ; In Omega_Micro ; Jacobian Vol;} } }
      { Name vol    ; Value { Integral { [ 1. ] ;In Omega_Micro ; Jacobian Vol ; Integration II ; } } }  // stored in register #12
      { Name v      ; Value { Term { [ {v} ] ; In Omega_Micro ; Jacobian Vol; } } }
      { Name v_proj ; Value { Term { [ {vp} ] ;  In Omega_Micro ; Jacobian Vol;  } } }
      { Name v_tot  ; Value { Term { [ {v} ] ; In Omega_Micro ; Jacobian Vol; }
          Term { [ {vp} ] ; In Omega_Micro ; Jacobian Vol;  } } }
      { Name e ; Value { Term { [ -{d v} ] ; In Omega_Micro ; Jacobian Vol;  } } }
      { Name e_proj ; Value { Term { [ -{d vp} ] ; In Omega_Micro ; Jacobian Vol;  } } }
      { Name e_tot ; Value {
          Term { [(-{d v}) ] ; In Omega_Micro ; Jacobian Vol ; Integration II ; }
          Term { [ (Pert~{iSub}[{d v}+{d vp}]) ] ; In Omega_Micro ; Jacobian Vol ; Integration II ; }
          Term { [ -{d vp} ] ; In Omega_Micro ; Jacobian Vol ; Integration II ; } } }
      { Name e_tot_mean ; Value {
          Integral { [ (-{d v})                        /#12 ] ; In Omega_Micro ; Jacobian Vol ; Integration II ; }
          Integral { [ (Pert~{iSub}[{d v}+{d vp}])/#12 ] ; In Omega_Micro ; Jacobian Vol ; Integration II ; }
          Integral { [ (-{d vp})                   /#12 ] ; In Omega_Micro ; Jacobian Vol ; Integration II ; } } }
      { Name j_tot ; Value {
          Term { [ -mu_Micro[-{d v}-{d vp} + Pert~{iSub}[-{d v}-{d vp}] ] * {d v                            } ] ; In Omega_Micro ; Jacobian Vol ; Integration II ; }
          Term { [ -mu_Micro[-{d v}-{d vp} + Pert~{iSub}[{d v}+{d vp}] ] * {d vp                       } ] ; In Omega_Micro ; Jacobian Vol ; Integration II ; }
          Term { [  -mu_Micro[-{d v}-{d vp} + Pert~{iSub}[-{d v}-{d vp}] ] * Pert~{iSub}[-{d v}-{d vp} ] ] ; In Omega_Micro ; Jacobian Vol ; Integration II ; } } }
      { Name j_tot_mean ; Value {// stored in #22
          Integral { [ (-mu_Micro[-{d v}-{d vp} + Pert~{iSub}[-{d v}-{d vp}] ] * {d v} )                            /#12 ] ; In Omega_Micro ; Jacobian Vol ; Integration II ;}
          Integral { [ (-mu_Micro[-{d v}-{d vp} + Pert~{iSub}[-{d v}-{d vp}] ] * {d vp} )                       /#12 ] ; In Omega_Micro ; Jacobian Vol ; Integration II ;}
          Integral { [ (-mu_Micro[-{d v}-{d vp} + Pert~{iSub}[-{d v}-{d vp}] ] * Pert~{iSub}[-{d v}-{d vp} ] )/#12 ] ; In Omega_Micro ; Jacobian Vol ; Integration II ;} } }
    }
  }

  EndFor
    }

nameDirRes = "output/";
nameDirRes_ref = "output/";
n = 10;

For iSub In {1:Nbr_SubProblems}
fileExt~{iSub} = Str[ Sprintf("%g", iSub) ];
fileExt_txt~{iSub} = StrCat[fileExt~{iSub}, ".txt"];
fileExt_pos~{iSub} = StrCat[fileExt~{iSub}, ".pos"];
EndFor

PostOperation {
  For iSub In {1:Nbr_SubProblems}
  { Name sigma_mean~{iSub} ; NameOfPostProcessing v_Micro_NR~{iSub};
    Operation{
      Print[ vol[Omega_Micro], OnGlobal, Format Table, Store 12, File "output/vol.txt" ] ;
      Print[ j_tot_mean[Omega_Micro], OnGlobal, Format Table, Store 22, File StrCat[StrCat[nameDirRes, "j"]     , fileExt_txt~{iSub}] ] ;
      Print[ e_tot_mean[Omega_Micro], OnGlobal, Format Table, Store 21, File StrCat[StrCat[nameDirRes, "e"]     , fileExt_txt~{iSub}] ] ;
      Print[ Energy_tot_mean[Omega_Micro], OnGlobal, Format Table, Store 20, File StrCat[StrCat[nameDirRes, "Energy"], fileExt_txt~{iSub}] ] ;
      Print[ sigma_homo_MeanEnergyOnSquareMeanE, OnRegion Omega_Micro, Format Table, File StrCat[StrCat[nameDirRes, "sigmaH"], fileExt_txt~{iSub}] ] ;
    }
  }

 { Name sigma_mean_Picard~{iSub} ; NameOfPostProcessing v_Micro_Picard~{iSub};
   Operation{
     Print[ vol[Omega_Micro], OnGlobal, Format Table, Store 12, File "vol.txt" ] ;
     Print[ j_tot_mean[Omega_Micro],      OnGlobal, Format Table, Store 22, File StrCat[StrCat[nameDirRes, "j"]     , fileExt_txt~{iSub}] ] ;
     Print[ e_tot_mean[Omega_Micro],      OnGlobal, Format Table, Store 21, File StrCat[StrCat[nameDirRes, "e"]     , fileExt_txt~{iSub}] ] ;
     //Print[ Energy_tot_mean[Omega_Micro], OnGlobal, Format Table, Store 20, File StrCat[StrCat[nameDirRes, "Energy"], fileExt_txt~{iSub}] ] ;
     //Print[ sigma_homo_MeanEnergyOnSquareMeanE, OnRegion Omega_Micro, Format Table, File StrCat[StrCat[nameDirRes, "sigmaH"], fileExt_txt~{iSub}] ] ;
   }
 }

 EndFor

 { Name cuts_Proj ; NameOfPostProcessing v_Proj;
   Operation{
     Print[ v_Interpolated, OnElementsOf Omega_Micro, File StrCat[nameDirRes, "v_Interpolated.pos"] ];
     Print[ v_proj, OnElementsOf Omega_Micro, File StrCat[nameDirRes, "v_proj.pos"] ];
   }
 }

 For iSub In {1:Nbr_SubProblems}
 { Name cuts_field~{iSub} ; NameOfPostProcessing v_Micro_NR~{iSub};
   Operation{

     //Print[ vol[Omega_Micro], OnGlobal, Format Table, Store 12, File "kkkk_v.txt" ] ;
     /*
       Print[ v, OnGrid { $A, $B, 0} { {x_gauss-lx/2 : x_gauss+lx/2 : 1./np}, {y_gauss}, {0.} },
       File StrCat[StrCat[nameDirRes_ref, "v_pert"], fileExt_txt~{iSub}], Format Table];
        Print[ vp, OnGrid { $A, $B, 0} { {x_gauss-lx/2 : x_gauss+lx/2 : 1./np}, {y_gauss}, {0.} },
        File StrCat[StrCat[nameDirRes_ref, "v_proj"], fileExt_txt~{iSub}], Format Table];
        Print[ v_tot, OnGrid { $A, $B, 0} { {x_gauss-lx/2 : x_gauss+lx/2 : 1./np}, {y_gauss}, {0.} },
        File StrCat[StrCat[nameDirRes_ref, "v_tot"], fileExt_txt~{iSub}], Format Table];
     */
     Print[ v, OnElementsOf Omega_Micro, File StrCat[StrCat[nameDirRes_ref, "v_pert"], fileExt_pos~{iSub}] ];
     Print[ v_proj, OnElementsOf Omega_Micro, File StrCat[StrCat[nameDirRes_ref, "v_proj"], fileExt_pos~{iSub}] ];
     Print[ v_tot, OnElementsOf Omega_Micro, File StrCat[StrCat[nameDirRes_ref, "v_tot"], fileExt_pos~{iSub}] ];
     Print[ e_tot, OnElementsOf Omega_Micro, File StrCat[StrCat[nameDirRes_ref, "e_tot"], fileExt_pos~{iSub}] ];
     Print[ j_tot, OnElementsOf Omega_Micro, File StrCat[StrCat[nameDirRes_ref, "j_tot"], fileExt_pos~{iSub}] ];
     Print[ mu, OnElementsOf Omega_Micro, File StrCat[StrCat[nameDirRes_ref, "mu"], fileExt_pos~{iSub}] ];
   }
 }


 { Name cuts_Picard~{iSub} ; NameOfPostProcessing v_Micro_Picard~{iSub};
   Operation{
     Print[ vol[Omega_Micro], OnGlobal, Format Table, Store 12 ] ;
     Print[ v, OnGrid { $A, $B, 0} { {x_gauss-lx/2 : x_gauss+lx/2 : 1./np}, {y_gauss}, {0.} },
            File StrCat[StrCat[nameDirRes_ref, "v_pert"], fileExt_txt~{iSub}], Format Table];
     Print[ v_proj, OnGrid { $A, $B, 0} { {x_gauss-lx/2 : x_gauss+lx/2 : 1./np}, {y_gauss}, {0.} },
            File StrCat[StrCat[nameDirRes_ref, "v_proj"], fileExt_txt~{iSub}], Format Table];
       Print[ v_tot, OnGrid { $A, $B, 0} { {x_gauss-lx/2 : x_gauss+lx/2 : 1./np}, {y_gauss}, {0.} },
              File StrCat[StrCat[nameDirRes_ref, "v_tot"], fileExt_txt~{iSub}], Format Table];
       Print[ v, OnElementsOf Omega_Micro, File StrCat[StrCat[nameDirRes_ref, "v_pert"], fileExt_pos~{iSub}] ];
       Print[ v_proj, OnElementsOf Omega_Micro, File StrCat[StrCat[nameDirRes_ref, "v_proj"], fileExt_pos~{iSub}] ];
       Print[ v_tot, OnElementsOf Omega_Micro, File StrCat[StrCat[nameDirRes_ref, "v_tot"], fileExt_pos~{iSub}] ];
       Print[ e_tot, OnElementsOf Omega_Micro, File StrCat[StrCat[nameDirRes_ref, "e_tot"], fileExt_pos~{iSub}] ];
       Print[ j_tot, OnElementsOf Omega_Micro, File StrCat[StrCat[nameDirRes_ref, "j_tot"], fileExt_pos~{iSub}] ];
   }
 }

 EndFor
   }
