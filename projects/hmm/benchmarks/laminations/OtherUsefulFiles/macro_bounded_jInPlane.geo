Mesh.Algorithm = 1; // 2D mesh algorithm (1=MeshAdapt, 2=Automatic, 5=Delaunay, 6=Frontal, 7=bamg, 8=delquad)
Mesh.CharacteristicLengthFactor = 4 ;
Include "macro_bounded_jInPlane.dat";
s = 100;
//s = 50;
//lc2 = 0.3/s;
lc2 = 0.8/s;
lc3 = 0.8/s;

Delta_1  = L_X/6;
Delta_78 = 0.5 * (1 - Delta_1);
p1 = newp; Point(p1) = {0.0                , 0.0  , 0.0,lc3};
p2 = newp; Point(p2) = {L_X                , 0.0  , 0.0,lc3} ;
p3 = newp; Point(p3) = {L_X                , L_Y  , 0.0,lc3} ;
p4 = newp; Point(p4) = {0.0                , L_Y  , 0.0,lc3} ;
p5 = newp; Point(p5) = {0.5 * L_X - Delta_1, 0.0  , 0.0,lc2} ;
p6 = newp; Point(p6) = {0.5 * L_X + Delta_1, 0.0  , 0.0,lc2} ;
p7 = newp; Point(p7) = {0.5 * L_X - Delta_1, L_Y  , 0.0,lc2} ;
p8 = newp; Point(p8) = {0.5 * L_X + Delta_1, L_Y  , 0.0,lc2} ;
l1 = newl; Line(l1) = {p5,p6} ;
l2 = newl; Line(l2) = {p2,p3} ;
l3 = newl; Line(l3) = {p8,p7} ;
l4 = newl; Line(l4) = {p4,p1} ;
l5 = newl; Line(l5) = {p6,p2} ;
l6 = newl; Line(l6) = {p1,p5} ;
l7 = newl; Line(l7) = {p3,p8} ;
l8 = newl; Line(l8) = {p7,p4} ;
ll5 = newll; Line Loop(ll5) = {l6,l1,l5,l2,l7,l3,l8,l4} ;

//===========================================================================================

// Outer circle (C1)
//==================

p16 = newp; Point(p16) = {c_x,       c_y,       0.0};
p17 = newp; Point(p17) = {r1 + c_x,  c_y,       0.0, lc_Circ1};
p18 = newp; Point(p18) = {c_x,       r1 + c_y,  0.0, lc_Circ1};
p19 = newp; Point(p19) = {-r1 + c_x, c_y,       0.0, lc_Circ1};
p20 = newp; Point(p20) = {c_x,       -r1 + c_y, 0.0, lc_Circ1};

c1 = newl; Circle(c1) = {p17, p16, p18};
c2 = newl; Circle(c2) = {p18, p16, p19};
c3 = newl; Circle(c3) = {p19, p16, p20};
c4 = newl; Circle(c4) = {p20, p16, p17};

ll17 = newll; Line Loop(ll17) = {c1, c2, c3, c4} ;
//ll18 = newll; Line Loop(ll18) = {l8, l1, l9, right[], l35, l33, l36, left[]};
s19 = news  ; Plane Surface(s19) = {ll17, ll5} ;


// Outer circle (C2)
//==================

p21 = newp; Point(p21) = {r1 + r2 + c_x,  c_y,           0.0, lc_Circ2};
p22 = newp; Point(p22) = {c_x,            r1 + r2 + c_y,  0.0, lc_Circ2};
p23 = newp; Point(p23) = {-r1 - r2 + c_x, c_y,           0.0, lc_Circ2};
p24 = newp; Point(p24) = {c_x,            -r1 - r2 + c_y, 0.0, lc_Circ2};

c5 = newl; Circle(c5) = {p21, p16, p22};
c6 = newl; Circle(c6) = {p22, p16, p23};
c7 = newl; Circle(c7) = {p23, p16, p24};
c8 = newl; Circle(c8) = {p24, p16, p21};

l20 = newl; Line(l20) = {p17, p21};
l21 = newl; Line(l21) = {p22, p18};
l22 = newl; Line(l22) = {p19, p23};
l23 = newl; Line(l23) = {p24, p20};

// Outer circle (C3)
//==================

p25 = newp; Point(p25) = {r1 + r2 + r3 + c_x,  c_y,                 0.0, lc_Circ3};
p26 = newp; Point(p26) = {c_x,                 r1 + r2 + r3 + c_y,  0.0, lc_Circ3};
p27 = newp; Point(p27) = {-r1 - r2 - r3 + c_x, c_y,                 0.0, lc_Circ3};
p28 = newp; Point(p28) = {c_x,                 -r1 - r2 - r3 + c_y, 0.0, lc_Circ3};

c9  = newl; Circle(c9)  = {p25, p16, p26};
c10 = newl; Circle(c10) = {p26, p16, p27};
c11 = newl; Circle(c11) = {p27, p16, p28};
c12 = newl; Circle(c12) = {p28, p16, p25};

l24 = newl; Line(l24) = {p21, p25};
l25 = newl; Line(l25) = {p26, p22};
l26 = newl; Line(l26) = {p23, p27};
l27 = newl; Line(l27) = {p28, p24};

circle_2[] = {};
circle_3[] = {};

If(Flag_TriMesh_SourceOuter)
  ll20 = newll; Line Loop(ll20) = {c5, c6, c7, c8} ;
  s21 = news  ; Plane Surface(s21) = {ll20, ll17} ;
  circle2[] += {s21};

  ll22 = newll; Line Loop(ll22) = {c9, c10, c11, c12} ;
  s23 = news  ; Plane Surface(s23) = {ll22, ll20} ;
  circle3[] += {s23};
EndIf
If(!Flag_TriMesh_SourceOuter)
  ll13 = newll; Line Loop(ll13)   = {l20,c5,l21,-c1} ;
  s14  = news ; Plane Surface(s14) = {ll13} ;

  ll15 = newll; Line Loop(ll15)   = {-l21,c6,-l22,-c2} ;
  s16  = news ; Plane Surface(s16) = {ll15} ;

  ll17 = newll; Line Loop(ll17)   = {l22,c7,l23,-c3} ;
  s18  = news ; Plane Surface(s18) = {ll17} ;

  ll19 = newll; Line Loop(ll19)   = {-l23,c8,-l20,-c4} ;
  s20  = news ; Plane Surface(s20) = {ll19} ;

  Transfinite Line{l20, l21, l22, l23}           = n_Circ1Tan+1;
  Transfinite Line{c1, c2, c3, c4, c5, c6, c7, c8} = n_Circ1Rad+1;

  Transfinite Surface{s14} = {p17, p21, p22, p18}; Recombine Surface{s14} ;
  Transfinite Surface{s16} = {p18, p22, p23, p19}; Recombine Surface{s16} ;
  Transfinite Surface{s18} = {p19, p23, p24, p20}; Recombine Surface{s18} ;
  Transfinite Surface{s20} = {p20, p24, p21, p17}; Recombine Surface{s20} ;
  circle2[] += {s14, s16, s18, s20};


  ll13 = newll; Line Loop(ll13)   = {l24,c9,l25,-c5} ;
  s14  = news ; Plane Surface(s14) = {ll13} ;

  ll15 = newll; Line Loop(ll15)   = {-l25,c10,-l26,-c6} ;
  s16  = news ; Plane Surface(s16) = {ll15} ;

  ll17 = newll; Line Loop(ll17)   = {l26,c11,l27,-c7} ;
  s18  = news ; Plane Surface(s18) = {ll17} ;

  ll19 = newll; Line Loop(ll19)   = {-l27,c12,-l24,-c8} ;
  s20  = news ; Plane Surface(s20) = {ll19} ;

  Transfinite Line{l24, l25, l26, l27} = n_Circ2Tan+1;
  Transfinite Line{c9, c10, c11, c12}  = n_Circ1Rad+1;

  Transfinite Surface{s14} = {p21, p25, p26, p22}; Recombine Surface{s14} ;
  Transfinite Surface{s16} = {p22, p26, p27, p23}; Recombine Surface{s16} ;
  Transfinite Surface{s18} = {p23, p27, p28, p24}; Recombine Surface{s18} ;
  Transfinite Surface{s20} = {p24, p28, p25, p21}; Recombine Surface{s20} ;
  circle3[] += {s14, s16, s18, s20};
EndIf

//===========================================================================================

s6 = news; Plane Surface(s6) = {ll5} ;
Physical Point(CORNER)  = {p1} ;
Physical Line(UP)  = {l3} ;
Physical Line(DOWN) = {l1} ;
Physical Surface(SURF) = {s6} ;
Physical Surface(AIR1) = {s19} ;
Physical Surface(OMEGA_S) = {circle2[]} ;
Physical Surface(AIR2) = {circle3[] } ;
