Mesh.Algorithm = 1; // 2D mesh algorithm (1=MeshAdapt, 2=Automatic, 5=Delaunay, 6=Frontal, 7=bamg, 8=delquad)
Mesh.CharacteristicLengthFactor = 4 ;
Include "macro_bounded_jPerp.dat";
s = 100;
//s = 50;
//lc2 = 0.3/s;
//lc3 = 0.4/s;

lc2 = 0.2/s;
lc3 = 0.2/s;

Delta_1  = L_X/6;
Delta_78 = 0.5 * (1 - Delta_1);
p1 = newp; Point(p1) = {0.0                , 0.0  , 0.0,lc3};
p2 = newp; Point(p2) = {L_X                , 0.0  , 0.0,lc3} ;
p3 = newp; Point(p3) = {L_X                , L_Y  , 0.0,lc3} ;
p4 = newp; Point(p4) = {0.0                , L_Y  , 0.0,lc3} ;
p5 = newp; Point(p5) = {0.5 * L_X - Delta_1, 0.0  , 0.0,lc2} ;
p6 = newp; Point(p6) = {0.5 * L_X + Delta_1, 0.0  , 0.0,lc2} ;
p7 = newp; Point(p7) = {0.5 * L_X - Delta_1, L_Y  , 0.0,lc2} ;
p8 = newp; Point(p8) = {0.5 * L_X + Delta_1, L_Y  , 0.0,lc2} ;
l1 = newl; Line(l1) = {p5,p6} ;
l2 = newl; Line(l2) = {p2,p3} ;
l3 = newl; Line(l3) = {p8,p7} ;
l4 = newl; Line(l4) = {p4,p1} ;
l5 = newl; Line(l5) = {p6,p2} ;
l6 = newl; Line(l6) = {p1,p5} ;
l7 = newl; Line(l7) = {p3,p8} ;
l8 = newl; Line(l8) = {p7,p4} ;
ll5 = newll; Line Loop(ll5) = {l6,l1,l5,l2,l7,l3,l8,l4} ;
s6 = news; Plane Surface(s6) = {ll5} ;

//========================================================================================
// Outer circle (C1)
//==================

p16 = newp; Point(p16) = {c_x,    c_y,    0.0};
p17 = newp; Point(p17) = {r+c_x,  c_y,    0.0, lc_Circ1};
p18 = newp; Point(p18) = {c_x,    r+c_y,  0.0, lc_Circ1};
p19 = newp; Point(p19) = {-r+c_x, c_y,    0.0, lc_Circ1};
p20 = newp; Point(p20) = {c_x,    -r+c_y, 0.0, lc_Circ1};

c1 = newl; Circle(c1) = {p17, p16, p18};
c2 = newl; Circle(c2) = {p18, p16, p19};
c3 = newl; Circle(c3) = {p19, p16, p20};
c4 = newl; Circle(c4) = {p20, p16, p17};

ll17 = newll; Line Loop(ll17) = {c1, c2, c3, c4} ;
s19 = news  ; Plane Surface(s19) = {ll17, ll5} ;

//========================================================================================
Physical Point(CORNER)  = {p1} ;
Physical Line(UP)  = {l3} ;
Physical Line(DOWN) = {l1} ;
Physical Surface(SURF) = {s6} ;
Physical Line(GAMMA_INF) = {c1, c2, c3, c4} ;
Physical Surface(AIR) = {s19} ;
