Include "common.dat" ;
Include "macro_lam_jInPlane.dat";



Flag_AnalyticalFunc = 0;

If(!Flag_AnalyticalFunc)
n1_1    = 15;// * nbrT ;
n1_2    = 5;// * nbrT ;
n2      = 50;// * nbrT ;

//==============
i           = 0;
//==============

y_bottom[i] = (i) * eps;
y_middle[i] = (i) * eps + ratio * eps;
y_top[i]    = (i+1) * eps;

x_left[i]   = 0.0;//(i) * eps;
x_right[i]  = L_X;//(i+1) * eps;
Delta_1  =0.20/6.0;
Delta_78 = 0.5 * (1 - 10 * Delta_1);

p1 = newp; Point(p1) = {x_left[i]  , y_bottom[i], 0.0, lc_int};
p2 = newp; Point(p2) = {L_X        , y_bottom[i], 0.0, lc_int};
p3 = newp; Point(p3) = {L_X        , y_middle[i], 0.0, lc_int};
p4 = newp; Point(p4) = {L_X        , y_top[i]   , 0.0, lc_int};
p5 = newp; Point(p5) = {x_left[i] , y_top[i]    , 0.0, lc_int};
p6 = newp; Point(p6) = {x_left[i] , y_middle[i] , 0.0, lc_int};
p7 = newp; Point(p7) = {0.5 * L_X - Delta_1  , y_bottom[i], 0.0, lc_int};
p8 = newp; Point(p8) = {0.5 * L_X + Delta_1  , y_bottom[i], 0.0, lc_int};



//==============
vec_p1[] +={p1};
vec_p2[] +={p2};
vec_p3[] +={p3};
vec_p4[] +={p4};
vec_p5[] +={p5};
vec_p6[] +={p6};
vec_p7[] +={p7};
vec_p8[] +={p8};
//==============

l1 = newl ; Line(l1)  = {p7,p8} ;
l2 = newl ; Line(l2)  = {p2,p3} ;
l3 = newl ; Line(l3)  = {p3,p6} ;
l4 = newl ; Line(l4)  = {p6,p1} ;
l5 = newl ; Line(l5)  = {p3,p4} ;
l6 = newl ; Line(l6)  = {p4,p5} ;
l7 = newl ; Line(l7)  = {p5,p6} ;
l8 = newl ; Line(l8)  = {p1,p7} ;
l9 = newl ; Line(l9)  = {p8,p2} ;
//================
vec_l1[]  +={l1} ;
vec_l2[]  +={l2} ;
vec_l3[]  +={l3} ;
vec_l4[]  +={l4} ;
vec_l5[]  +={l5} ;
vec_l6[]  +={l6} ;
vec_l7[]  +={l7} ;
vec_l8[]  +={l8} ;
vec_l9[]  +={l9} ;
//================

ll13 = newll; Line Loop(ll13)   = {l8,l1,l9,l2,l3,l4} ;
s14 = news ; Plane Surface(s14) = {ll13} ;

ll15 = newll; Line Loop(ll15)   = {l5,l6,l7,-l3} ;
s16 = news ; Plane Surface(s16) = {ll15} ;

If(!Floor(Delta_78 * n2))
   n2_78 = 1;
EndIf
If(Floor(Delta_78 * n2))
   n2_78 = Floor(Delta_78 * n2);
EndIf

Transfinite Line{l2,l4}     = n1_1+1;
Transfinite Line{l5,l7}      = n1_2+1;
Transfinite Line{l8,l9}      = n2_78+1;//n2+1;
Transfinite Line{l1}         = n2 - 2 * (n2_78) + 1; //n2+1;
Transfinite Line{l3,l6}      = n2+1;
Transfinite Surface{s14} = {1,2,3,6}; Recombine Surface{s14} ;
Transfinite Surface{s16} = {3,4,5,6}; Recombine Surface{s16} ;

right[] += {l2, l5};
left[]  += {l7, l4};

inno1[] += {s14};
inno2[] += {s16};

For i In{1 : nbrT-1}

x_left[i]   = (i) * eps;
x_right[i]  = (i+1) * eps;
y_middle[i] = (i) * eps + ratio * eps;
y_top[i]    = (i+1) * eps;

p3 = newp; Point(p3) = {L_X        , y_middle[i] , 0.0, lc_int};
p4 = newp; Point(p4) = {L_X        , y_top[i]    , 0.0, lc_int};
p5 = newp; Point(p5) = {0.0        , y_top[i]    , 0.0, lc_int};
p6 = newp; Point(p6) = {0.0        , y_middle[i] , 0.0, lc_int};

//==============
vec_p3[] +={p3};
vec_p4[] +={p4};
vec_p5[] +={p5};
vec_p6[] +={p6};
//==============

l2 = newl ; Line(l2)  = {vec_p4[i-1],p3} ;
l3 = newl ; Line(l3)  = {p3,p6} ;
l4 = newl ; Line(l4)  = {p6,vec_p5[i-1]} ;
l5 = newl ; Line(l5)  = {p3,p4} ;
l6 = newl ; Line(l6)  = {p4,p5} ;
l7 = newl ; Line(l7)  = {p5,p6} ;

//================
vec_l2[]  +={l2} ;
vec_l3[]  +={l3} ;
vec_l4[]  +={l4} ;
vec_l5[]  +={l5} ;
vec_l6[]  +={l6} ;
vec_l7[]  +={l7} ;
//================

ll13 = newll; Line Loop(ll13)   = {(vec_l6[i-1]),-l2,-l3,-l4} ;
s14 = news ; Plane Surface(s14) = {-ll13} ;

ll15 = newll; Line Loop(ll15)   = {l5,l6,l7,-l3} ;
s16 = news ; Plane Surface(s16) = {ll15} ;

Transfinite Line{vec_l2[i],vec_l4[i]} = n1_1+1;
Transfinite Line{vec_l5[i],vec_l7[i]} = n1_2+1;
Transfinite Line{vec_l3[i],vec_l6[i]} = n2+1;
Transfinite Surface{s14,s16}; Recombine Surface{s14,s16} ;

right[] += {l2, l5};
left[]  += {l7, l4};

inno1[] += {s14};
inno2[] += {s16};

EndFor



// Last iron layer
//================
//x_13 = nbrT * eps + ratio * eps;
y_13 =( nbrT) * eps + ratio * eps; 

p12 = newp; Point(p12) = {L_X , y_13 , 0.0, lc_int};
p13 = newp; Point(p13) = {0.0 , y_13 , 0.0, lc_int};
p14 = newp; Point(p14) = {0.5 * L_X + Delta_1  , y_13, 0.0, lc_int};
p15 = newp; Point(p15) = {0.5 * L_X - Delta_1  , y_13, 0.0, lc_int};

l32 = newl ; Line(l32)  = {vec_p4[nbrT-1],p12} ;
l33 = newl ; Line(l33)  = {p14,p15} ;
l34 = newl ; Line(l34)  = {p13,vec_p5[nbrT-1]} ;
l35 = newl ; Line(l35)  = {p12,p14} ;
l36 = newl ; Line(l36)  = {p15,p13} ;

ll38 = newll; Line Loop(ll38) = {-vec_l6[nbrT-1],l32,l35,l33,l36,l34} ;
s39 = news ; Plane Surface(s39) = {ll38} ;

Transfinite Line{l32,l34} = n1_1+1;
Transfinite Line{l35,l36} = n2_78+1;
Transfinite Line{l33} = n2 - 2 * (n2_78) + 1;
Transfinite Surface{s39} = {vec_p4[nbrT-1],p12, p13, vec_p5[nbrT-1]}; Recombine Surface{s39} ;

right[] += {l32};
left[]  += {l34};

inno1[] += {s39};


// Outer circle (C1)
//==================

p16 = newp; Point(p16) = {c_x,       c_y,       0.0};
p17 = newp; Point(p17) = {r1 + c_x,  c_y,       0.0, lc_Circ1};
p18 = newp; Point(p18) = {c_x,       r1 + c_y,  0.0, lc_Circ1};
p19 = newp; Point(p19) = {-r1 + c_x, c_y,       0.0, lc_Circ1};
p20 = newp; Point(p20) = {c_x,       -r1 + c_y, 0.0, lc_Circ1};

c1 = newl; Circle(c1) = {p17, p16, p18};
c2 = newl; Circle(c2) = {p18, p16, p19};
c3 = newl; Circle(c3) = {p19, p16, p20};
c4 = newl; Circle(c4) = {p20, p16, p17};

ll17 = newll; Line Loop(ll17) = {c1, c2, c3, c4} ;
ll18 = newll; Line Loop(ll18) = {l8, l1, l9, right[], l35, l33, l36, left[]};
s19 = news  ; Plane Surface(s19) = {ll17, ll18} ;


// Outer circle (C2)
//==================

p21 = newp; Point(p21) = {r1 + r2 + c_x,  c_y,           0.0, lc_Circ2};
p22 = newp; Point(p22) = {c_x,            r1 + r2 + c_y,  0.0, lc_Circ2};
p23 = newp; Point(p23) = {-r1 - r2 + c_x, c_y,           0.0, lc_Circ2};
p24 = newp; Point(p24) = {c_x,            -r1 - r2 + c_y, 0.0, lc_Circ2};

c5 = newl; Circle(c5) = {p21, p16, p22};
c6 = newl; Circle(c6) = {p22, p16, p23};
c7 = newl; Circle(c7) = {p23, p16, p24};
c8 = newl; Circle(c8) = {p24, p16, p21};

l20 = newl; Line(l20) = {p17, p21};
l21 = newl; Line(l21) = {p22, p18};
l22 = newl; Line(l22) = {p19, p23};
l23 = newl; Line(l23) = {p24, p20};

// Outer circle (C3)
//==================

p25 = newp; Point(p25) = {r1 + r2 + r3 + c_x,  c_y,                 0.0, lc_Circ3};
p26 = newp; Point(p26) = {c_x,                 r1 + r2 + r3 + c_y,  0.0, lc_Circ3};
p27 = newp; Point(p27) = {-r1 - r2 - r3 + c_x, c_y,                 0.0, lc_Circ3};
p28 = newp; Point(p28) = {c_x,                 -r1 - r2 - r3 + c_y, 0.0, lc_Circ3};

c9  = newl; Circle(c9)  = {p25, p16, p26};
c10 = newl; Circle(c10) = {p26, p16, p27};
c11 = newl; Circle(c11) = {p27, p16, p28};
c12 = newl; Circle(c12) = {p28, p16, p25};

l24 = newl; Line(l24) = {p21, p25};
l25 = newl; Line(l25) = {p26, p22};
l26 = newl; Line(l26) = {p23, p27};
l27 = newl; Line(l27) = {p28, p24};

circle2[] = {};
circle3[] = {};

If(Flag_TriMesh_SourceOuter)
  ll20 = newll; Line Loop(ll20) = {c5, c6, c7, c8} ;
  s21 = news  ; Plane Surface(s21) = {ll20, ll17} ;
  circle2[] += {s21};

  ll22 = newll; Line Loop(ll22) = {c9, c10, c11, c12} ;
  s23 = news  ; Plane Surface(s23) = {ll22, ll20} ;
  circle3[] += {s23};
EndIf
If(!Flag_TriMesh_SourceOuter)
  ll13 = newll; Line Loop(ll13)   = {l20,c5,l21,-c1} ;
  s14  = news ; Plane Surface(s14) = {ll13} ;

  ll15 = newll; Line Loop(ll15)   = {-l21,c6,-l22,-c2} ;
  s16  = news ; Plane Surface(s16) = {ll15} ;

  ll17 = newll; Line Loop(ll17)   = {l22,c7,l23,-c3} ;
  s18  = news ; Plane Surface(s18) = {ll17} ;

  ll19 = newll; Line Loop(ll19)   = {-l23,c8,-l20,-c4} ;
  s20  = news ; Plane Surface(s20) = {ll19} ;

  Transfinite Line{l20, l21, l22, l23}           = n_Circ1Tan+1;
  Transfinite Line{c1, c2, c3, c4, c5, c6, c7, c8} = n_Circ1Rad+1;

  Transfinite Surface{s14} = {p17, p21, p22, p18}; Recombine Surface{s14} ;
  Transfinite Surface{s16} = {p18, p22, p23, p19}; Recombine Surface{s16} ;
  Transfinite Surface{s18} = {p19, p23, p24, p20}; Recombine Surface{s18} ;
  Transfinite Surface{s20} = {p20, p24, p21, p17}; Recombine Surface{s20} ;
  circle2[] += {s14, s16, s18, s20};


  ll13 = newll; Line Loop(ll13)   = {l24,c9,l25,-c5} ;
  s14  = news ; Plane Surface(s14) = {ll13} ;

  ll15 = newll; Line Loop(ll15)   = {-l25,c10,-l26,-c6} ;
  s16  = news ; Plane Surface(s16) = {ll15} ;

  ll17 = newll; Line Loop(ll17)   = {l26,c11,l27,-c7} ;
  s18  = news ; Plane Surface(s18) = {ll17} ;

  ll19 = newll; Line Loop(ll19)   = {-l27,c12,-l24,-c8} ;
  s20  = news ; Plane Surface(s20) = {ll19} ;

  Transfinite Line{l24, l25, l26, l27} = n_Circ2Tan+1;
  Transfinite Line{c9, c10, c11, c12}  = n_Circ1Rad+1;

  Transfinite Surface{s14} = {p21, p25, p26, p22}; Recombine Surface{s14} ;
  Transfinite Surface{s16} = {p22, p26, p27, p23}; Recombine Surface{s16} ;
  Transfinite Surface{s18} = {p23, p27, p28, p24}; Recombine Surface{s18} ;
  Transfinite Surface{s20} = {p24, p28, p25, p21}; Recombine Surface{s20} ;
  circle3[] += {s14, s16, s18, s20};
EndIf


Physical Point(CORNER) = {p1};
Physical Line(DOWN) = {vec_l1[0]};
Physical Line(UP) = {l33};
Physical Surface(SURF1) = {inno1[]} ;
Physical Surface(SURF2) = {inno2[]} ;
Physical Surface(AIR1)  = {s19} ;
Physical Surface(OMEGA_S)  = {circle2[]} ;
Physical Surface(AIR2)  = {circle3[]} ;

EndIf












































/*


Flag_AnalyticalFunc = 0;

If(!Flag_AnalyticalFunc)
n1_1    = 2 * 7;// * nbrT ;
n1_2    = 2 * 3;// * nbrT ;
n2      = 30;// * nbrT ;

For i In{0 : nbrT-1}

x_left[i]   = (i) * eps;
x_middle[i] = (i) * eps + ratio * eps;
x_right[i]  = (i+1) * eps;
y_bottom[i] = (i) * eps;
y_middle[i] = (i) * eps + ratio * eps;
y_top[i]    = (i+1) * eps;

p1 = newp; Point(p1) = {x_left[i]  , y_bottom[i], 0.0};
p2 = newp; Point(p2) = {L_X        , y_bottom[i], 0.0};
p3 = newp; Point(p3) = {L_X        , y_middle[i], 0.0};
p4 = newp; Point(p4) = {L_X        , y_top[i]   , 0.0};
p5 = newp; Point(p5) = {x_right[i] , y_top[i]   , 0.0};
p6 = newp; Point(p6) = {x_right[i] , L_Y        , 0.0};
p7 = newp; Point(p7) = {x_middle[i], L_Y        , 0.0};
p8 = newp; Point(p8) = {x_left[i]  , L_Y        , 0.0};
p9 = newp; Point(p9) = {x_middle[i], y_middle[i], 0.0};

l1 = newl ; Line(l1)  = {p1,p2} ;
l2 = newl ; Line(l2)  = {p2,p3} ;
l3 = newl ; Line(l3)  = {p3,p9} ;
l4 = newl ; Line(l4)  = {p9,p1} ;
l5 = newl ; Line(l5)  = {p3,p4} ;
l6 = newl ; Line(l6)  = {p4,p5} ;
l7 = newl ; Line(l7)  = {p5,p9} ;
l8 = newl ; Line(l8)  = {p5,p6} ;
l9 = newl ; Line(l9)  = {p6,p7} ;
l10 = newl; Line(l10) = {p7,p9} ;
l11 = newl; Line(l11) = {p7,p8} ;
l12 = newl; Line(l12) = {p8,p1} ;

ll13 = newll; Line Loop(ll13)   = {l1,l2,l3,l4} ;
s14 = news ; Plane Surface(s14) = {ll13} ;

ll15 = newll; Line Loop(ll15)   = {l5,l6,l7,-l3} ;
s16 = news ; Plane Surface(s16) = {ll15} ;

ll17 = newll; Line Loop(ll17)   = {l8,l9,l10,-l7} ;
s18 = news ; Plane Surface(s18) = {ll17} ;

ll19 = newll; Line Loop(ll19)   = {l11,l12,-l4,-l10} ;
s20 = news ; Plane Surface(s20) = {ll19} ;

Transfinite Line{l2,l4,l11}     = n1_1+1;
Transfinite Line{l5,l7,l9}      = n1_2+1;
Transfinite Line{l1,l3,l6}      = n2+1;
Transfinite Line{l12,l8,l10}    = n2+1;
Transfinite Surface{s14,s16,s18,s20}; Recombine Surface{s14,s16,s18,s20} ;

inno1[] += {s14,s20};
inno2[] += {s16,s18};

Delete { Line{l6, l8}; }

EndFor


// Last iron layer
//================
x_10 = nbrT * eps;
y_10 = nbrT * eps;
x_13 = nbrT * eps + ratio * eps;
y_13 = nbrT * eps + ratio * eps; 
p10 = newp; Point(p10) = {x_10, y_10 , 0.0};
p11 = newp; Point(p11) = {L_X , y_10 , 0.0};
p12 = newp; Point(p12) = {L_X , y_13 , 0.0};
p13 = newp; Point(p13) = {x_13, y_13 , 0.0};
p14 = newp; Point(p14) = {x_13, L_Y  , 0.0};
p15 = newp; Point(p15) = {x_10, L_Y  , 0.0};

l31 = newl ; Line(l31)  = {p10,p11} ;
l32 = newl ; Line(l32)  = {p11,p12} ;
l33 = newl ; Line(l33)  = {p12,p13} ;
l34 = newl ; Line(l34)  = {p13,p10} ;
l35 = newl ; Line(l35)  = {p13,p14} ;
l36 = newl ; Line(l36)  = {p14,p15} ;
l37 = newl ; Line(l37)  = {p15,p10} ;

ll38 = newll; Line Loop(ll38) = {l31,l32,l33,l34} ;
s39 = news ; Plane Surface(s39) = {ll38} ;

ll40 = newll; Line Loop(ll40) = {-l34,l35,l36,l37} ;
s41 = news ; Plane Surface(s41) = {ll40} ;

Transfinite Line{l32,l34,l36} = n1_1+1;
Transfinite Line{l35,l37} = n2+1;
Transfinite Line{l31,l33} = n2+1;
Transfinite Surface{s39,s41}; Recombine Surface{s39,s41} ;

inno1[] += {s39,s41};


Physical Line(Left) = {1,12};
Physical Line(Right) = {l6,l8};
//Physical Line(Right) = {l33,l35};
Physical Surface(Surf1) = {inno1[]} ;
Physical Surface(Surf2) = {inno2[]} ;

EndIf


*/



If(Flag_AnalyticalFunc)
n1      = 30 * nbrT ;
n2      = 30 * nbrT ;

L_X = 2.;
L_Y = L_X;
L_XX = 1.;
L_YY = L_YY;

Point(1) = {0.0 , 0.0 , 0.0};
Point(2) = {L_X , 0.0 , 0.0};
Point(3) = {L_X , L_YY, 0.0};
Point(4) = {L_XX, L_YY, 0.0};
Point(5) = {L_XX, L_Y , 0.0};
Point(6) = {0.0 , L_Y , 0.0};

Line(1) = {1,2} ;
Line(2) = {2,3} ;
Line(3) = {3,4} ;
Line(4) = {4,5} ;
Line(5) = {5,6} ;
Line(6) = {6,1} ;
Line(7) = {4,1} ;

Line Loop(8) = {1,2,3,7} ;
Plane Surface(9) = {8} ;

Line Loop(10) = {-7,4,5,6} ;
Plane Surface(11) = {10} ;

Transfinite Line{1:7} = n1+1;
Transfinite Surface{9,11}; Recombine Surface{9,11} ;

// Physical regions
Left  = 12;
Right = 21;
Surf  = 22;

Physical Line(Right)  = {3,4} ;
Physical Line(Left) = {1,6};
Physical Surface(Surf) = {9,11} ;
EndIf

