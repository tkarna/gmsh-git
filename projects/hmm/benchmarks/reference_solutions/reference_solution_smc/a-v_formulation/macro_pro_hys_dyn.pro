//Include "macro_lam.dat";
Include "macro_lam_jPerp.dat";

Group {
  Gamma_Inf     = Region[ {GAMMA_INF} ];
  Omega_S1      = Region[ {OMEGA_S1} ];
  Omega_S2      = Region[ {OMEGA_S2} ];
  Omega_S       = Region[ {Omega_S1, Omega_S2} ];  
  Omega_L       = Region[ {INSU, AIR, Omega_S} ]; 
  Omega_CC      = Region[ {Omega_L} ]; 
  For k In {1:nlam+1}
  Iron~{k}      = #{(IRON+k-1)};
  Omega_C       += Region[ {Iron~{k}} ]; 
  EndFor
  Omega_NL      = Region[ {Omega_C} ]; 
  Omega         = Region[ {Omega_NL, Omega_L, Omega_S} ] ; // the entire computational domain
}
Function {
  mu_0            = 4.e-7 * Pi ;
  mu_Air          = mu_0;       nu_Air  = 1/mu_Air ;
  mu_Iron         = 100 * mu_0; nu_Iron = 1/mu_Iron ;
  sigma[Omega_C]  = 5e6;       //sigma[Omega_CC] = 1e-9;
  nu[Omega_L]     = nu_Air/(1); //nu[Omega_NL]    = nu_Iron ;

 //Gyselink's paper: Incorporation of a Jiles-Atherton vector hysteresis model in 2D FE magnetic field computations
  Msat = 1145220; aaa = 59.5; kkk = 99.2; ccc = 0.54; alpha = 1.3e-4 ;
  hyst_FeSi = { Msat, aaa, kkk, ccc, alpha};

  Freq = 50 ; T= 1/Freq ;
  nbrT = 1./10 ;
  nbrstep = 120 ;
  t0 = 0 ;
  tmax = nbrT*T ;
  dt = T/nbrstep ;
  MaxNumIter = 30;
  Tolerance = 1e-4;

  Val_I = 50 ;
  j_fixed[Omega_S1] = Vector[0, 0, -Val_I] * F_Sin_wt_p[]{2*Pi*Freq, 0} ;
  j_fixed[Omega_S2] = Vector[0, 0,  Val_I] * F_Sin_wt_p[]{2*Pi*Freq, 0} ;
}

//-------------------------------------------------------------------------------------

Jacobian {
  { Name Vol; Case { { Region All ; Jacobian Vol; } } }
  { Name Sur; Case { { Region All ; Jacobian Sur; } } }
}

Integration {
  { Name I1 ; Case { { Type Gauss ; Case {
          { GeoElement Triangle   ; NumberOfPoints  7 ; }
	  { GeoElement Quadrangle ; NumberOfPoints  4 ; }
	  { GeoElement Line       ; NumberOfPoints  13 ; }
        } }
    }
  }
}

Constraint {
  { Name MVP_2D ;
    Case {
      { Region Gamma_Inf  ; Type Assign; Value  0 ; }
    }
  }
 { Name Current_2D ;
    Case {
      For k In {1:nlam+1}
      { Region Iron~{k}   ; Type Assign; Value  0 ; } // forcing net current in each lamination to be equal to zero
      EndFor
    }
  }
}

//-----------------------------------------------------------------------------------------------

FunctionSpace {

  { Name Hcurl_a_2D ; Type Form1P ;
    BasisFunction {
      { Name se1 ; NameOfCoef ae1 ; Function BF_PerpendicularEdge ;
        Support #{Omega} ; Entity NodesOf [ All] ; }
   }
    Constraint {
      { NameOfCoef ae1 ; EntityType NodesOf  ; NameOfConstraint MVP_2D ; }
    }
  }

// Gradient of Electric scalar potential (2D)
  { Name Hregion_u_2D ; Type Form1P ;
    BasisFunction {
      { Name sr ; NameOfCoef ur ; Function BF_RegionZ ;
        Support Omega_C ; Entity Omega_C ; }
    }
    GlobalQuantity {
      { Name U ; Type AliasOf        ; NameOfCoef ur ; }
      { Name I ; Type AssociatedWith ; NameOfCoef ur ; }
    }
    Constraint {
      { NameOfCoef U ;
        EntityType Region ; NameOfConstraint Voltage_2D ; }
      { NameOfCoef I ;
        EntityType Region ; NameOfConstraint Current_2D ; }
    }
  }


  { Name H_vector ; Type Vector;
    BasisFunction {
      { Name sex ; NameOfCoef aex ; Function BF_VolumeX ; Support Omega_NL ; Entity VolumesOf[ Omega_NL ] ; }
      { Name sey ; NameOfCoef aey ; Function BF_VolumeY ; Support Omega_NL ; Entity VolumesOf[ Omega_NL ] ; }
      { Name sez ; NameOfCoef aez ; Function BF_VolumeZ ; Support Omega_NL ; Entity VolumesOf[ Omega_NL ] ; }
    }
  } 
}

//-----------------------------------------------------------------------------------------------

Formulation {
  { Name GetH; Type FemEquation ;
    Quantity {
      { Name a ; Type Local  ; NameOfSpace Hcurl_a_2D ; }
      { Name h ; Type Local  ; NameOfSpace H_vector ; } // For saving h_Jiles
    }
    Equation {
      Galerkin { [          Dof{h}, {h} ]; In Omega_NL; Jacobian Vol; Integration I1; }
      Galerkin { [ -h_Jiles[ {h}[1], {d a}[1], {d a} ]{List[hyst_FeSi]} , {h} ]; 
        In Omega_NL ; Jacobian Vol ; Integration I1 ; }
    }
  }
  { Name MagDyn_a_2D_JilesAtherton; Type FemEquation ;
    Quantity {
      { Name a ; Type Local  ; NameOfSpace Hcurl_a_2D ; }
      { Name h ; Type Local  ; NameOfSpace H_vector ; } // For saving h_Jiles
      { Name ur ; Type Local  ; NameOfSpace Hregion_u_2D ; }
      { Name I  ; Type Global ; NameOfSpace Hregion_u_2D [I] ; }
      { Name U  ; Type Global ; NameOfSpace Hregion_u_2D [U] ; }
    }
    Equation {
      Galerkin { DtDof[ sigma[] * Dof{a} , {a} ]  ; In Omega_C; Jacobian Vol; Integration I1; }
      Galerkin { DtDof [ sigma[] * Dof{a} , {ur} ]; In Omega_C ; Jacobian Vol; Integration I1 ; }
      Galerkin { [ sigma[] * Dof{ur}, {a}  ]      ; In Omega_C ; Jacobian Vol ; Integration I1 ; }
      Galerkin { [ sigma[] * Dof{ur}, {ur} ]      ; In Omega_C ; Jacobian Vol ; Integration I1 ; }    
      GlobalTerm { [ Dof{I} , {U} ]               ; In Omega_C ; } 
      Galerkin { [ nu[] * Dof{d a}  , {d a} ]     ; In Omega_L; Jacobian Vol; Integration I1; }
      Galerkin { [ h_Jiles[ {h}[1], {d a}[1], {d a} ]{List[hyst_FeSi]} , {d a} ]; In Omega_NL; Jacobian Vol; Integration I1; }
      Galerkin { JacNL[ dhdb_Jiles[{h}, {d a}, {h}-{h}[1]]{List[hyst_FeSi]} * Dof{d a} , {d a} ]; 
        In Omega_NL ; Jacobian Vol ; Integration I1 ; }
      Galerkin { [ (-j_fixed[]), {a} ]; In Omega_S; Jacobian Sur; Integration I1; }
      Galerkin { [          Dof{h}, {h} ]         ; In Omega_NL; Jacobian Vol; Integration I1; }
      Galerkin { [ -h_Jiles[{h}[1], {d a}[1], {d a}]{List[hyst_FeSi]} , {h} ]; In Omega_NL; Jacobian Vol; Integration I1; }
    }
  }  
}

//-----------------------------------------------------------------------------------------------

Resolution {
  { Name MagDyn_a_2D_JilesAtherton ;
    System {
      //{ Name H ; NameOfFormulation GetH ; }
      { Name A ; NameOfFormulation MagDyn_a_2D_JilesAtherton ; }
    }
    Operation {

      InitSolution[A] ; InitSolution[A] ;
      //InitSolution[H] ; InitSolution[H] ;
      TimeLoopTheta[ t0, tmax, dt, 1 ]{
        IterativeLoop[MaxNumIter, Tolerance, (($Iteration<10) ? 1. : 0.6)] {
          GenerateJac[A]; SolveJac[A];
          //Generate[H]; Solve[H];
        }
        SaveSolution[A] ;
        //SaveSolution[H] ;
      }
    }
  }
}
//-----------------------------------------------------------------------------------------------

PostProcessing {
 { Name MagDyn_a_2D_JA ; NameOfFormulation MagDyn_a_2D_JilesAtherton ;
   PostQuantity {
     { Name a ; Value { Term { [ {a} ] ; In Omega ; Jacobian Vol ; } } }
     { Name ur ; Value { Term { [ {ur} ] ; In Omega ; Jacobian Vol ; } } }
     { Name az; Value { Term { [ CompZ[{a}] ] ; In Omega ; Jacobian Vol ; } } }
     { Name b ; Value { Term { [ {d a} ] ; In Omega ; Jacobian Vol ; } } }
     { Name h ; Value { 
         Term { [ nu[] * {d a} ]   ; In Omega_L  ; Jacobian Vol ; } 
         Term { [ {h} ]            ; In Omega_NL ; Jacobian Vol ; } } }
     { Name hb; Value { Term { [ TensorSym[ CompX[{h}],   CompY[{h}],   CompZ[{h}],
                                            CompX[{d a}], CompY[{d a}], CompZ[{d a}]] ] ; In Omega ; Jacobian Vol ;} } }
     { Name e ; Value { Term { [ -Dt[{a}] ] ; In Omega ; Jacobian Vol ; } } }
     { Name j ; Value { Term { [ -sigma[] * (Dt[{a}] + {ur}) ] ; In Omega_C ; Jacobian Vol ; } } }
     { Name jz ; Value { Term { [ CompZ[-sigma[] * ( Dt[{a}] + {ur} )] ] ; In Omega_C ; Jacobian Vol ; } } }
     { Name rhoj2 ; Value { Term { [ sigma[]*SquNorm[Dt[{a}]  + {ur} ] ]; In Omega_C; Jacobian Vol; } } }
     { Name JouleLosses ; Value { Integral { [ sigma[] * SquNorm[ Dt[{a} ]  + {ur} ] ]; In Omega_C; Jacobian Vol; Integration I1; } } }
   } 
 }
}
//-----------------------------------------------------------------------------------------------
L_X = 0.2 ;
L_Y = L_X ;

index = 137;
PostOperation MapDyn UsingPost MagDyn_a_2D_JA {
  Print[ az    ,  OnElementsOf Omega,             File Sprintf("res_dyn/a_%g.pos", index)    , Format Gmsh ] ;
  Print[ ur    ,  OnElementsOf Omega,             File Sprintf("res_dyn/ur_%g.pos", index)    , Format Gmsh ] ;
  Print[ b     ,  OnElementsOf Omega,             File Sprintf("res_dyn/b_%g.pos", index)    , Format Gmsh ] ;
  Print[ h     ,  OnElementsOf Omega,             File Sprintf("res_dyn/h_%g.pos", index)    , Format Gmsh ] ;
  Print[ e     ,  OnElementsOf Omega,             File Sprintf("res_dyn/e_%g.pos", index)    , Format Gmsh ] ;
  Print[ j     ,  OnElementsOf Omega,             File Sprintf("res_dyn/j_%g.pos", index)    , Format Gmsh ] ;
  Print[ jz    ,  OnElementsOf Omega,             File Sprintf("res_dyn/jz_%g.pos", index)   , Format Gmsh ] ;
  Print[ rhoj2 ,  OnElementsOf Omega,             File Sprintf("res_dyn/rhoj2_%g.pos", index), Format Gmsh ] ;
  Print[ hb    ,  OnPoint {0.2/3.0, 0.2/4.0, 0} , File Sprintf("res_dyn/hb_%g.dat", index)   , Format TimeTable ] ;
}
