Include "macro_jPerp_smc.dat";

x_L = - 0.5 * L_X;
y_L = - 0.5 * L_Y;
p1 = newp ;  Point(p1)  = {x_L + 0.0              , y_L + 0.0              , 0.0, lc_ext};
p2 = newp ;  Point(p2)  = {x_L + eps              , y_L +0.0              , 0.0, lc_ext};
p3 = newp ;  Point(p3)  = {x_L + eps              , y_L +eps              , 0.0, lc_ext};
p4 = newp ;  Point(p4)  = {x_L + 0.0              , y_L +eps              , 0.0, lc_ext};
p5 = newp ;  Point(p5)  = {x_L + d_i              , y_L +d_i + r_c        , 0.0, lc_int};
p6 = newp ;  Point(p6)  = {x_L + d_i + r_c        , y_L +d_i              , 0.0, lc_int};
p7 = newp ;  Point(p7)  = {x_L + d_i + r_c        , y_L +d_i + r_c        , 0.0, lc_int};
p8 = newp ;  Point(p8)  = {x_L + eps - (d_i + r_c), y_L +d_i              , 0.0, lc_int};
p9 = newp ;  Point(p9)  = {x_L + eps - d_i        , y_L +d_i + r_c        , 0.0, lc_int};
p10 = newp;  Point(p10) = {x_L + eps - (d_i + r_c), y_L +d_i + r_c        , 0.0, lc_int};
p11 = newp;  Point(p11) = {x_L + eps - d_i        , y_L +eps - (d_i + r_c), 0.0, lc_int};
p12 = newp;  Point(p12) = {x_L + eps - (d_i + r_c), y_L +eps - d_i        , 0.0, lc_int};
p13 = newp;  Point(p13) = {x_L + eps - (d_i + r_c), y_L +eps - (d_i + r_c), 0.0, lc_int};
p14 = newp;  Point(p14) = {x_L + (d_i + r_c)      , y_L +eps - d_i        , 0.0, lc_int};
p15 = newp;  Point(p15) = {x_L + d_i              , y_L +eps - (d_i + r_c), 0.0, lc_int};
p16 = newp;  Point(p16) = {x_L + d_i + r_c        , y_L +eps - (d_i + r_c), 0.0, lc_int};

l1 = newl ; Line(l1)  = {p1,p2} ;
l2 = newl ; Line(l2)  = {p2,p3} ;
l3 = newl ; Line(l3)  = {p3,p4} ;
l4 = newl ; Line(l4)  = {p4,p1} ;
l5 = newl; Circle(l5) = {p5, p7, p6};
l6 = newl ; Line(l6)  = {p6,p8} ;
l7 = newl; Circle(l7) = {p8, p10, p9};
l8 = newl ; Line(l8)  = {p9,p11} ;
l9 = newl; Circle(l9) = {p11, p13, p12};
l10 = newl ; Line(l10)  = {p12,p14} ;
l11 = newl; Circle(l11) = {p14, p16, p15};
l12 = newl ; Line(l12)  = {p15,p5} ;

ll1 = newll; Line Loop(ll1)   = {l5,l6,l7,l8, l9, l10, l11, l12} ;
s_cond[] = news ; Plane Surface(news) = {ll1} ;
ll2 = newll; Line Loop(ll2)   = {l1,l2,l3,l4} ;
s_iso[]  = news ; Plane Surface(news) = {ll2, ll1} ;

For k In {1 : num-1}
  s_cond[] += Translate {0, k * eps, 0} { Duplicata { Surface{s_cond[ {0} ]}; } };
  s_iso[]  += Translate {0, k * eps, 0} { Duplicata { Surface{s_iso[ {0} ]}; } };
EndFor
For k In {1 : num-1}
  s_cond[] += Translate {k * eps, 0, 0} { Duplicata { Surface{s_cond[ {0:num-1} ]}; } };
  s_iso[]  += Translate {k * eps, 0, 0} { Duplicata { Surface{s_iso[ {0:num-1} ]}; } };
EndFor

allsurfaces[] = Surface '*' ;
bnd[]=CombinedBoundary{Surface{allsurfaces[]};};

allll = newll; Line Loop(newll) = bnd[]; 

// // Upper inductor
//==================
p25 = newp; Point(p25) = { - 0.5 * dx, 0.5 * L_Y + delta,   0.0, lc_int};
p26 = newp; Point(p26) = {   0.5 * dx, 0.5 * L_Y + delta,   0.0, lc_int};
p27 = newp; Point(p27) = {   0.5 * dx, 0.5 * L_Y + delta + dy, 0.0, lc_int};
p28 = newp; Point(p28) = { - 0.5 * dx, 0.5 * L_Y + delta + dy, 0.0, lc_int};

l37 = newl; Line(l37) = {p25, p26};
l38 = newl; Line(l38) = {p26, p27};
l39 = newl; Line(l39) = {p27, p28};
l40 = newl; Line(l40) = {p28, p25};

ll21 = newll; Line Loop(ll21) = {l37, l38, l39, l40} ;
s22 = news  ; Plane Surface(s22) = {ll21} ;
Transfinite Line{l37,l39}     = nx;
Transfinite Line{l38,l40}     = ny;
Transfinite Surface{s22} = {p25,p26,p27,p28}; Recombine Surface{s22} ;

// // Bottom inductor
//==================
p25 = newp; Point(p25) = {- 0.5 * dx, - 0.5 * L_Y - delta - dy, 0.0, lc_int};
p26 = newp; Point(p26) = {  0.5 * dx, - 0.5 * L_Y - delta - dy, 0.0, lc_int};
p27 = newp; Point(p27) = {  0.5 * dx, - 0.5 * L_Y - delta     , 0.0, lc_int};
p28 = newp; Point(p28) = {- 0.5 * dx, - 0.5 * L_Y - delta     , 0.0, lc_int};

l37 = newl; Line(l37) = {p25, p26};
l38 = newl; Line(l38) = {p26, p27};
l39 = newl; Line(l39) = {p27, p28};
l40 = newl; Line(l40) = {p28, p25};

ll23 = newll; Line Loop(ll23) = {l37, l38, l39, l40} ;
s24 = news  ; Plane Surface(s24) = {ll23} ;

Transfinite Line{l37,l39}     = nx;
Transfinite Line{l38,l40}     = ny;
Transfinite Surface{s24} = {p25,p26,p27,p28}; Recombine Surface{s24} ;

// // Outer circle (C1)
// //==================

p16 = newp; Point(p16) = {c_x,    c_y,    0.0};
p17 = newp; Point(p17) = {r+c_x,  c_y,    0.0, lc_Circ1};
p18 = newp; Point(p18) = {c_x,    r+c_y,  0.0, lc_Circ1};
p19 = newp; Point(p19) = {-r+c_x, c_y,    0.0, lc_Circ1};
p20 = newp; Point(p20) = {c_x,    -r+c_y, 0.0, lc_Circ1};

c1 = newl; Circle(c1) = {p17, p16, p18};
c2 = newl; Circle(c2) = {p18, p16, p19};
c3 = newl; Circle(c3) = {p19, p16, p20};
c4 = newl; Circle(c4) = {p20, p16, p17};
ll17 = newll; Line Loop(ll17) = {c1, c2, c3, c4} ;
s19  = news ; Plane Surface(s19) = {ll17, ll21, ll23, allll} ;


//Physical
//========

Physical Point(CORNER) = {p1};

Physical Line(GAMMA_INF) = {c1, c2, c3, c4};

Physical Surface(OMEGA_S1) = {s22};
Physical Surface(OMEGA_S2) = {s24};
Physical Surface(INSU)  = {s_iso[]} ;
Physical Surface(AIR)  = {s19} ;
For k In {0 : num - 1}
  For j In {0 : num - 1}
    Physical Surface(IRON + k * num + j ) = { s_cond[ { k * num + j} ] }  ;
  EndFor
EndFor
