clear all
close all

syms z

alpha0 = z^0 ;
alpha2 = 1/2*(-1+12*z^2) ;
alpha4 = 1/8*(3-120*z^2+560*z^4) ;
alpha6 = 1/16*(-5+105*(2*z)^2-315*(2*z)^4+231*(2*z)^6);
alpha8 = 1/128*(6435*(2*z)^8-12012*(2*z)^6+6930*(2*z)^4-1260*(2*z)^2+35);

beta2 = -int(int(alpha0,z),z) + subs(int(int(alpha0,z),z),1/2);
beta4 = -int(int(alpha2)) + subs(int(int(alpha2)),1/2);
beta6 = -int(int(alpha4)) + subs(int(int(alpha4)),1/2);
beta8 = -int(int(alpha6)) + subs(int(int(alpha6)),1/2);
beta10 = -int(int(alpha8)) + subs(int(int(alpha8)),1/2);

d = [-1/2:0.02:1/2];

a0 = subs(alpha0,z,d);
a2 = subs(alpha2,z,d);
a4 = subs(alpha4,z,d);
a6 = subs(alpha6,z,d);
a8 = subs(alpha8,z,d);

%A = [d' a0' a2' a4' a6']; 
%save legendre.dat -ascii -double A

figure,plot(d,a2,d,a4,d,a6,d,a8,'LineWidth',2);
legend('a2','a4','a6','a8');

b2 = subs(beta2,z,d);
b4 = subs(beta4,z,d);
b6 = subs(beta6,z,d);
b8 = subs(beta8,z,d);
b10 = subs(beta10,z,d);

figure,plot(d,b2,d,b4,d,b6,d,b8,d,b10,'LineWidth',2); legend('b2','b4','b6','b8','b10');
%B = [d' b2' b4' b6' b8'];
%save beta.dat -ascii -double B   

a = -1/2 ;
b = 1/2 ;

alpha = [alpha0 alpha2 alpha4 alpha6 alpha8] ;
beta =  [beta2 beta4 beta6 beta8 beta10] ;

P = int(alpha.'*alpha,a,b)
Q = int(beta.'*alpha,a,b)


%===============================================================================

N = 8;

fid = fopen('PQ.pro', 'wt');
fprintf(fid, 'Function { \n\n');

for i = 1:2:N+1
    ii = ceil(i/2);
    %fprintf(fid, 'p_%d = %.16g ; \n', i-1, double(P(i,i)) );
    fprintf(fid, 'p_%d = %s ; \n', i-1, char(P(ii,ii)) );
end

fprintf(fid, '\n');

for i = 1:2:N+1
    ii = ceil(i/2); 
    for j = i:2:N+1
        jj = ceil(j/2);
        %fprintf(fid, 'q_%d_%d = %.16g ; \n', i-1, j-1, double(Q(i,j)));
        fprintf(fid, 'q_%d_%d = %s ; \n', i-1, j-1, char(Q(ii,jj)));
    end
end
fprintf(fid, '\n');

fprintf(fid, '} \n');
fclose(fid)



