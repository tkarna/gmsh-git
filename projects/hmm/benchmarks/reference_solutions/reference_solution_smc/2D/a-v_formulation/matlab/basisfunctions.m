syms z


alpha0 = z^0 ;
alpha2 = 1/2*(-1+12*z^2) ;
alpha4 = 1/8*(3-120*z^2+560*z^4) ;
alpha6 = 1/16*(-5+105*(2*z)^2-315*(2*z)^4+231*(2*z)^6);

beta2 = -int(int(alpha0,z),z) + subs(int(int(alpha0,z),z),1/2);
beta4 = -int(int(alpha2)) + subs(int(int(alpha2)),1/2);
beta6 = -int(int(alpha4)) + subs(int(int(alpha4)),1/2);
beta8 = -int(int(alpha6)) + subs(int(int(alpha6)),1/2);

%beta2 = 1/8*(1-4*z^2);
%beta4 = 1/32*(-1+8*z^2-16*z^4);
%beta6 = 1/192*(1-4*9*z^2+16*15*z^4-64*7*z^6);

% curl e = -d_t b
gamma1 = int(alpha0,z) ;
gamma3 = int(alpha2,z) ;
gamma5 = int(alpha4,z) ;
gamma7 = int(alpha6,z) ;

% curl h = j
gamma1_ = diff(beta2,z) ;
gamma3_ = diff(beta4,z) ;
gamma5_ = diff(beta6,z) ;

a = -1/2 ;
b = 1/2 ;

P = [int(alpha0*alpha0,a,b) int(alpha0*alpha2,a,b) int(alpha0* ...
                                                  alpha4,a,b);
    int(alpha2*alpha0,a,b) int(alpha2*alpha2,a,b) int(alpha2* ...
                                                  alpha4,a,b);
    int(alpha4*alpha0,a,b) int(alpha4*alpha2,a,b) int(alpha4* ...
                                                  alpha4,a,b)]

Q = [int(alpha0*beta2,a,b) int(alpha0*beta4,a,b) int(alpha0* ...
                                                  beta6,a,b);
    int(alpha2*beta2,a,b) int(alpha2*beta4,a,b) int(alpha2* ...
                                                  beta6,a,b);
    int(alpha4*beta2,a,b) int(alpha4*beta4,a,b) int(alpha4* ...
                                                  beta6,a,b)]


syms z d ds
syms s nu hs b0 b1 b2


% Complex domain

A= nu*P(1:2,1:2)*[b0 b2].' + j*nu*2*ds^2*Q(1:2,1:2)*[b0 b2].';

hs = A(1,1)

%b2 = -(30*i*((b0*i*nu*ds^2)/6 + b0*nu))/(ds^2*nu)
%hs = b0*nu + (b0*ds^2*i*nu)/6 - (b2*ds^2*i*nu)/30

b2= (b0*ds^2*i)/30/(1/5+i*ds^2/105)
hs = b0*nu + (b0*ds^2*i*nu)/6 - (b2*ds^2*i*nu)/30