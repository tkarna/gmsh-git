clear all
close all

syms x z d 
syms s nu he ho b0 b1 
syms dt_b0 dt_b1 dt_b2 dt_b3 dt_b4 dt_b5 dt_b6 dt_b7

x = 2*z/d ;

% Legendre Polynomials
alpha0 = x^0 ;
alpha1 = x ;
alpha2 = 1/2*(3*x^2-1) ;
alpha3 = 1/2*(5*x^3-3*x);
alpha4 = 1/8*(35*x^4-30*x^2+3);
alpha5 = 1/8*(63*x^5-70*x^3+15*x);
alpha6 = 1/16*(231*x^6-315*x^4+105*x^2-5);
alpha7 = 1/16*(429*x^7-693*x^5+315*x^3-35*x);
alpha8 = 1/128*(6435*x^8-12012*x^6+6930*x^4-1260*x^2+35);
alpha9 = 1/128*(12155*x^9-25740*x^7+18018*x^5-4620*x^3+315*x);
alpha10 = 1/256*(46189*x^10-109395*x^8+90090*x^6-30030*x^4+3465*x^2-63);

% d^2 * \partial_z\beta_{k+2}= \alpha_k(z)
beta2 = 1/d^2 *(int(int(alpha0,z),z) - subs(int(int(alpha0,z),z),d/2));
beta3 = 1/d^2 *(int(int(alpha1,z),z) - x*subs(int(int(alpha1,z),z),d/2));
beta4 = 1/d^2 *(int(int(alpha2,z),z) - subs(int(int(alpha2,z),z),d/2));
beta5 = 1/d^2 *(int(int(alpha3,z),z) - x*subs(int(int(alpha3,z),z),d/2));
beta6 = 1/d^2 *(int(int(alpha4,z),z) - subs(int(int(alpha4,z),z),d/2));
beta7 = 1/d^2 *(int(int(alpha5,z),z) - x*subs(int(int(alpha5,z),z),d/2));
beta8 = 1/d^2 *(int(int(alpha6,z),z) - subs(int(int(alpha6,z),z),d/2));
beta9 = 1/d^2 *(int(int(alpha7,z),z) - x*subs(int(int(alpha7,z),z),d/2));
beta10 = 1/d^2 *(int(int(alpha8,z),z) - subs(int(int(alpha8,z),z),d/2));
beta11 = 1/d^2 *(int(int(alpha9,z),z) - x*subs(int(int(alpha9,z),z),d/2));
beta12 = 1/d^2 *(int(int(alpha10,z),z) - subs(int(int(alpha10,z),z),d/2));


a = -d/2 ;
b = d/2 ;

alpha = [alpha0 alpha1 alpha2 alpha3 alpha4 alpha5 alpha6 alpha7 ...
         alpha8 alpha9] ;
beta = -[beta2 beta3 beta4 beta5 beta6 beta7 beta8 beta9 beta10 beta11] ;

P = int(alpha.'*alpha,a,b)
Q = int(beta.'*alpha,a,b)

P0 = int(alpha(1:2).'*alpha(1:2),a,b);
Q0 = int(alpha(1:2).'*beta(1:2),a,b);

P2 = int(alpha(1:4).'*alpha(1:4),a,b);
Q2 = int(alpha(1:4).'*beta(1:4),a,b);

n = size(alpha,2);
P_e = int(alpha(1:2:n).'*alpha(1:2:n),a,b);
Q_e = int(alpha(1:2:n).'*beta(1:2:n),a,b);

he = nu*b0 + s*d^2*(dt_b0/12 - dt_b2/60 );
ho = nu*b1 + s*d^2*(dt_b1/60 - dt_b3/140); 

h = he + ho*alpha1 + s*d^2*(beta2*dt_b0 + beta3*dt_b1 + ...
                            beta4*dt_b2 + beta5*dt_b3 + ...
                            beta6*dt_b4 + beta7*dt_b5 + ...
                            beta8*dt_b6 + beta9*dt_b7);
gamma1 = diff(beta2,z);
gamma2 = diff(beta3,z);
gamma3 = diff(beta4,z);
gamma4 = diff(beta5,z);
gamma5 = diff(beta6,z);
gamma6 = diff(beta7,z);
gamma7 = diff(beta8,z);
gamma8 = diff(beta9,z);
gamma9 = diff(beta10,z);
gamma10 = diff(beta11,z);


j = ho*diff(alpha1) + s*d^2*( gamma1*dt_b0 + gamma2*dt_b1 + ...
                              gamma3*dt_b2 + gamma4*dt_b3 + ...
                              gamma5*dt_b4 + gamma6*dt_b5 + ...
                              gamma7*dt_b6 + gamma8*dt_b7 );

je = expand((subs(j,d/2) + subs(j,-d/2))/2);
%pretty(je)
jo = expand((subs(j,d/2) - subs(j,-d/2))/2);
%pretty(jo)


gamma = [gamma1 gamma2 gamma3 gamma4 gamma5 gamma6 gamma7 gamma8 ...
         gamma9 gamma10];
G = int(gamma.'*gamma,a,b)
%mfunlist

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Joule losses computation

% curl e = -d_t b ==> d_z e  = -d_t b
% b(z,t) = \sum_k \alpha_k(z) b_k(t)
% e = -\int_{-d/2}^{d/2} \alpha_k(z) d_t b_k(t) dz 
%   = -\gamma_k d_t b_k

e = - gamma1*dt_b0...
    - gamma2*dt_b1...
    - gamma3*dt_b2...
    - gamma4*dt_b3...
    - gamma5*dt_b4...
    - gamma6*dt_b5 ;

gamma1_ = 1/d *(int(alpha0,z) - subs(int(alpha0,z),d/2));
gamma2_ = 1/d *(int(alpha1,z) - subs(int(alpha1,z),d/2));
gamma3_ = 1/d *(int(alpha2,z) - subs(int(alpha2,z),d/2));
gamma4_ = 1/d *(int(alpha3,z) - subs(int(alpha3,z),d/2));
gamma5_ = 1/d *(int(alpha4,z) - subs(int(alpha4,z),d/2));
gamma6_ = 1/d *(int(alpha5,z) - subs(int(alpha5,z),d/2));

e_ = - gamma1_*dt_b0...
    - gamma2_*dt_b1...
    - gamma3_*dt_b2...
    - gamma4_*dt_b3...
    - gamma5_*dt_b4...
    - gamma6_*dt_b5; 

%===============================================================================

N = 10;

fid = fopen('PQ.pro', 'wt');
fprintf(fid, 'Function { \n\n');

for i = 1:N
    %fprintf(fid, 'p_%d = %.16g ; \n', i-1, double(P(i,i)) );
    fprintf(fid, 'p_%d = %s ; \n', i-1, char(P(i,i)/d) );
end

fprintf(fid, '\n');

for i = 1:N
    for j = 1:N
        %fprintf(fid, 'q_%d_%d = %.16g ; \n', i-1, j-1, double(Q(i,j)));
        fprintf(fid, 'q_%d_%d = %s ; \n', i-1, j-1, char(Q(i,j)/d));
    end
end
fprintf(fid, '\n');

for i = 1:N
    for j = 1:N
        fprintf(fid, 'g_%d_%d = %s ; \n', i-1, j-1, char(d*G(i,j)));
    end
end

fprintf(fid, '\n');




fprintf(fid, '} \n');
fclose(fid)