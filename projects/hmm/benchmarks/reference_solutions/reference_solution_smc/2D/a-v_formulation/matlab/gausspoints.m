clear all
close all

format long

syms x z 
x = 2*z ;

%Even legendre polynomials defined between [-1/2,1/2]
alpha0 = x^0 ;
alpha2 = 1/2*(3*x^2-1) ;
alpha4 = 1/8*(35*x^4-30*x^2+3);
alpha6 = 1/16*(231*x^6-315*x^4+105*x^2-5);
alpha8 = 1/128*(6435*x^8-12012*x^6+6930*x^4-1260*x^2+35);
alpha10 = 1/256*(46189*x^10-109395*x^8+90090*x^6-30030*x^4+3465*x^2-63);


N = 18 ; % Watch out! It must be even, as we are integrating on
         % half the width of the lamination, i.e. taking half the
         % number of gauss points

% Using Legendre-Gauss quadrature: N points in [-1,1]
[lx,lp] = lgwt(N,-1,1) ; 

% Shell defined in [-1/2,1/2] ==> Divide by 2 the Gauss pnts
lx_ = lx/2; lp_ = lp/2;

% for taking advantage of symmetry: only half of the
% gauss points... ==> watch out N must be pair 
lx_ = lx(find(lx>=0))/2 ;
lp_ = 2*lp(find(lx>=0))/2 ;

a2_ = subs(alpha2,z,lx_);
a4_ = subs(alpha4,z,lx_);
a6_ = subs(alpha6,z,lx_);
a8_ = subs(alpha8,z,lx_);
a10_ = subs(alpha10,z,lx_);

% using the legendre function from Matlab
a2_ = legendreP(2,0,2*lx_) ;
a4_ = legendreP(4,0,2*lx_) ;
a6_ = legendreP(6,0,2*lx_) ;
a8_ = legendreP(8,0,2*lx_) ;
a10_ = legendreP(10,0,2*lx_) ;


%zth = [-1/2:0.01:1/2];
zth = [0:0.01:1/2];

a2 = legendreP(2,0,2*zth) ;
a4 = legendreP(4,0,2*zth) ;
a6 = legendreP(6,0,2*zth) ;
a8 = legendreP(8,0,2*zth) ;
a10 = legendreP(10,0,2*zth) ;


figure,plot(zth,a2,'b',zth,a4,'g',zth,a6,'r',zth,a8,'c',zth,a10,'m',...
            lx_,a2_,'ob',lx_,a4_,'xg',lx_,a6_,'+r',lx_,a8_,'xc',lx_,a10_,'dm',...
            'LineWidth',2,'MarkerSize',10);
legend('alpha2','alpha4','alpha6','alpha8', 'alpha10');


A = [ a2_.*a2_.*lp_ a4_.*a4_.*lp_ ...
      a6_.*a6_.*lp_ a8_.*a8_.*lp_ a10_.*a10_.*lp_] ;

Pnum = sum(A)
P = [1/5 1/9 1/13 1/17 1/21]

RelativeError = (Pnum-P)./P


%===============================================================================
if(1) % one for creating file

N = ceil(N/2); % if symmetry taken into account    
filename = sprintf('gausspoints_Ngp%g.pro',N);    
fid = fopen(filename, 'wt');

fprintf(fid, 'Function { \n\n');

fprintf(fid, 'Nqp = %.16g ; // nbrQuadraturePoints \n', N );

for i = 1:N
    fprintf(fid, 'y_%d = %.16g ; \n', i, lx_(i) );% gauss point lamination                                                  
end

fprintf(fid, '\n');
for i = 1:N
    fprintf(fid, 'w_%d = %.16g ; \n', i, lp_(i) );
end

fprintf(fid, '\n');

for i = 1:N
    fprintf(fid, 'a_2_%d = %.16g ; \n', i, a2_(i) );
end

fprintf(fid, '\n');

for i = 1:N
    fprintf(fid, 'a_4_%d = %.16g ; \n', i, a4_(i) );
end

fprintf(fid, '\n');

for i = 1:N
    fprintf(fid, 'a_6_%d = %.16g ; \n', i, a6_(i) );
end

fprintf(fid, '\n');

for i = 1:N
    fprintf(fid, 'a_8_%d = %.16g ; \n', i, a8_(i) );
end

fprintf(fid, '\n');

fprintf(fid, '} \n');
%================================================================
fclose(fid)

end
%===============================================================================
