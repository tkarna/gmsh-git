Include "common.dat";

nlam = num * num;
Printf("=============> num = %g nlam = %g", num, nlam);

Group {
  Gamma_Inf       = Region[ {GAMMA_INF} ]         ; 
  Omega_S1        = Region[ {OMEGA_S1} ]          ;
  Omega_S2        = Region[ {OMEGA_S2} ]          ;
  Omega_S         = Region[ {Omega_S1, Omega_S2} ];
  Omega_L         = Region[ {INSU, AIR, Omega_S} ]; 
  Omega_CC        = Region[ {Omega_L} ]; 
  For k In {1:nlam}
    Iron~{k}        = #{(IRON+k-1)};
    Omega_C         += Region[ {Iron~{k}} ]; 
  EndFor
  Omega_NL        = Region[ {Omega_C} ]; 
  Omega           = Region[ {Omega_NL, Omega_L} ] ; // the entire computational domain
}
Function {
  mu0     = 4.e-7 * Pi;
  mu_Air  = mu0       ; 
  nu_Air  = 1/mu_Air  ;
  mu_Iron = 1e3 * mu0 ; 
  nu_Iron = 1/mu_Iron ;

  //=========================================
  aa = 388 ; bb = 0.3774 ; cc = 2.97 ;
  nu_a[]     = aa + bb * Exp[cc*$1] ; // $1 = SquNorm[{b}]
  dnudb2_a[] = bb * cc * Exp[cc*$1] ;

  nu_sat     = aa+bb*Exp[cc*1.29*1.29]; 
  mur_sat    = 1.0/(mu0 * nu_sat)     ;
  mur        = mur_sat                ;
  Freq       = 5000                   ; 
  T          = 1/Freq                 ; 
  nbrT       = 1./1                   ;
  nbrstep    = 5                      ;  
  t0         = 0                      ; 
  tmax       = nbrT*T                 ; 
  dt         = T/nbrstep              ;  
  MaxNumIter = 30                     ; 
  Tolerance  = 1e-4                   ;

  Flag_NL = 0;

  sigma[Omega_C]   = 5e6   ; //sigma[Omega_CC] = 1e-9;
  nu[Omega_L]      = nu_Air; //10 * nu_sat ;  
  If(!Flag_NL)
    nu[Omega_NL]   = nu_Iron; //1. * nu_sat ;
    dhdb_NL[Omega] = TensorDiag[0., 0., 0.] ;
  EndIf
  If(Flag_NL)
    nu[Omega_NL]      = nu_a[SquNorm[$1]] ;
    dhdb_NL[Omega_NL] = 2*dnudb2_a[SquNorm[$1]] * SquDyadicProduct[$1] ;
  EndIf

  Val_I = 2.5e7 ;
  j_fixed[Omega_S1] = Vector[ 0, 0,  Val_I] * F_Sin_wt_p[]{2*Pi*Freq, 0}; //Using h_fixed is equivalent to use a surface current:
  j_fixed[Omega_S2] = Vector[ 0, 0, -Val_I] * F_Sin_wt_p[]{2*Pi*Freq, 0}; //Using h_fixed is equivalent to use a surface current:
}

//-------------------------------------------------------------------------------------

Jacobian {
  { Name Vol; Case { { Region All ; Jacobian Vol; } } }
  { Name Sur; Case { { Region All ; Jacobian Sur; } } }
}

Integration {
  { Name I1 ; Case { { Type Gauss ; Case {
          { GeoElement Triangle   ; NumberOfPoints  7 ; }
	  { GeoElement Quadrangle ; NumberOfPoints  4 ; }
	  { GeoElement Line       ; NumberOfPoints  13 ; }
        } }
    }
  }
}

Constraint {
  { Name MVP_2D ;
    Case {
      { Region Gamma_Inf   ; Type Assign; Value  0 ; } // needed only in magnetostatics
    }
  }
 { Name Current_2D ;
    Case {
      For k In {1:nlam}
      { Region Iron~{k}   ; Type Assign; Value  0 ; } // forcing net current in each lamination to be equal to zero
      EndFor
    }
  }
}

//-----------------------------------------------------------------------------------------------

FunctionSpace {

  { Name Hcurl_a_2D ; Type Form1P ;
    BasisFunction {
      { Name se1 ; NameOfCoef ae1 ; Function BF_PerpendicularEdge ;
        Support #{Omega} ; Entity NodesOf [ All] ; }
   }
    Constraint {
      { NameOfCoef ae1 ; EntityType NodesOf  ; NameOfConstraint MVP_2D ; }
    }
  }

// Gradient of Electric scalar potential (2D)
  { Name Hregion_u_2D ; Type Form1P ;
    BasisFunction {
      { Name sr ; NameOfCoef ur ; Function BF_RegionZ ;
        Support Omega_C ; Entity Omega_C ; }
    }
    GlobalQuantity {
      { Name U ; Type AliasOf        ; NameOfCoef ur ; }
      { Name I ; Type AssociatedWith ; NameOfCoef ur ; }
    }
    Constraint {
      { NameOfCoef U ;
        EntityType Region ; NameOfConstraint Voltage_2D ; }
      { NameOfCoef I ;
        EntityType Region ; NameOfConstraint Current_2D ; }
    }
  }


  { Name H_vector ; Type Vector;
    BasisFunction {
      { Name sex ; NameOfCoef aex ; Function BF_VolumeX ; Support Omega_NL ; Entity VolumesOf[ Omega_NL ] ; }
      { Name sey ; NameOfCoef aey ; Function BF_VolumeY ; Support Omega_NL ; Entity VolumesOf[ Omega_NL ] ; }
      { Name sez ; NameOfCoef aez ; Function BF_VolumeZ ; Support Omega_NL ; Entity VolumesOf[ Omega_NL ] ; }
    }
  } 
}

//-----------------------------------------------------------------------------------------------

Formulation {
  { Name MagDyn_a_2D_JilesAtherton; Type FemEquation ;
    Quantity {
      { Name a ; Type Local  ; NameOfSpace Hcurl_a_2D ; }
      { Name h ; Type Local  ; NameOfSpace H_vector ; } // For saving h_Jiles
      { Name ur ; Type Local  ; NameOfSpace Hregion_u_2D ; }
      { Name I  ; Type Global ; NameOfSpace Hregion_u_2D [I] ; }
      { Name U  ; Type Global ; NameOfSpace Hregion_u_2D [U] ; }
    }
    Equation {
      Galerkin { DtDof[ sigma[] * Dof{a} , {a} ]  ; In Omega_C; Jacobian Vol; Integration I1; }
      Galerkin { DtDof [ sigma[] * Dof{a} , {ur} ]; In Omega_C ; Jacobian Vol; Integration I1 ; }
      Galerkin { [ sigma[] * Dof{ur}, {a}  ]      ; In Omega_C ; Jacobian Vol ; Integration I1 ; }
      Galerkin { [ sigma[] * Dof{ur}, {ur} ]      ; In Omega_C ; Jacobian Vol ; Integration I1 ; }    
      GlobalTerm { [ Dof{I} , {U} ]               ; In Omega_C ; } 
      Galerkin { [ nu[{d a}] * Dof{d a}, {d a} ]  ; In Omega; Jacobian Vol; Integration I1; }
      Galerkin { JacNL[ dhdb_NL[{d a}] * Dof{d a}, {d a} ]; In Omega_NL; Jacobian Vol; Integration I1; }
      Galerkin { [ (-j_fixed[]), {a} ]; In Omega_S; Jacobian Sur; Integration I1; }
    }
  }  
}

//-----------------------------------------------------------------------------------------------

Resolution {
  { Name MagDyn_a_2D_JilesAtherton ;
    System {
      { Name A ; NameOfFormulation MagDyn_a_2D_JilesAtherton ; }
    }
    Operation {

      InitSolution[A] ; //InitSolution[A] ;
      TimeLoopTheta[ t0, tmax, dt, 1 ]{
        IterativeLoop[MaxNumIter, Tolerance, (($Iteration<10) ? 1. : 0.6)] {
          GenerateJac[A]; SolveJac[A];
        }
        SaveSolution[A] ;
      }
    }
  }
}
//-----------------------------------------------------------------------------------------------

PostProcessing {
 { Name MagDyn_a_2D_JA ; NameOfFormulation MagDyn_a_2D_JilesAtherton ;
   PostQuantity {
     { Name a ; Value { Term { [ {a} ]        ; In Omega ; Jacobian Vol ; } } }
     { Name ur ; Value { Term { [ {ur} ]      ; In Omega ; Jacobian Vol ; } } }
     { Name az; Value { Term { [ CompZ[{a}] ] ; In Omega ; Jacobian Vol ; } } }
     { Name b ; Value { Term { [ {d a} ]      ; In Omega ; Jacobian Vol ; } } }
     { Name h ; Value { 
         Term { [ nu[ {d a} ] * {d a} ]       ; In Omega  ; Jacobian Vol ; } } }
     { Name e ; Value { Term { [ -Dt[{a}] ]   ; In Omega ; Jacobian Vol ; } } }
     { Name j ; Value { Term { [ -sigma[] * (Dt[{a}] + {ur}) ]           ; In Omega_C ; Jacobian Vol ; } } }
     { Name jz ; Value { Term { [ CompZ[-sigma[] * ( Dt[{a}] + {ur} )] ] ; In Omega_C ; Jacobian Vol ; } } }
     { Name rhoj2 ; Value { Term { [ sigma[]*SquNorm[Dt[{a}]  + {ur} ] ] ; In Omega_C; Jacobian Vol; } } }
     { Name JouleLosses ; Value { Integral { [ sigma[] * SquNorm[ Dt[{a} ]  + {ur} ] ]; In Omega_C; Jacobian Vol; Integration I1; } } }
   } 
 }
}
//-----------------------------------------------------------------------------------------------
L_X = 0.2 ;
L_Y = L_X ;

index = 137;
PostOperation MapDyn UsingPost MagDyn_a_2D_JA {
  Print[ az    ,  OnElementsOf Omega, File Sprintf("res_dyn/a_%g.pos", index)    , Format Gmsh ] ;
  Print[ ur    ,  OnElementsOf Omega, File Sprintf("res_dyn/ur_%g.pos", index)    , Format Gmsh ] ;
  Print[ b     ,  OnElementsOf Omega, File Sprintf("res_dyn/b_%g.pos", index)    , Format Gmsh ] ;
  Print[ h     ,  OnElementsOf Omega, File Sprintf("res_dyn/h_%g.pos", index)    , Format Gmsh ] ;
  Print[ e     ,  OnElementsOf Omega, File Sprintf("res_dyn/e_%g.pos", index)    , Format Gmsh ] ;
  Print[ j     ,  OnElementsOf Omega, File Sprintf("res_dyn/j_%g.pos", index)    , Format Gmsh ] ;
  Print[ jz    ,  OnElementsOf Omega, File Sprintf("res_dyn/jz_%g.pos", index)   , Format Gmsh ] ;
  Print[ rhoj2 ,  OnElementsOf Omega, File Sprintf("res_dyn/rhoj2_%g.pos", index), Format Gmsh ] ;
}
