// Flag_Fine = 1 ; //==> laminated domain
// getdp square2d_.pro -msh square2d_.msh -sol MagDyn_a_t_NL -pos Dyn_NL -v 1
// getdp square2d_.pro -msh square2d_.msh -sol MagDyn_a_t_Hyst -pos Dyn_Hyst -v 1

// Flag_Fine = 0 ; //==> homogenized domain
// getdp square2d_.pro -msh homog.msh -sol MagDyn_a_t_Homo_NL -pos Dyn_Homo_NL -v 1
// getdp square2d_.pro -msh homog.msh -sol MagDyn_a_t_Homo_Hyst -pos Dyn_Homo_Hyst -v 1

Include "square2d_data.pro" ;

//==========================================================
Include "current_Flags.pro" ;
//==========================================================

SymmetryFactor = 4 ;
AxialLength = 1; //1 m is the default

N = 100;

DefineConstant[ Flag_NL_law = { 1, Choices{
      0="linear",
      1="analytical",
      2="anhysteretic part of JA-model",
      3="Jiles-Atherton hysteresis model"}, Name "Nonlinear BH curve"} ];

DefineConstant[ ORDER = { 2, Choices{
      1="zero",
      2="2nd",
      3="4rd",
      4="6th",
      5="8th"}, Name "Order of homogenization approximation"} ];

ORDERMAX = 5;

Printf("==============> Flag_Fine = %g", Flag_Fine);
Printf("==============> Frequency = %g", Freq);
Printf("==============> Flag_NL_law = %g", Flag_NL_law);
Printf("==============> ORDER = %g", ORDER);


Dir="res/";
If(Flag_Fine)
  ExtGmsh    = Str[ Sprintf("_nl%g_f%g.pos", Flag_NL_law, Freq) ];
  ExtGnuplot = Str[ Sprintf("_nl%g_f%g.dat", Flag_NL_law, Freq) ];
EndIf
If(!Flag_Fine)
  ExtGmsh    = Str[ Sprintf("_nl%g_f%g_n%g.pos", Flag_NL_law, Freq, ORDER) ];
  ExtGnuplot = Str[ Sprintf("_nl%g_f%g_n%g.dat", Flag_NL_law, Freq, ORDER) ];
EndIf


Group {


  For k In {1:N}
    Iron~{k} = #{(IRON+k-1)};
    //Iron~{k} = #{(IRON+k-1)};
    Iron += Region[{Iron~{k}}];
    SkinIron += Region[{(SKINIRON+k-1)}];
  EndFor

  Isolation = #ISOL;
  Air  = #AIR;
  OuterBnd = #OUTERBND;

  SurfaceGe0 = #{MIDDLE, MIDDLE_IND};
  SurfaceSym = #SYMMETRY_X0;
  SurfacesDirichletBC = Region[{SurfaceGe0, OuterBnd, SurfaceSym}];

  Ind  = #IND;

  DomainS0    = Region[ {Ind} ];
  DomainIsol  = Region[ {Isolation} ];
  DomainHomog = Region[ {Iron, Isolation} ];

  DomainL   = Region[ {Air, Ind, Isolation} ] ;
  DomainNL  = Region[ {Iron} ];

  DomainCC  = Region[ {Air, Ind, Isolation} ] ;
  DomainC   = Region[ {Iron} ];

  Domain    = Region[ {DomainC, DomainCC}] ;
}

Include "matlab/BH_anhysteretic.pro";

Function {
  T = 1./Freq;
  Omega = 2*Pi*Freq;
  mu0 = 4.e-7*Pi;
  nu0 = 1./mu0;
  sigmaIron = 5e6;
  sigma[]   = sigmaIron;
  la = lambda; // Fill factor
  sigmaH[] = sigmaIron/lambda ;
  mur = 2500; // linear case (testing purposes)
  nu[#{Air, Ind, Isolation}] = 1./mu0 ;
  nuIron = nu0/mur ;

  // Parameters for Jiles-Atherton hysteresis model
  //Gyselink's paper: Incorporation of a Jiles-Atherton vector hysteresis model in 2D FE magnetic field computations
  Msat = 1145220; aaa = 59.5; kkk = 99.2; ccc = 0.54; alpha = 1.3e-4 ;
  hyst_FeSi = { Msat, aaa, kkk, ccc, alpha};

  If(Flag_NL_law==0) // For testing purposes
    nu[#{Iron}] = nuIron * (Flag_Fine ? 1.: lambda) ;
    h[] = $1*nuIron ;
    dhdb[]    = TensorDiag[1., 1., 1.] * nuIron ;
    dhdb_NL[] = TensorDiag[0., 0., 0.] ;
  EndIf

  // Example of analytical nonlinear law
  aa = 100; bb = 10;     cc = 1.80; // Ruth's values
  //aa = 388; bb = 0.3774; cc = 2.97; // Inno's values
  nu_1a[]     = aa + bb * Exp[cc*SquNorm[$1]] ; // $1 => b ={d a}
  dnudb2_1a[] = bb *cc* Exp[cc*SquNorm[$1]] ;
  h_1a[]      = nu_1a[$1#1] * #1 ;
  dhdb_1a[]   = TensorDiag[1,1,1] * nu_1a[$1#1] + 2*dnudb2_1a[#1] * SquDyadicProduct[#1]  ;
  dhdb_1a_NL[]= 2*dnudb2_1a[$1#1] * SquDyadicProduct[#1]  ;

  If (Flag_NL_law==1)  // Using analytical law
    h[]         = h_1a[$1];
    dhdb[]      = dhdb_1a[$1];
    nu[#{Iron}] = nu_1a[$1];
    dhdb_NL[]   = dhdb_1a_NL[$1];
  EndIf
  If (Flag_NL_law==2) //Using anhysteretic curve from Jiles-Atherton model
    h[]         = h_anhys[$1];
    dhdb[]      = dhdb_anhys[$1];
    nu[#{Iron}] = nu_anhys[$1];
    dhdb_NL[]   = dhdb_anhys_NL[$1];
  EndIf

  // Source
    //==============
    Ns[#{Ind}]       = (-3.5e9)/1.0;
  js0[] = Ns[] * Vector[0,0,1] ;
  js[]  = js0[] * (F_Sin_wt_p[]{2*Pi*Freq, 0.} );
  //js[]  = js0[] * (1.0 +  F_Sin_wt_p[]{2*Pi*Freq, 0. } );
  //js[]  = js0[] * (F_Sin_wt_p[]{2*Pi*Freq, 0.} + 0.05 * F_Sin_wt_p[]{10*Pi*Freq, 0.});

  NbT = 2. ;
  NbSteps = 200; //500, 1e3
  time0 = 0. ;
  timemax = T*NbT ;
  dtime = T/NbSteps ;
  theta_value = 1; // Time discretisation scheme: 1==Euler implicit; 0.5==Cranck-Nicolson


  //NbT = 2. ;
  //NbSteps = 80; //500, 1e3
  //time0 = 0. ;
  //timemax = T*NbT ;
  //dtime = T/(5 * NbSteps) ;
  //theta_value = 1; // Time discretisation scheme: 1==Euler implicit; 0.5==Cranck-Nicolson

  // Determine the final and end time for computing the hysteretic losses
  //=====================================================================
  num = 1;
  t_init = 1.5 * T;
  t_final = t_init + num * T;

  Nb_max_iter = 30;
  reltol = 1e-4;
  abstol = 1e-6;
  relaxation_factor[] = ($Iteration < Nb_max_iter/2) ? 1: 0.3;
}

Include "matlab/PQ.pro";
Include Sprintf("matlab/gausspoints_Ngp%g.pro", 5); // integration along the thickness of the lamination

Jacobian {
  { Name JVol ;
    Case {
      { Region All ; Jacobian Vol ; }
    }
  }
  { Name JSur ; Case { { Region All ; Jacobian Sur; } } }
}

Integration {
  { Name I ; Case {
      { Type Gauss ;
        Case {
          { GeoElement Triangle    ; NumberOfPoints  6 ; }
          { GeoElement Quadrangle  ; NumberOfPoints  7 ; }
          { GeoElement Tetrahedron ; NumberOfPoints  4 ; }
        }
      }
    }
  }
}

Constraint {

  { Name MVP  ; Type Assign ;
    Case {
      { Region SurfacesDirichletBC ; Value 0. ; }
    }
  }

  { Name MVP_aux  ; Type Assign ;
    Case {
      { Region SurfacesDirichletBC ; Value 0. ; }
    }
  }


  { Name Current_2D  ; Type Assign ; // Zero net current in each lamination
    Case {
      For k In {1:N}
        { Region Iron~{k} ; Value 0. ; }
      EndFor
    }
  }

  { Name Voltage_2D  ; Type Assign ;
    Case {
    }
  }

}

FunctionSpace {

  { Name Hcurl_a ; Type Form1P ;
    BasisFunction {
      { Name se  ; NameOfCoef ae  ; Function BF_PerpendicularEdge ;
        Support Domain ; Entity NodesOf[All] ; }
     }
    Constraint {
      { NameOfCoef ae ; EntityType NodesOf ; NameOfConstraint MVP ; }
    }
  }

  // Gradient of Electric scalar potential (2D)
  { Name Hregion_u_2D ; Type Form1P ;
    BasisFunction {
      { Name sr ; NameOfCoef ur ; Function BF_RegionZ ;
        Support DomainC ; Entity DomainC ; }
    }
    GlobalQuantity {
      { Name U ; Type AliasOf        ; NameOfCoef ur ; }
      { Name I ; Type AssociatedWith ; NameOfCoef ur ; }
    }
    Constraint {
      { NameOfCoef U ; EntityType Region ; NameOfConstraint Voltage_2D ; }
      { NameOfCoef I ; EntityType Region ; NameOfConstraint Current_2D ; }
    }
  }


  { Name H_hysteresis ; Type Vector;
    BasisFunction {
      { Name sex ; NameOfCoef aex ; Function BF_VolumeX ; Support DomainNL ; Entity VolumesOf[ All ] ; }
      { Name sey ; NameOfCoef aey ; Function BF_VolumeY ; Support DomainNL ; Entity VolumesOf[ All ] ; }
      { Name sez ; NameOfCoef aez ; Function BF_VolumeZ ; Support DomainNL ; Entity VolumesOf[ All ] ; }
    }
  }

  // Store hystory for every integration point along thickness
  NbGaussPnts = Nqp;
  For i In {1:NbGaussPnts}
  { Name H_hysteresis~{i} ; Type Vector;
    BasisFunction {
      { Name sex ; NameOfCoef aex ; Function BF_VolumeX ; Support DomainNL ; Entity VolumesOf[ All ] ; }
      { Name sey ; NameOfCoef aey ; Function BF_VolumeY ; Support DomainNL ; Entity VolumesOf[ All ] ; }
      { Name sez ; NameOfCoef aez ; Function BF_VolumeZ ; Support DomainNL ; Entity VolumesOf[ All ] ; }
    }
  }
  EndFor

}


Formulation {
  // ====================================================================
  // HYSTERESIS
  // ====================================================================
  { Name MagDyn_a_Hyst ; Type FemEquation ;
    Quantity {
      { Name a  ; Type Local  ; NameOfSpace Hcurl_a ; }
      { Name ur ; Type Local  ; NameOfSpace Hregion_u_2D ; }
      { Name I  ; Type Global ; NameOfSpace Hregion_u_2D [I] ; }
      { Name U  ; Type Global ; NameOfSpace Hregion_u_2D [U] ; }
      { Name h  ; Type Local  ; NameOfSpace H_hysteresis ; }
    }
    Equation {
      Galerkin { [ nu[] * Dof{d a} , {d a} ]     ; In DomainL ; Jacobian JVol ; Integration I ; }

      Galerkin { DtDof[ sigma[] * Dof{a} , {a} ] ; In DomainC ; Jacobian JVol ; Integration I ; }
      Galerkin { [ sigma[] * Dof{ur} , {a} ]     ; In DomainC ; Jacobian JVol ; Integration I ; }

      Galerkin { DtDof [ sigma[] * Dof{a} , {ur} ] ; In DomainC ; Jacobian JVol ; Integration I ; }
      Galerkin { [ sigma[] * Dof{ur}, {ur} ]       ; In DomainC ; Jacobian JVol ; Integration I ; }
      GlobalTerm { [ Dof{I} , {U} ]                ; In DomainC ; }

      Galerkin { [ -js[], {a} ] ; In DomainS0     ; Jacobian JVol; Integration I ; }

      Galerkin { [ h_Jiles[ {h}[1], {d a}[1], {d a} ]{List[hyst_FeSi]}#7, {d a} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }
      Galerkin { JacNL[ dhdb_Jiles[{h}, {d a}, {h}-{h}[1] ]{List[hyst_FeSi]} * Dof{d a} , {d a} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }

      Galerkin { [ Dof{h}, {h} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }
      Galerkin { [    -#7, {h} ]  ;
        In DomainNL ; Jacobian JVol ; Integration I ; }
    }
  }
}

Resolution {
  { Name MagDyn_a_Hyst ;
    System {
      //{ Name H ; NameOfFormulation GetH ; }
      { Name A ; NameOfFormulation MagDyn_a_Hyst ; }
    }
    Operation {
      InitSolution[A] ; InitSolution[A] ;
      TimeLoopTheta[ time0, timemax, dtime, theta_value]{
        IterativeLoop[Nb_max_iter, reltol, relaxation_factor[]] {
          GenerateJac[A]; SolveJac[A];
        }
        SaveSolution[A] ;
      }
    }
  }
}

PostProcessing {
  { Name MagDyn_a_Hyst ; NameOfFormulation MagDyn_a_Hyst ;
    PostQuantity {
      { Name a  ; Value { Local { [ {a} ] ; In Domain ; Jacobian JVol; } } }
      { Name az ; Value { Local { [ CompZ[{a}] ] ; In Domain ; Jacobian JVol; } } }

      { Name b ; Value { Local { [ {d a} ] ; In Domain ; Jacobian JVol; } } }
      { Name h ; Value {
          Term { [ nu[] * {d a} ]   ; In DomainL  ; Jacobian JVol ; }
          Term { [ {h} ]            ; In DomainNL ; Jacobian JVol ; } } }
      { Name hb ; Value { Term { [ TensorSym[
                CompX[nu[{d a}]*{d a}], CompY[nu[{d a}]*{d a}], CompZ[nu[{d a}]*{d a}],
                CompX[{d a}],           CompY[{d a}],           CompZ[{d a}]] ]; In DomainNL ; Jacobian JVol ; } } }

      { Name j  ; Value { Local { [ -sigma[]*(Dt[{a}]+{ur}) ] ; In DomainC ; Jacobian JVol; } } }
      { Name jz ; Value { Local { [ CompZ[-sigma[]*(Dt[{a}]+{ur})] ] ; In DomainC ; Jacobian JVol; } } }
      { Name js  ; Value { Local { [ js[] ] ; In DomainS0 ; Jacobian JVol; } } }

      { Name U ; Value { Term { [ {U} ] ; In DomainC ; } } }
      { Name I ; Value { Term { [ {I} ] ; In DomainC ; } } }

      { Name JouleLosses ; Value {
          Integral { [ sigma[] * SquNorm[Dt[{a}] + {ur} ] ] ; In DomainC ; Jacobian JVol ; Integration I ; } } }
      { Name HystLosses ; Value {
          Integral { [ 0.5 * ( {h} + {h}[1] ) * ({d a} - {d a}[1] ) ] ; In DomainNL ; Jacobian JVol ; Integration I ; } } }

      { Name MagEnergyHomogRegion ; Value {
          Integral { [ {h} * Dt[ {d a} ] ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
          Integral { [ nu[] * {d a} * Dt[ {d a} ] ] ; In DomainIsol ; Jacobian JVol ; Integration I ; } } }

      { Name MagEnergy ; Value {
          Integral { [ {h} * Dt[ {d a} ] ] ; In DomainNL ; Jacobian JVol ; Integration I ; }
          Integral { [ nu[] * {d a} * Dt[ {d a} ] ] ; In DomainL ; Jacobian JVol ; Integration I ; } } }

      { Name TotalEnergyHomog ; Value {
          Integral { [ sigma[] * SquNorm[Dt[{a}] + {ur} ] ] ; In DomainC    ; Jacobian JVol ; Integration I ; }
          Integral { [ {h} * Dt[ {d a} ] ]                  ; In DomainNL   ; Jacobian JVol ; Integration I ; }
          Integral { [ nu[] * {d a} * Dt[ {d a} ] ]         ; In DomainIsol ; Jacobian JVol ; Integration I ; } } }

      { Name TotalEnergy ; Value {
          Integral { [ sigma[] * SquNorm[Dt[{a}] + {ur} ] ] ; In DomainC    ; Jacobian JVol ; Integration I ; }
          Integral { [ {h} * Dt[ {d a} ] ]                  ; In DomainNL   ; Jacobian JVol ; Integration I ; }
          Integral { [ nu[] * {d a} * Dt[ {d a} ] ]         ; In DomainL    ; Jacobian JVol ; Integration I ; } } }

      { Name Flux ; Value { Integral { [ SymmetryFactor*AxialLength*js0[]*{a} ] ; In DomainS0 ; Jacobian JVol ; Integration I ; } } }
    }
  }
}

PostOperation {
  L_X = h ; L_Y=h;
  x0 = 0 ; // Center
  x1 = 0.25*L_X; // must be < 0.5*L_X

  lam_init = 0 ;
  lam_end  = N-1 ;
  { Name Dyn_NL_Hyst ; NameOfPostProcessing MagDyn_a_Hyst ;
    Operation {
      Print[ Flux[Ind], OnGlobal, Format TimeTable, File StrCat[Dir, Sprintf("Flux_nl%g_f%g.dat", Flag_NL_law, Freq)] ] ;
      Print[ JouleLosses[Iron], OnGlobal, Format TimeTable, File StrCat[Dir, Sprintf("JL_nl%g_f%g.dat", Flag_NL_law, Freq)] ] ;
      Print[ HystLosses[Iron], OnGlobal, Format TimeTable, File StrCat[Dir, Sprintf("HL_nl%g_f%g.dat", Flag_NL_law, Freq) ], TimeStep{1*NbSteps:2*NbSteps-1} ] ;

      Print[ MagEnergyHomogRegion[DomainHomog], OnGlobal, Format TimeTable, File StrCat[Dir, Sprintf("MagEnergyHomog_nl%g_f%g.dat", Flag_NL_law, Freq) ] ] ;
      Print[ MagEnergy[Domain], OnGlobal, Format TimeTable, File StrCat[Dir, Sprintf("MagEnergy_nl%g_f%g.dat", Flag_NL_law, Freq) ] ] ;

      Print[ TotalEnergyHomog[DomainHomog], OnGlobal, Format TimeTable, File StrCat[Dir, Sprintf("TotalEnergyHomog_nl%g_f%g.dat", Flag_NL_law, Freq) ] ] ;
      Print[ TotalEnergy[Domain], OnGlobal, Format TimeTable, File StrCat[Dir, Sprintf("TotalEnergy_nl%g_f%g.dat", Flag_NL_law, Freq) ] ] ;

      Print[ HystLosses[Iron], OnGlobal, Format TimeTable, File StrCat[Dir, Sprintf("HL_nl%g_f%g.dat", Flag_NL_law, Freq) ], TimeStep{5*NbSteps:(10*NbSteps-1) } ] ;

      Print[ az, OnElementsOf Domain,  File StrCat[Dir,StrCat["az",ExtGmsh]] ];
      Print[ j,  OnElementsOf DomainC, File StrCat[Dir,StrCat["j",ExtGmsh]] ];
      Print[ b,  OnElementsOf Domain,  File StrCat[Dir,StrCat["b",ExtGmsh]] ];
      Print[ h,  OnElementsOf Domain , File StrCat[Dir,StrCat["h",ExtGmsh]] ];
    }
  }
}

