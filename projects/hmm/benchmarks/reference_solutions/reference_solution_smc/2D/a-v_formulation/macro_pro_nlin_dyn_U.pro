// Flag_Fine = 1 ; //==> laminated domain
// getdp square2d_.pro -msh square2d_.msh -sol MagDyn_a_t_NL -pos Dyn_NL -v 1
// getdp square2d_.pro -msh square2d_.msh -sol MagDyn_a_t_Hyst -pos Dyn_Hyst -v 1

// Flag_Fine = 0 ; //==> homogenized domain
// getdp square2d_.pro -msh homog.msh -sol MagDyn_a_t_Homo_NL -pos Dyn_Homo_NL -v 1
// getdp square2d_.pro -msh homog.msh -sol MagDyn_a_t_Homo_Hyst -pos Dyn_Homo_Hyst -v 1

Include "square2d_data.pro" ;

//==========================================================
Include "current_Flags.pro" ;
//==========================================================

SymmetryFactor = 4 ;
AxialLength = 1; //1 m is the default

N = 100;

DefineConstant[ Flag_NL_law = { 1, Choices{
      0="linear",
      1="analytical",
      2="anhysteretic part of JA-model",
      3="Jiles-Atherton hysteresis model"}, Name "Nonlinear BH curve"} ];

DefineConstant[ ORDER = { 2, Choices{
      1="zero",
      2="2nd",
      3="4rd",
      4="6th",
      5="8th"}, Name "Order of homogenization approximation"} ];

ORDERMAX = 5;

Printf("==============> Flag_Fine = %g", Flag_Fine);
Printf("==============> Flag_NL_law = %g", Flag_NL_law);
Printf("==============> ORDER = %g", ORDER);


Dir="res/";
If(Flag_Fine)
  ExtGmsh    = Str[ Sprintf("_nl%g_f%g.pos", Flag_NL_law, Freq) ];
  ExtGnuplot = Str[ Sprintf("_nl%g_f%g.dat", Flag_NL_law, Freq) ];
EndIf
If(!Flag_Fine)
  ExtGmsh    = Str[ Sprintf("_nl%g_f%g_n%g.pos", Flag_NL_law, Freq, ORDER) ];
  ExtGnuplot = Str[ Sprintf("_nl%g_f%g_n%g.dat", Flag_NL_law, Freq, ORDER) ];
EndIf


Group {


  For k In {1:N}
    Iron~{k} = #{(IRON+k-1)};
    //Iron~{k} = #{(IRON+k-1)};
    Iron += Region[{Iron~{k}}];
    SkinIron += Region[{(SKINIRON+k-1)}];
  EndFor

  Isolation = #ISOL;
  Air  = #AIR;
  OuterBnd = #OUTERBND;

  SurfaceGe0 = #{MIDDLE, MIDDLE_IND};
  SurfaceSym = #SYMMETRY_X0;
  SurfacesDirichletBC = Region[{SurfaceGe0, OuterBnd, SurfaceSym}];

  Ind  = #IND;

  DomainS0 = Region[ {Ind} ];

  DomainL   = Region[ {Air, Ind, Isolation} ] ;
  DomainNL  = Region[ {Iron} ];

  DomainCC  = Region[ {Air, Ind, Isolation} ] ;
  DomainC   = Region[ {Iron} ];

  Domain    = Region[ {DomainC, DomainCC}] ;
}

Include "matlab/BH_anhysteretic.pro";

Function {
  T = 1./Freq;
  Omega = 2*Pi*Freq;

  mu0 = 4.e-7*Pi;
  nu0 = 1./mu0;

  sigmaIron = 5e6;
  sigma[]   = sigmaIron;

  la = lambda; // Fill factor
  sigmaH[] = sigmaIron/lambda ;

  mur = 2500; // linear case (testing purposes)

  nu[#{Air, Ind, Isolation}] = 1./mu0 ;
  nuIron = nu0/mur ;

  // Parameters for Jiles-Atherton hysteresis model
  //Gyselink's paper: Incorporation of a Jiles-Atherton vector hysteresis model in 2D FE magnetic field computations
  Msat = 1145220; aaa = 59.5; kkk = 99.2; ccc = 0.54; alpha = 1.3e-4 ;
  hyst_FeSi = { Msat, aaa, kkk, ccc, alpha};

  If(Flag_NL_law==0) // For testing purposes
    nu[#{Iron}] = nuIron * (Flag_Fine ? 1.: lambda) ;
    h[] = $1*nuIron ;
    dhdb[]    = TensorDiag[1., 1., 1.] * nuIron ;
    dhdb_NL[] = TensorDiag[0., 0., 0.] ;
  EndIf

  // Example of analytical nonlinear law
  aa = 100; bb = 10;     cc = 1.80; // Ruth's values
  //aa = 388; bb = 0.3774; cc = 2.97; // Inno's values
  nu_1a[]     = aa + bb * Exp[cc*SquNorm[$1]] ; // $1 => b ={d a}
  dnudb2_1a[] = bb *cc* Exp[cc*SquNorm[$1]] ;
  h_1a[]      = nu_1a[$1#1] * #1 ;
  dhdb_1a[]   = TensorDiag[1,1,1] * nu_1a[$1#1] + 2*dnudb2_1a[#1] * SquDyadicProduct[#1]  ;
  dhdb_1a_NL[]= 2*dnudb2_1a[$1#1] * SquDyadicProduct[#1]  ;

  If (Flag_NL_law==1)  // Using analytical law
    h[]         = h_1a[$1];
    dhdb[]      = dhdb_1a[$1];
    nu[#{Iron}] = nu_1a[$1];
    dhdb_NL[]   = dhdb_1a_NL[$1];
  EndIf
  If (Flag_NL_law==2) //Using anhysteretic curve from Jiles-Atherton model
    h[]         = h_anhys[$1];
    dhdb[]      = dhdb_anhys[$1];
    nu[#{Iron}] = nu_anhys[$1];
    dhdb_NL[]   = dhdb_anhys_NL[$1];
  EndIf

  // Source
  Ns[#{Ind}]       = -250000000;
  //Sc[#{Ind}] = SurfaceArea[]{IND};

  js0[] = Ns[] * Vector[0,0,1] ;
  //js0[] = Ns[]/Sc[]*Vector[0,0,1] ;
  js[]  = js0[] * F_Sin_wt_p[]{2*Pi*Freq, 0.};

  NbT = 1. ;
  NbSteps = 20; //500, 1e3
  time0 = 0. ; timemax = T*NbT ; dtime = T/NbSteps ;
  theta_value = 1; // Time discretisation scheme: 1==Euler implicit; 0.5==Cranck-Nicolson

  Nb_max_iter = 30;
  reltol = 1e-4;
  abstol = 1e-6;
  relaxation_factor[] = ($Iteration < Nb_max_iter/2) ? 1: 0.3;

  // Legendre Polynomials (used in post-processing)
  // Watch out: yy[] implies even number of laminations ==> FIX ME!
  //yy[] = (Fmod[Fabs[$Y],e]-e/2)*2/d ; // y=z/(d/2)
  yy[] = ((Fmod[Fabs[$Y],e]-e/2)*2/d) ; // y=z/(d/2)
  alpha_0[] = 1 ; // zero order
  alpha_2[] = 1/2*(3*yy[]^2-1) ; //2nd order
  alpha_4[] = 1/8*(35*yy[]^4-30*yy[]^2+3); //4th order
  alpha_6[] = 1/16*(231*yy[]^6-315*yy[]^4+105*yy[]^2-5);
  alpha_8[] = 1/128*(6435*yy[]^8-12012*yy[]^6+6930*yy[]^4-1260*yy[]^2+35);

  // dy = 1/(d/2)*dz = (2/d)*dz
  dalpha_0[] = 0. ;
  dalpha_2[] = 3*yy[]*2/d;
  dalpha_4[] = 1/2*(35*yy[]^3-15*yy[])*2/d; //4th order
  dalpha_6[] = 1/8*(231*3*yy[]^5-315*2*yy[]^3+105*yy[])*2/d;
  dalpha_8[] = 1/64*(6435*4*yy[]^7-12012*3*yy[]^5+6930*2*yy[]^3-1260*yy[])*2/d;

  // dz = (d/2)*dy
  ialpha_0[] = yy[]*d/2 ;
  ialpha_2[] = 1/2*(yy[]^3-yy[])*d/2;
  ialpha_4[] = 1/8*(7*yy[]^5-10*yy[]^3+3*yy[])*d/2;
  ialpha_6[] = 1/16*(33*yy[]^7-63*yy[]^5+35*yy[]^3-5*yy[])*d/2 ;
  ialpha_8[] = 1/128*(6435*yy[]^9/9-12012*yy[]^7/7+6930*yy[]^5/5-1260*yy[]^3/3+35*yy[])*d/2;
}

Include "matlab/PQ.pro";
Include Sprintf("matlab/gausspoints_Ngp%g.pro", 5); // integration along the thickness of the lamination

Jacobian {
  { Name JVol ;
    Case {
      { Region All ; Jacobian Vol ; }
    }
  }
  { Name JSur ; Case { { Region All ; Jacobian Sur; } } }
}

Integration {
  { Name I ; Case {
      { Type Gauss ;
        Case {
          { GeoElement Triangle    ; NumberOfPoints  6 ; }
          { GeoElement Quadrangle  ; NumberOfPoints  7 ; }
          { GeoElement Tetrahedron ; NumberOfPoints  4 ; }
        }
      }
    }
  }
}

Constraint {

  { Name MVP  ; Type Assign ;
    Case {
      { Region SurfacesDirichletBC ; Value 0. ; }
    }
  }

  { Name MVP_aux  ; Type Assign ;
    Case {
      { Region SurfacesDirichletBC ; Value 0. ; }
    }
  }


  { Name Current_2D  ; Type Assign ; // Zero net current in each lamination
    Case {
      For k In {1:N}
        { Region Iron~{k} ; Value 0. ; }
      EndFor
    }
  }

  { Name Voltage_2D  ; Type Assign ;
    Case {
    }
  }

}

FunctionSpace {

  { Name Hcurl_a ; Type Form1P ;
    BasisFunction {
      { Name se  ; NameOfCoef ae  ; Function BF_PerpendicularEdge ;
        Support Domain ; Entity NodesOf[All] ; }
     }
    Constraint {
      { NameOfCoef ae ; EntityType NodesOf ; NameOfConstraint MVP ; }
    }
  }

  // Gradient of Electric scalar potential (2D)
  { Name Hregion_u_2D ; Type Form1P ;
    BasisFunction {
      { Name sr ; NameOfCoef ur ; Function BF_RegionZ ;
        Support DomainC ; Entity DomainC ; }
    }
    GlobalQuantity {
      { Name U ; Type AliasOf        ; NameOfCoef ur ; }
      { Name I ; Type AssociatedWith ; NameOfCoef ur ; }
    }
    Constraint {
      { NameOfCoef U ; EntityType Region ; NameOfConstraint Voltage_2D ; }
      { NameOfCoef I ; EntityType Region ; NameOfConstraint Current_2D ; }
    }
  }


  { Name H_hysteresis ; Type Vector;
    BasisFunction {
      { Name sex ; NameOfCoef aex ; Function BF_VolumeX ; Support DomainNL ; Entity VolumesOf[ All ] ; }
      { Name sey ; NameOfCoef aey ; Function BF_VolumeY ; Support DomainNL ; Entity VolumesOf[ All ] ; }
      { Name sez ; NameOfCoef aez ; Function BF_VolumeZ ; Support DomainNL ; Entity VolumesOf[ All ] ; }
    }
  }

  // Store hystory for every integration point along thickness
  NbGaussPnts = Nqp;
  For i In {1:NbGaussPnts}
  { Name H_hysteresis~{i} ; Type Vector;
    BasisFunction {
      { Name sex ; NameOfCoef aex ; Function BF_VolumeX ; Support DomainNL ; Entity VolumesOf[ All ] ; }
      { Name sey ; NameOfCoef aey ; Function BF_VolumeY ; Support DomainNL ; Entity VolumesOf[ All ] ; }
      { Name sez ; NameOfCoef aez ; Function BF_VolumeZ ; Support DomainNL ; Entity VolumesOf[ All ] ; }
    }
  }
  EndFor

}


Formulation {
  // ====================================================================
  // NONLINEAR - single value b-h curve
  // ====================================================================

  { Name MagDyn_a_NL ; Type FemEquation ;
    Quantity {
      { Name a  ; Type Local  ; NameOfSpace Hcurl_a ; }
      { Name ur ; Type Local  ; NameOfSpace Hregion_u_2D ; }
      { Name I  ; Type Global ; NameOfSpace Hregion_u_2D [I] ; }
      { Name U  ; Type Global ; NameOfSpace Hregion_u_2D [U] ; }
    }
    Equation {
      Galerkin { [ nu[{d a}] * Dof{d a} , {d a} ]  ; In Domain ; Jacobian JVol ; Integration I ; }
      Galerkin { JacNL[ dhdb_NL[{d a}]* Dof{d a} , {d a} ] ; In DomainNL ; Jacobian JVol ; Integration I ; }

      Galerkin { DtDof[ sigma[] * Dof{a} , {a} ]   ; In DomainC ; Jacobian JVol ; Integration I ; }
      Galerkin { [      sigma[] * Dof{ur}, {a} ]   ; In DomainC ; Jacobian JVol ; Integration I ; }

      Galerkin { DtDof[ sigma[] * Dof{a} , {ur} ]  ; In DomainC ; Jacobian JVol ; Integration I ; }
      Galerkin { [      sigma[] * Dof{ur}, {ur} ]  ; In DomainC ; Jacobian JVol ; Integration I ; }
      GlobalTerm { [ Dof{I} , {U} ]                ; In DomainC ; }

      Galerkin { [ -js[] , {a} ] ; In DomainS0     ; Jacobian JVol; Integration I ; }
    }
  }


  // ====================================================================
  // HYSTERESIS
  // ====================================================================
  { Name MagDyn_a_Hyst ; Type FemEquation ;
    Quantity {
      { Name a  ; Type Local  ; NameOfSpace Hcurl_a ; }
      { Name ur ; Type Local  ; NameOfSpace Hregion_u_2D ; }
      { Name I  ; Type Global ; NameOfSpace Hregion_u_2D [I] ; }
      { Name U  ; Type Global ; NameOfSpace Hregion_u_2D [U] ; }
      { Name h  ; Type Local  ; NameOfSpace H_hysteresis ; }
    }
    Equation {
      Galerkin { [ nu[] * Dof{d a} , {d a} ]     ; In DomainL ; Jacobian JVol ; Integration I ; }

      Galerkin { DtDof[ sigma[] * Dof{a} , {a} ] ; In DomainC ; Jacobian JVol ; Integration I ; }
      Galerkin { [ sigma[] * Dof{ur} , {a} ]     ; In DomainC ; Jacobian JVol ; Integration I ; }

      Galerkin { DtDof [ sigma[] * Dof{a} , {ur} ] ; In DomainC ; Jacobian JVol ; Integration I ; }
      Galerkin { [ sigma[] * Dof{ur}, {ur} ]       ; In DomainC ; Jacobian JVol ; Integration I ; }
      GlobalTerm { [ Dof{I} , {U} ]                ; In DomainC ; }

      Galerkin { [ -js[], {a} ] ; In DomainS0     ; Jacobian JVol; Integration I ; }

      Galerkin { [ h_Jiles[ {h}[1], {d a}[1], {d a} ]{List[hyst_FeSi]}#7, {d a} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }
      Galerkin { JacNL[ dhdb_Jiles[{h}, {d a}, {h}-{h}[1] ]{List[hyst_FeSi]} * Dof{d a} , {d a} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }

      Galerkin { [ Dof{h}, {h} ] ;
        In DomainNL ; Jacobian JVol ; Integration I ; }
      Galerkin { [    -#7, {h} ]  ;
        In DomainNL ; Jacobian JVol ; Integration I ; }
    }
  }
}


Resolution {

  { Name MagDyn_a_t_NL ;
    System {
      { Name A ; NameOfFormulation MagDyn_a_NL ; }
    }
    Operation {
      InitSolution[A] ; SaveSolution[A] ;
      TimeLoopTheta[ time0, timemax, dtime, theta_value]{
        IterativeLoop[Nb_max_iter, reltol, relaxation_factor[]] {
          //IterativeLoopN[ Nb_max_iter, relaxation_factor[], System{ {A, reltol, abstol, Residual MeanL2Norm} }]{
          // Solution, Residual, RecalcResidual
          // L1Norm, MeanL1Norm, L2Norm, MeanL2Norm, LinfNorm
          GenerateJac[A]; SolveJac[A];
        }
        SaveSolution[A];
      }
    }
  }
}


PostProcessing {
  //=================================================
  // NONLINEAR
  //=================================================
  { Name MagDyn_a_NL ; NameOfFormulation MagDyn_a_NL ;
    PostQuantity {
      { Name a  ; Value { Local { [ {a} ] ; In Domain ; Jacobian JVol; } } }
      { Name az ; Value { Local { [ CompZ[{a}] ] ; In Domain ; Jacobian JVol; } } }

      { Name b ; Value { Local { [ {d a} ] ; In Domain ; Jacobian JVol; } } }
      { Name h ; Value { Local { [ nu[{d a}]*{d a} ] ; In Domain  ; Jacobian JVol; } } }
      { Name hb ; Value { Term { [ TensorSym[
                CompX[nu[{d a}]*{d a}], CompY[nu[{d a}]*{d a}], CompZ[nu[{d a}]*{d a}],
                CompX[{d a}],           CompY[{d a}],           CompZ[{d a}]] ]; In DomainNL ; Jacobian JVol ; } } }

      { Name j  ; Value { Local { [ -sigma[]*(Dt[{a}]+{ur}) ] ; In DomainC ; Jacobian JVol; } } }
      { Name jz ; Value { Local { [ CompZ[-sigma[]*(Dt[{a}]+{ur})] ] ; In DomainC ; Jacobian JVol; } } }
      { Name js  ; Value { Local { [ js[] ] ; In DomainS0 ; Jacobian JVol; } } }

      { Name U ; Value { Term { [ {U} ] ; In DomainC ; } } }
      { Name I ; Value { Term { [ {I} ] ; In DomainC ; } } }

      { Name JouleLosses ; Value {
          Integral { [ sigma[] * SquNorm[Dt[{a}]] ] ; In DomainC ; Jacobian JVol ; Integration I ; } } }
      { Name MagEnergy ; Value {
          Integral { [ nu[{d a}]*{d a}*Dt[{d a}] ] ; In DomainC ; Jacobian JVol ; Integration I ; } } }

      { Name Flux ; Value { Integral { [ SymmetryFactor*AxialLength*js0[]*{a} ] ; In DomainS0 ; Jacobian JVol ; Integration I ; } } }
    }
  }
}



PostOperation {
  L_X = h ; L_Y=h;
  x0 = 0 ; // Center
  x1 = 0.25*L_X; // must be < 0.5*L_X

  lam_init = 0 ;
  lam_end  = N-1 ;

  { Name Dyn_NL ; NameOfPostProcessing MagDyn_a_NL ;
    Operation {
      Print[ az, OnElementsOf Domain,  File StrCat[Dir,StrCat["az",ExtGmsh]] ];
      Print[ j,  OnElementsOf DomainC, File StrCat[Dir,StrCat["j",ExtGmsh]] ];
      Print[ b,  OnElementsOf Domain,  File StrCat[Dir,StrCat["b",ExtGmsh]] ];
      Print[ h,  OnElementsOf Domain , File StrCat[Dir,StrCat["h",ExtGmsh]] ];

      For i In {lam_init:lam_init}
        For k In {1:NbGaussPnts}
          Print[ hb, OnPoint {x0 , L_Y/2-d/2+y~{k}*d-i*e, 0}, Format TimeTable,
            File StrCat[Dir,Sprintf("hbx0_f%g_l%g_gp%g.dat", Freq, i, k)] ];
          Print[ hb, OnPoint {x1 , L_Y/2-d/2+y~{k}*d-i*e, 0}, Format TimeTable,
            File StrCat[Dir,Sprintf("hbx1_f%g_l%g_gp%g.dat", Freq, i, k)] ];
          Print[ j,  OnPoint {x0 , L_Y/2-d/2+y~{k}*d-i*e, 0}, Format TimeTable,
            File StrCat[Dir,Sprintf("jx0_f%g_l%g_gp%g.dat", Freq, i, k)] ];
          Print[ j,  OnPoint {x1 , L_Y/2-d/2+y~{k}*d-i*e, 0}, Format TimeTable,
            File StrCat[Dir,Sprintf("jx1_f%g_l%g_gp%g.dat", Freq, i, k)] ];
        EndFor
      EndFor

      Print[ Flux[Ind], OnGlobal, Format TimeTable, File StrCat[Dir, Sprintf("Flux_nl%g_f%g.dat", Flag_NL_law, Freq)] ] ;
      Print[ JouleLosses[Iron], OnGlobal, Format TimeTable, File StrCat[Dir, Sprintf("JL_nl%g_f%g.dat", Flag_NL_law, Freq)] ] ;
    }
  }
}

