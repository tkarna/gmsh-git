Include "physical.dat" ;
Include "common.dat";

Mesh.Algorithm = 2; // 2D mesh algorithm (1=MeshAdapt, 2=Automatic, 5=Delaunay, 6=Frontal, 7=bamg, 8=delquad)
Geometry.CopyMeshingMethod = 1; // Copy meshing method when duplicating geometrical entities?

Flag_Transfinite = 1 ;

x_L = - 0.5 * L_X;
y_L = 0.0    ;

p1 = newp;   Point(p1) = {x_L + 0.0              , y_L + 0.0              , 0.0, lc_smc_iso};
p2 = newp;   Point(p2) = {x_L + eps              , y_L + 0.0              , 0.0, lc_smc_iso};
p3 = newp;   Point(p3) = {x_L + eps              , y_L + eps              , 0.0, lc_smc_iso};
p4 = newp;   Point(p4) = {x_L + 0.0              , y_L + eps              , 0.0, lc_smc_iso };
p5 = newp ;  Point(p5)  = {x_L + d_i              , y_L + d_i + r_c        , 0.0, lc_smc_cond};
p6 = newp ;  Point(p6)  = {x_L + d_i + r_c        , y_L +  d_i             , 0.0, lc_smc_cond};
p7 = newp ;  Point(p7)  = {x_L + d_i + r_c        , y_L + d_i + r_c        , 0.0, lc_smc_cond};
p8 = newp ;  Point(p8)  = {x_L + eps - (d_i + r_c), y_L + d_i              , 0.0, lc_smc_cond};
p9 = newp ;  Point(p9)  = {x_L + eps - d_i        , y_L + d_i + r_c        , 0.0, lc_smc_cond};
p10 = newp;  Point(p10) = {x_L + eps - (d_i + r_c), y_L + d_i + r_c        , 0.0, lc_smc_cond};
p11 = newp;  Point(p11) = {x_L + eps - d_i        , y_L + eps - (d_i + r_c), 0.0, lc_smc_cond};
p12 = newp;  Point(p12) = {x_L + eps - (d_i + r_c), y_L + eps - d_i        , 0.0, lc_smc_cond};
p13 = newp;  Point(p13) = {x_L + eps - (d_i + r_c), y_L + eps - (d_i + r_c), 0.0, lc_smc_cond};
p14 = newp;  Point(p14) = {x_L + (d_i + r_c)      , y_L + eps - d_i        , 0.0, lc_smc_cond};
p15 = newp;  Point(p15) = {x_L + d_i              , y_L + eps - (d_i + r_c), 0.0, lc_smc_cond};
p16 = newp;  Point(p16) = {x_L + d_i + r_c        , y_L + eps - (d_i + r_c), 0.0, lc_smc_cond};

l1 = newl ; Line(l1)  = {p1,p2} ;
l2 = newl ; Line(l2)  = {p2,p3} ;
l3 = newl ; Line(l3)  = {p3,p4} ;
l4 = newl ; Line(l4)  = {p4,p1} ;
l5 = newl; Circle(l5) = {p5, p7, p6};
l6 = newl ; Line(l6)  = {p6,p8} ;
l7 = newl; Circle(l7) = {p8, p10, p9};
l8 = newl ; Line(l8)  = {p9,p11} ;
l9 = newl; Circle(l9) = {p11, p13, p12};
l10 = newl ; Line(l10)  = {p12,p14} ;
l11 = newl; Circle(l11) = {p14, p16, p15};
l12 = newl ; Line(l12)  = {p15,p5} ;


ll1 = newll; Line Loop(ll1)   = {l5,l6,l7,l8, l9, l10, l11, l12} ;
s_cond[] += news ; Plane Surface(news) = {ll1} ;
ll2 = newll; Line Loop(ll2)   = {l1,l2,l3,l4} ;
s_iso[]  += news ; Plane Surface(news) = {ll2, ll1} ;

// Duplicate the cell geometry
//============================

bnd[] = Boundary{Surface{s_iso[0],s_cond[0]};};
l_bottom[] += bnd[0];


For k In {1:num-1}
s_cond[] += Translate {k * eps, 0, 0} { Duplicata { Surface{s_cond[{0}] }; } };
s_iso[]+= Translate {k * eps, 0, 0} { Duplicata { Surface{s_iso[{0}]  }; } };
bnd[] = Boundary{Surface{s_iso[k],s_cond[k]};};
l_bottom[] += bnd[0];
EndFor
For k In {1:num/2-1}
s_cond[] += Translate {0, k * eps, 0} { Duplicata { Surface{s_cond[{0:num-1}] }; } };
s_iso[]  += Translate {0, k * eps, 0} { Duplicata { Surface{s_iso[{0:num-1}]  }; } };
EndFor

bnd_num[] = Boundary{Surface{s_iso[num-1],s_cond[num-1]};};
lastCellBnd[] = Boundary{Line{bnd_num[0]}; };
p_lastCellBnd = lastCellBnd[1]; 

// Getting the Boundary of the entire duplicated structure
//========================================================

allSurfaces[] = Surface '*';
bndlines[] = CombinedBoundary{Surface{allSurfaces[]};};

For k In {0:#bndlines[]-1}
Printf("%g", bndlines[k]);
EndFor

alll = newll; Line Loop(newll) = bndlines[];

// Defining the inductors
//=======================

// Upper inductor
//=============== 
p17 = newp; Point(p17) = { -0.5 * w_indu_u             , 0.5 * L_Y + vgap_u          , 0.0};
p18 = newp; Point(p18) = {  0.5 * w_indu_u             , 0.5 * L_Y + vgap_u         , 0.0};
p19 = newp; Point(p19) = {  0.5 * w_indu_u             , 0.5 * L_Y + vgap_u + th_indu_u          , 0.0};
p20 = newp; Point(p20) = { -0.5 * w_indu_u             , 0.5 * L_Y + vgap_u + th_indu_u          , 0.0};

l17 = newl; Line(l17) = {p17, p18}; 
l18 = newl; Line(l18) = {p18, p19}; 
l19 = newl; Line(l19) = {p19, p20}; 
l20 = newl; Line(l20) = {p20, p17}; 

ll_indu_U = newll; Line Loop(ll_indu_U) = {l17, l18, l19, l20};
s_indu_U = news; Plane Surface(s_indu_U) = ll_indu_U;

Transfinite Line{l17, l19} = nth_indu;
Transfinite Line{l18, l20} = nw_indu;
Transfinite Surface{s_indu_U} = {p17, p18, p19, p20}; Recombine Surface{s_indu_U};

// Right inductor
//=============== 
p17 = newp; Point(p17) = {0.5 * L_X + hgap_lat             , vgap_lat         , 0.0};
p18 = newp; Point(p18) = {0.5 * L_X + hgap_lat+th_indu_lat , vgap_lat         , 0.0};
p19 = newp; Point(p19) = {0.5 * L_X + hgap_lat+th_indu_lat , vgap_lat + w_indu_lat, 0.0};
p20 = newp; Point(p20) = {0.5 * L_X + hgap_lat             , vgap_lat + w_indu_lat, 0.0};

l17 = newl; Line(l17) = {p17, p18}; 
l18 = newl; Line(l18) = {p18, p19}; 
l19 = newl; Line(l19) = {p19, p20}; 
l20 = newl; Line(l20) = {p20, p17}; 

ll_indu_R = newll; Line Loop(ll_indu_R) = {l17, l18, l19, l20};
s_indu_R = news; Plane Surface(s_indu_R) = ll_indu_R;

Transfinite Line{l17, l19} = nth_indu;
Transfinite Line{l18, l20} = nw_indu;
Transfinite Surface{s_indu_R} = {p17, p18, p19, p20}; Recombine Surface{s_indu_R};

// Left inductor
//============== 
lcc = 0.00001; 
p17 = newp; Point(p17) = {- (0.5 * L_X + hgap_lat)             , vgap_lat             , 0.0, lcc};
p18 = newp; Point(p18) = {- (0.5 * L_X + hgap_lat+th_indu_lat) , vgap_lat             , 0.0, lcc};
p19 = newp; Point(p19) = {- (0.5 * L_X + hgap_lat+th_indu_lat) , vgap_lat + w_indu_lat, 0.0, lcc};
p20 = newp; Point(p20) = {- (0.5 * L_X + hgap_lat)             , vgap_lat + w_indu_lat, 0.0, lcc};

l17 = newl; Line(l17) = {p17, p18}; 
l18 = newl; Line(l18) = {p18, p19}; 
l19 = newl; Line(l19) = {p19, p20}; 
l20 = newl; Line(l20) = {p20, p17}; 

ll_indu_L = newll; Line Loop(ll_indu_L) = {l17, l18, l19, l20};
s_indu_L = news; Plane Surface(s_indu_L) = -ll_indu_L; // The negative sign in order to have the normal in the right direction

Transfinite Line{l17, l19} = nth_indu;
Transfinite Line{l18, l20} = nw_indu;
Transfinite Surface{s_indu_L} = {p17, p18, p19, p20}; Recombine Surface{s_indu_L};

bndlines[] -=l_bottom[];

// Inner circle
//============= 

p_cen   = newp; Point(p_cen) = {0.0, 0.0, 0.0}; 
p_L_int = newp; Point(p_L_int) = {-Val_Rint, 0.0     , 0.0, lc_rint}; 
p_U_int = newp; Point(p_U_int) = {0.0      , Val_Rint, 0.0, lc_rint}; 
p_R_int = newp; Point(p_R_int) = {Val_Rint , 0.0     , 0.0, lc_rint}; 

c_L_int = newl ; Circle(c_L_int) = {p_U_int, p_cen, p_L_int};
c_R_int = newl ; Circle(c_R_int) = {p_R_int, p_cen, p_U_int};

l_L_int = newl ; Line(l_L_int) = {p_L_int, p1};
l_R_int = newl ; Line(l_R_int) = {p_lastCellBnd, p_R_int};

ll_circle_int = newll; 
Line Loop(ll_circle_int) = {l_L_int, c_L_int, c_R_int, l_R_int, -bndlines[]};
s_circle_int  = news;  Plane Surface(s_circle_int) = {ll_circle_int, ll_indu_L, ll_indu_R, ll_indu_U}; // The negative sign in order to have the normal in the right direction

// Outer circle 
//=============

p_L_ext = newp; Point(p_L_ext) = {-Val_Rext, 0.0     , 0.0, lc_rext}; 
p_U_ext = newp; Point(p_U_ext) = {0.0      , Val_Rext, 0.0, lc_rext}; 
p_R_ext = newp; Point(p_R_ext) = {Val_Rext , 0.0     , 0.0, lc_rext}; 

c_L_ext = newl ; Circle(c_L_ext) = {p_U_ext, p_cen, p_L_ext};
c_R_ext = newl ; Circle(c_R_ext) = {p_R_ext, p_cen, p_U_ext};

l_L_ext = newl ; Line(l_L_ext) = {p_L_ext, p_L_int};
l_R_ext = newl ; Line(l_R_ext) = {p_R_int, p_R_ext};

ll_circle_ext = newll; 
Line Loop(ll_circle_ext) = {l_L_ext, -c_L_int, -c_R_int, l_R_ext, c_R_ext, c_L_ext};
s_circle_ext  = news;  Plane Surface(s_circle_ext) = {ll_circle_ext};


Physical Line(GAMMA_INF) = {c_L_ext, c_R_ext};
Physical Line(SYMMETRY) = {l_L_ext, l_L_int, l_bottom[], l_R_int, l_R_ext};
For k In {0:#s_cond[]-1}
  Physical Surface(IRON+k) = {s_cond[k]} ;
EndFor
Physical Surface(INSU)       = {s_iso[]} ;
Physical Surface(AIR)        = {s_circle_int, s_circle_ext} ;
Physical Surface(OMEGA_U)   = {s_indu_U};
Physical Surface(OMEGA_R)   = {s_indu_R};
Physical Surface(OMEGA_L)   = {s_indu_L};
