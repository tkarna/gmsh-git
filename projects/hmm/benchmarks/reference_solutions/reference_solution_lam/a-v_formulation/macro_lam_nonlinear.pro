Include "macro_lam.dat";

Group {
  Corner        = Region[ { CORNER   } ];
  GammaTop      = Region[ { UP   } ];
  GammaBottom   = Region[ { DOWN } ];
  Omega_NL      = Region[ {SURF1} ]; 
  Omega_L       = Region[ {SURF2} ]; 
  Omega_C       = Region[ {SURF1} ]; 
  Omega_CC      = Region[ {SURF2} ]; 
  Omega         = Region[ {Omega_NL, Omega_L} ] ; // the entire computational domain
  Sur_Fixed_nxh = Region[{GammaTop, GammaBottom}] ;
}

Function {
  NbrMaxIter         = 100;
  Eps                = 1e-4;
  sigma_0            = 137;
  mu0                = 4.e-7 * Pi ;
  murIron            = 100; // For linear cases
  muIron             = mu0 * murIron ;
  mu_1               = muIron;
  mu_2               = mu0;
  nu_1               = 1.0/mu_1;
  nu_2               = 1.0/mu_2;
  Freq = 50 ; T= 1/Freq ;
  nbrT = 1 ;
  nbrstep = 120 ;
  t0 = 0 ; 
  tmax = nbrT*T ;
  dt = T/nbrstep ;
  Flag_MagStat = 0;

  //=========================================
  aa = 388 ; bb = 0.3774 ; cc = 2.97 ;

  nu_a[] = aa + bb * Exp[cc*$1] ; // $1 = SquNorm[{b}]
  dnudb2_a[] = bb * cc * Exp[cc*$1] ;

  nu_sat = aa+bb*Exp[cc*1.29*1.29] ;
  mur_sat = 1/(mu0*nu_sat) ; 
  mur = mur_sat ;
  Flag_NL = 1;
  nu[Omega_L]  = 10 * nu_sat ;
  If(!Flag_NL)
    nu[Omega_NL] = 1. * nu_sat ;
  dhdb_NL[Omega] = TensorDiag[0., 0., 0.] ;
  EndIf
    If(Flag_NL)
    nu[Omega_NL]      = nu_a[SquNorm[$1]] ;
  dhdb_NL[Omega_NL] = 2*dnudb2_a[SquNorm[$1]] * SquDyadicProduct[$1] ;
  EndIf
    
  sigma[Omega_C]  = sigma_0; 
  sigma[Omega_CC] = 0.0;
  Val_I = 250 ;
  h_fixed[] = Vector[Val_I, 0, 0] * F_Sin_wt_p[]{2*Pi*Freq, 0} ;
}

Constraint {
  { Name a ;
    Case { 
      If(Flag_MagStat)
      { Region Corner   ; Type Assign; Value  0 ; } // Needed only in statics
      EndIf
    }
  }
}

Jacobian {
  { Name Vol ;
    Case  {
        { Region All ; Jacobian Vol ; }
    }
  }
  { Name Sur ;
    Case {
      { Region All ; Jacobian Sur ; }
    }
  }
}

Integration {
  { Name II ; Case { {Type Gauss ; Case {
          { GeoElement Line    ; NumberOfPoints  13 ; }
          { GeoElement Triangle    ; NumberOfPoints  7 ; }
          { GeoElement Quadrangle  ; NumberOfPoints  7 ; }
          { GeoElement Tetrahedron ; NumberOfPoints  4 ; }
          { GeoElement Hexahedron  ; NumberOfPoints  6 ; }
          { GeoElement Prism       ; NumberOfPoints  9 ; } }
      }
    }
  }
}

FunctionSpace {
  { Name Hgrad_a ; Type Form1P ;
    BasisFunction{
      { Name sn ; NameOfCoef an ; Function BF_PerpendicularEdge ;
        Support Region[{ Omega, Sur_Fixed_nxh}] ; Entity NodesOf[ All ] ; }
    }
    Constraint{
      { NameOfCoef an ; EntityType NodesOf ; NameOfConstraint a ; }
    }
  }
}

Formulation {

  { Name a_NR ; Type FemEquation ;
    Quantity {
      { Name a  ; Type Local ; NameOfSpace Hgrad_a ; }
    }
    Equation{
      Galerkin { [ nu[ {d a}] * Dof{d a} , {d a} ]; In Omega; Jacobian Vol; Integration II; }
      Galerkin { JacNL[ dhdb_NL[{d a}] * Dof{d a}, {d a} ]; In Omega_NL; Jacobian Vol; Integration II; }
      Galerkin { [ (-Normal[] /\ h_fixed[]), {a} ]; In Sur_Fixed_nxh; Jacobian Sur; Integration II; }
    }
  }

  { Name a_Dyn_NR ; Type FemEquation ;
    Quantity {
      { Name a  ; Type Local ; NameOfSpace Hgrad_a ; }
    }
    Equation{
      Galerkin { DtDof[ sigma[] * Dof{a} , {a} ]; In Omega_C; Jacobian Vol; Integration II; }
      Galerkin { [ nu[ {d a}] * Dof{d a} , {d a} ]; In Omega; Jacobian Vol; Integration II; }
      Galerkin { JacNL[ dhdb_NL[{d a}] * Dof{d a}, {d a} ]; In Omega_NL; Jacobian Vol; Integration II; }
      Galerkin { [ (-Normal[] /\ h_fixed[]), {a} ]; In Sur_Fixed_nxh; Jacobian Sur; Integration II; }
    }
  }

}

Resolution {
  { Name Magneto_NR ;
    System {
      { Name Macro ; NameOfFormulation a_NR ;}
    }
    Operation {
      InitSolution[Macro];
      IterativeLoop[NbrMaxIter, Eps, 1.]{
        GenerateJac[Macro] ; SolveJac[Macro] ;
      }
      SaveSolution[Macro];
    }
  } 
  { Name MagnetoDynamic_NR ;
    System {
      { Name Macro ; NameOfFormulation a_Dyn_NR ;}
    }
    Operation {
      InitSolution[Macro];
      TimeLoopTheta[t0, tmax, dt, 1.] {
        IterativeLoop[NbrMaxIter, Eps, 1.]{
          GenerateJac[Macro] ; SolveJac[Macro] ;
        }
        SaveSolution[Macro];
      }
    }
  }
}

PostProcessing{
  { Name a_NR ; NameOfFormulation a_NR ;
    PostQuantity {
      { Name az          ; Value { Term { [ CompZ[ {a} ] ] ; In Omega ; Jacobian Vol; } } }
      { Name a           ; Value { Term { [ {a} ] ; In Omega ; Jacobian Vol; } } }
      { Name b           ; Value { Term { [ {d a} ] ; In Omega ; Jacobian Vol;  } } }
      { Name h           ; Value { Term { [ nu[ {d a}] * {d a} ] ; In Omega ; Jacobian Vol ;} } }
    }
  }
  { Name a_Dyn_NR ; NameOfFormulation a_Dyn_NR ;
    PostQuantity {
      { Name az          ; Value { Term { [ CompZ[ {a} ] ] ; In Omega ; Jacobian Vol; } } }
      { Name a           ; Value { Term { [ {a} ] ; In Omega ; Jacobian Vol; } } }
      { Name b           ; Value { Term { [ {d a} ] ; In Omega ; Jacobian Vol;  } } }
      { Name h           ; Value { Term { [ nu[ {d a} ] * {d a} ] ; In Omega ; Jacobian Vol ;} } }
      { Name e           ; Value { Term { [ - Dt[{a}] ] ; In Omega ; Jacobian Vol ; } } }
      { Name j           ; Value { Term { [ -sigma[] * Dt[{a}] ] ; In Omega_C ; Jacobian Vol ; } } }
      { Name jz          ; Value { Term { [ CompZ[-sigma[] * Dt[{a}]] ] ; In Omega_C ; Jacobian Vol ; } } }
      { Name rhoj2       ; Value { Term { [ sigma[]*SquNorm[Dt[{a}]] ]; In Omega_C; Jacobian Vol; } } }
      { Name JouleLosses ; Value { Integral { [ sigma[] * SquNorm[ Dt[{a} ]] ]; In Omega_C; Jacobian Vol; Integration II; } } }
    }
  }
}

PostOperation {

 { Name cuts_field ; NameOfPostProcessing a_NR;
   Operation{
     Print[ a, OnElementsOf Omega, File "res_stat/a.pos" ];
     Print[ b, OnElementsOf Omega, File "res_stat/b.pos" ];
     Print[ h, OnElementsOf Omega, File "res_stat/h.pos" ];
   }
 }
 { Name cuts_field_Dyn ; NameOfPostProcessing a_Dyn_NR;
   Operation{
     Print[ a,  OnElementsOf Omega, File "res_dyn/a.pos" ];
     Print[ az, OnElementsOf Omega, File "res_dyn/az.pos"];
     Print[ b,  OnElementsOf Omega, File "res_dyn/b.pos" ];
     Print[ h,  OnElementsOf Omega, File "res_dyn/h.pos" ];
     Print[ e , OnElementsOf Omega, File "res_dyn/e.pos" ];
     Print[ j , OnElementsOf Omega, File "res_dyn/j.pos" ];
     Print[ jz, OnElementsOf Omega, File "res_dyn/jz.pos"] ;
     Print[ rhoj2, OnElementsOf Omega, File "res_dyn/rhoj2.pos"] ;
   }
 }
}
