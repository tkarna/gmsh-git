Include "macro_lam.dat";

Group {

  Corner        = Region[ { CORNER   } ];
  GammaUp       = Region[ { UP   } ];
  GammaDown     = Region[ { DOWN } ];
  Omega_NL      = Region[ {SURF1} ]; 
  Omega_L       = Region[ {SURF2} ]; 
  Omega_C       = Region[ {SURF1} ]; 
  Omega_CC      = Region[ {SURF2} ]; 
  Omega         = Region[ {Omega_NL, Omega_L} ] ; // the entire computational domain
  Sur_Fixed_nxh = Region[{GammaUp, GammaDown}] ;
  Sur_Fixed_nxe = Region[{GammaUp, GammaDown}] ;
}

Function {
  mu0 = 4.e-7 * Pi ;
  nu0 = 1/mu0; nu[Omega_L] = nu0;

 //Gyselink's paper: Incorporation of a Jiles-Atherton vector hysteresis model in 2D FE magnetic field computations
  Msat = 1145220; aaa = 59.5; kkk = 99.2; ccc = 0.54; alpha = 1.3e-4 ;
  hyst_FeSi = { Msat, aaa, kkk, ccc, alpha};

  Freq = 50 ; T= 1/Freq ;
  nbrT = 1 ;
  nbrstep = 120 ;
  t0 = 0 ;
  tmax = nbrT*T ;
  dt = T/nbrstep ;
  MaxNumIter = 25;
  Tolerance = 1e-3;
  Flag_MagStat = 1;

  Val_I = 100 ;
  h_fixed[] = Vector[Val_I, 0, 0] * F_Sin_wt_p[]{2*Pi*Freq, 0} ;

  // Using h_fixed is equivalent to use a surface current:
  js_surf[GammaUp]   =   Vector[0, 0, -Val_I];
  js_surf[GammaDown] =  Vector[0, 0,  Val_I];
}

//-------------------------------------------------------------------------------------

Jacobian {
  { Name Vol; Case { { Region All ; Jacobian Vol; } } }
  { Name Sur; Case { { Region All ; Jacobian Sur; } } }
}

Integration {
  { Name I1 ; Case { { Type Gauss ; Case {
          { GeoElement Triangle   ; NumberOfPoints  7 ; }
	  { GeoElement Quadrangle ; NumberOfPoints  4 ; }
	  { GeoElement Line       ; NumberOfPoints  13 ; }
        } }
    }
  }
}

//-------------------------------------------------------------------------------------

Constraint {
  { Name MVP_2D ;
    Case {
      If(Flag_MagStat)
      { Region Corner   ; Type Assign; Value  0 ; } // needed only in magnetostatics
      EnfIf
    }
  }
}

FunctionSpace {

  { Name Hcurl_a_2D ; Type Form1P ;
    BasisFunction {
      { Name se1 ; NameOfCoef ae1 ; Function BF_PerpendicularEdge ;
        Support #{Omega, Sur_Fixed_nxh} ; Entity NodesOf [ All] ; }
   }
    Constraint {
      { NameOfCoef ae1 ; EntityType NodesOf  ; NameOfConstraint MVP_2D ; }
    }
  }

  { Name H_vector ; Type Vector;
    BasisFunction {
      { Name sex ; NameOfCoef aex ; Function BF_VolumeX ; Support Omega ; Entity VolumesOf[ Omega ] ; }
      { Name sey ; NameOfCoef aey ; Function BF_VolumeY ; Support Omega ; Entity VolumesOf[ Omega ] ; }
      { Name sez ; NameOfCoef aez ; Function BF_VolumeZ ; Support Omega ; Entity VolumesOf[ Omega ] ; }
    }
  }

}

Formulation {
  { Name MagSta_a_2D_JilesAtherton ; Type FemEquation ;
    Quantity {
      { Name a ; Type Local  ; NameOfSpace Hcurl_a_2D ; }
      { Name h ; Type Local  ; NameOfSpace H_vector ; } // For saving h_Jiles
    }
    Equation {
      Galerkin { [ nu[] * Dof{d a}  , {d a} ]; In Omega_L; Jacobian Vol; Integration I1; }
      Galerkin { [ h_Jiles[ {h}[1], {d a}[1], {d a} ]{List[hyst_FeSi]} , {d a} ]; In Omega_NL; Jacobian Vol; Integration I1; }
      Galerkin { JacNL[ dhdb_Jiles[{h}, {d a}, {h}-{h}[1]]{List[hyst_FeSi]} * Dof{d a} , {d a} ]; In Omega_NL; Jacobian Vol; Integration I1; }
      Galerkin { [ (-Normal[] /\ h_fixed[]), {a} ]; In Sur_Fixed_nxh; Jacobian Sur; Integration I1; }
      Galerkin { [          Dof{h}, {h} ]; In Omega_NL; Jacobian Vol; Integration I1; }
      Galerkin { [ -h_Jiles[{h}[1], {d a}[1], {d a}]{List[hyst_FeSi]} , {h} ]; In Omega_NL; Jacobian Vol; Integration I1; }
    }
  }
}

Resolution {
  { Name MagSta_a_2D_JilesAtherton ;
    System {
      { Name A ; NameOfFormulation MagSta_a_2D_JilesAtherton ; }
    }
    Operation {
      InitSolution[A] ;
      TimeLoopTheta[ t0, tmax, dt, 1. ]{
        IterativeLoop[MaxNumIter, Tolerance, 1.] {
          GenerateJac[A]; SolveJac[A];
        }
        SaveSolution[A] ;
      }
    }
  }
}

PostProcessing {

 { Name MagSta_a_2D_JA ; NameOfFormulation MagSta_a_2D_JilesAtherton ;
   PostQuantity {
     { Name a ; Value { Term { [  {a} ]           ; In Omega ; Jacobian Vol ; } } }
     { Name az ; Value { Term { [  CompZ[{a}] ]   ; In Omega ; Jacobian Vol ; } } }
     { Name b ; Value { Term { [ {d a} ]          ; In Omega ; Jacobian Vol ; } } }
     { Name h  ; Value { Term {  [ {h} ]          ; In Omega ; Jacobian Vol ;} } }
     { Name hx ; Value { Term { [ CompX[{h}] ]    ; In Omega ;  Jacobian Vol ;} } }
     { Name hy ; Value { Term { [ CompY[{h}] ]    ; In Omega ; Jacobian Vol ;} } }
     { Name hb ; Value { Term { [ TensorSym[ CompX[{h}],   CompY[{h}],   CompZ[{h}],
                                             CompX[{d a}], CompY[{d a}], CompZ[{d a}]] ] ; In Omega ; Jacobian Vol ;} } }
   }
 }
}
//-----------------------------------------------------------------------------------------------
L_X = 0.2 ;
L_Y = L_X ;

PostOperation MapMag UsingPost MagSta_a_2D_JA {
  Print[ az, OnElementsOf Omega,              File "res_stat/a.pos", Format Gmsh ] ;
  Print[ b , OnElementsOf Omega,              File "res_stat/b.pos", Format Gmsh ] ;
  Print[ h , OnElementsOf Omega,              File "res_stat/h.pos", Format Gmsh ] ;
  Print[ hb,  OnPoint {0.2/3.0, 0.2/4.0, 0} , File "res_stat/hb.dat", Format TimeTable ] ;
}
