Include "macro_jPerp_lam.dat";

n1_1        = 21;//
n1_2        = 5;
n2          = 30;

i           = 0;

y_bottom[i] = -0.5 *L_X + (i) * e;
y_middle[i] = -0.5 *L_X + (i) * e + lambda * e;
y_top[i]    = -0.5 *L_X + (i+1) * e;
x_left      = - 0.5 * L_X;
x_right     =   0.5 * L_X;
Delta_1     = 0.20/6.0;
Delta_78    = 0.5 * (1 - 10 * Delta_1);

p1 = newp;  Point(p1) = {x_left  , y_bottom[i], 0.0, lc_int};
p2 = newp;  Point(p2) = {x_right , y_bottom[i], 0.0, lc_int};
p3 = newp;  Point(p3) = {x_right , y_middle[i], 0.0, lc_int};
p4 = newp;  Point(p4) = {x_right , y_top[i]   , 0.0, lc_int};
p5 = newp;  Point(p5) = {x_left  , y_top[i]   , 0.0, lc_int};
p6 = newp;  Point(p6) = {x_left  , y_middle[i], 0.0, lc_int};
vec_p1[] +={p1};
vec_p2[] +={p2};
vec_p3[] +={p3};
vec_p4[] +={p4};
vec_p5[] +={p5};
vec_p6[] +={p6};

l1 = newl ; Line(l1)  = {p1,p2} ;
l2 = newl ; Line(l2)  = {p2,p3} ;
l3 = newl ; Line(l3)  = {p3,p6} ;
l4 = newl ; Line(l4)  = {p6,p1} ;
l5 = newl ; Line(l5)  = {p3,p4} ;
l6 = newl ; Line(l6)  = {p4,p5} ;
l7 = newl ; Line(l7)  = {p5,p6} ;
vec_l1[]  +={l1} ;
vec_l2[]  +={l2} ;
vec_l3[]  +={l3} ;
vec_l4[]  +={l4} ;
vec_l5[]  +={l5} ;
vec_l6[]  +={l6} ;
vec_l7[]  +={l7} ;

ll13 = newll; Line Loop(ll13)   = {l1,l2,l3,l4} ;
s14 = news ; Plane Surface(s14) = {ll13} ;
ll15 = newll; Line Loop(ll15)   = {l5,l6,l7,-l3} ;
s16 = news ; Plane Surface(s16) = {ll15} ;
vec_lllam[] += {ll13};
vec_lliso[] += {ll15};

If(!Floor(Delta_78 * n2))
   n2_78 = 1;
EndIf
If(Floor(Delta_78 * n2))
   n2_78 = Floor(Delta_78 * n2);
EndIf

fbumpX = 0.1;
fbumpY = 2.5;

Transfinite Line{l2,l4}  = n1_1+1 Using Bump fbumpX;
Transfinite Line{l5,l7}  = n1_2+1 ;
Transfinite Line{l1}     = n2 + 1 Using Bump fbumpY; //n2+1;
Transfinite Line{l3,l6}  = n2+1;
Transfinite Surface{s14} = {1,2,3,6}; Recombine Surface{s14} ;
Transfinite Surface{s16} = {3,4,5,6}; Recombine Surface{s16} ;

right[] += {l2, l5};
left[]  += {l7, l4};

inno1[] += {s14};
inno2[] += {s16};

// middle
//=======

For i In{1 : nlam - 1}

y_middle[i] = - 0.5 * L_Y + (i) * e + lambda * e;
y_top[i]    = - 0.5 * L_Y + (i+1) * e;
p3 = newp; Point(p3) = {x_right , y_middle[i] , 0.0, lc_int};
p4 = newp; Point(p4) = {x_right , y_top[i]    , 0.0, lc_int};
p5 = newp; Point(p5) = {x_left  , y_top[i]    , 0.0, lc_int};
p6 = newp; Point(p6) = {x_left  , y_middle[i] , 0.0, lc_int};
vec_p3[] +={p3};
vec_p4[] +={p4};
vec_p5[] +={p5};
vec_p6[] +={p6};

l2 = newl ; Line(l2)  = {vec_p4[i-1],p3} ;
l3 = newl ; Line(l3)  = {p3,p6} ;
l4 = newl ; Line(l4)  = {p6,vec_p5[i-1]} ;
l5 = newl ; Line(l5)  = {p3,p4} ;
l6 = newl ; Line(l6)  = {p4,p5} ;
l7 = newl ; Line(l7)  = {p5,p6} ;
vec_l2[]  +={l2} ;
vec_l3[]  +={l3} ;
vec_l4[]  +={l4} ;
vec_l5[]  +={l5} ;
vec_l6[]  +={l6} ;
vec_l7[]  +={l7} ;

ll13 = newll; Line Loop(ll13)   = {(vec_l6[i-1]),-l2,-l3,-l4} ;
s14 = news ; Plane Surface(s14) = {-ll13} ;
ll15 = newll; Line Loop(ll15)   = {l5,l6,l7,-l3} ;
s16 = news ; Plane Surface(s16) = {ll15} ;
vec_lllam[] += {ll13};
vec_lliso[] += {ll15};

Transfinite Line{vec_l2[i],vec_l4[i]} = n1_1+1 Using Bump fbumpX;
Transfinite Line{vec_l5[i],vec_l7[i]} = n1_2+1 ;
Transfinite Line{vec_l3[i],vec_l6[i]} = n2+1 Using Bump fbumpY;
Transfinite Surface{s14,s16}; Recombine Surface{s14,s16} ;

right[] += {l2, l5};
left[]  += {l7, l4};

inno1[] += {s14};
inno2[] += {s16};

EndFor



// Last iron layer
//================
y_13 = - 0.5 * L_Y + ( nlam) * e + lambda * e; 

p12  = newp; Point(p12) = {x_right , y_13 , 0.0, lc_int};
p13  = newp; Point(p13) = {x_left , y_13 , 0.0, lc_int};

l32  = newl ; Line(l32)  = {vec_p4[nlam-1],p12} ;
l33  = newl ; Line(l33)  = {p12,p13} ;
l34  = newl ; Line(l34)  = {p13,vec_p5[nlam-1]} ;

ll38 = newll; Line Loop(ll38) = {-vec_l6[nlam-1],l32,l33,l34} ;
s39  = news ; Plane Surface(s39) = {ll38} ;
vec_lllam[] += {ll38};

Transfinite Line{l32,l34} = n1_1+1 Using Bump fbumpX;
Transfinite Line{l33}     = n2 + 1 Using Bump fbumpY;
Transfinite Surface{s39}  = {vec_p4[nlam-1],p12, p13, vec_p5[nlam-1]}; Recombine Surface{s39} ;

right[] += {l32};
left[]  += {l34};

inno1[] += {s39};


// // Outer circle (C1)
// //==================

p16 = newp; Point(p16) = {c_x,    c_y,    0.0};
p17 = newp; Point(p17) = {r+c_x,  c_y,    0.0, lc_Circ1};
p18 = newp; Point(p18) = {c_x,    r+c_y,  0.0, lc_Circ1};
p19 = newp; Point(p19) = {-r+c_x, c_y,    0.0, lc_Circ1};
p20 = newp; Point(p20) = {c_x,    -r+c_y, 0.0, lc_Circ1};

c1 = newl; Circle(c1) = {p17, p16, p18};
c2 = newl; Circle(c2) = {p18, p16, p19};
c3 = newl; Circle(c3) = {p19, p16, p20};
c4 = newl; Circle(c4) = {p20, p16, p17};

// // Upper inductor
//==================
p25 = newp; Point(p25) = { - 0.5 * dx, 0.5 * L_Y + delta,   0.0, lc_int};
p26 = newp; Point(p26) = {   0.5 * dx, 0.5 * L_Y + delta,   0.0, lc_int};
p27 = newp; Point(p27) = {   0.5 * dx, 0.5 * L_Y + delta + dy, 0.0, lc_int};
p28 = newp; Point(p28) = { - 0.5 * dx, 0.5 * L_Y + delta + dy, 0.0, lc_int};

l37 = newl; Line(l37) = {p25, p26};
l38 = newl; Line(l38) = {p26, p27};
l39 = newl; Line(l39) = {p27, p28};
l40 = newl; Line(l40) = {p28, p25};

ll21 = newll; Line Loop(ll21) = {l37, l38, l39, l40} ;
s22 = news  ; Plane Surface(s22) = {ll21} ;
Transfinite Line{l37,l39}     = nx;
Transfinite Line{l38,l40}     = ny;
Transfinite Surface{s22} = {p25,p26,p27,p28}; Recombine Surface{s22} ;

// // Bottom inductor
//==================
p25 = newp; Point(p25) = {- 0.5 * dx, - 0.5 * L_Y - delta - dy, 0.0, lc_int};
p26 = newp; Point(p26) = {  0.5 * dx, - 0.5 * L_Y - delta - dy, 0.0, lc_int};
p27 = newp; Point(p27) = {  0.5 * dx, - 0.5 * L_Y - delta     , 0.0, lc_int};
p28 = newp; Point(p28) = {- 0.5 * dx, - 0.5 * L_Y - delta     , 0.0, lc_int};

l37 = newl; Line(l37) = {p25, p26};
l38 = newl; Line(l38) = {p26, p27};
l39 = newl; Line(l39) = {p27, p28};
l40 = newl; Line(l40) = {p28, p25};

ll23 = newll; Line Loop(ll23) = {l37, l38, l39, l40} ;
s24 = news  ; Plane Surface(s24) = {ll23} ;

Transfinite Line{l37,l39}     = nx;
Transfinite Line{l38,l40}     = ny;
Transfinite Surface{s24} = {p25,p26,p27,p28}; Recombine Surface{s24} ;

ll17 = newll; Line Loop(ll17) = {c1, c2, c3, c4} ;
s19  = news ; Plane Surface(s19) = {ll17, ll21, ll23, vec_lllam[], vec_lliso[]} ;


//Physical
//========

Physical Point(CORNER) = {p1};

Physical Line(GAMMA_INF) = {c1, c2, c3, c4};

Physical Surface(OMEGA_S1) = {s22};
Physical Surface(OMEGA_S2) = {s24};
Physical Surface(INSU)  = {vec_lliso[]} ;
Physical Surface(AIR)  = {s19} ;
For k In {0 : nlam}
  Physical Surface(IRON+k) = {vec_lllam[ {k} ] }  ;
EndFor
