Include "macro_jPerp_lam_half_2Indu.dat" ;
Include "physical.dat" ;
Mesh.Algorithm = 2; // 2D mesh algorithm (1=MeshAdapt, 2=Automatic, 5=Delaunay, 6=Frontal, 7=bamg, 8=delquad)
Geometry.CopyMeshingMethod = 1; // Copy meshing method when duplicating geometrical entities?

Flag_Transfinite = 1 ;

lclam = L_X/80 ;
lcair  = R1/10 ; // inner circle
lcair2 = R2/10 ; // outer circle

// number of divisions for transfinite mesh of the laminations
nwl = 31 ;//number of divisions along thickness of the lamination (with possible Bump)
nll  = Ceil[Lsrc/lclam] + 1 ;
nllm = Ceil[(L_X-2*Lsrc)/lclam] + 1 ;
nwe = 5 ;

fbump = 0.3 ;
fprog = 0.9 ;

//============================================================
// Lamination stack center at (0,0)
// First lamination with corner at (-L_X/2, L_Y/2, 0)
//============================================================

x0 = -L_X/2 ;
y0 = (e-d)/2 ;
z0 = 0 ;

pp[] += newp; Point(pp[0]) = { x0,      y0,   z0, lclam};
pp[] += newp; Point(pp[1]) = { x0+Lsrc, y0,   z0, lclam};
pp[] += newp; Point(pp[2]) = {-x0-Lsrc, y0,   z0, lclam};
pp[] += newp; Point(pp[3]) = {-x0,      y0,   z0, lclam};

pp[] += newp; Point(pp[4]) = {-x0,      y0+d, z0, lclam};
pp[] += newp; Point(pp[5]) = {-x0-Lsrc, y0+d, z0, lclam};
pp[] += newp; Point(pp[6]) = { x0+Lsrc, y0+d, z0, lclam};
pp[] += newp; Point(pp[7]) = { x0,      y0+d, z0, lclam};

ppe[] += newp; Point(newp) = {-x0,      y0+e, z0, lclam};
ppe[] += newp; Point(newp) = {-x0-Lsrc, y0+e, z0, lclam};
ppe[] += newp; Point(newp) = { x0+Lsrc, y0+e, z0, lclam};
ppe[] += newp; Point(newp) = { x0,      y0+e, z0, lclam};

For k In {0:7}
  llam[] += newl ; Line(newl) = {pp[k],pp[((k==7)?0:k+1)]};
EndFor
llam[] += newl ; Line(newl) = {pp[1],pp[6]}; // vertical lines in the middle
llam[] += newl ; Line(newl) = {pp[2],pp[5]};

lllam[] += newll ; Line Loop(newll) = {llam[0],llam[8],llam[6],llam[7]};
lllam[] += newll ; Line Loop(newll) = {llam[1],llam[9],llam[5],-llam[8]};
lllam[] += newll ; Line Loop(newll) = {llam[2],llam[3],llam[4],-llam[9]};

slam[]+=news ; Plane Surface(news) = lllam[0]; // Left
slam[]+=news ; Plane Surface(news) = lllam[1]; // Middle
slam[]+=news ; Plane Surface(news) = lllam[2]; // Right

If(Flag_Transfinite)
Transfinite Line{llam[{3,7,8,9}]} = nwl Using Bump fbump ; // divisions along thickness of the laminations
Transfinite Line{llam[{0}], -llam[{6}]} = nll Using Bump fbump ; // divisions left
Transfinite Line{llam[{1,5}]} = nllm Using Bump fbump ; // divisions middle
Transfinite Line{-llam[{2}], llam[{4}]} = nll Using Bump fbump ; // divisions right

Transfinite Surface{slam[]}; Recombine Surface{slam[]};
EndIf

For k In {0:2}
  lli[] += newl ; Line(newl) = {ppe[k],ppe[k+1]};
EndFor
lli[] += newl ; Line(newl) = {pp[7],ppe[3]}; // vertical lines isolation
lli[] += newl ; Line(newl) = {pp[6],ppe[2]};
lli[] += newl ; Line(newl) = {pp[5],ppe[1]};
lli[] += newl ; Line(newl) = {pp[4],ppe[0]};

llli[] += newll ; Line Loop(newll) = {-llam[6], lli[4], lli[2],-lli[3]};
llli[] += newll ; Line Loop(newll) = {-llam[5], lli[5], lli[1],-lli[4]};
llli[] += newll ; Line Loop(newll) = {-llam[4], lli[6], lli[0],-lli[5]};

siso[] +=news ; Plane Surface(news) = llli[0]; // Left
siso[] +=news ; Plane Surface(news) = llli[1]; // Middle
siso[] +=news ; Plane Surface(news) = llli[2]; // Right

If(Flag_Transfinite)
Transfinite Line{lli[{3:6}]} = nwe Using Bump 1 ; // divisions along thickness of isolation
Transfinite Line{-lli[{2}]} = nll Using Bump fbump; // divisions left
Transfinite Line{ lli[{1}]} = nllm Using Bump fbump ; // divisions middle
Transfinite Line{ lli[{0}]} = nll Using Bump fbump ; // divisions right
Transfinite Surface{siso[]}; Recombine Surface{siso[]};
EndIf

// Generating the rest of the laminations and isolation...
For k In {1:nlam/2-1}
  slam[] += Translate {0, k*e,0} { Duplicata { Surface{slam[{0:2}]}; } };
EndFor
For k In {1:nlam/2-2}
  siso[] += Translate {0, k*e,0} { Duplicata { Surface{siso[{0:2}]}; } };
EndFor

ltoplam[]  = Boundary{Surface{slam[#slam[]-2]};};
ltoplaml[] = Boundary{Surface{slam[#slam[]-1]};};
ltoplamr[] = Boundary{Surface{slam[#slam[]-3]};};

//For k In {0:#ltoplam[]-1}
//Printf("%g %g",k,ltoplam[k]); EndFor

//===============================
// Half of first isolation layer
//===============================
pa[] += newp; Point(pa[0]) = { x0,      0,   z0, lclam};
pa[] += newp; Point(pa[1]) = { x0+Lsrc, 0,   z0, lclam};
pa[] += newp; Point(pa[2]) = {-x0-Lsrc, 0,   z0, lclam};
pa[] += newp; Point(pa[3]) = {-x0,      0,   z0, lclam};

For k In {0:2}
  lla[] += newl ; Line(newl) = {pa[k],pa[k+1]};
EndFor
For k In {0:3}
  lla_[] += newl ; Line(newl) = {pa[k],pp[k]};
EndFor

llla[] += newll ; Line Loop(newll) = {lla[0], lla_[1], -llam[0],-lla_[0]};
llla[] += newll ; Line Loop(newll) = {lla[1], lla_[2], -llam[1],-lla_[1]};
llla[] += newll ; Line Loop(newll) = {lla[2], lla_[3], -llam[2],-lla_[2]};
siso[] +=news ; Plane Surface(news) = llla[0]; // Left
siso[] +=news ; Plane Surface(news) = llla[1]; // Middle
siso[] +=news ; Plane Surface(news) = llla[2]; // Right


Transfinite Line{lla_[]} = (nwe/2+1) Using Bump 1 ; // divisions along thickness of isolation
Transfinite Line{lla[{0}]} = nll Using Bump fbump ; // divisions left
Transfinite Line{lla[{1}]} = nllm Using Bump fbump ; // divisions middle
Transfinite Line{-lla[{2}]} = nll Using Bump fbump ; // divisions right

Transfinite Surface{siso[{#siso[]-3:#siso[]-1}]}; Recombine Surface{siso[{#siso[]-3:#siso[]-1}]};


// Adding an inductor and air around
// airgap between inductor and laminations...
pgap[] += newp; Point(newp) = { x0+Lsrc, L_Y/2+dist_ind_lam, z0, lclam};
pgap[] += newp; Point(newp) = {-x0-Lsrc, L_Y/2+dist_ind_lam, z0, lclam};


//up
pp_[]+=Boundary{Line{ltoplam[2]};};
lgap_[] += newl ; Line(newl) = {pp_[0],pgap[1]};
lgap_[] += newl ; Line(newl) = {pgap[1] ,pgap[0]};
lgap_[] += newl ; Line(newl) = {pgap[0], pp_[1]};

llgap[] += newll ;  Line Loop(newll) = {lgap_[], -ltoplam[2]};
sgap[]+=news; Plane Surface(news) = llgap[0];

If(0)
Transfinite Line{lgap_[{0,2}]} = nwe Using Bump 1 ; // divisions along thickness of isolation
Transfinite Line{lgap_[{1}]}   = nllm Using Bump fbump ; // divisions middle
Transfinite Surface{sgap[]}; Recombine Surface{sgap[]};
EndIf
// inductor
lcind = wind/4 ;
pi[] += newp; Point(newp) = { x0+Lsrc, L_Y/2+dist_ind_lam+wind, z0, lcind};
pi[] += newp; Point(newp) = {-x0-Lsrc, L_Y/2+dist_ind_lam+wind, z0, lcind};

//up
pp_[]+=Boundary{Line{ltoplam[2]};};
lind_[] += newl ; Line(newl) = {pgap[1],pi[1]};
lind_[] += newl ; Line(newl) = {pi[1] ,pi[0]};
lind_[] += newl ; Line(newl) = {pi[0], pgap[0]};

llind[] += newll ;  Line Loop(newll) = {lind_[],-lgap_[1]};
sind[]+=news; Plane Surface(news) = llind[0];

Transfinite Line{lind_[{0}], lind_[{2}]} = 20;
Transfinite Line{lind_[{1}], -lgap_[1]} = 15;
Transfinite Surface{sind[]} = {pi[{0}],pi[{1}],pgap[{1}],pgap[{0}]}; Recombine Surface{sind[]};





// Air around everything
cen = newp ; Point(cen) = { 0,  0, z0, lcair};

pair[] += newp; Point(newp) = { R1,  0, z0, lcair};
pair[] += newp; Point(newp) = { 0,  R1, z0, lcair};
pair[] += newp; Point(newp) = {-R1, 0, z0, lcair};

For k In {0:1}
 lair[] += newl ; Circle(newl) = {pair[k],cen,pair[k+1]};
EndFor

lair[] += newl ; Line(newl) = {pa[3],pair[0]};
lair[] += newl ; Line(newl) = {pair[2],pa[0]};



allSurfaces[] = Surface '*';
bndlines[]=CombinedBoundary{Surface{allSurfaces[]};};
NN = #bndlines[] ;

// Defining the two lateral inductors
//===================================

// Right inductor
//=============== 
p1 = newp; Point(p1) = {0.5 * L_X + hgap          , vgap         , 0.0, lcind};
p2 = newp; Point(p2) = {0.5 * L_X + hgap+thickness, vgap         , 0.0, lcind};
p3 = newp; Point(p3) = {0.5 * L_X + hgap+thickness, vgap + height, 0.0, lcind};
p4 = newp; Point(p4) = {0.5 * L_X + hgap          , vgap + height, 0.0, lcind};

l1 = newl; Line(l1) = {p1, p2}; 
l2 = newl; Line(l2) = {p2, p3}; 
l3 = newl; Line(l3) = {p3, p4}; 
l4 = newl; Line(l4) = {p4, p1}; 

ll1 = newll; Line Loop(ll1) = {l1, l2, l3, l4};
s1 = news; Plane Surface(s1) = ll1;
Transfinite Line{l1, l3} = 20;
Transfinite Line{l2, l4} = 16;
Transfinite Surface{s1} = {p1, p2, p3, p4}; Recombine Surface{s1};

// Left inductor
//============== 
p5 = newp; Point(p5) = {-(0.5 * L_X + hgap+thickness), vgap         , 0.0, lcind};
p6 = newp; Point(p6) = {-(0.5 * L_X + hgap)          , vgap         , 0.0, lcind};
p7 = newp; Point(p7) = {-(0.5 * L_X + hgap)          , vgap + height, 0.0, lcind};
p8 = newp; Point(p8) = {-(0.5 * L_X + hgap+thickness), vgap + height, 0.0, lcind};

l5 = newl; Line(l5) = {p5, p6}; 
l6 = newl; Line(l6) = {p6, p7}; 
l7 = newl; Line(l7) = {p7, p8}; 
l8 = newl; Line(l8) = {p8, p5}; 

ll2 = newll; Line Loop(ll2) = {l5, l6, l7, l8};
s2 = news; Plane Surface(s2) = ll2;

Transfinite Line{l5, l7} = 20;
Transfinite Line{l6, l8} = 16;
Transfinite Surface{s2} = {p5, p6, p7, p8}; Recombine Surface{s2};

llair=newll; Line Loop(newll) = {lair[{2,0,1,3}], -bndlines[{0:NN-11,NN-7:NN-1}], -l1, -l2, -l3, -l4, -l5, -l6, -l7, -l8} ;
sair = news ; Plane Surface(sair) = llair ;


// air infinity
pair_[] += newp; Point(pair_[0]) = { R2,  0, z0, lcair2};
pair_[] += newp; Point(pair_[1]) = { 0,  R2, z0, lcair2};
pair_[] += newp; Point(pair_[2]) = {-R2, 0, z0, lcair2};

For k In {0:1}
 lair2[] += newl ; Circle(newl) = {pair_[k],cen,pair_[k+1]};
EndFor

lair2[] += newl ; Line(newl) = {pair[0],pair_[0]};
lair2[] += newl ; Line(newl) = {pair[2],pair_[2]};


llair2=newll; Line Loop(newll) = {lair2[{0,1}], -lair2[{3}], -lair[{1,0}], lair2[{2}]};
sair2 = news ; Plane Surface(sair2) = {llair2};


//================================================================================
// Physical regions
//================================================================================

Physical Point(CORNER)       = {pp[{1}]};

Physical Line(GAMMA_INF)     = {lair2[ {0, 1} ] };
Physical Line(SYMMETRY)      = {lla[], lair[ {2, 3} ], lair2[ {2, 3} ] };

For k In {0:#slam[]-1:3}
  Physical Surface(IRON+k/3) = {slam[{k:k+2}]} ;
EndFor
Physical Surface(INSU)       = {siso[]} ;
Physical Surface(AIR)        = {sair, sgap[], sair2} ;
Physical Surface(OMEGA_S1)   = {sind[]};
Physical Surface(OMEGA_R)   = {s1};
Physical Surface(OMEGA_L)   = {s2};




Point(1332) = {-0.000275, 0.0, 0, 1.0};
Point(1333) = {-0.000275, 0.008225, 0, 1.0};
Line(371) = {1333, 1332};
Color Red {Line{371};}
