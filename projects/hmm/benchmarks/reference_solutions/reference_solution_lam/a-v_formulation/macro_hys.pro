Include "macro.dat";

Group {

  Corner        = Region[ { CORNER   } ];
  GammaUp       = Region[ { UP   } ];
  GammaDown     = Region[ { DOWN } ];
  Omega_NL      = Region[ {SURF} ]; 
  Omega_L       = Region[ {} ]; 
  Omega_C       = Region[ {SURF} ]; 
  Omega_CC      = Region[ {} ]; 
  Omega         = Region[ {Omega_NL, Omega_L} ] ; // the entire computational domain
  Sur_Fixed_nxh = Region[{GammaUp, GammaDown}] ;
  Sur_Fixed_nxe = Region[{GammaUp, GammaDown}] ;
}

Function {
  mu0 = 4.e-7 * Pi ;
  nu0 = 1/mu0 ;

  sigma[Omega_C] = 6e2 ; //  6e7
  nu[Omega_L] = nu0 ; //  6e7

 //Gyselink's paper: Incorporation of a Jiles-Atherton vector hysteresis model in 2D FE magnetic field computations
  Msat = 1145220; aaa = 59.5; kkk = 99.2; ccc = 0.54; alpha = 1.3e-4 ;
  hyst_FeSi = { Msat, aaa, kkk, ccc, alpha};

  Freq = 50 ; T= 1/Freq ;
  nbrT = 2 ;
  nbrstep = 20 ;
  t0 = 0 ;
  tmax = nbrT*T ;
  dt = T/nbrstep ;

  Val_I = 100 ;
  //h_fixed[] = Vector[Val_I, 0, 0] * F_Cos_wt_p[]{2*Pi*Freq, 0}  ;
  h_fixed[] = Vector[Val_I, 0, 0] * F_Sin_wt_p[]{2*Pi*Freq, 0} ;

  // Using h_fixed is equivalent to use a surface current:
  js_surf[GammaUp]   = Vector[0, 0, -Val_I];
  js_surf[GammaDown] = Vector[0, 0, Val_I ];

  a_tprevious[] = Vector[ 0., 0., ScalarField[XYZ[]]{0}];
  h_tprevious[] = VectorField[XYZ[]]{1};
}

//-------------------------------------------------------------------------------------

Jacobian {
  { Name Vol; Case { { Region All ; Jacobian Vol; } } }
  { Name Sur; Case { { Region All ; Jacobian Sur; } } }
}

Integration {
  { Name I1 ; Case { { Type Gauss ; Case {
          { GeoElement Triangle   ; NumberOfPoints  7 ; }
	  { GeoElement Quadrangle ; NumberOfPoints  4 ; }
	  { GeoElement Line       ; NumberOfPoints  13 ; }
        } }
    }
  }
}

//-------------------------------------------------------------------------------------

Constraint {
  { Name MVP_2D ;
    Case {
      { Region Corner   ; Type Assign; Value  0 ; } // needed only in magnetostatics
    }
  }
}

//-----------------------------------------------------------------------------------------------

FunctionSpace {

  { Name Hcurl_a_2D ; Type Form1P ;
    BasisFunction {
      { Name se1 ; NameOfCoef ae1 ; Function BF_PerpendicularEdge ;
        Support #{Omega, Sur_Fixed_nxh} ; Entity NodesOf [ All] ; }
   }
    Constraint {
      { NameOfCoef ae1 ; EntityType NodesOf  ; NameOfConstraint MVP_2D ; }
    }
  }

  { Name H_vector ; Type Vector;
    BasisFunction {
      { Name sex ; NameOfCoef aex ; Function BF_VolumeX ; Support Omega ; Entity VolumesOf[ Omega ] ; }
      { Name sey ; NameOfCoef aey ; Function BF_VolumeY ; Support Omega ; Entity VolumesOf[ Omega ] ; }
      { Name sez ; NameOfCoef aez ; Function BF_VolumeZ ; Support Omega ; Entity VolumesOf[ Omega ] ; }
    }
  }

  { Name Hcurl_a_2D_Proj ; Type Form1P ;
    BasisFunction {
      { Name se1 ; NameOfCoef ae1 ; Function BF_PerpendicularEdge ;
        Support #{Omega} ; Entity NodesOf [ All] ; }
   }
  }

  { Name H_vector_Proj ; Type Vector;
    BasisFunction {
      { Name sex ; NameOfCoef aex ; Function BF_VolumeX ; Support Omega ; Entity VolumesOf[ Omega ] ; }
      { Name sey ; NameOfCoef aey ; Function BF_VolumeY ; Support Omega ; Entity VolumesOf[ Omega ] ; }
      { Name sez ; NameOfCoef aez ; Function BF_VolumeZ ; Support Omega ; Entity VolumesOf[ Omega ] ; }
    }
  }

}

//-----------------------------------------------------------------------------------------------

Formulation {

  { Name MagSta_a_2D_JilesAtherton ; Type FemEquation ;
    Quantity {
      { Name a ; Type Local  ; NameOfSpace Hcurl_a_2D ; }
      { Name h ; Type Local  ; NameOfSpace H_vector ; } // For saving h_Jiles
    }
    Equation {
      Galerkin { [ nu[] * Dof{d a}  , {d a} ]; In Omega_L ; Jacobian Vol ; Integration I1 ; }
      Galerkin { [ h_Jiles[ {h}[1], {d a}[1], {d a} ]{List[hyst_FeSi]} , {d a} ];
        In Omega_NL ; Jacobian Vol ; Integration I1 ; }
      Galerkin { JacNL[ dhdb_Jiles[{h}, {d a}, {h}-{h}[1]]{List[hyst_FeSi]} * Dof{d a} , {d a} ];
        In Omega_NL ; Jacobian Vol ; Integration I1 ; }
      Galerkin { [ (-Normal[] /\ h_fixed[]), {a} ]; In Sur_Fixed_nxh; Jacobian Sur; Integration I1; }

      Galerkin { [          Dof{h}, {h} ]; In Omega_NL ; Jacobian Vol ; Integration I1 ; }
      Galerkin { [ -h_Jiles[{h}[1], {d a}[1], {d a}]{List[hyst_FeSi]} , {h} ]  ;
        In Omega_NL ; Jacobian Vol ; Integration I1 ; }
    }
  }

  { Name Init_tprevious ; Type FemEquation ;
    Quantity
      {
        { Name h_tp ; Type Local  ; NameOfSpace H_vector_Proj ; } // For saving h_Jiles
        { Name a_tp ; Type Local  ; NameOfSpace Hcurl_a_2D_Proj ; } // For saving h_Jiles
      }
    Equation{
        Galerkin { [ Dof{a_tp} , {a_tp} ] ;
          In Omega ; Jacobian Vol ; Integration I1 ; }
        Galerkin { [ - a_tprevious[], {a_tp} ] ;
          In Omega ; Jacobian Vol ; Integration I1 ; }
        Galerkin { [ Dof{h_tp} , {h_tp} ] ;
          In Omega ; Jacobian Vol ; Integration I1 ; }
        Galerkin { [ - h_tprevious[], {h_tp} ] ;
          In Omega ; Jacobian Vol ; Integration I1 ; }
      }
  }

  { Name MagSta_a_2D_JilesAtherton_Step_By_Step ; Type FemEquation ;
    Quantity {
      { Name a ; Type Local  ; NameOfSpace Hcurl_a_2D ; }
      { Name h ; Type Local  ; NameOfSpace H_vector ; } // For saving h_Jiles
      { Name h_tp ; Type Local  ; NameOfSpace H_vector_Proj ; } // For saving h_Jiles
      { Name a_tp ; Type Local  ; NameOfSpace Hcurl_a_2D_Proj ; } // For saving h_Jiles
    }
    Equation {
      Galerkin { [ nu[] * Dof{d a}  , {d a} ] ;
        In Omega_L ; Jacobian Vol ; Integration I1 ; }

      Galerkin { [ h_Jiles[ {h_tp}, {d a_tp}, {d a} ]{List[hyst_FeSi]} , {d a} ];
        In Omega_NL ; Jacobian Vol ; Integration I1 ; }
      Galerkin { JacNL[ dhdb_Jiles[{h}, {d a}, {h}-{h_tp}]{List[hyst_FeSi]} * Dof{d a} , {d a} ];
        In Omega_NL ; Jacobian Vol ; Integration I1 ; }

      Galerkin { [ (-Normal[] /\ h_fixed[]), {a} ];
        In Sur_Fixed_nxh; Jacobian Sur; Integration I1; }


      Galerkin { [          Dof{h}, {h} ] ;
        In Omega_NL ; Jacobian Vol ; Integration I1 ; }
      Galerkin { [ -h_Jiles[{h_tp}, {d a_tp}, {d a}]{List[hyst_FeSi]} , {h} ]  ;
        In Omega_NL ; Jacobian Vol ; Integration I1 ; }


    }
  }

}

//-----------------------------------------------------------------------------------------------

Resolution {
  { Name MagSta_a_2D_JilesAtherton ;
    System {
      { Name A ; NameOfFormulation MagSta_a_2D_JilesAtherton ; }
    }
    Operation {
      InitSolution[A] ;
      TimeLoopTheta[ t0, tmax, dt, 1. ]{
        IterativeLoop[50, 1e-4, 1.] {
          GenerateJac[A]; SolveJac[A];
        }
        SaveSolution[A] ;
      }
    }
  }
  { Name MagSta_a_2D_JilesAtherton_Step_By_Step ;
    System {
      { Name P ; NameOfFormulation Init_tprevious         ;}
      { Name A ; NameOfFormulation MagSta_a_2D_JilesAtherton_Step_By_Step ; }
    }
    Operation {
      GmshRead["a0.pos"];
      GmshRead["h0.pos"];
      Generate [P] ; Solve [P] ; SaveSolution [P] ;

      SetTime[t0+dt];
      //InitSolution[A] ;
      //TimeLoopTheta[ t0, t0+dt, dt, 1. ]{
        IterativeLoop[50, 1e-4, 1.] {
          GenerateJac[A]; SolveJac[A];
        }
        SaveSolution[A] ;
        //}
    }
  }
}


//-----------------------------------------------------------------------------------------------

PostProcessing {

 { Name MagSta_a_2D_JA ; NameOfFormulation MagSta_a_2D_JilesAtherton ;
   PostQuantity {
     { Name a ; Value { Term { [  {a} ]   ; In Omega ; Jacobian Vol ; } } }
     { Name az ; Value { Term { [  CompZ[{a}] ]   ; In Omega ; Jacobian Vol ; } } }
     { Name b ; Value { Term { [ {d a} ] ; In Omega ; Jacobian Vol ; } } }

     { Name h  ; Value { Term {  [ {h} ]   ; In Omega ; } } }
     { Name hx ; Value { Term { [ CompX[{h}] ] ; In Omega ; } } }
     { Name hy ; Value { Term { [ CompY[{h}] ] ; In Omega ; } } }
     { Name hb ; Value { Term { [ TensorSym[ CompX[{h}],   CompY[{h}],   CompZ[{h}],
                                             CompX[{d a}], CompY[{d a}], CompZ[{d a}]] ] ; In Omega ; } } }
   }

 }

 { Name Projection ; NameOfFormulation Init_tprevious ;
   PostQuantity {
     { Name a_tp ; Value { Term { [  {a_tp} ]   ; In Omega ; Jacobian Vol ; } } }
     { Name h_tp ; Value { Term { [  {h_tp} ]   ; In Omega ; Jacobian Vol ; } } }
   }

 }

}
//-----------------------------------------------------------------------------------------------
Lx = 0.2 ;
Ly = Lx  ;

//currentStep = 1;


PostOperation CheckInit UsingPost Projection {
  Print[ a_tp, OnElementsOf Omega, File Sprintf("a_tp.pos"), Format Gmsh ] ;
  Print[ h_tp, OnElementsOf Omega, File Sprintf("h_tp.pos"), Format Gmsh ] ;
}

PostOperation MapMag UsingPost MagSta_a_2D_JA {
//   Print[ az, OnElementsOf Omega, File Sprintf("a%g.pos", currentStep), Format Gmsh, LastTimeStepOnly ] ;
//   Print[ b, OnElementsOf Omega, File Sprintf("b%g.pos", currentStep), Format Gmsh, LastTimeStepOnly ] ;
//   Print[ h,  OnElementsOf Omega, File Sprintf("h%g.pos", currentStep), Format Gmsh, LastTimeStepOnly ] ;
//   Print[ hb,  OnPoint {0,Ly/2-Ly/4,0} , File "hb.dat", Format TimeTable ] ;

  Print[ az, OnElementsOf Omega, File Sprintf("a.pos"), Format Gmsh] ;
  Print[ b , OnElementsOf Omega, File Sprintf("b.pos"), Format Gmsh] ;
  Print[ h , OnElementsOf Omega, File Sprintf("h.pos"), Format Gmsh] ;
  Print[ hb, OnPoint {0,Ly/2-Ly/4,0} , File "hb.dat", Format TimeTable ] ;}


