//
// C++ Interface: solverAlgorithms for hmm
//
// Description: Collection of functions that are used to assemble the matrix and the right handside for classical and multiscale problems.
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef _HMM_SOLVERALGORITHMS_H_
#define _HMM_SOLVERALGORITHMS_H_

#include <iostream>
#include <complex>
#include "dofManager.h"
#include "NonLinearDofManager.h"
#include "hmmFunction.h"
#include "hmmFormulation.h"
#include "functionSpace.h"
#include "quadratureRules.h"
#include "MVertex.h"

template<class Iterator, class Assembler> void hmmAssembleMatrixAndRHS_Lin( hmmBilinearTermBase &term, FunctionSpaceBase &space, 
                                                                            Iterator itbegin, Iterator itend, Assembler &assembler) {
  fullMatrix<typename Assembler::dataMat> localMatrix;
  fullVector<typename Assembler::dataVec> localVector;
  std::vector<Dof> R;
  for (Iterator it = itbegin; it != itend; ++it){
    MElement *e = *it;
    R.clear();
    IntPt *GP;
    int npts = term.getQuadrature()->getIntPoints(e, &GP);
    term.get(e, npts, GP, localMatrix, localVector); 
    space.getKeys(e, R);
    assembler.assemble(R, localMatrix);
  }
}
template<class Iterator, class Assembler> int hmmAssembleMatrixAndRHS_NLin( hmmBilinearTermBase &term, FunctionSpaceBase &space, 
                                                                            Iterator itbegin, Iterator itend, Assembler &assembler) {
  fullMatrix<typename Assembler::dataMat> localMatrix;
  fullVector<typename Assembler::dataVec> localVector;
  std::vector<Dof> R;
  for (Iterator it = itbegin; it != itend; it++){
    MElement *e = *it;
    R.clear();
    IntPt *GP;
    int npts = term.getQuadrature()->getIntPoints(e, &GP);
    term.get(e, npts, GP, localMatrix, localVector); 
    space.getKeys(e, R);
    //assembler.assemble(R, localMatrix);
    assembler.assembleNR(R, localMatrix);
    if (term.getMaterialLaw()->getType() == HMM_NONLINEAR_MLAW) {
      localVector.scale(-1.0);
      assembler.assembleIntForces(R, localVector);
      localMatrix.print("print matrix elec.");
      localVector.print("print vector elec.");
    }
    else {
      localMatrix.scale(-1.0);
      assembler.assembleIntForces(R, localMatrix);
      localMatrix.print("print matrix elec.");
    }
  }
  return 1;
}

/*
template<class Iterator, class Assembler> int hmmAssembleMatrixAndRHS_H_Form( hmmBilinearTermBase &term, FunctionSpaceBase &space, 
                                                                            Iterator itbegin, Iterator itend, Assembler &assembler) {
  fullMatrix<typename Assembler::dataMat> localMatrix;
  fullVector<typename Assembler::dataVec> localVector;
  std::vector<Dof> R;
  for (Iterator it = itbegin; it != itend; it++){
    MElement *e = *it;
    R.clear();
    IntPt *GP;
    int npts = term.getQuadrature()->getIntPoints(e, &GP);
    term.get(e, npts, GP, localMatrix, localVector); 
    space.getKeys(e, R);
    assembler.assembleNR(R, localMatrix);
    if (term.getMaterialLaw()->getType() == HMM_NONLINEAR_MLAW) {
      localVector.scale(-1.0);
      assembler.assembleIntForces(R, localVector);
    }
    //    else {
    //      localMatrix.scale(-1.0);
    //      assembler.assembleIntForces(R, localMatrix);
    //    }
  }
  return 1;
}
*/

template<class Iterator, class Assembler> int hmmAssembleLinearTerm( hmmLinearTermBase &term, FunctionSpaceBase &space, Iterator itbegin, 
                                                                     Iterator itend, Assembler &assembler, double freq , double currentTime) {
  fullVector<typename Assembler::dataVec> localVector;
  std::vector<Dof> R;
  for (Iterator it = itbegin; it != itend; it++){
    MElement *e = *it;
    R.clear();
    IntPt *GP;
    int npts = term.getQuadrature()->getIntPoints(e, &GP);
    //Msg::Info("Number of integ points is %d", npts);
    term.get(e, npts, GP, localVector, freq, currentTime); 
    space.getKeys(e, R);
    localVector.scale(-1.0);
    assembler.assembleIntForces(R, localVector);
  }
  return 1;
}

template<class Iterator, class Assembler> int hmmAssembleMatrixAndRHS_Dt( hmmBilinearTermBase &term, FunctionSpaceBase &space, Iterator itbegin, 
                                                                          Iterator itend, Assembler &assembler, double invDTime) {
  fullMatrix<typename Assembler::dataMat> localMatrix;
  fullVector<typename Assembler::dataVec> localVector;
  //======================================================
  fullVector<typename Assembler::dataVec> localVector_Old;
  //======================================================
  std::vector<Dof> R;
  for (Iterator it = itbegin; it != itend; it++) {
    MElement *e = *it;
    R.clear();
    IntPt *GP;
    int npts = term.getQuadrature()->getIntPoints(e, &GP);
    if(invDTime == 0.0)
      Msg::Error("dTime = 0!");
    else {
      if (term.getMaterialLaw()->getType() == HMM_NONLINEAR_MLAW) {
        term.get(e, npts, GP, localMatrix, localVector, localVector_Old);
        localMatrix.scale(invDTime);
        localVector.scale(invDTime);
        localVector_Old.scale(invDTime);
        //localMatrix.print();
        //localVector.print();
        //localVector_Old.print();
      }
      else {
        term.get(e, npts, GP, localMatrix, localVector);
        localMatrix.scale(invDTime);
        localVector.scale(invDTime);
      }
      space.getKeys(e, R);
      //assembler.assemble(R, localMatrix);
      assembler.assembleNR(R, localMatrix);
      
      // If constitutive law is of type "HMM_NL" and the nonlinear if in the time derivative term 
      //=========================================================================================
      if (term.getMaterialLaw()->getType() == HMM_NONLINEAR_MLAW) {
        localVector.scale(-1.0);
        assembler.assembleIntForces(R, localVector);
        localVector_Old.scale(-1.0);
        assembler.assembleIntForces(R, localVector_Old);
        //localMatrix.scale(-1.0);
        //assembler.assembleIntForces(R, localMatrix);
        localMatrix.print("Print matrix mag hmm_non_linear.");
        localVector.print("Print vector mag hmm_non_linear.");
      }
      else {
        localMatrix.scale(-1.0);
        assembler.assembleIntForcesTime(R, localMatrix);
        localMatrix.print("Print matrix mag linear.");
      }
    }
  }
  return 1;
}

template<class Iterator, class Assembler> int hmmAssembleMatrixAndRHS_Dt_H_Form( hmmBilinearTermBase &term, FunctionSpaceBase &space, Iterator itbegin, 
                                                                          Iterator itend, Assembler &assembler, double invDTime) {
  fullMatrix<typename Assembler::dataMat> localMatrix;
  fullVector<typename Assembler::dataVec> localVector;
  //======================================================
  fullVector<typename Assembler::dataVec> localVector_Old;
  //======================================================
  std::vector<Dof> R;
  for (Iterator it = itbegin; it != itend; it++) {
    MElement *e = *it;
    R.clear();
    IntPt *GP;
    int npts = term.getQuadrature()->getIntPoints(e, &GP);
    if(invDTime == 0.0)
      Msg::Error("dTime = 0!");
    else {
      if (term.getMaterialLaw()->getType() == HMM_NONLINEAR_MLAW) {
        term.get(e, npts, GP, localMatrix, localVector, localVector_Old);
        localMatrix.scale(invDTime);
        localVector.scale(invDTime);
        localVector_Old.scale(invDTime);
        //localMatrix.print();
        //localVector.print();
        //localVector_Old.print();
      }
      else {
        term.get(e, npts, GP, localMatrix, localVector);
        localMatrix.scale(invDTime);
        localVector.scale(invDTime);
      }
      space.getKeys(e, R);
      //assembler.assemble(R, localMatrix);
      assembler.assembleNR(R, localMatrix);
      
      // If constitutive law is of type "HMM_NL" and the nonlinear if in the time derivative term 
      //=========================================================================================
      if (term.getMaterialLaw()->getType() == HMM_NONLINEAR_MLAW) {
        localVector.scale(-1.0);
        assembler.assembleIntForces(R, localVector);
        localVector_Old.scale(-1.0);
        assembler.assembleIntForces(R, localVector_Old);
        //localMatrix.scale(-1.0);
        //assembler.assembleIntForces(R, localMatrix);
        localMatrix.print("Print matrix mag hmm_non_linear.");
        localVector.print("Print vector mag hmm_non_linear.");
      }
      else {
        localMatrix.scale(-1.0);
        assembler.assembleIntForcesTime(R, localMatrix);
        localMatrix.print("Print matrix mag linear.");
      }
    }
  }
  return 1;
}

// template<class Iterator> void getFirstAndLastIterators(Iterator &itbegin, Iterator &itend) {
//   fullMatrix<typename Assembler::dataMat> localMatrix;
//   fullVector<typename Assembler::dataVec> localVector;
//   std::vector<Dof> R;
//   for (Iterator it = itbegin; it != itend; ++it){
//     MElement *e = *it;
//     R.clear();
//     IntPt *GP;
//     int npts = term.getQuadrature()->getIntPoints(e, &GP);
//     term.get(e, npts, GP, localMatrix, localVector); 
//     space.getKeys(e, R);
//     assembler.assemble(R, localMatrix);
//   }
// }

#endif// _HMM_SOLVERALGORITHMS_H_

