%feature("autodoc", "1");
%module hmm
%include std_string.i
%include std_vector.i
%include std_map.i
%include <std_complex.i>
%{
  #include "dofManager.h"
  #include "linearSystem.h"
  #include "linearSystemGMM.h"
  #include "solverField.h"
  #include "NonLinearDofManager.h"
  #include "MultiscaleNonLinearSystem.h""
  #include "hmmSolverField.h"
  #include "hmmSolverAlgorithms.h"
  #include "hmmDomain.h"
  #include "hmmFunction.h"
  #include "hmmConstraints.h"
  #include "hmmFunctionSpace.h"
  #include "hmmFormulation.h"
  #include "hmmResolution.h"
  #include "hmmPostProcessing.h"
  #include "hmmProblem.h"
%}
%include "dofManager.h"
%template (dofManagerDouble) dofManager<double>;
%include "linearSystem.h"
%template (linearSystemDouble) linearSystem<double>;
%include "linearSystemGMM.h"
%template (linearSystemGmmDouble) linearSystemGmm<double>;

%include "solverField.h"
%template (SolverFieldDouble) SolverField<double>;

%include "NonLinearDofManager.h"
%template (NonLinearDofManagerDouble) NonLinearDofManager<double>;
%include "hmmSolverField.h"
%include "hmmSolverAlgorithms.h"
%nodefaultctor MultiscaleNonLinearSystem;
%include "MultiscaleNonLinearSystem.h"
%template (MultiscaleNonLinearSystemGmmDouble) MultiscaleNonLinearSystemGmm<double>;
%include "hmmDomain.h"               
%include "hmmFunction.h"
%nodefaultctor hmmFunctionBase;
%nodefaultctor hmmMaterialLawBase;
%nodefaultctor MLQtyBase;
%nodefaultctor hmmMicroSolverBase;
%nodefaultctor hmmMicroSolverGetDPBase;
%include "hmmConstraints.h"
%include "hmmFunctionSpace.h"
%include "hmmFormulation.h"
%include "hmmResolution.h"
%include "hmmPostProcessing.h"
%include "hmmProblem.h"
