from dgpy import *
from math import *
from gmshpy import *
import os
import sys

fixed = 1
free = -1
fail = 1
success = 0
geo = ".geo"
msh = ".msh"
TIME = function.getTime()

#generate a mesh
def genMesh(name, dim, order):
  fullName = name+geo
  print fullName
  if (os.path.exists(fullName)):
    g2 = GModel()
    g2.load(fullName)
    g2.mesh(dim)
    if (order > 1):
      g2.setOrderN(order, True, False)
    g2.save(name+msh)
  return name

def exitWithSuccess():
  sys.exit(success)

def exitWithFaillure():
  sys.exit(fail)
