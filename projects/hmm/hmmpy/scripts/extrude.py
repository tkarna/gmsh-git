from dgpy import *
import sys

def extrude(model, getLayers) :
  if isinstance(model, basestring) :
    name = model
    model = GModel()
    model.load(name)

  def newElement(elementType, partition, entity, entityAdd, vertices) :
    el = elementType(*vertices)
    for v in vertices :
      if v.thisown != 0 :
        entity.addMeshVertex(v)
        v.thisown = 0
    entityAdd(el)
    el.thisown = 0
    el.setPartition(partition)
    return el

  def newLine(nodes, z, edge, partition) :
    if (order == 1) : 
      return newElement(MLine, partition, edge, edge.addLine, (nodes[0].v(z), nodes[1].v(z)))
    elif (order == 2) :
      return newElement(MLine3, partition, edge, edge.addLine, (nodes[0].v(z), nodes[1].v(z), nodes[2].v(z)))

  def newTriangle(nodes, z, face, partition) :
    if (order == 1) : 
      return newElement(MTriangle, partition, face, face.addTriangle, (nodes[0].v(z), nodes[1].v(z), nodes[2].v(z)))
    elif (order == 2) :
      return newElement(MTriangle6, partition, face, face.addTriangle, (nodes[0].v(z), nodes[1].v(z), nodes[2].v(z), nodes[3].v(z), nodes[4].v(z), nodes[5].v(z)))

  def newQuadrangleV(nodes, z, face, partition) :
    if (order == 1):
      return newElement(MQuadrangle, partition, face, face.addQuadrangle, (nodes[0].v(z), nodes[0].v(z + 1), nodes[1].v(z + 1), nodes[1].v(z)))
    elif (order == 2):
      return newElement(MQuadrangle9, partition, face, face.addQuadrangle, (nodes[0].v(z), nodes[0].v(z + 1), nodes[1].v(z + 1), nodes[1].v(z), nodes[0].v(z, 0), nodes[2].v(z + 1), nodes[1].v(z, 0), nodes[2].v(z), nodes[2].v(z, 0)))

  def newPrism(nodes, z, region, partition) :
    if (order == 1):
      return  newElement(MPrism, partition, region, region.addPrism, (nodes[0].v(z + 1), nodes[1].v(z + 1), nodes[2].v(z + 1), nodes[0].v(z), nodes[1].v(z), nodes[2].v(z)))
    elif (order == 2):
      return newElement(MPrism18, partition, region, region.addPrism, (nodes[0].v(z+1), nodes[1].v(z+1), nodes[2].v(z+1), nodes[0].v(z), nodes[1].v(z), nodes[2].v(z), nodes[3].v(z+1), nodes[5].v(z+1), nodes[0].v(z, 0), nodes[4].v(z+1), nodes[1].v(z, 0), nodes[2].v(z,0), nodes[3].v(z), nodes[5].v(z), nodes[4].v(z), nodes[3].v(z, 0), nodes[5].v(z, 0), nodes[5].v(z, 0)))
    print("ERROR : extrusion not implemented for order > 2")

  def newEntity(model, entityType, physicalTag) :
    if entityType == discreteEdge :
      entity = entityType(model, model.getMaxElementaryNumber(-1) + 1, None, None)
    else :
      entity = entityType(model, model.getMaxElementaryNumber(-1) + 1)
    tag = model.getPhysicalNumber(entity.dim(), physicalTag)
    if (tag == -1) :
      tag = model.setPhysicalName(physicalTag, entity.dim())
    entity.addPhysicalEntity(tag)
    model.add(entity)
    entity.thisown = 0
    return entity

  class extrudedVertical(list) :
    def v(self, z, ho = -1):
      return self[z * order + ho + 1]

  groups = dgGroupCollection(model)
  groups.splitGroupsByPhysicalTag()
  order = groups.getElementGroup(0).getOrder()
  model3d = GModel()

  elementsNLayers = {}
  verticals = {}

  for iGroup in range(groups.getNbElementGroups()) :
    group = groups.getElementGroup(iGroup)
    for iElement in range(group.getNbElements()) :
      el = group.getElement(iElement)
      nLayersMin = sys.maxint 
      for iPt in range(group.getNbNodes()) :
        v = el.getVertex(iPt)
        layers = getLayers(v.x(), v.y(), iGroup, iElement, iPt)
        if (not verticals.has_key(v.getNum())) :
          extruded = extrudedVertical() 
          for i in range(len(layers) - 1) :
            for j in range(0, order) :
              extruded.append(MVertex(v.x(), v.y(), -(layers[i] * (order - j) + layers[i + 1] * j) * 1./ order))
          extruded.append(MVertex(v.x(), v.y(), -layers[-1]))
          verticals[v.getNum()] = extruded
        nLayersMin = min(nLayersMin , len(layers) - 1)
        if (nLayersMin < 1) :
          print("error : at least 2 vertices are needed")
      elementsNLayers[el.getNum()] = nLayersMin

  for iGroup in range(groups.getNbFaceGroups()) :
    group = groups.getFaceGroup(iGroup)
    nC = group.getNbGroupOfConnections()
    if nC == 1 :
      gLine = newEntity(model3d, discreteEdge, group.getPhysicalTag())
      gFace = newEntity(model3d, discreteFace, group.getPhysicalTag())
    else :
      gFace = newEntity(model3d, discreteFace, "vertical_bottom")
    for iFace in range(group.getNbElements()):
      eNLayers = [elementsNLayers[group.getGroupOfConnections(i).getElement(iFace).getNum()] for i in range(nC)]
      el = group.getFace(iFace)
      nodes = [verticals[el.getVertex(i).getNum()] for i in range(el.getNumVertices())]
      nLayersMax = max(eNLayers)
      partition = group.getGroupOfConnections(eNLayers.index(nLayersMax)).getElement(iFace).getPartition()
      if nC == 1 :
        newLine(nodes, 0, gLine, partition)
        nLayersMin = 0
      else :
        nLayersMin =  min(eNLayers)
      for z in range(nLayersMin, nLayersMax):
        newQuadrangleV(nodes, z, gFace, partition)

  for iGroup in range(groups.getNbElementGroups()) :
    group = groups.getElementGroup(iGroup)
    tag = group.getPhysicalTag()
    if tag != "" :
      tag = "_" + tag
    gFaceTop = newEntity(model3d, discreteFace, "top" + tag)
    gFaceBottom = newEntity(model3d, discreteFace, "bottom" + tag)
    gRegion = newEntity(model3d, discreteRegion, "volume" + tag)
    for iElement in range(group.getNbElements()):
      el = group.getElement(iElement)
      partition = el.getPartition()
      nodes = [verticals[el.getVertex(i).getNum()] for i in range(el.getNumVertices())]
      newTriangle(nodes, 0, gFaceTop, partition)
      newTriangle(nodes, elementsNLayers[el.getNum()], gFaceBottom, partition)
      for z in range(elementsNLayers[el.getNum()]) :
        newPrism(nodes, z, gRegion, partition)

  model3d.createPartitionBoundaries(1, 1)
  return model3d
