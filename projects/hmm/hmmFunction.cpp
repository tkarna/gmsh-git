#include "hmmFunction.h"

MLQtyBase::MLQtyBase(){}
MLQtyBase::~MLQtyBase(){}

HomQtyLinear::HomQtyLinear(fullMatrix<double> &matLaw, double vec1, double vec2, double vec3) {
  _matLaw = &matLaw;
  _vecLaw = new fullVector<double>(3);
  _vecLaw->set(0, vec1); _vecLaw->set(1, vec2); _vecLaw->set(2, vec3);
  //==================================================================
  _vecLawOld = new fullVector<double>(3);
  _vecLawOld->set(0, vec1); _vecLaw->set(1, vec2); _vecLaw->set(2, vec3);
  //==================================================================
}
HomQtyLinear::HomQtyLinear() {
  _matLaw = new fullMatrix<double>(3, 3);
  _vecLaw = new fullVector<double>(3);
  //==================================================================
  _vecLawOld = new fullVector<double>(3);
  //==================================================================
}
void HomQtyLinear::deletePointersFromAnyWhere() {
  if (_matLaw) delete _matLaw;
  if (_vecLaw) delete _vecLaw;
  //================================
  if (_vecLawOld) delete _vecLawOld;
  //================================  
}
void HomQtyLinear::printMLQty() {  
  std::cout << "The linear material law is defined by the following matrix and vector" << std::endl;
  getMatrix()->print("Matrix");
  getVector()->print("Vector");
}

HomQtyNL::HomQtyNL(hmmGroupOfDomains &dMLNL) { _dMLNL = &dMLNL; }
HomQtyNL::~HomQtyNL(){}
void HomQtyNL::InitMatLaw( GaussQuadrature &integrator, fullMatrix<double> &matLaw, double vec1, double vec2, double vec3) {
  std::set<hmmDomainBase*>::const_iterator itDom;
  for (itDom = _dMLNL->getGroupOfDomains()->begin(); itDom != _dMLNL->getGroupOfDomains()->end(); ++itDom) {
    std::map<int, HomQtyLinear *> currentMap;
    groupOfElements::elementContainer::const_iterator itElem;
    for (itElem = ( (*itDom)->getGroupOfElements()->begin() );  itElem != ( (*itDom)->getGroupOfElements()->end() ); ++itElem ) {
      MElement *ele = *(itElem);
      IntPt *GP; int npts = integrator.getIntPoints(ele, &GP);
      currentMap.empty();
      for(int i = 0; i < npts; i++) {
        HomQtyLinear *currentHomQtyLinear = new HomQtyLinear(matLaw, vec1, vec2, vec3);
        currentMap[i] =  currentHomQtyLinear; 
      }
      _globalMatLaw[ele] = currentMap;
    }
  }
}
std::map<MElement*, std::map<int, HomQtyLinear*> >::iterator HomQtyNL::begin() {
  std::map<MElement*, std::map<int, HomQtyLinear *> >::iterator itNLML = _globalMatLaw.begin();
  return itNLML;
}
std::map<MElement*, std::map<int, HomQtyLinear *> >::iterator HomQtyNL::end() {
  std::map<MElement*, std::map<int, HomQtyLinear *> >::iterator itNLML = _globalMatLaw.end();
  return itNLML;
}
std::map<MElement*, std::map<int, HomQtyLinear *> > *HomQtyNL::getMaterialLaw() {
  return &_globalMatLaw;
}
HomQtyLinear *HomQtyNL::getMaterialLawAtGaussPoint(MElement *ele, int gaussPointNum) {
  std::map<MElement*, std::map<int, HomQtyLinear *> >::iterator itGlobalML = _globalMatLaw.find(ele);
  std::map<int, HomQtyLinear *>::iterator itGaussPoint = itGlobalML->second.find(gaussPointNum);
  return itGaussPoint->second;
}
int HomQtyNL::getNumMicroProblems() const {
  int totNumMicroProb = 0;
  std::map<MElement*, std::map<int, HomQtyLinear *> >::const_iterator itNLML;
  for (itNLML = _globalMatLaw.begin(); itNLML != _globalMatLaw.end(); itNLML++) {
    totNumMicroProb += ( (int)(itNLML->second.size() ) );
  }
  return totNumMicroProb;
}
void HomQtyNL::UpdateOldVector() {
  std::map<MElement*, std::map<int, HomQtyLinear *> >::iterator itHQNL;
  for (itHQNL = this->begin(); itHQNL != this->end(); itHQNL++) {
    std::map<int, HomQtyLinear *> thisIntHQL = itHQNL->second;
    std::map<int, HomQtyLinear *>::iterator itHQL;
    for (itHQL = itHQNL->second.begin(); itHQL != itHQNL->second.end(); itHQL++) {
      itHQL->second->UpdateOldVector();
    }
  }
}
void HomQtyNL::printMLQty() {
  std::map<MElement*, std::map<int, HomQtyLinear*> >::iterator it;
  for (it = _globalMatLaw.begin(); it != _globalMatLaw.end(); ++it) {
    std::map<int, HomQtyLinear*>::iterator itELE;
    for (itELE = it->second.begin(); itELE != it->second.end(); ++itELE) {
      std::cout << "the material law for Gauss point number "<< itELE->first << " of element " << it->first->getNum() << " is " << std::endl;
      itELE->second->getMatrix()->print("matrix");
      itELE->second->getVector()->print("vector");
      
    }
  }
}

//================================================================================================================================================
HomQtyJouleLosses::HomQtyJouleLosses(hmmGroupOfDomains &dMLNL) { _dMLNL = &dMLNL; }
HomQtyJouleLosses::~HomQtyJouleLosses(){}
void HomQtyJouleLosses::InitJLMap( GaussQuadrature &integrator, double JL) {
  std::set<hmmDomainBase*>::const_iterator itDom;
  for (itDom = _dMLNL->getGroupOfDomains()->begin(); itDom != _dMLNL->getGroupOfDomains()->end(); ++itDom) {
    std::map<int, double> currentMap;
    groupOfElements::elementContainer::const_iterator itElem;
    for (itElem = ( (*itDom)->getGroupOfElements()->begin() );  itElem != ( (*itDom)->getGroupOfElements()->end() ); ++itElem ) {
      MElement *ele = *(itElem);
      IntPt *GP; int npts = integrator.getIntPoints(ele, &GP);
      currentMap.empty();

      // Loop over Gauss points
      //=======================
      for(int i = 0; i < npts; i++) {
        currentMap[i] =  JL; 
      }
      _globalJL[ele] = currentMap;
      std::cout << "The size of the map is " << _globalJL.size()  << "\n" << std::endl;
    }
  }
}
std::map<MElement*, std::map<int, double> >::iterator HomQtyJouleLosses::begin() {
  std::map<MElement*, std::map<int, double> >::iterator itNLML = _globalJL.begin();
  return itNLML;
}
std::map<MElement*, std::map<int, double> >::iterator HomQtyJouleLosses::end() {
  std::map<MElement*, std::map<int, double> >::iterator itNLML = _globalJL.end();
  return itNLML;
}
std::map<MElement*, std::map<int, double> > *HomQtyJouleLosses::getMapJL() {
  return &_globalJL;
}
double HomQtyJouleLosses::getJLAtGaussPoint(MElement *ele, int gaussPointNum) {
  std::map<MElement*, std::map<int, double> >::iterator itGlobalML = _globalJL.find(ele);
  std::map<int, double>::iterator itGaussPoint = itGlobalML->second.find(gaussPointNum);
  return itGaussPoint->second;
}
double HomQtyJouleLosses::computeTotalJouleLossesTimeStep(GaussQuadrature *integrator, int _currentTimeStep, double _currentTime) {
  // iterator for looping over all Gauss points of all elements
  //===========================================================
  std::map<MElement*, std::map<int, double> >::iterator itELE;
  double jouleLosses = 0.0;
  if(itELE != this->getMapJL()->end() ) {
    // if the map "_globalJL" (protceted member of the class "HomQtyJouleLosses") is not empty
    //========================================================================================
    for (itELE = this->getMapJL()->begin(); itELE != this->getMapJL()->end(); ++itELE) {
      // loop over the elements
      //=======================
      MElement * e = (*itELE).first;
      IntPt *GP; 
      int npts = integrator->getIntPoints(e, &GP);
      double jac[3][3];
      double val_JL_perElement = 0.0;
      for (int i = 0; i < npts; i++) {
        // loop over the Gauss points of an element
        //=========================================
        std::map<int, double>::const_iterator itJLGP = itELE->second.find(i);
        if(itJLGP !=  itELE->second.end() ) { 
          const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
          const double weight = GP[i].weight; const double detJ = e->getJacobian(u, v, w, jac);
          val_JL_perElement += detJ * weight * itJLGP->second; 
          Msg::Info("(Dans hmmFunction.cpp, for element %d  detJ = %g and j * e = %g) \n ", e->getNum(), detJ, itJLGP->second);      
          Msg::Info("(val_JL_perElement for gauss point %d of element %d = %g) \n ", i, e->getNum(), val_JL_perElement);      
        }
        else {
          Msg::Error("Done computing total Joule loses in hmmFunction. The Gauss point does not belong to  Element %d. \n", e->getNum() );
        }
      }
      jouleLosses += val_JL_perElement; 
      Msg::Info("(Joule losses for element %d = %g) \n ", e->getNum(), jouleLosses);
    }
  }
  else {
    Msg::Warning("Done computing Joule losses in hmmFunction. Iterator on elements not found. \n");
  }
  FILE *fp_JouleLosses;
  if(_currentTimeStep == 0) {
    fp_JouleLosses = fopen("../output/macroproblem/jouleLosses.dat", "w") ;
    fprintf(fp_JouleLosses, "%d %g %g \n" , _currentTimeStep, _currentTime, jouleLosses );
    fclose(fp_JouleLosses) ;
  }
  if(_currentTimeStep != 0) {
    fp_JouleLosses = fopen("../output/macroproblem/jouleLosses.dat", "a") ;
    fprintf(fp_JouleLosses, "%d %g %g \n" , _currentTimeStep, _currentTime, jouleLosses );
    fclose(fp_JouleLosses) ;
  }
  return jouleLosses;
}
int HomQtyJouleLosses::getNumMicroProblems() const {
  int totNumMicroProb = 0;
  std::map<MElement*, std::map<int, double> >::const_iterator itNLML;
  for (itNLML = _globalJL.begin(); itNLML != _globalJL.end(); itNLML++) {
    totNumMicroProb += ( (int)(itNLML->second.size() ) );
  }
  return totNumMicroProb;
}
void HomQtyJouleLosses::printMLQty() {
  std::map<MElement*, std::map<int, double> >::iterator it;
  for (it = this->getMapJL()->begin(); it != this->getMapJL()->end(); ++it) {
    std::map<int, double>::iterator itELE;
    for (itELE = it->second.begin(); itELE != it->second.end(); ++itELE) {
      std::cout << "Joule losses for Gauss point number "<< itELE->first << " of element " << it->first->getNum() << " is " << itELE->second << std::endl;
    }
  }
}
//===========================================================================================================================================


//================================================================================================================================================
HomQtyHystereticLosses::HomQtyHystereticLosses(hmmGroupOfDomains &dMLNL) { _dMLNL = &dMLNL; }
HomQtyHystereticLosses::~HomQtyHystereticLosses(){}
void HomQtyHystereticLosses::InitHLMapTimeStep( GaussQuadrature &integrator, double currentTime, double HL) {
  std::map<MElement*, std::map<int, double> > thisTSMap;
  std::set<hmmDomainBase*>::const_iterator itDom;
  for (itDom = this->_dMLNL->getGroupOfDomains()->begin(); itDom != this->_dMLNL->getGroupOfDomains()->end(); ++itDom) {
    std::map<int, double> currentMap;
    groupOfElements::elementContainer::const_iterator itElem;
    for (itElem = ( (*itDom)->getGroupOfElements()->begin() );  itElem != ( (*itDom)->getGroupOfElements()->end() ); ++itElem ) {
      MElement *ele = *(itElem);
      IntPt *GP; int npts = integrator.getIntPoints(ele, &GP);
      currentMap.empty();
      // Loop over Gauss points
      //=======================
      for(int i = 0; i < npts; i++) {
        currentMap[i] =  HL; 
        std::cout << "The size of the map is " << thisTSMap.size()  << " and the value for Gauss point "<< i <<" of element "<< ele->getNum() << " has been initialized to "<< HL << std::endl;
      }
      thisTSMap[ele] = currentMap;
    }
  }
  _globalHL[currentTime] = thisTSMap;
}

void HomQtyHystereticLosses::deleteHLMapTimeStep(double currentTime) {
  std::map< double, std::map<MElement*, std::map<int, double> > >::iterator itCurrentTimeHL = _globalHL.find(currentTime);
  if(itCurrentTimeHL != _globalHL.end() ) {
    Msg::Warning("Start deleting the pair corresponding to time %g in the map of hysteretic losses. \n");
    _globalHL.erase(currentTime);
    Msg::Warning("End deleting the pair corresponding to time %g in the map of hysteretic losses. \n");    
  }
}

std::map<double, std::map<MElement*, std::map<int, double> > >::iterator HomQtyHystereticLosses::begin() {
  std::map< double, std::map<MElement*, std::map<int, double> > >::iterator itCurrentTimeHL = _globalHL.begin();
  return itCurrentTimeHL;
}
std::map<double, std::map<MElement*, std::map<int, double> > >::iterator HomQtyHystereticLosses::end() {
  std::map< double, std::map<MElement*, std::map<int, double> > >::iterator itCurrentTimeHL = _globalHL.end();
  return itCurrentTimeHL;
}
std::map<double, std::map<MElement*, std::map<int, double> > >::iterator HomQtyHystereticLosses::getIteratorCurrentTime(double _currentTime, int isIteratorFound) {
  isIteratorFound = 1;
  std::map< double, std::map<MElement*, std::map<int, double> > >::iterator itCurrentTimeHL = _globalHL.find(_currentTime);
  if(itCurrentTimeHL == _globalHL.end() ) {
    std::map< double, std::map<MElement*, std::map<int, double> > >::iterator itFinalTimeHL = _globalHL.end();
    itFinalTimeHL--;
    Msg::Warning("Done while computing the hysteretic losses. The iterator you are searching for the hb curve is out of limit. The time %g for which you are searching info has rather been replaced by %g. \n", _currentTime, itFinalTimeHL->first);
    isIteratorFound = 0;
  }
  return itCurrentTimeHL;
}
std::map<double, std::map<MElement*, std::map<int, double> > > *HomQtyHystereticLosses::getMapHL() {
  return &_globalHL;
}

std::map<double, std::map<MElement*, std::map<int, double> > >::iterator HomQtyHystereticLosses::getIteratorNearestFirstTime(double initTime, int isIteratorFound) {
  isIteratorFound = 0;
  std::map< double, std::map<MElement*, std::map<int, double> > >::iterator itCurrentTimeHL = _globalHL.find(initTime);
  if (itCurrentTimeHL == _globalHL.end() ) {
    std::map<double, std::map<MElement*, std::map<int, double> >  >::iterator findNearestTimeIterator;
    for(findNearestTimeIterator = _globalHL.begin(); findNearestTimeIterator != _globalHL.end(); ++findNearestTimeIterator) {
      if (findNearestTimeIterator->first >= initTime) {
        itCurrentTimeHL = findNearestTimeIterator;
        isIteratorFound = 1;
        break;
      }
    }
  }
  else {
    isIteratorFound = 1;
  }
  return itCurrentTimeHL;
}
void HomQtyHystereticLosses::addTwoMaps(std::map<MElement*, std::map<int, double> > &mapResults, std::map<MElement*, std::map<int, double> > &mapOther){
  std::map<MElement*, std::map<int, double> >::iterator it1;
  for (it1 = mapResults.begin(); it1 != mapResults.end(); ++it1) {
    std::map<int, double> *thisEleMapResults = &(it1->second);
    std::map<int, double>::iterator itThisEleMapResults;
    for (itThisEleMapResults = it1->second.begin(); itThisEleMapResults != it1->second.end(); ++itThisEleMapResults) {
      std::map<MElement*, std::map<int, double> >::iterator it2 = mapOther.find(it1->first) ; 
      
      //std::map<int, double> thisEleMapOther = it2->second;
      std::map<int, double>::iterator itThisEleMapOther = it2->second.find(itThisEleMapResults->first);
      Msg::Warning("Done while adding the two maps. For the map of results, element %d, Gauss point %d. For the current time map , element %d, Gauss point %d.", it1->first, itThisEleMapResults->first, it2->first, itThisEleMapOther->first);

      thisEleMapResults->at(itThisEleMapResults->first) += itThisEleMapOther->second;
      //itThisEleMapResults->second += itThisEleMapOther->second;

      Msg::Warning("Done while adding the two maps. For the map of results, element %d, Gauss point %d. The old value of the map of results is %g, the current value is %g and the new value of the result map ", it1->first, itThisEleMapResults->first, itThisEleMapResults->second, itThisEleMapOther->second, (itThisEleMapResults->second + itThisEleMapOther->second) );
    }
  }
}

std::map<int, std::vector<double> > HomQtyHystereticLosses::computeTotaLocallHystereticLossesOverCycle(GaussQuadrature *integrator, 
                                                                                                       double initTime, double dtime, double freq, int cycleNum) {
  std::map<int, std::vector<double> > HystLossesData; 
  
  //   // First initialize the map
  //   //=========================
  
  //   if (freq != 0.0) {
  //     double period = 1.0/freq; 
  //     // check if initTime is the key of an element. If this is the case, use it to find the first iterator of the map
  //     // else use the pair whose key is the double that comes just after _initiTime. 
  //     // It should also be possible to check if the _finalTime = _initTime + period * cycleNum is still in the map.
  //     std::map<double, std::map<MElement*, std::map<int, double> >  >::iterator itInitTimeHL;
  //     int isIteratorFound;
  //     itInitTimeHL = getIteratorCurrentTime(initTime, isIteratorFound);
  
  //     // determine the final iterator.
  //     //=============================
  //     double finalTime = initTime + cycleNum * period;
  //     std::map<double, std::map<MElement*, std::map<int, double> >  >::iterator itFinalTimeHL = getIteratorCurrentTime(finalTime);
  //     if(itFinalTimeHL == this->end() ) {
  //       itFinalTimeHL--;
  //     }
  
  //     // Fill in the map of local unknowns
  //     std::map<double, std::map<MElement*, std::map<int, double> >  >::iterator itLoopOverTimeHL;
  //     std::map<MElement*, std::map<int, double> > thisTimeMapHL;
  //     for(itLoopOverTimeHL = itInitTimeHL; itLoopOverTimeHL != itFinalTimeHL; ++itLoopOverTimeHL) {
  //       thisTimeMapHL = itLoopOverTimeHL->second;
  //       std::map<MElement*, std::map<int, double> > thisTimeMapHL; 
  //     }
  //   }
  return HystLossesData;
}

double HomQtyHystereticLosses::integrateHystereticLossesOneTimeStep(GaussQuadrature *integrator, std::map<MElement*, std::map<int, double> > &_thisTimeMapHL, 
                                                                    const char *fileName, double currentTime) {
  Msg::Warning("Done in hmmFunction.cpp, Writing Hysteresis losses for time %g. \n", currentTime);
  std::map<MElement*, std::map<int, double> >::iterator itELE;
  double GQValue = 0.0;
  for (itELE = _thisTimeMapHL.begin(); itELE != _thisTimeMapHL.end(); ++itELE) {
    MElement * e = itELE->first;
    IntPt *GP; 
    int npts = integrator->getIntPoints(e, &GP);
    double jac[3][3];
    double val_HL_perElement = 0.0;
    for (int i = 0; i < npts; ++i) {
      std::map<int, double>::const_iterator itHLGP = itELE->second.find(i);
      if(itHLGP !=  itELE->second.end() ) { 
        const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
        const double weight = GP[i].weight; const double detJ = e->getJacobian(u, v, w, jac);
        val_HL_perElement += detJ * weight * itHLGP->second; 
        Msg::Info("(Dans hmmFunction.cpp, for element %d  detJ = %g and j * e = %g) \n ", e->getNum(), detJ, itHLGP->second);      
        Msg::Info("(val_JL_perElement for gauss point %d of element %d = %g) \n ", i, e->getNum(), val_HL_perElement);      
      }
      else {
        Msg::Error("Done computing total Joule loses in hmmFunction. The Gauss point does not belong to  Element %d. \n", e->getNum() );
      }
    }
    GQValue += val_HL_perElement; 
    Msg::Info("Current hysteresis losses at time %g is %g \n ", currentTime, GQValue);
  }
  FILE *fp_HystLosses;
  //char *thisFileName = fileName.c_str();
  fp_HystLosses = fopen(fileName, "a") ;
  fprintf(fp_HystLosses, "%g %g \n", currentTime, GQValue );
  fclose(fp_HystLosses) ;
  return GQValue;
}

double HomQtyHystereticLosses::computeTotalHystereticLossesOverCycle(GaussQuadrature *integrator,
                                                                     double initTime, double dtime, double freq, int cycleNum) {
  double TotalHystLossesOverCycle = 0.0; 
  std::map<MElement*, std::map<int, double> > _totalMapHL;
  // Initializing the map
  //=====================
  std::set<hmmDomainBase*>::const_iterator itDom;
  for (itDom = this->_dMLNL->getGroupOfDomains()->begin(); itDom != this->_dMLNL->getGroupOfDomains()->end(); ++itDom) {
    std::map<int, double> currentMap;
    groupOfElements::elementContainer::const_iterator itElem;
    for (itElem = ( (*itDom)->getGroupOfElements()->begin() );  itElem != ( (*itDom)->getGroupOfElements()->end() ); ++itElem ) {
      MElement *ele = *(itElem);
      IntPt *GP; int npts = integrator->getIntPoints(ele, &GP);
      currentMap.empty();
      // Loop over Gauss points
      //=======================
      for(int i = 0; i < npts; i++) {
        currentMap[i] =  0.0; 
        std::cout << "The size of the map of results is " << _totalMapHL.size()  << " and the value for Gauss point "<< i <<" of element "<< ele->getNum() << " has been initialized to "<< 0.0 << std::endl;
      }
      _totalMapHL[ele] = currentMap;
    }
  }

  Msg::Warning("Done in hmmFunction.cpp computing the total hysteresis losses over one cycle. The map for hysteresis losses has got %d time steps and a total of %d elements. \n", this->getMapHL()->size(), _totalMapHL.size() );
  this->printMLQty();
  // First initialize the map
  //=========================
  if (freq != 0.0) {
    double period = 1.0/freq; 
    // check if initTime is the key of an element. If this is the case, use it to find the first iterator of the map
    // else use the pair whose key is the double that comes just after _initiTime. 
    // It should also be possible to check if the _finalTime = _initTime + period * cycleNum is still in the map.
    std::map<double, std::map<MElement*, std::map<int, double> >  >::iterator itInitTimeHL;
    int isIteratorFound;
    itInitTimeHL = getIteratorCurrentTime(initTime, isIteratorFound);
    // determine the final iterator.
    //=============================
    double finalTime = initTime + cycleNum * period - dtime;
    int intFinalIterator;
    std::map<double, std::map<MElement*, std::map<int, double> >  >::iterator itFinalTimeHL = getIteratorCurrentTime(finalTime, intFinalIterator);
    if(itFinalTimeHL == this->end() ) {
      // Probably using indices out of the map
      //======================================
      itFinalTimeHL--;
    }
    // Fill in the map of local unknowns = Map of total losses per element
    //====================================================================
    std::map<double, std::map<MElement*, std::map<int, double> >  >::iterator itLoopOverTimeHL;
    std::map<MElement*, std::map<int, double> > thisTimeMapHL;

    FILE *fp_HystLosses;
    const char *fileName = "../output/macroproblem/HystereticLosses_AllTimeSteps.dat";
    fp_HystLosses = fopen(fileName, "w") ;
    fclose(fp_HystLosses) ;

    // Loop over time in order to change the values of the map of total losses
    //========================================================================
    double currentTime = initTime;
    for(itLoopOverTimeHL = itInitTimeHL; itLoopOverTimeHL != itFinalTimeHL; ++itLoopOverTimeHL) {
      // add the contribution of current timestep to total losses
      //=========================================================
      thisTimeMapHL = itLoopOverTimeHL->second;
      this->addTwoMaps(_totalMapHL, thisTimeMapHL);
      //this->integrateHystereticLossesOneTimeStep(integrator, thisTimeMapHL, fileName, currentTime);
      currentTime += dtime;
    }
  }
  else {
    Msg::Warning("Done computing hysteretic losses.You cannot provide a frequency which is zero.");
  }
  Msg::Warning("Done in hmmFunction.cpp, The size of the map containing total local hysteresis losses is %d. \n", _totalMapHL.size() );
  std::map<MElement*, std::map<int, double> >::iterator itELE;
  double HystLosses = 0.0;
  for (itELE = _totalMapHL.begin(); itELE != _totalMapHL.end(); ++itELE) {
    MElement * e = itELE->first;
    IntPt *GP; 
    int npts = integrator->getIntPoints(e, &GP);
    double jac[3][3];
    double val_HL_perElement = 0.0;
    for (int i = 0; i < npts; ++i) {
      std::map<int, double>::const_iterator itHLGP = itELE->second.find(i);
      if(itHLGP !=  itELE->second.end() ) { 
        const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
        const double weight = GP[i].weight; const double detJ = e->getJacobian(u, v, w, jac);
        val_HL_perElement += detJ * weight * itHLGP->second; 
        Msg::Info("(Dans hmmFunction.cpp, for element %d  detJ = %g and j * e = %g) \n ", e->getNum(), detJ, itHLGP->second);      
        Msg::Info("(val_JL_perElement for gauss point %d of element %d = %g) \n ", i, e->getNum(), val_HL_perElement);      
      }
      else {
        Msg::Error("Done computing total Joule loses in hmmFunction. The Gauss point does not belong to  Element %d. \n", e->getNum() );
      }
    }
    HystLosses += val_HL_perElement; 
    Msg::Info("Total hysteresis over %d = cycles is %g \n ", cycleNum, HystLosses);
  }
  FILE *fp_HystLosses;
  fp_HystLosses = fopen("../output/macroproblem/HystereticLosses_Total.dat", "w") ;
  fprintf(fp_HystLosses, "%g \n", HystLosses );
  fclose(fp_HystLosses) ;
  return TotalHystLossesOverCycle;
}

void HomQtyHystereticLosses::printMLQty() {
  Msg::Warning("Done in hmmFunction.cpp. \n");
  Msg::Warning("Start printing the map of hysteresis losses in time. \n");
  std::map<double, std::map<MElement*, std::map<int, double> > >::iterator itTimeHL;
  for (itTimeHL = this->_globalHL.begin(); itTimeHL != this->_globalHL.end(); ++itTimeHL) {
    Msg::Warning("Start printing for time %g. \n", itTimeHL->first);
    std::map<MElement*, std::map<int, double> > thisTimeHL = itTimeHL->second;
    std::map<MElement*, std::map<int, double> >::iterator itThisTimeHL;
    for (itThisTimeHL = thisTimeHL.begin(); itThisTimeHL != thisTimeHL.end(); ++itThisTimeHL) {
      std::map<int, double> thisEleMapHL = itThisTimeHL->second;
      std::map<int, double>::iterator itThisEleMapHL;
      for (itThisEleMapHL = thisEleMapHL.begin(); itThisEleMapHL != thisEleMapHL.end(); ++itThisEleMapHL) {
        Msg::Warning("Done printing the map of hysteresis losses. The value of Hysteresis losses for time = %g, Element %d and Gauss Point %d is %g. \n", itTimeHL->first, itThisTimeHL->first->getNum(), itThisEleMapHL->first, itThisEleMapHL->second);
      }
    }
  }
}
//===========================================================================================================================================


void hmmFunctionBase::addGroupOfDomains(hmmGroupOfDomains &groupOfDomains, MLQtyBase &MQB) { 
  _matLawByGroupOfDom.insert(std::make_pair(&groupOfDomains, &MQB) );
}
const std::map<hmmGroupOfDomains *, MLQtyBase *> *hmmFunctionBase::getMatLawByGroupOfDomains() const { 
  return &_matLawByGroupOfDom; 
}
MLQtyBase *hmmFunctionBase::getMQB(hmmGroupOfDomains &thisGoD) const { 
  std::map<hmmGroupOfDomains *, MLQtyBase *>::const_iterator itGoDMQB = _matLawByGroupOfDom.find(&thisGoD);
  if (itGoDMQB != _matLawByGroupOfDom.end() ) {
    return itGoDMQB->second; 
  }
  else {
    Msg::Error("The ML is not defined on the current groupOfDomains");
    return NULL;
  }
}
bool hmmFunctionBase::setMQB(hmmGroupOfDomains &thisGoD, MLQtyBase &MQB) {
  // Method used in order to set the MQB associated to a hmmGoD
  std::map<hmmGroupOfDomains *, MLQtyBase*>::iterator itMQB = _matLawByGroupOfDom.find(&thisGoD);
  if (itMQB != _matLawByGroupOfDom.end() ) {
    _matLawByGroupOfDom[&thisGoD] = (&MQB);
    return true;
  }
  else {
    Msg::Error("The function 'setMQB(GoD, MQB)' is not defined on this domain. \n");
    return false;
  }
}
hmmGroupOfDomains *hmmFunctionBase::getGroupOfDomainsForElement(MElement *ele) const {
  // Method used in order to know the hmmGroupOfDomains an element belongs to (eg: In case a material law is defined 
  // piecewise on different hmmGroupOfDomains, it would be possible to get the MLQtyBase for an element of a hmmGroupOfDomains). 
  std::map<hmmGroupOfDomains *, MLQtyBase *>::const_iterator itGoDMQB;
  bool eleInGoD = false;
  for (itGoDMQB = _matLawByGroupOfDom.begin(); itGoDMQB != _matLawByGroupOfDom.end(); ++itGoDMQB) {
    hmmGroupOfDomains *thisGoD = itGoDMQB->first;
    if (thisGoD->elementIsIncluded(ele) ) {
      eleInGoD = true;
      return itGoDMQB->first;
    }
  }
  if(!eleInGoD) {
    Msg::Error("This ML is not defined on this element, returning a NULL pointer.");
    return NULL;
  }
}
MLQtyBase *hmmFunctionBase::getMLOnElement(MElement *ele) const {
  // Method used in order to get the MLQtyBase on the element 'ele'.  
  hmmGroupOfDomains *thisGoD;
  thisGoD = this->getGroupOfDomainsForElement(ele);
  if (thisGoD != NULL) {
    std::map<hmmGroupOfDomains *, MLQtyBase *>::const_iterator itGoDMQB = _matLawByGroupOfDom.find(thisGoD);
    return itGoDMQB->second;
  }
  else {
    Msg::Error("The material law is not defined on this element, returning a NULL pointer.");
    return NULL;
  }
}
int hmmFunctionBase::printNumOfDomains() {
  int numOfDomains = (int) (_matLawByGroupOfDom.size() ) ;
  Msg::Info("This function is defined on %d domains. \n", numOfDomains );
  return numOfDomains;
}

hmmMaterialLawBase::hmmMaterialLawBase(std::string tag) : hmmFunctionBase::hmmFunctionBase() { _tag = tag; } 
MLQtyBase *hmmMaterialLawBase::getMaterialLaw(hmmGroupOfDomains &thisGoD) const {
  // the method returns the MQB for the ML on the groupOfDomains 'thisGoD'.
  return this->getMQB(thisGoD);
}
MLQtyBase *hmmMaterialLawBase::getMaterialLaw(MElement *ele) const {
  // the method returns the MQB for the ML on the element 'ele'.
  return this->getMLOnElement(ele);
}
bool hmmMaterialLawBase::setMaterialLaw(hmmGroupOfDomains &thisGoD, MLQtyBase &MQB) {
  // the method sets the MQB for the ML defined on the groupOfDomains 'thisGoD'.
  return this->setMQB(thisGoD, MQB);
}
void hmmMaterialLawBase::printML(hmmGroupOfDomains &thisGoD) const {
  std::map<hmmGroupOfDomains *, MLQtyBase *>::const_iterator itGoDMQB = this->getMatLawByGroupOfDomains()->find(&thisGoD);
  if(itGoDMQB != this->getMatLawByGroupOfDomains()->end() ) {
    (this->getMQB(thisGoD))->printMLQty();
  }
  else {
    Msg::Error("In hmmFunction. You are printing the material law '%s' that is not defined on domain '%s' ", 
               (this->getTag().c_str() ), ( (thisGoD.getTagGroupOfDomains() ).c_str() ) );
  }
    return; 
}
void hmmMaterialLawBase::printMLOnAllDom() const {
  std::map<hmmGroupOfDomains *, MLQtyBase *>::const_iterator itGoDMQB;
  for (itGoDMQB = getMatLawByGroupOfDomains()->begin(); itGoDMQB != getMatLawByGroupOfDomains()->end(); ++itGoDMQB) {
    this->printML(*(itGoDMQB->first));
  }
  return; 
}

hmmLinearMaterialLaw::hmmLinearMaterialLaw(std::string tag) : hmmMaterialLawBase::hmmMaterialLawBase(tag) {
  this->_whatFunctionType = LINEAR_MLAW;
} 

hmmMicroSolverBase::hmmMicroSolverBase(std::string tag, double lx, double ly, int nx, int ny) : hmmMaterialLawBase::hmmMaterialLawBase(tag) {
  _lx = lx; _ly = ly;
  _nx = nx; _ny = ny;
}

hmmMicroSolverGetDPBase::hmmMicroSolverGetDPBase(std::string tag, double lx, double ly, int nx, int ny) : hmmMicroSolverBase::hmmMicroSolverBase(tag, lx, ly, nx, ny) {
  this->_whatFunctionType = HMM_LINEAR_MLAW;
  _GMSHVerbosity = " 0 "; _GetDPVerbosity = " 0 ";
  _microProblemFileName = ""; //FixMe
}
void hmmMicroSolverGetDPBase::setMicroParam(std::string GMSHVerbosity, std::string GMSHGeoFileName, std::string GetDPVerbosity, std::string GetDPProFileName, 
                                            std::string GetDPResolutionName, std::string GetDPPostPro, std::string GMSHExactGeoFileName, std::string GetDPExactProFileName, std::string microProblemFileName) {
  _GMSHVerbosity  = GMSHVerbosity ; _GMSHGeoFileName = GMSHGeoFileName; 
  _GetDPVerbosity = GetDPVerbosity; _GetDPProFileName = GetDPProFileName;
  _GetDPResolutionName = GetDPResolutionName; _GetDPPostPro = GetDPPostPro;
  if (GMSHExactGeoFileName == "") {
    _GMSHExactGeoFileName = GMSHGeoFileName; 
    _GetDPExactProFileName = GetDPProFileName;
  } 
  else {
    _GMSHExactGeoFileName = GMSHExactGeoFileName;
    _GetDPExactProFileName  = GetDPExactProFileName;
  }
  _microProblemFileName = microProblemFileName;
}
double hmmMicroSolverGetDPBase::evaluateFuncInPoint(SPoint3 &sp, SPoint3 &gaussPoint, ValType &func, GradType &gradFunc) {
  return func+ (sp.x()-gaussPoint.x())*gradFunc[0] + (sp.y()-gaussPoint.y())*gradFunc[1] + (sp.z()-gaussPoint.z())*gradFunc[2] ;
}
void hmmMicroSolverGetDPBase::evaluateFuncOnRegularGrid(SPoint3 &gaussPoint, ValType &func, GradType &gradFunc) {
  double xL = gaussPoint.x()-(this->_lx/2);
  double yB = gaussPoint.y()-(this->_ly/2);
  double dx = this->_lx/(this->_nx-1), dy = this->_ly/(this->_ny-1);
  int numpoints = this->_nx * this->_ny;

  std::vector<double> x_coord, y_coord, funct_values;

  for (int i = 0; i < this->_nx; i++) { x_coord.push_back(xL + dx * i); }
  for (int j = 0; j < this->_ny; j++) { y_coord.push_back(yB + dy * j); }
  for (int k = 0; k < numpoints; k++) {
    int i = floor(k/(this->_nx)); int j = k%(this->_nx);
    SPoint3 current_point = SPoint3(x_coord[j], y_coord[i], 0.0);
    funct_values.push_back( evaluateFuncInPoint(current_point, gaussPoint, func, gradFunc) );
  }
  std::string _gridFileName = _microProblemFileName+"../output/mesoproblems/computational/grid.txt";
  FILE *fp; fp = fopen(_gridFileName.c_str(), "w") ;

  fprintf(fp, "%s %d, %d, \n", "List_XY_potential = {", this->_nx, this->_ny);
  for (int i = 0; i < this->_nx; i++)
    if (i == this->_nx-1)  fprintf(fp, "%.16g, \n ", x_coord[i]);
    else fprintf(fp, "%.16g, ", x_coord[i]);
  for (int j = 0; j < this->_ny; j++)
    if (j == this->_ny-1) fprintf(fp, "%.16g, \n",  y_coord[j]);
    else fprintf(fp, "%.16g, ", y_coord[j]);
  for (int k = 0; k < numpoints; k++)
    if (k == numpoints-1) fprintf(fp, "%.16g %s ", funct_values[k], "};");
    else fprintf(fp, "%.16g, ", funct_values[k]);

  fclose(fp);
}

hmmMicroSolverGetDPLinearDouble::~hmmMicroSolverGetDPLinearDouble() {}

hmmMicroSolverGetDPNonLinearDouble::hmmMicroSolverGetDPNonLinearDouble(NonLinearDofManager<double> &pAssembler, hmmScalarLagrangeFunctionSpace &LagSpace, GaussQuadrature &integrator, std::string tagMaterialLaw, double lx, double ly, int nx, int ny) : hmmMicroSolverGetDPBase::hmmMicroSolverGetDPBase(tagMaterialLaw, lx, ly, nx, ny)  {
  _integrator = &integrator;
  _solField = new hmmSolverField(pAssembler, LagSpace);
  this->setType();
  _GMSHVerbosity = " 0 "; _GetDPVerbosity = " 0 ";
}

void hmmMicroSolverGetDPNonLinearDouble::addValueToMapHL(double currentTime, MElement *ele, SPoint3 &_gaussPoint, double val) {
  Msg::Warning("Done in hmmFunction.cpp while filling in the map of hyst. Losses for time %g. %d time steps have already been written", currentTime, getMapHL()->getMapHL()->size() );
  std::map<double, std::map<MElement *, std::map<int, double> > >::iterator itGlobalHL = _HL->getMapHL()->find(currentTime);
  if(itGlobalHL != _HL->getMapHL()->end() ) {
    Msg::Warning("Done in hmmFunction.cpp while filling in the map of global quantities for time = %g have already been initialized. \n", currentTime);
    std::map<MElement *, std::map<int, double> > *thisTimeMapHL = &(itGlobalHL->second);
    std::map<MElement *, std::map<int, double> >::iterator itThisTimeMapHL = thisTimeMapHL->find(ele);
    if(itThisTimeMapHL != thisTimeMapHL->end() ) {
      std::map<int, double> *thisEleMapHL = &(itThisTimeMapHL->second);
      int thisGaussPointNum = convertFromPointToInt(ele, _gaussPoint);
      std::map<int, double>::iterator itThisGPMapHL = itThisTimeMapHL->second.find(thisGaussPointNum); 
      double hystBefore = itThisGPMapHL->second;
      thisEleMapHL->at(itThisGPMapHL->first) = val;
      //thisEleMapHL[itThisGPMapHL->first] = val;
      Msg::Warning("Done in hmmFunction.cpp while filling in the map of hysteresis for Element %d,Gauss point %d for time = %g have already been initialized. It is being changed from %g to %g \n", itThisTimeMapHL->first->getNum(), itThisGPMapHL->first, currentTime, hystBefore, val);       
      Msg::Warning("Done in hmmFunction.cpp while filling in the map of global quantities. There are %d timesteps, %d MElements and %d Gauss Points for element %d. \n", getMapHL()->getMapHL()->size(), thisTimeMapHL->size(), thisEleMapHL->size() );        
    }
    else {
      Msg::Error("Done while filling in the map of losses for the Gauss Point %d of Element %d at Time = %g. The update for hysteresis losses has failed, MElement not found. \n", convertFromPointToInt(ele, _gaussPoint), ele->getNum(), currentTime);
    }
  }
  else {
    Msg::Warning("Done in hmmFunction.cpp while filling in the map of hyst. Hysteresis losses for time = %g are being initialized. \n", currentTime);
    //itGQ->second->InitHLMapTimeStep(*_integrator, currentTime, 0.0);
    _HL->InitHLMapTimeStep(*_integrator, currentTime, 0.0);
    std::map<MElement *, std::map<int, double> > *thisTimeMapHL = &(itGlobalHL->second);
    std::map<MElement *, std::map<int, double> >::iterator itThisTimeMapHL = thisTimeMapHL->find(ele);
    if(itThisTimeMapHL != thisTimeMapHL->end() ) {
      std::map<int, double> *thisEleMapHL = &(itThisTimeMapHL->second);
      int thisGaussPointNum = convertFromPointToInt(ele, _gaussPoint);
      std::map<int, double>::iterator itThisGPMapHL = itThisTimeMapHL->second.find(thisGaussPointNum); 
      thisEleMapHL->at(itThisGPMapHL->first) = val;
      //thisEleMapHL[itThisGPMapHL->first] = HystereticLosses;
    }
    else {
      Msg::Error("Done while filling in the map of losses for the Gauss Point %d of Element %d at Time = %g in the initialization process. The update of hysteresis losses has failed. \n", convertFromPointToInt(ele, _gaussPoint), ele->getNum(), currentTime);
    }
  }
  return; 
}



void hmmMicroSolverGetDPNonLinearDouble::addValueToMapGQ(std::string nameGQ, double currentTime, MElement *ele, SPoint3 &_gaussPoint, double val) {
  // Search in the map of GQ
  //======================== 
  std::map<std::string, HomQtyHystereticLosses *>::iterator itGQ = _GQ->getMapGQ()->find(nameGQ);
  //std::map<std::string, std::map<double, std::map<MElement *, std::map<int, double> > > >::iterator itGQ = getMapGQ()->getMapGQ()->find(nameGQ);
  if(itGQ != _GQ->getMapGQ()->end() ) {
    HomQtyHystereticLosses *thisGQ = itGQ->second;
    std::map<double, std::map<MElement *, std::map<int, double> > > *thisGQMap = thisGQ->getMapHL();
    std::map<double, std::map<MElement *, std::map<int, double> > >::iterator itGlobalHL = thisGQMap->find(currentTime);
    
    if(itGlobalHL != thisGQMap->end() ) {
      Msg::Warning("Done in hmmFunction.cpp while filling in the map of global quantities for time = %g have already been initialized. \n", currentTime);
      std::map<MElement *, std::map<int, double> > *thisTimeMapHL = &(itGlobalHL->second);
      std::map<MElement *, std::map<int, double> >::iterator itThisTimeMapHL = thisTimeMapHL->find(ele);
      if(itThisTimeMapHL != thisTimeMapHL->end() ) {
        std::map<int, double> *thisEleMapHL = &(itThisTimeMapHL->second);
        int thisGaussPointNum = convertFromPointToInt(ele, _gaussPoint);
        std::map<int, double>::iterator itThisGPMapHL = itThisTimeMapHL->second.find(thisGaussPointNum); 
        double GQBefore = itThisGPMapHL->second;
        thisEleMapHL->at(itThisGPMapHL->first) = val;
        //thisEleMapHL[itThisGPMapHL->first] = HystereticLosses;
        Msg::Warning("Done in hmmFunction.cpp while filling in the map of hysteresis for Element %d,Gauss point %d Global quantity %s for time = %g have already been initialized. It is being changed from %g to %g \n", itThisTimeMapHL->first->getNum(), itThisGPMapHL->first, nameGQ.c_str(), currentTime, GQBefore, val);       
        Msg::Warning("Done in hmmFunction.cpp while filling in the map of global quantities. There are %d global quantities, %d timesteps, %d MElements and %d Gauss Points for element %d. \n", _GQ->getMapGQ()->size(), thisGQ->getMapHL()->size(), thisTimeMapHL->size(), thisEleMapHL->size(), itThisTimeMapHL->first->getNum());        
      }
      else {
        Msg::Error("Done while filling in the map of losses for the Gauss Point %d of Element %d at Time = %g not in the initialization process. The update of hysteresis losses has failed. \n", convertFromPointToInt(ele, _gaussPoint), ele->getNum(), currentTime);
      }
    }

    else {
      Msg::Warning("Done in hmmFunction.cpp while filling in the map of hyst. Hysteresis losses for time = %g are being initialized. \n", currentTime);
      itGQ->second->InitHLMapTimeStep(*_integrator, currentTime, 0.0);
      //_HL->InitHLMapTimeStep(*_integrator, currentTime, 0.0);
      std::map<MElement *, std::map<int, double> > *thisTimeMapHL = &(itGlobalHL->second);
      std::map<MElement *, std::map<int, double> >::iterator itThisTimeMapHL = thisTimeMapHL->find(ele);
      if(itThisTimeMapHL != thisTimeMapHL->end() ) {
        std::map<int, double> *thisEleMapHL = &(itThisTimeMapHL->second);
        int thisGaussPointNum = convertFromPointToInt(ele, _gaussPoint);
        std::map<int, double>::iterator itThisGPMapHL = itThisTimeMapHL->second.find(thisGaussPointNum); 
        thisEleMapHL->at(itThisGPMapHL->first) = val;
        //thisEleMapHL[itThisGPMapHL->first] = HystereticLosses;
      }
      else {
        Msg::Error("Done while filling in the map of losses for the Gauss Point %d of Element %d at Time = %g in the initialization process. The update of hysteresis losses has failed. \n", convertFromPointToInt(ele, _gaussPoint), ele->getNum(), currentTime);
      }
    }
  }
  else {
    Msg::Error("Done while filling in the map of global quantities labelled %s for the Gauss Point %d of Element %d at Time = %g. The update of the global quantity has failed. \n", nameGQ.c_str(), convertFromPointToInt(ele, _gaussPoint), ele->getNum(), currentTime);
  }
  return; 
}

void hmmMicroSolverGetDPNonLinearDouble::initMaterialLaw(hmmGroupOfDomains &thisGoD, fullMatrix<double> &matLaw, double vec1, double vec2, double vec3) {
  std::map<hmmGroupOfDomains *, MLQtyBase *>::iterator itGoDMQB =  this->_matLawByGroupOfDom.find(&thisGoD);
  if(itGoDMQB !=  this->_matLawByGroupOfDom.end() ) {
    HomQtyNL *thisNLMQB = (HomQtyNL *)(itGoDMQB->second);
    thisNLMQB->InitMatLaw(*_integrator, matLaw, vec1, vec2, vec3);
    Msg::Warning("Done in hmmFunction.cpp. The HomQtyNL has been defined on region tagged '%s'. \n", (thisGoD.getTagGroupOfDomains() ).c_str() );
  }
  else {
    Msg::Error("Error in hmmFunction.cpp. The HomQtyNL is not defined on region tagged '%s'. \n", (thisGoD.getTagGroupOfDomains() ).c_str() );
  }
}

void hmmMicroSolverGetDPNonLinearDouble::initJLMap(hmmGroupOfDomains &thisGoD, double JL) {
  _JL = new HomQtyJouleLosses(thisGoD);
  _JL->InitJLMap(*_integrator);
}
double hmmMicroSolverGetDPNonLinearDouble::integRealQtyAtGaussPoints(std::map<MElement*, std::map<int, double> > &thisTimeMap, 
                                                                     std::string nameOfQty, int _currentTimeStep, double _currentTime) {
  std::map<MElement *, std::map<int, double> >::iterator itGlobalQty;
  double globalQty = 0.0;
  double val_GlobalQty_perElement;
  if (thisTimeMap.size() != 0) {
    for (itGlobalQty = thisTimeMap.begin(); itGlobalQty != thisTimeMap.end(); ++itGlobalQty) {
      MElement * e = itGlobalQty->first;
      IntPt *GP; 
      int npts = getIntegrationMethod()->getIntPoints(e, &GP);
      double jac[3][3];
      val_GlobalQty_perElement = 0.0;
      for (int i = 0; i < npts; ++i) {
        std::map<int, double>::const_iterator itHLGP = itGlobalQty->second.find(i);
        if(itHLGP !=  itGlobalQty->second.end() ) { 
          const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
          const double weight = GP[i].weight; const double detJ = e->getJacobian(u, v, w, jac);
          val_GlobalQty_perElement += detJ * weight * itHLGP->second; 
          Msg::Info("(Dans hmmFunction.cpp, for element %d  detJ = %g and j * e = %g) \n ", e->getNum(), detJ, itHLGP->second);      
          Msg::Info("(val_JL_perElement for gauss point %d of element %d = %g) \n ", i, e->getNum(), val_GlobalQty_perElement);      
        }
        else {
          Msg::Error("Done computing a global quantity in 'hmmFunction.cpp'. The Gauss point does not belong to  Element %d. \n", e->getNum() );
        }
      }
      globalQty += val_GlobalQty_perElement; 
    }
  }
  else {
    Msg::Warning("Done in hmmFunction.cpp while trying to integrate a quantity defined at Gauss points. The size of the map is %d. \n", thisTimeMap.size() );
  }
  FILE *fp_JouleLosses;
  std::string saveFileName = "../output/macroproblem/" + nameOfQty + ".dat";
  if(_currentTimeStep == 0) {
    fp_JouleLosses = fopen(saveFileName.c_str(), "w") ;
    fprintf(fp_JouleLosses, "%d %g %g \n" , _currentTimeStep, _currentTime, globalQty );
    fclose(fp_JouleLosses) ;
  }
  if(_currentTimeStep != 0) {
    fp_JouleLosses = fopen(saveFileName.c_str(), "a") ;
    fprintf(fp_JouleLosses, "%d %g %g \n" , _currentTimeStep, _currentTime, globalQty );
    fclose(fp_JouleLosses) ;
  }
  return globalQty;
}

double hmmMicroSolverGetDPNonLinearDouble::integrateGQOneTimeStep(std::string nameGQ, std::string nameOfQty, int _currentTimeStep, double _currentTime) {  
  double val = 0.0;
  std::map<std::string, HomQtyHystereticLosses* >::iterator itGQHom = _GQ->getMapGQ()->find(nameGQ);
  if(itGQHom != _GQ->getMapGQ()->end() ) {
    std::map<double, std::map<MElement*, std::map<int, double> > > *thisGQMap = itGQHom->second->getMapHL();
    std::map<double, std::map<MElement *, std::map<int, double> > >::iterator itOneGQ = thisGQMap->find(_currentTime);
    if(itOneGQ != thisGQMap->end() ) {
      std::map<MElement *, std::map<int, double> > thisTimeMap = itOneGQ->second;
      val = integRealQtyAtGaussPoints(thisTimeMap, nameOfQty, _currentTimeStep, _currentTime);
    }
  }
  else {
    Msg::Error("Done in hmmFunction.cpp: Error occurred computing total global quantity for a given timestep.");
  }
  return val;
}

void hmmMicroSolverGetDPNonLinearDouble::integrateGQ(std::string nameGQ, std::string fileName) {
  HomQtyGlobalQuantities *thisGQMap = getMapGQ();
  thisGQMap->integrateGQOverTime(nameGQ, this->_integrator, fileName);
}

void hmmMicroSolverGetDPNonLinearDouble::convertFromGaussPointDataToElementData(std::map<MElement*, std::map<int, double> > &thisTimeMap, 
                                                                                std::map<int, std::vector<double> > &mapForPlot, 
                                                                                bool isQtyExtensive) {
  std::map<MElement *, std::map<int, double> >::iterator itGlobalQty;
  //double globalQty = 0.0;
  if (thisTimeMap.size() != 0) {
    for (itGlobalQty = thisTimeMap.begin(); itGlobalQty != thisTimeMap.end(); ++itGlobalQty) {
      MElement * e = itGlobalQty->first;
      IntPt *GP; 
      int npts = getIntegrationMethod()->getIntPoints(e, &GP);
      double jac[3][3];
      double val_GlobalQty_perElement = 0.0;
      for (int i = 0; i < npts; ++i) {
        std::map<int, double>::const_iterator itHLGP = itGlobalQty->second.find(i);
        if(itHLGP !=  itGlobalQty->second.end() ) { 
          const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
          const double weight = GP[i].weight; const double detJ = e->getJacobian(u, v, w, jac);
          val_GlobalQty_perElement += detJ * weight * itHLGP->second; 
          Msg::Info("(Dans hmmFunction.cpp, for element %d  detJ = %g and j * e = %g) \n ", e->getNum(), detJ, itHLGP->second);      
          Msg::Info("(val_JL_perElement for gauss point %d of element %d = %g) \n ", i, e->getNum(), val_GlobalQty_perElement);      
        }
        else {
          Msg::Error("Done computing a global quantity in 'hmmFunction.cpp'. The Gauss point does not belong to  Element %d. \n", e->getNum() );
        }
      }
      std::vector<double> vec(1);
      // The total value over the element is stored as the value for the element. For intensive quantities 
      // (e.g.: fields) the total value for the element is divided by the volume of the element. 
      //==================================================================================================
      if (!isQtyExtensive) {
        val_GlobalQty_perElement /= (e->getVolume() );
      }
      vec[0] = val_GlobalQty_perElement;
      mapForPlot[e->getNum()] = vec;
    }
  }
  else {
    Msg::Warning("Done in hmmFunction.cpp while trying to convert GaussPointsData to ElementData. \n");
  }
}
//void hmmMicroSolverGetDPNonLinearDouble::convertExtensiveData_From_GaussPointData_To_ElementData(std::map<int, std::vector<double> > &mapForPlot,  std::map<MElement *, std::map<int, double> > &thisTimeMap) const {
  // First compute the value for the element
  //======================================== 
  
//}


//===========================================================================================================================================

SPoint3 *hmmMicroSolverGetDPNonLinearDouble::convertFromIntToPoint(MElement *ele, int GPNum) {
  IntPt *GP;
  int npts = _integrator->getIntPoints(ele, &GP);
  for(int i = 0; i < npts; i++) {
    if (GPNum ==i) {
      const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
      SPoint3 *currentGP = new SPoint3();
      ele->pnt(u, v, w, *currentGP);
      return currentGP;
    }
    else {
      Msg::Error("The point does not belong to the current element. \n");
      SPoint3 *zero = new SPoint3(0.0, 0.0, 0.0);
      return zero;
    }
  }
}
int hmmMicroSolverGetDPNonLinearDouble::convertFromPointToInt(MElement *ele, SPoint3 &p) {
  // Convert from xyz to uvw coordinates
  //====================================
  double xyz[3];
  xyz[0] = p.x(); xyz[1] = p.y(); xyz[2] = p.z();
  double uvw[3];
  ele->xyz2uvw(xyz, uvw);
  
  int GPNum = -1;
  IntPt *GP;
  int npts = _integrator->getIntPoints(ele, &GP);
  double eps = 10.0e-14;
  for(int i = 0; i < npts; i++) {
    const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
    if ( ( (u - uvw[0]) < eps) && ( (v - uvw[1]) < eps ) && ( (w - uvw[2]) < eps) ) {
      GPNum = i;
    }
  }
  return GPNum;
}

void hmmMicroSolverGetDPNonLinearDouble::computeMaterialLawAtGaussPoint( HomQtyLinear &HomFields, SPoint3 &_gaussPoint, MElement *ele, int thisThreadNum, double currentTime, int currentTimeStep, bool h_conform, bool changeDualPrevValue) 
{
  double xyz[3] = {_gaussPoint.x(), _gaussPoint.y(), _gaussPoint.z()};
  double uvw[3] ;
  
  ele->xyz2uvw(xyz, uvw);
  TensorialTraits<double>::ValType _func, _func_prev, _dtFunc; TensorialTraits<double>::GradType _gradFunc, _gradFunc_prev, _dtGradFunc;
  hmmSolverField *solFields = (hmmSolverField *)(_solField);
  solFields->dtF(ele, uvw[0], uvw[1], uvw[2], _dtFunc);
  solFields->dtGradf(ele, uvw[0], uvw[1], uvw[2], _dtGradFunc);
  std::cout << "_dtFunc = "<< _dtFunc <<", _dtgradFunc = ["<< _dtGradFunc[0] <<","<< _dtGradFunc[1] <<","<< _dtGradFunc[2] <<"]"<< std::endl;
  
  solFields->f(ele, uvw[0], uvw[1], uvw[2], _func);
  solFields->gradf(ele, uvw[0], uvw[1], uvw[2], _gradFunc);
  std::cout << "func = "<< _func <<", gradFunc = ["<< _gradFunc[0] <<","<< _gradFunc[1] <<","<< _gradFunc[2] <<"]"<< std::endl;

  solFields->f(ele, uvw[0], uvw[1], uvw[2], 1, _func_prev);
  solFields->gradf(ele, uvw[0], uvw[1], uvw[2], 1, _gradFunc_prev);
  std::cout << "func_prev = "<< _func_prev <<", gradFunc_prev = ["<< _gradFunc_prev[0] <<","<< _gradFunc_prev[1] <<","<< _gradFunc_prev[2] <<"]"<< std::endl;
  

  std::string _thisGMSHMSHFileName;
  std::string s; std::stringstream out_Thread; out_Thread << thisThreadNum; s = out_Thread.str();
  FILE *fp_gaussPointPosition;
  
  // Writing the gaussPositionPoint corresponding to a given thread
  //===============================================================

  std::string GPPosition = "../output/mesoproblems/computational/gaussPointPosition_";
  GPPosition = GPPosition + s + ".dat";
  fp_gaussPointPosition = fopen(GPPosition.c_str(), "w") ;
  fprintf(fp_gaussPointPosition, "x_gauss = %.16lf ; y_gauss = %.16lf ; \n" , _gaussPoint.x(), _gaussPoint.y() );
  fprintf(fp_gaussPointPosition, "lx = %.16lf ; ly = %.16lf ; \n", this->_lx, this->_ly);
  fprintf(fp_gaussPointPosition, "currentTimeStep = %d ; numEle = %d ; \n", currentTimeStep, ele->getNum());
  fprintf(fp_gaussPointPosition, "exactMicroGeo = %d ; \n", 0);    
  fprintf(fp_gaussPointPosition, "_dtFunc    = %.16lf ; _dtGradFunc_1    = %.16lf; _dtGradFunc_2    = %.16lf; _dtGradFunc_3    = %.16lf; \n", _dtFunc   , _dtGradFunc[0], _dtGradFunc[1], _dtGradFunc[2]);  
  fprintf(fp_gaussPointPosition, "_func      = %.16lf ; _gradFunc_1      = %.16lf; _gradFunc_2      = %.16lf; _gradFunc_3      = %.16lf; \n", _func     , _gradFunc[0]  , _gradFunc[1]  , _gradFunc[2]  );    
  fprintf(fp_gaussPointPosition, "_func_prev = %.16lf ; _gradFunc_prev_1 = %.16lf; _gradFunc_prev_2 = %.16lf; _gradFunc_prev_3 = %.16lf; \n", _func_prev, _gradFunc_prev[0]  , _gradFunc_prev[1]  , _gradFunc_prev[2]  );    
  fclose(fp_gaussPointPosition) ; 
  std::cout << "the coordinates of the current gaussPoint are " << _gaussPoint.x() <<","<< _gaussPoint.y() <<","<< _gaussPoint.z() << " and belong to element " << ele->getNum() << std::endl; 
  std::cout << "the value in gaussPoint is " << _func << " and the gradient is " << _gradFunc.x() <<","<< _gradFunc.y() <<","<< _gradFunc.z() << std::endl; 
  _thisGMSHMSHFileName = "micro_smc";//currentNLML->getGMSHGeoFileName();
  std::string strThisEleNum; std::stringstream out; out << (ele->getNum() ); strThisEleNum = out.str();
  _thisGMSHMSHFileName += "_" + strThisEleNum + ".msh"; 
  _thisGMSHMSHFileName = "../meso/mesoMeshes/"+ _thisGMSHMSHFileName;
  std::cout << "getdp filename is " << this->_GetDPProFileName << std::endl;

  SystemCall("getdp -verbose " + this->_GetDPVerbosity + " " + this->_GetDPProFileName + "-setnumber thisThreadNum " + s +" -msh " + _thisGMSHMSHFileName + " -solve " + this->_GetDPResolutionName + " -pos " + _GetDPPostPro, true);    

  Msg::Warning("Done In computeMaterialLaw. getdpProFileName is %s \n", this->_GetDPProFileName.c_str() );
  double J_x, J_y, J_z, J_x_xp, J_y_xp, J_x_yp, J_y_yp, J_z_zp, E_x, E_y, E_z, E_x_xp, E_x_yp, E_y_xp, E_y_yp, E_z_zp, JouleLosses, HystereticLosses, MagEnergy, TotalEnergy, TotalEnergyDifference;
  
  double num; double x_comp, y_comp, z_comp;
  FILE * OpenHomQty; std::string currentFile;
  std::string eleDotTxt = "_" + strThisEleNum + ".txt";
  
  currentFile = _microProblemFileName+"../output/mesoproblems/computational/j1"+ eleDotTxt; OpenHomQty = fopen(currentFile.c_str(), "r");
  fscanf(OpenHomQty,"%lf %lf %lf %lf", &num, &x_comp, &y_comp, &z_comp); J_x = x_comp; J_y = y_comp; J_z = z_comp; fclose(OpenHomQty);
  currentFile = _microProblemFileName+"../output/mesoproblems/computational/e1"+ eleDotTxt; OpenHomQty = fopen(currentFile.c_str(), "r");
  fscanf(OpenHomQty,"%lf %lf %lf %lf", &num, &x_comp, &y_comp, &z_comp); E_x = x_comp; E_y = y_comp; E_z = z_comp; fclose(OpenHomQty);
  
  bool threeProblems = true;
  
  currentFile = _microProblemFileName+"../output/mesoproblems/computational/j2"+ eleDotTxt; OpenHomQty = fopen(currentFile.c_str(), "r");
  fscanf(OpenHomQty,"%lf %lf %lf %lf", &num, &x_comp, &y_comp, &z_comp); J_x_xp = x_comp; J_y_xp = y_comp; fclose(OpenHomQty);
  currentFile = _microProblemFileName+"../output/mesoproblems/computational/e2"+ eleDotTxt; OpenHomQty = fopen(currentFile.c_str(), "r");
  fscanf(OpenHomQty,"%lf %lf %lf %lf", &num, &x_comp, &y_comp, &z_comp); E_x_xp = x_comp; E_y_xp = y_comp; fclose(OpenHomQty);
  currentFile = _microProblemFileName+"../output/mesoproblems/computational/j3"+ eleDotTxt; OpenHomQty = fopen(currentFile.c_str(), "r");
  fscanf(OpenHomQty,"%lf %lf %lf %lf", &num, &x_comp, &y_comp, &z_comp); J_x_yp = x_comp; J_y_yp = y_comp; fclose(OpenHomQty);
  currentFile = _microProblemFileName+"../output/mesoproblems/computational/e3"+ eleDotTxt; OpenHomQty = fopen(currentFile.c_str(), "r");
  fscanf(OpenHomQty,"%lf %lf %lf %lf", &num, &x_comp, &y_comp, &z_comp); E_x_yp = x_comp; E_y_yp = y_comp; fclose(OpenHomQty);

  //====================================================================================================================================
  currentFile = _microProblemFileName+"../output/mesoproblems/computational/j4"+ eleDotTxt; OpenHomQty = fopen(currentFile.c_str(), "r");
  fscanf(OpenHomQty,"%lf %lf %lf %lf", &num, &x_comp, &y_comp, &z_comp); J_z_zp = z_comp; fclose(OpenHomQty);
  //fscanf(OpenHomQty,"%lf %lf %lf ", &num, &x_comp, &y_comp, &z_comp); J_x_zp = x_comp; J_y_zp = y_comp; J_z_zp = z_comp; fclose(OpenHomQty);
  currentFile = _microProblemFileName+"../output/mesoproblems/computational/e4"+ eleDotTxt; OpenHomQty = fopen(currentFile.c_str(), "r");
  fscanf(OpenHomQty,"%lf %lf %lf %lf", &num, &x_comp, &y_comp, &z_comp); E_z_zp = z_comp; fclose(OpenHomQty);

  Msg::Error("Displaying the components the magnetic field. E_z_zp = %E and E_z = %E.", E_z_zp, E_z);

  //fscanf(OpenHomQty,"%lf %lf %lf ", &num, &x_comp, &y_comp, &z_comp); E_x_yp = x_comp; E_y_yp = y_comp; E_z_zp = z_comp; fclose(OpenHomQty);
  //====================================================================================================================================

  Msg::Warning("Test Test Test. Print after reading H and B \n");
  

  currentFile = _microProblemFileName+"../output/mesoproblems/computational/JL1"+ eleDotTxt; OpenHomQty = fopen(currentFile.c_str(), "r");
  fscanf(OpenHomQty,"%lf %lf %lf", &num, &x_comp, &y_comp); JouleLosses = x_comp; fclose(OpenHomQty);
  
  currentFile = _microProblemFileName+"../output/mesoproblems/computational/MagLosses1"+ eleDotTxt; OpenHomQty = fopen(currentFile.c_str(), "r");
  fscanf(OpenHomQty,"%lf %lf %lf", &num, &x_comp, &y_comp); HystereticLosses = x_comp; fclose(OpenHomQty);
  
  currentFile = _microProblemFileName+"../output/mesoproblems/computational/MagEnergy1"+ eleDotTxt; OpenHomQty = fopen(currentFile.c_str(), "r");
  fscanf(OpenHomQty,"%lf %lf %lf", &num, &x_comp, &y_comp); MagEnergy = x_comp; fclose(OpenHomQty);
  
  currentFile = _microProblemFileName+"../output/mesoproblems/computational/TotalEnergy1"+ eleDotTxt; OpenHomQty = fopen(currentFile.c_str(), "r");
  fscanf(OpenHomQty,"%lf %lf %lf", &num, &x_comp, &y_comp); TotalEnergy = x_comp; fclose(OpenHomQty);
  
  currentFile = _microProblemFileName+"../output/mesoproblems/computational/TotalEnergyDifference1"+ eleDotTxt; OpenHomQty = fopen(currentFile.c_str(), "r");
  fscanf(OpenHomQty,"%lf %lf %lf", &num, &x_comp, &y_comp); TotalEnergyDifference = x_comp; fclose(OpenHomQty);
  
  bool b_conform = true;
  
    if(b_conform) {
      if(threeProblems) {
      std::cout << "b_x_equi = " << E_x   << ", b_x_xp = " << E_x_xp << ", b_y_yp = " << E_y_yp << std::endl;
      std::cout << "h_x_equi = " << J_x   << ", h_x_xp = " << J_x_xp << ", h_y_yp = " << J_y_yp << std::endl;
      std::cout << "h_x_yp = " << J_x_yp << ", h_y_xp = " << J_y_xp << std::endl;
      }
      else {
      std::cout << "b_x_equi = " << std::endl;
      std::cout << "h_x_equi = " << std::endl;
      }
    }
    else {
      if(h_conform) {
        std::cout << "h_x_equi = " << E_x   << ", h_x_xp = " << E_x_xp << ", h_y_yp = " << E_y_yp << std::endl;
        std::cout << "b_x_equi = " << J_x   << ", b_x_xp = " << J_x_xp << ", b_y_yp = " << J_y_yp << std::endl;
        std::cout << "b_x_yp = " << J_x_yp << ", b_y_xp = " << J_y_xp << std::endl;
      }
      else {
        std::cout << "h_x_equi = " << E_x   << ", h_x_xp = " << E_x_xp << ", h_y_yp = " << E_y_yp << std::endl;
        std::cout << "b_x_equi = " << J_x   << ", b_x_xp = " << J_x_xp << ", b_y_yp = " << J_y_yp << std::endl;
        std::cout << "b_x_yp = " << J_x_yp << ", b_y_xp = " << J_y_xp << std::endl;
      }
    }
    Msg::Warning("GetDPExactProfileName = %s \n",this->_GetDPExactProFileName.c_str() );
    Msg::Warning("GetDPProfileName = %s \n",this->_GetDPProFileName.c_str() );
    Msg::Warning(" Mean JouleLosses Gauss point number %d = %g", convertFromPointToInt(ele, _gaussPoint), JouleLosses);

    //==========================================================================================================================
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //==========================================================================================================================
    //if( h_conform && changeDualPrevValue ) {
    //  HomFields.UpdateOldVector();
    //}
    //==========================================================================================================================
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //==========================================================================================================================
    HomFields.setVectorElement(0, J_x);
    HomFields.setVectorElement(1, J_y);
    HomFields.setVectorElement(2, J_z );
    //HomFields.setVectorElement(2, 0.0 );
    HomFields.setMatrixElement(0, 0, ((J_x_xp - J_x)/(E_x_xp - E_x)) );
    HomFields.setMatrixElement(0, 1, ((J_x_yp - J_x)/(E_y_yp - E_y)) );
    HomFields.setMatrixElement(0, 2, 0.0 );
    HomFields.setMatrixElement(1, 0, ((J_y_xp - J_y)/(E_x_xp - E_x)) );
    HomFields.setMatrixElement(1, 1, ((J_y_yp - J_y)/(E_y_yp - E_y)) );
    HomFields.setMatrixElement(1, 2, 0.0 );
    HomFields.setMatrixElement(2, 0, 0.0 );
    HomFields.setMatrixElement(2, 1, 0.0 );
    HomFields.setMatrixElement(2, 2, ((J_z_zp - J_z)/(E_z_zp-E_z)) );
    //HomFields.setMatrixElement(2, 2, 0.0 );
    std::cout << "HomVec = ("   << (*HomFields.getVector())(0) <<","<< (*HomFields.getVector())(1) <<","<< (*HomFields.getVector())(2) << ")" << std::endl;
    Msg::Warning("h_11 = %E, h_11_xp = %E, h_22 = %E, h_22_yp = %E", E_x, E_x_xp, E_y, E_y_yp);
    std::cout << "HomMat[1][1] = "<< HomFields.getMatrix()->get(0, 0) << std::endl;
    std::cout << "HomMat[1][2] = "<< HomFields.getMatrix()->get(0, 1) << std::endl;
    std::cout << "HomMat[2][1] = "<< HomFields.getMatrix()->get(1, 0) << std::endl;
    std::cout << "HomMat[2][2] = "<< HomFields.getMatrix()->get(1, 1) << std::endl;
    std::cout << "HomMat[3][3] = "<< HomFields.getMatrix()->get(2, 2) << std::endl;
    
    if (!h_conform) {
      // Adding Joule losses contribution for current time
      //==================================================
      std::map<MElement *, std::map<int, double> >::iterator itGlobalJL = getMapJL()->getMapJL()->find(ele); 
      if(itGlobalJL != getMapJL()->getMapJL()->end() ) {
        Msg::Warning("In hmmFunction.cpp. You are writting into the map for element %d", itGlobalJL->first->getNum() );
        std::map<int, double>::iterator itJLELE = itGlobalJL->second.find(convertFromPointToInt(ele, _gaussPoint) );
        if( (itJLELE != itGlobalJL->second.end() ) ) {
          Msg::Warning("In hmmFunction.cpp. For element %d, You are writting into the map for Gauss Point %d", itGlobalJL->first->getNum(), itJLELE->first );
          itGlobalJL->second.at(convertFromPointToInt(ele, _gaussPoint) ) = JouleLosses;
        }
        else{
          Msg::Warning("In hmmFunction.cpp. For element %d, You could not write into the map for Gauss Point %d", itGlobalJL->first->getNum(), itJLELE->first );
        }
      }
      else{
        Msg::Warning("In hmmFunction.cpp. You could not write into the map for element %d", itGlobalJL->first->getNum() );
      }
      
      // Adding global quantity contribution for current Gauss point
      //============================================================
      std::string fileNameJouleLosses           = "JouleLosses";
      std::string fileNameMagEnergy             = "MagEnergy";
      std::string fileNameTotalEnergy           = "TotalEnergy";
      std::string fileNameTotalEnergyDifference = "TotalEnergyDifference";
      addValueToMapGQ(fileNameJouleLosses, currentTime, ele, _gaussPoint, JouleLosses);
      addValueToMapGQ(fileNameMagEnergy  , currentTime, ele, _gaussPoint, MagEnergy  );
      addValueToMapGQ(fileNameTotalEnergy, currentTime, ele, _gaussPoint, TotalEnergy);
      addValueToMapGQ(fileNameTotalEnergyDifference, currentTime, ele, _gaussPoint, TotalEnergyDifference);
      
      // Adding Hysteresis losses contribution for current time
      //=======================================================
      addValueToMapHL(currentTime, ele, _gaussPoint, HystereticLosses);
    }
}

void hmmMicroSolverGetDPNonLinearDouble::computeLocalFields( HomQtyLinear &HomFields, int keyForThisPoint, SPoint3 &_gaussPoint, MElement *ele, int thisThreadNum, double currentTime, int currentTimeStep) 
{
  SystemCall("rm -f ~/.gmshrc", true);
  double xyz[3] = {_gaussPoint.x(), _gaussPoint.y(), _gaussPoint.z()};
  double uvw[3] ;
  
  ele->xyz2uvw(xyz, uvw);
  TensorialTraits<double>::ValType _func, _func_prev, _dtFunc; TensorialTraits<double>::GradType _gradFunc, _gradFunc_prev, _dtGradFunc;
  hmmSolverField *solFields = (hmmSolverField *)(_solField);
  solFields->dtF(ele, uvw[0], uvw[1], uvw[2], _dtFunc);
  solFields->dtGradf(ele, uvw[0], uvw[1], uvw[2], _dtGradFunc);
  std::cout << "_dtFunc = "<< _dtFunc <<", _dtgradFunc = ["<< _dtGradFunc[0] <<","<< _dtGradFunc[1] <<","<< _dtGradFunc[2] <<"]"<< std::endl;
  
  solFields->f(ele, uvw[0], uvw[1], uvw[2], _func);
  solFields->gradf(ele, uvw[0], uvw[1], uvw[2], _gradFunc);
  std::cout << "func = "<< _func <<", gradFunc = ["<< _gradFunc[0] <<","<< _gradFunc[1] <<","<< _gradFunc[2] <<"]"<< std::endl;

  solFields->f(ele, uvw[0], uvw[1], uvw[2], 1, _func_prev);
  solFields->gradf(ele, uvw[0], uvw[1], uvw[2], 1, _gradFunc_prev);
  std::cout << "func_prev = "<< _func_prev <<", gradFunc_prev = ["<< _gradFunc_prev[0] <<","<< _gradFunc_prev[1] <<","<< _gradFunc_prev[2] <<"]"<< std::endl;

  std::string s; std::stringstream out_Thread; out_Thread << thisThreadNum; s = out_Thread.str();
  

  FILE *fp_gaussPointPosition;
  std::string GPPosition = "../output/mesoproblems/computational/gaussPointPosition_";
  GPPosition = GPPosition + s + ".dat";
  fp_gaussPointPosition = fopen(GPPosition.c_str(), "w") ;


  fprintf(fp_gaussPointPosition, "x_gauss = %.16lf ; y_gauss = %.16lf ; \n" , _gaussPoint.x(), _gaussPoint.y() );
  fprintf(fp_gaussPointPosition, "lx = %.16lf ; ly = %.16lf ; \n", this->_lx, this->_ly);
  fprintf(fp_gaussPointPosition, "currentTimeStep = %d ; numEle = %d ;\n", currentTimeStep, keyForThisPoint );
  Msg::Warning("GetDPExactProfileName = %s \n",this->_GetDPExactProFileName.c_str() );
  Msg::Warning("GetDPProfileName = %s \n",this->_GetDPProFileName.c_str() );

    fprintf(fp_gaussPointPosition, "exactMicroGeo = %d ; \n", 1);    
    fprintf(fp_gaussPointPosition, "_dtFunc    = %f ; _dtGradFunc_1    = %f; _dtGradFunc_2    = %f; _dtGradFunc_3    = %f; \n", _dtFunc   , _dtGradFunc[0], _dtGradFunc[1], _dtGradFunc[2]);  
    fprintf(fp_gaussPointPosition, "_func      = %f ; _gradFunc_1      = %f; _gradFunc_2      = %f; _gradFunc_3      = %f; \n", _func     , _gradFunc[0]  , _gradFunc[1]  , _gradFunc[2]  );   
    fprintf(fp_gaussPointPosition, "_func_prev = %f ; _gradFunc_prev_1 = %f; _gradFunc_prev_2 = %f; _gradFunc_prev_3 = %f; \n", _func_prev, _gradFunc_prev[0]  , _gradFunc_prev[1]  , _gradFunc_prev[2]  );    
  fclose(fp_gaussPointPosition) ; 
  
  std::cout << "the coordinates of the current gaussPoint are " << _gaussPoint.x() <<","<< _gaussPoint.y() <<","<< _gaussPoint.z() << " and belong to element " << ele->getNum() << std::endl; 
  std::cout << "the value in gaussPoint is " << _func << " and the gradient is " << _gradFunc.x() <<","<< _gradFunc.y() <<","<< _gradFunc.z() << std::endl; 

  std::string _thisGMSHMSHFileName = "micro_smc";//currentNLML->getGMSHGeoFileName();
  std::string strThisEleNum; std::stringstream out; out << keyForThisPoint; strThisEleNum = out.str();
  _thisGMSHMSHFileName += "_" + strThisEleNum + ".msh"; 
  _thisGMSHMSHFileName = "../meso/localMeshes/"+ _thisGMSHMSHFileName;
  SystemCall("getdp -verbose " + this->_GetDPVerbosity + " " + this->_GetDPProFileName + "-setnumber thisThreadNum " + s + " -msh " + _thisGMSHMSHFileName + " -solve " + this->_GetDPResolutionName + " -pos " + _GetDPPostPro, true);
  Msg::Warning("Done in computeLocalFields. GetDPProFileName is %s \n", this->_GetDPProFileName.c_str() );


}

bool hmmMicroSolverGetDPNonLinearDouble::updateMaterialLaw(double currentTime, int currentTimeStep, bool h_conform, bool changeDualPrevValue) {
  // Method used in order to update the nonlinear/hyst ML
  bool update = true;
  std::map<hmmGroupOfDomains *, MLQtyBase *>::iterator itGoDMQB;
  for (itGoDMQB = this->_matLawByGroupOfDom.begin(); itGoDMQB != this->_matLawByGroupOfDom.end(); ++itGoDMQB) {
    Msg::Info("NLML is defined on %d domains.", ( (int)(_matLawByGroupOfDom.size() ) ) );
    //==========================================================================================================================
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //==========================================================================================================================
//     if(h_conform){
//       if(changeDualPrevValue){
        
//       }
//       else
//     }
//     else{
//       update &= updateMaterialLaw( *(itGoDMQB->first), currentTime, currentTimeStep);
//     }
    //==========================================================================================================================
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //==========================================================================================================================
    update &= updateMaterialLaw( *(itGoDMQB->first), currentTime, currentTimeStep, h_conform, changeDualPrevValue);
  }
  return update;
}

bool hmmMicroSolverGetDPNonLinearDouble::updateMaterialLaw(hmmGroupOfDomains &thisGoD, double currentTime, int currentTimeStep, bool h_conform, bool changeDualPrevValue) {
  std::map<hmmGroupOfDomains *, MLQtyBase *>::iterator itGoDMQB = this->_matLawByGroupOfDom.find(&thisGoD);
  if (itGoDMQB !=_matLawByGroupOfDom.end() ) { // check if the nonlinear is defined on this hmmGoD

    // Updating the old values of the dual unknown if(updateOldVec). Used for 
    // keeping trace of unknows from previous time step for a HMM_NONLINEAR_MLAW
    //==========================================================================
    if(changeDualPrevValue) {
      HomQtyNL *thisHQNL = ( (HomQtyNL *)(itGoDMQB->second) );
      thisHQNL->UpdateOldVector();
    }
    std::set<hmmDomainBase *>::const_iterator itSetOfDom;
    for (itSetOfDom = thisGoD.getGroupOfDomains()->begin(); itSetOfDom != thisGoD.getGroupOfDomains()->end(); ++itSetOfDom) {
      groupOfElements *grOfEle = (*itSetOfDom)->getGroupOfElements();
      Msg::Info("NLML is defined on %d elements.", ( (int)(grOfEle->size() ) ) );
      std::set<MElement*>::const_iterator itSetOfEle;

      //====================================================
      // convert a set of elements into a vector of elements
      //====================================================
      std::map<MElement *, std::map<int, HomQtyLinear *> > *thisMapNLMQB = ((HomQtyNL *)(itGoDMQB->second) )->getNLHomQty();
      //================================
      // Creating the vector of elements
      //================================
      std::vector<MElement *> vecEle;
      vecEle.reserve(thisMapNLMQB->size() );      
      for(itSetOfEle = grOfEle->begin(); itSetOfEle != grOfEle->end(); ++itSetOfEle) {
        vecEle.push_back(*(itSetOfEle) );
      }
      //=====================================
      // Loop over the elements of the vector
      //=====================================
      int sizeOfVec = (int)(vecEle.size() );
#pragma omp parallel 
      {
        int thisThreadNum = omp_get_thread_num();
        
#pragma omp for  
        for (int iii = 0; iii < sizeOfVec; iii++) {
          // Loop over all the elements included in the hmmDomainBase
          MElement *ele = vecEle[iii];
          Msg::Info("Element tagged %d.", ele->getNum()  );
          std::map<MElement *, std::map<int, HomQtyLinear *> >::iterator itNLMQB = thisMapNLMQB->find(ele);
          if(itNLMQB != thisMapNLMQB->end() ) { 
            std::map<int, HomQtyLinear *>::const_iterator itIntHom;
            for (itIntHom = itNLMQB->second.begin(); itIntHom != itNLMQB->second.end(); ++itIntHom) {
              SPoint3 *thisGP;
              thisGP = this->convertFromIntToPoint(ele, itIntHom->first);
              this->updateMaterialLaw( *(itIntHom->second), *thisGP, ele, thisThreadNum, currentTime, currentTimeStep, h_conform, changeDualPrevValue);
            }
          }
          else {
            Msg::Error("The nonlinear MQB is not defined on the element tagged %d", ele->getNum() );
          }
        } 
      }
    }
    return true;
  } 
  else {
    Msg::Error("The multiscale nonlinear law tagged %s is not defined on the hmmGroupOfDomains %s", 
               (this->getTag()).c_str(), (thisGoD.getTagGroupOfDomains()).c_str() );
    return false;
  }
}
bool hmmMicroSolverGetDPNonLinearDouble::updateMaterialLaw( HomQtyLinear &HomFields, SPoint3 &_gaussPoint, MElement *ele, int thisThreadNum, double currentTime, int currentTimeStep, bool h_conform, bool changeDualPrevValue) {
  this->computeMaterialLawAtGaussPoint(HomFields, _gaussPoint, ele, thisThreadNum, currentTime, currentTimeStep, h_conform, changeDualPrevValue);
  return true;
}

std::map<int, HomQtyLinear *> hmmMicroSolverGetDPNonLinearDouble::getMaterialLawOnElement(hmmGroupOfDomains &thisGoD, MElement *ele) const {
  // the method returns the MQB for the ML on the element 'ele'.
  std::map<hmmGroupOfDomains *, MLQtyBase *>::const_iterator itGoDMQB = this->_matLawByGroupOfDom.find(&thisGoD);
  if(itGoDMQB != this->_matLawByGroupOfDom.end() ) {
    HomQtyNL *thisMQB = (HomQtyNL *)(itGoDMQB->second);
    std::map<MElement *, std::map<int, HomQtyLinear *> >::iterator itHomQtyNL = thisMQB->getNLHomQty()->find(ele);
    if(itHomQtyNL != thisMQB->getNLHomQty()->end() ) {
      return (itHomQtyNL->second);
    }
    else {
      Msg::Error("Material law on element number %d was not found. Returning a NULL pointer.", ele->getNum() );
      //return NULL;
    }
  }
  else {
    Msg::Error("The material law %s is not defined on the groupOfDomains %s.", (this->getTag()).c_str(), (thisGoD.getTagGroupOfDomains()).c_str() );
  }
}

HomQtyLinear *hmmMicroSolverGetDPNonLinearDouble::getMaterialLawAtGaussPoint(hmmGroupOfDomains &thisGoD, MElement *ele, int numGP) const {
  // the method returns the MQB for the ML at the Gauss point numbered numGP of the element 'ele'.
  std::map<int, HomQtyLinear *> thisEleML = this->getMaterialLawOnElement(thisGoD, ele);
  //if (thisEleML != NULL) {
  std::map<int, HomQtyLinear *>::iterator itEleML = thisEleML.find(numGP);
  if (itEleML != thisEleML.end() ) {
    return itEleML->second;
  }
  else {
    Msg::Error("The Gauss point numbered %d does not belong to element %d. Returning a NULL pointer.", numGP, ele->getNum() );
    return NULL;
  }
}
bool hmmMicroSolverGetDPNonLinearDouble::setMaterialLaw(hmmGroupOfDomains &thisGoD, MLQtyBase &MQB) {
  // the method sets the MQB for the ML define ond on the groupOfDomains 'thisGoD'.
  return this->setMQB(thisGoD, MQB);
}
