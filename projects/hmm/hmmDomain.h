// C++ Interface: hmmDomain
//
// Description: Classes used to describe physical regions and groups of physical regions. 
// A physical region (hmmDomain) contains a pointer to a groupOfElements and a tag number of the region.
// A group of physical regions is simply a set of hmmDomains with a tag number of the group.
//
// Copyright: See COPYING file that comes with this distribution
//
// bugs and problems to the public mailing list <gmsh@geuz.org>.
//

#ifndef _HMMDOMAIN_H_
#define _HMMDOMAIN_H_

#include <stdio.h>
#include <algorithm>
#include "GEntity.h"
#include "GFace.h"
#include "GRegion.h"
#include "groupOfElements.h"


class hmmDomainBase {
protected:
  int _physical;
  groupOfElements *_g;
public:
  // Constructors and destructors
  //=============================
  hmmDomainBase(groupOfElements &g);
  hmmDomainBase(int dim, int physical);
  hmmDomainBase(const hmmDomainBase &other);
  hmmDomainBase(GFace &gFace);
  hmmDomainBase(GRegion &gRegion);
  virtual ~hmmDomainBase();

  // methods
  //========
  inline void setPhysical(int physical) { _physical = physical;}
  inline int getPhysical() const { 
    if(_physical == -1) {
      Msg::Warning("Be careful ! The physical number  = %d and the domain has probably been created from a groupOfElements. \n", _physical);
    }
    return _physical;
  }
  inline void setGroupOfElements( groupOfElements &g) { _g = &g;}
  inline groupOfElements *getGroupOfElements() const { return _g;}
  inline bool elementIsIncluded(MElement *ele) const {
    return this->getGroupOfElements()->find(ele);
  }

  // Definition of operators == and < for cases when hmmDomains can be used as keys in a map
  //========================================================================================
  virtual bool operator == (const hmmDomainBase &other) const { 
    if (!(this->_physical == other._physical)) 
      return false;
    else 
      return true;
  }
  virtual bool operator < (const hmmDomainBase &other) const {
    if (!(this->_physical >= other._physical)) 
      return false;
    else 
      return true;
  }
};

class hmmGroupOfDomains {
protected:
  std::string _tagGroupOfDomains;
  std::set<hmmDomainBase *> _groupOfDomains;
public:
  // 1. Constructors and destructors
  //================================
  hmmGroupOfDomains(std::string tagGroupOfDomains) { _tagGroupOfDomains = tagGroupOfDomains;}
  virtual ~hmmGroupOfDomains(){}
  // 2. methods
  //===========
  inline void addDomain(hmmDomainBase & domain) { 
    // For adding a domain to an existing group of domains
    _groupOfDomains.insert(&domain);}
  inline void addDomain(int dim, int physical) {  
    // For adding a domain to an existing group of domains
    hmmDomainBase *domain = new hmmDomainBase(dim, physical);
    _groupOfDomains.insert(domain);
  }
  inline std::set<hmmDomainBase *>::iterator itBegin() const {
    std::set<hmmDomainBase *>::iterator itHDB = _groupOfDomains.begin();
    return itHDB;
  }
  inline std::set<hmmDomainBase *>::iterator itEnd() const {
    std::set<hmmDomainBase *>::iterator itHDB = _groupOfDomains.end();
    return itHDB;
  }
  inline std::string getTagGroupOfDomains() const { return _tagGroupOfDomains;}
  inline void setGroupOfDomains(std::set<hmmDomainBase *> &groupOfDomains) { _groupOfDomains = groupOfDomains;}
  inline const std::set<hmmDomainBase *> *getGroupOfDomains() const { return  &_groupOfDomains;}
  bool operator == (const hmmGroupOfDomains &other) const {
    if (_tagGroupOfDomains == other.getTagGroupOfDomains()) return true;
    else return false;
  }
  bool isIncluded(const hmmGroupOfDomains &other);
  bool elementIsIncluded(MElement *ele) const;
  hmmDomainBase *getHmmDomainBase(int physical) const;
  inline bool isHmmDomainIncluded(int physical) const {
    return ( getHmmDomainBase(physical) != NULL);
  }
};

#endif //_HMMDOMAIN_H_
