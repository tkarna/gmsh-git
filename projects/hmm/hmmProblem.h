// C++ Interface: hmmProblem
//
// Description:
//
// Copyright: See COPYING file that comes with this distribution
//
// bugs and problems to the public mailing list <gmsh@geuz.org>.
//

#ifndef _HMMPROBLEM_H_
#define _HMMPROBLEM_H_

#include <stdio.h>
#include <map>
#include <set>
#include <vector>
#include <complex>
#include <string>
#include <iostream>
#include <iterator>
#include "GModel.h"
#include "PView.h"
#include "PViewData.h"
#include "functionSpace.h"
#include "quadratureRules.h"
#include "NonLinearDofManager.h"
#include "MultiscaleNonLinearSystem.h"
#include "SVector3.h"
#include "SPoint3.h"
#include "hmmDomain.h"
#include "hmmFunction.h"
#include "hmmConstraints.h"
#include "hmmFunctionSpace.h"
#include "hmmFormulation.h"
#include "hmmResolution.h"
#include "hmmPostProcessing.h"


class hmmProblemBase{
protected:
  GModel *_model;
  NonLinearDofManager<double> *_assembler;
  MultiscaleNonLinearSystemGmm<double> *_linsys;
  std::map<std::string, GaussQuadrature *> allQuadratureRules;
  std::map<std::string, hmmGroupOfDomains *> allDomains;
  std::map<std::string, hmmMaterialLawBase *> allMaterialLaws; // FixMe
  std::map<std::string, hmmDirichlet *> allDirichletBC; // FixMe
  std::map<std::string, hmmScalarLagrangeFunctionSpace *> allFunctionSpaces; // FixMe
  std::map<std::string, hmmFormulation *> allFormulations;
  std::map<std::string, hmmResolutionBase *> allResolutions;
  std::map<std::string, hmmPostProBase *> allPostPro;
  int _dim, _problemType, _nx, _ny;
  int _maxNumIter, _numIter, _GmresValue;
  double _lx, _ly, _tol;

public:
  hmmProblemBase();
  virtual ~hmmProblemBase();
  inline void setProblemType(int problemType=1) { _problemType = problemType;}
  inline int getProblemType() const { return _problemType;}
  inline void setMaxNumIter(int maxNumIter) { _maxNumIter = maxNumIter;}
  inline int getMaxNumIter() const { return _maxNumIter;}
  inline void setNumIter(int numIter=-1) { _numIter = numIter ;}
  inline int getNumIter() const { return _numIter;}
  inline void setTolerance(double tol=1e-12) { _tol = tol;}
  inline double getTolerance() const { return  _tol;}
  inline void setDim(int dim) { _dim = dim;}
  inline int getDim() const{ return _dim;}
  inline void setNX(int nx) { _nx = nx;}
  inline int getNX() const { return _nx;}
  inline void setNY(int ny) { _ny = ny;}
  inline int getNY() const { return _ny;}
  inline void setLX(double lx) { _lx = lx;}
  inline double getLX() const { return _lx;}
  inline void setLY(double ly) { _ly = ly;}
  inline double getLY() const { return _ly;}
  inline void setGmresValue(int GmresValue=0) {_GmresValue = GmresValue; }

  // 1. setters and getters for  all objects needed (geometric model, system, dofManager,maps of all domains, 
  // material laws, constraints, function spaces, formulation, resolutions and postprocessings...)
  //=========================================================================================
  inline void setModel(GModel *pModel) { _model = pModel; _dim = _model->getNumRegions() ? 3 : 2; }
  void setMesh(const std::string &meshFileName);
  inline GModel *getModel() const { return _model;}
  inline void setSystem(MultiscaleNonLinearSystemGmm<double> &linsys){ _linsys= &linsys;};
  inline MultiscaleNonLinearSystemGmm<double> *getSystem() const { return _linsys;}
  inline NonLinearDofManager<double> *getNonLinearDofManager() const { return _assembler;}
  inline std::map<std::string, hmmGroupOfDomains *> getAllDomains() const { return allDomains;}
  inline std::map<std::string, hmmMaterialLawBase *> getAllMaterialLaws() const { return allMaterialLaws;} // FixMe
  inline std::map<std::string, hmmDirichlet *> getAllDiri() const { return allDirichletBC;} // FixMe
  inline std::map<std::string, hmmScalarLagrangeFunctionSpace *> getAllFunctionSpaces() const { return allFunctionSpaces;} // FixMe
  inline std::map<std::string, hmmFormulation *> getAllFormulations() const { return allFormulations;}
  inline std::map<std::string, hmmResolutionBase *> getAllResolution() const { return allResolutions;}
  inline std::map<std::string, hmmPostProBase *> getPostPro() const { return allPostPro;}

  // 2. hmmGroupOfDomains
  //=====================
  void getDomainsInfo() const;
  void addGroupOfDomains(std::string tagGroupOfDomain);
  void addDomainToGroupOfDomains(std::string tagGroupOfDomain, int dim, int physical);
  hmmGroupOfDomains *getGroupOfDomains(std::string tagGroupOfDomain);

  // 3. hmmFunction
  //===============
  void getFunctionInfo() const;

  // Functions for adding material laws with a group of domains "hmmGroupOfDomains". First check if the group of domains 
  // exists (the string "hmmGroupOfDomainsTag"). If so, check if the material law exists (the tag "tagMaterialLaw"). If 
  // the material law exists, check if it is not defined on the current group of domains. If not, add the domain to the 
  // group of domains of the material law. If the material law is already defined on the domain, print a warning message.  
  void add1ScaleLinearMaterialLaw(std::string tagMaterialLaw, std::string hmmGroupOfDomainsTag, 
                                  double mat11, double mat12, double mat13, 
                                  double mat21, double mat22, double mat23, 
                                  double mat31, double mat32, double mat33 );
  void add1ScaleLinearMaterialLawInno(std::string tagMaterialLaw, std::string hmmGroupOfDomainsTag, 
                                  double mat11, double mat12, double mat13, 
                                  double mat21, double mat22, double mat23, 
                                  double mat31, double mat32, double mat33 );
  void changeMQB(std::string tagMaterialLaw, std::string tagGroupOfDomains,
                 double mat11, double mat12, double mat13, 
                 double mat21, double mat22, double mat23, 
                 double mat31, double mat32, double mat33);
//   void add2ScaleLinearMaterialLaw(std::string tagMaterialLaw, std::string hmmGroupOfDomainsTag, std::string GMSHVerbosity, 
//                                   std::string GetDPGeoFileName, std::string GetDPVerbosity, std::string GetDPProFileName, 
//                                   std::string GetDPResolutionName, std::string GetDPPostPro, SPoint3 &computPoint); //FIX ME
//   void add2ScaleLinearMaterialLawInno(std::string tagMaterialLaw, std::string hmmGroupOfDomainsTag, std::string GMSHVerbosity, 
//                                   std::string GetDPGeoFileName, std::string GetDPVerbosity, std::string GetDPProFileName, 
//                                   std::string GetDPResolutionName, std::string GetDPPostPro, SPoint3 &computPoint); //FIX ME
  void add2ScaleNonLinearMaterialLaw(std::string tagMaterialLaw, std::string hmmGroupOfDomainsTag,std::string tagSpace, std::string tagQuad, 
                                     std::string GMSHVerbosity, std::string GMSHGeoFileName, std::string GetDPVerbosity, 
                                     std::string GetDPProFileName, std::string GetDPResolutionName, std::string GetDPPostPro, 
                                     std::string GMSHExactGeoFileName = "",  std::string GMSHExactProFileName = "", std::string microProblemFileName = ""); //FixMe
  void add2ScaleNonLinearMaterialLawInno(std::string tagMaterialLaw, std::string hmmGroupOfDomainsTag,std::string tagSpace, std::string tagQuad, 
                                     std::string GMSHVerbosity, std::string GMSHGeoFileName, std::string GetDPVerbosity, 
                                     std::string GetDPProFileName, std::string GetDPResolutionName, std::string GetDPPostPro, 
                                     std::string GMSHExactGeoFileName = "",  std::string GMSHExactProFileName = "", std::string microProblemFileName = ""); //FixMe
  
  // Initialization of the nonlinear material laws
//   void Init2ScaleNonLinearML(std::string tagMaterialLaw, double mat11, double mat12, double mat13, double mat21, 
//                              double mat22, double mat23, double mat31, double mat32, double mat33 ); //FixMe
  void Init2ScaleNonLinearMLInno(std::string tagMaterialLaw, std::string tagGroupOfDomains, double mat11, double mat12, double mat13, double mat21, 
                                 double mat22, double mat23, double mat31, double mat32, double mat33 ); //FixMe
  void Init2ScaleNonLinearMap(std::string tagMaterialLaw, std::string tagGroupOfDomains, double JL = 0.0);
  void Init2ScaleNonLinearHLMap(std::string tagQuad, std::string tagMaterialLaw, std::string tagGoD, double initTime, double HL = 0.0);
  void Init2ScaleNonLinearGQMap(std::string tagQuad, std::string tagMaterialLaw, std::string tagGoD);
  void addGQTo2ScaleNonLinearGQMap(std::string tagGQ, std::string tagMaterialLaw);
  void InitGQMap(std::string tagQuad, std::string tagMaterialLaw, double initTime, double HL = 0.0);

  void computeGQOverTime(std::string tagNameGQ, std::string tagMaterialLaw, std::string fileName) {
    std::map<std::string, hmmMaterialLawBase *>::const_iterator itML =  allMaterialLaws.find(tagMaterialLaw);
    if (itML != allMaterialLaws.end() ) {
      hmmMicroSolverGetDPNonLinearDouble *thisNLML = (hmmMicroSolverGetDPNonLinearDouble *) (itML->second);
      thisNLML->integrateGQ(tagNameGQ, fileName);
    }
    else {
      Msg::Error("Done computing the global quantity for a material law that does not exist. \n");
    }
    return;
  }

//   void computeGQOverTime(std::string tagMaterialLaw, std::string fileName) {
//     std::map<std::string, hmmMaterialLawBase *>::const_iterator itML =  allMaterialLaws.find(tagMaterialLaw);
//     if (itML != allMaterialLaws.end() ) {
//       hmmMicroSolverGetDPNonLinearDouble *thisNLML = (hmmMicroSolverGetDPNonLinearDouble *) (itML->second);
//       HomQtyGlobalQuantities *thisGQ = thisNLML->getMapGQ();
//       std::map<std::string, HomQtyHystereticLosses* > *thisGQMap = thisGQ->getMapGQ();
//       std::map<std::string, HomQtyHystereticLosses* >::iterator itGQMap;
//       for(itGQMap = thisGQMap->begin(); itGQMap != thisGQMap->end(); ++itGQMap) {
//         thisNLML->integrateGQ(itGQMap->first, fileName);
//       }
//     }
//     else {
//       Msg::Error("Done computing the global quantity for a material law that does not exist. \n");
//     }
//   }

  hmmMaterialLawBase *getMaterialLaw(std::string tagML) const; //FixMe
  void printMaterialLaw(std::string tagMaterialLaw, std::string tagGoD) const;
  void printMaterialLaw(std::string tagMaterialLaw) const;

  std::string convertInt2Str(int number)
  {
    std::stringstream ss;//create a stringstream
    ss << number;//add number to the stream
    return ss.str();//return a string with the contents of the stream
  }
  
  // 4. hmmConstraints
  //==================
  void getConstraintsInfo() const;
  void addDirichletBC(std::string tagGroupOfDomain, std::string tagCons, int comp, double imposedValue);
  hmmDirichlet *getDirichletBC(std::string tagCons) const; //FixMe
  
  // 5. Quadratures
  //==================
  void getQuadInfo() const;
  void addQuad(std::string tagQuad); // FixMe
  GaussQuadrature *getQuad(std::string tagQuad) const;
  
  // 6. hmmFunctionSpace
  //====================
  void getFunctionSpaceInfo() const;
  void addFunctionSpace(std::string tagFS, std::string tagDom, int iField, 
                        FSType funcSpacType, BFType BasisFuncType);
  //=============================================================================================================
  void addDomainToFunctionSpace(std::string tagFS, std::string tagDom) {
    //std::map<hmmGroupOfDomains>
  }
  //=============================================================================================================
  void addConstraintToFunctionSpace(std::string tagFS, std::string tagCons);
  hmmScalarLagrangeFunctionSpace *getFunctionSpace(std::string tagFS) const; //FixMe
  
  // 7. hmmFormulation
  //==================
  void getFormulationInfo() const;
  void addFormulation(std::string Form);
  void addBilinearTermToFormulation(std::string tagForm, std::string tagSpace, std::string tagMaterialLaw, 
                                    std::string tagDom, BType whatType, std::string tagQuad); // FixMe lots of unnecessary terms
  void addLinearTermToFormulation(std::string tagForm, std::string tagSpace, std::string tagDom, 
                                  std::string tagML, std::string tagQuad);
  void addSourceTermToFormulation(std::string tagForm, std::string tagSpace, std::string tagDom, 
                                  std::string tagQuad, double js_x, double js_y, double js_z);// FixMe: Valid only for constant source terms.  
  void addSourceTermToFormulation(std::string tagForm, std::string tagSpace, std::string tagDom, 
                                  std::string tagQuad, double js_x, double js_y, double js_z, int tag);// FixMe: Valid only for constant source terms.
  
  // 8. hmmResolution
  //=================
  void getResolutionInfo() const;
  void setLinsysAndDofManager();
  void addSolver(std::string resolutionName, ResType resoType);
  void addParameters(std::string tagRes, double _initTime, double _endTime, double _dTime, double _theta, double _freq);
  void addOp2Solver(std::string resolutionName, std::string tagForm, OpType operation, hmmPostProBase &postPro); //comment
  //void addPostPro2Solver(std::string tagPostPro);
  
  // 9. hmmPostPro
  //==============
  void getPostProInfo() const;
  void addPostPro(std::string tagPostPro); // FixMe formulation and the resolution should be enough
  hmmPostProBase *getPostPro(std::string tagPostPro) const; // FixMe formulation and the resolution should be enough
  //void addPostQuantity(std::string tagPostPro, PostQuantityName postQty); // FixMe use addTerm to be able to add any term to a postQty
  //void addTerm(); // add this function instead of using a postquantity with terms known in advance
  //void getPostOp(std::string tagPostPro, PostQuantityName postQty, std::string postOpName); //FixMe 

  void addPostProcessing(std::string potentialFileName, std::string primalDerivedFieldFileName, std::string dualDerivedFieldFileName, 
                         std::string tagSpace, std::string tagML, std::string tagPostPro, int T2I ); // FixMe 
  void addPostProcessingTime(std::string potentialFileName, std::string primalDerivedFieldFileName, std::string dualDerivedFieldFileName, 
                             std::string eddyCurrentFieldFileName, std::string tagSpace, std::string tagML, std::string tagReso, std::string tagPostPro, int T2I); // FixMe
  void computeLinkageFluxDensity(std::string tagPostPro, std::string tagGoD, std::string tagSpace, std::string tagForm, 
                                 std::string tagResoType, std::string tagQuad, int tagLinearSourceTerm);
  void computeTotalHysteresisLossesOverCycle(std::string tagMaterialLaw, double initTime, double dtime, double freq, int cycleNum = 1) {
    std::map<std::string, hmmMaterialLawBase *>::iterator itMatLaw = allMaterialLaws.find(tagMaterialLaw);
    if(itMatLaw != allMaterialLaws.end() ) {
      hmmMicroSolverGetDPNonLinearDouble *thisML = (hmmMicroSolverGetDPNonLinearDouble *)(itMatLaw->second);
      thisML->computeTotalHysteresisLossesOverCycle(initTime, dtime, freq, cycleNum);
    }
    else {
      Msg::Error("Done computing the total hysteretic losses. You can not compute hysteresis losses for the material law tagged %s. \n", tagMaterialLaw.c_str() );
    }
  }
  void addPoint2MapOfPoints(std::string tagML, std::string tagPostPro, int intPt, double x, double y, double z);
  void initMapOfPoints(std::string tagPostPro);
};
#endif //_HMMPROBLEM_H_

