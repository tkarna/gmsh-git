#include <math.h>
#include <stdlib.h> 
#include "hmmResolution.h"
#include "hmmSolverAlgorithms.h"   
#include "hmmPostProcessing.h"



#define Pi 3.141592653589793

hmmResolutionBase::hmmResolutionBase(std::string tagRes, MultiscaleNonLinearSystemGmm<double> &linsys, 
                                     NonLinearDofManager<double> &pAssembler, 
                                     std::map<std::string, GaussQuadrature *> &allQuadratureRules, 
                                     std::map<std::string, hmmDirichlet *> &allDirichletBC, 
                                     std::map<std::string, hmmMaterialLawBase *> &allMaterialLaws,
                                     std::map<std::string, hmmScalarLagrangeFunctionSpace *> &allFunctionSpaces, 
                                     std::map<std::string, hmmFormulation *> &allFormulations, 
                                     int maxNumIter, int numIter, double tol, ResType resoType) {
  _tagRes             = tagRes;
  _resoType           = resoType;
  _linsys             = &linsys;
  _pAssembler         = &pAssembler;
  _allQuadratureRules = &allQuadratureRules;
  _allDirichletBC     = &allDirichletBC;
  _allMaterialLaws     = &allMaterialLaws;
  _allFunctionSpaces  = &allFunctionSpaces;
  _allFormulations    = &allFormulations;
  _maxNumIter = maxNumIter; _numIter = numIter; _tol = tol;
}
void hmmResolutionBase::writeResidu(std::string residuTxt, double residu, int timestep) const {
  FILE *fp;
  if( (this->_numIter == 0) && (timestep == 0)) {
    fp = fopen(residuTxt.c_str(), "w");
    fprintf(fp, "%d %d %.16g \n", timestep,  0, residu);
    fclose(fp);
  }
  else {
    fp = fopen(residuTxt.c_str(), "append"); 
    fprintf(fp, "%d %d %.16g \n", timestep, this->_numIter, residu);            
    fclose(fp);                                      
  }
}
void hmmResolutionBase::printMatrix() const {
  printf("Matrix for iteration %d is :\n", this->_numIter+1); 
  for(int i = 0; i < this->_pAssembler->sizeOfR(); i++) {
    for(int j = 0; j < this->_pAssembler->sizeOfR(); j++) {
      double d = 0.; this->_linsys->getFromMatrix(i, j,d);
      std::cout<< "A["<< i <<"]["<< j <<"] = "<< d <<std::endl;
    }
  }
}
void hmmResolutionBase::printRHS() const {
  printf("RHS for iteration %d is :\n", this->_numIter+1); 
  for(int j = 0; j < this->_pAssembler->sizeOfR(); j++) {
    double d = 0.; this->_linsys->getFromRightHandSide(j,d);
    std::cout<< d <<std::endl;
  }
}
void hmmResolutionBase::printSolution() const {
  printf("The solution for iteration %d is:\n", this->_numIter+1); 
  for(int j=0; j < this->_pAssembler->sizeOfR(); j++) {
    double d = 0.; this->_linsys->getFromSolution(j, d);
    std::cout<< d <<std::endl;
  }
}
void hmmResolutionBase::fixDirichletBC(std::string tagSpace) {
  FilterDofTrivial filter;
  std::map<std::string, hmmScalarLagrangeFunctionSpace *>::iterator itSpace = _allFunctionSpaces->find(tagSpace);
  if (itSpace != _allFunctionSpaces->end() ) {
    std::map<std::string, hmmDirichlet *>::const_iterator itDiriBC;
    for (itDiriBC = itSpace->second->getConstraint()->begin(); itDiriBC != itSpace->second->getConstraint()->end(); ++itDiriBC) {
      std::set<hmmDomainBase *>::const_iterator itDom;
      for(itDom = itDiriBC->second->getAllDomains()->getGroupOfDomains()->begin(); 
          itDom != itDiriBC->second->getAllDomains()->getGroupOfDomains()->end(); ++itDom) {
        FixNodalDofs( *(itSpace->second), (*itDom)->getGroupOfElements()->begin(), 
                      (*itDom)->getGroupOfElements()->end(), *_pAssembler, *(itDiriBC->second->getSimpleFunction()), filter);
      }
    }
  }
  else Msg::Error("The selected function space does not exist. \n");
  return;
}
void hmmResolutionBase::fixAllDirichletBC() {
  std::map<std::string, hmmScalarLagrangeFunctionSpace *>::iterator itSpace;
  for (itSpace = _allFunctionSpaces->begin(); itSpace != _allFunctionSpaces->end(); ++itSpace) 
    fixDirichletBC(itSpace->first);
}
void hmmResolutionBase::numberDofs(std::string tagSpace) {
  std::map<std::string, hmmScalarLagrangeFunctionSpace *>::iterator itSpace = _allFunctionSpaces->find(tagSpace);
  if (itSpace != _allFunctionSpaces->end() ) {
    std::set<hmmDomainBase *>::const_iterator itDom;
    for (itDom = itSpace->second->getGroupOfDomains()->getGroupOfDomains()->begin(); 
         itDom != itSpace->second->getGroupOfDomains()->getGroupOfDomains()->end(); ++itDom) {
      NumberDofs( *(itSpace->second), (*itDom)->getGroupOfElements()->begin(), 
                  (*itDom)->getGroupOfElements()->end(), *_pAssembler);
    }
  }
  else Msg::Error("The selected function space does not exist. \n");
}
void hmmResolutionBase::numberAllDofs() {
  std::map<std::string, hmmScalarLagrangeFunctionSpace *>::iterator itSpace;
  for (itSpace = _allFunctionSpaces->begin(); itSpace != _allFunctionSpaces->end(); itSpace++) {
    this->numberDofs(itSpace->first);
  }
}
void hmmResolutionBase::assembleBilinearTerms(std::string tagForm) {
  std::map<std::string, hmmFormulation *>::iterator itForm = _allFormulations->find(tagForm);
  if (itForm != _allFormulations->end() ) {
    std::vector<hmmBilinearTerm *>::const_iterator itBilinearTerm;
    for (itBilinearTerm = itForm->second->getVectorOfBilinearTerms()->begin(); 
         itBilinearTerm != itForm->second->getVectorOfBilinearTerms()->end(); ++itBilinearTerm ) { 
      std::set<hmmDomainBase *>::const_iterator itDom;
      for (itDom = (*itBilinearTerm)->getGroupOfDomains()->getGroupOfDomains()->begin(); 
           itDom != (*itBilinearTerm)->getGroupOfDomains()->getGroupOfDomains()->end();++itDom ) {
        if( (*itBilinearTerm)->getType() == GBF ) {
          hmmAssembleMatrixAndRHS_Lin( *(*itBilinearTerm), *( (*itBilinearTerm)->getFunctionSpace1() ), 
                                       (*itDom)->getGroupOfElements()->begin(), (*itDom)->getGroupOfElements()->end(),
                                       *_pAssembler);
        }
      }
    }
  }
  else Msg::Error("The selected formulation does not exist. \n");
}
void hmmResolutionBase::assembleBilinearTermsJac(std::string tagForm) {
  std::map<std::string, hmmFormulation *>::iterator itForm = _allFormulations->find(tagForm);
  if (itForm != _allFormulations->end() ) {
      std::vector<hmmBilinearTerm *>::const_iterator itBilinearTerm;
      for (itBilinearTerm = itForm->second->getVectorOfBilinearTerms()->begin(); 
           itBilinearTerm != itForm->second->getVectorOfBilinearTerms()->end(); ++itBilinearTerm ) {
        std::set<hmmDomainBase *>::const_iterator itDom;
        for (itDom = (*itBilinearTerm)->getGroupOfDomains()->getGroupOfDomains()->begin(); 
             itDom != (*itBilinearTerm)->getGroupOfDomains()->getGroupOfDomains()->end();++itDom ) {
          if( (*itBilinearTerm)->getType() == GBF ) {
            hmmAssembleMatrixAndRHS_NLin( *(*itBilinearTerm), *( (*itBilinearTerm)->getFunctionSpace1() ), 
                                          (*itDom)->getGroupOfElements()->begin(), (*itDom)->getGroupOfElements()->end(),
                                          *_pAssembler);
          }
        }
      }
  }
  else Msg::Error("The selected formulation does not exist. \n");
}

void hmmResolutionBase::assembleLinearTerms(std::string tagForm, double freq , double currentTime) {
  std::map<std::string, hmmFormulation *>::iterator  itForm = _allFormulations->find(tagForm);
  if (itForm != _allFormulations->end() ) {
    std::vector<hmmLinearTermBase *>::const_iterator itLinearTerm;
    for (itLinearTerm = itForm->second->getVectorOfLinearTerms()->begin(); 
         itLinearTerm != itForm->second->getVectorOfLinearTerms()->end(); ++itLinearTerm ) {
      std::set<hmmDomainBase *>::const_iterator itDom;
      for (itDom = (*itLinearTerm)->getGroupOfDomains()->getGroupOfDomains()->begin(); 
           itDom != (*itLinearTerm)->getGroupOfDomains()->getGroupOfDomains()->end();++itDom ) {
        hmmAssembleLinearTerm( *(*itLinearTerm), *( (*itLinearTerm)->getFunctionSpace1() ), 
                               (*itDom)->getGroupOfElements()->begin(), (*itDom)->getGroupOfElements()->end(),
                               *_pAssembler, 0.0, 0.0);
      }
    }
  }
  else Msg::Error("The selected formulation does not exist. \n");
}
void hmmResolutionBase::assembleSourceLinearTerms(std::string tagForm, double freq, double currentTime) { // FixMe
  std::map<std::string, hmmFormulation *>::iterator itForm = _allFormulations->find(tagForm);
  if (itForm != _allFormulations->end() ) {
    std::vector<hmmLinearTermBase *>::const_iterator itLinearTermBase;
    for(itLinearTermBase = itForm->second->getVectorOfLinearTerms()->begin(); 
        itLinearTermBase != itForm->second->getVectorOfLinearTerms()->end(); ++itLinearTermBase ) {
      std::set<hmmDomainBase *>::const_iterator itDom;
      for(itDom = (*itLinearTermBase)->getGroupOfDomains()->getGroupOfDomains()->begin(); 
          itDom != (*itLinearTermBase)->getGroupOfDomains()->getGroupOfDomains()->end();++itDom ) {
        Msg::Info("Frequency is equal to %g. \n", freq);
        hmmAssembleLinearTerm( *(*itLinearTermBase), *( (*itLinearTermBase)->getFunctionSpace1() ), 
                               (*itDom)->getGroupOfElements()->begin(), (*itDom)->getGroupOfElements()->end(),
                               *_pAssembler, freq, currentTime);
      }
    }
  }
  else Msg::Error("The selected formulation does not exist. \n");
}
void hmmResolutionBase::InitSolution(std::string tagForm) {
  Msg::Info("-- Fixing boundary conditions for the function spaces of the formulation %s... \n", tagForm.c_str());
  fixAllDirichletBC();
  Msg::Info("-- Finished fixing boundary conditions ! \n");
  Msg::Info("-- Numbering all the dofs... \n");
  numberAllDofs();
  Msg::Info("-- Finished numbering all the dofs ! \n");
  _linsys->setInitialValues(_pAssembler->sizeOfR(), 0.0);
}
void hmmResolutionBase::InitSolutionLinear(std::string tagForm) {
  Msg::Info("-- Fixing boundary conditions for the function spaces of the formulation %s... \n", tagForm.c_str());
  fixAllDirichletBC();
  Msg::Info("-- Finished fixing boundary conditions ! \n");
  Msg::Info("-- Numbering all the dofs... \n");
  numberAllDofs();
  Msg::Info("-- Finished numbering all the dofs ! \n");
  Msg::Info("-- Done assembling the matrix and the right hand side... \n");
  assembleBilinearTerms(tagForm);
  assembleSourceLinearTerms(tagForm, 0.0, 0.0); 
  // Msg::Info("Komera cane chef ......");
  Msg::Info("-- Finished assembling the matrix and the right hand side ! \n");
}
void hmmResolutionBase::UpdateMatLaw(std::string tagForm) {
  std::map<std::string, hmmFormulation *>::iterator itForm = _allFormulations->find(tagForm);
  std::vector<hmmBilinearTerm *>::const_iterator itBTerms;
  for (itBTerms = itForm->second->getVectorOfBilinearTerms()->begin(); 
       itBTerms != itForm->second->getVectorOfBilinearTerms()->end(); ++itBTerms) {
    if ((*itBTerms)->getMaterialLaw()->getType() == HMM_NONLINEAR_MLAW) { // Fix me each ML appearing many time in a form should not be updated once
      hmmMicroSolverGetDPNonLinearDouble *currentNLML = (hmmMicroSolverGetDPNonLinearDouble *) ( (*itBTerms)->getMaterialLaw() );
      currentNLML->updateMaterialLaw(0.0, 1);   
    }
  }
}
void hmmResolutionBase::Generate(std::string tagForm) {
  Msg::Info("-- Done assembling the matrix and the right hand side... \n");
  assembleBilinearTerms(tagForm);
  //assembleLinearTerms(tagForm, 0.0, 0.0);
  assembleSourceLinearTerms(tagForm, 0.0, 0.0);
  Msg::Info("-- Finished assembling the matrix and the right hand side ! \n");
}
void hmmResolutionBase::GenerateJac(std::string tagForm) {
  Msg::Info("-- Done assembling the matrix and the right hand side... \n");
  assembleBilinearTermsJac(tagForm);
  //assembleLinearTerms(tagForm, 0.0, 0.0);
  assembleSourceLinearTerms(tagForm, 0.0, 0.0);
  Msg::Info("-- Finished assembling the matrix and the right hand side ! \n");
}
void hmmResolutionBase::Solve(std::string tagForm, hmmPostProBase &postPro) {
  if ( _linsys != NULL) {
    Msg::Info("-- Done solving for the %d th iteration! \n", _numIter);
    this->_linsys->systemSolve();
    Msg::Info("-- Finished solving for the %d th iteration! \n", _numIter);
    _linsys->zeroMatrix(); _linsys->zeroRightHandSide();  
  }
  else {
    Msg::Error("Done solving, the linear system is not defined.");   
  }
}
void hmmResolutionBase::SolveJac(std::string tagForm, hmmPostProBase &postPro) {
  double residu = 0.0, relError = 10 * this->_tol;
  for (_numIter = 0; _numIter < _maxNumIter; _numIter++) {
    if (_linsys != NULL) {
      if (relError > _tol) {
        if (this->_numIter < this->_maxNumIter) {
          this->Solve(tagForm, postPro);
          if (_numIter == 0) 
            _residu0 = _linsys->normInfRightHandSide();
          residu = _linsys->normInfRightHandSide();
          relError = (_residu0 == 0.0) ? residu : residu/_residu0;
          writeResidu("../output/macroproblem/Residu.txt", relError);                                      
          Msg::Info("-- the residu is equal to %f \n", relError); 
          _linsys->zeroMatrix(); _linsys->zeroRightHandSide(); 
        }
        else {
          Msg::Info("The number of iterations is not enough. It has been doubled. \n");
          setMaxNumIter(2 * _maxNumIter);
        }
      }
      else {
        Msg::Info("The problem has converged and the residu is equal %f to \n", residu);
        _lastIteration = _numIter;
        _numIter = _maxNumIter;
      }
    }
    else {
      Msg::Info("Done solving... The linear system has not been defined. \n");
    }
  }
}
void hmmResolutionBase::setOperation(std::string tagForm, OpType thisOp, hmmPostProBase &postPro) {
  if (thisOp == InitSol) {
    this->InitSolution(tagForm);
  } 
  else if (thisOp == InitSolLin) {
    this->InitSolutionLinear(tagForm);
  } 
  else if (thisOp == UpMatLaw) {
    this->InitSolution(tagForm);
  } 
  else if (thisOp == Gen) {
    this->Generate(tagForm);
  } 
  else if (thisOp == GenJac) {
    this->GenerateJac(tagForm);
  } 
  else if (thisOp == Sol) {
    this->Solve(tagForm, postPro);
  } 
  else if (thisOp == SolJac) {
    this->SolveJac(tagForm, postPro);
  } 
  else if (thisOp == SaveSol) {
    this->SaveSolution(tagForm);
  } 
  else
    Msg::Error("This operation has not been defined. \n");
}
void hmmResolutionBase::addPostPro(std::string potentialFileName,  std::string primalDerivedFieldFileName, 
                                   std::string dualDerivedFieldFileName,  std::string eddyCurrentFieldFileName,
                                   std::string tagML, std::string tagSpace,  hmmPostProBase &_postPro) {
  int T2I = 100000000 + _numIter;
  _postPro.buildPotentialView(tagSpace, potentialFileName, T2I, "../output/macroproblem/a_View_");
  _postPro.buildPrimalDerivedFieldView(tagSpace, primalDerivedFieldFileName, T2I, "../output/macroproblem/b_View_");
  //_postPro.buildDualDerivedFieldView_hM(tagSpace, tagML, dualDerivedFieldFileName, T2I, "../output/macroproblem/h_View_hM_");
  //_postPro.buildDualDerivedFieldView_tot(tagSpace, tagML, dualDerivedFieldFileName, T2I, "../output/macroproblem/h_View_tot_");
  //_postPro.buildEddyCurrentView(tagSpace, eddyCurrentFieldFileName, T2I, "../output/macroproblem/j_View_", 6e2);
}
void hmmResolutionTimeDomain::updateDirichletBC(std::string tagSpace, std::string tagDiri, double ValueToImpose) {
  std::map<std::string, hmmScalarLagrangeFunctionSpace *>::iterator itSpace = _allFunctionSpaces->find(tagSpace);
  std::map<std::string, hmmDirichlet *>::const_iterator itDiriToUpdate = itSpace->second->getConstraint()->find(tagDiri);
  itDiriToUpdate->second->setSimpleFunction(ValueToImpose);
}
void hmmResolutionTimeDomain::updateDirichletBC(std::string tagSpace, std::map<std::string, double> diriToUpdate) {
  std::map<std::string, double>::iterator itStrDouble;
  for(itStrDouble = diriToUpdate.begin(); itStrDouble != diriToUpdate.end(); ++itStrDouble) {
    updateDirichletBC(tagSpace, itStrDouble->first, itStrDouble->second);
  }
}
void  hmmResolutionTimeDomain::printAllSolution() const {
  std::map<double, int>::const_iterator itD2I;
  for (itD2I = this->_pAssembler->getTime2Int()->begin(); itD2I != this->_pAssembler->getTime2Int()->end(); ++itD2I ) {
    printf("The solution for timestep number %d  ie timestep %f is:\n", itD2I->second, itD2I->first); 
    std::set<Dof>::const_iterator itDof;
    for(itDof = this->_pAssembler->getSetOfDofs()->begin(); itDof != this->_pAssembler->getSetOfDofs()->end(); ++itDof) {
      double d = 0.; this->_pAssembler->getDofValue( (*itDof), itD2I->first, d);
      std::cout<< " Solution at node " << (*itDof).getEntity() <<" for timestep " << itD2I->first << " is :" << d <<std::endl;
    }
  }
}
void hmmResolutionTimeDomain::assembleBilinearTermsJacDt(std::string tagForm) {
  if (_dTime != 0.0) {
    double invDTime = 1./_dTime;
    std::map<std::string, hmmFormulation *>::iterator itForm = _allFormulations->find(tagForm);
    std::vector<hmmBilinearTerm *>::const_iterator itBilinearTerm;
    for (itBilinearTerm = itForm->second->getVectorOfBilinearTerms()->begin(); 
         itBilinearTerm != itForm->second->getVectorOfBilinearTerms()->end(); ++itBilinearTerm ) {
      std::set<hmmDomainBase *>::const_iterator itDom;
      for (itDom = (*itBilinearTerm)->getGroupOfDomains()->getGroupOfDomains()->begin(); 
           itDom != (*itBilinearTerm)->getGroupOfDomains()->getGroupOfDomains()->end();++itDom ) {
        if( (*itBilinearTerm)->getType() == SBF ) {
          hmmAssembleMatrixAndRHS_Dt( *(*itBilinearTerm), *( (*itBilinearTerm)->getFunctionSpace1() ), (*itDom)->getGroupOfElements()->begin(), (*itDom)->getGroupOfElements()->end(),*_pAssembler, invDTime);          
        }   
      }
    }
  }
  else
    Msg::Error("Error. dTime = infinity ! ");
}
void hmmResolutionTimeDomain::assembleBilinearTermsJacDt_H_Form(std::string tagForm) {
  if (_dTime != 0.0) {
    double invDTime = 1./_dTime;
    std::map<std::string, hmmFormulation *>::iterator itForm = _allFormulations->find(tagForm);
    std::vector<hmmBilinearTerm *>::const_iterator itBilinearTerm;
    for (itBilinearTerm = itForm->second->getVectorOfBilinearTerms()->begin(); 
         itBilinearTerm != itForm->second->getVectorOfBilinearTerms()->end(); ++itBilinearTerm ) {
      std::set<hmmDomainBase *>::const_iterator itDom;
      for (itDom = (*itBilinearTerm)->getGroupOfDomains()->getGroupOfDomains()->begin(); 
           itDom != (*itBilinearTerm)->getGroupOfDomains()->getGroupOfDomains()->end();++itDom ) {
        if( (*itBilinearTerm)->getType() == SBF ) {
          hmmAssembleMatrixAndRHS_Dt_H_Form( *(*itBilinearTerm), *( (*itBilinearTerm)->getFunctionSpace1() ), (*itDom)->getGroupOfElements()->begin(), (*itDom)->getGroupOfElements()->end(),*_pAssembler, invDTime);          
        }   
      }
    }
  }
  else
    Msg::Error("Error. dTime = infinity ! ");
}
void hmmResolutionTimeDomain::InitSolutionLinear(std::string tagForm) {  
  Msg::Info("-- Start Initiliazing the solution by solving a linear system... \n");
  Msg::Info("-- Fixing boundary conditions for the function spaces of the formulation %s... \n", tagForm.c_str());
  fixAllDirichletBC();
  Msg::Info("-- Finished fixing boundary conditions ! \n");
  Msg::Info("-- Numbering all the dofs... \n");
  numberAllDofs();

  Msg::Info("There are %d dofs", _pAssembler->sizeOfR() );

  Msg::Info("-- Finished numbering all the dofs ! \n");
  _pAssembler->init(_allFunctionSpaces, _initTime);
  Msg::Info("-- Done assembling the matrix and the right hand side... \n");
  this->assembleBilinearTerms(tagForm);
  this->assembleSourceLinearTerms(tagForm, _freq, _currentTime);
  //this->assembleBilinearTermsJacDt(tagForm); 
  Msg::Info("-- Finished assembling the matrix and the right hand side ! \n");
  Msg::Info("-- End Initiliazing the solution by solving a linear system... \n");
}

void hmmResolutionTimeDomain::InitSolutionLinear_H_Form(std::string tagForm) {  
  Msg::Info("-- Start Initiliazing the solution by solving a linear system... \n");
  Msg::Info("-- Fixing boundary conditions for the function spaces of the formulation %s... \n", tagForm.c_str());
  fixAllDirichletBC();
  Msg::Info("-- Finished fixing boundary conditions ! \n");
  Msg::Info("-- Numbering all the dofs... \n");
  numberAllDofs();

  Msg::Info("There are %d dofs", _pAssembler->sizeOfR() );
  Msg::Info("There are %d fixed dofs", _pAssembler->sizeOfF() );

  Msg::Info("-- Finished numbering all the dofs ! \n");
  _pAssembler->init(_allFunctionSpaces, _initTime);
  Msg::Info("-- Done assembling the matrix and the right hand side... \n");
  this->assembleBilinearTerms(tagForm);
  this->assembleBilinearTermsJacDt_H_Form(tagForm); 
  Msg::Info("-- Finished assembling the matrix and the right hand side ! \n");
  Msg::Info("-- End Initiliazing the solution by solving a linear system... \n");
}

void hmmResolutionTimeDomain::UpdateMatLaw(std::string tagForm) {

  Msg::Info("-- Start updating the hmm nonlinear material law... \n");
  std::string delPosFiles1 = "";//" rm -f ~/Users/inno/src/gmsh/projects/hmm/benchmark/laminations/output/microproblems/*_cs"; //FixMe
  std::string delPosFiles2 = "";//" rm -f ~/Users/inno/src/gmsh/projects/hmm/benchmark/computational_homogenization/laminations/output/mesoproblems/computational/*_s"; //FixMe

  std::string s ; std::stringstream out ;
  std::string s1; std::stringstream out1;
  if(_currentTimeStep >= 2) {
    out << _currentTimeStep-2; s = out.str();
    delPosFiles1 += s; delPosFiles1 += "_gp*.pos";
    SystemCall(delPosFiles1, true);
  }
  if(_currentTimeStep) {
    out1 << _currentTimeStep  ; s1 = out1.str();
    delPosFiles2 += s1; delPosFiles2 += ".pos";
    SystemCall(delPosFiles2, true);
  }
  std::map<std::string, hmmFormulation *>::iterator itForm = _allFormulations->find(tagForm);
  std::vector<hmmBilinearTerm *>::const_iterator itBTerms;
  for (itBTerms = itForm->second->getVectorOfBilinearTerms()->begin(); 
       itBTerms != itForm->second->getVectorOfBilinearTerms()->end(); ++itBTerms) {
    if ((*itBTerms)->getMaterialLaw()->getType() == HMM_NONLINEAR_MLAW) {  // Fix me each ML appearing many time in a form should not be updated once
      hmmMicroSolverGetDPNonLinearDouble *currentNLML = (hmmMicroSolverGetDPNonLinearDouble *) ( (*itBTerms)->getMaterialLaw() );
      //currentNLML->computeMaterialLaw(_currentTimeStep, delPosFiles1, delPosFiles2);
      currentNLML->updateMaterialLaw(_currentTime, _currentTimeStep);
    }
  }
  Msg::Info("-- End updating the hmm nonlinear material law... \n");
}







void hmmResolutionTimeDomain::UpdateMatLaw_H_Form(std::string tagForm, bool changedDualPrevValue) {

  Msg::Info("-- Start updating the hmm nonlinear material law... \n");
  std::string delPosFiles1 = "";//" rm -f ~/Users/inno/src/gmsh/projects/hmm/benchmark/laminations/output/microproblems/*_cs"; //FixMe
  std::string delPosFiles2 = "";//" rm -f ~/Users/inno/src/gmsh/projects/hmm/benchmark/computational_homogenization/laminations/output/mesoproblems/computational/*_s"; //FixMe

  std::string s ; std::stringstream out ;
  std::string s1; std::stringstream out1;
  if(_currentTimeStep >= 2) {
    out << _currentTimeStep-2; s = out.str();
    delPosFiles1 += s; delPosFiles1 += "_gp*.pos";
    SystemCall(delPosFiles1, true);
  }
  if(_currentTimeStep) {
    out1 << _currentTimeStep  ; s1 = out1.str();
    delPosFiles2 += s1; delPosFiles2 += ".pos";
    SystemCall(delPosFiles2, true);
  }
  std::map<std::string, hmmFormulation *>::iterator itForm = _allFormulations->find(tagForm);
  std::vector<hmmBilinearTerm *>::const_iterator itBTerms;
  for (itBTerms = itForm->second->getVectorOfBilinearTerms()->begin(); 
       itBTerms != itForm->second->getVectorOfBilinearTerms()->end(); ++itBTerms) {
    if ((*itBTerms)->getMaterialLaw()->getType() == HMM_NONLINEAR_MLAW) {  // Fix me each ML appearing many time in a form should not be updated once
      hmmMicroSolverGetDPNonLinearDouble *currentNLML = (hmmMicroSolverGetDPNonLinearDouble *) ( (*itBTerms)->getMaterialLaw() );
      //currentNLML->computeMaterialLaw(_currentTimeStep, delPosFiles1, delPosFiles2);
      currentNLML->updateMaterialLaw(_currentTime, _currentTimeStep, true, changedDualPrevValue);
    }
  }
  Msg::Info("-- End updating the hmm nonlinear material law... \n");
}











void hmmResolutionTimeDomain::Generate(std::string tagForm) {
  Msg::Info("-- Start generating the system... \n");
  Msg::Info("-- Done assembling the matrix and the right hand side... \n");
  assembleBilinearTerms(tagForm);
  //assembleLinearTerms(tagForm, _freq, _currentTime);
  assembleSourceLinearTerms(tagForm, _freq, _currentTime);
  assembleBilinearTermsJacDt(tagForm); 
  Msg::Info("-- Finished assembling the matrix and the right hand side ! \n");
  Msg::Info("-- End generating the system... \n");
}
void hmmResolutionTimeDomain::GenerateJac(std::string tagForm) {
  Msg::Info("-- Start generating the system - Newton Raphson scheme... \n");
  Msg::Info("-- Done assembling the matrix and the right hand side... \n");
  assembleBilinearTermsJac(tagForm);
  //assembleLinearTerms(tagForm, _freq, _currentTime);
  assembleSourceLinearTerms(tagForm, _freq, _currentTime);
  assembleBilinearTermsJacDt(tagForm); 
  Msg::Info("-- Finished assembling the matrix and the right hand side ! \n");
  Msg::Info("-- End generating the system - Newton Raphson scheme... \n");
}
void hmmResolutionTimeDomain::GenerateJac_H_Form(std::string tagForm) {
  Msg::Info("-- Start generating the system - Newton Raphson scheme... \n");
  Msg::Info("-- Done assembling the matrix and the right hand side... \n");
  assembleBilinearTermsJac(tagForm);
  assembleBilinearTermsJacDt_H_Form(tagForm); 
  Msg::Info("-- Finished assembling the matrix and the right hand side ! \n");
  Msg::Info("-- End generating the system - Newton Raphson scheme... \n");
}
void hmmResolutionTimeDomain::Solve(std::string tagForm, hmmPostProBase &postPro) {
  Msg::Info("-- Start solving... \n");
  Msg::Info("-- Done solving for the %d th iteration! \n", _numIter);
  //this->_linsys->systemSolveTot();
  this->_linsys->systemSolve();
  Msg::Info("-- Finished solving for the %d th iteration! \n", _numIter);
  postPro.buildPotentialView("BF_Perp", "a_", 0, "../output/macroproblem/a_CurrentTimeStep_"); // ??????????????????????
  Msg::Info("-- End solving... \n");
}
void hmmResolutionTimeDomain::Solve_H_Form(std::string tagForm, hmmPostProBase &postPro) {
  Msg::Info("-- Start solving... \n");
  Msg::Info("-- Done solving for the %d th iteration! \n", _numIter);
   this->_linsys->systemSolve();
  Msg::Info("-- Finished solving for the %d th iteration! \n", _numIter);
  Msg::Info("-- End solving... \n");
}
void hmmResolutionTimeDomain::SolveJac(std::string tagForm, hmmPostProBase &postPro) {
  Msg::Info("-- Start solving - Newton Raphson scheme... \n");
  //============================================
  std::string tagGroup_Cond  = "Conductor";
  std::string tagGroup_Air   = "Air";
  std::string tagGroup_Indu1 = "Inductor1";
  std::string tagGroup_Indu2 = "Inductor2";
  std::string tagGroup_InduR = "Inductor_R";
  std::string tagGroup_InduL = "Inductor_L";
 
  std::string tagNLML_hb     = "_hb_NLMatLaw";
  std::string tagLML_hb      = "_hb_MatLaw";
  std::string tagLML_sigma   = "_sigma_MatLaw";
  
  std::string tagQuad        = "Grad";
  std::string tagSpace       = "BF_Perp";
  std::string tagForm1       = "a_Hyst_ML";
  std::string tagRes1        = "a_Hyst_Res";
  std::string tagPostPro1 = "a_Hyst_Post";
  
  //========================================================= 
  for (_currentTime = _initTime; _currentTime <= (_endTime); _currentTime += _dTime) {
    if (_currentTime == _initTime)
      _currentTimeStep = 0;
    else
      _currentTimeStep++;
    Msg::Info("-- The current time step is %d and the current time is %f", _currentTimeStep, _currentTime); 
    double residu = 0.0, relError = 10 * _tol;
    _numIter = 0;
    if (_currentTimeStep == 0) {
      this->GenerateJac(tagForm);
      this->Solve(tagForm, postPro);
      _linsys->zeroMatrix(); _linsys->zeroRightHandSide();    
    }

    if(_currentTimeStep >= 2) {
      std::string twoStepBackward; std::stringstream out; out << (_currentTimeStep - 2); twoStepBackward = out.str();
        SystemCall("rm -f ../output/mesoproblems/computational/*_p1_cs" + twoStepBackward + "_gp*.pos", true);
    }

    for (_numIter = 0; _numIter < _maxNumIter; ++_numIter) {
      std::map<std::string, hmmMaterialLawBase *>::iterator itML =  _allMaterialLaws->find("_hb_NLMatLaw");//??????

      if (relError > _tol) {
        Msg::Info("The problem has not converged yet and the residu is equal to %f. \n", residu);
        if (this->_numIter < this->_maxNumIter) {
          this->UpdateMatLaw(tagForm);
          this->GenerateJac(tagForm);
	  this->Solve(tagForm, postPro);          
          if (_numIter == 0) 
            this->_residu0 = _linsys->normInfRightHandSide();
          residu = _linsys->normInfRightHandSide();
          relError = (this->_residu0 == 0.0) ? residu : residu/this->_residu0;
          writeResidu("../output/macroproblem/Residu.txt", relError, _currentTimeStep); // FixMe                            
          Msg::Info("-- the residu is equal to %f \n", relError); 
          _linsys->zeroMatrix(); _linsys->zeroRightHandSide();     
        }
        else {
          Msg::Info("The number of iterations was not enough for convergence. It has been doubled. \n");
          setMaxNumIter(2 * _maxNumIter);
        }
        // Do some post pro for the local fields
        //======================================



        Msg::Info("Start computing local fields for next computations... \n");
        postPro.buildPotentialView("BF_Perp", "a_", _currentTimeStep, "../output/macroproblem/a_");
        postPro.buildPrimalDerivedFieldView("BF_Perp", "b_", _currentTimeStep, "../output/macroproblem/b_");
        postPro.buildDualDerivedFieldView_HMM("BF_Perp", "_hb_NLMatLaw", "h_HMM_", _currentTimeStep, "../output/macroproblem/h_HMM_");
        postPro.buildDualDerivedFieldView("BF_Perp", "_hb_MatLaw", "h_", _currentTimeStep, "../output/macroproblem/h_");
        postPro.buildElectricCurrentView("BF_Perp", "a_Hyst_Res", "e_", _currentTimeStep, "../output/macroproblem/e_"); 
        postPro.buildEddyCurrentView("BF_Perp", "_sigma_MatLaw", "a_Hyst_Res", "j_", _currentTimeStep, "../output/macroproblem/j_"); 

        // Calculation of magnetic energy and total energy
        //===================================================================================================================================  
        double macroMagEnergy    = 0.0;
        double macroMagEnergy_NL = 0.0;
        double macroMagEnergy_L  = 0.0;
        
        std::string fileName_TotalEnergy      = "../output/macroproblem/MacroMagEnergy.dat"     ;
        std::string fileName_TotalEnergy_Lin  = "../output/macroproblem/MacroMagEnergy_Lin.dat" ;
        std::string fileName_TotalEnergy_NLin = "../output/macroproblem/MacroMagEnergy_NLin.dat";
        macroMagEnergy_NL = postPro.buildMagEnergyView_a_v_formulation(macroMagEnergy, tagNLML_hb, tagQuad, 
                                                                       tagSpace, tagGroup_Cond,_dTime);
        postPro.buildMagEnergyView_a_v_formulation(macroMagEnergy, tagLML_hb, tagQuad, tagSpace, tagGroup_Air,_dTime);
        postPro.buildMagEnergyView_a_v_formulation(macroMagEnergy, tagLML_hb, tagQuad, tagSpace, tagGroup_Indu1,_dTime);
        postPro.buildMagEnergyView_a_v_formulation(macroMagEnergy, tagLML_hb, tagQuad, tagSpace, tagGroup_Indu2,_dTime);
        postPro.buildMagEnergyView_a_v_formulation(macroMagEnergy, tagLML_hb, tagQuad, tagSpace, tagGroup_InduR,_dTime);
        postPro.buildMagEnergyView_a_v_formulation(macroMagEnergy, tagLML_hb, tagQuad, tagSpace, tagGroup_InduL,_dTime, 
                                                   _currentTimeStep, _currentTime, fileName_TotalEnergy.c_str(), true);
        macroMagEnergy_L = macroMagEnergy - macroMagEnergy_NL;

        FILE *fp;
        fp = fopen(fileName_TotalEnergy_Lin.c_str() , "a") ;
        fprintf(fp, "%d %g %g \n" , _currentTimeStep, _currentTime, macroMagEnergy_L);
        fclose(fp) ;
        fp = fopen(fileName_TotalEnergy_NLin.c_str() , "a") ;
        fprintf(fp, "%d %g %g \n" , _currentTimeStep, _currentTime, macroMagEnergy_NL);
        fclose(fp) ;
        //=================================================================================================================================
        
        Msg::Info("End computing local fields for next computations... \n");

        // Do some post pro for global quantities
        //=======================================
        std::map<std::string, hmmMaterialLawBase *>::iterator itML =  _allMaterialLaws->find("_hb_NLMatLaw"); //??????
        if (itML != _allMaterialLaws->end() ) {
          // Computing Joule losses for this time step
          //==========================================
          hmmMicroSolverGetDPNonLinearDouble *currentML = static_cast<hmmMicroSolverGetDPNonLinearDouble *>(itML->second);
          currentML->computeHomJL(_currentTimeStep, _currentTime);
          
          // Writing the map of losses and hyst losses
          //==========================================
          HomQtyJouleLosses *JL      = currentML->getMapJL();
          std::map<MElement*, std::map<int, double> > thisTimeMap_JL = *(JL->getMapJL() );
          std::map<int, std::vector<double> > data_JL;
          currentML->convertFromGaussPointDataToElementData(thisTimeMap_JL, data_JL, false);
          postPro.buildViewFromMap(data_JL, "JL_", _currentTimeStep, "../output/macroproblem/JL_", "ElementData");
          
          HomQtyHystereticLosses *HL = currentML->getMapHL();
          int isIteratorFound;
          std::map<MElement*, std::map<int, double> > thisTimeMap_HL = HL->getIteratorCurrentTime(_currentTime, isIteratorFound)->second;
          if (isIteratorFound) {
            // Compute the total HL for current timestep
            //==========================================
            double totalHL_TS = currentML->integRealQtyAtGaussPoints(thisTimeMap_HL, "JouleLosses", _currentTimeStep, _currentTime); 
            Msg::Info("Done in hmmResolution.cpp. The total hysteresis losses for timestep %d is %f. \n", _currentTimeStep, totalHL_TS);

            // Write the map of hysteresis losses for current timestep
            //========================================================            
            std::map<int, std::vector<double> > data_HL;
            currentML->convertFromGaussPointDataToElementData(thisTimeMap_HL, data_HL, false);
            postPro.buildViewFromMap(data_HL, "HL_", _currentTimeStep, "../output/macroproblem/HL_", "ElementData");
          }
          
          double magEnergy = 0.0;
          double totalEnergy = 0.0;
          
          std::string nameGQJouleLosses              = "JouleLosses";
          std::string nameGQMagEnergy                = "MagEnergy";
          std::string nameGQTotalEnergy              = "TotalEnergy";
          std::string nameGQTotalEnergyDifference    = "TotalEnergyDifference";

          std::string nameOfQtyJouleLosses           = "JouleLosses_DuringComp";
          std::string nameOfQtyMagEnergy             = "MagEnergy_DuringComp";
          std::string nameOfQtyTotalEnergy           = "TotalEnergy_DuringComp";
          std::string nameOfQtyTotalEnergyDifference = "TotalEnergyDifference_DuringComp";

          currentML->integrateGQOneTimeStep(nameGQJouleLosses, (nameOfQtyJouleLosses + "_Hom"), _currentTimeStep, _currentTime);
          magEnergy   = currentML->integrateGQOneTimeStep(nameGQMagEnergy, (nameOfQtyMagEnergy + "_Hom"), _currentTimeStep, _currentTime);
          totalEnergy = currentML->integrateGQOneTimeStep(nameGQTotalEnergy, (nameOfQtyTotalEnergy + "_Hom"), _currentTimeStep, _currentTime);
          currentML->integrateGQOneTimeStep(nameGQTotalEnergyDifference, (nameOfQtyTotalEnergyDifference + "_Hom"), _currentTimeStep, _currentTime);
          
          std::string MagFileName = "../output/macroproblem/" + nameOfQtyMagEnergy + "_Tot.dat";
          std::string TotFileName = "../output/macroproblem/" + nameOfQtyTotalEnergy + "_Tot.dat";
          
          postPro.buildMagEnergyView_a_v_formulation(magEnergy, tagLML_hb, tagQuad, tagSpace, tagGroup_Air  ,_dTime);
          postPro.buildMagEnergyView_a_v_formulation(magEnergy, tagLML_hb, tagQuad, tagSpace, tagGroup_Indu1,_dTime);
          postPro.buildMagEnergyView_a_v_formulation(magEnergy, tagLML_hb, tagQuad, tagSpace, tagGroup_Indu2,_dTime);
          postPro.buildMagEnergyView_a_v_formulation(magEnergy, tagLML_hb, tagQuad, tagSpace, tagGroup_InduR,_dTime);
          postPro.buildMagEnergyView_a_v_formulation(magEnergy, tagLML_hb, tagQuad, tagSpace, tagGroup_InduL,_dTime, 
                                                     _currentTimeStep, _currentTime, MagFileName.c_str(), true);
          
          postPro.buildMagEnergyView_a_v_formulation(totalEnergy, tagLML_hb, tagQuad, tagSpace, tagGroup_Air  ,_dTime);
          postPro.buildMagEnergyView_a_v_formulation(totalEnergy, tagLML_hb, tagQuad, tagSpace, tagGroup_Indu1,_dTime);
          postPro.buildMagEnergyView_a_v_formulation(totalEnergy, tagLML_hb, tagQuad, tagSpace, tagGroup_Indu2,_dTime);
          postPro.buildMagEnergyView_a_v_formulation(totalEnergy, tagLML_hb, tagQuad, tagSpace, tagGroup_InduR,_dTime);
          postPro.buildMagEnergyView_a_v_formulation(totalEnergy, tagLML_hb, tagQuad, tagSpace, tagGroup_InduL,_dTime, 
                                                     _currentTimeStep, _currentTime, TotFileName.c_str(), true);
        }
        else {
          Msg::Error("Done computing Joule losses in hmmResolutionTimeDomain. The materialLaw does not exist."); 
        }
        
        // do some local field computations
        //=================================
        Msg::Info("Start computing local fields... \n");
        std::map<std::string, hmmFormulation *>::iterator itForm = _allFormulations->find(tagForm);
        std::vector<hmmBilinearTerm *>::const_iterator itBTerms;
        for (itBTerms = itForm->second->getVectorOfBilinearTerms()->begin(); 
             itBTerms != itForm->second->getVectorOfBilinearTerms()->end(); ++itBTerms) {
          if ((*itBTerms)->getMaterialLaw()->getType() == HMM_NONLINEAR_MLAW) {  // Fix me each ML appearing many time in a form should not be updated once
            hmmMicroSolverGetDPNonLinearDouble *currentNLML = (hmmMicroSolverGetDPNonLinearDouble *) ( (*itBTerms)->getMaterialLaw() );
	    Msg::Warning("One step before updating...");
            postPro.computeLocalFields( currentNLML->getTag() , this->getTagRes() );
	    Msg::Warning("One step after updating...");
          }
        }
        Msg::Info("End computing local fields... \n");
      }
    }
    _pAssembler->addSolution(_currentTime);
  }
  Msg::Info("-- End solving - Newton Raphson scheme... \n");
}


void hmmResolutionTimeDomain::SolveJac_H_Form(std::string tagForm, hmmPostProBase &postPro) 
{
  Msg::Info("-- Start solving - Newton Raphson scheme... \n");
  //============================================
  std::string tagGroup_Cond  = "Conductor";
  std::string tagGroup_Air   = "Air";
  std::string tagGroup_Indu  = "Inductor";
 
  std::string tagNLML_hb     = "_bh_NLMatLaw";
  std::string tagLML_hb      = "_bh_MatLaw";
  std::string tagLML_sigma   = "_rho_MatLaw";
  
  std::string tagCons        = "diriInf";
  std::string tagQuad        = "Grad";
  std::string tagSpace       = "BF_Perp";
  std::string tagForm1       = "h_NL_Form";
  std::string tagRes1        = "h_NL_Res";
  std::string tagPostPro1    = "h_NL_Post";
  
  //========================================================= 
  bool changedDualPrevValue = false; 
  for (_currentTime = _initTime; _currentTime <= (_endTime); _currentTime += _dTime) {
    if (_currentTime != _initTime) {
      changedDualPrevValue = true; 
    }
    std::map<std::string, hmmDirichlet *>::const_iterator itCons = _allDirichletBC->find(tagCons);
    if (itCons != _allDirichletBC->end() ) {
      Msg::Info("You are going to update the value of the Dirichlet boundary condition. \n");
    }
    else {
      Msg::Error("The selected Dirichlet boundary condition does not exist. \n");
      exit(EXIT_FAILURE);
    }
    itCons->second->setNewDirichletBC(sin (2 * pi * _freq * _currentTime) );
    this->fixAllDirichletBC();
    
    if (_currentTime == _initTime) {
      _currentTimeStep = 0;
    }
    else {
      _currentTimeStep++;
    }
    Msg::Info("-- The current time step is %d and the current time is %f", _currentTimeStep, _currentTime); 
    double residu = 0.0, relError = 10 * _tol;
    _numIter = 0;
    if (_currentTimeStep == 0) {
      this->GenerateJac_H_Form(tagForm);
      this->Solve_H_Form(tagForm, postPro);
      _linsys->zeroMatrix(); _linsys->zeroRightHandSide();    
    }

    if(_currentTimeStep >= 2) {
      std::string twoStepBackward; std::stringstream out; out << (_currentTimeStep - 2); twoStepBackward = out.str();
        SystemCall("rm -f ../output/mesoproblems/computational/*_p1_cs" + twoStepBackward + "_gp*.pos", true);
    }

    for (_numIter = 0; _numIter < _maxNumIter; ++_numIter) {

      if (_numIter != 0) {
        changedDualPrevValue = false;
      }
      std::map<std::string, hmmMaterialLawBase *>::iterator itML =  _allMaterialLaws->find("_hb_NLMatLaw");//??????

      if (relError > _tol) {
        Msg::Info("Timestep = %d, number iteration = %d. The problem has not converged yet and the residu is equal to %f. \n", 
                  _currentTimeStep, _numIter, residu);
        if (this->_numIter >= this->_maxNumIter) {
          Msg::Info("The number of nonlinear iterations is not enough for convergence. It is multiplied by two. \n");
          setMaxNumIter(2 * _maxNumIter);
        }
        if (this->_numIter < this->_maxNumIter) {
          this->UpdateMatLaw_H_Form(tagForm, changedDualPrevValue);
          this->GenerateJac_H_Form(tagForm);
	  this->Solve_H_Form(tagForm, postPro);          
          if (_numIter == 0) 
            this->_residu0 = _linsys->normInfRightHandSide();
          residu = _linsys->normInfRightHandSide();
          relError = (this->_residu0 == 0.0) ? residu : residu/this->_residu0;
          writeResidu("../output/macroproblem/Residu.txt", relError, _currentTimeStep); // FixMe                            
          Msg::Info("-- the residu is equal to %f \n", relError); 
          _linsys->zeroMatrix(); _linsys->zeroRightHandSide();     
        }
        else{}

        // Do some postpro for the local fields
        //=====================================
        Msg::Info("Start computing local fields for next computations... \n");
        postPro.buildPotentialView("BF_Perp", "h_", _currentTimeStep, "../output/macroproblem/h_"); 
        postPro.buildPrimalDerivedFieldView("BF_Perp", "j_", _currentTimeStep, "../output/macroproblem/j_"); 
        postPro.buildDualDerivedFieldView_HMM("BF_Perp", "_bh_NLMatLaw", "b_HMM_", _currentTimeStep, "../output/macroproblem/b_HMM_"); 
        postPro.buildDualDerivedFieldView("BF_Perp", "_bh_MatLaw", "b_", _currentTimeStep, "../output/macroproblem/b_"); 
        //postPro.buildElectricFieldView("BF_Perp", "_rho_MatLaw", "h_NL_Res", "e_", _currentTimeStep, "../output/macroproblem/e_"); 
        Msg::Info("End computing local fields for next computations... \n");
        
      } // end if loop for the relative error 
      else {
        Msg::Info("The problem has converged and the residu is equal %f to \n", residu);
        _lastIteration = _numIter;
        _numIter       = _maxNumIter;

        // Do some post pro for the local fields
        //==================================
        Msg::Info("Start computing local fields for next computations... \n");
        postPro.buildPotentialView("BF_Perp", "h_", _currentTimeStep, "../output/macroproblem/h_");
        postPro.buildPrimalDerivedFieldView("BF_Perp", "j_", _currentTimeStep, "../output/macroproblem/j_");
        postPro.buildDualDerivedFieldView_HMM("BF_Perp", "_bh_NLMatLaw", "b_HMM_", _currentTimeStep, "../output/macroproblem/b_HMM_");
        postPro.buildDualDerivedFieldView("BF_Perp", "_bh_MatLaw", "b_", _currentTimeStep, "../output/macroproblem/b_"); 
        //postPro.buildElectricFieldView("BF_Perp", "_rho_MatLaw", "h_NL_Res", "e_", _currentTimeStep, "../output/macroproblem/e_"); 
        Msg::Info("End computing local fields for next computations... \n");
      }
      // do some local field computations
      //=================================
      Msg::Info("Start computing local fields... \n");
      std::map<std::string, hmmFormulation *>::iterator itForm = _allFormulations->find(tagForm);
      std::vector<hmmBilinearTerm *>::const_iterator itBTerms;
      for (itBTerms = itForm->second->getVectorOfBilinearTerms()->begin(); 
           itBTerms != itForm->second->getVectorOfBilinearTerms()->end(); ++itBTerms) {
        if ((*itBTerms)->getMaterialLaw()->getType() == HMM_NONLINEAR_MLAW) {  // Fix me each ML appearing many time in a form should not be updated once
          hmmMicroSolverGetDPNonLinearDouble *currentNLML = (hmmMicroSolverGetDPNonLinearDouble *) ( (*itBTerms)->getMaterialLaw() );
          Msg::Warning("One step before updating...");
          postPro.computeLocalFields( currentNLML->getTag() , this->getTagRes() );
          Msg::Warning("One step after updating...");
        }
      }
      Msg::Info("End computing local fields... \n");
    }
    _pAssembler->addSolution(_currentTime);
  }
  Msg::Info("-- End solving - Newton Raphson scheme... \n");
}

void hmmResolutionTimeDomain::setOperation(std::string tagForm, OpType thisOp, hmmPostProBase &postPro) {
  if (thisOp == SolJac_H_Form) {
    Msg::Info("Solving using SolveJac_H_Form");
    this->SolveJac_H_Form(tagForm, postPro);
  }
  else if (thisOp == InitSolLin_H_Form) {
    this->InitSolutionLinear_H_Form(tagForm);
  }
  else {
    this->hmmResolutionBase::setOperation(tagForm, thisOp, postPro);
  }
}
