#include "hmmFunctionSpace.h"

void hmmScalarLagrangeFunctionSpace::setConstraint(std::string tagDiri, double imposedValue) { 
  std::map<std::string, hmmDirichlet *>::iterator itDiri = _diriFS.find(tagDiri);
  if (itDiri != _diriFS.end() ) 
    itDiri->second->setSimpleFunction(imposedValue);
  else Msg::Error("The Dirichlet boundary condition you are adding to the function space does not exist. \n");
}
void hmmBFPerpendicularEdge::df(MElement *ele, double u, double v, double w, std::vector<TensorialTraits<double>::GradType> &curls)
{
  if(ele->getParent()) ele = ele->getParent();
  int ndofs = ele->getNumShapeFunctions();
  curls.reserve(curls.size() + ndofs);
  double gradsuvw[256][3];
  ele->getGradShapeFunctions(u, v, w, gradsuvw);
  double jac[3][3];
  double invjac[3][3];
  ele->getJacobian(u, v, w, jac); // compute the elemental jacobian 
  inv3x3(jac, invjac); // compute the inverse of the jacobian
  for(int i = 0; i < ndofs; ++i)
    curls.push_back(GradType( invjac[1][0] * gradsuvw[i][0] + invjac[1][1] * gradsuvw[i][1] + invjac[1][2] * gradsuvw[i][2],
                              -(invjac[0][0] * gradsuvw[i][0] + invjac[0][1] * gradsuvw[i][1] + invjac[0][2] * gradsuvw[i][2]),
                              invjac[2][0] * gradsuvw[i][0] + invjac[2][1] * gradsuvw[i][1] + invjac[2][2] * gradsuvw[i][2]));
}
