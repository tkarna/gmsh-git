
#include "NonLinearDofManager.h"

/*
std::map<Dof, std::vector<double> >::const_iterator hmmPrevSolutions::begin() const {
  std::map<Dof, std::vector<double> >::const_iterator _itPrevSol = _prevSol.begin();
  return _itPrevSol;
}
std::map<Dof, std::vector<double> >::const_iterator hmmPrevSolutions::end() const {
  std::map<Dof, std::vector<double> >::const_iterator _itPrevSol = _prevSol.end();
  return _itPrevSol;
}
const double hmmPrevSolutions::getDofValue(Dof d, double time) const {
  std::map<Dof, std::vector<double> >::const_iterator _itPrevSol = _prevSol.find(d);
  std::map<double, int>::const_iterator itTime2Int = _time2int.find(time);
  return _itPrevSol->second[itTime2Int->second];
}
void hmmPrevSolutions::init(std::map<int, hmmScalarLagrangeFunctionSpace *> *_allFunctionSpaces) {
  std::map<int, hmmScalarLagrangeFunctionSpace *>::const_iterator itSpace;
  std::vector<Dof> keys;
  for (itSpace = _allFunctionSpaces->begin(); itSpace != _allFunctionSpaces->end(); ++itSpace) {
    std::vector<hmmDomainBase *>::const_iterator itDom;
    for (itDom = itSpace->second->getGroupOfDomains()->getGroupOfDomains()->begin(); itDom != itSpace->second->getGroupOfDomains()->getGroupOfDomains()->end(); ++itDom) {
      std::set<MElement*>::const_iterator itElement;
      for (itElement = (*itDom)->getGroupOfElements()->begin(); itElement != (*itDom)->getGroupOfElements()->end(); ++itElement) {
        itSpace->second->getKeys( *(itElement), keys);
      }
    }
  }
  std::vector<Dof>::const_iterator itKeys;
  for (itKeys = keys.begin(); itKeys != keys.end(); ++itKeys) {
    std::vector<double> currentDof;
    currentDof.push_back(0.0);
    _prevSol[*itKeys] = currentDof;
  }
}
void hmmPrevSolutions::addSolution(NonLinearDofManager<double> *pAssembler, double currentTime) {
  std::map<Dof, std::vector<double> >::iterator itPrevSol;
  for (itPrevSol = _prevSol.begin(); itPrevSol != _prevSol.end(); ++itPrevSol) {
    Dof currentDof = itPrevSol->first;
    double val;
    pAssembler->getDofValue(currentDof, val);
    itPrevSol->second.push_back(val);
  }
  std::map<double, int>::const_iterator itLast = _time2int.end();
  itLast--;
  _time2int[currentTime] = (itLast->second)+1;
}
*/
