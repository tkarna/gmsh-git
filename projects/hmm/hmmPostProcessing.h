//
// C++ Interface: PostPro
//
// Description: Class used to calculate/display views from the solution.
//
//
// Copyright: See COPYING file that comes with this distribution
//
//


#ifndef _HMM_POSTPRO_H_
#define _HMM_POSTPRO_H_

#include <map>
#include <complex>
#include <vector>
#include <iostream>
#include <iterator>
#include "GModel.h"
#include "MElement.h"
#include "NonLinearDofManager.h"
#include "solverAlgorithms.h"
#include "groupOfElements.h"
#include "MultiscaleNonLinearSystem.h"
#include "quadratureRules.h"
#include "PView.h"
#include "PViewData.h"
#include "PViewDataGModel.h"
#include "hmmDomain.h"
#include "hmmFunction.h"
#include "hmmConstraints.h"
#include "hmmFunctionSpace.h"
#include "hmmFormulation.h"
#include "hmmSolverAlgorithms.h"
#include "hmmResolution.h"

#define pi 3.1415926535897932

enum PostQuantityName{potential, primalField, dualField, eddyCurrent, localField};

class hmmPostProBase 
{
protected :
  GModel *_pModel;
  NonLinearDofManager<double> *_pAssembler;
  std::map<std::string, hmmMaterialLawBase *> *_allMaterialLaws;
  std::map<std::string, hmmScalarLagrangeFunctionSpace *> *_allFunctionSpaces;
  std::map<std::string, hmmFormulation *> *_allFormulations;
  std::map<std::string, hmmResolutionBase *> *_allResolutions;
  std::map<std::string, GaussQuadrature *> *_allQuadratureRules;


  //List of all points for local field computation 
  std::map<int, SPoint3 *> _allLocalFieldPoints;

public :
  hmmPostProBase(GModel &pModel, NonLinearDofManager<double> &pAssembler, std::map<std::string, hmmMaterialLawBase *> &allMaterialLaws, 
                 std::map<std::string, hmmScalarLagrangeFunctionSpace *> &allFunctionSpaces, std::map<std::string, hmmFormulation *> &allFormulations, 
                 std::map<std::string, hmmResolutionBase *> &allResolutions, std::map<std::string, GaussQuadrature *> &allQuadratureRules);
  virtual ~hmmPostProBase(){}

  // "setting" and "getting" data members
  inline void setGModel(GModel &pModel) {  _pModel = &pModel; }
  inline GModel *getGModel() const { return _pModel; }
  inline void setDofManager(NonLinearDofManager<double> &pAssembler) {  _pAssembler = &pAssembler; }
  inline NonLinearDofManager<double> *getDofManager() const {  return _pAssembler; }
  inline void setMaterialLaw(std::map<std::string, hmmMaterialLawBase *> &allMaterialLaws) { _allMaterialLaws = &allMaterialLaws; }
  inline std::map<std::string, hmmMaterialLawBase *> *getMaterilaLaw() const { return _allMaterialLaws; }
  inline void setFunctionSpace(std::map<std::string, hmmScalarLagrangeFunctionSpace *> &allFunctionSpaces) { _allFunctionSpaces = &allFunctionSpaces; }
  inline std::map<std::string, hmmScalarLagrangeFunctionSpace *> *getFunctionSpace() const { return _allFunctionSpaces; }
  inline void setFormulation(std::map<std::string, hmmFormulation *> &allFormulations) { _allFormulations = &allFormulations; }
  inline std::map<std::string, hmmFormulation *> *getFormulation() const { return _allFormulations; }
  inline void setResolutions(std::map<std::string, hmmResolutionBase *> &allResolutions) { _allResolutions = &allResolutions; }
  inline std::map<std::string, hmmResolutionBase *> *getResolution() const { return _allResolutions; }
  inline const std::map<int, SPoint3 *> *getListOfPtsForLocalFields() const { return &_allLocalFieldPoints; }
  // Building potentials, primal, duals fields using a mesh-based format or old format
  void fillInDataPrimal_Scalar(std::string tagSpace, std::map<int, std::vector<double> > &data, int physical = -1) const;
  void fillInDataPrimal_ExtDer(std::string tagSpace, std::map<int, std::vector<double> > &data) const;
  void fillInDataDual_ExtDer(std::string tagSpace, std::string tagMaterialLaw, std::map<int, std::vector<double> > &data) const;
  // Used in "fillInDataDual_ExtDer" to compute the average of the constitutive law matrix over the element
  bool meanEleHomQtyLinear(hmmMaterialLawBase *currentML, MElement *ele, HomQtyLinear &currentMLQtyBase, 
                           std::map<hmmGroupOfDomains *, MLQtyBase *>::const_iterator itGoDMQB) const; 
  // Used in "fillInDataDual_ExtDer" to compute the dual field on a given element
  // void computeDualFieldOnElement(std::map<std::string, hmmScalarLagrangeFunctionSpace *>::iterator itSpace, 
  void computeDualFieldOnElement(hmmScalarLagrangeFunctionSpace *funcSpace, MElement *ele, std::map<int, std::vector<double> > &data, 
                                 HomQtyLinear &currentMLQtyBase, bool isBuilt)const;
  bool buildViewFromMap(std::map<int, std::vector<double> > data, std::string fieldFileName, 
                        int _iteration, std::string location, std::string DataType) const;
  virtual bool buildPotentialView(std::string tagSpace, std::string fieldFileName, int _iteration, std::string location) const;
  double buildMagEnergyView_a_v_formulation(double &totalEnergy, std::string tagML, std::string tagQuad, std::string tagSpace, 
                                            std::string tagGoD, double dTime, int currentTimeStep = -1, double currentTime = 0.0,
                                            std::string fileName = "../output/macroproblem/", bool writeResults = false);
  double buildMagEnergyView_a_v_formulation(hmmMaterialLawBase *matLaw, hmmScalarLagrangeFunctionSpace *funcSpace,  
                                            GaussQuadrature *GaussQuad,  hmmGroupOfDomains *thisGoD, double dTime, MElement *e);
  virtual bool buildPrimalDerivedFieldView(std::string tagSpace, std::string fieldFileName, int _iteration, std::string location) const;
  virtual bool buildDualDerivedFieldView(std::string tagSpace, std::string tagMaterialLaw, std::string fieldFileName, int _iteration, std::string location) const;
  virtual bool buildDualDerivedFieldView_HMM(std::string tagSpace, std::string tagMaterialLaw, std::string fieldFileName, int _iteration, std::string location) const; 
  //virtual bool buildDualDerivedFieldView_Tot(std::string tagSpace, std::string tagMaterialLaw, std::string fieldFileName, int _iteration, std::string location) const; 
  virtual bool buildElectricCurrentView(std::string tagSpace, std::string tagResoType, std::string fieldFileName, int _iteration, std::string location) const;
  virtual bool buildEddyCurrentView(std::string tagSpace, std::string tagML, std::string tagResoType, std::string fieldFileName, int _iteration, std::string location) const;
  //  virtual bool buildEddyCurrentView(std::string tagSpace, std::string fieldFileName, int _iteration, std::string location, double sigma) const; 
  virtual bool buildPrimalDerivedFieldView_OldFormat(std::string tagSpace, std::string fieldFileName, int _iteration, std::string location) const;

  double computeLinkageFluxDensityForTimeStep(std::string tagGoD, std::string tagSpace, std::string tagForm, std::string tagResoType,  
                                              std::string tagQuad, int tagLinearSourceTerm) const;
  //Computation of local fields on meso-domain around a given point
  void initMapOfPoints();
  void addPoint2MapOfPoints(std::string tagML, int intPt, double x, double y, double z);
  void emptyMapOfPoints();
  bool computeLocalFields(int keyForThisPoint, SPoint3 &thisPoint, std::string tagML, int thisThreadNum, std::string tagRes) const; // 
  bool computeLocalFields(std::string tagML, std::string tagRes) const; // 
};

#endif// _HMM_POSTPRO_H_
