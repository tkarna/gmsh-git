//
// Description: System for non linear static scheme
//
// Copyright: See COPYING file that comes with this distribution
//
// The system solves "K*Delta_x = -res" and then adds a Delta_x to xsol(the actual solution)
// There are new features as compared to the "linearSystem class": we have added the "systemSolve" function (we add the increment _x
// to the actual solution: _xsol += _x). The function "allocate" allows to allocate "_xsol",
// the "getFromSolution" function (return _xsol[i] instead of _x[i]). The "clear" function must also destroy "_xsol"
// The "setInitialValues" function allows to set initial values of unknows prior to doing any computations and the "setNoisyValue"
// and "getNoisyValue" and the "setGmresValue" and "getGmresValue" which allow to set and get the noisy and the Gmres values

#include <iostream>
#include <sstream>
#include <string>
#include <complex>
#include "linearSystemFull.h"
#include "linearSystemGMM.h"
#include "linearSystemPETSc.h"



#ifndef _MULTISCALE_NONLINEARSYSTEMS_H_
#define _MULTISCALE_NONLINEARSYSTEMS_H_
/*
template<class scalar>
class MultiscaleNonLinearSystem{
#ifndef SWIG
 public:
  MultiscaleNonLinearSystem(){}
  ~MultiscaleNonLinearSystem(){}
  //virtual void getFromSolutionLin(int row, scalar &val) const=0;
  //virtual void setInitialValues(int i, scalar &val)=0;
  #endif // SWIG
  };
*/

// gmm
template <class scalar>
class MultiscaleNonLinearSystemGmm : public linearSystemGmm<scalar> {//, public MultiscaleNonLinearSystem<scalar>{
protected:
  std::vector<scalar> *_xsol;
  int _getNoisyValue, _getGmresValue, _intInitialize;
public:
  MultiscaleNonLinearSystemGmm(int getNoisyValue = 0, int getGmresValue = 1, int intInitialize = 1) : 
    linearSystemGmm<scalar>(), _xsol(NULL), _getNoisyValue(getNoisyValue), 
    _getGmresValue(getGmresValue), _intInitialize(intInitialize) {}
  virtual ~MultiscaleNonLinearSystemGmm(){}
  
  inline void setGmresValue(int GmresValue) { _getGmresValue = GmresValue; }
  inline void setNoisyValue(int NoisyValue) { _getNoisyValue = NoisyValue; }
  inline int getGmresValue() const { return _getGmresValue; }
  inline int getNoisyValue() const { return _getNoisyValue; }
  //inline void setElementSol(int i, scalar val) { _xsol[i] = val; }
  //inline void addToElementSol(int i, scalar val) { _xsol[i] += val; }
  inline std::vector<scalar> *getVectorSol() { return _xsol; }
  
#if defined(HAVE_GMM)
  virtual void setInitialValues(int nbRows, const scalar val = 0.0) {
    if(!(this->isAllocated()) )
      this->allocate(nbRows);
    //if(this->isAllocated())
    for(int i = 0; i < nbRows; i++)
      (*_xsol)[i] = val;
    //else 
    //  Msg::Error("Can not initialize a non-allocated system !");
  }    
  
  virtual void allocate(int nbRows) {
    this->linearSystemGmm<scalar>::allocate(nbRows);
    if(_xsol !=NULL)
      delete _xsol;
    _xsol = new std::vector<scalar>(nbRows);
    if (_intInitialize) 
      setInitialValues(nbRows, 0.0);
  }
  virtual void clear() {
    this->linearSystemGmm<scalar>::clear();
    if(_xsol !=NULL)
      delete _xsol;
    _xsol = NULL;
  }
  virtual void getFromSolution(int row, scalar &val) const { 
    if (!_xsol) Msg::Error("Byanze ga mwa !");    
    val = (*_xsol)[row];
  }
  virtual void getFromMatrix (int row, int col, scalar &val) const {
    this->linearSystemGmm<scalar>::getFromMatrix(row, col, val);
  }  
  
  virtual void printVector (std::string matName = "", std::string matFileName = "../output/b_", int iteration = 0) const {
    std::string s; std::stringstream out; out << iteration; s = out.str();
    std::string extension (".txt");
    matFileName += s;; matFileName += extension;
    
    Msg::Warning("The matFileName string is %s. \n", matFileName.c_str() );
    
    std::string name = "Printing the matrix"+matName;
    std::cout << name << std::endl; 
    FILE *fp; fp = fopen(matFileName.c_str(), "w") ;
    //fprintf(fp, "b =[");
    fclose(fp);    
    
    fp = fopen(matFileName.c_str(), "a") ;
    int solVecSize = (int )(_xsol->size() );
    scalar val; 
    for (int i = 0; i < solVecSize; i++) {
      val = (double)(this->linearSystemGmm<scalar>::_b->at(i) );
      std::cout <<"b["<<i<<"] is equal to "<< val << std::endl; 
      double value = (double)(val);
      fprintf(fp, "%.16g \n", value);
    }
    fclose(fp);
  }
  
  
  virtual void printMatrix (std::string matName = "", std::string matFileName = "../output/A_", int iteration = 0) const {
    std::string s; std::stringstream out; out << iteration; s = out.str();
    std::string extension (".txt");
    matFileName += s;; matFileName += extension;
    
    Msg::Warning("The matFileName string is %s. \n", matFileName.c_str() );
    
    std::string name = "Printing the matrix"+matName;
    std::cout << name << std::endl; 
    FILE *fp; fp = fopen(matFileName.c_str(), "w") ;
    //fprintf(fp, "A =[");
    fclose(fp);
    
    fp = fopen(matFileName.c_str(), "a") ;
    int solVecSize = (int )(_xsol->size() );
    for (int i = 0; i < solVecSize; ++i) {
      for (int j = 0; j < solVecSize; ++j) {
        scalar val; getFromMatrix(i, j, val);
        std::cout <<"A["<<i<<"]["<<j<<"] is equal to "<< val << std::endl; 
        double value = (double)(val);
        fprintf(fp, "%.16g ", value);
      }
      fprintf(fp, " \n");
    }
    //fprintf(fp, "] ;");
    fclose(fp);
  }
  virtual double normInfRightHandSide() const{
    double nor = 0.; double temp;
    this->linearSystemGmm<scalar>::normInfRightHandSide();
    int sizeOfRHS = (int )(this->linearSystemGmm<scalar>::_b->size() );
    for(int i=0; i < sizeOfRHS; i++) {
      temp = abs( (std::complex<double>)(this->linearSystemGmm<scalar>::_b->at(i) ) );
      Msg::Error("The residu for b(%d) = %E", i, temp);
      if(nor < temp) 
        nor=temp;
    }
    return nor;
  }
  virtual void setGmres(int n) { this->linearSystemGmm<scalar>::setGmres(n); }
  virtual void setNoisy(int n) { this->linearSystemGmm<scalar>::setNoisy(n); }
  
  virtual int systemSolve(bool solveForDelta = true) {
    if (getGmresValue() ) setGmres(1);
    else setGmres(0);
    if (getNoisyValue() ) setNoisy(1);
    else setNoisy(0);
    this->linearSystemGmm<scalar>::systemSolve();
    int solVecSize = (int )(getVectorSol()->size() );
    if(solveForDelta) {
      for(int i = 0; i < solVecSize; i++){
        scalar val;
        this->linearSystemGmm<scalar>::getFromSolution(i,val);
        //this->addToElementSol(i, val);
        (*_xsol)[i] += val;
      }
    }
    return 1;
  }
  
  virtual int systemSolveTot() {
    if (getGmresValue() ) setGmres(1);
    else setGmres(0);
    if (getNoisyValue() ) setNoisy(1);
    else setNoisy(0);
    this->linearSystemGmm<scalar>::systemSolve();
    int solVecSize = (int )(this->getVectorSol()->size() );
    for(int i = 0; i < solVecSize; i++){
      scalar val;
      this->linearSystemGmm<scalar>::getFromSolution(i,val);
      //this->setElementSol(i, val);
      (*_xsol)[i] = val;
    }
    return 1;
  }
  
  
//   int systemSolve(bool solveForDelta = true);//  {
// //     if (_getGmresValue) setGmres(1);
// //     else setGmres(0);
// //     if (_getNoisyValue) setNoisy(1);
// //     else setNoisy(0);
// //     this->linearSystemGmm<scalar>::systemSolve();
// //     int solVecSize = (int )(_xsol->size() );
// //     if(solveForDelta) {
// //       for(int i = 0; i < solVecSize; i++){
// //         scalar val;
// //         this->linearSystemGmm<scalar>::getFromSolution(i,val);
// //         (*_xsol)[i] += val;
// //       }
// //     }
// //     return 1;
// //   }
//   int systemSolveTot();
#endif // HAVE_GMM
};

// Full
//=====
template <class scalar>
class MultiscaleNonLinearSystemFull : public linearSystemFull<scalar> {
 protected:
  std::vector<scalar> *_xsol;
 public:
  MultiscaleNonLinearSystemFull() : linearSystemFull<scalar>(), _xsol(NULL) {}
  virtual ~MultiscaleNonLinearSystemFull(){}
  virtual void setInitialValues(const scalar &val) {
    if(this->isAllocated()) {
      for(int i = 0; i < _xsol->size(); i++)
        (*_xsol)[i] = val;
    }
    else Msg::Error("Can not initialize a non-allocated system !");
  }
  virtual void allocate(int nbRows) {
    this->linearSystemFull<scalar>::allocate(nbRows);
    if(_xsol !=NULL) delete _xsol; //
    _xsol = new std::vector<scalar>(nbRows);
    setInitialValues(0.0);
  }
  virtual void clear() {
    this->linearSystemFull<scalar>::clear();
    if(_xsol !=NULL) delete _xsol;
    _xsol=NULL;
  }
  virtual void getFromSolution(int row, scalar &val) const {
    val = (*_xsol)[row];
  }
  virtual int systemSolve() {
    this->linearSystemFull<scalar>::systemSolve();
    for(int i = 0; i < _xsol->size(); i++){
      scalar val;
      this->linearSystemFull<scalar>::getFromSolution(i,val);
      (*_xsol)[i] += val;
      std::cout << "Delta_Phi["<<i<<"] is equal to " << val <<"\n";
    }
    return 1;
  }
};


#endif // _MULTISCALE_NONLINEAR_SYSTEM_

