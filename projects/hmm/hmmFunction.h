// C++ Interface: hmmFunction
//
// Description: Definition of functions (constants, space coordinate dependent, material laws 
// [onescale, multiscale, linear, nonlinear/hysteretic], ...). Each function class has a map 
// "std::map<hmmGroupOfDomains *, MLQtyBase *>" associating a MLQtyBase to a hmmGroupOfDomains
// (e.g.: in the case of linear material laws, the MLQtyBase is a matrix defining the tensor of the material law). 

//
// Copyright: See COPYING file that comes with this distribution
//
// bugs and problems to the public mailing list <gmsh@geuz.org>.
//

#ifndef _HMMFUNCTION_H_
#define _HMMFUNCTION_H_

#include <stdio.h>
#include <iostream>
#include <vector>
#include <omp.h>
#include "OS.h"
#include "GModel.h"
#include "MElement.h"
#include "SPoint3.h"
#include "fullMatrix.h"
#include "quadratureRules.h"
#include "groupOfElements.h"
#include "NonLinearDofManager.h"
#include "hmmDomain.h"
#include "solverField.h"
#include "hmmSolverField.h"

class hmmPostProBase;

typedef enum functionType{UNDEF, CONSTANT, ANALYTICAL_MLAW, SPACE_COORDINATES_DEPENDENT, LINEAR_MLAW, 
                          NONLINEAR_MLAW, HYSTERETIC, HMM_LINEAR_MLAW, HMM_NONLINEAR_MLAW, HMM_HYSTERETIC_MLAW};

class MLQtyBase {
public:
  MLQtyBase();
  virtual ~MLQtyBase();
  virtual void printMLQty() = 0;
};

class HomQtyLinear : public MLQtyBase {
protected:
  int _xDim, _yDim;
  fullMatrix<double> *_matLaw;
  fullVector<double> *_vecLaw;

  // We add a fullvector for keeping track of the previous 
  // values of "b" in h-conform formulations. 
  //======================================================

  fullVector<double> *_vecLawOld;

  //=============================
public:
  HomQtyLinear();
  HomQtyLinear(fullMatrix<double> &matLaw, double vec1 = 0.0, double vec2 = 0.0, double vec3 = 0.0);
  virtual ~HomQtyLinear() {}
  virtual void deletePointersFromAnyWhere();
  inline void setMatrix(fullMatrix<double> &matLaw) { _matLaw = &matLaw;} //FixMe
  inline void addToMatrix(fullMatrix<double> &otherMatLaw) { _matLaw->axpy(otherMatLaw);} //FixMe
  inline void setMatrixElement(int i, int j, double value) { _matLaw->set(i, j, value);}
  inline fullMatrix<double> *getMatrix() { return _matLaw;}
  inline void setVectorElement(int i, double value) { _vecLaw->set(i, value); }
  inline fullVector<double> *getVector() { return _vecLaw; }
  inline void  addToVector(fullVector<double> &otherFullVec) { _vecLaw->axpy(otherFullVec); }
  
  // Functions for the h-formulations
  //=============================================================================================
  inline void setOldVectorElement(int i, double value) { _vecLawOld->set(i, value); }
  inline fullVector<double> *getOldVector() { return _vecLawOld; }
  inline void addToVectorOld(fullVector<double> &otherFullVec) { _vecLawOld->axpy(otherFullVec); }
  inline void UpdateOldVector() { *(_vecLawOld) = *(_vecLaw); }
  //============================================================================================
  void printMLQty();
};

class HomQtyNL : public MLQtyBase {
protected:
  hmmGroupOfDomains *_dMLNL;
  std::map<MElement*, std::map<int, HomQtyLinear *> > _globalMatLaw;
public:
  HomQtyNL(hmmGroupOfDomains &dMLNL);
  virtual ~HomQtyNL();
  virtual void InitMatLaw( GaussQuadrature &integrator, fullMatrix<double> &ML, double vec1 = 0.0, double vec2 = 0.0, double vec3 = 0.0);
  std::map<MElement*, std::map<int, HomQtyLinear *> >::iterator begin();
  std::map<MElement*, std::map<int, HomQtyLinear *> >::iterator end();
  std::map<MElement*, std::map<int, HomQtyLinear *> > *getMaterialLaw();
  std::map<MElement*, std::map<int, HomQtyLinear *> > *getNLHomQty() { return &_globalMatLaw; }
  HomQtyLinear *getMaterialLawAtGaussPoint(MElement *ele, int gaussPointNum);
  int getNumMicroProblems() const;
  inline hmmGroupOfDomains *getGoD() const{ return _dMLNL; }
  void UpdateOldVector();
  void printMLQty();
};

//=======================================================================================================================================
class HomQtyJouleLosses : public MLQtyBase {
protected:
  hmmGroupOfDomains *_dMLNL;
  std::map<MElement*, std::map<int, double> > _globalJL;
public:
  HomQtyJouleLosses(hmmGroupOfDomains &dMLNL);
  virtual ~HomQtyJouleLosses();
  virtual void InitJLMap( GaussQuadrature &integrator, double JL = 0.0);
  std::map<MElement*, std::map<int, double> >::iterator begin();
  std::map<MElement*, std::map<int, double> >::iterator end();
  std::map<MElement*, std::map<int, double> > *getMapJL();
  double getJLAtGaussPoint(MElement *ele, int gaussPointNum);
  double computeTotalJouleLossesTimeStep(GaussQuadrature *integrator, int _currentTimeStep, double _currentTime);
  int getNumMicroProblems() const;
  void printMLQty();
};
//=======================================================================================================================================
class HomQtyHystereticLosses : public MLQtyBase {
protected:
  hmmGroupOfDomains *_dMLNL;
  std::map<double, std::map<MElement*, std::map<int, double> > > _globalHL;
  std::map<int, std::vector<double> > _localTotatHystresisLosses;
public:
  HomQtyHystereticLosses(hmmGroupOfDomains &dMLNL);
  virtual ~HomQtyHystereticLosses();
  void InitHLMapTimeStep( GaussQuadrature &integrator, double currentTime, double HL = 0.0);
  inline hmmGroupOfDomains *getGOD() const {
    return _dMLNL;
  }
  void deleteHLMapTimeStep( double currentTime);
  std::map<double, std::map<MElement*, std::map<int, double> > >::iterator begin();
  std::map<double, std::map<MElement*, std::map<int, double> > >::iterator end();
  std::map<double, std::map<MElement*, std::map<int, double> > >::iterator getIteratorCurrentTime(double _currentTime, int isIteratorFound);
  std::map<double, std::map<MElement*, std::map<int, double> > > *getMapHL();
  std::map<double, std::map<MElement*, std::map<int, double> > >::iterator getIteratorNearestFirstTime(double initTime, int isIteratorFound);
  void addTwoMaps(std::map<MElement*, std::map<int, double> > &mapResults, std::map<MElement*, std::map<int, double> > &mapOther);
  std::map<int, std::vector<double> > computeTotaLocallHystereticLossesOverCycle(GaussQuadrature *integrator, double initTime, double dtime, double freq, int cycleNum = 1);
  double integrateHystereticLossesOneTimeStep(GaussQuadrature *integrator, std::map<MElement*, std::map<int, double> > &_thisTimeMapHL, 
                                              const char *fileName, double currentTime);
  double computeTotalHystereticLossesOverCycle(GaussQuadrature *integrator, double _initTime, double dtime, double freq, int cycleNum = 1);
  void printMLQty();
};


class HomQtyGlobalQuantities : public MLQtyBase {
protected:
  hmmGroupOfDomains *_dMLNL;
  std::map<std::string, HomQtyHystereticLosses* > _globalQuantities; // a structure containing all the global quantities (magnetic energy, joule losses, electric energy, ...) corresponding to all the Gauss points of the macro domain and for all the times
public:
  HomQtyGlobalQuantities(hmmGroupOfDomains &dMLNL) {
    _dMLNL = &dMLNL;
  }
  virtual ~HomQtyGlobalQuantities(){}
  std::map<std::string, HomQtyHystereticLosses* > *getMapGQ() {
    return &_globalQuantities;
  }
  void addGlobalQuantityToMap(std::string nameGQ) {
    HomQtyHystereticLosses *thisHomGQ = new HomQtyHystereticLosses(*_dMLNL);
    _globalQuantities[nameGQ] = thisHomGQ;
  }
  HomQtyHystereticLosses *getHomQty(std::string nameGQ) const {
    std::map<std::string, HomQtyHystereticLosses* >::const_iterator itGQ = _globalQuantities.find(nameGQ);
    if(itGQ != _globalQuantities.end() ) {
      return itGQ->second;
    }
    else {
      Msg::Error("Done in hmmFunction getting the global quantity named %s. Global quantity not found, returning a null pointer. \n", nameGQ.c_str() );
      return NULL;
    }
  }
  void InitHLMapTimeStep( std::string nameGQ, GaussQuadrature &integrator, double currentTime, double HL = 0.0) {
    std::map<std::string, HomQtyHystereticLosses* >::iterator itGQHom = _globalQuantities.find(nameGQ);
    if(itGQHom != _globalQuantities.end() ) {
      itGQHom->second->InitHLMapTimeStep(integrator, currentTime, HL);
    }
    else {
      Msg::Error("Done in hmmFunction.cpp: Error occurred initializing a 'HomQtyHystereticLosses' structure which does not exist.");
    }
  }
  void InitHLMapTimeStep(GaussQuadrature &integrator, double currentTime, double HL = 0.0) {
    std::map<std::string, HomQtyHystereticLosses* >::iterator itGQHom;
    for(itGQHom = _globalQuantities.begin(); itGQHom != _globalQuantities.end(); ++itGQHom) {
      InitHLMapTimeStep(itGQHom->first, integrator, currentTime, HL);
    }
  }
  void integrateGQOverTime(std::string nameGQ, GaussQuadrature *integrator, std::string fileName) {
    std::map<std::string, HomQtyHystereticLosses* >::iterator itGQHom = _globalQuantities.find(nameGQ);
    if(itGQHom != _globalQuantities.end() ) {
      HomQtyHystereticLosses *thisHomQtyHyst = itGQHom->second;
      std::map<double, std::map<MElement*, std::map<int, double> > >::iterator itThisTimeMap;
      for(itThisTimeMap = thisHomQtyHyst->getMapHL()->begin(); itThisTimeMap != thisHomQtyHyst->getMapHL()->end(); ++itThisTimeMap) {
        thisHomQtyHyst->integrateHystereticLossesOneTimeStep(integrator, itThisTimeMap->second, fileName.c_str(), itThisTimeMap->first);
      }
    }
    else {
      Msg::Error("Done in hmmFunction.cpp while computing the integral of the global quantity labelled '%s'. Name of the global quantity not found :-(", nameGQ.c_str() );
    }
    return;
  }
  inline hmmGroupOfDomains *getGOD() const {
    return _dMLNL;
  }
  void printMLQty(){}
};
//=======================================================================================================================================

class hmmFunctionBase {
  // Be able to define a piecewise function on different hmmGroupOfDomains. Add a map 
  // associating each hmmGoD to a given MQB. It will then be possible to define the Fcn piecewise.
protected:
  functionType _whatFunctionType;
  std::map<hmmGroupOfDomains *, MLQtyBase *> _matLawByGroupOfDom;
public:
  hmmFunctionBase() {}
  ~hmmFunctionBase() {}
  inline void defineFunctionType(functionType whatFunctionType) {_whatFunctionType = whatFunctionType;}
  virtual void setType() = 0;
  inline functionType getType() const { return _whatFunctionType; }
  void addGroupOfDomains(hmmGroupOfDomains &groupOfDomains, MLQtyBase &MQB);
  const std::map<hmmGroupOfDomains *, MLQtyBase *> *getMatLawByGroupOfDomains() const;
  MLQtyBase *getMQB(hmmGroupOfDomains &thisGoD) const;
  bool setMQB(hmmGroupOfDomains &thisGoD, MLQtyBase &MQB);
  virtual hmmGroupOfDomains *getGroupOfDomainsForElement(MElement *ele) const;
  //==========================================================================
  virtual hmmGroupOfDomains *getGroupOfDomains(std::string tagGoD) const {
    std::map<hmmGroupOfDomains *, MLQtyBase *>::const_iterator itMLGoD;
    hmmGroupOfDomains *thisGoD = NULL;
    for (itMLGoD = getMatLawByGroupOfDomains()->begin(); itMLGoD != getMatLawByGroupOfDomains()->end(); ++itMLGoD) {
      if (itMLGoD->first->getTagGroupOfDomains() == tagGoD) {
        thisGoD = itMLGoD->first;
        Msg::Info("The hmmGoD labelled %s has been found.", tagGoD.c_str() );
        //return itMLGoD->first;
      }
      else {
        Msg::Info("The hmmGoD labelled %s has not been found :-(", tagGoD.c_str() );
        //return NULL;
      }
    }
    return thisGoD;
  }
  //==========================================================================
  MLQtyBase *getMLOnElement(MElement *ele) const;
  int printNumOfDomains();
};


class hmmMaterialLawBase : public hmmFunctionBase {
protected:
  std::string _tag;
public:
  hmmMaterialLawBase(std::string tag);
  virtual ~hmmMaterialLawBase(){}
  inline void setTag(std::string tag) { _tag = tag;}
  inline std::string getTag()const { return _tag;}
  virtual MLQtyBase *getMaterialLaw(hmmGroupOfDomains &thisGoD) const;
  virtual MLQtyBase *getMaterialLaw(MElement *ele) const;
  virtual bool setMaterialLaw(hmmGroupOfDomains &thisGoD, MLQtyBase &MQB);
  void printML(hmmGroupOfDomains &thisGoD) const;
  void printMLOnAllDom() const;
};

class hmmLinearMaterialLaw : public hmmMaterialLawBase {
protected:
  HomQtyLinear *_ML;
public:
  hmmLinearMaterialLaw(std::string tag);
  virtual ~hmmLinearMaterialLaw() {} 
  inline void setType() {this->defineFunctionType(LINEAR_MLAW);}
};

class hmmMicroSolverBase : public hmmMaterialLawBase {
protected:
  int _nx, _ny;
  double _lx, _ly;
public:
  hmmMicroSolverBase(std::string tag, double lx, double ly, int nx, int ny);
  ~hmmMicroSolverBase(){}
  inline int getNX() const{ return _nx;}
  inline int getNY() const { return _ny;}
  inline double getLengthX() const { return _lx;}
  inline double getLengthY() const { return _ly;}
};

class hmmMicroSolverGetDPBase : public hmmMicroSolverBase {
public:
  typedef TensorialTraits<double>::ValType ValType;
  typedef TensorialTraits<double>::GradType GradType;
protected:
  std::string _microProblemFileName;
  std::string _GMSHVerbosity, _GMSHGeoFileName, _GMSHExactGeoFileName; 
  std::string _GetDPVerbosity, _GetDPProFileName, _GetDPExactProFileName,  _GetDPResolutionName, _GetDPPostPro; 
public:
  hmmMicroSolverGetDPBase(std::string tag, double lx, double ly, int nx, int ny);
  virtual ~hmmMicroSolverGetDPBase() {}
  virtual void setMicroParam(std::string GMSHVerbosity, std::string GMSHGeoFileName, std::string GetDPVerbosity, 
                             std::string GetDPProFileName, std::string GetDPResolutionName, std::string GetDPPostPro, 
                             std::string GMSHExactGeoFileName = "", std::string GMSHExactProfiFeName = "",std::string microProblemFileName = "");
  inline std::string getMicroProblemFileName() const {return _microProblemFileName;}
  inline std::string getGMSHVerbosity() const {return _GMSHVerbosity;}
  inline std::string getGetDPVerbosity() const {return _GetDPVerbosity;}
  inline std::string getGMSHGeoFileName() const {return _GMSHGeoFileName;}
  inline void setGMSHGeoFileName(std::string GMSHGeoFileName) { _GMSHGeoFileName = GMSHGeoFileName;}
  inline std::string getGMSHExactGeoFileName() const {return _GMSHExactGeoFileName;}
  inline std::string getGetDPProFileName() const {return _GetDPProFileName;}
  inline void setGetDPProFileName(std::string GetDPProFileName) { _GetDPProFileName = GetDPProFileName;}
  inline std::string getGetDPExactProFileName() const {return _GetDPExactProFileName;}
  inline std::string getGetDPResolutionName() const {return _GetDPResolutionName;}
  inline std::string getGetDPPostPro() const {return _GetDPPostPro;}
  virtual double evaluateFuncInPoint(SPoint3 &sp, SPoint3 &gaussPoint, ValType &func, GradType &gradFunc);
  virtual void evaluateFuncOnRegularGrid(SPoint3 &gaussPoint, ValType &func, GradType &gradFunc);
  virtual bool updateMaterialLaw(hmmGroupOfDomains &hmmGoD, double currentTime = 0.0, int currentTimeStep = 0, 
                                 bool h_conform = false, bool changeDualPrevValue = false) = 0;
};

class hmmMicroSolverGetDPLinearDouble : public hmmMicroSolverGetDPBase {
protected:
  HomQtyLinear *_ML;
public:
  hmmMicroSolverGetDPLinearDouble(std::string tagMaterialLaw, double lx, double ly, int nx, int ny) : hmmMicroSolverGetDPBase(tagMaterialLaw, lx, ly, nx, ny) {}
  virtual ~hmmMicroSolverGetDPLinearDouble();
  virtual inline void setType() {this->defineFunctionType(HMM_LINEAR_MLAW);}
  virtual bool updateMaterialLaw(hmmGroupOfDomains &hmmGoD, double currentTime = 0.0, int currentTimeStep = 0, 
                                 bool h_conform = false, bool changeDualPrevValue = false) {return true;}
};

class hmmMicroSolverGetDPNonLinearDouble : public hmmMicroSolverGetDPBase {
protected:
  HomQtyNL *_ML;
  HomQtyJouleLosses *_JL;
  HomQtyHystereticLosses *_HL;
  HomQtyGlobalQuantities *_GQ;
  GaussQuadrature *_integrator;
  SolverField<double> *_solField;
public:
  hmmMicroSolverGetDPNonLinearDouble(NonLinearDofManager<double> &pAssembler, hmmScalarLagrangeFunctionSpace &LagSpace, 
                                     GaussQuadrature &integrator, std::string tagMaterialLaw, double lx, double ly, int nx, int ny);
  virtual ~hmmMicroSolverGetDPNonLinearDouble() {}
  virtual void setType() { this->defineFunctionType(HMM_NONLINEAR_MLAW); }
  inline GaussQuadrature *getIntegrationMethod() const { return _integrator; }
  inline SolverField<double> *getSolverField() const { return _solField; }


  inline HomQtyNL *getHomQtyNL() const {
    Msg::Info(" IIIIIIIIIIIIIIIIIIIIIIIIIIIIIII There are %d problems for the NL domain.", this->_ML->getNumMicroProblems() );
    return _ML; 
  }
  inline hmmGroupOfDomains *getGoDHomQtyNL() const{
    Msg::Info("The found ML is labelled %s ", getHomQtyNL()->getGoD()->getTagGroupOfDomains().c_str() );
    return _ML->getGoD(); 
  }
  //=====================================================================================================
  virtual hmmGroupOfDomains *getGroupOfDomainsForElement(MElement *ele) const {return getGoDHomQtyNL(); }
  //virtual hmmGroupOfDomains *getGroupOfDomains(std::string tagGoD) const {
  //  Msg::Info("Komera mwa :-)");
  //  return getGoDHomQtyNL(); 
  //}
  //=====================================================================================================
  inline HomQtyJouleLosses *getMapJL() const { return _JL; }
  inline void addHLMap(HomQtyHystereticLosses &thisHLMap) { _HL = &thisHLMap; }
  inline HomQtyHystereticLosses *getMapHL() const { return _HL; }
  inline void addGQMap(HomQtyGlobalQuantities &thisGQMap) { _GQ = &thisGQMap; }
  inline void addOneGQ(std::string nameGQ) { _GQ->addGlobalQuantityToMap(nameGQ) ;}
  inline HomQtyHystereticLosses *getOneGQ(std::string nameGQ) const { return _GQ->getHomQty(nameGQ); }
  inline void initGQ(std::string nameGQ, double currentTime, double HL = 0.0) { this->_GQ->InitHLMapTimeStep(nameGQ, *(_integrator), currentTime, HL) ;}
  inline HomQtyGlobalQuantities *getMapGQ()const { return _GQ; } //===============================================================
  void addValueToMapHL(double currentTime, MElement *ele, SPoint3 &_gaussPoint, double val);
  void addValueToMapGQ(std::string nameGQ, double currentTime, MElement *ele, SPoint3 &_gaussPoint, double val);
  void initMaterialLaw(hmmGroupOfDomains &thisGoD, fullMatrix<double> &matLaw, double vec1 = 0.0, double vec2 = 0.0, double vec3 = 0.0);
  void initJLMap(hmmGroupOfDomains &thisGoD, double JL = 0.0);
  double integRealQtyAtGaussPoints(std::map<MElement*, std::map<int, double> > &thisTimeMap, 
                                   std::string nameOfQty, int _currentTimeStep, double _currentTime);
  double integrateGQOneTimeStep(std::string nameGQ, std::string nameOfQty, int _currentTimeStep, double _currentTime);  
  void integrateGQ(std::string nameGQ, std::string fileName);
  void convertFromGaussPointDataToElementData(std::map<MElement*, std::map<int, double> > &thisTimeMap, 
                                              std::map<int, std::vector<double> > &mapForPlot, bool isQtyExtensive = true);
  SPoint3 *convertFromIntToPoint(MElement *ele, int GPNum);
  int convertFromPointToInt(MElement *ele, SPoint3 &p);
  virtual void computeMaterialLawAtGaussPoint(HomQtyLinear &HomFields, SPoint3 &_gaussPoint, MElement *ele, int thisThreadNum, 
                                              double currentTime, int currentTimeStep, bool h_conform = false, bool changeDualPrevValue = false);
  virtual void computeLocalFields(HomQtyLinear &HomFields, int keyForThisPoint, SPoint3 &_gaussPoint, MElement *ele, 
                                  int thisThreadNum, double currentTime, int currentTimeStep);
  virtual bool updateMaterialLaw(double currentTime = 0.0, int currentTimeStep = 0, bool h_conform = false, bool changeDualPrevValue = false);
  virtual bool updateMaterialLaw(hmmGroupOfDomains &thisGoD, double currentTime = 0.0, int currentTimeStep = 0, 
                                 bool h_conform = false, bool changeDualPrevValue = false); 
  virtual bool updateMaterialLaw( HomQtyLinear &HomFields, SPoint3 &_gaussPoint, MElement *ele, int thisThreadNum, 
                                  double currentTime = 0.0, int currentTimeStep = 0, bool h_conform = false, bool changeDualPrevValue = false);

  inline double computeHomJL(int _currentTimeStep, double _currentTime) {
   return _JL->computeTotalJouleLossesTimeStep(_integrator, _currentTimeStep, _currentTime);
  }

  virtual std::map<int, HomQtyLinear *> getMaterialLawOnElement(hmmGroupOfDomains &thisGoD, MElement *ele) const;
  virtual HomQtyLinear *getMaterialLawAtGaussPoint(hmmGroupOfDomains &thisGoD, MElement *ele, int numGP) const;
  virtual bool setMaterialLaw(hmmGroupOfDomains &thisGoD, MLQtyBase &MQB);
  inline void computeTotalHysteresisLossesOverCycle(double initTime, double dtime, double freq, int cycleNum = 1) {
    double totalHLLosses = 0.0;
    totalHLLosses = _HL->computeTotalHystereticLossesOverCycle(_integrator, initTime, dtime, freq, cycleNum);
  }
};
#endif //_HMMFUNCTION_H_
