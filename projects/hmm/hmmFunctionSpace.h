// C++ Interface: hmmFunctionSpace
//
// Description: classes defined here are used to create function spaces. These spaces are derived 
// from the ones in "gmsh/Solver/functionSpace.h" but we add data members: "*_dFS", a physical group 
// on which the function space is defined and "_diriFS", the map of all Dirichlet BCs. 
//
// Copyright: See COPYING file that comes with this distribution
//
// bugs and problems to the public mailing list <gmsh@geuz.org>.
//

#ifndef _HMMFUNCTIONSPACE_H_
#define _HMMFUNCTIONSPACE_H_

#include <stdio.h>
#include <map>
#include "functionSpace.h"
#include "hmmDomain.h"
#include "hmmConstraints.h"

enum FSType{Form0, Form1P, Form1};
enum BFType{BF_Node, BF_PerpendicularEdge, BF_Edge};

class hmmFunctionSpaceBase
{
protected:
  hmmGroupOfDomains *_dFS;
  FSType _whatFS;
  BFType _whatBF;
public:
  inline void setGroupOfDomains(hmmGroupOfDomains &dFS) { _dFS = &dFS;}
  inline hmmGroupOfDomains *getGroupOfDomains() const { return _dFS;}
  inline void setfunctionSpaceType(FSType &whatFS) { _whatFS = whatFS;}
  inline FSType getfunctionSpaceType() const { return _whatFS;}
  inline void setBasisFunctionType(BFType &whatBF) { _whatBF = whatBF;}
  inline BFType getBasisFunctionType() const { return _whatBF;}
  virtual void f(MElement *ele, double u, double v, double w, std::vector<TensorialTraits<double>::ValType> &func) = 0; 
  virtual void df(MElement *ele, double u, double v, double w, std::vector<TensorialTraits<double>::GradType> &grads) = 0; 
};

class hmmScalarLagrangeFunctionSpace : public ScalarLagrangeFunctionSpace, public hmmFunctionSpaceBase
{
protected:
  std::map<std::string, hmmDirichlet *> _diriFS;
public:
  hmmScalarLagrangeFunctionSpace(int iField, FSType whatFS = Form0, 
                                 BFType whatBF = BF_Node) : ScalarLagrangeFunctionSpace(iField) { 
    this->_dFS = NULL; 
    this->_whatFS = whatFS;
    this->_whatBF = whatBF;
  }
  hmmScalarLagrangeFunctionSpace(hmmGroupOfDomains &dFS, int iField, FSType whatFS = Form0, 
                                 BFType whatBF = BF_Node) : ScalarLagrangeFunctionSpace(iField) {
    this->_dFS = &dFS; 
    this->_whatFS = whatFS;
    this->_whatBF = whatBF;
  }
  virtual ~hmmScalarLagrangeFunctionSpace(){}
  inline void setConstraint(hmmDirichlet &diriFS) { _diriFS.insert(std::make_pair(diriFS.getTag(), &diriFS) );}
  inline void setConstraint(std::map<std::string, hmmDirichlet *> &diriFS) { _diriFS = diriFS;}
  void setConstraint(std::string tagDiri, double imposedValue);
  inline const std::map<std::string, hmmDirichlet *> *getConstraint() const { return &_diriFS;}
  virtual void f(MElement *ele, double u, double v, double w, std::vector<TensorialTraits<double>::ValType> &func) { 
    this->ScalarLagrangeFunctionSpace::f(ele, u, v, w, func);
    //std::cout <<"f is computed for hmmScalarLagrangeFunctionSpace. "<< std::endl;
    return;
  }
  virtual void df(MElement *ele, double u, double v, double w, std::vector<TensorialTraits<double>::GradType> &grads) { 
    this->ScalarLagrangeFunctionSpace::gradf(ele, u, v, w, grads);
    return;
  }
};

class hmmBFPerpendicularEdge : public hmmScalarLagrangeFunctionSpace
{
protected:
  std::map<std::string, hmmDirichlet *> _diriFS;
public:
  hmmBFPerpendicularEdge(int iField)  : hmmScalarLagrangeFunctionSpace(iField, Form1P, BF_PerpendicularEdge){}
  hmmBFPerpendicularEdge(hmmGroupOfDomains &dFS, int iField) : hmmScalarLagrangeFunctionSpace(dFS, iField, Form1P, BF_PerpendicularEdge){}
//   hmmBFPerpendicularEdge(const hmmBFPerpendiculaEdge &other) {
//     this->_dFS = other._dFS;  this->_whatFS = other._whatFS;  
//     this->_whatBF = other._whatBF; this->_diriFS = other._diriFS;
//   }
  virtual ~hmmBFPerpendicularEdge(){}
  virtual void f(MElement *ele, double u, double v, double w, std::vector<TensorialTraits<double>::GradType> &func) { 
    if(ele->getParent()) ele = ele->getParent();
    int ndofs = ele->getNumShapeFunctions();
    int curpos = func.size();
    func.resize(curpos + ndofs);
    double vals[ndofs];
    ele->getShapeFunctions(u, v, w, vals);
    for (int i = 0; i < ndofs-1; i++) { // FIXME make sure to get back val's in a good order
      double val = vals[i];
      func.push_back(GradType(0.0, 0.0, val) );
    }
    //std::cout <<"f is computed for hmmBFPerpendicularEdge. "<< std::endl;
  }
  virtual void df(MElement *ele, double u, double v, double w, std::vector<TensorialTraits<double>::GradType> &curls);
};

#endif //_HMMFUNCTIONSPACE_H_
