// C++ Interface: terms
//
// Description: 
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef _HMM_FORMULATION_H_
#define _HMM_FORMULATION_H_

#include <math.h>
#include <map>
#include <complex>
#include <vector>
#include <iostream>
#include <iterator>
#include <stdlib.h>
#include "terms.h"
#include "SVector3.h"
#include "Numeric.h"
#include "MElement.h"
#include "functionSpace.h"
#include "groupOfElements.h"
#include "SPoint3.h"
#include "dofManager.h"
#include "fullMatrix.h"
#include "quadratureRules.h"
#include "NonLinearDofManager.h"
#include "hmmDomain.h"
#include "hmmFunction.h"
#include "hmmConstraints.h"
#include "hmmFunctionSpace.h"

typedef enum BType{SBF, GBF, CBF, DBF};

class  hmmBilinearTermBase 
{
protected:
  BType _whatType;
public:
  virtual void get(MElement *ele, int npts, IntPt *GP, fullMatrix<double> &m, fullVector<double> &vv) const = 0;
  //========================================================================================================================================
  virtual void get(MElement *ele, int npts, IntPt *GP, fullMatrix<double> &m, fullVector<double> &vv, fullVector<double> &vv_Old) const = 0;
  //========================================================================================================================================
  inline void setType(BType whatType) {_whatType = whatType; }
  inline BType getType() const { return _whatType; } 
  virtual GaussQuadrature *getQuadrature() const = 0; 
  virtual hmmMaterialLawBase *getMaterialLaw() const = 0;
};

class  hmmLinearTermBase 
{
public:
  virtual hmmScalarLagrangeFunctionSpace *getFunctionSpace1() const = 0;
  virtual hmmGroupOfDomains *getGroupOfDomains() const = 0;
  virtual GaussQuadrature *getQuadrature() const = 0; 
  virtual void get(MElement *ele, int npts, IntPt *GP, fullVector<double> &vv, double freq = 0.0, double currentTime = 0.0) const = 0; //FixMe
};

class hmmBilinearTerm : public hmmBilinearTermBase
{
protected:
  hmmMaterialLawBase *_materialLaw;
  hmmScalarLagrangeFunctionSpace *_space1;
  hmmScalarLagrangeFunctionSpace *_space2;
  hmmGroupOfDomains *_dTerms;
  GaussQuadrature *_integ;
public:
  hmmBilinearTerm( hmmScalarLagrangeFunctionSpace &space1, hmmScalarLagrangeFunctionSpace &space2, 
                   hmmMaterialLawBase &materialLaw, hmmGroupOfDomains &dTerms, GaussQuadrature &integ);
  hmmBilinearTerm( hmmScalarLagrangeFunctionSpace &space1, hmmMaterialLawBase &materialLaw, 
                   hmmGroupOfDomains &dTerms, GaussQuadrature &integ);
  virtual ~hmmBilinearTerm(){}
  inline hmmMaterialLawBase *getMaterialLaw() const { return _materialLaw;}
  inline hmmScalarLagrangeFunctionSpace *getFunctionSpace1() const { return _space1;}
  inline hmmScalarLagrangeFunctionSpace *getFunctionSpace2() const { return _space2;}
  inline hmmGroupOfDomains *getGroupOfDomains() const { return _dTerms;} 
  virtual GaussQuadrature *getQuadrature() const { return _integ;} 

  // 
  //=======================================================================================================
  virtual void get(MElement *ele, int npts, IntPt *GP, fullMatrix<double> &m, fullVector<double> &v) const;
  //===================================================================================================================================
  virtual void get(MElement *ele, int npts, IntPt *GP, fullMatrix<double> &m, fullVector<double> &v,  fullVector<double> &v_Old) const;
  //===================================================================================================================================
};

class hmmLinearTerm : public hmmLinearTermBase // FixMe get
{
protected:
  hmmGroupOfDomains *_dTerms;
  hmmMaterialLawBase *_materialLaw;
  hmmScalarLagrangeFunctionSpace *_space1;
  GaussQuadrature *_integ;
public:
  hmmLinearTerm( hmmScalarLagrangeFunctionSpace &space1, hmmMaterialLawBase &materialLaw, 
                 hmmGroupOfDomains &dTerms, GaussQuadrature &integ);
  virtual ~hmmLinearTerm(){}
  inline hmmMaterialLawBase *getMaterialLaw() const { return _materialLaw;}
  inline hmmScalarLagrangeFunctionSpace *getFunctionSpace1() const { return _space1;}
  inline hmmGroupOfDomains *getGroupOfDomains() const { return _dTerms;} 
  virtual GaussQuadrature *getQuadrature() const { return _integ;} 
  virtual void get(MElement *ele, int npts, IntPt *GP, fullVector<double> &v, double freq = 0.0, double currentTime = 0.0) const;
};

class hmmLinearTermSource : public hmmLinearTermBase // FixMe: _js, _space1, constr, get
{
protected:
  int _tag;
  hmmGroupOfDomains *_dTerms;
  fullVector<double> *_js; 
  hmmScalarLagrangeFunctionSpace *_space1; 
  GaussQuadrature *_integ;
public:
  hmmLinearTermSource( hmmScalarLagrangeFunctionSpace &space1, fullVector<double> &js, 
                       hmmGroupOfDomains &dTerms, GaussQuadrature &integ); 
  hmmLinearTermSource( hmmScalarLagrangeFunctionSpace &space1, fullVector<double> &js, 
                       hmmGroupOfDomains &dTerms, GaussQuadrature &integ, int tag);
  virtual ~hmmLinearTermSource(){}
  inline int getTag() const { return _tag;}
  inline fullVector<double> *getSource() const { return _js;}
  inline hmmScalarLagrangeFunctionSpace *getFunctionSpace1() const { return _space1;}
  inline hmmGroupOfDomains *getGroupOfDomains() const { return _dTerms;} 
  virtual GaussQuadrature *getQuadrature() const { return _integ;} 
  virtual void get(MElement *ele, int npts, IntPt *GP, fullVector<double> &v, double freq = 0.0, double currentTime = 0.0) const;
};

class hmmFormulation
{
protected:
  std::string _tag;
  std::vector<hmmBilinearTerm *> _vectorOfBilinearTerms; // FixMe
  std::vector<hmmLinearTermBase *> _vectorOfLinearTerms;
public:
  hmmFormulation(std::string tag) { _tag = tag;}
  virtual ~hmmFormulation(){}
  inline void setTag(std::string tag) {_tag = tag;}
  inline std::string getTag() const { return _tag; }
  inline const std::vector<hmmBilinearTerm *> *getVectorOfBilinearTerms() const {
    return &_vectorOfBilinearTerms;
  }
  inline const std::vector<hmmLinearTermBase *> *getVectorOfLinearTerms() const {
    return &_vectorOfLinearTerms;
  }
  bool operator == (const hmmFormulation &other) const { //FIXME
    if (_tag == other.getTag() ) return true;
    else return false;
  }
  void addBilinearTerm(hmmScalarLagrangeFunctionSpace &space1, hmmMaterialLawBase &materialLaw, 
                       hmmGroupOfDomains &dTerms, BType whatType, GaussQuadrature &integ);
  void addBilinearTerm(hmmScalarLagrangeFunctionSpace &space1, hmmScalarLagrangeFunctionSpace &space2, 
                       hmmMaterialLawBase &materialLaw, hmmGroupOfDomains &dTerms, BType whatType, GaussQuadrature &integ);
  void addLinearTerm(hmmScalarLagrangeFunctionSpace &space1, 
                     hmmMaterialLawBase &materialLaw, hmmGroupOfDomains &dTerms, GaussQuadrature &integ); //FixMe
  void addLinearTerm(hmmScalarLagrangeFunctionSpace &space1, 
                     fullVector<double> &js, hmmGroupOfDomains &dTerms, GaussQuadrature &integ); // FixMe
  void addLinearTerm(hmmScalarLagrangeFunctionSpace &space1, fullVector<double> &js, 
                     hmmGroupOfDomains &dTerms, GaussQuadrature &integ, int tag); // FixMe
};

#endif// _HMM_FORMULATION_H_
