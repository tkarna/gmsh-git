#include "hmmDomain.h"

hmmDomainBase::hmmDomainBase(groupOfElements &g)  {
  _g        = &g;
  printf("The tag number of the groupOfElements is not specified in this case. ");
  _physical = -1;
}
hmmDomainBase::hmmDomainBase(int dim, int physical) {
  _g        = new groupOfElements(dim, physical);
  _physical = physical;
}
hmmDomainBase::hmmDomainBase(const hmmDomainBase &other) {
  _g        = other.getGroupOfElements();
  _physical = other.getPhysical();
}
hmmDomainBase::hmmDomainBase(GFace &gFace) {
  _g        = new groupOfElements(&gFace);
  _physical = gFace.tag();
}
hmmDomainBase::hmmDomainBase(GRegion &gRegion) {
  _g        = new groupOfElements(&gRegion);
  _physical = gRegion.tag();
}
hmmDomainBase::~hmmDomainBase() {} // FIXME: dispose off properly the pointer "*_g" to avoid having orphan memories
// Method "isIncluded" is used to check if all domains of the current "hmmGroupOfDomains" are included in the group 
// of domains of the hmmgroupOfDomains tagged "other". This is useful in order to check if the "hmmDomainBases" of 
// objects we define are included in the "hmmGroupOfDomains" of objects they include (e.g.: terms of a 
// formulation  may include function and functionspaces (e.g.: a material law). A postprocessing may also  
// include function spaces and a material law )
bool hmmGroupOfDomains::isIncluded(const hmmGroupOfDomains &other) {
  // calculate the intersection of both sets of hmmDomainBase and compare it with the set of this->hmmGroupOfDomains
  // if the intersecting set is equal to the set of this->hmmGroupOfDomains, then return true else return false
  std::set<hmmDomainBase *> intersectSet;
  // Loop over elemnts of both sets and insert "hmmDomainBase *" in IntersectSet is found in both sets.  
  set_intersection(_groupOfDomains.begin(), _groupOfDomains.end(), other.getGroupOfDomains()->begin(), 
                   other.getGroupOfDomains()->end(), inserter(intersectSet, intersectSet.begin() ) );
  if (intersectSet == this->_groupOfDomains) {
    Msg::Error("The group of domains labelled '%s' is included in the group of domains labelled '%s' \n", 
               this->getTagGroupOfDomains().c_str(), other.getTagGroupOfDomains().c_str() );
    return true;
  }
  else {
    Msg::Error("The group of domains labelled '%s' is not included in the group of domains labelled '%s' \n", 
               this->getTagGroupOfDomains().c_str(), other.getTagGroupOfDomains().c_str() );
    return false;
  }
}
bool hmmGroupOfDomains::elementIsIncluded(MElement *ele) const {
  // Method used for checking if an element is included in hmmGroupOfDomains. 
  //Uses the method elementIsIncluded() from hmmDomainsBase.
  const std::set<hmmDomainBase *> *vecOfDom = this->getGroupOfDomains();
  std::set<hmmDomainBase *>::const_iterator itDom;    
  bool eleIsIncluded = false; // default value
  for (itDom = vecOfDom->begin(); itDom != vecOfDom->end(); ++itDom) {
    eleIsIncluded = (*itDom)->elementIsIncluded(ele);
    //return (*itDom)->elementIsIncluded(ele);
  }
  return eleIsIncluded;
}
hmmDomainBase *hmmGroupOfDomains::getHmmDomainBase(int physical) const {
  hmmDomainBase *thisHDB = NULL;
  std::set<hmmDomainBase *>::iterator itHDB;
  for(itHDB = this->itBegin(); itHDB != this->itEnd(); itHDB++) {
    if ( (*itHDB)->getPhysical() == physical) {
      thisHDB = (*itHDB);
    }
  }
  return thisHDB;
}
