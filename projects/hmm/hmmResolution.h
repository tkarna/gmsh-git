//
// C++ Interface: terms
//
// Description:
//
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef _HMM_RESOLUTION_H_
#define _HMM_RESOLUTION_H_

#include <map>
#include <complex>
#include <vector>
#include <iostream>
#include <iterator>
#include "NonLinearDofManager.h"
#include "solverAlgorithms.h"
#include "groupOfElements.h"
#include "MultiscaleNonLinearSystem.h"
#include "quadratureRules.h"
#include "hmmDomain.h"
#include "hmmFunction.h"
#include "hmmConstraints.h"
#include "hmmFunctionSpace.h"
#include "hmmFormulation.h"
#include "hmmSolverAlgorithms.h"

class hmmPostProBase;
 
enum ResType{statDomSolver, freqDomSolver, timeDomSolver};
enum OpType{InitSol, InitSolLin, InitSolLin_H_Form, UpMatLaw, Gen, GenJac, Sol, SolJac, SolJac_H_Form, SaveSol};

class hmmResolutionBase
{
private:
  friend class hmmPostProBase;
protected:
  std::string _tagRes;
  ResType _resoType;
  MultiscaleNonLinearSystemGmm<double> *_linsys;
  NonLinearDofManager<double> *_pAssembler;
  std::map<std::string, GaussQuadrature *> *_allQuadratureRules;
  std::map<std::string, hmmDirichlet *> *_allDirichletBC;
  std::map<std::string, hmmMaterialLawBase *> *_allMaterialLaws;
  std::map<std::string, hmmScalarLagrangeFunctionSpace *> *_allFunctionSpaces;
  std::map<std::string, hmmFormulation *> *_allFormulations;
  int _maxNumIter, _numIter, _lastIteration;
  double _tol, _residu0;
public:
  hmmResolutionBase(std::string tagRes, MultiscaleNonLinearSystemGmm<double> &linsys, NonLinearDofManager<double> &pAssembler, 
                    std::map<std::string, GaussQuadrature *> &allQuadratureRules,
                    std::map<std::string, hmmDirichlet *> &allDirichletBC, 
                    std::map<std::string, hmmMaterialLawBase *> &allMaterialLaws, 
                    std::map<std::string, hmmScalarLagrangeFunctionSpace *> &allFunctionSpaces, 
                    std::map<std::string, hmmFormulation *> &allFormulations, 
                    int maxNumIter, int numIter, double tol, ResType resoType = statDomSolver);
  virtual ~hmmResolutionBase(){}
  inline std::string getTagRes() const { return _tagRes;}
  inline ResType getResType() const { return _resoType;}
  inline int getMaxNumIter() const { return _maxNumIter;}
  inline int getNumIter() const { return _numIter;}
  inline int getTolerance() const { return _tol;}
  inline int setMaxNumIter(int maxNumIter) { _maxNumIter = maxNumIter; return 1; }
  inline int setTolerance(double tol) { _tol = tol; return 1;}
  inline MultiscaleNonLinearSystemGmm<double> *getSystem() const { return _linsys;}
  inline NonLinearDofManager<double> *getDofManager() const { return _pAssembler;}
  inline std::map<std::string, hmmDirichlet *> *getDiriBC() const { return _allDirichletBC;}
  inline std::map<std::string, hmmScalarLagrangeFunctionSpace *> *getFunctionSpace() const {
    return _allFunctionSpaces;
  }
  inline std::map<std::string, hmmFormulation *> *getFormulations() const { return _allFormulations;}
  inline int getLastIterNum() const { return _lastIteration; }
  void writeResidu(std::string residuTxt, double residu, int timestep = 1) const;
  void printMatrix() const; 
  void printRHS() const; 
  void printSolution() const; 
  virtual void fixDirichletBC(std::string tagSpace);
  virtual void fixAllDirichletBC();
  virtual void numberDofs(std::string tagSpace);
  virtual void numberAllDofs();
  virtual void assembleBilinearTerms(std::string tagForm);
  virtual void assembleBilinearTermsJac(std::string tagForm);
  virtual void assembleLinearTerms(std::string tagForm, double freq, double currentTime);
  virtual void assembleSourceLinearTerms(std::string tagForm, double freq, double currentTime);
  //virtual void InitFromSolution(); To fix
  virtual void InitSolution(std::string tagForm);
  virtual void InitSolutionLinear(std::string tagForm);
  virtual void UpdateMatLaw(std::string tagForm); // FixMe delete: for a given timestep, delete the ".pos" files before updating the material law 
  virtual void Generate(std::string tagForm);
  virtual void GenerateJac(std::string tagForm);
  virtual void Solve(std::string tagForm, hmmPostProBase &postPro);
  virtual void SolveJac(std::string tagForm,  hmmPostProBase &_postPro); // FixMe 
  virtual void SaveSolution(std::string tagForm){}
  virtual void setOperation(std::string tagForm, OpType thisOp, hmmPostProBase &postPro);
  void addPostPro(std::string potentialFileName,  std::string primalDerivedFieldFileName, 
                                   std::string dualDerivedFieldFileName,  std::string eddyCurrentFieldFileName,
                                   std::string tagML, std::string tagSpace,  hmmPostProBase &_postPro);
};


class hmmResolutionTimeDomain : public hmmResolutionBase
{
protected:
  double _initTime, _endTime, _dTime, _theta, _currentTime, _freq;
  int _currentTimeStep;
public:
  hmmResolutionTimeDomain(std::string tagRes, MultiscaleNonLinearSystemGmm<double> &linsys, NonLinearDofManager<double> &pAssembler, 
                          std::map<std::string, GaussQuadrature *> &allQuadratureRules,
                          std::map<std::string, hmmDirichlet *> &allDirichletBC, 
                          std::map<std::string, hmmMaterialLawBase *> &allMaterialLaws, 
                          std::map<std::string, hmmScalarLagrangeFunctionSpace *> &allFunctionSpaces, 
                          std::map<std::string, hmmFormulation *> &allFormulations,
                          int maxNumIter, int numIter, double tol) : hmmResolutionBase(tagRes, linsys, pAssembler, allQuadratureRules, allDirichletBC, allMaterialLaws, 
                                                                                       allFunctionSpaces, allFormulations, maxNumIter, numIter, tol, timeDomSolver) {
    _currentTimeStep = 0;
  }
  virtual ~hmmResolutionTimeDomain(){}
  inline void setInitTime(double initTime) { _initTime = initTime; _currentTime = _initTime;}
  inline void setEndTime(double endTime) { _endTime = endTime;}
  inline void setDeltaTime(double dTime) { _dTime = dTime;}
  inline void setTheta(double theta) { _theta = theta;}
  inline void setFrequency(double freq) { _freq = freq;}
  inline double getInitTime() const { return _initTime;}
  inline double getEndTime() const { return _endTime;}
  inline double getDeltaTime() const { return _dTime;}
  inline double getCurrentTime() const { return _currentTime;}
  inline int getCurrentTimeStep() const { return _currentTimeStep;}
  inline double getTheta() const { return _theta;}
  inline double getFrequency() const { return _freq;}
  virtual void updateDirichletBC(std::string tagSpace, std::string tagDiri, double ValueToImpose); // FIX ME
  virtual void updateDirichletBC(std::string tagSpace, std::map<std::string, double> diriToUpdate); // FIX ME
  void printAllSolution() const;
  virtual void assembleBilinearTerms(std::string tagForm) {
    this->hmmResolutionBase::assembleBilinearTerms(tagForm);
  }
  virtual void assembleLinearTerms(std::string tagForm, double freq , double currentTime) {
    this->hmmResolutionBase::assembleLinearTerms(tagForm, freq, currentTime);
  }
  virtual void assembleSourceLinearTerms(std::string tagForm, double freq , double currentTime) {
    this->hmmResolutionBase::assembleSourceLinearTerms(tagForm, freq, currentTime);
  }
  virtual void assembleBilinearTermsJacDt(std::string tagForm);
  virtual void assembleBilinearTermsJacDt_H_Form(std::string tagForm);
  virtual void InitSolutionLinear(std::string tagForm);
  virtual void InitSolutionLinear_H_Form(std::string tagForm);
  virtual void UpdateMatLaw(std::string tagForm);
  virtual void UpdateMatLaw_H_Form(std::string tagForm, bool changedDualPrevValue);
  virtual void Generate(std::string tagForm);
  virtual void GenerateJac(std::string tagForm);
  virtual void GenerateJac_H_Form(std::string tagForm);
  virtual void Solve(std::string tagForm, hmmPostProBase &postPro);
  virtual void Solve_H_Form(std::string tagForm, hmmPostProBase &postPro);
  virtual void SolveJac(std::string tagForm, hmmPostProBase &postPro);
  virtual void SolveJac_H_Form(std::string tagForm, hmmPostProBase &postPro);
  virtual void SaveSolution(std::string tagForm) {
    _pAssembler->addSolution(_currentTime);
  }
  virtual void setOperation(std::string tagForm, OpType thisOp, hmmPostProBase &postPro);
};
#endif// _HMM_RESOLUTION_H_
