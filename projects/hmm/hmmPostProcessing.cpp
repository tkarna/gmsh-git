#include "hmmPostProcessing.h"

hmmPostProBase::hmmPostProBase(GModel &pModel, NonLinearDofManager<double> &pAssembler, 
                               std::map<std::string, hmmMaterialLawBase *> &allMaterialLaws, 
                               std::map<std::string, hmmScalarLagrangeFunctionSpace *> &allFunctionSpaces,  
                               std::map<std::string, hmmFormulation *> &allFormulations, 
                               std::map<std::string, hmmResolutionBase*> &allResolutions, 
                               std::map<std::string, GaussQuadrature *> &allQuadratureRules) {
  _pModel             = &pModel; 
  _pAssembler         = &pAssembler; 
  _allMaterialLaws    = &allMaterialLaws; 
  _allFunctionSpaces  = &allFunctionSpaces; 
  _allFormulations    = &allFormulations;
  _allResolutions     = &allResolutions;
  _allQuadratureRules = &allQuadratureRules; 
}

//void hmmPostProBase::

void hmmPostProBase::fillInDataPrimal_Scalar(std::string tagSpace, std::map<int, std::vector<double> > &data, int physical) const{
  std::vector<Dof> D; double val; std::vector<double> vec(1);
  std::map<std::string, hmmScalarLagrangeFunctionSpace *>::iterator itSpace = _allFunctionSpaces->find(tagSpace);
  // Test if the FS is defined.
  std::set<hmmDomainBase *>::const_iterator itDom;
  std::set<hmmDomainBase *>::const_iterator itBegin;
  std::set<hmmDomainBase *>::const_iterator itEnd;
  if(physical<0) {
    itBegin = itSpace->second->getGroupOfDomains()->getGroupOfDomains()->begin();
    itEnd   = itSpace->second->getGroupOfDomains()->getGroupOfDomains()->end();
  }
  else {
    std::set<hmmDomainBase *>::const_iterator itDomPhysical;
    hmmDomainBase * thisHDBPhysical;
    for (itDomPhysical = itSpace->second->getGroupOfDomains()->getGroupOfDomains()->begin(); 
         itDomPhysical != itSpace->second->getGroupOfDomains()->getGroupOfDomains()->end(); itDomPhysical++) {
      if( (*itDomPhysical)->getPhysical() == physical ) {
        thisHDBPhysical = (*itDomPhysical);
      }
    }
    itBegin = itSpace->second->getGroupOfDomains()->getGroupOfDomains()->find(thisHDBPhysical);
    itEnd = itBegin;
  }
  for (itDom = itBegin; itDom != itEnd; itDom++) {
    for (groupOfElements::elementContainer::const_iterator it = ( (*itDom)->getGroupOfElements()->begin() ); 
         it != ( (*itDom)->getGroupOfElements()->end() ); ++it) {
      MElement *e = *it;
      for(int j = 0; j < e->getNumShapeFunctions(); j++) {
        MVertex *vert = e->getShapeFunctionNode(j);
        _pAssembler->getDofValue(vert->getNum(), itSpace->second->getId(), val); 
        vec[0] = val; data[vert->getNum()] = vec;
      }
    }
  }
}

void hmmPostProBase::fillInDataPrimal_ExtDer(std::string tagSpace, std::map<int, std::vector<double> > &data) const{
  std::vector<Dof> D; double val; std::vector<double> vec;
  std::map<std::string, hmmScalarLagrangeFunctionSpace *>::iterator itSpace = _allFunctionSpaces->find(tagSpace);
  // Test if the FS is defined.
  double grad_az[3];
  std::set<hmmDomainBase *>::const_iterator itDom;
  for (itDom = itSpace->second->getGroupOfDomains()->getGroupOfDomains()->begin(); 
       itDom != itSpace->second->getGroupOfDomains()->getGroupOfDomains()->end(); ++itDom) {
    for (groupOfElements::elementContainer::const_iterator it = ( (*itDom)->getGroupOfElements()->begin() ); 
         it != ( (*itDom)->getGroupOfElements()->end() ); ++it) {
      MElement *e = *it;
      SPoint3 barycenterPoint = e->barycenterUVW();
      double u = barycenterPoint.x();
      double v = barycenterPoint.y(); 
      double w = barycenterPoint.z();
      std::vector<TensorialTraits<double>::GradType> Grads;
      itSpace->second->df(e, u, v, w, Grads);
        grad_az[0] = 0.0; grad_az[1] = 0.0; grad_az[2] = 0.0;
      for(int j = 0; j < e->getNumShapeFunctions(); j++) {
        MVertex *vert = e->getShapeFunctionNode(j);
        _pAssembler->getDofValue(vert->getNum(), itSpace->second->getId(), val);
        grad_az[0] += val * Grads[j][0]; 
        grad_az[1] += val * Grads[j][1]; 
        grad_az[2] += val * Grads[j][2]; 
      }
      vec.clear();
      vec.push_back(grad_az[0]); vec.push_back(grad_az[1]); vec.push_back(0.0); data[e->getNum()] = vec;
    }
  }
}


void hmmPostProBase::fillInDataDual_ExtDer(std::string tagSpace, std::string tagMaterialLaw, std::map<int, std::vector<double> > &data) const {
  std::map<std::string, hmmScalarLagrangeFunctionSpace *>::iterator itSpace = _allFunctionSpaces->find(tagSpace);
  // Test if the FS is defined.
  std::map<std::string, hmmMaterialLawBase *>::iterator itMaterial = _allMaterialLaws->find(tagMaterialLaw);
  // Test if the ML is defined.
  hmmMaterialLawBase *currentML = itMaterial->second;
  std::set<hmmDomainBase *>::const_iterator itDom;
  std::map<hmmGroupOfDomains *, MLQtyBase *>::const_iterator itGoDMQB ; 
  for ( itGoDMQB = currentML->getMatLawByGroupOfDomains()->begin(); 
        itGoDMQB != currentML->getMatLawByGroupOfDomains()->end(); ++itGoDMQB) { 
    for (itDom = itGoDMQB->first->getGroupOfDomains()->begin(); 
         itDom != itGoDMQB->first->getGroupOfDomains()->end(); ++itDom) {
      for (groupOfElements::elementContainer::const_iterator it = ( (*itDom)->getGroupOfElements()->begin() ); 
           it != ( (*itDom)->getGroupOfElements()->end() ); ++it) {
        MElement *ele = *it;
        HomQtyLinear *currentMLQtyBase = new HomQtyLinear();
        bool isBuilt; 
        isBuilt = meanEleHomQtyLinear(currentML, ele, *currentMLQtyBase, itGoDMQB);
        computeDualFieldOnElement(itSpace->second, ele, data, *currentMLQtyBase, isBuilt);
        //void hmmPostProBase::computeDualFieldOnElement(hmmScalarLagrangeFunctionSpace *funcSpace, MElement *ele, std::map<int, std::vector<double> > &data, 
        //HomQtyLinear &currentMLQtyBase, bool isBuilt) const {
      }
    }
  }
}

bool hmmPostProBase::meanEleHomQtyLinear(hmmMaterialLawBase *currentML, MElement *ele, HomQtyLinear &currentMLQtyBase, 
                                         std::map<hmmGroupOfDomains *, MLQtyBase *>::const_iterator itGoDMQB) const {
  // Compute the average of the constitutive law matrix over the element
  //======================================================================= 
  bool isBuilt; // is true if the material law is defined on the element
  if(currentML == NULL) {
    Msg::Error("Can not compute the dual field (in hmmPostProcessing) because the material law does not exist!");
    isBuilt = false;
  }
  else {
    if(currentML->getType() == HMM_NONLINEAR_MLAW) {
      hmmMicroSolverGetDPNonLinearDouble *convCurrentNLML = (hmmMicroSolverGetDPNonLinearDouble *)(currentML);
      HomQtyNL *currentQtyBase = (HomQtyNL *)(convCurrentNLML->getMaterialLaw( *(itGoDMQB->first ) ) );
      std::map<MElement*, std::map<int, HomQtyLinear *> >::iterator itEle = currentQtyBase->getMaterialLaw()->find(ele);
      // Test if the MQB is defined on element 'e'
      if (itEle != currentQtyBase->getMaterialLaw()->end() ) {
        std::map<int, HomQtyLinear *>::iterator itEleML;
        for (itEleML = itEle->second.begin(); itEleML != itEle->second.end(); ++itEleML) {
          HomQtyLinear *thisMQB = convCurrentNLML->getMaterialLawAtGaussPoint(*(itGoDMQB->first), ele, itEleML->first);
          //currentMLQtyBase->addToVector( *(itEleML->second->getVector() ) );
          currentMLQtyBase.addToMatrix(*(thisMQB->getMatrix() ) );
        }
        currentMLQtyBase.getMatrix()->scale( (1.0/(itEle->second.size() ) ) );
        currentMLQtyBase.printMLQty();
        isBuilt = true;
      }
      else {
        Msg::Error("Can not compute the dual field (in hmmPostProcessing). The nonlinear material law is not properly defined.");
        isBuilt = false;
      }
    }
    else if (currentML->getType() == HMM_LINEAR_MLAW) {
      hmmMicroSolverGetDPLinearDouble *currentLML = (hmmMicroSolverGetDPLinearDouble *)(currentML);
      currentMLQtyBase = (HomQtyLinear &)( *(currentLML->getMaterialLaw(ele) ) );
      isBuilt = true;
    }
    else if (currentML->getType() == LINEAR_MLAW) {
      hmmLinearMaterialLaw *currentLML = (hmmLinearMaterialLaw *)(currentML);
      currentMLQtyBase = (HomQtyLinear &)( *(currentLML->getMaterialLaw(ele) ) );
      isBuilt = true;
    }
    else {
      Msg::Error("Can not compute the dual field (in hmmPostProcessing). This material law is not implemented yet!");
      isBuilt = false;
    }
  }
  return isBuilt;
}

 void hmmPostProBase::computeDualFieldOnElement(hmmScalarLagrangeFunctionSpace *funcSpace, MElement *ele, std::map<int, std::vector<double> > &data,
                                                HomQtyLinear &currentMLQtyBase, bool isBuilt) const {
  std::vector<double> vec;
  if(isBuilt != false) { // used if the meanHomQtyLinear has been calculated    
    // 1. Compute all the external derivatives at the barycenter for element 'e'.
    //===========================================================================
    SPoint3 barycenterPoint = ele->barycenterUVW();
    double u = barycenterPoint.x(); 
    double v = barycenterPoint.y(); 
    double w = barycenterPoint.z();
    std::vector<TensorialTraits<double>::GradType> Grads;
    funcSpace->df(ele, u, v, w, Grads);
    
    double ext_der[3]; ext_der[0] = 0.0; ext_der[1] = 0.0; ext_der[2] = 0.0;
    
    // 2. Contributions from all vertices of an element. valid for BFNode and BFPerp BF.
    //==================================================================================
    double val; 
    for(int j = 0; j < ele->getNumShapeFunctions(); j++) {
      MVertex *vert = ele->getShapeFunctionNode(j);
      //_pAssembler->getDofValue( Dof(vert->getNum(), itSpace->second->getId() ), val);
      _pAssembler->getDofValue( Dof(vert->getNum(), funcSpace->getId() ), val);
      ext_der[0] += val * ( currentMLQtyBase.getMatrix()->get(0,0) * Grads[j][0] + 
                            currentMLQtyBase.getMatrix()->get(0,1) * Grads[j][1] + 
                            currentMLQtyBase.getMatrix()->get(0,2) * Grads[j][2]);
      ext_der[1] += val * ( currentMLQtyBase.getMatrix()->get(1,0) * Grads[j][0] + 
                            currentMLQtyBase.getMatrix()->get(1,1) * Grads[j][1] + 
                            currentMLQtyBase.getMatrix()->get(1,2) * Grads[j][2]);
      ext_der[2] += val * ( currentMLQtyBase.getMatrix()->get(2,0) * Grads[j][0] + 
                            currentMLQtyBase.getMatrix()->get(2,1) * Grads[j][1] + 
                            currentMLQtyBase.getMatrix()->get(2,2) * Grads[j][2]);
    }
    vec.clear();
    vec.push_back(ext_der[0]); vec.push_back(ext_der[1]); vec.push_back(ext_der[2]); data[ele->getNum()] = vec;
  }
  else {
    Msg::Error("The material law is not defined on element");
  }
}

bool hmmPostProBase::buildPotentialView(std::string tagSpace, std::string fieldFileName, 
                                           int _iteration, std::string location) const {
  std::cout <<  "Building the the field View ..."<< std::endl;
  std::map<int, std::vector<double> > data;
  fillInDataPrimal_Scalar(tagSpace, data);
  // Building the PView
  //===================
  std::string s; std::stringstream out; out << _iteration; s = out.str();
  std::string extension (".pos");
  fieldFileName += s; location += s; 
  location += extension;
  PView *pv = new PView(fieldFileName, "NodeData", _pModel, data, 0.0);
  pv->getData()->writeMSH(location);
  return true;
}

double hmmPostProBase::buildMagEnergyView_a_v_formulation(double &totalEnergy, std::string tagML, std::string tagQuad, std::string tagSpace, std::string tagGoD, 
                                                          double dTime, int currentTimeStep, double currentTime, std::string fileName, bool writeResults) {
  double val = 0.0;
  std::map<std::string, hmmMaterialLawBase *>::iterator itML = _allMaterialLaws->find(tagML);
  std::map<std::string, hmmScalarLagrangeFunctionSpace *>::iterator itSpace = _allFunctionSpaces->find(tagSpace);
  hmmScalarLagrangeFunctionSpace *funcSpace = itSpace->second;
  //getGroupOfDomainsForElement(MElement *ele)
  if(itML != _allMaterialLaws->end() ) {
    hmmMaterialLawBase *matLaw = itML->second;
    hmmGroupOfDomains *thisGoD = matLaw->getGroupOfDomains(tagGoD);
    std::set<hmmDomainBase *>::iterator itSetDomains;
    for(itSetDomains = thisGoD->itBegin(); itSetDomains != thisGoD->itEnd(); ++itSetDomains) {
      hmmDomainBase *thisDomain = *(itSetDomains);
      std::set<MElement *>::const_iterator itSetEle;
      for(itSetEle = thisDomain->getGroupOfElements()->begin(); itSetEle != thisDomain->getGroupOfElements()->end(); ++itSetEle) {
        MElement *e = *(itSetEle);
        std::map<std::string, GaussQuadrature *>::iterator itQuad = _allQuadratureRules->find(tagQuad);
        if (itQuad != _allQuadratureRules->end() ) {
          val += buildMagEnergyView_a_v_formulation(matLaw, funcSpace, itQuad->second, thisGoD, dTime, e);
          Msg::Info("Done in hmmPostProcessing while calculating the total magnetic energy using a-v formulation. Energy for element %d is %f. ", e->getNum(), val);

        }
        else {
          Msg::Error("Done in hmmPostProcessing while calculating the total magnetic energy using a-v formulation. The quadrature labelled %s has not been found :-(. ", tagQuad.c_str() );
        }
      }
    }
  }
  else {
    Msg::Error("Done in hmmFunction. The materialLaw labelled %s has not been found :-(. \n", tagML.c_str() );
  }
  totalEnergy +=val;
  if(writeResults) {
     FILE *fp_JouleLosses;
     fp_JouleLosses = fopen(fileName.c_str() , "a") ;
     fprintf(fp_JouleLosses, "%d %g %g \n" , currentTimeStep, currentTime, totalEnergy);
     fclose(fp_JouleLosses) ;
  }
  return val;
}

double hmmPostProBase::buildMagEnergyView_a_v_formulation(hmmMaterialLawBase *matLaw, hmmScalarLagrangeFunctionSpace *funcSpace,  
                                                          GaussQuadrature *GaussQuad,  hmmGroupOfDomains *thisGoD, double dTime, MElement *e) {
  double eleEnergy = 0.0;
  double energyPerGP;
  double val = 0.0; 
  double val_prev = 0.0; 
  double grad_az[3];
  double grad_az_prev[3];
  if ( (matLaw->getType() == LINEAR_MLAW) || (matLaw->getType() == HMM_LINEAR_MLAW) ) {
    HomQtyLinear *HQL = (HomQtyLinear *) (matLaw->getMaterialLaw(e) );
    IntPt *GP; 
    int npts = GaussQuad->getIntPoints(e, &GP);
    std::vector<TensorialTraits<double>::GradType> Grads;
    double jac[3][3];
    for (int i = 0; i < npts; i++) { // contribution for different Gauss Points (x, y, z) <==> (u, v, w).
      const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
      const double weight = GP[i].weight; const double detJ = e->getJacobian(u, v, w, jac);
      funcSpace->df(e, u, v, w, Grads);
      grad_az[0]      = 0.0; grad_az[1]      = 0.0; grad_az[2]      = 0.0; // initialization of the external derivative for GP "i"
      grad_az_prev[0] = 0.0; grad_az_prev[1] = 0.0; grad_az_prev[2] = 0.0; // initialization of the external derivative for GP "i"
      for(int j = 0; j < e->getNumShapeFunctions(); j++) {  // for a given GP, contribution from different nodes.
        energyPerGP = 0.0;
        val = 0.0; val_prev = 0.0;
        MVertex *vert = e->getShapeFunctionNode(j);
        _pAssembler->getDofValue(vert->getNum(), funcSpace->getId(), val);
        _pAssembler->getDofValue(vert->getNum(), funcSpace->getId(), 1, val_prev);
        grad_az[0] += val * Grads[j][0]; 
        grad_az[1] += val * Grads[j][1];  
        grad_az[2] += val * Grads[j][2]; 

        grad_az_prev[0] += val_prev * Grads[j][0]; 
        grad_az_prev[1] += val_prev * Grads[j][1];  
        grad_az_prev[2] += val_prev * Grads[j][2]; 
      }
      fullVector<double> *thisVecGrads = new fullVector<double>(3);
      thisVecGrads->set(0, grad_az[0]); 
      thisVecGrads->set(1, grad_az[1]); 
      thisVecGrads->set(2, grad_az[2]); 
      
      fullVector<double> *thisVec_Dt_Grads = new fullVector<double>(3);// will contain \partial_t b
      thisVec_Dt_Grads->set(0, (grad_az[0] - grad_az_prev[0])/dTime ); 
      thisVec_Dt_Grads->set(1, (grad_az[1] - grad_az_prev[1])/dTime ); 
      thisVec_Dt_Grads->set(2, (grad_az[2] - grad_az_prev[2])/dTime ); 
      
      fullVector<double> *thisVecResult = new fullVector<double>(3);// will contain h = nu * b
      thisVecResult->set(0, 0.0); 
      thisVecResult->set(1, 0.0); 
      thisVecResult->set(2, 0.0); 
      fullMatrix<double> *thisMatrix = HQL->getMatrix();
      thisMatrix->mult(*thisVecGrads, *thisVecResult); 
      energyPerGP = ( (*thisVec_Dt_Grads)(0) * (*thisVecResult)(0) ) + ( (*thisVec_Dt_Grads)(1) * (*thisVecResult)(1) ) + ( (*thisVec_Dt_Grads)(2) * (*thisVecResult)(2) ); 
      eleEnergy += detJ * weight * energyPerGP; 
      delete thisVec_Dt_Grads;
      delete thisVecResult;
    }
  }
  else if(matLaw->getType() == HMM_NONLINEAR_MLAW) {
    hmmMicroSolverGetDPNonLinearDouble *currentML = (hmmMicroSolverGetDPNonLinearDouble *)(matLaw);
    IntPt *GP; 
    int npts = GaussQuad->getIntPoints(e, &GP);
    std::vector<TensorialTraits<double>::GradType> Grads;
    double jac[3][3];
    for (int i = 0; i < npts; i++) { // contribution for different Gauss Points (x, y, z) <==> (u, v, w).
      int numGP = i;
      HomQtyLinear *HQL = currentML->getMaterialLawAtGaussPoint(*thisGoD, e, numGP);
      const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
      const double weight = GP[i].weight; const double detJ = e->getJacobian(u, v, w, jac);
      funcSpace->df(e, u, v, w, Grads);
      grad_az[0]      = 0.0; grad_az[1]      = 0.0; grad_az[2]      = 0.0; // initialization of the external derivative for GP "i"
      grad_az_prev[0] = 0.0; grad_az_prev[1] = 0.0; grad_az_prev[2] = 0.0; // initialization of the external derivative for GP "i"
      for(int j = 0; j < e->getNumShapeFunctions(); j++) {  // for a given GP, contribution from different nodes.
        energyPerGP = 0.0;
        MVertex *vert = e->getShapeFunctionNode(j);
        _pAssembler->getDofValue(vert->getNum(), funcSpace->getId(), val);
        _pAssembler->getDofValue(vert->getNum(), funcSpace->getId(), 1, val_prev);

        grad_az[0] += val * Grads[j][0]; 
        grad_az[1] += val * Grads[j][1];  
        grad_az[2] += val * Grads[j][2]; 

        grad_az_prev[0] += val_prev * Grads[j][0]; 
        grad_az_prev[1] += val_prev * Grads[j][1];  
        grad_az_prev[2] += val_prev * Grads[j][2]; 
      }
      fullVector<double> *thisVec_Dt_Grads = new fullVector<double>(3);
      thisVec_Dt_Grads->set(0, (grad_az[0] - grad_az_prev[0])/dTime ); 
      thisVec_Dt_Grads->set(1, (grad_az[1] - grad_az_prev[1])/dTime ); 
      thisVec_Dt_Grads->set(2, (grad_az[2] - grad_az_prev[2])/dTime ); 
      
      fullVector<double> *thisVecDual = HQL->getVector();
      thisVec_Dt_Grads->print("magField");
      thisVecDual->print("magInduct");
      energyPerGP = ( (*thisVec_Dt_Grads)(0) * (*thisVecDual)(0) ) + ( (*thisVec_Dt_Grads)(1) * (*thisVecDual)(1) ) + ( (*thisVec_Dt_Grads)(2) * (*thisVecDual)(2) ); 
      eleEnergy += detJ * weight * energyPerGP; 
      delete thisVec_Dt_Grads;
    }
  }
  else {
    Msg::Error("Done in hmmPostProcessing. The computationa of magnetic energy for the material law tagged %s has not been implemented :-(", matLaw->getTag().c_str() );
  }
  return eleEnergy;
}

bool hmmPostProBase::buildViewFromMap(std::map<int, std::vector<double> > data, std::string fieldFileName, 
                                      int _iteration, std::string location, std::string DataType) const {
  Msg::Info("Done in hmmPostPro while building the field view named %s ...", fieldFileName.c_str() );
  // Building the PView
  //===================
  std::string s; std::stringstream out; out << _iteration; s = out.str();
  std::string extension (".pos");
  fieldFileName += s; location += s; 
  location += extension;
  PView *pv = new PView(fieldFileName, DataType, _pModel, data, 0.0);
  pv->getData()->writeMSH(location);
  return true;
}

bool hmmPostProBase::buildPrimalDerivedFieldView(std::string tagSpace, std::string fieldFileName, 
                                                   int _iteration, std::string location) const {
  std::cout <<  "building the primal flux density view using the new format ..."<< std::endl;
  std::map<int, std::vector<double> > data;
  fillInDataPrimal_ExtDer(tagSpace, data);
  // Building the PView
  //===================
  std::string s; std::stringstream out; out << _iteration; s = out.str();
  std::string extension (".pos");
  fieldFileName += s; location += s; location += extension;
  PView *pv = new PView (fieldFileName, "ElementData", _pModel, data, 0.0, 3);
  pv->getData()->writeMSH(location);
  return true;
}

bool hmmPostProBase::buildPrimalDerivedFieldView_OldFormat(std::string tagSpace, std::string fieldFileName, 
                                                           int _iteration, std::string location) const {
  std::cout <<  "building the primal flux density view using the old format ..."<< std::endl;
  std::string s; std::stringstream out; out << _iteration; s = out.str();
  std::string extension (".pos");
  fieldFileName += s; location += s; location += extension;
  FILE *fp; fp = fopen(location.c_str(), "w");
  fprintf(fp, "View \"%s\" {\n", fieldFileName.c_str() );
  std::vector<Dof> D; double val; std::vector<double> vec; std::map<int, std::vector<double> > data;
  std::map<std::string, hmmScalarLagrangeFunctionSpace *>::iterator itSpace = _allFunctionSpaces->find(tagSpace);
  // Test if the FS is defined.
  double grad_az[3];
  std::set<hmmDomainBase *>::const_iterator itDom;
  for (itDom = itSpace->second->getGroupOfDomains()->getGroupOfDomains()->begin(); 
       itDom != itSpace->second->getGroupOfDomains()->getGroupOfDomains()->end(); ++itDom) {
    for (groupOfElements::elementContainer::const_iterator it = ( (*itDom)->getGroupOfElements()->begin() ); 
         it != ( (*itDom)->getGroupOfElements()->end() ); ++it) {
      MElement *e = *it;
      SPoint3 barycenterPoint = e->barycenterUVW();
      double u = barycenterPoint.x(); double v = barycenterPoint.y();  double w = barycenterPoint.z();
      std::vector<TensorialTraits<double>::GradType> Grads;
      itSpace->second->df(e, u, v, w, Grads);
      grad_az[0] = 0.0; grad_az[1] = 0.0; grad_az[2] = 0.0;
      for(int j = 0; j < e->getNumShapeFunctions(); j++) {
        MVertex *vert = e->getShapeFunctionNode(j);
        _pAssembler->getDofValue(vert->getNum(), itSpace->second->getId(), val);
        grad_az[0] += val * Grads[j][0]; grad_az[1] += val * Grads[j][1];  grad_az[2] += val * Grads[j][2]; 
      }
      vec.clear();
      vec.push_back(grad_az[0]); vec.push_back(grad_az[1]); vec.push_back(grad_az[2]); 
      SPoint3 currentPoint;
      e->pnt(u, v, w, currentPoint);
      fprintf(fp, " VP(%g, %g, %g){ %g, %g, %g};\n ", currentPoint.x(), currentPoint.y(), currentPoint.z(), vec[0], vec[1], vec[2]);
    }
  }
  fprintf(fp, " }; ");
  fclose(fp);
  return true;
}

bool hmmPostProBase::buildDualDerivedFieldView(std::string tagSpace, std::string tagMaterialLaw, std::string fieldFileName, 
                                               int _iteration, std::string location) const {
  bool isBuilt = false; // return true if the view is built and false otherwise. Set to false by default
  std::cout <<  "building the dual flux density view in the non-multiscale domain ..."<< std::endl;
  std::vector<Dof> D; 
  std::map<int, std::vector<double> > data;
  fillInDataDual_ExtDer(tagSpace, tagMaterialLaw, data);

  std::string s; std::stringstream out; out << _iteration; s = out.str();
  std::string extension (".pos");
  fieldFileName += s; location += s; location += extension;
  std::string macro = fieldFileName + "_Macro";
  PView *pv = new PView (macro, "ElementData", _pModel, data, 0.0, 3);
  pv->getData()->writeMSH(location);
  return isBuilt;
}

bool hmmPostProBase::buildDualDerivedFieldView_HMM(std::string tagSpace, std::string tagMaterialLaw, 
                                                   std::string fieldFileName, int _iteration, std::string location) const {
  bool isBuilt = false ; // return true if the view is built and false otherwise. Default value = false.
  std::cout <<  "building the dual flux density view in the multiscale domain ..."<< std::endl;
  std::vector<Dof> D; 
  std::vector<double> vec; std::map<int, std::vector<double> > data;
  std::map<std::string, hmmScalarLagrangeFunctionSpace *>::iterator itSpace = _allFunctionSpaces->find(tagSpace);
  // Test if the FS is defined. ???????????????
  std::map<std::string, hmmMaterialLawBase *>::iterator itMaterial = _allMaterialLaws->find(tagMaterialLaw);
  // Test if the ML is defined. ???????????????
  hmmMaterialLawBase *currentML = itMaterial->second;
  std::set<hmmDomainBase *>::const_iterator itDom;
  
  std::map<hmmGroupOfDomains *, MLQtyBase *>::const_iterator itGoDMQB ; 
  hmmMicroSolverGetDPNonLinearDouble * nonlinML = (hmmMicroSolverGetDPNonLinearDouble *)(currentML);
  for ( itGoDMQB = nonlinML->getMatLawByGroupOfDomains()->begin(); 
        itGoDMQB != nonlinML->getMatLawByGroupOfDomains()->end(); ++itGoDMQB) { 
    for (itDom = itGoDMQB->first->getGroupOfDomains()->begin(); 
         itDom != itGoDMQB->first->getGroupOfDomains()->end(); ++itDom) {
      for (groupOfElements::elementContainer::const_iterator it = ( (*itDom)->getGroupOfElements()->begin() ); 
           it != ( (*itDom)->getGroupOfElements()->end() ); ++it) {
        // Loop over all the MElements of a hmmDomainsBase
        MElement *e = *it;
        HomQtyLinear *currentMLQtyBase = new HomQtyLinear();


        if(currentML->getType() == HMM_NONLINEAR_MLAW) {
          hmmMicroSolverGetDPNonLinearDouble *convCurrentML = (hmmMicroSolverGetDPNonLinearDouble *)(currentML);
          //HomQtyNL *currentQtyBase = (HomQtyNL *)(convCurrentML->getMaterialLaw( *(itSpace->second->getGroupOfDomains() ) ) );
          HomQtyNL *currentQtyBase = (HomQtyNL *)(convCurrentML->getMaterialLaw( *(itGoDMQB->first) ) );
          std::map<MElement*, std::map<int, HomQtyLinear *> >::iterator itEle = currentQtyBase->getMaterialLaw()->find(e);
          std::map<int, HomQtyLinear *>::iterator itEleML;
          for (itEleML = itEle->second.begin(); itEleML != itEle->second.end(); ++itEleML) {
            HomQtyLinear *thisMQB = convCurrentML->getMaterialLawAtGaussPoint(*(itGoDMQB->first), e, itEleML->first);
            //currentMLQtyBase->addToVector( *(itEleML->second->getVector() ) );
            currentMLQtyBase->addToVector(*(thisMQB->getVector() ) );
          }
          currentMLQtyBase->getVector()->scale(1.0/(itEle->second.size() ) );
          isBuilt = true;
        }


        else {
          std::string  MLType;
          if (currentML->getType() == LINEAR_MLAW) {
            MLType = "LINEAR_MLAW";
          }
          else if (currentML->getType() == HMM_LINEAR_MLAW) {
            MLType = "HMM_LINEAR_MLAW";
          }
          else {
            MLType = "UNKNOWN_MLAW";
          }
          Msg::Error("In hmmPostProcessing. The method 'buildDualDerivedFieldView_hM' can not be called for '%s' material law type." 
                     "This method is only implemented for two-scale nonlinear material laws. ", MLType.c_str() );
          // isBuilt = false; default value.
        }
        double hM_az[3]; hM_az[0] = 0.0; hM_az[1] = 0.0; hM_az[2] = 0.0;
        hM_az[0] = ( *(currentMLQtyBase->getVector() ) )(0);
        hM_az[1] = ( *(currentMLQtyBase->getVector() ) )(1);
        hM_az[2] = ( *(currentMLQtyBase->getVector() ) )(2);
        vec.clear();
        vec.push_back(hM_az[0]); vec.push_back(hM_az[1]); vec.push_back(hM_az[2]); data[e->getNum()] = vec;
        delete currentMLQtyBase; 
      }
    }
  }
  std::string s; std::stringstream out; out << _iteration; s = out.str();
  std::string extension (".pos");
  fieldFileName += s; location += s; location += extension;
  std::string macro = fieldFileName + "_Macro";
  PView *pv = new PView (macro, "ElementData", _pModel, data, 0.0, 3);
  pv->getData()->writeMSH(location);
  return isBuilt;
}

bool hmmPostProBase::buildElectricCurrentView(std::string tagSpace, std::string tagReso, std::string fieldFileName, 
                                          int _iteration, std::string location) const { 
  bool isBuilt; // return value for the method
  std::cout <<  "building the electric field View ..."<< std::endl;
  std::vector<Dof> D; double val; double valPrev;
  std::vector<double> vec; std::map<int, std::vector<double> > data;
  std::map<std::string, hmmScalarLagrangeFunctionSpace *>::iterator itSpace = _allFunctionSpaces->find(tagSpace);
  // Test if the FS is defined.
  if (itSpace != _allFunctionSpaces->end() ) {
      std::map<std::string, hmmResolutionBase *>::iterator itReso = _allResolutions->find(tagReso);
      // Test if the Resolution is defined and is either harmonic or time dependent
      if ( (itReso != _allResolutions->end() ) && ( (itReso->second->getResType() == freqDomSolver) || (itReso->second->getResType() == timeDomSolver) ) ) {
        std::set<hmmDomainBase *>::const_iterator itDom;
        for (itDom = itSpace->second->getGroupOfDomains()->getGroupOfDomains()->begin(); 
             itDom != itSpace->second->getGroupOfDomains()->getGroupOfDomains()->end(); ++itDom) {
          for (groupOfElements::elementContainer::const_iterator it = ( (*itDom)->getGroupOfElements()->begin() ); 
               it != ( (*itDom)->getGroupOfElements()->end() ); ++it) {
            MElement *e = *it;
            int nbFF = itSpace->second->getNumKeys(e);
            fullMatrix<double> N(3, nbFF); // used for storing the values of the BF at the point of interest. 
            for(int j = 0; j < e->getNumShapeFunctions(); j++) {
              // Compute all the external derivatives for the element 'e'
              SPoint3 barycenterPoint = e->barycenterUVW();
              double u = barycenterPoint.x(); 
              double v = barycenterPoint.y(); 
              double w = barycenterPoint.z();
              std::vector<TensorialTraits<double>::ValType> func;
              itSpace->second->f(e, u, v, w, func);
              for(int j = 0; j < nbFF; j++) {
                N(0, j)  = 0.0;
                N(1, j)  = 0.0;
                N(2, j)  = func[j];
              }
              double e_az[3]; e_az[0] = 0.0; e_az[1] = 0.0; e_az[2] = 0.0;
              
              // Contributions from all vertices of an element. valid for BFNode and BFPerp BF.
              for(int j = 0; j < e->getNumShapeFunctions(); j++) {
                MVertex *vert = e->getShapeFunctionNode(j);
                int stepsBack = 1;
                _pAssembler->getDofValue( Dof(vert->getNum(), itSpace->second->getId() ), val);
                _pAssembler->getDofValue( Dof(vert->getNum(), itSpace->second->getId() ), stepsBack, valPrev);
                e_az[0] += (val- valPrev) * N(2, j) ;
                e_az[1] += (val- valPrev) * N(2, j) ;
                e_az[2] += (val- valPrev) * N(2, j) ;
              }
              vec.clear();
              vec.push_back(e_az[0]); vec.push_back(e_az[1]); vec.push_back(e_az[2]); data[e->getNum()] = vec;
            }
          }
        }
        std::string s; std::stringstream out; out << _iteration; s = out.str();
        std::string extension (".pos");
        fieldFileName += s; location += s; location += extension;
        PView *pv = new PView (fieldFileName, "ElementData", _pModel, data, 0.0);
        pv->getData()->writeMSH(location);
        isBuilt = true;
      }
      else {
        Msg::Error("Done in hmmPostProcessing. Can not build eddy current view for a static solver");
        isBuilt = false;
      }
  }
  else {
    Msg::Error("Done in hmmPostProcessing. Can not build eddy current view '%s'. The function space '%s' is not defined.", 
               fieldFileName.c_str(), tagSpace.c_str() );
    isBuilt = false;
    
  }
  return isBuilt;
}

bool hmmPostProBase::buildEddyCurrentView(std::string tagSpace, std::string tagML, std::string tagReso, std::string fieldFileName,
                                          int _iteration, std::string location) const { 
  bool isBuilt; // return value for the method
  std::cout <<  "building the eddy current field View ..."<< std::endl;
  std::vector<Dof> D; double val; double valPrev;
  std::vector<double> vec; std::map<int, std::vector<double> > data;
  std::map<std::string, hmmScalarLagrangeFunctionSpace *>::iterator itSpace = _allFunctionSpaces->find(tagSpace);
  // Test if the FS is defined.
  if (itSpace != _allFunctionSpaces->end() ) {
    std::map<std::string, hmmMaterialLawBase *>::iterator itML = _allMaterialLaws->find(tagML);
    // Test if the ML is defined.
    if (itML != _allMaterialLaws->end() ) {
      std::map<std::string, hmmResolutionBase *>::iterator itReso = _allResolutions->find(tagReso);
      // Test if the Resolution is defined and is either harmonic or time dependent
      if ( (itReso != _allResolutions->end() ) && ( (itReso->second->getResType() == freqDomSolver) || (itReso->second->getResType() == timeDomSolver) ) ) {
        std::set<hmmDomainBase *>::const_iterator itDom;
        std::map<hmmGroupOfDomains *, MLQtyBase *>::const_iterator itGoDMQB ; 
        for ( itGoDMQB = itML->second->getMatLawByGroupOfDomains()->begin(); 
              itGoDMQB != itML->second->getMatLawByGroupOfDomains()->end(); ++itGoDMQB) { 
          for (itDom = itGoDMQB->first->getGroupOfDomains()->begin(); 
               itDom != itGoDMQB->first->getGroupOfDomains()->end(); ++itDom) {
            for (groupOfElements::elementContainer::const_iterator it = ( (*itDom)->getGroupOfElements()->begin() ); 
                 it != ( (*itDom)->getGroupOfElements()->end() ); ++it) {
              MElement *e = *it;
              int nbFF = itSpace->second->getNumKeys(e);
              HomQtyLinear *MQB;
              MQB = (HomQtyLinear *) (itML->second->getMaterialLaw(e) ); //FixMe
              fullMatrix<double> N(3, nbFF); // used for storing the values of the BF at the point of interest. 
              for(int j = 0; j < e->getNumShapeFunctions(); j++) {
                
                // Compute all the external derivatives for the element 'e'
                SPoint3 barycenterPoint = e->barycenterUVW();
                double u = barycenterPoint.x(); 
                double v = barycenterPoint.y(); 
                double w = barycenterPoint.z();
                std::vector<TensorialTraits<double>::ValType> func;
                itSpace->second->f(e, u, v, w, func);
                for(int j = 0; j < nbFF; j++) {
                  N(0, j)  = 0.0;
                  N(1, j)  = 0.0;
                  N(2, j)  = func[j];
                }
                double h_az[3]; h_az[0] = 0.0; h_az[1] = 0.0; h_az[2] = 0.0;
                
                // Contributions from all vertices of an element. valid for BFNode and BFPerp BF.
                for(int j = 0; j < e->getNumShapeFunctions(); j++) {
                  MVertex *vert = e->getShapeFunctionNode(j);
                  _pAssembler->getDofValue( Dof(vert->getNum(), itSpace->second->getId() ), val);
                  _pAssembler->getDofValue( Dof(vert->getNum(), itSpace->second->getId() ), 1, valPrev);

                  h_az[2] += (val- valPrev) * ( MQB->getMatrix()->get(2,0) * N(2, j)  + 
                                                MQB->getMatrix()->get(2,1) * N(2, j)  + 
                                                MQB->getMatrix()->get(2,2) * N(2, j) );
                }
                vec.clear();
                vec.push_back(h_az[0]); vec.push_back(h_az[1]); vec.push_back(h_az[2]); data[e->getNum()] = vec;
              }
            }
          }
        }
        std::string s; std::stringstream out; out << _iteration; s = out.str();
        std::string extension (".pos");
        fieldFileName += s; location += s; location += extension;
        PView *pv = new PView (fieldFileName, "ElementData", _pModel, data, 0.0);
        pv->getData()->writeMSH(location);
        isBuilt = true;
      }
      else {
        Msg::Error("Done in hmmPostProcessing. Can not build eddy current view for a static solver");
        isBuilt = false;
      }
    } 
    else {
      Msg::Error("Done in hmmPostProcessing. Can not build eddy current view '%s'. The material law '%s' is not defined.", 
                 fieldFileName.c_str(), tagML.c_str() );
      isBuilt = false;
      
    }
  }
  else {
    Msg::Error("Done in hmmPostProcessing. Can not build eddy current view '%s'. The function space '%s' is not defined.", 
               fieldFileName.c_str(), tagSpace.c_str() );
    isBuilt = false;
    
  }
  return isBuilt;
}
double hmmPostProBase::computeLinkageFluxDensityForTimeStep(std::string tagGoD, std::string tagSpace, std::string tagForm, std::string tagResoType, 
                                                            std::string tagQuad, int tagLinearSourceTerm) const {

  std::cout <<  "Building the the Linkage Flux Density view..."<< std::endl;
  std::vector<Dof> D; double val; std::vector<double> vec(1); std::map<int, std::vector<double> > data;
  std::map<std::string, hmmScalarLagrangeFunctionSpace *>::iterator itSpace = _allFunctionSpaces->find(tagSpace);
  std::map<std::string, hmmFormulation *>::iterator itForm = _allFormulations->find(tagForm);
  std::map<std::string, hmmResolutionBase *>::iterator itReso = _allResolutions->find(tagResoType);
  std::map<std::string, GaussQuadrature *>::iterator itQuad = _allQuadratureRules->find(tagQuad);
  fullVector<double> _sourceCurrentVector;
  double fluxLinkage = 0.0;
  if( (itSpace != _allFunctionSpaces->end() ) && (itSpace != _allFunctionSpaces->end() ) 
      && (itForm != _allFormulations->end() ) && ( itReso != _allResolutions->end() ) && (itQuad != _allQuadratureRules->end() ) ) {
    // get the time resolution parameters (frequency, ...)
    //====================================================
    hmmResolutionTimeDomain *thisReso = static_cast<hmmResolutionTimeDomain *>(itReso->second);
    double _freq = thisReso->getFrequency();
    double _currentTime = thisReso->getCurrentTime();
    int _currentTimeStep = thisReso->getCurrentTimeStep();
    Msg::Info("Frequency while computing the flux linkage density is %f and the current time is %f", _freq, _currentTime);
    double scalingFactor = sin (2 * pi * _freq * _currentTime);

    // getThe source term "fullVector"
    //================================
    inline const std::vector<hmmLinearTermBase *> *getVectorOfLinearTerms();
    const std::vector<hmmLinearTermBase *> *listOfSourceTerms = itForm->second->getVectorOfLinearTerms();
    std::vector<hmmLinearTermBase *>::const_iterator itLinearTerms;
    for( itLinearTerms = listOfSourceTerms->begin(); itLinearTerms != listOfSourceTerms->end(); itLinearTerms++) {
      hmmLinearTermSource *thisLinearSourceTerm = static_cast<hmmLinearTermSource *>(*itLinearTerms);
      if( thisLinearSourceTerm->getTag() == tagLinearSourceTerm) {
        _sourceCurrentVector = (*thisLinearSourceTerm->getSource() );
        //_sourceCurrentVector.scale(scalingFactor);
      }
    }
    Msg::Info("_sourceCurrentVector.(0) = %f, _sourceCurrentVector.(1) = %f, _sourceCurrentVector.(2) = %f ", _sourceCurrentVector(0), _sourceCurrentVector(1), _sourceCurrentVector(2) );
    double val_GP_perElement = 0.0;
    std::set<hmmDomainBase *>::const_iterator itDom;

    hmmGroupOfDomains * thisGoD;
    //     for (itSpace->second->getGroupOfDomains()->getGroupOfDomains()->begin(); 
    //          itSpace->second->getGroupOfDomains()->getGroupOfDomains()->end(); ++itDom) {
    //if(itSpace->second->getGroupOfDomains()->getTagGroupOfDomains() == tagGoD) {   
    
    for (itDom = itSpace->second->getGroupOfDomains()->getGroupOfDomains()->begin(); 
         itDom != itSpace->second->getGroupOfDomains()->getGroupOfDomains()->end(); ++itDom) {
      if( (*itDom)->getPhysical() == 13) { 
        for (groupOfElements::elementContainer::const_iterator it = ( (*itDom)->getGroupOfElements()->begin() ); 
             it != ( (*itDom)->getGroupOfElements()->end() ); ++it) {
          MElement *e = *it;
          IntPt *GP; 
          int npts = itQuad->second->getIntPoints(e, &GP);
          std::vector<TensorialTraits<double>::ValType> _func;
          double jac[3][3];
          for (int i = 0; i < npts; i++) {
            const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
            const double weight = GP[i].weight; const double detJ = e->getJacobian(u, v, w, jac);
            itSpace->second->f(e, u, v, w, _func);
            for(int j = 0; j < e->getNumShapeFunctions(); j++) {
              MVertex *vert = e->getShapeFunctionNode(j);
              _pAssembler->getDofValue(vert->getNum(), itSpace->second->getId(), val);
              val_GP_perElement += detJ * weight * val * _func[j]; 
              Msg::Warning("contribution from node = %d to fluxlinkage for GP = %d of element %d = %g", j, i, e->getNum(), val_GP_perElement);
            }
            fluxLinkage += val_GP_perElement * _sourceCurrentVector(2); 
            Msg::Warning("fluxlinkage for GP = %d of element %d = %g", i, e->getNum(), fluxLinkage);
          }
        }
      }
      else {
        Msg::Warning("Done in hmmPostProcessing. The hmmDomaineBase Ind1 has not been found. \n");
      }
    }

     //}
    //else {
    //  Msg::Warning("Done in hmmPostProcessing. Could not compute linkage flux density coz the GoD %s is not included in hmmFunctionSpace %s \n", tagGoD.c_str(), itSpace->first.c_str() );
    //}
    //}
    
    // writing the answer in a ".txt"
    // ==============================
    FILE *fp_fluxLinkage;
    if(_currentTimeStep == 0) {
      fp_fluxLinkage = fopen("../output/macroproblem/linkageFluxDensity.dat", "w") ;
      fprintf(fp_fluxLinkage, "%d %g %g \n" , _currentTimeStep, _currentTime, fluxLinkage );
      fclose(fp_fluxLinkage) ;
    }
    if(_currentTimeStep != 0) {
      fp_fluxLinkage = fopen("../output/macroproblem/linkageFluxDensity.dat", "a") ;
      fprintf(fp_fluxLinkage, "%d %g %g \n" , _currentTimeStep, _currentTime, fluxLinkage );
      fclose(fp_fluxLinkage) ;
    }
  }
  else {
    Msg::Error("Elements missing while computing linkage flux density...");
    //Distinguish different cases...
  }
  return fluxLinkage;
}

void  hmmPostProBase::initMapOfPoints() {
  FILE *fp; fp = fopen("../meso/localMeshes", "w");
  fprintf(fp, "//Here is the list of points for local computations. \n");
  fprintf(fp, "//================================================== \n");
  fprintf(fp, "// x y z \n");
  fclose(fp);
}
void hmmPostProBase::addPoint2MapOfPoints(std::string tagML, int intPt, double x, double y, double z) {
  // Function used for adding a point for local field computations to the map of points
  // The key of type int will be used for writing local fields in GetDP   
  SPoint3 *thisPoint = new SPoint3(x, y, z);
  _allLocalFieldPoints.insert(std::make_pair(intPt, thisPoint) );
  
  // Adding the points to the map of points
  // ======================================
  FILE *fp; fp = fopen("../meso/localMeshes/LocalMeshes.dat", "a");
  fprintf(fp, "%f %f %f \n", x, y, z);
  fclose(fp);
  
  // Mesh local geometries
  // =====================
  // write the data of the GP in microMeshes
  std::map<std::string, hmmMaterialLawBase *>::iterator itML = _allMaterialLaws->find(tagML);
  if(itML == _allMaterialLaws->end() ) {
    Msg::Error("The material law does not exist. ");
  }
  else {
    hmmMicroSolverGetDPNonLinearDouble *currentNLML = (hmmMicroSolverGetDPNonLinearDouble *)(itML->second);

    Msg::Info("x_gauss = %.16lf ; y_gauss = %.16lf ; \n", thisPoint->x(), thisPoint->y() );
    Msg::Info("lx = %.16lf ; ly = %.16lf ; \n", currentNLML->getLengthX(), currentNLML->getLengthY());

    FILE *fp_meshes;
    fp_meshes = fopen("../meso/mesoMeshes/gaussPointPosition.dat", "w") ;
    fprintf(fp_meshes, "x_gauss = %.16lf ; y_gauss = %.16lf ; \n", thisPoint->x(), thisPoint->y() );
    fprintf(fp_meshes, "lx = %.16lf ; ly = %.16lf ; \n", currentNLML->getLengthX(), currentNLML->getLengthY());
    fclose(fp_meshes) ; 
    std::string _thisGMSHMSHFileName = "micro_smc";
    std::stringstream out; out << intPt;
    _thisGMSHMSHFileName += "_" + out.str() + ".msh"; 
    _thisGMSHMSHFileName = "../meso/localMeshes/"+ _thisGMSHMSHFileName;

    // Mesh the geometry
    //==================
    SystemCall("rm -f ~/.gmshrc", true);
    SystemCall("gmsh -verbose 2 "  + currentNLML->getGMSHGeoFileName() + " -2 -o " + _thisGMSHMSHFileName, true);
  }
}
void hmmPostProBase::emptyMapOfPoints() {

  //function used for emptying the map of points
  std::map<int, SPoint3 *>::iterator itPoint;
  for(itPoint = _allLocalFieldPoints.begin(); itPoint != _allLocalFieldPoints.end(); ++itPoint) {
    delete itPoint->second;
  }
  _allLocalFieldPoints.clear();
}
bool hmmPostProBase::computeLocalFields( int keyForThisPoint, SPoint3 &thisPoint, std::string tagML, int thisThreadNum, std::string tagRes) const  {
  bool isComputed = true;
  std::map<std::string, hmmMaterialLawBase *>::iterator itML = _allMaterialLaws->find(tagML);
  // Check if ML is defined ????????????????????????
  std::map<std::string, hmmResolutionBase *>::iterator itRes = _allResolutions->find(tagRes);
  // Check if Reso is defined ????????????????????????

  // Get the current time step. If Resolution type is static, set to 1 
  int currentTimeStep;
  double currentTime;
  if (itRes->second->getResType() == timeDomSolver) {

    hmmResolutionTimeDomain *thisReso = (hmmResolutionTimeDomain *) (itRes->second);
    currentTimeStep = thisReso->getCurrentTimeStep();
    currentTime     = thisReso->getCurrentTime();
  }
  else {
    currentTimeStep = 1;
  }
  if (itML->second->getType() == HMM_NONLINEAR_MLAW) {
    hmmMicroSolverGetDPNonLinearDouble *microMaterialLaw = ( (hmmMicroSolverGetDPNonLinearDouble *) (itML->second) );
    MElement *ele = _pModel->getMeshElementByCoord(thisPoint, 2, true);
    if (ele != NULL) {
      Msg::Info("The element exists \n");
    }
    else {
      Msg::Error("The element does not exist. \n");
      isComputed = false;
    }
    // store the GMSH and GetDO file names before using the exact geometry
    //====================================================================
    std::string storeGMSHGeoFileName  = microMaterialLaw->getGMSHGeoFileName() ;
    std::string storeGetDPProFileName = microMaterialLaw->getGetDPProFileName();

    microMaterialLaw->setGMSHGeoFileName(microMaterialLaw->getGMSHExactGeoFileName() );
    microMaterialLaw->setGetDPProFileName(microMaterialLaw->getGetDPExactProFileName() );
    HomQtyLinear *MQB = new HomQtyLinear();

    Msg::Info("========================================================== \n");
    Msg::Info("The current time step in hmmPostProcessing is equal to %d. \n", currentTimeStep);
    Msg::Info("========================================================== \n");

    microMaterialLaw->computeLocalFields(*MQB, keyForThisPoint, thisPoint, ele, thisThreadNum, currentTime, currentTimeStep);
    delete MQB;
    microMaterialLaw->setGMSHGeoFileName(storeGMSHGeoFileName);
    microMaterialLaw->setGetDPProFileName(storeGetDPProFileName);
  }
  else {
    Msg::Info("Done in hmmPostProcessing. You are computing locla fields for a ML of non-type 'multiscale nonlinear'. ");
    isComputed = false;
  }
  return isComputed;
}
bool hmmPostProBase::computeLocalFields(std::string tagML, std::string tagRes) const {
  bool areComputed = true;
  std::map<int, SPoint3 *>::const_iterator itStrSpt;

  // Display the content of the map
  for (itStrSpt = this->getListOfPtsForLocalFields()->begin(); itStrSpt != this->getListOfPtsForLocalFields()->end(); ++itStrSpt) {
    Msg::Info("========================================================== \n");
    Msg::Info( " The point tagged %d has coordinates x = %g, y = %g, z = %g; \n", (itStrSpt->first), (*(itStrSpt->second)).x(), (*(itStrSpt->second)).y(), (*(itStrSpt->second)).z() );
    Msg::Info("========================================================== \n");
  }

  //for (itStrSpt = this->getListOfPtsForLocalFields()->begin(); itStrSpt != this->getListOfPtsForLocalFields()->end(); ++itStrSpt) {
  //  areComputed = (areComputed && this->computeLocalFields( (itStrSpt->first), *(itStrSpt->second), tagML, tagRes) );
  //}
  //return areComputed;


  std::vector< std::pair<int, SPoint3 *> > vecIntLocalFields;
  for (itStrSpt = this->getListOfPtsForLocalFields()->begin(); itStrSpt != this->getListOfPtsForLocalFields()->end(); ++itStrSpt) {
    vecIntLocalFields.push_back(std::make_pair(itStrSpt->first, itStrSpt->second) );
  } 

  
#pragma omp parallel
  {
    int thisThreadNum = omp_get_thread_num();
    Msg::Warning("The number of threads is %d \n", thisThreadNum);
    // // Copying the .pro file for a given thread                                                                                                                                      
    // //=========================================                                                                                                                                      
    //std::string s; std::stringstream out; out << thisThreadNum; s = out.str();                                                                                                        
    //std::string copyDotProFile = "cp" + " " + this->GetDPProfileName + " " + this->GetDPProfileName + "_" + s ;                                                                       
    //SystemCall(copyDotProFile, true);                                                                                                                                                 
    
#pragma omp for
    for (int iii = 0; iii < vecIntLocalFields.size(); ++iii) {
      areComputed = (areComputed && this->computeLocalFields( (vecIntLocalFields[iii].first), *(vecIntLocalFields[iii].second), tagML, thisThreadNum, tagRes) );
      //areComputed = (areComputed && this->computeLocalFields( (itStrSpt->first), *(itStrSpt->second), tagML, thisThreadNum, tagRes) );
    }
  }
  return areComputed;
  
  
}
