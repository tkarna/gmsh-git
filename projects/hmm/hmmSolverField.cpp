#include "hmmSolverField.h"

void hmmSolverField::dtF(MElement *ele, double u, double v, double w, TensorialTraits<double>::ValType &val) const {
  std::vector<Dof> D;
  std::vector<ValType> SFVals;
  std::vector<double> DMVals;
  std::vector<double> prevDMVals;
  _fs->getKeys(ele, D);
  _dm->getDofValue(D, DMVals);
  _dm->getDofValue(D, 1, prevDMVals);
  _fs->f(ele, u, v, w, SFVals);
  val = ValType();
  for (unsigned int i = 0; i < D.size(); ++i) {
    val += SFVals[i] * (DMVals[i] - prevDMVals[i]);
  }
}
void hmmSolverField::dtGradf(MElement *ele, double u, double v, double w, TensorialTraits<double>::GradType &grad) const {
  std::vector<Dof> D;
  std::vector<GradType> SFGrads;
  std::vector<double> DMVals;
  std::vector<double> prevDMVals;
  _fs->getKeys(ele, D);
  _dm->getDofValue(D, 1, prevDMVals);
  _dm->getDofValue(D, DMVals);
  _fs->gradf(ele, u, v, w, SFGrads);
  grad = GradType();
  for (unsigned int i = 0; i < D.size(); ++i)
    grad += SFGrads[i] * ( DMVals[i]- prevDMVals[i]);
}
void hmmSolverField::f(MElement *ele, double u, double v, double w, int stepsBack, TensorialTraits<double>::ValType &val) const {
  std::vector<Dof> D;
  std::vector<ValType> SFVals;
  std::vector<double> prevDMVals;
  _fs->getKeys(ele, D);
  _dm->getDofValue(D, stepsBack, prevDMVals);
  _fs->f(ele, u, v, w, SFVals);
  val = ValType();
  for (unsigned int i = 0; i < D.size(); ++i)
    val += SFVals[i] * prevDMVals[i];
}
void hmmSolverField::gradf(MElement *ele, double u, double v, double w, int stepsBack, TensorialTraits<double>::GradType &grad) const {
  std::vector<Dof> D;
  std::vector<GradType> SFGrads;
  std::vector<double> prevDMVals;
  _fs->getKeys(ele, D);
  _dm->getDofValue(D, stepsBack, prevDMVals);
  _fs->gradf(ele, u, v, w, SFGrads);
  grad = GradType();
  for (unsigned int i = 0; i < D.size(); ++i)
    grad += SFGrads[i] * prevDMVals[i];
}

