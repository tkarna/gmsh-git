//
// C++ Interface: NonLinearDofManager
//
// Description: dofManager for non linear system (rewritte of assemble(fullMatrix) no contribution for fixed Dof
//              The contribution is taken into account in the assemble(fullvector) of RightHandSide
// More add functions to archiving force can be placed in dofManager but I put it here to not pollute gmsh code
//
// "assembleNR" functions used for NR's iterations to avoid contribution from Dirichlet BC for DX in "J_n * (DX)_n = -r_n"
// "assemble" functions are classical functions of dofManager.
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef _NONLINEAR_DOFMANAGER_H_
#define _NONLINEAR_DOFMANAGER_H_

#include <complex>
#include <iterator>
#include <iostream>
#include "dofManager.h"
#include "MultiscaleNonLinearSystem.h"
#include "hmmFunctionSpace.h"

template<class scalar> class NonLinearDofManager : public dofManager<scalar> {
protected:
  std::map<Dof, std::vector<scalar> > _prevSol;
  std::map<double, int> _time2int;
  std::set<Dof> _allDofs;
public :
  typedef typename dofManager<scalar>::dataVec dataVec;
  typedef typename dofManager<scalar>::dataMat dataMat;
  NonLinearDofManager(linearSystem< dataMat > *l) : dofManager<scalar>(l){}
  NonLinearDofManager(linearSystem< dataMat > *l1, linearSystem<dataMat> *l2) : dofManager<scalar>(l1,l2){}
  virtual ~NonLinearDofManager() {}
  const std::map<double, int> *getTime2Int() const {
    return &(_time2int);
  }
  const std::set<Dof> *getSetOfDofs() const {
    return &(_allDofs);
  }
  const std::map<Dof, std::vector<scalar> > *getPrevSol() const {
    return &(_prevSol);
  }
  typename std::map<Dof, std::vector<scalar> >::const_iterator begin() const {
    typename std::map<Dof, std::vector<scalar> >::const_iterator _itPrevSol = _prevSol.begin();
    return _itPrevSol;
  }
  typename std::map<Dof, std::vector<scalar> >::const_iterator end() const {
    typename std::map<Dof, std::vector<scalar> >::const_iterator _itPrevSol = _prevSol.end();
    return _itPrevSol;
  }
  void getDofValue(Dof d, double time, scalar &value) const {
    typename std::map<Dof, std::vector<scalar> >::const_iterator _itPrevSol = _prevSol.find(d);
    std::map<double, int>::const_iterator itTime2Int = _time2int.find(time);
    value = (_itPrevSol->second[itTime2Int->second]);
  }
  void getDofValue(Dof d, int stepsBack, scalar &value) const {
    typename std::map<Dof, std::vector<scalar> >::const_iterator itPrevSol = _prevSol.find(d);
    typename std::vector<scalar>::const_iterator itPrevVal = itPrevSol->second.end();
    itPrevVal -= (stepsBack); 
    //std::map<double, int>::const_iterator itTime2Int = _time2int.find(time);
    value = (*itPrevVal);
    //std::cout << "In the dofManager, the previous value is " << value << std::endl;
  }
  inline void getDofValue(int ent, int type, int stepsBack, scalar &val) const {
    Dof *D = new Dof(ent, type);
    getDofValue(*D, stepsBack, val);
  }
  void getDofValue(std::vector<Dof> &keys, int stepsBack, std::vector<scalar> &Vals) const {
    //typename std::map<Dof, std::vector<scalar> >::const_iterator itPrevSol = _prevSol.find(d);
    //typename std::vector<scalar>::const_iterator itPrevVal = itPrevSol->second.end();
    //itPrevVal -= (stepsBack); 
    //value = (*itPrevVal);

    int ndofs = keys.size();
    size_t originalSize = Vals.size();
    Vals.resize(originalSize + ndofs);
    for (int i = 0; i < ndofs; ++i) {
      getDofValue(keys[i], stepsBack, Vals[originalSize+i]);
    }
  }
  //void init(std::map<int, hmmScalarLagrangeFunctionSpace *> *_allFunctionSpaces, MultiscaleNonLinearSystem<double> &system, double initTime) {
  void init(std::map<std::string, hmmScalarLagrangeFunctionSpace *> *_allFunctionSpaces, double initTime) {
    std::map<std::string, hmmScalarLagrangeFunctionSpace *>::iterator itSpace;
    std::vector<Dof> keys;
    for (itSpace = _allFunctionSpaces->begin(); itSpace != _allFunctionSpaces->end(); ++itSpace) {
      std::set<hmmDomainBase *>::const_iterator itDom;
      for (itDom = itSpace->second->getGroupOfDomains()->getGroupOfDomains()->begin(); itDom != itSpace->second->getGroupOfDomains()->getGroupOfDomains()->end(); ++itDom) {
        std::set<MElement*>::const_iterator itElement;
        for (itElement = (*itDom)->getGroupOfElements()->begin(); itElement != (*itDom)->getGroupOfElements()->end(); ++itElement) {
          itSpace->second->getKeys( *(itElement), keys);
        }
      }
    }
    std::vector<Dof>::const_iterator itKeys;
    std::set<Dof>::const_iterator itSetOfKeys;
    for (itKeys = keys.begin(); itKeys != keys.end(); ++itKeys) {
      _allDofs.insert( (*itKeys) );
    }
    for (itSetOfKeys = _allDofs.begin(); itSetOfKeys != _allDofs.end(); ++itSetOfKeys) {
      std::vector<scalar> currentDof;
      MultiscaleNonLinearSystemGmm<scalar> *system = (MultiscaleNonLinearSystemGmm<scalar> *)(this->_current);
      system->setInitialValues(this->sizeOfR(), 0.0);
      scalar val = 0.0;
      if (this->isFixed(*itSetOfKeys) ) {
        this->getDofValue( (*itSetOfKeys), val);///////////////////////////////////////////////////////////////////////////////
      }
      currentDof.push_back(val);
      _prevSol[*itSetOfKeys] = currentDof;

    }
    _time2int[initTime] = 0;
  }
  void addSolution(double currentTime) {
    typename std::map<Dof, std::vector<scalar> >::iterator itPrevSol;
    for (itPrevSol = _prevSol.begin(); itPrevSol != _prevSol.end(); ++itPrevSol) {
      Dof currentDof = itPrevSol->first;
      scalar val;
      this->dofManager<scalar>::getDofValue(currentDof, val);
      itPrevSol->second.push_back(val);
    }
    std::map<double, int>::iterator itLast = _time2int.end(); --itLast;
    _time2int[currentTime] = (itLast->second)+1;
  }
  inline void assemble(std::vector<Dof> &R, std::vector<Dof> &C, const fullMatrix<scalar> &m) {
    this->dofManager<scalar>::assemble(R, C, m);
  }

  virtual inline void assemble(std::vector<Dof> &R, const fullMatrix<scalar> &m) {
    this->dofManager<scalar>::assemble(R, m);
  }
  inline void assemble(const Dof &R, const dataMat &value) {
    this->dofManager<scalar>::assemble(R, value);
  }
  inline void assembleNR(std::vector<Dof> &R, std::vector<Dof> &C, const fullMatrix<dataMat> &m) {
    if (!this->dofManager<scalar>::_current->isAllocated()) 
      this->dofManager<scalar>::_current->allocate(this->dofManager<scalar>::unknown.size());
    std::vector<int> NR(R.size()), NC(C.size());
    for (unsigned int i = 0; i < R.size(); i++) {
      std::map<Dof, int>::iterator itR = this->dofManager<scalar>::unknown.find(R[i]);
      if (itR != this->dofManager<scalar>::unknown.end()) 
        NR[i] = itR->second;
      else 
        NR[i] = -1;
    }
    for (unsigned int i = 0; i < C.size(); i++) {
      std::map<Dof, int>::iterator itC = this->dofManager<scalar>::unknown.find(C[i]);
      if (itC != this->dofManager<scalar>::unknown.end()) 
        NC[i] = itC->second;
      else 
        NC[i] = -1;
    }
    for (unsigned int i = 0; i < R.size(); i++) {
      if (NR[i] != -1) {
        for (unsigned int j = 0; j < C.size(); j++) {
          if (NC[j] != -1)
            this->dofManager<scalar>::_current->addToMatrix(NR[i], NC[j], m(i, j));
          else {
            typename std::map<Dof, dataVec>::iterator itFixed = this->dofManager<scalar>::fixed.find(C[j]);
            if (itFixed != this->dofManager<scalar>::fixed.end())
              this->assembleLinConst(R[i], C[j], m(i, j)); 
          }
        }
      }
      else{
        for (unsigned int j = 0; j < C.size(); j++)
          this->assembleLinConst(R[i], C[j], m(i, j));
      }
    }
  }
  virtual inline void assembleNR(std::vector<Dof> &R, const fullMatrix<dataMat> &m) {
    if (!this->dofManager<scalar>::_current->isAllocated()) 
      this->dofManager<scalar>::_current->allocate(this->dofManager<scalar>::unknown.size());
    std::vector<int> NR(R.size());
    for (unsigned int i = 0; i < R.size(); i++) {
      std::map<Dof, int>::iterator itR = this->dofManager<scalar>::unknown.find(R[i]);
      if (itR != this->dofManager<scalar>::unknown.end()) 
        NR[i] = itR->second;
      else 
        NR[i] = -1;
    }
    for (unsigned int i = 0; i < R.size(); i++) {
      if (NR[i] != -1) {
        for (unsigned int j = 0; j < R.size(); j++) {
          if (NR[j] != -1)
            this->dofManager<scalar>::_current->addToMatrix(NR[i], NR[j], m(i, j));
          else{
            typename std::map<Dof, dataVec>::iterator itFixed = this->dofManager<scalar>::fixed.find(R[j]);
            if (itFixed != this->dofManager<scalar>::fixed.end())
              this->assembleLinConst(R[i], R[j], m(i, j));
          }
        }
      }
      else {
        for (unsigned int j = 0; j < R.size(); j++)
          this->assembleLinConst(R[i], R[j], m(i, j));
      }
    }
  }
  inline void assembleNR(const Dof &R, const dataMat &value) {
    if(!this->dofManager<scalar>::_current->isAllocated()) 
      this->dofManager<scalar>::_current->allocate(this->dofManager<scalar>::unknown.size());
    std::map<Dof, int>::iterator itR = this->dofManager<scalar>::unknown.find(R);
    if(itR != this->dofManager<scalar>::unknown.end())
      this->dofManager<scalar>::_current->addToRightHandSide(itR->second, value);
    else {
      typename std::map<Dof, DofAffineConstraint<dataVec> >::iterator itConstraint;
      itConstraint = this->dofManager<scalar>::constraints.find(R);
      if (itConstraint != this->dofManager<scalar>::constraints.end()) {
        for (unsigned j = 0; j < (itConstraint->second).linear.size(); j++) {
          dataMat tmp;
          dofTraits<scalar>::gemm(tmp, (itConstraint->second).linear[j].second, value, 1, 0);
          this->dofManager<scalar>::assemble((itConstraint->second).linear[j].first, tmp);
        }
      }
    }
  }
  virtual inline void assembleIntForces(std::vector<Dof> &R, const fullMatrix<dataMat> &m) {
    if (!this->dofManager<scalar>::_current->isAllocated()) 
      this->dofManager<scalar>::_current->allocate(this->dofManager<scalar>::sizeOfR());
    std::vector<int> NR(R.size());
    for (unsigned int i = 0; i < R.size(); i++) {
      std::map<Dof, int>::iterator itR = this->dofManager<scalar>::unknown.find(R[i]);
      if (itR != this->dofManager<scalar>::unknown.end()) 
        NR[i] = itR->second;
      else 
        NR[i] = -1;
    }
    for(unsigned int i = 0; i < R.size(); i++) {
      if(NR[i] != -1) {
        for(unsigned int j = 0; j < R.size(); j++) {
          if(NR[j] != -1) {
            scalar dofValue;
            this->dofManager<scalar>::getDofValue(R[j], dofValue);
            dataVec tmp = m(i, j) * dofValue;
            this->dofManager<scalar>::_current->addToRightHandSide(NR[i], tmp);
          }
          else {
            typename std::map<Dof,  dataVec>::iterator itFixed = this->dofManager<scalar>::fixed.find(R[j]);
            if(itFixed != this->dofManager<scalar>::fixed.end()) {
              dataVec tmp = m(i, j) * itFixed->second;//(itFixed->second);
              this->dofManager<scalar>::_current->addToRightHandSide(NR[i], tmp);
            }
          }
        }
      }
    }
  }
//   virtual inline void assembleIntForcesPrevTime(std::vector<Dof> &R, const fullMatrix<dataMat> &m) {
//     if (!this->dofManager<scalar>::_current->isAllocated()) 
//       this->dofManager<scalar>::_current->allocate(this->dofManager<scalar>::sizeOfR());
//     std::vector<int> NR(R.size());
//     for (unsigned int i = 0; i < R.size(); i++) {
//       std::map<Dof, int>::iterator itR = this->dofManager<scalar>::unknown.find(R[i]);
//       if (itR != this->dofManager<scalar>::unknown.end()) 
//         NR[i] = itR->second;
//       else 
//         NR[i] = -1;
//     }
//     for(unsigned int i = 0; i < R.size(); i++) {
//       if(NR[i] != -1) {
//         for(unsigned int j = 0; j < R.size(); j++) {
//           if(NR[j] != -1) {
//             scalar dofValue, prevDofValue;
//             this->getDofValue(R[j], dofValue);
//             this->getDofValue(R[j], 1, prevDofValue);              
//             if ( (R[j].getEntity() == 14) || (R[j].getEntity() == 22)) {
//               //std::cout << " In assembleIntForcesPrevTime, previous and current values for the node " << R[j].getEntity() << " are " << prevDofValue << " and " << dofValue << std::endl;
//             }
//             dataVec tmp = -m(i, j) * dofValue;
//             this->dofManager<scalar>::_current->addToRightHandSide(NR[i], tmp);
//           }
//           else {
//             typename std::map<Dof,  dataVec>::iterator itFixed = this->dofManager<scalar>::fixed.find(R[j]);
//             if(itFixed != this->dofManager<scalar>::fixed.end()) {
//               dataVec tmp = -m(i, j) * itFixed->second;//(itFixed->second);
//               this->dofManager<scalar>::_current->addToRightHandSide(NR[i], tmp);
//             }
//           }
//         }
//       }
//     }
//   }
  //==============================================================================================

  virtual inline void assembleIntForcesTime(std::vector<Dof> &R, const fullMatrix<dataMat> &m) { //FIXME
    if (!this->dofManager<scalar>::_current->isAllocated()) 
      this->dofManager<scalar>::_current->allocate(this->dofManager<scalar>::sizeOfR());
    std::vector<int> NR(R.size());
    for (unsigned int i = 0; i < R.size(); i++) {
      std::map<Dof, int>::iterator itR = this->dofManager<scalar>::unknown.find(R[i]);
      if (itR != this->dofManager<scalar>::unknown.end()) 
        NR[i] = itR->second;
      else 
        NR[i] = -1;
    }
    for(unsigned int i = 0; i < R.size(); i++) {
      if(NR[i] != -1) {
        for(unsigned int j = 0; j < R.size(); j++) {
          if(NR[j] != -1) {
            scalar dofValue; scalar prevDofValue;
            std::map<double, int>::const_iterator itD2I = _time2int.end();
            itD2I--; double lastTime = itD2I->first;
            this->dofManager<scalar>::getDofValue(R[j], dofValue);
            
            double dofValueReal = (double)(dofValue);
            //Msg::Error("print dofValue %f", dofValueReal);
            
            this->getDofValue(R[j], lastTime, prevDofValue);
            dataVec tmp = m(i, j) * (dofValue-prevDofValue);
            this->dofManager<scalar>::_current->addToRightHandSide(NR[i], tmp);
          }
          else {
            typename std::map<Dof,  dataVec>::iterator itFixed = this->dofManager<scalar>::fixed.find(R[j]);
            if(itFixed != this->dofManager<scalar>::fixed.end()) {
              scalar dofValue; scalar prevDofValue;
              std::map<double, int>::const_iterator itD2I = _time2int.end();
              itD2I--; double lastTime = itD2I->first;
              this->dofManager<scalar>::getDofValue(R[j], dofValue);
              this->getDofValue(R[j], lastTime, prevDofValue);
              dataVec tmp = m(i, j) * (itFixed->second - prevDofValue);
              this->dofManager<scalar>::_current->addToRightHandSide(NR[i], tmp);
            }
          }
        }
      }
    }
  }


  virtual inline void assembleIntForcesTime_H_Form(std::vector<Dof> &R, const fullMatrix<dataMat> &m) { //FIXME
    if (!this->dofManager<scalar>::_current->isAllocated()) 
      this->dofManager<scalar>::_current->allocate(this->dofManager<scalar>::sizeOfR());
    std::vector<int> NR(R.size());
    for (unsigned int i = 0; i < R.size(); i++) {
      std::map<Dof, int>::iterator itR = this->dofManager<scalar>::unknown.find(R[i]);
      if (itR != this->dofManager<scalar>::unknown.end()) 
        NR[i] = itR->second;
      else 
        NR[i] = -1;
    }
    for(unsigned int i = 0; i < R.size(); i++) {
      if(NR[i] != -1) {
        for(unsigned int j = 0; j < R.size(); j++) {
          if(NR[j] != -1) {
            scalar dofValue; scalar prevDofValue;
            std::map<double, int>::const_iterator itD2I = _time2int.end();
            itD2I--; double lastTime = itD2I->first;
            this->dofManager<scalar>::getDofValue(R[j], dofValue);
            
            double dofValueReal = (double)(dofValue);
            //Msg::Error("print dofValue %f", dofValueReal);
            
            this->getDofValue(R[j], lastTime, prevDofValue);
            dataVec tmp = m(i, j) * (dofValue-prevDofValue);
            this->dofManager<scalar>::_current->addToRightHandSide(NR[i], tmp);
          }
          else {
//             typename std::map<Dof,  dataVec>::iterator itFixed = this->dofManager<scalar>::fixed.find(R[j]);
//             if(itFixed != this->dofManager<scalar>::fixed.end()) {
//               scalar dofValue; scalar prevDofValue;
//               std::map<double, int>::const_iterator itD2I = _time2int.end();
//               itD2I--; double lastTime = itD2I->first;
//               this->dofManager<scalar>::getDofValue(R[j], dofValue);
//               this->getDofValue(R[j], lastTime, prevDofValue);
//               dataVec tmp = m(i, j) * (itFixed->second - prevDofValue);
//               this->dofManager<scalar>::_current->addToRightHandSide(NR[i], tmp);
//             }
          }
        }
      }
    }
  }
  
  //===================================================================================================
  virtual inline void assembleIntForces(std::vector<Dof> &R, const fullVector<dataVec> &b) {
    if (!this->dofManager<scalar>::_current->isAllocated()) 
      this->dofManager<scalar>::_current->allocate(this->dofManager<scalar>::sizeOfR());
    std::vector<int> NR(R.size());
    for (unsigned int i = 0; i < R.size(); i++) {
      std::map<Dof, int>::iterator itR = this->dofManager<scalar>::unknown.find(R[i]);
      if (itR != this->dofManager<scalar>::unknown.end()) 
        NR[i] = itR->second;
      else 
        NR[i] = -1;
    }
    
    for (unsigned int i = 0; i < R.size(); i++){
      if (NR[i] != -1)
        this->dofManager<scalar>::_current->addToRightHandSide(NR[i], b(i));
    }
  }
  virtual inline void getDofValue(std::vector<Dof> &keys, std::vector<dataVec> &Vals) {
    this->dofManager<scalar>::getDofValue(keys, Vals);
  }
  virtual inline void getDofValue(Dof key, dataVec &val) {
    this->dofManager<scalar>::getDofValue(key, val);
  }
  inline void getDofValue(int ent, int type, dataVec &v) const {
    this->dofManager<scalar>::getDofValue(ent, type, v);
  }
  inline void getDofValue(MVertex *v, int iComp, int iField, dataVec &value) const {
    this->dofManager<scalar>::getDofValue(v->getNum(), Dof::createTypeWithTwoInts(iComp, iField), value);
  }
  void systemSolve() {
    this->dofManager<scalar>::systemSolve(); 
  }
  void systemClear() {
    this->dofManager<scalar>::systemClear();
  }
  int sizeOfR() {
    return this->dofManager<scalar>::sizeOfR();
  }
  int sizeOfF() {
    return this->dofManager<scalar>::sizeOfF();
  }
};

#endif // NONLINEAR_DOFMANAGERSYSTEM_H_

