#include <sstream>
#include "hmmProblem.h"

hmmProblemBase::hmmProblemBase() {
  _model = NULL; 
  _linsys = NULL; 
  _assembler = NULL;
}
hmmProblemBase::~hmmProblemBase() {
  if (_assembler) delete _assembler;
  if (_linsys)    delete _linsys;
  std::map<std::string, hmmGroupOfDomains *>::iterator itDom; 
  for (itDom = allDomains.begin(); itDom != allDomains.end(); itDom++) 
  if ( (*itDom).second ) delete (*itDom).second;
  
  std::map<std::string, hmmMaterialLawBase *>::iterator itMaterialLaw; 
  for (itMaterialLaw = allMaterialLaws.begin(); itMaterialLaw != allMaterialLaws.end(); itMaterialLaw++) 
  if ( (*itMaterialLaw).second ) delete (*itMaterialLaw).second;
  
  std::map<std::string, hmmDirichlet *>::iterator itDiri; 
  for (itDiri = allDirichletBC.begin(); itDiri != allDirichletBC.end(); itDiri++) 
  if ( (*itDiri).second ) delete (*itDiri).second;
  
  std::map<std::string, hmmScalarLagrangeFunctionSpace *>::iterator itSpace; 
  for (itSpace = allFunctionSpaces.begin(); itSpace != allFunctionSpaces.end(); itSpace++) 
  if ( (*itSpace).second ) delete (*itSpace).second;
}

void hmmProblemBase::setMesh(const std::string &meshFileName) {   
  _model = new GModel();
  _model->readMSH(meshFileName.c_str());
  _dim = _model->getNumRegions() ? 3 : 2;
  return; 
}  

// 2. hmmGroupOfDomains
//=====================
void hmmProblemBase::getDomainsInfo() const {
  Msg::Info("--\n");
  std::cout << "The list of hmmGroupOfDomains: ";
  std::map<std::string, hmmGroupOfDomains *>::const_iterator itDom;
  for (itDom = allDomains.begin(); itDom != allDomains.end(); ++itDom) {
    if(itDom == allDomains.begin() ) {
      std::cout << "[ " << itDom->first <<", ";
    }
    else if(itDom == (--allDomains.end() )) {
      std::cout << itDom->first << " ]" << std::endl;
    }
    else {
      std::cout << itDom->first << ", ";
    }
  }
  Msg::Info("--\n");
}
void hmmProblemBase::addGroupOfDomains(std::string tagGroupOfDomain) {
  hmmGroupOfDomains *newGroupOfDomains = new hmmGroupOfDomains(tagGroupOfDomain);
  allDomains.insert(std::make_pair(tagGroupOfDomain, newGroupOfDomains) );
}
void hmmProblemBase::addDomainToGroupOfDomains(std::string tagGroupOfDomain, int dim, int physical) {
  std::map<std::string, hmmGroupOfDomains* >::iterator it = allDomains.find(tagGroupOfDomain);
  if (it != allDomains.end() )
    it->second->addDomain(dim, physical);
  else Msg::Error("The selected domain does not exist. \n");
}

hmmGroupOfDomains *hmmProblemBase::getGroupOfDomains(std::string tagGroupOfDomain) {
  if (allDomains.empty()) return NULL;
  else {
    std::map<std::string, hmmGroupOfDomains *>::iterator it = allDomains.find(tagGroupOfDomain);
    if (it != allDomains.end() )
      return it->second;
    else {
      Msg::Error("The selected domain does not exist. \n");
      return NULL;
    }
  }
}

// 3. hmmFunction
//===============
void hmmProblemBase::getFunctionInfo() const {
  Msg::Info("--\n");
  Msg::Info("There is a total of %d material laws. \n", allMaterialLaws.size() );
  std::map<std::string, hmmMaterialLawBase *>::const_iterator itMaterialLaw;
  for (itMaterialLaw = allMaterialLaws.begin(); itMaterialLaw != allMaterialLaws.end(); ++itMaterialLaw) {
    std::cout << "The current material law is tagged " << itMaterialLaw->first << std::endl;
  }
  Msg::Info("--\n");
}
void hmmProblemBase::add1ScaleLinearMaterialLaw(std::string tagMaterialLaw, std::string tagGroupOfDomains, 
                                                double mat11, double mat12, double mat13, 
                                                double mat21, double mat22, double mat23, 
                                                double mat31, double mat32, double mat33) {
  std::map<std::string, hmmGroupOfDomains *>::iterator itDom = allDomains.find(tagGroupOfDomains);
  if (itDom != allDomains.end() ) {
    std::map<std::string, hmmMaterialLawBase *>::iterator itMatLaw = allMaterialLaws.find(tagMaterialLaw);
    if(itMatLaw == allMaterialLaws.end() ) {
      hmmLinearMaterialLaw *materialLaw = new hmmLinearMaterialLaw( tagMaterialLaw); 
      fullMatrix<double> *thisMatrix = new fullMatrix<double>(3, 3);
      thisMatrix->set(0, 0, mat11); thisMatrix->set(0, 1, mat12); thisMatrix->set(0, 2, mat13);
      thisMatrix->set(1, 0, mat21); thisMatrix->set(1, 1, mat22); thisMatrix->set(1, 2, mat23);
      thisMatrix->set(2, 0, mat31); thisMatrix->set(2, 1, mat32); thisMatrix->set(2, 2, mat33);
      HomQtyLinear *MQB = new HomQtyLinear(*thisMatrix);
      materialLaw->setMQB( *(itDom->second), *MQB); //FixMe
      allMaterialLaws.insert(std::make_pair(tagMaterialLaw, materialLaw) );
    }
    else {
      Msg::Error("The material law you are adding exists already");
    }
  }
  else Msg::Error("The selected domain does not exist. \n");
}

//================================================================================================================
// Add a material law. "tagMaterialLaw" is the tag for the new ML. First check if the Dom exists. If not, 
// raise an error message. Else, check if the the Material law exists. If not, create it, create the 
// MQB and add the GOD and the MQB to the ML. If the material law exists, check if it is already defined 
// on the GOD. If not, create a MQB and associate it with the GOD else, raise an error message because it
// is not possible to change the MQB of an existing ML here. This is done in the function "changeMQB()"

void hmmProblemBase::add1ScaleLinearMaterialLawInno(std::string tagMaterialLaw, std::string tagGroupOfDomains,
                                                double mat11, double mat12, double mat13, 
                                                double mat21, double mat22, double mat23, 
                                                double mat31, double mat32, double mat33) {
  std::map<std::string, hmmGroupOfDomains *>::iterator itDom = allDomains.find(tagGroupOfDomains);
  // Check if the hmmGoD exists
  if (itDom != allDomains.end() ) {
    std::map<std::string, hmmMaterialLawBase *>::iterator itMatLaw = allMaterialLaws.find(tagMaterialLaw);
    // Check if the ML exists
    hmmLinearMaterialLaw *materialLaw;
    if(itMatLaw == allMaterialLaws.end() ) { // The ML does not exist ==> create it
      Msg::Info("The material law you are adding does not exist.");
      Msg::Info("Start creating the new material law...");
      materialLaw = new hmmLinearMaterialLaw(tagMaterialLaw); //FixMe 
      allMaterialLaws.insert(std::make_pair(tagMaterialLaw, materialLaw) ); 
      Msg::Info("End creating the new material law...");
    }
    else { // The ML exists ==> check if the ML is defined on the hmmGoD
      std::map<hmmGroupOfDomains *, MLQtyBase *>::const_iterator itGoDMQB = itMatLaw->second->getMatLawByGroupOfDomains()->find(itDom->second);
      if(itGoDMQB == itMatLaw->second->getMatLawByGroupOfDomains()->end() ) { 
        Msg::Info("The material law already exists.");
        materialLaw = (hmmLinearMaterialLaw *) (itMatLaw->second);
      }
      else {
        Msg::Info("You can not change the MQB of an already defined ML in this method. You should instead use the method 'changeMQB'"); 
      }
    }
    // Create the MLQtyBase and associate it to the hmmGoD
    fullMatrix<double> * matLaw = new fullMatrix<double>(3, 3);
    matLaw->set(0, 0, mat11); matLaw->set(0, 1, mat12); matLaw->set(0, 2, mat13);
    matLaw->set(1, 0, mat21); matLaw->set(1, 1, mat22); matLaw->set(1, 2, mat23);
    matLaw->set(2, 0, mat31); matLaw->set(2, 1, mat32); matLaw->set(2, 2, mat33);
    HomQtyLinear *HLQ = new HomQtyLinear(*matLaw);
    materialLaw->addGroupOfDomains(*(itDom->second), *HLQ); 
  }
  else Msg::Error("The selected groupOfDomains does not exist. \n");  
}
void hmmProblemBase::changeMQB(std::string tagMaterialLaw, std::string tagGroupOfDomains,
                               double mat11, double mat12, double mat13, 
                               double mat21, double mat22, double mat23, 
                               double mat31, double mat32, double mat33) {
  std::map<std::string, hmmGroupOfDomains *>::iterator itDom = allDomains.find(tagGroupOfDomains);
  if (itDom != allDomains.end() ) {
    std::map<std::string, hmmMaterialLawBase *>::iterator itMatLaw = allMaterialLaws.find(tagMaterialLaw);
    if(itMatLaw != allMaterialLaws.end() ) { // The ML exists
      std::map<hmmGroupOfDomains *, MLQtyBase *>::const_iterator itGoDMQB = itMatLaw->second->getMatLawByGroupOfDomains()->find(itDom->second);
      if(itGoDMQB != itMatLaw->second->getMatLawByGroupOfDomains()->end() ) { 
        Msg::Info("The material law already exists.");
        Msg::Info("Adding the GoD to the material law tagged %s.", tagMaterialLaw.c_str() );
        fullMatrix<double> * matLaw = new fullMatrix<double>(3, 3);
        matLaw->set(0, 0, mat11); matLaw->set(0, 1, mat12); matLaw->set(0, 2, mat13);
        matLaw->set(1, 0, mat21); matLaw->set(1, 1, mat22); matLaw->set(1, 2, mat23);
        matLaw->set(2, 0, mat31); matLaw->set(2, 1, mat32); matLaw->set(2, 2, mat33);
        HomQtyLinear *HLQ = new HomQtyLinear(*matLaw);
        itMatLaw->second->setMQB( *(itDom->second), *HLQ);
        Msg::Info("Finished adding the GoD to the material law...\n ");
      }
    }
    else {
      Msg::Info("You are changing the MQB of a ML which does not exists");     
    }
  }
  else Msg::Error("The selected groupOfDomains does not exist. \n");  
}

// //==================================================================================================================
// void hmmProblemBase::add2ScaleLinearMaterialLaw(std::string tagMaterialLaw, std::string hmmGroupOfDomainsTag, std::string GMSHVerbosity, 
//                                                 std::string GMSHGeoFileName, std::string GetDPVerbosity, std::string GetDPProFileName, 
//                                                 std::string GetDPResolutionName, std::string GetDPPostPro, SPoint3 &computPoint) {
//   std::map<std::string, hmmGroupOfDomains *>::iterator itDom = allDomains.find(hmmGroupOfDomainsTag);
//   if ( itDom != allDomains.end() ) {
//     double func = 1.0;
//     SVector3 gradFunc = SVector3(1.0, 1.0, 1.0);
//     double x_gauss = computPoint.x(), y_gauss = computPoint.y();
    
//     FILE *fp_gaussPointPosition;
//     fp_gaussPointPosition = fopen("output/gaussPointPosition.dat", "w") ;
//     fprintf(fp_gaussPointPosition, "x_gauss = %.16lf ; y_gauss = %.16lf ; \n", computPoint.x(), y_gauss );
//     fprintf(fp_gaussPointPosition, "lx = %.16lf ; ly = %.16lf ; \n", _lx, _ly);
//     fclose(fp_gaussPointPosition) ;
    
//     hmmMicroSolverGetDPLinearDouble *hmmMaterialLaw = new hmmMicroSolverGetDPLinearDouble( *(itDom->second), tagMaterialLaw, _lx, _ly, _nx, _ny); //FixMe
//     hmmMaterialLaw->setMicroParam(GMSHVerbosity, GMSHGeoFileName, GetDPVerbosity, GetDPProFileName, GetDPResolutionName, GetDPPostPro);
//     HomQtyLinear *currentHomQtyLinear = (HomQtyLinear *) (hmmMaterialLaw->computeMaterialLaw());
//     hmmMaterialLaw->setMaterialLaw(*currentHomQtyLinear);
//     this->allMaterialLaws.insert(std::make_pair(tagMaterialLaw, hmmMaterialLaw) );
//   }
//   else Msg::Error("The selected domain does not exist. \n");
// }

// void hmmProblemBase::add2ScaleLinearMaterialLawInno(std::string tagMaterialLaw, std::string tagGroupOfDomains, std::string GMSHVerbosity, 
//                                                 std::string GMSHGeoFileName, std::string GetDPVerbosity, std::string GetDPProFileName, 
//                                                 std::string GetDPResolutionName, std::string GetDPPostPro, SPoint3 &computPoint) {
//   std::map<std::string, hmmGroupOfDomains *>::iterator itDom = allDomains.find(tagGroupOfDomains);
//   if ( itDom != allDomains.end() ) {
//     double func = 1.0;
//     SVector3 gradFunc = SVector3(1.0, 1.0, 1.0);
//     double x_gauss = computPoint.x(), y_gauss = computPoint.y();
    
//     FILE *fp_gaussPointPosition;
//     fp_gaussPointPosition = fopen("output/gaussPointPosition.dat", "w") ;
//     fprintf(fp_gaussPointPosition, "x_gauss = %.16lf ; y_gauss = %.16lf ; \n", computPoint.x(), y_gauss );
//     fprintf(fp_gaussPointPosition, "lx = %.16lf ; ly = %.16lf ; \n", _lx, _ly);
//     fclose(fp_gaussPointPosition) ;
    
//     hmmMicroSolverGetDPLinearDouble *hmmMaterialLaw = new hmmMicroSolverGetDPLinearDouble( *(itDom->second), tagMaterialLaw, _lx, _ly, _nx, _ny); //FixMe
//     hmmMaterialLaw->setMicroParam(GMSHVerbosity, GMSHGeoFileName, GetDPVerbosity, GetDPProFileName, GetDPResolutionName, GetDPPostPro);
//     HomQtyLinear *currentHomQtyLinear = (HomQtyLinear *) (hmmMaterialLaw->computeMaterialLaw());
//     hmmMaterialLaw->setMaterialLaw(*currentHomQtyLinear);
//     this->allMaterialLaws.insert(std::make_pair(tagMaterialLaw, hmmMaterialLaw) );
//   }
//   else Msg::Error("The selected domain does not exist. \n");








// //   std::map<std::string, hmmGroupOfDomains *>::iterator itDom = allDomains.find(tagGroupOfDomains);
// //   // Check if the hmmGoD exists
// //   if (itDom != allDomains.end() ) {
// //     std::map<std::string, hmmMaterialLawBase *>::iterator itMatLaw = allMaterialLaws.find(tagMaterialLaw);
// //     // Check if the ML exists
// //     hmmMicroSolverGetDPLinearDouble *materialLaw;
// //     if(itMatLaw == allMaterialLaws.end() ) { // The ML does not exist
// //       Msg::Info("The material law you are adding does not exist.");
// //       Msg::Info("Start creating the new material law...");
// //       hmmMicroSolverGetDPLinearDouble *hmmMaterialLaw = new hmmMicroSolverGetDPLinearDouble( *(itDom->second), tagMaterialLaw, _lx, _ly, _nx, _ny); //FixMe
// //       materialLaw = new hmmLinearMaterialLaw(tagMaterialLaw); //FixMe 
// //       Msg::Info("End creating the new material law...");
// //     }
// //     else { // The ML exists
// //       std::map<hmmGroupOfDomains *, MLQtyBase *>::const_iterator itGoDMQB = itMatLaw->second->getMatLawByGroupOfDomains()->find(itDom->second);
// //       if(itGoDMQB == itMatLaw->second->getMatLawByGroupOfDomains()->end() ) { 
// //         Msg::Info("The material law already exists.");
// //         materialLaw = (hmmLinearMaterialLaw *) (itMatLaw->second);
// //       }
// //       else {
// //         Msg::Info("You can not change the MQB of an already defined ML in this method. You should instead use the method 'changeMQB'"); 
// //       }
// //     }
// //     fullMatrix<double> * matLaw = new fullMatrix<double>(3, 3);
// //     matLaw->set(0, 0, mat11); matLaw->set(0, 1, mat12); matLaw->set(0, 2, mat13);
// //     matLaw->set(1, 0, mat21); matLaw->set(1, 1, mat22); matLaw->set(1, 2, mat23);
// //     matLaw->set(2, 0, mat31); matLaw->set(2, 1, mat32); matLaw->set(2, 2, mat33);
// //     HomQtyLinear *HLQ = new HomQtyLinear(*matLaw);
// //     materialLaw->addGroupOfDomains(*(itDom->second), *HLQ); 
// //     allMaterialLaws.insert(std::make_pair(tagMaterialLaw, materialLaw) ); 
// //     Msg::Info("end creating the new material law...\n ");
// //   }
// //   else Msg::Error("The selected groupOfDomains does not exist. \n");  



// }


void hmmProblemBase::add2ScaleNonLinearMaterialLaw(std::string tagMaterialLaw, std::string hmmGroupOfDomainsTag, std::string tagSpace, std::string tagQuad,
                                                   std::string GMSHVerbosity, std::string GMSHGeoFileName, std::string GetDPVerbosity, 
                                                   std::string GetDPProFileName, std::string GetDPResolutionName, std::string GetDPPostPro, std::string GMSHExactGeoFileName, std::string GMSHExactProFileName, std::string microProblemFileName) {
  std::map<std::string, hmmGroupOfDomains *>::iterator itDom = allDomains.find(hmmGroupOfDomainsTag);
  if ( itDom != allDomains.end() ) {
    std::map<std::string, hmmScalarLagrangeFunctionSpace *>::iterator itSpace = allFunctionSpaces.find(tagSpace); //FixMe general FS
    std::map<std::string, GaussQuadrature *>::iterator itQuad = allQuadratureRules.find(tagQuad); 
    if(itSpace != allFunctionSpaces.end() ) {
      if(itQuad != allQuadratureRules.end() ) {
        hmmMicroSolverGetDPNonLinearDouble *hmmMaterialLaw = new hmmMicroSolverGetDPNonLinearDouble(*(_assembler), *(itSpace->second), *(itQuad->second), 
                                                                                                    tagMaterialLaw, _lx, _ly, _nx, _ny);
        HomQtyNL *thisMQB = new HomQtyNL( *(itDom->second) );
        hmmMaterialLaw->setMQB( *(itDom->second), *thisMQB);
        hmmMaterialLaw->setMicroParam(GMSHVerbosity, GMSHGeoFileName, GetDPVerbosity, GetDPProFileName, GetDPResolutionName, GetDPPostPro, GMSHExactGeoFileName, GMSHExactProFileName, microProblemFileName);
        allMaterialLaws.insert(std::make_pair(tagMaterialLaw, hmmMaterialLaw) );
      }
      else Msg::Error("The selected quadrature rule does not exist. \n");
    }
    else Msg::Error("The selected function space does not exist. \n");
  }
  else Msg::Error("The selected domain does not exist. \n");
}


void hmmProblemBase::add2ScaleNonLinearMaterialLawInno(std::string tagMaterialLaw, std::string tagGroupOfDomains, std::string tagSpace, std::string tagQuad,
                                                   std::string GMSHVerbosity, std::string GMSHGeoFileName, std::string GetDPVerbosity, 
                                                   std::string GetDPProFileName, std::string GetDPResolutionName, std::string GetDPPostPro, std::string GMSHExactGeoFileName, std::string GMSHExactProFileName, std::string microProblemFileName) {
  std::map<std::string, hmmGroupOfDomains *>::iterator itDom = allDomains.find(tagGroupOfDomains);
  // Check if the hmmGoD exists
  if ( itDom != allDomains.end() ) {
    std::map<std::string, hmmScalarLagrangeFunctionSpace *>::iterator itSpace = allFunctionSpaces.find(tagSpace); //FixMe general FS
    std::map<std::string, GaussQuadrature *>::iterator itQuad = allQuadratureRules.find(tagQuad); 
    std::map<std::string, hmmMaterialLawBase *>::iterator itMatLaw = allMaterialLaws.find(tagMaterialLaw); 
    hmmMicroSolverGetDPNonLinearDouble *hmmMaterialLaw;
        // Check if the FS, the integration method and the ML exist
    if(itSpace != allFunctionSpaces.end() ) {
      if(itQuad != allQuadratureRules.end() ) {
        if(itMatLaw == allMaterialLaws.end() ) { 
          // The ML does not exist ==> create it 
          hmmMaterialLaw = new hmmMicroSolverGetDPNonLinearDouble(*(_assembler), *(itSpace->second), *(itQuad->second), 
                                                                  tagMaterialLaw, _lx, _ly, _nx, _ny);
          hmmMaterialLaw->setMicroParam(GMSHVerbosity, GMSHGeoFileName, GetDPVerbosity, GetDPProFileName, GetDPResolutionName, GetDPPostPro, GMSHExactGeoFileName, GMSHExactProFileName, microProblemFileName);
          
          allMaterialLaws.insert(std::make_pair(tagMaterialLaw, hmmMaterialLaw) );
          
          Msg::Info("Nonlinear successfully added. This ML is tagged %s", tagMaterialLaw.c_str() );
        }
        else {
          // The ML exists ==> verifiy if it is defined on hmmGoD 

          std::map<hmmGroupOfDomains *, MLQtyBase *>::const_iterator itGoDMQB = itMatLaw->second->getMatLawByGroupOfDomains()->find(itDom->second);
          if(itGoDMQB == itMatLaw->second->getMatLawByGroupOfDomains()->end() ) { 
            Msg::Info("The material law already exists.");
            hmmMaterialLaw = (hmmMicroSolverGetDPNonLinearDouble *) (itMatLaw->second);
          }
          else {
            Msg::Info("You can not change the MQB of an already defined ML in this method. You should instead use the method 'changeMQB'"); 
          }
        }
        HomQtyNL *NLMQB = new HomQtyNL( *(itDom->second ));
        hmmMaterialLaw->addGroupOfDomains( *(itDom->second), *NLMQB);
        Msg::Info("Finished adding the GoD to the material law...\n ");
      }
      else Msg::Error("The selected quadrature rule does not exist. \n");
    }
    else Msg::Error("The selected function space does not exist. \n");
  }
  else {
    Msg::Error("The selected domain does not exist. \n");
  }
}
// void hmmProblemBase::Init2ScaleNonLinearML(std::string tagMaterialLaw, double mat11, double mat12, double mat13, double mat21, 
//                                            double mat22, double mat23, double mat31, double mat32, double mat33) {
//   std::map<std::string, hmmMaterialLawBase *>::iterator itML = this->allMaterialLaws.find(tagMaterialLaw);
//   if (itML->second->getType() == HMM_NONLINEAR_MLAW) {
//     hmmMicroSolverGetDPNonLinearDouble *currentNLML = (hmmMicroSolverGetDPNonLinearDouble *) (itML->second);
//     fullMatrix<double> *initMat = new fullMatrix<double>(3, 3);
//     initMat->set(0, 0, mat11); initMat->set(0, 1, mat12); initMat->set(0, 2, mat13);
//     initMat->set(1, 0, mat21); initMat->set(1, 1, mat22); initMat->set(1, 2, mat23);
//     initMat->set(2, 0, mat31); initMat->set(2, 1, mat32); initMat->set(2, 2, mat33);
//     currentNLML->initMaterialLaw(*initMat); 
//   }
//   else 
//     Msg::Error("The material law being initialized is not nonlinear. \n");
// }
void hmmProblemBase::Init2ScaleNonLinearMLInno(std::string tagMaterialLaw, std::string tagGroupOfDomains, double mat11, double mat12, double mat13, double mat21, 
                                               double mat22, double mat23, double mat31, double mat32, double mat33) {
  // Initialize the ML defined on the hmmGoD tagged as 'tagGroupOfDomains' 
  std::map<std::string, hmmMaterialLawBase *>::iterator itML = this->allMaterialLaws.find(tagMaterialLaw);
  if (itML->second->getType() == HMM_NONLINEAR_MLAW) {
    std::map<std::string, hmmGroupOfDomains *>::iterator itStrGoD = allDomains.find(tagGroupOfDomains);
    if(itStrGoD != allDomains.end() ) {
      // chech if the hmmGoD exists
      std::map<hmmGroupOfDomains *, MLQtyBase *>::const_iterator itGoDMQB = itML->second->getMatLawByGroupOfDomains()->find(itStrGoD->second);
      if(itGoDMQB != itML->second->getMatLawByGroupOfDomains()->end() ) {
        hmmMicroSolverGetDPNonLinearDouble *currentNLML = (hmmMicroSolverGetDPNonLinearDouble *) (itML->second);
        fullMatrix<double> *initMat = new fullMatrix<double>(3, 3);
        initMat->set(0, 0, mat11); initMat->set(0, 1, mat12); initMat->set(0, 2, mat13);
        initMat->set(1, 0, mat21); initMat->set(1, 1, mat22); initMat->set(1, 2, mat23);
        initMat->set(2, 0, mat31); initMat->set(2, 1, mat32); initMat->set(2, 2, mat33);
        //currentNLML->initMaterialLaw(*initMat); 
        currentNLML->initMaterialLaw( *(itGoDMQB->first), *initMat); 

        // Create all meshes for all the Gauss points.
        //============================================
        SystemCall("rm -f ../meso/mesoMeshes/*", true);
        HomQtyNL *thisNLMQB = (HomQtyNL *)(currentNLML->getMQB(*(itStrGoD->second) ) );
        std::map<MElement*, std::map<int, HomQtyLinear *> >::iterator itMEleMQB;
        //Msg::Error("Check 1. Dim HQMLB = %d", thisNLMQB->getNLHomQty()->size() );
        // Loop over all the element
        for (itMEleMQB = thisNLMQB->begin(); itMEleMQB != thisNLMQB->end(); ++itMEleMQB ) {
          MElement *ele = itMEleMQB->first;
          // Loop over Gauss Points for a given element
          std::map<int, HomQtyLinear *>::iterator itIntHom;
          for (itIntHom = itMEleMQB->second.begin(); itIntHom != itMEleMQB->second.end(); ++itIntHom) {
            // create points 
            SPoint3 thisGP = *(currentNLML->convertFromIntToPoint(ele, itIntHom->first) );
            // write the data of the GP in microMeshes
            FILE *fp_meshes;
            fp_meshes = fopen("../meso/mesoMeshes/gaussPointPosition.dat", "w") ;
            fprintf(fp_meshes, "x_gauss = %.16lf ; y_gauss = %.16lf ; \n", thisGP.x(), thisGP.y() );
            fprintf(fp_meshes, "lx = %.16lf ; ly = %.16lf ; \n", currentNLML->getLengthX(), currentNLML->getLengthY());
            fprintf(fp_meshes, "numEle = %d ; \n", ele->getNum());
            fclose(fp_meshes) ; 
            std::string _thisGMSHMSHFileName = "micro_smc";//currentNLML->getGMSHGeoFileName();
            std::string strThisEleNum = convertInt2Str(ele->getNum() );
            _thisGMSHMSHFileName += "_" + strThisEleNum + ".msh"; 
            Msg::Info(strThisEleNum.c_str() );


            //Msg::Info(string_inno.c_str() );
            printf("The number of Gauss point is %d \n", ele->getNum() );
            _thisGMSHMSHFileName = "../meso/mesoMeshes/" + _thisGMSHMSHFileName;
            // Mesh the geometry
            SystemCall("rm -f ~/.gmshrc", true);
            Msg::Warning("This is the geoFileName %s, \n", currentNLML->getGMSHGeoFileName().c_str() );
            Msg::Warning("This is the GMSHVerbosity %s, \n", currentNLML->getGMSHVerbosity().c_str() );
            SystemCall("gmsh -verbose " + currentNLML->getGMSHVerbosity() + currentNLML->getGMSHGeoFileName() + " -2 -o " + _thisGMSHMSHFileName, true);
            //SystemCall("gmsh -verbose 2 "  + currentNLML->getGMSHGeoFileName() + " -2 -o " + _thisGMSHMSHFileName, true);
          }
        }
      }
    }
    else {
      Msg::Error("The hmmGoD is not defined.");
    }
  }
  else {
    Msg::Error("The material law being initialized is not nonlinear.");
  }
}


void hmmProblemBase::Init2ScaleNonLinearMap(std::string tagMaterialLaw, std::string tagGroupOfDomains, double JL) {
  // Initialize the ML defined on the hmmGoD tagged as 'tagGroupOfDomains' 
  std::map<std::string, hmmMaterialLawBase *>::iterator itML = this->allMaterialLaws.find(tagMaterialLaw);
  if (itML->second->getType() == HMM_NONLINEAR_MLAW) {
    std::map<std::string, hmmGroupOfDomains *>::iterator itStrGoD = allDomains.find(tagGroupOfDomains);
    if(itStrGoD != allDomains.end() ) {
      hmmMicroSolverGetDPNonLinearDouble *thisML = static_cast<hmmMicroSolverGetDPNonLinearDouble *>(itML->second);
      thisML->initJLMap( *(itStrGoD->second), JL); 
    }
    else {
      Msg::Error("The hmmGoD is not defined.");
    }
  }
  else {
    Msg::Error("The material law being initialized is not nonlinear.");
  }
}
void hmmProblemBase::Init2ScaleNonLinearHLMap(std::string tagQuad, std::string tagMaterialLaw, std::string tagGoD, double initTime, double HL) {
  // Initialize the ML defined on the hmmGoD tagged as 'tagGroupOfDomains' 
  std::map<std::string, hmmMaterialLawBase *>::iterator itML = this->allMaterialLaws.find(tagMaterialLaw);
  std::map<std::string, GaussQuadrature *>::iterator itQuad = this->allQuadratureRules.find(tagQuad);
  std::map<std::string, hmmGroupOfDomains *>::iterator itGoD = this->allDomains.find(tagGoD);
  if (itML->second->getType() == HMM_NONLINEAR_MLAW && (itQuad != this->allQuadratureRules.end() ) ) {
    hmmMicroSolverGetDPNonLinearDouble *thisML = static_cast<hmmMicroSolverGetDPNonLinearDouble *>(itML->second);
    HomQtyHystereticLosses *thisHLMap = new HomQtyHystereticLosses(*(itGoD->second) );
    thisHLMap->InitHLMapTimeStep( *(itQuad->second), initTime, HL);
    thisML->addHLMap(*thisHLMap); 
  }
  else {
    Msg::Error("The material law being initialized is not nonlinear, does not exist or the quadrature rule does not exist.");
  }
}

//================================================================================================================================================
void hmmProblemBase::Init2ScaleNonLinearGQMap(std::string tagQuad, std::string tagMaterialLaw, std::string tagGoD) {
  // Initialize the ML defined on the hmmGoD tagged as 'tagGroupOfDomains' 
  std::map<std::string, hmmMaterialLawBase *>::iterator itML = this->allMaterialLaws.find(tagMaterialLaw);
  std::map<std::string, GaussQuadrature *>::iterator itQuad = this->allQuadratureRules.find(tagQuad);
  std::map<std::string, hmmGroupOfDomains *>::iterator itGoD = this->allDomains.find(tagGoD);
  if (itML->second->getType() == HMM_NONLINEAR_MLAW && (itQuad != this->allQuadratureRules.end() ) ) {
    hmmMicroSolverGetDPNonLinearDouble *thisML = static_cast<hmmMicroSolverGetDPNonLinearDouble *>(itML->second);
    HomQtyGlobalQuantities *thisGQMap = new HomQtyGlobalQuantities(*(itGoD->second) );
    thisML->addGQMap(*thisGQMap); 
  }
  else {
    Msg::Error("The material law being initialized is not nonlinear, does not exist or the quadrature rule does not exist.");
  }
}
void hmmProblemBase::addGQTo2ScaleNonLinearGQMap(std::string tagGQ, std::string tagMaterialLaw) {
  // Initialize the ML defined on the hmmGoD tagged as 'tagGroupOfDomains' 
  std::map<std::string, hmmMaterialLawBase *>::iterator itML = this->allMaterialLaws.find(tagMaterialLaw);
  hmmMicroSolverGetDPNonLinearDouble *thisML = static_cast<hmmMicroSolverGetDPNonLinearDouble *>(itML->second);
  thisML->addOneGQ(tagGQ);
  return;
}
void hmmProblemBase::InitGQMap(std::string tagGQ, std::string tagMaterialLaw, double initTime, double HL) {
  // Initialize the ML defined on the hmmGoD tagged as 'tagGroupOfDomains' 
  std::map<std::string, hmmMaterialLawBase *>::iterator itML = this->allMaterialLaws.find(tagMaterialLaw);
  hmmMicroSolverGetDPNonLinearDouble *thisML = static_cast<hmmMicroSolverGetDPNonLinearDouble *>(itML->second);
  thisML->initGQ( tagGQ, initTime, HL);
}
//===================================================================================================================================================


hmmMaterialLawBase *hmmProblemBase::getMaterialLaw(std::string tagML) const {
  std::map<std::string, hmmMaterialLawBase *>::const_iterator itML =  allMaterialLaws.find(tagML);
  if (itML != allMaterialLaws.end() ) 
    return itML->second;
  else {
    Msg::Error("The selected material law does not exist. \n");
    return NULL;
  }
}
void hmmProblemBase::printMaterialLaw(std::string tagMaterialLaw, std::string tagGoD) const {
  std::map<std::string, hmmMaterialLawBase *>::const_iterator itML = allMaterialLaws.find(tagMaterialLaw);
  if (itML != allMaterialLaws.end() ) {
    std::map<std::string, hmmGroupOfDomains *>::const_iterator itGoD = allDomains.find(tagGoD);
    //std::map<hmmGroupOfDomains *, MLQtyBase *>::const_iterator itGoDMQB = itML->second->getMatLawByGroupOfDomains()
    std::string nameAllGoD = "";
    if (itGoD != allDomains.end() ) {
      itML->second->printML( *(itGoD->second));
      std::map<hmmGroupOfDomains *, MLQtyBase *>::const_iterator itGoD;
      for (itGoD = (*(itML->second->getMatLawByGroupOfDomains() ) ).begin(); itGoD != (*(itML->second->getMatLawByGroupOfDomains() ) ).end(); itGoD++) {
        nameAllGoD = nameAllGoD + itGoD->first->getTagGroupOfDomains() + " and " ;
      }
      Msg::Warning("Done in hmmProblem.cpp. KKKKKKKKK The material law tagged %s has been defined on domains %s.\n ", itML->second->getTag().c_str(), nameAllGoD.c_str() );
    }
    else {
      Msg::Error("Done in hmmProblem. You are printing the material law '%s' that was not defined on the domain '%s'.", 
                 (tagMaterialLaw.c_str() ),  (tagGoD.c_str() ));
    }
  }
  else {
    Msg::Error("Done in hmmProblem. You are printing the material law '%s' that was not defined.", 
               (tagMaterialLaw.c_str() ) );
  }
  return;
}
void hmmProblemBase::printMaterialLaw(std::string tagMaterialLaw) const {
  std::map<std::string, hmmMaterialLawBase *>::const_iterator itML = allMaterialLaws.find(tagMaterialLaw);
  if (itML != allMaterialLaws.end() ) {
    itML->second->printMLOnAllDom();
  }
  else {
    Msg::Error("Done in hmmProblem. You are printing the material law '%s' that was not defined.", 
               (tagMaterialLaw.c_str() ) );
  }
  return;
}
// 4. hmmConstraints
//==================
void hmmProblemBase::getConstraintsInfo() const {
  Msg::Info("--\n");
  Msg::Info("There is a total of %d constraints. \n", allDirichletBC.size() );
  std::map<std::string, hmmDirichlet *>::const_iterator itDiriBC;
  for (itDiriBC = allDirichletBC.begin(); itDiriBC != allDirichletBC.end(); ++itDiriBC) {
    std::cout << "The current boundary condition is tagged "<< itDiriBC->first << std::endl;
  }
  Msg::Info("--\n");
}
void hmmProblemBase::addDirichletBC(std::string tagGroupOfDomain, std::string tagCons, int comp, double imposedValue) {
  std::map<std::string, hmmGroupOfDomains *>::iterator itDom = allDomains.find(tagGroupOfDomain);
  if ( itDom != allDomains.end() ) {
    hmmDirichlet *DiriBC = new hmmDirichlet( *(itDom->second), tagCons, comp, imposedValue); //FixMe
    allDirichletBC.insert(std::make_pair(tagCons, DiriBC) );
  }
  else Msg::Info("The selected domain does not exist. \n");
}
hmmDirichlet *hmmProblemBase::getDirichletBC(std::string tagCons) const {
  std::map<std::string, hmmDirichlet *>::const_iterator itCons = allDirichletBC.find(tagCons);
  if (itCons != allDirichletBC.end() ) 
    return itCons->second;
  else {
    Msg::Error("The selected boundary condition does not exist. \n");
    return NULL;
  }
}

// 5. quadratures rules
//=====================
void hmmProblemBase::getQuadInfo() const {
  Msg::Info("--\n");
  Msg::Info("There is a total of %d quadrature rules. \n", allQuadratureRules.size() );
  std::map<std::string, GaussQuadrature *>::const_iterator itQuad;
  for (itQuad = allQuadratureRules.begin(); itQuad != allQuadratureRules.end(); ++itQuad) {
    std::cout << "The current quadrature rule is tagged "<< itQuad->first << std::endl;
  }
  Msg::Info("--\n");
}
void hmmProblemBase::addQuad(std::string tagQuad) {
  GaussQuadrature *quad = new GaussQuadrature(GaussQuadrature::GradGrad); //FixMe
  allQuadratureRules.insert(std::make_pair(tagQuad, quad) );
}
GaussQuadrature *hmmProblemBase::getQuad(std::string tagQuad) const {
  std::map<std::string, GaussQuadrature *>::const_iterator itQuad = allQuadratureRules.find(tagQuad);
  if (itQuad != allQuadratureRules.end() ) 
    return itQuad->second;
  else {
    Msg::Error("The selected quadrature rules does not exist. \n");
    return NULL;
  }
}

// 6. hmmFunctionSpace
//====================
void hmmProblemBase::getFunctionSpaceInfo() const {
  Msg::Info("--\n");
  Msg::Info("There is a total of %d function spaces. \n", allFunctionSpaces.size() );
  std::map<std::string, hmmScalarLagrangeFunctionSpace *>::const_iterator itSpace; //FixMe
  for (itSpace = allFunctionSpaces.begin(); itSpace != allFunctionSpaces.end(); ++itSpace) {
    std::cout << "The current functionSpace is tagged "<< itSpace->first << " and it has " 
              << itSpace->second->getConstraint()->size() << " constraints." <<std::endl;
  }
  Msg::Info("--\n");
}
void hmmProblemBase::addFunctionSpace(std::string tagFS, std::string tagDom, int iField, 
                                        FSType funcSpacType, BFType BasisFuncType) {
  std::map<std::string, hmmGroupOfDomains *>::iterator itDom = allDomains.find(tagDom);
  if (itDom != allDomains.end() ) {
    if (funcSpacType == Form0) {
      hmmScalarLagrangeFunctionSpace *LagSpace = new hmmScalarLagrangeFunctionSpace( *(itDom->second), iField);
      allFunctionSpaces.insert(std::make_pair(tagFS, LagSpace) );
      Msg::Info("The function space with Node basis functions has been successfully created. \n");
    }
    else if (funcSpacType == Form1P) {
      hmmBFPerpendicularEdge *LagSpace = new hmmBFPerpendicularEdge( *(itDom->second), iField);
      allFunctionSpaces.insert(std::make_pair(tagFS, LagSpace) );
      Msg::Info("The function space with PerpendicularEdge basis functions has been successfully created. \n");
    }
    else { 
      Msg::Error(" The selected function space type is not implemented yet. \n");
    }
  }
  else Msg::Error("The groupOfDomains supporting the function space does not exist. \n");
}
void hmmProblemBase::addConstraintToFunctionSpace(std::string tagFS, std::string tagCons) {
  std::map<std::string, hmmScalarLagrangeFunctionSpace *>::iterator itSpace = allFunctionSpaces.find(tagFS); //FixMe
  std::map<std::string, hmmDirichlet *>::iterator itDiri = allDirichletBC.find(tagCons);                     //FixMe
  if ( itSpace != allFunctionSpaces.end() ) {
    if (itDiri != allDirichletBC.end() ) {
      itSpace->second->setConstraint( *(itDiri->second) );
      std::cout <<" The constraints tagged "<< itDiri->first <<" has been successfully added to the function space." << itSpace->first << std::endl;
    }
    else Msg::Error("The selected constraints does not exist. \n");
  }
  else Msg::Error("The selected function space  does not exist. \n");
}
hmmScalarLagrangeFunctionSpace *hmmProblemBase::getFunctionSpace(std::string tagFS) const {
  std::map<std::string, hmmScalarLagrangeFunctionSpace *>::const_iterator itSpace = allFunctionSpaces.find(tagFS);
  if ( itSpace != allFunctionSpaces.end() ) 
    return itSpace->second;
  else {
    Msg::Error("The selected function space does not exist. \n");
    return NULL;
  }  
}

// 7. hmmFormulation
//==================

void hmmProblemBase::getFormulationInfo() const {
  Msg::Info("--\n");
  Msg::Info("There is a total of %d formulations. \n", allFormulations.size() );
  std::map<std::string, hmmFormulation *>::const_iterator itForm;
  for (itForm = allFormulations.begin(); itForm != allFormulations.end(); ++itForm)
    std::cout << "The current formulation is tagged "<< itForm->first << " and it has " << itForm->second->getVectorOfBilinearTerms()->size() 
              << " bilinear terms and "<< itForm->second->getVectorOfLinearTerms()->size() << " linear terms."<< std::endl;
  Msg::Info("--\n");
}
void hmmProblemBase::addFormulation(std::string tagForm) {
  hmmFormulation *currentFormulation = new hmmFormulation(tagForm);
  allFormulations.insert(std::make_pair(tagForm, currentFormulation) );
}
void hmmProblemBase::addBilinearTermToFormulation(std::string tagForm, std::string tagSpace, std::string tagMaterialLaw,  // Fix Me add the lin term of HMM_NON_LINEAR
                                          std::string tagDom, BType whatType, std::string tagQuad) {
  std::map<std::string, hmmFormulation *>::iterator itForm = allFormulations.find(tagForm);
  std::map<std::string, hmmScalarLagrangeFunctionSpace *>::iterator itSpace = allFunctionSpaces.find(tagSpace); // FixMe
  std::map<std::string, hmmMaterialLawBase *>::iterator itMaterialLaw = allMaterialLaws.find(tagMaterialLaw); // FixMe
  std::map<std::string, hmmGroupOfDomains *>::iterator itDomain = allDomains.find(tagDom);
  std::map<std::string, GaussQuadrature *>::iterator itQuad = allQuadratureRules.find(tagQuad);  
  itForm->second->addBilinearTerm( *(itSpace->second), *(itMaterialLaw->second), *(itDomain->second), whatType, *(itQuad->second) );
  Msg::Info("The number of bilinear terms is %d. \n", itForm->second->getVectorOfBilinearTerms()->size() );
  std::cout <<"You have just added the material law labelled '" << itMaterialLaw->first <<"'"<<std::endl;
}
void hmmProblemBase::addLinearTermToFormulation(std::string tagForm, std::string tagSpace, 
                                                std::string tagDom, std::string tagML, std::string tagQuad) {
  std::map<std::string, hmmFormulation *>::iterator itForm = allFormulations.find(tagForm);
  std::map<std::string, hmmScalarLagrangeFunctionSpace *>::iterator itSpace = allFunctionSpaces.find(tagSpace);
  std::map<std::string, hmmGroupOfDomains *>::iterator itDomain = allDomains.find(tagDom);
  std::map<std::string, hmmMaterialLawBase *>::iterator itML = allMaterialLaws.find(tagML);
  std::map<std::string, GaussQuadrature *>::iterator itQuad = allQuadratureRules.find(tagQuad);  
  itForm->second->addLinearTerm( *(itSpace->second), *(itML->second), *(itDomain->second), *(itQuad->second) );
  Msg::Info("the size of vector of LinearTerms is %d \n", itForm->second->getVectorOfLinearTerms()->size());
}

void hmmProblemBase::addSourceTermToFormulation(std::string tagForm, std::string tagSpace, std::string tagDom, 
                                                std::string tagQuad, double js_x, double js_y, double js_z) {
  std::map<std::string, hmmFormulation *>::iterator itForm = allFormulations.find(tagForm);
  std::map<std::string, hmmScalarLagrangeFunctionSpace *>::iterator itSpace = allFunctionSpaces.find(tagSpace);
  std::map<std::string, hmmGroupOfDomains *>::iterator itDomain = allDomains.find(tagDom);
  std::map<std::string, GaussQuadrature *>::iterator itQuad = allQuadratureRules.find(tagQuad);  
  fullVector<double> *source = new fullVector<double>(3);
  source->set(0, js_x); source->set(1, js_y); source->set(2, js_z);
  Msg::Info("source(0) is equal to %g \n", (*source)(2) );
  itForm->second->addLinearTerm( *(itSpace->second), *source, *(itDomain->second), *(itQuad->second) );
  Msg::Info("the size of vector of LinearTerms is %d \n", itForm->second->getVectorOfLinearTerms()->size());
}

void hmmProblemBase::addSourceTermToFormulation(std::string tagForm, std::string tagSpace, std::string tagDom, 
                                                std::string tagQuad, double js_x, double js_y, double js_z, int tag) {
  std::map<std::string, hmmFormulation *>::iterator itForm = allFormulations.find(tagForm);
  std::map<std::string, hmmScalarLagrangeFunctionSpace *>::iterator itSpace = allFunctionSpaces.find(tagSpace);
  std::map<std::string, hmmGroupOfDomains *>::iterator itDomain = allDomains.find(tagDom);
  std::map<std::string, GaussQuadrature *>::iterator itQuad = allQuadratureRules.find(tagQuad);  
  fullVector<double> *source = new fullVector<double>(3);
  source->set(0, js_x); source->set(1, js_y); source->set(2, js_z);
  Msg::Info("source(0) is equal to %g \n", (*source)(2) );
  itForm->second->addLinearTerm( *(itSpace->second), *source, *(itDomain->second), *(itQuad->second), tag);
  Msg::Info("the size of vector of LinearTerms is %d \n", itForm->second->getVectorOfLinearTerms()->size());
}

// 8. hmmResolution
//=================
void hmmProblemBase::getResolutionInfo() const {
  Msg::Info("--\n");
  Msg::Info("There is a total of %d resolutions. \n", allResolutions.size() );
  std::map<std::string, hmmResolutionBase *>::const_iterator itReso;
  for (itReso = allResolutions.begin(); itReso != allResolutions.end(); ++itReso)
    std::cout << "The current resolution is tagged "<< itReso->first << std::endl;
  Msg::Info("--\n");
}
void hmmProblemBase::setLinsysAndDofManager() {
  if (_linsys != NULL) delete _linsys; 
  //_linsys = new MultiscaleNonLinearSystemGmm<double>(_GmresValue); 
  _linsys = new MultiscaleNonLinearSystemGmm<double>(); 
  _linsys->setGmresValue(_GmresValue);
  if (_assembler != NULL) delete _assembler; 
  _assembler = new NonLinearDofManager<double>(_linsys);
}
void hmmProblemBase::addSolver(std::string resolutionName, ResType resoType) {
  if ( (resoType == statDomSolver) || (resoType == freqDomSolver) ) {
    hmmResolutionBase *_solver = new hmmResolutionBase(resolutionName, *_linsys, *_assembler, allQuadratureRules, allDirichletBC, allMaterialLaws, allFunctionSpaces, 
                                    allFormulations, _maxNumIter, _numIter, _tol);
    allResolutions.insert(std::make_pair(resolutionName, _solver) );
    Msg::Info("The static domain resolution scheme has been successfully added ! \n");
  }
  else if (resoType == timeDomSolver) {
    hmmResolutionTimeDomain *_solver = new hmmResolutionTimeDomain(resolutionName, *_linsys, *_assembler, allQuadratureRules, allDirichletBC, allMaterialLaws, allFunctionSpaces, 
                                          allFormulations, _maxNumIter, _numIter, _tol);
    allResolutions.insert(std::make_pair(resolutionName, _solver) );
    Msg::Info("The time domain resolution scheme has been successfully added ! \n");
  }
  else {
    Msg::Error("The selected type of resolution does not exist. \n");
  }
}
void hmmProblemBase::addParameters(std::string tagRes, double _initTime, double _endTime, double _dTime, double _theta, double _freq) {
  std::map<std::string, hmmResolutionBase *>::iterator itRes = allResolutions.find(tagRes);
  if(itRes->second->getResType() == timeDomSolver ) {
    hmmResolutionTimeDomain *currentRes = (hmmResolutionTimeDomain *) (itRes->second);
    currentRes->setInitTime(_initTime);
    currentRes->setEndTime(_endTime);
    currentRes->setDeltaTime(_dTime);
    currentRes->setTheta(_theta);
    currentRes->setFrequency(_freq);
  }
  else Msg::Error("The selected resolution scheme does not contain these parameters. \n");
}
void hmmProblemBase::addOp2Solver(std::string tagRes, std::string tagForm, OpType thisOp, hmmPostProBase &postPro) { // FixMe 
  std::map<std::string, hmmResolutionBase *>::iterator itReso = allResolutions.find(tagRes);
  std::map<std::string, hmmFormulation *>::iterator itForm = allFormulations.find(tagForm);
  if (itReso != allResolutions.end() ) {
    itReso->second->setOperation(tagForm, thisOp, postPro);
  }
  else Msg::Error("The selected resolution does not exist. \n");
}

// 9. hmmPostPro//==============
void hmmProblemBase::getPostProInfo() const {
  Msg::Info("--\n");
  Msg::Info("There is a total of %d postprocessings. \n", allPostPro.size() );
  std::map<std::string, hmmPostProBase *>::const_iterator itPostPro;
  for (itPostPro = allPostPro.begin(); itPostPro != allPostPro.end(); ++itPostPro)
    std::cout << "The current post-processing is tagged "<< itPostPro->first << std::endl;
  Msg::Info("--\n");
}
void hmmProblemBase::addPostPro(std::string tagPostPro) {
  hmmPostProBase *_postPro = new hmmPostProBase(*_model, *_assembler, allMaterialLaws, allFunctionSpaces, allFormulations, allResolutions, allQuadratureRules);
  allPostPro.insert(std::make_pair(tagPostPro, _postPro) );
  Msg::Info("The post processing has been successfully added ! \n");
}
hmmPostProBase *hmmProblemBase::getPostPro(std::string tagPostPro) const {
  std::map<std::string, hmmPostProBase *>::const_iterator itPostPro = allPostPro.find(tagPostPro);
  if(itPostPro != allPostPro.end() ) {
    return (itPostPro->second);
  }
  else {
    Msg::Error("The selected post pro does not exist. \n");
    return NULL;
  }
}
// void hmmProblemBase::addPostQuantity(std::string tagPostPro, PostQuantityName postQty) {
//   std::map<std::string, hmmPostProBase *>::iterator itPostPro = allPostPro.find(tagPostPro);
//   if (itPostPro != allPostPro.end() )
//     itPostPro->second->addPostQuantity(postQty);
//   else Msg::Error("The selected post-processing does not exist. \n");
// }
// void getPostOp(std::string tagPostPro, PostQuantityName postQty, std::string postOpName) {
//   std::map<std::string, hmmPostProBase *>::iterator itPostPro = allPostPro.find(tagPostPro);
//   if (itPostPro != allPostPro.end() ) {
//     std::map<std::string, PostQuantityName*>::iterator itPostQty = itPostPro->second->getAllPostQty().find(postQty);
//     itPostQty->second->printPostOp(postOpName);
//   }
//   else Msg::Error("The selected post-processing does not exist. \n");
// } 

void hmmProblemBase::addPostProcessing(std::string potentialFileName,std::string primalDerivedFieldFileName, std::string dualDerivedFieldFileName, 
                                       std::string tagSpace, std::string tagML, std::string tagPostPro, int T2I) {
  std::map<std::string, hmmPostProBase *>::const_iterator itPostPro = allPostPro.find(tagPostPro);
  if (itPostPro != allPostPro.end() ) {
    hmmPostProBase * _postPro = itPostPro->second;
    _postPro->buildPotentialView(tagSpace, potentialFileName, T2I, "output/a_View_");
    _postPro->buildPrimalDerivedFieldView(tagSpace, primalDerivedFieldFileName, T2I, "output/b_View_");
    _postPro->buildDualDerivedFieldView(tagSpace, tagML, dualDerivedFieldFileName, T2I, "output/h_View_");
    _postPro->buildDualDerivedFieldView_HMM(tagSpace, tagML, dualDerivedFieldFileName, T2I, "output/h_View_HMM_");
  }
  else Msg::Error("The selected post pro does not exist. \n");
}
void hmmProblemBase::addPostProcessingTime(std::string potentialFileName,std::string primalDerivedFieldFileName, std::string dualDerivedFieldFileName, 
                                           std::string eddyCurrentFieldFileName, std::string tagSpace, std::string tagML, std::string tagReso, 
                                           std::string tagPostPro, int T2I) {
  hmmPostProBase *_postPro = new hmmPostProBase(*_model, *_assembler, allMaterialLaws, allFunctionSpaces, allFormulations, allResolutions, allQuadratureRules);
  _postPro->buildPotentialView(tagSpace, potentialFileName, T2I, "output/a_View_");
  _postPro->buildPrimalDerivedFieldView(tagSpace, primalDerivedFieldFileName, T2I, "output/b_View_");
  _postPro->buildDualDerivedFieldView_HMM(tagSpace, tagML, dualDerivedFieldFileName, T2I, "output/h_View_HMM_");
  _postPro->buildDualDerivedFieldView(tagSpace, tagML, dualDerivedFieldFileName, T2I, "output/h_View_");
  _postPro->buildEddyCurrentView(tagSpace, tagML, tagReso, eddyCurrentFieldFileName, T2I, "output/j_View_");//FixMe Urgent
}

void hmmProblemBase::computeLinkageFluxDensity(std::string tagPostPro, std::string tagGoD, std::string tagSpace, std::string tagForm, 
                                               std::string tagResoType, std::string tagQuad, int tagLinearSourceTerm) {
  std::map<std::string, hmmPostProBase *>::const_iterator itPostPro = allPostPro.find(tagPostPro);
  if (itPostPro != allPostPro.end() ) {
    itPostPro->second->computeLinkageFluxDensityForTimeStep(tagGoD, tagSpace, tagForm, tagResoType, tagQuad, tagLinearSourceTerm) ;
  }
  else Msg::Error("The selected post pro does not exist. \n");
}

void hmmProblemBase::initMapOfPoints(std::string tagPostPro) {
  std::map<std::string, hmmPostProBase *>::iterator itPostPro = allPostPro.find(tagPostPro);
  if (itPostPro == allPostPro.end() ) {
    Msg::Error("You are adding a point to a post-processing that does not exist.");
  }
  else {
    itPostPro->second->initMapOfPoints();
  }
  return;
}

void hmmProblemBase::addPoint2MapOfPoints(std::string tagML, std::string tagPostPro, int intPt, double x, double y, double z) {
  std::map<std::string, hmmPostProBase *>::iterator itPostPro = allPostPro.find(tagPostPro);
  if (itPostPro == allPostPro.end() ) {
    Msg::Error("You are adding a point to a post-processing that does not exist.");
  }
  else {
    // Fix me you should first check if the map has been initialized
    itPostPro->second->addPoint2MapOfPoints(tagML, intPt, x, y, z);
  }
  return;
}

