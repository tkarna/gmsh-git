//
// C++ Interface: hmmSolverField
//
// Description: given a function space "fs" and a dofManager "dm", a hmmSolverField allows to calculate the 
// unknown field (and its derivatives) in any point. The classes will be templated (parameter of the dofManager)
//
// Copyright: See COPYING file that comes with this distribution
//
// bugs and problems to the public mailing list <gmsh@geuz.org>.
//


#ifndef _HMM_SOLVERFIELD_H_
#define _HMM_SOLVERFIELD_H_

#include "dofManager.h"
#include "NonLinearDofManager.h"
#include "hmmFunctionSpace.h"
#include "solverField.h"

class hmmSolverField : public SolverField<double>
{
protected:
  NonLinearDofManager<double> *_dm; 
  hmmScalarLagrangeFunctionSpace *_fs; 
public:
  hmmSolverField(NonLinearDofManager<double> &dm, FunctionSpace<double> &fs) : SolverField<double>(&dm, &fs) {
    _dm = &dm;
    _fs = (hmmScalarLagrangeFunctionSpace *)(&fs);
  }
  inline NonLinearDofManager<double> *getDofManager() const { return _dm; }
  inline hmmScalarLagrangeFunctionSpace *getFunctionSpace() const { return _fs; }
  virtual void dtF(MElement *ele, double u, double v, double w, TensorialTraits<double>::ValType &val) const;
  virtual void dtGradf(MElement *ele, double u, double v, double w, TensorialTraits<double>::GradType &grad) const;
  virtual void f(MElement *ele, double u, double v, double w, TensorialTraits<double>::ValType &val) {
    this->SolverField<double>::f(ele, u, v, w, val);
  }
  virtual void gradf(MElement *ele, double u, double v, double w, TensorialTraits<double>::GradType &grad) {
    this->SolverField<double>::gradf(ele, u, v, w, grad);
  }
  virtual void f(MElement *ele, double u, double v, double w, int stepsBack, TensorialTraits<double>::ValType &val) const;
  virtual void gradf(MElement *ele, double u, double v, double w, int stepsBack, TensorialTraits<double>::GradType &grad) const;
};


#endif //_HMM_SOLVERFIELD_H_
