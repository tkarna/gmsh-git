#include "hmmFormulation.h"

#define pi 3.1415926535897932
hmmBilinearTerm::hmmBilinearTerm( hmmScalarLagrangeFunctionSpace &space1, hmmScalarLagrangeFunctionSpace &space2, 
                                  hmmMaterialLawBase &materialLaw, hmmGroupOfDomains &dTerms, GaussQuadrature &integ) {
  _space1 = &space1; 
  _space2 = &space2; 
  _materialLaw = &materialLaw; 
  _dTerms = &dTerms ;
  _integ = &integ;
}
hmmBilinearTerm::hmmBilinearTerm( hmmScalarLagrangeFunctionSpace &space1, hmmMaterialLawBase &materialLaw, 
                                  hmmGroupOfDomains &dTerms, GaussQuadrature &integ) {
  _space1 = &space1; 
  _space2 = &space1; 
  _materialLaw = &materialLaw; 
  _dTerms = &dTerms ;
  _integ = &integ;
}

void hmmBilinearTerm::get(MElement *ele, int npts, IntPt *GP, fullMatrix<double> &m, 
                          fullVector<double> &vv, fullVector<double> &vv_Old) const {
  // BilinearTerms calculated using simple basis functions. Terms of type (\sigma \partial_t \boldsymbol{a}, \boldsymbol{a}^{'})_{\Omega_c}
  if (this->getType() == SBF) { 
    hmmScalarLagrangeFunctionSpace *_space1_;
    hmmScalarLagrangeFunctionSpace *_space2_;
    if(ele->getParent()) ele = ele->getParent();
    int nbFF = this->_space1->getNumKeys(ele);
    double jac[3][3];
    fullMatrix<double> N(3, nbFF);
    fullMatrix<double> NTH(nbFF, 3);
    fullMatrix<double> NT(nbFF, 3);
    m.resize(nbFF, nbFF);
    m.setAll(0.0);
    //=========================================================
    vv.resize(nbFF);
    vv.setAll(0.0);    
    vv_Old.resize(nbFF);
    vv_Old.setAll(0.0);
    //=========================================================
    for(int i = 0; i < npts; i++) {
      const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
      const double weight = GP[i].weight; const double detJ = ele->getJacobian(u, v, w, jac);
      if ( (_space1->getfunctionSpaceType() != Form0) && (_space2->getfunctionSpaceType() != Form0) ) { // FixMe
        _space1_ = (hmmScalarLagrangeFunctionSpace *) (_space1);
        _space2_ = (hmmScalarLagrangeFunctionSpace *) (_space2);
        std::vector<TensorialTraits<double>::ValType> func1;
        std::vector<TensorialTraits<double>::ValType> func2; 
        _space1_->f(ele, u, v, w, func1);
        _space2_->f(ele, u, v, w, func2);
        for(int j = 0; j < nbFF; j++) {
          NT(j, 0) = 0.0;       N(0, j)  = 0.0;
          NT(j, 1) = 0.0;       N(1, j)  = 0.0;
          NT(j, 2) = func1[j];  N(2, j)  = func2[j];
        }
        NTH.setAll(0.0);
        if ( _materialLaw->getType() == LINEAR_MLAW) {
          HomQtyLinear *MQB = (HomQtyLinear *)(_materialLaw->getMLOnElement(ele) );
          NTH.gemm(NT, *(MQB->getMatrix() ) );
          m.gemm(NTH, N, weight * detJ, 1.);
        }
        if ( _materialLaw->getType() == HMM_LINEAR_MLAW) {
          hmmLinearMaterialLaw *currentML = (hmmLinearMaterialLaw *) (_materialLaw);
          HomQtyLinear *MQB = (HomQtyLinear *)(currentML->getMLOnElement(ele) );
          NTH.gemm(NT, *(MQB->getMatrix() ) );
          m.gemm(NTH, N, weight * detJ, 1.);
        }
        else if ( _materialLaw->getType() == HMM_NONLINEAR_MLAW) {
          Msg::Warning("Done in hmmFormulation.cpp. Computing terms for the time derivative HMM_NONLINEAR_MLAW");
          hmmMicroSolverGetDPNonLinearDouble *currentNLML = (hmmMicroSolverGetDPNonLinearDouble *) (_materialLaw);
          fullMatrix<double> *currentFM = currentNLML->getMaterialLawAtGaussPoint( (*_dTerms), ele, i)->getMatrix();
          NTH.gemm(NT, *(currentFM) );
          m.gemm(NTH, N, weight * detJ, 1.);
          fullVector<double> *currentNLVec = currentNLML->getMaterialLawAtGaussPoint( (*_dTerms), ele, i)->getVector();
          fullVector<double> *currentNLVec_Old = currentNLML->getMaterialLawAtGaussPoint( (*_dTerms), ele, i)->getOldVector();

          //=========================================================================
          currentFM->print("print the tangent matrix for elemental contribution");
          currentNLVec->print("print new vector for elemental contribution");
          currentNLVec_Old->print("print old vector for elemental contribution");
          //=========================================================================

          for (int k = 0; k < nbFF; k++) {
            vv(k) += weight * detJ * ( NT(k, 0) * (*currentNLVec)(0) + NT(k, 1) * (*currentNLVec)(1) + NT(k, 2) * (*currentNLVec)(2) ) ;
            vv_Old(k) -= weight * detJ * ( NT(k, 0) * (*currentNLVec_Old)(0) + NT(k, 1) * (*currentNLVec_Old)(1) + NT(k, 2) * (*currentNLVec_Old)(2) ) ;
          }
        }
      }
      else Msg::Error("The time derivative term is not implemented yet for nodal basis functions. \n");
    }
  } 
}

// void hmmBilinearTerm::get_Dt(MElement *ele, int npts, IntPt *GP, fullMatrix<double> &m, 
//                              fullVector<double> &vv, fullVector<double> &vv_Old, double dTime) const {
//   // BilinearTerms calculated using simple basis functions. Terms of type (\sigma \partial_t \boldsymbol{a}, \boldsymbol{a}^{'})_{\Omega_c}
//   if (this->getType() == SBF) { 
//     hmmScalarLagrangeFunctionSpace *_space1_;
//     hmmScalarLagrangeFunctionSpace *_space2_;
//     if(ele->getParent()) ele = ele->getParent();
//     int nbFF = this->_space1->getNumKeys(ele);
//     double jac[3][3];
//     fullMatrix<double> N(3, nbFF);
//     fullMatrix<double> NTH(nbFF, 3);
//     fullMatrix<double> NT(nbFF, 3);
//     m.resize(nbFF, nbFF);
//     m.setAll(0.0);
//     //=========================================================
//     vv.resize(nbFF);
//     vv.setAll(0.0);    
//     vv_Old.resize(nbFF);
//     vv_Old.setAll(0.0);
//     //=========================================================
//     for(int i = 0; i < npts; i++) {
//       const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
//       const double weight = GP[i].weight; const double detJ = ele->getJacobian(u, v, w, jac);
//       if ( (_space1->getfunctionSpaceType() != Form0) && (_space2->getfunctionSpaceType() != Form0) ) { // FixMe
//         _space1_ = (hmmScalarLagrangeFunctionSpace *) (_space1);
//         _space2_ = (hmmScalarLagrangeFunctionSpace *) (_space2);
//         std::vector<TensorialTraits<double>::ValType> func1;
//         std::vector<TensorialTraits<double>::ValType> func2; 
//         _space1_->f(ele, u, v, w, func1);
//         _space2_->f(ele, u, v, w, func2);
//         for(int j = 0; j < nbFF; j++) {
//           NT(j, 0) = 0.0;       N(0, j)  = 0.0;
//           NT(j, 1) = 0.0;       N(1, j)  = 0.0;
//           NT(j, 2) = func1[j];  N(2, j)  = func2[j];
//         }
//         NTH.setAll(0.0);
//         if ( _materialLaw->getType() == LINEAR_MLAW) {
//           HomQtyLinear *MQB = (HomQtyLinear *)(_materialLaw->getMLOnElement(ele) );
//           NTH.gemm(NT, *(MQB->getMatrix() ) );
//           m.gemm(NTH, N, weight * detJ, 1.);
//         }
//         if ( _materialLaw->getType() == HMM_LINEAR_MLAW) {
//           hmmLinearMaterialLaw *currentML = (hmmLinearMaterialLaw *) (_materialLaw);
//           HomQtyLinear *MQB = (HomQtyLinear *)(currentML->getMLOnElement(ele) );
//           NTH.gemm(NT, *(MQB->getMatrix() ) );
//           m.gemm(NTH, N, weight * detJ, 1.);
//         }
//         else if ( _materialLaw->getType() == HMM_NONLINEAR_MLAW) {
//           Msg::Warning("Done in hmmFormulation.cpp. Computing terms for the time derivative HMM_NONLINEAR_MLAW");
//           hmmMicroSolverGetDPNonLinearDouble *currentNLML = (hmmMicroSolverGetDPNonLinearDouble *) (_materialLaw);
//           fullMatrix<double> *currentFM = currentNLML->getMaterialLawAtGaussPoint( (*_dTerms), ele, i)->getMatrix();
//           NTH.gemm(NT, *(currentFM) );
//           m.gemm(NTH, N, weight * detJ, 1.);
//           fullVector<double> *currentNLVec = currentNLML->getMaterialLawAtGaussPoint( (*_dTerms), ele, i)->getVector();
//           fullVector<double> *currentNLVec_Old = currentNLML->getMaterialLawAtGaussPoint( (*_dTerms), ele, i)->getOldVector();
//           for (int k = 0; k < nbFF; k++) {
//             vv(k) += weight * detJ * ( NT(k, 0) * (*currentNLVec)(0) + NT(k, 1) * (*currentNLVec)(1) + NT(k, 2) * (*currentNLVec)(2) ) ;
//             vv_Old(k) -= weight * detJ * ( NT(k, 0) * (*currentNLVec_Old)(0) + NT(k, 1) * (*currentNLVec_Old)(1) + NT(k, 2) * (*currentNLVec_Old)(2) ) ;
//           }
//         }
//       }
//       else {
//         Msg::Error("The time derivative term is not implemented yet for nodal basis functions. \n");
//       }
//     }
//   }
// }

void hmmBilinearTerm::get(MElement *ele, int npts, IntPt *GP, fullMatrix<double> &m, fullVector<double> &vv) const {
  if (this->getType() == SBF) { // BilinearTerms calculated using simple basis functions
    hmmScalarLagrangeFunctionSpace *_space1_;
    hmmScalarLagrangeFunctionSpace *_space2_;
    if(ele->getParent()) ele = ele->getParent();
    int nbFF = this->_space1->getNumKeys(ele);
    double jac[3][3];
    fullMatrix<double> N(3, nbFF);
    fullMatrix<double> NTH(nbFF, 3);
    fullMatrix<double> NT(nbFF, 3);
    m.resize(nbFF, nbFF);
    m.setAll(0.0);
    //=======================
    vv.resize(nbFF);
    vv.setAll(0.0);
    //=========================
    for(int i = 0; i < npts; i++) {
      const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
      const double weight = GP[i].weight; const double detJ = ele->getJacobian(u, v, w, jac);
      if ( (_space1-> getfunctionSpaceType() != Form0) && (_space2-> getfunctionSpaceType() != Form0) ) { // FixMe
        _space1_ = (hmmScalarLagrangeFunctionSpace *) (_space1);
        _space2_ = (hmmScalarLagrangeFunctionSpace *) (_space2);
        std::vector<TensorialTraits<double>::ValType> func1;
        std::vector<TensorialTraits<double>::ValType> func2; 
        _space1_->f(ele, u, v, w, func1);
        _space2_->f(ele, u, v, w, func2);
        for(int j = 0; j < nbFF; j++) {
          NT(j, 0) = 0.0;       N(0, j)  = 0.0;
          NT(j, 1) = 0.0;       N(1, j)  = 0.0;
          NT(j, 2) = func1[j];  N(2, j)  = func2[j];
        }
        NTH.setAll(0.0);
        if ( _materialLaw->getType() == LINEAR_MLAW) {
          HomQtyLinear *MQB = (HomQtyLinear *)(_materialLaw->getMLOnElement(ele) );
          NTH.gemm(NT, *(MQB->getMatrix() ) );
          m.gemm(NTH, N, weight * detJ, 1.);
        }
        if ( _materialLaw->getType() == HMM_LINEAR_MLAW) {
          hmmLinearMaterialLaw *currentML = (hmmLinearMaterialLaw *) (_materialLaw);
          HomQtyLinear *MQB = (HomQtyLinear *)(currentML->getMLOnElement(ele) );
          NTH.gemm(NT, *(MQB->getMatrix() ) );
          m.gemm(NTH, N, weight * detJ, 1.);
        }



        if ( _materialLaw->getType() == HMM_NONLINEAR_MLAW) {
          hmmMicroSolverGetDPNonLinearDouble *currentNLML = (hmmMicroSolverGetDPNonLinearDouble *) (_materialLaw);
          fullMatrix<double> *currentFM = currentNLML->getMaterialLawAtGaussPoint( (*_dTerms), ele, i)->getMatrix();
          currentFM->print("Test Matrix const law");
          NTH.gemm(NT, *(currentFM) );
          m.print("print m avant calcul.");
          m.gemm(NTH, N, weight * detJ, 1.);
          m.print("print m après calcul.");
          currentFM->print("Test Matrix elemental contrib");
          fullVector<double> *currentNLVec = currentNLML->getMaterialLawAtGaussPoint( (*_dTerms), ele, i)->getVector();
          fullVector<double> *currentNLVec_Old = currentNLML->getMaterialLawAtGaussPoint( (*_dTerms), ele, i)->getOldVector();
          currentNLVec->print("Test Vector const law");
          currentNLVec_Old->print("Test Vector_Old const law");
          Msg::Warning(" The number of dofs for element %d is %d. \n", ele->getNum(), nbFF);
          vv.print("print vv");
          for (int k = 0; k < nbFF; k++) {
            Msg::Warning(" For dof %d, the number of dofs for element %d is %d. \n", k, ele->getNum(), nbFF);
            vv(k) += 0.0;//weight * detJ * ( NT(k, 0) * (*currentNLVec)(0) + NT(k, 1) * (*currentNLVec)(1) + NT(k, 2) * (*currentNLVec)(2) ) ;
            //vv_Old(k) -= 0.0 ;//weight * detJ * ( NT(k, 0) * (*currentNLVec_Old)(0) + NT(k, 1) * (*currentNLVec_Old)(1) + NT(k, 2) * (*currentNLVec_Old)(2) ) ;
          }
          vv.print("Test Vec const contrib");
          //vv_Old.print("Test Vec_Old elemental contrib");
        }




      }
      else Msg::Error("The time derivative term is not implemented yet for nodal basis functions. \n");
    }
  } 
  else if ( (this->getType() == GBF) || (this->getType() == CBF)) {  // BilinearTerms calculated using Grad or Curl
    if(ele->getParent()) ele = ele->getParent();
    int nbFF = this->_space1->getNumKeys(ele);
    double jac[3][3];
    fullMatrix<double> B(3, nbFF);
    fullMatrix<double> BTH(nbFF, 3);
    fullMatrix<double> BT(nbFF, 3);
    m.resize(nbFF, nbFF); m.setAll(0.0);
    vv.resize(nbFF); vv.setAll(0.0);
    for(int i = 0; i < npts; i++) {
      const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
      const double weight = GP[i].weight; const double detJ = ele->getJacobian(u, v, w, jac);
      std::vector<TensorialTraits<double>::GradType> Grads;
      _space1->df(ele, u, v, w, Grads);
      for(int j = 0; j < nbFF; j++) {
        BT(j, 0) = B(0, j) = Grads[j](0);
        BT(j, 1) = B(1, j) = Grads[j](1);
        BT(j, 2) = B(2, j) = Grads[j](2);
      }
      BTH.setAll(0.0);
      if( _materialLaw->getType() == LINEAR_MLAW) {
        HomQtyLinear *currentML = (HomQtyLinear *) (_materialLaw->getMLOnElement(ele) );
        BTH.gemm(BT, *(currentML->getMatrix() ) );
        m.gemm(BTH, B, weight * detJ, 1.);
      }
      else if( _materialLaw->getType() == HMM_LINEAR_MLAW) {
        hmmMicroSolverGetDPLinearDouble *currentML = (hmmMicroSolverGetDPLinearDouble *) (_materialLaw);
        HomQtyLinear *MQB = (HomQtyLinear *)(currentML->getMLOnElement(ele) );
        BTH.gemm(BT, *(MQB->getMatrix() ) );
        m.gemm(BTH, B, weight * detJ, 1.);
      }
      else if ( _materialLaw->getType() == HMM_NONLINEAR_MLAW) {
        hmmMicroSolverGetDPNonLinearDouble *currentNLML = (hmmMicroSolverGetDPNonLinearDouble *) (_materialLaw);
        fullMatrix<double> *currentFM = currentNLML->getMaterialLawAtGaussPoint( (*_dTerms), ele, i)->getMatrix();
        BTH.gemm(BT, *(currentFM) );
        m.gemm(BTH, B, weight * detJ, 1.);
        fullVector<double> *currentNLVec = currentNLML->getMaterialLawAtGaussPoint( (*_dTerms), ele, i)->getVector();
        for (int k = 0; k < nbFF; k++) {
          vv(k) += weight * detJ * ( BT(k, 0) * (*currentNLVec)(0) + BT(k, 1) * (*currentNLVec)(1) + BT(k, 2) * (*currentNLVec)(2) ) ;
        }
      }
      else {
        std::cout << "This function member is not implemented for this Material law PPPPPPPP." << std::endl;
      }
    }
  }
}

hmmLinearTerm::hmmLinearTerm(hmmScalarLagrangeFunctionSpace &space1, 
                             hmmMaterialLawBase &materialLaw, hmmGroupOfDomains &dTerms, GaussQuadrature &integ) {
  _space1      = &space1; 
  _materialLaw = &materialLaw; 
  _dTerms      = &dTerms ;
  _integ       = &integ;
} 
void hmmLinearTerm::get(MElement *ele, int npts, IntPt *GP, fullVector<double> &vv, double freq, double currentTime) const {
  if(ele->getParent()) ele = ele->getParent();
  int nbFF = this->_space1->getNumKeys(ele);
  double jac[3][3];
  fullMatrix<double> B(3, nbFF);
  fullMatrix<double> BT(nbFF, 3);
  vv.resize(nbFF); vv.setAll(0.0);
  for(int i = 0; i < npts; i++) {
    const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
    const double weight = GP[i].weight; const double detJ = ele->getJacobian(u, v, w, jac);
    std::vector<TensorialTraits<double>::GradType> Grads;
    this->_space1->df(ele, u, v, w, Grads);
    for(int j = 0; j < nbFF; j++) {
      BT(j, 0) = B(0, j) = Grads[j](0);
      BT(j, 1) = B(1, j) = Grads[j](1);
      BT(j, 2) = B(2, j) = Grads[j](2);
    }
    if ( _materialLaw->getType() == HMM_NONLINEAR_MLAW) {
      hmmMicroSolverGetDPNonLinearDouble *currentNLML = (hmmMicroSolverGetDPNonLinearDouble *) (this->_materialLaw);
        fullVector<double> *currentNLVec = currentNLML->getMaterialLawAtGaussPoint((*_dTerms), ele, i)->getVector();
      for (int k = 0; k < nbFF; k++) {
        vv(k) += weight * detJ * ( BT(k, 0) * (*currentNLVec)(0) + BT(k, 1) * (*currentNLVec)(1) + BT(k, 2) * (*currentNLVec)(2) ) ;
      }
    }
    else {
      std::cout << "This function member is not implemented for this case." << std::endl;
    }
  }
}

hmmLinearTermSource::hmmLinearTermSource(hmmScalarLagrangeFunctionSpace &space1, fullVector<double> &js, 
                                         hmmGroupOfDomains &dTerms, GaussQuadrature &integ) {
  _space1 = &space1; 
  _js = &js; 
  _dTerms = &dTerms ;
  _integ  = &integ;
  _tag = 0;
}
hmmLinearTermSource::hmmLinearTermSource(hmmScalarLagrangeFunctionSpace &space1, fullVector<double> &js, 
                                         hmmGroupOfDomains &dTerms, GaussQuadrature &integ, int tag) {
  _space1 = &space1; 
  _js     = &js; 
  _dTerms = &dTerms ;
  _integ  = &integ;
  _tag    = tag;
}
void hmmLinearTermSource::get(MElement *ele, int npts, IntPt *GP, fullVector<double> &vv, double freq, double currentTime) const {
  if(ele->getParent()) ele = ele->getParent();
  hmmScalarLagrangeFunctionSpace *_space1_;
  int nbFF = this->_space1->getNumKeys(ele);
  double jac[3][3];
  fullMatrix<double> NT(nbFF, 3);
  vv.resize(nbFF); vv.setAll(0.0);
  for(int i = 0; i < npts; i++) {
    const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
    const double weight = GP[i].weight; const double detJ = ele->getJacobian(u, v, w, jac);
    if (_space1->getfunctionSpaceType() != Form0) {
      //hmmBFPerpendicularEdge &_space1_ = ( hmmBFPerpendicularEdge &) ( *(_space1) );
      _space1_ = ( hmmScalarLagrangeFunctionSpace *) (_space1);
      //std::vector<TensorialTraits<double>::GradType> func;
      std::vector<TensorialTraits<double>::ValType> func;
      _space1_->f(ele, u, v, w, func);
      for(int j = 0; j < nbFF; j++) {
        NT(j, 0) = 0.0;//func[j][0];
        NT(j, 1) = 0.0;//func[j][1];
        NT(j, 2) = func[j];
      }
      //double scalingFactor = sin (2 * pi * freq * currentTime);
      double scalingFactor = (sin (2 * pi * freq * currentTime) + 0.05 * sin (2 * 5 * pi * freq * currentTime) );
      fullVector<double> curJS = *(_js);// * scalingFactor;//->scale(sin(2*pi*freq*currentTime) );
      curJS.scale(scalingFactor);// * scalingFactor;//->scale(sin(2*pi*freq*currentTime) );
      for (int k = 0; k < nbFF; k++) {
        vv(k) += weight * detJ * ( NT(k, 0) * (curJS)(0) + NT(k, 1) * (curJS)(1) + NT(k, 2) * (curJS)(2) ) ;
      }
    }
    else Msg::Error("The time derivative term is not implented yet for nodal basis functions. \n");
  }
}

void hmmFormulation::addBilinearTerm(hmmScalarLagrangeFunctionSpace &space1, hmmMaterialLawBase &materialLaw,  
                                     hmmGroupOfDomains &dTerms, BType whatType, GaussQuadrature &integ) { 
  hmmBilinearTerm *bilinearTerm =  new hmmBilinearTerm(space1, materialLaw, dTerms, integ);
  bilinearTerm->setType(whatType);
  _vectorOfBilinearTerms.push_back(bilinearTerm);
}
void hmmFormulation::addBilinearTerm(hmmScalarLagrangeFunctionSpace &space1, hmmScalarLagrangeFunctionSpace &space2, 
                                     hmmMaterialLawBase &materialLaw,  hmmGroupOfDomains &dTerms, BType whatType, GaussQuadrature &integ) { 
  hmmBilinearTerm *bilinearTerm =  new hmmBilinearTerm(space1, space2, materialLaw, dTerms, integ);
  bilinearTerm->setType(whatType);
  _vectorOfBilinearTerms.push_back(bilinearTerm);
}
void hmmFormulation::addLinearTerm(hmmScalarLagrangeFunctionSpace &space1, 
                                   hmmMaterialLawBase &materialLaw, hmmGroupOfDomains &dTerms, GaussQuadrature &integ) { 
  hmmLinearTerm *linearTerm = new hmmLinearTerm(space1, materialLaw, dTerms, integ);
  _vectorOfLinearTerms.push_back(linearTerm);
}
void hmmFormulation::addLinearTerm(hmmScalarLagrangeFunctionSpace &space1, 
                                   fullVector<double> &js, hmmGroupOfDomains &dTerms, GaussQuadrature &integ) { 
  hmmLinearTermSource *linearTerm = new hmmLinearTermSource(space1, js, dTerms, integ);
  Msg::Info("Affiche js(3) %g \n", js(2));
  _vectorOfLinearTerms.push_back(linearTerm);
}
void hmmFormulation::addLinearTerm(hmmScalarLagrangeFunctionSpace &space1, fullVector<double> &js, 
                                   hmmGroupOfDomains &dTerms, GaussQuadrature &integ, int tag) { 
  hmmLinearTermSource *linearTerm = new hmmLinearTermSource(space1, js, dTerms, integ, tag);
  Msg::Info("Affiche js(3) %g \n", js(2));
  _vectorOfLinearTerms.push_back(linearTerm);
}
