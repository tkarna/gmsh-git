cl1 = 1e+22;

Point(1) = {2.5, 0, 1, cl1};
Point(2) = {1, 1, 0, cl1};
Point(3) = {0, 0, 0, cl1};
Point(4) = {1, -1, 0, cl1};
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 1};
Line(4) = {1, 4};
Line(5) = {4, 2};
Line(6) = {3, 4};

Line Loop(5) = {1, 2, 3};
Plane Surface(5) = {5};

Line Loop(7) = {4, 5, -1};
Plane Surface(7) = {7};

Line Loop(9) = {-3, -4, 6};
Plane Surface(9) = {9};
