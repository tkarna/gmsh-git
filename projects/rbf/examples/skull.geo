Mesh.RemeshParametrization=2; //(0) harmonic (1) conformal (2) Rbf
General.Verbosity=5;
Mesh.Algorithm=5;//(1=MeshAdapt, 2=Automatic, 5=Delaunay, 6=Frontal, 7=bamg, 8=delquad)

Mesh.CharacteristicLengthFactor = 0.5;
//Mesh.ElementOrder = 2;
//Mesh.RecombineAll = 1;

Merge "skullCLASS.msh";

CreateTopology; 

//Compound Line(100) = {5};
Compound Surface(200) = {202} Boundary{{5}};

//Physical Line(100)={100};
Physical Surface(200)={200,201};

