from dgpy import *
from gmshpy import *
from math import *
import sys

# ========================================================================
#Fonction that write the curvature in a file

def write_curvature(face,filename):
  
  param  = SPoint2()
  dirMax = SVector3()
  dirMin = SVector3()
  curvMax = new_doublep()
  curvMin = new_doublep()
  
  output = open(filename,'w')
  output.write("View \"Curvature\" {\n")
  
  
  nbElement = face.getNumMeshElements()
  print "The total nomber of elements is: %d" %nbElement
  
  for ie in range(nbElement): # Loop over the elements ie. over the triangles
    elmt = gf.getMeshElement(ie)
    nv = elmt.getNumVertices() # with this I get the number of vertices PER elements

    output.write("ST(")
    
    for iv in range(nv): # 1st loop over the vertices of ONE element
      vertex = elmt.getVertex(iv) 
      output_str =  "%f,%f,%f" % (vertex.x(),vertex.y(),vertex.z())
      output.write(output_str)

      if(iv < (nv-1)): #==> If this is not the last vertex, write comma after the three coordinates (x,y,z):
        output.write(",")

    output.write("){")
      
    for iv in range(nv): #==> 2nd loop over the vertices in order to write values of curvature at vertices:
      vertex = elmt.getVertex(iv)
      success = reparamMeshVertexOnFace(vertex, face, param)
      face.curvatures(param, dirMax, dirMin, curvMax, curvMin)
      output_str = "%f" % (doublep_value(curvMax)) #% (doublep_value(curvMin))
      output.write(output_str)

      if(iv < (nv-1)): #==> If this is not the last vertex, write comma after the three coordinates (x,y,z):
        output.write(",")

    output.write("};\n")
  
  output.write('};');
  output.close()
  
  
# ========================================================================
#For the moment it's ONLY possible to have the curvature on ONE geometrical surface, so
#NOT POSSIBLE for : sevral geometrical surfaces and for physical surface
#I'm working on it. E.S.


#Utilisation de la fonction "write_curvature":
#nameMesh = "Torus" #surface nr 23
nameMesh = "Sphere"  #surface nr 114
  
model = GModel()
model.load (nameMesh+".geo")
model.load (nameMesh+".msh")

param  = SPoint2()
dirMax = SVector3()
dirMin = SVector3()
curvMax = new_doublep()
curvMin = new_doublep()

gf = model.getFaceByTag(114) #take surface 23 pour Torus, 114 pour sphere
nbVertex = gf.getNumMeshVertices()

write_curvature(gf,'curvature.pos') 
