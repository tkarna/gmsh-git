#!/usr/bin/env python

from gmshpy import *

lc = 1.3; 
GmshSetOption('Mesh', 'CharacteristicLengthFactor', lc)


#g = GModel();
#g.setFactory('OpenCASCADE')
#g.addBlock([-R,-R,-R],[R,R,R]);
#g.addTorus([0,0,0],[1,0,0], 2.0, 1.2);

R = 1.4;
s = .7;
t = 1.1;
myModel = GModel();
myModel.addTorus([0,0,0],[1,0,0], R, 0.6*R);

myTool = GModel();
myTool.addSphere(0, 1.8*R,0,R*t);

myModel.computeBooleanUnion(myTool);

myTool2 = GModel();
myTool2.addBlock([-4*R,-4*R,-4*R],[0,4*R,4*R]);

myModel.computeBooleanDifference(myTool2);

myModel.setAsCurrent();

myModel.mesh(2);
myModel.save("occtorus.msh");
myModel.save("occtorus.brep");

#FlGui.instance();
#FlGui.run();
