Mesh.RemeshParametrization=2; //(0) harmonic (1) conformal (2) Rbf
General.Verbosity=5;
Mesh.Algorithm=5;//(1=MeshAdapt, 2=Automatic, 5=Delaunay, 6=Frontal, 7=bamg, 8=delquad)

Mesh.CharacteristicLengthFactor = 0.4;
//Mesh.ElementOrder = 2;
//Mesh.RecombineAll = 1;

//Merge "CAD.stl";
//Merge "Sphere.stl";
//Merge "boule2.stl"; 
Merge "bouleHOLE.stl"; 
//Merge "overlap.stl"; 
//Merge "hole.stl"; 
//Merge "boule.stl";
//Merge "capotHOLE.stl";
//Merge "capot.stl";
//Merge "teeth.stl";
//Merge "Tjunc2.stl";
//Merge "frogOpen.stl"; //KO with low
//Merge "skull.stl"; //"skull2.stl" for remaining part
//Merge "teethCUT.stl";

CreateTopologyNoHoles; 

//Compound Line(100) = {2};
Compound Surface(200) = {1} ; //Boundary{{2}};

Physical Line(100)={100};
Physical Surface(200)={200};

