Mesh.RemeshParametrization=2; //(0) harmonic (1) conformal (2) Rbf
General.Verbosity=5;
Mesh.Algorithm=5;//(1=MeshAdapt, 2=Automatic, 5=Delaunay, 6=Frontal, 7=bamg, 8=delquad)
Geometry.Tolerance = 1.e-4;
Coherence Geometry;
Coherence Mesh;

Merge "Tjunc4.stl";
CreateTopologyNoHoles;

Compound Line(100) = {1};
Compound Surface(200) = {1};

Physical Line(100)={100};
Physical Surface(200)={200};
