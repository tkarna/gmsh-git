Mesh.Algorithm=1;//(1=MeshAdapt, 2=Automatic, 5=Delaunay, 6=Frontal, 7=bamg, 8=delquad)

Mesh.RemeshParametrization=2; //(0) harmonic (1) conformal (2) rbf
Mesh.RemeshAlgorithm=0; //(0) nosplit (1) automatic (2) split only with metis

Geometry.Tolerance = 1.e-14;
//Mesh.RecombineAll = 1;
Mesh.CharacteristicLengthFactor = 0.22;

Merge "squirrelCUT_cut.msh";
//RefineMesh;
CreateTopologyNoHoles;
 
Compound Line(200) = {1};

Compound Surface(600) = {2} Boundary {{1}};

Physical Surface(200) = {600};
