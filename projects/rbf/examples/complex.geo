Mesh.RemeshParametrization=2; //(0) harmonic (1) conformal (2) Rbf
General.Verbosity=5;
Mesh.Algorithm=5;//(1=MeshAdapt, 2=Automatic, 5=Delaunay, 6=Frontal, 7=bamg, 8=delquad)

//Geometry.Tolerance= 1.e-4;

Mesh.CharacteristicLengthFactor = 0.07;

Point(1) = {0, 1, 0.1, 1.0}; 
Point(2) = {1, 1, 0.1, 1.0}; 
Point(3) = {1, 0.8, 0, 1.0}; 
Point(4) = {1, 0.2, 0, 1.0}; 
Point(5) = {1, 0., 0.2, 1.0}; 
Point(6) = {0, 0., 0.2, 1.0}; 
Point(7) = {0, 0.2, 0, 1.0}; 
Point(8) = {0, 0.8, 0, 1.0}; 
Point(13) = {0.4, 0.8, 0, 1.0}; 
Point(14) = {0.4, 0.2, 0, 1.0}; 
Point(15) = {0.45, 0.2, 0, 1.0}; 
Point(16) = {0.45, 0.8, 0, 1.0}; 
Point(17) = {0.5, 0.5, 0, 1.0}; 
Point(18) = {0.46, 0.3, 0, 1.0}; 
Point(19) = {0.38, 0.7, 0, 1.0}; 

//Point(9) = {1, 0.5, 0, 1.0, 2*lc};
//Point(10) = {1.2, 0.5, +0.3, 1.0, lc};
//Point(11) = {1.2, 1, +0.3, 1.0, lc};

Line(1) = {6, 7};
Line(2) = {7, 8};
Line(3) = {8, 1};
Line(4) = {1, 2};
Line(5) = {2, 3};
Line(6) = {3, 4};
Line(7) = {4, 5};
Line(8) = {5, 6};
Line(9) = {8, 3};
Line(10) = {7, 4};
Line(11) = {15, 16};

//Line(12) = {2, 11};
//Line(13) = {11, 10};
//Line(14) = {10, 9};
//Line(18) = {9, 2};

BSpline(15) = {13, 19, 17, 18, 14};
Line(22) = {8, 13};
Line(23) = {7, 14};
Line(24) = {15, 4};
Line(25) = {16, 3};

Line Loop(16) = {9, -5, -4, -3};
Plane Surface(17) = {16};

Line Loop(20) = {-10, -7, -8, -1};
Plane Surface(21) = {20};

Line Loop(22) = {-2,-22,-15,23};
Plane Surface(23) = {22};

Line Loop(24) = {24,-6,-25,-11};
Plane Surface(25) = {24};

//Line Loop(26) = {18,12,13,14};
//Plane Surface(27) = {26};

//Compound Line(100) = {1,2,3,4,5,6,7,8};
Compound Surface(200) = {17,21,23,25} Boundary{{1,2,3,4,5,6,7,8}};
Line{9,10} In Surface {200};
