Mesh.RemeshParametrization=2; //(0) harmonic (1) conformal (2) Rbf
Mesh.RemeshAlgorithm=0; //(0) nosplit (1) automatic (2) split metis
Mesh.Algorithm = 5;//(1=MeshAdapt, 2=Automatic, 5=Delaunay, 6=Frontal, 7=bamg, 8=delquad)
Mesh.CharacteristicLengthFactor=0.8;

Point(1) = {-0.2, 0.7, 0};
Point(2) = {0.4, 0.7, 1};
Point(3) = {-0.8, 0.7, 0};
Point(4) = {-0.8, 0, 0};
Point(5) = {-0.2, 0, 0};
Point(6) = {0.4, 0, 1};
Line(1) = {2, 1};
Line(2) = {1, 3};
Line(3) = {3, 4};
Line(4) = {4, 5};
Line(5) = {5, 1};
Line(6) = {5, 6};
Line(7) = {6, 2};
Line Loop(8) = {7, 1, -5, 6};
Plane Surface(9) = {8};
Line Loop(10) = {2, 3, 4, 5};
Plane Surface(11) = {10};


//Compound Line(100)={1,2,3,4,6,7};
//Compound Line(100)={5};
Compound Surface(100)={9,11} Boundary{{1,2,3,4,6,7}};
Line{5} In Surface {100};

Physical Surface(100)={100};
