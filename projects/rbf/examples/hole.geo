Mesh.RemeshParametrization=2; //(0) harmonic (1) conformal (2) Rbf
Mesh.RemeshAlgorithm=0; //(0) nosplit (1) automatic (2) split metis
Mesh.Algorithm = 5;//(1=MeshAdapt, 2=Automatic, 5=Delaunay, 6=Frontal, 7=bamg, 8=delquad)
Mesh.CharacteristicLengthFactor=0.8;

lc = 0.08;
lc2 = 2*lc;

Point(1) = {0, 0, 0, lc};
Point(2) = {1, 0, 0, lc};
Point(3) = {1, 1, 0, lc};
Point(4) = {0, 1, 0, lc};
Point(5) = {0.5, 1, 0, lc};
Point(6) = {0.5, 0, 0, lc};
Point(7) = {0.5, 0, 0, lc};
Point(8) = {0.5, 1, 0, lc2};
Point(9) = {0.5, 0.3, 0, lc};
Point(10) = {0.5, 0.6, 0, lc};
Point(11) = {0.5, 0.6, 0, lc};
Point(12) = {0.4, 0.5, 0, lc};
Point(13) = {0.6, 0.45, 0, lc};

Line(1)  = {1, 6};
Line(2)  = {6, 9};
Line(3)  = {9, 12};
Line(4)  = {12, 10};
Line(5)  = {10, 5};
Line(6)  = {5, 4};
Line(7)  = {4, 1};

Line(8)  = {7, 2};
Line(9)  = {2, 3};
Line(10)  = {3, 8};
Line(11)  = {8, 11};
Line(12)  = {11, 13};
Line(13)  = {13, 9};
Line(14)  = {9, 7};

//HOLE
Line Loop(1)={1:7};
Line Loop(2)={8:14};
//Line Loop(2)={-8,-9,-10,-11,-12,-13,-14};

Plane Surface(1)={1};
Plane Surface(2)={2};

Coherence Geometry;
Coherence Mesh;

Compound Line(100)={1,8,9,10,6,7};
Compound Surface(200)={1,2} Boundary{{1,8,9,10,6,7}};
