Mesh.RemeshParametrization=2; //(0) harmonic (1) conformal (2) rbf
Mesh.RemeshAlgorithm=0; //(0) nosplit (1) automatic (2) split only with metis

General.Verbosity=5;
Mesh.Algorithm=6;//(1=MeshAdapt, 2=Automatic, 5=Delaunay, 6=Frontal, 7=bamg, 8=delquad)
//Mesh.RecombineAll = 1;

Mesh.CharacteristicLengthFactor = 0.22;

Merge "duckCLASS.msh";
CreateTopologyNoHoles;
 
//Compound Line(100) = {7};
Compound Line(200) = {22};

Compound Surface(800) = {2} Boundary {{22}};
Compound Surface(900) = {3} Boundary {{22}};

Physical Line(100)={100,200};
Physical Surface(200)={800,900};