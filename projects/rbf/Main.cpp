#include <math.h>
#include <vector>
#include "GModel.h"
#include "GVertex.h"
#include "GFace.h"
#include "GEntity.h"
#include "MVertex.h"
#include "MEdge.h"
#include "MTriangle.h"
#include "fullMatrix.h"
#include "Mline.h"
#include "GEdgeCompound.h"
#include "GFaceCompound.h"
#include "meshGFace.h"
#include "meshGEdge.h" 
#include "rbf.h"
#include <iostream>
#include <fstream>
#include "Gmsh.h"
#include "Os.h"
#include "Context.h"
class rbf;
using namespace std;

static bool orderVertices(const std::vector<GEdge*> &e, std::vector<MVertex*> &l,
                          std::vector<double> &coord){

  l.clear();
  coord.clear();

  std::list<MLine*> temp;
  double tot_length = 0;
  for( int k = 0; k< e.size(); k++){
    for(unsigned int i = 0; i <e[k]->lines.size(); i++ ){
      temp.push_back(e[k]->lines[i]);
      MVertex *v0 = e[k]->lines[i]->getVertex(0);
      MVertex *v1 = e[k]->lines[i]->getVertex(1);    
      const double length = sqrt((v0->x() - v1->x()) * (v0->x() - v1->x()) + 
                                 (v0->y() - v1->y()) * (v0->y() - v1->y()) +
                                 (v0->z() - v1->z()) * (v0->z() - v1->z()));
      tot_length += length;
    }
  }
   
  MVertex *first_v = (*temp.begin())->getVertex(0);
  MVertex *current_v = (*temp.begin())->getVertex(1);

  l.push_back(first_v);
  coord.push_back(0.0);
  temp.erase(temp.begin());

  while(temp.size()){
    bool found = false;
    for(std::list<MLine*>::iterator itl = temp.begin(); itl != temp.end(); ++itl){
      MLine *ll = *itl;
      MVertex *v0 = ll->getVertex(0);
      MVertex *v1 = ll->getVertex(1);
      if(v0 == current_v){
        found = true;
        l.push_back(current_v);
        current_v = v1;
        temp.erase(itl);
        const double length = sqrt((v0->x() - v1->x()) * (v0->x() - v1->x()) + 
                                   (v0->y() - v1->y()) * (v0->y() - v1->y()) +
                                   (v0->z() - v1->z()) * (v0->z() - v1->z()));  
        coord.push_back(coord[coord.size()-1] + length / tot_length);
        break;
      }
      else if(v1 == current_v){
        found = true;
        l.push_back(current_v);
        current_v = v0;
        temp.erase(itl);
        const double length = sqrt((v0->x() - v1->x()) * (v0->x() - v1->x()) + 
                                   (v0->y() - v1->y()) * (v0->y() - v1->y()) +
                                   (v0->z() - v1->z()) * (v0->z() - v1->z()));
        coord.push_back(coord[coord.size()-1] + length / tot_length);
        break;
      }
    }
    if(!found) return false;
  }    
 
  return true;
}

	
void exportScalar ( const char * filename, fullMatrix<double> & x, fullMatrix<double> &fct , int iField) {
  FILE *f = fopen (filename, "w");
  fprintf(f,"View  \" rbf \" {\n");
  
  for (int i=0;i<x.size1();i++){
    fprintf(f,"SP(%g,%g,%g) {%g};\n",x(i,0),x(i,1),x(i,2),fct(i,iField));
  }
  
  fprintf(f,"};\n");
  
  fclose(f);
}

void exportParametrizedMesh(std::list<GFace*> listF, std::map<MVertex*, int> mapV, fullMatrix<double> & UV){

  FILE *f = fopen ("UV.pos", "w");
  fprintf(f,"View  \" uv \" {\n");

  Msg::Info("*** RBF exporting 'UV.pos' ");
  std::list<GFace*>::iterator itF = listF.begin();
  for (; itF != listF.end(); itF++){
    GFace *gf = *itF;
    for (int i=0;i<gf->getNumMeshElements() ; ++i){
      MElement *e = gf->getMeshElement(i);
      MVertex *v0 = e->getVertex(0);
      MVertex *v1 = e->getVertex(1);
      MVertex *v2 = e->getVertex(2);
      std::map<MVertex*, int>::iterator it0 = mapV.find(v0);
      int num0 = it0->second;
      std::map<MVertex*, int>::iterator it1 = mapV.find(v1);
      int num1 = it1->second;
      std::map<MVertex*, int>::iterator it2 = mapV.find(v2);
      int num2 = it2->second;
      fprintf(f,"ST(%22.15E,%22.15E,%22.15E,%22.15E,%22.15E,%22.15E,%22.15E,%22.15E,%22.15E){%22.15E,%22.15E,%22.15E};\n",
	      UV(num0,0), UV(num0,1), 0.0,
	      UV(num1,0), UV(num1,1), 0.0,
	      UV(num2,0), UV(num2,1), 0.0,
	      1., 1., 1.);
    }
  }
  fprintf(f,"};\n");
  
  fclose(f);
}

void exportInitialMeshWithParam(std::list<GFace*> listF,  std::map<MVertex*, int> mapV, fullMatrix<double> & UV){

  FILE *f = fopen ("XYZU.pos", "w");
  fprintf(f,"View  \" xyzu \" {\n");

  FILE *f2 = fopen ("XYZV.pos", "w");
  fprintf(f2,"View  \" xyzv \" {\n");

  Msg::Info("*** RBF exporting 'XYZ-(UV).pos' ");
  std::list<GFace*>::iterator itF = listF.begin();
  for (; itF != listF.end(); itF++){
    GFace *gf = *itF;
    for (int i=0;i<gf->getNumMeshElements() ; ++i){
      MElement *e = gf->getMeshElement(i);
      MVertex *v0 = e->getVertex(0);
      MVertex *v1 = e->getVertex(1);
      MVertex *v2 = e->getVertex(2);
      std::map<MVertex*, int>::iterator it0 = mapV.find(v0);
      int num0 = it0->second;
      std::map<MVertex*, int>::iterator it1 = mapV.find(v1);
      int num1 = it1->second;
      std::map<MVertex*, int>::iterator it2 = mapV.find(v2);
      int num2 = it2->second;
      fprintf(f,"ST(%22.15E,%22.15E,%22.15E,%22.15E,%22.15E,%22.15E,%22.15E,%22.15E,%22.15E){%22.15E,%22.15E,%22.15E};\n",
	      v0->x(), v0->y(), v0->z(),
	      v1->x(), v1->y(), v1->z(),
	      v2->x(), v2->y(), v2->z(),
	      UV(num0,0), UV(num1,0), UV(num2,0));
      fprintf(f2,"ST(%22.15E,%22.15E,%22.15E,%22.15E,%22.15E,%22.15E,%22.15E,%22.15E,%22.15E){%22.15E,%22.15E,%22.15E};\n",
	      v0->x(), v0->y(), v0->z(),
	      v1->x(), v1->y(), v1->z(),
	      v2->x(), v2->y(), v2->z(),
	      UV(num0,1), UV(num1,1), UV(num2,1));
    }
  }
  fprintf(f,"};\n");
  fclose(f);

  fprintf(f2,"};\n");
  fclose(f2);
}


void FuncValsReader ( const char * filename, fullMatrix<double> & fVals) {
  ifstream in(filename);
  int num;
  for (int i=0;i<fVals.size1();i++){
    in >> num;
    in >> fVals(num-1,0);
    in >> fVals(num-1,1);
    in >> fVals(num-1,2);
  }
  in.close();
}

void computeNormals( std::list<GFace*> listF,  std::map<MVertex*, SVector3> &_normals) {
  _normals.clear();
  double J[3][3];
  std::list<GFace*>::iterator itF = listF.begin();
  for (; itF != listF.end(); itF++){
    GFace *gf = *itF;
    for(unsigned int i = 0; i < gf->triangles.size(); ++i){
      MTriangle *t = gf->triangles[i];
      t->getJacobian(0, 0, 0, J);
      SVector3 n (J[2][0],J[2][1],J[2][2]);
      n.normalize();
      for(int j = 0; j < 3; j++){
	std::map<MVertex*, SVector3>::iterator itn = _normals.find(t->getVertex(j));
	if(itn == _normals.end())_normals[t->getVertex(j)] = n;
	else itn->second += n;
      }
    }    
  }
  std::map<MVertex*,SVector3>::iterator itn = _normals.begin();
  for( ; itn != _normals.end() ; ++itn) itn->second.normalize();
}


//---------------------------------------------------------------
rbf* computeParametrizationRBF(GModel *myModel, std::list<GFace*> listF, std::vector<GEdge*> listE,
			       std::map<MVertex*, SPoint3> &rbf_param){

  //Compute allvertices and map index MVertex
  std::vector<MVertex*> allV;
  std::map<MVertex*, int> mapV;
  
  std::vector<MVertex*> ordered;
  std::vector<double> coords;  
  bool success = orderVertices(listE, ordered, coords);
 
  //-> first put boundary nodes
  std::vector<GEntity*> entities;
  for(unsigned int i = 0; i < ordered.size(); i++)
    allV.push_back(ordered[i]);
  //->then the other nodes
  std::list<GFace*>::iterator itF = listF.begin();
  for (; itF != listF.end(); itF++){
    GFace *myFace = *itF;
    for(unsigned int i = 0; i < myFace->triangles.size(); ++i){
      MTriangle *t = myFace->triangles[i];
      for(int j = 0; j < 3; j++){
	MVertex *v = t->getVertex(j);
	std::vector<MVertex*>::iterator it = std::find(allV.begin(), allV.end(), v);
	if (it == allV.end()) allV.push_back(v); 
      }
    }
  }

  //compute coordinates
  int nbNodes = allV.size();
  fullMatrix<double> x(nbNodes,3);  
  SBoundingBox3d bb; 
  int index = 0;
  for(std::vector<MVertex *>::iterator itv = allV.begin(); itv !=allV.end() ; ++itv){
    MVertex *v = *itv;
    x(index,0) = v->x();
    x(index,1) = v->y();
    x(index,2) = v->z();
    SPoint3 pt(v->x(), v->y(), v->z());
    bb +=pt;
    mapV.insert(std::make_pair(v, index));
    index++;
  }
  double boxSize = norm(SVector3(bb.max(), bb.min()));
  
  //compute normals
  fullMatrix<double> myNormal(nbNodes,3);
  std::map<MVertex*, SVector3> normals;
  computeNormals(listF,normals);
  std::map<MVertex*, SVector3>::const_iterator it = normals.begin();
  for (; it != normals.end(); it++){
    SVector3 norm =  it->second;
    std::map<MVertex*, int>::iterator itm = mapV.find(it->first);
    if(itm == mapV.end()) {printf("not found \n"); exit(1);}
    int index = itm->second;
    myNormal(index,0) = norm.x();
    myNormal(index,1) = norm.y();
    myNormal(index,2) = norm.z();
  }

  ////////////////////////
  //////// PARAMETERS/////
  ////////////////////////

  fullMatrix<double> Oper(3*nbNodes,3*nbNodes);

  double dist_min = 1.e6;
  for (int i = 0; i < nbNodes; i++) {
    for (int j = i+1; j < nbNodes; j++) {
      double dist = sqrt((x(i,0)-x(j,0))*(x(i,0)-x(j,0))+(x(i,1)-x(j,1))*(x(i,1)-x(j,1))+(x(i,2)-x(j,2))*(x(i,2)-x(j,2)));
      if (dist<dist_min) dist_min = dist;
    }
  }

  int variableEpsilon = 0;
  int radFunInd = 1; //MQ RBF
  double delta = 0.33*dist_min; //0.005*boxSize;
  double radius= 3*boxSize/sqrt(nbNodes);

  //**************CPM LOW GLOBAL, CPM LOW LOCAL, CPM HIGH GLOBAL***************************
  double epsilon = 0.5*(nbNodes/150.0)/dist_min; //max(2.5, 0.5*(nbNodes/150.0)/dist_min);   

  //**************CPM HIGH LOCAL**************************
  //double epsilon =6;//0.5*(nbNodes/250.0)/dist_min;
  //**************PROJECTION***************************
  //double epsilon =2.5;//0.5*(nbNodes/250.0)/dist_min;

  Msg::Info("*****************************************");
  Msg::Info("*** RBF rad=%g dist_min =%g ", radius, dist_min);
  Msg::Info("*** RBF eps=%g  delta =%g ", epsilon, delta);
  Msg::Info("*****************************************");
   
  // test curvature
  //printf("test curvature \n");
  //fullMatrix<double> myCurvature;
  //rbf* rbfCurv = new rbf(epsilon,variableEpsilon,radFunInd,x);
  //rbfCurv->curvature(radius, x, myNormal, x, myCurvature);
  //exportScalar("curvature.pos", x, myCurvature, 0);
  //exit(1);

  //Defines an RBF interpolant of the level-set that follows the shape of the arbitrary surface
  rbf* rbfTest = new rbf(epsilon,delta, variableEpsilon, radFunInd,x);

  rbfTest->RbfLapSurface_global_CPM_low(x,myNormal,Oper);
  //rbfTest->RbfLapSurface_local_CPM(true, x,radius, myNormal,Oper);
  //rbfTest->RbfLapSurface_global_CPM_high(x, myNormal,Oper);
  //rbfTest->RbfLapSurface_local_CPM(false, x, radius, myNormal,Oper);
  //rbfTest->RbfLapSurface_global_projection(x, myNormal, Oper);
  //rbfTest->RbfLapSurface_local_projection(x,radius, myNormal,Oper);

  //solve harmonic map
  rbfTest->solveHarmonicMap(Oper, mapV, ordered, coords);

  //export
  fullMatrix<double> UV = rbfTest->getUV();
  exportParametrizedMesh(listF, mapV, UV);
  exportInitialMeshWithParam(listF, mapV, UV);

  //fill rbf_param
  std::map<MVertex*, int>::iterator itm = mapV.begin();
  for (; itm != mapV.end(); itm++){
    int index = itm->second;
    SPoint3 coords(UV(index,0), UV(index,1), 0.0);
    rbf_param.insert(std::make_pair(itm->first,coords));
  }

   return rbfTest;
 
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

int main (int argc, char *argv[]){

  GmshInitialize();
  GmshSetOption("General", "Terminal", 1.);

  Msg::Info("*** RBF Reading stl mesh ");
  //GmshMergeFile(argv[1]);
  //GModel *myModel = GModel::current();
  GModel *myModel = new GModel(); //ADD HERE
  myModel->readGEO(argv[1]);

  myModel->createTopologyFromMesh(1); //(1) to ignore interior holes and to remesh them
  myModel->checkMeshCoherence(CTX::instance()->geom.tolerance);
  myModel->removeDuplicateMeshVertices(CTX::instance()->geom.tolerance);
 
  Msg::Info("*** RBF create Compounds");
  int num_gec = 100;
  std::vector<GEdge*> listE;
  listE.push_back(myModel->getEdgeByTag(1)); //ADD LIST HERE
  // listE.push_back(myModel->getEdgeByTag(2)); //ADD LIST HERE
  // listE.push_back(myModel->getEdgeByTag(3)); //ADD LIST HERE
  // listE.push_back(myModel->getEdgeByTag(4)); //ADD LIST HERE
  // listE.push_back(myModel->getEdgeByTag(6)); //ADD LIST HERE
  // listE.push_back(myModel->getEdgeByTag(7)); //ADD LIST HERE
  GEdgeCompound *gec = new GEdgeCompound(myModel, num_gec, listE);
  myModel->add(gec);
  gec->parametrize();
  
  int num_gfc = 200;
  std::list<GFace*> listF;
  std::list<GEdge*> U0;
  listF.push_back(myModel->getFaceByTag(1)); 
  int allowPartition = 0;
  GFaceCompound *gfc = new GFaceCompound(myModel, num_gfc, listF, U0, 0,
  					 GFaceCompound::RBF, allowPartition);
  gfc->meshAttributes.recombine = myModel->getFaceByTag(1)->meshAttributes.recombine;
  myModel->add(gfc);
 
   Msg::Info("*** RBF parametrization");
  std::map<MVertex*, SPoint3> rbf_param;
  double t0 = Cpu();
  rbf* myRBF = computeParametrizationRBF(myModel, listF, listE, rbf_param);

  exit(1);

  gfc->setParam(rbf_param, myRBF);
  gfc->parametrize(); 
  double t1 = Cpu();
  Msg::Info("*** RBF Parametrize (%g s)", t1-t0);

  Msg::Info("*** RBF remeshing");
  double t2 = Cpu();
  myModel->mesh(2);
  double t3 = Cpu();

  GEntity *gef = myModel->getFaceByTag(num_gfc);
  gef->physicals.push_back(myModel->setPhysicalName("remeshed face", gef->dim()));
  GEntity *gee = myModel->getEdgeByTag(num_gec);
  gee->physicals.push_back(myModel->setPhysicalName("remeshed line", gee->dim()));
  myModel->writeMSH("new.msh", 1.0, false, false, false);
 
  Msg::Info("*** RBF Done meshing 'new.msh' (%g  s)", t3-t2);
  GmshFinalize();

  return 0;
}



