#coding-Utf-8-*-
import numpy as npy
import os
import subprocess
import pickle
from polycrystal_utils import genGeo
from ElasticityTensor_utils import GetElasticityTensor,DeCSVtoArray

#Definition of the geometry
h_x=6
h_y=6
h_z=1.

#Material properties
rho   = 2329.
Ex=130000.;	Ey=130000.;	Ez=130000.;
nu    = 0.3
mu    = 75500
Vxy=0.28;	Vxz=0.28;	Vyz=0.28;
MUxy=mu;MUxz=mu;MUyz=mu;

material=[rho,Ex,Ey,Ez,Vxy,Vxz,Vyz,MUxy,MUxz,MUyz]

#Grain & mesh properties
GrainSize=3.
MeshSize_gmsh=1.2
nbr_layer=1

#Monte-Carlo properties
Nsim=50;
flag_bounds=1;
BC='KUBC';

#Definition of the .geo file
Ngrain=genGeo(h_x,h_y,h_z,GrainSize,MeshSize_gmsh,nbr_layer)
#Ngrain=4
#Construction of the .msh file from the .geo file
subprocess.call(['gmsh','-3','-order', '2','polycrystal.geo'])

#Definition of the 3 Euler angles of each grain

angle_disp=-1; #==1 : load angles from data file / ==0 : rdm angles +  creation of data / ==-1 : rdm angles
liste_C=[];
liste_Cl=[];
liste_Cu=[];

new_it=1

if new_it==1:

	liste_C_save=[]
	liste_Cu_save=[]
	liste_Cl_save=[]

	with open('data/Result_save.dat','wb') as Result_MC:
		mon_pickler=pickle.Pickler(Result_MC);
		mon_pickler.dump(liste_C_save);
		mon_pickler.dump(liste_Cu_save);
		mon_pickler.dump(liste_Cl_save);


	del liste_C_save
	del liste_Cu_save
	del liste_Cl_save	

for i in range(Nsim):

	#Rotation de type ZXZ

	#Angles from file
	if angle_disp==1:
		with open('data/data_angle.dat','rb') as data:
			mon_depickler=pickle.Unpickler(data)
			angle=mon_depickler.load()

	#random angles + save
	elif angle_disp==0:
		angle=[];angle.append([]);angle.append([]);angle.append([]);
		for k in range(Ngrain):
			angle[0].append(npy.random.rand()*45)
			angle[1].append(npy.random.rand()*45)
			angle[2].append(npy.random.rand()*45)

		with open('data/data_angle.dat','wb') as data:
			mon_pickler = pickle.Pickler(data)
			mon_pickler.dump(angle)

	#random angles
	else:
		angle=[];angle.append([]);angle.append([]);angle.append([]);
		for k in range(Ngrain):
			angle[0].append(npy.random.rand()*360)
			angle[1].append(npy.random.rand()*360)
			angle[2].append(npy.random.rand()*360)

	#angle=[[0.,45.,45.,0.],[0.,0.,45.,0.],[0.,0.,45.,45.]];

	if flag_bounds==1:
		GetElasticityTensor(h_x,h_y,h_z,Ngrain,'KUBC',material,angle,i+Nsim)

	print "=======>>>>>>>ITERATION num :"
	print "=======>>>>>>>ITERATION num :"
	print "=======>>>>>>>ITERATION num :"
	print "=======>>>>>>>ITERATION num :"
	print "=======>>>>>>>ITERATION num :"
	print "=======>>>>>>>ITERATION num :"
	print "=======>>>>>>>ITERATION num :"
	print "=======>>>>>>>ITERATION num :"
	print "=======>>>>>>>ITERATION num :"
	print "=======>>>>>>>ITERATION num :"
	print "=======>>>>>>>ITERATION num :"
	print "=======>>>>>>>ITERATION num :"
	print i	

	if flag_bounds==1:
		GetElasticityTensor(h_x,h_y,h_z,Ngrain,'MIXEDBC',material,angle,i)
	else:
		GetElasticityTensor(h_x,h_y,h_z,Ngrain,BC,material,angle,i,)

	if flag_bounds==1:
		GetElasticityTensor(h_x,h_y,h_z,Ngrain,'SUBC',material,angle,i+2*Nsim)

	C=DeCSVtoArray(i);

	if flag_bounds==1:
		Cl=DeCSVtoArray(i+Nsim*2);
		Cu=DeCSVtoArray(i+Nsim);

	with open('data/Result_save.dat','rb') as data:
		mon_depickler=pickle.Unpickler(data)
		liste_C=mon_depickler.load()
		liste_Cu=mon_depickler.load()
		liste_Cl=mon_depickler.load()

	liste_C+=[C];
	if flag_bounds==1:
		liste_Cu+=[Cu];
		liste_Cl+=[Cl];
	else:
		liste_Cu+=[];
		liste_Cl+=[];
		
	with open('data/Result_save.dat','wb') as Result_MC:
		mon_pickler=pickle.Pickler(Result_MC);
		mon_pickler.dump(liste_C);
		mon_pickler.dump(liste_Cu);
		mon_pickler.dump(liste_Cl);

	os.system("rm E_*")
	del liste_C
	del liste_Cu
	del liste_Cl
	del C
	del Cu	
	del Cl
	del mon_pickler




