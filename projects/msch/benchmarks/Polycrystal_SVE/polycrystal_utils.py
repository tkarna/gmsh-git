#coding-Utf-8-*-
import numpy as npy
import scipy as spy
import matplotlib
import os
import sys
#import random
from scipy.spatial import Voronoi

def voronoi_VP(x):

	for i in range(len(x)):
		x[i]=x[i].squeeze();

	x=npy.array(x);

	vor=Voronoi(x);
	V=vor.vertices;
	C=vor.regions;

	V=npy.vstack((npy.array([float('Inf'),float('Inf')]),V));
	
	for i in range(len(C)):
		if C[i]==[]:
			i_sv=i;
		else:
			for j in range(len(C[i])):
				C[i][j]=C[i][j]+1;
	del C[i_sv];
	del i_sv;
	
	return V,C

def sign(x):

	if x==0:

		return 0

	elif abs(x)!=x:

		return -1

	else:
		return 1
	

def genGeo(h_x,h_y,h_z,GrainSize,MeshSize_gmsh,nbr_layer):

	if h_x<0:
		h_x=abs(h_x)
		h_y=abs(h_y)
		h_z=abs(h_z)
		alpha_unit=0.1

	else:
		alpha_unit=1.

	c_l=MeshSize_gmsh;

	perturbation_max = 0.25; # Perturbation in term of unit of grid subdivision, do NOT put it at 0!
	dist2bound=  0; # h/(2*(n-1)); # Distance min between the boundary and the seeds
	mesh_size = 0.2; # Check manually that it's not bigger than the grid subdivisions! (h/(n-1)) and bigger than 0!
	factor_mesh = 1.;
	FlagCheckLength = 1;

	# Automated second parameters and initialization

	n_x = npy.ceil(h_x/GrainSize)+2.; # Number of horizontal grid points (along x) (the +2 ensures that outside seeds perturbed can be in)
	pertu_x = perturbation_max * GrainSize;
	pertu_y = perturbation_max * GrainSize * 2./npy.sqrt(3.);
	Voronoi_polygone_length = 0;
	tolerance_1 = mesh_size * 1.e-12;
	tolerance_2 = mesh_size * 1.e-5;
	n_y = 1.+npy.ceil( (n_x-1.) * h_y/h_x * 2./npy.sqrt(3.) ) +2.; #(the +2 ensures that outside seeds perturbed can be in)
	numberOfGrains = npy.floor(h_x*h_y/pow(GrainSize,2));
	numberOfGrainsTemp = 0;

	############ DO NOT CHANGE THESE ############
	FlagCheckGrainSize = 0;
	FlagEraseSeeds = 0; 
	#############################################

	if ( n_y%2 == 0 ): # n_y is even
		nb_seeds = n_x*n_y - n_y/2.;
	else: # n_y is odd
		nb_seeds = n_x*n_y - (n_y-1.)/2.;

	if (FlagEraseSeeds == 1.):
		proba_nothing = (nb_seeds-numberOfGrains)/(nb_seeds); # Probability to have no point on the grid
	else:
		proba_nothing = 0.;

	# Condition to keep the Voronoi diagram based on having polygons sides > mesh size
	while ((Voronoi_polygone_length < mesh_size) or (numberOfGrainsTemp != numberOfGrains)):

		C_new=None
    		C_new_temp_1=None
    		C_new_temp_2=None
    
    		# Create an initial grid with perturbation and out-of-the-box points (the ones we will skip)
    		grid = npy.zeros((n_x*n_y,2));
    		for i in range(int(n_x)):
        		for j in range(int(n_y)):
          			if (npy.random.rand() > proba_nothing):      
            				grid[ (i)*n_y+j , 0 ] = -h_x/2 + (i-1)*GrainSize + (2*npy.random.rand()-1)*pertu_x + ((j+1)%2)*GrainSize/2.;
            				grid[ (i)*n_y+j , 1 ] = -h_y/2 + (j-1)*GrainSize + (2*npy.random.rand()-1)*pertu_y;
          			else:
            				grid[ (i)*n_y+j , 0 ] = 2*max(h_x,h_y); # Trick to get rid of them at the next step

		
		#Create the seeds for the Voronoi diagram by getting rid of the out-of-the-box points and those too close to the boundary
    		#k=0;
    		#seeds_b = grid; #Beware : above k, data that should not be used
    		#for i in range(int(n_x*n_y)):
        	#	if ( ( abs(grid[i,0]) <= (h_x/2-dist2bound) )  &  ( abs(grid[i,1]) <= (h_y/2-dist2bound) ) ):           			
		#		seeds_b[k,:] = grid[i,:]; 
          	#		k = k+1;

		#seeds=npy.zeros((k,2));
		#for i in range(k):
		#	seeds[i,:]=seeds_b[i,:];
		#del seeds_b;

		k=0;
		seeds = []
		for i in range(int(n_x*n_y)):
			if ((abs(grid[i,0]) <= (h_x/2-dist2bound) )  and  ( abs(grid[i,1]) <= (h_y/2-dist2bound) ) ):
				seeds.append(grid[i,:])
				k=k+1;

		if k==0:
			seeds.append([0.,0.])
			k=1;
			

		seeds=npy.array(seeds);

		#data = open("grid_vor.dat", "r");
		#Data = data.read();
		#NN=12
		#k=12
		#seeds=npy.zeros((NN,2.));
		#i=0;
		#j=0;
		#index=0;
		#new_it=1;
		#while i<NN:
		#	if new_it==1:
		#		yo="";
		#		yo+=Data[index]
		#		new_it=0;
		#	elif Data[index] != "a":
		#		yo+=Data[index]
		#	elif j==0:
		#		seeds[i,0]=float(yo);
		#		j=1;
		#		new_it=1;
		#	else:
		#		seeds[i,1]=float(yo);
		#		i=i+1;
		#		j=0;
		#		new_it=1;
		#	index+=1;
		#		
		#data.close()


		seeds_0=npy.array([seeds[:,0]]).transpose()
		seeds_1=npy.array([seeds[:,1]]).transpose()

		# Create the 8 mirrors of the seeds (in order to have boundaries created)
		seeds_mirror_1 = npy.hstack([-seeds_0+h_x*npy.ones((k,1)),seeds_1])
		seeds_mirror_2 = npy.hstack([-seeds_0+h_x*npy.ones((k,1)),-seeds_1+h_y*npy.ones((k,1))])
		seeds_mirror_3 = npy.hstack([-seeds_0+h_x*npy.ones((k,1)),-seeds_1-h_y*npy.ones((k,1))])
		seeds_mirror_4 = npy.hstack([-seeds_0-h_x*npy.ones((k,1)),seeds_1])
		seeds_mirror_5 = npy.hstack([-seeds_0-h_x*npy.ones((k,1)),-seeds_1+h_y*npy.ones((k,1))])
		seeds_mirror_6 = npy.hstack([-seeds_0-h_x*npy.ones((k,1)),-seeds_1-h_y*npy.ones((k,1))])
		seeds_mirror_7 = npy.hstack([seeds_0,-seeds_1+h_y*npy.ones((k,1))])
		seeds_mirror_8 = npy.hstack([seeds_0,-seeds_1-h_y*npy.ones((k,1))])

		del seeds_0
		del seeds_1

    		#Assemble all the seeds

		#seeds_total de type : bcp X 2 ! le construire en plusieurs ligne, cf seeds_mirror
    		#seeds_total = [ seeds , seeds_mirror_1 , seeds_mirror_2 , seeds_mirror_3 , seeds_mirror_4 , seeds_mirror_5 , seeds_mirror_6 , seeds_mirror_7 , seeds_mirror_8 ];		
		seeds_total = npy.concatenate((seeds,seeds_mirror_1,seeds_mirror_2,seeds_mirror_3,seeds_mirror_4,seeds_mirror_5,seeds_mirror_6,seeds_mirror_7,seeds_mirror_8))		

 		#voronoi(seeds_total(:,1),seeds_total(:,2));
    
    		#Create the associated Voronoi diagram
    		V,C = voronoi_VP(seeds_total);
    
    		#Get rid of double nodes by finding them and the ones out-of-the-box
    		found_flag = 0;
    		counter = 0;
    		V_correspondance = npy.zeros((V.shape[0],1));

		for i in range(V.shape[0]):
        		found_flag = 0;
        		if ( ( abs(V[i,0]) > (h_x/2 + tolerance_2) ) or  ( abs(V[i,1]) > (h_y/2 + tolerance_2) )):  # out-of-the-box nodes            			
				found_flag = 1
        		else: 
            			if (counter > 0.5): # Condition for the definition of "size(V_new,1)"
                			for j in range(V_new.shape[0]):

						test_norm=npy.sqrt(npy.dot(V[i,:]-V_new[j,:],V[i,:]-V_new[j,:]))
                    				if ( test_norm <  tolerance_2 ): # already found
                        				found_flag = 2
                        				V_correspondance[i] =  j; # give the found node the same new number as the already existing one
                        				break;
        
        		if ( found_flag < 0.5 ): # Adding new nodes in-the-box
            			if ( counter < 0.5): # First found one
                			V_new = npy.array([V[i,:]]); 
                			counter = 1;
            			else:
					V_new=npy.vstack((V_new,V[i,:]));
                			counter = counter + 1;
            			V_correspondance[i] =  counter;


    		# Get rif of out-of-the-box polygons and find the polygons with twice or more the same node
    		
		C_new_temp_2=[];
		for i in range(len(C)):
        		C_new_temp_1=[];
        		for j in range(len(C[i])):
            			if ( V_correspondance[ C[i][j] ] > 0.5 ): # found one polygone with at least one node in-the-box
                			C_new_temp_1.append(V_correspondance[C[i][j]]);
    
        		if (len(C_new_temp_1) > 2.5): # Get rid of two-points "polygons"
            			C_new_temp_2.append(C_new_temp_1);

		# Get rid of "flat" elements
		C_new=[];
    		for i in range(len(C_new_temp_2)):
        
        		# Get rid of double nodes
        		C_new_temp_1=[];
        		C_new_temp_1.append(C_new_temp_2[i][0]);
        		for j in range(1,len(C_new_temp_2[i])):
            			found_flag = 0;
            			for k in range(len(C_new_temp_1)):
                			if ( abs(C_new_temp_1[k] - C_new_temp_2[i][j]) < tolerance_2 ):
                    				found_flag = 1;
                    				break;
            			if ( found_flag < 0.5 ):
                			C_new_temp_1.append(C_new_temp_2[i][j]);
        
        		# Get rid of two-points polygones left
        		if ( len(C_new_temp_1) > 2.5 ):
            			C_new.append(C_new_temp_1);

       		count_adj_cell = npy.zeros((V_new.shape[0],1));
    		for i in range(len(C_new)):
       			for j in range(len(C_new[i])):
	   			C_now = int(C_new[i][j])-1;
	   			count_adj_cell[C_now]=count_adj_cell[C_now]+1;


		increment = 0.001;
    		Voronoi_polygone_length = max(h_x,h_y);
    		for ii in range (1,1001):

      			# Calculate the smallest side       
      			correction = 0;
      			for i in range(len(C_new)):
  				for j in range(len(C_new[i])):
      					C_now = int(C_new[i][j])-1;
      					if (j==0):
	  					C_previous = int(C_new[i][len(C_new[i])-1])-1;
      					else:
	  					C_previous = int(C_new[i][j-1])-1;
      
      					Voronoi_polygone_length_temp = npy.sqrt( (V_new[C_now,0]-V_new[C_previous,0])**2 + (V_new[C_now,1]-V_new[C_previous,1])**2 );

          				#Added by Vincent Peron
	      				if ( Voronoi_polygone_length_temp < mesh_size ):
						vec=npy.zeros((2.));		 
		 				vec[0] = V_new[C_now,0]-V_new[C_previous,0];
		 				vec[1] = V_new[C_now,1]-V_new[C_previous,1];
               					norm = npy.sqrt(vec[0]**2+vec[1]**2);
               					vec[0] /= norm;
	 					vec[1] /= norm;
                 				if (((count_adj_cell[C_now] == 3) and (count_adj_cell[C_previous] == 3)) or ((count_adj_cell[C_now] == 2) and (count_adj_cell[C_previous] == 2))):
		   					V_new[C_now,0] += vec[0]*increment;
		   					V_new[C_now,1] += vec[1]*increment;
		   					V_new[C_previous,0] -= vec[0]*increment;
		   					V_new[C_previous,1] -= vec[1]*increment;
		 
                 				if (((count_adj_cell[C_now] == 3) and (count_adj_cell[C_previous]== 2)) or ((count_adj_cell[C_now] == 2) and (count_adj_cell[C_previous] == 1))):
		   					V_new[C_now,0] += vec[0]*increment;
							V_new[C_now,1] += vec[1]*increment;
	
                 				if (((count_adj_cell[C_now] == 2) and (count_adj_cell[C_previous] == 3)) or ((count_adj_cell[C_now] == 1) and (count_adj_cell[C_previous] == 2))):
		   					V_new[C_previous,0] -= vec[0]*increment;
		   					V_new[C_previous,1] -= vec[1]*increment;

		 				correction+=1;


      			if (correction == 0):
				break;


    		if (FlagCheckGrainSize == 0):
        		numberOfGrainsTemp = numberOfGrains;
    		else:
        		numberOfGrainsTemp = size(C_new,2);

	#End of calculs part
	#Remaining step : writing the file
	fid = open("polycrystal.geo", "w")

	fid.write("%s\n" % ('Geometry.AutoCoherence=0;'));
	fid.write("%s\n" % ('cl = {};'.format(c_l)));

	

	llPerPoint=npy.zeros((V_new.shape[0],3));

	for i in range(len(C_new)):
		for j in range(len(C_new[i])):

			C_now = int(C_new[i][j])-1;
			if j==(len(C_new[i])-1):
				C_next = int(C_new[i][0])-1;
				C_prev = int(C_new[i][j-1])-1;
			elif j==1:
				C_next = int(C_new[i][1])-1;
				C_prev = int(C_new[i][len(C_new[i])-1])-1;
			else:
				C_next = int(C_new[i][j+1])-1;
				C_prev = int(C_new[i][j-1])-1;

			dx_1 = V_new[C_now,0] - V_new[C_prev,0];
			dy_1 = V_new[C_now,1] - V_new[C_prev,1];

			dx_2 = V_new[C_now,0] - V_new[C_next,0];
			dy_2 = V_new[C_now,1] - V_new[C_next,1];

			dr_1 = npy.sqrt(dx_1**2 + dy_1**2);
			dr_2 = npy.sqrt(dx_2**2 + dy_2**2);

			llPerPoint[C_now,0]=npy.sqrt(dx_1**2 + dy_1**2) + npy.sqrt(dx_2**2 + dy_2**2);
			llPerPoint[C_now,1]=llPerPoint[C_now,1]+1;
		
			if llPerPoint[C_now,2]==0:

				llPerPoint[C_now,2]=min(dr_1,dr_2);
			else:

				llPerPoint[C_now,2]=min(llPerPoint[C_now,2],min(dr_1,dr_2));
	
	for i in range(V_new.shape[0]):
		#fid.write("%s %i %s %f %s %f %s %f %s\n" %('Point(', i,') = {',abs(V_new[i,0]+h_x/2),', ',abs(V_new[i,1]+h_y/2), ', 0, cl*',llPerPoint[i,3],'};'));		
		fid.write("%s %i %s %f %s %f %s %f %s\n" %('Point(', i+1,') = {',(abs(V_new[i,0]+h_x/2))*alpha_unit,', ',(abs(V_new[i,1]+h_y/2))*alpha_unit, ', 0, cl*',1,'};'));
	
	TabBound=[]
	count = 1
	for i in range(len(C_new)):
    		for j in range(len(C_new[i])):
        		C_now = int(C_new[i][j])-1;
        		if ( j == len(C_new[i])-1):
            			C_next = int(C_new[i][0])-1;
        		else:
            			C_next = int(C_new[i][j+1])-1;
        		
			fid.write("%s %i %s %i %s %i %s\n" %('Line(', count, ') = {',C_now+1,', ',C_next+1,'};'));

			x_1=abs(V_new[C_now,0]+h_x/2)
			y_1=abs(V_new[C_now,1]+h_y/2)
			x_2=abs(V_new[C_next,0]+h_x/2)
			y_2=abs(V_new[C_next,1]+h_y/2)

			dx_1 = x_1 - x_2;
			dy_1 = y_1 - y_2;
			dr_1 = npy.sqrt(dx_1**2 + dy_1**2);

			nbr = npy.ceil(dr_1/1); 
			#fid.write("%s%i%s%i%s\n"%('Transfinite Line{',count,'} = ',nbr,';'));

			atest=V_new[C_now,:]-V_new[C_next,:];
			toltol=10**(-5);
			
			if abs(atest[0])<toltol and sign(V_new[C_now,0])==1 and abs(V_new[C_now,0]-h_x/2)<toltol:

				TabBound.append([count,0,0,0]);

			elif abs(atest[0])<toltol and sign(V_new[C_now,0])==-1 and abs(V_new[C_now,0]+h_x/2)<toltol:

				TabBound.append([0,count,0,0]);

			elif abs(atest[1])<toltol and sign(V_new[C_now,1])==1 and abs(V_new[C_now,1]-h_y/2)<toltol:

				TabBound.append([0,0,count,0]);

			elif abs(atest[1])<toltol and sign(V_new[C_now,1])==-1 and abs(V_new[C_now,1]+h_y/2)<toltol:

				TabBound.append([0,0,0,count]);
	
			count = count +1;

	count = 1
	for i in range(len(C_new)):
    		fid.write("%s %i %s" %('Line Loop(', i+1, ') = {'));
    		for j in range(len(C_new[i])):
        		C_now = C_new[i][j];
        		if ( j == len(C_new[i])-1 ):
            			C_next = C_new[i][1]-1;
        		else:
            			C_next = C_new[i][j+1]-1;

        		if ( j == len(C_new[i])-1  ):
            			fid.write("%i" %(count));
        		else:
            			fid.write("%i %s" %(count,', '));
			count = count +1;
    		fid.write("%s\n" %('};'));
    		fid.write("%s %i %s %i %s\n" %('Plane Surface(', i+1, ') = {', i+1, '};'));
		
	for i in range(len(C_new)):
		fid.write("%s %i %s %i %s\n" %('Physical Volume(',i+11,') ={',i+1,'};'));

	for i in range(len(C_new)):
		#fprintf(fid, '%s%i%s%f%s%i%s\n','out',i,'[] = Extrude{0,0,',h_z,'}{ Surface{',i,'};Layers{ {2}, {1} };Recombine;};');
		fid.write("%s%i%s%f%s%i%s%i%s\n" %('out',i+1,'[] = Extrude{0,0,',h_z*alpha_unit,'}{ Surface{',i+1,'};Layers{ {',nbr_layer,'}, {1} };};'));


	fid.write("%s %i %s" %('Physical Surface(',130,') = {'));

	for i in range(len(C_new)-1):
		fid.write("%s%i%s%i%s" %('out', i+1,'[',0,'],'));

	fid.write("%s%i%s%i%s\n" %('out', i+2,'[',0,']};'));

	fid.write("%s %i %s" %('Physical Surface(',103,') = {'));

	for i in range(len(C_new)-1):
		fid.write("%s%i%s%i%s" %('out', i+1,'[',1,'],'));
	
	fid.write("%s%i%s%i%s\n" %('out', i+2,'[',1,']};'));

	NumberingBNDR=[110,101,120,102];
	TabBound=npy.array(TabBound);
	for i in range(4):
		index=0;
		for j in range(TabBound.shape[0]):

			if (TabBound[j,i]!=0):
				index+=1;
				fid.write("%s%i%i%s%f%s%i%s\n" %('out',i+2,index,'[]=Extrude{0,0,',h_z*alpha_unit,'}{ Line{',TabBound[j,i],'};};'));
			
		fid.write("%s%i%s" %('Physical Surface(',NumberingBNDR[i],') = {'));
		for k in range(index-1):
			fid.write("%s%i%i%s" %('out',i+2,k+1,'[1],'));
	
		fid.write("%s%i%i%s\n"  %('out',i+2,index,'[1]};'));
	fid.write("%s\n" %('Coherence;'));
	fid.close()

	return len(C_new)













