#coding-Utf-8-*-
import numpy as npy
import scipy as spy
import matplotlib
from gmshpy import *
from msch import*
import os
import sys

def GetElasticityTensor(h_x,h_y,h_z,Ngrain,BC,material,angle,number):
	
	#h_x,h_y,h_z : sides of the volume element
	#Ngrain : number of grains of the voronoi
	#BC : str defining the boundary conditions : 'SUBC', 'PERIODIC', 'KUBC'
	#material : list containing the material properties
	#angle : 3 angles defining the direction of the material for each grain

	# geometry
	meshfile="polycrystal.msh" # name of mesh file

	if BC=='save_stiff' or BC=='save_mass':
		meshfile="beam.msh"

	# solver
	sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
	soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
	nstep = 1   # number of step (used only if soltype=1)
	ftime =1.   # Final time (used only if soltype=1)
	tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
	nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
	fullDg = 0 #O = CG, 1 = DG
	space1 = 0 # function space (Lagrange=0)
	system = 0 # Displacement elimination =0 Multiplier elimination = 1 Displacement+ multiplier = 2
	control = 0 # load control = 0 arc length control euler = 1
	
	if BC=='save_stiff':
		soltype=1
	elif BC=='save_mass':
		soltype=4

	# creation of Solver
	mysolver = nonLinearMechSolver(1000)
	mysolver.loadModel(meshfile)

	#Deterministic variables
	rho   = material[0]
	Ex=material[1];	Ey=material[2];	Ez=material[3];
	Vxy=material[4];Vxz=material[5];Vyz=material[6];
	MUxy=material[7];MUxz=material[8];MUyz=material[9];

	liste_nfield=[]
	liste_myfield=[]
	liste_law=[]

	liste_alpha=angle[0]
	liste_beta=angle[1]
	liste_gamma=angle[2]

	#Grain dependant
	lawnum=1
	while lawnum<=Ngrain:

		liste_nfield.append(lawnum+10) # number of the field (physical number osrc/mat/interface/matrix.cf surface)
		liste_myfield.append(FSDomain(1000,liste_nfield[lawnum-1],lawnum,3))
		liste_law.append(AnisotropicFSMaterialLaw(lawnum,rho,Ex,Ey,Ez,Vxy,Vxz,Vyz,MUxy,MUxz,MUyz,liste_alpha[lawnum-1],liste_beta[lawnum-1],liste_gamma[lawnum-1]))

		mysolver.addDomain(liste_myfield[lawnum-1])
		mysolver.addMaterialLaw(liste_law[lawnum-1])
		
		lawnum+=1;

	mysolver.Scheme(soltype)
	mysolver.Solver(sol)

	mysolver.snlData(nstep,ftime,tol)
	mysolver.stepBetweenArchiving(nstepArch)
	mysolver.setMessageView(1)
	mysolver.setSystemType(system)
	#mysolver.setControlType(control)
        mysolver.setMicroProblemIndentification(0, number);

	#boundary condition
	if BC=="KUBC":

		mysolver.setPeriodicity(h_x,0,0,"x")
		mysolver.setPeriodicity(0,h_y,0,"y")
		mysolver.setPeriodicity(0,0,h_z,"z")

		#linear displacement BC
		mysolver.addLinearDisplacementBC(1000,101,102,103,110,120,130)

	elif BC=="SUBC":

		mysolver.setPeriodicity(h_x,0,0,"x")
		mysolver.setPeriodicity(0,h_y,0,"y")
		mysolver.setPeriodicity(0,0,h_z,"z")

		#minimal kinematical BC
		mysolver.addMinimalKinematicBC(1000,110,120,130,101,102,103)

	elif BC=="PERIODIC":

		mysolver.setPeriodicity(h_x,0,0,"x")
		mysolver.setPeriodicity(0,h_y,0,"y")
		mysolver.setPeriodicity(0,0,h_z,"z")

		# periodiodic BC
		method = 1	# Periodic mesh = 0, Langrange interpolation = 1, Cubic spline interpolation =2 
		degree = 9	# Ordesrc/mat/interface/matrix.cr used for polynomial interpolation 
		addvertex = 0 # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex 
		mysolver.addPeriodicBC(1000,110,120,130,101,102,103) #Periodic boundary condition
		mysolver.setPeriodicBCOptions(method, degree,addvertex)

	elif BC=="MIXEDBC":

		mysolver.setPeriodicity(h_x,0,0,"x")
		mysolver.setPeriodicity(0,h_y,0,"y")
		mysolver.setPeriodicity(0,0,h_z,"z")

		mysolver.addMixedBC(1000,110,120,130,101,102,103) #Periodic boundary condition
		mysolver.addKinematicPhysical(120)
		mysolver.addKinematicPhysical(102)

		mysolver.addStaticPhysical(110)
		mysolver.addStaticPhysical(101)

		mysolver.addStaticPhysical(130)
		mysolver.addStaticPhysical(103)


	elif BC=="XTRACT":

		mysolver.displacementBC("Face",101,0,0.) 
		mysolver.displacementBC("Face",110,0,0.6)
		#mysolver.displacementBC("Line",102,0,0.)
		mysolver.displacementBC("Face",102,1,0.)
		mysolver.displacementBC("Face",130,2,0.)
		#mysolver.constraintBC("Face",120,1)
		#mysolver.constraintBC("Face",103,2)
		#mysolver.setMicroSolverFlag(1)

	elif BC=='save_stiff' or BC=='save_mass':
		
		mysolver.displacementBC("Face",101,0,0.) 
		mysolver.displacementBC("Face",101,1,0.) 
		mysolver.displacementBC("Face",101,2,0.) 

		mysolver.forceBC("Face",110,0,10.)

	else:

		print "WARNING - unclear BC. KUBC used by default."
		mysolver.addLinearDisplacementBC(1000,101,102,103,110,120,130)

	# kinematic macro-variable
	mysolver.setOrder(1) # Homogenization order

	# Deformation gradient
	#mysolver.setDeformationGradient(1.1,0.2,0.0,0.2,1.3,0.0,0.0,0.0,0.9) # Deformation gradient
	mysolver.setDeformationGradient(1.1,0.3,0.0,0.00,1.,0.0,0.0,0.0,1.) # Deformation gradient
	#stress averaging flag and averaging method 0- VOLUME, 1- SURFACE
	mysolver.stressAveragingFlag(1) # set stress averaging OFF- 0 , ON-1
	mysolver.setStressAveragingMethod(0) # 0 -volume 1- surface
	#tangent averaging flag
	mysolver.tangentAveragingFlag(1) # set tangent averaging OFF -0, ON -1
	mysolver.setTangentAveragingMethod(0,1e-6) # 0- perturbation 1- condensation

	mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
	mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
	mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
	mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
	mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
	mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
	mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
	mysolver.internalPointBuildView("eps_xx",IPField.STRAIN_XX, 1, 1)
	mysolver.internalPointBuildView("eps_yy",IPField.STRAIN_YY, 1, 1)
	mysolver.internalPointBuildView("eps_zz",IPField.STRAIN_ZZ, 1, 1)
	mysolver.internalPointBuildView("eps_xy",IPField.STRAIN_XY, 1, 1)
	mysolver.internalPointBuildView("eps_yz",IPField.STRAIN_YZ, 1, 1)
	mysolver.internalPointBuildView("eps_xz",IPField.STRAIN_XZ, 1, 1)

	#mysolver.initMicroSolver(); #solve()
	mysolver.solve()
	if BC=='save_stiff' or BC=='save_mass':
		mysolver.saveStiffnessMatrixToFile(1)

	return
	

def DeCSVtoArray(number):

	with open('E_0_GP_%i_tangent.csv' %(number),'r') as data_tangent:
		B_string=data_tangent.read()

	B_string=B_string[B_string.find('\n')+3:];
	B_string=B_string.split(';');
	del B_string[len(B_string)-1];

	B=map(float,B_string);
	
	C=npy.zeros((3.,3.,3.,3.));

	index_1=0;
	index_2=0;
	index_3=0;
	index_4=0;

	for i in range(len(B)):

		C[index_1,index_2,index_3,index_4]=B[i];

		index_1+=1;

		if index_1==3:
		
			index_1=0;
			index_2+=1;
		
			if index_2==3:
			
				index_1=0;
				index_2=0;
				index_3+=1;
			
				if index_3==3:
				
					index_1=0;
					index_2=0;
					index_3=0;
					index_4+=1;

					if index_4==3:

						index_4=0;
	

	return C








