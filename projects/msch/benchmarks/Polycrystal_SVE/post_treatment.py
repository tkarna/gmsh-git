#coding-Utf-8-*-
import numpy as npy
import scipy as spy
import matplotlib
import os
import subprocess
import pickle
import subprocess
from polycrystal_utils import genGeo
from ElasticityTensor_utils import GetElasticityTensor,DeCSVtoArray
from utils_utils import GetVoightNotation,Frob_norm,Material_from_C
import time
from matplotlib import pyplot as plt

with open('data/data_ssh/Result_6-6-1-25.dat','rb') as data:

	mon_depickler=pickle.Unpickler(data)
	liste_C=mon_depickler.load()
	liste_Cu=mon_depickler.load()
	liste_Cl=mon_depickler.load()
	#liste_Cpbc=mon_depickler.load() #BW en DESSOUS

liste_Cpbc=liste_C
dimdim=len(liste_C)
#dimdim=50

Ex_u=[]
Ex=[]
Ex_l=[]
Ey_u=[]
Ey=[]
Ey_l=[]
Ez_u=[]
Ez=[]
Ez_l=[]

numbering=[]

for i in range(dimdim):
	mat=Material_from_C(liste_C[i])
	mat_u=Material_from_C(liste_Cu[i])
	mat_l=Material_from_C(liste_Cl[i])
	Ex_u.append(mat_u[6])
	Ex.append(mat[6])
	Ex_l.append(mat_l[6])
	Ey_u.append(mat_u[1])
	Ey.append(mat[1])
	Ey_l.append(mat_l[1])
	Ez_u.append(mat_u[2])
	Ez.append(mat[2])
	Ez_l.append(mat_l[2])

	numbering.append(i+1)

font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 22}

matplotlib.rc('font', **font)

plt.plot(npy.array(numbering),npy.array(Ex),'b')
plt.plot(npy.array(numbering),npy.array(Ex_u),'g')
plt.plot(npy.array(numbering),npy.array(Ex_l),'r')
plt.xlabel('Number of sample')
plt.ylabel('Young modulus [MPa]')
plt.title('Young modulus in X direction')
plt.legend(("Periodic", "Upper bound","Lower bound"), 'best')
plt.show()

plt.plot(npy.array(numbering),npy.array(Ey),'b')
plt.plot(npy.array(numbering),npy.array(Ey_u),'g')
plt.plot(npy.array(numbering),npy.array(Ey_l),'r')
plt.xlabel('Number of sample')
plt.ylabel('Young Modulus [MPa]')
plt.title('Young modulus in y direction')
plt.legend(("Periodic", "Upper bound","Lower bound"), 'best')
plt.show()

plt.plot(npy.array(numbering),npy.array(Ez),'b')
plt.plot(npy.array(numbering),npy.array(Ez_u),'g')
plt.plot(npy.array(numbering),npy.array(Ez_l),'r')
plt.xlabel('Number of sample')
plt.ylabel('Young Modulus [MPa]')
plt.title('Young modulus in z direction')
plt.legend(("Periodic", "Upper bound","Lower bound"), 'best')
plt.show()

x_u=[]
x_l=[]

div_max_u=[]
div_min_u=[]
div_max_l=[]
div_min_l=[]

div_min_pbc=[]
div_max_pbc=[]

for i in range(dimdim):

	C_mat=GetVoightNotation(liste_C[i]);
	C_mat_KUBC=GetVoightNotation(liste_Cu[i]);
	C_mat_SUBC=GetVoightNotation(liste_Cl[i]);
	C_mat_pbc=GetVoightNotation(liste_Cpbc[i]);
	print "==========>>>> KUBC :"
	D,V=npy.linalg.eig(C_mat_KUBC-C_mat)
	D.sort()
	print D

	x_u.append(sum([1 for x in D if x <0]))
	div_max_u.append(D[2])
	div_min_u.append(npy.min(D))

	print "==========>>>> SUBC :"
	D,V=npy.linalg.eig(C_mat-C_mat_SUBC)
	D.sort()
	print D

	x_l.append(sum([1 for x in D if x <0]))

	div_max_l.append(D[2])
	div_min_l.append(npy.min(D))
	#CAS PBC
	#print "==========>>>> KUBC - PBC:"	
	D,V=npy.linalg.eig(C_mat_pbc-C_mat_SUBC)
	D.sort()
	#print D
	div_max_pbc.append(D[2])
	div_min_pbc.append(npy.min(D))
#x_u.sort()
#x_l.sort()

xx=[x+1 for x in range(dimdim)]

plt.plot(npy.array(xx),npy.array(x_u),'ro')
plt.plot(npy.array(xx),npy.array(x_l),'go')
plt.xlabel('Number of sample')
plt.ylabel('Number of negative eigenvalue of A-B')
plt.title('Positive definiteness for the bounds')
plt.legend(("Upper bound","Lower bound"), 'best')
plt.axis([0,dimdim+1,-1,3])
plt.show()

#Pour avoir les distributions sur Ex.

mean_Ex=npy.sum(Ex)/dimdim;
var_Ex=npy.sum([ex**2-mean_Ex**2 for ex in Ex])/dimdim;
sigma=npy.sqrt(var_Ex)

mean_Ex_u=npy.sum(Ex_u)/dimdim;
var_Ex_u=npy.sum([ex**2-mean_Ex_u**2 for ex in Ex_u])/dimdim;
sigma_u=npy.sqrt(var_Ex_u)

mean_Ex_l=npy.sum(Ex_l)/dimdim;
var_Ex_l=npy.sum([ex**2-mean_Ex_l**2 for ex in Ex_l])/dimdim;
sigma_l=npy.sqrt(var_Ex_l)

X=npy.linspace(120000,200000,200000)
pdf=1/(npy.sqrt(2*npy.pi*sigma**2))*npy.exp(-(X-mean_Ex)**2/(2*sigma**2))
pdf_u=1/(npy.sqrt(2*npy.pi*sigma_u**2))*npy.exp(-(X-mean_Ex_u)**2/(2*sigma_u**2))
pdf_l=1/(npy.sqrt(2*npy.pi*sigma_l**2))*npy.exp(-(X-mean_Ex_l)**2/(2*sigma_l**2))

font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 22}

matplotlib.rc('font', **font)

plt.plot(X,pdf,'b')
plt.plot(X,pdf_u,'g')
plt.plot(X,pdf_l,'r')
plt.xlabel('Young modulus in x direction [MPa]')
plt.ylabel('Probability')
plt.title('Distribution of Ex with different BCs')
plt.legend(("MBC", "KUBC","SUBC"), 'best')
plt.show()


div_max_u=npy.array(div_max_u)
div_min_u=npy.array(div_min_u)
div_max_l=npy.array(div_max_l)
div_min_l=npy.array(div_min_l)

div_min_pbc=npy.array(div_min_pbc)
div_max_pbc=npy.array(div_max_pbc)

#plt.plot(npy.array(numbering),div_max_pbc,'g')
#plt.plot(npy.array(numbering),div_min_pbc,'g--')
plt.plot(npy.array(numbering),div_max_l,'g')
plt.plot(npy.array(numbering),div_min_l,'r')
plt.xlabel('Number of sample')
plt.ylabel('Eigenvalue')
plt.title('Eigenvalue analysis - lower bound')
plt.legend(("3rd value","Minimum"),'best')
plt.show()

print("sigma vaut: ")
print(sigma)


plt.plot(npy.array(numbering),npy.array(Ex_u)-npy.array(Ex),'b')
plt.plot(npy.array(numbering),npy.array(Ex)-npy.array(Ex_l),'g')
plt.xlabel('Number of sample')
plt.ylabel('mu-mu [MPa]')
plt.title('Difference between mu xy and its bounds')
plt.legend(("Upper bound","Lower bound"), 'best')
plt.show()

