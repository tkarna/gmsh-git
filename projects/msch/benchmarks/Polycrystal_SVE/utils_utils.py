#coding-Utf-8-*-
import numpy as npy
import scipy as spy
import matplotlib
import scipy.stats as spystats

def GetVoightNotation(C):

	#C is a 4th order tensor.
	#The function returns its voight notation

	C_mat =  npy.zeros((6,6));
	C_mat[0,:] = [C[0,0,0,0], C[0,0,1,1], C[0,0,2,2], C[0,0,1,2], C[0,0,2,0], C[0,0,0,1]];
	C_mat[1,:] = [C[1,1,0,0], C[1,1,1,1], C[1,1,2,2], C[1,1,1,2], C[1,1,2,0], C[1,1,0,1]];
	C_mat[2,:] = [C[2,2,0,0], C[2,2,1,1], C[2,2,2,2], C[2,2,1,2], C[2,2,2,0], C[2,2,0,1]];
	C_mat[3,:] = [C[1,2,0,0], C[1,2,1,1], C[1,2,2,2], C[1,2,1,2], C[1,2,2,0], C[1,2,0,1]];
	C_mat[4,:] = [C[2,0,0,0], C[2,0,1,1], C[2,0,2,2], C[2,0,1,2], C[2,0,2,0], C[2,0,0,1]];
	C_mat[5,:] = [C[0,1,0,0], C[0,1,1,1], C[0,1,2,2], C[0,1,1,2], C[0,1,2,0], C[0,1,0,1]];

	C_mat2 = npy.zeros((6.,6.))
	for I in range(6):

		if I==0:
			j=0
			k=0
		elif I==1:
			j=1
			k=1
		elif I==2:
			j=2
			k=2
		elif I==3:
			j=0
			k=1
		elif I==4:
			j=0
			k=2
		elif I==5:
			j=1
			k=2
		for J in range(6):

			if J==0:
				l=0
				m=0
			elif J==1:
				l=1
				m=1
			elif J==2:
				l=2
				m=2
			elif J==3:
				l=0
				m=1
			elif J==4:
				l=0
				l=2
			elif J==5:
				l=1
				m=2

			C_mat2[I,J]=C[j,k,l,m]
	
	return C_mat

def Frob_norm(A,dim):

	A_frobenius=0;
	for i in range(dim):
		for j in range(dim):
			A_frobenius+=A[i,j]**2

	A_frobenius=npy.sqrt(A_frobenius);

	return A_frobenius

def trace(A):
	
	Ndim=A.shape[0]
	a=0;
	for i in range(Ndim):
		a+=A[i,i];
	return a


def MatriceDeRotation(alpha,beta,gamma):
	alpha=alpha*npy.pi/180
	beta=beta*npy.pi/180
	gamma=gamma*npy.pi/180
	R_z1=npy.array([[npy.cos(alpha),-npy.sin(alpha),0.],[npy.sin(alpha),npy.cos(alpha),0.],[0.,0.,1.]])
	R_x=npy.array([[1.,0.,0.],[0.,npy.cos(beta),-npy.sin(beta)],[0,npy.sin(beta),npy.cos(beta)]])
	R_z2=npy.array([[npy.cos(gamma),-npy.sin(gamma),0.],[npy.sin(gamma),npy.cos(gamma),0.],[0.,0.,1.]])
	R=npy.dot(npy.dot(R_z2,R_x),R_z1);

	return R


def Material_from_C(C):

	if C.shape[0]==3:

		C=GetVoightNotation(C)
	
	S=npy.linalg.inv(C)
	Ex=1./S[0,0]
	Ey=1./S[1,1]
	Ez=1./S[2,2]

	Vxy=-S[0,1]/S[1,1]
	Vxz=-S[0,2]/S[2,2]
	Vyz=-S[0,2]/S[2,2]

	MUxy=1./S[3,3]
	MUxz=1./S[4,4]
	MUyz=1./S[5,5]

	material=[Ex,Ey,Ez,Vxy,Vxz,Vyz,MUxy,MUxz,MUyz]

	return material

def getCfromMat(material,alpha,beta,gamma):

	rho   = 2329.
	Ex=material[0];	Ey=material[1];	Ez=material[2];
	Vxy=material[3];	Vxz=material[4];	Vyz=material[5];
	MUxy=material[6];MUxz=material[7];MUyz=material[8];

	_alpha=alpha
	_beta=beta
	_gamma=gamma

	ElasticityTensor=npy.zeros((3.,3.,3.,3.))

	Vyx= Vxy*Ey/Ex  ; 
	Vzx= Vxz*Ez/Ex  ; 
	Vzy= Vyz*Ez/Ey  ;
	D=( 1-Vxy*Vyx-Vzy*Vyz-Vxz*Vzx-2*Vxy*Vyz*Vzx ) / ( Ex*Ey*Ez );

	ElasticityTensor[0,0,0,0]=( 1-Vyz*Vzy ) / (Ey*Ez*D );
	ElasticityTensor[1,1,1,1]=( 1-Vxz*Vzx ) / (Ex*Ez*D );
	ElasticityTensor[2,2,2,2]=( 1-Vyx*Vxy ) / (Ey*Ex*D );
	ElasticityTensor[0,0,1,1]=( Vyx+Vzx*Vyz ) / (Ey*Ez*D );
	ElasticityTensor[0,0,2,2]=( Vzx+Vyx*Vzy ) / (Ey*Ez*D );
	ElasticityTensor[1,1,2,2]=( Vzy+Vxy*Vzx ) / (Ex*Ez*D );
	ElasticityTensor[1,1,0,0]=( Vxy+Vzy*Vxz ) / (Ex*Ez*D );
	ElasticityTensor[2,2,0,0]=( Vxz+Vzy*Vyz ) / (Ey*Ex*D );
	ElasticityTensor[2,2,1,1]=( Vyz+Vxz*Vyx ) / (Ey*Ex*D );
	ElasticityTensor[1,2,1,2]=MUyz;ElasticityTensor[1,2,2,1]=MUyz;
	ElasticityTensor[2,1,2,1]=MUyz;ElasticityTensor[2,1,1,2]=MUyz;
	ElasticityTensor[0,1,0,1]=MUxy;ElasticityTensor[0,1,1,0]=MUxy;
	ElasticityTensor[1,0,1,0]=MUxy;ElasticityTensor[1,0,0,1]=MUxy;
	ElasticityTensor[0,2,0,2]=MUxz;ElasticityTensor[0,2,2,0]=MUxz;
	ElasticityTensor[2,0,2,0]=MUxz;ElasticityTensor[2,0,0,2]=MUxz;
	
	pi=3.14159265359;
	fpi = pi/180.;

	c1 = npy.cos(_alpha*fpi);
	s1 = npy.sin(_alpha*fpi);

	c2 = npy.cos(_beta*fpi);
	s2 = npy.sin(_beta*fpi);

	c3 = npy.cos(_gamma*fpi);
	s3 = npy.sin(_gamma*fpi);

	s1c2 = s1*c2;
	c1c2 = c1*c2;
	
	R=npy.zeros((3.,3.))
	
	R[0,0] = c3*c1 - s1c2*s3;
	R[0,1] = c3*s1 + c1c2*s3;
	R[0,2] = s2*s3;

	R[1,0] = -s3*c1 - s1c2*c3;
	R[1,1] = -s3*s1 + c1c2*c3;
	R[1,2] = s2*c3;

	R[2,0] = s1*s2;
	R[2,1] = -c1*s2;
	R[2,2] = c2;

	#R=MatriceDeRotation(_alpha,_beta,_gamma)
	#print R

	_ElasticityTensor=npy.zeros((3.,3.,3.,3.))

	for i in range(3):
		for j in range(3):

			for k in range(3):

				for l in range(3):
					_ElasticityTensor[i,j,k,l]=0.;
					for m in range(3):
	
						for n in range(3):
	
							for o in range(3):
	
								for p in range(3):

									_ElasticityTensor[i,j,k,l]+=R[m,i]*R[n,j]*R[o,k]*R[p,l]*ElasticityTensor[m,n,o,p];

	return _ElasticityTensor

def isotropeMat(E,mu):
	nu=E/(2*mu)-1;
	return [E,E,E,nu,nu,nu,mu,mu,mu]


def absolute_bound(liste_C,liste_Cl,liste_Cu):

	print "seeking Cu and Cl ..."
	
	if len(liste_C[0])==3:
		liste_Cu=[GetVoightNotation(A) for A in liste_Cu]
		liste_Cl=[GetVoightNotation(A) for A in liste_Cl]
		liste_C=[GetVoightNotation(A) for A in liste_C]

	nbr=len(liste_C);
	#Get max E, mu and min
	max_E=[]
	min_E=[]

	max_mu=[]
	min_mu=[]
	for i,Cu in enumerate(liste_Cu):
		Cl=liste_Cl[i];
		MatU=Material_from_C(Cu);
		MatL=Material_from_C(Cl);

		max_E+=[MatU[0],MatU[1],MatU[2]];
		min_E+=[MatL[0],MatL[1],MatL[2]];
		
		max_mu+=[MatU[6],MatU[7],MatU[8]];
		min_mu+=[MatL[6],MatL[7],MatL[8]];

	max_E=npy.max(max_E);
	min_E=npy.min(min_E);
	
	max_mu=npy.max(max_mu);
	min_mu=npy.min(min_mu);

	max_E0=max_E;
	max_mu0=max_mu;
	min_E0=min_E;
	min_mu0=min_mu;

	#Upper Absolute Bound
	#====================

	#nu=max_E/(2*max_mu)-1;
	Mat=isotropeMat(max_E,max_mu);
	Cu=GetVoightNotation(getCfromMat(Mat,0.,0.,0.));	

	#Identification of the worst eigenvalue
	for i,C in enumerate(liste_C):
		D,V=npy.linalg.eig(Cu-C);
		D=npy.min(D);
		if i==0:
			I=0;
			lambda_min=D;
		elif D<lambda_min:
			I=i;
			lambda_min=D;
	
	#If already absolute bound:
	de=0.1
	dmu=0.1
	alpha=100
	Lambda_min=lambda_min;
	while Lambda_min<0:
		lambda_min=Lambda_min;
		while lambda_min<0:
			Mat=isotropeMat(max_E+de,max_mu);
			Cu_dotE=GetVoightNotation(getCfromMat(Mat,0.,0.,0.));

			D,V=npy.linalg.eig(Cu_dotE-liste_C[I]);
			D=npy.min(D)
			lambda_dotE=(D-lambda_min)/de;
		
			Mat=isotropeMat(max_E,max_mu+dmu);
			Cu_dotmu=GetVoightNotation(getCfromMat(Mat,0.,0.,0.));

			D,V=npy.linalg.eig(Cu_dotmu-liste_C[I]);
			D=npy.min(D)
			lambda_dotmu=(D-lambda_min)/dmu;
		
			max_E=max_E+alpha*lambda_dotE;
			max_mu=max_mu+alpha*lambda_dotmu;
			Mat=isotropeMat(max_E,max_mu);
			Cu=GetVoightNotation(getCfromMat(Mat,0.,0.,0.));
			D,V=npy.linalg.eig(Cu-liste_C[I]);
			lambda_min=npy.min(D);
	
		#Identification of the worst eigenvalue
		for i,C in enumerate(liste_C):
			D,V=npy.linalg.eig(Cu-C);
			D=npy.min(D);
			if i==0:
				I=0;
				Lambda_min=D;
			elif D<lambda_min:
				I=i;
				Lambda_min=D;i

	#Lower Absolute Bound
	#====================

	#nu=min_E/(2*min_mu)-1;
	Mat=isotropeMat(min_E,min_mu);
	Cl=GetVoightNotation(getCfromMat(Mat,0.,0.,0.));	

	#Identification of the worst eigenvalue
	for i,C in enumerate(liste_C):
		D,V=npy.linalg.eig(C-Cl);
		D=npy.min(D);
		if i==0:
			I=0;
			lambda_min=D;
		elif D<lambda_min:
			I=i;
			lambda_min=D;
	
	#If already absolute bound:
	de=-0.1
	dmu=-0.1
	alpha=200
	Lambda_min=lambda_min;
	while Lambda_min<0:
		lambda_min=Lambda_min;
		while lambda_min<0:
			Mat=isotropeMat(min_E+de,min_mu);
			Cl_dotE=GetVoightNotation(getCfromMat(Mat,0.,0.,0.));

			D,V=npy.linalg.eig(liste_C[I]-Cl_dotE);
			D=npy.min(D)
			lambda_dotE=(D-lambda_min)/de;
		
			Mat=isotropeMat(min_E,min_mu+dmu);
			Cl_dotmu=GetVoightNotation(getCfromMat(Mat,0.,0.,0.));

			D,V=npy.linalg.eig(liste_C[I]-Cl_dotmu);
			D=npy.min(D)
			lambda_dotmu=(D-lambda_min)/dmu;
		
			min_E=min_E+alpha*lambda_dotE;
			min_mu=min_mu+alpha*lambda_dotmu;
			Mat=isotropeMat(min_E,min_mu);
			Cl=GetVoightNotation(getCfromMat(Mat,0.,0.,0.));
			D,V=npy.linalg.eig(liste_C[I]-Cl);
			lambda_min=npy.min(D);
	
		#Identification of the worst eigenvalue
		for i,C in enumerate(liste_C):
			D,V=npy.linalg.eig(C-Cl);
			D=npy.min(D);
			if i==0:
				I=0;
				Lambda_min=D;
			elif D<lambda_min:
				I=i;

	return Cu,Cl

def Vsort(A_init):
	#This function returns the sorted version of A
	#It also returns the position of the new element in the initial matrix
	
	A_final=npy.sort(A_init)
	pos=A_final*0.;

	indice=npy.arange(len(A_init))

	#for i,a_final in enumerate(A_final):
	i=0
	while i<len(A_init):
		a_final=A_final[i]
		pos[i:i+len(indice[A_init==a_final])]=indice[A_init==a_final]
		i=i+len(indice[A_init==a_final])

	pos=pos.astype(int);

	return A_final,pos

def GenRand(distri,parameters):

	if distri=="GAUSS":

		mean=parameters[0]
		sigma=parameters[1]
		size=parameters[2]
		npy.random.normal(mean,sigma,size)

	elif distri=="GAUSSP":

		mean=parameters[0]
		sigma=parameters[1]
		size=parameters[2]
		A=npy.zeros((size,1.))
		for i in range(int(size)):
			a=-1;
			while (a<0):
				a=npy.random.normal(mean,sigma,size)
			A[i,0.]=a;

		return A

	elif distri=="GAMMA":

		a=parameters[0]
		b=parameters[1]
		c=parameters[2]
		d=parameters[3]

		return scipystats.gamma.rvs(a,loc=b,scale=c,size=d)

	else:

		print "Distribution not found"
		print "Gauss considered ..."

#The MatName is either Mass or Stiffness
def ReadMatFile(FileName):
	
	with open(FileName,'rb') as FileMat:
		Mat=FileMat.read();

	return npy.array([[float(b) for b in a.split(" ") if b!= '' and b != ' '] for a in Mat.split("\n") if a.split(" ")!=['']])

def GenBeamGeo(h_x,h_y,h_z,NelemX,NelemY,NelemZ):

	fid = open("beam.geo", "w")

	fid.write("%s\n" %('x={};'.format(h_x)));
	fid.write("%s\n" %('y={};'.format(h_y)));
	fid.write("%s\n" %('z={};'.format(h_z)));
	fid.write("%s\n" % ('Lc1=1.5;'));
	fid.write("%s\n" % ('Point(1) = { 0.0 , 0.0 , 0.0 , Lc1};'));
	fid.write("%s\n" % ('Point(2) = {  x  , 0.0 , 0.0 , Lc1};'));
	fid.write("%s\n" % ('Point(3) = {  x  ,  y  , 0.0 , Lc1};'));
	fid.write("%s\n" % ('Point(4) = { 0.0 ,  y  , 0.0 , Lc1};'));
	fid.write("%s\n" % ('Point(5) = { 0.0 , 0.0 , z , Lc1};'));
	fid.write("%s\n" % ('Point(6) = {  x  , 0.0 , z , Lc1};'));
	fid.write("%s\n" % ('Point(7) = {  x  ,  y  , z , Lc1};'));
	fid.write("%s\n" % ('Point(8) = { 0.0 ,  y  , z , Lc1};'));



	fid.write("%s\n" % ('Line(1) = {1,2};'));
	fid.write("%s\n" % ('Line(2) = {2,3};'));
	fid.write("%s\n" % ('Line(3) = {3,4};'));
	fid.write("%s\n" % ('Line(4) = {4,1};'));
	fid.write("%s\n" % ('Line(5) = {5,6};'));
	fid.write("%s\n" % ('Line(6) = {6,7};'));
	fid.write("%s\n" % ('Line(7) = {7,8};'));
	fid.write("%s\n" % ('Line(8) = {8,5};'));
	fid.write("%s\n" % ('Line(9) = {1,5};'));
	fid.write("%s\n" % ('Line(10)= {2,6};'));
	fid.write("%s\n" % ('Line(11)= {3,7};'));
	fid.write("%s\n" % ('Line(12)= {4,8};'));


	fid.write("%s\n" % ('Line Loop(1) = {1,2,3,4};'));
	fid.write("%s\n" % ('Line Loop(2) = {5,6,7,8};'));
	fid.write("%s\n" % ('Line Loop(3) = {1,10,-5,-9};'));
	fid.write("%s\n" % ('Line Loop(4) = {2,11,-6,-10};'));
	fid.write("%s\n" % ('Line Loop(5) = {3,12,-7,-11};'));
	fid.write("%s\n" % ('Line Loop(6) = {4,9,-8,-12};'));

	fid.write("%s\n" % ('Plane Surface(1) = {1};'));
	fid.write("%s\n" % ('Plane Surface(2) = {2};'));
	fid.write("%s\n" % ('Plane Surface(3) = {3};'));
	fid.write("%s\n" % ('Plane Surface(4) = {4};'));
	fid.write("%s\n" % ('Plane Surface(5) = {5};'));
	fid.write("%s\n" % ('Plane Surface(6) = {6};'));


	fid.write("%s\n" % ('Surface Loop(7) = {1,2,3,4,5,6};'));
	fid.write("%s\n" % ('Volume(1) = {7};'));


	fid.write("%s\n" % ('Physical Surface(103) = {1};'));
	fid.write("%s\n" % ('Physical Surface(130) = {2};'));
	fid.write("%s\n" % ('Physical Surface(102) = {3};'));
	fid.write("%s\n" % ('Physical Surface(101) = {4};'));
	fid.write("%s\n" % ('Physical Surface(120) = {5};'));
	fid.write("%s\n" % ('Physical Surface(110) = {6};'));

	fid.write("%s\n" % ('Physical Volume(11) ={1};'));

	fid.write("%s%f%s\n" % ('Transfinite Line {1,3,5,7} =',NelemX+1,';'));
	fid.write("%s%f%s\n" % ('Transfinite Line {2,4,6,8} =',NelemY+1,';'));
	fid.write("%s%f%s\n" % ('Transfinite Line {9,10,11,12} =',NelemZ+1,';'));
	fid.write("%s\n" % ('Transfinite Surface {1,2,3,4,5,6} ;'));
	fid.write("%s\n" % ('Recombine Surface {1,2,3,4,5,6} ;'));
	fid.write("%s\n" % ('Transfinite Volume {1};'));

	
