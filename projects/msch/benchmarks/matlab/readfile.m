clear all
close all

strain = importdata('./E_0_GP_0_strain.csv');
stress = importdata('./E_0_GP_0_stress.csv');

straintable = strain.data;

stresstable = stress.data; %dlmread('./homogenizedStress.csv');

stresstime = straintable(:,6);
figure(1);
plot(stresstime,stresstable(:,2:10),'o-','LineWidth',1.5);
legend(stress.textdata(2:10))
set(gca,'FontSize',16)
xlabel('\mu')
ylabel('P (Pa)')
grid on

print -f1 -r600 -depsc stressImage
system('epstopdf stressImage.eps')

tangent = importdata('./E_0_GP_0_tangent.csv');
figure(2)
tangenttable = tangent.data; %dlmread('./homogenizedTangent.csv');
plot(stresstime,tangenttable(:,2:82),'LineWidth',1.5);
legend(tangent.textdata(2:82))
set(gca,'FontSize',16)
xlabel('\mu')
ylabel('L (Pa)')
grid on

%print -f2 -r600 -depsc tangentImage
%system('epstopdf tangentImage.eps')



tangent = importdata('./E_0_GP_0_second_tangent.csv');
figure(3)
tangenttable = tangent.data; %dlmread('./homogenizedTangent.csv');
plot(stresstime,tangenttable(:,2:730),'LineWidth',1.5);
legend(tangent.textdata(2:730))
set(gca,'FontSize',16)
xlabel('\mu')
ylabel('L (Pa)')
grid on

