clear all
close all
clc
%----------------------------------------
tangent = importdata('./E_0_GP_0_tangent.csv');

siz = size(tangent.data);
row = siz(1);
col = siz(2);
theta = linspace(0,2*pi,200);
N = length(theta);

detQ = theta;
mindetQ = zeros(row,1);
thethaminQ = zeros(row,1);

for ii=1:row
   L = vec2ten(tangent.data(ii,2:col),tangent.textdata(2:col));
   for jj=1:N
    Q = zeros(3,3);
    n = zeros(3,1);
    n(1) = cos(theta(jj));
    n(2) = sin(theta(jj));
    n(3) = 0;
    for j=1:3
        for k=1:3
            for i=1:3
                for l=1:3
                    Q(j,k) =Q(j,k)+ n(i)*L(i,j,k,l)*n(l); 
                end
            end
        end
    end
    QQ = Q(1:2,1:2);
    detQ(ii,jj) = det(QQ);
   end

   [mindetQ(ii) ll] = min(detQ(ii,:));
   thethaminQ(ii)= theta(ll);
end

figure(2)
plot(theta*180/pi,detQ,'LineWidth',2)
set(gca,'FontSize',16)
xlabel('\theta *\pi')
ylabel('det(Q)')


print -f2 -r600 -depsc detQImage
system('epstopdf detQImage.eps')

figure(3)
plot(tangent.data(:,1),mindetQ,'x-','LineWidth',2)
set(gca,'FontSize',16)
xlabel('\mu ')
ylabel('min det(Q)')
grid on

print -f3 -r600 -depsc mindetQImage

figure(4)
plot(tangent.data(:,1),thethaminQ*180/pi,'x-','LineWidth',2)
system('epstopdf mindetQImage.eps')


