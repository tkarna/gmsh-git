clear all
close all
clc

eig = load('./E_0_GP_0_eigenValue.csv');
A = size(eig);

figure(1)
grid on
plot(eig(:,1),eig(:,2),'o--','LineWidth',2);
set(gca,'FontSize',16)
xlabel('\mu');
ylabel('Min eigenvalue')
print -f1 -r600 -depsc eigen
system('epstopdf eigen.eps')


figure()
grid on
hold on
for i=2:2:10
    plot(eig(:,1),eig(:,i),'-')
end
