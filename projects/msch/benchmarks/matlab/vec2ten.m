function [ out ] = vec2ten( in, header)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
 
 lsize = max(size(in));
 if (lsize==9)
    out = zeros(3,3);
 elseif (lsize==27)
     out = zeros(3,3,3);
 elseif (lsize==81)
     out = zeros(3,3,3,3);
 elseif (lsize==243)
     out = zeros(3,3,3,3,3);
 elseif (lsize==729)
     out = zeros(3,3,3,3,3,3);
 end
 
 for ii=1:lsize
    s = header{ii};
    if (lsize==9)
        i = str2num(s(6))+1;
        j = str2num(s(7))+1;
        out(i,j) = in(ii);
    elseif (lsize==27)
        i = str2num(s(6))+1;
        j = str2num(s(7))+1;
        k = str2num(s(8))+1;
        out(i,j,k) = in(ii);
    elseif (lsize==81)
        i = str2num(s(6))+1;
        j = str2num(s(7))+1;
        k = str2num(s(8))+1;
        l = str2num(s(9))+1;
        out(i,j,k,l) = in(ii);
    elseif (lsize==243)
        i = str2num(s(6))+1;
        j = str2num(s(7))+1;
        k = str2num(s(8))+1;
        l = str2num(s(9))+1;
        m = str2num(s(10))+1;
        out(i,j,k,l,m) = in(ii);
    elseif (lsize==729)
        i = str2num(s(6))+1;
        j = str2num(s(7))+1;
        k = str2num(s(8))+1;
        l = str2num(s(9))+1;
        m = str2num(s(10))+1;
        n = str2num(s(11))+1;
        out(i,j,k,l,m,n) = in(ii);

    end
        
 end



end

