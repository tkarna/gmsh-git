#coding-Utf-8-*-
from gmshpy import *
from msch import*

#script to launch PBC problem with a python script

#script to launch PBC problem with a python script

# material law
lawnum = 11 # unique number of law
K = 76E3 	# Bulk mudulus
mu =26E3  # Shear mudulus
rho = 800. # Bulk mass
sy0 = 400
h = 0.*sy0
E = 9.*K*mu/(3.*K+mu)
nu= (3.*K -2.*mu)/2./(3.*K+mu)

# creation of material law
#law1 = FSJ2LinearMaterialLaw(lawnum,E,nu,sy0,h,rho)
law1 = FSElasticMaterialLaw(lawnum,0,K,mu,rho)

# geometry
#meshfile="idealHole3D.msh" # name of mesh file
meshfile = "idealCube.msh"

# creation of part Domain
nfield = 1 # number of the field (physical number of entity)
dim =3
myfield1 = FSDomain(1000,nfield,lawnum,dim)

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
system = 1 # Displacement elimination =0 Multiplier elimination = 1 Displacement+ multiplier = 2
control = 0 # load control = 0 arc length control euler = 1

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.setSystemType(system)
mysolver.setControlType(control)

#rve
mysolver.setPeriodicity(1.,0,0,"x")
mysolver.setPeriodicity(0,1.,0,"y")
mysolver.setPeriodicity(0,0,1.,"z")

#boundary condition

#linear displacement BC
#mysolver.addLinearDisplacementBC(1000,109,111,113,110,112,114) 


#minimal kinematical BC
#mysolver.addMinimalKinematicBC(1000,109,111,113,110,112,114) 


# periodiodic BC
method = 1		# Periodic mesh = 0, Langrange interpolation = 1, Cubic spline interpolation =2 
degree = 5		# Order used for polynomial interpolation 
addvertex = 1 # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex 
#mysolver.addPeriodicBC(1000,109,111,113,110,112,114) #Periodic boundary condition
mysolver.addPeriodicBC(1000,1,3,5,2,4,6) #Periodic boundary condition
mysolver.setPeriodicBCOptions(method, degree,addvertex) 

# kinematic macro-variable
mysolver.setOrder(2) # Homogenization order
mysolver.setDeformationGradient(1.0,0.0,0.,0.0,1.01,0.0,0.0,0.0,1.01) # Deformation gradient
mysolver.setGradientOfDeformationGradient(2,1,2,0.1) # Gradient of deformation gradient
mysolver.setGradientOfDeformationGradient(2,2,1,0.1)
mysolver.setGradientOfDeformationGradient(1,0,1,0.1)
mysolver.setGradientOfDeformationGradient(0,1,2,1)
mysolver.setGradientOfDeformationGradient(0,2,1,1)
mysolver.setGradientOfDeformationGradient(1,2,0,1)
mysolver.setGradientOfDeformationGradient(1,0,2,1)
mysolver.setGradientOfDeformationGradient(2,1,0,1)
mysolver.setGradientOfDeformationGradient(2,0,1,1)
mysolver.setGradientOfDeformationGradient(0,0,0,1)


# eigensolver
#mysolver.eigenValueSolver(100) # set eigensolver with number of eigenvalue

#stress averaging flag and averaging method 0- VOLUME, 1- SURFACE
mysolver.stressAveragingFlag(1) # set stress averaging ON- 0 , OFF-1
#tangent averaging flag
mysolver.tangentAveragingFlag(1) # set tangent averaging ON -0, OFF -1
mysolver.setTangentAveragingMethod(1,1e-8) # 0- perturbation 1- condensation



# build view
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GLSTRAINEQUIVALENT, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

#stiffness modifcation 
mysolver.stiffnessModification(1)

#need iterative procedure
mysolver.iterativeProcedure(1)

# solve
mysolver.solve()

