mm = 1;
L = 1*mm;
R1 = 0.1*mm;
R2 = 0.15*mm;
R3 = 0.2*mm;
R4 = 0.25*mm;

lsca = 0.03*mm;

Point(1) = {0, 0, 0,lsca};
Point(2) = {L, 0, 0,lsca};
Point(3) = {L, L, 0,lsca};
Point(4) = {0, L, 0,lsca};
Point(5) = {R1, 0, 0,lsca};
Point(6) = {0, R1, 0,lsca};
Point(7) = {L-R2, 0, 0,lsca};
Point(8) = {L, R2, 0,lsca};
Point(9) = {L,L-R3,0,lsca};
Point(10) = {L-R3, L, 0,lsca};
Point(11) = {R4, L, 0,lsca};
Point(12) = {0, L-R4, 0,lsca};

x = L/2; y=L/2; r = 0.2*mm; sl2 = 0.5*lsca;
p1=newp; Point(p1)={x-r,y,0,sl2};
p2=newp; Point(p2)={x,y+r,0,sl2};
p3=newp; Point(p3)={x+r,y,0,sl2};
p4=newp; Point(p4)={x,y-r,0,sl2};
pc=newp; Point(pc)={x,y,0,sl2};
c1 = newreg; Circle(c1) = {p1,pc,p2};
c2 = newreg; Circle(c2) = {p2,pc,p3};
c3 = newreg; Circle(c3) = {p3,pc,p4};
c4 = newreg; Circle(c4) = {p4,pc,p1};



Line(5) = {5, 7};
Line(6) = {8, 9};
Line(7) = {10, 11};
Line(8) = {12, 6};
Circle(9) = {12, 4, 11};
Circle(10) = {10, 3, 9};
Circle(11) = {8, 2, 7};
Circle(12) = {5, 1, 6};
Line Loop(13) = {9, -7, 10, -6, 11, -5, 12, -8};
Line Loop(14) = {1, 2, 3, 4};
Plane Surface(15) = {13, 14};
Physical Line(1) = {5};
Physical Line(2) = {6};
Physical Line(3) = {7};
Physical Line(4) = {8};
Physical Surface(11) = {15};
