mm=0.1;

n=10;
L=n*mm;

Lcut=3*mm;
lsca1=0.15*mm;

For i In {0:n}
  x[i]=i*L/n;
  y[i]=i*L/n;
EndFor
/*
For i In {0:n}
	For j In {0:n-1}
		p1=newp; Point(p1)={x[i],y[j],0};
		p2=newp; Point(p2)={x[i],y[j+1],0};
		s=newreg; Line(s)={p1,p2};
	EndFor
EndFor

For i In {0:n-1}
	For j In {0:n}
		p1=newp; Point(p1)={x[i],y[j],0};
		p2=newp; Point(p2)={x[i+1],y[j],0};
		s=newreg; Line(s)={p1,p2};
	EndFor
EndFor
*/
For i In {0:n-1}
	For j In {0:n-1}
		ij=1000*i+j;
		cpx[ij]=(x[i+1]+x[i])/2+(Rand(1)+Rand(-1))*((x[i+1]-x[i])/2);
		cpy[ij]=(y[j+1]+y[j])/2+(Rand(1)+Rand(-1))*((y[j+1]-y[j])/2);
	EndFor
EndFor

Function circle
	center=newp; Point(center)={cx,cy,0};
	p1=newp; Point(p1)={cx-r,cy,0,lsca2};
	p2=newp; Point(p2)={cx,cy+r,0,lsca2};
	p3=newp; Point(p3)={cx+r,cy,0,lsca2};
	p4=newp; Point(p4)={cx,cy-r,0,lsca2};
	s1=newreg; Circle(s1)={p1,center,p2};
	s2=newreg; Circle(s2)={p2,center,p3};
	s3=newreg; Circle(s3)={p3,center,p4};
	s4=newreg; Circle(s4)={p4,center,p1};
	icircle+=1; cir[icircle]=newreg; Line Loop(cir[icircle])={s1,s2,s3,s4};
  voidsum+=Pi*r*r;
	Printf("Inner void: %g, %g",r, Pi*r*r);
Return

Function creatVoid
		/*
		lsca2=lsca1;
		p1=newp; Point(p1)={a[0],a[1],0,lsca2};
		p2=newp; Point(p2)={b[0],b[1],0,lsca2};
		p3=newp; Point(p3)={c[0],c[1],0,lsca2};

	 	s=newreg; Line(s)={p1,p2};
		s=newreg; Line(s)={p2,p3};
		s=newreg; Line(s)={p3,p1};
*/
		AB=Sqrt((a[0]-b[0])^2+(a[1]-b[1])^2);
		AC=Sqrt((a[0]-c[0])^2+(a[1]-c[1])^2);
		BC=Sqrt((c[0]-b[0])^2+(c[1]-b[1])^2);

		For i In {0:1}
			uAB[i]=(b[i]-a[i])/AB;
			uAC[i]=(c[i]-a[i])/AC;
			u1[i]=uAB[i]+uAC[i];
			uBC[i]=(c[i]-b[i])/BC;
			u2[i]=uBC[i]-uAB[i];
		EndFor

		det=-u1[0]*u2[1]+u1[1]*u2[0];
		t=(u2[1]*(a[0]-b[0])-u2[0]*(a[1]-b[1]))/det;
		For i In {0:1}
			O[i]=a[i]+t*u1[i];
		EndFor

		nAB[0]=-uAB[1];
		nAB[1]=uAB[0];
		r=(nAB[0]*(O[0]-a[0])+nAB[1]*(O[1]-a[1]))/Sqrt(nAB[0]*nAB[0]+nAB[1]*nAB[1])*0.7;
		If (r<0)
			r=-r;
		EndIf

		cx=O[0]; cy=O[1]; 
		hbot=cy-(L/2-Lcut/2);
		htop=L/2+Lcut/2-cy;
		hleft=cx-(L/2-Lcut/2);
		hright=L/2+Lcut/2-cx;

		If ((hbot>=r) && (htop>=r) && (hleft>=r) && (hright>=r))
			rsum+=r; rcomp+=1; lsca2=0.3*r;
			Call circle;
			
		EndIf
Return


xcut[0]=L/2+Lcut/2; ycut[0]=L/2-Lcut/2;
xcut[1]=L/2+Lcut/2; ycut[1]=L/2+Lcut/2;
xcut[2]=L/2-Lcut/2;	ycut[2]=L/2+Lcut/2;
xcut[3]=L/2-Lcut/2;	ycut[3]=L/2-Lcut/2;

For i In {0:3}
	p[i]=newp; Point(p[i])={xcut[i],ycut[i],0,lsca1};
EndFor

icircle=0;
ileft=1; iright=1; itop=1; ibot=1;
left[0]=p[3]; right[0]=p[0]; top[0]=p[2]; bot[0]=p[3];
dleft[0]=L/2-Lcut/2; dright[0]=L/2-Lcut/2; dtop[0]=L/2-Lcut/2; dbot[0]=L/2-Lcut/2;
icar=0;
rsum=0;
rcomp=0;
voidsum=0;
For k In {0:n-2}
	For l In {0:n-2}
		ij=1000*k+l;
		ij1=1000*k+l+1;
		i1j=1000*(k+1)+l;
		i1j1=1000*(k+1)+l+1;
		a[0]=cpx[ij];
		a[1]=cpy[ij];
		b[0]=cpx[i1j];	
		b[1]=cpy[i1j];
		c[0]=cpx[i1j1];
		c[1]=cpy[i1j1];
		Call creatVoid;
		
		a[0]=cpx[ij];
		a[1]=cpy[ij];
		b[0]=cpx[ij1];	
		b[1]=cpy[ij1];
		c[0]=cpx[i1j1];
		c[1]=cpy[i1j1];
		Call creatVoid;
	EndFor
EndFor

left[ileft]=p[2]; right[iright]=p[1]; top[itop]=p[1]; bot[ibot]=p[0];
dleft[ileft]=L/2+Lcut/2; dright[iright]=L/2+Lcut/2; dtop[itop]=L/2+Lcut/2; dbot[ibot]=L/2+Lcut/2;

Printf("-----------------------------------------------------------");
For i In {0:ileft}
	Printf("Left: %g  %g ",left[i], dleft[i]);
EndFor

For i In {0:iright}
	Printf("right: %g  %g",right[i], dright[i]);
EndFor

For i In {0:itop}
	Printf("Top: %g   %g",top[i], dtop[i]);
EndFor

For i In {0:ibot}
	Printf("Bot: %g  %g",bot[i], dbot[i]);
EndFor

Printf("-----------------------------------------------------------");

For i In {0: ileft}
	For j In {i:ileft}
		If (dleft[j]<dleft[i])
			temp=left[i];
			left[i]=left[j];
			left[j]=temp;
			
			dtemp=dleft[i];
			dleft[i]=dleft[j];
			dleft[j]=dtemp;
		EndIf
	EndFor
EndFor


For i In {0: iright}
	For j In {i:iright}
		If (dright[j]<dright[i])
			temp=right[i];
			right[i]=right[j];
			right[j]=temp;
			
			dtemp=dright[i];
			dright[i]=dright[j];
			dright[j]=dtemp;
		EndIf
	EndFor
EndFor

For i In {0: itop}
	For j In {i:itop}
		If (dtop[j]<dtop[i])
			temp=top[i];
			top[i]=top[j];
			top[j]=temp;

			dtemp=dtop[i];
			dtop[i]=dtop[j];
			dtop[j]=dtemp;
		EndIf
	EndFor
EndFor

For i In {0: ibot}
	For j In {i:ibot}
		If (dbot[j]<dbot[i])
			temp=bot[i];
			bot[i]=bot[j];
			bot[j]=temp;

			dtemp=dbot[i];
			dbot[i]=dbot[j];
			dbot[j]=dtemp;
		EndIf
	EndFor
EndFor

Printf("-----------------------------------------------------------");
For i In {0:ileft}
	Printf("Left: %g  %g ",left[i], dleft[i]);
EndFor

For i In {0:iright}
	Printf("right: %g  %g",right[i], dright[i]);
EndFor

For i In {0:itop}
	Printf("Top: %g   %g",top[i], dtop[i]);
EndFor

For i In {0:ibot}
	Printf("Bot: %g  %g",bot[i], dbot[i]);
EndFor


comm=0;
For i In {0:ibot-1}
	If (i%2==0)
		s=newreg; Line(s)={bot[i], bot[i+1]};
		phybot[comm]=s; comm+=1;
		lloop[icar]=s; 
		icar+=1;
	EndIf
EndFor

comm=0;
For i In {0:iright-1}
	If (i%2==0)
		s=newreg; Line(s)={right[i], right[i+1]};
		phyright[comm]=s; comm+=1;
		lloop[icar]=s; 
		icar+=1;
	EndIf
EndFor

comm=0;

For i In {0:itop-1}
	If (i%2==0)
		s=newreg; Line(s)={top[i], top[i+1]};
		phytop[comm]=s; comm+=1;
		lloop[icar]=-s; 
		icar+=1;
	EndIf
EndFor

comm=0;
For i In {0:ileft-1}
	If (i%2==0)
		s=newreg; Line(s)={left[i], left[i+1]};
		phyleft[comm]=s; comm+=1;
		lloop[icar]=-s; Printf("SS1 bot: %g %g", Pi*r*r, r*r*alpha-su);
		icar+=1;
	EndIf
EndFor


cir[0]=newreg; Line Loop(cir[0])={lloop[]};
Plane Surface(11)={cir[]};

Physical Line(1)={phybot[]};
Physical Line(2)={phyright[]};
Physical Line(3)={phytop[]};
Physical Line(4)={phyleft[]};

Physical Surface(11)={11}; 
//Recombine Surface {11};

Translate {-L/2+Lcut/2, -L/2+Lcut/2, 0} {
  Surface{11};
}
