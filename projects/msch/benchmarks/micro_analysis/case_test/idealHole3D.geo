l = 1;
r = 0.3*l;
lsc = 0.1*l;
c = l/2;

Point(1) = {0,0,0,lsc};
Point(2) = {l,0,0,lsc};
Point(3) = {l,l,0,lsc};
Point(4) = {0,l,0,lsc};

Point(5) = {c,c,0,lsc};
Point(6) = {c-r/Sqrt(2),c-r/Sqrt(2),0,lsc};
Point(7) = {c+r/Sqrt(2),c-r/Sqrt(2),0,lsc};
Point(8) = {c+r/Sqrt(2),c+r/Sqrt(2),0,lsc};
Point(9) = {c-r/Sqrt(2),c+r/Sqrt(2),0,lsc};
Line(1) = {4, 3};
Line(2) = {3, 2};
Line(3) = {2, 1};
Line(4) = {1, 4};
Circle(5) = {9, 5, 6};
Circle(6) = {9, 5, 8};
Circle(7) = {8, 5, 7};
Circle(8) = {7, 5, 6};
Line(9) = {6, 1};
Line(10) = {7, 2};
Line(11) = {8, 3};
Line(12) = {9, 4};
Line Loop(13) = {12, 1, -11, -6};
Plane Surface(14) = {13};
Line Loop(15) = {11, 2, -10, -7};
Plane Surface(16) = {15};
Line Loop(17) = {8, 9, -3, -10};
Plane Surface(18) = {17};
Line Loop(19) = {5, 9, 4, -12};
Plane Surface(20) = {19};
Extrude {0, 0, l} {
  Surface{20, 14, 16, 18};
}
Transfinite Line {36, 45, 1, 54, 67, 2, 76, 3, 90, 32, 24, 4, 27, 58, 80, 28} = 10 Using Progression 1;
Transfinite Line {25, 12, 11, 46, 10, 68, 9, 23} = 10 Using Progression 1;
Transfinite Line {6, 47, 7, 69, 5, 22, 8, 88} = 10 Using Progression 1;
Transfinite Surface {95};
Transfinite Surface {29};
Transfinite Surface {63};
Transfinite Surface {85};
Transfinite Surface {59};
Transfinite Surface {41};
Transfinite Surface {33};
Transfinite Surface {81};
Transfinite Surface {77};
Transfinite Surface {55};
Transfinite Surface {37};
Transfinite Surface {103};
Transfinite Surface {103};
Transfinite Surface {42};
Transfinite Surface {64};
Transfinite Surface {86};
Transfinite Surface {108};
Transfinite Surface {14};
Transfinite Surface {20};
Transfinite Surface {18};
Transfinite Surface {16};
Recombine Surface {86, 64, 108, 42, 59, 77, 85, 55, 16, 63, 81, 14, 41, 95, 103, 29, 37, 18, 20, 33};

Transfinite Volume {1};
Transfinite Volume {2};
Transfinite Volume {3};
Transfinite Volume {4};
Physical Volume(5) = {1,2,3,4};

Physical Surface(109) = {20, 14, 16, 18};
Physical Surface(110) = {64, 42, 108, 86};
Physical Surface(111) = {77};
Physical Surface(112) = {37};
Physical Surface(113) = {103};
Physical Surface(114) = {55};
Translate {-0.5*l, -0.5*l, -0.5*l} {
  Volume{2, 1, 3, 4};
}
