Point(1) = {0, 0, 0, 1.0};
Point(2) = {0, 1, 0, 1.0};
Point(3) = {0, 1, 1, 1.0};
Point(4) = {0, 0, 1, 1.0};
Line(1) = {4, 3};
Line(2) = {3, 2};
Line(3) = {2, 1};
Line(4) = {1, 4};
Line Loop(5) = {4, 1, 2, 3};
Plane Surface(6) = {5};
Extrude {1, 0, 0} {
  Surface{6};
}
Surface Loop(29) = {23, 6, 15, 19, 28, 27};
Physical Volume(30) = {1};
Physical Surface(31) = {6};
Physical Surface(32) = {28};
Transfinite Line {8, 9, 10, 11, 4, 3, 2, 1} = 2 Using Progression 1;
Transfinite Line {13, 14, 22, 18} = 3 Using Progression 1;
Transfinite Surface {28};
Transfinite Surface {6};
Transfinite Surface {23};
Transfinite Surface {19};
Transfinite Surface {15};
Transfinite Surface {27};
Recombine Surface {28, 15, 19, 27, 6, 23};
Transfinite Volume {1};
