#coding-Utf-8-*-
from gmshpy import *
from msch import*

#script to launch PBC problem with a python script

#DEFINE MICRO PROBLEM

# micro-material law
lawnum = 11 # unique number of law
K = 76.E3 	# Bulk mudulus
mu =26.E3	  # Shear mudulus
rho = 2700. # Bulk mass
sy0 = 300
h = 0.01*sy0
E = 9.*K*mu/(3.*K+mu)
nu= (3.*K -2.*mu)/2./(3.*K+mu)

# micro-geometry
micromeshfile="micro.msh" # name of mesh file


# creation of material law
law1 = FSElasticMaterialLaw(lawnum,1,K,mu,rho)
#law1 = FSJ2LinearMaterialLaw(lawnum,E,nu,sy0,h,rho)

# creation of  micro part Domain
nfield = 5 # number of the field (physical number of entity)
dim =3
myfield1 = FSDomain(1000,nfield,lawnum,dim)

# periodiodic BC
method = 0 # Periodic mesh = 0 Langrange interpolation = 1 Cubic spline interpolation =2
degree = 5 
addVertex = 0

nstepmicro = 1  # number of step (used only if soltype=1)
tolmicro=1.e-6  # relative tolerance for NR scheme (used only if soltype=1)

# DEFINE MACROPROBLEM
matnum = 1;
macromat = hoDGMultiscaleMaterialLaw(matnum , 1000)
macromat.loadModel(micromeshfile);
macromat.addDomain(myfield1)
macromat.addMaterialLaw(law1);

macromat.setPeriodicity(1.,0,0,"x")
macromat.setPeriodicity(0,1.,0,"y")
macromat.setPeriodicity(0,0,1.,"z")

macromat.addPeriodicBC(1000,109,111,113,110,112,114)
macromat.setPeriodicBCOptions(method, degree,addVertex)
macromat.setViewAllMicroProblems(0)
macromat.setOrder(2)
macromat.setAverageStressMethod(1)
macromat.setNumStep(1)
macromat.setTolerance(1e-6)
macromat.setSystemType(1)
macromat.Solver(2)
macromat.Scheme(1)


macromeshfile="macro.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)


# creation of  macro part Domain
fulldg= 1
beta1  = 10
bulkpert =0
interpert =0
perturbation = 1e-6

nfield = 30 # number of the field (physical number of entity)
dim =3
macrodomain =hoDGDomain(1000,nfield,0,matnum,fulldg)
macrodomain.stabilityParameters(beta1)
macrodomain.matrixByPerturbation(bulkpert,interpert,interpert,perturbation)
#macrodomain.gaussIntegration(hoDGDomain.Gauss,2,2)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(macromeshfile)
mysolver.addDomain(macrodomain)
mysolver.addMaterialLaw(macromat)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)


# boundary condition
mysolver.displacementBC("Face",31,0,0.0)
mysolver.displacementBC("Face",31,1,0.0)
mysolver.displacementBC("Face",31,2,0.0)

mysolver.displacementBC("Face",32,0,0.0)
mysolver.displacementBC("Face",32,1,0.05)
mysolver.displacementBC("Face",32,2,0.0)

#mysolver.forceBC("Face",32,2,1)

# archivage
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GLSTRAINEQUIVALENT, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

mysolver.internalPointBuildView("High-order strain norm",IPField.HIGHSTRAIN_NORM, 1, 1);
mysolver.internalPointBuildView("High-order stress norm",IPField.HIGHSTRESS_NORM, 1, 1);
mysolver.internalPointBuildView("Green -Lagrange strain norm",IPField.GL_NORM, 1, 1);
mysolver.internalPointBuildView("PK2 stress norm",IPField.STRESS_NORM, 1, 1);

mysolver.archivingForceOnPhysicalGroup("Face",32,0)
mysolver.archivingForceOnPhysicalGroup("Face",32,1)
mysolver.archivingForceOnPhysicalGroup("Face",32,2)

# solve
mysolver.solve()
