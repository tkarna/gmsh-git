
mm=1.0;
n=2; 
m = 2;
L=n*mm;
H = m*mm; 
sl1=0.5*L/n;
Point(1)={0,0,0,sl1};
Point(2)={L,0,0,sl1};
Point(3)={L,H,0,sl1};
Point(4)={0,H,0,sl1};
Line(1)={1,2};
Line(2)={2,3};
Line(3)={3,4};
Line(4)={4,1};

l[0]=newreg;
Line Loop(l[0])={1,2,3,4};
Plane Surface(11)={l[]};
Physical Line(1)={1}; 
Physical Line(2)={2};
Physical Line(3)={3};
Physical Line(4)={4}; 
Physical Surface(11)={11};
Physical Point(1)={1}; 
Physical Point(2)={2};
Physical Point(3)={3};
Physical Point(4)={4}; 
Transfinite Line {3, 1} = 3 Using Progression 1;
Transfinite Line {2, 4} = 3 Using Progression 1;
Transfinite Surface {11};
Recombine Surface {11};
