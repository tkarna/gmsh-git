clear all
close all
clc

l =[1e-3
5.e-3
1.e-2
2.e-2
5.e-2
0.1
0.2
0.4
0.7
1];

F = [
1.78E+000
1.840411e+00
1.94E+000
2.135890e+00
2.622850e+00
3.48E+000
5.92E+000
1.40E+001
3.32E+001
5.86E+001
];

h = 1


K1 = 76E3;
mu1 =26E3;
E1 = 9.*K1*mu1/(3.*K1+mu1);
nu1= (3.*K1 -2.*mu1)/2./(3.*K1+mu1);
d=0.1;
L=10*h;
I = h^4/12;

l=L./l;
F = 3.*E1*I/L^3*d./F;


f=figure(1)
loglog(l,F,'*--');
xlabel('L/l');
ylabel('(3EI \delta )/(L^3F)')
legend('L/h = 10','Location',4)
W=4; B=3;
set (f,'papertype', '<custom>')
set (f,'paperorientation', 'landscape')
set (f,'paperunits','inches');
set (f,'papersize',[W B])
set (f,'paperposition', [0,0,[W B]])
set (f,'defaultaxesposition', [0.15*B, 0.15*B, 0.75*W, 0.75*W])
set (0,'defaultaxesfontsize', 16)

print -deps figure.eps
system('epstopdf figure.eps')



