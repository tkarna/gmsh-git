#coding-Utf-8-*-
from gmshpy import *
from msch import*

#script to launch PBC problem with a python script

# macro-material law
lawnum1 = 11 # unique number of law
K1 = 76E3 	# Bulk mudulus
mu1 =26E3  # Shear mudulus
rho1 = 0.1e-3 # Bulk mass
sy01 = 400e10
h1 = 0.5*sy01
E1 = 9.*K1*mu1/(3.*K1+mu1)
nu1= (3.*K1 -2.*mu1)/2./(3.*K1+mu1)

# creation of material law
#law1 = FSElasticMaterialLaw(lawnum1,1,rho1)
#law1.setParameter("YOUNG_MODULUS",E1)
#law1.setParameter("POISSON_RATIO",nu1)
#law1 = FSJ2LinearMaterialLaw(lawnum1,E1,nu1,sy01,h1,rho1)

law1 = J2LinearDG3DMaterialLaw(lawnum1,rho1,E1,nu1,sy01,h1)


# micro-geometry
meshfile="flexion.msh" # name of mesh file


# creation of  micro part Domain

nfield1 = 31 # number of the field (physical number of entity)
#myfield1 = FSDomain(1000,nfield1,lawnum1,2)

fulldg= 1
beta1  = 100

myfield1 = dG3DDomain(1000,nfield1,0,lawnum1,fulldg,3)
myfield1.stabilityParameters(beta1)


nfield2 = 55 # number of the field (physical number of entity)
#myfield2 = FSDomain(1000,nfield2,lawnum1,2)

myfield2 = dG3DDomain(1000,nfield2,0,lawnum1,fulldg,3)
myfield2.stabilityParameters(beta1)


interdomain = interDomainBetween3D(1000,myfield1,myfield2)
interdomain.stabilityParameters(beta1)

#
#
# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 20  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=5.e-5  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)



# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addMaterialLaw(law1)
mysolver.addDomain(myfield1)
mysolver.addDomain(myfield2)
mysolver.addDomain(interdomain)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
#mysolver.snlManageTimeStep(50,2, 2., 15);
#mysolver.explicitSpectralRadius(ftime,0.1,0.)
#mysolver.dynamicRelaxation(0.1, ftime, 1.e-3,2)
#mysolver.explicitTimeStepEvaluation(nstep)
mysolver.pathFollowing(0)
mysolver.setPathFollowingControlType(0)
#mysolver.lineSearch(0)

# boundary condition

mysolver.displacementBC("Volume",55,2,0)
mysolver.displacementBC("Volume",55,1,0)
mysolver.displacementBC("Volume",55,0,0)

mysolver.displacementBC("Face",30,1,3)


# archivage
mysolver.internalPointBuildView("F_xx",IPField.F_XX, 1, 1);
mysolver.internalPointBuildView("F_xy",IPField.F_XY, 1, 1);
mysolver.internalPointBuildView("F_xz",IPField.F_XZ, 1, 1);
mysolver.internalPointBuildView("F_yx",IPField.F_YX, 1, 1);
mysolver.internalPointBuildView("F_yy",IPField.F_YY, 1, 1);
mysolver.internalPointBuildView("F_yz",IPField.F_YZ, 1, 1);
mysolver.internalPointBuildView("F_zx",IPField.F_ZX, 1, 1);
mysolver.internalPointBuildView("F_zy",IPField.F_ZY, 1, 1);
mysolver.internalPointBuildView("F_zz",IPField.F_ZZ, 1, 1);

mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GLSTRAINEQUIVALENT, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

mysolver.internalPointBuildView("strain_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("strain_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("strain_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("strain_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("strain_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("strain_xz",IPField.STRAIN_XZ, 1, 1);

mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);

mysolver.internalPointBuildView("High-order strain norm",IPField.HIGHSTRAIN_NORM, 1, 1);
mysolver.internalPointBuildView("High-order stress norm",IPField.HIGHSTRESS_NORM, 1, 1);
mysolver.internalPointBuildView("Green -Lagrange strain norm",IPField.GL_NORM, 1, 1);

mysolver.archivingForceOnPhysicalGroup("Face",30,1)
mysolver.archivingNodeDisplacement(32,1,1)

# solve
mysolver.solve()
