mm =1.;
l = 1*mm;
L = 20*l;
lsca = 0.1*l;

Point(1) = {0, 0, 0, lsca};
Point(2) = {l, 0, 0, lsca};
Point(3) = {l, l, 0, lsca};
Point(4) = {0, l, 0, lsca};


Line(1) = {4, 3};
Line(2) = {3, 2};
Line(3) = {2, 1};
Line(4) = {1, 4};
Line Loop(5) = {1, 2, 3, 4};
Plane Surface(6) = {5};
Extrude {0, 0, L} {
  Surface{6};
}

Physical Surface(29) = {6};
Physical Surface(30) = {28};
Physical Volume(31) = {1};
Physical Point(32) = {6};
Extrude {0, 0, -1} {
  Surface{6};
}
Physical Volume(55) = {2};

Transfinite Line {39, 40, 34, 37, 36, 35, 48, 44, 3, 2, 4, 1, 11, 9, 8, 10, 10} = 3 Using Progression 1;
Transfinite Line {14, 18, 13, 22} = 16 Using Progression 1;

Transfinite Surface {41, 45, 54, 6, 23, 49, 53, 19, 15, 28, 27};

Transfinite Volume{1, 2};



