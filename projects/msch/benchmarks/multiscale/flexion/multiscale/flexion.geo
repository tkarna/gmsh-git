Point(1) = {0, 0, 0, 1.0};
Point(2) = {0, 0.5, 0, 1.0};
Point(3) = {0, 1, 0, 1.0};
Point(4) = {10, 0, 0, 1.0};
Point(5) = {10, 0.5, 0, 1.0};
Point(6) = {10, 1, 0, 1.0};
Line(1) = {1, 4};
Line(2) = {2, 5};
Line(3) = {3, 6};
Line(4) = {1, 2};
Line(5) = {2, 3};
Line(6) = {4, 5};
Line(7) = {5, 6};
Line Loop(8) = {3, -7, -2, 5};
Plane Surface(9) = {8};
Line Loop(10) = {2, -6, -1, 4};
Plane Surface(11) = {10};
Extrude {0, 0, 1} {
  Surface{9, 11};
}
Transfinite Line {3, 13, 2, 15, 1, 37} = 10 Using Progression 1;
Transfinite Line {19, 23, 45, 18, 27, 49} = 3 Using Progression 1;
Transfinite Line {5, 16, 4, 38, 7, 14, 6, 36} = 2 Using Progression 1;
Transfinite Surface {32, 54, 33, 55, 20, 28, 50, 9, 24, 11, 46};
Recombine Surface {32, 54, 33, 55, 20, 28, 50, 9, 24, 11, 46};
Transfinite Volume {1, 2};
Physical Volume(56) = {1,2};

Physical Surface(58) = {32, 54};
Physical Surface(59) = {24, 46};
Physical Line(60) = {3};
Physical Line(61) = {2};
Physical Line(62) = {1};
Physical Line(63) = {19};
