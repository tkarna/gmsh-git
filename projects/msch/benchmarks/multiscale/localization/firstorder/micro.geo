
mm = 1.e-3;
n=1;
L=1*mm;
sl1=0.3*L/n;
Point(1)={0,0,0,1.5*sl1};
Point(2)={L,0,0,sl1};
Point(3)={L,L,0,0.5*sl1};
Point(4)={0,L,0,sl1};
Line(1)={1,2};
Line(2)={2,3};
Line(3)={3,4};
Line(4)={4,1};
x0=0.5*L/n; y0=0.5*L/n; r=0.1*L/n; 
num=0;
moyen=0;
surf=0;
For i In {0:n-1}
  	For j In {0:n-1}
		x=x0*(2*i+1);
		y=y0*(2*j+1);
		sl2=0.7*sl1;
		moyen=moyen+r;
		surf=surf+Pi*r*r;

		p1=newp; Point(p1)={x-r,y,0,sl2};
		p2=newp; Point(p2)={x,y+r,0,sl2};
		p3=newp; Point(p3)={x+r,y,0,sl2};
		p4=newp; Point(p4)={x,y-r,0,sl2};
		pc=newp; Point(pc)={x,y,0,sl2};

		c1 = newreg; Circle(c1) = {p1,pc,p2};
		c2 = newreg; Circle(c2) = {p2,pc,p3};
		c3 = newreg; Circle(c3) = {p3,pc,p4};
		c4 = newreg; Circle(c4) = {p4,pc,p1};

		Transfinite Line {c3, c2, c4, c1} = 6 Using Progression 1;
		num+=1;
		l[num]=newreg; Line Loop(l[num]) = {c1,c2,c3,c4}; 
	EndFor	
EndFor
l[0]=newreg;
Line Loop(l[0])={1,2,3,4};
Plane Surface(11)={l[]};
Physical Line(1)={1}; 
Physical Line(2)={2};
Physical Line(3)={3};
Physical Line(4)={4}; 
Physical Surface(11)={11};
Physical Point(1)={1}; 
Physical Point(2)={2};
Physical Point(3)={3};
Physical Point(4)={4}; 
Transfinite Line {3, 2, 4, 1} = 10*n Using Progression 1;
//Transfinite Surface{11};
//Recombine Surface{11};
/*
Translate {-L/2, -L/2, 0} {
  Surface{11};
}
*/
