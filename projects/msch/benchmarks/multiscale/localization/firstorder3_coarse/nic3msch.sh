#!/bin/sh

# script example for nic3.ulg.ac.be
# the #dollar are pseudo command for qsub so they have to be edited !!


#$ -N multiscaleTest_L1
#$ -m beas
#$ -M vandung.nguyen@ulg.ac.be
#$ -l highmem=true
#$ -l h_vmem=2000M
#$ -l h_rt=120:00:00
#$ -pe openmpi 20
#$ -cwd
#$ -j yes

source /etc/profile.d/modules.sh 
source /etc/profile.d/segi.sh 

module add openmpi/gcc/64/1.3.0
subdir=$HOME/gmsh/projects/msch/benchmarks/multiscale/localization/firstorder3_coarse
echo "subdir=" $subdir

workdir=/tmpscratch/${USER}_$JOB_ID
echo "workdir=" $workdir

pyfile=multiscale.py
mshfile=macro.msh
rvemsh=micro.msh

echo "cat \$TMPDIR/machines | uniq"
cat $TMPDIR/machines | uniq

for ii in `cat $TMPDIR/machines | uniq`
	do
		echo $ii
		ssh $ii mkdir $workdir 
		ssh $ii cp $subdir/*.py $workdir/ 
		ssh $ii cp $subdir/$mshfile $workdir/
                ssh $ii cp $subdir/$rvemsh $workdir/ 
	done

cd $workdir # needed to work in the tmpscrach dir otherwise work on home !!
#ls -artl
export PETSC_DIR=$HOME/petsc-3.3-p5
export PETSC_ARCH=linux-gnu-c-opt
export SLEPC_DIR=$HOME/slepc-3.3-p3
export NLSMPIINC=/cvos/shared/apps/openmpi/gcc/64/1.6.2/include
export LD_PRELOAD=$LD_PRELOAD:/cvos/shared/apps/openmpi/gcc/64/1.6.2/lib/libmpi.so
export PATH=/cvos/shared/apps/openmpi/gcc/64/1.6.2/bin:$HOME/gmsh/projects/msch/build/dG3D/NonLinearSolver/gmsh:$PETSC_DIR/$PETSC_ARCH/lib:$PETSC_DIR/$PETSC_ARCH/include:$PETSC_DIR/$PETSC_ARCH/bin:$PATH
export PYTHONPATH=$HOME/gmsh/projects/msch/build:$HOME/gmsh/projects/msch/build/dG3D:$HOME/gmsh/projects/msch/build/dG3D/NonLinearSolver/gmsh/wrappers:$PYTHONPATH
                                                                                                                                
mpiexec -n 20 python  $workdir/$pyfile >& $workdir/output.txt

echo -e "\n"

sleep 5

for ii in `tac $TMPDIR/machines | uniq`
  do
     ssh $ii mv $workdir/output.txt $subdir/output_$ii.txt
     ssh $ii mv $workdir/previousScheme $subdir/previousScheme_$ii # if a previous scheme exist
     ssh $ii mv $workdir/* $subdir/ # copy archiving files
     ssh $ii rm -rf $workdir
  done
echo -e "\n"
