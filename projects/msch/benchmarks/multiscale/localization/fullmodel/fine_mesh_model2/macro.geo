
mm = 1.;
n=1;
L=1*mm;
sl1=0.1*L/n;
Point(1)={0,0,0,sl1};
Point(2)={L,0,0,sl1};
Point(3)={L,L,0,sl1};
Point(4)={0,L,0,sl1};
Line(1)={1,2};
Line(2)={2,3};
Line(3)={3,4};
Line(4)={4,1};
x0=0.5*L/n; y0=0.5*L/n; r=0.2*L/n; 
num=0;
moyen=0;
surf=0;
For i In {0:n-1}
  	For j In {0:n-1}
		x=x0*(2*i+1);
		y=y0*(2*j+1);
		sl2=0.7*sl1;
		moyen=moyen+r;
		surf=surf+Pi*r*r;

		p1=newp; Point(p1)={x-r,y,0,sl2};
		p2=newp; Point(p2)={x,y+r,0,sl2};
		p3=newp; Point(p3)={x+r,y,0,sl2};
		p4=newp; Point(p4)={x,y-r,0,sl2};
		pc=newp; Point(pc)={x,y,0,sl2};

		c1 = newreg; Circle(c1) = {p1,pc,p2};
		c2 = newreg; Circle(c2) = {p2,pc,p3};
		c3 = newreg; Circle(c3) = {p3,pc,p4};
		c4 = newreg; Circle(c4) = {p4,pc,p1};
		num+=1;
		l[num]=newreg; Line Loop(l[num]) = {c1,c2,c3,c4}; 
	EndFor	
EndFor
l[0]=newreg;
Line Loop(l[0])={1,2,3,4};
Plane Surface(11)={l[]};
Coherence;

For i In {1:5}
	Translate {i*L, 0, 0} {
		Duplicata { Surface{11}; }
	}
EndFor

For i In {1:5}
	Translate {0, i*L, 0} {
		Duplicata { Surface{11, 12, 21, 30, 39, 48}; }
	}
EndFor


Physical Line(182) = {277, 223, 169, 115, 61, 4};
Physical Line(183) = {1, 13, 22, 31, 40, 49};
Physical Line(184) = {276, 285, 294, 303, 312, 321};
Physical Surface(185) = {11};
Physical Surface(186) = {57, 66, 12};
Physical Surface(187) = {111, 120, 129, 75, 21};
Physical Surface(188) = {30, 84, 138, 192, 183, 174, 165};
Physical Surface(189) = {219, 228, 237, 246, 255, 201, 147, 93, 39};
Physical Surface(190) = {48, 102, 156, 210, 264, 318, 309, 300, 291, 282, 273};
