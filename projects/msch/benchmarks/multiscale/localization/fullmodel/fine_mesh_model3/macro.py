#coding-Utf-8-*-
from gmshpy import *
from msch import*

#script to launch PBC problem with a python script

# macro-material law

E1 =68.9E3
nu1= 0.33
rho1 = 2.7e-3

sy01 = 276
h1 = 0.1*sy01

# creation of material law
lawnum1 = 11 # unique number of law
law1 = FSJ2LinearMaterialLaw(lawnum1,E1,nu1,0.75*sy01,h1,rho1)

lawnum2 = 12 # unique number of law
law2 = FSJ2LinearMaterialLaw(lawnum2,E1,nu1,0.8*sy01,h1,rho1)

lawnum3 = 13 # unique number of law
law3 = FSJ2LinearMaterialLaw(lawnum3,E1,nu1,0.85*sy01,h1,rho1)

lawnum4 = 14 # unique number of law
law4 = FSJ2LinearMaterialLaw(lawnum4,E1,nu1,0.9*sy01,h1,rho1)

lawnum5 = 15 # unique number of law
law5 = FSJ2LinearMaterialLaw(lawnum5,E1,nu1,0.95*sy01,h1,rho1)

lawnum6 = 16 # unique number of law
law6 = FSJ2LinearMaterialLaw(lawnum6,E1,nu1,sy01,h1,rho1)


# micro-geometry
meshfile="macro.msh" # name of mesh file

nfield1 = 185 # number of the field (physical number of entity)
myfield1 = FSDomain(1000,nfield1,lawnum1,2)

nfield2 = 186 # number of the field (physical number of entity)
myfield2 = FSDomain(1000,nfield2,lawnum2,2)

nfield3 = 187 # number of the field (physical number of entity)
myfield3 = FSDomain(1000,nfield3,lawnum3,2)

nfield4 = 188 # number of the field (physical number of entity)
myfield4 = FSDomain(1000,nfield4,lawnum4,2)

nfield5 = 189 # number of the field (physical number of entity)
myfield5 = FSDomain(1000,nfield5,lawnum5,2)

nfield6 = 190 # number of the field (physical number of entity)
myfield6 = FSDomain(1000,nfield6,lawnum6,2)






# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 200  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=5.e-5  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)



# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addMaterialLaw(law1)
mysolver.addMaterialLaw(law2)
mysolver.addMaterialLaw(law3)
mysolver.addMaterialLaw(law4)
mysolver.addMaterialLaw(law5)
mysolver.addMaterialLaw(law6)

mysolver.addDomain(myfield1)
mysolver.addDomain(myfield2)
mysolver.addDomain(myfield3)
mysolver.addDomain(myfield4)
mysolver.addDomain(myfield5)
mysolver.addDomain(myfield6)

mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
mysolver.pathFollowing(0)
mysolver.setPathFollowingControlType(0)
mysolver.lineSearch(1)

# boundary condition

#mysolver.constraintBC("Edge",184,1)
mysolver.displacementBC("Edge",183,1,0)
mysolver.displacementBC("Edge",182,0,0)

#mysolver.forceBC("Edge",184,1,1.e3)
mysolver.displacementBC("Edge",184,1,5e-2)

# archivage
mysolver.internalPointBuildView("F_xx",IPField.F_XX, 1, 1);
mysolver.internalPointBuildView("F_xy",IPField.F_XY, 1, 1);
mysolver.internalPointBuildView("F_xz",IPField.F_XZ, 1, 1);
mysolver.internalPointBuildView("F_yx",IPField.F_YX, 1, 1);
mysolver.internalPointBuildView("F_yy",IPField.F_YY, 1, 1);
mysolver.internalPointBuildView("F_yz",IPField.F_YZ, 1, 1);
mysolver.internalPointBuildView("F_zx",IPField.F_ZX, 1, 1);
mysolver.internalPointBuildView("F_zy",IPField.F_ZY, 1, 1);
mysolver.internalPointBuildView("F_zz",IPField.F_ZZ, 1, 1);

mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GLSTRAINEQUIVALENT, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

mysolver.internalPointBuildView("strain_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("strain_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("strain_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("strain_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("strain_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("strain_xz",IPField.STRAIN_XZ, 1, 1);

mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);

mysolver.internalPointBuildView("High-order strain norm",IPField.HIGHSTRAIN_NORM, 1, 1);
mysolver.internalPointBuildView("High-order stress norm",IPField.HIGHSTRESS_NORM, 1, 1);
mysolver.internalPointBuildView("Green -Lagrange strain norm",IPField.GL_NORM, 1, 1);

mysolver.archivingForceOnPhysicalGroup("Edge",184,1)
mysolver.archivingNodeDisplacement(191,1,1)

# solve
mysolver.solve()
