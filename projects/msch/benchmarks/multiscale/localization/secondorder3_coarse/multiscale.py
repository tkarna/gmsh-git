#coding-Utf-8-*-
from gmshpy import *
from msch import*

#script to launch PBC problem with a python script

#DEFINE MICRO PROBLEM


import nummat1
import nummat2
import nummat3
import nummat4
import nummat5
import nummat6

macromeshfile="macro.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1000  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-3  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)


# creation of  macro part Domain
nfield1 = 185 # number of the field (physical number of entity)
dim =2
fulldg= 1
beta1  = 10

field1 = hoDGDomain(1000,nfield1,0,nummat1.matnum,fulldg,dim)
field1.stabilityParameters(beta1)

nfield2 = 186
field2 = hoDGDomain(1000,nfield2,0,nummat2.matnum,fulldg,dim)
field2.stabilityParameters(beta1)

nfield3 = 187
field3 = hoDGDomain(1000,nfield3,0,nummat3.matnum,fulldg,dim)
field3.stabilityParameters(beta1)

nfield4 = 188
field4 = hoDGDomain(1000,nfield4,0,nummat4.matnum,fulldg,dim)
field4.stabilityParameters(beta1)

nfield5 = 189
field5 = hoDGDomain(1000,nfield5,0,nummat5.matnum,fulldg,dim)
field5.stabilityParameters(beta1)

nfield6 = 190
field6 = hoDGDomain(1000,nfield6,0,nummat6.matnum,fulldg,dim)
field6.stabilityParameters(beta1)

myfieldinter1 = hoDGInterDomain(1000,field1,field2)
myfieldinter1.stabilityParameters(beta1)

myfieldinter2 = hoDGInterDomain(1000,field2,field3)
myfieldinter2.stabilityParameters(beta1)

myfieldinter3 = hoDGInterDomain(1000,field3,field4)
myfieldinter3.stabilityParameters(beta1)

myfieldinter4 = hoDGInterDomain(1000,field4,field5)
myfieldinter4.stabilityParameters(beta1)

myfieldinter5 = hoDGInterDomain(1000,field5,field6)
myfieldinter5.stabilityParameters(beta1)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(macromeshfile)
mysolver.addDomain(field1)
mysolver.addDomain(field2)
mysolver.addDomain(field3)
mysolver.addDomain(field4)
mysolver.addDomain(field5)
mysolver.addDomain(field6)

mysolver.addDomain(myfieldinter1)
mysolver.addDomain(myfieldinter2)
mysolver.addDomain(myfieldinter3)
mysolver.addDomain(myfieldinter4)
mysolver.addDomain(myfieldinter5)

mysolver.addMaterialLaw(nummat1.mat)
mysolver.addMaterialLaw(nummat2.mat)
mysolver.addMaterialLaw(nummat3.mat)
mysolver.addMaterialLaw(nummat4.mat)
mysolver.addMaterialLaw(nummat5.mat)
mysolver.addMaterialLaw(nummat6.mat)

mysolver.setMultiscaleFlag(1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)


# boundary condition

mysolver.displacementBC("Face",185,2,0)
mysolver.displacementBC("Face",186,2,0)
mysolver.displacementBC("Face",187,2,0)
mysolver.displacementBC("Face",188,2,0)
mysolver.displacementBC("Face",189,2,0)
mysolver.displacementBC("Face",190,2,0)

mysolver.displacementBC("Edge",183,1,0.0)
mysolver.displacementBC("Edge",182,0,0.0)

mysolver.displacementBC("Edge",184,1,5.e-2)


# archivage
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GLSTRAINEQUIVALENT, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

mysolver.archivingForceOnPhysicalGroup("Edge",184,1)
mysolver.archivingNodeDisplacement(191,1,1)

# solve
mysolver.solve()

