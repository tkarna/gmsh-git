
mm = 1.;

d = 1*mm;
lsca = 0.1*mm;

n = 5;

Point(1) = {0,0,0,lsca};
Point(2) = {d,0,0,lsca};
Point(3) = {d,d,0,lsca};
Point(4) = {0,d,0,lsca};
Coherence;
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line Loop(5) = {4, 1, 2, 3};
Plane Surface(6) = {5};
For i In {1:n}
	Translate {i*d, 0, 0} {
		Duplicata { Surface{6}; }
	}
EndFor

For i In {1:n}
	Translate {0, i*d, 0} {
		Duplicata { Surface{6, 7, 12, 17, 22, 27}; }
	}
EndFor
Physical Line(182) = {153, 123, 93, 63, 33, 4};
Physical Line(183) = {1, 9, 14, 19, 24, 29};
Physical Line(184) = {156, 161, 166, 171, 176, 181};

Physical Surface(185) = {6};
Physical Surface(186) = {32, 37, 7};
Physical Surface(187) = {62, 67, 72, 42, 12};
Physical Surface(188) = {17, 47, 77, 107, 102, 97, 92};
Physical Surface(189) = {122, 127, 132, 137, 142, 112, 82, 52, 22};
Physical Surface(190) = {27, 57, 87, 117, 147, 177, 172, 167, 162, 157, 152};

Transfinite Line {153, 123, 93, 63, 33, 4, 156, 126, 155, 125, 96, 95, 66, 65, 36, 3, 35, 2, 1, 161, 160, 131, 101, 130, 71, 100, 41, 70, 40, 11, 10, 9, 166, 136, 165, 135, 106, 76, 105, 46, 75, 45, 16, 14, 15, 171, 170, 141, 140, 111, 110, 81, 80, 51, 50, 21, 20, 19, 176, 146, 175, 116, 145, 115, 86, 85, 56, 26, 55, 24, 25, 181, 180, 151, 150, 121, 120, 91, 90, 61, 31, 60, 30, 29} = 3 Using Progression 1;

Transfinite Surface {6};
Transfinite Surface {37, 7};
Transfinite Surface {62, 67, 72, 42, 12};
Transfinite Surface {17, 47, 77, 107, 102, 97, 92};
Transfinite Surface {122, 127, 132, 137, 142, 112, 82, 52, 22};
Transfinite Surface {27, 57, 87, 117, 147, 177, 172, 167, 162, 157, 152, 32};

//Recombine Surface {152, 122, 92, 62, 32, 6, 157, 127, 97, 67, 37, 7, 162, 132, 102, 72, 42, 12, 167, 137, 107, 77, 47, 17, 172, 142, 112, 82, 52, 22, 177, 147, 117, 87, 57, 27};
Physical Point(191) = {415};
