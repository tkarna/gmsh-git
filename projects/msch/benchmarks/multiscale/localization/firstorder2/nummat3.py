#coding-Utf-8-*-
from gmshpy import *
from msch import*

#DEFINE MULTISCALE MATERIAL LAW

# MATERIAL PROPERTIES
lawnum1 = 11 									# unique number of law
E1 	= 70.E3									# Young modulus
nu1 = 0.35											# Poisson ratio
K1 	= E1/3./(1.-2.*nu1)				# Bulk mudulus
mu1 = E1/2./(1.+nu1)	 			 	# Shear mudulus
rho1 = 2.7 										# Bulk mass
sy01 =0.4*100.										# Yield stress
h1 	=0.2*sy01										# Hardening coefficent

# MATERIAL LAW
# Elastic material
#type = 1  # 0-linear 1- NeoHookean 2-Bilogarithm
#law1 = FSElasticMaterialLaw(lawnum1,type,K1,mu1,rho1)  

# Elastoplastic material

law1 = FSJ2LinearMaterialLaw(lawnum1,E1,nu1,sy01,h1,rho1) # J2 linear material law

# micro-geometry
rve="micro.msh" # name of mesh file


# creation of  micro part Domain
nfield1 = 11 # number of the field (physical number of entity)
dim =2 			 # dimension of rve 2-two dimensional 3 -three dimentional

myfield1 = FSDomain(1000,nfield1,lawnum1,dim) #finite strain domain


# DEFINE MULTISCALE MATERIAL LAW
matnum = 3;
mat = FSMultiscaleMaterialLaw(matnum, 1000)

# add rve mesh
mat.loadModel(rve);

#	add domain
mat.addDomain(myfield1)

# add material law
mat.addMaterialLaw(law1);

# set periodic geometry 
mat.setPeriodicity(1.e-3,0,0,"x")
mat.setPeriodicity(0,1.e-3,0,"y")
mat.setPeriodicity(0,0,1.,"z")

# set linear displacement BC
#mat.addLinearDisplacementBC(1000,1,2,3,4)

# set minimal kinematical BC
#mat.addMinimalKinematicBC(1000,1,2,3,4)

# periodiodic BC
mat.addPeriodicBC(1000,1,2,3,4)
# method for PBC constraint
# 	0- Periodic mesh 
# 	1- Langrange interpolation 
# 	2- Cubic spline interpolation
method = 0 			
# polynomial degree for Lagrange or
# number of segment for cubic spline
degree = 5 			
# using boundary vertices for contraint
# 0- using exist vertices
# 1- using new vertices
addvertex = 0
mat.setPeriodicBCOptions(method, degree,addvertex)

# set number step 
mat.setNumStep(5)

# set tolerance for NR scheme
mat.setTolerance(1e-6)

# set view all micro problem
flag = 1 # 1-yes 0-no
gp = 0 # gauss point
mat.setViewAllMicroProblems(flag,gp) 

# set specific view to micro problem
ele = 0
gp  = 0
mat.addViewMicroSolver(ele,gp)

# set rve order 
mat.setOrder(1) # 1- first order 2- second order

# set average stress method
methodstress = 0  # 0- volume integral, 1 -surface integral
mat.setAverageStressMethod(methodstress) 

# set average tangent method
methodtangent = 0 # 0-perturbation 1-condensation
toltangent = 1.e-6
mat.setAverageTangentMethod(methodtangent,toltangent)

# set scheme
mat.Scheme(1) # 0 -linear elastic 1 - nonlinear

# set solver
mat.Solver(2) # 0 -GMM, 1- TAUSC 2-PETSc

# set system type
# 0- direct constrainte elimination 
# 1- direct multiplier elimination
# 2- using multipliers as unknowns
mat.setSystemType(1)

# set using iterative procedure
mat.iterativeProcedure(1);

# set using different stiffness matrix
mat.stiffnessModification(1)

# stability analysis
mat.setStabilityAnalysis(0);

# set computed eigen value
mat.setComputedEigenValue(10)

# set view mode
mat.setModeView(0)

# set perturbation flag
mat.setPerturbation(0)

# set perturbation fatcor
mat.setPerturbationFactor(0.01);

#
mat.setInstabilityCriterion(0.);


