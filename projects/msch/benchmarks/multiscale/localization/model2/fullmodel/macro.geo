Mesh.Algorithm=8;
Mesh.RecombineAll=1;
mm = 1.;
n=1;
L=1*mm;
sl1=0.1*L/n;
Point(1)={0,0,0,sl1};
Point(2)={L,0,0,sl1};
Point(3)={L,L,0,sl1};
Point(4)={0,L,0,sl1};
Line(1)={1,2};
Line(2)={2,3};
Line(3)={3,4};
Line(4)={4,1};
x0=0.5*L/n; y0=0.5*L/n; r=0.2*L/n; 
num=0;
moyen=0;
surf=0;
For i In {0:n-1}
  	For j In {0:n-1}
		x=x0*(2*i+1);
		y=y0*(2*j+1);
		sl2=0.5*r;
		moyen=moyen+r;
		surf=surf+Pi*r*r;

		p1=newp; Point(p1)={x-r,y,0,sl2};
		p2=newp; Point(p2)={x,y+r,0,sl2};
		p3=newp; Point(p3)={x+r,y,0,sl2};
		p4=newp; Point(p4)={x,y-r,0,sl2};
		pc=newp; Point(pc)={x,y,0,sl2};

		c1 = newreg; Circle(c1) = {p1,pc,p2};
		c2 = newreg; Circle(c2) = {p2,pc,p3};
		c3 = newreg; Circle(c3) = {p3,pc,p4};
		c4 = newreg; Circle(c4) = {p4,pc,p1};
		num+=1;
		l[num]=newreg; Line Loop(l[num]) = {c1,c2,c3,c4}; 
	EndFor	
EndFor
l[0]=newreg;
Line Loop(l[0])={1,2,3,4};
Plane Surface(11)={l[]};
Coherence;

For i In {1:2}
	Translate {i*L, 0, 0} {
		Duplicata { Surface{11}; }
	}
EndFor

For i In {1:2}
	Translate {0, i*L, 0} {
		Duplicata { Surface{11, 12, 21}; }
	}
EndFor


Physical Line(182) = {61, 34, 4};
Physical Line(183) = {1, 13, 22};
Physical Line(184) = {60, 69, 78};
Physical Surface(185) = {11};
Physical Surface(186) = {30, 39, 12};
Physical Surface(187) = {57, 66, 75, 48, 21};
