#coding-Utf-8-*-
from gmshpy import *
from msch import*

#script to launch PBC problem with a python script

#DEFINE MICRO PROBLEM


import nummat1
import nummat2
import nummat3


macromeshfile="macro.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 300  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-4  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)


# creation of  macro part Domain
nfield1 = 185 # number of the field (physical number of entity)
dim =2
fulldg= 1
beta1  = 10

field1 = hoDGDomain(1000,nfield1,0,nummat1.matnum,fulldg,dim)
field1.stabilityParameters(beta1)

nfield2 = 186
field2 = hoDGDomain(1000,nfield2,0,nummat2.matnum,fulldg,dim)
field2.stabilityParameters(beta1)

nfield3 = 187
field3 = hoDGDomain(1000,nfield3,0,nummat3.matnum,fulldg,dim)
field3.stabilityParameters(beta1)


myfieldinter1 = hoDGInterDomain(1000,field1,field2)
myfieldinter1.stabilityParameters(beta1)

myfieldinter2 = hoDGInterDomain(1000,field2,field3)
myfieldinter2.stabilityParameters(beta1)


# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(macromeshfile)
mysolver.addDomain(field1)
mysolver.addDomain(field2)
mysolver.addDomain(field3)

mysolver.addDomain(myfieldinter1)
mysolver.addDomain(myfieldinter2)

mysolver.addMaterialLaw(nummat1.mat)
mysolver.addMaterialLaw(nummat2.mat)
mysolver.addMaterialLaw(nummat3.mat)


mysolver.setMultiscaleFlag(1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)


# boundary condition

mysolver.displacementBC("Face",185,2,0)
mysolver.displacementBC("Face",186,2,0)
mysolver.displacementBC("Face",187,2,0)

mysolver.displacementBC("Edge",183,1,0.0)
mysolver.displacementBC("Edge",182,0,0.0)

mysolver.displacementBC("Edge",184,1,0.06)


# archivage
mysolver.internalPointBuildView("F_xx",IPField.F_XX, 1, 1);
mysolver.internalPointBuildView("F_xy",IPField.F_XY, 1, 1);
mysolver.internalPointBuildView("F_xz",IPField.F_XZ, 1, 1);
mysolver.internalPointBuildView("F_yx",IPField.F_YX, 1, 1);
mysolver.internalPointBuildView("F_yy",IPField.F_YY, 1, 1);
mysolver.internalPointBuildView("F_yz",IPField.F_YZ, 1, 1);
mysolver.internalPointBuildView("F_zx",IPField.F_ZX, 1, 1);
mysolver.internalPointBuildView("F_zy",IPField.F_ZY, 1, 1);
mysolver.internalPointBuildView("F_zz",IPField.F_ZZ, 1, 1);


mysolver.internalPointBuildView("G_xxx",IPField.G_XXX, 1, 1);
mysolver.internalPointBuildView("G_xyy",IPField.G_XYY, 1, 1);
mysolver.internalPointBuildView("G_xzz",IPField.G_XZZ, 1, 1);
mysolver.internalPointBuildView("G_xxy",IPField.G_XXY, 1, 1);
mysolver.internalPointBuildView("G_xxz",IPField.G_XXZ, 1, 1);
mysolver.internalPointBuildView("G_xyz",IPField.G_XYZ, 1, 1);

mysolver.internalPointBuildView("G_yxx",IPField.G_YXX, 1, 1);
mysolver.internalPointBuildView("G_yyy",IPField.G_YYY, 1, 1);
mysolver.internalPointBuildView("G_yzz",IPField.G_YZZ, 1, 1);
mysolver.internalPointBuildView("G_yxy",IPField.G_YXY, 1, 1);
mysolver.internalPointBuildView("G_yxz",IPField.G_YXZ, 1, 1);
mysolver.internalPointBuildView("G_yyz",IPField.G_YYZ, 1, 1);

mysolver.internalPointBuildView("G_zxx",IPField.G_ZXX, 1, 1);
mysolver.internalPointBuildView("G_zyy",IPField.G_ZYY, 1, 1);
mysolver.internalPointBuildView("G_zzz",IPField.G_ZZZ, 1, 1);
mysolver.internalPointBuildView("G_zxy",IPField.G_ZXY, 1, 1);
mysolver.internalPointBuildView("G_zxz",IPField.G_ZXZ, 1, 1);
mysolver.internalPointBuildView("G_zyz",IPField.G_ZYZ, 1, 1);

mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GLSTRAINEQUIVALENT, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

mysolver.internalPointBuildView("High-order strain norm",IPField.HIGHSTRAIN_NORM, 1, 1);
mysolver.internalPointBuildView("High-order stress norm",IPField.HIGHSTRESS_NORM, 1, 1);
mysolver.internalPointBuildView("Green -Lagrange strain norm",IPField.GL_NORM, 1, 1);


mysolver.archivingForceOnPhysicalGroup("Edge",184,1)

# solve
mysolver.solve()

