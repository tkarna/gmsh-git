
mm = 1.;

d = 1*mm;
lsca = 0.1*mm;

n = 2;

Point(1) = {0,0,0,lsca};
Point(2) = {d,0,0,lsca};
Point(3) = {d,d,0,lsca};
Point(4) = {0,d,0,lsca};
Coherence;
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 1};
Line Loop(5) = {4, 1, 2, 3};
Plane Surface(6) = {5};
For i In {1:n}
	Translate {i*d, 0, 0} {
		Duplicata { Surface{6}; }
	}
EndFor

For i In {1:n}
	Translate {0, i*d, 0} {
		Duplicata { Surface{6, 7, 12}; }
	}
EndFor

Physical Line(182) = {33, 18, 4};
Physical Line(183) = {1, 9, 14};
Physical Line(184) = {36, 41, 46};
Physical Surface(185) = {6};
Physical Surface(186) = {17, 22, 7};
Physical Surface(187) = {32, 37, 42, 27, 12};
Transfinite Line {4, 18, 33, 1, 3, 2, 20, 21, 35, 36, 9, 10, 11, 26, 25, 40, 41, 14, 16, 15, 31, 30, 45, 46} = 3 Using Progression 1;
Transfinite Surface {6, 17, 32, 7, 22, 37, 12, 27, 42};
Recombine Surface {6, 17, 32, 7, 22, 37, 12, 27, 42};
