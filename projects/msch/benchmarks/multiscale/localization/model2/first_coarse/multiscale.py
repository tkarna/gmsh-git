#coding-Utf-8-*-
from gmshpy import *
from msch import*

#script to launch PBC problem with a python script

#DEFINE MICRO PROBLEM


import nummat1
import nummat2
import nummat3

macromeshfile="macro.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 500  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-3  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)


# creation of  macro part Domain
nfield1 = 185 # number of the field (physical number of entity)
dim =2
field1 = FSDomain(1000,nfield1,nummat1.matnum,dim)

nfield2 = 186
field2 = FSDomain(1000,nfield2,nummat2.matnum,dim)

nfield3 = 187
field3 = FSDomain(1000,nfield3,nummat3.matnum,dim)


# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(macromeshfile)
mysolver.addDomain(field1)
mysolver.addDomain(field2)
mysolver.addDomain(field3)

mysolver.addMaterialLaw(nummat1.mat)
mysolver.addMaterialLaw(nummat2.mat)
mysolver.addMaterialLaw(nummat3.mat)

mysolver.setMultiscaleFlag(1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)


# boundary condition
mysolver.displacementBC("Edge",183,1,0.0)
mysolver.displacementBC("Edge",182,0,0.0)

mysolver.displacementBC("Edge",184,1,1.e-2)


# archivage
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GLSTRAINEQUIVALENT, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

mysolver.archivingForceOnPhysicalGroup("Edge",184,1)
mysolver.archivingNodeDisplacement(191,1,1)

# solve
mysolver.solve()

