l = 0.2;
r = 0.2*l;
lsc = 0.2*l;
lsc1 = 0.15*l;
c= 0;

P1 = newp; Point(P1) = {-l/2,-l/2,0,lsc};
P2 = newp; Point(P2) = {l/2,-l/2,0,lsc};
P3 = newp; Point(P3) = {l/2,l/2,0,lsc};
P4 = newp; Point(P4) = {-l/2,l/2,0,lsc};
P5 = newp; Point(P5) = {c-r/Sqrt(2),c-r/Sqrt(2),0,lsc1};
P6 = newp; Point(P6) = {c+r/Sqrt(2),c-r/Sqrt(2),0,lsc1};
P7 = newp; Point(P7) = {c+r/Sqrt(2),c+r/Sqrt(2),0,lsc1};
P8 = newp; Point(P8) = {c-r/Sqrt(2),c+r/Sqrt(2),0,lsc1};
P9 = newp; Point(P9) = {c,c,0,lsc};

l12 = newreg; Line(l12) = {P1,P2};
l23 = newreg; Line(l23) = {P2,P3};
l34 = newreg; Line(l34) = {P3,P4};
l41 = newreg; Line(l41) = {P4,P1};

c56 = newreg; Circle(c56) = {P5,P9,P6};
c67 = newreg; Circle(c67) = {P6,P9,P7};
c78 = newreg; Circle(c78) = {P7,P9,P8};
c85 = newreg; Circle(c85) = {P8,P9,P5};

ll1 = newreg; Line Loop(ll1) = {l12,l23,l34,l41,c56,c67,c78,c85};
s1 = newreg; Plane Surface(s1) = {ll1};

Physical Line(1) = {l12};
Physical Line(2) = {l23};
Physical Line(3) = {l34};
Physical Line(4) = {l41};

Physical Surface(1) = {s1};
