L = 1;
lsc = 0.1*L;
Point(1) = {-L/2, -L/2,0,lsc};
Point(2) = {L/2, -L/2,0,lsc};
Point(3) = {L/2, L/2,0,lsc};
Point(4) = {-L/2, L/2,0,lsc};

Line(1) = {4, 3};
Line(2) = {3, 2};
Line(3) = {2, 1};
Line(4) = {1, 4};
Line Loop(5) = {1, 2, 3, 4};
Plane Surface(6) = {5};
Extrude {0, 0, 0.1} {
  Surface{6};
}
Physical Volume(73) = {1};


Physical Surface(75) = {19};
Physical Surface(76) = {27};
Transfinite Line {8, 1, 9, 2, 10, 3, 4, 11} = 10 Using Progression 1;
Transfinite Line {13, 22, 18, 14} = 2 Using Progression 1;
Transfinite Surface {23, 19, 28, 6, 15, 27};
Recombine Surface {23, 19, 28, 6, 15, 27};
Transfinite Volume {1};
Physical Line(77) = {18};
Physical Line(78) = {22};
Physical Line(79) = {14};
Physical Line(80) = {13};
