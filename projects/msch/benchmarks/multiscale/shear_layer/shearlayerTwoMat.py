#coding-Utf-8-*-
from gmshpy import *
from msch import*

#script to launch PBC problem with a python script

#DEFINE MICRO PROBLEM

# micro-material law
lawnum = 11 # unique number of law
K = 76.E3 	# Bulk mudulus
mu =26.E3	  # Shear mudulus
rho = 2700. # Bulk mass
sy0 = 200
h = 0.5*sy0
E = 9.*K*mu/(3.*K+mu)
nu= (3.*K -2.*mu)/2./(3.*K+mu)

# micro-geometry
micromeshfile="micro.msh" # name of mesh file

# creation of material law
#law1 = FSElasticMaterialLaw(lawnum,1,K,mu,rho)
law1 = FSJ2LinearMaterialLaw(lawnum,E,nu,sy0,h,rho)

# creation of  micro part Domain
nfield = 11 # number of the field (physical number of entity)
dim =2
myfield1 = FSDomain(1000,nfield,lawnum,dim)



# periodiodic BC
method = 0 # Periodic mesh = 0 Langrange interpolation = 1 Cubic spline interpolation =2
degree = 5 
addvertex = 0

# DEFINE MACROPROBLEM
matnum = 1;
macromat = hoDGMultiscaleMaterialLaw(matnum, 1000)
macromat.loadModel(micromeshfile);
macromat.addDomain(myfield1)
macromat.addMaterialLaw(law1);
macromat.setPeriodicity(1.,0,0,"x")
macromat.setPeriodicity(0,1.,0,"y")
macromat.setPeriodicity(0,0,1.,"z")
macromat.addPeriodicBC(1000,1,2,3,4)
macromat.setPeriodicBCOptions(method, degree,addvertex)
macromat.setNumStep(2)
macromat.setTolerance(1e-6)
macromat.setSystemType(1)
macromat.Solver(2)
macromat.setViewAllMicroProblems(0,0)
macromat.addViewMicroSolver(18,0)
macromat.setOrder(2)
macromat.Scheme(1)
macromat.setAverageStressMethod(1)
macromat.stiffnessModification(1);
macromat.iterativeProcedure(1);


macrolawnum = 2 # unique number of law
macroK = 76.E8	# Bulk mudulus
macromu =26.E8	  # Shear mudulus
macrorho = 2700. # Bulk mass
macrosy0 = 200
macroh = 0.5*macrosy0
macroE = 9.*macroK*macromu/(3.*macroK+macromu)
macronu= (3.*macroK -2.*macromu)/2./(3.*macroK+macromu)
le = 0.
secondpar = macromu*le*le*0.5 

# creation of material law
macromat2 = hoDGElasticMaterialLaw(macrolawnum,0,3,macroK,macromu,macrorho)
macromat2.setParameter("A1",secondpar)
macromat2.setParameter("A2",secondpar)
macromat2.setParameter("A3",secondpar)
macromat2.setParameter("A4",secondpar)
macromat2.setParameter("A5",secondpar)


macromeshfile="shearlayer.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 10  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)


# creation of  macro part Domain

dim =2
beta1 = 50;
fdg=1

nfield1 = 11
macrodomain1 = hoDGDomain(1000,nfield1,0,matnum,fdg,dim)
macrodomain1.stabilityParameters(beta1)

nfield2 = 12
macrodomain2 = hoDGDomain(1000,nfield2,0,macrolawnum,fdg,dim)
macrodomain2.stabilityParameters(beta1)

nfield3 = 13
macrodomain3 = hoDGDomain(1000,nfield3,0,macrolawnum,fdg,dim)
macrodomain3.stabilityParameters(beta1)

interdomain1 = hoDGInterDomain(1000,macrodomain1,macrodomain2)
interdomain2 = hoDGInterDomain(1000,macrodomain1,macrodomain3)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(macromeshfile)
mysolver.addDomain(macrodomain1)
mysolver.addDomain(macrodomain2)
mysolver.addDomain(macrodomain3)
mysolver.addDomain(interdomain1)
mysolver.addDomain(interdomain2)
mysolver.addMaterialLaw(macromat)
mysolver.addMaterialLaw(macromat2)
mysolver.setMultiscaleFlag(1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)


# boundary condition
mysolver.displacementBC("Face",11,2,0.0)
mysolver.displacementBC("Face",12,2,0.0)
mysolver.displacementBC("Face",13,2,0.0)

mysolver.displacementBC("Face",12,1,0.0)
mysolver.displacementBC("Face",12,0,0.0)
mysolver.displacementBC("Face",13,0,0.0)
mysolver.displacementBC("Face",13,1,0.25)

mysolver.periodicBC("Edge",1,3,0)
mysolver.periodicBC("Edge",1,3,1)
# archivage
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GLSTRAINEQUIVALENT, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

mysolver.archivingForceOnPhysicalGroup("Edge",2,0)
mysolver.archivingForceOnPhysicalGroup("Edge",2,1)
mysolver.archivingForceOnPhysicalGroup("Edge",4,0)
mysolver.archivingForceOnPhysicalGroup("Edge",4,1)
# solve
mysolver.solve()

