clear all
clc;
close all;

%strain = importdata('homogenizedStrain.csv');
%stress = importdata('homogenizedStress.csv');

stresstable = dlmread('homogenizedStress.csv');

stresstime = stresstable(:,1);
figure(1);
plot(stresstime,stresstable(:,2:10),'LineWidth',1.5);
%legend(stress.textdata)
set(gca,'FontSize',16)
xlabel('\mu')
ylabel('P (Pa)')
grid on

%print -f1 -r600 -depsc stressImage
%system('epstopdf stressImage.eps')

%tangent = importdata('homogenizedTangent.csv');
figure(2)
tangenttable = dlmread('homogenizedTangent.csv');
plot(stresstime,tangenttable(:,2:82),'LineWidth',1.5);

%legend(tangent.textdata)
set(gca,'FontSize',16)
xlabel('\mu')
ylabel('L (Pa)')
grid on

%print -f2 -r600 -depsc tangentImage
%system('epstopdf tangentImage.eps')
