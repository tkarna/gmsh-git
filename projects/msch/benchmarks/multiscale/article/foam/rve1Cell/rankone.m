clear all
close all
clc
%----------------------------------------
tangent = importdata('./homogenizedTangent.csv');

siz = size(tangent.data);
row = siz(1);
col = siz(2)
theta = linspace(0,2*pi,100);
N = length(theta);

detQ = theta;
mindetQ = zeros(N,1);


for ii=1:row
   L = vec2ten(tangent.data(ii,2:col),tangent.textdata(2:col));
   for jj=1:N
    Q = zeros(3,3);
    n = zeros(3,1);
    n(1) = cos(theta(jj));
    n(2) = sin(theta(jj));
    n(3) = 0;
    for j=1:3
        for k=1:3
            for i=1:3
                for l=1:3
                    Q(j,k) =Q(j,k)+ n(i)*L(i,j,k,l)*n(l); 
                end
            end
        end
    end
    QQ = Q(1:2,1:2);
    detQ(ii,jj) = det(QQ);
   end
    mindetQ(ii) = min(detQ(ii,:));
end

figure(2)
plot(theta/pi,detQ,'LineWidth',2)
set(gca,'FontSize',16)
xlabel('\theta *\pi')
ylabel('det(Q)')


print -f2 -r600 -depsc detQImage
system('epstopdf detQImage.eps')

figure(3)
plot(tangent.data(:,1),mindetQ,'x-','LineWidth',2)
set(gca,'FontSize',16)
xlabel('\mu ')
ylabel('mindet(Q)')
grid on

print -f3 -r600 -depsc mindetQImage
system('epstopdf mindetQImage.eps')


