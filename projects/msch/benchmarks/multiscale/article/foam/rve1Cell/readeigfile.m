clear all
clc

eig = load('eigenValue.csv');
A = size(eig);

figure(1)
plot(eig(:,1),eig(:,2),'o--','LineWidth',2);
set(gca,'FontSize',16)
xlabel('\mu');
ylabel('Min eigenvalue')
print -f1 -r600 -depsc eigen
system('epstopdf eigen.eps')

figure()

plot(eig(:,1),eig(:,4),'s--',eig(:,1),eig(:,5),'o--')
xlabel('\mu');
ylabel('Max eigen value')

figure()
hold on
for i=2:2:10
    plot(eig(:,1),eig(:,i),'s--')
end
