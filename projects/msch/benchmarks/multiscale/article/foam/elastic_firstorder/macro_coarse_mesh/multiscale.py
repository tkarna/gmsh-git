#coding-Utf-8-*-
from gmshpy import *
from msch import*

#script to launch PBC problem with a python script

#DEFINE MICRO PROBLEM

# micro-material law
lawnum = 11 # unique number of law
K = 76.E3 	# Bulk mudulus
mu =26.E3	  # Shear mudulus
rho = 2700. # Bulk mass
sy0 = 200
h = 0.5*sy0
E = 9.*K*mu/(3.*K+mu)
nu= (3.*K -2.*mu)/2./(3.*K+mu)

# micro-geometry
micromeshfile="rve1Cell.msh" # name of mesh file

# creation of material law
law1 = FSElasticMaterialLaw(lawnum,1,rho)
law1.setParameter("YOUNG_MODULUS",E)
law1.setParameter("POISSON_RATIO",nu)

#law1 = FSJ2LinearMaterialLaw(lawnum,E,nu,sy0,h,rho)

# creation of  micro part Domain
nfield = 11 # number of the field (physical number of entity)
dim =2
myfield1 = FSDomain(1000,nfield,lawnum,dim)



# periodiodic BC
method = 0 # Periodic mesh = 0 Langrange interpolation = 1 Cubic spline interpolation =2
degree = 5 
addvertex = 0

# DEFINE MACROPROBLEM
matnum = 1;
macromat = FSMultiscaleMaterialLaw(matnum, 1000)
macromat.loadModel(micromeshfile);
macromat.addDomain(myfield1)
macromat.addMaterialLaw(law1);
macromat.setPeriodicity(1.,0,0,"x")
macromat.setPeriodicity(0,1.,0,"y")
macromat.setPeriodicity(0,0,1.,"z")
macromat.addPeriodicBC(1000,1,2,3,4)
macromat.setPeriodicBCOptions(method, degree,addvertex)
macromat.setNumStep(30)
macromat.setTolerance(1e-6)
macromat.setSystemType(1)
macromat.Solver(2)
macromat.setViewAllMicroProblems(1,0)
#macromat.addViewMicroSolver(18,0)
macromat.setOrder(1)
macromat.Scheme(1)
macromat.setAverageStressMethod(0)
macromat.setAverageTangentMethod(0)
macromat.stiffnessModification(1);
macromat.iterativeProcedure(1);

macromeshfile="holeCenter.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 300  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)


# creation of  macro part Domain
nfield = 17 # number of the field (physical number of entity)
dim =2
macrodomain = FSDomain(1000,nfield,matnum,dim)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(macromeshfile)
mysolver.addDomain(macrodomain)
mysolver.addMaterialLaw(macromat)
mysolver.setMultiscaleFlag(1)

mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
#mysolver.snlManageTimeStep(50,2, 2., 15);
#mysolver.explicitSpectralRadius(ftime,0.1,0.)
#mysolver.dynamicRelaxation(0.1, ftime, 1.e-3,2)
#mysolver.explicitTimeStepEvaluation(nstep)
mysolver.pathFollowing(1)
mysolver.setPathFollowingControlType(1)
#mysolver.lineSearch(0)

# boundary condition

mysolver.constraintBC("Edge",12,1)
mysolver.displacementBC("Edge",15,1,0)
mysolver.displacementBC("Edge",13,0,0)

mysolver.forceBC("Edge",12,1,500)



# archivage
mysolver.internalPointBuildView("F_xx",IPField.F_XX, 1, 1);
mysolver.internalPointBuildView("F_xy",IPField.F_XY, 1, 1);
mysolver.internalPointBuildView("F_xz",IPField.F_XZ, 1, 1);
mysolver.internalPointBuildView("F_yx",IPField.F_YX, 1, 1);
mysolver.internalPointBuildView("F_yy",IPField.F_YY, 1, 1);
mysolver.internalPointBuildView("F_yz",IPField.F_YZ, 1, 1);
mysolver.internalPointBuildView("F_zx",IPField.F_ZX, 1, 1);
mysolver.internalPointBuildView("F_zy",IPField.F_ZY, 1, 1);
mysolver.internalPointBuildView("F_zz",IPField.F_ZZ, 1, 1);

mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GLSTRAINEQUIVALENT, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

mysolver.internalPointBuildView("strain_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("strain_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("strain_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("strain_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("strain_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("strain_xz",IPField.STRAIN_XZ, 1, 1);

mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);

mysolver.internalPointBuildView("High-order strain norm",IPField.HIGHSTRAIN_NORM, 1, 1);
mysolver.internalPointBuildView("High-order stress norm",IPField.HIGHSTRESS_NORM, 1, 1);
mysolver.internalPointBuildView("Green -Lagrange strain norm",IPField.GL_NORM, 1, 1);

mysolver.archivingForceOnPhysicalGroup("Edge",12,1)
mysolver.archivingNodeDisplacement(18,1,1)

# solve
mysolver.solve()

