
mm = 1.;

L = 150*mm;
R = 0.3*L;
lsca = 0.1*R;

Point(1) = {R,0,0,lsca};
Point(2) = {L,0,0,lsca};
Point(3) = {L,L,0,lsca};
Point(4) = {0,L,0,lsca};
Point(5) = {0,R,0,lsca};
Point(6) = {R/Sqrt(2.),R/Sqrt(2.),0,lsca};
Point(7) = {0,0,0,lsca};

Line(1) = {5, 4};
Line(2) = {4, 3};
Line(3) = {3, 2};
Line(4) = {2, 1};
Circle(5) = {5, 7, 6};
Circle(6) = {6, 7, 1};
Line(7) = {6, 3};
Line Loop(8) = {1, 2, -7, -5};
Plane Surface(9) = {8};
Line Loop(10) = {7, 3, 4, -6};
Plane Surface(11) = {10};
Physical Line(12) = {2};
Physical Line(13) = {1};
Physical Line(14) = {6, 5};
Physical Line(15) = {4};
Physical Line(16) = {3};
Physical Surface(17) = {9, 11};
Transfinite Line {1, -4} = 8 Using Progression 1.1;
Transfinite Line {7} = 8 Using Progression 1.18;
Transfinite Line {2, 5, 6, 3} = 6 Using Progression 1;
Transfinite Surface {9, 11};

Physical Point(18) = {4};
