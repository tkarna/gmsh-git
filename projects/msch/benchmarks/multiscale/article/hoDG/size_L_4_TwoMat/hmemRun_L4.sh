#!/bin/bash

#SBATCH --job-name=mpiMultiscale_L4
#
#SBATCH --mail-user=vandung.nguyen@ulg.ac.be
#SBATCH --mail-type=ALL
#
#SBATCH --output="out"

#SBATCH --ntasks=30
#SBATCH --mem-per-cpu=2000
#SBATCH --time=5-0

# place to launch computation 
# place of simulation & files to launch

PYFILE=shearlayer.py

# command (normally souldn't edit)
$PETSC_DIR/$PETSC_ARCH/bin/mpiexec python $PYFILE
