L=4;
Point(1) = {0, 0, 0, 1.0};
Point(2) = {0.1*L, 0, 0, 1.0};
Point(3) = {0.1*L, 0, 0.1*L, 1.0};
Point(4) = {0., 0, 0.1*L, 1.0};
Line(1) = {4, 3};
Line(2) = {3, 2};
Line(3) = {2, 1};
Line(4) = {1, 4};
Line Loop(5) = {1, 2, 3, 4};
Plane Surface(6) = {5};
Extrude {0, 0.5*L,0} {
  Surface{6};
}
Extrude {0, 0.5*L,0} {
  Surface{28};
}
Physical Volume(51) = {1,2};
Physical Surface(53) = {50};
Physical Surface(54) = {6};
Transfinite Line {31, 33, 11, 9, 2, 4} =2 Using Progression 1;
Transfinite Line {1, 3, 8, 10, 30, 32} = 2 Using Progression 1;
Transfinite Line {-36, -35, -40, -44, 14, 13, 18, 22} = 11 Using Progression 1.15;

Transfinite Surface {37};
Transfinite Surface {50};
Transfinite Surface {41};
Transfinite Surface {45};
Transfinite Surface {49};
Transfinite Surface {28};
Transfinite Surface {6};
Transfinite Surface {15};
Transfinite Surface {19};
Transfinite Surface {23};
Transfinite Surface {27};
Recombine Surface {41, 45, 37, 49, 50, 28, 23, 19, 27, 6, 15};
Transfinite Volume {1};
Transfinite Volume {2};

Physical Surface(57) = {23, 45};
Physical Surface(58) = {37, 15};

Extrude {0, -0.1*L,0} {
  Surface{6};
}
Extrude {0, 0.1*L, 0} {
  Surface{50};
}
Physical Volume(103) = {3};
Physical Volume(104) = {4};
Transfinite Line {62, 60, 61, 60, 63, 83, 82, 84, 85} = 2 Using Progression 1;
Transfinite Line {70, 66, 74, 65, 92, 88, 96, 87} = 2 Using Progression 1;

Transfinite Surface {67, 80, 71, 79, 75, 89, 93, 101, 102, 97};
Recombine Surface {67, 80, 71, 79, 75, 89, 93, 101, 102, 97};
Transfinite Volume {3,4};


Physical Surface(105) = {41, 19};
Physical Surface(106) = {49, 27};
