#!/bin/bash

#SBATCH --job-name=mpiMultiscale
#
#SBATCH --mail-user=vandung.nguyen@ulg.ac.be
#SBATCH --mail-type=ALL
#
#SBATCH --output="out"

#SBATCH --ntasks=6
#SBATCH --mem-per-cpu=3000
#SBATCH --time=200:5:0

# place to launch computation 
# place of simulation & files to launch

PYFILE=shearlayer.py

# command (normally souldn't edit)
$PETSC_DIR/$PETSC_ARCH/bin/mpiexec python $PYFILE
