

mm =1.;

B = 10.*Sqrt(3.)*mm;
H = 23.*mm;
lsca = mm;

Point(1) = {0,0,0,lsca};
Point(2) = {B,0,0,lsca};
Point(3) = {B,H,0,lsca};
Point(4) = {0,H,0,lsca};


Line(1) = {4, 3};
Line(2) = {3, 2};
Line(3) = {2, 1};
Line(4) = {1, 4};
Line Loop(5) = {1, 2, 3, 4};

Extrude {0, 2*mm, 0} {
  Line{1};
}
Extrude {0, -2*mm, 0} {
  Line{3};
}

Plane Surface(6) = {5};
Physical Surface(14) = {9};
Physical Surface(15) = {6};
Physical Surface(16) = {13};
Transfinite Line {6, 1, 3, 10} = 7 Using Progression 1;
Transfinite Line {4, 2} = 15 Using Progression 1;
Transfinite Line {7, 8, 11, 12} = 2 Using Progression 1;
Transfinite Surface {13, 6, 9};
Recombine Surface {13, 6, 9};
