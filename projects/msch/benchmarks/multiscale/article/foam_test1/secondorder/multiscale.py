#coding-Utf-8-*-
from gmshpy import *
from msch import*

#script to launch PBC problem with a python script

import nummat1

#--------------------------------------------------------------------------------

macrolawnum = 2 # unique number of law
macroK = 76.E8	# Bulk mudulus
macromu =26.E8	  # Shear mudulus
macrorho = 2700. # Bulk mass
macrosy0 = 200
macroh = 0.5*macrosy0
macroE = 9.*macroK*macromu/(3.*macroK+macromu)
macronu= (3.*macroK -2.*macromu)/2./(3.*macroK+macromu)
le = 0.
secondpar = macromu*le*le*0.5 

# creation of material law
macromat2 = hoDGElasticMaterialLaw(macrolawnum,0,3,macroK,macromu,macrorho)
macromat2.setParameter("A1",secondpar)
macromat2.setParameter("A2",secondpar)
macromat2.setParameter("A3",secondpar)
macromat2.setParameter("A4",secondpar)
macromat2.setParameter("A5",secondpar)


#-------------------------------------------

# macro-geometry
meshfile="macro.msh" # name of mesh file

# creation of  micro part Domain
fulldg= 1
beta1  = 1000
dim = 2


nfield1 = 15 # number of the field (physical number of entity)
myfield1 = hoDGDomain(1000,nfield1,0,nummat1.matnum,fulldg,dim)
myfield1.stabilityParameters(beta1)

nfield2 = 14 # number of the field (physical number of entity)
myfield2 = hoDGDomain(1000,nfield2,0,macrolawnum,fulldg,dim)
myfield2.stabilityParameters(beta1)

nfield3 = 16 # number of the field (physical number of entity)
myfield3 = hoDGDomain(1000,nfield3,0,macrolawnum,fulldg,dim)
myfield3.stabilityParameters(beta1)


myfieldinter1 = hoDGInterDomain(1000,myfield1,myfield2)
myfieldinter1.stabilityParameters(beta1)

myfieldinter2 = hoDGInterDomain(1000,myfield1,myfield3)
myfieldinter2.stabilityParameters(beta1)

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1000  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-5  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)



# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)

mysolver.addMaterialLaw(nummat1.mat)
mysolver.addMaterialLaw(macromat2)

mysolver.addDomain(myfield1)
mysolver.addDomain(myfield2)
mysolver.addDomain(myfield3)

mysolver.addDomain(myfieldinter1)
mysolver.addDomain(myfieldinter2)

mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)

# boundary condition

mysolver.displacementBC("Face",15,2,0)

mysolver.displacementBC("Face",14,0,-0.05)
mysolver.displacementBC("Face",14,1,0)
mysolver.displacementBC("Face",14,2,0)

mysolver.displacementBC("Face",16,0,0)
mysolver.displacementBC("Face",16,1,0)
mysolver.displacementBC("Face",16,2,0)

# archivage
mysolver.internalPointBuildView("F_xx",IPField.F_XX, 1, 1);
mysolver.internalPointBuildView("F_xy",IPField.F_XY, 1, 1);
mysolver.internalPointBuildView("F_xz",IPField.F_XZ, 1, 1);
mysolver.internalPointBuildView("F_yx",IPField.F_YX, 1, 1);
mysolver.internalPointBuildView("F_yy",IPField.F_YY, 1, 1);
mysolver.internalPointBuildView("F_yz",IPField.F_YZ, 1, 1);
mysolver.internalPointBuildView("F_zx",IPField.F_ZX, 1, 1);
mysolver.internalPointBuildView("F_zy",IPField.F_ZY, 1, 1);
mysolver.internalPointBuildView("F_zz",IPField.F_ZZ, 1, 1);


mysolver.internalPointBuildView("G_xxx",IPField.G_XXX, 1, 1);
mysolver.internalPointBuildView("G_xyy",IPField.G_XYY, 1, 1);
mysolver.internalPointBuildView("G_xzz",IPField.G_XZZ, 1, 1);
mysolver.internalPointBuildView("G_xxy",IPField.G_XXY, 1, 1);
mysolver.internalPointBuildView("G_xxz",IPField.G_XXZ, 1, 1);
mysolver.internalPointBuildView("G_xyz",IPField.G_XYZ, 1, 1);

mysolver.internalPointBuildView("G_yxx",IPField.G_YXX, 1, 1);
mysolver.internalPointBuildView("G_yyy",IPField.G_YYY, 1, 1);
mysolver.internalPointBuildView("G_yzz",IPField.G_YZZ, 1, 1);
mysolver.internalPointBuildView("G_yxy",IPField.G_YXY, 1, 1);
mysolver.internalPointBuildView("G_yxz",IPField.G_YXZ, 1, 1);
mysolver.internalPointBuildView("G_yyz",IPField.G_YYZ, 1, 1);

mysolver.internalPointBuildView("G_zxx",IPField.G_ZXX, 1, 1);
mysolver.internalPointBuildView("G_zyy",IPField.G_ZYY, 1, 1);
mysolver.internalPointBuildView("G_zzz",IPField.G_ZZZ, 1, 1);
mysolver.internalPointBuildView("G_zxy",IPField.G_ZXY, 1, 1);
mysolver.internalPointBuildView("G_zxz",IPField.G_ZXZ, 1, 1);
mysolver.internalPointBuildView("G_zyz",IPField.G_ZYZ, 1, 1);



mysolver.internalPointBuildView("strain_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("strain_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("strain_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("strain_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("strain_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("strain_xz",IPField.STRAIN_XZ, 1, 1);

mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);

mysolver.internalPointBuildView("High-order strain norm",IPField.HIGHSTRAIN_NORM, 1, 1);
mysolver.internalPointBuildView("High-order stress norm",IPField.HIGHSTRESS_NORM, 1, 1);
mysolver.internalPointBuildView("Green -Lagrange strain norm",IPField.GL_NORM, 1, 1);

mysolver.archivingForceOnPhysicalGroup("Face",14,0)
mysolver.archivingForceOnPhysicalGroup("Face",14,1)
mysolver.archivingForceOnPhysicalGroup("Face",14,2)


# solve
mysolver.solve()
