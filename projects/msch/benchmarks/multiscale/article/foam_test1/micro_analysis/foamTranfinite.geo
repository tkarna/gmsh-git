// for creating foam structures

// generate random points  

mm = 1;

l = 1*mm;
h = 0.02*l;
lsca1 = 0.7*h;

Mesh.RecombineAll=0;

x0 = 0; y0 = 0;

x[1] = x0;							y[1] = y0;
x[2] = x[1]; 							y[2] = y[1]+l;
x[3] = x[1]+l*Sqrt(3)/2.; y[3] =y[1]+ l+l/2.;
x[4] = x[1]+l*Sqrt(3); 		y[4] = y[1]+l;
x[5] = x[1]+l*Sqrt(3); 			y[5] = y[1];
x[6] = x[3]; 							y[6] = y[3]+l;
x[7] = x[6]+l*Sqrt(3)/2; 	y[7] = y[6]+l/2;
x[8] = x[7]-l*Sqrt(3); 		y[8] = y[7];
x[9] = x[1]+2*h; 					y[9]= y[1];
x[10] = x[1]+h; 					y[10] = y[2]-h/Tan(Pi/3); 
x[11] = x[3]; 						y[11] = y[3] - h/Sin(Pi/3);
x[12] = x[5]-h;						y[12] = y[10];
x[13] = x[12]-h;					y[13] = y[1];
x[14] = x[2];							y[14] = y[2]+h/Sin(Pi/3);
x[15] = x[3]-h;						y[15] = y[3]+ h/Tan(Pi/3); 
x[16] = x[15]; 						y[16] = y[6] - h/Tan(Pi/3);
x[17] = x[1];							y[17] = y[8] - h/Sin(Pi/3);
x[18] = x[8]+2*h;					y[18] = y[8];
x[19] = x[6];							y[19] = y[6]+h/Sin(Pi/3);
x[20] = x[7] - 2*h;				y[20] = y[7];
x[21] = x[7];							y[21] = y[17];
x[22] = x[6]+h;						y[22] = y[16];
x[23] = x[22];						y[23] = y[15];
x[24] = x[5];							y[24] = y[14];
x[25] = x[1]+h;						y[25] = y[1]+h*Tan(Pi/6);
x[26] = x[5]-h;						y[26] = y[25];

For i In {1:26}
	  p[i] = newp; Point(p[i]) ={x[i],y[i],0,lsca1};
EndFor

iter = 0;
lin[iter] = newreg; Line(lin[iter]) = {p[1],p[9]};
iter++;
lin[iter] = newreg; Line(lin[iter]) = {p[9],p[25]};
iter++;
lin[iter] = newreg; Line(lin[iter]) = {p[25],p[10]};
iter++;
lin[iter] = newreg; Line(lin[iter]) = {p[10],p[11]};
iter++;
lin[iter] = newreg; Line(lin[iter]) = {p[11],p[12]};
iter++;
lin[iter] = newreg; Line(lin[iter]) = {p[12],p[26]};
iter++;
lin[iter] = newreg; Line(lin[iter]) = {p[26],p[13]};
iter++;
lin[iter] = newreg; Line(lin[iter]) = {p[13],p[5]};
iter++;
lin[iter] = newreg; Line(lin[iter]) = {p[5],p[24]};
iter++;
lin[iter] = newreg; Line(lin[iter]) = {p[24],p[23]};
iter++;
lin[iter] = newreg; Line(lin[iter]) = {p[23],p[22]};
iter++;
lin[iter] = newreg; Line(lin[iter]) = {p[22],p[21]};
iter++;
lin[iter] = newreg; Line(lin[iter]) = {p[21],p[7]};
iter++;
lin[iter] = newreg; Line(lin[iter]) = {p[7],p[20]};
iter++;
lin[iter] = newreg; Line(lin[iter]) = {p[20],p[19]};
iter++;
lin[iter] = newreg; Line(lin[iter]) = {p[19],p[18]};
iter++;
lin[iter] = newreg; Line(lin[iter]) = {p[18],p[8]}; 
iter++;
lin[iter] = newreg; Line(lin[iter]) = {p[8],p[17]};
iter++;
lin[iter] = newreg; Line(lin[iter]) = {p[17],p[16]};
iter++;
lin[iter] = newreg; Line(lin[iter]) = {p[16],p[15]};
iter++;
lin[iter] = newreg; Line(lin[iter]) = {p[15],p[14]};
iter++;
lin[iter] = newreg; Line(lin[iter]) = {p[14],p[1]};




Line(23) = {14, 10};
Line(24) = {15, 11};
Line(25) = {11, 23};
Line(26) = {23, 15};
Line(27) = {24, 12};
Line(28) = {26, 5};
Line(29) = {25, 1};
Line(30) = {16, 19};
Line(31) = {19, 22};
Line(32) = {22, 16};
Line(33) = {20, 21};
Line(34) = {18, 17};
Line Loop(35) = {17, 18, -34};
Plane Surface(36) = {35};
Line Loop(37) = {16, 34, 19, 30};
Plane Surface(38) = {37};
Line Loop(39) = {30, 31, 32};
Plane Surface(40) = {39};
Line Loop(41) = {15, 31, 12, -33};
Plane Surface(42) = {41};
Line Loop(43) = {14, 33, 13};
Plane Surface(44) = {43};
Line Loop(45) = {11, 32, 20, -26};
Plane Surface(46) = {45};
Line Loop(47) = {26, 24, 25};
Plane Surface(48) = {47};
Line Loop(49) = {10, -25, 5, -27};
Plane Surface(50) = {49};
Line Loop(51) = {21, 23, 4, -24};
Plane Surface(52) = {51};
Line Loop(53) = {23, -3, 29, -22};
Plane Surface(54) = {53};
Line Loop(55) = {2, 29, 1};
Plane Surface(56) = {55};
Line Loop(57) = {8, -28, 7};
Plane Surface(58) = {57};
Line Loop(59) = {6, 28, 9, 27};
Plane Surface(60) = {59};
Transfinite Line {21, 4, 10, 5, 11, 20, 9, 6, 3, 22, 12, 15, 16, 19} = 21 Using Progression 1;
Transfinite Line {17, 34, 30, 32, 31, 33, 17, 18, 14, 13, 26, 24, 25, 27, 28, 7, 8, 1, 2, 29, 23} = 3 Using Progression 1;
Transfinite Surface {56, 54, 52, 58, 48, 36, 50, 38, 60, 46, 40, 42, 44};
Physical Line(1) = {1, 8};
Physical Line(2) = {9, 13};
Physical Line(3) = {14, 17};
Physical Line(4) = {18, 22};
Physical Surface(1) = {46, 54, 52, 60, 50, 48, 40, 38, 36, 42, 44, 56, 58};
