#coding-Utf-8-*-
from gmshpy import *
from msch import*

#script to launch PBC problem with a python script

# material law
lawnum = 11 # unique number of law
E = 70E3 #Young modulus
nu = 0.3 #Poisson ratio
K = E/3./(1.-2.*nu)
mu = E/2./(1+nu)
sy0 = 300. #Yield stress
h = 300. # hardening modulus
rho = 1.  #density
# geometry

meshfile="arch.msh" # name of mesh file

# creation of material law

#law1 = FSElasticMaterialLaw(lawnum,1,K,mu,rho)

#law1 = FSJ2LinearMaterialLaw(lawnum,E,nu,sy0,h,rho)
law1 = J2LinearDG3DMaterialLaw(lawnum,rho,E,nu,sy0,h)

# creation of part Domain
nfield = 15 # number of the field (physical number of entity)
dim =2
beta1 = 50;
fdg=1
myfield1 = dG3DDomain(1000,nfield,0,lawnum,fdg,dim)
myfield1.stabilityParameters(beta1)
#myfield1 = FSDomain(1000,nfield,lawnum,dim)

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 300  # number of step (used only if soltype=1)
ftime =20.   # Final time (used only if soltype=1)
tol=1.e-5 # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)

mysolver.pathFollowing(1)
mysolver.setPathFollowingControlType(1)
#mysolver.eigenValueSolver(100)
#mysolver.setPerturbationFactor(0.01)
mysolver.iterativeProcedure(1)


mysolver.displacementBC("Face",15,2,0)
mysolver.displacementBC("Edge",12,0,0)
mysolver.displacementBC("Edge",12,1,0)
mysolver.displacementBC("Edge",13,0,0)
mysolver.displacementBC("Edge",13,1,0)

mysolver.forceBC("Node",16,1,-1e2)

# build view
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GLSTRAINEQUIVALENT, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);


mysolver.archivingNodeDisplacement(16,1,1)


# solve
mysolver.solve()
