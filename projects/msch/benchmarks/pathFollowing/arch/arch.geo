mm = 1.;

R=1.5*mm;
alpha = 50./180.*Pi;
L = R*Sin(alpha);
Delta = R-R*Cos(alpha);
t = 0.1;

Point(1) = {0,0,0,1};
Point(2) = {2*L,0,0,1};
Point(3) = {L,Delta,0,1};
Point(4) = {L,Delta-R,0,1};


Point(5)={-1.*t*Sin(alpha),t*Cos(alpha),0,1};
Point(6)={2*L+1.*t*Sin(alpha),t*Cos(alpha),0,1};
Point(7)= {L,Delta+t,0,1};


Circle(1) = {5, 4, 7};
Circle(2) = {7, 4, 6};
Circle(3) = {1, 4, 3};
Circle(4) = {3, 4, 2};
Line(5) = {5, 1};
Line(6) = {7, 3};
Line(7) = {6, 2};
Line Loop(8) = {1, 6, -3, -5};
Plane Surface(9) = {8};
Line Loop(10) = {6, 4, -7, -2};
Plane Surface(11) = {10};
Transfinite Line {5, 6, 7} = 3 Using Progression 1;
Transfinite Line {1, 2, 4, 3} = 31 Using Progression 1;
Transfinite Surface {9, 11};
//Recombine Surface {9, 11};
Physical Line(12) = {5};
Physical Line(13) = {7};
Physical Line(14) = {1, 2};
Physical Surface(15) = {9, 11};
Physical Point(16) = {7};
