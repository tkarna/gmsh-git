#coding-Utf-8-*-
from gmshpy import *
from msch import*

#script to launch PBC problem with a python script

# macro-material law
lawnum1 = 11 # unique number of law
E1 = 70E3
nu1 = 0.
K1 = E1/3./(1.-2.*nu1)	# Bulk mudulus
mu1 =E1/2./(1.+nu1)	  # Shear mudulus
rho1 = 2700. # Bulk mass

le1 = 0.1


# creation of material law
law1 = hoDGMaterialLaw(lawnum1,K1,mu1,rho1)
law1.setCharacteristicLength(le1)
law1.setElasticFlag(1)

lawnum2 = 12 # unique number of law
E2 = 17.5E3
nu2 = 0.
K2 = E2/3./(1.-2.*nu2)	# Bulk mudulus
mu2 =E2/2./(1.+nu2)
rho2 = 2700. # Bulk mass

le2 = 0.1

# creation of material law
law2 = hoDGMaterialLaw(lawnum2,K2,mu2,rho2)
law2.setCharacteristicLength(le2)
law2.setElasticFlag(1)


# micro-geometry
meshfile="bimaterial.msh" # name of mesh file
#meshfile="bimaterialPart4.msh" # name of mesh file


# creation of  micro part Domain
fulldg= 1
beta1  = 10
bulkpert =0
interpert =0
perturbation = 1e-6

nfield1 = 51 # number of the field (physical number of entity)
myfield1 = hoDGDomain(1000,nfield1,0,lawnum1,fulldg)
myfield1.stabilityParameters(beta1)
myfield1.matrixByPerturbation(bulkpert,interpert,interpert,perturbation)
#myfield1.gaussIntegration(hoDGDomain.Gauss,2,2)

nfield2 = 52 # number of the field (physical number of entity)
myfield2 = hoDGDomain(1000,nfield2,0,lawnum2,fulldg)
myfield2.stabilityParameters(beta1)
myfield2.matrixByPerturbation(bulkpert,interpert,interpert,perturbation)
#myfield2.gaussIntegration(hoDGDomain.Gauss,2,2)

myfieldinter = hoDGInterDomain(1000,myfield1,myfield2)
myfieldinter.stabilityParameters(beta1)
myfieldinter.matrixByPerturbation(interpert,perturbation)
#myfieldinter.gaussIntegration(hoDGDomain.Gauss,2,2)



# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1e-6  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)



# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)

mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)

mysolver.addDomain(myfield2)
mysolver.addMaterialLaw(law2)

mysolver.addDomain(myfieldinter)

mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)

# boundary condition
mysolver.displacementBC("Face",54,0,0.)
mysolver.displacementBC("Face",54,1,0.)
mysolver.displacementBC("Face",54,2,0.)

#mysolver.displacementBC("Face",2,2,0.)
#mysolver.displacementBC("Face",2,1,0.)
mysolver.displacementBC("Face",53,2,1.e-2)
mysolver.displacementBC("Face",53,1,0)
mysolver.displacementBC("Face",53,0,0)

#mysolver.forceBC("Face",53,2,100)

# archivage
#mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
#mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
#mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
#mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
#mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
#mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
#mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
#mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
#mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
#mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
#mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
#mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GLSTRAINEQUIVALENT, 1, 1);
#mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

mysolver.internalPointBuildView("High-order strain norm",IPField.HIGHSTRAIN_NORM, 1, 1);
mysolver.internalPointBuildView("High-order stress norm",IPField.HIGHSTRESS_NORM, 1, 1);
mysolver.internalPointBuildView("Green -Lagrange strain norm",IPField.GL_NORM, 1, 1);
mysolver.internalPointBuildView("PK2 stress norm",IPField.STRESS_NORM, 1, 1);

# solve
mysolver.solve()
