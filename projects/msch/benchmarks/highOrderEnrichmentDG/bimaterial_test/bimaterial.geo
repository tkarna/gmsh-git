Point(1) = {0, 0, 0, 1.0};
Point(2) = {.1, 0, 0, 1.0};
Point(3) = {.1, .1, 0, 1.0};
Point(4) = {0., .1, 0, 1.0};
Line(1) = {4, 3};
Line(2) = {3, 2};
Line(3) = {2, 1};
Line(4) = {1, 4};
Line Loop(5) = {1, 2, 3, 4};
Plane Surface(6) = {5};
Extrude {0, 0, 0.5} {
  Surface{6};
}
Extrude {0, 0, 0.5} {
  Surface{28};
}
Physical Volume(51) = {2};
Physical Volume(52) = {1};
Physical Surface(53) = {50};
Physical Surface(54) = {6};
Transfinite Line {30, 32, 31, 33, 8, 11, 9, 10, 1, 2, 2, 3, 4} =2 Using Progression 1;
Transfinite Line {36, 35, 40, 44, 14, 13, 18, 22} = 15 Using Progression 1;
Transfinite Surface {37};
Transfinite Surface {50};
Transfinite Surface {41};
Transfinite Surface {45};
Transfinite Surface {49};
Transfinite Surface {28};
Transfinite Surface {6};
Transfinite Surface {15};
Transfinite Surface {19};
Transfinite Surface {23};
Transfinite Surface {27};
Recombine Surface {41, 45, 37, 49, 50, 28, 23, 19, 27, 6, 15};
Transfinite Volume {1};
Transfinite Volume {2};



Physical Surface(55) = {19, 41};
Physical Surface(56) = {27, 49};
Physical Surface(57) = {15, 37};
Physical Surface(58) = {23, 45};
