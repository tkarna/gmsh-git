L=1;
lca = 0.1*L;
Point(1) = {0.0, 0.0, 0.0, lca};
Point(2) = {0.1*L, 0.0, 0.0, lca};
Point(3) = {0.1*L, 0.0, 0.1*L, lca};
Point(4) = {0.0, 0.0, 0.1*L, lca};
Line(1) = {4, 3};
Line(2) = {3, 2};
Line(3) = {2, 1};
Line(4) = {1, 4};
Line Loop(5) = {1, 2, 3, 4};
Plane Surface(6) = {5};
out[] = Extrude {0, L, 0} {
  Surface{6};
};
outUp[] = Extrude {0, 0.1*L, 0}{
	Surface{out[0]};
};
outLow[] = Extrude {0, -0.1*L, 0}{
	Surface{6};
};

Physical Volume(1) = {out[1]};
Physical Volume(2) = {outUp[1]};
Physical Volume(3) = {outLow[1]};
Transfinite Line {18, 14, 22, 13} = 15 Using Progression 1;
Transfinite Line {10, 8, 11, 9, 40, 36, 30, 32, 31, 33, 30, 44, 35, 3, 4, 1, 2, 62, 58, 66, 57, 58, 52, 57, 53, 62, 54, 55, 66} = 2 Using Progression 1;
Transfinite Surface {50, 37, 41, 49, 28, 45, 15, 19, 27, 6, 23, 59, 63, 71, 72, 67};
Recombine Surface {50, 37, 41, 49, 28, 45, 15, 19, 27, 6, 23, 59, 63, 71, 72, 67};
Transfinite Volume{2, 1, 3};
Physical Surface(73) = {19};
Physical Surface(74) = {27};
Physical Line(75) = {18};
