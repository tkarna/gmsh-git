#coding-Utf-8-*-
from gmshpy import *
from msch import*

#script to launch PBC problem with a python script

# macro-material law
lawnum1 = 11 # unique number of law
E1 = 70E3
nu1 = 0.3
K1 = E1/3./(1.-2.*nu1)	# Bulk mudulus
mu1 =E1/2./(1.+nu1)	  # Shear mudulus
rho1 = 0.01 # Bulk mass

le1 = 0.01
highorder1 = 1
elastic1 = 1

# creation of material law
law1 = hoDGMaterialLaw(lawnum1,K1,mu1,rho1)
law1.setCharacteristicLength(le1)
law1.setElasticFlag(elastic1)
law1.setHighOrderFlag(highorder1)

# micro-geometry
meshfile="shearlayerQuad8Elements.msh" # name of mesh file

# creation of  micro part Domain
fulldg= 1
beta1  = 1000
bulkpert =0
interpert = 0
perturbation = 1e-6

nfield1 = 1 # number of the field (physical number of entity)
myfield1 = hoDGDomain(1000,nfield1,0,lawnum1,fulldg)
myfield1.stabilityParameters(beta1)
myfield1.matrixByPerturbation(bulkpert,interpert,interpert,perturbation)
#myfield1.gaussIntegration(hoDGDomain.Gauss,2,2)

nfield2 = 2 # number of the field (physical number of entity)
myfield2 = hoDGDomain(1000,nfield2,0,lawnum1,fulldg)
myfield2.stabilityParameters(beta1)
myfield2.matrixByPerturbation(bulkpert,interpert,interpert,perturbation)
#myfield2.gaussIntegration(hoDGDomain.Gauss,2,2)

nfield3 = 3 # number of the field (physical number of entity)
myfield3 = hoDGDomain(1000,nfield3,0,lawnum1,fulldg)
myfield3.stabilityParameters(beta1)
myfield3.matrixByPerturbation(bulkpert,interpert,interpert,perturbation)
#myfield3.gaussIntegration(hoDGDomain.Gauss,2,2)


myfieldinter1 = hoDGInterDomain(1000,myfield1,myfield2)
myfieldinter1.stabilityParameters(beta1)
myfieldinter1.matrixByPerturbation(interpert,perturbation)
#myfieldinter1.gaussIntegration(hoDGDomain.Gauss,2,2)

myfieldinter2 = hoDGInterDomain(1000,myfield1,myfield3)
myfieldinter2.stabilityParameters(beta1)
myfieldinter2.matrixByPerturbation(interpert,perturbation)
#myfieldinter.gaussIntegration(hoDGDomain.Gauss,2,2)


# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =2 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1  # number of step (used only if soltype=1)
ftime =1e-7  # Final time (used only if soltype=1)
tol=1.e-4  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=50 # Number of step between 2 archiving (used only if soltype=1)



# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)

mysolver.addMaterialLaw(law1)

mysolver.addDomain(myfield1)
mysolver.addDomain(myfield2)
mysolver.addDomain(myfield3)
mysolver.addDomain(myfieldinter1)
mysolver.addDomain(myfieldinter2)

mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.explicitSpectralRadius(ftime,0.001,1.)
mysolver.explicitTimeStepEvaluation(nstep)
mysolver.stepBetweenArchiving(nstepArch)

# boundary condition

mysolver.displacementBC("Volume",1,1,0.0)
mysolver.displacementBC("Volume",1,2,0.0)

mysolver.displacementBC("Volume",2,0,1)
mysolver.displacementBC("Volume",2,1,0)
mysolver.displacementBC("Volume",2,2,0)

mysolver.displacementBC("Volume",3,0,0)
mysolver.displacementBC("Volume",3,1,0)
mysolver.displacementBC("Volume",3,2,0)


# archivage
mysolver.internalPointBuildView("F_xx",IPField.F_XX, 1, 1);
mysolver.internalPointBuildView("F_xy",IPField.F_XY, 1, 1);
mysolver.internalPointBuildView("F_xz",IPField.F_XZ, 1, 1);
mysolver.internalPointBuildView("F_yx",IPField.F_YX, 1, 1);
mysolver.internalPointBuildView("F_yy",IPField.F_YY, 1, 1);
mysolver.internalPointBuildView("F_yz",IPField.F_YZ, 1, 1);
mysolver.internalPointBuildView("F_zx",IPField.F_ZX, 1, 1);
mysolver.internalPointBuildView("F_zy",IPField.F_ZY, 1, 1);
mysolver.internalPointBuildView("F_zz",IPField.F_ZZ, 1, 1);


mysolver.internalPointBuildView("G_xxx",IPField.G_XXX, 1, 1);
mysolver.internalPointBuildView("G_xyy",IPField.G_XYY, 1, 1);
mysolver.internalPointBuildView("G_xzz",IPField.G_XZZ, 1, 1);
mysolver.internalPointBuildView("G_xxy",IPField.G_XXY, 1, 1);
mysolver.internalPointBuildView("G_xxz",IPField.G_XXZ, 1, 1);
mysolver.internalPointBuildView("G_xyz",IPField.G_XYZ, 1, 1);

mysolver.internalPointBuildView("G_yxx",IPField.G_YXX, 1, 1);
mysolver.internalPointBuildView("G_yyy",IPField.G_YYY, 1, 1);
mysolver.internalPointBuildView("G_yzz",IPField.G_YZZ, 1, 1);
mysolver.internalPointBuildView("G_yxy",IPField.G_YXY, 1, 1);
mysolver.internalPointBuildView("G_yxz",IPField.G_YXZ, 1, 1);
mysolver.internalPointBuildView("G_yyz",IPField.G_YYZ, 1, 1);

mysolver.internalPointBuildView("G_zxx",IPField.G_ZXX, 1, 1);
mysolver.internalPointBuildView("G_zyy",IPField.G_ZYY, 1, 1);
mysolver.internalPointBuildView("G_zzz",IPField.G_ZZZ, 1, 1);
mysolver.internalPointBuildView("G_zxy",IPField.G_ZXY, 1, 1);
mysolver.internalPointBuildView("G_zxz",IPField.G_ZXZ, 1, 1);
mysolver.internalPointBuildView("G_zyz",IPField.G_ZYZ, 1, 1);



mysolver.internalPointBuildView("strain_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("strain_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("strain_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("strain_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("strain_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("strain_xz",IPField.STRAIN_XZ, 1, 1);

mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);

mysolver.internalPointBuildView("High-order strain norm",IPField.HIGHSTRAIN_NORM, 1, 1);
mysolver.internalPointBuildView("High-order stress norm",IPField.HIGHSTRESS_NORM, 1, 1);
mysolver.internalPointBuildView("Green -Lagrange strain norm",IPField.GL_NORM, 1, 1);

mysolver.archivingForceOnPhysicalGroup("Volume",2,0)
mysolver.archivingForceOnPhysicalGroup("Volume",2,1)
mysolver.archivingForceOnPhysicalGroup("Volume",2,2)


# solve
mysolver.solve()
