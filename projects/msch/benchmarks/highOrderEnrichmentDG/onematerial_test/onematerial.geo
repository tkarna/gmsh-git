Point(1) = {0, 0, 0, 1.0};
Point(2) = {0.1, 0, 0, 1.0};
Point(3) = {0.1, 0.1, 0, 1.0};
Point(4) = {0., 0.1, 0, 1.0};
Line(1) = {4, 3};
Line(2) = {3, 2};
Line(3) = {2, 1};
Line(4) = {1, 4};
Line Loop(5) = {1, 2, 3, 4};
Plane Surface(6) = {5};
Extrude {0, 0, 1} {
  Surface{6};
}

Extrude {0, 0, 0.1} {
  Surface{28};
}
Extrude {0, 0, -0.1} {
  Surface{6};
}
Physical Volume(73) = {3};
Physical Volume(74) = {2};
Physical Volume(75) = {1};
Transfinite Line {22, 13, 18, 14} = 10 Using Progression 1;
Transfinite Line {4, 66, 55, 57, 3, 2, 1, 62, 58, 52, 53, 54, 44, 32, 30, 33, 11, 35, 10, 8, 9, 40, 36, 31} = 2 Using Progression 1;
Transfinite Surface {37, 49, 50, 28, 45, 41, 15, 27, 19, 23, 59, 71, 6, 72, 63, 67};
Recombine Surface {37, 49, 50, 28, 45, 41, 15, 27, 19, 23, 59, 71, 6, 72, 63, 67};
Transfinite Volume {2, 1, 3};
