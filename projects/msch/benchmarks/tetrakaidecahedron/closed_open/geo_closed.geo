lsca = 0.17;
f = 0.99;
gamma = 0.2;


Point(1) = {0,2,0,lsca};
Point(2) = {0,2,1,lsca};
Point(3) = {0,2,2,lsca};
Point(4) = {0,1,2,lsca};
Point(5) = {0,0,2,lsca};
Point(6) = {1,0,2,lsca};
Point(7) = {2,0,2,lsca};
Point(8) = {2,0,1,lsca};
Point(9) = {2,0,0,lsca};
Point(10) = {2,1,0,lsca};
Point(11) = {2,2,0,lsca};
Point(12) = {1,2,0,lsca};

Point(101) = {f*0,f*2,f*0,lsca};
Point(102) = {f*0,f*2,f*1,lsca};
Point(104) = {f*0,f*1,f*2,lsca};
Point(105) = {f*0,f*0,f*2,lsca};
Point(106) = {f*1,f*0,f*2,lsca};
Point(108) = {f*2,f*0,f*1,lsca};
Point(109) = {f*2,f*0,f*0,lsca};
Point(1010) = {f*2,f*1,f*0,lsca};
Point(1012) = {f*1,f*2,f*0,lsca};

Line(1) = {2, 3};
Line(2) = {3, 4};
Line(3) = {4, 5};
Line(4) = {2, 1};
Line(5) = {1, 12};
Line(6) = {2, 12};
Line(7) = {2, 4};
Line(8) = {4, 6};
Line(9) = {6, 8};
Line(10) = {12, 10};
Line(11) = {10, 8};
Line(12) = {6, 7};
Line(13) = {7, 8};
Line(14) = {8, 9};
Line(15) = {9, 10};
Line(16) = {10, 11};
Line(17) = {11, 12};
Line(18) = {5, 6};
Line(20) = {102, 104};
Line(23) = {106, 108};
Line(26) = {1010, 1012};
Line(28) = {108, 1010};
Line(29) = {1012, 102};
Line(30) = {104, 106};

Point(1014) = {1.5, 0, 1.5, lsca};
Point(1015) = {3-1.5*f, 0, 3-1.5*f, lsca};

Point(1016) = {0, 3-1.5*f, 3-1.5*f, lsca};
Point(1017) = {3-1.5*f, 3-1.5*f,0, lsca};

alpha = Fabs(1-1.5*f)*2;
Printf(" val = %e",alpha);

Point(1018) = {2-alpha, 0,2, lsca};
Point(1019) = {2, 0,2-alpha, lsca};
Point(1020) = {0, 2-alpha, 2, lsca};
Point(1021) = {0, 2, 2-alpha, lsca};

Point(1022) = {2, 2-alpha, 0, lsca};
Point(1023) = { 2-alpha, 2, 0, lsca};
Point(1024) = { 2, 2*(1-f), 2, lsca};
Point(1025) = { 2*(1-f), 2, 2,lsca};
Point(1026) = { 2, 2, 2*(1-f), lsca};
Line(38) = {7, 1024};
Line(39) = {1025, 3};
Line(40) = {11, 1026};
beta = 1-f +0.5*alpha;
Point(1027) = { 2, 2-2*beta, 2*(1-f), lsca};
Point(1028) = { 2, 2*(1-f), 2-2*beta, lsca};
Point(1029) = { 2-2*beta, 2*(1-f), 2,   lsca};
Point(1030) = { 2*(1-f), 2-2*beta,  2,   lsca};
Point(1031) = { 2*(1-f),   2, 2-2*beta,   lsca};
Point(1032) = { 2-2*beta,   2,  2*(1-f),   lsca};
Line(47) = {1031, 1030};
Line(48) = {1032, 1027};
Line(49) = {1028, 1029};

Line(50) = {1032, 1031};
Line(51) = {1030, 1029};
Line(52) = {1028, 1027};
Line(53) = {9, 109};
Line(54) = {105, 5};
Line(55) = {101, 1};


Point(1050) = { 2*(1-f), 2-2*beta+2*beta*gamma, 2,  lsca};
Point(1051) = { 2*(1-f), 2, 2-2*beta+2*beta*gamma,  lsca};
Point(1052) = { 2-2*beta+2*beta*gamma,  2, 2*(1-f),  lsca};
Point(1053) = { 2, 2-2*beta+2*beta*gamma,   2*(1-f),  lsca};
Point(1054) = { 2-2*beta+2*beta*gamma,   2*(1-f), 2,   lsca};
Point(1055) = { 2, 2*(1-f), 2-2*beta+2*beta*gamma,   lsca};
Line(102) = {1050, 1051};
Line(103) = {1052, 1053};
Line(104) = {1054, 1055};
Point(1056) = { 2*(1-f)+gamma*f, 2-2*beta, 2-gamma*f,   lsca};
Point(1057) = { 2-2*beta,2*(1-f)+gamma*f, 2-gamma*f,   lsca};
Point(1058) = { 2-2*beta, 2-gamma*f, 2*(1-f)+gamma*f,   lsca};
Point(1059) = { 2*(1-f)+gamma*f, 2-gamma*f, 2-2*beta,   lsca};
Point(1060) = {  2-gamma*f, 2*(1-f)+gamma*f, 2-2*beta,   lsca};
Point(1061) = {  2-gamma*f, 2-2*beta, 2*(1-f)+gamma*f,    lsca};
Line(105) = {1056, 1059};
Line(106) = {1059, 1058};
Line(107) = {1058, 1061};
Line(108) = {1061, 1060};
Line(109) = {1060, 1057};
Line(110) = {1057, 1056};
Line(111) = {1056, 1050};
Line(112) = {1050, 1054};
Line(113) = {1054, 1057};
Line(114) = {1055, 1060};
Line(115) = {1061, 1053};
Line(116) = {1055, 1053};
Line(117) = {1058, 1052};
Line(118) = {1031, 1059};
Line(119) = {1051, 1059};
Line(120) = {1051, 1052};

Point(1062) = {0, 2*f, f-gamma*f, lsca};
Point(1063) = { f-gamma*f, 2*f, 0,  lsca};
Point(1064) = { f-gamma*f, 0,  2*f,  lsca};
Point(1065) = {0, f-gamma*f, 2*f,  lsca};
Point(1066) = { 2*f, 0,f-gamma*f, lsca};
Point(1067) = { 2*f, f-gamma*f, 0, lsca};
Point(1068) = { f, 2*f-gamma*f, gamma*f, lsca};
Point(1069) = { f, gamma*f, 2*f-gamma*f, lsca};
Point(1070) = { 2*f-gamma*f, gamma*f, f,  lsca};
Point(1071) = { 2*f-gamma*f, f,gamma*f,   lsca};
Point(1072) = {gamma*f,  f, 2*f-gamma*f,  lsca};
Point(1073) = {gamma*f, 2*f-gamma*f, f,   lsca};
Line(121) = {1067, 1071};
Line(122) = {1071, 1070};
Line(123) = {1070, 1066};
Line(124) = {1066, 1067};
Line(125) = {1067, 1063};
Line(126) = {1063, 1068};
Line(127) = {1068, 1073};
Line(128) = {1073, 1062};
Line(129) = {1062, 1063};
Line(130) = {1071, 1068};
Line(131) = {1073, 1072};
Line(132) = {1069, 1072};
Line(133) = {1069, 1070};
Line(134) = {1069, 1064};
Line(135) = {1065, 1072};
Line(136) = {1065, 1062};
Line(137) = {1064, 1066};
Line(138) = {108, 1070};
Line(139) = {1010, 1071};
Line(140) = {1012, 1068};
Line(141) = {1073, 102};
Line(142) = {1072, 104};
Line(143) = {1027, 1061};
Line(144) = {1028, 1060};
Line(145) = {1056, 1030};
Line(146) = {1032, 1058};
Line(147) = {1030, 1050};
Line(148) = {1050, 1025};
Line(149) = {1025, 1051};
Line(150) = {1051, 1031};
Line(151) = {1032, 1052};
Line(152) = {1052, 1026};
Line(153) = {1026, 1053};
Line(154) = {1053, 1027};
Line(155) = {1024, 1055};
Line(156) = {1055, 1028};
Line(157) = {1024, 1054};
Line(158) = {1054, 1029};
Line(159) = {106, 1064};
Line(160) = {1064, 105};
Line(161) = {108, 1066};
Line(162) = {1066, 109};
Line(163) = {109, 1067};
Line(164) = {1067, 1010};
Line(165) = {1012, 1063};
Line(166) = {1063, 101};
Line(167) = {101, 1062};
Line(168) = {1062, 102};
Line(169) = {104, 1065};
Line(170) = {1065, 105};
Line(171) = {1069, 106};
Line(172) = {1057, 1029};

Delete {
  Line{9, 8, 7, 6, 10, 11};
}
Line(173) = {1057, 1069};
Line(174) = {1056, 1072};
Line(175) = {1060, 1070};
Line(176) = {1061, 1071};
Line(177) = {1058, 1068};
Line(178) = {1059, 1073};

Point(1080) = {0, 2, f-gamma*f, lsca};
Point(1081) = { f-gamma*f, 2, 0, lsca};

Line(179) = {1080, 1062};
Line(180) = {1081, 1063};
Line(181) = {1080, 1081};
Delete {
  Line{4, 5};
}
Line(182) = {1081, 12};
Line(183) = {1080, 2};
Delete {
  Line{167, 166, 55};
}

Point(1082) = {0,2-2*beta+2*beta*gamma, 2, lsca};
Point(1083) = {0, 2, 2-2*beta+2*beta*gamma,  lsca};
Line(184) = {1050, 1082};
Line(185) = {1051, 1083};
Line(186) = {1082, 1083};
Delete {
  Line{148, 149, 39, 1, 2};
}
Line(187) = {4, 1082};
Line(188) = {2, 1083};

Point(1084) = {2, 0,  2-2*beta+2*beta*gamma,  lsca};
Point(1085) = {2,2-2*beta+2*beta*gamma,  0,   lsca};

Point(1086) = {2-2*beta+2*beta*gamma, 2,  0,   lsca};

Point(1087) = {2-2*beta+2*beta*gamma,  0, 2,  lsca};
Line(189) = {1054, 1087};

Line(191) = {1055, 1084};
Line(192) = {1084, 1087};
Delete {
  Line{13, 155, 157, 38, 12};
}
Line(193) = {8, 1084};
Line(194) = {1087, 6};

Line(195) = {1053, 1085};
Line(196) = {1086, 1085};
Line(197) = {1086, 1052};
Delete {
  Line{17, 152, 40, 16, 153};
}
Line(198) = {1085, 10};
Line(199) = {1086, 12};

Point(1089) = { 0, f-gamma*f, 2, lsca};
Point(1090) = { f-gamma*f, 0, 2, lsca};
Line(200) = {1064, 1090};
Line(201) = {1090, 1089};
Line(202) = {1089, 1065};
Line(203) = {1065, 1064};
Delete {
  Line{160, 18, 54, 3, 170};
}
Line(204) = {1090, 6};
Line(205) = {1089, 4};

Point(1091) = {2, 0,  f-gamma*f, lsca};
Point(1092) = { 2,f-gamma*f, 0, lsca};


Line(206) = {1066, 1091};
Line(207) = {1091, 1092};
Line(208) = {1092, 1067};
Delete {
  Line{162, 14, 15, 163, 53};
}
Line(209) = {8, 1091};
Line(210) = {1092, 10};

Line(369) = {1082, 3};
Line(370) = {3, 1083};
Line(371) = {3, 1025};
Line(372) = {1025, 1051};
Line(373) = {1025, 1050};

Line(384) = {1053, 1026};
Line(385) = {1026, 1052};
Line(386) = {1086, 11};
Line(387) = {11, 1026};
Line(388) = {11, 1085};

Line(399) = {1084, 7};
Line(400) = {7, 1087};
Line(401) = {1024, 7};
Line(402) = {1055, 1024};
Line(403) = {1054, 1024};

Line(414) = {1080, 1};
Line(415) = {1, 101};
Line(416) = {101, 1063};
Line(417) = {1081, 1};
Line(418) = {1062, 101};

Line(429) = {9, 109};
Line(430) = {109, 1067};
Line(431) = {109, 1066};
Line(432) = {1091, 9};
Line(433) = {1092, 9};

Line(444) = {1089, 5};
Line(445) = {5, 1090};
Line(446) = {5, 105};
Line(447) = {105, 1065};
Line(448) = {105, 1064};


Delete {
  Line{47, 145};
}
Delete {
  Line{150, 118, 50};
}
Delete {
  Line{186, 184, 185, 181, 197, 151, 146, 48};
}
Delete {
  Line{195, 196, 154, 143, 52, 144, 156, 49};
}
Delete {
  Line{191};
}
Delete {
  Line{192, 189};
}
Delete {
  Line{51, 158, 147, 172};
}
Delete {
  Line{161, 28, 138, 23};
}
Delete {
  Line{26, 139, 164};
}
Delete {
  Line{171, 159, 30};
}
Delete {
  Line{142, 169, 20};
}
Delete {
  Line{141, 168, 29};
}
Delete {
  Line{179, 180};
}
Delete {
  Line{165, 140};
}
Delete {
  Line{208, 207, 206};
}
Delete {
  Line{201, 200, 202};
}
Line Loop(449) = {125, 126, -130, -121};
Plane Surface(450) = {449};
Line Loop(451) = {126, 127, 128, 129};
Plane Surface(452) = {451};
Line Loop(453) = {128, -136, 135, -131};
Plane Surface(454) = {453};
Line Loop(455) = {135, -132, 134, -203};
Plane Surface(456) = {455};
Line Loop(457) = {134, 137, -123, -133};
Plane Surface(458) = {457};
Line Loop(459) = {123, 124, 121, 122};
Plane Surface(460) = {459};
Line Loop(461) = {116, -115, 108, -114};
Plane Surface(462) = {461};
Line Loop(463) = {114, 109, -113, 104};
Plane Surface(464) = {463};
Line Loop(465) = {113, 110, 111, 112};
Plane Surface(466) = {465};
Line Loop(467) = {105, -119, -102, -111};
Plane Surface(468) = {467};
Line Loop(469) = {106, 117, -120, 119};
Plane Surface(470) = {469};
Line Loop(471) = {115, -103, -117, 107};
Plane Surface(472) = {471};
Line Loop(473) = {116, 384, -387, 388, 198, -210, 433, -432, -209, 193, 399, -401, -402};
Plane Surface(474) = {473};

Line Loop(475) = {387, 385, -120, -372, -371, 370, -188, -183, 414, -417, 182, -199, 386};
Plane Surface(476) = {475};
Line Loop(477) = {371, 373, 112, 403, 401, 400, 194, -204, -445, -444, 205, 187, 369};
Plane Surface(478) = {477};
Line Loop(479) = {137, -431, -429, -432, -209, 193, 399, 400, 194, -204, -445, 446, 448};
Plane Surface(480) = {479};
Line Loop(481) = {182, -199, 386, 388, 198, -210, 433, 429, 430, 125, -416, -415, -417};
Plane Surface(482) = {481};
Line Loop(483) = {446, 447, 136, 418, -415, -414, 183, 188, -370, -369, -187, -205, 444};
Plane Surface(484) = {483};
Line Loop(485) = {447, 203, -448};
Plane Surface(486) = {485};
Line Loop(487) = {129, -416, -418};
Plane Surface(488) = {487};
Line Loop(489) = {430, -124, -431};
Plane Surface(490) = {489};
Line Loop(491) = {402, -403, 104};
Plane Surface(492) = {491};
Line Loop(493) = {103, 384, 385};
Plane Surface(494) = {493};
Line Loop(495) = {102, -372, 373};
Plane Surface(496) = {495};
Line Loop(497) = {108, 109, 110, 105, 106, 107};
Plane Surface(498) = {497};
Line Loop(499) = {133, -122, 130, 127, 131, -132};
Plane Surface(500) = {499};
Surface Loop(501) = {496, 468, 498, 462, 474, 494, 472, 470, 476, 478, 466, 464, 492, 480, 458, 456, 454, 452, 450, 482, 490, 460, 500, 488, 484, 486};
Volume(502) = {501};
Coherence;

Delete {
  Line{178, 177, 176, 175, 173, 174};
}
Symmetry {1, 0, 0, 0} {
  Duplicata { Volume{502}; }
}
Symmetry {0, 1, 0, 0} {
  Duplicata { Volume{502, 503}; }
}
Symmetry {0, 0,1, -2} {
  Duplicata { Volume{502, 683, 503, 866}; }
}
Physical Volume(11) = {1046, 1229, 1412, 1595, 502, 503, 683, 866};
Physical Surface(1) = {1068, 474, 705, 1251};
Physical Surface(2) = {888, 525, 1434, 1617};
Physical Surface(3) = {553, 476, 1096, 1462};
Physical Surface(4) = {916, 733, 1279, 1645};
Physical Surface(5) = {1360, 1177, 1543, 1726};
Physical Surface(6) = {482, 814, 997, 634};
