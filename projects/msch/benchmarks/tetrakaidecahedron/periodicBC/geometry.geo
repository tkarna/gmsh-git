f = 0.975;
lsca = 10*(1-f);
p1 = newp; Point(p1) =  {1, 0, 2, lsca};
p2 = newp; Point(p2) =  {0, 1, 2, lsca};
p3 = newp; Point(p3) =  {-1, 0, 2, lsca};
p4 = newp; Point(p4) =  {0, -1, 2, lsca};
p5 = newp; Point(p5) =  {2, 0, 1, lsca};
p6 = newp; Point(p6) =  {0, 2, 1, lsca};
p7 = newp; Point(p7) =  {-2, 0, 1, lsca};
p8 = newp; Point(p8) =  {0, -2, 1, lsca};
p9 = newp; Point(p9) =  {2, 1, 0, lsca};
p10 = newp; Point(p10) =  {1, 2, 0, lsca};
p11 = newp; Point(p11) =  {-1, 2, 0, lsca};
p12 = newp; Point(p12) =  {-2, 1, 0, lsca};
p13 = newp; Point(p13) =  {-2, -1, 0, lsca};
p14 = newp; Point(p14) =  {-1, -2, 0,lsca};
p15 = newp; Point(p15) =  {1, -2, 0, lsca};
p16 = newp; Point(p16) =  {2, -1, 0, lsca};
p17 = newp; Point(p17) =  {2, 0, -1, lsca};
p18 = newp; Point(p18) =  {0, 2, -1, lsca};
p19 = newp; Point(p19) =  {-2, 0, -1, lsca};
p20 = newp; Point(p20) =  {0, -2, -1, lsca};
p21 = newp; Point(p21) =  {1, 0, -2, lsca};
p22 = newp; Point(p22) =  {0, 1, -2, lsca};
p23 = newp; Point(p23) =  {-1, 0, -2, lsca};
p24 = newp; Point(p24) =  {0, -1, -2, lsca};
p25 = newp; Point(p25) = {2, 0, 0, lsca};
p26 = newp; Point(p26) = {-2, 0, 0, lsca};
p27 = newp; Point(p27) = {0, 2, 0, lsca};
p28 = newp; Point(p28) = {0, -2, 0, lsca};
p29 = newp; Point(p29) = {0, 0, 2, lsca};
p30 = newp; Point(p30) = {0, 0, -2, lsca};

l1 = newl; Line(l1) = {p1, p2};
l2 = newl; Line(l2) = {p1, p4};
l3 = newl; Line(l3) = {p1, p5};
l4 = newl; Line(l4) = {p2, p3};
l5 = newl; Line(l5) = {p2, p6};
l6 = newl; Line(l6) = {p3, p4};
l7 = newl; Line(l7) = {p3, p7};
l8 = newl; Line(l8) = {p4, p8};
l9 = newl; Line(l9) = {p5, p9};
l10 = newl; Line(l10) = {p5, p16};
l11 = newl; Line(l11) = {p6, p10};
l12 = newl; Line(l12) = {p6, p11};
l13 = newl; Line(l13) = {p7, p12};
l14 = newl; Line(l14) = {p7, p13};
l15 = newl; Line(l15) = {p8, p14};
l16 = newl; Line(l16) = {p8, p15};
l17 = newl; Line(l17) = {p9, p10};
l18 = newl; Line(l18) = {p9, p17};
l19 = newl; Line(l19) = {p10, p18};
l20 = newl; Line(l20) = {p11, p12};
l21 = newl; Line(l21) = {p11, p18};
l22 = newl; Line(l22) = {p12, p19};
l23 = newl; Line(l23) = {p13, p14};
l24 = newl; Line(l24) = {p13, p19};
l25 = newl; Line(l25) = {p14, p20};
l26 = newl; Line(l26) = {p15, p16};
l27 = newl; Line(l27) = {p15, p20};
l28 = newl; Line(l28) = {p16, p17};
l29 = newl; Line(l29) = {p17, p21};
l30 = newl; Line(l30) = {p18, p22};
l31 = newl; Line(l31) = {p19, p23};
l32 = newl; Line(l32) = {p20, p24};
l33 = newl; Line(l33) = {p21, p22};
l34 = newl; Line(l34) = {p21, p24};
l35 = newl; Line(l35) = {p22, p23};
l36 = newl; Line(l36) = {p23, p24};
l37 = newl; Line(l37) = {p27, p18};
l38 = newl; Line(l38) = {p27, p10};
l39 = newl; Line(l39) = {p27, p6};
l40 = newl; Line(l40) = {p27, p11};
l41 = newl; Line(l41) = {p26, p12};
l42 = newl; Line(l42) = {p26, p19};
l43 = newl; Line(l43) = {p26, p13};
l44 = newl; Line(l44) = {p26, p7};
l45 = newl; Line(l45) = {p22, p30};
l46 = newl; Line(l46) = {p23, p30};
l47 = newl; Line(l47) = {p21, p30};
l48 = newl; Line(l48) = {p24, p30};
l49 = newl; Line(l49) = {p14, p28};
l50 = newl; Line(l50) = {p20, p28};
l51 = newl; Line(l51) = {p15, p28};
l52 = newl; Line(l52) = {p8, p28};
l53 = newl; Line(l53) = {p9, p25};
l54 = newl; Line(l54) = {p17, p25};
l55 = newl; Line(l55) = {p16, p25};
l56 = newl; Line(l56) = {p5, p25};
l57 = newl; Line(l57) = {p29, p2};
l58 = newl; Line(l58) = {p3, p29};
l59 = newl; Line(l59) = {p4, p29};
l60 = newl; Line(l60) = {p1, p29};

ll1 = newll; Line Loop(ll1) = {l2, l8, l16, l26, -l10, -l3};
ll2 = newll; Line Loop(ll2) = {l26, l28, l29, l34, -l32, -l27};
ll3 = newll; Line Loop(ll3) = {l35, -l31, -l22, -l20, l21, l30};
ll4 = newll; Line Loop(ll4) = {l20, -l13, -l7, -l4, l5, l12};
ll4 = newll; Line Loop(ll4) = {l20, -l13, -l7, -l4, l5, l12};
ll5 = newll; Line Loop(ll5) = {l6, l8, l15, -l23, -l14, -l7};
ll6 = newll; Line Loop(ll6) = {l32, -l36, -l31, -l24, l23, l25};
ll7 = newll; Line Loop(ll7) = {l30, -l33, -l29, -l18, l17, l19};
ll8 = newll; Line Loop(ll8) = {l17, -l11, -l5, -l1, l3, l9};
ll9 = newll; Line Loop(ll9) = {l57, l4, l58};
ll10 = newll; Line Loop(ll10) = {l58, -l59, -l6};
ll11 = newll; Line Loop(ll11) = {l59, -l60, l2};
ll12 = newll; Line Loop(ll12) = {l60, l57, -l1};
ll13 = newll; Line Loop(ll13) = {l33, l45, -l47};
ll14 = newll; Line Loop(ll14) = {l47, -l48, -l34};
ll15 = newll; Line Loop(ll15) = {l48, -l46, l36};
ll16 = newll; Line Loop(ll16) = {l46, -l45, l35};
ll17 = newll; Line Loop(ll17) = {l9, l53, -l56};
ll18 = newll; Line Loop(ll18) = {l56, -l55, -l10};
ll19 = newll; Line Loop(ll19) = {l55, -l54, -l28};
ll20 = newll; Line Loop(ll20) = {l54, -l53, l18};
ll21 = newll; Line Loop(ll21) = {l41, -l13, -l44};
ll22 = newll; Line Loop(ll22) = {l44, l14, -l43};
ll23 = newll; Line Loop(ll23) = {l43, l24, -l42};
ll24 = newll; Line Loop(ll24) = {l42, -l22, -l41};
ll25 = newll; Line Loop(ll25) = {l38, l19, -l37};
ll26 = newll; Line Loop(ll26) = {l38, -l11, -l39};
ll27 = newll; Line Loop(ll27) = {l12, -l40, l39};
ll28 = newll; Line Loop(ll28) = {l40, l21, -l37};
ll29 = newll; Line Loop(ll29) = {l25, l50, -l49};
ll30 =newll; Line Loop(ll30) = {l50, -l51, l27};
ll31 = newll; Line Loop(ll31) = {l51, -l52, l16};
ll32 = newll; Line Loop(ll32) = {l52, -l49, -l15};

s1 = news; Plane Surface(s1) = {ll1};
s2 = news; Plane Surface(s2) = {ll2};
s3 = news; Plane Surface(s3) = {ll3};
s4 = news; Plane Surface(s4) = {ll4};
s5 = news; Plane Surface(s5) = {ll5};
s6 = news; Plane Surface(s6) = {ll6};
s7 = news; Plane Surface(s7) = {ll7};
s8 = news; Plane Surface(s8) = {ll8};
s9 = news; Plane Surface(s9) = {ll9};
s10 = news; Plane Surface(s10) = {ll10};
s11 = news; Plane Surface(s11) = {ll11};
s12 = news; Plane Surface(s12) = {ll12};
s13 = news; Plane Surface(s13) = {ll13};
s14 = news; Plane Surface(s14) = {ll14};
s15 = news; Plane Surface(s15) = {ll15};
s16 = news; Plane Surface(s16) = {ll16};
s17 = news; Plane Surface(s17) = {ll17};
s18 = news; Plane Surface(s18) = {ll18};
s19 = news; Plane Surface(s19) = {ll19};
s20 = news; Plane Surface(s20) = {ll20};
s21 = news; Plane Surface(s21) = {ll21};
s22 = news; Plane Surface(s22) = {ll22};
s23 = news; Plane Surface(s23) = {ll23};
s24 = news; Plane Surface(s24) = {ll24};
s25 = news; Plane Surface(s25) = {ll25};
s26 = news; Plane Surface(s26) = {ll26};
s27 = news; Plane Surface(s27) = {ll27};
s28 = news; Plane Surface(s28) = {ll28};
s29 = news; Plane Surface(s29) = {ll29};
s30 = news; Plane Surface(s30) = {ll30};
s31 = news; Plane Surface(s31) = {ll31};
s32 = news; Plane Surface(s32) = {ll32};

faceloop1 = newreg; Surface Loop(faceloop1) = {s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,s11,s12,s13,s14,s15,s16,s17,s18,s19,s20,s21,s22,s23,s24,s25,s26,s27,s28,s29,s30,s31,s32};


// crop faces

p1 = newp; Point(p1) =  {1*f, 0*f, 2*f, lsca};
p2 = newp; Point(p2) =  {0*f, 1*f, 2*f, lsca};
p3 = newp; Point(p3) =  {-1*f, 0*f, 2*f, lsca};
p4 = newp; Point(p4) =  {0*f, -1*f, 2*f, lsca};
p5 = newp; Point(p5) =  {2*f, 0*f, 1*f, lsca};
p6 = newp; Point(p6) =  {0*f, 2*f, 1*f, lsca};
p7 = newp; Point(p7) =  {-2*f, 0*f, 1*f, lsca};
p8 = newp; Point(p8) =  {0*f, -2*f, 1*f, lsca};
p9 = newp; Point(p9) =  {2*f, 1*f, 0*f, lsca};
p10 = newp; Point(p10) =  {1*f, 2*f, 0*f, lsca};
p11 = newp; Point(p11) =  {-1*f, 2*f, 0*f, lsca};
p12 = newp; Point(p12) =  {-2*f, 1*f, 0*f, lsca};
p13 = newp; Point(p13) =  {-2*f, -1*f, 0*f, lsca};
p14 = newp; Point(p14) =  {-1*f, -2*f, 0*f,lsca};
p15 = newp; Point(p15) =  {1*f, -2*f, 0*f, lsca};
p16 = newp; Point(p16) =  {2*f, -1*f, 0*f, lsca};
p17 = newp; Point(p17) =  {2*f, 0*f, -1*f, lsca};
p18 = newp; Point(p18) =  {0*f, 2*f, -1*f, lsca};
p19 = newp; Point(p19) =  {-2*f, 0*f, -1*f, lsca};
p20 = newp; Point(p20) =  {0*f, -2*f, -1*f, lsca};
p21 = newp; Point(p21) =  {1*f, 0*f, -2*f, lsca};
p22 = newp; Point(p22) =  {0*f, 1*f, -2*f, lsca};
p23 = newp; Point(p23) =  {-1*f, 0*f, -2*f, lsca};
p24 = newp; Point(p24) =  {0*f, -1*f, -2*f, lsca};
p25 = newp; Point(p25) = {2*f, 0*f, 0*f, lsca};
p26 = newp; Point(p26) = {-2*f, 0*f, 0*f, lsca};
p27 = newp; Point(p27) = {0*f, 2*f, 0*f, lsca};
p28 = newp; Point(p28) = {0*f, -2*f, 0*f, lsca};
p29 = newp; Point(p29) = {0*f, 0*f, 2*f, lsca};
p30 = newp; Point(p30) = {0*f, 0*f, -2*f, lsca};

l1 = newl; Line(l1) = {p1, p2};
l2 = newl; Line(l2) = {p1, p4};
l3 = newl; Line(l3) = {p1, p5};
l4 = newl; Line(l4) = {p2, p3};
l5 = newl; Line(l5) = {p2, p6};
l6 = newl; Line(l6) = {p3, p4};
l7 = newl; Line(l7) = {p3, p7};
l8 = newl; Line(l8) = {p4, p8};
l9 = newl; Line(l9) = {p5, p9};
l10 = newl; Line(l10) = {p5, p16};
l11 = newl; Line(l11) = {p6, p10};
l12 = newl; Line(l12) = {p6, p11};
l13 = newl; Line(l13) = {p7, p12};
l14 = newl; Line(l14) = {p7, p13};
l15 = newl; Line(l15) = {p8, p14};
l16 = newl; Line(l16) = {p8, p15};
l17 = newl; Line(l17) = {p9, p10};
l18 = newl; Line(l18) = {p9, p17};
l19 = newl; Line(l19) = {p10, p18};
l20 = newl; Line(l20) = {p11, p12};
l21 = newl; Line(l21) = {p11, p18};
l22 = newl; Line(l22) = {p12, p19};
l23 = newl; Line(l23) = {p13, p14};
l24 = newl; Line(l24) = {p13, p19};
l25 = newl; Line(l25) = {p14, p20};
l26 = newl; Line(l26) = {p15, p16};
l27 = newl; Line(l27) = {p15, p20};
l28 = newl; Line(l28) = {p16, p17};
l29 = newl; Line(l29) = {p17, p21};
l30 = newl; Line(l30) = {p18, p22};
l31 = newl; Line(l31) = {p19, p23};
l32 = newl; Line(l32) = {p20, p24};
l33 = newl; Line(l33) = {p21, p22};
l34 = newl; Line(l34) = {p21, p24};
l35 = newl; Line(l35) = {p22, p23};
l36 = newl; Line(l36) = {p23, p24};
l37 = newl; Line(l37) = {p27, p18};
l38 = newl; Line(l38) = {p27, p10};
l39 = newl; Line(l39) = {p27, p6};
l40 = newl; Line(l40) = {p27, p11};
l41 = newl; Line(l41) = {p26, p12};
l42 = newl; Line(l42) = {p26, p19};
l43 = newl; Line(l43) = {p26, p13};
l44 = newl; Line(l44) = {p26, p7};
l45 = newl; Line(l45) = {p22, p30};
l46 = newl; Line(l46) = {p23, p30};
l47 = newl; Line(l47) = {p21, p30};
l48 = newl; Line(l48) = {p24, p30};
l49 = newl; Line(l49) = {p14, p28};
l50 = newl; Line(l50) = {p20, p28};
l51 = newl; Line(l51) = {p15, p28};
l52 = newl; Line(l52) = {p8, p28};
l53 = newl; Line(l53) = {p9, p25};
l54 = newl; Line(l54) = {p17, p25};
l55 = newl; Line(l55) = {p16, p25};
l56 = newl; Line(l56) = {p5, p25};
l57 = newl; Line(l57) = {p29, p2};
l58 = newl; Line(l58) = {p3, p29};
l59 = newl; Line(l59) = {p4, p29};
l60 = newl; Line(l60) = {p1, p29};

ll1 = newll; Line Loop(ll1) = {l2, l8, l16, l26, -l10, -l3};
ll2 = newll; Line Loop(ll2) = {l26, l28, l29, l34, -l32, -l27};
ll3 = newll; Line Loop(ll3) = {l35, -l31, -l22, -l20, l21, l30};
ll4 = newll; Line Loop(ll4) = {l20, -l13, -l7, -l4, l5, l12};
ll4 = newll; Line Loop(ll4) = {l20, -l13, -l7, -l4, l5, l12};
ll5 = newll; Line Loop(ll5) = {l6, l8, l15, -l23, -l14, -l7};
ll6 = newll; Line Loop(ll6) = {l32, -l36, -l31, -l24, l23, l25};
ll7 = newll; Line Loop(ll7) = {l30, -l33, -l29, -l18, l17, l19};
ll8 = newll; Line Loop(ll8) = {l17, -l11, -l5, -l1, l3, l9};
ll9 = newll; Line Loop(ll9) = {l57, l4, l58};
ll10 = newll; Line Loop(ll10) = {l58, -l59, -l6};
ll11 = newll; Line Loop(ll11) = {l59, -l60, l2};
ll12 = newll; Line Loop(ll12) = {l60, l57, -l1};
ll13 = newll; Line Loop(ll13) = {l33, l45, -l47};
ll14 = newll; Line Loop(ll14) = {l47, -l48, -l34};
ll15 = newll; Line Loop(ll15) = {l48, -l46, l36};
ll16 = newll; Line Loop(ll16) = {l46, -l45, l35};
ll17 = newll; Line Loop(ll17) = {l9, l53, -l56};
ll18 = newll; Line Loop(ll18) = {l56, -l55, -l10};
ll19 = newll; Line Loop(ll19) = {l55, -l54, -l28};
ll20 = newll; Line Loop(ll20) = {l54, -l53, l18};
ll21 = newll; Line Loop(ll21) = {l41, -l13, -l44};
ll22 = newll; Line Loop(ll22) = {l44, l14, -l43};
ll23 = newll; Line Loop(ll23) = {l43, l24, -l42};
ll24 = newll; Line Loop(ll24) = {l42, -l22, -l41};
ll25 = newll; Line Loop(ll25) = {l38, l19, -l37};
ll26 = newll; Line Loop(ll26) = {l38, -l11, -l39};
ll27 = newll; Line Loop(ll27) = {l12, -l40, l39};
ll28 = newll; Line Loop(ll28) = {l40, l21, -l37};
ll29 = newll; Line Loop(ll29) = {l25, l50, -l49};
ll30 =newll; Line Loop(ll30) = {l50, -l51, l27};
ll31 = newll; Line Loop(ll31) = {l51, -l52, l16};
ll32 = newll; Line Loop(ll32) = {l52, -l49, -l15};

s1 = news; Plane Surface(s1) = {ll1};
s2 = news; Plane Surface(s2) = {ll2};
s3 = news; Plane Surface(s3) = {ll3};
s4 = news; Plane Surface(s4) = {ll4};
s5 = news; Plane Surface(s5) = {ll5};
s6 = news; Plane Surface(s6) = {ll6};
s7 = news; Plane Surface(s7) = {ll7};
s8 = news; Plane Surface(s8) = {ll8};
s9 = news; Plane Surface(s9) = {ll9};
s10 = news; Plane Surface(s10) = {ll10};
s11 = news; Plane Surface(s11) = {ll11};
s12 = news; Plane Surface(s12) = {ll12};
s13 = news; Plane Surface(s13) = {ll13};
s14 = news; Plane Surface(s14) = {ll14};
s15 = news; Plane Surface(s15) = {ll15};
s16 = news; Plane Surface(s16) = {ll16};
s17 = news; Plane Surface(s17) = {ll17};
s18 = news; Plane Surface(s18) = {ll18};
s19 = news; Plane Surface(s19) = {ll19};
s20 = news; Plane Surface(s20) = {ll20};
s21 = news; Plane Surface(s21) = {ll21};
s22 = news; Plane Surface(s22) = {ll22};
s23 = news; Plane Surface(s23) = {ll23};
s24 = news; Plane Surface(s24) = {ll24};
s25 = news; Plane Surface(s25) = {ll25};
s26 = news; Plane Surface(s26) = {ll26};
s27 = news; Plane Surface(s27) = {ll27};
s28 = news; Plane Surface(s28) = {ll28};
s29 = news; Plane Surface(s29) = {ll29};
s30 = news; Plane Surface(s30) = {ll30};
s31 = news; Plane Surface(s31) = {ll31};
s32 = news; Plane Surface(s32) = {ll32};
faceloop2 = newreg; Surface Loop(faceloop2) = {s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,s11,s12,s13,s14,s15,s16,s17,s18,s19,s20,s21,s22,s23,s24,s25,s26,s27,s28,s29,s30,s31,s32};

Coherence;
Translate {-2, -2, 2} {
  Duplicata { Surface{118, 244, 226, 113, 239, 106, 232}; }
}
Translate {2, 2, -2} {
  Duplicata { Surface{224, 241, 229, 103, 115, 125, 251}; }
}
Translate {-2, 2, -2} {
  Duplicata { Surface{111, 124, 104, 220, 250, 237, 230}; }
}
Translate {2, -2, 2} {
  Duplicata { Surface{222, 243, 247, 235, 109, 117, 121}; }
}

Translate {2, 2, 2} {
  Duplicata { Surface{116, 122, 108, 225, 248, 242, 234}; }
}
Translate {-2, -2, -2} {
  Duplicata { Surface{227, 245, 236, 231, 110, 119, 105}; }
}
Translate {2, -2, -2} {
  Duplicata { Surface{223, 246, 228, 240}; }
}
Translate {-2, 2, 2} {
  Duplicata { Surface{221, 249, 238, 233}; }
}

// create volume
Volume(130) = {faceloop1, faceloop2};


Line(465) = {408, 480};
Line(466) = {200, 493};
Line(467) = {407, 473};
Line Loop(468) = {465, 399, 392, -397, -467, 372, 1, -376};
Plane Surface(469) = {468};
Line Loop(470) = {467, -396, 388, 403, -466, 301, -9, 374};
Plane Surface(471) = {470};
Line Loop(472) = {466, 404, 390, 401, -465, -378, 11, 300};
Plane Surface(473) = {472};
Surface Loop(474) = {469, 473, 471, 394, 387, 402, 398, 299, 371, 375, 101};
Volume(475) = {474};
Line(476) = {408, 646};
Line(477) = {634, 61};
Line Loop(478) = {477, 254, -4, -376, 476, -459, -448, -456};
Plane Surface(479) = {478};
Line(480) = {658, 262};
Line Loop(481) = {480, -323, -13, 256, -477, -455, -452, -464};
Plane Surface(482) = {481};
Line Loop(483) = {480, 324, -12, 378, 476, 460, 450, 463};
Plane Surface(484) = {483};
Surface Loop(485) = {458, 479, 482, 484, 322, 447, 462, 454, 253, 97, 375};
Volume(486) = {485};
Line(487) = {250, 298};
Line(488) = {262, 322};
Line(489) = {211, 310};
Line Loop(490) = {487, 335, -328, 342, -488, -323, 22, -320};
Plane Surface(491) = {490};
Line Loop(492) = {488, 343, -332, 338, -489, 304, -21, -324};
Plane Surface(493) = {492};
Line Loop(494) = {489, 339, -330, 334, -487, -319, -35, 306};
Plane Surface(495) = {494};
Surface Loop(496) = {341, 491, 495, 493, 326, 333, 337, 303, 96, 322, 318};
Volume(497) = {496};

Line(498) = {224, 236};
Line(499) = {211, 175};
Line(500) = {200, 188};
Line Loop(501) = {498, 313, 288, 294, -499, -306, -33, -309};
Plane Surface(502) = {501};
Line Loop(503) = {499, 292, 290, 296, -500, -300, 19, -304};
Plane Surface(504) = {503};
Line Loop(505) = {500, 297, 286, 312, -498, -308, -18, -301};
Plane Surface(506) = {505};
Surface Loop(507) = {303, 291, 504, 502, 506, 295, 284, 311, 307, 100, 299};
Volume(508) = {507};
Line(509) = {589, 224};
Line(510) = {608, 559};
Line(511) = {384, 596};
Line Loop(512) = {440, 433, -438, 509, -308, -28, -365, 511};
Plane Surface(513) = {512};
Line Loop(514) = {509, 309, 34, 426, -510, 444, -429, 437};
Plane Surface(515) = {514};
Line Loop(516) = {510, 427, -27, 364, 511, -442, -431, 446};
Plane Surface(517) = {516};
Surface Loop(518) = {515, 513, 439, 428, 443, 517, 424, 363, 435, 307, 95};
Volume(519) = {518};
Line(520) = {110, 347};
Line(521) = {384, 372};
Line(522) = {407, 359};
Line Loop(523) = {521, 361, -350, 358, -522, -374, 10, -365};
Plane Surface(524) = {523};
Line Loop(525) = {521, -360, 346, -352, -520, -269, 16, 364};
Plane Surface(526) = {525};
Line Loop(527) = {354, -520, 270, -2, -372, 522, 356, -348};
Plane Surface(528) = {527};
Surface Loop(529) = {526, 524, 359, 344, 351, 528, 268, 355, 94, 363, 371};
Volume(530) = {529};
Line(531) = {110, 122};
Line(532) = {138, 150};
Line(533) = {61, 73};
Line Loop(534) = {533, 258, -266, -274, -531, 270, -6, -254};
Plane Surface(535) = {534};
Line Loop(536) = {531, -273, -264, -283, -532, 279, -15, 269};
Plane Surface(537) = {536};
Line Loop(538) = {532, -282, -262, 260, -533, -256, 14, 278};
Plane Surface(539) = {538};
Surface Loop(540) = {537, 535, 539, 280, 261, 272, 257, 276, 268, 98, 253};
Volume(541) = {540};
Line(542) = {138, 543};
Line(543) = {559, 535};
Line(544) = {250, 518};
Line Loop(545) = {544, 413, -406, 418, -543, -426, -36, 319};
Plane Surface(546) = {545};
Line Loop(547) = {543, 419, -410, 421, -542, 279, 25, -427};
Plane Surface(548) = {547};
Line Loop(549) = {542, 422, -408, 415, -544, 320, -24, 278};
Plane Surface(550) = {549};
Surface Loop(551) = {416, 405, 546, 550, 548, 420, 412, 276, 318, 424, 99};
Volume(552) = {551};

Physical Surface(1) = {471, 524, 513, 506, 113, 110, 111, 112};
Physical Surface(2) = {484, 473, 504, 493, 120, 119, 118, 121};
Physical Surface(3) = {479, 469, 528, 535, 102, 105, 104, 103};
Physical Surface(4) = {482, 491, 550, 539, 115, 116, 117, 114};
Physical Surface(5) = {537, 526, 517, 548, 122, 125, 124, 123};
Physical Surface(6) = {502, 495, 546, 515, 107, 106, 109, 108};
Physical Volume(11) = {475, 486, 530, 541, 130, 552, 497, 508, 519};


