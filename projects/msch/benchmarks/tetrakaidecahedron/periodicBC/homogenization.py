#coding-Utf-8-*-

from gmshpy import *
from msch import*

#script to launch PBC problem with a python script

# material law
lawnum = 11 # unique number of law


E = 210E3
nu = 0.3
K = E/3./(1.-2.*nu)	# Bulk mudulus
mu =E/2./(1.+nu)	  # Shear mudulus
rho = 1. # Bulk mass
sy0 = 507.e100
h = 200

# creation of material law

#law1 = FSJ2LinearMaterialLaw(lawnum,E,nu,sy0,h,rho)

law1 = FSElasticMaterialLaw(lawnum,0,K,mu,rho)

# geometry
meshfile="geometry.msh"

# creation of part Domain
nfield = 11 # number of the field (physical number of entity)
dim =3
myfield1 = FSDomain(1000,nfield,lawnum,dim)


# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
system = 0 # Displacement elimination =0 Multiplier elimination = 1 Displacement+ multiplier = 2
control = 0 # load control = 0 arc length control euler = 1

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.setSystemType(system)
mysolver.setControlType(control)
mysolver.stiffnessModification(1)
mysolver.iterativeProcedure(1)
mysolver.setMessageView(1)


mysolver.pathFollowing(0)
mysolver.setPathFollowingControlType(1)

#rve - periodicity
mysolver.setPeriodicity(4.,0,0,"x")
mysolver.setPeriodicity(0,4.,0,"y")
mysolver.setPeriodicity(0,0,4.,"z")

#boundary condition

#linear displacement BC
#mysolver.addLinearDisplacementBC(1000,1,2,3,4,5,6)

#minimal kinematical BC
#mysolver.addMinimalKinematicBC(1000,1,2,3,4)

# periodiodic BC

method = 4	# Periodic mesh = 0, Langrange interpolation = 1, Cubic spline interpolation =2,  FE linear= 3, FE Quad = 4
degree = 4	# Order used for polynomial interpolation 
addvertex = 1 # Polynomial interpolation by mesh vertex = 0, Polynomial interpolation by virtual vertex 
mysolver.addPeriodicBC(1000,1,2,3,4,5,6) #Periodic boundary condition
mysolver.setPeriodicBCOptions(method, degree,addvertex) 



# kinematic macro-variable
mysolver.setOrder(1) # Homogenization order

 # Deformation gradient
mysolver.setDeformationGradient(1.01,0.0,0.0,1)

 # Gradient of deformation gradient
#mysolver.setGradientOfDeformationGradient(1,1,1,0.1)
#mysolver.setGradientOfDeformationGradient(0,1,1,1) 
#mysolver.setGradientOfDeformationGradient(1,0,1,1) 
#mysolver.setGradientOfDeformationGradient(1,1,0,1) 
#mysolver.setGradientOfDeformationGradient(0,0,1,-0.1) 
#mysolver.setGradientOfDeformationGradient(0,0,0,1)


#stress averaging flag and averaging method 0- VOLUME, 1- SURFACE
mysolver.stressAveragingFlag(1) # set stress averaging ON- 0 , OFF-1
mysolver.setStressAveragingMethod(0) # 0 -volume 1- surface
#tangent averaging flag
mysolver.tangentAveragingFlag(0) # set tangent averaging ON -0, OFF -1
mysolver.setTangentAveragingMethod(1,1e-6) # 0- perturbation 1- condensation


# build view
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GLSTRAINEQUIVALENT, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);



# solve
mysolver.solve()


