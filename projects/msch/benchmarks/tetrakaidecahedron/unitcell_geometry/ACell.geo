lsca = 0.2;
p1 = newp; Point(p1) =  {1, 0, 2, lsca};
p2 = newp; Point(p2) =  {0, 1, 2, lsca};
p3 = newp; Point(p3) =  {-1, 0, 2, lsca};
p4 = newp; Point(p4) =  {0, -1, 2, lsca};
p5 = newp; Point(p5) =  {2, 0, 1, lsca};
p6 = newp; Point(p6) =  {0, 2, 1, lsca};
p7 = newp; Point(p7) =  {-2, 0, 1, lsca};
p8 = newp; Point(p8) =  {0, -2, 1, lsca};
p9 = newp; Point(p9) =  {2, 1, 0, lsca};
p10 = newp; Point(p10) =  {1, 2, 0, lsca};
p11 = newp; Point(p11) =  {-1, 2, 0, lsca};
p12 = newp; Point(p12) =  {-2, 1, 0, lsca};
p13 = newp; Point(p13) =  {-2, -1, 0, lsca};
p14 = newp; Point(p14) =  {-1, -2, 0,lsca};
p15 = newp; Point(p15) =  {1, -2, 0, lsca};
p16 = newp; Point(p16) =  {2, -1, 0, lsca};
p17 = newp; Point(p17) =  {2, 0, -1, lsca};
p18 = newp; Point(p18) =  {0, 2, -1, lsca};
p19 = newp; Point(p19) =  {-2, 0, -1, lsca};
p20 = newp; Point(p20) =  {0, -2, -1, lsca};
p21 = newp; Point(p21) =  {1, 0, -2, lsca};
p22 = newp; Point(p22) =  {0, 1, -2, lsca};
p23 = newp; Point(p23) =  {-1, 0, -2, lsca};
p24 = newp; Point(p24) =  {0, -1, -2, lsca};
l1 = newl; Line(l1) = {p1, p2};
l2 = newl; Line(l2) = {p1, p4};
l3 = newl; Line(l3) = {p1, p5};
l4 = newl; Line(l4) = {p2, p3};
l5 = newl; Line(l5) = {p2, p6};
l6 = newl; Line(l6) = {p3, p4};
l7 = newl; Line(l7) = {p3, p7};
l8 = newl; Line(l8) = {p4, p8};
l9 = newl; Line(l9) = {p5, p9};
l10 = newl; Line(l10) = {p5, p16};
l11 = newl; Line(l11) = {p6, p10};
l12 = newl; Line(l12) = {p6, p11};
l13 = newl; Line(l13) = {p7, p12};
l14 = newl; Line(l14) = {p7, p13};
l15 = newl; Line(l15) = {p8, p14};
l16 = newl; Line(l16) = {p8, p15};
l17 = newl; Line(l17) = {p9, p10};
l18 = newl; Line(l18) = {p9, p17};
l19 = newl; Line(l19) = {p10, p18};
l20 = newl; Line(l20) = {p11, p12};
l21 = newl; Line(l21) = {p11, p18};
l22 = newl; Line(l22) = {p12, p19};
l23 = newl; Line(l23) = {p13, p14};
l24 = newl; Line(l24) = {p13, p19};
l25 = newl; Line(l25) = {p14, p20};
l26 = newl; Line(l26) = {p15, p16};
l27 = newl; Line(l27) = {p15, p20};
l28 = newl; Line(l28) = {p16, p17};
l29 = newl; Line(l29) = {p17, p21};
l30 = newl; Line(l30) = {p18, p22};
l31 = newl; Line(l31) = {p19, p23};
l32 = newl; Line(l32) = {p20, p24};
l33 = newl; Line(l33) = {p21, p22};
l34 = newl; Line(l34) = {p21, p24};
l35 = newl; Line(l35) = {p22, p23};
l36 = newl; Line(l36) = {p23, p24};

l37 = newreg; Line Loop(l37) = {l4, l6, -l2, l1};
s38 = newreg; Plane Surface(s38) = {l37};

l39 = newreg; Line Loop(l39) = {l2, l8, l16, l26, -l10, -l3};
s40 = newreg; Plane Surface(s40) = {l39};

l41 = newreg; Line Loop(l41) = {l26, l28, l29, l34, -l32, -l27};
s42 = newreg; Plane Surface(s42) = {l41};

l43 = newreg; Line Loop(l43) = {l33, l35, l36, -l34};
s44 = newreg; Plane Surface(s44) = {l43};

l45 = newreg; Line Loop(l45) = {l35, -l31, -l22, -l20, l21, l30};
s46 = newreg; Plane Surface(s46) = {l45};

l47 = newreg; Line Loop(l47) = {l20, -l13, -l7, -l4, l5, l12};
s48 = newreg; Plane Surface(s48) = {l47};

l49 = newreg; Line Loop(l49) = {l6, l8, l15, -l23, -l14, -l7};
s50 = newreg; Plane Surface(s50) = {l49};

l51 = newreg; Line Loop(l51) = {l15, l25, -l27, -l16};
s52 = newreg; Plane Surface(s52) = {l51};

l53 = newreg; Line Loop(l53) = {l32, -l36, -l31, -l24, l23, l25};
s54 = newreg; Plane Surface(s54) = {l53};

l55 = newreg; Line Loop(l55) = {l30, -l33, -l29, -l18, l17, l19};
s56 = newreg; Plane Surface(s56) = {l55};

l57 = newreg; Line Loop(l57) = {l19, -l21, -l12, l11};
s58 = newreg; Plane Surface(s58) = {l57};

l59 = newreg; Line Loop(l59) = {l28, -l18, -l9, l10};
s60 = newreg; Plane Surface(s60) = {l59};

l61 = newreg; Line Loop(l61) = {l17, -l11, -l5, -l1, l3, l9};
s62 = newreg; Plane Surface(s62) = {l61};

l63 = newreg; Line Loop(l63) = {l22, -l24, -l14, l13};
s64 = newreg; Plane Surface(s64) = {l63};

faceloop1 = newreg; Surface Loop(faceloop1) = {s38, s48, s46, s44, s56, s42, s40, s50, s52, s54, s64, s60, s62, s58};
Coherence;
Translate {2, 2, -2} {
  Duplicata { Surface{38, 40, 48, 50, 62, 64, 60, 58, 52, 46, 54, 56, 42, 44}; }
}

