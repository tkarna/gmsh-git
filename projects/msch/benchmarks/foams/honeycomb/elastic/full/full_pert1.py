#coding-Utf-8-*-
from gmshpy import *
from msch import*

#script to launch PBC problem with a python script

# material law
lawnum1 = 11 # unique number of law
E = 68.9E3 #Young modulus
nu = 0.33 #Poisson ratio
K = E/3./(1.-2.*nu)
mu = E/2./(1+nu)
sy0 = 276.e1000 #Yield stress
h = E/100 # hardening modulus
rho = 1.  #density
# geometry

#law1 = FSElasticMaterialLaw(lawnum1,1,K,mu,rho)

law1 = FSJ2LinearMaterialLaw(lawnum1,E,nu,sy0,h,rho)
#law1 = J2LinearDG3DMaterialLaw(lawnum,rho,E,nu,sy0,h)

#lawnum2 = 12
#law2 = FSJ2LinearMaterialLaw(lawnum2,E,nu,sy0*0.8,h,rho)

meshfile="full_pert1.msh" # name of mesh file


# creation of material law


# creation of part Domain
dim =2
beta1 = 20;
fdg=1
#myfield1 = dG3DDomain(1000,nfield,0,lawnum,fdg,dim)
#myfield1.stabilityParameters(beta1)
myfield1 = FSDomain(1000,11,lawnum1,dim)
#myfield2 = FSDomain(1000,12,lawnum2,dim)

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 50  # number of step (used only if soltype=1)
ftime = 1.   # Final time (used only if soltype=1)
tol=1.e-4 # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
#mysolver.addDomain(myfield2)
mysolver.addMaterialLaw(law1)
#mysolver.addMaterialLaw(law2)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)

mysolver.pathFollowing(1)
mysolver.setPathFollowingControlType(1)
#mysolver.eigenValueSolver(100)
#mysolver.setPerturbationFactor(0.01)
mysolver.iterativeProcedure(1)

#mysolver.createStrainMapping('box.msh')
#mysolver.setStrainMappingFollowing(1)

#mysolver.displacementBC("Face",11,2,0)
mysolver.displacementBC("Node",1,0,0)
#mysolver.displacementBC("Node",6,1,0)
mysolver.displacementBC("Edge",1,1,0)
mysolver.constraintBC("Edge",3,1)
mysolver.forceBC("Edge",3,1,-1e3)

#mysolver.periodicBC("Edge",2,4,0)
#mysolver.periodicBC("Edge",2,4,1)

# build view
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GLSTRAINEQUIVALENT, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

mysolver.archivingForceOnPhysicalGroup("Edge",1,1)
mysolver.archivingNodeDisplacement(4,1,1)



# solve
mysolver.solve()
