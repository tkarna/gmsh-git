function createFile(v, c, x0, x1, y0, y1, filename)

comfid = fopen(filename,'w');
fprintf(comfid,'$Point\n');

numP = 0;
for i=1:length(v(:,1))
    if (v(i,1)>x1 || v(i,1)<x0 || v(i,2)>y1 || v(i,2)<y0)
        numP = numP+1;
    end
end

fprintf(comfid,'%d\n',length(v(:,1))-numP);
for i=1:length(v(:,1))
    if (v(i,1)<=x1&& v(i,1)>=x0 && v(i,2)<=y1 && v(i,2)>=y0)
        fprintf(comfid,'%d %g %g \n',i,v(i,1),v(i,2));
    end
end

fprintf(comfid,'$EndPoint\n');

numElim = 0;
for i=1:length(c)
   sizeVer =length(c{i}); 
   for j=1:sizeVer
        if (v(c{i}(j),1)>x1 || v(c{i}(j),1)<x0 || v(c{i}(j),2)>y1 || v(c{i}(j),2)<y0)
            numElim = numElim+1;
            break;
        end
   end
end


fprintf(comfid,'$Cell\n');
fprintf(comfid,'%d\n',length(c)-numElim);
for i=1:length(c)
   sizeVer =length(c{i}); 
   ok = 0;
   for j=1:sizeVer
        if (v(c{i}(j),1)>x1 || v(c{i}(j),1)<x0 || v(c{i}(j),2)>y1 || v(c{i}(j),2)<y0)
            ok=1;
            break
        end
   end
   
   if (ok==0)
       fprintf(comfid,'%d  %d ',i,sizeVer);
       for j=1:sizeVer
            fprintf(comfid,'%d ',c{i}(j));
       end
       fprintf(comfid,'\n');
   end
  
end
fprintf(comfid,'$EndCell');
fclose(comfid)
