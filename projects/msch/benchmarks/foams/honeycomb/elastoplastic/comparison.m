clear all
close all
clc


u_full = load('./full/result/NodalDisplacement21194comp1.csv');
f_full = load('./full/result/force1comp1.csv');

u_s_0 = load('./second_0_1Cell/result/NodalDisplacement4comp1_part0.csv');
f_s_0 = load('./second_0_1Cell/result/force1comp1_part0.csv');


u_s_1 = load('./second_1_1Cell/result/NodalDisplacement4comp1_part0.csv');
f_s_1 = load('./second_1_1Cell/result/force1comp1_part0.csv');


u_s_2 = load('./second_2_1Cell/result/NodalDisplacement4comp1_part0.csv');
f_s_2 = load('./second_2_1Cell/result/force1comp1_part0.csv');

H = 102;
L = 32.909*2;
l= 1.;
t = l*0.1;
rho = 2*t/l/sqrt(3)

Es = 68.9E3;
nu = 0.33;
sy0 = 276.;
E = 2.3*(t/l)^3*Es 
syf = sy0*(2/3)*(t/l)^2


figure(1)
hold on
set(gca,'FontSize',16)
plot([0; -u_full(:,2)/H*100],[0;f_full(:,2)/L],'-',...
    [0;-u_s_0(1:1:size(u_s_0(:,2)),2)/H*100],[0;f_s_0(1:1:size(u_s_0(:,2)),2)/L],'x--',...
    [0;-u_s_1(1:1:size(u_s_1(:,2)),2)/H*100],[0;f_s_1(1:1:size(u_s_1(:,2)),2)/L],'s-',...
    [0;-u_s_2(1:1:size(u_s_2(:,2)),2)/H*100],[0;f_s_2(1:1:size(u_s_2(:,2)),2)/L],'o--',...
    'LineWidth',1.3,'MarkerSize',10)

legend('Full mesh','Mesh 1','Mesh 2','Mesh 3','Location','Best')
xlabel('-strain (%)')
ylabel('- Stress (MPa)')
legend boxoff
print -f1 -dpdf comparison.pdf
print -f1 -depsc comparisonc.eps
print -f1 -deps comparison.eps
