#!/bin/sh

# script example for nic3.ulg.ac.be
# the #dollar are pseudo command for qsub so they have to be edited !!


#$ -N s1_p1
#$ -m beas
#$ -M vandung.nguyen@ulg.ac.be
##$ -l highmem=true
#$ -l h_vmem=2500M
#$ -l h_rt=480:00:00
#$ -pe openmpi 50
#$ -cwd
#$ -j yes

source /etc/profile.d/modules.sh 
source /etc/profile.d/segi.sh 

module add openmpi/gcc/64/1.3.0
subdir=$PWD
echo "subdir=" $subdir

workdir=/tmpscratch/${USER}_$JOB_ID
echo "workdir=" $workdir

echo "cat \$TMPDIR/machines | uniq"
cat $TMPDIR/machines | uniq

for ii in `cat $TMPDIR/machines | uniq`
    do
		    echo $ii
		    ssh $ii mkdir $workdir 
		    ssh $ii cp $subdir/*.py $workdir/ 
		    ssh $ii cp $subdir/*.msh $workdir/
	  done

cd $workdir # needed to work in the tmpscrach dir otherwise work on home !!
#ls -artl
export PETSC_DIR=$HOME/petsc-3.3-p5
export PETSC_ARCH=linux-gnu-c-opt
export SLEPC_DIR=$HOME/slepc-3.3-p3
export NLSMPIINC=/cvos/shared/apps/openmpi/gcc/64/1.6.2/include
export LD_PRELOAD=$LD_PRELOAD:/cvos/shared/apps/openmpi/gcc/64/1.6.2/lib/libmpi.so
export PATH=/cvos/shared/apps/openmpi/gcc/64/1.6.2/bin:$HOME/gmsh/projects/msch/build/dG3D/NonLinearSolver/gmsh:$PETSC_DIR/$PETSC_ARCH/lib:$PETSC_DIR/$PETSC_ARCH/include:$PETSC_DIR/$PETSC_ARCH/bin:$PATH
export PYTHONPATH=$HOME/gmsh/projects/msch/build:$HOME/gmsh/projects/msch/build/dG3D:$HOME/gmsh/projects/msch/build/dG3D/NonLinearSolver/gmsh/wrappers:$PYTHONPATH
                                                                                                                                
mpiexec -n 50 python  $workdir/multiscale.py >& $workdir/output.txt

echo -e "\n"

sleep 5

for ii in `tac $TMPDIR/machines | uniq`
  do
     # ssh $ii mv $workdir/output.txt $subdir/output_$ii.txt
  done
echo -e "\n"
