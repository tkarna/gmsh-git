#coding-Utf-8-*-
from gmshpy import *
from msch import*

import math

#script to launch PBC problem with a python script
#---------------------------------------------------------------------------------------------------------------#
#DEFINE MICRO PROBLEM

# micro-material law
E = 68.9E3 #Young modulus
nu = 0.33 #Poisson ratio
K = E/3./(1.-2.*nu)
mu = E/2./(1+nu)
sy0 = 276. #Yield stress
h = E/100 # hardening modulus
rho = 1.  #density
# micro-geometry

# creation of material law
law1 = FSJ2LinearMaterialLaw(11,E,nu,sy0,h,rho)

# creation of  micro part Domain
myfield1 = FSDomain(1000,11,11,2)

# DEFINE MACROPROBLEM
matnum1 = 1;
macromat1 = hoDGMultiscaleMaterialLaw(matnum1, 1000)
#macromat1 = FSMultiscaleMaterialLaw(matnum1, 1000)

macromat1.loadAllRVEMeshes("rve")
macromat1.addDomain(myfield1)
macromat1.addMaterialLaw(law1);

macromat1.setPeriodicity(1.7321,0,0,"x")
macromat1.setPeriodicity(0,6.,0,"y")
macromat1.setPeriodicity(0,0,1.,"z")

macromat1.addPeriodicBC(1000,1,2,3,4)
macromat1.setPeriodicBCOptions(1, 11,1)
macromat1.setNumStep(1)
macromat1.setTolerance(1e-6)
macromat1.setSystemType(1)
macromat1.Solver(2)
macromat1.setViewAllMicroProblems(1,0)
macromat1.setOrder(2)
macromat1.Scheme(1)

macromat1.setStressAveragingMethod(1);
macromat1.setTangentAveragingMethod(1,1e-6);

macromat1.stressAveragingFlag(1)
macromat1.tangentAveragingFlag(1)

macromat1.stiffnessModification(1)
macromat1.iterativeProcedure(1)

macromat1.pathFollowing(1)
macromat1.setPathFollowingControlType(1)

#------------------------------------------
macromeshfile="macro.msh" # name of mesh file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 50  # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-5  # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)


# creation of  macro part Domain

dim =2
fulldg = 1
beta1 = 1000

#macrodomain1 = FSDomain(1000,11,matnum1,dim)
macrodomain1 =hoDGDomain(1000,11,0,matnum1,fulldg,dim)
macrodomain1.stabilityParameters(beta1)
macrodomain1.distributeOnRootRank(1)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(macromeshfile)
mysolver.addDomain(macrodomain1)
mysolver.addMaterialLaw(macromat1)

mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)

mysolver.pathFollowing(1)
mysolver.setPathFollowingControlType(1)


# boundary condition
mysolver.displacementBC("Face",11,2,0.0)

mysolver.displacementBC("Node",4,0,0)
mysolver.displacementBC("Edge",1,1,0)

mysolver.constraintBC("Edge",3,1)
mysolver.forceBC("Edge",3,1,-0.25e2)

# archivage
mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GLSTRAINEQUIVALENT, 1, 1);
mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);
mysolver.internalPointBuildView("High-order strain norm",IPField.HIGHSTRAIN_NORM, 1, 1);
mysolver.internalPointBuildView("High-order stress norm",IPField.HIGHSTRESS_NORM, 1, 1);
mysolver.internalPointBuildView("Green -Lagrange strain norm",IPField.GL_NORM, 1, 1);
mysolver.internalPointBuildView("PK2 stress norm",IPField.STRESS_NORM, 1, 1);

mysolver.archivingForceOnPhysicalGroup("Edge",1,1)
mysolver.archivingNodeDisplacement(4,1,1)

# solve
mysolver.solve()

