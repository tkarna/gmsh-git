#-*-coding:Utf-8-*-
# script to copy the scratch of a simulation launched on nic3
# this script has to be launch in the folder where you launch the simulation

from os import system,getcwd,listdir

# get the .po file containing the nodes
lfiles = listdir(getcwd())

pofile = " "
for fname in lfiles :
    if ".po" in fname:
        pofile = fname
	break
if pofile==" ":
    print "The file .po is missing"
else:   
    system("rm -rf tmp") 
    system("mkdir tmp")
    # get the number of the simulation
    cpo = pofile
    lpo = cpo.split(".po")
    simnum = int(lpo[1])
    rfile = open(pofile,'r')
    lnodes = []
    rfile.readline() # skip first line which do not contain a node
    while 1:
        tmpo = rfile.readline()[:-1] # :-1 to remove end of line
        if tmpo =='':
            break
        else:
            if tmpo not in lnodes:
                lnodes.append(tmpo)
	        system("scp %s:/tmpscratch/vdnguyen_%d/* ./tmp"%(tmpo,simnum))

    print "copy scratch OK"
