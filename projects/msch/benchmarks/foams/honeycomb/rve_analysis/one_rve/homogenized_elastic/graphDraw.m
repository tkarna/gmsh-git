clear all
close all
clc


segmax = 5;
degmax = 13;

dlag=[];
lag = [];
dspl =[];
spl=[];

comp = 2
for i =1:segmax
  txt = ['E_' num2str(i) '_GP_0_tangent.csv'];
  F = importdata(txt);
  spl = [spl F.data(1,comp)];
  
  txt = ['E_' num2str(i) '_GP_0_virtualSize.csv'];
  G = load(txt);
  dspl = [dspl G];
end

for i =1:degmax
  txt = ['E_0_GP_' num2str(i) '_tangent.csv'];
  F = importdata(txt);
  lag = [lag F.data(1,comp)];
  
  txt = ['E_0_GP_' num2str(i) '_virtualSize.csv'];
  G = load(txt);
  dlag = [dlag G];
end

figure(1)
hold on
plot(dlag,lag,'bs-',dspl,spl,'ro--','MarkerSize',8,'LineWidth',1.3)


dlag=[];
lag = [];
dspl =[];
spl=[];

comp = 42
for i =1:segmax
  txt = ['E_' num2str(i) '_GP_0_tangent.csv'];
  F = importdata(txt);
  spl = [spl F.data(1,comp)];
  
  txt = ['E_' num2str(i) '_GP_0_virtualSize.csv'];
  G = load(txt);
  dspl = [dspl G];
end

for i =1:degmax
  txt = ['E_0_GP_' num2str(i) '_tangent.csv'];
  F = importdata(txt);
  lag = [lag F.data(1,comp)];
  
  txt = ['E_0_GP_' num2str(i) '_virtualSize.csv'];
  G = load(txt);
  dlag = [dlag G];
end

figure(1)
plot(dlag,lag,'gd-',dspl,spl,'k*--','MarkerSize',10,'LineWidth',1.3)

set(gca,'FontSize',16)
xlabel('Number of Dofs')
ylabel('Tangent (MPa)')
legend('L_{0000}, Lagrange','L_{0000}, Cubic spline',...
    'L_{1111}, Lagrange','L_{1111}, Cubic spline','Location','Best')
legend boxoff

% dlag=[];
% lag = [];
% dspl =[];
% spl=[];
% 
% comp = 12
% for i =1:segmax
%   txt = ['E_' num2str(i) '_GP_0_tangent.csv'];
%   F = importdata(txt);
%   spl = [spl F.data(1,comp)];
%   
%   txt = ['E_' num2str(i) '_GP_0_virtualSize.csv'];
%   G = load(txt);
%   dspl = [dspl G];
% end
% 
% for i =1:degmax
%   txt = ['E_0_GP_' num2str(i) '_tangent.csv'];
%   F = importdata(txt);
%   lag = [lag F.data(1,comp)];
%   
%   txt = ['E_0_GP_' num2str(i) '_virtualSize.csv'];
%   G = load(txt);
%   dlag = [dlag G];
% end
% 
% figure(1)
% plot(dlag,lag,'gx-',dspl,spl,'rv--','MarkerSize',10,'LineWidth',1.3)
% 
% 
% set(gca,'FontSize',16)
% xlabel('Number of Dofs')
% ylabel('Tangent (MPa)')
% legend('L_{0000}, Lagrange','L_{0000}, Cubic spline',...
%     'L_{1111}, Lagrange','L_{1111}, Cubic spline',...
%     'L_{1010}, Lagrange','L_{1010}, Cubic spline','Location','West')
% legend boxoff

dlag=[];
lag = [];
dspl =[];
spl=[];

comp = 254
for i =1:segmax
  txt = ['E_' num2str(i) '_GP_0_second_tangent.csv'];
  F = importdata(txt);
  spl = [spl F.data(1,comp)];
  
  txt = ['E_' num2str(i) '_GP_0_virtualSize.csv'];
  G = load(txt);
  dspl = [dspl G];
end

for i =1:degmax
  txt = ['E_0_GP_' num2str(i) '_second_tangent.csv'];
  F = importdata(txt);
  lag = [lag F.data(1,comp)];
  
  txt = ['E_0_GP_' num2str(i) '_virtualSize.csv'];
  G = load(txt);
  dlag = [dlag G];
end

figure(2)
hold on
plot(dlag,lag,'s-',dspl,spl,'o--','LineWidth',1.3,'MarkerSize',10)
% xlabel('Number of Dofs')
% ylabel('J_{001001} (MPa/mm)')
% legend('Lagrange','Cubic spline')

dlag=[];
lag = [];
dspl =[];
spl=[];

comp = 282
for i =1:segmax
  txt = ['E_' num2str(i) '_GP_0_second_tangent.csv'];
  F = importdata(txt);
  spl = [spl F.data(1,comp)];
  
  txt = ['E_' num2str(i) '_GP_0_virtualSize.csv'];
  G = load(txt);
  dspl = [dspl G];
end

for i =1:degmax
  txt = ['E_0_GP_' num2str(i) '_second_tangent.csv'];
  F = importdata(txt);
  lag = [lag F.data(1,comp)];
  
  txt = ['E_0_GP_' num2str(i) '_virtualSize.csv'];
  G = load(txt);
  dlag = [dlag G];
end

figure(2)
plot(dlag,lag,'*-',dspl,spl,'x--','LineWidth',1.3,'MarkerSize',10)
% xlabel('Number of Dofs')
% ylabel('J_{101101} (MPa/mm)')
% legend('Lagrange','Cubic spline')


dlag=[];
lag = [];
dspl =[];
spl=[];

comp = 2
for i =1:segmax
  txt = ['E_' num2str(i) '_GP_0_second_tangent.csv'];
  F = importdata(txt);
  spl = [spl F.data(1,comp)];
  
  txt = ['E_' num2str(i) '_GP_0_virtualSize.csv'];
  G = load(txt);
  dspl = [dspl G];
end

for i =1:degmax
  txt = ['E_0_GP_' num2str(i) '_second_tangent.csv'];
  F = importdata(txt);
  lag = [lag F.data(1,comp)];
  
  txt = ['E_0_GP_' num2str(i) '_virtualSize.csv'];
  G = load(txt);
  dlag = [dlag G];
end

figure(2)
plot(dlag,lag,'d-',dspl,spl,'v--','LineWidth',1.3,'MarkerSize',10)
% xlabel('Number of Dofs')
% ylabel('J_{000000} (MPa/mm)')
% legend('Lagrange','Cubic spline')

dlag=[];
lag = [];
dspl =[];
spl=[];

comp = 366
for i =1:segmax
  txt = ['E_' num2str(i) '_GP_0_second_tangent.csv'];
  F = importdata(txt);
  spl = [spl F.data(1,comp)];
  
  txt = ['E_' num2str(i) '_GP_0_virtualSize.csv'];
  G = load(txt);
  dspl = [dspl G];
end

for i =1:degmax
  txt = ['E_0_GP_' num2str(i) '_second_tangent.csv'];
  F = importdata(txt);
  lag = [lag F.data(1,comp)];
  
  txt = ['E_0_GP_' num2str(i) '_virtualSize.csv'];
  G = load(txt);
  dlag = [dlag G];
end

figure(2)
set(gca,'FontSize',16)
hold on
plot(dlag,lag,'<-',dspl,spl,'^--','LineWidth',1.3,'MarkerSize',10)
xlabel('Number of Dofs')
ylabel('Second-order tangent (MPa.mm^2)')

legend('J_{001001}, Lagrange','J_{001001}, Cubic spline','J_{101101}, Lagrange','J_{101101}, Cubic spline',...
    'J_{000000}, Lagrange','J_{000000}, Cubic spline','J_{111111}, Lagrange','J_{111111}, Cubic spline')

legend boxoff

print -f1 -deps L_interp.eps
system('epstopdf L_interp.eps')
print -f2 -deps J_interp.eps
system('epstopdf J_interp.eps')





