clear all
close all
clc
%----------------------------------------

oldir = cd('lagarange_9_elastic');

path = importdata('./pathResult.csv');
tangent = importdata('./E_0_GP_9_tangent.csv');

siz = size(tangent.data);
row = siz(1);
col = siz(2);
theta = linspace(0,2*pi,200);
N = length(theta);

detQ = theta;
mindetQ = zeros(row,1);
thethaminQ = zeros(row,1);

for ii=1:row
   L = vec2ten(tangent.data(ii,2:col),tangent.textdata(2:col));
   for jj=1:N
    Q = zeros(3,3);
    n = zeros(3,1);
    n(1) = cos(theta(jj));
    n(2) = sin(theta(jj));
    n(3) = 0;
    for j=1:3
        for k=1:3
            for i=1:3
                for l=1:3
                    Q(j,k) =Q(j,k)+ n(i)*L(i,j,k,l)*n(l); 
                end
            end
        end
    end
    QQ = Q(1:2,1:2);
    detQ(ii,jj) = det(QQ);
   end

   [mindetQ(ii) ll] = min(detQ(ii,:));
   
end

hoz = [0; path.data(:,2)];

figure(2)
hold on
hoz1 = hoz;
ver1 = hoz;

for i=1:row
    k = 5*i;
    if (k==5*1)
        plot(theta(:),detQ(k,:),'--','LineWidth',1.5);
        plot(theta(:),detQ(k,:),'-','LineWidth',1.5);
    elseif (k<=row)
        plot(theta(:),detQ(k,:),'--','LineWidth',1.5);
    end
end


figure(3)
hold on
plot(hoz,mindetQ,'--','LineWidth',1.5)




cd(oldir)
oldir =cd('lagarange_9')




path = importdata('./pathResult.csv');
tangent = importdata('./E_0_GP_9_tangent.csv');

siz = size(tangent.data);
row = siz(1);
col = siz(2);
theta = linspace(0,2*pi,200);
N = length(theta);

detQ = theta;
mindetQ = zeros(row,1);
thethaminQ = zeros(row,1);

for ii=1:row
   L = vec2ten(tangent.data(ii,2:col),tangent.textdata(2:col));
   for jj=1:N
    Q = zeros(3,3);
    n = zeros(3,1);
    n(1) = cos(theta(jj));
    n(2) = sin(theta(jj));
    n(3) = 0;
    for j=1:3
        for k=1:3
            for i=1:3
                for l=1:3
                    Q(j,k) =Q(j,k)+ n(i)*L(i,j,k,l)*n(l); 
                end
            end
        end
    end
    QQ = Q(1:2,1:2);
    detQ(ii,jj) = det(QQ);
   end

   [mindetQ(ii) ll] = min(detQ(ii,:));
   
end

hoz = [0; path.data(:,2)];

figure(2)
for i=1:row
    k = 5*i;
    if (k<=row)
        plot(theta(:),detQ(k,:),'-','LineWidth',1.5);
    end
end
set(gca,'FontSize',16)
% ylim([-6e4 15e4])
legend('Elastic','Elasto-plastic','Location','North')
legend boxoff
xlabel('\theta')
ylabel('det(Q)')



figure(3)
hold on
plot(hoz,mindetQ,'-','LineWidth',1.5)
plot(hoz,zeros(1,size(hoz)),'k-','LineWidth',0.5)
set(gca,'FontSize',16)
xlabel('\mu ')
ylabel('min det(Q)')
legend('Elastic','Elasto-plastic','Location','North')
legend boxoff

cd(oldir)

print -f2 -deps detQ
system('epstopdf detQ.eps')

print -f3 -deps mindetQ
system('epstopdf mindetQ.eps')

