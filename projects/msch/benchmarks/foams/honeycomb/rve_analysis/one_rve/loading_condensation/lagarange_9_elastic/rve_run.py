#coding-Utf-8-*-

from gmshpy import *
from msch import*

import math

def rve_run(method, degree, n, m, addvertex):
		# material law
		lawnum = 11 # unique number of law
		E = 68.9E3 #Young modulus
		nu = 0.33 #Poisson ratio
		K = E/3./(1.-2.*nu)
		mu = E/2./(1+nu)
		sy0 = 276.E100 #Yield stress
		h = E/100 # hardening modulus
		rho = 1.  #density

		# creation of material law
		law1 = FSJ2LinearMaterialLaw(lawnum,E,nu,sy0,h,rho)

		#law1 = FSJ2LinearMaterialLawSecondOrder(lawnum,E,nu,sy0,h,rho,1e-6,1,1e-6)
		#law1.setType(3)
		#law1.setParameter("YOUNG_MODULUS",E);
		#law1.setParameter("LENGTH",le);
		#law1.setParameter("A1",mu*le*le/2.)
		#law1.setParameter("A2",mu*le*le/2.)
		#law1.setParameter("A3",mu*le*le/2.)
		#law1.setParameter("A4",mu*le*le/2.)
		#law1.setParameter("A5",mu*le*le/2.)

		#type = 0;
		#law1 = FSElasticMaterialLaw(lawnum,type,K,mu,rho)

		# geometry
		meshfile="rve1.msh" # name of mesh file

		# creation of part Domain
		nfield = 11 # number of the field (physical number of entity)
		dim =2

		myfield1 = FSDomain(1000,nfield,lawnum,dim)
		#myfield1 = FSDomainSecondOrder(1000,nfield,lawnum,dim)


		# solver
		sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
		soltype =1 # StaticLinear=0 (default) StaticNonLinear=1
		nstep = 50  # number of step (used only if soltype=1)
		ftime =1.   # Final time (used only if soltype=1)
		tol=1.e-5  # relative tolerance for NR scheme (used only if soltype=1)
		nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)
		system = 1 # Displacement elimination =0 Multiplier elimination = 1 Displacement+ multiplier = 2
		control = 0 # load control = 0 arc length control euler = 1

		# creation of Solver
		mysolver = nonLinearMechSolver(1000)

		mysolver.loadModel(meshfile)

		mysolver.addDomain(myfield1)
		mysolver.addMaterialLaw(law1)
		mysolver.Scheme(soltype)
		mysolver.Solver(sol)
		mysolver.snlData(nstep,ftime,tol)
		mysolver.setSystemType(system)
		mysolver.setControlType(control)
		mysolver.stiffnessModification(1)
		mysolver.iterativeProcedure(1)
		mysolver.setMessageView(1)

		mysolver.setMicroProblemIndentification(n,m)

		mysolver.pathFollowing(1)
		mysolver.setPathFollowingControlType(1)

		#rve - periodicity
		mysolver.setPeriodicity(2.*math.sqrt(3.),0,0,"x")
		mysolver.setPeriodicity(0,6.,0,"y")
		mysolver.setPeriodicity(0,0,1.,"z")
		#boundary condition

		#linear displacement BC
		#mysolver.addLinearDisplacementBC(1000,1,2,3,4)

		#minimal kinematical BC
		#mysolver.addMinimalKinematicBC(1000,1,2,3,4)

		# periodiodic BC

		mysolver.addPeriodicBC(1000,1,2,3,4) #Periodic boundary condition
		mysolver.setPeriodicBCOptions(method, degree,addvertex) 

		# kinematic macro-variable
		mysolver.setOrder(2) # Homogenization order

		 # Deformation gradient
		mysolver.setDeformationGradient(1.0,0.0,0.0,0.98)

		 # Gradient of deformation gradient
		#mysolver.setGradientOfDeformationGradient(0,1,0,0.1)
		#mysolver.setGradientOfDeformationGradient(0,0,1,0.1) 
		#mysolver.setGradientOfDeformationGradient(1,0,1,0.1) 
		#mysolver.setGradientOfDeformationGradient(1,1,1,0.1) 
		#mysolver.setGradientOfDeformationGradient(0,0,1,-0.1) 
		#mysolver.setGradientOfDeformationGradient(0,0,0,0.1)

		# eigensolver
		mysolver.eigenValueSolver(300) # set eigensolver with number of eigenvalue
		mysolver.setEigenSolverFollowing(0)
		mysolver.perturbateBucklingModeByMinimalEigenValueMode(0)
		mysolver.setPerturbationFactor(0.001);
		mysolver.setInstabilityCriterion(0.);
		mysolver.setModeView(0)
		mysolver.setModeView(1)
		mysolver.setModeView(2)
		mysolver.setModeView(3)

		#stress averaging flag and averaging method 0- VOLUME, 1- SURFACE
		mysolver.stressAveragingFlag(1) # set stress averaging ON- 0 , OFF-1
		mysolver.setStressAveragingMethod(1) # 0 -volume 1- surface
		#tangent averaging flag
		mysolver.tangentAveragingFlag(1) # set tangent averaging ON -0, OFF -1
		mysolver.setTangentAveragingMethod(1,1e-6) # 0- perturbation 1- condensation

		mysolver.setExtractPerturbationToFileFlag(0)		

		# build view
		mysolver.internalPointBuildView("Green-Lagrange_xx",IPField.STRAIN_XX, 1, 1);
		mysolver.internalPointBuildView("Green-Lagrange_yy",IPField.STRAIN_YY, 1, 1);
		mysolver.internalPointBuildView("Green-Lagrange_zz",IPField.STRAIN_ZZ, 1, 1);
		mysolver.internalPointBuildView("Green-Lagrange_xy",IPField.STRAIN_XY, 1, 1);
		mysolver.internalPointBuildView("Green-Lagrange_yz",IPField.STRAIN_YZ, 1, 1);
		mysolver.internalPointBuildView("Green-Lagrange_xz",IPField.STRAIN_XZ, 1, 1);
		mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1);
		mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1);
		mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1);
		mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1);
		mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1);
		mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1);
		mysolver.internalPointBuildView("sig_VM",IPField.SVM, 1, 1);
		mysolver.internalPointBuildView("Green-Lagrange equivalent strain",IPField.GLSTRAINEQUIVALENT, 1, 1);
		mysolver.internalPointBuildView("Equivalent plastic strain",IPField.PLASTICSTRAIN, 1, 1);

		mysolver.internalPointBuildView("F_xx",IPField.F_XX, 1, 1);
		mysolver.internalPointBuildView("F_xy",IPField.F_XY, 1, 1);
		mysolver.internalPointBuildView("F_yx",IPField.F_YX, 1, 1);
		mysolver.internalPointBuildView("F_yy",IPField.F_YY, 1, 1);

		mysolver.internalPointBuildView("G_xxx",IPField.G_XXX, 1, 1);
		mysolver.internalPointBuildView("G_xyy",IPField.G_XYY, 1, 1);
		mysolver.internalPointBuildView("G_xxy",IPField.G_XXY, 1, 1);
		mysolver.internalPointBuildView("G_yxx",IPField.G_YXX, 1, 1);
		mysolver.internalPointBuildView("G_yyy",IPField.G_YYY, 1, 1);
		mysolver.internalPointBuildView("G_yxy",IPField.G_YXY, 1, 1);


		mysolver.internalPointBuildView("High-order strain norm",IPField.HIGHSTRAIN_NORM, 1, 1);
		mysolver.internalPointBuildView("High-order stress norm",IPField.HIGHSTRESS_NORM, 1, 1);
		mysolver.internalPointBuildView("Green -Lagrange strain norm",IPField.GL_NORM, 1, 1);


		# solve
		mysolver.solve()
		#mysolver.initMicroSolver()


