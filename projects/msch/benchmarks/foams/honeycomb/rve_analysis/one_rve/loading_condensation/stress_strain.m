clear all
close all

oldir = cd('lagarange_9_elastic');

strain = importdata('./E_0_GP_9_strain.csv');
stress = importdata('./E_0_GP_9_stress.csv');
straintable = strain.data;
stresstable = stress.data; 
A=size(stresstable);
row = A(1)
col = A(2) 
stresstime = straintable(:,6);
normstress = stresstime;
for i=1:row
   normstress(i)= sqrt(sum(stresstable(i,2:10).^2+stresstable(i,2:10).^2));
end

figure(1);
hold on
plot(-stresstime,normstress,'--','LineWidth',1.5);

figure(2)
hold on
tangent = importdata('./E_0_GP_9_tangent.csv');
tangenttable = tangent.data;
plot(-stresstime,tangenttable(:,42),'--','LineWidth',1.5);
plot(-stresstime,tangenttable(:,2),':','LineWidth',1.5);

figure(3)
hold on
tangent = importdata('./E_0_GP_9_second_tangent.csv');
tangenttable = tangent.data;
plot(-stresstime,tangenttable(:,282),'--','LineWidth',1.5);
plot(-stresstime,tangenttable(:,254),':','LineWidth',1.5);

cd(oldir)
oldir = cd('lagarange_9');

strain = importdata('./E_0_GP_9_strain.csv');
stress = importdata('./E_0_GP_9_stress.csv');
straintable = strain.data;
stresstable = stress.data; 
A=size(stresstable);
row = A(1)
col = A(2) 
stresstime = straintable(:,6);
normstress = stresstime;
for i=1:row
   normstress(i)= sqrt(sum(stresstable(i,2:10).^2+stresstable(i,2:10).^2));
end

figure(1);
plot(-stresstime,normstress,'-','LineWidth',1.5);
set(gca,'FontSize',16)
xlabel('Strain norm')
ylabel('Stress norm (MPa)')
legend('Elastic','Elasto-plastic','Location','Best')
legend boxoff

figure(2)
hold on
tangent = importdata('./E_0_GP_9_tangent.csv');
tangenttable = tangent.data;
plot(-stresstime,tangenttable(:,42),'-','LineWidth',1.5);
plot(-stresstime,tangenttable(:,2),'-.','LineWidth',1.5);
set(gca,'FontSize',16)
xlabel('Strain norm')
ylabel('Tangent (MPa)')
legend('L_{1111}, elastic','L_{0000}, elastic','L_{1111}, elasto-plastic','L_{0000}, elasto-plastic',1)
legend boxoff

figure(3)
tangent = importdata('./E_0_GP_9_second_tangent.csv');
tangenttable = tangent.data;
plot(-stresstime,tangenttable(:,282),'-','LineWidth',1.5);
plot(-stresstime,tangenttable(:,254),'-.','LineWidth',1.5);
set(gca,'FontSize',16)
xlabel('Strain norm')
ylabel('Second-order tangent (MPa.mm^2)')
legend('J_{101101}, elastic','J_{010010}, elastic','J_{101101}, elasto-plastic','J_{010010}, elasto-plastic',1)
legend boxoff

cd(oldir)
print -f1 -deps stress
system('epstopdf stress.eps')

print -f2 -deps firsttangent
system('epstopdf firsttangent.eps')

print -f3 -deps secondtangent
system('epstopdf secondtangent.eps')

% 
% tangent = importdata('./E_0_GP_11_tangent.csv');
% figure(2)
% tangenttable = tangent.data; %dlmread('./homogenizedTangent.csv');
% plot(stresstime,tangenttable(:,2:82),'LineWidth',1.5);
% legend(tangent.textdata(2:82))
% set(gca,'FontSize',16)
% xlabel('\mu')
% ylabel('L (Pa)')
% grid on
% 
% %print -f2 -r600 -depsc tangentImage
% %system('epstopdf tangentImage.eps')
% 
% 
% 
% tangent = importdata('./E_0_GP_11_second_tangent.csv');
% figure(3)
% tangenttable = tangent.data; %dlmread('./homogenizedTangent.csv');
% plot(stresstime,tangenttable(:,2:730),'LineWidth',1.5);
% legend(tangent.textdata(2:730))
% set(gca,'FontSize',16)
% xlabel('\mu')
% ylabel('L (Pa)')
% grid on

cd(oldir)
