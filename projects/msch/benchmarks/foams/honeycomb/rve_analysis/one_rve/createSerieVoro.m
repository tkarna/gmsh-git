clear all
close all
clc



for ll=1:1
    ll
    l = 1.;
    t = 0.05*l;
    lsca = 4*t;

    Mcut = 9;
    Ncut = 9;

    xli = [-(Mcut-1)/2*l*sqrt(3) (Mcut-1)/2*l*sqrt(3)];
    yli = [-(Ncut-1)/2*l*1.5,(Ncut-1)/2*l*1.5];

    M = 11;
    N = 11;

    x = zeros(1,N*M);
    y = zeros(1,N*M);

    x0 = linspace(-(M-1)/2*l*sqrt(3),(M-1)/2*l*sqrt(3),M)+l*sqrt(3)/4;
    y0 = linspace(-(N-1)/2*l*1.5,(N-1)/2*l*1.5,N);

    for j=1:N
        for i=1:M
           if (rem(j,2)==1)
            x(i+M*(j-1))= x0(i);
           else
            x(i+M*(j-1))= x0(i)+0.5*l*sqrt(3);
           end
           y(i+M*(j-1))= y0(j);
        end
    end


    pert = 0.01;
    for i=1:N*M
       c =1;% rand();
       phi=rand();
       x(i) = x(i)+c*l*pert*cos(phi*2*pi);
       y(i) = y(i)+c*l*pert*sin(phi*2*pi);
    end

%     figure(1)
%     xlim(xli)
%     ylim(yli)
%     hold on
%     plot(x,y,'+')
%     voronoi(x,y);
%     axis equal
    [v,c] = voronoin([x(:) y(:)]);
    
    filename = ['voroData' num2str(ll) '.txt'];
    createFile(v,c,min(x0),max(x0),min(y0),max(y0),filename);
end
