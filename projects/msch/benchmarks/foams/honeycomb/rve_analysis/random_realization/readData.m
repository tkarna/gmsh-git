clear all
close all
clc

system('rm -rf 1Cell_pert1');
system('mkdir 1Cell_pert1');
system('cp createFile.m  createSerieVoro.m 1Cell_pert1');
system('cp *.py 1Cell_pert1');
oldir = cd('1Cell_pert1');

val=[];

size = 200;
createSerieVoro(0.01,size);

for i=1:size
    %create geo
    vororve = ['voro voroData'  num2str(i) '.txt' ' 2 3 rve'  num2str(i) '.geo']
    system( vororve);
    % meshing
    system(['gmsh rve' num2str(i) '.geo -2 -order 2 -algo delquad']);
    % run
    com = ['python rve.py rve' num2str(i) '.msh ' num2str(i)]
    system(com) 
    
   txt = ['E_' num2str(i) '_GP_0_tangent.csv']
   tangent = importdata(txt);
   data = tangent.data;
   val = [val; data(1,42)];
end

xmin = min(val);
xmax = max(val);

dx = 0.1*(xmax-xmin);

N = 20;
x = linspace(xmin-dx,xmax+dx,N);
y = x;
for i=1:N
    y(i)=0;
    for j=1:size
        if val(j)<=x(i)
            y(i) = y(i)+1;
        end
    end
end

figure(1)
hold on
y = y/size;
plot(x,y,'o-',mean(val)*ones(1,size),linspace(1,size,size),'o-');

cd(oldir)


system('rm -rf 1Cell_pert5');
system('mkdir 1Cell_pert5');
system('cp createFile.m  createSerieVoro.m 1Cell_pert5');
system('cp *.py 1Cell_pert5');
oldir = cd('1Cell_pert5');

val=[];

size = 100;
createSerieVoro(0.05,size);

for i=1:size
    %create geo
    vororve = ['voro voroData'  num2str(i) '.txt' ' 2 3 rve'  num2str(i) '.geo']
    system( vororve);
    % meshing
    system(['gmsh rve' num2str(i) '.geo -2 -order 2 -algo delquad']);
    % run
    com = ['python rve.py rve' num2str(i) '.msh ' num2str(i)]
    system(com) 
    
   txt = ['E_' num2str(i) '_GP_0_tangent.csv']
   tangent = importdata(txt);
   data = tangent.data;
   val = [val; data(1,42)];
end

xmin = min(val);
xmax = max(val);

dx = 0.1*(xmax-xmin);

N = 20;
x = linspace(xmin-dx,xmax+dx,N);
y = x;
for i=1:N
    y(i)=0;
    for j=1:size
        if val(j)<=x(i)
            y(i) = y(i)+1;
        end
    end
end

figure(1)
hold on
y = y/size;
plot(x,y,'o-',mean(val)*ones(1,size),linspace(1,size,size),'o-');


cd(oldir)


print -f1 -depsc f1.eps
system('epstopdf f1.eps')