clear all
close all
clc

system('cp *.py rve_pert1_1Cell');
oldir = cd('rve_pert1_1Cell');

val=[];

size = 100;

for i=1:size
%     com = ['python rve.py rve' num2str(i) '.msh ' num2str(i)]
%     system(com) 
    
   txt = ['E_' num2str(i) '_GP_0_tangent.csv']
   tangent = importdata(txt);
   data = tangent.data;
   val = [val; data(1,42)];
end

xmin = min(val);
xmax = max(val);

dx = 0.3*(xmax-xmin);

N = 10;
x = linspace(xmin-dx,xmax+dx,N);
y = x;
for i=1:N
    y(i)=0;
    for j=1:size
        if val(j)<=x(i)
            y(i) = y(i)+1;
        end
    end
end

figure(1)
hold on
y = y/size;
plot(x,y,'o-',mean(val)*ones(1,size),linspace(0,1,size),'-','MarkerSize',10,'LineWidth',1.3);

cd(oldir)

system('cp *.py rve_pert1_2Cell');
oldir = cd('rve_pert1_2Cell');

val=[];

size = 100;

for i=1:size

%     com = ['python rve2Cell.py rve' num2str(i) '.msh ' num2str(i)]
%     system(com) 
    
   txt = ['E_' num2str(i) '_GP_0_tangent.csv']
   tangent = importdata(txt);
   data = tangent.data;
   val = [val; data(1,42)];
end

xmin = min(val);
xmax = max(val);

dx = 0.3*(xmax-xmin);

N = 10;
x = linspace(xmin-dx,xmax+dx,N);
y = x;
for i=1:N
    y(i)=0;
    for j=1:size
        if val(j)<=x(i)
            y(i) = y(i)+1;
        end
    end
end

figure(1)
y = y/size;
plot(x,y,'*--',mean(val)*ones(1,size),linspace(0,1,size),'--','MarkerSize',10,'LineWidth',1.3);
set(gca,'FontSize',16)
xlabel('L_{1111} (MPa)')
ylabel('Cumulative distribution function')

legend('1Cell','Mean value, 1Cell','2Cell', 'Mean value, 2Cell','Location','Best')
legend boxoff

cd(oldir)


print -f1 -depsc Ldistribution.eps
system('epstopdf Ldistribution.eps')