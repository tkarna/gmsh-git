clear all
clc
close all

l = 1.;
t = 0.05*l;
lsca = 4*t;

pert = 0.5;

M = 21;
N = 21;

x = zeros(1,N*M);
y = zeros(1,N*M);

x0 = linspace(-(M-1)/2*l*sqrt(3),(M-1)/2*l*sqrt(3),M)+l*sqrt(3)/4;
y0 = linspace(-(N-1)/2*l*1.5,(N-1)/2*l*1.5,N);


for j=1:N
    for i=1:M
       if (rem(j,2)==1)
        x(i+M*(j-1))= x0(i);
       else
        x(i+M*(j-1))= x0(i)+0.5*l*sqrt(3);
       end
       y(i+M*(j-1))= y0(j);
    end
end



for i=1:N*M
   c = rand();
   phi=rand();
   x(i) = x(i)+c*l*pert*cos(phi*2*pi);
   y(i) = y(i)+c*l*pert*sin(phi*2*pi);
end


[v,c] = voronoin([x(:) y(:)]);
createFile(v,c,min(x0),max(x0),min(y0),max(y0),'voroData.txt');
disp('OK done')
