#ifndef GEO_H_
#define GEO_H_

#include <set>
#include <vector>
#include <stdio.h>
#include <string>
#include <map>
#include <stdlib.h>

class Point{
  public:
    static int maxNum;

  public:
    double x, y, z;
    int num;

  public:
    Point(double xx, double yy, double zz, int nn=0);
    ~Point();
};

class Vec {
  public:
    Point* p;

  public:
    Vec(const Point* A, const Point* B);
    ~Vec();

    double operator[](const int i) const {
      if (i==0) return p->x;
      else if (i==1) return p->y;
      else return 0.;
    }
};



class Line {
  public:
    static int maxNum;
  public:
    Point* p1, *p2;
    int num;
  public:
    Line(Point* p1_, Point* p2_, int n=0);
    ~Line();
    double squareLength() const {
      return (p2->x - p1->x)*(p2->x - p1->x)+(p2->y - p1->y)*(p2->y - p1->y);
    }
};


class LineLoop{
  public:
    static int maxNum;
  public:
    std::vector<Point*> points;
    std::map<int,Line*> lines;
    int num;
  public:
    LineLoop(std::vector<Point*>& pt, std::map<int,Line*>& l,int n = 0);
    ~LineLoop();
};


class Face {
  public:
    static int maxNum;
  public:
    std::set<Point*> points;
    std::set<Line*> lines;
    std::map<int,LineLoop*> loops;
    int num;

  public:
    Face(std::map<int,LineLoop*>& l,int n =0);
    ~Face();
};

#endif // GEO_H_
