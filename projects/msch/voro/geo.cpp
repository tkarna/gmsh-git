#include "geo.h"
#include "math.h"
#include "voro.h"
#include "numeric.h"
#include "filter.h"

int Point::maxNum = 0;
int Line::maxNum = 0;
int LineLoop::maxNum = 0;
int Face::maxNum = 0;

Point::Point(double x_, double y_, double z_,int n):x(x_),y(y_),z(z_){
  if (n>maxNum){
    maxNum = n;
    num = n;
  }
  else{
    maxNum++;
    num = maxNum;
  }
}
Point::~Point(){};

Vec::Vec(const Point* A, const Point* B){
  p = new Point(B->x - A->x,B->y - A->y, B->z - A->z);
};

Vec::~Vec(){
  if (p) delete p;
  p=NULL;
}

Line::Line(Point* p1_, Point* p2_,int n): p1(p1_),p2(p2_){
  if (n>maxNum){
    maxNum = n;
    num = n;
  }
  else{
    maxNum++;
    num = maxNum;
  }
}
Line::~Line(){}


LineLoop::LineLoop(std::vector<Point*>& pt, std::map<int,Line*>& l,int n): points(pt),lines(l){
  if (n>maxNum){
    maxNum = n;
    num = n;
  }
  else{
    maxNum++;
    num = maxNum;
  }
}
LineLoop::~LineLoop(){}

Face::Face(std::map<int,LineLoop*>& l, int n): loops(l){
  for (std::map<int,LineLoop*>::iterator it = l.begin(); it!= l.end(); it++){
    LineLoop* ll = it->second;
    for (std::map<int,Line*>::iterator itl = ll->lines.begin(); itl!= ll->lines.end(); itl++){
      lines.insert(itl->second);
    }
    for (int i=0; i<ll->points.size(); i++){
      points.insert(ll->points[i]);
    }
  }

  if (n>maxNum){
    maxNum = n;
    num = n;
  }
  else{
    maxNum++;
    num = maxNum;
  }
}
Face::~Face(){};

