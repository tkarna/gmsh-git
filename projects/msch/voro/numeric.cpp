#include "numeric.h"
#include "math.h"

double distance(const Point* p1, const Point* p2){
  return distance(p1->x,p1->y,p1->z,p2->x,p2->y,p2->z);
};
double distance(double x1, double y1, double z1, double x2, double y2, double z2){
  return sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1)+(z2-z1)*(z2-z1));
};

bool inside(const Point* pt1, const Point* pt2, const Point* pt3){
  double u21 = pt1->x - pt2->x;
  double v21 = pt1->y - pt2->y;

  double u23 = pt3->x - pt2->x;
  double v23 = pt3->y - pt2->y;

  double val = u21*u23+ v21*v23;
  if (val >0) return false;
  else return true;
}
int sign(const int a){
  if (a>=0 ) return 1;
  else return -1;
}
int e(int i, int j, int k){
  if (i==0 && j==1&& k==2) return 1;
  else if (i==1 && j==2 && k==0 ) return 1;
  else if (i==2 && j==0 && k==1) return 1;
  else if (i==0 && j==2 && k==1 ) return -1;
  else if (i==1 && j==0 && k==2) return -1;
  else if (i==2 && j==1 && k==0) return -1;
  else return 0;
};
 Point cutPoint(const Point* A, const Point* B, const Point* C, const Point* D){
  Point P(0,0,0);
  double xA = A->x; double yA = A->y;
  double xB = B->x; double yB = B->y;

  double xC = C->x; double yC = C->y;
  double xD = D->x; double yD = D->y;

  double det = (xA*yC - xC*yA - xA*yD - xB*yC + xC*yB + xD*yA + xB*yD - xD*yB);
  P.x = (xA*xC*yB - xB*xC*yA - xA*xD*yB + xB*xD*yA - xA*xC*yD + xA*xD*yC + xB*xC*yD - xB*xD*yC)/det;
  P.y = (xA*yB*yC - xB*yA*yC - xA*yB*yD + xB*yA*yD - xC*yA*yD + xD*yA*yC + xC*yB*yD - xD*yB*yC)/det;

  return P;
};

double crossprod(const Vec& n, const Vec& m){
  double val = 0;
  for (int i=0; i<2; i++){
    for (int j=0; j<2; j++){
      val += e(2,i,j)*n[i]*m[j];
    }
  }
  return val;
}


Point* getPointFrom3Point(const Point* A, const Point* B,const Point* C, double t){
  Point* pt= new Point(0.,0., 0.);
  double BA = sqrt((A->x - B->x)*(A->x - B->x) + (A->y - B->y)*(A->y - B->y));
  double BC = sqrt((C->x - B->x)*(C->x - B->x) + (C->y - B->y)*(C->y - B->y));
  double uBA = (A->x - B->x)/BA;
  double vBA = (A->y - B->y)/BA;

  double uBC = (C->x - B->x)/BC;
  double vBC = (C->y - B->y)/BC;

  double u = uBA+uBC;
  double v = vBA+vBC;

  double norm = sqrt(u*u+v*v);
  u/= norm;
  v/= norm;

  double alpha = 0.5*acos(uBA*uBC+vBA*vBC);
  double l = t/sin(alpha);

  pt->x = B->x+l*u;
  pt->y = B->y+l*v;
  return pt;
};

bool isSameSide(const Point* A, const Point* B, const Point* C, const Point* D){
  Vec CA(C,A);
  Vec AD(A,D);
  double n  = crossprod(CA,AD);
  Vec CB(C,B);
  Vec BD(B,D);
  double m =crossprod(CB,BD);

  double si = m*n;
  if (si>=0) return true;
  else return false;
};

