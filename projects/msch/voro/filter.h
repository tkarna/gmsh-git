#ifndef FILTERBOX_H_
#define FILTERBOX_H_

#include "geo.h"
#include <math.h>

// filter base class
class filter{
  public:
    virtual ~filter(){}
    // check if point is inside of domain
    virtual bool inside(const Point* p) const =0;
    // check if point is ouside of domain
    virtual bool outside(const Point* p) const =0;
    // check if point is on domain boundary
    virtual bool boundary(const Point* p) const =0;
    // operator true if inside+boundary else false
    virtual bool operator() (const Point* p) const ;
    // check face is inside
    virtual bool isInside(const Face* face) const;
    // check boundary face if intersection with boundary of domain
    virtual bool isBoundary(const Face* face) const;
    // cut with p1 inside and p2 outsize
    virtual Point cut(Point* p1, Point* p2) const;
    // generate geo file of filter
    virtual void createBoxGeo(FILE* file) const;

    virtual bool isLeft(const Line* line) const;
    virtual bool isRight(const Line* line) const;
    virtual bool isTop(const Line* line) const;
    virtual bool isBottom(const Line* line) const;
};

// rectangular box filter
class filterBox : public filter{
  protected:
    double _x0, _x1;
    double _y0, _y1;

  public:
    filterBox(double x0, double x1, double y0, double y1);
    virtual ~filterBox();
    virtual bool inside(const Point* p) const;
    virtual bool outside(const Point* p) const;
    virtual bool boundary(const Point* p) const;
    virtual void createBoxGeo(FILE* file) const;
    virtual bool isLeft(const Line* line) const;
    virtual bool isRight(const Line* line) const;
    virtual bool isTop(const Line* line) const;
    virtual bool isBottom(const Line* line) const;
};

// rectangular with center circle filter
class filterBoxCircle : public filterBox{
  protected:
    double _xC, _yC; // center
    double _r; // radius

  public:
    filterBoxCircle(double x0, double x1, double y0, double y1,
                 double x, double y, double r);
    virtual ~filterBoxCircle();

    virtual bool inside(const Point* p) const ;
    virtual bool outside(const Point* p) const ;
    virtual bool boundary(const Point* p) const;
    virtual bool operator()(const Point* p) const;
};
#endif // FILTERBOX_H_
