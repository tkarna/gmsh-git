#include "voro.h"
#include "filter.h"
#include <math.h>
#include <string.h>
#include "geo.h"
#include "operations.h"

double voro2Geo::error = 1e-6;
std::map<int,Point*> voro2Geo::allPoints;
std::map<int,Line*> voro2Geo::allLines;
std::map<int,LineLoop*> voro2Geo::allLineLoops;
std::map<int,Face*> voro2Geo::allFaces;

voro2Geo::voro2Geo(double tt, double ls):t(tt), lsca(ls) {
  voro2Geo::error = 1e-6 * t;
};

voro2Geo::~voro2Geo(){
  for (std::map<int,Point*>::iterator it = allPoints.begin(); it != allPoints.end(); it++){
    if (it->second) delete it->second;
    it->second = NULL;
  }

  for (std::map<int,Line*>::iterator it = allLines.begin(); it != allLines.end(); it++){
    if (it->second) delete it->second;
    it->second = NULL;
  }
  for (std::map<int,LineLoop*>::iterator it = allLineLoops.begin(); it != allLineLoops.end(); it++){
    if (it->second) delete it->second;
    it->second = NULL;
  }
  for (std::map<int,Face*>::iterator it = allFaces.begin(); it != allFaces.end(); it++){
    if (it->second) delete it->second;
    it->second = NULL;
  }
};

void voro2Geo::readVoronoiData(FILE* file){
  if (file == NULL) {
    printf("File is not exist \n");
    return;
  }
  else{
    printf("begin reading \n");
    while (!feof (file)){
      char what[256];
      fscanf(file,"%s",what);
      if (!strcmp(what, "$Point")){
        int nump;
        fscanf(file,"%d",&nump);
        for (int i=0; i<nump; i++){
          int n;
          double x, y;
          fscanf(file,"%d %lf %lf",&n,&x,&y);
          // create 2D point
          Point* p = new Point(x,y,0.,n);
          printf("create point: %d  %g %g %g \n",n,x,y,0.);
          // add to library
          voro2Geo::allPoints[n] = p;
        }
      }
      else if (!strcmp(what, "$Cell")){
        int numCell;
        fscanf(file,"%d",&numCell);
        for (int i=0; i<numCell; i++){
          int n, numver;
          fscanf(file,"%d %d",&n,&numver);
          printf("On face %d ver:",n);
          std::vector<Point*> allVer;
          for (int i=0; i<numver; i++){
            int ver;
            fscanf(file,"%d",&ver);
            allVer.push_back(allPoints[ver]);
            printf(" %d ",ver);
          }
          printf("\n");

          // create loop
          LineLoop* loop = NULL;
          createLineLoop(allVer,loop);

          // create cropped loop with thickness t
          LineLoop* cropL = NULL;
          cropLoop(loop,cropL,t);

          // create face with positive loop and negative cropped loop
          std::map<int,LineLoop*> mulLoop;
          mulLoop[loop->num] = loop;
          mulLoop[-1*cropL->num] = cropL;
          Face* face = new Face(mulLoop,n);
          voro2Geo::allFaces[face->num] =  face;
        }
      }
    }
    fclose(file);
  }

  printf("num point = %d \n",allPoints.size());
  printf("num edge = %d \n", allLines.size());
  printf("num loop = %d \n", allLineLoops.size());
  printf("all face = %d \n", allFaces.size());
};

void voro2Geo::writeUncutGeo(FILE* file, filter* fil){
  if (file == NULL){
    printf("file does not exist \n");
    return;
  }

  for (std::map<int,Point*>::iterator it = allPoints.begin(); it!= allPoints.end(); it++){
    Point* pt = it->second;
    fprintf(file,"Point(%d) = {%g, %g, %g, %g}; \n",pt->num, pt->x, pt->y,0.,lsca);
  }

  for (std::map<int,Line*>::iterator itl = allLines.begin(); itl!= allLines.end(); itl++){
    Line* l = itl->second;
    fprintf(file,"Line(%d) = {%d, %d }; \n", l->num,l->p1->num, l->p2->num);
  }

  for (std::map<int,LineLoop*>::iterator it = allLineLoops.begin(); it!= allLineLoops.end(); it++){
    LineLoop* loop = it->second;
    std::map<int,Line*>& lineMap = loop->lines;

    std::map<int,Line*>::iterator itm = lineMap.begin();
    fprintf(file,"Line Loop(%d) = {%d",loop->num,itm->first);

    for (itm = lineMap.begin(); itm!= lineMap.end(); itm++){
      if (itm!= lineMap.begin())
        fprintf(file,",%d ",itm->first);
    }
    fprintf(file,"}; \n");
  };

  for (std::map<int,Face*>::iterator itf = allFaces.begin(); itf != allFaces.end(); itf++){
    Face* f = itf->second;
    std::map<int,LineLoop*>& loop = f->loops;
    std::map<int,LineLoop*>::iterator it = loop.begin();

    fprintf(file,"Plane Surface(%d) = {%d ",f->num,it->first);
    for (it = loop.begin(); it!= loop.end(); it++){
      if (it != loop.begin())
        fprintf(file,",%d ",it->first);
    }
    fprintf(file,"};\n");
  }

  if (fil!= NULL)
    fil->createBoxGeo(file);
};

void voro2Geo::reset(){
  cutPoints.clear();
  cutLines.clear();
  cutLoops.clear();
  cutFaces.clear();
  boundaryLines.clear();
  left.clear();
  right.clear();
  top.clear();
  bottom.clear();
};

void voro2Geo::cutGeo(filter* filter){
  this->reset();
  for (std::map<int,Face*>::iterator itf = allFaces.begin(); itf != allFaces.end(); itf++){
    Face* f = itf->second;
    if (filter->isInside(f)){
      cutFaces.insert(f);
      printf("inner face = %d \n",f->num);
      for (std::map<int,LineLoop*>::iterator itloop = f->loops.begin(); itloop != f->loops.end(); itloop++){
        LineLoop* loop = itloop->second;
        cutLoops.insert(loop);
        for (std::map<int,Line*>::iterator itline = loop->lines.begin(); itline != loop->lines.end(); itline++){
          Line* line = itline->second;
          cutLines.insert(line);
          cutPoints.insert(line->p1);
          cutPoints.insert(line->p2);
        }
      }
    }
    else if (filter->isBoundary(f)){
      Face* cutf = NULL;
      cutFace(f,filter,cutf);
      if (cutf != NULL){
        cutFaces.insert(cutf);
        printf("boundary face = %d \n",f->num);
        printf("cut face = %d \n",cutf->num);

        for (std::map<int,LineLoop*>::iterator itloop = cutf->loops.begin(); itloop != cutf->loops.end(); itloop++){
          LineLoop* loop = itloop->second;
          cutLoops.insert(loop);
          for (std::map<int,Line*>::iterator itline = loop->lines.begin(); itline != loop->lines.end(); itline++){
            Line* line = itline->second;
            cutLines.insert(line);
            cutPoints.insert(line->p1);
            cutPoints.insert(line->p2);
          }
        }
      }
    };
  }

  for (std::set<Line*>::iterator itl = cutLines.begin(); itl!= cutLines.end(); itl++){
    Line* ll = *itl;
    if (filter->boundary(ll->p1) && filter->boundary(ll->p2)){
      boundaryLines.insert(ll);
    }
  };

  for (std::set<Line*>::iterator itl = boundaryLines.begin(); itl!= boundaryLines.end(); itl++){
    Line* line = *itl;
    if (filter->isLeft(line)){
      left.insert(line);
    }
    else if (filter->isRight(line)){
      right.insert(line);
    }
    else if (filter->isTop(line)){
      top.insert(line);
    }
    else if (filter->isBottom(line)){
      bottom.insert(line);
    }
  }
};


void voro2Geo::writeCutGeo(FILE* file){
  if (file == NULL){
    printf("file does not exist \n");
    return;
  }

  for (std::set<Point*>::iterator it = cutPoints.begin(); it!= cutPoints.end(); it++){
    Point* pt = *it;
    fprintf(file,"Point(%d) = {%g, %g, %g, %g}; \n",pt->num, pt->x, pt->y,0.,lsca);
  }
  fflush(file);
  for (std::set<Line*>::iterator itl = cutLines.begin(); itl!= cutLines.end(); itl++){
    Line* l = *itl;
    fprintf(file,"Line(%d) = {%d, %d }; \n", l->num,l->p1->num, l->p2->num);
  }
  fflush(file);
  for (std::set<LineLoop*>::iterator it = cutLoops.begin(); it!= cutLoops.end(); it++){
    LineLoop* loop = *it;
    std::map<int,Line*>& lineMap = loop->lines;
    std::map<int,Line*>::iterator itm = lineMap.begin();
    fprintf(file,"Line Loop(%d) = {%d",loop->num,itm->first);

    for (itm = lineMap.begin(); itm!= lineMap.end(); itm++){
      if (itm!= lineMap.begin())
        fprintf(file,",%d ",itm->first);
    }
    fprintf(file,"}; \n");
  };
  fflush(file);
  for (std::set<Face*>::iterator itf = cutFaces.begin(); itf != cutFaces.end(); itf++){
    Face* f = *itf;
    std::map<int,LineLoop*>& loop = f->loops;
    std::map<int,LineLoop*>::iterator it = loop.begin();

    fprintf(file,"Plane Surface(%d) = {%d ",f->num,it->first);
    for (it = loop.begin(); it!= loop.end(); it++){
      if (it != loop.begin())
        fprintf(file,",%d ",it->first);
    }
    fprintf(file,"};\n");
  }
  fflush(file);
  for (std::set<Face*>::iterator itf = cutFaces.begin(); itf != cutFaces.end(); itf++){
    Face* f = *itf;
    if (itf == cutFaces.begin()){
      fprintf(file,"Physical Surface(11) = {%d ",f->num);
    }
    else{
      fprintf(file,",%d",f->num);
    }
  }
  if (cutFaces.size()>0)
    fprintf(file,"};\n");
  fflush(file);
  // for line


  std::set<Point*> physicalPoint;

  for (std::set<Line*>::iterator itl = bottom.begin(); itl!= bottom.end(); itl++){
    Line* line = *itl;
    if (itl == bottom.begin()){
      fprintf(file,"Physical Line(1) = {%d ",line->num);
      if (physicalPoint.find(line->p1) == physicalPoint.end())
        physicalPoint.insert(line->p1);
      else
        physicalPoint.insert(line->p2);
    }
    else{
      fprintf(file,",%d",line->num);
    }
  }
  if (bottom.size()>0)
    fprintf(file,"};\n");


  for (std::set<Line*>::iterator itl = right.begin(); itl!= right.end(); itl++){
    Line* line = *itl;
    if (itl == right.begin()){
      fprintf(file,"Physical Line(2) = {%d ",line->num);
      if (physicalPoint.find(line->p1) == physicalPoint.end())
        physicalPoint.insert(line->p1);
      else
        physicalPoint.insert(line->p2);
    }
    else{
      fprintf(file,",%d",line->num);
    }
  }
  if (right.size()>0)
    fprintf(file,"};\n");

  for (std::set<Line*>::iterator itl = top.begin(); itl!= top.end(); itl++){
    Line* line = *itl;
    if (itl == top.begin()){
      fprintf(file,"Physical Line(3) = {%d ",line->num);
      if (physicalPoint.find(line->p1) == physicalPoint.end())
        physicalPoint.insert(line->p1);
      else
        physicalPoint.insert(line->p2);
    }
    else{
      fprintf(file,",%d",line->num);
    }
  }
  if (top.size()>0)
    fprintf(file,"};\n");

  for (std::set<Line*>::iterator itl = left.begin(); itl!= left.end(); itl++){
    Line* line = *itl;
    if (itl == left.begin()){
      fprintf(file,"Physical Line(4) = {%d ",line->num);
      if (physicalPoint.find(line->p1) == physicalPoint.end())
        physicalPoint.insert(line->p1);
      else
        physicalPoint.insert(line->p2);
    }
    else{
      fprintf(file,",%d",line->num);
    }
  }
  if (left.size()>0)
    fprintf(file,"};\n");

  int idex = 1;
  for (std::set<Point*>::iterator it = physicalPoint.begin(); it!= physicalPoint.end(); it++){
    Point* p = *it;
    fprintf(file,"Physical Point(%d) = {%d};\n",idex,p->num);
    idex++;
  }
  fflush(file);
};
