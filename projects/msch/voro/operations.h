#ifndef OPERATIONS_H_
#define OPERATIONS_H_

#include "geo.h"

class filter;

// geometrical operations
int createPoint(double x, double y, double z, Point*& point);
int createLine(Point* p1, Point* p2, Line*& line);
int createLineLoop(std::vector<Point*>& pt, std::map<int,Line*>& l, LineLoop*& loop);
int createLineLoop(std::vector<Point*>& pt, LineLoop*& loop);
int createLineLoop(std::map<int,Line*>&, LineLoop*& loop);
int createFace(std::map<int,LineLoop*> l, Face*& face);

void cropLoop(LineLoop* loop, LineLoop*& croploop, double t);
void seftCut(LineLoop*& loop);
void cutFace(Face* face, filter* filter, Face*& newface);
#endif // OPERATIONS_H_
