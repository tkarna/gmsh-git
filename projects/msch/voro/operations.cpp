#include "operations.h"
#include "voro.h"
#include "numeric.h"
#include "filter.h"

int createPoint(double x, double y, double z, Point*& point){
  point = NULL;
  for (std::map<int,Point*>::iterator it = voro2Geo::allPoints.begin(); it!= voro2Geo::allPoints.end(); it++){
    Point* p = it->second;
    if (distance(x,y,z,p->x,p->y,p->z)<voro2Geo::error){
      point = p;
      printf("take %g %g %g existing point %d: %g, %g %g, error %g\n",x,y,z,p->num, p->x, p->y,p->z,voro2Geo::error);
      break;
    }
  }
  if (point == NULL){
    point = new Point(x,y,z);
    printf("create new point %d : %g %g %g\n",point->num,x,y,z);
    voro2Geo::allPoints[point->num] = point;
  }
  return point->num;
};

int createLine(Point* p1, Point* p2, Line*& line){
  line = NULL;
  if (p1==NULL or p2 == NULL){
    printf("Error in creation of lines \n");
    return 0;
  }
  else if (p1->num == p2->num){
    printf("impossible to create line with same point\n");
    return 0;
  }

  int samedir = 1;
  for (std::map<int,Line*>::iterator it = voro2Geo::allLines.begin(); it!= voro2Geo::allLines.end(); it++){
    Line* l = it->second;
    if (l->p1->num == p1->num && l->p2->num == p2->num){
      line = l;
      samedir = 1;
      break;
    }
    else if (l->p1->num == p2->num && l->p2->num == p1->num){
      line = l;
      samedir = -1;
      break;
    }
  }
  if (line==NULL){
    line = new Line(p1,p2);
    voro2Geo::allLines[line->num] = line;
  }
  return line->num* samedir;
};

int createLineLoop(std::vector<Point*>& pt, std::map<int,Line*>& l, LineLoop*& loop){
  loop = new LineLoop(pt,l);
  voro2Geo::allLineLoops[loop->num] = loop;
  return loop->num;
};

int  createLineLoop(std::vector<Point*>& pt, LineLoop*& loop){
  if (pt.size() <=2){
    loop = NULL;
    return 0;
  }
  std::map<int,Line*> lineMap;
  for (int i=0; i<pt.size()-1; i++){
    Line* ll = NULL;
    int num = createLine(pt[i],pt[i+1],ll);
    if (num != 0)
      lineMap[num] = ll;
    else
      printf("Error in  voro2Geo::createLineLoop\n ");
  }
  Line* lll = NULL;
  int numl = createLine(pt[pt.size()-1],pt[0],lll);
  if (numl!= 0)
    lineMap[numl] = lll;
  else
      printf("Error in  voro2Geo::createLineLoop\n ");

  numl = createLineLoop(pt,lineMap,loop);
  return numl;
};

int createLineLoop(std::map<int,Line*>& lineMap, LineLoop*& loop){
  if (lineMap.size() <=2){
    loop = NULL;
    return 0;
  }
  std::set<Point*> point;
  for (std::map<int,Line*>::iterator it =lineMap.begin(); it!= lineMap.end(); it++){
    Line* line= it->second;
    point.insert(line->p1);
    point.insert(line->p2);
  }
  std::vector<Point*> vv(point.begin(),point.end());
  createLineLoop(vv,lineMap,loop);
  return loop->num;
};


int createFace(std::map<int,LineLoop*> l, Face*& face){
  face = new Face(l);
  voro2Geo::allFaces[face->num] = face;
  return face->num;
};

void cropLoop(LineLoop* loop, LineLoop*& croploop, double t){
  std::vector<Point*>& points = loop->points;
  std::vector<Point*> cropPoints;
  Point* pt=getPointFrom3Point(points[points.size()-1],points[0],points[1],t);
  voro2Geo::allPoints[pt->num] = pt;
  cropPoints.push_back(pt);

  for (int i=1; i<points.size()-1; i++){
    pt=getPointFrom3Point(points[i-1],points[i],points[i+1],t);
    voro2Geo::allPoints[pt->num] = pt;
    cropPoints.push_back(pt);
  }

  pt=getPointFrom3Point(points[points.size()-2],points[points.size()-1],points[0],t);
  voro2Geo::allPoints[pt->num] = pt;
  cropPoints.push_back(pt);

  createLineLoop(cropPoints,croploop);
  seftCut(croploop);
};

void seftCut(LineLoop*& loop){
  std::map<int,Line*>& lines = loop->lines;
  std::vector<Point*>& points = loop->points;

  std::set<Point*> in, out;
  int s = points.size();

  if (s>4){
    for (std::map<int,Line*>::iterator it = lines.begin(); it!= lines.end(); it++){
      Line* l = it->second;
      std::set<Point*> left, right;
      for (int i=0; i<s; i++){
        if (l->p1 == points[i] or l->p2 == points[i]) continue;
        if (left.size() == 0 && right.size() == 0){
          left.insert(points[i]);
        }
        else if (left.size()>0){
          if (isSameSide(points[i],*left.begin(),l->p1,l->p2)){
            left.insert(points[i]);
          }
          else{
            right.insert(points[i]);
          }
        }
      }
      if (left.size() == 1 && right.size() >1) out.insert(*left.begin());
      if (right.size() == 1 && left.size() >1) out.insert(*right.begin());
    }

    for (int i=0; i< points.size(); i++){
      if (out.find(points[i]) ==out.end()){
        in.insert(points[i]);
      }
    }
  }
  else if (s==4){
    double lIn = 0.;
    double lOut = 0.;
    bool autocut = false;

    for (std::map<int,Line*>::iterator it1 = lines.begin(); it1!= lines.end(); it1++){
      Line* l1 = it1->second;
      for (std::map<int,Line*>::iterator it2 = lines.begin(); it2!= lines.end(); it2++){
        Line* l2 = it2->second;
        if (l1->p1 == l2->p1 or l1->p2 == l2->p1 or l1->p1 == l2->p2 or l1->p2 == l2->p2) continue;
        if (isSameSide(l1->p1,l1->p2,l2->p1,l2->p2) == false){
          autocut = true;
          break;
        }
      };
      if (autocut) break;
    }

    if (autocut){
      bool found = false;
      for (std::map<int,Line*>::iterator it1 = lines.begin(); it1!= lines.end(); it1++){
        Line* l1 = it1->second;
        for (std::map<int,Line*>::iterator it2 = lines.begin(); it2!= lines.end(); it2++){
          Line* l2 = it2->second;
          if (l1->p1 == l2->p1 or l1->p2 == l2->p1 or l1->p1 == l2->p2 or l1->p2 == l2->p2) continue;
          if (isSameSide(l1->p1,l1->p2,l2->p1,l2->p2)){
            in.insert(l1->p1);
            in.insert(l1->p2);
            lIn = l1->squareLength();
            out.insert(l2->p1);
            out.insert(l2->p2);
            lOut = l2->squareLength();
            found = true;
            break;
            printf("found \n");
          }
        };
        if (found) break;
      }

      if (lIn < lOut){
        out.clear();
        printf("reverse\n");
        out.insert(in.begin(),in.end());
      }
    }
  }


  if (out.size() ==0 or in.size() ==0) {
    return;
  }

  printf(" s= %d , out = %d\n",s,out.size());

  std::map<int,Line*> allElim;
  for (std::map<int,Line*>::iterator it = lines.begin(); it!= lines.end(); it++){
    Line* l = it->second;
    if (out.find(l->p1) != out.end() && out.find(l->p2) != out.end()){
      printf("eliminate = %d \n",it->first);

      Point* p1 = l->p1;
      Point* p2 = l->p2;

      Line* linit = NULL;
      int numInit = 0;
      for (std::map<int,Line*>::iterator it = lines.begin(); it!= lines.end(); it++){
        Line* ll = it->second;
        if (ll == l) continue;
        if (ll->p1 == p1){
          linit = ll;
          break;
        }
        else if (ll->p2 == p1){
          linit = ll;
          break;
        }
      }

      Line* lend= NULL;
      for (std::map<int,Line*>::iterator it = lines.begin(); it!= lines.end(); it++){
        Line* ll = it->second;
        if (ll == l) continue;
        if (ll->p1 == p2 ){
          lend = ll;
          break;
        }
        else if (ll->p2 == p2 ){
          lend = ll;
          break;
        }
      }

      if (out.find(linit->p1) != out.end() && out.find(linit->p2) != out.end() &&
          out.find(lend->p1) != out.end() && out.find(lend->p2) != out.end()){
        printf("double \n");
      }
      else{
        allElim[it->first] = l;
      }
    }
  };
  for (std::map<int,Line*>::iterator itl = allElim.begin(); itl!= allElim.end(); itl++ ){
    Line* l = itl->second;
    Point* p1 = l->p1;
    Point* p2 = l->p2;
    int numElim = itl->first;

    Point* pinint = NULL;
    Point* pend = NULL;

    Point* pRemoveInit = NULL;
    Point* pRemoveEnd = NULL;

    Line* linit = NULL;
    int numInit = 0;
    for (std::map<int,Line*>::iterator it = lines.begin(); it!= lines.end(); it++){
      Line* ll = it->second;
      if (ll == l) continue;
      if (ll->p1 == p1){
        linit = ll;
        numInit = it->first;
        pinint = ll->p2;
        pRemoveInit = ll->p1;
        break;
      }
      else if (ll->p2 == p1){
        linit = ll;
        numInit = it->first;
        pinint = ll->p1;
        pRemoveInit = ll->p2;
        break;
      }
    }

    Line* lend= NULL;
    int numEnd = 0;
    for (std::map<int,Line*>::iterator it = lines.begin(); it!= lines.end(); it++){
      Line* ll = it->second;
      if (ll == l) continue;
      if (ll->p1 == p2 ){
        lend = ll;
        numEnd = it->first;
        pend = ll->p2;
        pRemoveEnd = ll->p1;
        break;
      }
      else if (ll->p2 == p2 ){
        lend = ll;
        numEnd = it->first;
        pend = ll->p1;
        pRemoveEnd = ll->p2;
        break;
      }
    }

    printf("pint = %d, pReinit  = %d, pReend = %d, pend = %d, lint = %d, lend = %d\n",
           pinint->num,pRemoveInit->num, pRemoveEnd->num,pend->num,numInit,numEnd);

    Point* newPoint = NULL;
    Point newp = cutPoint(pinint,pRemoveInit,pRemoveEnd,pend);
    createPoint(newp.x,newp.y,newp.z,newPoint);

    if (linit->p1 == pRemoveInit){
      linit->p1 = newPoint;
      voro2Geo::allPoints.erase(pRemoveInit->num);
      delete pRemoveInit;
    }
    else if (linit->p2 == pRemoveInit){
      linit->p2 = newPoint;
      voro2Geo::allPoints.erase(pRemoveInit->num);
      delete pRemoveInit;
    }

    if (lend->p1 == pRemoveEnd){
      lend->p1 = newPoint;
      voro2Geo::allPoints.erase(pRemoveEnd->num);
      delete pRemoveEnd;
    }
    else if (lend->p2 == pRemoveEnd){
      lend->p2 = newPoint;
      voro2Geo::allPoints.erase(pRemoveEnd->num);
      delete pRemoveEnd;
    }

  }

  for (std::map<int,Line*>::iterator itl = allElim.begin(); itl!= allElim.end(); itl++ ){
    lines.erase(itl->first);
  };

  points.clear();
  std::set<Point*> pp;
  for (std::map<int,Line*>::iterator it = lines.begin(); it!= lines.end(); it++){
    pp.insert(it->second->p1);
    pp.insert(it->second->p2);
  };
  points.assign(pp.begin(),pp.end());
};


void cutFace(Face* face, filter* filter, Face*& newface){
  newface = NULL;
  std::map<int,LineLoop*>& loops = face->loops;
  if (loops.size()!= 2) {
    printf("This work with 2 loop only \n");
    return;
  }
  else{
    LineLoop* loop1, *loop2;
    std::map<int,LineLoop*>::iterator it = loops.begin();
    if (it->first>0){
      loop1 = it->second;
      it++;
      loop2 = it->second;
    }
    else{
      loop2 = it->second;
      it++;
      loop1 = it->second;
    }

    std::map<int,Line*> newgrOut, newgrIn;
    int cutOut = 0;
    int cutIn = 0;

    std::map<int,Line*>& line1 = loop1->lines;
    Point* pOut1=NULL;
    Point* pOut2 = NULL;
    for (std::map<int,Line*>::iterator it = line1.begin(); it!= line1.end(); it++){
      Line* ll = it->second;
      printf("outter line %d \n",it->first);
      Point* pinit = NULL;
      Point* pend = NULL;
      if (it->first>0){
        pinit = ll->p1;
        pend = ll->p2;
      }
      else{
        pinit = ll->p2;
        pend = ll->p1;
      }

      if (filter->operator()(pinit) && filter->operator()(pend)){
        newgrOut[it->first] = ll;
      }
      else if (filter->operator()(pinit) && !filter->operator()(pend)){
        Point p = filter->cut(pinit,pend);
        createPoint(p.x, p.y, p.z, pOut1);
        printf("create p11: %d \n",pOut1->num);
        Line* line = NULL;
        int num = createLine(pinit,pOut1,line);
        if (num!= 0)
          newgrOut[num] = line;
        cutOut++;
      }
      else if (!filter->operator()(pinit) && filter->operator()(pend)){
        Point p = filter->cut(pinit,pend);
        createPoint(p.x, p.y, p.z, pOut2);
        printf("create p12: %d \n",pOut2->num);
        Line* line = NULL;
        int num = createLine(pOut2,pend,line);
        if (num!= 0)
          newgrOut[num] = line;
        cutOut++;
      }
    }

    std::map<int,Line*>& line2 = loop2->lines;
    Point* pIn1=NULL;
    Point* pIn2 = NULL;
    for (std::map<int,Line*>::iterator it = line2.begin(); it!= line2.end(); it++){
      Line* ll = it->second;
      printf("inner line %d \n",it->first);
      Point* pinit = NULL;
      Point* pend = NULL;
      if (it->first>0){
        pinit = ll->p1;
        pend = ll->p2;
      }
      else{
        pinit = ll->p2;
        pend = ll->p1;
      }

      if (filter->operator()(pinit) && filter->operator()(pend)){
        newgrIn[it->first] = ll;
      }
      else if (filter->operator()(pinit) && !filter->operator()(pend)){
        Point p = filter->cut(pinit,pend);
        createPoint(p.x, p.y, p.z,pIn1);
        printf("create p21: %d \n",pIn1->num);
        Line* line = NULL;
        int num = createLine(pinit,pIn1,line);
        if (num != 0)
          newgrIn[num] = line;
        cutIn++;
      }
      else if (!filter->operator()(pinit) && filter->operator()(pend)){
        Point p = filter->cut(pinit,pend);
        createPoint(p.x, p.y, p.z, pIn2);
        printf("create p22: %d \n",pIn2->num);
        Line* line = NULL;
        int num = createLine(pIn2,pend,line);
        if (num != 0)
          newgrIn[num] = line;
        cutIn++;
      }
    }

    printf(" cut Out = %d , cut in = %d \n",cutOut,cutIn);
    if (cutOut == 2 && cutIn == 2){
      Line* l1 = NULL;
      Line* l2 = NULL;
      int num1  = createLine(pOut1,pIn1,l1);
      int num2 = createLine(pIn2,pOut2,l2);

      if (num1 != 0) {
        newgrOut[num1] = l1;
      }

      if (num2!= 0){
        newgrOut[num2] = l2;
      }

      for (std::map<int,Line*>::iterator it = newgrIn.begin(); it!= newgrIn.end(); it++){
        newgrOut[-1*it->first] = it->second;
      }
      // boundary lines

      LineLoop* lineloop = NULL;
      int numloop = createLineLoop(newgrOut,lineloop);
      if (numloop != 0){
        std::map<int,LineLoop*> lllll;
        lllll[lineloop->num] = lineloop;
        createFace(lllll, newface);
      }

    }
    else if (cutOut==2 && cutIn == 0){
      Line* l1 = NULL;
      int num1 = createLine(pOut1,pOut2,l1);
      if (num1!= 0){
        newgrOut[l1->num] = l1;
      }

      LineLoop* lineloop = NULL;
      int numloop = createLineLoop(newgrOut,lineloop);

      if (numloop!= 0){
        std::map<int,LineLoop*> lllll;
        lllll[lineloop->num] = lineloop;

        if (newgrIn.size() == line2.size()){
          lllll[loop2->num] = loop2;
        }
        else if (newgrIn.size() == 0){

        }
        else{
          printf("Fatal error \n");
        }
        createFace(lllll, newface);
      }


    }
  }
};
