#include <iostream>
#include "voro.h"
#include "filter.h"


using namespace std;

int main(int argc, char* argv[])
{
  printf("voro input.txt xCut yCut out.geo\n", argc);
  for (int i=0; i<argc; i++){
    printf("argument %d: %s \n",i,argv[i]);
  }

  int Mcut_set = 0;
  int Ncut_set = 0;
  std::string inputFileName_set ="";
  std::string outputFileName_set ="";

  if (argc==5){
    inputFileName_set = argv[1];
    printf("input file: %s \n",inputFileName_set.c_str());
    Mcut_set  = atoi(argv[2]);
    Ncut_set  = atoi(argv[3]);
    outputFileName_set = argv[4];
    printf("out file: %s \n",outputFileName_set.c_str());
  }
  else{
    printf("voro input.txt xCut yCut out.geo\n", argc);
    return 0;
  }

  double l = 1.;
  double t = 0.025*l;
  double lsca = 4*t;


  int Mcut =39;
  if (Mcut_set>0) Mcut = Mcut_set;

  int Ncut =69;
  if (Ncut_set>0) Ncut = Ncut_set;


  double xmin = -(Mcut-1.)/2.*l*sqrt(3);
  double xmax = (Mcut-1.)/2.*l*sqrt(3);
  double ymin = -(Ncut-1.)/2.*l*1.5;
  double ymax = (Ncut-1.)/2.*l*1.5;
  printf("xmin = %e, xmax  = %e, ymin = %e, ymax = %e \n",xmin,xmax,ymin,ymax);

  double xC = 0.;
  double yC = 0.;
  double r = 0.25*(xmax-xmin);

  //filterBox* fBox = new filterBoxCircle(xmin, xmax, ymin, ymax,xC,yC,r);
  filterBox* fBox = new filterBox(xmin, xmax, ymin, ymax);

  // write box geometry to file
  std::string boxFileName = "box.geo";
  FILE* boxFile = fopen(boxFileName.c_str(),"w");
  fBox->createBoxGeo(boxFile);
  fclose(boxFile);


  // create voro object
  voro2Geo* voro = new voro2Geo(t,lsca);

  // load input file
  std::string inputFileName = "voroData.txt";
  if (inputFileName_set != "")
    inputFileName = inputFileName_set;
  FILE* inputFile = fopen(inputFileName.c_str(),"r");
  voro->readVoronoiData(inputFile);

  // write uncut geometry
  std::string uncutFileName = "unCutGeo.geo";
  FILE* uncutFile = fopen(uncutFileName.c_str(),"w");
  voro->writeUncutGeo(uncutFile,fBox);
  fclose(uncutFile);

  // cut geomertry
  voro->cutGeo(fBox);

  // write cut geometry to file
  std::string outputFileName = "voro.geo";
  if (outputFileName_set != "")
    outputFileName = outputFileName_set;
  FILE* outputFile = fopen(outputFileName.c_str(),"w");
  voro->writeCutGeo(outputFile);
  fclose(outputFile);

  delete voro;
  delete fBox;

/*

  for (int i=1; i<2; i++){

    voro2Geo::allPoints.clear();
    voro2Geo::allLines.clear();
    voro2Geo::allLineLoops.clear();
    voro2Geo::allFaces.clear();

    int Mcut = 3;
    int Ncut = 5;

    double l = 1;
    double t = 0.05*l;
    double lsca = 4*t;

    voro2Geo* voro = new voro2Geo(t,lsca);
    std::string filename = "voroData" + int2str(i)+".txt";
    voro->readVoronoiData(filename);


    double xmin = -(Mcut-1)/2.*l*sqrt(3);
    double xmax = (Mcut-1)/2.*l*sqrt(3);
    double ymin = -(Ncut-1)/2.*l*1.5;
    double ymax = (Ncut-1)/2.*l*1.5;
    printf("xmin = %e, xmax  = %e, ymin = %e, ymax = %e \n",xmin,xmax,ymin,ymax);

    //voro->writeGeo("unCutGeo.geo",xmin,xmax,ymin,ymax);



    filterBox * filter = new filterBox(xmin, xmax, ymin, ymax);
    //filter->createBoxGeo("box.geo",(Mcut-1)/2+1,(Ncut-1)/4+1,lsca);
    std::string outfile = "rve"+int2str(i)+".geo";
    voro->cutGeo(filter);
    voro->writeCutGeo(outfile);

    std::string txt = "gmsh "+ outfile+" -2 -order 2 -algo delquad";
    system(txt.c_str());
    delete voro;
    delete filter;

  }
*/
  return 0;

}
