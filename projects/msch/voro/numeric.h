#ifndef NUMERIC_H_
#define NUMERIC_H_
#include "geo.h"
#include <string.h>
#include <sstream>

inline std::string int2str(int number){
   std::stringstream ss;//create a stringstream
   ss << number;//add number to the stream
   return ss.str();//return a string with the contents of the stream
};

double distance(const Point* p1, const Point* p2);
double distance(double x1, double y1, double z1, double x2, double y2, double z2);

/* check pt2 inside pt1 and pt3*/
bool inside(const Point* pt1, const Point* pt2, const Point* pt3);

/*returen the sign of a*/
int sign(const int a);

int e(int i, int j, int k);

Point cutPoint(const Point* A, const Point* B, const Point* C, const Point* D);
double crossprod(const Vec& n, const Vec& m);

bool isSameSide(const Point* A, const Point* B, const Point* C, const Point* D);
Point* getPointFrom3Point(const Point* A, const Point* B,const Point* C, double t);

#endif // NUMERIC_H_
