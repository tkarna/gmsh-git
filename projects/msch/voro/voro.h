#ifndef VORO_H_
#define VORO_H_

#include <set>
#include <vector>
#include <stdio.h>
#include <string>
#include <map>
#include <stdlib.h>

#include "geo.h"
class filter;
class filterBox;

class voro2Geo{
  public:
    static double error; // numerical precision
    // all geometries by static variable
    static std::map<int,Point*> allPoints;
    static std::map<int,Line*> allLines;
    static std::map<int,LineLoop*> allLineLoops;
    static std::map<int,Face*> allFaces;


  protected:
    // all cut geometries
    std::set<Point*> cutPoints;
    std::set<Line*> cutLines;
    std::set<LineLoop*> cutLoops;
    std::set<Face*> cutFaces;
    std::set<Line*> boundaryLines;
    // 4 physical Line o
    std::set<Line*> left, right, top, bottom;

    double t; // half of thickness
    double lsca; // characteristic length

  public:
    voro2Geo(double tt, double ls);
    virtual ~voro2Geo();

    // read voroi data
    void readVoronoiData(FILE* file);
    // write uncut geometry to file
    void writeUncutGeo(FILE* file, filter* f = NULL);
    // reset all
    void reset();
    // cut geometry
    void cutGeo(filter* filter);
    // write cut geometry to file
    void writeCutGeo(FILE* file);
};


#endif // VORO_H_
