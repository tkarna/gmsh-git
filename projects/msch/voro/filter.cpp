#include "filter.h"
#include "numeric.h"
#include "voro.h"

bool filter::operator() (const Point* p) const {
  if (inside(p) or boundary(p)) return true;
  else return false;
};

bool filter::isInside(const Face* face) const{
  const std::set<Point*>& pset = face->points;
  for (std::set<Point*>::iterator it = pset.begin(); it!= pset.end(); it++){
    if (outside(*it)){
      return false;
    }
  }
  return true;
};

bool filter::isBoundary(const Face* face) const{
  const std::set<Point*>& pset = face->points;
  int in = 0;
  int out = 0;
  for (std::set<Point*>::iterator it = pset.begin(); it!= pset.end(); it++){
    if (inside(*it) or boundary(*it)){
      in++;
    }
    else if (outside(*it)){
      out++;
    }
    if (in>0 && out>0) break;
  }
  return (in>0 && out>0);
};

// cut with p1 inside and p2 outsize
Point filter::cut(Point* p1, Point* p2) const {
 /*
  printf("cut box %g %g %g %g \n",_x0,_x1,_y0,_y1);
  printf("cut vertex %d-> %g %g,  %d -> %g %g \n", p1->num,p1->x,p1->y, p2->num,p2->x,p2->y);
  */
  double tol = 1.e-16;
  int maxiter = 1000;
  bool ok = true;
  Point pa(0.,0.,0.);
  Point pb(0.,0.,0.);
  double length = distance(p1,p2);
  if (inside(p1) && outside(p2)){
    pa.x = p1->x;
    pa.y = p1->y;

    pb.x = p2->x;
    pb.y = p2->y;
  }
  else if (inside(p2)&& outside(p1)){
    pa.x = p2->x;
    pa.y = p2->y;

    pb.x = p1->x;
    pb.y = p1->y;
  }
  else if (boundary(p1) && !boundary(p2)){
    return *p1;
  }
  else if (boundary(p2) && !boundary(p1)){
    return *p2;
  }
  else if (boundary(p1)&& boundary(p2)){
    printf("two points lie on the boundary\n");
    return *p1;
  }

  int iter = 0;
  while (ok){
    iter++;
    double x = (pa.x+ pb.x)/2.;
    double y = (pa.y+ pb.y)/2.;
    double z = (pa.z+ pb.z)/2.;
    Point p(x,y,z);
    if (inside(&p)){
      pa.x = p.x;
      pa.y = p.y;
      pa.z = p.z;
    }
    else if (outside(&p)){
      pb.x = p.x;
      pb.y = p.y;
      pb.z = p.z;
    }
    else if (boundary(&p)){
      return p;
    }

    double rel = distance(&pa,&pb)/length;
    //printf("tol = %e \n",dist);
    if (rel <tol or iter>maxiter) break;

    if (iter > maxiter)
     printf("exceed maximal iteration");

  }
  //printf("result %g %g with num of iter = %d \n",pa.x,pa.y,iter);
  return pa;
}

bool filter::isLeft(const Line* line) const{
  printf("this is not defined\n");
  return false;
}
bool filter::isRight(const Line* line) const{
  printf("this is not defined\n");
  return false;
}
bool filter::isTop(const Line* line) const{
  printf("this is not defined\n");
  return false;
}
bool filter::isBottom(const Line* line) const{
  printf("this is not defined\n");
  return false;
}
void filter::createBoxGeo(FILE* file) const{
 printf("this is not defined\n");
}

filterBox::filterBox(double x0, double x1, double y0, double y1): _x0(x0),_x1(x1),_y0(y0),_y1(y1){};
filterBox::~filterBox(){};
bool filterBox::inside(const Point* p) const{
  if (p->x < _x1 -voro2Geo::error  && p->x > _x0+voro2Geo::error &&
      p->y<_y1-voro2Geo::error  && p->y> _y0 +voro2Geo::error ) return true;
  else return false;
};

bool filterBox::outside(const Point* p) const{
  if (p->x > _x1+voro2Geo::error  || p->x < _x0-voro2Geo::error  ||
      p->y>_y1+voro2Geo::error  || p->y< _y0-voro2Geo::error ) return true;
  else return false;
};

bool filterBox::boundary(const Point* p) const{
  if (inside(p) or outside(p)) return false;
  else return true;
};

bool filterBox::isLeft(const Line* line) const{
  double x1 = line->p1->x;
  double x2 = line->p2->x;
  if (fabs(x1-_x0) < voro2Geo::error && fabs(x2-_x0)< voro2Geo::error) return true;
  else return false;
};

bool filterBox::isRight(const Line* line) const{
  double x1 = line->p1->x;
  double x2 = line->p2->x;
  if (fabs(x1-_x1) < voro2Geo::error && fabs(x2-_x1)< voro2Geo::error) return true;
  else return false;
};

bool filterBox::isTop(const Line* line) const{
  double y1 = line->p1->y;
  double y2 = line->p2->y;
  if (fabs(y1-_y1) < voro2Geo::error && fabs(y2-_y1)< voro2Geo::error) return true;
  else return false;
};

bool filterBox::isBottom(const Line* line) const{
  double y1 = line->p1->y;
  double y2 = line->p2->y;
  if (fabs(y1-_y0) < voro2Geo::error && fabs(y2-_y0)< voro2Geo::error) return true;
  else return false;
};

void filterBox::createBoxGeo(FILE* file) const{
  if  (file == NULL) {
    printf("file does not exist \n");
    return;
  }
  double lsca = 1.;
  fprintf(file,"p1 = newp; Point(p1) = {%g,%g,0,%g};\n",_x0,_y0,lsca);
  fprintf(file,"p2 = newp; Point(p2) = {%g,%g,0,%g};\n",_x1,_y0,lsca);
  fprintf(file,"p3 = newp; Point(p3) = {%g,%g,0,%g};\n",_x1,_y1,lsca);
  fprintf(file,"p4 = newp; Point(p4) = {%g,%g,0,%g};\n",_x0,_y1,lsca);
  fprintf(file,"l1 = newl; Line(l1) = {p1,p2};\n");
  fprintf(file,"l2 = newl; Line(l2) = {p2,p3};\n");
  fprintf(file,"l3 = newl; Line(l3) = {p3,p4};\n");
  fprintf(file,"l4 = newl; Line(l4) = {p4,p1};\n");
  fprintf(file,"l5 = newl; Line Loop(l5) = {l1,l2,l3,l4};\n");
  fprintf(file,"s1 = newreg; Plane Surface(s1) = {l5};\n");
}

filterBoxCircle::filterBoxCircle(double x0, double x1, double y0, double y1,
                 double x, double y, double r): filterBox(x0,x1,y0,y1),_xC(x),_yC(y),_r(r){};
filterBoxCircle::~filterBoxCircle(){};

bool filterBoxCircle::inside(const Point* p) const {
  if (filterBox::inside(p) == true){
    double d = sqrt((p->x - _xC)*(p->x - _xC) + (p->y - _yC)*(p->y - _yC));
    if (d>_r) return true;
    else return false;
  }
  else
    return false;
};
bool filterBoxCircle::outside(const Point* p) const {
  if (filterBox::outside(p)) return true;
  if (filterBox::inside(p)){
    double d = sqrt((p->x - _xC)*(p->x - _xC) + (p->y - _yC)*(p->y - _yC));
    if (d<_r) return true;
    else return false;
  }
  else return false;
};
bool filterBoxCircle::boundary(const Point* p) const {
  if (filterBox::boundary(p)) return true;
  if (filterBox::inside(p)){
    double d = sqrt((p->x - _xC)*(p->x - _xC) + (p->y - _yC)*(p->y - _yC));
    if (d==_r) return true;
    else return false;
  }
  else return false;
};

bool filterBoxCircle::operator()(const Point* p) const{
  if (filterBox::operator()(p) == true){
    double d = sqrt((p->x - _xC)*(p->x - _xC) + (p->y - _yC)*(p->y - _yC));
    if (d>=_r) return true;
    else return false;
  }
  else
    return false;

};
