#ifndef HODGMATERIALLAW_H_
#define HODGMATERIALLAW_H_

#include "dG3DMaterialLaw.h"
#include "STensor53.h"
#include "STensor63.h"
#include "numericalMaterial.h"
#include "elasticLaw.h"


class hoDGElasticMaterialLaw : public dG3DMaterialLaw{
  public:
    STensor63 elasticSecondOrderTangent;
    STensor53 elasticSecondFirstTangent;
    STensor53 elasticFirstSecondTangent;
  #ifndef SWIG
  protected:
    elasticLawBase* _elasticFirst,* _elasticSecond;
    elasticLawBase::ELASTIC_TYPE _typeFirst, _typeSecond;
    bool _filled;
  #endif

  public:
    hoDGElasticMaterialLaw(const int num, const double rho, const bool init);
    hoDGElasticMaterialLaw(const int num, const int first, const int second,
                           const double K, const double mu, const double rho, const double tol = 1e-6,
                          const bool init=true);
    void setType(const int first, const int second = -1);
    void setParameter(const std::string key, const double val);

    #ifndef SWIG
    hoDGElasticMaterialLaw(const hoDGElasticMaterialLaw& src);
    virtual ~hoDGElasticMaterialLaw();


    virtual matname getType() const{return materialLaw::secondOrderElastic;};
    virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const;
    virtual void createIPVariable(IPVariable* &ipv,const MElement *ele=NULL, const int nbFF_=0) const;
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw);
    virtual double soundSpeed() const;
    virtual bool isHighOrder() const;
    virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true);
    virtual void setElasticStiffness(IPVariable* ipv);

    virtual void fillElasticStiffness(elasticLawBase* firstlaw, STensor43 &tangent) const;
    virtual void fillElasticStiffness(elasticLawBase* secondlaw,STensor63& secondsecond);
    #endif //SWIG
};

/**
 full law with w = 0.5*L_ijkl*E_ij*E_kl+ 0.5*J_ijkpqr*G_ijk*G_pqr+b_i*E_jk*G_ijk
**/

class fullHODGElasticMaterialLaw : public hoDGElasticMaterialLaw{
  protected:
    elasticLawBase* _elasticFirstSecond;

  public:
    fullHODGElasticMaterialLaw(const int num, const double rho, const bool init);
    fullHODGElasticMaterialLaw(const int num, const int first, const int second,
                           const double K, const double mu, const double rho, const double tol = 1e-6,
                          const bool init=true);
    void setParameter(const std::string key, const double val);

    #ifndef SWIG
    fullHODGElasticMaterialLaw(const fullHODGElasticMaterialLaw& src);
    virtual ~fullHODGElasticMaterialLaw();


    virtual matname getType() const{return materialLaw::secondOrderElastic;};
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw);

    virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true);

    virtual void fillElasticStiffness(elasticLawBase* fs, STensor53& firstsecond, STensor53& secondfirst);
    #endif //SWIG
};



class hoDGMultiscaleMaterialLaw : public dG3DMaterialLaw,
                                  public numericalMaterial{
  public:
    STensor63 elasticSecondOrderTangent;
    STensor53 elasticSecondFirstTangent;
    STensor53 elasticFirstSecondTangent;
  protected:
    bool _filled;

  public:
    hoDGMultiscaleMaterialLaw(const int lnum, const int tag, const bool init= true);
    #ifndef SWIG
    hoDGMultiscaleMaterialLaw(const hoDGMultiscaleMaterialLaw& src)
                            : dG3DMaterialLaw(src),numericalMaterial(src),
                            _filled(src._filled){};
    virtual ~hoDGMultiscaleMaterialLaw(){};
    virtual matname getType() const{return materialLaw::numeric;};
    virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const;

    virtual void createIPState(const bool isSolve, IPStateBase* &ips,const bool* state =NULL,
                                 const MElement *ele=NULL, const int nbFF_=0, const int gpt=0) const;
    // To allow initialization of bulk ip in case of fracture
    virtual void createIPVariable(IPVariable* &ipv,const MElement *ele=NULL, const int nbFF_=0) const{
      Msg::Warning("hoDGMultiscaleMaterialLaw::createIPVariable is not implemented");
    };
    virtual void calledBy2lawFracture(){}
    virtual void initLaws(const std::map<int,materialLaw*> &maplaw);
    virtual double getRho() const{return _rho;}
    virtual double density() const {return _rho;};
    // As to be defined for explicit scheme. If law for implicit only
    // please define this function with return 0
    virtual double soundSpeed() const {return 0;};
    // If law enable fracture the bool checkfrac allow to detect fracture. This bool is unused otherwise
    // The bool is necessery for computation of matrix by perturbation in this case the check of fracture
    // has not to be performed.
    virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff=true, const bool checkfrac=true);
    virtual bool isHighOrder() const ;
    void fillElasticStiffness(double & rho, STensor43& LF, STensor53& LG, STensor53& JF, STensor63& JG);
    virtual void setElasticStiffness(IPVariable* ipv);
    virtual bool isNumeric() const{return true;}
    #endif
};

#endif // HODGMATERIALLAW_H_
