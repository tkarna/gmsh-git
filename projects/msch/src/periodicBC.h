#ifndef PERIODICBC_H_
#define PERIODICBC_H_

#include "groupOfElements.h"
#include "fullMatrix.h"

class periodicBC{
  public:
    int _dim, _method, _degree;
    int _physical[6];
    fullMatrix<double> _strain;
    std::vector<groupOfElements*> _periodicGroup;

  public:
    periodicBC(){};
    ~periodicBC(){};
    periodicBC(int g1, int g2, int g3, int g4, int g5=0, int g6=0):_strain(3,3),_method(1),_degree(5){
      _strain.setAll(0.0);
      _physical[0]=g1;
      _physical[1]=g2;
      _physical[2]=g3;
      _physical[3]=g4;
      _physical[4]=g5;
      _physical[5]=g6;
      if ((_physical[4]==0)and(_physical[5])==0){
      	_dim=2;
			}
			else {
				_dim = 3;
			};
      for (int i=0; i<6; i++){
        if (_physical[i]!=0){
          groupOfElements* g= new groupOfElements(_dim-1,_physical[i]);
          _periodicGroup.push_back(g);
        };
      };
    };
    periodicBC(const periodicBC& source):_degree(source._degree),_method(source._method), _dim(source._dim),_strain(source._strain),_periodicGroup(source._periodicGroup){
      for (int i=0; i<6; i++){
          _physical[i]= source._physical[i];
      };
    };
};

#endif // PERIODICBC_H_
