
#include "hoDGPartDomain.h"
#include "hoDGIPVariable.h"
#include "hoDGTerms.h"
#include "IFace.h"
#include "GaussLobatto2DQuadratureRules.h"
#include "IEdge.h"
#include "MInterfaceLine.h"
#include "dG3DFunctionSpace.h"
/* has to be moved in dG3D */
hoDGDomain::hoDGDomain(const int tag, const int phys, const int sp, const int lnum, const int fdg,const int dim)
                      :dG3DDomain(tag,phys,sp,lnum,fdg,dim)
{
  // specific space
  if(_space!=NULL) delete _space; // delete the space created in dG3DDomain
  switch(_wsp)
  {
   case functionSpaceType::Lagrange:
    if(_fullDg)
      _space = new dG3DhoDGFunctionSpace(_phys,3);
    else
      _space = new g3DhoDGFunctionSpace(_phys,3);
    break;
   case functionSpaceType::Inter :
    break; // InterfaceDomain set function space later
   default:
    Msg::Error("Function space type is unknown on domain %d",_phys);
  }
  _spaceMinus = _space;
  _spacePlus = _space;

};

hoDGDomain::hoDGDomain(const hoDGDomain &source) :dG3DDomain(source){
};

void hoDGDomain::computeStrain(AllIPState *aips,MElement *e, IPStateBase::whichState ws,
                                        fullVector<double> &disp){
  dG3DDomain::computeStrain(aips,e,ws,disp);
  dG3DMaterialLaw* dglaw = static_cast<dG3DMaterialLaw*>(_mlaw);

  if (dglaw->isHighOrder()){

    bool hessianFlag = true;
    bool thirdFlag = false;
    if (_fullDg)
      thirdFlag = true;

    FunctionSpace<double> *sp = static_cast<FunctionSpace<double>*>(_space);
    IntPt *GP;
    //std::vector<TensorialTraits<double>::HessType> Hesses;
    int npts_bulk=integBulk->getIntPoints(e,&GP);
    AllIPState::ipstateElementContainer *vips = aips->getIPstate(e->getNum());
    int nbFF = e->getNumShapeFunctions();
    for(int j=0;j<npts_bulk;j++){
      // hessien value at gauss points
      //Hesses.clear();
      //sp->hessf(e,GP[j].pt[0],GP[j].pt[1],GP[j].pt[2],Hesses);

      IPStateBase* ips = (*vips)[j];
      hoDGIPVariable *ipv = static_cast<hoDGIPVariable*>(ips->getState(ws));
      hoDGIPVariable *ipvprev = static_cast<hoDGIPVariable*>(ips->getState(IPStateBase::previous));

      std::vector<TensorialTraits<double>::HessType>& Hesses = ipv->hessf(_space,e,GP[j],hessianFlag,thirdFlag);
      //x y z
      STensor33* gradientOfGradientDeformation = &ipv->getRefToGradientOfDeformationGradient();
      gradientOfGradientDeformation->operator=(0.);

      for (int i = 0; i < nbFF; i++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            gradientOfGradientDeformation->operator()(0,k,l) += Hesses[i+0*nbFF](k,l)*disp(i);
            gradientOfGradientDeformation->operator()(1,k,l) += Hesses[i+0*nbFF](k,l)*disp(i+nbFF);
            gradientOfGradientDeformation->operator()(2,k,l) += Hesses[i+0*nbFF](k,l)*disp(i+2*nbFF);
          };
        };
      };
    };
  }
};

void hoDGDomain::computeStrain(AllIPState *aips,MInterfaceElement *ie, IntPt *GP,const IPStateBase::whichState ws,
                                        partDomain* efMinus, partDomain *efPlus, fullVector<double> &dispm,
                                        fullVector<double> &dispp,const bool virt){
  dG3DDomain::computeStrain(aips,ie,GP,ws,efMinus,efPlus,dispm,dispp,virt);
  dG3DMaterialLaw* dgplus = static_cast<dG3DMaterialLaw*>(efPlus->getMaterialLaw());
  dG3DMaterialLaw* dgminus = static_cast<dG3DMaterialLaw*>(efMinus->getMaterialLaw());
  if (dgplus->isHighOrder() or dgminus->isHighOrder()){
    if(!virt){
      bool hessianFlagMinus = true;
      bool hessianFlagPlus = true;
      bool thirdFlagMinus = false;
      bool thirdFlagPlus = false;
      if (_fullDg){
        thirdFlagMinus = true;
        thirdFlagPlus = true;
      }
      //nlsFunctionSpaceXYZ<double> *spaceminus = static_cast<nlsFunctionSpaceXYZ<double> *>(efMinus->getFunctionSpace());
      FunctionSpaceBase* spaceminus = efMinus->getFunctionSpace();
      MElement *ele = dynamic_cast<MElement*>(ie);
      int npts=integBound->getIntPoints(ele,&GP);
      // Value of Gauss points on elements
      IntPt *GPm; IntPt *GPp;
      _interQuad->getIntPoints(ie,GP,&GPm,&GPp);
      // Get values of hess and ThirdDev at all Gauss point
      //std::vector<GaussPointSpaceValues<double>*> vallm;
      //spaceminus->get(ie->getElem(0),npts,GPm,vallm);
      // vector with nodal displacement of interface
      // take from minus displacement vector
      //nlsFunctionSpaceXYZ<double>* spaceplus = static_cast<nlsFunctionSpaceXYZ<double>*>(efPlus->getFunctionSpace());
      FunctionSpaceBase* spaceplus = efPlus->getFunctionSpace();
      MElement *em = ie->getElem(0);
      MElement *ep = ie->getElem(1);
      //std::vector<GaussPointSpaceValues<double>*> vallp;
      //spaceplus->get(ep,npts,GPp,vallp);
       // gauss point
      int nbdofm = spaceminus->getNumKeys(em);
      int nbdofp = spaceplus->getNumKeys(ep);
      int nbFFm = em->getNumVertices();
      int nbFFp = ep->getNumVertices();
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(ie->getNum());
      for(int j=0;j<npts;j++){
        // key of gauss point grad value at gauss point
//        double uem = GPm[j].pt[0]; double vem = GPm[j].pt[1]; double wem = GPm[j].pt[2];
//        double uep = GPp[j].pt[0]; double vep = GPp[j].pt[1]; double wep = GPp[j].pt[2];
//        ie->getuvwOnElem(GP[j].pt[0],GP[j].pt[1],uem,vem,wem,uep,vep,wep);
//        Hessm.clear(); Hessp.clear();
//        spaceminus->hessf(em,uem,vem,wem,Hessm);
//        spaceplus->hessf(ep,uep,vep,wep,Hessp);
        //std::vector<TensorialTraits<double>::HessType> &Hessm = vallm[j]->_vhess;
        //std::vector<TensorialTraits<double>::HessType> &Hessp = vallp[j]->_vhess;



        IPStateBase* ipsm = (*vips)[j];
        IPStateBase* ipsp = (*vips)[j+npts];

        hoDGIPVariable* ipvm = static_cast<hoDGIPVariable*>(ipsm->getState(ws));
        hoDGIPVariable* ipvp = static_cast<hoDGIPVariable*>(ipsp->getState(ws));
        hoDGIPVariable* ipvmprev = static_cast<hoDGIPVariable*>(ipsm->getState(IPStateBase::previous));
        hoDGIPVariable* ipvpprev = static_cast<hoDGIPVariable*>(ipsp->getState(IPStateBase::previous));


        std::vector<TensorialTraits<double>::HessType> &Hessm  = ipvm->hessf(spaceminus,em, GPm[j],hessianFlagMinus,thirdFlagMinus);
        std::vector<TensorialTraits<double>::HessType> &Hessp = ipvp->hessf(spaceplus,ep,GPp[j], hessianFlagPlus,thirdFlagPlus);

        STensor33* gradientOfDeformationGradientm = &ipvm->getRefToGradientOfDeformationGradient();
        gradientOfDeformationGradientm->operator =(0.);

        for (int i = 0; i < nbFFm; i++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              gradientOfDeformationGradientm->operator()(0,k,l) += Hessm[i+0*nbFFm](k,l)*dispm(i);
              gradientOfDeformationGradientm->operator()(1,k,l) += Hessm[i+0*nbFFm](k,l)*dispm(i+nbFFm);
              gradientOfDeformationGradientm->operator()(2,k,l) += Hessm[i+0*nbFFm](k,l)*dispm(i+2*nbFFm);
            };
          };
        };
        //gradientOfDeformationGradientm->print("Negative G");

        STensor33* gradientOfDeformationGradientp = &ipvp->getRefToGradientOfDeformationGradient();
        gradientOfDeformationGradientp->operator=(0.);
        for (int i = 0; i < nbFFp; i++){
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              gradientOfDeformationGradientp->operator()(0,k,l) += Hessp[i+0*nbFFp](k,l)*dispp(i);
              gradientOfDeformationGradientp->operator()(1,k,l) += Hessp[i+0*nbFFp](k,l)*dispp(i+nbFFp);
              gradientOfDeformationGradientp->operator()(2,k,l) += Hessp[i+0*nbFFp](k,l)*dispp(i+2*nbFFp);
            };
          };
        };
        //gradientOfDeformationGradientp->print("positive G");

        if (_fullDg){
//          Thirdm.clear();
//          Thirdp.clear();
//          spaceminus->thirdDevf(em,uem,vem,wem,Thirdm);
//          spaceplus->thirdDevf(ep,uep,vep,wep,Thirdp);
          //std::vector<TensorialTraits<double>::ThirdDevType> &Thirdm = vallm[j]->_vthird;
          //std::vector<TensorialTraits<double>::ThirdDevType> &Thirdp = vallp[j]->_vthird;

          std::vector<TensorialTraits<double>::ThirdDevType> &Thirdm  = ipvm->thirdDevf(spaceminus,em,GPm[j],hessianFlagMinus,thirdFlagMinus);
          std::vector<TensorialTraits<double>::ThirdDevType> &Thirdp = ipvp->thirdDevf(spaceplus,ep,GPp[j],hessianFlagPlus,thirdFlagPlus);

          STensor43* thirdOrderGradientm = & ipvm->getRefToThirdOrderDeformationGradient();
          thirdOrderGradientm->operator*=(0.);
          for (int i=0; i<nbFFm; i++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++)
                for (int m=0; m<3; m++){
                  thirdOrderGradientm->operator()(0,k,l,m) += Thirdm[i+0*nbFFm](k,l,m)*dispm(i);
                  thirdOrderGradientm->operator()(1,k,l,m) += Thirdm[i+0*nbFFm](k,l,m)*dispm(i+1*nbFFm);
                  thirdOrderGradientm->operator()(2,k,l,m) += Thirdm[i+0*nbFFm](k,l,m)*dispm(i+2*nbFFm);
                };
          STensor43* thirdOrderGradientp = & ipvp->getRefToThirdOrderDeformationGradient();
          thirdOrderGradientp->operator*=(0.);
          for (int i=0; i<nbFFp; i++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++)
                for (int m=0; m<3; m++){
                  thirdOrderGradientp->operator()(0,k,l,m) += Thirdp[i+0*nbFFm](k,l,m)*dispp(i);
                  thirdOrderGradientp->operator()(1,k,l,m) += Thirdp[i+0*nbFFm](k,l,m)*dispp(i+1*nbFFp);
                  thirdOrderGradientp->operator()(2,k,l,m) += Thirdp[i+0*nbFFm](k,l,m)*dispp(i+2*nbFFp);
                };
        };

      };
    }
    else{
      // Virtual interface element
      Msg::Error("No virtual interface for 3D");

    };
  }
};


void hoDGDomain::createTerms(unknownField *uf,IPField*ip, std::vector<nonLinearNeumannBC> &vNeumann){
  FunctionSpace<double>* space = static_cast<FunctionSpace<double>*>(_space);
  ltermBulk = new hoDGForceBulk(*space,_mlaw,_fullDg,ip);
  ltermBound = new hoDGForceInter(*space,space,_mlaw,_mlaw,_interQuad,_beta1,_fullDg,ip);
  if(_bmbp)
    btermBulk = new BilinearTermPerturbation<double>(ltermBulk,*space,*space,uf,ip,this,_eps);
  else
   btermBulk = new hoDGStiffnessBulk(*space,_mlaw,_fullDg,ip);

  if(_interByPert)
    btermBound = new BilinearTermPerturbation<double>(ltermBound,*space,*space,uf,ip,this,_eps);
  else
    btermBound = new hoDGStiffnessInter(*space,*space,_mlaw,_mlaw,_interQuad,_beta1,ip,uf,_fullDg,_eps);

  this->btermVirtBound = new BiNonLinearTermVoid();
  this->ltermVirtBound = new nonLinearTermVoid();

  massterm = new mass3D(*space,_mlaw);

  // term of neumannBC
  for(int i=0;i<vNeumann.size();i++){
    nonLinearNeumannBC &neu = vNeumann[i];
    for(int j=0;j<neu._vspace.size();j++){
      if(neu._vdom[j] == this){
        FunctionSpace<double> *spneu = static_cast<FunctionSpace<double>*>(neu._vspace[j]);
        neu._vterm[j] = new g3DLoadTerm<double>(*spneu,neu._f);
      };
    };
  };
};

void hoDGDomain::allocateInterfaceMPI(const int rankOtherPart, const groupOfElements &groupOtherPart,dgPartDomain** dgdom)
{
  // domain creation has it function create a domain with a part of the same domain contained on an other part,
  // the type of domain is known
  if(*dgdom==NULL){ // domain doesn't exist --> creation
    (*dgdom) = new hoDGInterDomain(this->getTag(),this,this,0);
    // set the stability parameters
    hoDGDomain* newInterDom3D = dynamic_cast< hoDGDomain* >(*dgdom);
    newInterDom3D->stabilityParameters(this->_beta1);
  }
}


hoDGInterDomain::hoDGInterDomain(const int tag, partDomain *dom1,partDomain *dom2, const int lnum) :
                                  interDomainBase(dom1,dom2),
                                  hoDGDomain(tag,manageInterface::getKey(dom1->getPhysical(),
                                                                    dom2->getPhysical()),10000,lnum,true,dom1->getDim()){

  // create empty groupOfElements
  g = new groupOfElements();
  // set functionalSpace
  _spaceMinus = interDomainBase::_domMinus->getFunctionSpace();
  _spacePlus = interDomainBase::_domPlus->getFunctionSpace();
  if((_domMinus->getFunctionSpaceType() == functionSpaceType::Lagrange) and
     (_domPlus->getFunctionSpaceType() == functionSpaceType::Lagrange)){
        if(_space != NULL) delete _space;
    _space = new dG3DLagrangeBetween2DomainsFunctionSpace(3,_spaceMinus,_spacePlus);
    _wsp = functionSpaceType::Lagrange;
  }
  else{
    Msg::Error("Impossible to create functional space on hoDGInterDomain %d",_phys);
  };
  if (_domMinus->getTag() != _domPlus->getTag()){
    Msg::Error("Error in creating space betwen two hoDGDomain %d and %d",_domMinus->getTag(),_domPlus->getTag());
  }
};

hoDGInterDomain::hoDGInterDomain(const hoDGInterDomain& src) : hoDGDomain(src),interDomainBase(src){};

void hoDGInterDomain::createTerms(unknownField *uf,IPField*ip, std::vector<nonLinearNeumannBC> &vNeumann){
  // void term (as they have to exist)
  this->ltermBulk = new nonLinearTermVoid();
  this->btermBulk = new BiNonLinearTermVoid();

  this->btermVirtBound = new BiNonLinearTermVoid();
  this->ltermVirtBound = new nonLinearTermVoid();
  this->massterm = new BiNonLinearTermVoid();

  FunctionSpace<double>*  sp = static_cast<FunctionSpace<double>*>(_space);
  FunctionSpace<double>*  sp1 = static_cast<FunctionSpace<double>*>(_spaceMinus);
  FunctionSpace<double>*  sp2 = static_cast<FunctionSpace<double>*>(_spacePlus);

  hoDGDomain* domMinus = static_cast<hoDGDomain*>(_domMinus);
  hoDGDomain* domPlus = static_cast<hoDGDomain*>(_domPlus);

  ltermBound = new hoDGForceInter(*sp1,sp2,_mlawMinus,_mlawPlus,_interQuad,_beta1,_fullDg,ip);

  if(_interByPert)
    btermBound = new BilinearTermPerturbation<double>(ltermBound,*sp1,*sp2,uf,ip,this,_eps);
  else
    btermBound = new hoDGStiffnessInter(*sp1,*sp2,_mlawMinus, _mlawPlus,_interQuad,_beta1,ip,uf,_fullDg,_eps);
};

void hoDGInterDomain::computeAllIPStrain(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, const bool virt){
  IntPt *GP;
  fullVector<double> dispm;
  fullVector<double> dispp;
  std::vector<Dof> R;

  for(groupOfElements::elementContainer::iterator it=gi->begin(); it!=gi->end(); ++it){
    MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(*it);
    R.clear();
    _spaceMinus->getKeys(iele->getElem(0),R);
    dispm.resize(R.size());
    ufield->get(R,dispm);
    R.clear();
    _spacePlus->getKeys(iele->getElem(1),R);
    dispp.resize(R.size());
    ufield->get(R,dispp);
    this->computeStrain(aips,iele,GP,ws,_domMinus,_domPlus,dispm,dispp,virt);
  }
};

void hoDGInterDomain::computeIPVariable(AllIPState *aips,const unknownField *ufield,
                                                const IPStateBase::whichState ws, bool stiff){
  #if defined(HAVE_MPI)
  if (_mlawMinus->getType()==materialLaw::numeric and this->_otherRanks.size()>0){
    this->computeIPVariableMPI(aips,ufield,ws,stiff);
  }
  else
  #endif //HAVE_MPI
  {
    this->computeAllIPStrain(aips,ufield,ws,false);
    IntPt *GP;
    fullVector<double> dispm;
    fullVector<double> dispp;
    std::vector<Dof> R;
    dG3DMaterialLaw *mlawMinus = static_cast<dG3DMaterialLaw*>(_mlawMinus);
    dG3DMaterialLaw *mlawPlus = static_cast<dG3DMaterialLaw*>(_mlawPlus);
    for(groupOfElements::elementContainer::iterator it=gi->begin(); it!=gi->end(); ++it){
      MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(*it);
      int npts_inter= integBound->getIntPoints(*it,&GP);
      R.clear();
      _spaceMinus->getKeys(iele->getElem(0),R);
      dispm.resize(R.size());
      ufield->get(R,dispm);
      R.clear();
      _spacePlus->getKeys(iele->getElem(1),R);
      dispp.resize(R.size());
      ufield->get(R,dispp);
      this->computeIpv(aips,iele,GP,ws,_domMinus,_domPlus,mlawMinus,mlawPlus,dispm,dispp,false,stiff);
    };
  }
};

void hoDGInterDomain::stabilityParameters(const double b1){
  _beta1 = b1;
};

void hoDGInterDomain::matrixByPerturbation(const int iinter, const double eps){
  _interByPert = iinter;
  _eps = eps;
};

void hoDGInterDomain::setMaterialLaw(const std::map<int,materialLaw*> maplaw){
  // oterwise the law is already set
  if(!setmaterial){
    _domMinus->setMaterialLaw(maplaw);
    _domPlus->setMaterialLaw(maplaw);
    _mlaw = NULL;
    _mlawMinus = _domMinus->getMaterialLaw();
    _mlawPlus = _domPlus->getMaterialLaw();
    _mlawMinus->initLaws(maplaw);
    _mlawPlus->initLaws(maplaw);
    setmaterial=true;
    dG3DMaterialLaw* dgminus = dynamic_cast<dG3DMaterialLaw*>(_mlawMinus);
    dG3DMaterialLaw* dgplus = dynamic_cast<dG3DMaterialLaw*>(_mlawPlus);
    if (dgminus->isHighOrder() and dgplus->isHighOrder())
      _isHighOrder = true;
    else
      _isHighOrder = false;
  }
}

void hoDGInterDomain::setGaussIntegrationRule(){
  setGaussIntegrationRuleBase(gi, &integBound, &integBulk, &_interQuad, _gaussorderbound, _gqt);
};

void hoDGInterDomain::initializeTerms(unknownField *uf,IPField*ip, std::vector<nonLinearNeumannBC> &vNeumann){
  createTerms(uf,ip, vNeumann);
};





