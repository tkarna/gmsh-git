
#include "hoDGTerms.h"
#include "STensor33.h"
#include "STensor53.h"
#include "STensor63.h"

void hoDGStiffnessBulk::get(MElement *ele, int npts, IntPt *GP, fullMatrix<double> &mStiff) const{
  dG3DStiffnessBulk::get(ele,npts,GP,mStiff);
  if (this->sym){
    //nlsFunctionSpaceXYZ<double>* sp1 = static_cast<nlsFunctionSpaceXYZ<double>*>(&(this->space1)); // sym
    if (_mlaw->isHighOrder()){
      if (ele->getParent()) ele=ele->getParent();
      int nbFF = ele->getNumShapeFunctions();
      //double jac[3][3];
      _ipf->getIPv(ele,vipv);
      // get values of Grads, Hess and detJ at all Gauss points
      //std::vector<GaussPointSpaceValues<double>*> vall;
      //sp1->get(ele,npts,GP,vall);

      bool hessflag = true;
      bool thirdflag = false;
      if (_fullDg)
        thirdflag = true;
      for (int i = 0; i < npts; i++){
//        const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
        const double weight = GP[i].weight; //const double detJ = ele->getJacobian(u, v, w, jac);

//        const double detJ = vall[i]->_detJ;
//        Grads.clear();
//        space1.gradf(ele,u,v,w,Grads);
//        Hesses.clear();
//        space1.hessf(ele,u,v,w,Hesses);
//        std::vector<TensorialTraits<double>::GradType> &Grads = vall[i]->_vgrads;
 //       std::vector<TensorialTraits<double>::HessType> &Hesses = vall[i]->_vhess;

        const hoDGIPVariable* hoipv = static_cast<const hoDGIPVariable*>(vipv[i]);
        std::vector<TensorialTraits<double>::GradType>& Grads = hoipv->gradf(&space1,ele,GP[i],hessflag,thirdflag);
        std::vector<TensorialTraits<double>::HessType>& Hesses =hoipv->hessf(&space1,ele,GP[i],hessflag,thirdflag);
        double& detJ = hoipv->getJacobianDeterminant(&space1,ele,GP[i],hessflag,thirdflag);

        const STensor63& secondtangent = hoipv->getConstRefToSecondOrderTangent();
        const STensor53& firstsecond = hoipv->getConstRefToCrossTangentFirstSecond();
        const STensor53& secondfirst = hoipv->getConstRefToCrossTangentSecondFirst();

        const double ratio = detJ*weight;

        for(int j=0;j<nbFF; j++){
          for (int p=0; p<nbFF; p++){
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++)
                for (int m=0; m<3; m++)
                  for (int q=0; q<3; q++)
                    for (int r=0; r<3; r++){
                      mStiff(j+k*nbFF,p+l*nbFF) += firstsecond(k,m,l,q,r)*Grads[j+0*nbFF][m]*Hesses[p+0*nbFF](q,r)*ratio+
                                                   secondfirst(k,m,q,l,r)*Hesses[j+0*nbFF](m,q)*Grads[p+0*nbFF](r)*ratio;
                      for (int s=0; s<3; s++){
                        mStiff(j+k*nbFF,p+l*nbFF) += secondtangent(k,m,q,l,r,s)*Hesses[j+0*nbFF](m,q)*Hesses[p+0*nbFF](r,s)*ratio;
                      }
                    }
          };
        };
      };
    }
  }
  else {
    Msg::Info("Non-symetrical stiffness term is not implemented yet");
  };
};

void hoDGForceBulk::get(MElement *ele,int npts,IntPt *GP,fullVector<double> &vFor) const{
  dG3DForceBulk::get(ele,npts,GP,vFor);
  if (_mlaw->isHighOrder()){
    //nlsFunctionSpaceXYZ<double>* sp1 = static_cast<nlsFunctionSpaceXYZ<double>*>(&(this->space1));
    if (ele->getParent()) ele=ele->getParent();
    int nbDof = space1.getNumKeys(ele);
    int nbFF = ele->getNumShapeFunctions();
    double jac[3][3];
    //std::vector<std::vector<TensorialTraits<double>::HessType> >vhess;
    //std::vector<std::vector<TensorialTraits<double>::GradType> >vgradsuvw;
    //sp1->gethessf(ele,npts,GP,vhess);
    //sp1->getgradfuvw(ele,npts,GP,vgradsuvw);
    _ipf->getIPv(ele,vipv,vipvprev);

    bool hessflag = true;
    bool thirdflag = false;
    if (_fullDg)
        thirdflag = true;
    for (int i = 0; i < npts; i++){
      //const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
      const double weight = GP[i].weight; //const double detJ = ele->getJacobian(u, v, w, jac);
 //     const double detJ = ele->getJacobian(vgradsuvw[i],jac);
//      Hesses.clear();
//      space1.hessf(ele,u,v,w,Hesses);
 //     std::vector<TensorialTraits<double>::HessType> &Hesses = vhess[i];
      const hoDGIPVariable* hoipv = static_cast<const hoDGIPVariable*>(vipv[i]);
      std::vector<TensorialTraits<double>::HessType>& Hesses = hoipv->hessf(&space1,ele,GP[i],hessflag,thirdflag);
      double detJ = hoipv->getJacobianDeterminant(&space1,ele,GP[i],hessflag,thirdflag);

      const STensor33& QK = hoipv->getConstRefToSecondOrderStress();
      const double ratio = detJ*weight;

      for(int j=0;j<nbFF; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            for (int m=0; m<3; m++){
              vFor(j+ m*nbFF)+=QK(m,k,l)*Hesses[j+0*nbFF](k,l)*ratio;
            }
          };
        };
      };
    };
  };
};

void hoDGStiffnessInter::get(MElement *ele,int npts,IntPt *GP, fullMatrix<double> &stiff)const{
  dG3DStiffnessInter::get(ele,npts,GP,stiff);
  if (_mlawMinus->isHighOrder() or _mlawPlus->isHighOrder()){

    //printf("assebling the interface stiffness term \n");
    MInterfaceElement *ie = dynamic_cast<MInterfaceElement*>(ele);

    double hs = ie->getCharacteristicSize();

    MElement* eleminus = ie->getElem(0);
    MElement* eleplus = ie->getElem(1);

    int mDofSize = space1.getNumKeys(eleminus);
    int pDofSize = space2.getNumKeys(eleplus);

    int nbFFm = eleminus->getNumShapeFunctions();
    int nbFFp = eleplus->getNumShapeFunctions();

    double jac[3][3];

    bool hessianflag =true;
    bool thirdflag= false;
    if (_fullDg) thirdflag = true;

    _ipf->getIPv(ele,vipv,vipvprev);
    // Get values of Gauss points on minus and plus elements
    IntPt *GPm; IntPt *GPp;
    _interQuad->getIntPoints(ie,GP,&GPm,&GPp);
   // std::vector<std::vector<TensorialTraits<double>::GradType> >vgradsuvw;
   // std::vector<GaussPointSpaceValues<double>*> vallm;
   // std::vector<GaussPointSpaceValues<double>*> vallp;
   // _minusSpace->getgradfuvw(ele,npts,GP,vgradsuvw); // specific function space at interface ??
   // _minusSpace->get(eleminus,npts,GPm,vallm); // not optimal for CG
   // _plusSpace->get(eleplus,npts,GPp,vallp); // not optimal for CG

    for (int i = 0; i < npts; i++){
      const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
      const double weight = GP[i].weight;
      const double detJ = ele->getJacobian(u, v, w, jac);
      //const double detJ = ele->getJacobian(vgradsuvw[i],jac);
      SVector3 normal = vipv[i]->getConstRefToReferenceOutwardNormal();
      normal*= (1./normal.norm());

      const double ratio = detJ*weight;

      std::vector<TensorialTraits<double>::GradType>& GradMinus = vipv[i]->gradf(&space1,eleminus,GPm[i],hessianflag,thirdflag);
      std::vector<TensorialTraits<double>::GradType>&  GradPlus = vipv[i+npts]->gradf(&space2,eleplus,GPp[i],hessianflag,thirdflag);

      std::vector<TensorialTraits<double>::HessType>& HessMinus = vipv[i]->hessf(&space1,eleminus,GPm[i],hessianflag,thirdflag);
      std::vector<TensorialTraits<double>::HessType>& HessPlus = vipv[i+npts]->hessf(&space2,eleplus,GPp[i],hessianflag,thirdflag);


      std::vector<TensorialTraits<double>::ValType>& ValMinus = vipv[i]->f(&space1,eleminus,GPm[i],hessianflag,thirdflag);
      std::vector<TensorialTraits<double>::ValType>& ValPlus = vipv[i+npts]->f(&space2,eleplus,GPp[i],hessianflag,thirdflag);

      std::vector<TensorialTraits<double>::ThirdDevType>& ThirdMius = vipv[i]->thirdDevf(&space1,eleminus,GPm[i],hessianflag,thirdflag);
      std::vector<TensorialTraits<double>::ThirdDevType>& ThirdPlus = vipv[i+npts]->thirdDevf(&space2,eleplus,GPp[i],hessianflag,thirdflag);


//      double uem = GPm[i].pt[0]; double vem = GPm[i].pt[1]; double wem = GPm[i].pt[2];
//      double uep = GPp[i].pt[0]; double vep = GPp[i].pt[1]; double wep = GPp[i].pt[2];
//      ie->getuvwOnElem(u,v,uem,vem,wem,uep,vep,wep);

//      GradPlus.clear();
//      GradMinus.clear();
//      space1.gradf(eleminus,uem,vem,wem,GradMinus);
//      space2.gradf(eleplus,uep, vep, wep, GradPlus);


//      HessMinus.clear();
//      HessPlus.clear();
//      space1.hessf(eleminus,uem,vem,wem,HessMinus);
//      space2.hessf(eleplus,uep,vep,wep,HessPlus);
 //     std::vector<TensorialTraits<double>::GradType> &GradMinus = vallm[i]->_vgrads;
 //     std::vector<TensorialTraits<double>::GradType> &GradPlus = vallp[i]->_vgrads;
 //     std::vector<TensorialTraits<double>::HessType> &HessMinus = vallm[i]->_vhess;
  //    std::vector<TensorialTraits<double>::HessType> &HessPlus = vallp[i]->_vhess;


 //     if (_fullDg){ // Before my comment it computes outside the loop by getting Val and Third even in CG ??
 //       ValMinus.clear(); ValPlus.clear();
 //       space1.f(eleminus,uem,vem,wem,ValMinus);
 //       space2.f(eleplus,uep,vep,wep,ValPlus);

 //       ThirdMius.clear(); ThirdPlus.clear();
 //       space1.thirdDevf(eleminus,uem,vem,wem,ThirdMius);
 //       space2.thirdDevf(eleplus,uep,vep,wep,ThirdPlus);

//      }
      // no computation so OK even if CG
      //std::vector<TensorialTraits<double>::ValType> &ValMinus = vallm[i]->_vvals;
      //std::vector<TensorialTraits<double>::ValType> &ValPlus = vallp[i]->_vvals;
      //std::vector<TensorialTraits<double>::ThirdDevType> &ThirdMius = vallm[i]->_vthird;
      //std::vector<TensorialTraits<double>::ThirdDevType> &ThirdPlus = vallp[i]->_vthird;

      // get all elastic tangent
      const STensor63& elJminus = vipv[i]->getConstRefToSecondOrderElasticTangentModuli();
      const STensor63& elJplus = vipv[i+npts]->getConstRefToSecondOrderElasticTangentModuli();
      const STensor53& elJFminus = vipv[i]->getConstRefToCrossSecondFirstElasticTangentModuli();
      const STensor53& elJFplus = vipv[i+npts]->getConstRefToCrossSecondFirstElasticTangentModuli();
      const STensor53& elLGminus = vipv[i]->getConstRefToCrossFirstSecondElasticTangentModuli();
      const STensor53& elLGplus = vipv[i+npts]->getConstRefToCrossFirstSecondElasticTangentModuli();

      // get all current tangent
      const STensor63& Jminus = vipv[i]->getConstRefToSecondOrderTangent();
      const STensor63& Jplus = vipv[i+npts]->getConstRefToSecondOrderTangent();
      const STensor53& LGminus = vipv[i]->getConstRefToCrossTangentFirstSecond();
      const STensor53& JFminus = vipv[i]->getConstRefToCrossTangentSecondFirst();
      const STensor53& LGplus = vipv[i+npts]->getConstRefToCrossTangentFirstSecond();
      const STensor53& JFplus = vipv[i+npts]->getConstRefToCrossTangentSecondFirst();

      STensor63 elJmeanbetahs(elJminus);
      elJmeanbetahs += elJplus;
      elJmeanbetahs *= 0.5*_beta1/hs;

      for (int k=0; k<3; k++){
        for (int q=0; q<3; q++){
          for (int l=0; l<3; l++){
            for (int m=0; m<3; m++){
              for (int r=0; r<3; r++){
                for (int j=0; j<nbFFm; j++){
                  for (int p=0; p<nbFFm; p++){
                    if (_fullDg){
                      stiff(j+k*nbFFm, p+q*nbFFm) -= 0.5*ValMinus[j+0*nbFFm]*normal[l]*(LGminus(k,l,q,m,r) - JFminus(k,l,r,q,m))*HessMinus[p+0*nbFFm](m,r)*ratio;
                      stiff(j+k*nbFFm, p+q*nbFFm) -= 0.5*ValMinus[p+0*nbFFm]*normal[l]*(elLGminus(q,l,k,m,r) - elJFminus(q,l,r,k,m))*HessMinus[j+0*nbFFm](m,r)*ratio;
                    }
                    stiff(j+k*nbFFm, p+q*nbFFm) -= 0.5*GradMinus[j+0*nbFFm](m)*normal[l]*JFminus(k,m,l,q,r)*GradMinus[p+0*nbFFm](r)*ratio;
                    stiff(j+k*nbFFm, p+q*nbFFm) -= 0.5*GradMinus[p+0*nbFFm](m)*normal[l]*elJFminus(q,m,l,k,r)*GradMinus[j+0*nbFFm](r)*ratio;
                    for (int s=0; s<3; s++){
                      if (_fullDg){
                        stiff(j+k*nbFFm, p+q*nbFFm) += 0.5*ValMinus[j+0*nbFFm]*normal[l]*Jminus(k,l,r,q,m,s)*ThirdMius[p+0*nbFFm](m,s,r)*ratio;
                        stiff(j+k*nbFFm, p+q*nbFFm) += 0.5*ValMinus[p+0*nbFFm]*normal[l]*elJminus(q,l,r,k,m,s)*ThirdMius[j+0*nbFFm](m,s,r)*ratio;
                      }
                      stiff(j+k*nbFFm, p+q*nbFFm) -= 0.5*GradMinus[j+0*nbFFm](m)*normal[l]*Jminus(k,m,l,q,r,s)*HessMinus[p+0*nbFFm](r,s)*ratio;
                      stiff(j+k*nbFFm, p+q*nbFFm) -= 0.5*GradMinus[p+0*nbFFm](m)*normal[l]*elJminus(q,m,l,k,r,s)*HessMinus[j+0*nbFFm](r,s)*ratio;
                      stiff(j+k*nbFFm, p+q*nbFFm) += GradMinus[j+0*nbFFm](m)*normal[l]*elJmeanbetahs(k,m,l,q,r,s)*GradMinus[p+0*nbFFm](r)*normal[s]*ratio;
                    }
                  }
                  for (int p=0; p<nbFFp; p++){
                    if (_fullDg){
                      stiff(j+k*nbFFm, p+q*nbFFp+mDofSize) -= 0.5*ValMinus[j+0*nbFFm]*normal[l]*(LGplus(k,l,q,m,r) - JFplus(k,l,r,q,m))*HessPlus[p+0*nbFFp](m,r)*ratio;
                      stiff(j+k*nbFFm, p+q*nbFFp+mDofSize) += 0.5*ValPlus[p+0*nbFFp]*normal[l]*(elLGminus(q,l,k,m,r) - elJFminus(q,l,r,k,m))*HessMinus[j+0*nbFFm](m,r)*ratio;
                    }
                    stiff(j+k*nbFFm, p+q*nbFFp+mDofSize) -= 0.5*GradMinus[j+0*nbFFm](m)*normal[l]*JFplus(k,m,l,q,r)*GradPlus[p+0*nbFFp](r)*ratio;
                    stiff(j+k*nbFFm, p+q*nbFFp+mDofSize) += 0.5*GradPlus[p+0*nbFFp](m)*normal[l]*elJFminus(q,m,l,k,r)*GradMinus[j+0*nbFFm](r)*ratio;
                    for (int s=0; s<3; s++){
                      if (_fullDg){
                        stiff(j+k*nbFFm, p+q*nbFFp+mDofSize) += 0.5*ValMinus[j+0*nbFFm]*normal[l]*Jplus(k,l,r,q,m,s)*ThirdPlus[p+0*nbFFp](m,s,r)*ratio;
                        stiff(j+k*nbFFm, p+q*nbFFp+mDofSize) -= 0.5*ValPlus[p+0*nbFFp]*normal[l]*elJminus(q,l,r,k,m,s)*ThirdMius[j+0*nbFFm](m,s,r)*ratio;
                      }
                      stiff(j+k*nbFFm, p+q*nbFFp+mDofSize) -= 0.5*GradMinus[j+0*nbFFm](m)*normal[l]*Jplus(k,m,l,q,r,s)*HessPlus[p+0*nbFFp](r,s)*ratio;
                      stiff(j+k*nbFFm, p+q*nbFFp+mDofSize) += 0.5*GradPlus[p+0*nbFFp](m)*normal[l]*elJminus(q,m,l,k,r,s)*HessMinus[j+0*nbFFm](r,s)*ratio;
                      stiff(j+k*nbFFm, p+q*nbFFp+mDofSize) -= GradMinus[j+0*nbFFm](m)*normal[l]*elJmeanbetahs(k,m,l,q,r,s)*GradPlus[p+0*nbFFp](r)*normal[s]*ratio;
                    }
                  }
                }
                for (int j=0; j<nbFFp; j++){
                  for (int p=0; p<nbFFm; p++){
                    if (_fullDg){
                      stiff(j+k*nbFFp+mDofSize, p+q*nbFFm) += 0.5*ValPlus[j+0*nbFFp]*normal[l]*(LGminus(k,l,q,m,r) - JFminus(k,l,r,q,m))*HessMinus[p+0*nbFFm](m,r)*ratio;
                      stiff(j+k*nbFFp+mDofSize, p+q*nbFFm) -= 0.5*ValMinus[p+0*nbFFm]*normal[l]*(elLGplus(q,l,k,m,r) - elJFplus(q,l,r,k,m))*HessPlus[j+0*nbFFp](m,r)*ratio;
                    }
                    stiff(j+k*nbFFp+mDofSize, p+q*nbFFm) += 0.5*GradPlus[j+0*nbFFp](m)*normal[l]*JFminus(k,m,l,q,r)*GradMinus[p+0*nbFFm](r)*ratio;
                    stiff(j+k*nbFFp+mDofSize, p+q*nbFFm) -= 0.5*GradMinus[p+0*nbFFm](m)*normal[l]*elJFplus(q,m,l,k,r)*GradPlus[j+0*nbFFp](r)*ratio;
                    for (int s=0; s<3; s++){
                      if (_fullDg){
                        stiff(j+k*nbFFp+mDofSize, p+q*nbFFm) -= 0.5*ValPlus[j+0*nbFFp]*normal[l]*Jminus(k,l,r,q,m,s)*ThirdMius[p+0*nbFFm](m,s,r)*ratio;
                        stiff(j+k*nbFFp+mDofSize, p+q*nbFFm) += 0.5*ValMinus[p+0*nbFFm]*normal[l]*elJplus(q,l,r,k,m,s)*ThirdPlus[j+0*nbFFp](m,s,r)*ratio;
                      }
                      stiff(j+k*nbFFp+mDofSize, p+q*nbFFm) += 0.5*GradPlus[j+0*nbFFp](m)*normal[l]*Jminus(k,m,l,q,r,s)*HessMinus[p+0*nbFFm](r,s)*ratio;
                      stiff(j+k*nbFFp+mDofSize, p+q*nbFFm) -= 0.5*GradMinus[p+0*nbFFm](m)*normal[l]*elJplus(q,m,l,k,r,s)*HessPlus[j+0*nbFFp](r,s)*ratio;
                      stiff(j+k*nbFFp+mDofSize, p+q*nbFFm) -= GradPlus[j+0*nbFFp](m)*normal[l]*elJmeanbetahs(k,m,l,q,r,s)*GradMinus[p+0*nbFFm](r)*normal[s]*ratio;
                    }
                  }
                  for (int p=0; p<nbFFp; p++){
                    if (_fullDg){
                      stiff(j+k*nbFFp+mDofSize, p+q*nbFFp+mDofSize) += 0.5*ValPlus[j+0*nbFFp]*normal[l]*(LGplus(k,l,q,m,r) - JFplus(k,l,r,q,m))*HessPlus[p+0*nbFFp](m,r)*ratio;
                      stiff(j+k*nbFFp+mDofSize, p+q*nbFFp+mDofSize) += 0.5*ValPlus[p+0*nbFFp]*normal[l]*(elLGplus(q,l,k,m,r) - elJFplus(q,l,r,k,m))*HessPlus[j+0*nbFFp](m,r)*ratio;
                    }
                    stiff(j+k*nbFFp+mDofSize, p+q*nbFFp+mDofSize) += 0.5*GradPlus[j+0*nbFFp](m)*normal[l]*JFplus(k,m,l,q,r)*GradPlus[p+0*nbFFp](r)*ratio;
                    stiff(j+k*nbFFp+mDofSize, p+q*nbFFp+mDofSize) += 0.5*GradPlus[p+0*nbFFp](m)*normal[l]*elJFplus(q,m,l,k,r)*GradPlus[j+0*nbFFp](r)*ratio;
                    for (int s=0; s<3; s++){
                      if (_fullDg){
                        stiff(j+k*nbFFp+mDofSize, p+q*nbFFp+mDofSize) -= 0.5*ValPlus[j+0*nbFFp]*normal[l]*Jplus(k,l,r,q,m,s)*ThirdPlus[p+0*nbFFp](m,s,r)*ratio;
                        stiff(j+k*nbFFp+mDofSize, p+q*nbFFp+mDofSize) -= 0.5*ValPlus[p+0*nbFFp]*normal[l]*elJplus(q,l,r,k,m,s)*ThirdPlus[j+0*nbFFp](m,s,r)*ratio;
                      }
                      stiff(j+k*nbFFp+mDofSize, p+q*nbFFp+mDofSize) += 0.5*GradPlus[j+0*nbFFp](m)*normal[l]*Jplus(k,m,l,q,r,s)*HessPlus[p+0*nbFFp](r,s)*ratio;
                      stiff(j+k*nbFFp+mDofSize, p+q*nbFFp+mDofSize) += 0.5*GradPlus[p+0*nbFFp](m)*normal[l]*elJplus(q,m,l,k,r,s)*HessPlus[j+0*nbFFp](r,s)*ratio;
                      stiff(j+k*nbFFp+mDofSize, p+q*nbFFp+mDofSize) += GradPlus[j+0*nbFFp](m)*normal[l]*elJmeanbetahs(k,m,l,q,r,s)*GradPlus[p+0*nbFFp](r)*normal[s]*ratio;
                    }
                  }
                }
              }
            }
          }
        }
      }
    };
  }
};

void hoDGForceInter::get(MElement *ele,int npts,IntPt *GP,fullVector<double> &vFor) const{
  dG3DForceInter::get(ele,npts,GP,vFor);
  if (_mlawMinus->isHighOrder() and _mlawPlus->isHighOrder()){
    MInterfaceElement *ie = dynamic_cast<MInterfaceElement*>(ele);
    double hs = ie->getCharacteristicSize();
    double betahs = _beta1/hs;

    MElement* eleminus = ie->getElem(0);
    MElement* eleplus = ie->getElem(1);
    // number keys for each element
    int mDofSize = _minusSpace->getNumKeys(eleminus);
    int pDofSize = _plusSpace->getNumKeys(eleplus);
    // number of shape functions
    int nbFFm = eleminus->getNumShapeFunctions();
    int nbFFp = eleplus->getNumShapeFunctions();
    //
    double jac[3][3];

    bool hessianflag = true;
    bool thirdflag = false;
    if (_fullDg)
      thirdflag = true;

    _ipf->getIPv(ele,vipv,vipvprev);
    // Get values of Gauss points on minus and plus elements
    IntPt *GPm; IntPt *GPp;
    _interQuad->getIntPoints(ie,GP,&GPm,&GPp);
    // detJ and all values at Gauss Points
    //std::vector<std::vector<TensorialTraits<double>::GradType> >vgradsuvw;
    //std::vector<GaussPointSpaceValues<double>*> vallm;
    //std::vector<GaussPointSpaceValues<double>*> vallp;
    //_minusSpace->getgradfuvw(ele,npts,GP,vgradsuvw);
    //_minusSpace->get(eleminus,npts,GPm,vallm); // not optimal for CG
    //_plusSpace->get(eleplus,npts,GPp,vallp); // not optimal for CG

    for (int i = 0; i < npts; i++){
      const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
      const double weight = GP[i].weight;
      const double detJ = ele->getJacobian(u, v, w, jac);
      //const double detJ = ele->getJacobian(vgradsuvw[i],jac);
//      double uem = GPm[i].pt[0]; double vem = GPm[i].pt[1]; double wem = GPm[i].pt[2];
//      double uep = GPp[i].pt[0]; double vep = GPp[i].pt[1]; double wep = GPp[i].pt[2];
//      ie->getuvwOnElem(u,v,uem,vem,wem,uep,vep,wep);
      // negative normal
      SVector3 normal(vipv[i]->getConstRefToReferenceOutwardNormal());
      normal*= (1./normal.norm()); // normalize
      // get grads
//      GradPlus.clear(); GradMinus.clear();
//      _minusSpace->gradf(eleminus,uem, vem, wem, GradMinus);
//      _plusSpace->gradf(eleplus,uep, vep, wep, GradPlus);
      // get hessiens
//      HessMinus.clear(); HessPlus.clear();
//      _minusSpace->hessf(eleminus,uem,vem,wem,HessMinus);
//      _plusSpace->hessf(eleplus,uep,vep,wep,HessPlus);
  //    std::vector<TensorialTraits<double>::GradType> &GradMinus = vallm[i]->_vgrads;
  //    std::vector<TensorialTraits<double>::GradType> &GradPlus = vallp[i]->_vgrads;
  //    std::vector<TensorialTraits<double>::HessType> &HessMinus = vallm[i]->_vhess;
  //    std::vector<TensorialTraits<double>::HessType> &HessPlus = vallp[i]->_vhess;

      std::vector<TensorialTraits<double>::GradType>& GradMinus =vipv[i]->gradf(_minusSpace,eleminus,GPm[i],hessianflag,thirdflag);
      std::vector<TensorialTraits<double>::GradType>& GradPlus = vipv[i+npts]->gradf(_plusSpace,eleplus,GPp[i],hessianflag,thirdflag);

      std::vector<TensorialTraits<double>::HessType> &HessMinus = vipv[i]->hessf(_minusSpace,eleminus,GPm[i],hessianflag,thirdflag);
      std::vector<TensorialTraits<double>::HessType> &HessPlus = vipv[i+npts]->hessf(_plusSpace,eleplus,GPp[i],hessianflag,thirdflag);


      const double ratio = detJ*weight;

      // Elastic properties
      const STensor63& Jelminus = vipv[i]->getConstRefToSecondOrderElasticTangentModuli();
      const STensor63& Jelplus= vipv[i+npts]->getConstRefToSecondOrderElasticTangentModuli();
      const STensor53& JFelminus = vipv[i]->getConstRefToCrossSecondFirstElasticTangentModuli();
      const STensor53& JFelplus = vipv[i+npts]->getConstRefToCrossSecondFirstElasticTangentModuli();
      const STensor53& LGelminus = vipv[i]->getConstRefToCrossFirstSecondElasticTangentModuli();
      const STensor53& LGelplus = vipv[i+npts]->getConstRefToCrossFirstSecondElasticTangentModuli();

      if (_fullDg){
        // third order gradient of shape functions
//        ThirdMius.clear(); ThirdPlus.clear();
//        _minusSpace->thirdDevf(eleminus,uem,vem,wem,ThirdMius);
//        _plusSpace->thirdDevf(eleplus,uep,vep,wep,ThirdPlus);
//        ValMinus.clear(); ValPlus.clear();
//        _minusSpace->f(eleminus,uem,vem,wem,ValMinus);
//        _plusSpace->f(eleminus,uep,vep,wep,ValPlus);
   //   std::vector<TensorialTraits<double>::ValType> &ValMinus = vallm[i]->_vvals;
    //  std::vector<TensorialTraits<double>::ValType> &ValPlus = vallp[i]->_vvals;
    //  std::vector<TensorialTraits<double>::ThirdDevType> &ThirdMius = vallm[i]->_vthird;
    //  std::vector<TensorialTraits<double>::ThirdDevType> &ThirdPlus = vallp[i]->_vthird;
        std::vector<TensorialTraits<double>::ValType> &ValMinus =vipv[i]->f(_minusSpace,eleminus,GPm[i],hessianflag,thirdflag);
        std::vector<TensorialTraits<double>::ValType> &ValPlus = vipv[i+npts]->f(_plusSpace,eleplus,GPp[i],hessianflag,thirdflag);

        std::vector<TensorialTraits<double>::ThirdDevType> &ThirdMius = vipv[i]->thirdDevf(_minusSpace,eleminus,GPm[i],hessianflag,thirdflag);
        std::vector<TensorialTraits<double>::ThirdDevType> &ThirdPlus = vipv[i+npts]->thirdDevf(_plusSpace,eleplus,GPp[i],hessianflag,thirdflag);
        // tangent
        const STensor63& Jminus = vipv[i]->getConstRefToSecondOrderTangent();
        const STensor63& Jplus = vipv[i+npts]->getConstRefToSecondOrderTangent();

        const STensor53& JFminus = vipv[i]->getConstRefToCrossTangentSecondFirst();
        const STensor53& JFplus = vipv[i+npts]->getConstRefToCrossTangentSecondFirst();

        const STensor33& Gminus = vipv[i]->getConstRefToGradientOfDeformationGradient();
        const STensor33& Gplus = vipv[i+npts]->getConstRefToGradientOfDeformationGradient();

        const STensor43& Kminus = vipv[i]->getConstRefToThirdOrderDeformationGradient();
        const STensor43& Kplus = vipv[i+npts]->getConstRefToThirdOrderDeformationGradient();

        // compute the mean value of divergence of the second-stress
        STensor3 meanDivSecondStress(0.);
        for (int j=0; j<3; j++)
          for (int k=0; k<3; k++)
            for (int l=0; l<3; l++)
              for (int p=0; p<3; p++)
                for (int q=0; q<3; q++){
                  meanDivSecondStress(j,k) += 0.5*(JFminus(j,k,l,p,q)*Gminus(p,q,l)+
                                                   JFplus(j,k,l,p,q)*Gplus(p,q,l));
                  for (int r=0; r<3; r++){
                    meanDivSecondStress(j,k) += 0.5*(Jminus(j,k,l,p,q,r)*Kminus(p,q,r,l)+
                                                     Jplus(j,k,l,p,q,r)*Kplus(p,q,r,l));
                  }
                }

        const SVector3& jump = vipv[i]->getConstRefToJump();
        for (int k=0; k<3; k++){
          for (int j=0; j<nbFFm; j++){
            //term add from mean of divergence of second stress
            vFor(j + 0*nbFFm) += meanDivSecondStress(0,k)*ValMinus[j+0*nbFFm]*normal[k]*ratio;
            vFor(j + 1*nbFFm) += meanDivSecondStress(1,k)*ValMinus[j+0*nbFFm]*normal[k]*ratio;
            vFor(j + 2*nbFFm) += meanDivSecondStress(2,k)*ValMinus[j+0*nbFFm]*normal[k]*ratio;
            for (int l=0; l<3; l++){
              for (int m=0; m<3; m++){
                for (int n=0; n<3; n++){
                  // term from the variations of PK effective stress
                  vFor(j + 0*nbFFm) += 0.5*jump[l]*(LGelminus(l,k,0,m,n) - JFelminus(l,k,n,0,m))*HessMinus[j+0*nbFFm](m,n)*normal[k]*ratio;
                  vFor(j + 1*nbFFm) += 0.5*jump[l]*(LGelminus(l,k,1,m,n) - JFelminus(l,k,n,1,m))*HessMinus[j+0*nbFFm](m,n)*normal[k]*ratio;
                  vFor(j + 2*nbFFm) += 0.5*jump[l]*(LGelminus(l,k,2,m,n) - JFelminus(l,k,n,2,m))*HessMinus[j+0*nbFFm](m,n)*normal[k]*ratio;

                  for (int s=0; s<3; s++){
                    vFor(j + 0*nbFFm) -= 0.5*jump[l]*Jelminus(l,k,s,0,m,n)*ThirdMius[j+0*nbFFm](m,n,s)*normal[k]*ratio;
                    vFor(j + 1*nbFFm) -= 0.5*jump[l]*Jelminus(l,k,s,1,m,n)*ThirdMius[j+0*nbFFm](m,n,s)*normal[k]*ratio;
                    vFor(j + 2*nbFFm) -= 0.5*jump[l]*Jelminus(l,k,s,2,m,n)*ThirdMius[j+0*nbFFm](m,n,s)*normal[k]*ratio;
                  }
                }
              }
            }
          }
          for (int j=0; j<nbFFp; j++){
            vFor(j + 0*nbFFp+mDofSize) -= meanDivSecondStress(0,k)*ValPlus[j+0*nbFFp]*normal[k]*ratio;
            vFor(j + 1*nbFFp+mDofSize) -= meanDivSecondStress(1,k)*ValPlus[j+0*nbFFp]*normal[k]*ratio;
            vFor(j + 2*nbFFp+mDofSize) -= meanDivSecondStress(2,k)*ValPlus[j+0*nbFFp]*normal[k]*ratio;
            for (int l=0; l<3; l++){
              for (int m=0; m<3; m++){
                for (int n=0; n<3; n++){
                  vFor(j + 0*nbFFp+mDofSize) += 0.5*jump[l]*(LGelplus(l,k,0,m,n)- JFelplus(l,k,n,0,m))*HessPlus[j+0*nbFFp](m,n)*normal[k]*ratio;
                  vFor(j + 1*nbFFp+mDofSize) += 0.5*jump[l]*(LGelplus(l,k,1,m,n)- JFelplus(l,k,n,1,m))*HessPlus[j+0*nbFFp](m,n)*normal[k]*ratio;
                  vFor(j + 2*nbFFp+mDofSize) += 0.5*jump[l]*(LGelplus(l,k,2,m,n)- JFelplus(l,k,n,2,m))*HessPlus[j+0*nbFFp](m,n)*normal[k]*ratio;
                  for (int s=0; s<3; s++){
                    vFor(j + 0*nbFFp+mDofSize) -= 0.5*jump[l]*Jelplus(l,k,s,0,m,n)*ThirdPlus[j+0*nbFFp](m,n,s)*normal[k]*ratio;
                    vFor(j + 1*nbFFp+mDofSize) -= 0.5*jump[l]*Jelplus(l,k,s,1,m,n)*ThirdPlus[j+0*nbFFp](m,n,s)*normal[k]*ratio;
                    vFor(j + 2*nbFFp+mDofSize) -= 0.5*jump[l]*Jelplus(l,k,s,2,m,n)*ThirdPlus[j+0*nbFFp](m,n,s)*normal[k]*ratio;
                  }
                }
              }
            }
          }
        }
      }

      //  deformation gradient
      const STensor3& Fminus = vipv[i]->getConstRefToDeformationGradient();
      const STensor3& Fplus =  vipv[i+npts]->getConstRefToDeformationGradient();
      // high stress
      const STensor33& Qminus = vipv[i]->getConstRefToSecondOrderStress();
      const STensor33& Qplus = vipv[i+npts]->getConstRefToSecondOrderStress();

      STensor33 Qmean(0.);
      STensor33 FjumpN(0.);

      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            Qmean(j,k,l) = 0.5*(Qminus(j,k,l)+Qplus(j,k,l));
            FjumpN(j,k,l) = (Fplus(j,k) - Fminus(j,k))*normal[l];
          }
        }
      };

      STensor33 FjumNJelminus(0), FjumNJelplus(0), FjumNMeanJFel(0);
      STensor3 FjumNJFelminus(0), FjumNJFelplus(0);

      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            for (int p=0; p<3; p++){
              for (int q=0; q<3; q++){
                FjumNJFelminus(p,q) += FjumpN(j,k,l)*JFelminus(j,k,l,p,q);
                FjumNJFelplus(p,q) += FjumpN(j,k,l)*JFelplus(j,k,l,p,q);
                for (int r=0; r<3; r++){
                  double valm = FjumpN(j,k,l)*Jelminus(j,k,l,p,q,r);
                  double valp = FjumpN(j,k,l)*Jelplus(j,k,l,p,q,r);
                  FjumNJelminus(p,q,r) += valm;
                  FjumNJelplus(p,q,r) += valp;
                  FjumNMeanJFel(p,q,r) += betahs*0.5*(valm+valp);
                }
              }
            }
          }
        }
      }
      // for vForce

      for(int j=0;j<nbFFm; j++){
        for (int k=0; k<3; k++){
          vFor(j+ 0*nbFFm) += 0.5* FjumNJFelminus(0,k)*GradMinus[j+0*nbFFm](k)*ratio;
          vFor(j+ 1*nbFFm) += 0.5* FjumNJFelminus(1,k)*GradMinus[j+0*nbFFm](k)*ratio;
          vFor(j+ 2*nbFFm) += 0.5* FjumNJFelminus(2,k)*GradMinus[j+0*nbFFm](k)*ratio;
          for (int m = 0; m<3; m++){
            vFor(j+ 0*nbFFm) +=( 0.5*FjumNJelminus(0,k,m)*HessMinus[j+0*nbFFm](k,m)
                                -(Qmean(0,k,m)+FjumNMeanJFel(0,k,m))*normal[m]*GradMinus[j+0*nbFFm](k))*ratio;
            vFor(j+ 1*nbFFm) +=( 0.5*FjumNJelminus(1,k,m)*HessMinus[j+0*nbFFm](k,m)
                                -(Qmean(1,k,m)+FjumNMeanJFel(1,k,m))*normal[m]*GradMinus[j+0*nbFFm](k))*ratio;
            vFor(j+ 2*nbFFm) +=( 0.5*FjumNJelminus(2,k,m)*HessMinus[j+0*nbFFm](k,m)
                                -(Qmean(2,k,m)+FjumNMeanJFel(2,k,m))*normal[m]*GradMinus[j+0*nbFFm](k))*ratio;
          }
        }
      };

      for(int j=0;j<nbFFp; j++){
        for (int k=0; k<3; k++){
          vFor(j+ 0*nbFFp+mDofSize) += 0.5* FjumNJFelplus(0,k)*GradPlus[j+0*nbFFm](k)*ratio;
          vFor(j+ 1*nbFFp+mDofSize) += 0.5* FjumNJFelplus(1,k)*GradPlus[j+0*nbFFm](k)*ratio;
          vFor(j+ 2*nbFFp+mDofSize) += 0.5* FjumNJFelplus(2,k)*GradPlus[j+0*nbFFm](k)*ratio;
          for (int m = 0; m<3; m++){
            vFor(j+ 0*nbFFp+mDofSize) +=( 0.5*FjumNJelplus(0,k,m)*HessPlus[j+0*nbFFp](k,m)
                                +(Qmean(0,k,m)+FjumNMeanJFel(0,k,m))*normal[m]*GradPlus[j+0*nbFFp](k))*ratio;
            vFor(j+ 1*nbFFp+mDofSize) +=( 0.5*FjumNJelplus(1,k,m)*HessPlus[j+0*nbFFp](k,m)
                                +(Qmean(1,k,m)+FjumNMeanJFel(1,k,m))*normal[m]*GradPlus[j+0*nbFFp](k))*ratio;
            vFor(j+ 2*nbFFp+mDofSize) +=( 0.5*FjumNJelplus(2,k,m)*HessPlus[j+0*nbFFp](k,m)
                                +(Qmean(2,k,m)+FjumNMeanJFel(2,k,m))*normal[m]*GradPlus[j+0*nbFFp](k))*ratio;
          }
        }
      };
    };
  }
};
