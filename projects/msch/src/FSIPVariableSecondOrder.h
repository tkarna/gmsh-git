#ifndef FSIPVARIABLESECONDORDER_H_
#define FSIPVARIABLESECONDORDER_H_

#include "FSIPVariable.h"
#include "STensor33.h"
#include "STensor63.h"

class FSIPVariableSecondOrderBase{
  private:
    STensor33 gradientOfDeformationGradient;
    STensor33 secondOrderStress;
    STensor63 secondOrderTangent;

  public:
    FSIPVariableSecondOrderBase();
    FSIPVariableSecondOrderBase(const FSIPVariableSecondOrderBase& src);
    FSIPVariableSecondOrderBase& operator = (const FSIPVariableSecondOrderBase& src);
    virtual ~FSIPVariableSecondOrderBase(){}

    virtual STensor63& getRefToSecondOrderTangent(){return secondOrderTangent;};
    virtual const STensor63& getConstRefToSecondOrderTangent()const {return secondOrderTangent;};

    virtual STensor33& getRefToGradientOfDeformationGradient(){return gradientOfDeformationGradient;};
    virtual const STensor33& getConstRefToGradientOfDeformationGradient() const{return gradientOfDeformationGradient;};

    virtual STensor33& getRefToSecondOrderStress(){return secondOrderStress;};
    virtual const STensor33& getConstRefToSecondOrderStress() const{return secondOrderStress;};

    virtual double getIPComp(const int i) const;

};

class FSIPVariableSecondOrder : public FSIPVariable, public FSIPVariableSecondOrderBase{
  public:
    FSIPVariableSecondOrder();
    FSIPVariableSecondOrder(const FSIPVariableSecondOrder& src);
    virtual FSIPVariableSecondOrder& operator = (const IPVariable& src);
    virtual ~FSIPVariableSecondOrder();
    virtual double get(const int i) const;
};

class LinearElasticIPVariableSecondOrder : public LinearElasticIPVariable, public FSIPVariableSecondOrderBase{
  public:
    LinearElasticIPVariableSecondOrder();
    LinearElasticIPVariableSecondOrder(const LinearElasticIPVariableSecondOrder& src);
    virtual LinearElasticIPVariableSecondOrder& operator = (const IPVariable& src);
    virtual ~LinearElasticIPVariableSecondOrder();
    virtual double get(const int i) const;
};


class FSJ2LinearIPVariableSecondOrder : public FSJ2LinearIPVariable, public FSIPVariableSecondOrderBase{
  public:
    FSJ2LinearIPVariableSecondOrder(const mlawJ2linear& j2law);
    FSJ2LinearIPVariableSecondOrder(const FSJ2LinearIPVariableSecondOrder& src);
    virtual FSJ2LinearIPVariableSecondOrder& operator = (const IPVariable& src);
    virtual ~FSJ2LinearIPVariableSecondOrder();
    virtual double get(const int i) const;
};



#endif // FSIPVARIABLESECONDORDER_H_
