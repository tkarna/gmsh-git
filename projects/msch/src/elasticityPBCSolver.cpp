#include <string.h>
#include "GmshConfig.h"
#include "elasticitySolver.h"
#include "linearSystemCSR.h"
#include "linearSystemPETSc.h"
#include "linearSystemGMM.h"
#include "linearSystemFull.h"
#include "Numeric.h"
#include "GModel.h"
#include "functionSpace.h"
#include "terms.h"
#include "solverAlgorithms.h"
#include "quadratureRules.h"
#include "solverField.h"
#include "MPoint.h"
#if defined(HAVE_POST)
#include "PView.h"
#include "PViewData.h"
#endif
#include "elasticityPBCSolver.h"
#include "periodicBoundaryCondition.h"

elasticityPBCSolver::elasticityPBCSolver(int tag): elasticitySolver(tag),pEig(0){};
elasticityPBCSolver::elasticityPBCSolver(GModel* model, int tag):elasticitySolver(model,tag),pEig(0){};
elasticityPBCSolver::~elasticityPBCSolver(){};

void elasticityPBCSolver::applyPeriodicBC(dofManager<double>* p){
  for (int iter=0; iter<allPeriodic.size(); iter++){
    periodicConstraintsBase* peri;
    if (wm == elasticityPBCSolver::CEM){
    	printf("Constraint Elimination Method \n");
      peri = new constraintElimination(LagSpace,p);
    }
    else if(wm == elasticityPBCSolver::LIM){
    	printf("Lagrange Interpolation Method \n");
      if (allPeriodic[iter]._dim ==2){
        peri = new polynomialInterpolationByLagrangeFuction(LagSpace,p,allPeriodic[iter]._degree);
      }
      else if (allPeriodic[iter]._dim ==3){
        peri = new polynomialInterpolationByLagrangeFuction3D(LagSpace,p,allPeriodic[iter]._degree);
      };
    }
    else if(wm == elasticityPBCSolver::CSIM){
    	printf("Cubic Spline Interpolation Method \n");
      if (allPeriodic[iter]._dim ==2){
        peri = new polynomialInterpolationByCubicSpline(LagSpace,p,allPeriodic[iter]._degree);
      }
      else if (allPeriodic[iter]._dim ==3){
        peri = new polynomialInterpolationByCubicSpline3D(LagSpace,p,allPeriodic[iter]._degree);
      };
    }
    else{
      Msg::Error("Error in applying periodic BC");
    };
    peri->setPeriodicGroup(allPeriodic[iter]._periodicGroup,allPeriodic[iter]._dim);
    peri->setDeformation(allPeriodic[iter]._strain);
    peri->applyPeriodicCondition();
  };
};

void elasticityPBCSolver::addPeriodicBC(int g1, int g2, int g3, int g4, int g5, int g6){
  periodicBC peri(g1,g2,g3,g4,g5,g6);
  allPeriodic.push_back(peri);
};

void elasticityPBCSolver::setImposeMethod(int method){
  for (int i=0; i<allPeriodic.size(); i++){
    allPeriodic[i]._method = method;
    if (method ==1){
			wm = elasticityPBCSolver::CEM;
		}
		else if (method == 2){
			wm = elasticityPBCSolver::LIM;
		}
		else if (method ==3){
			wm = elasticityPBCSolver::CSIM;
		}
		else{
			Msg::Error("This method is not implemented");
		};
  };
};

void elasticityPBCSolver::setImposeDegree(int degree){
  for (int i=0; i<allPeriodic.size(); i++){
    allPeriodic[i]._degree = degree;
  };
};

void elasticityPBCSolver::setImposeDeformation(int ii, int jj, double val){
  for (int i=0; i<allPeriodic.size(); i++){
    allPeriodic[i]._strain(ii,jj) = val;
  };
};

void elasticityPBCSolver::assemble(linearSystem<double>* lsys){
  if (pAssembler) delete pAssembler;
  pAssembler = new dofManager<double>(lsys);

  // periodic BC
  std::cout<<"Applying periodic BC"<<std::endl;
  this->applyPeriodicBC(pAssembler);

  // we first do all fixations. the behavior of the dofManager is to
  // give priority to fixations : when a dof is fixed, it cannot be
  // numbered afterwards

  // Dirichlet conditions
  for (unsigned int i = 0; i < allDirichlet.size(); i++)
  {
    FilterDofComponent filter(allDirichlet[i]._comp);
    FixNodalDofs(*LagSpace,allDirichlet[i].g->begin(),allDirichlet[i].g->end(),*pAssembler,*allDirichlet[i]._f,filter);
  }
  // LagrangeMultipliers
  for (unsigned int i = 0; i < LagrangeMultiplierFields.size(); ++i)
  {
    NumberDofs(*LagrangeMultiplierSpace, LagrangeMultiplierFields[i].g->begin(), LagrangeMultiplierFields[i].g->end(), *pAssembler);
  }
  // Elastic Fields
  for (unsigned int i = 0; i < elasticFields.size(); ++i)
  {
    if(elasticFields[i]._E != 0.)
      NumberDofs(*LagSpace, elasticFields[i].g->begin(), elasticFields[i].g->end(),*pAssembler);
  }
  // Voids
  for (unsigned int i = 0; i < elasticFields.size(); ++i)
  {
    if(elasticFields[i]._E == 0.)
      FixVoidNodalDofs(*LagSpace, elasticFields[i].g->begin(), elasticFields[i].g->end(), *pAssembler);
  }
  // Neumann conditions
  GaussQuadrature Integ_Boundary(GaussQuadrature::Val);
  if (allNeumann.size() >0)
		std::cout <<  "Neumann BC"<< std::endl;
  for (unsigned int i = 0; i < allNeumann.size(); i++)
  {
    LoadTerm<SVector3> Lterm(*LagSpace,*allNeumann[i]._f);
    Assemble(Lterm,*LagSpace,allNeumann[i].g->begin(),allNeumann[i].g->end(),Integ_Boundary,*pAssembler);
  }
  // Assemble cross term, laplace term and rhs term for LM
  GaussQuadrature Integ_LagrangeMult(GaussQuadrature::ValVal);
  GaussQuadrature Integ_Laplace(GaussQuadrature::GradGrad);
  for (unsigned int i = 0; i < LagrangeMultiplierFields.size(); i++)
  {
    LagrangeMultiplierTerm LagTerm(*LagSpace, *LagrangeMultiplierSpace, LagrangeMultiplierFields[i]._d);
    Assemble(LagTerm, *LagSpace, *LagrangeMultiplierSpace,
             LagrangeMultiplierFields[i].g->begin(), LagrangeMultiplierFields[i].g->end(), Integ_LagrangeMult, *pAssembler);

    LaplaceTerm<double,double> LapTerm(*LagrangeMultiplierSpace, LagrangeMultiplierFields[i]._tau);
    Assemble(LapTerm, *LagrangeMultiplierSpace, LagrangeMultiplierFields[i].g->begin(), LagrangeMultiplierFields[i].g->end(), Integ_Laplace, *pAssembler);

    LoadTerm<double> Lterm(*LagrangeMultiplierSpace, LagrangeMultiplierFields[i]._f);
    Assemble(Lterm, *LagrangeMultiplierSpace, LagrangeMultiplierFields[i].g->begin(), LagrangeMultiplierFields[i].g->end(), Integ_Boundary, *pAssembler);
  }
  // Assemble elastic term for
  GaussQuadrature Integ_Bulk(GaussQuadrature::GradGrad);
  for (unsigned int i = 0; i < elasticFields.size(); i++)
  {
    IsotropicElasticTerm Eterm(*LagSpace,elasticFields[i]._E,elasticFields[i]._nu);
    Assemble(Eterm,*LagSpace,elasticFields[i].g->begin(),elasticFields[i].g->end(),Integ_Bulk,*pAssembler);
  }

  /*for (int i=0;i<pAssembler->sizeOfR();i++){
    for (int j=0;j<pAssembler->sizeOfR();j++){
      double d;lsys->getFromMatrix(i,j,d);
      printf("%12.5E ",d);
    }
    double d;lsys->getFromRightHandSide(i,d);
    printf(" |  %12.5E\n",d);
  }*/

  printf("nDofs=%d\n",pAssembler->sizeOfR());
  printf("nFixed=%d\n",pAssembler->sizeOfF());
};

void elasticityPBCSolver::setSolver(std::string sol){
	if (sol == "Gmm"){
		ws = elasticityPBCSolver::Gmm;
	}
	else if ( sol == "Petsc"){
		ws == elasticityPBCSolver::Petsc;
	}
	else if ( sol == "Taucs"){
		ws == elasticityPBCSolver::Taucs;
	}
	else {
		Msg::Error("This solver is not installed");
	};
};

void elasticityPBCSolver::solve(){
  linearSystem<double> *lsys;
  if (ws==elasticityPBCSolver::Gmm){
    lsys= new linearSystemGmm<double>;
    static_cast<linearSystemGmm<double>*>(lsys)->setNoisy(2);
  }
  else if (ws == elasticityPBCSolver::Taucs){
    #if defined(HAVE_TAUCS)
    lsys = new linearSystemCSRTaucs<double>;
    #else
    Msg::Error("Taucs is not installed\n Gmm is chosen to solve");
    lsys= new linearSystemGmm<double>;
    static_cast<linearSystemGmm<double>*>(lsys)->setNoisy(2);
    #endif
  }
  else if (ws == elasticityPBCSolver::Petsc){
    #if defined(HAVE_PETSC)
    lsys = new linearSystemPETSc<double>;
    #else
    Msg::Error("Petsc is not installed\n Gmm is chosen to solve");
    lsys= new linearSystemGmm<double>;
    static_cast<linearSystemGmm<double>*>(lsys)->setNoisy(2);
    #endif
  };

  this->assemble(lsys);
  lsys->systemSolve();
  printf("-- done solving!\n");
  GaussQuadrature Integ_Bulk(GaussQuadrature::GradGrad);

  double energ=0;
  for (unsigned int i = 0; i < elasticFields.size(); i++)
  {
    SolverField<SVector3> Field(pAssembler, LagSpace);
    IsotropicElasticTerm Eterm(Field,elasticFields[i]._E,elasticFields[i]._nu);
    BilinearTermToScalarTerm Elastic_Energy_Term(Eterm);
    Assemble(Elastic_Energy_Term,elasticFields[i].g->begin(),elasticFields[i].g->end(),Integ_Bulk,energ);
  }
  printf("elastic energy=%f\n",energ);
};

void elasticityPBCSolver::displacementView(std::string filename){
	PView *pv = this->buildDisplacementView("displacement");
  pv->getData()->writeMSH(filename);
  delete pv;
};

void elasticityPBCSolver::stressView(std::string filename){
	PView *pv = this->buildStressesView("stress");
	pv->getData()->writeMSH(filename);
	delete pv;
};

void elasticityPBCSolver::setDeformationTensor(fullMatrix<double> eps){
	for (int i=0; i< allPeriodic.size(); i++){
		allPeriodic[i]._strain = eps;
	};

};

void elasticityPBCSolver::setRVEVolume(double v){
	RVEVolume =v;
};

void getStrainFromDisplacement(dofManager<double> *a, MElement *e,
                       double u, double v, double w,
                       double E, double nu, int _tag, fullMatrix<double> &strain){
  double valx[256];
  double valy[256];
  double valz[256];
  for (int i=0; i<256; i++){
    valx[i]=0;valy[i]=0;valz[i]=0;
  };
  for (int i=0; i<e->getNumVertices();++i){
    MVertex* v=e->getVertex(i);
    a->getDofValue(v,0,_tag,valx[i]);
    a->getDofValue(v,1,_tag,valy[i]);
    a->getDofValue(v,2,_tag,valz[i]);
  };
  double gradux[3], graduy[3], graduz[3];
  e->interpolateGrad(valx, u, v, w, gradux);
  e->interpolateGrad(valy, u, v, w, graduy);
  e->interpolateGrad(valz, u, v, w, graduz);
  double eps[6] = {gradux[0], graduy[1], graduz[2],
                     0.5 * (gradux[1] + graduy[0]),
                     0.5 * (gradux[2] + graduz[0]),
                     0.5 * (graduy[2] + graduz[1])
                    };
  strain.resize(3,3);
  strain(0,0)=eps[0];
  strain(1,1)=eps[1];
  strain(2,2)=eps[2];
  strain(0,1)=strain(1,0)=eps[3];
  strain(0,2)=strain(2,0)=eps[4];
  strain(1,2)=strain(2,1)=eps[5];
};

void getStressFromDisplacement(dofManager<double> *a, MElement *e,
                       double u, double v, double w,
                       double E, double nu, int _tag, fullMatrix<double> &stress){
  double valx[256];
  double valy[256];
  double valz[256];
  for (int i=0; i<256; i++){
    valx[i]=0;valy[i]=0;valz[i]=0;
  };
  for (int i=0; i<e->getNumVertices();++i){
    MVertex* v=e->getVertex(i);
    a->getDofValue(v,0,_tag,valx[i]);
    a->getDofValue(v,1,_tag,valy[i]);
    a->getDofValue(v,2,_tag,valz[i]);
  };
  double gradux[3], graduy[3], graduz[3];
  e->interpolateGrad(valx, u, v, w, gradux);
  e->interpolateGrad(valy, u, v, w, graduy);
  e->interpolateGrad(valz, u, v, w, graduz);
  double eps[6] = {gradux[0], graduy[1], graduz[2],
                     0.5 * (gradux[1] + graduy[0]),
                     0.5 * (gradux[2] + graduz[0]),
                     0.5 * (graduy[2] + graduz[1])
                    };
  double A = E / (1. + nu);
  double B = A * (nu / (1. - 2. * nu));
  double trace = eps[0] + eps[1] + eps[2] ;
  stress.resize(3,3); stress.setAll(0.0);
  stress(0,0) = A * eps[0] + B * trace;
  stress(1,1) = A * eps[1] + B * trace;
  stress(2,2) = A * eps[2] + B * trace;
  stress(0,1)=stress(1,0) = A * eps[3];
  stress(0,2)=stress(2,0) = A * eps[4];
  stress(1,2)=stress(2,1) = A * eps[5];
};

static double vonMises(fullMatrix<double> s)
{
  double stress[9] = {s(0,0), s(0,1), s(0,2),
                      s(1,0), s(1,1), s(1,2),
                      s(2,0), s(2,1), s(2,2)
                     };
    return ComputeVonMises(stress);
}

void elasticityPBCSolver::calculateAverageStress(fullMatrix<double>& _mStress){
  _mStress.resize(3,3);_mStress.setAll(0.0);
  double solidVolume=0.0;

  GaussQuadrature Integ_Bulk(GaussQuadrature::GradGrad);
  for (unsigned int i=0; i<elasticFields.size(); i++){
    for (groupOfElements::elementContainer::iterator it=elasticFields[i].g->begin(); it!=elasticFields[i].g->end(); it++){
      MElement* e=*it;
      IntPt* GP;
      int npts=Integ_Bulk.getIntPoints(e,&GP);
      for (int j=0; j<npts; j++){
        double u=GP[j].pt[0];double v=GP[j].pt[1];double w=GP[j].pt[2];double weight=GP[j].weight;
        double E=elasticFields[i]._E; double nu=elasticFields[i]._nu; int tag=elasticFields[i]._tag;
        fullMatrix<double> sig;
        getStressFromDisplacement(pAssembler,e, u,v,w,E,nu,tag,sig);
        double detJ=e->getJacobianDeterminant(u,v,w);
        sig.scale(weight*detJ);
        _mStress+=sig;
        solidVolume+=weight*detJ;
      };
    };
  };
   _mStress.scale(1.0/RVEVolume);
};

void elasticityPBCSolver::averageStress(){
	fullMatrix<double> sigM;
	this->calculateAverageStress(sigM);
	sigM.print("Average Stress:");
	/*
  std::cout<<"Total RVE volume: V= "<<RVEVolume<<std::endl;
  std::cout<<"Volume of solid part: Vs=  "<<solidVolume<<std::endl;
  std::cout<<"Volume of void part: Vv= "<<RVEVolume-solidVolume<<std::endl;
  std::cout<<"Void fraction: f= "<<(RVEVolume-solidVolume)/RVEVolume<<std::endl;
  sigM.print("- calculated average stress: ");
  std::ofstream myfile;
  //
  myfile.open("voidFraction.txt");
  myfile<<(RVEVolume-solidVolume)/RVEVolume<<std::endl;
  myfile.close();
  */
};

void elasticityPBCSolver::getBoundaryGroupVertex(std::set<MVertex*>& bv){
	bv.clear();
	for (int i =0; i<allPeriodic.size(); i++){
		for (int j = 0; j<allPeriodic[i]._periodicGroup.size(); j++){
			groupOfElements* g = allPeriodic[i]._periodicGroup[j];
			for (groupOfElements::vertexContainer::iterator it =g->vbegin(); it!= g->vend(); it++ ){
				bv.insert(*it);
			};
		};
	};
};

void elasticityPBCSolver::getKeysFromVertex(FunctionSpaceBase* sp, MVertex* v, std::vector<Dof>& vk){
	MPoint pt(v,0,0);
  sp->getKeys(&pt,vk);
};


void elasticityPBCSolver::averageTangentByStaticCondension(){
	std::set<MVertex*> abv;
	this->getBoundaryGroupVertex(abv);
	std::set<Dof> bDof;
	std::set<Dof> iDof;
	// add boundary dof to bDof
	for (std::set<MVertex*>::iterator it = abv.begin(); it!=abv.end(); it++){
		std::vector<Dof> vk;
		this->getKeysFromVertex(LagSpace,*it,vk);
		for (int i=0; i<vk.size(); i++){
			bDof.insert(vk[i]);
		};
	};

	// add internal Dof to iDof
	for (int i=0; i< elasticFields.size(); i++){
		for (groupOfElements::vertexContainer::iterator it = elasticFields[i].g->vbegin(); it!= elasticFields[i].g->vend(); it++){
			std::vector<Dof> vk;
			this->getKeysFromVertex(LagSpace,*it,vk);
			for (int j=0; j<vk.size(); j++){
				iDof.insert(vk[j]);
			};
		};
	};
};

int elasticityPBCSolver::getDeformationTensor(fullMatrix<double>& eps){
	int dim;
	for (int i=0; i< allPeriodic.size(); i++){
		eps = allPeriodic[i]._strain;
		dim = allPeriodic[i]._dim;
	};
	return dim;
};

void elasticityPBCSolver::averageTangentByNumericalPerturbation(double perturb){
	fullMatrix<double> eps;
	int dim = this->getDeformationTensor(eps);
	STensor43 tangent(0.0);
	//
	for ( int i=0; i<dim; i++){
		for (int j=i; j<dim; j++){
			fullMatrix<double> epsplus(eps);
			fullMatrix<double> epsmoins(eps);
			epsplus(i,j) = eps(i,j)+perturb;
			epsplus(j,i) = eps(i,j)+perturb;
			epsmoins(i,j)= eps(i,j)-perturb;
			epsmoins(j,i) = eps(i,j)-perturb;
			fullMatrix<double> sigplus, sigmoins;
			this->setDeformationTensor(epsplus);
			this->solve();
			this->calculateAverageStress(sigplus);
			this->setDeformationTensor(epsmoins);
			this->solve();
			this->calculateAverageStress(sigmoins);
			fullMatrix<double> tang;
			tang.resize(3,3); tang.setAll(0.0);
			for (int ii=0; ii<dim; ii++){
				for (int jj=0; jj<dim; jj++){
					if (ii==jj)
						tangent(ii,jj,i,j) = (sigplus(ii,jj) - sigmoins(ii,jj))/(2.0*perturb);
					else
						tangent(ii,jj,i,j) = (sigplus(ii,jj) - sigmoins(ii,jj))/(4.0*perturb);

					tangent(ii,jj,j,i) = tangent(ii,jj,i,j);
				};
			};
		};
	};
for (int i=0; i<dim; i++){
	for (int j=0; j<dim; j++) {
		for (int k=0; k<dim; k++){
			for (int l= 0; l<dim; l++){
				printf("H( %d %d %d %d) = %f \n",i,j,k,l,tangent(i,j,k,l));
			};
		};
	};
};

};

