
#include "FSTermSecondOrder.h"
#include "FSIPVariableSecondOrder.h"

void FSSecondOrderInternalForceTerm::get(MElement *ele,int npts,IntPt *GP,fullVector<double> &vFor) const{
  FSInternalForceTerm::get(ele,npts,GP,vFor);
  if (this->_mlaw->isHighOrder()){
    int nbDof = space1.getNumKeys(ele);
    int nbFF = ele->getNumShapeFunctions();
    int dim = nbDof/ nbFF;
    _ipf->getIPv(ele,vipv,vipvprev);

    bool hessflag = true;
    bool thirdflag = false;
    for (int i = 0; i < npts; i++){
      const double weight = GP[i].weight;
      const FSIPVariable* fsipv = dynamic_cast<const FSIPVariable*>(vipv[i]);
      std::vector<TensorialTraits<double>::HessType>& Hesses =fsipv->hessf(&space1,ele,GP[i],hessflag,thirdflag);
      double detJ = fsipv->getJacobianDeterminant(&space1,ele,GP[i],hessflag,thirdflag);

      const FSIPVariableSecondOrderBase* hoipv = dynamic_cast<const FSIPVariableSecondOrderBase*>(vipv[i]);


      const STensor33& QK = hoipv->getConstRefToSecondOrderStress();
      const double ratio = detJ*weight;

      for(int j=0;j<nbFF; j++){
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            for (int m=0; m<dim; m++){
              vFor(j+ m*nbFF)+=QK(m,k,l)*Hesses[j+0*nbFF](k,l)*ratio;
            }
          };
        };
      };
    };
  };
};


void FSSecondOrderStiffnessTerm::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &mStiff) const{
  FSStiffnessTerm::get(ele,npts,GP,mStiff);
  if (this->sym){
    if (_mlaw->isHighOrder()){
      int nbDof = space1.getNumKeys(ele);
      int nbFF = ele->getNumShapeFunctions();
      int dim = nbDof/nbFF;
      _ipf->getIPv(ele,vipv);

      bool hessflag = true;
      bool thirdflag = false;

      for (int i = 0; i < npts; i++){
        const double weight = GP[i].weight;
        const FSIPVariable* fsipv = dynamic_cast<const FSIPVariable*>(vipv[i]);
        std::vector<TensorialTraits<double>::HessType>& Hesses =fsipv->hessf(&space1,ele,GP[i],hessflag,thirdflag);
        double& detJ = fsipv->getJacobianDeterminant(&space1,ele,GP[i],hessflag,thirdflag);

        const FSIPVariableSecondOrderBase* hoipv = dynamic_cast<const FSIPVariableSecondOrderBase*>(vipv[i]);
        const STensor63& secondtangent = hoipv->getConstRefToSecondOrderTangent();

        const double ratio = detJ*weight;

        for(int j=0;j<nbFF; j++){
          for (int p=0; p<nbFF; p++){
            for (int k=0; k<dim; k++)
              for (int l=0; l<dim; l++)
                for (int m=0; m<3; m++)
                  for (int q=0; q<3; q++)
                    for (int r=0; r<3; r++)
                      for (int s=0; s<3; s++)
                        mStiff(j+k*nbFF,p+l*nbFF) += secondtangent(k,m,q,l,r,s)*Hesses[j+0*nbFF](m,q)*Hesses[p+0*nbFF](r,s)*ratio;
          };
        };
      };
    }
  }
  else
    Msg::Fatal("only symetrical term is used");
};
