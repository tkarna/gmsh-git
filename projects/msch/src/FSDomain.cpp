//
// C++ Interface: FSDomain
//
// Description: Interface class to used solver. Your term ha to be store in a domain
//
// Author:   (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "FSDomain.h"
#include "FSMaterialLaw.h"
#include "FSFilterDofComponent.h"
#include "quadratureRules.h"
#include "nlTerms.h"
#include "FSTerms.h"
#include "pbcFunctionSpace.h"
#include "FSIPVariableSecondOrder.h"
#include "FSTermSecondOrder.h"

FSDomain::FSDomain(const int tag,const int phys, const int lnum, const int dim):
                        partDomain(tag,phys),_lnum(lnum),_dim(dim),_mlaw(NULL),_gaussInt(-1){
    #if defined(HAVE_MPI)
    _rootRank = 0;
    _otherRanks.clear();
    _domainIP.clear();
    _mapIP.clear();
    _distributedOnRoot = true;
    #endif
		if (_dim ==3)
      _space = new FSFunctionSpace(_tag);
		else if (_dim ==2)
      _space = new FSFunctionSpace(_tag,0,1);
};

FSDomain::FSDomain(const FSDomain& src): partDomain(src){
  _mlaw = src._mlaw;
  _space= src._space;
  _lnum = src._lnum;
  _dim  = src._dim;
  _gaussInt = src._gaussInt;
  #if defined(HAVE_MPI)
  _rootRank = src._rootRank;
  _otherRanks = src._otherRanks;
  _domainIP = src._domainIP;
  _mapIP = src._mapIP;
  _distributedOnRoot = src._distributedOnRoot;
  #endif
}

FSDomain& FSDomain::operator= (const partDomain& source){
  partDomain::operator= (source);
  const FSDomain* src= static_cast<const FSDomain*>(&source);
  _mlaw = src->_mlaw;
  _space= src->_space;
  _lnum = src->_lnum;
  _dim  = src->_dim;
  _gaussInt = src->_gaussInt;
  #if defined(HAVE_MPI)
  _rootRank = src->_rootRank;
  _otherRanks = src->_otherRanks;
  _domainIP = src->_domainIP;
  _mapIP = src->_mapIP;
  #endif
  return *this;
};

FSDomain::~FSDomain(){
  if (g) delete g;
  if (_space) delete _space;
  #if defined(HAVE_MPI)
  _domainIP.clear();
  _mapIP.clear();
  #endif
};

void FSDomain::distributeOnRootRank(const int flg){
  #if defined(HAVE_MPI)
  _distributedOnRoot = flg;
  #endif
};

void FSDomain::gaussIntegration(const int gint){
  _gaussInt = gint;
};

void FSDomain::setMaterialLaw(const std::map<int,materialLaw*> maplaw){
  if(!setmaterial){
    for(std::map<int,materialLaw*>::const_iterator it=maplaw.begin(); it!=maplaw.end(); ++it){
      if(it->first == _lnum){
        _mlaw = it->second;
        _mlaw->initLaws(maplaw);
      };
    };
    setmaterial = true;
  };
};

void FSDomain::initializeTerms(unknownField *uf,IPField*ip, std::vector<nonLinearNeumannBC> &vNeumann){
  FunctionSpace<double>* sp = static_cast<FunctionSpace<double>*>(_space);

  ltermBulk = new FSInternalForceTerm(*sp,_mlaw,ip);

  if (_bmbp)
    btermBulk = new BilinearTermPerturbation<double>(ltermBulk,*sp,*sp,uf,ip,this);
  else
    btermBulk = new FSStiffnessTerm(*sp,_mlaw,ip);

  massterm = new mass3D(*sp,_mlaw);

  for(int i=0; i< vNeumann.size(); i++){
    nonLinearNeumannBC& neu = vNeumann[i];
		for(int j=0;j<neu._vspace.size();j++){
      if(neu._vdom[j] == this)
      {
        FunctionSpace<double> *spneu = static_cast<FunctionSpace<double>*>(neu._vspace[j]);
        neu._vterm[j] = new FSLoadTerm<double>(*spneu,*neu._f);
      }
    }
  };
};
#if defined(HAVE_MPI)
void FSDomain::computeIPVariableMPI(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, bool stiff){
  IntPt *GP;
  MPI_Status status;
  if (Msg::GetCommRank() == _rootRank){
    // compute all IP strain on root rank
    computeAllIPStrain(aips,ufield,ws);
    // send strain to other procs
    for (std::set<int>::iterator it = _otherRanks.begin(); it!= _otherRanks.end(); it++){
      int otherRank = *it;
      std::set<int>& IPonRank = _mapIP[otherRank];
      for (std::set<int>::iterator its = IPonRank.begin(); its!= IPonRank.end(); its++){
        int num = *its;
        int elnum, gnum;
        numericalMaterial::getTwoIntsFromType(num,elnum,gnum);
        AllIPState::ipstateElementContainer *vips = aips->getIPstate(elnum);
        IPStateBase* ips = (*vips)[gnum];
        ipFiniteStrain* ipv = dynamic_cast<ipFiniteStrain*>(ips->getState(ws));
        int bufferSize = ipv->getMacroNumberElementDataSendToMicroProblem(); // already known
        double *buffer = new double[bufferSize];
        ipv->getMacroDataSendToMicroProblem(buffer);
        int tag = numericalMaterial::createTypeWithTwoInts(elnum,gnum);
        MPI_Send(buffer,bufferSize,MPI_DOUBLE,otherRank,tag,MPI_COMM_WORLD);
        delete[] buffer;
      };
    }
  }
  else if (_otherRanks.find(Msg::GetCommRank()) != _otherRanks.end()){
    for (std::set<int>::iterator it = _domainIP.begin(); it != _domainIP.end(); it++){
      int num = *it;
      int elnum, gnum;
      numericalMaterial::getTwoIntsFromType(num,elnum,gnum);
      int tag = numericalMaterial::createTypeWithTwoInts(elnum,gnum);
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(num);
      IPStateBase* ips = (*vips)[0];
      ipFiniteStrain* ipv = dynamic_cast<ipFiniteStrain*>(ips->getState(ws));
      int bufferSize = ipv->getMacroNumberElementDataSendToMicroProblem(); // already known
      double *buffer = new double[bufferSize];
      MPI_Recv(buffer,bufferSize,MPI_DOUBLE,_rootRank,tag,MPI_COMM_WORLD,&status);
      ipv->setReceivedMacroDataToMicroProblem(buffer);
      delete [] buffer;
    };
  };

  FSMaterialLaw *mlaw = static_cast<FSMaterialLaw*>(_mlaw);
  for (std::set<int>::iterator it = _domainIP.begin(); it != _domainIP.end(); it++){
    int num = *it;
    int elnum, gnum;
    numericalMaterial::getTwoIntsFromType(num,elnum,gnum);
    IPStateBase* ips = NULL;
    if (Msg::GetCommRank() == _rootRank){
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(elnum);
      ips = (*vips)[gnum];
    }
    else if (_otherRanks.find(Msg::GetCommRank()) != _otherRanks.end()){
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(num);
      ips = (*vips)[0];
    }

    IPVariable *ipv = ips->getState(ws);
    if (ws == IPStateBase::initial){
      mlaw->initIPVariable(ipv,stiff);
    }
    else{
      IPVariable *ipvprev = ips->getState(IPStateBase::previous);
      mlaw->stress(ipv,ipvprev,stiff);
    }
  };

  if (_otherRanks.find(Msg::GetCommRank()) != _otherRanks.end()){
    for (std::set<int>::iterator it = _domainIP.begin(); it != _domainIP.end(); it++){
      int num = *it;
      int elnum, gnum;
      numericalMaterial::getTwoIntsFromType(num,elnum,gnum);
      int tag = numericalMaterial::createTypeWithTwoInts(elnum,gnum);
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(num);
      IPStateBase* ips = (*vips)[0];
      ipFiniteStrain* ipv = dynamic_cast<ipFiniteStrain*>(ips->getState(ws));
      int bufferSize = ipv->getMicroNumberElementDataSendToMacroProblem(); // already known
      double *buffer = new double[bufferSize];
      ipv->getMicroDataToMacroProblem(buffer);
      MPI_Send(buffer,bufferSize,MPI_DOUBLE,_rootRank,tag,MPI_COMM_WORLD);
      delete [] buffer;
    };
  }
  else if (Msg::GetCommRank() == _rootRank){
    for (std::set<int>::iterator it = _otherRanks.begin(); it!= _otherRanks.end(); it++){
      int otherRank = *it;
      std::set<int>& IPonRank = _mapIP[otherRank];
      for (std::set<int>::iterator its = IPonRank.begin(); its!= IPonRank.end(); its++){
        int num = *its;
        int elnum, gnum;
        numericalMaterial::getTwoIntsFromType(num,elnum,gnum);
        AllIPState::ipstateElementContainer *vips = aips->getIPstate(elnum);
        IPStateBase* ips = (*vips)[gnum];
        ipFiniteStrain* ipv = dynamic_cast<ipFiniteStrain*>(ips->getState(ws));
        int bufferSize = ipv->getMicroNumberElementDataSendToMacroProblem(); // already known
        double *buffer = new double[bufferSize];
        int tag = numericalMaterial::createTypeWithTwoInts(elnum,gnum);
        MPI_Recv(buffer,bufferSize,MPI_DOUBLE,otherRank,tag,MPI_COMM_WORLD,&status);
        ipv->setReceivedMicroDataToMacroProblem(buffer);
        delete[] buffer;
      };
    }
  };
};
#endif // HAVE_MPI


void FSDomain::computeIPVariable(AllIPState *aips,const unknownField *ufield, const IPStateBase::whichState ws, bool stiff){
  #if defined(HAVE_MPI)
  if (_mlaw->getType()==materialLaw::numeric and _otherRanks.size()>0)
    this->computeIPVariableMPI(aips,ufield,ws,stiff);
  else
  #endif
  {
    computeAllIPStrain(aips,ufield,ws);
    fullVector<double> unknowns;
    for (groupOfElements::elementContainer::const_iterator it = g->begin(); it != g->end(); ++it){
      MElement *e = *it;
      this->computeIpv(aips,e,ws,_mlaw,unknowns,stiff);
    };
  }
};


FilterDof* FSDomain::createFilterDof(const int comp) const{
  FilterDof* fdof = new FSFilterDofComponent(comp);
  return fdof;
};

void FSDomain::computeAverageStress(const IPField* ipf, STensor3& sig, double &volume) const{
  sig *= 0;
  volume = 0.;
  IntPt* GP;
  const ipFiniteStrain *vipv[256];
  bool hessflag = false;
  if (_mlaw->isHighOrder()){
    hessflag = true;
  }
  bool thirdflag = false;
  for (groupOfElements::elementContainer::iterator it= g->begin(); it!=g->end(); it++){
    MElement* ele= *it;
    ipf->getIPv(ele,vipv);
    int npts= integBulk->getIntPoints(ele,&GP);
    for (int i=0; i<npts; i++){
      double weight= GP[i].weight;
      double &detJ = vipv[i]->getJacobianDeterminant(_space,ele,GP[i],hessflag,thirdflag);
      const STensor3& P = vipv[i]->getConstRefToFirstPiolaKirchhoffStress();
      sig+=P*detJ*weight;
      volume+=detJ*weight;
    };
  };
  double inv = 1./volume;
  sig*= inv;
};



void FSDomain::computeAverageHighOrderStress(const IPField* ipf, STensor33& sig, double &volume) const{
  sig*= 0.;
  volume = 0;
  IntPt* GP;
  const ipFiniteStrain *vipv[256];
  bool hessflag = false;
  if (_mlaw->isHighOrder()){
    hessflag = true;
  }
  bool thirdflag = false;
  for (groupOfElements::elementContainer::iterator it= g->begin(); it!=g->end(); it++){
    MElement* ele= *it;
    if(ele->getParent()) ele = ele->getParent();
    ipf->getIPv(ele,vipv);
    int npts= integBulk->getIntPoints(ele,&GP);
    for (int i=0; i<npts; i++){
      double weight= GP[i].weight;
      double &detJ = vipv[i]->getJacobianDeterminant(_space,ele,GP[i],hessflag,thirdflag);
      SPoint3 pt;
      std::vector<TensorialTraits<double>::ValType> &Vals = vipv[i]->f(_space,ele,GP[i],hessflag,thirdflag);
      ele->pnt(Vals,pt);
      const STensor3& P = vipv[i]->getConstRefToFirstPiolaKirchhoffStress();
      double ratio = detJ*weight;
      for (int ii=0; ii<3; ++ii){
        for (int jj=0; jj<3; ++jj){
          for (int kk=0; kk<3; ++kk){
            sig(ii,jj,kk) += 0.5*(P(ii,jj)*pt[kk]+P(ii,kk)*pt[jj])*ratio;
          }
        }
      }
      volume+=ratio;
    };
  };
  double inv = 1./volume;
  sig*= inv;
};

double FSDomain::computeVolumeDomain(const IPField* ipf) const{
  double volume = 0;
  IntPt* GP;
  const ipFiniteStrain *vipv[256];
  bool hessflag = false;
  if (_mlaw->isHighOrder()){
    hessflag = true;
  }
  bool thirdflag = false;
  for (groupOfElements::elementContainer::iterator it= g->begin(); it!=g->end(); it++){
    MElement* ele= *it;
    if(ele->getParent()) ele = ele->getParent();
    int npts= integBulk->getIntPoints(ele,&GP);
    ipf->getIPv(ele,vipv);
    for (int i=0; i<npts; i++){
      double weight= GP[i].weight;
      double& detJ = vipv[i]->getJacobianDeterminant(_space,ele,GP[i],hessflag,thirdflag);
      double ratio = detJ*weight;
      volume+=ratio;
    };
  };
  return volume;
};

void FSDomain::computeAllIPStrain(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws){
  IntPt *GP;
  fullVector<double> unknowns;
  std::vector<Dof> R;

  bool hessflag = false;
  if (_mlaw->isHighOrder()){
    hessflag = true;
  }
  bool thirdflag = false;

  for (groupOfElements::elementContainer::const_iterator it = g->begin(); it != g->end(); ++it){
    MElement *e = *it;
    R.clear();
    _space->getKeys(e,R);
    unknowns.resize(R.size());
    ufield->get(R,unknowns);
    AllIPState::ipstateElementContainer *vips = aips->getIPstate(e->getNum());
    int npts_bulk=integBulk->getIntPoints(e,&GP);
    int nbDof = _space->getNumKeys(e);
    int nbFF = e->getNumShapeFunctions();
    int ncomp = nbDof/nbFF;
    for(int i=0; i<npts_bulk; i++){
      IPStateBase* ips = (*vips)[i];
      ipFiniteStrain *ipv = static_cast<ipFiniteStrain*>(ips->getState(ws));
      std::vector<TensorialTraits<double>::GradType> &Grads = ipv->gradf(_space,e,GP[i],hessflag,thirdflag);
      STensor3& F =  ipv->getRefToDeformationGradient();
      STensor3 I(1.0);
      F*=0.;
      F += I;
      for(int k=0;k<nbFF; k++)
        for (int p=0; p<ncomp; p++)
          for (int q=0; q<3; q++)
            F(p,q) += Grads[k+0*nbFF](q)*unknowns(k+p*nbFF);


    };
  };
};

void FSDomain::computeIpv(AllIPState *aips,MElement *e, IPStateBase::whichState ws,materialLaw *mlaw__,
                                              fullVector<double> &unknowns, bool stiff){
  FSMaterialLaw *mlaw = static_cast<FSMaterialLaw*>(mlaw__);
  IntPt *GP;
  int npts_bulk=integBulk->getIntPoints(e,&GP);
  AllIPState::ipstateElementContainer *vips = aips->getIPstate(e->getNum());
  for(int i=0; i<npts_bulk; i++){
    IPStateBase* ips = (*vips)[i];
    IPVariable *ipv = ips->getState(ws);
    if (ws == IPStateBase::initial){
      mlaw->initIPVariable(ipv,stiff);
    }
    else{
      IPVariable *ipvprev = ips->getState(IPStateBase::previous);
      mlaw->stress(ipv,ipvprev,stiff);
    }
  };
};


void FSDomain::setGaussIntegrationRule(){
  if (g->size()>0){
    groupOfElements::elementContainer::iterator it = g->begin();
    MElement *e = *it;
    if(_gaussInt == -1){
      if (e->getTypeForMSH() ==MSH_TET_4){
        integBulk = new GaussQuadrature(2);
      }
      else if(e->getTypeForMSH() == MSH_TET_10){
        integBulk = new GaussQuadrature(2); // under integration
      }
      else if (e->getTypeForMSH()==MSH_TRI_6){
        integBulk = new GaussQuadrature(2);
      }
      else if(e->getTypeForMSH() == MSH_HEX_27){
        integBulk = new GaussQuadrature(4); // full integration
      }
      else if(e->getTypeForMSH() == MSH_HEX_8){
        integBulk = new GaussQuadrature(4); // under integration
      }
      else if (e->getTypeForMSH() == MSH_QUA_9){
        integBulk = new GaussQuadrature(2);
      }
      else if (e->getTypeForMSH() == MSH_QUA_4){
        integBulk = new GaussQuadrature(2);
      }
      else if (e->getTypeForMSH() == MSH_TRI_3){
        integBulk = new GaussQuadrature(2);
      }
      else {
        Msg::Fatal("Miss case in FSDomain::setGaussIntegrationRule");
      }
    }
    else{
      integBulk = new GaussQuadrature(_gaussInt);
    }
    IntPt* GP;
    int npts = integBulk->getIntPoints(e,&GP);
    Msg::Error("On procs %d, Num of Gauss Pts =%d",Msg::GetCommRank(),npts);
  }
  else
    integBulk = new QuadratureVoid();
}

FunctionSpaceBase* FSDomain::getSpaceForBC(const nonLinearBoundaryCondition &boundary, const groupOfElements *groupBC) const{
	FunctionSpaceBase* spacebc;
  if(boundary.getType() == nonLinearBoundaryCondition::DIRICHLET){
    const nonLinearDirichletBC* diri = static_cast<const nonLinearDirichletBC*>(&boundary);
    switch(_wsp){
     case functionSpaceType::Lagrange:
      // spacebc = this->_space;
				if (_dim ==3) spacebc = new FSFunctionSpace(_tag);
				if (_dim ==2) spacebc = new FSFunctionSpace(_tag,0,1);
       break;
     default :
       Msg::Error("Unknown function space type for dirichlet BC %d on domain %d",boundary._tag,_tag);
    }
  }
  else if(boundary.getType() == nonLinearBoundaryCondition::NEUMANN)
  {
    const nonLinearNeumannBC *neu = static_cast<const nonLinearNeumannBC*>(&boundary);
    switch(_wsp){
     case functionSpaceType::Lagrange :
       spacebc = new FSFunctionSpace(_tag,neu->_comp);
       break;
     default :
      Msg::Error("Unknown function space type on neumman BC %d on domain %d",boundary._tag,_tag);
    }
  }
  else if(boundary.getType() == nonLinearBoundaryCondition::INITIAL)
  {
    switch(_wsp){
     case functionSpaceType::Lagrange:
      // spacebc = new FSVectorFunctionSpace(_tag);
			 if (_dim ==3) spacebc = new FSFunctionSpace(_tag);
			 if (_dim ==2) spacebc = new FSFunctionSpace(_tag,0,1);
       break;
     default :
       Msg::Error("Unknown function space type for dirichlet BC %d on domain %d",boundary._tag,_tag);
    }
  }
  return spacebc;
};

QuadratureBase* FSDomain::getQuadratureRulesForNeumannBC(nonLinearNeumannBC &neu) const{
	QuadratureBase *integ;
	integ = new GaussQuadrature(GaussQuadrature::GradGrad);
  return integ;
};

partDomain* FSDomain::clone() const{
  return new FSDomain(this->_tag,this->_phys,this->_lnum,this->_dim);
};

void FSDomain::createIPState(AllIPState::ipstateContainer& map, const bool *state) const{
  #if defined(HAVE_MPI)
  if (_mlaw->getType() == materialLaw::numeric and  _otherRanks.size()>0){
    if (Msg::GetCommRank() == _rootRank){
      // get all IP number
      std::vector<int> allIPNum;
      IntPt* GP;
      for (groupOfElements::elementContainer::const_iterator it = this->g->begin(); it != this->g->end(); ++it){
        MElement *ele = *it;
        int npts_bulk=this->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
        for(int i=0;i<npts_bulk;i++){
          int num = numericalMaterial::createTypeWithTwoInts(ele->getNum(),i);
          allIPNum.push_back(num);
        }
      }
      //distribute for all
      std::vector<int> allRanks(_otherRanks.begin(), _otherRanks.end());
      if (_distributedOnRoot)
        allRanks.push_back(_rootRank);
      numericalMaterial::distributeMacroIP(allIPNum,allRanks,_mapIP);
      // Send data
      for (std::map<int,std::set<int> >::iterator it = _mapIP.begin(); it!= _mapIP.end(); it++){
        int rank = it->first;
        std::set<int>& numElOnRank = _mapIP[rank];

        if (rank == _rootRank){
          _domainIP = numElOnRank;
        }
        else{
          int bufferSize = numElOnRank.size();
          MPI_Send(&bufferSize,1,MPI_INT,rank,numericalMaterial::createTypeWithTwoInts(getPhysical(),0),MPI_COMM_WORLD);
          int* buffer = new int[bufferSize];
          int idex = 0;
          for (std::set<int>::iterator its = numElOnRank.begin(); its != numElOnRank.end(); its++){
            buffer[idex] = *its;
            idex++;
          };
          MPI_Send(buffer,bufferSize,MPI_INT,rank,numericalMaterial::createTypeWithTwoInts(getPhysical(),1),MPI_COMM_WORLD);
          delete[] buffer;
        }
      };
      // create IP State
      //const materialLaw *mlaw = this->getMaterialLaw();
      const materialLaw* mlaw = this->getMaterialLaw();
      for (groupOfElements::elementContainer::const_iterator it = this->g->begin(); it != this->g->end(); ++it){
        MElement *ele = *it;
        int npts_bulk=this->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
        AllIPState::ipstateElementContainer tp(npts_bulk);
        for(int i=0;i<npts_bulk;i++){
          int num = numericalMaterial::createTypeWithTwoInts(ele->getNum(),i);
          if (_domainIP.find(num) != _domainIP.end())
            mlaw->createIPState(tp[i],state,ele,ele->getNumVertices(),i);
          else{
            const numericalMaterial* nummat = dynamic_cast<const numericalMaterial*>(mlaw);
            nummat->createIPState(false,tp[i],state,ele,ele->getNumVertices(),i);
          }
        }
        map.insert(AllIPState::ipstatePairType(ele->getNum(),tp));
      }
    }
    else if (_otherRanks.find(Msg::GetCommRank()) != _otherRanks.end()) {
      // receive data from root proc
      MPI_Status status;
      int bufferSize;
      MPI_Recv(&bufferSize,1,MPI_INT,_rootRank,numericalMaterial::createTypeWithTwoInts(getPhysical(),0),MPI_COMM_WORLD,&status);
      int* buffer = new int[bufferSize];
      MPI_Recv(buffer,bufferSize,MPI_INT,_rootRank,numericalMaterial::createTypeWithTwoInts(getPhysical(),1),MPI_COMM_WORLD,&status);
      _domainIP.clear();
      for (int i=0; i<bufferSize; i++)
        _domainIP.insert(buffer[i]);
      delete[] buffer;
      //
      const materialLaw *mlaw = this->getMaterialLaw();
      for (std::set<int>::iterator it = _domainIP.begin(); it != _domainIP.end(); it++){
        int num = *it;
        int elnum, gnum;
        numericalMaterial::getTwoIntsFromType(num,elnum,gnum);
        MElement* ele = new MPoint(NULL,elnum,Msg::GetCommRank()-1);
        AllIPState::ipstateElementContainer tp(1);
        mlaw->createIPState(tp[0],state,ele,ele->getNumVertices(),gnum);
        delete ele;
        map.insert(AllIPState::ipstatePairType(num,tp));
      };
    }
  }
  else
 #endif
  {
    partDomain::createIPState(map,state);
  }
};


FSDomainSecondOrder::FSDomainSecondOrder(const int tag,const int phys, const int lnum, const int dim) : FSDomain(tag,phys,lnum,dim){
  if (_space) delete _space;
  if (_dim ==3)
    _space = new FSFunctionSpace(_tag,true,false);
  else if (_dim ==2)
    _space = new FSFunctionSpace(_tag,0,1,true,false);
};
FSDomainSecondOrder::FSDomainSecondOrder(const FSDomainSecondOrder& src): FSDomain(src){}
FSDomainSecondOrder& FSDomainSecondOrder::operator=(const FSDomainSecondOrder& src){
  FSDomain::operator=(src);
  return *this;
}


void FSDomainSecondOrder::computeAllIPStrain(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws){
  FSDomain::computeAllIPStrain(aips,ufield,ws);
  if (_mlaw->isHighOrder()){
    bool hessianFlag = true;
    bool thirdFlag = false;

    IntPt *GP;
    fullVector<double> unknowns;
    std::vector<Dof> R;
    for (groupOfElements::elementContainer::const_iterator it = g->begin(); it != g->end(); ++it){
      MElement *e = *it;
      R.clear();
      _space->getKeys(e,R);
      unknowns.resize(R.size());
      ufield->get(R,unknowns);
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(e->getNum());
      int npts_bulk=integBulk->getIntPoints(e,&GP);
      int nbDof = _space->getNumKeys(e);
      int nbFF = e->getNumShapeFunctions();
      int ncomp = nbDof/nbFF;

      for(int j=0;j<npts_bulk;j++){
        IPStateBase* ips = (*vips)[j];

        FSIPVariable* fsipv = dynamic_cast<FSIPVariable*>(ips->getState(ws));
        FSIPVariable *fsipvprev = dynamic_cast<FSIPVariable*>(ips->getState(IPStateBase::previous));

        std::vector<TensorialTraits<double>::HessType>& Hesses = fsipv->hessf(_space,e,GP[j],hessianFlag,thirdFlag);

        FSIPVariableSecondOrderBase *ipv = dynamic_cast<FSIPVariableSecondOrderBase*>(ips->getState(ws));
        FSIPVariableSecondOrderBase *ipvprev = dynamic_cast<FSIPVariableSecondOrderBase*>(ips->getState(IPStateBase::previous));


        //x y z
        STensor33* gradientOfGradientDeformation = &ipv->getRefToGradientOfDeformationGradient();
        gradientOfGradientDeformation->operator=(0.);

        for (int i = 0; i < nbFF; i++){
          for (int s = 0; s< ncomp; s++){
            for (int k=0; k<3; k++){
              for (int l=0; l<3; l++){
                gradientOfGradientDeformation->operator()(s,k,l) += Hesses[i+s*nbFF](k,l)*unknowns(i+s*nbFF);
              };
            };
          }
        };
      };
    }
  }
};

void FSDomainSecondOrder::initializeTerms(unknownField *uf,IPField*ip, std::vector<nonLinearNeumannBC> &vNeumann){
  FunctionSpace<double>* sp = static_cast<FunctionSpace<double>*>(_space);

  ltermBulk = new FSSecondOrderInternalForceTerm(*sp,_mlaw,ip);

  if (_bmbp)
    btermBulk = new BilinearTermPerturbation<double>(ltermBulk,*sp,*sp,uf,ip,this);
  else
    btermBulk = new FSSecondOrderStiffnessTerm(*sp,_mlaw,ip);

  massterm = new mass3D(*sp,_mlaw);

  for(int i=0; i< vNeumann.size(); i++){
    nonLinearNeumannBC& neu = vNeumann[i];
		for(int j=0;j<neu._vspace.size();j++){
      if(neu._vdom[j] == this)
      {
        FunctionSpace<double> *spneu = static_cast<FunctionSpace<double>*>(neu._vspace[j]);
        neu._vterm[j] = new FSLoadTerm<double>(*spneu,*neu._f);
      }
    }
  };
};

partDomain* FSDomainSecondOrder::clone() const{
  return new FSDomainSecondOrder(this->_tag,this->_phys,this->_lnum,this->_dim);
};
