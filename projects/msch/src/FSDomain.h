//
// C++ Interface: FiniteStrainDomain
//
// Description: Interface class to used solver. Your term ha to be store in a domain
//
// Author:   (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef FINITESTRAINDOMAIN_H_
#define FINITESTRAINDOMAIN_H_
#include "partDomain.h"
#include "materialLaw.h"

class FSDomain: public partDomain{
  public:
  protected:
    materialLaw* _mlaw;  // material law
    int _lnum;           // material number
    int _dim;            // space dimension
    FunctionSpaceBase* _space; // function space
    int _gaussInt;

    #if defined(HAVE_MPI)
    bool _distributedOnRoot;
    mutable std::set<int> _domainIP; // integration points lie on this domain
    mutable std::map<int,std::set<int> > _mapIP; // map of integration points for exchange date for MPI interface
    #endif // HAVE_MPI

  public:
    FSDomain(const int tag,const int phys, const int lnum, const int dim);
    virtual void gaussIntegration(const int gint);
    virtual void distributeOnRootRank(const int flg);
    #ifndef SWIG
    FSDomain(const FSDomain& source);
    FSDomain& operator= (const partDomain& source);
    virtual ~FSDomain();
    virtual int getDim() const{return _dim;}
    virtual bool IsInterfaceTerms() const{return false;}
    virtual FunctionSpaceBase* getFunctionSpace() const {return _space;};
    virtual materialLaw* getMaterialLaw(){return _mlaw;};
    virtual const materialLaw* getMaterialLaw() const{return _mlaw;};
    virtual int getLawNum() const{return _lnum;};
    virtual void createInterface(manageInterface &maninter){};
    //Iedge has to be change in IElement
    virtual MElement* createVirtualInterface(IElement *ie) const{};
    virtual MElement* createInterface(IElement* ie1,IElement* ie2) const{};
    virtual void setMaterialLaw(const std::map<int,materialLaw*> maplaw);

    virtual void initializeTerms(unknownField *uf,IPField*ip, std::vector<nonLinearNeumannBC> &vNeumann);

    virtual void computeAllIPStrain(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws);
    #if defined(HAVE_MPI)
    virtual void computeIPVariableMPI(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, bool stiff);
    #endif
    virtual void computeIPVariable(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, bool stiff);

    virtual void computeAverageStress(const IPField* ipf, STensor3& sig, double &volume) const;
    virtual void computeAverageHighOrderStress(const IPField* ipf, STensor33& sig, double &volume) const;
    virtual double computeVolumeDomain(const IPField* ipf) const;
    // Add for BC
    virtual FilterDof* createFilterDof(const int comp) const;
    virtual void computeIpv(AllIPState *aips,MElement *e, IPStateBase::whichState ws,
                         materialLaw *mlaw,fullVector<double> &disp, bool stiff);
    virtual void setGaussIntegrationRule();

    virtual FunctionSpaceBase* getSpaceForBC(const nonLinearBoundaryCondition &boundary, const groupOfElements *groupBC) const;
    virtual QuadratureBase* getQuadratureRulesForNeumannBC(nonLinearNeumannBC &neu) const;
    virtual partDomain* clone() const;
    virtual void createIPState(AllIPState::ipstateContainer& map, const bool *state) const;
    #endif
};

class FSDomainSecondOrder : public FSDomain{
  public:
    FSDomainSecondOrder(const int tag,const int phys, const int lnum, const int dim);
    #ifndef SWIG
    FSDomainSecondOrder(const FSDomainSecondOrder& src);
    FSDomainSecondOrder& operator=(const FSDomainSecondOrder& src);
    virtual ~FSDomainSecondOrder(){};
    virtual void computeAllIPStrain(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws);
    virtual void initializeTerms(unknownField *uf,IPField*ip, std::vector<nonLinearNeumannBC> &vNeumann);
    virtual partDomain* clone() const;
    #endif
};

#endif //FINITESTRAINDOMAIN_H_
