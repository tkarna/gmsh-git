//
// C++ Interface: terms
//
// Description: Elementary matrix terms for finite strain problem
//
//
// Author:  (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "FSTerms.h"

void FSInternalForceTerm::get(MElement *ele,int npts,IntPt *GP,fullVector<double> &vFor) const{
  if (ele->getParent()) ele=ele->getParent();
  int nbDof = space1.getNumKeys(ele);
  int nbFF = ele->getNumShapeFunctions();
  int ncomp = nbDof/nbFF;
  vFor.resize(nbDof);
  vFor.setAll(0.);
  _ipf->getIPv(ele,vipv,vipvprev);

  bool hessflag = false;
  if (_mlaw->isHighOrder()){
    hessflag = true;
  }
  bool thirdflag = false;

  for (int i = 0; i < npts; i++){
    const double weight = GP[i].weight;
    std::vector<TensorialTraits<double>::GradType>&  Grads  = vipv[i]->gradf(&space1,ele,GP[i],hessflag,thirdflag);
    double &detJ = vipv[i]->getJacobianDeterminant(&space1,ele,GP[i],hessflag,thirdflag);
    const double ratio = detJ*weight;
    const STensor3& P = vipv[i]->getConstRefToFirstPiolaKirchhoffStress();
    for (int j=0; j<nbFF; j++)
      for (int k=0; k<ncomp; k++)
        for (int r= 0; r<3; r++)
          vFor(j+k*nbFF)+= P(k,r)*Grads[j](r)*ratio;
  };
};

void FSStiffnessTerm::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &mStiff) const{
  if (sym){
    if (ele->getParent()) ele=ele->getParent();
    int nbDof = space1.getNumKeys(ele);
    int nbFF = ele->getNumShapeFunctions();
    int ncomp = nbDof/nbFF;
    mStiff.resize(nbDof, nbDof);
    mStiff.setAll(0.);
    _ipf->getIPv(ele,vipv,vipvprev);
    bool hessflag = false;
    if (_mlaw->isHighOrder()){
      hessflag = true;
    }
    bool thirdflag = false;
    for (int i = 0; i < npts; i++){
      const double weight = GP[i].weight;
      std::vector<TensorialTraits<double>::GradType>& Grads = vipv[i]->gradf(&space1,ele,GP[i],hessflag,thirdflag);
      double& detJ = vipv[i]->getJacobianDeterminant(&space1,ele,GP[i],hessflag,thirdflag);

      const double ratio = detJ*weight;
      const STensor43& L = vipv[i]->getConstRefToTangentModuli();
      for(int j=0;j<nbFF; j++){
        for (int p=0; p<nbFF; p++){
          for (int k=0; k<ncomp; k++){
            for (int r=0; r<ncomp; r++){
              for (int kk=0; kk<3; kk++){
                for (int rr=0; rr<3; rr++){
                  mStiff(j+k*nbFF,p+r*nbFF) += ratio*Grads[j+0*nbFF][kk]*L(k,kk,r,rr)*Grads[p+0*nbFF][rr];
                }
              }
            }
          }
        }
      };
    };
  }
  else {
    Msg::Info("Non-symetrical stiffness term is not implemented yet");
  };

};



