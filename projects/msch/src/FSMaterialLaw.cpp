//
// C++ Interface: matrialLaw
//
// Description: Class with definition of materialLaw for finite strain problem
//
//
// Author:   (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "FSMaterialLaw.h"
#include "ipstate.h"
#include "FSIPVariable.h"
#include "terms.h"
#include "FSDomain.h"

double FSMaterialLaw::_sameStateTolerance = 1.e-100;

FSMaterialLaw::FSMaterialLaw(const int num, const double r, const bool init) :_rho(r), materialLaw(num,init){};
FSMaterialLaw::FSMaterialLaw(const FSMaterialLaw& src): materialLaw(src),_rho(src._rho){};
FSMaterialLaw& FSMaterialLaw::operator = (const materialLaw& src){
  materialLaw::operator=(src);
  const FSMaterialLaw* psrc = dynamic_cast<const FSMaterialLaw*>(&src);
  if (psrc != NULL){
    _rho  = psrc->_rho;
  }
  return *this;
};
FSMaterialLaw::~FSMaterialLaw(){};


FSElasticMaterialLaw::FSElasticMaterialLaw(const int num, const double rho, const bool init)
												:FSMaterialLaw(num,rho,init),_type(elasticLawBase::LINEAR_ELASTIC),_elasticLaw(NULL){};

FSElasticMaterialLaw::FSElasticMaterialLaw(const int num, const int type, const double rho,const bool init)
												:FSMaterialLaw(num,rho,init),_elasticLaw(NULL){
	 _type = (elasticLawBase::ELASTIC_TYPE)type;
  if (_type == elasticLawBase::LINEAR_ELASTIC){
    _elasticLaw = new linearElasticLaw();
		Msg::Info("Linear elastic material is used");
	}
  else if (_type == elasticLawBase::NEO_HOOKEAN){
    _elasticLaw = new NeoHookeanElasticLaw();
		Msg::Info("Neo Hookean elastic material is used");
	}
  else if (_type == elasticLawBase::BI_LOGARITHMIC){
    _elasticLaw = new biLogarithmicElasticLaw();
		Msg::Info("Bilogarithmic elastic material is used");
	}
	else
		Msg::Fatal("Elastic material law does not exist");

};


FSElasticMaterialLaw::FSElasticMaterialLaw(int num, const int type, const double k, const double mu,
                         const double rho, const double tol,const bool init):
                           FSMaterialLaw(num,rho,init),_elasticLaw(NULL){
  _type = (elasticLawBase::ELASTIC_TYPE)type;
  if (_type == elasticLawBase::LINEAR_ELASTIC){
    _elasticLaw = new linearElasticLaw(k,mu);
		Msg::Info("Linear elastic material is used");
	}
  else if (_type == elasticLawBase::NEO_HOOKEAN){
    _elasticLaw = new NeoHookeanElasticLaw(k,mu,tol);
		Msg::Info("Neo Hookean elastic material is used");
	}
  else if (_type == elasticLawBase::BI_LOGARITHMIC){
    _elasticLaw = new biLogarithmicElasticLaw(k,mu);
		Msg::Info("Bilogarithmic elastic material is used");
	}
	else
		Msg::Fatal("Elastic material law does not exist");
};

FSElasticMaterialLaw::FSElasticMaterialLaw(const FSElasticMaterialLaw& src):FSMaterialLaw(src),
              _type(src._type){
  if (src._elasticLaw) {
    _elasticLaw = dynamic_cast<elasticLaw*>(src._elasticLaw->clone());
  }
  else {
    _elasticLaw = NULL;
  }
};

FSElasticMaterialLaw& FSElasticMaterialLaw::operator = (const materialLaw& src){
  FSMaterialLaw::operator=(src);
  const FSElasticMaterialLaw* psrc = dynamic_cast<const FSElasticMaterialLaw*>(&src);
  if (psrc != NULL){
    _type = psrc->_type;
    if (psrc->_elasticLaw == NULL){
      if (_elasticLaw != NULL) delete _elasticLaw;
      _elasticLaw = NULL;
    }
    else{
      if (_elasticLaw == NULL){
        _elasticLaw = dynamic_cast<elasticLaw*>(psrc->_elasticLaw->clone());
      }
      else{
        _elasticLaw->operator=(*psrc->_elasticLaw);
      }
    }
  }
  return *this;
};


FSElasticMaterialLaw::~FSElasticMaterialLaw(){
	if (_elasticLaw) delete _elasticLaw;
	_elasticLaw = NULL;
}

void FSElasticMaterialLaw::setType(const int  type){
	_type = (elasticLawBase::ELASTIC_TYPE)type;

	if (_elasticLaw and _elasticLaw->getType()!= _type){
		delete _elasticLaw;
		_elasticLaw == NULL;
	}

	if (_elasticLaw ==NULL){
		 if (_type == elasticLawBase::LINEAR_ELASTIC){
    	_elasticLaw = new linearElasticLaw();
			Msg::Info("Linear elastic material is used");
		}
  	else if (_type == elasticLawBase::NEO_HOOKEAN){
    	_elasticLaw = new NeoHookeanElasticLaw();
			Msg::Info("Neo Hookean elastic material is used");
		}
  	else if (_type == elasticLawBase::BI_LOGARITHMIC){
    	_elasticLaw = new biLogarithmicElasticLaw();
			Msg::Info("Bilogarithmic elastic material is used");
		}
		else
			Msg::Fatal("Elastic material law does not exist");
	}
};


void FSElasticMaterialLaw::setParameter(const std::string key, const double val){
	if (_elasticLaw==NULL) this->setType(_type);
	_elasticLaw->setParameter(key,val);
};

void FSElasticMaterialLaw::createIPState(IPStateBase* &ips,const bool* state,const MElement *ele,
                                         const int nbFF, const int gpt) const {
  if (_type == elasticLawBase::LINEAR_ELASTIC){
    IPVariable* ipvi = new LinearElasticIPVariable();
    IPVariable* ipv1 = new LinearElasticIPVariable();
    IPVariable* ipv2 = new LinearElasticIPVariable();
    if(ips != NULL) delete ips;
    ips = new IP3State(state,ipvi,ipv1,ipv2);
  }
  else{
    IPVariable* ipvi = new FSIPVariable();
    IPVariable* ipv1 = new FSIPVariable();
    IPVariable* ipv2 = new FSIPVariable();
    if(ips != NULL) delete ips;
    ips = new IP3State(state,ipvi,ipv1,ipv2);
  }
};

void FSElasticMaterialLaw::initIPVariable(IPVariable* ipv, bool stiff){
  ipFiniteStrain* fsipv = dynamic_cast<ipFiniteStrain*>(ipv);
	const STensor3& F = fsipv->getConstRefToDeformationGradient();
	STensor3& P = fsipv->getRefToFirstPiolaKirchhoffStress();
	STensor43& L = fsipv->getRefToTangentModuli();
	_elasticLaw->constitutive(F,P,L,stiff);
};

void FSElasticMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvprev, bool stiff){
	ipFiniteStrain* fsipv = dynamic_cast<ipFiniteStrain*>(ipv);
	const ipFiniteStrain* fsipvprev = dynamic_cast<const ipFiniteStrain*>(ipvprev);

	const STensor3& F1 = fsipv->getConstRefToDeformationGradient();
	const STensor3& F0 = fsipvprev->getConstRefToDeformationGradient();

	STensor3 dF(F1);
	dF -= F0;
	if (dF.norm0() >FSMaterialLaw::getSameStateTolerance()){
    STensor3& P = fsipv->getRefToFirstPiolaKirchhoffStress();
    STensor43& L = fsipv->getRefToTangentModuli();
    _elasticLaw->constitutive(F1,P,L,stiff);
    double J = F1.determinant();
    if (J<1e-16)
      Msg::Error("Negative jacobian FS");
	}
	else{
    ipv->operator=(*ipvprev);
	}
};

double FSElasticMaterialLaw::soundSpeed() const {
  double _K = _elasticLaw->getParameter("BULK_MODULUS");
  return std::sqrt(_K/_rho);
};
double FSElasticMaterialLaw::scaleFactor() const {
  return _elasticLaw->getParameter("SHEAR_MODULUS");
};

FSJ2LinearMaterialLaw::FSJ2LinearMaterialLaw(const int num, const double E, const double nu, const double sy0, const double h,
																						 const double rho, const double tol,
																						 const bool flag, const double eps, const bool init):
													FSMaterialLaw(num,rho,init),_E(E),_nu(nu),_j2law(num,E,nu,rho,sy0,h,tol,flag,eps){};
FSJ2LinearMaterialLaw::FSJ2LinearMaterialLaw(const FSJ2LinearMaterialLaw& src): FSMaterialLaw(src),_j2law(src._j2law),_E(src._E), _nu(src._nu){};

FSJ2LinearMaterialLaw& FSJ2LinearMaterialLaw::operator = (const materialLaw& src){
  FSMaterialLaw::operator=(src);
  const FSJ2LinearMaterialLaw* psrc = dynamic_cast<const FSJ2LinearMaterialLaw*>(&src);
  if (psrc != NULL){
    _E = psrc->_E;
    _nu = psrc->_nu;
    _j2law.operator=(psrc->_j2law);
  }
  return *this;
};

FSJ2LinearMaterialLaw::~FSJ2LinearMaterialLaw(){};

void FSJ2LinearMaterialLaw::createIPState(IPStateBase* &ips,const bool* state_,const MElement *ele, const int nbFF_, const int gpt) const{
	IPVariable* ipvi = new  FSJ2LinearIPVariable(_j2law);
  IPVariable* ipv1 = new  FSJ2LinearIPVariable(_j2law);
  IPVariable* ipv2 = new  FSJ2LinearIPVariable(_j2law);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
};

void FSJ2LinearMaterialLaw::initIPVariable(IPVariable* ipv, bool stiff){
  FSJ2LinearIPVariable* ipvc = static_cast<FSJ2LinearIPVariable*>(ipv);
  IPJ2linear* q1 = ipvc->getIPJ2linear();
  const IPJ2linear q0(*q1);
  const STensor3 F0(ipvc->getConstRefToDeformationGradient());
  _j2law.constitutive(F0,ipvc->getConstRefToDeformationGradient(),ipvc->getRefToFirstPiolaKirchhoffStress(),
                      &q0,q1,ipvc->getRefToTangentModuli(), stiff);
};

void FSJ2LinearMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvprev, bool stiff ){
	FSJ2LinearIPVariable* ipvc = static_cast<FSJ2LinearIPVariable*>(ipv);
	const FSJ2LinearIPVariable* ipvp = static_cast<const FSJ2LinearIPVariable*>(ipvprev);

	const STensor3& F1 = ipvc->getConstRefToDeformationGradient();
	const STensor3& F0 = ipvp->getConstRefToDeformationGradient();

	STensor3 dF(F1);
	dF -= F0;
	if (dF.norm0()>FSMaterialLaw::getSameStateTolerance()){
    IPJ2linear* q1 = ipvc->getIPJ2linear();
    const IPJ2linear* q0 = ipvp->getIPJ2linear();
    _j2law.constitutive(ipvp->getConstRefToDeformationGradient(),ipvc->getConstRefToDeformationGradient(),
                       ipvc->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvc->getRefToTangentModuli(), stiff);
	}
	else{
    ipvc->operator=(*ipvprev);
	}
};

FSMultiscaleMaterialLaw::FSMultiscaleMaterialLaw(const int lnum, const int tag, const bool init)
                          :FSMaterialLaw(lnum,0,init),numericalMaterial(tag){};

void FSMultiscaleMaterialLaw::createIPState(IPStateBase* &ips,const bool* state_,const MElement *ele, const int nbFF, const int gpt) const{
	int el = ele->getNum();
	nonLinearMechSolver* solver = this->createMicroSolver(el,gpt);

	IPVariable* ipvi = new FSMultiscaleIPVariable(solver);
	IPVariable* ipv1 = new FSMultiscaleIPVariable(solver);
	IPVariable* ipv2 = new FSMultiscaleIPVariable(solver);
	if(ips != NULL) delete ips;
	ips = new IP3StateMultiscale(state_,ipvi,ipv1,ipv2,solver);
};

void FSMultiscaleMaterialLaw::createIPState(const bool isSolve,IPStateBase* &ips,const bool* state_,
                                 const MElement *ele, const int nbFF_, const int gpt) const{
  nonLinearMechSolver* solver = NULL;
  if (isSolve){
    int el = ele->getNum();
    solver = this->createMicroSolver(el,gpt);
  }

  IPVariable* ipvi = new FSMultiscaleIPVariable(solver);
  IPVariable* ipv1 = new FSMultiscaleIPVariable(solver);
  IPVariable* ipv2 = new FSMultiscaleIPVariable(solver);
  if(ips != NULL) delete ips;
  ips = new IP3StateMultiscale(state_,ipvi,ipv1,ipv2,solver);
};

void FSMultiscaleMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvprev, bool stiff){
  FSMultiscaleIPVariable* mipv = dynamic_cast<FSMultiscaleIPVariable*>(ipv);
  const FSMultiscaleIPVariable* mipvprev = dynamic_cast<const FSMultiscaleIPVariable*>(ipvprev);

  nonLinearMechSolver* solver = mipv->getSolver();
  const STensor3& F = mipv->getConstRefToDeformationGradient();

  solver->setDeformationGradient(F);
  solver->tangentAveragingFlag(stiff);
  double time =solver->microSolve();

  STensor3& P = mipv->getRefToFirstPiolaKirchhoffStress();
  solver->getStress(P);
  if (stiff){
    STensor43& L = mipv->getRefToTangentModuli();
    solver->getTangent(L);
  }
};


AnisotropicFSMaterialLaw::AnisotropicFSMaterialLaw(
                                   const int num,const double rho,
				   const double Ex,const double Ey,const double Ez,
				   const double Vxy,const double Vxz,const double Vyz,
				   const double MUxy,const double MUxz,const double MUyz,
                                   const double alpha, const double beta, const double gamma) :
                                     	FSMaterialLaw(num,rho,true),
                                     	_tilaw(num,rho,Ex,Ey,Ez,Vxy,Vxz,Vyz,
                                            MUxy,MUxz,MUyz,alpha,beta,gamma),
				     	_ISTensor3(1.)
{
  double nu = _tilaw.poissonRatio();
  double mu = _tilaw.shearModulus();
  double E = 2.*mu*(1.+nu); //Where does this come from? To Have the E corresponding to the Vmax, MUmax
		            //In the linear & homogeneous & isotropic case ?
}

AnisotropicFSMaterialLaw::AnisotropicFSMaterialLaw(const AnisotropicFSMaterialLaw &source) :
								     FSMaterialLaw(source),
								     _tilaw(source._tilaw),
                                                                     _ElasticityTensor(source._ElasticityTensor),
                                                                     _ISTensor3(source._ISTensor3)
{

}

void AnisotropicFSMaterialLaw::createIPState(IPStateBase* &ips,const bool* state_,const MElement *ele, const int nbFF_, const int gpt) const
{
  IPVariable* ipvi = new  AnisotropicFSIPVariable();
  IPVariable* ipv1 = new  AnisotropicFSIPVariable();
  IPVariable* ipv2 = new  AnisotropicFSIPVariable();
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void AnisotropicFSMaterialLaw::createIPVariable(IPVariable* &ipv,const MElement *ele, const int nbFF) const
{
  if(ipv !=NULL) delete ipv;
  ipv = new  AnisotropicFSIPVariable();
}


void AnisotropicFSMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff)
{
  /* get ipvariable */
  AnisotropicFSIPVariable* ipvcur;
  const AnisotropicFSIPVariable* ipvprev;
  ipvcur = static_cast<AnisotropicFSIPVariable*>(ipv);
  ipvprev = static_cast<const AnisotropicFSIPVariable*>(ipvp);

  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPAnisotropic* q1 = ipvcur->getIPAnisotropic();
  const IPAnisotropic* q0 = ipvprev->getIPAnisotropic();

  /* compute stress */
  _tilaw.constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),stiff);

}


void AnisotropicFSMaterialLaw::initIPVariable(IPVariable* ipv, bool stiff){
  AnisotropicFSIPVariable* ipvcur = static_cast<AnisotropicFSIPVariable*>(ipv);
  ipvcur->getRefToTangentModuli() = _tilaw.getElasticityTensor();
}


