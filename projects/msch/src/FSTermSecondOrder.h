#ifndef FSTERMSECONDORDER_H_
#define FSTERMSECONDORDER_H_

#include "FSTerms.h"

class FSSecondOrderInternalForceTerm : public FSInternalForceTerm{
  public:
    FSSecondOrderInternalForceTerm(FunctionSpace<double>& space1_,const materialLaw* mlaw, const IPField* ipf)
                                  :FSInternalForceTerm(space1_,mlaw,ipf){}

    virtual ~FSSecondOrderInternalForceTerm(){}
    virtual void get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const;
    virtual LinearTermBase<double>* clone () const{
      return new FSSecondOrderInternalForceTerm(space1,_mlaw,_ipf);
    };
};


class FSSecondOrderStiffnessTerm : public FSStiffnessTerm {
  public:
    FSSecondOrderStiffnessTerm(FunctionSpace<double>& space1_,FunctionSpace<double>& space2_,const materialLaw* mlaw, const IPField* ipf):
                                FSStiffnessTerm(space1_,space2_,mlaw,ipf){}
    FSSecondOrderStiffnessTerm(FunctionSpace<double>& space1_, const materialLaw* mlaw, const IPField* ipf):
                      FSStiffnessTerm(space1_,mlaw,ipf){}
    virtual ~FSSecondOrderStiffnessTerm(){};
    virtual void get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const;
    virtual BilinearTermBase* clone () const{
      return new FSSecondOrderStiffnessTerm(space1,_mlaw,_ipf);
    }

};

#endif // FSTERMSECONDORDER_H_
