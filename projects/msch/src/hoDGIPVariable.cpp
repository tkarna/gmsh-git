
#include "hoDGIPVariable.h"
#include "ipField.h"

hoDGIPVariable::hoDGIPVariable(bool oninter): dG3DIPVariable(oninter){

};

hoDGIPVariable::hoDGIPVariable(const hoDGIPVariable& src): dG3DIPVariable(src),
        gradientOfDeformationGradient(src.gradientOfDeformationGradient),
        secondOrderStress(src.secondOrderStress),
        secondOrderTangent(src.secondOrderTangent),
        crossTangentFirstSecond(src.crossTangentFirstSecond),
        crossTangentSecondFirst(src.crossTangentSecondFirst),
        thirdOrderDeformationGradient(src.thirdOrderDeformationGradient){
  secondOrderElasticTangentModuli = src.secondOrderElasticTangentModuli;
  crossFirstSecondElasticTangentModuli  = src.crossFirstSecondElasticTangentModuli;
  crossSecondFirstElasticTangentModuli = src.crossSecondFirstElasticTangentModuli;
};

hoDGIPVariable& hoDGIPVariable::operator= (const IPVariable& _src){
  dG3DIPVariable::operator =(_src);
  const hoDGIPVariable* src = dynamic_cast<const hoDGIPVariable*>(&_src);
  if (src){
    gradientOfDeformationGradient = src->gradientOfDeformationGradient;
    secondOrderStress = src->secondOrderStress;
    secondOrderTangent = src->secondOrderTangent;
    crossTangentFirstSecond = src->crossTangentFirstSecond;
    crossTangentSecondFirst = src->crossTangentSecondFirst;
    secondOrderElasticTangentModuli = src->secondOrderElasticTangentModuli;
    crossFirstSecondElasticTangentModuli  = src->crossFirstSecondElasticTangentModuli;
    crossSecondFirstElasticTangentModuli = src->crossSecondFirstElasticTangentModuli;
    thirdOrderDeformationGradient = src->thirdOrderDeformationGradient;
  };
  return *this;
};


void hoDGIPVariable::getStrain(STensor3& GL) const{
  GL*= 0;
  STensor3 I(1.);
  GL -=I;
  for (int i = 0; i< 3; i ++){
    for (int j = 0; j< 3; j ++){
      for (int k=0; k<3; k++){
        GL(i,j)  += this->getConstRefToDeformationGradient()(k,i)*this->getConstRefToDeformationGradient()(k,j);
      };
    };
  };
  GL*=0.5;
};

double hoDGIPVariable::defoEnergy() const{
  STensor3 cauchy;
  getCauchyStress(cauchy);
  STensor3 strain;
  getStrain(strain);
  double energ = 0.5*dot(strain,cauchy);

  const STensor33& G = getConstRefToGradientOfDeformationGradient();
  const STensor33& Q = getConstRefToSecondOrderStress();

  energ += 0.5*dot(G,Q);

  return energ;
};



double hoDGIPVariable::VMEquivalentStrain(STensor3& ten) const{
  double tr = ten.trace()/3.;
	double temp = 0;
	for (int i=0; i<3; i++){
		for (int j=0; j<3; j++){
			if (i==j){
				temp+= (ten(i,j) - tr)*(ten(i,j)-tr);
			}
			else{
				temp+= ten(i,j)*ten(i,j);
			};
		};
	};
	temp*=(2.0/3.0);
	return sqrt(temp);
};

double hoDGIPVariable::get(const int comp) const{
  const STensor3& F = getConstRefToDeformationGradient();
  const STensor3& P = getConstRefToFirstPiolaKirchhoffStress();
  const STensor33& G = getConstRefToGradientOfDeformationGradient();
  const STensor33& Q = getConstRefToSecondOrderStress();
  STensor3 sig(0.0);
	this->getCauchyStress(sig);
	STensor3 GL(0.);
  this->getStrain(GL);

  if (comp ==IPField::SIG_XX){
    return sig(0,0);
  }
  else if (comp ==IPField::SIG_YY){
    return sig(1,1);
  }
  else if (comp ==IPField::SIG_ZZ){
    return sig(2,2);
  }
  else if (comp ==IPField::SIG_XY){
    return sig(0,1);
  }
  else if (comp ==IPField::SIG_XZ){
    return sig(0,2);
  }
  else if (comp ==IPField::SIG_YZ){
    return sig(1,2);
  }
  else if (comp ==IPField::SVM){
    return this->vonMises();
  }
  else if (comp ==IPField::GLSTRAINEQUIVALENT){
    return VMEquivalentStrain(GL);
  }
  else if (comp ==IPField::STRAIN_XX){
    return GL(0,0);
  }
  else if (comp ==IPField::STRAIN_XY){
    return GL(0,1);
  }
  else if (comp ==IPField::STRAIN_XZ){
    return GL(0,2);
  }
  else if (comp ==IPField::STRAIN_YY){
    return GL(1,1);
  }
  else if (comp ==IPField::STRAIN_YZ){
    return GL(1,2);
  }
  else if (comp ==IPField::STRAIN_ZZ){
    return GL(2,2);
  }
  else if (comp == IPField::HIGHSTRAIN_NORM){
    return sqrt(dot(this->gradientOfDeformationGradient,this->gradientOfDeformationGradient));
  }
  else if (comp == IPField::HIGHSTRESS_NORM){
    return sqrt(dot(this->secondOrderStress,this->secondOrderStress));
  }
  else if (comp == IPField::GL_NORM){
    return sqrt(dot(GL,GL));
  }
  else if (comp == IPField::STRESS_NORM){
    return sqrt(dot(sig,sig));
  }
  else if (comp == IPField::F_XX){
        return F(0,0);
	}
	else if (comp == IPField::F_XY){
        return F(0,1);
	}
	else if (comp == IPField::F_XZ){
        return F(0,2);
	}
	else if (comp == IPField::F_YX){
        return F(1,0);
	}
	else if (comp == IPField::F_YY){
        return F(1,1);
	}
	else if (comp == IPField::F_YZ){
        return F(1,2);
	}
	else if (comp == IPField::F_ZX){
        return F(2,0);
	}
	else if (comp == IPField::F_ZY){
        return F(2,1);
	}
	else if (comp == IPField::F_ZZ){
        return F(2,2);
	}
	else if (comp == IPField::P_XX){
        return P(0,0);
	}
	else if (comp == IPField::P_XY){
        return P(0,1);
	}
	else if (comp == IPField::P_XZ){
        return P(0,2);
	}
	else if (comp == IPField::P_YX){
        return P(1,0);
	}
	else if (comp == IPField::P_YY){
        return P(1,1);
	}
	else if (comp == IPField::P_YZ){
        return P(1,2);
	}
	else if (comp == IPField::P_ZX){
        return P(2,0);
	}
	else if (comp == IPField::P_ZY){
        return P(2,1);
	}
	else if (comp == IPField::P_ZZ){
        return P(2,2);
	}
	else if (comp == IPField::G_XXX){
    return G(0,0,0);
	}
	else if (comp == IPField::G_XXY){
    return G(0,0,1);
	}
	else if (comp == IPField::G_XXZ){
    return G(0,0,2);
	}
	else if (comp == IPField::G_XYX){
    return G(0,1,0);
	}
	else if (comp == IPField::G_XYY){
    return G(0,1,1);
	}
	else if (comp == IPField::G_XYZ){
    return G(0,1,2);
	}
	else if (comp == IPField::G_XZX){
    return G(0,2,0);
	}
	else if (comp == IPField::G_XZY){
    return G(0,2,1);
	}
	else if (comp == IPField::G_XZZ){
    return G(0,2,2);
	}
	else if (comp == IPField::G_YXX){
    return G(1,0,0);
	}
	else if (comp == IPField::G_YXY){
    return G(1,0,1);
	}
	else if (comp == IPField::G_YXZ){
    return G(1,0,2);
	}
	else if (comp == IPField::G_YYX){
    return G(1,1,0);
	}
	else if (comp == IPField::G_YYY){
    return G(1,1,1);
	}
	else if (comp == IPField::G_YYZ){
    return G(1,1,2);
	}
	else if (comp == IPField::G_YZX){
    return G(1,2,0);
	}
	else if (comp == IPField::G_YZY){
    return G(1,2,1);
	}
	else if (comp == IPField::G_YZZ){
    return G(1,2,2);
	}
	else if (comp == IPField::G_ZXX){
    return G(2,0,0);
	}
	else if (comp == IPField::G_ZXY){
    return G(2,0,1);
	}
	else if (comp == IPField::G_ZXZ){
    return G(2,0,2);
	}
	else if (comp == IPField::G_ZYX){
    return G(2,1,0);
	}
	else if (comp == IPField::G_ZYY){
    return G(2,1,1);
	}
	else if (comp == IPField::G_ZYZ){
    return G(2,1,2);
	}
	else if (comp == IPField::G_ZZX){
    return G(2,2,0);
	}
	else if (comp == IPField::G_ZZY){
    return G(2,2,1);
	}
	else if (comp == IPField::G_ZZZ){
    return G(2,2,2);
	}
	else if (comp == IPField::Q_XXX){
    return Q(0,0,0);
	}
	else if (comp == IPField::Q_XXY){
    return Q(0,0,1);
	}
	else if (comp == IPField::Q_XXZ){
    return Q(0,0,2);
	}
	else if (comp == IPField::Q_XYX){
    return Q(0,1,0);
	}
	else if (comp == IPField::Q_XYY){
    return Q(0,1,1);
	}
	else if (comp == IPField::Q_XYZ){
    return Q(0,1,2);
	}
	else if (comp == IPField::Q_XZX){
    return Q(0,2,0);
	}
	else if (comp == IPField::Q_XZY){
    return Q(0,2,1);
	}
	else if (comp == IPField::Q_XZZ){
    return Q(0,2,2);
	}
	else if (comp == IPField::Q_YXX){
    return Q(1,0,0);
	}
	else if (comp == IPField::Q_YXY){
    return Q(1,0,1);
	}
	else if (comp == IPField::Q_YXZ){
    return Q(1,0,2);
	}
	else if (comp == IPField::Q_YYX){
    return Q(1,1,0);
	}
	else if (comp == IPField::Q_YYY){
    return Q(1,1,1);
	}
	else if (comp == IPField::Q_YYZ){
    return Q(1,1,2);
	}
	else if (comp == IPField::G_YZX){
    return Q(1,2,0);
	}
	else if (comp == IPField::Q_YZY){
    return Q(1,2,1);
	}
	else if (comp == IPField::Q_YZZ){
    return Q(1,2,2);
	}
	else if (comp == IPField::Q_ZXX){
    return Q(2,0,0);
	}
	else if (comp == IPField::Q_ZXY){
    return Q(2,0,1);
	}
	else if (comp == IPField::Q_ZXZ){
    return Q(2,0,2);
	}
	else if (comp == IPField::Q_ZYX){
    return Q(2,1,0);
	}
	else if (comp == IPField::Q_ZYY){
    return Q(2,1,1);
	}
	else if (comp == IPField::Q_ZYZ){
    return Q(2,1,2);
	}
	else if (comp == IPField::Q_ZZX){
    return Q(2,2,0);
	}
	else if (comp == IPField::Q_ZZY){
    return Q(2,2,1);
	}
	else if (comp == IPField::Q_ZZZ){
    return Q(2,2,2);
	}
  else{
    return 0.0;
  };
};

#if defined(HAVE_MPI)

// using in multiscale analysis with MPI
int hoDGIPVariable::getMacroNumberElementDataSendToMicroProblem() const {
  return 9+27;
};
int hoDGIPVariable::getMicroNumberElementDataSendToMacroProblem() const {
  return 9+27+81+243+243+729;
};
void hoDGIPVariable::getMacroDataSendToMicroProblem(double* val) const {
  const STensor3& F = this->getConstRefToDeformationGradient();
  int ii =0;
  for (int idex = 0; idex < 9; idex++){
    int i,j;
    Tensor23::getIntsFromIndex(idex,i,j);
    val[ii] = F(i,j);
    ii++;
  }
  const STensor33& G = this->getConstRefToGradientOfDeformationGradient();
  for (int idex = 0; idex < 27; idex++){
    int i,j,k;
    Tensor33::getIntsFromIndex(idex,i,j,k);
    val[ii] = G(i,j,k);
    ii++;
  }
};
void hoDGIPVariable::getMicroDataToMacroProblem(double* val) const {
  const STensor3& P = this->getConstRefToFirstPiolaKirchhoffStress();
  const STensor33& Q = this->getConstRefToSecondOrderStress();
  const STensor43& L = this->getConstRefToTangentModuli();
  const STensor53& JFirstSecond = this->getConstRefToCrossTangentFirstSecond();
  const STensor53& JSecondFirst = this->getConstRefToCrossTangentSecondFirst();
  const STensor63& J = this->getConstRefToSecondOrderTangent();

  int ii=0;
  for (int idex = 0; idex< 9; idex++){
    int i,j;
    Tensor23::getIntsFromIndex(idex,i,j);
    val[ii] = P(i,j);
    ii++;
  }
  for (int idex = 0; idex <27; idex ++){
    int i,j,k;
    Tensor33::getIntsFromIndex(idex,i,j,k);
    val[ii] = Q(i,j,k);
    ii++;
  };

  for (int idex = 0; idex <81; idex ++){
    int i,j,k,l;
    Tensor43::getIntsFromIndex(idex,i,j,k,l);
    val[ii] = L(i,j,k,l);
    ii++;
  };

  for (int idex = 0; idex <243; idex ++){
    int i,j,k,l,m;
    Tensor53::getIntsFromIndex(idex,i,j,k,l,m);
    val[ii] = JFirstSecond(i,j,k,l,m);
    ii++;
  };
  for (int idex = 0; idex< 243; idex++){
    int i,j,k,l,m;
    Tensor53::getIntsFromIndex(idex,i,j,k,l,m);
    val[ii] = JSecondFirst(i,j,k,l,m);
    ii++;
  }
  for (int idex = 0;idex< 729; idex++){
    int i,j,k,p,q,r;
    Tensor63::getIntsFromIndex(idex,i,j,k,p,q,r);
    val[ii] = J(i,j,k,p,q,r);
    ii++;
  }
};
void hoDGIPVariable::setReceivedMacroDataToMicroProblem(const double* val){
  STensor3& F = this->getRefToDeformationGradient();
  int ii = 0;
  for (int idex = 0; idex <9; idex ++){
    int i,j;
    Tensor23::getIntsFromIndex(idex,i,j);
    F(i,j) = val[ii];
    ii++;
  }
  STensor33& G = this->getRefToGradientOfDeformationGradient();
  for (int idex = 0; idex < 27; idex++){
    int i,j,k;
    Tensor33::getIntsFromIndex(idex,i,j,k);
     G(i,j,k) = val[ii];
     ii++;
  };
};
void hoDGIPVariable::setReceivedMicroDataToMacroProblem(const double* val){
  STensor3& P = this->getRefToFirstPiolaKirchhoffStress();
  STensor33& Q = this->getRefToSecondOrderStress();
  STensor43& L = this->getRefToTangentModuli();
  STensor53& JFirstSecond = this->getRefToCrossTangentFirstSecond();
  STensor53& JSecondFirst = this->getRefToCrossTangentSecondFirst();
  STensor63& J = this->getRefToSecondOrderTangent();

  int ii=0;
  for (int idex = 0; idex< 9; idex++){
    int i,j;
    Tensor23::getIntsFromIndex(idex,i,j);
    P(i,j) = val[ii];
    ii++;
  }
  for (int idex = 0; idex <27; idex ++){
    int i,j,k;
    Tensor33::getIntsFromIndex(idex,i,j,k);
    Q(i,j,k) = val[ii];
    ii++;
  };

  for (int idex = 0; idex <81; idex ++){
    int i,j,k,l;
    Tensor43::getIntsFromIndex(idex,i,j,k,l);
     L(i,j,k,l)= val[ii];
    ii++;
  };

  for (int idex = 0; idex <243; idex ++){
    int i,j,k,l,m;
    Tensor53::getIntsFromIndex(idex,i,j,k,l,m);
    JFirstSecond(i,j,k,l,m) = val[ii];
    ii++;
  };
  for (int idex = 0; idex< 243; idex++){
    int i,j,k,l,m;
    Tensor53::getIntsFromIndex(idex,i,j,k,l,m);
    JSecondFirst(i,j,k,l,m) = val[ii];
    ii++;
  }
  for (int idex = 0;idex< 729; idex++){
    int i,j,k,p,q,r;
    Tensor63::getIntsFromIndex(idex,i,j,k,p,q,r);
    J(i,j,k,p,q,r) = val[ii];
    ii++;
  }
};
#endif // MPI
void hoDGLinearElasticIPVariable::getStrain(STensor3& GL) const{
  const STensor3& F = getConstRefToDeformationGradient();
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      if (i==j) GL(i,j) = F(i,j)-1.;
      else GL(i,j) = 0.5*(F(i,j)+F(j,i));
};

void hoDGLinearElasticIPVariable::getCauchyStress(STensor3& cauchy) const{
  cauchy = this->getConstRefToFirstPiolaKirchhoffStress();
}


