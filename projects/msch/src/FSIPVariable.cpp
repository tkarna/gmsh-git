#include "FSIPVariable.h"
#include "ipstate.h"
#include "ipField.h"
#include "highOrderTensor.h"


FSIPVariable::FSIPVariable() : ipFiniteStrain(),
                  deformationGradient(1),
                  firstPiolaKirchhoffStress(0),
                  tangentModuli(0){};
FSIPVariable::FSIPVariable(const FSIPVariable &source) : ipFiniteStrain(source),
                        deformationGradient(source.deformationGradient),
                        firstPiolaKirchhoffStress(source.firstPiolaKirchhoffStress),
                        tangentModuli(source.tangentModuli){
};
FSIPVariable& FSIPVariable::operator = (const IPVariable &_source){
	ipFiniteStrain::operator=(_source);
	const FSIPVariable *source=dynamic_cast<const FSIPVariable *> (&_source);
	if (source){
		deformationGradient = source->deformationGradient;
		firstPiolaKirchhoffStress = source->firstPiolaKirchhoffStress;
		tangentModuli = source ->tangentModuli;
	};
	return *this;
}
FSIPVariable::~FSIPVariable(){
};

double FSIPVariable::get(const int comp) const{
	STensor3 sig(0.0);
	this->getCauchyStress(sig);
	STensor3 GL(0.0);
	this->getStrain(GL);
	const STensor3& F = this->getConstRefToDeformationGradient();
	const STensor3& P = this->getConstRefToFirstPiolaKirchhoffStress();
	if (comp==IPField::GL_NORM){
    return sqrt(dot(GL,GL));
	}
	else if (comp == IPField::P_STRESS_NORM){
    return sqrt(dot(P,P));
	}
	else if (comp == IPField::STRESS_NORM){
    return sqrt(dot(sig,sig));
	}
  else if (comp ==IPField::SIG_XX){
    return sig(0,0);
  }
  else if (comp ==IPField::SIG_YY){
    return sig(1,1);
  }
  else if (comp ==IPField::SIG_ZZ){
    return sig(2,2);
  }
  else if (comp ==IPField::SIG_XY){
    return sig(0,1);
  }
  else if (comp ==IPField::SIG_XZ){
    return sig(0,2);
  }
  else if (comp ==IPField::SIG_YZ){
    return sig(1,2);
  }
  else if (comp ==IPField::SVM){
    return vonMises();
  }
  else if (comp ==IPField::GLSTRAINEQUIVALENT){
		return equivalentStrain();
	}
	else if (comp ==IPField::STRAIN_XX){
		return GL(0,0);
	}
	else if (comp ==IPField::STRAIN_XY){
		return GL(0,1);
	}
	else if (comp ==IPField::STRAIN_XZ){
		return GL(0,2);
	}
	else if (comp ==IPField::STRAIN_YY){
		return GL(1,1);
	}
	else if (comp ==IPField::STRAIN_YZ){
		return GL(1,2);
	}
	else if (comp ==IPField::STRAIN_ZZ){
		return GL(2,2);
	}
	else if (comp == IPField::F_XX){
        return F(0,0);
	}
	else if (comp == IPField::F_XY){
        return F(0,1);
	}
	else if (comp == IPField::F_XZ){
        return F(0,2);
	}
	else if (comp == IPField::F_YX){
        return F(1,0);
	}
	else if (comp == IPField::F_YY){
        return F(1,1);
	}
	else if (comp == IPField::F_YZ){
        return F(1,2);
	}
	else if (comp == IPField::F_ZX){
        return F(2,0);
	}
	else if (comp == IPField::F_ZY){
        return F(2,1);
	}
	else if (comp == IPField::F_ZZ){
        return F(2,2);
	}
	else if (comp == IPField::P_XX){
        return P(0,0);
	}
	else if (comp == IPField::P_XY){
        return P(0,1);
	}
	else if (comp == IPField::P_XZ){
        return P(0,2);
	}
	else if (comp == IPField::P_YX){
        return P(1,0);
	}
	else if (comp == IPField::P_YY){
        return P(1,1);
	}
	else if (comp == IPField::P_YZ){
        return P(1,2);
	}
	else if (comp == IPField::P_ZX){
        return P(2,0);
	}
	else if (comp == IPField::P_ZY){
        return P(2,1);
	}
	else if (comp == IPField::P_ZZ){
        return P(2,2);
	}
  else{
    return 0.0;
  };
};

double FSIPVariable::getJ() const{
  double mat[3][3];
  for(int i = 0; i < 3; i++)
    for(int j = 0; j < 3; j++)
      mat[i][j] = getConstRefToDeformationGradient()(i,j);
  return det3x3(mat);
};

double FSIPVariable::vonMises() const{
  STensor3 cauchy;
  getCauchyStress(cauchy);
  double svm= (cauchy(0,0)-cauchy(1,1))*(cauchy(0,0)-cauchy(1,1))+(cauchy(0,0)-cauchy(2,2))*(cauchy(0,0)-cauchy(2,2))+
               (cauchy(2,2)-cauchy(1,1))*(cauchy(2,2)-cauchy(1,1));
  svm += 6.*(cauchy(0,1)*cauchy(1,0)+cauchy(0,2)*cauchy(2,0)+cauchy(2,1)*cauchy(1,2));
  svm /= 2.;
  svm = sqrt(svm);
  return svm;
}

double FSIPVariable::defoEnergy() const{
  STensor3 sig(0.0);
	this->getCauchyStress(sig);
	STensor3 GL(0.0);
	this->getStrain(GL);
  double energ = 0;
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      energ+= 0.5*sig(i,j)*GL(i,j);
    }
  }
	return energ;
};

void FSIPVariable::getStrain(STensor3& GL)const{
  const STensor3& F = getConstRefToDeformationGradient();
  GL*=0;
  for (int i=0; i<3; i++){
    GL(i,i) -= 0.5;
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        GL(i,j) += 0.5*F(k,i)*F(k,j);
      }
    }
  }
};
void FSIPVariable::getCauchyStress(STensor3& sig) const{
  const STensor3& F = getConstRefToDeformationGradient();
  const STensor3& P = getConstRefToFirstPiolaKirchhoffStress();
  double J= getJ();
  if (J < 1.e-12) Msg::Error("Negative Jacobian FSDomain");
  sig*=0;
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      for (int k=0; k<3; k++)
        sig(i,j) += P(i,k)*F(j,k)/J;
};


double FSIPVariable::equivalentStrain() const{
  STensor3 strain(0.);
  getStrain(strain);
	double tr = strain.trace()/3.;
	double temp = 0;
	for (int i=0; i<3; i++){
		for (int j=0; j<3; j++){
			if (i==j){
				temp+= (strain(i,j) - tr)*(strain(i,j)-tr);
			}
			else{
				temp+= strain(i,j)*strain(i,j);
			};
		};
	};
	temp*=(2.0/3.0);
	return sqrt(temp);
};

double FSIPVariable::plasticEnergy() const {return 0;};

STensor3& FSIPVariable::getRefToDeformationGradient(){return deformationGradient;};
const STensor3& FSIPVariable::getConstRefToDeformationGradient() const{return deformationGradient;};
STensor3& FSIPVariable::getRefToFirstPiolaKirchhoffStress(){return firstPiolaKirchhoffStress;};
const STensor3& FSIPVariable::getConstRefToFirstPiolaKirchhoffStress() const {return firstPiolaKirchhoffStress;};
STensor43& FSIPVariable::getRefToTangentModuli(){return tangentModuli;};
const STensor43& FSIPVariable::getConstRefToTangentModuli() const{return tangentModuli;};



LinearElasticIPVariable::LinearElasticIPVariable():FSIPVariable(){};
LinearElasticIPVariable::LinearElasticIPVariable(const LinearElasticIPVariable& src) : FSIPVariable(src){};
LinearElasticIPVariable& LinearElasticIPVariable::operator = (const IPVariable& src){
  FSIPVariable::operator=(src);
  return *this;
};
LinearElasticIPVariable::~LinearElasticIPVariable(){};


void LinearElasticIPVariable::getCauchyStress(STensor3& sig) const {
  sig = this->getConstRefToFirstPiolaKirchhoffStress();
}
void LinearElasticIPVariable::getStrain(STensor3& GL) const{
  const STensor3& F = getConstRefToDeformationGradient();
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      if (i==j) GL(i,j) = F(i,j)-1.;
      else GL(i,j) = 0.5*(F(i,j)+F(j,i));
}

FSJ2LinearIPVariable::FSJ2LinearIPVariable(const mlawJ2linear& j2law):FSIPVariable(){
  if (j2law.getJ2IsotropicHardening() == NULL)
    Msg::Error("mlawJ2linear has no J2IsotropicHardening");
  else
    _j2ipv = new IPJ2linear(j2law.getJ2IsotropicHardening());
};
FSJ2LinearIPVariable::FSJ2LinearIPVariable(const FSJ2LinearIPVariable& src) : FSIPVariable(src){
  if (src._j2ipv == NULL)
    _j2ipv = NULL;
  else
    _j2ipv = new IPJ2linear(*src._j2ipv);
};
FSJ2LinearIPVariable& FSJ2LinearIPVariable::operator = (const IPVariable& src){
	FSIPVariable::operator = (src);
	const FSJ2LinearIPVariable* s = dynamic_cast<const FSJ2LinearIPVariable*>(&src);
	if (s){
    if (s->_j2ipv == NULL){
      if (_j2ipv != NULL) delete _j2ipv;
      _j2ipv = NULL;
    }
    else if (s->_j2ipv != NULL && _j2ipv != NULL){
      _j2ipv->operator=(*s->_j2ipv);
    }
    else if (s->_j2ipv != NULL && _j2ipv == NULL){
      _j2ipv = new IPJ2linear(*s->_j2ipv);
    }

	}
	return *this;
};
FSJ2LinearIPVariable::~FSJ2LinearIPVariable(){
  if (_j2ipv != NULL) delete _j2ipv;
  _j2ipv = NULL;
};

double FSJ2LinearIPVariable::get(const int comp) const{
	if(comp == IPField::PLASTICSTRAIN){
    return _j2ipv->_j2lepspbarre;
  }
  else{
    return FSIPVariable::get(comp);
  }
};

double FSJ2LinearIPVariable::plasticEnergy() const {
  return _j2ipv->plasticEnergy();
};

AnisotropicFSIPVariable::AnisotropicFSIPVariable() : FSIPVariable()
{

}
AnisotropicFSIPVariable::AnisotropicFSIPVariable(const AnisotropicFSIPVariable &source) : FSIPVariable(source)
{
   _tiipv = source._tiipv;
}
AnisotropicFSIPVariable& AnisotropicFSIPVariable::operator=(const IPVariable &source)
{
  FSIPVariable::operator=(source);
  const AnisotropicFSIPVariable* src = dynamic_cast<const AnisotropicFSIPVariable*>(&source);
  if(src != NULL)
    _tiipv = src->_tiipv;
  return *this;
}
double AnisotropicFSIPVariable::get(const int comp) const
{
  if(comp == IPField::PLASTICSTRAIN)
  {
    return 0.;
  }
  else
  {
    return FSIPVariable::get(comp);
  }
}
double  AnisotropicFSIPVariable::defoEnergy() const
{
  return getIPAnisotropic()->defoEnergy();
}
double  AnisotropicFSIPVariable::plasticEnergy() const
{
  return getIPAnisotropic()->plasticEnergy();
}

