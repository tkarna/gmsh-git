//
// C++ Interface: matrialLaw
//
// Description: Class with definition of materialLaw for finite strain problem
//
//
// Author:  , (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef FINITESTRAINMATERIALLAW_H_
#define FINITESTRAINMATERIALLAW_H_
#ifndef SWIG
#include "mlaw.h"
#include "FSIPVariable.h"
#include "elasticLaw.h"
#include "mlawAnisotropic.h"
#endif
#include "numericalMaterial.h"

/* Interface for finite strain material*/
class FSMaterialLaw : public materialLaw{
	protected:
		double _rho;

    static double _sameStateTolerance;

	public:
    static void setSameStateTolerance(const double eps){ _sameStateTolerance = eps;}
    static double getSameStateTolerance() {return _sameStateTolerance;}
		#ifndef SWIG
		FSMaterialLaw(const int num, const double r , const bool init = true);
		FSMaterialLaw(const FSMaterialLaw& src);
		virtual FSMaterialLaw& operator = (const materialLaw& src);
		virtual ~FSMaterialLaw();

		virtual double density() const{return _rho;};
		virtual matname getType() const=0;
		virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt=0) const=0;
		virtual void initLaws(const std::map<int,materialLaw*> &maplaw)=0;
		// for explicit scheme it must return sqrt(E/rho) adapted to your case
		virtual double soundSpeed() const=0;
		virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, bool stiff)=0;
		virtual double scaleFactor() const =0;

		virtual void initIPVariable(IPVariable* ipv, bool stiff)= 0;
		#endif //_SWIG
};

/* Elastic material*/
class FSElasticMaterialLaw : public FSMaterialLaw{
  #ifndef SWIG
  protected:
    elasticLaw* _elasticLaw;
    elasticLawBase::ELASTIC_TYPE _type; /*Linear elastic, Neo Hookean, Bi-Logarithm */
	#endif

  public:
		FSElasticMaterialLaw(const int num, const double rho, const bool init = true);
		FSElasticMaterialLaw(const int num, const int type, const double rho,const bool init = true);
    FSElasticMaterialLaw(const int num, const int type, const double k, const double mu,
                         const double rho, const double tol = 1.e-6,const bool init = true);
		virtual void setType(const int  type);
		virtual void setParameter(const std::string key, const double val);
    #ifndef SWIG
    FSElasticMaterialLaw(const FSElasticMaterialLaw& src);
    virtual FSElasticMaterialLaw& operator = (const materialLaw& src);
    virtual ~FSElasticMaterialLaw();

    virtual matname getType() const {return materialLaw::FSElastic;};
		virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt=0) const;
		virtual void initLaws(const std::map<int,materialLaw*> &maplaw) {};
		// for explicit scheme it must return sqrt(E/rho) adapted to your case
		virtual double soundSpeed() const;
		virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, bool stiff);
		virtual double scaleFactor() const;
		virtual void initIPVariable(IPVariable* ipv, bool stiff);
		#endif
};

/* Elasto-plastic material */
class FSJ2LinearMaterialLaw : public FSMaterialLaw{
	protected:
		mlawJ2linear _j2law;
		double _E,_nu;

	public:
		FSJ2LinearMaterialLaw(const int num, const double E, const double nu, const double sy0, const double h,
													const double rho, const double tol =1.e-6,const bool flag = false, const double eps = 1e-8,const bool init = true);
		#ifndef SWIG
		FSJ2LinearMaterialLaw(const FSJ2LinearMaterialLaw& src);
		FSJ2LinearMaterialLaw& operator = (const materialLaw& src);
		virtual ~FSJ2LinearMaterialLaw();
		virtual matname getType() const{return materialLaw::FSElastoPlastic;};
		virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const;
		virtual void initLaws(const std::map<int,materialLaw*> &maplaw){};
		virtual double soundSpeed() const{ return _j2law.soundSpeed();};
		virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, bool stiff);
		virtual double scaleFactor() const {return _E/2./(1+_nu);};

		virtual void initIPVariable(IPVariable* ipv, bool stiff);
		#endif //SWIG
};
class AnisotropicFSMaterialLaw : public FSMaterialLaw
{
 protected:
  mlawAnisotropic _tilaw;
  std::vector<double> _ElasticityTensor;
 private: // cache data
  mutable STensor43 _tangent3D;
  const STensor3 _ISTensor3;
  mutable STensor3 tau,PKxyz,PKsh,invF,tauXYZ,Fp1,Fp0;
 public:
  AnisotropicFSMaterialLaw(const int num,const double rho,
				const double Ex,const double Ey,const double Ez,
				const double Vxy,const double Vxz,const double Vyz,
				const double MUxy,const double MUxz,const double MUyz,
                   		const double alpha, const double beta, const double gamma);
 #ifndef SWIG
  AnisotropicFSMaterialLaw(const AnisotropicFSMaterialLaw &source);
  virtual ~AnisotropicFSMaterialLaw(){}
  // set the time of _tilaw
   virtual void setTime(const double t,const double dtime){
    this->_timeStep = dtime;
    this->_currentTime = t;
    _tilaw.setTime(t,dtime);
  }
  virtual materialLaw::matname getType() const {return _tilaw.getType();}
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv,const MElement *ele=NULL, const int nbFF_=0) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const{return _tilaw.soundSpeed();} // or change value ??
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, const bool stiff=true);
  virtual double scaleFactor() const {return _tilaw.getElasticityTensor()(0,0,0,0);};
  virtual void initIPVariable(IPVariable* ipv, bool stiff);

 #endif // SWIG
};


/*Multiscale material law */
class FSMultiscaleMaterialLaw : public FSMaterialLaw,
                                 public numericalMaterial{
  public:
    FSMultiscaleMaterialLaw(const int lnum, const int tag, const bool init = true);
    #ifndef SWIG
    FSMultiscaleMaterialLaw(const FSMultiscaleMaterialLaw& src)
                          :FSMaterialLaw(src),numericalMaterial(src){};
    virtual ~FSMultiscaleMaterialLaw(){};

    virtual matname getType() const{ return materialLaw::numeric;};
		virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt=0) const;
		/*Create IPState using in case of multiscale analysis where the solver is not used*/
		virtual void createIPState(const bool isSolve, IPStateBase* &ips,const bool* state=NULL,
                                 const MElement *ele=NULL, const int nbFF_=0, const int gpt=0) const;

		virtual void initLaws(const std::map<int,materialLaw*> &maplaw){};
		// for explicit scheme it must return sqrt(E/rho) adapted to your case
		virtual double soundSpeed() const{ return 0;};
		virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, bool stiff);
		virtual double scaleFactor() const {return 1;};

		virtual void initIPVariable(IPVariable* ipv, bool stiff) {};
		virtual bool isNumeric() const{return true;}
    #endif //SWIG
};
#endif // FINITESTRAINMATERIALLAW_H_
