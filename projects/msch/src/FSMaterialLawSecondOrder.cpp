
#include "FSMaterialLawSecondOrder.h"
#include "FSIPVariableSecondOrder.h"

FSElasticMaterialLawSecondOrder::FSElasticMaterialLawSecondOrder(const int num, const double rho, const bool init):
                                FSElasticMaterialLaw(num,rho,init),_secondElasticLaw(NULL),
                                _typeSecond(elasticLawBase::LINEAR_ELASTIC_SECOND_ORDER){}

FSElasticMaterialLawSecondOrder::FSElasticMaterialLawSecondOrder(const int num, const int first, const int second,
                                const double rho,const bool init):
                                FSElasticMaterialLaw(num,first,rho,init){
  _typeSecond = (elasticLawBase::ELASTIC_TYPE)second;
  if (_typeSecond == elasticLawBase::LINEAR_ELASTIC_SECOND_ORDER)
    _secondElasticLaw = new linearElasticSecondOrderLaw();
  else if (_typeSecond == elasticLawBase::LINEAR_MINDLIN)
    _secondElasticLaw = new linearElasticMindlinLaw();
  else{
    Msg::Error("this elastic second law is not available");
    _secondElasticLaw  = NULL;
   }
};

FSElasticMaterialLawSecondOrder::FSElasticMaterialLawSecondOrder(const int num, const int first, const int second,
                                              const double k, const double mu,
                                              const double rho, const double tol,const bool init):
                                FSElasticMaterialLaw(num,first,k,mu,rho,tol,init){
  _typeSecond = (elasticLawBase::ELASTIC_TYPE)second;
  if (_typeSecond == elasticLawBase::LINEAR_ELASTIC_SECOND_ORDER)
    _secondElasticLaw = new linearElasticSecondOrderLaw();
  else if (_typeSecond == elasticLawBase::LINEAR_MINDLIN)
    _secondElasticLaw = new linearElasticMindlinLaw();
  else{
    Msg::Error("this elastic second law is not available");
    _secondElasticLaw = NULL;
  }
};

FSElasticMaterialLawSecondOrder::FSElasticMaterialLawSecondOrder(const FSElasticMaterialLawSecondOrder& src):
                  FSElasticMaterialLaw(src),_typeSecond(src._typeSecond){
  if (src._secondElasticLaw){
    if (_secondElasticLaw != NULL) delete  _secondElasticLaw;
    _secondElasticLaw = dynamic_cast<elasticSecondOrderLaw*>(src._secondElasticLaw->clone());
  }
  else
    _secondElasticLaw = NULL;
};

FSElasticMaterialLawSecondOrder::~FSElasticMaterialLawSecondOrder(){
  if (_secondElasticLaw) delete _secondElasticLaw;
  _secondElasticLaw = NULL;
};

void FSElasticMaterialLawSecondOrder::setType(const int  type){
  this->setType(type,_typeSecond);
};

void FSElasticMaterialLawSecondOrder::setType(const int first, const int second){
  FSElasticMaterialLaw::setType(first);
  _typeSecond = (elasticLawBase::ELASTIC_TYPE)second;
  if (_secondElasticLaw != NULL and _secondElasticLaw->getType() !=_typeSecond){
    delete _secondElasticLaw;
    _secondElasticLaw = NULL;
  }
  if (_secondElasticLaw == NULL){
    if (_typeSecond == elasticLawBase::LINEAR_ELASTIC_SECOND_ORDER)
      _secondElasticLaw = new linearElasticSecondOrderLaw();
    else if (_typeSecond == elasticLawBase::LINEAR_MINDLIN)
      _secondElasticLaw = new linearElasticMindlinLaw();
    else{
      Msg::Error("this second law is not available");
      _secondElasticLaw = NULL;
    }
  }

};

void FSElasticMaterialLawSecondOrder::setParameter(const std::string key, const double val){
  FSElasticMaterialLaw::setParameter(key,val);
  if (_secondElasticLaw == NULL)
    this->setType(this->_type,_typeSecond);

  if (_secondElasticLaw)
    _secondElasticLaw->setParameter(key,val);
};

bool FSElasticMaterialLawSecondOrder::isHighOrder() const {
  if (_secondElasticLaw) return true;
  else return false;
};

void FSElasticMaterialLawSecondOrder::initIPVariable(IPVariable* ipv, bool stiff){
  FSElasticMaterialLaw::initIPVariable(ipv,stiff);
  if (_secondElasticLaw){
    FSIPVariableSecondOrderBase* hoipv = dynamic_cast<FSIPVariableSecondOrderBase*>(ipv);
    const STensor33& G = hoipv->getConstRefToGradientOfDeformationGradient();
    STensor33& Q = hoipv->getRefToSecondOrderStress();
    STensor63& J = hoipv->getRefToSecondOrderTangent();
    _secondElasticLaw->constitutive(G,Q,J,stiff);
  }
};

void FSElasticMaterialLawSecondOrder::createIPState(IPStateBase* &ips,const bool* state_,const MElement *ele, const int nbFF_, const int gpt) const{
	if (_type == elasticLawBase::LINEAR_ELASTIC){
    IPVariable* ipvi = new LinearElasticIPVariableSecondOrder();
    IPVariable* ipv1 = new LinearElasticIPVariableSecondOrder();
    IPVariable* ipv2 = new LinearElasticIPVariableSecondOrder();
    if(ips != NULL) delete ips;
    ips = new IP3State(state_,ipvi,ipv1,ipv2);
  }
  else{
    IPVariable* ipvi = new FSIPVariableSecondOrder();
    IPVariable* ipv1 = new FSIPVariableSecondOrder();
    IPVariable* ipv2 = new FSIPVariableSecondOrder();
    if(ips != NULL) delete ips;
    ips = new IP3State(state_,ipvi,ipv1,ipv2);
  }
};

void FSElasticMaterialLawSecondOrder::stress(IPVariable* ipv, const IPVariable* ipvprev, bool stiff){
  FSElasticMaterialLaw::stress(ipv,ipvprev,stiff);
  if (_secondElasticLaw){
    FSIPVariableSecondOrderBase* hoipv = dynamic_cast<FSIPVariableSecondOrderBase*>(ipv);
    const FSIPVariableSecondOrderBase* hoipvprev = dynamic_cast<const FSIPVariableSecondOrderBase*>(ipvprev);
    const STensor33& G = hoipv->getConstRefToGradientOfDeformationGradient();
    STensor33& Q = hoipv->getRefToSecondOrderStress();
    STensor63& J = hoipv->getRefToSecondOrderTangent();
    _secondElasticLaw->constitutive(G,Q,J,stiff);
  }
};

FSJ2LinearMaterialLawSecondOrder::FSJ2LinearMaterialLawSecondOrder(const int num, const double E, const double nu, const double sy0, const double h,
													const double rho, const double tol,const bool flag, const double eps,const bool init):
                          FSJ2LinearMaterialLaw(num,E,nu,sy0,h,rho,tol,flag,eps,init),
                          _typeSecond(elasticLawBase::LINEAR_ELASTIC_SECOND_ORDER), _secondElasticLaw(NULL){};
FSJ2LinearMaterialLawSecondOrder::FSJ2LinearMaterialLawSecondOrder(const FSJ2LinearMaterialLawSecondOrder& src):
                      FSJ2LinearMaterialLaw(src),_typeSecond(src._typeSecond){
  if (src._secondElasticLaw == NULL){
    if (_secondElasticLaw) delete _secondElasticLaw;
    _secondElasticLaw  = dynamic_cast<elasticSecondOrderLaw*>(src._secondElasticLaw->clone());
  }
  else
    _secondElasticLaw = NULL;
}
FSJ2LinearMaterialLawSecondOrder::~FSJ2LinearMaterialLawSecondOrder(){
  if (_secondElasticLaw) delete _secondElasticLaw;
  _secondElasticLaw = NULL;
}

void FSJ2LinearMaterialLawSecondOrder::setType(const int second){
  _typeSecond = (elasticLawBase::ELASTIC_TYPE)second;
  if (_secondElasticLaw!= NULL and _secondElasticLaw->getType() != _typeSecond){
    delete _secondElasticLaw;
    _secondElasticLaw = NULL;
  }
  if (_secondElasticLaw == NULL){
    if (_typeSecond == elasticLawBase::LINEAR_ELASTIC_SECOND_ORDER)
      _secondElasticLaw = new linearElasticSecondOrderLaw();
    else if (_typeSecond == elasticLawBase::LINEAR_MINDLIN)
      _secondElasticLaw = new linearElasticMindlinLaw();
    else{
      Msg::Error("this second law is not available");
      _secondElasticLaw == NULL;
    }
  }
};

void FSJ2LinearMaterialLawSecondOrder::setParameter(const std::string key, const double val){
  this->setType(_typeSecond);
  if (_secondElasticLaw)
    _secondElasticLaw->setParameter(key,val);
};

void FSJ2LinearMaterialLawSecondOrder::createIPState(IPStateBase* &ips,const bool* state_,const MElement *ele, const int nbFF_, const int gpt) const{
	IPVariable* ipvi = new  FSJ2LinearIPVariableSecondOrder(this->_j2law);
  IPVariable* ipv1 = new  FSJ2LinearIPVariableSecondOrder(this->_j2law);
  IPVariable* ipv2 = new  FSJ2LinearIPVariableSecondOrder(this->_j2law);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
};

void FSJ2LinearMaterialLawSecondOrder::stress(IPVariable* ipv, const IPVariable* ipvprev, bool stiff ){
	FSJ2LinearMaterialLaw::stress(ipv,ipvprev,stiff);
	if (_secondElasticLaw){
    FSJ2LinearIPVariableSecondOrder* hoipv = dynamic_cast<FSJ2LinearIPVariableSecondOrder*>(ipv);
    const FSJ2LinearIPVariableSecondOrder* hoipvprev = dynamic_cast<const FSJ2LinearIPVariableSecondOrder*>(ipvprev);
    const STensor33& G = hoipv->getConstRefToGradientOfDeformationGradient();
    STensor33& Q = hoipv->getRefToSecondOrderStress();
    STensor63& J = hoipv->getRefToSecondOrderTangent();
    _secondElasticLaw->constitutive(G,Q,J,stiff);
	}
};

bool FSJ2LinearMaterialLawSecondOrder::isHighOrder() const {
  if (_secondElasticLaw) return true;
  else return false;
};

void FSJ2LinearMaterialLawSecondOrder::initIPVariable(IPVariable* ipv, bool stiff){
  FSJ2LinearMaterialLaw::initIPVariable(ipv,stiff);
  if (_secondElasticLaw){
    FSJ2LinearIPVariableSecondOrder* hoipv = dynamic_cast<FSJ2LinearIPVariableSecondOrder*>(ipv);
    const STensor33& G = hoipv->getConstRefToGradientOfDeformationGradient();
    STensor33& Q = hoipv->getRefToSecondOrderStress();
    STensor63& J = hoipv->getRefToSecondOrderTangent();
    _secondElasticLaw->constitutive(G,Q,J,stiff);
	}
};
