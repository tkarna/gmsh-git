#ifndef ELASTICITYPBCSOLVER_H_
#define ELASTICITYPBCSOLVER_H_

#include "elasticitySolver.h"
#include "periodicBC.h"
#include "eigenSolver.h"
#include "STensor3.h"

class elasticityPBCSolver : public elasticitySolver{
	public:
		enum whichMethod{CEM, LIM, CSIM};
		enum whichSolver{Petsc, Gmm, Taucs};

  protected:
    std::vector<periodicBC> allPeriodic;
    whichMethod wm;
    whichSolver ws;
    bool eigSolver;
		eigenSolver* pEig;
		double RVEVolume;
	//	fullVector<double> _sigM;
	//	fullMatrix<double> _tangentM;

  protected:
    virtual void applyPeriodicBC(dofManager<double>* p);
    virtual void assemble(linearSystem<double> *lsys);
		virtual void getKeysFromVertex(FunctionSpaceBase* sp, MVertex* v, std::vector<Dof>& vk);
		virtual int getDeformationTensor(fullMatrix<double>& eps);

  public:
    elasticityPBCSolver(int tag);
    elasticityPBCSolver(GModel *model, int tag);
    virtual ~elasticityPBCSolver();

    virtual void solve();
    virtual void setSolver(std::string sol);

		virtual void setRVEVolume(double v);
    virtual void addPeriodicBC(int g1, int g2, int g3, int g4, int g5=0, int g6=0);
    virtual void setImposeMethod(int method);
    virtual void setImposeDegree(int degree);
    virtual void setImposeDeformation(int i, int j, double val);

    virtual void setDeformationTensor(fullMatrix<double> eps);

    virtual void displacementView(std::string filename);
    virtual void stressView(std::string filename);

    virtual void calculateAverageStress(fullMatrix<double>& sigM);
    virtual void averageStress();

    virtual void averageTangentByNumericalPerturbation(	double perturb = 1.e-6);
    virtual void averageTangentByStaticCondension();
    virtual void getBoundaryGroupVertex(std::set<MVertex*>& bvertex);
};


#endif // ELASTICITYPBCSOLVER_H_
