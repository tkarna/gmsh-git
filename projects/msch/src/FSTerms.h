//
// C++ Interface: terms
//
// Description: Elementary matrix terms for finite strain problem
//
//
// Author: , (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef FINITESTRAINTERMS_H
#define FINITESTRAINTERMS_H
#include "SVector3.h"
#include <string>
#include "FSIPVariable.h"
#include "simpleFunction.h"
#include "unknownField.h"
#include "ipField.h"
#include "terms.h"
#include "nlsFunctionSpace.h"
template<class T1,class T2> class FSBilinearTerm : public BiNonLinearTermBase
{
 protected :
  FunctionSpace<T1>& space1;
  FunctionSpace<T2>& space2;
 public :
  FSBilinearTerm(FunctionSpace<T1>& space1_,FunctionSpace<T2>& space2_) : space1(space1_),space2(space2_) {};
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const{};
  virtual ~FSBilinearTerm() {};
};

template<class T1> class FSLinearTerm : public nonLinearTermBase<double>{
 protected :
  FunctionSpace<T1>& space1;
 public :
  FSLinearTerm(FunctionSpace<T1>& space1_) : space1(space1_){};
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &vv) const{};
  virtual ~FSLinearTerm() {}
};

class FSInternalForceTerm : public FSLinearTerm<double>{
  protected :
    const materialLaw *_mlaw;
    const IPField *_ipf;
    mutable const ipFiniteStrain *vipv[256];
    mutable const ipFiniteStrain* vipvprev[256];

  public :
    FSInternalForceTerm(FunctionSpace<double>& space1_,const materialLaw* mlaw, const IPField* ipf)
                                  :FSLinearTerm<double>(space1_),_ipf(ipf),_mlaw(mlaw){
    };
    virtual ~FSInternalForceTerm(){}
    virtual void get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const;
    virtual const bool isData() const {return false;};
    virtual void set(const fullVector<double> *datafield){};
    virtual LinearTermBase<double>* clone () const{
      return new FSInternalForceTerm(space1,_mlaw,_ipf);
    };
}; // FiniteStrainInternalForceTerm


class FSStiffnessTerm : public FSBilinearTerm<double,double>{
  protected :
    const materialLaw *_mlaw;
    const IPField *_ipf;
    bool sym;

    mutable const ipFiniteStrain *vipv[256];
    mutable const ipFiniteStrain* vipvprev[256];
  public :
    FSStiffnessTerm(FunctionSpace<double>& space1_,FunctionSpace<double>& space2_,const materialLaw* mlaw, const IPField* ipf):
                          FSBilinearTerm<double,double>(space1_,space2_),_ipf(ipf),_mlaw(mlaw){
      sym=(&space1_==&space2_);
    };
    FSStiffnessTerm(FunctionSpace<double>& space1_, const materialLaw* mlaw, const IPField* ipf) :
                          FSBilinearTerm<double,double>(space1_,space1_),_ipf(ipf),_mlaw(mlaw){
      sym=true;
    }
    virtual ~FSStiffnessTerm(){}
    virtual void get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const;
    virtual const bool isData() const {return false;} //bareps appears in internal forces, so should be perturbated
    virtual void set(const fullVector<double> *datafield) {};
    virtual BilinearTermBase* clone () const{
      return new FSStiffnessTerm(space1,_mlaw,_ipf);
    }
}; // StiffFiniteStrainTerm

template<class T>
class FSLoadTerm : public LinearTerm<T>{
  protected:
    simpleFunction<typename TensorialTraits<T>::ValType> &Load;
  public :
    FSLoadTerm(FunctionSpace<T>& space1_, simpleFunction<typename TensorialTraits<T>::ValType> &Load_) :
        LinearTerm<T>(space1_), Load(Load_) {};
    virtual ~FSLoadTerm() {};
    virtual void get(MElement *ele, int npts, IntPt *GP, fullVector<double> &m) const{
      if(ele->getParent()) ele = ele->getParent();
      int nbFF = this->space1.getNumKeys(ele);
      double jac[3][3];
      m.resize(nbFF);
      m.scale(0.);
      // Get values at all Gauss points
      nlsFunctionSpaceXYZ<double>* sp1 = static_cast<nlsFunctionSpaceXYZ<double>*>(&(this->space1));
      std::vector<std::vector<TensorialTraits<double>::ValType> >vvals;
      std::vector<std::vector<TensorialTraits<double>::GradType> >vgradsuvw;
      sp1->get(ele,npts,GP,vvals);
      sp1->getgradfuvw(ele,npts,GP,vgradsuvw);
      for(int i = 0; i < npts; i++){
        const double weight = GP[i].weight;
        const double detJ = ele->getJacobian(vgradsuvw[i],jac);
        std::vector<typename TensorialTraits<T>::ValType> &Vals = vvals[i];
        SPoint3 p;
        ele->pnt(Vals, p);
        typename TensorialTraits<T>::ValType load = Load(p.x(), p.y(), p.z());
        for(int j = 0; j < nbFF ; ++j){
          m(j) += dot(Vals[j], load) * weight * detJ;
        };
      };
    };
    virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &vv) const {};
    virtual LinearTermBase<double>* clone () const {
      return new FSLoadTerm<T>(this->space1,Load);
    };
};

#endif // FINITESTRAINTERMS_H
