//
// C++ Interface: ipvariable
//
// Description: Class with definition of ipvariable for finite strain problem
//
//
// Author: V.D. NGUYEN 2011
//
// Copyright: See COPYING file that comes with this distribution

#ifndef FINITESTRAINIPVARIABLE_H_
#define FINITESTRAINIPVARIABLE_H_
#include "ipvariable.h"
#include "fullMatrix.h"
#include "STensor3.h"
#include "STensor43.h"
#include "mlawJ2linear.h"
#include "ipFiniteStrain.h"
#include "ipAnisotropic.h"

class FSIPVariable : public ipFiniteStrain{
  private:
    STensor3 deformationGradient; // deformation gradient
    STensor3 firstPiolaKirchhoffStress; // first Piola Kirfchoff stress
    STensor43 tangentModuli; // tangent moduli

  public:
    FSIPVariable();
    FSIPVariable(const FSIPVariable& source);
    virtual FSIPVariable& operator = (const IPVariable& source);
    virtual ~FSIPVariable();

    virtual double get(const int i) const;
    virtual double defoEnergy() const;
    virtual double plasticEnergy() const;
    virtual double vonMises() const;
    virtual void getCauchyStress(STensor3 &cauchy) const;
    virtual void getStrain(STensor3 &strain) const;
    virtual double getJ() const;
    virtual double equivalentStrain() const;

    virtual const STensor3 &getConstRefToDeformationGradient() const;
    virtual STensor3 &getRefToDeformationGradient();

    virtual const STensor3 &getConstRefToFirstPiolaKirchhoffStress() const;
    virtual STensor3 &getRefToFirstPiolaKirchhoffStress();

    virtual const STensor43 &getConstRefToTangentModuli() const;
    virtual STensor43 &getRefToTangentModuli();
    virtual IPVariable* clone() const {return new FSIPVariable(*this);};
};

class LinearElasticIPVariable : public FSIPVariable{
  public:
    LinearElasticIPVariable();
    LinearElasticIPVariable(const LinearElasticIPVariable& src);
    virtual LinearElasticIPVariable& operator = (const IPVariable& src);
    virtual ~LinearElasticIPVariable();

    virtual void getCauchyStress(STensor3 &cauchy) const;
    virtual void getStrain(STensor3 &strain) const;
    virtual IPVariable* clone() {return new LinearElasticIPVariable(*this);};
};

class FSJ2LinearIPVariable : public FSIPVariable{
	protected:
		IPJ2linear* _j2ipv;

	public:
		FSJ2LinearIPVariable(const mlawJ2linear& j2law);
		FSJ2LinearIPVariable(const FSJ2LinearIPVariable& src);
		virtual FSJ2LinearIPVariable& operator = (const IPVariable& src);
		virtual ~FSJ2LinearIPVariable();
		IPJ2linear* getIPJ2linear(){return _j2ipv;}
		const IPJ2linear* getIPJ2linear() const{return _j2ipv;}
		virtual double get(const int i) const;
		virtual double plasticEnergy() const;
		virtual IPVariable* clone() {return new FSJ2LinearIPVariable(*this);};
};

class AnisotropicFSIPVariable : public FSIPVariable // or store data in a different way
{
 protected:
  IPAnisotropic _tiipv;
 public:
  AnisotropicFSIPVariable();
  AnisotropicFSIPVariable(const AnisotropicFSIPVariable &source);
  AnisotropicFSIPVariable& operator=(const IPVariable &source);

 /* specific function */
  IPAnisotropic* getIPAnisotropic(){return &_tiipv;}
  const IPAnisotropic* getIPAnisotropic() const{return &_tiipv;}
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual IPVariable* clone() {return new AnisotropicFSIPVariable(*this);};

};

#include "ipMultiscale.h"

class FSMultiscaleIPVariable : public FSIPVariable,
                               public IPMultiscale{
  public:
    FSMultiscaleIPVariable(nonLinearMechSolver* solver)
                              : FSIPVariable(),IPMultiscale(solver){};
    FSMultiscaleIPVariable(const FSMultiscaleIPVariable& src):
                              FSIPVariable(src),IPMultiscale(src){};
    virtual FSMultiscaleIPVariable& operator=(const IPVariable& src){
      FSIPVariable::operator=(src);
      IPMultiscale::operator=(*(dynamic_cast<const IPMultiscale*>(&src)));
      return *this;
    }

    virtual ~FSMultiscaleIPVariable(){};
    virtual IPVariable* clone() {return new FSMultiscaleIPVariable(*this);};
};
#endif // FINITESTRAINIPVARIABLE_H_
