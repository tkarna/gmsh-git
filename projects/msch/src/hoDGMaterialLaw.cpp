
#include "hoDGMaterialLaw.h"
#include "hoDGIPVariable.h"
#include "terms.h"
#include "MInterfaceElement.h"
#include "nonLinearMechSolver.h"
#include "FSDomain.h"

void hoDGElasticMaterialLaw::fillElasticStiffness(elasticLawBase* firstlaw, STensor43 &tangent) const{
  if (firstlaw == NULL or dynamic_cast<elasticLaw*>(firstlaw)==NULL)
    Msg::Fatal("elastic law must be used");

  double E = firstlaw->getParameter("YOUNG_MODULUS");
  double nu = firstlaw->getParameter("POISSON_RATIO");
  dG3DMaterialLaw::fillElasticStiffness(E,nu,tangent);
};
void hoDGElasticMaterialLaw::fillElasticStiffness(elasticLawBase* secondlaw, STensor63& secondsecond){
  if (secondlaw == NULL or dynamic_cast<elasticSecondOrderLaw*>(secondlaw) == NULL)
    Msg::Fatal("second elastic law must be used");

  elasticSecondOrderLaw* law = dynamic_cast<elasticSecondOrderLaw*>(secondlaw);
  STensor33 G(0), Q(0);
  law->constitutive(G,Q,secondsecond,true);
};


hoDGElasticMaterialLaw::hoDGElasticMaterialLaw(const int num, const double rho, const bool init) : dG3DMaterialLaw(num,rho,init),
                        _elasticFirst(NULL),_elasticSecond(NULL),_typeFirst(elasticLawBase::LINEAR_ELASTIC),
                        _typeSecond(elasticLawBase::LINEAR_ELASTIC_SECOND_ORDER),_filled(false){}


hoDGElasticMaterialLaw::hoDGElasticMaterialLaw(const int num, const int first, const int second,
                           const double k, const double mu, const double rho,
                           const double tol,const bool init)
                           :dG3DMaterialLaw(num,rho,init),_filled(false){

  _typeFirst = (elasticLawBase::ELASTIC_TYPE)first;
  if (_typeFirst == elasticLawBase::LINEAR_ELASTIC){
    _elasticFirst = new linearElasticLaw(k,mu);
		Msg::Info("Linear elastic material is used");
	}
  else if (_typeFirst == elasticLawBase::NEO_HOOKEAN){
    _elasticFirst = new NeoHookeanElasticLaw(k,mu,tol);
		Msg::Info("Neo Hookean elastic material is used");
	}
  else if (_typeFirst == elasticLawBase::BI_LOGARITHMIC){
    _elasticFirst = new biLogarithmicElasticLaw(k,mu);
		Msg::Info("Bilogarithmic elastic material is used");
	}
	else{
		Msg::Error("Elastic law is not used");
    _elasticFirst = NULL;
  }


  _typeSecond = (elasticLawBase::ELASTIC_TYPE)second;
  if (_typeSecond == elasticLawBase::LINEAR_ELASTIC_SECOND_ORDER)
    _elasticSecond = new linearElasticSecondOrderLaw();
  else if (_typeSecond == elasticLawBase::LINEAR_MINDLIN)
    _elasticSecond = new linearElasticMindlinLaw();
  else{
    Msg::Error("Elastic second order law is not used");
    _elasticSecond == NULL;
  }

};

hoDGElasticMaterialLaw::hoDGElasticMaterialLaw(const hoDGElasticMaterialLaw& src) :dG3DMaterialLaw(src),
  _typeFirst(src._typeFirst),_typeSecond(src._typeSecond),_filled(src._filled){
  if (src._elasticFirst){
    if (_elasticFirst) delete _elasticFirst;
    _elasticFirst = src._elasticFirst->clone();
  }
  else
    _elasticFirst = NULL;

  if (src._elasticSecond){
    if (_elasticSecond) delete _elasticSecond;
    _elasticSecond = src._elasticSecond->clone();
  }
  else
    _elasticSecond = NULL;
};

hoDGElasticMaterialLaw::~hoDGElasticMaterialLaw(){
  if (_elasticFirst) delete _elasticFirst;
  if (_elasticSecond) delete _elasticSecond;
  _elasticFirst = NULL;
  _elasticSecond = NULL;
};

void hoDGElasticMaterialLaw::setType(const int first, const int second){
  _typeFirst = (elasticLawBase::ELASTIC_TYPE)first;
	if (_elasticFirst and _elasticFirst->getType()!= _typeFirst){
		delete _elasticFirst;
		_elasticFirst == NULL;
	}
	if (_elasticFirst ==NULL){
		 if (_typeFirst == elasticLawBase::LINEAR_ELASTIC){
    	_elasticFirst = new linearElasticLaw();
			Msg::Info("Linear elastic material is used");
		}
  	else if (_typeFirst == elasticLawBase::NEO_HOOKEAN){
    	_elasticFirst = new NeoHookeanElasticLaw();
			Msg::Info("Neo Hookean elastic material is used");
		}
  	else if (_typeFirst == elasticLawBase::BI_LOGARITHMIC){
    	_elasticFirst = new biLogarithmicElasticLaw();
			Msg::Info("Bilogarithmic elastic material is used");
		}
		else{
			Msg::Error("Elastic material law is not used");
			_elasticFirst = NULL;
    }
	}

	_typeSecond = (elasticLawBase::ELASTIC_TYPE)second;
	if (_elasticSecond and  _elasticSecond->getType()!=_typeSecond){
    delete _elasticSecond;
    _elasticSecond= NULL;
	}
	if (_elasticSecond == NULL){
    if (_typeSecond == elasticLawBase::LINEAR_ELASTIC_SECOND_ORDER)
      _elasticSecond = new linearElasticSecondOrderLaw();
    else if (_typeSecond == elasticLawBase::LINEAR_MINDLIN)
      _elasticSecond = new linearElasticMindlinLaw();
    else{
      Msg::Error("Elastic second order law is not used");
      _elasticSecond = NULL;
    }

	}
};
void hoDGElasticMaterialLaw::setParameter(const std::string key, const double val){
  if (_elasticFirst==NULL or _elasticSecond ==NULL)
    this->setType(_typeFirst,_typeSecond);
  if (_elasticFirst)
    _elasticFirst->setParameter(key,val);
	if (_elasticSecond)
    _elasticSecond->setParameter(key,val);
};

double hoDGElasticMaterialLaw::soundSpeed() const {
  if (_elasticFirst){
    double K = _elasticFirst->getParameter("BULK_MODULUS");
    double rho = this->density();
    return std::sqrt(K/rho);
  }
  else{
    Msg::Error("first order law is not used");
    return 0.;
  }

};

bool hoDGElasticMaterialLaw::isHighOrder() const {
  if (_elasticSecond)
    return true;
  else
    return false;
};

void hoDGElasticMaterialLaw::createIPState(IPStateBase* &ips,const bool* state_,const MElement *ele, const int nbFF_, const int gpt ) const{
  bool inter = true;
  const MInterfaceElement* ie = dynamic_cast<const MInterfaceElement*>(ele);
  if (ie==NULL) inter = false;
  if (_typeFirst == elasticLawBase::LINEAR_ELASTIC){
    IPVariable *ipvi = new hoDGLinearElasticIPVariable(inter);
    IPVariable *ipv1 = new hoDGLinearElasticIPVariable(inter);
    IPVariable *ipv2 = new hoDGLinearElasticIPVariable(inter);
    if(ips != NULL) delete ips;
    ips = new IP3State(state_,ipvi,ipv1,ipv2);
  }
  else{
    IPVariable *ipvi = new hoDGIPVariable(inter);
    IPVariable *ipv1 = new hoDGIPVariable(inter);
    IPVariable *ipv2 = new hoDGIPVariable(inter);
    if(ips != NULL) delete ips;
    ips = new IP3State(state_,ipvi,ipv1,ipv2);
  }
};

void hoDGElasticMaterialLaw::createIPVariable(IPVariable* &ipv,const MElement *ele, const int nbFF) const{
  bool inter = true;
  const MInterfaceElement* iele  = dynamic_cast<const MInterfaceElement*>(ele);
  if (iele ==NULL) inter = false;
  if (ipv) delete ipv;
  ipv = new hoDGIPVariable(inter);
};

void hoDGElasticMaterialLaw::initLaws(const std::map<int,materialLaw*> &maplaw){
  if (!_filled){
    _filled = true;
    if (_elasticFirst){
      fillElasticStiffness(_elasticFirst,elasticStiffness);
    }
    if (_elasticSecond){
      fillElasticStiffness(_elasticSecond,elasticSecondOrderTangent);
    }
  }
};

void hoDGElasticMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff, const bool checkfrac){

  hoDGIPVariable* hoipv = static_cast<hoDGIPVariable*>(ipv);
  const hoDGIPVariable* hoipvprev = static_cast<const hoDGIPVariable*>(ipvprev);
  const STensor3& F = hoipv->getConstRefToDeformationGradient();
  STensor3& P = hoipv->getRefToFirstPiolaKirchhoffStress();
  STensor43& L = hoipv->getRefToTangentModuli();

  if (_elasticFirst){
    elasticLaw* firstlaw = dynamic_cast<elasticLaw*>(_elasticFirst);
    firstlaw->constitutive(F,P,L,stiff);
  }

  if (isHighOrder()){
    const STensor33& G = hoipv->getConstRefToGradientOfDeformationGradient();
    STensor33& Q = hoipv->getRefToSecondOrderStress();

    STensor53& LG = hoipv->getRefToCrossTangentFirstSecond();
    STensor53& JF = hoipv->getRefToCrossTangentSecondFirst();
    STensor63& J = hoipv->getRefToSecondOrderTangent();

    if (_elasticSecond){
      elasticSecondOrderLaw* secondlaw = dynamic_cast<elasticSecondOrderLaw*>(_elasticSecond);
      secondlaw->constitutive(G,Q,J,stiff);
      LG*=0.;
      JF*=0.;
    }
  };
  hoDGElasticMaterialLaw::setElasticStiffness(ipv);
};

void hoDGElasticMaterialLaw::setElasticStiffness(IPVariable* ipv){
  hoDGIPVariable* hoipv = dynamic_cast<hoDGIPVariable*>(ipv);
  if (_elasticFirst)
    hoipv->setRefToElasticTangentModuli(elasticStiffness);
  if (_elasticSecond){
    hoipv->setRefToSecondOrderElasticTangentModuli(elasticSecondOrderTangent);
    hoipv->setRefToCrossFirstSecondElasticTangentModuli(elasticFirstSecondTangent);
    hoipv->setRefToCrossSecondFirstElasticTangentModuli(elasticSecondFirstTangent);
  }
};

fullHODGElasticMaterialLaw::fullHODGElasticMaterialLaw(const int num, const double rho, const bool init)
                    :hoDGElasticMaterialLaw(num,rho,init),_elasticFirstSecond(NULL){};
fullHODGElasticMaterialLaw::fullHODGElasticMaterialLaw(const int num, const int first, const int second,
                       const double K, const double mu, const double rho, const double tol,
                      const bool init)
                      : hoDGElasticMaterialLaw(num,first,second,K,mu,rho,tol,init),_elasticFirstSecond(NULL){};

fullHODGElasticMaterialLaw::fullHODGElasticMaterialLaw(const fullHODGElasticMaterialLaw& src): hoDGElasticMaterialLaw(src){
  if (src._elasticFirstSecond){
    this->_elasticFirstSecond = src._elasticFirstSecond->clone();
  }
  else
    _elasticFirstSecond = NULL;
};

fullHODGElasticMaterialLaw::~fullHODGElasticMaterialLaw(){
  if (_elasticFirstSecond) delete _elasticFirstSecond;
  _elasticFirstSecond = NULL;
}

void fullHODGElasticMaterialLaw::setParameter(const std::string key, const double val){
  hoDGElasticMaterialLaw::setParameter(key,val);
  if (_elasticFirstSecond == NULL){
    _elasticFirstSecond = new linearElasticFirstSecondLaw();
  }
  _elasticFirstSecond->setParameter(key,val);
};

void fullHODGElasticMaterialLaw::fillElasticStiffness(elasticLawBase* fs, STensor53& firstsecond, STensor53& secondfirst){
  STensor3 F(0.), P(0.);
  STensor33 G(0.), Q(0.);
  elasticFirstSecondLaw* pfs = dynamic_cast<linearElasticFirstSecondLaw*>(fs);
  pfs->constitutive(F,G,P,Q,firstsecond,secondfirst,true);
};

void fullHODGElasticMaterialLaw::initLaws(const std::map<int,materialLaw*> &maplaw){
  if (!_filled){
    _filled = true;
    if (_elasticFirst){
      hoDGElasticMaterialLaw::fillElasticStiffness(_elasticFirst,elasticStiffness);
    }
    if (_elasticSecond){
      hoDGElasticMaterialLaw::fillElasticStiffness(_elasticSecond,elasticSecondOrderTangent);
    }
    if (_elasticFirstSecond){
      fillElasticStiffness(_elasticFirstSecond,elasticFirstSecondTangent,elasticSecondFirstTangent);
    }
  }

};

void fullHODGElasticMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff, const bool checkfrac){

  hoDGIPVariable* hoipv = static_cast<hoDGIPVariable*>(ipv);
  const hoDGIPVariable* hoipvprev = static_cast<const hoDGIPVariable*>(ipvprev);
  const STensor3& F = hoipv->getConstRefToDeformationGradient();
  STensor3& P = hoipv->getRefToFirstPiolaKirchhoffStress();
  STensor43& L = hoipv->getRefToTangentModuli();

  if (_elasticFirst){
    elasticLaw* firstlaw = dynamic_cast<elasticLaw*>(_elasticFirst);
    firstlaw->constitutive(F,P,L,stiff);
  }

  if (isHighOrder()){
    const STensor33& G = hoipv->getConstRefToGradientOfDeformationGradient();
    STensor33& Q = hoipv->getRefToSecondOrderStress();

    STensor53& LG = hoipv->getRefToCrossTangentFirstSecond();
    STensor53& JF = hoipv->getRefToCrossTangentSecondFirst();
    STensor63& J = hoipv->getRefToSecondOrderTangent();

    if (_elasticSecond){
      elasticSecondOrderLaw* secondlaw = dynamic_cast<elasticSecondOrderLaw*>(_elasticSecond);
      secondlaw->constitutive(G,Q,J,stiff);
    }


    if (_elasticFirstSecond){
      STensor3 P1(0.);
      STensor33 Q1(0.);
      elasticFirstSecondLaw* fs = dynamic_cast<elasticFirstSecondLaw*>(_elasticFirstSecond);
      fs->constitutive(F,G,P1,Q1,LG,JF,stiff);
      P+= P1;
      Q+= Q1;
    }
  };
  hoDGElasticMaterialLaw::setElasticStiffness(ipv);
};



hoDGMultiscaleMaterialLaw::hoDGMultiscaleMaterialLaw(const int lnum, const int tag, const bool init)
                            : dG3DMaterialLaw(lnum,0,init),numericalMaterial(tag),_filled(false){};

void hoDGMultiscaleMaterialLaw::initLaws(const std::map<int,materialLaw*> &maplaw){
  if (!_filled){
    this->fillElasticStiffness(_rho,elasticStiffness, elasticFirstSecondTangent,
                           elasticSecondFirstTangent,elasticSecondOrderTangent);
    _filled = true;
  }
};

bool hoDGMultiscaleMaterialLaw::isHighOrder() const {
  if (_order ==1){
    return false;
  }
  else if (_order==2){
    return true;
  }
  else{
    Msg::Fatal("The homogenization order is not correctly defined");
  }
};

void hoDGMultiscaleMaterialLaw::fillElasticStiffness(double & rho, STensor43& LF, STensor53& LG, STensor53& JF, STensor63& JG){
  printf("get elastic properties\n");
  bool temp = this->_tangentFlag;

  if (this->_tangentFlag == false){
    Msg::Warning("active tangent averaging to find the elastic stiffnes");
    this->_tangentFlag = true;
  }
  this->assignMeshId(0,0);
  nonLinearMechSolver* solver = this->createMicroSolver(0,0);

  solver->getTangent(LF);
  if (_order == 2){
    solver->getHighOrderCrossFirstSecondTangent(LG);
    solver->getHighOrderCrossSecondFirstTangent(JF);
    solver->getHighOrderTangent(JG);
  };
  rho = solver->getHomogenizedDensity();

  this->_tangentFlag = temp;

  delete solver;
}

void hoDGMultiscaleMaterialLaw::createIPState(IPStateBase* &ips,const bool* state_,const MElement *ele, const int nbFF_, const int gpt) const{
  int el = ele->getNum();
	nonLinearMechSolver* solver = this->createMicroSolver(el,gpt);
  bool oninter = true;
  const MInterfaceElement* iele = dynamic_cast<const MInterfaceElement*>(ele);
  if (iele == NULL) oninter = false;
	IPVariable* ipvi = new hoDGMultiscaleIPVariable(oninter,solver);
	IPVariable* ipv1 = new hoDGMultiscaleIPVariable(oninter,solver);
	IPVariable* ipv2 = new hoDGMultiscaleIPVariable(oninter,solver);
	if(ips != NULL) delete ips;
	ips = new IP3StateMultiscale(state_,ipvi,ipv1,ipv2,solver);
};

void hoDGMultiscaleMaterialLaw::createIPState(const bool isSolve, IPStateBase* &ips,const bool* state,
                                 const MElement *ele, const int nbFF, const int gpt) const{
  if (isSolve)
    hoDGMultiscaleMaterialLaw::createIPState(ips,state,ele,nbFF,gpt);
  else{
    nonLinearMechSolver* solver = NULL;
    bool oninter = false;
    IPVariable* ipvi = new hoDGMultiscaleIPVariable(oninter,solver);
    IPVariable* ipv1 = new hoDGMultiscaleIPVariable(oninter,solver);
    IPVariable* ipv2 = new hoDGMultiscaleIPVariable(oninter,solver);
    if(ips != NULL) delete ips;
    ips = new IP3StateMultiscale(state,ipvi,ipv1,ipv2,solver);
  }
};

void hoDGMultiscaleMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvprev, const bool stiff, const bool checkfrac){
  hoDGMultiscaleIPVariable* mipv = static_cast<hoDGMultiscaleIPVariable*>(ipv);
  const hoDGMultiscaleIPVariable* mipvprev = static_cast<const hoDGMultiscaleIPVariable*>(ipvprev);
  nonLinearMechSolver* solver = mipv->getSolver();
  // set kinematics input
  const STensor3& F = mipv->getConstRefToDeformationGradient();
  solver->setDeformationGradient(F);
  if (_order ==2){
    const STensor33& G = mipv->getConstRefToGradientOfDeformationGradient();
    solver->setGradientOfDeformationGradient(G);
  };
  // set tangent averaging flag
  solver->tangentAveragingFlag(stiff);
  // solve micro problem
  double time = solver->microSolve();
  // get homogenized properties form solved micro problem
  solver->getStress(mipv->getRefToFirstPiolaKirchhoffStress());
  if (stiff)
    solver->getTangent(mipv->getRefToTangentModuli());

  mipv->setRefToElasticTangentModuli(elasticStiffness);

  if (_order ==2){
    solver->getHighOrderStress(mipv->getRefToSecondOrderStress());
    if (stiff){
      solver->getHighOrderCrossFirstSecondTangent(mipv->getRefToCrossTangentFirstSecond());
      solver->getHighOrderCrossSecondFirstTangent(mipv->getRefToCrossTangentSecondFirst());
      solver->getHighOrderTangent(mipv->getRefToSecondOrderTangent());
    }

    mipv->setRefToCrossFirstSecondElasticTangentModuli(elasticFirstSecondTangent);
    mipv->setRefToCrossSecondFirstElasticTangentModuli(elasticSecondFirstTangent);
    mipv->setRefToSecondOrderElasticTangentModuli(elasticSecondOrderTangent);
  }
};

void hoDGMultiscaleMaterialLaw::setElasticStiffness(IPVariable* ipv){
  dG3DIPVariable* dGipv =dynamic_cast<dG3DIPVariable*>(ipv);
  if (dGipv)
    dGipv->setRefToElasticTangentModuli(elasticStiffness);

  hoDGIPVariable* hoipv = dynamic_cast<hoDGIPVariable*>(ipv);
  if (hoipv){
    hoipv->setRefToSecondOrderElasticTangentModuli(elasticSecondOrderTangent);
    hoipv->setRefToCrossFirstSecondElasticTangentModuli(elasticFirstSecondTangent);
    hoipv->setRefToCrossSecondFirstElasticTangentModuli(elasticSecondFirstTangent);
  }
};

