
#include "FSIPVariableSecondOrder.h"

FSIPVariableSecondOrderBase::FSIPVariableSecondOrderBase():gradientOfDeformationGradient(0.),secondOrderStress(0.),secondOrderTangent(0.){}
FSIPVariableSecondOrderBase::FSIPVariableSecondOrderBase(const FSIPVariableSecondOrderBase& src){
  gradientOfDeformationGradient = src.gradientOfDeformationGradient;
  secondOrderStress = src.secondOrderStress;
  secondOrderTangent = src.secondOrderTangent;
}
FSIPVariableSecondOrderBase& FSIPVariableSecondOrderBase::operator = (const FSIPVariableSecondOrderBase& src){
  gradientOfDeformationGradient = src.gradientOfDeformationGradient;
  secondOrderStress = src.secondOrderStress;
  secondOrderTangent = src.secondOrderTangent;
  return *this;
}

double FSIPVariableSecondOrderBase::getIPComp(const int comp) const{
  const STensor33& G = getConstRefToGradientOfDeformationGradient();
  const STensor33& Q = getConstRefToSecondOrderStress();

  if (comp == IPField::HIGHSTRAIN_NORM){
    return sqrt(dot(G,G));
  }
  else if (comp == IPField::HIGHSTRESS_NORM){
    return sqrt(dot(Q,Q));
  }
  else if (comp == IPField::G_XXX){
    return G(0,0,0);
	}
	else if (comp == IPField::G_XXY){
    return G(0,0,1);
	}
	else if (comp == IPField::G_XXZ){
    return G(0,0,2);
	}
	else if (comp == IPField::G_XYX){
    return G(0,1,0);
	}
	else if (comp == IPField::G_XYY){
    return G(0,1,1);
	}
	else if (comp == IPField::G_XYZ){
    return G(0,1,2);
	}
	else if (comp == IPField::G_XZX){
    return G(0,2,0);
	}
	else if (comp == IPField::G_XZY){
    return G(0,2,1);
	}
	else if (comp == IPField::G_XZZ){
    return G(0,2,2);
	}
	else if (comp == IPField::G_YXX){
    return G(1,0,0);
	}
	else if (comp == IPField::G_YXY){
    return G(1,0,1);
	}
	else if (comp == IPField::G_YXZ){
    return G(1,0,2);
	}
	else if (comp == IPField::G_YYX){
    return G(1,1,0);
	}
	else if (comp == IPField::G_YYY){
    return G(1,1,1);
	}
	else if (comp == IPField::G_YYZ){
    return G(1,1,2);
	}
	else if (comp == IPField::G_YZX){
    return G(1,2,0);
	}
	else if (comp == IPField::G_YZY){
    return G(1,2,1);
	}
	else if (comp == IPField::G_YZZ){
    return G(1,2,2);
	}
	else if (comp == IPField::G_ZXX){
    return G(2,0,0);
	}
	else if (comp == IPField::G_ZXY){
    return G(2,0,1);
	}
	else if (comp == IPField::G_ZXZ){
    return G(2,0,2);
	}
	else if (comp == IPField::G_ZYX){
    return G(2,1,0);
	}
	else if (comp == IPField::G_ZYY){
    return G(2,1,1);
	}
	else if (comp == IPField::G_ZYZ){
    return G(2,1,2);
	}
	else if (comp == IPField::G_ZZX){
    return G(2,2,0);
	}
	else if (comp == IPField::G_ZZY){
    return G(2,2,1);
	}
	else if (comp == IPField::G_ZZZ){
    return G(2,2,2);
	}
	else if (comp == IPField::Q_XXX){
    return Q(0,0,0);
	}
	else if (comp == IPField::Q_XXY){
    return Q(0,0,1);
	}
	else if (comp == IPField::Q_XXZ){
    return Q(0,0,2);
	}
	else if (comp == IPField::Q_XYX){
    return Q(0,1,0);
	}
	else if (comp == IPField::Q_XYY){
    return Q(0,1,1);
	}
	else if (comp == IPField::Q_XYZ){
    return Q(0,1,2);
	}
	else if (comp == IPField::Q_XZX){
    return Q(0,2,0);
	}
	else if (comp == IPField::Q_XZY){
    return Q(0,2,1);
	}
	else if (comp == IPField::Q_XZZ){
    return Q(0,2,2);
	}
	else if (comp == IPField::Q_YXX){
    return Q(1,0,0);
	}
	else if (comp == IPField::Q_YXY){
    return Q(1,0,1);
	}
	else if (comp == IPField::Q_YXZ){
    return Q(1,0,2);
	}
	else if (comp == IPField::Q_YYX){
    return Q(1,1,0);
	}
	else if (comp == IPField::Q_YYY){
    return Q(1,1,1);
	}
	else if (comp == IPField::Q_YYZ){
    return Q(1,1,2);
	}
	else if (comp == IPField::G_YZX){
    return Q(1,2,0);
	}
	else if (comp == IPField::Q_YZY){
    return Q(1,2,1);
	}
	else if (comp == IPField::Q_YZZ){
    return Q(1,2,2);
	}
	else if (comp == IPField::Q_ZXX){
    return Q(2,0,0);
	}
	else if (comp == IPField::Q_ZXY){
    return Q(2,0,1);
	}
	else if (comp == IPField::Q_ZXZ){
    return Q(2,0,2);
	}
	else if (comp == IPField::Q_ZYX){
    return Q(2,1,0);
	}
	else if (comp == IPField::Q_ZYY){
    return Q(2,1,1);
	}
	else if (comp == IPField::Q_ZYZ){
    return Q(2,1,2);
	}
	else if (comp == IPField::Q_ZZX){
    return Q(2,2,0);
	}
	else if (comp == IPField::Q_ZZY){
    return Q(2,2,1);
	}
	else if (comp == IPField::Q_ZZZ){
    return Q(2,2,2);
	}
	else
    0.;
};

LinearElasticIPVariableSecondOrder::LinearElasticIPVariableSecondOrder(): LinearElasticIPVariable(),FSIPVariableSecondOrderBase(){};
LinearElasticIPVariableSecondOrder::LinearElasticIPVariableSecondOrder(const LinearElasticIPVariableSecondOrder& src) :
                  LinearElasticIPVariable(src),FSIPVariableSecondOrderBase(src){};
LinearElasticIPVariableSecondOrder& LinearElasticIPVariableSecondOrder::operator = (const IPVariable& src){
  LinearElasticIPVariable::operator=(src);
  const FSIPVariableSecondOrderBase* ip = dynamic_cast<const FSIPVariableSecondOrderBase*>(&src);
  if (ip){
    FSIPVariableSecondOrderBase::operator=(*ip);
  }
  return *this;
};
LinearElasticIPVariableSecondOrder::~LinearElasticIPVariableSecondOrder(){};
double LinearElasticIPVariableSecondOrder::get(const int comp) const{
  double val = LinearElasticIPVariable::get(comp);
  if (val != 0.)
    return val;
  else
    return getIPComp(comp);
};


FSIPVariableSecondOrder::FSIPVariableSecondOrder() : FSIPVariable(),FSIPVariableSecondOrderBase(){};
FSIPVariableSecondOrder::FSIPVariableSecondOrder(const FSIPVariableSecondOrder& src):
    FSIPVariable(src),FSIPVariableSecondOrderBase(src){};
FSIPVariableSecondOrder& FSIPVariableSecondOrder::operator = (const IPVariable& src){
  FSIPVariable::operator=(src);
  const FSIPVariableSecondOrderBase* ip = dynamic_cast<const FSIPVariableSecondOrderBase*>(&src);
  if (ip){
    FSIPVariableSecondOrderBase::operator=(*ip);
  }
  return *this;
};
FSIPVariableSecondOrder::~FSIPVariableSecondOrder(){};
double FSIPVariableSecondOrder::get(const int comp) const{
  double val = FSIPVariable::get(comp);
  if (val != 0.)
    return val;
  else
    return getIPComp(comp);
};


FSJ2LinearIPVariableSecondOrder::FSJ2LinearIPVariableSecondOrder(const mlawJ2linear& j2law): FSIPVariableSecondOrderBase(), FSJ2LinearIPVariable(j2law){}
FSJ2LinearIPVariableSecondOrder::FSJ2LinearIPVariableSecondOrder(const FSJ2LinearIPVariableSecondOrder& src):
                        FSJ2LinearIPVariable(src), FSIPVariableSecondOrderBase(src){};
FSJ2LinearIPVariableSecondOrder& FSJ2LinearIPVariableSecondOrder::operator = (const IPVariable& src){
  FSJ2LinearIPVariable::operator=(src);
  const FSIPVariableSecondOrderBase* ip = dynamic_cast<const FSIPVariableSecondOrderBase*>(&src);
  if (ip){
    FSIPVariableSecondOrderBase::operator=(*ip);
  }
  return *this;
};

FSJ2LinearIPVariableSecondOrder::~FSJ2LinearIPVariableSecondOrder(){};

double FSJ2LinearIPVariableSecondOrder::get(const int comp) const{
  double val = FSJ2LinearIPVariable::get(comp);
  if (val != 0.)
    return val;
  else
    return getIPComp(comp);
};
