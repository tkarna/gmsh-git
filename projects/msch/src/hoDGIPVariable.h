#ifndef HODGIPVARIABLE_H_
#define HODGIPVARIABLE_H_

#include "dG3DIPVariable.h"
#include "STensor53.h"
#include "STensor63.h"


class hoDGIPVariable : public dG3DIPVariable{
  private:
    STensor43 thirdOrderDeformationGradient;
    STensor33 gradientOfDeformationGradient;
    STensor33 secondOrderStress;
    STensor63 secondOrderTangent;
    STensor53 crossTangentFirstSecond ,crossTangentSecondFirst;
    const STensor63 *secondOrderElasticTangentModuli;
    const STensor53 *crossFirstSecondElasticTangentModuli;
    const STensor53 *crossSecondFirstElasticTangentModuli;

  public:
    hoDGIPVariable(bool oninter = false);
    hoDGIPVariable(const hoDGIPVariable& src);
    hoDGIPVariable& operator= (const IPVariable& src);
    virtual ~hoDGIPVariable(){};
    // Archiving data
    virtual double get(const int i) const;
    virtual double VMEquivalentStrain(STensor3& GL) const;
    virtual double defoEnergy() const;
    virtual void getStrain(STensor3& GL) const;

    virtual const STensor53& getConstRefToCrossFirstSecondElasticTangentModuli() const {return *crossFirstSecondElasticTangentModuli;}
    virtual void setRefToCrossFirstSecondElasticTangentModuli(const STensor53& e){crossFirstSecondElasticTangentModuli = &e;}

    virtual const STensor53& getConstRefToCrossSecondFirstElasticTangentModuli() const {return *crossSecondFirstElasticTangentModuli;}
    virtual void setRefToCrossSecondFirstElasticTangentModuli(const STensor53& e){crossSecondFirstElasticTangentModuli = &e;}

    virtual const STensor63 &getConstRefToSecondOrderElasticTangentModuli() const {return *secondOrderElasticTangentModuli;}
    virtual void setRefToSecondOrderElasticTangentModuli(const STensor63 &eT) {secondOrderElasticTangentModuli=&eT;}

    virtual STensor33& getRefToGradientOfDeformationGradient(){return gradientOfDeformationGradient;};
    virtual const STensor33& getConstRefToGradientOfDeformationGradient() const{return gradientOfDeformationGradient;};

    virtual STensor33& getRefToSecondOrderStress(){return secondOrderStress;};
    virtual const STensor33& getConstRefToSecondOrderStress() const{return secondOrderStress;};

    virtual STensor53& getRefToCrossTangentFirstSecond() {return crossTangentFirstSecond;};
    virtual const STensor53& getConstRefToCrossTangentFirstSecond() const{return crossTangentFirstSecond;};

    virtual STensor53& getRefToCrossTangentSecondFirst() {return crossTangentSecondFirst;};
    virtual const STensor53& getConstRefToCrossTangentSecondFirst()const{return crossTangentSecondFirst;};

    virtual STensor63& getRefToSecondOrderTangent(){return secondOrderTangent;};
    virtual const STensor63& getConstRefToSecondOrderTangent()const {return secondOrderTangent;};

    virtual STensor43& getRefToThirdOrderDeformationGradient(){return thirdOrderDeformationGradient;};
    virtual const STensor43& getConstRefToThirdOrderDeformationGradient() const{return thirdOrderDeformationGradient;};

    #if defined(HAVE_MPI)
    // using in multiscale analysis with MPI
    // get number of values to set the kinematics of microscopic problem
    virtual int getMacroNumberElementDataSendToMicroProblem() const;
    // get number of values obtained by microscopic analysis to send to macroscopic analysis
    virtual int getMicroNumberElementDataSendToMacroProblem() const ;
    // get macroscopic kinematic data to send to microscopic problem
    virtual void getMacroDataSendToMicroProblem(double* val) const;
    // get computed data obtaind by microscopic analysis to send to macroscopic analysis
    virtual void getMicroDataToMacroProblem(double* val) const ;
    // set the received data from microscopic analysis to microscopic analysis
    virtual void setReceivedMacroDataToMicroProblem(const double* val);
    // set the received data from microscopic analysis to macroscopic analysis
    virtual void setReceivedMicroDataToMacroProblem(const double* val);
    #endif
};

class hoDGLinearElasticIPVariable : public hoDGIPVariable{
  public:
    hoDGLinearElasticIPVariable(bool oninter = false) : hoDGIPVariable(oninter){};
    hoDGLinearElasticIPVariable(const hoDGLinearElasticIPVariable& src):hoDGIPVariable(src){};
    hoDGLinearElasticIPVariable& operator= (const IPVariable& src){
      hoDGIPVariable::operator=(src);
      return *this;
    };
    virtual void getCauchyStress(STensor3 &cauchy) const;
    virtual ~hoDGLinearElasticIPVariable(){};
    virtual void getStrain(STensor3& GL) const;

};

#include "ipMultiscale.h"

class hoDGMultiscaleIPVariable : public hoDGIPVariable,
                                 public IPMultiscale{
  public:
    hoDGMultiscaleIPVariable(bool oninter, nonLinearMechSolver* solver)
                              : hoDGIPVariable(oninter),IPMultiscale(solver){};
    hoDGMultiscaleIPVariable(const hoDGMultiscaleIPVariable& src):
                              hoDGIPVariable(src),IPMultiscale(src){};
    hoDGMultiscaleIPVariable& operator=(const IPVariable& src){
      hoDGIPVariable::operator=(src);
      IPMultiscale::operator=(*(dynamic_cast<const IPMultiscale*>(&src)));
      return *this;
    }
    virtual ~hoDGMultiscaleIPVariable(){};
};

#endif // HODGIPVARIABLE_H_
