#ifndef FSMATERIALLAWSECONDORDER_H_
#define FSMATERIALLAWSECONDORDER_H_

#include "FSMaterialLaw.h"
#include "elasticLaw.h"

class FSElasticMaterialLawSecondOrder : public FSElasticMaterialLaw{
  #ifndef SWIG
  protected:
    elasticSecondOrderLaw* _secondElasticLaw;
    elasticLawBase::ELASTIC_TYPE _typeSecond;
  #endif

  public:
    FSElasticMaterialLawSecondOrder(const int num, const double rho, const bool init = true);
		FSElasticMaterialLawSecondOrder(const int num, const int first, const int second,
                                    const double rho,const bool init = true);
    FSElasticMaterialLawSecondOrder(const int num, const int first, const int second,
                                    const double k, const double mu,
                                    const double rho, const double tol = 1.e-6,const bool init = true);
    virtual void setType(const int  first, const int second);
    virtual void setType(const int first);
		virtual void setParameter(const std::string key, const double val);
		#ifndef SWIG
		FSElasticMaterialLawSecondOrder(const FSElasticMaterialLawSecondOrder& src);
		virtual ~FSElasticMaterialLawSecondOrder();
    virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt=0) const;
		virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, bool stiff);
		virtual bool isHighOrder() const;
		virtual void initIPVariable(IPVariable* ipv, bool stiff);
		#endif
};

class FSJ2LinearMaterialLawSecondOrder : public FSJ2LinearMaterialLaw {
  #ifndef SWIG
  protected:
    elasticSecondOrderLaw* _secondElasticLaw;
    elasticLawBase::ELASTIC_TYPE _typeSecond;
  #endif

  public:
    FSJ2LinearMaterialLawSecondOrder(const int num, const double E, const double nu, const double sy0, const double h,
													const double rho, const double tol =1.e-6,
													const bool flag = false, const double eps = 1e-8,const bool init = true);

    virtual void setType(const int second);
    virtual void setParameter(const std::string key, const double val);
    #ifndef SWIG
    FSJ2LinearMaterialLawSecondOrder(const FSJ2LinearMaterialLawSecondOrder& src);
    virtual ~FSJ2LinearMaterialLawSecondOrder();

		virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const;
		virtual void stress(IPVariable* ipv, const IPVariable* ipvprev, bool stiff);
		virtual bool isHighOrder() const;
		virtual void initIPVariable(IPVariable* ipv, bool stiff);
		#endif
};

#endif // FSMATERIALLAWSECONDORDER_H_
