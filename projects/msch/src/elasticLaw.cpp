#include "elasticLaw.h"

void elasticLaw::setParameter(const std::string key, const double val){
  _matData.insert(std::pair<std::string,double>(key,val));
  _isModified = true;
};

double elasticLaw::getParameter(const std::string key) const{
  if (_matData.size()< 2){
    Msg::Fatal("Parameters are not enough for this law");
    return 0;
  }
  else if (!_isComputed or _isModified){
    _isComputed = true;
    _isModified = false;
    std::map<std::string,double>::const_iterator itE = _matData.find("YOUNG_MODULUS");
    std::map<std::string,double>::const_iterator itnu = _matData.find("POISSON_RATIO");
    std::map<std::string,double>::const_iterator itK = _matData.find("BULK_MODULUS");
    std::map<std::string,double>::const_iterator itMu = _matData.find("SHEAR_MODULUS");
    std::map<std::string,double>::const_iterator itLambda = _matData.find("LAMBDA");
    double _E, _nu,_K,_mu,_lambda;

    if (itE != _matData.end() and itnu != _matData.end()){
      _E = itE->second;
      _nu = itnu->second;
      _K = _E/(3.*(1.-2.*_nu));
      _mu = _E/(2.*(1.+_nu));
      _lambda = _E*_nu/(1.+_nu)/(1.-2*_nu);
      _matData["BULK_MODULUS"] = _K;
      _matData["SHEAR_MODULUS"] = _mu;
      _matData["LAMBDA"] = _lambda;
    }
    else if (itE != _matData.end() and itnu != _matData.end()) {
      _E = itE->second;
      _K = itK->second;
      _mu = 3.*_K*_E/(9.*_K-_E);
      _nu = (3.*_K - _E)/(6.*_K);
      _lambda = 3.*_K*(3.*_K - _E)/(9.*_K-_E);

      _matData["SHEAR_MODULUS"] = _mu;
      _matData["LAMBDA"] = _lambda;
      _matData["POISSON_RATIO"] = _nu;
    }
    else if (itE != _matData.end() and itMu != _matData.end()){
      _E = itE->second;
      _mu = itMu->second;
      _K = _E*_mu/(3.*(3.*_mu-_E));
      _lambda = _mu*(_E-2.*_mu)/(3.*_mu-_E);
      _nu = _E/(2.*_mu)-1.;

      _matData["BULK_MODULUS"] = _K;
      _matData["LAMBDA"] = _lambda;
      _matData["POISSON_RATIO"] = _nu;
    }
    else if (itE != _matData.end() and itLambda != _matData.end()){
      _E = itE->second;
      _lambda = itLambda->second;
      _nu = 2./(1.+ sqrt(1.+4.*(2.+_E/_lambda)));
      _K = _E/(3.*(1.-2.*_nu));
      _mu = _E/(2.*(1.+_nu));

      _matData["BULK_MODULUS"] = _K;
      _matData["SHEAR_MODULUS"] = _mu;
      _matData["POISSON_RATIO"] = _nu;
    }
    else if (itnu != _matData.end() and itK != _matData.end()){
      _nu = itnu->second;
      _K = itK->second;
      _E = 3.*_K*(1.-2.*_nu);
      _mu = _E/(2.*(1.+_nu));
      _lambda = _E*_nu/(1.+_nu)/(1.-2*_nu);

      _matData["SHEAR_MODULUS"] = _mu;
      _matData["LAMBDA"] = _lambda;
      _matData["YOUNG_MODULUS"] = _E;
    }
    else if (itnu != _matData.end() and itMu != _matData.end()){
      _nu  = itnu->second;
      _mu = itMu->second;
      _E = 2.*_mu*(1.+_nu);
      _K = _E/(3.*(1.-2.*_nu));
      _lambda = _E*_nu/(1.+_nu)/(1.-2*_nu);

      _matData["BULK_MODULUS"] = _K;
      _matData["LAMBDA"] = _lambda;
      _matData["YOUNG_MODULUS"] = _E;
    }
    else if (itnu != _matData.end() and itLambda != _matData.end()){
      _nu = itnu->second;
      _lambda = itLambda->second;
      _E = _lambda*(1.+_nu)*(1.-2*_nu);
      _mu = _E/(2.*(1.+_nu));
      _K = _E/(3.*(1.-2.*_nu));

      _matData["BULK_MODULUS"] = _K;
      _matData["SHEAR_MODULUS"] = _mu;
      _matData["YOUNG_MODULUS"] = _E;
    }
    else if (itK != _matData.end() and itMu != _matData.end()){
      _K = itK->second;
      _mu = itMu->second;
      _E = 9.*_K*_mu*(3.*_K+_mu);
      _nu = _E/(2.*_mu)-1.;
      _lambda = _E*_nu/(1.+_nu)/(1.-2*_nu);

      _matData["LAMBDA"] = _lambda;
      _matData["YOUNG_MODULUS"] = _E;
      _matData["POISSON_RATIO"] = _nu;
    }
    else if (itK != _matData.end() and itLambda != _matData.end()){
      _K = itK->second;
      _lambda = itLambda->second;
      _E = 9.*_K*(_K-_lambda)/(3.*_K-_lambda);
      _mu = 3.*_K*_E/(9.*_K-_E);
      _nu = (3.*_K - _E)/(6.*_K);

      _matData["SHEAR_MODULUS"] = _mu;
      _matData["YOUNG_MODULUS"] = _E;
      _matData["POISSON_RATIO"] = _nu;
    }
    else if (itMu != _matData.end() and itLambda != _matData.end()){
      _mu = itMu->second;
      _lambda = itLambda->second;
      _nu = _lambda/(2.*(_lambda+_mu));
      _E = 2.*_mu*(1+_nu);
      _K = _E/(3.*(1.-2*_nu));

      _matData["BULK_MODULUS"] = _K;
      _matData["YOUNG_MODULUS"] = _E;
      _matData["POISSON_RATIO"] = _nu;
    }
    std::map<std::string,double>::const_iterator itTol = _matData.find("TOLERANCE");
    if (itTol == _matData.end()){
      _matData["TOLERANCE"] = 1.e-6;
    }
  }

  std::map<std::string,double>::const_iterator it = _matData.find(key);
  if (it == _matData.end()) return 0;
  else return it->second;
}


void elasticSecondOrderLaw::setParameter(const std::string key, const double val){
  _matData.insert(std::pair<std::string,double>(key,val));
};

double elasticSecondOrderLaw::getParameter(const std::string key) const{
  std::map<std::string,double>::const_iterator it = _matData.find(key);
  if (it == _matData.end()) return 0;
  else return it->second;
};


void elasticFirstSecondLaw::setParameter(const std::string key, const double val){
  _matData.insert(std::pair<std::string,double>(key,val));
};

double elasticFirstSecondLaw::getParameter(const std::string key) const{
  std::map<std::string,double>::const_iterator it = _matData.find(key);
  if (it == _matData.end()) return 0;
  else return it->second;
};



linearElasticLaw::linearElasticLaw(): elasticLaw(){}
linearElasticLaw::linearElasticLaw(const double K, const double mu): elasticLaw(){
  this->setParameter("BULK_MODULUS",K);
  this->setParameter("SHEAR_MODULUS",mu);
};
linearElasticLaw::linearElasticLaw(const linearElasticLaw& src): elasticLaw(src){}

void linearElasticLaw::constitutive(const STensor3& F, STensor3& P, STensor43& H, const bool stiff) const{
  STensor3 E(-2.);
  E += F;
  E += F.transpose();
  E*= 0.5;

  double _K = this->getParameter("BULK_MODULUS");
  double _mu = this->getParameter("SHEAR_MODULUS");

  double lambda = _K - 2./3.*_mu;
	double twicemu = _mu+_mu;
	double trlambda = (E(0,0)+E(1,1)+E(2,2))*lambda;

	P*= 0;
	P(0,0)= trlambda;
	P(1,1)= trlambda;
	P(2,2)= trlambda;
	E*= twicemu;
	P += E;

  if (stiff){
    H*= 0;
    H(0,0,0,0) = lambda + twicemu;
    H(1,1,0,0) = lambda;
    H(2,2,0,0) = lambda;
    H(0,0,1,1) = lambda;
    H(1,1,1,1) = lambda + twicemu;
    H(2,2,1,1) = lambda;
    H(0,0,2,2) = lambda;
    H(1,1,2,2) = lambda;
    H(2,2,2,2) = lambda + twicemu;

    H(1,0,1,0) = _mu;
    H(2,0,2,0) = _mu;
    H(0,1,0,1) = _mu;
    H(2,1,2,1) = _mu;
    H(0,2,0,2) = _mu;
    H(1,2,1,2) = _mu;

    H(0,1,1,0) = _mu;
    H(0,2,2,0) = _mu;
    H(1,0,0,1) = _mu;
    H(1,2,2,1) = _mu;
    H(2,0,0,2) = _mu;
    H(2,1,1,2) = _mu;
  }
}


NeoHookeanElasticLaw::NeoHookeanElasticLaw():elasticLaw(){};
NeoHookeanElasticLaw::NeoHookeanElasticLaw(const double K, const double mu, const double tol){
  this->setParameter("BULK_MODULUS",K);
  this->setParameter("SHEAR_MODULUS",mu);
  this->setParameter("TOLERANCE",tol);
};
NeoHookeanElasticLaw::NeoHookeanElasticLaw(const NeoHookeanElasticLaw& src):elasticLaw(src){};

void NeoHookeanElasticLaw::getStress(const STensor3& F, STensor3& P) const{
  double K = this->getParameter("BULK_MODULUS");
  double mu = this->getParameter("SHEAR_MODULUS");
	double J = F.determinant();
	double lnJ = log(J);
	double div23 = 2.0/3.0;
	double J23 = pow(J,div23);
	STensor3 C(0); //C = FT*F;
	for (int i=0; i<3; i++)
    for (int j=0; j<3;j++)
      C(i,j) = F(0,i)*F(0,j)+F(1,i)*F(1,j)+F(2,i)*F(2,j);
  double trC = C(0,0)+C(1,1)+C(2,2);
  STensor3 invF(F.invert());
	fullMatrix<double> temp(3,3);
	for (int i=0; i<3; i++){
		for (int j=0; j<3; j++){
			temp(i,j)= mu/J23*F(i,j)+ (K*lnJ-1.0/3.0*trC*mu/J23)*invF(j,i);
		};
	};
	P.setMat(temp);
};

/*Compute tangent operator by perturbation */
void NeoHookeanElasticLaw::getConstitutiveTangent(const STensor3& F, STensor43& L)const{
  double ref = F.dotprod();
  double tol = this->getParameter("TOLERANCE");
  double dF = ref* tol;
	for (int k=0; k<3; k++){
		for (int l=0; l<3; l++){
			STensor3 Fp(F), Fn(F);
			Fp(k,l)+=dF;
			Fn(k,l)-=dF;
			STensor3 Pp, Pn;
			this->getStress(Fp,Pp);
			this->getStress(Fn,Pn);
			for (int i=0; i<3; i++){
				for (int j=0; j<3; j++){
					L(i,j,k,l) = (Pp(i,j) - Pn(i,j))/(2.*dF);
				};
			};
		};
	};
};

void NeoHookeanElasticLaw::constitutive(const STensor3& F, STensor3& P, STensor43& L, const bool stiff) const{
  getStress(F,P);
  if (stiff)
    getConstitutiveTangent(F,L);
};

biLogarithmicElasticLaw::biLogarithmicElasticLaw():elasticLaw(){};
biLogarithmicElasticLaw::biLogarithmicElasticLaw(const double K, const double mu):elasticLaw(){
  this->setParameter("BULK_MODULUS",K);
  this->setParameter("SHEAR_MODULUS",mu);
};
biLogarithmicElasticLaw::biLogarithmicElasticLaw(const biLogarithmicElasticLaw& src): elasticLaw(src){};

void biLogarithmicElasticLaw::loga(const STensor3& a, STensor3& loga)const{
  const STensor3 I(-1.);
  loga = I;
  loga+= a;
};

void biLogarithmicElasticLaw::constitutive(const STensor3& F, STensor3& P, STensor43& L, const bool stiff) const{
  STensor3 C, E, Sig;
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      C(i,j) = F(0,i)*F(0,j)+F(1,i)*F(1,j)+F(2,i)*F(2,j);
  this->loga(C,E);
  E*=0.5;
  double _K = this->getParameter("BULK_MODULUS");
  double _mu = this->getParameter("SHEAR_MODULUS");
  double lambda = _K - 2./3.*_mu;
	double twicemu = _mu+_mu;
	double trlambda = (E(0,0)+E(1,1)+E(2,2))*lambda;
	STensor3 S(1);
	S *= trlambda;
	E*= twicemu;
	S += E;

	for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      P(i,j) = F(i,0)*S(0,j)+F(i,1)*S(1,j)+F(i,2)*S(2,j);

  if (stiff){
    STensor43 H(0);
    H(0,0,0,0) = lambda + twicemu;
    H(1,1,0,0) = lambda;
    H(2,2,0,0) = lambda;
    H(0,0,1,1) = lambda;
    H(1,1,1,1) = lambda + twicemu;
    H(2,2,1,1) = lambda;
    H(0,0,2,2) = lambda;
    H(1,1,2,2) = lambda;
    H(2,2,2,2) = lambda + twicemu;

    H(1,0,1,0) = _mu;
    H(2,0,2,0) = _mu;
    H(0,1,0,1) = _mu;
    H(2,1,2,1) = _mu;
    H(0,2,0,2) = _mu;
    H(1,2,1,2) = _mu;

    H(0,1,1,0) = _mu;
    H(0,2,2,0) = _mu;
    H(1,0,0,1) = _mu;
    H(1,2,2,1) = _mu;
    H(2,0,0,2) = _mu;
    H(2,1,1,2) = _mu;

    L*=0;

    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          L(i,j,i,k) += S(j,k);
          for (int l=0; l<3; l++){
            for (int p=0; p<3; p++)
              for (int q=0; q<3; q++)
                 L(i,j,k,l)+= F(i,p)*H(p,j,q,l)*F(k,q);
          };
        };
      };
    };
  }
};


linearElasticSecondOrderLaw::linearElasticSecondOrderLaw() :elasticSecondOrderLaw(){} ;
linearElasticSecondOrderLaw::linearElasticSecondOrderLaw(const double a1, const double a2, const double a3, const double a4, const double a5):
                               elasticSecondOrderLaw(){
    this->setParameter("A1",a1);
    this->setParameter("A2",a2);
    this->setParameter("A3",a3);
    this->setParameter("A4",a4);
    this->setParameter("A5",a5);
  };
linearElasticSecondOrderLaw::linearElasticSecondOrderLaw(const linearElasticSecondOrderLaw& src) : elasticSecondOrderLaw(src){};



void linearElasticSecondOrderLaw::constitutive(const STensor33& G, STensor33& Q, STensor63& J, const bool stiff) const{
  double a1 = this->getParameter("A1");
  double a2 = this->getParameter("A2");
  double a3 = this->getParameter("A3");
  double a4 = this->getParameter("A4");
  double a5 = this->getParameter("A5");

  //Msg::Error("a1 = %e, a2 = %e, a3 = %e, a4 = %e, a5 = %e",a1,a2,a3,a4,a5);

  Q*= 0;
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int p=0; p<3; p++){
          Q(i,j,k) += a1*(G(p,p,j)*delta(i,k)+G(p,p,k)*delta(i,j));
          Q(i,j,k) += 0.5*a2*(G(j,p,p)*delta(i,k)+G(k,p,p)*delta(i,j)+2.*delta(j,k)*G(p,p,i));
          Q(i,j,k) += 2*a3*G(i,p,p)*delta(j,k);
        };
        Q(i,j,k) += 2*a4*G(i,j,k);
        Q(i,j,k) += a5*(G(j,k,i)+G(k,j,i));
      };
    };
  };

  if (stiff){
    J*= 0.;
    for (int i=0; i<3; i++)
      for (int j=0; j<3; j++)
        for (int k=0; k<3; k++)
          for (int p=0; p<3; p++)
            for (int q=0; q<3; q++)
              for (int r=0; r<3; r++){
                J(i,j,k,p,q,r) = a1*0.5*(delta(i,k)*delta(p,q)*delta(j,r)+
                                  delta(i,k)*delta(p,r)*delta(j,q)+
                                  delta(i,j)*delta(p,q)*delta(k,r)+
                                  delta(i,j)*delta(p,r)*delta(k,q))+
                            a2*0.5*(delta(j,p)*delta(q,r)*delta(i,k)+
                                          delta(k,p)*delta(q,r)*delta(i,j)+
                                          delta(p,q)*delta(i,r)*delta(j,k)+
                                          delta(p,r)*delta(q,i)*delta(j,k))+
                            a3*2.*delta(i,p)*delta(q,r)*delta(j,k) +
                            a4*delta(i,p)*(delta(j,q)*delta(k,r)+delta(j,r)*delta(k,q))+
                            a5*0.5*(delta(j,p)*delta(i,q)*delta(k,r)+
                                          delta(j,p)*delta(i,r)*delta(k,q)+
                                          delta(k,p)*delta(i,q)*delta(j,r)+
                                          delta(k,p)*delta(i,r)*delta(j,q));
              }
  }
}

linearElasticMindlinLaw::linearElasticMindlinLaw() : elasticSecondOrderLaw(){};
linearElasticMindlinLaw::linearElasticMindlinLaw(const double E, const double le): elasticSecondOrderLaw(){
  this->setParameter("YOUNG_MODULUS",E);
  this->setParameter("LENGTH",le);
};
linearElasticMindlinLaw::linearElasticMindlinLaw(const linearElasticMindlinLaw& src) :elasticSecondOrderLaw(src){};

void linearElasticMindlinLaw::constitutive(const STensor33& G, STensor33& Q, STensor63& J, const bool stiff) const{
  double E = this->getParameter("YOUNG_MODULUS");
  double le = this->getParameter("LENGTH");

  J *= 0.;
  STensor63 J1(0.), J2(0.), J3(0.), J4(0.);
  for ( int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int p=0; p<3; p++){
          for (int q=0; q<3; q++){
            for (int r=0; r<3; r++){
              J1(i,j,k,p,q,r) +=1./6.*((d(i,p)*d(j,q)+d(i,q)*d(j,p))*d(k,r) +
                                        (d(j,p)*d(k,q)+d(j,q)*d(k,p))*d(i,r) +
                                        (d(i,p)*d(k,q)+d(i,q)*d(k,p)*d(j,r)))
                                -1./15.*((d(i,j)*d(k,r)+d(j,k)*d(i,r)+d(k,i)*d(j,r))*d(p,q))
                                -1./15.*((d(i,j)*d(k,p)+d(j,k)*d(i,p)+d(k,i)*d(j,p))*d(q,r))
                                -1./15.*((d(i,j)*d(k,q)+d(j,k)*d(i,q)+d(k,i)*d(j,q))*d(r,p));

              J2(i,j,k,p,q,r) += 1./12*(e(i,k,q)*e(j,p,r)+e(j,k,q)*e(i,p,r)+e(i,k,p)*e(j,q,r)+e(j,k,p)*e(i,q,r))
                                + 1./12.*(2.*(d(i,p)*d(j,q)+d(i,q)*d(j,p))*d(k,r)
                                          - (d(j,p)*d(k,q)+d(j,q)*d(k,p))*d(i,r)
                                          - (d(i,p)*d(k,q)+d(i,q)*d(k,p))*d(j,r));

              J3(i,j,k,p,q,r) += -1./12.*(e(i,k,q)*e(j,p,r)+ e(j,k,q)*e(i,p,r)+e(i,k,p)*e(j,q,r)+e(j,k,p)*e(i,q,r))
                                -1./8.*((d(i,p)*d(j,k)+d(j,p)*d(i,k))*d(q,r)+ (d(i,q)*d(k,j)+d(j,q)*d(i,k))*d(p,r))
                                +1./12*(2.*(d(i,p)*d(j,q)+d(i,q)*d(j,p))*d(q,r)
                                            - (d(j,p)*d(k,q)+d(j,q)*d(k,p))*d(i,r)
                                            - (d(i,p)*d(k,q)+d(i,q)*d(k,p))*d(j,r))
                                +1./15.*((d(i,j)*d(k,r)+d(j,k)*d(i,r)+d(k,i)*d(j,r))*d(p,q)
                                        +(d(i,j)*d(k,p)+d(j,k)*d(i,p)+d(k,i)*d(j,p))*d(q,r)
                                        +(d(i,j)*d(q,k)+d(j,k)*d(i,q)+d(k,i)*d(j,k))*d(r,p));

              J4(i,j,k,p,q,r)+= 1./8.*((d(i,k)*d(j,p)+d(j,k)*d(i,p))*d(q,r)+ (d(i,k)*d(j,q)+d(j,k)*d(i,q))*d(p,r));
            }
          }
        }
      }
    }
  }
  double k1 = 2.*E*le*le;
  double k2 = 2.*E*le*le/4.;
  double k3 = 2.*E*le*le*5./24.;
  double k4 = 0.;

  J1*= k1;
  J2*= k2;
  J3*= k3;
  J4*= k4;

  J+= J1;
  J+= J2;
  J+= J3;
  J+= J4;

  Q*= 0.;
  for ( int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        for (int p=0; p<3; p++){
          for (int q=0; q<3; q++){
            for (int r=0; r<3; r++){
              Q(i,j,k) += J(i,j,k,p,q,r)*G(p,q,r);
            }
          }
        }
      }
    }
  }

};

void linearElasticFirstSecondLaw::constitutive(const STensor3& F, const STensor33& G, STensor3& P, STensor33& Q,
                                          STensor53& JFG, STensor53& JGF, const bool stiff) const{
  SVector3 b(0.);
  b[0] = this->getParameter("B0");
  b[1] = this->getParameter("B1");
  b[2] = this->getParameter("B2");

  STensor3 E(0);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      E(i,j) = 0.5*(F(i,j)+F(j,i)-2.*delta(i,j));
    }
  }

  P*= 0.;
  Q*= 0.;
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      for (int p=0; p<3; p++){
        P(i,j) += b[p]*G(p,i,j);
        Q(p,i,j) = b[p]*E(i,j);
      }
    }
  };
  if (stiff){
    JFG*= 0.;
    JGF*= 0.;

    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        for (int k=0; k<3; k++){
          for (int p=0; p<3; p++){
            for (int q=0; q<3; q++){
              JFG(p,q,i,j,k) = b[i]*0.5*(delta(p,j)*delta(q,k)+delta(p,k)*delta(q,j));
              JGF(i,j,k,p,q) = JFG(p,q,i,j,k);
            }
          }
        }
      }
    }
  };
}


