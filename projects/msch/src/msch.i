%feature("autodoc","1");
#ifdef _DEBUG
  %module mschDebug
#else
  %module msch
#endif
%include std_string.i
%include std_vector.i
%include std_map.i


%include dG3Dpy.i

%{
  #include "FSDomain.h"
  #include "FSMaterialLaw.h"
  #include "FSMaterialLawSecondOrder.h"
  #include "elasticitySolver.h"
  #include "elasticityPBCSolver.h"
  #include "hoDGPartDomain.h"
  #include "hoDGMaterialLaw.h"
%}

%nodefaultctor FSMaterialLaw;
%nodefaultctor FSElasticMaterialLawSecondOrder;
%nodefaultctor FSJ2LinearMaterialLawSecondOrder;
%nodefaultctor FSDomain;
%nodefaultctor FSDomainSecondOrder;
%nodefaultctor hoDGMaterialLaw;
%nodefaultctor hoDGMultiscaleMaterialLaw;
%nodefaultctor FSMultiscaleMaterialLaw;
%nodefaultctor hoDGDomain;


%include "FSDomain.h"
%include "FSMaterialLaw.h"
%include "FSMaterialLawSecondOrder.h"
%include "elasticitySolver.h"
%include "elasticityPBCSolver.h"
%include "hoDGPartDomain.h"
%include "hoDGMaterialLaw.h"
