#coding-Utf-8-*-
from gmshpy import *
#from nonLocalDamagepyDebug import*
from nonLocalDamagepy import*

#script to launch composite problem with a python script

# material law
lawnonlocal = 1 # unique number of law
lawlinear = 2 # unique number of law
rho = 7850. # Bulk mass
length =  1.e-7


# geometry
meshfile="composite.msh" # name of mesh file
propertiesLC="lemaitreChaboche.i01" #properties file
propertiesLin="linear.i01" #properties file
propertiesNoDamage="noDamage.i01" #properties file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 40   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)

#  compute solution and BC (given directly to the solver
# creation of law
law1 = NonLocalDamageMaterialLaw(lawnonlocal,rho,length,propertiesLC)
law2 = NonLocalDamageMaterialLaw(lawlinear,rho,length,propertiesLin)

# creation of ElasticField
Mfield = 1 # number of the field (physical number of Mtrix)
Ffield = 2 # number of the field (physical number of Fiber)

space1 = 0 # function space (Lagrange=0)

Matixfield = NonLocalDamageDomain(1000,Mfield,space1,lawlinear)
Fiberfield = NonLocalDamageDomain(1000,Ffield,space1,lawlinear)

#myfield1.matrixByPerturbation(1,1e-8)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(Matixfield)
mysolver.addDomain(Fiberfield)
mysolver.addMaterialLaw(law1)
mysolver.addMaterialLaw(law2)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)

# BC
# fixed face
mysolver.displacementBC("Face",1,0,0.)
mysolver.displacementBC("Face",2,0,0.)
mysolver.displacementBC("Face",3,0,0.)
mysolver.displacementBC("Face",4,0,0.)
mysolver.displacementBC("Face",5,0,0.)

mysolver.displacementBC("Face",11,1,0.)

mysolver.displacementBC("Face",1,2,0.)
mysolver.displacementBC("Face",2,2,0.)
mysolver.displacementBC("Face",3,2,0.)
mysolver.displacementBC("Face",4,2,0.)
mysolver.displacementBC("Face",5,2,0.)
mysolver.displacementBC("Face",6,2,0.)
mysolver.displacementBC("Face",7,2,0.)
mysolver.displacementBC("Face",8,2,0.)
mysolver.displacementBC("Face",9,2,0.)
mysolver.displacementBC("Face",10,2,0.)
mysolver.displacementBC("Face",11,2,0.)



# displacement
d1=0.00002
#0.00002567;
#cyclicFunction1=cycleFunctionTime(ftime/5., d1, 3.*ftime/5., -d1,ftime, d1); 

#mysolver.displacementBC("Face",6,0,cyclicFunction1)
#mysolver.displacementBC("Face",7,0,cyclicFunction1)
#mysolver.displacementBC("Face",8,0,cyclicFunction1)
#mysolver.displacementBC("Face",9,0,cyclicFunction1)
#mysolver.displacementBC("Face",10,0,cyclicFunction1)

mysolver.displacementBC("Face",6,0,d1)
mysolver.displacementBC("Face",7,0,d1)
mysolver.displacementBC("Face",8,0,d1)
mysolver.displacementBC("Face",9,0,d1)
mysolver.displacementBC("Face",10,0,d1)



# needed resultes
mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("damage",IPField.DAMAGE, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 6, 0)
mysolver.archivingForceOnPhysicalGroup("Face", 7, 0)
mysolver.archivingForceOnPhysicalGroup("Face", 8, 0)

mysolver.solve()

