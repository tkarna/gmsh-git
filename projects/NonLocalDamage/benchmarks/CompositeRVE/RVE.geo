// creat a cube with fiber

//defination of unit
mm = 1e-03;

// volum fraction

f=0.45;

// characteristic size for fibe
R= 0.05*mm;                          // fiber radius
ly=2.0*R*Sqrt(Pi/(2.0*Sqrt(3.0)*f));  // distance between fibers' centers
lx=ly*Sqrt(3.0)/2.0;

// characteristic Size for the volum
x = ly;
y = lx;
z = lx/8.0;

sq2 = Sqrt(2.0);


// Characteristic length
Lc1=R/4;


// definition of points***********************************************
Point(1) = { 0.0 , 0.0 , 0.0 , Lc1};
Point(2) = {  x-R  , 0.0 , 0.0 , Lc1};
Point(3) = {  x  ,  R  , 0.0 , Lc1};
Point(4) = {  x ,  y  , 0.0 , Lc1};
Point(5) = {  R ,  y , 0.0 , Lc1};
Point(6) = {  0.0  , y-R , 0.0 , Lc1};
Point(7) = {  x  ,  0.0  , 0.0 , Lc1};
Point(8) = { 0.0 ,  y  , 0.0 , Lc1};

Point(9) = {  x/2.0 ,  y/2.0 , 0.0 , Lc1};

Point(10) = {  x-R/sq2 , R/sq2 , 0.0 , Lc1};
Point(11) = {  x-R/(2.0*sq2) , R/(2.0*sq2) , 0.0 , Lc1};
Point(12) = {  x-R/(2.0*sq2) ,  0.0  , 0.0 , Lc1};
Point(13) = {  x  ,  R/(2.0*sq2)  , 0.0 , Lc1};

Point(14) = { R/sq2 ,  y-R/sq2  , 0.0 , Lc1};
Point(15) = { R/(2.0*sq2) , y-R/(2.0*sq2), 0.0 , Lc1};
Point(16) = { R/(2.0*sq2) ,  y  , 0.0 , Lc1};
Point(17) = { 0.0 ,  y-R/(2.0*sq2) , 0.0 , Lc1};

Point(101) = { 0.0 , 0.0 , z , Lc1};
Point(102) = {  x-R  , 0.0 , z , Lc1};
Point(103) = {  x  ,  R  , z , Lc1};
Point(104) = {  x ,  y  , z , Lc1};
Point(105) = {  R ,  y , z , Lc1};
Point(106) = {  0.0  , y-R , z , Lc1};
Point(107) = {  x  ,  0.0  , z , Lc1};
Point(108) = { 0.0 ,  y  , z , Lc1};

Point(109) = {  x/2.0 ,  y/2.0 , z , Lc1};

Point(110) = {  x-R/sq2 , R/sq2 , z , Lc1};
Point(111) = {  x-R/(2.0*sq2) , R/(2.0*sq2) , z , Lc1};
Point(112) = {  x-R/(2.0*sq2) ,  0.0  , z , Lc1};
Point(113) = {  x  ,  R/(2.0*sq2)  , z , Lc1};

Point(114) = { R/sq2 ,  y-R/sq2  , z , Lc1};
Point(115) = { R/(2.0*sq2) , y-R/(2.0*sq2), z , Lc1};
Point(116) = { R/(2.0*sq2) ,  y  , z , Lc1};
Point(117) = { 0.0 ,  y-R/(2.0*sq2) , z , Lc1};


// Line between points
Line(1) = {1,2};
Line(2) = {3,4};
Line(3) = {4,5};
Line(4) = {6,1};

Line(5) = {2,12};
Line(6) = {12,7};
Line(7) = {7,13};
Line(8) = {13,3};
Line(9) = {10,11};
Line(10) = {12,11};
Line(11) = {11,13};

Circle(12) = {3,7,10};
Circle(13) = {10,7,2};

Line(14) = {5,16};
Line(15) = {16,8};
Line(16) = {8,17};
Line(17) = {17,6};
Line(18) = {14,15};
Line(19) = {16,15};
Line(20) = {15,17};

Circle(21) = {6,8,14};
Circle(22) = {14,8,5};

Line(23) = {10,9};
Line(24) = {9,1};
Line(25) = {14,9};
Line(26) = {9,4};

Line(101) = {101,102};
Line(102) = {103,104};
Line(103) = {104,105};
Line(104) = {106,101};

Line(105) = {102,112};
Line(106) = {112,107};
Line(107) = {107,113};
Line(108) = {113,103};
Line(109) = {110,111};
Line(110) = {112,111};
Line(111) = {111,113};

Circle(112) = {103,107,110};
Circle(113) = {110,107,102};

Line(114) = {105,116};
Line(115) = {116,108};
Line(116) = {108,117};
Line(117) = {117,106};
Line(118) = {114,115};
Line(119) = {116,115};
Line(120) = {115,117};

Circle(121) = {106,108,114};
Circle(122) = {114,108,105};

Line(123) = {110,109};
Line(124) = {109,101};
Line(125) = {114,109};
Line(126) = {109,104};

Line(1001) = {1,101};
Line(1002) = {2,102};
Line(1003) = {3,103};
Line(1004) = {4,104};
Line(1005) = {5,105};
Line(1006) = {6,106};
Line(1007) = {7,107};
Line(1008) = {8,108};
Line(1009) = {9,109};
Line(1010) = {10,110};
Line(1011) = {11,111};
Line(1012) = {12,112};
Line(1013) = {13,113};
Line(1014) = {14,114};
Line(1015) = {15,115};
Line(1016) = {16,116};
Line(1017) = {17,117};

// from surface
Line Loop(1) = {1,-13,23,24}; 
Line Loop(2) = {2,-26,-23,-12}; 
Line Loop(3) = {3,-22,25,26}; 
Line Loop(4) = {4,-24,-25,-21}; 

Line Loop(5) = {5,10,-9,13};
Line Loop(6) = {6,7,-11,-10};
Line Loop(7) = {8,12,9,11};

Line Loop(8) = {14,19,-18,22};
Line Loop(9) = {15,16,-20,-19};
Line Loop(10) = {17,21,18,20};

Line Loop(101) = {101,-113,123,124}; 
Line Loop(102) = {102,-126,-123,-112}; 
Line Loop(103) = {103,-122,125,126}; 
Line Loop(104) = {104,-124,-125,-121}; 

Line Loop(105) = {105,110,-109,113};
Line Loop(106) = {106,107,-111,-110};
Line Loop(107) = {108,112,109,111};

Line Loop(108) = {114,119,-118,122};
Line Loop(109) = {115,116,-120,-119};
Line Loop(110) = {117,121,118,120};

Line Loop(1001) = {1,1002,-101,-1001};
Line Loop(1002) = {2,1004,-102,-1003};
Line Loop(1003) = {3,1005,-103,-1004};
Line Loop(1004) = {4,1001,-104,-1006};
Line Loop(1005) = {5,1012,-105,-1002};
Line Loop(1006) = {6,1007,-106,-1012};
Line Loop(1007) = {7,1013,-107,-1007};
Line Loop(1008) = {8,1003,-108,-1013};
Line Loop(1009) = {9,1011,-109,-1010};
Line Loop(1010) = {10,1011,-110,-1012};
Line Loop(1011) = {11,1013,-111,-1011};
Line Loop(1012) = {12,1010,-112,-1003};
Line Loop(1013) = {13,1002,-113,-1010};
Line Loop(1014) = {14,1016,-114,-1005};
Line Loop(1015) = {15,1008,-115,-1016};
Line Loop(1016) = {16,1017,-116,-1008};
Line Loop(1017) = {17,1006,-117,-1017};
Line Loop(1018) = {18,1015,-118,-1014};
Line Loop(1019) = {19,1015,-119,-1016};
Line Loop(1020) = {20,1017,-120,-1015};
Line Loop(1021) = {21,1014,-121,-1006};
Line Loop(1022) = {22,1005,-122,-1014};
Line Loop(1023) = {23,1009,-123,-1010};
Line Loop(1024) = {24,1001,-124,-1009};
Line Loop(1025) = {25,1009,-125,-1014};
Line Loop(1026) = {26,1004,-126,-1009};

Plane Surface(1) = {1};
Plane Surface(2) = {2};
Plane Surface(3) = {3};
Plane Surface(4) = {4};

Plane Surface(5) = {5};
Plane Surface(6) = {6};
Plane Surface(7) = {7};

Plane Surface(8) = {8};
Plane Surface(9) = {9};
Plane Surface(10) = {10};

Plane Surface(101) = {101};
Plane Surface(102) = {102};
Plane Surface(103) = {103};
Plane Surface(104) = {104};

Plane Surface(105) = {105};
Plane Surface(106) = {106};
Plane Surface(107) = {107};

Plane Surface(108) = {108};
Plane Surface(109) = {109};
Plane Surface(110) = {110};

Plane Surface(1001) = {1001};
Plane Surface(1002) = {1002};
Plane Surface(1003) = {1003};
Plane Surface(1004) = {1004};
Plane Surface(1005) = {1005};
Plane Surface(1006) = {1006};
Plane Surface(1007) = {1007};
Plane Surface(1008) = {1008};
Plane Surface(1009) = {1009};
Plane Surface(1010) = {1010};
Plane Surface(1011) = {1011};
Plane Surface(1012) = {1012};
Plane Surface(1013) = {1013};
Plane Surface(1014) = {1014};
Plane Surface(1015) = {1015};
Plane Surface(1016) = {1016};
Plane Surface(1017) = {1017};
Plane Surface(1018) = {1018};
Plane Surface(1019) = {1019};
Plane Surface(1020) = {1020};
Plane Surface(1021) = {1021};
Plane Surface(1022) = {1022};
Plane Surface(1023) = {1023};
Plane Surface(1024) = {1024};
Plane Surface(1025) = {1025};
Plane Surface(1026) = {1026};

Surface Loop(1) = {1,1001,1013,1023,1024,101};
Surface Loop(2) = {2,1002,1026,1023,1012,102};
Surface Loop(3) = {3,1003,1022,1025,1026,103};
Surface Loop(4) = {4,1004,1024,1025,1021,104};
Surface Loop(5) = {5,1013,1005,1010,1009,105};
Surface Loop(6) = {6,1006,1007,1011,1010,106};
Surface Loop(7) = {7,1008,1012,1009,1011,107};
Surface Loop(8) = {8,1014,1019,1018,1022,108};
Surface Loop(9) = {9,1015,1016,1020,1019,109};
Surface Loop(10) = {10,1017,1021,1018,1020,110};


Volume(1) = {1};
Volume(2) = {2};
Volume(3) = {3};
Volume(4) = {4};
Volume(5) = {5};
Volume(6) = {6};
Volume(7) = {7};
Volume(8) = {8};
Volume(9) = {9};
Volume(10) = {10};

Transfinite Line {1,23,2,4,25,3,101,123,102,104,125,103} = 4;
Transfinite Line {5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,24,26} = 3;
Transfinite Line {105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,124,126} = 3;
Transfinite Line {1001,1002,1003,1004,1005,1006,1007,1008,1009,1010} = 2;
Transfinite Line {1011,1012,1013,1014,1015,1016,1017} = 2;

Transfinite Surface {1,2,3,4,5,6,7,8,9,10};
Transfinite Surface {101,102,103,104,105,106,107,108,109,110};
Transfinite Surface {1001,1002,1003,1004,1005,1006,1007,1008,1009,1010};
Transfinite Surface {1011,1012,1013,1014,1015,1016,1017,1018,1019,1020};
Transfinite Surface {1021,1022,1023,1024,1025,1026};

Recombine Surface {1,2,3,4,5,6,7,8,9,10};
Recombine Surface {101,102,103,104,105,106,107,108,109,110};
Recombine Surface {1001,1002,1003,1004,1005,1006,1007,1008,1009,1010};
Recombine Surface {1011,1012,1013,1014,1015,1016,1017,1018,1019,1020};
Recombine Surface {1021,1022,1023,1024,1025,1026};

Transfinite Volume {1};
Transfinite Volume {2};
Transfinite Volume {3};
Transfinite Volume {4};
Transfinite Volume {5};
Transfinite Volume {6};
Transfinite Volume {7};
Transfinite Volume {8};
Transfinite Volume {9};
Transfinite Volume {10};

Physical Volume(101) ={1,2,3,4};
Physical Volume(102) ={5,6,7,8,9,10};

// fix x
Physical Surface(1) = {1004,1017,1016};

// fix y
Physical Surface(2) = {1001,1005,1006};

// rigid y
Physical Surface(6) = {1003,1014,1015};

// depl x
Physical Surface(3) = {1002,1007,1008};

// depl z
Physical Surface(4) = {1,2,3,4,5,6,7,8,9,10};
Physical Surface(5) = {101,102,103,104,105,106,107,108,109,110};


