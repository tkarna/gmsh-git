#coding-Utf-8-*-
from gmshpy import *
#from nonLocalDamagepyDebug import*
from nonLocalDamagepy import*

#script to launch composite problem with a python script

# material law
lawnonlocal1 = 1 # unique number of law
lawnonlocal2 = 2 # unique number of law
rho = 7850. # Bulk mass


# geometry
meshfile="RVE.msh" # name of mesh file
propertiesC1="properties_an1.i01" #properties file
propertiesC2="properties_an2.i01" #properties file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 200   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)

#  compute solution and BC (given directly to the solver
# creation of law
law1 = NonLocalDamageMaterialLaw(lawnonlocal1,rho,propertiesC1)
law2 = NonLocalDamageMaterialLaw(lawnonlocal2,rho,propertiesC2)


# creation of ElasticField
nfield1 = 1001 # number of the field (physical number of surface)
nfield2 = 2001 

space1 = 0 # function space (Lagrange=0)

myfield1 = NonLocalDamageDomain(1000,nfield1,space1,lawnonlocal1)
myfield2 = NonLocalDamageDomain(1000,nfield2,space1,lawnonlocal2)

#myfield1.matrixByPerturbation(1,1e-8)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addDomain(myfield2)
mysolver.addMaterialLaw(law1)
mysolver.addMaterialLaw(law2)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
# BC
#shearing
#mysolver.displacementBC("Face",1234,0,0.)
#mysolver.displacementBC("Face",1234,1,0.)
#mysolver.displacementBC("Face",1234,2,0.)
#mysolver.displacementBC("Face",5678,1,0.00)
#mysolver.displacementBC("Face",5678,0,0.00001)
#mysolver.forceBC("Face",2376,2,100000000)
#mysolver.forceBC("Face",4158,2,-100000000)
#mysolver.forceBC("Face",5678,0,100000000)
#tension along z
#mysolver.displacementBC("Face",1234,2,0.)
#mysolver.displacementBC("Face",5678,2,0.0001)
#mysolver.displacementBC("Face",4158,0,0.)
#mysolver.displacementBC("Face",1265,1,0.)
#tension along x

#d1=0.00002567;
d1=0.001
#d2=0.00003
#d3=0.00004

#cyclicFunction1=cycleFunctionTime(ftime/4., d1, 3.*ftime/4., d2,ftime, d3); 
#fct1 = simpleFunctionTimeDouble(d1)
mysolver.displacementBC("Face",101,0,0.)
#mysolver.displacementBC("Face",2376,0,cyclicFunction1)
mysolver.displacementBC("Face",102,0,d1)
mysolver.displacementBC("Face",111,1,0.)

mysolver.displacementBC("Face",121,2,0.)

#mysolver.constraintBC("Face",3487,1)


mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("damage",IPField.DAMAGE, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 101, 0)
mysolver.archivingForceOnPhysicalGroup("Face", 102, 0)

mysolver.solve()

