// creat a cube with fiber

//defination of unit
mm = 0.001;

lx=5*mm;

// characteristic Size for the volum
x = 75*mm;
y = 12.5*mm;    //ly/2;
z = lx/20.0;

// Characteristic length
Lc1=z;

// definition of points
Point(1) = { 0.0 , 0.0 , 0.0 , Lc1};
Point(2) = {  x  , 0.0 , 0.0 , Lc1};
Point(3) = {  x  , 0.0 , z , Lc1};
Point(4) = {  x  , 0.0 , 3.0*z , Lc1};
Point(5) = {  x  , 0.0 , 4.0*z , Lc1};
Point(6) = { 0.0 , 0.0 , 4.0*z , Lc1};
Point(7) = { 0.0 , 0.0 , 3.0*z , Lc1};
Point(8) = { 0.0 , 0.0 , z , Lc1};

// Line between points
Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,8};
Line(4) = {8,1};
Line(5) = {3,4};
Line(6) = {4,7};
Line(7) = {7,8};
Line(8) = {4,5};
Line(9) = {5,6};
Line(10)= {6,7};


// Surface definition
Line Loop(1) = {1,2,3,4};
Line Loop(2) = {-3,5,6,7};
Line Loop(3) = {-6,8,9,10};

Plane Surface(1) = {1};
Plane Surface(2) = {2};
Plane Surface(3) = {3};


//VOlume

my_mat1[] = Extrude {0.0 , y , 0.0} {Surface{1,3};};  // {Layers{1};};
my_mat2[] = Extrude {0.0 , y , 0.0} {Surface{2};};   // {Layers{1}};



// Physical objects to applied BC and material
Physical Surface(111) = {1,2,3};
Physical Surface(112) = {32,54,76};
Physical Surface(101) = {31,53,75};
Physical Surface(102) = {23,45,67};
Physical Surface(121) = {19};
Physical Surface(122) = {49};
//Physical Line(12) = {1};
//Physical Line(23) = {2};
//Physical Line(34) = {3};
//Physical Line(41) = {4};
//Physical Line(56) = {5};
//Physical Line(67) = {6};
//Physical Line(78) = {7};
//Physical Line(85) = {8};
//Physical Line(15) = {9};
//Physical Line(26) = {10};
//Physical Line(37) = {11};
//Physical Line(48) = {12};

//Physical Point(1) ={1};
//Physical Point(2) ={2};
//Physical Point(3) ={3};
//Physical Point(4) ={4};
//Physical Point(5) ={5};
//Physical Point(6) ={6};
//Physical Point(7) ={7};
//Physical Point(8) ={8};

Physical Volume(1001) ={1,2};
Physical Volume(2001) ={3};

// define transfinite mesh
Transfinite Line {1,3,6,9,12,14,34,36} =  51;
Transfinite Line {18,22,40,44,17,26,39,48} = 11;
Transfinite Line {2,8,4,10,15,37,13,35} = 2;
Transfinite Line {5,7,57,59} = 3;
Transfinite Surface {1,2,3,32,54,76,23,45,67,31,53,75,19,27,41,49} ;
Recombine Surface {1,2,3,32,54,76,23,45,67,31,53,75,19,27,41,49} ;
Transfinite Volume {1,2,3};










