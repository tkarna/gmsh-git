// creat a sample 

//defination of unit
un = 1e-3;

// characteristic size for sample
R= 5*un;                          // radius of notch
lx1=8*un;                        
lx=30*un;                            // half length of sample
ly=8*un;                        // half width of sample 

z = 2*un;                       // thickness
sq2 = Sqrt(2.0);


// Characteristic length
Lc1=R/4.0;
Lc2=R/2.0;

// definition of points***********************************************
Point(1) = { 0.0 , 0.0 , 0.0 , Lc2};
Point(2) = {  lx1 , 0.0 , 0.0 , Lc2};
Point(3) = {  lx1 , ly  , 0.0 , Lc2};
Point(4) = {   R , ly  , 0.0 , Lc1};
Point(5) = {  0.0+R*sq2/2.0, ly-R*sq2/2.0  , 0.0 , Lc1};
Point(6) = { 0.0 ,ly-R , 0.0 , Lc1};
Point(7) = {  0.0, ly  , 0.0 , Lc1};
Point(8) = {  lx , 0.0 , 0.0 , Lc2};
Point(9) = {  lx , ly  , 0.0 , Lc2};

// Line between points
Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {5,2};
Line(5) = {6,1};

Circle(6) = {4,7,5};
Circle(7) = {5,7,6};

Line(8) = {2,8};
Line(9) = {8,9};
Line(10) = {9,3};

// Surface definition
Line Loop(1) = {1,-4,7,5};
Line Loop(2) = {2,3,6,4};
Line Loop(3) = {8,9,10,-2};

Plane Surface(1) = {1};
Plane Surface(2) = {2};

Plane Surface(3) = {3};

//VOlume
mat1[] = Extrude {0.0 , 0.0 , z/2.0} {Surface{1,2,3}; Layers{1}; Recombine;};



//physical object for BC and material

//fix Z
Physical Point(1) ={1};
Physical Point(2) ={2};
Physical Point(3) ={3};
Physical Point(4) ={4};
Physical Point(5) ={5};
Physical Point(6) ={6};
Physical Point(7) ={7};

Physical Line(1) ={1};
Physical Line(2) ={4};


Physical Surface(121) = {1,2,3};

//opposite to surface 1 and 2
Physical Surface(122) = {32,54,76};




//fix x
Physical Surface(101) = {31};

//fix y
Physical Surface(111) = {19,63};

Physical Surface(112) = {45,71};

//loading X
Physical Surface(102) = {67};

//
Physical Volume(1000) ={mat1[]};



// define transfinite mesh
Transfinite Line {1} = 31;
Transfinite Line {2} = 27;
Transfinite Line {3} = 21;
Transfinite Line {4} = 21;
Transfinite Line {5} = 21;
Transfinite Line {6} = 27;
Transfinite Line {7} = 31;

Transfinite Line {8} = 21;
Transfinite Line {9} = 27;
Transfinite Line {10} = 21;

Transfinite Surface {1} ;
Transfinite Surface {2} ;
Transfinite Surface {3} ;
Recombine Surface {1} ;
Recombine Surface {2} ;
Recombine Surface {3} ;


Transfinite Volume {1,2,3};

