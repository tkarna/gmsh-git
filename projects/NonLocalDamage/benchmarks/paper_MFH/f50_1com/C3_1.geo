// creat a cube with fiber

//defination of unit
mm = 0.001;

// volum fraction

f=0.50;

// characteristic size for fibe
R= 0.05*mm;                          // fiber radius
ly=2.0*R*Sqrt(Pi/(2.0*Sqrt(3.0)*f));  // distance between fibers' centers
lx=ly*Sqrt(3.0)/2.0;

// characteristic Size for the volum
x = 6*ly;
y = 6*lx;
z = R/2.0;


// Characteristic length
Lc1=lx/5.0;
Lc2=lx/5.0;

// definition of points***********************************************
Point(1) = { 0.0 , 0.0 , 0.0 , Lc1};
Point(2) = {  x  , 0.0 , 0.0 , Lc1};
Point(3) = {  x  ,  y  , 0.0 , Lc1};
Point(4) = { 0.0 ,  y  , 0.0 , Lc1};

// Line between points
Line(1) = {1,2};
Line(2) = {3,4};


//define line Loop list for fibers
t=-1;
//fiber*************************************************************

For j In {0:4:2}
For i In {0:5}

t=t+1;
  
//define new point
p1 = newp; Point(p1) = {i*ly+ly/2.0 , j*lx+lx/2.0 ,0.0 ,Lc2};
p2 = newp; Point(p2) = {i*ly+ly/2.0-R,j*lx+lx/2.0 ,0.0 ,Lc2}; 
p3 = newp; Point(p3) = {i*ly+ly/2.0 , j*lx+lx/2.0-R ,0.0 ,Lc2};
p4 = newp; Point(p4) = {i*ly+ly/2.0+R,j*lx+lx/2.0 ,0.0 ,Lc2}; 
p5 = newp; Point(p5) = {i*ly+ly/2.0 , j*lx+lx/2.0+R ,0.0 ,Lc2}; 


//define new circles

c1 = newreg; Circle(c1) = {p2,p1,p3};
c2 = newreg; Circle(c2) = {p3,p1,p4};
c3 = newreg; Circle(c3) = {p4,p1,p5};
c4 = newreg; Circle(c4) = {p5,p1,p2};

theloop[t] = newreg; Line Loop(theloop[t]) = {c1,c2,c3,c4}; 

thesurface[t] = news;
Plane Surface( thesurface[t]) = {theloop[t]};

EndFor
EndFor

//****************************************************************
For j In {1:5:2}
For i In {1:5}
  
t=t+1;

//define new point
p1 = newp; Point(p1) = {i*ly , j*lx+lx/2.0 ,0.0 ,Lc2};
p2 = newp; Point(p2) = {i*ly-R,j*lx+lx/2.0 ,0.0 ,Lc2}; 
p3 = newp; Point(p3) = {i*ly , j*lx+lx/2.0-R ,0.0 ,Lc2};
p4 = newp; Point(p4) = {i*ly+R,j*lx+lx/2.0 ,0.0 ,Lc2}; 
p5 = newp; Point(p5) = {i*ly , j*lx+lx/2.0+R ,0.0 ,Lc2}; 


//define new circles

c1 = newreg; Circle(c1) = {p2,p1,p3};
c2 = newreg; Circle(c2) = {p3,p1,p4};
c3 = newreg; Circle(c3) = {p4,p1,p5};
c4 = newreg; Circle(c4) = {p5,p1,p2};


theloop[t] = newreg; Line Loop(theloop[t]) = {c1,c2,c3,c4}; 

thesurface[t] = news;
Plane Surface( thesurface[t]) = {theloop[t]};

EndFor
EndFor

//***********************************************************
// define new point

t=-1;
For j In {1:5:2}

t=t+1;

p1 = newp; Point(p1) = {0.0, j*lx+lx/2.0 ,0.0 ,Lc2};
p2 = newp; Point(p2) = {0.0, j*lx+lx/2.0-R ,0.0 ,Lc2}; 
p3 = newp; Point(p3) = { R , j*lx+lx/2.0 ,0.0 ,Lc2};
p4 = newp; Point(p4) = {0.0, j*lx+lx/2.0+R ,0.0 ,Lc2}; 

//define new circles

c1 = newreg; Circle(c1) = {p2,p1,p3};
c2 = newreg; Circle(c2) = {p3,p1,p4};
l3 = newreg; Line (l3) = {p4,p2};



loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {c1,c2,l3}; 

surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

EndFor

//***********************************************************
// define new point

For j In {1:5:2}

t=t+1;

p1 = newp; Point(p1) = {x, j*lx+lx/2.0 ,0.0 ,Lc2};
p2 = newp; Point(p2) = {x, j*lx+lx/2.0-R ,0.0 ,Lc2}; 
p3 = newp; Point(p3) = {x-R, j*lx+lx/2.0 ,0.0 ,Lc2};
p4 = newp; Point(p4) = {x, j*lx+lx/2.0+R ,0.0 ,Lc2}; 

//define new circles

c1 = newreg; Circle(c1) = {p4,p1,p3};
c2 = newreg; Circle(c2) = {p3,p1,p2};
l3 = newreg; Line (l3) = {p2,p4};


loop_edg[t] = newreg; Line Loop(loop_edg[t]) = {c1,c2,l3}; 

surface_edg[t] = news;
Plane Surface( surface_edg[t]) = {loop_edg[t]};

EndFor

//define the line loop for matrix surface****************************

l1 = newreg; Line (l1) = {2,183};
l2 = newreg; Line (l2) = {185,187};
l3 = newreg; Line (l3) = {189,191};
l4 = newreg; Line (l4) = {193,3};
l5 = newreg; Line (l5) = {4,181};
l6 = newreg; Line (l6) = {179,177};
l7 = newreg; Line (l7) = {175,173};
l8 = newreg; Line (l8) = {171,1};

// Surface definition
lineloop_matix = newreg;

Line Loop(lineloop_matix) = {1,l1,-217,-216,l2,-222,-221,l3,-227,-226,l4,2,l5,-212,-211,l6,-207,-206,l7,-202,-201,l8};

// define the surface for matrix
MaS=news;
Plane Surface(MaS) = {lineloop_matix,theloop[]};


//VOlume of fiber and matrix ******************************************************
my_fiberV[] = Extrude {0.0 , 0.0 , z} {Surface{thesurface[],surface_edg[]};Layers{1};};
my_mtrixV[] = Extrude {0.0 , 0.0 , z} {Surface {MaS}; Layers{1};};



// Physical objects to applied BC **********************************
//fix x displacement
Physical Surface(101) = {1275,1016,1287,999,1299,982,1311};

//add x displacement
Physical Surface(102) = {1231,1033,1243,1050,1255,1067,1267};

//fix y displacement
Physical Surface(111) = {1227};
Physical Surface(112) = {1271};

//fix z displacement
Physical Surface(121) = {MaS,thesurface[],surface_edg[]};
Physical Surface(122) = {1017,878,900,922,944,966,1068,636,614,592,570,548,526,1000,768,790,812,834,856,1051,504,482,460,438,416,394,983,658,680,702,724,746,1034,372,350,328,306,284,262,1840};



// Physical objects to applied material*********************
Physical Volume(1001) ={my_mtrixV[]};
Physical Volume(1002) ={my_fiberV[]};


//define transfinite mesh***************************************
//Transfinite Line {l3,l4} = 2;










