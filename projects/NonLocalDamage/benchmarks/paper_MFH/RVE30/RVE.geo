// creat a cube with fiber

//defination of unit
mm = 0.001;

// volum fraction

f=0.30;

// characteristic size for fibe
R= 0.05*mm;                          // fiber radius
ly=2.0*R*Sqrt(Pi/(2.0*Sqrt(3.0)*f));  // distance between fibers' centers
lx=ly*Sqrt(3.0)/2.0;

// characteristic Size for the volum
x = lx;
y = ly/2.0;
z = lx/30.0;

sq2 = Sqrt(2.0);


// Characteristic length
Lc1=R/4;


// definition of points***********************************************
Point(1) = { 0.0 , 0.0 , 0.0 , Lc1};
Point(2) = {  x-R  , 0.0 , 0.0 , Lc1};
Point(3) = {  x  ,  R  , 0.0 , Lc1};
Point(4) = {  x ,  y  , 0.0 , Lc1};
Point(5) = {  R ,  y , 0.0 , Lc1};
Point(6) = {  0.0  , y-R , 0.0 , Lc1};
Point(7) = {  x  ,  0.0  , 0.0 , Lc1};
Point(8) = { 0.0 ,  y  , 0.0 , Lc1};

Point(9) = {  x/2.0 ,  y/2.0 , 0.0 , Lc1};

Point(10) = {  x-R/sq2 , R/sq2 , 0.0 , Lc1};
Point(11) = {  x-R/(2.0*sq2) , R/(2.0*sq2) , 0.0 , Lc1};
Point(12) = {  x-R/(2.0*sq2) ,  0.0  , 0.0 , Lc1};
Point(13) = {  x  ,  R/(2.0*sq2)  , 0.0 , Lc1};

Point(14) = { R/sq2 ,  y-R/sq2  , 0.0 , Lc1};
Point(15) = { R/(2.0*sq2) , y-R/(2.0*sq2), 0.0 , Lc1};
Point(16) = { R/(2.0*sq2) ,  y  , 0.0 , Lc1};
Point(17) = { 0.0 ,  y-R/(2.0*sq2) , 0.0 , Lc1};



// Line between points
Line(1) = {1,2};
Line(2) = {3,4};
Line(3) = {4,5};
Line(4) = {6,1};

Line(5) = {2,12};
Line(6) = {12,7};
Line(7) = {7,13};
Line(8) = {13,3};
Line(9) = {10,11};
Line(10) = {12,11};
Line(11) = {11,13};

Circle(12) = {3,7,10};
Circle(13) = {10,7,2};

Line(14) = {5,16};
Line(15) = {16,8};
Line(16) = {8,17};
Line(17) = {17,6};
Line(18) = {14,15};
Line(19) = {16,15};
Line(20) = {15,17};

Circle(21) = {6,8,14};
Circle(22) = {14,8,5};

Line(23) = {10,9};
Line(24) = {9,1};
Line(25) = {14,9};
Line(26) = {9,4};




// from surface
Line Loop(1) = {1,-13,23,24}; 
Line Loop(2) = {2,-26,-23,-12}; 
Line Loop(3) = {3,-22,25,26}; 
Line Loop(4) = {4,-24,-25,-21}; 

Line Loop(5) = {5,10,-9,13};
Line Loop(6) = {6,7,-11,-10};
Line Loop(7) = {8,12,9,11};

Line Loop(8) = {14,19,-18,22};
Line Loop(9) = {15,16,-20,-19};
Line Loop(10) = {17,21,18,20};


Plane Surface(1) = {1};
Plane Surface(2) = {2};
Plane Surface(3) = {3};
Plane Surface(4) = {4};

Plane Surface(5) = {5};
Plane Surface(6) = {6};
Plane Surface(7) = {7};

Plane Surface(8) = {8};
Plane Surface(9) = {9};
Plane Surface(10) = {10};



//VOlume of fiber and matrix ******************************************************
my_mtrixV[] = Extrude {0.0 , 0.0 , z} {Surface {1,2,3,4}; Layers{1};Recombine;};
my_fiberV[] = Extrude {0.0 , 0.0 , z} {Surface{5,6,7,8,9,10}; Layers{1};Recombine;};



// Physical objects to applied BC **********************************

//fix x displacement
Physical Surface(101) = {101,215,233};


//add x displacement
Physical Surface(102) = {57,167,149};


//fix y displacement
Physical Surface(111) = {35,123,145};
Physical Surface(112) = {79,189,211};

//fix z displacement
Physical Surface(121) = {1,2,3,4,5,6,7,8,9,10};

Physical Surface(122) = {70,48,92,114,136,158,180,202,224,246};




// Physical objects to applied material*********************
Physical Volume(1001) ={my_mtrixV[]};
Physical Volume(1002) ={my_fiberV[]};

// define transfinite mesh
Transfinite Line {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26} = 6;



Transfinite Surface {1};
Transfinite Surface {2};
Transfinite Surface {3};
Transfinite Surface {4};
Transfinite Surface {5};
Transfinite Surface {6};
Transfinite Surface {7};
Transfinite Surface {8};
Transfinite Surface {9};
Transfinite Surface {10};

Recombine Surface {1};
Recombine Surface {2};
Recombine Surface {3};
Recombine Surface {4};
Recombine Surface {5};
Recombine Surface {6};
Recombine Surface {7};
Recombine Surface {8};
Recombine Surface {9};
Recombine Surface {10};


Transfinite Volume {1};
Transfinite Volume {2};
Transfinite Volume {3};
Transfinite Volume {4};
Transfinite Volume {5};
Transfinite Volume {6};
Transfinite Volume {7};
Transfinite Volume {8};
Transfinite Volume {9};
Transfinite Volume {10};










