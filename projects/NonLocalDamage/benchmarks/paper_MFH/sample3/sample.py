#coding-Utf-8-*-
from gmshpy import *
#from nonLocalDamagepyDebug import*
from nonLocalDamagepy import*

#script to launch beam problem with a python script

# material law
lawnum = 1 # unique number of law
rho = 7850. # Bulk mass


# geometry
meshfile="sample.msh" # name of mesh file
propertiesLC="lemaitreChaboche.i01" #properties file
#
#"lemaitreChaboche.i01" #properties file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 9000   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=20 # Number of step between 2 archiving (used only if soltype=1)

#  compute solution and BC (given directly to the solver
# creation of law
law1 = NonLocalDamageMaterialLaw(lawnum,rho,propertiesLC)


# creation of ElasticField
nfield1 = 1000 # number of the field (physical number of surface)
#nfield2 = 2 # number of the field (physical number of surface)

space1 = 0 # function space (Lagrange=0)
myfield1 = NonLocalDamageDomain(1000,nfield1,space1,lawnum)
#myfield2 = NonLocalDamageDomain(1000,nfield2,space1,lawnum)
#myfield1.matrixByPerturbation(1,1e-8)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
#mysolver.addDomain(myfield2)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)

# BC

d1=0.001
d2=0.0025
d3=0.003
cyclicFunction1=cycleFunctionTime(ftime/15., d1, 8*ftime/15.,d2,ftime, d3); 
#fct1 = simpleFunctionTimeDouble(d1)
mysolver.displacementBC("Face",101,0,0.)
#mysolver.displacementBC("Face",102,0,d1)
mysolver.displacementBC("Face",102,0,cyclicFunction1)
mysolver.displacementBC("Face",111,1,0.)
mysolver.displacementBC("Face",121,2,0.)
mysolver.displacementBC("Face",122,2,0.)




mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("damage",IPField.DAMAGE, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 101, 0)
mysolver.archivingForceOnPhysicalGroup("Face", 102, 0)

mysolver.solve()

