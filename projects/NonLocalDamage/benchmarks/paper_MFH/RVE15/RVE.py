#coding-Utf-8-*-
from gmshpy import *
#from nonLocalDamagepyDebug import*
from nonLocalDamagepy import*

#script to launch composite problem with a python script

# material law
lawnonlocal = 1 # unique number of law
lawlinear = 2 # unique number of law
rho = 7850. # Bulk mass


# geometry
meshfile="RVE.msh" # name of mesh file
propertiesLC="lemaitreChaboche.i01" #properties file
propertiesLin="linear.i01" #properties file
propertiesNoDamage="noDamage.i01" #properties file

# solver
sol = 2  # Gmm=0 (default) Taucs=1 PETsc=2
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 3000   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=10 # Number of step between 2 archiving (used only if soltype=1)

#  compute solution and BC (given directly to the solver
# creation of law
law1 = NonLocalDamageMaterialLaw(lawnonlocal,rho,propertiesLC)
law2 = NonLocalDamageMaterialLaw(lawlinear,rho,propertiesLin)

# creation of ElasticField
Mfield = 1001 # number of the field (physical number of Mtrix)
Ffield = 1002 # number of the field (physical number of Fiber)

space1 = 0 # function space (Lagrange=0)

Matixfield = NonLocalDamageDomain(1000,Mfield,space1,lawnonlocal)
Fiberfield = NonLocalDamageDomain(1000,Ffield,space1,lawlinear)

#myfield1.matrixByPerturbation(1,1e-8)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(Matixfield)
mysolver.addDomain(Fiberfield)
mysolver.addMaterialLaw(law1)
mysolver.addMaterialLaw(law2)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)

# BC
# fixed face
mysolver.displacementBC("Face",101,0,0.)

mysolver.displacementBC("Face",111,1,0.)
mysolver.constraintBC("Face",112,1)

mysolver.displacementBC("Face",121,2,0.)
mysolver.displacementBC("Face",122,2,0.)


# displacement
d1=0.000014
d2=0.000021
d3=0.0
#0.00002567  0.008665;
cyclicFunction1=cycleFunctionTime(ftime/4., d1, 3.*ftime/4., d2,ftime, d3); 

#mysolver.displacementBC("Face",6,0,cyclicFunction1)
#mysolver.displacementBC("Face",7,0,cyclicFunction1)
#mysolver.displacementBC("Face",8,0,cyclicFunction1)
#mysolver.displacementBC("Face",9,0,cyclicFunction1)
mysolver.displacementBC("Face",102,0,cyclicFunction1)

#mysolver.displacementBC("Face",102,0,d1)





# needed resultes
mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)
mysolver.internalPointBuildView("damage",IPField.DAMAGE, 1, 1)
mysolver.internalPointBuildView("epl",IPField.PLASTICSTRAIN, 1, 1)
mysolver.archivingForceOnPhysicalGroup("Face", 101, 0)
mysolver.archivingForceOnPhysicalGroup("Face", 102, 0)

mysolver.solve()

