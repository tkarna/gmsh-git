//
// C++ Interface: matrialLaw
//
// Description: Class with definition of materialLaw for non local damage
//
//
// Author:   (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "NonLocalDamageMaterialLaw.h"
#include "ipstate.h"
#include "NonLocalDamageIPVariable.h"
#include "MInterfaceElement.h"

#include <fstream>

void NonLocalDamageMaterialLaw::createIPState(IPStateBase* &ips,const bool* state_,const MElement *ele, const int nbFF_, const int gpt) const
{
  // check interface element
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  int nsdv = _nldlaw.getNsdv();
  IPVariable* ipvi = new  NonLocalDamageIPVariable(nsdv,inter);
  IPVariable* ipv1 = new  NonLocalDamageIPVariable(nsdv,inter);
  IPVariable* ipv2 = new  NonLocalDamageIPVariable(nsdv,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
  _nldlaw.createIPState((dynamic_cast <NonLocalDamageIPVariable*> (ipvi))->getIPNonLocalDamage(),
                        (dynamic_cast <NonLocalDamageIPVariable*> (ipv1))->getIPNonLocalDamage(),
			(dynamic_cast <NonLocalDamageIPVariable*> (ipv2))->getIPNonLocalDamage());

}

void NonLocalDamageMaterialLaw::createIPVariable(IPVariable* &ipv,const MElement *ele, const int nbFF) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL){
    inter=false;
  }
  ipv = new  NonLocalDamageIPVariable(_nldlaw.getNsdv(),inter);
}


void NonLocalDamageMaterialLaw::stress(IPVariable* ipv, const IPVariable* ipvp, const bool stiff)
{
  /* get ipvariable */
  NonLocalDamageIPVariable* ipvcur = dynamic_cast < NonLocalDamageIPVariable *> (ipv);
  const NonLocalDamageIPVariable* ipvprev= dynamic_cast <const  NonLocalDamageIPVariable *> (ipvp);
  
  /* strain xyz */
  const STensor3& F1 = ipvcur->getConstRefToDeformationGradient();
  const STensor3& F0 = ipvprev->getConstRefToDeformationGradient();

  /* data for J2 law */
  IPNonLocalDamage* q1 = ipvcur->getIPNonLocalDamage();
  const IPNonLocalDamage* q0 = ipvprev->getIPNonLocalDamage();

  /* compute stress */
  _nldlaw.constitutive(F0,F1,ipvcur->getRefToFirstPiolaKirchhoffStress(),q0,q1,ipvcur->getRefToTangentModuli(),ipvcur->dNonLocalPlasticStrainDStrain,
  			    ipvcur->dStressDNonLocalPlasticStrain,
                            ipvcur->dLocalPlasticStrainDNonLocalPlasticStrain,stiff);

}

