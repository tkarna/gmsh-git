//
// C++ Interface: matrialLaw
//
// Description: Class with definition of materialLaw for non local damage
//
//
// Author: L. Noels , (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef NONLOCALDAMAGEMATERIALLAW_H_
#define NONLOCALDAMAGEMATERIALLAW_H_

#ifndef SWIG
#include "NonLocalDamageIPVariable.h"
#include "mlawNonLocalDamage.h"
#include "nonLocalDamageLaw.h"
#endif

class NonLocalDamageMaterialLaw : public materialLaw
{

 protected:
  double _rho;
  mlawNonLocalDamage _nldlaw;


 public:
  NonLocalDamageMaterialLaw(const int num, const double rho, const char *propName, const bool init=true) :
                                                              materialLaw(num,init), _rho(rho),
                                                             _nldlaw(num,rho,propName)
  {

  }
  ~NonLocalDamageMaterialLaw() {};


#ifndef SWIG
  NonLocalDamageMaterialLaw(const NonLocalDamageMaterialLaw &source) : materialLaw(source), _nldlaw(source._nldlaw)
  {

  }

  // set the time of _j2law
  virtual void setTime(const double t,const double dtime){
    this->_timeStep = dtime;
    this->_currentTime = t;
    _nldlaw.setTime(t,dtime);
  }
  virtual materialLaw::matname getType() const {return _nldlaw.getType();}
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt=0) const;
  virtual void createIPVariable(IPVariable* &ipv,const MElement *ele=NULL, const int nbFF_=0) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const{return _nldlaw.soundSpeed();} // or change value ??
  virtual void stress(IPVariable*ipv, const IPVariable*ipvprev, bool stiff);
  virtual double getRho() const{return _rho;}

#endif
};
#endif // NONLOCALDAMAGEMATERIALLAW_H_
