//
// C++ Interface: NonLocalDamageDomain
//
// Description: Interface class to used solver. Your term ha to be store in a domain
//
// Author:   (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "nonLinearMechSolver.h"
#include "NonLocalDamageTerms.h"
#include "NonLocalDamageDomain.h"
#include "NonLocalDamageFilterDofComponent.h"
NonLocalDamageDomain::NonLocalDamageDomain (const int tag,const int phys, const int ws, const int lnum,
                                          const bool fdg) : partDomain(tag,phys,ws,fdg), _lnum(lnum)
{
  switch(_wsp){
   case functionSpaceType::Lagrange:
      _space = new NonLocalDamageLagrangeFunctionSpace(tag);
    break;
   case functionSpaceType::Inter :
    break; // InterfaceDomain set function space later
   default:
    Msg::Error("Function space type is unknown on domain %d",_phys);
  }
 }

NonLocalDamageDomain::NonLocalDamageDomain(const NonLocalDamageDomain &source) : partDomain(source), _lnum(source._lnum),
                                                                               _mlaw(source._mlaw), _space(source._space){}

NonLocalDamageDomain& NonLocalDamageDomain::operator=(const partDomain &source)
{
    partDomain::operator=(source);
    const NonLocalDamageDomain *src = dynamic_cast<const NonLocalDamageDomain *> (&source);
    _mlaw  = src->_mlaw;
    _space = src->_space;
    _lnum  = src->_lnum;
    return *this;
}

void NonLocalDamageDomain::setMaterialLaw(const std::map<int,materialLaw*> maplaw)
{
  if(!setmaterial){
    for(std::map<int,materialLaw*>::const_iterator it=maplaw.begin(); it!=maplaw.end(); ++it){
      if(it->first == _lnum){
        _mlaw = it->second;
        _mlaw->initLaws(maplaw);
      }
    }
    setmaterial = true;
  }

}

void NonLocalDamageDomain::matrixByPerturbation(const int ibulk, const double eps)
{
  this->setBulkMatrixByPerturbation(ibulk,eps);
}

FunctionSpaceBase* NonLocalDamageDomain::getSpaceForBC(const nonLinearBoundaryCondition &boundary, const groupOfElements *groupBC) const
{
  FunctionSpaceBase* spacebc;
  if(boundary.getType() == nonLinearBoundaryCondition::DIRICHLET)
  {
    const nonLinearDirichletBC* diri = dynamic_cast<const nonLinearDirichletBC*>(&boundary);
    switch(_wsp){
     case functionSpaceType::Lagrange:
       spacebc = new NonLocalDamageBoundaryConditionLagrangeFunctionSpace(_tag,diri->_comp);
       break;
     default :
       Msg::Error("Unknown function space type for dirichlet BC %d on domain %d",boundary._tag,_tag);
    }
  }
  else if(boundary.getType() == nonLinearBoundaryCondition::NEUMANN)
  {
    const nonLinearNeumannBC *neu = dynamic_cast<const nonLinearNeumannBC*>(&boundary);
    switch(_wsp){
     case functionSpaceType::Lagrange :
       spacebc = new NonLocalDamageBoundaryConditionLagrangeFunctionSpace(_tag,neu->_comp);
       break;
     default :
      Msg::Error("Unknown function space type on neumman BC %d on domain %d",boundary._tag,_tag);
    }
  }
  else if(boundary.getType() == nonLinearBoundaryCondition::INITIAL)
  {
    switch(_wsp){
     case functionSpaceType::Lagrange:
       spacebc = new NonLocalDamageLagrangeFunctionSpace(_tag);
       break;
     default :
       Msg::Error("Unknown function space type for dirichlet BC %d on domain %d",boundary._tag,_tag);
    }
  }
  return spacebc;
}

QuadratureBase* NonLocalDamageDomain::getQuadratureRulesForNeumannBC(nonLinearNeumannBC &neu) const
{
  QuadratureBase *integ;
  if((neu.onWhat == nonLinearBoundaryCondition::PRESSURE) or (neu.onWhat == nonLinearBoundaryCondition::ON_FACE)){
     Msg::Error("Unknown function space type for Neumann BC");
  }
  return integ;
}

void NonLocalDamageDomain::initializeTerms(unknownField *uf,IPField*ip, std::vector<nonLinearNeumannBC> &vNeumann)
{
  FunctionSpace<double>* space = dynamic_cast<FunctionSpace<double>*>(_space);
  ltermBulk = new ForceNonLocalDamageTerm(*space,_mlaw,ip);
  if(_bmbp)
    btermBulk = new BilinearTermPerturbation<double>(ltermBulk,*space,*space,uf,ip,this,_eps);
  else
    btermBulk = new StiffNonLocalDamageTerm(*space,_mlaw,ip);
  massterm = new MassNonLocalDamageTerm(*space,_mlaw);

  // term of neumannBC
  for(int i=0;i<vNeumann.size();i++){
    nonLinearNeumannBC &neu = vNeumann[i];
    for(int j=0;j<neu._vspace.size();j++){
      if(neu._vdom[j] == this)
      {
        FunctionSpace<double> *spneu = dynamic_cast<FunctionSpace<double>*>(neu._vspace[j]);
        neu._vterm[j] = new NonLocalDamageLoadTerm<double>(*spneu,neu._f);
      }
    }
  }
}

void NonLocalDamageDomain::computeIPVariable(AllIPState *aips,const unknownField *ufield, const IPStateBase::whichState ws, bool stiff)
{
  IntPt *GP;
  fullVector<double> unknowns;
  std::vector<Dof> R;
  // interface elements
  partDomain *dom = dynamic_cast<partDomain*>(this);
  // bulk
  for (groupOfElements::elementContainer::const_iterator it = g->begin(); it != g->end(); ++it){
    MElement *e = *it;
    R.clear();
    _space->getKeys(e,R);
    unknowns.resize(R.size());
    ufield->get(R,unknowns);
    this->computeIpv(aips,e,ws,_mlaw,unknowns, stiff);
  }
}


void NonLocalDamageDomain::computeIpv(AllIPState *aips,MElement *e, IPStateBase::whichState ws,materialLaw *mlaw__,
                                              fullVector<double> &unknowns, bool stiff)
{
//  FunctionSpace<double> *_space = dynamic_cast<FunctionSpace<double>*>(ef->getFunctionSpace());
  NonLocalDamageMaterialLaw *mlaw = dynamic_cast<NonLocalDamageMaterialLaw*>(mlaw__);
  FunctionSpace<double> *sp = dynamic_cast<FunctionSpace<double>*>(_space);
  IntPt *GP;
  std::vector<TensorialTraits<double>::GradType> Grads;
  std::vector<TensorialTraits<double>::ValType> Vals;
  int npts_bulk=integBulk->getIntPoints(e,&GP);
  int nbdof = _space->getNumKeys(e);
  int nbFF = e->getNumShapeFunctions();
  AllIPState::ipstateElementContainer *vips = aips->getIPstate(e->getNum());
  for(int j=0;j<npts_bulk;j++){
    //grad value at gauss point
    Grads.clear();
    // CHECK HERE WHAT WE NEED
    sp->gradf(e,GP[j].pt[0],GP[j].pt[1],GP[j].pt[2],Grads);
    Vals.clear();
    sp->fuvw(e,GP[j].pt[0],GP[j].pt[1],GP[j].pt[2], Vals);

    IPStateBase* ips = (*vips)[j];
    NonLocalDamageIPVariable *ipv = dynamic_cast<NonLocalDamageIPVariable*>(ips->getState(ws));
    NonLocalDamageIPVariable *ipvprev = dynamic_cast<NonLocalDamageIPVariable*>(ips->getState(IPStateBase::previous));
    //x y z
    STensor3 *deformationGradient = &(ipv->getRefToDeformationGradient());
    double *effectivePlasticStrain = &(ipv->getIPNonLocalDamage()->_nldEffectivePlasticStrain);
    deformationGradient->operator=(0.);
    deformationGradient->operator()(0,0) = 1.;
    deformationGradient->operator()(1,1) = 1.;
    deformationGradient->operator()(2,2) = 1.;
    *effectivePlasticStrain = 0.;
    for (int i = 0; i < nbFF; i++)
    { 
        //x y z: take compoenent x arbitrarily
	deformationGradient->operator()(0,0) += Grads[i+0*nbFF][0]*unknowns(i);
        deformationGradient->operator()(0,1) += Grads[i+0*nbFF][1]*unknowns(i);
        deformationGradient->operator()(0,2) += Grads[i+0*nbFF][2]*unknowns(i);
        deformationGradient->operator()(1,0) += Grads[i+0*nbFF][0]*unknowns(i+nbFF);
        deformationGradient->operator()(1,1) += Grads[i+0*nbFF][1]*unknowns(i+nbFF);
        deformationGradient->operator()(1,2) += Grads[i+0*nbFF][2]*unknowns(i+nbFF);
        deformationGradient->operator()(2,0) += Grads[i+0*nbFF][0]*unknowns(i+2.*nbFF);
        deformationGradient->operator()(2,1) += Grads[i+0*nbFF][1]*unknowns(i+2.*nbFF);
        deformationGradient->operator()(2,2) += Grads[i+0*nbFF][2]*unknowns(i+2.*nbFF);

        //epsbar: take compoenent epsbar
        *effectivePlasticStrain += Vals[i+3*nbFF]*unknowns(i+3*nbFF); 
    }
    mlaw->stress(ipv,ipvprev, stiff);
  }
}

FilterDof* NonLocalDamageDomain::createFilterDof(const int comp) const
{
  FilterDof* fdof = new NonLocalDamageFilterDofComponent(comp);
  return fdof;

}
void NonLocalDamageDomain::setGaussIntegrationRule(){
  groupOfElements::elementContainer::iterator it = g->begin();
  MElement *e = *it;
  // CEHCK HERE THE NUMBER OF POINTS BY DEBUG
  if(e->getTypeForMSH() == MSH_TET_4)
    integBulk = new GaussQuadrature(1); // under integration
  else if(e->getTypeForMSH() == MSH_TET_10)
    integBulk = new GaussQuadrature(2); // under integration
  else if(e->getTypeForMSH() == MSH_HEX_27)
    integBulk = new GaussQuadrature(4); // full integration
  else if(e->getTypeForMSH() == MSH_HEX_8)
    integBulk = new GaussQuadrature(2); // full integration
}

void NonLocalDamageDomain::createInterface(manageInterface &maninter)
{
}

MElement* NonLocalDamageDomain::createVirtualInterface(IElement *ie) const
{
}
MElement* NonLocalDamageDomain::createInterface(IElement *ie1, IElement *ie2) const
{
}

