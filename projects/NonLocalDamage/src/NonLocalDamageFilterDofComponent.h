//
// C++ Interface: terms
//
// Description: FilterDof for Non Local Damage
//
//
// Author:  , (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
#ifndef _NONLOCALDAMAGEFILTERRDOFCOMPONENT_H_
#define _NONLOCALDAMAGEFILTERRDOFCOMPONENT_H_
#include "dofManager.h"
class NonLocalDamageFilterDofComponent :public FilterDof
{
  int comp;
 public :
  NonLocalDamageFilterDofComponent(int comp_): comp(comp_) {}
  virtual bool operator()(Dof key)
  {
    int type=key.getType();
    int icomp,itag;
    Dof::getTwoIntsFromType(type, icomp, itag);
    if (icomp==comp) return true;
    return false;
  }
  virtual void filter(const std::vector<Dof> R, std::vector<Dof> &Rfilter){
    Rfilter.clear();
    for(int i=0; i<R.size();i++)
      if(this->operator()(R[i])) Rfilter.push_back(R[i]);
  }
};

#endif
