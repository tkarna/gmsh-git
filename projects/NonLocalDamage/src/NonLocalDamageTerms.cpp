//
// C++ Interface: terms
//
// Description: Elementary matrix terms for Non Local Damage
//
//
// Author:  (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "NonLocalDamageTerms.h"


void MassNonLocalDamageTerm::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const{
  // For 10 nodes tetra the traditional integration does not work
  // the weight of 6 nodes has to be prescribed 1/32 at corner and 7/48 for middle nodes
  if(ele->getTypeForMSH() == MSH_TET_10){
    // integration on Gauss' points to know area
    double vol = 0.;
    double onediv32 = 1./32.;
    int nbdof = BilinearTerm<double,double>::space1.getNumKeys(ele);
    int nbFF = ele->getNumShapeFunctions();
    m.resize(nbdof, nbdof,true); // true --> setAll 0.
    double jac[3][3];
    for (int i = 0; i < npts; i++)
    {
      // Coordonate of Gauss' point i
      double u = GP[i].pt[0]; double v = GP[i].pt[1]; double w = GP[i].pt[2];
      // Weight of Gauss' point i
      double weight = GP[i].weight;
      double detJ = ele->getJacobian(u, v, w, jac);

      vol+=weight*detJ;
    }
    double massele = vol*_rho;
    // mass in three directions
    for(int kk=0;kk<3;kk++){
       m(kk*nbFF,kk*nbFF) += onediv32*massele;
       m(1+kk*nbFF,1+kk*nbFF) += onediv32*massele;
       m(2+kk*nbFF,2+kk*nbFF) += onediv32*massele;
       m(3+kk*nbFF,3+kk*nbFF) += onediv32*massele;
       m(4+kk*nbFF,4+kk*nbFF) += 7.*massele/48.;
       m(5+kk*nbFF,5+kk*nbFF) += 7.*massele/48.;
       m(6+kk*nbFF,6+kk*nbFF) += 7.*massele/48.;
       m(7+kk*nbFF,7+kk*nbFF) += 7.*massele/48.;
       m(8+kk*nbFF,8+kk*nbFF) += 7.*massele/48.;
       m(9+kk*nbFF,9+kk*nbFF) += 7.*massele/48.;
    }
    // ADD MASS TO FOURTH DDL?
  }
  else{
    if(sym){
      // Initialization of some data
      int nbdof = BilinearTerm<double,double>::space1.getNumKeys(ele);
      int nbFF = ele->getNumShapeFunctions();
      m.resize(nbdof, nbdof,true); // true --> setAll 0.
      // sum on Gauss' points
      double jac[3][3];
      for (int i = 0; i < npts; i++)
      {
        // Coordonate of Gauss' point i
        double u = GP[i].pt[0]; double v = GP[i].pt[1]; double w = GP[i].pt[2];
        // Weight of Gauss' point i
        double weight = GP[i].weight;
        double detJ = ele->getJacobian(u, v, w, jac);
        Vals.clear();
        BilinearTerm<double,double>::space1.fuvw(ele,u, v, w, Vals);
        for(int j=0;j<nbFF; j++)
        {
          for(int k=0;k<nbFF;k++){
            // same mass in the 3 directions
            double mij = detJ*weight*_rho*Vals[j]*Vals[k];
            for(int kk=0;kk<3;kk++)
              m(j+kk*nbFF,k+kk*nbFF) += mij;
          }
        }
      }
 //     m.print("mass");
   }
   else
     Msg::Error("Mass matrix is not implemented for dg shell with two different function spaces");
  }
}

void StiffNonLocalDamageTerm::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &mStiff) const
{ 
  if (sym)
  {
    // Initialization of some data
    int nbdof = BilinearTerm<double,double>::space1.getNumKeys(ele);
    int nbFF = ele->getNumShapeFunctions();
    mStiff.resize(nbdof, nbdof,true); // true --> setAll(0.)
    mStiff.setAll(0.);
    std::vector<TensorialTraits<double>::GradType> Grads;
    std::vector<TensorialTraits<double>::GradType> Gradsipg;
    std::vector<TensorialTraits<double>::ValType> Vals;
    // get ipvariables
    ipf->getIPv(ele,vipv);

    // sum on Gauss' points
    double jac[3][3];
    SVector3 Bepsbar(3);
    SVector3 BTepsbar(3);
    SVector3 Bipg,newB,newBT;
    std::vector<SVector3> HBaverage;

   if(ele->getTypeForMSH() == MSH_HEX_8)
   {
      for(int k=0;k<nbFF;k++){
        SVector3 HBaverageTmp;
        for (int ipg = 0; ipg < npts; ipg++)
        {
          double uipg = GP[ipg].pt[0]; double vipg = GP[ipg].pt[1]; double wipg = GP[ipg].pt[2];
          Gradsipg.clear();
          space1.gradf(ele,uipg,vipg,wipg,Gradsipg);
          const STensor43 *Hipg =&vipv[ipg]->getConstRefToTangentModuli();
          Bipg(0) = Gradsipg[k+0*nbFF][0];
          Bipg(1) = Gradsipg[k+0*nbFF][1];
          Bipg(2) = Gradsipg[k+0*nbFF][2];
          for(int ll=0; ll<3; ll++)
          {
            for(int n=0; n<3; n++)
            {
              HBaverageTmp(ll) += (Hipg->operator()(0,0,ll,n)+Hipg->operator()(1,1,ll,n)+Hipg->operator()(2,2,ll,n))*Bipg(n)/3./npts;
            }
          }
        }
        HBaverage.push_back(HBaverageTmp);
      }
    }
    for (int i = 0; i < npts; i++)
    {
        // Coordonate of Gauss' point i
      double u = GP[i].pt[0]; double v = GP[i].pt[1]; double w = GP[i].pt[2];
      // Weight of Gauss' point i
      double weight = GP[i].weight;
      double detJ = ele->getJacobian(u, v, w, jac);
      // x, y, z
      const STensor43 *H =&vipv[i]->getConstRefToTangentModuli();

      const STensor3  *dpds = &vipv[i]->dNonLocalPlasticStrainDStrain;
      const STensor3  *dsdp = &vipv[i]->dStressDNonLocalPlasticStrain;
      double          SpBar = vipv[i]->dLocalPlasticStrainDNonLocalPlasticStrain;
      const STensor3  *cg = &vipv[i]->getIPNonLocalDamage()->_nldCharacteristicLengthMatrix;           // by Wu Ling
      /*const fullVector< double > *dsdp =&vipv[i]->getIPNonLocalDamage()->_nldCouplingStressEffectiveStrain;
      const fullVector< double > *dpds =&vipv[i]->getIPNonLocalDamage()->_nldCouplingEffectiveStrainStress;
      const double SpBar =   vipv[i]->getIPNonLocalDamage()->_nldSpBar;*/

      //double length = _mlaw->getLength();
      double eqRatio = 1./(cg->operator()(0,0));               //by Wu Ling
      Grads.clear();
      space1.gradf(ele,u,v,w,Grads);
      Vals.clear();
      space1.fuvw(ele,u, v, w, Vals);
      for(int j=0;j<nbFF; j++)
      {
        newBT(0) = Grads[j+0*nbFF][0];
        newBT(1) = Grads[j+0*nbFF][1];
        newBT(2) = Grads[j+0*nbFF][2];
        BTepsbar(0) = Grads[j+3*nbFF][0];
        BTepsbar(1) = Grads[j+3*nbFF][1];
	BTepsbar(2) = Grads[j+3*nbFF][2];
        for(int k=0;k<nbFF;k++){
          double ratio = detJ*weight;
	  // x y z
          newB(0) = Grads[k+0*nbFF][0];
          newB(1) = Grads[k+0*nbFF][1];
          newB(2) = Grads[k+0*nbFF][2];
          Bepsbar(0) = Grads[k+3*nbFF][0];
          Bepsbar(1) = Grads[k+3*nbFF][1];
	  Bepsbar(2) = Grads[k+3*nbFF][2];

          for(int kk=0;kk<3;kk++)
          {
             for(int ll=0; ll<3; ll++)
             {
               if(ele->getTypeForMSH() == MSH_HEX_8)
               {
                 for(int m=0; m<3; m++)
                 {
                   for(int n=0; n<3; n++)
                   {
                     mStiff(j+kk*nbFF,k+ll*nbFF) += ratio*(newBT(m)*H->operator()(kk,m,ll,n)*newB(n));
                   }
                   mStiff(j+kk*nbFF,k+ll*nbFF) -= ratio*(newBT(kk)*(H->operator()(0,0,ll,m)+H->operator()(1,1,ll,m)+H->operator()(2,2,ll,m))*newB(m))/3.;
                 }
                 mStiff(j+kk*nbFF,k+ll*nbFF) += ratio*(newBT(kk)*HBaverage[k](ll));
               }
               else
               {
                 for(int m=0; m<3; m++)
                 {
                   for(int n=0; n<3; n++)
                   {
                     mStiff(j+kk*nbFF,k+ll*nbFF) += ratio*(newBT(m)*H->operator()(kk,m,ll,n)*newB(n));
                   }
                 }
               }
            }
          }
          // epsbar
	  double BtB = 0.;
	  for(int m=0; m<3; m++)
          {
             for(int n=0; n<3; n++)
            {
             BtB += BTepsbar(m)*(cg->operator()(m,n))*Bepsbar(n);    // by Wu Ling
            }
          }
          mStiff(j+3*nbFF,k+3*nbFF) += eqRatio*ratio*(Vals[j+3*nbFF]*Vals[k+3*nbFF]*(1.-SpBar)+ BtB);
          // cross terms to check
          for(int kk=0;kk<3;kk++)
          {
            for(int m=0; m<3; m++)
            {
              mStiff(j+kk*nbFF,k+3*nbFF) += ratio*(newBT(m)*dsdp->operator()(kk,m)*Vals[k+3*nbFF]);
              mStiff(j+3*nbFF,k+kk*nbFF) += eqRatio*ratio*(Vals[j+3*nbFF]*dpds->operator()(kk,m)*newBT(m));
            }
          }

        }
      }
    }
   //mStiff.print("bulk");
  }
  else
    printf("not implemented\n");
}

void ForceNonLocalDamageTerm::get(MElement *ele,int npts,IntPt *GP,fullVector<double> &vFor) const
{
  // Initialization of some data
  int nbdof = NonLocalDamageLinearTerm::space1.getNumKeys(ele);
  int nbFF = ele->getNumShapeFunctions();
  vFor.resize(nbdof);
  vFor.scale(0.);
  double jac[3][3];
  SVector3 BT;
  fullVector<double> Bepsbar(3);
  fullVector<double> BTepsbar(3);
  std::vector<TensorialTraits<double>::GradType> Grads;
  std::vector<TensorialTraits<double>::ValType> Vals;

  // get gauss' point data
  ipf->getIPv(ele,vipv,vipvprev);
  double averagePressure = 0.;
  if(ele->getTypeForMSH() == MSH_HEX_8)
  {
    for (int i = 0; i < npts; i++)
    {
      const STensor3 *stress = &(vipv[i]->getConstRefToFirstPiolaKirchhoffStress());
      double pres = (stress->operator()(0,0)+stress->operator()(1,1)+stress->operator()(2,2))/3.;
      averagePressure += pres/npts;
    }
  }
  for (int i = 0; i < npts; i++)
  {
    double u = GP[i].pt[0]; double v = GP[i].pt[1]; double w = GP[i].pt[2];
    // Weight of Gauss' point i
    double weight = GP[i].weight;
    double detJ = ele->getJacobian(u, v, w, jac);
    const STensor3 *stress = &(vipv[i]->getConstRefToFirstPiolaKirchhoffStress());
    const STensor3 *cg = &vipv[i]->getIPNonLocalDamage()->_nldCharacteristicLengthMatrix;           // by Wu Ling
    double ratio = detJ*weight;
    double eqRatio = 1./(cg->operator()(0,0));                    //by Wu Ling
    double pres = (stress->operator()(0,0)+stress->operator()(1,1)+stress->operator()(2,2))/3.;

    Grads.clear();
    space1.gradf(ele,u, v, w, Grads); // a optimiser
    Vals.clear();
    space1.fuvw(ele,u, v, w, Vals);

    for(int j=0;j<nbFF; j++)
    {
      BT(0) = Grads[j+0*nbFF][0];
      BT(1) = Grads[j+0*nbFF][1];
      BT(2) = Grads[j+0*nbFF][2];
      BTepsbar(0) = Grads[j+3*nbFF][0];
      BTepsbar(1) = Grads[j+3*nbFF][1];
      BTepsbar(2) = Grads[j+3*nbFF][2];
      // x y z
      for(int kk=0;kk<3;kk++)
      {
        if(ele->getTypeForMSH() == MSH_HEX_8)
        {
          for(int m=0; m<3; m++)
          {
	      vFor(j+kk*nbFF) += ratio*(BT(m)*(stress->operator()(kk,m)));
          }
          vFor(j+kk*nbFF) -= ratio*(BT(kk)*(pres-averagePressure));
        }
        else
        {
          for(int m=0; m<3; m++)
          {
            vFor(j+kk*nbFF) += ratio*(BT(m)*stress->operator()(kk,m));
          }
        }
      }
      // epsbar
      for(int k=0;k<nbFF; k++)
      {
        Bepsbar(0) = Grads[k+3*nbFF][0];
        Bepsbar(1) = Grads[k+3*nbFF][1];
	Bepsbar(2) = Grads[k+3*nbFF][2];
        double BtB = 0.;
        for(int m=0; m<3; m++)  
        {
            for(int n=0; n<3; n++)
            {
             BtB += BTepsbar(m)*(cg->operator()(m,n))*Bepsbar(n);    // by Wu Ling
            }
        }
        vFor(j+3*nbFF) += eqRatio*ratio*(Vals[j+3*nbFF]*Vals[k+3*nbFF]+ BtB)*_data->operator()(k+3*nbFF);
      }
      vFor(j+3*nbFF) -= eqRatio*ratio*(Vals[j+3*nbFF]*vipv[i]->getIPNonLocalDamage()->_nldCurrentPlasticStrain);
    }
  }
  // vFor.print("interF");
}

