//
// C++ Interface: ipvariable
//
// Description: Class with definition of ipvariable for non linear damage
//
//
// Author:  , (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "NonLocalDamageIPVariable.h"
#include "ipstate.h"
#include "ipField.h"


NonLocalDamageIPVariable::NonLocalDamageIPVariable(int _nsdv, const bool oninter) : IPVariableMechanics()
{
  _nldipv = new IPNonLocalDamage(_nsdv);
  deformationGradient(0,0) = 1.;    
  deformationGradient(1,1) = 1.;    
  deformationGradient(2,2) = 1.;    
}
NonLocalDamageIPVariable::NonLocalDamageIPVariable(const NonLocalDamageIPVariable &source) : IPVariableMechanics(source)
{
  _nldipv = new IPNonLocalDamage(source.getIPNonLocalDamage()->getNsdv());
  deformationGradient       = source.deformationGradient; 
  firstPiolaKirchhoffStress = source.firstPiolaKirchhoffStress; 
  tangentModuli             = source.tangentModuli;

  dNonLocalPlasticStrainDStrain             = source.dNonLocalPlasticStrainDStrain;
  dStressDNonLocalPlasticStrain             = source.dStressDNonLocalPlasticStrain;
  dLocalPlasticStrainDNonLocalPlasticStrain = source.dLocalPlasticStrainDNonLocalPlasticStrain;

  _nldipv->operator= (*(source.getIPNonLocalDamage()));
}
NonLocalDamageIPVariable& NonLocalDamageIPVariable::operator=(const IPVariable &source)
{
  IPVariableMechanics::operator=(source);
  const NonLocalDamageIPVariable* src = dynamic_cast<const NonLocalDamageIPVariable*>(&source);
  _nldipv->operator= (*(src->getIPNonLocalDamage()));
  deformationGradient       = src->deformationGradient; 
  firstPiolaKirchhoffStress = src->firstPiolaKirchhoffStress; 
  tangentModuli             = src->tangentModuli;
  dNonLocalPlasticStrainDStrain             = src->dNonLocalPlasticStrainDStrain;
  dStressDNonLocalPlasticStrain             = src->dStressDNonLocalPlasticStrain;
  dLocalPlasticStrainDNonLocalPlasticStrain = src->dLocalPlasticStrainDNonLocalPlasticStrain;
  return *this;
}

double NonLocalDamageIPVariable::get(const int i) const
{ 
  return getIPNonLocalDamage()->get(i); 
}
double NonLocalDamageIPVariable::defoEnergy() const 
{ 
  return getIPNonLocalDamage()->defoEnergy(); 
}
double NonLocalDamageIPVariable::getEffectivePlasticStrain() const 
{ 
  return getIPNonLocalDamage()->_nldEffectivePlasticStrain;
}
double NonLocalDamageIPVariable::getCurrentPlasticStrain() const 
{ 
  return getIPNonLocalDamage()->_nldCurrentPlasticStrain;
}


