//
// C++ Interface: NonLocalDamageDomain
//
// Description: Interface class to used solver. Your term ha to be store in a domain
//
// Author:   (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef NONLOCALDAMAGEDOMAIN_H_
#define NONLOCALDAMAGEDOMAIN_H_
#ifndef SWIG
#include "partDomain.h"
// class for a domain with non local damage
#endif
class NonLocalDamageDomain : public partDomain{

 public:
  NonLocalDamageDomain(const int tag, const int phys,
             const int ws,const int lnum, const bool fdg=false);
  void matrixByPerturbation(const int ibulk, const double eps);

#ifndef SWIG
  // Constructors
 protected:
  materialLaw *_mlaw;
  int _lnum;
  FunctionSpaceBase *_space;

 public:
//  ~partDomain(){delete btermBulk; delete ltermBulk; delete integBulk;}
  // Dimension of domain
  NonLocalDamageDomain(const NonLocalDamageDomain &source);
  NonLocalDamageDomain& operator=(const partDomain &source);

  virtual int getDim() const{return 3;}
  virtual void initializeTerms(unknownField *uf,IPField *ip, std::vector<nonLinearNeumannBC> &vNeumann);
  // true is domain has interface terms
  virtual bool IsInterfaceTerms() const{return false;}
  // can be return const FunctionSpaceBase if the function of this class are declarated const
  virtual FunctionSpaceBase* getFunctionSpace() const {return _space;}

  virtual void computeIPVariable(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, bool stiff);
  virtual void computeIpv(AllIPState *aips,MElement *e, IPStateBase::whichState ws,
                           materialLaw *mlaw,fullVector<double> &disp, bool stiff);
  virtual void setGaussIntegrationRule();
  virtual void setMaterialLaw(const std::map<int,materialLaw*> maplaw);
  virtual materialLaw* getMaterialLaw() {return _mlaw;}
  virtual const materialLaw* getMaterialLaw() const{return _mlaw;}
  virtual int getLawNum() const{return _lnum;};
  // creation of interface. At least boundary to create interface domain
  // can be empty be not interdomain creation in this case
  virtual void createInterface(manageInterface &maninter);
  //Iedge has to be change in IElement
  virtual MElement* createVirtualInterface(IElement *ie) const;
  virtual MElement* createInterface(IElement* ie1,IElement* ie2) const;
  // Add for BC
  virtual FilterDof* createFilterDof(const int comp) const;
  virtual FunctionSpaceBase* getSpaceForBC(const nonLinearBoundaryCondition &boundary, const groupOfElements *groupBC) const;
  virtual QuadratureBase* getQuadratureRulesForNeumannBC(nonLinearNeumannBC &neu) const;

#endif
};
#endif
