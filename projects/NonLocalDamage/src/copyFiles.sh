ORIGINAL_DIR=~/Inc\ Tgtdam/
echo $ORIGINAL_DIR
cp -f "$ORIGINAL_DIR/matrix_operations.h" ../../NonLinearSolver/internalPoints/matrix_operations.h
cp -f "$ORIGINAL_DIR/matrix_operations.c" ../../NonLinearSolver/internalPoints/matrix_operations.cpp
cp -f "$ORIGINAL_DIR/ID.h" ../../NonLinearSolver/materialLaw/ID.h
cp -f "$ORIGINAL_DIR/j2plast.h" ../../NonLinearSolver/materialLaw/j2plast.h
cp -f "$ORIGINAL_DIR/j2plast.c" ../../NonLinearSolver/materialLaw/j2plast.cpp
cp -f "$ORIGINAL_DIR/rotations.h" ../../NonLinearSolver/internalPoints/rotations.h
cp -f "$ORIGINAL_DIR/rotations.c" ../../NonLinearSolver/internalPoints/rotations.cpp
cp -f "$ORIGINAL_DIR/homogenization.h" ../../NonLinearSolver/materialLaw/homogenization.h
cp -f "$ORIGINAL_DIR/homogenization.c" ../../NonLinearSolver/materialLaw/homogenization.cpp
cp -f "$ORIGINAL_DIR/homogenization_2rd.h" ../../NonLinearSolver/materialLaw/homogenization_2rd.h
cp -f "$ORIGINAL_DIR/homogenization_2rd.c" ../../NonLinearSolver/materialLaw/homogenization_2rd.cpp
cp -f "$ORIGINAL_DIR/material.h" ../../NonLinearSolver/materialLaw/material.h
cp -f "$ORIGINAL_DIR/material.cc" ../../NonLinearSolver/materialLaw/material.cpp
cp -f "$ORIGINAL_DIR/damage.h" ../../NonLinearSolver/materialLaw/damage.h
cp -f "$ORIGINAL_DIR/damage.cc" ../../NonLinearSolver/materialLaw/damage.cpp
cp -f "$ORIGINAL_DIR/clength.h" ../../NonLinearSolver/materialLaw/clength.h
cp -f "$ORIGINAL_DIR/clength.cc" ../../NonLinearSolver/materialLaw/clength.cpp
cp -f "$ORIGINAL_DIR/lccfunctions.h" ../../NonLinearSolver/materialLaw/lccfunctions.h
cp -f "$ORIGINAL_DIR/lccfunctions.c" ../../NonLinearSolver/materialLaw/lccfunctions.cpp



