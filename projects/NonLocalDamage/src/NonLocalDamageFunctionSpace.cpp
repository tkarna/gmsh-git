#include "NonLocalDamageFunctionSpace.h"

void NonLocalDamageLagrangeFunctionSpace::f(MElement *ele, double u, double v, double w, std::vector<ValType> &vals)
  {
    ele->getShapeFunctions(u,v,w,_ival);
    int nbFF = ele->getNumShapeFunctions();
    int valssize = vals.size();
    vals.resize(valssize + _ncomp*nbFF);
    for(int i=0;i<nbFF;i++)
      for(int l=0;l<_ncomp;l++)
        vals[valssize + i+l*nbFF] = _ival[i];
  }

void NonLocalDamageLagrangeFunctionSpace::gradf(MElement *ele, double u, double v, double w,std::vector<GradType> &grads)
  {
    ele->getGradShapeFunctions(u, v, w, _igrad);
    int nbFF = ele->getNumShapeFunctions();
    double jac[3][3];
    double invjac[3][3];
    const double detJ = ele->getJacobian(u, v, w, jac);
    inv3x3(jac, invjac);
    int gradssize = grads.size();
    grads.resize(gradssize + _ncomp*nbFF);
    for(int i = 0; i < nbFF; ++i)
      for(int l=0;l<_ncomp;l++)
      {
        gradt(0)=invjac[0][0] * _igrad[i][0] + invjac[0][1] * _igrad[i][1] + invjac[0][2] * _igrad[i][2];
        gradt(1)=invjac[1][0] * _igrad[i][0] + invjac[1][1] * _igrad[i][1] + invjac[1][2] * _igrad[i][2];
        gradt(2)=invjac[2][0] * _igrad[i][0] + invjac[2][1] * _igrad[i][1] + invjac[2][2] * _igrad[i][2];
        grads[gradssize + i+l*nbFF]=gradt;
      }
  }
void NonLocalDamageLagrangeFunctionSpace::gradfuvw(MElement *ele, double u, double v, double w, std::vector<GradType> &grads)
  {
    ele->getGradShapeFunctions(u,v,w,_igrad);
    int nbFF = ele->getNumShapeFunctions();
    int gradssize = grads.size();
    grads.resize(gradssize + _ncomp*nbFF);
    for(int i=0;i<nbFF;i++){
      gradt(0) = _igrad[i][0];
      gradt(1) = _igrad[i][1];
      gradt(2) = _igrad[i][2];
      for(int l=0;l<_ncomp; l++)
        grads[gradssize + i + l*nbFF] = gradt;
    }
  }
  // = f
void NonLocalDamageLagrangeFunctionSpace::fuvw(MElement *ele, double u, double v, double w,std::vector<ValType> &vals)
  {
    ele->getShapeFunctions(u,v,w,_ival);
    int nbFF = ele->getNumShapeFunctions();
    int valssize = vals.size();
    vals.resize(valssize + _ncomp*nbFF);
    for(int i=0;i<nbFF;i++)
      for(int l=0;l<_ncomp;l++)
        vals[valssize + i+l*nbFF] = _ival[i];
  }
void NonLocalDamageLagrangeFunctionSpace::hessfuvw(MElement *ele, double u, double v, double w, std::vector<HessType> &hess)
  {
    ele->getHessShapeFunctions(u,v,w,_ihess);
    int nbFF = ele->getNumShapeFunctions();
    int hesssize = hess.size();
    hess.resize(hesssize + _ncomp*nbFF);
    for(int i=0;i<nbFF;i++){
      hesst(0,0) = _ihess[i][0][0]; hesst(0,1) = _ihess[i][0][1]; hesst(0,2) = _ihess[i][0][2];
      hesst(1,0) = _ihess[i][1][0]; hesst(1,1) = _ihess[i][1][1]; hesst(1,2) = _ihess[i][1][2];
      hesst(2,0) = _ihess[i][2][0]; hesst(2,1) = _ihess[i][2][1]; hesst(2,2) = _ihess[i][2][2];
      for(int l=0;l<_ncomp; l++)
        hess[hesssize + i + l*nbFF] = hesst;
    }
  }
