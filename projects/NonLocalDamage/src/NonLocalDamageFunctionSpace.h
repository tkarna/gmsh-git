//
// C++ Interface: terms
//
// Description: FunctionSpace used (scalar lagragian function space in 3D)
//
//
// Author: , (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef NONLOCALDAMAGEFUNCTIONSPACE_H_
#define NONLOCALDAMAGEFUNCTIONSPACE_H_
#include "MElement.h"
#include "functionSpace.h"
#include "dofManager.h"
#include "groupOfElements.h"

class NonLocalDamageLagrangeFunctionSpace : public FunctionSpace<double>
{
 protected :
  std::vector<int> comp;
  const int _tag;
  const int _ncomp; // comp size;
 public:
 private: // avoid constant reallocation
   double _ival[256];
   double _igrad[256][3];
   double _ihess[256][3][3];
   GradType gradt;
   HessType hesst;
 public:

  NonLocalDamageLagrangeFunctionSpace(int id) : _tag(id), _ncomp(4)
  {comp.push_back(0),comp.push_back(1),comp.push_back(2),comp.push_back(3);}
  NonLocalDamageLagrangeFunctionSpace(int id, int comp1_) : _tag(id), _ncomp(1)
  {
    comp.push_back(comp1_);
  }
  NonLocalDamageLagrangeFunctionSpace(int id, int comp1_, int comp2_) : _tag(id), _ncomp(2)
  {
    comp.push_back(comp1_); comp.push_back(comp2_);
  }
  NonLocalDamageLagrangeFunctionSpace(int id, int comp1_, int comp2_, int comp3_) : _tag(id), _ncomp(3)
  {
    comp.push_back(comp1_); comp.push_back(comp2_); comp.push_back(comp3_);
  }
  ~NonLocalDamageLagrangeFunctionSpace(){}

  virtual void f(MElement *ele, double u, double v, double w, std::vector<ValType> &vals);
  virtual void gradf(MElement *ele, double u, double v, double w,std::vector<GradType> &grads);
  virtual void gradfuvw(MElement *ele, double u, double v, double w, std::vector<GradType> &grads);
  virtual void hessfuvw(MElement *ele, double u, double v, double w, std::vector<HessType> &hess);
  // = f
  virtual void fuvw(MElement *ele, double u, double v, double w,std::vector<ValType> &vals);
  virtual int getNumKeys(MElement *ele) {if (ele->getParent()) ele = ele->getParent();return _ncomp*ele->getNumVertices();}
  virtual int getId(void) const {return _tag;}

  virtual void getKeys(MElement *ele, std::vector<Dof> &keys){
    for(int j=0;j<_ncomp;j++)
    {
      for(int v=0;v<ele->getNumVertices(); v++)
        keys.push_back(Dof(ele->getVertex(v)->getNum(),Dof::createTypeWithTwoInts(comp[j],_tag)));
    }
  }
};

class NonLocalDamageBoundaryConditionLagrangeFunctionSpace : public NonLocalDamageLagrangeFunctionSpace{
 public:
  NonLocalDamageBoundaryConditionLagrangeFunctionSpace(int id) : NonLocalDamageLagrangeFunctionSpace(id){}
  NonLocalDamageBoundaryConditionLagrangeFunctionSpace(int id, int comp1) : NonLocalDamageLagrangeFunctionSpace(id,comp1){}
  NonLocalDamageBoundaryConditionLagrangeFunctionSpace(int id, int comp1, int comp2) : NonLocalDamageLagrangeFunctionSpace(id,comp1,comp2){}
  NonLocalDamageBoundaryConditionLagrangeFunctionSpace(int id, int comp1, int comp2, int comp3) : NonLocalDamageLagrangeFunctionSpace(id,comp1,comp2,comp3){}
  ~NonLocalDamageBoundaryConditionLagrangeFunctionSpace();

  virtual void getKeys(MElement *ele, std::vector<Dof> &keys){
    NonLocalDamageLagrangeFunctionSpace::getKeys(ele,keys);
  }
};
#endif // NONLOCALDAMAGEFUNCTIONSPACE_H_
