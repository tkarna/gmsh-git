%feature("autodoc","1");
#ifdef _DEBUG
  %module nonLocalDamagepyDebug
#else
  %module nonLocalDamagepy
#endif
%include std_string.i
%include std_vector.i
%include std_map.i

%include nlmechsolpy.i

%{
  #include "NonLocalDamageMaterialLaw.h"
  #include "NonLocalDamageDomain.h"
%}

%include "NonLocalDamageMaterialLaw.h"
%include "NonLocalDamageDomain.h"

