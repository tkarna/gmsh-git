//
// C++ Interface: terms
//
// Description: Elementary matrix terms for non local damage
//
//
// Author: , (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef NONLOCALDAMAGETERMS_H
#define NONLOCALDAMAGETERMS_H
#include "SVector3.h"
#include <string>
#include "NonLocalDamageFunctionSpace.h"
#include "NonLocalDamageMaterialLaw.h"
#include "simpleFunction.h"
#include "unknownField.h"
#include "ipField.h"
#include "terms.h"

template<class T1,class T2> class NonLocalDamageBilinearTerm : public BiNonLinearTermBase
{
 protected :
  FunctionSpace<T1>& space1;
  FunctionSpace<T2>& space2;
 public :
  NonLocalDamageBilinearTerm(FunctionSpace<T1>& space1_,FunctionSpace<T2>& space2_) : space1(space1_),space2(space2_) {}
  virtual ~NonLocalDamageBilinearTerm() {}
};

template<class T1> class NonLocalDamageLinearTerm : public nonLinearTermBase<double>
{
 protected :
  FunctionSpace<T1>& space1;
 public :
  NonLocalDamageLinearTerm(FunctionSpace<T1>& space1_) : space1(space1_){}
  // for shell specific integration rule --> return only integrated redsults
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &vv) const
  {
    Msg::Error("Can't use integration by point for shell");
  };
  virtual ~NonLocalDamageLinearTerm() {}
};

template<class T1> class NonLocalDamageLoadTerm : public LinearTerm<T1>
{
  simpleFunction<typename TensorialTraits<T1>::ValType> *Load;
 protected:
  // Data of get function (Allocated once)
  mutable int nbdof;
  mutable double jac[3][3];
  mutable double u,v,w,weight,detJ;
  mutable std::vector<typename TensorialTraits<T1>::ValType> Vals;
  mutable SPoint3 p;
  mutable typename TensorialTraits<T1>::ValType load;
 public :
  NonLocalDamageLoadTerm(FunctionSpace<T1>& space1_,
               simpleFunction<typename TensorialTraits<T1>::ValType> *Load_) : LinearTerm<T1>(space1_),Load(Load_),
                                                                                nbdof(0), u(0.), v(0.), w(0.), weight(0.),
                                                                                detJ(0.)
  {

  }
  virtual ~NonLocalDamageLoadTerm() {}

  virtual void get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const
  {
    // in the function space we have only x, y, z or bareps, once at a time
    nbdof=LinearTerm<double>::space1.getNumKeys(ele);
    jac[3][3];
    m.resize(nbdof);
    m.scale(0.);
    for (int i = 0; i < npts; i++)
    {
      u = GP[i].pt[0]; v = GP[i].pt[1]; w = GP[i].pt[2];
      weight = GP[i].weight; detJ = ele->getJacobian(u, v, w, jac);
      Vals.clear();
      LinearTerm<T1>::space1.f(ele,u, v, w, Vals);
      ele->pnt(u, v, w, p);
      load=Load->operator()(p.x(),p.y(),p.z());
      double loadvalue = load*weight*detJ;
      for (int j = 0; j < nbdof ; ++j)
        m(j)+=Vals[j]*loadvalue;
    }
  }
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &vv) const
  {
    Msg::Error("Define me get by integration gauss NonLocalDamageLoadTerm");
  }
  virtual LinearTermBase<double>* clone () const
  {
    return new NonLocalDamageLoadTerm(this->space1,Load);
  }
};

class ForceNonLocalDamageTerm : public NonLocalDamageLinearTerm<double>
{
 protected :
  const IPField *ipf;
  const NonLocalDamageMaterialLaw *_mlaw;
  const fullVector<double> *_data;
  // data of get function (Allocated 1 time)
 private:
  mutable const NonLocalDamageIPVariable* vipv[256]; // max 256 Gauss' point ??
  mutable const NonLocalDamageIPVariable* vipvprev[256];
 public :
  ForceNonLocalDamageTerm(FunctionSpace<double>& space1_,const materialLaw* mlaw,
                                       const IPField *ip) : NonLocalDamageLinearTerm<double>(space1_),
                                                                                 ipf(ip), _data(NULL)
  {
    _mlaw = dynamic_cast<const NonLocalDamageMaterialLaw*>(mlaw);
  }
  virtual ~ForceNonLocalDamageTerm(){}
  virtual void get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const;
  virtual const bool isData() const {return true;} //bareps appears in internal forces, so should be perturbated
  virtual void set(const fullVector<double> *datafield){_data = datafield;}
  virtual LinearTermBase<double>* clone () const
  {
    return new ForceNonLocalDamageTerm(space1,_mlaw,ipf);
  }
}; // ForceNonLocalDamageTerm


// elementary mass matrix
class MassNonLocalDamageTerm : public BilinearTerm<double,double>{
 protected:
  double _rho;
  bool sym;
  // Allocation of space for variables used in get function
  mutable std::vector<TensorialTraits<double>::ValType> Vals;
 public:
  MassNonLocalDamageTerm(FunctionSpace<double> &space1_,
                materialLaw *mlaw): BilinearTerm<double,double>(space1_,space1_), sym(true)
 {
    NonLocalDamageMaterialLaw *slaw = dynamic_cast<NonLocalDamageMaterialLaw*>(mlaw);
    _rho = slaw->getRho();
  }
  MassNonLocalDamageTerm(FunctionSpace<double> &space1_, FunctionSpace<double> &space2_,
                 materialLaw *mlaw): BilinearTerm<double,double>(space1_,space2_)
  {
    sym=(&space1_==&space2_);
    NonLocalDamageMaterialLaw *slaw = dynamic_cast<NonLocalDamageMaterialLaw*>(mlaw);
    _rho = slaw->getRho();
  }
  MassNonLocalDamageTerm(FunctionSpace<double> &space1_, FunctionSpace<double> &space2_,
                 double rho): BilinearTerm<double,double>(space1_,space2_), _rho(rho)
  {
    sym=(&space1_==&space2_);
  }
  virtual void get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me get by integration point MassNonLocalDamageTerm");
  }
  virtual BilinearTermBase* clone () const
  {
    return new MassNonLocalDamageTerm(space1,space2,_rho);
  }
};

class StiffNonLocalDamageTerm : public BilinearTerm<double,double>
{
 protected :
  const NonLocalDamageMaterialLaw *_mlaw;
  bool sym;
  const IPField *ipf;
  // Data for get function (Allocated one time)
  mutable const NonLocalDamageIPVariable *vipv[256]; // max 256 Gauss' points
 public :
  StiffNonLocalDamageTerm(FunctionSpace<double>& space1_,FunctionSpace<double>& space2_,materialLaw* mlaw,
                                  IPField *ip) : BilinearTerm<double,double>(space1_,space2_),
                                                            ipf(ip),
                                                            _mlaw(dynamic_cast<const NonLocalDamageMaterialLaw*>(mlaw))

  {
    sym=(&space1_==&space2_);
  }
  StiffNonLocalDamageTerm(FunctionSpace<double>& space1_, const materialLaw* mlaw,
                                  const IPField *ip) : BilinearTerm<double,double>(space1_,space1_),
                                                             ipf(ip),
                                                             _mlaw(dynamic_cast<const NonLocalDamageMaterialLaw*>(mlaw))
  {
    sym=true;
  }
  virtual ~StiffNonLocalDamageTerm(){}
  virtual void get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me get by gauss point StiffNonLocalDamageTerm");
  }
  virtual BilinearTermBase* clone () const
  {
    return new StiffNonLocalDamageTerm(space1,_mlaw,ipf);
  }
}; // StiffNonLocalDamageTerm

#endif // NONLOCALDAMAGETERMS_H
