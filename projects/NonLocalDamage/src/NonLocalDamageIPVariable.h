//
// C++ Interface: ipvariable
//
// Description: Class with definition of ipvariable for non local damage
//
//
// Author: (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef NONLOCALDAMAGEIPVARIABLE_H_
#define NONLOCALDAMAGEIPVARIABLE_H_
#include "ipvariable.h"
#include "ipNonLocalDamage.h"
#include "fullMatrix.h"
#include "matrix_operations.h"
#include "STensor43.h"

// type array for store in vector
class NonLocalDamageIPVariable : virtual public IPVariableMechanics
{
 public:
  STensor3                     deformationGradient; 
  STensor3                     firstPiolaKirchhoffStress; 
  STensor43                    tangentModuli; //include geometric terms
  // for coupling
  STensor3                     dNonLocalPlasticStrainDStrain;
  STensor3                     dStressDNonLocalPlasticStrain;
  double                      dLocalPlasticStrainDNonLocalPlasticStrain;

 protected:
  IPNonLocalDamage *_nldipv;
 public:
  NonLocalDamageIPVariable(int _nsdv, const bool oninter=false);
  NonLocalDamageIPVariable(const NonLocalDamageIPVariable &source);
  NonLocalDamageIPVariable& operator=(const IPVariable &source);

 /* specific function */
  IPNonLocalDamage* getIPNonLocalDamage(){return _nldipv;}
  const IPNonLocalDamage* getIPNonLocalDamage() const{return _nldipv;}
  virtual ~NonLocalDamageIPVariable()
  {
     delete _nldipv;   
  }
  // Archiving data
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double getEffectivePlasticStrain() const;
  virtual double getCurrentPlasticStrain() const;

  virtual const STensor3 &getConstRefToDeformationGradient() const {return deformationGradient;}
  virtual STensor3 &getRefToDeformationGradient() {return deformationGradient;}

  virtual const STensor3 &getConstRefToFirstPiolaKirchhoffStress() const {return firstPiolaKirchhoffStress;}
  virtual STensor3 &getRefToFirstPiolaKirchhoffStress() {return firstPiolaKirchhoffStress;}

  virtual const STensor43 &getConstRefToTangentModuli() const {return tangentModuli;}
  virtual STensor43 &getRefToTangentModuli() {return tangentModuli;}

};
#endif //IPVARIABLESHELL
