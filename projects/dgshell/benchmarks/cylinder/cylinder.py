#-*-coding:Utf-8-*-
from gmshpy import *
from dgshellpy import *

#script for pinched cylinder benchmark
# material law
lawnum = 1
E = 3.e6
nu = 0.3
rho = 1000.
# geometry
h = 0.003
geofile = "cylinder.geo"
meshfile = "cylinder.msh"
# integration
nsimp = 1 

# solver
sol = 2 
beta1 = 100.
beta2 = 100.
beta3 = 100.
soltype = 1 
nstep = 1
ftime = 1.
tol = 1.e-5
nstepArch = 1

#  Compute solution and BC

# creation of material law
law1 = linearElasticShellLaw(lawnum,E,nu,h,nsimp,rho)

# creation of field
nfield = 99
fullDg = 1 
space1 = 0
myfield1 =dgLinearShellDomain(1000,nfield,space1,lawnum,fullDg)
myfield1.stabilityParameters(beta1,beta2,beta3)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,2,2,1)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)

# BC
mysolver.displacementBC("Edge",12,0,0.)
mysolver.displacementBC("Edge",14,1,0.)
#mysolver.displacementBC("Face",99,2,0.)
mysolver.displacementBC("Edge",13,0,0.)
mysolver.displacementBC("Edge",13,1,0.)
mysolver.displacementBC("Edge",13,2,0.)
mysolver.thetaBC(13)
mysolver.thetaBC(12)
mysolver.thetaBC(14)
mysolver.thetaBC(11)
mysolver.forceBC("Node",222,1,-0.5)

# archiving
mysolver.archivingNodeDisplacement(222,1)
#mysolver.archivingNodeDisplacement(333,1)
# solve
mysolver.solve()

