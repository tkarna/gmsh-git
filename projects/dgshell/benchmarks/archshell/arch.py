#-*-coding:Utf-8-*-
from dgshellpy import *
from gmshpy import*

# arch in bending
#material law
lawnum = 1
E = 3.e7
nu = 0.3
rho = 1000.

# geometry
h = 1.
geofile = "arch.geo"
meshfile ="arch.msh"
#  integration
nsimp = 1 

# solver
sol = 2 
beta1 = 10.
beta2 = 10.
beta3 = 10.
soltype = 0  
nstep = 1
ftime = 1.
tol = 1.e-6
nstepArch = 1

# Compute solution and BC

# creation of material law
law1 = linearElasticShellLaw(lawnum,E,nu,h,nsimp,rho)

# creation of field
nfield = 99
fullDg = 1 
space1 = 0 
myfield1 = dgLinearShellDomain(1000,nfield,space1,lawnum,fullDg)
myfield1.stabilityParameters(beta1,beta2,beta3)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
#mysolver.loadModel(meshfile)
mysolver.createModel(geofile,meshfile,2,3,1)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
#mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)

# BC
mysolver.displacementBC("Edge",11,1,0.)
mysolver.displacementBC("Edge",13,0,0.)
# mysolver.displacementBC("Edge",11,2,0.)
mysolver.displacementBC("Face",99,2,0.)
mysolver.thetaBC(13)
mysolver.thetaBC(11)
mysolver.forceBC("Edge",13,1,-5.e4)

# archiving
mysolver.archivingNodeDisplacement(333,1)
mysolver.archivingNodeDisplacement(111,0)
# solve
mysolver.solve()
