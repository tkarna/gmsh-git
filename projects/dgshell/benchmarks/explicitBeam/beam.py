#-*-coding:Utf-8-*-
from gmshpy import *
from dgshellpy import *

#script to launch beam problem with a python script

# material law
lawnum = 1 # unique number of the law
E = 10000.e6 # Young's modulus
nu = 0.   # Poisson's ratio
rho = 10000. # Bulk mass

# geometry
h = 0.01  # thickness
geofile="beam.geo"
meshfile="beam.msh" # name of mesh file
# integration
nsimp = 1 # number of Simpson's points (odd)

# solver
sol = 2 #Gmm=0 (default) Taucs=1 PETsc=2
beta1 = 10. # value of stabilization parameter
beta2 = 10.
beta3 = 10.
soltype = 2 # StaticLinear=0 (default) StaticNonLinear=1 Explicit=2
nstep = 50   # number of step (soltype=1 or 2 step for time step evaluation)
ftime =0.005  # Final time (used only if soltype=1 or 2)
tol=1.e-4   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=50 # Number of step between 2 archiving (used only if soltype=1 or 2)
# explicit scheme
#beta=0.5 #0.5
#gamma=1. #1.
gamma_s = 0.8
#alpham=0.5 #0.5
rhoinfty = 1.

# compute solution and BC (given directly to the solver

# creation of law
law1 = linearElasticShellLaw(lawnum,E,nu,h,nsimp,rho)

# creation of ElasticField
nfield =99 # number of the field (physical number of surface)
fullDg = 1 #  formulation CgDg=0 fullDg =1
space1 = 0 # function space Lagrange=0
myfield1 = dgLinearShellDomain(1000,nfield,space1,lawnum,fullDg)
myfield1.stabilityParameters(beta1,beta2,beta3)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,2,3,0)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
if soltype !=2 :
     mysolver.snlData(nstep,ftime,tol)
else :
     mysolver.explicitSpectralRadius(ftime,gamma_s,rhoinfty)
mysolver.explicitTimeStepEvaluation(nstep)
mysolver.stepBetweenArchiving(nstepArch)
# BC
mysolver.displacementBC("Edge",41,0,0.)
mysolver.displacementBC("Edge",41,1,0.)
mysolver.displacementBC("Edge",41,2,0.)
mysolver.initialBC("Face","Velocity",99,0,-1.)
# Archive
mysolver.archivingNodeVelocity(22,0)
mysolver.archivingNodeIP(22,0,0)
mysolver.archivingForceOnPhysicalGroup("Node",22,0)
mysolver.solve()
