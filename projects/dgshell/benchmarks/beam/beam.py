#-*-coding:Utf-8-*-
from gmshpy import *
from dgshellpy import *
# bending beam

# custom python BCfunction
def force_edge(x,y,z,t,extra):
    return extra*t

# material law
lawnum = 1 #  unique number of the law
E = 100.e9 # Young's modulus
nu = 0.3   #  Poisson's ratio
rho =7850. 
# geometry
h = 0.01  # thickness
meshfile="beam50.msh" # name of mesh file
# integration
nsimp = 3 #  number of Simpson's points (odd)

# solver
sol = 2 # Gmm=0 (default) Taucs=1 PETsc=2
beta1 = 10. # value of stabilization parameter
beta2 = 10.
beta3 = 1.
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 5   # number of step (used only if soltype=1)
ftime =1.   #  Final time (used only if soltype=1)
tol=1.e-4  #  relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)

 # compute solution and BC (given directly to the solver

#  creation of law
law1 = linearElasticShellLaw(lawnum,E,nu,h,nsimp,rho)

#  creation of ElasticField
nfield =99 # number of the field (physical number of surface)
fullDg = 1 #  formulation CgDg=0 fullDg =1
space1 = 0 # function space Lagrange=0
myfield1 = dgLinearShellDomain(1000,nfield,space1,lawnum,fullDg)
myfield1.stabilityParameters(beta1,beta2,beta3)
#  creation of Solver
mysolver = nonLinearMechSolver(1000)
#mysolver.loadModel(meshfile)
mysolver.createModel("beam.geo","beam.msh",2,2,1)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
# BC
mysolver.displacementBC("Edge",41,0,0.)
mysolver.displacementBC("Edge",41,1,0.)
mysolver.displacementBC("Edge",41,2,0.)
#mysolver.displacementBC("Edge",21,0,0.)
#mysolver.displacementBC("Edge",21,1,0.)
#mysolver.displacementBC("Edge",21,2,0.)
#mysolver.displacementBC("Edge",21,2,0.4)
#mysolver.forceBC("Face",99,2,1000.)
force_magnitude = -1.e6 
fct1 = PythonBCfunctionDouble(force_edge,force_magnitude)
mysolver.forceBC("Edge",21,2,fct1)
# mysolver.pressureOnPhysicalGroupBC(99,1000.)
mysolver.thetaBC(41)
#mysolver:AddThetaConstraint(21)
mysolver.archivingForceOnPhysicalGroup("Edge",41,2)
mysolver.archivingNodeDisplacement(22,2)
mysolver.solve()
