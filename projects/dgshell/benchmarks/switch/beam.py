#-*-coding:Utf-8-*-
from gmshpy import *
from dgshellpy import *
# bending beam

# material law
lawnum = 1 #  unique number of the law
E = 100.e9 # Young's modulus
nu = 0.3   #  Poisson's ratio
rho =7850. 
# geometry
h = 0.01  # thickness
geofile="beam.geo"
meshfile="beam.msh" # name of mesh file
# integration
nsimp = 3 #  number of Simpson's points (odd)

# solver
sol = 0 # Gmm=0 (default) Taucs=1 PETsc=2
beta1 = 10. # value of stabilization parameter
beta2 = 10.
beta3 = 0.0001
soltype = 2 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 1000   # number of step (used only if soltype=1)
ftime = 1.  #0.005   #  Final time (used only if soltype=1)
tol= 0.1  #  relative tolerance for NR scheme (used only if soltype=1)
nstepArch=100000 # Number of step between 2 archiving (used only if soltype=1)
gams =0.366666 
 # compute solution and BC (given directly to the solver

#  creation of law
law1 = linearElasticShellLaw(lawnum,E,nu,h,nsimp,rho)

#  creation of ElasticField
nfield =99 # number of the field (physical number of surface)
fullDg = 1 #  formulation CgDg=0 fullDg =1
space1 = 0 # function space Lagrange=0
myfield1 = dgLinearShellDomain(1000,nfield,space1,lawnum,fullDg)
myfield1.stabilityParameters(beta1,beta2,beta3)
#myfield1.gaussIntegration(dgLinearShellDomain.Lobatto,5,3)
#  creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,2,3,0)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.dynamicRelaxation(gams,ftime,tol,2)
mysolver.stepBetweenArchiving(nstepArch)
# BC
mysolver.displacementBC("Edge",41,0,0.)
mysolver.displacementBC("Edge",41,1,0.)
mysolver.displacementBC("Edge",41,2,0.)
#mysolver.displacementBC("Edge",21,2,0.4)
fct1 = simpleFunctionTimeDouble(-1e6,0)
mysolver.forceBC("Edge",21,2,fct1)
mysolver.thetaBC(41)

mysolver.archivingForceOnPhysicalGroup("Edge",41,2)
mysolver.archivingNodeDisplacement(22,2)
mysolver.archivingNodeDisplacement(22,1)
mysolver.archivingNodeDisplacement(22,0)
mysolver.internalPointBuildView("Von Mises",-1)
#mysolver.energyComputation(10)
mysolver.solve()
# continue dynamic analysis after
ftime =  1.e-5
nstep = 10
nstepArch = 1000
rhoinfty = 0.99
mysolver.Scheme(2)
mysolver.explicitSpectralRadius(ftime,gams,rhoinfty)
mysolver.explicitTimeStepEvaluation(nstep)
mysolver.stepBetweenArchiving(nstepArch)

# new BC
mysolver.resetBoundaryConditions()
mysolver.displacementBC("Edge",41,0,0.)
mysolver.displacementBC("Edge",41,1,0.)
mysolver.displacementBC("Edge",41,2,0.)
mysolver.thetaBC(41)
mysolver.solve()

