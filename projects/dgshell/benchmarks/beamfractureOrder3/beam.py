#-*-coding:Utf-8-*-
from gmshpy import *
from dgshellpy import *
#script to launch beam problem with a python script

#  data 
# material law
lawnum = 1
E = 71.e9 # Young's modulus
nu = 0.   #  Poisson's ratio
rho = 1000. # bulk mass
lawcnum =2
Gc=8800.  # fracture energy [J/m²]
sigmac=400.e6 # fracture limit in tension [Pa]
beta =0.87 # ratio KII/KI
mu =0.41 # friction coefficient ??
# geometry
h = 0.0025  # thickness
geofile="beam.geo"
meshfile="beamO3.msh" # name of mesh file
# integration
nsimp = 3 #  number of Simpson's points (odd)

# solver
sol = 2 # Gmm=0 (default) Taucs=1 PETsc=2
beta1 =10. # value of stabilization parameter
beta2 =10.
beta3 =10.
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep= 120   # number of step (used only if soltype=1)
ftime =1.   #  Final time (used only if soltype=1)
tol=1.e-6   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 #  Number of step between 2 archiving (used only if soltype=1)

#  compute solution and BC (given directly to the solver
# creation of law
law1 = linearElasticShellLaw(lawnum,E,nu,h,nsimp,rho)
law2 = shellLinearCohesiveLaw(lawcnum,Gc,sigmac,beta,mu)
law3 = shellFractureByCohesiveLaw(3,lawnum,lawcnum)

# creation of ElasticField
nfield =99 # number of the field (physical number of surface)
fullDg = 1 #  formulation CgDg=0 fullDg =1
space = 0 #  set the function space (Lagrange=0)
myfield1 = dgLinearShellDomain(1000,nfield,space,3,fullDg)
myfield1.stabilityParameters(beta1,beta2,beta3)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,2,3,1)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.addMaterialLaw(law2)
mysolver.addMaterialLaw(law3)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
# BC
mysolver.independentDisplacementBC("Edge",31,0,0.00002)
mysolver.displacementBC("Edge",31,1,0.)
mysolver.displacementBC("Edge",31,2,0.0)
mysolver.displacementBC("Edge",61,0,0.)
mysolver.displacementBC("Edge",61,1,0.)
mysolver.displacementBC("Edge",61,2,0.)
mysolver.displacementBC("Edge",71,2,0.001)

mysolver.thetaBC(31)
mysolver.thetaBC(61)

mysolver.archivingForceOnPhysicalGroup("Edge",61,2)
mysolver.archivingForceOnPhysicalGroup("Edge",31,2)
mysolver.archivingForceOnPhysicalGroup("Edge",71,2)
#mysolver.archivingNodeDisplacement(105,2) make a physical group
#mysolver.ArchivingNodalDisplacement(106,2) make a physical group
mysolver.solve()
#print " a  "
