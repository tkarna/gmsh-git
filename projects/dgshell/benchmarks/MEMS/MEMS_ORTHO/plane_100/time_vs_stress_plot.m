%This file is used to plot the time vs stress curve.
%Modify Area to represent correct value
%--------------------------------------------------------------------------
clear all; clc;

Area = 0.0528*0.0025;
A = load('./force1006comp0.csv');
x = A(:, 1);
y = A(:, 2);
y = y / Area;

plot(x, y, '-r');
