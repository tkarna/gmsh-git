#-*-coding:Utf-8-*-
from gmshpy import *
#from dgshellpyDebug import *
from dgshellpy import *
#script to launch beam problem with a lua script

# material law
lawnum = 1 # unique number of the law


# Poisson's ratio
rho =2.33e-9  

lawnum2 =2
lawnum3 =3
lawnum4 =4
lawnum5 =5
lawnum6 =6

lawnum7 =7
lawnum8 =8
lawnum9 =9
lawnum10 =10
lawnum11 =11
lawnum12 =12
lawnum13 =13
lawnum14 =14
lawnum15 =15
lawnum16 =16
lawnum17 =17
lawnum18 =18
lawnum19 =19
lawnum20 =20

lawnum31 =31
lawnum32 =32
lawnum33 =33
lawnum34 =34
lawnum35 =35
lawnum36 =36
lawnum37 =37
lawnum38 =38
lawnum39 =39

lawnumInt1 = 41

Gc=2.54e-3  
#
sigmac100=1.53e3
sigmac110=1.21e3
sigmac111=0.868e3




beta = 0.87

# geometry
h = 0.0025  # thickness
meshfile="beam-9dom.msh" # name of mesh file
# integration
nsimp1 = 3 # number of Simpson's points (odd)
nsimp2 = 3 

# solver
sol = 2  #Gmm=0 (default) Taucs=1 PETsc=2
beta1 = 10. # value of stabilization parameter
beta2 = 10.
beta3 = 10.
soltype = 2 # StaticLinear=0 (default) StaticNonLinear=1 slotype=2 dynamic
nstep = 10 # number of step (used only if soltype=1)
ftime =5.25e-8   # Final time (used only if soltype=1)    this time is less better 
tol=1.e-5   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=50 # Number of step between 2 archiving 

# compute solution and BC (given directly to the solver

h0 = 0.0025  # thickness
nsimp = 3 #  number of Simpson's points (odd)
# law1 = linearElasticShellLaw(lawnum,E,nu,h,nsimp,rho)
oEx=144.e3 # Young's modulus of x direction   change 5
oEy=144.e3 # Young's modulus of y direction
oEz=144.e3 # Young's modulus of z direction

onuxy=0.27826 #  Poisson's ratio of xy 
onuxz=0.27826 #  Poisson's ratio of xz
onuyz=0.27826 #  Poisson's ratio of yz

omuxy =80e3 # shear modulus xy??             change 6            
omuxz =80e3 # shear modulus xz??  
omuyz =80e3 # shear modulus yz??

theta=0.0# rotation around x
phi=0.0# rotation with y
psi=0.785398# rotation with z

mu=omuxy 
theta2=0.0# rotation around x
phi2=0# rotation with y
psi2=0.785398# rotation with z

theta3=0.0# rotation around x
phi3=0.0# rotation with y
psi3=0.785398# rotation with z

theta4=0.0# rotation around x
phi4=0.0# rotation with y
psi4=0.785398# rotation with z

theta5=0.0# rotation around x
phi5=0.0# rotation with y
psi5=0.785398# rotation with z

theta6=0.0# rotation around x
phi6=0.0# rotation with y
psi6=0.785398# rotation with z

theta7=0.0# rotation around x
phi7=0.0# rotation with y
psi7=0.785398# rotation with z

theta8=0.0# rotation around x
phi8=0.0# rotation with y
psi8=0.785398# rotation with z


theta9=0.0# rotation around x
phi9=0.0# rotation with y
psi9=0.785398# rotation with z


law1=linearElasticOrthotropicShellLaw(lawnum,rho,h0,nsimp,oEx,oEy,oEz,onuxy,onuxz,onuyz,omuxy,omuxz,omuyz,theta,phi,psi)
law2=linearElasticOrthotropicShellLaw(lawnum2,rho,h0,nsimp,oEx,oEy,oEz,onuxy,onuxz,onuyz,omuxy,omuxz,omuyz,theta2,phi2,psi2)
law3=linearElasticOrthotropicShellLaw(lawnum3,rho,h0,nsimp,oEx,oEy,oEz,onuxy,onuxz,onuyz,omuxy,omuxz,omuyz,theta3,phi3,psi3)
law4=linearElasticOrthotropicShellLaw(lawnum4,rho,h0,nsimp,oEx,oEy,oEz,onuxy,onuxz,onuyz,omuxy,omuxz,omuyz,theta4,phi4,psi4)
law5=linearElasticOrthotropicShellLaw(lawnum5,rho,h0,nsimp,oEx,oEy,oEz,onuxy,onuxz,onuyz,omuxy,omuxz,omuyz,theta5,phi5,psi5)
law6=linearElasticOrthotropicShellLaw(lawnum6,rho,h0,nsimp,oEx,oEy,oEz,onuxy,onuxz,onuyz,omuxy,omuxz,omuyz,theta6,phi6,psi6)
law7=linearElasticOrthotropicShellLaw(lawnum7,rho,h0,nsimp,oEx,oEy,oEz,onuxy,onuxz,onuyz,omuxy,omuxz,omuyz,theta7,phi7,psi7)
law8=linearElasticOrthotropicShellLaw(lawnum8,rho,h0,nsimp,oEx,oEy,oEz,onuxy,onuxz,onuyz,omuxy,omuxz,omuyz,theta8,phi8,psi8)
law9=linearElasticOrthotropicShellLaw(lawnum9,rho,h0,nsimp,oEx,oEy,oEz,onuxy,onuxz,onuyz,omuxy,omuxz,omuyz,theta9,phi9,psi9)
#law1 = linearElasticShellLaw(lawnum,Young[0],nu,h,nsimp1,rho)

law11 = shellLinearCohesiveLawWithOrientation(lawnum11,Gc,sigmac100,sigmac110,sigmac111,beta,mu,theta,phi,psi,0.95,1.05)
law12 = shellLinearCohesiveLawWithOrientation(lawnum12,Gc,sigmac100,sigmac110,sigmac111,beta,mu,theta2,phi2,psi2,0.95,1.05)
law13 = shellLinearCohesiveLawWithOrientation(lawnum13,Gc,sigmac100,sigmac110,sigmac111,beta,mu,theta3,phi3,psi3,0.95,1.05)
law14 = shellLinearCohesiveLawWithOrientation(lawnum14,Gc,sigmac100,sigmac110,sigmac111,beta,mu,theta4,phi4,psi4,0.95,1.05)
law15 = shellLinearCohesiveLawWithOrientation(lawnum15,Gc,sigmac100,sigmac110,sigmac111,beta,mu,theta5,phi5,psi5,0.95,1.05)
law16 = shellLinearCohesiveLawWithOrientation(lawnum16,Gc,sigmac100,sigmac110,sigmac111,beta,mu,theta6,phi6,psi6,0.95,1.05)
law17 = shellLinearCohesiveLawWithOrientation(lawnum17,Gc,sigmac100,sigmac110,sigmac111,beta,mu,theta7,phi7,psi7,0.95,1.05)
law18 = shellLinearCohesiveLawWithOrientation(lawnum18,Gc,sigmac100,sigmac110,sigmac111,beta,mu,theta8,phi8,psi8,0.95,1.05)
law19 = shellLinearCohesiveLawWithOrientation(lawnum19,Gc,sigmac100,sigmac110,sigmac111,beta,mu,theta9,phi9,psi9,0.95,1.05)

law31 = shellFractureByCohesiveLaw(lawnum31,lawnum,lawnum11)
law32 = shellFractureByCohesiveLaw(lawnum32,lawnum2,lawnum12)
law33 = shellFractureByCohesiveLaw(lawnum33,lawnum3,lawnum13)
law34 = shellFractureByCohesiveLaw(lawnum34,lawnum4,lawnum14)
law35 = shellFractureByCohesiveLaw(lawnum35,lawnum5,lawnum15)
law36 = shellFractureByCohesiveLaw(lawnum36,lawnum6,lawnum16)
law37 = shellFractureByCohesiveLaw(lawnum37,lawnum7,lawnum17)
law38 = shellFractureByCohesiveLaw(lawnum38,lawnum8,lawnum18)
law39 = shellFractureByCohesiveLaw(lawnum39,lawnum9,lawnum19)



lawInt1 = shellLinearCohesiveLawWithOrientation(lawnumInt1,Gc,sigmac100,sigmac110,sigmac111,beta,mu,theta,phi,psi,0.95,1.05)



#law10 = shellLinearCohesiveLaw(lawnum10,Gc,sigmac,beta,mu)
#law11 = shellLinearCohesiveLaw(lawnum11,Gc,sigmac,beta,mu)
#law12 = shellLinearCohesiveLaw(lawnum12,Gc,sigmac,beta,mu)
#law13 = shellLinearCohesiveLaw(lawnum13,Gc,sigmac,beta,mu)
#law14 = shellLinearCohesiveLaw(lawnum14,Gc,sigmac,beta,mu)

#law15 = shellLinearCohesiveLaw(lawnum15,Gc,sigmac,beta,mu)
#law16 = shellLinearCohesiveLaw(lawnum16,Gc,sigmac,beta,mu)
#law17 = shellLinearCohesiveLaw(lawnum17,Gc,sigmac,beta,mu)
#law18 = shellLinearCohesiveLaw(lawnum18,Gc,sigmac,beta,mu)
#law19 = shellLinearCohesiveLaw(lawnum19,Gc,sigmac,beta,mu)
#law20 = shellLinearCohesiveLaw(lawnum20,Gc,sigmac,beta,mu)

#law3 = linearElasticShellLaw(lawnum3,Young[1],nu,h,nsimp1,rho)
#law4 = linearElasticShellLaw(lawnum4,Young[2],nu,h,nsimp1,rho)

# creation of ElasticField
nfield =99 # number of the field (physical number of surface)
fullDg = 1 #  formulation CgDg=0 fullDg =1
space1 = 0 # function space Lagrange=0
myfield1 = dgLinearShellDomain(1000,nfield,space1,lawnum31,fullDg)
myfield1.stabilityParameters(beta1,beta2,beta3)

nfield2 =98 # number of the field (physical number of surface)
fullDg2 = 1 #  formulation CgDg=0 fullDg =1
space2 = 0 # function space Lagrange=0
myfield2 = dgLinearShellDomain(1000,nfield2,space2,lawnum32,fullDg2)
myfield2.stabilityParameters(beta1,beta2,beta3)

nfield3 =97 # number of the field (physical number of surface)
fullDg3 = 1 #  formulation CgDg=0 fullDg =1
space2 = 0 # function space Lagrange=0
myfield3 = dgLinearShellDomain(1000,nfield3,space2,lawnum33,fullDg3)
myfield3.stabilityParameters(beta1,beta2,beta3)


nfield4 =96 # number of the field (physical number of surface)
fullDg4 = 1 #  formulation CgDg=0 fullDg =1
space2 = 0 # function space Lagrange=0
myfield4 = dgLinearShellDomain(1000,nfield4,space2,lawnum34,fullDg4)
myfield4.stabilityParameters(beta1,beta2,beta3)

nfield5 =95 # number of the field (physical number of surface)
fullDg5 = 1 #  formulation CgDg=0 fullDg =1
space2 = 0 # function space Lagrange=0
myfield5 = dgLinearShellDomain(1000,nfield5,space2,lawnum35,fullDg5)
myfield5.stabilityParameters(beta1,beta2,beta3)

nfield6 =94 # number of the field (physical number of surface)
fullDg6 = 1 #  formulation CgDg=0 fullDg =1
space2 = 0 # function space Lagrange=0
myfield6 = dgLinearShellDomain(1000,nfield6,space2,lawnum36,fullDg6)
myfield6.stabilityParameters(beta1,beta2,beta3)

nfield7 =93 # number of the field (physical number of surface)
fullDg7 = 1 #  formulation CgDg=0 fullDg =1
space2 = 0 # function space Lagrange=0
myfield7 = dgLinearShellDomain(1000,nfield7,space2,lawnum37,fullDg7)
myfield7.stabilityParameters(beta1,beta2,beta3)

nfield8 =92 # number of the field (physical number of surface)
fullDg8 = 1 #  formulation CgDg=0 fullDg =1
space2 = 0 # function space Lagrange=0
myfield8 = dgLinearShellDomain(1000,nfield8,space2,lawnum38,fullDg8)
myfield8.stabilityParameters(beta1,beta2,beta3)

nfield9 =91 # number of the field (physical number of surface)
fullDg9 = 1 #  formulation CgDg=0 fullDg =1
space2 = 0 # function space Lagrange=0
myfield9 = dgLinearShellDomain(1000,nfield9,space2,lawnum39,fullDg9)
myfield9.stabilityParameters(beta1,beta2,beta3)


#interface 1
myinterfield = interDomainBetweenShell(1000,myfield1,myfield2,lawnumInt1)
myinterfield.stabilityParameters(beta1,beta2,beta3)
myinterfield.matrixByPerturbation(1,1e-8)

#interface 2
myinterfield2 = interDomainBetweenShell(1000,myfield1,myfield3,lawnumInt1)
myinterfield2.stabilityParameters(beta1,beta2,beta3)
myinterfield2.matrixByPerturbation(1,1e-8)

#interface 3
myinterfield3 = interDomainBetweenShell(1000,myfield2,myfield4,lawnumInt1)
myinterfield3.stabilityParameters(beta1,beta2,beta3)
myinterfield3.matrixByPerturbation(1,1e-8)


#interface 4
myinterfield4 = interDomainBetweenShell(1000,myfield4,myfield5,lawnumInt1)
myinterfield4.stabilityParameters(beta1,beta2,beta3)
myinterfield4.matrixByPerturbation(1,1e-8)

#interface 5
myinterfield5 = interDomainBetweenShell(1000,myfield5,myfield7,lawnumInt1)
myinterfield5.stabilityParameters(beta1,beta2,beta3)
myinterfield5.matrixByPerturbation(1,1e-8)

#interface 6
myinterfield6 = interDomainBetweenShell(1000,myfield4,myfield6,lawnumInt1)
myinterfield6.stabilityParameters(beta1,beta2,beta3)
myinterfield6.matrixByPerturbation(1,1e-8)

#interface 7
myinterfield7 = interDomainBetweenShell(1000,myfield6,myfield8,lawnumInt1)
myinterfield7.stabilityParameters(beta1,beta2,beta3)
myinterfield7.matrixByPerturbation(1,1e-8)

#interface 8
myinterfield8 = interDomainBetweenShell(1000,myfield7,myfield8,lawnumInt1)
myinterfield8.stabilityParameters(beta1,beta2,beta3)
myinterfield8.matrixByPerturbation(1,1e-8)


#interface 9
myinterfield9 = interDomainBetweenShell(1000,myfield8,myfield9,lawnumInt1)
myinterfield9.stabilityParameters(beta1,beta2,beta3)
myinterfield9.matrixByPerturbation(1,1e-8)

#interface 10
myinterfield10 = interDomainBetweenShell(1000,myfield3,myfield5,lawnumInt1)
myinterfield10.stabilityParameters(beta1,beta2,beta3)
myinterfield10.matrixByPerturbation(1,1e-8)

#interface 11
myinterfield11 = interDomainBetweenShell(1000,myfield6,myfield7,lawnumInt1)
myinterfield11.stabilityParameters(beta1,beta2,beta3)
myinterfield11.matrixByPerturbation(1,1e-8)


# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addDomain(myfield2)
mysolver.addDomain(myfield3)
mysolver.addDomain(myfield4)
mysolver.addDomain(myfield5)
mysolver.addDomain(myfield6)
mysolver.addDomain(myfield7)
mysolver.addDomain(myfield8)
mysolver.addDomain(myfield9)
mysolver.addDomain(myinterfield)
mysolver.addDomain(myinterfield2)
mysolver.addDomain(myinterfield3)
mysolver.addDomain(myinterfield4)
mysolver.addDomain(myinterfield5)
mysolver.addDomain(myinterfield6)
mysolver.addDomain(myinterfield7)
mysolver.addDomain(myinterfield8) 
mysolver.addDomain(myinterfield9) 
mysolver.addDomain(myinterfield10) 
mysolver.addDomain(myinterfield11) 
mysolver.addMaterialLaw(law1)
mysolver.addMaterialLaw(law2)
mysolver.addMaterialLaw(law3)
mysolver.addMaterialLaw(law4)
mysolver.addMaterialLaw(law5)
mysolver.addMaterialLaw(law6)
mysolver.addMaterialLaw(law7)
mysolver.addMaterialLaw(law8)
mysolver.addMaterialLaw(law9)
mysolver.addMaterialLaw(law11)
mysolver.addMaterialLaw(law12)
mysolver.addMaterialLaw(law13)
mysolver.addMaterialLaw(law14)
mysolver.addMaterialLaw(law15)
mysolver.addMaterialLaw(law16)
mysolver.addMaterialLaw(law17)
mysolver.addMaterialLaw(law18)
mysolver.addMaterialLaw(law19)
mysolver.addMaterialLaw(law31)
mysolver.addMaterialLaw(law32)
mysolver.addMaterialLaw(law33)
mysolver.addMaterialLaw(law34)
mysolver.addMaterialLaw(law35)
mysolver.addMaterialLaw(law36)
mysolver.addMaterialLaw(law37)
mysolver.addMaterialLaw(law38)
mysolver.addMaterialLaw(law39)
mysolver.addMaterialLaw(lawInt1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
# interdomain

#mysolver.snlData(nstep,ftime,tol)
#mysolver.dynamicRelaxation(0.2, ftime, 1.e-3,2)
mysolver.explicitSpectralRadius(ftime,0.5,0.)
mysolver.explicitTimeStepEvaluation(nstep)
mysolver.stepBetweenArchiving(nstepArch)

# BC
mysolver.displacementBC("Edge",1018,0,0.)
mysolver.displacementBC("Edge",1018,1,0.)
mysolver.displacementBC("Edge",1018,2,0.)
mysolver.displacementBC("Edge",1001,2,0.)
mysolver.displacementBC("Edge",1002,2,0.)
mysolver.displacementBC("Edge",1003,2,0.)
mysolver.displacementBC("Edge",1004,2,0.)
mysolver.displacementBC("Edge",1005,2,0.)
mysolver.displacementBC("Edge",1006,2,0.)
mysolver.displacementBC("Edge",1007,2,0.)
mysolver.displacementBC("Edge",1008,2,0.)
mysolver.displacementBC("Edge",1009,2,0.)
mysolver.displacementBC("Edge",1010,2,0.)
mysolver.displacementBC("Edge",1011,2,0.)
mysolver.displacementBC("Edge",1012,2,0.)
mysolver.displacementBC("Edge",1013,2,0.)
mysolver.displacementBC("Edge",1014,2,0.)
mysolver.displacementBC("Edge",1015,2,0.)
mysolver.displacementBC("Edge",1016,2,0.)
mysolver.displacementBC("Edge",1017,2,0.)
mysolver.displacementBC("Edge",1018,2,0.)
mysolver.displacementBC("Edge",1019,2,0.)
mysolver.displacementBC("Edge",1020,2,0.)
mysolver.displacementBC("Edge",1021,2,0.)
mysolver.displacementBC("Edge",1022,2,0.)
mysolver.displacementBC("Edge",1023,2,0.)
mysolver.displacementBC("Edge",1024,2,0.)
mysolver.displacementBC("Edge",1025,2,0.)
mysolver.displacementBC("Edge",1026,2,0.)
mysolver.displacementBC("Edge",1027,2,0.)
mysolver.displacementBC("Edge",1028,2,0.)
mysolver.displacementBC("Edge",1029,2,0.)
mysolver.displacementBC("Edge",1030,2,0.)


#mysolver.independentDisplacementBC("Edge",31,0,0.00002)
d1=0.02
d2=0.022
d3=0.03

#cyclicFunction1=cycleFunctionTime(ftime/4., d1/ftime, 3.*ftime/4., d2/ftime,ftime, d3/ftime);  

mysolver.displacementBC("Edge",1009,0,1000.) #1009 is the name of line which is applied the displacement. 0 is direction, 3000 is the value of displacement.
#mysolver.displacementBC("Edge",1009,0,cyclicFunction1) #1009 is the name of line which is applied the displacement. 0 is direction, 3000 is the value of displacement.
#mysolver.displacementBC("Edge",1008,0,0.001)
#mysolver.thetaBC(61)
#mysolver.thetaBC(31)


mysolver.archivingForceOnPhysicalGroup("Edge",1009,0,nstepArch/10)

mysolver.internalPointBuildView("svm",IPField.SVM, 1, 1)#?????????
mysolver.internalPointBuildView("sig_xx",IPField.SIG_XX, 1, 1)
mysolver.internalPointBuildView("sig_yy",IPField.SIG_YY, 1, 1)
mysolver.internalPointBuildView("sig_zz",IPField.SIG_ZZ, 1, 1)
mysolver.internalPointBuildView("sig_xy",IPField.SIG_XY, 1, 1)
mysolver.internalPointBuildView("sig_yz",IPField.SIG_YZ, 1, 1)
mysolver.internalPointBuildView("sig_xz",IPField.SIG_XZ, 1, 1)

#mysolver.archivingNodeDisplacement(6,2,nstepArch)
#mysolver.archivingNodeDisplacement(7,2,nstepArch)
#mysolver.archivingNodeIP(102,-1,0,nstepArch)
mysolver.solve()
