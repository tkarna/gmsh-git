#!/usr/bin/env python
#-*-coding:Utf-8-*-
import onelab

OL = onelab.client()
E = OL.defineNumber('Mat/Young',value=10000.e6, label='Young') # Young's modulus
rho = OL.defineNumber('Mat/Density',value=1.e4, label='Density')
time = OL.defineNumber('Dyna/Time',label='Time')
endtime = OL.defineNumber('Dyna/Final',value=5.e-3,label='End time')
meshfile = OL.defineString('Mesh/Name',value='beam.msh',label='Name')

# mesh display
OL.mergeFile(onelab.path(__file__,meshfile))

if OL.action == 'check':
    exit(0)
OL.run('subclient', 'python ./beam.py','')
