#-*-coding:Utf-8-*-
from gmshpy import *
from dgshellpy import *
import onelab
#script to launch beam problem with a python script

OL = onelab.client()
# material law
lawnum = 1 # unique number of the law
E = OL.getNumber('Mat/Young')
nu = 0.   # Poisson's ratio
rho = OL.getNumber('Mat/Density') # Bulk mass

# geometry
h = 0.01  # thickness
meshfile= str(OL.getString('Mesh/Name').encode('utf-8'))
# integration
nsimp = 3 # number of Simpson's points (odd)

# solver
sol =  0 #Gmm=0 (default) Taucs=1 PETsc=2
beta1 = 10. # value of stabilization parameter
beta2 = 10.
beta3 = 0.001
soltype = 2 # StaticLinear=0 (default) StaticNonLinear=1 Explicit=2
nstep = 100   # number of step (soltype=1 or 2 step for time step evaluation)
ftime =OL.getNumber('Dyna/Final')  # Final time (used only if soltype=1 or 2)
tol=1.e-4   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1000 # Number of step between 2 archiving (used only if soltype=1 or 2)
# explicit scheme
#beta=0.5 #0.5
#gamma=1. #1.
gamma_s =0.8 # 0.8666666667
#alpham=0.5 #0.5
rhoinfty =1. 

# compute solution and BC (given directly to the solver

# creation of law
law1 = linearElasticShellLaw(lawnum,E,nu,h,nsimp,rho)

# creation of ElasticField
nfield =99 # number of the field (physical number of surface)
fullDg = 1 #  formulation CgDg=0 fullDg =1
space1 = 0 # function space Lagrange=0
myfield1 = dgLinearShellDomain(1000,nfield,space1,lawnum,fullDg)
myfield1.stabilityParameters(beta1,beta2,beta3)
# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.loadModel(meshfile)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
if soltype !=2 :
     mysolver.snlData(nstep,ftime,tol)
else :
     mysolver.explicitSpectralRadius(ftime,gamma_s,rhoinfty)
mysolver.explicitTimeStepEvaluation(nstep)
mysolver.stepBetweenArchiving(nstepArch)
# BC
mysolver.displacementBC("Edge",41,0,0.)
mysolver.displacementBC("Edge",41,1,0.)
mysolver.displacementBC("Edge",41,2,0.)
mysolver.initialBC("Face","Velocity",99,0,-1.)
# Archive
mysolver.archivingNodeVelocity(22,0,3)
mysolver.archivingNodeDisplacement(22,0,3)
mysolver.archivingNodeIP(22,0,0)
mysolver.archivingForceOnPhysicalGroup("Node",22,0,3)
#view
mysolver.unknownBuildView(1,nstepArch)
mysolver.energyBuildView("total",4,nstepArch)
mysolver.internalPointBuildView("VonMises",-1,nstepArch)
mysolver.internalPointBuildView("VonMisesMax",-1,nstepArch,3) # max=3
mysolver.internalPointBuildView("Sigmaxx",0,nstepArch)
mysolver.internalPointBuildView("Sigmayy",1,nstepArch)
mysolver.internalPointBuildView("tauxy",3,nstepArch)

## Onelab control


# solve via explicit scheme
time = 0.
mysolver.initializeExplicitScheme()
# fullVector to get the archived nodal values to display them
nodalValues = fullVectorDouble(2)
while time < ftime :
    time = mysolver.oneExplicitStep(time)
    # create a view with the velocity for the display in Gmsh
    mysolver.createOnelabVelocityView('one_velocity.msh',time)
    # display it
# the view option does not work. WHY?
#    OL.mergeFile(onelab.path(__file__,'one_velocity.msh.opt'))
    OL.mergeFile(onelab.path(__file__,'one_velocity.msh'))

    # get nodal values
    mysolver.getOnelabArchiveNodalUnknowns(nodalValues)

    OL.setNumber('Dyna/Time',value=time)
    OL.addNumberChoice('Dyna/Time',time)
    OL.setNumber('Results/Velocity',value=nodalValues(0))
    OL.addNumberChoice('Results/Velocity',nodalValues(0))
    OL.setNumber('Results/Displacement',value=nodalValues(1))
    OL.addNumberChoice('Results/Displacement',nodalValues(1))
    OL.setString('Gmsh/Action', value='refresh')

mysolver.finalizeExplicitScheme(ftime)
