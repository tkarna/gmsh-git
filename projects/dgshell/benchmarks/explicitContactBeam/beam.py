#-*-coding:Utf-8-*-
from gmshpy import *
from dgshellpy import *

#script to launch beam contact problem with a python script

# material law
lawnum = 1 # unique number of the law
E = 100.e9 # Young's modulus
nu = 0.3   # Poisson's ratio
rho =7850. 
# geometry
h = 0.01  # thickness
geofile="beam.geo"
meshfile="beam.msh" # name of mesh file
# integration
nsimp = 3 # number of Simpson's points (odd)

# cylinder
cyl_thick = 0.01
c_penalty = 1.e11
rhocyl = 7850.
# solver
sol = 0  #Gmm=0 (default) Taucs=1 PETsc=2
beta1 = 10. # value of stabilization parameter
beta2 = 10.
beta3 = 1.
soltype = 2 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 100   # number of step (used only if soltype=1)
ftime =3.e-5   # Final time (used only if soltype=1)
tol=1.e-3   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=100 # Number of step between 2 archiving (used only if soltype=1)
rhoinfty=0.95
gamma_s = 0.036666667

# compute solution and BC (given directly to the solver

# creation of law
law1 = linearElasticShellLaw(lawnum,E,nu,h,nsimp,rho)

# creation of ElasticField
nfield =99 # number of the field (physical number of surface)
fullDg = 1  #  formulation CgDg=0 fullDg =1
space1 = 0 # function space Lagrange=0
myfield1 = dgLinearShellDomain(1000,nfield,space1,lawnum,fullDg)
myfield1.stabilityParameters(beta1,beta2,beta3)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,2,3,0)
mysolver.addDomain(myfield1)
mysolver.addMaterialLaw(law1)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.explicitSpectralRadius(ftime,gamma_s,rhoinfty)
mysolver.explicitTimeStepEvaluation(nstep)
mysolver.stepBetweenArchiving(nstepArch)

# contact
contact1 = dgC0ShellRigidCylinderContact(1000,1000,99,205,208,c_penalty,cyl_thick,rhocyl)
mysolver.contactInteraction(contact1)

# BC
mysolver.displacementBC("Edge",103,0,0.00000000)
mysolver.displacementBC("Edge",103,1,0.)
mysolver.displacementBC("Edge",103,2,0.)
mysolver.displacementBC("Edge",118,0,0.)
mysolver.displacementBC("Edge",118,1,0.)
mysolver.displacementBC("Edge",118,2,0.)
mysolver.displacementRigidContactBC(1000,0,0.)
mysolver.displacementRigidContactBC(1000,1,0.)
mysolver.displacementRigidContactBC(1000,2,-0.1e3)
mysolver.thetaBC(103)
mysolver.thetaBC(118)
mysolver.archivingForceOnPhysicalGroup("Edge",119,2)
mysolver.archivingNodeDisplacement(215,2)
mysolver.archivingNodeDisplacement(216,2)
mysolver.archivingRigidContact(1000,2,0)
mysolver.archivingRigidContact(1000,2,1)
mysolver.archivingRigidContact(1000,2,2)

# Solve
mysolver.solve()
