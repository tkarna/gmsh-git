#-*-coding:Utf-8-*-
from gmshpy import *
from dgshellpy import *

#script to launch beam problem with a lua script

# material law
lawnum = 1 # unique number of the law
E = 71.e9 # Young's modulus
nu = 0.   # Poisson's ratio
rho =7850. 

lawnum2 =2
Gc=8800.
sigmac = 400.e6
beta = 0.87
mu = 0.41
# geometry
h = 0.0025  # thickness
geofile="beam.geo"
meshfile="beam.msh" # name of mesh file
# integration
nsimp1 = 3 # number of Simpson's points (odd)
nsimp2 = 3 

# solver
sol = 2  #Gmm=0 (default) Taucs=1 PETsc=2
beta1 = 10. # value of stabilization parameter
beta2 = 10.
beta3 = 10.
soltype = 1 # StaticLinear=0 (default) StaticNonLinear=1
nstep = 120   # number of step (used only if soltype=1)
ftime =1.   # Final time (used only if soltype=1)
tol=1.e-5   # relative tolerance for NR scheme (used only if soltype=1)
nstepArch=1 # Number of step between 2 archiving (used only if soltype=1)

# compute solution and BC (given directly to the solver

# creation of law
law1 = linearElasticShellLaw(lawnum,E,nu,h,nsimp1,rho)
law2 = shellLinearCohesiveLaw(lawnum2,Gc,sigmac,beta,mu)

# creation of ElasticField
nfield =98 # number of the field (physical number of surface)
fullDg = 0 #  formulation CgDg=0 fullDg =1
space1 = 0 # function space Lagrange=0
myfield1 = dgLinearShellDomain(1000,nfield,space1,lawnum,fullDg)
myfield1.stabilityParameters(beta1,beta2,beta3)

nfield2 =99 # number of the field (physical number of surface)
fullDg2 = 1 #  formulation CgDg=0 fullDg =1
space2 = 0 # function space Lagrange=0
myfield2 = dgLinearShellDomain(1000,nfield2,space2,lawnum,fullDg2)
myfield2.stabilityParameters(beta1,beta2,beta3)

#interface
myinterfield = interDomainBetweenShell(1000,myfield1,myfield2,lawnum2)
myinterfield.stabilityParameters(beta1,beta2,beta3)
myinterfield.matrixByPerturbation(1,1e-8)

# creation of Solver
mysolver = nonLinearMechSolver(1000)
mysolver.createModel(geofile,meshfile,2,3,0)
mysolver.addDomain(myfield2)
mysolver.addDomain(myfield1)
mysolver.addDomain(myinterfield)
mysolver.addMaterialLaw(law1)
mysolver.addMaterialLaw(law2)
mysolver.Scheme(soltype)
mysolver.Solver(sol)
mysolver.snlData(nstep,ftime,tol)
mysolver.stepBetweenArchiving(nstepArch)
# interdomain

# BC
mysolver.displacementBC("Edge",61,0,0.)
mysolver.displacementBC("Edge",61,1,0.)
mysolver.displacementBC("Edge",61,2,0.)
mysolver.independentDisplacementBC("Edge",31,0,0.00002)
mysolver.displacementBC("Edge",31,1,0.)
mysolver.displacementBC("Edge",31,2,0.0)
mysolver.displacementBC("Edge",71,2,0.001)
mysolver.thetaBC(61)
mysolver.thetaBC(31)

mysolver.archivingForceOnPhysicalGroup("Edge",61,2)
mysolver.archivingForceOnPhysicalGroup("Edge",31,2)
mysolver.archivingForceOnPhysicalGroup("Edge",71,2)
mysolver.archivingNodeDisplacement(5,2)
mysolver.archivingNodeDisplacement(6,2)
mysolver.archivingNodeIP(102,-1,0)
mysolver.solve()
