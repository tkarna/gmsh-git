//
// C++ Interface: ipvariable
//
// Description: Class with definition of ipvarible for shell
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "shellIPVariable.h"
#include "openingForShell.h"
#include "ipstate.h"
double SimpsonIntegration(const std::vector<double> &y, const double bminusa);

static void alphaTensprodAdd(const double alpha,const SVector3 &a,const SVector3 &b,STensor3 &sigma)
{
  if(alpha !=0.){
    for(int i=0;i<3;i++){
      for(int j=0;j<3;j++){
        sigma(i,j) += (alpha*a[i]*b[j]);
      }
    }
  }
}

shellLocalBasis* IPLinearShell::getshellLocalBasisOfInterface()
{
  #ifdef _DEBUG
    if(!_oninter){
      Msg::Debug("Use localBasis of interface on a bulk point");
      if(_lbs == NULL)
        _lbs = new linearShellLocalBasisInter();
    }
  #endif
  return _lbs;
}
void IPLinearShell::setshellLocalBasisOfInterface(const shellLocalBasis *lb)
{
  #ifdef _DEBUG
    if(!_oninter){
      Msg::Debug("Set shellLocalBasis of interface on a bulk point");
      if(_lbs==NULL)
        _lbs = new linearShellLocalBasisInter();
    }
  #endif
  (*_lbs) = (*lb);
}

IPLinearShell::IPLinearShell(const double h,const int nbFF,
                             const short int ns, const bool inter) : IPVariableShell(),
                                                                                      _h(h),nsimp(ns),
                                                                                      _B(nbFF), _lbs(NULL), _lb(),
                                                                                      _oninter(inter)
{
  if(_oninter) _lbs = new linearShellLocalBasisInter(); // creation of interface local basis on interface
  zsimp.resize(nsimp);
  if(nsimp ==1){ // in this case index 0 = Membrane index 1 = Bending
    zsimp[0] = 0.;
    sigma.resize(nsimp+1);
    epsilon.resize(nsimp+1);
  }
  else{
    sigma.resize(nsimp);
    epsilon.resize(nsimp);
    double hsimp = double(h)/double(nsimp-1); // numsimp is always odd by build of materialLaw
    double halfh = 0.5*h;
    for(int i=0;i<nsimp;i++)
      zsimp[i] = -halfh + i*hsimp;
  }
}
IPLinearShell& IPLinearShell::operator=(const IPVariable &source)
{
  IPVariableShell::operator=(source);
  const IPLinearShell *src = static_cast<const IPLinearShell*>(&source);
  _lb = src->_lb;
  if(src->_lbs != NULL){
    if(_lbs == NULL) _lbs = new linearShellLocalBasisInter(*(src->_lbs));
    else (*_lbs) = *(src->_lbs);
  }
  else{
    if(_lbs != NULL) delete _lbs;
    _lbs = NULL;
  }
  _h = src->_h;
  _B = src->_B;
  nsimp = src->getNumSimp();
  _oninter = src->_oninter;
  int ns = nsimp;
  if(nsimp ==1){
    ns = 2;
  }
  sigma.resize(ns);
  epsilon.resize(ns);
  zsimp.resize(ns);
  for(int i=0; i<ns;i++){
    sigma[i] = src->sigma[i];
    epsilon[i] = src->epsilon[i];
    zsimp[i] = src->zsimp[i];
  }
  return *this;
}
IPLinearShell::IPLinearShell(const IPLinearShell& source) : IPVariableShell(source), _lb(source._lb),
                                                             _h(source._h), _B(source._B), _oninter(source._oninter)
{
  nsimp = source.getNumSimp();
  if(source._lbs != NULL){
    if(_lbs == NULL) _lbs = new linearShellLocalBasisInter(*(source._lbs));
    else (*_lbs) = *(source._lbs);
  }
  else
    _lbs = NULL;
  int ns=nsimp;
  if(ns == 1)
    ns =2;
  sigma.resize(ns);
  epsilon.resize(ns);
  zsimp.resize(ns);
  for(int i=0; i<ns;i++){
    sigma[i] = source.sigma[i];
    epsilon[i] = source.epsilon[i];
    zsimp[i] = source.zsimp[i];
  }
}

void IPLinearShell::strain(const fullVector<double> &disp, const int npoint,const shellLocalBasisBulk *lbinit)
{
  double veps[6];
  double vrho[6];
  _B.strain(disp,veps,vrho);
  if(nsimp==1)
  {
    epsilon[0][0] = veps[0];
    epsilon[0][1] = veps[1];
    epsilon[0][3] = veps[3];
//    epsilon[0][2] = epsilon[0][4] = epsilon[0][5]=0.; // USEFULL ??
    epsilon[1][0] = vrho[0];
    epsilon[1][1] = vrho[1];
    epsilon[1][3] = vrho[3];
//    epsilon[1][2] = epsilon[1][4] = epsilon[1][5]=0. // USEFULL ??
  }
  else
  {
    for(int i=0;i<nsimp;i++){
      epsilon[i][0] = veps[0] + zsimp[i]*vrho[0];
      epsilon[i][1] = veps[1] + zsimp[i]*vrho[1];
      epsilon[i][3] = veps[3] + zsimp[i]*vrho[3];
//      epsilon[i][2] = epsilon[i][4] = epsilon[i][5] = 0.; //USEFULL ??
    }
  }
}

void IPLinearShell::getUpperStressTensorBrokenEval(stressTensor &sigma_) const
{
  #ifdef _DEBUG
  if(nsimp == 1) Msg::Debug("Use getUpperStressTensor with only one Simpson's point. See IPLinearShell");
  #endif //_DEBUG
  for(int i=0;i<3;i++) for(int j=0;j<3;j++) sigma_(i,j) = 0.;
  reductionElement reducedStress;
  reducedStress(0,0) = sigma[nsimp-1][0];
  reducedStress(0,1) = reducedStress(1,0) = sigma[nsimp-1][3];
  reducedStress(1,1) = sigma[nsimp-1][1];
  for(int alpha=0;alpha<2;alpha++){
    for(int beta=0;beta<2;beta++){
      double sigalphabeta=0;
      for(int gamma=0;gamma<2;gamma++){
        for(int delta=0;delta<2;delta++){
          sigalphabeta+= _lb.basisPushTensor(gamma,alpha)*reducedStress(gamma,delta)*_lb.basisPushTensor(delta,beta);
        }
      }
      alphaTensprodAdd(sigalphabeta,_lbs->basisVector(alpha),_lbs->basisVector(beta),sigma_);
    }
  }
}

void IPLinearShell::getLowerStressTensorBrokenEval(stressTensor &sigma_) const
{
  #ifdef _DEBUG
  if(nsimp == 1) Msg::Debug("Use getUpperStressTensor with only one Simpson's point. See IPLinearShell");
  #endif //_DEBUG
  for(int i=0;i<3;i++) for(int j=0;j<3;j++) sigma_(i,j) = 0.;
  reductionElement reducedStress;
  reducedStress(0,0) = sigma[0][0];
  reducedStress(0,1) = reducedStress(1,0) = sigma[0][3];
  reducedStress(1,1) = sigma[0][1];
  for(int alpha=0;alpha<2;alpha++){
    for(int beta=0;beta<2;beta++){
      double sigalphabeta=0;
      for(int gamma=0;gamma<2;gamma++){
        for(int delta=0;delta<2;delta++){
          sigalphabeta+= _lb.basisPushTensor(gamma,alpha)*reducedStress(gamma,delta)*_lb.basisPushTensor(delta,beta);
        }
      }
      alphaTensprodAdd(sigalphabeta,_lbs->basisVector(alpha),_lbs->basisVector(beta),sigma_);
    }
  }
}

double IPLinearShell::get(const int i) const
{
  // if i < 6*nbsimp --> stress
  if(i<6*nsimp){
    // find the gauss point on the thickness
    int pos = i/6; // pos on thickness
    int cmp = i%6;
    if(cmp < 0){ // VonMises
      return this->vonMises(cmp);
    }
    else{
      reductionElement rstress;
      double sig;
      if(nsimp == 1){
        rstress(0,0) = sigma[0][0] + sigma[1][0];
        rstress(1,1) = sigma[0][1] + sigma[1][1];
        rstress(0,1) = rstress(1,0) = sigma[0][3] + sigma[1][3];
      }
      else{
        if(pos >= nsimp){
          Msg::Error("Try to archive stress on a inexisting Simpson's point");
          return 0;
        }
        rstress(0,0) = sigma[pos][0];
        rstress(1,1) = sigma[pos][1];
        rstress(0,1) = rstress(1,0) = sigma[pos][3];
      }
      stressTensor stress(rstress,&_lb);
      // compute of stress for representation
      switch(cmp){
        case component::xx :
          sig = stress(0,0);//stress.getComponent(lb->basisOrthonormalVector(0),lb->basisOrthonormalVector(0));
          break;
        case component::yy :
          sig = stress(1,1);//stress.getComponent(lb->basisOrthonormalVector(1),lb->basisOrthonormalVector(1));
          break;
        case component::xy :
          sig = stress(0,1);//stress.getComponent(lb->basisOrthonormalVector(0),lb->basisOrthonormalVector(1));
          break;
        default :
          sig=0.;
      }
      return sig;
    }
  }
  else if(i<12*nsimp){
    Msg::Warning("strain archive is not implemented for IPLinearShell");
    return 0.;
  }
}

double IPLinearShell::vonMises(const int pos) const
{
  // pos in negative by construction take it with positive sign
  int ind = -(pos+1); // by chosse of construction
  // Stress in shell basis
  double sx,sy,txy,sz,txz,tyz;
  reductionElement rstress;
  // NO THICKNESS INTEGRATION --> 0 Membrane 1 Bending
  if(nsimp == 1 ){
    rstress(0,0) = sigma[0][0] + sigma[1][0];
    rstress(1,1) = sigma[0][1] + sigma[1][1] ;
    rstress(0,1) = rstress(1,0) = sigma[0][3] + sigma[1][3];
  }
  else{ //
    if(ind >= nsimp){
      Msg::Error("try to archive Von Mises on a non existing Simpson's point");
      return 0;
    }
    rstress(0,0) = sigma[ind][0];
    rstress(1,1) = sigma[ind][1];
    rstress(0,1) = rstress(1,0) = sigma[ind][3];
  }
  stressTensor stress(rstress,&_lb);
  // compute of stress for representation
  sz = tyz = txz =0.;
  sx = stress(0,0);
  sy = stress(1,1);
  txy = stress(0,1);
  return sqrt(1./2.*((sx-sy)*(sx-sy)+(sy-sz)*(sy-sz)+(sx-sz)*(sx-sz)+6*(txy*txy+tyz*tyz+txz*txz)));
}

double IPLinearShell::defoEnergy() const
{
  // Stress in shell basis
  reductionElement rstress, rdefo;
  double cipt;
  if(nsimp == 1){
    rstress(0,0) = sigma[0][0] + sigma[1][0];
    rstress(1,1) = sigma[0][1] + sigma[1][1];
    rstress(0,1) = rstress(1,0) = sigma[0][3] + sigma[1][3];
    // Stress in xyz
    stressTensor stress(rstress,&_lb);
    // Defo in shell basis
    rdefo(0,0) = epsilon[0][0] + epsilon[1][0];
    rdefo(1,1) = epsilon[0][1] + epsilon[1][1];
    rdefo(0,1) = rdefo(1,0) = epsilon[0][3] + epsilon[1][3];
    // Defo in xyz
    strainTensor defo(rdefo,&_lb);

    double contractTimes = dot(stress,defo);
    cipt = 0.5*contractTimes*_h;
  }
  else{
    std::vector<double> vcp;
    for(int pos=0; pos<nsimp; pos++){
      // Stress in shell basis
      rstress(0,0) = sigma[pos][0];
      rstress(1,1) = sigma[pos][1];
      rstress(0,1) = rstress(1,0) = sigma[pos][3];
      stressTensor stress(rstress,&_lb);
      // defo in shell basis
      rdefo(0,0) = epsilon[pos][0];
      rdefo(1,1) = epsilon[pos][1];
      rdefo(0,1) = rdefo(1,0) = epsilon[pos][3];
      strainTensor defo(rdefo,&_lb);
      vcp.push_back(dot(stress,defo));
    }
    cipt = 0.5*SimpsonIntegration(vcp,_h);
  }
  return cipt;
}

void IPLinearShell::createRestart(FILE *fp)
{
  IPVariableShell::createRestart(fp);
  _lb.createRestart(fp);
  if(_lbs!=NULL) _lbs->createRestart(fp);
  _B.createRestart(fp);
  int oninter = 0;
  if(_oninter) oninter=1;
  fprintf(fp,"%e %d %d ",_h,(int)nsimp,oninter);
  for(int i=0;i<nsimp;i++)
  {
    fprintf(fp,"%e %e %e %e %e %e %e %e %e %e %e %e %e ",sigma[i][0],sigma[i][1],sigma[i][2],sigma[i][3],sigma[i][4],sigma[i][5],
                                                         epsilon[i][0],epsilon[i][1],epsilon[i][2],epsilon[i][3],epsilon[i][4],epsilon[i][5],
                                                         zsimp[i]);
  }
}

void IPLinearShell::setFromRestart(FILE *fp)
{
  IPVariableShell::setFromRestart(fp);
  _lb.setFromRestart(fp);
  if(_lbs!=NULL) _lbs->setFromRestart(fp);
  _B.setFromRestart(fp);

  int ns,oninter;
  fscanf(fp,"%e %d %d",&_h,&ns,&oninter);
  nsimp = (short int) ns;
  _oninter = false;
  if(oninter==1) _oninter = true;
  double s0,s1,s2,s3,s4,s5,e0,e1,e2,e3,e4,e5,z;
  for(int i=0;i<nsimp;i++)
  {
    fscanf(fp,"%e %e %e %e %e %e %e %e %e %e %e %e %e",&s0,&s1,&s2,&s3,&s4,&s5,
                                                       &e0,&e1,&e2,&e3,&e4,&e5,
                                                       &z);
    sigma[i][0] = s0;
    sigma[i][1] = s1;
    sigma[i][2] = s2;
    sigma[i][3] = s3;
    sigma[i][4] = s4;
    sigma[i][5] = s5;
    epsilon[i][0] = e0;
    epsilon[i][1] = e1;
    epsilon[i][2] = e2;
    epsilon[i][3] = e3;
    epsilon[i][4] = e4;
    epsilon[i][5] = e5;
    zsimp[i] = z;
  }
}


IPLinearShellWD::IPLinearShellWD(const double h,const int nbFF,
                                       const short int numsimp, const double d0,
                                       const bool inter): IPLinearShell(h,nbFF,numsimp,inter) , D0(d0)
{
  _D.resize(numsimp);
  Bifurcation.resize(numsimp);
  for(int i=0;i<numsimp; i++){
   _D[i] = D0;
   Bifurcation[i] = 0.;
  }
}
IPLinearShellWD::IPLinearShellWD(const IPLinearShellWD &source): IPLinearShell(source)
{
  D0 = source.D0;
  for(int i=0;i<nsimp; i++){
   _D[i] = source._D[i];
   Bifurcation[i] = source.Bifurcation[i];
  }
}
IPLinearShellWD& IPLinearShellWD::operator = (const IPVariable &source)
{
  IPLinearShell::operator = (source);
  const IPLinearShellWD* src = static_cast<const IPLinearShellWD*>(&source);
  D0 = src->D0;
  for(int i=0;i<nsimp; i++){
   _D[i] = src->_D[i];
   Bifurcation[i] = src->Bifurcation[i];
  }
  return *this;
}

double IPLinearShellWD::get(const int i) const
{
  if(i<12*nsimp)
    return IPLinearShell::get(i);
  if(i<13*nsimp){
    int cmp = i%(12*nsimp);
    return _D[cmp];
  }
  if(i>14*nsimp)
  {
    Msg::Error("try to archive Damage on a non existing Simpson's point");
    return 0;
  }
  int cmp = i%(13*nsimp);
  return Bifurcation[cmp];
}

IPLinearOrthotropicShell::IPLinearOrthotropicShell(const double h,const int nbFF,
                                       const short int numsimp,
                                       const bool inter): IPLinearShell(h,nbFF,numsimp,inter), Htens(), initHtens(false)
{}
IPLinearOrthotropicShell::IPLinearOrthotropicShell(const IPLinearOrthotropicShell &source): IPLinearShell(source), Htens(source.Htens), initHtens(source.initHtens)
{}
IPLinearOrthotropicShell& IPLinearOrthotropicShell::operator = (const IPVariable &source)
{
  IPLinearShell::operator = (source);
  const IPLinearOrthotropicShell* src = static_cast<const IPLinearOrthotropicShell*>(&source);
  Htens = src->Htens;
  initHtens = src->initHtens;
  return *this;
}

IPLinearCohesiveShell::IPLinearCohesiveShell() : IPVariableMechanics(), deltan(0.), delta(0.),
                                                 deltat(0.), deltamax(0.), n0(), m0(), _n(), _m(),
                                                 unjump(0.), unjump0(0.), rnjump(0.),rnjump0(0.),
                                                 utjump(0.), utjump0(0.), rtjump(0.), rtjump0(0.),
                                                 deltac(0.), deltan0(0.), deltat0(0.),sigmac(0.),
                                                 sigI(0.), tauII(0.), etaI(0.), etaII(0.), beta(1.),
                                                 heqI(0.), heqII(0.), tension(true),
                                                 _energyBending(0.), _energyMembrane(0.),
                                                 lengthphi1(0.), lengthphi2(0.){}

IPLinearCohesiveShell::IPLinearCohesiveShell(const IPLinearCohesiveShell &source): IPVariableMechanics(source)
{
  n0=source.n0;
  m0=source.m0;
  _n=source._n;
  _m=source._m;
  unjump = source.unjump;
  unjump0= source.unjump0;
  rnjump = source.rnjump;
  rnjump0=source.rnjump0;
  utjump = source.utjump;
  utjump0 = source.utjump0;
  rtjump = source.rtjump;
  rtjump0= source.rtjump0;
  deltan = source.deltan;
  deltat = source.deltat;
  deltac = source.deltac;
  deltan0 = source.deltan0;
  deltat0 = source.deltat0;
  deltamax = source.deltamax;
  delta = source.delta;
  sigmac = source.sigmac;
  sigI = source.sigI;
  tauII = source.tauII;
  etaI = source.etaI;
  etaII = source.etaII;
  heqI = source.heqI;
  heqII= source.heqII;
  beta = source.beta;
  tension = source.tension;
  _energyBending = source._energyBending;
  _energyMembrane= source._energyMembrane;
  lengthphi1 = source.lengthphi1;
  lengthphi2 = source.lengthphi2;
}
IPLinearCohesiveShell& IPLinearCohesiveShell::operator = (const IPVariable &source)
{
  IPVariableMechanics::operator=(source);
  const IPLinearCohesiveShell *src = static_cast<const IPLinearCohesiveShell*>(&source);
  n0=src->n0;
  m0=src->m0;
  _n=src->_n;
  _m=src->_m;
  unjump = src->unjump;
  unjump0= src->unjump0;
  rnjump = src->rnjump;
  rnjump0=src->rnjump0;
  utjump = src->utjump;
  utjump0=src->utjump0;
  rtjump = src->rtjump;
  rtjump0=src->rtjump0;
  deltan = src->deltan;
  deltat = src->deltat;
  deltac = src->deltac;
  deltan0 = src->deltan0;
  deltat0 = src->deltat0;
  deltamax = src->deltamax;
  delta = src->delta;
  sigmac = src->sigmac;
  sigI = src->sigI;
  tauII= src->tauII;
  etaI = src->etaI;
  etaII= src->etaII;
  heqI = src->heqI;
  heqII= src->heqII;
  beta = src->beta;
  tension = src->tension;
  _energyBending = src->_energyBending;
  _energyMembrane = src->_energyMembrane;
  lengthphi1 = src->lengthphi1;
  lengthphi2 = src->lengthphi2;
  return *this;
}

void IPLinearCohesiveShell::initfrac(const SVector3 &du, const double dr[3],const shellLocalBasis *lbs,
                                     const double h_)
{

  double M0 = m0(1,1);
  double N0 = n0(1,1);
  double N12 = n0(0,1)*(lbs->basisVector(0).norm());//added by Prof. Ludovic and Shantanu on 5-07-2013
  double M12 = m0(0,1)*(lbs->basisVector(0).norm());//added by Prof. Ludovic and Shantanu on 5-07-2013
  double checkI,checkII;
  checkI = (sigI*h_-N0)/N0;
  checkII= (fabs(tauII)*h_-fabs(N12))/fabs(N12);
  if(checkI <0.) checkI = -checkI;
  if(checkII<0.) checkII= -checkII;
  if(checkI>1.e-6) // tolerance on a pure tension test (no bending)
    heqI = M0 /(h_*sigI-N0);
  if(checkII>1.e-6)
    heqII = fabs(M12)/(h_*fabs(tauII)-fabs(N12)); // tolerance to a pure shearing test (no twist torque)
  if(heqI<0) heqI = -heqI;
  if(heqII<0) heqII = -heqII;
  if(sigI!=0.)
    etaI = 1 - N0/(h_*sigI);
  if(tauII !=0.)
    etaII= 1 - fabs(N12)/(h_*fabs(tauII));
   //added by Shantanu on 5-7-2013------------------//
  if (fabs(heqI) == 0.)
        etaI = 0.;
  if (fabs(heqII) == 0.)
    etaII = 0.;
  //added by Shantanu on 5-7-2013------------------//
//  double m0abshdiv6, m12abshdiva;
  if(M0<0) {
    //m0abshdiv6 = - M0/heqI;
    rnjump0 = -rnjump0;
  }
//  else if(M0!=0.) m0abshdiv6 = M0/heqI;
//  if(M0 == 0.) etaI = 0.; // to avoid nan value in case of initial broken
//  else etaI = m0abshdiv6/(m0abshdiv6+N0);
  if(M12<0) {
    //m12abshdiva = - M12/heqII;
    rtjump0 = -rtjump0;
  }
//  else if(M12!=0) m12abshdiva = M12/heqII;
//  if(M12 == 0.) etaII =0.; // to avoid nan value in case of initial broken
//  else etaII = m12abshdiva/(m12abshdiva+N12);
  // compute initial value
  unjump0 = - scaldot(du,lbs->basisVector(1))/lbs->basisVector(1).norm(); // minus because minus normal of shellLocalBasis of interface = - phi0,1
  utjump0 = scaldot(du,lbs->basisVector(0))/lbs->basisVector(0).norm();
  rnjump0 = - scaldot(dr,lbs->basisVector(1))/lbs->basisVector(1).norm(); // minus because minus normal of shellLocalBasis of interface = - phi0,1
  rtjump0 = scaldot(dr,lbs->basisVector(0))/lbs->basisVector(0).norm();
  deltan0 = etaI*heqI*rnjump0 + (1-etaI)*unjump0;
  deltat0 = etaII*heqII*rtjump0 + (1-etaII)*utjump0;
/*
  double hdiv6 = h_/6.;
  double hdiva = h_/6.;
  double M0 = m0(1,1);
  double N0 = n0(1,1);
  double N12 = n0(0,1);
  double M12 = m0(0,1);
  double m0abshdiv6, m12abshdiva;
  if(M0<0) {m0abshdiv6 = - M0/hdiv6; rnjump0 = -rnjump0;}
  else m0abshdiv6 = M0/hdiv6;
  if(M0 == 0. and N0 == 0.) etaI = 0.; // to avoid nan value in case of initial broken
  else etaI = m0abshdiv6/(m0abshdiv6+N0);
  if(M12<0) {m12abshdiva = - M12/hdiv6; rtjump0 = -rtjump0;}
  else m12abshdiva = M12/hdiv6;
  if(M12 == 0. and N12 == 0.) etaII =0.; // to avoid nan value in case of initial broken
  else etaII = m12abshdiva/(m12abshdiva+N12);
  // compute initial value
  unjump0 = - scaldot(du,lbs->basisVector(1))/lbs->basisVector(1).norm(); // minus because minus normal of shellLocalBasis of interface = - phi0,1
  utjump0 = scaldot(du,lbs->dualBasisVector(0))/lbs->basisVector(0).norm();
  rnjump0 = - scaldot(dr,lbs->basisVector(1))/lbs->basisVector(1).norm(); // minus because minus normal of shellLocalBasis of interface = - phi0,1
  rtjump0 = scaldot(dr,lbs->dualBasisVector(0))/lbs->basisVector(0).norm();
  deltan0 = etaI*hdiv6*rnjump0 + (1-etaI)*unjump0;
  deltat0 = etaII*hdiva*rtjump0 + (1-etaII)*utjump0;
*/
  lengthphi1 = lbs->basisVector(0).norm();
  lengthphi2 = lbs->basisVector(1).norm();
}

void IPLinearCohesiveShell::initmean(IPLinearCohesiveShell *ipv2)
{
 #ifdef _DEBUG
  if(tension!= ipv2->ifTension())
    Msg::Error("Initial fracture with one element in tension and the other in compression!");
 #endif
  sigmac += ipv2->getSigmac();
  sigmac*=0.5;
  deltac += ipv2->getDeltac();
  deltac*=0.5;
  n0 += ipv2->getn0();
  m0 += ipv2->getm0();
  n0 *= 0.5;
  m0 *= 0.5;
  sigI += ipv2->getSigI();
  tauII+= ipv2->getTauII();
  sigI*=0.5;
  tauII*=0.5;
  ipv2->sigmac = sigmac;
  ipv2->deltac = deltac;
  ipv2->n0 = n0;
  ipv2->m0 = m0;
  ipv2->sigI = sigI;
  ipv2->tauII = tauII;
  _n = n0;
  _m = m0;
  ipv2->setnm(n0,m0);
}

void IPLinearCohesiveShell::setFracture(const IPLinearCohesiveShell*ipvprev,const SVector3 &ujump_,
                                        const double rjump_[3], const shellLocalBasis *lbs, const double h)
{
  n0 =ipvprev->n0;
  m0 =ipvprev->m0;
  unjump0 = ipvprev->unjump0;
  rnjump0 = ipvprev->rnjump0;
  utjump0 = ipvprev->utjump0;
  rtjump0 = ipvprev->rtjump0;
  sigmac = ipvprev->sigmac;
  sigI = ipvprev->sigI;
  tauII= ipvprev->tauII;
  deltac = ipvprev->deltac;
  deltan0 = ipvprev->deltan0;
  deltat0 = ipvprev->deltat0;
  etaI = ipvprev->etaI;
  etaII= ipvprev->etaII;
  heqI = ipvprev->heqI;
  heqII= ipvprev->heqII;
  beta = ipvprev->beta;
  tension = ipvprev->tension;
  openingForShell(ujump_,rjump_,this,lbs,h,deltan,deltat,delta);
  // Update deltamax
  deltamax = ipvprev->deltamax;
  if(fabs(delta)>fabs(deltamax)) deltamax = delta;
  unjump = - scaldot(ujump_,lbs->basisVector(1))/lbs->basisVector(1).norm(); // - unjump0; // minus because normal of shellLocalBasis of interface = - phi0,1
  utjump = scaldot(ujump_,lbs->basisVector(0))/lbs->basisVector(0).norm(); // - utjump0;
  if( m0(1,1)<0) rnjump = scaldot(rjump_,lbs->basisVector(1))/lbs->basisVector(1).norm(); // - rnjump0;
  else rnjump = - scaldot(rjump_,lbs->basisVector(1))/lbs->basisVector(1).norm(); // - rnjump0;
  if( m0(0,1)<0) rtjump = scaldot(rjump_,lbs->basisVector(0))/lbs->basisVector(0).norm(); // - rtjump0;
  else rtjump = - scaldot(rjump_,lbs->basisVector(0))/lbs->basisVector(0).norm(); // - rtjump0;
/*  double hdiv6 = this->getThickness()/6.;
  double hdiva = this->getThickness()/alpha;
  deltan = etaI*hdiv6*rnjump + (1-etaI)*unjump;
  deltat = etaII*hdiva*rtjump+(1-etaII)*utjump;
  if(deltan > 0.)
    delta = sqrt(deltan*deltan+beta*beta*deltat*deltat);
  else
    delta = beta*deltat;
*/
  lengthphi1 = lbs->basisVector(0).norm();
  lengthphi2 = lbs->basisVector(1).norm();
}

void IPLinearCohesiveShell::setnm(const reductionElement &n, const reductionElement &m)
{
  for(int i=0;i<2;i++)
    for(int j=0;j<2;j++)
    {
      _n(i,j) = n(i,j);
      _m(i,j) = m(i,j);
    }
}
//--------------------------------------------------------------------------------------------------------------------------------------
//definition of class IPLinearCohesiveShellWithOrientation added by shantanu on 10-08-2012
IPLinearCohesiveShellWithOrientation::IPLinearCohesiveShellWithOrientation() : IPLinearCohesiveShell(), _rotation_angle(0.0){}
//----------------------------------------------------------------------------------------------------------------------------
IPLinearCohesiveShellWithOrientation::IPLinearCohesiveShellWithOrientation(const double &_rot_angle) : IPLinearCohesiveShell()
{
    _rotation_angle = _rot_angle;
}
//----------------------------------------------------------------------------------------------------------------------------------------
IPLinearCohesiveShellWithOrientation::IPLinearCohesiveShellWithOrientation(const IPLinearCohesiveShellWithOrientation &source) : IPLinearCohesiveShell(source)
{
    _rotation_angle = source._rotation_angle;
}
//------------------------------------------------------------------------------------------------------------------------------------------
void IPLinearCohesiveShellWithOrientation::set_rotation_angle(const double &_angle)
{
    _rotation_angle = _angle;
}
//---------------------------------------------------------------------------------------------------------------------------------------
double IPLinearCohesiveShellWithOrientation::get_rotation_angle() const
{
    return(_rotation_angle);
}
//---------------------------------------------------------------------------------------------------------------------------------------
void IPLinearCohesiveShellWithOrientation::setDataFromLaw(const double smax, const double sI, const double tII, const double Gc, const double beta_, const bool ift, const double angle_)
{
    _rotation_angle = angle_;
    _effGc = Gc;
    IPLinearCohesiveShell::setDataFromLaw(smax, sI, tII, getEffGc(), beta_, ift);
    deltac = (2*Gc/smax)/cos(angle_);
}
//-----------------------------------------------------------------------------------------------------------------------------------------
void IPLinearCohesiveShellWithOrientation::setFracture(const IPLinearCohesiveShell*ipvprev,const SVector3 &ujump_,
                                           const double rjump_[3], const shellLocalBasis *lbs, const double h)
{
  IPLinearCohesiveShell::setFracture(ipvprev,ujump_,rjump_,lbs,h);
  _rotation_angle = (dynamic_cast<const IPLinearCohesiveShellWithOrientation*>(ipvprev))->_rotation_angle;
}
//-----------------------------------------------------------------------------------------------------------------------------------------
void IPLinearCohesiveShellWithOrientation::initmean(IPLinearCohesiveShell *ipv2)
{
  IPLinearCohesiveShell::initmean(ipv2);
  _rotation_angle += (dynamic_cast<IPLinearCohesiveShellWithOrientation*>(ipv2))->_rotation_angle;
  _rotation_angle*=0.5;
  (dynamic_cast<IPLinearCohesiveShellWithOrientation*>(ipv2))->_rotation_angle = _rotation_angle;
}
//---------------------------------------------------------------------------------------------------------------------------------------
IPLinearCohesiveShellWithOrientation& IPLinearCohesiveShellWithOrientation::operator = (const IPVariable &source)
{
  IPLinearCohesiveShell::operator=(source);//immediate parent class function
  const IPLinearCohesiveShellWithOrientation *srcwo = dynamic_cast<const IPLinearCohesiveShellWithOrientation*>(&source);
  _rotation_angle =srcwo->_rotation_angle;
  return *this;
}
//--------------------------------------------------------------------------------------------------------------------------------------

IPShellFractureCohesive::IPShellFractureCohesive(const double facSigmac) : IPVariable2ForFracture<IPVariableShell,IPLinearCohesiveShell>(),
                                                                           _facSigmac(facSigmac), _initTime(0.){}
IPShellFractureCohesive::IPShellFractureCohesive(const IPShellFractureCohesive &source) : IPVariable2ForFracture<IPVariableShell,IPLinearCohesiveShell>(source),
                                                                  _facSigmac(source._facSigmac), _initTime(source._initTime){}
IPShellFractureCohesive& IPShellFractureCohesive::operator=(const IPVariable &source)
{
  IPVariable2ForFracture<IPVariableShell,IPLinearCohesiveShell>::operator=(source);
  const IPShellFractureCohesive *src = static_cast<const IPShellFractureCohesive*>(&source);
  _facSigmac = src->_facSigmac;
  _initTime = src->_initTime;
  return *this;
}

void IPShellFractureCohesive::createRestart(FILE *fp)
{
  IPVariable2ForFracture<IPVariableShell,IPLinearCohesiveShell>::createRestart(fp);
  _ipvbulk->createRestart(fp);
  _ipvfrac->createRestart(fp);
  fprintf(fp,"%e %e ",_facSigmac,_initTime);
}

void IPShellFractureCohesive::setFromRestart(FILE *fp)
{
  IPVariable2ForFracture<IPVariableShell,IPLinearCohesiveShell>::setFromRestart(fp);
  _ipvbulk->setFromRestart(fp);
  _ipvfrac->setFromRestart(fp);
  fscanf(fp,"%e %e",&_facSigmac,&_initTime);
}
