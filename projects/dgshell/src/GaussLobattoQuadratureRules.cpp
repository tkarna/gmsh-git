//
// C++ Interface: Quadrature Rule
//
// Description: Define 1D Gauss-Lobatto quadrature rule (number of point 3 to 6)
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "GaussLobattoQuadratureRules.h"
#include "GaussLobattoQuadrature.h"
int GaussLobatto1DQuadrature::getIntPoints(MElement *e, IntPt **GP)
{
  switch(e->getType()){
   case TYPE_PNT :
    *GP = getGaussLobattoQuadrature0DPts(0);
    return getNGaussLobattoQuadrature0DPts(0);
   case TYPE_LIN :
    *GP = getGaussLobattoQuadrature1DPts(_order);
    return getNGaussLobattoQuadrature1DPts(_order);
   default:
    Msg::Error("The Gauss-Lobatto quadrature rule is not defined for this type of element");
    return 0;
  }
}
