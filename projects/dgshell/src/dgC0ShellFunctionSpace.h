//
// C++ Interface: terms
//
// Description: FunctionSpace used (scalar lagragian function space in 3D)
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef DGC0PLATEFUNCTIONSPACE_H_
#define DGC0PLATEFUNCTIONSPACE_H_
#include "MElement.h"
#include "functionSpace.h"
#include "MInterfaceElement.h"
#include "Dof3IntType.h"
#include "groupOfElements.h"
#include "GmshConfig.h"
#include "interFunctionSpace.h"
#include "MInterfaceLine.h"
#include "ThreeDLagrangeFunctionSpace.h"

class DgC0CgDgLagrangeFunctionSpace : public IsoparametricLagrangeFunctionSpace{
 public :
  DgC0CgDgLagrangeFunctionSpace(int id) : IsoparametricLagrangeFunctionSpace(id){};
  DgC0CgDgLagrangeFunctionSpace(int id, int comp1) : IsoparametricLagrangeFunctionSpace(id,comp1){}
  DgC0CgDgLagrangeFunctionSpace(int id, int comp1,int comp2) : IsoparametricLagrangeFunctionSpace(id,comp1,comp2){}
  virtual ~DgC0CgDgLagrangeFunctionSpace(){}

 protected :
  virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys)
  {
    int nk=ele->getNumVertices(); // return the number of vertices
    // negative type in mpi if the Dof are not located on this partition
    // take into account the number of partition in the Dof to ensure the good transfert of Dof at a junction between 3 or 4 partitions
    #if defined(HAVE_MPI) // small duplication to avoid multiple if in a loop
    if( (ele->getPartition() != 0)and (ele->getPartition() != Msg::GetCommRank() +1))
    {
      for (int j=0;j<_ncomp;++j)
        for (int i=0;i<nk;++i)
          keys.push_back(Dof(ele->getVertex(i)->getNum(),-Dof3IntType::createTypeWithThreeInts(comp[j],_ifield,ele->getPartition())));
    }
    else
    #endif // HAVE_MPI
    {
      for (int j=0;j<_ncomp;++j)
        for (int i=0;i<nk;++i)
          keys.push_back(Dof(ele->getVertex(i)->getNum(),Dof3IntType::createTypeWithThreeInts(comp[j],_ifield,ele->getPartition())));
    }
  }
 public:
// warning MPI used Seems unused??
//  virtual void getKeys(MVertex *ver, std::vector<Dof> &keys){
//    for(int j=0;j<_ncomp;j++)
//      keys.push_back(Dof(ver->getNum(),Dof3IntType::createTypeWithThreeInts(comp[j],_ifield)));
//  }
};

class DgC0FullDgLagrangeFunctionSpace : public IsoparametricLagrangeFunctionSpace{
 protected:
  virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys){
    int nk=ele->getNumVertices(); // return the number of vertices
    #if defined(HAVE_MPI) // small duplication to avoid multiple if in a loop
    if( (ele->getPartition() != 0)and (ele->getPartition() != Msg::GetCommRank() +1))
    {
      for (int j=0;j<_ncomp;++j)
        for (int i=0;i<nk;i++)
          keys.push_back(Dof(ele->getNum(),-Dof3IntType::createTypeWithThreeInts(comp[j],_ifield,i)));
    }
    else
    #endif // HAVE_MPI
    {
      for (int j=0;j<_ncomp;++j)
        for (int i=0;i<nk;i++)
          keys.push_back(Dof(ele->getNum(),Dof3IntType::createTypeWithThreeInts(comp[j],_ifield,i)));
    }
  }
 public :
  DgC0FullDgLagrangeFunctionSpace(int id) : IsoparametricLagrangeFunctionSpace(id){};
  DgC0FullDgLagrangeFunctionSpace(int id, int comp1) : IsoparametricLagrangeFunctionSpace(id,comp1){}
  DgC0FullDgLagrangeFunctionSpace(int id,int comp1,int comp2) : IsoparametricLagrangeFunctionSpace(id,comp1,comp2){}
  virtual ~DgC0FullDgLagrangeFunctionSpace(){}
};


class DgC0CgDgBoundaryConditionLagrangeFunctionSpace : public DgC0CgDgLagrangeFunctionSpace{
 public:
  DgC0CgDgBoundaryConditionLagrangeFunctionSpace(int id) : DgC0CgDgLagrangeFunctionSpace(id){}
  DgC0CgDgBoundaryConditionLagrangeFunctionSpace(int id, int comp1) : DgC0CgDgLagrangeFunctionSpace(id,comp1){}
  DgC0CgDgBoundaryConditionLagrangeFunctionSpace(int id, int comp1, int comp2) : DgC0CgDgLagrangeFunctionSpace(id,comp1,comp2){}
  virtual ~DgC0CgDgBoundaryConditionLagrangeFunctionSpace(){}

  virtual void getKeys(MElement *ele, std::vector<Dof> &keys){
    DgC0CgDgLagrangeFunctionSpace::getKeysOnElement(ele,keys);
  }
};

class DgC0CgDgBoundaryConditionLagrangeFunctionSpaceGhost : public DgC0CgDgBoundaryConditionLagrangeFunctionSpace{
 public:
  DgC0CgDgBoundaryConditionLagrangeFunctionSpaceGhost(int id) : DgC0CgDgBoundaryConditionLagrangeFunctionSpace(id){}
  DgC0CgDgBoundaryConditionLagrangeFunctionSpaceGhost(int id, int comp1) : DgC0CgDgBoundaryConditionLagrangeFunctionSpace(id,comp1){}
  DgC0CgDgBoundaryConditionLagrangeFunctionSpaceGhost(int id, int comp1, int comp2) : DgC0CgDgBoundaryConditionLagrangeFunctionSpace(id,comp1,comp2){}
  virtual ~DgC0CgDgBoundaryConditionLagrangeFunctionSpaceGhost(){};

  // Here we be sure that the dofs have negative type (Ghost)
  virtual void getKeys(MElement *ele, std::vector<Dof> &keys){
    int nk = ele->getNumVertices();
    for (int j=0;j<_ncomp;++j)
      for (int i=0;i<nk;++i)
        keys.push_back(Dof(ele->getVertex(i)->getNum(),-Dof3IntType::createTypeWithThreeInts(comp[j],_ifield,ele->getPartition())));
  }
};


class DgC0FullDgBoundaryConditionLagrangeFunctionSpace : public DgC0FullDgLagrangeFunctionSpace{
 protected:
  std::map<MElement*,std::pair<std::vector<int>,std::vector<int> > > mapLocalVertex;
  std::map<MElement*,std::pair<MElement*,MElement*> > mapInter;
 private:
  // constructor function
  void __init__(const groupOfElements *g, groupOfElements *gi, groupOfElements *vinter,groupOfElements *ghostMPI)
  {
    for(groupOfElements::elementContainer::iterator it=g->begin(); it!=g->end(); ++it){
      MElement *e = *it;
      bool InternalEdge=false;
      std::vector<MVertex*> tabV;
      int nv=e->getNumVertices();
      for (int i=0;i<nv;++i) tabV.push_back(e->getVertex(i));
      MInterfaceLine *ielem=NULL;
      // Find the interface element linked with e
      int ind, posfind;
      if(nv==1) ind =0; // BC to applied on a vertex --> look at extremities too
      else ind = 2;
      for(groupOfElements::elementContainer::const_iterator it2=vinter->begin();it2!=vinter->end();++it2){
        int nn = (*it2)->getNumVertices();
        for(int i=ind;i<nn;i++)  // pass node on extremities
          if(tabV[ind]==(*it2)->getVertex(i))
          {
            ielem=dynamic_cast<MInterfaceLine*>(*it2);
            posfind = i;
          }
      }
      // If not find on external edge. Look in internal edge
      if(ielem==NULL){
        for(groupOfElements::elementContainer::const_iterator it2=gi->begin();it2!=gi->end();++it2){
          int nn = (*it2)->getNumVertices();
          for(int i=ind;i<nn;i++)  // pass node on extremities
            if(tabV[ind]==(*it2)->getVertex(i))
            {
              ielem=dynamic_cast<MInterfaceLine*>(*it2);
              posfind = i;
              InternalEdge=true;
            }
        }
      }
     #if defined(HAVE_MPI)
      if(ielem == NULL and Msg::GetCommSize() > 1){
     #else
      if(ielem == NULL){
     #endif // HAVE_MPI
        // not found yet look on ghost element
        bool ghostfind = false;
        if(ghostMPI != NULL) // NULL for Neumman BC
        {
          for(groupOfElements::elementContainer::const_iterator itE=ghostMPI->begin(); itE!=ghostMPI->end();++itE)
          {
            MElement *gele = *itE;
            if(e->getNum() == gele->getNum()) // BC of same dim as domain's element
            {
              std::vector<int> vernum;
              int nvi = gele->getNumVertices();
              vernum.resize(nvi);
              for(int kk=0;kk<nvi;kk++){
                vernum[kk] = kk; // local number of vertex (and we take all vertices...)
              }
              mapLocalVertex[e] = std::pair<std::vector<int>,std::vector<int> >(vernum,vernum);
              mapInter[e] = std::pair<MElement*,MElement*>(gele,gele);
              ghostfind = true;
              break;
            }
            else if(e->getDim() == 0){ // BC applied on a vertex
              // look if there is a match between the vertex
              int nvi = gele->getNumVertices();
              for(int kk=0;kk<nvi;kk++){
                if(e->getNum() == gele->getVertex(kk)->getNum()){
                  std::vector<int> vernum(1);
                  vernum.push_back(kk);
                  mapLocalVertex[e] = std::pair<std::vector<int>,std::vector<int> >(vernum,vernum);
                  mapInter[e] = std::pair<MElement*,MElement*>(gele,gele);
                  ghostfind = true;
                  break;
                }
                if(ghostfind) break;
              }
            }
            else{ // last possibilities BC on an edge
              // identification of the first interior vertex of each edge (OK min degree 2)
              int nedge = gele->getNumEdges();
              std::vector<MVertex*> verEdge;
              for(int kk=0;kk<nedge;kk++){
                gele->getEdgeVertices(kk,verEdge);
                int nver = verEdge.size();
                if((e->getVertex(2) == verEdge[2]) or (e->getVertex(2) == verEdge[nver-1]))
                {
                  std::vector<int> vernum;
                  vernum.resize(nver);
                  // find local vertex num (only triangle and quadrangle !!)
                  switch(kk){
                   case 0 :
                    vernum[0] = 0;
                    vernum[1] = 1;
                    break;
                   case 1 :
                    vernum[0] = 1;
                    vernum[1] = 2;
                    break;
                   case 2 :
                    if(gele->getType()==TYPE_TRI){vernum[0]=2;vernum[1]=0;}
                    else{vernum[0]=2;vernum[1]=3;}
                    break;
                   case 3 :
                    vernum[0] = 3;
                    vernum[1] = 0;
                    break;
                   default : Msg::Error("Impossible to get local vertex number in this case");
                  }
                  // interior edge node
                  for(int j=2;j<vernum.size();j++)
                    vernum[j]=gele->getNumEdges()+kk*(gele->getPolynomialOrder()-1)+(j-2);
                  mapLocalVertex[e] = std::pair<std::vector<int>,std::vector<int> >(vernum,vernum);
                  mapInter[e] = std::pair<MElement*,MElement*>(gele,gele);
                  ghostfind = true;
                  break;
                }
                if(ghostfind) break;
              }
            }
          }
        }
        if(!ghostfind){
          Msg::Error("Impossible to fix dof for element %d on rank %d",e->getNum(),Msg::GetCommRank());
        }
      }
      else{
        int nvi=ielem->getNumVertices();
        std::vector<int> vernum;
        vernum.resize(nvi);
        ielem->getLocalVertexNum(0,vernum);
        if(nv == 1){ // if boundary condition is applied on a vertex
          int temp = vernum[posfind];
          vernum.clear();
          vernum.push_back(temp);
        }
        if(InternalEdge){
          std::vector<int> vernum2;
          vernum2.resize(nvi);
          ielem->getLocalVertexNum(1,vernum2);
          if(nv == 1){ // if boundary condition is applied on a vertex
            int temp = vernum2[posfind];
            vernum2.clear();
            vernum2.push_back(temp);
          }
          mapLocalVertex[e] = std::pair<std::vector<int>,std::vector<int> >(vernum,vernum2);
          mapInter[e] = std::pair<MElement*,MElement*>(ielem->getElem(0),ielem->getElem(1));
        }
        else{
          mapLocalVertex[e] = std::pair<std::vector<int>,std::vector<int> >(vernum,vernum);
          mapInter[e] = std::pair<MElement*,MElement*>(ielem->getElem(0),ielem->getElem(0));
        }
      }
    }
  }
 public:

  virtual ~DgC0FullDgBoundaryConditionLagrangeFunctionSpace(){}
  // Copy the constructor function
  DgC0FullDgBoundaryConditionLagrangeFunctionSpace(int id, const groupOfElements *g, groupOfElements *gi,
                                          groupOfElements *vinter, groupOfElements *ghostMPI) : DgC0FullDgLagrangeFunctionSpace(id)
  {
    this->__init__(g,gi,vinter,ghostMPI);
  }
  DgC0FullDgBoundaryConditionLagrangeFunctionSpace(int id, int comp1, const groupOfElements *g, groupOfElements *gi,
                                          groupOfElements *vinter, groupOfElements *ghostMPI) : DgC0FullDgLagrangeFunctionSpace(id,comp1)
  {
    this->__init__(g,gi,vinter,ghostMPI);
  }
  DgC0FullDgBoundaryConditionLagrangeFunctionSpace(int id, int comp1, int comp2,const groupOfElements *g, groupOfElements *gi,
                                          groupOfElements *vinter, groupOfElements *ghostMPI) : DgC0FullDgLagrangeFunctionSpace(id,comp1,comp2)
  {
    this->__init__(g,gi,vinter,ghostMPI);
  }

  virtual void getKeys(MInterfaceElement *ielem, std::vector<Dof> &keys){
    Msg::Error("Impossible to get keys on interface element for a Dirichlet Boundary Conditions");
  }
  virtual void getKeys(MElement *e, std::vector<Dof> &keys){
    std::map<MElement*,std::pair<MElement*,MElement*> >::iterator itele=mapInter.find(e);
    MElement *ele1 = itele->second.first;
    MElement *ele2 = itele->second.second;
    std::map<MElement*,std::pair<std::vector<int>,std::vector<int> > >::iterator itvec=mapLocalVertex.find(e);
    int nbcomp=comp.size();
    #if defined(HAVE_MPI) // small duplication to avoid multiple if in a loop
    if( (ele1->getPartition() != 0)and (ele1->getPartition() != Msg::GetCommRank() +1))
    {
      for (int j=0;j<nbcomp;++j)
        for (int i=0;i<itvec->second.first.size();i++)
          keys.push_back(Dof(ele1->getNum(),-Dof3IntType::createTypeWithThreeInts(comp[j],this->getId(),itvec->second.first[i])));
    }
    else
    #endif // HAVE_MPI
    {
      for (int j=0;j<nbcomp;++j)
        for (int i=0;i<itvec->second.first.size();i++)
          keys.push_back(Dof(ele1->getNum(),Dof3IntType::createTypeWithThreeInts(comp[j],this->getId(),itvec->second.first[i])));
    }
    // Keys of plus element in case of a internal boundary conditions
    if(ele1 != ele2){
      #if defined(HAVE_MPI) // idem
      if( (ele2->getPartition() != 0)and (ele2->getPartition() != Msg::GetCommRank() +1))
      {
        for (int j=0;j<nbcomp;++j)
          for (int i=0;i<itvec->second.second.size();i++)
            keys.push_back(Dof(ele2->getNum(),-Dof3IntType::createTypeWithThreeInts(comp[j],this->getId(),itvec->second.second[i])));
      }
      else
      #endif
      {
        for (int j=0;j<nbcomp;++j)
          for (int i=0;i<itvec->second.second.size();i++)
            keys.push_back(Dof(ele2->getNum(),Dof3IntType::createTypeWithThreeInts(comp[j],this->getId(),itvec->second.second[i])));
      }
    }
  }

};

// Space of the interface with acces to the two function space
class DgC0FullDgLagrangeBetween2DomainsFunctionSpace : public IsoparametricLagrangeFunctionSpace, public interFunctionSpace{
 protected:
  FunctionSpaceBase *spaceMinus;
  FunctionSpaceBase *spacePlus;
  virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys){
    Msg::Error("Impossible to get key for an element on a interface Domain");
  }
 public:
  DgC0FullDgLagrangeBetween2DomainsFunctionSpace(FunctionSpaceBase*sp1, FunctionSpaceBase *sp2) :
                                                  IsoparametricLagrangeFunctionSpace(0), spaceMinus(sp1), spacePlus(sp2){}
  virtual ~DgC0FullDgLagrangeBetween2DomainsFunctionSpace(){}

  virtual void getKeys(MInterfaceElement *iele, std::vector<Dof> &keys){
    spaceMinus->getKeys(iele->getElem(0),keys);
    spacePlus->getKeys(iele->getElem(1),keys);
  }
  virtual void getKeys(MElement *ele, std::vector<Dof> &keys){
    // As all element are interface element on an interface domain
    MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(ele);
    this->getKeys(iele,keys);
  }
  // special function of interFunctionSpace
  virtual FunctionSpaceBase* getMinusSpace()const {return spaceMinus;}
  virtual FunctionSpaceBase* getPlusSpace() const {return spacePlus;}
  virtual void getNumKeys(MInterfaceElement *ele, int &numMinus, int &numPlus) const
  {
    numMinus = spaceMinus->getNumKeys(ele->getElem(0));
    numPlus = spacePlus->getNumKeys(ele->getElem(1));
  }
  virtual void getKeys(MInterfaceElement *ele, std::vector<Dof> &Rminus,std::vector<Dof> &Rplus) const
  {
    spaceMinus->getKeys(ele->getElem(0),Rminus);
    spacePlus->getKeys(ele->getElem(1),Rplus);
  }
};

#endif // DGC0PLATEFUNCTIONSPACE_H_
