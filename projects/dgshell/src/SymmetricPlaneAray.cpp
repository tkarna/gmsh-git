//
// C++ Interface: material law
//
// Description: Array with the symmetric planes of polycrystalline silicon
//
//
// Author:  <Shantanu MULAY>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
#include "SymmetricPlaneAray.h"

symmetry_planes::symmetry_planes(const double &sigmac100, const double &sigmac110,
                                 const double &sigmac111, const double &Gc100, const double &Gc110, const double &Gc111)
{
    int i=0, j=0;
   //--------Initialization end------------------------------//
   _sigmac100 = sigmac100; _sigmac110 = sigmac110; _sigmac111 = sigmac111;
   _Gc100 = Gc100; _Gc110 = Gc110; _Gc111 = Gc111;

    const double sqrt2 = 1.0/sqrt(2); const double sqrt3 = 1.0/sqrt(3);
    //initialize swap array
    is_n2_n3_swapped[0] = 0; is_n2_n3_swapped[1] = 1; is_n2_n3_swapped[2] = 1; is_n2_n3_swapped[3] = 0; is_n2_n3_swapped[4] = 0; is_n2_n3_swapped[5] = 1;
    is_n2_n3_swapped[6] = 1; is_n2_n3_swapped[7] = 0; is_n2_n3_swapped[8] = 0; is_n2_n3_swapped[9] = 1; is_n2_n3_swapped[10]= 1; is_n2_n3_swapped[11]= 0;
    is_n2_n3_swapped[12] = 1; is_n2_n3_swapped[13] = 0; is_n2_n3_swapped[14] = 1; is_n2_n3_swapped[15] = 0; is_n2_n3_swapped[16] = 0; is_n2_n3_swapped[17] = 1;
    is_n2_n3_swapped[18] = 0; is_n2_n3_swapped[19] = 1; is_n2_n3_swapped[20] = 0; is_n2_n3_swapped[21] = 1; is_n2_n3_swapped[22] = 1; is_n2_n3_swapped[23] = 0;
    is_n2_n3_swapped[24] = 0; is_n2_n3_swapped[25] = 1; is_n2_n3_swapped[26] = 0; is_n2_n3_swapped[27] = 1; is_n2_n3_swapped[28] = 0; is_n2_n3_swapped[29] = 1;
    is_n2_n3_swapped[30] = 0; is_n2_n3_swapped[31] = 1; is_n2_n3_swapped[32] = 1; is_n2_n3_swapped[33] = 0; is_n2_n3_swapped[34] = 1; is_n2_n3_swapped[35] = 0;
    is_n2_n3_swapped[36] = 1; is_n2_n3_swapped[37] = 0; is_n2_n3_swapped[38] = 0; is_n2_n3_swapped[39] = 1; is_n2_n3_swapped[40] = 0; is_n2_n3_swapped[41] = 1;
    is_n2_n3_swapped[42] = 1; is_n2_n3_swapped[43] = 0; is_n2_n3_swapped[44] = 1; is_n2_n3_swapped[45] = 0; is_n2_n3_swapped[46] = 1; is_n2_n3_swapped[47] = 0;
    //initialize "family_1, family_2, and family_3"
    for(i=0; i<8; i++)//8 quadrants are needed to completly define the symmetry planes
    {
        if (i == 0)//theta = 0 to 90 and phi = 0 to 90
        {
            family_1[0][0] = 1.0; family_1[0][1] = 0.0; family_1[0][2] = 0.0;
            family_2[0][0] = sqrt2; family_2[0][1] = sqrt2; family_2[0][2] = 0.0;
            family_3[0][0] = sqrt3; family_3[0][1] = sqrt3; family_3[0][2] = sqrt3;

            family_1[1][0] = 1.0; family_1[1][1] = 0.0; family_1[1][2] = 0.0;
            family_2[1][0] = sqrt2; family_2[1][1] = 0.0; family_2[1][2] = sqrt2;
            family_3[1][0] = sqrt3; family_3[1][1] = sqrt3; family_3[1][2] = sqrt3;

            family_1[2][0] = 0.0; family_1[2][1] = 1.0; family_1[2][2] = 0.0;
            family_2[2][0] = sqrt2; family_2[2][1] = sqrt2; family_2[2][2] = 0.0;
            family_3[2][0] = sqrt3; family_3[2][1] = sqrt3; family_3[2][2] = sqrt3;

            family_1[3][0] = 0.0; family_1[3][1] = 1.0; family_1[3][2] = 0.0;
            family_2[3][0] = 0.0; family_2[3][1] = sqrt2; family_2[3][2] = sqrt2;
            family_3[3][0] = sqrt3; family_3[3][1] = sqrt3; family_3[3][2] = sqrt3;

            family_1[4][0] = 0.0; family_1[4][1] = 0.0; family_1[4][2] = 1.0;
            family_2[4][0] = sqrt2; family_2[4][1] = 0.0; family_2[4][2] = sqrt2;
            family_3[4][0] = sqrt3; family_3[4][1] = sqrt3; family_3[4][2] = sqrt3;

            family_1[5][0] = 0.0; family_1[5][1] = 0.0; family_1[5][2] = 1.0;
            family_2[5][0] = 0.0; family_2[5][1] = sqrt2; family_2[5][2] = sqrt2;
            family_3[5][0] = sqrt3; family_3[5][1] = sqrt3; family_3[5][2] = sqrt3;
        }
        else if(i == 1)//theta = 90 to 180 and phi = 0 to 90
        {
            family_1[6][0] = -1.0; family_1[6][1] = 0.0; family_1[6][2] = 0.0;
            family_2[6][0] = -sqrt2; family_2[6][1] = sqrt2; family_2[6][2] = 0.0;
            family_3[6][0] = -sqrt3; family_3[6][1] = sqrt3; family_3[6][2] = sqrt3;

            family_1[7][0] = -1.0; family_1[7][1] = 0.0; family_1[7][2] = 0.0;
            family_2[7][0] = -sqrt2; family_2[7][1] = 0.0; family_2[7][2] = sqrt2;
            family_3[7][0] = -sqrt3; family_3[7][1] = sqrt3; family_3[7][2] = sqrt3;

            family_1[8][0] = 0.0; family_1[8][1] = 1.0; family_1[8][2] = 0.0;
            family_2[8][0] = -sqrt2; family_2[8][1] = sqrt2; family_2[8][2] = 0.0;
            family_3[8][0] = -sqrt3; family_3[8][1] = sqrt3; family_3[8][2] = sqrt3;

            family_1[9][0] = 0.0; family_1[9][1] = 1.0; family_1[9][2] = 0.0;
            family_2[9][0] = 0.0; family_2[9][1] = sqrt2; family_2[9][2] = sqrt2;
            family_3[9][0] = -sqrt3; family_3[9][1] = sqrt3; family_3[9][2] = sqrt3;

            family_1[10][0] = 0.0; family_1[10][1] = 0.0; family_1[10][2] = 1.0;
            family_2[10][0] = -sqrt2; family_2[10][1] = 0.0; family_2[10][2] = sqrt2;
            family_3[10][0] = -sqrt3; family_3[10][1] = sqrt3; family_3[10][2] = sqrt3;

            family_1[11][0] = 0.0; family_1[11][1] = 0.0; family_1[11][2] = 1.0;
            family_2[11][0] = 0.0; family_2[11][1] = sqrt2; family_2[11][2] = sqrt2;
            family_3[11][0] = -sqrt3; family_3[11][1] = sqrt3; family_3[11][2] = sqrt3;
        }//esle fi (i == 1)
        else if(i == 2)//theta = 180 to 270 and phi = 0 to 90
        {
            family_1[12][0] = -1.0; family_1[12][1] = 0.0; family_1[12][2] = 0.0;
            family_2[12][0] = -sqrt2; family_2[12][1] = 0.0; family_2[12][2] = sqrt2;
            family_3[12][0] = -sqrt3; family_3[12][1] = -sqrt3; family_3[12][2] = sqrt3;

            family_1[13][0] = -1.0; family_1[13][1] = 0.0; family_1[13][2] = 0.0;
            family_2[13][0] = -sqrt2; family_2[13][1] = -sqrt2; family_2[13][2] = 0.0;
            family_3[13][0] = -sqrt3; family_3[13][1] = -sqrt3; family_3[13][2] = sqrt3;

            family_1[14][0] = 0.0; family_1[14][1] = -1.0; family_1[14][2] = 0.0;
            family_2[14][0] = -sqrt2; family_2[14][1] = -sqrt2; family_2[14][2] = 0.0;
            family_3[14][0] = -sqrt3; family_3[14][1] = -sqrt3; family_3[14][2] = sqrt3;

            family_1[15][0] = 0.0; family_1[15][1] = -1.0; family_1[15][2] = 0.0;
            family_2[15][0] = 0.0; family_2[15][1] = -sqrt2; family_2[15][2] = sqrt2;
            family_3[15][0] = -sqrt3; family_3[15][1] = -sqrt3; family_3[15][2] = sqrt3;

            family_1[16][0] = 0.0; family_1[16][1] = 0.0; family_1[16][2] = 1.0;
            family_2[16][0] = -sqrt2; family_2[16][1] = 0.0; family_2[16][2] = sqrt2;
            family_3[16][0] = -sqrt3; family_3[16][1] = -sqrt3; family_3[16][2] = sqrt3;

            family_1[17][0] = 0.0; family_1[17][1] = 0.0; family_1[17][2] = 1.0;
            family_2[17][0] = 0.0; family_2[17][1] = -sqrt2; family_2[17][2] = sqrt2;
            family_3[17][0] = -sqrt3; family_3[17][1] = -sqrt3; family_3[17][2] = sqrt3;
        }//esle fi (i == 2)
        else if(i == 3)//theta = 270 to 360 and phi = 0 to 90
        {
            family_1[18][0] = 1.0; family_1[18][1] = 0.0; family_1[18][2] = 0.0;
            family_2[18][0] = sqrt2; family_2[18][1] = 0.0; family_2[18][2] = sqrt2;
            family_3[18][0] = sqrt3; family_3[18][1] = -sqrt3; family_3[18][2] = sqrt3;

            family_1[19][0] = 1.0; family_1[19][1] = 0.0; family_1[19][2] = 0.0;
            family_2[19][0] = sqrt2; family_2[19][1] = -sqrt2; family_2[19][2] = 0.0;
            family_3[19][0] = sqrt3; family_3[19][1] = -sqrt3; family_3[19][2] = sqrt3;

            family_1[20][0] = 0.0; family_1[20][1] = -1.0; family_1[20][2] = 0.0;
            family_2[20][0] = sqrt2; family_2[20][1] = -sqrt2; family_2[20][2] = 0.0;
            family_3[20][0] = sqrt3; family_3[20][1] = -sqrt3; family_3[20][2] = sqrt3;

            family_1[21][0] = 0.0; family_1[21][1] = -1.0; family_1[21][2] = 0.0;
            family_2[21][0] = 0.0; family_2[21][1] = -sqrt2; family_2[21][2] = sqrt2;
            family_3[21][0] = sqrt3; family_3[21][1] = -sqrt3; family_3[21][2] = sqrt3;

            family_1[22][0] = 0.0; family_1[22][1] = 0.0; family_1[22][2] = 1.0;
            family_2[22][0] = sqrt2; family_2[22][1] = 0.0; family_2[22][2] = sqrt2;
            family_3[22][0] = sqrt3; family_3[22][1] = -sqrt3; family_3[22][2] = sqrt3;

            family_1[23][0] = 0.0; family_1[23][1] = 0.0; family_1[23][2] = 1.0;
            family_2[23][0] = 0.0; family_2[23][1] = -sqrt2; family_2[23][2] = sqrt2;
            family_3[23][0] = sqrt3; family_3[23][1] = -sqrt3; family_3[23][2] = sqrt3;
        }//esle fi (i == 3)
        else if(i == 4)//theta = 0 to 90 and phi = 90 to 180
        {
            family_1[24][0] = 1.0; family_1[24][1] = 0.0; family_1[24][2] = 0.0;
            family_2[24][0] = sqrt2; family_2[24][1] = 0.0; family_2[24][2] = -sqrt2;
            family_3[24][0] = sqrt3; family_3[24][1] = sqrt3; family_3[24][2] = -sqrt3;

            family_1[25][0] = 1.0; family_1[25][1] = 0.0; family_1[25][2] = 0.0;
            family_2[25][0] = sqrt2; family_2[25][1] = sqrt2; family_2[25][2] = 0.0;
            family_3[25][0] = sqrt3; family_3[25][1] = sqrt3; family_3[25][2] = -sqrt3;

            family_1[26][0] = 0.0; family_1[26][1] = 1.0; family_1[26][2] = 0.0;
            family_2[26][0] = sqrt2; family_2[26][1] = sqrt2; family_2[26][2] = 0.0;
            family_3[26][0] = sqrt3; family_3[26][1] = sqrt3; family_3[26][2] = -sqrt3;

            family_1[27][0] = 0.0; family_1[27][1] = 1.0; family_1[27][2] = 0.0;
            family_2[27][0] = 0.0; family_2[27][1] = sqrt2; family_2[27][2] = -sqrt2;
            family_3[27][0] = sqrt3; family_3[27][1] = sqrt3; family_3[27][2] = -sqrt3;

            family_1[28][0] = 0.0; family_1[28][1] = 0.0; family_1[28][2] = -1.0;
            family_2[28][0] = 0.0; family_2[28][1] = sqrt2; family_2[28][2] = -sqrt2;
            family_3[28][0] = sqrt3; family_3[28][1] = sqrt3; family_3[28][2] = -sqrt3;

            family_1[29][0] = 0.0; family_1[29][1] = 0.0; family_1[29][2] = -1.0;
            family_2[29][0] = sqrt2; family_2[29][1] = 0.0; family_2[29][2] = -sqrt2;
            family_3[29][0] = sqrt3; family_3[29][1] = sqrt3; family_3[29][2] = -sqrt3;
        }//esle fi (i == 4)
        else if(i == 5)//theta = 90 to 180 and phi = 90 to 180
        {
            family_1[30][0] = -1.0; family_1[30][1] = 0.0; family_1[30][2] = 0.0;
            family_2[30][0] = -sqrt2; family_2[30][1] = sqrt2; family_2[30][2] = 0.0;
            family_3[30][0] = -sqrt3; family_3[30][1] = sqrt3; family_3[30][2] = -sqrt3;

            family_1[31][0] = -1.0; family_1[31][1] = 0.0; family_1[31][2] = 0.0;
            family_2[31][0] = -sqrt2; family_2[31][1] = 0.0; family_2[31][2] = -sqrt2;
            family_3[31][0] = -sqrt3; family_3[31][1] = sqrt3; family_3[31][2] = -sqrt3;

            family_1[32][0] = 0.0; family_1[32][1] = 1.0; family_1[32][2] = 0.0;
            family_2[32][0] = -sqrt2; family_2[32][1] = sqrt2; family_2[32][2] = 0.0;
            family_3[32][0] = -sqrt3; family_3[32][1] = sqrt3; family_3[32][2] = -sqrt3;

            family_1[33][0] = 0.0; family_1[33][1] = 1.0; family_1[33][2] = 0.0;
            family_2[33][0] = 0.0; family_2[33][1] = sqrt2; family_2[33][2] = -sqrt2;
            family_3[33][0] = -sqrt3; family_3[33][1] = sqrt3; family_3[33][2] = -sqrt3;

            family_1[34][0] = 0.0; family_1[34][1] = 0.0; family_1[34][2] = -1.0;
            family_2[34][0] = 0.0; family_2[34][1] = sqrt2; family_2[34][2] = -sqrt2;
            family_3[34][0] = -sqrt3; family_3[34][1] = sqrt3; family_3[34][2] = -sqrt3;

            family_1[35][0] = 0.0; family_1[35][1] = 0.0; family_1[35][2] = -1.0;
            family_2[35][0] = -sqrt2; family_2[35][1] = 0.0; family_2[35][2] = -sqrt2;
            family_3[35][0] = -sqrt3; family_3[35][1] = sqrt3; family_3[35][2] = -sqrt3;
        }//esle fi (i == 5)
        else if(i == 6)//theta = 180 to 270 and phi = 90 to 180
        {
            family_1[36][0] = -1.0; family_1[36][1] = 0.0; family_1[36][2] = 0.0;
            family_2[36][0] = -sqrt2; family_2[36][1] = -sqrt2; family_2[36][2] = 0.0;
            family_3[36][0] = -sqrt3; family_3[36][1] = -sqrt3; family_3[36][2] = -sqrt3;

            family_1[37][0] = -1.0; family_1[37][1] = 0.0; family_1[37][2] = 0.0;
            family_2[37][0] = -sqrt2; family_2[37][1] = 0.0; family_2[37][2] = -sqrt2;
            family_3[37][0] = -sqrt3; family_3[37][1] = -sqrt3; family_3[37][2] = -sqrt3;

            family_1[38][0] = 0.0; family_1[38][1] = -1.0; family_1[38][2] = 0.0;
            family_2[38][0] = -sqrt2; family_2[38][1] = -sqrt2; family_2[38][2] = 0.0;
            family_3[38][0] = -sqrt3; family_3[38][1] = -sqrt3; family_3[38][2] = -sqrt3;

            family_1[39][0] = 0.0; family_1[39][1] = -1.0; family_1[39][2] = 0.0;
            family_2[39][0] = 0.0; family_2[39][1] = -sqrt2; family_2[39][2] = -sqrt2;
            family_3[39][0] = -sqrt3; family_3[39][1] = -sqrt3; family_3[39][2] = -sqrt3;

            family_1[40][0] = 0.0; family_1[40][1] = 0.0; family_1[40][2] = -1.0;
            family_2[40][0] = 0.0; family_2[40][1] = -sqrt2; family_2[40][2] = -sqrt2;
            family_3[40][0] = -sqrt3; family_3[40][1] = -sqrt3; family_3[40][2] = -sqrt3;

            family_1[41][0] = 0.0; family_1[41][1] = 0.0; family_1[41][2] = -1.0;
            family_2[41][0] = -sqrt2; family_2[41][1] = 0.0; family_2[41][2] = -sqrt2;
            family_3[41][0] = -sqrt3; family_3[41][1] = -sqrt3; family_3[41][2] = -sqrt3;
        }//esle fi (i == 6)
        else if(i == 7)//theta = 270 to 360 and phi = 90 to 180
        {
            family_1[42][0] = 1.0; family_1[42][1] = 0.0; family_1[42][2] = 0.0;
            family_2[42][0] = sqrt2; family_2[42][1] = 0.0; family_2[42][2] = -sqrt2;
            family_3[42][0] = sqrt3; family_3[42][1] = -sqrt3; family_3[42][2] = -sqrt3;

            family_1[43][0] = 1.0; family_1[43][1] = 0.0; family_1[43][2] = 0.0;
            family_2[43][0] = sqrt2; family_2[43][1] = -sqrt2; family_2[43][2] = 0.0;
            family_3[43][0] = sqrt3; family_3[43][1] = -sqrt3; family_3[43][2] = -sqrt3;

            family_1[44][0] = 0.0; family_1[44][1] = -1.0; family_1[44][2] = 0.0;
            family_2[44][0] = sqrt2; family_2[44][1] = -sqrt2; family_2[44][2] = 0.0;
            family_3[44][0] = sqrt3; family_3[44][1] = -sqrt3; family_3[44][2] = -sqrt3;

            family_1[45][0] = 0.0; family_1[45][1] = -1.0; family_1[45][2] = 0.0;
            family_2[45][0] = 0.0; family_2[45][1] = -sqrt2; family_2[45][2] = -sqrt2;
            family_3[45][0] = sqrt3; family_3[45][1] = -sqrt3; family_3[45][2] = -sqrt3;

            family_1[46][0] = 0.0; family_1[46][1] = 0.0; family_1[46][2] = -1.0;
            family_2[46][0] = 0.0; family_2[46][1] = -sqrt2; family_2[46][2] = -sqrt2;
            family_3[46][0] = sqrt3; family_3[46][1] = -sqrt3; family_3[46][2] = -sqrt3;

            family_1[47][0] = 0.0; family_1[47][1] = 0.0; family_1[47][2] = -1.0;
            family_2[47][0] = sqrt2; family_2[47][1] = 0.0; family_2[47][2] = -sqrt2;
            family_3[47][0] = sqrt3; family_3[47][1] = -sqrt3; family_3[47][2] = -sqrt3;
        }//esle fi (i == 7)
    }//end for(i=0)
    //initialize "is_n2_n3_swapped" array "0":not swapped and "1":swapped

    //initialize "fracture_strength_array"
    for(i=0; i<48;i++)
    {
        j = is_n2_n3_swapped[i];
        fracture_strength_array[i][0] = _sigmac100;
        Gc_array[i][0] = _Gc100;
        if(j == 0)//not swapped
        {
            fracture_strength_array[i][1] = _sigmac110;
            fracture_strength_array[i][2] = _sigmac111;
            Gc_array[i][1] = _Gc110; Gc_array[i][2] = _Gc111;
        }
        else if(j == 1)//swapped
        {
            fracture_strength_array[i][1] = _sigmac111;
            fracture_strength_array[i][2] = _sigmac110;
            Gc_array[i][1] = _Gc111; Gc_array[i][2] = _Gc110;
        }
    }//end for(i = 0)
    //initialize "n1_d, n2_d, n3_d" arrays
    for(i=0; i<8; i++)//8 quadrants are needed to completly define the symmetry planes
    {
        if (i == 0)//theta = 0 to 90 and phi = 0 to 90
        {
            n1_d[0][0] = 1.000000; n1_d[0][1] = -1.000000; n1_d[0][2] = 0.000000;
            n2_d[0][0] = 0.000000; n2_d[0][1] = 1.414227; n2_d[0][2] = -1.414227;
            n3_d[0][0] = 0.000000; n3_d[0][1] = 0.000000; n3_d[0][2] = 1.731902;

            n1_d[1][0] = 1.000000; n1_d[1][1] = 0.000000; n1_d[1][2] = -1.000000;
            n2_d[1][0] = 0.000000; n2_d[1][1] = 1.731902; n2_d[1][2] = 0.000000;
            n3_d[1][0] = 0.000000; n3_d[1][1] = -1.414227; n3_d[1][2] = 1.414227;

            n1_d[2][0] = -1.000000; n1_d[2][1] = 1.000000; n1_d[2][2] = 0.000000;
            n2_d[2][0] = 0.000000; n2_d[2][1] = 0.000000; n2_d[2][2] = 1.731902;
            n3_d[2][0] = 1.414227; n3_d[2][1] = 0.000000; n3_d[2][2] = -1.414227;

            n1_d[3][0] = 0.000000; n1_d[3][1] = 1.000000; n1_d[3][2] = -1.000000;
            n2_d[3][0] = -1.414227; n2_d[3][1] = 0.000000; n2_d[3][2] = 1.414227;
            n3_d[3][0] = 1.731902; n3_d[3][1] = 0.000000; n3_d[3][2] = 0.000000;

            n1_d[4][0] = -1.000000; n1_d[4][1] = 0.000000; n1_d[4][2] = 1.000000;
            n2_d[4][0] = 1.414227; n2_d[4][1] = -1.414227; n2_d[4][2] = 0.000000;
            n3_d[4][0] = 0.000000; n3_d[4][1] = 1.731902; n3_d[4][2] = 0.000000;

            n1_d[5][0] = 0.000000; n1_d[5][1] = -1.000000; n1_d[5][2] = 1.000000;
            n2_d[5][0] = 1.731902; n2_d[5][1] = 0.000000; n2_d[5][2] = 0.000000;
            n3_d[5][0] = -1.414227; n3_d[5][1] = 1.414227; n3_d[5][2] = 0.000000;
        }
        else if(i == 1)//theta = 90 to 180 and phi = 0 to 90
        {
            n1_d[6][0] = -1.000000; n1_d[6][1] = -1.000000; n1_d[6][2] = 0.000000;
            n2_d[6][0] = 0.000000; n2_d[6][1] = 0.000000; n2_d[6][2] = 1.731902;
            n3_d[6][0] = 0.000000; n3_d[6][1] = 1.414227; n3_d[6][2] = -1.414227;

            n1_d[7][0] = -1.000000; n1_d[7][1] = 0.000000; n1_d[7][2] = -1.000000;
            n2_d[7][0] = 0.000000; n2_d[7][1] = -1.414227; n2_d[7][2] = 1.414227;
            n3_d[7][0] = 0.000000; n3_d[7][1] = 1.731902; n3_d[7][2] = 0.000000;

            n1_d[8][0] = 1.000000; n1_d[8][1] = 1.000000; n1_d[8][2] = 0.000000;
            n2_d[8][0] = -1.414227; n2_d[8][1] = 0.000000; n2_d[8][2] = -1.414227;
            n3_d[8][0] = 0.000000; n3_d[8][1] = -0.000000; n3_d[8][2] = 1.731902;

            n1_d[9][0] = 0.000000; n1_d[9][1] = 1.000000; n1_d[9][2] = -1.000000;
            n2_d[9][0] = -1.731902; n2_d[9][1] = 0.000000; n2_d[9][2] = 0.000000;
            n3_d[9][0] = 1.414227; n3_d[9][1] = -0.000000; n3_d[9][2] = 1.414227;

            n1_d[10][0] = 1.000000; n1_d[10][1] = 0.000000; n1_d[10][2] = 1.000000;
            n2_d[10][0] = 0.000000; n2_d[10][1] = 1.731902; n2_d[10][2] = -0.000000;
            n3_d[10][0] = -1.414227; n3_d[10][1] = -1.414227; n3_d[10][2] = 0.000000;

            n1_d[11][0] = 0.000000; n1_d[11][1] = -1.000000; n1_d[11][2] = 1.000000;
            n2_d[11][0] = 1.414227; n2_d[11][1] = 1.414227; n2_d[11][2] = -0.000000;
            n3_d[11][0] = -1.731902; n3_d[11][1] = 0.000000; n3_d[11][2] = 0.000000;
        }//esle fi (i == 1)
        else if(i == 2)//theta = 180 to 270 and phi = 0 to 90
        {
            n1_d[12][0] = -1.000000; n1_d[12][1] = 0.000000; n1_d[12][2] = -1.000000;
            n2_d[12][0] = 0.000000; n2_d[12][1] = -1.731902; n2_d[12][2] = 0.000000;
            n3_d[12][0] = 0.000000; n3_d[12][1] = 1.414227; n3_d[12][2] = 1.414227;

            n1_d[13][0] = -1.000000; n1_d[13][1] = 1.000000; n1_d[13][2] = 0.000000;
            n2_d[13][0] = -0.000000; n2_d[13][1] = -1.414227; n2_d[13][2] = -1.414227;
            n3_d[13][0] = 0.000000; n3_d[13][1] = 0.000000; n3_d[13][2] = 1.731902;

            n1_d[14][0] = 1.000000; n1_d[14][1] = -1.000000; n1_d[14][2] = 0.000000;
            n2_d[14][0] = 0.000000; n2_d[14][1] = 0.000000; n2_d[14][2] = 1.731902;
            n3_d[14][0] = -1.414227; n3_d[14][1] = -0.000000; n3_d[14][2] = -1.414227;

            n1_d[15][0] = 0.000000; n1_d[15][1] = -1.000000; n1_d[15][2] = -1.000000;
            n2_d[15][0] = 1.414227; n2_d[15][1] = 0.000000; n2_d[15][2] = 1.414227;
            n3_d[15][0] = -1.731902; n3_d[15][1] = 0.000000; n3_d[15][2] = 0.000000;

            n1_d[16][0] = 1.000000; n1_d[16][1] = 0.000000; n1_d[16][2] = 1.000000;
            n2_d[16][0] = -1.414227; n2_d[16][1] = 1.414227; n2_d[16][2] = 0.000000;
            n3_d[16][0] = 0.000000; n3_d[16][1] = -1.731902; n3_d[16][2] = 0.000000;

            n1_d[17][0] = 0.000000; n1_d[17][1] = 1.000000; n1_d[17][2] = 1.000000;
            n2_d[17][0] = -1.731902; n2_d[17][1] = 0.000000; n2_d[17][2] = 0.000000;
            n3_d[17][0] = 1.414227; n3_d[17][1] = -1.414227; n3_d[17][2] = 0.000000;
        }//esle fi (i == 2)
        else if(i == 3)//theta = 270 to 360 and phi = 0 to 90
        {
            n1_d[18][0] = 1.000000; n1_d[18][1] = 0.000000; n1_d[18][2] = -1.000000;
            n2_d[18][0] = -0.000000; n2_d[18][1] = 1.414227; n2_d[18][2] = 1.414227;
            n3_d[18][0] = 0.000000; n3_d[18][1] = -1.731902; n3_d[18][2] = 0.000000;

            n1_d[19][0] = 1.000000; n1_d[19][1] = 1.000000; n1_d[19][2] = 0.000000;
            n2_d[19][0] = -0.000000; n2_d[19][1] = 0.000000; n2_d[19][2] = 1.731902;
            n3_d[19][0] = 0.000000; n3_d[19][1] = -1.414227; n3_d[19][2] = -1.414227;

            n1_d[20][0] = -1.000000; n1_d[20][1] = -1.000000; n1_d[20][2] = 0.000000;
            n2_d[20][0] = 1.414227; n2_d[20][1] = 0.000000; n2_d[20][2] = -1.414227;
            n3_d[20][0] = 0.000000; n3_d[20][1] = 0.000000; n3_d[20][2] = 1.731902;

            n1_d[21][0] = 0.000000; n1_d[21][1] = -1.000000; n1_d[21][2] = -1.000000;
            n2_d[21][0] = 1.731902; n2_d[21][1] = 0.000000; n2_d[21][2] = 0.000000;
            n3_d[21][0] = -1.414227; n3_d[21][1] = 0.000000; n3_d[21][2] = 1.414227;

            n1_d[22][0] = -1.000000; n1_d[22][1] = 0.000000; n1_d[22][2] = 1.000000;
            n2_d[22][0] = 0.000000; n2_d[22][1] = -1.731902; n2_d[22][2] = 0.000000;
            n3_d[22][0] = 1.414227; n3_d[22][1] = 1.414227; n3_d[22][2] = -0.000000;

            n1_d[23][0] = 0.000000; n1_d[23][1] = 1.000000; n1_d[23][2] = 1.000000;
            n2_d[23][0] = -1.414227; n2_d[23][1] = -1.414227; n2_d[23][2] = 0.000000;
            n3_d[23][0] = 1.731902; n3_d[23][1] = 0.000000; n3_d[23][2] = -0.000000;
        }//esle fi (i == 3)
        else if(i == 4)//theta = 0 to 90 and phi = 90 to 180
        {
            n1_d[24][0] = 1.000000; n1_d[24][1] = 0.000000; n1_d[24][2] = 1.000000;
            n2_d[24][0] = 0.000000; n2_d[24][1] = -1.414227; n2_d[24][2] = -1.414227;
            n3_d[24][0] = -0.000000; n3_d[24][1] = 1.731902; n3_d[24][2] = 0.000000;

            n1_d[25][0] = 1.000000; n1_d[25][1] = -1.000000; n1_d[25][2] = 0.000000;
            n2_d[25][0] = 0.000000; n2_d[25][1] = 0.000000; n2_d[25][2] = -1.731902;
            n3_d[25][0] = -0.000000; n3_d[25][1] = 1.414227; n3_d[25][2] = 1.414227;

            n1_d[26][0] = -1.000000; n1_d[26][1] = 1.000000; n1_d[26][2] = 0.000000;
            n2_d[26][0] = 1.414227; n2_d[26][1] = -0.000000; n2_d[26][2] = 1.414227;
            n3_d[26][0] = 0.000000; n3_d[26][1] = 0.000000; n3_d[26][2] = -1.731902;

            n1_d[27][0] = 0.000000; n1_d[27][1] = 1.000000; n1_d[27][2] = 1.000000;
            n2_d[27][0] = 1.731902; n2_d[27][1] = -0.000000; n2_d[27][2] = 0.000000;
            n3_d[27][0] = -1.414227; n3_d[27][1] = 0.000000; n3_d[27][2] = -1.414227;

            n1_d[28][0] = 0.000000; n1_d[28][1] = -1.000000; n1_d[28][2] = -1.000000;
            n2_d[28][0] = -1.414227; n2_d[28][1] = 1.414227; n2_d[28][2] = 0.000000;
            n3_d[28][0] = 1.731902; n3_d[28][1] = 0.000000; n3_d[28][2] = 0.000000;

            n1_d[29][0] = -1.000000; n1_d[29][1] = 0.000000; n1_d[29][2] = -1.000000;
            n2_d[29][0] = 0.000000; n2_d[29][1] = 1.731902; n2_d[29][2] = 0.000000;
            n3_d[29][0] = 1.414227; n3_d[29][1] = -1.414227; n3_d[29][2] = 0.000000;
        }//esle fi (i == 4)
        else if(i == 5)//theta = 90 to 180 and phi = 90 to 180
        {
            n1_d[30][0] = -1.000000; n1_d[30][1] = -1.000000; n1_d[30][2] = 0.000000;
            n2_d[30][0] = 0.000000; n2_d[30][1] = 1.414227; n2_d[30][2] = 1.414227;
            n3_d[30][0] = 0.000000; n3_d[30][1] = 0.000000; n3_d[30][2] = -1.731902;

            n1_d[31][0] = -1.000000; n1_d[31][1] = 0.000000; n1_d[31][2] = 1.000000;
            n2_d[31][0] = 0.000000; n2_d[31][1] = 1.731902; n2_d[31][2] = 0.000000;
            n3_d[31][0] = -0.000000; n3_d[31][1] = -1.414227; n3_d[31][2] = -1.414227;

            n1_d[32][0] = 1.000000; n1_d[32][1] = 1.000000; n1_d[32][2] = 0.000000;
            n2_d[32][0] = 0.000000; n2_d[32][1] = 0.000000; n2_d[32][2] = -1.731902;
            n3_d[32][0] = -1.414227; n3_d[32][1] = 0.000000; n3_d[32][2] = 1.414227;

            n1_d[33][0] = 0.000000; n1_d[33][1] = 1.000000; n1_d[33][2] = 1.000000;
            n2_d[33][0] = 1.414227; n2_d[33][1] = 0.000000; n2_d[33][2] = -1.414227;
            n3_d[33][0] = -1.731902; n3_d[33][1] = 0.000000; n3_d[33][2] = 0.000000;

            n1_d[34][0] = 0.000000; n1_d[34][1] = -1.000000; n1_d[34][2] = -1.000000;
            n2_d[34][0] = -1.731902; n2_d[34][1] = 0.000000; n2_d[34][2] = 0.000000;
            n3_d[34][0] = 1.414227; n3_d[34][1] = 1.414227; n3_d[34][2] = 0.000000;

            n1_d[35][0] = 1.000000; n1_d[35][1] = 0.000000; n1_d[35][2] = -1.000000;
            n2_d[35][0] = -1.414227; n2_d[35][1] = -1.414227; n2_d[35][2] = -0.000000;
            n3_d[35][0] = 0.000000; n3_d[35][1] = 1.731902; n3_d[35][2] = 0.000000;
        }//esle fi (i == 5)
        else if(i == 6)//theta = 180 to 270 and phi = 90 to 180
        {
            n1_d[36][0] = -1.000000; n1_d[36][1] = 1.000000; n1_d[36][2] = 0.000000;
            n2_d[36][0] = -0.000000; n2_d[36][1] = 0.000000; n2_d[36][2] = -1.731902;
            n3_d[36][0] = 0.000000; n3_d[36][1] = -1.414227; n3_d[36][2] = 1.414227;

            n1_d[37][0] = -1.000000; n1_d[37][1] = 0.000000; n1_d[37][2] = 1.000000;
            n2_d[37][0] = 0.000000; n2_d[37][1] = 1.414227; n2_d[37][2] = -1.414227;
            n3_d[37][0] = -0.000000; n3_d[37][1] = -1.731902; n3_d[37][2] = 0.000000;

            n1_d[38][0] = 1.000000; n1_d[38][1] = -1.000000; n1_d[38][2] = 0.000000;
            n2_d[38][0] = -1.414227; n2_d[38][1] = 0.000000; n2_d[38][2] = 1.414227;
            n3_d[38][0] = 0.000000; n3_d[38][1] = -0.000000; n3_d[38][2] = -1.731902;

            n1_d[39][0] = 0.000000; n1_d[39][1] = -1.000000; n1_d[39][2] = 1.000000;
            n2_d[39][0] = -1.731902; n2_d[39][1] = -0.000000; n2_d[39][2] = 0.000000;
            n3_d[39][0] = 1.414227; n3_d[39][1] = 0.000000; n3_d[39][2] = -1.414227;

            n1_d[40][0] = 0.000000; n1_d[40][1] = 1.000000; n1_d[40][2] = -1.000000;
            n2_d[40][0] = 1.414227; n2_d[40][1] = -1.414227; n2_d[40][2] = 0.000000;
            n3_d[40][0] = -1.731902; n3_d[40][1] = 0.000000; n3_d[40][2] = -0.000000;

            n1_d[41][0] = 1.000000; n1_d[41][1] = 0.000000; n1_d[41][2] = -1.000000;
            n2_d[41][0] = 0.000000; n2_d[41][1] = -1.731902; n2_d[41][2] = -0.000000;
            n3_d[41][0] = -1.414227; n3_d[41][1] = 1.414227; n3_d[41][2] = 0.000000;
        }//esle fi (i == 6)
        else if(i == 7)//theta = 270 to 360 and phi = 90 to 180
        {
            n1_d[42][0] = 1.000000; n1_d[42][1] = 0.000000; n1_d[42][2] = 1.000000;
            n2_d[42][0] = 0.000000; n2_d[42][1] = -1.731902; n2_d[42][2] = 0.000000;
            n3_d[42][0] = 0.000000; n3_d[42][1] = 1.414227; n3_d[42][2] = -1.414227;

            n1_d[43][0] = 1.000000; n1_d[43][1] = 1.000000; n1_d[43][2] = 0.000000;
            n2_d[43][0] = 0.000000; n2_d[43][1] = -1.414227; n2_d[43][2] = 1.414227;
            n3_d[43][0] = 0.000000; n3_d[43][1] = 0.000000; n3_d[43][2] = -1.731902;

            n1_d[44][0] = -1.000000; n1_d[44][1] = -1.000000; n1_d[44][2] = 0.000000;
            n2_d[44][0] = 0.000000; n2_d[44][1] = 0.000000; n2_d[44][2] = -1.731902;
            n3_d[44][0] = 1.414227; n3_d[44][1] = 0.000000; n3_d[44][2] = 1.414227;

            n1_d[45][0] = 0.000000; n1_d[45][1] = -1.000000; n1_d[45][2] = 1.000000;
            n2_d[45][0] = -1.414227; n2_d[45][1] = -0.000000; n2_d[45][2] = -1.414227;
            n3_d[45][0] = 1.731902; n3_d[45][1] = 0.000000; n3_d[45][2] = 0.000000;

            n1_d[46][0] = 0.000000; n1_d[46][1] = 1.000000; n1_d[46][2] = -1.000000;
            n2_d[46][0] = 1.731902; n2_d[46][1] = 0.000000; n2_d[46][2] = 0.000000;
            n3_d[46][0] = -1.414227; n3_d[46][1] = -1.414227; n3_d[46][2] = -0.000000;

            n1_d[47][0] = -1.000000; n1_d[47][1] = 0.000000; n1_d[47][2] = -1.000000;
            n2_d[47][0] = 1.414227; n2_d[47][1] = 1.414227; n2_d[47][2] = 0.000000;
            n3_d[47][0] = 0.000000; n3_d[47][1] = -1.731902; n3_d[47][2] = 0.000000;
        }//esle fi (i == 7)
    }//end for(i=0)
}
//-----------------------------------------------------------------------------------------------------------------------------------
symmetry_planes & symmetry_planes::operator=(const symmetry_planes &_rhs)
{
    int i=0, j=0;
    //-------------Initialization end---------------------------------------------------------------------------------//
    const symmetry_planes *src = dynamic_cast<const symmetry_planes*>(&_rhs);
    _sigmac100 = src->_sigmac100; _sigmac110 = src->_sigmac110; _sigmac111 = src->_sigmac111;
    _Gc100 = src->_Gc100; _Gc110 = src->_Gc110; _Gc111 = src->_Gc111;

    for(i=0; i<48; i++)
    {
        for(j=0; j<3; j++)
        {
            family_1[i][j] = src->family_1[i][j]; family_2[i][j] = src->family_2[i][j];
            family_3[i][j] = src->family_3[i][j];

            fracture_strength_array[i][j] = src->fracture_strength_array[i][j];

            Gc_array[i][j] = src->Gc_array[i][j];

            n1_d[i][j] = src->n1_d[i][j]; n2_d[i][j] = src->n2_d[i][j]; n3_d[i][j] = src->n3_d[i][j];
        }
        is_n2_n3_swapped[i] = src->is_n2_n3_swapped[i];
    }
    return *this;
}
//------------------------------------------------------------------------------------------------------------------------------------
symmetry_planes::symmetry_planes(const symmetry_planes &src)
{
    int i=0, j=0;
    //----------------Initialization end-------------------------------------------------------------//
    _sigmac100 = src._sigmac100; _sigmac110 = src._sigmac110; _sigmac111 = src._sigmac111;

    for(i=0; i<48; i++)
    {
        for(j=0; j<3; j++)
        {
            family_1[i][j] = src.family_1[i][j]; family_2[i][j] = src.family_2[i][j];
            family_3[i][j] = src.family_3[i][j];
            fracture_strength_array[i][j] = src.fracture_strength_array[i][j];
            n1_d[i][j] = src.n1_d[i][j]; n2_d[i][j] = src.n2_d[i][j]; n3_d[i][j] = src.n3_d[i][j];
        }
    }
    for(i=0; i<48; i++)
        is_n2_n3_swapped[i] = src.is_n2_n3_swapped[i];
}
//------------------------------------------------------------------------------------------------------------------------------------
void symmetry_planes::get_dual_vector_set(int &i, double *&n1d, double *&n2d, double *&n3d) const
{
    int j = 0;
//------------Initialization end---------------------------------------------------------------------------------//
    for(j=0; j<3;j++)
    {
        n1d[j] = n1_d[i][j]; n2d[j] = n2_d[i][j]; n3d[j] = n3_d[i][j];
    }
}
//------------------------------------------------------------------------------------------------------------------------------------
void symmetry_planes::get_fracture_strengths(int &i, double &tmp1, double &tmp2, double &tmp3) const
{
    tmp1 = fracture_strength_array[i][0]; tmp2 = fracture_strength_array[i][1];
    tmp3 = fracture_strength_array[i][2];
}
//------------------------------------------------------------------------------------------------------------------------------------
void symmetry_planes::get_Gc_values(int &i, double &tmp1, double &tmp2, double &tmp3) const
{
    tmp1 = Gc_array[i][0]; tmp2 = Gc_array[i][1]; tmp3 = Gc_array[i][2];
}
//------------------------------------------------------------------------------------------------------------------------------------
void symmetry_planes::get_basis_vector_set(int &i, double *&n1_tmp, double *&n2_tmp, double *&n3_tmp) const
{
    int j = 0;
    for(j=0;j<3;j++)
    {
        n1_tmp[j] = family_1[i][j];
        if(is_n2_n3_swapped[i] == 0)
        {
            n2_tmp[j] = family_2[i][j]; n3_tmp[j] = family_3[i][j];
        }
        else if(is_n2_n3_swapped[i] == 1)
        {
            n2_tmp[j] = family_3[i][j]; n3_tmp[j] = family_2[i][j];
        }
    }//end for(j=0)
}






