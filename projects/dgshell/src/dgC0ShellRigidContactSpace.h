//
// C++ Interface: terms
//
// Description: Functional space for rigid contact in case of dgshell has to be here due to dof creation
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef _DGC0SHELLRIGIDCONTACTSPACE_H_
#define _DGC0SHELLRIGIDCONTACTSPACE_H_
#include "contactFunctionSpace.h"
#include "Dof3IntType.h"
class dgC0ShellRigidContactSpace : public rigidContactSpaceBase{
 public:
  dgC0ShellRigidContactSpace(const int id, FunctionSpace<double> *sp, MVertex *ver) : rigidContactSpaceBase(id,sp,ver){}
  dgC0ShellRigidContactSpace(const int id, FunctionSpace<double> *sp, MVertex *ver, const int comp1,
                   const int comp2 = -1, const int comp3 =-1) : rigidContactSpaceBase(id,sp,ver,comp1,comp2,comp3){}
  virtual void getKeys(MElement *ele,std::vector<Dof> &keys){
    if(ele->getDim() != 0){ // if dim 0 return the key of gravity center only !!
      _space->getKeys(ele,keys);
    }
    for(int i=0;i<_comp.size();i++){
      keys.push_back(Dof(_gc->getNum(),Dof3IntType::createTypeWithThreeInts(_comp[i],_id)));
    }
  }
  virtual int getNumKeys(MElement *ele){
    int nkeysele = _space->getNumKeys(ele);
    return nkeysele + _comp.size();
  }
  virtual void getKeysOfGravityCenter(std::vector<Dof> &keys){
    for(int i=0;i<_comp.size(); i++)
      keys.push_back(Dof(_gc->getNum(),Dof3IntType::createTypeWithThreeInts(_comp[i],_id)));
  }
  virtual int getNumKeysOfGravityCenter(){
    return _comp.size();
  }
  virtual void getKeys(MElement *ele, const int ind, std::vector<Dof> &keys){
    // generate keys of element and select the good ones after LAGRANGE OK ?? CHANGE THIS HOW TODO
    std::vector<Dof> tkeys;
    this->getKeys(ele,tkeys);
    int nkeys = this->getNumKeys(ele);
    int ncomp = _comp.size();
    int nbver = nkeys/ncomp-1; // because GC is in the nkeys
    for(int i=0;i<ncomp; i++)
      keys.push_back(tkeys[ind+i*nbver]);
    this->getKeysOfGravityCenter(keys);
  }
  virtual int getNumKeys(MElement *ele, int ind){
    return 2*_comp.size();
  }
};

#endif // _DGC0SHELLRIGIDCONTACTSPACE_H_
