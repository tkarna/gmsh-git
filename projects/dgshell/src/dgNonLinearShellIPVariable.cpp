//
//
// Description: Class of ipvarible for non linear dg shell
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "dgNonLinearShellIPVariable.h"
#include "ipstate.h"
double SimpsonIntegration(const std::vector<double> &y, double h);

static void multSTensor3SecondTranspose(const STensor3 &a, const STensor3 &b, STensor3 &c)
{
  for(int i=0;i<3;i++)
    for(int j=0;j<3;j++)
      c(i,j) = a(i,0)*b(j,0)+a(i,1)*b(j,1)+a(i,2)*b(j,2);
}

static void alphaTensprodAdd(const double alpha,const SVector3 &a,const SVector3 &b,STensor3 &sigma)
{
  if(alpha != 0.){
    for(int i=0;i<3;i++){
      for(int j=0;j<3;j++){
        sigma(i,j) += (alpha*a[i]*b[j]);
      }
    }
  }
}

IPVariableNonLinearShell::IPVariableNonLinearShell(const double h0,const short int nsimp,
                                                   const bool oninter) : IPVariableShell(),
                                                             _nsimp(nsimp), _oninter(oninter), _lb(nsimp), _lbs(NULL),
                                                             _integlambdah(1.)
{
  /* size vector */
  _lambdah.resize(nsimp);
  _zsimp.resize(nsimp);

  /* Ordinate of simpson's points */
  double hsimp = double(h0)/double(nsimp-1); // numsimp is always odd by build of materialLaw
  double halfh = 0.5*h0;
  for(int i=0;i<nsimp;i++){
    _zsimp[i] = -halfh + i*hsimp;
    // initialization of lambda
    _lambdah[i] = 1.;
  }
  // if on interface define a localBasis of Interface
  if(_oninter)
    _lbs = new nonLinearShellLocalBasisInter();
}

IPVariableNonLinearShell::IPVariableNonLinearShell(const IPVariableNonLinearShell & source) : IPVariableShell(source),
                                                                                              _nsimp(source._nsimp),
                                                                                              _oninter(source._oninter),
                                                                                              _lb(source._lb),
                                                                                              _integlambdah(source._integlambdah)
{
  /* size vector */
  _lambdah.resize(_nsimp);
  _zsimp.resize(_nsimp);
  for(int i=0;i<_nsimp;i++){
    _lambdah[i] = source._lambdah[i];
    _zsimp[i] = source._zsimp[i];
  }

  /* interface localbasis copy */
  if(source._lbs == NULL){
    if(_lbs!=NULL) delete _lbs;
  }
  else{
    if(_lbs==NULL){
      _lbs = new nonLinearShellLocalBasisInter(*source._lbs);
    }
    else{
      (*_lbs) = (*source._lbs);
    }
  }
}

IPVariableNonLinearShell& IPVariableNonLinearShell::operator=(const IPVariable &source)
{
  IPVariableShell::operator=(source);
  const IPVariableNonLinearShell*src = static_cast<const IPVariableNonLinearShell*>(&source);
  _integlambdah = src->_integlambdah;
  _nsimp = src->_nsimp;
  _oninter = src->_oninter;
  _lb = src->_lb;
  _lambdah.resize(_nsimp);
  _zsimp.resize(_nsimp);
  for(int i=0;i<_nsimp;i++){
    _lambdah[i] = src->_lambdah[i];
    _zsimp[i] = src->_zsimp[i];
  }
  // interface localbasis copy
  if(src->_lbs == NULL){
    if(_lbs!=NULL) delete _lbs;
  }
  else{
    if(_lbs==NULL){
      _lbs = new nonLinearShellLocalBasisInter(*src->_lbs);
    }
    else{
      (*_lbs) = (*src->_lbs);
    }
  }
  return *this;
}

void IPVariableNonLinearShell::setGlobalStretch()
{
  double thick = _zsimp.back()-_zsimp.front();
 _integlambdah = 1./thick*SimpsonIntegration(_lambdah,thick);
}


shellLocalBasis* IPVariableNonLinearShell::getshellLocalBasisOfInterface()
{
#ifdef _DEBUG
  if(_lbs ==NULL){
    Msg::Warning("Try to get undefined interface Local basis create one");
    _lbs = new nonLinearShellLocalBasisInter();
  }
#endif
  return _lbs;
}
const shellLocalBasis* IPVariableNonLinearShell::getshellLocalBasisOfInterface() const
{
#ifdef _DEBUG
  if(_lbs ==NULL){
    Msg::Error("Try to get a const undefined interface Local basis create one");
  }
#endif
  return _lbs;
}
void IPVariableNonLinearShell::setshellLocalBasisOfInterface(const shellLocalBasis *lb)
{
#ifdef DEBUG_
  if(_lbs ==NULL){
    Msg::Warning("Try to set undefined interface Local basis create one");
    _lbs = new nonLinearShellLocalBasisInter(*lb);
  }
  else
   (*_lbs)=(*lb);
#else
  (*_lbs)=(*lb);
#endif
}

neoHookeanShellIPv::neoHookeanShellIPv(const double h0,const short int nsimp,
                                       const bool oninter) : IPVariableNonLinearShell(h0,nsimp,oninter), _strainEnergy(0.)
{
  // resize vector
  _stress.resize(nsimp);
  _strain.resize(nsimp);
}
 neoHookeanShellIPv::neoHookeanShellIPv(const neoHookeanShellIPv &source) : IPVariableNonLinearShell(source),
                                                                             _strainEnergy(source._strainEnergy)
{
  _stress.resize(_nsimp);
  _strain.resize(_nsimp);
  for(int i=0;i<_nsimp;i++){
    _stress[i] = source._stress[i];
    _strain[i] = source._strain[i];
  }
}
neoHookeanShellIPv& neoHookeanShellIPv::operator = (const IPVariable &source)
{
  IPVariableNonLinearShell::operator=(source);
  const neoHookeanShellIPv *src = static_cast<const neoHookeanShellIPv*>(&source);
  _strainEnergy = src->_strainEnergy;
  _stress.resize(_nsimp);
  _strain.resize(_nsimp);
  for(int i=0;i<_nsimp;i++){
    _stress[i] = src->_stress[i];
    _strain[i] = src->_strain[i];
  }
  return *this;
}

void neoHookeanShellIPv::setB(const std::vector<TensorialTraits<double>::GradType> &Grads,
                               const std::vector<TensorialTraits<double>::HessType> &Hess)
{
  Msg::Error("Define BVector in nonLinearShellIPv");
}

void neoHookeanShellIPv::strain(const fullVector<double> &disp,const int npoint,const shellLocalBasisBulk* lbinit)
{
  //Msg::Error("Define strain computation in nonLinearShellIPv");
  // skip nothing to do --> implement this
}

void neoHookeanShellIPv::getUpperStressTensorBrokenEval(stressTensor &sigma_) const
{
  double invJ = _lb.getInitialJacobian(_nsimp-1)/_lb.getJacobian(_nsimp-1);
  sigma_(0,0) = invJ*_stress[_nsimp-1][0];
  sigma_(1,1) = invJ*_stress[_nsimp-1][1];
  sigma_(2,2) = invJ*_stress[_nsimp-1][2];
  sigma_(0,1) = sigma_(1,0) = invJ*_stress[_nsimp-1][3];
  sigma_(1,2) = sigma_(2,1) = invJ*_stress[_nsimp-1][4];
  sigma_(0,2) = sigma_(2,0) = invJ*_stress[_nsimp-1][5];
}

void neoHookeanShellIPv::getLowerStressTensorBrokenEval(stressTensor &sigma_) const
{
  double invJ = _lb.getInitialJacobian(0)/_lb.getJacobian(0);
  sigma_(0,0) = invJ*_stress[0][0];
  sigma_(1,1) = invJ*_stress[0][1];
  sigma_(2,2) = invJ*_stress[0][2];
  sigma_(0,1) = sigma_(1,0) = invJ*_stress[0][3];
  sigma_(1,2) = sigma_(2,1) = invJ*_stress[0][4];
  sigma_(0,2) = sigma_(2,0) = invJ*_stress[0][5];
}

double neoHookeanShellIPv::get(const int i) const
{
  // if i < 6*nbsimp --> stress
  if(i<6*_nsimp){
    // find the gauss point on the thickness
    int pos = i/6; // pos on thickness
    int cmp = i%6;
    if(cmp < 0){ // VonMises
      return this->vonMises(cmp);
    }
    else{
      if(pos >= _nsimp){
        Msg::Error("Try to archive stress on a inexisting Simpson's point");
        return 0;
      }
      double sig;
      double invJ = _lb.getInitialJacobian(pos) / _lb.getJacobian(pos);
      // compute of stress for representation
      switch(cmp){
        case component::xx :
          sig = invJ*_stress[pos][0];
          break;
        case component::yy :
          sig = invJ*_stress[pos][1];
          break;
        case component::zz :
          sig = invJ*_stress[pos][2];
          break;
        case component::xy :
          sig = invJ*_stress[pos][3];
          break;
        case component::yz :
          sig = invJ*_stress[pos][4];
          break;
        case component::xz :
          sig = invJ*_stress[pos][5];
          break;
        default :
          sig=0.;
      }
      return sig;
    }
  }
  else if(i<12*_nsimp){
    Msg::Warning("strain archive is not implemented for neoHookeanShellIPv");
    return 0.;
  }
}

double neoHookeanShellIPv::vonMises(const int pos) const
{
  // pos in negative by construction take it with positive sign
  int ind = -(pos+1); // by chosse of construction
  // Stress in shell basis
  double invJ = _lb.getInitialJacobian(ind)/_lb.getJacobian(ind);
  // compute of stress for representation
  double sx = invJ*_stress[ind][0];
  double sy = invJ*_stress[ind][1];
  double sz = invJ*_stress[ind][2];
  double txy = invJ*_stress[ind][3];
  double tyz = invJ*_stress[ind][4];
  double txz = invJ*_stress[ind][5];
  return sqrt(1./2.*((sx-sy)*(sx-sy)+(sy-sz)*(sy-sz)+(sx-sz)*(sx-sz)+6*(txy*txy+tyz*tyz+txz*txz)));
}


J2linearShellIPv::J2linearShellIPv(const double h0,const short int nsimp,
                                   const mlawJ2linear &_j2law,
                                   const bool oninter) : IPVariableNonLinearShell(h0,nsimp,oninter),
                                                         _F(nsimp), _tau(nsimp),
                                                         _j2ipv(nsimp,IPJ2linear(_j2law.getJ2IsotropicHardening())){}
J2linearShellIPv::J2linearShellIPv(const J2linearShellIPv &source) : IPVariableNonLinearShell(source)
{
  for(int i=0;i<_nsimp;i++){
    _j2ipv[i].operator = (source._j2ipv[i]);
    _F[i] = source._F[i];
    _tau[i] = source._tau[i];
  }
}
J2linearShellIPv& J2linearShellIPv::operator=(const IPVariable &source)
{
  IPVariableNonLinearShell::operator=(source);
  const J2linearShellIPv* src = static_cast<const J2linearShellIPv*>(&source);
  for(int i=0;i<_nsimp;i++){
    _j2ipv[i].operator = (src->_j2ipv[i]);
    _F[i] = src->_F[i];
    _tau[i] = src->_tau[i];
  }
  return *this;
}

void J2linearShellIPv::setB(const std::vector<TensorialTraits<double>::GradType> &Grads,
                            const std::vector<TensorialTraits<double>::HessType> &Hess)
{
  Msg::Error("Can't save B vector in a J2linearShellIPv");
}

void J2linearShellIPv::strain(const fullVector<double> &disp, const int npoint,const shellLocalBasisBulk* lbinit__)
{
  const nonLinearShellLocalBasisBulk *lbinit = static_cast<const nonLinearShellLocalBasisBulk*>(lbinit__);
  int np,npend;
  if(npoint == -1)
  {
    np=0;
    npend = _nsimp;
  }
  else
  {
    np=npoint;
    npend = npoint+1;
  }
  for(np;np<npend;np++)
  {
      const SVector3& g01 = lbinit->dualBasisVectorG1(np);
      const SVector3& g02 = lbinit->dualBasisVectorG2(np);
      const SVector3& g03 = lbinit->dualBasisVectorG3(np);
/*    // way of article but it doesn't work for ensure plne stress (just g_i that is modified during iteration)
      const SVector3& phi1 = _lb.basisVector(0);
      const SVector3& phi2 = _lb.basisVector(1);
      const SVector3& nor = _lb.basisNormal();
      const SVector3& t1 = _lb.basisDnormal(0);
      const SVector3& t2 = _lb.basisDnormal(1);
      double xi3lh = _zsimp[np]*_integlambdah;

      tensprod(phi1,g01,_F[np]);
      alphaTensprodAdd(1.,phi2,g02,_F[np]);
      alphaTensprodAdd(xi3lh,t1,g01,_F[np]);
      alphaTensprodAdd(xi3lh,t2,g02,_F[np]);
      alphaTensprodAdd(_integlambdah,nor,g03,_F[np]);
*/
    const SVector3 &g_1 = _lb.basisVectorG1(np);
    const SVector3 &g_2 = _lb.basisVectorG2(np);
    const SVector3 &g_3 = _lb.basisVectorG3(np);

    tensprod(g_1,g01,_F[np]);
    alphaTensprodAdd(1.,g_2,g02,_F[np]);
    alphaTensprodAdd(1.,g_3,g03,_F[np]);

  }
}

const Bvector* J2linearShellIPv::getBvector() const
{
  Msg::Error("Can't get B vector in a J2linearShellIPv");
}

Bvector* J2linearShellIPv::getBvector()
{
  Msg::Error("Can't get B vector in a J2linearShellIPv");
}

tab6& J2linearShellIPv::getEpsilon(const int i)
{

  Msg::Error("can't get strain on form of a tab6 because strain are not store like this in a J2linearShellIPv");
  static tab6 tt;
  return tt;
}
const tab6& J2linearShellIPv::getEpsilon(const int i) const
{
  Msg::Error("can't get strain on form of a tab6 because strain are not store like this in a J2linearShellIPv");
  const static tab6 tt;
  return tt;
}

void J2linearShellIPv::setStrainEnergy(const double se)
{
 #if defined _DEBUG
  Msg::Warning("Strain Energy not store in J2linearShellIPv");
 #endif // _DEBUG
}

void J2linearShellIPv::getUpperStressTensorBrokenEval(stressTensor &sigma_)const
{
  double invJ = _lb.getInitialJacobian(_nsimp-1)/_lb.getJacobian(_nsimp-1);
  sigma_(0,0) = invJ*_tau[_nsimp-1][0];
  sigma_(1,1) = invJ*_tau[_nsimp-1][1];
  sigma_(2,2) = invJ*_tau[_nsimp-1][2];
  sigma_(0,1) = sigma_(1,0) = invJ*_tau[_nsimp-1][3];
  sigma_(1,2) = sigma_(2,1) = invJ*_tau[_nsimp-1][4];
  sigma_(0,2) = sigma_(2,0) = invJ*_tau[_nsimp-1][5];
}

void J2linearShellIPv::getLowerStressTensorBrokenEval(stressTensor &sigma_)const
{
  double invJ = _lb.getInitialJacobian(0)/_lb.getJacobian(0);
  sigma_(0,0) = invJ*_tau[0][0];
  sigma_(1,1) = invJ*_tau[0][1];
  sigma_(2,2) = invJ*_tau[0][2];
  sigma_(0,1) = sigma_(1,0) = invJ*_tau[0][3];
  sigma_(1,2) = sigma_(2,1) = invJ*_tau[0][4];
  sigma_(0,2) = sigma_(2,0) = invJ*_tau[0][5];
}

double J2linearShellIPv::vonMises(const int pos) const
{
  // pos in negative by construction take it with positive sign
  int ind = -(pos+1); // by chosse of construction
  // Stress in shell basis
  double invJ = _lb.getInitialJacobian(ind)/_lb.getJacobian(ind);
  // compute of stress for representation
  double sx = invJ*_tau[ind][0];
  double sy = invJ*_tau[ind][1];
  double sz = invJ*_tau[ind][2];
  double txy = invJ*_tau[ind][3];
  double tyz = invJ*_tau[ind][4];
  double txz = invJ*_tau[ind][5];
  return sqrt(1./2.*((sx-sy)*(sx-sy)+(sy-sz)*(sy-sz)+(sx-sz)*(sx-sz)+6*(txy*txy+tyz*tyz+txz*txz)));
}

double J2linearShellIPv::get(const int i) const
{
  if(i<0)
  {
    return this->vonMises(i);
  }
  if(i < 6*_nsimp) // stress component
  {
    int pos = i/6; // pos on thickness
    if(pos >= _nsimp){
      Msg::Debug("Try to archive stress on a inexisting Simpson's point");
      return 0;
    }
    int cmp = i%6;
    double invJ = _lb.getInitialJacobian(pos)/_lb.getJacobian(pos);
    return invJ*_tau[pos][cmp];
  }
  if(i < 12*_nsimp) // strain component
  {
    Msg::Debug("strain archive is not implemented for IPLinearShell");
    return 0.;
  }
  if(i < 13*_nsimp) // equivalent plastic strain
  {
    int pos = i%(12*_nsimp);
    return _j2ipv[pos]._j2lepspbarre;
  }
  Msg::Debug("get J2linearShellIPv is not defined for your component !!");
  return 0.;
}

double J2linearShellIPv::defoEnergy() const
{
  std::vector<double> vener(_j2ipv.size());
  for(int i=0;i<_j2ipv.size();i++)
  {
    vener[i] = _j2ipv[i].defoEnergy();
  }
  return SimpsonIntegration(vener,this->getThickness());
}

double J2linearShellIPv::plasticEnergy() const
{
  std::vector<double> vener(_j2ipv.size());
  for(int i=0;i<_j2ipv.size();i++)
  {
    vener[i] = _j2ipv[i].plasticEnergy();
  }
  return SimpsonIntegration(vener,this->getThickness());
}
