//
// C++ Interface: inter domain between 2 nonLinearDomain HAS TO BE REMOVED AFTER FIX PROBLEM OF INTERDOMAIN MANAGEMENT (computation of ipv)
//                for now +/- copy-paste of interDomainBetweenShell
// Description: Interface class for dg shell
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

// BEAWARE copy paste of interDomainBetweenShell (will be removed)
#include "nonLinearInterDomainShell.h"
#include "dgNonLinearShellTerms.h"
#include "GaussLobattoQuadratureRules.h"
nonLinearInterDomainShell::nonLinearInterDomainShell(const int tag, partDomain *dom1,partDomain *dom2,
                                                 const int lnum) : dgNonLinearShellDomain(tag,manageInterface::getKey(dom1->getPhysical(),dom2->getPhysical()),10000,lnum,true),
                                                                   _mlawMinus(NULL), _mlawPlus(NULL)
{
  // By convention the domain with the small number is the domMinus
  // in MPI if domMinus==domPlus domMinus = domain on the partion with the lowest rank
  if(dom2->getPhysical() > dom1->getPhysical())
  {
    _domMinus = dom1;
    _domPlus = dom2;
  }
  else if(dom1->getPhysical() > dom2->getPhysical())
  {
    _domMinus = dom2;
    _domPlus = dom1;
  }
 #if defined(HAVE_MPI)
  else if(Msg::GetCommSize() > 1 and(dom1->getPhysical()==dom2->getPhysical()))
  {
    _domMinus = dom1;
    _domPlus = dom2; // dom1==dom2 so we don't care about which is domMinus and wich is domPlus
  }
  else if(Msg::GetCommSize() > 1){
    // Check if the two domains are added to solver
    groupOfElements *g1 = dom1->g;
    groupOfElements *g2 = dom2->g;
    if ((g1->size() !=0) and (g2->size() !=0))
    {
      groupOfElements::elementContainer::iterator it1 = g1->begin();
      MElement *ele1 = *it1;
      groupOfElements::elementContainer::iterator it2 = g2->begin();
      MElement *ele2 = *it2;
      if(ele1->getPartition() < ele2->getPartition())
      {
        _domMinus = dom1;
        _domPlus = dom2;
      }
      else if(ele2->getPartition() < ele1->getPartition())
      {
        _domMinus = dom2;
        _domPlus = dom1;
      }

    }
    else
    {
      Msg::Error("Impossible to known the partion of domain --> no inter domain created between %d and %d",dom1->getPhysical(),dom2->getPhysical());
    }
  }
 #endif // HAVE_MPI

  // create empty groupOfElements
  g = new groupOfElements();
  // set functionalSpace
  _spaceMinus = _domMinus->getFunctionSpace();
  _spacePlus = _domPlus->getFunctionSpace();
  if((_domMinus->getFunctionSpaceType() == functionSpaceType::Lagrange) and
     (_domPlus->getFunctionSpaceType() == functionSpaceType::Lagrange)){
    if(_space != NULL) delete _space;
    _space = new DgC0FullDgLagrangeBetween2DomainsFunctionSpace(_spaceMinus,_spacePlus);
    _wsp = functionSpaceType::Lagrange;
  }
  else{
    Msg::Error("Impossible to create functional space on interDomainBetweenShell %d",_phys);
  }
}


nonLinearInterDomainShell::nonLinearInterDomainShell(const nonLinearInterDomainShell &source) : dgNonLinearShellDomain(source),
                                                                                           _domMinus(source._domMinus),
                                                                                           _domPlus(source._domPlus),
                                                                                           _physMinus(source._physMinus),
                                                                                           _physPlus(source._physPlus),
                                                                                           _mlawMinus(source._mlawMinus),
                                                                                           _mlawPlus(source._mlawPlus)
{}

void nonLinearInterDomainShell::initializeTerms(unknownField *uf,IPField*ip,std::vector<nonLinearNeumannBC> &vNeumann)
{
  // void term (as they have to exist)
  this->ltermBulk = new nonLinearTermVoid();
  this->btermBulk = new BiNonLinearTermVoid();
  this->btermVirtBound = new BiNonLinearTermVoid();
  this->ltermVirtBound = new nonLinearTermVoid();
  this->massterm = new BiNonLinearTermVoid();

  FunctionSpace<double>* dgspace = static_cast<FunctionSpace<double>*>(_space);
  FunctionSpace<double>* dgspace1 = static_cast<FunctionSpace<double>*>(_spaceMinus);
  FunctionSpace<double>* dgspace2 = static_cast<FunctionSpace<double>*>(_spacePlus);
  dgNonLinearShellDomain* dgldomMinus = static_cast<dgNonLinearShellDomain*>(_domMinus);
  dgNonLinearShellDomain* dgldomPlus = static_cast<dgNonLinearShellDomain*>(_domPlus);
  ltermBound = new dgNonLinearShellForceInter(*dgspace1,dgspace2,_mlawMinus,_mlawPlus,_interQuad,_beta1,
                                                 _beta2,_beta3,true,ip);
  if(_interByPert)
    btermBound = new BilinearTermPerturbation<double>(ltermBound,*dgspace1,*dgspace2,uf,ip,this,_eps);
  else
    btermBound = new BiNonLinearTermVoid();


  // Normally no neumannBC on interdomain --> no need to search for neumannBC here

  /* same strength fracture value on both side (to avoid discontinuities) */
  /* no very elegant to put here but very efficient */
  // take the value of minus element by choice
  if(_mlawMinus->getType() == materialLaw::fracture and _mlawPlus->getType() == materialLaw::fracture)
  {
    _sameValueInterfaceGaussPoints = dgldomMinus->sameValueGaussPoint(); // if minus OK plus OK ??
    this->ensureSameValuesBothSideIPvariable(ip);
  }
}

void nonLinearInterDomainShell::setGaussIntegrationRule()
{
  groupOfElements::elementContainer::const_iterator it = gi->begin();
  MElement *e = *it;
  if(_gaussorderbound == -1) {
    _gqt == Gauss ? integBound = new GaussQuadrature(5) : integBound = new GaussLobatto1DQuadrature(7);
    _gqt == Gauss ? _interQuad = new interface1DQuadrature(5) : _interQuad = new interface1DQuadrature(7,true);
  }
  else{
    _gqt == Gauss ? integBound = new GaussQuadrature(_gaussorderbound) : integBound = new GaussLobatto1DQuadrature(_gaussorderbound);
    _gqt == Gauss ? _interQuad = new interface1DQuadrature(_gaussorderbound) : _interQuad = new interface1DQuadrature(_gaussorderbound,true);
  }
  integBulk = NULL; // no element

}

void nonLinearInterDomainShell::computeIPVariable(AllIPState *aips,const unknownField *ufield,
                                                const IPStateBase::whichState ws, bool stiff)
{
  IntPt *GP;
  fullVector<double> dispm;
  fullVector<double> dispp;
  std::vector<Dof> R;
  shellMaterialLaw *mlawMinus = static_cast<shellMaterialLaw*>(_mlawMinus);
  shellMaterialLaw *mlawPlus = static_cast<shellMaterialLaw*>(_mlawPlus);
  for(groupOfElements::elementContainer::iterator it=gi->begin(); it!=gi->end(); ++it){
    MInterfaceLine *iele = dynamic_cast<MInterfaceLine*>(*it);
    int npts_inter= integBound->getIntPoints(iele,&GP);
    R.clear();
    _spaceMinus->getKeys(iele->getElem(0),R);
    dispm.resize(R.size());
    ufield->get(R,dispm);
    R.clear();
    _spacePlus->getKeys(iele->getElem(1),R);
    dispp.resize(R.size());
    ufield->get(R,dispp);
    this->computeIpv(aips,iele,GP,ws,_domMinus,_domPlus,mlawMinus,mlawPlus,dispm,dispp,false, stiff);
    // set initial value change this
    if(ws == IPStateBase::initial)
    {
       // get ipvariable
       AllIPState::ipstateElementContainer *vips = aips->getIPstate(iele->getNum());
       for(int j=0; j<vips->size(); j++)
       {
         // set initial Jacobian value in other ipv (change this HOW ??)
         IPStateBase *ips = (*vips)[j];
         IPVariableShell* ipvprev = static_cast<IPVariableShell*>(ips->getState(IPStateBase::previous));
         IPVariableShell* ipvcur = static_cast<IPVariableShell*>(ips->getState(IPStateBase::current));
         IPVariableShell* ipvinit = static_cast<IPVariableShell*>(ips->getState(IPStateBase::initial));
         // local basis
         nonLinearShellLocalBasisBulk *lbinit = static_cast<nonLinearShellLocalBasisBulk*>(ipvinit->getshellLocalBasis());
         nonLinearShellLocalBasisBulk *lbprev = static_cast<nonLinearShellLocalBasisBulk*>(ipvprev->getshellLocalBasis());
         nonLinearShellLocalBasisBulk *lbcur = static_cast<nonLinearShellLocalBasisBulk*>(ipvcur->getshellLocalBasis());
         // set value in previous and current
         lbinit->setInitialJacobian(lbinit->getJacobianOfSimpsonPoints()); // change this HOW (beaware of copy ipv which copy initial->previous and remove the stored value)??
         lbcur->setInitialJacobian(lbinit->getJacobianOfSimpsonPoints());
         lbprev->setInitialJacobian(lbinit->getJacobianOfSimpsonPoints());
         ipvinit->strain(dispm,-1,lbinit);
       }
    }
  }
}

void nonLinearInterDomainShell::setMaterialLaw(const std::map<int,materialLaw*> maplaw)
{
  // oterwise the law is already set
  if(!setmaterial){
    _domMinus->setMaterialLaw(maplaw);
    _domPlus->setMaterialLaw(maplaw);
    _mlaw = NULL;
    if(_lnum == 0){ // no fracture law prescribed --> materialLawMinus and Plus are given via domain
      _mlawMinus = _domMinus->getMaterialLaw();
      _mlawPlus = _domPlus->getMaterialLaw();
      if(_mlawMinus == _mlawPlus)
      {
        _mlaw = _mlawMinus; // same law on both
      }
      else
      {
        Msg::Error("Cannot set the interface law on nonLinearInterDomainShell");
      }
    }
    else{
      // create law (use lnum with negative number same number for the two but it doesn't matter)
      // these law don't appear in map with all laws
      // check if minus and plus laws are already fracture by 2 laws;
      int numlawBulkMinus, numlawBulkPlus;
      if(_domMinus->getMaterialLaw()->getType() != materialLaw::fracture){
        numlawBulkMinus = _domMinus->getLawNum();
      }
      else{
        materialLaw2LawsInitializer *m2law = dynamic_cast<materialLaw2LawsInitializer*>(_domMinus->getMaterialLaw());
        numlawBulkMinus = m2law->bulkLawNumber();
      }
      if(_domPlus->getMaterialLaw()->getType() != materialLaw::fracture){
        numlawBulkPlus = _domPlus->getLawNum();
      }
      else{
        materialLaw2LawsInitializer *m2law = dynamic_cast<materialLaw2LawsInitializer*>(_domPlus->getMaterialLaw());
        numlawBulkPlus = m2law->bulkLawNumber();
      }
      _mlawMinus = new shellFractureByCohesiveLaw(-numlawBulkMinus,numlawBulkMinus,_lnum);
      _mlawPlus = new shellFractureByCohesiveLaw(-numlawBulkPlus,numlawBulkPlus,_lnum);
      if(numlawBulkMinus == numlawBulkPlus)
      {
        _mlaw = _mlawMinus;
      }
      else
      {
        Msg::Warning("Cannot set the interface law on interDomainBetweenShell but normally it is unuse");
      }
    }
    _mlawMinus->initLaws(maplaw);
    _mlawPlus->initLaws(maplaw);
    setmaterial=true;
  }
}
