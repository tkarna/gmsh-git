//
//
// Description: Class of ipvariable for non linear dg shell
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef _DGNONLINEARSHELLIPVARIABLE_H_
#define _DGNONLINEARSHELLIPVARIABLE_H_
#include "shellIPVariable.h"
#include "ipJ2linear.h"
#include "mlawJ2linear.h"
#include "ipField.h"
/* some data that are normally common to all IPVariable for non linear shell */
/* note that your IPvariable has not necessery derive from this one as long as it derives from IPvariableShell */
class IPVariableNonLinearShell : public IPVariableShell
{
 protected:
  std::vector<double> _lambdah;
  double _integlambdah;
  std::vector<double> _zsimp;
  short int _nsimp;
  bool _oninter;
  nonLinearShellLocalBasisBulk _lb;
  nonLinearShellLocalBasisInter *_lbs;
 public:
  IPVariableNonLinearShell(const double h0,const short int nsimp,const bool oninter=false);
  IPVariableNonLinearShell(const IPVariableNonLinearShell &source);
  IPVariableNonLinearShell &operator=(const IPVariable &source);
  /* pure virtual functions */
  virtual void setB(const std::vector<TensorialTraits<double>::GradType> &Grads,
                    const std::vector<TensorialTraits<double>::HessType> &Hess)=0;
  virtual void strain(const fullVector<double> &disp,const int npoint=-1,const shellLocalBasisBulk* lbinit=NULL)=0;
  virtual const Bvector* getBvector() const=0;
  virtual Bvector* getBvector()=0;
  virtual double getSigma(const int i,const int j) const=0;
  virtual double getEpsilon(const int i,const int j)const=0;
  virtual tab6& getSigma(const int i)=0;
  virtual tab6& getEpsilon(const int i)=0;
  virtual const tab6& getEpsilon(const int i) const=0;
  virtual void setStrainEnergy(const double se)=0;
  virtual void getUpperStressTensorBrokenEval(stressTensor &sigma_)const=0;
  virtual void getLowerStressTensorBrokenEval(stressTensor &sigma_)const=0;
  virtual double vonMises(const int pos) const=0;
  virtual double get(const int i) const=0;
  virtual double defoEnergy() const=0;
  virtual double plasticEnergy() const=0;
  /* Common function */
  virtual void setGlobalStretch(); // has to be called at the end of ensurePlaneStress
  virtual double getThickness() const{return (_zsimp.back()-_zsimp.front());}
  virtual const shellLocalBasis * getshellLocalBasis() const{return &_lb;}
  virtual shellLocalBasis * getshellLocalBasis(){return &_lb;}
  virtual shellLocalBasis* getshellLocalBasisOfInterface();
  virtual const shellLocalBasis* getshellLocalBasisOfInterface() const;
  virtual void setshellLocalBasisOfInterface(const shellLocalBasis *lb);
  virtual const bool isInterface() const{return _oninter;}
  virtual const std::vector<double>& getSimpsonPoint() const{return _zsimp;}
  virtual const double getSimpsonPoint(const int i) const{return _zsimp[i];}
  virtual short int getNumSimp() const{return _nsimp;}
  virtual double getGlobalStretch() const{return _integlambdah;}
  virtual double getThicknessStretch(const int i) const{return _lambdah[i];}
  virtual const std::vector<double>* getThicknessStretch() const{return &_lambdah;}
  virtual void setThicknessStretch(const int np,const double l){_lambdah[np]=l;}
};

class neoHookeanShellIPv : public IPVariableNonLinearShell
{
 protected:
  std::vector<tab6> _stress;
  std::vector<tab6> _strain;
  double _strainEnergy; // depends on material parameters has to be computed in the material law and set after
 public:
  neoHookeanShellIPv(const double h0,const short int nsimp,const bool oninter=false);
  neoHookeanShellIPv(const neoHookeanShellIPv &source);
  neoHookeanShellIPv &operator=(const IPVariable &source);
  virtual ~neoHookeanShellIPv(){if(_lbs !=NULL) delete _lbs;}
  virtual void setB(const std::vector<TensorialTraits<double>::GradType> &Grads,
                    const std::vector<TensorialTraits<double>::HessType> &Hess);
  virtual void strain(const fullVector<double> &disp,const int npoint=-1,const shellLocalBasisBulk* lbinit=NULL);
  virtual const Bvector* getBvector() const{Msg::Error("Define BVector and return it in neoHookeanShellIPV");}
  virtual Bvector* getBvector(){Msg::Error("Define BVector and return it in neoHookeanShellIPV");}
  virtual double getSigma(const int i,const int j) const{return _stress[i][j];}
  virtual double getEpsilon(const int i,const int j)const{return _strain[i][j];}
  virtual tab6& getSigma(const int i){return _stress[i];}
  virtual tab6& getEpsilon(const int i){return _strain[i];}
  virtual const tab6& getEpsilon(const int i) const {return _strain[i];}
  virtual const tab6& getSigma(const int i) const {return _stress[i];}
  virtual void setStrainEnergy(const double se){_strainEnergy = se;}
  virtual void getUpperStressTensorBrokenEval(stressTensor &sigma_)const;
  virtual void getLowerStressTensorBrokenEval(stressTensor &sigma_)const;
  // Archiving data
  virtual double vonMises(const int pos) const;
  virtual double get(const int i) const;
  virtual double defoEnergy() const{return _strainEnergy;}
  virtual double plasticEnergy() const{return 0.;}
};

class J2linearShellIPv : public IPVariableNonLinearShell // or store data in a diffrent way
{
 protected:
  std::vector<IPJ2linear> _j2ipv;
  // deformation and stress Tensor
  std::vector<STensor3> _F;  // we store the two-point deformation gradient in XYZ basis !!
  std::vector<tab6> _tau;    // BE AWARE FOR STRESS tau are store and not the first PK tensor !!
 public:
  J2linearShellIPv(const double h0,const short int nsimp,const mlawJ2linear &_j2law,
                   const bool oninter=false);
  J2linearShellIPv(const J2linearShellIPv &source);
  J2linearShellIPv& operator=(const IPVariable &source);
  virtual void setB(const std::vector<TensorialTraits<double>::GradType> &Grads,
                    const std::vector<TensorialTraits<double>::HessType> &Hess);
  virtual void strain(const fullVector<double> &disp,const int npoint=-1,const shellLocalBasisBulk* lbinit=NULL);
  virtual const Bvector* getBvector() const;
  virtual Bvector* getBvector();
  virtual double getSigma(const int npoint,const int j) const{return this-> _tau[npoint][j];}
  virtual double getEpsilon(const int i,const int j)const{return _F[i][j];}
  virtual tab6& getSigma(const int npoint){return _tau[npoint];}
  virtual const tab6& getSigma(const int npoint) const {return _tau[npoint];}
  virtual tab6& getEpsilon(const int i);
  virtual const tab6& getEpsilon(const int i) const;
  virtual void setStrainEnergy(const double se);
  virtual void getUpperStressTensorBrokenEval(stressTensor &sigma_)const;
  virtual void getLowerStressTensorBrokenEval(stressTensor &sigma_)const;
  virtual double vonMises(const int pos) const;
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  /* specific function */
  IPJ2linear* getIPJ2linear(const int i){return &(_j2ipv[i]);}
  const IPJ2linear* getIPJ2linear(const int i) const{return &(_j2ipv[i]);}
  STensor3& deformationGradient(const int i){return _F[i];}
  const STensor3& deformationGradient(const int i) const {return _F[i];}
};

#endif // _DGNONLINEARSHELLIPVARIABLE
