//
//
// Description: Class of materials for non linear dg shell
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef _DGNONLINEARSHELLMATERIAL_H_
#define _DGNONLINEARSHELLMATERIAL_H_
#ifndef SWIG
#include "shellMaterialLaw.h"
#include "mlawJ2linear.h"
#include "j2IsotropicHardening.h"
#endif
// non linear shell material law must have specific function. (eg ensure plane stress by newton-raphson iteration)
class dgNonLinearShellMaterialLaw : public shellMaterialLaw{
 protected:
  int _itemax; // maximal number of iteration for ensure t33=0 set 1000 by default
  double _itetol; // tolerance of iteration (0.001 by default)
  mutable double _C33ij[3][3]; // cache data
  mutable double _dg_ij[3][3]; // cache data
 public:
  void iterationData(const int ite,const double tol){_itemax = ite;_itetol=tol;}
#ifndef SWIG
  dgNonLinearShellMaterialLaw(const int num,const double thick,
                              const int nsimp,
                              const double rho,
                              const bool init_) : shellMaterialLaw(num,thick,nsimp,rho,init_),
                                                   _itemax(1000), _itetol(0.001){}
  dgNonLinearShellMaterialLaw(const dgNonLinearShellMaterialLaw &source): shellMaterialLaw(source), _itemax(source._itemax){}
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt=0) const=0;
  virtual void createIPVariable(IPVariable* &ipv,const MElement *ele=NULL, const int nbFF_=0) const=0;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw)=0;
  virtual double soundSpeed() const=0;
  virtual void stress(IPStateBase*,bool stiff, const bool checkfrac=true)=0;
  virtual void shellBendingStiffness(const IPVariableShell *ipv, LinearElasticShellHookeTensor &Hbend) const=0;
  virtual void shellMembraneStiffness(const IPVariableShell *ipv, LinearElasticShellHookeTensor &Htens) const=0;
  virtual void shellShearingStiffness(const IPVariableShell *ipv, linearElasticShellShearingHookeTensor &Htens) const=0;
  // default implementation. can be overloaded if the data is store in a different way
  virtual void reduction(const IPStateBase *ips,std::vector<SVector3> &n_,
                         std::vector<SVector3> &mtilde_,bool fdg=true) const;
  virtual void materialTensor33(const IPStateBase *ips, const int npoint,double C33ij[3][3]) const=0; // npoint = numero of Simpson's point
  virtual void stresst33(const IPStateBase *ips,const int npoint, double &t33, double &t33rel) const=0; //t33rel is the relative value divided by a material constant used to check convergence
  // same for all material Law which must have the 2 previous function materialTensor33 stresst33
  virtual void ensurePlaneStress(IPStateBase*ips,const bool nonEvalStiffness);
  virtual void dg_ijDstretch(const IPStateBase *ips,const int npoint,double dgij[3][3]) const; // fill dg_ij that can be used then in ensurePlaneStress
#endif // SWIG
};

class neoHookeanShellLaw : public dgNonLinearShellMaterialLaw
{
 protected:
  const double _K0,_G0,_C11,_nu,_K0minus2G0div3;
 private:
  mutable hookeTensor _H; // cache data
 public:
  neoHookeanShellLaw(const int num, const double E,
                     const double nu, const double thick,const int nsimp,const double rho=0.);
  neoHookeanShellLaw(const neoHookeanShellLaw &source);
  ~neoHookeanShellLaw(){}
#ifndef SWIG
  virtual matname getType() const{return materialLaw::nonLinearShell;}
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const;
  // To allow initialization of bulk ip in case of fracture
  virtual void createIPVariable(IPVariable* &ipv,const MElement *ele=NULL, const int nbFF_=0) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){} // nothing to do for this law
  virtual double soundSpeed() const;
  virtual void stress(IPStateBase*,bool stiff, const bool checkfrac=true);
  virtual void shellBendingStiffness(const IPVariableShell *ipv, LinearElasticShellHookeTensor &Hbend) const;
  virtual void shellMembraneStiffness(const IPVariableShell *ipv, LinearElasticShellHookeTensor &Htens) const;
  virtual void shellShearingStiffness(const IPVariableShell *ipv, linearElasticShellShearingHookeTensor &Htens) const;
  virtual void materialTensor33(const IPStateBase *ips, const int npoint,double C33ij[3][3]) const;
  virtual void stresst33(const IPStateBase *ips,const int npoint,double &t33, double &t33rel) const;
#endif // SWIG
};

class J2linearShellLaw : public dgNonLinearShellMaterialLaw
{
 protected:
  mlawJ2linear _j2law;
  double _tolt33factor;
  double _C11;
 private: // cache data
  mutable STensor43 _tangent3D;
  const STensor3 _ISTensor3;
  mutable STensor3 tau,PKxyz,PKsh,invF,tauXYZ,Fp1,Fp0;
 public:
  J2linearShellLaw(const int num,const double thick,const int nsimp,const double rho,
                   double E,const double nu,const double sy0,const double h,const double tol=1.e-6);
  J2linearShellLaw(const int num,const double thick,const int nsimp,const double rho,
                   double E,const double nu, J2IsotropicHardening &j2IH ,const double tol=1.e-6);
 #ifndef SWIG
  J2linearShellLaw(const J2linearShellLaw &source);
  virtual ~J2linearShellLaw(){}
  // set the time of _j2law
  virtual void setTime(const double t,const double dtime){
    this->_timeStep = dtime;
    this->_currentTime = t;
    _j2law.setTime(t,dtime);
  }
  virtual materialLaw::matname getType() const {return _j2law.getType();}
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv,const MElement *ele=NULL, const int nbFF_=0) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const{return _j2law.soundSpeed();} // or change value ??
  virtual void stress(IPStateBase*,bool stiff, const bool checkfrac=true);
  virtual void shellBendingStiffness(const IPVariableShell *ipv, LinearElasticShellHookeTensor &Hbend) const;
  virtual void shellMembraneStiffness(const IPVariableShell *ipv, LinearElasticShellHookeTensor &Htens) const;
  virtual void shellShearingStiffness(const IPVariableShell *ipv, linearElasticShellShearingHookeTensor &Htens) const;
  virtual void materialTensor33(const IPStateBase *ips, const int npoint,double C33ij[3][3]) const; // npoint = numero of Simpson's point
  virtual void stresst33(const IPStateBase *ips,const int npoint, double &t33, double &t33rel) const; //t33rel is the relative value divided by a material constant used to check convergence
 #endif // SWIG
};
#endif // _DGNONLINEARSHELLMATERIAL_H_
