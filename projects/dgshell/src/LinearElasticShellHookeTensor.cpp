//
// C++ Interface: terms
//
// Description: Hooke Tensor for shell (linear elastic)
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "LinearElasticShellHookeTensor.h"

void LinearElasticShellHookeTensor::set(const shellLocalBasis *lb,const double &C11,const double &nu)
{
  const double half1mnu = 0.5*(1.-nu);
// precompute the dot product
  static double dotDual[4];
  for(int i=0;i<2;i++)
    for(int j=0;j<2;j++)
      dotDual[i+j+j] = dot(lb->dualBasisVector(i),lb->dualBasisVector(j));
  for(int i=0;i<2;i++)
    for(int j=0;j<2;j++)
      for(int ii=0;ii<2;ii++)
        for(int jj=0;jj<2;jj++)
          tensor[i][j][ii][jj] = C11*( nu*dotDual[i+j+j]*dotDual[ii+jj+jj] + half1mnu*(dotDual[i+ii+ii]*dotDual[jj+j+j] + dotDual[i+jj+jj]*dotDual[ii+j+j]) );
}

/* // unused Check optimized implementation!
void LinearElasticShellHookeTensor::hat(const shellLocalBasisBulk *lb, const double C11, const double nu)
{
  this->setAll(0.);
  const double half1mnu = 0.5*(1.-nu);
  // Precompute the dot product
  static double dotDual[4];
  for(int i=0;i<2;i++)
    for(int j=0;j<2;j++)
      dotDual[i+j+j] = dot(lb->dualBasisVector(i),lb->dualBasisVector(j));
  // Precompute the pushTensor products
  static double Tprod[2][2][2][2];
  for(int a=0;a<2;a++)
    for(int b=0;b<2;b++)
      for(int i=0;i<2;i++)
        for(int j=0;j<2;j++)
          Tprod[a][b][i][j] = lb->basisPushTensor(a,i)*lb->basisPushTensor(b,j);

  for(int i=0;i<2;i++)
    for(int j=0;j<2;j++)
      double (*tij)[2]=t[i][j];
      for(int a=0;a<2;a++)
        for(int b=0;b<2;b++)
        {
          double C11Tabij = C11*Tprod[a][b][i][j];
          double nuab = nu*dotDual[a+b+b];
          for(int c=0;c<2;c++)
            for(int d=0;d<2;d++)
            {
              double fact = C11Tabij*( nuab*dotDual[c+d+d] + half1mnu*(dotDual[a+c+c]*dotDual[d+b+b]+dotDual[a+d+d]*dotDual[c+b+b]));
              double (*Tcd)[2] = T[c][d];
              for(int ii=0;ii<2;ii++)
                for(int jj=0;jj<2;jj++)
                  tij[ii][jj] += fact*Tcd[ii][jj];
            }
        }

}
*/
/*
void LinearElasticShellHookeTensor::hat(const shellLocalBasisBulk *lb,const LinearElasticShellHookeTensor *H)
{
  double temp;
  for(int i=0;i<2;i++)
    for(int j=0;j<2;j++)
      for(int ii=0;ii<2;ii++)
        for(int jj=0;jj<2;jj++){
          temp =0.;
          for(int a=0;a<2;a++)
            for(int b=0;b<2;b++)
              for(int c=0;c<2;c++)
                for(int d=0;d<2;d++)
                  temp += lb->basisPushTensor(a,i)*lb->basisPushTensor(b,j)*H->get(a,b,c,d)*lb->basisPushTensor(c,ii)*lb->basisPushTensor(d,jj);
          this->tensor[i][j][ii][jj]=temp;
        }
}
*/
void LinearElasticShellHookeTensor::hat(const shellLocalBasisBulk *lb)
{
  LinearElasticShellHookeTensor H(*this);
  this->setAll(0.);
  // Precompute the pushTensor products
  static double Tprod[2][2][2][2];
  for(int a=0;a<2;a++)
    for(int b=0;b<2;b++)
      for(int i=0;i<2;i++)
        for(int j=0;j<2;j++)
          Tprod[a][b][i][j] = lb->basisPushTensor(a,i)*lb->basisPushTensor(b,j);
  for(int i=0;i<2;i++)
    for(int j=0;j<2;j++)
    {
      double (*tij)[2] = this->tensor[i][j];
      for(int a=0;a<2;a++)
        for(int b=0;b<2;b++)
        {
          double (*Tab)[2] = Tprod[a][b];
          for(int c=0;c<2;c++)
            for(int d=0;d<2;d++)
            {
              double TabijHabcd = Tab[i][j]*H(a,b,c,d);
              double (*Tcd)[2] = Tprod[c][d];
              for(int ii=0;ii<2;ii++)
                for(int jj=0;jj<2;jj++)
                  tij[ii][jj] += TabijHabcd*Tcd[ii][jj];
            }
        }
    }
}

void LinearElasticShellHookeTensor::mean(const LinearElasticShellHookeTensor *Hp, const LinearElasticShellHookeTensor *Hm)
{
  for(int a=0;a<2;a++)
    for(int b=0;b<2;b++)
      for(int c=0;c<2;c++)
        for(int d=0;d<2;d++)
          tensor[a][b][c][d] = 0.5*(Hp->get(a,b,c,d)+Hm->get(a,b,c,d));
}

double scaldot(const SVector3 &a, const SVector3 &b){
  double sum=0.;
  for(int i=0;i<3;i++)
    sum += a(i)*b(i);
  return sum;
}

void linearElasticShellShearingHookeTensor::set(const shellLocalBasis *lb, const double &C11){
  this->setAll(0.);
  for(int a=0;a<2;a++){
    for(int b=0;b<2;b++){
      this->tensor[a][b] += C11*scaldot(lb->dualBasisVector(a),lb->dualBasisVector(b));
    }
  }
//  this->print();
}

void linearElasticShellShearingHookeTensor::set(const hookeTensor &H,const double mufactor, const shellLocalBasis *lb){
  this->setAll(0.);
  for(int a=0;a<2;a++)
    for(int b=0;b<2;b++)
      tensor[a][b] += mufactor*scaldot(lb->dualBasisVector(a),lb->dualBasisVector(b));
}

void linearElasticShellShearingHookeTensor::mean(const linearElasticShellShearingHookeTensor &Hp, const linearElasticShellShearingHookeTensor &Hm){
  for(int a=0;a<2;a++)
    for(int b=0;b<2;b++)
      tensor[a][b] = 0.5*(Hp(a,b)+Hm(a,b));
}

void LinearElasticShellHookeTensor::lambdahat(const LinearElasticShellHookeTensor &H, const shellLocalBasisBulk *lb)
{
  for(int mu=0;mu<2;mu++)
    for(int k=0;k<2;k++)
      for(int eta=0;eta<2;eta++)
        for(int xi=0;xi<2;xi++)
          tensor[mu][k][eta][xi] = lb->getlambda(k,0)*H(mu,0,eta,xi)+lb->getlambda(k,1)*H(mu,1,eta,xi);
  // hat operation
  this->hat(lb);
}
