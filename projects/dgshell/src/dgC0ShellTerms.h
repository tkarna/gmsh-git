//
// C++ Interface: terms
//
// Description: Elementary matrix terms for C0 Dg Shell
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef DGC0PLATETERMS_H
#define DGC0PLATETERMS_H
#include "SVector3.h"
#include <string>
#include "shellLocalBasis.h"
#include "LinearElasticShellHookeTensor.h"
#include "reduction.h"
#include "dgC0ShellFunctionSpace.h"
#include "simpleFunction.h"
#include "unknownField.h"
#include "ipField.h"
#include "terms.h"
#include "nodeStiffnessContact.h"
#include "dgC0ShellNormalVariation.h"
#include "dgC0ShellManageBvector.h"
#include "dgC0ShellElementaryTerms.h"
#include "contactTerms.h"
#include "interfaceQuadrature.h"
static inline void matTvectprod(const fullMatrix<double> &mbuildterm, const SVector3 &v, fullVector<double> &v2){
  for(int i=0;i<3;i++)
    v2(i) = mbuildterm(0,i)*v[0] + mbuildterm(1,i)*v[1] + mbuildterm(2,i)*v[2];
}

template<class T1,class T2> class DgC0BilinearTerm : public BiNonLinearTermBase
{
 protected :
  FunctionSpace<T1>& space1;
  FunctionSpace<T2>& space2;
 public :
  DgC0BilinearTerm(FunctionSpace<T1>& space1_,FunctionSpace<T2>& space2_) : space1(space1_),space2(space2_) {}
  virtual ~DgC0BilinearTerm() {}
};

template<class T1> class DgC0LinearTerm : public nonLinearTermBase<double>
{
 protected :
  FunctionSpace<T1>& space1;
 public :
  DgC0LinearTerm(FunctionSpace<T1>& space1_) : space1(space1_){}
  // for shell specific integration rule --> return only integrated redsults
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &vv) const
  {
    Msg::Error("Can't use integration by point for shell");
  };
  virtual ~DgC0LinearTerm() {}
};

template<class T1> class DgC0LoadTerm : public LinearTerm<T1>
{
  simpleFunction<typename TensorialTraits<T1>::ValType> *Load;
 protected:
  // Data of get function (Allocated once)
  mutable int nbdof;
  mutable double jac[3][3];
  mutable double weight;
  mutable SPoint3 p;
  mutable typename TensorialTraits<T1>::ValType load;
 public :
  DgC0LoadTerm(FunctionSpace<T1>& space1_,
               simpleFunction<typename TensorialTraits<T1>::ValType> *Load_) : LinearTerm<T1>(space1_),Load(Load_),
                                                                                nbdof(0), weight(0.)
  {

  }
  virtual ~DgC0LoadTerm() {}

  virtual void get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const
  {
    nbdof=this->space1.getNumKeys(ele);
    jac[3][3];
    m.resize(nbdof);
    m.scale(0.);
    nlsFunctionSpaceUVW<double>* sp1 = static_cast<nlsFunctionSpaceUVW<double>*>(&(this->space1));
    std::vector<GaussPointSpaceValues<double>*> vgps;
    sp1->get(ele,npts,GP,vgps);
    for (int i = 0; i < npts; i++)
    {
      weight = GP[i].weight;
      std::vector<typename TensorialTraits<T1>::ValType> &Vals = vgps[i]->_vvals;
      std::vector<typename TensorialTraits<T1>::GradType> &vgrads = vgps[i]->_vgrads;
      const double detJ = ele->getJacobian(vgrads,jac);
      ele->pnt(Vals,p);
      load=Load->operator()(p.x(),p.y(),p.z());
      double loadvalue = load*weight*detJ;
      for (int j = 0; j < nbdof ; ++j)
        m(j)+=Vals[j]*loadvalue;
    }
  }
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &vv) const
  {
    Msg::Error("Define me get by integration gauss DgC0LoadTerm");
  }
  virtual LinearTermBase<double>* clone () const
  {
    return new DgC0LoadTerm(this->space1,Load);
  }
};


template<class T1> class DgC0VertexLoadTerm : public LinearTerm<T1>
{
  simpleFunction<typename TensorialTraits<T1>::ValType> *Load;
  int _mycomp;
  double _thick;
 protected:
  mutable typename TensorialTraits<T1>::ValType load;
 public :
  DgC0VertexLoadTerm(FunctionSpace<T1>& space1_,
                     simpleFunction<typename TensorialTraits<T1>::ValType> *Load_,
                     const int cmp,const double h) : LinearTerm<T1>(space1_),Load(Load_), _mycomp(cmp), _thick(h){}
  virtual ~DgC0VertexLoadTerm() {}
  virtual void get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const
  {
    int nbdof=this->space1.getNumKeys(ele);
    m.resize(nbdof);
    int nbvertex = ele->getNumVertices();
    int ncomp = nbvertex*_mycomp;
    double vol = _thick*ele->getVolume(); // return area as 2D element
    for (int i = 0; i < nbvertex; i++)
    {
      MVertex *ver = ele->getVertex(i);
      m(i+ncomp)=vol*Load->operator()(ver->x(),ver->y(),ver->z());
    }
    if(ele->getTypeForMSH() == MSH_TRI_6)
    {
      double oneDiv12 = 1./12.;
      m(ncomp) *= oneDiv12;
      m(1+ncomp) *= oneDiv12;
      m(2+ncomp) *= oneDiv12;
      m(3+ncomp) *= 0.25;
      m(4+ncomp) *= 0.25;
      m(5+ncomp) *= 0.25;
    }
    else
    {
      Msg::Fatal("DgC0VertexLoadTerm not implemented in this case");
    }
  }
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &vv) const
  {
    Msg::Error("Define me get by integration gauss DgC0VertexLoadTerm");
  }
  virtual LinearTermBase<double>* clone () const
  {
    return new DgC0VertexLoadTerm(this->space1,Load,_mycomp,_thick);
  }
};



class DgC0PressureLoadTerm : public LinearTerm<double>
{
  simpleFunction<TensorialTraits<double>::ValType> *Load;
  const IPField* _ipf;
 protected:
  //Data used by get function (Allocated once)
  mutable int nbdof,nbFF;
  mutable double u,v,w,weight,wJ;
  mutable const IPVariableShell *vipv[256]; // max number of gauss' points by element if more KO
  mutable double pressureAtVertex[256]; // max number of shape functions by element
  mutable double xyz[3];
  mutable double uvw[3];
  mutable fullMatrix<double> mVals;
  mutable fullMatrix<double> mt0;
  mutable fullMatrix<double> mbuildV;
  mutable fullMatrix<double> mbuildt;
  mutable fullMatrix<double> mres;
 public :
  DgC0PressureLoadTerm(FunctionSpace<double>& space1_,
                    simpleFunction<TensorialTraits<double>::ValType> *Load_,
                    const IPField *ipf) : LinearTerm<double>(space1_),Load(Load_),
                                           nbdof(0), nbFF(0), u(0.), v(0.), w(0.),
                                           weight(0.), wJ(0.), _ipf(ipf){}
  virtual ~DgC0PressureLoadTerm() {}
  virtual void get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const
{
    nbdof=LinearTerm<double>::space1.getNumKeys(ele);
    nbFF=nbdof/3;
    m.resize(nbdof); // no need to set to zero (set value later.
    mVals.resize(nbFF,npts,false); // add value by copy --> no need to set All 0
    mt0.resize(npts,3,false); // add value by copy --> no need to set All 0
    mbuildt.resize(1,3,false); // set value
    mbuildV.resize(nbFF,1,false); // set value
    mres.resize(nbFF,3); // add in matrix so set to 0
    _ipf->getIPv(ele,vipv);
    nlsFunctionSpaceUVW<double>* sp1 = static_cast<nlsFunctionSpaceUVW<double>*>(&(this->space1));
    std::vector<GaussPointSpaceValues<double>*> vgps;
    sp1->get(ele,npts,GP,vgps);
    // give the barycenter of element to compute the pressure
    //SPoint3 bary = ele->barycenter();
    //double press = Load->operator()(bary.x(),bary.y(),bary.z()); // independent of position of Gauss point change this ??
    // compute the pressure at each vertex
    for(int i=0;i<ele->getNumVertices();i++)
    {
      pressureAtVertex[i] = Load->operator()(ele->getVertex(i)->x(),ele->getVertex(i)->y(),ele->getVertex(i)->z());
    }
    for (int i = 0; i < npts; i++)
    {
      //u = GP[i].pt[0]; v = GP[i].pt[1]; w = GP[i].pt[2];
      weight = GP[i].weight;
      const shellLocalBasis *lb = vipv[i]->getshellLocalBasis();
      //Vals.clear();
      //LinearTerm<double>::space1.f(ele,u,v,w,Vals);
      std::vector<TensorialTraits<double>::ValType> &Vals = vgps[i]->_vvals;
      wJ = - weight*lb->getJacobian(); // minus by chosen normal convention same pressure in all direction
      for(int j=0;j<nbFF;j++)
        mbuildV(j,0) = Vals[j]*pressureAtVertex[j]; // no need to access other components
      mVals.copy(mbuildV,0,nbFF,0,1,0,i);
      for(int k=0;k<3;k++)
        mbuildt(0,k) = lb->basisNormal(k);
      mbuildt.scale(wJ);
      mt0.copy(mbuildt,0,1,0,3,i,0);
        // Assembly
    }
    mVals.mult(mt0,mres);
    int j=0;
    for(int k=0;k<3;k++)
      for(int i=0;i<nbFF;i++){
        m(j) = mres(i,k);
        j++;
      }
  }
  // keep for scholar purpose
/*  virtual void get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m)
  {
    nbdof=DgC0LinearTerm<SVector3>::space1.getNumKeys(ele);
    nbFF=nbdof/3;
    m.resize(nbdof);
    m.scale(0.);
    for (int i = 0; i < npts; i++)
    {
      u = GP[i].pt[0]; v = GP[i].pt[1]; w = GP[i].pt[2];
      weight = GP[i].weight;
      DgC0LinearTerm<SVector3>::space1.fuvw(ele,u,v,w,Vals);
      DgC0LinearTerm<SVector3>::space1.gradfuvw(ele,u,v,w,Grads);
      DgC0LinearTerm<SVector3>::space1.hessfuvw(ele,u,v,w,Hess);
      lb.set(ele,Grads,Hess);
      wJpress = - weight*lb.getJacobian()*pressure; // minus by chosen normal convention
      for(int j=0;j<nbFF;j++){
        Vals[j]*=wJpress;
        for(int k=0;k<3;k++)
          m(j+k*nbFF) +=Vals[j]*lb.basisNormal(k); //
      }
    }
  }*/
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &vv) const
  {
    Msg::Error("Define me get by integration point DgC0PressureLoadTerm");
  }
  virtual LinearTermBase<double>* clone () const
  {
    return new DgC0PressureLoadTerm(space1,Load,_ipf);
  }

};

class IsotropicForceBulkTermC0DgShell : public DgC0LinearTerm<double>
{
 protected :
  const bool fullDg;
  const IPField *ipf;
  const shellMaterialLaw *_mlaw;
  // data of get function (Allocated 1 time)
 private:
  mutable int nbdof, nbFF;
  mutable double wJ,weight;
  mutable std::vector<SVector3> malpha;
  mutable std::vector<SVector3> vnalpha[256]; // max 256 Gauss' point ?? n is used outside mainloop
  mutable reductionElement malphabeta; // m is not used outside main loop
//  mutable std::vector<TensorialTraits<double>::GradType> Grad;
  mutable fullMatrix<double> m1; // matrix to build term
  mutable fullMatrix<double> m2; // matrix to build term
 public :
  IsotropicForceBulkTermC0DgShell(FunctionSpace<double>& space1_,const materialLaw* mlaw,bool FullDG,
                                       const IPField *ip) : DgC0LinearTerm<double>(space1_),
                                                                                 fullDg(FullDG), ipf(ip),
                                                                                 nbdof(0), nbFF(0), wJ(0.),
                                                                                 weight(0.), malpha(2),
                                                                                 _mlaw(static_cast<const shellMaterialLaw*>(mlaw))
  {
    for(int i=0;i<256;i++){
      vnalpha[i].resize(2);
    }
  }
  virtual ~IsotropicForceBulkTermC0DgShell(){}
  virtual void get(MElement *ele,int npts,IntPt *GP,fullVector<double> &m) const;
  virtual const bool isData() const {return false;}
  virtual void set(const fullVector<double> *datafield){}
  virtual LinearTermBase<double>* clone () const
  {
    return new IsotropicForceBulkTermC0DgShell(space1,_mlaw,fullDg,ipf);
  }
}; // IsotropicForceBulkTermC0DgShell


// elementary mass matrix
class massC0DgShell : public BilinearTerm<double,double>{
 protected:
  double _rho; // rho is the mass by thickness unit
  bool sym;
  // Allocation of space for variables used in get function
  mutable int nbdof,nbFF;
//  mutable std::vector<TensorialTraits<double>::ValType> Vals;
//  mutable std::vector<TensorialTraits<double>::HessType> Hess;
//  mutable std::vector<TensorialTraits<double>::GradType> Grad;
  mutable linearShellLocalBasisBulk lb;
  mutable double mij, detJ,weight;
 public:
  massC0DgShell(FunctionSpace<double> &space1_,
                materialLaw *mlaw): BilinearTerm<double,double>(space1_,space1_), sym(true),
                                               nbdof(0),nbFF(0), mij(0),detJ(0),weight(0)
  {
    //shellMaterialLaw *slaw = static_cast<shellMaterialLaw*>(mlaw);
    _rho = mlaw->density();
  }
  massC0DgShell(FunctionSpace<double> &space1_, FunctionSpace<double> &space2_,
                 materialLaw *mlaw): BilinearTerm<double,double>(space1_,space2_)
  {
    sym=(&space1_==&space2_);
    //shellMaterialLaw *slaw = static_cast<shellMaterialLaw*>(mlaw);
    _rho = mlaw->density();
  }
  massC0DgShell(FunctionSpace<double> &space1_, FunctionSpace<double> &space2_,
                 double rho): BilinearTerm<double,double>(space1_,space2_),_rho(rho)
  {
    sym=(&space1_==&space2_);
  }
  virtual void get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me get by integration point massC0DgShell");
  }
  virtual BilinearTermBase* clone () const
  {
    return new massC0DgShell(space1,space2,_rho);
  }
};

class IsotropicStiffBulkTermC0DgShell : public BilinearTerm<double,double>
{
 protected :
  const shellMaterialLaw *_mlaw;
  bool sym;
  const bool fullDg;
  const IPField *ipf;
  // Data for get function (Allocated one time)
  mutable int nbdof,nbFF;
  mutable double wJ,weight;
  mutable LinearElasticShellHookeTensor Hbend, Htens;
  mutable const IPVariableShell *vipv[256]; // max 256 Gauss' points
 private:
  // Avoid recursive allocation
  mutable fullMatrix<double> mbuild1, mbuild3;
 public :
  IsotropicStiffBulkTermC0DgShell(FunctionSpace<double>& space1_,FunctionSpace<double>& space2_,materialLaw* mlaw,
                                  bool FullDG, IPField *ip) : BilinearTerm<double,double>(space1_,space2_),
                                                            fullDg(FullDG), ipf(ip), nbdof(0), nbFF(0), wJ(0.),
                                                            _mlaw(static_cast<const shellMaterialLaw*>(mlaw)),
                                                            weight(0.),
                                                            mbuild3(4,4)

  {
    sym=(&space1_==&space2_);
  }
  IsotropicStiffBulkTermC0DgShell(FunctionSpace<double>& space1_, const materialLaw* mlaw,
                                  bool FullDG,const IPField *ip) : BilinearTerm<double,double>(space1_,space1_),
                                                             fullDg(FullDG), ipf(ip), nbdof(0), nbFF(0), wJ(0.),
                                                             _mlaw(static_cast<const shellMaterialLaw*>(mlaw)),
                                                             weight(0.),
                                                             mbuild3(4,4)
  {
    sym=true;
  }
  virtual ~IsotropicStiffBulkTermC0DgShell(){}
  virtual void get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me get by gauss point IsotropicStiffBulkTermC0DgShell");
  }
  virtual BilinearTermBase* clone () const
  {
    return new IsotropicStiffBulkTermC0DgShell(space1,_mlaw,fullDg,ipf);
  }
}; // IsotropicStiffBulkTermC0DgShell

class IsotropicForceInterTermC0DgShell : public DgC0LinearTerm<double> // minus element
{
 protected :
  double beta1,beta2,beta3;
  IPField *ipf;
  nlsFunctionSpaceUVW<double> *_minusSpace;
  nlsFunctionSpaceUVW<double> *_plusSpace;
//  materialLaw *_mflaw;
  shellMaterialLaw *_mlawMinus;
  shellMaterialLaw *_mlawPlus;
  const bool _fulldg;
  interfaceQuadratureBase *_interQuad;
  // Data for get function (Allocated once)
  mutable MInterfaceLine *iele;
  mutable int nbdof_m,nbdof_p,nbFF_m,nbFF_p;
//  mutable std::vector<TensorialTraits<double>::ValType> Val_m;
//  mutable std::vector<TensorialTraits<double>::ValType> Val_p;
//  mutable std::vector<TensorialTraits<double>::GradType> Grads_m;
//  mutable std::vector<TensorialTraits<double>::GradType> Grads_p;
  mutable Bvector _Bmhat,_Bphat;
  mutable double Bs_p[256][3],Bs_m[256][3];
  mutable LinearElasticShellHookeTensor H_p,H_m,Hmean;
  mutable linearElasticShellShearingHookeTensor Hs_m,Hs_p,Hsmean;
  mutable LinearElasticShellHookeTensor Hlambda_m, Hlambda_p;
  mutable double wJ;
  mutable fullVector<double> ujump;
  mutable double h_s,Bhs,B2hs,B3hs;
  mutable double weight;
 private:
  mutable fullMatrix<double> mbuild;
  mutable fullVector<double> vbuild;
  mutable fullMatrix<double> mbuildblock;
  mutable fullMatrix<double> mbuildminus;
  mutable fullMatrix<double> mbuildplus;
  mutable fullVector<double> rjump;
  mutable double wJnucomp[2];
  mutable double rjumpdotphi0[2];
  mutable double ujumpphi0[2];
  mutable normalVariation _nvar;
  mutable reductionElement nhatmean;
  mutable std::vector<SVector3> _vmhatmean;
  mutable std::vector<SVector3> _vmhatp;
  mutable std::vector<SVector3> _vnhatmean;
  mutable std::vector<SVector3> _vnhatp;
  mutable bool broken;
  // To get displacemenb
  const fullVector<double> *_data;
/*  inline void bendingCompatibility(const int jj, const int nbFF, const int nbdof, const shellLocalBasis *lbs,
                                   const LinearElasticShellHookeTensor &H, Bvector &B, fullVector<double> &m)
  {
    vbuild1.scale(0.);
    for(int c=0;c<2;c++)
      for(int d=0;d<2;d++){
        buildfactor1 = -0.5*((rjumpdotphi0[0]*H(0,0,c,d)+rjumpdotphi0[1]*H(1,0,c,d))*lbs->getMinusNormal(0)+
                             (rjumpdotphi0[0]*H(0,1,c,d)+rjumpdotphi0[1]*H(1,1,c,d))*lbs->getMinusNormal(1)); // minus because -nu^-(beta) with the chosen convention
        vbuild1(0)+= buildfactor1*B.getBending(jj,0,c,d) ; vbuild1(1)+=buildfactor1*B.getBending(jj,1,c,d); vbuild1(2)+=buildfactor1*B.getBending(jj,2,c,d);
      }
    for(int k=0;k<3;k++)
      m(jj+k*nbFF+nbdof) += vbuild1(k);
  }
  inline void bendingStability(const int jj, const int nbFF, const int nbdof, const shellLocalBasis *lbs,
                               const  fullMatrix<double> Dt,fullVector<double> &m)
  {
    vbuild2.scale(0.);
    for(int c=0;c<2;c++){
      matTvectprod(Dt,lbs->basisVector(c),vbuild1);
      buildfactor1 = ((rjumpdotphi0[0]*Hmean(0,0,c,0)+rjumpdotphi0[1]*Hmean(1,0,c,0))*lbs->getMinusNormal(0)+(rjumpdotphi0[0]*Hmean(0,0,c,1)+rjumpdotphi0[1]*Hmean(1,0,c,1))*lbs->getMinusNormal(1))*lbs->getMinusNormal(0)+
                     ((rjumpdotphi0[0]*Hmean(0,1,c,0)+rjumpdotphi0[1]*Hmean(1,1,c,0))*lbs->getMinusNormal(0)+(rjumpdotphi0[0]*Hmean(0,1,c,1)+rjumpdotphi0[1]*Hmean(1,1,c,1))*lbs->getMinusNormal(1))*lbs->getMinusNormal(1); // -- = + for the two getMinusNormal
        vbuild2.axpy(vbuild1,buildfactor1);
    }
    for(int k=0;k<3;k++)
      m(jj+k*nbFF+nbdof) += vbuild2(k);
  }*/

 public :
  IsotropicForceInterTermC0DgShell(FunctionSpace<double>& space1_, FunctionSpace<double> *space2_,
                                       materialLaw *mlawMinus, materialLaw *mlawPlus,interfaceQuadratureBase *iquad,
                                       double beta1_, double beta2_, double beta3_,
                                       bool fdg, IPField *ip) : DgC0LinearTerm<double>(space1_),
                                                                _minusSpace(static_cast<nlsFunctionSpaceUVW<double>*>(&space1_)),
                                                                _plusSpace(static_cast<nlsFunctionSpaceUVW<double>*>(space2_)),
                                                                beta1(beta1_), beta2(beta2_),beta3(beta3_),
                                                                ipf(ip),_mlawMinus(static_cast<shellMaterialLaw*>(mlawMinus)),
                                                                _mlawPlus(static_cast<shellMaterialLaw*>(mlawPlus)),
                                                                _interQuad(iquad),
                                                                _fulldg(fdg),nbdof_m(0), nbdof_p(0), nbFF_m(0), nbFF_p(0),
                                                                wJ(0.),h_s(0.), Bhs(0.), B2hs(0.), B3hs(0.),
                                                                weight(0.),
                                                                ujump(3), rjump(3),_nvar(),
                                                                _Bmhat(), _Bphat(), _data(NULL),
                                                                _vnhatmean(2), _vnhatp(2), _vmhatp(2), _vmhatmean(2)
  {}
  virtual ~IsotropicForceInterTermC0DgShell() {}
  virtual const bool isData() const{return true;}
  virtual void set(const fullVector<double> *datafield){_data = datafield;}
//  virtual void getOld(MElement *iele,int npts,IntPt *GP, fullVector<double> &v);
  virtual void get(MElement *ele, int npts, IntPt *GP, fullVector<double> &v)const;
  virtual LinearTermBase<double>* clone () const
  {
    return new IsotropicForceInterTermC0DgShell(*_minusSpace,_plusSpace,_mlawMinus,_mlawPlus,_interQuad,beta1,beta2,beta3,_fulldg,ipf);
  }
}; // class IsotropicForceInterDomainTermC0DgShell

class IsotropicStiffInterTermC0DgShell : public BilinearTerm<double,double>
{
 protected :
  double beta1,beta2,beta3;
  unknownField *_ufield;
  IPField *ipf;
  const shellMaterialLaw *_mlawMinus;
  const shellMaterialLaw *_mlawPlus;
  interfaceQuadratureBase *_interQuad;
  const bool _fulldg;
  double perturbation; // To compute numerically matrix if fracture
  // Data for get function (Allocated Once)
 private:
  mutable normalVariation _nvar;
  mutable MInterfaceLine *iele;
  mutable int nbdof_m,nbdof_p,nbFF_m,nbFF_p;
//  mutable std::vector<TensorialTraits<double>::ValType> Val_m;
//  mutable std::vector<TensorialTraits<double>::ValType> Val_p;
//  mutable std::vector<TensorialTraits<double>::GradType> Grads_m;
//  mutable std::vector<TensorialTraits<double>::GradType> Grads_p;
  mutable IPVariableShell *vipvm[256];
  mutable IPVariableShell *vipvp[256];
  mutable IPVariableShell *vipvprevm[256];
  mutable IPVariableShell *vipvprevp[256];
  mutable Bvector Bhat_p;
  mutable Bvector Bhat_m;
  mutable double Bs_m[256][3], Bs_p[256][3];
  mutable LinearElasticShellHookeTensor H_p,H_m,Hmean;
  mutable LinearElasticShellHookeTensor Hlambda_m, Hlambda_p;
  mutable linearElasticShellShearingHookeTensor Hs_m,Hs_p,Hsmean;
  mutable double wJ,weight,Cst;
  mutable double me_cons[3][3];
  mutable double me_comp[3][3];
  mutable double me_stab[3][3];
  mutable double h_s,Bhs,B2hs,B3hs;
  // matrix by perturbation (fracture case)
  mutable fullVector<double> fp;
  mutable fullVector<double> fm;
  mutable fullVector<double> disp;
  mutable std::vector<Dof> R;
 public :
  IsotropicStiffInterTermC0DgShell(FunctionSpace<double>& space1_,FunctionSpace<double>& space2_,
                                    const materialLaw* mlawMinus, const materialLaw* mlawPlus, interfaceQuadratureBase *iquad,
                                    double beta1_,double beta2_, double beta3_, IPField *ip, unknownField *uf,
                                    bool fulldg,double eps=1.e-6) : BilinearTerm<double,double>(space1_,space2_),
                                                                     beta1(beta1_),beta2(beta2_),beta3(beta3_),
                                                                     ipf(ip), _ufield(uf),
                                                                     _mlawMinus(static_cast<const shellMaterialLaw*>(mlawMinus)),
                                                                     _mlawPlus(static_cast<const shellMaterialLaw*>(mlawPlus)),
                                                                     _interQuad(iquad),
                                                                     nbdof_m(0), nbdof_p(0), nbFF_m(0), nbFF_p(0),
                                                                     wJ(0.), h_s(0.), Bhs(0.), B2hs(0.), B3hs(0.),
                                                                     Cst(0.), _fulldg(fulldg),
                                                                     Bhat_m(), Bhat_p(), _nvar(), disp(), perturbation(eps)
  {

  }
  virtual ~IsotropicStiffInterTermC0DgShell() {}
  virtual void get(MElement *ele,int npts,IntPt *GP, fullMatrix<double> &m)const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me ?? get by integration point IsotropicStiffIntertermC0DgShell");
  }
  virtual BilinearTermBase* clone () const
  {
    return new IsotropicStiffInterTermC0DgShell(space1,space2,_mlawMinus,_mlawPlus,_interQuad,beta1,beta2,beta3,ipf,_ufield,_fulldg,perturbation);
  }
}; // class IsotropicStiffInterDomainTermC0DgShell


class IsotropicElasticForceVirtualInterfaceTermC0DgShell : public DgC0LinearTerm<double>
{
 protected :
  const double beta1,beta2,beta3;
  bool  fullDg;
  IPField *ipf;
  shellMaterialLaw *_mlaw;
  interfaceQuadratureBase *_interQuad;
  // Data for get function (Allocated once)
  mutable MInterfaceLine *iele;
  mutable int nbdof_m,nbFF_m;
  mutable Bvector Bhat_m;
  mutable normalVariation nvar;
  mutable fullVector<double> rjump;
  mutable double rjumpdotphi0[2];
  mutable fullVector<double> vbuild1;
  mutable fullVector<double> vbuild2;
  mutable double buildfactor1;
  mutable LinearElasticShellHookeTensor H_m;
  mutable double wJ;
//  mutable reductionElement mhatmean, nhatmean;
  mutable std::vector<SVector3> vmhat;
  mutable std::vector<SVector3> vnhat;
  mutable double h_s,Bhs;
  mutable double weight;
  mutable double dtmalpha[3];
  mutable fullVector<double> m1; // matrix used to build term
  mutable fullMatrix<double> m2; // matrix used to build term
  mutable fullVector<double> ujump;
  // access to data
  const fullVector<double> *_data;
 public :
  IsotropicElasticForceVirtualInterfaceTermC0DgShell(FunctionSpace<double>& space1_,materialLaw *mlaw, interfaceQuadratureBase *iquad,
                                                     double beta1_,double beta2_, double beta3_, bool FullDg_,
                                                     IPField  *ip) : DgC0LinearTerm<double>(space1_),
                                                                     beta1(beta1_), beta2(beta2_),beta3(beta3_),fullDg(FullDg_),
                                                                     ipf(ip),_mlaw(static_cast<shellMaterialLaw*>(mlaw)),
                                                                     _interQuad(iquad),nbdof_m(0), nbFF_m(0), wJ(0.),h_s(0.), Bhs(0.),
                                                                     weight(0.),rjump(3), vbuild1(3), vbuild2(3), nvar(), ujump(3),
                                                                     _data(NULL), vmhat(2), vnhat(2)
  {

  }
  virtual ~IsotropicElasticForceVirtualInterfaceTermC0DgShell() {}
  virtual void set(const fullVector<double> *datafield){_data = datafield;}
  virtual void get(MElement *iele,int npts,IntPt *GP, fullVector<double> &v) const;
  virtual const bool isData() const{return true;}
  virtual LinearTermBase<double>* clone () const
  {
    return new IsotropicElasticForceVirtualInterfaceTermC0DgShell(space1,_mlaw,_interQuad,beta1,beta2,beta3,fullDg,ipf);
  }
}; // class IsotropicElasticForceVirtualInterfaceTermC0DgShell


class IsotropicStiffVirtualInterfaceTermC0DgShell : public BilinearTerm<double,double>
{
 protected :
  double beta1,beta2,beta3;
  bool sym;
  bool  fullDg;
  unknownField *ufield;
  IPField *ipf;
  shellMaterialLaw *_mlaw;
  interfaceQuadratureBase *_interQuad;
  // Data used by get function (Allocated once)
  mutable MInterfaceLine *iele;
  mutable int nbdof_m,nbFF_m;
  mutable IPVariableShell *vipvm[256];
  mutable IPVariableShell *vipvprevm[256];
  mutable Bvector Bhat_m;
  mutable LinearElasticShellHookeTensor H_m;
  mutable normalVariation _nvar;
  mutable double wJ;
  mutable double me_cons[3][3];
  mutable double me_comp[3][3];
  mutable double me_stab[3][3];
  mutable double h_s, Bhs;
  mutable double weight;
 public :
  IsotropicStiffVirtualInterfaceTermC0DgShell(FunctionSpace<double>& space1_,FunctionSpace<double>& space2_,
                                              materialLaw *mlaw,interfaceQuadratureBase *iquad,double beta1_,double beta2_, double beta3_,
                                              unknownField *uf, IPField *ipf_,
                                              bool FullDg_) : BilinearTerm<double,double>(space1_,space2_),
                                                              beta1(beta1_),beta2(beta2_),beta3(beta3_),fullDg(FullDg_),
                                                              ipf(ipf_),
                                                              _mlaw(static_cast<shellMaterialLaw*>(mlaw)), _interQuad(iquad),
                                                              sym(false),nbdof_m(0), nbFF_m(0), wJ(0.),h_s(0.), Bhs(0.),
                                                              weight(0.), Bhat_m(), _nvar()
  {
    sym = (&space1_ == &space2_) ;
  }
  IsotropicStiffVirtualInterfaceTermC0DgShell(FunctionSpace<double>& space1_,materialLaw* mlaw,interfaceQuadratureBase *iquad,
                                       double beta1_,double beta2_, double beta3_,
                                       unknownField *uf, IPField *ipf_,
                                       bool FullDg_) : BilinearTerm<double,double>(space1_,space1_),
                                                       beta1(beta1_),beta2(beta2_),beta3(beta3_),fullDg(FullDg_) , ufield(uf),
                                                       ipf(ipf_), _mlaw(static_cast<shellMaterialLaw*>(mlaw)), sym(true),
                                                       _interQuad(iquad),
                                                        nbdof_m(0), nbFF_m(0), wJ(0.),
                                                        h_s(0.), Bhs(0.), weight(0.), Bhat_m(), _nvar()
  {

  }
  virtual ~IsotropicStiffVirtualInterfaceTermC0DgShell() {}
  virtual void get(MElement *iele,int npts,IntPt *GP, fullMatrix<double> &m) const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me get by integration point IsotropicStiffVirtualInterfaceTermC0DgShell");
  }
  virtual BilinearTermBase* clone () const
  {
    return new IsotropicStiffVirtualInterfaceTermC0DgShell(space1,_mlaw,_interQuad,beta1,beta2,beta3,ufield,ipf,fullDg);
  }
}; // class IsotropicStiffVirtualInterfaceTermC0DgShell


/* Old formulation of force interface term keep for school understanding
class IsotropicForceInterfaceTermC0DgShell : public DgC0LinearTerm<SVector3>
{
 protected :
  double beta1,beta2,beta3;
  double h;
  bool  fullDg;
  unknownField *ufield;
  IPField *ipf;
  SolElementType::eltype _elemtype; // TODO Two SolElementType One for minus and one for + element
  bool MatrixByPerturbation, minus; // Use to compute matrix by numerical perturbation
  Dof pertDof;
  double pert;
  materialLaw *_mlaw;
 public :
  IsotropicForceInterfaceTermC0DgShell(DgC0FunctionSpace<SVector3>& space1_, materialLaw *mlaw, double beta1_,
                                       double beta2_, double beta3_, double h_, bool FullDg_, unknownField *uf,
                                       IPField *ip,
                                       SolElementType::eltype et,bool mbp=false) : DgC0LinearTerm<SVector3>(space1_),
                                                                            beta1(beta1_), beta2(beta2_),beta3(beta3_),h(h_),
                                                                            fullDg(FullDg_), ufield(uf), ipf(ip),
                                                                            _elemtype(et), MatrixByPerturbation(mbp), minus(true),
                                                                            pert(0.), pertDof(0,0), _mlaw(mlaw){}
  virtual ~IsotropicForceInterfaceTermC0DgShell() {}
  virtual void get(MElement *iele,int npts,IntPt *GP, fullVector<double> &v);
  void setMinus(const bool e){minus = e;}
  virtual void setPert(const double eps){pert = eps;}
  virtual void setPertDof(const Dof &D){pertDof = Dof(D.getEntity(),D.getType());}
  virtual void invSign(){inverseSign ? inverseSign = false : inverseSign=true;}
}; // class IsotropicForceInterfaceTermC0DgShell
*/



#endif // DGC0PLATETERMS_H
