//
// C++ Interface: terms
//
// Description: Functions used to compute a component of elementary matrix term
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef DGC0SHELLELEMENTARYTERMS_H_
#define DGC0SHELLELEMENTARYTERMS_H_
#include "SVector3.h"
#include "functionSpace.h"
#include "LinearElasticShellHookeTensor.h"
#include "openingForShell.h"
#include "shellIPVariable.h"
#include "shellMaterialLaw.h"
#include "shellFractureLaw.h"
inline void matTvectprod(const fullMatrix<double> &mbuildterm, const SVector3 &v, double v2[3]){
  for(int i=0;i<3;i++)
    v2[i] = mbuildterm(0,i)*v[0] + mbuildterm(1,i)*v[1] + mbuildterm(2,i)*v[2];
}

inline void matTvectprod(const double mbuildterm[3][3], const SVector3 &v, double v2[3]){
  for(int i=0;i<3;i++)
    v2[i] = mbuildterm[0][i]*v[0] + mbuildterm[1][i]*v[1] + mbuildterm[2][i]*v[2];
}

inline void diaprodAdd(double a[3], const double b[3], const double wJ, const int nbFF,const int j,
                       const int k, fullMatrix<double> &m){
  for(int jj=0;jj<3;jj++){
    a[jj]*=wJ;
    for(int kk=0;kk<3;kk++)
      m(j+jj*nbFF,k+kk*nbFF)+=a[jj]*b[kk];
  }
}

inline double dot(const fullVector<double> &a,const SVector3 &b){
  return a(0)*b(0)+a(1)*b(1)+a(2)*b(2);
}

inline void diaprod(const fullVector<double> &a, const fullVector<double> &b, fullMatrix<double> &mbt){
  for(int i=0;i<3;i++)
    for(int j=0;j<3;j++)
      mbt(i,j)=a(i)*b(j);
}

inline void diaprodAdd(const double a[3], const double b[3], double m[3][3]){
  for(int i=0;i<3;i++)
    for(int j=0;j<3;j++)
      m[i][j]+=a[i]*b[j];
}

inline void diaprodAdd(const fullVector<double> &a, const fullVector<double> &b, fullMatrix<double> &m){
  for(int i=0;i<3;i++)
    for(int j=0;j<3;j++)
      m(i,j)+=a(i)*b(j);
}

inline void diaprodAdd(const double a[3], const SVector3 &b, double m[3][3]){
  for(int i=0;i<3;i++)
    for(int j=0;j<3;j++)
      m[i][j]+=a[i]*b[j];
}

inline void diaprodAdd(const SVector3 &a, const double b[3], double m[3][3]){
  for(int i=0;i<3;i++)
    for(int j=0;j<3;j++)
      m[i][j]+=a[i]*b[j];
}

inline void diaprod(const SVector3 &a, const double b[3], double m[3][3]){
  for(int i=0;i<3;i++)
    for(int j=0;j<3;j++)
      m[i][j]=a[i]*b[j];
}

inline void diaprod(const SVector3 &a, const SVector3 &b, fullMatrix<double> &m){
  for(int i=0;i<3;i++)
    for(int j=0;j<3;j++)
      m(i,j)=a[i]*b[j];
}

// group with other displacement jump functions that are in reduction.h ???
static void displacementjump(const std::vector<TensorialTraits<double>::ValType> &Val_m,const int n_m,
                             const std::vector<TensorialTraits<double>::ValType> &Val_p,
                             const int n_p,const std::vector<double> &disp,fullVector<double> &ujump){
  ujump.scale(0.);
  for(int i=0;i<3;i++){
    for(int j=0;j<n_m;j++)
      ujump(i)-=Val_m[j]*disp[j+i*n_m];
    for(int j=0;j<n_p;j++)
      ujump(i)+=Val_p[j]*disp[j+i*n_p+3*n_m];
  }
}

/*inline void consC0ShellStiffnessTerms(LinearElasticShellHookeTensor *Hhat,const int jj, const Bvector &Bhat,
                                      const fullMatrix<double> &dt, const shellLocalBasis *lb, double me[3][3]){
  for(int i=0;i<3;i++) for(int j=0;j<3;j++) me[i][j]=0.;
  double v1[3], v2[3];
  double temp=0.;
  double temp2=0.;
  for(int alpha=0;alpha<2;alpha++){
    matTvectprod(dt,lb->basisVector(alpha),v2);
    for(int beta=0;beta<2;beta++){
      temp = 0.5*(-scaldot(lb->basisVector(1),lb->basisVector(beta)));
      v1[0]=0.; v1[1]=0.; v1[2]=0.;
      for(int gamma=0;gamma<2;gamma++)
        for(int delta=0;delta<2;delta++){
          temp2 = temp *Hhat->get(alpha,beta,gamma,delta);
          v1[0] += temp2*Bhat.getBending(jj,0,gamma,delta); v1[1] += temp2*Bhat.getBending(jj,1,gamma,delta); v1[2] += temp2*Bhat.getBending(jj,2,gamma,delta);
        }
      diaprodAdd(v2,v1,me);
    }
  }
}*/
// BEAWARE indices alpha and beta are inverted
inline void consC0ShellStiffnessCouplingTerms(const LinearElasticShellHookeTensor &Hlambda,
                                               const int jj, const double Val, const Bvector &Bhat,
                                               const linearShellLocalBasisBulk *lb, const linearShellLocalBasisInter *lbs, double me[3][3]){
  for(int i=0;i<3;i++) for(int j=0;j<3;j++) me[i][j]=0.;
  double v3[3],v4[3];
  double B[3];
  double temp3=0.;
  for(int beta=0;beta<2;beta++){
    v3[0]=0.; v3[1]=0.; v3[2]=0.;
    v4[0] = lbs->basisVector(beta,0)*Val;
    v4[1] = lbs->basisVector(beta,1)*Val;
    v4[2] = lbs->basisVector(beta,2)*Val;
    for(int gamma=0;gamma<2;gamma++)
      for(int delta=0;delta<2;delta++){
        B[0] = Bhat.getBending(jj,0,gamma,delta);
        B[1] = Bhat.getBending(jj,1,gamma,delta);
        B[2] = Bhat.getBending(jj,2,gamma,delta);
        temp3 = -0.5*(lbs->getMinusNormal(0)*Hlambda(0,beta,gamma,delta)+lbs->getMinusNormal(1)*Hlambda(1,beta,gamma,delta));
        v3[0] += temp3*B[0], v3[1] += temp3*B[1]; v3[2] += temp3*B[2];
      }
    diaprodAdd(v4,v3,me);
  }
}

// BEAWARE indices alpha and beta are inverted
inline void consC0ShellStiffnessTerms(const LinearElasticShellHookeTensor *Hhat,const int jj, const Bvector &Bhat,
                                      const fullMatrix<double> &dt, const linearShellLocalBasisBulk *lb,
                                      const linearShellLocalBasisInter *lbs, double me[3][3]){
  for(int i=0;i<3;i++) for(int j=0;j<3;j++) me[i][j]=0.;
  double v1[3], v2[3],v3[3];
  double temp2=0.;
  for(int beta=0;beta<2;beta++){
    matTvectprod(dt,lbs->basisVector(beta),v2);
    v1[0]=0.; v1[1]=0.; v1[2]=0.;
    v3[0]=0.; v3[1]=0.; v3[2]=0.;
    for(int gamma=0;gamma<2;gamma++)
      for(int delta=0;delta<2;delta++){
        temp2 = -0.5*(lbs->getMinusNormal(0)*Hhat->get(0,beta,gamma,delta)+lbs->getMinusNormal(1)*Hhat->get(1,beta,gamma,delta));
        v1[0] += temp2*Bhat.getBending(jj,0,gamma,delta); v1[1] += temp2*Bhat.getBending(jj,1,gamma,delta); v1[2] += temp2*Bhat.getBending(jj,2,gamma,delta);
      }
    diaprodAdd(v2,v1,me);
  }
}

/*inline void compC0ShellStiffnessTerms(LinearElasticShellHookeTensor *Hhat,const int jj,const Bvector &Bhat,const fullMatrix<double> &dt, const shellLocalBasis *lb, double me[3][3]){
  for(int i=0;i<3;i++) for(int j=0;j<3;j++) me[i][j]=0.;
  double v1[3], v2[3];
  double temp=0.;
  double temp2 = 0.;
  for(int alpha=0;alpha<2;alpha++){
    matTvectprod(dt,lb->basisVector(alpha),v2);
    for(int beta=0;beta<2;beta++){
      temp2 = (-scaldot(lb->basisVector(1),lb->basisVector(beta)));
      v1[0]=0.; v1[1]=0.; v1[2]=0.;
      for(int gamma=0;gamma<2;gamma++)
        for(int delta=0;delta<2;delta++){
          temp = 0.5*Hhat->get(alpha,beta,gamma,delta)*temp2;
          v1[0] += temp*Bhat.getBending(jj,0,gamma,delta);
          v1[1] += temp*Bhat.getBending(jj,1,gamma,delta);
          v1[2] += temp*Bhat.getBending(jj,2,gamma,delta);
        }
      diaprodAdd(v1,v2,me);
    }
  }
}
*/
// BEAWARE indices alpha and beta are inverted
inline void compC0ShellStiffnessCouplingTerms(LinearElasticShellHookeTensor &Hlambda,
                                      const int jj, const double Val, const Bvector &Bhat,
                                      const linearShellLocalBasisBulk *lb, const linearShellLocalBasisInter *lbs, double me[3][3]){
  for(int i=0;i<3;i++) for(int j=0;j<3;j++) me[i][j]=0.;
  double v3[3],v4[3];
  double B[3];
  double temp3=0.;
  for(int beta=0;beta<2;beta++){
    v3[0]=0.; v3[1]=0.; v3[2]=0.;
    v4[0] = lbs->basisVector(beta,0)*Val;
    v4[1] = lbs->basisVector(beta,1)*Val;
    v4[2] = lbs->basisVector(beta,2)*Val;
    for(int gamma=0;gamma<2;gamma++)
      for(int delta=0;delta<2;delta++){
        B[0] = Bhat.getBending(jj,0,gamma,delta);
        B[1] = Bhat.getBending(jj,1,gamma,delta);
        B[2] = Bhat.getBending(jj,2,gamma,delta);
        temp3 = -0.5*(lbs->getMinusNormal(0)*Hlambda(0,beta,gamma,delta)+lbs->getMinusNormal(1)*Hlambda(1,beta,gamma,delta));
        v3[0] += temp3*B[0], v3[1] += temp3*B[1]; v3[2] += temp3*B[2];
      }
    diaprodAdd(v3,v4,me);
  }
}
// BEAWARE indices alpha and beta are inverted
inline void compC0ShellStiffnessTerms(const LinearElasticShellHookeTensor *Hhat,const int jj, const Bvector &Bhat,
                                      const fullMatrix<double> &dt, const linearShellLocalBasisBulk *lb,
                                      const linearShellLocalBasisInter *lbs, double me[3][3]){
  for(int i=0;i<3;i++) for(int j=0;j<3;j++) me[i][j]=0.;
  double v1[3], v2[3],v3[3];
  double temp2=0.;
  for(int beta=0;beta<2;beta++){
    matTvectprod(dt,lbs->basisVector(beta),v2);
    v1[0]=0.; v1[1]=0.; v1[2]=0.;
    v3[0]=0.; v3[1]=0.; v3[2]=0.;
    for(int gamma=0;gamma<2;gamma++)
      for(int delta=0;delta<2;delta++){
        temp2 = -0.5*(lbs->getMinusNormal(0)*Hhat->get(0,beta,gamma,delta)+lbs->getMinusNormal(1)*Hhat->get(1,beta,gamma,delta));
        v1[0] += temp2*Bhat.getBending(jj,0,gamma,delta); v1[1] += temp2*Bhat.getBending(jj,1,gamma,delta); v1[2] += temp2*Bhat.getBending(jj,2,gamma,delta);
      }
    diaprodAdd(v1,v2,me);
  }
}


inline void stabilityC0ShellStiffnessTerms(LinearElasticShellHookeTensor *Hhat, const fullMatrix<double> &dta, const fullMatrix<double> &dtb, const shellLocalBasis *lb, double me[3][3]){
  for(int i=0;i<3;i++) for(int j=0;j<3;j++) me[i][j]=0.;
  double v1[3],v2[3],v3[3];
  double temp=0.;
  double temp2;
  for(int alpha=0;alpha<2;alpha++){
    matTvectprod(dtb,lb->basisVector(alpha),v2);
    for(int beta=0;beta<2;beta++){
      temp2 = (-scaldot(lb->basisVector(1),lb->basisVector(beta)));
      v3[0]=0.; v3[1]=0.; v3[2]=0.;
      for(int gamma=0;gamma<2;gamma++){
        matTvectprod(dta,lb->basisVector(gamma),v1);
        for(int delta=0;delta<2;delta++){
          temp = Hhat->get(alpha,beta,gamma,delta)*(-scaldot(lb->basisVector(1),lb->basisVector(delta)))*temp2;
          v3[0]+=temp*v1[0];
          v3[1]+=temp*v1[1];
          v3[2]+=temp*v1[2];
        }
      }
      diaprodAdd(v3,v2,me);
    }
  }
}

inline void compC0ShellStiffnessMembraneTerms(LinearElasticShellHookeTensor *Hhat,const int jj, const Bvector &Bhat,const double Na, const shellLocalBasis *lb, double me[3][3]){
  for(int i=0;i<3;i++) for(int j=0;j<3;j++) me[i][j]=0.;
  double v1[3];// v2[3];
  double temp=0.;
  double temp2;
  for(int alpha=0;alpha<2;alpha++){
    temp2 = (-scaldot(lb->basisVector(1),lb->basisVector(alpha)));
    for(int beta=0;beta<2;beta++){
      v1[0]=0.; v1[1]=0.; v1[2]=0.;
      for(int gamma=0;gamma<2;gamma++)
        for(int delta=0;delta<2;delta++){
          temp = 0.5*Na*Hhat->get(alpha,beta,gamma,delta)*temp2;
          v1[0] += temp*Bhat.getMembrane(jj,0,gamma,delta); v1[1] += temp*Bhat.getMembrane(jj,1,gamma,delta); v1[2] += temp*Bhat.getMembrane(jj,2,gamma,delta);
        }
      diaprodAdd(v1,lb->basisVector(beta),me);
    }
  }
}

inline void consC0ShellStiffnessMembraneTerms(LinearElasticShellHookeTensor *Hhat,const int jj, const Bvector &Bhat,const double Na, const shellLocalBasis *lb, double me[3][3]){
  for(int i=0;i<3;i++) for(int j=0;j<3;j++) me[i][j]=0.;
  double v1[3];
  double temp=0.;
  double temp2;
  for(int alpha=0;alpha<2;alpha++){
    temp2 = (-scaldot(lb->basisVector(1),lb->basisVector(alpha)));
    for(int beta=0;beta<2;beta++){
      v1[0]=0.; v1[1]=0.; v1[2]=0.;
      for(int gamma=0;gamma<2;gamma++)
        for(int delta=0;delta<2;delta++){
          temp = 0.5*Na*Hhat->get(alpha,beta,gamma,delta)*temp2;
          v1[0] += temp*Bhat.getMembrane(jj,0,gamma,delta); v1[1] += temp*Bhat.getMembrane(jj,1,gamma,delta); v1[2] += temp*Bhat.getMembrane(jj,2,gamma,delta);
        }
      diaprodAdd(lb->basisVector(beta),v1,me);
    }
  }
}

inline void stabilityC0ShellStiffnessMembraneTerms(LinearElasticShellHookeTensor *Hhat, const double Na, const double Nb, const shellLocalBasis *lb, double me[3][3]){
  for(int i=0;i<3;i++) for(int j=0;j<3;j++) me[i][j]=0.;
  SVector3 v1(0.,0.,0.);
  double temp=0.;
  double temp2;
  for(int alpha=0;alpha<2;alpha++)
    for(int beta=0;beta<2;beta++){
      temp2 = (-scaldot(lb->basisVector(1),lb->basisVector(beta)));
      v1[0]=0.; v1[1]=0.; v1[2]=0.;
      for(int gamma=0;gamma<2;gamma++)
        for(int delta=0;delta<2;delta++){
          temp = Na*Nb*Hhat->get(alpha,beta,gamma,delta)*(-scaldot(lb->basisVector(1),lb->basisVector(delta)))*temp2;
          v1[0] += lb->basisVector(gamma,0)*temp;
          v1[1] += lb->basisVector(gamma,1)*temp;
          v1[2] += lb->basisVector(gamma,2)*temp;
        }
      diaprodAdd(lb->basisVector(alpha),v1,me);
    }
}

inline void stabilityC0ShellStiffnessShearingTerms(const double Bj[3], const double Bk[3],const shellLocalBasis *lbs,
                                                    const linearElasticShellShearingHookeTensor &Hsmean, double me[3][3]){
  double temp=0.;
  for(int alpha=0;alpha<2;alpha++)
    for(int beta=0;beta<2;beta++)
      temp += Hsmean(alpha,beta)*scaldot(lbs->basisVector(1),lbs->basisVector(alpha))*scaldot(lbs->basisVector(1),lbs->basisVector(beta));
  SVector3 v1;
  v1(0) = temp*Bj[0]; v1(1) = temp*Bj[1]; v1(2) = temp*Bj[2];
  diaprod(v1,Bk,me);
}


static void compute_Bs(const shellLocalBasis *lb, const std::vector<TensorialTraits<double>::ValType> &Vals,
                       const int n, double B[][3]){
  for(int i=0;i<n;i++){
    for(int a=0;a<3;a++)
        B[i][a] = lb->basisNormal(a)*Vals[i];
      }
}
static void computeFintFrac(const fullVector<double> &disp,
                             IPVariableShell *ipvm, const IPVariableShell *ipvmprev,const double weight,
                             const materialLaw *mlaw,const int nbFF_m,
                             const std::vector<TensorialTraits<double>::ValType> &Vals_m,
                             const std::vector<TensorialTraits<double>::GradType> &Grads_m, const int nbdof_m,
                             const int nbFF_p, const std::vector<TensorialTraits<double>::ValType> &Vals_p,
                             const std::vector<TensorialTraits<double>::GradType> &Grads_p, normalVariation &nvar,fullVector<double> &m){

//  normalVariation nvar; // pass it to function to create this one time. Otherwise it's very slow
  reductionElement nhatmean;
  reductionElement mhatmean;
  const linearShellLocalBasisInter *lbs = static_cast<const linearShellLocalBasisInter*>(ipvm->getshellLocalBasisOfInterface());
  // BEAWARE RECOMPUTE nvar if depends on displacement
  // compute jump
  fullVector<double> ujump(3);
  SVector3 ujump__;
  double rjump[3];
  displacementjump(Vals_m,nbFF_m,Vals_p,nbFF_p,disp,ujump);
  for(int i=0;i<3;i++)
    ujump__[i] = ujump(i);
  nvar.jump(disp,rjump);

  IPShellFractureCohesive *ipv = static_cast<IPShellFractureCohesive*>(ipvm);
  const IPShellFractureCohesive *ipvprev = static_cast<const IPShellFractureCohesive*>(ipvmprev);
  double deltan,deltat,delta;
  openingForShell(ujump__,rjump,ipv,deltan,deltat,delta);
  const shellFractureByCohesiveLaw *mflaw = static_cast<const shellFractureByCohesiveLaw*>(mlaw);
//  mflaw->reduction(ipvm->getm0(),ipvm->getn0(),delta,ipvm->getDeltamax(),deltan,deltat,
//                      ipvm->getDeltac(),ipvm->ifTension(),nhatmean,mhatmean);
  IPLinearCohesiveShell *ipvc = static_cast<IPLinearCohesiveShell*>(ipv->getIPvFrac());
  const IPLinearCohesiveShell *ipvcprev = static_cast<const IPLinearCohesiveShell*>(ipvprev->getIPvFrac());
  mflaw->reduction(ipvc,ipvcprev,deltan,deltat,delta,nhatmean,mhatmean);
  double wJ = weight*lbs->getJacobian();
  // Assemblage
  double dt[3];
  for(int alpha=0;alpha<2;alpha++){
    SVector3 malpha = mhatmean(alpha,0) * lbs->basisVector(0) + mhatmean(alpha,1)*lbs->basisVector(1);
    SVector3 nalpha = nhatmean(alpha,0) * lbs->basisVector(0) + nhatmean(alpha,1)*lbs->basisVector(1);
    double wJnucomp = - wJ*lbs->getMinusNormal(alpha); // minus due to chosen convention
    for(int j=0;j<nbFF_m;j++){
      matTvectprod(nvar.deltatTildeMinus(j),malpha,dt);
      for(int k=0;k<3;k++){
        m(j+k*nbFF_m)-= wJnucomp*(dt[k]+nalpha(k)*Vals_m[j]);
      }
    }
    for(int j=0;j<nbFF_p;j++){
      matTvectprod(nvar.deltatTildePlus(j),malpha,dt);
      for(int k=0;k<3;k++)
        m(j+k*nbFF_p+nbdof_m)+= wJnucomp*(dt[k]+nalpha(k)*Vals_p[j]);
    }
  }
};

// method from Adrian to compute the weight associated to each node for the mass repartition

inline void
CalculateTriangleWeightNodeLumpedMassMatrix(const int pOrder,std::vector<double>& elem_ref){

    if(pOrder>5)
      Msg::Fatal("No mass matrix implemented for triangular element of order > 5!");
    // create the elemental lumped matrix for the element of reference
    int nodes_elem = (pOrder + 1)*(pOrder + 2)/2;
    elem_ref.resize(nodes_elem);

    // fill in the elemental lumped matrix for the element of reference
    if (pOrder==1){
        elem_ref[0] = 1.0/3.0;
        elem_ref[1] = 1.0/3.0;
        elem_ref[2] = 1.0/3.0;
        return;
    }
    else if (pOrder==2){
        elem_ref[0] = 1.0/12.0;
        elem_ref[1] = 1.0/12.0;
        elem_ref[2] = 1.0/12.0;
        elem_ref[3] = 0.25;
        elem_ref[4] = 0.25;
        elem_ref[5] = 0.25;
        return;
    }
    else if (pOrder==3){
        elem_ref[0] = 1.5541264737406e-02;
        elem_ref[1] = 1.5541264737406e-02;
        elem_ref[2] = 1.5541264737406e-02;
        elem_ref[3] = 9.3783494105038e-02;
        elem_ref[4] = 9.3783494105038e-02;
        elem_ref[5] = 3.9067524115755e-01;
        elem_ref[6] = 9.3783494105038e-02;
        elem_ref[7] = 9.3783494105038e-02;
        elem_ref[8] = 9.3783494105038e-02;
        elem_ref[9] = 9.3783494105038e-02;
        return;
    }
    else if (pOrder==4){
        elem_ref[0] = 6.5338892721705e-03;
        elem_ref[1] = 6.5338892721705e-03;
        elem_ref[2] = 6.5338892721705e-03;
        elem_ref[3] = 3.8262289309351e-02;
        elem_ref[4] = 4.9986822311398e-02;
        elem_ref[5] = 3.8262289309351e-02;
        elem_ref[6] = 3.8262289309351e-02;
        elem_ref[7] = 4.9986822311398e-02;
        elem_ref[8] = 3.8262289309351e-02;
        elem_ref[9] = 3.8262289309351e-02;
        elem_ref[10] = 4.9986822311398e-02;
        elem_ref[11] = 3.8262289309351e-02;
        elem_ref[12] = 2.0028804313106e-01;
        elem_ref[13] = 2.0028804313106e-01;
        elem_ref[14] = 2.0028804313106e-01;
        return;
    }
    else if (pOrder==5){
        elem_ref[0] = 3.4362622451252e-03;
        elem_ref[1] = 3.4362622451252e-03;
        elem_ref[2] = 3.4362622451251e-03;
        elem_ref[3] = 1.8512062129519e-02;
        elem_ref[4] = 2.6562528579272e-02;
        elem_ref[5] = 2.6562528579272e-02;
        elem_ref[6] = 1.8512062129519e-02;
        elem_ref[7] = 1.8512062129519e-02;
        elem_ref[8] = 2.6562528579272e-02;
        elem_ref[9] = 2.6562528579272e-02;
        elem_ref[10] = 1.8512062129519e-02;
        elem_ref[11] = 1.8512062129519e-02;
        elem_ref[12] = 2.6562528579272e-02;
        elem_ref[13] = 2.6562528579272e-02;
        elem_ref[14] = 1.8512062129519e-02;
        elem_ref[15] = 1.0403225154388e-01;
        elem_ref[16] = 1.3571563812675e-01;
        elem_ref[17] = 1.0403225154388e-01;
        elem_ref[18] = 1.3571563812675e-01;
        elem_ref[19] = 1.0403225154388e-01;
        elem_ref[20] = 1.3571563812675e-01;
        return;
    }
/*    else if (pOrder==6){
        elem_ref[0] = 2.1637845815825e-03;
        elem_ref[1] = 1.0346822533794e-02;
        elem_ref[2] = 1.5028520794799e-02;
        elem_ref[3] = 1.6836302347016e-02;
        elem_ref[4] = 1.5028520794799e-02;
        elem_ref[5] = 1.0346822533794e-02;
        elem_ref[6] = 2.1637845815825e-03;
        elem_ref[7] = 1.0346822533794e-02;
        elem_ref[8] = 5.8190530525921e-02;
        elem_ref[9] = 8.4597480825836e-02;
        elem_ref[10] = 8.4597480825837e-02;
        elem_ref[11] = 5.8190530525921e-02;
        elem_ref[12] = 1.0346822533794e-02;
        elem_ref[13] = 1.5028520794799e-02;
        elem_ref[14] = 8.4597480825836e-02;
        elem_ref[15] = 1.0859120270986e-01;
        elem_ref[16] = 8.4597480825837e-02;
        elem_ref[17] = 1.5028520794799e-02;
        elem_ref[18] = 1.6836302347016e-02;
        elem_ref[19] = 8.4597480825837e-02;
        elem_ref[20] = 8.4597480825836e-02;
        elem_ref[21] = 1.6836302347016e-02;
        elem_ref[22] = 1.5028520794799e-02;
        elem_ref[23] = 5.8190530525921e-02;
        elem_ref[24] = 1.5028520794799e-02;
        elem_ref[25] = 1.0346822533794e-02;
        elem_ref[26] = 1.0346822533794e-02;
        elem_ref[27] = 2.1637845815825e-03;
        return;
    }
    else if (pOrder==7){
        elem_ref[0] = 1.5486233922991e-03;
        elem_ref[1] = 6.5040031093597e-03;
        elem_ref[2] = 9.1080691328198e-03;
        elem_ref[3] = 1.0678305486907e-02;
        elem_ref[4] = 1.0678305486907e-02;
        elem_ref[5] = 9.1080691328198e-03;
        elem_ref[6] = 6.5040031093597e-03;
        elem_ref[7] = 1.5486233922991e-03;
        elem_ref[8] = 6.5040031093597e-03;
        elem_ref[9] = 3.5334114363858e-02;
        elem_ref[10] = 5.3543870942136e-02;
        elem_ref[11] = 5.9985615607282e-02;
        elem_ref[12] = 5.3543870942136e-02;
        elem_ref[13] = 3.5334114363858e-02;
        elem_ref[14] = 6.5040031093597e-03;
        elem_ref[15] = 9.1080691328198e-03;
        elem_ref[16] = 5.3543870942136e-02;
        elem_ref[17] = 7.6796482627451e-02;
        elem_ref[18] = 7.6796482627451e-02;
        elem_ref[19] = 5.3543870942137e-02;
        elem_ref[20] = 9.1080691328198e-03;
        elem_ref[21] = 1.0678305486907e-02;
        elem_ref[22] = 5.9985615607282e-02;
        elem_ref[23] = 7.6796482627451e-02;
        elem_ref[24] = 5.9985615607282e-02;
        elem_ref[25] = 1.0678305486907e-02;
        elem_ref[26] = 1.0678305486907e-02;
        elem_ref[27] = 5.3543870942136e-02;
        elem_ref[28] = 5.3543870942137e-02;
        elem_ref[29] = 1.0678305486907e-02;
        elem_ref[30] = 9.1080691328198e-03;
        elem_ref[31] = 3.5334114363858e-02;
        elem_ref[32] = 9.1080691328198e-03;
        elem_ref[33] = 6.5040031093598e-03;
        elem_ref[34] = 6.5040031093597e-03;
        elem_ref[35] = 1.5486233922991e-03;
        return;
    }
    else if (pOrder==8){
        elem_ref[0] = 1.2650898144030e-03;
        elem_ref[1] = 4.6101467639675e-03;
        elem_ref[2] = 5.9510288777256e-03;
        elem_ref[3] = 6.9511540839036e-03;
        elem_ref[4] = 7.3391487284120e-03;
        elem_ref[5] = 6.9511540839036e-03;
        elem_ref[6] = 5.9510288777256e-03;
        elem_ref[7] = 4.6101467639675e-03;
        elem_ref[8] = 1.2650898144030e-03;
        elem_ref[9] = 4.6101467639675e-03;
        elem_ref[10] = 2.3390807302451e-02;
        elem_ref[11] = 3.5343458302007e-02;
        elem_ref[12] = 4.1390867063979e-02;
        elem_ref[13] = 4.1390867063979e-02;
        elem_ref[14] = 3.5343458302007e-02;
        elem_ref[15] = 2.3390807302451e-02;
        elem_ref[16] = 4.6101467639675e-03;
        elem_ref[17] = 5.9510288777256e-03;
        elem_ref[18] = 3.5343458302007e-02;
        elem_ref[19] = 5.3382170682392e-02;
        elem_ref[20] = 5.9462806622511e-02;
        elem_ref[21] = 5.3382170682392e-02;
        elem_ref[22] = 3.5343458302006e-02;
        elem_ref[23] = 5.9510288777256e-03;
        elem_ref[24] = 6.9511540839036e-03;
        elem_ref[25] = 4.1390867063979e-02;
        elem_ref[26] = 5.9462806622511e-02;
        elem_ref[27] = 5.9462806622511e-02;
        elem_ref[28] = 4.1390867063979e-02;
        elem_ref[29] = 6.9511540839036e-03;
        elem_ref[30] = 7.3391487284120e-03;
        elem_ref[31] = 4.1390867063979e-02;
        elem_ref[32] = 5.3382170682392e-02;
        elem_ref[33] = 4.1390867063979e-02;
        elem_ref[34] = 7.3391487284120e-03;
        elem_ref[35] = 6.9511540839037e-03;
        elem_ref[36] = 3.5343458302007e-02;
        elem_ref[37] = 3.5343458302007e-02;
        elem_ref[38] = 6.9511540839036e-03;
        elem_ref[39] = 5.9510288777256e-03;
        elem_ref[40] = 2.3390807302451e-02;
        elem_ref[41] = 5.9510288777256e-03;
        elem_ref[42] = 4.6101467639675e-03;
        elem_ref[43] = 4.6101467639675e-03;
        elem_ref[44] = 1.2650898144030e-03;
        return;
    }
    else if (pOrder==9){
        elem_ref[0] = 1.1369980678352e-03;
        elem_ref[1] = 3.6269583209455e-03;
        elem_ref[2] = 4.1790475460801e-03;
        elem_ref[3] = 4.7041281127913e-03;
        elem_ref[4] = 5.0605684208266e-03;
        elem_ref[5] = 5.0605684208266e-03;
        elem_ref[6] = 4.7041281127913e-03;
        elem_ref[7] = 4.1790475460801e-03;
        elem_ref[8] = 3.6269583209455e-03;
        elem_ref[9] = 1.1369980678352e-03;
        elem_ref[10] = 3.6269583209455e-03;
        elem_ref[11] = 1.6896350252325e-02;
        elem_ref[12] = 2.4664761777965e-02;
        elem_ref[13] = 2.8983355698210e-02;
        elem_ref[14] = 3.0415917037330e-02;
        elem_ref[15] = 2.8983355698209e-02;
        elem_ref[16] = 2.4664761777965e-02;
        elem_ref[17] = 1.6896350252325e-02;
        elem_ref[18] = 3.6269583209455e-03;
        elem_ref[19] = 4.1790475460801e-03;
        elem_ref[20] = 2.4664761777965e-02;
        elem_ref[21] = 3.7841361566331e-02;
        elem_ref[22] = 4.4114136710078e-02;
        elem_ref[23] = 4.4114136710078e-02;
        elem_ref[24] = 3.7841361566331e-02;
        elem_ref[25] = 2.4664761777965e-02;
        elem_ref[26] = 4.1790475460801e-03;
        elem_ref[27] = 4.7041281127913e-03;
        elem_ref[28] = 2.8983355698209e-02;
        elem_ref[29] = 4.4114136710078e-02;
        elem_ref[30] = 4.9130379707156e-02;
        elem_ref[31] = 4.4114136710078e-02;
        elem_ref[32] = 2.8983355698209e-02;
        elem_ref[33] = 4.7041281127913e-03;
        elem_ref[34] = 5.0605684208266e-03;
        elem_ref[35] = 3.0415917037330e-02;
        elem_ref[36] = 4.4114136710078e-02;
        elem_ref[37] = 4.4114136710078e-02;
        elem_ref[38] = 3.0415917037330e-02;
        elem_ref[39] = 5.0605684208266e-03;
        elem_ref[40] = 5.0605684208266e-03;
        elem_ref[41] = 2.8983355698209e-02;
        elem_ref[42] = 3.7841361566331e-02;
        elem_ref[43] = 2.8983355698209e-02;
        elem_ref[44] = 5.0605684208266e-03;
        elem_ref[45] = 4.7041281127913e-03;
        elem_ref[46] = 2.4664761777965e-02;
        elem_ref[47] = 2.4664761777965e-02;
        elem_ref[48] = 4.7041281127913e-03;
        elem_ref[49] = 4.1790475460801e-03;
        elem_ref[50] = 1.6896350252325e-02;
        elem_ref[51] = 4.1790475460801e-03;
        elem_ref[52] = 3.6269583209455e-03;
        elem_ref[53] = 3.6269583209455e-03;
        elem_ref[54] = 1.1369980678352e-03;
        return;
    }
    else if (pOrder==10){
        elem_ref[0] = 1.1292989350917e-03;
        elem_ref[1] = 3.2017408044491e-03;
        elem_ref[2] = 3.2024063520324e-03;
        elem_ref[3] = 3.3266773184004e-03;
        elem_ref[4] = 3.5267792061938e-03;
        elem_ref[5] = 3.6147987099874e-03;
        elem_ref[6] = 3.5267792061938e-03;
        elem_ref[7] = 3.3266773184005e-03;
        elem_ref[8] = 3.2024063520323e-03;
        elem_ref[9] = 3.2017408044491e-03;
        elem_ref[10] = 1.1292989350917e-03;
        elem_ref[11] = 3.2017408044490e-03;
        elem_ref[12] = 1.3499174605582e-02;
        elem_ref[13] = 1.8459110077058e-02;
        elem_ref[14] = 2.0943494617764e-02;
        elem_ref[15] = 2.2116557419877e-02;
        elem_ref[16] = 2.2116557419877e-02;
        elem_ref[17] = 2.0943494617764e-02;
        elem_ref[18] = 1.8459110077057e-02;
        elem_ref[19] = 1.3499174605582e-02;
        elem_ref[20] = 3.2017408044490e-03;
        elem_ref[21] = 3.2024063520323e-03;
        elem_ref[22] = 1.8459110077058e-02;
        elem_ref[23] = 2.7929871717651e-02;
        elem_ref[24] = 3.2680180737143e-02;
        elem_ref[25] = 3.4143761604225e-02;
        elem_ref[26] = 3.2680180737143e-02;
        elem_ref[27] = 2.7929871717651e-02;
        elem_ref[28] = 1.8459110077058e-02;
        elem_ref[29] = 3.2024063520323e-03;
        elem_ref[30] = 3.3266773184004e-03;
        elem_ref[31] = 2.0943494617764e-02;
        elem_ref[32] = 3.2680180737143e-02;
        elem_ref[33] = 3.8102534694960e-02;
        elem_ref[34] = 3.8102534694960e-02;
        elem_ref[35] = 3.2680180737143e-02;
        elem_ref[36] = 2.0943494617764e-02;
        elem_ref[37] = 3.3266773184005e-03;
        elem_ref[38] = 3.5267792061938e-03;
        elem_ref[39] = 2.2116557419877e-02;
        elem_ref[40] = 3.4143761604225e-02;
        elem_ref[41] = 3.8102534694960e-02;
        elem_ref[42] = 3.4143761604225e-02;
        elem_ref[43] = 2.2116557419877e-02;
        elem_ref[44] = 3.5267792061938e-03;
        elem_ref[45] = 3.6147987099874e-03;
        elem_ref[46] = 2.2116557419877e-02;
        elem_ref[47] = 3.2680180737143e-02;
        elem_ref[48] = 3.2680180737143e-02;
        elem_ref[49] = 2.2116557419877e-02;
        elem_ref[50] = 3.6147987099875e-03;
        elem_ref[51] = 3.5267792061938e-03;
        elem_ref[52] = 2.0943494617764e-02;
        elem_ref[53] = 2.7929871717651e-02;
        elem_ref[54] = 2.0943494617764e-02;
        elem_ref[55] = 3.5267792061938e-03;
        elem_ref[56] = 3.3266773184005e-03;
        elem_ref[57] = 1.8459110077058e-02;
        elem_ref[58] = 1.8459110077058e-02;
        elem_ref[59] = 3.3266773184005e-03;
        elem_ref[60] = 3.2024063520324e-03;
        elem_ref[61] = 1.3499174605582e-02;
        elem_ref[62] = 3.2024063520323e-03;
        elem_ref[63] = 3.2017408044491e-03;
        elem_ref[64] = 3.2017408044490e-03;
        elem_ref[65] = 1.1292989350917e-03;
        return;
    }
    else if (pOrder==11){
        elem_ref[0] = 1.1720837046660e-03;
        elem_ref[1] = 3.0369440513387e-03;
        elem_ref[2] = 2.6259689909246e-03;
        elem_ref[3] = 2.4592144212503e-03;
        elem_ref[4] = 2.5181582977136e-03;
        elem_ref[5] = 2.5963927907757e-03;
        elem_ref[6] = 2.5963927907757e-03;
        elem_ref[7] = 2.5181582977136e-03;
        elem_ref[8] = 2.4592144212503e-03;
        elem_ref[9] = 2.6259689909246e-03;
        elem_ref[10] = 3.0369440513387e-03;
        elem_ref[11] = 1.1720837046660e-03;
        elem_ref[12] = 3.0369440513387e-03;
        elem_ref[13] = 1.1766802537129e-02;
        elem_ref[14] = 1.4847918721243e-02;
        elem_ref[15] = 1.5827523128930e-02;
        elem_ref[16] = 1.6313337577952e-02;
        elem_ref[17] = 1.6494139281948e-02;
        elem_ref[18] = 1.6313337577952e-02;
        elem_ref[19] = 1.5827523128930e-02;
        elem_ref[20] = 1.4847918721243e-02;
        elem_ref[21] = 1.1766802537129e-02;
        elem_ref[22] = 3.0369440513387e-03;
        elem_ref[23] = 2.6259689909246e-03;
        elem_ref[24] = 1.4847918721243e-02;
        elem_ref[25] = 2.1811487091407e-02;
        elem_ref[26] = 2.4848583865622e-02;
        elem_ref[27] = 2.6046713666220e-02;
        elem_ref[28] = 2.6046713666220e-02;
        elem_ref[29] = 2.4848583865622e-02;
        elem_ref[30] = 2.1811487091407e-02;
        elem_ref[31] = 1.4847918721243e-02;
        elem_ref[32] = 2.6259689909246e-03;
        elem_ref[33] = 2.4592144212503e-03;
        elem_ref[34] = 1.5827523128930e-02;
        elem_ref[35] = 2.4848583865622e-02;
        elem_ref[36] = 2.9269829791400e-02;
        elem_ref[37] = 3.0577479902848e-02;
        elem_ref[38] = 2.9269829791400e-02;
        elem_ref[39] = 2.4848583865622e-02;
        elem_ref[40] = 1.5827523128930e-02;
        elem_ref[41] = 2.4592144212502e-03;
        elem_ref[42] = 2.5181582977136e-03;
        elem_ref[43] = 1.6313337577952e-02;
        elem_ref[44] = 2.6046713666220e-02;
        elem_ref[45] = 3.0577479902847e-02;
        elem_ref[46] = 3.0577479902848e-02;
        elem_ref[47] = 2.6046713666220e-02;
        elem_ref[48] = 1.6313337577952e-02;
        elem_ref[49] = 2.5181582977136e-03;
        elem_ref[50] = 2.5963927907757e-03;
        elem_ref[51] = 1.6494139281948e-02;
        elem_ref[52] = 2.6046713666220e-02;
        elem_ref[53] = 2.9269829791400e-02;
        elem_ref[54] = 2.6046713666220e-02;
        elem_ref[55] = 1.6494139281948e-02;
        elem_ref[56] = 2.5963927907757e-03;
        elem_ref[57] = 2.5963927907757e-03;
        elem_ref[58] = 1.6313337577952e-02;
        elem_ref[59] = 2.4848583865622e-02;
        elem_ref[60] = 2.4848583865622e-02;
        elem_ref[61] = 1.6313337577952e-02;
        elem_ref[62] = 2.5963927907757e-03;
        elem_ref[63] = 2.5181582977137e-03;
        elem_ref[64] = 1.5827523128930e-02;
        elem_ref[65] = 2.1811487091407e-02;
        elem_ref[66] = 1.5827523128930e-02;
        elem_ref[67] = 2.5181582977136e-03;
        elem_ref[68] = 2.4592144212503e-03;
        elem_ref[69] = 1.4847918721243e-02;
        elem_ref[70] = 1.4847918721243e-02;
        elem_ref[71] = 2.4592144212503e-03;
        elem_ref[72] = 2.6259689909246e-03;
        elem_ref[73] = 1.1766802537129e-02;
        elem_ref[74] = 2.6259689909246e-03;
        elem_ref[75] = 3.0369440513387e-03;
        elem_ref[76] = 3.0369440513387e-03;
        elem_ref[77] = 1.1720837046660e-03;
        return;
    }
    else if (pOrder==12){
        elem_ref[0] = 1.3026033886310e-03;
        elem_ref[1] = 3.1618997971242e-03;
        elem_ref[2] = 2.3861819607347e-03;
        elem_ref[3] = 1.9363033916384e-03;
        elem_ref[4] = 1.8360893210666e-03;
        elem_ref[5] = 1.8565546395653e-03;
        elem_ref[6] = 1.8742338198510e-03;
        elem_ref[7] = 1.8565546395653e-03;
        elem_ref[8] = 1.8360893210666e-03;
        elem_ref[9] = 1.9363033916384e-03;
        elem_ref[10] = 2.3861819607347e-03;
        elem_ref[11] = 3.1618997971241e-03;
        elem_ref[12] = 1.3026033886310e-03;
        elem_ref[13] = 3.1618997971242e-03;
        elem_ref[14] = 1.1363263217014e-02;
        elem_ref[15] = 1.3123515991294e-02;
        elem_ref[16] = 1.2731400618999e-02;
        elem_ref[17] = 1.2325773836639e-02;
        elem_ref[18] = 1.2194523570630e-02;
        elem_ref[19] = 1.2194523570630e-02;
        elem_ref[20] = 1.2325773836639e-02;
        elem_ref[21] = 1.2731400618999e-02;
        elem_ref[22] = 1.3123515991294e-02;
        elem_ref[23] = 1.1363263217014e-02;
        elem_ref[24] = 3.1618997971242e-03;
        elem_ref[25] = 2.3861819607347e-03;
        elem_ref[26] = 1.3123515991295e-02;
        elem_ref[27] = 1.8354970241688e-02;
        elem_ref[28] = 1.9682312512543e-02;
        elem_ref[29] = 1.9923255630729e-02;
        elem_ref[30] = 1.9971947726529e-02;
        elem_ref[31] = 1.9923255630729e-02;
        elem_ref[32] = 1.9682312512544e-02;
        elem_ref[33] = 1.8354970241688e-02;
        elem_ref[34] = 1.3123515991294e-02;
        elem_ref[35] = 2.3861819607346e-03;
        elem_ref[36] = 1.9363033916384e-03;
        elem_ref[37] = 1.2731400618999e-02;
        elem_ref[38] = 1.9682312512544e-02;
        elem_ref[39] = 2.2607931893411e-02;
        elem_ref[40] = 2.3649416015745e-02;
        elem_ref[41] = 2.3649416015745e-02;
        elem_ref[42] = 2.2607931893411e-02;
        elem_ref[43] = 1.9682312512543e-02;
        elem_ref[44] = 1.2731400618999e-02;
        elem_ref[45] = 1.9363033916383e-03;
        elem_ref[46] = 1.8360893210666e-03;
        elem_ref[47] = 1.2325773836639e-02;
        elem_ref[48] = 1.9923255630729e-02;
        elem_ref[49] = 2.3649416015745e-02;
        elem_ref[50] = 2.4731785418385e-02;
        elem_ref[51] = 2.3649416015745e-02;
        elem_ref[52] = 1.9923255630729e-02;
        elem_ref[53] = 1.2325773836638e-02;
        elem_ref[54] = 1.8360893210666e-03;
        elem_ref[55] = 1.8565546395653e-03;
        elem_ref[56] = 1.2194523570630e-02;
        elem_ref[57] = 1.9971947726529e-02;
        elem_ref[58] = 2.3649416015745e-02;
        elem_ref[59] = 2.3649416015745e-02;
        elem_ref[60] = 1.9971947726529e-02;
        elem_ref[61] = 1.2194523570630e-02;
        elem_ref[62] = 1.8565546395653e-03;
        elem_ref[63] = 1.8742338198510e-03;
        elem_ref[64] = 1.2194523570630e-02;
        elem_ref[65] = 1.9923255630729e-02;
        elem_ref[66] = 2.2607931893411e-02;
        elem_ref[67] = 1.9923255630729e-02;
        elem_ref[68] = 1.2194523570630e-02;
        elem_ref[69] = 1.8742338198510e-03;
        elem_ref[70] = 1.8565546395653e-03;
        elem_ref[71] = 1.2325773836639e-02;
        elem_ref[72] = 1.9682312512543e-02;
        elem_ref[73] = 1.9682312512543e-02;
        elem_ref[74] = 1.2325773836638e-02;
        elem_ref[75] = 1.8565546395653e-03;
        elem_ref[76] = 1.8360893210666e-03;
        elem_ref[77] = 1.2731400618999e-02;
        elem_ref[78] = 1.8354970241688e-02;
        elem_ref[79] = 1.2731400618999e-02;
        elem_ref[80] = 1.8360893210666e-03;
        elem_ref[81] = 1.9363033916384e-03;
        elem_ref[82] = 1.3123515991294e-02;
        elem_ref[83] = 1.3123515991295e-02;
        elem_ref[84] = 1.9363033916383e-03;
        elem_ref[85] = 2.3861819607347e-03;
        elem_ref[86] = 1.1363263217014e-02;
        elem_ref[87] = 2.3861819607347e-03;
        elem_ref[88] = 3.1618997971241e-03;
        elem_ref[89] = 3.1618997971241e-03;
        elem_ref[90] = 1.3026033886310e-03;
        return;
    }
    else if (pOrder==13){
        elem_ref[0] = 1.4510774107324e-03;
        elem_ref[1] = 3.3695774855912e-03;
        elem_ref[2] = 2.2644480836143e-03;
        elem_ref[3] = 1.5906994456359e-03;
        elem_ref[4] = 1.3724622982052e-03;
        elem_ref[5] = 1.3415146804770e-03;
        elem_ref[6] = 1.3507269136417e-03;
        elem_ref[7] = 1.3507269136417e-03;
        elem_ref[8] = 1.3415146804770e-03;
        elem_ref[9] = 1.3724622982052e-03;
        elem_ref[10] = 1.5906994456359e-03;
        elem_ref[11] = 2.2644480836143e-03;
        elem_ref[12] = 3.3695774855912e-03;
        elem_ref[13] = 1.4510774107324e-03;
        elem_ref[14] = 3.3695774855911e-03;
        elem_ref[15] = 1.1583394475687e-02;
        elem_ref[16] = 1.2394782403604e-02;
        elem_ref[17] = 1.0866630477017e-02;
        elem_ref[18] = 9.6525328259971e-03;
        elem_ref[19] = 9.0980761511986e-03;
        elem_ref[20] = 8.9565608378847e-03;
        elem_ref[21] = 9.0980761511986e-03;
        elem_ref[22] = 9.6525328259972e-03;
        elem_ref[23] = 1.0866630477017e-02;
        elem_ref[24] = 1.2394782403604e-02;
        elem_ref[25] = 1.1583394475688e-02;
        elem_ref[26] = 3.3695774855912e-03;
        elem_ref[27] = 2.2644480836143e-03;
        elem_ref[28] = 1.2394782403604e-02;
        elem_ref[29] = 1.6667026759760e-02;
        elem_ref[30] = 1.6599444954843e-02;
        elem_ref[31] = 1.5730710752579e-02;
        elem_ref[32] = 1.5254159326220e-02;
        elem_ref[33] = 1.5254159326220e-02;
        elem_ref[34] = 1.5730710752579e-02;
        elem_ref[35] = 1.6599444954843e-02;
        elem_ref[36] = 1.6667026759760e-02;
        elem_ref[37] = 1.2394782403604e-02;
        elem_ref[38] = 2.2644480836143e-03;
        elem_ref[39] = 1.5906994456359e-03;
        elem_ref[40] = 1.0866630477017e-02;
        elem_ref[41] = 1.6599444954843e-02;
        elem_ref[42] = 1.8219002245613e-02;
        elem_ref[43] = 1.8434598970846e-02;
        elem_ref[44] = 1.8441622015995e-02;
        elem_ref[45] = 1.8434598970846e-02;
        elem_ref[46] = 1.8219002245613e-02;
        elem_ref[47] = 1.6599444954843e-02;
        elem_ref[48] = 1.0866630477017e-02;
        elem_ref[49] = 1.5906994456358e-03;
        elem_ref[50] = 1.3724622982052e-03;
        elem_ref[51] = 9.6525328259971e-03;
        elem_ref[52] = 1.5730710752579e-02;
        elem_ref[53] = 1.8434598970846e-02;
        elem_ref[54] = 1.9373920048718e-02;
        elem_ref[55] = 1.9373920048718e-02;
        elem_ref[56] = 1.8434598970846e-02;
        elem_ref[57] = 1.5730710752579e-02;
        elem_ref[58] = 9.6525328259971e-03;
        elem_ref[59] = 1.3724622982052e-03;
        elem_ref[60] = 1.3415146804770e-03;
        elem_ref[61] = 9.0980761511986e-03;
        elem_ref[62] = 1.5254159326220e-02;
        elem_ref[63] = 1.8441622015995e-02;
        elem_ref[64] = 1.9373920048718e-02;
        elem_ref[65] = 1.8441622015995e-02;
        elem_ref[66] = 1.5254159326220e-02;
        elem_ref[67] = 9.0980761511986e-03;
        elem_ref[68] = 1.3415146804770e-03;
        elem_ref[69] = 1.3507269136417e-03;
        elem_ref[70] = 8.9565608378847e-03;
        elem_ref[71] = 1.5254159326220e-02;
        elem_ref[72] = 1.8434598970846e-02;
        elem_ref[73] = 1.8434598970846e-02;
        elem_ref[74] = 1.5254159326220e-02;
        elem_ref[75] = 8.9565608378846e-03;
        elem_ref[76] = 1.3507269136417e-03;
        elem_ref[77] = 1.3507269136417e-03;
        elem_ref[78] = 9.0980761511986e-03;
        elem_ref[79] = 1.5730710752579e-02;
        elem_ref[80] = 1.8219002245613e-02;
        elem_ref[81] = 1.5730710752579e-02;
        elem_ref[82] = 9.0980761511986e-03;
        elem_ref[83] = 1.3507269136417e-03;
        elem_ref[84] = 1.3415146804770e-03;
        elem_ref[85] = 9.6525328259972e-03;
        elem_ref[86] = 1.6599444954843e-02;
        elem_ref[87] = 1.6599444954843e-02;
        elem_ref[88] = 9.6525328259971e-03;
        elem_ref[89] = 1.3415146804770e-03;
        elem_ref[90] = 1.3724622982052e-03;
        elem_ref[91] = 1.0866630477017e-02;
        elem_ref[92] = 1.6667026759760e-02;
        elem_ref[93] = 1.0866630477017e-02;
        elem_ref[94] = 1.3724622982052e-03;
        elem_ref[95] = 1.5906994456359e-03;
        elem_ref[96] = 1.2394782403604e-02;
        elem_ref[97] = 1.2394782403604e-02;
        elem_ref[98] = 1.5906994456359e-03;
        elem_ref[99] = 2.2644480836143e-03;
        elem_ref[100] = 1.1583394475687e-02;
        elem_ref[101] = 2.2644480836142e-03;
        elem_ref[102] = 3.3695774855911e-03;
        elem_ref[103] = 3.3695774855911e-03;
        elem_ref[104] = 1.4510774107324e-03;
        return;
    }
    else if (pOrder==14){
        elem_ref[0] = 1.6564148520808e-03;
        elem_ref[1] = 3.7433176559219e-03;
        elem_ref[2] = 2.3134003852590e-03;
        elem_ref[3] = 1.4071880015498e-03;
        elem_ref[4] = 1.0649593576179e-03;
        elem_ref[5] = 9.7468603184516e-04;
        elem_ref[6] = 9.6061787562926e-04;
        elem_ref[7] = 9.6025413616269e-04;
        elem_ref[8] = 9.6061787562926e-04;
        elem_ref[9] = 9.7468603184516e-04;
        elem_ref[10] = 1.0649593576179e-03;
        elem_ref[11] = 1.4071880015498e-03;
        elem_ref[12] = 2.3134003852589e-03;
        elem_ref[13] = 3.7433176559219e-03;
        elem_ref[14] = 1.6564148520807e-03;
        elem_ref[15] = 3.7433176559219e-03;
        elem_ref[16] = 1.2491977188062e-02;
        elem_ref[17] = 1.2616779951652e-02;
        elem_ref[18] = 1.0022280101669e-02;
        elem_ref[19] = 7.9664238632027e-03;
        elem_ref[20] = 6.9059457493424e-03;
        elem_ref[21] = 6.5073144703747e-03;
        elem_ref[22] = 6.5073144703747e-03;
        elem_ref[23] = 6.9059457493424e-03;
        elem_ref[24] = 7.9664238632027e-03;
        elem_ref[25] = 1.0022280101669e-02;
        elem_ref[26] = 1.2616779951653e-02;
        elem_ref[27] = 1.2491977188062e-02;
        elem_ref[28] = 3.7433176559217e-03;
        elem_ref[29] = 2.3134003852589e-03;
        elem_ref[30] = 1.2616779951653e-02;
        elem_ref[31] = 1.6365985708002e-02;
        elem_ref[32] = 1.5028131820672e-02;
        elem_ref[33] = 1.2960085988264e-02;
        elem_ref[34] = 1.1709937813123e-02;
        elem_ref[35] = 1.1329136860813e-02;
        elem_ref[36] = 1.1709937813123e-02;
        elem_ref[37] = 1.2960085988264e-02;
        elem_ref[38] = 1.5028131820672e-02;
        elem_ref[39] = 1.6365985708002e-02;
        elem_ref[40] = 1.2616779951652e-02;
        elem_ref[41] = 2.3134003852590e-03;
        elem_ref[42] = 1.4071880015498e-03;
        elem_ref[43] = 1.0022280101670e-02;
        elem_ref[44] = 1.5028131820672e-02;
        elem_ref[45] = 1.5418846831984e-02;
        elem_ref[46] = 1.4556003014437e-02;
        elem_ref[47] = 1.4004141363518e-02;
        elem_ref[48] = 1.4004141363518e-02;
        elem_ref[49] = 1.4556003014437e-02;
        elem_ref[50] = 1.5418846831984e-02;
        elem_ref[51] = 1.5028131820672e-02;
        elem_ref[52] = 1.0022280101669e-02;
        elem_ref[53] = 1.4071880015498e-03;
        elem_ref[54] = 1.0649593576179e-03;
        elem_ref[55] = 7.9664238632027e-03;
        elem_ref[56] = 1.2960085988264e-02;
        elem_ref[57] = 1.4556003014437e-02;
        elem_ref[58] = 1.4806525006992e-02;
        elem_ref[59] = 1.4821765861077e-02;
        elem_ref[60] = 1.4806525006992e-02;
        elem_ref[61] = 1.4556003014437e-02;
        elem_ref[62] = 1.2960085988264e-02;
        elem_ref[63] = 7.9664238632026e-03;
        elem_ref[64] = 1.0649593576179e-03;
        elem_ref[65] = 9.7468603184516e-04;
        elem_ref[66] = 6.9059457493424e-03;
        elem_ref[67] = 1.1709937813123e-02;
        elem_ref[68] = 1.4004141363518e-02;
        elem_ref[69] = 1.4821765861077e-02;
        elem_ref[70] = 1.4821765861077e-02;
        elem_ref[71] = 1.4004141363518e-02;
        elem_ref[72] = 1.1709937813123e-02;
        elem_ref[73] = 6.9059457493424e-03;
        elem_ref[74] = 9.7468603184516e-04;
        elem_ref[75] = 9.6061787562926e-04;
        elem_ref[76] = 6.5073144703747e-03;
        elem_ref[77] = 1.1329136860813e-02;
        elem_ref[78] = 1.4004141363518e-02;
        elem_ref[79] = 1.4806525006992e-02;
        elem_ref[80] = 1.4004141363518e-02;
        elem_ref[81] = 1.1329136860813e-02;
        elem_ref[82] = 6.5073144703747e-03;
        elem_ref[83] = 9.6061787562923e-04;
        elem_ref[84] = 9.6025413616269e-04;
        elem_ref[85] = 6.5073144703747e-03;
        elem_ref[86] = 1.1709937813123e-02;
        elem_ref[87] = 1.4556003014437e-02;
        elem_ref[88] = 1.4556003014437e-02;
        elem_ref[89] = 1.1709937813123e-02;
        elem_ref[90] = 6.5073144703747e-03;
        elem_ref[91] = 9.6025413616273e-04;
        elem_ref[92] = 9.6061787562925e-04;
        elem_ref[93] = 6.9059457493424e-03;
        elem_ref[94] = 1.2960085988264e-02;
        elem_ref[95] = 1.5418846831984e-02;
        elem_ref[96] = 1.2960085988264e-02;
        elem_ref[97] = 6.9059457493425e-03;
        elem_ref[98] = 9.6061787562924e-04;
        elem_ref[99] = 9.7468603184517e-04;
        elem_ref[100] = 7.9664238632027e-03;
        elem_ref[101] = 1.5028131820672e-02;
        elem_ref[102] = 1.5028131820672e-02;
        elem_ref[103] = 7.9664238632027e-03;
        elem_ref[104] = 9.7468603184516e-04;
        elem_ref[105] = 1.0649593576179e-03;
        elem_ref[106] = 1.0022280101669e-02;
        elem_ref[107] = 1.6365985708002e-02;
        elem_ref[108] = 1.0022280101670e-02;
        elem_ref[109] = 1.0649593576179e-03;
        elem_ref[110] = 1.4071880015498e-03;
        elem_ref[111] = 1.2616779951653e-02;
        elem_ref[112] = 1.2616779951653e-02;
        elem_ref[113] = 1.4071880015498e-03;
        elem_ref[114] = 2.3134003852590e-03;
        elem_ref[115] = 1.2491977188062e-02;
        elem_ref[116] = 2.3134003852589e-03;
        elem_ref[117] = 3.7433176559218e-03;
        elem_ref[118] = 3.7433176559218e-03;
        elem_ref[119] = 1.6564148520807e-03;
        return;
    }
      else if (pOrder==15){
        elem_ref[0] = 1.8550444075138e-03;
        elem_ref[1] = 4.1159059653081e-03;
        elem_ref[2] = 2.3803858614255e-03;
        elem_ref[3] = 1.2818886641265e-03;
        elem_ref[4] = 8.4826719289035e-04;
        elem_ref[5] = 7.1518989391147e-04;
        elem_ref[6] = 6.8051731904718e-04;
        elem_ref[7] = 6.7205652457086e-04;
        elem_ref[8] = 6.7205652457087e-04;
        elem_ref[9] = 6.8051731904718e-04;
        elem_ref[10] = 7.1518989391148e-04;
        elem_ref[11] = 8.4826719289035e-04;
        elem_ref[12] = 1.2818886641265e-03;
        elem_ref[13] = 2.3803858614255e-03;
        elem_ref[14] = 4.1159059653081e-03;
        elem_ref[15] = 1.8550444075138e-03;
        elem_ref[16] = 4.1159059653081e-03;
        elem_ref[17] = 1.3583744797521e-02;
        elem_ref[18] = 1.3186931143466e-02;
        elem_ref[19] = 9.6690342158703e-03;
        elem_ref[20] = 6.8886949838031e-03;
        elem_ref[21] = 5.3858896365783e-03;
        elem_ref[22] = 4.7365000027032e-03;
        elem_ref[23] = 4.5627850814847e-03;
        elem_ref[24] = 4.7365000027032e-03;
        elem_ref[25] = 5.3858896365783e-03;
        elem_ref[26] = 6.8886949838031e-03;
        elem_ref[27] = 9.6690342158701e-03;
        elem_ref[28] = 1.3186931143466e-02;
        elem_ref[29] = 1.3583744797520e-02;
        elem_ref[30] = 4.1159059653079e-03;
        elem_ref[31] = 2.3803858614255e-03;
        elem_ref[32] = 1.3186931143467e-02;
        elem_ref[33] = 1.6870116541084e-02;
        elem_ref[34] = 1.4545745207095e-02;
        elem_ref[35] = 1.1368506398555e-02;
        elem_ref[36] = 9.3053646264436e-03;
        elem_ref[37] = 8.4063049118109e-03;
        elem_ref[38] = 8.4063049118109e-03;
        elem_ref[39] = 9.3053646264436e-03;
        elem_ref[40] = 1.1368506398555e-02;
        elem_ref[41] = 1.4545745207095e-02;
        elem_ref[42] = 1.6870116541084e-02;
        elem_ref[43] = 1.3186931143466e-02;
        elem_ref[44] = 2.3803858614256e-03;
        elem_ref[45] = 1.2818886641265e-03;
        elem_ref[46] = 9.6690342158703e-03;
        elem_ref[47] = 1.4545745207095e-02;
        elem_ref[48] = 1.4069192693119e-02;
        elem_ref[49] = 1.2148655582638e-02;
        elem_ref[50] = 1.0825159674528e-02;
        elem_ref[51] = 1.0401497745307e-02;
        elem_ref[52] = 1.0825159674528e-02;
        elem_ref[53] = 1.2148655582638e-02;
        elem_ref[54] = 1.4069192693119e-02;
        elem_ref[55] = 1.4545745207095e-02;
        elem_ref[56] = 9.6690342158702e-03;
        elem_ref[57] = 1.2818886641265e-03;
        elem_ref[58] = 8.4826719289034e-04;
        elem_ref[59] = 6.8886949838031e-03;
        elem_ref[60] = 1.1368506398555e-02;
        elem_ref[61] = 1.2148655582638e-02;
        elem_ref[62] = 1.1594458246578e-02;
        elem_ref[63] = 1.1167625303940e-02;
        elem_ref[64] = 1.1167625303940e-02;
        elem_ref[65] = 1.1594458246578e-02;
        elem_ref[66] = 1.2148655582638e-02;
        elem_ref[67] = 1.1368506398555e-02;
        elem_ref[68] = 6.8886949838030e-03;
        elem_ref[69] = 8.4826719289035e-04;
        elem_ref[70] = 7.1518989391147e-04;
        elem_ref[71] = 5.3858896365782e-03;
        elem_ref[72] = 9.3053646264436e-03;
        elem_ref[73] = 1.0825159674528e-02;
        elem_ref[74] = 1.1167625303940e-02;
        elem_ref[75] = 1.1217742809893e-02;
        elem_ref[76] = 1.1167625303940e-02;
        elem_ref[77] = 1.0825159674528e-02;
        elem_ref[78] = 9.3053646264436e-03;
        elem_ref[79] = 5.3858896365782e-03;
        elem_ref[80] = 7.1518989391147e-04;
        elem_ref[81] = 6.8051731904719e-04;
        elem_ref[82] = 4.7365000027032e-03;
        elem_ref[83] = 8.4063049118109e-03;
        elem_ref[84] = 1.0401497745307e-02;
        elem_ref[85] = 1.1167625303940e-02;
        elem_ref[86] = 1.1167625303940e-02;
        elem_ref[87] = 1.0401497745307e-02;
        elem_ref[88] = 8.4063049118109e-03;
        elem_ref[89] = 4.7365000027032e-03;
        elem_ref[90] = 6.8051731904718e-04;
        elem_ref[91] = 6.7205652457086e-04;
        elem_ref[92] = 4.5627850814846e-03;
        elem_ref[93] = 8.4063049118109e-03;
        elem_ref[94] = 1.0825159674528e-02;
        elem_ref[95] = 1.1594458246578e-02;
        elem_ref[96] = 1.0825159674528e-02;
        elem_ref[97] = 8.4063049118109e-03;
        elem_ref[98] = 4.5627850814846e-03;
        elem_ref[99] = 6.7205652457086e-04;
        elem_ref[100] = 6.7205652457087e-04;
        elem_ref[101] = 4.7365000027032e-03;
        elem_ref[102] = 9.3053646264436e-03;
        elem_ref[103] = 1.2148655582638e-02;
        elem_ref[104] = 1.2148655582638e-02;
        elem_ref[105] = 9.3053646264436e-03;
        elem_ref[106] = 4.7365000027032e-03;
        elem_ref[107] = 6.7205652457086e-04;
        elem_ref[108] = 6.8051731904718e-04;
        elem_ref[109] = 5.3858896365783e-03;
        elem_ref[110] = 1.1368506398555e-02;
        elem_ref[111] = 1.4069192693119e-02;
        elem_ref[112] = 1.1368506398555e-02;
        elem_ref[113] = 5.3858896365782e-03;
        elem_ref[114] = 6.8051731904718e-04;
        elem_ref[115] = 7.1518989391147e-04;
        elem_ref[116] = 6.8886949838031e-03;
        elem_ref[117] = 1.4545745207095e-02;
        elem_ref[118] = 1.4545745207095e-02;
        elem_ref[119] = 6.8886949838030e-03;
        elem_ref[120] = 7.1518989391147e-04;
        elem_ref[121] = 8.4826719289035e-04;
        elem_ref[122] = 9.6690342158703e-03;
        elem_ref[123] = 1.6870116541084e-02;
        elem_ref[124] = 9.6690342158702e-03;
        elem_ref[125] = 8.4826719289034e-04;
        elem_ref[126] = 1.2818886641265e-03;
        elem_ref[127] = 1.3186931143467e-02;
        elem_ref[128] = 1.3186931143467e-02;
        elem_ref[129] = 1.2818886641265e-03;
        elem_ref[130] = 2.3803858614255e-03;
        elem_ref[131] = 1.3583744797521e-02;
        elem_ref[132] = 2.3803858614255e-03;
        elem_ref[133] = 4.1159059653081e-03;
        elem_ref[134] = 4.1159059653079e-03;
        elem_ref[135] = 1.8550444075137e-03;
        return;
    }
*/
}

#endif //DGC0SHELLELEMENTARYTERMS_H_

