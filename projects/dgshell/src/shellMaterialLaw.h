//
// C++ Interface: matrialLaw
//
// Description: Class with definition of materialLaw for shell
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef SHELLMATERIALLAW_H_
#define SHELLMATERIALLAW_H_
#include "mlaw.h"
#include "shellIPVariable.h"
#include "SymmetricPlaneAray.h"//added by shantanu on 27-07-2012
//#include <cstdlib>

// function to generate random factor for fracture strength
// srand initiated in constructor of NonLinearMechSolver
static double frand(const double a,const double b){
  return (rand()/(double)RAND_MAX) * (b-a) + a;
}

class shellMaterialLaw : public materialLaw{
 protected:
  const double _h0; // initial shell thickness
  const double _rho;
  short int _nsimp; // number of Simpson's point for thickness integration. If = 1 no integration but used analytic value
#ifndef SWIG
 public:
  shellMaterialLaw(const int num, const double h, const int nsimp, const double rho,
                   const bool init=true) : materialLaw(num,init), _h0(h), _rho(rho)
  {
    // Ensure an odd number of simpson's point
    if(nsimp%2 == 0)
      _nsimp = nsimp+1;
    else
      _nsimp = nsimp;
  }
  shellMaterialLaw(const shellMaterialLaw &source) : materialLaw(source), _h0(source._h0), _nsimp(source._nsimp),
                                                      _rho(source._rho){}
  ~shellMaterialLaw(){}
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const=0;
  // To allow initialization of bulk ip in case of fracture
  virtual void createIPVariable(IPVariable* &ipv,const MElement *ele=NULL, const int nbFF_=0) const=0;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw)=0;
  virtual short int getNumberofSimpsonPoint() const{return _nsimp;}
  virtual double getThickness() const{return _h0;}
  virtual double density() const{return _rho*_h0;} // return the mass by unit thickness
  // As to be defined for explicit scheme. If law for implicit only
  // please define this function with return 0
  virtual double soundSpeed() const=0;
  // If law enable fracture the bool checkfrac allow to detect fracture. This bool is unused otherwise
  // The bool is necessery for computation of matrix by perturbation in this case the check of fracture
  // has not to be performed.
  virtual void stress(IPStateBase*, bool stiff, const bool checkfrac=true)=0;
  virtual void shellBendingStiffness(const IPVariableShell *ipv, LinearElasticShellHookeTensor &Hbend) const=0;
  virtual void shellMembraneStiffness(const IPVariableShell *ipv, LinearElasticShellHookeTensor &Htens) const=0;
  virtual void shellShearingStiffness(const IPVariableShell *ipv, linearElasticShellShearingHookeTensor &Htens) const=0;
// give a default implementation for linear case. this function is overloaded in dgNonLinearShellMaterialLaw for the non linear case
  virtual void reduction(const IPStateBase *ips,std::vector<SVector3> &n_,
                         std::vector<SVector3> &m_,bool fdg=true) const;
#endif
};
// class for linear elastic law
class linearElasticShellLaw : public shellMaterialLaw{
 protected :
  const double _E; // YoungModulus
  const double _nu; // Poisson ratio
  const double _G; // shearing modulus
  const double C11;
  LinearElasticShellHookeTensor H;
  int _ns; // integer for stress computation =2 if _nsimp=1 otherwise = _nsimp
 public :
  linearElasticShellLaw(const int num, const double E,
                        const double nu, const double thick,const int nsimp,const double rho=0.);
#ifndef SWIG
  ~linearElasticShellLaw(){}
  linearElasticShellLaw( const linearElasticShellLaw &source);
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // nothing to do for this law
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv,const MElement *ele=NULL, const int nbFF_=0) const;
  virtual double getYoung() const{return _E;}
  virtual double getPoisson() const{return _nu;}
  virtual double soundSpeed()const;
  virtual void stress(IPStateBase *ips, bool stiff, const bool checkfrac=true);
  virtual void shellBendingStiffness(const IPVariableShell *ipv, LinearElasticShellHookeTensor &Hbend) const;
  virtual void shellMembraneStiffness(const IPVariableShell *ipv, LinearElasticShellHookeTensor &Htens) const;
  virtual void shellShearingStiffness(const IPVariableShell *ipv, linearElasticShellShearingHookeTensor &Htens) const;
  virtual matname getType() const{return materialLaw::linearElasticPlaneStress;}
#endif
};

class linearElasticOrthotropicShellLaw : public shellMaterialLaw{
 protected:
  double _Ex,_Ey,_Ez,_nuxy,_nuyx,_nuxz,_nuzx,_nuyz,_nuzy,_muxy,_muxz,_muyz,_D;
  // rotation angle
  double _theta,_phi,_psi;
  rotMatrix _R;
  hookeTensor _H;
//  hookeTensor _Hplane;
  int _ns; // int for stress computation if _nsimp=1 _ns=2 otherwise = _nsimp
  // Create the Hooke tensor in plane stress
//  void initHplane();
  // Because it is not possible to initialize all parameters with lua
  void initD();
 public:
  linearElasticOrthotropicShellLaw(const int num,const double rho,const double h0,const int nsimp,
                                   const double Ex, const double Ey, const double Ez, const double nuxy,
                                   const double nuxz,const double nuyz, const double muxy, const double muxz,
                                   const double muyz,const double theta, const double phi,
                                   const double psi);
  // For lua as the maximal number of component are not enough
  linearElasticOrthotropicShellLaw(const int num,const double rho,const double h0, const int nsimp);
  linearElasticOrthotropicShellLaw(const linearElasticOrthotropicShellLaw &source);
  ~linearElasticOrthotropicShellLaw(){}
  void setYoung(const double Ex, const double Ey, const double Ez);
  void setMu(const double muxy, const double muxz, const double muyz);
  void setPoisson(const double nuxy, const double nuxz, const double nuyz);
  void setRotation(const double theta, const double phi, const double psi);
#ifndef SWIG
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){} // nothing to do for this law
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv,const MElement *ele=NULL, const int nbFF_=0) const;
  double getYoungMax() const;
  double getPoissonMax() const;
  virtual double soundSpeed() const;
  virtual void stress(IPStateBase *ips, bool stiff, const bool checkfrac=true);
  virtual void shellBendingStiffness(const IPVariableShell *ipv, LinearElasticShellHookeTensor &Hbend) const;
  virtual void shellMembraneStiffness(const IPVariableShell *ipv, LinearElasticShellHookeTensor &Htens) const;
  virtual void shellShearingStiffness(const IPVariableShell *ipv, linearElasticShellShearingHookeTensor &Htens) const;
  virtual matname getType() const {return materialLaw::linearElasticOrthotropicPlaneStress;}
  // overload reduction stress
  virtual void reduction(const IPStateBase *ips,std::vector<SVector3> &n_,
                         std::vector<SVector3> &m_,bool fdg=true) const;
#endif
};

class linearElasticShellLawWithDamage : public linearElasticShellLaw{
 protected :
  double _D0; // Initial damage coefficient
  double _Dc; // Failure limite
  double _Yc;// Energy related to a deformation threshold
  double _halfm;
 public :
  linearElasticShellLawWithDamage(const int num, const double E,const double nu, const double thick, const int nsimp, const double Yc,
                                  const double D0, const double Dc, const double m, const double rho=0.);
#ifndef SWIG
  ~linearElasticShellLawWithDamage(){}
  linearElasticShellLawWithDamage( const linearElasticShellLawWithDamage &source);
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){} // nothing to do for this law
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt=0) const;
  virtual void createIPVariable(IPVariable* &ipv,const MElement *ele=NULL, const int nbFF_=0) const;
  virtual void stress(IPStateBase *ips, bool stiff, const bool checkfrac=true);
  virtual double getDamage0() {return _D0;}
// The three next function should be defined to compute analytical stiffness.
// Be aware that for compatibility and stability undammaged stiffness has to be returned
//  virtual void shellBendingStiffness(const IPVariableShell *ipv, LinearElasticShellHookeTensor &Hbend) const;
//  virtual void shellMembraneStiffness(const IPVariableShell *ipv, LinearElasticShellHookeTensor &Htens) const;
//  virtual void shellShearingStiffness(const IPVariableShell *ipv, linearElasticShellShearingHookeTensor &Htens) const;
  virtual matname getType() const {return materialLaw::linearElasticPlaneStressWithDamage;}
#endif
};
// cohesive law for shell
class shellCohesiveLaw : public materialLaw{
 protected :
  // All cohesive law must have at least these four variable
  const double _Gc;
  const double _sigmac;
  const double _beta;
  const double _mu;
  const double _fscmin,_fscmax; // Boundary value for uncertainty on sigma_c (default min = max = 1)
  virtual void createIPVfrac(const double smax, const double sI, const double tauII, const bool ift,IPVariableShell *ipvcur, IPVariableShell *ipvprev)const=0; // initialize the storage in fracture is detected
#ifndef SWIG
 public :
  virtual void initDataFromLaw(const IPVariable *ipvlaw,IPVariable *ipv) const=0;
  shellCohesiveLaw(const int num, const double Gc, const double sigmac, const double beta, const double mu,
                   const double fscmin=1.,const double fscmax=1.,const bool init=true);
  shellCohesiveLaw(const shellCohesiveLaw &source);
  ~shellCohesiveLaw(){}
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw)=0;
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const=0;
  virtual void createIPVariable(IPVariable* &ipv,const MElement *ele=NULL, const int nbFF_=0) const=0;
  // get operation
  double getGc() const{return _Gc;}
  double getSigmac() const{return _sigmac;}
  double getBeta() const{return _beta;}
  double getMu() const{return _mu;}
//  double getFractureStrengthFactorMin() const{return _fscmin;}
//  double getFractureStrengthFactorMax() const{return _fscmax;}
  virtual double getFractureStrengthFactor() const{return frand(_fscmin,_fscmax);}
  // onto this form to compute easily matrix by perturbation
  virtual matname getType() const{return materialLaw::cohesiveLaw;}
  virtual const bool checkBroken(IPVariableShell *ipvcur, IPVariableShell *ipvprev) const=0;
  virtual void reduction(IPLinearCohesiveShell *ipvcur, const IPLinearCohesiveShell *ipvprev,
                          reductionElement &nalpha, reductionElement &malpha) const=0;
  virtual void reduction(IPLinearCohesiveShell *ipv,const IPLinearCohesiveShell *ipvprev,
                          const double deltan,const double deltat,
                          const double delta,reductionElement &nhat, reductionElement &mhat) const=0;
  virtual double soundSpeed() const
  {
    Msg::Warning("Use a cohesive law to determine sound speed ?? value =1");
    return 1;
  }
  // default for cohesive law full broken if delta > delta_c
  virtual bool fullBroken(const IPLinearCohesiveShell *ipv) const
  {
    if((fabs(ipv->getDelta()) >= fabs(ipv->getDeltac())) and (ipv->getSigmac() !=0.))  return true; // second condition remove initially broken element
    return false;
  }
#endif
};

class shellLinearCohesiveLaw : public shellCohesiveLaw{
 protected:
  const double _betaexpm2;
  const double _cohesive_law_tol;//variable added by shantanu on 21-03-2013, to be used to avoid unloading in cohesive law
  virtual void createIPVfrac(const double smax,const double sI, const double tauII, const bool ift,IPVariableShell *ipvcur, IPVariableShell *ipvprev)const;
#ifndef SWIG
  virtual void reduction(const reductionElement &m0, const reductionElement &n0, const double delta,
                            const double delta_max, const double deltan, const double deltat, const double deltac, const bool tension,
                            reductionElement &nhatmean, reductionElement &mhatmean) const;
#endif
 public:
  virtual void initDataFromLaw(const IPVariable *ipvlaw,IPVariable *ipv) const;
  shellLinearCohesiveLaw(const int num, const double Gc, const double sigmac, const double beta, const double mu,
                         const double fractureStrengthFactorMin = 1., const double fractureStrengthFactorMax = 1., const double cohesive_law_tol = 0.0);
#ifndef SWIG
  shellLinearCohesiveLaw(const shellLinearCohesiveLaw &source);
  ~shellLinearCohesiveLaw(){}
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){} // nothing to do for this law
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt=0) const;
  virtual void createIPVariable(IPVariable* &ipv,const MElement *ele=NULL, const int nbFF_=0) const;
  virtual const bool checkBroken(IPVariableShell *ipvcur, IPVariableShell *ipvprev) const;
  virtual void reduction(IPLinearCohesiveShell *ipvcur, const IPLinearCohesiveShell *ipvprev,
                          reductionElement &nalpha, reductionElement &malpha) const;
  virtual void reduction(IPLinearCohesiveShell *ipv,const IPLinearCohesiveShell *ipvprev,
                          const double deltan,const double deltat,
                          const double delta,reductionElement &nhat, reductionElement &mhat) const;
#endif
};

class shellWeibullLinearCohesiveLaw: public shellLinearCohesiveLaw{
 protected:
  const double _invm;
  const double _sc0;
  // scmin is stored in cohesive law = _sigmac
  virtual void createIPVfrac(const double smax,const double sI, const double tauII, const bool ift,IPVariableShell *ipvcur, IPVariableShell *ipvprev)const; // Gc is modify to ensure a constant time of opening whatever sigmac is
 public:
  shellWeibullLinearCohesiveLaw(const int num, const double Gc, const double sigmacmin,const double beta,const double mu,
                                const double expm,const double sc0);
  shellWeibullLinearCohesiveLaw(const shellWeibullLinearCohesiveLaw &source);
 #ifndef SWIG
  // return the strength factor. As _sigmac == _sigmacmin the factor is necesseraly > 1
  virtual double getFractureStrengthFactor() const;
 #endif // SWIG
};

class shellWeibullZhouLinearCohesiveLaw: public shellLinearCohesiveLaw{
 protected:
  const double _invm;
  // sc0 is stored in cohesive law = _sigmac
  virtual void createIPVfrac(const double smax,const double sI, const double tauII, const bool ift,IPVariableShell *ipvcur, IPVariableShell *ipvprev)const; // Gc is modify to ensure a constant time of opening whatever sigmac is
 public:
  shellWeibullZhouLinearCohesiveLaw(const int num, const double Gc, const double sc0,const double beta,const double mu,
                                    const double expm);
  shellWeibullZhouLinearCohesiveLaw(const shellWeibullZhouLinearCohesiveLaw &source);
 #ifndef SWIG
  // return the strength factor. As _sigmac == _sigmacmin the factor is necesseraly > 1
  virtual double getFractureStrengthFactor() const;
 #endif // SWIG
};

class shellLinearCohesiveLawWithOrientation : public shellLinearCohesiveLaw{
 protected:
   double _theta, _phi, _psi;
   double _sigmac100,_sigmac110,_sigmac111, _Gc100,_Gc110,_Gc111;
   rotMatrix  _Rt;//this is the inverse (transpose) of original transformation matrix (shantanu)
   symmetry_planes _symmetry_planes;//added by shantanu on 27-07-2012
 public:
  virtual void initDataFromLaw(const IPVariable *ipvlaw,IPVariable *ipv) const;
  shellLinearCohesiveLawWithOrientation(const int num, const double Gc100, const double Gc110, const double Gc111,
                                               const double sigmac100,const double sigmac110,const double sigmac111,
                                               const double beta,const double mu, double theta, double phi, double psi,
                                               const double fractureStrengthFactorMin = 1., const double fractureStrengthFactorMax = 1., const double _cohesive_law_tol = 0.0);
#ifndef SWIG
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const;
  virtual void createIPVariable(IPVariable* &ipv,const MElement *ele=NULL, const int nbFF_=0) const;
  shellLinearCohesiveLawWithOrientation(const shellLinearCohesiveLawWithOrientation &source);
  double dot_prod(double *&, double *&) const;//added by shantanu on 30-07-2012
   ~shellLinearCohesiveLawWithOrientation(){}
  virtual const bool checkBroken(IPVariableShell *ipvcur, IPVariableShell *ipvprev) const;
#endif
};

class shellLinearCohesiveLawWithDamage : public shellLinearCohesiveLaw{
 // The value of Dc is store in shellCohesiveLaw::sigmac !!!
 public:
  shellLinearCohesiveLawWithDamage(const int num, const double Gc, const double Dc, const double beta, const double mu,
                                   const double damageFactorMin = 1., const double damageFactorMax = 1.);
#ifndef SWIG
  shellLinearCohesiveLawWithDamage(const shellLinearCohesiveLawWithDamage &source);
  ~shellLinearCohesiveLawWithDamage(){}
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){} // nothing to do for this law
  virtual const bool checkBroken(IPVariableShell *ipvcur, IPVariableShell *ipvprev) const;
#endif
};

#endif // SHELLMATERIALLAW_H_
