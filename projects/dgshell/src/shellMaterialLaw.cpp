//
// C++ Interface: matrialLaw
//
// Description: Class with definition of materialLaw for shell
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "shellMaterialLaw.h"
#include "ipstate.h"
#include "reduction.h"
#include "SimpsonIntegrationRule.h"
#include "math.h"

static void alphaTensprodSym(const double alpha,const SVector3 &a,const SVector3 &b,tab6& msym)
{
  msym[0]+=2*alpha*a[0]*b[0];
  msym[1]+=2*alpha*a[1]*b[1];
  msym[2]+=2*alpha*a[2]*b[2];
  msym[3]+=alpha*(a[0]*b[1]+a[1]*b[0]);
  msym[4]+=alpha*(a[1]*b[2]+a[2]*b[1]);
  msym[5]+=alpha*(a[0]*b[2]+a[2]*b[0]);
}

static void alphaTensprodSym(const double alpha,const SVector3 &a,tab6& msym)
{
  msym[0]+=alpha*a[0]*a[0];
  msym[1]+=alpha*a[1]*a[1];
  msym[2]+=alpha*a[2]*a[2];
  msym[3]+=alpha*a[0]*a[1];
  msym[4]+=alpha*a[1]*a[2];
  msym[5]+=alpha*a[0]*a[2];
}

static void matvectprod(const fullMatrix<double> &m,const SVector3 &a,SVector3 &b)
{
  for(int i=0;i<3;i++){
    b(i)=0.;
    for(int j=0;j<3;j++){
      b(i)+= m(i,j)*a(j);
    }
  }
}

void shellMaterialLaw::reduction(const IPStateBase *ips,std::vector<SVector3> &n_,
                         std::vector<SVector3> &m_,bool fdg) const
{
  n_.resize(3); // FIX THIS (good resize outside) !!
  reductionElement nalpha,malpha; // make cache data ??
  const IPVariableShell *ipv = static_cast<const IPVariableShell*>(ips->getState(IPStateBase::current));
  const shellLocalBasisBulk *lb = static_cast<const shellLocalBasisBulk*>(ipv->getshellLocalBasis());
  const double h = ipv->getThickness();
  if(_nsimp==1)
  {
    if(fdg){
      nalpha(0,0) = h*ipv->getSigma(0,0); // stressMembrane[0];
      nalpha(0,1) = nalpha(1,0) = h*ipv->getSigma(0,3); //stressMembrane[3];
      nalpha(1,1) = h*ipv->getSigma(0,1); //stressMembrane[1];
    }
    // compute malpha
    double hcubdiv12 = h*h*h/12.;
    malpha(0,0) = hcubdiv12*ipv->getSigma(1,0); //stressBending[0];
    malpha(0,1) = malpha(1,0) = hcubdiv12*ipv->getSigma(1,3); //stressBending[3];
    malpha(1,1) = hcubdiv12*ipv->getSigma(1,1); //stressBending[1];

  }
  else
  {
    const std::vector<double> &zsimp = ipv->getSimpsonPoint();
    std::vector<double> temp;
    temp.resize(_nsimp);
    for(int i=0;i<_nsimp;i++)
      temp[i]= ipv->getSigma(i,0); //stress[i][0]; // retrieve component change this ?? TODO
    if(fdg) nalpha(0,0) = SimpsonIntegration(temp,h);
    malpha(0,0) = SimpsonIntegration(temp,zsimp);
    for(int i=0;i<_nsimp; i++)
      temp[i]=ipv->getSigma(i,3); //stress[i][3];
    if(fdg) nalpha(0,1) = nalpha(1,0) = SimpsonIntegration(temp,h);
    malpha(0,1) = malpha(1,0) = SimpsonIntegration(temp,zsimp);
    for(int i=0;i<_nsimp; i++)
      temp[i]=ipv->getSigma(i,1); //stress[i][1];
    if(fdg) nalpha(1,1) = SimpsonIntegration(temp,h);
    malpha(1,1) = SimpsonIntegration(temp,zsimp);
  }
  // if on interface hat operation
  if(ipv->isInterface()){
    // nalphabeta = ntildealphabeta + lambda mubeta malphamu
    if(fdg){
      for(int a=0;a<2;a++)
        for(int b=0;b<2;b++)
          for(int mu=0;mu<2;mu++)
            nalpha(a,b)+=lb->getlambda(b,mu)*malpha(a,mu);
    reductionElement nhat(nalpha);
    nhat.hat(lb,nalpha);
    }
    reductionElement mhat(malpha);
    mhat.hat(lb,malpha);
    // put on vector form (if on interface multiply by the localBasis of interface)
    const shellLocalBasisInter* lbs = static_cast<const shellLocalBasisInter*>(ipv->getshellLocalBasisOfInterface());
    for(int alpha=0;alpha<2;alpha++){
      n_[alpha] = nalpha(alpha,0)*lbs->basisVector(0) + nalpha(alpha,1)*lbs->basisVector(1);
      m_[alpha] = malpha(alpha,0)*lbs->basisVector(0) + malpha(alpha,1)*lbs->basisVector(1);
    }
  }
  else{
    // put on vector form ( bulk point use local basis of bulk)
    for(int alpha=0;alpha<2;alpha++){
      n_[alpha] = nalpha(alpha,0)*lb->basisVector(0) + nalpha(alpha,1)*lb->basisVector(1);
      m_[alpha] = malpha(alpha,0)*lb->basisVector(0) + malpha(alpha,1)*lb->basisVector(1);
    }
  }
}

void linearElasticOrthotropicShellLaw::reduction(const IPStateBase *ips,std::vector<SVector3> &n_,
                         std::vector<SVector3> &m_,bool fdg) const
{
  reductionElement nalpha,malpha; // make cache data ??
  const IPVariableShell *ipv = static_cast<const IPVariableShell*>(ips->getState(IPStateBase::current));
  const linearShellLocalBasisBulk *lb = static_cast<const linearShellLocalBasisBulk*>(ipv->getshellLocalBasis());
  const double h = ipv->getThickness();
  if(_nsimp==1)
  {
    if(fdg){
      nalpha(0,0) = h*ipv->getSigma(0,0); // stressMembrane[0];

      // nalpha(0,2)  nalpha(1,2)


      nalpha(0,1) = nalpha(1,0) = h*ipv->getSigma(0,3); //stressMembrane[3];
      nalpha(1,1) = h*ipv->getSigma(0,1); //stressMembrane[1];
    }
    // compute malpha
    double hcubdiv12 = h*h*h/12.;
    malpha(0,0) = hcubdiv12*ipv->getSigma(1,0); //stressBending[0];
    malpha(0,1) = malpha(1,0) = hcubdiv12*ipv->getSigma(1,3); //stressBending[3];
    malpha(1,1) = hcubdiv12*ipv->getSigma(1,1); //stressBending[1];
    // if on interface hat operation
    if(ipv->isInterface()){
      // nalphabeta = ntildealphabeta + lambda mubeta malphamu
      if(fdg){
        for(int a=0;a<2;a++)
          for(int b=0;b<2;b++)
            for(int mu=0;mu<2;mu++)
              nalpha(a,b)+=lb->getlambda(b,mu)*malpha(a,mu);
      reductionElement nhat(nalpha);
      nhat.hat(lb,nalpha);
      }
      reductionElement mhat(malpha);
      mhat.hat(lb,malpha);
      // put on vector form (if on interface multiply by the localBasis of interface)
      const shellLocalBasisInter* lbs = static_cast<const shellLocalBasisInter*>(ipv->getshellLocalBasisOfInterface());
      for(int alpha=0;alpha<2;alpha++){
        n_[alpha] = nalpha(alpha,0)*lbs->basisVector(0) + nalpha(alpha,1)*lbs->basisVector(1);
        m_[alpha] = malpha(alpha,0)*lbs->basisVector(0) + malpha(alpha,1)*lbs->basisVector(1);
      }
    }
    else{
      // put on vector form ( bulk point use local basis of bulk)
      for(int alpha=0;alpha<2;alpha++){
        n_[alpha] = nalpha(alpha,0)*lb->basisVector(0) + nalpha(alpha,1)*lb->basisVector(1);
        m_[alpha] = malpha(alpha,0)*lb->basisVector(0) + malpha(alpha,1)*lb->basisVector(1);
      }
    }
  }
  else
  {
    short int nsimp_ = ipv->getNumSimp();
    std::vector<double> npcontrib1g1(nsimp_);
    std::vector<double> npcontrib2g1(nsimp_);
    std::vector<double> npcontrib3g1(nsimp_);
    std::vector<double> npcontrib1g2(nsimp_);
    std::vector<double> npcontrib2g2(nsimp_);
    std::vector<double> npcontrib3g2(nsimp_);
    std::vector<double> npcontrib1g3(nsimp_);
    std::vector<double> npcontrib2g3(nsimp_);
    std::vector<double> npcontrib3g3(nsimp_);
    const std::vector<double>& zsimp = ipv->getSimpsonPoint();
    const SVector3 &phi_1 = lb->basisVector(0);
    const SVector3 &phi_2 = lb->basisVector(1);
    const SVector3 &g_3 = lb->basisNormal();
    const SVector3 &t_1 = lb->basisDeriNormal(0);
    const SVector3 &t_2 = lb->basisDeriNormal(1);
    SVector3 gtmp;
    tab6 kcov;
    SVector3 g1,g2,g3;
    const double invJacbarre = 1./lb->getJacobian();
    for(int np=0;np<nsimp_;np++){
      const tab6& kir = ipv->getSigma(np);
      /* compute dual local basis */
      const SVector3 g_1 = phi_1 + zsimp[np]*t_1;
      const SVector3 g_2 = phi_2 + zsimp[np]*t_2;
      lb->dualGbasis(g_1,g_2,g_3,g1,g2,g3);

      /* stress in convected basis */
      for(int i=0;i<6;i++) kcov[i]=0.;
      alphaTensprodSym(kir[0],g_1,kcov);
      alphaTensprodSym(kir[1],g_2,kcov);
      alphaTensprodSym(kir[2],g_3,kcov);
      alphaTensprodSym(kir[3],g_1,g_2,kcov);
      alphaTensprodSym(kir[4],g_2,g_3,kcov);
      alphaTensprodSym(kir[5],g_1,g_3,kcov);

      /* compute jacobian */
      crossprod(g_1,g_2,gtmp);
      double jac = dot(gtmp,g_3);

      // mat vect prod of Piola-Kirchhoff tensor with basis vector and time j0
      // g1
      npcontrib1g1[np] = jac*(kcov[0]*g1[0]+kcov[3]*g1[1]+kcov[5]*g1[2]);
      npcontrib2g1[np] = jac*(kcov[3]*g1[0]+kcov[1]*g1[1]+kcov[4]*g1[2]);
      npcontrib3g1[np] = jac*(kcov[5]*g1[0]+kcov[4]*g1[1]+kcov[2]*g1[2]);
      // g2
      npcontrib1g2[np] = jac*(kcov[0]*g2[0]+kcov[3]*g2[1]+kcov[5]*g2[2]);
      npcontrib2g2[np] = jac*(kcov[3]*g2[0]+kcov[1]*g2[1]+kcov[4]*g2[2]);
      npcontrib3g2[np] = jac*(kcov[5]*g2[0]+kcov[4]*g2[1]+kcov[2]*g2[2]);
      // g3
      npcontrib1g3[np] = jac*(kcov[0]*g3[0]+kcov[3]*g3[1]+kcov[5]*g3[2]);
      npcontrib2g3[np] = jac*(kcov[3]*g3[0]+kcov[1]*g3[1]+kcov[4]*g3[2]);
      npcontrib3g3[np] = jac*(kcov[5]*g3[0]+kcov[4]*g3[1]+kcov[2]*g3[2]);
    }
    n_.resize(3);
    m_.resize(2);
    // simpson's integration group npcontrib (try to maximize cache hit)
    n_[0][0]      = invJacbarre*SimpsonIntegration(npcontrib1g1,h);
    m_[0][0]      = invJacbarre*SimpsonIntegration(npcontrib1g1,zsimp);
    m_[0][1]      = invJacbarre*SimpsonIntegration(npcontrib2g1,zsimp);
    n_[0][1]      = invJacbarre*SimpsonIntegration(npcontrib2g1,h);
    n_[0][2]      = invJacbarre*SimpsonIntegration(npcontrib3g1,h);
    m_[0][2]      = invJacbarre*SimpsonIntegration(npcontrib3g1,zsimp);
    n_[1][0]      = invJacbarre*SimpsonIntegration(npcontrib1g2,h);
    m_[1][0]      = invJacbarre*SimpsonIntegration(npcontrib1g2,zsimp);
    m_[1][1]      = invJacbarre*SimpsonIntegration(npcontrib2g2,zsimp);
    n_[1][1]      = invJacbarre*SimpsonIntegration(npcontrib2g2,h);
    n_[1][2]      = invJacbarre*SimpsonIntegration(npcontrib3g2,h);
    m_[1][2]      = invJacbarre*SimpsonIntegration(npcontrib3g2,zsimp);
    n_[2][0]      = invJacbarre*SimpsonIntegration(npcontrib1g3,h);
    n_[2][1]      = invJacbarre*SimpsonIntegration(npcontrib2g3,h);
    n_[2][2]      = invJacbarre*SimpsonIntegration(npcontrib3g3,h);

    // put ñ and not n in n_ !!!
    for(int alpha=0;alpha<2;alpha++)
    {
      for(int beta=0;beta<2;beta++)
      {
        n_[alpha] -= (lb->getlambda(beta,0)*dot(m_[alpha],lb->dualBasisVector(0))+lb->getlambda(beta,1)*dot(m_[alpha],lb->dualBasisVector(1)))*lb->basisVector(beta);
      }
    }

    // Rotation on interface (nhat rotation only 1 and 2, 3 is perpendicular?)
    if(ipv->isInterface()){
      SVector3 vhat1;
      SVector3 vhat2;
      if(fdg){
        vhat1 = n_[0];
        vhat2 = n_[1];
        n_[0] = lb->basisPushTensor(0,0)*vhat1 + lb->basisPushTensor(1,0)*vhat2;
        n_[1] = lb->basisPushTensor(0,1)*vhat1 + lb->basisPushTensor(1,1)*vhat2;
      }
      vhat1 = m_[0];
      vhat2 = m_[1];
      m_[0] = lb->basisPushTensor(0,0)*vhat1 + lb->basisPushTensor(1,0)*vhat2;
      m_[1] = lb->basisPushTensor(0,1)*vhat1 + lb->basisPushTensor(1,1)*vhat2; // BEAWARE T(alpha^-,alpha) = dot(phi^,a,phi_,a-) is inverted in MetaSheel
    }

  }
}

linearElasticShellLaw::linearElasticShellLaw(const int num, const double E,const double nu,const double thick,
                                             const int nsimp, const double rho) : shellMaterialLaw(num,thick,nsimp,rho,true),
                                                                 _E(E), _nu(nu), C11(E/((1-nu)*(1+nu))), _G(0.5*E/(1.+nu))
{
  _nsimp == 1 ? _ns=2 : _ns=nsimp;
}

linearElasticShellLaw::linearElasticShellLaw( const linearElasticShellLaw &source)
                                             : shellMaterialLaw(source), _E(source._E),
                                               _nu(source._nu), C11(source.C11), H(source.H),
                                               _G(source._G), _ns(source._ns){}
void linearElasticShellLaw::createIPState(IPStateBase* &ips,const bool* state_,
                                          const MElement *ele, const int nbFF_, const int gpt) const
{
  // check interface element ??
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL) inter=false;
  IPVariable* ipvi = new IPLinearShell(_h0,nbFF_,_nsimp,inter);
  IPVariable* ipv1 = new IPLinearShell(_h0,nbFF_,_nsimp,inter);
  IPVariable* ipv2 = new IPLinearShell(_h0,nbFF_,_nsimp,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);

}

void linearElasticShellLaw::createIPVariable(IPVariable* &ipv,const MElement *ele, const int nbFF_) const
{
  if(ipv !=NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL) inter=false;
  ipv = new IPLinearShell(_h0,nbFF_,_nsimp,inter);
}

void linearElasticShellLaw::shellBendingStiffness(const IPVariableShell *ipv, LinearElasticShellHookeTensor &Hbend) const {
  const linearShellLocalBasisBulk *lb = static_cast<const linearShellLocalBasisBulk*>(ipv->getshellLocalBasis());
  double h = ipv->getThickness();
  double bendfactor = h*h*h/12.*C11;
  Hbend.set(lb,bendfactor,_nu);
}
void linearElasticShellLaw::shellMembraneStiffness(const IPVariableShell *ipv, LinearElasticShellHookeTensor &Htens) const {
  const linearShellLocalBasisBulk *lb = static_cast<const linearShellLocalBasisBulk*>(ipv->getshellLocalBasis());
  double h = ipv->getThickness();
  double membFactor = h*C11;
  Htens.set(lb,membFactor,_nu);
}
void linearElasticShellLaw::shellShearingStiffness(const IPVariableShell *ipv, linearElasticShellShearingHookeTensor &Hshear) const {
  const linearShellLocalBasisBulk *lb = static_cast<const linearShellLocalBasisBulk*>(ipv->getshellLocalBasis());
  double h = ipv->getThickness();
  double shearFactor = h*_G;
  Hshear.set(lb,shearFactor);
}

void linearElasticShellLaw::stress(IPStateBase *ips, bool stiff, const bool checkfrac)
{
  IPVariableShell *ipv = static_cast<IPVariableShell*>(ips->getState(IPStateBase::current));
  const linearShellLocalBasisBulk *lb = static_cast<linearShellLocalBasisBulk*>(ipv->getshellLocalBasis());
  H.set(lb,C11,_nu);
  // loop on simpson's point
  for(int i=0;i<_ns; i++){
    const tab6 &eps = ipv->getEpsilon(i);
    tab6 &sig = ipv->getSigma(i);
    sig[0] = H(0,0,0,0)*eps[0]+(H(0,0,0,1)+H(0,0,1,0))*eps[3]+H(0,0,1,1)*eps[1];
    sig[1] = H(1,1,0,0)*eps[0]+(H(1,1,0,1)+H(1,1,1,0))*eps[3]+H(1,1,1,1)*eps[1];
    sig[3] = H(0,1,0,0)*eps[0]+(H(0,1,0,1)+H(0,1,1,0))*eps[3]+H(0,1,1,1)*eps[1];

    //sig[3] = H(0,1,0,0)*eps[0]+(H(0,1,0,1))*eps[3]+H(0,1,1,1)*eps[1];

    sig[2]=sig[4]=sig[5]=0.;
  }
}

double linearElasticShellLaw::soundSpeed() const
{
    double factornu = (1.-_nu)/((1.+_nu)*(1.-2.*_nu));
    return sqrt(_E*factornu/_rho);
}

 linearElasticOrthotropicShellLaw::linearElasticOrthotropicShellLaw(const int num,const double rho,
                                                                    const double h0, const int nsimp) :
                                                 shellMaterialLaw(num,h0,nsimp,rho,true),
                                                 _Ex(0.), _Ey(0.),_Ez(0.), _nuxy(0.), _nuxz(0.),_nuyz(0.), _nuyx(0.), _nuzx(0.), _nuzy(0.),
                                                 _muxy(0.), _muxz(0.), _muyz(0.), _theta(0.), _phi(0.), _psi(0.),
                                                 _R(_theta,_phi,_psi), _D(0.),
                                                 _H(_Ex,_Ey,_Ez,_nuxy,_nuxz,_nuyz,_nuyx,_nuzx,_nuzy,_muxy,_muxz,_muyz,_D)
{
  nsimp==1 ? _ns =2 : _ns = nsimp;
}
linearElasticOrthotropicShellLaw::linearElasticOrthotropicShellLaw(const int num,const double rho,const double h0,
                                                                   const int nsimp,const double Ex, const double Ey,
                                                                   const double Ez, const double nuxy,const double nuxz,
                                                                   const double nuyz, const double muxy,
                                                                   const double muxz,const double muyz,
                                                                   const double theta, const double phi,
                                                                   const double psi) : shellMaterialLaw(num,h0,nsimp,rho,true),
                                                                                   _Ex(Ex), _Ey(Ey), _Ez(Ez), _nuxy(nuxy),
                                                                                   _nuxz(nuxz), _nuyz(nuyz),_muxy(muxy),
                                                                                   _muxz(muxz), _muyz(muyz),
                                                                                   _theta(theta), _phi(phi), _psi(psi),
                                                                                   _R(theta,phi,psi),_nuzy(_nuyz * _Ez/_Ey),
                                                                                   _nuzx(_nuxz * _Ez/_Ex),
                                                                                   _nuyx(_nuxy * _Ey/_Ex),
                                                                                   _D((1. - _nuxy*_nuyx - _nuzy*_nuyz - _nuzx*_nuxz - 2.*_nuxy*_nuyz*_nuzx) / (_Ex*_Ey*_Ez)),
                                                                                   _H(_Ex,_Ey,_Ez,_nuxy,_nuxz,_nuyz,_nuyx,_nuzx,_nuzy,_muxy,_muxz,_muyz,_D)
{
if(_nuxy <0. or _nuxy > 0.499999999) Msg::Error("Poisson ratio xy worng %f: \n",_nuxy);
if(_nuyx <0. or _nuyx > 0.499999999) Msg::Error("Poisson ratio yx worng %f: \n",_nuyx);
if(_nuxz <0. or _nuxz > 0.499999999) Msg::Error("Poisson ratio xz worng %f: \n",_nuxz);
if(_nuzx <0. or _nuzx > 0.499999999) Msg::Error("Poisson ratio zx worng %f: \n",_nuzx);
if(_nuyz <0. or _nuyz > 0.499999999) Msg::Error("Poisson ratio yz worng %f: \n",_nuyz);
if(_nuzy <0. or _nuzy > 0.499999999) Msg::Error("Poisson ratio zy worng %f: \n",_nuzy);


  nsimp==1 ? _ns =2 : _ns = nsimp;
  _H.rotate(_R);
}

 linearElasticOrthotropicShellLaw::linearElasticOrthotropicShellLaw(const linearElasticOrthotropicShellLaw &source) :
                                                    shellMaterialLaw(source), _R(source._R), _H(source._H),
                                                    _ns(source._ns), _D(source._D)
{
  _Ex = source._Ex;
  _Ey = source._Ey;
  _Ez = source._Ez;
  _nuxy = source._nuxy;
  _nuxz = source._nuxz;
  _nuyz = source._nuyz;
  _nuyx = source._nuyx;
  _nuzx = source._nuzx;
  _nuzy = source._nuzy;
  _muxy = source._muxy;
  _muxz = source._muxz;
  _muyz = source._muyz;
  _theta = source._theta;
  _phi = source._phi;
  _psi = source._psi;
}

void linearElasticOrthotropicShellLaw::createIPState(IPStateBase* &ips,const bool* state_,
                                          const MElement *ele, const int nbFF_, const int gpt) const
{
  // check interface element ??
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL) inter=false;
  IPVariable* ipvi = new IPLinearOrthotropicShell(_h0,nbFF_,_nsimp,inter);
  IPVariable* ipv1 = new IPLinearOrthotropicShell(_h0,nbFF_,_nsimp,inter);
  IPVariable* ipv2 = new IPLinearOrthotropicShell(_h0,nbFF_,_nsimp,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}


void linearElasticOrthotropicShellLaw::createIPVariable(IPVariable* &ipv, const MElement *ele, const int nbFF_) const
{
  if(ipv != NULL) delete ipv;
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL) inter = false;
  ipv = new IPLinearOrthotropicShell(_h0,nbFF_,_nsimp,inter);
}

void linearElasticOrthotropicShellLaw::initD()
{
  // Do computation only if Young moduli are initialized
  if(_Ex != 0.)
    _D = (1. - _nuxy*_nuyx - _nuzy*_nuyz - _nuzx*_nuxz - 2.*_nuxy*_nuyz*_nuzx) / (_Ex*_Ey*_Ez);
    _H.set(_Ex,_Ey,_Ez,_nuxy,_nuxz,_nuyz,_nuyx,_nuzx,_nuzy,_muxy,_muxz,_muyz,_D);
    _H.rotate(_R);
}

void linearElasticOrthotropicShellLaw::setYoung(const double Ex, const double Ey, const double Ez)
{
  _Ex = Ex;
  _Ey = Ey;
  _Ez = Ez;
  this->initD();
}

void linearElasticOrthotropicShellLaw::setMu(const double muxy, const double muxz, const double muyz)
{
  _muxy = muxy;
  _muxz = muxz;
  _muyz = muyz;
  if( _Ex != 0.)
    this->initD();
}

void linearElasticOrthotropicShellLaw::setPoisson(const double nuxy, const double nuxz, const double nuyz){
  _nuxy = nuxy;
  _nuxz = nuxz;
  _nuyz = nuyz;
  if(_Ex != 0.){
    _nuzy = _nuyz * _Ez/_Ey;
    _nuzx = _nuxz * _Ez/_Ex;
    _nuyx = _nuxy * _Ey/_Ex;
    this->initD();
  }
}

void linearElasticOrthotropicShellLaw::setRotation(const double theta, const double phi, const double psi)
{
  _theta = theta;
  _phi = phi;
  _psi = psi;
  _R.setAngle(theta,phi,psi);
  this->initD();
}

double linearElasticOrthotropicShellLaw::getYoungMax() const
{
  double E;
  if(_Ex > _Ey) E = _Ex;
    else E = _Ey;
  return (E > _Ez) ? E : _Ez;
}

double linearElasticOrthotropicShellLaw::getPoissonMax() const
{
  double nu;
  if(_nuxy > _nuyx) nu = _nuxy;
  else nu = _nuyx;
  if(nu < _nuxz) nu = _nuxz;
  if(nu < _nuzx) nu = _nuzx;
  if(nu < _nuyz) nu = _nuyz;
  return (nu > _nuzy) ? nu : _nuzy;
}

double linearElasticOrthotropicShellLaw::soundSpeed() const
{
    double E = this->getYoungMax();
    double nu = this->getPoissonMax();
    double factornu = (1.-nu)/((1.+nu)*(1.-2.*nu));
    return sqrt(E*factornu/_rho);
}

void linearElasticOrthotropicShellLaw::shellBendingStiffness(const IPVariableShell *ipv, LinearElasticShellHookeTensor &Hbend) const {
  const IPLinearOrthotropicShell *ipvortho;
  const IPShellFractureCohesive *ipvtmp = dynamic_cast<const IPShellFractureCohesive*>(ipv);
  if(ipvtmp != NULL)
    ipvortho = static_cast<const IPLinearOrthotropicShell*>(ipvtmp->getIPvBulk());
  else
    ipvortho = static_cast<const IPLinearOrthotropicShell*>(ipv);
  double h = ipv->getThickness();
  double bendfactor = h*h*h/12.;
  Hbend = ipvortho->getHooke();
  Hbend *=bendfactor;
}
void linearElasticOrthotropicShellLaw::shellMembraneStiffness(const IPVariableShell *ipv, LinearElasticShellHookeTensor &Htens) const {
  const IPLinearOrthotropicShell *ipvortho;
  const IPShellFractureCohesive *ipvtmp = dynamic_cast<const IPShellFractureCohesive*>(ipv);
  if(ipvtmp != NULL)
    ipvortho = static_cast<const IPLinearOrthotropicShell*>(ipvtmp->getIPvBulk());
  else
    ipvortho = static_cast<const IPLinearOrthotropicShell*>(ipv);
  double h = ipv->getThickness();
  double membFactor = h;
  Htens = ipvortho->getHooke();
  Htens*= membFactor;
}
void linearElasticOrthotropicShellLaw::shellShearingStiffness(const IPVariableShell *ipv, linearElasticShellShearingHookeTensor &Hshear) const {
  const linearShellLocalBasisBulk *lb = static_cast<const linearShellLocalBasisBulk*>(ipv->getshellLocalBasis());
  double h = ipv->getThickness();
  double muMax = getYoungMax()/2./(1.+getPoissonMax());
  double shearFactor = h*muMax;
  Hshear.set(lb,shearFactor);
}

void linearElasticOrthotropicShellLaw::stress(IPStateBase *ips, bool stiff, const bool checkfrac)
{
  IPLinearOrthotropicShell *ipv = dynamic_cast<IPLinearOrthotropicShell*>(ips->getState(IPStateBase::current));
  if(ipv == NULL) // due to fracture law
  {
    IPShellFractureCohesive *ipvtmp = static_cast<IPShellFractureCohesive*>(ips->getState(IPStateBase::current));
    ipv = static_cast<IPLinearOrthotropicShell*>(ipvtmp->getIPvBulk());
  }
  const shellLocalBasis *lb = ipv->getshellLocalBasis();
  LinearElasticShellHookeTensor *HshellPlane;
  bool &ihooke = ipv->getHooke(HshellPlane);
  if(!ihooke)
  {
    // 1) H --> H plan shell

    hookeTensor Hshell3D = hookeTensor(); // TO BE DEFINED with lb->dualBasisVector() //= ...;
    for(int n=0;n<3;n++){
      for(int m=0;m<3;m++){
        for(int o=0;o<3;o++){
          for(int p=0;p<3;p++){
            for(int i=0;i<3;i++){
              for(int j=0;j<3;j++){
                for(int k=0;k<3;k++){
                  for(int l=0;l<3;l++){
                    Hshell3D(n,m,o,p) += _H(i,j,k,l)*lb->dualBasisVector(n,i)*lb->dualBasisVector(m,j)*lb->dualBasisVector(o,k)*lb->dualBasisVector(p,l);
                  }
                }
              }
            }
          }
        }
      }
    }


 HshellPlane->operator()(0,0,0,0) = Hshell3D(0,0,0,0) - Hshell3D(0,0,2,2)*Hshell3D(2,2,0,0)/Hshell3D(2,2,2,2);
 HshellPlane->operator()(0,0,0,1) = Hshell3D(0,0,0,1) - Hshell3D(0,0,2,2)*Hshell3D(2,2,0,1)/Hshell3D(2,2,2,2);
 HshellPlane->operator()(0,0,1,0) = Hshell3D(0,0,1,0) - Hshell3D(0,0,2,2)*Hshell3D(2,2,1,0)/Hshell3D(2,2,2,2);
 HshellPlane->operator()(0,0,1,1) = Hshell3D(0,0,1,1) - Hshell3D(0,0,2,2)*Hshell3D(2,2,1,1)/Hshell3D(2,2,2,2);

 HshellPlane->operator()(0,1,0,0) = Hshell3D(0,1,0,0) - Hshell3D(0,1,2,2)*Hshell3D(2,2,0,0)/Hshell3D(2,2,2,2);
 HshellPlane->operator()(0,1,0,1) = Hshell3D(0,1,0,1) - Hshell3D(0,1,2,2)*Hshell3D(2,2,0,1)/Hshell3D(2,2,2,2);
 HshellPlane->operator()(0,1,1,0) = Hshell3D(0,1,1,0) - Hshell3D(0,1,2,2)*Hshell3D(2,2,1,0)/Hshell3D(2,2,2,2);
 HshellPlane->operator()(0,1,1,1) = Hshell3D(0,1,1,1) - Hshell3D(0,1,2,2)*Hshell3D(2,2,1,1)/Hshell3D(2,2,2,2);

 HshellPlane->operator()(1,0,0,0) = Hshell3D(1,0,0,0) - Hshell3D(1,0,2,2)*Hshell3D(2,2,0,0)/Hshell3D(2,2,2,2);
 HshellPlane->operator()(1,0,0,1) = Hshell3D(1,0,0,1) - Hshell3D(1,0,2,2)*Hshell3D(2,2,0,1)/Hshell3D(2,2,2,2);
 HshellPlane->operator()(1,0,1,0) = Hshell3D(1,0,1,0) - Hshell3D(1,0,2,2)*Hshell3D(2,2,1,0)/Hshell3D(2,2,2,2);
 HshellPlane->operator()(1,0,1,1) = Hshell3D(1,0,1,1) - Hshell3D(1,0,2,2)*Hshell3D(2,2,1,1)/Hshell3D(2,2,2,2);


 HshellPlane->operator()(1,1,0,0) = Hshell3D(1,1,0,0) - Hshell3D(1,1,2,2)*Hshell3D(2,2,0,0)/Hshell3D(2,2,2,2);
 HshellPlane->operator()(1,1,0,1) = Hshell3D(1,1,0,1) - Hshell3D(1,1,2,2)*Hshell3D(2,2,0,1)/Hshell3D(2,2,2,2);
 HshellPlane->operator()(1,1,1,0) = Hshell3D(1,1,1,0) - Hshell3D(1,1,2,2)*Hshell3D(2,2,1,0)/Hshell3D(2,2,2,2);
 HshellPlane->operator()(1,1,1,1) = Hshell3D(1,1,1,1) - Hshell3D(1,1,2,2)*Hshell3D(2,2,1,1)/Hshell3D(2,2,2,2);
    ihooke = true;
  }

  /////////////////////////////////////////////ipv->initializeHPlane(_H);

  reductionElement redstrain;
  // loop on simpson's point
  for(int i=0;i<_ns;i++){ // if 1 simpson's point i=0 Membrane i=1 Bending
    const tab6 &eps = ipv->getEpsilon(i);
    tab6&sig = ipv->getSigma(i);
   // sig[0] = HshellPlane->get(0,0,0,0)*eps[0] + (HshellPlane->get(0,0,0,1) + HshellPlane->get(0,0,1,0))*eps[3] + HshellPlane->get(0,0,1,1)*eps[1];
    //sig[1] = HshellPlane->get(1,1,0,0)*eps[0] + (HshellPlane->get(1,1,0,1) + HshellPlane->get(1,1,1,0))*eps[3] + HshellPlane->get(1,1,1,1)*eps[1];
    //sig[3] = HshellPlane->get(0,1,0,0)*eps[0] + (HshellPlane->get(0,1,0,1) + HshellPlane->get(0,1,1,0))*eps[3] + HshellPlane->get(0,1,1,1)*eps[1];

    sig[0] = HshellPlane->get(0,0,0,0)*eps[0] + (HshellPlane->get(0,0,0,1) + HshellPlane->get(0,0,1,0))*eps[3] + HshellPlane->get(0,0,1,1)*eps[1];
    sig[1] = HshellPlane->get(1,1,0,0)*eps[0] + (HshellPlane->get(1,1,0,1) + HshellPlane->get(1,1,1,0))*eps[3] + HshellPlane->get(1,1,1,1)*eps[1];
    sig[3] = HshellPlane->get(0,1,0,0)*eps[0] + (HshellPlane->get(0,1,0,1) + HshellPlane->get(0,1,1,0))*eps[3] + HshellPlane->get(0,1,1,1)*eps[1];
    //sig[4] = HshellPlane->get(1,2,0,0)*eps[0] + (HshellPlane->get(1,2,0,1) + HshellPlane->get(1,2,1,0))*eps[3] + HshellPlane->get(1,2,1,1)*eps[1] + HshellPlane->get(1,2,2,2)*eps[2];
    //sig[5] = HshellPlane->get(0,2,0,0)*eps[0] + (HshellPlane->get(0,2,0,1) + HshellPlane->get(0,2,1,0))*eps[3] + HshellPlane->get(0,2,1,1)*eps[1] + HshellPlane->get(0,2,2,2)*eps[2];

    sig[2]  = sig[4] = sig[5] = 0;
     //sig[2] =0;
    for(int j=0;j<6;j++)
      if(isnan(sig[j]))
        Msg::Error("sig nzn");
  }
}

linearElasticShellLawWithDamage::linearElasticShellLawWithDamage(const int num, const double E,const double nu,
                                                                 const double h,const int nsimp, const double Yc,
                                                                 const double D0, const double Dc, const double m,
                                                                 const double rho) : linearElasticShellLaw(num,E,nu,h,nsimp,rho),
                                                                                      _Yc(Yc), _D0(D0), _Dc(Dc), _halfm(0.5*m){}

linearElasticShellLawWithDamage::linearElasticShellLawWithDamage(const linearElasticShellLawWithDamage &source) :
                                                                 linearElasticShellLaw(source),_Yc(source._Yc),
                                                                 _D0(source._D0), _Dc(source._Dc), _halfm(source._halfm){}

void linearElasticShellLawWithDamage::createIPState(IPStateBase* &ips,const bool* state_,
                                          const MElement *ele, const int nbFF_, const int gpt) const
{
  bool inter = true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL) inter = false;
  IPVariable* ipvi = new IPLinearShellWD(_h0,nbFF_,_nsimp,_D0,inter);
  IPVariable* ipv1 = new IPLinearShellWD(_h0,nbFF_,_nsimp,_D0,inter);
  IPVariable* ipv2 = new IPLinearShellWD(_h0,nbFF_,_nsimp,_D0,inter);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void linearElasticShellLawWithDamage::createIPVariable(IPVariable* &ipv, const MElement *ele, const int nbFF_) const
{
  if(ipv != NULL) delete ipv;
  bool inter = true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele == NULL) inter = false;
  ipv = new IPLinearShellWD(_h0,nbFF_,_nsimp,_D0,inter);
}

void linearElasticShellLawWithDamage::stress(IPStateBase *ips, bool stiff, const bool checkfrac)
{
  IPLinearShellWD *ipvd;
  IPLinearShellWD *ipvp;
  IPShellFractureCohesive *ipvtmp = dynamic_cast<IPShellFractureCohesive*>(ips->getState(IPStateBase::current));
  if(ipvtmp != NULL)
  {
    ipvd = static_cast<IPLinearShellWD*>(ipvtmp->getIPvBulk());
    ipvtmp = static_cast<IPShellFractureCohesive*>(ips->getState(IPStateBase::previous));
    ipvp = static_cast<IPLinearShellWD*>(ipvtmp->getIPvBulk()); // ipvd= current ipvp= previous
  }
  else
  {
    ipvd = static_cast<IPLinearShellWD*>(ips->getState(IPStateBase::current));
    ipvp = static_cast<IPLinearShellWD*>(ips->getState(IPStateBase::previous));
  }
  const linearShellLocalBasisBulk *lb = static_cast<linearShellLocalBasisBulk*>(ipvd->getshellLocalBasis());
  H.set(lb,C11,_nu);
  // loop on simpson's point
  for(int i=0;i<_nsimp;i++){
    const tab6 &eps = ipvd->getEpsilon(i);
    tab6& sig = ipvd->getSigma(i);
    double &D = ipvd->getDamage(i);
    double &Bifur = ipvd->getBifurcation(i);
//    double f;
    double r;
    const double &Dprev = ipvp->getDamage(i);
    if(checkfrac){
      double Y = 0.5*(eps[0]*(H(0,0,0,0)*eps[0]+H(0,0,0,1)*eps[3]+H(0,0,1,0)*eps[3]+H(0,0,1,1)*eps[1])+
                      eps[1]*(H(1,1,0,0)*eps[0]+H(1,1,0,1)*eps[3]+H(1,1,1,0)*eps[3]+H(1,1,1,1)*eps[1])+
                    2*eps[3]*(H(0,1,0,0)*eps[0]+H(0,1,0,1)*eps[3]+H(0,1,1,0)*eps[3]+H(0,1,1,1)*eps[1]));
      if ((1.-Dprev)*pow(Y,_halfm)-pow(_Yc,_halfm )>0.) {
        D = 1.-pow(_Yc/Y,_halfm);
     /* if (f<_Yc){
        r=_Yc;
      } else {
          r=f;
        }
        if ((1.-Dprev)*f-_Yc*_Yc/r >0.) {
          D=1-(_Yc*_Yc)/(f*r);*/
          if(D > _Dc)  D=_Dc;
        }
      else{
        D = Dprev;
      }
    }

    sig[0] = (1.-D)*(H(0,0,0,0)*eps[0]+(H(0,0,0,1)+H(0,0,1,0))*eps[3]+H(0,0,1,1)*eps[1]);
    sig[1] = (1.-D)*(H(1,1,0,0)*eps[0]+(H(1,1,0,1)+H(1,1,1,0))*eps[3]+H(1,1,1,1)*eps[1]);
    sig[3] = (1.-D)*(H(0,1,0,0)*eps[0]+(H(0,1,0,1)+H(0,1,1,0))*eps[3]+H(0,1,1,1)*eps[1]);


     //sig[3] = (1.-D)*(H(0,1,0,0)*eps[0]+(H(0,1,0,1))*eps[3]+H(0,1,1,1)*eps[1]);
    sig[2]=sig[4]=sig[5]=0.;

    /* We don't care about bifurcation. (criterion a a maximal damage value)
    if(checkfrac){
      // compute stress tensor XYZ
      reductionElement rstress;
      rstress(0,0) = sig[0];
      rstress(1,1) = sig[1];
      rstress(0,1) = rstress(1,0) = sig[3];
      stressTensor stress(rstress,lb);
      double stressI = (stress(0,0)+stress(1,1)+sqrt((stress(0,0)-stress(1,1))*(stress(0,0)-stress(1,1))+4*stress(0,1)))/2.;
      double stressIII = (stress(0,0)+stress(1,1)-sqrt((stress(0,0)-stress(1,1))*(stress(0,0)-stress(1,1))+4*stress(0,1)))/2.;
      if (stressI >0. && stressIII>0.){
          stressIII=0.;
      }
      else if (stressI<0. && stressIII<0.) {
          stressI=0.;
      }
      double Zcrit = (stressI*stressI+stressIII*stressIII-2*_nu*stressI*stressIII)/((1.-D)*_E); //Modified Hooke tensor
      //double Zcrit = ((1-_nu)*(stressI*stressI+stressIII*stressIII)-2*_nu*stressI*stressIII)*(1+_nu)/((1.-D)*_E);
      if (D==Dprev){
          Bifur=0.;
      }
      else{
          Bifur=Zcrit/((1-D)*f*f/_m);
          //Bifur = Zcrit/(_Yc*_Yc*r*r/(f*(r+f)));
          }
          //if (D==Dprev){
          //    Bifur=0;
          //}
          //else{
          //  double H=-_Yc*_Yc/(r*r);
          //  Bifur = H/(1-D-r*r*(1-D)*(1-D)/Zcrit);
          //}
    }
    */
  }
//  double &Bifur0 = ipvd->getDamage(0);
//  if (Bifur0>0.4){
//      Msg::Info("Damage 04 %e",Bifur0);
//  }
}

shellCohesiveLaw::shellCohesiveLaw(const int num, const double Gc, const double sigmac,
                                   const double beta, const double mu,const double fscmin, const double fscmax,
                                   const bool init) : materialLaw(num,init), _Gc(Gc), _sigmac(sigmac),
                                                       _beta(beta), _mu(mu), _fscmin(fscmin), _fscmax(fscmax){}
shellCohesiveLaw::shellCohesiveLaw(const shellCohesiveLaw &source) : materialLaw(source), _Gc(source._Gc),
                                                                      _sigmac(source._sigmac),
                                                                      _beta(source._beta), _mu(source._mu),
                                                                      _fscmin(source._fscmin), _fscmax(source._fscmax){}

shellLinearCohesiveLaw::shellLinearCohesiveLaw(const int num, const double Gc, const double sigmac,
                                               const double beta,const double mu,
                                               const double fractureStrengthFactorMin,
                                               const double fractureStrengthFactorMax, const double cohesive_law_tol) : shellCohesiveLaw(num,Gc,sigmac,beta,mu,fractureStrengthFactorMin,
                                                                                                          fractureStrengthFactorMax,true),
                                                                                         _betaexpm2(1./(_beta*_beta)), _cohesive_law_tol(cohesive_law_tol){}
shellLinearCohesiveLaw::shellLinearCohesiveLaw(const shellLinearCohesiveLaw &source) : shellCohesiveLaw(source),
                                                                                        _betaexpm2(source._betaexpm2), _cohesive_law_tol(source._cohesive_law_tol){}

void shellLinearCohesiveLaw::initDataFromLaw(const IPVariable *ipvlaw,IPVariable *ipv) const
{
  const IPLinearCohesiveShell *ipvlawc = static_cast<const IPLinearCohesiveShell*>(ipvlaw);
  IPLinearCohesiveShell *ipvc = static_cast<IPLinearCohesiveShell*>(ipv);
  ipvc->setDataFromLaw(ipvlawc->getSigmac(),ipvlawc->getSigI(),ipvlawc->getTauII(),_Gc,ipvlawc->getBeta(),ipvlawc->ifTension());
}

void shellLinearCohesiveLaw::createIPState(IPStateBase* &ips,const bool* state_,
                                          const MElement *ele, const int nbFF_, const int gpt) const
{
  IPVariable *ipvi = new IPLinearCohesiveShell();
  IPVariable *ipv1 = new IPLinearCohesiveShell();
  IPVariable *ipv2 = new IPLinearCohesiveShell();
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void shellLinearCohesiveLaw::createIPVariable(IPVariable* &ipv,const MElement *ele,const int nbFF) const
{
  ipv = new IPLinearCohesiveShell();
}

void shellLinearCohesiveLaw::reduction(IPLinearCohesiveShell *ipvcur, const IPLinearCohesiveShell *ipvprev,
                                        reductionElement &nalphabeta, reductionElement &malphabeta) const
{
//  nalpha.setAll(0.);
//  malpha.setAll(0.);
  const double delta = ipvcur->getDelta();
  const double deltan=ipvcur->getDeltan();
  const double deltat=ipvcur->getDeltat();
  const double deltac = ipvcur->getDeltac();
  const bool tension = ipvcur->ifTension();
  const reductionElement& m0 = ipvcur->getm0();
  const reductionElement& n0 = ipvcur->getn0();
  const double delta_max=ipvprev->getDeltamax(); // Delta_max at previous iteration (initialization = 0 --> OK)
  this->reduction(m0,n0,delta,delta_max,deltan,deltat,deltac,tension,nalphabeta,malphabeta);
  // compute the energy
  ipvcur->_energyBending = ipvprev->_energyBending;
  ipvcur->_energyMembrane = ipvprev->_energyMembrane;
  if(fabs(delta)>=fabs(delta_max) or (delta_max==0))
  {
    if(ipvcur->getDeltaun()>0) // otherwise compression
      ipvcur->_energyMembrane += 0.5*(ipvprev->getn()(1,1)+nalphabeta(1,1))*(ipvcur->getDeltaun()*ipvcur->getNormPhi2()-ipvprev->getDeltaun()*ipvprev->getNormPhi2()); // n0 >= 0 by construction = 0 if compression
    ipvcur->_energyMembrane += 0.5*fabs(fabs(ipvprev->getn()(1,0))+fabs(nalphabeta(1,0)))*fabs(fabs(ipvcur->getDeltaut()*ipvcur->getNormPhi1())-fabs(ipvprev->getDeltaut()*ipvprev->getNormPhi1()));
    ipvcur->_energyBending +=  0.5*fabs(ipvprev->getm()(1,1)+malphabeta(1,1))*fabs(ipvcur->getDeltarn()*ipvcur->getNormPhi2()-ipvprev->getDeltarn()*ipvprev->getNormPhi2()); // m0 can be <0 depending on the loading --> take absolute value
    ipvcur->_energyBending += 0.5*fabs(ipvprev->getm()(1,0)+malphabeta(1,0))*fabs(ipvcur->getDeltart()*ipvcur->getNormPhi1()-ipvprev->getDeltart()*ipvprev->getNormPhi1());
    ipvcur->setnm(nalphabeta,malphabeta);
  }
  else
  {
    ipvcur->setnm(ipvprev->getn(),ipvprev->getm());
  }
}

void shellLinearCohesiveLaw::reduction(IPLinearCohesiveShell *ipv,const IPLinearCohesiveShell *ipvprev,const double deltan,const double deltat,
                                        const double delta,reductionElement &nhat, reductionElement &mhat) const
{
//  nhat.setAll(0.);
//  mhat.setAll(0.);
  double deltac = ipv->getDeltac();
  double delta_max=ipvprev->getDeltamax();
  bool tension = ipv->ifTension();
  const reductionElement& m0 = ipv->getm0();
  const reductionElement& n0 = ipv->getn0();
  this->reduction(m0,n0,delta,delta_max,deltan,deltat,deltac,tension,nhat,mhat);

// Matrix by perturbation --> no energy computation
}

void shellLinearCohesiveLaw::reduction(const reductionElement &m0, const reductionElement &n0, const double delta,
                                        const double delta_max, const double deltan, const double deltat,
                                        const double deltac,const bool tension, reductionElement &nhatmean,
                                        reductionElement &mhatmean) const
{
  nhatmean.setAll(0.);
  mhatmean.setAll(0.);
  double _tol = this->_cohesive_law_tol;
  //-----added by Shantanu for debugging on 21-06-2013, don't forget to comment it after it's done------//

 /* char *_reductionfilename = "reduction_values.csv";
  FILE *tmp = fopen(_reductionfilename, "a");
  fprintf(tmp, "%12.5E %12.5E %12.5E %12.5E\n", delta, deltan, deltat, deltac);
  fclose(tmp);
*/
  //-----added by Shantanu for debugging on 21-06-2013, don't forget to comment it after it's done------//
  // monotonic decreasing cohesive law (Pandolfi & Ortiz 1999)
  if(tension){   // tension case
    if(fabs(delta)<=fabs(deltac) and deltan >=0.) //and fabs(delta_max)<fabs(deltac))
    {
      double t=0.;
      if( (fabs(delta) >= fabs(delta_max)) or (delta_max == 0)) // loading case
      {
        if(delta*deltac >=0.)
          t = 1.-delta/deltac;
        else
          t = -1.-delta/deltac;
      }
      else //unloading case
      {
        if(fabs(delta_max)>(_tol*deltac))
        {
          t = delta/delta_max - delta/deltac;
        }
        else
        {
          if(delta_max*deltac >=0.)
            t=1.- delta_max/deltac;
          else
            t=-1.-delta_max/deltac;
        }
      }
      double facn=1.;
      double fact=1.;
      if(fabs(delta) > 1.e-10*deltac){ // !=0){
        facn = deltan/delta;
        fact = _beta*fabs(deltat)/delta;
      }
      mhatmean(1,1) = m0(1,1)*t*facn; // Change this ??
      nhatmean(1,1) = n0(1,1)*t*facn;
      mhatmean(0,1) = mhatmean(1,0) = m0(0,1)*t*fact;
      nhatmean(0,1) = nhatmean(1,0) = n0(0,1)*t*fact;
//        printf("%lf %lf %lf\n",delta,mhatmean(1,1),m0(1,1));
    }
//    else
//      Msg::Error("Deltan is <0 in a tension case !");
  }
  else
  { // compression case
    double fact =1.;
    if(delta !=0.) fact = _beta;
    if(fabs(delta)<=fabs(deltac))
    {
      double c=0.;
      if(fabs(delta) >= fabs(delta_max) or (delta_max==0)) // loading case
      {
        if(delta/deltac >= 0)
          c = 1.- delta/deltac;
        else
          c = -1.-delta/deltac;
      }
      else //unloading case
      {
        if(fabs(delta_max)>(_tol*deltac))
        {
          c = delta/delta_max - delta/deltac;
        }
        else
        {
          if(delta_max*deltac >=0.)
            c=1.- delta_max/deltac;
          else
            c=-1.-delta_max/deltac;
        }
      }
      mhatmean(0,1) = mhatmean(1,0) = m0(0,1)*c*fact;
      nhatmean(0,1) = nhatmean(1,0) = n0(0,1)*c*fact;
    }
  }
}
void shellLinearCohesiveLaw::createIPVfrac(const double smax, const double sI, const double tauII,const bool ift,IPVariableShell *ipvcur, IPVariableShell *ipvprev)const
{
  // create ipvariable for fracture (Allocation --> previous too)
  IPShellFractureCohesive *ipvfcur = static_cast<IPShellFractureCohesive*>(ipvcur);
  IPShellFractureCohesive *ipvfprev= static_cast<IPShellFractureCohesive*>(ipvprev);
  IPLinearCohesiveShell *ipvccur = ipvfcur->getIPvFrac();
  IPLinearCohesiveShell *ipvcprev= ipvfprev->getIPvFrac();
  // if ipv have been created before (previous iteration delete first) CHANGE THIS just reset the function
  if(ipvccur !=NULL){delete ipvccur;}
  if(ipvcprev!=NULL){delete ipvcprev;}
  IPVariable* ipvccur2 = static_cast<IPVariable*>(ipvccur);
  IPVariable* ipvcprev2 = static_cast<IPVariable*>(ipvcprev);
  this->createIPVariable(ipvccur2);
  this->createIPVariable(ipvcprev2);
  ipvfcur->setIPvFrac(ipvccur2);
  ipvfprev->setIPvFrac(ipvcprev2);
  ipvfcur->setInitializationTime(_currentTime);
  ipvfprev->setInitializationTime(_currentTime);
  ipvccur = ipvfcur->getIPvFrac();
  ipvccur->setDataFromLaw(smax,sI,tauII,_Gc,_beta,ift);
  // needed for stiffness matrix in non linear
  ipvcprev = ipvfcur->getIPvFrac();
  ipvcprev->setDataFromLaw(smax,sI,tauII,_Gc,_beta,ift);
}

const bool shellLinearCohesiveLaw::checkBroken(IPVariableShell *ipvcur, IPVariableShell *ipvprev) const
{
  IPShellFractureCohesive *ipvfcur = static_cast<IPShellFractureCohesive*>(ipvcur);
  IPVariableShell *ipv = static_cast<IPVariableShell*>(ipvfcur->getIPvBulk());
  const shellLocalBasisInter *lbs = static_cast<const shellLocalBasisInter*>(ipv->getshellLocalBasisOfInterface());
  /* value of sigma_c which depends on random factor */
  const double effsigc = ipvfcur->fractureStrengthFactor() * _sigmac;

  // Look upper fiber first
  stressTensor stress;
  ipv->getUpperStressTensorBrokenEval(stress);
  // Normal and tangential components
  double snor = stress.getComponent(lbs->basisOrthonormalVector(1),lbs->basisOrthonormalVector(1));
  double tau = stress.getComponent(lbs->basisOrthonormalVector(0), lbs->basisOrthonormalVector(1));
  bool ift;
  double smax;
  double seff;
  // sigma eff (Camacho and Ortiz)
  if(snor/fabs(tau)>=0.001){ // check this (avoid problem with case snor approx 0)
    seff = sqrt(snor*snor+_betaexpm2*tau*tau);
    smax = snor;
    ift = true;
  }
  else{
    double temp = fabs(tau)-_mu*fabs(snor);
    if (temp >0 && _mu >=0.)
      seff = 1./_beta*temp;
    else
      seff = 0.;
    smax = tau;
    ift=false;

  }
  if(seff> effsigc){
    ipvfcur->broken();
  }
   // no computation for last Simpson's point if it already broken
  else{
    stressTensor stress;
    ipv->getLowerStressTensorBrokenEval(stress);
    snor = stress.getComponent(lbs->basisOrthonormalVector(1),lbs->basisOrthonormalVector(1));
    tau = stress.getComponent(lbs->basisOrthonormalVector(0), lbs->basisOrthonormalVector(1));
    // sigma eff (Camacho and Ortiz)
    if(snor>=0.){
      seff = sqrt(snor*snor+_betaexpm2*tau*tau);
      smax = snor;
      ift = true;
    }
    else{
      double temp = fabs(tau)-_mu*fabs(snor);
      if (temp >0 && _mu >=0.) //_mu >=0 added to avoid fracture in compression
        seff = 1./_beta*temp;
      else
        seff = 0.;
      smax = tau;
      ift=false;

    }
    if(seff> effsigc){
      ipvfcur->broken();
    }
  }
  if(ipvfcur->isbroken())
    {
    this->createIPVfrac(smax,snor,tau,ift,ipvcur,ipvprev);
    std::string _debugfilename;
    #if defined(HAVE_MPI)
    if(Msg::GetCommSize() > 1)
    {
       std::ostringstream oss;
       oss << Msg::GetCommRank();
        _debugfilename = "debug_part" + oss.str() + ".csv";
    }
    else
    #endif // HAVE_MPI
     {
         _debugfilename = "debug.csv";
     }
     double __deltac = 2*_Gc/smax;
     FILE *tmp = fopen(_debugfilename.c_str(), "a");
     fprintf(tmp, "effGc = %12.5E\n", _Gc);
     fprintf(tmp, "snor = %12.5E\n", snor);
     fprintf(tmp, "tau = %12.5E\n", tau);
     fprintf(tmp, "seff = %12.5E\n", seff);
     fprintf(tmp, "effsigc = %12.5E\n", effsigc);
     fprintf(tmp, "smax = %12.5E\n", smax);
     fprintf(tmp, "deltac = %12.5E\n", __deltac);
     fprintf(tmp, "-------------------------------------------\n");
     fclose(tmp);
  }
  return ipvfcur->isbroken();
}

 shellWeibullLinearCohesiveLaw::shellWeibullLinearCohesiveLaw(const int num, const double Gc, const double sigmacmin,const double beta,const double mu,
                                                              const double expm,
                                                              const double sc0) : shellLinearCohesiveLaw(num,Gc,sigmacmin,beta,mu,1.,1.e8),
                                                                                  _invm(1./expm), _sc0(sc0){}
 shellWeibullLinearCohesiveLaw::shellWeibullLinearCohesiveLaw(const shellWeibullLinearCohesiveLaw &source) : shellLinearCohesiveLaw(source),
                                                                                                             _invm(source._invm), _sc0(source._sc0){}

double shellWeibullLinearCohesiveLaw::getFractureStrengthFactor() const
{
  return ( (1./_sigmac) *(_sc0*(pow(-log(frand(1.e-10,1.)),_invm))+_sigmac) );
}

void shellWeibullLinearCohesiveLaw::createIPVfrac(const double smax,const double sI, const double tauII, const bool ift,IPVariableShell *ipvcur, IPVariableShell *ipvprev)const
{
  // create ipvariable for fracture (Allocation --> previous too)
  IPShellFractureCohesive *ipvfcur = static_cast<IPShellFractureCohesive*>(ipvcur);
  IPShellFractureCohesive *ipvfprev= static_cast<IPShellFractureCohesive*>(ipvprev);
  IPLinearCohesiveShell *ipvccur = ipvfcur->getIPvFrac();
  IPLinearCohesiveShell *ipvcprev= ipvfprev->getIPvFrac();
  double facSigma = ipvfprev->fractureStrengthFactor();
  // if ipv have been created before (previous iteration delete first) CHANGE THIS just reset the function
  if(ipvccur !=NULL){delete ipvccur;}
  if(ipvcprev!=NULL){delete ipvcprev;}
  IPVariable* ipvccur2 = static_cast<IPVariable*>(ipvccur);
  IPVariable* ipvcprev2 = static_cast<IPVariable*>(ipvcprev);
  this->createIPVariable(ipvccur2);
  this->createIPVariable(ipvcprev2);
  ipvfcur->setIPvFrac(ipvccur2);
  ipvfprev->setIPvFrac(ipvcprev2);
  ipvfcur->setInitializationTime(_currentTime);
  ipvfprev->setInitializationTime(_currentTime);
  ipvccur = ipvfcur->getIPvFrac();
  ipvccur->setDataFromLaw(smax,sI,tauII,_Gc*facSigma*facSigma,_beta,ift);
  // needed for stiffness matrix in non linear
  ipvcprev = ipvfcur->getIPvFrac();
  ipvcprev->setDataFromLaw(smax,sI,tauII,_Gc*facSigma*facSigma,_beta,ift);
}


 shellWeibullZhouLinearCohesiveLaw::shellWeibullZhouLinearCohesiveLaw(const int num, const double Gc, const double sc0,const double beta,const double mu,
                                                              const double expm) : shellLinearCohesiveLaw(num,Gc,sc0,beta,mu,1.,1.e8),
                                                                                  _invm(1./expm){}
 shellWeibullZhouLinearCohesiveLaw::shellWeibullZhouLinearCohesiveLaw(const shellWeibullZhouLinearCohesiveLaw &source) : shellLinearCohesiveLaw(source),
                                                                                                             _invm(source._invm){}

double shellWeibullZhouLinearCohesiveLaw::getFractureStrengthFactor() const
{
  return (pow(-log(frand(1.e-12,1.)),_invm));
}

void shellWeibullZhouLinearCohesiveLaw::createIPVfrac(const double smax,const double sI,const double tauII,const bool ift,IPVariableShell *ipvcur, IPVariableShell *ipvprev)const
{
  // create ipvariable for fracture (Allocation --> previous too)
  IPShellFractureCohesive *ipvfcur = static_cast<IPShellFractureCohesive*>(ipvcur);
  IPShellFractureCohesive *ipvfprev= static_cast<IPShellFractureCohesive*>(ipvprev);
  IPLinearCohesiveShell *ipvccur = ipvfcur->getIPvFrac();
  IPLinearCohesiveShell *ipvcprev= ipvfprev->getIPvFrac();
  double facSigma = ipvfprev->fractureStrengthFactor();
  // if ipv have been created before (previous iteration delete first) CHANGE THIS just reset the function
  if(ipvccur !=NULL){delete ipvccur;}
  if(ipvcprev!=NULL){delete ipvcprev;}
  IPVariable* ipvccur2 = static_cast<IPVariable*>(ipvccur);
  IPVariable* ipvcprev2 = static_cast<IPVariable*>(ipvcprev);
  this->createIPVariable(ipvccur2);
  this->createIPVariable(ipvcprev2);
  ipvfcur->setIPvFrac(ipvccur2);
  ipvfprev->setIPvFrac(ipvcprev2);
  ipvfcur->setInitializationTime(_currentTime);
  ipvfprev->setInitializationTime(_currentTime);
  ipvccur = ipvfcur->getIPvFrac();
  ipvccur->setDataFromLaw(smax,sI,tauII,_Gc*facSigma,_beta,ift); // smax = facSigma*sc --> Gc est divisé par facSigma to ensure a constant critical opening
  // needed for stiffness matrix in non linear
  ipvcprev = ipvfcur->getIPvFrac();
  ipvcprev->setDataFromLaw(smax,sI,tauII,_Gc*facSigma,_beta,ift);
}


// following part change by xingjun 10/2011, the objective is calculate the fractureStrengthFactor to get sigmac function
// with theta (angle of mesh) and the origentation of force

shellLinearCohesiveLawWithOrientation::shellLinearCohesiveLawWithOrientation (const int num, const double Gc100, const double Gc110, const double Gc111,
                                               const double sigmac100,const double sigmac110,const double sigmac111,
                                               const double beta,const double mu, double theta, double phi, double psi,
                                               const double fractureStrengthFactorMin, const double fractureStrengthFactorMax, const double _cohesive_law_tol)
                                               : shellLinearCohesiveLaw(num,Gc100,sigmac100,beta,mu,fractureStrengthFactorMin, fractureStrengthFactorMax, _cohesive_law_tol), _theta(theta), _phi(phi), _psi(psi),
                                                 _Gc100(Gc100),_Gc110(Gc110),_Gc111(Gc111),_sigmac100(sigmac100),_sigmac110(sigmac110),_sigmac111(sigmac111),
                                                 _Rt(theta,phi,psi), _symmetry_planes(sigmac100, sigmac110, sigmac111, Gc100, Gc110, Gc111)
                                               {
                                               _Rt.transposeInPlace();// get the R(-1),, R(-1)=R(T)

                                               }
shellLinearCohesiveLawWithOrientation::shellLinearCohesiveLawWithOrientation(const shellLinearCohesiveLawWithOrientation &source) : shellLinearCohesiveLaw(source),_Rt(source._Rt), _symmetry_planes(source._symmetry_planes){}

void shellLinearCohesiveLawWithOrientation::initDataFromLaw(const IPVariable *ipvlaw, IPVariable *ipv) const
{
  const IPLinearCohesiveShellWithOrientation* ipvlawc = static_cast<const IPLinearCohesiveShellWithOrientation*>(ipvlaw);
  IPLinearCohesiveShellWithOrientation* ipvc = static_cast<IPLinearCohesiveShellWithOrientation*>(ipv);
  ipvc->setDataFromLaw(ipvlawc->getSigmac(),ipvlawc->getSigI(),ipvlawc->getTauII(),ipvlawc->getEffGc(),ipvlawc->getBeta(),ipvlawc->ifTension(),ipvlawc->get_rotation_angle());
}

void shellLinearCohesiveLawWithOrientation::createIPState(IPStateBase* &ips,const bool* state_,
                                          const MElement *ele, const int nbFF_, const int gpt) const
{
  IPVariable *ipvi = new IPLinearCohesiveShellWithOrientation();
  IPVariable *ipv1 = new IPLinearCohesiveShellWithOrientation();
  IPVariable *ipv2 = new IPLinearCohesiveShellWithOrientation();
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

void shellLinearCohesiveLawWithOrientation::createIPVariable(IPVariable* &ipv,const MElement *ele,const int nbFF) const
{
  //ipv = new IPLinearCohesiveShell();
  ipv = new IPLinearCohesiveShellWithOrientation();//added by shantanu on 14-08-2012
}
//-------------------------------------------------------------------------------------------------------------------------
double shellLinearCohesiveLawWithOrientation::dot_prod(double *& n1, double *& n2) const//added by shantanu on 30-07-2012
{
   double temp = 0.0;
   //----------Initialization end--------------------------------------//
   temp = n1[0]*n2[0] + n1[1]*n2[1] + n1[2]*n2[2];
   return(temp);
}
//------------------------------------------------------------------------------------------------------------------------
const bool shellLinearCohesiveLawWithOrientation::checkBroken(IPVariableShell *ipvcur, IPVariableShell *ipvprev) const
{


  IPShellFractureCohesive *ipvfcur = static_cast<IPShellFractureCohesive*>(ipvcur);
  IPVariableShell *ipv = static_cast<IPVariableShell*>(ipvfcur->getIPvBulk());
  const shellLocalBasisInter *lbs = static_cast<const shellLocalBasisInter*>(ipv->getshellLocalBasisOfInterface());
 //-------added by shantanu on 5-07-2012----------------------------------------------------------//
 double effsigc=0.0, effGc=0.0, smax = 0.0, seff = 0.0, pi=22.0/7, snor=0.0, tau = 0.0, tau_0 = 0.0, tau_reslnt = 0.0;
 double n100 = 0.0, n110 = 0.0, n111 = 0.0;
 double dtemp1 = 0.0, dtemp2 = 0.0, dtemp3 = 0.0, _tmp = 0.0;
 double str_100_tmp = 0.0, str_110_tmp = 0.0, str_111_tmp = 0.0;
 double sigmaeff_x = 0.0, sigmaeff_y = 0.0, sigmaeff_z = 0.0;
 double Gc_100_tmp = 0.0, Gc_110_tmp = 0.0, Gc_111_tmp = 0.0;
 double Gceff_x = 0.0, Gceff_y = 0.0, Gceff_z = 0.0;
 double temp_theta = 0.0, temp_x = 0.0, temp_y = 0.0, temp_z = 0.0;
 int i = 0, j=0;
 double k = 0, theta_left = 0, theta_right = 0, theta_incrmnt = 0;
 double *n1_d_temp = NULL, *n2_d_temp = NULL, *n3_d_temp = NULL, *n_tmp = NULL;
 double *n1_temp = NULL, *n2_temp = NULL, *n3_temp = NULL;
 bool _is_found = false, ift = false;
 //---------------------End of initialization-------------------------------------------------------//
  SVector3 elementtangnt = lbs->basisOrthonormalVector(0);//extract in-plane tangent vector t = Phi_0[0]
  SVector3 elementNorm = lbs->basisOrthonormalVector(1);//extract in-plane normal vector n = Phi_0[1]
  SVector3 elementNorm_0 = lbs->basisOrthonormalVector(2);//extract out of plane normal vector t_0

  SVector3 elementtangnt_theta, elementNorm_theta, elementNorm_0_theta;// local convected basis vectors rotated in current configuration
  SVector3 n_refrnce, t_0_refrnce, t_refrnce;// local convected basis vectors in reference configuration

  theta_left = -45; theta_right = 45; theta_incrmnt = 2.5;//set limit of rotation angle about "vector t"
  // int mincriterion = 1.e15;

  for(k = theta_left; k<=theta_right;k+=theta_incrmnt)
  {
      //first rotate the local convected basis about vector t
      temp_theta = k*pi/180; temp_x = 0.0, temp_y = 0.0, temp_z = 0.0;
            //compute vector n^'
      temp_x = cos(temp_theta)*elementNorm.x() + sin(temp_theta)*elementNorm_0.x();
      temp_y = cos(temp_theta)*elementNorm.y() + sin(temp_theta)*elementNorm_0.y();
      temp_z = cos(temp_theta)*elementNorm.z() + sin(temp_theta)*elementNorm_0.z();
      elementNorm_theta(0) = temp_x; elementNorm_theta(1) = temp_y; elementNorm_theta(2) = temp_z;
      temp_x = 0.0; temp_y = 0.0; temp_z = 0.0;

            //compute t_0^'
      temp_x = -sin(temp_theta)*elementNorm.x() + cos(temp_theta)*elementNorm_0.x();
      temp_y = -sin(temp_theta)*elementNorm.y() + cos(temp_theta)*elementNorm_0.y();
      temp_z = -sin(temp_theta)*elementNorm.z() + cos(temp_theta)*elementNorm_0.z();
      elementNorm_0_theta(0) = temp_x; elementNorm_0_theta(1) = temp_y; elementNorm_0_theta(2) = temp_z;
      temp_x = 0.0; temp_y = 0.0; temp_z = 0.0;

            //compute t^'
      elementtangnt_theta = elementtangnt;
      //convert all rotated vectors into unit length vectors
      _tmp = elementNorm_theta.normalize(); _tmp = elementNorm_0_theta.normalize(); _tmp = elementtangnt_theta.normalize();

      //transfer all vectors in reference configuration
      matvectprod(_Rt,elementNorm_theta,n_refrnce); matvectprod(_Rt,elementNorm_0_theta,t_0_refrnce);
      matvectprod(_Rt,elementtangnt_theta,t_refrnce);

      n1_temp = new double(3); n2_temp = new double(3); n3_temp = new double(3);
      n1_d_temp = new double(3); n2_d_temp = new double(3); n3_d_temp = new double(3); n_tmp = new double(3);
      for(i=0;i<3;i++)//initialization
      {
          n1_d_temp[i] = 0.0; n2_d_temp[i] = 0.0; n3_d_temp[i] = 0.0;
          n1_temp[i] = 0.0; n2_temp[i] = 0.0; n3_temp[i] = 0.0; n_tmp[i] = 0.0;
      }
      n_tmp[0] =n_refrnce.x(); n_tmp[1] =n_refrnce.y(); n_tmp[2] =n_refrnce.z();
      for(i = 0; i<48;i++)//there are 48 combinations of symmetry plane vectors
      {
          _symmetry_planes.get_dual_vector_set(i, n1_d_temp, n2_d_temp, n3_d_temp);
          _symmetry_planes.get_basis_vector_set(i, n1_temp, n2_temp, n3_temp);
          _symmetry_planes.get_fracture_strengths(i, str_100_tmp, str_110_tmp, str_111_tmp);//already correctly swapped for positive volume
          _symmetry_planes.get_Gc_values(i, Gc_100_tmp, Gc_110_tmp, Gc_111_tmp); //already correctly swapped for positive volume

          dtemp1 = dot_prod(n1_d_temp, n_tmp); dtemp2 = dot_prod(n2_d_temp, n_tmp); dtemp3 = dot_prod(n3_d_temp, n_tmp);
          if(dtemp1 >=0.0 && dtemp2>=0.0 && dtemp3>=0.0)
          {
              _is_found = true; break;
          }
      }
      if(_is_found == true)
      {
          //compute effective fracture strength
          sigmaeff_x = (str_100_tmp * dtemp1 * n1_temp[0]) + (str_110_tmp * dtemp2 * n2_temp[0]) + (str_111_tmp * dtemp3 * n3_temp[0]);
          sigmaeff_y = (str_100_tmp * dtemp1 * n1_temp[1]) + (str_110_tmp * dtemp2 * n2_temp[1]) + (str_111_tmp * dtemp3 * n3_temp[1]);
          sigmaeff_z = (str_100_tmp * dtemp1 * n1_temp[2]) + (str_110_tmp * dtemp2 * n2_temp[2]) + (str_111_tmp * dtemp3 * n3_temp[2]);
          effsigc = sqrt((sigmaeff_x*sigmaeff_x) + (sigmaeff_y*sigmaeff_y) + (sigmaeff_z*sigmaeff_z));
          effsigc = effsigc * ipvfcur->fractureStrengthFactor();

          //compute effective surface energy
          Gceff_x = (Gc_100_tmp * dtemp1 * n1_temp[0]) + (Gc_110_tmp * dtemp2 * n2_temp[0]) + (Gc_111_tmp * dtemp3 * n3_temp[0]);
          Gceff_y = (Gc_100_tmp * dtemp1 * n1_temp[1]) + (Gc_110_tmp * dtemp2 * n2_temp[1]) + (Gc_111_tmp * dtemp3 * n3_temp[1]);
          Gceff_z = (Gc_100_tmp * dtemp1 * n1_temp[2]) + (Gc_110_tmp * dtemp2 * n2_temp[2]) + (Gc_111_tmp * dtemp3 * n3_temp[2]);
          effGc = sqrt((Gceff_x*Gceff_x) + (Gceff_y*Gceff_y) + (Gceff_z*Gceff_z));
          //effGc = effGc * ipvfcur->fractureStrengthFactor();
      }
      else if(_is_found == false)
        printf("Caution: No solid angle is captured, effective fracture strength and surface energy may be wrong\n");

      //free all temporary memory
      delete []n1_d_temp; n1_d_temp = NULL; delete []n2_d_temp; n2_d_temp = NULL; delete []n3_d_temp; n3_d_temp = NULL;
      delete []n_tmp; n_tmp = NULL; delete []n1_temp; n1_temp = NULL; delete []n2_temp; n2_temp = NULL; delete []n3_temp; n3_temp = NULL;

      //compute effective and maximum stresses
      stressTensor stress;
      ipv->getUpperStressTensorBrokenEval(stress);
      // extract normal and tangential components of stress
      snor = stress.getComponent(elementNorm_theta, elementNorm_theta);//sigma_{n^',n^'}, eg. sigma_{xx}
      tau = stress.getComponent(elementtangnt_theta, elementNorm_theta);//sigma_{n^', t^'}, eg. sigma_{xy}
      tau_0 = stress.getComponent(elementNorm_0_theta, elementNorm_theta);//sigma_{n^', t_0^'}, eg. sigma_{xz}
      tau_reslnt = sqrt((tau*tau) + (tau_0*tau_0));//compute resultant shear stress in the plane of interface element
      ift = false;
     // printf("beta value = %f\n", _beta);

      if(snor>=0.) // sigma eff (Camacho and Ortiz)
      { // check this (avoid problem with case snor approx 0)
          seff = sqrt(snor*snor+(_betaexpm2*tau_reslnt*tau_reslnt));
          smax = stress.getComponent(elementNorm_theta, elementNorm_theta); //snor; //tension casesnor; //tension case
          ift = true;
      }
      else
      {
          double temp = fabs(tau_reslnt) - (_mu*fabs(snor));
          if(temp >0 && _mu >= 0.)
            seff = 1./_beta*temp;
          else
            seff = 0.0;
          smax =  tau_reslnt*sign(tau); //tau; //compression case
          ift=false;
      }//esle

      if(seff> effsigc)
      {
          ipvfcur->broken();
      }
      else
      {
          stressTensor stress;
          ipv->getLowerStressTensorBrokenEval(stress);
          snor = stress.getComponent(elementNorm_theta, elementNorm_theta);//sigma_{n^',n^'}, eg. sigma_{xx}
          tau = stress.getComponent(elementtangnt_theta, elementNorm_theta);//sigma_{n^', t^'}, eg. sigma_{xy}
          tau_0 = stress.getComponent(elementNorm_0_theta, elementNorm_theta);//sigma_{n^', t_0^'}, eg. sigma_{xz}
          tau_reslnt = sqrt((tau*tau) + (tau_0*tau_0));
          if(snor>=0.0) // sigma eff (Camacho and Ortiz)
          {
              seff = sqrt(snor*snor+(_betaexpm2*tau_reslnt*tau_reslnt));
              smax = stress.getComponent(elementNorm_theta, elementNorm_theta); //snor; //tension case
              ift = true;
          }
          else
          {
              double temp = fabs(tau_reslnt)-(_mu*fabs(snor));
              if (temp >0 && _mu >= 0.)
                seff = 1./_beta*temp;
              else
                seff = 0.;
              smax = tau_reslnt*sign(tau); //tau; //compression case
              ift=false;
          }
          if(seff> effsigc)
          {
              ipvfcur->broken();
          }
      }//esle

      if(ipvfcur->isbroken())
      {
          // create ipvariable for fracture (Allocation --> previous too)
          IPShellFractureCohesive *ipvfprev= dynamic_cast<IPShellFractureCohesive*>(ipvprev);
          IPLinearCohesiveShellWithOrientation *ipvcwocur = dynamic_cast<IPLinearCohesiveShellWithOrientation*>(ipvfcur->getIPvFrac());
          IPLinearCohesiveShellWithOrientation *ipvcwoprev = dynamic_cast<IPLinearCohesiveShellWithOrientation*>(ipvfprev->getIPvFrac());
          if(ipvcwocur != NULL)
          {
              delete ipvcwocur; ipvcwocur = NULL;
          }
          if(ipvcwoprev != NULL)
          {
              delete ipvcwoprev; ipvcwoprev = NULL;
          }

          IPVariable *ipvcwocur2 = dynamic_cast<IPVariable*>(ipvcwocur);
          IPVariable *ipvcwoprev2 = dynamic_cast<IPVariable*>(ipvcwoprev);

          this->createIPVariable(ipvcwocur2);
          this->createIPVariable(ipvcwoprev2);

          ipvfcur->setIPvFrac(ipvcwocur2);
          ipvfprev->setIPvFrac(ipvcwoprev2);

          ipvfcur->setInitializationTime(_currentTime);
          ipvfprev->setInitializationTime(_currentTime);

          ipvcwocur = dynamic_cast<IPLinearCohesiveShellWithOrientation*>(ipvfcur->getIPvFrac());
          ipvcwoprev = dynamic_cast<IPLinearCohesiveShellWithOrientation*>(ipvfprev->getIPvFrac());
          double Gc_modfed = effGc/cos(temp_theta);
          double snorIni = stress.getComponent(elementNorm, elementNorm);//sigma_{n^,n}
          double tauIni = stress.getComponent(elementtangnt, elementNorm);//sigma_{n,t^}
          ipvcwocur->setDataFromLaw(smax, snorIni, tauIni, Gc_modfed, _beta, ift, temp_theta);
          ipvcwoprev->setDataFromLaw(smax, snorIni, tauIni, Gc_modfed, _beta, ift, temp_theta);
          std::string _debugfilename;
          #if defined(HAVE_MPI)
          if(Msg::GetCommSize() > 1)
          {
              std::ostringstream oss;
              oss << Msg::GetCommRank();
              _debugfilename = "debug_part" + oss.str() + ".csv";
           }
           else
            #endif // HAVE_MPI
           {
               _debugfilename = "debug.csv";
           }
           double __deltac_theta = 2*Gc_modfed/smax; double deltac_plane = __deltac_theta/cos(temp_theta);
          FILE *tmp = fopen(_debugfilename.c_str(), "a");
          fprintf(tmp, "theta_degrees =  %f\n", k);
          fprintf(tmp, "theta_radians = %f\n", temp_theta);
          fprintf(tmp, "effGc = %12.5E\n", effGc);
          fprintf(tmp, "Gc_modfed (effGc/cos(theta_radians)) = %12.5E\n", Gc_modfed);
          fprintf(tmp, "snor_theta = %12.5E\n", snor);
          fprintf(tmp, "tau_theta = %12.5E\n", tau);
          fprintf(tmp, "tau_0_theta = %12.5E\n", tau_0);
          fprintf(tmp, "tau_reslnt = %12.5E\n", tau_reslnt);
          fprintf(tmp, "seff_theta = %12.5E\n", seff);
          fprintf(tmp, "effsigc_theta = %12.5E\n", effsigc);
          fprintf(tmp, "smax_theta = %12.5E\n", smax);
          fprintf(tmp, "snorIni = %12.5E\n", snorIni);
          fprintf(tmp, "tauIni = %12.5E\n", tauIni);
          fprintf(tmp, "deltac_theta = %12.5E\n", __deltac_theta);
          fprintf(tmp, "deltac_plane (deltac_theta/cos(temp_theta)) = %12.5E\n", deltac_plane);
          fprintf(tmp, "-------------------------------------------\n");
          fclose(tmp);
          break;//break the outer "for loop" over the angle theta
      }//fi (ipvfcur->isbroken())
  }//end for(k = theta_left; k<=theta_right;k+=theta_incrmnt)
  return ipvfcur->isbroken();
}

// up part change by xingjun 10/2011




shellLinearCohesiveLawWithDamage::shellLinearCohesiveLawWithDamage(const int num, const double Gc, const double Dc,
                                               const double beta,const double mu,const double damageFactorMin,
                                               const double damageFactorMax) : shellLinearCohesiveLaw(num,Gc,Dc,beta,mu,damageFactorMin,
                                                                                                      damageFactorMax){} // put Dc in sigmac !!
shellLinearCohesiveLawWithDamage::shellLinearCohesiveLawWithDamage(const shellLinearCohesiveLawWithDamage &source) : shellLinearCohesiveLaw(source){}


const bool shellLinearCohesiveLawWithDamage::checkBroken(IPVariableShell *ipvcur, IPVariableShell *ipvprev) const
{
  IPShellFractureCohesive *ipvfcur = static_cast<IPShellFractureCohesive*>(ipvcur);
  IPVariableShell *ipv = static_cast<IPVariableShell*>(ipvfcur->getIPvBulk());
  IPLinearShellWD *ipvd = static_cast<IPLinearShellWD*>(ipvfcur->getIPvBulk());
  double smax;
  bool  ift;
  double &D = ipvd->getDamage(0);
  //double &Bifur=ipvd->getBifurcation(0); // not used
   //   Msg::Info("Bifurcation %e",Bifur);
  const double Dc = ipvfcur->fractureStrengthFactor() * _sigmac; // Dc is stored in _sigmac !!
  if (D>Dc){
//   Msg::Info("Bifurcation %e",Bifur);
//   Msg::Info("Broken point");
    const shellLocalBasisInter *lbs = static_cast<const shellLocalBasisInter*>(ipvd->getshellLocalBasisOfInterface());
    // Look upper fiber first
    stressTensor stress;
    ipvd->getUpperStressTensorBrokenEval(stress);
    // Normal and tangential components
    double snor = stress.getComponent(lbs->basisOrthonormalVector(1),lbs->basisOrthonormalVector(1));
    double tau = stress.getComponent(lbs->basisOrthonormalVector(0), lbs->basisOrthonormalVector(1));
    if(snor>=0.){
      smax = snor;
      ift = true;
    }
    else{
      smax = tau;
      ift=false;
    }
    ipvfcur->broken();

    // create ipvariable for fracture (Allocation --> previous too)
    IPShellFractureCohesive *ipvfprev= static_cast<IPShellFractureCohesive*>(ipvprev);
    IPLinearCohesiveShell *ipvccur = ipvfcur->getIPvFrac();
    IPLinearCohesiveShell *ipvcprev= ipvfprev->getIPvFrac();
    // if ipv have been created before (previous iteration delete first) CHANGE THIS just reset the function
    if(ipvccur !=NULL){delete ipvccur;}
    if(ipvcprev!=NULL){delete ipvcprev;}
    IPVariable* ipvccur2 = static_cast<IPVariable*>(ipvccur);
    IPVariable* ipvcprev2 = static_cast<IPVariable*>(ipvcprev);
    this->createIPVariable(ipvccur2);
    this->createIPVariable(ipvcprev2);
    ipvfcur->setIPvFrac(ipvccur2);
    ipvfprev->setIPvFrac(ipvcprev2);
    ipvccur = ipvfcur->getIPvFrac();
    ipvccur->setDataFromLaw(smax,snor,tau,_Gc,_beta,ift);
    // needed for stiffness matrix in non linear
    ipvcprev = ipvfcur->getIPvFrac();
    ipvcprev->setDataFromLaw(smax,snor,tau,_Gc,_beta,ift);
  }
  return ipvfcur->isbroken();
}



