//
// C++ Interface: material law
//
// Description: Array with the symmetric planes of polycrystalline silicon material
//
//
// Author:  <Shantanu MULAY>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
#ifndef SYMMETRICPLANEARRAY_H_
#define SYMMETRICPLANEARRAY_H_
#include <math.h>
#include <stdio.h>

class symmetry_planes
{
protected:
//    double sqrt2, sqrt3;
    double _sigmac100, _sigmac110, _sigmac111, _Gc100, _Gc110, _Gc111;
    double family_1[48][3], family_2[48][3], family_3[48][3];//family_1:(100), family_2:(110), family_3:(111) plane
    double fracture_strength_array[48][3];//fracture_strength[][0]:family_1, fracture_strength[][1]:family_2, fracture_strength[][2]:family_3
    double Gc_array[48][3]; //Gc_array[][0]:family_1, Gc_array[][1]:family_2, Gc_array[][2]:family_3
    int is_n2_n3_swapped[48];//if is_n2_n3_swapped == 0: not swapped, else if == 1: swapped
    double n1_d[48][3], n2_d[48][3], n3_d[48][3];//dual vector arrays for vectors giving positive volume, i.e., already swapped
public:
    symmetry_planes(const double &sigmac100, const double &sigmac110, const double &sigmac111, const double &Gc100, const double &Gc110, const double &Gc111);
    symmetry_planes(const symmetry_planes &src);
    void get_dual_vector_set(int &i, double *&n1d, double *&n2d, double *&n3d) const;
    void get_fracture_strengths(int &i, double &tmp1, double &tmp2, double &tmp3) const;
    void get_Gc_values(int &i, double &tmp1, double &tmp2, double &tmp3) const;
    void get_basis_vector_set(int &i, double *&n1_tmp, double *&n2_tmp, double *&n3_tmp) const;
    symmetry_planes & operator=(const symmetry_planes &_rhs);
};
#endif // SYMMETRICPLANEARRAY_H_
