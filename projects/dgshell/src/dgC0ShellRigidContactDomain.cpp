//
// C++ Interface: terms
//
// Description: Domain of contact for rigid contact for shell. Has to be here to create the good functional space
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
#include "dgC0ShellRigidContactDomain.h"
#include "dgC0ShellRigidContactSpace.h"

void dgC0ShellRigidCylinderContact::setDomainAndFunctionSpace(partDomain *dom){
  _dom = dom;
  FunctionSpace<double> *spdom = static_cast<FunctionSpace<double>*>(dom->getFunctionSpace());
  if(_space != NULL) delete _space;
  _space = new dgC0ShellRigidContactSpace(_phys,spdom,_vergc);
}

void dgC0ShellRigidConeContact::setDomainAndFunctionSpace(partDomain *dom){
  _dom = dom;
  FunctionSpace<double> *spdom = static_cast<FunctionSpace<double>*>(dom->getFunctionSpace());
  if(_space != NULL) delete _space;
  _space = new dgC0ShellRigidContactSpace(_phys,spdom,_vergcTot);
}

