//
// C++ Interface: partDomain
//
// Description: Interface class for dg shell
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "nonLinearMechSolver.h"
#include "dgC0ShellTerms.h"
#include "dgNonLinearShellTerms.h"
#include "dgShellDomain.h"
#include "dgC0ShellAlgo.h"
#include "IEdge.h"
#include "GaussLobattoQuadratureRules.h"
#include "nonLinearInterDomainShell.h"
#include "quadratureTriQuadMix.h"

dgLinearShellDomain::dgLinearShellDomain (const int tag,const int phys, const int ws, const int lnum,
                                          const int fdg) : dgPartDomain(tag,phys,ws,fdg), _lnum(lnum),
                                                            _beta1(10.),_beta2(10.), _beta3(10.),_sts(1.),
                                                            _space(NULL), _spaceMinus(NULL), _spacePlus(NULL),
                                                            _nvarDomain(), _gqt(Gauss), _gaussorderbulk(-1), _gaussorderbound(-1),
                                                            _sameValueInterfaceGaussPoints(false)
{
  switch(_wsp){
   case functionSpaceType::Lagrange:
    if(_fullDg)
      _space = new DgC0FullDgLagrangeFunctionSpace(_phys);
    else
      _space = new DgC0CgDgLagrangeFunctionSpace(_phys);
    break;
   case functionSpaceType::Inter :
    break; // InterfaceDomain set function space later
   default:
    Msg::Error("Function space type is unknown on domain %d",_phys);
  }
  _spaceMinus = _space;
  _spacePlus = _space;
}
dgLinearShellDomain::dgLinearShellDomain(const dgLinearShellDomain &source) : dgPartDomain(source), _lnum(source._lnum),
                                                                               _mlaw(source._mlaw), _beta1(source._beta1),
                                                                               _beta2(source._beta2), _beta3(source._beta3),
                                                                               _sts(source._sts), _space(source._space),
                                                                               _spaceMinus(source._spaceMinus),
                                                                               _spacePlus(source._spacePlus),
                                                                               _nvarDomain(), _gqt(source._gqt),
                                                                               _gaussorderbulk(source._gaussorderbulk),
                                                                               _gaussorderbound(source._gaussorderbound),
                                                                               _interQuad(source._interQuad),
                                                                               _sameValueInterfaceGaussPoints(source._sameValueInterfaceGaussPoints) {}

void dgLinearShellDomain::setMaterialLaw(const std::map<int,materialLaw*> maplaw)
{
  if(!setmaterial){
    for(std::map<int,materialLaw*>::const_iterator it=maplaw.begin(); it!=maplaw.end(); ++it){
      if(it->first == _lnum){
        _mlaw = it->second;
        _mlaw->initLaws(maplaw);
      }
    }
    setmaterial = true;
  }

}

void dgLinearShellDomain::stabilityParameters(const double b1, const double b2, const double b3)
{
  _beta1 = b1;
  _beta2 = b2;
  _beta3 = b3;
  if(_beta2 <= 1.)
    _sts = 1./sqrt(_beta1);
  else
     _sts= 1./sqrt(_beta2);
}

const double dgLinearShellDomain::stabilityParameter(const int i) const
{
  switch(i)
  {
   case 1:
    return _beta1;
   case 2:
    return _beta2;
   case 3:
    return _beta3;
   default:
    Msg::Error("This domain has only 3 stability parameters !");
    return 0.;
  }
}

void dgLinearShellDomain::matrixByPerturbation(const int ibulk, const int iinter, const int ivirt,const double eps)
{
  this->setBulkMatrixByPerturbation(ibulk,eps);
  iinter == 0 ? _interByPert = false : _interByPert = true;
  ivirt == 0 ? _virtByPert = false : _virtByPert = true;
}

void dgLinearShellDomain::sameStatisticValuesOnInterfaceGaussPoints()
{
  _sameValueInterfaceGaussPoints = true;
}

FunctionSpaceBase* dgLinearShellDomain::getSpaceForBC(const nonLinearBoundaryCondition &boundary, const groupOfElements *groupBC) const
{
  FunctionSpaceBase* spacebc;
  if(boundary.getType() == nonLinearBoundaryCondition::DIRICHLET)
  {
    const nonLinearDirichletBC* diri = static_cast<const nonLinearDirichletBC*>(&boundary);
    switch(_wsp){
     case functionSpaceType::Lagrange:
      if(_fullDg){
        if(boundary.onWhat !=nonLinearBoundaryCondition::ON_FACE)
          spacebc= new DgC0FullDgBoundaryConditionLagrangeFunctionSpace(_phys,diri->_comp,groupBC,gi,giv,_groupGhostMPI);
        else
          spacebc = new DgC0FullDgLagrangeFunctionSpace(_phys,diri->_comp);
      }
      else{
        // For MPI check if the BC is applied on element or ghost element
        // the BC being applied on the element of the BC with maybe element->getPartition()==0 !!
       #if defined(HAVE_MPI)
        if(Msg::GetCommSize()>1)
        {
           groupOfElements::vertexContainer::const_iterator itv1=g->vend();
           groupOfElements::vertexContainer::const_iterator itv2=g->vend();
           groupOfElements::vertexContainer::const_iterator it = groupBC->vbegin();
           MVertex* bver1 = *it;
           MVertex* bver2 = *(++it);
           for(groupOfElements::vertexContainer::const_iterator itv=g->vbegin(); itv!=g->vend();++itv)
           {
             MVertex* ver = *itv;
             if(ver->getNum()==bver1->getNum() or ver->getNum()==bver2->getNum())
             {
                if(itv1==g->vend()) itv1 = itv;
                else
                {
                  itv2 = itv;
                  break;
                }
             }
           }
           if(itv2!=g->vend() and g->size()>0)
           {
             spacebc = new DgC0CgDgBoundaryConditionLagrangeFunctionSpace(_phys,diri->_comp);
           }
           else
           {
             spacebc = new DgC0CgDgBoundaryConditionLagrangeFunctionSpaceGhost(_phys,diri->_comp);
           }
        }
        else
        #endif //HAVE_MPI
        {
          spacebc = new DgC0CgDgBoundaryConditionLagrangeFunctionSpace(_phys,diri->_comp);
        }
      }
      break;
     default :
       Msg::Error("Unknown function space type for dirichlet BC %d on domain %d",boundary._tag,_phys);
    }
  }
  else if(boundary.getType() == nonLinearBoundaryCondition::NEUMANN)
  {
    const nonLinearNeumannBC *neu = static_cast<const nonLinearNeumannBC*>(&boundary);
    switch(_wsp){
     case functionSpaceType::Lagrange :
      if(_fullDg){
        if((boundary.onWhat != nonLinearBoundaryCondition::ON_FACE) and (boundary.onWhat != nonLinearBoundaryCondition::PRESSURE))
        {
          if(boundary.onWhat != nonLinearBoundaryCondition::ON_FACE_VERTEX)
          {
            spacebc = new DgC0FullDgBoundaryConditionLagrangeFunctionSpace(_phys,neu->_comp,groupBC,gi,giv,NULL); // no ghostMPI for Neumann BC
          }
          else
          {
            spacebc = new DgC0FullDgLagrangeFunctionSpace(_phys);
          }
        }
        else if(neu->onWhat == nonLinearBoundaryCondition::ON_FACE)
            spacebc = new DgC0FullDgLagrangeFunctionSpace(_phys,neu->_comp); // no ghostMPI for Neumann BC
        else
          spacebc = new DgC0FullDgLagrangeFunctionSpace(_phys);
      }
      else{
        if((neu->onWhat != nonLinearBoundaryCondition::ON_FACE) and (boundary.onWhat != nonLinearBoundaryCondition::PRESSURE))
        {
          if(boundary.onWhat != nonLinearBoundaryCondition::ON_FACE_VERTEX)
            spacebc = new DgC0CgDgBoundaryConditionLagrangeFunctionSpace(_phys,neu->_comp);
          else
            spacebc = new DgC0CgDgBoundaryConditionLagrangeFunctionSpace(_phys);
        }
        else if(neu->onWhat == nonLinearBoundaryCondition::ON_FACE)
            spacebc = new DgC0CgDgBoundaryConditionLagrangeFunctionSpace(_phys,neu->_comp);
        else
          spacebc = new DgC0CgDgBoundaryConditionLagrangeFunctionSpace(_phys);
     }
     break;
     default :
      Msg::Error("Unknown function space type on neumman BC %d on domain %d",boundary._tag,_phys);
    }
  }
  else if(boundary.getType() == nonLinearBoundaryCondition::INITIAL)
  {
    switch(_wsp){
     case functionSpaceType::Lagrange:
      if(_fullDg){
        if(boundary.onWhat !=nonLinearBoundaryCondition::ON_FACE)
          spacebc = new DgC0FullDgBoundaryConditionLagrangeFunctionSpace(_phys,groupBC,gi,giv,_groupGhostMPI);
        else
          spacebc = new DgC0FullDgLagrangeFunctionSpace(_phys);
      }
      else{
        spacebc = new DgC0CgDgBoundaryConditionLagrangeFunctionSpace(_phys);
      }
      break;
     default :
       Msg::Error("Unknown function space type for dirichlet BC %d on domain %d",boundary._tag,_phys);
    }
  }
  return spacebc;
}

QuadratureBase* dgLinearShellDomain::getQuadratureRulesForNeumannBC(nonLinearNeumannBC &neu) const
{
  QuadratureBase *integ;
  if((neu.onWhat == nonLinearBoundaryCondition::PRESSURE) or (neu.onWhat == nonLinearBoundaryCondition::ON_FACE) or
     (neu.onWhat == nonLinearBoundaryCondition::ON_FACE_VERTEX)){
    if(GaussQuadrature *intgauss = dynamic_cast<GaussQuadrature*>(integBulk)){
      integ = new GaussQuadrature(*intgauss);
    }
    else{
      quadratureGaussTriQuadMix*integmix = static_cast<quadratureGaussTriQuadMix*>(integBulk);
      integ = new quadratureGaussTriQuadMix(*integmix);
    }
  }
  else{
    if(_gqt == Gauss){
      GaussQuadrature *intgauss = static_cast<GaussQuadrature*>(integBound);
      integ = new GaussQuadrature(*intgauss);
    }
    else{
      GaussLobatto1DQuadrature *intgauss = static_cast<GaussLobatto1DQuadrature*>(integBound);
      integ = new GaussLobatto1DQuadrature(*intgauss);
    }
  }
  return integ;
}

void dgLinearShellDomain::initializeTerms(unknownField *uf,IPField*ip,std::vector<nonLinearNeumannBC> &vNeumann)
{
  FunctionSpace<double>* dgspace = static_cast<FunctionSpace<double>*>(_space);
  ltermBulk = new IsotropicForceBulkTermC0DgShell(*dgspace,_mlaw,_fullDg,ip);
  ltermBound = new IsotropicForceInterTermC0DgShell(*dgspace,dgspace,_mlaw,_mlaw,_interQuad,_beta1,_beta2,_beta3,_fullDg,ip);
  ltermVirtBound = new IsotropicElasticForceVirtualInterfaceTermC0DgShell(*dgspace,_mlaw,_interQuad,_beta1,_beta2,_beta3,_fullDg,ip);
  if(_bmbp)
    btermBulk = new BilinearTermPerturbation<double>(ltermBulk,*dgspace,*dgspace,uf,ip,this,_eps);
  else
   btermBulk = new IsotropicStiffBulkTermC0DgShell(*dgspace,_mlaw,_fullDg,ip);
  if(_interByPert)
    btermBound = new BilinearTermPerturbation<double>(ltermBound,*dgspace,*dgspace,uf,ip,this,_eps);
  else
    btermBound = new IsotropicStiffInterTermC0DgShell(*dgspace,*dgspace,_mlaw,_mlaw,_interQuad,_beta1,_beta2,_beta3,ip,uf,_fullDg);
  if(_virtByPert)
    btermVirtBound = new BilinearTermPerturbation<double>(ltermVirtBound,*dgspace,*dgspace,uf,ip,this,_eps);
  else
    btermVirtBound = new IsotropicStiffVirtualInterfaceTermC0DgShell(*dgspace,_mlaw,_interQuad,_beta1,_beta2,_beta3,uf,ip,_fullDg);
  massterm = new massC0DgShell(*dgspace,_mlaw);

  // term of neumannBC
  for(int i=0;i<vNeumann.size();i++){
    nonLinearNeumannBC &neu = vNeumann[i];
    for(int j=0;j<neu._vspace.size();j++){
      if(neu._vdom[j] == this)
      {
//      if(neu._vterm[j] != NULL) delete neu._vterm[j];
        FunctionSpace<double> *spneu = static_cast<FunctionSpace<double>*>(neu._vspace[j]);
        if((neu.onWhat != nonLinearBoundaryCondition::PRESSURE) and(neu.onWhat != nonLinearBoundaryCondition::ON_FACE_VERTEX))
          neu._vterm[j] = new DgC0LoadTerm<double>(*spneu,neu._f);
        else if(neu.onWhat == nonLinearBoundaryCondition::PRESSURE)
          neu._vterm[j] = new DgC0PressureLoadTerm(*spneu,neu._f,ip);
        else{
          shellMaterialLaw *slaw = static_cast<shellMaterialLaw*>(_mlaw);
          neu._vterm[j] = new DgC0VertexLoadTerm<double>(*spneu,neu._f,neu._comp,slaw->getThickness());
        }
      }
    }
  }
  /* same value for fracture strength on both side */
  // not very elegant put very efficient to put here
  if(_mlaw->getType() == materialLaw::fracture)
  {
    this->ensureSameValuesBothSideIPvariable(ip);
  }
}

void dgLinearShellDomain::ensureSameValuesBothSideIPvariable(IPField *ip)
{
  // loop on ipvariable
  IntPt *GP;
  if(!_sameValueInterfaceGaussPoints)
  {
    for(groupOfElements::elementContainer::iterator it=gi->begin(); it!=gi->end(); ++it)
    {
      MElement *ele = *it;
      int npts_inter=integBound->getIntPoints(ele,&GP);
      AllIPState::ipstateElementContainer *vips = ip->getAips()->getIPstate(ele->getNum());
      for(int j=0;j<npts_inter;j++){ // npts_inter points on both sides
        IPStateBase* ipsm = (*vips)[j];
        IPStateBase* ipsp = (*vips)[j+npts_inter];
        IPShellFractureCohesive *ipvm = static_cast<IPShellFractureCohesive*>(ipsm->getState(IPStateBase::initial));
        IPShellFractureCohesive *ipvp = static_cast<IPShellFractureCohesive*>(ipsp->getState(IPStateBase::initial));
        ipvp->setFractureStrengthFactor(ipvm->fractureStrengthFactor());
        ipvm = static_cast<IPShellFractureCohesive*>(ipsm->getState(IPStateBase::previous));
        ipvp = static_cast<IPShellFractureCohesive*>(ipsp->getState(IPStateBase::previous));
        ipvp->setFractureStrengthFactor(ipvm->fractureStrengthFactor());
        ipvm = static_cast<IPShellFractureCohesive*>(ipsm->getState(IPStateBase::current));
        ipvp = static_cast<IPShellFractureCohesive*>(ipsp->getState(IPStateBase::current));
        ipvp->setFractureStrengthFactor(ipvm->fractureStrengthFactor());
      }
    }
  }
  else // take the value of minus 0 point and put on all other Gauss point
  {
    for(groupOfElements::elementContainer::iterator it=gi->begin(); it!=gi->end(); ++it)
    {
      MElement *ele = *it;
      int npts_inter=integBound->getIntPoints(ele,&GP);
      AllIPState::ipstateElementContainer *vips = ip->getAips()->getIPstate(ele->getNum());
      IPStateBase* ips = (*vips)[0];
      IPShellFractureCohesive *ipv = static_cast<IPShellFractureCohesive*>(ips->getState(IPStateBase::initial));
      double cohesiveStrength = ipv->fractureStrengthFactor();
      for(int j=1;j<2*npts_inter;j++){ // npts_inter points on both sides
        ips = (*vips)[j];
        IPShellFractureCohesive *ipv = static_cast<IPShellFractureCohesive*>(ips->getState(IPStateBase::initial));
        ipv->setFractureStrengthFactor(cohesiveStrength);
        ipv = static_cast<IPShellFractureCohesive*>(ips->getState(IPStateBase::previous));
        ipv->setFractureStrengthFactor(cohesiveStrength);
        ipv = static_cast<IPShellFractureCohesive*>(ips->getState(IPStateBase::current));
        ipv->setFractureStrengthFactor(cohesiveStrength);
      }
    }
  }
}

void dgLinearShellDomain::computeIPVariable(AllIPState *aips,const unknownField *ufield, const IPStateBase::whichState ws, bool stiff)
{
  IntPt *GP;
  fullVector<double> dispm;
  fullVector<double> dispp;
  std::vector<Dof> R;
  // interface elements
  partDomain *dom = static_cast<partDomain*>(this);
  for(groupOfElements::elementContainer::const_iterator it=gi->begin(); it!=gi->end();++it){
    MInterfaceLine *ie = dynamic_cast<MInterfaceLine*>(*it);
    R.clear();
    _spaceMinus->getKeys(ie->getElem(0),R);
    dispm.resize(R.size());
    ufield->get(R,dispm);
    R.clear();
    _spacePlus->getKeys(ie->getElem(1),R);
    dispp.resize(R.size());
    ufield->get(R,dispp);
    int npts = integBound->getIntPoints(ie,&GP);
    this->computeIpv(aips,ie, GP,ws,dom,dom,_mlaw,_mlaw,dispm,dispp,false, stiff);
  }
  // Virtual interface element
  for(groupOfElements::elementContainer::const_iterator it=gib->begin(); it!=gib->end();++it){
    MInterfaceLine *ie = dynamic_cast<MInterfaceLine*>(*it);
    R.clear();
    _spaceMinus->getKeys(ie->getElem(0),R);
    dispm.resize(R.size());
    ufield->get(R,dispm);
    this->computeIpv(aips,ie, GP,ws,dom,dom,_mlaw,_mlaw,dispm,dispm,true, stiff);
  }

  // bulk
  for (groupOfElements::elementContainer::const_iterator it = g->begin(); it != g->end(); ++it){
    MElement *e = *it;
    R.clear();
    _space->getKeys(e,R);
    dispm.resize(R.size());
    ufield->get(R,dispm);
    this->computeIpv(aips,e,ws,_mlaw,dispm, stiff);
  }
}

void dgLinearShellDomain::computeIpv(AllIPState *aips,MInterfaceElement *ie, IntPt *GP,
                                  const IPStateBase::whichState ws,partDomain* efMinus, partDomain *efPlus,
                                  materialLaw *mlawmi, materialLaw *mlawp,
                                  fullVector<double> &dispm,
                                  fullVector<double> &dispp,const bool virt, bool stiff,const bool checkfrac){
  MInterfaceLine *ieline = dynamic_cast<MInterfaceLine*>(ie);
  nlsFunctionSpaceUVW<double> *_spaceminus = static_cast<nlsFunctionSpaceUVW<double>*>(efMinus->getFunctionSpace());
//  double uem,vem,uep,vep;
  shellMaterialLaw *mlawminus = static_cast<shellMaterialLaw*>(mlawmi);
  MElement *ele = dynamic_cast<MElement*>(ie);
  int npts=integBound->getIntPoints(ele,&GP);
  IntPt *GPm;
  IntPt *GPp;
  _interQuad->getIntPoints(ie,GP,&GPm,&GPp);
  // Get values of shape functions and derivative at Gauss points
  std::vector<GaussPointSpaceValues<double>*> vgpss;
  _spaceminus->get(ieline,npts,GP,vgpss);
  std::vector<GaussPointSpaceValues<double>*> vgpsm;
  _spaceminus->get(ie->getElem(0),npts,GPm,vgpsm);
  if(!virt){
    nlsFunctionSpaceUVW<double> *_spaceplus = static_cast<nlsFunctionSpaceUVW<double>*>(efPlus->getFunctionSpace());
    std::vector<GaussPointSpaceValues<double>*> vgpsp;
    _spaceplus->get(ie->getElem(1),npts,GPp,vgpsp);
    shellMaterialLaw *mlawplus = static_cast<shellMaterialLaw*>(mlawp);
    MElement *em = ie->getElem(0);
    MElement *ep = ie->getElem(1);
    // gauss point
    // vector with nodal displacement
    int nbdofm = _spaceminus->getNumKeys(em);
    int nbdofp = _spaceplus->getNumKeys(ep);
    int nbFFm = em->getNumVertices();
    int nbFFp = ep->getNumVertices();
    AllIPState::ipstateElementContainer *vips = aips->getIPstate(ie->getNum());
    int npts = vips->size()/2; // even number of pts in interface (same on both sides)
    for(int j=0;j<npts;j++){
      // key of gauss point grad value at gauss point
//      double wem, wep = 0.;
//      ieline->getuvwOnElem(GP[j].pt[0],0.,uem,vem,wem,uep,vep,wep);
//      Grads.clear(); Gradm.clear(); Gradp.clear(); Hessm.clear(); Hessp.clear();
//      _spaceminus->gradf(ieline,GP[j].pt[0],GP[j].pt[1],GP[j].pt[2],Grads);
//      _spaceminus->gradf(em,GPm[j].pt[0],GPm[j].pt[1],GPm[j].pt[2],Gradm);
//      _spaceminus->hessf(em,GPm[j].pt[0],GPm[j].pt[1],GPm[j].pt[2],Hessm);
//      _spaceplus->gradf(ep,GPp[j].pt[0],GPp[j].pt[1],GPp[j].pt[2],Gradp);
//      _spaceplus->hessf(ep,GPp[j].pt[0],GPp[j].pt[1],GPp[j].pt[2],Hessp);
      std::vector<TensorialTraits<double>::GradType> &Grads = vgpss[j]->_vgrads;
      std::vector<TensorialTraits<double>::GradType> &Gradm = vgpsm[j]->_vgrads;
      std::vector<TensorialTraits<double>::GradType> &Gradp = vgpsp[j]->_vgrads;
      std::vector<TensorialTraits<double>::HessType> &Hessm = vgpsm[j]->_vhess;
      std::vector<TensorialTraits<double>::HessType> &Hessp = vgpsp[j]->_vhess;
      // local basis on element is needed to compute the local basis on interfaceelement (normal)
      // modify only the shellLocalBasis of ipv
      IPStateBase* ipsm = (*vips)[j];
      IPStateBase* ipsp = (*vips)[j+npts];

      IPVariableShell* ipvm = static_cast<IPVariableShell*>(ipsm->getState(ws));
      IPVariableShell* ipvp = static_cast<IPVariableShell*>(ipsp->getState(ws));
      IPVariableShell* ipvmprev = static_cast<IPVariableShell*>(ipsm->getState(IPStateBase::previous));
      IPVariableShell* ipvpprev = static_cast<IPVariableShell*>(ipsp->getState(IPStateBase::previous));
      linearShellLocalBasisBulk *lbm = static_cast<linearShellLocalBasisBulk*>(ipvm->getshellLocalBasis());
      linearShellLocalBasisBulk *lbp = static_cast<linearShellLocalBasisBulk*>(ipvp->getshellLocalBasis());
      linearShellLocalBasisInter *lbsm = static_cast<linearShellLocalBasisInter*>(ipvm->getshellLocalBasisOfInterface());
      lbm->set(em,Gradm,Hessm);
      lbp->set(ep,Gradp,Hessp);
      lbsm->set(ieline,Grads,lbm->basisNormal(),lbp->basisNormal());
      ipvp->setshellLocalBasisOfInterface(lbsm);
      lbm->setBasisPushTensor(lbsm);
      lbp->setBasisPushTensor(lbsm);
      ipvm->setB(Gradm,Hessm);
      ipvm->strain(dispm);
      if(ws != IPStateBase::initial)
        mlawminus->stress(ipsm,stiff,checkfrac);
      ipvp->setB(Gradp,Hessp);
      ipvp->strain(dispp);
      if(ws != IPStateBase::initial)
        mlawplus->stress(ipsp, stiff, checkfrac);
      if(mlawminus->getType() == materialLaw::fracture){
        IPShellFractureCohesive *ipvfprevm = static_cast<IPShellFractureCohesive*>(ipvmprev);
        IPShellFractureCohesive *ipvfprevp = static_cast<IPShellFractureCohesive*>(ipvpprev);
        IPShellFractureCohesive  *ipvfm = static_cast<IPShellFractureCohesive*>(ipvm);
        IPShellFractureCohesive  *ipvfp = static_cast<IPShellFractureCohesive*>(ipvp);
        IPLinearCohesiveShell *ipvcm = ipvfm->getIPvFrac();
        IPLinearCohesiveShell *ipvcp = ipvfp->getIPvFrac();
        SVector3 ujump;
        double rjump[3];
        if((ipvfm->isbroken()) or (ipvfp->isbroken())){
          // compute the jump
          //std::vector<TensorialTraits<double>::ValType> Valm;
          //_spaceminus->f(ie->getElem(0),GPm[j].pt[0],GPm[j].pt[1],GPm[j].pt[2], Valm);
          //std::vector<TensorialTraits<double>::ValType> Valp;
          //_spaceplus->f(ie->getElem(1),GPp[j].pt[0], GPp[j].pt[1],GPp[j].pt[2], Valp);
          std::vector<TensorialTraits<double>::ValType> &Valm = vgpsm[j]->_vvals;
          std::vector<TensorialTraits<double>::ValType> &Valp = vgpsp[j]->_vvals;
          displacementjump(Valm,nbFFm,Valp,nbFFp,dispm,dispp,ujump);
          _nvarDomain.setDeltatTilde(lbm,Gradm,nbFFm,lbp,Gradp,nbFFp);
          _nvarDomain.jump(dispm,dispp,rjump);
        }
        // minus element
        if(ipvfprevm->isbroken()){ // if previous broken plus and minus are broken
          const IPLinearCohesiveShell *ipvcmprev = ipvfprevm->getIPvFrac();
          ipvcm->setFracture(ipvcmprev,ujump,rjump,lbsm,ipvm->getThickness());
          const IPLinearCohesiveShell *ipvcpprev = ipvfprevp->getIPvFrac();
          ipvcp->setFracture(ipvcpprev,ujump,rjump,lbsm,ipvp->getThickness());
        }
        // check for fracture initialization
        else if(ipvfm->isbroken()){ // previous not broken --> initialization of jump
          if(!ipvfp->isbroken()){ // At this time ipvfp has to be initialized
            const shellFractureByCohesiveLaw * mflaw = static_cast<const shellFractureByCohesiveLaw*>(mlawminus);
            const shellLinearCohesiveLaw *mclaw = static_cast<const shellLinearCohesiveLaw*>(mflaw->getFractureLaw());
            IPShellFractureCohesive *ipvprevp = static_cast<IPShellFractureCohesive*>(ipsp->getState(IPStateBase::previous));
            IPVariable *ipvcpprev = ipvprevp->getIPvFrac();
            linearShellLocalBasisInter *lbsp = static_cast<linearShellLocalBasisInter*>(ipvp->getshellLocalBasisOfInterface());
            IPVariable *ipvcp2 = static_cast<IPVariable*>(ipvcp);
            mclaw->createIPVariable(ipvcp2);
            mclaw->createIPVariable(ipvcpprev);
            ipvfp->setIPvFrac(ipvcp2);
            ipvfp->setInitializationTime(ipvfm->getInitializationTime());
            ipvprevp->setIPvFrac(ipvcpprev);
            ipvprevp->setInitializationTime(ipvfm->getInitializationTime());
            ipvcp = ipvfp->getIPvFrac();
            // same value than minus element
            mclaw->initDataFromLaw(ipvcm,ipvcp);
            // compute initial value
            std::vector<SVector3> vnalpha(2);
            std::vector<SVector3> vmalpha(2);
            mlawplus->reduction(ipsp,vnalpha,vmalpha,true);
            reductionElement& n0 = ipvcp->getn0();
            reductionElement& m0 = ipvcp->getm0();
            for(int alpha=0;alpha<2;alpha++){
              for(int beta=0;beta<2;beta++){
                n0(alpha,beta) = dot(vnalpha[alpha],lbsp->dualBasisVector(beta));
                m0(alpha,beta) = dot(vmalpha[alpha],lbsp->dualBasisVector(beta));
              }
            }
            ipvfp->broken(); // has to be after computation of reduction element otherwise nalpha and malpha =0.
          }
          // The two are broken --> mean values
          ipvcm->initmean(ipvcp);
          ipvcm->initfrac(ujump,rjump,lbsm,ipvm->getThickness());
          ipvcp->initfrac(ujump,rjump,lbsm,ipvp->getThickness());
        }
        else if(ipvfp->isbroken()){ // previous not broken -->initialization of jump
          // here we are sure that ipvfm is not broken otherwise previous else if
          const shellFractureByCohesiveLaw * mflaw = static_cast<const shellFractureByCohesiveLaw*>(mlawminus);
          const shellLinearCohesiveLaw *mclaw = static_cast<const shellLinearCohesiveLaw*>(mflaw->getFractureLaw());
          IPShellFractureCohesive *ipvprevm = static_cast<IPShellFractureCohesive*>(ipsm->getState(IPStateBase::previous));
          IPVariable* ipvcmprev = ipvprevm->getIPvFrac();
          IPVariable* ipvcm2 = dynamic_cast<IPVariable*>(ipvcm);
          mclaw->createIPVariable(ipvcm2);
          mclaw->createIPVariable(ipvcmprev);
          ipvfm->setIPvFrac(ipvcm2);
          ipvfm->setInitializationTime(ipvfp->getInitializationTime());
          ipvfprevm->setIPvFrac(ipvcmprev);
          ipvfprevm->setInitializationTime(ipvfp->getInitializationTime());
          ipvcm = ipvfm->getIPvFrac();
          // same value than plus element
          mclaw->initDataFromLaw(ipvcp,ipvcm);
          // compute reduction value
          std::vector<SVector3> vn(2);
          std::vector<SVector3> vm(2);
          reductionElement& n0 = ipvcm->getn0();
          reductionElement& m0 = ipvcm->getm0();
          mlawminus->reduction(ipsm,vn,vm,true);
          for(int alpha=0;alpha<2;alpha++){
            for(int beta=0;beta<2;beta++){
              n0(alpha,beta) = dot(vn[alpha],lbsm->dualBasisVector(beta));
              m0(alpha,beta) = dot(vm[alpha],lbsm->dualBasisVector(beta));
            }
          }
          ipvfm->broken(); // has to be after computation of reduction element otherwise nalpha=malpha=0
          // The two are broken --> mean values
          ipvcp->initmean(ipvcm);
          ipvcm->initfrac(ujump,rjump,lbsm,ipvm->getThickness());
          ipvcp->initfrac(ujump,rjump,lbsm,ipvp->getThickness());
        }
      }
    }
  }
  else{  // Virtual interface element
    // no fracture on virtual interface element --> use bulk law to compute stress
    shellMaterialLaw *mbulk;
    if(mlawminus->getType() == materialLaw::fracture)
      mbulk = (static_cast<shellFractureByCohesiveLaw*>(mlawminus))->getBulkLaw();
    else
      mbulk = mlawminus;
    MElement *e = ie->getElem(0);
    int edge = ie->getEdgeOrFaceNumber(0);
    // vector with nodal displacement
    int nbdof = _spaceminus->getNumKeys(e);
    int nbFF = e->getNumVertices();
    AllIPState::ipstateElementContainer *vips = aips->getIPstate(ie->getNum());
    for(int j=0;j<npts;j++){
      // key of gauss point grad value at gauss point
      std::vector<TensorialTraits<double>::GradType> &Grads = vgpss[j]->_vgrads;
      std::vector<TensorialTraits<double>::GradType> &Gradm = vgpsm[j]->_vgrads;
      std::vector<TensorialTraits<double>::HessType> &Hessm = vgpsm[j]->_vhess;
      // local basis on element is needed to compute the local basis on interfaceelement (normal)
      IPStateBase* ips = (*vips)[j];
      IPVariableShell *ipv = static_cast<IPVariableShell*>(ips->getState(ws));

      linearShellLocalBasisBulk* lbm = static_cast<linearShellLocalBasisBulk*>(ipv->getshellLocalBasis());
      linearShellLocalBasisInter* lbs = static_cast<linearShellLocalBasisInter*>(ipv->getshellLocalBasisOfInterface());
      lbm->set(e,Gradm,Hessm);
      lbs->set(ieline,Grads,lbm->basisNormal());
      lbm->setBasisPushTensor(lbs);
      ipv->setB(Gradm,Hessm);
      ipv->strain(dispm);
      if(ws != IPStateBase::initial)
        mbulk->stress(ips, stiff);
    }
  }
}

void dgLinearShellDomain::computeIpv(AllIPState *aips,MElement *e, IPStateBase::whichState ws,materialLaw *mlaw__,
                                              fullVector<double> &disp, bool stiff)
{
  shellMaterialLaw *mlaw = static_cast<shellMaterialLaw*>(mlaw__);
  nlsFunctionSpaceUVW<double> *sp = static_cast<nlsFunctionSpaceUVW<double>*>(_space);
  IntPt *GP;
  int npts_bulk=integBulk->getIntPoints(e,&GP);
  // shape functions values (with derivatives)
  std::vector<GaussPointSpaceValues<double>*> vgps;
  sp->get(e,npts_bulk,GP,vgps);
  AllIPState::ipstateElementContainer *vips = aips->getIPstate(e->getNum());
  for(int j=0;j<npts_bulk;j++){
    //grad value at gauss point
    std::vector<TensorialTraits<double>::GradType> &Grads = vgps[j]->_vgrads;
    std::vector<TensorialTraits<double>::HessType> &Hessm = vgps[j]->_vhess;
    IPStateBase* ips = (*vips)[j];
    IPVariableShell *ipv = static_cast<IPVariableShell*>(ips->getState(ws));
    IPVariableShell *ipvprev = static_cast<IPVariableShell*>(ips->getState(IPStateBase::previous));
    linearShellLocalBasisBulk* lb = static_cast<linearShellLocalBasisBulk*>(ipv->getshellLocalBasis());
    lb->set(e,Grads,Hessm);
    ipv->setB(Grads,Hessm);
    ipv->strain(disp);
    if(ws != IPStateBase::initial)
    mlaw->stress(ips, stiff);
  }
}

FilterDof* dgLinearShellDomain::createFilterDof(const int comp) const
{
  FilterDof* fdof = new DgC0ShellFilterDofComponent(comp);
  return fdof;

}

void dgLinearShellDomain::gaussIntegration(const GaussType gqt,const int orderbulk_, const int orderbound_){
  _gqt = gqt;
  _gaussorderbulk = orderbulk_;
  _gaussorderbound= orderbound_;
}

void dgLinearShellDomain::setGaussIntegrationRule(){
  groupOfElements::elementContainer::iterator it = g->begin();
  MElement *e = *it;
 // default case bulk
  if(_gaussorderbulk == -1){
    if(e->getPolynomialOrder()<3){
      _fullDg ? integBulk = new quadratureGaussTriQuadMix(2,4) : integBulk = new quadratureGaussTriQuadMix(2,3);
    }
    else{
      integBulk = new GaussQuadrature(5); // 25 points for third degree ?? GradGrad
    }
  }
  else{
    integBulk = new GaussQuadrature(_gaussorderbulk);
    //integBulk = new quadratureGaussTriQuadMix(_gaussorderbulk,_gaussorderbulk); // change this
  }
  // default case bound
  if(_gaussorderbound == -1){
    _gqt == Gauss ? integBound = new GaussQuadrature(5) : integBound = new GaussLobatto1DQuadrature(5);
    _gqt == Gauss ? _interQuad = new interface1DQuadrature(5) : _interQuad = new interface1DQuadrature(5,true);
  }
  else{
    //integBound = new GaussQuadrature(_gaussorderbound);
    _gqt == Gauss ? integBound = new GaussQuadrature(_gaussorderbound) : integBound = new GaussLobatto1DQuadrature(_gaussorderbound);
    _gqt == Gauss ? _interQuad = new interface1DQuadrature(_gaussorderbound) : _interQuad = new interface1DQuadrature(_gaussorderbound,true);
  }
}

void dgLinearShellDomain::createInterface(manageInterface &maninter)
{
  for (groupOfElements::elementContainer::const_iterator it = g->begin(); it != g->end(); ++it)
  {
    MElement *e=*it;
    int nedge = e->getNumEdges();
    for(int j=0;j<nedge;j++){
      std::vector<MVertex*> vv;
      e->getEdgeVertices(j,vv);
      IElement *ie = new IEdge(vv,e,this);
      maninter.insert(ie,this);
    }
  }
}

MElement* dgLinearShellDomain::createVirtualInterface(IElement *ie) const
{
  std::vector<MVertex*> Mv = ie->getVertices();
  MElement *ele = new MInterfaceLine(Mv,0,0,ie->getElement(),ie->getElement());
  return ele;
}
MElement* dgLinearShellDomain::createInterface(IElement *ie1, IElement *ie2) const
{

  MElement *nelem;
  if(ie1->getPhys() < ie2->getPhys())
  {
    std::vector<MVertex*> vv = ie1->getVertices();
    nelem = new MInterfaceLine(vv, 0, 0, ie1->getElement(), ie2->getElement());
  }
  else if(ie1->getPhys() > ie2->getPhys())
  {
    std::vector<MVertex*> vv = ie2->getVertices();
    nelem = new MInterfaceLine(vv, 0, 0, ie2->getElement(), ie1->getElement());
  }
 #if defined(HAVE_MPI)
  else if(Msg::GetCommSize() !=1)
  {
    int part1 = ie1->getElement()->getPartition();
    int part2 = ie2->getElement()->getPartition();
    if(part2 > part1){
      std::vector<MVertex*> vv = ie1->getVertices();
      nelem = new MInterfaceLine(vv, 0, Msg::GetCommRank()+1, ie1->getElement(), ie2->getElement());
    }
    else if(part1 > part2) {
      std::vector<MVertex*> vv = ie2->getVertices();
      nelem = new MInterfaceLine(vv, 0,Msg::GetCommRank()+1,ie2->getElement(),ie1->getElement());
    }
    else{
      std::vector<MVertex*> vv = ie1->getVertices();
      nelem = new MInterfaceLine(vv, 0, 0, ie1->getElement(), ie2->getElement());
    }
    if((part1 != Msg::GetCommRank() + 1) and (part1 != 0)) // add ghost interface for Dirichlet BC
    {
      ie1->getDomain()->_groupGhostMPI->insert(ie1->getElement());
    }
    if((part2 != Msg::GetCommRank() + 1) and (part2 != 0)) // add ghost interface for Dirichlet BC
    {
      ie2->getDomain()->_groupGhostMPI->insert(ie2->getElement());
    }
  }
 #endif // HAVE_MPI
  else
  {
    std::vector<MVertex*> vv = ie1->getVertices();
    nelem = new MInterfaceLine(vv, 0, 0, ie1->getElement(), ie2->getElement());
  }
  return nelem;
}

void dgLinearShellDomain::createInterfaceMPI(const int rankOtherPart, const groupOfElements &groupOtherPart,dgPartDomain* &dgdom)
{
  // domain creation has it function create a domain with a part of the same domain contained on an other part,
  // the type of domain is known
  if(dgdom==NULL){ // if undifined --> interface MPI of same domain
    dgdom = new interDomainBetweenShell(this->getTag(),this,this,0);
    // set the stability parameters
    dgLinearShellDomain* newInterDomShell = static_cast<dgLinearShellDomain*>(dgdom);
    newInterDomShell->stabilityParameters(this->_beta1,this->_beta2,this->_beta3);
  }
  // create interface (no need to use an interface builder ??)
  std::set<IEdge> map_edge;// the type Iedge is known
  // create interface with groupOtherPart (no match possible match has to be done with dom->gib)
  for(groupOfElements::elementContainer::const_iterator it = groupOtherPart.begin(); it != groupOtherPart.end(); ++it)
  {
	MElement *e=*it;
	int nedge = e->getNumEdges();
	for(int j=0;j<nedge;j++){
	  std::vector<MVertex*> vv;
	  e->getEdgeVertices(j,vv);
      map_edge.insert(IEdge(vv,e,this));
	}
  }
  // now use interface created on boundary to identify the interface element
  for(groupOfElements::elementContainer::iterator it=giv->begin(); it!=giv->end(); ++it) // loop on gib too (normally no need)
  {
    MInterfaceElement *interele = dynamic_cast<MInterfaceElement*>(*it);
    MElement *e = interele->getElem(0); // on boundary interface geetElem(0) == getElem(1)
	int nedge = e->getNumEdges();
	for(int j=0;j<nedge;j++){
	  std::vector<MVertex*> vv;
	  e->getEdgeVertices(j,vv);
	  IEdge ie = IEdge(vv,e,this);
	  const interfaceKeys& key = ie.getkey();
      std::set<IEdge>::iterator it_edge;
      bool findedge=false;
      for(it_edge = map_edge.begin(); it_edge != map_edge.end();++it_edge)
      {
        if(  it_edge->getkey() == key)
        {
          findedge = true;
          break;
        }
      }
      if(findedge){
        MElement *interel;
        // the partition with the lowest number is choosen by convention like the one corresponding to minus element
        if(dgdom->getPhysical() == _phys){
          if(Msg::GetCommRank() < rankOtherPart){
            interel =  new MInterfaceLine(vv, 0, Msg::GetCommRank()+1, e, it_edge->getElement());
          }
          else{
            // vv has to be oriented has the minus element --> get the
            std::vector<MVertex*> vvother = it_edge->getVertices();
            interel = new MInterfaceLine(vvother,0,Msg::GetCommRank()+1,it_edge->getElement(),e);
          }
          _groupGhostMPI->insert(it_edge->getElement());
        }
        else{ // interDomain defined between 2 partitions. The minus element has to be included in the minus domain
          groupOfElements::elementContainer::iterator itfound = dgdom->getMinusDomain()->g->end();
          for(groupOfElements::elementContainer::iterator ite=dgdom->getMinusDomain()->g->begin(); ite!=dgdom->getMinusDomain()->g->end();++ite)
          {
            if(e->getNum()==(*ite)->getNum())
            {
              itfound = ite;
              break;
            }
          }
          if(itfound!=dgdom->getMinusDomain()->g->end())
          {
            interel =  new MInterfaceLine(vv, 0, Msg::GetCommRank()+1, e, it_edge->getElement());
            // add element of other partition to prescribed Dirichlet BC later
            if(e->getPartition() != Msg::GetCommRank()+1)
               dgdom->getMinusDomain()->_groupGhostMPI->insert(e);
            else
               dgdom->getPlusDomain()->_groupGhostMPI->insert(it_edge->getElement());

          }
          else{ // e is included in the plus Domain
            // vv has to be oriented has the minus element --> get the
            std::vector<MVertex*> vvother = it_edge->getVertices();
            interel = new MInterfaceLine(vvother,0,Msg::GetCommRank()+1,it_edge->getElement(),e);
            // add element of other partition to prescribed Dirichlet BC later
            if(e->getPartition() != Msg::GetCommRank()+1)
               dgdom->getPlusDomain()->_groupGhostMPI->insert(e);
            else
               dgdom->getMinusDomain()->_groupGhostMPI->insert(it_edge->getElement());
          }
        }
        dgdom->gi->insert(interel);
        // add element of other partition to prescribed Dirichlet BC later
        map_edge.erase(it_edge);
      }
    }
  }
}

dgNonLinearShellDomain::dgNonLinearShellDomain(const int tag, const int phys, const int sp_,
                                                const int lnum,
                                                const int fdg) : dgLinearShellDomain(tag,phys,sp_,lnum,fdg),
                                                                  _evalStiff(false){}
dgNonLinearShellDomain::dgNonLinearShellDomain(const dgNonLinearShellDomain &source) : dgLinearShellDomain(source),
                                                                                       _evalStiff(source._evalStiff){}


void dgNonLinearShellDomain::computeIPVariable(AllIPState *aips,const unknownField *ufield, const IPStateBase::whichState ws, bool stiff)
{
  _evalStiff = false; // directly access to computeIPv is for matrix by perturbation otherwise compute ipvarible thanks to this function
  if(ws == IPStateBase::initial){
    this->initialIPVariable(aips,ufield,ws);
  }
  else{
    IntPt *GP;
    fullVector<double> dispm;
    fullVector<double> dispp;
    std::vector<Dof> R;
    // interface elements
    partDomain *dom = static_cast<partDomain*>(this);
    for(groupOfElements::elementContainer::const_iterator it=gi->begin(); it!=gi->end();++it){
      MInterfaceLine *ie = dynamic_cast<MInterfaceLine*>(*it);
      R.clear();
      _spaceMinus->getKeys(ie->getElem(0),R);
      dispm.resize(R.size());
      ufield->get(R,dispm);
      R.clear();
      _spacePlus->getKeys(ie->getElem(1),R);
      dispp.resize(R.size());
      ufield->get(R,dispp);
      int npts = integBound->getIntPoints(ie,&GP);
      this->computeIpv(aips,ie, GP,ws,dom,dom,_mlaw,_mlaw,dispm,dispp,false, stiff);
    }
    // Virtual interface element
    for(groupOfElements::elementContainer::const_iterator it=gib->begin(); it!=gib->end();++it){
      MInterfaceLine *ie = dynamic_cast<MInterfaceLine*>(*it);
      R.clear();
      _spaceMinus->getKeys(ie->getElem(0),R);
      dispm.resize(R.size());
      ufield->get(R,dispm);
      this->computeIpv(aips,ie, GP,ws,dom,dom,_mlaw,_mlaw,dispm,dispm,true, stiff);
    }

    // bulk
    for (groupOfElements::elementContainer::const_iterator it = g->begin(); it != g->end(); ++it){
      MElement *e = *it;
      R.clear();
      _space->getKeys(e,R);
      dispm.resize(R.size());
      ufield->get(R,dispm);
      this->computeIpv(aips,e,ws,_mlaw,dispm, stiff);
    }
  }
  _evalStiff = true;
}

void dgNonLinearShellDomain::computeIpv(AllIPState *aips,MElement *e, IPStateBase::whichState ws,
                                        materialLaw *mlaw__,fullVector<double> &disp, bool stiff)
{
  dgNonLinearShellMaterialLaw *mlaw;
  if(mlaw__->getType() == materialLaw::fracture)
    mlaw = static_cast<dgNonLinearShellMaterialLaw*>((static_cast<shellFractureByCohesiveLaw*>(mlaw__))->getBulkLaw());
  else{
    mlaw= static_cast<dgNonLinearShellMaterialLaw*>(mlaw__);
  }
  nlsFunctionSpaceUVW<double> *sp = static_cast<nlsFunctionSpaceUVW<double>*>(_space);
  IntPt *GP;
  int npts_bulk=integBulk->getIntPoints(e,&GP);
  AllIPState::ipstateElementContainer *vips = aips->getIPstate(e->getNum());
  // Get shape functions values (with derivatives)
  std::vector<GaussPointSpaceValues<double>*> vgps;
  sp->get(e,npts_bulk,GP,vgps);

  for(int j=0;j<npts_bulk;j++){
    //grad value at gauss point
    std::vector<TensorialTraits<double>::GradType> &Grads = vgps[j]->_vgrads;
    std::vector<TensorialTraits<double>::HessType> &Hessm = vgps[j]->_vhess;
    IPStateBase* ips = (*vips)[j];
    IPVariableShell *ipv = static_cast<IPVariableShell*>(ips->getState(ws));
    IPVariableShell *ipvprev = static_cast<IPVariableShell*>(ips->getState(IPStateBase::previous));
    nonLinearShellLocalBasisBulk* lb = static_cast<nonLinearShellLocalBasisBulk*>(ipv->getshellLocalBasis());
    lb->set(e,Grads,Hessm,disp,*(ipv->getThicknessStretch()),ipv->getSimpsonPoint());
    mlaw->ensurePlaneStress(ips,_evalStiff);
    mlaw->stress(ips, stiff);
  }
}
void dgNonLinearShellDomain::computeIpv(AllIPState *aips,MInterfaceElement *ie, IntPt *GP,const IPStateBase::whichState ws,
                                        partDomain* efMinus, partDomain *efPlus,materialLaw *mlawmi,
                                        materialLaw *mlawp,fullVector<double> &dispm,
                                        fullVector<double> &dispp,const bool virt, bool stiff,const bool checkfrac)
{
  MInterfaceLine *ieline = dynamic_cast<MInterfaceLine*>(ie);
  nlsFunctionSpaceUVW<double> *_spaceminus = static_cast<nlsFunctionSpaceUVW<double>*>(efMinus->getFunctionSpace());
  MElement *ele = dynamic_cast<MElement*>(ie);
  int npts=integBound->getIntPoints(ele,&GP);
  // Gauss points values on interface
  IntPt *GPm; IntPt *GPp;
  _interQuad->getIntPoints(ie,GP,&GPm,&GPp);
  // Get shape functions values (with derivatives)
  std::vector<GaussPointSpaceValues<double>*> vgpss;
  _spaceminus->get(ele,npts,GP,vgpss);
  std::vector<GaussPointSpaceValues<double>*> vgpsm;
  _spaceminus->get(ie->getElem(0),npts,GPm,vgpsm);
  shellMaterialLaw *mlawminus = static_cast<shellMaterialLaw*>(mlawmi);
  if(!virt){
    nlsFunctionSpaceUVW<double> *_spaceplus = static_cast<nlsFunctionSpaceUVW<double>*>(efPlus->getFunctionSpace());
    std::vector<GaussPointSpaceValues<double>*> vgpsp;
    _spaceplus->get(ie->getElem(1),npts,GPp,vgpsp);
    shellMaterialLaw *mlawplus = static_cast<shellMaterialLaw*>(mlawp);
    MElement *em = ie->getElem(0);
    MElement *ep = ie->getElem(1);
    // gauss point
    int nbdofm = _spaceminus->getNumKeys(em);
    int nbdofp = _spaceplus->getNumKeys(ep);
    int nbFFm = em->getNumVertices();
    int nbFFp = ep->getNumVertices();
    AllIPState::ipstateElementContainer *vips = aips->getIPstate(ie->getNum());
    // vector with nodal displacement of interface
    // take from minus displacement vector
    fullVector<double> dispinter;
    int nbdofinter = _space->getNumKeys(ieline);
    int nbvertexInter = ieline->getNumVertices();
    std::vector<int> vn(nbvertexInter);
    ieline->getLocalVertexNum(0,vn);
    dispinter.resize(nbdofinter);
    for(int i=0;i<nbvertexInter;i++){
      dispinter(i) = dispm(vn[i]);
      dispinter(i+nbvertexInter) = dispm(vn[i]+nbFFm);
      dispinter(i+2*nbvertexInter) = dispm(vn[i]+2*nbFFm);
    }
//    int npts = vips->size()/2; // even number of pts in interface (same on both sides)
    for(int j=0;j<npts;j++){
      // key of gauss point grad value at gauss point
      std::vector<TensorialTraits<double>::GradType> &Grads = vgpss[j]->_vgrads;
      std::vector<TensorialTraits<double>::GradType> &Gradm = vgpsm[j]->_vgrads;
      std::vector<TensorialTraits<double>::GradType> &Gradp = vgpsp[j]->_vgrads;
      std::vector<TensorialTraits<double>::HessType> &Hessm = vgpsm[j]->_vhess;
      std::vector<TensorialTraits<double>::HessType> &Hessp = vgpsp[j]->_vhess;
      // local basis on element is needed to compute the local basis on interfaceelement (normal)
      // modify only the shellLocalBasis of ipv
      IPStateBase* ipsm = (*vips)[j];
      IPStateBase* ipsp = (*vips)[j+npts];

      IPVariableShell* ipvm = static_cast<IPVariableShell*>(ipsm->getState(ws));
      IPVariableShell* ipvp = static_cast<IPVariableShell*>(ipsp->getState(ws));
      // Special function for non linear
      dgNonLinearShellMaterialLaw *nlmlawminus;
      if(mlawminus->getType() == materialLaw::fracture){
        nlmlawminus = static_cast<dgNonLinearShellMaterialLaw*>((static_cast<shellFractureByCohesiveLaw*>(mlawminus))->getBulkLaw());
      }
      else{
        nlmlawminus = static_cast<dgNonLinearShellMaterialLaw*>(mlawminus);
      }
      dgNonLinearShellMaterialLaw* nlmlawplus;
      if(mlawplus->getType() == materialLaw::fracture){
        nlmlawplus = static_cast<dgNonLinearShellMaterialLaw*>((static_cast<shellFractureByCohesiveLaw*>(mlawplus))->getBulkLaw());
      }
      else{
        nlmlawplus = static_cast<dgNonLinearShellMaterialLaw*>(mlawplus);
      }

      IPVariableShell* ipvmprev = static_cast<IPVariableShell*>(ipsm->getState(IPStateBase::previous));
      IPVariableShell* ipvpprev = static_cast<IPVariableShell*>(ipsp->getState(IPStateBase::previous));
      nonLinearShellLocalBasisBulk *lbm = static_cast<nonLinearShellLocalBasisBulk*>(ipvm->getshellLocalBasis());
      nonLinearShellLocalBasisBulk *lbp = static_cast<nonLinearShellLocalBasisBulk*>(ipvp->getshellLocalBasis());
      nonLinearShellLocalBasisInter *lbsm = static_cast<nonLinearShellLocalBasisInter*>(ipvm->getshellLocalBasisOfInterface());
      lbm->set(em,Gradm,Hessm,dispm,*(ipvm->getThicknessStretch()),ipvm->getSimpsonPoint());
      lbp->set(ep,Gradp,Hessp,dispp,*(ipvp->getThicknessStretch()),ipvp->getSimpsonPoint());
      if(ws != IPStateBase::initial) // CHANGE THIS (COMPUTE INITIAL IPV ON nonLinearInterDomain)
      {
        nlmlawminus->ensurePlaneStress(ipsm,_evalStiff);
        nlmlawplus->ensurePlaneStress(ipsp,_evalStiff);
      }
      else
      {
        lbm->setLambda(1.);
        lbp->setLambda(1.);
      }
      lbsm->set(ieline,Grads,dispinter,lbm,ipvm->getGlobalStretch(),lbp,ipvp->getGlobalStretch());
      ipvp->setshellLocalBasisOfInterface(lbsm);
      // Has to be called in lbsm->set
//      lbm->setBasisPushTensor(lbsm);
//      lbp->setBasisPushTensor(lbsm);
      if(ws != IPStateBase::initial) // CHANGE THIS (COMPUTE INITIAL IPV ON nonLinearInterDomain)
      {
        mlawminus->stress(ipsm,stiff, checkfrac);
        mlawplus->stress(ipsp, stiff, checkfrac);
      }
      if(mlawminus->getType() == materialLaw::fracture){
        IPShellFractureCohesive *ipvfprevm = static_cast<IPShellFractureCohesive*>(ipvmprev);
        IPShellFractureCohesive *ipvfprevp = static_cast<IPShellFractureCohesive*>(ipvpprev);
        IPShellFractureCohesive  *ipvfm = static_cast<IPShellFractureCohesive*>(ipvm);
        IPShellFractureCohesive  *ipvfp = static_cast<IPShellFractureCohesive*>(ipvp);
        IPLinearCohesiveShell *ipvcm = ipvfm->getIPvFrac();
        IPLinearCohesiveShell *ipvcp = ipvfp->getIPvFrac();
        IPVariableShell* ipvm0 = static_cast<IPVariableShell*>(ipsm->getState(IPStateBase::initial));
        IPVariableShell* ipvp0 = static_cast<IPVariableShell*>(ipsp->getState(IPStateBase::initial));
        nonLinearShellLocalBasisBulk *lbm0 = static_cast<nonLinearShellLocalBasisBulk*>(ipvm0->getshellLocalBasis());
        nonLinearShellLocalBasisBulk *lbp0 = static_cast<nonLinearShellLocalBasisBulk*>(ipvp0->getshellLocalBasis());
        SVector3 ujump;
        double rjump[3];
//        double rjumptest[3];
        if((ipvfm->isbroken()) or (ipvfp->isbroken())){
          // compute the jump
          std::vector<TensorialTraits<double>::ValType> &Valm = vgpsm[j]->_vvals;
          std::vector<TensorialTraits<double>::ValType> &Valp = vgpsp[j]->_vvals;
          displacementjump(Valm,nbFFm,Valp,nbFFp,dispm,dispp,ujump);
          for(int i=0;i<3;i++)
            rjump[i] = (lbp->basisNormal(i)-lbp0->basisNormal(i))-(lbm->basisNormal(i)-lbm0->basisNormal(i));
          // OLD WAY works better ??
 //         _nvarDomain.setDeltatTilde(lbm,Gradm,nbFFm,lbp,Gradp,nbFFp);
//          _nvarDomain.jump(dispm,dispp,rjump);
//          if(checkfrac)
//            Msg::Info("rjump %e %e %e, test %e %e %e",rjump[0],rjump[1],rjump[2],rjumptest[0],rjumptest[1],rjumptest[2]);
        }
        // minus element
        if( (ipvfprevm->isbroken()) or ( (!checkfrac) and ( (ipvfm->isbroken()) or (ipvfp->isbroken()) ) ) ){ // if previous broken plus and minus are broken. Second condition is for perturbating for the stiffness matrix computation
          const IPLinearCohesiveShell *ipvcmprev = ipvfprevm->getIPvFrac();
          ipvcm->setFracture(ipvcmprev,ujump,rjump,lbsm,ipvm->getThickness());
//          if(checkfrac)
//            Msg::Info("Opening deltamax %e deltan %e deltat %e delta %e",ipvcm->getDeltamax(),ipvcm->getDeltan(),ipvcm->getDeltat(),ipvcm->getDelta());
          const IPLinearCohesiveShell *ipvcpprev = ipvfprevp->getIPvFrac();
          ipvcp->setFracture(ipvcpprev,ujump,rjump,lbsm,ipvp->getThickness());
        }
        // check for fracture initialization
        else if(ipvfm->isbroken()){ // previous not broken --> initialization of jump
          if(!ipvfp->isbroken()){ // At this time ipvfp has to be initialized
            const shellFractureByCohesiveLaw * mflaw = static_cast<const shellFractureByCohesiveLaw*>(mlawminus);
            const shellLinearCohesiveLaw *mclaw = static_cast<const shellLinearCohesiveLaw*>(mflaw->getFractureLaw());
            IPShellFractureCohesive *ipvprevp = static_cast<IPShellFractureCohesive*>(ipsp->getState(IPStateBase::previous));
            nonLinearShellLocalBasisInter *lbsp = static_cast<nonLinearShellLocalBasisInter*>(ipvp->getshellLocalBasisOfInterface());
            IPVariable *ipvcpprev = ipvprevp->getIPvFrac();
            IPVariable *ipvcp2 = static_cast<IPVariable*>(ipvcp);
            mclaw->createIPVariable(ipvcp2);
            mclaw->createIPVariable(ipvcpprev);
            ipvfp->setIPvFrac(ipvcp2);
            ipvfp->setInitializationTime(ipvfm->getInitializationTime());
            ipvprevp->setIPvFrac(ipvcpprev);
            ipvprevp->setInitializationTime(ipvfm->getInitializationTime());
            ipvcp = ipvfp->getIPvFrac();
            // same value than minus element
            mclaw->initDataFromLaw(ipvcm,ipvcp);
            // compute initial value
            std::vector<SVector3> vnalpha(2);
            std::vector<SVector3> vmalpha(2);
            mlawplus->reduction(ipsp,vnalpha,vmalpha,true);
            reductionElement& n0 = ipvcp->getn0();
            reductionElement& m0 = ipvcp->getm0();
            for(int alpha=0;alpha<2;alpha++){
              for(int beta=0;beta<2;beta++){
                n0(alpha,beta) = dot(vnalpha[alpha],lbsp->dualBasisVector(beta));
                m0(alpha,beta) = dot(vmalpha[alpha],lbsp->dualBasisVector(beta));
              }
            }
            ipvfp->broken(); // has to be after computation of reduction element otherwise nalpha = malpha = 0
          }
          // The two are broken --> mean values
          ipvcm->initmean(ipvcp);
          ipvcm->initfrac(ujump,rjump,lbsm,ipvm->getThickness());
          ipvcp->initfrac(ujump,rjump,lbsm,ipvp->getThickness());
        }
        else if(ipvfp->isbroken()){ // previous not broken -->initialization of jump
          // here we are sure that ipvfm is not broken otherwise previous else if
          const shellFractureByCohesiveLaw * mflaw = static_cast<const shellFractureByCohesiveLaw*>(mlawminus);
          const shellLinearCohesiveLaw *mclaw = static_cast<const shellLinearCohesiveLaw*>(mflaw->getFractureLaw());
          IPShellFractureCohesive *ipvprevm = static_cast<IPShellFractureCohesive*>(ipsm->getState(IPStateBase::previous));
          IPVariable* ipvcmprev = ipvprevm->getIPvFrac();
          IPVariable* ipvcm2 = static_cast<IPVariable*>(ipvcm);
          mclaw->createIPVariable(ipvcm2);
          mclaw->createIPVariable(ipvcmprev);
          ipvfm->setIPvFrac(ipvcm2);
          ipvfm->setInitializationTime(ipvfp->getInitializationTime());
          ipvfprevm->setIPvFrac(ipvcmprev);
          ipvfprevm->setInitializationTime(ipvfp->getInitializationTime());
          ipvcm = ipvfm->getIPvFrac();
          // same value than plus element
          mclaw->initDataFromLaw(ipvcp,ipvcm);
          // compute reduction value
          std::vector<SVector3> vn(2);
          std::vector<SVector3> vm(2);
          reductionElement& n0 = ipvcm->getn0();
          reductionElement& m0 = ipvcm->getm0();
          mlawminus->reduction(ipsm,vn,vm,true);
          for(int alpha=0;alpha<2;alpha++){
            for(int beta=0;beta<2;beta++){
              n0(alpha,beta) = dot(vn[alpha],lbsm->dualBasisVector(beta));
              m0(alpha,beta) = dot(vm[alpha],lbsm->dualBasisVector(beta));
            }
          }
          ipvfm->broken();  // has to be after computation of reduction element otherwise nalpha = malpha = 0
          // The two are broken --> mean values
          ipvcp->initmean(ipvcm);
          ipvcm->initfrac(ujump,rjump,lbsm,ipvm->getThickness());
          ipvcp->initfrac(ujump,rjump,lbsm,ipvp->getThickness());
        }
      }
    }
  }
  else{  // Virtual interface element
    // no fracture on virtual interface element --> use bulk law to compute stress
    dgNonLinearShellMaterialLaw *mbulk;
    if(mlawminus->getType() == materialLaw::fracture)
      mbulk = static_cast<dgNonLinearShellMaterialLaw*>((static_cast<shellFractureByCohesiveLaw*>(mlawminus))->getBulkLaw());
    else
      mbulk = static_cast<dgNonLinearShellMaterialLaw*>(mlawminus);
    MElement *e = ie->getElem(0);
    int edge = ie->getEdgeOrFaceNumber(0);
    //int npts_inter= dgdom->getInterfaceGaussIntegrationRule()->getIntPoints(ie,&GP);
    // vector with nodal displacement
    int nbdof = _spaceminus->getNumKeys(e);
    int nbFF = e->getNumVertices();
    // vector with nodal displacement of interface
    // take from minus displacement vector
    fullVector<double> dispinter;
    int nbdofinter = _space->getNumKeys(ieline);
    int nbvertexInter = ieline->getNumVertices();
    std::vector<int> vn(nbvertexInter);
    ieline->getLocalVertexNum(0,vn);
    dispinter.resize(nbdofinter);
    for(int i=0;i<nbvertexInter;i++){
      dispinter(i) = dispm(vn[i]);
      dispinter(i+nbvertexInter) = dispm(vn[i]+nbFF);
      dispinter(i+2*nbvertexInter) = dispm(vn[i]+2*nbFF);
    }
    AllIPState::ipstateElementContainer *vips = aips->getIPstate(ie->getNum());
    for(int j=0;j<npts;j++){
      // key of gauss point grad value at gauss point
      std::vector<TensorialTraits<double>::GradType> &Grads = vgpss[j]->_vgrads;
      std::vector<TensorialTraits<double>::GradType> &Gradm = vgpsm[j]->_vgrads;
      std::vector<TensorialTraits<double>::HessType> &Hessm = vgpsm[j]->_vhess;
      // local basis on element is needed to compute the local basis on interfaceelement (normal)
      IPStateBase* ips = (*vips)[j];
      IPVariableShell *ipv = static_cast<IPVariableShell*>(ips->getState(ws));

      nonLinearShellLocalBasisBulk* lbm = static_cast<nonLinearShellLocalBasisBulk*>(ipv->getshellLocalBasis());
      nonLinearShellLocalBasisInter* lbs = static_cast<nonLinearShellLocalBasisInter*>(ipv->getshellLocalBasisOfInterface());
      lbm->set(e,Gradm,Hessm,dispm,*(ipv->getThicknessStretch()),ipv->getSimpsonPoint());
      mbulk->ensurePlaneStress(ips,_evalStiff); // basis before compute of interface basis
      lbs->set(ieline,Grads,dispinter,lbm,ipv->getGlobalStretch());
      // has to be called in lbs->set
//      lbm->setBasisPushTensor(lbs);
//      ipv->setB(Gradm,Hessm);
//      ipv->strain(dispm);
      mbulk->stress(ips,stiff, false); // no fracture at extremities for now
    }
  }
}

void dgNonLinearShellDomain::initialIPVariable(AllIPState *aips,const unknownField *ufield,
                                               const IPStateBase::whichState ws)
{
  fullVector<double> unknowns_m;
  fullVector<double> unknowns_p;
  fullVector<double> unknowns_inter;
  std::vector<Dof> R;
  nlsFunctionSpaceUVW<double> *spminus = static_cast<nlsFunctionSpaceUVW<double>*>(_spaceMinus);
  nlsFunctionSpaceUVW<double> *spplus = static_cast<nlsFunctionSpaceUVW<double>*>(_spacePlus);
  IntPt *GP;

  // loop on interface
  for(groupOfElements::elementContainer::iterator it=gi->begin(); it!=gi->end(); ++it){
    MInterfaceLine *ieline = dynamic_cast<MInterfaceLine*>(*it);
    MElement *em = ieline->getElem(0);
    MElement *ep = ieline->getElem(1);
    // get displacement
    R.clear();
    _spaceMinus->getKeys(em,R);
    unknowns_m.resize(R.size());
    ufield->get(R,unknowns_m);
    R.clear();
    _spacePlus->getKeys(ep,R);
    unknowns_p.resize(R.size());
    ufield->get(R,unknowns_p);
    int nbdofinter = _space->getNumKeys(ieline);
    int nbvertexInter = ieline->getNumVertices();
    int nbFFm = ieline->getElem(0)->getNumVertices();
    std::vector<int> vn(nbvertexInter);
    ieline->getLocalVertexNum(0,vn);
    unknowns_inter.resize(nbdofinter);
    for(int i=0;i<nbvertexInter;i++){
      unknowns_inter(i) = unknowns_m(vn[i]);
      unknowns_inter(i+nbvertexInter) = unknowns_m(vn[i]+nbFFm);
      unknowns_inter(i+2*nbvertexInter) = unknowns_m(vn[i]+2*nbFFm);
    }

    AllIPState::ipstateElementContainer *vips = aips->getIPstate(ieline->getNum());
    int npts = integBound->getIntPoints(ieline,&GP);
    // Get Gauss points values on minus and plus elements
    IntPt *GPm; IntPt *GPp;
    _interQuad->getIntPoints(ieline,GP,&GPm,&GPp);
      // Get shape functions values (with derivatives)
    std::vector<GaussPointSpaceValues<double>*> vgpss;
    spminus->get(ieline,npts,GP,vgpss);
    std::vector<GaussPointSpaceValues<double>*> vgpsm;
    spminus->get(ieline->getElem(0),npts,GPm,vgpsm);
    std::vector<GaussPointSpaceValues<double>*> vgpsp;
    spplus->get(ieline->getElem(1),npts,GPp,vgpsp);
    for(int j=0;j<npts;j++){
      // key of gauss point grad value at gauss point
      std::vector<TensorialTraits<double>::GradType> &Grads = vgpss[j]->_vgrads;
      std::vector<TensorialTraits<double>::GradType> &Gradm = vgpsm[j]->_vgrads;
      std::vector<TensorialTraits<double>::GradType> &Gradp = vgpsp[j]->_vgrads;
      std::vector<TensorialTraits<double>::HessType> &Hessm = vgpsm[j]->_vhess;
      std::vector<TensorialTraits<double>::HessType> &Hessp = vgpsp[j]->_vhess;
      // local basis on element is needed to compute the local basis on interfaceelement (normal)
      IPStateBase* ipsm = (*vips)[j];
      IPStateBase* ipsp = (*vips)[j+npts];

      IPVariableShell* ipvm = static_cast<IPVariableShell*>(ipsm->getState(IPStateBase::initial));
      IPVariableShell* ipvp = static_cast<IPVariableShell*>(ipsp->getState(IPStateBase::initial));
      // set initial Jacobian value in other ipv (change this HOW ??)
      IPVariableShell* ipvmprev = static_cast<IPVariableShell*>(ipsm->getState(IPStateBase::previous));
      IPVariableShell* ipvmcur = static_cast<IPVariableShell*>(ipsm->getState(IPStateBase::current));
      IPVariableShell* ipvpprev = static_cast<IPVariableShell*>(ipsp->getState(IPStateBase::previous));
      IPVariableShell* ipvpcur = static_cast<IPVariableShell*>(ipsp->getState(IPStateBase::current));

      nonLinearShellLocalBasisBulk *lbm = static_cast<nonLinearShellLocalBasisBulk*>(ipvm->getshellLocalBasis());
      nonLinearShellLocalBasisBulk *lbmprev = static_cast<nonLinearShellLocalBasisBulk*>(ipvmprev->getshellLocalBasis());
      nonLinearShellLocalBasisBulk *lbmcur = static_cast<nonLinearShellLocalBasisBulk*>(ipvmcur->getshellLocalBasis());
      nonLinearShellLocalBasisBulk *lbp = static_cast<nonLinearShellLocalBasisBulk*>(ipvp->getshellLocalBasis());
      nonLinearShellLocalBasisBulk *lbpprev =  static_cast<nonLinearShellLocalBasisBulk*>(ipvpprev->getshellLocalBasis());
      nonLinearShellLocalBasisBulk *lbpcur = static_cast<nonLinearShellLocalBasisBulk*>(ipvpcur->getshellLocalBasis());
      nonLinearShellLocalBasisInter *lbsm = static_cast<nonLinearShellLocalBasisInter*>(ipvm->getshellLocalBasisOfInterface());
      lbm->set(em,Gradm,Hessm,unknowns_m,*(ipvm->getThicknessStretch()),ipvm->getSimpsonPoint());
      lbp->set(ep,Gradp,Hessp,unknowns_p,*(ipvp->getThicknessStretch()),ipvp->getSimpsonPoint());
      lbsm->set(ieline,Grads,unknowns_inter,lbm,ipvm->getGlobalStretch(),lbp,ipvp->getGlobalStretch());
      ipvp->setshellLocalBasisOfInterface(lbsm);
      // has to be called in lbsm->set
//      lbm->setBasisPushTensor(lbsm);
//      lbp->setBasisPushTensor(lbsm);
      lbm->setLambda(1.);
      lbp->setLambda(1.);
      lbm->setInitialJacobian(lbm->getJacobianOfSimpsonPoints()); // change this HOW (beaware of copy ipv which copy initial->previous and remove the stored value)??
      lbp->setInitialJacobian(lbp->getJacobianOfSimpsonPoints()); // change this HOW (beaware of copy ipv which copy initial->previous and remove the stored value)??
      lbmcur->setInitialJacobian(lbm->getJacobianOfSimpsonPoints());
      lbmprev->setInitialJacobian(lbm->getJacobianOfSimpsonPoints());
      lbpcur->setInitialJacobian(lbp->getJacobianOfSimpsonPoints());
      lbpprev->setInitialJacobian(lbp->getJacobianOfSimpsonPoints());
      // no need to compute stress initial --> compute in previous ipstate
      // and no fracture check
      ipvm->strain(unknowns_m,-1,lbm);
      ipvp->strain(unknowns_p,-1,lbp);
    }

  }
  // loop on virtual interface
  for(groupOfElements::elementContainer::iterator it=gib->begin(); it!=gib->end(); ++it){
    MInterfaceLine *ie = dynamic_cast<MInterfaceLine*>(*it);
    MElement *e = ie->getElem(0);
//    int edge = ie->getEdgeOrFaceNumber(0);
    // vector with nodal displacement
    R.clear();
    spminus->getKeys(e,R);
    unknowns_m.resize(R.size());
    ufield->get(R,unknowns_m);
    int nbdofinter = _space->getNumKeys(ie);
    int nbvertexInter = ie->getNumVertices();
    int nbFFm = ie->getElem(0)->getNumVertices();
    std::vector<int> vn(nbvertexInter);
    ie->getLocalVertexNum(0,vn);
    unknowns_inter.resize(nbdofinter);
    for(int i=0;i<nbvertexInter;i++){
      unknowns_inter(i) = unknowns_m(vn[i]);
      unknowns_inter(i+nbvertexInter) = unknowns_m(vn[i]+nbFFm);
      unknowns_inter(i+2*nbvertexInter) = unknowns_m(vn[i]+2*nbFFm);
    }

    AllIPState::ipstateElementContainer *vips = aips->getIPstate(ie->getNum());
    int npts=integBound->getIntPoints(ie,&GP);
    // Get Gauss points values on minus element
    IntPt *GPm; IntPt *GPp;
    _interQuad->getIntPoints(ie,GP,&GPm,&GPp);
    // Get shape functions values (with derivatives)
    std::vector<GaussPointSpaceValues<double>*> vgpss;
    spminus->get(ie,npts,GP,vgpss);
    std::vector<GaussPointSpaceValues<double>*> vgpsm;
    spminus->get(e,npts,GPm,vgpsm);
    for(int j=0;j<npts;j++){
      // key of gauss point grad value at gauss point
      std::vector<TensorialTraits<double>::GradType> &Grads = vgpss[j]->_vgrads;
      std::vector<TensorialTraits<double>::GradType> &Gradm = vgpsm[j]->_vgrads;
      std::vector<TensorialTraits<double>::HessType> &Hessm = vgpsm[j]->_vhess;
      // local basis on element is needed to compute the local basis on interfaceelement (normal)
      IPStateBase* ips = (*vips)[j];
      IPVariableShell *ipv = static_cast<IPVariableShell*>(ips->getState(ws));
      // set initial jacobian in other ipv (needed for stress change this HOW ??)
      IPVariableShell *ipvprev = static_cast<IPVariableShell*>(ips->getState(IPStateBase::previous));
      IPVariableShell *ipvcur = static_cast<IPVariableShell*>(ips->getState(IPStateBase::current));
      nonLinearShellLocalBasisBulk* lbm = static_cast<nonLinearShellLocalBasisBulk*>(ipv->getshellLocalBasis());
      nonLinearShellLocalBasisBulk* lbmprev = static_cast<nonLinearShellLocalBasisBulk*>(ipvprev->getshellLocalBasis());
      nonLinearShellLocalBasisBulk* lbmcur = static_cast<nonLinearShellLocalBasisBulk*>(ipvcur->getshellLocalBasis());
      nonLinearShellLocalBasisInter* lbs = static_cast<nonLinearShellLocalBasisInter*>(ipv->getshellLocalBasisOfInterface());
      lbm->set(e,Gradm,Hessm,unknowns_m,*(ipv->getThicknessStretch()),ipv->getSimpsonPoint());
      lbs->set(ie,Grads,unknowns_inter,lbm,ipv->getGlobalStretch());
      lbm->setLambda(1.);
      lbm->setInitialJacobian(lbm->getJacobianOfSimpsonPoints()); // change this HOW (beaware of copy ipv which copy initial->previous and remove the stored value)??
      lbmcur->setInitialJacobian(lbm->getJacobianOfSimpsonPoints());
      lbmprev->setInitialJacobian(lbm->getJacobianOfSimpsonPoints());

      // Only local basis for initial state if initial stress compute in previous?
      ipv->strain(unknowns_m,-1,lbm);
    }
  }
  // loop on element
  fullVector<double> disp;
  for(groupOfElements::elementContainer::iterator it=g->begin(); it!=g->end(); ++it)
  {
    MElement *ele = *it;
    // get displacement of element to known the current element configuration
    R.clear();
    nlsFunctionSpaceUVW<double> *sp = static_cast<nlsFunctionSpaceUVW<double>*>(_space);
    sp->getKeys(ele,R);
    disp.resize(R.size());
    ufield->get(R,disp);
    // loop on Gauss's point
    int npts = integBulk->getIntPoints(ele,&GP);
    AllIPState::ipstateElementContainer *vips = aips->getIPstate(ele->getNum());
    // Get shape functions values (with derivatives)
    std::vector<GaussPointSpaceValues<double>*> vgps;
    sp->get(ele,npts,GP,vgps);
    for(int j=0;j<npts;j++){
      std::vector<TensorialTraits<double>::GradType> &grads = vgps[j]->_vgrads;
      std::vector<TensorialTraits<double>::HessType> &hesss = vgps[j]->_vhess;

      IPVariableShell *ipv = static_cast<IPVariableShell*>((*vips)[j]->getState(IPStateBase::initial));
      // set initial jacobian in other ipv (Change this HOW ??)
      IPVariableShell *ipvcur = static_cast<IPVariableShell*>((*vips)[j]->getState(IPStateBase::current));
      IPVariableShell *ipvprev = static_cast<IPVariableShell*>((*vips)[j]->getState(IPStateBase::previous));

      nonLinearShellLocalBasisBulk* lb = static_cast<nonLinearShellLocalBasisBulk*>(ipv->getshellLocalBasis());
      nonLinearShellLocalBasisBulk* lbcur = static_cast<nonLinearShellLocalBasisBulk*>(ipvcur->getshellLocalBasis());
      nonLinearShellLocalBasisBulk* lbprev = static_cast<nonLinearShellLocalBasisBulk*>(ipvprev->getshellLocalBasis());
      lb->set(ele,grads,hesss,disp,*(ipv->getThicknessStretch()),ipv->getSimpsonPoint());
      lb->setLambda(1.);
      lb->setInitialJacobian(lb->getJacobianOfSimpsonPoints()); // change this HOW (beaware of copy ipv which copy initial->previous and remove the stored value)??
      lbcur->setInitialJacobian(lb->getJacobianOfSimpsonPoints());
      lbprev->setInitialJacobian(lb->getJacobianOfSimpsonPoints());
      // no need to compute stress on thickness because initial stage (the value of Lambdah is known)
      // no need of stress (stress has to be compute in previous step)
      ipv->strain(disp,-1,lb);
    }
  }
}
void dgNonLinearShellDomain::initializeTerms(unknownField *uf,IPField*ip,std::vector<nonLinearNeumannBC> &vNeumann)
{
  FunctionSpace<double>* dgspace = static_cast<FunctionSpace<double>*>(_space);
  ltermBulk = new dgNonLinearShellForceBulk(*dgspace,_mlaw,_fullDg,ip);
  ltermBound = new dgNonLinearShellForceInter(*dgspace,dgspace,_mlaw,_mlaw,_interQuad,_beta1,_beta2,_beta3,_fullDg,ip);
  ltermVirtBound = new dgNonLinearShellForceVirtInter(*dgspace,_mlaw,_interQuad,_beta1,_fullDg,ip);
  if(_bmbp)
    btermBulk = new BilinearTermPerturbation<double>(ltermBulk,*dgspace,*dgspace,uf,ip,this,_eps);
  else
   btermBulk = new dgNonLinearShellStiffnessBulk(*dgspace,_mlaw,_fullDg,ip);
  if(_interByPert)
    btermBound = new BilinearTermPerturbation<double>(ltermBound,*dgspace,*dgspace,uf,ip,this,_eps);
  else
    btermBound = new dgNonLinearShellStiffnessInter(*dgspace,*dgspace,_mlaw,_mlaw,_interQuad,_beta1,_beta2,_beta3,ip,uf,_fullDg);
  if(_virtByPert)
    btermVirtBound = new BilinearTermPerturbation<double>(ltermVirtBound,*dgspace,*dgspace,uf,ip,this,_eps);
  else
    btermVirtBound = new dgNonLinearShellStiffnessVirtInter(*dgspace,_mlaw,_interQuad,_beta1,uf,ip,_fullDg);
  massterm = new massC0DgShell(*dgspace,_mlaw);

  // term of neumannBC
  for(int i=0;i<vNeumann.size();i++){
    nonLinearNeumannBC &neu = vNeumann[i];
    for(int j=0;j<neu._vspace.size();j++){
      if(neu._vdom[j] == this)
      {
      if(neu._vterm[j] != NULL) delete neu._vterm[j];
      FunctionSpace<double> *spneu = static_cast<FunctionSpace<double>*>(neu._vspace[j]);
      if(neu.onWhat != nonLinearBoundaryCondition::PRESSURE)
        neu._vterm[j] = new DgC0LoadTerm<double>(*spneu,neu._f);
      else
        neu._vterm[j] = new DgC0PressureLoadTerm(*spneu,neu._f,ip);
      }
    }
  }

   /* same value for fracture strength on both side */
   // not very elegant put very efficient to put here
   if(_mlaw->getType() == materialLaw::fracture)
   {
     this->ensureSameValuesBothSideIPvariable(ip);
   }
}

void dgNonLinearShellDomain::setGaussIntegrationRule(){
  groupOfElements::elementContainer::iterator it = g->begin();
  MElement *e = *it;
 // default case bulk
  if(_gaussorderbulk == -1){
    if(e->getPolynomialOrder()<3){
      _fullDg ? integBulk = new quadratureGaussTriQuadMix(2,4) : integBulk = new quadratureGaussTriQuadMix(2,3);
    }
    else{
      integBulk = new quadratureGaussTriQuadMix(4,5);//GaussQuadrature(5); // 25 points for third degree ?? GradGrad
    }
  }
  else{
    integBulk = new GaussQuadrature(_gaussorderbulk);
  }
  // default case bound
  if(_gaussorderbound == -1){
    if(e->getPolynomialOrder()<3){
      _gqt == Gauss ? integBound = new GaussQuadrature(5)  : integBound = new GaussLobatto1DQuadrature(7);
      _gqt == Gauss ? _interQuad = new interface1DQuadrature(5) : _interQuad = new interface1DQuadrature(7,true);
    }
    else{
      _gqt == Gauss ? integBound = new GaussQuadrature(7) : integBound = new GaussLobatto1DQuadrature(9);
      _gqt == Gauss ? _interQuad = new interface1DQuadrature(7) : _interQuad = new interface1DQuadrature(9,true);
    }
  }
  else{
    _gqt == Gauss ? integBound = new GaussQuadrature(_gaussorderbound) : integBound = new GaussLobatto1DQuadrature(_gaussorderbound);
    _gqt == Gauss ? _interQuad = new interface1DQuadrature(_gaussorderbound) : _interQuad = new interface1DQuadrature(_gaussorderbound,true);
  }
}


// copy paste of linear change this !!
void dgNonLinearShellDomain::createInterfaceMPI(const int rankOtherPart, const groupOfElements &groupOtherPart,dgPartDomain* &dgdom)
{
   // domain creation has it function create a domain with a part of the same domain contained on an other part,
  // the type of domain is known
  if(dgdom==NULL){
    dgdom = new nonLinearInterDomainShell(this->getTag(),this,this,0);
    // set the stability parameters
    dgNonLinearShellDomain* newInterDomShell = static_cast<dgNonLinearShellDomain*>(dgdom);
    newInterDomShell->stabilityParameters(_beta1,_beta2,_beta3);
  }
  // create interface (no need to use an interface builder ??)
  std::set<IEdge> map_edge;// the type Iedge is known
  // create interface with groupOtherPart (no match possible match has to be done with dom->gib)
  for(groupOfElements::elementContainer::const_iterator it = groupOtherPart.begin(); it != groupOtherPart.end(); ++it)
  {
	MElement *e=*it;
	int nedge = e->getNumEdges();
	for(int j=0;j<nedge;j++){
	  std::vector<MVertex*> vv;
	  e->getEdgeVertices(j,vv);
      map_edge.insert(IEdge(vv,e,this));
	}
  }
  // now use interface crated on boundary to identify the interface element
  for(groupOfElements::elementContainer::iterator it=giv->begin(); it!=giv->end(); ++it) // loop on gib too (normally no need)
  {
    MInterfaceElement *interele = dynamic_cast<MInterfaceElement*>(*it);
    MElement *e = interele->getElem(0); // on boundary interface geetElem(0) == getElem(1)
	int nedge = e->getNumEdges();
	for(int j=0;j<nedge;j++){
	  std::vector<MVertex*> vv;
	  e->getEdgeVertices(j,vv);
	  IEdge ie = IEdge(vv,e,this);
	  const interfaceKeys& key = ie.getkey();
      std::set<IEdge>::iterator it_edge;
      bool findedge = false;
      for(it_edge = map_edge.begin(); it_edge != map_edge.end(); ++it_edge)
      {
        if( it_edge->getkey() == key){
          findedge = true;
          break;
        }
      }
//      if(it_edge != map_edge.end()){ // else nothing to do
      if(findedge){
        MElement *interel;
        // the partition with the lowest number is choosen by convention like the one corresponding to minus element
        if(_phys == dgdom->getPhysical())
        {
          if(Msg::GetCommRank() < rankOtherPart){
            interel =  new MInterfaceLine(vv, 0, Msg::GetCommRank()+1, e, it_edge->getElement());
          }
          else{
            // vv has to be oriented has the minus element --> get the
            std::vector<MVertex*> vvother = it_edge->getVertices();
            interel = new MInterfaceLine(vvother,0,Msg::GetCommRank()+1,it_edge->getElement(),e);
          }
          _groupGhostMPI->insert(it_edge->getElement());
        }
        else{ // interDomain defined between 2 partitions. The minus element has to be included in the minus domain
          groupOfElements::elementContainer::iterator itfound = dgdom->getMinusDomain()->g->end();
          for(groupOfElements::elementContainer::iterator ite=dgdom->getMinusDomain()->g->begin(); ite!=dgdom->getMinusDomain()->g->end();++ite)
          {
            if(e->getNum()==(*ite)->getNum())
            {
              itfound = ite;
              break;
            }
          }
          if(itfound!=dgdom->getMinusDomain()->g->end())
          {
            interel =  new MInterfaceLine(vv, 0, Msg::GetCommRank()+1, e, it_edge->getElement());
            if(e->getPartition() !=Msg::GetCommRank()+1)
            {
               dgdom->getMinusDomain()->_groupGhostMPI->insert(e);
            }
            else
            {
              dgdom->getPlusDomain()->_groupGhostMPI->insert(it_edge->getElement());
            }
          }
          else{ // e is included in the plus Domain
            // vv has to be oriented has the minus element --> get the
            std::vector<MVertex*> vvother = it_edge->getVertices();
            interel = new MInterfaceLine(vvother,0,Msg::GetCommRank()+1,it_edge->getElement(),e);
            if(e->getPartition() !=Msg::GetCommRank()+1)
            {
               dgdom->getPlusDomain()->_groupGhostMPI->insert(e);
            }
            else
            {
              dgdom->getMinusDomain()->_groupGhostMPI->insert(it_edge->getElement());
            }
          }
        }
        dgdom->gi->insert(interel);
        map_edge.erase(it_edge);
      }
    }
  }
}

interDomainBetweenShell::interDomainBetweenShell(const int tag, partDomain *dom1,partDomain *dom2,
                                                 const int lnum) : dgLinearShellDomain(tag,manageInterface::getKey(dom1->getPhysical(),dom2->getPhysical()),
                                                                                       10000,lnum,true),_mlawMinus(NULL), _mlawPlus(NULL)
{
  // By convention the domain with the small number is the domMinus
  // In MPI if domMinus->getPhysical() == domPlus->getPhysical() domMinus = domain on the partion with the lowest rank

  // look for element (INTERDOMAIN HAVE TO BE CREATED AFTER addDomain to solver)
  if(dom2->getPhysical() > dom1->getPhysical())
  {
    _domMinus = dom1;
    _domPlus = dom2;
  }
  else if(dom1->getPhysical() > dom2->getPhysical())
  {
    _domMinus = dom2;
    _domPlus = dom1;
  }
 #if defined(HAVE_MPI)
  else if((Msg::GetCommSize() > 1) and (dom1->getPhysical() == dom2->getPhysical()))
  {
    _domMinus = dom1; // same --> no matter
    _domPlus = dom2;
  }
  else if ( (Msg::GetCommSize() > 1) and ((dom1->g->size() !=0) and (dom2->g->size() !=0)))
  {
    groupOfElements::elementContainer::iterator it1 = dom1->g->begin();
    MElement *ele1 = *it1;
    groupOfElements::elementContainer::iterator it2 = dom2->g->begin();
    MElement *ele2 = *it2;
    if(ele1->getPartition() < ele2->getPartition())
    {
      _domMinus = dom1;
      _domPlus = dom2;
    }
    else if(ele2->getPartition() < ele1->getPartition())
    {
      _domMinus = dom2;
      _domPlus = dom1;
    }
  }
 #endif // HAVE_MPI
  else
  {
    Msg::Error("Impossible to create an interface domain between domain %d and %d",dom1->getPhysical(),dom2->getPhysical());
  }

  // create empty groupOfElements
  g = new groupOfElements();
  // set functionalSpace
  _spaceMinus = _domMinus->getFunctionSpace();
  _spacePlus = _domPlus->getFunctionSpace();
  if((_domMinus->getFunctionSpaceType() == functionSpaceType::Lagrange) and
     (_domPlus->getFunctionSpaceType() == functionSpaceType::Lagrange)){
    if(_space != NULL) delete _space;
    _space = new DgC0FullDgLagrangeBetween2DomainsFunctionSpace(_spaceMinus,_spacePlus);
    _wsp = functionSpaceType::Lagrange;
  }
  else{
    Msg::Error("Impossible to create functional space on interDomainBetweenShell %d",_phys);
  }
}


interDomainBetweenShell::interDomainBetweenShell(const interDomainBetweenShell &source) : dgLinearShellDomain(source),
                                                                                           _domMinus(source._domMinus),
                                                                                           _domPlus(source._domPlus),
                                                                                           _mlawMinus(source._mlawMinus),
                                                                                           _mlawPlus(source._mlawPlus)
{}

void interDomainBetweenShell::initializeTerms(unknownField *uf,IPField*ip,std::vector<nonLinearNeumannBC> &vNeumann)
{
  // void term (as they have to exist)
  this->ltermBulk = new nonLinearTermVoid();
  this->btermBulk = new BiNonLinearTermVoid();
  this->btermVirtBound = new BiNonLinearTermVoid();
  this->ltermVirtBound = new nonLinearTermVoid();
  this->massterm = new BiNonLinearTermVoid();

  FunctionSpace<double>* dgspace = static_cast<FunctionSpace<double>*>(_space);
  FunctionSpace<double>* dgspace1 = static_cast<FunctionSpace<double>*>(_spaceMinus);
  FunctionSpace<double>* dgspace2 = static_cast<FunctionSpace<double>*>(_spacePlus);
  dgLinearShellDomain* dgldomMinus = static_cast<dgLinearShellDomain*>(_domMinus);
  dgLinearShellDomain* dgldomPlus = static_cast<dgLinearShellDomain*>(_domPlus);
  ltermBound = new IsotropicForceInterTermC0DgShell(*dgspace1,dgspace2,_mlawMinus,_mlawPlus,_interQuad,_beta1,
                                                 _beta2,_beta3,true,ip);
  if(_interByPert)
    btermBound = new BilinearTermPerturbation<double>(ltermBound,*dgspace1,*dgspace2,uf,ip,this,_eps);
  else
    btermBound = new IsotropicStiffInterTermC0DgShell(*dgspace1,*dgspace2,_mlawMinus,
                                                  _mlawPlus,_interQuad,_beta1,_beta2,_beta3,
                                                  ip,uf,true);

  // normally no neumann BC on interDomain so no need to search for NeumannBC here

  /* same strength fracture value on both side (to avoid discontinuities) */
  /* no very elegant to put here but very efficient */
  // take the value of minus element by choice
  if(_mlawMinus->getType() == materialLaw::fracture and _mlawPlus->getType() == materialLaw::fracture)
  {
    _sameValueInterfaceGaussPoints = dgldomMinus->sameValueGaussPoint(); // if minus OK plus OK ??
    this->ensureSameValuesBothSideIPvariable(ip);
  }
}

void interDomainBetweenShell::setGaussIntegrationRule()
{
  if(_gaussorderbound == -1) {
    _gqt == Gauss ? integBound = new GaussQuadrature(5) : integBound = new GaussLobatto1DQuadrature(5);
    _gqt == Gauss ? _interQuad = new interface1DQuadrature(5) : _interQuad = new interface1DQuadrature(5,true);
  }
  else{
    _gqt == Gauss ? integBound = new GaussQuadrature(_gaussorderbound) : integBound = new GaussLobatto1DQuadrature(_gaussorderbound);
    _gqt == Gauss ? _interQuad = new interface1DQuadrature(_gaussorderbound) : _interQuad = new interface1DQuadrature(_gaussorderbound,true);
  }
  integBulk = NULL; // no element
}

void interDomainBetweenShell::computeIPVariable(AllIPState *aips,const unknownField *ufield,
                                                const IPStateBase::whichState ws, bool stiff)
{
  IntPt *GP;
  fullVector<double> dispm;
  fullVector<double> dispp;
  std::vector<Dof> R;
  shellMaterialLaw *mlawMinus = static_cast<shellMaterialLaw*>(_mlawMinus);
  shellMaterialLaw *mlawPlus = static_cast<shellMaterialLaw*>(_mlawPlus);
  for(groupOfElements::elementContainer::iterator it=gi->begin(); it!=gi->end(); ++it){
    MInterfaceLine *iele = dynamic_cast<MInterfaceLine*>(*it);
    int npts_inter= integBound->getIntPoints(iele,&GP);
    R.clear();
    _spaceMinus->getKeys(iele->getElem(0),R);
    dispm.resize(R.size());
    ufield->get(R,dispm);
    R.clear();
    _spacePlus->getKeys(iele->getElem(1),R);
    dispp.resize(R.size());
    ufield->get(R,dispp);
//    shellMaterialLaw* smlawminus = dynamic_cast<shellMaterialLaw*>(_mlawMinus);
//    shellMaterialLaw* smlawplus = dynamic_cast<shellMaterialLaw*>(_mlawPlus);
    this->computeIpv(aips,iele,GP,ws,_domMinus,_domPlus,mlawMinus,mlawPlus,dispm,dispp,false, stiff);
  }
}

void interDomainBetweenShell::setMaterialLaw(const std::map<int,materialLaw*> maplaw)
{
  // oterwise the law is already set
  if(!setmaterial){
    _domMinus->setMaterialLaw(maplaw);
    _domPlus->setMaterialLaw(maplaw);
    _mlaw = NULL;
    if(_lnum == 0){ // no fracture law prescribed --> materialLawMinus and Plus are given via domain
      _mlawMinus = _domMinus->getMaterialLaw();
      _mlawPlus = _domPlus->getMaterialLaw();
      if(_domMinus->getPhysical() == _domPlus->getPhysical())
      {
         _mlaw = _mlawMinus; // same law on both
      }
      else if(_mlawMinus->getNum() == _mlawPlus->getNum())
      {
        _mlaw = _mlawMinus; // same law on both
      }
      else
      {
        Msg::Warning("Cannot set the interface law on interDomainBetweenShell but normally it is unuse");
      }
    }
    else{
      // create law (use lnum with negative number same number for the two but it doesn't matter)
      // these law don't appear in map with all laws
      // check if minus and plus laws are already fracture by 2 laws;
      int numlawBulkMinus, numlawBulkPlus;
      if(_domMinus->getMaterialLaw()->getType() != materialLaw::fracture){
        numlawBulkMinus = _domMinus->getLawNum();
      }
      else{
        materialLaw2LawsInitializer *m2law = dynamic_cast<materialLaw2LawsInitializer*>(_domMinus->getMaterialLaw());
        numlawBulkMinus = m2law->bulkLawNumber();
      }
      if(_domPlus->getMaterialLaw()->getType() != materialLaw::fracture){
        numlawBulkPlus = _domPlus->getLawNum();
      }
      else{
        materialLaw2LawsInitializer *m2law = dynamic_cast<materialLaw2LawsInitializer*>(_domPlus->getMaterialLaw());
        numlawBulkPlus = m2law->bulkLawNumber();
      }
      _mlawMinus = new shellFractureByCohesiveLaw(-numlawBulkMinus,numlawBulkMinus,_lnum);
      _mlawPlus = new shellFractureByCohesiveLaw(-numlawBulkPlus,numlawBulkPlus,_lnum);
      if(numlawBulkMinus == numlawBulkPlus)
      {
        _mlaw = _mlawMinus;
      }
      else
      {
        Msg::Warning("Cannot set the interface law on interDomainBetweenShell but normally it is unuse");
      }
    }
    _mlawMinus->initLaws(maplaw);
    _mlawPlus->initLaws(maplaw);
    setmaterial=true;
  }
}


void interDomainBetweenShell::stabilityParameters(const double b1, const double b2, const double b3)
{
  _beta1 = b1;
  _beta2 = b2;
  _beta3 = b3;
}

void interDomainBetweenShell::matrixByPerturbation(const int iinter, const double eps)
{
  _interByPert = iinter;
  _eps = eps;

}
