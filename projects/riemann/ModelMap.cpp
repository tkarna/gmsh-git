// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#include "ModelMap.h"

namespace rm
{

  GModel* ModelMapN::createModel(const ModelMapN* mm, bool discontinousChart)
  {
    std::map<int, std::vector<MElement*> > elements;
    std::map<int, std::vector<int> > physicals;

    MElementFactory factory;
    std::map<int, MVertex*> visitedVertices;

    int dim = mm->getDim();
    if(dim > 3) {
      Msg::Error("Cannot create models of manifolds with dimension greater than 3.");
      return new GModel();
    }

    GModel* gm = mm->getModel();
    std::vector<GEntity*> entities;
    gm->getEntities(entities);

    for(unsigned int iEnt = 0; iEnt < entities.size(); iEnt++) {
      GEntity* ge = entities.at(iEnt);
      int tag = ge->tag();
      for(unsigned int iElem = 0; iElem < ge->getNumMeshElements(); iElem++) {
        MElement* me = ge->getMeshElement(iElem);
        std::vector<MVertex*> meVertices;
        for(int iV = 0; iV < me->getNumVertices(); iV++) {
          Point* p = new Point(gm, me, iV);
          MVertex* newver;
          double c[MAXDIM];
          if(dim == 0) {
            const ModelMap<0>* mm2 = (const ModelMap<0>*)mm;
            mm2->mapPoint(p, c);
            newver = new MVertex(0, 0, 0);
          }
          if(dim == 1) {
            const ModelMap<1>* mm2 = (const ModelMap<1>*)mm;
            mm2->mapPoint(p, c);
            newver = new MVertex(c[0], 0, 0);
          }
          if(dim == 2) {
            const ModelMap<2>* mm2 = (const ModelMap<2>*)mm;
            mm2->mapPoint(p, c);
            newver = new MVertex(c[0], c[1], 0);
          }
          if(dim == 3) {
            const ModelMap<3>* mm2 = (const ModelMap<3>*)mm;
            mm2->mapPoint(p, c);
            newver = new MVertex(c[0], c[1], c[2]);
          }
          delete p;

          if(discontinousChart) meVertices.push_back(newver);
          else {
            int vnum = me->getVertex(iV)->getNum();
            if(visitedVertices.count(vnum)) {
              meVertices.push_back(visitedVertices[vnum]);
              delete newver;
            }
            else {
              meVertices.push_back(newver);
              visitedVertices[vnum] = newver;
            }
          }
        }
        MElement* newme = factory.create(me->getTypeForMSH(), meVertices,
                                         me->getNum(), me->getPartition());
        elements[tag].push_back(newme);
      }
      physicals[tag] = ge->getPhysicalEntities();
    }

    GModel* newgm = GModel::createGModel(elements, physicals);
    return newgm;
  }
}
