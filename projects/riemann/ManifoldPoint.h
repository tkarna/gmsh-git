// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#ifndef _RM_MANIFOLD_POINT_H_
#define _RM_MANIFOLD_POINT_H_

#include "Point.h"
#include "ModelMap.h"
#include "Manifold.h"


namespace rm
{

  /// Class representing a point on n-manifold
  template <int n>
  class ManifoldPoint
  {
  private:
    const Manifold<n>* _m;
    double _c[n];

    mutable Point* _p;
    bool _ownsp;

  public:
    ManifoldPoint(const Manifold<n>* m, const Point* p) :
      _m(m), _ownsp(true)
    {
      _p = new Point(*p);
      m->getModelMap()->mapPoint(p, _c);
    }

    ManifoldPoint(const Manifold<n>* m, const double c[n]) :
      _m(m), _p(NULL), _ownsp(true)
    {
      vecinit<n>(c, _c);
      ModelMapType mt = m->getModelMapType();
      if(mt == MM_XYZ || mt == MM_XY || mt == MM_X) {
        double xyz[3];
        for(int i = 0; i < 3; i++) xyz[i] = c[i];
        for(int i = n; i < 3; i++) xyz[i] = 0.;
        _p = m->getPoint(xyz[0], xyz[1], xyz[2]);
      }
      if(_p == NULL) _ownsp = false;
    }

    ManifoldPoint(const Manifold<n>* m, double x, double y, double z) :
      _m(m), _ownsp(true)
    {
      _p = m->getPoint(x,y,z);
      m->getModelMap()->mapPoint(_p, _c);
    }

    ManifoldPoint(const Manifold<n>* m, MElement* me,
                  double u, double v, double w) :
      _m(m)
    {
      _p = m->getPoint(me, u, v, w);
      _ownsp = true;
      m->getModelMap()->mapPoint(_p, _c);
    }

    ManifoldPoint(const Manifold<n>* m, MElement* me, int num) :
      _m(m), _ownsp(true)
    {
      _p = m->getPoint(me, num);
      m->getModelMap()->mapPoint(_p, _c);
    }

    template <int n2>
    ManifoldPoint(const Manifold<n>* m, const ManifoldPoint<n2>& mp) :
      _m(m), _ownsp(true)
    {
      _p = mp.getPoint();
      m->getModelMap()->mapPoint(_p, _c);
      if(_p == NULL) _ownsp = false;
    }

    const Manifold<n>* getManifold() const { return _m; }
    const Point* getPoint() const
    {
      if(_p == NULL) {
        //Msg::Error("No corresponding model point for manifold point");
        return _p;
      }
      return new Point(*_p);
    }

    inline double operator() (int i) const { return _c[i]; }
    const double* getPointerToCoeffs() const { return _c; }

    void print() const
    {
      Msg::Info("Manifold point on %d-manifold", n);
      Msg::Info("  Manifold coordinates:");
      if(n == 1) {
        Msg::Info("  [ %12.5E ]", this->_c[0]);
      }
      else if(n == 2) {
        Msg::Info("  [ %12.5E %12.5E ]", this->_c[0], this->_c[1]);
      }
      else if(n == 3) {
        Msg::Info("  [ %12.5E %12.5E %12.5E ]",
                  this->_c[0], this->_c[1], this->_c[2]);
      }
      if(_p != NULL) {
        double x,y,z;
        _p->getXYZ(x,y,z);
        Msg::Info("  Model coordinates:");
        Msg::Info("  [ %12.5E %12.5E %12.5E ]", x, y, z);
      }
    }

    ~ManifoldPoint()
    {
      //if(_ownsp && _p != NULL) delete _p;
    }
  };
}

#endif
