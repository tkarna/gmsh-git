// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#ifndef _RM_SCALAR_POISSON_SOLVER_H_
#define _RM_SCALAR_POISSON_SOLVER_H_

#include "Riemann.h"
#include "dofManager.h"
#include "OS.h"
#include "linearSystem.h"
#include "linearSystemFull.h"
#include "linearSystemPETSc.h"
#include "linearSystemGMM.h"
#include "linearSystemMUMPS.h"

namespace rm
{

  template <int n>
  class WrapScalarField : public DefinedField<LinTransform<n> >
  {
  private:
    Field<Covector<n, 0> >* _f;

  public:
    WrapScalarField(Field<Covector<n,0> >* f) :
      DefinedField<LinTransform<n> >(f->getManifold()), _f(f) {}

    virtual void getValue(const Point* p, LinTransform<n>& tp) const
    {
      const int nc = TensorTraits<LinTransform<n> >::numCoeffs;
      double c[nc];
      Covector<n,0> sc = _f->operator()(p);
      double s = sc(0);

      for(int i = 0; i < nc; i++) c[i] = 0.;
      if(n >= 1) c[0] = s;
      if(n == 2) c[3] = s;
      else if(n == 3) {
        c[4] = c[8] = s;
      }
      else if(n == 4) {
        c[5] = c[10] = c[15] = s;

      }
      tp = LinTransform<n>(this->getManifold(), p, c, MANIFOLD);
    }
  };

  template <int n>
  void ScalarPoissonSolver
    (const std::vector<Manifold<n>*>& m,
     const std::vector<Field<Covector<n-1, 0> >*>& dircond,
     const std::vector<Field<Covector<n-1, n-1> >*>& neucond,
     const std::vector<Field<Covector<n, n> >*>& source,
     const std::vector<Field<Covector<n, 0> >*>& mat,
     std::vector<MeshField<Covector<n, 0> > >& phi)
  {
    std::vector<Field<LinTransform<n> >* > mat2;
    for(unsigned int i = 0; i < mat.size(); i++) {
      mat2.push_back(new WrapScalarField<n>(mat.at(i)));
    }

    std::vector<std::vector<Field<Covector<n,1> >*> > coh;
    std::vector<std::vector<std::pair<double,bool> > > cohval;

    ScalarPoissonSolver(m, dircond, neucond, source, mat2, coh, cohval, phi);

    for(unsigned int i = 0; i < mat2.size(); i++) {
      delete mat2.at(i);
    }
  }

  template <int n>
  void ScalarPoissonSolver
    (const std::vector<Manifold<n>*>& m,
     const std::vector<Field<Covector<n-1, 0> >*>& dircond,
     const std::vector<Field<Covector<n-1, n-1> >*>& neucond,
     const std::vector<Field<Covector<n, n> >*>& source,
     const std::vector<Field<LinTransform<n> >*>& mat,
     const std::vector<std::vector<Field<Covector<n,1> >*> >& coh,
     const std::vector<std::vector<std::pair<double,bool> > >& cohval,
     std::vector<MeshField<Covector<n, 0> > >& phi)
  {
    if(m.empty()) {
      Msg::Error("No manifolds given.");
      return;
    }

    // Check input
    if(m.size() != source.size() || m.size() != mat.size()) {
      Msg::Error("Source field or material parameter field not given for each manifold in the domain.");
      return;
    }

    if(coh.size() > m.size() || coh.size() != cohval.size()) {
      Msg::Error("Erronous number cohomology conditions.");
      return;
    }

    for(unsigned int i = 0; i < m.size(); i++) {
      if(m.at(i)->getTag() != source.at(i)->getManifold()->getTag() ||
         m.at(i)->getTag() != mat.at(i)->getManifold()->getTag() ) {
        Msg::Error("Source field or material parameter field cannot be transformed to corresponding domain manifold.");
        return;
      }
    }

    int numCohConds = 0;
    for(unsigned int i = 0; i < coh.size(); i++) {
      for(unsigned int j = 0; j < coh.at(i).size(); j++) {
        numCohConds++;
        if(coh.at(i).size() != cohval.at(i).size()) {
          Msg::Error("Erronous number cohomology conditions.");
          return;
        }
        if(m.at(i)->getTag() != coh.at(i).at(j)->getManifold()->getTag()) {
          Msg::Error("Cohomology field cannot be transformed to corresponding domain manifold.");
          return;
        }
      }
    }

    // Integration orders
    int intOrderN1 = 2;
    int intOrderN2 = 1;

    int intOrderNm11 = 2;
    int intOrderNm12 = 1;

    // Gmsh linear system solver interface
    linearSystem<double>* lsys;
#ifdef HAVE_PETSC
    Msg::Info("ScalarPoissonSolver uses PETSc");
    lsys = new linearSystemPETSc<double>();
#elif defined HAVE_MUMPS && !defined HAVE_PETSC
    Msg::Info("ScalarPoissonSolver uses MUMPS");
    linearSystemMUMPS<double>* lsysmumps = new linearSystemMUMPS<double>();
    lsys = lsysmumps;
#else
    Msg::Info("ScalarPoissonSolver uses LAPACK");
    lsys = new linearSystemFull<double>();
#endif

    // Gmsh linear system assembler
    dofManager<double> myAssembler(lsys);
    int doftag = 1;
    int cohtags = 2;

    // Fix Dirichlet condition nodes
    for(unsigned int iM = 0; iM < dircond.size(); iM++) {

      const Manifold<n-1>* man = dircond.at(iM)->getManifold();
      Field<Covector<n-1, 0> >& tphi = *dircond.at(iM);

      for(MElemIt it = man->firstMeshElement();
            it != man->lastMeshElement(); it++) {
        MElement* me = it->first;
        for(int iV = 0; iV < me->getNumVertices(); iV++) {
          int vert = me->getVertex(iV)->getNum();
          Point* p = man->getPoint(me, iV);
          myAssembler.fixDof(vert, doftag, tphi(p)(0));
          delete p;
        }
      }
    }
    // If no Dirichlet conditions are given, gauge one node to zero
    if(dircond.empty()) {
      const Manifold<n>* man = m.at(0);
      int vert = man->firstMeshElement()->first->getVertex(0)->getNum();
      myAssembler.fixDof(vert, doftag, 0.);
    }

    // Number DoF nodes
    int numElems = 0;
    for(unsigned int iM = 0; iM < m.size(); iM++) {
      const Manifold<n>* man = m.at(iM);
      for(MElemIt it = man->firstMeshElement();
          it != man->lastMeshElement(); it++) {
        MElement* me = it->first;
        numElems++;
        for(int iV = 0; iV < me->getNumVertices(); iV++) {
          int vert = me->getVertex(iV)->getNum();
          myAssembler.numberDof(vert, doftag);
        }
      }
    }

    // Fix and number cohomology basis function coefficients
    for(unsigned int i = 0; i < coh.size(); i++) {
      for(unsigned int j = 0; j < coh.at(i).size(); j++) {
        if(!cohval.at(i).at(j).second) {
          myAssembler.fixDof(j, cohtags+i, cohval.at(i).at(j).first);
        }
        else myAssembler.numberDof(j, cohtags+i);
      }
    }

    Msg::Info("Solving scalar Poisson problem on %d-manifold:", n);
    Msg::Info("  Elements:         %d", numElems);
    Msg::Info("  DoF's:            %d", myAssembler.sizeOfR());
    Msg::Info("  Constraints:      %d", myAssembler.sizeOfF());
    Msg::Info("  Cohomology BF's:  %d", numCohConds);


    double t1 = Cpu();
    Msg::StatusBar(true, "Assembling system ...");

    // Assemble Neumann condition vector
    for(unsigned int iM = 0; iM < neucond.size(); iM++) {

      const Manifold<n-1>* man = neucond.at(iM)->getManifold();
      Field<Covector<n-1, n-1> >& neumannField = *neucond.at(iM);

      // Iterate manifold mesh elements
      for(MElemIt it = man->firstMeshElement();
          it != man->lastMeshElement(); it++) {
        MElement* me = it->first;
        int ori = it->second;

        fullVector<double> localVector(me->getNumShapeFunctions());
        const int integrationOrder =
          intOrderNm11*me->getPolynomialOrder() + intOrderNm12;

        // Get integration points on element
        ManifoldIntegrationPoints<n-1> ips(man, me, integrationOrder);
        for(int k = 0; k < ips.getNumPoints(); k++) {
          Point* ip = ips.getPoint(k);
          double weight = ips.getWeight(k);

          // Get partition of unity at integration point
          // a.k.a nodal finite element shape functions
          std::vector<Covector<n-1, 0> > sf;
          man->getPartOfUnity(ip, sf);

          // Compute integrands at integration points
          for(int iS = 0; iS < me->getNumShapeFunctions(); iS++) {
            Covector<n-1,n-1> b = neumannField(ip) % sf.at(iS);
            localVector(iS) += b(0)*weight*ori;
          }
        }

        // Assemble local vector to linear system
        const int nbR = localVector.size();
        for (int j = 0; j < nbR; j++) {
          Dof r(me->getShapeFunctionNode(j)->getNum(), doftag);
          myAssembler.assemble(r, localVector(j));
        }
      }
    }

    // Assemble system matrix and source vector
    for(unsigned int iM = 0; iM < m.size(); iM++) {
      Manifold<n>* man = m.at(iM);
      Field<LinTransform<n> >& matField = *mat.at(iM);
      Field<Covector<n, n> >& sourceField = *source.at(iM);

      int nbC = 0;
      if(!coh.empty() && !coh.at(iM).empty()) nbC = coh.at(iM).size();
      std::vector<bool> weakCohSet;
      for(int i = 0; i < nbC; i++) weakCohSet.push_back(false);

      // Iterate manifold mesh elements
      for(MElemIt it = man->firstMeshElement();
          it != man->lastMeshElement(); it++) {
        MElement* me = it->first;
        int ori = it->second;

        fullMatrix<double> localMatrix(me->getNumShapeFunctions(),
                                       me->getNumShapeFunctions());

        fullMatrix<double> cohMatrix12(me->getNumShapeFunctions(), nbC);
        fullMatrix<double> cohMatrix21(nbC, me->getNumShapeFunctions());
        fullMatrix<double> cohMatrix22(nbC, nbC);

        fullVector<double> localVector(me->getNumShapeFunctions()+nbC);

        const int integrationOrder =
          intOrderN1*me->getPolynomialOrder() + intOrderN2;

        // Get mesh element integration points
        ManifoldIntegrationPoints<n> ips(man, me, integrationOrder);
        for(int k = 0; k < ips.getNumPoints(); k++) {
          Point* ip = ips.getPoint(k);
          double weight = ips.getWeight(k);

          //MetricTensor<n> gp;
          //man->getMetricTensor(ip, gp);
          //gp.print();

          // Get a partition of unity and their exterior derivatives
          // at integration points
          std::vector<Covector<n, 0> > sf;
          man->getPartOfUnity(ip, sf);
          std::vector<Covector<n, 1> > dsf;
          man->getDiffPartOfUnity(ip, dsf);

          LinTransform<n> mat = matField(ip);

          // Compute integrands
          for(int iS = 0; iS < me->getNumShapeFunctions(); iS++) {

            // apply hodge and material parameter
            Covector<n,n-1> ai = h(mat*dsf.at(iS));

            for(int jS = 0; jS <= iS; jS++) {
              Covector<n,n> aij = ai % dsf.at(jS);
              localMatrix(iS, jS) += aij(0)*weight;
            }

            // source field
            Covector<n,n> rho = sourceField(ip);
            Covector<n,n> bi = rho % sf.at(iS);
            localVector(iS) += bi(0)*weight*ori;
          }

          // compute cohomology basis function integrands
          if(!coh.empty() && !coh.at(iM).empty()) {
            for(unsigned int iC = 0; iC < coh.at(iM).size(); iC++) {
              Field<Covector<n,1> >& iCohField = *coh.at(iM).at(iC);
              if(!iCohField.inSupport(me)) continue;

              Covector<n,1> iCoh = iCohField(ip);
              Covector<n,n-1> iHCoh = h(mat*iCohField(ip));

              for(unsigned int jC = 0; jC < coh.at(iM).size(); jC++) {
                Field<Covector<n,1> >& jCohField = *coh.at(iM).at(jC);
                if(jC != iC && !jCohField.inSupport(me)) continue;

                Covector<n,1> jCoh = jCohField(ip);
                Covector<n,n> ijCoh = iHCoh % jCoh;
                cohMatrix22(iC, jC) += ijCoh(0)*weight;
              }

              for(int iS = 0; iS < me->getNumShapeFunctions(); iS++) {
                Covector<n,n> bi = iHCoh % dsf.at(iS);
                cohMatrix12(iS, iC) += bi(0)*weight;
              }

              // "weak" i.e. dual cohomology condition
              if(cohval.at(iM).at(iC).second && !weakCohSet.at(iC)) {
                double cohValue = cohval.at(iM).at(iC).first;
                localVector(me->getNumShapeFunctions()+iC) = cohValue;
                weakCohSet.at(iC) = true;
              }

            }
          }

        } // integration point

        // complete the matrices
        for(int i = 0; i < me->getNumShapeFunctions(); i++) {
          for(int j = 0; j < i; j++) {
            localMatrix(j, i) += localMatrix(i, j);
          }
        }
        cohMatrix21 = cohMatrix12.transpose();

        // Assemble local matrices and local vector to the linear system
        std::vector<Dof> R;
        std::vector<Dof> RC;
        for (int j = 0; j < me->getNumShapeFunctions(); j++) {
          Dof dof(me->getShapeFunctionNode(j)->getNum(), doftag);
          R.push_back(dof);
          RC.push_back(dof);
        }
        myAssembler.assemble(R, localMatrix);

        std::vector<Dof> C;
        if(!coh.empty() && !coh.at(iM).empty()) {
          for(unsigned int iC = 0; iC < coh.at(iM).size(); iC++) {
            Dof dof(iC, cohtags+iM);
            C.push_back(dof);
            RC.push_back(dof);
          }
          myAssembler.assemble(R, C, cohMatrix12);
          myAssembler.assemble(C, R, cohMatrix21);
          myAssembler.assemble(C, cohMatrix22);
        }

        myAssembler.assemble(RC, localVector);

      } // mesh element
    }

    double t2 = Cpu();
    Msg::StatusBar(true, "System assembled (%g s)", t2-t1);

    Msg::StatusBar(true, "Solving system ...");
    t1 = Cpu();
    lsys->systemSolve();
    t2 = Cpu();
    Msg::StatusBar(true, "System solved (%g s)", t2-t1);

    // Collect the result to fields, one for each manifold in the domain
    phi.clear();
    for(unsigned int iM = 0; iM < m.size(); iM++) {
      Manifold<n>* man = m.at(iM);
      MeshField<Covector<n, 0> > phi_i(man);

      for(MElemIt it = man->firstMeshElement();
          it != man->lastMeshElement(); it++) {
        MElement* me = it->first;
        for(int iV = 0; iV < me->getNumVertices(); iV++) {
          double value = 0.;
          myAssembler.getDofValue(me->getVertex(iV)->getNum(), doftag, value);
          double c[1] = {value};
          phi_i.assign(c, me, iV);
        }
      }
      phi.push_back(phi_i);
    }

    lsys->clear();
    delete lsys;
  }

  // ScalarPoissonSolver interface for simple problems
  template <int n>
  class ScalarPoissonData
  {
  private:
    std::vector<Manifold<n>*> _m;
    std::vector<Field<Covector<n-1, 0> >*> _tphi;
    std::vector<Field<Covector<n-1, n-1> >*> _thdphi;
    std::vector<Field<Covector<n, n> >*> _source;
    std::vector<Field<Covector<n, 0> >*> _mat;
    std::vector<Field<LinTransform<n> >*> _mat2;
    std::vector<std::vector<Field<Covector<n,1> >*> > _coh;
    std::vector<std::vector<std::pair<double, bool> > > _cohval;

    std::vector<int> _tphidel;
    std::vector<int> _thdphidel;
    std::vector<std::vector<int> > _cohdel;

    bool _delmat;
    bool _delmat2;
    bool _delsource;

  public:
      // Create from a single manifold
    ScalarPoissonData(Manifold<n>* m)
    {
      _m.push_back(m);
      _source.push_back(new ZeroField<Covector<n, n> >(m));
      _mat.push_back(new ConstField<Covector<n, 0> >
                     (m, new double(1.), MODEL));
      _delsource = true;
      _delmat = true;
      _delmat2 = false;
    }

    void addSource(Field<Covector<n,n> >* f)
    {
      if(_delsource) delete _source.at(0);
      _source.at(0) = f;
      _delsource = false;
    }

    void addCohomologyCond(Field<Covector<n,1> >* f, double value, bool dual)
    {
      std::pair<double, bool> val = std::make_pair(value, dual);
      if(_coh.empty()) {
        _coh.push_back(std::vector<Field<Covector<n,1> >*>(1, f));
        _cohval.push_back(std::vector<std::pair<double, bool> >(1, val));
        _cohdel.push_back(std::vector<int>(1, false));
      }
      else {
        _coh.at(0).push_back(f);
        _cohval.at(0).push_back(val);
        _cohdel.at(0).push_back(false);
      }
    }

    void setMaterial(Manifold<n>* s, double value)
    {
      clearMaterial();
      _mat.at(0) = new ConstField<Covector<n,0> >
                     (s, new double(value), MODEL);
      _delmat = true;
    }

    void setMaterial(Field<Covector<n,0> >* f)
    {
      clearMaterial();
      _mat.at(0) = f;
      _delmat = false;
    }

    void setMaterial(Field<LinTransform<n> >* f)
    {
      clearMaterial();
      _mat2.push_back(f);
      _delmat2 = false;
    }

    void addTraceCond(Manifold<n-1>* s, double value)
    {
      _tphi.push_back(new ConstField<Covector<n-1,0> >
                      (s, new double(value), MODEL));
      _tphidel.push_back(0);
    }

    void addTraceCond(Field<Covector<n-1,0> >* f)
    {
      _tphi.push_back(f);
      _tphidel.push_back(0);
    }

    void addFluxCond(Manifold<n-1>* s, double value)
    {
      _thdphi.push_back(new ConstField<Covector<n-1,n-1> >
                        (s, new double(value), MODEL));
      _thdphidel.push_back(1);
    }

    void addFluxCond(Field<Covector<n-1,n-1> >* flux)
    {
      _thdphi.push_back(flux);
      _thdphidel.push_back(0);
    }

    void clearTraceConds()
    {
      for(unsigned int i = 0; i < _tphi.size(); i++) {
        if(_tphidel.at(i)) delete _tphi.at(i);
      }
      _tphi.clear();
      _tphidel.clear();
    }

    void clearSource()
    {
      if(_delsource) delete _source.at(0);
      _source.clear();
      _source.push_back(new ZeroField<Covector<n, n> >(_m.at(0)));
      _delsource = true;
    }

    void clearFluxConds()
    {
      for(unsigned int i = 0; i < _thdphi.size(); i++) {
        if(_thdphidel.at(i)) delete _thdphi.at(i);
      }
      _thdphi.clear();
      _thdphidel.clear();
    }

    void clearCohomologyConds()
    {
      for(unsigned int i = 0; i < _coh.size(); i++) {
        for(unsigned int j = 0; j < _coh.at(i).size(); j++) {
          if(_cohdel.at(i).at(j)) delete _coh.at(i).at(j);
        }
      }
      _coh.clear();
      _cohdel.clear();
    }

    void clearMaterial()
    {
      if(_delmat) {
        delete _mat.at(0);
        _mat.at(0) = NULL;
        _delmat = false;
      }
      if(!_mat2.empty()) {
        if(_delmat2) delete _mat2.at(0);
        _mat2.clear();
        _delmat2 = false;
      }
    }

    MeshField<Covector<n, 0> > solve()
    {
      std::vector<MeshField<Covector<n, 0> > > phi;

      if(!_mat2.empty()) {
        ScalarPoissonSolver(_m, _tphi, _thdphi, _source, _mat2,
                            _coh, _cohval, phi);
      }
      else {
        std::vector<Field<LinTransform<n> >* > mat2;
        for(unsigned int i = 0; i < _mat.size(); i++) {
          mat2.push_back(new WrapScalarField<n>(_mat.at(i)));
        }
        ScalarPoissonSolver(_m, _tphi, _thdphi, _source, mat2,
                            _coh, _cohval, phi);

        for(unsigned int i = 0; i < mat2.size(); i++) {
          delete mat2.at(i);
        }
      }

      return phi.at(0);
    }

    ~ScalarPoissonData()
    {
      clearTraceConds();
      clearFluxConds();
      clearCohomologyConds();
      clearMaterial();
      if(_delsource) delete _source.at(0);
    }

};

}
#endif
