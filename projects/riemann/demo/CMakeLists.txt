# Set executable name of this project
set(PROJECT_NAME rmdemo)

# Set where riemann and gmsh directories are relative to the 
# directory of this project
set(RM_DIR ../)
set(GMSH_DIR ../../../)

# Add extra compiler options
set(PROJECT_COMPILER_OPTIONS "-O3 -Wall -pedantic -ansi")
# Link with extra libraries
set(PROJECT_LINK_LIBRARIES "")
# Add extra include directories
set(PROJECT_EXTERNAL_INCLUDES "")

# Example: to use gprof profiler
#set(PROJECT_COMPILER_OPTIONS "-Wall -pg")
#set(PROJECT_LINK_LIBRARIES "-pg")

# Source files of this project
set(PROJECT_SRC
  mainBVP.cpp)

# Normally, no need to edit below this line
# (unless you want to add build options for extra libraries)

cmake_minimum_required(VERSION 2.6 FATAL_ERROR)
if(DEFINED CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE ${CMAKE_BUILD_TYPE} CACHE STRING "Choose build type")
else(DEFINED CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE RelWithDebInfo CACHE STRING "Choose build type")
endif(DEFINED CMAKE_BUILD_TYPE)

project(${PROJECT_NAME} CXX)

if(NOT ENABLE_GMSH)
add_subdirectory(${GMSH_DIR} "${CMAKE_CURRENT_BINARY_DIR}/gmsh")
endif(NOT ENABLE_GMSH)
add_subdirectory(${RM_DIR} "${CMAKE_CURRENT_BINARY_DIR}/riemann")

if(NOT ENABLE_GMSH)
  if(ENABLE_MPI)
    find_package(MPI)
    if(MPI_FOUND)
      include(CMakeForceCompiler)
      cmake_force_cxx_compiler(${MPI_COMPILER} "MPI C++ Compiler")
  endif(MPI_FOUND)
endif(ENABLE_MPI)
endif(NOT ENABLE_GMSH)

add_definitions(${PROJECT_COMPILER_OPTIONS})
list(APPEND EXTERNAL_LIBRARIES ${PROJECT_LINK_LIBRARIES})
list(APPEND EXTERNAL_LIBRARIES ${RM_EXTERNAL_LIBRARIES})
list(APPEND EXTERNAL_INCLUDES ${RM_EXTERNAL_INCLUDES})
list(APPEND EXTERNAL_INCLUDES ${PROJECT_EXTERNAL_INCLUDES})

if(ENABLE_GMSH)
include_directories(. ${RM_DIR}/ ${EXTERNAL_INCLUDES})
else(ENABLE_GMSH)
include_directories(. ${RM_DIR}/ ${GMSH_DIR}/Common ${GMSH_DIR}/Numeric 
  ${GMSH_DIR}/Geo ${GMSH_DIR}/Mesh ${GMSH_DIR}/Solver ${GMSH_DIR}/Post 
  ${GMSH_DIR}/Plugin ${GMSH_DIR}/Graphics ${GMSH_DIR}/contrib/kbipack
   ${GMSH_EXTERNAL_INCLUDE_DIRS} ${CMAKE_CURRENT_BINARY_DIR}/gmsh/Common
   ${EXTERNAL_INCLUDES})
endif(ENABLE_GMSH)

foreach(FILE ${RM_SRC})
  list(APPEND LIST ${RM_DIR}/${FILE})  
endforeach(FILE)
set(RM_SRC ${LIST})

add_library(rm STATIC ${RM_SRC})
set_target_properties(rm PROPERTIES LINKER_LANGUAGE CXX)
target_link_libraries(rm ${EXTERNAL_LIBRARIES})

if(ENABLE_GMSH)
set(LINK_LIBRARIES rm ${EXTERNAL_LIBRARIES})
else(ENABLE_GMSH)
set(LINK_LIBRARIES shared rm ${EXTERNAL_LIBRARIES})
endif(ENABLE_GMSH)
add_executable(${PROJECT_NAME} ${PROJECT_SRC})
target_link_libraries(${PROJECT_NAME} ${LINK_LIBRARIES})

configure_file(${RM_DIR}/rmConfig.h.in 
  ${CMAKE_CURRENT_SOURCE_DIR}/${RM_DIR}/rmConfig.h)