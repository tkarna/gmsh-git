m = 0.005;
mk = 1;

r1 = 0.01;
r2 = 0.02;
r = (r2-r1)/2;

a = 0.1;
b = 0.05;

Point(newp) = {0, 0, 0, m};
Point(newp) = {0, 0, -r, m};
Point(newp) = {0, 0, r, m};
Point(newp) = {r1, 0, 0, m};
Point(newp) = {-r1, 0, 0, m};
Point(newp) = {0, r1, 0, m};
Point(newp) = {0, -r1, 0, m};

Point(newp) = {r2, 0, 0, m};
Point(newp) = {-r2, 0, 0, m};
Point(newp) = {0, r2, 0, m};
Point(newp) = {0, -r2, 0, m};

Point(newp) = {a, a, b, mk*m};
Point(newp) = {-a, a, b, mk*m};
Point(newp) = {a, -a, b, mk*m};
Point(newp) = {-a, -a, b, mk*m};
Point(newp) = {a, a, -b, mk*m};
Point(newp) = {-a, a, -b, mk*m};
Point(newp) = {a, -a, -b, mk*m};
Point(newp) = {-a, -a, -b, mk*m};

Point(newp) = {r1+r, 0, r, m};
Point(newp) = {r1+r, 0, -r, m};
Point(newp) = {-r1-r, 0, r, m};
Point(newp) = {-r1-r, 0, -r, m};
Point(newp) = {0, r1+r, r, m};
Point(newp) = {0, r1+r, -r, m};
Point(newp) = {0, -r1-r, r, m};
Point(newp) = {0, -r1-r, -r, m};

Circle(1) = {5, 1, 6};
Circle(2) = {6, 1, 4};
Circle(3) = {4, 1, 7};
Circle(4) = {7, 1, 5};
Circle(5) = {22, 3, 24};
Circle(6) = {24, 3, 20};
Circle(7) = {20, 3, 26};
Circle(8) = {26, 3, 22};
Circle(9) = {25, 2, 21};
Circle(10) = {21, 2, 27};
Circle(11) = {27, 2, 23};
Circle(12) = {23, 2, 25};
Circle(13) = {10, 1, 8};
Circle(14) = {8, 1, 11};
Circle(15) = {11, 1, 9};
Circle(16) = {9, 1, 10};

Point(newp) = {r1+r, 0, 0, m};
Point(newp) = {-r1-r, 0, 0, m};
Point(newp) = {0, r1+r, 0, m};
Point(newp) = {0, -r1-r, 0, m};

Delete {
  Point{12, 16, 17, 13, 15, 19, 18, 14};
}

Circle(17) = {24, 30, 6};
Circle(18) = {6, 30, 25};
Circle(19) = {25, 30, 10};
Circle(20) = {10, 30, 24};
Circle(21) = {5, 29, 23};
Circle(22) = {23, 29, 9};
Circle(23) = {9, 29, 22};
Circle(24) = {22, 29, 5};
Circle(25) = {20, 28, 4};
Circle(26) = {20, 28, 8};
Circle(27) = {8, 28, 21};
Circle(28) = {21, 28, 4};
Circle(29) = {26, 31, 7};
Circle(30) = {7, 31, 27};
Circle(31) = {27, 31, 11};
Circle(32) = {11, 31, 26};
Line Loop(33) = {8, 24, -4, -29};
Ruled Surface(34) = {33};
Line Loop(35) = {24, 1, -17, -5};
Ruled Surface(36) = {35};
Line Loop(37) = {17, 2, -25, -6};
Ruled Surface(38) = {37};
Line Loop(39) = {3, -29, -7, 25};
Ruled Surface(40) = {39};
Line Loop(41) = {6, 26, -13, 20};
Ruled Surface(42) = {41};
Line Loop(43) = {5, -20, -16, 23};
Ruled Surface(44) = {43};
Line Loop(45) = {15, 23, -8, -32};
Ruled Surface(46) = {45};
Line Loop(47) = {14, 32, -7, 26};
Ruled Surface(48) = {47};
Line Loop(49) = {13, 27, -9, 19};
Ruled Surface(50) = {49};
Line Loop(51) = {18, -12, -21, 1};
Ruled Surface(52) = {51};
Line Loop(53) = {4, 21, -11, -30};
Ruled Surface(54) = {53};
Line Loop(55) = {3, 30, -10, 28};
Ruled Surface(56) = {55};
Line Loop(57) = {9, 28, -2, 18};
Ruled Surface(58) = {57};
Line Loop(59) = {16, -19, -12, 22};
Ruled Surface(60) = {59};
Line Loop(61) = {10, 31, -14, 27};
Ruled Surface(62) = {61};
Line Loop(87) = {22, -15, -31, 11};
Ruled Surface(88) = {87};


Physical Surface(1) = {60, 36, 52, 38, 42, 50, 58, 48, 62, 88, 46, 54, 34, 40, 56, 44};
Physical Point(2) = {8};
