// Riemannian manifold demo project

#include "Riemann.h"
#include "ScalarPoissonSolver.h"

using namespace rm;

#if defined(HAVE_GINAC)
using namespace GiNaC;
#endif

// User defined model map from model to 2-manifold
class TestModelMap2 : public ModelMap<2>
{
private:

public:
  // Constructor
  TestModelMap2(GModel* gm) : ModelMap<2>(gm, MM_NUMERIC, MM_DIFF) {}

  // Define the mapping here,
  // how a model point maps to manifold's coordinate chart
  virtual void mapPoint(const Point* p, double c[2]) const
  {
    double x,y,z;
    p->getXYZ(x,y,z);
    c[0] = 2*x;
    c[1] = y;
  }

  // Define the Jacobian matrix of the mapping here
  virtual void getModelToManifoldJacobian(const Point* p,
                                          double jac[3*2]) const
  {
    double x,y,z;
    p->getXYZ(x,y,z); // Model coordinates of the point p

    // Define the Jacobian matrix here,
    // J_ij = dx2_i/dx_j, where x2 = x2(x_0, x_1, .., x_n)
    jac[0*3+0] = 2.;
    jac[0*3+1] = 0.;
    jac[0*3+2] = 0.;

    jac[1*3+0] = 0;
    jac[1*3+1] = 1.;
    jac[1*3+2] = 0.;

    // Resulted Jacobian matrix
    // J = [ jac[0*3+0] jac[0*3+1] jac[0*3+2]
    //       jac[1*3+0] jac[1*3+1] jac[1*3+2] ]
  }

};

// User defined model map from model to 3-manifold
class DemoModelMap3 : public ModelMap<3>
{
private:

public:
  // Constructor
  DemoModelMap3(GModel* gm) : ModelMap<3>(gm) {}

  // Define the mapping here,
  // how a model point maps to manifold's coordinate chart
  virtual void mapPoint(const Point* p, double c[3]) const
  {
    double x,y,z;
    p->getXYZ(x,y,z);
    c[0] = x;
    c[1] = y;
    c[2] = sin(x)*sin(y);
  }

  // Define the Jacobian matrix of the mapping here
  virtual void getModelToManifoldJacobian(const Point* p,
                                          double jac[3*3]) const
  {
    double x,y,z;
    p->getXYZ(x,y,z); // Model coordinates of the point p


    // Define the Jacobian matrix here,
    // J_ij = dx2_i/dx_j, where x2 = x2(x_0, x_1, .., x_n)
    jac[0*3+0] = 1.;
    jac[0*3+1] = 0.;
    jac[0*3+2] = 0.;

    jac[1*3+0] = 0;
    jac[1*3+1] = 1.;
    jac[1*3+2] = 0.;

    jac[2*3+0] = cos(x)*sin(y);
    jac[2*3+1] = sin(x)*cos(y);
    jac[2*3+2] = 0.;

    // Resulted Jacobian matrix
    // J = [ jac[0*3+0] jac[0*3+1] jac[0*3+2];
    //       jac[1*3+0] jac[1*3+1] jac[1*3+2];
    //       jac[2*3+0] jac[2*3+1] jac[2*3+2] ]
  }

};

// User defined metric tensor field on 2-manifold
class DemoMetric3 : public DefinedField<MetricTensor<3> >
{
public:
  // Constructor
  DemoMetric3(const Manifold<3>* man) : DefinedField<MetricTensor<3> >(man) {}

  // Define the metric tensor at point p here
  virtual void getValue(const Point* p, MetricTensor<3>& gp) const
  {
    double x,y,z;
    p->getXYZ(x,y,z); // Model coordinates of the point p

    double g11 = 1;
    double g12 = 0;
    double g13 = 0.;
    double g22 = 1;
    double g23 = 0.;
    double g33 = 1.;

    // Metric tensor coefficients
    double c[6] = {g11, g12,  g13, g22, g23, g33};
    // Resulting metric tensor matrix:
    // [ c[0] c[1] c[2];
    //   c[1] c[3] c[4];
    //   c[2] c[4] c[5]  ]

    // Coefficients are expressed in model coordinates
    //gp = MetricTensor<3>(this->getManifold(), p, c, MODEL);

    // Coefficients are expressed in manifold coordinates
    gp = MetricTensor<3>(this->getManifold(), p, c, MANIFOLD);

  }

};

// User defined 1-covector field on 2-manifold
class DemoField2 : public DefinedField<Covector<2, 1> >
{
public:
  // Constructor
  DemoField2(Manifold<2>* m) : DefinedField<Covector<2, 1> >(m) {}

  // Define the field here
  void getValue(const Point* p, Covector<2,1>& cov) const
  {
    double x,y,z;
    p->getXYZ(x,y,z);  // Model coordinates of the point p

    // Covector coefficients
    double c[3] = {y, x, 0};

    // Coefficients are expressed in model coordinates
    cov = Covector<2,1>(this->getManifold(), p, c, MODEL);

    // Coefficients are expressed in manifold coordinates
    //cov = Covector<2,1>(this->getManifold(), p, c, MANIFOLD);
  }
};

class PotField2 : public DefinedField<Covector<2, 0> >
{
public:
  // Constructor
  PotField2(Manifold<2>* m) : DefinedField<Covector<2, 0> >(m) {}

  // Define the field here
  void getValue(const Point* p, Covector<2,0>& cov) const
  {
    double x,y,z;
    p->getXYZ(x,y,z);  // Model coordinates of the point p

    // Covector coefficients
    double c[1] = {sin(x)*sin(y)};

    // Coefficients are expressed in model coordinates
    cov = Covector<2,0>(this->getManifold(), p, c, MODEL);

    // Coefficients are expressed in manifold coordinates
    //cov = Covector<2,1>(this->getManifold(), p, c, MANIFOLD);
  }
};

// User defined 1-covector field on 2-manifold
class DemoLinTransform2 : public DefinedField<LinTransform<2> >
{
public:
  // Constructor
  DemoLinTransform2(Manifold<2>* m) : DefinedField<LinTransform<2> >(m) {}

  // Define the field here
  void getValue(const Point* p, LinTransform<2>& t) const
  {
    double x,y,z;
    p->getXYZ(x,y,z);  // Model coordinates of the point p

    // Linear transformation coefficients
    double c[9] = {1, 0, 0, 0, 2, 0, 0, 0, 1};

    // Coefficients are expressed in model coordinates
    t = LinTransform<2>(this->getManifold(), p, c, MODEL);

    // Coefficients are expressed in manifold coordinates
    //t = LinTransform<2>(this->getManifold(), p, c, MANIFOLD);
  }
};

class SourceField2 : public DefinedField<Covector<2, 2> >
{
public:
  // Constructor
  SourceField2(Manifold<2>* m) : DefinedField<Covector<2, 2> >(m) {}

  // Define the field here
  void getValue(const Point* p, Covector<2,2>& cov) const
  {
    double x,y,z;
    p->getXYZ(x,y,z);  // Model coordinates of the point p

    MFace f = p->getMeshElement()->getFace(0);
    SVector3 n = f.normal();

    // Covector coefficients
    double c[3] = {n(0), n(1), n(2)};

    // Coefficients are expressed in model coordinates
    cov = Covector<2,2>(this->getManifold(), p, c, MODEL);
  }
};

int main(int argc, char **argv)
{
  // Initialize Gmsh
  GmshInitialize(argc, argv);
  GmshSetOption("General","Terminal",1.);
  GmshSetOption("General","Verbosity",99.); // debug verbosity

  // Create a new GModel
  GModel* gm = new GModel();

  // Read a mesh or a geometry file
  //gm->readMSH("../demo.msh");
  gm->readGEO("../demo.geo");

  // Mesh if geometry file was read
  gm->mesh(2);

  // Increase the mesh order
  //int order = 2;
  //gm->setOrderN(order,1,0);

  // Save the mesh
  gm->writeMSH("demo.msh");

  // Create Riemannian manifolds
  RiemannianManifold<2>* surf = new RiemannianManifold<2>(gm, 5);
  RiemannianManifold<2>* surfxy = new RiemannianManifold<2>(gm, 5, MODELXY);
  RiemannianManifold<1>* bottom = new RiemannianManifold<1>(gm, 3);
  RiemannianManifold<1>* left = new RiemannianManifold<1>(gm, 1);
  RiemannianManifold<1>* top = new RiemannianManifold<1>(gm, 4);
  RiemannianManifold<1>* right = new RiemannianManifold<1>(gm, 2);

  surf->drawChart("surf.msh");

  // Initialize and write user defined field on 2-manifold
  DemoField2 f(surf);
  //f.writeMSH("f.msh");

  // Create 1-covector field on 2-manifold from symbolic expressions
#if defined(HAVE_GINAC)
  symbol xs = get_symbol("xs");
  symbol ys = get_symbol("ys");
  symbol zs = get_symbol("zs");
  ex fx = xs;
  ex fy = ys;
  ex fz = zs;
  lst flist(fx,fy,fz);
  SymbolicField<Covector<2,1> > gf(surf, flist, MODEL, MODELCOORDS);
  gf = gf*2 + gf;
  gf.writeMSH("gf.msh");
#endif
  DemoLinTransform2 lt(surf);
  //(lt*f).writeMSH("ltf.msh");

  // Riemannian 3-manifold that embeds the geometry in 3-dim space
#if defined(HAVE_GINAC)
  //ex f1 = sin(xs)*cos(ys);//xs;
  //ex f2 = sin(xs)*sin(ys);//ys;
  //ex f3 = cos(xs);//sin(xs)*sin(ys);
  ex f1 = xs;
  ex f2 = ys;
  ex f3 = sin(xs)*sin(ys);
  ModelMap<3>* mm = new SymbolicModelMap<3>(gm, f1, f2, f3);
#else
  ModelMap<3>* mm = new DemoModelMap3(gm);
#endif
  RiemannianManifold<3>* embsurf =
    new RiemannianManifold<3>(gm, 5, USERCHART, mm);
  embsurf->drawChart("embsurf.msh");


  // Set the 3-manifold metric
  DemoMetric3 g(embsurf);
  surf->setRiemannianMetric(g); // pullback metric on 2-manifold
  surfxy->setRiemannianMetric(g);
  g.writeMSH("gdemo.msh");

  // Compute geodesics on 2-manifold
  Point* p0 = surf->getPoint(3.14/3., 3.14/2., 0);
  Point* p0xy = surfxy->getPoint(3.14/3., 3.14/2., 0);
  double cv[3] = {0, 0.5, 0};
  Vector<2,1> v0(surf, p0, cv, MODEL);
  Vector<2,1> v0xy(surfxy, p0xy, cv, MODEL);
  ManifoldCurve<2>* geog =
    ManifoldCurve<2>::geodesic(surf, p0, v0, 0, 1000, 10);
  geog->writeMSH("geog.msh", POS);
  ManifoldCurve<2>* geogxy =
    ManifoldCurve<2>::geodesic(surfxy, p0xy, v0xy, 0, 1000, 10);
  geogxy->writeMSH("geogxy.msh", POS);

  GModel* geogm = geog->getCurveModel();

#if defined(HAVE_GINAC)
  lst flist2(f3);
  SymbolicField<Covector<2,0> > hpot(surf, flist2, MODEL, MODELCOORDS);
#else
  PotField2 hpot(surf);
#endif
  hpot.writeMSH("hpot.msh");

  MeshField<MetricTensor<2> > pbg = pullback(g, surf);
  double E = 0.5*sqrt((v0*v0)(0)) + hpot(p0)(0);
  ConstField<Covector<2, 0> > efield(surf, new double(E), MANIFOLD);
  ModelMetric<2> modmet(surf);
  MeshField<MetricTensor<2> > potmetric = (efield-hpot)*pbg;
  potmetric.writeMSH("potmetric.msh");
  surf->setRiemannianMetric(potmetric);
  ManifoldCurve<2>* geog2 =
    ManifoldCurve<2>::geodesic(surf, p0, v0, 0, 100, 10);
  geog2->writeMSH("geog2.msh", POS);

  surf->setRiemannianMetric(g);
#if defined(HAVE_GINAC)
  ModelMap<3>* mmgeo = new SymbolicModelMap<3>(geogm, f1, f2, f3);
#else
  ModelMap<3>* mmgeo = new DemoModelMap3(geogm);
#endif
  /// Embed geodesic to the 3-manifold
  RiemannianManifold<3>* embgeo =
    new RiemannianManifold<3>(geogm, 1, USERCHART, mmgeo);
    embgeo->drawChart("embgeo.msh");

  // Solve a Poisson problem on the geometry
  //   d*d \phi = 0 in the domain
  //   \phi = 1 on left boundary
  //   \phi = 0 on right boundary
  //   t*d \phi = 0 on top and bottom boundaries

  // Problem data
  std::vector<Manifold<2>*> domain(1, surf);     // The domain
  std::vector<Field<Covector<1, 0> >*> tphi;     // Dirichlet boundary conds
  std::vector<Field<Covector<1, 1> >*> thdphi;   // Neumann boundary conds
  std::vector<Field<Covector<2, 2> >*> source;   // Source fields
  std::vector<Field<Covector<2, 0> >*> mat;      // Material coefficients


  // Zero source
  source.push_back(new ZeroField<Covector<2, 2> >(surf));

  // Constant source
  //double scoeffs[3] = {0., 0., -1.};
  //source.push_back(new ConstField<Covector<2, 2> >(surf, scoeffs, MODEL));
  //source.at(0)->writeMSH("source.msh");

  // Material coefficient = 1
  mat.push_back(new ConstField<Covector<2, 0> >
                (surf, new double(1.), MODEL));

  // Set Dirichlet conditions
  tphi.push_back(new ConstField<Covector<1,0> >
                 (left, new double(1.), MODEL));
  tphi.push_back(new ConstField<Covector<1,0> >
                 (right, new double(0.), MODEL));

  // Results storage
  std::vector<MeshField<Covector<2, 0> > > phi;
  // Run solver
  ScalarPoissonSolver(domain, tphi, thdphi, source, mat, phi);

  // Write result
  phi.at(0).writeMSH("phi.msh");

  // Read a result
  MeshField<Covector<2, 0> > phi2(surf);
  phi2.readMSH("phi.msh");
  phi2.writeMSH("phi2.msh");

  // Post-process:

  // Exterior derivative
  MeshField<Covector<2, 1> > dphi = d(phi.at(0));
  dphi.writeMSH("dphi.msh", MODEL, MODELCOORDS, POS);

  // Trace of exterior derivative to submanifold on boundary
  MeshField<Covector<1, 1> > tdphi = pullback(dphi, top);
  tdphi.writeMSH("tdphi.msh");

  // Hodge
  MeshField<Covector<2, 1> > hdphi = *dphi;
  hdphi.writeMSH("hdphi.msh");
  MeshField<Covector<2, 2> > hphi = *phi.at(0);
  hphi.writeMSH("hphi.msh");

  // Field metric
  MeshField<MetricTensor<2> > fmetric =
    symtprod(dphi, dphi) + symtprod(hdphi, hdphi);
  fmetric.writeMSH("fmetric.msh");

  // Field energy
  double energy = 0.5*i(dphi%hdphi, surf);
  Msg::Info("Field energy: %g", energy);

  // Gradient
  MeshField<Vector<2, 1> > gradphi = ~dphi;
  gradphi.writeMSH("gradphi.msh");


  // Create new Gmsh model from the manifold chart
  GModel* embsurfModel = embsurf->createModel();
  embsurfModel->writeMSH("embsurf.msh");

  // Pushforward gradient vector field to the 3-manifold based
  // on different Gmsh model
  MeshField<Vector<3, 1> > pfgradphi = pushforward(gradphi, embsurf);

  // Write vector field coefficient representation
  pfgradphi.writeMSH("pfgradphi.msh", MANIFOLD, MANIFOLDCOORDS);


  // Compute geodesic with field metric
  double cv2[3] = {1, 1, 0};
  Point* p2 = surf->getPoint(3.14, 3.14, 0.);
  surf->setRiemannianMetric(fmetric);
  ManifoldCurve<2>* geof = ManifoldCurve<2>::geodesic
    (surf, p2, Vector<2,1>(surf, p2, cv2, MODEL), 0, 100, 10);
  geof->writeMSH("geof.msh", POS);

  // Create 1-manifold from the geodesic
  RiemannianManifold<1>* geofman =
  new RiemannianManifold<1>(geof->getCurveModel(), 1);

  // Pullback and integral of dphi over the geodesic manifold
  MeshField<Covector<1, 1> > dphipbgeo = pullback(dphi, geofman);
  dphipbgeo.writeMSH("dphipbgeo.msh");
  double intdphigeo = i(dphi, geofman);
  Msg::Info("Geodesic integral: %g", intdphigeo);

  MetricVolumeForm<1> vol1(geofman);
  double intvol1 = i(vol1, geofman);
  Msg::Info("Geodesic length in model metric: %g", intvol1);
  geofman->setRiemannianMetric(fmetric);
  double intvol12 = i(vol1, geofman);
  Msg::Info("Geodesic length in field metric: %g", intvol12);

  return EXIT_SUCCESS;

  surf->setRiemannianMetric(g);

  MeshField<Covector<2, 0> > x(phi.at(0));
  x.writeMSH("x.msh");

  phi.clear();
  tphi.clear();
  tphi.push_back(new ConstField<Covector<1,0> >
                 (top, new double(1.), MODEL));
  tphi.push_back(new ConstField<Covector<1,0> >
                 (bottom, new double(0.), MODEL));
  ScalarPoissonSolver(domain, tphi, thdphi, source, mat, phi);

  MeshField<Covector<2, 0> > y(phi.at(0));
  y.writeMSH("y.msh");

  // Interpret 0-covector fields x and y as a chart for a new manifold.
  // Its metric tensor is pullback metric of Euclidean metric
  // on the model chart
  ModelMap<2>* mmxy = new FieldModelMap<2>(gm, x, y);
  std::vector<RiemannianManifold<2>*> newcoords;
  newcoords.push_back
    (new RiemannianManifold<2>(gm, 5, USERCHART, mmxy, MODELMETRIC));
  newcoords.push_back
    (new RiemannianManifold<2>(gm, 2, USERCHART, mmxy, MODELMETRIC));
  GModel* newcoordsmodel = Manifold<2>::createModel(newcoords);
  newcoordsmodel->writeMSH("newcoords.msh");

  // Write new manifold's metric
  Field<MetricTensor<2> >* ncg = newcoords.at(0)->getRiemannianMetric();
  ncg->writeMSH("ncg.msh", MANIFOLD);


  /*GModel* torus = new GModel();
  torus->readGEO("../torus.geo");
  torus->mesh(2);
  torus->writeMSH("torus.msh");

  RiemannianManifold<2>* mtorus = new RiemannianManifold<2>(torus, 1);
  RiemannianManifold<0>* mpoint = new RiemannianManifold<0>(torus, 2);


  const Point* p0tor = mtorus->getPoint(-0.01, 0., 0.);
  double cvtor[3] = {0, 0, 0.5};
  Vector<2,1> v0tor(mtorus, p0tor, cvtor, MODEL);
  ManifoldCurve<2>* geotor =
    ManifoldCurve<2>::geodesic(mtorus, p0tor, v0tor, 0, 0.5, 10);
  geotor->writeMSH("geotor.msh", MSH);

  domain = std::vector<Manifold<2>*>(1, mtorus);

  source.clear();
  source.push_back(new SourceField2(mtorus));
  source.at(0)->writeMSH("source.msh");

  mat.clear();
  mat.push_back(new ConstField<Covector<2, 0> >
                (mtorus, new double(1.), MODEL));

  tphi.clear();
  //tphi.push_back(new ConstField<Covector<0,0> >
  //             (mpoint, new double(1.), MODEL));
  phi.clear();
  ScalarPoissonSolver(domain, tphi, thdphi, source, mat, phi);

  ph.at(0).writeMSH("phi.msh");*/

  // Cleanup:

  for(unsigned int i = 0; i < tphi.size(); i++) delete tphi.at(i);
  for(unsigned int i = 0; i < thdphi.size(); i++) delete thdphi.at(i);
  for(unsigned int i = 0; i < source.size(); i++) delete source.at(i);
  for(unsigned int i = 0; i < mat.size(); i++) delete mat.at(i);

  delete mm;
  delete mmxy;
  delete surf;
  delete surfxy;
  delete embsurf;
  for(unsigned int i = 0; i < newcoords.size(); i++) delete newcoords.at(i);
  delete newcoordsmodel;
  delete bottom;
  delete left;
  delete top;
  delete right;
  delete gm;

  GmshFinalize();
}
