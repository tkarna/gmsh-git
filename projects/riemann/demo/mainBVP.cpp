// Riemannian manifold demo project

#include "Riemann.h"
#include "ScalarPoissonSolver.h"

// User defined model map from model to 3-manifold
class DemoModelMap3 : public rm::ModelMap<3>
{
private:

public:
  // Constructor
  DemoModelMap3(GModel* gm) : rm::ModelMap<3>(gm) {}

  // Define the mapping here,
  // how a model point maps to manifold's coordinate chart
  virtual void mapPoint(const rm::Point* p, double c[3]) const
  {
    double x,y,z;
    p->getXYZ(x,y,z);
    c[0] = x;
    c[1] = y;
    c[2] = sin(x)*sin(y);
  }

  // Define the Jacobian matrix of the mapping here
  virtual void getModelToManifoldJacobian(const rm::Point* p,
                                          double jac[3*3]) const
  {
    double x,y,z;
    p->getXYZ(x,y,z); // Model coordinates of the point p


    // Define the Jacobian matrix here,
    // J_ij = dx2_i/dx_j, where x2 = x2(x_0, x_1, .., x_n)
    jac[0*3+0] = 1.;
    jac[0*3+1] = 0.;
    jac[0*3+2] = 0.;

    jac[1*3+0] = 0;
    jac[1*3+1] = 1.;
    jac[1*3+2] = 0.;

    jac[2*3+0] = cos(x)*sin(y);
    jac[2*3+1] = sin(x)*cos(y);
    jac[2*3+2] = 0.;

    // Resulted Jacobian matrix
    // J = [ jac[0*3+0] jac[0*3+1] jac[0*3+2];
    //       jac[1*3+0] jac[1*3+1] jac[1*3+2];
    //       jac[2*3+0] jac[2*3+1] jac[2*3+2] ]
  }

};

// User defined metric tensor field on 2-manifold
class DemoMetric3 : public rm::DefinedField<rm::MetricTensor<3> >
{
public:
  // Constructor
  DemoMetric3(const rm::Manifold<3>* man) :
    rm::DefinedField<rm::MetricTensor<3> >(man) {}

  // Define the metric tensor at point p here
  virtual void getValue(const rm::Point* p, rm::MetricTensor<3>& gp) const
  {
    double x,y,z;
    p->getXYZ(x,y,z); // Model coordinates of the point p

    double g11 = 1;
    double g12 = 0;
    double g13 = 0.;
    double g22 = 1;
    double g23 = 0.;
    double g33 = 1.;

    // Metric tensor coefficients
    double c[6] = {g11, g12,  g13, g22, g23, g33};
    // Resulting metric tensor matrix:
    // [ c[0] c[1] c[2];
    //   c[1] c[3] c[4];
    //   c[2] c[4] c[5]  ]

    // Coefficients are expressed in model coordinates
    //gp = MetricTensor<3>(this->getManifold(), p, c, MODEL);

    // Coefficients are expressed in manifold coordinates
    gp = rm::MetricTensor<3>(this->getManifold(), p, c, rm::MANIFOLD);

  }

};

int main(int argc, char **argv)
{
  // Initialize Gmsh
  GmshInitialize(argc, argv);
  GmshSetOption("General","Terminal",1.);
  GmshSetOption("General","Verbosity",99.); // debug verbosity

  // Create a new GModel
  GModel* gm = new GModel();

  // Read a mesh or a geometry file
  //gm->readMSH("../demo.msh");
  gm->readGEO("../demo.geo");

  // Mesh if geometry file was read
  gm->mesh(2);

  // Increase the mesh order
  //int order = 2;
  //gm->setOrderN(order,1,0);

  // Save the mesh
  gm->writeMSH("demo.msh");

  rm::RiemannianManifold<2>* surf = new rm::RiemannianManifold<2>(gm, 5);
  rm::RiemannianManifold<1>* bottom = new rm::RiemannianManifold<1>(gm, 3);
  rm::RiemannianManifold<1>* left = new rm::RiemannianManifold<1>(gm, 1);
  rm::RiemannianManifold<1>* top = new rm::RiemannianManifold<1>(gm, 4);
  rm::RiemannianManifold<1>* right = new rm::RiemannianManifold<1>(gm, 2);

  // Riemannian 3-manifold that embeds the geometry in 3-dim space
  /*#if defined(HAVE_GINAC)
  GiNaC::symbol xs = rm::get_symbol("xs");
  GiNaC::symbol ys = rm::get_symbol("ys");
  GiNaC::symbol zs = rm::get_symbol("zs");
  GiNaC::ex f1 = xs;
  GiNaC::ex f2 = ys;
  GiNaC::ex f3 = sin(xs)*sin(ys);
  rm::ModelMap<3>* mm = new rm::SymbolicModelMap<3>(gm, f1, f2, f3);
  #else*/
  rm::ModelMap<3>* mm = new DemoModelMap3(gm);
  //#endif
  rm::RiemannianManifold<3>* embsurf =
    new rm::RiemannianManifold<3>(gm, 5, rm::USERCHART, mm);
  embsurf->drawChart("embsurf.msh");

  // Set the 3-manifold metric
  DemoMetric3 g(embsurf);
  surf->setRiemannianMetric(g); // pullback metric on 2-manifold
  g.writeMSH("gdemo.msh");


  // Solve a Poisson problem on the geometry
  //   d*d \phi = 0 in the domain
  //   \phi = 1 on left boundary
  //   \phi = 0 on right boundary
  //   t*d \phi = 0 on top and bottom boundaries

  // Problem data
  std::vector<rm::Manifold<2>*> domain(1, surf);     // The domain
  std::vector<rm::Field<rm::Covector<1, 0> >*> tphi;     // Dirichlet boundary conds
  std::vector<rm::Field<rm::Covector<1, 1> >*> thdphi;   // Neumann boundary conds
  std::vector<rm::Field<rm::Covector<2, 2> >*> source;   // Source fields
  std::vector<rm::Field<rm::Covector<2, 0> >*> mat;      // Material coefficients


  // Zero source
  source.push_back(new rm::ZeroField<rm::Covector<2, 2> >(surf));

  // Constant source
  //double scoeffs[3] = {0., 0., -1.};
  //source.push_back(new ConstField<Covector<2, 2> >(surf, scoeffs, MODEL));
  //source.at(0)->writeMSH("source.msh");

  // Material coefficient = 1
  mat.push_back(new rm::ConstField<rm::Covector<2, 0> >
                (surf, new double(1.), rm::MODEL));

  // Set Dirichlet conditions
  tphi.push_back(new rm::ConstField<rm::Covector<1,0> >
                 (left, new double(0.), rm::MODEL));
  tphi.push_back(new rm::ConstField<rm::Covector<1,0> >
                 (right, new double(1.), rm::MODEL));

  // Results storage
  std::vector<rm::MeshField<rm::Covector<2, 0> > > phi;
  // Run solver
  ScalarPoissonSolver(domain, tphi, thdphi, source, mat, phi);

  // Write result
  phi.at(0).writeMSH("phi.msh");

  // Read a result
  rm::MeshField<rm::Covector<2, 0> > phi2(surf);
  phi2.readMSH("phi.msh");
  phi2.writeMSH("phi2.msh");

  // Post-process:

  // Exterior derivative
  rm::MeshField<rm::Covector<2, 1> > dphi = d(phi.at(0));
  dphi.writeMSH("dphi.msh", rm::MODEL, rm::MODELCOORDS, rm::POS);

  // Trace of exterior derivative to submanifold on boundary
  rm::MeshField<rm::Covector<1, 1> > tdphi = pullback(dphi, top);
  tdphi.writeMSH("tdphi.msh");

  // Hodge
  rm::MeshField<rm::Covector<2, 1> > hdphi = *dphi;
  hdphi.writeMSH("hdphi.msh");

  // Field metric
  rm::MeshField<rm::MetricTensor<2> > fmetric =
    symtprod(dphi, dphi) + symtprod(hdphi, hdphi);
  fmetric.writeMSH("fmetric.msh");

  // Field energy
  double energy = 0.5*i(dphi%hdphi, surf);
  Msg::Info("Field energy: %g", energy);

  // Gradient
  rm::MeshField<rm::Vector<2, 1> > gradphi = ~dphi;
  gradphi.writeMSH("gradphi.msh");

  // Create new Gmsh model from the manifold chart
  GModel* embsurfModel = embsurf->createModel();
  embsurfModel->writeMSH("embsurf.msh");
  delete embsurfModel;

  // Pushforward gradient vector field to the 3-manifold
  rm::MeshField<rm::Vector<3, 1> > pfgradphi = pushforward(gradphi, embsurf);

  // Write vector field coefficient representation
  pfgradphi.writeMSH("pfgradphi.msh", rm::MANIFOLD, rm::MANIFOLDCOORDS);


  delete mm;

  delete surf;
  delete bottom;
  delete left;
  delete top;
  delete right;

  delete gm;

}
