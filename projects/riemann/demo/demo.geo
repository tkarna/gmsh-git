m = 0.1;

pi=3.142;
a = 2*pi;
b = 2*pi;

Point(newp) = {0, 0, 0, m};
Point(newp) = {a, 0, 0, m};
Point(newp) = {a, b, 0, m};
Point(newp) = {0, b, 0, m};

Point(newp) = {2*a, 0, 0, m};
Point(newp) = {2*a, b, 0, m};

Line(1) = {3, 2};
Line(2) = {2, 1};
Line(3) = {1, 4};
Line(4) = {4, 3};
Line Loop(5) = {1, 2, 3, 4};
Plane Surface(6) = {5};

/*Line(7) = {3, 6};
Line(8) = {6, 5};
Line(9) = {5, 2};
Line Loop(10) = {8, 9, -1, 7};
Plane Surface(11) = {10};*/

Physical Line(1) = {3};
Physical Line(2) = {1};
Physical Line(3) = {2};
Physical Line(4) = {4};
Physical Surface(5) = {6};
//Physical Line(6) = {4, 1, 2, 3};

//Physical Surface(7) = {11};
//Physical Surface(8) = {6, 11};

Physical Point(9) = {3};
