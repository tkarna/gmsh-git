// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#ifndef _RM_WEAK_DERIVATIVE_SOLVER_H_
#define _RM_WEAK_DERIVATIVE_SOLVER_H_

#include "Manifold.h"
#include "Field.h"
#include "dofManager.h"
#include "OS.h"
#include "linearSystem.h"
#include "linearSystemFull.h"
#include "linearSystemPETSc.h"

namespace rm {

  /// Solves the equation
  /// int(domain) df/dx*phi = - int(domain) f*d(phi)/dx
  ///                         + int(domainBoundary) f*phi  forall phi
  /// for df/dx by setting df/dx = sum_i c_i*phi_i,
  /// where phi's are nodal basis functions
  ///
  /// I.e. it finds continuous (for better or for worse!)
  /// weak partial derivative of a scalar field f

  /// Note: the manifold should have continuous parametrization,
  /// (I.e mesh element uvw-parametrization does not do)
  template <int n>
  class WeakDerivativeSolver
  {
  private:
    const Manifold<n>* _m;
    const Manifold<n-1>* _mbd;
    linearSystem<double>* _lsys;
    dofManager<double>* _myAssembler;

    int _doftag;

    int _intOrder2;
    int _intOrder1;

  public:
    WeakDerivativeSolver(const Manifold<n>* m, WeakDer wdjob=DER) :
    _m(m), _mbd(NULL), _lsys(NULL), _myAssembler(NULL), _doftag(1),
      _intOrder2(2), _intOrder1(1)
    {
      _mbd = m->getBoundary();
      if(_mbd == NULL) {
        Msg::Fatal("Cannot solve weak partial derivatives, manifold boundary is not set.");
      }
      if(m->getModelMap()->getModelMapType() == MM_UVW) {
        Msg::Error("Manifold parametrization is not continuous.");
      }

#ifdef HAVE_PETSC
      _lsys = new linearSystemPETSc<double>();
#else
      _lsys = new linearSystemFull<double>();
#endif

      _myAssembler = new dofManager<double>(_lsys);

      int numElems = 0;
      for(MElemIt it = _m->firstMeshElement();
          it != _m->lastMeshElement(); it++) {
        MElement* me = it->first;
        numElems++;
        for(int iV = 0; iV < me->getNumVertices(); iV++) {
          MVertex* vert = me->getVertex(iV);
          _myAssembler->numberVertex(vert, 0, _doftag);
        }
      }

      Msg::Info("Solving weak derivatives on %d-manifold:", n);
      Msg::Info("  Elements:     %d", numElems);
      Msg::Info("  DoF's:        %d", _myAssembler->sizeOfR());
      Msg::Info("  Constraints:  %d", _myAssembler->sizeOfF());

      double t1 = Cpu();
      Msg::StatusBar(true, "Assembling system matrix ...");

      for(MElemIt it = _m->firstMeshElement();
          it != _m->lastMeshElement(); it++) {
        MElement* me = it->first;
        fullMatrix<double> localMatrix(me->getNumShapeFunctions(),
                                       me->getNumShapeFunctions());
        const int integrationOrder =
          _intOrder1*me->getPolynomialOrder() + _intOrder2;

        ManifoldIntegrationPoints<n> ips(_m, me, integrationOrder);
        for(int k = 0; k < ips.getNumPoints(); k++) {
          Point* ip = ips.getPoint(k);
          double weight = ips.getWeight(k);

          std::vector<Covector<n, 0> > sf;
          _m->getPartOfUnity(ip, sf);

          if(wdjob == DER) {
            for(int iS = 0; iS < me->getNumShapeFunctions(); iS++) {
              for(int jS = 0; jS <= iS; jS++) {
                double val[1] = {1.};
                Covector<n,n> a(_m, ip, val, MANIFOLD);
                a = a%(sf.at(iS)%sf.at(jS));
                localMatrix(iS, jS) += a(0)*weight;
              }
            }
          }
          else if(wdjob == INVDER) {

          }
        }
        for(int i = 0; i < me->getNumShapeFunctions(); i++) {
          for(int j = 0; j < i; j++) {
            localMatrix(j, i) += localMatrix(i, j);
          }
        }
        const int nbR = localMatrix.size1();
        std::vector<Dof> R;
        R.reserve(nbR);
        for (int j = 0; j < nbR; j++) {
          Dof r(me->getShapeFunctionNode(j)->getNum(),
                Dof::createTypeWithTwoInts(0, _doftag));
          R.push_back(r);
        }
        _myAssembler->assemble(R, localMatrix);
      }
      double t2 = Cpu();
      Msg::StatusBar(true, "System matrix assembled (%g s)", t2-t1);
    }


    MeshField<Covector<n,0> >
    getWeakExteriorDerivativeInverse(const Field<Covector<n,1> >& f)
    {

    }

    template <class TensorType>
    MeshField<Covector<n,0> > getWeakPartialDerivative
    (const Field<TensorType>& f, int iPartial, int iComp=0)
    {
      _lsys->zeroRightHandSide();
      MeshField<Covector<n,0> > phi(_m);
      if(iPartial > n ||
         iComp > TensorTraits<TensorType>::numCoeffs) return phi;
      if(f.getManifold()->getNum() != _m->getNum()) {
        Msg::Error("Tensor field is not defined on this manifold.");
        return phi;
      }


      double t1 = Cpu();
      Msg::StatusBar(true, "Assembling right hand side ...");

      for(MElemIt it = _m->firstMeshElement();
          it != _m->lastMeshElement(); it++) {
        MElement* me = it->first;
        fullVector<double> localVector(me->getNumShapeFunctions());

        const int integrationOrder =
          _intOrder1*me->getPolynomialOrder() + _intOrder2;

        ManifoldIntegrationPoints<n> ips(_m, me, integrationOrder);
        for(int k = 0; k < ips.getNumPoints(); k++) {
          Point* ip = ips.getPoint(k);
          double weight = ips.getWeight(k);

          std::vector<Covector<n,1> > dsf;
          _m->getDiffPartOfUnity(ip, dsf);

          double fpComp = f(ip)(iComp);

          for(int iS = 0; iS < me->getNumShapeFunctions(); iS++) {

            double sfiPartial = dsf.at(iS)(iPartial);

            double val[1] = {-1.};
            Covector<n,n> a(_m, ip, val, MANIFOLD);
            localVector(iS) += a(0)*weight*fpComp*sfiPartial;
          }
        }
        const int nbR = localVector.size();
        std::vector<Dof> R;
        R.reserve(nbR);
        for (int j = 0; j < nbR; j++) {
          Dof r(me->getShapeFunctionNode(j)->getNum(),
                Dof::createTypeWithTwoInts(0, _doftag));
          R.push_back(r);
        }
        _myAssembler->assemble(R, localVector);
      }

      ManifoldMap<n-1,n> trace(_mbd, _m);
      for(MElemIt it = _mbd->firstMeshElement();
          it != _mbd->lastMeshElement(); it++) {
        MElement* me = it->first;
        int meOrientation = it->second;
        fullVector<double> localVector(me->getNumShapeFunctions());

        const int integrationOrder =
          _intOrder1*me->getPolynomialOrder() + _intOrder2;

        ManifoldIntegrationPoints<n-1> ips(_mbd, me, integrationOrder);
        for(int k = 0; k < ips.getNumPoints(); k++) {
          Point* ip = ips.getPoint(k);
          double weight = ips.getWeight(k);
          const Point* ipm = trace.mapPointToCod(ip);

          std::vector<Covector<n-1, 0> > sf;
          _mbd->getPartOfUnity(ip, sf);

          double fpComp = f(ipm)(iComp);
          for(int iS = 0; iS < me->getNumShapeFunctions(); iS++) {
            double valhs[n];
            vecinit<n>(valhs);
            if(n-1 == 1) {
              if(iPartial == 0) valhs[1] = 1.;
              else if(iPartial == 1) valhs[0] = -1.;
            }
            else if(n == 2) valhs[iPartial] = 1.;

            Covector<n,n-1> ahs(_m, ipm, valhs, MANIFOLD);
            Covector<n-1,n-1> a(_mbd, ip);
            trace.transformTensorToDom(ahs, a);

            a = a%sf.at(iS);
            localVector(iS) += a(0)*weight*meOrientation*fpComp;
          }
        }
        const int nbR = localVector.size();
        for (int j = 0; j < nbR; j++) {
          Dof r(me->getShapeFunctionNode(j)->getNum(),
                Dof::createTypeWithTwoInts(0, _doftag));
          _myAssembler->assemble(r, localVector(j));
        }
      }
      double t2 = Cpu();
      Msg::StatusBar(true, "Right hand side assembled (%g s)", t2-t1);

      Msg::StatusBar(true, "Solving system ...");
      t1 = Cpu();
      _lsys->systemSolve();
      t2 = Cpu();
      Msg::StatusBar(true, "System solved (%g s)", t2-t1);

      for(MElemIt it = _m->firstMeshElement();
            it != _m->lastMeshElement(); it++) {
        MElement* me = it->first;
        for(int iV = 0; iV < me->getNumVertices(); iV++) {
          double value = 0.;
          _myAssembler->getDofValue(me->getVertex(iV), 0, _doftag, value);
          double c[1] = {value};
          phi.assign(c, me, iV);
        }
      }
      _lsys->zeroRightHandSide();
      return phi;
    }



    ~WeakDerivativeSolver()
    {
      _lsys->clear();
      delete _myAssembler;
      delete _lsys;
    }


  };




}

#endif
