// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#include "Riemann.h"

namespace rm
{  

  /// Get Gmsh model physical groups and their orientations
  void getPhysicalGroups(GModel* gm,
                         std::map<int,
                                  std::pair< std::vector<GEntity*>,
                                             std::vector<int> > > groups[4])
  {
    std::vector<GEntity*> entities;
    gm->getEntities(entities);
    for(unsigned int i = 0; i < entities.size(); i++){
      GEntity* e = entities[i];
      std::map<int, std::pair<std::vector<GEntity*>, std::vector<int> > >
	&group(groups[e->dim()]);
      for(unsigned int j = 0; j < e->physicals.size(); j++){	
	// physicals can be stored with negative signs when the entity
	// should be "reversed"
	int pp = e->physicals[j];
	int p = std::abs(pp);

	if(std::find(group[p].first.begin(), group[p].first.end(), e) == group[p].first.end()) {
	  group[p].first.push_back(e);
	  group[p].second.push_back(pp/p);
	}
      }
    }
  }

  /// Create a map mesh element -> orientation of a physical group
  void processMeshElements(std::vector<GEntity*>& ents,
                           std::vector<int>& entsOr,
                           std::map<MElement*, int, MElemLessThanNum>& elems)
  {
    if(entsOr.size() != ents.size()) {
      entsOr = std::vector<int>(ents.size(), 1);
      Msg::Warning("Invalid Gmsh entity orientations");
    }

    for(unsigned int i = 0; i < ents.size(); i++){
      int o = entsOr[i];
      if(ents[i]->getNumMeshElements() == 0) {
        Msg::Fatal("Manifold domain is not entirely meshed.");
      }
      for(unsigned int j = 0; j < ents[i]->getNumMeshElements(); j++){
        MElement* me = ents[i]->getMeshElement(j);
        me->setVolumePositive();
        elems[me] = o;
      }
    }
  }

  void processMeshElements(GEntity* ent,
                           std::set<MElement*, MElemLessThanNum>& elems)
  {
    if(ent->getNumMeshElements() == 0) {
      Msg::Fatal("Manifold domain is not entirely meshed.");
    }
    for(unsigned int j = 0; j < ent->getNumMeshElements(); j++){
      MElement* me = ent->getMeshElement(j);
      me->setVolumePositive();
      elems.insert(me);
    }
  }

  bool meshElementInTrace(MElement* hi, MElement* lo)
  {
    if(hi->getDim() != lo->getDim()+1) return false;
    std::multiset<int> vertexNums;
    for(int i = 0; i < hi->getNumVertices(); i++)
      vertexNums.insert(hi->getVertex(i)->getNum());
    for(int i = 0; i < lo->getNumVertices(); i++)
      vertexNums.insert(lo->getVertex(i)->getNum());
    for(int i = 0; i < lo->getNumVertices(); i++)
      if(vertexNums.count(lo->getVertex(i)->getNum()) != 2)
        return false;
    return true;
  }

  bool meshElementsTouch(MElement* e1, MElement* e2)
  {
    std::set<int> vertexNums;
    for(int i = 0; i < e1->getNumVertices(); i++)
      vertexNums.insert(e1->getVertex(i)->getNum());

    for(int i = 0; i < e2->getNumVertices(); i++)
      if(vertexNums.count(e2->getVertex(i)->getNum())) return true;

    return false;
  }

  bool meshElementInTrace(MElement* hi, MElement* lo,
                          std::map<int, int>& vertexRelation, bool hi2lo)
  {
    if(hi->getDim() != lo->getDim()+1) return false;

    std::multiset<int> vertexNums;
    std::map<int, std::pair<int, int> > vertexToIndices;
    for(int i = 0; i < hi->getNumVertices(); i++) {
      int num = hi->getVertex(i)->getNum();
      vertexNums.insert(num);
      vertexToIndices[num].first = i;
    }
    for(int i = 0; i < lo->getNumVertices(); i++) {
      int num = lo->getVertex(i)->getNum();
      vertexNums.insert(num);
      vertexToIndices[num].second = i;
    }

    for(int i = 0; i < lo->getNumVertices(); i++)
      if(vertexNums.count(lo->getVertex(i)->getNum()) != 2)
        return false;

    if(hi2lo)
      for(int i = 0; i < hi->getNumVertices(); i++)
        vertexRelation[i] =
          vertexToIndices[hi->getVertex(i)->getNum()].second;
    else
      for(int i = 0; i < lo->getNumVertices(); i++)
        vertexRelation[i] =
          vertexToIndices[lo->getVertex(i)->getNum()].first;

    return true;
  }

  int nCk(int n, int k)
  {
    if(n == k || k == 0) return 1;
    if(k == 1 || k == n-1) return n;

    int fact_n = 1;
    for(int i = 2; i <= n; i++) fact_n *= i;
    int fact_k = 1;
    for(int i = 2; i <= k; i++) fact_k *= i;
    int fact_nmk = 1;
    for(int i = 2; i <= n-k; i++) fact_nmk *= i;
    return fact_n/(fact_nmk*fact_k);
  }

  int numCoeffs(int n, int k, int l, TensorSymmetry s, Representation r)
  {
    if(r == MODEL) n = 3;
    if(s == SKEWSYMMETRIC) {
      if(k > l) return nCk(n, k);
      else return nCk(n, l);
    }
    else if(s == SYMMETRIC) {
      return (n*n+n)/2;
    }
    else return n*n;
  }

#if defined(HAVE_GINAC)
  const GiNaC::symbol & get_symbol(const std::string& s)
  {
    static std::map<std::string, GiNaC::symbol> directory;
    std::map<std::string, GiNaC::symbol>::iterator i = directory.find(s);
    if (i != directory.end())
      return i->second;
    else
      return directory.insert(make_pair(s, GiNaC::symbol(s))).first->second;
  }

  double evalExpression(const double* x, const int nx,
                        const GiNaC::FUNCP_CUBA f)
  {
    const int in[1] = {nx};
    const int out[1] = {1};
    double y[1] = {0};
    f(in, x, out, y);
    return y[0];
  }

  bool compileExpressions(const GiNaC::lst& expressions,
                          const std::vector<GiNaC::symbol>& symbols,
                          std::vector<GiNaC::FUNCP_CUBA>& funcptrs)
  {
    GiNaC::lst symslst;
    for(unsigned int i = 0; i < symbols.size(); i++)
      symslst.append(symbols[i]);

    funcptrs.clear();
    funcptrs.resize(expressions.nops());
    double t1 = Cpu();
    Msg::Debug("Compiling symbolic expressions ...");

    int i = 0;
    for (GiNaC::lst::const_iterator it = expressions.begin();
         it != expressions.end(); ++it) {
      try{
        compile_ex(GiNaC::lst(*it), symslst, funcptrs.at(i));
      }
      catch(...) {
        Msg::Error("Could not evaluate symbolic expression.");
        return false;
      }
      i++;
    }
    double t2 = Cpu();
    Msg::Debug("Symbolic expressions compiled (%g s)", t2-t1);
    return true;
  }
#endif

}
