// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#ifndef _RM_MANIFOLD_MAP_H_
#define _RM_MANIFOLD_MAP_H_

#include "ModelMap.h"
#include "Manifold.h"
#include "Field.h"

namespace rm
{
  bool meshElementInTrace(MElement* hi, MElement* lo);
  bool meshElementInTrace(MElement* hi, MElement* lo,
                          std::map<int, int>& vertexRelation, bool hi2lo);

  /// Map a model point from a manifold to other manifold
  /// Creates a new point if return value is true
  //  (The model (x, y, z) coordinates are necessarily the same but
  //   the mesh element representation may differ)
  bool mapPointToManifold(const ManifoldN* dom, const ManifoldN* cod,
                          const Point* p, const Point*& mp);

  /// Differentials of maps between a n-manifold and a m-manifold.
  template <int n, int m>
  class ManifoldMap
  {
  protected:

    /// Map domain manifold
    const Manifold<n>* _dom;

    /// Map codomain manifold
    const Manifold<m>* _cod;

  public:

    /// Construct from two manifolds
    ManifoldMap(const Manifold<n>* dom, const Manifold<m>* cod) :
      _dom(dom), _cod(cod) {}

    /// Get the domain manifold
    const Manifold<n>* getDomain() const { return _dom; }

    /// Get the codomain manifold
    const Manifold<m>* getCodomain() const { return _cod; }

    bool isDomToCodMapDiff(TensorSpace tspace) const
    {
      if(tspace == TANGENT) {
        if(_dom->getModelMap()->getModelMapProp() == MM_DIFFMORPH)
          return true;
      }
      else if(tspace == COTANGENT) {
        if(_cod->getModelMap()->getModelMapProp() == MM_DIFFMORPH)
          return true;
      }
      else {
        if(_dom->getModelMap()->getModelMapProp() == MM_DIFFMORPH &&
           _cod->getModelMap()->getModelMapProp() == MM_DIFFMORPH)
          return true;
      }
      return false;
    }

    bool isCodToDomMapDiff(TensorSpace tspace) const
    {
      if(tspace == TANGENT) {
        if(_cod->getModelMap()->getModelMapProp() == MM_DIFFMORPH)
          return true;
      }
      else if(tspace == COTANGENT) {
        if(_dom->getModelMap()->getModelMapProp() == MM_DIFFMORPH)
          return true;
      }
      else {
        if(_dom->getModelMap()->getModelMapProp() == MM_DIFFMORPH &&
           _cod->getModelMap()->getModelMapProp() == MM_DIFFMORPH)
          return true;
      }
      return false;
    }


    /// Get the differential of this manifold map
    /// from domain manifold to codomain manifold
    /// using the model maps of those manifolds
    virtual bool getDiffMapToCod(const Point* p, const Point*& p2,
                                 double jac[MAYBE(m*n)], TensorSpace tspace)
    {
      double jac1[MAYBE(3*n)];
      double jac2[MAYBE(m*3)];
      vecinit<m*n>(jac);
      if(!_dom->getModelMap()->getDiffMapToModel(p, jac1, tspace))
        return false;
      if(p2 == NULL)
        if(!mapPointToManifold(_dom, _cod, p, p2)) return false;
      if(!_cod->getModelMap()->getDiffMapToManifold(p2, jac2, tspace))
        return false;
      matmat<m,3,n>(jac2, jac1, jac);
      return true;
    }

    /// Get the differential of this manifold map
    /// from codomain manifold to domain manifold
    /// using the model maps of those manifolds
    virtual bool getDiffMapToDom(const Point* p, const Point*& p2,
                                 double jac[MAYBE(n*m)], TensorSpace tspace)
    {
      double jac1[MAYBE(3*m)];
      double jac2[MAYBE(n*3)];
      vecinit<MAYBE(n*m)>(jac);
      if(!_cod->getModelMap()->getDiffMapToModel(p, jac1, tspace))
        return false;
      if(p2 == NULL)
        if(!mapPointToManifold(_cod, _dom, p, p2) ) return false;
      if(!_dom->getModelMap()->getDiffMapToManifold(p2, jac2, tspace))
        return false;
      matmat<n,3,m>(jac2, jac1, jac);
      return true;
    }

    /// Map a model point from domain manifold to codomain manifold
    /// Creates a new point if return value is true
    //  (The model (x, y, z) coordinates are necessarily the same but
    //   the mesh element representation may differ)
    const Point* mapPointToCod(const Point* p, bool report=true) const
    {
      const Point* p2;
      if(!mapPointToManifold(_dom, _cod, p, p2) && report) {
        Msg::Debug("Point could not be mapped to manifold:");
        p->print();
      }
      return p2;
    }

    /// Map a model point from codomain manifold to domain manifold
    /// Creates a new point if return value is true
    //  (The model (x, y, z) coordinates are necessarily the same but
    //   the mesh element representation may differ)
    const Point* mapPointToDom(const Point* p, bool report=true) const
    {
      const Point* p2;
      if(!mapPointToManifold(_cod, _dom, p, p2) && report){
        Msg::Debug("Point could not be mapped to manifold:");
        p->print();
      }
      return p2;
    }

    /// Transform a tensor to domain or codomain manifold
    // (When the manifolds are not based on the same physical group
    //  has complexity O(log(n)), where n is the number of mesh elements on the
    //  image manifold, else O(1).
    //  If tracing/embedding to different
    //  dimensinal manifold, O(2*log(n)+log(N)),
    //  where N is whatever dominates the octree search
    //  of mesh elements from the whole Gmsh model)
    template <class TensorType1, class TensorType2>
    bool transformTensorToCod(const TensorType1& x, TensorType2& y)
    {
      const int l = TensorTraits<TensorType1>::coIndex;
      const int k = TensorTraits<TensorType1>::ctrIndex;
      if(x.getManifold()->getNum() != _dom->getNum()) {
        Msg::Error("Given tensor does not belong to the domain of this manifold map.");
        return false;
      }
      if(_dom->getNum() == _cod->getNum()) {
        y = TensorType2(_cod, x.getPoint(),
                        x.getPointerToCoeffs(), MANIFOLD);
        return true;
      }

      /*const Point* px = x.getPoint();
      const Point* py = y.getPoint();
      if(py == NULL)
        if(!mapPointToManifold(_dom, _cod, px, py) ) return false;

      int o1 = _dom->getOrientation(px->getMeshElement());
      int o2 = _cod->getOrientation(py->getMeshElement());
      double o = o1*o2;

      double xmc[TensorTraits<TensorType1>::numModelCoeffs];
      x.getModelCoeffs(xmc);
      y = TensorType2(_cod, py, xmc, MODEL)*o;
      return true;*/

      if(!this->isDomToCodMapDiff(x.getTensorSpace()) && (k || l) )
        return false;

      double xmcoeffs[TensorTraits<TensorType1>::numModelCoeffs];
      if(!x.getModelCoeffs(xmcoeffs)) return false;
      y = TensorType2(_cod, y.getPoint(), xmcoeffs, MODEL);
      return true;
    }

    template <class TensorType1, class TensorType2>
    bool transformTensorToDom(const TensorType1& x, TensorType2& y)
    {
      const int l = TensorTraits<TensorType1>::coIndex;
      const int k = TensorTraits<TensorType1>::ctrIndex;
      if(x.getManifold()->getNum() != _cod->getNum()) {
        Msg::Error("Given tensor does not belong to the codomain of this manifold map.");
        return false;
      }

      if(_dom->getNum() == _cod->getNum()) {
        y = TensorType2(_dom, x.getPoint(),
                        x.getPointerToCoeffs(), MANIFOLD);
        return true;
      }

      /*const Point* px = x.getPoint();
      const Point* py = y.getPoint();
      if(py == NULL)
        if(!mapPointToManifold(_cod, _dom, px, py) ) return false;

      int o1 = _cod->getOrientation(px->getMeshElement());
      int o2 = _dom->getOrientation(py->getMeshElement());
      double o = o1*o2;

      double xmc[TensorTraits<TensorType1>::numModelCoeffs];
      x.getModelCoeffs(xmc);
      y = TensorType2(_dom, py, xmc, MODEL)*o;
      return true;*/


      if(!this->isCodToDomMapDiff(x.getTensorSpace()) && (k || l) )
        return false;

      double xmcoeffs[TensorTraits<TensorType1>::numModelCoeffs];
      if(!x.getModelCoeffs(xmcoeffs)) return false;
      y = TensorType2(_dom, y.getPoint(), xmcoeffs, MODEL);
      return true;
    }

    /// Get tensor of a tensor field at given point on the codomain
    template <class TensorType1, class TensorType2>
    bool transformFieldToCodAtPoint(const Field<TensorType1>& f,
                                    const Point* p, TensorType2& y)
    {
      y = TensorType2(_cod, p);
      const Point* domp = this->mapPointToDom(p);
      if(domp == NULL) return false;
      TensorType1 x = f(domp);
      if(!this->transformTensorToCod(x,y)) return false;
      if(domp != p) delete domp;
      return true;
    }

    /// Get tensor of a tensor field at given point on the domain
    template <class TensorType1, class TensorType2>
    bool transformFieldToDomAtPoint(const Field<TensorType1>& f,
                                    const Point* p, TensorType2& y)
    {
      y = TensorType2(_dom, p);
      const Point* codp = this->mapPointToDom(p);
      if(codp == NULL) return false;
      TensorType1 x = f(codp);
      if(!this->transformTensorToDom(x,y)) return false;
      if(codp != p) delete codp;
      return true;
    }

    /// Transform a tensor field to domain or codomain manifold
    // (O(n) or O(n*log(n)), see above)
    template <class TensorType1, class TensorType2>
    bool transformFieldToCod(const Field<TensorType1>& f,
                             MeshField<TensorType2>& g)
    {
      double t1 = Cpu();
      if(f.getManifold()->getNum() != _dom->getNum()) {
        Msg::Error("Given tensor field does not belong to the domain of this manifold map.");
        return false;
      }
      TensorSpace ts = TensorTraits<TensorType1>::space;
      int k = TensorTraits<TensorType1>::ctrIndex;
      int l = TensorTraits<TensorType1>::coIndex;
      if(!this->isDomToCodMapDiff(ts) && (k || l) ) return false;

      Msg::Debug("Transforming tensor field ...");
      for(MElemIt it = this->getCodomain()->firstMeshElement();
          it != this->getCodomain()->lastMeshElement(); it++) {
        MElement* me = it->first;
        for(int iV = 0; iV < me->getNumVertices(); iV++) {
          Point* codp = new Point(_cod->getModelMap()->getModel(),
                                  me, iV);
          const Point* domp = this->mapPointToDom(codp,false);
          if(domp != NULL) {
            TensorType1 x = f(domp);
            TensorType2 y(_cod, codp);
            this->transformTensorToCod(x,y);
            g.assign(y);
            if(domp != codp) delete domp;
          }
          delete codp;
        }
      }

      double t2 = Cpu();
      Msg::Debug("Tensor field transformed (%g s)", t2-t1);
      return true;
    }

    template <class TensorType1, class TensorType2>
    bool transformFieldToDom(const Field<TensorType1>& f,
                             MeshField<TensorType2>& g)
    {
      double t1 = Cpu();
      if(f.getManifold()->getNum() != _cod->getNum()) {
        Msg::Error("Given tensor field does not belong to the codomain of this manifold map.");
        return false;
      }
      TensorSpace ts = TensorTraits<TensorType1>::space;
      int k = TensorTraits<TensorType1>::ctrIndex;
      int l = TensorTraits<TensorType1>::coIndex;
      if(!this->isCodToDomMapDiff(ts) && (k || l) ) return false;

      Msg::Debug("Transforming tensor field ...");
      for(MElemIt it = this->getDomain()->firstMeshElement();
          it != this->getDomain()->lastMeshElement(); it++) {
        MElement* me = it->first;
        for(int iV = 0; iV < me->getNumVertices(); iV++) {
          Point* domp = new Point(_dom->getModelMap()->getModel(),
                                  me, iV);
          const Point* codp = this->mapPointToCod(domp,false);
          if(codp != NULL) {
            TensorType1 x = f(codp);
            TensorType2 y(_dom, domp);
            this->transformTensorToDom(x,y);
            g.assign(y);
            if(domp != codp) delete codp;
          }
          delete domp;
        }
      }
      double t2 = Cpu();
      Msg::Debug("Tensor field transformed (%g s)", t2-t1);
      return true;
    }
  };

}

#endif
