// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#ifndef _RM_MULTI_INDEX_H_
#define _RM_MULTI_INDEX_H_

#include "RiemannDefines.h"
#include "VectorSpace.h"

namespace rm
{

  int nCk(int n, int k);

  class MultiIndex : public EquivalenceCat<MultiIndex>
  {
  protected:
    std::vector<unsigned int> _indices;
    int _sstindex;

  public:

    // Empty indices
    MultiIndex() : _sstindex(0) {}

    // Construct from positive integers
    MultiIndex(std::vector<unsigned int>& indices, int sstindex=-1) :
      _indices(indices), _sstindex(sstindex) {}

    // Construct from skew-symmetric tensor index
    MultiIndex(int ssti, int n, int k);

    // Construct from symmetric tensor index
    MultiIndex(int sti, int n);

    // Construct from general tensor index
    MultiIndex(int gti, int n, int k, int l);

    // Concatenate two multi-indices
    MultiIndex(const MultiIndex& i1, const MultiIndex& i2);

    bool nextSkewSymmetricTensorIndex(int n, int k, MultiIndex& I) const;
    void split(int k, MultiIndex& I1, MultiIndex& I2);

    int getNumSkewSymmetricTensorIndices(int n, int k) const
    {
      return nCk(n,k);
    }

    void getIndices(std::vector<unsigned int>& indices) const
    {
      indices = _indices;
    }
    bool uniqueIndices() const {
      std::set<unsigned int> ind;
      for(unsigned int i = 0; i < _indices.size(); i++)
        ind.insert(_indices[i]);
      if(ind.size() == _indices.size()) return true;
      return false;
    }
    void getSortedIndices(std::vector<unsigned int>& indices, int& perm) const
    {
      perm = 1;
      indices = _indices;
      while(std::prev_permutation(indices.begin(), indices.end()))
        perm *= -1;
    }
    void getSortedIndices(std::vector<unsigned int>& indices) const
    {
      indices = _indices;
      std::sort(indices.begin(), indices.end());
    }
    void getSortedUniqueIndices(std::vector<unsigned int>& indices) const
    {
      indices = _indices;
      std::sort(indices.begin(), indices.end());
      std::unique(indices.begin(), indices.end());
    }
    unsigned int maxIndex() const {
      unsigned int max = 0;
      for(unsigned int i = 0; i < _indices.size(); i++) {
        if(_indices[i] > max) max = _indices[i];
      }
      return max;
    }
    unsigned int operator() (unsigned int i) const { return _indices[i]; }
    unsigned int& operator() (unsigned int i) { return _indices[i]; }
    unsigned int getSize() const { return _indices.size(); }
    void print() const;

    bool isEquivalentTo(const MultiIndex& mj) const
    {
      std::vector<unsigned int> j;
      mj.getIndices(j);
      if(j.size() != _indices.size()) return false;
      for(unsigned int i = 0; i < j.size(); i++)
        if(j[i] != _indices[i]) return false;
      return true;
    }

    // Get skew-symmetric tensor index on n-manifold,
    // perm equals -1 if the coefficient is "skewed"
    // returns -1 if the coefficient is zero
    virtual int skewSymmetricTensorIndex(unsigned int n, int& perm) const;

    // Get symmetric rank 2 tensor index on n-manifold, TBD
    virtual int symmR2TensorIndex(unsigned int n) const;

    // Get full tensor index on n-manifold, TBD
    virtual int fullTensorIndex(unsigned int n) const;

  };

  // Kronecker delta of two multi-indices:
  //   returns  1 if I is *even* permutation of J
  //   returns -1 if I is *odd* permutation of J
  //   return   0 if I is *not* a permutation of J
  int kroneckerDelta(const MultiIndex& I, const MultiIndex& J);

  // permutation symbol of multi-index I:
  //   returns  1 if I is *even* permutation of 0,1,2,...,n
  //   returns -1 if I is *odd* permutation of 0,1,2,...,n
  //   return   0 if I is *not* a permutation of 0,1,2,...,n
  int permutationSymbol(const MultiIndex& I);

}

#endif
