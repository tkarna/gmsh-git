// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#include "ManifoldMap.h"

namespace rm
{

  Point* mapToUVW(GModel* gm, SPoint3& sp, MElement* me)
  {
    double uvw[3];
    double xyz[3];
    for(int iC = 0; iC < 3; iC++) xyz[iC] = sp[iC];
    me->xyz2uvw(xyz, uvw);
    return new Point(gm, me, uvw[0], uvw[1], uvw[2]);
  }

  bool mapPointToManifold(const ManifoldN* dom, const ManifoldN* cod,
                          const Point* p, const Point*& mp)
  {
    GModel* gm = cod->getModel();

    mp = NULL;
    if((dom != NULL && gm == dom->getModel() && dom->getTag() == cod->getTag())
       || cod->containsPoint(p)) {
      mp = new Point(*p);
      return true;
    }

    MElement* firstFound = NULL;
    SPoint3 sp = p->getSPoint3();
    MElement* ep = p->getMeshElement();
    int pVertexIndex = p->getVertex();
    std::vector<MElement*> elems = gm->getMeshElementsByCoord(sp);
    for(unsigned int iE = 0; iE < elems.size(); iE++) {
      MElement* me = elems[iE];
      if(cod->containsMeshElement(me)) {
	firstFound = me;
        if(dom != NULL && gm != dom->getModel()) {
          mp = mapToUVW(gm, sp, me);
          return true;
        }
        MElement* hi = ep;
        MElement* lo = me;
        bool hi2lo = true;
        if(me->getDim() > ep->getDim()) {
          hi = me;
          lo = ep;
          hi2lo = false;
        }
        if(pVertexIndex == -1) {
          if(meshElementInTrace(hi, lo)) {
            double uvw[3];
            double xyz[3];
            for(int iC = 0; iC < 3; iC++) xyz[iC] = sp[iC];
            me->xyz2uvw(xyz, uvw);
            mp = new Point(gm, me, uvw[0], uvw[1], uvw[2]);
            return true;
          }
        }
        else {
          std::map<int, int> vertexRel;
          if(meshElementInTrace(hi, lo, vertexRel, hi2lo)) {
            mp = new Point(gm, me, vertexRel[pVertexIndex]);
            return true;
          }
        }
      }
    }
    if(firstFound != NULL) {
      mp = mapToUVW(gm, sp, firstFound);
      return true;
    }
    return false;
  }
}
