// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#ifndef _RM_RIEMANN_H_
#define _RM_RIEMANN_H_

#include "Gmsh.h"
#include "GModel.h"
#include "OpenFile.h"
#include "ModelMap.h"
#include "Manifold.h"
#include "ManifoldIntegrationPoints.h"
#include "ManifoldPartOfUnity.h"
#include "Tensor.h"
#include "ATensor.h"
#include "Point.h"
#include "RiemannNumeric.h"
#include "Field.h"
#include "ManifoldMap.h"
#include "RiemannianManifold.h"
#include "TensorFunctions.h"
#include "FieldFunctions.h"
#include "DifferentialOperators.h"
#include "ChristoffelSymbols.h"
#include "BasisForm.h"
#include "Utils.h"
#include "SymbolicField.h"
#include "ManifoldCurve.h"
#include <string>
#if defined(HAVE_GINAC)
#include <ginac/ginac.h>
#endif

namespace rm
{

  /// Get all Gmsh model physical groups as a list of their Gmsh entities and
  /// their orientations
  void getPhysicalGroups(GModel* gm, std::map<int,
                         std::pair< std::vector<GEntity*>,
                         std::vector<int> > > groups[4]);

  /// create a map from  mesh elements of Gmsh entities to their orientations
  void processMeshElements(std::vector<GEntity*>& ents,
                           std::vector<int>& entsOr,
                           std::map<MElement*, int, MElemLessThanNum>& elems);
  void processMeshElements(GEntity* ent,
                           std::set<MElement*, MElemLessThanNum>& elems);

  /// Is mesh element "lo" on boundary of mesh element "hi"
  bool meshElementInTrace(MElement* hi, MElement* lo);

  /// Also obtain relation between vertex indices (with given direction)
  bool meshElementInTrace(MElement* hi, MElement* lo,
                          std::map<int, int>& vertexRelation, bool hi2lo);

  /// Does given mesh elements touch at least at one vertex
  bool meshElementsTouch(MElement* e1, MElement* e2);

  int nCk(int n, int k);
  int numCoeffs(int n, int k, int l, TensorSymmetry s, Representation r);

#if defined(HAVE_GINAC)

  // Static symbolic expression symbol storage
  const GiNaC::symbol& get_symbol(const std::string& s);

  // Evaluate compiled symbolic expression at x, where x consists nx values
  double evalExpression(const double* x, const int nx,
                        const GiNaC::FUNCP_CUBA f);

  // compile symbolic expression to C function
  bool compileExpressions(const GiNaC::lst& expressions,
                          const std::vector<GiNaC::symbol>& symbols,
                          std::vector<GiNaC::FUNCP_CUBA>& funcptrs);
#endif

}

#endif
