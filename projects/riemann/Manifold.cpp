// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#include "Manifold.h"

namespace rm
{

  GModel* ManifoldN::createModel(std::vector<const ManifoldN*>& manifolds,
                                 Coordinates coords,
                                 bool discontinousChart)
  {
    std::map<int, std::vector<MElement*> > elements;
    std::map<int, std::vector<int> > physicals;

    MElementFactory factory;
    std::map<int, MVertex*> visitedVertices;

    for(unsigned int iM = 0; iM < manifolds.size(); iM++) {
      const ManifoldN* man = manifolds[iM];
      int dim = man->getDim();
      if(dim > 3) {
        Msg::Error("Cannot create models of manifolds with dimension greater than 3.");
        continue;
      }

      for(MElemIt it = man->firstMeshElement();
          it != man->lastMeshElement(); it++) {
        MElement* me = it->first;
        int reg = it.getCurrentEntityTag();
        std::vector<MVertex*> meVertices;
        for(int iV = 0; iV < me->getNumVertices(); iV++) {
          Point* p = man->getPoint(me, iV);
          MVertex* newver;
          if(coords == MANIFOLDCOORDS) {
            double c[MAXDIM];
            if(dim == 0) {
              const Manifold<0>* man2 = (const Manifold<0>*)man;
              man2->getModelMap()->mapPoint(p, c);
              newver = new MVertex(0, 0, 0);
            }
            if(dim == 1) {
              const Manifold<1>* man2 = (const Manifold<1>*)man;
              man2->getModelMap()->mapPoint(p, c);
              newver = new MVertex(c[0], 0, 0);
            }
            if(dim == 2) {
              const Manifold<2>* man2 = (const Manifold<2>*)man;
              man2->getModelMap()->mapPoint(p, c);
              newver = new MVertex(c[0], c[1], 0);
            }
            if(dim == 3) {
              const Manifold<3>* man2 = (const Manifold<3>*)man;
              man2->getModelMap()->mapPoint(p, c);
              newver = new MVertex(c[0], c[1], c[2]);
            }
          }
          else {
            double x,y,z;
            p->getXYZ(x,y,z);
            newver = new MVertex(x, y, z);
          }
          delete p;

          if(discontinousChart) meVertices.push_back(newver);
          else {
            int vnum = me->getVertex(iV)->getNum();
            if(visitedVertices.count(vnum)) {
              meVertices.push_back(visitedVertices[vnum]);
              delete newver;
            }
            else {
              meVertices.push_back(newver);
              visitedVertices[vnum] = newver;
            }
          }
        }

        MElement* newme = factory.create(me->getTypeForMSH(), meVertices,
                                         me->getNum(), me->getPartition());
        elements[reg].push_back(newme);
        physicals[reg] = std::vector<int>(1, man->getTag());
      }
    }

    GModel* gm = GModel::createGModel(elements, physicals);
    return gm;
  }


#if defined(HAVE_AVSOLVER)


  GModel* ManifoldN::createModel(std::vector<const ManifoldN*>& manifolds,
				 Coordinates coords,
				 bool discontinousChart,
				 void* user_data) {

    std::map<int, std::vector<MElement*> > elements;
    std::map<int, std::vector<int> > physicals;
    MElementFactory factory;
    std::map<int, MVertex*> visitedVertices;
    

    for(unsigned int iM = 0; iM < manifolds.size(); iM++) {
      const ManifoldN* man = manifolds[iM];
      int dim = man->getDim();

      /// additional parameter setting and dimension checking
      std::vector<double>* steptimes = (std::vector<double>*)user_data;
      if (dim==0) {
	const Manifold<0>* man2 = (const Manifold<0>*)man;
	man2->getModelMap()->setParameter(steptimes->at(iM));
      }
      else if (dim==1) {
	const Manifold<1>* man2 = (const Manifold<1>*)man;
	man2->getModelMap()->setParameter(steptimes->at(iM));
      }
      else if (dim==2) {
	const Manifold<2>* man2 = (const Manifold<2>*)man;
	man2->getModelMap()->setParameter(steptimes->at(iM));
      }
      else if (dim==3) {
	const Manifold<3>* man2 = (const Manifold<3>*)man;
	man2->getModelMap()->setParameter(steptimes->at(iM));
      }
      else {
	Msg::Error("Cannot create models of manifolds with dimension %i.", dim);
      }

      
      for(MElemIt it = man->firstMeshElement();
          it != man->lastMeshElement(); it++) {
        MElement* me = it->first;
        int reg = it.getCurrentEntityTag();
        std::vector<MVertex*> meVertices;
        for(int iV = 0; iV < me->getNumVertices(); iV++) {
          Point* p = man->getPoint(me, iV);
          MVertex* newver;
          if(coords == MANIFOLDCOORDS) {
            double c[MAXDIM];
            if(dim == 0) {
	      const Manifold<0>* man2 = (const Manifold<0>*)man;
	      man2->getModelMap()->mapPoint(p, c);
	      newver = new MVertex(0, 0, 0);
	    }
            if(dim == 1) {
	      const Manifold<1>* man2 = (const Manifold<1>*)man;
	      man2->getModelMap()->mapPoint(p, c);
	      newver = new MVertex(c[0], 0, 0);
	    }
            if(dim == 2) {
	      const Manifold<2>* man2 = (const Manifold<2>*)man;
	      man2->getModelMap()->mapPoint(p, c);
	      newver = new MVertex(c[0], c[1], 0);
	    }
            if(dim == 3) {
	      const Manifold<3>* man2 = (const Manifold<3>*)man;
	      man2->getModelMap()->mapPoint(p, c);
	      newver = new MVertex(c[0], c[1], c[2]);
	    }
          }
          else {
            double x,y,z;
            p->getXYZ(x,y,z);
            newver = new MVertex(x, y, z);
          }
          delete p;
	  
	  int vnum = me->getVertex(iV)->getNum();
	  if(visitedVertices.count(vnum)) {
	    meVertices.push_back(visitedVertices[vnum]);
	    delete newver;
	  }
	  else {
	    meVertices.push_back(newver);
	    visitedVertices[vnum] = newver;
	  }
	}
	
        MElement* newme = factory.create(me->getTypeForMSH(), meVertices,
                                         me->getNum(), me->getPartition());
        elements[reg].push_back(newme);
        physicals[reg] = std::vector<int>(1, man->getTag());
      }
    }

    GModel* gm = GModel::createGModel(elements, physicals);
    return gm;
  }
#endif

}
