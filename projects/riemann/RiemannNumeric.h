// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#ifndef _RM_RIEMANN_NUMERIC_H_
#define _RM_RIEMANN_NUMERIC_H_

#include <complex>
#include "Numeric.h"
#include "RiemannDefines.h"
#include "MultiIndex.h"

namespace rm {

  // Routines for small vectors and matrices

  template <int n, int m>
  int matIndex(int i, int j)
  {
    return i*m+j;
  }

  template <int n>
  int symIndex(int i, int j);

  template <int n>
  int mChrisIndex(int i, int j, int k)
  {
    return (k*SYMR2(n)+symIndex<n>(i,j));
  }

  template <int n>
    void printarr(const double* a/*[n]*/) {
    printf("Printing array: ");
    for(int i = 0; i < n; i++) printf("%12.5E ", a[i]);
    printf("\n");
  }
  template <int n, int m>
    void printarr(const double a[n*m]) {
    printf("Printing array: \n");
    for(int i = 0; i < n; i++) {
      for(int j = 0; j < m; j++) printf("%12.5E ", a[i*m+j]);
      printf("\n");
    }
    printf("\n");
  }

  // a[i] <- 0
  template <int n>
  inline void vecinit(double* a/*[n]*/)
  {
    for(int i = 0; i < n; i++) a[i] = 0.;
  }
  // dest[i] <- src[i]
  template <int n>
  inline void vecinit(const double* src/*[n]*/, double* dest/*[n]*/)
  {
    for(int i = 0; i < n; i++) dest[i] = src[i];
  }

  // c <- a + b
  template <int n>
  void sum(const double* a/*[n]*/, const double* b/*[n]*/, double* c/*[n]*/)
  {
    for(int i = 0; i < n; i++) c[i] = a[i]+b[i];
  }

  // c <- a*x
  template <int n>
  void smult(const double a, const double* x/*[n]*/, double* c/*[n]*/)
  {
    for(int i = 0; i < n; i++) c[i] = a*x[i];
  }

  // c <- a*x + y
  template <int n>
  void axpy(const double a, const double* x/*[n]*/, const double* y/*[n]*/,
            double* c/*[n]*/)
  {
    for(int i = 0; i < n; i++) c[i] = a*x[i]+y[i];
  }

  // a^T*b
  template <int n>
  double dot(const double* a/*[n]*/, const double* b/*[n]*/)
  {
    double res = 0.;
    for(int i = 0; i < n; i++) res += a[i]*b[i];
    return res;
  }
  // res <- mat*vec
  template <int n, int m>
  void matvec(const double* mat/*[n*m]*/, const double* vec/*[m]*/,
              double* res/*[n]*/)
  {
    for(int i = 0; i < n; i++) {
      res[i] = 0.;
      for(int j = 0; j < m; j++) res[i] += mat[i*m+j]*vec[j];
    }
  }
  /*template <>
  void matvec<1,1>(const double* mat, const double* vec, double* res);
  template <>
  void matvec<2,2>(const double* mat, const double* vec, double* res);
  template <>
  void matvec<3,2>(const double* mat, const double* vec, double* res);
  template <>
  void matvec<2,3>(const double* mat, const double* vec, double* res);
  template <>
  void matvec<3,3>(const double* mat, const double* vec, double* res);*/

  // c <- a*b
  template <int n, int m, int l>
  void matmat(const double* a/*[n*m]*/, const double* b/*[m*l]*/,
              double* c/*[n*l]*/)
  {
    for(int i = 0; i < n; i++) {
      for(int j = 0; j < l; j++) {
	c[i*l+j] = 0.;
	for(int k = 0; k < m; k++) c[i*l+j] += a[i*m+k]*b[k*l+j];
      }
    }
  }
  /*template <>
  void matmat<2, 3, 2>(const double* a, const double* b, double* c);
  template <>
  void matmat<2, 3, 3>(const double* a, const double* b, double* c);
  template <>
  void matmat<3, 3, 2>(const double* a, const double* b, double* c);
  template <>
  void matmat<3, 3, 3>(const double* a, const double* b, double* c);*/

  template <int n>
  void matsmats(const double a[SYMR2(n)], const double b[SYMR2(n)],
                double c[SYMR2(n)])
  {
    for(int i = 0; i < n; i++) {
      for(int j = i; j < n; j++) {
	c[symIndex<n>(i,j)] = 0.;
	for(int k = 0; k < n; k++)
          c[symIndex<n>(i,j)] += a[symIndex<n>(i,k)]*b[symIndex<n>(k,j)];
      }
    }
  }

  // det(mat)
  template <int n, int m>
  double det(const double* mat)
  {
    printf("TBD");
    return 0;
  }

  template <>
  double det<0,0>(const double* mat);
  template <>
  double det<1,1>(const double* mat);
  template <>
  double det<2,2>(const double* mat);
  template <>
  double det<3,3>(const double* mat);

  // cof <- (i,j)-cofactor of mat
  template <int n, int m>
  void cofactor(const int i, const int j,
                const double* mat/*[n*m]*/, double* cof/*[(n-1)*(m-1)]*/)
  {
    for(int I = 0; I < n; I++)
      for(int J = 0; J < m; J++)
        if(J != j && I != i)
          cof[(I < i ? I : I - 1)*(m-1)+(J < j ? J : J - 1)] = mat[I*m+J];
  }

  // adjt <- adj(mat)^T
  template <int n, int m>
  void adjt(const double* mat/*[n*m]*/, double* adjt/*[n*m]*/)
  {
    double cof[MAYBE((n-1)*(m-1))];
    for(int i = 0; i < n; i++){
      for(int j = 0; j < m; j++){
        cofactor<n,m>(i, j, mat, cof);
        double detc = det<n-1, m-1>(cof)*pow(-1, i+j);
        adjt[i*m+j] = detc; // no transpose
      }
    }
  }

  // adj <- (1st column of mat) cross (2nd column of mat)
  template <int n, int m, int k>
  void adjdegen(const double* mat/* [3*2] */,
                double* adj/* [3*1] */)
  {
    adj[0] = mat[2]*mat[5] - mat[4]*mat[3];
    adj[1] = mat[4]*mat[1] - mat[0]*mat[5];
    adj[2] = mat[0]*mat[3] - mat[2]*mat[1];
  }

  // adjt <- (1st row of mat) cross (2nd row of mat) transposed
  template <int n, int m, int k>
  void adjtdegen(const double* mat/* [2*3] */,
                 double* adjt/* [1*3] */)
  {
    adjt[0] = mat[1]*mat[5] - mat[2]*mat[4];
    adjt[1] = mat[2]*mat[3] - mat[0]*mat[5];
    adjt[2] = mat[0]*mat[4] - mat[1]*mat[3];
  }

  // determinant of a minor of a (n,m)-matrix,
  // minor rows and columns are given by multi-indices I and J, resprectively
  template <int n, int m>
  double det(const MultiIndex& I, const MultiIndex& J,
             const double* a/*[n*m]*/)
  {
    if(I.getSize() != J.getSize()) return 0;
    fullMatrix<double> ma(I.getSize(), I.getSize());

    for(unsigned int i = 0; i < I.getSize(); i++)
      for(unsigned int j = 0; j < J.getSize(); j++)
        ma(i,j) = a[I(i)*m+J(j)];

    return ma.determinant();
  }

  // about 2 times slower but works for any n,m,k
  template<int n, int m, int k>
  void diffv2(const double* dmat1/*[MAYBE(n*m)]*/,
              const double* v/*[(NCK(m,k))]*/,
              double* res/*[(NCK(n,k))]*/)
  {
    if(k == 0) {
      res[0] = v[0];
      return;
    }
    for(int i = 0; i < (NCK(n,k)); i++) {
      res[i] = 0;
      MultiIndex I(i, n, k);
      for(int j = 0; j < (NCK(m,k)); j++) {
        MultiIndex J(j, m, k);
        res[i] += v[j]*det<n,m>(I, J, dmat1);
      }
    }
  }

  // Transform skewsymmetric k-vector v from m-manifold to n-manifold
  // given the (n,m)-Jacobian matrix of the mapping
  template<int n, int m, int k>
  void diffv(const double* dmat1/*[n*m]*/, const double* v/*[(NCK(m,k))]*/,
             double* res/*[(NCK(n,k))]*/)
  {
    if(k > n) return;
    diffv2<n,m,k>(dmat1, v, res);
  }

  template <>
  void diffv<0, 0, 0>(const double* dmat1, const double* v,
                      double* res);
  template <>
  void diffv<1, 1, 0>(const double* dmat1, const double* v,
                      double* res);
  template <>
  void diffv<2, 2, 0>(const double* dmat1, const double* v,
                      double* res);
  template <>
  void diffv<0, 3, 0>(const double* dmat1, const double* v,
                      double* res);
  template <>
  void diffv<0, 3, 1>(const double* dmat1, const double* v,
                      double* res);
  template <>
  void diffv<0, 0, 2>(const double* dmat1, const double* v,
                      double* res);
  template <>
  void diffv<2, 1, 0>(const double* dmat1, const double* v,
                      double* res);
  template <>
  void diffv<1, 2, 0>(const double* dmat1, const double* v,
                      double* res);
  template <>
  void diffv<3, 1, 0>(const double* dmat1, const double* v,
                      double* res);
  template <>
  void diffv<1, 3, 0>(const double* dmat1, const double* v,
                      double* res);
  template <>
  void diffv<3, 2, 0>(const double* dmat1, const double* v,
                      double* res);
  template <>
  void diffv<2, 3, 0>(const double* dmat1, const double* v,
                      double* res);
  template <>
  void diffv<1, 0, 1>(const double* dmat1, const double* v,
                      double* res);
  template <>
  void diffv<1, 1, 1>(const double* dmat1, const double* v,
                      double* res);
  template <>
  void diffv<1, 1, 2>(const double* dmat1, const double* v,
                      double* res);
  template <>
  void diffv<2, 0, 1>(const double* dmat1, const double* v,
                      double* res);
  template <>
  void diffv<1, 2, 1>(const double* dmat1, const double* v,
                      double* res);
  template <>
  void diffv<2, 1, 1>(const double* dmat1, const double* v,
                      double* res);
  template <>
  void diffv<2, 2, 1>(const double* dmat1, const double* v,
                      double* res);
  template <>
  void diffv<2, 2, 2>(const double* dmat1, const double* v,
                      double* res);
  template <>
  void diffv<3, 0, 1>(const double* dmat1, const double* v,
                      double* res);
  template <>
  void diffv<1, 3, 1>(const double* dmat1, const double* v,
                      double* res);
  template <>
  void diffv<2, 3, 1>(const double* dmat1, const double* v,
                      double* res);
  template <>
  void diffv<3, 3, 0>(const double* dmat1, const double* v,
                      double* res);
  template <>
  void diffv<3, 3, 1>(const double* dmat1, const double* v,
                      double* res);
  template <>
  void diffv<3, 3, 2>(const double* dmat1, const double* v,
                      double* res);
  template <>
  void diffv<3, 3, 3>(const double* dmat1, const double* v,
                      double* res);
  template <>
  void diffv<2, 3, 2>(const double* dmat1, const double* v,
                      double* res);
  template <>
  void diffv<3, 2, 2>(const double* dmat1, const double* v,
                      double* res);
  template <>
  void diffv<3, 1, 1>(const double* dmat1, const double* v,
                      double* res);
  template <>
  void diffv<3, 2, 1>(const double* dmat1, const double* v,
                      double* res);

  // Identity matrix in symmetric format
  template <int n>
  void ids(double* s);

  // Convert full matrix to symmetric matrix format
  template <int n>
  void full2sym(const double* f, double* s);

  // Convert symmetric matrix to full matrix format
  template <int n>
  void sym2full(const double* s, double* f);

  // at <- a^T
  template <int n, int m>
  void transpose(const double* a/*[n*m]*/, double* at/*[m*n]*/)
  {
    for(int i = 0; i < n; i++)
      for(int j = 0; j < m; j++)
	at[j*n+i] = a[i*m+j];
  }

  // Transform symmetric (0,2)-tensor s from m-manifold to n-manifold
  template<int n, int m>
  void diffs(const double* dmat1/*[n*m]*/,
             const double* s/*[SYMR2(m)]*/, double* res/*[SYMR2(n)]*/)
  {
    // optimize this
    double sf[MAYBE(m*m)];
    sym2full<m>(s, sf);
    double js[MAYBE(n*m)];
    matmat<n,m,m>(dmat1, sf, js);
    double jact[MAYBE(m*n)];
    transpose<n,m>(dmat1, jact);
    double jsjt[MAYBE(n*n)];
    matmat<n,m,n>(js, jact, jsjt);
    full2sym<n>(jsjt, res);
  }
  template<>
  void diffs<1,1>(const double* dmat1,
                  const double* s, double* res);

  // Transform linear transformation t of 1-vectors
  // from m-manifold to n-manifold
  template<int n, int m>
  void difflt(const double* dmat1/*[n*m]*/, const double* invdmat1/*[m*n]*/,
              const double* t/*[m*m]*/, double* res/*[n*n]*/)
  {
    double temp[MAYBE(m*n)];
    matmat<m,m,n>(t, invdmat1, temp);
    matmat<n,m,n>(dmat1, temp, res);
  }

  // Determinant of a symmetric matrix
  template <int n>
  double dets(const double* smat/*[SYMR2(n)]*/);

  // inv <- smat^(-1)
  template <int n>
  double invs(const double* smat/*[SYMR2(n)]*/, double* inv/*[SYMR2(n)]*/);

  // inv <- mat^(-1)
  template <int n>
  double inv(const double* mat, double* inv);

  // Pseudo-inverse of a matrix
  template <int n, int m>
  double pinv(const double* mat, double* pinv)
  {
    if(n==m || n == 0 || m == 0) return inv<n>(mat, pinv);
    double d = 0.;
    double at[MAYBE(m*n)];
    transpose<n,m>(mat, at);
    if(n < m) { // pinv = A^T*(A*A^T)^(-1), A has linearly independent rows
      double aat[MAYBE(n*n)];
      double invaat[MAYBE(n*n)];
      matmat<n,m,n>(mat, at, aat);
      d = inv<n>(aat, invaat);
      matmat<m,n,n>(at, invaat, pinv);
    }
    else { // pinv = (A^T*A)^(-1)*A^T, A has linearly independent columns
      double ata[MAYBE(m*m)];
      double invata[MAYBE(m*m)];
      matmat<m,n,m>(at, mat, ata);
      d = inv<m>(ata, invata);
      matmat<m,m,n>(invata, at, pinv);
    }
    return d;
  }

  // FIXME: does not work
  template <int n, int k1, int k2>
  void wedge3(const double a[(NCK(n,k1))], const double b[(NCK(n,k2))],
              double c[(NCK(n,k1+k2))])
  {
    for(int i = 0; i < (NCK(n,k1+k2)); i++) {
      c[i] = 0;
      MultiIndex I(0, n,k1+k2);
      do {
        int perm = permutationSymbol(I);
        MultiIndex Ia;
        MultiIndex Ib;
        I.split(k1, Ia, Ib);
        int perma = 1;
        int permb = 1;
        int ia = Ia.skewSymmetricTensorIndex(n, perma);
        int ib = Ib.skewSymmetricTensorIndex(n, permb);
        c[i] += perm*a[ia]*perma*b[ib]*permb;
      }
      while(I.nextSkewSymmetricTensorIndex(n, k1+k2, I));
    }
  }

  // increasely slower as n grows but works for any n,k1,k2
  // (at n = 3, about 10 times slower)
  template <int n, int k1, int k2>
  void wedge2(const double a[(NCK(n,k1))], const double b[(NCK(n,k2))],
              double c[(NCK(n,k1+k2))])
  {
    if(k1 == 0 && k2 == 0) {
      c[0] = a[0]*b[0];
      return;
    }
    for(int i = 0; i < (NCK(n,k1+k2)); i++) {
      c[i] = 0;
      MultiIndex I(i, n, k1+k2);
      for(int j = 0; j < (NCK(n,k1)); j++) {
        MultiIndex J(j, n, k1);
        for(int k = 0; k < (NCK(n,k2)); k++) {
          MultiIndex K(k, n, k2);
          MultiIndex JK(J, K);
          c[i] += kroneckerDelta(JK, I)*a[j]*b[k];
        }
      }
    }
  }

  template <int n, int k1, int k2>
  void wedge0a(const double* a, const double* b, double* c)
  {
    for(int i = 0; i < (NCK(n,k1+k2)); i++) c[i] = a[0]*b[i];
  }
  template <int n, int k1, int k2>
  void wedge0b(const double* a, const double* b, double* c)
  {
    for(int i = 0; i < (NCK(n,k1+k2)); i++) c[i] = a[i]*b[0];
  }
  template <int n, int k1, int k2>
  void wedge21(const double* a, const double* b, double* c)
  {
    c[0] = a[0]*b[1] - a[1]*b[0];
  }
  template <int n, int k1, int k2>
  void wedge31(const double* a, const double* b, double* c)
  {
    c[0] = a[1]*b[2] - a[2]*b[1];
    c[1] = a[2]*b[0] - a[0]*b[2];
    c[2] = a[0]*b[1] - a[1]*b[0];
  }
  template <int n, int k1, int k2>
  void wedge321(const double* a, const double* b, double* c)
  {
    c[0] = 0.;
    for(int i = 0; i < 3; i++) c[0] += a[i]*b[i];
  }


  // Wedge product of k1-(co)vector and k2-(co)vector on n-manifold
  template <int n, int k1, int k2>
  void wedge(const double a[(NCK(n,k1))], const double b[(NCK(n,k2))],
             double c[(NCK(n,k1+k2))])
  {
    if(k1 == 0) {
      for(int i = 0; i < (NCK(n,k1+k2)); i++) c[i] = a[0]*b[i];
      return;
    }
    else if(k2 == 0) {
      for(int i = 0; i < (NCK(n,k1+k2)); i++) c[i] = a[i]*b[0];
      return;
    }
    if(k1 == 1 && k2 == 1) {
      if(n == 2) {
        c[0] = a[0]*b[1] - a[1]*b[0];
        return;
      }
      else if(n == 3) {
        c[0] = a[1]*b[2] - a[2]*b[1];
        c[1] = a[2]*b[0] - a[0]*b[2];
        c[2] = a[0]*b[1] - a[1]*b[0];
        return;
      }
      else {
        wedge2<n,k1,k2>(a, b, c);
        return;
      }
    }
    else if(((k1 == 1 && k2 == 2) || (k1 == 2 && k2 == 1)) && n == 3) {
      c[0] = 0.;
      for(int i = 0; i < 3; i++) c[0] += a[i]*b[i];
      return;
    }
    else {
      wedge2<n,k1,k2>(a, b, c);
      return;
    }
  }

  // Inner product matrix for k-(co)vectors on n-manifold
  template <int n, int k>
  void inprodmat(const double s[SYMR2(n)], double snk[SYMR2((NCK(n,k)))])
  {
    if(k == 0) {
      snk[0] = 1.;
      return;
    }
    else if(k == 1) {
      vecinit<SYMR2(n)>(s, snk);
      return;
    }
    else if(n == k) {
      snk[0] = fabs(dets<n>(s));
      return;
    }
    else if(k == 2 && n == 3) {
      double sqrtdet = sqrt(fabs(dets<n>(s)));
      double ins[SYMR2(n)];
      invs<n>(s, ins);
      for(int i = 0; i < 6; i++) snk[i] = ins[i]*sqrtdet;
      return;
    }
    else {
      vecinit<SYMR2(n)>(snk);
      Msg::Error("Inner product of %d-(co)vectors on %d-dim manifold not implemented.", k, n);
    }
  }

  // Inner product of k-(co)vectors on n-manifold
  template <int n, int k>
  double inprod(const double a[(NCK(n,k))], const double b[(NCK(n,k))],
                const double s[SYMR2(n)])
  {
    double smat[SYMR2((NCK(n,k)))];
    inprodmat<n,k>(s, smat);

    double temp[(NCK(n,k))];
    double fulls[(NCK(n,k))*(NCK(n,k))];
    sym2full<(NCK(n,k))>(smat, fulls);
    matvec<(NCK(n,k)), (NCK(n,k))>(fulls, b, temp);
    return dot<(NCK(n,k))>(a, temp);
  }

  // Hodge matrix of k-(co)vector on n-manifold
  template<int n, int k>
  void hodgemat(const double s[SYMR2(n)], double h[(NCK(n,k))*(NCK(n,k))])
  {
    if(n <= 3) {
      if(k == 0) {
        h[0] = sqrt(1./fabs(dets<n>(s)));
        return;
      }
      else if(k == n) {
        h[0] = sqrt(fabs(dets<n>(s)));
        return;
      }
      else if(k == 1) {
        double c = sqrt(1./fabs(dets<n>(s)));
        if(n == 2) {
          h[0*2+0] = -s[1]*c;
          h[0*2+1] = -s[2]*c;
          h[1*2+0] =  s[0]*c;
          h[1*2+1] =  s[1]*c;
          return;
        }
        if(n == 3) {
          double sf[MAYBE(n*n)];
          sym2full<n>(s, sf);
          for(int i = 0; i < 3; i++)
            for(int j = 0; j < 3; j++)
              h[i*3+j] = sf[i*3+j]*c;
          return;
        }
      }
      else if(k == 2) {
        double c = sqrt(fabs(dets<n>(s)));
        double ins[SYMR2(n)];
        invs<n>(s, ins);
        double insf[MAYBE(n*n)];
        sym2full<n>(ins, insf);
        for(int i = 0; i < 3; i++)
          for(int j = 0; j < 3; j++)
            h[i*3+j] = insf[i*3+j]*c;
        return;
      }
    }
    else {
      vecinit<(NCK(n,k))*(NCK(n,k))>(h);
      Msg::Error("Hodge operator of %d-(co)vector on %d-dim manifold not implemented.");
    }
  }

  // Hodge of k-(co)vector on n-manifold
  template <int n, int k>
  void hodge(const double a[(NCK(n,k))], double b[(NCK(n,k))],
             const double s[SYMR2(n)])
  {
    double h[(NCK(n,k))*(NCK(n,k))];
    hodgemat<n, k>(s, h);
    matvec<(NCK(n,k)), (NCK(n,k))>(h, a, b);
  }

  // Tensor product of 1-(co)vectors
  template <int n, int m>
  void tprod11(const double a[n], const double b[m], double c[n*m])
  {
    for(int i = 0; i < n; i++)
      for(int j = 0; j < m; j++) c[i*m+j] = a[i]*b[j];
  }

  // FIXME: not tested
  template <int n, int k, int kv>
  void intprod2(const double a[(NCK(n,k))], const double b[(NCK(n,kv))],
                double c[(NCK(n,k-kv))])
  {
    for(int i = 0; i < (NCK(n,k-kv)); i++) {
      c[i] = 0;
      MultiIndex I(i, n, k-kv);
      for(int j = 0; j < (NCK(n,kv)); j++) {
        MultiIndex J(j, n, kv);
        MultiIndex JI(J, I);
        int perma = 1;
        const int JIind = JI.skewSymmetricTensorIndex(n, perma);
        double bj = b[j];
        double aji = a[JIind]*perma;
        c[i] += bj*aji;
      }
    }
  }

  template <int n, int k, int kv>
  void intprod(const double a[(NCK(n,k))], const double b[(NCK(n,kv))],
               double c[(NCK(n,k-kv))])
  {
    if (kv == 0) {
      for (int i = 0; i < (NCK(n,k)); i++) c[i] = a[i]*b[0];
      return;
    }
    else if(k == kv) {
      c[0] = dot<(NCK(n,k))>(a, b);
      return;
    }
    else if(n == 2 && k == 2 && kv == 1) {
      c[0] = -a[0]*b[1];
      c[1] =  a[0]*b[0];
      return;
    }
    else if(n == 3 && k == 3 && kv == 1) {
      for(int i = 0; i < 3; i++) c[i] = a[0]*b[i];
      return;
    }
    else if(n == 3 && k == 2 && kv == 1) {
      c[0] = a[1]*b[2] - a[2]*b[1];
      c[1] = a[2]*b[0] - a[0]*b[2];
      c[2] = a[0]*b[1] - a[1]*b[0];
      return;
    }
    else {
      intprod2<n,k,kv>(a,b,c);
      return;
    }
  }

  // Flat and sharp of k-vectors and k-covectors on n-manifold
  template <int n, int k>
  void musical(const double a[(NCK(n,k))], double b[(NCK(n,k))],
               double s[SYMR2(n)])
  {
    double smat[SYMR2((NCK(n,k)))];
    inprodmat<n,k>(s, smat);
    double fulls[(NCK(n,k))*(NCK(n,k))];
    sym2full<(NCK(n,k))>(smat, fulls);
    matvec<(NCK(n,k)), (NCK(n,k))>(fulls, a, b);
  }

}

#endif
