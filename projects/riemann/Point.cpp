// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#include "Point.h"
#include <cmath>

namespace rm
{
  void Point::getUVW(double& u, double &v, double& w) const
  {
    if(_veronly) {
      _me->getNode(_ver, _u,_v,_w);
      _veronly = false;
    }
    u = _u;
    v = _v;
    w = _w;
  }

  void Point::getXYZ(double& x, double &y, double& z) const
  {
    if(_xyz) {
      x = _x;
      y = _y;
      z = _z;
      return;
    }
    if(_ver != -1) {
      x = _me->getVertex(_ver)->x();
      y = _me->getVertex(_ver)->y();
      z = _me->getVertex(_ver)->z();
      return;
    }
    x = y = z = 0;
    double sf[1256];
    _me->getShapeFunctions(_u, _v, _w, sf);
    for (int j = 0; j < _me->getNumShapeFunctions(); j++) {
      const MVertex* v = _me->getShapeFunctionNode(j);
      x += sf[j] * v->x();
      y += sf[j] * v->y();
      z += sf[j] * v->z();
    }
    _xyz = true;
    _x = x;
    _y = y;
    _z = z;
  }

  double Point::getX() const
  {
    if(_xyz) return _x;
    this->getXYZ(_x,_y,_z);
    _xyz = true;
    return _x;
  }

  double Point::getY() const
  {
    if(_xyz) return _y;
    this->getXYZ(_x,_y,_z);
    _xyz = true;
    return _y;
  }

  double Point::getZ() const
  {
    if(_xyz) return _z;
    this->getXYZ(_x,_y,_z);
    _xyz = true;
    return _z;
  }

  SPoint3 Point::getSPoint3() const
  {
    if(_xyz) return SPoint3(_x,_y,_z);
    this->getXYZ(_x,_y,_z);
    _xyz = true;
    return SPoint3(_x,_y,_z);
  }

  void Point::print() const
  {
    double x, y, z;
    this->getXYZ(x,y,z);
    double u,v,w;
    this->getUVW(u,v,w);
    Msg::Info("Point on Gmsh model on %d-dim mesh element %d:",
                           _me->getDim(), _me->getNum());
    Msg::Info("  (u, v, w) = (%12.5E, %12.5E, %12.5E )", u, v, w);
    Msg::Info("  (x, y, z) = (%12.5E, %12.5E, %12.5E )", x, y, z);
    if(_ver != -1)
      Msg::Info("  At mesh vertex %d, mesh element vertex index %d",
                _me->getVertex(_ver)->getNum(), _ver);
  }
 
  bool Point::isEqual(const Point* p, double precision) const
  {
    if (_me->getNum() != p->getMeshElement()->getNum()) {
      return false;
    }
    
    double u,v,w;
    this->getUVW(u,v,w);

    double u1,v1,w1;
    p->getUVW(u1,v1,w1);
    
    if (std::fabs(u-u1) > precision) return false;
    if (std::fabs(v-v1) > precision) return false;
    if (std::fabs(w-w1) > precision) return false;
    return true;
  }

}
