// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#ifndef _RM_CHRISTOFFEL_SYMBOLS_H_
#define _RM_CHRISTOFFEL_SYMBOLS_H_

#include "RiemannianManifold.h"

namespace rm {

  template <int n, int k>
  MeshField<Covector<n,k+1> > d(const Field<Covector<n,k> >& f);

  template <int n>
  class ChristoffelSymbols
  {
  protected:
    const Manifold<n>* _m;
  public:
    ChristoffelSymbols(const Manifold<n>* m) : _m(m) {}

    const Manifold<n>* getManifold() const { return _m; }
    virtual Covector<n, 0> getChristoffelSymbol(const Point* p,
                                                int i, int j, int k)
    {
      return Covector<n,0>(this->getManifold(), p);
    }
    virtual int getNumCoeffs() const { return CHRIS(n); }
    virtual int getNumModelCoeffs() const { return CHRIS(3); }

    virtual ~ChristoffelSymbols() {}
  };

  template <int n>
  class MetricChristoffelSymbols : public ChristoffelSymbols<n>
  {
  protected:
    MeshField<Covector<n,0> > _gamma[(MCHRIS(n))];
    bool _precompute;

  public:
    MetricChristoffelSymbols(const RiemannianManifold<n>* m,
                             bool precompute=true) :
      ChristoffelSymbols<n>(m), _precompute(precompute)
    {
      if(precompute) {
        Msg::StatusBar(true, "Computing Christoffel symbols ...");
        double t1 = Cpu();
        for(int i = 0; i < (MCHRIS(n)); i++)
          _gamma[i] = MeshField<Covector<n,0> >(m);

        Field<MetricTensor<n> >* g = m->getRiemannianMetric();

        MeshField<Covector<n, 0> > g_ij[(SYMR2(n))];
        for(int i = 0; i < (SYMR2(n)); i++) g_ij[i] = compField(*g, i);

        MeshField<Covector<n, 1> > dg_ij[(SYMR2(n))];
        for(int i = 0; i < (SYMR2(n)); i++) dg_ij[i] = d(g_ij[i]);

        for(MElemIt it = m->firstMeshElement();
            it != m->lastMeshElement(); it++) {
          MElement* me = it->first;
          for(int iV = 0; iV < me->getNumVertices(); iV++) {
            Point* p = m->getPoint(me, iV);
            MetricTensor<n> gp = (*g)(p);
            double ins[SYMR2(n)];
            invs<n>(gp.getPointerToCoeffs(), ins);
            for(int i = 0; i < n; i++) {
              for(int j = i; j < n; j++) {
                for(int k = 0; k < n; k++) {
                  double gamma_ij_k = 0.;
                  for(int l = 0; l < n; l++) {
                    double s_kl = ins[symIndex<n>(k,l)];
                    double g_jl_i = (dg_ij[symIndex<n>(j,l)](p))(i);
                    double g_li_j = (dg_ij[symIndex<n>(l,i)](p))(j);
                    double g_ij_l = (dg_ij[symIndex<n>(i,j)](p))(l);
                    gamma_ij_k += s_kl*( g_jl_i + g_li_j - g_ij_l);
                  }
                  gamma_ij_k *= 0.5;
                  _gamma[mChrisIndex<n>(i,j,k)].assign(&gamma_ij_k, me, iV);
                }
              }
            }
            delete p;
          }
        }
        double t2 = Cpu();
        Msg::StatusBar(true, "Christoffel symbols computed (%g s)", t2-t1);
      }
    }
    virtual int getNumCoeffs() const { return MCHRIS(n); }
    virtual int getNumModelCoeffs() const { return MCHRIS(3); }
    virtual Covector<n, 0> getChristoffelSymbol(const Point* p,
                                                int i, int j, int k)
    {
      if(_precompute) return (_gamma[mChrisIndex<n>(i,j,k)])(p);

      const RiemannianManifold<n>* rm =
        static_cast<const RiemannianManifold<n>*>(this->getManifold());
      Field<MetricTensor<n> >* g = rm->getRiemannianMetric();
      Covector<n, 1> dg_ij[(SYMR2(n))];
      for(int ig = 0; ig < (SYMR2(n)); ig++) dg_ij[ig] = d(*g, p, ig);

      MetricTensor<n> gp = (*g)(p);
      double ins[SYMR2(n)];
      invs<n>(gp.getPointerToCoeffs(), ins);
      double gamma_ij_k = 0.;
      for(int l = 0; l < n; l++) {
        double s_kl = ins[symIndex<n>(k,l)];
        double g_jl_i = (dg_ij[symIndex<n>(j,l)])(i);
        double g_li_j = (dg_ij[symIndex<n>(l,i)])(j);
        double g_ij_l = (dg_ij[symIndex<n>(i,j)])(l);
        gamma_ij_k += s_kl*( g_jl_i + g_li_j - g_ij_l);
      }
      gamma_ij_k *= 0.5;
      double c[1] = {gamma_ij_k};
      return Covector<n,0>(this->getManifold(), p, c, MANIFOLD);
    }
    virtual MeshField<Covector<n, 0> >
    getChristoffelSymbol(int i, int j, int k)
    {
      if(!_precompute)
        Msg::Debug("TBD");

      return _gamma[mChrisIndex<n>(i,j,k)];
    }
    MeshField<Covector<n, 0> >
    getGaussianCurvature() const
    {
      const Manifold<n>* man = this->getManifold();
      MeshField<Covector<n, 0> > kappa(man);
      if(n != 2) {
        Msg::Error("Gauss curvature is only defined on 2-manifold.");
        return kappa;
      }
      if(!_precompute) {
        Msg::Debug("TBD");
        return kappa;
      }

      Msg::StatusBar(true, "Computing Gaussian curvature ...");
      double t1 = Cpu();

      MeshField<Covector<n, 1> > dgamma_10_0 =
        d(_gamma[mChrisIndex<n>(1,0,0)]);
      MeshField<Covector<n, 1> > dgamma_10_1 =
        d(_gamma[mChrisIndex<n>(1,0,1)]);
      MeshField<Covector<n, 1> > dgamma_00_0 =
        d(_gamma[mChrisIndex<n>(0,0,0)]);
      MeshField<Covector<n, 1> > dgamma_00_1 =
        d(_gamma[mChrisIndex<n>(0,0,1)]);

      for(MElemIt it = man->firstMeshElement();
          it != man->lastMeshElement(); it++) {
        MElement* me = it->first;
        for(int iV = 0; iV < me->getNumVertices(); iV++) {
          Point* p = man->getPoint(me, iV);
          MetricTensor<n> gp;
          man->getMetricTensor(p, gp);
          double detg = dets<n>(gp.getPointerToCoeffs());
          double g01 = gp(symIndex<n>(0,1));
          double g11 = gp(symIndex<n>(1,1));
          double gamma_10_1 = _gamma[mChrisIndex<n>(1,0,1)](p)(0);
          double gamma_01_0 = _gamma[mChrisIndex<n>(0,1,0)](p)(0);
          double gamma_00_1 = _gamma[mChrisIndex<n>(0,0,1)](p)(0);
          double gamma_11_0 = _gamma[mChrisIndex<n>(1,1,0)](p)(0);

          double gamma_10_0 = _gamma[mChrisIndex<n>(1,0,0)](p)(0);
          double gamma_00_0 = _gamma[mChrisIndex<n>(0,0,0)](p)(0);
          double gamma_01_1 = _gamma[mChrisIndex<n>(0,1,1)](p)(0);
          double gamma_11_1 = _gamma[mChrisIndex<n>(1,1,1)](p)(0);

          double kappap = g01*(dgamma_10_0(p)(0)- dgamma_00_0(p)(1) +
                               gamma_10_1*gamma_01_0 -
                               gamma_00_1*gamma_11_0);
          kappap       += g11*(dgamma_10_1(p)(0)- dgamma_00_1(p)(1) +
                               gamma_10_0*gamma_00_1 -
                               gamma_00_0*gamma_10_1 +
                               gamma_10_1*gamma_01_1 -
                               gamma_00_1*gamma_11_1);
          kappap       /= detg;
          kappa.assign(&kappap, me, iV);
          delete p;
        }
      }
      double t2 = Cpu();
      Msg::StatusBar(true, "Gauss curvature computed (%g s)", t2-t1);
      return kappa;
    }

    virtual ~MetricChristoffelSymbols() {}
  };

}

#endif
