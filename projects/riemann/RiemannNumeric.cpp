// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#include "RiemannNumeric.h"

namespace rm {


  template <>
  void matvec<1,1>(const double* mat, const double* vec, double* res)
  {
    res[0] = mat[0]*vec[0];
  }
  template <>
  void matvec<2,2>(const double* mat, const double* vec, double* res)
  {
    res[0] = mat[0]*vec[0] + mat[1]*vec[1];
    res[1] = mat[2]*vec[0] + mat[3]*vec[1];
  }
  template <>
  void matvec<2,3>(const double* mat, const double* vec, double* res)
  {
    res[0] = mat[0]*vec[0] + mat[1]*vec[1] + mat[2]*vec[2];
    res[1] = mat[3]*vec[0] + mat[4]*vec[1] + mat[5]*vec[2];
  }
  template <>
  void matvec<3,2>(const double* mat, const double* vec, double* res)
  {
    res[0] = mat[0]*vec[0] + mat[1]*vec[1];
    res[1] = mat[2]*vec[0] + mat[3]*vec[1];
    res[2] = mat[4]*vec[0] + mat[5]*vec[1];
  }
  template <>
  void matvec<3,3>(const double* mat, const double* vec, double* res)
  {
    res[0] = mat[0]*vec[0] + mat[1]*vec[1] + mat[2]*vec[2];
    res[1] = mat[3]*vec[0] + mat[4]*vec[1] + mat[5]*vec[2];
    res[2] = mat[6]*vec[0] + mat[7]*vec[1] + mat[8]*vec[2];
  }

  template <>
  void matmat<2, 3, 2>(const double* a, const double* b, double* c)
  {
    c[0] = a[0]*b[0] + a[1]*b[2] + a[2]*b[4];
    c[1] = a[0]*b[1] + a[1]*b[3] + a[2]*b[5];

    c[2] = a[3]*b[0] + a[4]*b[2] + a[5]*b[4];
    c[3] = a[3]*b[1] + a[4]*b[3] + a[5]*b[5];
  }

  template <>
  void matmat<3, 3, 2>(const double* a, const double* b, double* c)
  {
    c[0] = a[0]*b[0] + a[1]*b[2] + a[2]*b[4];
    c[1] = a[0]*b[1] + a[1]*b[3] + a[2]*b[5];

    c[2] = a[3]*b[0] + a[4]*b[2] + a[5]*b[4];
    c[3] = a[3]*b[1] + a[4]*b[3] + a[5]*b[5];

    c[4] = a[6]*b[0] + a[7]*b[2] + a[8]*b[4];
    c[5] = a[6]*b[1] + a[7]*b[3] + a[8]*b[5];
  }

  template <>
  void matmat<3, 3, 3>(const double* a, const double* b, double* c)
  {
    c[0] = a[0]*b[0] + a[1]*b[3] + a[2]*b[6];
    c[1] = a[0]*b[1] + a[1]*b[4] + a[2]*b[7];
    c[2] = a[0]*b[2] + a[1]*b[5] + a[2]*b[8];

    c[3] = a[3]*b[0] + a[4]*b[3] + a[5]*b[6];
    c[4] = a[3]*b[1] + a[4]*b[4] + a[5]*b[7];
    c[5] = a[3]*b[2] + a[4]*b[5] + a[5]*b[8];

    c[6] = a[6]*b[0] + a[7]*b[3] + a[8]*b[6];
    c[7] = a[6]*b[1] + a[7]*b[4] + a[8]*b[7];
    c[8] = a[6]*b[2] + a[7]*b[5] + a[8]*b[8];
  }

  template <>
  double det<0,0>(const double* mat)
  {
    return 0;
  }
  template <>
  double det<1,1>(const double* mat)
  {
    return mat[0];
  }
  template <>
  double det<2,2>(const double* mat)
  {
    return mat[0]*mat[3] - mat[2]*mat[1];
  }
  template <>
  double det<3,3>(const double* mat)
  {
    return (mat[0*3+0] * (mat[1*3+1] * mat[2*3+2] - mat[1*3+2] * mat[2*3+1]) -
            mat[0*3+1] * (mat[1*3+0] * mat[2*3+2] - mat[1*3+2] * mat[2*3+0]) +
            mat[0*3+2] * (mat[1*3+0] * mat[2*3+1] - mat[1*3+1] * mat[2*3+0]));
  }

  template <>
  double inv<0>(const double* mat, double* inv)
  {
    return 0;
  }

  template <>
  double inv<1>(const double* mat, double* inv)
  {
    if(mat[0])
      inv[0] = 1./mat[0];
    else {
      Msg::Error("Singular (1,1)-matrix.");
      vecinit<1>(inv);
    }
    return mat[0];
  }
  template <>
  double inv<2>(const double* mat, double* inv)
  {
    double d = det<2,2>(mat);
    if(d) {
      double ud = 1./d;
      inv[0] =  mat[3] * ud;
      inv[2] = -mat[2] * ud;
      inv[1] = -mat[1] * ud;
      inv[3] =  mat[0] * ud;
    }
    else {
      Msg::Error("Singular (2,2)-matrix.");
      vecinit<2*2>(inv);
    }
    return d;
  }
  template <>
  double inv<3>(const double* mat, double* inv)
  {
    double d = det<3,3>(mat);
    if(d) {
      double ud = 1./d;
      inv[0*3+0] =  (mat[1*3+1]*mat[2*3+2] - mat[1*3+2]*mat[2*3+1])*ud;
      inv[1*3+0] = -(mat[1*3+0]*mat[2*3+2] - mat[1*3+2]*mat[2*3+0])*ud;
      inv[2*3+0] =  (mat[1*3+0]*mat[2*3+1] - mat[1*3+1]*mat[2*3+0])*ud;
      inv[0*3+1] = -(mat[0*3+1]*mat[2*3+2] - mat[0*3+2]*mat[2*3+1])*ud;
      inv[1*3+1] =  (mat[0*3+0]*mat[2*3+2] - mat[0*3+2]*mat[2*3+0])*ud;
      inv[2*3+1] = -(mat[0*3+0]*mat[2*3+1] - mat[0*3+1]*mat[2*3+0])*ud;
      inv[0*3+2] =  (mat[0*3+1]*mat[1*3+2] - mat[0*3+2]*mat[1*3+1])*ud;
      inv[1*3+2] = -(mat[0*3+0]*mat[1*3+2] - mat[0*3+2]*mat[1*3+0])*ud;
      inv[2*3+2] =  (mat[0*3+0]*mat[1*3+1] - mat[0*3+1]*mat[1*3+0])*ud;
    }
    else {
      Msg::Error("Singular (3,3)-matrix.");
      vecinit<3*3>(inv);
    }
    return d;
  }

  template<>
  double dets<1>(const double* smat)
  {
    return smat[0];
  }
  template<>
  double dets<2>(const double* smat)
  {
    return (smat[0]*smat[2] - smat[1]*smat[1]);
  }
  template<>
  double dets<3>(const double* smat)
  {
    return (smat[0]*smat[3]*smat[5] +
	    smat[1]*smat[4]*smat[2] +
	    smat[2]*smat[1]*smat[4] -
	    smat[2]*smat[3]*smat[2] -
	    smat[1]*smat[1]*smat[5] -
	    smat[0]*smat[4]*smat[4]);
  }
  template<>
  double invs<0>(const double* smat, double* inv)
  {
    return 0;
  }
  template<>
  double invs<1>(const double* smat, double* inv)
  {
    if(smat[0])
      inv[0] = 1./smat[0];
    else {
      Msg::Error("Singular symmetric (1,1)-matrix.");
      vecinit<1>(inv);
    }
    return smat[0];
  }
  template<>
  double invs<2>(const double* smat, double* inv)
  {
    double d = dets<2>(smat);
    if(d) {
      double di = 1./d;
      inv[0] =  smat[2]*di;
      inv[1] = -smat[1]*di;
      inv[2] =  smat[0]*di;
    }
    else {
      Msg::Error("Singular symmetric (2,2)-matrix.");
      vecinit<3>(inv);
    }
    return d;
  }

  template<>
  double invs<3>(const double* smat, double* inv)
  {
    double d = dets<3>(smat);
    if(d) {
      double di = 1./d;
      inv[0] = (smat[3]*smat[5] - smat[4]*smat[4])*di;
      inv[1] = (smat[4]*smat[2] - smat[1]*smat[5])*di;
      inv[2] = (smat[1]*smat[4] - smat[3]*smat[2])*di;
      inv[3] = (smat[0]*smat[5] - smat[2]*smat[2])*di;
      inv[4] = (smat[2]*smat[1] - smat[0]*smat[4])*di;
      inv[5] = (smat[0]*smat[3] - smat[1]*smat[1])*di;
    }
    else {
      Msg::Error("Singular symmetric (3,3)-matrix.");
      vecinit<6>(inv);
    }
    return d;
  }

  template <>
  void ids<0>(double* s)
  {
  }

  template <>
  void ids<1>(double* s)
  {
    s[0] = 1.;
  }

  template <>
  void ids<2>(double* s)
  {
    s[0] = 1.;
    s[1] = 0.;
    s[2] = 1.;
  }

  template <>
  void ids<3>(double* s)
  {
    s[0] = 1.;
    s[1] = 0.;
    s[2] = 0.;
    s[3] = 1.;
    s[4] = 0.;
    s[5] = 1.;
  }

  template <>
  void ids<4>(double* s)
  {
    s[0] = 1.;
    s[1] = 0.;
    s[2] = 0.;
    s[3] = 0.;
    s[4] = 1.;
    s[5] = 0.;
    s[6] = 0.;
    s[7] = 1.;
    s[8] = 0.;
    s[9] = 1.;
  }

  template <>
  void full2sym<0>(const double* f, double* s) {}
  template <>
  void full2sym<1>(const double* f, double* s)
  {
    s[0] = f[0];
  }
  template <>
  void full2sym<2>(const double* f, double* s)
  {
    s[0] = f[0];
    s[1] = f[1];
    s[2] = f[3];
  }
  template <>
  void full2sym<3>(const double* f, double* s)
  {
    s[0] = f[0];
    s[1] = f[1];
    s[2] = f[2];

    s[3] = f[4];
    s[4] = f[5];

    s[5] = f[8];
  }

  template <>
  void sym2full<0>(const double* s, double* f) {}
  template <>
  void sym2full<1>(const double* s, double* f)
  {
    f[0] = s[0];
  }
  template <>
  void sym2full<2>(const double* s, double* f)
  {
    f[0] = s[0];
    f[1] = s[1];
    f[2] = s[1];
    f[3] = s[2];
  }
  template <>
  void sym2full<3>(const double* s, double* f)
  {
    f[0] = s[0];
    f[1] = s[1];
    f[2] = s[2];

    f[3] = s[1];
    f[4] = s[3];
    f[5] = s[4];

    f[6] = s[2];
    f[7] = s[4];
    f[8] = s[5];
  }

  template <>
  void sym2full<4>(const double* s, double* f)
  {
    f[0] = s[0];
    f[1] = s[1];
    f[2] = s[2];
    f[3] = s[3];

    f[4] = s[1];
    f[5] = s[4];
    f[6] = s[5];
    f[7] = s[6];

    f[8] = s[2];
    f[9] = s[5];
    f[10] = s[7];
    f[11] = s[8];

    f[12] = s[3];
    f[13] = s[6];
    f[14] = s[8];
    f[15] = s[9];
  }

  template <>
  void diffv<0, 0, 0>(const double* dmat1, const double* v,
                      double* res) {}
  template <>
  void diffv<1, 1, 0>(const double* dmat1, const double* v,
                      double* res)
  {
    res[0] = v[0];
  }
  template <>
  void diffv<2, 2, 0>(const double* dmat1, const double* v,
                      double* res)
  {
    res[0] = v[0];
  }
  template <>
  void diffv<0, 3, 0>(const double* dmat1, const double* v,
                      double* res){}
  template <>
  void diffv<0, 3, 1>(const double* dmat1, const double* v,
                      double* res){}
  template <>
  void diffv<0, 0, 2>(const double* dmat1, const double* v,
                      double* res){}
  template <>
  void diffv<2, 1, 0>(const double* dmat1, const double* v,
                      double* res)
  {
    res[0] = v[0];
  }
  template <>
  void diffv<1, 2, 0>(const double* dmat1, const double* v,
                      double* res)
  {
    res[0] = v[0];
  }

  template <>
  void diffv<3, 1, 0>(const double* dmat1, const double* v,
                      double* res)
  {
    res[0] = v[0];
  }
  template <>
  void diffv<1, 3, 0>(const double* dmat1, const double* v,
                      double* res)
  {
    res[0] = v[0];
  }
  template <>
  void diffv<3, 2, 0>(const double* dmat1, const double* v,
                      double* res)
  {
    res[0] = v[0];
  }
  template <>
  void diffv<2, 3, 0>(const double* dmat1, const double* v,
                      double* res)
  {
    res[0] = v[0];
  }


  template <>
  void diffv<1, 0, 1>(const double* dmat1, const double* v,
                      double* res)
  {
    matvec<1,0>(dmat1, v, res);
  }
  template <>
  void diffv<1, 1, 1>(const double* dmat1, const double* v,
                      double* res)
  {
    matvec<1,1>(dmat1, v, res);
  }
  template <>
  void diffv<1, 1, 2>(const double* dmat1, const double* v,
                      double* res)
  {
    res[0] = 0.;
  }


  template <>
  void diffv<2, 0, 1>(const double* dmat1, const double* v,
                      double* res)
  {
    matvec<2,0>(dmat1, v, res);
  }
  template <>
  void diffv<1, 2, 1>(const double* dmat1, const double* v,
                      double* res)
  {
    matvec<1,2>(dmat1, v, res);
  }
  template <>
  void diffv<2, 1, 1>(const double* dmat1, const double* v,
                      double* res)
  {
    matvec<2,1>(dmat1, v, res);
  }
  template <>
  void diffv<2, 2, 1>(const double* dmat1, const double* v,
                      double* res)
  {
    matvec<2,2>(dmat1, v, res);
  }
  template <>
  void diffv<2, 2, 2>(const double* dmat1, const double* v,
                      double* res)
  {
    double d = det<2,2>(dmat1);
    res[0] = d*v[0];
  }

  template <>
  void diffv<3, 0, 1>(const double* dmat1, const double* v,
                      double* res)
  {
    vecinit<3>(res);
  }
  template <>
  void diffv<1, 3, 1>(const double* dmat1, const double* v,
                      double* res)
  {
    matvec<1,3>(dmat1, v, res);
  }
  template <>
  void diffv<2, 3, 1>(const double* dmat1, const double* v,
                      double* res)
  {
    matvec<2,3>(dmat1, v, res);
  }


  template <>
  void diffv<3, 3, 0>(const double* dmat1, const double* v,
                      double* res)
  {
    res[0] = v[0];
  }
  template <>
  void diffv<3, 3, 1>(const double* dmat1, const double* v,
                      double* res)
  {
    matvec<3,3>(dmat1, v, res);
  }
  template <>
  void diffv<3, 3, 2>(const double* dmat1, const double* v,
                      double* res)
  {
    double dm[3*3];
    adjt<3,3>(dmat1, dm);
    matvec<3,3>(dm, v, res);
  }
  template <>
  void diffv<3, 3, 3>(const double* dmat1, const double* v,
                      double* res)
  {
    double d = det<3,3>(dmat1);
    res[0] = d*v[0];
  }
  template <>
  void diffv<2, 3, 2>(const double* dmat1, const double* v,
                      double* res)
  {
    double dmatk[1*3];
    adjtdegen<2,3,2>(dmat1, dmatk);
    matvec<1,3>(dmatk, v, res);
  }
  template <>
  void diffv<3, 2, 2>(const double* dmat1, const double* v,
                      double* res)
  {
    double dmatk[3*1];
    adjdegen<3,2,2>(dmat1, dmatk);
    matvec<3,1>(dmatk, v, res);
  }

  template <>
  void diffv<3, 1, 1>(const double* dmat1, const double* v,
                      double* res)
  {
    matvec<3,1>(dmat1, v, res);
  }
  template <>
  void diffv<3, 2, 1>(const double* dmat1, const double* v,
                      double* res)
  {
    matvec<3,2>(dmat1, v, res);
  }

  template<>
  void diffs<1,1>(const double* dmat1,
                  const double* s, double* res)
  {
    res[0] = dmat1[0]*s[0]*dmat1[0];
  }


  template <>
  int symIndex<0>(int i, int j)
  {
    return 0;
  }

  template <>
  int symIndex<1>(int i, int j)
  {
    return 0;
  }
  template <>
  int symIndex<2>(int i, int j)
  {
    return i+j;
  }
  template <>
  int symIndex<3>(int i, int j)
  {
    int _index[3][3] = {{0,1,2},{1,3,4},{2,4,5}};
    return _index[i][j];
  }

}


