// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#ifndef _RM_MANIFOLD_H_
#define _RM_MANIFOLD_H_

#include "GModel.h"
#include "RiemannDefines.h"
#include "Point.h"
#include "ModelMap.h"
#include "VectorSpace.h"
#include "MElemIt.h"
#include "OS.h"
#include "StringUtils.h"
#include "PView.h"
#include "PViewData.h"

namespace rm
{
  static int maxManifoldNum = 0;

  template <int n> class MetricTensor;
  template <int n, int k> class Covector;
  void processMeshElements(std::vector<GEntity*>& ents,
                           std::vector<int>& entsOr,
                           std::map<MElement*, int, MElemLessThanNum>& elems);
  void processMeshElements(GEntity* ent,
                           std::set<MElement*, MElemLessThanNum>& elems);


  void getPhysicalGroups(GModel* gm,
                         std::map<int,
                                  std::pair< std::vector<GEntity*>,
                                             std::vector<int> > > groups[4]);

  /// Abstract base for n-manifold, for run-time expressions
  class ManifoldN
  {
  public:
    virtual int getDim() const = 0;
    virtual int getNum() const = 0;
    virtual int getTag() const = 0;
    virtual GModel* getModel() const = 0;
    virtual ModelMapType getModelMapType() const = 0;
    virtual ModelMapProp getModelMapProp() const = 0;
    virtual bool containsMeshElement(MElement* me) const = 0;
    virtual void getPointCoords(const Point* p, double c[MAXCOMP]) const = 0;
    virtual bool containsPoint(const Point* p) const = 0;
    virtual bool getDiffMapToManifold(const Point* p, double d[MAXCOMP],
                                      TensorSpace ts) const = 0;
    virtual bool getDiffMapToModel(const Point* p, double d[MAXCOMP],
                                   TensorSpace ts) const = 0;
    virtual void getMetricTensorCoeffs(const Point* p,
                                       double g[MAXCOMP],
                                       TensorSpace ts) const = 0;
    virtual void getPartOfUnityCoeffs(const Point* p,
                                      std::vector<double>& c,
                                      int order=-1) const = 0;
    virtual void getDiffPartOfUnityCoeffs(const Point* p,
                                          std::vector<double*>& c,
                                          int order=-1) const = 0;
    virtual void getEntities(std::vector<GEntity*>& ents,
                             std::vector<int>& entsOr) const = 0;
    virtual void setEntityOrientations(const std::vector<int>& entsOr) = 0;
    virtual void print() const = 0;
    virtual Point* getPoint(const Point* p) const = 0;
    virtual Point* getPoint(MElement* me, int num) const = 0;
    virtual Point* getPoint(MElement* me,
                            double u, double v, double w) const = 0;
    virtual Point* getPoint(double x, double y, double z,
                            bool strict=false) const = 0;
    virtual Point* getPoint(const double c[MAXCOMP]) const = 0;
    virtual int getNumMeshElements() const = 0;
    virtual MElemIt firstMeshElement() const = 0;
    virtual MElemIt lastMeshElement() const = 0;
    virtual void drawChart(std::string filename,
                           bool discontinousChart=false) const = 0;
    virtual GModel* createModel(Coordinates coords=MANIFOLDCOORDS,
                                bool discontinousChart=false) const = 0;
    static GModel* createModel(std::vector<const ManifoldN*>& manifolds,
                               Coordinates coords=MANIFOLDCOORDS,
                               bool discontinousChart=false);

#if defined(HAVE_AVSOLVER)
    /// Additonal parameter user_data
    static GModel* createModel(std::vector<const ManifoldN*>& manifolds,
                               Coordinates coords,
                               bool discontinousChart,
                               void* user_data);
#endif

    virtual ~ManifoldN() {}
  };

  /// Topological n-manifold
  template <int n>
  class Manifold : public ManifoldN
  {
  protected:
    /// How this manifold relates to Gmsh model
    ModelMap<n>* _mm;
    bool _ownmm;
    /// Gmsh physical group tag
    std::vector<int> _tags;

    /// Unique tag for this manoifold
    int _num;
    /// Boundary manifold of this manifold
    Manifold<n-1>* _bd;
    /// Gmsh entities on this manifold and their orientations
    std::vector<GEntity*> _ents;
    std::vector<int> _entsOr;
    /// Iterators to first and last mesh elements on this manifold
    MElemIt _firstME;
    MElemIt _lastME;

    /// Cached chart GModel
    mutable GModel* _cm;

    void _initEnts(GModel* gm, std::vector<int>& tags);
    void _initChart(GModel* gm, ManifoldChart chart, ModelMap<n>* mm);


    Manifold() : _mm(NULL), _ownmm(false), _tags(std::vector<int>(1,-1)),
                 _num(-1), _bd(NULL), _cm(NULL) {}

    Manifold(GModel* gm, std::vector<int>& tags,
             ManifoldChart chart=ELEMUVW,
             ModelMap<n>* mm=NULL);

  public:

    /// Construct from physical group with given model map
    Manifold(GModel* gm, int tag,
             ManifoldChart chart=ELEMUVW,
             ModelMap<n>* mm=NULL);

    /// Construct from a single mesh element
    Manifold(GModel* gm, MElement* me);

    /// Get Gmsh model this manifold is based on
    GModel* getModel() const { return _mm->getModel(); }

    /// Get Gmsh physical group tag of this manifold
    int getTag() const { return _tags.at(0); }

    /// Get unique tag for this manifold
    int getNum() const { return _num; }

    /// Get the dimension of this manifold
    int getDim() const { return n; }

    /// Get the map from model coordinates to manifold chart
    ModelMap<n>* getModelMap() const { return _mm; }

    /// Get the type of the model map
    ModelMapType getModelMapType() const { return _mm->getModelMapType(); }

    /// Get the property of the model map
    ModelMapProp getModelMapProp() const { return _mm->getModelMapProp(); }

    /// Get Gmsh entities of this manifold and their orientations
    void getEntities(std::vector<GEntity*>& ents,
                     std::vector<int>& entsOr) const;

    /// Set orientatios of Gmsh entities of this manifold
    void setEntityOrientations(const std::vector<int>& entsOr);

    /// Get boundary manifold of this manifold
    Manifold<n-1>* getBoundary() const { return _bd; }

    /// Set the boundary manifold of this manifold
    void setBoundary(Manifold<n-1>* bdm) { _bd = bdm; }

    /// Print manifold info
    virtual void print() const;

    /// Does this manifold contain given mesh element
    bool containsMeshElement(MElement* me) const;

    /// Orientation of a mesh element,
    /// returns 0 if not contained on the manifold
    int getOrientation(MElement* me) const;

    /// Does this manifold contain given point
    bool containsPoint(const Point* p) const;

    /// Get the number of mesh elements on this manifold
    int getNumMeshElements() const { return _firstME.getNumMeshElements(); }

    /// Iterator to mesh elements of this manifold
    MElemIt firstMeshElement() const { return _firstME; }
    MElemIt lastMeshElement() const { return _lastME; }

    /// Get mesh elements on this manifold
    void getMeshElements
    (std::map<MElement*, int, MElemLessThanNum>& elems) const;

    /// Get a new model point on this manifold, at given mesh element vertex
    Point* getPoint(MElement* me, int num) const;

    /// Transform model point to this manifold
    /// (base mesh element of the Points may differ)
    Point* getPoint(const Point* p) const;

    /// Transform model point to this manifold that is known to belong
    /// to the given manifold
    template <int m>
    const Point* getPoint(const Point* p, const Manifold<m>* mp) const;

    /// Get a new model point on this manifold, at given mesh element
    /// parametric coordinates
    Point* getPoint(MElement* me, double u, double v, double w) const;

    /// Get a new model point on this manifold, at model coordinates
    Point* getPoint(double x, double y, double z, bool strict=false) const;

    /// Get a new model point on this manifold, at manifold coordinates
    Point* getPoint(const double[MAXDIM]) const;

    /// Draw the chart of this manifold in a file
    void drawChart(std::string filename,
                   bool discontinousChart=false) const;

    /// Create new Gmsh model from this manifold
    ///  coords:
    ///     MANIFOLDCOORDS: draw the new model in manifold chart coordinates
    ///     MODELCOORDS:    draw the new model in manifold model coordinates
    GModel* createModel(Coordinates coords=MANIFOLDCOORDS,
                        bool discontinousChart=false) const;

    /// Get partition of unity on this manifold at a point
    /// (For example one generated by finite element shape functions)
    virtual void getPartOfUnity(const Point* p,
                                std::vector<Covector<n, 0> >& phi,
                                int order=-1) const;

    /// Get differentials of partition of unity on this manifold at a point
    /// (For example one generated by finite element shape functions)
    virtual void getDiffPartOfUnity(const Point* p,
                                    std::vector<Covector<n, 1> >& dphi,
                                    int order=-1) const;

    /// Get the metric tensor of this manifold
    virtual void getMetricTensor(const Point* p, MetricTensor<n>& gp) const;

    /// Map model point to manifold chart coordinates
    void getPointCoords(const Point* p, double c[MAXCOMP]) const;

    /// Differential (pullback/pushforward) of the map from model to manifold
    /// return false if not available
    /// (I.e. inverse transpose Jacobian or Jacobian)
    bool getDiffMapToManifold(const Point* p, double d[MAXCOMP],
                              TensorSpace ts) const;

    /// Differential (pullback/pushforward) of the map from manifold to model
    /// return false if not available
    /// (I.e. transpose Jacobian or inverse Jacobian)
    bool getDiffMapToModel(const Point* p, double d[MAXCOMP],
                           TensorSpace ts) const;

    /// Get the metric tensor coefficients of this manifold at given point
    virtual void getMetricTensorCoeffs(const Point* p,
                                       double g[MAXCOMP],
                                       TensorSpace ts) const
    {
      vecinit<MAXCOMP>(g);
      Msg::Error("Topological manifold does not have Riemannian metric. \n");
    }

    /// Get coefficients of partition of unity on this manifold at a point
    /// (For example one generated by finite element shape functions)
    virtual void getPartOfUnityCoeffs(const Point* p,
                                      std::vector<double>& c,
                                      int order=-1) const
    {
      Msg::Error("Topological manifold does not have partition of unity.");
    }

    /// Get coefficients of differentials of partition of unity on this
    /// manifold at a point
    /// (For example one generated by finite element shape functions)
    virtual void getDiffPartOfUnityCoeffs(const Point* p,
                                          std::vector<double*>& c,
                                          int order=-1) const
    {
      Msg::Error("Topological manifold does not have partition of unity.");
    }

    void drawJacobianSubspace(std::string filename, MatrixSubspace subspace,
                              bool inv=false, GmshFormat format=MSH) const;

    virtual ~Manifold();

  };

  ///
  /// Member function implementations
  ///


  template <int n>
  void Manifold<n>::_initEnts(GModel* gm, std::vector<int>& tags)
  {
    std::map<int, std::pair< std::vector<GEntity*>,
                             std::vector<int> > > groups[4];
    getPhysicalGroups(gm, groups);

    for(unsigned int iT = 0; iT < tags.size(); iT++) {
      int tag = tags[iT];

      for(int i = gm->getDim(); i >= 0; i--) {
        for(std::map<int, std::pair<std::vector<GEntity*>,
                                    std::vector<int> > >::iterator
        it = groups[i].begin(); it != groups[i].end(); it++){
          int iTag = it->first;
          if(iTag == tag) {
            _ents = it->second.first;
            _entsOr = it->second.second;
            break;
          }
        }
      }
      if(_ents.empty())
        Msg::Fatal("Physical group %d does not exist.", tag);

      _firstME = MElemIt(gm, _ents, _entsOr, false);
      _lastME = MElemIt(gm, _ents, _entsOr, true);
    }
  }

  template <int n>
  void Manifold<n>::_initChart(GModel* gm,
                               ManifoldChart chart, ModelMap<n>* mm)
  {
    bool fallbackUVW = false;
    if(chart == ELEMUVW) {
      fallbackUVW = true;
    }
    else if(chart == MODELXYZ) {
      if(n != 3) {
        Msg::Error("MODELXYZ chart only allowed for a 3-manifold (%d != n)",
                   n);
        fallbackUVW = true;
      }
      void* mmtemp = new ModelMapXYZ(gm);
      _mm = (ModelMap<n>*)mmtemp;
      _ownmm = true;
    }
    else if(chart == MODELXY) {
      if(n != 2) {
        Msg::Error("MODELXY chart only allowed for a 2-manifold (%d != n)",
                   n);
        fallbackUVW = true;
      }
      void* mmtemp = new ModelMapXY(gm);
      _mm = (ModelMap<n>*)mmtemp;
      _ownmm = true;
    }
    else if(chart == MODELX) {
      if(n != 1) {
        Msg::Error("MODELX chart only allowed for a 1-manifold (%d != n)",
                   n);
        fallbackUVW = true;
      }
      void* mmtemp = new ModelMapX(gm);
      _mm = (ModelMap<n>*)mmtemp;
      _ownmm = true;
    }
    else if(chart == USERCHART) {
      if(mm == NULL) {
        Msg::Error("User defined model map not provided.");
        fallbackUVW = true;
      }
      if(mm->getModel() != gm) {
        Msg::Error("The model map and the manifold are not based on the same Gmsh model");
        fallbackUVW = true;
      }
      else _mm = mm;
    }
    else {
      Msg::Error("Unknown manifold chart type %d", chart);
      fallbackUVW = true;
    }
    if(fallbackUVW) {
      _mm = new ModelMapUVW<n>(gm);
      _ownmm = true;
    }
  }

  template <int n>
  Manifold<n>::Manifold(GModel* gm, int tag,
                        ManifoldChart chart,
                        ModelMap<n>* mm) :
    _mm(NULL), _ownmm(false), _num(++maxManifoldNum), _bd(NULL), _cm(NULL)
  {
    Msg::StatusBar(true, "Creating %d-manifold from physical group %d ...", n, tag);
    double t1 = Cpu();

    std::vector<int> tags(1, tag);
    _tags = tags;
    _initEnts(gm, tags);
    _initChart(gm, chart, mm);

    double t2 = Cpu();
    Msg::StatusBar(true, "Done creating %d-manifold (%g s)", n, t2-t1);
  }

  template <int n>
  Manifold<n>::Manifold(GModel* gm, MElement* me) :
    _ownmm(true), _tags(std::vector<int>(1,-1)),
    _num(++maxManifoldNum), _bd(NULL), _cm(NULL)
  {
    if(me->getDim() != n)
      Msg::Fatal("Cannot create %d-manifold from %d-dim mesh element \n",
                 n, me->getDim());
    _mm = new ModelMapUVW<n>(gm);
    _firstME = MElemIt(gm, me, false);
    _lastME = MElemIt(gm, me, true);
  }

  template <int n>
  void Manifold<n>::getEntities(std::vector<GEntity*>& ents,
                                std::vector<int>& entsOr) const
  {
    ents = _ents;
    entsOr = _entsOr;
  }

  template <int n>
  void Manifold<n>::setEntityOrientations(const std::vector<int>& entsOr)
  {
    if(entsOr.size() != _ents.size()) {
      Msg::Error("Invalid entity orientations.");
      return;
    }
    _entsOr = entsOr;
  }

  template <int n>
  void Manifold<n>::print() const
  {
    Msg::Info("%d-Manifold with physical group tag %d and unique tag %d", n, _tags.at(0), _num);
    Msg::Info("  Elementary entities:");
    for(unsigned int i = 0; i < _ents.size(); i++) {
      Msg::Info("    %d-dimensional entity %d, orientation %d.", _ents.at(i)->dim(), _ents.at(i)->tag(), _entsOr.at(i));
    }
  }

  template <int n>
  bool Manifold<n>::containsMeshElement(MElement* me) const
  {
    return _firstME.containsMeshElement(me);
  }

  template <int n>
  int Manifold<n>::getOrientation(MElement* me) const
  {
    return _firstME.getOrientation(me);
  }

  template <int n>
  bool Manifold<n>::containsPoint(const Point* p) const
  {
    if(p->getModel() != this->getModel()) return false;
    return containsMeshElement(p->getMeshElement());
  }

  template <int n>
  void Manifold<n>::getMeshElements
  (std::map<MElement*, int, MElemLessThanNum>& elems) const
  {
    elems.clear();
    for(MElemIt it = this->firstMeshElement();
        it != this->lastMeshElement(); it++) {
      elems[it->first] = it->second;
    }
  }

  template <int n>
  Point* Manifold<n>::getPoint(MElement* me, int num) const
  {
    if(this->containsMeshElement(me))
      return new Point(this->getModel(), me, num);
    else {
      double x = me->getVertex(num)->x();
      double y = me->getVertex(num)->y();
      double z = me->getVertex(num)->z();
      return this->getPoint(x,y,z);
    }
  }

  template <int n>
  Point* Manifold<n>::getPoint(const Point* p) const
  {
    if(this->containsPoint(p)) return new Point(*p);
    double x,y,z;
    p->getXYZ(x,y,z);
    return this->getPoint(x,y,z);
  }

  template <int n>
  Point* Manifold<n>::getPoint(MElement* me,
                               double u, double v, double w) const
  {
    if(this->containsMeshElement(me)) {
      return new Point(this->getModel(), me, u, v, w);
    }
    else {
      SPoint3 sp;
      me->pnt(u, v, w, sp);
      return this->getPoint(sp.x(),sp.y(),sp.z());
    }
  }

  template <int n>
  Point* Manifold<n>::getPoint(double x, double y, double z, bool strict) const
  {
    Point* p = NULL;
    SPoint3 sp(x,y,z);
    GModel* gm = this->getModel();
    std::vector<MElement*> elems = gm->getMeshElementsByCoord(sp, -1, strict);
    for(unsigned int i = 0; i < elems.size(); i++) {
      MElement* me = elems[i];
      if(this->containsMeshElement(me)) {
        double uvw[3];
        double xyz[3];
        for(int i = 0; i < 3; i++) xyz[i] = sp[i];
        me->xyz2uvw(xyz, uvw);
        p = new Point(gm, me, uvw[0], uvw[1], uvw[2], x, y, z);
        break;
      }
    }
    if(p == NULL) {
      Msg::Error("This manifold does not contain point (%g, %g, %g) in model coordinates", x, y, z);
    }
    return p;
  }

  template <int n>
  template <int m>
  const Point* Manifold<n>::getPoint(const Point* p, const Manifold<m>* mp) const
  {
    if(this->containsPoint(p)) return new Point(*p);
    const Point* p2;
    bool success = false;
    if(n >= m) success = mapPointToManifold(mp, this, p, p2);
    else success = mapPointToManifold(this, mp, p, p2);

    if(!success) {
      double x,y,z;
      p->getXYZ(x,y,z);
      Msg::Debug("This manifold does not contain point (%g, %g, %g) in model coordinates", x, y, z);
    }

    return p2;
  }

  template<int n>
  Point* Manifold<n>::getPoint(const double c[MAXDIM]) const
  {
    if(n > 3) {
      Msg::Error("Not available for n-manifolds with n > 3");
      return NULL;
    }

    if(this->getModelMapType() == MM_UVW) {
      Msg::Error("Not available for manifolds with element-wise coordinates");
      return NULL;
    }

    if(this->getModelMapProp() != MM_DIFFMORPH) {
      Msg::Error("Not available for manifolds which are not in one-to-one correspondence with the model");
      return NULL;
    }

    GModel* gm = this->getModel();
    if(this->_cm == NULL) _cm = this->createModel();

    double xyz[3] = {0., 0., 0.};
    for(int i = 0; i < 3; i++) if(n > i) xyz[i] = c[i];
    SPoint3 sp(xyz[0], xyz[1], xyz[2]);

    MElement* chartME = _cm->getMeshElementByCoord(sp, n, false);
    if(chartME == NULL) {
      Msg::Debug("This manifold does not contain point (%g, %g, %g) in manifold coordinates", xyz[0], xyz[1], xyz[2]);
      return NULL;
    }

    double uvw[3];
    chartME->xyz2uvw(xyz, uvw);
    MElement* me = gm->getMeshElementByTag(chartME->getNum());

    return new Point(gm, me, uvw[0], uvw[1], uvw[2]);
  }

  template <int n>
  void Manifold<n>::drawChart(std::string filename,
                              bool discontinousChart) const
  {
    Msg::Info("Writing a manifold chart to file '%s' ... ", filename.c_str());
    GModel* gm = this->createModel(MANIFOLDCOORDS, discontinousChart);
    gm->writeMSH(filename);
    delete gm;
  }

  template <int n>
  GModel* Manifold<n>::createModel(Coordinates coords,
                                   bool discontinousChart) const
  {
    std::vector<const ManifoldN*> manifolds(1, this);
    return ManifoldN::createModel(manifolds, coords, discontinousChart);
  }

  template <int n>
  void Manifold<n>::getPartOfUnity(const Point* p,
                                   std::vector<Covector<n, 0> >& phi,
                                   int order) const
  {
    Msg::Error("Topological manifold does not have partition of unity.");
  }

  template <int n>
  void Manifold<n>::getDiffPartOfUnity(const Point* p,
                                       std::vector<Covector<n, 1> >& dphi,
                                       int order) const
  {
    Msg::Error("Topological manifold does not have partition of unity.");
  }

  template <int n>
  void Manifold<n>::getMetricTensor(const Point* p, MetricTensor<n>& gp) const
  {
    Msg::Error("Topological manifold does not have Riemannian metric. \n");
  }

  template <int n>
  void Manifold<n>::getPointCoords(const Point* p,
                                   double c[MAXCOMP]) const
  {
    this->getModelMap()->mapPoint(p, c);
  }
  template <int n>
  bool Manifold<n>::getDiffMapToManifold(const Point* p,
                                         double d[MAXCOMP],
                                         TensorSpace ts) const
  {
    return this->getModelMap()->getDiffMapToManifold(p, d, ts);
  }
  template <int n>
  bool Manifold<n>::getDiffMapToModel(const Point* p,
                                      double d[MAXCOMP],
                                      TensorSpace ts) const
  {
    return this->getModelMap()->getDiffMapToModel(p, d, ts);
  }

  template <int n>
  void Manifold<n>::drawJacobianSubspace(std::string filename,
                                         MatrixSubspace subspace, bool inv,
                                         GmshFormat format) const
  {
    double t1 = Cpu();
    Msg::Info("Writing Jacobian subspace to file '%s' ... ",
              filename.c_str());

    std::vector<std::map<int, std::vector<double> > > data;
    data.resize(MAX(n,3));

    for(MElemIt it = this->firstMeshElement();
        it != this->lastMeshElement(); it++) {
      MElement* me = it->first;
      for(int iV = 0; iV < me->getNumVertices(); iV++) {
        Point* p = this->getPoint(me, iV);
        double jac[n*3];
        int nrow = n;
        int ncol = 3;
        if(!inv) this->getModelMap()->getDiffMapToManifold(p, jac, TANGENT);
        else {
          nrow = 3;
          ncol = n;
          this->getModelMap()->getDiffMapToModel(p, jac, TANGENT);
        }
        if(subspace == ROWSPACE) {
          for(int i = 0; i < MIN(n,3); i++) {
            for(int k = 0; k < ncol; k++)
              data[i][me->getNum()].push_back(jac[i*ncol+k]);
            for(int k = ncol; k < 3; k++)
              data[i][me->getNum()].push_back(0.);
          }
        }
        else if(subspace == COLSPACE) {
          for(int j = 0; j < MIN(n,3); j++) {
            for(int k = 0; k < nrow; k++)
              data[j][me->getNum()].push_back(jac[k*ncol+j]);
            for(int k = nrow; k < 3; k++)
              data[j][me->getNum()].push_back(0.);
          }
        }
        delete p;
      }
    }

    std::string postFieldName = GetFileNameWithoutPath(filename);
    PView* view = new PView(postFieldName, "ElementNodeData", this->getModel(),
                            data.at(0), 0., 3);
    for(unsigned int i = 1; i < data.size(); i++) {
      view->addStep(this->getModel(), data.at(i), 0., 3);
    }

    if(format == POS) view->getData()->writePOS(filename);
    else view->getData()->writeMSH(filename);
    double t2 = Cpu();
    Msg::Info("Wrote Jacobian subspace (%g s)", t2-t1);
    delete view;
  }

  template <int n>
  Manifold<n>::~Manifold()
  {
    if(_ownmm) delete _mm;
    if(_cm != NULL) delete _cm;
  }

}

#endif
