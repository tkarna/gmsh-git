// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#ifndef _RM_RIEMANNIAN_MANIFOLD_H_
#define _RM_RIEMANNIAN_MANIFOLD_H_

#include "SmoothManifold.h"
#include "ManifoldMap.h"

namespace rm
{

  /// Metric tensor that is identity matrix in Gmsh model coordinates
  template <int n>
  class ModelMetric : public DefinedField<MetricTensor<n> >
  {
  protected:
  public:
    ModelMetric(Manifold<n>* m) :
      DefinedField<MetricTensor<n> >(m) {}

    void getValue(const Point* p, MetricTensor<n>& gp) const
    {
      if(this->getManifold()->getModelMap()->getModelMapType() != MM_XYZ) {
        double c[SYMR2(3)];
        ids<3>(c);
        gp = MetricTensor<n>(this->getManifold(), p, c, MODEL);
      }
      else {
        double c[SYMR2(n)];
        ids<n>(c);
        gp = MetricTensor<n>(this->getManifold(), p, c, MANIFOLD);
      }
    }
  };

  /// Metric tensor that is identity matrix in manifold coordinates
  /// The chart of the Riemannian manifold is then orthonormal
  template <int n>
  class EuclideanMetric : public DefinedField<MetricTensor<n> >
  {
  protected:
  public:
    EuclideanMetric(Manifold<n>* m) :
      DefinedField<MetricTensor<n> >(m) {}

    void getValue(const Point* p, MetricTensor<n>& gp) const
    {
      double c[SYMR2(n)];
      ids<n>(c);
      gp = MetricTensor<n>(this->getManifold(), p, c, MANIFOLD);
    }
  };

  /// Metric tensor that is pulled back from m-manifold to n-manifold
  template <int n>
  class PullbackMetric : public DefinedField<MetricTensor<n> >
  {
  protected:
    void* _g;
    int _dimg;
    MeshField<MetricTensor<n> >* _mg;
    bool _precompute;

  public:
    template <int m>
    PullbackMetric(Manifold<n>* man, Field<MetricTensor<m> >* g,
                   bool precompute=false) :
      DefinedField<MetricTensor<n> >(man)
    {
      _g = g;
      _dimg = m;
      _precompute = precompute;
      if(precompute) {
        ManifoldMap<m,n> phi(g->getManifold(), this->getManifold());
        _mg = new MeshField<MetricTensor<n> >(this->getManifold());
        phi.transformFieldToCod(*g, *_mg);
      }
    }

    void getValue(const Point* p, MetricTensor<n>& gp) const
    {
      if(_precompute) {
        _mg->getValue(p, gp);
        return;
      }
      if(_dimg == 0) {
        Field<MetricTensor<0> >* g = (Field<MetricTensor<0> >*)_g;
        ManifoldMap<0,n> phi(g->getManifold(), this->getManifold());
        phi.transformFieldToCodAtPoint(*g, p, gp);
      }
      else if(_dimg == 1) {
        Field<MetricTensor<1> >* g = (Field<MetricTensor<1> >*)_g;
        ManifoldMap<1,n> phi(g->getManifold(), this->getManifold());
        phi.transformFieldToCodAtPoint(*g, p, gp);
      }
      else if(_dimg == 2) {
        Field<MetricTensor<2> >* g = (Field<MetricTensor<2> >*)_g;
        ManifoldMap<2,n> phi(g->getManifold(), this->getManifold());
        phi.transformFieldToCodAtPoint(*g, p, gp);
      }
      else if(_dimg == 3) {
        Field<MetricTensor<3> >* g = (Field<MetricTensor<3> >*)_g;
        ManifoldMap<3,n> phi(g->getManifold(), this->getManifold());
        phi.transformFieldToCodAtPoint(*g, p, gp);
      }
      else if(_dimg == 4) {
        Field<MetricTensor<4> >* g = (Field<MetricTensor<4> >*)_g;
        ManifoldMap<4,n> phi(g->getManifold(), this->getManifold());
        phi.transformFieldToCodAtPoint(*g, p, gp);
      }
    }
  };

  /// Riemannian n-manifold :
  /// Smooth n-manifold together with a metric tensor field
  template <int n>
  class RiemannianManifold : public SmoothManifold<n>
  {
  protected:

    /// Metric tensor field on the manifold
    Field<MetricTensor<n> >* _g;

    /// Is the metric tensor field object owned by this manifold
    /// If true, destructor deletes it
    bool _owng;

  public:

    /// Construct from given metric tensor field
    /// and use its manifold as a base
    RiemannianManifold(Field<MetricTensor<n> >& g);

    /// Create a Riemannian manifold from Gmsh model physical group
    /// Either use predefined chart or define one using a model map
    ///
    /// Manifold chart type tags:
    ///  ELEMUVW: mesh element parametric coordinates
    ///  MODELXYZ: same as the model coordinates, for a 3-manifold only
    ///  MODELXY: XY-plane in the model coordinates, for a 2-manifold only
    ///  MODELX: X-line in model coordinates, for a 1-manifold only
    ///  USERCHART: user defined chart by a model map
    ///
    /// Manifold metric type tags
    ///  MODELMETRIC:     Metric tensor matrix is identity on model coords,
    ///                   i.e. model coords are orthonormal
    ///  EUCLIDEANMETRIC: Metric tensor matrix is identity on chart coords,
    ///                   i.e. chart coords are orthonormal
    ///  USERMETRIC:      Metric tensor field is provided by the user, neither
    ///                   chart or model coords are assumed orthonormal
    RiemannianManifold(GModel* gm, int tag,
                       ManifoldChart chart=ELEMUVW,
                       ModelMap<n>* mm=NULL,
                       ManifoldMetric metric=MODELMETRIC,
                       Field<MetricTensor<n> >* g=NULL);

    /// Create Riemannian manifolds from all n-dimensional physical
    /// groups of a Gmsh model, mapped by their physical group number
    static std::map<int, Manifold<n>*>
    createManifoldsFromPhysicals
    (GModel* gm, ManifoldChart chart=ELEMUVW, ModelMap<n>* mm=NULL,
     ManifoldMetric metric=MODELMETRIC, Field<MetricTensor<n> >* g=NULL);

    /// Set metric tensor field of this manifold
    template <int m>
    void setRiemannianMetric(Field<MetricTensor<m> >& g,
                             bool precompute=false);

    void setRiemannianMetric(ManifoldMetric metric);

    /// Get metric tensor at a point
    virtual void getMetricTensor(const Point* p, MetricTensor<n>& gp) const;

    /// Get metric tensor field of this manifold
    Field<MetricTensor<n> >* getRiemannianMetric() const { return _g; }

    virtual void getMetricTensorCoeffs(const Point* p,
                                       double g[MAXCOMP],
                                       TensorSpace ts) const
    {
      MetricTensor<n> gp(this, p);
      this->getMetricTensor(p, gp);
      gp.getOperatorCoeffs(g, ts);
    }

    virtual ~RiemannianManifold() { if(_owng) delete _g; }

  };

  template <int n>
  RiemannianManifold<n>::RiemannianManifold(Field<MetricTensor<n> >& g)
  {
    const Manifold<n>* man = g.getManifold();
    man->getEntities(this->_ents, this->_entsOr);
    this->_tag = man->getTag();
    this->_mm = man->getModelMap();
    this->_ownmm = false;
    this->_num = ++maxManifoldNum;
    this->_owng = false;
    this->_dimg = n;
    this->setRiemannianMetric(g);
    this->_bd = NULL;
  }

  template <int n>
  RiemannianManifold<n>::RiemannianManifold(GModel* gm, int tag,
                                            ManifoldChart chart,
                                            ModelMap<n>* mm,
                                            ManifoldMetric metric,
                                            Field<MetricTensor<n> >* g) :
    SmoothManifold<n>(gm, tag, chart, mm)
  {

    if(metric != USERMETRIC) {
      if(metric != MODELMETRIC) this->_g = new EuclideanMetric<n>(this);
      else this->_g = new ModelMetric<n>(this);
      this->_owng = true;
    }
    else if(g == NULL) {
      Msg::Error("User defined metric tensor field not provided.");
      this->_g = new ModelMetric<n>(this);
      this->_owng = true;
    }
    else this->setRiemannianMetric<n>(*g);
  }

  template <int n>
  std::map<int, Manifold<n>*>
  RiemannianManifold<n>::createManifoldsFromPhysicals
  (GModel* gm, ManifoldChart chart, ModelMap<n>* mm,
   ManifoldMetric metric, Field<MetricTensor<n> >* g)
  {
    std::map<int, Manifold<n>*> manifolds;

    std::map<int, std::vector<GEntity*> > groups[4];
    gm->getPhysicalGroups(groups);

    typedef std::map<int, std::vector<GEntity*> >::iterator ItType;
    for(ItType it = groups[n].begin(); it != groups[n].end(); it++) {
      RiemannianManifold<n>* m =
        new RiemannianManifold(gm, it->first, chart, mm, metric, g);
      manifolds[it->first] = m;
    }
    if(manifolds.empty()) {
      Msg::Warning("The model has no %d-dimensional physical groups, no manifolds created", n);
    }
    return manifolds;
  }

  template <int n>
  template <int m>
  void RiemannianManifold<n>::setRiemannianMetric(Field<MetricTensor<m> >& g,
                                                  bool precompute)
  {
    if(_owng) delete _g;
    if(g.getManifold()->getNum() == this->getNum()) {
      _g = (Field<MetricTensor<n> >*)&g;
      _owng = false;
    }
    else {
      _g = new PullbackMetric<n>(this, &g, precompute);
      _owng = true;
    }
  }

  template <int n>
  void RiemannianManifold<n>::setRiemannianMetric(ManifoldMetric metric)
  {
    if(metric == MODELMETRIC) {
      if(_owng) delete _g;
      this->_g = new ModelMetric<n>(this);
      this->_owng = true;
    }
    else if(metric == EUCLIDEANMETRIC) {
      if(_owng) delete _g;
      this->_g = new EuclideanMetric<n>(this);
      this->_owng = true;
    }
  }

  template <int n>
  void RiemannianManifold<n>::getMetricTensor(const Point* p,
                                              MetricTensor<n>& gp) const
  {
    _g->getValue(p, gp);
  }

}

#endif
