// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#ifndef _RM_TENSOR_FUNCTIONS_H_
#define _RM_TENSOR_FUNCTIONS_H_

#include "Manifold.h"
#include "Tensor.h"
#include "ManifoldMap.h"

namespace rm
{

  /// Base unary function object
  template <class TensorType1, class TensorType2>
  class UnaryFunction
  {
  public:
    virtual TensorType2 operator()(const TensorType1& x) const = 0;
  };


  /// Base binary function object
  template <class TensorType1, class TensorType2, class TensorType3>
  class BinaryFunction
  {
  protected:
    bool _checkOperands(const TensorType1& x1,
                        const TensorType2& x2) const
    {
      if(x1.getManifold()->getNum() != x2.getManifold()->getNum()) {
        Msg::Error("Operands do not belong to the same manifold.");
        return false;
      }
      return true;
    }

  public:
    virtual TensorType3 operator()(const TensorType1& x1,
                                   const TensorType2& x2) const = 0;
    virtual SupportType getSupportType() const = 0;
  };

  /// Function object for sum of tensors
  template <class TensorType>
  class Sum : public BinaryFunction<TensorType, TensorType, TensorType>
  {

  public:
    TensorType operator()(const TensorType& x1,
                          const TensorType& x2) const
    {
      return x1+x2;
    }
    SupportType getSupportType() const { return SUPP_UNION; }
  };

  /// Function object for scalar multiplication of tensors
  template <class TensorType>
  class ScalarMult : public BinaryFunction<TensorType, double, TensorType>
  {

  public:
    TensorType operator()(const TensorType& x, const double s) const
    {
      return x*s;
    }
    SupportType getSupportType() const { return SUPP_INTERSECTION; }
  };

  template <class TensorType1, class TensorType2>
  class ScalarMult2 :
    public BinaryFunction<TensorType1, TensorType2, TensorType1>
  {

  public:
    TensorType1 operator()(const TensorType1& x, const TensorType2& y) const
    {
      if(!this->_checkOperands(x, y)) {
        return TensorType1(x.getManifold(), x.getPoint());
      }
      const double temp = y(0);
      return x*temp;
    }
    SupportType getSupportType() const { return SUPP_INTERSECTION; }
  };

  /// Function object for exterior product
  template <class TensorType1, class TensorType2, class TensorType3>
  class ExteriorProduct : public BinaryFunction<TensorType1, TensorType2, TensorType3>
  {
  private:

  public:

    //ExteriorProduct() {}

    TensorType3 operator()(const TensorType1& x1,
                           const TensorType2& x2) const
    {
      const int n = TensorTraits<TensorType1>::dim;
      const int k1 = TensorTraits<TensorType1>::ctrIndex;
      const int l1 = TensorTraits<TensorType1>::coIndex;
      const int k2 = TensorTraits<TensorType2>::ctrIndex;
      const int l2 = TensorTraits<TensorType2>::coIndex;
      if(!this->_checkOperands(x1, x2)) {
        return TensorType3(x1.getManifold(), x1.getPoint());
      }
      double c[(NCK(n,(MAX(k1,l1))+(MAX(k2,l2))))];
      wedge<n, (MAX(k1,l1)), (MAX(k2,l2))>(x1.getPointerToCoeffs(),
                                           x2.getPointerToCoeffs(), c);
      return TensorType3(x1.getManifold(), x1.getPoint(), c, MANIFOLD);
    }
    SupportType getSupportType() const { return SUPP_INTERSECTION; }
  };

  /// Exterior product of k-covectors
  template <int n, int k1, int k2>
  inline Covector<n, k1+k2> operator%(const Covector<n, k1>& x1,
                                      const Covector<n, k2>& x2)
  {
    ExteriorProduct<Covector<n,k1>, Covector<n,k2>,
                    Covector<n,k1+k2> > extprod;
    return extprod(x1, x2);
  }

  /// Exterior product of k-vectors
  template <int n, int k1, int k2>
  inline Vector<n, k1+k2> operator%(const Vector<n, k1>& x1,
                                    const Vector<n, k2>& x2)
  {
    ExteriorProduct<Vector<n,k1>, Vector<n,k2>,
                    Vector<n,k1+k2> > extprod;
    return extprod(x1, x2);
  }

  /// Function object for inner product
  template <class TensorType1, class TensorType2, class TensorType3>
  class InnerProduct : public BinaryFunction<TensorType1, TensorType2, TensorType3>
  {
  public:
    TensorType3 operator()(const TensorType1& x1,
                           const TensorType2& x2) const
    {
      const int n = TensorTraits<TensorType1>::dim;
      const int k = TensorTraits<TensorType1>::ctrIndex;
      const int l = TensorTraits<TensorType1>::coIndex;
      if(!this->_checkOperands(x1, x2)) {
        return TensorType3(x1.getManifold(), x1.getPoint());
      }
      MetricTensor<n> gp;
      x1.getManifold()->getMetricTensor(x1.getPoint(), gp);
      double s[SYMR2(n)];
      gp.getOperatorCoeffs(s, x1.getTensorSpace());
      double c = inprod<n, (MAX(k,l))>(x1.getPointerToCoeffs(),
                                       x2.getPointerToCoeffs(), s);
      return TensorType3(x1.getManifold(), x1.getPoint(), &c, MANIFOLD);
    }
    SupportType getSupportType() const { return SUPP_INTERSECTION; }
  };

  /// Inner product of k-covectors
  template <int n, int k>
  inline Covector<n, 0> operator*(const Covector<n, k>& x1,
                                  const Covector<n, k>& x2)
  {
    InnerProduct<Covector<n,k>, Covector<n,k>, Covector<n,0> > inprod;
    return inprod(x1, x2);
  }

  /// Inner product of k-vectors
  template <int n, int k>
  inline Covector<n, 0> operator*(const Vector<n, k>& x1,
                                  const Vector<n, k>& x2)
  {
    InnerProduct<Vector<n,k>, Vector<n,k>, Covector<n,0> > inprod;
    return inprod(x1, x2);
  }

  /// Function object for Hodge operator
  template <class TensorType1, class TensorType2>
  class Hodge : public UnaryFunction<TensorType1, TensorType2>
  {
  public:
    TensorType2 operator()(const TensorType1& x) const
    {
      const int n = TensorTraits<TensorType1>::dim;
      const int k = TensorTraits<TensorType1>::ctrIndex;
      const int l = TensorTraits<TensorType1>::coIndex;
      double b[(NCK(n,n-(MAX(k,l))))];
      MetricTensor<n> gp;
      x.getManifold()->getMetricTensor(x.getPoint(), gp);
      double s[SYMR2(n)];
      gp.getOperatorCoeffs(s, x.getTensorSpace());
      hodge<n, (MAX(k,l))>(x.getPointerToCoeffs(), b, s);
      return TensorType2(x.getManifold(), x.getPoint(), b, MANIFOLD);
    }
  };

  /// Hodge operator of k-covectors
  template <int n, int k>
  inline Covector<n, n-k> operator*(const Covector<n, k>& x)
  {
    Hodge<Covector<n, k>, Covector<n, n-k> > hodge;
    return hodge(x);
  }
  template <int n, int k>
  inline Covector<n, n-k> h(const Covector<n, k>& x)
  {
    Hodge<Covector<n, k>, Covector<n, n-k> > hodge;
    return hodge(x);
  }

  /// Hodge operator of k-vectors
  template <int n, int k>
  inline Vector<n, n-k> operator*(const Vector<n, k>& x)
  {
    Hodge<Vector<n, k>, Vector<n, n-k> > hodge;
    return hodge(x);
  }
  template <int n, int k>
  inline Vector<n, n-k> h(const Vector<n, k>& x)
  {
    Hodge<Vector<n, k>, Vector<n, n-k> > hodge;
    return hodge(x);
  }

    /// Function object for inner product
  template <class TensorType1, class TensorType2, class TensorType3>
  class InteriorProduct : public BinaryFunction<TensorType1, TensorType2, TensorType3>
  {
  public:
    TensorType3 operator()(const TensorType1& x1,
                           const TensorType2& x2) const
    {
      const int n = TensorTraits<TensorType1>::dim;
      const int k1 = TensorTraits<TensorType1>::ctrIndex;
      const int l1 = TensorTraits<TensorType1>::coIndex;
      const int k2 = TensorTraits<TensorType2>::ctrIndex;
      const int l2 = TensorTraits<TensorType2>::coIndex;
      if(!this->_checkOperands(x1, x2)) {
        return TensorType3(x1.getManifold(), x1.getPoint());
      }
      double c[(NCK(n,(MAX(k1,l1))-(MAX(k2,l2))))];
      intprod<n, (MAX(k1,l1)), (MAX(k2,l2))>(x1.getPointerToCoeffs(),
                                             x2.getPointerToCoeffs(), c);
      return TensorType3(x1.getManifold(), x1.getPoint(), c, MANIFOLD);
    }
    SupportType getSupportType() const { return SUPP_INTERSECTION; }
  };

  /// Interior product of a k-covector and a kv-vector
  template <int n, int k, int kv>
  inline Covector<n, k-kv> operator/(const Covector<n, k>& x1,
                                     const Vector<n, kv>& x2)
  {
    InteriorProduct<Covector<n,k>, Vector<n,kv>, Covector<n,k-kv> > intprod;
    return intprod(x1, x2);
  }

  /// Division by 0-covector
  template <class TensorType>
  inline TensorType operator/
  (const TensorType& x1, const Covector<TensorTraits<TensorType>::dim,0>& x2)
  {
    if(x1.getManifold()->getNum() != x2.getManifold()->getNum()) {
      Msg::Error("Operands do not belong to the same manifold.");
      return x1;
    }
    const int nc = TensorTraits<TensorType>::numCoeffs;
    double c[nc];
    for(int i = 0; i < nc; i++) c[i] = x1(i) / x2(0);
    return TensorType(x1.getManifold(), x1.getPoint(), c, MANIFOLD);
  }

  /// Function object for musical isomorphisms flat and sharp
  template <class TensorType1, class TensorType2>
  class Musical : public UnaryFunction<TensorType1, TensorType2>
  {
  public:
    TensorType2 operator()(const TensorType1& x) const
    {
      const int n = TensorTraits<TensorType1>::dim;
      const int k = TensorTraits<TensorType1>::ctrIndex;
      const int l = TensorTraits<TensorType1>::coIndex;
      double b[(NCK(n,(MAX(k,l))))];
      MetricTensor<n> gp;
      x.getManifold()->getMetricTensor(x.getPoint(), gp);
      double s[SYMR2(n)];
      gp.getOperatorCoeffs(s, x.getTensorSpace());
      musical<n, (MAX(k,l))>(x.getPointerToCoeffs(), b, s);
      return TensorType2(x.getManifold(), x.getPoint(), b, MANIFOLD);
    }
  };

  /// Sharp operator of k-covectors
  template <int n, int k>
  inline Vector<n, k> operator~(const Covector<n, k>& x)
  {
    Musical<Covector<n,k>, Vector<n,k> > sharp;
    return sharp(x);
  }

  /// Flat operator of k-vectors
  template <int n, int k>
  inline Covector<n, k> operator~(const Vector<n, k>& x)
  {
    Musical<Vector<n,k>, Covector<n,k> > flat;
    return flat(x);
  }

  /// Function object for symmetric tensor product of rank 1 tensors
  template <class TensorType1, class TensorType2, class TensorType3>
  class SymTensorProduct11 : public BinaryFunction<TensorType1,
                                                   TensorType2,
                                                   TensorType3>
  {
  public:
    TensorType3 operator()(const TensorType1& x1,
                           const TensorType2& x2) const
    {
      const int n = TensorTraits<TensorType1>::dim;
      if(!this->_checkOperands(x1, x2)) {
        return TensorType3(x1.getManifold(), x1.getPoint());
      }
      double c[(SYMR2(n))];
      double cf[n*n];
      double cf1[n*n];
      double cf2[n*n];
      tprod11<n,n>(x1.getPointerToCoeffs(), x2.getPointerToCoeffs(), cf1);
      tprod11<n,n>(x2.getPointerToCoeffs(), x1.getPointerToCoeffs(), cf2);
      for(int i = 0; i < n*n; i++) cf[i] = 0.5*(cf1[i] + cf2[i]);
      full2sym<n>(cf, c);
      return TensorType3(x1.getManifold(), x1.getPoint(), c, MANIFOLD);
    }
    SupportType getSupportType() const { return SUPP_INTERSECTION; }
  };

  /// Symmetric tensor product of two covectors:
  /// 1/2*((x times y) + (y times x))
  template<int n>
  MetricTensor<n> symtprod(const Covector<n,1>& x, const Covector<n,1>& y)
  {
    SymTensorProduct11<Covector<n,1>, Covector<n,1>, MetricTensor<n> >
      tprod;
    return tprod(x, y);
  }

  /// Function object for tensor transforms
  template <class TensorType1, class TensorType2>
  class Transform : public UnaryFunction<TensorType1, TensorType2>
  {
  private:
    const Manifold<TensorTraits<TensorType2>::dim>* _man;
  public:
    Transform(const Manifold<TensorTraits<TensorType2>::dim>* man) : _man(man) {}
    TensorType2 operator()(const TensorType1& x) const
    {
      const int n = TensorTraits<TensorType1>::dim;
      const int m = TensorTraits<TensorType2>::dim;
      ManifoldMap<n,m> mm(x.getManifold(), this->_man);
      TensorType2 y;
      mm.transformTensorToCod(x, y);
      return y;
    }
  };

  /// Pullback of a (k,l)-tensor to a m-manifold
  template <int n, int m, int k, int l, TensorSymmetry s, int h>
  inline Tensor<m,k,l,s,h> pullback(const Tensor<n,k,l,s,h>& x,
                                    const Manifold<m>* man) {
    Transform<Tensor<n,k,l,s,h>, Tensor<m,k,l,s,h> > pb(man);
    return pb(x);
  }

  /// Pushforward of a (k,0)-tensor to a m-manifold
  template <int n, int m, int k, TensorSymmetry s, int h>
  inline Tensor<m,k,0,s,h> pushforward(const Tensor<n,k,0,s,h>& x,
                                       const Manifold<m>* man) {
    return pullback(x, man);
  }

  /// Function object for linear transformation
  template <class TensorType1, class TensorType2, class TensorType3>
  class LinearTransformation : public BinaryFunction<TensorType1, TensorType2, TensorType3>
  {
  public:
    TensorType3 operator()(const TensorType1& x1,
                           const TensorType2& x2) const
    {
      const int n = TensorTraits<TensorType2>::dim;
      const int kk = TensorTraits<TensorType2>::ctrIndex;
      const int ll = TensorTraits<TensorType2>::coIndex;
      const int k = MAX(kk, ll);

      if(!this->_checkOperands(x1, x2)) {
        return TensorType3(x1.getManifold(), x1.getPoint());
      }
      double tc[n*n];
      x1.getOperatorCoeffs(tc, x2.getTensorSpace());
      double c[NCK(n,k)];

      diffv<n,n,k>(tc, x2.getPointerToCoeffs(), c);

      return TensorType3(x2.getManifold(), x2.getPoint(), c, MANIFOLD);
    }
    SupportType getSupportType() const { return SUPP_INTERSECTION; }
  };

  /// Linear transformation of k-covectors
  template <int n, int k>
  inline Covector<n,k> operator*(const LinTransform<n>& t,
                                 const Covector<n,k>& x)
  {
    LinearTransformation<LinTransform<n>, Covector<n,k>, Covector<n,k> > lt;
    return lt(t, x);
  }

  /// Linear transformation of 1-vectors
  template <int n, int k>
  inline Vector<n,k> operator*(const LinTransform<n>& t,
                               const Vector<n,k>& x)
  {
    LinearTransformation<LinTransform<n>, Vector<n,k>, Vector<n,k> > lt;
    return lt(t, x);
  }


  /// Multiply a tensor with a (0,0)-tensor
  /*template <class TensorType>
  inline TensorType operator*
  (const Tensor<TensorTraits<TensorType>::dim,0,0,TensorTraits<TensorType>::symm,1>& y, const TensorType& x)
  {
    ScalarMult2<TensorType, Tensor<TensorTraits<TensorType>::dim,0,0,TensorTraits<TensorType>::symm,1> > sm;
    return sm(x,y);
  }
  template <class TensorType>
  inline TensorType operator*
  (const TensorType& x, const Tensor<TensorTraits<TensorType>::dim,0,0,TensorTraits<TensorType>::symm,1>& y)
  {
    ScalarMult2<TensorType, Tensor<TensorTraits<TensorType>::dim,0,0,TensorTraits<TensorType>::symm,1> > sm;
    return sm(x,y);
    }*/

}
#endif
