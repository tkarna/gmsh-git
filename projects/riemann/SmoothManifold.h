// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#ifndef _RM_SMOOTH_MANIFOLD_H_
#define _RM_SMOOTH_MANIFOLD_H_


#include "Manifold.h"
#include "Field.h"
#include "ManifoldPartOfUnity.h"

namespace rm
{
  // Smooth n-manifold : topological n-manifold together with
  // a piecewise smooth partition of unity
  template <int n>
  class SmoothManifold : public Manifold<n>
  {
  protected:
    // Custom differentiable partition of unity (not uset yet)
    mutable ElemPartOfUnity<n>* _phi;
    mutable ElemDiffPartOfUnity<n>* _dphi;
    mutable bool _ownphi;
    mutable bool _owndphi;
    SmoothManifold() : Manifold<n>() {}

  public:
    SmoothManifold(GModel* gm, int tag,
                   ManifoldChart chart=ELEMUVW,
                   ModelMap<n>* mm=NULL) :
      Manifold<n>(gm, tag, chart, mm), _phi(NULL), _dphi(NULL),
      _ownphi(false), _owndphi(false) {}

    SmoothManifold(GModel* gm, MElement* me) :
      Manifold<n>(gm, me), _phi(NULL), _dphi(NULL),
      _ownphi(false), _owndphi(false) {}

    /// Get partition of unity at point
    void getPartOfUnity(const Point* p, std::vector<Covector<n, 0> >& phi,
                        int order=-1) const;

    /// Get exterior derivatives of partition of unity at point
    void getDiffPartOfUnity(const Point* p,
                            std::vector<Covector<n, 1> >& dphi,
                            int order=-1) const;

    /// Get metric tensor at point
    virtual void getMetricTensor(const Point* p, MetricTensor<n>& gp) const;

    virtual void getPartOfUnityCoeffs(const Point* p,
                                      std::vector<double>& c,
                                      int order=-1) const
    {
      _phi = new ElemPartOfUnity<n>(this);
      _phi->getPartOfUnityCoeffs(p, c, order);
      delete _phi;
    }
    virtual void getDiffPartOfUnityCoeffs(const Point* p,
                                          std::vector<double*>& c,
                                          int order=-1) const
    {
      _dphi = new ElemDiffPartOfUnity<n>(this);
      _dphi->getDiffPartOfUnityCoeffs(p, c, order);
      delete _dphi;
    }

  };

  template <int n>
  void SmoothManifold<n>::getPartOfUnity(const Point* p,
                                         std::vector<Covector<n, 0> >& phi,
                                         int order) const
  {
    _phi = new ElemPartOfUnity<n>(this);
    _phi->getPartOfUnity(p, phi, order);
    delete _phi;
  }

  template <int n>
  void SmoothManifold<n>::getDiffPartOfUnity
  (const Point* p, std::vector<Covector<n, 1> >& dphi, int order) const
  {
    _dphi = new ElemDiffPartOfUnity<n>(this);
    _dphi->getDiffPartOfUnity(p, dphi, order);
    delete _dphi;
  }

  template <int n>
  void SmoothManifold<n>::getMetricTensor
  (const Point* p, MetricTensor<n>& gp) const
  {
    Msg::Error("Smooth manifold does not have Riemannian metric. \n");
  }

}

#endif
