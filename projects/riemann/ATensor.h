// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#ifndef _RM_ATENSOR_H_
#define _RM_ATENSOR_H_

#include "RiemannDefines.h"
#include "Manifold.h"
#include "RiemannNumeric.h"
#include "VectorSpace.h"

namespace rm
{
  int numCoeffs(int n, int k, int l, TensorSymmetry s, Representation r);

  /// Any tensor on any dimensional manifold, for run-time expressions
  class ATensor : public VectorSpaceCat<ATensor, double>
  {
  private:
    int _n;
    int _k;
    int _l;
    TensorSymmetry _s;
    const ManifoldN* _m;
    const Point* _p;

    std::vector<double> _c;
    Representation _r;

  public:
    ATensor(int n, int k, int l, TensorSymmetry s,
            const ManifoldN* m, const Point* p,
            std::vector<double>& c, Representation r) :
      _n(n), _k(k), _l(l), _s(s), _m(m), _p(p), _c(c), _r(r) {}

    template <class TensorType>
    ATensor(const TensorType& x)
    {
      _n = x.getDim();
      _k = x.getContravariantIndex();
      _l = x.getCovariantIndex();
      _s = x.getTensorSymmetry();
      _m = x.getManifold();
      _p = x.getPoint();
      for(int i = 0; i < x.getNumCoeffs(); i++) _c.push_back(x(i));
      _r = MANIFOLD;
    }
    const ManifoldN* getManifold() const
    {
      if(_n == 1) return static_cast<const Manifold<1>*>(_m);
      else if(_n == 2) return static_cast<const Manifold<2>*>(_m);
      else if(_n == 3)  return static_cast<const Manifold<3>*>(_m);
      else return _m;
    }
    const Point* getPoint() const { return _p; }
    Representation getRepresentation() const { return _r; }
    inline int getDim() const { return _n; }
    inline int getContravariantIndex() const { return _k; }
    inline int getCovariantIndex() const { return _l; }
    inline TensorSymmetry getTensorSymmetry() const { return _s; }
    inline TensorSpace getTensorSpace() const
    {
      if(_k != 0 && _l == 0) return TANGENT;
      else if(_k == 0 && _l != 0) return COTANGENT;
      else return MIXEDSPACE;
    }
    inline int getNumCoeffs() const
    {
      return numCoeffs(_n, _k, _l, _s, _r);
    }
    void print() const
    {
      Msg::Info("(%d, %d)-Tensor on %d-manifold", _k, _l, _n);
      if(_r == MANIFOLD)
        Msg::Info("  Manifold coefficients:");
      else if(_r == MODEL)
        Msg::Info("  Model coefficients:");
      for(int i = 0; i < this->getNumCoeffs(); i++)
        Msg::Info("  %12.5E", _c.at(i));
    }
    inline double  operator() (int i) const { return _c.at(i); }
    inline double& operator() (int i) { return _c.at(i); }

    ATensor& operator+=(const ATensor& x)
    {
      if(this->getDim() != x.getDim() ||
         this->getCovariantIndex() != x.getCovariantIndex() ||
         this->getContravariantIndex() != x.getContravariantIndex() ||
         this->getTensorSymmetry() != x.getTensorSymmetry()) {
        Msg::Error("Operands are not compatible.");
        return *this;
      }
      if(this->getManifold()->getNum() != x.getManifold()->getNum()) {
        Msg::Error("Operands do not belong to the same manifold.");
        return *this;
      }
      for(unsigned int i = 0; i < _c.size(); i++)
        this->operator()(i) += x(i);
      return *this;
    }
    ATensor& operator*=(const ATensor& x)
    {
      if(this->getDim() != x.getDim() ||
         x.getCovariantIndex() != 0 ||
         x.getContravariantIndex() != 0) {
        Msg::Error("Operands are not compatible.");
        return *this;
      }
      if(this->getManifold()->getNum() != x.getManifold()->getNum()) {
        Msg::Error("Operands do not belong to the same manifold.");
        return *this;
      }
      for(unsigned int i = 0; i < _c.size(); i++)
        this->operator()(i) *= x(0);
      return *this;
    }
    ATensor& operator*=(const double& x)
    {
      for(unsigned int i = 0; i < _c.size(); i++)
        this->operator()(i) *= x;
      return *this;
    }

  };
}

#endif
