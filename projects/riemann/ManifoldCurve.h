// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#ifndef _RM_MANIFOLD_CURVE_H_
#define _RM_MANIFOLD_CURVE_H_

#include "Manifold.h"

namespace rm
{
  bool meshElementsTouch(MElement* e1, MElement* e2);

  /// Curve on n-manifold
  /// I.e. a mapping from real number interval to n-manifold
  template <int n>
  class ManifoldCurve
  {
  protected:
    const Manifold<n>* _m;
    const double _tmin;
    const double _tmax;
    double _hcoeff; // step size coefficient, the actual step size is determined from the tangent lenghts and this coefficient
    std::vector<const Point*> _p; // images at tmin+sum_index(h(index))
    std::vector<Vector<n,1> > _v; // tangents at tmin+sum_index(h(index))
    std::vector<double> _h; // realized step sizes

    GModel* _cgm;
    std::vector<MVertex*> _modelVertices;

    ChristoffelSymbols<n>* _mcs;
    Field<Vector<n,1> >* _vf;

    void _createModel()
    {
      if(_cgm != NULL) return;
      std::vector<ManifoldCurve<n>*> curves(1,this);
      _cgm = ManifoldCurve<n>::createModel(curves);
    }

    const Point* _updatePoint(double y[2*n], bool findNextElem=true)
    {
      if(this->getManifold()->getModelMapType() != MM_UVW) {
        double xyz[3] = {0., 0., 0.};
        for(int in = 0; in < n; in++) xyz[in] = y[n+in];
        const Point* p = this->getManifold()->getPoint(xyz[0], xyz[1], xyz[2]);
        if(p == NULL) Msg::Debug("Model point not found");
        return p;
      }
      else {
        const Point* p = this->_p.back();
        MElement* me = p->getMeshElement();
        double uvw[3];
        for(int in = n; in < 2*n; in++) uvw[in-n] = y[in];
        for(int in = n; in < 3; in++) uvw[in] = 0.;
        Point* newp = new Point(p->getModel(), me, uvw[0], uvw[1], uvw[2]);
        if(!findNextElem || me->isInside(uvw[0], uvw[1], uvw[2])) return newp;
        else {
          double xx,yy,zz;
          newp->getXYZ(xx,yy,zz);
          delete newp;
          newp = this->getManifold()->getPoint(xx, yy, zz);
          if(newp == NULL) {
            Msg::Debug("Model point not found");
            return newp;
          }
          if(!meshElementsTouch(me, newp->getMeshElement())) {
            Msg::Error("Illegal curve point, use smaller step size");
            delete newp;
            return NULL;
          }

          /*double xxx,yyy,zzz;
          newp->getXYZ(xxx,yyy,zzz);
          printf("oldme %d (%g, %g, %g)\nnewme %d (%g, %g, %g)\n\n",
                 me->getNum(), xx, yy, zz,
                 newp->getMeshElement()->getNum(), xxx, yyy, zzz);*/

          // transform velocity vector to the new mesh element coords
          double jac1[3][3];
          me->getJacobian(uvw[0], uvw[1], uvw[2], jac1);
          double vec[3];
          for(int in = 0; in < n; in++) vec[in] = y[in];
          for(int in = n; in < 3; in++) vec[in] = 0.;
          double temp[3] = {0., 0., 0.};
          // jac1^T*vec, since Gmsh Jacobian is transposed
          temp[0] = jac1[0][0]*vec[0] + jac1[1][0]*vec[1] + jac1[2][0]*vec[2];
          temp[1] = jac1[0][1]*vec[0] + jac1[1][1]*vec[1] + jac1[2][1]*vec[2];
          temp[2] = jac1[0][2]*vec[0] + jac1[1][2]*vec[1] + jac1[2][2]*vec[2];

          newp->getUVW(uvw[0], uvw[1], uvw[2]);
          double jac2[3][3];
          newp->getMeshElement()->getJacobian(uvw[0], uvw[1], uvw[2], jac2);
          double inv2[3][3];
          inv3x3(jac2, inv2);
          // inv(jac2)^T*temp, since Gmsh Jacobian is transposed
          vec[0] = inv2[0][0]*temp[0] + inv2[1][0]*temp[1] + inv2[2][0]*temp[2];
          vec[1] = inv2[0][1]*temp[0] + inv2[1][1]*temp[1] + inv2[2][1]*temp[2];
          vec[2] = inv2[0][2]*temp[0] + inv2[1][2]*temp[1] + inv2[2][2]*temp[2];
          //printarr<2*n>(y);
          for(int in = n; in < 2*n; in++) y[in] = uvw[in-n];
          for(int in = 0; in < n; in++) y[in] = vec[in];
          //printarr<2*n>(y);
          return newp;
        }
      }
    }

    void f1(const double t, double y[2*n], double yp[2*n])
    {
      const Point* p = _updatePoint(y, false);
      vecinit<2*n>(yp);

      for(int k = 0; k < n; k++) {
        for(int i = 0; i < n; i++) {
          for(int j = 0; j < n; j++) {
            if(p != NULL) {
              yp[k] -= (_mcs->getChristoffelSymbol(p,i,j,k)(0))*y[i]*y[j];
            }
          }
        }
      }
      for(int k = n; k < 2*n; k++) {
        yp[k] = y[k-n];
      }
      if(p != NULL) delete p;
    }

    void f2(const double t, double y[2*n], double yp[2*n])
    {
      const Point* p = _updatePoint(y, false);
      vecinit<2*n>(yp);
      if(p == NULL) return;

      for(int k = 0; k < n; k++) {
        Vector<n,1> v = _vf->operator()(p);
        y[k] = v(k);
      }

      for(int k = n; k < 2*n; k++) {
        yp[k] = y[k-n];
      }
      delete p;
    }

    ManifoldCurve(const Manifold<n>* m, double tmin, double tmax,
                  double hcoeff) :
      _m(m), _tmin(tmin), _tmax(tmax), _hcoeff(hcoeff), _cgm(NULL), _mcs(NULL), _vf(NULL)
    {
      ModelMapType mt = m->getModelMapType();
      if(mt == MM_NUMERIC || mt == MM_SYMBOLIC) {
        Msg::Error("Manifold curves are available only for manifolds with model and mesh element coordinate charts");
      }
      if(_hcoeff > 1000.)
        Msg::Warning("Very small step size (try 1 - 1000 as step size coefficient)");
      else if (_hcoeff < 1.)
        Msg::Warning("Very large step size (try 1 - 1000 as step size coefficient)");
      if(_hcoeff <= 0.) {
        _hcoeff = 10.;
        Msg::Error("Invalid step size, step size coefficient set to %g",
                   _hcoeff);
      }
    }

    void RK4(const Point* p0, const Vector<n,1>& v0, int fi)
    {
      // Runge-Kutta 4:
      this->_p.push_back(new Point(*p0));
      this->_v.push_back(v0);

      double vc[n];
      v0.getCoeffs(vc);
      double length = sqrt(dot<n>(vc,vc));
      double h = 1./(this->_hcoeff*length);
      this->_h.push_back(h);

      double y[2*n];
      for(int in = 0; in < n; in++) y[in] = v0(in);
      if(this->getManifold()->getModelMapType() == MM_UVW) {
        double uvw[3];
        p0->getUVW(uvw[0], uvw[1], uvw[2]);
        for(int in = n; in < 2*n; in++) y[in] = uvw[in-n];
      }
      else {
        double xyz[3];
        p0->getXYZ(xyz[0], xyz[1], xyz[2]);
        for(int in = n; in < 2*n; in++) y[in] = xyz[in-n];
      }

      double t = this->_tmin;
      int step = 0;
      while(t <= this->_tmax) {
        step++;
        t = this->_tmin + step*h;
        //Msg::Debug("Geodesic computation step %d time %g", step, t);

        double k1[2*n];
        if(fi == 1) this->f1(t, y, k1);
        if(fi == 2) this->f2(t, y, k1);
        for(int in = 0; in < 2*n; in++) k1[in] *= h;

        double k2[2*n];
        double yk1[2*n];
        axpy<2*n>(0.5, k1, y, yk1);
        if(fi == 1) this->f1(t+0.5*h, yk1, k2);
        if(fi == 2) this->f2(t+0.5*h, yk1, k2);
        for(int in = 0; in < 2*n; in++) k2[in] *= h;

        double k3[2*n];
        double yk2[2*n];
        axpy<2*n>(0.5, k2, y, yk2);
        if(fi == 1) this->f1(t+0.5*h, yk2, k3);
        if(fi == 2) this->f2(t+0.5*h, yk2, k3);
        for(int in = 0; in < 2*n; in++) k3[in] *= h;

        double k4[2*n];
        double yk3[2*n];
        axpy<2*n>(1., k3, y, yk3);
        if(fi == 1) this->f1(t+h, yk3, k4);
        if(fi == 2) this->f2(t+h, yk3, k4);
        for(int in = 0; in < 2*n; in++) k4[in] *= h;

        for(int in = 0; in < 2*n; in++)
          y[in] = y[in] + (1./6.)*(k1[in] + 2*k2[in] + 2*k3[in] + k4[in]);


        const Point* p = this->_updatePoint(y);
        if(p == NULL) {
          Msg::Warning("Curve went off the manifold after %d steps at parameter value %g", step, t);
          break;
        }



        for(int in = 0; in < n; in++) vc[in] = y[in];
        this->_p.push_back(p);
        this->_v.push_back(Vector<n,1>(this->getManifold(), p, vc, MANIFOLD));

        length = sqrt(dot<n>(vc,vc));
        h = 1./(this->_hcoeff*length);
        this->_h.push_back(h);

        //printarr<2*n>(y);

        //_p.back().print();
        //_v.back().print();
      }
    }

  public:

    static ManifoldCurve<n>* flow
    (const Manifold<n>* m,
     const Point* p0, Field<Vector<n,1> >& vf,
     double tmin, double tmax, double hcoeff)
    {
      ManifoldCurve<n>* flow = new ManifoldCurve<n>(m, tmin, tmax, hcoeff);

      double t1 = Cpu();
      Msg::Info("Computing flow of a vector field ... ");

      flow->_vf = &vf;

      flow->RK4(p0, vf(p0), 2);

      double t2 = Cpu();
      Msg::Info("Flow curve computed (%g s)", t2-t1);

      flow->_createModel();

      return flow;
    }


    static ManifoldCurve<n>* geodesic
      (const RiemannianManifold<n>* m,
       const Point* p0, const Vector<n,1>& v0,
       double tmin, double tmax, double hcoeff)
    {
      ManifoldCurve<n>* geodesic = new ManifoldCurve<n>(m, tmin, tmax, hcoeff);

      double t1 = Cpu();
      Msg::Info("Computing geodesic curve ... ");

      const RiemannianManifold<n>* rm =
        static_cast<const RiemannianManifold<n>*>(geodesic->getManifold());
      geodesic->_mcs = new MetricChristoffelSymbols<n>(rm);

      geodesic->RK4(p0, v0, 1);

      double t2 = Cpu();
      Msg::Info("Geodesic curve computed (%g s)", t2-t1);

      geodesic->_createModel();

      return geodesic;
    }

    const Manifold<n>* getManifold() const { return _m; }
    GModel* getCurveModel() const { return _cgm; }

    double length()
    {
      if(_cgm == NULL) this->_createModel();

      const RiemannianManifold<n>* rm =
        static_cast<const RiemannianManifold<n>*>(this->getManifold());


      RiemannianManifold<1> curve(_cgm, 1);
      PullbackMetric<1> g(&curve, rm->getRiemannianMetric());
      curve.setRiemannianMetric(g);
      MetricVolumeForm<1> vol(&curve);
      return i(vol, &curve);
    }

    rm::MeshField<Vector<n,1> > getTangents() const
    {
      rm::MeshField<Vector<n,1> > tangents(this->getManifold());
      for(unsigned int i = 0; i < _v.size(); i++) {
        tangents.assign(_v.at(i));
      }
      return tangents;
    }

    /*ManifoldPoint<n> getManifoldPoint(double t)
    {
      if(_p.empty()) {
        Msg::Error("Curve is empty");
        double c[n];
        vecinit<n>(c);
        return ManifoldPoint<n>(_m, c);
      }

      // t = i*h+u
      int i = floor(t/_h);
      double c[n];
      vecinit<n>(c);
      if(i >= (int)_p.size()) {
        if(t >= this->_tmin && t <= this->_tmax)
          Msg::Debug("Manifold curve error");
        return ManifoldPoint<n>(this->getManifold(), c);
      }
      double u = t-i*this->_h;
      const double* c1 = _p.at(i).getPointerToCoeffs();
      const double* c2 = _p.at(i+1).getPointerToCoeffs();
      axpy<n>(u, c2, c1, c);
      return ManifoldPoint<n>(this->getManifold(), c);
      }*/

    void writeMSH(std::string filename, GmshFormat format=MSH) const
    {
      if(_p.empty() || _cgm == NULL) {
        Msg::Error("Curve is empty, not written to a file");
        return;
      }
      double t1 = Cpu();
      Msg::Info("Writing a curve to file '%s' ... ", filename.c_str());
      std::vector<std::map<int, std::vector<double> > > data;
      data.resize(1);

      std::string postFieldName = GetFileNameWithoutPath(filename);
      //gm->writeMSH(filename);

      //const double* p0 = _p[0].getPointerToCoeffs();
      double prevt = 0;
      for(unsigned int iV = 0; iV < _modelVertices.size(); iV++) {
        if(iV == 0) {
          data[0][_modelVertices[iV]->getNum()].push_back(_tmin);
          prevt = _tmin;
        }
        else {
          double ti = prevt+_h.at(iV);
          data[0][_modelVertices[iV]->getNum()].push_back(ti);
          prevt = ti;
        }
        /*const double* p1 = _p[iV].getPointerToCoeffs();
        double vec[n];
        for(int in = 0; in < n; in++) {
          vec[in] = p1[in] - p0[in];
          data[0][v[iV]->getNum()].push_back(vec[in]);
          }*/
      }

      PView* view = new PView(postFieldName, "NodeData", _cgm,
                              data.at(0), 0., 1);

      /*double steptime = 0;
      for(unsigned int i = 1; i < data.size(); i++) {
        steptime += i*_h;
        view.addStep(gm, data.at(i), steptime, n);
        }*/
      if(format == POS) view->getData()->writePOS(filename);
      else view->getData()->writeMSH(filename);

      double t2 = Cpu();
      Msg::Info("Wrote a curve (%g s)", t2-t1);
    }

    static GModel* createModel(std::vector<ManifoldCurve<n>*>& curves,
                               std::string filename="", GmshFormat format=MSH,
                               Field<Covector<n,0> >* interpolant=NULL)
    {
      std::map<int, std::vector<MElement*> > elements;
      std::map<int, std::vector<int> > physicals;
      MElementFactory factory;
      MVertex* prevver = NULL;

      std::vector<std::map<int, std::vector<double> > > data;
      if(interpolant != NULL) data.resize(3);
      else data.resize(2);

      for(unsigned int iC = 0; iC < curves.size(); iC++) {
        double modelLength = 0;
        int reg = iC+1;
        ManifoldCurve<n>* curve = curves.at(iC);
        double prevt = 0;
        for(unsigned int iP = 0; iP < curve->_p.size(); iP++) {
          MVertex* newver = NULL;
          double x,y,z;
          const Point* p = curve->_p[iP];
          p->getXYZ(x,y,z);
          newver = new MVertex(x,y,z);
          curve->_modelVertices.push_back(newver);
          if(iP == 0) {
            prevt = curve->_tmin;
            data[0][newver->getNum()].push_back(prevt);
          }
          else {
            double ti = prevt + curve->_h.at(iP);
            data[0][newver->getNum()].push_back(ti);
            prevt = ti;
          }
          if(interpolant != NULL) {
            data[2][newver->getNum()].push_back(interpolant->operator()(p)(0));
          }
          if(iP > 0) {
            std::vector<MVertex*> meVertices;
            meVertices.push_back(prevver);
            meVertices.push_back(newver);
            MElement* newme = factory.create(MSH_LIN_2, meVertices);
            elements[reg].push_back(newme);
            physicals[reg] = std::vector<int>(1, reg);
            modelLength += newme->getVolume();
          }
          data[1][newver->getNum()].push_back(modelLength);
          prevver = newver;
        }
      }
      GModel* gm = GModel::createGModel(elements, physicals);

      if(filename != "") {
        double t1 = Cpu();
        Msg::Info("Writing curves to file '%s' ... ", filename.c_str());
        std::string postFieldName = GetFileNameWithoutPath(filename);
        PView* view = new PView(postFieldName, "NodeData", gm,
                                data.at(0), 0., 1);
        view->addStep(gm, data.at(1), 1., 1);
        if(interpolant != NULL) {
          view->addStep(gm, data.at(2), 2., 1);
        }
        if(format == POS) view->getData()->writePOS(filename);
        else view->getData()->writeMSH(filename);
        double t2 = Cpu();
        Msg::Info("Wrote curves (%g s)", t2-t1);
      }

      return gm;
    }

    ~ManifoldCurve()
    {
      for(unsigned int iP = 0; iP < _p.size(); iP++) delete _p[iP];
      if(_mcs != NULL) delete _mcs;
      if(_cgm != NULL) delete _cgm;
    }

  };

}

#endif
