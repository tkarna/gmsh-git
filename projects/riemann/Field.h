// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#ifndef _RM_FIELD_H_
#define _RM_FIELD_H_

#include "Manifold.h"
#include "MElement.h"
#include "StringUtils.h"
#include "OS.h"
#include "PView.h"
#include "PViewData.h"
#include "Point.h"
#include "Tensor.h"
#include "TensorTraits.h"

namespace rm
{
  class TensorField
  {
  public:
    virtual const ManifoldN* getManifoldN() const = 0;
    virtual inline void getTensorCoeffs(const Point* p,
                                        double c[MAXCOMP]) const = 0;
    virtual bool inSupport(MElement* me) const = 0;
    virtual void writeMSH(std::string filename,
                          Representation r=MODEL,
                          Coordinates coords=MODELCOORDS,
                          GmshFormat format=MSH) const = 0;
    virtual void writePOS(std::string filename, Representation r=MODEL,
                          Coordinates coords=MODELCOORDS) const = 0;
    virtual bool readMSH(std::string filename, Representation r=MODEL,
                         int step=0, GmshFormat format=MSH) = 0;
    virtual bool readPOS(std::string filename, Representation r=MODEL,
                         int step=0) = 0;
  };

  /// Abstract base for tensor field on a manifold
  /// Templated by the tensor type (i.e. 1-vector, 1-covector ...)
  template <class TensorType>
  class Field : public TensorField
  {
  protected:
    /// Manifold this field belongs to
    const Manifold<TensorTraits<TensorType>::dim>* _m;

    Field() : _m(NULL) {}
  public:

    /// Construct from manifold
    Field(const Manifold<TensorTraits<TensorType>::dim>* m) : _m(m) {}

    /// Get the manifold this field is defined on
    const Manifold<TensorTraits<TensorType>::dim>* getManifold() const
    {
      return _m;
    }
    virtual const ManifoldN* getManifoldN() const
    {
      return _m;
    }

    /// Get value at a point
    virtual void getValue(const Point* p, TensorType& tp) const = 0;
    virtual inline TensorType operator() (const Point* p) const
    {
      TensorType tp(this->getManifold(), p);
      this->getValue(p, tp);
      return tp;
    }

    /// Get coefficients at a point
    virtual inline void getCoeffs
    (const Point* p, double c[TensorTraits<TensorType>::numCoeffs]) const
    {
      TensorType x = this->operator()(p);
      x.getCoeffs(c);
    }
    virtual inline void getTensorCoeffs(const Point* p,
                                        double c[MAXCOMP]) const
    {
      getCoeffs(p, c);
    }

    /// Return true if given mesh element overlaps with the support
    /// of this field
    virtual bool inSupport(MElement* me) const { return true; }

    int _addDataToWrite
      (std::vector<std::map<int, std::vector<double> > >& data,
       Representation r, int step=1) const
    {

      const int n = TensorTraits<TensorType>::dim;
      const Manifold<n>* man = this->getManifold();
      const int numcompm = TensorTraits<TensorType>::numModelCoeffs;
      const int numcomp = TensorTraits<TensorType>::numCoeffs;

      if((r==MANIFOLD && numcomp > 9) || (r==MODEL && numcompm > 9)) {
        Msg::Error("Tensor field write not implemented (over 9 components)");
        return 0;
      }

      int numcomp2 = 0;

      for(MElemIt it = man->firstMeshElement();
          it != man->lastMeshElement(); it++) {
        MElement* me = it->first;

        for(int iV = 0; iV < me->getNumVertices(); iV++) {
          Point* p = man->getPoint(me, iV);
          if(p == NULL) continue;

          TensorType x = this->operator()(p);
          numcomp2 = x.getPViewData(data, r, step);

          delete p;
        }
      }
      return numcomp2;
    }

    /// Write Gmsh post-processing file from a multiple tensor fields
    /// in a single Gmsh post-processing view as time steps
    static void writeMSH(std::vector<Field<TensorType>*> fields,
                         std::string filename,
                         Representation r=MODEL,
                         Coordinates coords=MODELCOORDS,
                         GmshFormat format=MSH,
                         std::vector<double> steptimes=std::vector<double>())
    {
      if(fields.empty()) {
        Msg::Error("Tensor fields not provided, tensor fields not written");
        return;
      }
      if(!steptimes.empty() && fields.size() != steptimes.size()) {
        Msg::Error("Not enough timestep times not provided, tensor fields not written");
        return;
      }

      double t1 = Cpu();
      Msg::Info("Writing a tensor fields to file '%s' ... ",
                filename.c_str());

      std::vector<std::map<int, std::vector<double> > > data;

      std::vector<const ManifoldN*> manifolds;

      bool onSameModel = true;
      GModel* gm = fields.at(0)->getManifold()->getModel();
      int numcomp = 0;
      for(unsigned int iF = 0; iF < fields.size(); iF++) {
        Field<TensorType>& f = *fields.at(iF);
        const ManifoldN* man = f.getManifold();
        manifolds.push_back(man);
        if (gm != man->getModel()) onSameModel = false;

        if(!steptimes.empty()) numcomp = f._addDataToWrite(data, r, iF+1);
        else numcomp = f._addDataToWrite(data, r, 1);

      }
      if(numcomp == 0) return;

      std::string postFieldName = GetFileNameWithoutPath(filename);
      if(coords == MANIFOLDCOORDS || !onSameModel)
        gm = ManifoldN::createModel(manifolds, coords);

      double steptime = 0.;
      if(!steptimes.empty()) steptime = steptimes.at(0);
      PView* view = new PView(postFieldName, "ElementNodeData", gm,
                              data.at(0), steptime, numcomp);

      if(!steptimes.empty()) {
        for(unsigned int iF = 1; iF < fields.size(); iF++) {
          double steptime = steptimes.at(iF);
          view->addStep(gm, data.at(iF), steptime, numcomp);
        }
      }

      if(format == POS) view->getData()->writePOS(filename);
      else view->getData()->writeMSH(filename, 3.);
      delete view;
      double t2 = Cpu();
      Msg::Info("Wrote tensor fields (%g s)", t2-t1);
    }



#if defined(HAVE_AVSOLVER)

    /// Modified from 'static void writeMSH' by J. Tampio
    /// Write Gmsh post-processing file from a multiple tensor fields
    /// in a single Gmsh post-processing view as time steps. Modelmap
    /// is updated at each timestep.
    static void writeMSH2(std::vector<Field<TensorType>*> fields,
                          std::string filename,
                          Representation r=MODEL,
                          Coordinates coords=MODELCOORDS,
                          GmshFormat format=MSH,
                          std::vector<double> steptimes=std::vector<double>())
    {
      if(fields.empty()) {
        Msg::Error("Tensor fields not provided, tensor fields not written");
        return;
      }
      if(!steptimes.empty() && fields.size() != steptimes.size()) {
        Msg::Error("Not enough timestep times provided, tensor fields not written");
        return;
      }

      double t1 = Cpu();
      Msg::Info("Writing a tensor fields to file '%s' ... ",
                filename.c_str());

      std::vector<std::map<int, std::vector<double> > > data;

      const int n = TensorTraits<TensorType>::dim;
      std::vector<const ManifoldN*> manifolds;

      bool onSameModel = true;
      GModel* gm = fields.at(0)->getManifold()->getModel();
      int numcomp = 0;
      for(unsigned int iF = 0; iF < fields.size(); iF++) {
        Field<TensorType>& f = *fields.at(iF);
        const Manifold<n>* man = f.getManifold();
        manifolds.push_back(man);
        if (gm != man->getModel()) onSameModel = false;

        if(!steptimes.empty()) numcomp = f._addDataToWrite(data, r, iF+1);
        else numcomp = f._addDataToWrite(data, r, 1);

      }
      if(numcomp == 0) return;

      std::string postFieldName = GetFileNameWithoutPath(filename);
      if(coords == MANIFOLDCOORDS || !onSameModel)
        gm = ManifoldN::createModel(manifolds, coords, false, (void*)(&steptimes));

      double steptime = 0.;
      if(!steptimes.empty()) steptime = steptimes.at(0);
      PView* view = new PView(postFieldName, "ElementNodeData", gm,
                              data.at(0), steptime, numcomp);

      if(!steptimes.empty()) {
        for(unsigned int iF = 1; iF < fields.size(); iF++) {
          double steptime = steptimes.at(iF);
          view->addStep(gm, data.at(iF), steptime, numcomp);
        }
      }

      double gmsh_version = 3.;
      bool append = true;

      if(format == POS) view->getData()->writePOS(filename, false, false, append);
      else view->getData()->writeMSH(filename, gmsh_version);
      delete view;
      double t2 = Cpu();
      Msg::Info("Wrote tensor fields (%g s)", t2-t1);
    }
#endif



    /// Write Gmsh post-processing file from a single field
    void writeMSH(std::string filename,
                  Representation r=MODEL, Coordinates coords=MODELCOORDS,
                  GmshFormat format=MSH) const
    {
      double t1 = Cpu();
      Msg::Info("Writing a tensor field to file '%s' ... ", filename.c_str());

      const int n = TensorTraits<TensorType>::dim;
      const Manifold<n>* man = this->getManifold();
      std::vector<std::map<int, std::vector<double> > > data;

      int numcomp = _addDataToWrite(data, r, 1);
      if(numcomp == 0) return;

      double steptime = 0.;
      std::string postFieldName = GetFileNameWithoutPath(filename);
      GModel* gm;
      if(coords == MANIFOLDCOORDS) gm = man->createModel();
      else gm = man->getModelMap()->getModel();
      PView* view = new PView(postFieldName, "ElementNodeData", gm,
                              data.at(0), steptime, numcomp);
      if(format == POS) view->getData()->writePOS(filename);
      else view->getData()->writeMSH(filename, 3.);
      delete view;
      double t2 = Cpu();
      Msg::Info("Wrote tensor field (%g s)", t2-t1);
    }

    void writePOS(std::string filename, Representation r=MODEL,
                  Coordinates coords=MODELCOORDS) const
    {
      this->writeMSH(filename, r, coords, POS);
    }


    /// Read a Gmsh post-processing .msh-file
    virtual bool readMSH(std::string filename,
                         Representation r=MODEL,
                         int step=0, GmshFormat format=MSH)
    {
      Msg::Error("Cannot read a tensor field to this type of field.");
      return false;
    }
    virtual bool readPOS(std::string filename,
                         Representation r=MODEL,
                         int step=0)
    {
      return this->readMSH(filename, r, step, POS);
    }

    virtual ~Field() {};
  };

  /// Tensor field represented as coefficient arrays at mesh element vertices
  template <class TensorType>
  class MeshField : public Field<TensorType>
  {
  protected:

    /// Tensor field coefficients:
    /// [Mesh element num][Mesh element vertex index][Tensor coefficients]
    std::vector< std::vector< std::vector<double> > > _data;

    void _setData(const double* c, unsigned int elemNum,
                  unsigned int verIndex)
    {
      if(elemNum >= _data.size()) _data.resize(elemNum+1);
      if(verIndex >= _data.at(elemNum).size())
        _data.at(elemNum).resize(verIndex+1);
      std::vector<double> coeffs(TensorTraits<TensorType>::numCoeffs);
      coeffs.assign(c, c+TensorTraits<TensorType>::numCoeffs);
      _data[elemNum][verIndex] = coeffs;
    }

    void inline _getData(double c[TensorTraits<TensorType>::numCoeffs],
                         unsigned int elemNum, unsigned int verIndex) const
    {
      if(elemNum >= _data.size() || verIndex >= _data.at(elemNum).size()) {
        const int nc = TensorTraits<TensorType>::numCoeffs;
        vecinit<nc>(c);
        return;
      }
      for(unsigned int i = 0; i < _data.at(elemNum).at(verIndex).size(); i++)
        c[i] = _data[elemNum][verIndex][i];
    }

  public:
    /// Uninitialized tensor field
  MeshField() : Field<TensorType>() {}

    /// Copy constructor
    MeshField(const Field<TensorType>& f) :
      Field<TensorType>(f.getManifold())
    {
      const int n = TensorTraits<TensorType>::dim;
      const Manifold<n>* man = this->getManifold();
      for(MElemIt it = man->firstMeshElement();
          it != man->lastMeshElement(); it++) {
        MElement* me = it->first;
        for(int iV = 0; iV < me->getNumVertices(); iV++) {
          Point* p = man->getPoint(me, iV);
          this->assign(f(p));
          delete p;
        }
      }
    }

    /// Empty tensor field, returns zero tensor everywhere
    MeshField(const Manifold<TensorTraits<TensorType>::dim>* m) :
      Field<TensorType>(m) {}

    /// Get tensor coefficients at a point
    virtual void getCoeffs(const Point* p,
			   double c[TensorTraits<TensorType>::numCoeffs]
                           ) const
    {
      MElement* me = p->getMeshElement();
      if(p->getVertex() != -1) {
        this->_getData(c, me->getNum(), p->getVertex());
        return;
      }
      const int nc = TensorTraits<TensorType>::numCoeffs;
      vecinit<nc>(c);
      double sf[1256];
      double u, v, w;
      p->getUVW(u, v, w);
      me->getShapeFunctions(u, v, w, sf);
      for(int i = 0; i < me->getNumShapeFunctions(); i++) {
	double w = sf[i];
	if(w == 0.) continue;
	double ci[nc];
	this->_getData(ci, me->getNum(), i);
	for(int j = 0; j < nc; j++) c[j] += ci[j]*w;
      }
    }

    /// Get pointer to nodal values at a mesh element
    const std::vector< std::vector<double> >*
    getPointerToData(MElement* me) const
    {
      return &_data.at(me->getNum());
    }

    /// Get tensor at a point
    virtual void getValue(const Point* p, TensorType& tp) const
    {
      double c[TensorTraits<TensorType>::numCoeffs];
      this->getCoeffs(p, c);
      tp = TensorType(this->getManifold(), p, c, MANIFOLD);
    }

    /// Assign coefficients at mesh element vertex
    void assign(const double* c, MElement* me, int verIndex)
    {
      this->_setData(c, me->getNum(), verIndex);
    }

    /// Assign value at a point
    /// (if not at a vertex will assing to all vertices)
    void assign(const TensorType& x)
    {
      int v = x.getPoint()->getVertex();
      if(v == -1) {
        Msg::Warning("Assingning field value to all vertices of a mesh element.");
        MElement* me = x.getPoint()->getMeshElement();
        for(int iV = 0; iV < me->getNumVertices(); iV++)
          this->assign(x.getPointerToCoeffs(), me, iV);
      }
      else this->assign(x.getPointerToCoeffs(),
                        x.getPoint()->getMeshElement(), v);
    }

    /// Read tensor field from a file
    virtual bool readMSH(std::string filename,
                         Representation r=MODEL,
                         int step=0, GmshFormat format=MSH)
    {
      double t1 = Cpu();
      Msg::Info("Reading a tensor field from file '%s' ... ",
                filename.c_str());

      const int n = TensorTraits<TensorType>::dim;
      const int nc = TensorTraits<TensorType>::numCoeffs;
      const int nmc = TensorTraits<TensorType>::numModelCoeffs;
      const Manifold<n>* man = this->getManifold();
      if(man == NULL) {
        Msg::Error("Manifold of the field is not defined");
        return false;
      }
      GModel* gm = man->getModelMap()->getModel();

      bool read = false;
      if(format == POS) read = PView::readPOS(filename);
      else read = PView::readMSH(filename);
      PView* view = PView::list.back();
      if(view == NULL || !read) {
        Msg::Error("Could not read a tensor field from file '%s'.",
                   filename.c_str());
        return false;
      }
      /*
      PView* view = PView::getViewByName(postFieldName);
      if(view == NULL) {
        Msg::Error("Could not read a tensor field: Field '%s' not found from file '%s'.",
                   postFieldName.c_str(), filename.c_str());
        return false;
        }*/
      PViewData* data = view->getData();
      if(data == NULL) {
        Msg::Error("Could not read a tensor field: empty data.");
        return false;
      }

      _data.clear();
      for(int ent = 0; ent < data->getNumEntities(step); ent++){
        for(int ele = 0; ele < data->getNumElements(step, ent); ele++){
          MElement* me = data->getElement(step, ent, ele);
          if(!man->containsMeshElement(me)) continue;
          //printf("entity %d, ele %d menum %d \n", ent, ele, me->getNum());

          if(data->skipElement(step, ent, ele)) continue;
          int numComp = data->getNumComponents(step, ent, ele);
          int numNodes = me->getNumVertices();
          // Gmsh PView interface does not support high order elements?
          //int numNodes = data->getNumNodes(step, ent, ele);

          for(int nod = 0; nod < numNodes; nod++){
            double c[MAXCOMP];
            for(int comp = 0; comp < numComp; comp++) {
              //printf("entity %d ele %d menum %d, numnodes %d numcomp %d node %d comp %d\n", ent, ele, me->getNum(), numNodes, numComp, nod, comp);
              data->getValue(step, ent, ele, nod, comp, c[comp]);
            }
            Point* p = new Point(gm, me, nod);
            if(r == MANIFOLD && numComp != nc) {
              Msg::Warning("Not enough coefficients available for a tensor field in %s.", filename.c_str());
              double cc[nc];
              for(int i = 0; i < numComp; i++) cc[i] = c[i];
              for(int i = numComp; i < nc; i++) cc[i] = 0.;
              TensorType x(man, p, cc, r);
              this->assign(x);
            }
            else if(r == MODEL && numComp != nmc) {
              Msg::Warning("Not enough coefficients available for a tensor field in %s.", filename.c_str());
              double cc[nc];
              for(int i = 0; i < numComp; i++) cc[i] = c[i];
              for(int i = numComp; i < nmc; i++) cc[i] = 0.;
              TensorType x(man, p, cc, r);
              this->assign(x);
            }
            else {
              TensorType x(man, p, c, r);
              this->assign(x);
            }
            delete p;
          }
        }
      }

      double t2 = Cpu();
      Msg::Info("Read a tensor field (%g s)", t2-t1);
      return true;
    }


  };

  /// Abstract base for tensor field defined by a derived class
  template <class TensorType>
  class DefinedField : public Field<TensorType>
  {
  protected:

  public:
    DefinedField(const Manifold<TensorTraits<TensorType>::dim>* m) :
      Field<TensorType>(m) {}

    /// Overdrive this to define a tensor field
    virtual void getValue(const Point* p, TensorType& tp) const
    {
      Msg::Error("Field is not defined.");
      const int  nc = TensorTraits<TensorType>::numCoeffs;
      double c[nc];
      vecinit<nc>(c);
      tp = TensorType(this->getManifold(), p, c, MANIFOLD);
    }
  };




}

#endif
