// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#ifndef _RM_FIELD_FUNCTIONS_H_
#define _RM_FIELD_FUNCTIONS_H_

#include <queue>
#include "RiemannianManifold.h"
#include "ManifoldMap.h"
#include "TensorFunctions.h"
#include "Utils.h"

namespace rm
{

  /// Function template for unary operation of tensor field
  template <class TensorType1, class TensorType2>
  MeshField<TensorType2>
  unaryOperation(const Field<TensorType1>& f,
                 const UnaryFunction<TensorType1, TensorType2>& op)
  {
    const int n2 = TensorTraits<TensorType2>::dim;
    const Manifold<n2>* man = f.getManifold();
    MeshField<TensorType2> res(man);

    for(MElemIt it = man->firstMeshElement();
        it != man->lastMeshElement(); it++) {
      MElement* me = it->first;
      if(!f.inSupport(me)) continue;
      for(int iV = 0; iV < me->getNumVertices(); iV++) {
        Point* p = man->getPoint(me, iV);
        TensorType2 x = op(f(p));
        res.assign(x);
        delete p;
      }
    }
    return res;
  }

  /// Function template for binary operation of tensor fields
  template <class TensorType1, class TensorType2, class TensorType3>
  MeshField<TensorType3>
  binaryOperation(const Field<TensorType1>& f1,
                  const Field<TensorType2>& f2,
                  const BinaryFunction<TensorType1,
                                       TensorType2,
                                       TensorType3>& op)
  {
    const int n3 = TensorTraits<TensorType3>::dim;
    const Manifold<n3>* man = f1.getManifold();
    MeshField<TensorType3> res(man);

    for(MElemIt it = man->firstMeshElement();
        it != man->lastMeshElement(); it++) {
      MElement* me = it->first;
      if(op.getSupportType() == SUPP_INTERSECTION &&
         (!f1.inSupport(me) || !f2.inSupport(me))) continue;
      else if(op.getSupportType() == SUPP_UNION &&
         (!f1.inSupport(me) && !f2.inSupport(me))) continue;
      for(int iV = 0; iV < me->getNumVertices(); iV++) {
        Point* p = man->getPoint(me, iV);
        TensorType3 x = op(f1(p), f2(p));
        res.assign(x);
        delete p;
      }
    }
    return res;
  }

  /// Integrate a k-covector field over k-manifold
  /// with given integration order
  template <int n, int k>
  double i(const Field<Covector<n,k> >& f, const Manifold<k>* m,
           int pOrder=-1)
  {
    double sum = 0.;
    bool success = true;
    for(MElemIt it = m->firstMeshElement();
        it != m->lastMeshElement(); it++) {
      MElement* me = it->first;
      if(pOrder == -1) pOrder = 2*me->getPolynomialOrder() + 1;
      double o = it->second;
      double mesum = 0.;
      ManifoldIntegrationPoints<k> ips(m, me, pOrder);
      for(int i = 0; i < ips.getNumPoints(); i++) {
        Point* ip = ips.getPoint(i);
        double weight = ips.getWeight(i);
        if(f.getManifold()->getNum() != m->getNum()) {
          ManifoldMap<n, k> mm(f.getManifold(), m);
          const Point* xdomp = mm.mapPointToDom(ip);
          if(xdomp != NULL) {
            Covector<n,k> x = f(xdomp);
            Covector<k,k> y(m, ip);
            if(!mm.transformTensorToCod(x,y)) success = false;
            mesum += y(0)*weight*o;
            delete xdomp;
          }
        }
        else {
          Covector<n,k> x = f(ip);
          mesum += x(0)*weight*o;
        }
      }
      sum += mesum;
    }
    if(!success)
      Msg::Error("Integration was not successful, could not pullback %d-covector field on %d-manifold to given %d-manifold", k, n, k);
    return sum;
  }

  static int getTriangleType (int order)
  {
    switch(order) {
    case 0 : return MSH_TRI_1;
    case 1 : return MSH_TRI_3;
    case 2 : return MSH_TRI_6;
    case 3 : return MSH_TRI_10;
    case 4 : return MSH_TRI_15;
    case 5 : return MSH_TRI_21;
    case 6 : return MSH_TRI_28;
    case 7 : return MSH_TRI_36;
    case 8 : return MSH_TRI_45;
    case 9 : return MSH_TRI_55;
    case 10 : return MSH_TRI_66;
    default : Msg::Error("triangle order %i unknown", order); return 0;
    }
  }

  static int getQuadType (int order)
  {
    switch(order) {
    case 0 : return MSH_QUA_1;
    case 1 : return MSH_QUA_4;
    case 2 : return MSH_QUA_9;
    case 3 : return MSH_QUA_16;
    case 4 : return MSH_QUA_25;
    case 5 : return MSH_QUA_36;
    case 6 : return MSH_QUA_49;
    case 7 : return MSH_QUA_64;
    case 8 : return MSH_QUA_81;
    case 9 : return MSH_QUA_100;
    case 10 : return MSH_QUA_121;
    default : Msg::Error("quad order %i unknown", order); return 0;
    }
  }

  static int getLineType (int order)
  {
    switch(order) {
    case 0 : return MSH_LIN_1;
    case 1 : return MSH_LIN_2;
    case 2 : return MSH_LIN_3;
    case 3 : return MSH_LIN_4;
    case 4 : return MSH_LIN_5;
    case 5 : return MSH_LIN_6;
    case 6 : return MSH_LIN_7;
    case 7 : return MSH_LIN_8;
    case 8 : return MSH_LIN_9;
    case 9 : return MSH_LIN_10;
    case 10 : return MSH_LIN_11;
    default : Msg::Error("line order %i unknown", order); return 0;
    }
  }

  /// Integrate a k-covector field over k-dim mesh element
  /// with given integration order
  template <int n, int k>
  double i(const Field<Covector<n,k> >& f, MElement* me,
           int subme=-1, int pOrder=-1)
  {
    bool newme = false;
    if(me->getDim() != k) {
      if(subme == -1) {
        Msg::Error("Cannot integrate %d-covector field over %d-dimensional mesh element", k, me->getDim());
        return 0;
      }
      std::vector<MVertex*> v;
      MElementFactory mf;
      if(k == 0) {
        if(subme == -1) subme = 0;
        Point* p = f.getManifold()->getPoint(me, subme);
        double temp = f(p)(0);
        delete p;
        return temp;
      }
      else if(k == 1) {
        me->getEdgeVertices(subme, v);
        int type = getLineType(me->getPolynomialOrder());
        me = mf.create(type, v);
        newme = true;
      }
      else if(k == 2) {
        me->getFaceVertices(subme, v);
        int type = 0;
        switch(me->getType()) {
        case TYPE_TRI :
          type = getTriangleType(me->getPolynomialOrder()); break;
        case TYPE_TET :
          type = getTriangleType(me->getPolynomialOrder()); break;
        case TYPE_QUA :
          type = getQuadType(me->getPolynomialOrder()); break;
        case TYPE_HEX :
          type = getQuadType(me->getPolynomialOrder()); break;
        case TYPE_PYR :
          if(subme < 4) type = getTriangleType(me->getPolynomialOrder());
          else type = getQuadType(me->getPolynomialOrder());
          break;
        case TYPE_PRI :
          if(subme < 2) type = getTriangleType(me->getPolynomialOrder());
          else type = getQuadType(me->getPolynomialOrder());
          break;
        default: type = 0; break;
        }
        me = mf.create(type, v);
        newme = true;
      }
      else return 0;
    }

    SmoothManifold<k> m(f.getManifold()->getModel(), me);
    double result = i(f, &m, pOrder);
    if(newme) delete me;
    return result;
  }

#if defined(HAVE_KBIPACK)
  /// Integrate a k-covector field over a k-chain
  /// with given integration order
  template <int n, int k, class C>
  double i(const Field<Covector<n,k> >& f, const Chain<C> c, int pOrder=-1)
  {
    double result = 0;
    if(c.getDim() != k) {
      Msg::Error("Cannot integrate %d-covector field over a %d-chain",
                 k, c.getDim());
      return 0;
    }

    for(typename Chain<C>::cecit it = c.firstElemChain();
        it != c.lastElemChain(); it++) {
      MElement* me = it->first.createMeshElement();
      SmoothManifold<k> m(f.getManifold()->getModel(), me);
      result += i(f, &m, pOrder)*it->second;
      delete me;
    }
    return result;
  }
#endif

  /// Compute integral of 1-covector field f
  /// from vertex v0 to every other vertex on the manifold of field f.
  /// Creates a 0-covector field from the result, whose value
  /// at vertex v0 equals the given offset.
  /// (df = 0 should hold, for the integral to be *locally* path-independent)
  template <int n>
  MeshField<Covector<n, 0> > i(const Field<Covector<n,1> >& f,
                               MVertex* v0=NULL, double offset=0.)
  {
    const Manifold<n>* m = f.getManifold();
    if(m->firstMeshElement()->first->getPolynomialOrder() != 1) {
      Msg::Warning("This algorithm does not support high order mesh");
    }
    std::map<MVertex*,
             std::set<MVertex*, MVertexLessThanNum>,
             MVertexLessThanNum> neighbors;
    for(MElemIt it = m->firstMeshElement();
      it != m->lastMeshElement(); it++) {
      MElement* me = it->first;
      for(int iE = 0; iE < me->getNumEdges(); iE++) {
        std::vector<MVertex*> v;
        me->getEdgeVertices(iE, v);
        neighbors[v[0]].insert(v[1]);
        neighbors[v[1]].insert(v[0]);
      }
    }

    MElementFactory ef;
    std::queue<MVertex*> Q;
    std::set<MVertex*, MVertexLessThanNum> Qset;
    std::map<MVertex*, double, MVertexLessThanNum> intf;
    if(v0 == NULL) {
      intf[neighbors.begin()->first] = offset;
      Q.push(neighbors.begin()->first);
      Qset.insert(neighbors.begin()->first);
    }
    else {
      intf[v0] = offset;
      Q.push(v0);
      Qset.insert(v0);
    }

    while(!Q.empty()) {
      MVertex* ver = Q.front();
      Q.pop();
      Qset.erase(ver);

      for(std::set<MVertex*, MVertexLessThanNum>::iterator
            it = neighbors[ver].begin(); it != neighbors[ver].end(); it++) {
        if(!intf.count(*it)) {
          std::vector<MVertex*> vertices(2,NULL);
          vertices[0] = ver;
          vertices[1] = *it;
          MElement* me = ef.create(MSH_LIN_2, vertices);
          intf[*it] = intf[ver] + rm::i(f, me);
          delete me;
          std::set<MVertex*, MVertexLessThanNum>::iterator
            find = Qset.find(*it);
          if(find == Qset.end()) {
            Qset.insert(*it);
            Q.push(*it);
          }
        }
      }
    }

    MeshField<Covector<n,0> > intfield(m);
    for(MElemIt it = m->firstMeshElement();
        it != m->lastMeshElement(); it++) {
      MElement* me = it->first;
      for(int iV = 0; iV < me->getNumVertices(); iV++) {
        double c[1] = { intf[me->getVertex(iV)] };
        intfield.assign(c, me, iV);
      }
    }
    return intfield;
  }

  /// Pullback of a (k,l)-tensor to a m-manifold
  template <int n, int m, int k, int l, TensorSymmetry s, int h>
  MeshField<Tensor<m,k,l,s,h> > pullback(const Field<Tensor<n,k,l,s,h> >& f,
                                         const Manifold<m>* man)
  {
    const Manifold<n>* manf = f.getManifold();
    MeshField<Tensor<m,k,l,s,h> > g(man);

    if(manf->getNumMeshElements() < man->getNumMeshElements()) {
      ManifoldMap<n,m> mm(manf, man);
      if(mm.transformFieldToCod(f, g)) return g;
    }
    else {
      ManifoldMap<m,n> mm(man, manf);
      if(mm.transformFieldToDom(f, g)) return g;
    }
    Msg::Error("Cannot transform field to a manifold");
    return g;
  }

  /// Pushforward of a (k,0)-tensor to a m-manifold
  template <int n, int m, int k, TensorSymmetry s, int h>
  MeshField<Tensor<m,k,0,s,h> > pushforward(const Field<Tensor<n,k,0,s,h> >& f,
                                            const Manifold<m>* man)
  {
    return pullback(f,man);
  }

  /// Pullback k-covetor field from n-manifold to m-manifold
  template <int n, int m, int k>
  MeshField<Covector<m,k> > pullback(const Field<Covector<n,k> >& f,
                                     const Manifold<m>* man)
  {
    const Manifold<n>* manf = f.getManifold();
    MeshField<Covector<m,k> > g(man);

    if(manf->getNumMeshElements() < man->getNumMeshElements()) {
      ManifoldMap<n,m> mm(manf, man);
      if(mm.transformFieldToCod(f, g)) return g;
    }
    else {
      ManifoldMap<m,n> mm(man, manf);
      if(mm.transformFieldToDom(f, g)) return g;
    }
    Msg::Error("Cannot transform field to a manifold");
    return g;
  }

  /// Pullback metric tensor field from n-manifold to m-manifold
  template <int n, int m>
  MeshField<MetricTensor<m> > pullback(const Field<MetricTensor<n> >& f,
                                       const Manifold<m>* man)
  {
    const Manifold<n>* manf = f.getManifold();
    MeshField<MetricTensor<m> > g(man);

    if(manf->getNumMeshElements() < man->getNumMeshElements()) {
      ManifoldMap<n,m> mm(manf, man);
      if(mm.transformFieldToCod(f, g)) return g;
    }
    else {
      ManifoldMap<m,n> mm(man, manf);
      if(mm.transformFieldToDom(f, g)) return g;
    }
    Msg::Error("Cannot transform field to a manifold");
    return g;
  }

  /// Pullback linear transformation field from n-manifold to m-manifold
  template <int n, int m>
  MeshField<LinTransform<m> > pullback(const Field<LinTransform<n> >& f,
                                       const Manifold<m>* man)
  {
    const Manifold<n>* manf = f.getManifold();
    MeshField<LinTransform<m> > g(man);

    if(manf->getNumMeshElements() < man->getNumMeshElements()) {
      ManifoldMap<n,m> mm(manf, man);
      if(mm.transformFieldToCod(f, g)) return g;
    }
    else {
      ManifoldMap<m,n> mm(man, manf);
      if(mm.transformFieldToDom(f, g)) return g;
    }
    Msg::Error("Cannot transform field to a manifold");
    return g;
  }

  /// Pushforward k-vector field from n-manifold to m-manifold
  template <int n, int m, int k>
  MeshField<Vector<m,k> > pushforward(const Field<Vector<n,k> >& f,
                                      const Manifold<m>* man)
  {
    const Manifold<n>* manf = f.getManifold();
    MeshField<Vector<m,k> > g(man);


    if(manf->getNumMeshElements() < man->getNumMeshElements()) {
      ManifoldMap<n,m> mm(manf, man);
      if(mm.transformFieldToCod(f, g)) return g;
    }
    else {
      ManifoldMap<m,n> mm(man, manf);
      if(mm.transformFieldToDom(f, g)) return g;
    }
    Msg::Error("Cannot transform field to a manifold");
    return g;
  }

  /// Produce i:th component field of a tensor field
  template <class TensorType>
  MeshField<Covector<TensorTraits<TensorType>::dim,0> >
  compField(const Field<TensorType>& f, int i)
  {
    const int n = TensorTraits<TensorType>::dim;
    const int nc = TensorTraits<TensorType>::numCoeffs;
    const Manifold<n>* man = f.getManifold();
    MeshField<Covector<n,0> > fi(man);

    if(i > nc || i < 0) return fi;

    for(MElemIt it = man->firstMeshElement();
        it != man->lastMeshElement(); it++) {
      MElement* me = it->first;
      for(int iV = 0; iV < me->getNumVertices(); iV++) {
        Point* p = man->getPoint(me, iV);
        double c[1] = {0.};
        c[0] = f(p)(i);
        fi.assign(c, me, iV);
        delete p;
      }
    }
    return fi;
  }

  /// Produce I:th component field of a tensor field
  template <class TensorType>
  MeshField<Covector<TensorTraits<TensorType>::dim,0> >
  compField(const Field<TensorType>& f, const MultiIndex& I)
  {
    const int n = TensorTraits<TensorType>::dim;
    const int k = TensorTraits<TensorType>::ctrIndex;
    const int l = TensorTraits<TensorType>::coIndex;
    const TensorSymmetry s = TensorTraits<TensorType>::symm;
    if(s == SKEWSYMMETRIC) {
      int perm = 0;
      int i = I.skewSymmetricTensorIndex(n, perm);
      return perm*compfield(f, i);
    }
    else if(s == SYMMETRIC && k+l == 2 && l != k) {
      int i = I.symmR2TensorIndex(n);
      return compfield(f, i);
    }
    else if(s == FULL && k == 1 && l == 1) {
      int i = I.fullTensorIndex(n);
      return compfield(f, i);
    }
    else {
      return compfield(f, -1);
    }
  }

  /// Sum of tensor fields
  template <class TensorType>
  inline MeshField<TensorType> operator+(const Field<TensorType>& f1,
                                         const Field<TensorType>& f2)
  {
    Sum<TensorType> sum;
    return binaryOperation(f1, f2, sum);
  }

  /// Scalar multiplication of tensor fields
  template <class TensorType>
  MeshField<TensorType> operator*(const Field<TensorType>& f, const double s)
  {
    const int n = TensorTraits<TensorType>::dim;
    const Manifold<n>* man = f.getManifold();
    MeshField<TensorType> res(man);

    for(MElemIt it = man->firstMeshElement();
        it != man->lastMeshElement(); it++) {
      MElement* me = it->first;
      for(int iV = 0; iV < me->getNumVertices(); iV++) {
        Point* p = man->getPoint(me, iV);
        TensorType x = f(p)*s;
        res.assign(x);
        delete p;
      }
    }
    return res;
  }

  template <class TensorType>
  MeshField<TensorType> operator*
  (const Field<TensorType>& f,
   const Field<Covector<TensorTraits<TensorType>::dim,0> >& s)
  {
    const int n = TensorTraits<TensorType>::dim;
    const Manifold<n>* man = f.getManifold();
    MeshField<TensorType> res(man);

    for(MElemIt it = man->firstMeshElement();
        it != man->lastMeshElement(); it++) {
      MElement* me = it->first;
      for(int iV = 0; iV < me->getNumVertices(); iV++) {
        Point* p = man->getPoint(me, iV);
        TensorType x = f(p)*(s(p)(0));
        res.assign(x);
        delete p;
      }
    }
    return res;
  }


  /// Derived vector space operations of tensor fields
  template <class TensorType>
  inline MeshField<TensorType> operator*(const double s,
                                         const Field<TensorType>& f)
  {
    return f*s;
  }

  template <class TensorType>
  inline MeshField<TensorType> operator*
  (const Field<Covector<TensorTraits<TensorType>::dim,0> >& s,
   const Field<TensorType>& f)
  {
    return f*s;
  }

  template <class TensorType>
  inline MeshField<TensorType> operator-(const Field<TensorType>& f)
  {
    return f*(-1.);
  }
  template <class TensorType>
  inline MeshField<TensorType> operator/(const Field<TensorType>& f,
                                         const double s)
  {
    return f*(1./s);
  }
  template <class TensorType>
  MeshField<TensorType> operator/
  (const Field<TensorType>& f,
   const Field<Covector<TensorTraits<TensorType>::dim,0> >& s)
  {
    return f*(1./s);
  }
  template <class TensorType>
  inline MeshField<TensorType> operator-(const Field<TensorType>& f1,
                                         const Field<TensorType>& f2)
  {
    MeshField<TensorType> temp = -f2;
    return f1+temp;
  }

  /// Inner product of k-covector fields
  template <int n, int k>
  MeshField<Covector<n, 0> >
  operator*(const Field<Covector<n, k> >& f1,
            const Field<Covector<n, k> >& f2)
  {
    InnerProduct<Covector<n, k>, Covector<n, k>, Covector<n, 0> > inprod;
    return binaryOperation(f1, f2, inprod);
  }

  /// Inner product of k-vector fields
  template <int n, int k>
  MeshField<Covector<n, 0> >
  operator*(const Field<Vector<n, k> >& f1,
            const Field<Vector<n, k> >& f2)
  {
    InnerProduct<Vector<n, k>, Vector<n, k>, Covector<n, 0> > inprod;
    return binaryOperation(f1, f2, inprod);
  }

  /// Exterior product of k-covector fields
  template <int n, int k1, int k2>
  MeshField<Covector<n, k1+k2> >
  operator%(const Field<Covector<n, k1> >& f1,
            const Field<Covector<n, k2> >& f2)
  {
    ExteriorProduct<Covector<n, k1>,
                    Covector<n, k2>,
                    Covector<n, k1+k2> > extprod;
    return binaryOperation(f1, f2, extprod);
  }

  /// Exterior product of k-vector fields
  template <int n, int k1, int k2>
  MeshField<Vector<n, k1+k2> >
  operator%(const Field<Vector<n, k1> >& f1,
            const Field<Vector<n, k2> >& f2)
  {
    ExteriorProduct<Vector<n, k1>,
                    Vector<n, k2>,
                    Vector<n, k1+k2> > extprod;
    return binaryOperation(f1, f2, extprod);
  }

  /// Hodge operator of k-covector field
  template <int n, int k>
  MeshField<Covector<n, n-k> >
  inline operator*(const Field<Covector<n, k> >& f)
  {
    Hodge<Covector<n,k>, Covector<n,n-k> > hodge;
    return unaryOperation(f, hodge);
  }
  template <int n, int k>
  MeshField<Covector<n, n-k> >
  inline h(const Field<Covector<n, k> >& f)
  {
    Hodge<Covector<n,k>, Covector<n,n-k> > hodge;
    return unaryOperation(f, hodge);
  }

  /// Hodge operator of k-vector field
  template <int n, int k>
  MeshField<Vector<n, n-k> >
  inline operator*(const Field<Vector<n, k> >& f)
  {
    Hodge<Vector<n,k>, Vector<n,n-k> > hodge;
    return unaryOperation(f, hodge);
  }
  template <int n, int k>
  MeshField<Vector<n, n-k> >
  inline h(const Field<Vector<n, k> >& f)
  {
    Hodge<Vector<n,k>, Vector<n,n-k> > hodge;
    return unaryOperation(f, hodge);
  }

  /// Flat operator of k-vector field
  template <int n, int k>
  MeshField<Covector<n, k> >
  inline operator~(const Field<Vector<n, k> >& f)
  {
    Musical<Vector<n,k>, Covector<n,k> > flat;
    return unaryOperation(f, flat);
  }

  /// Sharp operator of k-covector field
  template <int n, int k>
  MeshField<Vector<n, k> >
  inline operator~(const Field<Covector<n, k> >& f)
  {
    Musical<Covector<n,k>, Vector<n,k> > sharp;
    return unaryOperation(f, sharp);
  }

  /// Symmetric tensor product of 1-covector fields
  /// 1/2*((f1 times f2) + (f2 times f1))
  template <int n>
  MeshField<MetricTensor<n> >
  symtprod(const Field<Covector<n, 1> >& f1,
           const Field<Covector<n, 1> >& f2)
  {
    SymTensorProduct11<Covector<n,1>, Covector<n,1>, MetricTensor<n> > tprod;
    return binaryOperation(f1, f2, tprod);
  }

  /// Linear transformation of 1-covector field
  template <int n>
  MeshField<Covector<n,1> >
  operator*(const Field<LinTransform<n> >& t,
            const Field<Covector<n,1> >& f)
  {
    LinearTransformation<LinTransform<n>, Covector<n,1>, Covector<n,1> > lt;
    return binaryOperation(t, f, lt);
  }

  /// Linear transformation of 1-vector field
  template <int n>
  MeshField<Vector<n,1> >
  operator*(const Field<LinTransform<n> >& t,
            const Field<Vector<n,1> >& f)
  {
    LinearTransformation<LinTransform<n>, Vector<n,1>, Vector<n,1> > lt;
    return binaryOperation(t, f, lt);
  }

}

#endif
