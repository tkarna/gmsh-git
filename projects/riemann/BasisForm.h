// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics


#ifndef _RM_BASISFORM_H_
#define _RM_BASISFORM_H_

#include "Manifold.h"
#include "Field.h"
#include "ManifoldPartOfUnity.h"

namespace rm {

  template<int n, int k>
  class BasisForm : public DefinedField<Covector<n, k> > {

   private:
    int _order;

    void _changeOrientation(const Point* p, double* coeffs,
                            Orientation o, int index) const
    {
      if(o == GLOBALOR) {
        if(k == 1) {
          MEdge e = p->getMeshElement()->getEdge(index);
          if(e.getMinVertex() != e.getVertex(0)) {
            for(int i = 0; i < 3; i++) coeffs[i] *= -1;
          }
        }
        else if (k == 2) {
          int perm = 1;
          MFace f = p->getMeshElement()->getFace(index);
          if(f.getSortedVertex(0) != f.getVertex(0)) {
            perm *= -1;
          }
          if(f.getSortedVertex(2) != f.getVertex(2) &&
             f.getSortedVertex(2) != f.getVertex(0)) {
            perm *= -1;
          }
          if(perm != 1) for(int i = 0; i < 3; i++) coeffs[i] *= -1;
        }
        else if (k == 3) {
          int perm = p->getMeshElement()->getVolumeSign();
          if(perm != 1) for(int i = 0; i < 3; i++) coeffs[i] *= -1;
        }
        else {
          Msg::Error("Not implemented");
        }
      }
    }


   public:
    BasisForm(const Manifold<n>* m, int order=1) :
      DefinedField<Covector<n, k> > (m)
    {
      _order = 1;
      if(order > 1) {
        Msg::Debug("High order Whitney forms not implemented");
      }
    }

    void getCovectors(const Point* p, Orientation o,
                      std::vector<Covector<n,k> >& sf) const
    {
      sf.clear();
      int type = p->getMeshElement()->getType();
      double u,v,w;
      p->getUVW(u,v,w);

      double coeffs[3] = {0., 0., 0.};


      if (k == 1) {
        if(type == TYPE_LIN) {
          coeffs[0] = -0.5;
          _changeOrientation(p, coeffs, o, 0);
          sf.push_back(Covector<n,k>(this->getManifold(), p,
                                     coeffs, ELEMENT));
        }
        else if(type == TYPE_TRI) {
          coeffs[0] = 1-v;
          coeffs[1] = u;
          _changeOrientation(p, coeffs, o, 0);
          sf.push_back(Covector<n,k>(this->getManifold(), p,
                                     coeffs, ELEMENT));
          coeffs[0] = -v;
          coeffs[1] = u;
          _changeOrientation(p, coeffs, o, 1);
          sf.push_back(Covector<n,k>(this->getManifold(), p,
                                     coeffs, ELEMENT));
          coeffs[0] = -v;
          coeffs[1] = u-1;
          _changeOrientation(p, coeffs, o, 2);
          sf.push_back(Covector<n,k>(this->getManifold(), p,
                                     coeffs, ELEMENT));
        }
        else if(type == TYPE_TET) {
          coeffs[0] = 1-v-w;
          coeffs[1] = u;
          coeffs[2] = u;
          _changeOrientation(p, coeffs, o, 0);
          sf.push_back(Covector<n,k>(this->getManifold(), p,
                                     coeffs, ELEMENT));
          coeffs[0] = -v;
          coeffs[1] = u;
          coeffs[2] = 0;
          _changeOrientation(p, coeffs, o, 1);
          sf.push_back(Covector<n,k>(this->getManifold(), p,
                                     coeffs, ELEMENT));
          coeffs[0] = -v;
          coeffs[1] = u+w-1;
          coeffs[2] = -v;
          _changeOrientation(p, coeffs, o, 2);
          sf.push_back(Covector<n,k>(this->getManifold(), p,
                                     coeffs, ELEMENT));
          coeffs[0] = -w;
          coeffs[1] = -w;
          coeffs[2] = u+v-1;
          _changeOrientation(p, coeffs, o, 3);
          sf.push_back(Covector<n,k>(this->getManifold(), p,
                                     coeffs, ELEMENT));
          coeffs[0] = 0;
          coeffs[1] = w;
          coeffs[2] = -v;
          _changeOrientation(p, coeffs, o, 4);
          sf.push_back(Covector<n,k>(this->getManifold(), p,
                                     coeffs, ELEMENT));
          coeffs[0] = w;
          coeffs[1] = 0;
          coeffs[2] = -u;
          _changeOrientation(p, coeffs, o, 5);
          sf.push_back(Covector<n,k>(this->getManifold(), p,
                                     coeffs, ELEMENT));
        }
        else {
          Msg::Error("Not implemented");
          return;
        }
      }
      else if(k == 2) {
        if(type == TYPE_TRI) {
          coeffs[0] = 0.5;
          _changeOrientation(p, coeffs, o, 0);
          sf.push_back(Covector<n,k>(this->getManifold(), p,
                                     coeffs, ELEMENT));
        }
        else if(type == TYPE_TET) {
          coeffs[0] = 2*u;
          coeffs[1] = 2*v;
          coeffs[2] = 2*w-2;
          _changeOrientation(p, coeffs, o, 0);
          sf.push_back(Covector<n,k>(this->getManifold(), p,
                                     coeffs, ELEMENT));
          coeffs[0] = 2*u;
          coeffs[1] = 2*v-2;
          coeffs[2] = 2*w;
          _changeOrientation(p, coeffs, o, 1);
          sf.push_back(Covector<n,k>(this->getManifold(), p,
                                     coeffs, ELEMENT));
          coeffs[0] = 2*u-2;
          coeffs[1] = 2*v;
          coeffs[2] = 2*w;
          _changeOrientation(p, coeffs, o, 2);
          sf.push_back(Covector<n,k>(this->getManifold(), p,
                                     coeffs, ELEMENT));
          coeffs[0] = 2*u;
          coeffs[1] = 2*v;
          coeffs[2] = 2*w;
          _changeOrientation(p, coeffs, o, 3);
          sf.push_back(Covector<n,k>(this->getManifold(), p,
                                     coeffs, ELEMENT));
        }
        else {
          Msg::Error("Not implemented");
          return;
        }
      }
      else if(k == 3) {
        if(type == TYPE_TET) {
          coeffs[0] = 1./6.;
          _changeOrientation(p, coeffs, o, 0);
          sf.push_back(Covector<n,k>(this->getManifold(), p,
                                     coeffs, ELEMENT));
        }
        else {
          Msg::Error("Not implemented");
          return;
        }
      }
    }

    void getDiffCovectors(const Point* p, Orientation o,
                          std::vector<Covector<n,k+1> >& dsf) const
    {
      dsf.clear();
      if (k + 1 > n) return;

      int type = p->getMeshElement()->getType();
      double u,v,w;
      p->getUVW(u,v,w);

      double coeffs[3];

      if(k == 1) {
        if(type == TYPE_LIN) {
          dsf.push_back(Covector<n,k+1>(this->getManifold(), p));
        }
        else if(type == TYPE_TRI) {
          coeffs[0] = -2;
          for(int i = 0; i < 3; i++) {
            _changeOrientation(p, coeffs, o, i);
            dsf.push_back(Covector<n,k+1>(this->getManifold(), p,
                                          coeffs, ELEMENT));
          }
        }
        else if(type == TYPE_TET) {
          coeffs[0] = 0;
          coeffs[1] = 2;
          coeffs[2] = -2;
          _changeOrientation(p, coeffs, o, 0);
          dsf.push_back(Covector<n,k+1>(this->getManifold(), p,
                                        coeffs, ELEMENT));
          coeffs[0] = 0;
          coeffs[1] = 0;
          coeffs[2] = -2;
          _changeOrientation(p, coeffs, o, 1);
          dsf.push_back(Covector<n,k+1>(this->getManifold(), p,
                                        coeffs, ELEMENT));
          coeffs[0] = 2;
          coeffs[1] = 0;
          coeffs[2] = -2;
          _changeOrientation(p, coeffs, o, 2);
          dsf.push_back(Covector<n,k+1>(this->getManifold(), p,
                                        coeffs, ELEMENT));
          coeffs[0] = -2;
          coeffs[1] = 2;
          coeffs[2] = 0;
          _changeOrientation(p, coeffs, o, 3);
          dsf.push_back(Covector<n,k+1>(this->getManifold(), p,
                                        coeffs, ELEMENT));
          coeffs[0] = 2;
          coeffs[1] = 0;
          coeffs[2] = 0;
          _changeOrientation(p, coeffs, o, 4);
          dsf.push_back(Covector<n,k+1>(this->getManifold(), p,
                                        coeffs, ELEMENT));
          coeffs[0] = 0;
          coeffs[1] = -2;
          coeffs[2] = 0;
          _changeOrientation(p, coeffs, o, 5);
          dsf.push_back(Covector<n,k+1>(this->getManifold(), p,
                                        coeffs, ELEMENT));
        }
      }
      else if(k == 2) {
        if(type == TYPE_TET) {
          coeffs[0] = 6;
          _changeOrientation(p, coeffs, o, 0);
          dsf.push_back(Covector<n,k+1>(this->getManifold(), p,
                                        coeffs, ELEMENT));
          _changeOrientation(p, coeffs, o, 1);
          dsf.push_back(Covector<n,k+1>(this->getManifold(), p,
                                        coeffs, ELEMENT));
          _changeOrientation(p, coeffs, o, 2);
          dsf.push_back(Covector<n,k+1>(this->getManifold(), p,
                                        coeffs, ELEMENT));
          _changeOrientation(p, coeffs, o, 3);
          dsf.push_back(Covector<n,k+1>(this->getManifold(), p,
                                        coeffs, ELEMENT));
        }
        else {
          Msg::Error("Not implemented");
          return;
        }
      }
      else if(k == 3) {
        if(type == TYPE_TET) {
          dsf.push_back(Covector<n,k+1>(this->getManifold(), p));
        }
        else {
          Msg::Error("Not implemented");
          return;
        }
      }

    }
  };

  template<int n>
  class BasisForm<n,0> : public DefinedField<Covector<n, 0> > {

   private:
    int _order;

  public:
    BasisForm(const Manifold<n>* m, int order=1) :
      DefinedField<Covector<n, 0> > (m)
    {
      _order = order;
    }

    void getCovectors(const Point* p, Orientation o,
                      std::vector<Covector<n,0> >& sf) const
    {
      sf.clear();
      ElemPartOfUnity<n> scalarBasis(this->getManifold(), 0, _order);
      scalarBasis.getPartOfUnity(p, sf, _order);
    }

    void getDiffCovectors(const Point* p, Orientation o,
                          std::vector<Covector<n,1> >& dsf) const
    {
      dsf.clear();
      ElemDiffPartOfUnity<n> scalarBasisDiff(this->getManifold(), 0, _order);
      scalarBasisDiff.getDiffPartOfUnity(p, dsf, _order);
    }

  };

}

#endif
