// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#ifndef _RM_MELEMIT_H_
#define _RM_MELEMIT_H_

#include "GModel.h"
#include "MElement.h"
#include "RiemannDefines.h"

namespace rm
{

  void processMeshElements(GEntity* ent,
                           std::set<MElement*, MElemLessThanNum>& elems);


  /// Iterator for manifold mesh element and orientation pairs
  class MElemIt : public std::iterator<std::bidirectional_iterator_tag,
                                       std::pair<MElement*, int> >
  {

  private:
    /// Map Gmsh elementary entity tag to a set of its mesh elements
    static std::map<GModel*, std::map<int, std::set<MElement*, MElemLessThanNum> > > _elementCache[4];

    std::pair<MElement*, int> _currentElement;
    std::set<MElement*, MElemLessThanNum>::const_iterator _currentElemIt;
    unsigned int _currentEntIndex;
    unsigned int _currentElemIndex;

    GModel* _gm;
    std::vector<GEntity*> _ents;
    std::vector<int> _entsOr;
    bool _singleElem;

  public:
    MElemIt() : _gm(NULL) {}

    /// Iterator for mesh elements in given Gmsh entities
    /// with the given orientations of Gmsh entities
    MElemIt(GModel* gm,
            const std::vector<GEntity*>& ents,
            const std::vector<int>& entsOr, bool last);

    /// Iterator for single mesh element with orientation 1
    MElemIt(GModel* gm, MElement* me, bool last);
    const std::pair<MElement*, int>* operator*() const;
    const std::pair<MElement*, int>* operator->() const;

    /// True if the iterator range contains given mesh element
    bool containsMeshElement(MElement* me) const;

    /// 0 if not contained in the iterator range
    int getOrientation(MElement* me) const;

    /// The the current Gmsh elementary entity tag
    int getCurrentEntityTag() const;

    /// Get the number of mesh elements in entities
    int getNumMeshElements() const;

    /// Clear mesh element cache
    static void clearElementCache() { 
      for(int i = 0; i < 4; i++) _elementCache[i].clear(); 
    }

    /// Iterator operators
    bool operator==(const MElemIt& other) const;
    bool operator!=(const MElemIt& other) const;

    const std::pair<MElement*, int>* operator++(int);
    const std::pair<MElement*, int>* operator--(int);
    const std::pair<MElement*, int>* operator++();
    const std::pair<MElement*, int>* operator--();

  };

}
#endif
