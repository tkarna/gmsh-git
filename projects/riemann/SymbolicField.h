// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#ifndef _RM_SYMBOLIC_FIELD_H_
#define _RM_SYMBOLIC_FIELD_H_

#include "Field.h"

#if defined(HAVE_GINAC)
#include <ginac/ginac.h>

namespace rm
{
  const GiNaC::symbol& get_symbol(const std::string& s);
  double evalExpression(const double* x, const int nx,
                        const GiNaC::FUNCP_CUBA f);
  bool compileExpressions(const GiNaC::lst& expressions,
                          const std::vector<GiNaC::symbol>& symbols,
                          std::vector<GiNaC::FUNCP_CUBA>& funcptrs);

  /// Tensor field represented by coefficient arrays of symbolic expressions
  template <class TensorType>
  class SymbolicField : public Field<TensorType>,
                        public VectorSpaceCat<SymbolicField<TensorType>, double>
  {
  protected:
    GiNaC::lst _c;
    Representation _r;
    Coordinates _coords;

    bool _compiled;
    std::vector<GiNaC::FUNCP_CUBA> _cc;

    void _initExpressions()
    {
      unsigned int nc = 0;
      const int n = TensorTraits<TensorType>::dim;
      if(_r == MODEL) nc = TensorTraits<TensorType>::numModelCoeffs;
      else if(_r == MANIFOLD) nc = TensorTraits<TensorType>::numCoeffs;
      if(_c.nops() != nc) {
        Msg::Error("Wrong number of symbolic expressions given (%d wanted, %d given)", nc, _c.nops());
        _compiled = false;
      }
      else {
        std::vector<GiNaC::symbol> syms;
        if(_coords == MODELCOORDS) {
          syms.push_back(get_symbol("xs"));
          syms.push_back(get_symbol("ys"));
          syms.push_back(get_symbol("zs"));
        }
        else if(_coords == MANIFOLDCOORDS) {
          std::string temp = "";
          std::stringstream ss(temp);
          for(int i = 0; i < n; i++) {
            std::string coord = "u";
            ss << i;
            ss >> temp;
            coord += temp;
            syms.push_back(get_symbol(coord));
          }
        }
        _compiled = compileExpressions(_c, syms, _cc);
      }
    }

  public:
    SymbolicField() : Field<TensorType>() {}
    SymbolicField(const Manifold<TensorTraits<TensorType>::dim>* m) :
      Field<TensorType>(m) {}
    SymbolicField(const Manifold<TensorTraits<TensorType>::dim>* m,
                  GiNaC::lst c, Representation r, Coordinates coords) :
      Field<TensorType>(m), _c(c), _r(r), _coords(coords)
    {
      _initExpressions();
    }

    virtual void getValue(const Point* p, TensorType& tp) const
    {
      if(!_compiled) {
        tp = TensorType(this->getManifold(), p);
        return;
      }
      double c[MAXCOMP];
      vecinit<MAXCOMP>(c);
      int nc = 0;
      if(_r == MODEL) nc = TensorTraits<TensorType>::numModelCoeffs;
      else if(_r == MANIFOLD) nc = TensorTraits<TensorType>::numCoeffs;

      if(_coords == MODELCOORDS) {
        double x,y,z;
        p->getXYZ(x,y,z);
        const double xyz[3] = {x,y,z};
        for(int i = 0; i < nc; i++) {
          c[i] = evalExpression(xyz, 3,  _cc[i]);
        }
      }
      else if (_coords == MANIFOLDCOORDS) {
        const int n = TensorTraits<TensorType>::dim;
        double coords[n];
        this->getManifold()->getModelMap()->mapPoint(p, coords);
        for(int i = 0; i < nc; i++)
          c[i] = evalExpression(coords, n, _cc[i]);
      }
      tp = TensorType(this->getManifold(), p, c, _r);
    }

    bool convertToModelCoords() {
      if(_coords == MODELCOORDS) return true;
      ModelMapType mt = this->getManifold()->getModelMapType();
      if(mt != MM_SYMBOLIC) {
        Msg::Error("Cannot convert symbolic field to manifold coordinates, manifold chart model map is not symbolic");
        return false;
      }
      const int n = TensorTraits<TensorType>::dim;
      std::string temp = "";
      std::stringstream ss(temp);
      ModelMap<n>* mm = this->getManifold()->getModelMap();
      for(int i = 0; i < n; i++) {
        std::string coord = "u";
        ss << i;
        ss >> temp;
        coord += temp;
        GiNaC::ex ui = mm->getCoordExpr(i);
        for(unsigned int j = 0; j < _c.nops(); j++) {
          _c[j] = _c[j].subs(get_symbol(coord) == ui);
        }
        _coords = MODELCOORDS;
      }
      return true;
    }

    virtual GiNaC::ex getExpression(unsigned int i) const
    {
      if(i < _c.nops()) return _c[i];
      else return GiNaC::ex();
    }
    virtual void getExpressions(GiNaC::lst& c) const { c = _c; }

    virtual Representation getRepresentation() const { return _r; }
    virtual Coordinates getCoordinates() const { return _coords; }

    virtual SymbolicField<TensorType>& operator+=
    (const SymbolicField<TensorType>& f)
    {
      if(this->getManifold()->getNum() != f.getManifold()->getNum()) {
        Msg::Error("Fields do not belong to the same manifold.");
        return *this;
      }
      if(this->getRepresentation() != f.getRepresentation() ||
         this->getCoordinates() != f.getCoordinates()) {
        Msg::Error("Error symbolic fields are not compatible.");
        return *this;
      }
      GiNaC::lst c;
      f.getExpressions(c);
      for(unsigned int i = 0; i < _c.nops(); i++)
      _c[i] += c[i];
      return *this;
    }

    virtual SymbolicField<TensorType>& operator*=(const double& s)
    {
      for(unsigned int i = 0; i < _c.nops(); i++) _c[i] *= s;
      return *this;
    }

  };

}

#endif

#endif
