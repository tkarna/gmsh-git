// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#ifndef _RM_MANIFOLD_PART_OF_UNITY_H_
#define _RM_MANIFOLD_PART_OF_UNITY_H_

#include "Manifold.h"
#include "Field.h"

namespace rm
{

  /// Partition of unity of a manifold defined by mesh element shape functions
  template <int n>
  class ElemPartOfUnity : public DefinedField<Covector<n, 0> >
  {
  protected:
    int _num;
    int _order;
  public:

    /// Construct for a manifold
    ElemPartOfUnity(const Manifold<n>* m, int num=0, int order=-1) :
      DefinedField<Covector<n, 0> >(m), _num(num), _order(order) {}


    /// Get partition of unity
    virtual void getPartOfUnityCoeffs(const Point* p,
                                      std::vector<double>& c,
                                      int order=-1) const
    {
      double u, v, w;
      p->getUVW(u, v, w);
      double s[1256];
      p->getMeshElement()->getShapeFunctions(u, v, w, s, order);
      c = std::vector<double>(1256);
      c.assign(s, s+1256);
    }

    virtual void getPartOfUnity(const Point* p,
                                std::vector<Covector<n, 0> >& sf,
                                int order=-1) const
    {
      double u, v, w;
      p->getUVW(u, v, w);
      double s[1256];
      p->getMeshElement()->getShapeFunctions(u, v, w, s, order);
      for(int i = 0; i < p->getMeshElement()->getNumShapeFunctions(); i++) {
        sf.push_back(Covector<n, 0>(this->getManifold(), p, &s[i], MANIFOLD));
      }
    }
    virtual void setNum(int num) { _num = num; }
    virtual void setOrder(int order) { _order = order; }
    virtual void getValue(const Point* p, Covector<n,0>& x) const
    {
      std::vector<Covector<n, 0> > sf;
      getPartOfUnity(p, sf, _order);
      x = sf.at(_num);
    }

  };

  /// Exterior derivatives of partition of unity of a manifold defined by
  /// mesh element shape functions
  template <int n>
  class ElemDiffPartOfUnity : public DefinedField<Covector<n, 1> >
  {
  protected:
    int _num;
    int _order;
  public:

    /// Construct for a manifold
    ElemDiffPartOfUnity(const Manifold<n>* m, int num=0, int order=-1) :
      DefinedField<Covector<n, 1> >(m), _num(num), _order(order) {}

    /// Fill empty double arrays in a vector with the coefficients
    /// of exterior derivative of partition of unity
    virtual void getDiffPartOfUnityCoeffs(const Point* p,
                                          std::vector<double*>& c,
                                          int order=-1) const
    {
      std::vector<Covector<n, 1> > dsf;
      this->getDiffPartOfUnity(p, dsf, order);
      for(unsigned int i = 0; i < dsf.size(); i++) dsf[i].getCoeffs(c.at(i));
    }

    /// Get exterior derivatives of partition of unity
    virtual void getDiffPartOfUnity(const Point* p,
                                    std::vector<Covector<n, 1> >& dsf,
                                    int order=-1) const
    {
      double u, v, w;
      p->getUVW(u, v, w);
      MElement* me = p->getMeshElement();
      double ds[1256][3];
      double dsi[3];
      ModelMap<n>* mm = this->getManifold()->getModelMap();
      ModelMapType mt = mm->getModelMapType();
      me->getGradShapeFunctions(u, v, w, ds, order);
      if(mt == MM_UVW) {
        for(int i = 0; i < me->getNumShapeFunctions(); i++) {
          for(int j = 0; j < n; j++) dsi[j] = ds[i][j];
          dsf.push_back(Covector<n, 1>(this->getManifold(), p,
                                       dsi, MANIFOLD));
        }
      }
      else { // pullback to model coordinates from mesh element coordinates
        double jac[3][3];
        double invjac[3][3];
        me->getJacobian(u, v, w, jac);
        inv3x3(jac, invjac);
        double dsimod[3];
        for(int i = 0; i < me->getNumShapeFunctions(); i++) {
          for(int j = 0; j < 3; j++) dsi[j] = ds[i][j];
          ::matvec(invjac, dsi, dsimod); // Gmsh Numeric.h
          dsf.push_back(Covector<n, 1>(this->getManifold(), p,
                                       dsimod, MODEL));
        }
      }
    }
    virtual void setNum(int num) { _num = num; }
    virtual void setOrder(int order) { _order = order; }
    virtual void getValue(const Point* p, Covector<n,1>& x) const
    {
      std::vector<Covector<n, 1> > dsf;
      getDiffPartOfUnity(p, dsf, _order);
      x = dsf.at(_num);
    }
  };
}
#endif
