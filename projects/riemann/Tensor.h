// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#ifndef _RM_TENSOR_H_
#define _RM_TENSOR_H_

#include "RiemannDefines.h"
#include "Manifold.h"
#include "RiemannNumeric.h"
#include "VectorSpace.h"
#include "MultiIndex.h"
#include "ATensor.h"
#include "TensorTraits.h"
#include "ManifoldPoint.h"

namespace rm
{

  /// (k,l)-tensor on n-manifold with given symmetries
  template <int n, int k, int l, TensorSymmetry s, int h=1>
  class Tensor : public VectorSpaceCat<Tensor<n,k,l,s,h>, double>
  {
  protected:
    /// Manifold this tensor belongs to
    const Manifold<n>* _m;
    /// Point whose (co)tangent space this tensor belongs to
    const Point* _p;
    /// Coefficients that represent a (k,l)-tensor on n-manifold
    double _coeffs[TensorTraits<Tensor<n,k,l,s,h> >::numCoeffs];
    /// Initialize tensor coefficients
    inline void _initCoeffs(const double* c, Representation r,
                            ModelMapType mt);
    /// Auxiliary function for _initCoeffs
    inline void _initCoeffsFromModel(const double* c);

  public:

    /// Uninitialized tensor
    Tensor();

    /// Zero tensor on n-manifold m point p
    Tensor(const Manifold<n>* m, const Point* p);

    /// tensor on n-manifold m point p, represented as a coefficient
    /// array either in manifold coordinates or Gmsh model coordinates
    Tensor(const Manifold<n>* m, const Point* p,
           const double* c, Representation r);

    /// tensor on n-manifold point p, represented as a coefficient
    /// array either in manifold coordinates or Gmsh model coordinates
    Tensor(const ManifoldPoint<n>& p, const double* c, Representation r);

    /// Construct from tensor created from run-time expression
    Tensor(const ATensor& x);

    /// Construct from any tensor rype
    template <class TensorType>
    Tensor(const TensorType& x);

    /// Get the manifold this tensor belongs to
    const Manifold<n>* getManifold() const { return _m; }

    /// Get the point to whose (co)tangent space this tensor belongs to
    const Point* getPoint() const { return _p; }

    /// Number of coefficients that represent this tensor
    inline int getNumCoeffs() const;

    /// Get i:th coefficient
    inline double operator() (int i) const { return _coeffs[i]; }

    /// Get coefficient by multi-index
    double operator() (const MultiIndex& I, int harm=1) const;

    /// Does this tensor belong to tangent space or cotangent space or both
    /// of the manifold
    inline TensorSpace getTensorSpace() const;

    /// What symmetries does this tensor possess
    inline TensorSymmetry getTensorSymmetry() const;

    inline int getContravariantIndex() const { return k; }
    inline int getCovariantIndex() const { return l; }

    /// Print tensor info and coefficients
    void print() const;

    /// Get pointer to coefficient array that represents tensor
    const inline double* getPointerToCoeffs() const;

    /// Get coefficient array that represents this tensor
    void inline getCoeffs
    (double c[TensorTraits<Tensor<n,k,l,s,h> >::numCoeffs]) const;

    /// Get coefficient array in Gmsh model coordinates
    bool getModelCoeffs
    (double c[TensorTraits<Tensor<n,k,l,s,h> >::numModelCoeffs]) const;

    /// Get coefficients with which to operate on other tensors depending
    /// on their co/contravariance
    inline void getOperatorCoeffs
    (double c[TensorTraits<Tensor<n,k,l,s,h> >::numCoeffs],
     TensorSpace tspace) const;

    /// Fill tensor coefficients into a data structure from which a Gmsh
    /// post-processing view can be created
    int getPViewData(std::vector<std::map<int, std::vector<double> > >& data,
                     Representation r, int step=1);

    ///
    /// Vector space operations between tensors:
    ///
    Tensor<n,k,l,s,h>& operator+=(const Tensor<n,k,l,s,h>& x);
    Tensor<n,k,l,s,h>& operator*=(const double& x);
    Tensor<n,k,l,s,h>& operator*=(const Tensor<n,0,0,s,h>& x);

    virtual ~Tensor() {}
  };


  ///
  /// Derived types to name tensors for convinience
  ///

  // k-covector:
  //  A Linear map from k 1-vectors and from k-vectors to scalars
  template <int n, int k>
  class Covector : public Tensor<n,0,k,SKEWSYMMETRIC,1>
  {
  public:
    Covector() : Tensor<n,0,k,SKEWSYMMETRIC,1>() {}
    Covector(const Manifold<n>* m, const Point* p)
      : Tensor<n,0,k,SKEWSYMMETRIC,1>(m, p) {}
    Covector(const Manifold<n>* m, const Point* p,
             const double* c, Representation r)
      : Tensor<n,0,k,SKEWSYMMETRIC,1>(m, p, c, r) {}
    Covector(const ATensor& x)
      : Tensor<n,0,k,SKEWSYMMETRIC,1>(x) {}
    Covector(const Tensor<n, 0, k, SKEWSYMMETRIC,1>& x)
      : Tensor<n,0,k,SKEWSYMMETRIC,1>(x.getManifold(), x.getPoint())
    {
      x.getCoeffs(this->_coeffs);
    }
  };

  // k-vector:
  //  A Linear map from k 1-covectors and from k-covectors to scalars
  template <int n, int k>
  class Vector : public Tensor<n,k,0,SKEWSYMMETRIC,1>
  {
  public:
    Vector() : Tensor<n,k,0,SKEWSYMMETRIC,1>() {}
    Vector(const Manifold<n>* m, const Point* p)
      : Tensor<n,k,0,SKEWSYMMETRIC,1>(m, p) {}
    Vector(const Manifold<n>* m, const Point* p,
           const double* c, Representation r)
      : Tensor<n,k,0,SKEWSYMMETRIC,1>(m, p, c, r) {}
    Vector(const ATensor& x)
      : Tensor<n,k,0,SKEWSYMMETRIC,1>(x) {}
    Vector(const Tensor<n, k, 0, SKEWSYMMETRIC,1>& x)
      : Tensor<n,k,0,SKEWSYMMETRIC,1>(x.getManifold(), x.getPoint())
    {
      x.getCoeffs(this->_coeffs);
    }
  };

  // Symmetric (0,2)-tensor:
  //  A bilinear map from 1-vectors to scalars
  //  Induces maps for 1-covectors and k-(co)vectors
  template <int n>
  class MetricTensor : public Tensor<n,0,2,SYMMETRIC,1>
  {
  public:
    MetricTensor() : Tensor<n,0,2,SYMMETRIC,1>() {}
    MetricTensor(const Manifold<n>* m, const Point* p)
      : Tensor<n,0,2,SYMMETRIC,1>(m, p) {}
    MetricTensor(const Manifold<n>* m, const Point* p,
                 const double* c, Representation r)
      : Tensor<n,0,2,SYMMETRIC,1>(m, p, c, r) {}
    MetricTensor(const ATensor& x)
      : Tensor<n,0,2,SYMMETRIC,1>(x) {}
    MetricTensor(const Tensor<n,0,2,SYMMETRIC,1>& x)
      : Tensor<n,0,2,SYMMETRIC,1>(x.getManifold(), x.getPoint())
    {
      x.getCoeffs(this->_coeffs);
    }
  };

  // (1,1)-tensor:
  //  A linear map from 1-vectors to 1-vectors
  //  Induces maps for 1-covectors and k-(co)vectors
  template <int n>
  class LinTransform : public Tensor<n,1,1,FULL,1>
  {
  public:
    LinTransform() : Tensor<n,1,1,FULL,1>() {}
    LinTransform(const Manifold<n>* m, const Point* p)
      : Tensor<n,1,1,FULL,1>(m, p) {}
    LinTransform(const Manifold<n>* m, const Point* p,
                 const double* c, Representation r)
      : Tensor<n,1,1,FULL,1>(m, p, c, r) {}
    LinTransform(const ATensor& x)
      : Tensor<n,1,1,FULL,1>(x) {}
    LinTransform(const Tensor<n,1,1,FULL,1>& x)
      : Tensor<n,1,1,FULL,1>(x.getManifold(), x.getPoint())
    {
      x.getCoeffs(this->_coeffs);
    }
  };

  ///
  /// Member function implementations
  ///

  template <int n, int k, int l, TensorSymmetry s, int h>
  inline void Tensor<n,k,l,s,h>::_initCoeffsFromModel(const double* c)
  {
    double jac[MAYBE(n*3)];
    if(!this->getManifold()->getModelMap()->
       getDiffMapToManifold(this->getPoint(), jac, this->getTensorSpace())) {
      Msg::Error("(%d, %d)-tensor cannot be initialized from model coefficients, model map does not provide the coefficient transformation", k, l);
      vecinit<TensorTraits<Tensor<n,k,l,s,h> >::numCoeffs>(_coeffs);
      return;
    }

    if(s==SKEWSYMMETRIC) {
      diffv<n, 3, MAX(k,l)>(jac, c, _coeffs);
    }
    else if(s==SYMMETRIC && k+l == 2 && l != k) {
      diffs<n, 3>(jac, c, _coeffs);
    }
    else if(s==FULL && k == 1 && l == 1) {
      double invjac[MAYBE(n*3)];
      if(!this->getManifold()->getModelMap()->
         getDiffMapToModel(this->getPoint(), invjac,
                           this->getTensorSpace())) {
        Msg::Error("(%d, %d)-tensor cannot be initialized from model coefficients, model map does not provide the coefficient transformation", k, l);
        vecinit<TensorTraits<Tensor<n,k,l,s,h> >::numCoeffs>(_coeffs);
        return;
      }
      difflt<n, 3>(jac, invjac, c, _coeffs);
    }
    else {
      Msg::Debug("TBD");
    }
  }

  template <int n, int k, int l, TensorSymmetry s, int h>
  inline void Tensor<n,k,l,s,h>::_initCoeffs
  (const double* c, Representation r, ModelMapType mt)
  {
    if((k == 0 && l == 0) || r == MANIFOLD)
      vecinit<TensorTraits<Tensor<n,k,l,s,h> >::numCoeffs>(c, _coeffs);
    else if(r == MODEL && mt == MM_XYZ)
      vecinit<TensorTraits<Tensor<n,k,l,s,h> >::numCoeffs>(c, _coeffs);
    else if(r == ELEMENT && mt == MM_UVW)
      vecinit<TensorTraits<Tensor<n,k,l,s,h> >::numCoeffs>(c, _coeffs);
    else if(k + l == 1 && r == MODEL && (mt == MM_XY || mt == MM_X) )
      vecinit<TensorTraits<Tensor<n,k,l,s,h> >::numCoeffs>(c, _coeffs);
    else if(r == MODEL) {
      _initCoeffsFromModel(c);
    }
    else if(r == ELEMENT) {
      ModelMapUVW<n> mm(this->getManifold()->getModel());
      const int ncm = TensorTraits<Tensor<n,k,l,s,h> >::numModelCoeffs;
      double jac[MAYBE(n*3)];
      mm.getDiffMapToModel(this->getPoint(), jac, this->getTensorSpace());
      double mcoeffs[ncm];

      if(s==SKEWSYMMETRIC) {
        diffv<3, n, MAX(k,l)>(jac, c, mcoeffs);
      }
      else if(s==SYMMETRIC && k+l == 2 && l != k) {
        diffs<3, n>(jac, c, mcoeffs);
      }
      else if(s==FULL && k == 1 && l == 1) {
        double invjac[MAYBE(n*3)];
        mm.getDiffMapToModel(this->getPoint(), invjac,
                             this->getTensorSpace());
        difflt<3, n>(jac, invjac, c, mcoeffs);
      }
      _initCoeffsFromModel(mcoeffs);
    }
    else {
      Msg::Debug("Missing case in _initCoeffs");
    }
  }

  template <int n, int k, int l, TensorSymmetry s, int h>
  Tensor<n,k,l,s,h>::Tensor() : _m(NULL), _p(NULL)
  {
    vecinit<TensorTraits<Tensor<n,k,l,s,h> >::numCoeffs>(_coeffs);
  }

  template <int n, int k, int l, TensorSymmetry s, int h>
  Tensor<n,k,l,s,h>::Tensor(const Manifold<n>* m, const Point* p)
    : _m(m), _p(p)
  {
    vecinit<TensorTraits<Tensor<n,k,l,s,h> >::numCoeffs>(_coeffs);
  }

  template <int n, int k, int l, TensorSymmetry s, int h>
  Tensor<n,k,l,s,h>::Tensor(const Manifold<n>* m, const Point* p,
                            const double* c, Representation r)
    : _m(m), _p(p)
  {
    _initCoeffs(c, r, m->getModelMapType());
  }

  template <int n, int k, int l, TensorSymmetry s, int h>
  Tensor<n,k,l,s,h>::Tensor(const ATensor& x)
    : _m(NULL), _p(x.getPoint())
  {
    this->_m = static_cast<const Manifold<n>*>(x.getManifold());
    double c[1256];
    int numcoeffs = x.getNumCoeffs();
    if(numcoeffs > 1256) {
      Msg::Warning("ATensor has more coeffiecients than prepared for! (%d > 1256)", numcoeffs);
      numcoeffs = 1256;
    }
    for(int i = 0; i < numcoeffs; i++) c[i] = x(i);
    _initCoeffs(c, x.getRepresentation(),
                x.getManifold()->getModelMapType());
  }

  template <int n, int k, int l, TensorSymmetry s, int h>
  template <class TensorType>
  Tensor<n,k,l,s,h>::Tensor(const TensorType& x)
    : _m(x.getManifold()), _p(x.getPoint())
  {
    if(n != TensorTraits<TensorType>::dim ||
       k != TensorTraits<TensorType>::ctrIndex ||
       l != TensorTraits<TensorType>::coIndex ||
       s != TensorTraits<TensorType>::symm ||
       h != TensorTraits<TensorType>::h)
      Msg::Error("Cannot construct from given tensor type");
    else x.getCoeffs(_coeffs);
  }

  template <int n, int k, int l, TensorSymmetry s, int h>
  inline int Tensor<n,k,l,s,h>::getNumCoeffs() const
  {
    return TensorTraits<Tensor<n,k,l,s,h> >::numCoeffs;
  }

  template <int n, int k, int l, TensorSymmetry s, int h>
  double Tensor<n,k,l,s,h>::operator() (const MultiIndex& I, int harm) const
  {
    int nc = TensorTraits<Tensor<n,k,l,s,h> >::numCoeffs/h;
    if(s==SKEWSYMMETRIC) {
      int perm = 1;
      unsigned int i = I.skewSymmetricTensorIndex(n, perm);
      return _coeffs[(h-1)*nc+i]*perm;
    }
    else {
      Msg::Debug("TBD");
      return 0;
    }
  }

  template <int n, int k, int l, TensorSymmetry s, int h>
  inline TensorSpace Tensor<n,k,l,s,h>::getTensorSpace() const
  {
    return TensorTraits<Tensor<n,k,l,s,h> >::space;
  }

  template <int n, int k, int l, TensorSymmetry s, int h>
  inline TensorSymmetry Tensor<n,k,l,s,h>::getTensorSymmetry() const
  {
    return s;
  }

  template <int n, int k, int l, TensorSymmetry s, int h>
  void Tensor<n,k,l,s,h>::print() const
  {
    if(s==SKEWSYMMETRIC && this->getTensorSpace() == COTANGENT)
      Msg::Info("%d-Covector on %d-manifold", l, n);
    else if(s==SKEWSYMMETRIC && this->getTensorSpace() == TANGENT)
      Msg::Info("%d-Vector on %d-manifold", k, n);
    else
      Msg::Info("(%d, %d)-Tensor on %d-manifold", k, l, n);

    Msg::Info("  Manifold coefficients:");

    const int nmc = TensorTraits<Tensor<n,k,l,s,h> >::numModelCoeffs;
    const int nc = TensorTraits<Tensor<n,k,l,s,h> >::numCoeffs;

    std::string coeffs = "";
    std::stringstream ss1;

    for(int i = 0; i < nc; i++) {
      ss1 << std::scientific << _coeffs[i];
      ss1 << " ";
    }
    coeffs = ss1.str();
    Msg::Info("    %s", coeffs.c_str());
    double mc[nmc];
    this->getModelCoeffs(mc);

    std::stringstream ss2;
    Msg::Info("  Model coefficients:");
    for(int i = 0; i < nmc; i++) {
      ss2 << std::scientific << mc[i];
      ss2 << " ";
    }

    coeffs = ss2.str();
    Msg::Info("    %s", coeffs.c_str());
    Msg::Info(" ");
  }

  template <int n, int k, int l, TensorSymmetry s, int h>
  const inline double* Tensor<n,k,l,s,h>::getPointerToCoeffs() const
  {
    return _coeffs;
  }

  template <int n, int k, int l, TensorSymmetry s, int h>
  void inline Tensor<n,k,l,s,h>::getCoeffs
  (double c[TensorTraits<Tensor<n,k,l,s,h> >::numCoeffs]) const
  {
    vecinit<TensorTraits<Tensor<n,k,l,s,h> >::numCoeffs>(_coeffs, c);
  }

  template <int n, int k, int l, TensorSymmetry s, int h>
  bool Tensor<n,k,l,s,h>::getModelCoeffs
  (double c[TensorTraits<Tensor<n,k,l,s,h> >::numModelCoeffs]) const
  {
    if( (k == 0 && l == 0) ||
        this->getManifold()->getModelMap()->getModelMapType() == MM_XYZ) {
      getCoeffs(c);
      return true;
    }

    double jac[MAYBE(3*n)];
    if(!this->getManifold()->getModelMap()->
       getDiffMapToModel(this->getPoint(), jac, this->getTensorSpace())) {
      Msg::Error("Cannot produce the model coefficients of a (%d, %d)-tensor, model map does not provide the coefficient transformation", k, l);
      vecinit<TensorTraits<Tensor<n,k,l,s,h> >::numModelCoeffs>(c);
      return false;
    }
    if(s == SKEWSYMMETRIC) {
      diffv<3,n,MAX(k,l)>(jac, _coeffs, c);
    }
    else if(s == SYMMETRIC && k+l == 2 && l != k) {
      diffs<3,n>(jac, _coeffs, c);
    }
    else if(s == FULL && k == 1 && l == 1) {
      double invjac[MAYBE(3*n)];
      if(!this->getManifold()->getModelMap()->
         getDiffMapToManifold(this->getPoint(), invjac, this->getTensorSpace())) {
        vecinit<TensorTraits<Tensor<n,k,l,s,h> >::numModelCoeffs>(c);
        return false;
      }
      difflt<3,n>(jac, invjac, _coeffs, c);
    }
    else {
      Msg::Debug("TBD");
      return false;
    }
    return true;
  }

  template <int n, int k, int l, TensorSymmetry s, int h>
  inline void Tensor<n,k,l,s,h>::getOperatorCoeffs
  (double c[TensorTraits<Tensor<n,k,l,s,h> >::numCoeffs],
   TensorSpace tspace) const
  {
    const int nc = TensorTraits<Tensor<n,k,l,s,h> >::numCoeffs;
    if(s == FULL && k == 1 && l == 1) {
      if(tspace == TANGENT) vecinit<nc>(_coeffs, c);
      else if(tspace == COTANGENT) transpose<n,n>(_coeffs, c);
    }
    else if(s == SYMMETRIC && k+l == 2 && l != k) {
      if(tspace == TANGENT) vecinit<nc>(_coeffs, c);
      else if(tspace == COTANGENT) invs<n>(_coeffs, c);
    }
    else {
      Msg::Error("No operator coefficients for this kind of tensor");
      vecinit<nc>(c);
    }
  }

  template <int n, int k, int l, TensorSymmetry s, int h>
  Tensor<n,k,l,s,h>& Tensor<n,k,l,s,h>::operator+=(const Tensor<n,k,l,s,h>& x)
  {
    if(this->getManifold()->getNum() != x.getManifold()->getNum()) {
      Msg::Error("Operands do not belong to the same manifold.");
      return *this;
    }
    const int nc = TensorTraits<Tensor<n,k,l,s,h> >::numCoeffs;
    for(int i = 0; i < nc; i++) _coeffs[i] += x(i);
    return *this;
  }

  template <int n, int k, int l, TensorSymmetry s, int h>
  Tensor<n,k,l,s,h>& Tensor<n,k,l,s,h>::operator*=(const double& x)
  {
    const int nc = TensorTraits<Tensor<n,k,l,s,h> >::numCoeffs;
    for(int i = 0; i < nc; i++) _coeffs[i] *= x;
    return *this;
  }

  template <int n, int k, int l, TensorSymmetry s, int h>
  Tensor<n,k,l,s,h>& Tensor<n,k,l,s,h>::operator*=(const Tensor<n,0,0,s,h>& x)
  {
    if(this->getManifold()->getNum() != x.getManifold()->getNum()) {
      Msg::Error("Operands do not belong to the same manifold.");
      return *this;
    }
    const int nc = TensorTraits<Tensor<n,k,l,s,h> >::numCoeffs;
    if(h == 1) {
      for(int i = 0; i < nc; i++) _coeffs[i] *= x(0);
      return *this;
    }
    else {
      Msg::Debug("TBD");
      return *this;
    }
  }

  template <int n, int k, int l, TensorSymmetry s, int h>
  int Tensor<n,k,l,s,h>::getPViewData
    (std::vector<std::map<int, std::vector<double> > >& data,
     Representation r, int step)
    {
      data.resize(step);

      const int numcompm = TensorTraits<Tensor<n,k,l,s,h> >::numModelCoeffs;
      const int numcomp = TensorTraits<Tensor<n,k,l,s,h> >::numCoeffs;

      if((r==MANIFOLD && numcomp > 9) || (r==MODEL && numcompm > 9)) {
        Msg::Error("Tensor field write not implemented (over 9 components)");
        return 0;
      }

      int numcomp2 = 0;
      MElement* me = this->getPoint()->getMeshElement();

      if(r==MODEL) {
        double c[numcompm];
        this->getModelCoeffs(c);
        numcomp2 = 1;
        if(s == SKEWSYMMETRIC || (s == FULL && k+l==2)) {
          for(int j = 0; j < numcompm; j++) {
            data[step-1][me->getNum()].push_back(c[j]);
          }
          if(numcompm > 1) {
            numcomp2 = 3;
            for(int j = numcompm; j < 3; j++) {
              data[step-1][me->getNum()].push_back(0.);
            }
          }
          if(numcompm > 3) {
            numcomp2 = 9;
            for(int j = numcompm; j < 9; j++) {
              data[step-1][me->getNum()].push_back(0.);
            }
          }
        }
        else if (s == SYMMETRIC && k+l==2) {
          numcomp2 = 9;
          double fc[3*3];
          sym2full<3>(c, fc);
          for(int j = 0; j < 3*3; j++) {
            data[step-1][me->getNum()].push_back(fc[j]);
          }
        }
        else Msg::Error("Tensor field write not implemented.");
      }
      else if(r==MANIFOLD) {
        double c[numcomp];
        this->getCoeffs(c);
        numcomp2 = numcomp;
        if(numcomp > 1 && numcomp <= 3) numcomp2 = 3;
        else if(numcomp > 3) numcomp2 = 9;
        for(int j = 0; j < numcomp; j++) {
          data[step-1][me->getNum()].push_back(c[j]);
        }
        for(int j = numcomp; j < numcomp2; j++) {
          data[step-1][me->getNum()].push_back(0.);
        }
      }
      return numcomp2;
    }
}

#endif
