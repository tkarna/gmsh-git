// Riemannian manifold example project
//
// An example in an upcoming Ph.D. thesis [2].
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

// [2] Matti Pellikka, Tampere University of Technology, Tampere, Finland, 2013.

#include "Riemann.h"
#include "ScalarPoissonSolver.h"

// Manifold dependent permittivity field
class Permittivity : public rm::DefinedField<rm::Covector<3,0> >
{
private:
  const rm::Manifold<3>* _mMat1;
  const rm::Manifold<3>* _mMat2;
  double _eps1;
  double _eps2;

public:
  Permittivity(const rm::Manifold<3>* mDef,
               const rm::Manifold<3>* mMat1,
               const rm::Manifold<3>* mMat2,
               double eps1=1, double eps2=10) :
    rm::DefinedField<rm::Covector<3,0> >(mDef),
    _mMat1(mMat1), _mMat2(mMat2), _eps1(eps1), _eps2(eps2) {}


  virtual void getValue(const rm::Point* p, rm::Covector<3,0>& tp) const
  {
    double c[1] = {_eps1};
    if(_mMat2->containsMeshElement(p->getMeshElement())) c[0] = _eps2;
    tp = rm::Covector<3,0>(this->getManifold(), p, c, rm::MODEL);
  }
};

// Constructs permittivity field from the metric tensor of a manifold
class MetricRelPermittivity : public rm::DefinedField<rm::LinTransform<3> >
{
private:

public:
  MetricRelPermittivity(rm::Manifold<3>* mDef) :
    rm::DefinedField<rm::LinTransform<3> >(mDef) {}


  virtual void getValue(const rm::Point* p, rm::LinTransform<3>& tp) const
  {
    double c[9];
    for(int i = 0; i < 9; i++) c[i] = 0.;
    c[0] = 1.;
    c[4] = 1.;
    c[8] = 1.;
    rm::MetricTensor<3> g;
    this->getManifold()->getMetricTensor(p, g);
    double gc[6];
    g.getModelCoeffs(gc);
    double invgc[6];
    double detg = rm::dets<3>(gc);
    rm::invs<3>(gc, invgc);
    double invgcf[9];
    rm::sym2full<3>(invgc, invgcf);
    for(int i = 0; i < 9; i++) c[i] = invgcf[i]*detg;

    //g.getOperatorCoeffs(gc, rm::COTANGENT);
    //rm::hodgemat<3,1>(gc, c);
    tp = rm::LinTransform<3>(this->getManifold(), p, c, rm::MODEL);
  }
};

// Field to express a boundary condition of x-coordinate
class XBoundaryCond : public rm::DefinedField<rm::Covector<2,0> >
{
private:
  double _r;

public:
  XBoundaryCond(const rm::Manifold<2>* m, double r=0.01) :
    rm::DefinedField<rm::Covector<2,0> >(m), _r(r) {}


  virtual void getValue(const rm::Point* p, rm::Covector<2,0>& tp) const
  {
    double c[1] = {0.};
    double x,y,z;
    p->getXYZ(x,y,z);
    double r = sqrt(x*x+y*y+z*z);
    double theta = acos(z/r);
    double phi = atan2(y,x);
    c[0] = _r*sin(theta)*cos(phi);
    //printf("x: %g -> %g, when y = %g, z = %g \n", x, c[0], y, z);
    tp = rm::Covector<2,0>(this->getManifold(), p, c, rm::MODEL);
  }
};

// Field to express a boundary condition of y-coordinate
class YBoundaryCond : public rm::DefinedField<rm::Covector<2,0> >
{
private:
  double _r;

public:
  YBoundaryCond(const rm::Manifold<2>* m, double r=0.01) :
    rm::DefinedField<rm::Covector<2,0> >(m), _r(r) {}


  virtual void getValue(const rm::Point* p, rm::Covector<2,0>& tp) const
  {
    double c[1] = {0.};
    double x,y,z;
    p->getXYZ(x,y,z);
    double r = sqrt(x*x+y*y+z*z);
    double theta = acos(z/r);
    double phi = atan2(y,x);
    c[0] = _r*sin(theta)*sin(phi);

    tp = rm::Covector<2,0>(this->getManifold(), p, c, rm::MODEL);
  }
};

// Field to express a boundary condition of z-coordinate
class ZBoundaryCond : public rm::DefinedField<rm::Covector<2,0> >
{
private:
  double _r;

public:
  ZBoundaryCond(const rm::Manifold<2>* m, double r=0.01) :
    rm::DefinedField<rm::Covector<2,0> >(m), _r(r) {}


  virtual void getValue(const rm::Point* p, rm::Covector<2,0>& tp) const
  {
    double c[1] = {0.};
    double x,y,z;
    p->getXYZ(x,y,z);
    double r = sqrt(x*x+y*y+z*z);
    double theta = acos(z/r);
    c[0] = _r*cos(theta);

    tp = rm::Covector<2,0>(this->getManifold(), p, c, rm::MODEL);
  }
};

int main(int argc, char **argv)
{
  // Initialize Gmsh
  GmshInitialize(argc, argv);
  GmshSetOption("General","Terminal", 1.); // terminal mode
  GmshSetOption("General","Verbosity",99.); // debug verbosity

  if(CTX::instance()->files.empty()){
    Msg::Error("Mesh input file not given!");
    GmshFinalize();
    return 1;
  }

  // Create a new GModel and make it current
  GModel* gm = new GModel();
  GModel::current(GModel::list.size() - 1);

  MergeFile(CTX::instance()->files[0]);

  // Mesh if geometry file was read (does nothing if mesh file was read)
  gm->mesh(3);
  gm->writeMSH("cloak.msh");

  // Initialize the manifolds
  rm::RiemannianManifold<3> mat1(gm, 2001); // air
  rm::RiemannianManifold<3> mat2(gm, 2002); // dielectric
  rm::RiemannianManifold<3> all(gm, 2003);
  rm::RiemannianManifold<2> lft(gm, 1001);
  rm::RiemannianManifold<2> rgh(gm, 1002);

  rm::RiemannianManifold<2> lftp(gm, 1007);
  rm::RiemannianManifold<2> rghp(gm, 1008);


  rm::RiemannianManifold<2> top(gm, 1003);
  rm::RiemannianManifold<2> bot(gm, 1004);

  rm::RiemannianManifold<2> frt(gm, 1005);
  rm::RiemannianManifold<2> bck(gm, 1006);

  rm::RiemannianManifold<2> mat2bd(gm, 1010);

  // Solve a reference problem where the dielectric material is present
  rm::ScalarPoissonData<3> problem(&all);

  Permittivity eps(&all, &mat1, &mat2);
  problem.setMaterial(&eps);

  problem.addTraceCond(&lftp, 0.);
  problem.addTraceCond(&rghp, 1.);
  rm::MeshField<rm::Covector<3, 0> > phir = problem.solve();
  phir.writeMSH("phir.msh", rm::MODEL, rm::MODELCOORDS);


  // Compute reference field energy
  double Er = 0.5*rm::i( (*d(phir))%(d(phir)), &all);

  rm::MeshField<rm::Covector<3, 1> > dphir = d(phir);
  rm::MeshField<rm::Covector<3, 2> > hdphir = h(eps*dphir);
  dphir.writeMSH("er.msh", rm::MODEL, rm::MODELCOORDS);
  hdphir.writeMSH("dr.msh", rm::MODEL, rm::MODELCOORDS);

  // solve for new x-, y-, and z-coordinates where the dielectric
  // material in the model geometry is diminished

  XBoundaryCond xcond(&mat2bd);
  YBoundaryCond ycond(&mat2bd);
  ZBoundaryCond zcond(&mat2bd);

  problem.setMaterial(&all, 1.);

  problem.clearTraceConds();
  problem.addTraceCond(&lft, -1.);
  problem.addTraceCond(&rgh, 1.);
  problem.addTraceCond(&xcond);
  rm::MeshField<rm::Covector<3, 0> > x = problem.solve();
  x.writeMSH("x.msh");

  problem.clearTraceConds();
  problem.addTraceCond(&top, 1.);
  problem.addTraceCond(&bot, -1.);
  problem.addTraceCond(&ycond);
  rm::MeshField<rm::Covector<3, 0> > y = problem.solve();
  y.writeMSH("y.msh");

  problem.clearTraceConds();
  problem.addTraceCond(&frt, 1.);
  problem.addTraceCond(&bck, -1.);
  problem.addTraceCond(&zcond);
  rm::MeshField<rm::Covector<3, 0> > z = problem.solve();
  z.writeMSH("z.msh");


  // Create a new manifold whose chart is such that the dielectric material
  // is diminished

  // Exterior derivates of the coordinate functions, will serve as the
  // Jacobian matrix of the coordinate transformation
  rm::MeshField<rm::Covector<3, 1> > dx = d(x);
  rm::MeshField<rm::Covector<3, 1> > dy = d(y);
  rm::MeshField<rm::Covector<3, 1> > dz = d(z);

  // Map from model coordinates to the new chart coordinates
  // We declare it is a diffeomorphism
  rm::FieldModelMap<3> mm(gm, x, y, z, dx, dy, dz, rm::MM_DIFFMORPH);

  // Create the manifold
  rm::RiemannianManifold<3> all2(gm, 2003, rm::USERCHART, &mm,
                                 rm::EUCLIDEANMETRIC);
  all2.drawChart("transformedChart.msh");


  // Solve the cloaking problem where the dielectric material doesn't
  // *seem* to be present


  // Set pullback metric on the manifold from the manifold on whose
  // chart there's no dielectric material
  all.setRiemannianMetric(*all2.getRiemannianMetric());
  all.getRiemannianMetric()->writeMSH("g.msh", rm::MODEL, rm::MODELCOORDS);

  problem.clearTraceConds();
  problem.addTraceCond(&lftp, 0.);
  problem.addTraceCond(&rghp, 1.);
  rm::MeshField<rm::Covector<3, 0> > phi = problem.solve();
  phi.writeMSH("phi.msh", rm::MODEL, rm::MODELCOORDS);

  // Pullback the solution to the manifold where the dielectric
  // material is not present
  rm::MeshField<rm::Covector<3, 0> > pbphi = pullback(phi, &all2);
  pbphi.writeMSH("pbphi.msh", rm::MANIFOLD, rm::MANIFOLDCOORDS);

  // Compute cloaking field energy
  double E = 0.5*rm::i( *(d(phi))%d(phi), &all);


  MetricRelPermittivity anisoeps(&all);
  anisoeps.writeMSH("anisoeps.msh", rm::MODEL, rm::MODELCOORDS);
  all.setRiemannianMetric(rm::MODELMETRIC);
  problem.setMaterial(&anisoeps);
  rm::MeshField<rm::Covector<3, 0> > phir2 = problem.solve();
  phir2.writeMSH("phir2.msh", rm::MODEL, rm::MODELCOORDS);

  double Er2 = 0.5*rm::i( *(d(phir2))%d(phir2), &all);

  Msg::Info("Reference problem field energy: %g", Er);
  Msg::Info("Reference problem 2 field energy: %g", Er2);
  Msg::Info("Cloak problem field energy: %g", E);



  GmshFinalize();

  delete gm;
}
