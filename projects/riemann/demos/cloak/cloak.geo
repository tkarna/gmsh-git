m = 0.05;
m2 = 2*m;

a = 1;
b = 1;
c = 1;

w = 0.8;
h = 0.8;

p = newp;
Point(p) = {0, 0, 0, m2};
Point(p+1) = {a, b, -c, m2};
Point(p+2) = {a, -b, -c, m2};
Point(p+3) = {-a, -b, -c, m2};
Point(p+4) = {-a, b, -c, m2};

l = newl;
Line(l+1) = {p+1, p+2};
Line(l+2) = {p+2, p+3};
Line(l+3) = {p+3, p+4};
Line(l+4) = {p+4, p+1};

ll = newll;
Line Loop(ll+1) = {l+1, l+2, l+3, l+4};
s = news;
Plane Surface(s+1) = {ll+1};

Point(p+5) = {a, b, c, m2};
Point(p+6) = {a, -b, c, m2};
Point(p+7) = {-a, -b, c, m2};
Point(p+8) = {-a, b, c, m2};

Line(l+5) = {p+5, p+6};
Line(l+6) = {p+6, p+7};
Line(l+7) = {p+7, p+8};
Line(l+8) = {p+8, p+5};

Line Loop(ll+2) = {l+5, l+6, l+7, l+8};
Plane Surface(s+2) = {ll+2};

Line(l+9) = {p+1, p+5};
Line(l+10) = {p+2, p+6};
Line(l+11) = {p+3, p+7};
Line(l+12) = {p+4, p+8};

Line Loop(ll+3) = {l+1, l+10, -(l+5), -(l+9)};
Line Loop(ll+4) = {l+2, l+11, -(l+6), -(l+10)};
Plane Surface(s+3) = {ll+4};
Line Loop(ll+5) = {l+3, l+12, -(l+7), -(l+11)};
Line Loop(ll+6) = {l+4, l+9, -(l+8), -(l+12)};
Plane Surface(s+4) = {ll+6};

p = newp;
Point(p+1) = {-a, h, -w, m2};
Point(p+2) = {-a, h, w, m2};
Point(p+3) = {-a, -h, w, m2};
Point(p+4) = {-a, -h, -w, m2};

Line(l+13) = {p+1, p+2};
Line(l+14) = {p+2, p+3};
Line(l+15) = {p+3, p+4};
Line(l+16) = {p+4, p+1};
Line Loop(ll+7) = {l+13, l+14, l+15, l+16};
Plane Surface(s+5) = {ll+5, ll+7};
Plane Surface(s+6) = {ll+7};

p = newp;
Point(p+1) = {a, h, -w, m2};
Point(p+2) = {a, h, w, m2};
Point(p+3) = {a, -h, w, m2};
Point(p+4) = {a, -h, -w, m2};

Line(l+17) = {p+1, p+2};
Line(l+18) = {p+2, p+3};
Line(l+19) = {p+3, p+4};
Line(l+20) = {p+4, p+1};
Line Loop(ll+8) = {l+17, l+18, l+19, l+20};
Plane Surface(s+7) = {ll+3, ll+8};
Plane Surface(s+8) = {ll+8};


r1 = 0.4;
r2 = 0.3;
r3 = 0.2;

p = newp;
Point(p) = {0, 0, 0, m};
Point(p+1) = {r1, 0, 0, m};
Point(p+2) = {-r1, 0, 0, m};
Point(p+3) = {0, r2, 0, m};
Point(p+4) = {0, -r2, 0, m};
Point(p+5) = {0, 0, r3, m};
Point(p+6) = {0, 0, -r3, m};

l = newl;
Ellipse(l+1) = {p+3, p, p, p+1};
Ellipse(l+2) = {p+1, p, p, p+4};
Ellipse(l+3) = {p+4, p, p, p+2};
Ellipse(l+4) = {p+2, p, p, p+3};

Ellipse(l+5) = {p+3, p, p, p+5};
Ellipse(l+6) = {p+5, p, p, p+4};
Ellipse(l+7) = {p+4, p, p, p+6};
Ellipse(l+8) = {p+6, p, p, p+3};

Ellipse(l+9) = {p+1, p, p, p+5};
Ellipse(l+10) = {p+5, p, p, p+2};
Ellipse(l+11) = {p+2, p, p, p+6};
Ellipse(l+12) = {p+6, p, p, p+1};

ll = newll;
Line Loop(35) = {23, 31, -27};
Ruled Surface(s+9) = {35};
Line Loop(37) = {24, -28, -31};
Ruled Surface(s+10) = {37};
Line Loop(39) = {29, 34, 24};
Ruled Surface(s+11) = {39};
Line Loop(41) = {33, -29, 25};
Ruled Surface(s+12) = {41};
Line Loop(43) = {32, -25, -28};
Ruled Surface(s+13) = {43};
Line Loop(45) = {30, 23, -34};
Ruled Surface(s+14) = {45};
Line Loop(47) = {33, 30, -26};
Ruled Surface(s+15) = {47};
Line Loop(49) = {26, 27, 32};
Ruled Surface(s+16) = {49};

sl = newsl;
Surface Loop(sl) = {s+1, s+2, s+3, s+4, s+5, s+6, s+7, s+8};
Surface Loop(sl+1) = {s+9, s+10, s+11, s+12, s+13, s+14, s+15, s+16};
v = newv;
Volume(v) = {sl, sl+1};
Volume(v+1) = {sl+1};

skin_void[] = CombinedBoundary{ Volume{v+1}; };

LEFT = 1001;
RIGHT = 1002;
TOP = 1003;
BOTTOM = 1004;
FRONT = 1005;
BACK = 1006;
LEFTP = 1007;
RIGHTP = 1008;

SKIN_MAT2 = 1010;


MAT1 = 2001;
MAT2 = 2002;
ALL = 2003;

Physical Surface(LEFTP) = {s+6};
Physical Surface(RIGHTP) = {s+8};

Physical Surface(LEFT) = {s+5, s+6};
Physical Surface(RIGHT) = {s+7, s+8};
Physical Surface(TOP) = {s+4};
Physical Surface(BOTTOM) = {s+3};
Physical Surface(FRONT) = {s+2};
Physical Surface(BACK) = {s+1};
Physical Surface(SKIN_MAT2) = {skin_void[]};

Physical Volume(MAT2) = {v};
Physical Volume(MAT1) = {v+1};
Physical Volume(ALL) = {v+1, v};


