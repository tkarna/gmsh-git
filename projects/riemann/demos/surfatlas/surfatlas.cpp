// Riemannian manifold example project
//
// An example in an upcoming Ph.D. thesis [2].
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

// [2] Matti Pellikka, Tampere University of Technology, Tampere, Finland, 2013.

#include "Riemann.h"
#include "ScalarPoissonSolver.h"



int main(int argc, char **argv)
{
  // Initialize Gmsh
  GmshInitialize(argc, argv);
  GmshSetOption("General","Terminal", 1.); // terminal mode
  GmshSetOption("General","Verbosity",99.); // debug verbosity


  if(CTX::instance()->files.empty()){
    Msg::Error("Mesh input file not given!");
    GmshFinalize();
    return 1;
  }

  // Create a new GModel and make it current
  GModel* gm = new GModel();
  GModel::current(GModel::list.size() - 1);

  MergeFile(CTX::instance()->files[0]);

  if(gm->getMeshStatus() < 2) {
    Msg::Error("Mesh input file not given!");
    GmshFinalize();
    return 1;
  }

  // Initialize the manifolds
  std::map<int, rm::Manifold<2>*> surfaces =
    rm::RiemannianManifold<2>::createManifoldsFromPhysicals(gm);
  std::map<int, rm::Manifold<1>*> boundaries =
    rm::RiemannianManifold<1>::createManifoldsFromPhysicals(gm);

  if(surfaces.empty()) {
    Msg::Error("No physical group surface found from the Gmsh model");
    return 0;
  }

  if(surfaces.size() != 1 || boundaries.size() > 1) {
    Msg::Warning("More than one surface and boundary physical groups found, using the ones with lowest physical group number");
  }

  rm::Manifold<2>* surface = surfaces.begin()->second;
  rm::Manifold<1>* boundary = NULL;

  if(!boundaries.empty()) {
    surface->setBoundary(boundaries.begin()->second);
    boundary = boundaries.begin()->second;
  }

  std::vector<int> cohTypes(4, 1); // 0 -> relative, 1 -> absolute
  cohTypes.at(2) = 0;
  cohTypes.at(3) = 0;

  int cohBegin = 4;

  // solve the problem for each cut
  for(unsigned int i = 0; i < cohTypes.size(); i++) {
    rm::ScalarPoissonData<2> problem(surface);

    rm::CochainField<2,1,double> z(surface, cohBegin+i);
    problem.addCohomologyCond(&z, 1., false);

    if(!cohTypes.at(i) && boundary != NULL)
      problem.addTraceCond(surface->getBoundary(), 0);

    rm::MeshField<rm::Covector<2,0> > y1 = problem.solve();
    rm::MeshField<rm::Covector<2,0> > y2 = z.getLocalPotential();
    rm::MeshField<rm::Covector<2,0> > x = y1 - y2;

    std::stringstream ss;
    ss << "x";
    ss << i+1;
    ss << ".msh";

    x.writeMSH(ss.str());

  }


  GmshFinalize();

  delete gm;
}
