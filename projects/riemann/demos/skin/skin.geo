m = 0.02;
m2 = 30*m;

//Mesh.Algorithm3D = 4;

//m = 0.01;
//m2 = 30*m;

a = 1;
b = 1;
c = 1;

p = newp;
Point(p) = {0, 0, 0, m2};
Point(p+1) = {a, b, -c, m2};
Point(p+2) = {a, -b, -c, m2};
Point(p+3) = {-a, -b, -c, m2};
Point(p+4) = {-a, b, -c, m2};

l = newl;
Line(l+1) = {p+1, p+2};
Line(l+2) = {p+2, p+3};
Line(l+3) = {p+3, p+4};
Line(l+4) = {p+4, p+1};

ll = newll;
Line Loop(ll+1) = {l+1, l+2, l+3, l+4};
s = news;
Plane Surface(s+1) = {ll+1};

Point(p+5) = {a, b, c, m2};
Point(p+6) = {a, -b, c, m2};
Point(p+7) = {-a, -b, c, m2};
Point(p+8) = {-a, b, c, m2};

Line(l+5) = {p+5, p+6};
Line(l+6) = {p+6, p+7};
Line(l+7) = {p+7, p+8};
Line(l+8) = {p+8, p+5};

Line Loop(ll+2) = {l+5, l+6, l+7, l+8};
Plane Surface(s+2) = {ll+2};

Line(l+9) = {p+1, p+5};
Line(l+10) = {p+2, p+6};
Line(l+11) = {p+3, p+7};
Line(l+12) = {p+4, p+8};

Line Loop(ll+3) = {l+1, l+10, -(l+5), -(l+9)};
Plane Surface(s+3) = {ll+3};
Line Loop(ll+4) = {l+2, l+11, -(l+6), -(l+10)};
Plane Surface(s+4) = {ll+4};
Line Loop(ll+5) = {l+3, l+12, -(l+7), -(l+11)};
Plane Surface(s+5) = {ll+5};
Line Loop(ll+6) = {l+4, l+9, -(l+8), -(l+12)};
Plane Surface(s+6) = {ll+6};

p = newp;
a = 0.3;
b = 0.3;
c = 0.1;
Point(p+1) = {a, 0.05, b+0.05, m};
Point(p+2) = {a, 0, 0, m};

Point(p+3) = {a, 0.2, -b, m};
Point(p+4) = {0, 0.02-0.1, -b, m};

Point(p+5) = {-a-0.02, 0.1, -b, m};
Point(p+6) = {-a-0.05, 0, 0, m};

Point(p+7) = {-a+0.03, -0.2, b, m};
Point(p+8) = {0, 0, b, m};

l = newl;
Spline(l+1) = {p+1, p+2, p+3};
Spline(l+2) = {p+3, p+4, p+5};
Spline(l+3) = {p+5, p+6, p+7};
Spline(l+4) = {p+7, p+8, p+1};

ll = newll;
Line Loop(ll) = {l+1, l+2, l+3, l+4};
Ruled Surface(s+7) = {ll};

tmp[] = Extrude {0, c, 0} { Surface{s+7}; };

skin[] = CombinedBoundary{ Volume{tmp[1]}; };

sl = newsl;
Surface Loop(sl) = {s+1, s+2, s+3, s+4, s+5, s+6};
Surface Loop(sl+1) = {skin[]};
v = newv;
Volume(v) = {sl, sl+1};

skintopbd[] = CombinedBoundary{ Surface{skin[0]}; };

ALL = 3000;
COND = 3001;
AIR = 3002;

SKIN = 2001;
TOP = 2002;
BOT = 2003;

SKINTOP = 2004;

SKINTOPBD = 1004;

Physical Volume(COND) = {tmp[]};
Physical Volume(AIR) = {v};
Physical Volume(ALL) = {v, tmp[]};
Physical Surface(SKIN) = {-skin[0], skin[1], skin[2], skin[3], skin[4], skin[5]};
Physical Surface(SKINTOP) = {skin[0]};
Physical Line(SKINTOPBD) = {skintopbd[]};

Physical Surface(BOT) = {s+4};
Physical Surface(TOP) = {s+6};
