// Riemannian manifold example project
//
// An example in an upcoming Ph.D. thesis [2].
//
// See http://www.mcs.anl.gov/petsc/documentation/faq.html#zeropivot
// if you get zero pivot error in PETSc
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

// [2] Matti Pellikka, Tampere University of Technology, Tampere, Finland, 2013.

#include <unistd.h>
#include "Riemann.h"
#include "SkinSolver.h"

class BackgroundField : public rm::DefinedField<rm::Covector<3,2> >
{
private:
  double _intensity;
  int _direction;
public:
  BackgroundField(const rm::Manifold<3>* m, double intensity, int direction) :
    rm::DefinedField<rm::Covector<3,2> >(m),
    _intensity(intensity), _direction(direction) {}


  virtual void getValue(const rm::Point* p, rm::Covector<3,2>& tp) const
  {
    double c[3] = {0., 0., 0.};
    c[_direction] = _intensity;
    tp = rm::Covector<3,2>(this->getManifold(), p, c, rm::MODEL);
  }
};

class NormalField : public rm::DefinedField<rm::Covector<3,1> >
{
private:
  const rm::Manifold<2>* _s;
public:
  NormalField(const rm::Manifold<3>* m, const rm::Manifold<2>* s) :
    rm::DefinedField<rm::Covector<3,1> >(m),
    _s(s) {}


  virtual void getValue(const rm::Point* p, rm::Covector<3,1>& tp) const
  {
    double cs[1] = {1.};
    rm::Covector<2,2> covs(_s, p, cs, rm::MANIFOLD);

    double o = this->getManifold()->getOrientation(p->getMeshElement());

    rm::Covector<3,2> cov = pullback(covs, this->getManifold());
    double norm = sqrt((cov*cov)(0));
    cov = cov/norm*o;
    tp = h(cov);
  }
};

struct Options
{
  double mmf;
  double sigma;
  double mur;
  double freq;
};

Options getOptions(int argc, char **argv)
{
  Options options;
  options.mmf = 4e5;
  options.sigma = 6e7;
  options.mur = 1;
  options.freq = 100;

  int opt;
  char* fvalue;
  char* cvalue;

  while((opt = getopt(argc, argv, "freq:c:")) != -1) {
    switch (opt) {
    case 'freq':
      fvalue = optarg;
      options.freq = atof(fvalue);
      break;
    case 'c':
      cvalue = optarg;
      options.sigma = atof(cvalue);
      break;
    default:
      ;
    }
  }

  return options;
}

int main(int argc, char **argv)
{
  Options opt = getOptions(argc, argv);

  // Initialize Gmsh
  GmshInitialize(argc, argv);
  GmshSetOption("General","Terminal", 1.); // terminal mode
  GmshSetOption("General","Verbosity",99.); // debug verbosity

  if(CTX::instance()->files.empty()){
    Msg::Error("Mesh input file not given!");
    GmshFinalize();
    return 1;
  }

  // Create a new GModel and make it current
  GModel* gm = new GModel();
  GModel::current(GModel::list.size() - 1);

  MergeFile(CTX::instance()->files[0]);

  // Mesh if geometry file was read (does nothing if mesh file was read)
  gm->mesh(3);

  // Initialize the manifolds
  rm::RiemannianManifold<2> skin(gm, 2001);

  std::vector<int> oris;
  std::vector<GEntity*> ents;
  skin.getEntities(ents, oris);

  rm::RiemannianManifold<3> skinemb(gm, 2001, rm::MODELXYZ);
  rm::RiemannianManifold<2> skintop(gm, 2004);
  rm::RiemannianManifold<1> skintopbd(gm, 1004);

  rm::RiemannianManifold<2> bot(gm, 2003);
  rm::RiemannianManifold<2> top(gm, 2002);
  rm::RiemannianManifold<3> all(gm, 3000);
  rm::RiemannianManifold<3> cond(gm, 3001);
  rm::RiemannianManifold<3> air(gm, 3002);

  // Create background time harmonic magnetic flux density, for testing
  BackgroundField b(&all, 1., 1);
  rm::MeshField<rm::Covector<2,2> > tb = -pullback(b, &skin);
  double netflux = rm::i(tb, &skin);
  double onesideflux = rm::i(tb, &skintop);
  Msg::Info("\n net flux %g oneside flux %g ", netflux, onesideflux);
  tb.writeMSH("tb.msh", rm::MANIFOLD);

  // Field normal to the conductor surface, for testing and post-processing
  NormalField n(&skinemb, &skin);
  n.writeMSH("n.msh");

  // Compute conductor volume, for testing
  rm::MetricVolumeForm<3> unit(&cond);
  unit.writeMSH("unit.msh");
  double volume = rm::i(unit, &cond);
  Msg::Info(" Conductor vulume: %g ", volume);

  // Run solver
  rm::SkinSolver ssolver(&air, &cond, &skin, &top, &bot,
                         opt.mmf, opt.sigma, opt.mur, opt.freq);
  double loss = ssolver.solve();
  double sd = ssolver.getSkinDepth();

  // Get results
  rm::MeshField<rm::Covector<3,0> > psi1_r = ssolver.getPsi(0, 0);
  rm::MeshField<rm::Covector<3,0> > psi1_i = ssolver.getPsi(0, 1);
  rm::MeshField<rm::Covector<3,0> > psi2_r = ssolver.getPsi(1, 0);
  rm::MeshField<rm::Covector<3,0> > psi2_i = ssolver.getPsi(1, 1);
  rm::MeshField<rm::Covector<2,0> > chi_r = ssolver.getChi(0);
  rm::MeshField<rm::Covector<2,0> > chi_i = ssolver.getChi(1);

  psi1_r.writeMSH("psi1_r.msh");
  psi1_i.writeMSH("psi1_i.msh");
  psi2_r.writeMSH("psi2_r.msh");
  psi2_i.writeMSH("psi2_i.msh");
  chi_r.writeMSH("chi_r.msh");
  chi_i.writeMSH("chi_i.msh");

  rm::MeshField<rm::Covector<3, 2> > jr = 1./sd*pullback(d(chi_r), &skinemb)%n;
  jr.writeMSH("jr.msh", rm::MODEL, rm::MODELCOORDS, rm::POS);
  rm::MeshField<rm::Covector<3, 2> > ji = 1./sd*pullback(d(chi_i), &skinemb)%n;
  ji.writeMSH("ji.msh", rm::MODEL, rm::MODELCOORDS, rm::POS);

  rm::MeshField<rm::Covector<3, 1> > dpsi1_r = d(psi1_r);
  dpsi1_r.writeMSH("h1r.msh");
  rm::MeshField<rm::Covector<3, 1> > dpsi1_i = d(psi1_i);
  dpsi1_i.writeMSH("h1i.msh");

  rm::MeshField<rm::Covector<3, 1> > dpsi2_r = d(psi2_r);
  dpsi2_r.writeMSH("h2r.msh");
  rm::MeshField<rm::Covector<3, 1> > dpsi2_i = d(psi2_i);
  dpsi2_i.writeMSH("h2i.msh");


  GmshFinalize();

  delete gm;

  return 1;
}
