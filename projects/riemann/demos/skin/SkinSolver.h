// Riemannian manifold example project
//
// An example in an upcoming Ph.D. thesis [2].
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

// [2] Matti Pellikka, Tampere University of Technology, Tampere, Finland, 2013.

#include "Riemann.h"
#include "dofManager.h"
#include "OS.h"
#include "linearSystem.h"
#include "linearSystemFull.h"
#include "linearSystemPETSc.h"
#include "linearSystemGMM.h"

namespace rm {

  // compute integrands hodge(kappa*ds_i)^ds_j in an element on n-manifold
  template<int n>
  void computeLocalMatrix(const Manifold<n>* man, Field<Covector<n,0> >* kappa,
                          MElement* me, fullMatrix<double>& localMatrix) {

    int intOrderN1 = 2;
    int intOrderN2 = 1;

    const int integrationOrder =
      intOrderN1*me->getPolynomialOrder() + intOrderN2;

    ManifoldIntegrationPoints<n> ips(man, me, integrationOrder);
    for(int k = 0; k < ips.getNumPoints(); k++) {
      Point* ip = ips.getPoint(k);
      double weight = ips.getWeight(k);

      std::vector<Covector<n,0> > sf;
      man->getPartOfUnity(ip, sf);
      std::vector<Covector<n,1> > dsf;
      man->getDiffPartOfUnity(ip, dsf);

      // Compute integrands
      for(int iS = 0; iS < me->getNumShapeFunctions(); iS++) {

        // apply hodge and material parameter
        Covector<n, 0> kappap = kappa->operator()(ip);
        Covector<n, 1> kappadsfi = kappap(0)*dsf.at(iS);
        Covector<n,n-1> ai = h(kappadsfi);

        for(int jS = 0; jS <= iS; jS++) {
          Covector<n,n> aij = ai % dsf.at(jS);
          localMatrix(iS, jS) += aij(0)*weight;
        }
      }
    } // integration point

    for(int i = 0; i < me->getNumShapeFunctions(); i++) {
      for(int j = 0; j < i; j++) {
        localMatrix(j, i) += localMatrix(i, j);
      }
    }

  }

  // Solves surface currents on conductor surface placed in uniform
  // time-harmonic magnetic field.
  // The unknown fields psi1 and psi2 correspond to magnetic scalar
  // potentials inside and outside the conductor, the stream function for
  // the surface current is then psi1-psi2 on the conductor surface
  class SkinSolver
  {
  private:
    const Manifold<3>* _m[2]; // air, cond
    const Manifold<2>* _s; // skin

    const Manifold<2>* _sh[2]; // magnetic terminals

    int _solver;

    double _psi1[2];
    double _psi2[2];

    double _pi;
    double _muv;

    double _omega;
    double _d;
    double _mur;
    double _sigma;

    Field<Covector<2,0> >* _rho;
    bool _ownrho;
    Field<Covector<3,0> >* _mu[2];
    bool _ownmu[2];

    linearSystem<double>* _lsys;
    dofManager<double>* _assembler;

    MeshField<Covector<3,0> > _psi_r[2];
    MeshField<Covector<3,0> > _psi_i[2];
    MeshField<Covector<2,0> > _chi_r;
    MeshField<Covector<2,0> > _chi_i;
    MeshField<Covector<2,0> > _lag_r;
    MeshField<Covector<2,0> > _lag_i;

    int _psiRealTag(const int i) const {
      return 2*i+1;
    }
    int _psiImagTag(const int i) const {
      return 2*i+2;
    }
    int _chiRealTag() const { return 5; }
    int _chiImagTag() const { return 6; }

    // Assemble a symmetric element-wise matrix to system matrix,
    // possibly with imaginary entries
    void _assemble(MElement* me, fullMatrix<double>& localMatrix, double coeff,
                   int reTag, int imTag=0, bool imag=false)
    {
      fullMatrix<double> mat(localMatrix.size1(), localMatrix.size2());
      mat.axpy(localMatrix, coeff);

      const int nbC = mat.size2();
      std::vector<Dof> Cre;
      std::vector<Dof> Cim;

      for (int jS = 0; jS < nbC; jS++) {
        int num = me->getShapeFunctionNode(jS)->getNum();
        Cre.push_back(Dof(num, reTag));
        if(imTag) Cim.push_back(Dof(num, imTag));
      }

      if(!imag) {
        _assembler->assemble(Cre, mat);
        if(imTag) _assembler->assemble(Cim, mat);
      }
      else if(imTag && imag) {
        fullMatrix<double> matNeg(mat.size1(),
                                  mat.size2());
        matNeg.axpy(mat, -1.);
        _assembler->assemble(Cre, Cim, matNeg);
        _assembler->assemble(Cim, Cre, mat);
      }
    }

    // Assemble non-symmetric element-wise matrix to system matrix,
    // possibly with imaginary entries
    void _assemble(MElement* meR, MElement* meC,
                   fullMatrix<double> localMatrix, double coeff,
                   int reTagR, int reTagC, int imTagR=0, int imTagC=0,
                   bool imag=false)
    {
      fullMatrix<double> mat(localMatrix.size1(), localMatrix.size2());
      mat.axpy(localMatrix, coeff);

      std::vector<Dof> Rre;
      std::vector<Dof> Cre;
      std::vector<Dof> Rim;
      std::vector<Dof> Cim;

      for (int jS = 0; jS < meC->getNumShapeFunctions(); jS++) {
        int num = meC->getShapeFunctionNode(jS)->getNum();
        Cre.push_back(Dof(num, reTagC));
        if(imTagC) Cim.push_back(Dof(num, imTagC));
      }
      for (int iS = 0; iS < meR->getNumShapeFunctions(); iS++) {
        int num = meR->getShapeFunctionNode(iS)->getNum();
        Dof re(num, reTagR);
        Rre.push_back(Dof(num, reTagR));
        if(imTagR) Rim.push_back(Dof(num, imTagR));
      }

      if(!imag) {
        _assembler->assemble(Rre, Cre, mat);
        if(imTagR && imTagC)
          _assembler->assemble(Rim, Cim, mat);
      }
      else if(imTagR && imTagC && imag) {
        fullMatrix<double> matNeg(mat.size1(),
                                  mat.size2());
        matNeg.axpy(mat, -1.);
        _assembler->assemble(Rre, Cim, matNeg);
        _assembler->assemble(Rim, Cre, mat);
      }
    }

    void _init()
    {
      double eps = 1e-15;
      if(_omega < eps || _sigma < eps || _mur < eps) {
        Msg::Fatal("Frequency, resistivity, or permeability is zero");
      }

      _d = sqrt(2./(_sigma*_omega*(_muv*_mur)));

      double rho = 1./(_sigma*_d);

      double rhop[1] = {rho};
      if(_ownrho) delete _rho;
      _rho = new ConstField<Covector<2,0> >(_s, rhop, MODEL);
      _ownrho = true;

      double mu = _muv*_mur;
      double mup[1] = {mu};
      for(int i = 0; i < 2; i++) {
        if(_ownmu[i]) delete _mu[i];
        _mu[i] = new ConstField<Covector<3,0> >(_m[i], mup, MODEL);
        _ownmu[i] = true;
        _psi_r[i] = MeshField<Covector<3,0> >(_m[i]);
        _psi_i[i] = MeshField<Covector<3,0> >(_m[i]);
      }

      _chi_r = MeshField<Covector<2,0> >(_s);
      _chi_i = MeshField<Covector<2,0> >(_s);

      _lag_r = MeshField<Covector<2,0> >(_s);
      _lag_i = MeshField<Covector<2,0> >(_s);

      double freq = _omega/(2.*_pi);

      Msg::Info("Skin problem init:");
      Msg::Info("  Frequency:         %g Hz", freq);
      Msg::Info("  Permeability:      %g N/A^2", mu);
      Msg::Info("  Conductivity:      %g S/m", _sigma);
      Msg::Info("  Skin depth:        %g m", _d);
      Msg::Info("  Skin conductivity: %g S", _sigma*_d);

    }


  public:
    SkinSolver(Manifold<3>* ma, Manifold<3>* mc, Manifold<2>* s,
               Manifold<2>* sh1, Manifold<2>* sh2,
               double MMF=4e5, double sigma=3e7, double mur=1., double freq=50)
    {
      _solver = 2;

      _psi1[0] = MMF;
      _psi1[1] = 0.;
      _psi2[0] = 0.;
      _psi2[1] = 0.;
      _sh[0] = sh1;
      _sh[1] = sh2;
      _m[0] = ma;
      _m[1] = mc;
      _s = s;
      _sigma = sigma;
      _mur = mur;

      _ownrho = false;
      _ownmu[0] = false;
      _ownmu[1] = false;

      _pi = 3.14159265359;
      _muv = 4*_pi*1e-7;
      _omega = 2.*_pi*freq;

      _init();


      _lsys = NULL;

      if(_solver == 1) {
        Msg::Info("Skin solver uses LAPACK");
        _lsys =  new linearSystemFull<double>();
      }
      else if(_solver == 2) {
#ifndef HAVE_PETSC
        Msg::Error("PETSc not available");
        _lsys =  new linearSystemFull<double>();
#else
        Msg::Info("Skin solver uses PETSc");
        _lsys = new linearSystemPETSc<double>();
#endif
      }
      else if(_solver == 3) {
#ifndef HAVE_GMM
        Msg::Error("GMM not available");
        _lsys = new linearSystemFull<double>();
#else
        Msg::Info("Skin solver uses Gmm");
        linearSystemGmm<double>* lsysgmm = new linearSystemGmm<double>();
        lsysgmm->setNoisy(1);
        lsysgmm->setGmres(0);
        _lsys = lsysgmm;
#endif
      }

      // Gmsh linear system assembler
      _assembler = new dofManager<double>(_lsys);

    }

    void setFreq(double freq) {
      _omega = 2.*_pi*freq;
      _init();
    }
    double getSkinDepth() const {
      return _d;
    }

    double solve()
    {

      // Fix dirichlet conditions
      int numElemsM[2] = {0, 0};
      for(int iM = 0; iM < 2; iM++) {
        const Manifold<2>* man2 = _sh[iM];
        for(MElemIt it = man2->firstMeshElement();
            it != man2->lastMeshElement(); it++) {
          MElement* me = it->first;
          for(int iV = 0; iV < me->getNumVertices(); iV++) {
            int vert = me->getVertex(iV)->getNum();
            if(iM == 0) {
              _assembler->fixDof(vert, _psiRealTag(0), _psi1[0]);
              _assembler->fixDof(vert, _psiImagTag(0), _psi1[1]);
            }
            if(iM == 1) {
              _assembler->fixDof(vert, _psiRealTag(0), _psi2[0]);
              _assembler->fixDof(vert, _psiImagTag(0), _psi2[1]);
            }
          }
        }
      }

      bool fixed = false;
      int numElemsS = 0;
      for(MElemIt it = _s->firstMeshElement();
          it != _s->lastMeshElement(); it++) {
        MElement* me = it->first;
        numElemsS++;
      }

      // enumerate all degrees of freedom
      for(int iM = 0; iM < 2; iM++) {
        const Manifold<3>* man3 = _m[iM];
        for(MElemIt it = man3->firstMeshElement();
            it != man3->lastMeshElement(); it++) {
          MElement* me = it->first;
          numElemsM[iM]++;
          for(int iV = 0; iV < me->getNumVertices(); iV++) {
            int vert = me->getVertex(iV)->getNum();
            _assembler->numberDof(vert, _psiRealTag(iM));
            _assembler->numberDof(vert, _psiImagTag(iM));
          }
        }
      }

      Msg::Info("Solving skin problem:");
      Msg::Info("  3D Elements Air:   %d", numElemsM[0]);
      Msg::Info("  3D Elements Cond:  %d", numElemsM[1]);
      Msg::Info("  2D Elements:       %d", numElemsS);
      Msg::Info("  DoF's:             %d", _assembler->sizeOfR());
      Msg::Info("  Constraints:       %d", _assembler->sizeOfF());


      double t1 = Cpu();
      Msg::StatusBar(true, "Assembling system ...");

      // Iterate manifold M mesh elements
      Msg::StatusBar(true, "... Manifold M ...");
      for(int iM = 0; iM < 2; iM++) {
        const Manifold<3>* man = _m[iM];
        for(MElemIt it = man->firstMeshElement();
            it != man->lastMeshElement(); it++) {
          MElement* me = it->first;
          fullMatrix<double> localMatrix(me->getNumShapeFunctions(),
                                         me->getNumShapeFunctions());
          computeLocalMatrix<3>(man, _mu[iM], me, localMatrix);

          // Assemble local matrix and vector to linear system
          _assemble(me, localMatrix, _omega,
                    _psiRealTag(iM), _psiImagTag(iM), true);

        } // MElem m
      } // manifold m

      // Iterate manifold S mesh elements
      Msg::StatusBar(true, "... Manifold S ...");
      for(MElemIt it = _s->firstMeshElement();
          it != _s->lastMeshElement(); it++) {
        MElement* me = it->first;
        fullMatrix<double> localMatrix(me->getNumShapeFunctions(),
                                       me->getNumShapeFunctions());

        computeLocalMatrix<2>(_s, _rho, me, localMatrix);

        _assemble(me, localMatrix, -1.,
                  _psiRealTag(0), _psiImagTag(0));
        _assemble(me, localMatrix, 1.,
                  _psiRealTag(1), _psiImagTag(1));

        _assemble(me, me, localMatrix, 1.,
                  _psiRealTag(1), _psiRealTag(0),
                  _psiImagTag(1), _psiImagTag(0));
        _assemble(me, me, localMatrix, -1.,
                  _psiRealTag(0), _psiRealTag(1),
                  _psiImagTag(0), _psiImagTag(1));

      }

      double t2 = Cpu();
      Msg::StatusBar(true, "System assembled (%g s)", t2-t1);

      Msg::StatusBar(true, "Solving system ...");
      t1 = Cpu();
      _lsys->systemSolve();
      t2 = Cpu();
      Msg::StatusBar(true, "System solved (%g s)", t2-t1);

      // collect results for manifold M
      for(int iM = 0; iM < 2; iM++) {
        for(MElemIt it = _m[iM]->firstMeshElement();
            it != _m[iM]->lastMeshElement(); it++) {

          MElement* me = it->first;
          for(int iV = 0; iV < me->getNumVertices(); iV++) {
            double re = 123.;
            double im = 123.;
            Dof dofr(me->getVertex(iV)->getNum(), _psiRealTag(iM));
            Dof dofi(me->getVertex(iV)->getNum(), _psiImagTag(iM));
            _assembler->getDofValue(dofr, re);
            _assembler->getDofValue(dofi, im);

            double cr[1] = {re};
            double ci[1] = {im};
            _psi_r[iM].assign(cr, me, iV);
            _psi_i[iM].assign(ci, me, iV);
          }
        }
      }

      // collect results for manifold S
      for(MElemIt it = _s->firstMeshElement();
          it != _s->lastMeshElement(); it++) {
        MElement* me = it->first;

        for(int iV = 0; iV < me->getNumVertices(); iV++) {

          double re[2] = {0., 0.};
          double im[2] = {0., 0.};
          for(int iM = 0; iM < 2; iM++) {
            Dof dofRe(me->getVertex(iV)->getNum(), _psiRealTag(iM));
            Dof dofIm(me->getVertex(iV)->getNum(), _psiImagTag(iM));
            _assembler->getDofValue(dofRe, re[iM]);
            _assembler->getDofValue(dofIm, im[iM]);
          }
          double cr[1] = {(re[1] - re[0])};
          double ci[1] = {(im[1] - im[0])};
          _chi_r.assign(cr, me, iV);
          _chi_i.assign(ci, me, iV);

        }
      }

      _assembler->systemClear();
      _lsys->clear();

      MeshField<Covector<2,1> > dchir = d(_chi_r);
      MeshField<Covector<2,1> > dchii = d(_chi_i);
      dchir.writeMSH("dchir.msh");
      dchii.writeMSH("dchii.msh");

      double loss = rm::i( dchir % (h((*_rho)*dchir)) +
                           dchii % (h((*_rho)*dchii)), _s );
      Msg::Info("Eddy current losses: %g W", loss );

      double energy = 0;
      for(int iM = 0; iM < 2; iM++) {
        MeshField<Covector<3,1> > dpsir = d(_psi_r[iM]);
        MeshField<Covector<3,1> > dpsii = d(_psi_i[iM]);
        double e = rm::i( dpsir % (h((*_mu[iM])*dpsir)) +
                          dpsii % (h((*_mu[iM])*dpsii)), _m[iM] );
        energy += e;
      }
      Msg::Info("Magnetic field energy: %g J", energy );

      return loss;

    } // solve

    ~SkinSolver() {
       delete _lsys;
       delete _assembler;
       if(_ownrho) delete _rho;
       for(int i = 0; i < 2; i++)
         if(_ownmu[i]) delete _mu[i];
    }

    MeshField<Covector<3,0> > getPsi(int i, int reim)
    {
      if(reim == 1) return _psi_i[i];
      else return _psi_r[i];
    }
    MeshField<Covector<2,0> > getChi(int reim)
    {
      if(reim == 1) return _chi_i;
      else return _chi_r;
    }
    MeshField<Covector<2,0> > getLag(int reim)
    {
      if(reim == 1) return _lag_i;
      else return _lag_r;
    }

  };

}
