m = 0.1;

p = newp;

Point(p+1) = {0, 0.2, 0.9, m};
Point(p+2) = {1, 0.1, 1, m};
Point(p+3) = {2, 0, 1, m};

Point(p+4) = {0, 1, 0, m};
Point(p+5) = {1, 0.8, 0, m};
Point(p+6) = {2, 1.1, 0, m};

Point(p+7) = {0, 0.2, 0.5, m};
Point(p+8) = {2, 0.6, 0.5, m};

sp = newreg;
Spline(sp+1) = {p+1, p+2, p+3};
Spline(sp+2) = {p+4, p+5, p+6};
Spline(sp+3) = {p+1, p+7, p+4};
Spline(sp+4) = {p+3, p+8, p+6};

ll = newll;
Line Loop(ll) = {sp+2, -(sp+4), -(sp+1), sp+3};
s = news;
Ruled Surface(s) = {ll};

TOP = 1001;
BOT = 1002;
LFT = 1003;
RGH = 1004;
SURF = 2000;

Physical Surface(SURF) = {s};
Physical Line(BOT) = {sp+1};
Physical Line(TOP) = {sp+2};
Physical Line(LFT) = {sp+3};
Physical Line(RGH) = {sp+4};
