// Riemannian manifold example project
//
// An example in an article [1] and in an upcoming Ph.D. thesis [2].
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics


// [1] M. Pellikka, T. Tarhasaari, S. Suuriniemi, L. Kettunen, A programming interface to the Riemannian manifold in a finite element environment, Journal of Computational and Applied Mathematics, Volume 246, July 2013, Pages 225-233, ISSN 0377-0427, 10.1016/j.cam.2012.10.022.
// [2] Matti Pellikka, Tampere University of Technology, Tampere, Finland, 2013.

#include "Riemann.h"
#include "ScalarPoissonSolver.h"



int main(int argc, char **argv)
{
  // Initialize Gmsh
  GmshInitialize(argc, argv);
  GmshSetOption("General","Terminal", 1.); // terminal mode
  GmshSetOption("General","Verbosity",99.); // debug verbosity


  if(CTX::instance()->files.empty()){
    Msg::Error("Mesh input file not given!");
    GmshFinalize();
    return 1;
  }

  // Create a new GModel and make it current
  GModel* gm = new GModel();
  GModel::current(GModel::list.size() - 1);

  MergeFile(CTX::instance()->files[0]);

  // Mesh if geometry file was read (does nothing if mesh file was read)
  gm->mesh(2);

  // Initialize the manifolds
  rm::RiemannianManifold<2> surf(gm, 2000);
  rm::RiemannianManifold<1> bot(gm, 1002);
  rm::RiemannianManifold<1> lft(gm, 1003);
  rm::RiemannianManifold<1> top(gm, 1001);
  rm::RiemannianManifold<1> rgh(gm, 1004);


  // Solve a Laplace problem on the geometry
  //   d*d \phi = 0 in the domain
  //   \phi = 1 on left boundary
  //   \phi = 0 on right boundary
  //   t*d \phi = 0 on top and bottom boundaries
  rm::ScalarPoissonData<2> problem(&surf);
  problem.addTraceCond(&lft, 0.);
  problem.addTraceCond(&rgh, 1.);
  rm::MeshField<rm::Covector<2, 0> > xi1 = problem.solve();

  // Solve other problem with
  //   \phi = 1 on top boundary
  //   \phi = 0 on bottom boundary
  problem.clearTraceConds();
  problem.addTraceCond(&top, 1.);
  problem.addTraceCond(&bot, 0.);
  rm::MeshField<rm::Covector<2, 0> > xi2 = problem.solve();

  // Write results
  xi1.writeMSH("xi1.msh");
  xi2.writeMSH("xi2.msh");

  // Compute the exterior derivatives
  rm::MeshField<rm::Covector<2, 1> > dxi1 = d(xi1);
  rm::MeshField<rm::Covector<2, 1> > dxi2 = d(xi2);

  // Create a map from R^3 to R^2 from the fields
  // whose Jacobian matrix rows are dxi1 and dxi2
  rm::FieldModelMap<2> param(gm, xi1, xi2, dxi1, dxi2, rm::MM_DIFFMORPH);

  // Create a Riemannian manifold whose coordinate chart is given
  // by the above map, it metric tensor is the pullback of the
  // Euclidean metric in R^3
  rm::RiemannianManifold<2> plane(gm, 2000, rm::USERCHART,
                                  &param, rm::MODELMETRIC);

  // Write the metric tensor in a file, the component functions
  // are with respect the manifold coordinate chart,
  // and the mesh is transformed to the manifold coordinate chart
  plane.getRiemannianMetric()->writeMSH("g.msh", rm::MANIFOLD,
                                        rm::MANIFOLDCOORDS);
  GmshFinalize();

  delete gm;
}
