Riemannian manifold examples in an upcoming doctoral thesis [1]

To install:
-----------
mkdir build
cd build
cmake ..
ccmake ..
ENABLE_BUILD_DYNAMIC ON
ENABLE_BUILD_SHARED ON
ENABLE_MPI ON
ENABLE_PETSC ON
ENABLE_SLEPC OFF
make 

To run:
-------
./cloak ../cloak/cloak.geo  
./skin ../skin/skin.geo  
./patch ../patch/patch.geo  
./heightmap ../heightmap/heightmap.geo  
./surfatlas ../surfatlas/surf.msh  

[1] Matti Pellikka, Tampere University of Technology, Tampere, Finland, 2013.

-- 
Matti Pellikka, <matti.pellikka@tut.fi>
Tampere University of Technology, Electromagnetics
