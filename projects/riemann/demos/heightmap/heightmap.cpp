// Riemannian manifold example project
//
// An example in an article [1] and in an upcoming Ph.D. thesis [2].
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics


// [1] M. Pellikka, T. Tarhasaari, S. Suuriniemi, L. Kettunen, A programming interface to the Riemannian manifold in a finite element environment, Journal of Computational and Applied Mathematics, Volume 246, July 2013, Pages 225-233, ISSN 0377-0427, 10.1016/j.cam.2012.10.022.
// [2] Matti Pellikka, Tampere University of Technology, Tampere, Finland, 2013.

#include "Riemann.h"
#include "ScalarPoissonSolver.h"

// User defined model map from model to 3-manifold
class HeightMap : public rm::ModelMap<3>
{
private:
  PView* _heightmap;
  double _scale;
  double _h;

  double _getValue(double x, double y, double z) const
  {
    double val = 0.;
    if(!_heightmap->getData()->searchScalar(x,y,z, &val)) val = 0.;
    return _scale*val;
  }
  double _getDiffValueX(double x, double y, double z) const
  {
    double vals[2];
    if(!_heightmap->getData()->searchScalar(x-_h,y,z, &vals[0]))
      if(!_heightmap->getData()->searchScalar(x,y,z, &vals[0]))
        vals[0] = 0.;
    if(!_heightmap->getData()->searchScalar(x+_h,y,z, &vals[1]))
      if(!_heightmap->getData()->searchScalar(x,y,z, &vals[1]))
        vals[1] = 0.;
    return _scale*(vals[1] - vals[0])/(2*_h);
  }
  double _getDiffValueY(double x, double y, double z) const
  {
    double vals[2];
    if(!_heightmap->getData()->searchScalar(x,y-_h,z, &vals[0]))
      if(!_heightmap->getData()->searchScalar(x,y,z, &vals[0]))
        vals[0] = 0.;
    if(!_heightmap->getData()->searchScalar(x,y+_h,z, &vals[1]))
      if(!_heightmap->getData()->searchScalar(x,y,z, &vals[1]))
        vals[1] = 0.;
    return _scale*(vals[1] - vals[0])/(2*_h);
  }

public:
  // Constructor
  HeightMap(GModel* gm, PView* heightmap, double scale, double h=1.5) :
    rm::ModelMap<3>(gm, rm::MM_NUMERIC, rm::MM_DIFF),
  _heightmap(heightmap), _scale(scale), _h(h) {}

  // Define the mapping here,
  // how a model point maps to manifold's coordinate chart
  virtual void mapPoint(const rm::Point* p, double c[3]) const
  {
    double x,y,z;
    p->getXYZ(x,y,z);
    c[0] = x;
    c[1] = y;
    //c[2] = 100*sin(1./100.*x)*sin(1./100.*y);
    c[2] = _getValue(x, y, z);
  }

  // Define the Jacobian matrix of the mapping here
  virtual void getModelToManifoldJacobian(const rm::Point* p,
                                          double jac[3*3]) const
  {
    double x,y,z;
    p->getXYZ(x,y,z); // Model coordinates of the point p


    // Define the Jacobian matrix here,
    // J_ij = dx2_i/dx_j, where x2 = x2(x_0, x_1, .., x_n)
    jac[0*3+0] = 1.;
    jac[0*3+1] = 0.;
    jac[0*3+2] = 0.;

    jac[1*3+0] = 0;
    jac[1*3+1] = 1.;
    jac[1*3+2] = 0.;

    // dh/dx
    jac[2*3+0] = _getDiffValueX(x, y, z);
    // dh/dy
    jac[2*3+1] = _getDiffValueY(x, y, z);

    jac[2*3+2] = 0.;
  }

};


int main(int argc, char **argv)
{
  // Initialize Gmsh
  GmshInitialize(argc, argv);
  GmshSetOption("General","Terminal", 1.); // terminal mode
  GmshSetOption("General","Verbosity",99.); // debug verbosity


  if(CTX::instance()->files.empty()){
    Msg::Error("Input geometry file not given!");
    GmshFinalize();
    return 1;
  }

  // Create a new GModel and make it current
  GModel* gm = new GModel();
  GModel::current(GModel::list.size() - 1);

  gm->readGEO(CTX::instance()->files[0]);

  if(PView::list.empty()) {
    Msg::Error("Input geometry file didn't merge an image file");
    GmshFinalize();
    return 1;
  }

  PView* hmapp = PView::list.at(0);

  // Mesh the geometry
  gm->mesh(2);

  // Create model map that is the height map of the image
  HeightMap hmap(gm, hmapp, 500.);

  // Create a riemannian 3-manifold whose chart is the height map
  rm::RiemannianManifold<3> surf(gm, 2000, rm::USERCHART, &hmap,
                                 rm::EUCLIDEANMETRIC);
  surf.drawChart("heightmap.msh");

  // Initialize the manifolds
  rm::RiemannianManifold<2> plane(gm, 2000);
  rm::RiemannianManifold<1> bot(gm, 1002);
  rm::RiemannianManifold<1> lft(gm, 1003);
  rm::RiemannianManifold<1> top(gm, 1001);
  rm::RiemannianManifold<1> rgh(gm, 1004);

  // Set pullback metric on the manifold
  plane.setRiemannianMetric(*surf.getRiemannianMetric());

  // Solve a Laplace problem on the geometry
  //   d*d \phi = 0 in the domain
  //   \phi = 1 on left boundary
  //   \phi = 0 on right boundary
  //   t*d \phi = 0 on top and bottom boundaries
  rm::ScalarPoissonData<2> problem(&plane);
  problem.addTraceCond(&lft, 0.);
  problem.addTraceCond(&rgh, 1.);
  rm::MeshField<rm::Covector<2, 0> > f = problem.solve();

  // To view result on the heightmap manifold
  rm::MeshField<rm::Covector<3, 0> > pbf = pullback(f, &surf);

  // Write results
  f.writeMSH("f.msh");
  pbf.writeMSH("pbf.msh", rm::MANIFOLD, rm::MANIFOLDCOORDS);

  // Compute the exterior derivative
  rm::MeshField<rm::Covector<2, 1> > df = d(f);
  df.writeMSH("df.msh");

  // Compute the gradient (sharp of the exterior derivative)
  rm::MeshField<rm::Vector<2, 1> > gradf = ~df;
  gradf.writeMSH("gradf.msh");

  // Pushforward gradient vector field to the height map manifold
  rm::MeshField<rm::Vector<3, 1> > pfgradf = pushforward(gradf, &surf);

  // Write vector field coefficient representation
  pfgradf.writeMSH("pfgradf.msh", rm::MANIFOLD, rm::MANIFOLDCOORDS, rm::POS);

  // Write metric tensor of the manifold
  plane.getRiemannianMetric()->writeMSH("g.msh", rm::MODEL, rm::MODELCOORDS);


  // Create new model from the height map
  GModel* gm2 = rm::ModelMapN::createModel(&hmap);
  gm2->writeMSH("heightmapmodel.msh");

  // Interpret it as a 2-manifold
  rm::RiemannianManifold<2> plane2(gm2, 2000);
  rm::RiemannianManifold<1> lft2(gm2, 1003);
  rm::RiemannianManifold<1> rgh2(gm2, 1004);

  // Solve the same problem
  rm::ScalarPoissonData<2> problem2(&plane2);
  problem2.addTraceCond(&lft2, 0.);
  problem2.addTraceCond(&rgh2, 1.);
  rm::MeshField<rm::Covector<2, 0> > f2 = problem2.solve();
  rm::MeshField<rm::Covector<2, 1> > df2 = d(f2);
  rm::MeshField<rm::Vector<2, 1> > gradf2 = ~df2;
  gradf2.writeMSH("gradf2.msh", rm::MODEL, rm::MODELCOORDS, rm::POS);


  GmshFinalize();

  delete gm;
}
