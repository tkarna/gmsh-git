Merge "heightmap.png";

w = View[0].MaxX;
h = View[0].MaxY;

m = w*0.02;

p = newp;
Point(p+1) = {0, 0, 0, m};
Point(p+2) = {w, 0, 0, m};
Point(p+3) = {w, h, 0, m};
Point(p+4) = {0, h, 0, m};

l = newl;
Line(l+1) = {p+1,p+2};
Line(l+2) = {p+2,p+3};
Line(l+3) = {p+3,p+4};
Line(l+4) = {p+4,p+1};
ll = newll;
Line Loop(ll) = {l+3,l+4,l+1,l+2};
s = news;
Plane Surface(s) = {ll};

TOP = 1001;
BOT = 1002;
LFT = 1003;
RGH = 1004;
SURF = 2000;

Physical Surface(SURF) = {s};
Physical Line(BOT) = {l+1};
Physical Line(TOP) = {l+3};
Physical Line(LFT) = {l+4};
Physical Line(RGH) = {l+2};
