// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#ifndef _RM_VECTORSPACE_H_
#define _RM_VECTORSPACE_H_

namespace rm {

  template <class V, class S>
  class VectorSpaceCat
  {

  public:
    /// instantiated by the user
    virtual V& operator+=(const V& v) = 0;
    virtual V& operator*=(const S& s) = 0;
    //virtual V& zero() = 0;
    /// ---------------------

    friend V operator+(const V& v1, const V& v2) {
      V temp(v1);
      temp += v2;
      return temp;
    }
    friend V operator-(const V& v1, const V& v2) {
      V temp(v1);
      temp -= v2;
      return temp;
    }
    friend V operator*(const S& s, const V& v) {
      V temp(v);
      temp*=s;
      return temp;
    }
    friend V operator*(const V& v, const S& s) {
      return s*v;
    }
    friend V operator/(const V& v, const S& s) {
      S invs = 1./s;
      return invs*v;
    }

    // Warning: assummes that the multiplicative
    // identity element of field S can be converted from double "1."
    // and that additive inverse of v in abelian group V equals v*-1.
    // (true e.g. for double and std::complex<double>)
    // otherwise these need to be overridden by the user

    virtual V& operator-() {
      return (*this)*=(-1.);
    }
    virtual V& operator/=(const S& s) {
      S temp = 1./s;
      return (*this*=temp);
    }
    virtual V& operator-=(const V& v) {
      V temp(v);
      temp = -temp;
      return (*this+=temp);
    }


  };


  // no self-assigment operations for this vector space category
  template <class V, class Vc, class S>
  class VectorSpaceCatConst
  {


  public:
    /// instantiated by the user
    virtual V operator+(const Vc& v) const = 0;
    virtual V operator*(const S& s) const = 0;
    /// ---------------------


    V operator-() const {
      return (*this)*(-1);
    }

    friend V operator-(const Vc& v1, const Vc& v2) {
      V temp(v2);
      temp = -temp;
      return v1+temp;
    }
    friend V operator*(const S& s, const Vc& v) {
      return v*s;
    }
    friend V operator/(const Vc& v, const S& s) {
      S invs = 1./s;
      return invs*v;
    }
  };

  template <class Type>
  class EquivalenceCat
  {

  public:

    /// instantiated by the user
    virtual bool isEquivalentTo(const Type& t) const = 0;

    friend bool operator==(const Type& t1, const Type& t2) {
      return t1.isEquivalentTo(t2);
    }
    friend bool operator!=(const Type& t1, const Type& t2) {
      return !t1.isEquivalentTo(t2);
    }

  };

  // Class whose derivative classes are to have partial or total order
  template <class Type>
  class PosetCat
  {
  public:

    /// instantiated in derived classes
    virtual bool lessThan(const Type& t2) const = 0;

    friend bool operator<(const Type& t1, const Type& t2)
    {
      return t1.lessThan(t2);
    }
    friend bool operator>(const Type& t1, const Type& t2)
    {
      return !t1.lessThan(t2);
    }
    friend bool operator==(const Type& t1, const Type& t2) {
      if(t1.lessThan(t2) && t2.lessThan(t1)) return true;
      return false;
    }
    friend bool operator!=(const Type& t1, const Type& t2) {
      if(t1.lessThan(t2) && t2.lessThan(t1)) return false;
      return true;
    }
    friend bool operator<=(const Type& t1, const Type& t2)
    {
      if(t1.lessThan(t2) || t1==t2) return true;
      return false;
    }
    friend bool operator>=(const Type& t1, const Type& t2)
    {
      if(!t1.lessThan(t2) || t1==t2) return true;
      return false;
    }

  };

}

#endif
