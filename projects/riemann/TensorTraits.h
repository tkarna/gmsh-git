// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#ifndef _RM_TENSOR_TRAITS_H_
#define _RM_TENSOR_TRAITS_H_

#include "RiemannDefines.h"

namespace rm
{
  template <int n, int k, int l, TensorSymmetry s, int h> class Tensor;
  template <int n, int k> class Vector;
  template <int n, int k> class Covector;
  template <int n> class MetricTensor;
  template <int n> class LinTransform;

  template <class TensorType>
  struct TensorTraits
  {
    static const int dim = 0;
    static const int ctrIndex = 0;
    static const int coIndex = 0;
    static const int harmonic = 0;
    static const TensorSymmetry symm = FULL;
    static const TensorSpace space = COTANGENT;
    static const int numCoeffs = 1;
    static const int numModelCoeffs = 1;
  };

  template <int n, int h>
  struct TensorTraits<Tensor<n,0,0,SKEWSYMMETRIC,h> > {
    static const int dim = n;
    static const int ctrIndex = 0;
    static const int coIndex = 0;
    static const int harmonic = h;
    static const TensorSymmetry symm = SKEWSYMMETRIC;
    static const TensorSpace space = COTANGENT;
    static const int numCoeffs = h;
    static const int numModelCoeffs = h;
  };

  template <int n, int k, int h>
  struct TensorTraits<Tensor<n,0,k,SKEWSYMMETRIC,h> > {
    static const int dim = n;
    static const int ctrIndex = 0;
    static const int coIndex = k;
    static const int harmonic = h;
    static const TensorSymmetry symm = SKEWSYMMETRIC;
    static const TensorSpace space = COTANGENT;
    static const int numCoeffs = h*(MAYBE(NCK(n,k)));
    static const int numModelCoeffs = h*(NCK(3,k));
  };

  template <int n, int k, int h>
  struct TensorTraits<Tensor<n,k,0,SKEWSYMMETRIC,h> > {
    static const int dim = n;
    static const int ctrIndex = k;
    static const int coIndex = 0;
    static const int harmonic = h;
    static const TensorSymmetry symm = SKEWSYMMETRIC;
    static const TensorSpace space = TANGENT;
    static const int numCoeffs = h*(MAYBE(NCK(n,k)));
    static const int numModelCoeffs = h*(NCK(3,k));
  };

  template <int n, int h>
  struct TensorTraits<Tensor<n,0,2,SYMMETRIC,h> > {
    static const int dim = n;
    static const int ctrIndex = 0;
    static const int coIndex = 2;
    static const int harmonic = h;
    static const TensorSymmetry symm = SYMMETRIC;
    static const TensorSpace space = COTANGENT;
    static const int numCoeffs = h*(MAYBE(SYMR2(n)));
    static const int numModelCoeffs = h*(SYMR2(3));
  };

  template <int n, int h>
  struct TensorTraits<Tensor<n,1,1,FULL,h> > {
    static const int dim = n;
    static const int ctrIndex = 1;
    static const int coIndex = 1;
    static const int harmonic = h;
    static const TensorSymmetry symm = FULL;
    static const TensorSpace space = MIXEDSPACE;
    static const int numCoeffs = h*(MAYBE(n*n));
    static const int numModelCoeffs = h*3*3;
  };


  template <int n, int k>
  struct TensorTraits<Covector<n, k> > {
    static const int dim = n;
    static const int ctrIndex = 0;
    static const int coIndex = k;
    static const TensorSymmetry symm = SKEWSYMMETRIC;
    static const TensorSpace space = COTANGENT;
    static const int numCoeffs = MAYBE(NCK(n,k));
    static const int numModelCoeffs = NCK(3,k);
  };

  template <int n, int k>
  struct TensorTraits<Vector<n, k> > {
    static const int dim = n;
    static const int ctrIndex = k;
    static const int coIndex = 0;
    static const TensorSymmetry symm = SKEWSYMMETRIC;
    static const TensorSpace space = TANGENT;
    static const int numCoeffs = MAYBE(NCK(n,k));
    static const int numModelCoeffs = NCK(3,k);
  };

  template <int n>
  struct TensorTraits<MetricTensor<n> > {
    static const int dim = n;
    static const int ctrIndex = 0;
    static const int coIndex = 2;
    static const TensorSymmetry symm = SYMMETRIC;
    static const TensorSpace space = COTANGENT;
    static const int numCoeffs = MAYBE(SYMR2(n));
    static const int numModelCoeffs = SYMR2(3);
  };

  template <int n>
  struct TensorTraits<LinTransform<n> > {
    static const int dim = n;
    static const int ctrIndex = 1;
    static const int coIndex = 1;
    static const TensorSymmetry symm = FULL;
    static const TensorSpace space = MIXEDSPACE;
    static const int numCoeffs = MAYBE(n*n);
    static const int numModelCoeffs = 3*3;
  };

}

#endif
