// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#ifndef _RM_UTILS_H_
#define _RM_UTILS_H_

#include "RiemannianManifold.h"
#include "Chain.h"
#include "BasisForm.h"

#if defined(HAVE_GINAC)
#include <ginac/ginac.h>
#endif

#if defined(HAVE_KBIPACK)
#include "Chain.h"
#endif

namespace rm
{
  /// Zero tensor field
  template <class TensorType>
  class ZeroField : public DefinedField<TensorType>
  {
  protected:

  public:
    ZeroField(const Manifold<TensorTraits<TensorType>::dim>* m) :
      DefinedField<TensorType>(m) {}

    virtual void getValue(const Point* p, TensorType& tp) const
    {
      const int  nc = TensorTraits<TensorType>::numCoeffs;
      double c[nc];
      vecinit<nc>(c);
      tp = TensorType(this->getManifold(), p, c, MANIFOLD);
    }
  };

  /// A tensor field with constant coefficients
  template <class TensorType>
  class ConstField : public DefinedField<TensorType>
  {
  protected:
    double _c[TensorTraits<TensorType>::numCoeffs];
    double _mc[TensorTraits<TensorType>::numModelCoeffs];
    Representation _r;
  public:
    ConstField(const Manifold<TensorTraits<TensorType>::dim>* m,
               const double* c, Representation r=MANIFOLD) :
      DefinedField<TensorType>(m), _r(r)
    {
      const int nc = TensorTraits<TensorType>::numCoeffs;
      const int ncm = TensorTraits<TensorType>::numModelCoeffs;
      if(r == MANIFOLD) {
        vecinit<nc>(c, _c);
        vecinit<ncm>(_mc);
      }
      else if (r == MODEL) {
        vecinit<ncm>(c, _mc);
        vecinit<nc>(_c);
      }
    }

    virtual void getValue(const Point* p, TensorType& tp) const
    {
      if(_r == MANIFOLD)
        tp = TensorType(this->getManifold(), p, _c, _r);
      else if(_r == MODEL)
        tp = TensorType(this->getManifold(), p, _mc, _r);
    }
  };

  /// Metric volume n-form on Riemannian n-manifold
  template <int n>
  class MetricVolumeForm : public DefinedField<Covector<n,n> >
  {
  public:
    MetricVolumeForm(const Manifold<n>* m) :
      DefinedField<Covector<n, n> >(m) {}

    void getValue(const Point* p, Covector<n,n>& cov) const
    {
      MetricTensor<n> g;
      this->getManifold()->getMetricTensor(p, g);

      double o = 1.;
      if(p->getMeshElement()->getDim() != 3)
        o = this->getManifold()->getOrientation(p->getMeshElement());

      const double* gc = g.getPointerToCoeffs();
      double c[1] = { o*sqrt(fabs(dets<n>(gc))) };
      cov = Covector<n,n>(this->getManifold(), p, c, MANIFOLD);
    }
  };


#if defined(HAVE_KBIPACK)
  template <int n, int k, class C>
  class CochainField : public DefinedField<Covector<n,k> >
  {
  private:
    Chain<C> _c;
  public:
    CochainField(const Manifold<n>* m, const Chain<C>& c) :
      DefinedField<Covector<n,k> >(m), _c(c)
    {
      if(c.getDim() != k)
        Msg::Fatal("Cannot create %d-covector field from %d-cochain",
                   k, c.getDim());
    }

    CochainField(const Manifold<n>* m, int tag,
                 C coefficient=1.0) :
      DefinedField<Covector<n,k> >(m)
    {
      _c = Chain<C>(m->getModel(), tag)*coefficient;

      if(_c.getDim() != k)
        Msg::Fatal("Cannot create %d-covector field from %d-cochain",
                   k, _c.getDim());
    }

    virtual void getValue(const Point* p, Covector<n,k>& tp) const
    {
      MElement* me = p->getMeshElement();
      tp = Covector<n,k>(this->getManifold(), p);

      if(!this->inSupport(me)) return;

      BasisForm<n,k> wf(this->getManifold());
      std::vector<Covector<n, k> > sf;
      wf.getCovectors(p, ELEMENTOR, sf);

      tp = Covector<n,k>(this->getManifold(), p);
      if(k == 0)
        for(int i = 0; i < me->getNumVertices(); i++)
          tp += _c.getCoefficient(me, i)*sf.at(i);
      else if(k == 1)
        for(int i = 0; i < me->getNumEdges(); i++)
          tp += _c.getCoefficient(me, i)*sf.at(i);
      else if(k == 2)
        for(int i = 0; i < me->getNumFaces(); i++)
          tp += _c.getCoefficient(me, i)*sf.at(i);
      else if(k == 3)
        tp += _c.getCoefficient(me)*sf.at(0);
    }

    bool inSupport(MElement* me) const {
      if(k == 0) {
        for(int i = 0; i < me->getNumVertices(); i++)
          if(_c.getCoefficient(me, i)) return true;
      }
      else if(k == 1) {
        for(int i = 0; i < me->getNumEdges(); i++)
          if(_c.getCoefficient(me, i)) return true;
      }

      else if(k == 2) {
        for(int i = 0; i < me->getNumFaces(); i++)
          if(_c.getCoefficient(me, i)) return true;
      }
      else if(k == 3) {
        if(_c.getCoefficient(me)) return true;
      }
      return false;
    }

    MeshField<Covector<n, 0> > getLocalPotential() {
      const Manifold<n>* m = this->getManifold();
      MeshField<Covector<n,0> > potential(m);

      for(MElemIt it = m->firstMeshElement();
          it != m->lastMeshElement(); it++) {
        MElement* me = it->first;
        for(int i = 0; i < me->getNumEdges(); i++) {
          C coeff = _c.getCoefficient(me, i);
          if(!coeff) continue;
          double val[1] = { fabs(coeff) };
          MEdge edge = me->getEdge(i);
          MVertex* v0 = edge.getVertex(0);
          MVertex* v1 = edge.getVertex(1);
          int v0num = -1;
          int v1num = -1;
          for(int j = 0; j < me->getNumVertices(); j++) {
            MVertex* ver = me->getVertex(j);
            if(ver == v0) v0num = j;
            else if(ver == v1) v1num = j;
          }
          if(coeff > 0) potential.assign(val, me, v0num);
          else if(coeff < 0) potential.assign(val, me, v1num);
        }
      }

      return potential;
    }

  };
#endif

  /// Metric tensor on n-manifold defined by n 1-covector fields
  /// g = sum_i (f_i (tensor product) f_i)
  template <int n, int m>
  class FieldMetric : public DefinedField<MetricTensor<n> >
  {
  private:
    std::vector<MeshField<Covector<n,1> > > _f;

    void _addField(Field<Covector<m,1> >& f)
    {
      const Manifold<n>* man = this->getManifold();
      ManifoldMap<m,n> mm(f.getManifold(), man);
      _f.push_back(MeshField<Covector<n,1> >(man));
      mm.transformFieldToCod(f, _f.back());
    }

  public:
    FieldMetric(Manifold<n>* man,
                Field<Covector<m,1> >& f1) :
      DefinedField<MetricTensor<n> >(man)
    {
      if(n != 1)
        Msg::Warning("One 1-covector fields given to define metric tensor on %d-manifold", n);
      _addField(f1);
    }
    FieldMetric(Manifold<n>* man,
                Field<Covector<m,1> >& f1,
                Field<Covector<m,1> >& f2) :
      DefinedField<MetricTensor<n> >(man)
    {
      if(n != 2)
        Msg::Warning("Two 1-covector fields given to define metric tensor on %d-manifold", n);
      _addField(f1);
      _addField(f2);
    }
    FieldMetric(Manifold<n>* man,
                Field<Covector<m,1> >& f1,
                Field<Covector<m,1> >& f2,
                Field<Covector<m,1> >& f3) :
      DefinedField<MetricTensor<n> >(man)
    {
      if(n != 3)
        Msg::Warning("Three 1-covector fields given to define metric tensor on %d-manifold", n);
      _addField(f1);
      _addField(f2);
      _addField(f3);
    }

    virtual void getValue(const Point* p, MetricTensor<n>& gp) const
    {
      gp = MetricTensor<n>(this->getManifold(), p);
      for(unsigned int i = 0; i < _f.size(); i++)
        gp += symtprod(_f.at(i)(p), _f.at(i)(p));
    }

    ~FieldMetric()
    {
      //for(unsigned int i = 0; i < _f.size(); i++) delete _f.at(i);
    }
  };

  // Model map to n-manifold defined by n 1-covector fields df_i,
  // Jacobian matrix element J_ij = df_i's j:th component
  template <int n>
  class FieldModelMap : public ModelMap<n>
  {
  private:
    std::vector<Field<Covector<n,0> >*> _f;
    std::vector<Field<Covector<n,1> >*> _df;
  public:

  FieldModelMap(GModel* gm,
                const Field<Covector<n,1> >& dx,
                ModelMapProp mp=MM_DIFF) :
    ModelMap<n>(gm, MM_NUMERIC, mp)
    {
      if(n != 1) {
        Msg::Fatal("One 1-covector field cannot define a model map to %d-manifold.", n);
      }
      _df.push_back(new MeshField<Covector<n, 1> >(dx));
      _f.push_back(new MeshField<Covector<n, 0> >(i(dx)));
    }

    FieldModelMap(GModel* gm,
                  const Field<Covector<n,0> >& x,
                  ModelMapProp mp=MM_DIFF) :
    ModelMap<n>(gm, MM_NUMERIC, mp)
    {
      if(n != 1) {
        Msg::Fatal("One 0-covector field cannot define a model map to %d-manifold.", n);
      }
      _df.push_back(new MeshField<Covector<n, 1> >(d(x)));
      _f.push_back(new MeshField<Covector<n, 0> >(x));
    }

    FieldModelMap(GModel* gm,
                  const Field<Covector<n,0> >& x,
                  const Field<Covector<n,1> >& dx,
                  ModelMapProp mp=MM_DIFF) :
    ModelMap<n>(gm, MM_NUMERIC, mp)
    {
      if(n != 1) {
        Msg::Fatal("One 0-covector field cannot define a model map to %d-manifold.", n);
      }
      _df.push_back(new MeshField<Covector<n, 1> >(dx));
      _f.push_back(new MeshField<Covector<n, 0> >(x));
    }

    FieldModelMap(GModel* gm,
                  const Field<Covector<n,1> >& dx,
                  const Field<Covector<n,1> >& dy,
                  ModelMapProp mp=MM_DIFF) :
      ModelMap<n>(gm, MM_NUMERIC, mp)
    {
      if(n != 2) {
        Msg::Fatal("Two 1-covector fields cannot define a model map to %d-manifold.", n);
      }
      _df.push_back(new MeshField<Covector<n, 1> >(dx));
      _df.push_back(new MeshField<Covector<n, 1> >(dy));
      _f.push_back(new MeshField<Covector<n, 0> >(i(dx)));
      _f.push_back(new MeshField<Covector<n, 0> >(i(dy)));
      _f.at(0)->writeMSH("u.msh");
      _f.at(1)->writeMSH("v.msh");
    }
    FieldModelMap(GModel* gm,
                  const Field<Covector<n,0> >& x,
                  const Field<Covector<n,0> >& y,
                  ModelMapProp mp=MM_DIFF) :
      ModelMap<n>(gm, MM_NUMERIC, mp)
    {
      if(n != 2) {
        Msg::Fatal("Two 0-covector fields cannot define a model map to %d-manifold.", n);
      }
      _df.push_back(new MeshField<Covector<n, 1> >(d(x)));
      _df.push_back(new MeshField<Covector<n, 1> >(d(y)));
      _f.push_back(new MeshField<Covector<n, 0> >(x));
      _f.push_back(new MeshField<Covector<n, 0> >(y));
    }

    FieldModelMap(GModel* gm,
                  const Field<Covector<n,0> >& x,
                  const Field<Covector<n,0> >& y,
                  const Field<Covector<n,1> >& dx,
                  const Field<Covector<n,1> >& dy,
                  ModelMapProp mp=MM_DIFF) :
      ModelMap<n>(gm, MM_NUMERIC, mp)
    {
      if(n != 2) {
        Msg::Fatal("Two 0-covector fields cannot define a model map to %d-manifold.", n);
      }
      _df.push_back(new MeshField<Covector<n, 1> >(dx));
      _df.push_back(new MeshField<Covector<n, 1> >(dy));
      _f.push_back(new MeshField<Covector<n, 0> >(x));
      _f.push_back(new MeshField<Covector<n, 0> >(y));
    }

    FieldModelMap(GModel* gm,
                  const Field<Covector<n,1> >& dx,
                  const Field<Covector<n,1> >& dy,
                  const Field<Covector<n,1> >& dz,
                  ModelMapProp mp=MM_DIFF) :
      ModelMap<n>(gm, MM_NUMERIC, mp)
    {
      if(n != 3) {
        Msg::Fatal("Three 1-covector fields cannot define a model map to %d-manifold.", n);
      }
      _df.push_back(new MeshField<Covector<n, 1> >(dx));
      _df.push_back(new MeshField<Covector<n, 1> >(dy));
      _df.push_back(new MeshField<Covector<n, 1> >(dz));
      _f.push_back(new MeshField<Covector<n, 0> >(i(dx)));
      _f.push_back(new MeshField<Covector<n, 0> >(i(dy)));
      _f.push_back(new MeshField<Covector<n, 0> >(i(dz)));
    }

    FieldModelMap(GModel* gm,
                  const Field<Covector<n,0> >& x,
                  const Field<Covector<n,0> >& y,
                  const Field<Covector<n,0> >& z,
                  ModelMapProp mp=MM_DIFF) :
      ModelMap<n>(gm, MM_NUMERIC, mp)
    {
      if(n != 3) {
        Msg::Fatal("Three 1-covector fields cannot define a model map to %d-manifold.", n);
      }
      _df.push_back(new MeshField<Covector<n, 1> >(d(x)));
      _df.push_back(new MeshField<Covector<n, 1> >(d(y)));
      _df.push_back(new MeshField<Covector<n, 1> >(d(z)));
      _f.push_back(new MeshField<Covector<n, 0> >(x));
      _f.push_back(new MeshField<Covector<n, 0> >(y));
      _f.push_back(new MeshField<Covector<n, 0> >(z));
    }

    FieldModelMap(GModel* gm,
                  const Field<Covector<n,0> >& x,
                  const Field<Covector<n,0> >& y,
                  const Field<Covector<n,0> >& z,
                  const Field<Covector<n,1> >& dx,
                  const Field<Covector<n,1> >& dy,
                  const Field<Covector<n,1> >& dz,
                  ModelMapProp mp=MM_DIFF) :
      ModelMap<n>(gm, MM_NUMERIC, mp)
    {
      if(n != 3) {
        Msg::Fatal("Three 1-covector fields cannot define a model map to %d-manifold.", n);
      }
      _df.push_back(new MeshField<Covector<n, 1> >(dx));
      _df.push_back(new MeshField<Covector<n, 1> >(dy));
      _df.push_back(new MeshField<Covector<n, 1> >(dz));
      _f.push_back(new MeshField<Covector<n, 0> >(x));
      _f.push_back(new MeshField<Covector<n, 0> >(y));
      _f.push_back(new MeshField<Covector<n, 0> >(z));
    }

    virtual void mapPoint(const Point* p, double c[n]) const
    {
      if(_f.at(0) == NULL) {
        Msg::Warning("Point mapping not defined for this model map.");
        vecinit<n>(c);
      }
      else {
        Point* p2;
        const Manifold<n>* m0;
        if(_f.size() > 0) {
          Field<Covector<n,0> >& f0 = *_f.at(0);
          m0 = f0.getManifold();
          p2 = m0->getPoint(p);
          c[0] = f0(p2)(0);
        }
        else for(int i = 0; i < n; i++) c[i] = 0.;

        if(_f.size() > 1) {
          Field<Covector<n,0> >& f1 = *_f.at(1);
          if(f1.getManifold()->getTag() != m0->getTag()) {
            Point* p3 = f1.getManifold()->getPoint(p);
            c[1] = f1(p3)(0);
            delete p3;
          }
          else c[1] = f1(p2)(0);
        }
        else for(int i = 1; i < n; i++) c[i] = 0.;

        if(_f.size() > 2) {
          Field<Covector<n,0> >& f2 = *_f.at(2);
          if(f2.getManifold()->getTag() != m0->getTag()) {
            Point* p3 = f2.getManifold()->getPoint(p);
            c[2] = f2(p3)(0);
            delete p3;
          }
          else c[2] = f2(p2)(0);
        }
        else for(int i = 2; i < n; i++) c[i] = 0.;
        delete p2;
      }
    }
    virtual void getModelToManifoldJacobian(const Point* p,
                                            double jac[n*3]) const
    {
      vecinit<n*3>(jac);
      if(_df.size() != n ) {
        Msg::Error("Differential of this map is not defined.");
        return;
      }
      const Manifold<n>* m0 = _df[0]->getManifold();
      Point* p2 = m0->getPoint(p);

      for(unsigned int i = 0; i < _df.size(); i++) {
        double mc[3];
        if(_df[i]->getManifold()->getNum() == m0->getNum())
          (*_df[i])(p2).getModelCoeffs(mc);
        else {
          Point* p3 = _df[i]->getManifold()->getPoint(p);
          (*_df[i])(p3).getModelCoeffs(mc);
          delete p3;
        }
        for(int j = 0; j < 3; j++) jac[i*3+j] = mc[j];
      }
      delete p2;
    }


    ~FieldModelMap()
    {
      for(unsigned int i = 0; i < _df.size(); i++) delete _df.at(i);
      for(unsigned int i = 0; i < _f.size(); i++) {
        if(_f.at(i) != NULL) delete _f.at(i);
      }
    }
  };

#if defined(HAVE_GINAC)
  const GiNaC::symbol& get_symbol(const std::string& s);
  double evalExpression(const double* x, const int nx,
                        const GiNaC::FUNCP_CUBA f);
  bool compileExpressions(const GiNaC::lst& expressions,
                          const std::vector<GiNaC::symbol>& symbols,
                          std::vector<GiNaC::FUNCP_CUBA>& funcptrs);

  template <int n>
  class SymbolicModelMap : public ModelMap<n>
  {
  protected:
    GiNaC::lst _f;
    GiNaC::lst _df;
    std::vector<GiNaC::symbol> _syms;

    bool _compiled;
    std::vector<GiNaC::FUNCP_CUBA> _fc;
    std::vector<GiNaC::FUNCP_CUBA> _dfc;

    void _initExpressions()
    {
      this->_mt = MM_SYMBOLIC;
      std::vector<GiNaC::symbol> syms;
      syms.push_back(get_symbol("xs"));
      syms.push_back(get_symbol("ys"));
      syms.push_back(get_symbol("zs"));
      _compiled = compileExpressions(_f, syms, _fc);
      _compiled = compileExpressions(_df, syms, _dfc);
    }

  public:
    SymbolicModelMap(GModel* m, GiNaC::ex fx, ModelMapProp mp=MM_DIFF)
      : ModelMap<n>(m, MM_SYMBOLIC, mp)
    {
      if(n != 1) {
        Msg::Fatal("One 0-covector fields cannot define a model map to %d-manifold.", n);
      }
      _f = GiNaC::lst(fx);
      _df = GiNaC::lst(fx.diff(get_symbol("xs")),
                       fx.diff(get_symbol("ys")),
                       fx.diff(get_symbol("zs")));
      _initExpressions();
    }
    SymbolicModelMap(GModel* m, GiNaC::ex fx, GiNaC::ex fy,
                     ModelMapProp mp=MM_DIFF)
      : ModelMap<n>(m, MM_SYMBOLIC, mp)
    {
      if(n != 2) {
        Msg::Fatal("Two 0-covector fields cannot define a model map to %d-manifold.", n);
      }
      _f = GiNaC::lst(fx, fy);
      _df = GiNaC::lst(fx.diff(get_symbol("xs")),
                       fx.diff(get_symbol("ys")),
                       fx.diff(get_symbol("zs")),
                       fy.diff(get_symbol("xs")),
                       fy.diff(get_symbol("ys")),
                       fy.diff(get_symbol("zs")));
      _initExpressions();
    }
    SymbolicModelMap(GModel* m, GiNaC::ex fx, GiNaC::ex fy, GiNaC::ex fz,
                     ModelMapProp mp=MM_DIFF)
      : ModelMap<n>(m, MM_SYMBOLIC, mp)
    {
      if(n != 3) {
        Msg::Fatal("Three 0-covector fields cannot define a model map to %d-manifold.", n);
      }
      _f = GiNaC::lst(fx, fy, fz);
      _df = GiNaC::lst(fx.diff(get_symbol("xs")),
                       fx.diff(get_symbol("ys")),
                       fx.diff(get_symbol("zs")),
                       fy.diff(get_symbol("xs")),
                       fy.diff(get_symbol("ys")),
                       fy.diff(get_symbol("zs")),
                       fz.diff(get_symbol("xs")),
                       fz.diff(get_symbol("ys")),
                       fz.diff(get_symbol("zs")));
      _initExpressions();
    }
    virtual GiNaC::ex getCoordExpr(unsigned int i) const
    {
      if(i < n) return _f[i];
      else return GiNaC::ex();
    }
    virtual GiNaC::matrix getJacobianExpr() const
    {
      return GiNaC::matrix(n, 3, _df);
    }

    virtual void mapPoint(const Point* p, double c[n]) const
    {
      if(!_compiled) {
        vecinit<n>(c);
        return;
      }
      double x,y,z;
      p->getXYZ(x,y,z);
      const double xyz[3] = {x,y,z};

      for(int i = 0; i < n; i++) {
        c[i] = evalExpression(xyz, 3, _fc.at(i));
      }
    }

    virtual void getModelToManifoldJacobian(const Point* p,
                                            double jac[n*3]) const
    {
      if(!_compiled) {
        vecinit<n*3>(jac);
        return;
      }

      double x,y,z;
      p->getXYZ(x,y,z);
      const double xyz[3] = {x,y,z};

      for(int i = 0; i < n*3; i++) {
        jac[i] = evalExpression(xyz, 3, _dfc.at(i));
      }
    }
  };
#endif

}

#endif
