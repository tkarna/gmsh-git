// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#ifndef _RM_MANIFOLD_INTEGRATION_POINTS_H_
#define _RM_MANIFOLD_INTEGRATION_POINTS_H_

#include "Manifold.h"

namespace rm
{

  /// Manifold integration points by mesh element
  template <int n>
  class ManifoldIntegrationPoints
  {
  private:

    /// Manifold to which the mesh element belongs
    const Manifold<n>* _m;

    /// The mesh element
    MElement* _me;

    /// Number of integration points on the mesh element
    int _numPoints;

    /// Integration point weights
    std::vector<double> _weights;

    /// Integration points
    std::vector<Point*> _points;

  public:

    /// Construct for a manifold mesh element of given order
    ManifoldIntegrationPoints(const Manifold<n>* m,
                              MElement* me, int pOrder) :
      _m(m), _me(me)
    {
      if(me->getDim() != n) {
        Msg::Warning("Manifold dimension different than its mesh elements dimension, consider integrating over %d-manifold instead.", me->getDim());
      }
      if(n > 3) {
        Msg::Fatal("Cannot integrate over %d-manifold with dimension larger than 3.",n);
      }
      IntPt* gp;
      me->getIntegrationPoints(pOrder, &_numPoints, &gp);
      for(int i = 0; i < _numPoints; i++) {
        Point* ip = new Point(_m->getModelMap()->getModel(),
                              _me, gp[i].pt[0], gp[i].pt[1], gp[i].pt[2]);
        _points.push_back(ip);
        _weights.push_back(gp[i].weight);
        ModelMapType mt = _m->getModelMap()->getModelMapType();
        if(mt != MM_UVW) {
          ModelMapUVW<n> mmuvw(_m->getModelMap()->getModel());
          double jac1[MAYBE(3*n)];
          double jac2[MAYBE(n*3)];
          mmuvw.getDiffMapToModel(ip, jac1, TANGENT);
          if(!_m->getModelMap()->getDiffMapToManifold(ip, jac2, TANGENT)) {
            Msg::Fatal("Model map does not provide contravariant tensor coefficient transformation to the manifold, cannot integrate over it.");
          }
          double jac[MAYBE(n*n)];
          matmat<n,3,n>(jac2, jac1, jac);
          _weights[i] *= det<n,n>(jac);
        }
      }
    }

    /// Get number of integration points on the mesh element
    int getNumPoints() const { return _numPoints; }

    /// Get a integration point
    Point* getPoint(int num) const { return _points.at(num); }

    /// Get a integration point weight
    double getWeight(int num) const { return _weights.at(num); }

    ~ManifoldIntegrationPoints()
    {
      for(unsigned int i = 0; i < _points.size(); i++) delete _points[i];
    }
  };

}

#endif

