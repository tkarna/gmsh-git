// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#ifndef _RM_POINT_H_
#define _RM_POINT_H_

#include "GModel.h"
#include "MElement.h"

namespace rm
{
  /// Point of a Gmsh model, in terms of a mesh
  class Point
  {
  private:
    /// Gmsh model this point belongs to
    GModel* _m;
    /// Mesh element parametric coordinates of this point
    mutable double _u, _v, _w;
    /// Mesh element of this point
    MElement* _me;
    /// Mesh element vertex this point is situated at,
    /// -1 if not situated at a vertex
    int _ver;
    /// Currently parametric (u, v, w) coordinates are not computed
    mutable bool _veronly;

    /// Cached model coordinates (x, y, z) of this point
    mutable double _x,_y,_z;
    /// Whether model (x, y, z) coordinates are computed
    mutable bool _xyz;

  public:

    /// Construct from mesh element parametric coordinates
    Point(GModel* m, MElement* me, double u, double v, double w) :
    _m(m), _u(u), _v(v), _w(w), _me(me), _ver(-1), _veronly(false),
    _xyz(false) {}

    /// Construct from mesh element parametric coordinates,
    /// with known model coordinates
    Point(GModel* m, MElement* me, double u, double v, double w,
          double x, double y, double z) :
      _m(m), _u(u), _v(v), _w(w), _me(me), _ver(-1), _veronly(false),
      _x(x), _y(y), _z(z), _xyz(true) {}

    /// Construct from mesh element and vertex index
    Point(GModel* m, MElement* me, int num) :
    _m(m), _me(me), _ver(num), _veronly(true), _xyz(false) {}

    /// Get Gmsh model
    inline GModel* getModel() const { return _m; }

    /// Get mesh element of this point
    inline MElement* getMeshElement() const { return _me; }

    /// Get mesh element vertex this point is situated at
    /// returns -1 if not situated at a vertex
    inline int getVertex() const { return _ver; }

    /// Get mesh element parametric coordinates of this point
    void getUVW(double& u, double &v, double& w) const;

    /// Get Gmsh model coordinates of this point
    void getXYZ(double& x, double &y, double& z) const;
    double getX() const;
    double getY() const;
    double getZ() const;

    /// Get Gmsh model coordinates as Gmsh SPoint3
    SPoint3 getSPoint3() const;

    /// Print point info
    void print() const;

    /// Point objects are considered equal if they
    /// -have same MElement number
    /// -mesh element parametric coordinates are
    ///  the same up to 'precision'
    bool isEqual(const Point* p, double precision = 1e-12) const;

  };
}

#endif
