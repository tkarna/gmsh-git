// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#ifndef _RM_MODELMAP_H_
#define _RM_MODELMAP_H_

#include "GModel.h"
#include "Numeric.h"
#include "MElement.h"
#include "Point.h"
#include "RiemannDefines.h"
#include "RiemannNumeric.h"

#if defined(HAVE_GINAC)
#include <ginac/ginac.h>
#endif

namespace rm
{

  class ModelMapN
  {
  public:
    virtual int getDim() const = 0;
    virtual GModel* getModel() const = 0;
    static GModel* createModel(const ModelMapN* mm,
                               bool discontinousChart=false);
    virtual ~ModelMapN() {}
  };

  /// Differentials of maps between Gmsh model and a n-manifold.
  template <int n>
  class ModelMap : public ModelMapN
  {
  protected:

    /// Gmsh model this maps from
    GModel* _m;

    /// Model map type tag:
    ///   MM_UVW:      maps to mesh element parametric coordinates
    ///   MM_XYZ:      identity map
    ///   MM_NUMERIC:  user defined map
    ///   MM_SYMBOLIC: user defined symbolic map
    ModelMapType _mt;

    /// Model map property:
    ///   MM_DIFF:      map is differentiable but not a diffeomorphism
    ///   MM_DIFFMORPH: map is a diffeomorphism
    ModelMapProp _mp;

  public:

    /// Construct from a Gmsh model
    ModelMap(GModel* m) : _m(m), _mt(MM_NUMERIC), _mp(MM_DIFF) {}
    ModelMap(GModel* m, ModelMapType mmtype, ModelMapProp mmprop)
      : _m(m), _mt(mmtype), _mp(mmprop) {}

    /// Get the Gmsh model this maps from
    GModel* getModel() const { return _m; }

    /// Get the dimension of image manifold
    int getDim() const { return n; }

    /// Get the model map type tag
    ModelMapType getModelMapType() const { return _mt; }
    ModelMapProp getModelMapProp() const { return _mp; }

    /// How a model point maps under this mapping to manifold chart
    virtual void mapPoint(const Point* p, double c[MAYBE(n)]) const = 0;

    /// Jacobian of the mapping from model to manifold
    virtual void getModelToManifoldJacobian(const Point* p,
                                            double jac[MAYBE(n*3)]) const = 0;

    /// Differential (pullback/pushforward) of the map from manifold to model
    /// return false if not available
    /// (I.e. transpose Jacobian or inverse Jacobian)
    virtual bool getDiffMapToModel(const Point* p,
				   double jac[MAYBE(3*n)],
				   TensorSpace tspace) const
    {
      if(p == NULL) return false;
      double jac0[MAYBE(n*3)];
      this->getModelToManifoldJacobian(p, jac0);
      if(tspace == COTANGENT) {
        transpose<n,3>(jac0, jac);
        return true;
      }
      else {
        if(_mp == MM_DIFF) return false;
        if(n == 3) inv<n>(jac0, jac);
        else {
          //Msg::Warning("Pushforward by pseudo-inverse.");
          pinv<n,3>(jac0, jac);
        }
        return true;
      }
      return false;
    }

    /// Differential (pullback/pushforward) of the map from model to manifold
    /// return false if not available
    /// (I.e. inverse transpose Jacobian or Jacobian)
    virtual bool getDiffMapToManifold(const Point* p,
				      double jac[MAYBE(n)*3],
				      TensorSpace tspace) const
    {
      if(p == NULL) return false;
      if(tspace == COTANGENT) {
        if(_mp == MM_DIFF) return false;
        double jac0[MAYBE(n*3)];
        double jac1[MAYBE(n*3)];
        this->getModelToManifoldJacobian(p, jac0);
        if(n == 3) inv<n>(jac0, jac1);
        else {
          //Msg::Warning("Pullback by pseudo-inverse.");
          pinv<n,3>(jac0, jac1);
        }
        transpose<3,n>(jac1, jac);
        return true;
      }
      else {
        this->getModelToManifoldJacobian(p, jac);
        return true;
      }
      return false;
    }

#if defined(HAVE_GINAC)
    virtual GiNaC::ex getCoordExpr(unsigned int i) const
    {
      Msg::Error("This model map is not symbolic");
      return GiNaC::ex();
    }

    virtual GiNaC::matrix getJacobianExpr() const
    {
      Msg::Error("This model map is not symbolic");
      return GiNaC::matrix();
    }
#endif


#if defined(HAVE_AVSOLVER)
    virtual void setParameter(double parameter) {
      // Msg::Warning("'virtual void ModelMap::setParameter(double parameter)' is doing nothing");
      return;
    }
#endif

    virtual ~ModelMap() {}

  };

  /// Identity map from model to 3-manifold
  class ModelMapXYZ : public ModelMap<3>
  {
  public:
    ModelMapXYZ(GModel* m) : ModelMap<3>(m, MM_XYZ, MM_DIFFMORPH) {}

    virtual void mapPoint(const Point* p, double c[3]) const
    {
      p->getXYZ(c[0],c[1],c[2]);
    }

    virtual void getModelToManifoldJacobian(const Point* p,
                                            double jac[3*3]) const
    {
      getDiffMapToManifold(p, jac, TANGENT);
    }

    virtual bool getDiffMapToModel(const Point* p,
				   double jac[3*3],
				   TensorSpace tspace) const
    {
      for(int i = 0; i < 3; i++) {
        for(int j = 0; j < 3; j++) {
          if(i != j) jac[i*3+j] = 0.;
          else jac[i*3+j] = 1.;
        }
      }
      return true;
    }
    virtual bool getDiffMapToManifold(const Point* p,
				      double jac[3*3],
                                      TensorSpace tspace) const
    {
      for(int i = 0; i < 3; i++) {
        for(int j = 0; j < 3; j++) {
          if(i != j) jac[i*3+j] = 0.;
          else jac[i*3+j] = 1.;
        }
      }
      return true;
    }

  };

  /// Identity map from model to 2-manifold
  class ModelMapXY : public ModelMap<2>
  {
  public:
    ModelMapXY(GModel* m) : ModelMap<2>(m, MM_XY, MM_DIFFMORPH) {}

    virtual void mapPoint(const Point* p, double c[2]) const
    {
      double temp;
      p->getXYZ(c[0],c[1],temp);
    }

    virtual void getModelToManifoldJacobian(const Point* p,
                                            double jac[2*3]) const
    {
      getDiffMapToManifold(p, jac, TANGENT);
    }

    virtual bool getDiffMapToModel(const Point* p,
				   double jac[3*2],
				   TensorSpace tspace) const
    {
      for(int i = 0; i < 3; i++) {
        for(int j = 0; j < 2; j++) {
          if(i != j) jac[i*2+j] = 0.;
          else jac[i*2+j] = 1.;
        }
      }
      return true;
    }

    virtual bool getDiffMapToManifold(const Point* p,
				      double jac[2*3],
                                      TensorSpace tspace) const
    {
      for(int i = 0; i < 2; i++) {
        for(int j = 0; j < 3; j++) {
          if(i != j) jac[i*3+j] = 0.;
          else jac[i*3+j] = 1.;
        }
      }
      return true;
    }

  };

  /// Identity map from model to 1-manifold
  class ModelMapX : public ModelMap<1>
  {
  public:
    ModelMapX(GModel* m) : ModelMap<1>(m, MM_X, MM_DIFFMORPH) {}

    virtual void mapPoint(const Point* p, double c[1]) const
    {
      double t1,t2;
      p->getXYZ(c[0],t1,t2);
    }

    virtual void getModelToManifoldJacobian(const Point* p,
                                            double jac[1*3]) const
    {
      getDiffMapToManifold(p, jac, TANGENT);
    }

    virtual bool getDiffMapToModel(const Point* p,
				   double jac[3*1],
				   TensorSpace tspace) const
    {
      for(int i = 0; i < 3; i++) {
        for(int j = 0; j < 1; j++) {
          if(i != j) jac[i*1+j] = 0.;
          else jac[i*1+j] = 1.;
        }
      }
      return true;
    }

    virtual bool getDiffMapToManifold(const Point* p,
				      double jac[1*3],
                                      TensorSpace tspace) const
    {
      for(int i = 0; i < 1; i++) {
        for(int j = 0; j < 3; j++) {
          if(i != j) jac[i*3+j] = 0.;
          else jac[i*3+j] = 1.;
        }
      }
      return true;
    }

  };


  /// Map from model coordinates to mesh element parametric coordinates
  template<int n>
  class ModelMapUVW : public ModelMap<n>
  {
  private:
    void _getElemJacobian(const Point* p, double jac[MAYBE(3*n)]) const
    {
      vecinit<MAYBE(3*n)>(jac);
      MElement* me = p->getMeshElement();
      double u, v, w;
      p->getUVW(u,v,w);
      double gsf[1256][3];
      me->getGradShapeFunctions(u, v, w, gsf);
      for (int i = 0; i < me->getNumShapeFunctions(); i++) {
        const MVertex *ver = me->getShapeFunctionNode(i);
        double* gg = gsf[i];
        for (int j = 0; j < n; j++) {
          jac[0*n+j] += ver->x() * gg[j];
          jac[1*n+j] += ver->y() * gg[j];
          jac[2*n+j] += ver->z() * gg[j];
        }
      }
    }

    void _getRegElemJacobian(const Point* p, double jac[3*3]) const
    {
      vecinit<3*3>(jac);

      if(n == 3) {
        this->_getElemJacobian(p, jac);
        return;
      }

      double jac3n[MAYBE(3*n)];
      this->_getElemJacobian(p, jac3n);
      for(int i = 0; i < 3; i++) {
        for(int j = 0; j < n; j++) {
          jac[i*3+j] = jac3n[i*n+j];
        }
      }

      if(n == 0) {
        jac[0*3+0] = jac[1*3+1] = jac[2*3+2] = 1.;
        jac[0*3+1] = jac[1*3+0] = jac[2*3+0] = 0.;
        jac[0*3+2] = jac[1*3+2] = jac[2*3+1] = 0.;
      }
      else if(n == 1) {

        double a[3];
        double b[3];
        double c[3];
        for(int i = 0; i < 3; i++) a[i] = jac3n[i*n+0];

        if((fabs(a[0]) >= fabs(a[1]) && fabs(a[0]) >= fabs(a[2])) ||
           (fabs(a[1]) >= fabs(a[0]) && fabs(a[1]) >= fabs(a[2]))) {
          b[0] = a[1]; b[1] = -a[0]; b[2] = 0.;
        }
        else {
          b[0] = 0.; b[1] = a[2]; b[2] = -a[1];
        }

        wedge<3,1,1>(a, b, c);

        for(int i = 0; i < 3; i++) jac[i*3+1] = b[i];
        for(int i = 0; i < 3; i++) jac[i*3+2] = c[i];

      }
      else if (n == 2) {
        double a[3];
        double b[3];
        double c[3];
        for(int i = 0; i < 3; i++) a[i] = jac3n[i*n+0];
        for(int i = 0; i < 3; i++) b[i] = jac3n[i*n+1];

        wedge<3,1,1>(a, b, c);

        for(int i = 0; i < 3; i++) jac[i*3+2] = c[i];
      }
    }

  public:
    ModelMapUVW(GModel* m) : ModelMap<n>(m, MM_UVW, MM_DIFFMORPH)
    {
      if(n > 3)
        Msg::Fatal("Cannot parametrize %d-manifold with mesh element parametric coordinates", n);
    }

    virtual void mapPoint(const Point* p, double c[n]) const
    {
      if(p == NULL) {
        vecinit<n>(c);
        return;
      }
      double u,v,w;
      p->getUVW(u,v,w);
      if(n > 0) c[0] = u;
      if(n > 1) c[1] = v;
      if(n > 2) c[2] = w;
      for(int i = 3; i < n; i++) c[i] = 0.;
    }

    virtual void getModelToManifoldJacobian(const Point* p,
                                            double jac[n*3]) const
    {
      /*double jac2[3*n];
      this->_getElemJacobian(p, jac2);
      pinv<3,n>(jac2, jac);*/

      this->getDiffMapToManifold(p, jac, TANGENT);
    }

    virtual bool getDiffMapToModel(const Point* p, double jac[3*n],
                                   TensorSpace tspace) const
    {
      if(p == NULL) return false;
      if(p->getMeshElement()->getDim() != n) {
        Msg::Error("No mesh element model map from %d-dimensional mesh element to %d-manifold, perhaps use MM_XYZ or MM_XY for embeddings", p->getMeshElement()->getDim(), n);
        return false;
      }

      if(tspace == COTANGENT) {
        double jac2[3*3];
        this->_getRegElemJacobian(p, jac2);

        double invjac2[3*3];
        inv<3>(jac2, invjac2);

        for(int i = 0; i < 3; i++) {
          for(int j = 0; j < n; j++) {
            jac[i*n+j] = invjac2[j*3+i]; // transpose
          }
        }
      }
      else {
        this->_getElemJacobian(p, jac);
      }
      return true;
    }

    virtual bool getDiffMapToManifold(const Point* p, double jac[n*3],
                                      TensorSpace tspace) const
    {
      if(p == NULL) return false;
      if(p->getMeshElement()->getDim() != n) {
        Msg::Error("No mesh element model map from %d-dimensional mesh element to %d-manifold, perhaps use MM_XYZ or MM_XY for embeddings", p->getMeshElement()->getDim(), n);
        return false;
      }

      if(tspace == COTANGENT) {
        double jact[MAYBE(3*n)];
        this->_getElemJacobian(p, jact);
        transpose<3,n>(jact, jac);
      }
      else {
        double jac2[3*3];
        this->_getRegElemJacobian(p, jac2);

        double invjac2[3*3];
        inv<3>(jac2, invjac2);

        for(int i = 0; i < n; i++) {
          for(int j = 0; j < 3; j++) {
            jac[i*3+j] = invjac2[i*3+j];
          }
        }

      }
      return true;
    }

  };

}

#endif
