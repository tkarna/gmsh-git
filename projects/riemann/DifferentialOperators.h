// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#ifndef _RM_DIFFERENTIAL_OPERATORS_H_
#define _RM_DIFFERENTIAL_OPERATORS_H_

#include "RiemannianManifold.h"
#include "WeakDerivativeSolver.h"
#include "FieldFunctions.h"
#include "ChristoffelSymbols.h"

namespace rm
{
  template <int n, int k>
  Covector<n,k+1> d(const Field<Covector<n,k> >& f, const Point* p)
  {
    const Manifold<n>* man = f.getManifold();
    Covector<n,k+1> df(man, p);
    if(k == n) return df;

    std::vector<Covector<n, 1> > dsf;
    man->getDiffPartOfUnity(p, dsf);
    for(unsigned int i = 0; i < dsf.size(); i++) {
      Point* pi = man->getPoint(p->getMeshElement(), i);
      df += dsf[i] % f(pi);
      delete pi;
    }
    return df;
  }

  template <class TensorType>
  Covector<TensorTraits<TensorType>::dim,1>
  d(const Field<TensorType>& f, const Point* p, int i)
  {
    const int n = TensorTraits<TensorType>::dim;
    const Manifold<n>* man = f.getManifold();
    Covector<n,1> df(man, p);
    if(n == 0) return df;

    std::vector<Covector<n, 1> > dsf;
    man->getDiffPartOfUnity(p, dsf);
    for(unsigned int j = 0; j < dsf.size(); j++) {
      Point* pi = man->getPoint(p->getMeshElement(), j);
      double c[1] = {f(pi)(i)};
      Covector<n,0> fi(man, p, c, MANIFOLD);
      df += dsf[j] % fi;
      delete pi;
    }
    return df;
  }


  template <int n, int k>
    MeshField<Covector<n,k+1> > d(const Field<Covector<n,k> >& f)
    {
      return ExtDer(f);
  }

  template <int n, int k>
  MeshField<Covector<n,k+1> > ExtDer(const Field<Covector<n,k> >& f)
  {
    const Manifold<n>* man = f.getManifold();
    MeshField<Covector<n,k+1> > df(man);
    for(MElemIt it = man->firstMeshElement();
        it != man->lastMeshElement(); it++) {
      MElement* me = it->first;
      for(int iV = 0; iV < me->getNumVertices(); iV++) {
        Point* p = man->getPoint(me, iV);
        Covector<n,k+1> dfp = d(f, p);
        df.assign(dfp);
        delete p;
      }
    }
    return df;
  }

  template <int n, int k>
  MeshField<Covector<n,k+1> > ExtDerWeak(const Field<Covector<n,k> >& f)
  {
    const Manifold<n>* man = f.getManifold();
    MeshField<Covector<n,k+1> > dwf(man);
    if(k > 0) {
      Msg::Error("Weak exterior derivative of %d-covector field not implemented.", k);
      return dwf;
    }

    Manifold<n>* mant = NULL;
    if(man->getModelMap()->getModelMapType() == MM_UVW) {
      GModel* gm = man->getModelMap()->getModel();
      mant = new RiemannianManifold<n>(gm, man->getTag(), MODELXYZ);
      mant->setBoundary(man->getBoundary());
    }

    const Manifold<n>* man2 = mant;
    if(man2 == NULL) man2 = man;
    MeshField<Covector<n,k> > f2 = pullback(f, man2);
    const int nc = TensorTraits<Covector<n, k> >::numCoeffs;
    const int nc2 = TensorTraits<Covector<n, k+1> >::numCoeffs;

    std::vector< std::vector<MeshField<Covector<n, 0> > > > comps(n);
    WeakDerivativeSolver<n> wds(man2);
    for(int iP = 0; iP < n; iP++) {
      for(int iC = 0; iC < nc; iC++) {
        comps[iP].push_back(wds.getWeakPartialDerivative(f2, iP, iC));
      }
    }

    MeshField<Covector<n,k+1> > dwf2(man2);
    for(MElemIt it = man2->firstMeshElement();
        it != man2->lastMeshElement(); it++) {
      MElement* me = it->first;
      for(int iV = 0; iV < me->getNumVertices(); iV++) {
        Point* p = man2->getPoint(me, iV);
        double c[nc2];
        if(k+1 == 1) {
          for(int iP = 0; iP < nc2; iP++) c[iP] = (comps.at(iP).at(0)(p))(0);
        }
        Covector<n,k+1> dwfp(man2, p, c, MANIFOLD);
        dwf2.assign(dwfp);
        delete p;
      }
    }

    dwf = pullback(dwf2, man);
    return dwf;
  }

  template <int n>
  MeshField<Vector<n,1> > L(const Field<Covector<n,1> >& f,
                            const Field<Vector<n,1> >& x,
                            Derivative der=BASISFUN)
  {

  }
  template <int n>
  MeshField<Covector<n,1> > L(const Field<Vector<n,1> >& x,
                              const Field<Vector<n,1> >& y,
                              Derivative der=BASISFUN)
  {

  }

  template <int n>
  MeshField<Vector<n,1> > D(const Field<Vector<n,1> >& x,
                            const Field<Vector<n,1> >& y,
                            const ChristoffelSymbols<n>& chris,
                            Derivative der=BASISFUN)
  {

  }

}

#endif
