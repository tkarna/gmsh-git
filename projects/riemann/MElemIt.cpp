// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#include "MElemIt.h"

namespace rm
{

  std::map<GModel*, std::map<int, std::set<MElement*, MElemLessThanNum> > > MElemIt::_elementCache[4];

  MElemIt::MElemIt(GModel* gm,
                   const std::vector<GEntity*>& ents,
                   const std::vector<int>& entsOr,
                   bool last) :
    _gm(gm), _ents(ents), _entsOr(entsOr), _singleElem(false)
  {
    for(unsigned int i = 0; i < _ents.size(); i++) {
      int etag = _ents.at(i)->tag();
      int dim = _ents.at(i)->dim();
      if(MElemIt::_elementCache[dim][_gm][etag].empty()) {
        Msg::Debug("Creating mesh element cache for elementary entity %d ...", etag, _gm);
        processMeshElements(_ents.at(i), _elementCache[dim][_gm][etag]);
      }
    }
    
    if(!last) {
      _currentEntIndex = 0;
      _currentElemIndex = 0;
      _currentElement =
        std::make_pair( _ents.at(0)->getMeshElement(_currentEntIndex),
                        _entsOr.at(0));
    }
    else {
      _currentEntIndex = _ents.size()-1;
      _currentElemIndex =
        _ents.at(_currentEntIndex)->getNumMeshElements();
      _currentElement =
        std::make_pair( _ents.at(_currentEntIndex)
                        ->getMeshElement(_currentElemIndex-1),
                        _entsOr.at(_currentEntIndex));
    }    
  }
  
  MElemIt::MElemIt(GModel* gm, MElement* me, bool last) :
    _gm(gm), _singleElem(true)
  {
    _currentElement = std::make_pair(me, 1);
    if(last) _currentElement.first++;
  }
  
  bool MElemIt::containsMeshElement(MElement* me) const
  {
    if(!getOrientation(me)) return false;
    return true;
  }
  
  int MElemIt::getOrientation(MElement* me) const
  {
    if(_singleElem &&
       me->getNum() == _currentElement.first->getNum()) return 1;
    else if(_singleElem) return 0;
    
    for(unsigned int i = 0; i < _ents.size(); i++) {
      std::set<MElement*, MElemLessThanNum>::const_iterator it = _elementCache[_ents[i]->dim()][_gm][_ents[i]->tag()].find(me);
      if(it != _elementCache[_ents[i]->dim()][_gm][_ents[i]->tag()].end()) {
        return _entsOr[i];
      }
    }
    return 0;
  }
  
  int MElemIt::getCurrentEntityTag() const
  {
    if(_singleElem) return 0;
    return _ents.at(_currentEntIndex)->tag();
  }
  
  int MElemIt::getNumMeshElements() const
  {
    if(_singleElem) return 1;
    int num = 0;
    for(unsigned int i = 0; i < _ents.size(); i++) {
      num += _ents[i]->getNumMeshElements();
    }
    return num;
  }
  
  const std::pair<MElement*, int>* MElemIt::operator*() const
  {
    return &_currentElement;
  }
  const std::pair<MElement*, int>* MElemIt::operator->() const
  {
    return &_currentElement;
  }
  

  bool MElemIt::operator==(const MElemIt& other) const
  {
    if(_singleElem && other._singleElem &&
       _currentElement.first == other._currentElement.first)
      return true;
    else if(_singleElem || other._singleElem) return false;
    
    if(_ents.at(_currentEntIndex)->tag() !=
       other._ents.at(other._currentEntIndex)->tag()) {
      return false;
    }
    return (_currentElemIndex == other._currentElemIndex);
  }

  bool MElemIt::operator!=(const MElemIt& other) const
  {
    return !this->operator==(other);
  }
  
  const std::pair<MElement*, int>* MElemIt::operator++(int)
  {
    if(_singleElem) {
      _currentElement.first++;
      return &_currentElement;
    }
    if(_currentElemIndex <
       _ents.at(_currentEntIndex)->getNumMeshElements()-1) {
      _currentElemIndex++;
    }
    else if(_currentEntIndex < _ents.size()-1){
      _currentEntIndex++;
      _currentElemIndex = 0;
    }
    else _currentElemIndex++;


    if(_currentEntIndex < _ents.size() &&
       _currentElemIndex < _ents.at(_currentEntIndex)->getNumMeshElements())
      _currentElement = std::make_pair(_ents.at(_currentEntIndex)->
                                       getMeshElement(_currentElemIndex),
                                       _entsOr.at(_currentEntIndex));
    else {
      _currentElement =
        std::make_pair( _ents.at(_currentEntIndex)
                        ->getMeshElement(_currentElemIndex-1),
                        _entsOr.at(_currentEntIndex));
    }

    return &_currentElement;
  }

  const std::pair<MElement*, int>* MElemIt::operator--(int)
  {
    if(_singleElem) {
      _currentElement.first--;
      return &_currentElement;
    }
    if(_currentElemIndex > 0) {
      _currentElemIndex--;
    }
    else if(_currentEntIndex > 0){
      _currentEntIndex--;
      _currentElemIndex = _ents.at(_currentEntIndex)->getNumMeshElements()-1;
    }
    else _currentElemIndex--;


    if(_currentEntIndex < _ents.size() &&
       _currentElemIndex < _ents.at(_currentEntIndex)->getNumMeshElements())
      _currentElement = std::make_pair(_ents.at(_currentEntIndex)->
                                       getMeshElement(_currentElemIndex),
                                       _entsOr.at(_currentEntIndex));
    else {
      _currentElement =
        std::make_pair( _ents.at(_currentEntIndex)
                        ->getMeshElement(_currentElemIndex),
                        _entsOr.at(_currentEntIndex));
    }


    return &_currentElement;
  }

  const std::pair<MElement*, int>* MElemIt::operator++()
  {
    std::pair<MElement*, int>* temp = &_currentElement;
    (*this)++;
    return temp;
  }
  const std::pair<MElement*, int>* MElemIt::operator--()
  {
    std::pair<MElement*, int>* temp = &_currentElement;
    (*this)--;
    return temp;
  }

}

