// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#ifndef _RM_RIEMANN_DEFINES_H_
#define _RM_RIEMANN_DEFINES_H_

#include "MElement.h"
#include "rmConfig.h"


#define NCK(n, k) ( ((k)==(n) || (k)==0) ? (1) : (((k)==(1) || (k)==(n-1)) ? (n) : ( (n)==4 ? (6) : (1) )) )
#define SYMR2(n) ((n) == 0 ? 1 : ((n)*(n)+(n))/2)
#define CHRIS(n) ((n) == 0 ? 1 : (n)*(n)*(n))
#define MCHRIS(n) ( (n) == 0 ? 1 : (n)*((n)*(n)+(n))/2 )
#define MAX(a, b) ((a) < (b) ? (b) : (a))
#define MIN(a, b) ((a) > (b) ? (b) : (a))
#define MAXCOMP 81
#define MAXDIM 4
#define MAYBE(n) ((n) == 0 ? 1 : (n))

namespace rm {

  enum Representation { MANIFOLD=0, MODEL=1, ELEMENT=2 };
  enum Coordinates { MANIFOLDCOORDS=0, MODELCOORDS=1 };

  enum TransType { PUSHFORWARD=0, PULLBACK=1, MIXED=2 };
  enum TensorSpace { TANGENT=0, COTANGENT=1, MIXEDSPACE=2 };
  enum TensorSymmetry { SKEWSYMMETRIC=0, SYMMETRIC=1, FULL=2 };
  enum ModelMapType { MM_UVW=0, MM_XYZ=1, MM_NUMERIC=2, MM_SYMBOLIC=3,
                      MM_XY=4, MM_XZ=5, MM_YZ=6, MM_X=7, MM_Y=8, MM_Z=9 };
  enum ModelMapProp { MM_DIFF=0, MM_DIFFMORPH=1 };
  enum Derivative { BASISFUN=0, WEAK=1};
  enum ManifoldChart { MODELXYZ=0, ELEMUVW=1, USERCHART=2,
                       MODELXY=3, MODELXZ=4, MODELYZ=5,
                       MODELX=6, MODELY=7, MODELZ=8 };
  enum ManifoldMetric { MODELMETRIC=0, EUCLIDEANMETRIC=1, USERMETRIC=2 };
  enum WeakDer { DER, INVDER };
  enum SupportType { SUPP_UNION, SUPP_INTERSECTION };

  enum GmshFormat { MSH, POS };
  enum ModelCoord { X, Y, Z };
  enum MatrixSubspace { ROWSPACE=0, COLSPACE=1 };
  enum Orientation { ELEMENTOR=0, GLOBALOR=1 };


  class MElemLessThanNum
  {
  public:
    inline bool operator()(const MElement* e1, const MElement* e2) const
    {
      return (e1->getNum() < e2->getNum());
    }
  };

}

#endif
