// Riemannian manifold library for Gmsh
//
// --
// Matti Pellikka, <matti.pellikka@tut.fi>
// Tampere University of Technology, Electromagnetics

#include <sstream>
#include "MultiIndex.h"

namespace rm {

  template <typename Iterator>
  bool next_combination(const Iterator first, Iterator k, const Iterator last)
  {
    /* Credits: Mark Nelson http://marknelson.us */
    if ((first == last) || (first == k) || (last == k))
      return false;
    Iterator i1 = first;
    Iterator i2 = last;
    ++i1;
    if (last == i1)
      return false;
    i1 = last;
    --i1;
    i1 = k;
    --i2;
    while (first != i1)
      {
        if (*--i1 < *i2)
          {
            Iterator j = k;
            while (!(*i1 < *j)) ++j;
            std::iter_swap(i1,j);
            ++i1;
            ++j;
            i2 = k;
            std::rotate(i1,j,last);
            while (last != j)
              {
                ++j;
                ++i2;
              }
            std::rotate(k,i2,last);
            return true;
          }
      }
    std::rotate(first,k,last);
    return false;
  }

  int kroneckerDelta(const MultiIndex& I, const MultiIndex& J)
  {
    if(I.getSize() != J.getSize()) return 0;
    if(I == J) return 1;
    std::vector<unsigned int> i;
    I.getIndices(i);
    std::vector<unsigned int> j;
    J.getIndices(j);
    int perm = 1;
    while(std::next_permutation(i.begin(), i.end())) {
      perm *= -1;
      if(J == MultiIndex(i)) return perm;
    }
    I.getIndices(i);
    perm = 1;
    while(std::prev_permutation(i.begin(), i.end())) {
      perm *= -1;
      if(J == MultiIndex(i)) return perm;
    }
    return 0;
  }

  int permutationSymbol(const MultiIndex& I)
  {
    std::vector<unsigned int> j;
    for(unsigned int i = 0; i < I.getSize(); i++) j.push_back(i);
    MultiIndex J(j);
    return kroneckerDelta(I, J);
  }

  MultiIndex::MultiIndex(const MultiIndex& i1, const MultiIndex& i2)
  {
    _sstindex = -1;
    i1.getIndices(_indices);
    for(unsigned int i = 0; i < i2.getSize(); i++)
      _indices.push_back(i2(i));
  }

  MultiIndex::MultiIndex(int ssti, int n, int k)
  {
    _sstindex = ssti;
    if(ssti >= nCk(n,k))
      Msg::Fatal("Invalid skew-symmetric tensor index %d (n=%d, k=%d)",
                 ssti, n, k);
    if(k == 0) {
      // empty indices
    }

    // dx, dy, ..
    else if(k == 1) _indices = std::vector<unsigned int>(1, ssti);

    // dx^dy
    else if(k == 2 && n == 2) {
      _indices = std::vector<unsigned int>(2);
      _indices[0] = 0;
      _indices[1] = 1;
    }

    // dy^dz, dz^dx, dx^dy
    else if(k == 2 && n == 3) {
      _indices = std::vector<unsigned int>(2);
      if(ssti == 0) {
        _indices[0] = 1;
        _indices[1] = 2;
      }
      else if(ssti == 1) {
        _indices[0] = 2;
        _indices[1] = 0;
      }
      else if(ssti == 2) {
        _indices[0] = 0;
        _indices[1] = 1;
      }
    }

    // dx^dy^dz
    else if(k == 3 && n == 3) {
      _indices = std::vector<unsigned int>(3);
      for(int i = 0; i < 3; i++) _indices[i] = i;
    }

    // ssti:th n choose k combination of dx_0^dx_1^,...,^dx_k
    // in lexicographical order
    else {
      std::vector<unsigned int> ind;
      for(int i = 0; i < n; i++) ind.push_back(i);
      for(int i = 0; i < ssti; i++)
        next_combination(ind.begin(), ind.begin()+k, ind.end());

      for(int i = 0; i < k; i++) _indices.push_back(ind.at(i));
    }
  }

  bool MultiIndex::nextSkewSymmetricTensorIndex(int n, int k,
                                                MultiIndex& I) const
  {
    if(_sstindex == -1) {
      Msg::Error("Not a skew-symmetic tensor multi-index");
      I = MultiIndex();
      return false;
    }
    if(k == 0) {
      I = MultiIndex();
      return false;
    }
    else if(k == 1) {
      if(_sstindex < n) {
        I = MultiIndex(_sstindex+1, n, k);
        return true;
      }
      else {
        I = MultiIndex(0, n, k);
        return false;
      }
    }
    else if(k == 2 && n == 2) {
      I = MultiIndex(0, n, k);
      return false;
    }
    else if(k == 2 && n == 3) {
      std::vector<unsigned int> ind(2,0);
      if(_sstindex == 0) {
        ind[0] = 2;
        ind[1] = 0;
        I = MultiIndex(ind, 1);
        return true;
      }
      else if(_sstindex == 1) {
        ind[0] = 0;
        ind[1] = 1;
        I = MultiIndex(ind, 2);
        return true;
      }
      else if(_sstindex == 2) {
        ind[0] = 1;
        ind[1] = 2;
        I = MultiIndex(ind, 1);
        return false;
      }
    }
    else if(k == 3 && n == 3) {
      I = MultiIndex(0, n, k);
      return false;
    }
    else {
      I = MultiIndex();
      return false;
    }
    I = MultiIndex();
    return false;
  }

  void MultiIndex::split(int k, MultiIndex& I1, MultiIndex& I2)
  {
    std::vector<unsigned int> i1;
    std::vector<unsigned int> i2;
    for(int i = 0; i < k; i++) i1.push_back(_indices[i]);
    for(unsigned int i = k; i < _indices.size(); i++) i2.push_back(_indices[i]);
    I1 = MultiIndex(i1);
    I2 = MultiIndex(i2);
  }

  void MultiIndex::print() const
  {
    std::string inds = "";
    std::string ind = "";
    std::stringstream ss(inds);
    for(unsigned int i = 0; i < _indices.size(); i++) {
      ss << _indices.at(i);
      ss << " ";
      ss >> ind;
      inds += ind;
    }
    Msg::Info("MultiIndex: (%s)", inds.c_str());
  }

  int MultiIndex::skewSymmetricTensorIndex(unsigned int n, int& perm) const
  {
    if(_sstindex != -1) return _sstindex;
    perm = 1;
    unsigned int k = this->getSize();
    if(k > n) return -1;
    if(this->maxIndex() >= n) return -1;
    if(!this->uniqueIndices()) return -1;

    // dx, dy, ..
    if(k == 1) return _indices[0];

    // dx^dy
    else if(k == 2 && n == 2) {
      if(_indices[0] > _indices[1]) perm = -1;
      return 0;
    }

    // dy^dz, dz^dx, dy^dz
    else if(k == 2 && n == 3) {
      std::vector<unsigned int> si;
      this->getSortedIndices(si, perm);
      if(si[0] == 0 && si[1] == 1) return 2;
      if(si[0] == 0 && si[1] == 2) return 1;
      if(si[0] == 1 && si[1] == 2) return 0;
    }

    // dx^dy^dz
    else if(k == 3 && n == 3) {
      std::vector<unsigned int> si;
      this->getSortedIndices(si, perm);
      return 0;
    }

    // k:th n choose k combination of dx_0^dx_1^,...,^dx_k
    // in lexicographical order
    else {
      int N = 0;
      for(unsigned int i = 0; i < _indices.size(); i++)
        N += nCk(_indices[i], k-i);
      return N;
    }

    return -1;
  }

  int MultiIndex::symmR2TensorIndex(unsigned int n) const
  {
    Msg::Debug("TBD");
    return 0;
  }

  int MultiIndex::fullTensorIndex(unsigned int n) const
  {
    Msg::Debug("TBD");
    return 0;
  }

}
