
#include "pbcSupplementConstraintHighOrder.h"


supplementConstraintSecondOrder::supplementConstraintSecondOrder(FunctionSpaceBase* space, FunctionSpaceBase* mspace,
                                int n, groupOfElements* g):
                                  supplementConstraint(space,mspace,n,g){
  _XXmean*= 0;
  for (int i=0; i<_v.size();i++){
    MVertex* v = _v[i];
    SPoint3 pnt = v->point();
    for( int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        _XXmean(j,k) += _weight(i)*0.5*pnt[j]*pnt[k];
      }
    }
  }
  _T.resize(_ndofs,27);
  fullMatrix<double> temp;
  getMatrixFromTensor3(_XXmean,temp);
  for (int i=0; i<_ndofs; i++)
    for (int j=0; j<temp.size2(); j++)
      _T(i,j) = temp(i,j);
};

void supplementConstraintSecondOrder::getSecondOrderKinematicalMatrix(fullMatrix<double>& m) const {
  m = _T;
};
void supplementConstraintSecondOrder::getSecondOrderKinematicalVector(const STensor33& G, fullVector<double>& m) const {
  m.resize(_ndofs);
  m.setAll(0.);
  for (int i=0; i<_ndofs; i++)
    for (int j=0; j<3; j++)
      for (int k=0; k<3; k++)
        m(i) += G(i,j,k)*_XXmean(j,k);
};
void supplementConstraintSecondOrder::getLinearConstraints(const STensor3& F, const STensor33& G,
                 std::map<Dof,DofAffineConstraint<double> >& con) const{
  supplementConstraint::getLinearConstraints(F,con);
  fullVector<double> g;
  this->getSecondOrderKinematicalVector(G,g);
  std::vector<Dof> kP;
  this->getDependentKeys(kP);
  for (int i=0; i<kP.size(); i++)
    con[kP[i]].shift += g(i);
};


lagrangeSupplementConstraintSecondOrder::lagrangeSupplementConstraintSecondOrder(FunctionSpaceBase* space, FunctionSpaceBase* mspace,
                                        int ndofs, std::vector<MVertex*>& v):
                                         lagrangeSupplementConstraint(space,mspace,ndofs,v){
  _XXmean*= 0;
  for (int i=0; i<_v.size();i++){
    MVertex* v = _v[i];
    SPoint3 pnt = v->point();
    for( int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        _XXmean(j,k) += _weight(i)*0.5*pnt[j]*pnt[k];
      }
    }
  }
  _T.resize(_ndofs,27);

  fullMatrix<double> temp;
  getMatrixFromTensor3(_XXmean,temp);
  for (int i=0; i<_ndofs; i++)
    for (int j=0; j<temp.size2(); j++)
      _T(i,j) = temp(i,j);
};

void lagrangeSupplementConstraintSecondOrder::getSecondOrderKinematicalMatrix(fullMatrix<double>& m) const {
  m = _T;
};
void lagrangeSupplementConstraintSecondOrder::getSecondOrderKinematicalVector(const STensor33& G, fullVector<double>& m) const {
  m.resize(_ndofs);
  m.setAll(0.);
  for (int i=0; i<_ndofs; i++)
    for (int j=0; j<3; j++)
      for (int k=0; k<3; k++)
        m(i) += G(i,j,k)*_XXmean(j,k);
};
void lagrangeSupplementConstraintSecondOrder::getLinearConstraints(const STensor3& F, const STensor33& G,
                 std::map<Dof,DofAffineConstraint<double> >& con) const{
  lagrangeSupplementConstraint::getLinearConstraints(F,con);
  fullVector<double> g;
  this->getSecondOrderKinematicalVector(G,g);
  std::vector<Dof> kP;
  this->getDependentKeys(kP);
  for (int i=0; i<kP.size(); i++)
    con[kP[i]].shift += g(i);
};

cubicSplineSupplementConstraintSecondOrder::cubicSplineSupplementConstraintSecondOrder(FunctionSpaceBase* space, FunctionSpaceBase* mspace,
                                            int ndofs, std::vector<MVertex*>& v, int flag) :
                                            cubicSplineSupplementConstraint(space,mspace,ndofs,v,flag){
  _XXmean*= 0;
  for (int i=0; i<_v.size();i++){
    MVertex* v = _v[i];
    SPoint3 pnt = v->point();
    for( int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        _XXmean(j,k) += _weight(2*i)*0.5*pnt[j]*pnt[k];
      }
    }
  }
  _T.resize(_ndofs,27);

  fullMatrix<double> temp;
  getMatrixFromTensor3(_XXmean,temp);
  for (int i=0; i<_ndofs; i++)
    for (int j=0; j<temp.size2(); j++)
      _T(i,j) = temp(i,j);
};

void cubicSplineSupplementConstraintSecondOrder::getSecondOrderKinematicalMatrix(fullMatrix<double>& m) const {
  m = _T;
};
void cubicSplineSupplementConstraintSecondOrder::getSecondOrderKinematicalVector(const STensor33& G, fullVector<double>& m) const {
  m.resize(_ndofs);
  m.setAll(0.);
  for (int i=0; i<_ndofs; i++)
    for (int j=0; j<3; j++)
      for (int k=0; k<3; k++)
        m(i) += G(i,j,k)*_XXmean(j,k);
};
void cubicSplineSupplementConstraintSecondOrder::getLinearConstraints(const STensor3& F, const STensor33& G,
                 std::map<Dof,DofAffineConstraint<double> >& con) const{
  cubicSplineSupplementConstraint::getLinearConstraints(F,con);
  fullVector<double> g;
  this->getSecondOrderKinematicalVector(G,g);
  std::vector<Dof> kP;
  this->getDependentKeys(kP);
  for (int i=0; i<kP.size(); i++)
    con[kP[i]].shift += g(i);
};

CoonsPatchLagrangeSupplementConstraintSecondOrder::CoonsPatchLagrangeSupplementConstraintSecondOrder(FunctionSpaceBase* space, FunctionSpaceBase* mspace,
                                            int ndofs, lagrangeSupplementConstraint* conX,
                                           lagrangeSupplementConstraint* conY):
                                           CoonsPatchLagrangeSupplementConstraint(space,mspace,ndofs,conX,conY){
  _XXmean*= 0;
  for (int i=0; i<_v.size();i++){
    MVertex* v = _v[i];
    SPoint3 pnt = v->point();
    for( int j=0; j<3; j++){
      for (int k=0; k<3; k++){
        _XXmean(j,k) += _weight(i)*0.5*pnt[j]*pnt[k];
      }
    }
  }
  getMatrixFromTensor3(_XXmean,_T);
};

void CoonsPatchLagrangeSupplementConstraintSecondOrder::getSecondOrderKinematicalMatrix(fullMatrix<double>& m) const {
  m = _T;
};
void CoonsPatchLagrangeSupplementConstraintSecondOrder::getSecondOrderKinematicalVector(const STensor33& G, fullVector<double>& m) const {
  m.resize(_ndofs);
  m.setAll(0.);
  for (int i=0; i<_ndofs; i++)
    for (int j=0; j<3; j++)
      for (int k=0; k<3; k++)
        m(i) += G(i,j,k)*_XXmean(j,k);
};
void CoonsPatchLagrangeSupplementConstraintSecondOrder::getLinearConstraints(const STensor3& F, const STensor33& G,
                 std::map<Dof,DofAffineConstraint<double> >& con) const{
  CoonsPatchLagrangeSupplementConstraint::getLinearConstraints(F,con);
  fullVector<double> g;
  this->getSecondOrderKinematicalVector(G,g);
  std::vector<Dof> kP;
  this->getDependentKeys(kP);
  for (int i=0; i<kP.size(); i++)
    con[kP[i]].shift += g(i);
};
