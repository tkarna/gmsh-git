#ifndef EXTRACTMACROPROPERTIESPETSC_H_
#define EXTRACTMACROPROPERTIESPETSC_H_

#include "extractMacroProperties.h"
#include "linearConstraintMatrixPETSc.h"
#include "splitStiffnessMatrixPETSc.h"
#include "GmshConfig.h"


#if defined(HAVE_PETSC)
#include "petscmat.h"
#include "petscvec.h"
#endif

class stiffnessCondensationPETSc : public stiffnessCondensation{
	#if defined(HAVE_PETSC)
	protected:
		splitStiffnessMatrixPETSc* _stiffness; // split stiffness matrix
		dofManager<double>* _pAssembler; // dof mananger
		linearConstraintMatrixPETSc* _bConMat, *_cConMat; // all linear constraint matrix
		pbcAlgorithm* _pAl; // pbc algorithm
		bool _tangentflag,_stressflag; // stress and tangent flag
		Mat _CbT,_SbT, _ScT, _RT, _minusCcT; // all mat


    MPI_Comm _comm; // Comunicator MPI
    MatFactorInfo info;
    IS perm,  iperm;


	public:
		stiffnessCondensationPETSc(dofManager<double>* p, pbcAlgorithm* ma,
                               bool sflag, bool tflag,MPI_Comm com = PETSC_COMM_SELF);
		virtual ~stiffnessCondensationPETSc();
		virtual void init();
		virtual void clear();
		virtual void stressSolve();
		virtual void tangentCondensationSolve();
  #else
  public:
    stiffnessCondensationPETSc(dofManager<double>* p, pbcAlgorithm* ma,bool sflag, bool tflag){
      Msg::Error("Petsc is required");
    };
    virtual ~stiffnessCondensationPETSc(){};
    virtual void init(){};
    virtual void clear() {};
    virtual void stressSolve(){};
    virtual void tangentCondensationSolve(){};

  #endif //HAVE_PETSC
};

class stiffnessCondensationSecondOrderPETSc : public stiffnessCondensationSecondOrder{
  #if defined(HAVE_PETSC)
  protected:
    splitStiffnessMatrixPETSc* _stiffness; // split stiffness matrix
		dofManager<double>* _pAssembler; // dof mananger
		linearConstraintMatrixSecondOrderPETSc* _bConMat; // all linear constraint matrix
		pbcAlgorithm* _pAl; // pbc algorithm
		bool _tangentflag,_stressflag; // stress and tangent flag
		Mat _Cbc, _CbcT, _Sbc, _SbcT, _Tbc, _TbcT; // all mat

    MPI_Comm _comm; // Comunicator MPI
    MatFactorInfo info;
    IS perm,  iperm;


	public:
		stiffnessCondensationSecondOrderPETSc(dofManager<double>* p, pbcAlgorithm* ma,
                                          bool sflag, bool tflag, MPI_Comm com = PETSC_COMM_SELF);
		virtual ~stiffnessCondensationSecondOrderPETSc();

		virtual void init();
		virtual void clear();
		virtual void stressSolve();
		virtual void tangentCondensationSolve();

  #else
  public:
    stiffnessCondensationSecondOrderPETSc(dofManager<double>* p, pbcAlgorithm* ma,bool sflag, bool tflag){
      Msg::Error("Petsc is required");
    };
    virtual ~stiffnessCondensationSecondOrderPETSc(){};
    virtual void init(){};
    virtual void clear(){};
		virtual void stressSolve(){};
		virtual void tangentCondensationSolve(){};
  #endif
};

#endif // EXTRACTMACROPROPERTIESPETSC_H_
