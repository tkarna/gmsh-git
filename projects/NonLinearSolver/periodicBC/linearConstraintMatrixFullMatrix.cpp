
#include "linearConstraintMatrixFullMatrix.h"

linearConstraintMatrixFullMatrix::linearConstraintMatrixFullMatrix(): _isAllocated(false),
                                  _firstMatrix(0),_secondMatrix(0),_firstOrderMatrix(0){};
linearConstraintMatrixFullMatrix::~linearConstraintMatrixFullMatrix(){
  clear();
};

bool linearConstraintMatrixFullMatrix::isAllocated() const {return _isAllocated;};

void linearConstraintMatrixFullMatrix::clear(){
  if (_isAllocated) {
    if (_firstCol>0) delete _firstMatrix;
    if (_secondCol>0) delete _secondMatrix;
    delete _firstOrderMatrix;
    _isAllocated = false;
  };
};

void linearConstraintMatrixFullMatrix::allocate(int row, int first, int second){
  _row = row;
  _firstCol = first;
  _secondCol = second;
  clear();
  _isAllocated = true;
  if (_firstCol>0)
    _firstMatrix = new fullMatrix<double>(_row,_firstCol);
  if (_secondCol>0)
    _secondMatrix = new fullMatrix<double>(_row,_secondCol);
  _firstOrderMatrix = new fullMatrix<double>(_row,9);
};

void linearConstraintMatrixFullMatrix::addToFirstMatrix(int i, int j, double val){
  if (_firstCol <= 0) return;
  if (val != 0.)
    _firstMatrix->operator()(i,j) += val;
}

void linearConstraintMatrixFullMatrix::addToSecondMatrix(int i, int j, double val){
  if (_secondCol<=0) return;
  if (val!= 0.)
    _secondMatrix->operator()(i,j) += val;
}

void linearConstraintMatrixFullMatrix::addToFirstOrderKinematicalMatrix(int i, int j, double val){
  if (val!=0.)
    _firstOrderMatrix->operator()(i,j) += val;
}
fullMatrix<double>* linearConstraintMatrixFullMatrix::getFirstMatrix(){return _firstMatrix;};
fullMatrix<double>* linearConstraintMatrixFullMatrix::getSecondMatrix(){return _secondMatrix;};
fullMatrix<double>* linearConstraintMatrixFullMatrix::getFirstOrderKinematicalMatrix(){return _firstOrderMatrix;};


linearConstraintMatrixSecondOrderFullMatrix::linearConstraintMatrixSecondOrderFullMatrix():
                  _secondOrderMatrix(0),linearConstraintMatrixFullMatrix(){};
linearConstraintMatrixSecondOrderFullMatrix::~linearConstraintMatrixSecondOrderFullMatrix(){
  this->clear();
};

void linearConstraintMatrixSecondOrderFullMatrix::clear(){
  if (_isAllocated) {
    linearConstraintMatrixFullMatrix::clear();
    delete _secondOrderMatrix;
  };
};

void linearConstraintMatrixSecondOrderFullMatrix::allocate(int row, int first, int second){
  linearConstraintMatrixFullMatrix::allocate(row,first,second);
  _secondOrderMatrix = new fullMatrix<double>(_row,27);
};

void linearConstraintMatrixSecondOrderFullMatrix::addToSecondOrderKinematicalMatrix(int i, int j, double val) {
  _secondOrderMatrix->operator()(i,j) += val;
};

fullMatrix<double>* linearConstraintMatrixSecondOrderFullMatrix::getSecondOrderKinematicalMatrix(){return _secondOrderMatrix;};
