#ifndef EIGENMODEFIELD_H_
#define EIGENMODEFIELD_H_

#include "unknownField.h"
#include "numericalFunctions.h"
#include "eigenSolver.h"

class eigenVectorField : public unknownField{
 protected:
  eigenSolver* _eigenSolver;
  int _num;
 public:
  eigenVectorField(dofManager<double> *pas, std::vector<partDomain*> &elas, std::vector<partDomain*> &gdom,
                   std::set<contactDomain*> *acontact,
                   const int nc, std::vector<unknownField::archiveNode> &archiving,
                   const std::vector<nlsField::dataBuildView> &dbview_,
                   eigenSolver* eig, int num, const std::string file = "mode");
  virtual ~eigenVectorField(){};
  virtual void get(Dof &D,double &udof) const;
  virtual void get(std::vector<Dof> &R, std::vector<double> &disp) const;
  virtual void get(std::vector<Dof> &R, fullVector<double> &disp) const;
  virtual void get(FunctionSpaceBase *sp1,FunctionSpaceBase *sp2, MInterfaceElement *iele,std::vector<double> &udofs){};
//  For archiving
  virtual void get(partDomain *dom, MElement *ele,std::vector<double> &udofs, const int cc,
                   const ElemValue ev=crude)const;
  virtual void archive(const double time,const int step=1);
  virtual void setInitial(const std::vector<Dof> &R,const std::vector<double> &disp);
};



#endif // EIGENMODEFIELD_H_
