#ifndef FUNCTIONPETSC_H_
#define FUNCTIONPETSC_H_
#include "GmshConfig.h"
#if defined(HAVE_PETSC)
#include "petscksp.h"
#include "petscmat.h"
#include "petscsys.h"
#include <iostream>


PetscErrorCode MatGetDeterminant(Mat A, PetscReal* realpart, PetscReal* imagpart, PetscInt* ex);

void MatToFile(Mat& a, const std::string filename);
void VecToFile(Vec& a,const std::string filename);

// interface PETSc for compute A22 - A21 inv(A11) A12
PetscErrorCode GetSchurComplementMat(Mat fA11, Mat A12, Mat A21, Mat A22, Mat* A);
PetscErrorCode GetSchurComplementMat_Naive(KSP& ksp, Mat A11, Mat A12, Mat A21, Mat A22, Mat* A);

// interface PETSc for compute B = inv(A)
PetscErrorCode MatInverse_Naive(Mat A, Mat* B, MPI_Comm comm = PETSC_COMM_SELF);

#endif // PETSC

#endif // FUNCTIONPETSC_H_
