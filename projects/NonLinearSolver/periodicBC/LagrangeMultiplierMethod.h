#ifndef LAGRANGEMULTIPLIERMETHOD_H_INCLUDED
#define LAGRANGEMULTIPLIERMETHOD_H_INCLUDED

#include "periodicConstraints.h"

class lagrangeMultiplier: public periodicConstraintsBase{

  protected:
    void addMultiplierKeys(MVertex* vp, MVertex* vn,std::vector<Dof> &keys,int flag); //flag for creating multiplier dofs
    void get(MVertex* vp, MVertex* vn, fullMatrix<double> &m, fullVector<double> &g);
    void unitAssemble(MVertex* vp, MVertex* vn,int flag);

  public:
    lagrangeMultiplier(FunctionSpaceBase* sp, dofManager<double> *_p)
                      : periodicConstraintsBase(sp,_p){};
    lagrangeMultiplier(const int tag, const int dim, dofManager<double> *_p)
                      : periodicConstraintsBase(tag,dim,_p){};
    virtual ~lagrangeMultiplier(){};

    virtual void applyPeriodicCondition();
    virtual void numberDof();
    virtual void assemble();
};

#endif // LAGRANGEMULTIPLIERMETHOD_H_INCLUDED
