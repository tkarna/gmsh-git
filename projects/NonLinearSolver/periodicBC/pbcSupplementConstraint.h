#ifndef PBCSUPPLEMENTCONSTRAINT_H_
#define PBCSUPPLEMENTCONSTRAINT_H_

#include "pbcConstraintElement.h"
#include "quadratureRules.h"

/**
class interface is used to impose the mean displacement on a group of elements
**/


class supplementConstraintBase : public constraintElement{
  public:
    virtual ~supplementConstraintBase(){};
		virtual void getConstraintKeys(std::vector<Dof>& key) const = 0; //  dofs on constraint elements
		virtual void getMultiplierKeys(std::vector<Dof>& key) const = 0;	// multiplier dof on constraint element
		virtual void getConstraintMatrix(fullMatrix<double>& m) const =0;		// matrix C

		virtual void getFirstOrderKinematicalMatrix(fullMatrix<double>& m) const = 0;	// matrix S
		virtual void getFirstOrderKinematicalVector(const STensor3& F, fullVector<double>& m) const = 0;	// matrix g

		virtual void getDependentKeys(std::vector<Dof>& keys) const =0; // left dofs
    virtual void getIndependentKeys(std::vector<Dof>& keys) const = 0; // right dofs
    virtual void getVirtualKeys(std::vector<Dof>& keys) const {}; // for spline interpolation
    virtual void getLinearConstraints(const STensor3& F, std::map<Dof,DofAffineConstraint<double> >& con) const=0; // linear constraint
    virtual constraintElement::ElementType getType() const =0; //  get element type
		virtual void print() const{
      Msg::Info("Print the supplement constraint ");
		};
		virtual double getConstraintFactor() const {return 0;};
		virtual std::vector<MVertex*>& getVertexList() = 0;
		virtual fullVector<double>& getWeight() = 0;
		virtual bool isActive(const MVertex* v) const =0;
};


class supplementConstraint : public supplementConstraintBase{
  protected:
    groupOfElements* _g;
    QuadratureBase* _integrationRule; // integration rule for approximate integrals
		fullMatrix<double>  _C, _S, _Cbc; // constraint matrix
		int _ndofs; // number of dof to constraints
		SVector3 _Xmean; // int_S{X} dA
		std::vector<MVertex*> _v; // all Vertex
		fullVector<double> _weight; //
		int _positive; // positive position
		MVertex* _tempVertex;
		double _factor;
		FunctionSpaceBase* _periodicSpace, *_multSpace;
		int _comp; // in case enforce a particular _comp = -1 by default


  public:
    supplementConstraint(FunctionSpaceBase* space, FunctionSpaceBase* mspace,
                          int ndofs, groupOfElements* g, int comp=-1);
    virtual ~supplementConstraint(){}
		virtual void getConstraintKeys(std::vector<Dof>& key) const ; // real dofs on constraint elements
		virtual void getMultiplierKeys(std::vector<Dof>& key) const ;	// multiplier dof on constraint element
		virtual void getConstraintMatrix(fullMatrix<double>& m) const ;		// matrix C
		virtual void getFirstOrderKinematicalMatrix(fullMatrix<double>& m) const ;	// matrix S
		virtual void getFirstOrderKinematicalVector(const STensor3& F, fullVector<double>& m) const;	// matrix g

		virtual void getDependentKeys(std::vector<Dof>& keys) const; // left real dofs
    virtual void getIndependentKeys(std::vector<Dof>& keys) const; // for spline interpolation
    virtual void getLinearConstraints(const STensor3& F, std::map<Dof,DofAffineConstraint<double> >& con) const;
    virtual constraintElement::ElementType getType() const{return constraintElement::Supplement;};
    virtual void print() const;
    virtual double getConstraintFactor() const {return _factor;};
    virtual std::vector<MVertex*>& getVertexList() {return _v;};
    virtual fullVector<double>& getWeight() {return _weight;};
    virtual bool isActive(const MVertex* v) const {
      for (int i=0; i<_v.size(); i++){
        if (_v[i]->getNum() == v->getNum()) return true;
      }
      return false;
    };
};

class lagrangeSupplementConstraint : public supplementConstraintBase{
  protected:
    std::vector<MVertex*>& _v; // all Vertex
		fullVector<double> _weight; //
		int _ndofs;
		fullMatrix<double>  _C, _S, _Cbc; // constraint matrix
		int _positive;
		MVertex* _tempVertex;
		SVector3 _Xmean;
		double _factor;
		FunctionSpaceBase* _periodicSpace, *_multSpace;

	public:
    void integrateFF(const std::vector<double>& base, fullVector<double>& integ);

  public:
    lagrangeSupplementConstraint(FunctionSpaceBase* space, FunctionSpaceBase* mspace, int ndofs, std::vector<MVertex*>& v);
    virtual ~lagrangeSupplementConstraint(){};
    virtual void getConstraintKeys(std::vector<Dof>& key) const ; // real dofs on constraint elements
		virtual void getMultiplierKeys(std::vector<Dof>& key) const ;	// multiplier dof on constraint element
		virtual void getConstraintMatrix(fullMatrix<double>& m) const ;		// matrix C
		virtual void getFirstOrderKinematicalMatrix(fullMatrix<double>& m) const ;	// matrix S
		virtual void getFirstOrderKinematicalVector(const STensor3& F, fullVector<double>& m) const;	// matrix g
		virtual void getDependentKeys(std::vector<Dof>& keys) const; // left real dofs
    virtual void getIndependentKeys(std::vector<Dof>& keys) const; // for spline interpolation
    virtual void getLinearConstraints(const STensor3& F, std::map<Dof,DofAffineConstraint<double> >& con) const;
    virtual constraintElement::ElementType getType() const{return constraintElement::Supplement;};
    virtual void print() const;
    virtual double getConstraintFactor() const {return _factor;};
    virtual std::vector<MVertex*>& getVertexList() {return _v;};
    virtual fullVector<double>& getWeight() {return _weight;};
    virtual bool isActive(const MVertex* v) const {
      for (int i=0; i<_v.size(); i++){
        if (_v[i]->getNum() == v->getNum()) return true;
      }
      return false;
    };

};

class cubicSplineSupplementConstraint : public supplementConstraintBase{
   protected:
    std::vector<MVertex*>& _v; // all Vertex
		fullVector<double> _weight; //
		int _ndofs;
		fullMatrix<double>  _C, _S, _Cbc; // constraint matrix
		int _positive;
		MVertex* _tempVertex;
		SVector3 _Xmean;
		int _flag;
		double _factor;
		FunctionSpaceBase* _periodicSpace, *_multSpace;

	protected:
    void integrateFF(double s1, double s2, fullVector<double>& integval) const{
      integval.resize(4);
      double L = s2 - s1;
      integval(0) = L/2.;
      integval(1) = L*L/12.;
      integval(2) = L/2.;
      integval(3) = L*L/12.;
    }

  public:
    cubicSplineSupplementConstraint(FunctionSpaceBase* space, FunctionSpaceBase* mspace,int ndofs, std::vector<MVertex*>& v, int flag = 0);
    virtual ~cubicSplineSupplementConstraint(){};
    virtual void getConstraintKeys(std::vector<Dof>& key) const; // real dofs on constraint elements
		virtual void getMultiplierKeys(std::vector<Dof>& key) const;	// multiplier dof on constraint element
		virtual void getConstraintMatrix(fullMatrix<double>& m) const;		// matrix C
		virtual void getFirstOrderKinematicalMatrix(fullMatrix<double>& m) const;	// matrix S
		virtual void getFirstOrderKinematicalVector(const STensor3& F, fullVector<double>& m) const;	// matrix g
		virtual void getDependentKeys(std::vector<Dof>& keys) const; // left real dofs
    virtual void getIndependentKeys(std::vector<Dof>& keys) const; // for spline interpolation
    virtual void getLinearConstraints(const STensor3& F, std::map<Dof,DofAffineConstraint<double> >& con) const;
    virtual constraintElement::ElementType getType() const{return constraintElement::Supplement;};
    virtual void print() const{};
    virtual double getConstraintFactor() const {return _factor;};
    virtual std::vector<MVertex*>& getVertexList() {return _v;};
    virtual fullVector<double>& getWeight() {return _weight;};
    virtual bool isActive(const MVertex* v) const {
      for (int i=0; i<_v.size(); i++){
        if (_v[i]->getNum() == v->getNum()) return true;
      }
      return false;
    };
};

class CoonsPatchLagrangeSupplementConstraint : public supplementConstraintBase{
  protected:
    std::vector<MVertex*> _v;
    MVertex* _vroot;
    int _ndofs;
		fullMatrix<double>  _C, _S, _Cbc; // constraint matrix
		MVertex* _tempVertex;
		fullVector<double> _weight;
		int _positive;
		double _factor;
		SVector3 _Xmean;
		FunctionSpaceBase* _periodicSpace, *_multSpace;

  public:
    CoonsPatchLagrangeSupplementConstraint(FunctionSpaceBase* space, FunctionSpaceBase* mspace,
                                           int ndofs, lagrangeSupplementConstraint* conX,
                                           lagrangeSupplementConstraint* conY);
    virtual ~CoonsPatchLagrangeSupplementConstraint(){};
    virtual void getConstraintKeys(std::vector<Dof>& key) const; // real dofs on constraint elements
		virtual void getMultiplierKeys(std::vector<Dof>& key) const;	// multiplier dof on constraint element
		virtual void getConstraintMatrix(fullMatrix<double>& m) const;		// matrix C
		virtual void getFirstOrderKinematicalMatrix(fullMatrix<double>& m) const;	// matrix S
		virtual void getFirstOrderKinematicalVector(const STensor3& F, fullVector<double>& m) const;	// matrix g
		virtual void getDependentKeys(std::vector<Dof>& keys) const; // left real dofs
    virtual void getIndependentKeys(std::vector<Dof>& keys) const; // for spline interpolation
      virtual void getLinearConstraints(const STensor3& F, std::map<Dof,DofAffineConstraint<double> >& con) const;
    virtual constraintElement::ElementType getType() const{return constraintElement::Supplement;};
    virtual void print() const{};
    virtual std::vector<MVertex*>& getVertexList() { return _v;};
    virtual fullVector<double>& getWeight() {return _weight;};
    virtual bool isActive(const MVertex* v) const {
      for (int i=0; i<_v.size(); i++){
        if (_v[i]->getNum() == v->getNum()) return true;
      }
      return false;
    };
};


#endif // PBCSUPPLEMENTCONSTRAINT_H_
