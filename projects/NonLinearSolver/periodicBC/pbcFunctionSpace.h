#ifndef PBCFUNCTIONSPACE_H_
#define PBCFUNCTIONSPACE_H_

#include "ThreeDLagrangeFunctionSpace.h"

class FSFunctionSpace : public ThreeDLagrangeFunctionSpace{
  public:
    typedef TensorialTraits<double>::ValType ValType;
    typedef TensorialTraits<double>::GradType GradType;
    typedef TensorialTraits<double>::HessType HessType;

  protected:
    virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys){
      Msg::Warning("getKeysOnElement undefined for FSFunctionSpace");
    }

  public:

    FSFunctionSpace(int tag, const bool hess = false, const bool third = false) : ThreeDLagrangeFunctionSpace(tag,3,hess,third){}
    FSFunctionSpace(int tag, int comp1, const bool hess = false, const bool third = false) : ThreeDLagrangeFunctionSpace(tag,1,comp1,hess,third){}
    FSFunctionSpace(int tag, int comp1, int comp2, const bool hess = false, const bool third = false) : ThreeDLagrangeFunctionSpace(tag,2,comp1,comp2,hess,third){}


    virtual ~FSFunctionSpace(){};

    virtual int getNumKeys(MElement *ele){
      return _ncomp*ele->getNumVertices();
    };
    virtual void getKeys(MElement *ele, std::vector<Dof> &keys){
      int nbFF = ele->getNumVertices();
      for(int i=0; i<_ncomp; i++){
        for(int j=0; j<nbFF; j++){
          keys.push_back(Dof(ele->getVertex(j)->getNum(),Dof::createTypeWithTwoInts(comp[i],_ifield))); // _ifield == _tag
        };
      };
    };

    virtual FunctionSpaceBase* clone(const std::vector<int>& comp) const{
      if (comp.size() == 1){
        return new FSFunctionSpace(this->_ifield,comp[0],this->_hessianComputation,this->_thirdDevComputation);
      }
      else if (comp.size() == 2){
        return new FSFunctionSpace(this->_ifield,comp[0],comp[1],this->_hessianComputation,this->_thirdDevComputation);
      }
      else{
        return new FSFunctionSpace(this->_ifield,this->_hessianComputation,this->_thirdDevComputation);
      }
    };
};


#endif // PBCFUNCTIONSPACE_H_
