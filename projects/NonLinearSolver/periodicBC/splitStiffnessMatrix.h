#ifndef SPLITTEDSTIFFNESSMATRIX_H_
#define SPLITTEDSTIFFNESSMATRIX_H_

/*
This class saves three arbitrary parts by splitting the glogal stiffnes matrix
I - B - C is three arbitrary group dofs which gives the parts:

K  == [KII KIB KIC
       KBI KBB KBC
       KCI KCB KCC]
*/

class splitStiffnessMatrix {
	public:
		virtual ~splitStiffnessMatrix(){};
		virtual void clear() = 0;
		virtual bool isAllocated() const =0;
		virtual void allocateSubMatrix(int ni, int nb, int nc = 0)  = 0;
		virtual void zeroSubMatrix() = 0;
		virtual void addToSubMatrix_II(int row, int col, const double& val) = 0;
		virtual void addToSubMatrix_IB(int row, int col, const double& val) = 0;
		virtual void addToSubMatrix_BI(int row, int col, const double& val) = 0;
		virtual void addToSubMatrix_BB(int row, int col, const double& val) = 0;
		virtual void addToSubMatrix_IC(int row, int col, const double& val) =0;
		virtual void addToSubMatrix_BC(int row, int col, const double& val) =0;
		virtual void addToSubMatrix_CI(int row, int col, const double& val) = 0;
		virtual void addToSubMatrix_CB(int row, int col, const double& val) =0;
		virtual void addToSubMatrix_CC(int row, int col, const double& val) =0;
};

#endif // SPLITTEDSTIFFNESSMATRIX_H_
