#include "SplineInterpolation.h"

//-----------Interpolation by spline function---------------

polynomialInterpolationByCubicSpline::polynomialInterpolationByCubicSpline(FunctionSpaceBase* sp, dofManager<double>* p, int deg):
                                polynomialInterpolationBase2D(sp,p,deg){}

polynomialInterpolationByCubicSpline::polynomialInterpolationByCubicSpline(const int tag, const int dim, dofManager<double>* _p
																	,int deg):polynomialInterpolationBase2D(tag,dim,_p,deg){
};

void polynomialInterpolationByCubicSpline::addKeysToVertex(MVertex* v, std::vector<Dof> &keys,int flag){
  keys.clear();
  std::vector<Dof> k;
  getKeysFromVertex(v,k);
  int ndofs=k.size();
  int type=k[0].getType();
  int entity=k[0].getEntity();
  int i1, i2;
  Dof::getTwoIntsFromType(type,i1,i2);
  for (int j=0; j<ndofs; j++){
    keys.push_back(Dof(entity, Dof::createTypeWithTwoInts(3+j+10*flag,i2)));
  };
  for (int i=0; i<ndofs; i++){
    p->numberDof(keys[i]);
  };
};

void polynomialInterpolationByCubicSpline::formCondition(){
  //selon x
  MVertex* v1y=ylist[0];
  MVertex* v2y=ylist[1];
  for (vertexContainer::iterator it= gVertex[0].begin(); it!=gVertex[0].end(); it++){
    MVertex* v=*it;
    MVertex* v1, *v2;
    bool flag=true;
    for (int i=0; i<xlist.size(); i++){
      if (v==xlist[i]) flag=false;
    };
    if (flag){
      double s=distance(v->point(),v1y->point(),v2y->point());
      double s1,s2;
      for (int i=0; i<xlist.size()-1; i++){
        double sleft=distance(xlist[i]->point(),v1y->point(),v2y->point());
        double sright=distance(xlist[i+1]->point(),v1y->point(),v2y->point());
        if ((sleft<s)&&(s<sright)){
          v1=xlist[i];
          v2=xlist[i+1];
          s1=sleft;
          s2=sright;
        };
      };
      std::vector<Dof> k, k1, k2, keys1, keys2;
      getKeysFromVertex(v,k);
      getKeysFromVertex(v1,k1);
      getKeysFromVertex(v2,k2);
      int ndofs=k.size();
      addKeysToVertex(v1,keys1,1);
      addKeysToVertex(v2,keys2,1);

      std::vector<double> FF;
      getFF(s,s1,s2,FF);
      DofAffineConstraint<double> cons;
      for (int i=0; i<ndofs; i++){
        cons.shift=0.0;
        cons.linear.push_back(std::pair<Dof,double>(k1[i],FF[0]));
        cons.linear.push_back(std::pair<Dof,double>(keys1[i],FF[1]));
        cons.linear.push_back(std::pair<Dof,double>(k2[i],FF[2]));
        cons.linear.push_back(std::pair<Dof,double>(keys2[i],FF[3]));
        p->setLinearConstraint(k[i],cons);
        cons.linear.clear();
      };
    };
  };
  //slon y
  MVertex* v1x=xlist[0];
  MVertex* v2x=xlist[1];
  for (vertexContainer::iterator it= gVertex[1].begin(); it!=gVertex[1].end(); it++){
    MVertex* v=*it;
    MVertex* v1, *v2;
    bool flag=true;
    for (int i=0; i<ylist.size(); i++){
      if (v==ylist[i]) flag=false;
    };
    if (flag){
      double s=distance(v->point(),v1x->point(),v2x->point());
      double s1,s2;
      for (int i=0; i<ylist.size()-1; i++){
        double sleft=distance(ylist[i]->point(),v1x->point(),v2x->point());
        double sright=distance(ylist[i+1]->point(),v1x->point(),v2x->point());
        if ((sleft<s)&&(s<sright)){
          v1=ylist[i];
          v2=ylist[i+1];
          s1=sleft;
          s2=sright;
        };
      };
      std::vector<Dof> k, k1, k2, keys1, keys2;
      getKeysFromVertex(v,k);
      getKeysFromVertex(v1,k1);
      getKeysFromVertex(v2,k2);
      int ndofs=k.size();
      addKeysToVertex(v1,keys1,2);
      addKeysToVertex(v2,keys2,2);

      std::vector<double> FF;
      getFF(s,s1,s2,FF);
      DofAffineConstraint<double> cons;
      for (int i=0; i<ndofs; i++){
        cons.shift=0.0;
        cons.linear.push_back(std::pair<Dof,double>(k1[i],FF[0]));
        cons.linear.push_back(std::pair<Dof,double>(keys1[i],FF[1]));
        cons.linear.push_back(std::pair<Dof,double>(k2[i],FF[2]));
        cons.linear.push_back(std::pair<Dof,double>(keys2[i],FF[3]));
        p->setLinearConstraint(k[i],cons);
        cons.linear.clear();
      };
    };
  };
};

void polynomialInterpolationByCubicSpline::periodicCondition(){
//selon x
  MVertex* v1y=ylist[0];
  MVertex* v2y=ylist[1];
  MVertex* v1x=xlist[0];
  MVertex* v2x=xlist[1];

  MVertex* vroot=*(gVertex[2].begin());
  SPoint3 proot=vroot->point();
  SPoint3 pp=project(vroot->point(),v1x->point(),v2x->point());
  fullVector<double> posi;posi.resize(3);
  posi(0)=(proot.x()-pp.x()); posi(1)=(proot.y()-pp.y()); posi(2)=(proot.z()-pp.z());
  fullVector<double> g;g.resize(3);
  imposedDef.mult(posi,g);

  for (vertexContainer::iterator it= gVertex[2].begin(); it!=gVertex[2].end(); it++){
    MVertex* v=*it;
    MVertex* v1, *v2;
    if (coinVertex.find(v)==coinVertex.end()){
      SPoint3 pt=project(v->point(),v1x->point(),v2x->point());
      double s=distance(pt,v1y->point(),v2y->point());
      double s1,s2;
      for (int i=0; i<xlist.size()-1; i++){
        double sleft=distance(xlist[i]->point(),v1y->point(),v2y->point());
        double sright=distance(xlist[i+1]->point(),v1y->point(),v2y->point());
        if ((sleft<=s)&&(s<=sright)){
          v1=xlist[i];
          v2=xlist[i+1];
          s1=sleft;
          s2=sright;
        };
      };
      std::vector<Dof> k, k1, k2, keys1, keys2;
      getKeysFromVertex(v,k);
      getKeysFromVertex(v1,k1);
      getKeysFromVertex(v2,k2);
      int ndofs=k.size();
      addKeysToVertex(v1,keys1,1);
      addKeysToVertex(v2,keys2,1);

      std::vector<double> FF;
      getFF(s,s1,s2,FF);
      DofAffineConstraint<double> cons;
      for (int i=0; i<ndofs; i++){
        cons.shift=g(i);
        cons.linear.push_back(std::pair<Dof,double>(k1[i],FF[0]));
        cons.linear.push_back(std::pair<Dof,double>(keys1[i],FF[1]));
        cons.linear.push_back(std::pair<Dof,double>(k2[i],FF[2]));
        cons.linear.push_back(std::pair<Dof,double>(keys2[i],FF[3]));
        p->setLinearConstraint(k[i],cons);
        cons.linear.clear();
      };
    };
  };
  //slon y
  vroot=*(gVertex[3].begin());
  proot=vroot->point();
  pp=project(vroot->point(),v1y->point(),v2y->point());
  posi(0)=(proot.x()-pp.x()); posi(1)=(proot.y()-pp.y()); posi(2)=(proot.z()-pp.z());
  imposedDef.mult(posi,g);

  for (vertexContainer::iterator it= gVertex[3].begin(); it!=gVertex[3].end(); it++){
    MVertex* v=*it;
    MVertex* v1, *v2;

    if (coinVertex.find(v)==coinVertex.end()){
      SPoint3 pt=project(v->point(),v1y->point(),v2y->point());
      double s=distance(pt,v1x->point(),v2x->point());
      double s1,s2;
      for (int i=0; i<ylist.size()-1; i++){
        double sleft=distance(ylist[i]->point(),v1x->point(),v2x->point());
        double sright=distance(ylist[i+1]->point(),v1x->point(),v2x->point());
        if ((sleft<s)&&(s<sright)){
          v1=ylist[i];
          v2=ylist[i+1];
          s1=sleft;
          s2=sright;
        };
      };
      std::vector<Dof> k, k1, k2, keys1, keys2;
      getKeysFromVertex(v,k);
      getKeysFromVertex(v1,k1);
      getKeysFromVertex(v2,k2);
      int ndofs=k.size();
      addKeysToVertex(v1,keys1,2);
      addKeysToVertex(v2,keys2,2);

      std::vector<double> FF;
      getFF(s,s1,s2,FF);
      DofAffineConstraint<double> cons;
      for (int i=0; i<ndofs; i++){
        cons.shift=g(i);
        cons.linear.push_back(std::pair<Dof,double>(k1[i],FF[0]));
        cons.linear.push_back(std::pair<Dof,double>(keys1[i],FF[1]));
        cons.linear.push_back(std::pair<Dof,double>(k2[i],FF[2]));
        cons.linear.push_back(std::pair<Dof,double>(keys2[i],FF[3]));
        p->setLinearConstraint(k[i],cons);
        cons.linear.clear();
      };
    };
  };
};

void polynomialInterpolationByCubicSpline::applyPeriodicCondition(){
  std::cout<<"------------Warning!--------------------------" <<std::endl;
  std::cout<<"------------Cubic spline interpolation------------" <<std::endl;
  setInterpolationPoint();
  formCondition();
  periodicCondition();
  cornerConstraint();
};

//---------------implementation for 3D case----------------------
polynomialInterpolationByCubicSpline3D::polynomialInterpolationByCubicSpline3D(FunctionSpaceBase* sp, dofManager<double>* p, int deg):
                polynomialInterpolationBase3D(sp,p,deg){};

polynomialInterpolationByCubicSpline3D::polynomialInterpolationByCubicSpline3D(const int tag, const int dim, dofManager<double>* _p,
        int deg):polynomialInterpolationBase3D(tag,dim,_p,deg){
};

void polynomialInterpolationByCubicSpline3D::addKeysToVertex(MVertex* v, std::vector<Dof> &keys,int flag){
  keys.clear();
  std::vector<Dof> k;
  getKeysFromVertex(v,k);
  int ndofs=k.size();
  int type=k[0].getType();
  int entity=k[0].getEntity();
  int i1, i2;
  Dof::getTwoIntsFromType(type,i1,i2);
  for (int j=0; j<ndofs; j++){
    keys.push_back(Dof(entity, Dof::createTypeWithTwoInts(3+j+10*flag,i2)));
  };
  for (int i=0; i<ndofs; i++){
    p->numberDof(keys[i]);
  };
};

void polynomialInterpolationByCubicSpline3D::lineFormCondition(vertexContainer container, std::vector<MVertex*> vlist,
                           MVertex* vroot, MVertex* vtip, int num){

  std::cout<<"Line form condition \n";
  for (vertexContainer::iterator it= container.begin(); it!=container.end(); it++){
    MVertex* v=*it;
    MVertex* v1, *v2;
    bool flag=true;
    for (int i=0; i<vlist.size(); i++){
      if (v==vlist[i]) flag=false;
    };
    if (flag){
      double s=distance(v->point(),vroot->point(),vtip->point());
      double s1,s2;
      for (int i=0; i<vlist.size()-1; i++){
        double sleft=distance(vlist[i]->point(),vroot->point(),vtip->point());
        double sright=distance(vlist[i+1]->point(),vroot->point(),vtip->point());
        if ((sleft<s)&&(s<sright)){
          v1=vlist[i];
          v2=vlist[i+1];
          s1=sleft;
          s2=sright;
        };
      };
      std::vector<Dof> k, k1, k2, keys1, keys2;
      getKeysFromVertex(v,k);
      getKeysFromVertex(v1,k1);
      getKeysFromVertex(v2,k2);
      int ndofs=k.size();
      addKeysToVertex(v1,keys1,num);
      addKeysToVertex(v2,keys2,num);

      std::vector<double> FF;
      getFF(s,s1,s2,FF);
      DofAffineConstraint<double> cons;
      for (int i=0; i<ndofs; i++){
        cons.shift=0.0;
        cons.linear.push_back(std::pair<Dof,double>(k1[i],FF[0]));
        cons.linear.push_back(std::pair<Dof,double>(keys1[i],FF[1]));
        cons.linear.push_back(std::pair<Dof,double>(k2[i],FF[2]));
        cons.linear.push_back(std::pair<Dof,double>(keys2[i],FF[3]));
        p->setLinearConstraint(k[i],cons);
        cons.linear.clear();
      };
    };
  };
};

void polynomialInterpolationByCubicSpline3D::linePeriodicCondition(vertexContainer container, std::vector<MVertex*> vlist,
                           MVertex* vroot, MVertex* vtip, MVertex* lroot, MVertex* ltip ,int num){

  std::cout<<"Line periodic condition \n";
  MVertex* vr=*(container.begin());
  SPoint3 proot=vr->point();
  SPoint3 pp=project(vr->point(),lroot->point(),ltip->point());
  fullVector<double> posi;posi.resize(3);
  posi(0)=(proot.x()-pp.x()); posi(1)=(proot.y()-pp.y()); posi(2)=(proot.z()-pp.z());
  fullVector<double> g;g.resize(3);
  imposedDef.mult(posi,g);

  for (vertexContainer::iterator it= container.begin(); it!=container.end(); it++){
    MVertex* v=*it;
    MVertex* v1, *v2;
    if (coinVertex.find(v)==coinVertex.end()){
      SPoint3 pt=project(v->point(),lroot->point(),ltip->point());
      double s=distance(pt,vroot->point(),vtip->point());
      double s1,s2;
      for (int i=0; i<vlist.size()-1; i++){
        double sleft=distance(vlist[i]->point(),vroot->point(),vtip->point());
        double sright=distance(vlist[i+1]->point(),vroot->point(),vtip->point());
        if ((sleft<=s)&&(s<=sright)){
          v1=vlist[i];
          v2=vlist[i+1];
          s1=sleft;
          s2=sright;
        };
      };
      std::vector<Dof> k, k1, k2, keys1, keys2;
      getKeysFromVertex(v,k);
      getKeysFromVertex(v1,k1);
      getKeysFromVertex(v2,k2);
      int ndofs=k.size();
      addKeysToVertex(v1,keys1,num);
      addKeysToVertex(v2,keys2,num);

      std::vector<double> FF;
      getFF(s,s1,s2,FF);
      DofAffineConstraint<double> cons;
      for (int i=0; i<ndofs; i++){
        cons.shift=g(i);
        cons.linear.push_back(std::pair<Dof,double>(k1[i],FF[0]));
        cons.linear.push_back(std::pair<Dof,double>(keys1[i],FF[1]));
        cons.linear.push_back(std::pair<Dof,double>(k2[i],FF[2]));
        cons.linear.push_back(std::pair<Dof,double>(keys2[i],FF[3]));
        p->setLinearConstraint(k[i],cons);
        cons.linear.clear();
      };
    };
  };
};

void polynomialInterpolationByCubicSpline3D::faceFormCondition(vertexContainer container, std::vector<MVertex*> xl, std::vector<MVertex*> yl,
                          MVertex* v1, MVertex* v2, MVertex* v3, MVertex* v4, int num1, int num2){
  std::cout<<"face form condition \n";
  std::vector<Dof> k0;
  getKeysFromVertex(v1,k0);
  if (v1->getNum()!=v3->getNum()){
    std::cout<<" a node have to add to corner!" <<std::endl;
  }
  else{
    for (vertexContainer::iterator it= container.begin(); it!=container.end(); it++){
      MVertex* v=*it;
      MVertex* v1xi, *v2xi, *v1neta, *v2neta;
      bool flag=true;
      for (int i=0; i<xl.size(); i++){
        if (v==xl[i]) flag=false;
      };
      for (int i=0; i<yl.size(); i++){
        if (v==yl[i]) flag=false;
      };
      if (flag){
        double xi=distance(v->point(), v3->point(),v4->point());
        double neta=distance(v->point(), v1->point(), v2->point());

        double s1xi,s2xi,s1neta,s2neta;

        for (int i=0; i<xl.size()-1; i++){
          double sleft=distance(xl[i]->point(),v3->point(),v4->point());
          double sright=distance(xl[i+1]->point(),v3->point(),v4->point());
          if ((sleft<=xi)&&(xi<=sright)){
            v1xi=xl[i];
            v2xi=xl[i+1];
            s1xi=sleft;
            s2xi=sright;
          };
        };
        for (int i=0; i<yl.size()-1; i++){
          double sleft=distance(yl[i]->point(),v1->point(),v2->point());
          double sright=distance(yl[i+1]->point(),v1->point(),v2->point());
          if ((sleft<=neta)&&(neta<=sright)){
            v1neta=yl[i];
            v2neta=yl[i+1];
            s1neta=sleft;
            s2neta=sright;
          };
        };

        std::vector<Dof> k, k1xi, k2xi, keys1xi, keys2xi;
        getKeysFromVertex(v,k);
        getKeysFromVertex(v1xi,k1xi);
        getKeysFromVertex(v2xi,k2xi);
        int ndofs=k.size();
        addKeysToVertex(v1xi,keys1xi,num1);
        addKeysToVertex(v2xi,keys2xi,num1);

        std::vector<Dof>  k1neta, k2neta, keys1neta, keys2neta;
        getKeysFromVertex(v1neta,k1neta);
        getKeysFromVertex(v2neta,k2neta);
        addKeysToVertex(v1neta,keys1neta,num2);
        addKeysToVertex(v2neta,keys2neta,num2);

        std::vector<double> FFxi, FFneta;
        getFF(xi,s1xi,s2xi,FFxi);
        getFF(neta,s1neta,s2neta,FFneta);

        DofAffineConstraint<double> cons;
        for (int i=0; i<ndofs; i++){
          cons.shift=0.0;
          cons.linear.push_back(std::pair<Dof,double>(k1xi[i],FFxi[0]));
          cons.linear.push_back(std::pair<Dof,double>(keys1xi[i],FFxi[1]));
          cons.linear.push_back(std::pair<Dof,double>(k2xi[i],FFxi[2]));
          cons.linear.push_back(std::pair<Dof,double>(keys2xi[i],FFxi[3]));

          cons.linear.push_back(std::pair<Dof,double>(k1neta[i],FFneta[0]));
          cons.linear.push_back(std::pair<Dof,double>(keys1neta[i],FFneta[1]));
          cons.linear.push_back(std::pair<Dof,double>(k2neta[i],FFneta[2]));
          cons.linear.push_back(std::pair<Dof,double>(keys2neta[i],FFneta[3]));

          cons.linear.push_back(std::pair<Dof,double>(k0[i],-1));

          p->setLinearConstraint(k[i],cons);
          cons.linear.clear();
        };
      };
    };
  };
};

void polynomialInterpolationByCubicSpline3D::facePeriodicCondition(vertexContainer container, std::vector<MVertex*> xl, std::vector<MVertex*> yl,
                          MVertex* v1, MVertex* v2, MVertex* v3, MVertex* v4, int num1, int num2){

  std::cout<<"face periodic condition \n";
  MVertex* vr=*(container.begin());
  SPoint3 proot=vr->point();
  SPoint3 pp=project(vr->point(),v1->point(),v2->point(),v4->point());
  fullVector<double> posi;posi.resize(3);
  posi(0)=(proot.x()-pp.x()); posi(1)=(proot.y()-pp.y()); posi(2)=(proot.z()-pp.z());
  fullVector<double> g;g.resize(3);
  imposedDef.mult(posi,g);

  std::vector<Dof> k0;
  getKeysFromVertex(v1,k0);
  if (v1->getNum()!=v3->getNum()){
    std::cout<<" a node have to add to corner!" <<std::endl;
  }
  else{
    for (vertexContainer::iterator it= container.begin(); it!=container.end(); it++){
      MVertex* v=*it;
      MVertex* v1xi, *v2xi, *v1neta, *v2neta;
      if (coinVertex.find(v)==coinVertex.end()){
        SPoint3 pt=project(v->point(),v1->point(),v2->point(),v4->point());
        double xi=distance(pt, v3->point(),v4->point());
        double neta=distance(pt, v1->point(), v2->point());

        double s1xi,s2xi,s1neta,s2neta;

        for (int i=0; i<xl.size()-1; i++){
          double sleft=distance(xl[i]->point(),v3->point(),v4->point());
          double sright=distance(xl[i+1]->point(),v3->point(),v4->point());
          if ((sleft<=xi)&&(xi<=sright)){
            v1xi=xl[i];
            v2xi=xl[i+1];
            s1xi=sleft;
            s2xi=sright;
          };
        };
        for (int i=0; i<yl.size()-1; i++){
          double sleft=distance(yl[i]->point(),v1->point(),v2->point());
          double sright=distance(yl[i+1]->point(),v1->point(),v2->point());
          if ((sleft<=neta)&&(neta<=sright)){
            v1neta=yl[i];
            v2neta=yl[i+1];
            s1neta=sleft;
            s2neta=sright;
          };
        };

        std::vector<Dof> k, k1xi, k2xi, keys1xi, keys2xi;
        getKeysFromVertex(v,k);
        getKeysFromVertex(v1xi,k1xi);
        getKeysFromVertex(v2xi,k2xi);
        int ndofs=k.size();
        addKeysToVertex(v1xi,keys1xi,num1);
        addKeysToVertex(v2xi,keys2xi,num1);

        std::vector<Dof>  k1neta, k2neta, keys1neta, keys2neta;
        getKeysFromVertex(v1neta,k1neta);
        getKeysFromVertex(v2neta,k2neta);
        addKeysToVertex(v1neta,keys1neta,num2);
        addKeysToVertex(v2neta,keys2neta,num2);

        std::vector<double> FFxi, FFneta;
        getFF(xi,s1xi,s2xi,FFxi);
        getFF(neta,s1neta,s2neta,FFneta);

        DofAffineConstraint<double> cons;
        for (int i=0; i<ndofs; i++){
          cons.shift=g(i);
          cons.linear.push_back(std::pair<Dof,double>(k1xi[i],FFxi[0]));
          cons.linear.push_back(std::pair<Dof,double>(keys1xi[i],FFxi[1]));
          cons.linear.push_back(std::pair<Dof,double>(k2xi[i],FFxi[2]));
          cons.linear.push_back(std::pair<Dof,double>(keys2xi[i],FFxi[3]));

          cons.linear.push_back(std::pair<Dof,double>(k1neta[i],FFneta[0]));
          cons.linear.push_back(std::pair<Dof,double>(keys1neta[i],FFneta[1]));
          cons.linear.push_back(std::pair<Dof,double>(k2neta[i],FFneta[2]));
          cons.linear.push_back(std::pair<Dof,double>(keys2neta[i],FFneta[3]));

          cons.linear.push_back(std::pair<Dof,double>(k0[i],-1));

          p->setLinearConstraint(k[i],cons);
          cons.linear.clear();
        };
      };
    };
  }
};

void polynomialInterpolationByCubicSpline3D::formCondition(){
  lineFormCondition(xline[0],xlist,v1y,v2y,1);
  lineFormCondition(yline[0],ylist,v1x,v2x,5);
  lineFormCondition(zline[0],zlist,v1y,v2y,10);

  faceFormCondition(gVertex[0],xlist,zlist,v1x,v2x,v1z,v2z,1,10);
  faceFormCondition(gVertex[1],ylist,zlist,v1y,v2y,v1z,v2z,5,10);
  faceFormCondition(gVertex[2],xlist,ylist,v1x,v2x,v1y,v2y,1,5);

};

void polynomialInterpolationByCubicSpline3D::periodicCondition(){
  linePeriodicCondition(xline[1],xlist,v1y,v2y,v1x,v2x,1);
  linePeriodicCondition(xline[2],xlist,v1y,v2y,v1x,v2x,1);
  linePeriodicCondition(xline[3],xlist,v1y,v2y,v1x,v2x,1);

  linePeriodicCondition(yline[1],ylist,v1x,v2x,v1y,v2y,5);
  linePeriodicCondition(yline[2],ylist,v1x,v2x,v1y,v2y,5);
  linePeriodicCondition(yline[3],ylist,v1x,v2x,v1y,v2y,5);

  linePeriodicCondition(zline[1],zlist,v1y,v2y,v1z,v2z,10);
  linePeriodicCondition(zline[2],zlist,v1y,v2y,v1z,v2z,10);
  linePeriodicCondition(zline[3],zlist,v1y,v2y,v1z,v2z,10);

  facePeriodicCondition(gVertex[3],xlist,zlist,v1x,v2x,v1z,v2z,1,10);
  facePeriodicCondition(gVertex[4],ylist,zlist,v1y,v2y,v1z,v2z,5,10);
  facePeriodicCondition(gVertex[5],xlist,ylist,v1x,v2x,v1y,v2y,1,5);
};

void polynomialInterpolationByCubicSpline3D::applyPeriodicCondition(){
  std::cout<<"------------Warning!--------------------------" <<std::endl;
  std::cout<<"------------3D Cubic spline interpolation------------" <<std::endl;
  initialize();
  formCondition();
  periodicCondition();
  cornerConstraint();
};

