#ifndef PBCSYSTEMS_H_
#define PBCSYSTEMS_H_

// interface to introduce linear constraints
template<class scalar>
class pbcSystem{
	public:
		virtual ~pbcSystem(){};
		virtual bool isAllocatedConstraintMatrix() const=0; // allocated flag
 		virtual void clearConstraintMatrix()  = 0; // clear function
    virtual void allocateConstraintMatrix(int nbcon, int nbR) = 0;  // constraint dof number, system dof number

    virtual bool isComputedProjectMatrices() const = 0;
 		virtual void clearConstraintProjectMatrices() = 0;
		virtual void computeConstraintProjectMatrices() =0; // compute all constraint matrix

		virtual void addToConstraintMatrix(int row, int col, const scalar& val) = 0; // add to constraint matrix
		virtual void addToConstraintResidual(int row, const scalar& val) = 0; // add to constraint residual
		virtual void setScaleFactor(double scale) = 0; // scale factor
		virtual void zeroConstraintMatrix() =0; // zero constraint matrix
		virtual void zeroConstraintResidual() =0; // zero constraint residual
};

#endif // PBCSYSTEMS_H_
