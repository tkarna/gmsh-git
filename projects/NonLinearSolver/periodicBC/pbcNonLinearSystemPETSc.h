#ifndef PBCNONLINEARSYSTEM_H_
#define PBCNONLINEARSYSTEM_H_

#include "pbcSystems.h"
#include "nonLinearSystems.h"
#include "functionPETSc.h"

/*
  class non linear system with linear constraints
*/

template<class scalar>
class pbcNonLinearSystemPETSc : public pbcSystem<scalar>,
                                public nonLinearSystemPETSc<scalar>{
	#if defined(HAVE_PETSC)
	protected:
		Mat _K; // structural stiffness
		Mat _C; // constraint matrix
		Vec _rc; // constraint residual

    // project matrices
    Mat _CT; // CT = transpose of C
    Mat _CTC; // CTC = C*CT
    Mat _R; // R = CT*inv(C*CT)
    Mat _Q; // Q = I-R*C = CT*inv(C*CT)*C --> Q is symmetric QT = Q

    int _constraintSize; // number of linear constraint == row of C
    bool _isAllocatedConstraintMatrix; // flag for allocating the matrix C, rc
    PetscScalar _scale;
    bool _matrixModified;
    bool _isComputedProjectMatrices;

		mutable bool _rhsflag; // flag for right hand side compuutation

    #ifdef PETSC_HAVE_MUMPS
		mutable bool _setRef; // reference for determinant
    mutable int val;
    mutable double base; // base*2^val;
    #endif // PETSC_HAVE_MUMPS


	public:
		pbcNonLinearSystemPETSc(MPI_Comm com = PETSC_COMM_SELF)
													: nonLinearSystemPETSc<scalar>(com),pbcSystem<scalar>(),
													_rhsflag(false),_constraintSize(0),_isAllocatedConstraintMatrix(false),
													_isComputedProjectMatrices(false),_matrixModified(true),_scale(1.){
      #ifdef PETSC_HAVE_MUMPS
      _setRef = false;
      #endif // PETSC_HAVE_MUMPS
    };
		virtual ~pbcNonLinearSystemPETSc(){
      clear();
      clearConstraintMatrix();
      clearConstraintProjectMatrices();
		};

		virtual bool isAllocatedConstraintMatrix() const {
      return _isAllocatedConstraintMatrix;
		};

 		virtual void clearConstraintMatrix()  {
      if (_isAllocatedConstraintMatrix){
        _isAllocatedConstraintMatrix = false;
        _try(MatDestroy(&_C));
        _try(VecDestroy(&_rc));
      }
 		};


		virtual void allocateConstraintMatrix(int nbcon, int systemSize){
      this->clearConstraintMatrix();
      _constraintSize = nbcon;
      _isAllocatedConstraintMatrix = true;
      _try(MatCreate(this->getComm(),&_C));
      _try(MatSetSizes(_C,_constraintSize,systemSize,PETSC_DETERMINE,PETSC_DETERMINE));
      _try(MatSetFromOptions(_C));

      #if (((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)))
      _try(MatSetUp(this->_C));
      #endif // (PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)

      _try(VecCreate(this->getComm(),&_rc));
      _try(VecSetSizes(_rc,_constraintSize,PETSC_DECIDE));
      _try(VecSetFromOptions(_rc));
		};  // number of constraints

		virtual bool isComputedProjectMatrices() const {
      return _isComputedProjectMatrices;
		};

 		virtual void clearConstraintProjectMatrices() {
 		  if (_isComputedProjectMatrices){
 		    _isComputedProjectMatrices = false;
        _try(MatDestroy(&_CT));
        _try(MatDestroy(&_CTC));
        _try(MatDestroy(&_R));
        _try(MatDestroy(&_Q));
 		  }
 		};

		virtual void computeConstraintProjectMatrices(){
		  clearConstraintProjectMatrices();
      _isComputedProjectMatrices = true;
      Msg::Info("Begin computing all constraint project matrices");

      _try(MatAssemblyBegin(_C,MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(_C,MAT_FINAL_ASSEMBLY));

      //get transpose of C
      _try(MatTranspose(_C,MAT_INITIAL_MATRIX,&_CT));
      _try(MatAssemblyBegin(_CT,MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(_CT,MAT_FINAL_ASSEMBLY));
      //
      Mat CCT, invCCT;
      _try(MatMatMult(_C,_CT,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&CCT));
      _try(MatInverse_Naive(CCT,&invCCT));
      _try(MatMatMult(_CT,invCCT,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&_R));
      _try(MatAssemblyBegin(_R,MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(_R,MAT_FINAL_ASSEMBLY));
      _try(MatDestroy(&invCCT));
      _try(MatDestroy(&CCT));

      /*Q = I-RC */
      _try(MatMatMult(_R,_C,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&_Q));
      _try(MatScale(_Q,-1));
      _try(MatShift(_Q,1.));
      _try(MatAssemblyBegin(_Q,MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(_Q,MAT_FINAL_ASSEMBLY));

      _try(MatMatMult(_CT,_C,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&_CTC));
      _try(MatAssemblyBegin(_CTC,MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(_CTC,MAT_FINAL_ASSEMBLY));

      _isComputedProjectMatrices = true;
      Msg::Info("End computing all constraint project matrices");
		};

		virtual void addToConstraintMatrix(int row, int col, const scalar& val){
      if (val != 0.){
        PetscInt ii = row, jj = col;
        PetscScalar s = val;
        _try(MatSetValues(_C,1,&ii,1,&jj,&s,ADD_VALUES));
        _matrixModified = true;
      }
    };

		virtual void addToConstraintResidual(int row, const scalar& val){
      if (val != 0.){
        PetscInt ii = row;
        PetscScalar s = val;
        _try(VecSetValues(_rc,1,&ii,&s,ADD_VALUES));
      }
    };

		virtual void setScaleFactor(double scale){
      _scale = scale;
    };

    virtual void zeroConstraintMatrix(){
      if (_isAllocatedConstraintMatrix){
        _try(MatAssemblyBegin(_C,MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(_C,MAT_FINAL_ASSEMBLY));
        _try(MatZeroEntries(_C));
        _matrixModified = true;
      }
    };

    virtual void zeroConstraintResidual() {
      if (_isAllocatedConstraintMatrix){
        _try(VecAssemblyBegin(this->_rc));
        _try(VecAssemblyEnd(this->_rc));
        _try(VecZeroEntries(this->_rc));
      };
		}

		virtual void clear(){
			if (nonLinearSystemPETSc<scalar>::isAllocated()){
				nonLinearSystemPETSc<scalar>::clear();
				_try(MatDestroy(&_K));
			};
		};
		virtual void preAllocateEntries(){
      Msg::Info("This is not defined for this class");
    };

		virtual void allocate(int nbRows){
			this->clear();
			nonLinearSystemPETSc<scalar>::allocate(nbRows);

			_try(MatCreate(this->getComm(), &_K));
      _try(MatSetSizes(_K, nbRows, nbRows, PETSC_DETERMINE, PETSC_DETERMINE));

      if (this->_parameters.count("petscOptions"))
        _try(PetscOptionsInsertString(this->_parameters["petscOptions"].c_str()));
      if (this->_parameters.count("petscPrefix"))
        _try(MatAppendOptionsPrefix(_K, this->_parameters["petscPrefix"].c_str()));
      _try(MatSetFromOptions(_K));


      #if (((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)))
      _try(MatSetUp(this->_K));
      #endif // (PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)
		};

		virtual void addToMatrix(int row, int col, const scalar& val){
      PetscInt i = row, j = col;
      PetscScalar s = val;
      _try(MatSetValues(_K, 1, &i, 1, &j, &s, ADD_VALUES));
      _matrixModified = true;
		};

		virtual void zeroMatrix(){
		  if (this->isAllocated()){
        _try(MatAssemblyBegin(_K,MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(_K,MAT_FINAL_ASSEMBLY));
        _try(MatZeroEntries(_K));
        _matrixModified = true;
		  };
		};

		virtual void zeroRightHandSide(){
			nonLinearSystemPETSc<scalar>::zeroRightHandSide();
			if (_isAllocatedConstraintMatrix){
        _try(VecAssemblyBegin(this->_rc));
        _try(VecAssemblyEnd(this->_rc));
        _try(VecZeroEntries(this->_rc));
      };
			_rhsflag = false;
		};



		virtual double getInstabilityCriterion() const {
      #ifdef PETSC_HAVE_MUMPS
      PC pc;
			_try(KSPGetPC(this->_ksp,&pc));
      Mat F;
      _try(PCFactorGetMatrix(pc,&F));

      PetscReal real, imag;
      PetscInt ex;
      _try(MatGetDeterminant(F,&real,&imag,&ex));

      if (_setRef == false){
        _setRef = true;
        val = ex;
        base = real;
      }
      double detRef = real/base*pow(2,ex-val);
      #ifdef _DEBUG
      Msg::Error("determinant: %e",detRef);
      #endif // _DEBUG
      return detRef;
      #else
      Msg::Error("Mumps must be used to solve");
      return 1.;
      #endif
		};

		virtual int systemSolve(){
      /**compute new stiffness matrix **/
      if (_matrixModified){
        _try(MatDestroy(&this->_a));
        _try(MatAssemblyBegin(_K,MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(_K,MAT_FINAL_ASSEMBLY));

        _try(MatPtAP(this->_K,_Q,MAT_INITIAL_MATRIX,2.,&this->_a));
        _try(MatAXPY(this->_a,_scale,_CTC,DIFFERENT_NONZERO_PATTERN));

        _try(MatAssemblyBegin(this->_a,MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(this->_a,MAT_FINAL_ASSEMBLY));

        if (!this->_kspAllocated)
          this->_kspCreate();

        _try(KSPSetOperators(this->_ksp, this->_a, this->_a, DIFFERENT_NONZERO_PATTERN));
        _matrixModified = false;

        //Msg::Error("recompute stiffness matrix");

      }

      /** compute new right hand side**/
			if (!_rhsflag){
        Vec Rrc; // Rrc = R*rc;
        Vec RHS; // RHS = fext - fint + KRrc
        Vec CTrc; //CTrc = CT*rc;
        _try(VecDuplicate(this->_x,&Rrc));
        _try(VecDuplicate(this->_x,&RHS));
        _try(VecDuplicate(this->_x,&CTrc));

        _try(MatMult(_R,_rc,Rrc));
        _try(MatMult(_K,Rrc,RHS));

        _try(VecAXPY(RHS,1.,this->_Fext));
        _try(VecAXPY(RHS,-1.,this->_Fint));

        _try(MatMult(_CT,_rc,CTrc));
        _try(MatMult(_Q,RHS,this->_b));

        PetscScalar minusscale = -1.*_scale;
        _try(VecAXPY(this->_b,minusscale,CTrc));

        _try(VecAssemblyBegin(this->_b));
        _try(VecAssemblyEnd(this->_b));
	_try(VecDestroy(&Rrc));
	_try(VecDestroy(&RHS));
	_try(VecDestroy(&CTrc));
        _rhsflag = true;
			}

      _try(KSPSolve(this->_ksp, this->_b, this->_x));
      _try(VecAXPY(this->_xsol,1.,this->_x));
			return 1;
		};

		virtual double normInfRightHandSide() const{
      /** compute new right hand side**/
			if (!_rhsflag){
			  _try(MatAssemblyBegin(_K,MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(_K,MAT_FINAL_ASSEMBLY));
        Vec Rrc; // Rrc = R*rc;
        Vec RHS; // RHS = fext - fint + KRrc
        Vec CTrc; //CTrc = CT*rc;
        _try(VecDuplicate(this->_x,&Rrc));
        _try(VecDuplicate(this->_x,&RHS));
        _try(VecDuplicate(this->_x,&CTrc));

        _try(MatMult(_R,_rc,Rrc));
        _try(MatMult(_K,Rrc,RHS));

        _try(VecAXPY(RHS,1.,this->_Fext));
        _try(VecAXPY(RHS,-1.,this->_Fint));

        _try(MatMult(_CT,_rc,CTrc));
        _try(MatMult(_Q,RHS,this->_b));

        PetscScalar minusscale = -1.*_scale;
        _try(VecAXPY(this->_b,minusscale,CTrc));

        _try(VecAssemblyBegin(this->_b));
        _try(VecAssemblyEnd(this->_b));
	_try(VecDestroy(&Rrc));
	_try(VecDestroy(&RHS));
	_try(VecDestroy(&CTrc));
        _rhsflag = true;
			}
			return linearSystemPETSc<scalar>::normInfRightHandSide();
		};
  #else
  public:
    pbcNonLinearSystemPETSc(){Msg::Error("Petsc is not available");};
    virtual ~pbcNonLinearSystemPETSc(){};
    virtual bool isAllocatedConstraintMatrix() const{ return false;}; // allocated flag
 		virtual void clearConstraintMatrix()  {}; // clear function
		virtual void allocateConstraintMatrix(int nbcon, int nbR) {};  // constraint dof number, system dof number
		virtual bool isComputedProjectMatrices() const {return false;};
 		virtual void clearConstraintProjectMatrices() {};
		virtual void computeConstraintProjectMatrices() {}; // compute all constraint matrix
		virtual void addToConstraintMatrix(int row, int col, const scalar& val) {}; // add to constraint matrix
		virtual void addToConstraintResidual(int row, const scalar& val) {}; // add to constraint residual
		virtual void setScaleFactor(double scale) {}; // scale factor
		virtual void zeroConstraintMatrix() {}; // zero constraint matrix
		virtual void zeroConstraintResidual() {}; // zero constraint residual
	#endif
};
#endif // PBCNONLINEARSYSTEM_H_
