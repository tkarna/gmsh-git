#ifndef SPLITSTIFFNESSMATRIXPETSC_H_
#define SPLITSTIFFNESSMATRIXPETSC_H_


#include "splitStiffnessMatrix.h"
#include "GmshConfig.h"

#if defined(HAVE_PETSC)
#include "petscmat.h"
#endif // HAVE_PETSC

class splitStiffnessMatrixPETSc : public splitStiffnessMatrix{
  #if defined(HAVE_PETSC)
	private:
		void _try(int ierr) { CHKERRABORT(_comm, ierr); };

	protected:
		Mat _aii, _aib, _aic,
        _abi, _abb, _abc,
        _aci, _acb, _acc; // all mat to split
		bool _isAllocated; // allocated flag
		PetscInt _ni, _nb, _nc; // number of dofs for I, B, C groups
		MPI_Comm _comm; // comunicator used in the context, PETSC_COMM_SELF for multiscale problem

	public:
		splitStiffnessMatrixPETSc(MPI_Comm com = PETSC_COMM_SELF);
    virtual ~splitStiffnessMatrixPETSc();
		virtual bool isAllocated() const;
		virtual void clear();
		virtual void allocateSubMatrix(int ni, int nb, int nc = 0);
		virtual void zeroSubMatrix();
		virtual void addToSubMatrix_II(int row, int col, const double &val);
		virtual void addToSubMatrix_IB(int row, int col, const double& val);
		virtual void addToSubMatrix_BI(int row, int col, const double& val);
    virtual void addToSubMatrix_BB(int row, int col, const double& val);
		virtual void addToSubMatrix_IC(int row, int col, const double& val);
		virtual void addToSubMatrix_BC(int row, int col, const double& val);
		virtual void addToSubMatrix_CI(int row, int col, const double& val);
		virtual void addToSubMatrix_CB(int row, int col, const double& val);
		virtual void addToSubMatrix_CC(int row, int col, const double& val);
		Mat& getIIMat();
		Mat& getIBMat();
		Mat& getBIMat();
		Mat& getBBMat();
		Mat& getICMat();
		Mat& getBCMat();
		Mat& getCIMat();
		Mat& getCBMat();
		Mat& getCCMat();
  #else
  public:
    splitStiffnessMatrixPETSc(){Msg::Error("Petsc must be used");};
    virtual bool isAllocated() const {return false;};
    virtual void clear() {};
    virtual void allocateSubMatrix(int ni, int nb, int nc = 0)  {};
    virtual void zeroSubMatrix() {};
    virtual void addToSubMatrix_II(int row, int col, const double& val) {};
    virtual void addToSubMatrix_IB(int row, int col, const double& val) {};
    virtual void addToSubMatrix_BI(int row, int col, const double& val) {};
    virtual void addToSubMatrix_BB(int row, int col, const double& val) {};
    virtual void addToSubMatrix_IC(int row, int col, const double& val) {};
    virtual void addToSubMatrix_BC(int row, int col, const double& val) {};
    virtual void addToSubMatrix_CI(int row, int col, const double& val) {};
    virtual void addToSubMatrix_CB(int row, int col, const double& val) {};
    virtual void addToSubMatrix_CC(int row, int col, const double& val) {};
	#endif // HAVE_PETSC
};

#endif // SPLITSTIFFNESSMATRIXPETSC_H_
