
#include "pbcAlgorithm.h"
#include "pbcDofManager.h"
#include "numericalFunctions.h"
#include "staticDofManager.h"
#include "GmshConfig.h"
#include "pbcFunctionSpace.h"
#include "nonLinearMechSolver.h"

void pbcAlgorithm::__int__(){
  // create function space within microscopic boundary condition
  int microtag = _mbc->getTag();
  int dim = _mbc->getDim();
  int lagtag = microtag;

  for (int i=0; i<_allDomain.size(); i++){
    partDomain* dom = _allDomain[i];
    if (dim == 0) dim = dom->getDim();
    int domtag = dom->getTag();
    lagtag+= domtag;
  }
  if (dim ==2){
    _space = new FSFunctionSpace(microtag,0,1);
    _mspace = new FSFunctionSpace(lagtag,0,1);
  }
  else if (dim ==3){
    _space = new FSFunctionSpace(microtag);
    _mspace = new FSFunctionSpace(lagtag);
  };

  // create PBC group constraint
  _pbcConstraint  = new pbcConstraintElementGroup(_mbc,_allDomain,_space,_mspace);
	// create constraint elements within pbcConstraintElementGroup
	_pbcConstraint->createConstraintGroup();
  #ifdef _DEBUG
  _pbcConstraint->print();
  #endif

  Msg::Info("size of virtual vertices = %d",_pbcConstraint->sizeVirtualVertices());

  //length scale for multiplier problem
  const groupOfElements* g = _pbcConstraint->getAllBulkElement();
  if (g->size()>0){
    MElement* ele = *(g->begin());
    double volume = ele->getVolume();
    double length = volume;
    if (volume <= 0) Msg::Fatal(" negative element volume");
    else {
      if (dim ==2){
        length = sqrt(volume);
      }
      else if (dim ==3){
        length = pow(volume,1.0/3.0);
      };
    };
    _scale = _allDomain[0]->getMaterialLaw()->scaleFactor()*length;
	}
	#ifdef _DEBUG
	printf("scale = %f\n",_scale);
	#endif
};

pbcAlgorithm::pbcAlgorithm(nonLinearMicroBC* bc,std::vector<partDomain*>& d, dofManager<double>*p)
									: _mbc(bc),_allDomain(d), _space(NULL),_mspace(NULL),_pAssembler(p),
                  _pbcConstraint(NULL),_splittedDofs(NULL),
									_isSplitted(false),_scale(1.){
  this->__int__();
};

pbcAlgorithm::~pbcAlgorithm(){
  if (_pbcConstraint) delete _pbcConstraint;
  if (_splittedDofs) delete _splittedDofs;
  if (_space) delete _space;
  if (_mspace) delete _mspace;
  _isSplitted = false;
};

void pbcAlgorithm::splitDofs(){
  if (_splittedDofs == NULL)
    _splittedDofs = new pbcDofManager();

  if (_isSplitted) _splittedDofs->clear();
  _isSplitted = true;

  printf("begin splitting dofs\n");
  if (dynamic_cast<nonLinearMinimalKinematicBC*>(_mbc)){
    for (std::set<constraintElement*>::iterator it= _pbcConstraint->constraintGroupBegin();
                                                it!= _pbcConstraint->constraintGroupEnd(); it++){
      constraintElement* cele = *it;
      std::vector<Dof> keys;
      cele->getConstraintKeys(keys);
      int ndofs = keys.size();
      for (int i=0; i<ndofs; i++){
        _splittedDofs->numberBoundaryDof(keys[i]);
        _splittedDofs->numberRealBoundaryDof(keys[i]);
      }
      std::vector<Dof> mkeys;
      cele->getMultiplierKeys(mkeys);
      for (int i=0; i<mkeys.size(); i++){
         _splittedDofs->numberMultiplierDof(mkeys[i]);
         _splittedDofs->numberDependentMultiplierDof(mkeys[i]);
      }
    }
    // for dependent dofs
    for (std::set<constraintElement*>::iterator it= _pbcConstraint->constraintGroupBegin();
                                                it!= _pbcConstraint->constraintGroupEnd(); it++){
      constraintElement* cel = *it;
      std::vector<Dof> keys;
      cel->getDependentKeys(keys);
      int ndofs = keys.size();
      for (int i=0; i<ndofs; i++){
        _splittedDofs->numberDependentDof(keys[i]);
        _splittedDofs->numberRealDependentDof(keys[i]);
      };
    };
    // for independent dofs
    for (std::set<constraintElement*>::iterator it= _pbcConstraint->constraintGroupBegin();
                                                it!= _pbcConstraint->constraintGroupEnd(); it++){
      constraintElement* cel = *it;
      std::vector<Dof> keys;
      cel->getIndependentKeys(keys);
      int ndofs = keys.size();
      for (int i=0; i<ndofs; i++){
        _splittedDofs->numberIndependentDof(keys[i]);
        _splittedDofs->numberRealIndependentDof(keys[i]);
      };
    };

    for (int i=0; i<_allDomain.size(); i++){
      partDomain* dom = _allDomain[i];
      FunctionSpaceBase* sp = dom->getFunctionSpace();
      for (groupOfElements::elementContainer::iterator it = dom->g->begin(); it!= dom->g->end(); it++){
        MElement* ele = *it;
        std::vector<Dof> keys;
        sp->getKeys(ele,keys);
        int ndofs = keys.size();
        for (int j=0; j<ndofs; j++){
          _splittedDofs->numberInternalDof(keys[j]);
        };
      };
    };
  }
  else if (dynamic_cast<nonLinearDisplacementBC*>(_mbc)){
    for (std::set<constraintElement*>::iterator it= _pbcConstraint->constraintGroupBegin();
                                                it!= _pbcConstraint->constraintGroupEnd(); it++){
      constraintElement* cele = *it;
      std::vector<Dof> keys;
      cele->getConstraintKeys(keys);
      int ndofs = keys.size();
      for (int i=0; i<ndofs; i++){
        _splittedDofs->numberBoundaryDof(keys[i]);
        _splittedDofs->numberRealBoundaryDof(keys[i]);
        _splittedDofs->numberDependentDof(keys[i]);
      }
      std::vector<Dof> mkeys;
      cele->getMultiplierKeys(mkeys);
      for (int i=0; i<mkeys.size(); i++){
         _splittedDofs->numberMultiplierDof(mkeys[i]);
         _splittedDofs->numberDependentMultiplierDof(mkeys[i]);
      }
    }

    for (int i=0; i<_allDomain.size(); i++){
      partDomain* dom = _allDomain[i];
      FunctionSpaceBase* sp = dom->getFunctionSpace();
      for (groupOfElements::elementContainer::iterator it = dom->g->begin(); it!= dom->g->end(); it++){
        MElement* ele = *it;
        std::vector<Dof> keys;
        sp->getKeys(ele,keys);
        int ndofs = keys.size();
        for (int j=0; j<ndofs; j++){
          _splittedDofs->numberInternalDof(keys[j]);
        };
      };
    };

  }
  else if (dynamic_cast<nonLinearPeriodicBC*>(_mbc) or dynamic_cast<nonLinearMixedBC*>(_mbc)){
    // for virtual dofs from new Vertex created within the constraint creation
    // if addVertexFlag option is activated

    if (_mbc->getOrder() == 1){
      /* in case of first-order MCH
         the boundary dofs are devided into two parts:
         dependent dof == boundary Dof, and
         independent dof == corner Dofs
      */

      // for independent dofs
      for (std::set<constraintElement*>::iterator it= _pbcConstraint->constraintGroupBegin();
                                                  it!= _pbcConstraint->constraintGroupEnd(); it++){
        constraintElement* cel = *it;
        if (cel->getType() == constraintElement::PeriodicNode){
          std::vector<Dof> keys;
          cel->getConstraintKeys(keys);
          int ndofs = keys.size();
          for (int i=0; i<ndofs; i++){
            _splittedDofs->numberIndependentDof(keys[i]);
            _splittedDofs->numberRealIndependentDof(keys[i]);
          };
          keys.clear();
          cel->getMultiplierKeys(keys);
          ndofs = keys.size();
          for (int i=0; i<ndofs; i++){
            _splittedDofs->numberIndependentMultiplierDof(keys[i]);
          }
        }
      };
    }


    for (std::set<MVertex*>::iterator it = _pbcConstraint->virtualVertexBegin();
                                      it!= _pbcConstraint->virtualVertexEnd(); it++){
      MVertex* v = *it;
      if (_pbcConstraint->isActive(v)){
         #ifdef _DEBUG
        Msg::Info("Virtual vertex = %d", v->getNum());
        #endif
        std::vector<Dof> keys;
        getKeysFromVertex(_space,v,keys);
        int ndofs = keys.size();
        for (int i=0; i< ndofs; i++){
          _splittedDofs->numberVirtualDof(keys[i]);
        };
      }
    };
    // virtual dofs from constraint which uses in the interpolation formulation -- slop in spline interpolation
    for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin();
                                                it!= _pbcConstraint->constraintGroupEnd(); it++){
      constraintElement* cele = *it;
      std::vector<Dof> keys;
      cele->getVirtualKeys(keys);
      int ndofs = keys.size();
      for (int i=0; i< ndofs; i++){
        _splittedDofs->numberVirtualDof(keys[i]);
      };
    };
    // numbering the boundary dofs and real boundary dofs
    for (std::set<constraintElement*>::iterator it= _pbcConstraint->constraintGroupBegin();
                                                it!= _pbcConstraint->constraintGroupEnd(); it++){
      constraintElement* cele = *it;
      std::vector<Dof> keys;
      cele->getConstraintKeys(keys);
      int ndofs = keys.size();
      for (int i=0; i<ndofs; i++){
        _splittedDofs->numberBoundaryDof(keys[i]);
        _splittedDofs->numberRealBoundaryDof(keys[i]);
      }
    }
    // for interior dofs
    for (int i=0; i<_allDomain.size(); i++){
      partDomain* dom = _allDomain[i];
      FunctionSpaceBase* sp = dom->getFunctionSpace();
      for (groupOfElements::elementContainer::iterator it = dom->g->begin(); it!= dom->g->end(); it++){
        MElement* ele = *it;
        std::vector<Dof> keys;
        sp->getKeys(ele,keys);
        int ndofs = keys.size();
        for (int j=0; j<ndofs; j++){
          _splittedDofs->numberInternalDof(keys[j]);
        };
      };
    };
    // for multiplier dofs
    for (std::set<constraintElement*>::iterator it= _pbcConstraint->constraintGroupBegin();
                                                it!= _pbcConstraint->constraintGroupEnd(); it++){
      constraintElement* cel = *it;
      std::vector<Dof> keys;
      cel->getMultiplierKeys(keys);
      int ndofs = keys.size();
      for (int i=0; i<ndofs; i++){
        _splittedDofs->numberMultiplierDof(keys[i]);
      };
    };

    if (_mbc->getOrder() == 1){
      /* in case of first-order MCH
         the boundary dofs are devided into two parts:
         dependent dof == boundary Dof, and
         independent dof == corner Dofs
      */
      // for independent dofs
      /*
      for (std::set<constraintElement*>::iterator it= _pbcConstraint->constraintGroupBegin();
                                                  it!= _pbcConstraint->constraintGroupEnd(); it++){
        constraintElement* cel = *it;
        if (cel->getType() == constraintElement::PeriodicNode){
          std::vector<Dof> keys;
          cel->getConstraintKeys(keys);
          int ndofs = keys.size();
          for (int i=0; i<ndofs; i++){
            _splittedDofs->numberIndependentDof(keys[i]);
            _splittedDofs->numberRealIndependentDof(keys[i]);
          };
          keys.clear();
          cel->getMultiplierKeys(keys);
          ndofs = keys.size();
          for (int i=0; i<ndofs; i++){
            _splittedDofs->numberIndependentMultiplierDof(keys[i]);
          }
        }
      };
      */
      // for dependent dofs
      for (std::set<constraintElement*>::iterator it= _pbcConstraint->constraintGroupBegin();
                                                  it!= _pbcConstraint->constraintGroupEnd(); it++){
        constraintElement* cel = *it;
        if (cel->getType() != constraintElement::PeriodicNode){
          std::vector<Dof> keys;
          cel->getConstraintKeys(keys);
          int ndofs = keys.size();
          for (int i=0; i<ndofs; i++){
            _splittedDofs->numberDependentDof(keys[i]);
            _splittedDofs->numberRealDependentDof(keys[i]);
          };
          keys.clear();
          cel->getMultiplierKeys(keys);
          ndofs = keys.size();
          for (int i=0; i<ndofs; i++){
            _splittedDofs->numberDependentMultiplierDof(keys[i]);
          }
        }
      };
    }
    else if (_mbc->getOrder() == 2){
      /*
        in case of second-order MCH, the boundary dofs are devided into two parts:
        dependent dofs and independent dofs
      */
      // for dependent dofs
      for (std::set<constraintElement*>::iterator it= _pbcConstraint->constraintGroupBegin();
                                                  it!= _pbcConstraint->constraintGroupEnd(); it++){
        constraintElement* cel = *it;
        std::vector<Dof> keys;
        cel->getDependentKeys(keys);
        int ndofs = keys.size();
        for (int i=0; i<ndofs; i++){
          _splittedDofs->numberDependentDof(keys[i]);
          _splittedDofs->numberRealDependentDof(keys[i]);
        };
        keys.clear();
        cel->getMultiplierKeys(keys);
        ndofs = keys.size();
        for (int i=0; i<ndofs; i++){
          _splittedDofs->numberDependentMultiplierDof(keys[i]);
        }
      };
      // for independent dofs
      for (std::set<constraintElement*>::iterator it= _pbcConstraint->constraintGroupBegin();
                                                  it!= _pbcConstraint->constraintGroupEnd(); it++){
        constraintElement* cel = *it;
        std::vector<Dof> keys;
        cel->getIndependentKeys(keys);
        int ndofs = keys.size();
        for (int i=0; i<ndofs; i++){
          _splittedDofs->numberIndependentDof(keys[i]);
          _splittedDofs->numberRealIndependentDof(keys[i]);
        };
      };
    }
    else {
      Msg::Fatal(" Multiscale order %d is not implemented",_mbc->getOrder());
    }
  }
  else{
    Msg::Fatal("this bc is not splitted");
  }
  printf("end splitting dofs\n");
};

void pbcAlgorithm::numberDof(const int type){
  if (this->isSplittedDof() == false) this->splitDofs();
  if (type == nonLinearMechSolver::MULT_ELIM){
    Msg::Info("Number all displacement dofs");
    _splittedDofs->numberDof(pbcDofManager::INTERNAL,_pAssembler);
    _splittedDofs->numberDof(pbcDofManager::DEPENDENT,_pAssembler);
    _splittedDofs->numberDof(pbcDofManager::INDEPENDENT,_pAssembler);
    _splittedDofs->numberDof(pbcDofManager::VIRTUAL,_pAssembler);
  }
  else if (type == nonLinearMechSolver::DISP_ELIM){
    Msg::Info("Number all displacement dofs accounting for constraints");
    this->applyPBCByConstraintElimination();
    _splittedDofs->numberDof(pbcDofManager::INTERNAL,_pAssembler);
    _splittedDofs->numberDof(pbcDofManager::DEPENDENT,_pAssembler);
    _splittedDofs->numberDof(pbcDofManager::INDEPENDENT,_pAssembler);
    _splittedDofs->numberDof(pbcDofManager::VIRTUAL,_pAssembler);
  }
  else if (type == nonLinearMechSolver::DISP_MULT){
    Msg::Info("Number all displacement dofs and multiplier dofs");
    _splittedDofs->numberDof(pbcDofManager::INTERNAL,_pAssembler);
    _splittedDofs->numberDof(pbcDofManager::DEPENDENT,_pAssembler);
    _splittedDofs->numberDof(pbcDofManager::INDEPENDENT,_pAssembler);
    _splittedDofs->numberDof(pbcDofManager::VIRTUAL,_pAssembler);
    _splittedDofs->numberDof(pbcDofManager::MULTIPLIER,_pAssembler);
  }
  else{
    Msg::Error("system type used in mechanical solver is not valid");
  };
};

void pbcAlgorithm::assembleConstraintMatrix(){
	//printf("Assembling constraint matrix to linear systems \n");
	for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin(); it!= _pbcConstraint->constraintGroupEnd(); it++){
		constraintElement* cele = *it;
		fullMatrix<double> C;
		cele->getConstraintMatrix(C);
		int row = C.size1();
		int col = C.size2();
		int size = row+col;
		fullMatrix<double> m; m.resize(size,size); m.setAll(0.0);
		for (int i =0; i<row; i++){
			for (int j=0; j<col; j++){
				m(i+col,j) = C(i,j)*_scale;
				m(j,i+col) = C(i,j)*_scale;
			};
		};
		std::vector<Dof> keys;
		cele->getConstraintKeys(keys);
		cele->getMultiplierKeys(keys);
		_pAssembler->assemble(keys,m);
	};
};
void pbcAlgorithm::assembleRightHandSide(){
  STensor3 F = _mbc->getFirstOrderKinematicalVariable();
  STensor33 G = _mbc->getSecondOrderKinematicalVariable();
	for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin(); it!= _pbcConstraint->constraintGroupEnd(); it++){
		constraintElement* cele = *it;
		std::vector<Dof> keys;
		cele->getMultiplierKeys(keys);
		fullVector<double> vec;
		cele->getFirstOrderKinematicalVector(F,vec);
		constraintElementSecondOrder* highele =dynamic_cast<constraintElementSecondOrder*>(cele);
		if (highele){
      fullVector<double> highvec;
      highele->getSecondOrderKinematicalVector(G,highvec);
      vec.axpy(highvec,1.);
		}
		vec.scale(_scale);
		if (dynamic_cast<staticDofManager<double>* >(_pAssembler)){
			static_cast<staticDofManager<double>* >(_pAssembler)->assemble(keys,vec,nonLinearSystem<double>::Fext);
		}
		else{
			_pAssembler->assemble(keys,vec);
		};
	};
};
void pbcAlgorithm::computeLinearConstraints(){
	//Msg::Info("Recomputing linear constraint");
	STensor3 F = _mbc->getFirstOrderKinematicalVariable();
	STensor33 G(_mbc->getSecondOrderKinematicalVariable());
	for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin(); it!= _pbcConstraint->constraintGroupEnd(); it++){
		constraintElement* cele = *it;
		fullMatrix<double> C;
		cele->getConstraintMatrix(C);
		int row = C.size1();
		int col = C.size2();
		int size = row+col;
		fullMatrix<double> m; m.resize(size,size); m.setAll(0.0);
		for (int i =0; i<row; i++){
			for (int j=0; j<col; j++){
				m(i+col,j) = C(i,j)*_scale;
				m(j,i+col) = C(i,j)*_scale;
			};
		};
		std::vector<Dof> keys;
		cele->getConstraintKeys(keys);
		cele->getMultiplierKeys(keys);
		fullVector<double> solution(size);
		for (int i=0; i<size; i++){
			_pAssembler->getDofValue(keys[i],solution(i));
		};
		fullVector<double> val(size);
		m.mult(solution,val);

		fullVector<double> rhs;
		cele->getFirstOrderKinematicalVector(F,rhs);
		constraintElementSecondOrder* highele = dynamic_cast<constraintElementSecondOrder*>(cele);
		if (highele){
      fullVector<double> highvec;
      highele->getSecondOrderKinematicalVector(G,highvec);
      rhs.axpy(highvec,1.);
		}
		rhs.scale(_scale);
		for (int i=0; i<row; i++) {
			val(i+col) = val(i+col)-rhs(i);
		};

		if (dynamic_cast<staticDofManager<double>* >(_pAssembler)){
			static_cast<staticDofManager<double>* >(_pAssembler)->assemble(keys,val,nonLinearSystem<double>::Fint);
		}
		else{
			val.scale(-1.0);
			_pAssembler->assemble(keys,val);
		};
	};
};


void pbcAlgorithm::assembleConstraintResidualToSystem(){
	#if _DEBUG
	printf("assemble constraint residuals to system \n");
	#endif //_DEBUG
	std::string name = "A";
	linearSystem<double>* lsys = _pAssembler->getLinearSystem(name);
	pbcSystem<double>* pbcsys = dynamic_cast<pbcSystem<double>*>(lsys);
	if (!pbcsys){
		printf("periodic non linear system is not used! \n");
	}
	else{
	  STensor3 FI = _mbc->getFirstOrderKinematicalVariable();
	  STensor33 G = _mbc->getSecondOrderKinematicalVariable();
		for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin(); it!= _pbcConstraint->constraintGroupEnd(); it++){
			constraintElement* cele = *it;
			fullMatrix<double> m;
			cele->getConstraintMatrix(m);
			fullVector<double> vec;
			cele->getFirstOrderKinematicalVector(FI,vec);
			std::vector<Dof> keyL, keyB;
			cele->getMultiplierKeys(keyL);
			cele->getConstraintKeys(keyB);
			fullVector<double> result(keyB.size());
			for (int i=0; i<keyB.size(); i++){
				_pAssembler->getDofValue(keyB[i],result(i));
			};
			fullVector<double> residual(keyL.size());
			residual.setAll(0.0);
			m.mult(result,residual);
			residual.axpy(vec,-1.0);
			constraintElementSecondOrder* hc = dynamic_cast<constraintElementSecondOrder*>(cele);
			if (hc){
        fullVector<double> hvec;
        hc->getSecondOrderKinematicalVector(G,hvec);
        residual.axpy(hvec,-1);
			}
			for (int i=0; i<keyL.size();i++){
				int row = _splittedDofs->getMultiplierDofNumber(keyL[i]);
				pbcsys->addToConstraintResidual(row,residual(i));
			};
		};
	};
};

void pbcAlgorithm::assembleLinearConstraintMatrixToSystem(){
	#ifdef _DEBUG
	printf("assemble constraint matrix to system \n");
	#endif
	std::string name = "A";
	linearSystem<double>* lsys = _pAssembler->getLinearSystem(name);
	pbcSystem<double>* pbcsys = dynamic_cast<pbcSystem<double>*>(lsys);
	if (!pbcsys){
		printf("periodic non linear system is not used! \n");
	}
	else{
		for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin(); it!= _pbcConstraint->constraintGroupEnd(); it++){
			constraintElement* cele = *it;
			fullMatrix<double> m;
			cele->getConstraintMatrix(m);
			std::vector<Dof> keyL, keyB;
			cele->getMultiplierKeys(keyL);
			cele->getConstraintKeys(keyB);
			for (int i=0; i<keyL.size(); i++){
				int row = _splittedDofs->getMultiplierDofNumber(keyL[i]);
				for (int j=0; j<keyB.size(); j++){
					int col = _pAssembler->getDofNumber(keyB[j]);
					if (fabs(m(i,j))>0.)
						pbcsys->addToConstraintMatrix(row,col,m(i,j));
				};
			};
		};

    if (pbcsys->isComputedProjectMatrices() == false)
      pbcsys->computeConstraintProjectMatrices();
	};
};

void pbcAlgorithm::allocateConstraintMatrixToSystem(){
	std::string name = "A";
	linearSystem<double>* lsys = _pAssembler->getLinearSystem(name);
	pbcSystem<double>* pbcsys = dynamic_cast<pbcSystem<double>*>(lsys);
	int size = dynamic_cast<pbcDofManager*>(_splittedDofs)->sizeOfMultiplierDof();
	if (pbcsys){
		pbcsys->allocateConstraintMatrix(size,_pAssembler->sizeOfR());
		pbcsys->setScaleFactor(_scale);
	}
};


void pbcAlgorithm::applyPBCByConstraintElimination(){
  std::map<Dof,DofAffineConstraint<double> > linearConstraint;
  STensor3 FI = _mbc->getFirstOrderKinematicalVariable();
  STensor33 G = _mbc->getSecondOrderKinematicalVariable();
  for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin(); it!= _pbcConstraint->constraintGroupEnd(); it++){
    constraintElement* cele = *it;
    constraintElementSecondOrder* hocele = dynamic_cast<constraintElementSecondOrder*>(cele);
    if (hocele == NULL)
      cele->getLinearConstraints(FI,linearConstraint);
    else
      hocele->getLinearConstraints(FI,G,linearConstraint);
    for (std::map<Dof,DofAffineConstraint<double> >::iterator it = linearConstraint.begin(); it != linearConstraint.end(); it++){
      _pAssembler->setLinearConstraint(it->first, it->second);
    };
    linearConstraint.clear();
	};
};


void pbcAlgorithm::computeDirectLinearConstraintMatrix(linearConstraintMatrix* lcon){
  linearConstraintMatrixSecondOrder* highconmat = dynamic_cast<linearConstraintMatrixSecondOrder*>(lcon);
  if (!lcon->isAllocated()){
    int csize = _splittedDofs->sizeOfIndependentMultiplierDof();
    lcon->allocate(csize);
  }
  for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin();
                                              it != _pbcConstraint->constraintGroupEnd(); it++){
    constraintElement* cele = *it;
    if (cele->getType() == constraintElement::PeriodicNode){
      fullMatrix<double> m;
      cele->getFirstOrderKinematicalMatrix(m);
      std::vector<Dof> R;
      cele->getConstraintKeys(R);
      std::vector<int> NR(R.size());
      for (unsigned int i = 0; i < R.size(); i++){
        NR[i] = _splittedDofs->getIndependentDofNumber(R[i]);
      };
      fullMatrix<double> mhigh;
      constraintElementSecondOrder* highele = dynamic_cast<constraintElementSecondOrder*>(cele);
      if (highele){
        highele->getSecondOrderKinematicalMatrix(mhigh);
      }

      for (unsigned int i = 0; i < R.size(); i++){
        if (NR[i] != -1){
          for (unsigned int j = 0; j < m.size2(); j++){
            lcon->addToFirstOrderKinematicalMatrix(NR[i],j, m(i, j));
          };
          if (highele){
            for (int j=0; j<mhigh.size2(); j++)
              highconmat->addToSecondOrderKinematicalMatrix(NR[i],j,mhigh(i,j));
          };
        }
        else{
          printf("Error in dof numbering _C matrix \n");
        };
      };
    };
  };
};
void pbcAlgorithm::computeIndirectLinearConstraintMatrix(linearConstraintMatrix* lcon){
  linearConstraintMatrixSecondOrder* highconmat = dynamic_cast<linearConstraintMatrixSecondOrder*>(lcon);
  bool addNewVer = false;
  nonLinearPeriodicBC* pbc = dynamic_cast<nonLinearPeriodicBC*>(_mbc);
  if (pbc) addNewVer = pbc->isNewVertices();
  if (addNewVer == false){
    if (!lcon->isAllocated()){
      int constraintnum = _splittedDofs->sizeOfDependentMultiplierDof();
      int bsize = _splittedDofs->sizeOfDependentDof();
      int csize = _splittedDofs->sizeOfIndependentDof();
      lcon->allocate(constraintnum,bsize,csize);
    }
    for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin();
                                                it!= _pbcConstraint->constraintGroupEnd(); it++){
      constraintElement* cele = *it;
      if (cele->getType() != constraintElement::PeriodicNode){
        fullMatrix<double> mleft, mright;
        cele->getConstraintMatrix(mleft);
        cele->getFirstOrderKinematicalMatrix(mright);
        mleft.scale(_scale);
        mright.scale(_scale);
        std::vector<Dof> R, C;
        cele->getMultiplierKeys(R);
        cele->getConstraintKeys(C);
        fullMatrix<double> mhigh;
        constraintElementSecondOrder* highele = dynamic_cast<constraintElementSecondOrder*>(cele);
        if (highele){
          highele->getSecondOrderKinematicalMatrix(mhigh);
          mhigh.scale(_scale);
        }
        std::vector<int> NR(R.size()), NC(C.size());
        for (unsigned int i = 0; i < R.size(); i++){
          NR[i] = _splittedDofs->getDependentMultiplierDofNumber(R[i]);
        };
        for (unsigned int i = 0; i < C.size(); i++){
          NC[i] = _splittedDofs->getDependentDofNumber(C[i]);
        };
        for (unsigned int i = 0; i < R.size(); i++){
          if (NR[i] != -1){
            for (unsigned int j = 0; j < C.size(); j++){
              if (NC[j] != -1){
                lcon->addToFirstMatrix(NR[i], NC[j], mleft(i, j));
              }
              else{
                int cnum = _splittedDofs->getIndependentDofNumber(C[j]);
                if (cnum != -1){
                  lcon->addToSecondMatrix(NR[i],cnum,mleft(i,j));
                }
                else
                  Msg::Info("Error in dof numbering C  Dofs  \n");
              };
            };
            for (unsigned int j = 0; j < 9; j++){
                lcon->addToFirstOrderKinematicalMatrix(NR[i],j, mright(i, j));
            };
            if (highele){
              for (int j=0; j<27; j++){
                highconmat->addToSecondOrderKinematicalMatrix(NR[i],j,mhigh(i,j));
              }
            };
          }
          else{
            printf("Error in dof numbering L Dofs \n");
          };
        };
      };
    };
  }
  else {
    STensor3 F = _mbc->getFirstOrderKinematicalVariable();
    if (!lcon->isAllocated()){
      int bsize = _splittedDofs->sizeOfDependentDof();
      int csize = _splittedDofs->sizeOfIndependentDof();
      int vsize = _splittedDofs->sizeOfVirtualDof();
      lcon->allocate(bsize,vsize,csize);
    }
    for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin();
                                            it!= _pbcConstraint->constraintGroupEnd(); it++){
      constraintElement* cele = *it;
      if (cele->getType() != constraintElement::PeriodicNode){
        std::vector<Dof> keys;
        cele->getDependentKeys(keys);
        int ndofs = keys.size();
        fullMatrix<double> mat, mhigh;
        cele->getFirstOrderKinematicalMatrix(mat);
        constraintElementSecondOrder* highele = dynamic_cast<constraintElementSecondOrder*>(cele);
        if (highele)
          highele->getSecondOrderKinematicalMatrix(mhigh);
        int size2 = mat.size2();
        for (int i=0; i<ndofs; i++){
          int row = _splittedDofs->getDependentDofNumber(keys[i]);
          for (int j=0; j<size2; j++){
            lcon->addToFirstOrderKinematicalMatrix(row,j,mat(i,j));
          };
          if (highele){
            for (int j=0; j<27; j++){
              highconmat->addToSecondOrderKinematicalMatrix(row,j,mhigh(i,j));
            }
          };
        };
        std::map<Dof,DofAffineConstraint<double> > linearConstraint;
        cele->getLinearConstraints(F,linearConstraint);
        for (std::map<Dof,DofAffineConstraint<double> >::iterator it = linearConstraint.begin(); it != linearConstraint.end(); it++){
          std::map<Dof,int>::const_iterator itbf = _splittedDofs->dependentDofFind(it->first);
          if (itbf != _splittedDofs->dependentDofEnd()){
            int row = itbf->second;
            DofAffineConstraint<double>* cons = &(it->second);
            for (int i=0; i<cons->linear.size(); i++){
              std::pair<Dof,double> pir = cons->linear[i];
              double val = pir.second;
              std::map<Dof,int>::const_iterator itsf = _splittedDofs->virtualDofFind(pir.first);
              if (itsf!= _splittedDofs->virtualDofEnd()){
                int col = itsf->second;
                lcon->addToFirstMatrix(row,col,val);
              }
              else{
                int col = _splittedDofs->getIndependentDofNumber(pir.first);
                lcon->addToSecondMatrix(row,col,val);
              };
            };
          };
        };
      };
    };
  }
};

void pbcAlgorithm::computeLinearConstraintMatrix(linearConstraintMatrix* lcon){
  if (!lcon->isAllocated()){
    int nl = _splittedDofs->sizeOfMultiplierDof();
    int nb = _splittedDofs->sizeOfBoundaryDof();
    lcon->allocate(nl,nb);
  }
  linearConstraintMatrixSecondOrder* highconmat = dynamic_cast<linearConstraintMatrixSecondOrder*>(lcon);
  for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin();
                                              it != _pbcConstraint->constraintGroupEnd(); it++){
    constraintElement* cele = *it;
    std::vector<Dof> R, C;
    cele->getMultiplierKeys(R);
    cele->getConstraintKeys(C);
    fullMatrix<double> mleft, mright, mhigh;
    cele->getConstraintMatrix(mleft);
    cele->getFirstOrderKinematicalMatrix(mright);
    constraintElementSecondOrder* highele = dynamic_cast<constraintElementSecondOrder*>(cele);
    if (highele){
      highele->getSecondOrderKinematicalMatrix(mhigh);
    }

    std::vector<int> NR(R.size()), NC(C.size());
    for (unsigned int i = 0; i < R.size(); i++){
      NR[i] = _splittedDofs->getMultiplierDofNumber(R[i]);
    };
    for (unsigned int i = 0; i < C.size(); i++){
      NC[i] = _splittedDofs->getBoundaryDofNumber(C[i]);
    };
    for (int i=0; i<R.size(); i++){
      for (int j=0; j<C.size(); j++){
        lcon->addToFirstMatrix(NR[i],NC[j],mleft(i,j));
      }
      for (int j=0; j<mright.size2(); j++){
        lcon->addToFirstOrderKinematicalMatrix(NR[i],j,mright(i,j));
      }
      if (highconmat){
        for (int j=0; j<mhigh.size2(); j++){
          highconmat->addToSecondOrderKinematicalMatrix(NR[i],j,mhigh(i,j));
        }
      }
    }
  }
};

void pbcAlgorithm::computeLinearConstraintMatrixLDBC(linearConstraintMatrix* lcon){
  if (lcon->isAllocated() == false){
    lcon->allocate(_splittedDofs->sizeOfBoundaryDof());
  };
  linearConstraintMatrixSecondOrder* holcon = dynamic_cast<linearConstraintMatrixSecondOrder*>(lcon);
  for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin();
                                              it != _pbcConstraint->constraintGroupEnd(); it++){
    constraintElement* cele = *it;
    std::vector<Dof> R;
    cele->getConstraintKeys(R);
    fullMatrix<double> m, mhigh;
    cele->getFirstOrderKinematicalMatrix(m);
    constraintElementSecondOrder* highele = dynamic_cast<constraintElementSecondOrder*>(cele);
    if (highele){
      highele->getSecondOrderKinematicalMatrix(mhigh);
    }
    std::vector<int> NR(R.size());
    for (unsigned int i = 0; i < R.size(); i++){
      NR[i] = _splittedDofs->getBoundaryDofNumber(R[i]);
    };
    for (int i=0; i<R.size(); i++){
      for (int j=0; j<m.size2(); j++){
        lcon->addToFirstOrderKinematicalMatrix(NR[i],j,m(i,j));
      }
      if (holcon){
        for (int j=0; j<mhigh.size2(); j++){
          holcon->addToSecondOrderKinematicalMatrix(NR[i],j,mhigh(i,j));
        }
      }
    }
  };
};

void pbcAlgorithm::computeLoadVector(){
  std::string name = "A";
  linearSystem<double>* lsys = _pAssembler->getLinearSystem(name);
  nonLinearSystem<double>* nonsys = dynamic_cast<nonLinearSystem<double>*>(lsys);
  if (nonsys == NULL){
    Msg::Error("The nonlinear system must be used");
    return;
  }
  else{
    nonsys->zeroLoadVector();
    STensor3 dF = _mbc->getDifferenceOfDeformationGradient();
    STensor33 dG = _mbc->getDifferenceOfGradientOfDeformationGradient();
    for (std::set<constraintElement*>::iterator it = _pbcConstraint->constraintGroupBegin();
                                              it != _pbcConstraint->constraintGroupEnd(); it++){
      constraintElement* cele = *it;
      std::vector<Dof> R;
      fullVector<double> m;
      cele->getMultiplierKeys(R);
      cele->getFirstOrderKinematicalVector(dF,m);
      for (int i=0; i< R.size(); i++){
        int num = _splittedDofs->getMultiplierDofNumber(R[i]);
        if (num <0)
          Msg::Fatal("Error in number Dof");
        else
          nonsys->addToLoadVector(num,m(i));
      }

      constraintElementSecondOrder* highele = dynamic_cast<constraintElementSecondOrder*>(cele);
      if (highele){
        highele->getSecondOrderKinematicalVector(dG,m);
        for (int i=0; i< R.size(); i++){
          int num = _splittedDofs->getMultiplierDofNumber(R[i]);
          if (num <0)
            Msg::Fatal("Error in number Dof");
          else
            nonsys->addToLoadVector(num,m(i));
        }
      }

    }
  }
};



