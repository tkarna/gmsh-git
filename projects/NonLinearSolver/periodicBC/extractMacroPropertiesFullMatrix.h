#ifndef EXTRACTMACROPROPERTIESFULLMATRIX_H_
#define EXTRACTMACROPROPERTIESFULLMATRIX_H_

#include "extractMacroProperties.h"
#include "splitStiffnessMatrixFullMatrix.h"
#include "linearConstraintMatrixFullMatrix.h"


class stiffnessCondensationFullMatrix : public stiffnessCondensation{
  protected:
    pbcAlgorithm* _pAl;
    bool _stressflag, _tangentflag;
    dofManager<double>* _pAssembler;
    splitStiffnessMatrixFullMatrix* _stiffness;
    linearConstraintMatrixFullMatrix* _bConMat, *_cConMat;
		fullMatrix<double>* _CbT,*_SbT, *_ScT, *_RT, *_CcT;

  private:
    fullMatrix<double>* Kbbt,* Kbct,* Kcbt,*Kcct,*Kblamda,* Kccstar,*Sbc,*SbcT ,* ScTKccstar,*L1,*L;

  public:
    stiffnessCondensationFullMatrix(dofManager<double>* pAssem, pbcAlgorithm* pal,bool sflag = false, bool tflag = true)
                                    : stiffnessCondensation(),_pAssembler(pAssem),_pAl(pal),
                                    _stressflag(sflag),_tangentflag(tflag),_bConMat(0),_cConMat(0){
      _stiffness = static_cast<splitStiffnessMatrixFullMatrix*>(_pAl->getSplitStiffness());
    }
    virtual ~stiffnessCondensationFullMatrix(){};
    virtual void init();
    virtual void clear();
    virtual void stressSolve();
		virtual void tangentCondensationSolve();
};

#endif // EXTRACTMACROPROPERTIESFULLMATRIX_H_
