#ifndef PBCCREATCONSTRAINTS_H_
#define PBCCREATCONSTRAINTS_H_

#include "partDomain.h"
#include "pbcConstraintElement.h"
#include "nonLinearBC.h"
#include "pbcConstraintElementHighOrder.h"
#include "nonLinearMicroBC.h"
#include "dofManager.h"
class GModel;

/*
   //This class creates all constraints based on vertices
   //for RVE 2D
   // line periodic l12 (g[0]) - l34 (g[2])
   // line periodic l41 (g[1]) - l23 (g[3])
   // node periodic 1 - 2 -3 -4
   // root control point 1
   // other control points 2 - 4
   // base line 12 - 41

   4--------l34-------3
   |                  |
   |                  |
  l41      RVE 2D    l23
   |                  |
   |                  |
   1-------l12--------2



   // FOR RVE 3D

   // periodic surface 1234 (g[0]) - 5678 (g[3])
   // periodic surface 1562 (g[1]) - 4873 (g[4])
   // periodic surface 1584 (g[2]) - 2673 (g[5])

   // line periodic l12 - l34 - l46 - l78
   // line periodic l41 - l23 - l67 - l85
   // line periodic l15 - l26 - l37 - l48

   // node periodic 1 - 2 - 3 - 4 - 5 - 6 - 7 - 8

   // root control point 1
   // other control points 2 - 4 - 5
   // base line 12 - 41 - 15
   // base surface 1234 - 1562 - 1584

            8--------l78--------7
           *|                  *|
         *  !                *  |
       l85  !              l67  |
      *    l48             *   l37
    *       !            *      |
   5--------!l56--------6       |
   |        4-------l34-|-------3
   |       *            |      *
  l15    *             l26    *
   |   l41              |  l23
   |  *                 |  *
   | *                  |*
   1--------l12---------2
*/


class pbcConstraintElementGroup{
  protected:
    bool _fullDG;
    std::vector<partDomain*>& _allDomain;
    nonLinearMicroBC* _mbc; // micro bc
    FunctionSpaceBase* _LagSpace, *_MultSpace; // lagspace and mult space
    std::set<constraintElement*> _allConstraint; // all constraint
    std::set<MVertex*> _cVertices;  // corner vertices
    std::set<MVertex*> _bVertices;  // edge vertices, in 2D corner and edger vertices are same
    std::set<MVertex*> _virtualVertices; // new vertices added to systems for polynomial interpolation
    std::vector<MVertex*> _xBase, _yBase, _zBase; // interpolation basis
    groupOfElements* _g;
    /*in case of 2D*/
    groupOfElements* f1234; // surface
    std::set<MVertex*> l12, l23, l34,l41; // 4 edges
    //add surfaces of RVE in 3D cases
    groupOfElements *f5678, *f1562, *f4873, *f1584, *f2673;
    // add 8 edges of RVE in 3D case
    std::set<MVertex*> l56, l67, l78, l85, l15, l26, l37, l48;
    MVertex* _v1, *_v2, *_v4, *_v5; // all control points
    MVertex* _v3, *_v6, *_v7, *_v8;
    groupOfElements* _gX, *_gY, *_gZ;
    groupOfElements* _gXY, *_gYZ, *_gZX;


  protected:
    void __init__();
    // add control point in case of missing
    void addControlPoints();
    // add other corner nodes
    void addOtherCornerPoints();
    // create interpolation basis
    void createPolynomialBasePoints();
    /*create a surface group to interpolate form two boundaries for FE method */
    void createBCGroup3D(std::vector<MVertex*>& vlx, std::vector<MVertex*>& vly,
                         MVertex* vc, groupOfElements* g);
    /*create all interpolate surfaces for FE method only*/
    void createAllBCGroups();
    /*
    This function allows to find a nearest vertex in a group vertex to one vertex
    by using the minimal distance flag
    */
    template<class Iterator>
    MVertex* findNearestVertex(Iterator itbegin, Iterator itend, MVertex* vp);
    MVertex* findNearestVertex(groupOfElements* g, MVertex* vp);
    /*
    Enforce periodic boundary condition on conformal meshes
    positive part: posivtiveitbegin-->positiveitend
    negative part: negativeitbegin-->negativeitend
    others = corner vertices
    */
    template<class Iterator>
    void periodicConditionForPeriodicMesh(Iterator posivtiveitbegin, Iterator positiveitend,
                                               Iterator negativeitbegin, Iterator negativeitend,
                                               std::set<MVertex*>& others);

    /* get interpolation points on a line*/
		void createNewVerticesForInterpolationPoints(int degree, MVertex* vinit, MVertex* vend,
                                                 std::vector<MVertex*>& base,
                                                 std::set<MVertex*>& others);
    // in this case, vinit and vend using for constructing the distance function
		template<class Iterator>
		void getInterpolatioPointsFromExistingVertices(int degree, MVertex* vinit, MVertex* vend,
                                                   Iterator itbegin, Iterator itend,
                                                   std::vector<MVertex*>& base);

    // for cubic spline
		template<class Iterator>
		void cubicSplineFormCondition(Iterator itbegin, Iterator itend,
                                  std::vector<MVertex*>& vlist, int flag);
		template<class Iterator>
		void cubicSplinePeriodicCondition(Iterator itbegin, Iterator itend,
                                      std::vector<MVertex*>& vlist, int flag, std::set<MVertex*>& others);
    // for cubic spline
		template<class Iterator>
		void cubicSplineFormConditionCoonsPatch(Iterator itbegin, Iterator itend, MVertex* vref,
                                    std::vector<MVertex*>& xlist, std::vector<MVertex*>& ylist,
                                    int flagx, int flagy,std::set<MVertex*>& others);
		template<class Iterator>
		void cubicSplinePeriodicConditionCoonsPatch(Iterator itbegin, Iterator itend, MVertex* vref,
                                        std::vector<MVertex*>& xlist,std::vector<MVertex*>& ylist,
                                        int flagx, int flagy, std::set<MVertex*>& others);
		// for lagrange
		template<class Iterator>
		void lagrangeFormCondition(Iterator itbegin, Iterator itend,
                                std::vector<MVertex*>& vlist);
		template<class Iterator>
		void lagrangePeriodicCondition(Iterator itbegin, Iterator itend,
                                   std::vector<MVertex*>& vlist,std::set<MVertex*>& others );

    // for lagrange
		template<class Iterator>
		void lagrangeFormConditionCoonsPatch(Iterator itbegin, Iterator itend, MVertex* vref,
                                 std::vector<MVertex*>& xlist, std::vector<MVertex*>& ylist,
                                 std::set<MVertex*>& others );
		template<class Iterator>
		void lagrangePeriodicConditionCoonsPatch(Iterator itbegin, Iterator itend, MVertex* vref,
                                     std::vector<MVertex*>& xlist,std::vector<MVertex*>& ylist,
                                     std::set<MVertex*>& others );

		bool isInside(MVertex* v, MElement* e) const;
    template<class Iterator>
    void pbcFEConstraintElementGroup(Iterator itbegin, Iterator itend, groupOfElements* g,
                                     std::set<MVertex*>& others);


    void createPeriodicMeshConstraintElementGroup();
    void createCubicSplineConstraintElementGroup();
    void createLagrangeConstraintElementGroup();
    void createFEConstraintElementGroup();

		void createLinearDisplacementConstraintElementGroup();
		void createMinimalKinematicConstraintElementGroup();
		void createMixBCConstraintElementGroup();

    SVector3 getUniformDisp(MVertex* v);
    SVector3 getPerturbation(dofManager<double>* pmanager, MVertex* v);
    SVector3 getTangentCubicSpline(dofManager<double>* pmanager, MVertex* v,const int dir);
    void cubicSplineToFile(std::vector<MVertex*>& vlist, dofManager<double>* pmanager, FILE* file, const int dir);
    void lagrangeToFile(std::vector<MVertex*>& vlist, dofManager<double>* pmanager, FILE* file);

  public:
    pbcConstraintElementGroup(nonLinearMicroBC* bc, std::vector<partDomain*>& allDom,
                              FunctionSpaceBase* lagspace, FunctionSpaceBase* multspace);
    virtual ~pbcConstraintElementGroup();
    void createConstraintGroup();
    std::set<constraintElement*>::iterator constraintGroupBegin(){return _allConstraint.begin();};
		std::set<constraintElement*>::iterator constraintGroupEnd(){return _allConstraint.end();};
		int size() const {return _allConstraint.size();};
    std::set<MVertex*>::iterator cVertexBegin(){return _cVertices.begin();};
    std::set<MVertex*>::iterator cVertexEnd(){return _cVertices.end();};
    std::set<MVertex*>::iterator virtualVertexBegin(){return _virtualVertices.begin();};
    std::set<MVertex*>::iterator virtualVertexEnd(){return _virtualVertices.end();};
    bool isActive(const MVertex* v) const; // check vertex lying in the constraints
    bool isVirtual(const MVertex* v) const;
    int sizeVirtualVertices() const {return _virtualVertices.size();};
    void print() const ;
    const groupOfElements* getAllBulkElement() const {return _g;};
    void createPBCFromFile(FILE* fp, GModel* pModel);
    void writePertBoundaryToFile(dofManager<double>* pmanager,const int e=0, const int g=0, const int iter =0);
};

#endif // PBCCREATCONSTRAINTS_H_
