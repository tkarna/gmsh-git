#ifndef LINEARCONSTRAINTMATRIX_H_
#define LINEARCONSTRAINTMATRIX_H_

// interface for linear constraint matrix

class linearConstraintMatrixBase{
  public:
    virtual ~linearConstraintMatrixBase(){};
    virtual bool isAllocated() const =0;
    virtual void allocate(int nRows, int nCols = 0, int nstar = 0) = 0;
};

// ------------------------------
// For the constraint matrix C_b u_b+ C_c u_c = S (FM- I) + T*GM
// First-matrix == C_b
// Second matrix == C_c
// First order kinematic matrix = S
// Second order kinematic matrix = T
//-------------------------
// For the constraint matrix u_b = C_bc u_c + S(FM-I)+ T*GM
// First matrix == C_bc,
// First order kinematic matrix = S
// Second order kinematic matrix = T
//----------------------------------
// For the constraint matrix  ub = Cu* + S(FM-I) + Muc+ T*GM
// First matrix == C
// Second matrix == M
// First order kinematic matrix = S
// Second order kinematic matrix = T
// --------------------------------
// For the constraint matrix u_b = S(FM-I) + T*GM
// First order kinematic matrix = S
// Second order kinematic matrix = T

class linearConstraintMatrix : public linearConstraintMatrixBase{
	public:
		virtual ~linearConstraintMatrix(){};
		virtual bool isAllocated() const = 0;
		virtual void allocate(int row, int firstcol =0, int secondcol =0) =0;
		virtual void clear() = 0;
		// first matrix
		virtual void addToFirstMatrix(int i, int j, double val)  = 0;
		// second matrix
		virtual void addToSecondMatrix(int i, int j, double val) = 0;
		// first order kinematical matrix
		virtual void addToFirstOrderKinematicalMatrix(int i, int j, double val) = 0;
};



//for second order homogenization
class linearConstraintMatrixSecondOrder{
  public:
    virtual ~linearConstraintMatrixSecondOrder(){};
    // for second kinematical matrix
    virtual void addToSecondOrderKinematicalMatrix(int i, int j, double val) =0;
};

#endif // LINEARCONSTRAINTMATRIX_H_
