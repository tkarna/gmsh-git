
#include "extractMacroPropertiesInterpolationPETSc.h"
#include "nonLinearSystems.h"
#include "pbcSystems.h"
#include "functionPETSc.h"

interpolationStiffnessCondensationPETSC::interpolationStiffnessCondensationPETSC(dofManager<double>* p, pbcAlgorithm* ma,
                                        bool sflag, bool tflag, MPI_Comm com):
                                       stiffnessCondensation(),_pAssembler(p),_pAl(ma),_tangentflag(tflag),_stressflag(sflag),
                                       _bConMat(NULL),_cConMat(NULL),_comm(com){
  _stiffness = static_cast<splitStiffnessMatrixPETSc*>(_pAl->getSplitStiffness());
};
interpolationStiffnessCondensationPETSC::~interpolationStiffnessCondensationPETSC(){
  this->clear();
}


void interpolationStiffnessCondensationPETSC::init(){
  this->clear();
  this->isInitialized() = true;

  pbcDofManager* pbcAssembler = _pAl->getSplittedDof();
  _bConMat = new linearConstraintMatrixPETSc(_comm);
  int nb = pbcAssembler->sizeOfDependentDof();
  int nc = pbcAssembler->sizeOfIndependentDof();
  int nvirt = pbcAssembler->sizeOfVirtualDof();
  Msg::Info("nb = %d, nvirt = %d, nc = %d",nb,nvirt,nc);
	_bConMat->allocate(nb,nvirt,nc);
	_pAl->computeIndirectLinearConstraintMatrix(_bConMat);

	_cConMat = new linearConstraintMatrixPETSc(_comm);
	_cConMat->allocate(nc);
	_pAl->computeDirectLinearConstraintMatrix(_cConMat);

  Mat& C = _bConMat->getFirstMatrix();
  Mat& Sb = _bConMat->getFirstOrderKinematicalMatrix();
  Mat& M = _bConMat->getSecondMatrix();
  Mat& Sc = _cConMat->getFirstOrderKinematicalMatrix();

  if (nvirt > 0){
    _try(MatTranspose(C,MAT_INITIAL_MATRIX,&_CT));
    _try(MatAssemblyBegin(_CT,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(_CT,MAT_FINAL_ASSEMBLY));
  }

  _try(MatMatMult(M,Sc,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&_Sbc));
  _try(MatAXPY(_Sbc,1.,Sb,DIFFERENT_NONZERO_PATTERN));
  _try(MatAssemblyBegin(_Sbc,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(_Sbc,MAT_FINAL_ASSEMBLY));

  _try(MatTranspose(_Sbc,MAT_INITIAL_MATRIX,&_SbcT));
  _try(MatAssemblyBegin(_SbcT,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(_SbcT,MAT_FINAL_ASSEMBLY));

  _try(MatTranspose(Sc,MAT_INITIAL_MATRIX,&_ScT));
  _try(MatAssemblyBegin(_ScT,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(_ScT,MAT_FINAL_ASSEMBLY));
};

void interpolationStiffnessCondensationPETSC::clear(){
  if (this->isInitialized()){
    this->isInitialized() = false;
    _try(MatDestroy(&_CT));
    _try(MatDestroy(&_Sbc));
    _try(MatDestroy(&_SbcT));
    _try(MatDestroy(&_ScT));
    if (_bConMat) delete _bConMat;
    if (_cConMat) delete _cConMat;
    _bConMat =NULL;
    _cConMat = NULL;
  }
};

void interpolationStiffnessCondensationPETSC::stressSolve(){
  if (_stressflag){
    if (!isInitialized()){
      this->init();
    }
    pbcDofManager* pbcAssembler = static_cast<pbcDofManager*>(_pAl->getSplittedDof());
    int sizeb = pbcAssembler->sizeOfDependentDof();
    int sizec = pbcAssembler->sizeOfIndependentDof();

    Vec lambdaB, lambdaC;
    _try(VecCreate(_comm,&lambdaB));
    _try(VecSetSizes(lambdaB,sizeb,PETSC_DECIDE));
    _try(VecSetFromOptions(lambdaB));
    _try(VecCreate(_comm,&lambdaC));
    _try(VecSetSizes(lambdaC,sizec,PETSC_DECIDE));
    _try(VecSetFromOptions(lambdaC));

    std::string str = "A";
    linearSystem<double>* lsys = _pAssembler->getLinearSystem(str);
    nonLinearSystem<double>* linearsys = dynamic_cast<nonLinearSystem<double>*>(lsys);
    pbcSystem<double>* pbcsys = dynamic_cast<pbcSystem<double>*>(lsys);
    if (pbcsys){
      for (std::map<Dof,int>::const_iterator it = pbcAssembler->dependentDofBegin();
                                             it!= pbcAssembler->dependentDofEnd(); it++){
        double val = 0.;
        int num = _pAssembler->getDofNumber(it->first);
        if (num != -1)
          linearsys->getFromRightHandSideMinus(num, val);
        else
          Msg::Error("The pbcNonLinearSystem has to used");
        PetscInt row = it->second;
        _try(VecSetValues(lambdaB,1,&row,&val,INSERT_VALUES));
      }
      for (std::map<Dof,int>::const_iterator it = pbcAssembler->independentDofBegin();
                                             it!= pbcAssembler->independentDofEnd(); it++){
        double val = 0.;
        int num = _pAssembler->getDofNumber(it->first);
        if (num != -1)
          linearsys->getFromRightHandSideMinus(num, val);
        else
          Msg::Error("The pbcNonLinearSystem has to used");
        PetscInt row = it->second;
        _try(VecSetValues(lambdaC,1,&row,&val,INSERT_VALUES));
      }
      Vec P;

      _try(VecCreate(_comm,&P));
      _try(VecSetSizes(P,9,PETSC_DECIDE));
      _try(VecSetFromOptions(P));

      _try(MatMult(_SbcT,lambdaB,P));
      _try(MatMultAdd(_ScT,lambdaC,P,P));
      _try(VecAssemblyBegin(P));
      _try(VecAssemblyEnd(P));
      #ifdef _DEBUG
      _try(VecView(P,PETSC_VIEWER_STDOUT_SELF));
      #endif

      STensor3& stress = this->getRefToMacroStress();
      double& rvevolume = this->getRVEVolume();
      PetscScalar* val;
      _try(VecGetArray(P,&val));
      for (int row = 0; row<9; row++){
        int i,j;
        Tensor23::getIntsFromIndex(row,i,j);
        stress(i,j) =val[row]/rvevolume;
      };

      _try(VecDestroy(&lambdaB));
      _try(VecDestroy(&lambdaC));
      _try(VecDestroy(&P));
    }
    else{
      Msg::Error("The pbc nonlinear solver have to used to compute");
    };
  };
}

void interpolationStiffnessCondensationPETSC::tangentCondensationSolve(){
  if (_tangentflag){
    if (!isInitialized()){
      this->init();
    }
    pbcDofManager* pbcAssembler = static_cast<pbcDofManager*>(_pAl->getSplittedDof());
    int sizeVirt = pbcAssembler->sizeOfVirtualDof();

    Mat& Kii = _stiffness->getIIMat();
    Mat& Kib = _stiffness->getIBMat();
    Mat& Kic = _stiffness->getICMat();

    Mat& Kbi = _stiffness->getBIMat();
    Mat& Kbb = _stiffness->getBBMat();
    Mat& Kbc = _stiffness->getBCMat();

    Mat& Kci = _stiffness->getCIMat();
    Mat& Kcb = _stiffness->getCBMat();
    Mat& Kcc = _stiffness->getCCMat();
    Mat Kbbt, Kbct, Kcbt, Kcct;

    // for factorize information

    int ni = pbcAssembler->sizeOfInternalDof();
    if (ni>0){
      Mat matfac;
      _try(MatDuplicate(Kii,MAT_COPY_VALUES,&matfac));
      _try(MatAssemblyBegin(matfac,MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(matfac,MAT_FINAL_ASSEMBLY));

      _try(MatGetOrdering(matfac,  "natural",  &perm,  &iperm));
      _try(MatFactorInfoInitialize(&info));
      _try(MatLUFactor(matfac, perm, iperm, &info));

      _try(GetSchurComplementMat(matfac,Kib,Kbi,Kbb,&Kbbt));
      _try(GetSchurComplementMat(matfac,Kic,Kci,Kcc,&Kcct));
      _try(GetSchurComplementMat(matfac,Kic,Kbi,Kbc,&Kbct));
      //_try(GetSchurComplementMat(matfac,Kib,Kci,Kcb,&Kcbt));

      _try(MatTranspose(Kbct,MAT_INITIAL_MATRIX,&Kcbt));
      _try(MatAssemblyBegin(Kcbt,MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(Kcbt,MAT_FINAL_ASSEMBLY));

      _try(MatDestroy(&matfac));
    }
    else{
      _try(MatDuplicate(Kbb,MAT_COPY_VALUES,&Kbbt));
      _try(MatDuplicate(Kbc,MAT_COPY_VALUES,&Kbct));
      _try(MatDuplicate(Kcb,MAT_COPY_VALUES,&Kcbt));
      _try(MatDuplicate(Kcc,MAT_COPY_VALUES,&Kcct));
    }

    Mat& C = _bConMat->getFirstMatrix();
    Mat& Sb = _bConMat->getFirstOrderKinematicalMatrix();
    Mat& M = _bConMat->getSecondMatrix();
    Mat& Sc = _cConMat->getFirstOrderKinematicalMatrix();

    Mat Kstar;
    if (sizeVirt >0){
      Mat KbbtC;
      _try(MatMatMult(Kbbt,C, MAT_INITIAL_MATRIX,PETSC_DEFAULT,&KbbtC));
      _try(MatMatMult(_CT,KbbtC, MAT_INITIAL_MATRIX,PETSC_DEFAULT,&Kstar));
      _try(MatAssemblyBegin(Kstar,MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(Kstar,MAT_FINAL_ASSEMBLY));
      _try(MatDestroy(&KbbtC));
    };

    Mat KbbstarT, KccstarT, SbcTKbbt, ScTKcct;
    _try(MatMatMult(_SbcT,Kbbt, MAT_INITIAL_MATRIX,PETSC_DEFAULT,&SbcTKbbt));
    _try(MatMatMult(_ScT,Kcct, MAT_INITIAL_MATRIX,PETSC_DEFAULT,&ScTKcct));

    _try(MatMatMult(_ScT,Kcbt, MAT_INITIAL_MATRIX,PETSC_DEFAULT,&KbbstarT));
    _try(MatMatMult(_SbcT,Kbct, MAT_INITIAL_MATRIX,PETSC_DEFAULT,&KccstarT));

    _try(MatAXPY(KbbstarT,1,SbcTKbbt,DIFFERENT_NONZERO_PATTERN));
    _try(MatAXPY(KccstarT,1,ScTKcct,DIFFERENT_NONZERO_PATTERN));

    _try(MatDestroy(&SbcTKbbt));
    _try(MatDestroy(&ScTKcct));

    Mat L, L1;
    _try(MatMatMult(KbbstarT,_Sbc, MAT_INITIAL_MATRIX,PETSC_DEFAULT,&L));
    _try(MatMatMult(KccstarT,Sc, MAT_INITIAL_MATRIX,PETSC_DEFAULT,&L1));

    _try(MatAXPY(L,1.,L1,DIFFERENT_NONZERO_PATTERN));
    _try(MatDestroy(&L1));

    if (sizeVirt >0){
      Mat KbbstarTC, KbbstarTCT, L2;
      _try(MatMatMult(KbbstarT,C, MAT_INITIAL_MATRIX,PETSC_DEFAULT,&KbbstarTC));
      _try(MatTranspose(KbbstarTC,MAT_INITIAL_MATRIX,&KbbstarTCT));
      _try(MatGetOrdering(Kstar,  "natural",  &perm,  &iperm));
      _try(MatFactorInfoInitialize(&info));
      _try(MatLUFactor(Kstar, perm, iperm, &info));
      _try(GetSchurComplementMat(Kstar,KbbstarTCT,KbbstarTC,PETSC_NULL,&L2));
      _try(MatAXPY(L,1.,L2,DIFFERENT_NONZERO_PATTERN));
      _try(MatDestroy(&Kstar));
      _try(MatDestroy(&KbbstarTC));
      _try(MatDestroy(&KbbstarTCT));
      _try(MatDestroy(&L2));
    }

    _try(MatAssemblyBegin(L,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(L,MAT_FINAL_ASSEMBLY));


    STensor43& tangent = this->getRefToMacroTangent();
    PetscInt Row, Col;
    _try(MatGetSize(L,&Row,&Col));
    double& rvevolume = this->getRVEVolume();
    if (rvevolume >0){
      for (int row = 0; row< Row; ++row){
        int i,j;
        Tensor23::getIntsFromIndex(row,i,j);
        for (int col = 0; col<Col; ++col){
          PetscScalar val;
          PetscInt ii = row, jj = col;
          _try(MatGetValues(L,1,&ii,1,&jj,&val));
          int k,l;
          Tensor23::getIntsFromIndex(col,k,l);
          tangent(i,j,k,l) = val/rvevolume;
        };
      };
    }
    else{
      printf("Error in rve volume /n");
    };
    _try(MatDestroy(&L));
  };
};
