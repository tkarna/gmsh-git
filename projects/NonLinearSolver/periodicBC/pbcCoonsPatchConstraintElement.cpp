#include "pbcCoonsPatchConstraintElement.h"

CoonsPatchCubicSplineConstraintElement::CoonsPatchCubicSplineConstraintElement(FunctionSpaceBase* lagspace, FunctionSpaceBase* multspace,
                                        int n, MVertex* vp, MVertex* vre, MVertex* vx1, MVertex* vx2,
                                          MVertex* vy1, MVertex* vy2, int flagx, int flagy)
                                          :_periodicSpace(lagspace),_multSpace(multspace),_reference(vre),
                                        _ndofs(n),_vp(vp),_vnx1(vx1),_vnx2(vx2),_vny1(vy1),_vny2(vy2),_flagx(flagx),_flagy(flagy){
  SPoint3 vppoint(_vp->point());
  _x1 = distance(_vnx1->point(),_vny1->point(),_vny2->point());
  _x2 = distance(_vnx2->point(),_vny1->point(),_vny2->point());
  _y1 = distance(_vny1->point(),_vnx1->point(),_vnx2->point());
  _y2 = distance(_vny2->point(),_vnx1->point(),_vnx2->point());
  // get 3rd independent vertex
  MVertex * vtemp(NULL);
  if (_y2>_y1)
    vtemp =_vny2;
  else
    vtemp = _vny1;
  // get projected point on negative surface
  SPoint3 vnpoint= project(vppoint,_vnx1->point(),_vnx2->point(),vtemp->point());
  // get  surface coordinate on negative surface
  _xi = distance(vnpoint,_vny1->point(),_vny2->point());
  _neta = distance(vnpoint,_vnx1->point(),_vnx2->point());
  // left matrix
  _C.resize(_ndofs,10*_ndofs); _C.setAll(0.0);
  _Cbc.resize(_ndofs,9*_ndofs); _Cbc.setAll(0.);
	int col = 0;
	for (int i=0; i<_ndofs; i++){
		_C(i,col) = 1.0;
		col++;
	};
	cubicSplineConstraintElement::getFF(_xi,_x1,_x2,_FFx);
	cubicSplineConstraintElement::getFF(_neta,_y1,_y2,_FFy);
	for (int i=0; i<_FFx.size(); i++){
		for (int j=0; j<_ndofs; j++){
				_C(j,col) = -1.*_FFx(i);
				_Cbc(j, col-_ndofs) = _FFx(i);
				col++;
		};
	};
	for (int i=0; i<_FFy.size(); i++){
		for (int j=0; j<_ndofs; j++){
				_C(j,col) = -1.*_FFy(i);
				_Cbc(j,col-_ndofs) = _FFy(i);
				col++;
		};
	};
	for (int i=0; i<_ndofs; i++){
		_C(i,col) = 1.0;
    _Cbc(i,col-_ndofs) = -1.;
		col++;
	};


	_L(0) = _vp->x() - _FFx(0)*_vnx1->x() - _FFx(2)*_vnx2->x()
          - _FFy(0)*_vny1->x() - _FFy(2)*_vny2->x()+ _reference->x();
  _L(1) = _vp->y() - _FFx(0)*_vnx1->y() - _FFx(2)*_vnx2->y()
          - _FFy(0)*_vny1->y() - _FFy(2)*_vny2->y()+ _reference->y();
  _L(2) = _vp->z() - _FFx(0)*_vnx1->z() - _FFx(2)*_vnx2->z()
          - _FFy(0)*_vny1->z() - _FFy(2)*_vny2->z()+ _reference->z();

  // right matrix
	getMatrixFromVector(_L,_S);
};


void CoonsPatchCubicSplineConstraintElement::getConstraintKeys(std::vector<Dof>& keys) const{
	getKeysFromVertex(_periodicSpace,_vp,keys);
	getKeysFromVertex(_periodicSpace,_vnx1,keys);
	cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vnx1,keys,_flagx);
	getKeysFromVertex(_periodicSpace,_vnx2,keys);
	cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vnx2,keys,_flagx);
	getKeysFromVertex(_periodicSpace,_vny1,keys);
	cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vny1,keys,_flagy);
	getKeysFromVertex(_periodicSpace,_vny2,keys);
	cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vny2,keys,_flagy);
	getKeysFromVertex(_periodicSpace,_reference,keys);
};
void CoonsPatchCubicSplineConstraintElement::getMultiplierKeys(std::vector<Dof>& keys) const{
	getKeysFromVertex(_multSpace,_vp, keys);
};
void CoonsPatchCubicSplineConstraintElement::getConstraintMatrix(fullMatrix<double>& m) const{
  m = _C;
};
void CoonsPatchCubicSplineConstraintElement::getFirstOrderKinematicalMatrix(fullMatrix<double>& m) const{
  m = _S;
};

void CoonsPatchCubicSplineConstraintElement::getFirstOrderKinematicalVector(const STensor3& FM, fullVector<double>& m) const{
	m.resize(_ndofs); m.setAll(0.);
	for (int i=0; i<_ndofs; i++){
	  for (int j=0; j<3; j++)
      m(i) += FM(i,j)*_L(j);
	};
};

void CoonsPatchCubicSplineConstraintElement::print() const{
  Msg::Info("Patch Coons Cubic Spline constraint element 3D");
  Msg::Info("positive = %d",_vp->getNum());
  Msg::Info("negative x = %d %d",_vnx1->getNum(),_vnx2->getNum());
  Msg::Info("negative y = %d %d",_vny1->getNum(),_vny2->getNum());
};


void CoonsPatchCubicSplineConstraintElement::getDependentKeys(std::vector<Dof>& key) const{
  getKeysFromVertex(_periodicSpace,_vp,key);
}

void CoonsPatchCubicSplineConstraintElement::getIndependentKeys(std::vector<Dof>& keys) const{
  getKeysFromVertex(_periodicSpace,_vnx1,keys);
	cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vnx1,keys,_flagx);
	getKeysFromVertex(_periodicSpace,_vnx2,keys);
	cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vnx2,keys,_flagx);
	getKeysFromVertex(_periodicSpace,_vny1,keys);
	cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vny1,keys,_flagy);
	getKeysFromVertex(_periodicSpace,_vny2,keys);
	cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vny2,keys,_flagy);
	getKeysFromVertex(_periodicSpace,_reference,keys);
}

void CoonsPatchCubicSplineConstraintElement::getVirtualKeys(std::vector<Dof>& key) const{
  if ((_vnx1) and (_vnx2)){
    cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vnx1,key,_flagx);
    cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vnx2,key,_flagx);
  };
  if ((_vny1) and (_vny2)){
    cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vny1,key,_flagy);
    cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vny2,key,_flagy);
  };
}

void CoonsPatchCubicSplineConstraintElement::getLinearConstraints(const STensor3& F,
                     std::map<Dof,DofAffineConstraint<double> >& con) const{
  std::vector<Dof> k, kx1, kx2, keysx1, keysx2;
  getKeysFromVertex(_periodicSpace,_vp,k);
  if ((_vnx1) and (_vnx2)){
    getKeysFromVertex(_periodicSpace,_vnx1,kx1);
    getKeysFromVertex(_periodicSpace,_vnx2,kx2);
    cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vnx1,keysx1,_flagx);
    cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vnx2,keysx2,_flagx);
  };

  std::vector<Dof> ky1, ky2, keysy1, keysy2;
  if ((_vny1) and (_vny2)){
    getKeysFromVertex(_periodicSpace,_vny1,ky1);
    getKeysFromVertex(_periodicSpace,_vny2,ky2);
    cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vny1,keysy1,_flagy);
    cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vny2,keysy2,_flagy);
  };

  std::vector<Dof> kref;
  if (_reference){
    getKeysFromVertex(_periodicSpace,_reference,kref);
  }

  fullVector<double> FFx, FFy;
  cubicSplineConstraintElement::getFF(_xi,_x1,_x2,FFx);
  cubicSplineConstraintElement::getFF(_neta,_y1,_y2,FFy);
  fullVector<double> g;
  getFirstOrderKinematicalVector(F,g);
  DofAffineConstraint<double> cons;
  for (int i=0; i<_ndofs; i++){
    cons.shift=g(i);
    if ((_vnx1) and (_vnx2)){
      cons.linear.push_back(std::pair<Dof,double>(kx1[i],FFx(0)));
      cons.linear.push_back(std::pair<Dof,double>(keysx1[i],FFx(1)));
      cons.linear.push_back(std::pair<Dof,double>(kx2[i],FFx(2)));
      cons.linear.push_back(std::pair<Dof,double>(keysx2[i],FFx(3)));
    };
    if ((_vny1) and (_vny2)){
      cons.linear.push_back(std::pair<Dof,double>(ky1[i],FFy(0)));
      cons.linear.push_back(std::pair<Dof,double>(keysy1[i],FFy(1)));
      cons.linear.push_back(std::pair<Dof,double>(ky2[i],FFy(2)));
      cons.linear.push_back(std::pair<Dof,double>(keysy2[i],FFy(3)));
    };
    if (_reference){
      cons.linear.push_back(std::pair<Dof,double>(kref[i],-1.));
    }
    con[k[i]] = cons;
    cons.linear.clear();
  };
};

CoonsPatchLagrangeConstraintElement::CoonsPatchLagrangeConstraintElement(FunctionSpaceBase* lagspace, FunctionSpaceBase* multspace,
                                                        int n, MVertex* v1, MVertex* vref,
                                                         std::vector<MVertex*>& xlist,
                                                         std::vector<MVertex*>& ylist):
                                                         _periodicSpace(lagspace),_multSpace(multspace),
                                                         _reference(vref),
                                                          _ndofs(n), _vp(v1),_vnx(xlist),_vny(ylist){
	int sizex = xlist.size();
	int sizey = ylist.size();
	for (int i=0; i<sizex; i++){
    double dist = distance(_vnx[i]->point(),_vny[0]->point(),_vny[sizey-1]->point());
    _distanceX.push_back(dist);
	};
	for (int i=0; i<sizey; i++){
    double dist = distance(_vny[i]->point(),_vnx[0]->point(),_vnx[sizex-1]->point());
    _distanceY.push_back(dist);
	};
	SPoint3 pp = _vp->point();
	MVertex * vtemp = NULL;
  if (_distanceY[sizey-1] > _distanceY[0])
    vtemp =_vny[sizey-1];
  else
    vtemp = _vny[0];
	SPoint3 point = project(pp,_vnx[0]->point(),_vnx[sizex-1]->point(),vtemp->point());


  _x = distance(point,_vny[0]->point(),_vny[sizey-1]->point());
  _y = distance(point,_vnx[0]->point(),_vnx[sizex-1]->point());

	_C.resize(_ndofs,(2+sizex+sizey)*_ndofs); _C.setAll(0.0);
	_Cbc.resize(_ndofs, (1+sizex+sizey)*_ndofs); _Cbc.setAll(0.);
	int col = 0;
	for (int i=0; i<_ndofs; i++){
		_C(i,col) = 1.0;
		col++;
	};
	lagrangeConstraintElement::getFF(_x,_distanceX,_FFx);
	lagrangeConstraintElement::getFF(_y,_distanceY,_FFy);

  SPoint3 ptref = _reference->point();
  for (int i=0; i<3; i++)
    _L[i] = pp[i] + ptref[i];


  for (int j=0; j<_FFx.size(); j++){
    SPoint3 pt = _vnx[j]->point();
    for (int i=0; i<3; i++){
      _L[i] -= _FFx(j)*pt[i];
    }
  }
  for (int j=0; j<_FFy.size(); j++){
    SPoint3 pt = _vny[j]->point();
    for (int i=0; i<3; i++){
      _L[i] -= _FFy(j)*pt[i];
    }
  }
	for (int i=0; i<_FFx.size(); i++){
		for (int j=0; j<_ndofs; j++){
				_C(j,col) = -1.*_FFx(i);
				_Cbc(j,col-_ndofs) = _FFx(i);
				col++;
		};
	};
	for (int i=0; i<_FFy.size(); i++){
		for (int j=0; j<_ndofs; j++){
				_C(j,col) = -1.*_FFy(i);
				_Cbc(j,col-_ndofs) = _FFy(i);
				col++;
		};
	};
	for (int i=0; i<_ndofs; i++){
		_C(i,col) = 1.0;
		_Cbc(i,col-_ndofs) = -1.;
		col++;
	};

	getMatrixFromVector(_L,_S);
};

void CoonsPatchLagrangeConstraintElement::getConstraintKeys(std::vector<Dof>& keys) const{
	getKeysFromVertex(_periodicSpace,_vp, keys);
	for (int i=0; i<_vnx.size(); i++){
		getKeysFromVertex(_periodicSpace,_vnx[i], keys);
	};
	for (int i=0; i<_vny.size(); i++){
		getKeysFromVertex(_periodicSpace,_vny[i], keys);
	};
	getKeysFromVertex(_periodicSpace,_reference, keys);
};

void CoonsPatchLagrangeConstraintElement::getMultiplierKeys(std::vector<Dof>& keys) const{
	getKeysFromVertex(_multSpace,_vp, keys);
};

void CoonsPatchLagrangeConstraintElement::getConstraintMatrix(fullMatrix<double>& m)const{
	m = _C;
};
void CoonsPatchLagrangeConstraintElement::getFirstOrderKinematicalMatrix(fullMatrix<double>& m)const{
	m = _S;
};
void CoonsPatchLagrangeConstraintElement::getFirstOrderKinematicalVector(const STensor3& FM, fullVector<double>& m) const{
	m.resize(_ndofs);
	m.setAll(0.);
	for (int i=0; i<_ndofs; i++){
	  for (int j=0; j<3; j++)
      m(i) += FM(i,j)*_L(j);
	};
};

void CoonsPatchLagrangeConstraintElement::print() const{
  Msg::Info("Patch Coons Lagrange constraint element 3D");
  Msg::Info("positive = %d",_vp->getNum());
  for (int i=0; i<_vnx.size(); i++)
    Msg::Info("negative x = %d ",_vnx[i]->getNum());
  for (int i=0; i<_vny.size(); i++)
    Msg::Info("negative y = %d ",_vny[i]->getNum());
};

void CoonsPatchLagrangeConstraintElement::getDependentKeys(std::vector<Dof>& keys) const{
  getKeysFromVertex(_periodicSpace,_vp,keys);
}; // left real dofs

void CoonsPatchLagrangeConstraintElement::getIndependentKeys(std::vector<Dof>& keys) const{
  for (int i=0; i<_vnx.size(); i++){
		getKeysFromVertex(_periodicSpace,_vnx[i], keys);
	};
	for (int i=0; i<_vny.size(); i++){
		getKeysFromVertex(_periodicSpace,_vny[i], keys);
	};
	getKeysFromVertex(_periodicSpace,_reference, keys);
}

void CoonsPatchLagrangeConstraintElement::getLinearConstraints(const STensor3& F,
                     std::map<Dof,DofAffineConstraint<double> >& con) const{
  std::vector<std::vector<Dof> > kx, ky;
  for (int i=0; i<_vnx.size(); i++){
    std::vector<Dof> ktemp;
    getKeysFromVertex(_periodicSpace,_vnx[i],ktemp);
    kx.push_back(ktemp);
  };
  for (int i=0; i<_vny.size(); i++){
    std::vector<Dof> ktemp;
    getKeysFromVertex(_periodicSpace,_vny[i],ktemp);
    ky.push_back(ktemp);
  };

  std::vector<Dof> kp, kref;
  getKeysFromVertex(_periodicSpace,_vp,kp);
  getKeysFromVertex(_periodicSpace,_reference,kref);

	fullVector<double> g;
  getFirstOrderKinematicalVector(F,g);

  DofAffineConstraint<double> cons;
  for (int i=0; i<_ndofs; i++){
    for (int j=0; j<_vnx.size(); j++){
      cons.linear.push_back(std::pair<Dof,double>(kx[j][i],_FFx(j)));
    };
    for (int j=0; j<_vny.size(); j++){
      cons.linear.push_back(std::pair<Dof,double>(ky[j][i],_FFy(j)));
    };
    cons.linear.push_back(std::pair<Dof,double>(kref[i],-1.));
    cons.shift=g(i);
    con[kp[i]] = cons;
    cons.linear.clear();
  };
};
