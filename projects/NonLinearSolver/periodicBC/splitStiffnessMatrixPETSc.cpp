
#include "splitStiffnessMatrixPETSc.h"

#if defined(HAVE_PETSC)

splitStiffnessMatrixPETSc::splitStiffnessMatrixPETSc(MPI_Comm com):
                      _isAllocated(false),_comm(com),_ni(0), _nb(0),_nc(0){};

splitStiffnessMatrixPETSc::~splitStiffnessMatrixPETSc(){
  this->clear();
};

bool splitStiffnessMatrixPETSc::isAllocated() const{
  return _isAllocated;
};
void splitStiffnessMatrixPETSc::clear(){
  if (isAllocated()){
  #if (PETSC_VERSION_RELEASE == 0 || ((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 2))) // petsc-dev
    if (_ni>0)
      _try(MatDestroy(&_aii));
    if (_ni>0 and _nb>0){
      _try(MatDestroy(&_aib));
      _try(MatDestroy(&_abi));
    }
    if (_nb>0)
      _try(MatDestroy(&_abb));
    if (_ni>0 and _nc>0){
      _try(MatDestroy(&_aic));
      _try(MatDestroy(&_aci));
    }
    if (_nb>0 and _nc>0){
      _try(MatDestroy(&_abc));
      _try(MatDestroy(&_acb));
    }
    if (_nc>0)
      _try(MatDestroy(&_acc));
  #else
    if (_ni>0)
      _try(MatDestroy(_aii));
    if (_ni>0 and _nb>0){
      _try(MatDestroy(_aib));
      _try(MatDestroy(_abi));
    }
    if (_nb>0)
      _try(MatDestroy(_abb));
    if (_ni>0 and _nc>0){
      _try(MatDestroy(_aic));
      _try(MatDestroy(_aci));
    }
    if (_nb>0 and _nc>0){
      _try(MatDestroy(_abc));
      _try(MatDestroy(_acb));
    }
    if (_nc>0)
      _try(MatDestroy(_acc));
  #endif
    _isAllocated = false;
  };
};

void splitStiffnessMatrixPETSc::allocateSubMatrix(int ni, int nb, int nc){
  _ni = ni;
  _nb = nb;
  _nc = nc;
  this->clear();
  _isAllocated = true;
  if (_ni>0){
    _try(MatCreate(_comm,&_aii));
    _try(MatSetSizes(_aii,ni,ni,PETSC_DETERMINE,PETSC_DETERMINE));
    _try(MatSetFromOptions(_aii));
    #if (((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)))
    _try(MatSetUp(_aii));
    #endif // (PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)
  }
  if (_ni>0 and _nb>0){
    _try(MatCreate(_comm,&_aib));
    _try(MatSetSizes(_aib,ni,nb,PETSC_DETERMINE,PETSC_DETERMINE));
    _try(MatSetType(_aib,MATDENSE)); // for condensation, the matdense must be used for MatMatSolve
    _try(MatSetFromOptions(_aib));
    #if (((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)))
    _try(MatSetUp(_aib));
    #endif // (PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)

    _try(MatCreate(_comm,&_abi));
    _try(MatSetSizes(_abi,nb,ni,PETSC_DETERMINE,PETSC_DETERMINE));
    _try(MatSetType(_abi,MATDENSE));
    _try(MatSetFromOptions(_abi));
    #if (((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)))
    _try(MatSetUp(_abi));
    #endif // (PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)
  }
  if (_nb>0){
    _try(MatCreate(_comm,&_abb));
    _try(MatSetSizes(_abb,nb,nb,PETSC_DETERMINE,PETSC_DETERMINE));
    _try(MatSetType(_abb,MATDENSE));
    _try(MatSetFromOptions(_abb));
    #if (((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)))
    _try(MatSetUp(_abb));
    #endif // (PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)
  }
  if (_ni>0 and _nc>0){
    _try(MatCreate(_comm,&_aic));
    _try(MatSetSizes(_aic,ni,nc,PETSC_DETERMINE,PETSC_DETERMINE));
    _try(MatSetType(_aic,MATDENSE));
    _try(MatSetFromOptions(_aic));
    #if (((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)))
    _try(MatSetUp(_aic));
    #endif // (PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)

    _try(MatCreate(_comm,&_aci));
    _try(MatSetSizes(_aci,nc,ni,PETSC_DETERMINE,PETSC_DETERMINE));
    _try(MatSetType(_aci,MATDENSE));
    _try(MatSetFromOptions(_aci));
    #if (((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)))
    _try(MatSetUp(_aci));
    #endif // (PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)
  }

  if (_nb>0 and _nc>0){
    _try(MatCreate(_comm,&_abc));
    _try(MatSetSizes(_abc,nb,nc,PETSC_DETERMINE,PETSC_DETERMINE));
    _try(MatSetType(_abc,MATDENSE));
    _try(MatSetFromOptions(_abc));
    #if (((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)))
    _try(MatSetUp(_abc));
    #endif // (PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)

    _try(MatCreate(_comm,&_acb));
    _try(MatSetSizes(_acb,nc,nb,PETSC_DETERMINE,PETSC_DETERMINE));
    _try(MatSetType(_acb,MATDENSE));
    _try(MatSetFromOptions(_acb));
    #if (((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)))
    _try(MatSetUp(_acb));
    #endif // (PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)
  }
  if (_nc>0){
    _try(MatCreate(_comm,&_acc));
    _try(MatSetSizes(_acc,nc,nc,PETSC_DETERMINE,PETSC_DETERMINE));
    _try(MatSetType(_acc,MATDENSE));
    _try(MatSetFromOptions(_acc));
    #if (((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)))
    _try(MatSetUp(_acc));
    #endif // (PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)
  }
};
void splitStiffnessMatrixPETSc::zeroSubMatrix(){
  if (_isAllocated){
    if (_ni>0){
      _try(MatAssemblyBegin(_aii, MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(_aii, MAT_FINAL_ASSEMBLY));
      _try(MatZeroEntries(_aii));
    }
    if (_ni>0 and _nb>0){
      _try(MatAssemblyBegin(_aib, MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(_aib, MAT_FINAL_ASSEMBLY));
      _try(MatZeroEntries(_aib));

      _try(MatAssemblyBegin(_abi, MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(_abi, MAT_FINAL_ASSEMBLY));
      _try(MatZeroEntries(_abi));
    }
    if (_nb>0){
      _try(MatAssemblyBegin(_abb, MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(_abb, MAT_FINAL_ASSEMBLY));
      _try(MatZeroEntries(_abb));
    }

    if (_ni>0 and _nc>0){
      _try(MatAssemblyBegin(_aic, MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(_aic, MAT_FINAL_ASSEMBLY));
      _try(MatZeroEntries(_aic));

      _try(MatAssemblyBegin(_aci, MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(_aci, MAT_FINAL_ASSEMBLY));
      _try(MatZeroEntries(_aci));
    }

    if (_nb>0 and _nc>0){
      _try(MatAssemblyBegin(_abc, MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(_abc, MAT_FINAL_ASSEMBLY));
      _try(MatZeroEntries(_abc));

      _try(MatAssemblyBegin(_acb, MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(_acb, MAT_FINAL_ASSEMBLY));
      _try(MatZeroEntries(_acb));
    }
    if (_nc>0){
      _try(MatAssemblyBegin(_acc, MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(_acc, MAT_FINAL_ASSEMBLY));
      _try(MatZeroEntries(_acc));
    }
  };
};

void splitStiffnessMatrixPETSc::addToSubMatrix_II(int row, int col, const double &val){
  PetscInt ii = row, jj = col;
  _try(MatSetValues(_aii,1,&ii,1,&jj,&val,ADD_VALUES));
};

void splitStiffnessMatrixPETSc::addToSubMatrix_IB(int row, int col, const double& val){
  PetscInt ii = row, jj = col;
_try(MatSetValues(_aib,1,&ii,1,&jj,&val,ADD_VALUES));
};

void splitStiffnessMatrixPETSc::addToSubMatrix_BI(int row, int col, const double& val){
  PetscInt ii = row, jj = col;
  _try(MatSetValues(_abi,1,&ii,1,&jj,&val,ADD_VALUES));
};

void splitStiffnessMatrixPETSc::addToSubMatrix_BB(int row, int col, const double& val){
  PetscInt ii = row, jj = col;
  _try(MatSetValues(_abb,1,&ii,1,&jj,&val,ADD_VALUES));
};

void splitStiffnessMatrixPETSc::addToSubMatrix_IC(int row, int col, const double& val){
  PetscInt ii = row, jj = col;
  _try(MatSetValues(_aic,1,&ii,1,&jj,&val,ADD_VALUES));
};

void splitStiffnessMatrixPETSc::addToSubMatrix_BC(int row, int col, const double& val){
  PetscInt ii = row, jj = col;
  _try(MatSetValues(_abc,1,&ii,1,&jj,&val,ADD_VALUES));
};

void splitStiffnessMatrixPETSc::addToSubMatrix_CI(int row, int col, const double& val){
  PetscInt ii = row, jj = col;
  _try(MatSetValues(_aci,1,&ii,1,&jj,&val,ADD_VALUES));
};

void splitStiffnessMatrixPETSc::addToSubMatrix_CB(int row, int col, const double& val){
  PetscInt ii = row, jj = col;
  _try(MatSetValues(_acb,1,&ii,1,&jj,&val,ADD_VALUES));
};

void splitStiffnessMatrixPETSc::addToSubMatrix_CC(int row, int col, const double& val){
  PetscInt ii = row, jj = col;
  _try(MatSetValues(_acc,1,&ii,1,&jj,&val,ADD_VALUES));
};

Mat& splitStiffnessMatrixPETSc::getIIMat(){
  if (_ni>0){
    _try(MatAssemblyBegin(_aii,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(_aii,MAT_FINAL_ASSEMBLY));
  }
  return _aii;
};
Mat& splitStiffnessMatrixPETSc::getIBMat(){
  if (_ni>0 and _nb>0){
    _try(MatAssemblyBegin(_aib,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(_aib,MAT_FINAL_ASSEMBLY));
  }
  return _aib;
};
Mat& splitStiffnessMatrixPETSc::getBIMat(){
  if (_ni>0 and _nb>0){
    _try(MatAssemblyBegin(_abi,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(_abi,MAT_FINAL_ASSEMBLY));
  }
  return _abi;
};
Mat& splitStiffnessMatrixPETSc::getBBMat(){
  if (_nb>0){
    _try(MatAssemblyBegin(_abb,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(_abb,MAT_FINAL_ASSEMBLY));
  }
  return _abb;
};

Mat& splitStiffnessMatrixPETSc::getICMat(){
  if (_nc>0 and _ni>0){
    _try(MatAssemblyBegin(_aic,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(_aic,MAT_FINAL_ASSEMBLY));
  }
  return _aic;
};

Mat& splitStiffnessMatrixPETSc::getBCMat(){
  if (_nb>0 and _nc>0){
    _try(MatAssemblyBegin(_abc,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(_abc,MAT_FINAL_ASSEMBLY));
  }
  return _abc;
};
Mat& splitStiffnessMatrixPETSc::getCIMat(){
  if (_ni>0 and _nc>0){
    _try(MatAssemblyBegin(_aci,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(_aci,MAT_FINAL_ASSEMBLY));
  }
  return _aci;
};
Mat& splitStiffnessMatrixPETSc::getCBMat(){
  if (_nb>0 and _nc>0){
    _try(MatAssemblyBegin(_acb,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(_acb,MAT_FINAL_ASSEMBLY));
  }
  return _acb;
};
Mat& splitStiffnessMatrixPETSc::getCCMat(){
  if (_nc>0){
    _try(MatAssemblyBegin(_acc,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(_acc,MAT_FINAL_ASSEMBLY));
  }
  return _acc;
};

#endif // HAVE_PETSC
