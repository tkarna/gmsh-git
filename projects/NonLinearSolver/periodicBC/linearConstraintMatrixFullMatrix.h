#ifndef LINEARCONSTRAINTMATRIXFULLMATRIX_H_
#define LINEARCONSTRAINTMATRIXFULLMATRIX_H_

#include "linearConstraintMatrix.h"
#include "fullMatrix.h"

// full matrix interface
class linearConstraintMatrixFullMatrix : public linearConstraintMatrix{
	protected:
		fullMatrix<double>* _firstMatrix, *_secondMatrix, *_firstOrderMatrix;
		bool _isAllocated;
		int _row, _firstCol, _secondCol;

	public:
		linearConstraintMatrixFullMatrix();
		virtual ~linearConstraintMatrixFullMatrix();
		virtual bool isAllocated() const ;
		virtual void clear();
		virtual void allocate(int row, int first =0, int second= 0);
		virtual void addToFirstMatrix(int i, int j, double val);
		virtual void addToSecondMatrix(int i, int j, double val);
		virtual void addToFirstOrderKinematicalMatrix(int i, int j, double val);
		fullMatrix<double>* getFirstMatrix();
		fullMatrix<double>* getSecondMatrix();
		fullMatrix<double>* getFirstOrderKinematicalMatrix();
};

class linearConstraintMatrixSecondOrderFullMatrix : public linearConstraintMatrixFullMatrix,
                                               public linearConstraintMatrixSecondOrder{
  protected:
    fullMatrix<double>* _secondOrderMatrix;

  public:
    linearConstraintMatrixSecondOrderFullMatrix();
    virtual ~linearConstraintMatrixSecondOrderFullMatrix();
    virtual void clear();
		virtual void allocate(int row, int first = 0, int second = 0);
		virtual void addToSecondOrderKinematicalMatrix(int i, int j, double val) ;
    fullMatrix<double>* getSecondOrderKinematicalMatrix();
};

#endif // LINEARCONSTRAINTMATRIXFULLMATRIX_H_
