#ifndef PERIODICCONSTRAINTS_H_
#define PERIODICCONSTRAINTS_H_

#include "groupOfElements.h"
#include "functionSpace.h"
#include "MPoint.h"
#include "numericalFunctions.h"

class periodicConstraintsBase{
  public:
    typedef std::set<MVertex*> vertexContainer;
    typedef std::set<MElement*> elementContainer;

  protected:
    std::vector<vertexContainer> gVertex; // entity for imposing PBC
    std::vector<elementContainer> gElement;
    FunctionSpaceBase* space; // to creat Dofs
    dofManager<double>* p;    // to manage Dofs
    fullMatrix<double> imposedDef; // imposed deformation
    vertexContainer coinVertex;  //coin vertex ->particularly treatment
    int _tag, _dim;

  protected:
    void getKeysFromVertex(MVertex* ver,std::vector<Dof> &keys);
    MVertex* findNearestVertex(MVertex* vP, vertexContainer container);
    void periodicConditionBase(MVertex* vP, MVertex* vN);
    void nodeConstraint(MVertex* v);
    void cornerConstraint();

  public:
    periodicConstraintsBase(){};
    periodicConstraintsBase(FunctionSpaceBase* _sp, dofManager<double>* _p): space(_sp),p(_p){};
    periodicConstraintsBase(const int tag, const int dim, dofManager<double> *_p);
    ~periodicConstraintsBase();

		void setDeformation(const STensor3& def){
			imposedDef.resize(3,3);
			for (int i=0; i<3; i++){
				for (int j=0; j<3; j++){
					imposedDef(i,j) = def(i,j);
				};
			};
		};
		void setDeformation(fullMatrix<double> def){
			imposedDef.resize(3,3);
			for (int i=0; i<3; i++){
				for (int j=0; j<3; j++){
					imposedDef(i,j) = def(i,j);
				};
			};
		};
    virtual void setPeriodicGroup(std::vector<groupOfElements*> group,int dim);
    void groupIntersection(vertexContainer g1, vertexContainer g2, vertexContainer &g);
    virtual void applyPeriodicCondition()=0;
};
#endif // PERIODICCONSTRAINTS_H_
