#ifndef EXTRACTMACROPROPERTIESLDBCPETSC_H_
#define EXTRACTMACROPROPERTIESLDBCPETSC_H_

#include "extractMacroPropertiesPETSc.h"
#include "functionPETSc.h"

#if defined(HAVE_PETSC)
#include "petscksp.h"
#include "petscmat.h"
#endif

class stiffnessCondensationLDBCPETSc : public stiffnessCondensation{
  #if defined(HAVE_PETSC)
  protected:
    splitStiffnessMatrixPETSc* _stiffness; // split stiffness matrix
		dofManager<double>* _pAssembler; // dof mananger
		linearConstraintMatrixPETSc* _bConMat; // all linear constraint matrix
		pbcAlgorithm* _pAl; // pbc algorithm
		bool _tangentflag,_stressflag; // stress and tangent flag
		Mat _ST;


    MPI_Comm _comm; // Comunicator MPI
    MatFactorInfo info;
    IS perm,  iperm;


  public:
    stiffnessCondensationLDBCPETSc(dofManager<double>* p, pbcAlgorithm* ma,
                               bool sflag, bool tflag,MPI_Comm com = PETSC_COMM_SELF);
		virtual ~stiffnessCondensationLDBCPETSc();

		virtual void init();
		virtual void clear();
		virtual void stressSolve();
		virtual void tangentCondensationSolve();

  #else
  public:
    stiffnessCondensationLDBCPETSc(dofManager<double>* p, pbcAlgorithm* ma,
                               bool sflag, bool tflag){
      Msg::Error("Petsc is required");
    };
    virtual ~stiffnessCondensationLDBCPETSc(){};

    virtual void init(){};
		virtual void clear(){};
		virtual void stressSolve(){};
		virtual void tangentCondensationSolve(){};

  #endif // HAVE_PETSC
};

class stiffnessCondensationSecondOrderLDBCPETSc : public stiffnessCondensationSecondOrder{
  #if defined(HAVE_PETSC)
  protected:
    splitStiffnessMatrixPETSc* _stiffness; // split stiffness matrix
		dofManager<double>* _pAssembler; // dof mananger
		linearConstraintMatrixSecondOrderPETSc* _bConMat; // all linear constraint matrix
		pbcAlgorithm* _pAl; // pbc algorithm
		bool _tangentflag,_stressflag; // stress and tangent flag
		Mat _ST, _TT;

    MPI_Comm _comm; // Comunicator MPI
    MatFactorInfo info;
    IS perm,  iperm;


  public:
    stiffnessCondensationSecondOrderLDBCPETSc(dofManager<double>* p, pbcAlgorithm* ma,
                               bool sflag, bool tflag,MPI_Comm com = PETSC_COMM_SELF);
		virtual ~stiffnessCondensationSecondOrderLDBCPETSc();

		virtual void init();
		virtual void clear();
		virtual void stressSolve();
		virtual void tangentCondensationSolve();

  #else
  public:
    stiffnessCondensationSecondOrderLDBCPETSc(dofManager<double>* p, pbcAlgorithm* ma,
                               bool sflag, bool tflag){
      Msg::Error("Petsc is required");
    };
		virtual ~stiffnessCondensationSecondOrderLDBCPETSc(){};

		virtual void init(){};
		virtual void clear(){};
		virtual void stressSolve(){};
		virtual void tangentCondensationSolve(){};

  #endif // HAVE_PETSC
};

#endif // EXTRACTMACROPROPERTIESLDBCPETSC_H_
