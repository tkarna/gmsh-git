
#include "eigenModeView.h"
#include "partDomain.h"
#include "staticDofManager.h"

eigenVectorField::eigenVectorField(dofManager<double> *pas, std::vector<partDomain*> &elas, std::vector<partDomain*> &gdom,
                      std::set<contactDomain*> *acontact,const int nc, std::vector<unknownField::archiveNode> &archiving,
                      const std::vector<nlsField::dataBuildView> &dbview_,
                      eigenSolver* eig, int num, const std::string file) : unknownField(pas,elas,gdom,acontact,nc,archiving,dbview_,file+int2str(num)+".msh"), _eigenSolver(eig), _num(num){}

void eigenVectorField::get(Dof &D,double &udof) const{
	int comp = pAssembler->getDofNumber(D);
	if (comp>=0){
		std::complex<double> val = _eigenSolver->getEigenVectorComp(_num,comp);
		udof = val.real();
	}
	else{
	  std::map<Dof, DofAffineConstraint<double> >& constraints = pAssembler->getAllLinearConstraints();
	  std::map<Dof, DofAffineConstraint<double> >::iterator it =constraints.find(D);
      if (it != constraints.end()){
        udof =  0.; //it->second.shift;
        for (unsigned i = 0; i < (it->second).linear.size(); i++){
          double tmp = 0;
          this->get(((it->second).linear[i]).first, tmp);
          udof += ((it->second).linear[i]).second * tmp;
        }
      }
	};
};

void eigenVectorField::get(std::vector<Dof> &R, std::vector<double> &disp) const{
	double du;
	for (int i=0; i<R.size(); i++){
		this->get(R[i],du);
		disp.push_back(du);
	};
};

void eigenVectorField::get(std::vector<Dof> &R, fullVector<double> &disp) const{
	double du;
	for (int i=0; i<R.size(); i++){
		this->get(R[i],du);
		disp(i) = du;
	};
};

void eigenVectorField::get(partDomain *dom, MElement *ele,std::vector<double> &udofs, const int cc,
                             const nlsField::ElemValue ev)const{
  if(ev == nlsField::crude){
    std::vector<Dof> R;
    FunctionSpaceBase *sp = dom->getFunctionSpace();
    sp->getKeys(ele,R);
    this->get(R,udofs);
  };
};

void eigenVectorField::archive(const double time,const int step) {
	this->buildAllView(*_vdom,time,step);
  if(!_forceView){
    double u;
    for(std::vector<archiveNode>::const_iterator it = varch.begin(); it!=varch.end();++it){
      Dof D = it->D;
      this->get(D,u);
      fprintf(it->fp,"%e;%e\n",time,u);
    };
  }
  else
  {
		_forceView = false; // the command forceView has to be used at each step you want to force archiving
  };
};

void eigenVectorField::setInitial(const std::vector<Dof> &R,const std::vector<double> &disp){
	staticDofManager<double>* staticAssembler = dynamic_cast<staticDofManager<double>*>(pAssembler);
	if (staticAssembler)
		staticAssembler->setInitial(R,disp);
};


