#ifndef LINEARCOMBINATIONMETHOD_H_
#define LINEARCOMBINATIONMETHOD_H_

#include "periodicConstraints.h"

class linearCombination : public periodicConstraintsBase{

  protected:
    virtual void findReceivedElement(MVertex* vP, elementContainer container,MElement* &ele);
    void periodicCondition(MVertex* v, MElement* e);

  public:
    linearCombination(FunctionSpaceBase* sp, dofManager<double>* _p):
                      periodicConstraintsBase(sp,_p){};
    linearCombination(const int tag, const int dim, dofManager<double>* _p)
                      : periodicConstraintsBase(tag,dim,_p){};
    virtual ~linearCombination(){};

    virtual void applyPeriodicCondition();
};

class linearCombination3D : public linearCombination{

  protected:
    virtual void findReceivedElement(MVertex* vP, elementContainer container, MElement* &ele);

  public:
    linearCombination3D(const int tag, const int dim, dofManager<double>* _p)
                        :linearCombination(tag,dim,_p){};
    virtual ~linearCombination3D(){};

};

#endif // LINEARCOMBINATIONMETHOD_H_
