#include "pbcDofManager.h"
#include "splitStiffnessMatrixPETSc.h"
#include "splitStiffnessMatrixFullMatrix.h"

pbcDofManager::pbcDofManager():_splittedStiffness(NULL) {}

pbcDofManager::~pbcDofManager(){
  if (_splittedStiffness) delete _splittedStiffness;
  _splittedStiffness = NULL;
  clear();
};

void pbcDofManager::allocateSplittedMatrix(int n1, int n2, int n3){
  if (_splittedStiffness == NULL)
    this->createSplitStiffnessMatrix();

  if (_splittedStiffness->isAllocated() == false)
    _splittedStiffness->allocateSubMatrix(n1,n2,n3);
}

void pbcDofManager::allocateSplittedMatrix(){
  if (_splittedStiffness == NULL)
    this->createSplitStiffnessMatrix();
  if (_splittedStiffness->isAllocated() == false)
    _splittedStiffness->allocateSubMatrix(sizeOfInternalDof(),sizeOfDependentDof(),sizeOfIndependentDof());
};

void pbcDofManager::createSplitStiffnessMatrix(){
  #if defined(HAVE_PETSC)
  _splittedStiffness = new splitStiffnessMatrixPETSc(PETSC_COMM_SELF);
  #else
  _splittedStiffness = new splitStiffnessMatrixFullMatrix();
  #endif // HAVE_PETSC
};

splitStiffnessMatrix* pbcDofManager::getSplitStiffnessMatrix(){
  return _splittedStiffness;
}

void pbcDofManager::clear(){
  _internalDof.clear();
  _boundaryDof.clear();
  _dependentDof.clear();
  _independentDof.clear();
  _realBoundaryDof.clear();
  _realDependentDof.clear();
  _realIndependentDof.clear();
  _virtualDof.clear();
  _dependentVirtualDof.clear();
  _independentVirtualDof.clear();
  _multiplierDof.clear();
  _dependentDof.clear();
  _independentMultiplierDof.clear();
};

void pbcDofManager::assembleSubMatrix(const Dof& R, const Dof& C, const double& val){
  if (val != 0.0){
    std::map<Dof,int>::iterator itRi = _internalDof.find(R);
    if (itRi != _internalDof.end()){
      int row = itRi->second;
      std::map<Dof,int>::iterator itCi = _internalDof.find(C);
      std::map<Dof,int>::iterator itCb = _dependentDof.find(C);
      std::map<Dof,int>::iterator itCc = _independentDof.find(C);
      if (itCi != _internalDof.end()){
        int col = itCi->second;
        _splittedStiffness->addToSubMatrix_II(row,col,val);
        return;
      }
      else if (itCb != _dependentDof.end()){
        int col = itCb->second;
        _splittedStiffness->addToSubMatrix_IB(row,col,val);
        return;
      }
      else if (itCc != _independentDof.end()){
        int col = itCc->second;
        _splittedStiffness->addToSubMatrix_IC(row,col,val);
        return;
      }
      else{
        Msg::Error("error in dof separation");
      };
    }
    else {
      std::map<Dof,int>::iterator itRb = _dependentDof.find(R);
      if (itRb != _dependentDof.end()){
        int row = itRb->second;
        std::map<Dof,int>::iterator itCi = _internalDof.find(C);
        std::map<Dof,int>::iterator itCb = _dependentDof.find(C);
        std::map<Dof,int>::iterator itCc = _independentDof.find(C);
        if (itCi != _internalDof.end()){
          int col = itCi->second;
          _splittedStiffness->addToSubMatrix_BI(row,col,val);
          return;
        }
        else if (itCb != _dependentDof.end()){
          int col = itCb->second;
          _splittedStiffness->addToSubMatrix_BB(row,col,val);
          return;
        }
        else if (itCc != _independentDof.end()){
          int col = itCc->second;
          _splittedStiffness->addToSubMatrix_BC(row,col,val);
          return;
        }
        else{
          Msg::Error("error in dof separation");
        };
      }
      else {
        std::map<Dof,int>::iterator itRc = _independentDof.find(R);
        if (itRc != _independentDof.end()){
          int row = itRc->second;
          std::map<Dof,int>::iterator itCi = _internalDof.find(C);
          std::map<Dof,int>::iterator itCb = _dependentDof.find(C);
          std::map<Dof,int>::iterator itCc = _independentDof.find(C);
          if (itCi != _internalDof.end()){
            int col = itCi->second;
            _splittedStiffness->addToSubMatrix_CI(row,col,val);
            return;
          }
          else if (itCb != _dependentDof.end()){
            int col = itCb->second;
            _splittedStiffness->addToSubMatrix_CB(row,col,val);
            return;
          }
          else if (itCc != _independentDof.end()){
            int col = itCc->second;
            _splittedStiffness->addToSubMatrix_CC(row,col,val);
            return;
          }
          else{
            Msg::Error("error in dof separation in colume");
          };
        }
        else {
          Msg::Error("error in dof separation in row");
        };
      };
    };
  };
};

