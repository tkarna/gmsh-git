
#include "extractMacroPropertiesLDBCPETSc.h"
#include "nonLinearSystems.h"
#include "pbcSystems.h"
#include "highOrderTensor.h"

#if defined(HAVE_PETSC)

stiffnessCondensationLDBCPETSc::stiffnessCondensationLDBCPETSc(dofManager<double>* p, pbcAlgorithm* ma,
                           bool sflag, bool tflag,MPI_Comm com)
                          :_pAssembler(p),_pAl(ma),_bConMat(NULL),
                          _stressflag(sflag), _tangentflag(tflag){
  _stiffness = static_cast<splitStiffnessMatrixPETSc*>(_pAl->getSplitStiffness());
  _comm = com;
};
stiffnessCondensationLDBCPETSc::~stiffnessCondensationLDBCPETSc(){
  if (this->isInitialized()){
    this->clear();
  }
};

void stiffnessCondensationLDBCPETSc::init(){
  if (this->isInitialized()){
    this->clear();
  }
  this->isInitialized() = true;
  pbcDofManager* pbcAssembler = _pAl->getSplittedDof();
  _bConMat = new linearConstraintMatrixPETSc(_comm);
  int nb = pbcAssembler->sizeOfBoundaryDof();
	_bConMat->allocate(nb);
	_pAl->computeLinearConstraintMatrixLDBC(_bConMat);
	Mat& S = _bConMat->getFirstOrderKinematicalMatrix();
  _try(MatTranspose(S,MAT_INITIAL_MATRIX,&_ST));
  _try(MatAssemblyBegin(_ST,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(_ST,MAT_FINAL_ASSEMBLY));
};

void stiffnessCondensationLDBCPETSc::clear(){
  if (this->isInitialized()){
    this->isInitialized() = false;
    _try(MatDestroy(&_ST));
    if (_bConMat) delete _bConMat;
    _bConMat = NULL;
  }
};

void stiffnessCondensationLDBCPETSc::stressSolve(){
  if (_stressflag){
    if (this->isInitialized() == false){
      this->init();
    };
    pbcDofManager* pbcAssembler = _pAl->getSplittedDof();
    int sizeb = pbcAssembler->sizeOfBoundaryDof();
    Vec lambda;
    _try(VecCreate(_comm,&lambda));
    _try(VecSetSizes(lambda,sizeb,PETSC_DECIDE));
    _try(VecSetFromOptions(lambda));
    std::string str = "A";
    linearSystem<double>* lsys = _pAssembler->getLinearSystem(str);
    nonLinearSystem<double>* linearsys = dynamic_cast<nonLinearSystem<double>*>(lsys);
    pbcSystem<double>* pbcsys = dynamic_cast<pbcSystem<double>*>(lsys);
    if (pbcsys){
      for (std::map<Dof,int>::const_iterator it = pbcAssembler->boundaryDofBegin();
                                             it!= pbcAssembler->boundaryDofEnd(); it++){
        double val = 0.;
        int num = _pAssembler->getDofNumber(it->first);
        if (num != -1)
          linearsys->getFromRightHandSideMinus(num, val);
        else
          Msg::Error("The pbcNonLinearSystem has to used");
        PetscInt row = it->second;
        _try(VecSetValues(lambda,1,&row,&val,INSERT_VALUES));
      }
    }
    else{
      Msg::Fatal("pbcNonLinearSystem must be used");
    }
    _try(VecAssemblyBegin(lambda));
    _try(VecAssemblyEnd(lambda));

    Vec P; // stress
    _try(VecCreate(_comm,&P));
    _try(VecSetSizes(P,9,PETSC_DECIDE));
    _try(VecSetFromOptions(P));

    _try(MatMult(_ST,lambda,P));
    _try(VecAssemblyBegin(P));
    _try(VecAssemblyEnd(P));
    #ifdef _DEBUG
    _try(VecView(P,PETSC_VIEWER_STDOUT_SELF));
    #endif

    STensor3& stress = this->getRefToMacroStress();
    PetscScalar* val;
    _try(VecGetArray(P,&val));
    double& rvevolume = this->getRVEVolume();
    for (int row = 0; row<9; row++){
      int i,j;
      Tensor23::getIntsFromIndex(row,i,j);
      stress(i,j) =val[row]/rvevolume;
    };
    _try(VecDestroy(&lambda));
    _try(VecDestroy(&P));
  }
};

void stiffnessCondensationLDBCPETSc::tangentCondensationSolve(){
  if (_tangentflag){
    if (this->isInitialized() ==false){
      this->init();
    }
    Mat& Kii = _stiffness->getIIMat();
    Mat& Kib = _stiffness->getIBMat();
    Mat& Kbi = _stiffness->getBIMat();
    Mat& Kbb = _stiffness->getBBMat();

    Mat Kbbt;

    // for factorize information
    pbcDofManager* pbcAssembler = static_cast<pbcDofManager*>(_pAl->getSplittedDof());
    int ni = pbcAssembler->sizeOfInternalDof();
    int nb = pbcAssembler->sizeOfBoundaryDof();
    if (ni>0){
      Mat matfac;
      _try(MatDuplicate(Kii,MAT_COPY_VALUES,&matfac));
      _try(MatAssemblyBegin(matfac,MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(matfac,MAT_FINAL_ASSEMBLY));

      _try(MatGetOrdering(matfac,  "natural",  &perm,  &iperm));
      _try(MatFactorInfoInitialize(&info));
      _try(MatLUFactor(matfac, perm, iperm, &info));
      if (nb>0)
        _try(GetSchurComplementMat(matfac,Kib,Kbi,Kbb,&Kbbt));
      _try(MatDestroy(&matfac));
    }
    else{
      if (nb>0)
        _try(MatDuplicate(Kbb,MAT_COPY_VALUES,&Kbbt));
    };
    Mat& S = _bConMat->getFirstOrderKinematicalMatrix();
    Mat L, KbbtS;
    _try(MatMatMult(Kbbt,S,MAT_INITIAL_MATRIX,1,&KbbtS));
    _try(MatMatMult(_ST,KbbtS,MAT_INITIAL_MATRIX,1.,&L));
    _try(MatAssemblyBegin(L,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(L,MAT_FINAL_ASSEMBLY));

    double& rvevolume = this->getRVEVolume();
    if (rvevolume >0){
      PetscInt M,N;
      _try(MatGetSize(L,&M,&N));
      STensor43& tangent = this->getRefToMacroTangent();
      for (int row = 0; row< M; ++row){
        int i,j;
        Tensor23::getIntsFromIndex(row,i,j);
        for (int col = 0; col<N; ++col){
          PetscScalar val;
          PetscInt ii = row, jj = col;
          _try(MatGetValues(L,1,&ii,1,&jj,&val));
          int k,l;
          Tensor23::getIntsFromIndex(col,k,l);
          tangent(i,j,k,l) = val/rvevolume;
        };
      };

      _try(MatDestroy(&L));
      _try(MatDestroy(&KbbtS));

    }
    else{
      printf("Error in rve volume /n");
    };

  }
};


stiffnessCondensationSecondOrderLDBCPETSc::stiffnessCondensationSecondOrderLDBCPETSc(dofManager<double>* p, pbcAlgorithm* ma,
                           bool sflag, bool tflag,MPI_Comm com)
                          :stiffnessCondensationSecondOrder(),_pAssembler(p),_pAl(ma),_bConMat(NULL),
                          _stressflag(sflag), _tangentflag(tflag){
  _stiffness = static_cast<splitStiffnessMatrixPETSc*>(_pAl->getSplitStiffness());
  _comm = com;
};
stiffnessCondensationSecondOrderLDBCPETSc::~stiffnessCondensationSecondOrderLDBCPETSc(){
  this->clear();
}

void stiffnessCondensationSecondOrderLDBCPETSc::init(){
  if (this->isInitialized()){
    this->clear();
  }
  this->isInitialized() = true;
  pbcDofManager* pbcAssembler = _pAl->getSplittedDof();
	// for constraint form C_b u_b + C_c u_c = S_b (FM - I)
  _bConMat = new linearConstraintMatrixSecondOrderPETSc(_comm);
  // size of boundary constraints without corner constraints
  int nb = pbcAssembler->sizeOfBoundaryDof();
  // size of corner dofs
	_bConMat->allocate(nb);
	_pAl->computeLinearConstraintMatrixLDBC(_bConMat);

	Mat& S = _bConMat->getFirstOrderKinematicalMatrix();
  _try(MatTranspose(S,MAT_INITIAL_MATRIX,&_ST));
  _try(MatAssemblyBegin(_ST,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(_ST,MAT_FINAL_ASSEMBLY));

  Mat& T = _bConMat->getSecondOrderKinematicalMatrix();
  _try(MatTranspose(T,MAT_INITIAL_MATRIX,&_TT));
  _try(MatAssemblyBegin(_TT,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(_TT,MAT_FINAL_ASSEMBLY));
};

void stiffnessCondensationSecondOrderLDBCPETSc::clear(){
  if (this->isInitialized()){
    this->isInitialized() = false;
    _try(MatDestroy(&_ST));
    _try(MatDestroy(&_TT));
    if (_bConMat) delete _bConMat;
    _bConMat = NULL;
  }
};

void stiffnessCondensationSecondOrderLDBCPETSc::stressSolve(){
  if (_stressflag){
    if (this->isInitialized() == false){
      this->init();
    }
    pbcDofManager* pbcAssembler = _pAl->getSplittedDof();
    int sizeb = pbcAssembler->sizeOfBoundaryDof();
    Vec lambda;
    _try(VecCreate(_comm,&lambda));
    _try(VecSetSizes(lambda,sizeb,PETSC_DECIDE));
    _try(VecSetFromOptions(lambda));
    std::string str = "A";
    linearSystem<double>* lsys = _pAssembler->getLinearSystem(str);
    nonLinearSystem<double>* linearsys = dynamic_cast<nonLinearSystem<double>*>(lsys);
    pbcSystem<double>* pbcsys = dynamic_cast<pbcSystem<double>*>(lsys);
    if (pbcsys){
      for (std::map<Dof,int>::const_iterator it = pbcAssembler->boundaryDofBegin();
                                             it!= pbcAssembler->boundaryDofEnd(); it++){
        double val = 0.;
        int num = _pAssembler->getDofNumber(it->first);
        if (num != -1)
          linearsys->getFromRightHandSideMinus(num, val);
        else
          Msg::Error("The pbcNonLinearSystem has to used");
        PetscInt row = it->second;
        _try(VecSetValues(lambda,1,&row,&val,INSERT_VALUES));
      }
    }
    else{
      Msg::Fatal("pbcNonLinearSystem must be used");
    }
    _try(VecAssemblyBegin(lambda));
    _try(VecAssemblyEnd(lambda));

    Vec P; // stress
    _try(VecCreate(_comm,&P));
    _try(VecSetSizes(P,9,PETSC_DECIDE));
    _try(VecSetFromOptions(P));

    _try(MatMult(_ST,lambda,P));
    _try(VecAssemblyBegin(P));
    _try(VecAssemblyEnd(P));
    #ifdef _DEBUG
    _try(VecView(P,PETSC_VIEWER_STDOUT_SELF));
    #endif

    Vec Q;
    _try(VecCreate(_comm,&Q));
    _try(VecSetSizes(Q,27,PETSC_DECIDE));
    _try(VecSetFromOptions(Q));

    _try(MatMult(_TT,lambda,Q));
    _try(VecAssemblyBegin(Q));
    _try(VecAssemblyEnd(Q));

    STensor3& stress = this->getRefToMacroStress();
    PetscScalar* valP;
    _try(VecGetArray(P,&valP));
    double& rvevolume = this->getRVEVolume();
    for (int row = 0; row<9; row++){
      int i,j;
      Tensor23::getIntsFromIndex(row,i,j);
      stress(i,j) =valP[row]/rvevolume;
    };

    PetscScalar* valQ;
    _try(VecGetArray(Q,&valQ));
    STensor33& highStress = this->getRefToMacroStressSecondOrder();
    for (int row = 0; row<27; row++){
      int i,j, k;
      Tensor33::getIntsFromIndex(row,i,j,k);
      highStress(i,j,k) =valQ[row]/rvevolume;
    };

    _try(VecDestroy(&lambda));
    _try(VecDestroy(&P));
  }
};

void stiffnessCondensationSecondOrderLDBCPETSc::tangentCondensationSolve(){
  if (_tangentflag){
    if (this->isInitialized() ==false){
      this->init();
    }
    Mat& Kii = _stiffness->getIIMat();
    Mat& Kib = _stiffness->getIBMat();
    Mat& Kbi = _stiffness->getBIMat();
    Mat& Kbb = _stiffness->getBBMat();

    Mat Kbbt;

    // for factorize information
    pbcDofManager* pbcAssembler = static_cast<pbcDofManager*>(_pAl->getSplittedDof());
    int ni = pbcAssembler->sizeOfInternalDof();
    int nb = pbcAssembler->sizeOfBoundaryDof();
    if (ni>0){
      Mat matfac;
      _try(MatDuplicate(Kii,MAT_COPY_VALUES,&matfac));
      _try(MatAssemblyBegin(matfac,MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(matfac,MAT_FINAL_ASSEMBLY));

      _try(MatGetOrdering(matfac,  "natural",  &perm,  &iperm));
      _try(MatFactorInfoInitialize(&info));
      _try(MatLUFactor(matfac, perm, iperm, &info));
      if (nb>0)
        _try(GetSchurComplementMat(matfac,Kib,Kbi,Kbb,&Kbbt));
      _try(MatDestroy(&matfac));
    }
    else{
      if (nb>0)
        _try(MatDuplicate(Kbb,MAT_COPY_VALUES,&Kbbt));
    };
    Mat& S = _bConMat->getFirstOrderKinematicalMatrix();
    Mat& T = _bConMat->getSecondOrderKinematicalMatrix();
    Mat KbbtS, KbbtT;
    _try(MatMatMult(Kbbt,S,MAT_INITIAL_MATRIX,1,&KbbtS));
    _try(MatMatMult(Kbbt,T,MAT_INITIAL_MATRIX,1,&KbbtT));

    Mat L;
    _try(MatMatMult(_ST,KbbtS,MAT_INITIAL_MATRIX,1.,&L));
    _try(MatAssemblyBegin(L,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(L,MAT_FINAL_ASSEMBLY));

    Mat LPG ;
    _try(MatMatMult(_ST,KbbtT,MAT_INITIAL_MATRIX,1.,&LPG));
    _try(MatAssemblyBegin(LPG,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(LPG,MAT_FINAL_ASSEMBLY));

    Mat LQF ;
    _try(MatMatMult(_TT,KbbtS,MAT_INITIAL_MATRIX,1.,&LQF));
    _try(MatAssemblyBegin(LQF,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(LQF,MAT_FINAL_ASSEMBLY));

    Mat LQG ;
    _try(MatMatMult(_TT,KbbtT,MAT_INITIAL_MATRIX,1.,&LQG));
    _try(MatAssemblyBegin(LQG,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(LQG,MAT_FINAL_ASSEMBLY));

    double& rvevolume = this->getRVEVolume();
    if (rvevolume >0){
      PetscInt M,N;
      _try(MatGetSize(L,&M,&N));
      STensor43& tangent = this->getRefToMacroTangent();
      for (int row = 0; row< M; ++row){
        int i,j;
        Tensor23::getIntsFromIndex(row,i,j);
        for (int col = 0; col<N; ++col){
          PetscScalar val;
          PetscInt ii = row, jj = col;
          _try(MatGetValues(L,1,&ii,1,&jj,&val));
          int k,l;
          Tensor23::getIntsFromIndex(col,k,l);
          tangent(i,j,k,l) = val/rvevolume;
        };
      };

      _try(MatGetSize(LPG,&M,&N));
      STensor53& tangent_PG= this->getRefToMacroTangentFirstSecond();
      for (int row = 0; row< M; row++){
        int i,j;
        Tensor23::getIntsFromIndex(row,i,j);
        for (int col = 0; col<N; col++){
          PetscScalar val;
          PetscInt ii = row, jj = col;
          _try(MatGetValues(LPG,1,&ii,1,&jj,&val));
          int k,l,m;
          Tensor33::getIntsFromIndex(col,k,l,m);
          tangent_PG(i,j,k,l,m) = val/rvevolume;
        };
      };

      _try(MatGetSize(LQF,&M,&N));
      STensor53& tangent_QF = this->getRefToMacroTangentSecondFirst();
      for (int row = 0; row< M; row++){
        int i,j,k;
        Tensor33::getIntsFromIndex(row,i,j,k);
        for (int col = 0; col<N; col++){
          PetscScalar val;
          PetscInt ii = row, jj = col;
          _try(MatGetValues(LQF,1,&ii,1,&jj,&val));
          int l,m;
          Tensor23::getIntsFromIndex(col,l,m);
          tangent_QF(i,j,k,l,m) = val/rvevolume;
        };
      };

      _try(MatGetSize(LQG,&M,&N));
      STensor63& tangent_QG = this->getRefToMacroTangentSecondSecond();
      for (int row = 0; row< M; row++){
        int i,j,k;
        Tensor33::getIntsFromIndex(row,i,j,k);
        for (int col = 0; col<N; col++){
          PetscScalar val;
          PetscInt ii = row, jj = col;
          _try(MatGetValues(LQG,1,&ii,1,&jj,&val));
          int l,m,n;
          Tensor33::getIntsFromIndex(col,l,m,n);
          tangent_QG(i,j,k,l,m,n) = val/rvevolume;
        };
      };

      _try(MatDestroy(&L));
      _try(MatDestroy(&KbbtS));
      _try(MatDestroy(&KbbtT));
      _try(MatDestroy(&LPG));
      _try(MatDestroy(&LQF));
      _try(MatDestroy(&LQG));
    }
    else{
      printf("Error in rve volume /n");
    };
  }
};



#endif // HAVE_PETSC
