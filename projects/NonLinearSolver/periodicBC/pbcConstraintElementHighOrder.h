#ifndef PBCCONSTRAINTELEMENTHIGHORDER_H_
#define PBCCONSTRAINTELEMENTHIGHORDER_H_

#include "pbcConstraintElement.h"
#include "pbcCoonsPatchConstraintElement.h"
#include "pbcSupplementConstraint.h"


class periodicMeshConstraintSecondOrder : public periodicMeshConstraint,
                                     public constraintElementSecondOrder{
	protected:
		STensor3 _XiXj;
		fullMatrix<double> _T;

	public:
		periodicMeshConstraintSecondOrder(FunctionSpaceBase* lagspace, FunctionSpaceBase* multspace,
                                      int n, MVertex* vp, MVertex* vn = NULL);
		virtual ~periodicMeshConstraintSecondOrder(){};
		virtual void getSecondOrderKinematicalMatrix(fullMatrix<double>& m) const;
		virtual void getSecondOrderKinematicalVector(const STensor33& G, fullVector<double>& m) const;
		virtual void getLinearConstraints(const STensor3& F, const STensor33& G,
                     std::map<Dof,DofAffineConstraint<double> >& con) const;
};


class cubicSplineConstraintElementSecondOrder : public cubicSplineConstraintElement,
                                           public constraintElementSecondOrder{
  protected:
    STensor3 _XiXj;
    fullMatrix<double> _T;

  public:
    cubicSplineConstraintElementSecondOrder(FunctionSpaceBase* lagspace, FunctionSpaceBase* multspace,
                                          int n, MVertex* v1, MVertex* v2, MVertex* v3, int flag =0);
    virtual ~cubicSplineConstraintElementSecondOrder();
    virtual void getSecondOrderKinematicalMatrix(fullMatrix<double>& m) const;
		virtual void getSecondOrderKinematicalVector(const STensor33& G, fullVector<double>& m)const;
		virtual void getLinearConstraints(const STensor3& F, const STensor33& G,
                     std::map<Dof,DofAffineConstraint<double> >& con) const;
};

class CoonsPatchCubicSplineConstraintElementSecondOrder : public CoonsPatchCubicSplineConstraintElement,
                                           public constraintElementSecondOrder{
  protected:
    STensor3 _XiXj;
    fullMatrix<double> _T;

  public:
    CoonsPatchCubicSplineConstraintElementSecondOrder(FunctionSpaceBase* lagspace, FunctionSpaceBase* multspace,
                                          int n, MVertex* vp, MVertex* vre, MVertex* vx1, MVertex* vx2,
                                          MVertex* vy1, MVertex* vy2, int flagx, int flagy);
    virtual ~CoonsPatchCubicSplineConstraintElementSecondOrder(){};
    virtual void getSecondOrderKinematicalMatrix(fullMatrix<double>& m) const;
		virtual void getSecondOrderKinematicalVector(const STensor33& G, fullVector<double>& m)const;
		virtual void getLinearConstraints(const STensor3& F, const STensor33& G,
                     std::map<Dof,DofAffineConstraint<double> >& con) const;
};



class lagrangeConstraintElementSecondOrder : public lagrangeConstraintElement,
                                        public constraintElementSecondOrder{
  protected:
    STensor3 _XiXj;
    fullMatrix<double> _T;

  public:
    lagrangeConstraintElementSecondOrder(FunctionSpaceBase* lagspace, FunctionSpaceBase* multspace,
                                        int n, MVertex* v1, std::vector<MVertex*>& vlist);
    virtual ~lagrangeConstraintElementSecondOrder();
    virtual void getSecondOrderKinematicalMatrix(fullMatrix<double>& m) const;
		virtual void getSecondOrderKinematicalVector(const STensor33& G, fullVector<double>& m)const;
		virtual void getLinearConstraints(const STensor3& F, const STensor33& G,
                     std::map<Dof,DofAffineConstraint<double> >& con) const;
};

class CoonsPatchLagrangeConstraintElementSecondOrder : public CoonsPatchLagrangeConstraintElement,
                                               public constraintElementSecondOrder{
  protected:
    STensor3 _XiXj;
    fullMatrix<double> _T;

  public:
    CoonsPatchLagrangeConstraintElementSecondOrder(FunctionSpaceBase* lagspace, FunctionSpaceBase* multspace,
                                int n, MVertex* v1, MVertex* vref,
                               std::vector<MVertex*>& xlist,
                               std::vector<MVertex*>& ylist);
    virtual ~CoonsPatchLagrangeConstraintElementSecondOrder(){};
    virtual void getSecondOrderKinematicalMatrix(fullMatrix<double>& m) const;
		virtual void getSecondOrderKinematicalVector(const STensor33& G, fullVector<double>& m)const;
		virtual void getLinearConstraints(const STensor3& F, const STensor33& G,
                     std::map<Dof,DofAffineConstraint<double> >& con) const;
};

class FEConstraintElementSecondOrder : public constraintElementSecondOrder,
                                        public FEConstraintElement{
  protected:
    STensor3 _XiXj;
    fullMatrix<double> _T;

  public:
    FEConstraintElementSecondOrder(FunctionSpaceBase* space, FunctionSpaceBase* mspace,
                        int n, MVertex* vp, MElement* en);
    virtual ~FEConstraintElementSecondOrder(){}
    virtual void getSecondOrderKinematicalMatrix(fullMatrix<double>& m) const;
		virtual void getSecondOrderKinematicalVector(const STensor33& G, fullVector<double>& m)const;
		virtual void getLinearConstraints(const STensor3& F, const STensor33& G,
                     std::map<Dof,DofAffineConstraint<double> >& con) const;
};

#endif // PBCCONSTRAINTELEMENTHIGHORDER_H_
