
#include "pbcConstraintElement.h"
#include "STensor33.h"
#include "SPoint3.h"

fixVertexConstraintElement::fixVertexConstraintElement(FunctionSpaceBase* space, FunctionSpaceBase* mspace,
                          int n, MVertex* v): _ndofs(n),_v(v),_periodicSpace(space),_multSpace(mspace){
  constraintElement::allPositiveVertex.insert(_v);
};
fixVertexConstraintElement::~fixVertexConstraintElement(){};
void fixVertexConstraintElement::getConstraintKeys(std::vector<Dof>& key) const{
  getKeysFromVertex(_periodicSpace,_v,key);
};
void fixVertexConstraintElement::getMultiplierKeys(std::vector<Dof>& key) const{
  getKeysFromVertex(_multSpace,_v,key);
};
void fixVertexConstraintElement::getConstraintMatrix(fullMatrix<double>& m) const{
  m.resize(_ndofs,_ndofs);
  m.setAll(0.0);
  for (int i=0; i<_ndofs; i++){
    m(i,i) =1.0;
  };
};
void fixVertexConstraintElement::getFirstOrderKinematicalMatrix(fullMatrix<double>& m) const{
  m.resize(_ndofs,9);
  m.setAll(0.0);
};
void fixVertexConstraintElement::getFirstOrderKinematicalVector(const STensor3& F, fullVector<double>& m) const{
  m.resize(_ndofs);
  m.setAll(0.0);
};
void fixVertexConstraintElement::print() const{
  printf("fix vertex = %d \n",_v->getNum());
};

void fixVertexConstraintElement::getDependentKeys(std::vector<Dof>& key) const{
  getKeysFromVertex(_periodicSpace,_v,key);
};
void fixVertexConstraintElement::getIndependentKeys(std::vector<Dof>& key) const{
};
void fixVertexConstraintElement::getLinearConstraints(const STensor3& F,
                     std::map<Dof,DofAffineConstraint<double> >& con) const{
  std::vector<Dof> k;
  getDependentKeys(k);
  DofAffineConstraint<double> c;
  for (int i=0; i<_ndofs; i++){
    c.shift=0.;
    con[k[i]] = c;
  };
};


periodicMeshConstraint::periodicMeshConstraint(FunctionSpaceBase* lagspace, FunctionSpaceBase* multspace, int n, MVertex* vp, MVertex* vn, int c) :
                      _comp(c),_vp(vp), _ndofs(n),_vn(vn),_periodicSpace(lagspace),_multSpace(multspace){
  constraintElement::allPositiveVertex.insert(_vp);
	if (_vn){
		_L(0) = _vp->x() - _vn->x();
		_L(1) = _vp->y() - _vn->y();
		_L(2) = _vp->z() - _vn->z();

		_C.resize(_ndofs,2*_ndofs);
		_C.setAll(0.0);
		_Cbc.resize(_ndofs,_ndofs);
		_Cbc.setAll(0.);
		for (int i=0; i<_ndofs; i++){
			_C(i,i) =1.0;
			_C(i,i+_ndofs) = -1.0;
			_Cbc(i,i) = 1.;
		};
	}
	else {
    _L(0) = _vp->x();
		_L(1) = _vp->y();
		_L(2) = _vp->z();
		_C.resize(_ndofs,_ndofs);
		_C.setAll(0.0);
		for (int i=0; i<_ndofs; i++){
			_C(i,i) =1.0;
		};
	};
	fullMatrix<double> temp;
	getMatrixFromVector(_L,temp);
	int size = temp.size2();
	_S.resize(_ndofs,size);
	for (int i=0; i< _ndofs; i++){
		for (int j=0; j< size; j++){
			_S(i,j) = temp(i,j);
		};
	};
};
void periodicMeshConstraint::print() const{
	if (_vn)
		printf("Postive = %d, negative = %d \n", _vp->getNum(), _vn->getNum());
	else{
		printf("Periodic point = %d \n", _vp->getNum());
	}
};
void periodicMeshConstraint::getConstraintKeys(std::vector<Dof>& key) const{
	getKeysFromVertex(_periodicSpace,_vp, key);
	if (_vn) getKeysFromVertex(_periodicSpace,_vn,key);
};
void periodicMeshConstraint::getMultiplierKeys(std::vector<Dof>& key) const{
	getKeysFromVertex(_multSpace,_vp,key);
};
void periodicMeshConstraint::getConstraintMatrix(fullMatrix<double>& m) const{
  m = _C;
};
void periodicMeshConstraint::getFirstOrderKinematicalMatrix(fullMatrix<double>& m) const{
  m = _S;
};
void periodicMeshConstraint::getFirstOrderKinematicalVector(const STensor3& FM, fullVector<double>& m) const{
	m.resize(_ndofs);
	m.setAll(0.);
	if (_comp == -1){
    for (int i=0; i<_ndofs; i++){
      for (int j=0; j<3; j++){
        m(i) += FM(i,j)*_L(j);
      }
    }
	}
	else{
    for (int i=0; i<_ndofs; i++){
      for (int j=0; j<3; j++){
        m(i) += FM(_comp,j)*_L(j);
      }
    }
	}
};

void periodicMeshConstraint::getDependentKeys(std::vector<Dof>& key) const{
  getKeysFromVertex(_periodicSpace,_vp,key);
};

void periodicMeshConstraint::getIndependentKeys(std::vector<Dof>& key) const{
  if (_vn)
    getKeysFromVertex(_periodicSpace,_vn,key);
}

void periodicMeshConstraint::getLinearConstraints(const STensor3& FM,
                                                  std::map<Dof,DofAffineConstraint<double> >& con) const{
  fullVector<double> g;
  getFirstOrderKinematicalVector(FM,g);
  std::vector<Dof> kP, kN;
  getKeysFromVertex(_periodicSpace,_vp,kP);
  if (_vn)
    getKeysFromVertex(_periodicSpace,_vn,kN);
  DofAffineConstraint<double> cons;
  for (int i=0; i<_ndofs; i++){
    if (_vn) cons.linear.push_back(std::pair<Dof,double>(kN[i],1.0));
    cons.shift=g(i);
    con[kP[i]] = cons;
    cons.linear.clear();
  };
};


cubicSplineConstraintElement::cubicSplineConstraintElement(FunctionSpaceBase* space, FunctionSpaceBase* mspace,
                                int n, MVertex* v1, MVertex* v2, MVertex* v3, int flag)
																:_ndofs(n), _vp(v1),_vn1(v2),_vn2(v3),_flag(flag),
																_periodicSpace(space),_multSpace(mspace){
  constraintElement::allPositiveVertex.insert(_vp);
  SPoint3 pp = _vp->point();
  SPoint3 np1 = _vn1->point();
  SPoint3 np2 = _vn2->point();
  SPoint3 point = project(pp,np1,np2);

  _s1 = point.distance(np1);
  _s2 = point.distance(np2);
  _length = np1.distance(np2);
  // left matrix
  _C.resize(_ndofs,5*_ndofs); _C.setAll(0.0);
	int col = 0;
	for (int i=0; i<_ndofs; i++){
		_C(i,col) = 1.0;
		col++;
	};
  _Cbc.resize(_ndofs, 4*_ndofs);
  _Cbc.setAll(0.);
	cubicSplineConstraintElement::getFF(_s1,0,_length,_FF);

	for (int i=0; i<_FF.size(); i++){
		for (int j=0; j<_ndofs; j++){
				_C(j,col) = -1.*_FF(i);
				_Cbc(j,col-_ndofs)= _FF(i);
				col++;
		};
	};

  _L(0) = pp.x() - _FF(0)*np1.x() -_FF(2)*np2.x() ;
  _L(1) = pp.y() - _FF(0)*np1.y() -_FF(2)*np2.y() ;
  _L(2) = pp.z() - _FF(0)*np1.z() -_FF(2)*np2.z() ;

	// right matrix
	fullMatrix<double> temp;
	getMatrixFromVector(_L,temp);
	int size = temp.size2();
	_S.resize(_ndofs,size);
	for (int i=0; i< _ndofs; i++){
		for (int j=0; j< size; j++){
			_S(i,j) = temp(i,j);
		};
	};
};

void cubicSplineConstraintElement::getConstraintKeys(std::vector<Dof>& keys) const{
	getKeysFromVertex(_periodicSpace,_vp,keys);

	getKeysFromVertex(_periodicSpace,_vn1,keys);
	cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vn1,keys,_flag);

	getKeysFromVertex(_periodicSpace,_vn2,keys);
	cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vn2,keys,_flag);
};
void cubicSplineConstraintElement::getMultiplierKeys(std::vector<Dof>& keys) const{
	getKeysFromVertex(_multSpace,_vp, keys);
};
void cubicSplineConstraintElement::getConstraintMatrix(fullMatrix<double>& m) const{
  m = _C;
};
void cubicSplineConstraintElement::getFirstOrderKinematicalMatrix(fullMatrix<double>& m) const{
  m = _S;
};

void cubicSplineConstraintElement::getFirstOrderKinematicalVector(const STensor3& FM, fullVector<double>& m) const{
	m.resize(_ndofs); m.setAll(0.);
	for (int i=0; i<_ndofs; i++){
	  for (int j=0; j<3; j++)
      m(i) += FM(i,j)*_L(j);
	};
};

void cubicSplineConstraintElement::print() const{
  printf("Cubic spline constraint element\n");
  printf("Positive = %d\n",_vp->getNum());
  printf("Negative = %d and %d\n",_vn1->getNum(),_vn2->getNum());
};

void cubicSplineConstraintElement::getDependentKeys(std::vector<Dof>& key) const{
  getKeysFromVertex(_periodicSpace,_vp,key);
}; // left real dofs
void cubicSplineConstraintElement::getIndependentKeys(std::vector<Dof>& keys) const{
  getKeysFromVertex(_periodicSpace,_vn1,keys);
	cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vn1,keys,_flag);
	getKeysFromVertex(_periodicSpace,_vn2,keys);
	cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vn2,keys,_flag);
};

void cubicSplineConstraintElement::getVirtualKeys(std::vector<Dof>& key) const{
  if ((_vn1) and (_vn2)){
    cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vn1,key,_flag);
    cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_vn2,key,_flag);
  };
}

void cubicSplineConstraintElement::getLinearConstraints(const STensor3& F,
                     std::map<Dof,DofAffineConstraint<double> >& con) const{
  std::vector<Dof> k, k1, k2, keys1, keys2;
  getKeysFromVertex(_periodicSpace,_vp,k);
  if ((_vn1) and (_vn2)){
    getKeysFromVertex(_periodicSpace,_vn1,k1);
    getKeysFromVertex(_periodicSpace,_vn2,k2);
    addKeysToVertex(_periodicSpace,_vn1,keys1,_flag);
    addKeysToVertex(_periodicSpace,_vn2,keys2,_flag);
  };
  fullVector<double> g;
  getFirstOrderKinematicalVector(F,g);
  DofAffineConstraint<double> cons;
  for (int i=0; i<_ndofs; i++){
    cons.shift=g(i);
    if ((_vn1) and (_vn2)){
      cons.linear.push_back(std::pair<Dof,double>(k1[i],_FF(0)));
      cons.linear.push_back(std::pair<Dof,double>(keys1[i],_FF(1)));
      cons.linear.push_back(std::pair<Dof,double>(k2[i],_FF(2)));
      cons.linear.push_back(std::pair<Dof,double>(keys2[i],_FF(3)));
    };
    con[k[i]] = cons;
    cons.linear.clear();
  };
};



lagrangeConstraintElement::lagrangeConstraintElement(FunctionSpaceBase* space, FunctionSpaceBase* mspace,
                                  int n, MVertex* v1, std::vector<MVertex*>& vlist):
                                        _ndofs(n), _vp(v1),_vn(vlist),
                                        _periodicSpace(space),_multSpace(mspace){
  constraintElement::allPositiveVertex.insert(_vp);
	for (int i=0; i<vlist.size(); i++){
    double dist = _vn[i]->distance(_vn[0]);
    _distance.push_back(dist);
	};
	SPoint3 pp = _vp->point();
	SPoint3 np1 = _vn[0]->point();
	SPoint3 np2 = _vn[_vn.size()-1]->point();
	SPoint3 point = project(pp,np1,np2);
	_s = point.distance(np1);
	int size = _vn.size();
	_C.resize(_ndofs,(1+size)*_ndofs); _C.setAll(0.0);
	int col = 0;
	for (int i=0; i<_ndofs; i++){
		_C(i,col) = 1.0;
		col++;
	};
	lagrangeConstraintElement::getFF(_s,_distance,_FF);
  _Cbc.resize(_ndofs,size*_ndofs);
  _Cbc.setAll(0.);
	for (int i=0; i<_FF.size(); i++){
		for (int j=0; j<_ndofs; j++){
				_C(j,col) = -1.*_FF(i);
				_Cbc(j,col-_ndofs) = _FF(i);
				col++;
		};
	};
  for (int j=0; j<3; j++){
    _L[j] = pp[j];
    for (int i=0; i<_FF.size(); i++){
      SPoint3 pt = _vn[i]->point();
      _L[j] -= _FF(i)*pt[j];
    }
  }
	fullMatrix<double> temp;
	getMatrixFromVector(_L,temp);
	size = temp.size2();
	_S.resize(_ndofs,size);
	for (int i=0; i< _ndofs; i++){
		for (int j=0; j< size; j++){
			_S(i,j) = temp(i,j);
		};
	};
};


void lagrangeConstraintElement::getConstraintKeys(std::vector<Dof>& keys) const{
	getKeysFromVertex(_periodicSpace,_vp, keys);
	for (int i=0; i<_vn.size(); i++){
		getKeysFromVertex(_periodicSpace,_vn[i], keys);
	};
};

void lagrangeConstraintElement::getMultiplierKeys(std::vector<Dof>& keys) const{
	getKeysFromVertex(_multSpace,_vp, keys);
};

void lagrangeConstraintElement::getConstraintMatrix(fullMatrix<double>& m)const{
	m = _C;
};
void lagrangeConstraintElement::getFirstOrderKinematicalMatrix(fullMatrix<double>& m)const{
	m = _S;
};
void lagrangeConstraintElement::getFirstOrderKinematicalVector(const STensor3& FM, fullVector<double>& m) const{
	m.resize(_ndofs);
	m.setAll(0.);
	for (int i=0; i<_ndofs; i++){
    for (int j=0; j<3; j++)
      m(i) += FM(i,j)*_L(j);
	};
};

void lagrangeConstraintElement::print() const{
  printf("Lagrange constraint element \n");
  printf("Positive = %d\n",_vp->getNum());
  for (int i=0; i<_vn.size(); i++){
    printf("%d \t", _vn[i]->getNum());
  }
  printf("\n");
};



void lagrangeConstraintElement::getDependentKeys(std::vector<Dof>& keys) const{
  getKeysFromVertex(_periodicSpace,_vp,keys);
}; // left real dofs

void lagrangeConstraintElement::getIndependentKeys(std::vector<Dof>& keys) const{
  for (int i=0; i<_vn.size(); i++){
		getKeysFromVertex(_periodicSpace,_vn[i], keys);
	};
};

void lagrangeConstraintElement::getLinearConstraints(const STensor3& F,
                     std::map<Dof,DofAffineConstraint<double> >& con) const{
  std::vector<std::vector<Dof> > k;
  for (int i=0; i<_vn.size(); i++){
    std::vector<Dof> ktemp;
    getKeysFromVertex(_periodicSpace,_vn[i],ktemp);
    k.push_back(ktemp);
  };

  std::vector<Dof> kp;
  getKeysFromVertex(_periodicSpace,_vp,kp);

	fullVector<double> g;
	getFirstOrderKinematicalVector(F,g);

  DofAffineConstraint<double> cons;
  for (int i=0; i<_ndofs; i++){
    for (int j=0; j<_vn.size(); j++){
      cons.linear.push_back(std::pair<Dof,double>(k[j][i],_FF(j)));
    };
    cons.shift=g(i);
    con[kp[i]] = cons;
    cons.linear.clear();
  };
};

FEConstraintElement::FEConstraintElement(FunctionSpaceBase* space, FunctionSpaceBase* mspace,
                        int n, MVertex* vp, MElement* en): _ndofs(n),
                        _multSpace(mspace),_periodicSpace(space),_v(vp),_e(en){
  constraintElement::allPositiveVertex.insert(_v);
  SPoint3 point;
  std::vector<MVertex*> vv;
  int size = _e->getNumVertices();
  _e->getVertices(vv);
  SPoint3 pp = _v->point();
  if (_e->getDim() == 1){
    point = project(pp,vv[0]->point(),vv[1]->point());
  }
  else if (_e->getDim() == 2){
    point = project(pp,vv[0]->point(),vv[1]->point(),vv[2]->point());
  };

  double xyz[3]={point[0],point[1],point[2]};
  double uvw[3];
  _e->xyz2uvw(xyz,uvw);

  _fVal = new double[size];
  _e->getShapeFunctions(uvw[0],uvw[1],uvw[2],_fVal);

  for (int i=0; i<3; i++){
    _L[i] = pp[i];
    for (int j=0; j<size; j++){
      SPoint3 pt = vv[j]->point();
      _L[i] -= (_fVal[j]*pt[i]);
    }
  }

  _C.resize(_ndofs,(1+size)*_ndofs); _C.setAll(0.0);
	int col = 0;
	for (int i=0; i<_ndofs; i++){
		_C(i,col) = 1.0;
		col++;
	};
  _Cbc.resize(_ndofs,size*_ndofs);
  _Cbc.setAll(0.);
  for (int j=0; j<_ndofs; j++){
    for (int i=0; i<size; i++){
				_C(j,col) = -1.*_fVal[i];
				_Cbc(j,col-_ndofs) = _fVal[i];
				col++;
		};
	};

	fullMatrix<double> temp;
	getMatrixFromVector(_L,temp);
	size = temp.size2();
	_S.resize(_ndofs,size);
  for (int j=0; j< size; j++){
    for (int i=0; i< _ndofs; i++){
			_S(i,j) = temp(i,j);
		};
	};

};
FEConstraintElement::~FEConstraintElement(){};

void FEConstraintElement::getConstraintKeys(std::vector<Dof>& keys) const{
	getKeysFromVertex(_periodicSpace,_v, keys);
	_periodicSpace->getKeys(_e,keys);
};

void FEConstraintElement::getMultiplierKeys(std::vector<Dof>& keys) const{
	getKeysFromVertex(_multSpace,_v, keys);
};

void FEConstraintElement::getConstraintMatrix(fullMatrix<double>& m)const{
	m = _C;
};
void FEConstraintElement::getFirstOrderKinematicalMatrix(fullMatrix<double>& m)const{
	m = _S;
};
void FEConstraintElement::getFirstOrderKinematicalVector(const STensor3& FM, fullVector<double>& m) const{
	m.resize(_ndofs);
	m.setAll(0.);
	for (int i=0; i<_ndofs; i++){
    for (int j=0; j<3; j++)
      m(i) += FM(i,j)*_L(j);
	};
};

void FEConstraintElement::print() const{
  printf("FE constraint element \n");
  printf("Positive = %d\n",_v->getNum());
  printf("projected element = %d\n",_e->getNum());
};

void FEConstraintElement::getDependentKeys(std::vector<Dof>& keys) const{
  getKeysFromVertex(_periodicSpace,_v,keys);
}; // left real dofs

void FEConstraintElement::getIndependentKeys(std::vector<Dof>& keys) const{
  _periodicSpace->getKeys(_e,keys);
};

void FEConstraintElement::getLinearConstraints(const STensor3& F,
                     std::map<Dof,DofAffineConstraint<double> >& con) const{
  std::vector<std::vector<Dof> > k;
  for (int i=0; i<_e->getNumVertices(); i++){
    std::vector<Dof> ktemp;
    getKeysFromVertex(_periodicSpace,_e->getVertex(i),ktemp);
    k.push_back(ktemp);
  };

  std::vector<Dof> kp;
  getKeysFromVertex(_periodicSpace,_v,kp);

	fullVector<double> g;
	getFirstOrderKinematicalVector(F,g);

  DofAffineConstraint<double> cons;
  for (int i=0; i<_ndofs; i++){
    for (int j=0; j<_e->getNumVertices(); j++){
      cons.linear.push_back(std::pair<Dof,double>(k[j][i],_fVal[j]));
    };
    cons.shift=g(i);
    con[kp[i]] = cons;
    cons.linear.clear();
  };
};

