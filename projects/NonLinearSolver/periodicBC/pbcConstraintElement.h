#ifndef PBCCONSTRAINTELEMENT_H_
#define PBCCONSTRAINTELEMENT_H_

#include "pbcConstraintElementBase.h"

class fixVertexConstraintElement : public constraintElement{
  protected:
    int _ndofs;
    MVertex* _v;
    FunctionSpaceBase* _periodicSpace, *_multSpace;

  public:
    fixVertexConstraintElement(FunctionSpaceBase* space, FunctionSpaceBase* mspace,
                              int n, MVertex* v);
    virtual ~fixVertexConstraintElement();
		virtual void getConstraintKeys(std::vector<Dof>& key) const; // real dofs on constraint elements
		virtual void getMultiplierKeys(std::vector<Dof>& key) const;	// multiplier dof on constraint element
		virtual void getConstraintMatrix(fullMatrix<double>& m) const;		// matrix C
		virtual void getFirstOrderKinematicalMatrix(fullMatrix<double>& m) const;	// matrix S
		virtual void getFirstOrderKinematicalVector(const STensor3& F, fullVector<double>& m) const;	// matrix g
		virtual void print() const;
    virtual void getDependentKeys(std::vector<Dof>& keys) const; // left real dofs
    virtual void getIndependentKeys(std::vector<Dof>& keys) const;
    virtual void getLinearConstraints(const STensor3& F,
                                     std::map<Dof,DofAffineConstraint<double> >& con) const;
    virtual constraintElement::ElementType getType() const {return constraintElement::FixedNode;};
    virtual bool isActive(const MVertex* v) const {
      return (v->getNum() == _v->getNum());
    };
};

class periodicMeshConstraint : public constraintElement{
	protected:
		MVertex* _vp, *_vn;		// two periodic vertex
		int _ndofs;   // number of Dofs to constraint
		SVector3 _L; //X^+ - X^-
		fullMatrix<double> _C, _S, _Cbc; // all constraint matrix
		FunctionSpaceBase* _periodicSpace, *_multSpace;
    int _comp; // in case enforce a particular _comp = -1 by default

	public:
		periodicMeshConstraint(FunctionSpaceBase* lagspace, FunctionSpaceBase* multspace,
                           int n, MVertex* vp, MVertex* vn = NULL, int c = -1);
		virtual ~periodicMeshConstraint(){};
		virtual void getConstraintKeys(std::vector<Dof>& key) const;
		virtual void getMultiplierKeys(std::vector<Dof>& key) const;
		virtual void getConstraintMatrix(fullMatrix<double>& m) const;
		virtual void getFirstOrderKinematicalMatrix(fullMatrix<double>& m) const;
		virtual void getFirstOrderKinematicalVector(const STensor3& F, fullVector<double>& m) const;
		virtual void print() const;
		virtual void getDependentKeys(std::vector<Dof>& keys) const; // left real dofs
    virtual void getIndependentKeys(std::vector<Dof>& keys) const;
    virtual void getLinearConstraints(const STensor3& F, std::map<Dof,DofAffineConstraint<double> >& con) const;
    virtual constraintElement::ElementType getType() const {
      if (_vn) return constraintElement::Periodic;
      else return constraintElement::PeriodicNode;
    };
    virtual bool isActive(const MVertex* v) const {
      if (_vp->getNum() == v->getNum()) return true;
      if (_vn) {
        if (_vn->getNum() == v->getNum()) return true;
      }
      return false;
    };
};


class cubicSplineConstraintElement : public constraintElement{
	protected:
		MVertex* _vp;
		MVertex* _vn1, *_vn2;
		int _ndofs, _flag;
		SVector3 _L;
		double _s1, _s2, _length;
		fullMatrix<double> _C, _S, _Cbc;
		fullVector<double> _FF;
		FunctionSpaceBase* _periodicSpace, *_multSpace;

	public:
    // to generate the slope keys
		static void addKeysToVertex(FunctionSpaceBase* space, MVertex* v, std::vector<Dof> &keys, int flag){
      std::vector<Dof> k;
      getKeysFromVertex(space,v,k);
      int ndofs=k.size();
      int type=k[0].getType();
      int entity=k[0].getEntity();
      int i1, i2;
      Dof::getTwoIntsFromType(type,i1,i2);
      for (int j=0; j<ndofs; j++){
        keys.push_back(Dof(entity, Dof::createTypeWithTwoInts(3+j+10*flag,i2)));
      };
		};
		static void getFF(double s, double s1, double s2, fullVector<double> &FF){
      FF.resize(4);
      double L=s2-s1;
      double r=(s-s1)/(s2-s1);
      FF(0) = (2*r*r*r-3*r*r+1);
      FF(1) = ((r*r*r-2*r*r+r)*L);
      FF(2) = (3*r*r-2*r*r*r);
      FF(3) = ((r*r*r-r*r)*L);
    };

	public:
		cubicSplineConstraintElement(FunctionSpaceBase* space, FunctionSpaceBase* mspace,
                                int n, MVertex* v1, MVertex* v2, MVertex* v3, int flag =0);
		virtual ~cubicSplineConstraintElement(){};
		virtual void getConstraintKeys(std::vector<Dof>& key) const; // real dofs on constraint elements
		virtual void getMultiplierKeys(std::vector<Dof>& key) const;	// multiplier dof on constraint element
		virtual void getConstraintMatrix(fullMatrix<double>& m) const;		// matrix C
		virtual void getFirstOrderKinematicalMatrix(fullMatrix<double>& m) const;	// matrix S
		virtual void getFirstOrderKinematicalVector(const STensor3& F, fullVector<double>& m) const;	// matrix g
		virtual void print() const;
		virtual void getDependentKeys(std::vector<Dof>& keys) const; // left real dofs
    virtual void getIndependentKeys(std::vector<Dof>& keys) const;
    virtual void getVirtualKeys(std::vector<Dof>& keys) const ; // for spline interpolation
    virtual void getLinearConstraints(const STensor3& F, std::map<Dof,DofAffineConstraint<double> >& con) const;
    virtual constraintElement::ElementType getType() const { return constraintElement::CubicSpline;};
    virtual bool isActive(const MVertex* v) const {
      if (_vp->getNum() == v->getNum() or _vn1->getNum() == v->getNum() or
          _vn2->getNum() == v->getNum()) return true;
      else return false;
    };
};

class lagrangeConstraintElement : public constraintElement{
	protected:
		MVertex* _vp;
		std::vector<MVertex*> _vn;  // sequence vertices
		int _ndofs;
		SVector3 _L;
		std::vector<double> _distance;
		double _s;
		fullMatrix<double> _C, _S, _Cbc;
		fullVector<double> _FF;
		FunctionSpaceBase* _periodicSpace, *_multSpace;

	public:
		static void getFF(double s, const std::vector<double>& base, fullVector<double> &vals){
      int n=base.size();
      vals.resize(n);
      for (int i=0; i<n; i++){
        vals(i)=1.0;
        for (int j=0; j<n; j++){
          if (j!=i){
            vals(i)*=(s-base[j])/(base[i]-base[j]);
          };
        };
      };
    };

	public:
		lagrangeConstraintElement(FunctionSpaceBase* space, FunctionSpaceBase* mspace,
                              int n, MVertex* v1, std::vector<MVertex*>& vlist);
		virtual ~lagrangeConstraintElement(){};
		virtual void getConstraintKeys(std::vector<Dof>& key) const; // real dofs on constraint elements
		virtual void getMultiplierKeys(std::vector<Dof>& key) const;	// multiplier dof on constraint element
		virtual void getConstraintMatrix(fullMatrix<double>& m) const;		// matrix C
		virtual void getFirstOrderKinematicalMatrix(fullMatrix<double>& m) const;	// matrix S
		virtual void getFirstOrderKinematicalVector(const STensor3& FM, fullVector<double>& m) const;	// matrix g
		virtual void print() const;
		virtual void getDependentKeys(std::vector<Dof>& keys) const; // left real dofs
		virtual void getIndependentKeys(std::vector<Dof>& keys) const; // left real dofs
    virtual void getLinearConstraints(const STensor3& F, std::map<Dof,DofAffineConstraint<double> >& con) const;
    virtual constraintElement::ElementType getType() const { return constraintElement::Lagrange;};
    virtual bool isActive(const MVertex* v) const {
      if (_vp->getNum() == v->getNum()) return true;
      for (int i=0; i<_vn.size(); i++){
        if (_vn[i]->getNum() == v->getNum()) return true;
      }
      return false;
    };
};

class FEConstraintElement:  public constraintElement{
  protected:
    MVertex * _v;
    MElement* _e;
    FunctionSpaceBase* _periodicSpace, *_multSpace;
    int _ndofs;
		SVector3 _L;
		double* _fVal;
		fullMatrix<double> _C, _S, _Cbc;

  public:
    FEConstraintElement(FunctionSpaceBase* space, FunctionSpaceBase* mspace,
                        int n, MVertex* vp, MElement* en);
    virtual ~FEConstraintElement();

    virtual void getConstraintKeys(std::vector<Dof>& key) const; // real dofs on constraint elements
		virtual void getMultiplierKeys(std::vector<Dof>& key) const;	// multiplier dof on constraint element
		virtual void getConstraintMatrix(fullMatrix<double>& m) const;		// matrix C
		virtual void getFirstOrderKinematicalMatrix(fullMatrix<double>& m) const;	// matrix S
		virtual void getFirstOrderKinematicalVector(const STensor3& FM, fullVector<double>& m) const;	// matrix g
		virtual void print() const;
		virtual void getDependentKeys(std::vector<Dof>& keys) const; // left real dofs
		virtual void getIndependentKeys(std::vector<Dof>& keys) const; // left real dofs
    virtual void getLinearConstraints(const STensor3& F, std::map<Dof,DofAffineConstraint<double> >& con) const;
    virtual constraintElement::ElementType getType() const { return constraintElement::FE;};
    virtual bool isActive(const MVertex* v) const {
      if (_v->getNum() == v->getNum()) return true;
      std::vector<MVertex*> vv;
      _e->getVertices(vv);
      for (int i=0; i<vv.size(); i++){
        if (vv[i]->getNum() == v->getNum()) return true;
      }
      return false;
    };
};


#endif // PBCCONSTRAINTELEMENT_H_
