#ifndef CONSTRAINTELIMINATIONMETHOD_H_
#define CONSTRAINTELIMINATIONMETHOD_H_

#include "periodicConstraints.h"

class constraintElimination : public periodicConstraintsBase{
  protected:
    void periodicCondition();

  public:
    constraintElimination(FunctionSpaceBase* sp, dofManager<double>* p): periodicConstraintsBase(sp,p){}
    constraintElimination(const int tag, const int dim,dofManager<double> *_p): periodicConstraintsBase(tag,dim,_p){
    };
    virtual ~constraintElimination(){};

    virtual void applyPeriodicCondition();
};

#endif // CONSTRAINTELIMINATIONMETHOD_H_
