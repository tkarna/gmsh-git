#ifndef LINEARDISPLACEMENTBC_H_
#define LINEARDISPLACEMENTBC_H_

#include "functionSpace.h"
#include "dofManager.h"
#include "periodicConstraints.h"

class linearDisplacementBC : public periodicConstraintsBase{
  public:
    typedef std::set<MVertex*> vertexContainer;

  protected:
    vertexContainer allVertex; // entity for imposing PBC

  public:
    linearDisplacementBC(const int tag, const int dim, dofManager<double>* p)
                        : periodicConstraintsBase(tag, dim,p){};
    virtual ~linearDisplacementBC(){};
    virtual void setPeriodicGroup(std::vector<groupOfElements*> group,int dim){
      gVertex.clear();
      for (int i=0; i<group.size(); i++){
        for (groupOfElements::vertexContainer::iterator it=group[i]->vbegin(); it!=group[i]->vend(); it++){
          allVertex.insert(*it);
        };
      };
    };

    virtual void applyPeriodicCondition(){
      Msg::Info("Imposed by linear displacemen boundary condition");
      for (vertexContainer::iterator it=allVertex.begin(); it!= allVertex.end(); it++){
        MVertex* v= *it;
        std::vector<Dof> k;
        getKeysFromVertex(v,k);
        fullVector<double> position; position.resize(3);
        position(0)= v->x();
        position(1)= v->y();
        position(2)= v->z();
        fullVector<double> g; g.resize(3);
        imposedDef.mult(position,g);
        int ndofs= k.size();
        for (int i=0; i<ndofs; i++)
          p->fixDof(k[i],g(i));
      };
    };

};

#endif // LINEARDISPLACEMENTBC_H_
