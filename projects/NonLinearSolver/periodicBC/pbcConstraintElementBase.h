#ifndef PBCCONSTRAINTELEMENTBASE_H_
#define PBCCONSTRAINTELEMENTBASE_H_

#include "functionSpace.h"
#include "fullMatrix.h"
#include "MPoint.h"
#include "simpleFunctionTime.h"
#include "numericalFunctions.h"

class constraintElementBase{
  public:
    virtual ~constraintElementBase(){};
};

/*Interface for constraint element
    First macroscopic kinematical variable FM-I
    Second macroscopic kinematical variable GM = grad(FM)
    Constraint form C* u = S*(FM-I)
    Constraint form ub = Cbc * uc + S*(FM-I)
  Notations:
    Left matrix       = C
    Right matrix      = S
    Right Vector  g   = S*(FM-I)
    Dependent matrix  = Cbc
    Constraint keys   = u : all keys relate in constraint
    Dependent keys   = ub
    Independent keys = uc
    Virtual keys     = all keys supplement added to approximate constraint
    within this constraint (Spline, ...). they are not the usual dofs
*/

// for constraint C*u - g = 0; g = S*FM
class constraintElement : public constraintElementBase{
  public:
    enum ElementType{Periodic =0, CubicSpline=1, Lagrange=2, FixedNode=3, PeriodicNode = 4, Supplement=5, FE = 6};
    static std::set<MVertex*> allPositiveVertex;

	public:
		virtual ~constraintElement(){};
		/** get all keys lie on constraint**/
		virtual void getConstraintKeys(std::vector<Dof>& key) const = 0; //  dofs on constraint elements
		/** get multiplier keys**/
		virtual void getMultiplierKeys(std::vector<Dof>& key) const = 0;	// multiplier dof on constraint element
		/** get constraint matrix**/
		virtual void getConstraintMatrix(fullMatrix<double>& m) const =0;		// matrix C
    /** get first order kinematrix matrix**/
		virtual void getFirstOrderKinematicalMatrix(fullMatrix<double>& m) const = 0;	// matrix S
		/** get first order kinematix vector**/
		virtual void getFirstOrderKinematicalVector(const STensor3& F, fullVector<double>& m) const = 0;	// matrix g
		/**dependent keys must be defined as unique dofs for each constraint **/
		virtual void getDependentKeys(std::vector<Dof>& keys) const =0; // left dofs
		/** remain keys are independent keys **/
    virtual void getIndependentKeys(std::vector<Dof>& keys) const = 0; // right dofs
    /** new keys add to the constraint, it can lie on dependent or indepent keys **/
    virtual void getVirtualKeys(std::vector<Dof>& keys) const {}; // for spline interpolation
    /** linear constraints are defined in terms of dependent and independent keys**/
    virtual void getLinearConstraints(const STensor3& F,
                     std::map<Dof,DofAffineConstraint<double> >& con) const=0; // linear constraint
    /**element type **/
    virtual constraintElement::ElementType getType() const =0; //  get element type
		/**print the constraint**/
		virtual void print() const{
      Msg::Info("Print the constraint ");
		};
		virtual bool isActive(const MVertex* v) const =0;
};


/*constraint for 2e order
  Constraint form C* u = S*(FM-I) + T*GM
  Constraint form ub = Cbc * uc + S*(FM-I)+T*GM
  Right matrix second order = T
  Right vector second order = T*GM
*/

class constraintElementSecondOrder {
	public:
		virtual ~constraintElementSecondOrder(){};
		/** get second order kinematic matrix**/
		virtual void getSecondOrderKinematicalMatrix(fullMatrix<double>& m) const = 0;
		/** get second order kinematic vector**/
		virtual void getSecondOrderKinematicalVector(const STensor33& G, fullVector<double>& m) const = 0;
		/** get linear constraint in second order constraint**/
		virtual void getLinearConstraints(const STensor3& F, const STensor33& G,
                     std::map<Dof,DofAffineConstraint<double> >& con) const=0;
};



#endif // PBCCONSTRAINTELEMENTBASE_H_
