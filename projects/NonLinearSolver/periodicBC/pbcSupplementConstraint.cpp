

#include "pbcSupplementConstraint.h"

supplementConstraint::supplementConstraint(FunctionSpaceBase* space, FunctionSpaceBase* mspace,
                                          int n, groupOfElements* g, int c): _comp(c),
                                  _g(g), _ndofs(n),_periodicSpace(space),_multSpace(mspace){
  int sizever = _g->vsize();
  _weight.resize(sizever);
  _v.resize(sizever);
  // number all vertices
  std::map<MVertex*,int> allVertices;
  for (groupOfElements::vertexContainer::iterator it = _g->vbegin(); it!= _g->vend(); it++){
    MVertex* vertex = *it;
    int size = allVertices.size();
    allVertices[vertex] = size;
    _v[size] = vertex;
  }
  _weight.resize(sizever);
  _weight.setAll(0.);
  // integration rule
  _integrationRule = new GaussQuadrature(GaussQuadrature::Val);
  IntPt* GP;
  double jac[3][3];
  double surface =0;
  double val[256];
  // Use a Space to compute Jacobian and shape Function values
  for (groupOfElements::elementContainer::iterator it = _g->begin(); it!= _g->end(); it++){
    MElement* ele = *it;
    int numVer = ele->getNumVertices();
    int npts = _integrationRule->getIntPoints(ele,&GP);
    for (int i=0; i<npts; i++){
      double u = GP[i].pt[0]; double v = GP[i].pt[1]; double w = GP[i].pt[2];
      double wgt = GP[i].weight;
      double detJ = ele->getJacobian(u, v, w, jac);
      double ratio = wgt*detJ;
      surface += ratio;
      ele->getShapeFunctions(u,v,w,val);
      for (int j=0; j<numVer; j++){
        MVertex* vertex= ele->getVertex(j);
        _weight(allVertices[vertex]) += val[j]*ratio;
      }
    }
  }

  _positive = 0;
  for (int i=0; i<sizever-1; i++){
    if (_weight(i+1)>_weight(_positive) &&
        constraintElement::allPositiveVertex.find(_v[i+1]) == constraintElement::allPositiveVertex.end()){
      _positive = i+1;
    }
  };
  constraintElement::allPositiveVertex.insert(_v[_positive]);
  double invs = 1./(_weight(_positive));
  _factor = _weight(_positive);
  _weight.scale(invs);
  _Xmean*= 0;
  for (int i=0; i<_v.size();i++){
    MVertex* v = _v[i];
    SPoint3 pnt = v->point();
    for( int j=0; j<3; j++){
      _Xmean[j] += _weight(i)*pnt[j];
    }
  }
  // add a virtual vertex for identify the constraint
  _tempVertex = new MVertex(_v[_positive]->x(),_v[_positive]->y(),_v[_positive]->z());

  _S.resize(_ndofs,9);

  fullMatrix<double> temp;
  getMatrixFromVector(_Xmean,temp);
  for (int i=0; i<_ndofs; i++)
    for (int j=0; j<temp.size2(); j++)
      _S(i,j) = temp(i,j);
  _C.resize(_ndofs,sizever*_ndofs);
  _C.setAll(0.);

  _Cbc.resize(_ndofs, (sizever-1)*_ndofs);
  _Cbc.setAll(0.);

  int col = 0;
  int colbc = 0;
  for (int i=0; i<_weight.size(); i++)
    for (int j=0; j<_ndofs; j++){
      _C(j,col) = _weight(i);
      if (i != _positive){
        _Cbc(j,colbc) = -1.*_weight(i);
        colbc++;
      }
      col++;
    }
};


void supplementConstraint::getConstraintKeys(std::vector<Dof>& key) const {
  for (int i=0; i<_v.size(); i++)
      getKeysFromVertex(_periodicSpace,_v[i],key);
}
void supplementConstraint::getMultiplierKeys(std::vector<Dof>& key) const{
  getKeysFromVertex(_multSpace,_tempVertex,key);
}
void supplementConstraint::getConstraintMatrix(fullMatrix<double>& m) const {
  m = _C;
};		// matrix C

void supplementConstraint::getFirstOrderKinematicalMatrix(fullMatrix<double>& m) const{
  m = _S;
};	// matrix S

void supplementConstraint::getFirstOrderKinematicalVector(const STensor3& F, fullVector<double>& m) const{
  m.resize(_ndofs); m.setAll(0.);
  if (_comp == -1){
    for (int i=0; i<_ndofs; i++)
      for (int j=0; j<3; j++)
        m(i) += F(i,j)*_Xmean(j);
  }
  else{
    for (int i=0; i<_ndofs; i++)
      for (int j=0; j<3; j++)
        m(i) += F(_comp,j)*_Xmean(j);
  }
};	// matrix g

// for constraint form  u = C u* + Sb*FM + M uc
void supplementConstraint::getDependentKeys(std::vector<Dof>& keys) const {
  getKeysFromVertex(_periodicSpace,_v[_positive],keys);
}; // left real dofs

void supplementConstraint::getIndependentKeys(std::vector<Dof>& keys) const{
  for (int i=0; i<_v.size(); i++)
    if (i!= _positive)
      getKeysFromVertex(_periodicSpace,_v[i],keys);
}


void supplementConstraint::getLinearConstraints(const STensor3& F,
                 std::map<Dof,DofAffineConstraint<double> >& con) const{
  std::vector<std::vector<Dof> > k;
  for (int i=0; i< _v.size(); i++){
    std::vector<Dof> ktemp;
    getKeysFromVertex(_periodicSpace,_v[i],ktemp);
    k.push_back(ktemp);
  };

  std::vector<Dof> kp;
  this->getDependentKeys(kp);

  fullVector<double> g;
  getFirstOrderKinematicalVector(F,g);

  DofAffineConstraint<double> cons;
  for (int i=0; i<_ndofs; i++){
    for (int j=0; j<_v.size(); j++){
      if (j!= _positive)
        cons.linear.push_back(std::pair<Dof,double>(k[j][i],-1.*_weight(j)));
    };
    cons.shift=g(i);
    con[kp[i]] = cons;
    cons.linear.clear();
  };
};

void supplementConstraint::print() const{
  Msg::Info("Positive = %d",_v[_positive]->getNum());
  Msg::Info("COnstraint numbered = %d",_tempVertex->getNum());
  for (int i=0; i<_v.size(); i++){
    printf("%d \t",_v[i]->getNum());
  }
  printf("\n");
};

lagrangeSupplementConstraint::lagrangeSupplementConstraint(FunctionSpaceBase* space, FunctionSpaceBase* mspace,
                            int ndofs, std::vector<MVertex*>& v):
                            _periodicSpace(space),_multSpace(mspace),
                            _ndofs(ndofs),_v(v),_tempVertex(0){
  std::vector<double> base;
  int n = _v.size();
  for (int i=0; i<n; i++){
    double val = _v[i]->distance(_v[0]);
    base.push_back(val);
  };
  lagrangeSupplementConstraint::integrateFF(base,_weight);
  _positive = 0;
  for (int i=0; i<n-1; i++){
   if (_weight(i+1)>_weight(_positive) &&
       constraintElement::allPositiveVertex.find(_v[i+1]) == constraintElement::allPositiveVertex.end()){
      _positive = i+1;
    }
  };
  constraintElement::allPositiveVertex.insert(_v[_positive]);
  double invs = 1./(_weight(_positive));
  _factor = _weight(_positive);
  _weight.scale(invs);
  _Xmean*= 0;
  for (int i=0; i<n;i++){
    SPoint3 pnt = _v[i]->point();
    for( int j=0; j<3; j++){
      _Xmean[j] += _weight(i)*pnt[j];
    }
  }
  // add a virtual vertex for identify the constraint
  _tempVertex = new MVertex(_v[_positive]->x(),_v[_positive]->y(),_v[_positive]->z());
  _S.resize(_ndofs,9);
  fullMatrix<double> temp;
  getMatrixFromVector(_Xmean,temp);
  for (int i=0; i<_ndofs; i++){
    for (int j=0; j<temp.size2(); j++){
      _S(i,j) = temp(i,j);
    }
  }
  _C.resize(_ndofs,n*_ndofs);
  _C.setAll(0.);
  _Cbc.resize(_ndofs, (n-1)*_ndofs);
  _Cbc.setAll(0.);

  int col = 0;
  int colbc = 0;
  for (int i=0; i<n; i++){
    for (int j=0; j<_ndofs; j++){
      _C(j,col) = _weight(i);
      if (i != _positive){
        _Cbc(j,colbc) = -1.*_weight(i);
        colbc++;
      }
      col++;
    }
  }
};

void lagrangeSupplementConstraint::integrateFF(const std::vector<double>& base, fullVector<double>& integ){
  int n = base.size();
  integ.resize(n);
  integ.setAll(0.);

  int N=15;

  double s0 = base[0];
  double sn = base[n-1];

  double delta = (sn-s0)/N;
  fullVector<double> horizon(N+1);
  for (int j=0; j<=N; j++)
    horizon(j) = s0+j*delta;
  std::vector<fullVector<double> > allVals;
  for (int j=0; j<=N; j++){
    fullVector<double> val;
    lagrangeConstraintElement::getFF(horizon(j),base,val);
    allVals.push_back(val);
  }

  for (int i=0; i<n; i++){
    integ(i) = 0;
    for (int j=0; j<N; j++){
      integ(i) += (horizon(j+1)-horizon(j))*(allVals[j+1](i)+allVals[j](i))*0.5;
    }
  }
}


void lagrangeSupplementConstraint::getConstraintKeys(std::vector<Dof>& key) const {
  for (int i=0; i<_v.size(); i++)
    getKeysFromVertex(_periodicSpace,_v[i],key);
}
void lagrangeSupplementConstraint::getMultiplierKeys(std::vector<Dof>& key) const{
  getKeysFromVertex(_multSpace,_tempVertex,key);
}
void lagrangeSupplementConstraint::getConstraintMatrix(fullMatrix<double>& m) const {
  m = _C;
};		// matrix C

void lagrangeSupplementConstraint::getFirstOrderKinematicalMatrix(fullMatrix<double>& m) const{
  m = _S;
};	// matrix S

void lagrangeSupplementConstraint::getFirstOrderKinematicalVector(const STensor3& F, fullVector<double>& m) const{
  m.resize(_ndofs); m.setAll(0.);
  for (int i=0; i<_ndofs; i++)
    for (int j=0; j<3; j++)
      m(i) += F(i,j)*_Xmean(j);
};	// matrix g

// for constraint form  u = C u* + Sb*FM + M uc
void lagrangeSupplementConstraint::getDependentKeys(std::vector<Dof>& keys) const {
  getKeysFromVertex(_periodicSpace,_v[_positive],keys);
}; // left real dofs

void lagrangeSupplementConstraint::getIndependentKeys(std::vector<Dof>& keys) const{
  for (int i=0; i<_v.size(); i++)
    if (i!= _positive)
      getKeysFromVertex(_periodicSpace,_v[i],keys);
}
void lagrangeSupplementConstraint::getLinearConstraints(const STensor3& F,
                 std::map<Dof,DofAffineConstraint<double> >& con) const{
  std::vector<std::vector<Dof> > k;
  for (int i=0; i< _v.size(); i++){
    std::vector<Dof> ktemp;
    getKeysFromVertex(_periodicSpace,_v[i],ktemp);
    k.push_back(ktemp);
  };

  std::vector<Dof> kp;
  this->getDependentKeys(kp);

  fullVector<double> g;
  getFirstOrderKinematicalVector(F,g);

  DofAffineConstraint<double> cons;
  for (int i=0; i<_ndofs; i++){
    for (int j=0; j<_v.size(); j++){
      if (j!= _positive)
        cons.linear.push_back(std::pair<Dof,double>(k[j][i],-1.*_weight(j)));
    };
    cons.shift=g(i);
    con[kp[i]] = cons;
    cons.linear.clear();
  };
};

void lagrangeSupplementConstraint::print() const{
  Msg::Info("Positive = %d",_v[_positive]->getNum());
  Msg::Info("COnstraint numbered = %d",_tempVertex->getNum());
  for (int i=0; i<_v.size(); i++){
    printf("%d \t",_v[i]->getNum());
  }
  printf("\n");
};


cubicSplineSupplementConstraint::cubicSplineSupplementConstraint(FunctionSpaceBase* space, FunctionSpaceBase* mspace,
                                int ndofs, std::vector<MVertex*>& v, int flag):
                                _periodicSpace(space),_multSpace(mspace),
                                _ndofs(ndofs),_v(v),_flag(flag),_tempVertex(NULL){
  std::vector<double> base;
  int n = _v.size();
  for (int i=0; i<n; i++){
    double val = _v[i]->distance(_v[0]);
    base.push_back(val);
  };

  _weight.resize(2*n);
  _weight.setAll(0.);
  for (int i=0; i<n-1; i++){
    fullVector<double> integ;
    cubicSplineSupplementConstraint::integrateFF(base[i],base[i+1],integ);
    _weight(2*i+0) += integ(0);
    _weight(2*i+1) += integ(1);
    _weight(2*i+2) += integ(2);
    _weight(2*i+3) += integ(3);
  }
  _weight.print("weight");
  _positive = 0;
  for (int i=0; i<n-1; i++){
    if (_weight(2*(i+1))>_weight(2*_positive) &&
        constraintElement::allPositiveVertex.find(_v[i+1]) == constraintElement::allPositiveVertex.end()){
      _positive = i+1;
    }
  };
  constraintElement::allPositiveVertex.insert(_v[_positive]);
  Msg::Info("positive= %d",_positive);

  double invs = 1./(_weight(2*_positive));
  _factor = _weight(2*_positive);
  _weight.scale(invs);

  _weight.print("weight");

  _Xmean*= 0;
  for (int i=0; i<n;i++){
    SPoint3 pnt = _v[i]->point();
    for( int j=0; j<3; j++){
      _Xmean[j] += _weight(2*i)*pnt[j];
    }
  }
  // add a virtual vertex for identify the constraint
  _tempVertex = new MVertex(_v[_positive]->x(),_v[_positive]->y(),_v[_positive]->z());
  _S.resize(_ndofs,9);
  fullMatrix<double> temp;
  getMatrixFromVector(_Xmean,temp);
  for (int i=0; i<_ndofs; i++){
    for (int j=0; j<temp.size2(); j++){
      _S(i,j) = temp(i,j);
    }
  }
  _C.resize(_ndofs,2*n*_ndofs);
  _C.setAll(0.);
  _Cbc.resize(_ndofs, (2*n-1)*_ndofs);
  _Cbc.setAll(0.);

  int col = 0;
  int colbc = 0;
  for (int i=0; i<2*n; i++){
    for (int j=0; j<_ndofs; j++){
      _C(j,col) = _weight(i);
      if (i != 2*_positive){
        _Cbc(j,colbc) = -1.*_weight(i);
        colbc++;
      }
      col++;
    }
  }
  _C.print("Matrix C");
  _Cbc.print("Matrix Cbc");

};
void cubicSplineSupplementConstraint::getConstraintKeys(std::vector<Dof>& key) const{
  int n = _v.size();
  for (int i=0; i<n; i++){
    getKeysFromVertex(_periodicSpace,_v[i],key);
    cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_v[i],key,_flag);
  }
};

void cubicSplineSupplementConstraint::getMultiplierKeys(std::vector<Dof>& key) const{
  getKeysFromVertex(_multSpace,_tempVertex,key);
};

void cubicSplineSupplementConstraint::getConstraintMatrix(fullMatrix<double>& m) const{
  m = _C;
};		// matrix C

void cubicSplineSupplementConstraint::getFirstOrderKinematicalMatrix(fullMatrix<double>& m) const{
  m = _S;
};	// matrix S

void cubicSplineSupplementConstraint::getFirstOrderKinematicalVector(const STensor3& F, fullVector<double>& m) const{
  m.resize(_ndofs);
  m.setAll(0.);
  for (int i=0; i<_ndofs; i++){
    for (int j=0; j<3; j++){
      m(i) += F(i,j)*_Xmean(j);
    }
  }
};	// matrix g

void cubicSplineSupplementConstraint::getDependentKeys(std::vector<Dof>& key) const{
  getKeysFromVertex(_periodicSpace,_v[_positive],key);
}; // left real dofs

void cubicSplineSupplementConstraint::getIndependentKeys(std::vector<Dof>& key) const{
  int n = _v.size();
  for (int i=0; i<n; i++){
    if (i != _positive)
      getKeysFromVertex(_periodicSpace,_v[i],key);
    cubicSplineConstraintElement::addKeysToVertex(_periodicSpace,_v[i],key,_flag);
  }
};

void cubicSplineSupplementConstraint::getLinearConstraints(const STensor3& F,
                     std::map<Dof,DofAffineConstraint<double> >& con) const{
  Msg::Fatal("This is not implemented");
};

CoonsPatchLagrangeSupplementConstraint::CoonsPatchLagrangeSupplementConstraint(FunctionSpaceBase* space, FunctionSpaceBase* mspace,
                                                                              int ndofs, lagrangeSupplementConstraint* conX,
                                                                               lagrangeSupplementConstraint* conY) :
                                _periodicSpace(space),_multSpace(mspace),
                                _ndofs(ndofs),_vroot(NULL){
  std::vector<MVertex*>& _vx = conX->getVertexList();
  std::vector<MVertex*>& _vy = conY->getVertexList();

  for (int i=0; i<_vx.size(); i++){
    for (int j =0; j<_vy.size(); j++){
      if (_vy[j] == _vx[i]){
        _vroot = _vx[i];
        break;
      }
    }
    if (_vroot) break;
  }
  for (int i=0; i<_vx.size(); i++){
    _v.push_back(_vx[i]);
  }
  for (int i=0; i<_vy.size(); i++){
    _v.push_back(_vy[i]);
  }
  _v.push_back(_vroot);

  fullVector<double>& weightX = conX->getWeight();
  fullVector<double>& weightY = conY->getWeight();

  double lengthX = _vx[0]->distance(_vx[_vx.size()-1]);
  double lengthY = _vy[0]->distance(_vy[_vy.size()-1]);

  double factorX = conX->getConstraintFactor();
  double factorY = conY->getConstraintFactor();

  _weight.resize(weightX.size()+ weightY.size()+1);
  for (int i=0; i< weightX.size(); i++){
    _weight(i) = weightX(i)* factorX*lengthY;
  }
  for (int i=0; i< weightY.size(); i++){
    _weight(i+weightX.size()) = weightY(i)*factorY*lengthX;
  }
  _weight(weightX.size()+ weightY.size()) = -1.*lengthX*lengthY;


  _tempVertex = new MVertex(_vroot->x(),_vroot->y(), _vroot->z());

  int n = _v.size();
  _positive = 0;
  for (int i=0; i<n-1; i++){
    if (_weight(i+1)>_weight(_positive) &&
        constraintElement::allPositiveVertex.find(_v[i+1]) == constraintElement::allPositiveVertex.end()){
      _positive = i+1;
    }
  };

  constraintElement::allPositiveVertex.insert(_v[_positive]);

  double invs = 1./(_weight(_positive));
  _factor = _weight(_positive);
  _weight.scale(invs);
  _Xmean*= 0;
  for (int i=0; i<n;i++){
    SPoint3 pnt = _v[i]->point();
    for( int j=0; j<3; j++){
      _Xmean[j] += _weight(i)*pnt[j];
    }
  }
  // add a virtual vertex for identify the constraint
  _tempVertex = new MVertex(_v[_positive]->x(),_v[_positive]->y(),_v[_positive]->z());
  _S.resize(_ndofs,9);
  fullMatrix<double> temp;
  getMatrixFromVector(_Xmean,temp);
  for (int i=0; i<_ndofs; i++){
    for (int j=0; j<temp.size2(); j++){
      _S(i,j) = temp(i,j);
    }
  }
  _C.resize(_ndofs,n*_ndofs);
  _C.setAll(0.);
  _Cbc.resize(_ndofs, (n-1)*_ndofs);
  _Cbc.setAll(0.);

  int col = 0;
  int colbc = 0;
  for (int i=0; i<n; i++){
    for (int j=0; j<_ndofs; j++){
      _C(j,col) = _weight(i);
      if (i != _positive){
        _Cbc(j,colbc) = -1.*_weight(i);
        colbc++;
      }
      col++;
    }
  }
}

void CoonsPatchLagrangeSupplementConstraint::getConstraintKeys(std::vector<Dof>& key) const{
  for (int i=0; i<_v.size(); i++)
    getKeysFromVertex(_periodicSpace,_v[i],key);
}; // real dofs on constraint elements
void CoonsPatchLagrangeSupplementConstraint::getMultiplierKeys(std::vector<Dof>& key) const{
  getKeysFromVertex(_multSpace,_tempVertex,key);
};	// multiplier dof on constraint element
void CoonsPatchLagrangeSupplementConstraint::getConstraintMatrix(fullMatrix<double>& m) const{
  m = _C;
};		// matrix C
void CoonsPatchLagrangeSupplementConstraint::getFirstOrderKinematicalMatrix(fullMatrix<double>& m) const{
  m = _S;
};	// matrix S
void CoonsPatchLagrangeSupplementConstraint::getFirstOrderKinematicalVector(const STensor3& F, fullVector<double>& m) const{
  m.resize(_ndofs); m.setAll(0.);
  for (int i=0; i<_ndofs; i++)
    for (int j=0; j<3; j++)
      m(i) += F(i,j)*_Xmean(j);
};	// matrix g


void CoonsPatchLagrangeSupplementConstraint::getDependentKeys(std::vector<Dof>& keys) const {
  getKeysFromVertex(_periodicSpace,_v[_positive],keys);
}; // left real dofs

void CoonsPatchLagrangeSupplementConstraint::getIndependentKeys(std::vector<Dof>& keys) const{
  for (int i=0; i<_v.size(); i++)
    if (i!= _positive)
      getKeysFromVertex(_periodicSpace,_v[i],keys);
}
void CoonsPatchLagrangeSupplementConstraint::getLinearConstraints(const STensor3& F,
                 std::map<Dof,DofAffineConstraint<double> >& con) const{
  std::vector<std::vector<Dof> > k;
  for (int i=0; i< _v.size(); i++){
    std::vector<Dof> ktemp;
    getKeysFromVertex(_periodicSpace,_v[i],ktemp);
    k.push_back(ktemp);
  };

  std::vector<Dof> kp;
  this->getDependentKeys(kp);

  fullVector<double> g;
  getFirstOrderKinematicalVector(F,g);

  DofAffineConstraint<double> cons;
  for (int i=0; i<_ndofs; i++){
    for (int j=0; j<_v.size(); j++){
      if (j!= _positive)
        cons.linear.push_back(std::pair<Dof,double>(k[j][i],-1.*_weight(j)));
    };
    cons.shift=g(i);
    con[kp[i]] = cons;
    cons.linear.clear();
  };
};
