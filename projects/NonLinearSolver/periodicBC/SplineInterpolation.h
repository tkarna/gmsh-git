#ifndef SPLINEINTERPOLATION_H_
#define SPLINEINTERPOLATION_H_

#include "polynomialInterpolation.h"

//--------------------------------Interpolation by cubic spline interpolation---------------------

class polynomialInterpolationByCubicSpline: public polynomialInterpolationBase2D{
  protected:
    void addKeysToVertex(MVertex* v, std::vector<Dof> &keys, int flag);
    void formCondition();
    void periodicCondition();

  public:
    polynomialInterpolationByCubicSpline(FunctionSpaceBase* sp, dofManager<double>* p, int deg);
    polynomialInterpolationByCubicSpline(const int tag, const int dim, dofManager<double>* _p,int deg);
    ~polynomialInterpolationByCubicSpline(){};

    virtual void applyPeriodicCondition();
};


class polynomialInterpolationByCubicSpline3D: public polynomialInterpolationBase3D{
  protected:
    void addKeysToVertex(MVertex* v, std::vector<Dof> &keys, int flag);
    void formCondition();
    void periodicCondition();
    void lineFormCondition(vertexContainer container, std::vector<MVertex*> vlist,
                            MVertex* vroot, MVertex* vtip, int num);
    void linePeriodicCondition(vertexContainer container, std::vector<MVertex*> vlist,
                           MVertex* vroot, MVertex* vtip, MVertex* lroot, MVertex* ltip ,int num);
    void faceFormCondition(vertexContainer container, std::vector<MVertex*> xl, std::vector<MVertex*> yl,
                          MVertex* v1, MVertex* v2, MVertex* v3, MVertex* v4, int num1, int num2);
    void facePeriodicCondition(vertexContainer container, std::vector<MVertex*> xl, std::vector<MVertex*> yl,
                          MVertex* v1, MVertex* v2, MVertex* v3, MVertex* v4, int num1, int num2);

  public:
    polynomialInterpolationByCubicSpline3D(FunctionSpaceBase* sp, dofManager<double>* p, int deg);
    polynomialInterpolationByCubicSpline3D(const int tag, const int dim, dofManager<double>* _p,int deg);
    ~polynomialInterpolationByCubicSpline3D(){};

    virtual void applyPeriodicCondition();
};

#endif // SPLINEINTERPOLATION_H_
