
#include "functionPETSc.h"
#include "linearSystemPETSc.hpp"
#include "GmshConfig.h"
#if defined(HAVE_PETSC)

void MatToFile(Mat& a, const std::string filename){
  FILE* file = fopen(filename.c_str(),"w");
  PetscInt M, N, err;
  err = MatGetSize(a,&M,&N); CHKERRABORT(PETSC_COMM_WORLD,err);
  for (int i=0; i<M; i++){
    for (int j=0; j<N; j++){
      double val;
      err = MatGetValues(a,1,&i,1,&j,&val); CHKERRABORT(PETSC_COMM_WORLD,err);
      fprintf(file,"%.16g ",val);
    };
    fprintf(file,"\n");
  }
  fclose(file);
};

void VecToFile(Vec& a,const std::string filename){
  FILE* file = fopen(filename.c_str(),"w");
  PetscInt size, err;
  err = VecGetSize(a,&size);CHKERRABORT(PETSC_COMM_WORLD,err);
  for (int i=0; i<size; i++){
    double val;
    err = VecGetValues(a,1,&i,&val); CHKERRABORT(PETSC_COMM_WORLD,err);
    fprintf(file,"%.16g \n",val);
  }
  fclose(file);
}

#ifdef PETSC_HAVE_MUMPS

 #if defined(PETSC_USE_COMPLEX)
  #if defined(PETSC_USE_REAL_SINGLE)
  #include <cmumps_c.h>
  #else
  #include <zmumps_c.h>
  #endif
#else
  #if defined(PETSC_USE_REAL_SINGLE)
  #include <smumps_c.h>
  #else
  #include <dmumps_c.h>
  #endif
  #endif

#if (PETSC_VERSION_RELEASE == 0 || ((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3))) // petsc-dev
#include "petsc-private/matimpl.h"
#else
#include "private/matimpl.h"
#endif

#define ICNTL(I) icntl[(I)-1]
#define CNTL(I) cntl[(I)-1]
#define INFOG(I) infog[(I)-1]
#define INFO(I) info[(I)-1]
#define RINFOG(I) rinfog[(I)-1]
#define RINFO(I) rinfo[(I)-1]

struct MatMUMPS{
  #if defined(PETSC_USE_COMPLEX)
    #if defined(PETSC_USE_REAL_SINGLE)
    CMUMPS_STRUC_C id;
    #else
    ZMUMPS_STRUC_C id;
    #endif
  #else
    #if defined(PETSC_USE_REAL_SINGLE)
    SMUMPS_STRUC_C id;
    #else
    DMUMPS_STRUC_C id;
    #endif
  #endif

/*
  MatStructure   matstruc;
  PetscMPIInt    myid,size;
  PetscInt       *irn,*jcn,nz,sym,nSolve;
  PetscScalar    *val;
  MPI_Comm       comm_mumps;
  VecScatter     scat_rhs, scat_sol;
  PetscBool      isAIJ,CleanUpMUMPS;
  Vec            b_seq,x_seq;
  PetscErrorCode (*Destroy)(Mat);
  PetscErrorCode (*ConvertToTriples)(Mat, int, MatReuse, int*, int**, int**, PetscScalar**);
  */
};
#endif

PetscErrorCode MatGetDeterminant(Mat A, PetscReal* realpart, PetscReal* imagpart, PetscInt* ex){
  #ifdef PETSC_HAVE_MUMPS
  MatMUMPS* FF = (MatMUMPS*)A->spptr;

  if (FF != NULL){
    *realpart = FF->id.RINFOG(12);
    *imagpart = FF->id.RINFOG(13);
    *ex = FF->id.INFOG(34);
  }
  else
    Msg::Error("Error in petsc matrix conversion");
  #else
  *realpart = 1.;
  *imagpart = 0.;
  *ex = 0;
  #endif
  PetscFunctionReturn(0);
};

// use MatMatSolve to compute schur complement
PetscErrorCode GetSchurComplementMat(Mat fA11, Mat A12, Mat A21, Mat A22, Mat* A){
  #ifdef _DEBUG
  Msg::Info("Begin computing Schur  complement");
  #endif
  _try(MatAssemblyBegin(A12,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(A12,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyBegin(A21,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(A21,MAT_FINAL_ASSEMBLY));

  Mat invA11A12;
  _try(MatDuplicate(A12,MAT_DO_NOT_COPY_VALUES,&invA11A12));
  _try(MatMatSolve(fA11,A12,invA11A12));

  _try(MatMatMult(A21,invA11A12, MAT_INITIAL_MATRIX,PETSC_DEFAULT,A));
  _try(MatScale(*A,-1.));

	if (A22){
	  _try(MatAssemblyBegin(A22,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(A22,MAT_FINAL_ASSEMBLY));
    _try(MatAXPY(*A,1.,A22,DIFFERENT_NONZERO_PATTERN));
	}
	_try(MatAssemblyBegin(*A,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(*A,MAT_FINAL_ASSEMBLY));

  //destroy the temp matrix
  _try(MatDestroy(&invA11A12));
  #ifdef _DEBUG
  Msg::Info("End computing Schur  complement");
  #endif
	PetscFunctionReturn(0);
};

PetscErrorCode GetSchurComplementMat_Naive(KSP& ksp, Mat A11, Mat A12, Mat A21, Mat A22, Mat* A){
  MPI_Comm comm = PETSC_COMM_WORLD;
  //Msg::Info("Begin computing Schur  complement");
  _try(MatAssemblyBegin(A12,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(A12,MAT_FINAL_ASSEMBLY));

  _try(MatAssemblyBegin(A21,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(A21,MAT_FINAL_ASSEMBLY));

  PetscInt M21, N21, M12, N12;
  _try(MatGetSize(A12,&M12,&N12));
  _try(MatGetSize(A21,&M21,&N21));

  if (N21 != M12)
    Msg::Fatal("Error in matrix dimension for stiffness condensation");

  if (A22){
    _try(MatDuplicate(A22,MAT_COPY_VALUES,A));
  }
  else{
    _try(MatCreate(comm,A));
    _try(MatSetSizes(*A,PETSC_DECIDE,PETSC_DECIDE,M21,N12));
    _try(MatSetFromOptions(*A));
  };

  _try(MatAssemblyBegin(*A,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(*A,MAT_FINAL_ASSEMBLY));

  Mat invA11A12;
  _try(MatDuplicate(A12,MAT_DO_NOT_COPY_VALUES,&invA11A12));

  for (int i=0; i<N12; i++){
		 Vec vec, x;
		_try(MatGetVecs(A11,&vec,PETSC_NULL));
		_try(MatGetColumnVector(A12,vec,i));
		_try(VecAssemblyBegin(vec));
		_try(VecAssemblyEnd(vec));
		_try(VecDuplicate(vec,&x));
		_try(KSPSolve(ksp,vec,x));

		PetscScalar* scalar;
		_try(VecGetArray(x,&scalar));

		PetscScalar norm;
		_try(VecNorm(x,NORM_INFINITY,&norm));
	//	printf(" M= %d, N = %d, nor = %f \n",M,N,norm);

		for (int j=0; j<M12; j++){
			PetscScalar val = scalar[j];
			PetscInt ii = i, jj=j;
			if (fabs(val/norm)>1e-6){
				_try(MatSetValues(invA11A12,1,&jj,1,&ii,&val,INSERT_VALUES));
			};
		};
		_try(VecDestroy(&vec));
		_try(VecDestroy(&x));
	};
  _try(MatAssemblyBegin(invA11A12,MAT_FINAL_ASSEMBLY));
	_try(MatAssemblyEnd(invA11A12,MAT_FINAL_ASSEMBLY));
	Mat A21invA11A12;
	_try(MatDuplicate(*A,MAT_DO_NOT_COPY_VALUES,&A21invA11A12));

	_try(MatMatMult(A21,invA11A12, MAT_INITIAL_MATRIX,PETSC_DEFAULT,&A21invA11A12));
	_try(MatAssemblyBegin(A21invA11A12,MAT_FINAL_ASSEMBLY));
	_try(MatAssemblyEnd(A21invA11A12,MAT_FINAL_ASSEMBLY));

	_try(MatAXPY(*A,-1,A21invA11A12,DIFFERENT_NONZERO_PATTERN));
	_try(MatAssemblyBegin(*A,MAT_FINAL_ASSEMBLY));
	_try(MatAssemblyEnd(*A,MAT_FINAL_ASSEMBLY));
	//destroy the temp matrix
	_try(MatDestroy(&invA11A12));
        _try(MatDestroy(&A21invA11A12));
  //
  //Msg::Info("End computing Schur  complement");
	PetscFunctionReturn(0);

};

PetscErrorCode MatInverse_Naive(Mat A, Mat* B, MPI_Comm comm){
  _try(MatAssemblyBegin(A,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(A,MAT_FINAL_ASSEMBLY));
  _try(MatDuplicate(A,MAT_DO_NOT_COPY_VALUES,B));
  PetscInt M,N;
  _try(MatGetSize(A,&M,&N));
  if (M!= N)
    Msg::Fatal("Can not inverse a rectangular matrix %d %d",M,N);

  KSP ksp;
  _try(KSPCreate(comm,&ksp));
  _try(KSPSetOperators(ksp,A,A,DIFFERENT_NONZERO_PATTERN));
  PC pc;
  _try(KSPGetPC(ksp,&pc));
  _try(KSPSetTolerances(ksp, 1.e-8, PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT));
  _try(KSPSetFromOptions(ksp));
  _try(PCSetFromOptions(pc));

  for (int i=0; i<N; i++){
    Vec vec, x;
    _try(MatGetVecs(A,&vec,PETSC_NULL));
    PetscInt col = i;
    PetscScalar s = 1.0;
    _try(VecSetValues(vec,1,&col,&s, INSERT_VALUES));
    _try(VecAssemblyBegin(vec));
    _try(VecAssemblyEnd(vec));
    // KSP solve
    _try(VecDuplicate(vec,&x));
    _try(KSPSolve(ksp,vec,x));
/*
    #ifdef _DEBUG
    PetscInt its;
    _try(KSPGetIterationNumber(ksp, &its));
    Msg::Info("Inverse matrix %d, %d iterations",i,its);
    #endif
*/
    _try(VecAssemblyBegin(x));
    _try(VecAssemblyEnd(x));
    PetscScalar norm;
    _try(VecNorm(x,NORM_INFINITY,&norm));

    if (norm < 1.e-16) Msg::Fatal("Singular matrix");

    PetscScalar* ss;
    _try(VecGetArray(x,&ss));
    for (int j=0; j<M; j++){
      PetscScalar val = ss[j];
      PetscInt ii = i, jj=j;
      if (fabs(val/norm)>1e-16){
        _try(MatSetValues(*B,1,&jj,1,&ii,&val,INSERT_VALUES));
      };
    };
    _try(VecDestroy(&vec));
    _try(VecDestroy(&x));
  }
  _try(KSPDestroy(&ksp));
  _try(MatAssemblyBegin(*B,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(*B,MAT_FINAL_ASSEMBLY));
  PetscFunctionReturn(0);
};

#endif // HAVE_PETSC
