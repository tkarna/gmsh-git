#ifndef LAGRANGEINTERPOLATION_H_
#define LAGRANGEINTERPOLATION_H_
#include "polynomialInterpolation.h"

//---------------------------Interpolation by lagrange function-----------------------------

class lagrangeFunctionApproximation{
  std::vector<double> listValues;
  public:
    lagrangeFunctionApproximation(){};
    ~lagrangeFunctionApproximation(){};
    void setValues(double vals);
    void getFunctionValues(double s, fullVector<double> &aVals);
    void printValues();
    void reset();
};

class polynomialInterpolationByLagrangeFuction: public polynomialInterpolationBase2D{
  protected:
    lagrangeFunctionApproximation xLag, yLag;

  protected:
    void setLagrangeFunctionApproximation();
    void formCondition();
    void periodicCondition();

  public:
    polynomialInterpolationByLagrangeFuction(FunctionSpaceBase* sp, dofManager<double>* p, int deg);
    polynomialInterpolationByLagrangeFuction(const int tag, const int dim, dofManager<double>* _p,int deg);
    ~polynomialInterpolationByLagrangeFuction(){};

    virtual void applyPeriodicCondition();
};

class polynomialInterpolationByLagrangeFuction3D: public polynomialInterpolationBase3D{
  protected:
    lagrangeFunctionApproximation xLag, yLag, zLag;

  protected:
    void setLagrangeFunctionApproximation();
    void formCondition();
    void periodicCondition();
    void lineFormCondition(vertexContainer container, std::vector<MVertex*> vlist,
                           lagrangeFunctionApproximation lag, MVertex* vroot, MVertex* vtip);
    void linePeriodicCondition(vertexContainer container, std::vector<MVertex*> vlist,
                           lagrangeFunctionApproximation lag, MVertex* vroot, MVertex* vtip, MVertex* lroot, MVertex* ltip);

    void faceFormCondition(vertexContainer container, std::vector<MVertex*> xl, std::vector<MVertex*> yl,
                          lagrangeFunctionApproximation lagx, lagrangeFunctionApproximation lagy,
                          MVertex* v1, MVertex* v2, MVertex* v3, MVertex* v4);
    void facePeriodicCondition(vertexContainer container, std::vector<MVertex*> xl, std::vector<MVertex*> yl,
                          lagrangeFunctionApproximation lagx, lagrangeFunctionApproximation lagy,
                          MVertex* v1, MVertex* v2, MVertex* v3, MVertex* v4);

  public:
    polynomialInterpolationByLagrangeFuction3D(FunctionSpaceBase* sp, dofManager<double>* p, int deg);
    polynomialInterpolationByLagrangeFuction3D(const int tag, const int dim, dofManager<double>* _p,int deg);
    ~polynomialInterpolationByLagrangeFuction3D(){};

    virtual void applyPeriodicCondition();
};

#endif // LAGRANGEINTERPOLATION_H_
