#ifndef LINEARCONSTRAINTMATRIXPETSC_H_
#define LINEARCONSTRAINTMATRIXPETSC_H_

#include "linearConstraintMatrix.h"
#include "GmshConfig.h"

#if defined(HAVE_PETSC)
#include "petscksp.h"
#include "petscmat.h"
#endif

class linearConstraintMatrixPETSc : public linearConstraintMatrix{
	#if defined(HAVE_PETSC)
	protected:
		void _try(int ierr) { CHKERRABORT(_comm, ierr); };

	protected:
		Mat _firstMatrix, _secondMatrix , _firstOrderMatrix; // first matrix, second matrix, _firstorder matrix
		bool _isAllocated;  // allocated flag
		PetscInt _row, _firstCol, _secondCol; // respectively row, col of first matrix, col of second matrix
		MPI_Comm _comm; // MPI comunicator

	public:
		linearConstraintMatrixPETSc(MPI_Comm com = PETSC_COMM_SELF);
		virtual ~linearConstraintMatrixPETSc();
		virtual bool isAllocated() const;
		virtual void clear();
		virtual void allocate(int row, int first =0, int second=0);
		virtual void addToFirstMatrix(int i, int j, double val);
		virtual void addToSecondMatrix(int i, int j, double val);
		virtual void addToFirstOrderKinematicalMatrix(int i, int j, double val);

		Mat& getFirstMatrix();
		Mat& getSecondMatrix();
		Mat& getFirstOrderKinematicalMatrix();
	#else
	public:
		linearConstraintMatrixPETSc(){ Msg::Info("Petsc is required!");};
		virtual bool isAllocated() const { return false;};
		virtual void allocate(int nRows, int col1=0, int col2 = 0){};
		virtual void clear(){};
		virtual void addToFirstMatrix(int i, int j, double val) {};
		virtual void addToSecondMatrix(int i, int  j ,double val) {};
		virtual void addToFirstOrderKinematicalMatrix(int i, int j, double val) {};
	#endif
};
class linearConstraintMatrixSecondOrderPETSc : public linearConstraintMatrixPETSc,
                                          public linearConstraintMatrixSecondOrder{
  #if defined(HAVE_PETSC)
  protected:
    Mat _secondOrderMatrix;

  public:
    linearConstraintMatrixSecondOrderPETSc(MPI_Comm com = PETSC_COMM_SELF);
    virtual ~linearConstraintMatrixSecondOrderPETSc();
    virtual void clear();
		virtual void allocate(int row, int first=0, int second=0);
		virtual void addToSecondOrderKinematicalMatrix(int i, int j, double val);
		Mat& getSecondOrderKinematicalMatrix();
  #else
  public:
    linearConstraintMatrixSecondOrderPETSc(){Msg::Error("Petsc is required");};
    virtual void addToSecondOrderKinematicalMatrix(int i, int j, double val) {};
  #endif
};
#endif // LINEARCONSTRAINTMATRIXPETSC_H_
