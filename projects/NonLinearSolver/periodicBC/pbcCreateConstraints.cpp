
#include "pbcCreateConstraints.h"
#include "numericalFunctions.h"
#include "MLine.h"
#include "pbcSupplementConstraintHighOrder.h"
#include <string.h>
#include "MQuadrangle.h"

pbcConstraintElementGroup::pbcConstraintElementGroup(nonLinearMicroBC* bc, std::vector<partDomain*>& adm,
                            FunctionSpaceBase* lagspace, FunctionSpaceBase* multspace)
                               :_mbc(bc),_allDomain(adm), _v1(NULL),_v2(NULL),_v4(NULL),_v5(NULL),
                               _v3(NULL),_v6(NULL),_v7(NULL),_v8(NULL), _gXY(NULL),_gYZ(NULL),_gZX(NULL),
                               _gX(NULL),_gY(NULL),_gZ(NULL), _g(NULL),
                               _LagSpace(lagspace),_MultSpace(multspace){
  this->__init__();
};

pbcConstraintElementGroup::~pbcConstraintElementGroup(){
  for (std::set<constraintElement*>::iterator it = _allConstraint.begin(); it!= _allConstraint.end(); it++){
    constraintElement* cel = *it;
    if (cel) delete cel;
    cel = NULL;
  }
  _allConstraint.clear();
  _cVertices.clear();
  _bVertices.clear();
  for (std::set<MVertex*>::iterator it = _virtualVertices.begin(); it!= _virtualVertices.end(); it++){
    MVertex* v = *it;
    if (v) delete v;
    v = NULL;
  }
  _virtualVertices.clear();
  l12.clear();
  l23.clear();
  l34.clear();
  l41.clear();
  l56.clear();
  l67.clear();
  l78.clear();
  l85.clear();
  l15.clear();
  l26.clear();
  l37.clear();
  l48.clear();

  if (_g) delete _g; _g = NULL;
  if (_gX) delete _gX; _gX = NULL;
  if (_gY) delete _gY; _gY = NULL;
  if (_gZ) delete _gZ; _gZ = NULL;

  if (_gXY) delete _gXY; _gXY = NULL;
  if (_gYZ) delete _gYZ; _gYZ = NULL;
  if (_gZX) delete _gZX; _gZX = NULL;
};

void pbcConstraintElementGroup::__init__(){
  /**get all element within all domain in CG case**/
  _g = new groupOfElements();
  bool _fullDG = false;
  for (int idom = 0; idom< _allDomain.size(); idom++){
    partDomain* dom = _allDomain[idom];
    if (dom->getFormulation()){
      _fullDG = true;
      break;
    }
  }
  if (!_fullDG){
    for (int idom = 0; idom< _allDomain.size(); idom++){
      partDomain* dom = _allDomain[idom];
      for (groupOfElements::elementContainer::iterator it = dom->g->begin(); it!= dom->g->end(); it++){
        _g->insert(*it);
      }
    }
  }
  else
    Msg::Fatal("The micro solver is not valid for DG domain");

  if (_g->size()==0)
    Msg::Fatal("Error in setting domain");


  nonLinearPeriodicBC* pbc = dynamic_cast<nonLinearPeriodicBC*>(_mbc);
  const std::vector<groupOfElements*>& bgroup = _mbc->getBoundaryGroupOfElements();
  int dim = _mbc->getDim();
  if (pbc != NULL && bgroup.size()>0){
		if (dim ==2){
			groupIntersection(bgroup[0],bgroup[1],_bVertices);
			groupIntersection(bgroup[1],bgroup[2],_bVertices);
			groupIntersection(bgroup[2],bgroup[3],_bVertices);
			groupIntersection(bgroup[3],bgroup[0],_bVertices);

			l12.insert(bgroup[0]->vbegin(),bgroup[0]->vend());
			l23.insert(bgroup[1]->vbegin(),bgroup[1]->vend());
			l34.insert(bgroup[2]->vbegin(),bgroup[2]->vend());
			l41.insert(bgroup[3]->vbegin(),bgroup[3]->vend());

			for (std::set<MVertex*>::iterator it = _bVertices.begin(); it!= _bVertices.end(); it++){
        _cVertices.insert(*it);
			}
		}
		else if (dim ==3){
		  f1234 = bgroup[0];
		  f5678 = bgroup[3];
		  f1562 = bgroup[1];
		  f4873 = bgroup[4];
		  f1584 = bgroup[2];
		  f2673 = bgroup[5];

		  groupIntersection(f1234,f1562,l12);
		  for (std::set<MVertex*>::iterator it = l12.begin(); it!= l12.end(); it++){
        _bVertices.insert(*it);
        #ifdef _DEBUG
        Msg::Info("vertex %d 12",(*it)->getNum());
        #endif //_DEBUG
		  };
		  groupIntersection(f1234,f2673,l23);
		  for (std::set<MVertex*>::iterator it = l23.begin(); it!= l23.end(); it++){
        _bVertices.insert(*it);
        #ifdef _DEBUG
        Msg::Info("vertex %d 23",(*it)->getNum());
        #endif //_DEBUG
		  };

		  groupIntersection(f1234,f4873,l34);
		  for (std::set<MVertex*>::iterator it = l34.begin(); it!= l34.end(); it++){
        _bVertices.insert(*it);
        #ifdef _DEBUG
        Msg::Info("vertex %d 34",(*it)->getNum());
        #endif //_DEBUG
		  };

		  groupIntersection(f1234,f1584,l41);
		  for (std::set<MVertex*>::iterator it = l41.begin(); it!= l41.end(); it++){
        _bVertices.insert(*it);
        #ifdef _DEBUG
        Msg::Info("vertex %d 41",(*it)->getNum());
        #endif //_DEBUG
		  };

		  groupIntersection(f5678,f1562,l56);
		  for (std::set<MVertex*>::iterator it = l56.begin(); it!= l56.end(); it++){
        _bVertices.insert(*it);
        #ifdef _DEBUG
        Msg::Info("vertex %d 56",(*it)->getNum());
        #endif //_DEBUG
		  };

      groupIntersection(f5678,f2673,l67);
		  for (std::set<MVertex*>::iterator it = l67.begin(); it!= l67.end(); it++){
        _bVertices.insert(*it);
        #ifdef _DEBUG
        Msg::Info("vertex %d 67",(*it)->getNum());
        #endif //_DEBUG
		  };

		  groupIntersection(f5678,f4873,l78);
		  for (std::set<MVertex*>::iterator it = l78.begin(); it!= l78.end(); it++){
        _bVertices.insert(*it);
        #ifdef _DEBUG
        Msg::Info("vertex %d 78",(*it)->getNum());
        #endif //_DEBUG
		  };

		  groupIntersection(f5678,f1584,l85);
		  for (std::set<MVertex*>::iterator it = l85.begin(); it!= l85.end(); it++){
        _bVertices.insert(*it);
        #ifdef _DEBUG
        Msg::Info("vertex %d 85",(*it)->getNum());
        #endif //_DEBUG
		  };

      groupIntersection(f1562,f1584,l15);
		  for (std::set<MVertex*>::iterator it = l15.begin(); it!= l15.end(); it++){
        _bVertices.insert(*it);
        #ifdef _DEBUG
        Msg::Info("vertex %d 15",(*it)->getNum());
        #endif //_DEBUG
		  };

		  groupIntersection(f1562,f2673,l26);
		  for (std::set<MVertex*>::iterator it = l26.begin(); it!= l26.end(); it++){
        _bVertices.insert(*it);
        #ifdef _DEBUG
        Msg::Info("vertex %d 26",(*it)->getNum());
        #endif //_DEBUG
		  };

		  groupIntersection(f2673,f4873,l37);
		  for (std::set<MVertex*>::iterator it = l37.begin(); it!= l37.end(); it++){
        _bVertices.insert(*it);
        #ifdef _DEBUG
        Msg::Info("vertex %d 37",(*it)->getNum());
        #endif //_DEBUG
		  };

		  groupIntersection(f4873,f1584,l48);
		  for (std::set<MVertex*>::iterator it = l48.begin(); it!= l48.end(); it++){
        _bVertices.insert(*it);
        #ifdef _DEBUG
        Msg::Info("vertex %d 48",(*it)->getNum());
        #endif //_DEBUG
		  };

			groupIntersection(l12,l23,_cVertices);
			groupIntersection(l23,l34,_cVertices);
			groupIntersection(l34,l41,_cVertices);
			groupIntersection(l41,l12,_cVertices);
			groupIntersection(l56,l67,_cVertices);
			groupIntersection(l67,l78,_cVertices);
			groupIntersection(l78,l85,_cVertices);
			groupIntersection(l85,l56,_cVertices);
		};
		#ifdef _DEBUG
		for (std::set<MVertex*>::iterator it = _bVertices.begin(); it!= _bVertices.end(); it++){
      MVertex* v = *it;
      Msg::Info("Boundary vertex = %d",v->getNum());
    }
    for (std::set<MVertex*>::iterator it = _cVertices.begin(); it!= _cVertices.end(); it++){
      MVertex* v = *it;
      Msg::Info("Corner vertex = %d",v->getNum());
    }
    #endif //_DEBUG
  }
};



void pbcConstraintElementGroup::addControlPoints(){
  std::set<MVertex*> vv;
  groupIntersection(l12,l41,vv);
  if (_mbc->getDim() == 3){
    groupIntersection(l12,l15,vv);
    groupIntersection(l41,l15,vv);
  }
  if (vv.size()==0){
    MVertex* v1(NULL),*v2(NULL),*v3(NULL);
    if (l12.size()>0 and l41.size()>2 ){
      v1 = *(l12.begin());
      std::set<MVertex*>::iterator it = l41.begin();
      v2 = *(it);
      it++;
      v3 = *(it);
    }
    if (v1!=NULL and v2!=NULL and v3!=NULL){
      SPoint3 pp= project(v1->point(),v2->point(),v3->point());
      _v1  = new MVertex(pp.x(),pp.y(),pp.z());
      Msg::Info("Create root vertex v1 %d: %e %e %e",_v1->getNum(),pp.x(),pp.y(),pp.z());
      l12.insert(_v1);
      l41.insert(_v1);
      if (_mbc->getDim()==3){
        l15.insert(_v1);
      }
      _virtualVertices.insert(_v1);
      _cVertices.insert(_v1);
      _bVertices.insert(_v1);
    }
  }
  else {
    _v1 = *(vv.begin());
  }

  vv.clear();
  groupIntersection(l12,l23,vv);
  if (_mbc->getDim()==3){
    groupIntersection(l12,l26,vv);
    groupIntersection(l23,l26,vv);
  }
  if (vv.size()==0){
    MVertex* v1(NULL),*v2(NULL),*v3(NULL);
    if (l12.size()>0 and l23.size()>2){
      v1 = *(l12.begin());
      std::set<MVertex*>::iterator it = l23.begin();
      v2 = *(it);
      it++;
      v3 = *(it);
    }
    if (v1!=NULL and v2!=NULL and v3!=NULL){
      SPoint3 pp= project(v1->point(),v2->point(),v3->point());
      _v2  = new MVertex(pp.x(),pp.y(),pp.z());
      Msg::Info("Create root vertex v2 %d: %e %e %e",_v2->getNum(),pp.x(),pp.y(),pp.z());
      l12.insert(_v2);
      _virtualVertices.insert(_v2);
      _cVertices.insert(_v2);
      _bVertices.insert(_v2);
    }
  }
  else {
    _v2 = *(vv.begin());
  }

  vv.clear();
  groupIntersection(l34,l41,vv);
  if (_mbc->getDim()==3){
    groupIntersection(l34,l48,vv);
    groupIntersection(l41,l48,vv);
  }
  if (vv.size()==0){
    MVertex* v1(NULL),*v2(NULL),*v3(NULL);
    if (l41.size()>0 and l34.size()>2){
      v1 = *(l41.begin());
      std::set<MVertex*>::iterator it = l34.begin();
      v2 = *(it);
      it++;
      v3 = *(it);
    }
    if (v1!=NULL and v2!=NULL and v3!=NULL){
      SPoint3 pp= project(v1->point(),v2->point(),v3->point());
      _v4  = new MVertex(pp.x(),pp.y(),pp.z());
      Msg::Info("Create root vertex v4 %d: %e %e %e",_v4->getNum(),pp.x(),pp.y(),pp.z());
      l41.insert(_v4);
      _virtualVertices.insert(_v4);
      _cVertices.insert(_v4);
      _bVertices.insert(_v4);
    }
  }
  else {
    _v4 = *(vv.begin());
  }
  if (_mbc->getDim()==3){
    vv.clear();
    groupIntersection(l85,l15,vv);
    groupIntersection(l85,l56,vv);
    groupIntersection(l56,l15,vv);

    if (vv.size()==0){
      MVertex* v1(NULL),*v2(NULL),*v3(NULL);
      if (l15.size()>0 and l85.size()>2){
        v1 = *(l15.begin());
        std::set<MVertex*>::iterator it = l85.begin();
        v2 = *(it);
        it++;
        v3 = *(it);
      }

      if (v1!=NULL and v2!=NULL and v3!=NULL){
        SPoint3 pp= project(v1->point(),v2->point(),v3->point());
        _v5  = new MVertex(pp.x(),pp.y(),pp.z());
        Msg::Info("Create root vertex v5 %d: %e %e %e",_v5->getNum(),pp.x(),pp.y(),pp.z());
        l15.insert(_v5);
        _virtualVertices.insert(_v5);
        _cVertices.insert(_v5);
        _bVertices.insert(_v5);
      }

    }
    else {
      _v5 = *(vv.begin());
    }

    if (_v1==NULL){
      MElement* e1 = *(f1234->begin());
      MElement* e2 = *(f1584->begin());
      MElement* e3 = *(f1562->begin());
      fullMatrix<double> m(3,3); m.setAll(0.);
      fullVector<double> b(3); b.setAll(0.);

      std::vector<MVertex*> vv1, vv2, vv3;
      e1->getVertices(vv1);
      e2->getVertices(vv2);
      e3->getVertices(vv3);

      computeSurface(vv1[0]->point(),vv1[1]->point(),vv1[2]->point(),m(0,0),m(0,1),m(0,2),b(0));
      computeSurface(vv2[0]->point(),vv2[1]->point(),vv2[2]->point(),m(1,0),m(1,1),m(1,2),b(1));
      computeSurface(vv3[0]->point(),vv3[1]->point(),vv3[2]->point(),m(2,0),m(2,1),m(2,2),b(2));

      fullMatrix<double> invm(3,3);
      m.invert(invm);
      fullVector<double> f(3);
      invm.mult(b,f);

      _v1  = new MVertex(f(0),f(1),f(2));
      Msg::Info("Create root vertex v1 %d: %e %e %e",_v1->getNum(),f(0),f(1),f(2));
      _virtualVertices.insert(_v1);
      _cVertices.insert(_v1);
      _bVertices.insert(_v1);
    }

    if (_v2==NULL){
      MElement* e1 = *(f1234->begin());
      MElement* e2 = *(f2673->begin());
      MElement* e3 = *(f1562->begin());
      fullMatrix<double> m(3,3); m.setAll(0.);
      fullVector<double> b(3); b.setAll(0.);

      std::vector<MVertex*> vv1, vv2, vv3;
      e1->getVertices(vv1);
      e2->getVertices(vv2);
      e3->getVertices(vv3);

      computeSurface(vv1[0]->point(),vv1[1]->point(),vv1[2]->point(),m(0,0),m(0,1),m(0,2),b(0));
      computeSurface(vv2[0]->point(),vv2[1]->point(),vv2[2]->point(),m(1,0),m(1,1),m(1,2),b(1));
      computeSurface(vv3[0]->point(),vv3[1]->point(),vv3[2]->point(),m(2,0),m(2,1),m(2,2),b(2));

      fullMatrix<double> invm(3,3);
      m.invert(invm);
      fullVector<double> f(3);
      invm.mult(b,f);

      _v2  = new MVertex(f(0),f(1),f(2));
      Msg::Info("Create root vertex v2 %d: %e %e %e",_v2->getNum(),f(0),f(1),f(2));
      _virtualVertices.insert(_v2);
      _cVertices.insert(_v2);
      _bVertices.insert(_v2);
    }

    if (_v4==NULL){
      MElement* e1 = *(f1234->begin());
      MElement* e2 = *(f1584->begin());
      MElement* e3 = *(f4873->begin());
      fullMatrix<double> m(3,3); m.setAll(0.);
      fullVector<double> b(3); b.setAll(0.);

      std::vector<MVertex*> vv1, vv2, vv3;
      e1->getVertices(vv1);
      e2->getVertices(vv2);
      e3->getVertices(vv3);

      computeSurface(vv1[0]->point(),vv1[1]->point(),vv1[2]->point(),m(0,0),m(0,1),m(0,2),b(0));
      computeSurface(vv2[0]->point(),vv2[1]->point(),vv2[2]->point(),m(1,0),m(1,1),m(1,2),b(1));
      computeSurface(vv3[0]->point(),vv3[1]->point(),vv3[2]->point(),m(2,0),m(2,1),m(2,2),b(2));

      fullMatrix<double> invm(3,3);
      m.invert(invm);
      fullVector<double> f(3);
      invm.mult(b,f);

      _v4  = new MVertex(f(0),f(1),f(2));
      Msg::Info("Create root vertex v4 %d: %e %e %e",_v4->getNum(),f(0),f(1),f(2));
      _virtualVertices.insert(_v4);
      _cVertices.insert(_v4);
      _bVertices.insert(_v4);
    }

    if (_v5==NULL){
      MElement* e1 = *(f1584->begin());
      MElement* e2 = *(f5678->begin());
      MElement* e3 = *(f1562->begin());
      fullMatrix<double> m(3,3); m.setAll(0.);
      fullVector<double> b(3); b.setAll(0.);

      std::vector<MVertex*> vv1, vv2, vv3;
      e1->getVertices(vv1);
      e2->getVertices(vv2);
      e3->getVertices(vv3);

      computeSurface(vv1[0]->point(),vv1[1]->point(),vv1[2]->point(),m(0,0),m(0,1),m(0,2),b(0));
      computeSurface(vv2[0]->point(),vv2[1]->point(),vv2[2]->point(),m(1,0),m(1,1),m(1,2),b(1));
      computeSurface(vv3[0]->point(),vv3[1]->point(),vv3[2]->point(),m(2,0),m(2,1),m(2,2),b(2));

      fullMatrix<double> invm(3,3);
      m.invert(invm);
      fullVector<double> f(3);
      invm.mult(b,f);

      _v5  = new MVertex(f(0),f(1),f(2));
      Msg::Info("Create root vertex v5 %d: %e %e %e",_v5->getNum(),f(0),f(1),f(2));
      _virtualVertices.insert(_v5);
      _cVertices.insert(_v5);
      _bVertices.insert(_v5);
    }

  }
};

void pbcConstraintElementGroup::addOtherCornerPoints(){
  std::set<MVertex*> vv;
  // find _v3
  vv.clear();
  groupIntersection(l23,l34,vv);
  if (vv.size()==0)
  groupIntersection(l34,l37,vv);
  if (vv.size() ==0)
  groupIntersection(l23,l37,vv);

  if (vv.size()==0){
    double x = _v4->x() + _v2->x() - _v1->x();
    double y = _v4->y() + _v2->y() - _v1->y();
    double z = _v4->z() + _v2->z() - _v1->z();
   _v3  = new MVertex(x,y,z);
    Msg::Info("Create root vertex v3 %d: %e %e %e",_v3->getNum(),x,y,z);
    l23.insert(_v3);
    l34.insert(_v3);
    l37.insert(_v3);
    _virtualVertices.insert(_v3);
    _cVertices.insert(_v3);
    _bVertices.insert(_v3);
  }
  else {
    _v3 = *(vv.begin());
  }

  // find _v6
  vv.clear();
  groupIntersection(l26,l56,vv);
  if (vv.size() == 0)
    groupIntersection(l56,l67,vv);

  if (vv.size() == 0)
    groupIntersection(l26,l67,vv);

  if (vv.size()==0){
    double x = _v2->x() + _v5->x() - _v1->x();
    double y = _v2->y() + _v5->y() - _v1->y();
    double z = _v2->z() + _v5->z() - _v1->z();
   _v6  = new MVertex(x,y,z);
    Msg::Info("Create root vertex v6 %d: %e %e %e",_v6->getNum(),x,y,z);
    l26.insert(_v6);
    l56.insert(_v6);
    l67.insert(_v6);
    _virtualVertices.insert(_v6);
    _cVertices.insert(_v6);
    _bVertices.insert(_v6);
  }
  else {
    _v6 = *(vv.begin());
  }

   // find _v8
  vv.clear();
  groupIntersection(l85,l48,vv);
  if (vv.size() == 0)
    groupIntersection(l48,l78,vv);

  if (vv.size() == 0)
    groupIntersection(l85,l78,vv);

  if (vv.size()==0){
    double x = _v4->x() + _v5->x() - _v1->x();
    double y = _v4->y() + _v5->y() - _v1->y();
    double z = _v4->z() + _v5->z() - _v1->z();
   _v8  = new MVertex(x,y,z);
    Msg::Info("Create root vertex v8 %d: %e %e %e",_v8->getNum(),x,y,z);
    l48.insert(_v8);
    l78.insert(_v8);
    l85.insert(_v8);
    _virtualVertices.insert(_v8);
    _cVertices.insert(_v8);
    _bVertices.insert(_v8);
  }
  else {
    _v8 = *(vv.begin());
  }

   // find _v7
  vv.clear();
  groupIntersection(l78,l37,vv);
  if (vv.size() == 0)
    groupIntersection(l37,l67,vv);

  if (vv.size() == 0)
    groupIntersection(l78,l67,vv);

  if (vv.size()==0){
    double x = _v3->x() + _v5->x() - _v1->x();
    double y = _v3->y() + _v5->y() - _v1->y();
    double z = _v3->z() + _v5->z() - _v1->z();
   _v7  = new MVertex(x,y,z);
    Msg::Info("Create root vertex v7 %d: %e %e %e",_v7->getNum(),x,y,z);
    l78.insert(_v7);
    l37.insert(_v7);
    l67.insert(_v7);
    _virtualVertices.insert(_v7);
    _cVertices.insert(_v7);
    _bVertices.insert(_v7);
  }
  else {
    _v7 = *(vv.begin());
  }
};

void pbcConstraintElementGroup::createPolynomialBasePoints(){
  int dim = _mbc->getDim();
  nonLinearPeriodicBC* pbc = static_cast<nonLinearPeriodicBC*>(_mbc);
  int degreeX = pbc->getPBCPolynomialDegree(0);
  int degreeY = pbc->getPBCPolynomialDegree(1);;
  int degreeZ = pbc->getPBCPolynomialDegree(2);;

  int sizeX = l12.size();
  int sizeY = l41.size();
  int sizeZ = 0;
  if (dim == 3){
    sizeX = (int) sqrt(f1234->vsize());
    sizeY = (int) sqrt(f1584->vsize());
    sizeZ = (int) sqrt(f1562->vsize());
  }
  Msg::Info("Size degree used in X direction %d, Y direction %d, Z direction %d",sizeX,sizeY,sizeZ);
  if (pbc->getPBCMethod() == nonLinearPeriodicBC::LIM or
      pbc->getPBCMethod() == nonLinearPeriodicBC::FE_LIN or
      pbc->getPBCMethod() == nonLinearPeriodicBC::FE_QUA){
    if (degreeX>sizeX-1) degreeX = sizeX-1;
    if (degreeY>sizeY-1) degreeY = sizeY-1;
    if (degreeZ>sizeZ-1 && dim == 3) degreeZ = sizeZ-1;
  }
  else if (pbc->getPBCMethod() == nonLinearPeriodicBC::CSIM){
    if (degreeX>sizeX/3-1) degreeX = sizeX/3-1;
    if (degreeY>sizeY/3-1) degreeY = sizeY/3-1;
    if (degreeZ>sizeZ/3-1 && dim == 3) degreeZ = sizeZ/3-1;
  }
  else{
    Msg::Fatal("interpolation type must be defined");
  }

  if (degreeX<1) degreeX = 1;
  if (degreeY<1) degreeY = 1;
  if (degreeZ<1) degreeZ = 1;

  pbc->setPBCPolynomialDegree(0,degreeX);
  pbc->setPBCPolynomialDegree(1,degreeY);
  pbc->setPBCPolynomialDegree(2,degreeZ);
  // add root points if necessary
  this->addControlPoints();

  Msg::Info("Interpolation degree used in X direction %d, Y direction %d, Z direction %d",degreeX,degreeY,degreeZ);
  bool ok = false;
  if (pbc->getPBCMethod() == nonLinearPeriodicBC::FE_LIN or
      pbc->getPBCMethod() == nonLinearPeriodicBC::FE_QUA){
    ok = true;
  }
  if (pbc->addNewVertices(0) or l12.size()<=2 or ok){
    this->createNewVerticesForInterpolationPoints(degreeX,_v1,_v2,_xBase,_cVertices);
  }
  else
    this->getInterpolatioPointsFromExistingVertices(degreeX,_v1,_v2,l12.begin(), l12.end(),_xBase);


  if (pbc->addNewVertices(1) or l41.size()<=2 or ok){
    this->createNewVerticesForInterpolationPoints(degreeY,_v1,_v4,_yBase,_cVertices);
  }
  else
    this->getInterpolatioPointsFromExistingVertices(degreeY,_v1,_v4,l41.begin(), l41.end(),_yBase);

  if (dim == 3){
    if (pbc->addNewVertices(2) or l15.size()<=2 or ok){
      this->createNewVerticesForInterpolationPoints(degreeZ,_v1,_v5,_zBase,_cVertices);
    }
    else
      this->getInterpolatioPointsFromExistingVertices(degreeZ,_v1,_v5,l15.begin(), l15.end(),_zBase);
  }
};

void pbcConstraintElementGroup::createBCGroup3D( std::vector<MVertex*>& vlx, std::vector<MVertex*>& vly,
        MVertex* vc, groupOfElements* g){
  if (vlx[0] != vly[0]){
    Msg::Fatal("two BC group must be started by same vertex");
    return;
  }
  MVertex* vroot = vlx[0];
  nonLinearPeriodicBC* pbc = dynamic_cast<nonLinearPeriodicBC*>(_mbc);
  if (pbc == NULL) return;
  int NX = vlx.size();
  int NY = vly.size();

  std::vector<std::vector<MVertex*> > vmat;
  vmat.push_back(vlx);

  for (int j=1; j<NY; j++){
    std::vector<MVertex*> vv;
    vv.push_back(vly[j]);
    for (int i=1; i< NX; i++){
      MVertex* vertex = NULL;
      if (j == NY-1 && i == NX-1){
        vertex = vc;
      }
      else{
       vertex = new MVertex(0,0,0);
       vertex->x() = vlx[i]->x() - vroot->x()+ vly[j]->x();
       vertex->y() = vlx[i]->y() - vroot->y()+ vly[j]->y();
       vertex->z() = vlx[i]->z() - vroot->z()+ vly[j]->z();
       _virtualVertices.insert(vertex);
      }
      vv.push_back(vertex);
    }
    vmat.push_back(vv);
  }


  if (pbc->getPBCMethod() == nonLinearPeriodicBC::FE_LIN){
    for (int i=0; i<NX-1; i++){
      for (int j=0; j<NY-1; j++){
        MElement* e = new MQuadrangle(vmat[j][i],vmat[j][i+1],vmat[j+1][i+1],vmat[j+1][i]);
        g->insert(e);
      }
    }
  }
  else if (pbc->getPBCMethod() == nonLinearPeriodicBC::FE_QUA){
    for (int i=0; i<NX-1; i+=2){
      for (int j=0; j<NY-1; j+=2){
        MElement* e = new MQuadrangle9(vmat[j][i],vmat[j][i+2],vmat[j+2][i+2],vmat[j+2][i],
                                      vmat[j][i+1],vmat[j+1][i+2],vmat[j+2][i+1],vmat[j+1][i],
                                      vmat[j+1][i+1]);
        g->insert(e);
      }
    }
  }
};

void pbcConstraintElementGroup::createAllBCGroups(){
  int dim = _mbc->getDim();
  _gX = new groupOfElements();
  _gY = new groupOfElements();

  nonLinearPeriodicBC* pbc = dynamic_cast<nonLinearPeriodicBC*>(_mbc);
  if (pbc->getPBCMethod() == nonLinearPeriodicBC::FE_LIN){
    for (int i=0; i<_xBase.size()-1; i++){
      MElement* line = new MLine(_xBase[i],_xBase[i+1]);
      _gX->insert(line);
    }

    for (int i=0; i<_yBase.size()-1; i++){
      MElement* line = new MLine(_yBase[i],_yBase[i+1]);
      _gY->insert(line);
    }

    if (dim == 3){
      _gZ = new groupOfElements();
      for (int i=0; i<_zBase.size()-1; i++){
        MElement* line = new MLine(_zBase[i],_zBase[i+1]);
        _gZ->insert(line);
      }
    }
  }
  else if (pbc->getPBCMethod() == nonLinearPeriodicBC::FE_QUA){
    std::vector<MVertex*> xtemp(_xBase);
    _xBase.clear();
    for (int i=0; i<xtemp.size()-1; i++){
      _xBase.push_back(xtemp[i]);
      MVertex* v = new MVertex(0.5*(xtemp[i+1]->x()+xtemp[i]->x()),
                              0.5*(xtemp[i+1]->y()+xtemp[i]->y()),
                              0.5*(xtemp[i+1]->z()+xtemp[i]->z()));

      _virtualVertices.insert(v);
      _bVertices.insert(v);
      _xBase.push_back(v);

      MElement* line = new MLine3(xtemp[i],xtemp[i+1],v);
      _gX->insert(line);
      if (i == xtemp.size()-2){
        _xBase.push_back(xtemp[i+1]);
      }
    }

    std::vector<MVertex*> ytemp(_yBase);
    _yBase.clear();
    for (int i=0; i<ytemp.size()-1; i++){
      _yBase.push_back(ytemp[i]);
      MVertex* v = new MVertex(0.5*(ytemp[i+1]->x()+ytemp[i]->x()),
                              0.5*(ytemp[i+1]->y()+ytemp[i]->y()),
                              0.5*(ytemp[i+1]->z()+ytemp[i]->z()));

      _virtualVertices.insert(v);
      _bVertices.insert(v);
      _yBase.push_back(v);

      MElement* line = new MLine3(ytemp[i],ytemp[i+1],v);
      _gY->insert(line);
       if (i == ytemp.size()-2){
        _yBase.push_back(ytemp[i+1]);
      }
    }

    if (dim== 3){
      std::vector<MVertex*> ztemp(_zBase);
      _zBase.clear();
      _gZ = new groupOfElements();
      for (int i=0; i<ztemp.size()-1; i++){
        _zBase.push_back(ztemp[i]);
        MVertex* v = new MVertex(0.5*(ztemp[i+1]->x()+ztemp[i]->x()),
                                0.5*(ztemp[i+1]->y()+ztemp[i]->y()),
                                0.5*(ztemp[i+1]->z()+ztemp[i]->z()));

        _virtualVertices.insert(v);
        _bVertices.insert(v);
        _zBase.push_back(v);

        MElement* line = new MLine3(ztemp[i],ztemp[i+1],v);
        _gZ->insert(line);
        if (i == ztemp.size()-2){
          _zBase.push_back(ztemp[i+1]);
        }
      }

    }
  };

  if (dim == 3){
    _gXY = new groupOfElements();
    _gZX = new groupOfElements();
    _gYZ = new groupOfElements();

     this->addOtherCornerPoints();

    this->createBCGroup3D(_xBase,_yBase,_v3,_gXY);
    this->createBCGroup3D(_yBase,_zBase,_v8,_gYZ);
    this->createBCGroup3D(_zBase,_xBase,_v6,_gZX);
  }
};

template<class Iterator>
MVertex* pbcConstraintElementGroup::findNearestVertex(Iterator itbegin, Iterator itend, MVertex* vp){
  MVertex* vinit = *itbegin;
  double dist=vinit->distance(vp);
  for (Iterator it=itbegin; it!=itend; it++){
    MVertex* v=*it;
    double tempDist=v->distance(vp);
    if (tempDist<dist) {
      dist=tempDist;
      vinit=v;
    };
  };
  return vinit;
};

MVertex* pbcConstraintElementGroup::findNearestVertex(groupOfElements* g, MVertex* vp){
  /*
  groupOfElements::vertexContainer::iterator it = g->vbegin();
	MVertex* vinit = *it;
  double dist=vinit->distance(vp);
  for (groupOfElements::vertexContainer::iterator it=g->vbegin(); it!=g->vend(); it++){
    MVertex* v=*it;
    double tempDist=v->distance(vp);
    if (tempDist<dist) {
      dist=tempDist;
      vinit=v;
    };
  };
  return vinit;
  */
  return findNearestVertex(g->vbegin(), g->vend(),vp);
};

template<class Iterator>
void pbcConstraintElementGroup::periodicConditionForPeriodicMesh(Iterator posivtiveitbegin, Iterator positiveitend,
                                               Iterator negativeitbegin, Iterator negativeitend,
                                               std::set<MVertex*>& others){
  int dim = _mbc->getDim();
  for (Iterator itp = posivtiveitbegin; itp!= positiveitend; itp++){
    MVertex* v = *itp;
    if ( others.find(v) == others.end()){
      MVertex* vn = findNearestVertex(negativeitbegin,negativeitend,v);
      if (_mbc->getOrder() ==1){
        constraintElement* cel = new periodicMeshConstraint(_LagSpace,_MultSpace,dim,v,vn);
        _allConstraint.insert(cel);
      }
      else if(_mbc->getOrder()==2){
        constraintElement* cel = new periodicMeshConstraintSecondOrder(_LagSpace,_MultSpace,dim,v,vn);
        _allConstraint.insert(cel);
      }
    };
  }
};

void pbcConstraintElementGroup::createNewVerticesForInterpolationPoints(int degree, MVertex* vinit, MVertex* vend,
                                                                        std::vector<MVertex*>& base,
                                                                        std::set<MVertex*>& others){
  base.clear();
  double x0 = vinit->x(); double x1 = vend->x();
  double y0 = vinit->y(); double y1 = vend->y();
  double z0 = vinit->z(); double z1 = vend->z();
  if (others.find(vinit) == others.end()){
    MVertex* v = new MVertex(x0,y0,z0);
    base.push_back(v);
    _virtualVertices.insert(v);
  }
  else{
    base.push_back(vinit);
  };
  double dx = (x1-x0)/degree;
  double dy = (y1-y0)/degree;
  double dz = (z1-z0)/degree;
  for (int j=0; j<degree-1; j++){
    double xi = x0+ (j+1)*dx;
    double yi = y0+ (j+1)*dy;
    double zi = z0+ (j+1)*dz;
    MVertex* v = new MVertex(xi,yi,zi);
    base.push_back(v);
    _virtualVertices.insert(v);
  };

  if (others.find(vend) == others.end()){
    MVertex* v = new MVertex(x1,y1,z1);
    base.push_back(v);
    _virtualVertices.insert(v);
  }
  else{
    base.push_back(vend);
  };
};

template<class Iterator>
void pbcConstraintElementGroup::getInterpolatioPointsFromExistingVertices(int degree, MVertex* v1, MVertex* v2,
                                                   Iterator itbegin, Iterator itend,
                                                   std::vector<MVertex*>& base){
  base.clear();
  std::vector<MVertex*> list;
  std::vector<double> vals;
  for (Iterator it=itbegin; it!=itend; it++){
    MVertex* v=*it;
    list.push_back(v);
    vals.push_back(distance(v->point(),v1->point(),v2->point()));
  };

  int size=list.size();
  for (int i=0; i<size; i++){
    for (int j=i; j<size; j++){
      if (vals[j]<vals[i]){
        double temp=vals[i]; vals[i]=vals[j]; vals[j]=temp;
        MVertex* vtemp=list[i]; list[i]=list[j]; list[j]=vtemp;
      };
    };
  };
  if (degree>size-1){
    degree=size-1;
    std::cout<<"Modify the imposed degree: "<<degree<<std::endl;
  };
  base.push_back(list[0]);
  for (int j=0; j<degree-1; j++){
    base.push_back(list[(j+1)*(size-1)/degree]);
  };
  base.push_back(list[size-1]);
};

// for cubic spline
template<class Iterator>
void pbcConstraintElementGroup::cubicSplineFormCondition(Iterator itbegin, Iterator itend,
                                                     std::vector<MVertex*>& vlist, int fl){
  int size = vlist.size();
  int dim = _mbc->getDim();
  for (Iterator it = itbegin; it!= itend;it++){
    MVertex* v = *it;
    bool flag = true;
    // check if this vertex lies on the interpolation groups
    for (int i=0; i<size; i++){
      if (v == vlist[i]) {
        flag = false;
        break;
      }
    };
    // do nothing if this vertex lies on the interpolation groups
    if (flag){
      MVertex* vleft(NULL), *vright(NULL);
      // find segment contains vertex
      for (int j=0; j<size-1; j++){
        if (inside(v->point(),vlist[j]->point(),vlist[j+1]->point())){
          vleft = vlist[j];
          vright = vlist[j+1];
          break;
        };
      };
      if (_mbc->getOrder()==1){
        constraintElement* cele = new cubicSplineConstraintElement(_LagSpace,_MultSpace,dim,v,vleft,vright,fl);
        _allConstraint.insert(cele);
      }
      else if (_mbc->getOrder()==2){
        constraintElement* cele = new cubicSplineConstraintElementSecondOrder(_LagSpace,_MultSpace,dim,v,vleft,vright,fl);
        _allConstraint.insert(cele);
      }
    };
  };
};
template<class Iterator>
void pbcConstraintElementGroup::cubicSplinePeriodicCondition(Iterator itbegin, Iterator itend,
                                                         std::vector<MVertex*>& vlist, int fl,
                                                         std::set<MVertex*>& others){
  int size = vlist.size();
  int dim = _mbc->getDim();
  MVertex* v1 = vlist[0];
  MVertex* v2 = vlist[size-1];
  for (Iterator it = itbegin; it != itend; it++){
    MVertex* v= *it;
    // because of formulation, particular constraint will be applied on corner vertices
    if (others.find(v) == others.end()){
      // find segment contains vertex
      MVertex* vleft(NULL), *vright(NULL);
      SPoint3 pt=project(v->point(),v1->point(),v2->point());
      for (int j=0; j<size-1; j++){
        if (inside(pt,vlist[j]->point(),vlist[j+1]->point())){
          vleft = vlist[j];
          vright = vlist[j+1];
          break;
        };
      };
      // if found segment
      if ((vleft != NULL) and (vright!= NULL)){
        if (_mbc->getOrder()==1){
          constraintElement* cele = new cubicSplineConstraintElement(_LagSpace,_MultSpace,dim,v,vleft,vright,fl);
          _allConstraint.insert(cele);
        }
        else if(_mbc->getOrder()==2){
          constraintElement* cele = new cubicSplineConstraintElementSecondOrder(_LagSpace,_MultSpace,dim,v,vleft,vright,fl);
          _allConstraint.insert(cele);
        }
      }
      else{
        Msg::Error("Segment not found");
      }
    };
  };
};

template<class Iterator>
void pbcConstraintElementGroup::cubicSplineFormConditionCoonsPatch(Iterator itbegin, Iterator itend, MVertex* vref,
                                                         std::vector<MVertex*>& xlist, std::vector<MVertex*>& ylist,
                                                         int flagx, int flagy, std::set<MVertex*>& others){

  int sizex = xlist.size();
  int sizey = ylist.size();

  MVertex* v1x = xlist[0];
  MVertex* v2x = xlist[sizex-1];
  MVertex* v1y = ylist[0];
  MVertex* v2y = ylist[sizey-1];

  int dim = _mbc->getDim();
  for (Iterator it = itbegin; it!= itend;it++){
    MVertex* v = *it;
    // particular constraint for edge vertices, we check for inner vertices
    if (others.find(v)==others.end()){
      MVertex* vleftx(NULL), *vrightx(NULL),* vlefty(NULL), *vrighty(NULL);
      // find segment corresponds to vertex
      SPoint3 xpoint = project(v->point(),v1x->point(),v2x->point());
      SPoint3 ypoint = project(v->point(),v1y->point(),v2y->point());
      for (int j=0; j<sizex-1; j++){
        if (inside(xpoint,xlist[j]->point(),xlist[j+1]->point())){
          vleftx = xlist[j];
          vrightx = xlist[j+1];
          break;
        };
      };
      for (int j=0; j<sizey-1; j++){
        if (inside(ypoint,ylist[j]->point(),ylist[j+1]->point())){
          vlefty = ylist[j];
          vrighty = ylist[j+1];
          break;
        };
      };
      // segment found
      if ((vleftx!=NULL) and (vlefty!=NULL) and (vrightx!=NULL) and (vrighty!=NULL)){
        if (_mbc->getOrder()==1){
          constraintElement* cele = new CoonsPatchCubicSplineConstraintElement(_LagSpace,_MultSpace,dim,v,vref,vleftx,vrightx,vlefty,vrighty,flagx,flagy);
          _allConstraint.insert(cele);
        }
        else if (_mbc->getOrder()==2){
          constraintElement* cele = new CoonsPatchCubicSplineConstraintElementSecondOrder(_LagSpace,_MultSpace,dim,v,vref,vleftx,vrightx,vlefty,vrighty,flagx,flagy);
          _allConstraint.insert(cele);
        }
      }
      else
        Msg::Error("patch element not found");
    };
  };
};

template<class Iterator>
void pbcConstraintElementGroup::cubicSplinePeriodicConditionCoonsPatch(Iterator itbegin, Iterator itend, MVertex* vref,
                                                             std::vector<MVertex*>& xlist,std::vector<MVertex*>& ylist,
                                                             int flagx, int flagy,std::set<MVertex*>& others ){

  int sizex = xlist.size();
  int sizey = ylist.size();

  MVertex* v1x = xlist[0];
  MVertex* v2x = xlist[sizex-1];
  MVertex* v1y = ylist[0];
  MVertex* v2y = ylist[sizey-1];

  MVertex * vtemp;
  if (v1y != v1x and v1y!= v2x)
    vtemp = v1y;
  else
    vtemp = v2y;

  int dim = _mbc->getDim();
  for (Iterator it = itbegin; it!= itend;it++){
    MVertex* v = *it;
    if (others.find(v)==others.end()){
      SPoint3 vnn = project(v->point(),v1x->point(),v2x->point(),vtemp->point());
      MVertex* vleftx, *vrightx,* vlefty, *vrighty;
      SPoint3 xpoint = project(vnn,v1x->point(),v2x->point());
      SPoint3 ypoint = project(vnn,v1y->point(),v2y->point());
      for (int j=0; j<sizex-1; j++){
        if (inside(xpoint,xlist[j]->point(),xlist[j+1]->point())){
          vleftx = xlist[j];
          vrightx = xlist[j+1];
          break;
        };
      };
      for (int j=0; j<sizey-1; j++){
        if (inside(ypoint,ylist[j]->point(),ylist[j+1]->point())){
          vlefty = ylist[j];
          vrighty = ylist[j+1];
          break;
        };
      };
      if ((vleftx!=NULL) and (vlefty!=NULL) and (vrightx!=NULL) and (vrighty!=NULL)){
        if (_mbc->getOrder()==1){
          constraintElement* cele = new CoonsPatchCubicSplineConstraintElement(_LagSpace,_MultSpace,dim,v,vref,vleftx,vrightx,vlefty,vrighty,flagx,flagy);
          _allConstraint.insert(cele);
        }
        else if (_mbc->getOrder()==2){
          constraintElement* cele = new CoonsPatchCubicSplineConstraintElementSecondOrder(_LagSpace,_MultSpace,dim,v,vref,vleftx,vrightx,vlefty,vrighty,flagx,flagy);
          _allConstraint.insert(cele);
        }
      }
      else
        Msg::Error("patch element not found");
    };
  };
};

// for lagrange
template<class Iterator>
void pbcConstraintElementGroup::lagrangeFormCondition(Iterator itbegin, Iterator itend,
                                                  std::vector<MVertex*>& vlist){
  int size = vlist.size();
  int dim = _mbc->getDim();
  for (Iterator it = itbegin; it!= itend;it++){
    MVertex* v = *it;
    bool flag = true;
    for (int i=0; i<size; i++){
      if (v->getNum() == vlist[i]->getNum()){
        flag = false;
        break;
      }
    };
    if (flag){
      if (_mbc->getOrder()==1){
        constraintElement* cele = new lagrangeConstraintElement(_LagSpace,_MultSpace,dim,v,vlist);
        _allConstraint.insert(cele);
      }
      else if (_mbc->getOrder()==2){
        constraintElement* cele = new lagrangeConstraintElementSecondOrder(_LagSpace,_MultSpace,dim,v,vlist);
        _allConstraint.insert(cele);
      };
    };
  };
};
template<class Iterator>
void pbcConstraintElementGroup::lagrangePeriodicCondition(Iterator itbegin, Iterator itend,
                                                      std::vector<MVertex*>& vlist, std::set<MVertex*>& others){
  int size = vlist.size();
  int dim = _mbc->getDim();
  for (Iterator it = itbegin; it != itend; it++){
    MVertex* v= *it;
    if (others.find(v) == others.end()){
      if (_mbc->getOrder()==1){
        constraintElement* cele = new lagrangeConstraintElement(_LagSpace,_MultSpace,dim,v,vlist);
        _allConstraint.insert(cele);
      }
      else if (_mbc->getOrder()==2){
        constraintElement* cele = new lagrangeConstraintElementSecondOrder(_LagSpace,_MultSpace,dim,v,vlist);
        _allConstraint.insert(cele);
      };
    };
  };
};

// for lagrange
template<class Iterator>
void pbcConstraintElementGroup::lagrangeFormConditionCoonsPatch(Iterator itbegin, Iterator itend, MVertex* vref,
                                                     std::vector<MVertex*>& xlist, std::vector<MVertex*>& ylist,
                                                     std::set<MVertex*>& others ){
  int dim = _mbc->getDim();
  for (Iterator it = itbegin; it!= itend;it++){
    MVertex* v = *it;
    if (others.find(v)==others.end()){
      if (_mbc->getOrder()==1){
        constraintElement* cele = new CoonsPatchLagrangeConstraintElement(_LagSpace,_MultSpace,dim,v,vref,xlist,ylist);
        _allConstraint.insert(cele);
      }
      else if (_mbc->getOrder()==2){
        constraintElement* cele = new CoonsPatchLagrangeConstraintElementSecondOrder(_LagSpace,_MultSpace,dim,v,vref,xlist,ylist);
        _allConstraint.insert(cele);
      }
    };
  };
};
template<class Iterator>
void pbcConstraintElementGroup::lagrangePeriodicConditionCoonsPatch(Iterator itbegin, Iterator itend, MVertex* vref,
                                                         std::vector<MVertex*>& xlist,std::vector<MVertex*>& ylist,
                                                         std::set<MVertex*>& others){
  int dim = _mbc->getDim();
  for (Iterator it = itbegin; it!= itend;it++){
    MVertex* v = *it;
    if (others.find(v)==others.end()){
      if (_mbc->getOrder()==1){
        constraintElement* cele = new CoonsPatchLagrangeConstraintElement(_LagSpace,_MultSpace,dim,v,vref,xlist,ylist);
        _allConstraint.insert(cele);
      }
      else if (_mbc->getOrder()==2){
        constraintElement* cele = new CoonsPatchLagrangeConstraintElementSecondOrder(_LagSpace,_MultSpace,dim,v,vref,xlist,ylist);
        _allConstraint.insert(cele);
      }
    };
  };
};

bool pbcConstraintElementGroup::isInside(MVertex* v, MElement* e) const{
  int dim = e->getDim();
  std::vector<MVertex*> vv;
  e->getVertices(vv);

  SPoint3 pp;
  if (dim == 1){
    pp = project(v->point(),vv[0]->point(), vv[1]->point());
  }
  else if (dim ==2){
    pp = project(v->point(),vv[0]->point(), vv[1]->point(),vv[2]->point());
  }

  double xyz[3] = {pp[0],pp[1],pp[2]};
  double uvw[3];

  e->xyz2uvw(xyz,uvw);
  if (e->isInside(uvw[0],uvw[1],uvw[2])){
    return true;
  }
  else return false;

};

template<class Iterator>
void pbcConstraintElementGroup::pbcFEConstraintElementGroup(Iterator itbegin, Iterator itend,
                groupOfElements* g, std::set<MVertex*>& others){
  int dim = _mbc->getDim();
  for (Iterator it = itbegin; it!= itend;it++){
    MVertex* v = *it;
    if (others.find(v)==others.end()){
      MElement* e = NULL;
      for (groupOfElements::elementContainer::iterator itg = g->begin(); itg!= g->end(); itg++){
        MElement* ele = *itg;
        if (isInside(v,ele)){
          e = ele;
          break;
        }
      }
      if (e == NULL){
        Msg::Error("receive element not found");
      }
      else{
        if (_mbc->getOrder()==1){
          constraintElement* cele = new FEConstraintElement(_LagSpace,_MultSpace,dim,v,e);
          _allConstraint.insert(cele);
        }
        else if (_mbc->getOrder()==2){
          constraintElement* cele = new FEConstraintElementSecondOrder(_LagSpace,_MultSpace,dim,v,e);
          _allConstraint.insert(cele);

        }
      }
    };
  };
};

void pbcConstraintElementGroup::createFEConstraintElementGroup(){
  int dim = _mbc->getDim();
  nonLinearPeriodicBC* pbc = static_cast<nonLinearPeriodicBC*>(_mbc);
  const std::vector<groupOfElements*>& bgroup = _mbc->getBoundaryGroupOfElements();
 // create base
  this->createPolynomialBasePoints();
  this->createAllBCGroups();

  if (dim ==2){
    this->pbcFEConstraintElementGroup(l12.begin(),l12.end(),_gX,_cVertices);
    this->pbcFEConstraintElementGroup(l34.begin(),l34.end(),_gX,_cVertices);
    this->pbcFEConstraintElementGroup(l41.begin(),l41.end(),_gY,_cVertices);
    this->pbcFEConstraintElementGroup(l23.begin(),l23.end(),_gY,_cVertices);
  }
  else if (dim == 3){
    #ifdef _DEBUG
    Msg::Error("ele num gxy =%d, gyz = %d, gzx = %d",_gXY->size(),_gYZ->size(), _gZX->size());
    #endif
    this->pbcFEConstraintElementGroup(l12.begin(),l12.end(),_gX,_cVertices);
    this->pbcFEConstraintElementGroup(l34.begin(),l34.end(),_gX,_cVertices);
    this->pbcFEConstraintElementGroup(l78.begin(),l78.end(),_gX,_cVertices);
    this->pbcFEConstraintElementGroup(l56.begin(),l56.end(),_gX,_cVertices);

    this->pbcFEConstraintElementGroup(l41.begin(),l41.end(),_gY,_cVertices);
    this->pbcFEConstraintElementGroup(l23.begin(),l23.end(),_gY,_cVertices);
    this->pbcFEConstraintElementGroup(l67.begin(),l67.end(),_gY,_cVertices);
    this->pbcFEConstraintElementGroup(l85.begin(),l85.end(),_gY,_cVertices);

    this->pbcFEConstraintElementGroup(l15.begin(),l15.end(),_gZ,_cVertices);
    this->pbcFEConstraintElementGroup(l26.begin(),l26.end(),_gZ,_cVertices);
    this->pbcFEConstraintElementGroup(l37.begin(),l37.end(),_gZ,_cVertices);
    this->pbcFEConstraintElementGroup(l48.begin(),l48.end(),_gZ,_cVertices);

    this->pbcFEConstraintElementGroup(f1234->vbegin(), f1234->vend(),_gXY,_bVertices);
    this->pbcFEConstraintElementGroup(f1584->vbegin(), f1584->vend(),_gYZ,_bVertices);
    this->pbcFEConstraintElementGroup(f1562->vbegin(), f1562->vend(),_gZX,_bVertices);

    this->pbcFEConstraintElementGroup(f5678->vbegin(), f5678->vend(),_gXY,_bVertices);
    this->pbcFEConstraintElementGroup(f2673->vbegin(), f2673->vend(),_gYZ,_bVertices);
    this->pbcFEConstraintElementGroup(f4873->vbegin(), f4873->vend(),_gZX,_bVertices);
  }

  if (_mbc->getOrder()==1){
    int numact = 0;
    for (std::set<MVertex*>::iterator it = _cVertices.begin(); it!= _cVertices.end(); it++){
      MVertex* vp1 = *it;
      if (isVirtual(vp1) == false){
        constraintElement* cel = new periodicMeshConstraint(_LagSpace,_MultSpace,dim,vp1);
        _allConstraint.insert(cel);
        numact++;
      }
      else if (isActive(vp1)) {
        Msg::Info("virtual corner vertex %d is active",vp1->getNum());
        constraintElement* cel = new periodicMeshConstraint(_LagSpace,_MultSpace,dim,vp1);
        _allConstraint.insert(cel);
        numact++;
      }
    }

    if (numact == 0){
      Msg::Info("all corner vertices are virtual and not active");
      std::set<MVertex*> constraintV;
      if (dim == 2){
        for (groupOfElements::vertexContainer::iterator itg = _gX->vbegin(); itg!= _gX->vend(); itg++){
          MVertex* vg = *itg;
          if (this->isActive(vg)){
            constraintElement* cel = new periodicMeshConstraint(_LagSpace,_MultSpace,dim,vg);
            _allConstraint.insert(cel);
            constraintV.insert(vg);
            break;
          }
        }

        for (groupOfElements::vertexContainer::iterator itg = _gY->vbegin(); itg!= _gY->vend(); itg++){
          MVertex* vg = *itg;
          if (this->isActive(vg) and constraintV.find(vg) == constraintV.end()){
            constraintElement* cel = new periodicMeshConstraint(_LagSpace,_MultSpace,dim,vg);
            _allConstraint.insert(cel);
            constraintV.insert(vg);
            break;
          }
        }
      }
      else if (dim==3){
        for (groupOfElements::vertexContainer::iterator itg = _gXY->vbegin(); itg!= _gXY->vend(); itg++){
          MVertex* vg = *itg;
          if (this->isActive(vg) and _bVertices.find(vg) == _bVertices.end()){
            constraintElement* cel = new periodicMeshConstraint(_LagSpace,_MultSpace,dim,vg);
            _allConstraint.insert(cel);
            constraintV.insert(vg);
            Msg::Info("fix vertex %d",vg->getNum());
			break;
          }
        }

        for (groupOfElements::vertexContainer::iterator itg = _gYZ->vbegin(); itg!= _gYZ->vend(); itg++){
          MVertex* vg = *itg;
          if (this->isActive(vg) and _bVertices.find(vg) == _bVertices.end() and constraintV.find(vg) == constraintV.end()){
            constraintElement* cel = new periodicMeshConstraint(_LagSpace,_MultSpace,dim,vg);
            _allConstraint.insert(cel);
            constraintV.insert(vg);
            Msg::Info("fix vertex %d",vg->getNum());
			break;
          }
        }
        for (groupOfElements::vertexContainer::iterator itg = _gZX->vbegin(); itg!= _gZX->vend(); itg++){
          MVertex* vg = *itg;
          if (this->isActive(vg) and _bVertices.find(vg) == _bVertices.end() and constraintV.find(vg) == constraintV.end()){
            constraintElement* cel = new periodicMeshConstraint(_LagSpace,_MultSpace,dim,vg);
            _allConstraint.insert(cel);
            constraintV.insert(vg);
			Msg::Info("fix vertex %d",vg->getNum());
            break;
          }
        }
      }

      Msg::Info("constraint %d vertex ",constraintV.size());
    }
  }
  else if (_mbc->getOrder() == 2){
    if (_cVertices.size() >0){
      MVertex* vn = *(_cVertices.begin());
      for(std::set<MVertex*>::iterator it = _cVertices.begin(); it!= _cVertices.end(); it++){
        if (it != _cVertices.begin())
        {
          MVertex* vp1 = *it;
          constraintElement* cel = new periodicMeshConstraintSecondOrder(_LagSpace,_MultSpace,dim,vp1,vn);
          _allConstraint.insert(cel);
        }
      }
    }
    if (dim== 2){
      constraintElement* ppc = new supplementConstraintSecondOrder(_LagSpace,_MultSpace,dim,_gX);
      _allConstraint.insert(ppc);
      ppc = new supplementConstraintSecondOrder(_LagSpace,_MultSpace,dim,_gY);
      _allConstraint.insert(ppc);
    }
    else if (dim == 3){
      constraintElement* ppc = new supplementConstraintSecondOrder(_LagSpace,_MultSpace,dim,_gXY);
      _allConstraint.insert(ppc);
      ppc = new supplementConstraintSecondOrder(_LagSpace,_MultSpace,dim,_gYZ);
      _allConstraint.insert(ppc);
      ppc = new supplementConstraintSecondOrder(_LagSpace,_MultSpace,dim,_gZX);
      _allConstraint.insert(ppc);
    }
  }

};

void pbcConstraintElementGroup::createPeriodicMeshConstraintElementGroup(){
  nonLinearPeriodicBC* pbc =  static_cast<nonLinearPeriodicBC*>(_mbc);
  int dim = _mbc->getDim();
  const std::vector<groupOfElements*>& bgroup = _mbc->getBoundaryGroupOfElements();

  /**fixed node in case of periodic boundary condition and corner nodes does not exist**/
  if ((_cVertices.size() ==0) and (_mbc->getOrder()==1)){
    if (dim==2){
      MVertex* v = *(bgroup[0]->vbegin());
      _cVertices.insert(v);
      _cVertices.insert(findNearestVertex(bgroup[2],v));
      for (std::set<MVertex*>::iterator it=_cVertices.begin(); it !=_cVertices.end(); it++){
        _bVertices.insert(*it);
      }
    }
    else if (dim==3) {
      MVertex* v = *(bgroup[0]->vbegin());
      _cVertices.insert(v);
      _cVertices.insert(findNearestVertex(bgroup[3],v));
      v = *(bgroup[1]->vbegin());
      _cVertices.insert(v);
      _cVertices.insert(findNearestVertex(bgroup[4],v));
      for (std::set<MVertex*>::iterator it=_cVertices.begin(); it !=_cVertices.end(); it++){
        _bVertices.insert(*it);
      };
    };
  };

  if (dim ==3){
    // periodic of edges
    periodicConditionForPeriodicMesh(l56.begin(),l56.end(),l12.begin(),l12.end(),_cVertices);
    periodicConditionForPeriodicMesh(l78.begin(),l78.end(),l12.begin(),l12.end(),_cVertices);
    periodicConditionForPeriodicMesh(l34.begin(),l34.end(),l12.begin(),l12.end(),_cVertices);

    periodicConditionForPeriodicMesh(l23.begin(),l23.end(),l41.begin(),l41.end(),_cVertices);
    periodicConditionForPeriodicMesh(l67.begin(),l67.end(),l41.begin(),l41.end(),_cVertices);
    periodicConditionForPeriodicMesh(l85.begin(),l85.end(),l41.begin(),l41.end(),_cVertices);

    periodicConditionForPeriodicMesh(l26.begin(),l26.end(),l15.begin(),l15.end(),_cVertices);
    periodicConditionForPeriodicMesh(l37.begin(),l37.end(),l15.begin(),l15.end(),_cVertices);
    periodicConditionForPeriodicMesh(l48.begin(),l48.end(),l15.begin(),l15.end(),_cVertices);

  };

  for (int i=0; i< dim; i++){
    periodicConditionForPeriodicMesh(bgroup[i+dim]->vbegin(),bgroup[i+dim]->vend(),
                                    bgroup[i]->vbegin(),bgroup[i]->vend(),_bVertices);
  };

  // for corner vertices
  if (_mbc->getOrder()==1){
    for (std::set<MVertex*>::iterator it = _cVertices.begin(); it!= _cVertices.end(); it++){
      MVertex* vp1 = *it;
      constraintElement* cel = new periodicMeshConstraint(_LagSpace,_MultSpace,dim,vp1);
      _allConstraint.insert(cel);
    }
  }
  else if (_mbc->getOrder() ==2){
    if (_cVertices.size() >0){
      MVertex* vn = *(_cVertices.begin());
      for(std::set<MVertex*>::iterator it = _cVertices.begin(); it!= _cVertices.end(); it++){
        if (it != _cVertices.begin())
        {
          MVertex* vp1 = *it;
          constraintElement* cel = new periodicMeshConstraintSecondOrder(_LagSpace,_MultSpace,dim,vp1,vn);
          _allConstraint.insert(cel);
        }
      }
    }

    for (int i=0; i<dim; i++){
      groupOfElements* g = bgroup[i];
      constraintElement* ppc = new supplementConstraintSecondOrder(_LagSpace,_MultSpace,dim,g);
      _allConstraint.insert(ppc);
    };
  };
}

void pbcConstraintElementGroup::createCubicSplineConstraintElementGroup(){

  const std::vector<groupOfElements*>& bgroup = _mbc->getBoundaryGroupOfElements();
  nonLinearPeriodicBC* pbc = static_cast<nonLinearPeriodicBC*>(_mbc);
  int dim = _mbc->getDim();

  // create basis
  this->createPolynomialBasePoints();

  if (dim ==2){
    this->cubicSplineFormCondition(l12.begin(),l12.end(),_xBase,0);
    this->cubicSplinePeriodicCondition(l34.begin(),l34.end(),_xBase,0,_cVertices);

    this->cubicSplineFormCondition(l41.begin(),l41.end(),_yBase,1);
    this->cubicSplinePeriodicCondition(l23.begin(),l23.end(),_yBase,1,_cVertices);
  }
  else if (dim ==3){
    this->cubicSplineFormCondition(l12.begin(),l12.end(),_xBase,0);
    this->cubicSplineFormCondition(l41.begin(),l41.end(),_yBase,1);
    this->cubicSplineFormCondition(l15.begin(),l15.end(),_zBase,2);

    this->cubicSplinePeriodicCondition(l56.begin(),l56.end(),_xBase,0,_cVertices);
    this->cubicSplinePeriodicCondition(l78.begin(),l78.end(),_xBase,0,_cVertices);
    this->cubicSplinePeriodicCondition(l34.begin(),l34.end(),_xBase,0,_cVertices);

    this->cubicSplinePeriodicCondition(l23.begin(),l23.end(),_yBase,1,_cVertices);
    this->cubicSplinePeriodicCondition(l67.begin(),l67.end(),_yBase,1,_cVertices);
    this->cubicSplinePeriodicCondition(l85.begin(),l85.end(),_yBase,1,_cVertices);

    this->cubicSplinePeriodicCondition(l26.begin(),l26.end(),_zBase,2,_cVertices);
    this->cubicSplinePeriodicCondition(l37.begin(),l37.end(),_zBase,2,_cVertices);
    this->cubicSplinePeriodicCondition(l48.begin(),l48.end(),_zBase,2,_cVertices);


    this->cubicSplineFormConditionCoonsPatch(f1234->vbegin(), f1234->vend(),_v1,_xBase,_yBase,0,1,_bVertices);
    this->cubicSplineFormConditionCoonsPatch(f1562->vbegin(), f1562->vend(),_v1,_xBase,_zBase,0,2,_bVertices);
    this->cubicSplineFormConditionCoonsPatch(f1584->vbegin(), f1584->vend(),_v1,_yBase,_zBase,1,2,_bVertices);

    this->cubicSplinePeriodicConditionCoonsPatch(f5678->vbegin(), f5678->vend(),_v1,_xBase,_yBase,0,1,_bVertices);
    this->cubicSplinePeriodicConditionCoonsPatch(f4873->vbegin(), f4873->vend(),_v1,_xBase,_zBase,0,2,_bVertices);
    this->cubicSplinePeriodicConditionCoonsPatch(f2673->vbegin(), f2673->vend(),_v1,_yBase,_zBase,1,2,_bVertices);
  };
  // for corner vertices
  if (_mbc->getOrder()==1){
    for (std::set<MVertex*>::iterator it = _cVertices.begin(); it!= _cVertices.end(); it++){
      MVertex* vp = *it;
      constraintElement* cel = new periodicMeshConstraint(_LagSpace,_MultSpace,dim,vp);
      _allConstraint.insert(cel);
    }
  }
  else if (_mbc->getOrder() ==2){
    for(std::set<MVertex*>::iterator it = _cVertices.begin(); it!= _cVertices.end(); it++){
      MVertex* v = *it;
      if ( v != _v1){
        constraintElement* cel = new periodicMeshConstraintSecondOrder(_LagSpace,_MultSpace,dim,v,_v1);
        _allConstraint.insert(cel);
      }
    }

    if (dim==2){
      constraintElement* ppc = new cubicSplineSupplementConstraintSecondOrder(_LagSpace,_MultSpace,dim,_xBase,0);
      _allConstraint.insert(ppc);
      ppc = new cubicSplineSupplementConstraintSecondOrder(_LagSpace,_MultSpace,dim,_yBase,1);
      _allConstraint.insert(ppc);
    }
    else if (dim == 3){
      for (int i=0; i<dim; i++){
        groupOfElements* g = bgroup[i+dim];
        constraintElement* ppc = new supplementConstraintSecondOrder(_LagSpace,_MultSpace,dim,g);
        _allConstraint.insert(ppc);
      }
    };
  };
};

void pbcConstraintElementGroup::createLagrangeConstraintElementGroup(){
  int dim = _mbc->getDim();
  nonLinearPeriodicBC* pbc = static_cast<nonLinearPeriodicBC*>(_mbc);
  const std::vector<groupOfElements*>& bgroup = _mbc->getBoundaryGroupOfElements();
 // create base
  this->createPolynomialBasePoints();

  if (dim ==2){
    this->lagrangeFormCondition(l12.begin(),l12.end(),_xBase);
    this->lagrangePeriodicCondition(l34.begin(),l34.end(),_xBase,_cVertices);

    this->lagrangeFormCondition(l41.begin(),l41.end(),_yBase);
    this->lagrangePeriodicCondition(l23.begin(),l23.end(),_yBase,_cVertices);
  }
  else if (dim ==3){
    this->lagrangeFormCondition(l12.begin(),l12.end(),_xBase);
    this->lagrangeFormCondition(l41.begin(),l41.end(),_yBase);
    this->lagrangeFormCondition(l15.begin(),l15.end(),_zBase);

    this->lagrangePeriodicCondition(l56.begin(),l56.end(),_xBase,_cVertices);
    this->lagrangePeriodicCondition(l78.begin(),l78.end(),_xBase,_cVertices);
    this->lagrangePeriodicCondition(l34.begin(),l34.end(),_xBase,_cVertices);

    this->lagrangePeriodicCondition(l23.begin(),l23.end(),_yBase,_cVertices);
    this->lagrangePeriodicCondition(l67.begin(),l67.end(),_yBase,_cVertices);
    this->lagrangePeriodicCondition(l85.begin(),l85.end(),_yBase,_cVertices);

    this->lagrangePeriodicCondition(l26.begin(),l26.end(),_zBase,_cVertices);
    this->lagrangePeriodicCondition(l37.begin(),l37.end(),_zBase,_cVertices);
    this->lagrangePeriodicCondition(l48.begin(),l48.end(),_zBase,_cVertices);

    this->lagrangeFormConditionCoonsPatch(f1234->vbegin(), f1234->vend(),_v1,_xBase,_yBase,_bVertices);
    this->lagrangeFormConditionCoonsPatch(f1562->vbegin(), f1562->vend(),_v1,_xBase,_zBase,_bVertices);
    this->lagrangeFormConditionCoonsPatch(f1584->vbegin(), f1584->vend(),_v1,_yBase,_zBase,_bVertices);

    this->lagrangePeriodicConditionCoonsPatch(f5678->vbegin(), f5678->vend(),_v1,_xBase,_yBase,_bVertices);
    this->lagrangePeriodicConditionCoonsPatch(f4873->vbegin(), f4873->vend(),_v1,_xBase,_zBase,_bVertices);
    this->lagrangePeriodicConditionCoonsPatch(f2673->vbegin(), f2673->vend(),_v1,_yBase,_zBase,_bVertices);
  };

  // for corner vertices
  if (_mbc->getOrder()==1){

    for (std::set<MVertex*>::iterator it = _cVertices.begin(); it!= _cVertices.end(); it++){
      MVertex* vp1 = *it;
      constraintElement* cel = new periodicMeshConstraint(_LagSpace,_MultSpace,dim,vp1);
      _allConstraint.insert(cel);
    }
  }
  else if (_mbc->getOrder() ==2){
    if (pbc->getMaxDegree()>1){
      for(std::set<MVertex*>::iterator it = _cVertices.begin(); it!= _cVertices.end(); it++){
        MVertex* v = *it;
        if (v != _v1){
          constraintElement* cel = new periodicMeshConstraintSecondOrder(_LagSpace,_MultSpace,dim,v,_v1);
          _allConstraint.insert(cel);
        }
      }

      if (dim==2){
        constraintElement* ppc = new lagrangeSupplementConstraintSecondOrder(_LagSpace,_MultSpace,dim,_xBase);
        _allConstraint.insert(ppc);
        ppc = new lagrangeSupplementConstraintSecondOrder(_LagSpace,_MultSpace,dim,_yBase);
        _allConstraint.insert(ppc);
      }
      else if (dim ==3){
        std::vector<lagrangeSupplementConstraint*> lconstraint;
        lagrangeSupplementConstraint* con = new lagrangeSupplementConstraintSecondOrder(_LagSpace,_MultSpace,dim,_xBase);
        lconstraint.push_back(con);
        con = new lagrangeSupplementConstraintSecondOrder(_LagSpace,_MultSpace,dim,_yBase);
        lconstraint.push_back(con);
        con = new lagrangeSupplementConstraintSecondOrder(_LagSpace,_MultSpace,dim,_zBase);
        lconstraint.push_back(con);
        constraintElement* ppc = new CoonsPatchLagrangeSupplementConstraintSecondOrder(_LagSpace,_MultSpace,dim,lconstraint[0],lconstraint[1]);
        _allConstraint.insert(ppc);
        ppc = new CoonsPatchLagrangeSupplementConstraintSecondOrder(_LagSpace,_MultSpace,dim,lconstraint[1],lconstraint[2]);
        _allConstraint.insert(ppc);
        ppc = new CoonsPatchLagrangeSupplementConstraintSecondOrder(_LagSpace,_MultSpace,dim,lconstraint[0],lconstraint[2]);
        _allConstraint.insert(ppc);
      }
    }
    else if(pbc->getMaxDegree() ==1) {
      for (std::set<MVertex*>::iterator it = _cVertices.begin(); it!= _cVertices.end(); it++){
        MVertex* vp1 = *it;
        constraintElement* cel = new periodicMeshConstraintSecondOrder(_LagSpace,_MultSpace,dim,vp1);
        _allConstraint.insert(cel);
      }
    }
  };

};

void pbcConstraintElementGroup::createLinearDisplacementConstraintElementGroup(){
  const std::vector<groupOfElements*>& bgroup = _mbc->getBoundaryGroupOfElements();
  int dim = bgroup.size()/2;
  std::set<MVertex*> allVertices;
  for (int i=0; i<bgroup.size(); i++){
    groupOfElements* g = bgroup[i];
    for (groupOfElements::vertexContainer::iterator it = g->vbegin(); it!= g->vend(); it++){
      MVertex* v= *it;
      if (_cVertices.find(v) == _cVertices.end())
        allVertices.insert(v);
    }
  }

  for (std::set<MVertex*>::iterator it = allVertices.begin(); it!= allVertices.end(); it++){
    MVertex* vp = *it;
    if (_mbc->getOrder()==1){
      constraintElement* cel = new periodicMeshConstraint(_LagSpace,_MultSpace,dim,vp);
      _allConstraint.insert(cel);
    }
    else if (_mbc->getOrder() ==2){
      constraintElement* cel = new periodicMeshConstraintSecondOrder(_LagSpace,_MultSpace,dim,vp);
      _allConstraint.insert(cel);
    }
  };
};

void pbcConstraintElementGroup::createMinimalKinematicConstraintElementGroup(){
  const std::vector<groupOfElements*>& bgroup = _mbc->getBoundaryGroupOfElements();
  int dim = _mbc->getDim();
  int size = bgroup.size();
  int order = _mbc->getOrder();
  for (int i=0; i<size; i++){
    if (order ==1){
      constraintElement* ppc = new supplementConstraint(_LagSpace,_MultSpace,dim,bgroup[i]);
      _allConstraint.insert(ppc);
    }
    else if (order ==2){
      constraintElement* ppc = new supplementConstraintSecondOrder(_LagSpace,_MultSpace,dim,bgroup[i]);
      _allConstraint.insert(ppc);
    };
  }
};

void pbcConstraintElementGroup::createMixBCConstraintElementGroup(){
  if (_mbc->getOrder() == 2) {
    Msg::Warning("This BC do not work for second order homogenization");
    return;
  }

  const std::vector<groupOfElements*>& bgroup = _mbc->getBoundaryGroupOfElements();
  const std::vector<int>& bphysical = _mbc->getBoundaryPhysicals();
  int dim = _mbc->getDim();
  nonLinearMixedBC* mixBC = dynamic_cast<nonLinearMixedBC*>(_mbc);
  const std::vector<std::pair<int,int> >& kphysical = mixBC->getKinematicPhysical();
  const std::vector<std::pair<int,int> >& sphysical = mixBC->getStaticPhysical();

  for (int i=0; i<kphysical.size(); i++){
    Msg::Error("k bc = %d comp = %d",kphysical[i].first,kphysical[i].second);
    constraintElement::allPositiveVertex.clear();
    const groupOfElements* gr = _mbc->getBCGroup(kphysical[i].first);
    std::vector<int> compK;
    compK.push_back(kphysical[i].second);
    FunctionSpaceBase* mspace = _MultSpace->clone(compK);
    FunctionSpaceBase* lspace = _LagSpace->clone(compK);
    for (groupOfElements::vertexContainer::iterator it = gr->vbegin(); it!= gr->vend(); it++){
      MVertex* vp = *it;
      if (_cVertices.find(vp) == _cVertices.end()){
        constraintElement* cel = new periodicMeshConstraint(lspace,mspace,1,vp, NULL, kphysical[i].second);
        _allConstraint.insert(cel);
      }
    }
  };

  for (int i=0; i<sphysical.size(); i++){
    Msg::Error("s bc = %d comp = %d",sphysical[i].first,sphysical[i].second);
    groupOfElements* gr = _mbc->getBCGroup(sphysical[i].first);
    std::vector<int> compS;
    compS.push_back(sphysical[i].second);
    FunctionSpaceBase* mspace = _MultSpace->clone(compS);
    FunctionSpaceBase* lspace = _LagSpace->clone(compS);
    constraintElement::allPositiveVertex.clear();
    constraintElement* ppc = new supplementConstraint(lspace,mspace,1, gr, sphysical[i].second);
    _allConstraint.insert(ppc);
  };

  if (sphysical.size() == 0){
    for (std::set<MVertex*>::iterator it = _cVertices.begin(); it!= _cVertices.end(); it++){
      MVertex* vp1 = *it;
      constraintElement* cel = new periodicMeshConstraint(_LagSpace,_MultSpace,dim,vp1);
      _allConstraint.insert(cel);
    }
  }
};


void pbcConstraintElementGroup::createConstraintGroup(){
  constraintElement::allPositiveVertex.clear();
  if (_mbc->getType() == nonLinearMicroBC::PBC){
    nonLinearPeriodicBC* pbc = static_cast<nonLinearPeriodicBC*>(_mbc);
    const std::vector<groupOfElements*>& bgroup = _mbc->getBoundaryGroupOfElements();
    if (bgroup.size()>0){
      if  (pbc->getPBCMethod() == nonLinearPeriodicBC::CEM){
      Msg::Info("Imposing PBC by periodic mesh formulation");
      this->createPeriodicMeshConstraintElementGroup();
      }
      else if (pbc->getPBCMethod() == nonLinearPeriodicBC::CSIM){
        Msg::Info("Imposing PBC by cubic spline formulation");
        this->createCubicSplineConstraintElementGroup();
      }
      else if (pbc->getPBCMethod() == nonLinearPeriodicBC::LIM){
        Msg::Info("Imposing PBC by lagrange formulation");
        if (pbc->getMaxDegree() >1)
          this->createLagrangeConstraintElementGroup();
        else
          createLinearDisplacementConstraintElementGroup();
      }
      else if (pbc->getPBCMethod() == nonLinearPeriodicBC::FE_LIN or  pbc->getPBCMethod() == nonLinearPeriodicBC::FE_QUA){
        Msg::Info("Imposing PBC by lagrange C0");
        this->createFEConstraintElementGroup();
      }
      else{
        Msg::Fatal("this method is not implemented to impose PBC");
      }
    }
  }
  else if (_mbc->getType()==nonLinearMicroBC::LDBC){
    Msg::Info("Imposing linear displacement BC");
    this->createLinearDisplacementConstraintElementGroup();
  }
  else if (_mbc->getType()==nonLinearMicroBC::MKBC){
    Msg::Info("Imposing minimal kinematical BC");
    this->createMinimalKinematicConstraintElementGroup();
  }
  else if (_mbc->getType() == nonLinearMicroBC::MIXBC){
    Msg::Info("imposing mixed BC");
    this->createMixBCConstraintElementGroup();
  }
  else{
    Msg::Error("this boundary condition type is not implemented");
  };

  constraintElement::allPositiveVertex.clear();

};

void pbcConstraintElementGroup::createPBCFromFile(FILE* fp, GModel* pModel){
  if (fp == NULL) return;
  int dim = pModel->getDim();
  while (1){
    if(feof(fp))
      break;
    char what[256];
    if(fscanf(fp, "%s", what) != 1) return;
    if (!strcmp(what, "PeriodicNodes")){
      int numver;
      fscanf(fp, "%d", &numver);
      for (int i=0; i<numver; i++){
        int num;
        if (fscanf(fp, "%d", &num)){
          MVertex* v = pModel->getMeshVertexByTag(num);
          Msg::Error("periodic node: %d",v->getNum());
          if (_mbc->getOrder()==1){
            constraintElement* cel = new periodicMeshConstraint(_LagSpace,_MultSpace,dim,v);
            _allConstraint.insert(cel);
          }
          else if (_mbc->getOrder()==2){
            constraintElement* cel = new periodicMeshConstraintSecondOrder(_LagSpace,_MultSpace,dim,v);
            _allConstraint.insert(cel);
          }
        }
      }
    }
    else if (!strcmp(what, "MatchingNodes")){
      int nummat;
      fscanf(fp, "%d", &nummat);
      for (int i=0; i<nummat; i++){
        int p, n;
        if (fscanf(fp,"%d %d",&p,&n)){
          MVertex* vp = pModel->getMeshVertexByTag(p);
          MVertex* vn = pModel->getMeshVertexByTag(n);

          Msg::Error("matching node: %d %d", vp->getNum(),vn->getNum());
          if (_mbc->getOrder()==1){
            int dim = pModel->getDim();
            constraintElement* cel = new periodicMeshConstraint(_LagSpace,_MultSpace,dim,vp,vn);
            _allConstraint.insert(cel);
          }
          else if (_mbc->getOrder()==2){
            constraintElement* cel = new periodicMeshConstraintSecondOrder(_LagSpace,_MultSpace,dim,vp,vn);
            _allConstraint.insert(cel);
          }

        }
      }
    }
  }
};

SVector3 pbcConstraintElementGroup::getUniformDisp(MVertex* v){
  const STensor3& FI = _mbc->getFirstOrderKinematicalVariable();
  const STensor33& G = _mbc->getSecondOrderKinematicalVariable();
  SVector3 u(0.,0.,0.);
  SPoint3 p = v->point();
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      u[i]+= FI(i,j)*p[j];
      if (_mbc->getOrder() == 2){
        for (int k=0; k<3; k++){
          u[i] += 0.5*G(i,j,k)*p[j]*p[k];
        }
      }
    }
  }
  return u;
};

SVector3 pbcConstraintElementGroup::getPerturbation(dofManager<double>* pmanager, MVertex* v){
  SVector3 ubar = this->getUniformDisp(v);
  std::vector<Dof> keys;
  std::vector<double> val;
  getKeysFromVertex(_LagSpace,v,keys);
  pmanager->getDofValue(keys,val);
  ubar *= -1.;
  for (int i=0; i<keys.size(); i++){
    ubar[i] +=  val[i];
  }
  return ubar;
};

SVector3 pbcConstraintElementGroup::getTangentCubicSpline(dofManager<double>* pmanager, MVertex* v, const int dir){
  SVector3 vec(0.,0.,0.);
  if (_mbc->getType() != nonLinearMicroBC::PBC) {
    Msg::Warning("this used for periodic BC and cubic spline method");
  }
  else {
    nonLinearPeriodicBC* pbc = dynamic_cast<nonLinearPeriodicBC*>(_mbc);
    if (pbc->getPBCMethod() == nonLinearPeriodicBC::CSIM){
      std::vector<Dof> dofs;
      std::vector<double> vals;
      cubicSplineConstraintElement::addKeysToVertex(_LagSpace,v,dofs,dir);
      pmanager->getDofValue(dofs,vals);
      for (int i=0; i<vals.size(); i++){
        vec[i] += vals[i];
      }
    }
  }
  return vec;
};

void pbcConstraintElementGroup::cubicSplineToFile(std::vector<MVertex*>& vlist, dofManager<double>* pmanager, FILE* file, const int dir){
  if (vlist.size() ==0) return;
  int N= 100;
  for (int i=0; i<vlist.size()-1; i++){
    MVertex* vinit = vlist[i];
    MVertex* vend = vlist[i+1];

    double L = vend->distance(vinit);
    SVector3 pertInit = this->getPerturbation(pmanager,vinit);
    SVector3 pertEnd = this->getPerturbation(pmanager,vend);

    SVector3 tInit = this->getTangentCubicSpline(pmanager,vinit,dir);
    SVector3 tEnd = this->getTangentCubicSpline(pmanager,vend,dir);

    fprintf(file,"%e %e %e %e %e %e \n",vinit->x(),vinit->y(),vinit->z(),pertInit[0],pertInit[1],pertInit[2]);
    SPoint3 pinit = vinit->point();
    SPoint3 pend = vend->point();

    SPoint3 dp = pend-pinit;
    dp /=(double(N-1));
    for (int j=1; j<N-1; j++){
      SPoint3 P = pinit+ dp*j;
      double l = P.distance(pinit);
      fullVector<double> FF;
      cubicSplineConstraintElement::getFF(l,0,L,FF);
      SVector3 val = pertInit*FF(0)+tInit*FF(1)+pertEnd*FF(2)+tEnd*FF(3);
      fprintf(file,"%e %e %e %e %e %e \n",P.x(),P.y(),P.z(),val[0],val[1],val[2]);
    }
    if (i == vlist.size()-2){
       fprintf(file,"%e %e %e %e %e %e \n",vend->x(),vend->y(),vend->z(),pertEnd[0],pertEnd[1],pertEnd[2]);
    }
  }
};

void pbcConstraintElementGroup::lagrangeToFile(std::vector<MVertex*>& vlist, dofManager<double>* pmanager, FILE* file){
  if (vlist.size() ==0) return;
  int N = 100;
  std::vector<double> base;
  std::vector<SVector3> pert;
  for (int i=0; i<vlist.size(); i++){
    base.push_back(vlist[i]->distance(vlist[0]));
    pert.push_back(this->getPerturbation(pmanager,vlist[i]));
  }

  SPoint3 p0 = vlist[0]->point();

  for (int i=0; i<vlist.size()-1; i++){
    MVertex* vinit = vlist[i];
    MVertex* vend = vlist[i+1];
    double length = vinit->distance(vlist[0]);
    fprintf(file,"%e %e %e %e %e %e \n",vinit->x(),vinit->y(),vinit->z(),pert[i][0],pert[i][1],pert[i][2]);

    SPoint3 pinit = vinit->point();
    SPoint3 pend = vend->point();
    SPoint3 dp = pend-pinit;
    dp /=(double(N-1));
    for (int j=1; j<N-1; j++){
      SPoint3 P = pinit+ dp*j;
      length = P.distance(p0);
      fullVector<double> FF;
      lagrangeConstraintElement::getFF(length,base,FF);
      SVector3 val(0.,0.,0.);
      for (int k=0; k<FF.size(); k++){
        val +=  pert[k]*FF(k);
      }
      fprintf(file,"%e %e %e %e %e %e \n",P.x(),P.y(),P.z(),val[0],val[1],val[2]);
    }
    if (i == vlist.size()-2){
       fprintf(file,"%e %e %e %e %e %e \n",vend->x(),vend->y(),vend->z(),pert[i+1][0],pert[i+1][1],pert[i+1][2]);
    }
  }
};

void pbcConstraintElementGroup::writePertBoundaryToFile(dofManager<double>* pmanager, const int e, const int g, const int iter ){
  if (_mbc->getType() == nonLinearMicroBC::PBC){
    nonLinearPeriodicBC* pbc = dynamic_cast<nonLinearPeriodicBC*>(_mbc);
    std::string filename = "E_"+int2str(e)+"_GP_"+int2str(g)+"_xInterpolation_iter_"+int2str(iter)+ ".csv";
    FILE* file = fopen(filename.c_str(),"w");
    if (pbc->getPBCMethod() == nonLinearPeriodicBC::CSIM)
      cubicSplineToFile(_xBase,pmanager,file,0);
    else
      lagrangeToFile(_xBase,pmanager,file);
    fclose(file);

    filename = "E_"+int2str(e)+"_GP_"+int2str(g)+"_yInterpolation_iter_"+int2str(iter)+ ".csv";
    file = fopen(filename.c_str(),"w");
    if (pbc->getPBCMethod() == nonLinearPeriodicBC::CSIM)
      cubicSplineToFile(_yBase,pmanager,file,1);
    else
      lagrangeToFile(_yBase,pmanager,file);
    fclose(file);

    filename = "E_"+int2str(e)+"_GP_"+int2str(g)+"_zInterpolation_iter_"+int2str(iter)+ ".csv";
    file = fopen(filename.c_str(),"w");
    if (pbc->getPBCMethod() == nonLinearPeriodicBC::CSIM)
      cubicSplineToFile(_zBase,pmanager,file,2);
    else
      lagrangeToFile(_zBase,pmanager,file);
    fclose(file);
  }

  const std::vector<groupOfElements*>& bgr = _mbc->getBoundaryGroupOfElements();
  for (int i =0; i< bgr.size(); i++){
    std::string filename ="E_"+int2str(e)+"_GP_"+int2str(g)+"_pert_" + int2str(i)+ "_iter_"+int2str(iter)+ ".csv";
    FILE* file = fopen(filename.c_str(),"w");
    const groupOfElements* g = bgr[i];
    for (groupOfElements::vertexContainer::iterator it = g->vbegin(); it!= g->vend(); it++){
      MVertex* v = *it;
      SVector3 pert = this->getPerturbation(pmanager,v);
      fprintf(file,"%d %e %e %e %e %e %e\n",v->getNum(), v->x(),v->y(),v->z(),pert[0],pert[1],pert[2]);
    }
    fclose(file);
  }
};

bool pbcConstraintElementGroup::isActive(const MVertex* v) const {
  for (std::set<constraintElement*>::const_iterator it = _allConstraint.begin();
            it!= _allConstraint.end(); it++){
    const constraintElement* cel = *it;
    if (cel->isActive(v)) return true;
  }
  return false;
};

bool pbcConstraintElementGroup::isVirtual(const MVertex* v) const{
  for (std::set<MVertex*>::const_iterator it = _virtualVertices.begin(); it!= _virtualVertices.end(); it++){
    MVertex* vv = *it;
    if (vv->getNum() == v->getNum()) return true;
  }

  return false;
};

void pbcConstraintElementGroup::print() const {
  for (std::set<constraintElement*>::iterator it = _allConstraint.begin(); it!= _allConstraint.end(); it++){
    (*it)->print();
  }
}
