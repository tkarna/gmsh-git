#ifndef FORWARDEULERPROCEDURE_H_
#define FORWARDEULERPROCEDURE_H_

// interface to solve the equation K . du/dt - q dm/dt  = 0

#include "nonLinearSystems.h"

template<class scalar>
class forwardEulerArcLengthBase{
  public:
    virtual ~forwardEulerArcLengthBase(){};
    virtual void setArcLengthControl(scalar val) = 0;
    virtual void getControlParameter(scalar &val) const = 0;
    virtual void setPerturbation(scalar* val, scalar pert) = 0;
};

template<class scalar>
class forwardEulerLinearSystemPETSc : public linearSystemPETSc<scalar>,
                                      public forwardEulerArcLengthBase<scalar> {
	#if defined(HAVE_PETSC)
	protected:
		Vec _xsol;
		scalar _controlIncrement,_controlParameter, _arcLength;

	public:
		forwardEulerLinearSystemPETSc(): linearSystemPETSc<scalar>(){
			_controlParameter = 0.;
		};
		virtual ~forwardEulerLinearSystemPETSc(){};
		virtual void allocate(int nbRows){
		  this->clear();
			linearSystemPETSc<scalar>::allocate(nbRows);
			_try(VecDuplicate(this->_x, &_xsol));
		};
		virtual void clear(){
			if (this->_isAllocated){
				_try(VecDestroy(&_xsol));
			};
			linearSystemPETSc<scalar>::clear();
		};
		virtual int systemSolve(){
		  // to compute v_n
			linearSystemPETSc<scalar>::systemSolve();
			// f = sqrt(vn.vn+1)
			PetscScalar f;
			_try(VecAssemblyBegin(this->_x));
			_try(VecAssemblyEnd(this->_x));
			_try(VecDot(this->_x,this->_x,&f));
			f = sqrt(f+1);
			PetscScalar sign;
			_try(VecDot(this->_b,this->_x,&sign));
			if (sign>0)
				_controlIncrement = -_arcLength/f;

			else if (sign<0)
				_controlIncrement = _arcLength/f;
			else{
				_controlIncrement = 0;
				printf("Buckling occurs here!\n");
			};

			printf("dmu = %f\n",_controlIncrement);
			_try(VecScale(this->_x,_controlIncrement));
			_try(VecAXPY(_xsol,1.,this->_x));
			_controlParameter += _controlIncrement;
			_try(VecAssemblyBegin(_xsol));
			_try(VecAssemblyEnd(_xsol));
			return 1;
		};
		virtual void getFromSolution(int row, scalar& val) const{
			_try(VecGetValues(_xsol,1,&row,&val));
		};
		virtual void setArcLengthControl(scalar val){
			_arcLength = val;
		};
		virtual void getControlParameter(scalar& val) const {
			val = _controlParameter;
		};
		virtual void setPerturbation(scalar* val, scalar pert){
		  int size;
		  _try(VecGetSize(this->_b,&size));
		  // normalization buckling mode
		  scalar norm =0.;
		  for (int i=0; i<size; i++){
        norm+=val[i]*val[i];
		  };
      for (int i =0; i<size; i++){
        scalar pertval = val[i]/norm*pert;
        _try(VecSetValues(this->_xsol,1,&i,&pertval,ADD_VALUES));
      };
		};
		#else
		public:
			forwardEulerLinearSystemPETSc(): linearSystemPETSc<scalar>(){};
			virtual ~forwardEulerLinearSystemPETSc(){};
			virtual void setArcLengthControl(scalar val) {};
      virtual void getControlParameter(scalar &val) const {};
      virtual void setPerturbation(scalar* val, scalar pert) {};
		#endif // HAVE_PETSC
};

#endif // FORWARDEULERPROCEDURE_H_
