#include "polynomialInterpolation.h"

//2D case base

polynomialInterpolationBase2D::polynomialInterpolationBase2D(FunctionSpaceBase* sp, dofManager<double>* _p, int deg):
          periodicConstraintsBase(sp,_p),degree(deg){};

polynomialInterpolationBase2D::polynomialInterpolationBase2D(const int tag, const int dim, dofManager<double>* _p,int deg)
          :periodicConstraintsBase(tag,dim,_p),degree(deg){};

polynomialInterpolationBase2D::~polynomialInterpolationBase2D(){};

void polynomialInterpolationBase2D::setPolynomialOrder(int i){
  degree=i;
};

void polynomialInterpolationBase2D::getPolynomialOrder(int &i){
  i=degree;
};

void polynomialInterpolationBase2D::getTwoFurthestVertexs(vertexContainer container, std::vector<MVertex*> &vlist){
  vlist.clear();
  std::vector<MVertex*> list;
  for (vertexContainer::iterator it=container.begin(); it!=container.end(); it++){
    list.push_back(*it);
  };
  int size=list.size();
  fullMatrix<double> m; m.resize(size,size); m.setAll(0.0);
  double length=0.0; int r=0; int c=0;
  for (int i=0; i<size; i++){
    for (int j=0; j<size; j++){
      m(i,j)=list[j]->distance(list[i]);
      if (length<m(i,j)){
        length=m(i,j); r=i; c=j;
      };
    };
  };
  vlist.push_back(list[r]);
  vlist.push_back(list[c]);
};

void polynomialInterpolationBase2D::getInterpolationPoint(std::vector<MVertex*> extreme,
                                          vertexContainer container, std::vector<MVertex*> &vlist){
  vlist.clear();
  std::vector<MVertex*> list;
  MVertex* v1=extreme[0];
  MVertex* v2=extreme[1];
  std::vector<double> vals;
  for (vertexContainer::iterator it=container.begin(); it!=container.end(); it++){
    MVertex* v=*it;
    list.push_back(v);
    vals.push_back(distance(v->point(),v1->point(),v2->point()));
  };
  #ifdef _DEBUG
  for (int i=0; i<vals.size(); i++)
     printf("val  = %f \n", vals[i]);
  #endif
  int size=list.size();
  for (int i=0; i<size; i++){
    for (int j=i; j<size; j++){
      if (vals[j]<vals[i]){
        double temp=vals[i]; vals[i]=vals[j]; vals[j]=temp;
        MVertex* vtemp=list[i]; list[i]=list[j]; list[j]=vtemp;
      };
    };
  };
  if (degree>size-1){
    degree=size-1;
    std::cout<<"Modify the imposed degree: "<<degree<<std::endl;
  };
  vlist.push_back(list[0]);
  for (int j=0; j<degree-1; j++){
    vlist.push_back(list[(j+1)*(size-1)/degree]);
  };
  vlist.push_back(list[size-1]);
};

void polynomialInterpolationBase2D::setInterpolationPoint(){
  xlist.clear(); ylist.clear();
  std::vector<MVertex*> list12, list23;
  getTwoFurthestVertexs(gVertex[0],list12);
  getTwoFurthestVertexs(gVertex[1],list23);
  getInterpolationPoint(list23,gVertex[0],xlist);
  getInterpolationPoint(list12,gVertex[1],ylist);
};

//----------------polynomial 3D base class-------------------------

polynomialInterpolationBase3D::polynomialInterpolationBase3D(FunctionSpaceBase* sp, dofManager<double>* p, int deg):
                            polynomialInterpolationBase2D(sp,p,deg){};

polynomialInterpolationBase3D::polynomialInterpolationBase3D(const int tag, const int dim, dofManager<double>* _p,
                            int deg):polynomialInterpolationBase2D(tag,dim,_p,deg){
};

void polynomialInterpolationBase3D::initialize(){
  xline.clear(); yline.clear(); zline.clear();
  vertexContainer l0; groupIntersection(gVertex[0],gVertex[2],l0); xline.push_back(l0);
  vertexContainer l2; groupIntersection(gVertex[3],gVertex[2],l2); xline.push_back(l2);
  vertexContainer l6; groupIntersection(gVertex[3],gVertex[5],l6); xline.push_back(l6);
  vertexContainer l4; groupIntersection(gVertex[0],gVertex[5],l4); xline.push_back(l4);

  vertexContainer l1; groupIntersection(gVertex[1],gVertex[2],l1); yline.push_back(l1);
  vertexContainer l3; groupIntersection(gVertex[4],gVertex[2],l3); yline.push_back(l3);
  vertexContainer l7; groupIntersection(gVertex[4],gVertex[5],l7); yline.push_back(l7);
  vertexContainer l5; groupIntersection(gVertex[1],gVertex[5],l5); yline.push_back(l5);

  vertexContainer l9; groupIntersection(gVertex[0],gVertex[1],l9); zline.push_back(l9);
  vertexContainer l10; groupIntersection(gVertex[1],gVertex[3],l10); zline.push_back(l10);
  vertexContainer l11; groupIntersection(gVertex[3],gVertex[4],l11); zline.push_back(l11);
  vertexContainer l8; groupIntersection(gVertex[4],gVertex[0],l8); zline.push_back(l8);

  std::vector<MVertex*> v12, v23, v26;
  getTwoFurthestVertexs(l0,v12); v1x=v12[0]; v2x=v12[1];
  getTwoFurthestVertexs(l1,v23); v1y=v23[0]; v2y=v23[1];
  getTwoFurthestVertexs(l9,v26); v1z=v26[0]; v2z=v26[1];

  if (distance(v1x->point(),v1y->point(),v2y->point())>distance(v2x->point(),v1y->point(),v2y->point())){
    MVertex* temp=v1x; v1x=v2x; v2x=temp;
  };

  if (distance(v1z->point(),v1y->point(),v2y->point())>distance(v2z->point(),v1y->point(),v2y->point())){
    MVertex* temp=v1z; v1z=v2z; v2z=temp;
  };

  if (distance(v1y->point(),v1x->point(),v2x->point())>distance(v2y->point(),v1x->point(),v2x->point())){
    MVertex* temp=v1y; v1y=v2y; v2y=temp;
  };

  xlist.clear(); ylist.clear(); zlist.clear();
  getInterpolationPoint(v23,l0,xlist);
  getInterpolationPoint(v12,l1,ylist);
  getInterpolationPoint(v12,l9,zlist);
};

