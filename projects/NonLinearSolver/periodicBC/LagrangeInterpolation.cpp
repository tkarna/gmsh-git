
#include "LagrangeInterpolation.h"

void lagrangeFunctionApproximation::setValues(double vals){
  listValues.push_back(vals);
};

void lagrangeFunctionApproximation::getFunctionValues(double s, fullVector<double> &aVals){
  int n=listValues.size();
  aVals.resize(n);
  for (int i=0; i<n; i++){
    aVals(i)=1.0;
    for (int j=0; j<n; j++){
      if (j!=i){
        aVals(i)*=(s-listValues[j])/(listValues[i]-listValues[j]);
      };
    };
  };
};

void lagrangeFunctionApproximation::printValues(){
  for (int i=0; i<listValues.size(); i++){
    std::cout<<listValues[i]<< "\t";
  };
  std::cout<<"\n";
};

void lagrangeFunctionApproximation::reset(){
  listValues.clear();
};

//-----------------------------LAGRANGE INTERPOLATION-------------------

polynomialInterpolationByLagrangeFuction::polynomialInterpolationByLagrangeFuction(FunctionSpaceBase* sp, dofManager<double>* p, int deg):
                                        polynomialInterpolationBase2D(sp,p,deg){};

polynomialInterpolationByLagrangeFuction::polynomialInterpolationByLagrangeFuction(const int tag, const int dim, dofManager<double>* _p,int deg):
                                             polynomialInterpolationBase2D(tag,dim,_p,deg){
};

void polynomialInterpolationByLagrangeFuction::setLagrangeFunctionApproximation(){
  yLag.reset(); xLag.reset();
  MVertex* v1x=xlist[0];
  MVertex* v2x=xlist[1];
  for (int i=0; i<ylist.size(); i++){
    yLag.setValues(distance(ylist[i]->point(),v1x->point(),v2x->point()));
  };

  MVertex* v1y=ylist[0];
  MVertex* v2y=ylist[1];
  for (int i=0; i<xlist.size(); i++){
    xLag.setValues(distance(xlist[i]->point(),v1y->point(),v2y->point()));
  };
};

void polynomialInterpolationByLagrangeFuction::formCondition(){
  //selon x
  MVertex* v1y=ylist[0];
  MVertex* v2y=ylist[1];
  for (std::set<MVertex*>::iterator it=gVertex[0].begin(); it!=gVertex[0].end(); it++){
    MVertex* v=*it;
    bool flag=true;
    for (int i=0; i<xlist.size(); i++){
      if (v->getNum()==xlist[i]->getNum()) flag=false;
    };
    if (flag){
      double sj=distance(v->point(),v1y->point(),v2y->point());
      fullVector<double> b;
      xLag.getFunctionValues(sj,b);
      std::vector<Dof> keys;
      getKeysFromVertex(v,keys);
      int ndofs=keys.size();
      std::vector<std::vector<Dof> > k;
      for (int i=0; i<xlist.size(); i++){
        std::vector<Dof> ktemp;
        getKeysFromVertex(xlist[i],ktemp);
        k.push_back(ktemp);
      };
      DofAffineConstraint<double> cons;
      for (int i=0; i<ndofs; i++){
        for (int j=0; j<k.size(); j++){
          cons.linear.push_back(std::pair<Dof,double>(k[j][i],b(j)));
        };
        cons.shift=0.0;
        p->setLinearConstraint(keys[i],cons);
        cons.linear.clear();
      };
    };
  };
  //slon y
  MVertex* v1x=xlist[0];
  MVertex* v2x=xlist[1];
  for (std::set<MVertex*>::iterator it=gVertex[1].begin(); it!=gVertex[1].end(); it++){
    MVertex* v=*it;
    bool flag=true;
    for (int i=0; i<ylist.size(); i++){
      if (v->getNum()==ylist[i]->getNum()) flag=false;
    };
    if (flag){
      double sj=distance(v->point(),v1x->point(),v2x->point());
      fullVector<double> b;
      yLag.getFunctionValues(sj,b);
      std::vector<Dof> keys;
      getKeysFromVertex(v,keys);
      int ndofs=keys.size();
      std::vector<std::vector<Dof> > k;
      for (int i=0; i<ylist.size(); i++){
        std::vector<Dof> ktemp;
        getKeysFromVertex(ylist[i],ktemp);
        k.push_back(ktemp);
      };
      DofAffineConstraint<double> cons;
      for (int i=0; i<ndofs; i++){
        for (int j=0; j<k.size(); j++){
          cons.linear.push_back(std::pair<Dof,double>(k[j][i],b(j)));
        };
        cons.shift=0.0;
        p->setLinearConstraint(keys[i],cons);
        cons.linear.clear();
      };
    };
  };
};


void polynomialInterpolationByLagrangeFuction::periodicCondition(){
  //selon x
  MVertex* v1y=ylist[0];
  MVertex* v2y=ylist[1];
  MVertex* v1x=xlist[0];
  MVertex* v2x=xlist[1];

  MVertex* vroot=*(gVertex[2].begin());
  SPoint3 proot=vroot->point();
  SPoint3 pp=project(vroot->point(),v1x->point(),v2x->point());
  fullVector<double> posi;posi.resize(3);
  posi(0)=(proot.x()-pp.x()); posi(1)=(proot.y()-pp.y()); posi(2)=(proot.z()-pp.z());
  fullVector<double> g;g.resize(3);
  imposedDef.mult(posi,g);

  for (std::set<MVertex*>::iterator it=gVertex[2].begin(); it!=gVertex[2].end(); it++){
    MVertex* v=*it;
    if (coinVertex.find(v)==coinVertex.end()){
      SPoint3 pt=project(v->point(),v1x->point(),v2x->point());
      double sj=distance(pt,v1y->point(),v2y->point());
      fullVector<double> b;
      xLag.getFunctionValues(sj,b);
      std::vector<Dof> keys;
      getKeysFromVertex(v,keys);
      int ndofs=keys.size();
      std::vector<std::vector<Dof> > k;
      for (int i=0; i<xlist.size(); i++){
        std::vector<Dof> ktemp;
        getKeysFromVertex(xlist[i],ktemp);
        k.push_back(ktemp);
      };
      DofAffineConstraint<double> cons;
      for (int i=0; i<ndofs; i++){
        for (int j=0; j<k.size(); j++){
          cons.linear.push_back(std::pair<Dof,double>(k[j][i],b(j)));
        };
        cons.shift=g(i);
        p->setLinearConstraint(keys[i],cons);
        cons.linear.clear();
      };
    };
  };
  //slon y
  vroot=*(gVertex[3].begin());
  proot=vroot->point();
  pp=project(vroot->point(),v1y->point(),v2y->point());
  posi(0)=(proot.x()-pp.x()); posi(1)=(proot.y()-pp.y()); posi(2)=(proot.z()-pp.z());
  imposedDef.mult(posi,g);

  for (std::set<MVertex*>::iterator it=gVertex[3].begin(); it!=gVertex[3].end(); it++){
    MVertex* v=*it;
    if (coinVertex.find(v)==coinVertex.end()){
      SPoint3 pt=project(v->point(),v1y->point(),v2y->point());
      double sj=distance(pt,v1x->point(),v2x->point());
      fullVector<double> b;
      yLag.getFunctionValues(sj,b);
      std::vector<Dof> keys;
      getKeysFromVertex(v,keys);
      int ndofs=keys.size();
      std::vector<std::vector<Dof> > k;
      for (int i=0; i<ylist.size(); i++){
        std::vector<Dof> ktemp;
        getKeysFromVertex(ylist[i],ktemp);
        k.push_back(ktemp);
      };
      DofAffineConstraint<double> cons;
      for (int i=0; i<ndofs; i++){
        for (int j=0; j<k.size(); j++){
          cons.linear.push_back(std::pair<Dof,double>(k[j][i],b(j)));
        };
        cons.shift=g(i);
        p->setLinearConstraint(keys[i],cons);
        cons.linear.clear();
      };
    };
  };
};

void polynomialInterpolationByLagrangeFuction::applyPeriodicCondition(){
  std::cout<<"------------Warning!--------------------------" <<std::endl;
  std::cout<<"------------Lagrange interpolation------------" <<std::endl;
  setInterpolationPoint();
  setLagrangeFunctionApproximation();
  formCondition();
  periodicCondition();
  cornerConstraint();
};

//---------------3D implementation------------------------
polynomialInterpolationByLagrangeFuction3D::polynomialInterpolationByLagrangeFuction3D(FunctionSpaceBase* sp, dofManager<double>* p, int deg):
            polynomialInterpolationBase3D(sp,p,deg){};

polynomialInterpolationByLagrangeFuction3D::polynomialInterpolationByLagrangeFuction3D(const int tag, const int dim, dofManager<double>* _p,
            int deg):polynomialInterpolationBase3D(tag,dim,_p,deg){
};

void polynomialInterpolationByLagrangeFuction3D::setLagrangeFunctionApproximation(){
  yLag.reset(); xLag.reset(); zLag.reset();
  for (int i=0; i<xlist.size(); i++){
    xLag.setValues(distance(xlist[i]->point(),v1y->point(),v2y->point()));
  };

  for (int i=0; i<ylist.size(); i++){
    yLag.setValues(distance(ylist[i]->point(),v1x->point(),v2x->point()));
  };

  for (int i=0; i<zlist.size(); i++){
    zLag.setValues(distance(zlist[i]->point(),v1y->point(),v2y->point()));
  };

  xLag.printValues();
  yLag.printValues();
  zLag.printValues();
};

void polynomialInterpolationByLagrangeFuction3D::lineFormCondition(vertexContainer container, std::vector<MVertex*> vlist,
                                        lagrangeFunctionApproximation lag, MVertex* vroot, MVertex* vtip){

  std::cout<<"Line form condition \n";
  for (vertexContainer::iterator it=container.begin(); it!=container.end(); it++){
    MVertex* v=*it;
    bool flag=true;
    for (int i=0; i<vlist.size(); i++){
      if (v->getNum()==vlist[i]->getNum()) flag=false;
    };
    if (flag){
      double sj=distance(v->point(),vroot->point(),vtip->point());
      fullVector<double> b;
      lag.getFunctionValues(sj,b);
      std::vector<Dof> keys;
      getKeysFromVertex(v,keys);
      int ndofs=keys.size();
      std::vector<std::vector<Dof> > k;
      for (int i=0; i<vlist.size(); i++){
        std::vector<Dof> ktemp;
        getKeysFromVertex(vlist[i],ktemp);
        k.push_back(ktemp);
      };
      DofAffineConstraint<double> cons;
      for (int i=0; i<ndofs; i++){
        for (int j=0; j<k.size(); j++){
          cons.linear.push_back(std::pair<Dof,double>(k[j][i],b(j)));
        };
        cons.shift=0.0;
        p->setLinearConstraint(keys[i],cons);
        cons.linear.clear();
      };
    };
  };
};

void polynomialInterpolationByLagrangeFuction3D::linePeriodicCondition(vertexContainer container, std::vector<MVertex*> vlist,
                           lagrangeFunctionApproximation lag, MVertex* vroot, MVertex* vtip, MVertex* lroot, MVertex* ltip){

  std::cout<<"Line periodic condition \n";
  MVertex* vr=*(container.begin());
  SPoint3 proot=vr->point();
  SPoint3 pp=project(vr->point(),lroot->point(),ltip->point());
  fullVector<double> posi;posi.resize(3);
  posi(0)=(proot.x()-pp.x()); posi(1)=(proot.y()-pp.y()); posi(2)=(proot.z()-pp.z());
  fullVector<double> g;g.resize(3);
  imposedDef.mult(posi,g);

  for (std::set<MVertex*>::iterator it=container.begin(); it!=container.end(); it++){
    MVertex* v=*it;
    if (coinVertex.find(v)==coinVertex.end()){
      SPoint3 pt=project(v->point(),lroot->point(),ltip->point());
      double sj=distance(pt,vroot->point(),vtip->point());
      fullVector<double> b;
      lag.getFunctionValues(sj,b);
      std::vector<Dof> keys;
      getKeysFromVertex(v,keys);
      int ndofs=keys.size();
      std::vector<std::vector<Dof> > k;
      for (int i=0; i<vlist.size(); i++){
        std::vector<Dof> ktemp;
        getKeysFromVertex(vlist[i],ktemp);
        k.push_back(ktemp);
      };
      DofAffineConstraint<double> cons;
      for (int i=0; i<ndofs; i++){
        for (int j=0; j<k.size(); j++){
          cons.linear.push_back(std::pair<Dof,double>(k[j][i],b(j)));
        };
        cons.shift=g(i);
        p->setLinearConstraint(keys[i],cons);
        cons.linear.clear();
      };
    };
  };
};

void polynomialInterpolationByLagrangeFuction3D::faceFormCondition(vertexContainer container, std::vector<MVertex*> xl, std::vector<MVertex*> yl,
                          lagrangeFunctionApproximation lagx, lagrangeFunctionApproximation lagy,
                          MVertex* v1, MVertex* v2, MVertex* v3, MVertex* v4){
  std::cout<<"face form condition \n";
  std::vector<Dof> k0;
  getKeysFromVertex(v1,k0);
  if (v1->getNum()!=v3->getNum()){
    std::cout<<" a node have to add to corner!" <<std::endl;
  }
  else{
    for (vertexContainer::iterator it=container.begin(); it!=container.end(); it++){
      MVertex* v=*it;
      bool flag=true;
      for (int i=0; i<xl.size(); i++){
        if (v->getNum()==xl[i]->getNum()) flag=false;
      };
      for (int i=0; i<yl.size(); i++){
        if (v->getNum()==yl[i]->getNum()) flag=false;
      };
      if (flag){
        double xi=distance(v->point(), v3->point(),v4->point());
        double neta=distance(v->point(), v1->point(), v2->point());
        //
        fullVector<double> bxi, bneta;
        lagx.getFunctionValues(xi,bxi);
        lagy.getFunctionValues(neta,bneta);
        //
        std::vector<Dof> keys;
        getKeysFromVertex(v,keys);
        int ndofs=keys.size();
        std::vector<std::vector<Dof> > kxi,kneta;
        for (int i=0; i<xl.size(); i++){
          std::vector<Dof> ktemp;
          getKeysFromVertex(xl[i],ktemp);
          kxi.push_back(ktemp);
        };
        for (int i=0; i<yl.size(); i++){
          std::vector<Dof> ktemp;
          getKeysFromVertex(yl[i],ktemp);
          kneta.push_back(ktemp);
        };
        DofAffineConstraint<double> cons;
        for (int i=0; i<ndofs; i++){
          for (int j=0; j<kxi.size(); j++){
            cons.linear.push_back(std::pair<Dof,double>(kxi[j][i],bxi(j)));
          };
          for (int j=0; j<kneta.size(); j++){
            cons.linear.push_back(std::pair<Dof,double>(kneta[j][i],bneta(j)));
          };
          cons.linear.push_back(std::pair<Dof,double>(k0[i],-1));
          cons.shift=0.0;
          p->setLinearConstraint(keys[i],cons);
          cons.linear.clear();
        };
      };
    };
  };
};

void polynomialInterpolationByLagrangeFuction3D::facePeriodicCondition(vertexContainer container, std::vector<MVertex*> xl, std::vector<MVertex*> yl,
                          lagrangeFunctionApproximation lagx, lagrangeFunctionApproximation lagy,
                          MVertex* v1, MVertex* v2, MVertex* v3, MVertex* v4){

  std::cout<<"face periodic condition \n";
  MVertex* vr=*(container.begin());
  SPoint3 proot=vr->point();
  SPoint3 pp=project(vr->point(),v1->point(),v2->point(),v4->point());
  fullVector<double> posi;posi.resize(3);
  posi(0)=(proot.x()-pp.x()); posi(1)=(proot.y()-pp.y()); posi(2)=(proot.z()-pp.z());
  fullVector<double> g;g.resize(3);
  imposedDef.mult(posi,g);

  std::vector<Dof> k0;
  getKeysFromVertex(v1,k0);
  if (v1->getNum()!=v3->getNum()){
    std::cout<<" a node have to add to corner!" <<std::endl;
  }
  else{
    for (vertexContainer::iterator it=container.begin(); it!=container.end(); it++){
      MVertex* v=*it;
      if (coinVertex.find(v)==coinVertex.end()){
        SPoint3 pt=project(v->point(),v1->point(),v2->point(),v4->point());
        double xi=distance(pt, v3->point(),v4->point());
        double neta=distance(pt, v1->point(), v2->point());
        //
        fullVector<double> bxi, bneta;
        lagx.getFunctionValues(xi,bxi);
        lagy.getFunctionValues(neta,bneta);
        //
        std::vector<Dof> keys;
        getKeysFromVertex(v,keys);
        int ndofs=keys.size();
        std::vector<std::vector<Dof> > kxi,kneta;
        for (int i=0; i<xl.size(); i++){
          std::vector<Dof> ktemp;
          getKeysFromVertex(xl[i],ktemp);
          kxi.push_back(ktemp);
        };
        for (int i=0; i<yl.size(); i++){
          std::vector<Dof> ktemp;
          getKeysFromVertex(yl[i],ktemp);
          kneta.push_back(ktemp);
        };
        DofAffineConstraint<double> cons;
        for (int i=0; i<ndofs; i++){
          for (int j=0; j<kxi.size(); j++){
            cons.linear.push_back(std::pair<Dof,double>(kxi[j][i],bxi(j)));
          };
          for (int j=0; j<kneta.size(); j++){
            cons.linear.push_back(std::pair<Dof,double>(kneta[j][i],bneta(j)));
          };
          cons.linear.push_back(std::pair<Dof,double>(k0[i],-1));
          cons.shift=g(i);
          p->setLinearConstraint(keys[i],cons);
          cons.linear.clear();
        };
      };
    };
  };
};

void polynomialInterpolationByLagrangeFuction3D::formCondition(){
  lineFormCondition(xline[0],xlist,xLag,v1y,v2y);
  lineFormCondition(yline[0],ylist,yLag,v1x,v2x);
  lineFormCondition(zline[0],zlist,zLag,v1y,v2y);

  faceFormCondition(gVertex[0],xlist,zlist,xLag,zLag,v1x,v2x,v1z,v2z);
  faceFormCondition(gVertex[1],ylist,zlist,yLag,zLag,v1y,v2y,v1z,v2z);
  faceFormCondition(gVertex[2],xlist,ylist,xLag,yLag,v1x,v2x,v1y,v2y);
};

void polynomialInterpolationByLagrangeFuction3D::periodicCondition(){
  linePeriodicCondition(xline[1],xlist,xLag,v1y,v2y,v1x,v2x);
  linePeriodicCondition(xline[2],xlist,xLag,v1y,v2y,v1x,v2x);
  linePeriodicCondition(xline[3],xlist,xLag,v1y,v2y,v1x,v2x);

  linePeriodicCondition(yline[1],ylist,yLag,v1x,v2x,v1y,v2y);
  linePeriodicCondition(yline[2],ylist,yLag,v1x,v2x,v1y,v2y);
  linePeriodicCondition(yline[3],ylist,yLag,v1x,v2x,v1y,v2y);

  linePeriodicCondition(zline[1],zlist,zLag,v1y,v2y,v1z,v2z);
  linePeriodicCondition(zline[2],zlist,zLag,v1y,v2y,v1z,v2z);
  linePeriodicCondition(zline[3],zlist,zLag,v1y,v2y,v1z,v2z);

  facePeriodicCondition(gVertex[3],xlist,zlist,xLag,zLag,v1x,v2x,v1z,v2z);
  facePeriodicCondition(gVertex[4],ylist,zlist,yLag,zLag,v1y,v2y,v1z,v2z);
  facePeriodicCondition(gVertex[5],xlist,ylist,xLag,yLag,v1x,v2x,v1y,v2y);
};

void polynomialInterpolationByLagrangeFuction3D::applyPeriodicCondition(){
  std::cout<<"--------Warning-----------------"<<std::endl;
  std::cout<<"--------3D lagrangian interpolation-----------------"<<std::endl;
  initialize();
  setLagrangeFunctionApproximation();
  formCondition();
  periodicCondition();
  cornerConstraint();
};
