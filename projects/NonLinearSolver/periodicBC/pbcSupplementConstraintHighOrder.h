#ifndef PBCSUPPLEMENTCONSTRAINTHIGHORDER_H_
#define PBCSUPPLEMENTCONSTRAINTHIGHORDER_H_

#include "pbcSupplementConstraint.h"


class supplementConstraintSecondOrder : public supplementConstraint,
                                        public constraintElementSecondOrder{
  protected:
		fullMatrix<double> _T; // constraint matrix
		STensor3 _XXmean; // int_S{0.5*X*X} dA

  public:
    supplementConstraintSecondOrder(FunctionSpaceBase* space, FunctionSpaceBase* mspace,
                                    int ndofs, groupOfElements* g);
    virtual ~supplementConstraintSecondOrder(){}

    virtual void getSecondOrderKinematicalMatrix(fullMatrix<double>& m) const;
		virtual void getSecondOrderKinematicalVector(const STensor33& G, fullVector<double>& m) const;
		virtual void getLinearConstraints(const STensor3& F, const STensor33& G,
                     std::map<Dof,DofAffineConstraint<double> >& con) const;
};

class lagrangeSupplementConstraintSecondOrder : public lagrangeSupplementConstraint,
                                                public constraintElementSecondOrder{
  protected:
    fullMatrix<double> _T;
    STensor3 _XXmean;

  public:
    lagrangeSupplementConstraintSecondOrder(FunctionSpaceBase* space, FunctionSpaceBase* mspace,
                                            int ndofs, std::vector<MVertex*>& v);
    virtual ~lagrangeSupplementConstraintSecondOrder(){};
    virtual void getSecondOrderKinematicalMatrix(fullMatrix<double>& m) const;
		virtual void getSecondOrderKinematicalVector(const STensor33& G, fullVector<double>& m) const;
		virtual void getLinearConstraints(const STensor3& F, const STensor33& G,
                     std::map<Dof,DofAffineConstraint<double> >& con) const;
};

class cubicSplineSupplementConstraintSecondOrder : public cubicSplineSupplementConstraint,
                                                  public constraintElementSecondOrder{
  protected:
    fullMatrix<double> _T;
    STensor3 _XXmean;

  public:
    cubicSplineSupplementConstraintSecondOrder(FunctionSpaceBase* space, FunctionSpaceBase* mspace,
                                              int ndofs, std::vector<MVertex*>& v, int flag = 0);
    virtual ~cubicSplineSupplementConstraintSecondOrder(){};
    virtual void getSecondOrderKinematicalMatrix(fullMatrix<double>& m) const;
		virtual void getSecondOrderKinematicalVector(const STensor33& G, fullVector<double>& m) const;
		virtual void getLinearConstraints(const STensor3& F, const STensor33& G,
                     std::map<Dof,DofAffineConstraint<double> >& con) const;
};

class CoonsPatchLagrangeSupplementConstraintSecondOrder : public CoonsPatchLagrangeSupplementConstraint,
                                                          public constraintElementSecondOrder{
  protected:
    fullMatrix<double> _T;
    STensor3 _XXmean;

  public:
    CoonsPatchLagrangeSupplementConstraintSecondOrder(FunctionSpaceBase* space, FunctionSpaceBase* mspace,
                                           int ndofs, lagrangeSupplementConstraint* conX,
                                           lagrangeSupplementConstraint* conY);
    virtual ~CoonsPatchLagrangeSupplementConstraintSecondOrder(){};
    virtual void getSecondOrderKinematicalMatrix(fullMatrix<double>& m) const;
		virtual void getSecondOrderKinematicalVector(const STensor33& G, fullVector<double>& m) const;
		virtual void getLinearConstraints(const STensor3& F, const STensor33& G,
                     std::map<Dof,DofAffineConstraint<double> >& con) const;
};

#endif // PBCSUPPLEMENTCONSTRAINTHIGHORDER_H_
