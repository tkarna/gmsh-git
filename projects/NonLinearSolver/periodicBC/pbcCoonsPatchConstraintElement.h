#ifndef PBCCOONSPATCHCONSTRAINTELEMENT_H_
#define PBCCOONSPATCHCONSTRAINTELEMENT_H_

#include "pbcConstraintElement.h"

class CoonsPatchCubicSplineConstraintElement : public constraintElement{
  protected:
    MVertex* _vp, *_reference;
    MVertex* _vnx1, *_vnx2, *_vny1, *_vny2;
    int _ndofs, _flagx, _flagy;
		SVector3 _L;
		double _xi, _neta, _x1, _x2, _y1, _y2;
		fullMatrix<double> _C, _S, _Cbc;
		fullVector<double> _FFx, _FFy;
		FunctionSpaceBase* _periodicSpace, *_multSpace;

  public:
    CoonsPatchCubicSplineConstraintElement(FunctionSpaceBase* lagspace, FunctionSpaceBase* multspace,
                                          int n, MVertex* vp, MVertex* vre, MVertex* vx1, MVertex* vx2,
                                          MVertex* vy1, MVertex* vy2, int flagx, int flagy);
    virtual ~CoonsPatchCubicSplineConstraintElement(){};
    virtual void getConstraintKeys(std::vector<Dof>& key) const; // real dofs on constraint elements
		virtual void getMultiplierKeys(std::vector<Dof>& key) const;	// multiplier dof on constraint element
		virtual void getConstraintMatrix(fullMatrix<double>& m) const;		// matrix C
		virtual void getFirstOrderKinematicalMatrix(fullMatrix<double>& m) const;	// matrix S
		virtual void getFirstOrderKinematicalVector(const STensor3& F, fullVector<double>& m) const;	// matrix g
		virtual void print() const;
		virtual void getDependentKeys(std::vector<Dof>& keys) const; // left real dofs
    virtual void getIndependentKeys(std::vector<Dof>& keys) const; // left real dofs
    virtual void getVirtualKeys(std::vector<Dof>& keys) const ; // for spline interpolation
    virtual void getLinearConstraints(const STensor3& F, std::map<Dof,DofAffineConstraint<double> >& con) const;
    virtual constraintElement::ElementType getType() const { return constraintElement::CubicSpline;};
    virtual bool isActive(const MVertex* v) const {
      if (_reference->getNum() == v->getNum() or _vp->getNum() == v->getNum() or
          _vnx1->getNum() == v->getNum()
          or _vnx2->getNum() == v->getNum() or _vny1->getNum() == _vny2->getNum()) return true;
      else return false;
    };
};


class CoonsPatchLagrangeConstraintElement : public constraintElement{
  protected:
    MVertex* _vp, *_reference;
		std::vector<MVertex*>& _vnx, &_vny;  // sequence vertices
		int _ndofs;
		SVector3 _L;
		std::vector<double> _distanceX, _distanceY;
		double _x, _y;
		fullMatrix<double> _C, _S, _Cbc;
		fullVector<double> _FFx, _FFy;
		FunctionSpaceBase* _periodicSpace, *_multSpace;

  public:
    CoonsPatchLagrangeConstraintElement(FunctionSpaceBase* lagspace, FunctionSpaceBase* multspace,
                                int n, MVertex* v1, MVertex* vref,
                                std::vector<MVertex*>& xlist,std::vector<MVertex*>& ylist);
    virtual ~CoonsPatchLagrangeConstraintElement(){};
    virtual void getConstraintKeys(std::vector<Dof>& key) const; // real dofs on constraint elements
		virtual void getMultiplierKeys(std::vector<Dof>& key) const;	// multiplier dof on constraint element
		virtual void getConstraintMatrix(fullMatrix<double>& m) const;		// matrix C
		virtual void getFirstOrderKinematicalMatrix(fullMatrix<double>& m) const;	// matrix S
		virtual void getFirstOrderKinematicalVector(const STensor3& FM, fullVector<double>& m) const;	// matrix g
		virtual void print() const;
		virtual void getDependentKeys(std::vector<Dof>& keys) const; // left real dofs
		virtual void getIndependentKeys(std::vector<Dof>& keys) const; // left real dofs
    virtual void getLinearConstraints(const STensor3& F, std::map<Dof,DofAffineConstraint<double> >& con) const;
    virtual constraintElement::ElementType getType() const { return constraintElement::Lagrange;};
    virtual bool isActive(const MVertex* v) const {
      if (_reference->getNum() == v->getNum()) return true;
      for (int i=0; i<_vnx.size();i++)
        if (_vnx[i]->getNum() == v->getNum()) return true;
      for (int i=0; i<_vny.size();i++)
        if (_vny[i]->getNum() == v->getNum()) return true;
      return false;
    };
};

#endif // PBCCOONSPATCHCONSTRAINTELEMENT_H_
