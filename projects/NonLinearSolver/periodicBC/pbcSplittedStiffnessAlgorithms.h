#ifndef PBCSPLITTEDSTIFFNESSALGORITHMS_H_
#define PBCSPLITTEDSTIFFNESSALGORITHMS_H_

#include "solverAlgorithms.h"

template<class Iterator, class Assembler> void AssembleSplittedStiffness(BilinearTermBase &term, FunctionSpaceBase &space,
                                                        Iterator itbegin, Iterator itend,
                                                        QuadratureBase &integrator, Assembler &assembler)
  // symmetric
{
  fullMatrix<double> localMatrix;
  std::vector<Dof> R;
  for (Iterator it = itbegin; it != itend; it++){
    MElement *e = *it;
    R.clear();
    IntPt *GP;
    int npts = integrator.getIntPoints(e, &GP);
    term.get(e, npts, GP, localMatrix); //localMatrix.print();
    space.getKeys(e, R);
    assembler.assemble(R, localMatrix);
  }
}



#endif // PBCSPLITTEDSTIFFNESSALGORITHMS_H_
