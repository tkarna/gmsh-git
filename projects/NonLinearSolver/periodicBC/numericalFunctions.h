#ifndef NUMERICALFUNCTIONS_H_
#define NUMERICALFUNCTIONS_H_
#include "SPoint3.h"
#include "SVector3.h"
#include "MVertex.h"
#include "groupOfElements.h"
#include "functionSpace.h"
#include "MPoint.h"
#include "highOrderTensor.h"
#include "STensor53.h"
#include "STensor63.h"

inline void computeSurface(SPoint3 A, SPoint3 B, SPoint3 C,double &a, double& b, double &c, double &d){
  SVector3 AB(A,B);
  SVector3 AC(A,C);
  SVector3 normal = crossprod(AB,AC);
  normal.normalize();
  a = normal[0];
  b = normal[1];
  c = normal[2];
  d = a*A[0]+b*A[1]+c*A[2];
};

inline SPoint3 project(SPoint3 P, SPoint3 A, SPoint3 B){
  SPoint3 root(0,0,0);
  SVector3 u(A,B);
  SVector3 Xpa(A,P);
  double t=dot(u,Xpa)/dot(u,u);
  SPoint3 temp(A.x()+t*u.x(),A.y()+t*u.y(),A.z()+t*u.z());
  return temp;
};

inline SPoint3 project(SPoint3 P, SPoint3 A, SPoint3 B, SPoint3 C){
  SVector3 vec1(A,B);
  SVector3 vec2(A,C);
  SVector3 normal=crossprod(vec1,vec2);
  SVector3 PA(P,A);
  double t=dot(normal,PA)/dot(normal,normal);
  SPoint3 temp(P.x()+t*normal.x(),P.y()+t*normal.y(),P.z()+t*normal.z());
  return temp;
};

inline double distance(SPoint3 P, SPoint3 A, SPoint3 B){
  SPoint3 H=project(P,A,B);
  double temp=P.distance(H);
  return temp;
};

inline double distance(SPoint3 P, SPoint3 A, SPoint3 B, SPoint3 C){
  SPoint3 H=project(P,A,B,C);
  double temp=P.distance(H);
  return temp;
};

// check P inside A, B
inline bool inside(SPoint3 P, SPoint3 A, SPoint3 B){
  SVector3 PA(P,A);SVector3 PB(P,B);
  if (dot(PA,PB)<=0) return true;
  else return false;
};

inline SPoint3 midpoint(SPoint3 A, SPoint3 B){
  double x = (A.x()+B.x())/2;
  double y = (A.y()+B.y())/2;
  double z = (A.z()+B.z())/2;
  SPoint3 pt(x,y,z);
  return pt;
}

// check same side P, Q via A, B
inline bool sameSide(SPoint3 P, SPoint3 Q, SPoint3 A, SPoint3 B){
  SVector3 PA(P,A);SVector3 PB(P,B);
  SVector3 n1=crossprod(PA,PB);

  SVector3 QA(Q,A); SVector3 QB(Q,B);
  SVector3 n2=crossprod(QA,QB);

  if (dot(n1,n2)<0) return false;
  else return true;
};

inline void getFF(double s, double s1, double s2, std::vector<double> &FF){
  FF.clear();
  double L=s2-s1;
  double r=(s-s1)/(s2-s1);
  FF.push_back(2*r*r*r-3*r*r+1);
  FF.push_back((r*r*r-2*r*r+r)*L);
  FF.push_back(3*r*r-2*r*r*r);
  FF.push_back((r*r*r-r*r)*L);
};


static void groupIntersection(groupOfElements* g1, groupOfElements* g2, std::set<MVertex*>& ver){
	for (groupOfElements::vertexContainer::iterator it1 = g1->vbegin(); it1!= g1->vend(); it1++){
		MVertex* v1 = *it1;
		for (groupOfElements::vertexContainer::iterator it2 = g2->vbegin(); it2!= g2->vend(); it2++){
			MVertex* v2 = *it2;
			if (v1->getNum() ==v2->getNum()){
				ver.insert(v1);
			};
		};
	};
};
static void groupIntersection(groupOfElements* g1, groupOfElements* g2, groupOfElements* &g){
	std::vector<MElement*> egroup;
	for (groupOfElements::elementContainer::iterator it1 = g1->begin(); it1!= g1->end(); it1++){
		MElement* e1 = *it1;
		for (groupOfElements::elementContainer::iterator it2 = g2->begin(); it2!= g2->end(); it2++){
			MElement* e2 = *it2;
			if (e1 == e2){
				egroup.push_back(e1);
			};
		};
	};
	g = new groupOfElements(egroup);
};
static void groupIntersection(std::set<MVertex*>& g1, std::set<MVertex*>& g2, std::set<MVertex*>& ver){
	for (std::set<MVertex*>::iterator it1 = g1.begin(); it1!= g1.end(); it1++){
		MVertex* v1 = *it1;
		for (std::set<MVertex*>::iterator it2 = g2.begin(); it2 != g2.end(); it2++){
			MVertex* v2 = *it2;
			if (v1==v2){
				ver.insert(v1);
			};
		};
	};
};


inline void getKeysFromVertex(FunctionSpaceBase* space, MVertex* v, std::vector<Dof>& keys){
	MPoint point(v,0,0);
	space->getKeys(&point,keys);
};
inline double norm(STensor3& ten){
	double temp = 0;
	for (int i=0; i<3; i++){
		for (int j=0; j<3; j++){
			double val = fabs(ten(i,j));
			if (val>temp){
				temp = val;
			};
		};
	};
	return temp;
};

inline void getMatrixFromVector(const SVector3& vec, fullMatrix<double>& mat){
	mat.resize(3,9);
	mat.setAll(0.0);
	for (int i=0; i<3; i++){
		for (int j=0; j<3; j++){
			int k = Tensor23::getIndex(i,j);
			mat(i,k) = vec(j);
		};
	};
};

inline void getMatrixFromTensor3(const STensor3& in, fullMatrix<double>& out){
	out.resize(3,27);
	out.setAll(0.0);
	for (int i=0; i<3; i++){
		for (int j=0; j<3; j++){
			for (int k=0; k<3; k++){
				int m = Tensor33::getIndex(i,j,k);
				out(i,m) = in(j,k);
			};
		};
	};
};

inline STensor3 vecprod(const SVector3& vec1, const SVector3& vec2){
  STensor3 ten(0.0);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      ten(i,j)= vec1(i)*vec2(j);
    };
  };
  return ten;
}

inline void multTensor3Vector(const STensor3& F, const SVector3& X, SVector3& result){
  for (int i=0; i<3; i++){
    result(i) = 0;
    for (int j=0; j<3; j++)
      result(i) += F(i,j)*X(j);
  }
}

static void printToFile(const fullMatrix<double>& mat, const std::string filename){
  FILE* file = fopen(filename.c_str(),"w");
  for (int i=0; i<mat.size1(); i++){
    for (int j=0; j<mat.size2(); j++){
      fprintf(file," \t%.16g\t" ,mat(i,j));
    }
    fprintf(file,"\n");
  }
	fclose(file);
}
static void printToFile(const fullVector<double>& vec, const std::string filename){
  FILE* file = fopen(filename.c_str(),"w");
  for (int i=0; i<vec.size(); i++){
    fprintf(file," \t%.16g\n" ,vec(i));
  }
	fclose(file);
}

#endif // NUMERICALFUNCTIONS_H_
