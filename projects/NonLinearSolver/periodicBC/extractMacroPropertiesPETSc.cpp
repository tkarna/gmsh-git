
#include "extractMacroPropertiesPETSc.h"
#include "nonLinearSystems.h"
#include "pbcSystems.h"
#include "functionPETSc.h"

#if defined(HAVE_PETSC)
/*
  Constraint bConMat  --> Cb* ub + Cc*uc = Sb(F -I)
  Constraint cConMat --> uc = Sc*(F-I)

  Stress: P = (SbT*lambdaB+ ScT*lambdaC)/Volume

  From equilibrium state:
  fi = 0
  fb = CbT* lambdaB
  fc = lambdaC+CcT*lambdaB

  => lambdaB = inv(Cb*CbT) * Cb* fb = RT* fb
  and  lambdaC = fc - CcT* lamdaB = fc+ minusCcT* lambdaB

*/

stiffnessCondensationPETSc::stiffnessCondensationPETSc(dofManager<double>* p, pbcAlgorithm* ma,
                               bool sflag, bool tflag,MPI_Comm com)
      :_pAssembler(p),_pAl(ma),_bConMat(NULL),_cConMat(NULL),
      _stressflag(sflag), _tangentflag(tflag){
			_stiffness = static_cast<splitStiffnessMatrixPETSc*>(_pAl->getSplitStiffness());
			_comm = com;
		};
stiffnessCondensationPETSc::~stiffnessCondensationPETSc(){
  this->clear();
};

void stiffnessCondensationPETSc::init(){
  this->clear();
  this->isInitialized() = true;
	pbcDofManager* pbcAssembler = _pAl->getSplittedDof();
	// for constraint form C_b u_b + C_c u_c = S_b (FM - I)
	// u_b == indirect dofs u_c == direct dofs
  _bConMat = new linearConstraintMatrixPETSc(_comm);
  // size of boundary constraints without direct constraints
  int nConstraint = pbcAssembler->sizeOfDependentMultiplierDof();
  // size of boundary dofs without direct dofs
  int nb = pbcAssembler->sizeOfDependentDof();
  // size of direct dofs
  int nc = pbcAssembler->sizeOfIndependentDof();
	_bConMat->allocate(nConstraint,nb,nc);
	_pAl->computeIndirectLinearConstraintMatrix(_bConMat);

	// for constraint form u_c = S_C (FM -I)
	_cConMat = new linearConstraintMatrixPETSc(_comm);
	_cConMat->allocate(nc);
	_pAl->computeDirectLinearConstraintMatrix(_cConMat);

  // get all Mat in linear constraint matrix
  Mat& Cb = _bConMat->getFirstMatrix();
  Mat& Cc = _bConMat->getSecondMatrix();
  Mat& Sb = _bConMat->getFirstOrderKinematicalMatrix();
  Mat& Sc = _cConMat->getFirstOrderKinematicalMatrix();

  // all necessary transpose matrix
  _try(MatTranspose(Sb,MAT_INITIAL_MATRIX,&_SbT));
  _try(MatAssemblyBegin(_SbT,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(_SbT,MAT_FINAL_ASSEMBLY));

  _try(MatTranspose(Cb,MAT_INITIAL_MATRIX,&_CbT));
  _try(MatAssemblyBegin(_CbT,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(_CbT,MAT_FINAL_ASSEMBLY));

  _try(MatTranspose(Sc,MAT_INITIAL_MATRIX,&_ScT));
  _try(MatAssemblyBegin(_ScT,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(_ScT,MAT_FINAL_ASSEMBLY));

  // inv(Cb*CbT) is necessary if computing stress -->compute from Lagrange multipliers
  if (_stressflag){
    _try(MatTranspose(Cc,MAT_INITIAL_MATRIX,&_minusCcT));
    _try(MatScale(_minusCcT,-1.));
    _try(MatAssemblyBegin(_minusCcT,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(_minusCcT,MAT_FINAL_ASSEMBLY));

    Mat CbCbT;
    _try(MatMatMult(Cb,_CbT,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&CbCbT));
    _try(MatAssemblyBegin(CbCbT,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(CbCbT,MAT_FINAL_ASSEMBLY));

    // create identity matrix
    Mat I, invCbCbT;
    _try(MatDuplicate(CbCbT,MAT_DO_NOT_COPY_VALUES,&I));
    _try(MatSetType(I,MATDENSE));

    PetscInt M, N;
    _try(MatGetSize(CbCbT,&M,&N));
    if (M != N) Msg::Fatal("it is not a squared matrix");

    _try(MatShift(I,1.));
    _try(MatAssemblyBegin(I,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(I,MAT_FINAL_ASSEMBLY));
    _try(MatDuplicate(I,MAT_DO_NOT_COPY_VALUES,&invCbCbT));

    _try(MatGetOrdering(CbCbT, "natural",  &perm,  &iperm));
    _try(MatFactorInfoInitialize(&info));
    _try(MatLUFactor(CbCbT,perm, iperm, &info));
    _try(MatMatSolve(CbCbT,I,invCbCbT));
    _try(MatAssemblyBegin(invCbCbT,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(invCbCbT,MAT_FINAL_ASSEMBLY));

    _try(MatDestroy(&I));
    _try(MatMatMult(invCbCbT,Cb,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&_RT));
    _try(MatAssemblyBegin(_RT,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(_RT,MAT_FINAL_ASSEMBLY));

    _try(MatDestroy(&CbCbT));
    _try(MatDestroy(&invCbCbT));
  };
};

void stiffnessCondensationPETSc::clear(){
  if (this->isInitialized()){
    this->isInitialized() = false;
    _try(MatDestroy(&_SbT));
    _try(MatDestroy(&_CbT));
    _try(MatDestroy(&_ScT));
    if (_stressflag){
      _try(MatDestroy(&_RT));
      _try(MatDestroy(&_minusCcT));
    };

    if (_bConMat) delete _bConMat;
    if (_cConMat) delete _cConMat;
    _bConMat = NULL;
    _cConMat = NULL;
  };
}

void stiffnessCondensationPETSc::stressSolve(){
  if (_stressflag){
    if (this->isInitialized() == false) {
      this->init();
    };
    pbcDofManager* pbcAssembler = _pAl->getSplittedDof();
    int sizeb = pbcAssembler->sizeOfDependentDof();
    int sizec = pbcAssembler->sizeOfIndependentDof();
    int sizelamdab = pbcAssembler->sizeOfDependentMultiplierDof();
    int sizelamdac = pbcAssembler->sizeOfIndependentMultiplierDof();
    // vec get value
    Vec lambdaB, lambdaC;
    _try(VecCreate(_comm,&lambdaB));
    _try(VecSetSizes(lambdaB,sizelamdab,PETSC_DECIDE));
    _try(VecSetFromOptions(lambdaB));

    _try(VecCreate(_comm,&lambdaC));
    _try(VecSetSizes(lambdaC,sizelamdac,PETSC_DECIDE));
    _try(VecSetFromOptions(lambdaC));

    // get linear system
    std::string str = "A";
    linearSystem<double>* lsys = _pAssembler->getLinearSystem(str);
    nonLinearSystem<double>* linearsys = dynamic_cast<nonLinearSystem<double>*>(lsys);
    pbcSystem<double>* pbcsys = dynamic_cast<pbcSystem<double>*>(lsys);
    if (pbcsys and linearsys){
      Vec fb, fc;
      _try(VecCreate(_comm,&fb));
      _try(VecSetSizes(fb,sizeb,PETSC_DECIDE));
      _try(VecSetFromOptions(fb));

      for (std::map<Dof,int>::const_iterator it = pbcAssembler->dependentDofBegin();
                                             it!= pbcAssembler->dependentDofEnd(); it++){
        double val = 0.;
        int num = _pAssembler->getDofNumber(it->first);
        if (num != -1)
          linearsys->getFromRightHandSideMinus(num, val);
        else
          Msg::Error("The pbcNonLinearSystem has to used");
        PetscInt row = it->second;
        _try(VecSetValues(fb,1,&row,&val,INSERT_VALUES));
      }
      _try(VecAssemblyBegin(fb));
      _try(VecAssemblyEnd(fb));

      _try(VecCreate(_comm,&fc));
      _try(VecSetSizes(fc,sizec,PETSC_DECIDE));
      _try(VecSetFromOptions(fc));

      for (std::map<Dof,int>::const_iterator it = pbcAssembler->independentDofBegin();
                                             it!= pbcAssembler->independentDofEnd(); it++){
        double val = 0.;
        int num = _pAssembler->getDofNumber(it->first);
        if (num != -1)
          linearsys->getFromRightHandSideMinus(num, val);
        else
          Msg::Error("The pbcNonLinearSystem has to used");
        PetscInt row = it->second;
        _try(VecSetValues(fc,1,&row,&val,INSERT_VALUES));
      }
      _try(VecAssemblyBegin(fc));
      _try(VecAssemblyEnd(fc));

      _try(MatMult(_RT,fb,lambdaB));
      _try(MatMultAdd(_minusCcT,lambdaB,fc,lambdaC));

      Vec P;
      _try(VecCreate(_comm,&P));
      _try(VecSetSizes(P,9,PETSC_DECIDE));
      _try(VecSetFromOptions(P));

      _try(MatMult(_SbT,lambdaB,P));
      _try(MatMultAdd(_ScT,lambdaC,P,P));
      _try(VecAssemblyBegin(P));
      _try(VecAssemblyEnd(P));
      #ifdef _DEBUG
      _try(VecView(P,PETSC_VIEWER_STDOUT_SELF));
      #endif

      double& rvevolume = this->getRVEVolume();
      STensor3& stress = this->getRefToMacroStress();
      PetscScalar* val;
      _try(VecGetArray(P,&val));
      for (int row = 0; row<9; row++){
        int i,j;
        Tensor23::getIntsFromIndex(row,i,j);
        stress(i,j) =val[row]/rvevolume;
      };

      _try(VecDestroy(&fb));
      _try(VecDestroy(&fc));
      _try(VecDestroy(&lambdaB));
      _try(VecDestroy(&lambdaC));
      _try(VecDestroy(&P));
    }
    else{
      Msg::Error("The pbc nonlinear solver have to used to compute");
    };
  };
}

void stiffnessCondensationPETSc::tangentCondensationSolve(){
  if (_tangentflag){
    if (this->isInitialized() == false){
      this->init();
    };
    // all submatrix created within the solver
    Mat& Kii = _stiffness->getIIMat();
    Mat& Kib = _stiffness->getIBMat();
    Mat& Kic = _stiffness->getICMat();

    Mat& Kbi = _stiffness->getBIMat();
    Mat& Kbb = _stiffness->getBBMat();
    Mat& Kbc = _stiffness->getBCMat();

    Mat& Kci = _stiffness->getCIMat();
    Mat& Kcb = _stiffness->getCBMat();
    Mat& Kcc = _stiffness->getCCMat();

    // first condensation step
    Mat Kbbt, Kbct, Kcbt, Kcct;

    // for factorize information
    pbcDofManager* pbcAssembler = static_cast<pbcDofManager*>(_pAl->getSplittedDof());
    int ni = pbcAssembler->sizeOfInternalDof();
    int nb = pbcAssembler->sizeOfDependentDof();
    int nc = pbcAssembler->sizeOfIndependentDof();
    if (ni>0){
      Mat matfac;
      _try(MatDuplicate(Kii,MAT_COPY_VALUES,&matfac));
      _try(MatAssemblyBegin(matfac,MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(matfac,MAT_FINAL_ASSEMBLY));
      _try(MatGetOrdering(matfac,  "natural",  &perm,  &iperm));
      _try(MatFactorInfoInitialize(&info));
      _try(MatLUFactor(matfac, perm, iperm, &info));
      if (nb>0)
        _try(GetSchurComplementMat(matfac,Kib,Kbi,Kbb,&Kbbt));
      if (nc>0)
        _try(GetSchurComplementMat(matfac,Kic,Kci,Kcc,&Kcct));
      if (nb>0 and nc>0){
        _try(GetSchurComplementMat(matfac,Kic,Kbi,Kbc,&Kbct));
        //_try(GetSchurComplementMat(matfac,Kib,Kci,Kcb,&Kcbt));
        _try(MatTranspose(Kbct,MAT_INITIAL_MATRIX,&Kcbt));
        _try(MatAssemblyBegin(Kcbt,MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(Kcbt,MAT_FINAL_ASSEMBLY));
      }
      _try(MatDestroy(&matfac));
    }
    else{
      if (nb>0)
        _try(MatDuplicate(Kbb,MAT_COPY_VALUES,&Kbbt));
      if (nc>0)
        _try(MatDuplicate(Kcc,MAT_COPY_VALUES,&Kcct));
      if ( nb>0 and nc>0){
        _try(MatDuplicate(Kbc,MAT_COPY_VALUES,&Kbct));
        _try(MatDuplicate(Kcb,MAT_COPY_VALUES,&Kcbt));
      }
    };

    Mat& Cc = _bConMat->getSecondMatrix();
    Mat& Cb = _bConMat->getFirstMatrix();
    Mat& Sb = _bConMat->getFirstOrderKinematicalMatrix();
    Mat& Sc = _cConMat->getFirstOrderKinematicalMatrix();

    // for mat Kbbt
    _try(MatGetOrdering(Kbbt,  "natural",  &perm,  &iperm));
    _try(MatFactorInfoInitialize(&info));
    _try(MatLUFactor(Kbbt, perm, iperm, &info));

    Mat Kccstar, Kblambda;
    _try(GetSchurComplementMat(Kbbt,Kbct,Kcbt,Kcct,&Kccstar));
    _try(GetSchurComplementMat(Kbbt,_CbT,Cb,PETSC_NULL,&Kblambda));

    Mat Cbc;
    _try(GetSchurComplementMat(Kbbt,Kbct,Cb,Cc,&Cbc));
    _try(MatScale(Cbc,-1.));

    Mat Sbc, SbcT;
    _try(MatMatMult(Cbc,Sc,MAT_INITIAL_MATRIX,1.,&Sbc));
    _try(MatAXPY(Sbc,1,Sb,DIFFERENT_NONZERO_PATTERN));
    _try(MatAssemblyBegin(Sbc,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(Sbc,MAT_FINAL_ASSEMBLY));
    _try(MatTranspose(Sbc,MAT_INITIAL_MATRIX,&SbcT));

    _try(MatDestroy(&Cbc));

    // for first-first
    _try(MatGetOrdering(Kblambda,  "natural",  &perm,  &iperm));
    _try(MatFactorInfoInitialize(&info));
    _try(MatLUFactor(Kblambda, perm, iperm, &info));

    Mat L1, L;
    _try(GetSchurComplementMat(Kblambda,Sbc,SbcT,PETSC_NULL,&L));
    Mat KccstarSc;
    _try(MatMatMult(Kccstar,Sc,MAT_INITIAL_MATRIX,1,&KccstarSc));
    _try(MatMatMult(_ScT,KccstarSc,MAT_INITIAL_MATRIX,1,&L1));

    _try(MatAXPY(L,1.,L1,DIFFERENT_NONZERO_PATTERN));
    _try(MatAssemblyBegin(L,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(L,MAT_FINAL_ASSEMBLY));


    if (nb>0)
      _try(MatDestroy(&Kbbt));
    if (nb>0 and nc>0){
      _try(MatDestroy(&Kbct));
      _try(MatDestroy(&Kcbt));
    }
    if (nc >0)
      _try(MatDestroy(&Kcct));
    _try(MatDestroy(&Sbc));
    _try(MatDestroy(&SbcT));
    _try(MatDestroy(&Kccstar));
    _try(MatDestroy(&KccstarSc));
    _try(MatDestroy(&L1));

    STensor43& tangent = this->getRefToMacroTangent();
    double& rvevolume = this->getRVEVolume();
    if (rvevolume >0){
      PetscInt M,N;
      _try(MatGetSize(L,&M,&N));
      //Msg::Info("M = %d, N = %d",M,N);
      for (int row = 0; row< M; ++row){
        int i,j;
        Tensor23::getIntsFromIndex(row,i,j);
        for (int col = 0; col<N; ++col){
          PetscScalar val;
          PetscInt ii = row, jj = col;
          _try(MatGetValues(L,1,&ii,1,&jj,&val));
          int k,l;
          Tensor23::getIntsFromIndex(col,k,l);
          tangent(i,j,k,l) = val/rvevolume;
        };
      };

      _try(MatDestroy(&L));

    }
    else{
      printf("Error in rve volume /n");
    };
  };
};

/*
  Constraint bConMat  --> Cb* ub + Cc*uc = S*(F -I) + T*G
  --> ub = Cbc* uc+  Sbc*(F-I) + Tbc* G

  Stress:
  P = SbcT*lambda/Volume
  Q = TbcT*lambda/Volume

  From equilibrium state:
  fi = 0
  fb = lambda
  fc = -CbcT*lambda

*/


stiffnessCondensationSecondOrderPETSc::stiffnessCondensationSecondOrderPETSc(dofManager<double>* p, pbcAlgorithm* ma,
                                          bool sflag, bool tflag, MPI_Comm com)
																				:_pAssembler(p),_pAl(ma),_bConMat(0),
																				_stressflag(sflag),_tangentflag(tflag){
  _stiffness = static_cast<splitStiffnessMatrixPETSc*>(_pAl->getSplitStiffness());
  _comm = com;
};
stiffnessCondensationSecondOrderPETSc::~stiffnessCondensationSecondOrderPETSc(){
  if (isInitialized()){
    this->clear();
  }
};

void stiffnessCondensationSecondOrderPETSc::init(){
  this->clear();
  this->isInitialized() = true;
  pbcDofManager* pbcAssembler = _pAl->getSplittedDof();
	// linea constraint matrix
	_bConMat = new linearConstraintMatrixSecondOrderPETSc();
	// size of constraints
	int nl = pbcAssembler->sizeOfMultiplierDof();
	// size of boundary dofs
	int nb = pbcAssembler->sizeOfBoundaryDof();
	// allocate the linear constraint
	_bConMat->allocate(nl,nb);
	// compute linear constraint by pbcAlgorithm
	_pAl->computeLinearConstraintMatrix(_bConMat);
	// constraint matrix
	Mat& C = _bConMat->getFirstMatrix();
  // first order matrix
  Mat& S = _bConMat->getFirstOrderKinematicalMatrix();
  // second order mareix
  Mat& T = _bConMat->getSecondOrderKinematicalMatrix();
  // dependent dof and independent dof
	int ndepen = pbcAssembler->sizeOfDependentDof();
	int nindepen = pbcAssembler->sizeOfIndependentDof();
  // split to two dependent matrix and independent matrix

  Msg::Error("nl = %d, nb = %d, ndepen = %d, nindepen = %d",nl,nb,ndepen,nindepen);

  Mat Cb, Cc;
  _try(MatCreate(_comm,&Cb));
  _try(MatSetSizes(Cb,PETSC_DECIDE,PETSC_DECIDE,nl,ndepen));
  _try(MatSetFromOptions(Cb));
  #if (((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)))
  _try(MatSetUp(Cb));
  #endif // (PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)

  _try(MatCreate(_comm,&Cc));
  _try(MatSetSizes(Cc,PETSC_DECIDE,PETSC_DECIDE,nl,nindepen));
  _try(MatSetType(Cc,MATDENSE));
  _try(MatSetFromOptions(Cc));
  #if (((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)))
  _try(MatSetUp(Cc));
  #endif // (PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)


  // begin splitting, by finding the dof in pbcDofManager
  for (int i=0; i<nl; i++){
    PetscInt row = i;
    for (std::map<Dof,int>::const_iterator it = pbcAssembler->boundaryDofBegin();
                                           it != pbcAssembler->boundaryDofEnd(); it++){
      PetscInt colC = it->second;
      PetscScalar s;
      _try(MatGetValues(C,1,&row,1,&colC,&s));
			if (fabs(s) > 0.){
				PetscInt col = pbcAssembler->getDependentDofNumber(it->first);
				if (col != -1){
					_try(MatSetValues(Cb,1,&row,1,&col,&s,ADD_VALUES));
				}
				else{
					col = pbcAssembler->getIndependentDofNumber(it->first);
					_try(MatSetValues(Cc,1,&row,1,&col,&s,ADD_VALUES));
				}
			}
    }
  }
  _try(MatAssemblyBegin(Cb,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(Cb,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyBegin(Cc,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(Cc,MAT_FINAL_ASSEMBLY));

  _try(MatDuplicate(Cc,MAT_DO_NOT_COPY_VALUES,&_Cbc));
  _try(MatDuplicate(S,MAT_DO_NOT_COPY_VALUES,&_Sbc));
  _try(MatDuplicate(T,MAT_DO_NOT_COPY_VALUES,&_Tbc));

/*
  Vec vv;
  _try(MatGetVecs(Cb,PETSC_NULL,&vv));
  _try(MatGetRowMaxAbs(Cb,vv,PETSC_NULL));
  VecToFile(vv,"vecVV.txt");
  MatToFile(Cb,"matCb.txt");
  */
  _try(MatGetOrdering(Cb, "natural",  &perm,  &iperm));
  _try(MatFactorInfoInitialize(&info));
  _try(MatLUFactor(Cb,perm, iperm, &info));

  _try(MatMatSolve(Cb,Cc,_Cbc));
  _try(MatScale(_Cbc,-1.));

  _try(MatMatSolve(Cb,S,_Sbc));
  _try(MatMatSolve(Cb,T,_Tbc));

  _try(MatDestroy(&Cb));
  _try(MatDestroy(&Cc));
  _try(MatAssemblyBegin(_Cbc,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(_Cbc,MAT_FINAL_ASSEMBLY));
  _try(MatTranspose(_Cbc,MAT_INITIAL_MATRIX,&_CbcT));
  _try(MatAssemblyBegin(_CbcT,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(_CbcT,MAT_FINAL_ASSEMBLY));

  _try(MatAssemblyBegin(_Sbc,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(_Sbc,MAT_FINAL_ASSEMBLY));
  _try(MatTranspose(_Sbc,MAT_INITIAL_MATRIX,&_SbcT));
  _try(MatAssemblyBegin(_SbcT,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(_SbcT,MAT_FINAL_ASSEMBLY));

  _try(MatAssemblyBegin(_Tbc,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(_Tbc,MAT_FINAL_ASSEMBLY));
  _try(MatTranspose(_Tbc,MAT_INITIAL_MATRIX,&_TbcT));
  _try(MatAssemblyBegin(_TbcT,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(_TbcT,MAT_FINAL_ASSEMBLY));
};

void stiffnessCondensationSecondOrderPETSc::clear(){
  if (isInitialized()){
    this->isInitialized() = false;
    _try(MatDestroy(&_Cbc));
    _try(MatDestroy(&_CbcT));
    _try(MatDestroy(&_Sbc));
    _try(MatDestroy(&_SbcT));
    _try(MatDestroy(&_Tbc));
    _try(MatDestroy(&_TbcT));
    if (_bConMat) delete _bConMat;
    _bConMat = NULL;
  }
}

void stiffnessCondensationSecondOrderPETSc::stressSolve(){
  if (_stressflag){
    if (!this->isInitialized()) {
      this->init();
    };
    pbcDofManager* pbcAssembler = _pAl->getSplittedDof();
    int sizedepend = pbcAssembler->sizeOfDependentDof();
    std::string str = "A";
    linearSystem<double>* lsys = _pAssembler->getLinearSystem(str);
    nonLinearSystem<double>* linearsys = dynamic_cast<nonLinearSystem<double>*>(lsys);
    pbcSystem<double>* pbcsys = dynamic_cast<pbcSystem<double>*>(lsys);
    if (linearsys and pbcsys){
      Vec fc;
      _try(VecCreate(_comm,&fc));
      _try(VecSetSizes(fc,PETSC_DECIDE,sizedepend));
      _try(VecSetFromOptions(fc));

      for (std::map<Dof,int>::const_iterator it = pbcAssembler->dependentDofBegin();
                                             it!= pbcAssembler->dependentDofEnd(); it++){
        double val = 0.;
        int num = _pAssembler->getDofNumber(it->first);
        if (num != -1)
          linearsys->getFromRightHandSideMinus(num, val);
        else
          Msg::Error("The pbcNonLinearSystem has to used");
        PetscInt row = it->second;
        _try(VecSetValues(fc,1,&row,&val,INSERT_VALUES));
      }
      _try(VecAssemblyBegin(fc));
      _try(VecAssemblyEnd(fc));

      Vec P, Q;
      _try(VecCreate(_comm,&P));
      _try(VecSetSizes(P,9,PETSC_DECIDE));
      _try(VecSetFromOptions(P));

      _try(VecCreate(_comm,&Q));
      _try(VecSetSizes(Q,27,PETSC_DECIDE));
      _try(VecSetFromOptions(Q));

      _try(MatMult(_SbcT,fc,P));
      _try(VecAssemblyBegin(P));
      _try(VecAssemblyEnd(P));

      _try(MatMult(_TbcT,fc,Q));
      _try(VecAssemblyBegin(Q));
      _try(VecAssemblyEnd(Q));


      //_try(VecView(P,PETSC_VIEWER_STDOUT_SELF));
      //_try(VecView(Q,PETSC_VIEWER_STDOUT_SELF));

      STensor3& stress = this->getRefToMacroStress();
      double& rvevolume = this->getRVEVolume();
      PetscScalar* val;
      _try(VecGetArray(P,&val));
      for (int row = 0; row<9; row++){
        int i,j;
        Tensor23::getIntsFromIndex(row,i,j);
        stress(i,j) =val[row]/rvevolume;
      };
      _try(VecGetArray(Q,&val));

      STensor33& highStress = this->getRefToMacroStressSecondOrder();
      for (int row = 0; row<27; row++){
        int i,j, k;
        Tensor33::getIntsFromIndex(row,i,j,k);
        highStress(i,j,k) =val[row]/rvevolume;
      };
      _try(VecDestroy(&fc));
      _try(VecDestroy(&P));
      _try(VecDestroy(&Q));
    }
    else{
      Msg::Error("The pbc nonlinear solver have to used to compute");
    };
  };
}



void stiffnessCondensationSecondOrderPETSc::tangentCondensationSolve(){
  if (_tangentflag){
    if (!isInitialized()) {
      this->init();
    }
    // get all submatrices on the splitted matrix
    Mat& Kii = _stiffness->getIIMat();
    Mat& Kib = _stiffness->getIBMat();
    Mat& Kic = _stiffness->getICMat();

    Mat& Kbi = _stiffness->getBIMat();
    Mat& Kbb = _stiffness->getBBMat();

    Mat& Kbc = _stiffness->getBCMat();

    Mat& Kci = _stiffness->getCIMat();
    Mat& Kcb = _stiffness->getCBMat();
    Mat& Kcc = _stiffness->getCCMat();

    // condense to other matrix by solving Schur problem
    Mat Kbbt, Kbct, Kcbt, Kcct;
    pbcDofManager* pbcAssembler = _pAl->getSplittedDof();
    int ni = pbcAssembler->sizeOfInternalDof();
    int nb = pbcAssembler->sizeOfDependentDof();
    int nc = pbcAssembler->sizeOfIndependentDof();
    if (ni>0){
      Mat matfac;
      _try(MatDuplicate(Kii,MAT_COPY_VALUES,&matfac));
      _try(MatAssemblyBegin(matfac,MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(matfac,MAT_FINAL_ASSEMBLY));

      _try(MatGetOrdering(matfac,  "natural",  &perm,  &iperm));
      _try(MatFactorInfoInitialize(&info));
      _try(MatLUFactor(matfac, perm, iperm, &info));
      if (nb>0)
        _try(GetSchurComplementMat(matfac,Kib,Kbi,Kbb,&Kbbt));
      if (nc>0)
        _try(GetSchurComplementMat(matfac,Kic,Kci,Kcc,&Kcct));
      if (nc>0 and nb>0){
        _try(GetSchurComplementMat(matfac,Kic,Kbi,Kbc,&Kbct));
        //_try(GetSchurComplementMat(matfac,Kib,Kci,Kcb,&Kcbt));
        _try(MatTranspose(Kbct,MAT_INITIAL_MATRIX,&Kcbt));
        _try(MatAssemblyBegin(Kcbt,MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(Kcbt,MAT_FINAL_ASSEMBLY));
      }
    _try(MatDestroy(&matfac));
    }
    else{
      if (nb>0)
        _try(MatDuplicate(Kbb,MAT_COPY_VALUES,&Kbbt));
      if (nc>0 and nb>0){
        _try(MatDuplicate(Kbc,MAT_COPY_VALUES,&Kbct));
        _try(MatDuplicate(Kcb,MAT_COPY_VALUES,&Kcbt));
      }
      if (nc>0)
        _try(MatDuplicate(Kcc,MAT_COPY_VALUES,&Kcct));
    };

    Mat Kstar;
    if (nc>0){
      Mat Kccstar, Kcbstar,KcbstarT;
      _try(MatMatMult(_CbcT,Kbbt,MAT_INITIAL_MATRIX,1.,&Kcbstar));
      _try(MatAXPY(Kcbstar,1,Kcbt,DIFFERENT_NONZERO_PATTERN));
      _try(MatAssemblyBegin(Kcbstar,MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(Kcbstar,MAT_FINAL_ASSEMBLY));
      _try(MatTranspose(Kcbstar,MAT_INITIAL_MATRIX,&KcbstarT));

      Mat mattemp;
      _try(MatMatMult(_CbcT,Kbct,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&mattemp));
      _try(MatMatMult(Kcbstar,_Cbc,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&Kccstar));
      _try(MatAXPY(Kccstar,1,Kcct,DIFFERENT_NONZERO_PATTERN));
      _try(MatAXPY(Kccstar,1,mattemp,DIFFERENT_NONZERO_PATTERN));
      _try(MatDestroy(&mattemp));
      _try(MatAssemblyBegin(Kccstar,MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(Kccstar,MAT_FINAL_ASSEMBLY));

      // for mat Kbbstar
      _try(MatGetOrdering(Kccstar,  "natural",  &perm,  &iperm));
      _try(MatFactorInfoInitialize(&info));
      _try(MatLUFactor(Kccstar, perm, iperm, &info));

      _try(GetSchurComplementMat(Kccstar,Kcbstar,KcbstarT,Kbbt,&Kstar));
      _try(MatDestroy(&Kccstar));
      _try(MatDestroy(&Kcbstar));
      _try(MatDestroy(&KcbstarT));
    }
    else{
      Kstar = Kbbt;
    }

    Mat KstarS, KstarT;
    _try(MatMatMult(Kstar,_Sbc,MAT_INITIAL_MATRIX,1.,&KstarS));
     _try(MatMatMult(Kstar,_Tbc,MAT_INITIAL_MATRIX,1.,&KstarT));

    Mat L;
    _try(MatMatMult(_SbcT,KstarS,MAT_INITIAL_MATRIX,1,&L));
    _try(MatAssemblyBegin(L,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(L,MAT_FINAL_ASSEMBLY));

    // for first-second
    Mat LPG;
    _try(MatMatMult(_SbcT,KstarT,MAT_INITIAL_MATRIX,1,&LPG));
    _try(MatAssemblyBegin(LPG,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(LPG,MAT_FINAL_ASSEMBLY));

    //_try(MatView(LPG,PETSC_VIEWER_STDOUT_WORLD));
    // for second first
    Mat LQF;
    _try(MatMatMult(_TbcT,KstarS,MAT_INITIAL_MATRIX,1,&LQF));
    _try(MatAssemblyBegin(LQF,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(LQF,MAT_FINAL_ASSEMBLY));

    // for second - seoncd
    Mat LQG;
    _try(MatMatMult(_TbcT,KstarT,MAT_INITIAL_MATRIX,1,&LQG));
    _try(MatAssemblyBegin(LQG,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(LQG,MAT_FINAL_ASSEMBLY));


    _try(MatDestroy(&Kstar));
    _try(MatDestroy(&KstarS));
    _try(MatDestroy(&KstarT));

    double& rvevolume = this->getRVEVolume();
    if (rvevolume >0){
      PetscInt M,N;
      _try(MatGetSize(L,&M,&N));
      //Msg::Info("M = %d, N = %d",M,N);

      STensor43& tangent = this->getRefToMacroTangent();

      for (int row = 0; row< M; ++row){
        int i,j;
        Tensor23::getIntsFromIndex(row,i,j);
        for (int col = 0; col<N; ++col){
          PetscScalar val;
          PetscInt ii = row, jj = col;
          _try(MatGetValues(L,1,&ii,1,&jj,&val));
          int k,l;
          Tensor23::getIntsFromIndex(col,k,l);
          tangent(i,j,k,l) = val/rvevolume;
        };
      };

      _try(MatGetSize(LPG,&M,&N));

      STensor53& tangent_PG = this->getRefToMacroTangentFirstSecond();
      for (int row = 0; row< M; row++){
        int i,j;
        Tensor23::getIntsFromIndex(row,i,j);
        for (int col = 0; col<N; col++){
          PetscScalar val;
          PetscInt ii = row, jj = col;
          _try(MatGetValues(LPG,1,&ii,1,&jj,&val));
          int k,l,m;
          Tensor33::getIntsFromIndex(col,k,l,m);
          tangent_PG(i,j,k,l,m) = val/rvevolume;
        };
      };

      _try(MatGetSize(LQF,&M,&N));
      STensor53& tangent_QF = this->getRefToMacroTangentSecondFirst();
        for (int row = 0; row< M; row++){
        int i,j,k;
        Tensor33::getIntsFromIndex(row,i,j,k);
        for (int col = 0; col<N; col++){
          PetscScalar val;
          PetscInt ii = row, jj = col;
          _try(MatGetValues(LQF,1,&ii,1,&jj,&val));
          int l,m;
          Tensor23::getIntsFromIndex(col,l,m);
          tangent_QF(i,j,k,l,m) = val/rvevolume;
        };
      };

      _try(MatGetSize(LQG,&M,&N));
      STensor63& tangent_QG= this->getRefToMacroTangentSecondSecond();
      for (int row = 0; row< M; row++){
        int i,j,k;
        Tensor33::getIntsFromIndex(row,i,j,k);
        for (int col = 0; col<N; col++){
          PetscScalar val;
          PetscInt ii = row, jj = col;
          _try(MatGetValues(LQG,1,&ii,1,&jj,&val));
          int l,m,n;
          Tensor33::getIntsFromIndex(col,l,m,n);
          tangent_QG(i,j,k,l,m,n) = val/rvevolume;
        };
      };

      _try(MatDestroy(&L));
      _try(MatDestroy(&LPG));
      _try(MatDestroy(&LQF));
      _try(MatDestroy(&LQG));
	_try(MatDestroy(&Kbbt));
	_try(MatDestroy(&Kbct));
	_try(MatDestroy(&Kcbt));
	_try(MatDestroy(&Kcct));
    }
    else{
      printf("Error in rve volume /n");
    };
  };
};



#endif // HAVE_PETSC
