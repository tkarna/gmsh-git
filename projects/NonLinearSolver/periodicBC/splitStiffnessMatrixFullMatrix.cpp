#include "splitStiffnessMatrixFullMatrix.h"

splitStiffnessMatrixFullMatrix::splitStiffnessMatrixFullMatrix():
              _isAllocated(false),_ni(0),_nb(0),_nc(0),
              _aii(0),_aib(0),_aic(0),_abi(0),_abb(0),_abc(0),_aci(0),_acb(0),_acc(0){};

splitStiffnessMatrixFullMatrix::~splitStiffnessMatrixFullMatrix(){
  this->clear();
};

bool splitStiffnessMatrixFullMatrix::isAllocated() const{return _isAllocated;};
void splitStiffnessMatrixFullMatrix::clear() {
  if (_isAllocated){
    _isAllocated = false;
    if (_ni>0)
      delete _aii;
    if (_ni>0 and _nb>0){
      delete _aib;
      delete _abi;
    }
    if (_nb>0)
      delete _abb;
    if (_ni>0 and _nc>0){
      delete _aic;
      delete _aci;
    }
    if (_nb>0 and _nc>0){
      delete _abc;
      delete _acb;
    }
    if (_nc>0){
      delete _acc;
    }
  }
};

void splitStiffnessMatrixFullMatrix::allocateSubMatrix(int ni, int nb, int nc){
  _ni = ni;
  _nb = nb;
  _nc = nc;
  this->clear();
  _isAllocated = true;
  if (_ni>0){
    _aii = new fullMatrix<double>(_ni,_ni);
  }
  if (_ni>0 and _nb>0){
    _aib = new fullMatrix<double>(_ni,_nb);
    _abi = new fullMatrix<double>(_nb,_ni);
  }
  if (_nb>0){
    _abb = new fullMatrix<double>(_nb,_nb);
  }
  if (_ni>0 and _nc>0){
    _aic = new fullMatrix<double>(_ni,_nc);
    _aci = new fullMatrix<double>(_nc,_ni);
  }
  if (_nb>0 and _nc>0){
    _abc = new fullMatrix<double>(_nb,_nc);
    _acb = new fullMatrix<double>(_nc,_nb);
  }
  if (_nc>0){
    _acc = new fullMatrix<double>(_nc,_nc);
  }
};

void splitStiffnessMatrixFullMatrix::zeroSubMatrix(){
  if (_isAllocated){
    if (_ni>0)
      _aii->setAll(0.);

    if (_ni>0 and _nb>0){
      _aib->setAll(0.);
      _abi->setAll(0.);
    }
    if (_nb>0)
      _abb->setAll(0.);
    if (_ni>0 and _nc>0){
      _aic->setAll(0.);
      _aci->setAll(0.);
    }
    if (_nb>0 and _nc>0){
      _abc->setAll(0.);
      _acb->setAll(0.);
    }
    if (_nc>0){
      _acc->setAll(0.);
    }
  };
};

void splitStiffnessMatrixFullMatrix::addToSubMatrix_II(int i, int j, const double &val){
  _aii->operator()(i,j) += val;
};

void splitStiffnessMatrixFullMatrix::addToSubMatrix_IB(int i, int j, const double& val){
  _aib->operator()(i,j) += val;
};

void splitStiffnessMatrixFullMatrix::addToSubMatrix_BI(int i, int j, const double& val){
  _abi->operator()(i,j) += val;
};

void splitStiffnessMatrixFullMatrix::addToSubMatrix_BB(int i, int j, const double& val){
  _abb->operator()(i,j) += val;
};

void splitStiffnessMatrixFullMatrix::addToSubMatrix_IC(int i, int j, const double& val){
  _aic->operator()(i,j) += val;
};

void splitStiffnessMatrixFullMatrix::addToSubMatrix_BC(int i, int j, const double& val){
  _abc->operator()(i,j) += val;
};

void splitStiffnessMatrixFullMatrix::addToSubMatrix_CI(int i, int j, const double& val){
  _aci->operator()(i,j) += val;
};

void splitStiffnessMatrixFullMatrix::addToSubMatrix_CB(int i, int j, const double& val){
  _acb->operator()(i,j) += val;
};

void splitStiffnessMatrixFullMatrix::addToSubMatrix_CC(int i, int j, const double& val){
  _acc->operator()(i,j)+= val;
};
fullMatrix<double>* splitStiffnessMatrixFullMatrix::getIIMat(){ return _aii; };
fullMatrix<double>* splitStiffnessMatrixFullMatrix::getIBMat(){ return _aib; };
fullMatrix<double>* splitStiffnessMatrixFullMatrix::getBIMat(){ return _abi; };
fullMatrix<double>* splitStiffnessMatrixFullMatrix::getBBMat(){ return _abb; };
fullMatrix<double>* splitStiffnessMatrixFullMatrix::getICMat(){ return _aic; };
fullMatrix<double>* splitStiffnessMatrixFullMatrix::getBCMat(){ return _abc; };
fullMatrix<double>* splitStiffnessMatrixFullMatrix::getCIMat(){ return _aci; };
fullMatrix<double>* splitStiffnessMatrixFullMatrix::getCBMat(){ return _acb; };
fullMatrix<double>* splitStiffnessMatrixFullMatrix::getCCMat(){ return _acc; };

