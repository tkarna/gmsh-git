#ifndef EXTRACTMACROPROPERTIESINTERPOLATIONPETSC_H_
#define EXTRACTMACROPROPERTIESINTERPOLATIONPETSC_H_

#include "extractMacroProperties.h"
#include "linearConstraintMatrixPETSc.h"
#include "splitStiffnessMatrixPETSc.h"
#include "GmshConfig.h"


#if defined(HAVE_PETSC)
#include "petscmat.h"
#include "petscvec.h"
#endif

class interpolationStiffnessCondensationPETSC : public stiffnessCondensation{
  #if defined(HAVE_PETSC)
  protected:
    splitStiffnessMatrixPETSc* _stiffness;
		dofManager<double>* _pAssembler;
		linearConstraintMatrixPETSc* _bConMat, *_cConMat;
		pbcAlgorithm* _pAl;
		bool _tangentflag, _stressflag;
		Mat _CT, _Sbc, _SbcT, _ScT;

    MPI_Comm _comm;
    MatFactorInfo info;
    IS perm,  iperm;

	public:
		interpolationStiffnessCondensationPETSC(dofManager<double>* p, pbcAlgorithm* ma,
                                            bool sflag, bool tflag, MPI_Comm com = PETSC_COMM_SELF);
		virtual ~interpolationStiffnessCondensationPETSC();

		virtual void init();
		virtual void clear();
		virtual void stressSolve();
		virtual void tangentCondensationSolve();


  #else
  public:
    interpolationStiffnessCondensationPETSC(dofManager<double>* p, pbcAlgorithm* ma,
                                            bool sflag, bool tflag){
      Msg::Error("Pestc is required");
    }
    virtual ~interpolationStiffnessCondensationPETSC(){}
    virtual void init(){};
    virtual void clear(){};
		virtual void stressSolve(){};
		virtual void tangentCondensationSolve(){};
  #endif //HAVE_PETSC
};
#endif // EXTRACTMACROPROPERTIESINTERPOLATIONPETSC_H_
