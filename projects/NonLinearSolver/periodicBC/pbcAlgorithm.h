#ifndef PBCALGORITHM_H_
#define PBCALGORITHM_H_

#include "nonLinearBC.h"
#include "pbcConstraintElement.h"
#include "pbcCreateConstraints.h"
#include "linearConstraintMatrix.h"
#include "splitStiffnessMatrix.h"
#include "pbcDofManager.h"

class pbcAlgorithm{
	protected:
		nonLinearMicroBC* _mbc;	  // for micro BC
		FunctionSpaceBase* _space, *_mspace;  // function space
		dofManager<double>* _pAssembler;	// dofManager
		std::vector<partDomain*>& _allDomain; // alldomain
		pbcConstraintElementGroup* _pbcConstraint;  // all linear periodic groups
		pbcDofManager* _splittedDofs;
		double _scale;	//factor scale
		bool _isSplitted;

  protected:
    void __int__();

	public:
		pbcAlgorithm(nonLinearMicroBC* bc,std::vector<partDomain*>& d, dofManager<double>*p);
		virtual ~pbcAlgorithm();

    pbcDofManager* getSplittedDof() {return _splittedDofs;};
    splitStiffnessMatrix* getSplitStiffness() {return _splittedDofs->getSplitStiffnessMatrix();};
    pbcConstraintElementGroup*  getPBCConstraintGroup() {return _pbcConstraint;};
    bool isSplittedDof() const {return _isSplitted;};

		int getMicroConstraintSize() const{return _pbcConstraint->size();};

    void splitDofs(); // split dofs

    void numberDof(const int type); // number Dof

    // for lagrange multipliers method : multipliers are accounted as normal dofs
		void assembleConstraintMatrix(); // add to stiffness matrix
		void assembleRightHandSide();	   // assemble external force vector
		void computeLinearConstraints(); // recompute the linear constraint

    // for lagrange multiplier method, all multipliers are eliminated
		void allocateConstraintMatrixToSystem();
		void assembleLinearConstraintMatrixToSystem();
		void assembleConstraintResidualToSystem();

		// apply PBC with constraint elimination
    void applyPBCByConstraintElimination();

		// write data to linear constraint matrix to condense the tangent
    void computeDirectLinearConstraintMatrix(linearConstraintMatrix* lcon);
    void computeIndirectLinearConstraintMatrix(linearConstraintMatrix* lcon);
    void computeLinearConstraintMatrix(linearConstraintMatrix* lcon);
    void computeLinearConstraintMatrixLDBC(linearConstraintMatrix* lcon);

    void computeLoadVector();
};

#endif // PBCALGORITHM_H_
