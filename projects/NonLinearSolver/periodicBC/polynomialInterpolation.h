#ifndef POLYNOMIALINTERPOLATION_H_
#define POLYNOMIALINTERPOLATION_H_

#include "periodicConstraints.h"


class polynomialInterpolationBase2D:public periodicConstraintsBase{
  protected:
    int degree;
    std::vector<MVertex*> xlist, ylist;

  protected:
    void getTwoFurthestVertexs(vertexContainer container, std::vector<MVertex*> &vlist);
    void getInterpolationPoint(std::vector<MVertex*> extreme, vertexContainer container, std::vector<MVertex*> &vlist);
    void setInterpolationPoint();

  public:
    polynomialInterpolationBase2D(FunctionSpaceBase* sp, dofManager<double>* _p, int deg);
    polynomialInterpolationBase2D(const int tag, const int dim, dofManager<double>* _p,int deg);
    ~polynomialInterpolationBase2D();
    void setPolynomialOrder(int i);
    void getPolynomialOrder(int &i);
};

class polynomialInterpolationBase3D: public polynomialInterpolationBase2D{
  protected:
    std::vector<MVertex*> zlist;
    std::vector<vertexContainer> xline, yline, zline;
    MVertex* v1x, *v2x, *v1y, *v2y, *v1z, *v2z;

  protected:
    void initialize();

  public:
    polynomialInterpolationBase3D(FunctionSpaceBase* sp, dofManager<double>* p, int deg);
    polynomialInterpolationBase3D(const int tag, const int dim, dofManager<double>* _p,int deg);
    ~polynomialInterpolationBase3D(){};


};
#endif // POLYNOMIALINTERPOLATION_H_
