#ifndef EXTRACTMACROPROPERTIESMKBCPETSC_H_
#define EXTRACTMACROPROPERTIESMKBCPETSC_H_

#include "extractMacroPropertiesPETSc.h"
#include "GmshConfig.h"

#if defined(HAVE_PETSC)
#include "petscmat.h"
#include "petscvec.h"
#endif

class stiffnessCondensationMKBCPETSc : public stiffnessCondensation{
  #if defined(HAVE_PETSC)
  protected:
    splitStiffnessMatrixPETSc* _stiffness; // split stiffness matrix
		dofManager<double>* _pAssembler; // dof mananger
		linearConstraintMatrixPETSc* _bConMat; // all linear constraint matrix
		pbcAlgorithm* _pAl; // pbc algorithm
		bool _tangentflag,_stressflag; // stress and tangent flag
		Mat _Cbc, _CbcT, _Sbc, _SbcT; // all mat

    MPI_Comm _comm; // Comunicator MPI

  private:
    MatFactorInfo info;
    IS perm,  iperm;
  public:
    stiffnessCondensationMKBCPETSc(dofManager<double>* p, pbcAlgorithm* ma,
                                    bool sflag, bool tflag, MPI_Comm com = PETSC_COMM_SELF);
    virtual ~stiffnessCondensationMKBCPETSc();
    virtual void init();
		virtual void clear();
		virtual void stressSolve();
		virtual void tangentCondensationSolve();
  #else
  public:
    stiffnessCondensationMKBCPETSc(dofManager<double>* p, pbcAlgorithm* ma,
                                    bool sflag, bool tflag){
      Msg::Error("Petsc is required");
    };
    virtual ~stiffnessCondensationMKBCPETSc(){};
    virtual void init()  {};
		virtual void clear() {};
		virtual void stressSolve() {};
		virtual void tangentCondensationSolve(){};

  #endif // HAVE_PETSC
};
#endif // EXTRACTMACROPROPERTIESMKBCPETSC_H_
