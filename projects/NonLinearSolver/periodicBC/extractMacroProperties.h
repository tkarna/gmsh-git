#ifndef EXTRACTMACROPROPERTIES_H_
#define EXTRACTMACROPROPERTIES_H_

#include "highOrderTensor.h"
#include "pbcAlgorithm.h"
#include "STensor53.h"
#include "STensor63.h"


class stiffnessCondensation{
  private:
    bool _isInitialized;
    double _rvevolume;
    STensor3 _stress; // stress
    STensor43 _tangent; // tangent

	public:
    stiffnessCondensation(): _isInitialized(false),_rvevolume(0),_stress(0),_tangent(0){};
		virtual ~stiffnessCondensation(){};
		void setRVEVolume( const double val){_rvevolume = val;};
		double getRVEVolume() const{return _rvevolume;};
		double& getRVEVolume() {return _rvevolume;};
		bool isInitialized() const {return _isInitialized;};
		bool& isInitialized(){return _isInitialized;};

		virtual void init() = 0;
		virtual void clear() = 0;
		virtual void stressSolve() = 0;
		virtual void tangentCondensationSolve() = 0;

    // get First order stress
		virtual const STensor3& getConstRefToMacroStress() const {return _stress;};
		virtual STensor3& getRefToMacroStress() {return _stress;};
    // get first order tangent moduli
		virtual const STensor43& getConstRefToMacroTangent() const {return _tangent;};
		virtual STensor43& getRefToMacroTangent() {return _tangent;};

		virtual const STensor3* getPointerToMacroStress() const  {return &_stress;};
		virtual const STensor43* getPointerToMacroTangent() const  {return& _tangent;};
		virtual STensor3* getPointerToMacroStress()  {return &_stress;};
		virtual STensor43* getPointerToMacroTangent()  {return& _tangent;};
};

class stiffnessCondensationSecondOrder : public stiffnessCondensation{
  private:
    STensor33 _highStress;
    STensor53 _tangent_PG, _tangent_QF;
    STensor63 _tangent_QG;

	public:
    stiffnessCondensationSecondOrder():stiffnessCondensation(),_highStress(0),_tangent_PG(0),
                                      _tangent_QF(0),_tangent_QG(0){}
		virtual ~stiffnessCondensationSecondOrder(){};

		virtual void init() = 0;
		virtual void clear() = 0;
		virtual void stressSolve() = 0;
		virtual void tangentCondensationSolve() = 0;


		virtual const STensor33& getConstRefToMacroStressSecondOrder() const {return _highStress;};
		virtual STensor33&  getRefToMacroStressSecondOrder() {return _highStress;};

		virtual const STensor53& getConstRefToMacroTangentFirstSecond() const {return _tangent_PG;};
		virtual STensor53& getRefToMacroTangentFirstSecond() {return _tangent_PG;};

		virtual const STensor53& getConstRefToMacroTangentSecondFirst() const {return _tangent_QF;};
		virtual STensor53& getRefToMacroTangentSecondFirst() {return _tangent_QF;};

		virtual const STensor63& getConstRefToMacroTangentSecondSecond() const {return _tangent_QG;};
		virtual STensor63& getRefToMacroTangentSecondSecond() {return _tangent_QG;};

    virtual  STensor33* getPointerToMacroStressSecondOrder() {return &_highStress;};
		virtual  STensor53* getPointerToMacroTangentFirstSecond() {return &_tangent_PG;};
		virtual  STensor53* getPointerToMacroTangentSecondFirst()  {return &_tangent_QF;};
		virtual  STensor63* getPointerToMacroTangentSecondSecond()  {return &_tangent_QG;};

		virtual  const STensor33* getPointerToMacroStressSecondOrder() const {return &_highStress;};
		virtual  const STensor53* getPointerToMacroTangentFirstSecond() const {return &_tangent_PG;};
		virtual  const STensor53* getPointerToMacroTangentSecondFirst()  const{return &_tangent_QF;};
		virtual  const STensor63* getPointerToMacroTangentSecondSecond() const {return &_tangent_QG;};
};

#endif // EXTRACTMACROPROPERTIES_H_
