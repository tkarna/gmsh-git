#ifndef PBCDOFMANAGER_H_
#define PBCDOFMANAGER_H_

#include "dofManager.h"
#include "splitStiffnessMatrix.h"


// infeface for dof management in micro-scopic problem

class pbcDofManager{
  public:
		enum DofType {INTERNAL = 0, BOUNDARY, VIRTUAL,
									DEPENDENT, INDEPENDENT, REALBOUNDARY,REAL_DEPENDENT,REAL_INDEPENDENT,
									MULTIPLIER, DEPENDENT_MULTIPLIER, INDEPENDENT_MULTIPLIER,
									VIRTUAL_DEPENDENT, VIRTUAL_INDEPENDENT,
									NONE};
	protected:
    // for all interior dofs
    std::map<Dof,int> _internalDof;
		//all boundary dof = real dofs + virtual dofs = dependent + independent dofs
    std::map<Dof,int> _boundaryDof;
		// for all dependent dofs
    std::map<Dof,int> _dependentDof;
    //for all independent dofs
		std::map<Dof,int> _independentDof;

    // all real boundary dof = real dependent + real independent dofs
    std::map<Dof,int> _realBoundaryDof;
    // all real dependent dofs
    std::map<Dof,int> _realDependentDof;
    // all real independent dofs
    std::map<Dof,int> _realIndependentDof;
    // all virtual dofs for added dofs
    std::map<Dof,int> _virtualDof;
    // all dependent virtual Dof
    std::map<Dof,int> _dependentVirtualDof;
    // all independent virtual Dof
    std::map<Dof,int> _independentVirtualDof;

    // with first order scheme, _depentdentDof used as bounday without corner and
    // _independentDof used as the corner dofs
    // _multiplierDof is splitted to _bMultiplierDof and _cMultiplierDof
    std::map<Dof,int> _multiplierDof;		// for all multiplier dofs, numerated constraints information
    std::map<Dof,int> _dependentMultiplierDof; // dependent constraints,
    std::map<Dof,int> _independentMultiplierDof; // independent constraint

    splitStiffnessMatrix* _splittedStiffness; // for  splitted stiffness matrix

	public:
		pbcDofManager();
		virtual ~pbcDofManager();

		virtual void clear();

    void createSplitStiffnessMatrix();
    void allocateSplittedMatrix(int n1, int n2, int n3);
		void allocateSplittedMatrix();
    splitStiffnessMatrix* getSplitStiffnessMatrix();

		int sizeOfInternalDof() const {return _internalDof.size();};
    int sizeOfDependentDof() const {return _dependentDof.size();};
		int sizeOfIndependentDof() const {return _independentDof.size();};
		int sizeOfBoundaryDof() const{return _boundaryDof.size();};
		int sizeOfRealBoundaryDof() const {return _realBoundaryDof.size();};
		int sizeOfRealDependentDof() const {return _realDependentDof.size();};
		int sizeOfRealIndependentDof() const{return _realIndependentDof.size();};
    int sizeOfVirtualDof() const {return _virtualDof.size();};
    int sizeOfDependentVirtualDof() const{return _dependentVirtualDof.size();};
    int sizeOfIndependentVirtualDof() const{ return _independentVirtualDof.size();};
    int sizeOfMultiplierDof() const {return _multiplierDof.size();};
    int sizeOfDependentMultiplierDof() const {return _dependentMultiplierDof.size();};
    int sizeOfIndependentMultiplierDof() const {return _independentMultiplierDof.size();};

		std::map<Dof,int>::const_iterator internalDofBegin() const {return _internalDof.begin();};
		std::map<Dof,int>::const_iterator internalDofEnd() const {return _internalDof.end();};
		std::map<Dof,int>::const_iterator internalDofFind(const Dof& key) const{return _internalDof.find(key);};

    std::map<Dof,int>::const_iterator dependentDofBegin() const {return _dependentDof.begin();};
    std::map<Dof,int>::const_iterator dependentDofEnd() const {return _dependentDof.end();};
    std::map<Dof,int>::const_iterator dependentDofFind(const Dof& key) const{return _dependentDof.find(key);};

		std::map<Dof,int>::const_iterator independentDofBegin() const {return _independentDof.begin();};
		std::map<Dof,int>::const_iterator independentDofEnd() const {return _independentDof.end();};
		std::map<Dof,int>::const_iterator independentDofFind(const Dof& key) const{return _independentDof.find(key);};

    std::map<Dof,int>::const_iterator boundaryDofBegin() const{return _boundaryDof.begin();};
    std::map<Dof,int>::const_iterator boundaryDofEnd() const{return _boundaryDof.end();};
    std::map<Dof,int>::const_iterator boundaryDofFind(const Dof& key) const{return _boundaryDof.find(key);}

    std::map<Dof,int>::const_iterator realBoundaryDofBegin() const{return _realBoundaryDof.begin();};
    std::map<Dof,int>::const_iterator realBoundaryDofEnd() const{return _realBoundaryDof.end();}
    std::map<Dof,int>::const_iterator realBoundaryDofFind(const Dof& key) const{return _realBoundaryDof.find(key);}

    std::map<Dof,int>::const_iterator realDependentDofBegin() const{return _realDependentDof.begin();};
    std::map<Dof,int>::const_iterator realDependentDofEnd() const{return _realDependentDof.end();}
    std::map<Dof,int>::const_iterator realDependentDofFind(const Dof& key) const{return _realDependentDof.find(key);}

    std::map<Dof,int>::const_iterator realIndependentDofBegin() const{return _realIndependentDof.begin();};
    std::map<Dof,int>::const_iterator realIndependentDofEnd() const{return _realIndependentDof.end();}
    std::map<Dof,int>::const_iterator realIndependentDofFind(const Dof& key) const{return _realIndependentDof.find(key);}

    std::map<Dof,int>::const_iterator virtualDofBegin() const {return _virtualDof.begin();};
    std::map<Dof,int>::const_iterator virtualDofEnd() const {return _virtualDof.end();};
    std::map<Dof,int>::const_iterator virtualDofFind(const Dof& key) const{return _virtualDof.find(key);};

    std::map<Dof,int>::const_iterator dependentVirtualDofBegin() const {return _dependentVirtualDof.begin();};
    std::map<Dof,int>::const_iterator dependentVirtualDofEnd() const {return _dependentVirtualDof.end();};
    std::map<Dof,int>::const_iterator dependentVirtualDofFind(const Dof& key) const{return _dependentVirtualDof.find(key);};

    std::map<Dof,int>::const_iterator independentVirtualDofBegin() const {return _independentVirtualDof.begin();};
    std::map<Dof,int>::const_iterator independentVirtualDofEnd() const {return _independentVirtualDof.end();};
    std::map<Dof,int>::const_iterator independentVirtualDofFind(const Dof& key) const{return _independentVirtualDof.find(key);}

    std::map<Dof,int>::const_iterator multiplierDofBegin() const {return _multiplierDof.begin();};
		std::map<Dof,int>::const_iterator multiplierDofEnd() const {return _multiplierDof.end();};
		std::map<Dof,int>::const_iterator multiplierDofFind(const Dof& key) const{return _multiplierDof.find(key);};

		std::map<Dof,int>::const_iterator dependentMultiplierDofBegin() const {return _dependentMultiplierDof.begin();};
		std::map<Dof,int>::const_iterator dependentMultiplierDofEnd() const {return _dependentMultiplierDof.end();};
		std::map<Dof,int>::const_iterator dependentMultiplierDofFind(const Dof& key) const{return _dependentMultiplierDof.find(key);};

		std::map<Dof,int>::const_iterator independentMultiplierDofBegin() const {return _independentMultiplierDof.begin();};
		std::map<Dof,int>::const_iterator independentMultiplierDofEnd() const {return _independentMultiplierDof.end();};
		std::map<Dof,int>::const_iterator independentMultiplierDofFind(const Dof& key) const{return _independentMultiplierDof.find(key);};


		int getBoundaryDofNumber(const Dof& key) const{
      std::map<Dof,int>::const_iterator it = _boundaryDof.find(key);
      if (it!= _boundaryDof.end()) return it->second;
      else return -1;
		}
		int getInternalDofNumber(const Dof& key) const{
			std::map<Dof,int>::const_iterator it = _internalDof.find(key);
			if (it != _internalDof.end()) return it->second;
			else return -1;
		};
		int getDependentDofNumber(const Dof& key) const{
			std::map<Dof,int>::const_iterator it = _dependentDof.find(key);
			if (it != _dependentDof.end()) return it->second;
			else return -1;
		};
		int getIndependentDofNumber(const Dof& key) const{
      std::map<Dof,int>::const_iterator it = _independentDof.find(key);
			if (it != _independentDof.end()) return it->second;
			else return -1;
		}
		int getRealBoundaryDofNumber(const Dof& key) const{
      std::map<Dof,int>::const_iterator it = _realBoundaryDof.find(key);
      if (it != _realBoundaryDof.end()) return it->second;
      else return -1;
		}
		int getRealDependentDofNumber(const Dof& key) const{
      std::map<Dof,int>::const_iterator it = _realDependentDof.find(key);
      if (it != _realDependentDof.end()) return it->second;
      else return -1;
		}
		int getRealIndependentDofNumber(const Dof& key) const{
      std::map<Dof,int>::const_iterator it = _realIndependentDof.find(key);
      if (it != _realIndependentDof.end()) return it->second;
      else return -1;
		}
		int getVirtualDofNumber(const Dof& key) const{
      std::map<Dof,int>::const_iterator it = _virtualDof.find(key);
			if (it != _virtualDof.end()) return it->second;
			else return -1;
		}
		int getDependentVirtualDofNumber(const Dof& key) const{
      std::map<Dof,int>::const_iterator it = _dependentVirtualDof.find(key);
			if (it != _dependentVirtualDof.end()) return it->second;
			else return -1;
		}
		int getIndependentVirtualDofNumber(const Dof& key) const{
      std::map<Dof,int>::const_iterator it = _independentVirtualDof.find(key);
			if (it != _independentVirtualDof.end()) return it->second;
			else return -1;
		}
		int getMultiplierDofNumber(const Dof& key) const{
			std::map<Dof,int>::const_iterator it = _multiplierDof.find(key);
			if (it!= _multiplierDof.end()) return it->second;
			else return -1;
		};
		int getDependentMultiplierDofNumber(const Dof& key) const{
			std::map<Dof,int>::const_iterator it = _dependentMultiplierDof.find(key);
			if (it!= _dependentMultiplierDof.end()) return it->second;
			else return -1;
		};

		int getIndependentMultiplierDofNumber(const Dof& key) const{
			std::map<Dof,int>::const_iterator it = _independentMultiplierDof.find(key);
			if (it!= _independentMultiplierDof.end()) return it->second;
			else return -1;
		};


		inline void numberInternalDof(const Dof& key){
		  if (_boundaryDof.find(key) != _boundaryDof.end()) return;
			std::map<Dof,int>::iterator it = _internalDof.find(key);
			if (it == _internalDof.end()){
				unsigned int size = _internalDof.size();
				_internalDof[key] = size;
			};
		};
		inline void numberDependentDof(const Dof& key){
      if (_internalDof.find(key) != _internalDof.end()) return;
      if (_independentDof.find(key)!= _independentDof.end()) return;
      std::map<Dof,int>::iterator it = _dependentDof.find(key);
      if (it == _dependentDof.end()){
        unsigned int size = _dependentDof.size();
        _dependentDof[key] = size;
      };
    };
    inline void numberIndependentDof(const Dof& key){
			if (_internalDof.find(key) != _internalDof.end()) return;
			if (_dependentDof.find(key) != _dependentDof.end()) return;
			std::map<Dof,int>::iterator it = _independentDof.find(key);
			if (it == _independentDof.end()){
				unsigned int size = _independentDof.size();
				_independentDof[key] = size;
			};
		};
		inline void numberBoundaryDof(const Dof& key){
      if (_internalDof.find(key) != _internalDof.end()) return;
      std::map<Dof,int>::iterator it = _boundaryDof.find(key);
      if (it == _boundaryDof.end()){
        unsigned int size = _boundaryDof.size();
        _boundaryDof[key] = size;
      }
		}

		virtual inline void numberRealBoundaryDof(const Dof& key){
      if (_internalDof.find(key) != _internalDof.end()) return;
      if (_virtualDof.find(key) != _virtualDof.end()) return;
      std::map<Dof,int>::iterator it = _realBoundaryDof.find(key);
      if (it == _realBoundaryDof.end()){
        unsigned int size = _realBoundaryDof.size();
        _realBoundaryDof[key] = size;
      }
		}
		virtual inline void numberRealDependentDof(const Dof& key){
      if (_internalDof.find(key) != _internalDof.end()) return;
      if (_virtualDof.find(key) != _virtualDof.end()) return;
      if (_realIndependentDof.find(key) != _realIndependentDof.end()) return;
      std::map<Dof,int>::iterator it = _realDependentDof.find(key);
      if (it == _realDependentDof.end()){
        unsigned int size = _realDependentDof.size();
        _realDependentDof[key] = size;
      }
		}
		virtual inline void numberRealIndependentDof( const Dof& key){
      if (_internalDof.find(key) != _internalDof.end()) return;
      if (_virtualDof.find(key) != _virtualDof.end()) return;
      if (_realDependentDof.find(key) != _realDependentDof.end()) return;
      std::map<Dof,int>::iterator it = _realIndependentDof.find(key);
      if (it == _realIndependentDof.end()){
        unsigned int size = _realIndependentDof.size();
        _realIndependentDof[key] = size;
      }
		}

		virtual inline void numberVirtualDof(const Dof& key){
			if (_realIndependentDof.find(key) != _realIndependentDof.end()) return;
			if (_realDependentDof.find(key) != _realDependentDof.end()) return;
			if (_internalDof.find(key) != _internalDof.end()) return;
			std::map<Dof,int>::iterator it = _virtualDof.find(key);
			if (it == _virtualDof.end()){
				unsigned int size = _virtualDof.size();
				_virtualDof[key] = size;
			};
		};

		virtual inline void numberDependentVirtualDof(const Dof& key){
      if (_realIndependentDof.find(key) != _realIndependentDof.end()) return;
      if (_realDependentDof.find(key) != _realDependentDof.end()) return;
      if (_internalDof.find(key) != _internalDof.end()) return;
      if (_independentVirtualDof.find(key) != _independentVirtualDof.end()) return;
      std::map<Dof,int>::iterator it = _dependentVirtualDof.find(key);
      if (it == _dependentVirtualDof.end()){
        unsigned int size = _dependentVirtualDof.size();
        _dependentVirtualDof[key] = size;
      }
		};

		virtual inline void numberIndependentVirtualDof(const Dof& key){
      if (_realIndependentDof.find(key) != _realIndependentDof.end()) return;
      if (_realDependentDof.find(key) != _realDependentDof.end()) return;
      if (_internalDof.find(key) != _internalDof.end()) return;
      if (_dependentVirtualDof.find(key) != _dependentVirtualDof.end()) return;
      std::map<Dof,int>::iterator it = _independentVirtualDof.find(key);
      if (it == _independentVirtualDof.end()){
        unsigned int size = _independentVirtualDof.size();
        _independentVirtualDof[key] = size;
      }
		};

		virtual inline void numberMultiplierDof(const Dof& key){
			std::map<Dof,int>::iterator it = _multiplierDof.find(key);
			if (it == _multiplierDof.end()){
				unsigned int size = _multiplierDof.size();
				_multiplierDof[key] = size;
			};
		};
		virtual inline void numberDependentMultiplierDof(const Dof& key){
			if (_independentMultiplierDof.find(key) != _independentMultiplierDof.end()) return;
			std::map<Dof,int>::iterator it = _dependentMultiplierDof.find(key);
			if (it == _dependentMultiplierDof.end()){
				unsigned int size = _dependentMultiplierDof.size();
				_dependentMultiplierDof[key] = size;
			};
		};
		virtual inline void numberIndependentMultiplierDof(const Dof& key){
			if (_dependentMultiplierDof.find(key) != _dependentMultiplierDof.end()) return;
			std::map<Dof,int>::iterator it = _independentMultiplierDof.find(key);
			if (it == _independentMultiplierDof.end()){
				unsigned int size = _independentMultiplierDof.size();
				_independentMultiplierDof[key] = size;
			};
		};

		virtual void assembleSubMatrix(const Dof& R, const Dof& C, const double& val);
		virtual void assemble(const std::vector<Dof>& R, const fullMatrix<double>& mat){
			for (int i=0; i<R.size(); i++){
				for (int j=0; j<R.size(); j++){
					assembleSubMatrix(R[i],R[j],mat(i,j));
				};
			};
		};
		virtual void numberDof(const DofType type, dofManager<double>* p) const{
      if (type == pbcDofManager::INTERNAL){
        for (std::map<Dof,int>::const_iterator it = this->internalDofBegin(); it!= this->internalDofEnd(); it++){
          p->numberDof(it->first);
        };
      }
      else if (type == pbcDofManager::DEPENDENT){
        for (std::map<Dof,int>::const_iterator it = this->dependentDofBegin(); it!= this->dependentDofEnd(); it++){
          p->numberDof(it->first);
        };
      }
      else if (type == pbcDofManager::INDEPENDENT){
        for (std::map<Dof,int>::const_iterator it = this->independentDofBegin(); it!= this->independentDofEnd(); it++){
          p->numberDof(it->first);
        };
      }
      else if (type == pbcDofManager::VIRTUAL){
        for (std::map<Dof,int>::const_iterator it = this->virtualDofBegin(); it!= this->virtualDofEnd(); it++){
          p->numberDof(it->first);
        };
      }
      else if (type == pbcDofManager::MULTIPLIER){
        for (std::map<Dof,int>::const_iterator it = this->multiplierDofBegin(); it!= this->multiplierDofEnd(); it++){
          p->numberDof(it->first);
        };
      }
      else{
        Msg::Error("This Dof type does not need numbering");
      };
		};
		virtual void numberDofLAGMULT(dofManager<double>* p) const{
		  Msg::Info("Numbering all Dofs + multipliers");
		  for (std::map<Dof,int>::const_iterator it = this->internalDofBegin(); it!= this->internalDofEnd(); it++){
        p->numberDof(it->first);
      };
      for (std::map<Dof,int>::const_iterator it = this->dependentDofBegin(); it!= this->dependentDofEnd(); it++){
        p->numberDof(it->first);
      };
      for (std::map<Dof,int>::const_iterator it = this->independentDofBegin(); it!= this->independentDofEnd(); it++){
        p->numberDof(it->first);
      };
      for (std::map<Dof,int>::const_iterator it = this->virtualDofBegin(); it!= this->virtualDofEnd(); it++){
        p->numberDof(it->first);
      };
      for (std::map<Dof,int>::const_iterator it = this->multiplierDofBegin(); it!= this->multiplierDofEnd(); it++){
        p->numberDof(it->first);
      }

		}
		virtual void numberDofLAGMULTELIM(dofManager<double>* p) const {
		  Msg::Info("Numbering all Dofs");
		  for (std::map<Dof,int>::const_iterator it = this->internalDofBegin(); it!= this->internalDofEnd(); it++){
        p->numberDof(it->first);
      };
      for (std::map<Dof,int>::const_iterator it = this->dependentDofBegin(); it!= this->dependentDofEnd(); it++){
        p->numberDof(it->first);
      };
      for (std::map<Dof,int>::const_iterator it = this->independentDofBegin(); it!= this->independentDofEnd(); it++){
        p->numberDof(it->first);
      };
      for (std::map<Dof,int>::const_iterator it = this->virtualDofBegin(); it!= this->virtualDofEnd(); it++){
        p->numberDof(it->first);
      };
		};
};

#endif // PBCDOFMANAGER_H_
