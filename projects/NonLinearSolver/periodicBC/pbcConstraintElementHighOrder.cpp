
#include "pbcConstraintElementHighOrder.h"
#include "numericalFunctions.h"
#include "highOrderTensor.h"
#include "quadratureRules.h"

periodicMeshConstraintSecondOrder::periodicMeshConstraintSecondOrder(FunctionSpaceBase* lagspace, FunctionSpaceBase* multspace,int n, MVertex* vp, MVertex* vn):
                            periodicMeshConstraint(lagspace, multspace,n,vp,vn), constraintElementSecondOrder(),_XiXj(0.){
	SPoint3 positive(vp->point());
	SPoint3 negative(0.,0.,0.);
  if (vn) negative = vn->point();

  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      _XiXj(i,j) = 0.5*(positive[i]*positive[j]-negative[i]*negative[j]);

  _T.resize(_ndofs,27); _T.setAll(0.);
  fullMatrix<double> temp;
  getMatrixFromTensor3(_XiXj,temp);
  for (int i=0; i<_ndofs; i++)
    for (int j=0; j<temp.size2(); j++)
      _T(i,j) = temp(i,j);
};

void periodicMeshConstraintSecondOrder::getSecondOrderKinematicalMatrix(fullMatrix<double>& m) const{
	m = _T;
};

void periodicMeshConstraintSecondOrder::getSecondOrderKinematicalVector(const STensor33& G, fullVector<double>& m) const{
  m.resize(_ndofs);
  m.setAll(0.);
  for (int i=0; i<_ndofs; i++)
    for (int j=0; j<3; j++)
      for (int k=0; k<3; k++)
        m(i) += G(i,j,k)*_XiXj(j,k);
};

void periodicMeshConstraintSecondOrder::getLinearConstraints(const STensor3& F, const STensor33& G,
                     std::map<Dof,DofAffineConstraint<double> >& con) const{
  periodicMeshConstraint::getLinearConstraints(F,con);
  fullVector<double> g;
  getSecondOrderKinematicalVector(G,g);
  std::vector<Dof> kP;
  periodicMeshConstraint::getDependentKeys(kP);
  for (int i=0; i<kP.size(); i++)
    con[kP[i]].shift += g(i);
};


cubicSplineConstraintElementSecondOrder::cubicSplineConstraintElementSecondOrder(FunctionSpaceBase* lagspace, FunctionSpaceBase* multspace,
                                            int n, MVertex* v1, MVertex* v2, MVertex* v3, int flag)
                              : cubicSplineConstraintElement(lagspace,multspace,n,v1,v2,v3,flag),constraintElementSecondOrder(),_XiXj(0.){
  SPoint3 positive = _vp->point();
  SPoint3 np1 = _vn1->point();
  SPoint3 np2 = _vn2->point();
  SPoint3 negative = project(positive,np1,np2);
  for (int i=0; i<3; i++){
    for (int j=0; j<3; j++){
      _XiXj(i,j) = 0.5*(positive[i]*positive[j] - negative[i]*negative[j]);
    }
  }
  fullMatrix<double> temp;
  getMatrixFromTensor3(_XiXj,temp);
  _T.resize(_ndofs,27);
  for (int i=0; i<_ndofs; i++){
    for (int j=0; j<27; j++){
      _T(i,j) = temp(i,j);
    }
  }
};
cubicSplineConstraintElementSecondOrder::~cubicSplineConstraintElementSecondOrder(){};

void cubicSplineConstraintElementSecondOrder::getSecondOrderKinematicalMatrix(fullMatrix<double>& m) const{
  m = _T;
};
void cubicSplineConstraintElementSecondOrder::getSecondOrderKinematicalVector(const STensor33& G, fullVector<double>& m) const{
  SVector3 temp= G*_XiXj;
	m.resize(_ndofs);
	for (int i=0; i<_ndofs; i++){
		m(i) = temp(i);
	};
};

void cubicSplineConstraintElementSecondOrder::getLinearConstraints(const STensor3& F, const STensor33& G,
                     std::map<Dof,DofAffineConstraint<double> >& con) const{
  cubicSplineConstraintElement::getLinearConstraints(F,con);
  fullVector<double> g;
  getSecondOrderKinematicalVector(G,g);
  std::vector<Dof> kP;
  cubicSplineConstraintElement::getDependentKeys(kP);
  for (int i=0; i<kP.size(); i++)
    con[kP[i]].shift += g(i);
};


CoonsPatchCubicSplineConstraintElementSecondOrder::CoonsPatchCubicSplineConstraintElementSecondOrder(FunctionSpaceBase* lagspace, FunctionSpaceBase* multspace,
                                          int n, MVertex* vp, MVertex* vre, MVertex* vx1, MVertex* vx2,
                                          MVertex* vy1, MVertex* vy2, int flagx, int flagy):
                                          CoonsPatchCubicSplineConstraintElement(lagspace,multspace,n,vp,vre,vx1,vx2,vy1,vy2,flagx,flagy){
  SPoint3 pp = _vp->point();
  SPoint3 nx1 = _vnx1->point();
  SPoint3 nx2 = _vnx2->point();
  SPoint3 ny1 = _vny1->point();
  SPoint3 ny2 = _vny2->point();
  SPoint3 ref = _reference->point();
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      _XiXj(i,j) = 0.5*( pp[i]*pp[j] - _FFx(0)*nx1[i]*nx1[j] - _FFx(2)*nx2[i]*nx2[j]
          - _FFy(0)*ny1[i]*ny1[j] - _FFy(2)*ny2[i]*ny2[j]+ ref[i]*ref[j]);
  getMatrixFromTensor3(_XiXj,_T);
}

void CoonsPatchCubicSplineConstraintElementSecondOrder::getSecondOrderKinematicalMatrix(fullMatrix<double>& m) const{
  m = _T;
};
void CoonsPatchCubicSplineConstraintElementSecondOrder::getSecondOrderKinematicalVector(const STensor33& G, fullVector<double>& m) const{
	m.resize(_ndofs); m.setAll(0.);
	for (int i=0; i<_ndofs; i++)
    for (int j=0; j<3; j++)
      for (int k=0; k<3; k++)
        m(i) += G(i,j,k)*_XiXj(j,k);
};

void CoonsPatchCubicSplineConstraintElementSecondOrder::getLinearConstraints(const STensor3& F, const STensor33& G,
                     std::map<Dof,DofAffineConstraint<double> >& con) const{
  CoonsPatchCubicSplineConstraintElement::getLinearConstraints(F,con);
  fullVector<double> g;
  getSecondOrderKinematicalVector(G,g);
  std::vector<Dof> kP;
  CoonsPatchCubicSplineConstraintElement::getDependentKeys(kP);
  for (int i=0; i<kP.size(); i++)
    con[kP[i]].shift += g(i);
};


lagrangeConstraintElementSecondOrder::lagrangeConstraintElementSecondOrder(FunctionSpaceBase* lagspace, FunctionSpaceBase* multspace,
                                int n, MVertex* v1, std::vector<MVertex*>& vlist)
                                :lagrangeConstraintElement(lagspace,multspace,n,v1,vlist),constraintElementSecondOrder(),_XiXj(0.){
  SPoint3 positive = _vp->point();
  SPoint3 np1 = _vn[0]->point();
  SPoint3 np2 = _vn[_vn.size()-1]->point();
  SPoint3 negative = project(positive,np1,np2);

  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      _XiXj(i,j) = 0.5*(positive[i]*positive[j]);

  for (int k=0; k<_FF.size(); k++){
    SPoint3 pt = _vn[k]->point();
    for (int i=0; i<3; i++)
      for (int j=0; j<3; j++)
        _XiXj(i,j) -= _FF(k)*0.5*(pt[i]*pt[j]);
  }
  fullMatrix<double> temp;
  getMatrixFromTensor3(_XiXj,temp);
  _T.resize(_ndofs,27);
  for (int i=0; i<_ndofs; i++){
    for (int j=0; j<27; j++){
      _T(i,j) = temp(i,j);
    }
  }
};
lagrangeConstraintElementSecondOrder::~lagrangeConstraintElementSecondOrder(){};
void lagrangeConstraintElementSecondOrder::getSecondOrderKinematicalMatrix(fullMatrix<double>& m) const{
  m= _T;
};

void lagrangeConstraintElementSecondOrder::getSecondOrderKinematicalVector(const STensor33& G, fullVector<double>& m) const{
	m.resize(_ndofs); m.setAll(0.);
	for (int i=0; i<_ndofs; i++)
		for (int j=0; j<3; j++)
      for (int k=0; k<3; k++)
        m(i) += G(i,j,k)*_XiXj(j,k);
};

void lagrangeConstraintElementSecondOrder::getLinearConstraints(const STensor3& F, const STensor33& G,
                     std::map<Dof,DofAffineConstraint<double> >& con) const{
  lagrangeConstraintElement::getLinearConstraints(F,con);
  fullVector<double> g;
  getSecondOrderKinematicalVector(G,g);
  std::vector<Dof> kP;
  lagrangeConstraintElement::getDependentKeys(kP);
  for (int i=0; i<kP.size(); i++)
    con[kP[i]].shift += g(i);
};

CoonsPatchLagrangeConstraintElementSecondOrder::CoonsPatchLagrangeConstraintElementSecondOrder(FunctionSpaceBase* lagspace, FunctionSpaceBase* multspace,
                                                        int n, MVertex* v1, MVertex* vref,
                                                         std::vector<MVertex*>& xlist,
                                                         std::vector<MVertex*>& ylist):
                                        CoonsPatchLagrangeConstraintElement(lagspace,multspace,n,v1,vref,xlist,ylist){
  SPoint3 pp = _vp->point();
  SPoint3 ptref = _reference->point();
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      _XiXj(i,j) = 0.5*(pp[i]*pp[j] + ptref[i]*ptref[j]);

  for (int k=0; k<_FFx.size(); k++){
    SPoint3 pt = _vnx[k]->point();
    for (int i=0; i<3; i++)
      for (int j=0; j<3; j++)
       _XiXj(i,j) -= (0.5*_FFx(k)*pt[i]*pt[j]);
  }

  for (int k=0; k<_FFy.size(); k++){
    SPoint3 pt = _vny[k]->point();
    for (int i=0; i<3; i++)
      for (int j=0; j<3; j++)
       _XiXj(i,j) -= (0.5*_FFy(k)*pt[i]*pt[j]);
  }

  getMatrixFromTensor3(_XiXj,_T);
}

void CoonsPatchLagrangeConstraintElementSecondOrder::getSecondOrderKinematicalMatrix(fullMatrix<double>& m) const{
  m = _T;
};
void CoonsPatchLagrangeConstraintElementSecondOrder::getSecondOrderKinematicalVector(const STensor33& G, fullVector<double>& m)const{
  m.resize(3); m.setAll(0.);
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      for (int k=0; k<3; k++)
        m(i) += G(i,j,k)*_XiXj(j,k);
};
void CoonsPatchLagrangeConstraintElementSecondOrder::getLinearConstraints(const STensor3& F, const STensor33& G,
                 std::map<Dof,DofAffineConstraint<double> >& con) const{
  CoonsPatchLagrangeConstraintElement::getLinearConstraints(F,con);
  fullVector<double> g;
  getSecondOrderKinematicalVector(G,g);
  std::vector<Dof> kP;
  CoonsPatchLagrangeConstraintElement::getDependentKeys(kP);
  for (int i=0; i<kP.size(); i++)
    con[kP[i]].shift += g(i);
};

FEConstraintElementSecondOrder::FEConstraintElementSecondOrder(FunctionSpaceBase* space, FunctionSpaceBase* mspace,
                        int n, MVertex* vp, MElement* en): FEConstraintElement(space,mspace,n,vp,en){
  SPoint3 positive = _v->point();
  std::vector<MVertex*> vv;
  _e->getVertices(vv);
  int size = vv.size();

  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      _XiXj(i,j) = 0.5*(positive[i]*positive[j]);

  for (int k=0; k<size; k++){
    SPoint3 pt = vv[k]->point();
    for (int i=0; i<3; i++)
      for (int j=0; j<3; j++)
        _XiXj(i,j) -= _fVal[k]*0.5*(pt[i]*pt[j]);
  }
  fullMatrix<double> temp;
  getMatrixFromTensor3(_XiXj,temp);
  _T.resize(_ndofs,27);
  for (int i=0; i<_ndofs; i++){
    for (int j=0; j<27; j++){
      _T(i,j) = temp(i,j);
    }
  }
}

void FEConstraintElementSecondOrder::getSecondOrderKinematicalMatrix(fullMatrix<double>& m) const{
  m = _T;
};
void FEConstraintElementSecondOrder::getSecondOrderKinematicalVector(const STensor33& G, fullVector<double>& m)const{
  m.resize(3); m.setAll(0.);
  for (int i=0; i<3; i++)
    for (int j=0; j<3; j++)
      for (int k=0; k<3; k++)
        m(i) += G(i,j,k)*_XiXj(j,k);
};
void FEConstraintElementSecondOrder::getLinearConstraints(const STensor3& F, const STensor33& G,
                 std::map<Dof,DofAffineConstraint<double> >& con) const{
  FEConstraintElement::getLinearConstraints(F,con);
  fullVector<double> g;
  getSecondOrderKinematicalVector(G,g);
  std::vector<Dof> kP;
  FEConstraintElement::getDependentKeys(kP);
  for (int i=0; i<kP.size(); i++)
    con[kP[i]].shift += g(i);
};
