
#include "linearConstraintMatrixPETSc.h"

#if defined(HAVE_PETSC)
linearConstraintMatrixPETSc::linearConstraintMatrixPETSc(MPI_Comm com):
                    _comm(com),_isAllocated(false),_row(0),_firstCol(0),_secondCol(0){};

linearConstraintMatrixPETSc::~linearConstraintMatrixPETSc(){
  this->clear();
};

bool linearConstraintMatrixPETSc::isAllocated() const {return _isAllocated;};

void linearConstraintMatrixPETSc::clear(){
  if (isAllocated()){
    _isAllocated= false;
    #if (PETSC_VERSION_RELEASE == 0 || ((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 2))) // petsc-dev
    if (_firstCol>0){
      _try(MatDestroy(&_firstMatrix));
    }
    if (_secondCol>0){
      _try(MatDestroy(&_secondMatrix));
    }
    _try(MatDestroy(&_firstOrderMatrix));
    #else
    if (_firstCol>0){
      _try(MatDestroy(_firstMatrix));
    }
    if (_secondCol>0){
      _try(MatDestroy(_secondMatrix));
    }
    _try(MatDestroy(_firstOrderMatrix));
    #endif
  };
};

void linearConstraintMatrixPETSc::allocate(int row, int first, int second){
  _row = row;
  _firstCol = first;
  _secondCol = second;
  this->clear();
  _isAllocated = true;
  if (_firstCol >0){
    _try(MatCreate(_comm,&_firstMatrix));
    _try(MatSetSizes(_firstMatrix,_row, _firstCol,PETSC_DETERMINE,PETSC_DETERMINE));
    _try(MatSetType(_firstMatrix,MATDENSE));
    _try(MatSetFromOptions(_firstMatrix));
    #if (((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)))
    _try(MatSetUp(_firstMatrix));
    #endif // (PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)
  }
  if (_secondCol>0){
    _try(MatCreate(_comm,&_secondMatrix));
    _try(MatSetSizes(_secondMatrix,_row,_secondCol,PETSC_DETERMINE,PETSC_DETERMINE));
    _try(MatSetType(_secondMatrix,MATDENSE));
    _try(MatSetFromOptions(_secondMatrix));
    #if (((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)))
    _try(MatSetUp(_secondMatrix));
    #endif // (PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)
  };

  _try(MatCreate(_comm,&_firstOrderMatrix));
  _try(MatSetSizes(_firstOrderMatrix,_row,9,PETSC_DETERMINE,PETSC_DETERMINE));
  _try(MatSetType(_firstOrderMatrix,MATDENSE));
  _try(MatSetFromOptions(_firstOrderMatrix));
  #if (((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)))
  _try(MatSetUp(_firstOrderMatrix));
  #endif // (PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)

};

void linearConstraintMatrixPETSc::addToFirstMatrix(int i, int j, double val){
  if (_firstCol <=0) return;
  if (val != 0.0){
    PetscInt ii = i, jj = j;
    PetscScalar s = val;
    _try(MatSetValues(_firstMatrix,1,&ii,1,&jj,&s,ADD_VALUES));
  }
}

void linearConstraintMatrixPETSc::addToSecondMatrix(int i, int j, double val){
  if (_secondCol <=0) return;
  if (val != 0.0){
    PetscInt ii = i, jj = j;
    PetscScalar s = val;
    _try(MatSetValues(_secondMatrix,1,&ii,1,&jj,&s,ADD_VALUES));
  }
}

void linearConstraintMatrixPETSc::addToFirstOrderKinematicalMatrix(int i, int j, double val){
  if (val != 0){
    PetscInt ii = i, jj = j;
    PetscScalar s = val;
    _try(MatSetValues(_firstOrderMatrix,1,&ii,1,&jj,&s,ADD_VALUES));
  }
}

Mat& linearConstraintMatrixPETSc::getFirstMatrix(){
  if (_firstCol>0){
    _try(MatAssemblyBegin(_firstMatrix,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(_firstMatrix,MAT_FINAL_ASSEMBLY));
  }
  return _firstMatrix;
};
Mat& linearConstraintMatrixPETSc::getSecondMatrix(){
  if (_secondCol >0){
    _try(MatAssemblyBegin(_secondMatrix,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(_secondMatrix,MAT_FINAL_ASSEMBLY));
  }
  return _secondMatrix;
};
Mat& linearConstraintMatrixPETSc::getFirstOrderKinematicalMatrix(){
  _try(MatAssemblyBegin(_firstOrderMatrix,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(_firstOrderMatrix,MAT_FINAL_ASSEMBLY));
  return _firstOrderMatrix;
};

linearConstraintMatrixSecondOrderPETSc::linearConstraintMatrixSecondOrderPETSc(MPI_Comm com)
              :linearConstraintMatrixPETSc(com){};

linearConstraintMatrixSecondOrderPETSc::~linearConstraintMatrixSecondOrderPETSc(){
  this->clear();
};

void linearConstraintMatrixSecondOrderPETSc::clear(){
  if (_isAllocated){
    linearConstraintMatrixPETSc::clear();
    #if (PETSC_VERSION_RELEASE == 0 || ((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 2))) // petsc-dev
    _try(MatDestroy(&_secondOrderMatrix));
    #else
    _try(MatDestroy(_secondOrderMatrix));
    #endif
  };
};

void linearConstraintMatrixSecondOrderPETSc::allocate(int row, int first, int second){
  this->clear();
  linearConstraintMatrixPETSc::allocate(row,first,second);
  _try(MatCreate(_comm,&_secondOrderMatrix));
  _try(MatSetSizes(_secondOrderMatrix,_row,27,PETSC_DETERMINE,PETSC_DETERMINE));
  _try(MatSetType(_secondOrderMatrix,MATDENSE));
  _try(MatSetFromOptions(_secondOrderMatrix));
  #if (((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)))
  _try(MatSetUp(_secondOrderMatrix));
  #endif // (PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)
};

void linearConstraintMatrixSecondOrderPETSc::addToSecondOrderKinematicalMatrix(int i, int j, double val){
  if (val != 0.){
    PetscInt ii = i, jj = j;
    PetscScalar s = val;
    _try(MatSetValues(_secondOrderMatrix,1,&ii,1,&jj,&s,ADD_VALUES));
  };
}
Mat& linearConstraintMatrixSecondOrderPETSc::getSecondOrderKinematicalMatrix(){
  _try(MatAssemblyBegin(_secondOrderMatrix,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(_secondOrderMatrix,MAT_FINAL_ASSEMBLY));
  return _secondOrderMatrix;
};

#endif //HAVE_PETSC
