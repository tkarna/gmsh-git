

#include "extractMacroPropertiesMKBCPETSc.h"
#include "pbcSystems.h"
#include "highOrderTensor.h"
#include "nonLinearSystems.h"
#include "functionPETSc.h"


#if defined(HAVE_PETSC)
stiffnessCondensationMKBCPETSc::stiffnessCondensationMKBCPETSc(dofManager<double>* p, pbcAlgorithm* ma,
                                    bool sflag, bool tflag, MPI_Comm com): stiffnessCondensation(),
                                    _pAssembler(p),_pAl(ma),_stressflag(sflag),_tangentflag(tflag),_comm(com){
  _stiffness = dynamic_cast<splitStiffnessMatrixPETSc*>(_pAl->getSplitStiffness());
};

stiffnessCondensationMKBCPETSc::~stiffnessCondensationMKBCPETSc(){
  clear();
};

void stiffnessCondensationMKBCPETSc::init(){
  if (isInitialized()) {
    clear();
  }
  isInitialized() = true;
  pbcDofManager* pbcAssembler = _pAl->getSplittedDof();
	// linea constraint matrix
	_bConMat = new linearConstraintMatrixPETSc();
	// size of constraints
	int nl = pbcAssembler->sizeOfMultiplierDof();
	// size of boundary dofs
	int nb = pbcAssembler->sizeOfBoundaryDof();
	// allocate the linear constraint
	_bConMat->allocate(nl,nb);
	// compute linear constraint by pbcAlgorithm
	_pAl->computeLinearConstraintMatrix(_bConMat);
	// constraint matrix
	Mat& C = _bConMat->getFirstMatrix();
  // first order matrix
  Mat& S = _bConMat->getFirstOrderKinematicalMatrix();
  // dependent dof and independent dof
	int ndepen = pbcAssembler->sizeOfDependentDof();
	int nindepen = pbcAssembler->sizeOfIndependentDof();
  // split to two dependent matrix and independent matrix
  Mat Cb, Cc;
  _try(MatCreate(_comm,&Cb));
  _try(MatSetSizes(Cb,PETSC_DECIDE,PETSC_DECIDE,nl,ndepen));
  _try(MatSetFromOptions(Cb));
  _try(MatCreate(_comm,&Cc));
  _try(MatSetSizes(Cc,PETSC_DECIDE,PETSC_DECIDE,nl,nindepen));
  _try(MatSetType(Cc,MATDENSE));
  _try(MatSetFromOptions(Cc));

  #if (((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)))
  _try(MatSetUp(Cb));
  _try(MatSetUp(Cc));
  #endif // (PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)
  // begin splitting, by finding the dof in pbcDofManager
  for (int i=0; i<nl; i++){
    PetscInt row = i;
    for (std::map<Dof,int>::const_iterator it = pbcAssembler->boundaryDofBegin();
                                           it != pbcAssembler->boundaryDofEnd(); it++){
      PetscInt colC = it->second;
      PetscScalar s;
      _try(MatGetValues(C,1,&row,1,&colC,&s));
			if (fabs(s) > 0.){
				PetscInt col = pbcAssembler->getDependentDofNumber(it->first);
				if (col != -1){
					_try(MatSetValues(Cb,1,&row,1,&col,&s,ADD_VALUES));
				}
				else{
					col = pbcAssembler->getIndependentDofNumber(it->first);
					_try(MatSetValues(Cc,1,&row,1,&col,&s,ADD_VALUES));
				}
			}
    }
  }
  _try(MatAssemblyBegin(Cb,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(Cb,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyBegin(Cc,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(Cc,MAT_FINAL_ASSEMBLY));

   _try(MatDuplicate(Cc,MAT_DO_NOT_COPY_VALUES,&_Cbc));
  _try(MatDuplicate(S,MAT_DO_NOT_COPY_VALUES,&_Sbc));

  _try(MatGetOrdering(Cb, "natural",  &perm,  &iperm));
  _try(MatFactorInfoInitialize(&info));
  _try(MatLUFactor(Cb,perm, iperm, &info));

  _try(MatMatSolve(Cb,Cc,_Cbc));
  _try(MatScale(_Cbc,-1.));
  _try(MatMatSolve(Cb,S,_Sbc));

  _try(MatDestroy(&Cb));
  _try(MatDestroy(&Cc));
  _try(MatAssemblyBegin(_Cbc,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(_Cbc,MAT_FINAL_ASSEMBLY));
  _try(MatTranspose(_Cbc,MAT_INITIAL_MATRIX,&_CbcT));
  _try(MatAssemblyBegin(_CbcT,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(_CbcT,MAT_FINAL_ASSEMBLY));

  _try(MatAssemblyBegin(_Sbc,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(_Sbc,MAT_FINAL_ASSEMBLY));
  _try(MatTranspose(_Sbc,MAT_INITIAL_MATRIX,&_SbcT));
  _try(MatAssemblyBegin(_SbcT,MAT_FINAL_ASSEMBLY));
  _try(MatAssemblyEnd(_SbcT,MAT_FINAL_ASSEMBLY));
};
void stiffnessCondensationMKBCPETSc::clear(){
  if (isInitialized()){
    this->isInitialized() = false;
    _try(MatDestroy(&_Cbc));
    _try(MatDestroy(&_CbcT));
    _try(MatDestroy(&_Sbc));
    _try(MatDestroy(&_SbcT));
    if (_bConMat) delete _bConMat;
    _bConMat = NULL;
  }
};
void stiffnessCondensationMKBCPETSc::stressSolve(){
  if (_stressflag){
    if (!this->isInitialized()) {
      this->init();
    };
    pbcDofManager* pbcAssembler = _pAl->getSplittedDof();
    int sizedepend = pbcAssembler->sizeOfDependentDof();
    std::string str = "A";
    linearSystem<double>* lsys = _pAssembler->getLinearSystem(str);
    nonLinearSystem<double>* linearsys = dynamic_cast<nonLinearSystem<double>*>(lsys);
    pbcSystem<double>* pbcsys = dynamic_cast<pbcSystem<double>*>(lsys);
    if (linearsys and pbcsys){
      Vec fc;
      _try(VecCreate(_comm,&fc));
      _try(VecSetSizes(fc,PETSC_DECIDE,sizedepend));
      _try(VecSetFromOptions(fc));

      for (std::map<Dof,int>::const_iterator it = pbcAssembler->dependentDofBegin();
                                             it!= pbcAssembler->dependentDofEnd(); it++){
        double val = 0.;
        int num = _pAssembler->getDofNumber(it->first);
        if (num != -1)
          linearsys->getFromRightHandSideMinus(num, val);
        else
          Msg::Error("The pbcNonLinearSystem has to used");
        PetscInt row = it->second;
        _try(VecSetValues(fc,1,&row,&val,INSERT_VALUES));
      }
      _try(VecAssemblyBegin(fc));
      _try(VecAssemblyEnd(fc));

      Vec P;
      _try(VecCreate(_comm,&P));
      _try(VecSetSizes(P,9,PETSC_DECIDE));
      _try(VecSetFromOptions(P));

      _try(MatMult(_SbcT,fc,P));
      _try(VecAssemblyBegin(P));
      _try(VecAssemblyEnd(P));

      STensor3& stress = this->getRefToMacroStress();
      double& rvevolume = this->getRVEVolume();
      PetscScalar* val;
      _try(VecGetArray(P,&val));
      for (int row = 0; row<9; row++){
        int i,j;
        Tensor23::getIntsFromIndex(row,i,j);
        stress(i,j) =val[row]/rvevolume;
      };

      _try(VecDestroy(&fc));
      _try(VecDestroy(&P));
    }
    else{
      Msg::Error("The pbc nonlinear solver have to used to compute");
    };
  };
};
void stiffnessCondensationMKBCPETSc::tangentCondensationSolve(){
  if (_tangentflag){
    if (!isInitialized()) {
      this->init();
    }
    // get all submatrices on the splitted matrix
    Mat& Kii = _stiffness->getIIMat();
    Mat& Kib = _stiffness->getIBMat();
    Mat& Kic = _stiffness->getICMat();

    Mat& Kbi = _stiffness->getBIMat();
    Mat& Kbb = _stiffness->getBBMat();

    Mat& Kbc = _stiffness->getBCMat();

    Mat& Kci = _stiffness->getCIMat();
    Mat& Kcb = _stiffness->getCBMat();
    Mat& Kcc = _stiffness->getCCMat();

    // condense to other matrix by solving Schur problem
    Mat Kbbt, Kbct, Kcbt, Kcct;
    pbcDofManager* pbcAssembler = _pAl->getSplittedDof();
    int ni = pbcAssembler->sizeOfInternalDof();
    int nb = pbcAssembler->sizeOfDependentDof();
    int nc = pbcAssembler->sizeOfIndependentDof();
    if (ni>0){
      Mat matfac;
      _try(MatDuplicate(Kii,MAT_COPY_VALUES,&matfac));
      _try(MatAssemblyBegin(matfac,MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(matfac,MAT_FINAL_ASSEMBLY));

      _try(MatGetOrdering(matfac,  "natural",  &perm,  &iperm));
      _try(MatFactorInfoInitialize(&info));
      _try(MatLUFactor(matfac, perm, iperm, &info));
      if (nb>0)
        _try(GetSchurComplementMat(matfac,Kib,Kbi,Kbb,&Kbbt));
      if (nc>0)
        _try(GetSchurComplementMat(matfac,Kic,Kci,Kcc,&Kcct));
      if (nc>0 and nb>0){
        _try(GetSchurComplementMat(matfac,Kic,Kbi,Kbc,&Kbct));
        //_try(GetSchurComplementMat(matfac,Kib,Kci,Kcb,&Kcbt));
        _try(MatTranspose(Kbct,MAT_INITIAL_MATRIX,&Kcbt));
        _try(MatAssemblyBegin(Kcbt,MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(Kcbt,MAT_FINAL_ASSEMBLY));
      }
    _try(MatDestroy(&matfac));
    }
    else{
      if (nb>0)
        _try(MatDuplicate(Kbb,MAT_COPY_VALUES,&Kbbt));
      if (nc>0 and nb>0){
        _try(MatDuplicate(Kbc,MAT_COPY_VALUES,&Kbct));
        _try(MatDuplicate(Kcb,MAT_COPY_VALUES,&Kcbt));
      }
      if (nc>0)
        _try(MatDuplicate(Kcc,MAT_COPY_VALUES,&Kcct));
    };

    Mat Kstar;
    if (nc>0){
      Mat Kccstar, Kcbstar,KcbstarT;
      _try(MatMatMult(_CbcT,Kbbt,MAT_INITIAL_MATRIX,1.,&Kcbstar));
      _try(MatAXPY(Kcbstar,1,Kcbt,DIFFERENT_NONZERO_PATTERN));
      _try(MatAssemblyBegin(Kcbstar,MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(Kcbstar,MAT_FINAL_ASSEMBLY));
      _try(MatTranspose(Kcbstar,MAT_INITIAL_MATRIX,&KcbstarT));

      Mat mattemp;
      _try(MatMatMult(_CbcT,Kbct,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&mattemp));
      _try(MatMatMult(Kcbstar,_Cbc,MAT_INITIAL_MATRIX,PETSC_DEFAULT,&Kccstar));
      _try(MatAXPY(Kccstar,1,Kcct,DIFFERENT_NONZERO_PATTERN));
      _try(MatAXPY(Kccstar,1,mattemp,DIFFERENT_NONZERO_PATTERN));
      _try(MatDestroy(&mattemp));
      _try(MatAssemblyBegin(Kccstar,MAT_FINAL_ASSEMBLY));
      _try(MatAssemblyEnd(Kccstar,MAT_FINAL_ASSEMBLY));

      // for mat Kbbstar
      _try(MatGetOrdering(Kccstar,  "natural",  &perm,  &iperm));
      _try(MatFactorInfoInitialize(&info));
      _try(MatLUFactor(Kccstar, perm, iperm, &info));

      _try(GetSchurComplementMat(Kccstar,Kcbstar,KcbstarT,Kbbt,&Kstar));
      _try(MatDestroy(&Kccstar));
      _try(MatDestroy(&Kcbstar));
      _try(MatDestroy(&KcbstarT));
    }
    else{
      Kstar = Kbbt;
    }

    Mat KstarS;
    _try(MatMatMult(Kstar,_Sbc,MAT_INITIAL_MATRIX,1.,&KstarS));

    Mat L;
    _try(MatMatMult(_SbcT,KstarS,MAT_INITIAL_MATRIX,1,&L));
    _try(MatAssemblyBegin(L,MAT_FINAL_ASSEMBLY));
    _try(MatAssemblyEnd(L,MAT_FINAL_ASSEMBLY));


    _try(MatDestroy(&Kstar));
    _try(MatDestroy(&KstarS));

    double& rvevolume = this->getRVEVolume();
    if (rvevolume >0){
      PetscInt M,N;
      _try(MatGetSize(L,&M,&N));
      //Msg::Info("M = %d, N = %d",M,N);

      STensor43& tangent = this->getRefToMacroTangent();

      for (int row = 0; row< M; ++row){
        int i,j;
        Tensor23::getIntsFromIndex(row,i,j);
        for (int col = 0; col<N; ++col){
          PetscScalar val;
          PetscInt ii = row, jj = col;
          _try(MatGetValues(L,1,&ii,1,&jj,&val));
          int k,l;
          Tensor23::getIntsFromIndex(col,k,l);
          tangent(i,j,k,l) = val/rvevolume;
        };
      };

      _try(MatDestroy(&L));
	_try(MatDestroy(&Kbbt));
	_try(MatDestroy(&Kbct));
	_try(MatDestroy(&Kcbt));
	_try(MatDestroy(&Kcct));
    }
    else{
      printf("Error in rve volume /n");
    };
  };
};


#endif //HAVE_PETSC
