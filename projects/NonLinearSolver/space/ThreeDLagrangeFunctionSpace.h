//
// C++ Interface: Function Space
//
// Description: FunctionSpace used (scalar lagragian function space in 3D)
//
//
// Author:  <G. Becker & L. Noels>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef THREEDLAGRANGEFUNCTIONSPACE_H_
#define THREEDLAGRANGEFUNCTIONSPACE_H_
#include "nlsFunctionSpace.h"
#include "MInterfaceElement.h"
// Put here as used by dG3D and msch (general function space)
class ThreeDLagrangeFunctionSpace : public nlsFunctionSpaceXYZ<double>
{
 protected :
  std::vector<int> comp;
  virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys)=0;
  const int _ifield;
 private: // avoid constant reallocation
   double _ival[256];
   double _igrad[256][3];
   double _ihess [256][3][3];
   double _ithird[256][3][3][3];
   HessType hesst;
   GradType gradt;
   ThirdDevType thirdt;
 public:

  ThreeDLagrangeFunctionSpace(int id, int ncomp,const bool hesscompute,
                              const bool thirdcompute) : nlsFunctionSpaceXYZ<double>(ncomp,hesscompute,thirdcompute), _ifield(id)
  {
    for(int i=0; i < ncomp; i++)
      comp.push_back(i);
  }
  ThreeDLagrangeFunctionSpace(int id, int ncomp, int comp1_, const bool hessc, const bool thirdc) : nlsFunctionSpaceXYZ<double>(1,hessc,thirdc),
                                                                                                    _ifield(id)
  {
    comp.push_back(comp1_);
  }
  ThreeDLagrangeFunctionSpace(int id, int ncomp, int comp1_, int comp2_,
                              const bool hessc, const bool thirdc) : nlsFunctionSpaceXYZ<double>(2,hessc,thirdc), _ifield(id)
  {
    comp.push_back(comp1_); comp.push_back(comp2_);
  }
  ThreeDLagrangeFunctionSpace(int id, int ncomp, int comp1_, int comp2_, int comp3_,
                              const bool hessc, const bool thirdc) : nlsFunctionSpaceXYZ<double>(3,hessc,thirdc), _ifield(id)
  {
    comp.push_back(comp1_); comp.push_back(comp2_); comp.push_back(comp3_);
  }
  ThreeDLagrangeFunctionSpace(int id, int ncomp, int comp1_, int comp2_, int comp3_,
                              int comp4_,const bool hessc, const bool thirdc) : nlsFunctionSpaceXYZ<double>(4,hessc,thirdc), _ifield(id)
  {
    comp.push_back(comp1_); comp.push_back(comp2_); comp.push_back(comp3_); comp.push_back(comp4_);
  }
  virtual ~ThreeDLagrangeFunctionSpace(){}

  virtual void f(MElement *ele, double u, double v, double w, std::vector<ValType> &vals)
  {
    ele->getShapeFunctions(u,v,w,_ival);
    int nbFF = ele->getNumShapeFunctions();
    int valssize = vals.size();
    vals.resize(valssize + _ncomp*nbFF);
    for(int i=0;i<nbFF;i++)
      for(int l=0;l<_ncomp;l++)
        vals[valssize + i+l*nbFF] = _ival[i];
  }

  virtual void fuvw(MElement *ele, double u, double v, double w, std::vector<ValType> &vals)
  {
    ele->getShapeFunctions(u,v,w,_ival);
    int nbFF = ele->getNumShapeFunctions();
    int valssize = vals.size();
    vals.resize(valssize + _ncomp*nbFF);
    for(int i=0;i<nbFF;i++)
      for(int l=0;l<_ncomp;l++)
        vals[valssize + i+l*nbFF] = _ival[i];
  }

  virtual void gradf(MElement *ele, double u, double v, double w,std::vector<GradType> &grads)
  {
    ele->getGradShapeFunctions(u, v, w, _igrad);
    int nbFF = ele->getNumShapeFunctions();
    double jac[3][3];
    double invjac[3][3];
    const double detJ = ele->getJacobian(u, v, w, jac);
    inv3x3(jac, invjac);
    int gradssize = grads.size();
    grads.resize(gradssize + _ncomp*nbFF);
    for(int i = 0; i < nbFF; ++i)
    {
      gradt(0)=invjac[0][0] * _igrad[i][0] + invjac[0][1] * _igrad[i][1] + invjac[0][2] * _igrad[i][2];
      gradt(1)=invjac[1][0] * _igrad[i][0] + invjac[1][1] * _igrad[i][1] + invjac[1][2] * _igrad[i][2];
      gradt(2)=invjac[2][0] * _igrad[i][0] + invjac[2][1] * _igrad[i][1] + invjac[2][2] * _igrad[i][2];
      for(int l=0;l<_ncomp;l++) // same values for all comp
        grads[gradssize + i+l*nbFF]=gradt;
    }
  }
  virtual void gradfuvw(MElement *ele, double u, double v, double w, std::vector<GradType> &grads)
  {
    ele->getGradShapeFunctions(u,v,w,_igrad);
    int nbFF = ele->getNumShapeFunctions();
    int gradssize = grads.size();
    grads.resize(gradssize + _ncomp*nbFF);
    for(int i=0;i<nbFF;i++){
      gradt(0) = _igrad[i][0];
      gradt(1) = _igrad[i][1];
      gradt(2) = _igrad[i][2];
      for(int l=0;l<_ncomp; l++)
        grads[gradssize + i + l*nbFF] = gradt;
    }
  }
  virtual void hessfuvw(MElement *ele, double u, double v, double w,std::vector<HessType> &hess)
  {
    ele->getHessShapeFunctions(u,v,w,_ihess);
    int nbFF = ele->getNumShapeFunctions();
    int hesssize = hess.size();
    hess.resize(hesssize + _ncomp*nbFF);
    for(int i=0;i<nbFF;i++){
      hesst(0,0) = _ihess[i][0][0]; hesst(0,1) = _ihess[i][0][1]; hesst(0,2) = _ihess[i][0][2];
      hesst(1,0) = _ihess[i][1][0]; hesst(1,1) = _ihess[i][1][1]; hesst(1,2) = _ihess[i][1][2];
      hesst(2,0) = _ihess[i][2][0]; hesst(2,1) = _ihess[i][2][1]; hesst(2,2) = _ihess[i][2][2];
      for(int l=0;l<_ncomp; l++){
        hess[hesssize + i+l*nbFF] = hesst;
      }
    }
  }
  virtual void hessf(MElement *ele, double u, double v, double w,std::vector<HessType> &hess)
  {
    ele->getGradShapeFunctions(u,v,w,_igrad);
    ele->getHessShapeFunctions(u,v,w,_ihess);
    int nbFF = ele->getNumShapeFunctions();
    int hesssize = hess.size();
    hess.resize(hesssize + _ncomp*nbFF);

    double jac[3][3];
    double invjac[3][3];
    const double detJ = ele->getJacobian(u, v, w, jac);
    inv3x3(jac, invjac);

    STensor33 graduvwJ(0.);
    int numver = ele->getNumVertices();
    for (int i=0; i<numver; i++){
      MVertex* v = ele->getVertex(i);
      SVector3 vec(v->x(),v->y(),v->z());
      for (int m=0; m<3; m++){
        for (int n=0; n<3; n++){
          for (int q=0; q<3; q++){
            graduvwJ(m,q,n) +=_ihess[i][m][n]*vec(q);
          };
        };
      };
    };

    for(int i=0;i<nbFF;i++){
      hesst*= 0.;
      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          for (int m=0; m<3; m++){
            for (int n=0; n<3; n++){
              hesst(k,l)+= invjac[k][m]*invjac[l][n]*_ihess[i][m][n];
              for (int q=0; q<3; q++){
                for (int p=0; p<3; p++){
                  hesst(k,l) -= invjac[k][m]*invjac[l][n]*graduvwJ(m,q,n)*invjac[q][p]*_igrad[i][p];
                };
              };
            };
          };
        };
      };
      for(int l=0;l<_ncomp; l++){
        hess[hesssize + i+l*nbFF] = hesst;
      };
    };
  };

  virtual void thirdDevfuvw(MElement *ele, double u, double v, double w,std::vector<ThirdDevType> &third){
    ele->getThirdDerivativeShapeFunctions(u,v,w,_ithird);
    int nbFF = ele->getNumShapeFunctions();
    int thirdsize = third.size();
    third.resize(thirdsize + _ncomp*nbFF);
    for(int i=0;i<nbFF;i++){
      for (int p=0; p<3; p++)
        for (int q=0; q<3; q++)
          for (int r=0; r<3; r++)
            thirdt(p,q,r) = _ithird[i][p][q][r];
      for(int l=0;l<_ncomp; l++){
        third[thirdsize + i+l*nbFF] = thirdt;
      }
    }
  }; //need to high order fem
  virtual void thirdDevf(MElement *ele, double u, double v, double w,std::vector<ThirdDevType> &third){
    ele->getGradShapeFunctions(u,v,w,_igrad);
    ele->getHessShapeFunctions(u,v,w,_ihess);
    ele->getThirdDerivativeShapeFunctions(u,v,w,_ithird);
    int nbFF = ele->getNumShapeFunctions();
    int thirdsize = third.size();
    third.resize(thirdsize + _ncomp*nbFF);

    double jac[3][3];
    double invjac[3][3];
    const double detJ = ele->getJacobian(u, v, w, jac);
    inv3x3(jac, invjac);

    STensor33 B(0.);
    int numver = ele->getNumVertices();
    for (int i=0; i<numver; i++){
      MVertex* v = ele->getVertex(i);
      SVector3 vec(v->x(),v->y(),v->z());
      for (int m=0; m<3; m++){
        for (int n=0; n<3; n++){
          for (int q=0; q<3; q++){
            B(m,q,n) +=_ihess[i][m][n]*vec(q);
          };
        };
      };
    };

    STensor43 C(0.);
    for (int i=0; i<numver; i++){
      MVertex* v = ele->getVertex(i);
      SVector3 vec(v->x(),v->y(),v->z());
      for (int m=0; m<3; m++){
        for (int n=0; n<3; n++){
          for (int q=0; q<3; q++){
            for (int r=0; r<3; r++ ){
              C(m,q,n,r) +=_ithird[i][m][n][r]*vec(q);
            }
          };
        };
      };
    };

    STensor33 A(0);
    for (int i=0; i<3; i++)
      for (int j=0; j<3; j++)
        for (int k=0; k<3; k++)
          for (int l=0; l<3; l++)
            for (int m=0; m<3; m++)
              A(i,j,k) -= B(l,m,k)*invjac[i][l]*invjac[m][j];

    for(int i=0;i<nbFF;i++){
      thirdt*= 0.;
      for(int j=0; j<3; j++)
        for (int k=0; k<3; k++)
          for (int l=0; l<3; l++)
            for (int m=0; m<3; m++){
              for (int s=0; s<3; s++)
                for (int r=0; r<3; r++){
                  double temp = invjac[l][s]*(A(j,m,s)*invjac[k][r]+ A(k,r,s)*invjac[j][m]);
                  double temp2 = invjac[j][m]*invjac[k][r]*invjac[l][s];
                  thirdt(j,k,l) += temp* _ihess[i][m][r]+
                                   temp2* _ithird[i][m][r][s];

                  for (int q=0; q<3; q++)
                    for (int p=0; p<3; p++){
                      thirdt(j,k,l) -= temp*B(m,q,r)*invjac[q][p]*_igrad[i][p]+
                                       temp2*(C(m,q,r,s)*invjac[q][p]*_igrad[i][p]+
                                              B(m,q,r)*A(q,p,s)*_igrad[i][p]+
                                              B(m,q,r)*invjac[q][p]*_ihess[i][s][p]);
                    }
                }
            }
      for(int l=0;l<_ncomp; l++){
        third[thirdsize + i+l*nbFF] = thirdt;
      };
    };
  }; //need to high order fem

  virtual int getNumKeys(MElement *ele) {if (ele->getParent()) ele = ele->getParent();return _ncomp*ele->getNumVertices();}
  virtual int getId(void) const {return _ifield;}

  virtual void getKeys(MElement *ele, std::vector<Dof> &keys){
  /*
    // Keys depends if MElement *ele is a bulk element or an interface element
    if(ele->getDim() == 2){
      MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(ele);
      this->getKeys(iele,keys);
    }
    else if ((ele->getDim() == 3) or (ele->getDim()==0)){ // dim 0 get for a MPoint CgDg
      this->getKeysOnElement(ele,keys);
    }
    else{
      Msg::Error("Keys are not generated for element of dim != 2 or 3");
    }
    */
    MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(ele);
    if (iele)
      this->getKeys(iele,keys);
    else
      this->getKeysOnElement(ele,keys);
  }

 protected :
  virtual void getKeys(MInterfaceElement *iele, std::vector<Dof> &keys){
    this->getKeysOnElement(iele->getElem(0),keys);
    if(!(iele->getElem(0) == iele->getElem(1)))
      this->getKeysOnElement(iele->getElem(1),keys);
  }
};

// same but in UVW
// used by dgshell and by dG3D for interface
class IsoparametricLagrangeFunctionSpace : public nlsFunctionSpaceUVW<double>
{
 protected :
  std::vector<int> comp;
  virtual void getKeysOnElement(MElement *ele, std::vector<Dof> &keys) {Msg::Error("You forget to define getKeysOnElement for your functional space");}
  const int _ifield;
 public:
 private: // avoid constant reallocation
   double _ival[256];
   double _igrad[256][3];
   double _ihess [256][3][3];
   HessType hesst;
   GradType gradt;
 public:

  IsoparametricLagrangeFunctionSpace(int id) : nlsFunctionSpaceUVW<double>(3,true,false), _ifield(id)
  {comp.push_back(0),comp.push_back(1),comp.push_back(2);}
  IsoparametricLagrangeFunctionSpace(int id, int comp1_) : nlsFunctionSpaceUVW<double>(1,true,false), _ifield(id)
  {
    comp.push_back(comp1_);
  }
  IsoparametricLagrangeFunctionSpace(int id, int comp1_, int comp2_) : nlsFunctionSpaceUVW<double>(2,true,false), _ifield(id)
  {
    comp.push_back(comp1_); comp.push_back(comp2_);
  }
  virtual ~IsoparametricLagrangeFunctionSpace(){}

  virtual void fuvw(MElement *ele, double u, double v, double w,std::vector<ValType> &vals)
  {
    ele->getShapeFunctions(u,v,w,_ival);
    int nbFF = ele->getNumShapeFunctions();
    int valssize = vals.size();
    vals.resize(valssize + _ncomp*nbFF);
    for(int i=0;i<nbFF;i++)
      for(int l=0;l<_ncomp;l++)
        vals[valssize + i+l*nbFF] = _ival[i];
  }
  // ==fuvw
  virtual void f(MElement *ele, double u, double v, double w, std::vector<ValType> &vals)
  {
    this->fuvw(ele,u,v,w,vals);
  }

  virtual void gradfuvw(MElement *ele, double u, double v, double w, std::vector<GradType> &grads)
  {
    ele->getGradShapeFunctions(u,v,w,_igrad);
    int nbFF = ele->getNumShapeFunctions();
    int gradssize = grads.size();
    grads.resize(gradssize + _ncomp*nbFF);
    for(int i=0;i<nbFF;i++){
      gradt(0) = _igrad[i][0];
      gradt(1) = _igrad[i][1];
      gradt(2) = _igrad[i][2];
      for(int l=0;l<_ncomp; l++)
        grads[gradssize + i + l*nbFF] = gradt;
    }
  }
  // == gradfuvw
  virtual void gradf(MElement *ele, double u, double v, double w,std::vector<GradType> &grads)
  {
    this->gradfuvw(ele,u,v,w,grads);
  }

  virtual void hessfuvw(MElement *ele, double u, double v, double w,std::vector<HessType> &hess)
  {
    ele->getHessShapeFunctions(u,v,w,_ihess);
    int nbFF = ele->getNumShapeFunctions();
    int hesssize = hess.size();
    hess.resize(hesssize + _ncomp*nbFF);
    for(int i=0;i<nbFF;i++){
      hesst(0,0) = _ihess[i][0][0]; hesst(0,1) = _ihess[i][0][1]; hesst(0,2) = _ihess[i][0][2];
      hesst(1,0) = _ihess[i][1][0]; hesst(1,1) = _ihess[i][1][1]; hesst(1,2) = _ihess[i][1][2];
      hesst(2,0) = _ihess[i][2][0]; hesst(2,1) = _ihess[i][2][1]; hesst(2,2) = _ihess[i][2][2];
      for(int l=0;l<_ncomp; l++){
        hess[hesssize + i+l*nbFF] = hesst;
      }
    }
  }
  // == hessfuvw
  virtual void hessf(MElement *ele, double u, double v, double w,std::vector<HessType> &hess)
  {
    this->hessfuvw(ele,u,v,w,hess);
  }
  virtual void thirdDevfuvw(MElement *ele, double u, double v, double w,ContainerThirdDevType &third){}; // Empty as "false"
  // == thirdDevuvw
  virtual void thirdDevf(MElement *ele, double u, double v, double w,ContainerThirdDevType &third)
  {
    this->thirdDevfuvw(ele,u,v,w,third);
  }
  virtual int getNumKeys(MElement *ele) {if (ele->getParent()) ele = ele->getParent();return _ncomp*ele->getNumVertices();}
  virtual int getId(void) const {return _ifield;}

  virtual void getKeys(MElement *ele, std::vector<Dof> &keys){
    // Keys depends if MElement *ele is a bulk element or an interface element
    if(ele->getDim() == 1){
      MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(ele);
      this->getKeys(iele,keys);
    }
    else if ((ele->getDim() == 2) or (ele->getDim()==0)){ // dim 0 get for a MPoint CgDg
      this->getKeysOnElement(ele,keys);
    }
    else{
      Msg::Error("Keys are not generated for element of dim != 1 or 2");
    }
  }

 protected :
  virtual void getKeys(MInterfaceElement *iele, std::vector<Dof> &keys){
    this->getKeysOnElement(iele->getElem(0),keys);
    if(!(iele->getElem(0) == iele->getElem(1)))
      this->getKeysOnElement(iele->getElem(1),keys);
  }
};


#endif // THREEDLAGRANGEFUNCTIONSPACE_H_
