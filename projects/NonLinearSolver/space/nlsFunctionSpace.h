//
// C++ Interface: space functions
//
// Description: Define a common functional space based on the new genTerm (see cadxfem)
//              Calls one for all the Gauss Points. Allow caching and avoid to recompute
//              the shape function each time which seems very costly
//
//
// Author:  <Gauthier BECKER>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef NLSFUNCTIONSPACE_H_
#define NLSFUNCTIONSPACE_H_
#include "functionSpace.h"
#include "GaussIntegration.h"

template<class T> struct nlsContainerTraits
{
  typedef T ContainerValType;
  typedef T ContainerGradType;
  typedef T ContainerHessType;
  typedef T ContainerThirdDevType;
};

template<> struct nlsContainerTraits<double>
{
  typedef std::vector<TensorialTraits<double>::ValType> ContainerValType;
  typedef std::vector<TensorialTraits<double>::GradType> ContainerGradType;
  typedef std::vector<TensorialTraits<double>::HessType> ContainerHessType;
  typedef std::vector<TensorialTraits<double>::ThirdDevType> ContainerThirdDevType;
};

// Structure to store the value of a Gauss point
template<class T> class nlsFunctionSpaceUVW;
template<class T> struct GaussPointSpaceValues
{
 public:
  typedef typename nlsContainerTraits<T>::ContainerValType ContainerValType;
  typedef typename nlsContainerTraits<T>::ContainerGradType ContainerGradType;
  typedef typename nlsContainerTraits<T>::ContainerHessType ContainerHessType;
  typedef typename nlsContainerTraits<T>::ContainerThirdDevType ContainerThirdDevType;
  ContainerValType _vvals;
  ContainerGradType _vgrads;
  ContainerHessType _vhess;
  ContainerThirdDevType _vthird;
  double _detJ; // value of detJacobian only for XYZ space as in UVW it is the same container for all elements !!
  GaussPointSpaceValues(nlsFunctionSpaceUVW<T> *sp,MElement *ele,const int ncomp,const double u, const double v, const double w,
                        const bool hessian, const bool thirdDev) : _detJ(0.)
  {
    // compute the value
    sp->fuvw(ele,u,v,w,_vvals);
    sp->gradfuvw(ele,u,v,w,_vgrads);
    if(hessian)
      sp->hessfuvw(ele,u,v,w,_vhess);
    if(thirdDev)
      sp->thirdDevfuvw(ele,u,v,w,_vthird);
  }
  // To create tempory storage space for XYZ space
  GaussPointSpaceValues(const bool hessian, const bool thirdDev,const int size_=0) : _vvals(size_), _vgrads(size_), _detJ(0.)
  {
    if(hessian) _vhess.resize(size_);
    if(thirdDev) _vthird.resize(size_);
  }
  GaussPointSpaceValues(const GaussPointSpaceValues &source) : _detJ(source._detJ)
  {
    _vvals.assign(source._vals.begin(),source._vals.end());
    _vgrads.assign(source._vgrads.begin(),source._vgrads.end());
    _vhess.assign(source._vhess.begin(),source._vhess.end());
    _vthird.assign(source._vthird.begin(),source._vthird.end());
  }
  virtual ~GaussPointSpaceValues()
  {
    _vvals.clear();
    _vgrads.clear();
    _vhess.clear();
    _vthird.clear();
  }
};

template<class T> class nlsFunctionSpaceUVW : public FunctionSpace<T>
{
 public:
  typedef typename TensorialTraits<T>::ValType ValType;
  typedef typename TensorialTraits<T>::GradType GradType;
  typedef typename TensorialTraits<T>::HessType HessType;
  typedef typename TensorialTraits<T>::ThirdDevType ThirdDevType;
  typedef typename nlsContainerTraits<T>::ContainerValType ContainerValType;
  typedef typename nlsContainerTraits<T>::ContainerGradType ContainerGradType;
  typedef typename nlsContainerTraits<T>::ContainerHessType ContainerHessType;
  typedef typename nlsContainerTraits<T>::ContainerThirdDevType ContainerThirdDevType;
 protected:
  // number of comp;
  const int _ncomp;
  // High orders are not always needed and so computed
  const bool _hessianComputation;
  const bool _thirdDevComputation;
  // map to store the data
  std::map<IntPt*,std::vector<GaussPointSpaceValues<T>*> > _mapGaussValues;

  // function to allocate the map
  typename std::map<IntPt*,std::vector<GaussPointSpaceValues<T>*> >::iterator allocatePoints(MElement *ele,int npts, IntPt *GP)
  {
    std::vector<GaussPointSpaceValues<T>*> allval;
    for(int i=0;i<npts;i++)
    {
      GaussPointSpaceValues<T>* gpsv = new GaussPointSpaceValues<T>(this,ele,_ncomp,GP[i].pt[0],GP[i].pt[1],GP[i].pt[2],_hessianComputation,_thirdDevComputation);
      allval.push_back(gpsv);
    }
    std::pair<typename std::map<IntPt*,std::vector<GaussPointSpaceValues<T>*> >::iterator,bool> pib = _mapGaussValues.insert(std::pair<IntPt*,std::vector<GaussPointSpaceValues<T>*> >(GP,allval));
    return pib.first;
  }
 public:
  virtual int getNumKeys(MElement *ele) = 0;
  virtual void getKeys(MElement *ele, std::vector<Dof> &keys) = 0;
  nlsFunctionSpaceUVW(const int nc, const bool hessian=false,const bool thirdDev=false) : _ncomp(nc), FunctionSpace<T>(), _hessianComputation(hessian),
                                                                                       _thirdDevComputation(thirdDev){}
  nlsFunctionSpaceUVW(const nlsFunctionSpaceUVW &source) : FunctionSpace<T>(source), _hessianComputation(source._hessianComputation),
                                                     _thirdDevComputation(source._thirdDevComputation), _ncomp(source._ncomp),
                                                     _mapGaussValues(){}
  virtual ~nlsFunctionSpaceUVW()
  {
    for(typename std::map<IntPt*,std::vector<GaussPointSpaceValues<T>*> >::iterator itG=_mapGaussValues.begin(); itG!=_mapGaussValues.end();++itG)
    {
      std::vector<GaussPointSpaceValues<T>*> &vgvals = itG->second;
      for(int j=0;j<vgvals.size();j++)
        delete vgvals[j];
    }
    _mapGaussValues.clear();
  }
  // traditionel functions
  virtual void fuvw(MElement *ele, double u, double v, double w, ContainerValType &vals)=0;
  virtual void f(MElement *ele, double u, double v, double w, ContainerValType &vals) = 0;
  virtual void gradfuvw(MElement *ele, double u, double v, double w, ContainerGradType &grads)=0;
  virtual void gradf(MElement *ele, double u, double v, double w, ContainerGradType &grads) = 0;
  virtual void hessfuvw(MElement *ele, double u, double v, double w, ContainerHessType &hess)=0;
  virtual void hessf(MElement *ele, double u, double v, double w, ContainerHessType &hess) =0; // define it empty if _hessianComputation == false
  virtual void thirdDevfuvw(MElement *ele, double u, double v, double w,ContainerThirdDevType &third)=0;
  virtual void thirdDevf(MElement *ele, double u, double v, double w,ContainerThirdDevType &third)=0; // define it empty if _thirdDevComputation == false


  // Functions needed to use a map which store the values for the different Gauss Points
  // These ones can be const if fuvw,gradfuvw, hessfuvw and thirdfuvw are const functions
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector< ContainerValType > &vvals)
  {
    typename std::map<IntPt*,std::vector<GaussPointSpaceValues<T>*> >::iterator itG = _mapGaussValues.find(GP);
    // fill the value if not allocated
    if(itG == _mapGaussValues.end())
    {
       itG = this->allocatePoints(ele,npts,GP);
    }
    std::vector<GaussPointSpaceValues<T>*>& allc = (itG->second);
    for(int i=0;i<npts;i++)
      vvals.push_back(allc[i]->_vvals); // space in uvw --> vvals == _vvals;
  }
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector< GaussPointSpaceValues<T>*> &vall)
  {
    typename std::map<IntPt*,std::vector<GaussPointSpaceValues<T>*> >::iterator itG = _mapGaussValues.find(GP);
    // fill the value if not allocated
    if(itG == _mapGaussValues.end())
    {
      itG = this->allocatePoints(ele,npts,GP);
    }
    std::vector<GaussPointSpaceValues<T>*>& allc = itG->second;
    vall.assign(allc.begin(),allc.end()); // space in uvw --> vall == allc;
  }
  virtual void getgradf(MElement *ele, int npts, IntPt *GP, std::vector< ContainerGradType > &vgrads)
  {
    typename std::map<IntPt*,std::vector<GaussPointSpaceValues<T>*> >::iterator itG = _mapGaussValues.find(GP);
    // fill the value if not allocated
    if(itG == _mapGaussValues.end())
    {
      itG = this->allocatePoints(ele,npts,GP);
    }
    std::vector<GaussPointSpaceValues<T>*>& allc = itG->second;
    for(int i=0;i<npts;i++)
      vgrads.push_back(allc[i]->_vgrads); // space in uvw --> vgrads == _vgrads;
  }
  // Needed to compute the Jacobian quickly !
  virtual void getgradfuvw(MElement *ele, int npts, IntPt *GP, std::vector< ContainerGradType > &vgrads)
  {
    this->getgradf(ele,npts,GP,vgrads);
  }
  virtual void gethessf(MElement *ele, int npts, IntPt *GP, std::vector< ContainerHessType > &vhesss)
  {
    if(_hessianComputation)
    {
      typename std::map<IntPt*,std::vector<GaussPointSpaceValues<T>*> >::iterator itG = _mapGaussValues.find(GP);
      if(itG == _mapGaussValues.end())
      {
        itG = this->allocatePoints(ele,npts,GP);
     }
    std::vector<GaussPointSpaceValues<T>*>& allc = itG->second;
    for(int i=0;i<npts;i++)
      vhesss.push_back(allc[i]->_vhess); // space in uvw --> vhesss == _vhess;
    }
    else
    {
      Msg::Fatal("You compute the hessian but give false for _hessianComputation to your function space");
    }
  }
  virtual void getthirdDevf(MElement *ele, int npts, IntPt *GP,std::vector<ContainerThirdDevType> &third)
  {
    if(_thirdDevComputation)
    {
      typename std::map<IntPt*,std::vector<GaussPointSpaceValues<T>*> >::iterator itG = _mapGaussValues.find(GP);
      if(itG == _mapGaussValues.end())
      {
        itG = this->allocatePoints(ele,npts,GP);
     }
     std::vector<GaussPointSpaceValues<T>*>& allc = itG->second;
     for(int i=0;i<npts;i++)
       third.push_back(allc[i]->_vthird); // space in uvw --> third == _vthird;
    }
    else
    {
      Msg::Fatal("You compute the third derivative but give false for _thirdDevComputation to your function space");
    }
  }
};

// add a lawer to have the values in XYZ
template<class T> class nlsFunctionSpaceXYZ : public nlsFunctionSpaceUVW<T>
{
 public:
  typedef typename TensorialTraits<T>::ValType ValType;
  typedef typename TensorialTraits<T>::GradType GradType;
  typedef typename TensorialTraits<T>::HessType HessType;
  typedef typename TensorialTraits<T>::ThirdDevType ThirdDevType;
  typedef typename nlsContainerTraits<T>::ContainerValType ContainerValType;
  typedef typename nlsContainerTraits<T>::ContainerGradType ContainerGradType;
  typedef typename nlsContainerTraits<T>::ContainerHessType ContainerHessType;
  typedef typename nlsContainerTraits<T>::ContainerThirdDevType ContainerThirdDevType;
 protected: // extra data
  mutable GradType gradt;
  mutable HessType hesst;
  mutable ThirdDevType thirdt;
  std::map<IntPt*,std::vector<GaussPointSpaceValues<T>*> > _mapgt; // to avoid constant allocation FIX double map search (one uvw one xyz) HOW ??
  // allocation of the map
  typename std::map<IntPt*,std::vector<GaussPointSpaceValues<T>*> >::iterator getStorageSpace(MElement *ele,const int npts,IntPt* GP)
  {
    typename std::map<IntPt*,std::vector<GaussPointSpaceValues<T>*> >::iterator itG = _mapgt.find(GP);
    if(itG == _mapgt.end())
    {
      std::vector<GaussPointSpaceValues<T>*> allval;
      for(int i=0;i<npts;i++)
      {
        GaussPointSpaceValues<T>* gpsv = new GaussPointSpaceValues<T>(this->_hessianComputation,this->_thirdDevComputation,this->_ncomp*ele->getNumShapeFunctions());
        allval.push_back(gpsv);
      }
      std::pair<typename std::map<IntPt*,std::vector<GaussPointSpaceValues<T>*> >::iterator,bool> pib = _mapgt.insert(std::pair<IntPt*,std::vector<GaussPointSpaceValues<T>*> >(GP,allval));
      itG = pib.first;
    }
    return itG;
  }
 public:
  virtual int getNumKeys(MElement *ele) = 0;
  virtual void getKeys(MElement *ele, std::vector<Dof> &keys) = 0;
  nlsFunctionSpaceXYZ(const int nc,const bool hessian,const bool thirdDev) : nlsFunctionSpaceUVW<T>(nc,hessian,thirdDev),
                                                                             gradt(0.), hesst(0.), thirdt(0.){}
  nlsFunctionSpaceXYZ(const nlsFunctionSpaceXYZ &source) : nlsFunctionSpaceUVW<T>(source){}
  virtual ~nlsFunctionSpaceXYZ()
  {
    for(typename std::map<IntPt*,std::vector<GaussPointSpaceValues<T>*> >::iterator itG=_mapgt.begin(); itG!=_mapgt.end();++itG)
    {
      std::vector<GaussPointSpaceValues<T>*> &vgvals = itG->second;
      for(int j=0;j<vgvals.size();j++)
        delete vgvals[j];
    }
    _mapgt.clear();
  }
  // traditionel functions
  virtual void fuvw(MElement *ele, double u, double v, double w, ContainerValType &vals)=0;
  virtual void f(MElement *ele, double u, double v, double w, ContainerValType &vals) = 0;
  virtual void gradfuvw(MElement *ele, double u, double v, double w, ContainerGradType &grads)=0;
  virtual void gradf(MElement *ele, double u, double v, double w, ContainerGradType &grads) = 0;
  virtual void hessfuvw(MElement *ele, double u, double v, double w, ContainerHessType &hess)=0;
  virtual void hessf(MElement *ele, double u, double v, double w, ContainerHessType &hess) =0; // define it empty if _hessianComputation == false
  virtual void thirdDevfuvw(MElement *ele, double u, double v, double w,ContainerThirdDevType &third)=0;
  virtual void thirdDevf(MElement *ele, double u, double v, double w,ContainerThirdDevType &third)=0; // define it empty if _thirdDevComputation == false

  // Redifine the functions to return the values in XYZ
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector< ContainerValType > &vvals)
  {
    typename std::map<IntPt*,std::vector<GaussPointSpaceValues<T>*> >::iterator itG = this->_mapGaussValues.find(GP);
    // fill the value if not allocated
    if(itG == this->_mapGaussValues.end())
    {
       itG = this->allocatePoints(ele,npts,GP);
    }
    // get the values in uvw
    std::vector<GaussPointSpaceValues<T>*>& allc = (itG->second);
    for(int i=0;i<npts;i++)
      vvals.push_back(allc[i]->_vvals); // value in uvw == value in xyz
  }
  //  no resize allowed after initialization --> grad.resize() is not allowed idem val,hess and third !!
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector< GaussPointSpaceValues<T>*> &vall)
  {
    typename std::map<IntPt*,std::vector<GaussPointSpaceValues<T>*> >::iterator itG = this->_mapGaussValues.find(GP);
    // fill the value if not allocated
    if(itG == this->_mapGaussValues.end())
    {
      itG = this->allocatePoints(ele,npts,GP);
    }
    // Get the values in UVW
    std::vector<GaussPointSpaceValues<T>*>& allcuvw = itG->second;
    // Storage space
    std::vector<GaussPointSpaceValues<T>*>& allc = (this->getStorageSpace(ele,npts,GP))->second;

    // Compute the values in XYZ for all Gauss Point
    int nbFF = ele->getNumShapeFunctions();
    int numver = ele->getNumVertices();
    double jac[3][3];
    double invjac[3][3];
    for(int jj=0; jj<npts;jj++)
    {
      // uvw value
      ContainerValType& ival = allcuvw[jj]->_vvals;
      ContainerGradType &igrad = allcuvw[jj]->_vgrads;
      ContainerHessType &ihess = allcuvw[jj]-> _vhess;
      ContainerThirdDevType &ithird = allcuvw[jj]->_vthird;
      // storage space for this Gauss point
      ContainerValType& val = allc[jj]->_vvals;
      ContainerGradType &grad = allc[jj]->_vgrads;
      ContainerHessType &hess = allc[jj]-> _vhess;
      ContainerThirdDevType &third = allc[jj]->_vthird;

      // common
      const double detJ = ele->getJacobian(igrad, jac);
      allc[jj]->_detJ = detJ;
      inv3x3(jac, invjac);

      // val
      val.assign(ival.begin(),ival.end()); // same

      // grad
      for(int i = 0; i < nbFF; ++i)
      {
        gradt(0)=invjac[0][0] * igrad[i][0] + invjac[0][1] * igrad[i][1] + invjac[0][2] * igrad[i][2];
        gradt(1)=invjac[1][0] * igrad[i][0] + invjac[1][1] * igrad[i][1] + invjac[1][2] * igrad[i][2];
        gradt(2)=invjac[2][0] * igrad[i][0] + invjac[2][1] * igrad[i][1] + invjac[2][2] * igrad[i][2];
        for(int l=0;l<this->_ncomp;l++) // same values for all comps
          grad[i+l*nbFF]=gradt;
      }

      // hessian
      STensor33 graduvwJ(0.); // use for hessian and thirdDev
      if(this->_hessianComputation or this->_thirdDevComputation) // hessian is computed in case of ThirdDev
      {
        for (int i=0; i<numver; i++){
          MVertex* v = ele->getVertex(i);
          SVector3 vec(v->x(),v->y(),v->z());
          for (int m=0; m<3; m++){
            for (int n=0; n<3; n++){
              for (int q=0; q<3; q++){
                graduvwJ(m,q,n) +=ihess[i](m,n)*vec(q);
              }
            }
          }
        }

        for(int i=0;i<nbFF;i++){
          hesst*= 0.;
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              for (int m=0; m<3; m++){
                for (int n=0; n<3; n++){
                  hesst(k,l)+= invjac[k][m]*invjac[l][n]*ihess[i](m,n);
                  for (int q=0; q<3; q++){
                    for (int p=0; p<3; p++){
                      hesst(k,l) -= invjac[k][m]*invjac[l][n]*graduvwJ(m,q,n)*invjac[q][p]*igrad[i][p];
                    }
                  }
                }
              }
            }
          }
          for(int l=0;l<this->_ncomp; l++)
            hess[i+l*nbFF] = hesst;
        }
      }

      // thirdDEv
      if(this->_thirdDevComputation)
      {
        STensor43 C(0.);
        for (int i=0; i<numver; i++){
          MVertex* v = ele->getVertex(i);
          SVector3 vec(v->x(),v->y(),v->z());
          for (int m=0; m<3; m++){
            for (int n=0; n<3; n++){
              for (int q=0; q<3; q++){
                for (int r=0; r<3; r++ ){
                  C(m,q,n,r) +=ithird[i](m,n,r)*vec(q);
                }
              }
            }
          }
        }

        STensor33 A(0);
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++)
                for (int m=0; m<3; m++)
                  A(i,j,k) -= graduvwJ(l,m,k)*invjac[i][l]*invjac[m][j];

        for(int i=0;i<nbFF;i++){
          thirdt*= 0.;
          for(int j=0; j<3; j++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++)
                for (int m=0; m<3; m++){
                  for (int s=0; s<3; s++)
                    for (int r=0; r<3; r++){
                      double temp = invjac[l][s]*(A(j,m,s)*invjac[k][r]+ A(k,r,s)*invjac[j][m]);
                      double temp2 = invjac[j][m]*invjac[k][r]*invjac[l][s];
                      thirdt(j,k,l) += temp* ihess[i](m,r)+
                                       temp2* ithird[i](m,r,s);

                      for (int q=0; q<3; q++)
                        for (int p=0; p<3; p++){
                          thirdt(j,k,l) -= temp*graduvwJ(m,q,r)*invjac[q][p]*igrad[i][p]+
                                           temp2*(C(m,q,r,s)*invjac[q][p]*igrad[i][p]+
                                                  graduvwJ(m,q,r)*A(q,p,s)*igrad[i][p]+
                                                  graduvwJ(m,q,r)*invjac[q][p]*ihess[i](s,p));
                    }
                }
            }
          for(int l=0;l<this->_ncomp; l++){
            third[i+l*nbFF] = thirdt;
          }
        }
      }
    }
    vall.assign(allc.begin(),allc.end()); // vall have pointer on storage space
  }
  // Needed to compute the Jacobian quickly without computing third
  virtual void getgradfuvw(MElement *ele, int npts, IntPt *GP, std::vector< ContainerGradType > &vgrads)
  {
    nlsFunctionSpaceUVW<T>::getgradf(ele,npts,GP,vgrads);
  }
  virtual void getgradf(MElement *ele, int npts, IntPt *GP, std::vector< ContainerGradType > &vgrads)
  {
    typename std::map<IntPt*,std::vector<GaussPointSpaceValues<T>*> >::iterator itG = this->_mapGaussValues.find(GP);
    // fill the value if not allocated
    if(itG == this->_mapGaussValues.end())
    {
      itG = this->allocatePoints(ele,npts,GP);
    }
    // get the values in uvw for all Gauss points
    std::vector<GaussPointSpaceValues<T>*>& allc = itG->second;

    // compute the values in XYZ
    int nbFF = ele->getNumShapeFunctions();
    double jac[3][3];
    double invjac[3][3];
    vgrads.resize(npts);
    for(int j=0;j<npts;j++)
    {
      ContainerGradType &grads = vgrads[j];
      ContainerGradType &gradsuvw = allc[j]->_vgrads;
      const double detJ = ele->getJacobian(gradsuvw, jac);
      inv3x3(jac, invjac);
      int gradssize = grads.size();
      grads.resize(gradssize + this->_ncomp*nbFF);
      for(int i = 0; i < nbFF; ++i)
      {
        gradt(0)=invjac[0][0] * gradsuvw[i][0] + invjac[0][1] * gradsuvw[i][1] + invjac[0][2] * gradsuvw[i][2];
        gradt(1)=invjac[1][0] * gradsuvw[i][0] + invjac[1][1] * gradsuvw[i][1] + invjac[1][2] * gradsuvw[i][2];
        gradt(2)=invjac[2][0] * gradsuvw[i][0] + invjac[2][1] * gradsuvw[i][1] + invjac[2][2] * gradsuvw[i][2];
        for(int l=0;l<this->_ncomp;l++) // same values for all comps
          grads[gradssize + i+l*nbFF]=gradt;
      }
    }
  }
  virtual void gethessf(MElement *ele, int npts, IntPt *GP, std::vector< ContainerHessType > &vhesss)
  {
    if(this->_hessianComputation)
    {
      typename std::map<IntPt*,std::vector<GaussPointSpaceValues<T>*> >::iterator itG = this->_mapGaussValues.find(GP);
      if(itG == this->_mapGaussValues.end())
      {
        itG = this->allocatePoints(ele,npts,GP);
      }
      // get the values in uvw for all Gauss points
      std::vector<GaussPointSpaceValues<T>*>& allc = itG->second;

      // Compute the values in XYZ
      int nbFF = ele->getNumShapeFunctions();
      int numver = ele->getNumVertices();
      double jac[3][3];
      double invjac[3][3];
      vhesss.resize(npts);
      for(int jj=0;jj<npts;jj++)
      {
        //ContainerValType  &ival  = allc[jj]->_vvals;
        ContainerGradType &igrad = allc[jj]->_vgrads;
        ContainerHessType &ihess = allc[jj]->_vhess;
        ContainerHessType &hess = vhesss[jj];
        int hesssize = hess.size();
        hess.resize(hesssize + this->_ncomp*nbFF);

        const double detJ = ele->getJacobian(igrad,jac);
        inv3x3(jac, invjac);

        STensor33 graduvwJ(0.);
        for (int i=0; i<numver; i++){
          MVertex* v = ele->getVertex(i);
          SVector3 vec(v->x(),v->y(),v->z());
          for (int m=0; m<3; m++){
            for (int n=0; n<3; n++){
              for (int q=0; q<3; q++){
                graduvwJ(m,q,n) +=ihess[i](m,n)*vec(q);
              }
            }
          }
        }

        for(int i=0;i<nbFF;i++){
          hesst*= 0.;
          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              for (int m=0; m<3; m++){
                for (int n=0; n<3; n++){
                  hesst(k,l)+= invjac[k][m]*invjac[l][n]*ihess[i](m,n);
                  for (int q=0; q<3; q++){
                    for (int p=0; p<3; p++){
                      hesst(k,l) -= invjac[k][m]*invjac[l][n]*graduvwJ(m,q,n)*invjac[q][p]*igrad[i][p];
                    }
                  }
                }
              }
            }
          }
          for(int l=0;l<this->_ncomp; l++)
            hess[hesssize + i+l*nbFF] = hesst;
        }
      }
    }
    else
    {
      Msg::Fatal("You compute the hessian but give false for _hessianComputation to your function space");
    }
  }
  virtual void getthirdDevf(MElement *ele, int npts, IntPt *GP,std::vector<ContainerThirdDevType> &vthird)
  {
    if(this->_thirdDevComputation)
    {
      typename std::map<IntPt*,std::vector<GaussPointSpaceValues<T>*> >::iterator itG = this->_mapGaussValues.find(GP);
      if(itG == this->_mapGaussValues.end())
      {
        itG = this->allocatePoints(ele,npts,GP);
      }
      // Get the values in UVW for all Gauss Points
      std::vector<GaussPointSpaceValues<T>*>& allc = itG->second;

      // Compute the value in xyz for all Gauss points
      int nbFF = ele->getNumShapeFunctions();
      int numver = ele->getNumVertices();
      double jac[3][3];
      double invjac[3][3];
      vthird.resize(npts);
      for(int jj=0;jj<npts;jj++)
      {
        //ContainerValType  &ival  = allc[jj]->_vvals;
        ContainerGradType &igrad = allc[jj]->_vgrads;
        ContainerHessType &ihess = allc[jj]->_vhess;
        ContainerThirdDevType &ithird = allc[jj]->_vthird;
        ContainerThirdDevType &third = vthird[jj];

        int thirdsize = third.size();
        third.resize(thirdsize + this->_ncomp*nbFF);


        const double detJ = ele->getJacobian(igrad, jac);
        inv3x3(jac, invjac);

        STensor33 B(0.);
        for (int i=0; i<numver; i++){
          MVertex* v = ele->getVertex(i);
          SVector3 vec(v->x(),v->y(),v->z());
          for (int m=0; m<3; m++){
            for (int n=0; n<3; n++){
              for (int q=0; q<3; q++){
                B(m,q,n) +=ihess[i](m,n)*vec(q);
              }
            }
          }
        }

        STensor43 C(0.);
        for (int i=0; i<numver; i++){
          MVertex* v = ele->getVertex(i);
          SVector3 vec(v->x(),v->y(),v->z());
          for (int m=0; m<3; m++){
            for (int n=0; n<3; n++){
              for (int q=0; q<3; q++){
                for (int r=0; r<3; r++ ){
                  C(m,q,n,r) +=ithird[i](m,n,r)*vec(q);
                }
              }
            }
          }
        }

        STensor33 A(0);
        for (int i=0; i<3; i++)
          for (int j=0; j<3; j++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++)
                for (int m=0; m<3; m++)
                  A(i,j,k) -= B(l,m,k)*invjac[i][l]*invjac[m][j];

        for(int i=0;i<nbFF;i++){
          thirdt*= 0.;
          for(int j=0; j<3; j++)
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++)
                for (int m=0; m<3; m++){
                  for (int s=0; s<3; s++)
                    for (int r=0; r<3; r++){
                      double temp = invjac[l][s]*(A(j,m,s)*invjac[k][r]+ A(k,r,s)*invjac[j][m]);
                      double temp2 = invjac[j][m]*invjac[k][r]*invjac[l][s];
                      thirdt(j,k,l) += temp* ihess[i](m,r)+
                                       temp2* ithird[i](m,r,s);

                      for (int q=0; q<3; q++)
                        for (int p=0; p<3; p++){
                          thirdt(j,k,l) -= temp*B(m,q,r)*invjac[q][p]*igrad[i][p]+
                                           temp2*(C(m,q,r,s)*invjac[q][p]*igrad[i][p]+
                                                  B(m,q,r)*A(q,p,s)*igrad[i][p]+
                                                  B(m,q,r)*invjac[q][p]*ihess[i](s,p));
                    }
                }
            }
          for(int l=0;l<this->_ncomp; l++){
            third[thirdsize + i+l*nbFF] = thirdt;
          }
        }
      }
    }
    else
    {
      Msg::Fatal("You compute the third derivative but give false for _thirdDevComputation to your function space");
    }
  }

};

#endif // NLSFUNCTIONSPACE_H_
