//
// C++ Interface: terms
//
// Description: Class to bluid an interface face (used at initialization)
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef _IFACE_H_
#define _IFACE_H_
#include "IElement.h"
#include "MVertex.h"
#include "MElement.h"
#include "InterfaceKeys.h"
// Class used to build 3D interface element
class IFace : public IElement{
  protected :
    interfaceKeys3D _key;
  public :
    IFace(std::vector<MVertex*> &v,MElement *e, partDomain *dom);
    ~IFace(){
    };
    virtual const interfaceKeys& getkey() const{return _key;}
    virtual IElement::IEtype getType()const{return IElement::Face;}
    virtual bool operator<(const IFace &_other) const{if (&_key < &(_other._key)) return true; else return false; }
};
#endif // _IFACE_H_
