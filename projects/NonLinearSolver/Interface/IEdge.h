//
// C++ Interface: terms
//
// Description: Class to bluid an interface line (used at initialization)
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef _IEDEGE_H_
#define _IEDEGE_H_
#include "IElement.h"
#include "MVertex.h"
#include "MElement.h"
// Class used to build 2D interface element
class IEdge : public IElement{
 protected:
  interfaceKeys2D _key;
  public :
    IEdge(std::vector<MVertex*> &v,MElement *e, partDomain *dom) : IElement(v,e,dom), _key(v[0]->getNum(),v[1]->getNum()){};
    ~IEdge(){};
    virtual const interfaceKeys& getkey() const{return _key;}
 /*   unsigned long int getkey() const{
      int i1,i2,i3;
      i1 = vertex[0]->getNum();
      i2 = vertex[1]->getNum();
      i1>i2 ? i3=i1*100000+i2 : i3=i2*100000+i1; // change this
      return i3;
    }
 */
    virtual IElement::IEtype getType()const{return IElement::Edge;}
    // To build a set of IEdge (needed for creation on interface in MPI case)
    virtual bool operator<(const IEdge &_other) const{if (&_key < &(_other._key)) return true; else return false; }
};
#endif // _IEDEGE_H_
