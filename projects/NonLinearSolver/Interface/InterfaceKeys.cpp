//
// Description: Keys for interface creation
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "InterfaceKeys.h"
#include <algorithm>
#include "GmshMessage.h"
interfaceKeys2D::interfaceKeys2D(const int i1,const int i2) : interfaceKeys()
{
  if(i1 > i2)
  {
    _i1 = i1;
    _i2 = i2;
  }
  else
  {
    _i1 = i2;
    _i2 = i1;
  }
}

bool interfaceKeys2D::operator==(const interfaceKeys &key) const
{
  const interfaceKeys2D *okey = static_cast<const interfaceKeys2D*>(&key);
  if(_i1 == okey->_i1 and _i2 == okey->_i2) return true;
  else return false;
}

interfaceKeys3D::interfaceKeys3D() : interfaceKeys(), _i1(0), _i2(0), _i3(0)

{
}
void interfaceKeys3D::setValues(const int j1, const int j2, const int j3)
{
  int i,i1,i2,i3; // tempory
  i1 = j1;
  i2 = j2;
  i3 = j3;
  // sort smaller --> higher
  if (i1 > i2)
    i=i1, i1=i2, i2=i;
  if (i2 > i3)
    i=i2, i2=i3, i3=i;
  if (i1 > i2)
    i=i1, i1=i2, i2=i;
  _i1 = i3;
  _i2 = i2;
  _i3 = i1;
}

void interfaceKeys3D::setValues(const int j1, const int j2, const int j3, const int j4)
{
  // remove the highest number (the key is created with 3 int only !!)
  int i1 = j1;
  int i2 = j2;
  int i3 = j3;
  int i4 = j4;
  int i = std::max(i1,std::max(i2,i3));
  int imax = std::max(i,i4);
  if(imax == i1) i1 = i4;
  else if(imax == i2) i2 = i4;
  else if(imax == i3) i3 = i4;
  // sort smaller --> higher (the 3 remaining number)
  if (i1 > i2)
    i=i1, i1=i2, i2=i;
  if (i2 > i3)
    i=i2, i2=i3, i3=i;
  if (i1 > i2)
    i=i1, i1=i2, i2=i;
  _i1 = i3;
  _i2 = i2;
  _i3 = i1;
}

bool interfaceKeys3D::operator==(const interfaceKeys &key) const
{
  const interfaceKeys3D* okey = static_cast<const interfaceKeys3D*>(&key);
  if( _i1 == okey->_i1 and _i2 == okey->_i2 and _i3 == okey->_i3) return true;
  else return false;
}
