//
// C++ Interface: terms
//
// Description: Class of interface element used for DG
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
// Has to be merge with interface element defined in dg project HOW ??
#include "MInterfaceElement.h"
#include "quadratureRules.h"
#include "MLine.h"
double MInterfaceElement::characSize(MElement *e, const int numedge)
{

  // OLD WAY Area/Perimeter
  // Compute the area of the element
  GaussQuadrature Integ_Bulk(GaussQuadrature::Grad); // which rule used for now not the same as the call function ??
  IntPt *GP;
  double perimeter = 0., Area = 0.;
  double jac[3][3];
  int npts=Integ_Bulk.getIntPoints(e,&GP);
  // Area
  for( int i = 0; i < npts; i++){
    // Coordonate of Gauss' point i
    const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
    const double weight = GP[i].weight; const double detJ = e->getJacobian(u, v, w, jac); // Or compute jacobian with crossprod(phi0[0],phi0[1]) ??
    Area += weight * detJ;
  }
  // perimeter
  int nside = e->getNumEdges();
  GaussQuadrature Integ_Bound(GaussQuadrature::ValVal);
  std::vector<MVertex*> vver;
  for(int i=0;i<nside;i++){
    IntPt *GPb;
    e->getEdgeVertices(i,vver);
    MLineN mlin = MLineN(vver);
    int npts = Integ_Bound.getIntPoints(&mlin,&GPb);
    for(int j=0;j<npts; j++){
      const double u = GPb[j].pt[0]; const double v = GPb[j].pt[1]; const double w = GPb[j].pt[2];
      const double weight = GPb[j].weight; const double detJ = mlin.getJacobian(u, v, w, jac);
      perimeter +=weight*detJ;
    }
    vver.clear();
  }

  return Area/perimeter;
/*

  // New way (MetaSheel) BEAWARE TRIANGLE
  SVector3 nodepos; // 3 nodes as to considered depend on edge
  switch(numedge){
   case 0:
    nodepos[0] = 0; nodepos[1] = 2; nodepos[2] = 1;
    break;
   case 1:
    nodepos[0] = 1; nodepos[1] = 3; nodepos[2] = 2;
    break;
   case 2:
    nodepos[0] = 2; nodepos[1] = 0; nodepos[2] = 3;
    break;
   case 3:
    nodepos[0] = 3; nodepos[1] = 1; nodepos[2] = 0;
  }
  SPoint3 pt0(e->getVertex(nodepos[0])->x(),e->getVertex(nodepos[0])->y(),e->getVertex(nodepos[0])->z());
  SPoint3 pt2(e->getVertex(nodepos[1])->x(),e->getVertex(nodepos[1])->y(),e->getVertex(nodepos[1])->z());
  SPoint3 pt3(e->getVertex(nodepos[2])->x(),e->getVertex(nodepos[2])->y(),e->getVertex(nodepos[2])->z());

  SVector3 v03(pt3,pt0);
  SVector3 v23(pt3,pt2);
  SVector3 vectprod = crossprod(v03,v23);
  return vectprod.norm()/v03.norm()*(double)(e->getNumPrimaryVertices())/(double(e->getNumVertices()));
*/
}
