//
// Description: Keys for interface creation
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef _INTERFACEKEYS_H_
#define _INTERFACEKEYS_H_
class interfaceKeys{
 public:
  interfaceKeys(){}
  ~interfaceKeys(){}
  virtual bool operator==(const interfaceKeys &key)const=0;
};

class interfaceKeys2D : public interfaceKeys{
 private:
  int _i1,_i2; // i1 > i2 by construction
 public:
  interfaceKeys2D(const int i1, const int i2);
  ~interfaceKeys2D(){};
   virtual bool operator==(const interfaceKeys &key)const;
};

class interfaceKeys3D : public interfaceKeys{
 private:
  int _i1,_i2,_i3; //_i1 > _i2 > _i3
 public:
   interfaceKeys3D();
   void setValues(const int i1, const int i2, const int i3);
   void setValues(const int i1, const int i2, const int i3, const int i4);
   ~interfaceKeys3D(){};
   virtual bool operator==(const interfaceKeys &key)const;
};

#endif // _INTERFACEKEYS_H_
