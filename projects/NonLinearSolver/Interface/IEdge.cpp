//
// C++ Interface: terms
//
// Description: Class to bluid an interface line (used at initialization)
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "IEdge.h"
