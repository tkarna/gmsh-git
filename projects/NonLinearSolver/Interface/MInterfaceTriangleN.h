//
// C++ Interface: terms
//
// Description: Class of interface element of triangle used for DG
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
// Has to be merge with interface element defined in dg project HOW ??

#ifndef _MINTERFACETRIANGLEN_H_
#define _MINTERFACETRIANGLEN_H_
#include "MTriangle.h"
#include "MLine.h"
#include "MVertex.h"
#include "MInterfaceElement.h"
class MInterfaceTriangleN : public MTriangleN, public MInterfaceElement{ // or don't derivate but in this case a vector with the vertices of interface element has to be save ??
  protected :
    // table of pointer on the two elements linked to the interface element
    MElement *_numElem[2];
    // edge's number linked to interface element of minus and plus element
    int _numFace[2];
    // dir = true if the edge and the interface element are defined in the same sens and dir = false otherwise
    bool _dir[2];

    int _permutation[2]; //element 0 should be 0 and element 1 will be 0, 1 or 2 to get matches vertices

  public :

    MInterfaceTriangleN(std::vector<MVertex*> &v, char order, int num = 0, int part = 0, MElement *e_minus = 0, MElement *e_plus = 0);

    // Destructor
    ~MInterfaceTriangleN(){}

    // Give the number of minus 0 or plus 1 element
    MElement* getElem(int index) const {return _numElem[index];}

    void getuvwOnElem(const double u, const double v, double &uem, double &vem, double &wem, double &uep, double &vep, double &wep);

    // Return the edge number of element
    int getEdgeOrFaceNumber(const int i) const {return _numFace[i];}

    // Return the local vertex number of interface
    void getLocalVertexNum(const int i,std::vector<int> &vn);
    // Compute the characteristic size of the side h_s = max_e (area_e/perimeter_e)
    double getCharacteristicSize() {
      double cs =this->getInnerRadius();
      cs = cs/(this->getPolynomialOrder()*this->getPolynomialOrder());
      return cs;
    }
    int getNum() const
    {
      return MTriangleN::getNum();
    }

    bool isSameDirection(const int i) const {return _dir[i];}
    int  getPermutation(const int i) const {return _permutation[i];}
    int getNumVertices() const { return MTriangleN::getNumVertices();}

};
#endif // _MINTERFACETRIANGLEN_H_
