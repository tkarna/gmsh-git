//
// C++ Interface: material law
//
// Description: gurson elasto-plastic law with non local damage interface
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "mlawNonLocalDamageGurson.h"
#include <math.h>
#include "MInterfaceElement.h"

void mlawNonLocalDamageGurson::logSTensor3(const STensor3& a, STensor3 &loga, STensor43 *dloga, STensor63 *ddloga) const
{
  /* linear approximation for now */
  static const STensor3 I(-1.);
  loga = I;
  loga+= a;
  if(dloga !=NULL)
  {
    (*dloga) = _I4;
  }
  if(ddloga !=NULL)
  {
    (*ddloga) *= 0.;
  }
}

void mlawNonLocalDamageGurson::expSTensor3(const STensor3 &a,STensor3 &expa,STensor43 *dexpa, STensor63 *ddexpa) const
{
  /*linear approximation for now */
  static const STensor3 I(1.);
  expa = I;
  expa += a;

  if(dexpa!=NULL)
  {
    (*dexpa) = _I4;
  }
  if(ddexpa!=NULL)
  {
    (*ddexpa) *= 0.;
  }
}

mlawNonLocalDamageGurson::mlawNonLocalDamageGurson(const int num,const double E,const double nu, const double rho, 
                           const double q1, const double q2, const double q3, const double fC, 
                           const double ff, const double ffstar, const double fVinitial,
				const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw, const GursonDamageNucleation &gdn,
                                const double tol, const bool matrixbyPerturbation, const double pert) : 
                                               materialLaw(num,true), _E(E), _nu(nu), _rho(rho),
                                               _lambda((E*nu)/(1.+nu)/(1.-2.*nu)),
                                               _mu(0.5*E/(1.+nu)),_K(E/3./(1.-2.*nu)), _K3(3.*_K), _mu3(3.*_mu),
                                               _mu2(2.*_mu), _tol(tol),_perturbationfactor(pert),
                                               _tangentByPerturbation(matrixbyPerturbation ),
                                               _q1(q1), _q2(q2), _q3(q3), _fC(fC), _ff(ff), _ffstar(ffstar), 
                                               _fVinitial(fVinitial),
                                               _I4(1.,1.), _I(1.)
{
    _j2IH  = j2IH.clone();
    _cLLaw = cLLaw.clone();
    _gdn   = gdn.clone();
    Cel=0.;
    Cel(0,0,0,0) = _lambda + _mu2;
    Cel(1,1,0,0) = _lambda;
    Cel(2,2,0,0) = _lambda;
    Cel(0,0,1,1) = _lambda;
    Cel(1,1,1,1) = _lambda + _mu2;
    Cel(2,2,1,1) = _lambda;
    Cel(0,0,2,2) = _lambda;
    Cel(1,1,2,2) = _lambda;
    Cel(2,2,2,2) = _lambda + _mu2;

    Cel(1,0,1,0) = _mu;
    Cel(2,0,2,0) = _mu;
    Cel(0,1,0,1) = _mu;
    Cel(2,1,2,1) = _mu;
    Cel(0,2,0,2) = _mu;
    Cel(1,2,1,2) = _mu;

    Cel(0,1,1,0) = _mu;
    Cel(0,2,2,0) = _mu;
    Cel(1,0,0,1) = _mu;
    Cel(1,2,2,1) = _mu;
    Cel(2,0,0,2) = _mu;
    Cel(2,1,1,2) = _mu;

   _Idev = _I4;
   STensor3 mIon3(-1./3);
   STensor43 mIIon3;
   tensprod(_I,mIon3, mIIon3);
   _Idev += mIIon3;
}
mlawNonLocalDamageGurson::mlawNonLocalDamageGurson(const mlawNonLocalDamageGurson &source) : 
                                                         materialLaw(source),_E(source._E), _nu(source._nu), _rho(source._rho),
                                                         _lambda(source._lambda),
                                                         _mu(source._mu), _K(source._K),_K3(source._K3), _mu3(source._mu3),
                                                         _mu2(source._mu2), _tol(source._tol),
                                                         _perturbationfactor(source._perturbationfactor),
                                                         _tangentByPerturbation(source._tangentByPerturbation),
                                                         _q1(source._q1), _q2(source._q2), _q3(source._q3), _fC(source._fC),
                                                         _ff(source._ff), _ffstar(source._ffstar), 
                                                         _fVinitial(source._fVinitial),
                                                         Cel(source.Cel), _I4(source._I4), _I(source._I), _Idev(source._Idev)


{
  if(source._j2IH != NULL)
  {
    _j2IH=source._j2IH->clone();
  }
  if(source._cLLaw != NULL)
  {
    _cLLaw=source._cLLaw->clone();
  }
  if(source._gdn != NULL)
  {
    _gdn = source._gdn->clone();
  }

}

mlawNonLocalDamageGurson& mlawNonLocalDamageGurson::operator = (const materialLaw &source)
{
  materialLaw::operator=(source);
  const mlawNonLocalDamageGurson* src =static_cast<const mlawNonLocalDamageGurson*>(&source);
  if(src != NULL)
  {
    _E = src->_E;
    _nu = src->_nu;
    _rho = src->_rho;
    _lambda = src->_lambda;
    _mu = src->_mu;
    _K = src->_K;
    _K3 = src->_K3;
    _mu3 = src->_mu3;
    _mu2 = src->_mu2;
    _tol = src->_tol;
    _perturbationfactor = src->_perturbationfactor;
    _tangentByPerturbation = src->_tangentByPerturbation;
    if(_j2IH != NULL) delete _j2IH;
    if(src->_j2IH != NULL)
    {
      _j2IH=src->_j2IH->clone();
    }
    if(_cLLaw != NULL) delete _cLLaw;
    if(src->_cLLaw != NULL)
    {
      _cLLaw=src->_cLLaw->clone();
    }
    if(_gdn != NULL) delete _gdn;
    if(src->_gdn != NULL)
    {
      _gdn = src->_gdn->clone();
    }
    _q1=src->_q1;
    _q2=src->_q2;
    _q3=src->_q3;
    _fC=src->_fC;
    _ff=src->_ff;
    _ffstar=src->_ffstar;
    _fVinitial=src->_fVinitial;
    Cel = src->Cel;
    _I4 = src->_I4;
    _I = src->_I;
    _Idev = src->_Idev;
  }
  return *this;
}

void mlawNonLocalDamageGurson::createIPState(IPStateBase* &ips,const bool* state_,const MElement *ele, const int nbFF_, const int gpt) const
{
  //bool inter=true;
  //const MInterfaceElement *iele = static_cast<const MInterfaceElement*>(ele);
  //if(iele==NULL) inter=false;
  IPVariable* ipvi = new IPNonLocalDamageGurson(_fVinitial,_j2IH,_cLLaw,_gdn);
  IPVariable* ipv1 = new IPNonLocalDamageGurson(_fVinitial,_j2IH,_cLLaw,_gdn);
  IPVariable* ipv2 = new IPNonLocalDamageGurson(_fVinitial,_j2IH,_cLLaw,_gdn);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}
void mlawNonLocalDamageGurson::createIPState(IPNonLocalDamageGurson *ivi, IPNonLocalDamageGurson *iv1, IPNonLocalDamageGurson *iv2) const
{

}
void mlawNonLocalDamageGurson::createIPVariable(IPNonLocalDamageGurson *&ipv,const MElement *ele,const int nbFF) const
{

}

double mlawNonLocalDamageGurson::soundSpeed() const
{
  double factornu = (1.-_nu)/((1.+_nu)*(1.-2.*_nu));
  return sqrt(_E*factornu/_rho);
}


void mlawNonLocalDamageGurson::constitutive(const STensor3& F0,const STensor3& Fn,STensor3 &P,const IPNonLocalDamageGurson *q0,
	IPNonLocalDamageGurson *q1,STensor43 &Tangent,
                                const bool stiff) const
{
  Msg::Error("mlawNonLocalDamageGurson::constitutive: local implementation does not exist");
}

void mlawNonLocalDamageGurson::constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPNonLocalDamageGurson *ipvprev,       // array of initial internal variable
                            IPNonLocalDamageGurson *ipvcur,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            STensor3  &dLocalCorrectedPorosityDStrain,
  			    STensor3  &dStressDNonLocalCorrectedPorosity,
                            double    &dLocalCorrectedPorosityDNonLocalCorrectedPorosity,
                            const bool stiff            // if true compute the tangents
                           ) const
{
  double hatp0 = ipvprev->getMatrixPlasticStrain();
  _cLLaw->computeCL(hatp0, ipvcur->getRefToIPCLength());

  static STensor3 Fp1;
  double fVstar = 0.;

  double tildefVstar = ipvcur->getNonLocalCorrectedPorosity();
 
  static STensor3 dDeltaGammadC, dDeltaHatPdC, dtrNpdC, dDeltafVdC;
  static STensor43 dNpDevdC, dKCordC, dFpdC;
  double dDeltaGammadtildefVstar, dDeltaHatPdtildefVstar, dtrNpdtildefVstar, dDeltafVdtildefVstar;
  static STensor3 dNpDevdtildefVstar, dKCordtildefVstar, dFpdtildefVstar;

  static STensor3 Fe1, kcor;
  static STensor43 Le;
  static STensor63 dLe;
  predictorCorrector(tildefVstar, Fn, ipvprev->getConstRefToFp(), Fp1, ipvprev, ipvcur, P, fVstar, stiff, 
                     dDeltaGammadC, dDeltaHatPdC, dtrNpdC, dDeltafVdC, dNpDevdC, dKCordC, dFpdC,
                     dDeltaGammadtildefVstar, dDeltaHatPdtildefVstar, 
                     dtrNpdtildefVstar, dDeltafVdtildefVstar,dNpDevdtildefVstar, dKCordtildefVstar, dFpdtildefVstar,
                     Fe1, kcor, Le, dLe);
  ipvcur->getRefToLocalCorrectedPorosity() = fVstar;

  if(stiff)
  {
    if (_tangentByPerturbation)
    {
      this->tangent_full_perturbation(Tangent, dLocalCorrectedPorosityDStrain,
  			    dStressDNonLocalCorrectedPorosity, dLocalCorrectedPorosityDNonLocalCorrectedPorosity,
                            P,Fn, ipvprev->getConstRefToFp(),Fp1,tildefVstar, fVstar, ipvprev,ipvcur);


    }
    else
    {
      this->tangent_full_analytic(Tangent, dLocalCorrectedPorosityDStrain,
  			    dStressDNonLocalCorrectedPorosity, dLocalCorrectedPorosityDNonLocalCorrectedPorosity, 	 	
				P,Fn, ipvprev->getConstRefToFp(),Fp1,tildefVstar, fVstar, ipvprev,ipvcur, 
                                dKCordC, dFpdC,dDeltafVdC,dKCordtildefVstar, dFpdtildefVstar,dDeltafVdtildefVstar, 
                                Fe1, kcor, Le, dLe);
     /* this->tangent_full_perturbation(Tangent, dLocalCorrectedPorosityDStrain,
  			    dStressDNonLocalCorrectedPorosity, dLocalCorrectedPorosityDNonLocalCorrectedPorosity,
                            P,Fn, ipvprev->getConstRefToFp(),Fp1,tildefVstar, fVstar, ipvprev,ipvcur);*/
    }
  }

}

void mlawNonLocalDamageGurson::predictorCorrector(double tildefVstar, const STensor3& F1_, const STensor3& Fp0_, 
                                         STensor3& Fp1_, const IPNonLocalDamageGurson *q0_,
                                         IPNonLocalDamageGurson *q1_,   STensor3&P_, double &fVstar, bool stiff, 
                                         STensor3 &dDeltaGammadC, STensor3 &dDeltaHatPdC, STensor3 &dtrNpdC, 
                                                STensor3 &dDeltafVdC, STensor43 &dNpDevdC,STensor43 &dKCordC,
                                                STensor43 &dFpdC,
                                                double &dDeltaGammadtildefVstar, double &dDeltaHatPdtildefVstar, 
                                                double &dtrNpdtildefVstar,
                                                double &dDeltafVdtildefVstar, STensor3 &dNpDevdtildefVstar,
                                                STensor3 &dKCordtildefVstar, STensor3 &dFpdtildefVstar, 
                                                STensor3 & Fe1, STensor3 & kcor, STensor43 &Le, STensor63 &dLe) const
{
  /* initialize terms */
  dDeltaGammadC*=0.;
  dDeltaHatPdC*=0.;
  dtrNpdC*=0.; 
  dDeltafVdC*=0.;
  dDeltaGammadtildefVstar=0.;
  dDeltaHatPdtildefVstar=0.;
  dtrNpdtildefVstar=0.;
  dDeltafVdtildefVstar=0.;

  /* compute elastic predictor */
  Fp1_ = Fp0_; 
  double &hatP      = q1_->getRefToMatrixPlasticStrain();
  double hatP0      = q0_->getMatrixPlasticStrain();
  double DeltaHatP  = 0;
  hatP              = hatP0;

  double &fV        = q1_->getRefToLocalPorosity();
  double fV0        = q0_->getLocalPorosity();
  double DeltafV    = 0;
  fV                = fV0;
 
  //evaluate A from previous plastic strain
  
  _gdn->rate(hatP,hatP0,q1_->getRefToIPGursonNucleation());
  double A0 = (q1_->getConstRefToIPGursonNucleation()).getDFdot();

  double DeltaGamma = 0;

  static STensor3 Fp1inv;
  Fp1inv = Fp1_.invert();

  static STensor3 Fe1pr;
  Fe1pr  = F1_;
  Fe1pr *= Fp1inv;

  static STensor3 Cepr;
  Cepr  =Fe1pr.transpose();
  Cepr *= Fe1pr;

  Fe1=Fe1pr;
  static STensor3 Ce;
  Ce=Cepr;

  static STensor3 logsqrtCepr;
  static STensor43 Lpr;
  static STensor3 logsqrtCe;
  static STensor43 ENp;

  logSTensor3(Cepr,logsqrtCepr,&Lpr,&dLe); 
  logsqrtCepr*=0.5;
  logsqrtCe   = logsqrtCepr;
  Le          = Lpr;

  ENp= _I4;

  // corrotational Kirchhoff predictor
  static STensor3 kprDev;
  double ppr;
  KirchhoffStress(kprDev, ppr, logsqrtCepr);

  // corrotational Kirchhoff stress
  kcor = kprDev;
  kcor(0,0)+=ppr;
  kcor(1,1)+=ppr;
  kcor(2,2)+=ppr;

 
  _j2IH->hardening(hatP0+DeltaHatP, q1_->getRefToIPJ2IsotropicHardening());
  double yield   = q1_->getConstRefToIPJ2IsotropicHardening().getR();
  double yield0  = yield;
  double h       = q1_->getConstRefToIPJ2IsotropicHardening().getDR();

  /* Test plasticity */

  double f = yieldFunction(kprDev, ppr, yield, tildefVstar);
 
  static fullMatrix<double> Jinv(4,4);
  static fullMatrix<double> J(4,4);
  J.setAll(0.);
  Jinv.setAll(0.);
  double trNp = 0.;

  if( f>0.)
  {
    // flow direction predictor
    static STensor3 Npprdev; 
    double trNppr;
    yieldNormal(Npprdev, trNppr, kprDev, ppr, yield, tildefVstar);



    /* Plastic correction */
    #if 0
      printf("plasticity\n");
    #endif
    int ite = 0, maxite = 1000;
    static fullVector<double> res(4);
    computeResidual(res, tildefVstar, kprDev, ppr, yield, yield0,DeltaHatP, DeltaGamma, trNp, fV0, DeltafV,A0);
//    while(fabs(f) > _tol or ite <1)
    double tolNR=1.e-6;
    while(fabs(f) > tolNR or ite <1)
    {

      computeJacobian(J, tildefVstar, kprDev, ppr, yield, yield0,h, DeltaHatP, DeltaGamma, trNp, fV0, DeltafV,A0);
      J(0,0) *=yield0;
      J(1,0) *=yield0;
      J(2,0) *=yield0;
      J(3,0) *=yield0;
      J(0,2) /=yield0;
      J(1,2) /=yield0;
      J(2,2) /=yield0;
      J(3,2) /=yield0;

      static fullVector<double> sol(4);
      sol.setAll(0.);
      //bool solvelu = J.luSolve(res, sol);
      //if(!solvelu)   Msg::Fatal("No inversion of J in plastic correction in gurson !!");
       bool solve = J.invert(Jinv);
      if(!solve)   Msg::Fatal("No inversion of J in plastic correction in gurson !!");
      for(int i=0; i<4; i++)
        for(int j=0; j<4; j++)
          sol(i) += Jinv(i,j)*res(j);
        
      if(-sol(0)*yield0>yield/10.)         
        DeltaGamma += yield/10.;
      else if (DeltaGamma-sol(0)*yield0<0)
        DeltaGamma /= 2.;
      else
        DeltaGamma -= sol(0)*yield0;
      if(-sol(1)>0.1)
        DeltaHatP  += 0.1;
      else if (DeltaHatP-sol(1)<0)
        DeltaHatP  /=2.;
      else
        DeltaHatP  -= sol(1);
      _j2IH->hardening(hatP0+DeltaHatP, q1_->getRefToIPJ2IsotropicHardening());
      yield   = q1_->getConstRefToIPJ2IsotropicHardening().getR();
      h       = q1_->getConstRefToIPJ2IsotropicHardening().getDR();

      if(fabs(ppr-_K*DeltaGamma*(trNp-sol(2)/yield0))>5.*yield0)
        trNp  = 9.*tildefVstar*_q1*_q2*_q2*ppr/2./yield/yield/(1.+9.*tildefVstar*_q1*_q2*_q2*_K*DeltaGamma/2./yield/yield);
      else if (trNp-sol(2)/yield0 <0.)
        trNp     /=2.;
      else
        trNp    -= sol(2)/yield0;
       
      if(-sol(3)>0.1)
        DeltafV +=0.1;
      else if(DeltafV-sol(3) <0.) 
        DeltafV /=2.;
      else if(fV0+DeltafV-sol(3) >0.999999999) 
        DeltafV = (0.999999999-fV0)/2.;
      else
        DeltafV -= sol(3);

      computeResidual(res, tildefVstar, kprDev, ppr, yield, yield0, DeltaHatP, DeltaGamma, trNp, fV0, DeltafV,A0);
      f = fabs(res.norm());
      if(fabs(f) < tolNR)
      {
        //recopute Jacobian for stiffness
        computeJacobian(J, tildefVstar, kprDev, ppr, yield, yield0,h, DeltaHatP, DeltaGamma, trNp, fV0, DeltafV,A0);
        J(0,0) *=yield0;
        J(1,0) *=yield0;
        J(2,0) *=yield0;
        J(3,0) *=yield0;
        J(0,2) /=yield0;
        J(1,2) /=yield0;
        J(2,2) /=yield0;
        J(3,2) /=yield0;
        bool solve = J.invert(Jinv);
      }
      ite++;
      //Msg::Error(" Gurson ite %d: Delta Gamma = %f, Delta Hat p = %f, trN = %f, DeltafV = %f, f= %f", ite, DeltaGamma/yield0, DeltaHatP, trNp*yield0, DeltafV, f);
//     #ifdef _DEBUG
      if(ite > maxite)
      {
        Msg::Fatal("No convergence for plastic correction in gurson !!");
        break;
      }
//     #endif // _DEBUG
    }
    // final state
    static STensor3 NpDev,DGNp;
    NpDev = kprDev;
    NpDev*= (3./(yield*yield+6.*_mu*DeltaGamma));
    DGNp=NpDev;
    DGNp(0,0) +=trNp/3.;
    DGNp(1,1) +=trNp/3.;
    DGNp(2,2) +=trNp/3.;
    DGNp*=DeltaGamma;

    static STensor3 dFp;
    expSTensor3(DGNp,dFp, &ENp);
    Fp1_ = dFp;
    Fp1_ *=Fp0_;
    Fp1inv = Fp1_.invert();
   
    Fe1  = F1_;
    Fe1 *= Fp1inv;

    Ce  =Fe1.transpose();
    Ce *= Fe1;

    logSTensor3(Ce,logsqrtCe,&Le,&dLe); 
    logsqrtCe*=0.5;

    // corrotational Kirchhoff stress
    /*static STensor3 kDev;
    double p;
    KirchhoffStress(kDev, p, logsqrtCe);

    kcor = kDev;
    kcor(0,0)+=p;
    kcor(1,1)+=p;
    kcor(2,2)+=p;*/
    // we use the expression used to derive the stiffness as the log is not exact
    kcor = NpDev;
    kcor *= (-2.*_mu*DeltaGamma);
    kcor +=kprDev;
    kcor(0,0) +=ppr - _K*DeltaGamma*trNp;
    kcor(1,1) +=ppr - _K*DeltaGamma*trNp;
    kcor(2,2) +=ppr - _K*DeltaGamma*trNp;
  }
    if(stiff)
    {
      computeDInternalVariables(dDeltaGammadC, dDeltaHatPdC, dtrNpdC, dDeltafVdC,
                                dDeltaGammadtildefVstar, dDeltaHatPdtildefVstar, dtrNpdtildefVstar,dDeltafVdtildefVstar, 
                                tildefVstar, Fp0_, kprDev, ppr, yield, h, yield0, DeltaHatP, DeltaGamma, trNp, 
                                                fV0, DeltafV, Lpr, Jinv);

      computeDNpDevAndDKCorAndDFp(dNpDevdC, dKCordC,dFpdC,dNpDevdtildefVstar,dKCordtildefVstar,dFpdtildefVstar,
                                                dDeltaGammadC,dDeltaHatPdC,dtrNpdC, dDeltafVdC,
                                                dDeltaGammadtildefVstar, dDeltaHatPdtildefVstar, 
                                                dtrNpdtildefVstar, dDeltafVdtildefVstar, 
						tildefVstar, Fp0_, kprDev, ppr,
						yield, h, yield0, DeltaHatP, 
                                                DeltaGamma, trNp, fV0, DeltafV, Lpr, ENp);
    }
    else
    {
       //for debug dump the internal variables to check with perturbation
       /*dDeltaGammadtildefVstar = DeltaGamma;
       dDeltaHatPdtildefVstar  = DeltaHatP; 
       dtrNpdtildefVstar       = trNp;
       dDeltafVdtildefVstar    = DeltafV;
       dNpDevdtildefVstar      = kprDev*(3./(yield*yield+6.*_mu*DeltaGamma));
       dKCordtildefVstar       = kcor;
       dFpdtildefVstar         = Fp1_;*/
    }
  //update internal variables
  hatP = hatP0 + DeltaHatP; 
  fV   = fV0   + DeltafV;
  q1_->getRefToFp() = Fp1_;
  q1_->getRefToElasticEnergy()=deformationEnergy(Ce);

  fVstar = fV;
  if(fVstar > _fC)
  {
    fVstar = _fC+(_ffstar-_fC)/(_ff-_fC)*(fV-_fC);
  }

  //corotational to PK1
  static STensor3 S;
  S*=0;
  for(int i=0; i<3; i++)
    for(int j=0; j<3; j++)
      for(int k=0; k<3; k++)
        for(int l=0; l<3; l++)
          S(i,j)+= Le(i,j,k,l)*kcor(k,l);
  P_=Fe1;
  P_*=S;
  P_*=Fp1inv.transpose();
}

void mlawNonLocalDamageGurson::KirchhoffStress(STensor3 &SigDev_, double &p, const STensor3 &E_) const
{
  double trace = E_.trace();
  
  double val = _mu2*trace/3.;
  p = _K*trace;
  SigDev_ = E_;
  SigDev_*= _mu2;
  SigDev_(0,0)-=val;
  SigDev_(1,1)-=val;
  SigDev_(2,2)-=val;
}

void mlawNonLocalDamageGurson::yieldNormal(STensor3 &Npdev, double &trNp, const STensor3 &kDev_, double p, double yield, 
                                                       double tildefVstar) const
{
   Npdev = kDev_;
   Npdev *= (3./yield/yield);

   trNp  = 3.*tildefVstar*_q1*_q2/yield*sinh(3.*_q2*p/2./yield);

}

double mlawNonLocalDamageGurson::yieldFunction(const STensor3 &kDev_, double p, double yield, 
                                                       double tildefVstar) const
{
   double keq2 = 3./2.*kDev_.dotprod();
   double f= keq2/yield/yield+2.*tildefVstar*_q1*cosh(3.*_q2*p/2./yield)-_q3*_q3*tildefVstar*tildefVstar-1.;
   return f;
}

void mlawNonLocalDamageGurson::computeResidual(fullVector<double> &res, 
                                                       double tildefVstar, const STensor3 &kprDev, double ppr,
						       double yield, double yield0, double DeltaHatP, 
                                                       double DeltaGamma, double trNp, 
                                                       double fVn, double DeltafV, double _A) const
{
   if(res.size()!=4) Msg::Error("mlawNonLocalDamageGurson::computeResidual: wrong size");
   

   res(0)=DeltaGamma*(3.*yield/(yield*yield+6.*_mu*DeltaGamma)/(yield*yield+6.*_mu*DeltaGamma)*kprDev.dotprod()+
                      (ppr-_K*DeltaGamma*trNp)/yield*trNp)-(1.-fVn-DeltafV/2.)*DeltaHatP;

   res(1)=3.*yield*yield/2./(yield*yield+6.*_mu*DeltaGamma)/(yield*yield+6.*_mu*DeltaGamma)*kprDev.dotprod()+
                      2.*tildefVstar*_q1*cosh(3.*_q2*(ppr-_K*DeltaGamma*trNp)/2./yield)-_q3*_q3*tildefVstar*tildefVstar-1.;

   res(2)=(3.*tildefVstar*_q1*_q2/yield*sinh(3.*_q2*(ppr-_K*DeltaGamma*trNp)/2./yield))-trNp;

   res(2)*=yield0; //normalize equation
 
   res(3)=(1.-fVn-DeltafV/2.)*trNp*DeltaGamma+_A*DeltaHatP-DeltafV; 

}  

void mlawNonLocalDamageGurson::computeJacobian(fullMatrix<double> &J, 
                                                       double tildefVstar, const STensor3 &kprDev, double ppr,
						       double yield, double yield0, double h, double DeltaHatP, 
                                                       double DeltaGamma, double trNp, 
                                                       double fVn, double DeltafV, double _A) const
{
   if(J.size1()!=4) Msg::Error("mlawNonLocalDamageGurson::computeResidual: wrong size");
   if(J.size2()!=4) Msg::Error("mlawNonLocalDamageGurson::computeResidual: wrong size");

   J(0,0)  = (3.*yield/(yield*yield+6.*_mu*DeltaGamma)/(yield*yield+6.*_mu*DeltaGamma)*kprDev.dotprod()+
                      (ppr-_K*DeltaGamma*trNp)/yield*trNp);
   J(0,0) += DeltaGamma*(-36.*_mu*yield/(yield*yield+6.*_mu*DeltaGamma)/(yield*yield+6.*_mu*DeltaGamma)/
                         (yield*yield+6.*_mu*DeltaGamma)*kprDev.dotprod()-_K*trNp*trNp/yield);
   J(0,1)  = DeltaGamma*((3.*h*(yield*yield+6.*_mu*DeltaGamma)-12.*h*yield*yield)/
                              (yield*yield+6.*_mu*DeltaGamma)/(yield*yield+6.*_mu*DeltaGamma)/(yield*yield+6.*_mu*DeltaGamma)*
                              kprDev.dotprod()-h*(ppr-_K*DeltaGamma*trNp)/yield/yield*trNp)- (1-fVn-DeltafV/2.);
   J(0,2)  = DeltaGamma*(ppr-2.*_K*DeltaGamma*trNp)/yield;
   J(0,3)  = DeltaHatP/2.;

   J(1,0)  = -18.*_mu*yield*yield/(yield*yield+6.*_mu*DeltaGamma)/(yield*yield+6.*_mu*DeltaGamma)/(yield*yield+6.*_mu*DeltaGamma)*
                     kprDev.dotprod()-3.*tildefVstar*_q1*_q2*_K*trNp/yield*sinh(3.*_q2*(ppr-_K*DeltaGamma*trNp)/2./yield);
   J(1,1)  = (2.*h*yield*(yield*yield+6.*_mu*DeltaGamma)-4.*h*yield*yield*yield)/
                     (yield*yield+6.*_mu*DeltaGamma)/(yield*yield+6.*_mu*DeltaGamma)/(yield*yield+6.*_mu*DeltaGamma)*
                     3./2.*kprDev.dotprod() - 3.*h*_q2*tildefVstar*_q1*(ppr-_K*DeltaGamma*trNp)/yield/yield*
                             sinh(3.*_q2*(ppr-_K*DeltaGamma*trNp)/2./yield);
   J(1,2)  = -3.*_q2*tildefVstar*_q1*_K*DeltaGamma/yield*sinh(3.*_q2*(ppr-_K*DeltaGamma*trNp)/2./yield);
   J(1,3)  = 0.;


   J(2,0)  = -9.*_K*trNp*tildefVstar*_q1*_q2*_q2/2./yield/yield*cosh(3.*_q2*(ppr-_K*DeltaGamma*trNp)/2./yield);
   J(2,1)  = -3.*tildefVstar*_q1*_q2*h/yield/yield*sinh(3.*_q2*(ppr-_K*DeltaGamma*trNp)/2./yield) -
             9.*h*tildefVstar*_q1*_q2*_q2*(ppr-_K*DeltaGamma*trNp)/2./yield/yield/yield*
                                     cosh(3.*_q2*(ppr-_K*DeltaGamma*trNp)/2./yield);
   J(2,2)  = -9.*_K*tildefVstar*_q1*_q2*_q2*DeltaGamma/2./yield/yield*cosh(3.*_q2*(ppr-_K*DeltaGamma*trNp)/2./yield)-1.;
   J(2,3)  =  0.;

   J(2,0) *=yield0;
   J(2,1) *=yield0;
   J(2,2) *=yield0;
   J(2,3) *=yield0;


   J(3,0)  = (1.-fVn-DeltafV/2.)*trNp;
   J(3,1)  = _A;
   J(3,2)  = (1.-fVn-DeltafV/.2)*DeltaGamma;
   J(3,3)  = -1.*(trNp*DeltaGamma/2.+1.);

}  
double mlawNonLocalDamageGurson::deformationEnergy(const STensor3 &C) const
{
  double Jac= sqrt((C(0,0) * (C(1,1) * C(2,2) - C(1,2) * C(2,1)) -
          C(0,1) * (C(1,0) * C(2,2) - C(1,2) * C(2,0)) +
          C(0,2) * (C(1,0) * C(2,1) - C(1,1) * C(2,0))));
  double lnJ = log(Jac);
  STensor3 logCdev;
  logSTensor3(C,logCdev,NULL);
  double trace = logCdev.trace();
  logCdev(0,0)-=trace/3.;
  logCdev(1,1)-=trace/3.;
  logCdev(2,2)-=trace/3.;

  double Psy = _K*0.5*lnJ*lnJ+_mu*0.25*dot(logCdev,logCdev);
  return Psy;;
}

void mlawNonLocalDamageGurson::tangent_full_perturbation(STensor43 &T_,
                            STensor3  &dLocalCorrectedPorosityDStrain,
  			    STensor3  &dStressDNonLocalCorrectedPorosity,
                            double    &dLocalCorrectedPorosityDNonLocalCorrectedPorosity,
                            const STensor3 &P_,const STensor3 &F_,const STensor3 &Fp0_,const STensor3 &Fp1_, 
                            double tildefVstar, double fVstar, 
                            const IPNonLocalDamageGurson* ipvprev,const IPNonLocalDamageGurson* ipvcur) const
{
    T_*=0;
    dLocalCorrectedPorosityDStrain*=0;
    dStressDNonLocalCorrectedPorosity*=0;
    dLocalCorrectedPorosityDNonLocalCorrectedPorosity*=0;
    IPNonLocalDamageGurson ipvp(*ipvcur);
    STensor3 Fpp(F_);
    STensor3 Fp1p(Fp1_);
    STensor3 Pp(0.0);
    double fVstarp =fVstar;
    static STensor3 dDeltaGammadC, dDeltaHatPdC, dtrNpdC, dDeltafVdC;
    static STensor43 dNpDevdC, dKCordC, dFpdC;
    double dDeltaGammadtildefVstar, dDeltaHatPdtildefVstar, dtrNpdtildefVstar, dDeltafVdtildefVstar;
    static STensor3 dNpDevdtildefVstar, dKCordtildefVstar, dFpdtildefVstar;
    static STensor3 Fe1p, kCorp; 
    static STensor43 Lep; 
    static STensor63 dLep;
      //for debug get the internal parameter
      /*  static STensor3 dDeltaGammadF, dDeltaHatPdF, dtrNpdF, dDeltafVdF;
        static STensor43 dNpDevdF,dKCordF, dFpdF;
        double dDeltaGammadtildefVstarp, dDeltaHatPdtildefVstarp, dtrNpdtildefVstarp, dDeltafVdtildefVstarp;
        static STensor3 dNpDevdtildefVstarp,dKCordtildefVstarp,dFpdtildefVstarp;
        this->predictorCorrector(tildefVstar, Fpp, ipvprev->getConstRefToFp(), Fp1p, ipvprev, &ipvp, Pp, fVstarp, true, 
                     dDeltaGammadF, dDeltaHatPdF, dtrNpdF, dDeltafVdF, dNpDevdC, dKCordC, dFpdC,
                     dDeltaGammadtildefVstarp, dDeltaHatPdtildefVstarp, 
                     dtrNpdtildefVstarp, dDeltafVdtildefVstarp,dNpDevdtildefVstarp, dKCordtildefVstarp, dFpdtildefVstarp,
                     Fe1p, kCorp, Lep, dLep);
        dDeltaGammadF*=(Fpp.transpose()*2.);
        dDeltaHatPdF*=(Fpp.transpose()*2.);
        dtrNpdF*=(Fpp.transpose()*2.);
        dDeltafVdF*=(Fpp.transpose()*2.);
        for(int i=0;i <3; i++)
          for(int j=0;j <3; j++)
            for(int k=0;k <3; k++)
              for(int l=0;l <3; l++)
              {
                dNpDevdF(i,j,k,l) =0.;
                dKCordF(i,j,k,l) =0.;
                dFpdF(i,j,k,l) =0.;
                for(int m=0;m <3; m++)
                {
                  dNpDevdF(i,j,k,l) += dNpDevdC(i,j,k,m)*Fpp(l,m)+dNpDevdC(i,j,m,l)*Fpp(k,m);
                  dKCordF(i,j,k,l)  += dKCordC(i,j,k,m)*Fpp(l,m)+dKCordC(i,j,m,l)*Fpp(k,m);
                  dFpdF(i,j,k,l)    += dFpdC(i,j,k,m)*Fpp(l,m)+dFpdC(i,j,m,l)*Fpp(k,m);
                }
              }
        double DeltaGamma, DeltaHatP, trNp, DeltafV;
        static STensor3 NpDev, KCor, Fp;
        this->predictorCorrector(tildefVstar, Fpp, ipvprev->getConstRefToFp(), Fp1p, ipvprev, &ipvp, Pp, fVstarp, false, 
                       dDeltaGammadC, dDeltaHatPdC, dtrNpdC, dDeltafVdC, dNpDevdC, dKCordC, dFpdC,
                       DeltaGamma, DeltaHatP, trNp, DeltafV, NpDev, KCor, Fp,
                       Fe1p, kCorp, Lep, dLep);*/
      //end debug

    //perturbation with F
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        Fpp = F_;
        ipvp.operator=((const IPVariable &)*ipvcur);
        Fpp(i,j)+=_perturbationfactor;
        fVstarp =fVstar;
        this->predictorCorrector(tildefVstar, Fpp, ipvprev->getConstRefToFp(), Fp1p, ipvprev, &ipvp, Pp, fVstarp, false, 
                     dDeltaGammadC, dDeltaHatPdC, dtrNpdC, dDeltafVdC, dNpDevdC, dKCordC, dFpdC,
                     dDeltaGammadtildefVstar, dDeltaHatPdtildefVstar, 
                     dtrNpdtildefVstar, dDeltafVdtildefVstar, dNpDevdtildefVstar, dKCordtildefVstar, dFpdtildefVstar,
                     Fe1p, kCorp, Lep, dLep);
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            T_(k,l,i,j) = (Pp(k,l)-P_(k,l))/_perturbationfactor;
              //for debug
              /*   dNpDevdF(k,l,i,j) = (dNpDevdtildefVstar(k,l)-NpDev(k,l))/_perturbationfactor;
                 dKCordF(k,l,i,j) = (dKCordtildefVstar(k,l)-KCor(k,l))/_perturbationfactor;
                 dFpdF(k,l,i,j) = (dFpdtildefVstar(k,l)-Fp(k,l))/_perturbationfactor;*/
              //
          }
        }
        dLocalCorrectedPorosityDStrain(i,j) = (fVstarp-fVstar)/_perturbationfactor;
          //for debug get the internal parameter
            /* dDeltaGammadF(i,j) = (dDeltaGammadtildefVstar-DeltaGamma)/_perturbationfactor;
             dDeltaHatPdF(i,j)   = (dDeltaHatPdtildefVstar-DeltaHatP)/_perturbationfactor;
             dtrNpdF(i,j) = (dtrNpdtildefVstar-trNp)/_perturbationfactor;
             dDeltafVdF(i,j) = (dDeltafVdtildefVstar-DeltafV)/_perturbationfactor;*/
          //end debug

      }
    }
    //perturbation with ftildestar
    Fpp = F_;
    ipvp.operator=((const IPVariable &)*ipvcur);
    double tildefVstarp = tildefVstar+_perturbationfactor;
    this->predictorCorrector(tildefVstarp, Fpp, ipvprev->getConstRefToFp(), Fp1p, ipvprev, &ipvp, Pp, fVstarp, false, 
                     dDeltaGammadC, dDeltaHatPdC, dtrNpdC, dDeltafVdC, dNpDevdC, dKCordC, dFpdC,
                     dDeltaGammadtildefVstar, dDeltaHatPdtildefVstar, 
                     dtrNpdtildefVstar, dDeltafVdtildefVstar, dNpDevdtildefVstar, dKCordtildefVstar, dFpdtildefVstar,
                     Fe1p, kCorp, Lep, dLep);
    for (int k=0; k<3; k++){
       for (int l=0; l<3; l++){
          dStressDNonLocalCorrectedPorosity(k,l) = (Pp(k,l)-P_(k,l))/_perturbationfactor;
            // for debug
            /*  dNpDevdtildefVstarp(k,l)= (dNpDevdtildefVstar(k,l)-NpDev(k,l))/_perturbationfactor;
              dKCordtildefVstarp(k,l)= (dKCordtildefVstar(k,l)-KCor(k,l))/_perturbationfactor;
              dFpdtildefVstarp(k,l)= (dFpdtildefVstar(k,l)-Fp(k,l))/_perturbationfactor;*/
            //end debug

          
       }
    }
    dLocalCorrectedPorosityDNonLocalCorrectedPorosity=(fVstarp-fVstar)/_perturbationfactor;
      //for debug get the internal parameter
      /*  dDeltaGammadtildefVstarp = (dDeltaGammadtildefVstar-DeltaGamma)/_perturbationfactor;
        dDeltaHatPdtildefVstarp   = (dDeltaHatPdtildefVstar-DeltaHatP)/_perturbationfactor;
        dtrNpdtildefVstarp = (dtrNpdtildefVstar-trNp)/_perturbationfactor;
        dDeltafVdtildefVstarp = (dDeltafVdtildefVstar-DeltafV)/_perturbationfactor;
        double a=0;*/
      //end debug
}

void mlawNonLocalDamageGurson::tangent_full_analytic(STensor43 &T_,
                            STensor3  &dLocalCorrectedPorosityDStrain,
  			    STensor3  &dStressDNonLocalCorrectedPorosity,
                            double    &dLocalCorrectedPorosityDNonLocalCorrectedPorosity,
                            const STensor3 &P_,const STensor3 &F_,const STensor3 &Fp0_,const STensor3 &Fp1_, 
                            double tildefVstar, double fVstar, 
                            const IPNonLocalDamageGurson* ipvprev,const IPNonLocalDamageGurson* ipvcur,
                            const STensor43 &dKCordC, const STensor43 &dFpdC,const STensor3 &dDeltafVdC,
                            const STensor3 &dKCordtildefVstar, const STensor3 &dFpdtildefVstar, 
                            const double &dDeltafVdtildefVstar,
                            const STensor3 & Fe1, const STensor3 & kCor, const STensor43 &Le, const STensor63 &dLe) const
{

  // from d /dC -> d /dF
  static STensor43 dKCordF, dFpdF;
  for( int i=0; i<3; i++)
    for( int j=0; j<3; j++)
      for( int k=0; k<3; k++)
        for( int l=0; l<3; l++)
        {
          dKCordF(i,j,k,l) = 0.;
          dFpdF(i,j,k,l)   = 0.;
          for( int m=0; m<3; m++)
          {
            dKCordF(i,j,k,l) += dKCordC(i,j,l,m)*F_(k,m)+dKCordC(i,j,m,l)*F_(k,m);
            dFpdF(i,j,k,l)   += dFpdC(i,j,l,m)*F_(k,m)+dFpdC(i,j,m,l)*F_(k,m);
          }
        }
  static STensor3 dDeltafVdF;
  for( int i=0; i<3; i++)
    for( int j=0; j<3; j++)
    {
      dDeltafVdF(i,j) = 0.;
      for( int m=0; m<3; m++)
      {
        dDeltafVdF(i,j) += dDeltafVdC(i,m)*F_(j,m)+dDeltafVdC(m,i)*F_(j,m);
      }
    }
  
  // d Fp-1 / d
  static STensor3 Fpinv;
  Fpinv = Fp1_.invert();
  static STensor43 dFpinvdF;
  static STensor3  dFpinvdtildefVstar;
  for( int i=0; i<3; i++)
    for( int j=0; j<3; j++)
      for( int k=0; k<3; k++)
        for( int l=0; l<3; l++)
        {
          dFpinvdF(i,j,k,l)   = 0.;
          for( int m=0; m<3; m++)
          {
            for( int n=0; n<3; n++)
            {
              dFpinvdF(i,j,k,l)   -= Fpinv(i,m)*dFpdF(m,n,k,l)*Fpinv(n,j);
            }
          }
        }
  for( int i=0; i<3; i++)
    for( int j=0; j<3; j++)
    {
      dFpinvdtildefVstar(i,j)   = 0.;
      for( int m=0; m<3; m++)
      {
        for( int n=0; n<3; n++)
        {
          dFpinvdtildefVstar(i,j)   -= Fpinv(i,m)*dFpdtildefVstar(m,n)*Fpinv(n,j);
        }
      }
    }
  
  // dFe / d 
  static STensor43 dFedF;
  static STensor3 dFedtildefVstar;
  for( int i=0; i<3; i++)
    for( int j=0; j<3; j++)
      for( int k=0; k<3; k++)
        for( int l=0; l<3; l++)
        {
          dFedF(i,j,k,l) = _I(i,k)*Fpinv(l,j);
          for( int m=0; m<3; m++)
            dFedF(i,j,k,l) += F_(i,m)*dFpinvdF(m,j,k,l);
        }
  dFedtildefVstar  = F_;
  dFedtildefVstar *=  dFpinvdtildefVstar;

  static STensor3 LeKCor;
  LeKCor = Le*kCor;

  static STensor43 LedKCordF;
  for( int i=0; i<3; i++)
    for( int j=0; j<3; j++)
      for( int k=0; k<3; k++)
        for( int l=0; l<3; l++)
        {
          LedKCordF(i,j,k,l)   = 0.;
          for( int m=0; m<3; m++)
            for( int n=0; n<3; n++)
              LedKCordF(i,j,k,l)  += Le(i,j,m,n)*dKCordF(m,n,k,l);
         }
  static STensor3 LedKCordtildefVstar;        
  LedKCordtildefVstar = Le*dKCordtildefVstar;

  // dLe / d
  // Here we assume Le constant given the Log approximation
  /*static STensor63 dLedF;
  static STensor43 dLedtildefVstar;
  for( int p=0; p<3; p++)
     for( int q=0; q<3; q++)
       for( int i=0; i<3; i++)
         for( int j=0; j<3; j++)
           for( int k=0; k<3; k++)
             for( int l=0; l<3; l++)
             {
               dLedF(p,q,i,j,k,l)   = 0.;
               for( int m=0; m<3; m++)
               {
                 for( int n=0; n<3; n++)
                 {
                   for( int o=0; o<3; o++)
                   {
                     dLedF(p,q,i,j,k,l) += dLe(p,q,i,j,m,n)*(dFedF(o,m,k,l)*Fe1(o,n)+Fe1(m,o)*dFedF(o,n,k,l));
                   }
                }
              }
            }
  for( int i=0; i<3; i++)
    for( int j=0; j<3; j++)
      for( int k=0; k<3; k++)
        for( int l=0; l<3; l++)
        {
          dLedtildefVstar(i,j,k,l)   = 0.;
          for( int m=0; m<3; m++)
          {
            for( int n=0; n<3; n++)
            {
              for( int o=0; o<3; o++)
              {
                dLedtildefVstar(i,j,k,l) += dLe(i,j,k,l,m,n)*(dFedtildefVstar(o,m)*Fe1(o,n)+Fe1(m,o)*dFedtildefVstar(o,n));
              }
            }
          }
        }

  static STensor43 dLedFKCor;
  for( int i=0; i<3; i++)
    for( int j=0; j<3; j++)
      for( int k=0; k<3; k++)
        for( int l=0; l<3; l++)
        {
          dLedFKCor(i,j,k,l)   = 0.;
          for( int m=0; m<3; m++)
            for( int n=0; n<3; n++)
              dLedFKCor(i,j,k,l)  += dLedF(i,j,m,n,k,l)*kCor(m,n);
         }
  static STensor3 dLedtildefVstarKCor;  
  dLedtildefVstarKCor = dLedtildefVstar*kCor;
  */
  for( int i=0; i<3; i++)
    for( int j=0; j<3; j++)
      for( int k=0; k<3; k++)
        for( int l=0; l<3; l++)
        {
          T_(i,j,k,l)   = 0.;
          for( int m=0; m<3; m++)
            for( int n=0; n<3; n++)
            {
              T_(i,j,k,l)  += dFedF(i,m,k,l)*LeKCor(m,n)*Fpinv(j,n);
              T_(i,j,k,l)  += Fe1(i,m)*LedKCordF(m,n,k,l)*Fpinv(j,n);
              //T_(i,j,k,l)  += Fe1(i,m)*dLedFKCor(m,n,k,l)*Fpinv(j,n);
              T_(i,j,k,l)  += Fe1(i,m)*LeKCor(m,n)*dFpinvdF(j,n,k,l);
            }
         }
  
  for( int i=0; i<3; i++)
    for( int j=0; j<3; j++)
    {
      dStressDNonLocalCorrectedPorosity(i,j)   = 0.;
      for( int m=0; m<3; m++)
         for( int n=0; n<3; n++)
         {
           dStressDNonLocalCorrectedPorosity(i,j)  += dFedtildefVstar(i,m)*LeKCor(m,n)*Fpinv(j,n);
           dStressDNonLocalCorrectedPorosity(i,j)  += Fe1(i,m)*LedKCordtildefVstar(m,n)*Fpinv(j,n);
           //dStressDNonLocalCorrectedPorosity(i,j)  += Fe1(i,m)*dLedtildefVstarKCor(m,n)*Fpinv(j,n);
           dStressDNonLocalCorrectedPorosity(i,j)  += Fe1(i,m)*LeKCor(m,n)*dFpinvdtildefVstar(j,n);
         }
    }

   dLocalCorrectedPorosityDStrain                    = dDeltafVdF;
   dLocalCorrectedPorosityDNonLocalCorrectedPorosity = dDeltafVdtildefVstar;
   if(tildefVstar > _fC)
   {
     dLocalCorrectedPorosityDStrain                    *= (_ffstar-_fC)/(_ff-_fC);
     dLocalCorrectedPorosityDNonLocalCorrectedPorosity *= (_ffstar-_fC)/(_ff-_fC);
   }

}

void mlawNonLocalDamageGurson::computeDResidual(STensor3 &dres0dC, STensor3 &dres1dC, STensor3 &dres2dC, STensor3 &dres3dC,
                                                double &dres0dtildefVstar, double &dres1dtildefVstar, double &dres2dtildefVstar,
                                                double &dres3dtildefVstar, 
                                                double tildefVstar, const STensor3 &Fppr, const STensor3 &kprDev, double ppr,
						double yield, double h, double yield0, double DeltaHatP, 
                                                double DeltaGamma, double trNp, 
                                                double fVn, double DeltafV, const STensor43 &L) const
{
   static STensor3 Fpprinv;
   Fpprinv = Fppr.invert();
   static STensor3 FpprinvT;
   FpprinvT = Fpprinv.transpose();
 
   static STensor3 kprDevIdev;
   kprDevIdev = kprDev*_Idev;
   static STensor3 kprDevIdevL;
   kprDevIdevL = kprDevIdev*L;

   static STensor3 FpinvkprDevIdevLFpinvT;
   FpinvkprDevIdevLFpinvT = Fpprinv;
   FpinvkprDevIdevLFpinvT *= kprDevIdevL;
   FpinvkprDevIdevLFpinvT *= FpprinvT;

   static STensor3 IL;
   IL = _I*L;
   
   static STensor3 FpinvILFpinvT;
   FpinvILFpinvT = Fpprinv;
   FpinvILFpinvT *= IL;
   FpinvILFpinvT *= FpprinvT;

   double rat1= 6.*DeltaGamma*yield*_mu/(yield*yield+6*_mu*DeltaGamma)/(yield*yield+6*_mu*DeltaGamma);
   double rat2= DeltaGamma*trNp*_K/2./yield;
   dres0dC = FpinvkprDevIdevLFpinvT*rat1;
   dres0dC += FpinvILFpinvT*rat2;

   double rat3= 3.*yield*yield*_mu/(yield*yield+6*_mu*DeltaGamma)/(yield*yield+6*_mu*DeltaGamma);
   double rat4= 3.*tildefVstar*_q1*_q2*_K/2./yield*sinh(3.*_q2*(ppr-_K*DeltaGamma*trNp)/2./yield);
   dres1dC = FpinvkprDevIdevLFpinvT*rat3;
   dres1dC += FpinvILFpinvT*rat4;

   double rat6= 9.*tildefVstar*_q1*_q2*_q2*_K/4./yield/yield*cosh(3.*_q2*(ppr-_K*DeltaGamma*trNp)/2./yield);
   dres2dC = FpinvILFpinvT*rat6;

   dres2dC *=yield0; //normalize

   dres3dC  *=0.;

   dres0dtildefVstar =0;

   dres1dtildefVstar =2.*_q1*cosh(3.*_q2*(ppr-_K*DeltaGamma*trNp)/2./yield)-2*_q3*_q3*tildefVstar;

   dres2dtildefVstar =3.*_q1*_q2/yield*sinh(3.*_q2*(ppr-_K*DeltaGamma*trNp)/2./yield);
   dres2dtildefVstar *=yield0; //normalize

   dres3dtildefVstar =0.;

}

void mlawNonLocalDamageGurson::computeDInternalVariables(
                                                STensor3 &dDeltaGammadC, STensor3 &dDeltaHatPdC, STensor3 &dtrNpdC, 
                                                STensor3 &dDeltafVdC,
                                                double &dDeltaGammadtildefVstar, double &dDeltaHatPdtildefVstar, 
                                                double &dtrNpdtildefVstar,
                                                double &dDeltafVdtildefVstar, 
                                                double tildefVstar, const STensor3 &Fppr, const STensor3 &kprDev, double ppr,
						double yield, double h, double yield0, double DeltaHatP, 
                                                double DeltaGamma, double trNp, 
                                                double fVn, double DeltafV, const STensor43 &L, 
                                                const fullMatrix<double> &scaledJinv) const
{
  static STensor3 dres0dC, dres1dC, dres2dC, dres3dC;
  double dres0dtildefVstar, dres1dtildefVstar, dres2dtildefVstar, dres3dtildefVstar;

  computeDResidual(dres0dC,dres1dC,dres2dC, dres3dC, dres0dtildefVstar, dres1dtildefVstar, dres2dtildefVstar,
                   dres3dtildefVstar, tildefVstar, Fppr, kprDev, ppr, yield, h, yield0, DeltaHatP, 
                   DeltaGamma, trNp, fVn, DeltafV, L);

  static fullVector<double> residual(4);
  static fullVector<double> sol(4);
  for(int i=0; i<3; i++)
  {
    for(int j=0; j<3; j++)
    {
       residual(0) = -dres0dC(i,j);
       residual(1) = -dres1dC(i,j);
       residual(2) = -dres2dC(i,j);
       residual(3) = -dres3dC(i,j);
       for(int k=0; k<4; k++)
       {
         sol(k)=0.;
         for(int l=0; l<4; l++)
           sol(k) += scaledJinv(k,l)*residual(l);
       }
       dDeltaGammadC(i,j) = sol(0)*yield0;
       dDeltaHatPdC(i,j)  = sol(1);
       dtrNpdC(i,j)       = sol(2)/yield0;
       dDeltafVdC(i,j)    = sol(3);
    }
  }
  residual(0) = -dres0dtildefVstar;
  residual(1) = -dres1dtildefVstar;
  residual(2) = -dres2dtildefVstar;
  residual(3) = -dres3dtildefVstar;
  for(int k=0; k<4; k++)
  {
    sol(k)=0.;
    for(int l=0; l<4; l++)
      sol(k) += scaledJinv(k,l)*residual(l);
  }
  dDeltaGammadtildefVstar = sol(0)*yield0;
  dDeltaHatPdtildefVstar  = sol(1);
  dtrNpdtildefVstar       = sol(2)/yield0;
  dDeltafVdtildefVstar    = sol(3);
  
}

void mlawNonLocalDamageGurson::computeDNpDevAndDKCorAndDFp(   STensor43 &dNpDevdC, STensor43 &dKCordC, STensor43 &dFpdC,
                                                        STensor3 &dNpDevdtildefVstar,
                                                        STensor3 &dKCordtildefVstar, STensor3 &dFpdtildefVstar,
                                                const STensor3 &dDeltaGammadC,const  STensor3 &dDeltaHatPdC,
                                                const STensor3 &dtrNpdC, const STensor3 &dDeltafVdC,
                                                double dDeltaGammadtildefVstar, double dDeltaHatPdtildefVstar, 
                                                double dtrNpdtildefVstar, double dDeltafVdtildefVstar, 
						double tildefVstar, const STensor3 &Fppr, const STensor3 &kprDev, double ppr,
						double yield, double h, double yield0, double DeltaHatP, 
                                                double DeltaGamma, double trNp, 
                                                double fVn, double DeltafV, const STensor43 &Lpr, const STensor43 &ENp) const
{

   static STensor3 Fpprinv;
   Fpprinv = Fppr.invert();

   static STensor43 LFPprinvTFPprinv;
   for(int i=0; i<3; i++)
     for(int j=0; j<3; j++)
       for(int k=0; k<3; k++)
         for(int l=0; l<3; l++)
         {
           LFPprinvTFPprinv(i,j,k,l)=0.;
           for(int m=0; m<3; m++)
             for(int n=0; n<3; n++)
               LFPprinvTFPprinv(i,j,k,l)+= Lpr(i,j,m,n)*Fpprinv(k,m)*Fpprinv(l,n);
         }

   double rat1 = 3.*_mu/(yield*yield+6.*_mu*DeltaGamma);
   double rat2 = -6.*h*yield/(yield*yield+6.*_mu*DeltaGamma)/(yield*yield+6.*_mu*DeltaGamma);
   double rat3 = -18.*_mu/(yield*yield+6.*_mu*DeltaGamma)/(yield*yield+6.*_mu*DeltaGamma);
   for(int i=0; i<3; i++)
     for(int j=0; j<3; j++)
       for(int k=0; k<3; k++)
         for(int l=0; l<3; l++)
         {
           dNpDevdC(i,j,k,l)=kprDev(i,j)*dDeltaHatPdC(k,l)*rat2+kprDev(i,j)*dDeltaGammadC(k,l)*rat3;
           for(int m=0; m<3; m++)
             for(int n=0; n<3; n++)
               dNpDevdC(i,j,k,l)+= _Idev(i,j,m,n)*LFPprinvTFPprinv(m,n,k,l)*rat1 ;
         }

   dNpDevdtildefVstar = kprDev;
   dNpDevdtildefVstar*= rat2*dDeltaHatPdtildefVstar+rat3*dDeltaGammadtildefVstar;

   static STensor3 IL;
   IL = _I*Lpr;
   static STensor3 NpDev;
   NpDev = kprDev;
   NpDev*= (3./(yield*yield+6.*_mu*DeltaGamma));

   for(int i=0; i<3; i++)
     for(int j=0; j<3; j++)
       for(int k=0; k<3; k++)
         for(int l=0; l<3; l++)
         {
           dKCordC(i,j,k,l)  = -_K*DeltaGamma*dtrNpdC(k,l)-_K*trNp*dDeltaGammadC(k,l);   
           dKCordC(i,j,k,l) += -2.*_mu* NpDev(i,j)*dDeltaGammadC(k,l)-2.*_mu*DeltaGamma*dNpDevdC(i,j,k,l);
           for(int m=0; m<3; m++)
             for(int n=0; n<3; n++)
               dKCordC(i,j,k,l)+= (_I(i,j)*_K/2.*_I(m,n)*LFPprinvTFPprinv(m,n,k,l))+_mu*_Idev(i,j,m,n)*LFPprinvTFPprinv(m,n,k,l);
         }


  dKCordtildefVstar  = _I*(-_K*DeltaGamma*dtrNpdtildefVstar-_K*trNp*dDeltaGammadtildefVstar);
  dKCordtildefVstar += NpDev*(-2.*_mu*dDeltaGammadtildefVstar);
  dKCordtildefVstar += dNpDevdtildefVstar*(-2.*_mu*DeltaGamma);


   static STensor43 EdGammaNpdC;
   for(int i=0; i<3; i++)
     for(int j=0; j<3; j++)
       for(int k=0; k<3; k++)
         for(int l=0; l<3; l++)
         {
           EdGammaNpdC(i,j,k,l)=0.;
           for(int m=0; m<3; m++)
             for(int n=0; n<3; n++)
               EdGammaNpdC(i,j,k,l)+= ENp(i,j,m,n)*(NpDev(m,n)*dDeltaGammadC(k,l)+DeltaGamma*dNpDevdC(m,n,k,l)+
                                                    DeltaGamma/3.*_I(m,n)*dtrNpdC(k,l));
         }
   static STensor3  EdGammaNpdftildeVstar;
   for(int i=0; i<3; i++)
     for(int j=0; j<3; j++)
     {
        EdGammaNpdftildeVstar(i,j)=0.;
        for(int m=0; m<3; m++)
          for(int n=0; n<3; n++)
            EdGammaNpdftildeVstar(i,j)+= ENp(i,j,m,n)*(NpDev(m,n)*dDeltaGammadtildefVstar+DeltaGamma*dNpDevdtildefVstar(m,n)+
                                                    DeltaGamma/3.*_I(m,n)*dtrNpdtildefVstar);
         }

   for(int i=0; i<3; i++)
     for(int j=0; j<3; j++)
       for(int k=0; k<3; k++)
         for(int l=0; l<3; l++)
         {
           dFpdC(j,i,k,l)    = 0.;
           for(int m=0; m<3; m++)
             dFpdC(j,i,k,l)+= Fppr(m,i)*EdGammaNpdC(m,j,k,l);
         }

   for(int i=0; i<3; i++)
     for(int j=0; j<3; j++)
     {
       dFpdtildefVstar(j,i)    = 0.;
           for(int m=0; m<3; m++)
             dFpdtildefVstar(j,i)+= Fppr(m,i)*EdGammaNpdftildeVstar(m,j);
         }

}


