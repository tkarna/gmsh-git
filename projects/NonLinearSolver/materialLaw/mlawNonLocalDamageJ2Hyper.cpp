//
// C++ Interface: material law
//
// Description: j2 elasto-plastic law with non local damage interface
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "mlawNonLocalDamageJ2Hyper.h"
#include <math.h>
#include "MInterfaceElement.h"

mlawNonLocalDamageJ2Hyper::mlawNonLocalDamageJ2Hyper(const int num,const double E,const double nu,
                           const double rho, const J2IsotropicHardening &_j2IH,
                           const CLengthLaw &_clLaw, const DamageLaw &_damLaw, 
			   const double tol, const bool pert, const double eps) 
				: mlawJ2linear(num,E,nu,rho,_j2IH,tol,pert,eps)
{
  cLLaw = _clLaw.clone();
  damLaw = _damLaw.clone();
    Cel=0.;
    Cel(0,0,0,0) = _lambda + _mu2;
    Cel(1,1,0,0) = _lambda;
    Cel(2,2,0,0) = _lambda;
    Cel(0,0,1,1) = _lambda;
    Cel(1,1,1,1) = _lambda + _mu2;
    Cel(2,2,1,1) = _lambda;
    Cel(0,0,2,2) = _lambda;
    Cel(1,1,2,2) = _lambda;
    Cel(2,2,2,2) = _lambda + _mu2;

    Cel(1,0,1,0) = _mu;
    Cel(2,0,2,0) = _mu;
    Cel(0,1,0,1) = _mu;
    Cel(2,1,2,1) = _mu;
    Cel(0,2,0,2) = _mu;
    Cel(1,2,1,2) = _mu;

    Cel(0,1,1,0) = _mu;
    Cel(0,2,2,0) = _mu;
    Cel(1,0,0,1) = _mu;
    Cel(1,2,2,1) = _mu;
    Cel(2,0,0,2) = _mu;
    Cel(2,1,1,2) = _mu;

}
mlawNonLocalDamageJ2Hyper::mlawNonLocalDamageJ2Hyper(const mlawNonLocalDamageJ2Hyper &source) : mlawJ2linear(source)
{
  if(source.cLLaw != NULL)
  {
    cLLaw=source.cLLaw->clone();
  }
  if(source.damLaw != NULL)
  {
    damLaw=source.damLaw->clone();
  }
  Cel = source.Cel;
}

mlawNonLocalDamageJ2Hyper& mlawNonLocalDamageJ2Hyper::operator = (const materialLaw &source)
{
  mlawJ2linear::operator=(source);
  const mlawNonLocalDamageJ2Hyper* src = dynamic_cast<const mlawNonLocalDamageJ2Hyper*>(&source);
  if(src != NULL)
  {
    if(cLLaw != NULL) delete cLLaw;
    if(src->cLLaw != NULL)
    {
      cLLaw=src->cLLaw->clone();
    }
    if(damLaw != NULL) delete damLaw;
    if(src->damLaw != NULL)
    {
      damLaw=src->damLaw->clone();
    }
    Cel = src->Cel;
  }
  return *this;
}

void mlawNonLocalDamageJ2Hyper::createIPState(IPStateBase* &ips,const bool* state_,const MElement *ele, const int nbFF_, const int gpt) const
{
  //bool inter=true;
  //const MInterfaceElement *iele = static_cast<const MInterfaceElement*>(ele);
  //if(iele==NULL) inter=false;
  IPVariable* ipvi = new IPNonLocalDamageJ2Hyper(_j2IH,cLLaw,damLaw);
  IPVariable* ipv1 = new IPNonLocalDamageJ2Hyper(_j2IH,cLLaw,damLaw);
  IPVariable* ipv2 = new IPNonLocalDamageJ2Hyper(_j2IH,cLLaw,damLaw);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}
void mlawNonLocalDamageJ2Hyper::createIPState(IPNonLocalDamageJ2Hyper *ivi, IPNonLocalDamageJ2Hyper *iv1, IPNonLocalDamageJ2Hyper *iv2) const
{

}
void mlawNonLocalDamageJ2Hyper::createIPVariable(IPNonLocalDamageJ2Hyper *&ipv,const MElement *ele,const int nbFF) const
{

}

double mlawNonLocalDamageJ2Hyper::soundSpeed() const
{
  return mlawJ2linear::soundSpeed();
}


void mlawNonLocalDamageJ2Hyper::constitutive(const STensor3& F0,const STensor3& Fn,STensor3 &P,const IPNonLocalDamageJ2Hyper *q0,
	IPNonLocalDamageJ2Hyper *q1,STensor43 &Tangent,
                                const bool stiff) const
{
  mlawJ2linear::constitutive(F0,Fn,P,q0,q1,Tangent,stiff);
}

void mlawNonLocalDamageJ2Hyper::constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPNonLocalDamageJ2Hyper *ipvprev,       // array of initial internal variable
                            IPNonLocalDamageJ2Hyper *ipvcur,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            STensor3  &dLocalPlasticStrainDStrain,
  			                STensor3  &dStressDNonLocalPlasticStrain,
                            double   &dLocalPlasticStrainDNonLocalPlasticStrain,
                            const bool stiff            // if true compute the tangents
                           ) const
{
  double p0 = ipvprev->getCurrentPlasticStrain();
  cLLaw->computeCL(p0, ipvcur->getRefToIPCLength());

  static STensor43 dFpdF;
  static STensor3 Fe;
  static STensor3 Fpinv;

  dFpdF*=0.;
  mlawJ2linear::constitutive(F0,Fn,P,ipvprev,ipvcur,Tangent,stiff,dFpdF);
  const STensor3 &Fp  = ipvcur->getConstRefToFp();
  inverseSTensor3(Fp,Fpinv);
  multSTensor3(Fn,Fpinv,Fe);




  double ene = ipvcur->defoEnergy();
  STensor3 Peff(P);
  damLaw->computeDamage(ipvcur->getEffectivePlasticStrain(), 
                        ipvprev->getEffectivePlasticStrain(),
                        ene, Fe, Fp, Peff, Cel, 
                        ipvprev->getConstRefToIPDamage(),ipvcur->getRefToIPDamage());


  double D = ipvcur->getDamage();
  P*=(1.-D);

  double elEne = (1-D)*ene;
  ipvcur->_elasticEnergy=elEne;

  if(stiff)
  {
    const STensor3 &Fp0  = ipvprev->getConstRefToFp();
    static STensor3 Fp0inv;
    static STensor3 FpFp0inv;
    static STensor3 lnFpFp0inv;
    static STensor43 dlnFpFp0inv;
    inverseSTensor3(Fp0,Fp0inv);
    multSTensor3(Fp,Fp0inv,FpFp0inv);
    dlnFpFp0inv*=0.;
    logSTensor3(FpFp0inv,lnFpFp0inv,&dlnFpFp0inv);
    double dp=ipvcur->getCurrentPlasticStrain()-ipvprev->getCurrentPlasticStrain();
    STensor3 &dpdF=ipvcur->getRefToDLocalPlasticStrainDStrain();
    if(dp>1.e-16)
    {
      for(int k=0; k<3; k++)
      {
        for(int l=0; l<3; l++)
        {
          dpdF(k,l) = 0;
          for(int i=0; i<3; i++)
          {
            for(int j=0; j<3; j++)
            {
              for(int m=0; m<3; m++)
              {
                for(int n=0; n<3; n++)
                {
                  for(int o=0; o<3; o++)
                  {
                    dpdF(k,l) += 2./3./dp*dlnFpFp0inv(i,j,m,n)*lnFpFp0inv(i,j)*Fp0inv(o,n)*dFpdF(m,o,k,l);
                  } 
                }
              }
            }
          }
        }
      }
    }
    static STensor43 dFedF;
    dFedF=0;
    for(int i=0; i<3; i++)
    {
      for(int j=0; j<3; j++)
      {
        for(int k=0; k<3; k++)
        {
          for(int l=0; l<3; l++)
          {
            dFedF(i,j,k,l) = 0;
            if(i==k) dFedF(i,j,k,l) += Fpinv(l,j);
            for(int m=0; m<3; m++)
            {
              for(int n=0; n<3; n++)
              {
                dFedF(i,j,k,l) -= Fn(i,m)*dFpdF(m,n,k,l)*Fpinv(n,j);
              }
            }
          }
        }
      }
    }
    // we need to correct partial P/partial F: (1-D) partial P/partial F - Peff otimes partial D partial F
    Tangent*=(1.-D);
    for(int i=0;i<3;i++)
    {
      for(int j=0;j<3;j++)
      {
        for(int k=0;k<3;k++)
        {
          for(int l=0;l<3;l++)
          {
            for(int m=0; m<3; m++)
            {
              for(int n=0; n<3; n++)
              {
                Tangent(i,j,k,l)-=Peff(i,j)*ipvcur->getConstRefToDDamageDFe()(m,n)*dFedF(m,n,k,l);
              }
            }
          }
        }
      }
    }
   
    // partial p/partial F
    dLocalPlasticStrainDStrain = ipvcur->getConstRefToDLocalPlasticStrainDStrain();
 
    // -hat{P} partial D/partial tilde p 
    dStressDNonLocalPlasticStrain = Peff*(-1*ipvcur->getDDamageDp());

    // partial p partial tilde p (0 if no MFH)
    ipvcur->getRefToDLocalPlasticStrainDNonLocalPlasticStrain()=0;
    dLocalPlasticStrainDNonLocalPlasticStrain = ipvcur->getDLocalPlasticStrainDNonLocalPlasticStrain();

  }

}

