//
// C++ Interface: numericalMaterial
//
// Description: Class creates microsolver from setting
//
//
// Author:  <Van Dung NGUYEN>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "numericalMaterial.h"
#include "nonLinearMechSolver.h"
#include "nonLinearBC.h"
#include "numericalFunctions.h"
#include "dirent.h"

void numericalMaterial::distributeMacroIP(const std::vector<int>& allIPNum, const std::vector<int>& allRanks,
                                    std::map<int,std::set<int> >& mapIP){
  int IPSize = allIPNum.size();
  mapIP.clear();

  int rankSize = allRanks.size();
  // init map
  for (int i=0; i<rankSize; i++){
    int rank = allRanks[i];
    std::set<int> numElOnRank;
    mapIP[rank] = numElOnRank;
  };

  int numOnEachRank = IPSize/rankSize;
  for (int i=0; i<rankSize; i++){
    int rank = allRanks[i];
    std::set<int>& numElOnRank = mapIP[rank];
    for (int j=0; j<numOnEachRank; j++){
      int num = j+i*numOnEachRank;
      numElOnRank.insert(allIPNum[num]);
    };
  };
  int j=rankSize*numOnEachRank;
  while (j < IPSize){
    int i = j- rankSize*numOnEachRank;
    std::set<int>& numElOnRank = mapIP[allRanks[i]];
    numElOnRank.insert(allIPNum[j]);
    j++;
  };

  for (std::map<int,std::set<int> >::iterator it = mapIP.begin(); it!= mapIP.end(); it++){
    int rank = it->first;
    std::set<int>& setIP = it->second;
    std::string fname = "IPonRank"+int2str(rank)+".csv";
    FILE* file = fopen(fname.c_str(),"a");
    for (std::set<int>::iterator it = setIP.begin(); it!= setIP.end(); it++){
      int num = *it;
      int e, g;
      numericalMaterial::getTwoIntsFromType(num,e,g);
      fprintf(file, "%d;%d;\n",e,g);
    }
    fclose(file);
  }
};

numericalMaterial::numericalMaterial(int tag)
                   :_tag(tag),_bcTag(tag),_numstep(1),_tol(1e-6),_order(1),_method(1),_degree(3),
                    _virtualVertexFlag(false), _xPeriodicity(0.,0.,0.),_yPeriodicity(0.,0.,0.),_zPeriodicity(0.,0.,0.),
                    _stressFlag(true),_stressIntegFlag(0),
                    _tangentFlag(true),_tangentIntegFlag(0),_tangentPerturbation(1.e-7),
                    _isIterative(true),_stiffModification(true),
                    _stabilityAnalysis(false), _instabilitycriterion(1.e-8),
                    _perturbation(false),_perturbationfactor(0.01), _numeigenvalue(100), _eigenFollowing(false),
                    _isViewAll(false),_systemType(1), _rveVolume(0.),
                    _scheme(1),_solver(2), _pathFollowing(false),_controlType(1),
                    _allGP(-1),_microMeshFile(""),_microGeoFile(""),
                    _meshOrder(-1),_meshDim(-1),_meshIncomplete(false),_pbcNodeData(""){};

numericalMaterial::numericalMaterial(const numericalMaterial& src){
  _tag = src._tag; // for tag of microsolver
  _bcTag = src._bcTag;
  _numstep = src._numstep; // number of time step at microsolve
  _tol = src._tol; // tolerance used in NR at microsolver
  _order = src._order; // homogenization order 1- first, 2- second
  _systemType = src._systemType;
  _solver = src._solver;
  _isIterative = src._isIterative;
  _stiffModification = src._stiffModification;
  // micro-data
  _microMeshFile = src._microMeshFile;	// micro meshfile
  _microGeoFile = src._microGeoFile;
  _meshDim = src._meshDim;
  _meshOrder = src._meshOrder;
  _meshIncomplete = src._meshIncomplete;

  _xPeriodicity = src._xPeriodicity;
  _yPeriodicity = src._yPeriodicity;
  _zPeriodicity = src._zPeriodicity; // micro-perioidcity
  //periodic boundary condition
  _method = src._method;
  _degree = src._degree;  // periodic options
  _physical = src._physical;  // entities for applying periodic boundary condition
  _virtualVertexFlag = src._virtualVertexFlag; // add virtual vertex flag
  _stressFlag = src._stressFlag;
  _stressIntegFlag = src._stressIntegFlag; // 0- volume, 1-surface
  _tangentFlag = src._tangentFlag;
  _tangentIntegFlag = src._tangentIntegFlag;
  _tangentPerturbation = src._tangentPerturbation;
  _allView = src._allView; // micro-solver to view
  _isViewAll = src._isViewAll; // true if view all
  _allDomain = src._allDomain;
  _allMaterialLaw = src._allMaterialLaw;

  _stabilityAnalysis = src._stabilityAnalysis;
  _modeview = src._modeview;
  _numeigenvalue = src._numeigenvalue;
  _perturbation = src._perturbation;
  _perturbationfactor = src._perturbationfactor;
  _instabilitycriterion = src._instabilitycriterion;
  _eigenFollowing = src._eigenFollowing;

  _allGP = src._allGP;

  _pathFollowing = src._pathFollowing;
  _controlType = src._controlType;

  _pbcNodeData = src._pbcNodeData;
  _rveVolume = src._rveVolume;
};

numericalMaterial& numericalMaterial::operator = (const numericalMaterial& src){
  _tag = src._tag; // for tag of microsolver
  _bcTag = src._bcTag;
  _numstep = src._numstep; // number of time step at microsolve
  _tol = src._tol; // tolerance used in NR at microsolver
  _order = src._order; // homogenization order 1- first, 2- second
  _systemType = src._systemType;
  _solver = src._solver;
  _isIterative = src._isIterative;
  _stiffModification = src._stiffModification;
  // micro-data
  _microMeshFile = src._microMeshFile;	// micro meshfile
  _microGeoFile = src._microGeoFile;
  _meshDim = src._meshDim;
  _meshOrder = src._meshOrder;
  _meshIncomplete = src._meshIncomplete;

  _xPeriodicity = src._xPeriodicity;
  _yPeriodicity = src._yPeriodicity;
  _zPeriodicity = src._zPeriodicity; // micro-perioidcity
  //periodic boundary condition
  _method = src._method;
  _degree = src._degree;  // periodic options
  _physical = src._physical;  // entities for applying periodic boundary condition
  _virtualVertexFlag = src._virtualVertexFlag; // add virtual vertex flag
  _stressFlag = src._stressFlag;
  _stressIntegFlag = src._stressIntegFlag; // 0- volume, 1-surface
  _tangentFlag = src._tangentFlag;
  _tangentIntegFlag = src._tangentIntegFlag;
  _tangentPerturbation = src._tangentPerturbation;
  _allView = src._allView; // micro-solver to view
  _isViewAll = src._isViewAll; // true if view all
  _allDomain = src._allDomain;
  _allMaterialLaw = src._allMaterialLaw;

  _stabilityAnalysis = src._stabilityAnalysis;
  _modeview = src._modeview;
  _numeigenvalue = src._numeigenvalue;
  _perturbation = src._perturbation;
  _perturbationfactor = src._perturbationfactor;
  _instabilitycriterion = src._instabilitycriterion;
  _eigenFollowing = src._eigenFollowing;

  _allGP = src._allGP;

  _pathFollowing = src._pathFollowing;
  _controlType = src._controlType;

  _pbcNodeData = src._pbcNodeData;
  _rveVolume = src._rveVolume;
  return *this;
};
numericalMaterial::~numericalMaterial(){

};

nonLinearMechSolver* numericalMaterial::createMicroSolver(const int ele, const int gpt ) const {
  /**create serial micro-solver **/
  nonLinearMechSolver* solver = new nonLinearMechSolver(_tag,false);
  solver->setMicroSolverFlag(true);
  solver->setMultiscaleFlag(true);

  /** set microsolver identification **/
  solver->setMicroProblemIndentification(ele,gpt);
  int type = numericalMaterial::createTypeWithTwoInts(ele,gpt);

  std::map<int,int>::const_iterator itm = _meshIdMap.find(type);
  if (itm == _meshIdMap.end() or _allPertMesh.size() ==0){
    /**load model**/
    if (_microMeshFile.length() >0){
      Msg::Error("create model from mesh file %s",_microMeshFile.c_str());
      solver->loadModel(_microMeshFile);
    }
    else if (_microGeoFile.length()>0 ){
      Msg::Error("create model from geometry file %s",_microGeoFile.c_str());
      solver->createMicroModel(_microGeoFile,_meshDim,_meshOrder,_meshIncomplete);
    }
    else{
      Msg::Fatal("micro model file must be provided");
    }
  }
  else{
    std::map<int,std::string>::const_iterator itstr = _allPertMesh.find(itm->second);
    std::string mshFile = itstr->second;
    printf("perturbation mesh on ele %d gp %d \n",ele,gpt);
    printf("create model from mesh file %s  \n",mshFile.c_str());
    solver->loadModel(mshFile);
  }


  /** set domain for micro solver**/
  for (int i=0; i<_allDomain.size(); i++){
    partDomain* dom = _allDomain[i]->clone();
    if (dom) solver->addDomain(dom);
  };
  /** set material law**/
  if (_pertMaterialLawIP.find(type) == _pertMaterialLawIP.end()){
    for (int i=0; i<_allMaterialLaw.size(); i++){
      solver->addMaterialLaw(_allMaterialLaw[i]);
    };
  }
  else{
    Msg::Warning("perturbation in ele %d GP %d",ele,gpt);
    for (int i=0; i<_allPertubationLaw.size(); i++){
      solver->addMaterialLaw(_allPertubationLaw[i]);
    };
  }

  if (_rveVolume >0){
    solver->setRVEVolume(_rveVolume);
  }
  /**set micro periodicity**/
  solver->setPeriodicity(_xPeriodicity,_yPeriodicity,_zPeriodicity);

  /** set micro BC**/
  if (_bcType == nonLinearMicroBC::PBC){
    solver->addPeriodicBC(_bcTag,_physical);
    solver->setPeriodicBCOptions(_method,_degree,_virtualVertexFlag);
  }
  else if( _bcType == nonLinearMicroBC::LDBC){
    solver->addLinearDisplacementBC(_bcTag,_physical);
  }
  else if (_bcType == nonLinearMicroBC::MKBC){
    solver->addMinimalKinematicBC(_bcTag,_physical);
  }
  else{
    Msg::Fatal("Micro BC must be correctly defined");
  }
  /**set control type --> always load control**/
  solver->setControlType(0);
  /** set system type**/
  solver->setSystemType(_systemType);
  /** set stress method**/
  solver->setStressAveragingMethod(_stressIntegFlag);
  /** set tangent method**/
  solver->setTangentAveragingMethod(_tangentIntegFlag,_tangentPerturbation);
  /** set scheme**/
  solver->Scheme(_scheme);
  /** set solver **/
  solver->Solver(_solver);
  /** set order of homogenization**/
  solver->setOrder(_order);
  /**set numstep and tolerance, compute time always from 0 to 1 **/
  solver->snlData(_numstep,1.,_tol);
  /** set eigensolver analysis**/

  if (_allStabilityView.find(type) != _allStabilityView.end() or _stabilityAnalysis){
    solver->eigenValueSolver(_numeigenvalue,true);
    for (int i=0; i<_modeview.size(); i++)
      solver->setModeView(_modeview[i]);
    solver->setEigenSolverFollowing(_eigenFollowing);
    solver->setInstabilityCriterion(_instabilitycriterion);
    solver->setPerturbationFactor(_perturbationfactor);
    solver->perturbateBucklingModeByMinimalEigenValueMode(_perturbation);
  }
  /** set stiffness midification **/
  solver->stiffnessModification(_stiffModification);
  /** set iterative solver**/
	solver->iterativeProcedure(_isIterative);


  /** for viewing**/

  std::set<int>::iterator it = _allView.find(type);
  if (it!=_allView.end() or (_isViewAll and gpt==_allGP)){
    solver->setMessageView(1);
    solver->setStrainArchiveFlag(1);
    solver->setHomogenizationPropertyArchiveFlag(1);
    solver->setDisplacementAndIPArchiveFlag(1);
    solver->internalPointBuildView("Green-Lagrange_xx",IPField::STRAIN_XX, 1, 1);
    solver->internalPointBuildView("Green-Lagrange_yy",IPField::STRAIN_YY, 1, 1);
    solver->internalPointBuildView("Green-Lagrange_zz",IPField::STRAIN_ZZ, 1, 1);
    solver->internalPointBuildView("Green-Lagrange_xy",IPField::STRAIN_XY, 1, 1);
    solver->internalPointBuildView("Green-Lagrange_yz",IPField::STRAIN_YZ, 1, 1);
    solver->internalPointBuildView("Green-Lagrange_xz",IPField::STRAIN_XZ, 1, 1);
    solver->internalPointBuildView("sig_xx",IPField::SIG_XX, 1, 1);
    solver->internalPointBuildView("sig_yy",IPField::SIG_YY, 1, 1);
    solver->internalPointBuildView("sig_zz",IPField::SIG_ZZ, 1, 1);
    solver->internalPointBuildView("sig_xy",IPField::SIG_XY, 1, 1);
    solver->internalPointBuildView("sig_yz",IPField::SIG_YZ, 1, 1);
    solver->internalPointBuildView("sig_xz",IPField::SIG_XZ, 1, 1);
    solver->internalPointBuildView("sig_VM",IPField::SVM, 1, 1);
    solver->internalPointBuildView("Green-Lagrange equivalent strain",IPField::GLSTRAINEQUIVALENT, 1, 1);
    solver->internalPointBuildView("Equivalent plastic strain",IPField::PLASTICSTRAIN, 1, 1);
  }
  else{
    solver->setHomogenizationPropertyArchiveFlag(0);
    solver->setDisplacementAndIPArchiveFlag(0);
    solver->setMessageView(0);
    /** archive all strain to recompute if necessary**/
    solver->setStrainArchiveFlag(0);
  }
  /** initialize microsolver from setting data**/

  solver->pathFollowing(_pathFollowing);
  solver->setPathFollowingControlType(_controlType);

  // stress and tangent initial flag
  solver->stressAveragingFlag(_stressFlag);
  solver->tangentAveragingFlag(_tangentFlag);

  if (_pbcNodeData.length()>0){
    solver->readPBCDataFromFile(_pbcNodeData);
  }

  solver->initMicroSolver();
  return solver;
};
void numericalMaterial::loadModel(const std::string meshfile){
  _microMeshFile = meshfile;
};

void numericalMaterial::createMicroModel(const std::string geoFileName,const int dim,const int order,
                   const bool incomplete){
  _microGeoFile = geoFileName;
  _meshDim = dim;
  _meshOrder = order;
  _meshIncomplete = incomplete;
};

// set micro part domains
void numericalMaterial::addDomain(partDomain* domain){
  _allDomain.push_back(domain);
};
// set micro material law
void numericalMaterial::addMaterialLaw(materialLaw* mlaw){
  _allMaterialLaw.push_back(mlaw);
};

void numericalMaterial::setPerturbationMaterialLawIP(const int ele, const int ip){
  int num = numericalMaterial::createTypeWithTwoInts(ele,ip);
  _pertMaterialLawIP.insert(num);
};

void numericalMaterial::addPerturbationMaterialLaw(materialLaw* mlaw){
  _allPertubationLaw.push_back(mlaw);
}
// set micro periodicity
void numericalMaterial::setPeriodicity(const double x, const double y, const double z, const std::string direction){
  if ((direction =="x") or (direction =="X")){
		_xPeriodicity(0) = x;
		_xPeriodicity(1) = y;
		_xPeriodicity(2) = z;
	}
	else if ((direction =="y") or (direction =="Y")){
		_yPeriodicity(0) = x;
		_yPeriodicity(1) = y;
		_yPeriodicity(2) = z;
	}
	else if ((direction =="z") or (direction =="Z")){
		_zPeriodicity(0) = x;
		_zPeriodicity(1) = y;
		_zPeriodicity(2) = z;
	}
	else{
    Msg::Error("periodic geometry must be correctly defined");
	}
};
// set periodic solver options
void numericalMaterial::setPeriodicBCOptions(const int m, const int d,const bool addver){
  _method = m;
  _degree = d;
  _virtualVertexFlag = addver;
};
// set periodic boundary conditon entities
void numericalMaterial::addPeriodicBC(const int tag, const int g1, const int g2, const int g3,
                                      const int g4, const int g5, const int g6){
  _bcTag = tag;
  _physical.clear();
	_physical.push_back(g1);
	_physical.push_back(g2);
	_physical.push_back(g3);
	_physical.push_back(g4);
	_physical.push_back(g5);
	_physical.push_back(g6);
	_bcType = nonLinearMicroBC::PBC;
};

// set linear displacement condition
void numericalMaterial::addLinearDisplacementBC(const int tag, const int g1, const int g2, const int g3,
                                      const int g4, const int g5, const int g6 ){
  _bcTag = tag;
  _physical.clear();
	_physical.push_back(g1);
	_physical.push_back(g2);
	_physical.push_back(g3);
	_physical.push_back(g4);
	_physical.push_back(g5);
	_physical.push_back(g6);
	_bcType = nonLinearMicroBC::LDBC;
};
// set minimal kinematical condition
void numericalMaterial::addMinimalKinematicBC(const int tag, const int g1, const int g2, const int g3,
                                      const int g4, const int g5, const int g6){
  _bcTag = tag;
  _physical.clear();
	_physical.push_back(g1);
	_physical.push_back(g2);
	_physical.push_back(g3);
	_physical.push_back(g4);
	_physical.push_back(g5);
	_physical.push_back(g6);
	_bcType = nonLinearMicroBC::MKBC;
};

// set tolerence for micro newton-raphson
void numericalMaterial::setNumStep(const int i){
  _numstep = i;
};
		//
void numericalMaterial::setTolerance(const double tol){
  _tol = tol;
};
// set view to this element
void numericalMaterial::addViewMicroSolver(const int e, const int gp){
  int type = numericalMaterial::createTypeWithTwoInts(e,gp);
  _allView.insert(type);
};
// set view all elements
void numericalMaterial::setViewAllMicroProblems(const bool flag, const int a){
  _allGP = a;
  _isViewAll= flag;
};
// set order
void numericalMaterial::setOrder(const int o){
  _order = o;
};

void numericalMaterial::tangentAveragingFlag(const bool fl){
  _tangentFlag = fl;

};
void numericalMaterial::stressAveragingFlag(const bool fl){
  _stressFlag = fl;
};

void numericalMaterial::setStressAveragingMethod(const int method){
  _stressIntegFlag = method;
};
void numericalMaterial::setTangentAveragingMethod(const int method, const double prec){
  _tangentIntegFlag = method;
  _tangentPerturbation = prec;
};

void numericalMaterial::Scheme(const int s){
  _scheme = s;
};
//
void numericalMaterial::Solver(const int s){
  _solver = s;
};
//
void numericalMaterial::setSystemType(const int s){
  _systemType = s;
};

void numericalMaterial::stiffnessModification(const bool flag){
  _stiffModification = flag;
};
		//
void numericalMaterial::iterativeProcedure(const bool flag){
  _isIterative = flag;
};

void numericalMaterial::pathFollowing(const bool p){
  _pathFollowing = p;
};

void numericalMaterial::setPathFollowingControlType(const int i){
  _controlType = i;
};

void numericalMaterial::readPBCDataFromFile(std::string fn){
  _pbcNodeData = fn;
};

void numericalMaterial::setRVEVolume(const double val){
  _rveVolume = val;
};

void numericalMaterial::addStabilityMicroSolver(const int e, const int gp){
  int type = numericalMaterial::createTypeWithTwoInts(e,gp);
  _allStabilityView.insert(type);
};

void numericalMaterial::eigenValueSolver(const int num, const bool fl){
  _numeigenvalue = num;
  _stabilityAnalysis = fl;
}; // number of eigenvalue
void numericalMaterial::setModeView(const int view){
  _modeview.push_back(view);
}; // for view buckling mode
void numericalMaterial::setPerturbationFactor(const double val){
  _perturbationfactor = val;
};
void numericalMaterial::setInstabilityCriterion(const double val){
  _instabilitycriterion = val;
};
void numericalMaterial::perturbateBucklingModeByMinimalEigenValueMode(const bool flag){
  _perturbation = flag;
};
void numericalMaterial::setEigenSolverFollowing(const bool flag){
  _eigenFollowing = flag;
};

void numericalMaterial::loadAllRVEMeshes(const std::string prefix){
  DIR *dir;
  struct dirent *ent;
  fileNameFilter* filter = new fileNameFilter(prefix,".msh");
  printf("read all file in current folder \n");
  if ((dir = opendir (".")) != NULL) {
    /* get all the files and directories within directory */
    while ((ent = readdir (dir)) != NULL) {
      std::string filename = ent->d_name;
      int num = filter->getNumber(filename);
      if (num>0){
        _allPertMesh[num] = filename;
        printf("id: %d  -->filename: %s \n",num,filename.c_str());
      }
    }
    closedir (dir);
  }
  _idIterator = _allPertMesh.begin();
};

void numericalMaterial::printMeshIdMap() const{
  int rank = Msg::GetCommRank();
  std::string  filename = "idMap"+int2str(rank)+".txt";
  FILE* file =fopen(filename.c_str(),"w");
  for (std::map<int,int>::const_iterator it = _meshIdMap.begin(); it!= _meshIdMap.end(); it++){
    fprintf(file,"%d %d\n",it->first,it->second);
  }
  fclose(file);
};

void numericalMaterial::assignMeshId(const int e, const int gpt){
  if (_allPertMesh.size() == 0) return;
  int type = numericalMaterial::createTypeWithTwoInts(e, gpt);
  std::map<int,int>::iterator ittype = _meshIdMap.find(type);
  if (ittype!= _meshIdMap.end()) return;
  else{
    if (_idIterator == _allPertMesh.end())
      _idIterator = _allPertMesh.begin();


    _meshIdMap[type] = _idIterator->first;
    _idIterator++;
  }
};

void numericalMaterial::setMeshId(const int e, const int gpt, const int id) {
  if (_allPertMesh.size() == 0) return;
  int type = numericalMaterial::createTypeWithTwoInts(e, gpt);
  _meshIdMap[type] = id;
}

int numericalMaterial::getMeshId(const int e, const int gpt) const{
  int type = numericalMaterial::createTypeWithTwoInts(e,gpt);
  std::map<int,int>::const_iterator it = _meshIdMap.find(type);
  if (it !=_meshIdMap.end())
    return it->second;
  else{
    Msg::Warning("mesh id is not set: ELE %d GP %d",e,gpt);
    return 0;
  }
};

std::string numericalMaterial::getMeshFileName(const int e, const int gpt) const{
  if (_allPertMesh.size() ==0) return _microMeshFile;
  int id = this->getMeshId(e,gpt);
  std::map<int,std::string>::const_iterator it = _allPertMesh.find(id);
  if (it == _allPertMesh.end()){
    return _microMeshFile;
  }
  else{
    return it->second;
  }
};
#if defined(HAVE_MPI)
int numericalMaterial::getNumberValuesMPI() const {return _meshIdMap.size()*2;};
void numericalMaterial::fillValuesMPI(int* val) const{
  int iter=0;
  for (std::map<int,int>::const_iterator it = _meshIdMap.begin(); it!= _meshIdMap.end(); it++){
    val[iter] = it->first;
    val[iter+1] = it->second;
    iter+=2;
  }
};
void numericalMaterial::getValuesMPI(const int* val, const int size){
  for (int i=0; i<size; i+=2){
    _meshIdMap[val[i]] = val[i+1];
  }
};
#endif //HAVE_MPI



fileNameFilter::fileNameFilter(const std::string prefix, const std::string tag): _prefix(prefix),_fileTag(tag)  {}

int fileNameFilter::getNumber(const std::string& filename){
  if (filename.size() < _prefix.size() + _fileTag.size()) return 0;
  int count=0;
  for (int i=0; i<_prefix.size(); i++){
    if (_prefix[i]== filename[i])
      count++;
  }
  if (count != _prefix.size()) return 0;
  count=0;
  for (int i= _fileTag.size() -1; i>=0; i--){
    if (_fileTag[i] == filename[filename.size()-_fileTag.size()+i]){
      count++;
    }
  }
  if (count != _fileTag.size()) return 0;

  std::string temp;
  for (unsigned int i = 0; i<filename.size(); i++){
    if (isdigit(filename[i]))
      temp += filename[i];

  }

   std::istringstream stream(temp);
   int number;
   stream >> number;

   return number;
}


