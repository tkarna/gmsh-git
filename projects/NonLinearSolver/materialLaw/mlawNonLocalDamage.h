//
// C++ Interface: material law
//
// Description: non local damage elasto-plastic law it is a "virtual implementation" you have to define a law
//              in your project which derive from this law. you have to do THE SAME for your IPVariable
//              which have to derive from IPJ2linear (all data in this ipvarible have name beggining by _nld...
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWNONLOCALDAMAGE_H_
#define MLAWNONLOCALDAMAGE_H_
#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"
#include "ipNonLocalDamage.h"
#include "material.h"
#include "nonLocalDamageLaw.h"
class mlawNonLocalDamage : public materialLawWithDamage
{

 protected:
  double _rho,_nu0,_mu0; //maximal mu for anisotropic

  // to be compatible with umat: use same variable
  double sq2;
  mutable double* strn_n, *strs_n, *dstrn, *strs;
  mutable double* statev_n, *statev_1;
  mutable double** Calgo;
  mutable double** Cref;
  mutable double*** dCref;
  mutable double* tau;
  mutable double** dtau;


  MFH::Material* mat;

  double *props;
  int nprops1;

  int nsdv;

  mutable double* dpdE;
  mutable double* strs_dDdp_bar;
  mutable double** chara_Length;
  mutable double SpBar;

 public:
  mlawNonLocalDamage(const int num, const double rho, const char *propName);

 #ifndef SWIG
  mlawNonLocalDamage(const mlawNonLocalDamage &source);
  mlawNonLocalDamage& operator=(const materialLaw &source);
  ~mlawNonLocalDamage();
  // function of materialLaw
  virtual matname getType() const{return materialLaw::nonLocalDamage;}

  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const
  {
     Msg::Error("Cannot be called");
  }
  virtual void createIPState(IPNonLocalDamage *ivi, IPNonLocalDamage *iv1, IPNonLocalDamage *iv2) const;
  virtual void createIPVariable(IPNonLocalDamage *&ipv,const MElement *ele,const int nbFF) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
  virtual double soundSpeed() const; // default but you can redefine it for your case
  virtual const double bulkModulus() const;
  virtual const double shearModulus() const;
  virtual const double poissonRatio() const;
  // specific function
 public:
  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPNonLocalDamage *q0,       // array of initial internal variable
                            IPNonLocalDamage *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            const bool stiff            // if true compute the tangents
                           ) const;

  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPNonLocalDamage *q0,       // array of initial internal variable
                            IPNonLocalDamage *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            STensor3  &dLocalPlasticStrainDStrain,
  			                STensor3  &dStressDNonLocalPlasticStrain,
                            double   &dLocalPlasticStrainDNonLocalPlasticStrain,
                            const bool stiff            // if true compute the tangents
                           ) const;

  int getNsdv() const { return nsdv;}

 #endif // SWIG
};

MFH::Material *init_material(double *_props, int idmat);
MFH::Damage* init_damage(double* props, int iddam);
MFH::Clength* init_clength(double* props, int idclength);

#endif // MLAWNONLOCALDAMAGE_H_
