//
// C++ Interface: material law
//
// Description: non-local damage elasto-plastic law
//
//
// Author:  <L. NOELS>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include <math.h>
#include "MInterfaceElement.h"
#include <fstream>
#include "mlawNonLocalDamage.h"

mlawNonLocalDamage::mlawNonLocalDamage(const int num, const double rho,
                   const char *propName) : materialLawWithDamage(num,true), _rho(rho)
  {
	sq2 = sqrt(2.);

	//allocate memory
	mallocvector(&strn_n,6);
	mallocvector(&strs_n,6);
	mallocvector(&dstrn,6);
	mallocvector(&strs,6);

	mallocmatrix(&Calgo,6,6);
	mallocmatrix(&Cref,6,6);
	malloctens3(&dCref,6,6,6);
	mallocvector(&tau,6);
	mallocmatrix(&dtau,6,6);

        std::ifstream in(propName);
        if(!in) Msg::Fatal("Cannot open the file %s! Maybe is missing   ",propName);
        int dummy;
        in >> dummy;
        in.ignore(256,'\n');
        in >> nprops1;
        in.ignore(256,'\n');
        mallocvector(&props,nprops1);
        for (int i=0;i < nprops1; i++)
        {
          in >> props[i];
          in.ignore(256,'\n');
        }
        mat = init_material(props,0);
        nsdv = mat->get_nsdv();

	mallocvector(&dpdE,6);
        mallocvector(&strs_dDdp_bar,6);

        mallocmatrix(&chara_Length,3,3);              //by Wu Ling 2011/10/04
        SpBar = 0.0;

        double **C0;
        mallocmatrix(&C0,6,6);
	mat->get_elOp(C0);
        double C1111 = C0[0][0];
        double C2222 = C0[1][1];
        double C3333 = C0[2][2];
        double C1122 = C0[0][1];
        double C1212 = C0[3][3];
        double C1313 = C0[4][4];
        double C2323 = C0[5][5];
        // taka max value if anistotropy
        _mu0 = C1212/2.;
        _nu0 = C1111/2./(_mu0+C1111);
        if(C1313>_mu0)
        {
           _mu0 = C1313;
           _nu0 = C2222/2./(_mu0+C2222);
        }
        if(C2323>_mu0)
        {
           _mu0 = C2323;
           _nu0 = C3333/2./(_mu0+C3333);
        }
        if (_nu0 < 0 or _nu0 >0.5) Msg::Error("mlawNonLocalDamage: wrong Poisson coefficient");
	freematrix(C0,6);

  }
  mlawNonLocalDamage::mlawNonLocalDamage(const mlawNonLocalDamage &source) :
                                        materialLawWithDamage(source), _rho(source._rho), _mu0(source._mu0), _nu0(source._nu0)
  {
	sq2 = sqrt(2.);

	//allocate memory
	mallocvector(&strn_n,6);
	mallocvector(&strs_n,6);
	mallocvector(&dstrn,6);
	mallocvector(&strs,6);

	mallocmatrix(&Calgo,6,6);
	mallocmatrix(&Cref,6,6);
	malloctens3(&dCref,6,6,6);
	mallocvector(&tau,6);
	mallocmatrix(&dtau,6,6);

        for(int i=0; i< 6; i++)
        {
           strn_n[i] = source.strn_n[i];
           strs_n[i] = source.strs_n[i];
           dstrn[i]  = source.dstrn[i];
           strs[i]   = source.strs[i];
           strs[i]   = source.strs[i];
           tau[i]    = source.tau[i];

           for(int j=0; j<6; j++)
           {
              Calgo[i][j] = source.Calgo[i][j];
              Cref[i][j]  = source.Cref[i][j];
              dtau[i][j]  = source.dtau[i][j];
	      for(int k=0; k<6; k++)
              {
                dCref[i][j][k] = source.dCref[i][j][k];
              }
           }
         }
         nprops1 = source.nprops1;
         mallocvector(&props,nprops1);
         for (int i=0;i < nprops1; i++)
         {
           props[i] = source.props[i];
         }
        mat = init_material(props,0);
        nsdv = mat->get_nsdv();

	mallocvector(&dpdE,6);
        mallocvector(&strs_dDdp_bar,6);
        for(int i=0; i< 6; i++)
        {
           dpdE[i] = source.dpdE[i];
           strs_dDdp_bar[i] = source.strs_dDdp_bar[i];
        }

        mallocmatrix(&chara_Length,3,3);              //by Wu Ling 2011/10/04
        for(int i=0; i< 3; i++)
        {
           for (int j=0; j<3; j++)
               {
                chara_Length[i][j] = source.chara_Length[i][j];
               }
        }                                             // by Wu Ling 2011/10/04
        SpBar = source.SpBar;

  }

  mlawNonLocalDamage& mlawNonLocalDamage::operator=(const materialLaw &source)
  {
     materialLawWithDamage::operator=(source);
     const mlawNonLocalDamage* src =static_cast<const mlawNonLocalDamage*>(&source);
     _rho = src->_rho;

	sq2 = sqrt(2.);

        for(int i=0; i< 6; i++)
        {
           strn_n[i] = src->strn_n[i];
           strs_n[i] = src->strs_n[i];
           dstrn[i]  = src->dstrn[i];
           strs[i]   = src->strs[i];
           strs[i]   = src->strs[i];
           tau[i]    = src->tau[i];

           for(int j=0; j<6; j++)
           {
              Calgo[i][j] = src->Calgo[i][j];
              Cref[i][j]  = src->Cref[i][j];
              dtau[i][j]  = src->dtau[i][j];
	      for(int k=0; k<6; k++)
              {
                dCref[i][j][k] = src->dCref[i][j][k];
              }
           }
         }
         nprops1 = src->nprops1;
         for (int i=0;i < nprops1; i++)
         {
           props[i] = src->props[i];
         }
         mat = init_material(props,0);
         nsdv = mat->get_nsdv();

         for(int i=0; i< 6; i++)
         {
           dpdE[i] = src->dpdE[i];
           strs_dDdp_bar[i] = src->strs_dDdp_bar[i];
         }

         for(int i=0; i< 3; i++)
         {
           for (int j=0; j<3; j++)
               {
                chara_Length[i][j] = src->chara_Length[i][j];
               }
        }                                             // by Wu Ling 2011/10/04
        SpBar = src->SpBar;

  return *this;
}


  mlawNonLocalDamage::~mlawNonLocalDamage()
  {
    free(strn_n);
    free(strs_n);
    free(dstrn);
    free(strs);
    freematrix(Calgo,6);
    freematrix(Cref,6);
    freetens3(dCref,6,6);
    free(tau);
    freematrix(dtau,6);

    free(props);

    free(dpdE);
    free(strs_dDdp_bar);

    freematrix(chara_Length,3);           // by Wu Ling 2011/10/04
  }
void mlawNonLocalDamage::createIPState(IPNonLocalDamage *ivi, IPNonLocalDamage *iv1, IPNonLocalDamage *iv2) const
{

  //initialize Eshelby tensor
  int kstep = 1;
  int kinc  = 1;
  double *stvi = ivi->_nldStatev;
  double *stv1 = iv1->_nldStatev;
  double *stv2 = iv2->_nldStatev;
  double SpBartmp = 0.0;
  mat->constbox(dstrn, strs_n, strs, stvi, stv1, Cref, dCref, tau, dtau, Calgo, 1., dpdE, strs_dDdp_bar, &SpBartmp, chara_Length, kinc, kstep);
  for(int i=0;i<3;i++)
  {
    for(int j=0;j<3;j++){
      ivi->_nldCharacteristicLengthMatrix(i,j) = chara_Length[i][j];                 //by Wu Ling
      iv1->_nldCharacteristicLengthMatrix(i,j) = chara_Length[i][j];                 //by Wu Ling
      iv2->_nldCharacteristicLengthMatrix(i,j) = chara_Length[i][j];                 //by Wu Ling
    }
  }
  ivi->setPosStrMtx(mat->get_pos_mtx_stress());
  ivi->setPosStrInc(mat->get_pos_inc_stress());
  iv1->setPosStrMtx(mat->get_pos_mtx_stress());
  iv1->setPosStrInc(mat->get_pos_inc_stress());
  iv2->setPosStrMtx(mat->get_pos_mtx_stress());
  iv2->setPosStrInc(mat->get_pos_inc_stress());

  copyvect(stv1,stvi,nsdv);
  copyvect(stv1,stv2,nsdv);

}

void mlawNonLocalDamage::createIPVariable(IPNonLocalDamage *&ipv,const MElement *ele,const int nbFF) const
{
  //initialize Eshelby tensor
  int kstep = 1;
  int kinc  = 1;
  double *stvi = ipv->_nldStatev;
  double SpBartmp = 0.0;
                                           // stv1 ??
  mat->constbox(dstrn, strs_n, strs, stvi, stvi, Cref, dCref, tau, dtau, Calgo, 1., dpdE, strs_dDdp_bar, &SpBartmp, chara_Length, kinc, kstep);
  for(int i=0;i<3;i++)
  {
    for(int j=0;j<3;j++){
      ipv->_nldCharacteristicLengthMatrix(i,j) = chara_Length[i][j];                 //by Wu Ling
    }
  }
  ipv->setPosStrMtx(mat->get_pos_mtx_stress());
  ipv->setPosStrInc(mat->get_pos_inc_stress());
}

double mlawNonLocalDamage::soundSpeed() const
{
  double nu = poissonRatio();
  double mu = shearModulus();
  double E = 2.*mu*(1.+nu);
  double factornu = (1.-nu)/((1.+nu)*(1.-2.*nu));
  return sqrt(E*factornu/_rho);
}


void mlawNonLocalDamage::constitutive(const STensor3& F0,const STensor3& Fn,STensor3 &P,const IPNonLocalDamage *ipvprev,
                                      IPNonLocalDamage *ipvcur,STensor43 &Tangent,const bool stiff) const
{

  //convert F to eps  :order xx yy zz xy xz yz
  for(int i=0;i<3;i++)
  {
    ipvcur->_nldStrain(i) = Fn(i,i)-1.;
  }
  //gamma
  ipvcur->_nldStrain(3) = Fn(0,1)+Fn(1,0);
  ipvcur->_nldStrain(4) = Fn(0,2)+Fn(2,0);
  ipvcur->_nldStrain(5) = Fn(2,1)+Fn(1,2);


  //get previous strain and stress
  for(int i=0;i<3;i++){
    strn_n[i]=(ipvprev->_nldStrain)(i);
    strs_n[i]=(ipvprev->_nldStress)(i);
    dstrn[i]=(ipvcur->_nldStrain)(i)-(ipvprev->_nldStrain)(i);
  }
  for(int i=3;i<6;i++){
    strn_n[i]=(ipvprev->_nldStrain)(i)/sq2;
    dstrn[i]=((ipvcur->_nldStrain)(i)-(ipvprev->_nldStrain)(i))/sq2;
    strs_n[i]=(ipvprev->_nldStress)(i)*sq2;
  }

  statev_n = ipvprev->_nldStatev;
  statev_1 = ipvcur->_nldStatev;

  //SHOULD BE MORE GENERAL
  if( mat->get_pos_effectiveplasticstrainfornonlocal() >=0 )
  {
    //should be 66 (-39 for EP)
    statev_n[mat->get_pos_effectiveplasticstrainfornonlocal()] = ipvprev->_nldEffectivePlasticStrain;
    statev_n[mat->get_pos_effectiveplasticstrainfornonlocal()+1] = (ipvcur->_nldEffectivePlasticStrain-
                                                                    ipvprev->_nldEffectivePlasticStrain);
  }

  // to required to use Eshelby tensor of previous step?
  copyvect(statev_n,statev_1,nsdv);
  int kinc = 1;
  int kstep = 2;
  SpBar = 0.0;
  mat->constbox(dstrn, strs_n, strs, statev_n, statev_1, Cref, dCref, tau, dtau, Calgo, 1., dpdE, strs_dDdp_bar, &SpBar, chara_Length, kinc, kstep);

  //update stress and tangent operator
  for(int i=0;i<3;i++)
  {
    (ipvcur->_nldStress)(i)=strs[i];
    for(int j=0;j<3;j++){
      (ipvcur->_nldMaterialTensor)(i,j)=Calgo[i][j];

      (ipvcur->_nldCharacteristicLengthMatrix)(i,j) = chara_Length[i][j];                 //by Wu Ling
    }
    if( mat->get_pos_damagefornonlocal()>0)
    {
      ipvcur->_nldCouplingStressEffectiveStrain(i) = strs_dDdp_bar[i];
      ipvcur->_nldCouplingEffectiveStrainStress(i) = dpdE[i];
    }

  }
  ipvcur->_nldSpBar = SpBar;
  for(int i=3;i<6;i++)
  {
    (ipvcur->_nldStress)(i)=strs[i]/sq2;
    for(int j=0;j<3;j++)
    {
      (ipvcur->_nldMaterialTensor)(i,j)=Calgo[i][j]/sq2;
      (ipvcur->_nldMaterialTensor)(j,i)=Calgo[j][i]/sq2;
    }
    for(int j=3;j<6;j++)
    {
      (ipvcur->_nldMaterialTensor)(i,j)=Calgo[i][j]/2.;
    }
    if( mat->get_pos_damagefornonlocal()>=0)
    {
      ipvcur->_nldCouplingStressEffectiveStrain(i) = strs_dDdp_bar[i]/sq2;
      ipvcur->_nldCouplingEffectiveStrainStress(i) = dpdE[i]/sq2;
    }
  }
  if( mat->get_pos_damagefornonlocal() >= 0)
  {
    ipvcur->_nldDamage = statev_1[mat->get_pos_damagefornonlocal()];
  }
  if( mat->get_pos_currentplasticstrainfornonlocal() >=0)
  {
    ipvcur->_nldCurrentPlasticStrain = statev_1[mat->get_pos_currentplasticstrainfornonlocal()];
  }

  ipvcur->_elasticEne = mat->getElasticEnergy(statev_1);
  ipvcur->_plasticEne = mat->getPlasticEnergy(statev_1);
  //convert str to P  :order xx yy zz xy xz yz
  for(int i=0;i<3;i++)
  {
    P(i,i) = ipvcur->_nldStress(i);
  }
  P(0,1) = ipvcur->_nldStress(3);
  P(1,0) = ipvcur->_nldStress(3);
  P(0,2) = ipvcur->_nldStress(4);
  P(2,0) = ipvcur->_nldStress(4);
  P(1,2) = ipvcur->_nldStress(5);
  P(2,1) = ipvcur->_nldStress(5);

  //convert MaterialTensor to Tangent  :order xx yy zz xy xz yz
 if(stiff){
  Tangent(0,0,0,0) = ipvcur->_nldMaterialTensor(0, 0);
  Tangent(0,0,0,1) = ipvcur->_nldMaterialTensor(0, 3);
  Tangent(0,0,0,2) = ipvcur->_nldMaterialTensor(0, 4);
  Tangent(0,0,1,0) = ipvcur->_nldMaterialTensor(0, 3);
  Tangent(0,0,1,1) = ipvcur->_nldMaterialTensor(0, 1);
  Tangent(0,0,1,2) = ipvcur->_nldMaterialTensor(0, 5);
  Tangent(0,0,2,0) = ipvcur->_nldMaterialTensor(0, 4);
  Tangent(0,0,2,1) = ipvcur->_nldMaterialTensor(0, 5);
  Tangent(0,0,2,2) = ipvcur->_nldMaterialTensor(0, 2);

  Tangent(1,1,0,0) = ipvcur->_nldMaterialTensor(1, 0);
  Tangent(1,1,0,1) = ipvcur->_nldMaterialTensor(1, 3);
  Tangent(1,1,0,2) = ipvcur->_nldMaterialTensor(1, 4);
  Tangent(1,1,1,0) = ipvcur->_nldMaterialTensor(1, 3);
  Tangent(1,1,1,1) = ipvcur->_nldMaterialTensor(1, 1);
  Tangent(1,1,1,2) = ipvcur->_nldMaterialTensor(1, 5);
  Tangent(1,1,2,0) = ipvcur->_nldMaterialTensor(1, 4);
  Tangent(1,1,2,1) = ipvcur->_nldMaterialTensor(1, 5);
  Tangent(1,1,2,2) = ipvcur->_nldMaterialTensor(1, 2);

  Tangent(2,2,0,0) = ipvcur->_nldMaterialTensor(2, 0);
  Tangent(2,2,0,1) = ipvcur->_nldMaterialTensor(2, 3);
  Tangent(2,2,0,2) = ipvcur->_nldMaterialTensor(2, 4);
  Tangent(2,2,1,0) = ipvcur->_nldMaterialTensor(2, 3);
  Tangent(2,2,1,1) = ipvcur->_nldMaterialTensor(2, 1);
  Tangent(2,2,1,2) = ipvcur->_nldMaterialTensor(2, 5);
  Tangent(2,2,2,0) = ipvcur->_nldMaterialTensor(2, 4);
  Tangent(2,2,2,1) = ipvcur->_nldMaterialTensor(2, 5);
  Tangent(2,2,2,2) = ipvcur->_nldMaterialTensor(2, 2);

  Tangent(0,1,0,0) = ipvcur->_nldMaterialTensor(3, 0);
  Tangent(0,1,0,1) = ipvcur->_nldMaterialTensor(3, 3);
  Tangent(0,1,0,2) = ipvcur->_nldMaterialTensor(3, 4);
  Tangent(0,1,1,0) = ipvcur->_nldMaterialTensor(3, 3);
  Tangent(0,1,1,1) = ipvcur->_nldMaterialTensor(3, 1);
  Tangent(0,1,1,2) = ipvcur->_nldMaterialTensor(3, 5);
  Tangent(0,1,2,0) = ipvcur->_nldMaterialTensor(3, 4);
  Tangent(0,1,2,1) = ipvcur->_nldMaterialTensor(3, 5);
  Tangent(0,1,2,2) = ipvcur->_nldMaterialTensor(3, 2);

  Tangent(1,0,0,0) = ipvcur->_nldMaterialTensor(3, 0);
  Tangent(1,0,0,1) = ipvcur->_nldMaterialTensor(3, 3);
  Tangent(1,0,0,2) = ipvcur->_nldMaterialTensor(3, 4);
  Tangent(1,0,1,0) = ipvcur->_nldMaterialTensor(3, 3);
  Tangent(1,0,1,1) = ipvcur->_nldMaterialTensor(3, 1);
  Tangent(1,0,1,2) = ipvcur->_nldMaterialTensor(3, 5);
  Tangent(1,0,2,0) = ipvcur->_nldMaterialTensor(3, 4);
  Tangent(1,0,2,1) = ipvcur->_nldMaterialTensor(3, 5);
  Tangent(1,0,2,2) = ipvcur->_nldMaterialTensor(3, 2);

  Tangent(0,2,0,0) = ipvcur->_nldMaterialTensor(4, 0);
  Tangent(0,2,0,1) = ipvcur->_nldMaterialTensor(4, 3);
  Tangent(0,2,0,2) = ipvcur->_nldMaterialTensor(4, 4);
  Tangent(0,2,1,0) = ipvcur->_nldMaterialTensor(4, 3);
  Tangent(0,2,1,1) = ipvcur->_nldMaterialTensor(4, 1);
  Tangent(0,2,1,2) = ipvcur->_nldMaterialTensor(4, 5);
  Tangent(0,2,2,0) = ipvcur->_nldMaterialTensor(4, 4);
  Tangent(0,2,2,1) = ipvcur->_nldMaterialTensor(4, 5);
  Tangent(0,2,2,2) = ipvcur->_nldMaterialTensor(4, 2);

  Tangent(2,0,0,0) = ipvcur->_nldMaterialTensor(4, 0);
  Tangent(2,0,0,1) = ipvcur->_nldMaterialTensor(4, 3);
  Tangent(2,0,0,2) = ipvcur->_nldMaterialTensor(4, 4);
  Tangent(2,0,1,0) = ipvcur->_nldMaterialTensor(4, 3);
  Tangent(2,0,1,1) = ipvcur->_nldMaterialTensor(4, 1);
  Tangent(2,0,1,2) = ipvcur->_nldMaterialTensor(4, 5);
  Tangent(2,0,2,0) = ipvcur->_nldMaterialTensor(4, 4);
  Tangent(2,0,2,1) = ipvcur->_nldMaterialTensor(4, 5);
  Tangent(2,0,2,2) = ipvcur->_nldMaterialTensor(4, 2);

  Tangent(2,1,0,0) = ipvcur->_nldMaterialTensor(5, 0);
  Tangent(2,1,0,1) = ipvcur->_nldMaterialTensor(5, 3);
  Tangent(2,1,0,2) = ipvcur->_nldMaterialTensor(5, 4);
  Tangent(2,1,1,0) = ipvcur->_nldMaterialTensor(5, 3);
  Tangent(2,1,1,1) = ipvcur->_nldMaterialTensor(5, 1);
  Tangent(2,1,1,2) = ipvcur->_nldMaterialTensor(5, 5);
  Tangent(2,1,2,0) = ipvcur->_nldMaterialTensor(5, 4);
  Tangent(2,1,2,1) = ipvcur->_nldMaterialTensor(5, 5);
  Tangent(2,1,2,2) = ipvcur->_nldMaterialTensor(5, 2);

  Tangent(1,2,0,0) = ipvcur->_nldMaterialTensor(5, 0);
  Tangent(1,2,0,1) = ipvcur->_nldMaterialTensor(5, 3);
  Tangent(1,2,0,2) = ipvcur->_nldMaterialTensor(5, 4);
  Tangent(1,2,1,0) = ipvcur->_nldMaterialTensor(5, 3);
  Tangent(1,2,1,1) = ipvcur->_nldMaterialTensor(5, 1);
  Tangent(1,2,1,2) = ipvcur->_nldMaterialTensor(5, 5);
  Tangent(1,2,2,0) = ipvcur->_nldMaterialTensor(5, 4);
  Tangent(1,2,2,1) = ipvcur->_nldMaterialTensor(5, 5);
  Tangent(1,2,2,2) = ipvcur->_nldMaterialTensor(5, 2);
 }

#if 0
  // TO BE IMPLEMENTED: elasticity to try
  double _E = props[7];
  double _nu = props[8];
  double FACT = _E / (1 + _nu);
  double C11 = FACT * (1 - _nu) / (1 - 2 * _nu);
  double C12 = FACT * _nu / (1 - 2 * _nu);
  double C44 = (C11 - C12);
  fullMatrix<double > *H = &ipvcur->MaterialTensor;
  H->scale(0.);
  // This tensor is for gamma in the strain field (gamma is stored)
  for(int i = 0; i < 3; i++) { H->operator()(i, i) = C11; H->operator()(i + 3, i + 3) = C44; }
  H->operator()(1, 0) = H->operator()(0, 1) = H->operator()(2, 0) = H->operator()(0, 2) = H->operator()(1, 2) = H->operator()(2, 1) = C12;

  ipvcur->stress.scale(0.);
  for (int i=0; i < 6; i++)
  {
    for(int j=0; j < 6; j++)
    {
      ipvcur->stress(i) +=H->operator()(i, j) *ipvcur->strain(j);

    }
  }
  ipvcur->effectivePlasticStrain = 0.;
  ipvcur->currentPlasticStrain = 0.;
  ipvcur->damage = 0.;
#endif



}

void mlawNonLocalDamage::constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPNonLocalDamage *ipvprev,       // array of initial internal variable
                            IPNonLocalDamage *ipvcur,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            STensor3  &dLocalPlasticStrainDStrain,
  			                STensor3  &dStressDNonLocalPlasticStrain,
                            double   &dLocalPlasticStrainDNonLocalPlasticStrain,
                            const bool stiff            // if true compute the tangents
                           ) const
{
  mlawNonLocalDamage::constitutive(F0, Fn, P, ipvprev, ipvcur, Tangent, stiff);
  if(stiff)
  {
    dLocalPlasticStrainDStrain(0,0) = ipvcur->_nldCouplingEffectiveStrainStress(0);
    dLocalPlasticStrainDStrain(1,1) = ipvcur->_nldCouplingEffectiveStrainStress(1);
    dLocalPlasticStrainDStrain(2,2) = ipvcur->_nldCouplingEffectiveStrainStress(2);
    dLocalPlasticStrainDStrain(0,1) = ipvcur->_nldCouplingEffectiveStrainStress(3);
    dLocalPlasticStrainDStrain(1,0) = ipvcur->_nldCouplingEffectiveStrainStress(3);
    dLocalPlasticStrainDStrain(0,2) = ipvcur->_nldCouplingEffectiveStrainStress(4);
    dLocalPlasticStrainDStrain(2,0) = ipvcur->_nldCouplingEffectiveStrainStress(4);
    dLocalPlasticStrainDStrain(2,1) = ipvcur->_nldCouplingEffectiveStrainStress(5);
    dLocalPlasticStrainDStrain(1,2) = ipvcur->_nldCouplingEffectiveStrainStress(5);

    dStressDNonLocalPlasticStrain(0,0) = ipvcur->_nldCouplingStressEffectiveStrain(0);
    dStressDNonLocalPlasticStrain(1,1) = ipvcur->_nldCouplingStressEffectiveStrain(1);
    dStressDNonLocalPlasticStrain(2,2) = ipvcur->_nldCouplingStressEffectiveStrain(2);
    dStressDNonLocalPlasticStrain(0,1) = ipvcur->_nldCouplingStressEffectiveStrain(3);
    dStressDNonLocalPlasticStrain(1,0) = ipvcur->_nldCouplingStressEffectiveStrain(3);
    dStressDNonLocalPlasticStrain(0,2) = ipvcur->_nldCouplingStressEffectiveStrain(4);
    dStressDNonLocalPlasticStrain(2,0) = ipvcur->_nldCouplingStressEffectiveStrain(4);
    dStressDNonLocalPlasticStrain(2,1) = ipvcur->_nldCouplingStressEffectiveStrain(5);
    dStressDNonLocalPlasticStrain(1,2) = ipvcur->_nldCouplingStressEffectiveStrain(5);

    dLocalPlasticStrainDNonLocalPlasticStrain = ipvcur->_nldSpBar;

  }

}

const double mlawNonLocalDamage::bulkModulus() const
{
  return 2.*_mu0*(1+_nu0)/3./(1.-2.*_nu0);
}
const double mlawNonLocalDamage::shearModulus() const
{
  return _mu0;
}

const double mlawNonLocalDamage::poissonRatio() const
{
  return _nu0;
}

MFH::Material* init_material(double *_props,int idmat)
{
	MFH::Material* _mat;
 	int model = (int)_props[idmat];

	//TO DO: use pointer of functions to avoid switch?
	switch(model){

		case EL:
			_mat = new MFH::EL_Material(_props,idmat);
			break;

		case EP:
			_mat = new MFH::EP_Material(_props,idmat);
			break;

		case VT:
			_mat = new MFH::VT_Material(_props,idmat);
			break;

		case MT:
			_mat = new MFH::MT_Material(_props,idmat);
			break;

		case SC:
			_mat = new MFH::SC_Material(_props,idmat);
			break;
		case ANEL:
			_mat = new MFH::ANEL_Material(_props,idmat);
			break;
                case MTSecF:
			_mat = new MFH::MTSecF_Material(_props,idmat);
			break;
		default :
			printf("Unknown material model: %d\n", model);
			_mat=NULL;

	}
    return _mat;
}
//*************add by wu ling *********************************

MFH::Damage* init_damage(double* props, int iddam){

	MFH::Damage* dam;
	int model;
	if(iddam != 0) model = (int)props[iddam];
	else model=0;

	switch(model){

		case 0:
			dam=NULL;
			break;

		case D_LC:
			dam = new MFH::LC_Damage(props,iddam);
			break;

	        case D_LCN:
			dam = new MFH::LCN_Damage(props,iddam);
			break;

	        case D_LINN:
			dam = new MFH::LINN_Damage(props,iddam);
			break;

	        case D_EXPN:
			dam = new MFH::EXPN_Damage(props,iddam);
			break;

	        case D_PLN:
			dam = new MFH::PLN_Damage(props,iddam);
			break;

		default:
			printf("Unknown damage model: %d\n", model);
			dam=NULL;

	}
	return dam;

}

//*************add by wu ling *********************************

MFH::Clength* init_clength(double* props, int idclength){

	MFH::Clength* clength;
	int model;
	if(idclength != 0) model = (int)props[idclength];
	else model=0;

	switch(model){
	
		case 0:
			clength = new MFH::Zero_Cl(props,idclength);
			break;

		case CL_Iso:
			clength = new MFH::Iso_Cl(props,idclength);
			break;

	        case CL_Aniso:
			clength = new MFH::Aniso_Cl(props,idclength);
			break;

	        case CL_Iso_V1:
			clength = new MFH::IsoV1_Cl(props,idclength);
			break;

	        case CL_Aniso_V1:
			clength = new MFH::AnisoV1_Cl(props,idclength);
			break;

		default:
			printf("Unknown Characteristic length model: %d\n", model);
			clength=NULL;			
	}
	return clength;
}
