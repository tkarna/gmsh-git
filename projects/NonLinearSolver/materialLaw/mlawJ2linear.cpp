//
// C++ Interface: material law
//
// Description: j2 linear elasto-plastic law
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "mlawJ2linear.h"
#include <math.h>
#include "MInterfaceElement.h"
double mlawJ2linear::determinantSTensor3(const STensor3 &a) const
{
  return (a(0,0) * (a(1,1) * a(2,2) - a(1,2) * a(2,1)) -
          a(0,1) * (a(1,0) * a(2,2) - a(1,2) * a(2,0)) +
          a(0,2) * (a(1,0) * a(2,1) - a(1,1) * a(2,0)));
}

void mlawJ2linear::inverseSTensor3(const STensor3 &a, STensor3 &ainv) const
{
  double udet = 1./determinantSTensor3(a);
  ainv(0,0) =  (a(1,1) * a(2,2) - a(1,2) * a(2,1))* udet;
  ainv(1,0) = -(a(1,0) * a(2,2) - a(1,2) * a(2,0))* udet;
  ainv(2,0) =  (a(1,0) * a(2,1) - a(1,1) * a(2,0))* udet;
  ainv(0,1) = -(a(0,1) * a(2,2) - a(0,2) * a(2,1))* udet;
  ainv(1,1) =  (a(0,0) * a(2,2) - a(0,2) * a(2,0))* udet;
  ainv(2,1) = -(a(0,0) * a(2,1) - a(0,1) * a(2,0))* udet;
  ainv(0,2) =  (a(0,1) * a(1,2) - a(0,2) * a(1,1))* udet;
  ainv(1,2) = -(a(0,0) * a(1,2) - a(0,2) * a(1,0))* udet;
  ainv(2,2) =  (a(0,0) * a(1,1) - a(0,1) * a(1,0))* udet;
}

void mlawJ2linear::multSTensor3(const STensor3 &a, const STensor3 &b, STensor3 &c) const
{
  for(int i=0;i<3;i++)
    for(int j=0;j<3;j++)
  {
    c(i,j) = a(i,0)*b(0,j)+a(i,1)*b(1,j)+a(i,2)*b(2,j);
  }
}

void mlawJ2linear::multSTensor3FirstTranspose(const STensor3 &A, const STensor3 &B, STensor3 &C) const
{
  for(int i=0;i<3;i++)
    for(int j=0;j<3;j++)
      C(i,j) = A(0,i)*B(0,j) + A(1,i)*B(1,j) + A(2,i)*B(2,j);
}

void mlawJ2linear::multSTensor3SecondTranspose(const STensor3 &a, const STensor3 &b, STensor3 &c) const
{
  for(int i=0;i<3;i++)
    for(int j=0;j<3;j++)
      c(i,j) = a(i,0)*b(j,0)+a(i,1)*b(j,1)+a(i,2)*b(j,2);
}

void mlawJ2linear::multSTensor43STensor3(const STensor43 &t, const STensor3 &m,STensor3 &val) const
{
  val*=0.;
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      for (int k = 0; k < 3; k++)
        for (int l = 0; l < 3; l++)
          val(i,j)+=t(i,j,k,l)*m(k,l);
}

void mlawJ2linear::multSTensor43(const STensor43 &a, const STensor43 &b, STensor43 &c) const
{
  c*=0.;
  for(int i=0;i<3;i++)
    for(int j=0;j<3;j++)
      for(int k=0;k<3;k++)
        for(int l=0;l<3;l++)
          for(int ii=0;ii<3;ii++)
            for(int jj=0;jj<3;jj++)
            {
              c(i,j,k,l) += a(i,j,ii,jj)*b(ii,jj,k,l);
            }
}
void mlawJ2linear::multSTensor43Trans(const STensor43 &a, const STensor43 &b, STensor43 &c) const
{
  c*=0.;
  for(int i=0;i<3;i++)
    for(int j=0;j<3;j++)
      for(int k=0;k<3;k++)
	for(int l=0;l<3;l++)
	  for(int ii=0;ii<3;ii++)
	    for(int jj=0;jj<3;jj++)
	    {
	      c(i,j,k,l) += a(ii,jj,i,j)*b(ii,jj,k,l);
	    }
}

void mlawJ2linear::addtensprod(const STensor3 &a, const STensor3 &b, STensor43 &c) const
{
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      for (int k = 0; k < 3; k++)
        for (int l = 0; l < 3; l++)
          c(i,j,k,l)+=a(i,j)*b(k,l);
}

void mlawJ2linear::logSTensor3(const STensor3& a, STensor3 &loga, STensor43 *dloga) const
{
  /* linear approximation for now */
  static const STensor3 I(-1.);
  static const STensor43 I4(1.,1.);
  loga = I;
  loga+= a;
  if(dloga !=NULL)
  {
    (*dloga) = I4;
  }
}

void mlawJ2linear::expSTensor3(const STensor3 &a,STensor3 &expa,STensor43 *dexpa) const
{
  /*linear approximation for now */
  static const STensor3 I(1.);
  static const STensor43 I4(1.,1.);
  expa = I;
  expa += a;

  if(dexpa!=NULL)
  {
    (*dexpa) = I4;
  }
}

// Keep this constructor for retro compatibility !
mlawJ2linear::mlawJ2linear(const int num,const double E,const double nu,
                           const double rho,const double sy0,const double h,
                           const double tol, const bool pert, const double eps) : materialLaw(num,true), _E(E), _nu(nu), _rho(rho),
                                                _lambda((E*nu)/(1.+nu)/(1.-2.*nu)),
                                               _mu(0.5*E/(1.+nu)),_K(E/3./(1.-2.*nu)), _K3(3.*_K), _mu3(3.*_mu),
                                               _mu2(2.*_mu), _tol(tol),_perturbationfactor(eps),_tangentByPerturbation(pert)
{

  _j2IH = new LinearExponentialJ2IsotropicHardening(num, sy0, h, 0., 10.);
  // Put a warning
  Msg::Warning("Deprecated constructor of mlawJ2linear! This constructor builds a LinearExponentialJ2IsotropicHardening to compute the hardening. You should provide the hardening law to the constructor");

}

mlawJ2linear::mlawJ2linear(const int num,const double E,const double nu, const double rho,const J2IsotropicHardening &j2IH,
                           const double tol, const bool pert, const double eps) : materialLaw(num,true), _E(E), _nu(nu), _rho(rho),
                                               _lambda((E*nu)/(1.+nu)/(1.-2.*nu)),
                                               _mu(0.5*E/(1.+nu)),_K(E/3./(1.-2.*nu)), _K3(3.*_K), _mu3(3.*_mu),
                                               _mu2(2.*_mu), _tol(tol),_perturbationfactor(eps),_tangentByPerturbation(pert)
{
  _j2IH=j2IH.clone();

}
mlawJ2linear::mlawJ2linear(const mlawJ2linear &source) : materialLaw(source),_E(source._E), _nu(source._nu), _rho(source._rho),
                                                         _lambda(source._lambda),
                                                         _mu(source._mu), _K(source._K),_K3(source._K3), _mu3(source._mu3),
                                                         _mu2(source._mu2), _tol(source._tol),
                                                         _perturbationfactor(source._perturbationfactor),
                                                         _tangentByPerturbation(source._tangentByPerturbation)
{
  if(source._j2IH != NULL)
  {
    _j2IH=source._j2IH->clone();
  }
}

mlawJ2linear& mlawJ2linear::operator=(const materialLaw &source)
{
  materialLaw::operator=(source);
  const mlawJ2linear* src =static_cast<const mlawJ2linear*>(&source);
  if(src != NULL)
  {
    _E = src->_E;
    _nu = src->_nu;
    _rho = src->_rho;
    _lambda = src->_lambda;
    _mu = src->_mu;
    _K = src->_K;
    _K3 = src->_K3;
    _mu3 = src->_mu3;
    _mu2 = src->_mu2;
    _tol = src->_tol;
    _perturbationfactor = src->_perturbationfactor;
    _tangentByPerturbation = src->_tangentByPerturbation;
    if(_j2IH != NULL) delete _j2IH;
    if(src->_j2IH != NULL)
    {
      _j2IH=src->_j2IH->clone();
    }
  }
  return *this;
}

void mlawJ2linear::createIPState(IPStateBase* &ips,const bool* state_,const MElement *ele, const int nbFF_, const int gpt) const
{
  //bool inter=true;
  //const MInterfaceElement *iele = static_cast<const MInterfaceElement*>(ele);
  //if(iele==NULL) inter=false;
  IPVariable* ipvi = new IPJ2linear(_j2IH);
  IPVariable* ipv1 = new IPJ2linear(_j2IH);
  IPVariable* ipv2 = new IPJ2linear(_j2IH);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

double mlawJ2linear::soundSpeed() const
{
  double factornu = (1.-_nu)/((1.+_nu)*(1.-2.*_nu));
  return sqrt(_E*factornu/_rho);
}


void mlawJ2linear::constitutive(const STensor3& F0,const STensor3& Fn,STensor3 &P,const IPJ2linear *q0,IPJ2linear *q1,STensor43 &Tangent,
                                const bool stiff) const
{

  constitutive(F0,Fn,P,q0,q1,Tangent,stiff,tmpdFpdF);
}
void mlawJ2linear::constitutive(const STensor3& F0,const STensor3& Fn,STensor3 &P,const IPJ2linear *q0,IPJ2linear *q1,STensor43 &Tangent,
                                const bool stiff, STensor43 &dFpdF) const
{


  /* compute gradient of deformation */
  F1 = Fn;

  /* extract the plastic part of the gradient of deformation */
  Fp1 = q1->_j2lepsp; // != for j2linear q1 = q1j2linear + I
  Fp0 = q0->_j2lepsp; // != for j2linear q1 = q1j2linear + I

  /* update */
  this->update(F1,Fp0,Fp1,q0,q1,P);

  /* tangents */
  if(stiff)
  {
    if (_tangentByPerturbation)
      this->tangent_full_perturbation(Tangent,P,F1,Fp0,Fp1,q0,q1,dFpdF);
    else
      this->tangent_full(Tangent,P,F1,Fp0,Fp1,q0,q1,dFpdF);
  }

  /* put-back plastic Fp in internal array */
  q1->_j2lepsp = Fp1;

  /* save yield stress increment */
  _j2IH->hardening(q1->_j2lepspbarre, q1->getRefToIPJ2IsotropicHardening());
  double Sy   = q1->getConstRefToIPJ2IsotropicHardening().getR();
  double H    = q1->getConstRefToIPJ2IsotropicHardening().getDR();
  double _sy0 = _j2IH->getYield0();
  //this->harden(q1->_j2lepspbarre,Sy,H);
  q1->_j2ldsy = Sy-_sy0;
}

void mlawJ2linear::update(const STensor3& F, const STensor3& Fp0_, STensor3& Fp1_, const IPJ2linear *q0_, IPJ2linear *q1,
                          STensor3&P) const
{

  /* compute elastic predictor */
  Fp1_ = Fp0_; // or already made before the update (it is the case in constitutive function)
  q1->_j2lepspbarre = q0_->_j2lepspbarre;

  inverseSTensor3(Fp1_,Fpinv);
  multSTensor3(F,Fpinv,Fe);
  multSTensor3FirstTranspose(Fe,Fe,Ce);

  /* Compute equivalent stress */
  logSTensor3(Ce,Ee,&L); // BE AWARE L is compute 2 times if plastic correction. This computation can been saved (usefull if not linear approximation ??)
  Ee*=0.5;

  // stress
  this->stress(Sig,Ee);
  this->J2flow(M,Ee);
  double Seq = dot(Sig,M);

  /* Test plasticity */
  _j2IH->hardening(q1->_j2lepspbarre, q1->getRefToIPJ2IsotropicHardening());
  double Sy   = q1->getConstRefToIPJ2IsotropicHardening().getR();
  double H    = q1->getConstRefToIPJ2IsotropicHardening().getDR();

  //double Sy,H;
  //this->harden(q1->_j2lepspbarre,Sy,H);
  double VMcriterion = Seq - Sy;
  double epspbarre0 = q0_->_j2lepspbarre;
  double eps = epspbarre0;
  if(VMcriterion >0.)
  {
    /* Plastic correction */
    #if 0
      printf("plasticity\n");
    #endif
    int ite = 0, maxite = 1000;
    while(fabs(VMcriterion)/_j2IH->getYield0() > _tol)
    {
      if(ite == 1)
        double hlll=2;
      double coef = _mu3+H;
      double deps = VMcriterion/coef;
      if(deps > 1)
        double hlll=3;
      if(eps+deps < epspbarre0 ) deps = -1.*(eps-epspbarre0);
      eps += deps;
      Ee.daxpy(M,-deps);
      this->stress(Sig,Ee);
      Seq = dot(Sig,M);
      _j2IH->hardening(eps, q1->getRefToIPJ2IsotropicHardening());
      Sy   = q1->getConstRefToIPJ2IsotropicHardening().getR();
      H    = q1->getConstRefToIPJ2IsotropicHardening().getDR();
      //this->harden(eps,Sy,H);
      VMcriterion = Seq-Sy;
      ite++;
//     #ifdef _DEBUG
      if(ite > maxite)
      {
        Msg::Fatal("No convergence for plastic correction in j2linear !!");
        break;
      }
//     #endif // _DEBUG
    }

    M*=(eps-epspbarre0);
    expSTensor3(M,dFp);
    multSTensor3(dFp,Fp0_,Fp1_);

    q1->_j2lepspbarre = eps;

    inverseSTensor3(Fp1_,Fpinv);
    multSTensor3(F,Fpinv,Fe);
    multSTensor3FirstTranspose(Fe,Fe,Ce);
    logSTensor3(Ce,Ee,&L);
  }

  multSTensor43STensor3(L,Sig,S);
  q1->_elasticEnergy=deformationEnergy(Ce);

  /* PK2->PK1 */
  multSTensor3(Fe,S,Sig);
  multSTensor3SecondTranspose(Sig,Fpinv,P);
}

void mlawJ2linear::stress(STensor3 &Sig_,const STensor3 &E_) const
{
  double trace = E_.trace();
  double press = _K*trace;
  double val = press-_mu2*trace/3.;

  Sig_ = E_;
  Sig_*= _mu2;
  Sig_(0,0)+=val;
  Sig_(1,1)+=val;
  Sig_(2,2)+=val;
}

void mlawJ2linear::J2flow(STensor3 &M_,const STensor3 &Ee_) const
{
  double tracediv3 = (Ee_.trace())/3.;
  M_ = Ee_;
  M_(0,0) -= tracediv3;
  M_(1,1) -= tracediv3;
  M_(2,2) -= tracediv3;

  double val = M_.dotprod();
  val = sqrt(1.5/val);

  M_*=val;
}

//void mlawJ2linear::harden(const double epspbarre,double &Sy, double &H) const
//{
//  Sy = _sy0 + _hbarre*epspbarre;
//  H = _hbarre;
//}

void mlawJ2linear::tangent_full(STensor43 &T_,const STensor3 &P_,const STensor3 &F_,const STensor3 &Fp0_,const STensor3 &Fp1_,
                            const IPJ2linear* q0_,const IPJ2linear* q1_, STensor43 &dFpdF) const
{
  dFpdF*=0.;
  /* Compute elastic predictor */
  inverseSTensor3(Fp0_,Fp0inv);
  multSTensor3(F_,Fp0inv,Fe0);
  multSTensor3FirstTranspose(Fe0,Fe0,Ce);

  logSTensor3(Ce,Epre,&tmp);
  Epre*=0.5;

  /* Compute current elastic-strain */
  inverseSTensor3(Fp1_,Fpinv);
  multSTensor3(F_,Fpinv,Fe);

  /* Compute small-strain tangent */
  this->tangent(KK,Epre,q0_,q1_,dFpdE);

  /* Small strain -> large deformations in log C */
  Ll *=0.;
  Lr = Ll;
  dFpdF = Ll;
  for(int l=0;l<3;l++)
    for(int k=0;k<3;k++)
      for(int j=0;j<3;j++)
        for(int i=0;i<3;i++)
          for(int m=0;m<3;m++)
            for(int n=0;n<3;n++)
            {
               Ll(i,j,k,l) += tmp(i,j,m,n)*Fe(k,m)*Fpinv(l,n);
               Lr(i,j,k,l) += tmp(i,j,m,n)*Fe0(k,m)*Fp0inv(l,n);
            }
  for(int l=0;l<3;l++)
    for(int k=0;k<3;k++)
      for(int j=0;j<3;j++)
        for(int i=0;i<3;i++)
          for(int m=0;m<3;m++)
            for(int n=0;n<3;n++)
              for(int o=0;o<3;o++)
              {
                dFpdF(i,j,k,l) += dFpdE(i,o,m,n)*Fp0(o,j)*Lr(m,n,k,l);
              }
  multSTensor43Trans(Ll,KK,tmp);
  multSTensor43(tmp,Lr,T_);

  /* Add geometric/kinematic terms */
  inverseSTensor3(F_,Finv);
  multSTensor3(Finv,P_,S);

  for(int l=0; l < 3; l++)
    for(int k=0; k < 3; k++)
      for(int j=0; j < 3; j++)
	  {
        T_(k,j,k,l) += S(l,j);
	    for (int i=0; i < 3; i++)
	    {
          for (int n=0;n<3;n++)
		    for (int o=0;o<3;o++)
		    {
			    T_(i,j,k,l)-=(Fe(i,o)*dFpdF(o,n,k,l)*S(n,j)+P_(i,n)*Fpinv(j,o)*dFpdF(o,n,k,l));
		    }
	    }
	  }
}

void mlawJ2linear::tangent_full_perturbation(STensor43 &T_,const STensor3 &P_,const STensor3 &F_,const STensor3 &Fp0_,const STensor3 &Fp1_,
                            const IPJ2linear* q0_,const IPJ2linear* q1_, STensor43 &dFpdF) const{
    T_*=0;
    dFpdF*=0;
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        STensor3 Fpp(F_);
        IPJ2linear q11(*q1_);
        Fpp(i,j)+=_perturbationfactor;
        STensor3 Pp(0.0), Fpp1(0.);
        this->update(Fpp,Fp0_,Fpp1,q0_,&q11,Pp);
        for (int k=0; k<3; k++){
          for (int l=0; l<3; l++){
            T_(k,l,i,j) = (Pp(k,l)-P_(k,l))/_perturbationfactor;
            dFpdF(k,l,i,j) = (Fpp1(k,l)-Fp1_(k,l))/_perturbationfactor;
          }
        }
      }
    }
}

void mlawJ2linear::tangent(STensor43 &K_,STensor3 &Epre_,const IPJ2linear *q0_,const IPJ2linear *q1_,
                           STensor43 &dFpdE_) const
{
  const double Two3ers = 2./3.;
  const double root1andHalf = sqrt(1.5);

  /* Compute beta (radial return factor) */
  double tracediv3 = Epre_.trace()/3.;
  Epre_(0,0) -= tracediv3;
  Epre_(1,1) -= tracediv3;
  Epre_(2,2) -= tracediv3;

  double Eeq = Epre_.dotprod();
  Eeq = sqrt(Eeq);

  double deps = q1_->_j2lepspbarre - q0_->_j2lepspbarre;

  double beta;
  if(deps > 0.)
  {
    if(Eeq > 1.e-12) beta = - _mu*root1andHalf*deps/Eeq;
    else beta = - _mu;
  }
  else
  {
    beta = 0.;
  }

  /* Fill matrix */
  const double lambdaMinus2div3beta = _lambda - Two3ers*beta;
  const double muPlusbeta = _mu + beta;
  const double twicemuPlusbeta = muPlusbeta +muPlusbeta;
  K_*=0.;
  K_(0,0,0,0) = lambdaMinus2div3beta + twicemuPlusbeta;
  K_(1,1,0,0) = lambdaMinus2div3beta;
  K_(2,2,0,0) = lambdaMinus2div3beta;
  K_(0,0,1,1) = lambdaMinus2div3beta;
  K_(1,1,1,1) = lambdaMinus2div3beta + twicemuPlusbeta;
  K_(2,2,1,1) = lambdaMinus2div3beta;
  K_(0,0,2,2) = lambdaMinus2div3beta;
  K_(1,1,2,2) = lambdaMinus2div3beta;
  K_(2,2,2,2) = lambdaMinus2div3beta + twicemuPlusbeta;

  K_(1,0,1,0) = muPlusbeta;
  K_(2,0,2,0) = muPlusbeta;
  K_(0,1,0,1) = muPlusbeta;
  K_(2,1,2,1) = muPlusbeta;
  K_(0,2,0,2) = muPlusbeta;
  K_(1,2,1,2) = muPlusbeta;

  K_(0,1,1,0) = muPlusbeta;
  K_(0,2,2,0) = muPlusbeta;
  K_(1,0,0,1) = muPlusbeta;
  K_(1,2,2,1) = muPlusbeta;
  K_(2,0,0,2) = muPlusbeta;
  K_(2,1,1,2) = muPlusbeta;

  if(deps > 0.)
  {
    double H    = q1_->getConstRefToIPJ2IsotropicHardening().getDR();
    //double Sy,H;
    //this->harden(q1_->_j2lepspbarre,Sy,H);
    if(Eeq > 1.e-12)
    {
      double EeqEeq = Eeq*Eeq;
      double factor = (root1andHalf*deps/Eeq-_mu3/(_mu3+H));
      double coef = _mu2/EeqEeq*factor;
      coefEpre = Epre_;
      coefEpre*= coef;
      addtensprod(coefEpre,Epre_,K_);

      double coef2 = -1./EeqEeq*factor;
      coefEpre*=(coef2/coef);

      tensprod(coefEpre,Epre_,ddeM);

      double coef3 = 0.5*deps/Eeq*root1andHalf;
      double four3erscoef3 = 2.*Two3ers*coef3;
      ddeM(0,0,0,0) += four3erscoef3;
      ddeM(1,1,1,1) += four3erscoef3;
      ddeM(2,2,2,2) += four3erscoef3;

      ddeM(1,1,0,0) += coef3;
      ddeM(2,2,0,0) += coef3;
      ddeM(0,0,1,1) += coef3;
      ddeM(2,2,1,1) += coef3;
      ddeM(0,0,2,2) += coef3;
      ddeM(1,1,2,2) += coef3;

      ddeM(1,0,1,0) += coef3;
      ddeM(2,0,2,0) += coef3;
      ddeM(0,1,0,1) += coef3;
      ddeM(2,1,2,1) += coef3;
      ddeM(0,2,0,2) += coef3;
      ddeM(1,2,1,2) += coef3;

      ddeM(0,1,1,0) += coef3;
      ddeM(0,2,2,0) += coef3;
      ddeM(1,0,0,1) += coef3;
      ddeM(1,2,2,1) += coef3;
      ddeM(2,0,0,2) += coef3;
      ddeM(2,1,1,2) += coef3;

      this->J2flow(MM,Epre_);
      MM*=deps;
      expSTensor3(MM,Aux,&iL);

      multSTensor43(iL,ddeM,dFpdE_);

    }
    else
    {
      dFpdE_ *= 0.;
    }
  }
  else
  {
    dFpdE_ *= 0.;
  }
}

void mlawJ2linear::constitutive_elastic_stiffness(const STensor3 &F0,const STensor3 &Fn,STensor3 &P, const IPJ2linear *q0, IPJ2linear *q1,
                                               STensor43 &Tangent,STensor43 &ElTangent,bool stiff) const
{
  this->constitutive(F0,Fn,P,q0,q1,Tangent,stiff); // or q1,q1 ??

  /* extract the plastic part of the gradient of deformation */
  eFp1 = q1->_j2lepsp; // != for j2linear q1 = q1j2linear + I

  this->tangent_full(ElTangent,P,Fn,eFp1,eFp1,q1,q1,tmpdFpdF);
}
double mlawJ2linear::deformationEnergy(const STensor3 &C) const
{
  double Jac= sqrt((C(0,0) * (C(1,1) * C(2,2) - C(1,2) * C(2,1)) -
          C(0,1) * (C(1,0) * C(2,2) - C(1,2) * C(2,0)) +
          C(0,2) * (C(1,0) * C(2,1) - C(1,1) * C(2,0))));
  double lnJ = log(Jac);
  STensor3 logCdev;
  logSTensor3(C,logCdev,NULL);
  double trace = logCdev.trace();
  logCdev(0,0)-=trace/3.;
  logCdev(1,1)-=trace/3.;
  logCdev(2,2)-=trace/3.;

  double Psy = _K*0.5*lnJ*lnJ+_mu*0.25*dot(logCdev,logCdev);
  return Psy;;
}
