//
//
//
// Description: Define damage law for non local j2 plasticity
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "DamageLaw.h"
#include <math.h>

DamageLaw::DamageLaw(const int num, const bool init): _num(num), _initialized(init)
{

}

DamageLaw::DamageLaw(const DamageLaw &source)
{
  _num = source._num;
  _initialized = source._initialized;
  
}

DamageLaw& DamageLaw::operator=(const DamageLaw &source)
{
  _num = source._num;
  _initialized = source._initialized;
  return *this;
}

LemaitreChabocheDamageLaw::LemaitreChabocheDamageLaw(const int num, const double _p0, const double _pc,
                                                    const double _n,
                                                    const double _alpha, const bool init) : 
				DamageLaw(num,init), p0(_p0), pc(_pc), n(_n), alpha(_alpha)
{

}
LemaitreChabocheDamageLaw::LemaitreChabocheDamageLaw(const LemaitreChabocheDamageLaw &source) :
					DamageLaw(source)
{
 
  p0     =  source.p0;
  pc     =  source.pc;
  n      =  source.n;
  alpha  =  source.alpha;
}

LemaitreChabocheDamageLaw& LemaitreChabocheDamageLaw::operator=(const DamageLaw &source)
{
  DamageLaw::operator=(source);
  const LemaitreChabocheDamageLaw* src = dynamic_cast<const LemaitreChabocheDamageLaw*>(&source);
  if(src != NULL)
  {
    p0     =  src->p0;
    pc     =  src->pc;
    n      =  src->n;
    alpha  =  src->alpha;

  }
  return *this;
}
void LemaitreChabocheDamageLaw::createIPVariable(IPDamage* &ipcl) const
{
  if(ipcl != NULL) delete ipcl;
  ipcl = new IPLemaitreChabocheDamage();
}

void LemaitreChabocheDamageLaw::computeDamage(double p1, double p0, double phiel, 
                                              const STensor3 &Fe, const STensor3 &Fp, 
                                              const STensor3 &Peff,
                                              const STensor43 &Cel, 
                                              const IPDamage &ipvprev, IPDamage &ipvcur) const
{
  double D0     = ipvprev.getDamage();
  double maxp0  = ipvprev.getMaximalP();
  double DeltaD = 0.;
  double dDdp   = 0.;
  static STensor3 dDdFe;
  dDdFe =0;

  double D=D0;
  double maxp = maxp0;
  if(p1>pc) p1=pc;
  if(p1>p0 and p1>p0)
  {
    DeltaD = pow(phiel/alpha,n)*(p1-p0);
    dDdp   = pow(phiel/alpha,n);
    for(int i=0; i<3; i++)
    {
      for(int j=0; j<3; j++)
      {
        dDdFe(i,j)=0.;
        for(int k=0; k<3; k++)
          dDdFe(i,j)+=Peff(i,k)*Fp(j,k);
       }
     }
     dDdFe *=(pow(phiel/alpha,n-1)*(p1-p0)*n/alpha);
  }
  D = D0+DeltaD;
  if(D>0.99999999999)
  {
     double oldDD=DeltaD;
     DeltaD      = 0.99999999999-D0;
     double rat = DeltaD/oldDD;
     D = D0+DeltaD;
     dDdp   *= rat;
     dDdFe  *= rat;
  }
  if(p1>maxp) maxp=p1;
  ipvcur.setValues(D, maxp, DeltaD, dDdp, dDdFe);
}

DamageLaw * LemaitreChabocheDamageLaw::clone() const
{
  return new LemaitreChabocheDamageLaw(*this);
}

PowerDamageLaw::PowerDamageLaw(const int num, const double _p0, const double _pc,
                                                    const double _alpha,
                                                    const double _beta, const bool init) : 
				DamageLaw(num,init), p0(_p0), pc(_pc), alpha(_alpha), beta(_beta)
{

}
PowerDamageLaw::PowerDamageLaw(const PowerDamageLaw &source) :
					DamageLaw(source)
{
 
  p0     =  source.p0;
  pc     =  source.pc;
  alpha  =  source.alpha;
  beta   =  source.beta;
}

PowerDamageLaw& PowerDamageLaw::operator=(const DamageLaw &source)
{
  DamageLaw::operator=(source);
  const PowerDamageLaw* src = dynamic_cast<const PowerDamageLaw*>(&source);
  if(src != NULL)
  {
    p0     =  src->p0;
    pc     =  src->pc;
    alpha  =  src->alpha;
    beta   =  src->beta;
  }
  return *this;
}
void PowerDamageLaw::createIPVariable(IPDamage* &ipcl) const
{
  if(ipcl != NULL) delete ipcl;
  ipcl = new IPPowerLaw();
}

void PowerDamageLaw::computeDamage(double p1, double p0, double phiel, 
                                              const STensor3 &Fe, 
                                              const STensor3 &Fp, const STensor3 &Peff,
                                              const STensor43 &Cel, 
                                    const IPDamage &ipvprev, IPDamage &ipvcur) const
{

}

DamageLaw * PowerDamageLaw::clone() const
{
  return new PowerDamageLaw(*this);
}

