//
//
//
// Description: Define j2 hardening law
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "j2IsotropicHardening.h"

J2IsotropicHardening::J2IsotropicHardening(const int num, double yield0, const bool init): _num(num), _initialized(init),
											_yield0(yield0)
{
  if(_yield0 < 0) Msg::Error("J2IsotropicHardening: negative yield stress");
}

J2IsotropicHardening::J2IsotropicHardening(const J2IsotropicHardening &source)
{
  _num = source._num;
  _initialized = source._initialized;
  _yield0 = source._yield0;
}

J2IsotropicHardening& J2IsotropicHardening::operator=(const J2IsotropicHardening &source)
{
  _num = source._num;
  _initialized = source._initialized;
  _yield0 = source._yield0;
  return *this;
}

PerfectlyPlasticJ2IsotropicHardening::PerfectlyPlasticJ2IsotropicHardening(const int num, double yield0, const bool init) :
					J2IsotropicHardening(num,yield0,init)
{

}
PerfectlyPlasticJ2IsotropicHardening::PerfectlyPlasticJ2IsotropicHardening(const PerfectlyPlasticJ2IsotropicHardening &source) :
					J2IsotropicHardening(source)
{

}

PerfectlyPlasticJ2IsotropicHardening& PerfectlyPlasticJ2IsotropicHardening::operator=(const J2IsotropicHardening &source)
{
  J2IsotropicHardening::operator=(source);
  const PerfectlyPlasticJ2IsotropicHardening* src = dynamic_cast<const PerfectlyPlasticJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
  }
  return *this;
}
void PerfectlyPlasticJ2IsotropicHardening::createIPVariable(IPJ2IsotropicHardening* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPPerfectlyPlasticJ2IsotropicHardening();
}

void PerfectlyPlasticJ2IsotropicHardening::hardening(double p, IPJ2IsotropicHardening &ipv) const
{
  double tol=1.e-15;
  double dR=0, ddR=0, intR=0;
  double R = getYield0();
  if(p>0)
  {
    R=getYield0()+tol*p;
    dR=tol;
    ddR=0;
    intR=0.5*tol*p*p+getYield0()*p;
  }
  ipv.set(R,dR,ddR,intR);
}
J2IsotropicHardening * PerfectlyPlasticJ2IsotropicHardening::clone() const
{
  return new PerfectlyPlasticJ2IsotropicHardening(*this);
}

PowerLawJ2IsotropicHardening::PowerLawJ2IsotropicHardening(const int num, double yield0, double h, double hexp, const bool init) :
					J2IsotropicHardening(num,yield0,init), _h(h), _hexp(hexp)
{
  if(_h<0 or _hexp<0) Msg::Error("PowerLawJ2IsotropicHardening: negative hardening parameters");
}

PowerLawJ2IsotropicHardening::PowerLawJ2IsotropicHardening(const PowerLawJ2IsotropicHardening &source) :
					J2IsotropicHardening(source)
{
  _h = source._h;
  _hexp = source._hexp;
}

PowerLawJ2IsotropicHardening& PowerLawJ2IsotropicHardening::operator=(const J2IsotropicHardening &source)
{
  J2IsotropicHardening::operator=(source);
  const PowerLawJ2IsotropicHardening* src = dynamic_cast<const PowerLawJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
    _h = src->_h;
    _hexp = src->_hexp;
  }
  return *this;
}
void PowerLawJ2IsotropicHardening::createIPVariable(IPJ2IsotropicHardening* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPPowerLawJ2IsotropicHardening();
}

void PowerLawJ2IsotropicHardening::hardening(double p, IPJ2IsotropicHardening &ipv) const
{
  double tol=1.e-16;
  double dR=0, ddR=0, intR=0;
  double R = getYield0();
  if(p< tol)
  {
    R=getYield0();
    if(_h<1.)
    {
      dR = 1e20;
      ddR = -1e20;
    }
    else
    {
      dR = _h;
      ddR = 0.;
    }
  }
  else
  {
    R=getYield0()+_h*pow(p,_hexp);
    dR = _h*_hexp*pow(p,_hexp)/p;
    ddR = dR*(_hexp-1.)/p;
    intR=getYield0()*p;
    if(fabs(_hexp+1.)!=0)
        intR+=_h*pow(p,_hexp+1.)/(_hexp+1.);
  }
  ipv.set(R,dR,ddR,intR);
}
J2IsotropicHardening * PowerLawJ2IsotropicHardening::clone() const
{
  return new PowerLawJ2IsotropicHardening(*this);
}

ExponentialJ2IsotropicHardening::ExponentialJ2IsotropicHardening(const int num, double yield0, double h, double hexp, const bool init) :
					J2IsotropicHardening(num,yield0,init), _h(h), _hexp(hexp)
{
  if(h<0 or hexp<0) Msg::Error("PowerLawJ2IsotropicHardening: negative hardening parameters");
}
ExponentialJ2IsotropicHardening::ExponentialJ2IsotropicHardening(const ExponentialJ2IsotropicHardening &source) :
					J2IsotropicHardening(source)
{
  _h = source._h;
  _hexp = source._hexp;
}

ExponentialJ2IsotropicHardening& ExponentialJ2IsotropicHardening::operator=(const J2IsotropicHardening &source)
{
  J2IsotropicHardening::operator=(source);
  const ExponentialJ2IsotropicHardening* src = dynamic_cast<const ExponentialJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
    _h = src->_h;
    _hexp = src->_hexp;
  }
  return *this;
}
void ExponentialJ2IsotropicHardening::createIPVariable(IPJ2IsotropicHardening* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPExponentialJ2IsotropicHardening();
}

void ExponentialJ2IsotropicHardening::hardening(double p, IPJ2IsotropicHardening &ipv) const
{
  double tol=1.e-16;
  double dR=0, ddR=0, intR=0;
  double R = getYield0();
  if(p< tol)
  {
    R=getYield0();
    dR = _h*_hexp;
    ddR = -_hexp*(dR);
  }
  else
  {
    double tmp = exp(-_hexp*p);
    R = getYield0()+_h*(1.-tmp);
    dR = _h*_hexp*tmp;
    ddR = -_hexp*(dR);
    intR=getYield0()*p+_h*p;
    if(fabs(_hexp)!=0) intR+=_h*(exp(-_hexp*p)-exp(0))/(_hexp);
  }
  ipv.set(R,dR,ddR,intR);
}
J2IsotropicHardening * ExponentialJ2IsotropicHardening::clone() const
{
  return new ExponentialJ2IsotropicHardening(*this);
}

SwiftJ2IsotropicHardening::SwiftJ2IsotropicHardening(const int num, double yield0, double h, double hexp, const bool init) :
					J2IsotropicHardening(num,_yield0,init), _h(h), _hexp(hexp)
{
  if(_h<0 or _hexp<0) Msg::Error("SwiftJ2IsotropicHardening: negative hardening parameters");
}
SwiftJ2IsotropicHardening::SwiftJ2IsotropicHardening(const SwiftJ2IsotropicHardening &source) :
					J2IsotropicHardening(source)
{
  _h = source._h;
  _hexp = source._hexp;
}

SwiftJ2IsotropicHardening& SwiftJ2IsotropicHardening::operator=(const J2IsotropicHardening &source)
{
  J2IsotropicHardening::operator=(source);
  const SwiftJ2IsotropicHardening* src = dynamic_cast<const SwiftJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
    _h = src->_h;
    _hexp = src->_hexp;
  }
  return *this;
}
void SwiftJ2IsotropicHardening::createIPVariable(IPJ2IsotropicHardening* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPSwiftJ2IsotropicHardening();
}

void SwiftJ2IsotropicHardening::hardening(double p, IPJ2IsotropicHardening &ipv) const
{
  double tol=1.e-16;
  double dR=0, ddR=0, intR=0;
  double R = getYield0();
  if(p< tol)
  {
    R=getYield0();
    dR = getYield0()*_h*_hexp;
    ddR = dR*(_hexp-1.)*_h;
  }
  else
  {
    double tmp = 1.+_h*p;
    R = getYield0()*pow(tmp,_hexp);
    dR = getYield0()*_h*_hexp*pow(tmp,_hexp)/tmp;
    ddR = (dR)*(_hexp-1.)*_h/tmp;
    if(fabs(_hexp+1.)!=0 && fabs(_h)!=0.) intR=getYield0()*pow(1.+_h*p,_hexp+1.)/(_hexp+1.)/_h;
  }
  ipv.set(R,dR,ddR,intR);
}
J2IsotropicHardening * SwiftJ2IsotropicHardening::clone() const
{
  return new SwiftJ2IsotropicHardening(*this);
}

LinearExponentialJ2IsotropicHardening::LinearExponentialJ2IsotropicHardening(const int num, double yield0, double h1,
					double h2, double hexp, const bool init) :
					J2IsotropicHardening(num,yield0,init), _h1(h1), _h2(h2), _hexp(hexp)
{
  if(h1<0 or h2<0 or hexp<0) Msg::Error("SwiftJ2IsotropicHardening: negative hardening parameters");
}
LinearExponentialJ2IsotropicHardening::LinearExponentialJ2IsotropicHardening(const LinearExponentialJ2IsotropicHardening &source) :
					J2IsotropicHardening(source)
{
  _h1 = source._h1;
  _h2 = source._h2;
  _hexp = source._hexp;
}

LinearExponentialJ2IsotropicHardening& LinearExponentialJ2IsotropicHardening::operator=(const J2IsotropicHardening &source)
{
  J2IsotropicHardening::operator=(source);
  const LinearExponentialJ2IsotropicHardening* src = dynamic_cast<const LinearExponentialJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
    _h1 = src->_h1;
    _h2 = src->_h2;
    _hexp = src->_hexp;
  }
  return *this;
}
void LinearExponentialJ2IsotropicHardening::createIPVariable(IPJ2IsotropicHardening* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPLinearExponentialJ2IsotropicHardening();
}

void LinearExponentialJ2IsotropicHardening::hardening(double p, IPJ2IsotropicHardening &ipv) const
{
  double tol=1.e-16;
  double dR=0, ddR=0, intR=0;
  double R = getYield0();
  if(p< tol)
  {
    R=getYield0();
    dR = _h1 + _h2*_hexp;
    ddR = -_h2*_hexp*_hexp;
  }
  else
  {
    double tmp = exp(-_hexp*p);
    R = getYield0()+_h1*p + _h2*(1.-tmp);
    dR = _h1 + _h2*_hexp*tmp;
    ddR = - _h2*_hexp*_hexp*tmp;
    intR=getYield0()*p+ _h1*p*p/2.+_h2*p;
    if(fabs(_hexp)!=0) intR+=_h2*(exp(-_hexp*p)-exp(0))/(_hexp);
  }
  ipv.set(R,dR,ddR,intR);
}

J2IsotropicHardening * LinearExponentialJ2IsotropicHardening::clone() const
{
  return new LinearExponentialJ2IsotropicHardening(*this);
}


