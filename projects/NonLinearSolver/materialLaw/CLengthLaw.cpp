//
//
//
// Description: Define clength for non local j2 plasticity
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "CLengthLaw.h"
#include <math.h>

CLengthLaw::CLengthLaw(const int num, const bool init): _num(num), _initialized(init)
{

}

CLengthLaw::CLengthLaw(const CLengthLaw &source)
{
  _num = source._num;
  _initialized = source._initialized;
  
}

CLengthLaw& CLengthLaw::operator=(const CLengthLaw &source)
{
  _num = source._num;
  _initialized = source._initialized;
  return *this;
}

ZeroCLengthLaw::ZeroCLengthLaw(const int num, const bool init) : CLengthLaw(num,init)
{

}
ZeroCLengthLaw::ZeroCLengthLaw(const ZeroCLengthLaw &source) :
					CLengthLaw(source)
{

}

ZeroCLengthLaw& ZeroCLengthLaw::operator=(const CLengthLaw &source)
{
  CLengthLaw::operator=(source);
  const ZeroCLengthLaw* src = dynamic_cast<const ZeroCLengthLaw*>(&source);
  if(src != NULL)
  {
  }
  return *this;
}
void ZeroCLengthLaw::createIPVariable(IPCLength* &ipcl) const
{
  double tol=1.e-10;
  if(ipcl != NULL) delete ipcl;
  ipcl = new IPZeroCLength();
  STensor3 &cl = ipcl->getRefToCL();
  cl=0;
  for(int i=0; i<3; i++) cl(i,i)=tol;
}

void ZeroCLengthLaw::computeCL(double p, IPCLength &ipcl) const
{
  double tol=1.e-10;
  STensor3 &cl = ipcl.getRefToCL();
  cl=0;
  for(int i=0; i<3; i++) cl(i,i)=tol;
}
CLengthLaw * ZeroCLengthLaw::clone() const
{
  return new ZeroCLengthLaw(*this);
}

IsotropicCLengthLaw::IsotropicCLengthLaw(const int num, const double _cl, const bool init) : CLengthLaw(num,init),
											cl(_cl)
{
 double tol=1.e-10;
 if(cl < tol) cl=tol;
}
IsotropicCLengthLaw::IsotropicCLengthLaw(const IsotropicCLengthLaw &source) :
					CLengthLaw(source), cl(source.cl)
{
}

IsotropicCLengthLaw& IsotropicCLengthLaw::operator=(const CLengthLaw &source)
{
  CLengthLaw::operator=(source);
  const IsotropicCLengthLaw* src = dynamic_cast<const IsotropicCLengthLaw*>(&source);
  if(src != NULL)
  {
    cl = src->cl;
  }
  return *this;
}
void IsotropicCLengthLaw::createIPVariable(IPCLength* &ipcl) const
{
 
  if(ipcl != NULL) delete ipcl;
  ipcl = new IPIsotropicCLength();
  STensor3 &clm = ipcl->getRefToCL();
  clm=0;
  computeCL(0.,*ipcl);

}

void IsotropicCLengthLaw::computeCL(double p, IPCLength &ipcl) const
{
  STensor3 &clm = ipcl.getRefToCL();
  clm=0;
  for(int i=0; i<3; i++) clm(i,i)=cl;
}
CLengthLaw * IsotropicCLengthLaw::clone() const
{
  return new IsotropicCLengthLaw(*this);
}

AnisotropicCLengthLaw::AnisotropicCLengthLaw(const int num, const double _cl1, const double _cl2, const double _cl3, 
					const double _euler1, const double _euler2, const double _euler3, 
					const bool init) : CLengthLaw(num,init), cl(_cl1,_cl2,_cl3), euler(_euler1,_euler2,_euler3)
{
 double tol=1.e-10;
 if(cl(0) < tol) cl(0)=tol;
 if(cl(1) < tol) cl(1)=tol;
 if(cl(2) < tol) cl(2)=tol;
}


AnisotropicCLengthLaw::AnisotropicCLengthLaw(const AnisotropicCLengthLaw &source) :
					CLengthLaw(source), cl(source.cl), euler(source.euler)
{
}

AnisotropicCLengthLaw& AnisotropicCLengthLaw::operator=(const CLengthLaw &source)
{
  CLengthLaw::operator=(source);
  const AnisotropicCLengthLaw* src = dynamic_cast<const AnisotropicCLengthLaw*>(&source);
  if(src != NULL)
  {
    cl    = src->cl;
    euler = src->euler;
  }
  return *this;
}
void AnisotropicCLengthLaw::createIPVariable(IPCLength* &ipcl) const
{
 
  if(ipcl != NULL) delete ipcl;
  ipcl = new IPAnisotropicCLength();
  STensor3 &clm = ipcl->getRefToCL();
  clm=0;

  computeCL(0.,*ipcl);
}

void AnisotropicCLengthLaw::computeCL(double p, IPCLength &ipcl) const
{
  STensor3 &clm = ipcl.getRefToCL();
  clm=0;
  static double fpi = M_PI/180.;
  static double sq2 = sqrt(2.);
  static double c1 = cos(euler(0)*fpi);
  static double s1 = sin(euler(0)*fpi);
  static double c2 = cos(euler(1)*fpi);
  static double s2 = sin(euler(1)*fpi);
  static double c3 = cos(euler(2)*fpi);
  static double s3 = sin(euler(2)*fpi);
  static double s1c2 = s1*c2;
  static double c1c2 = c1*c2;

 
  static STensor3 T(0);
  T(0,0) = cl(0);
  T(1,1) = cl(1);
  T(2,2) = cl(2);
  static STensor3 PT(0);
  PT(0,0) = (c3*c1 - s1c2*s3);
  PT(0,1) = (c3*s1 + c1c2*s3);
  PT(0,2) = (s2*s3);
  PT(1,0) = (-s3*c1 - s1c2*c3);
  PT(1,1) = (-s3*s1 + c1c2*c3);
  PT(1,2) = (s2*c3);
  PT(2,0) = (s1*s2);
  PT(2,1) = (-c1*s2);
  PT(2,2) = (c2);

  clm = PT.transpose();
  clm*=T;
  clm*=PT;
}

CLengthLaw * AnisotropicCLengthLaw::clone() const
{
  return new AnisotropicCLengthLaw(*this);
}

VariableIsotropicCLengthLaw::VariableIsotropicCLengthLaw(const int num, const double _cl, 
                                                         const double _el, const double _nl, const bool init) :
							 CLengthLaw(num,init), cl(_cl), el(_el), nl(_nl)
{
 double tol=1.e-10;
 if(cl < tol) cl=tol;
}
VariableIsotropicCLengthLaw::VariableIsotropicCLengthLaw(const VariableIsotropicCLengthLaw &source) :
					CLengthLaw(source), cl(source.cl), el(source.el), nl(source.nl)
{
}

VariableIsotropicCLengthLaw& VariableIsotropicCLengthLaw::operator=(const CLengthLaw &source)
{
  CLengthLaw::operator=(source);
  const VariableIsotropicCLengthLaw* src = dynamic_cast<const VariableIsotropicCLengthLaw*>(&source);
  if(src != NULL)
  {
    cl = src->cl;
    el = src->el;
    nl = src->nl;
  }
  return *this;
}
void VariableIsotropicCLengthLaw::createIPVariable(IPCLength* &ipcl) const
{
 
  if(ipcl != NULL) delete ipcl;
  ipcl = new IPVariableIsotropicCLength();
  STensor3 &clm = ipcl->getRefToCL();
  clm=0;
  computeCL(0.,*ipcl);

}

void VariableIsotropicCLengthLaw::computeCL(double p, IPCLength &ipcl) const
{
  double lcurrent=0.;
  STensor3 &clm = ipcl.getRefToCL();
  clm=0;

   if (p <= el){
     lcurrent=cl*pow(p/el, nl);
   }
   else
   {
     lcurrent=cl;
   }

   for(int i=0;i<3;i++)
   {
     clm(i,i) = lcurrent;
     if(clm(i,i) <1.e-10) clm(i,i) = 1.e-10;
   }

}
CLengthLaw * VariableIsotropicCLengthLaw::clone() const
{
  return new VariableIsotropicCLengthLaw(*this);
}

VariableAnisotropicCLengthLaw::VariableAnisotropicCLengthLaw(const int num, 
					const double _cl1, const double _cl2, const double _cl3, 
					const double _euler1, const double _euler2, const double _euler3, 
					const double _el, const double _nl,
					const bool init) : CLengthLaw(num,init), 
					cl(_cl1,_cl2,_cl3), euler(_euler1,_euler2,_euler3), el(_el), nl(_nl)
{
 double tol=1.e-10;
 if(cl(0) < tol) cl(0)=tol;
 if(cl(1) < tol) cl(1)=tol;
 if(cl(2) < tol) cl(2)=tol;
}


VariableAnisotropicCLengthLaw::VariableAnisotropicCLengthLaw(const VariableAnisotropicCLengthLaw &source) :
					CLengthLaw(source), cl(source.cl), euler(source.euler), 
					el(source.el), nl(source.nl)
{
}

VariableAnisotropicCLengthLaw& VariableAnisotropicCLengthLaw::operator=(const CLengthLaw &source)
{
  CLengthLaw::operator=(source);
  const VariableAnisotropicCLengthLaw* src = dynamic_cast<const VariableAnisotropicCLengthLaw*>(&source);
  if(src != NULL)
  {
    cl    = src->cl;
    euler = src->euler;
    el    = src->el;
    nl    = src->nl;
  }
  return *this;
}
void VariableAnisotropicCLengthLaw::createIPVariable(IPCLength* &ipcl) const
{
 
  if(ipcl != NULL) delete ipcl;
  ipcl = new IPVariableAnisotropicCLength();
  STensor3 &clm = ipcl->getRefToCL();
  clm=0;

  computeCL(0.,*ipcl);
}

void VariableAnisotropicCLengthLaw::computeCL(double p, IPCLength &ipcl) const
{
  double lcurrent=0.;
  STensor3 &clm = ipcl.getRefToCL();
  clm=0;
  static double fpi = M_PI/180.;
  static double sq2 = sqrt(2.);
  static double c1 = cos(euler(0)*fpi);
  static double s1 = sin(euler(0)*fpi);
  static double c2 = cos(euler(1)*fpi);
  static double s2 = sin(euler(1)*fpi);
  static double c3 = cos(euler(2)*fpi);
  static double s3 = sin(euler(2)*fpi);
  static double s1c2 = s1*c2;
  static double c1c2 = c1*c2;

 
  static STensor3 T(0);
  static STensor3 PT(0);
  PT(0,0) = (c3*c1 - s1c2*s3);
  PT(0,1) = (c3*s1 + c1c2*s3);
  PT(0,2) = (s2*s3);
  PT(1,0) = (-s3*c1 - s1c2*c3);
  PT(1,1) = (-s3*s1 + c1c2*c3);
  PT(1,2) = (s2*c3);
  PT(2,0) = (s1*s2);
  PT(2,1) = (-c1*s2);
  PT(2,2) = (c2);

  T=0;
  if (p <= el){
    for(int i=0;i<3;i++)
    {
      T(i,i)= cl(i)*pow(p/el, nl);
      if(T(i,i) <1.e-10) T(i,i) = 1.e-10;
    } 
  }
  else
  {
    for(int i=0;i<3;i++)
    {
      T(i,i)= cl(i);
      if(T(i,i) <1.e-10) T(i,i) = 1.e-10;
    }
  }
  clm = PT.transpose();
  clm*=T;
  clm*=PT;
}

CLengthLaw * VariableAnisotropicCLengthLaw::clone() const
{
  return new VariableAnisotropicCLengthLaw(*this);
}

