//
// C++ Interface: material law
//
// Description: j2 elasto-plastic law with electrophysiology interface
//
//
// Author:  <A. Jerusalem>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "mlawElectroPhysioJ2Hyper.h"
#include <math.h>
#include "MInterfaceElement.h"

mlawElectroPhysioJ2Hyper::mlawElectroPhysioJ2Hyper(const int num,const double E,const double nu,
                           const double rho, const J2IsotropicHardening &_j2IH, const ElectroPhysio &_epLaw, 
			   const double tol, const bool pert, const double eps) 
				: mlawJ2linear(num,E,nu,rho,_j2IH,tol,pert,eps)
{
  epLaw = _epLaw.clone();
    Cel=0.;
    Cel(0,0,0,0) = _lambda + _mu2;
    Cel(1,1,0,0) = _lambda;
    Cel(2,2,0,0) = _lambda;
    Cel(0,0,1,1) = _lambda;
    Cel(1,1,1,1) = _lambda + _mu2;
    Cel(2,2,1,1) = _lambda;
    Cel(0,0,2,2) = _lambda;
    Cel(1,1,2,2) = _lambda;
    Cel(2,2,2,2) = _lambda + _mu2;

    Cel(1,0,1,0) = _mu;
    Cel(2,0,2,0) = _mu;
    Cel(0,1,0,1) = _mu;
    Cel(2,1,2,1) = _mu;
    Cel(0,2,0,2) = _mu;
    Cel(1,2,1,2) = _mu;

    Cel(0,1,1,0) = _mu;
    Cel(0,2,2,0) = _mu;
    Cel(1,0,0,1) = _mu;
    Cel(1,2,2,1) = _mu;
    Cel(2,0,0,2) = _mu;
    Cel(2,1,1,2) = _mu;

}
mlawElectroPhysioJ2Hyper::mlawElectroPhysioJ2Hyper(const mlawElectroPhysioJ2Hyper &source) : mlawJ2linear(source)
{
  if(source.epLaw != NULL)
  {
    epLaw=source.epLaw->clone();
  }
}

mlawElectroPhysioJ2Hyper& mlawElectroPhysioJ2Hyper::operator = (const materialLaw &source)
{
  mlawJ2linear::operator=(source);
  const mlawElectroPhysioJ2Hyper* src = dynamic_cast<const mlawElectroPhysioJ2Hyper*>(&source);
  if(src != NULL)
  {
    if(epLaw != NULL) delete epLaw;
    if(src->epLaw != NULL)
    {
      epLaw=src->epLaw->clone();
    }
  }
  return *this;
}

void mlawElectroPhysioJ2Hyper::createIPState(IPStateBase* &ips,const bool* state_,const MElement *ele, const int nbFF_, const int gpt) const
{
  //bool inter=true;
  //const MInterfaceElement *iele = static_cast<const MInterfaceElement*>(ele);
  //if(iele==NULL) inter=false;
  IPVariable* ipvi = new IPElectroPhysioJ2Hyper(_j2IH,epLaw);
  IPVariable* ipv1 = new IPElectroPhysioJ2Hyper(_j2IH,epLaw);
  IPVariable* ipv2 = new IPElectroPhysioJ2Hyper(_j2IH,epLaw);
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}
void mlawElectroPhysioJ2Hyper::createIPState(IPElectroPhysioJ2Hyper *ivi, IPElectroPhysioJ2Hyper *iv1, IPElectroPhysioJ2Hyper *iv2) const
{

}
void mlawElectroPhysioJ2Hyper::createIPVariable(IPElectroPhysioJ2Hyper *&ipv,const MElement *ele,const int nbFF) const
{

}

double mlawElectroPhysioJ2Hyper::soundSpeed() const
{
  return mlawJ2linear::soundSpeed();
}


void mlawElectroPhysioJ2Hyper::constitutive(const STensor3& F0,const STensor3& Fn,STensor3 &P,const IPElectroPhysioJ2Hyper *q0,
					    IPElectroPhysioJ2Hyper *q1,STensor43 &Tangent, const bool stiff) const
{
  mlawJ2linear::constitutive(F0,Fn,P,q0,q1,Tangent,stiff);
}

void mlawElectroPhysioJ2Hyper::constitutive(
                            const STensor3& F0,                         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,                         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                                        // contains the initial values on input
                            const IPElectroPhysioJ2Hyper *ipvprev,      // array of initial internal variable
                            IPElectroPhysioJ2Hyper *ipvcur,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,                         // constitutive tangents (output)
			    STensor3  &TangentVV,
			    double    &TangentVu,
                            const bool stiff                            // if true compute the tangents
                           ) const
{

  // Tangent_uu (a.k.a. Tangent)
  dFpdF*=0.;
  mlawJ2linear::constitutive(F0,Fn,P,ipvprev,ipvcur,Tangent,stiff,dFpdF);

  if(stiff)
  {
   
    // Tangent_VV
    TangentVV = ipvcur->getConstRefToTangentVV();
 
    // Tangent_Vu 
    TangentVu = ipvcur->getConstRefToTangentVu();

  }

}

