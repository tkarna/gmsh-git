//
// C++ Interface: terms
//
// Description: Define material law
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef _MLAW_H_
#define _MLAW_H_
#ifndef SWIG
#include "ipstate.h"
#include "MElement.h"
#endif
class materialLaw{
 public :
  enum matname{linearElasticPlaneStress,linearElasticPlaneStressWithDamage, linearElasticOrthotropicPlaneStress,
               cohesiveLaw, fracture, nonLinearShell, j2linear, transverseIsotropic, Anisotropic, nonLocalDamage, vumat,
               FSElastic, FSElastoPlastic, numeric, secondOrderElastic, j2smallstrain,nonLocalDamageGurson,nonLocalDamageJ2Hyper,LinearThermoMechanics};
 protected :
  int _num; // number of law (must be unique !)
  bool _initialized; // to initialize law
  double _timeStep; // for law which works on increment. (Use only in so no getTimeStep function)
  double _currentTime; // To save results vs time
#ifndef SWIG
 public:
  // constructor
  materialLaw(const int num, const bool init=true);
  ~materialLaw(){}
  materialLaw(const materialLaw &source);
  materialLaw& operator=(const materialLaw &source);
  virtual int getNum() const{return _num;}
  virtual void setTime(const double ctime,const double dtime){_timeStep = dtime; _currentTime = ctime;}
  virtual matname getType() const=0;
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const=0;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw)=0;
  virtual const bool isInitialized() {return _initialized;}
  // for explicit scheme it must return sqrt(E/rho) adapted to your case
  virtual double soundSpeed() const=0;
  virtual double density() const{return 0.;}
  // for multiscale analysis
  virtual double scaleFactor() const { return 1.;};
  virtual bool isHighOrder() const { return false;};
  virtual bool isNumeric() const {return false;};
#endif
};
class materialLaw2LawsInitializer{
#ifndef SWIG
 public:
  materialLaw2LawsInitializer(){}
  ~materialLaw2LawsInitializer(){}
  materialLaw2LawsInitializer(const materialLaw2LawsInitializer &source){}
  virtual void initialBroken(IPStateBase *ips) const=0;
  virtual const int bulkLawNumber() const=0;
  virtual const int fractureLawNumber() const=0;
  virtual bool fullBroken(const IPVariable *ipv) const=0; // has to return true if the fracture process is completed
                                                    // for a given state so it is a ipvariable and not a IPState
                                                    // used for crack tracking algorithms
  virtual double timeInitBroken(const IPVariable *ipv)const{return 0.;} // To known the time when crack has initiate at the Gauss point
#endif // SWIG
};
// If you used two laws your class has to be derived from this one
template<class Tbulk, class Tfrac>class fractureBy2Laws : public materialLaw2LawsInitializer{
#ifndef SWIG
 protected:
  const int _nbulk;
  const int _nfrac;
  Tbulk *_mbulk;
  Tfrac *_mfrac;
 public:
  fractureBy2Laws(const int num, const int nbulk, const int nfrac) : materialLaw2LawsInitializer(),
                                                                       _nbulk(nbulk), _nfrac(nfrac),
                                                                       _mbulk(NULL), _mfrac(NULL){}
  fractureBy2Laws(const fractureBy2Laws &source) : materialLaw2LawsInitializer(source), _nbulk(source._nbulk),
                                                    _nfrac(source._nfrac),
                                                    _mbulk(source._mbulk), _mfrac(source._mfrac){}
  ~fractureBy2Laws(){};
  virtual const int bulkLawNumber() const{return _nbulk;}
  virtual const int fractureLawNumber() const{return _nfrac;}
  virtual const Tbulk* getBulkLaw() const{return _mbulk;}
  virtual Tbulk* getBulkLaw(){return _mbulk;}
  virtual const Tfrac* getFractureLaw() const{return _mfrac;}
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw)
  {
    bool findb=false;
    bool findc=false;
    for(std::map<int,materialLaw*>::const_iterator it = maplaw.begin(); it != maplaw.end(); ++it)
    {
      int num = it->first;
      if(num == _nbulk){
        findb=true;
        it->second->initLaws(maplaw);
        _mbulk = static_cast<Tbulk*>(it->second);
      }
      if(num == _nfrac){
        findc=true;
        it->second->initLaws(maplaw);
        _mfrac = static_cast<Tfrac*>(it->second);
      }
    }
    if(!findb) Msg::Error("The bulk law is not initialize for fracture2Law number %d",_nbulk);
    if(!findc and _nfrac!=-10000000) Msg::Error("The cohesive law is not initialize for fractureBy2Law number %d",_nfrac); // for old damage law change this
  }
  virtual void initialBroken(IPStateBase *ips) const=0;
  virtual bool fullBroken(const IPVariable *ipv)const=0;
#endif // SWIG
};
#endif //MLAW_H_
