cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     TWO SUBROUTINES ARE IN THIS FILE:
c              1-RIGHT BELOW IS THE ONE THAT YOU HAVE TO WRITE;
c                IT SHOULD OUTPUT THE SOUND VELOCITY AND
c                IMPLIES THAT YOU USE THE PROPS ARRAY TO EXTRACT
c                THE (ELASTIC) TANGENT MODULI TO EVALUATE IT
c              2-UNDERNEATH IS THE VUMAT YOU NEED TO ADD; NOTE
c                THE MODIFICATIONS FOR THE "INCLUDE"
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      subroutine vumat_soundspeed(
     +  nblock, nprops, props, density, soundSpeed)

      implicit real*8(a-h,o-z)

      dimension props(nprops), density(nblock), soundSpeed(nblock)

c        user coding to define soundSpeed

      return
      end

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     BELOW IS THE VUMAT
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      subroutine vumat(
C Read only (unmodifiable)variables -
     +  nblock, ndir, nshr, nstatev, nfieldv, nprops, lanneal,
     +  stepTime, totalTime, dt, cmname, coordMp, charLength,
     +  props, density, strainInc, relSpinInc,
     +  tempOld, stretchOld, defgradOld, fieldOld,
     +  stressOld, stateOld, enerInternOld, enerInelasOld,
     +  tempNew, stretchNew, defgradNew, fieldNew,
C Write only (modifiable) variables -
     +  stressNew, stateNew, enerInternNew, enerInelasNew )
C
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c      include 'vaba_param.inc'
c
c      YOU NEED TO COMMENT OUT THE ABOVE LINE AND ADD THE LINES UNDERNEATH
      implicit real*8(a-h,o-z)
      parameter (nprecd=2)
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

C
      dimension props(nprops), density(nblock), coordMp(nblock,*),
     +  charLength(nblock), strainInc(nblock,ndir+nshr),
     +  relSpinInc(nblock,nshr), tempOld(nblock),
     +  stretchOld(nblock,ndir+nshr),
     +  defgradOld(nblock,ndir+nshr+nshr),
     +  fieldOld(nblock,nfieldv), stressOld(nblock,ndir+nshr),
     +  stateOld(nblock,nstatev), enerInternOld(nblock),
     +  enerInelasOld(nblock), tempNew(nblock),
     +  stretchNew(nblock,ndir+nshr),
     +  defgradNew(nblock,ndir+nshr+nshr),
     +  fieldNew(nblock,nfieldv),
     +  stressNew(nblock,ndir+nshr), stateNew(nblock,nstatev),
     +  enerInternNew(nblock), enerInelasNew(nblock)
C
      character*80 cmname
C

      do 100 km = 1,nblock
c        user coding
  100 continue

      return
      end
