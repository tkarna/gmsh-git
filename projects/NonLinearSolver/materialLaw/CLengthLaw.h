//
// C++ Interface: terms
//
// Description: Define clength for non local j2 plasticity
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef _CLENGTHLAW_H_
#define _CLENGTHLAW_H_
#ifndef SWIG
#include "ipstate.h"
#include "MElement.h"
#include "ipCLength.h"
#endif
class CLengthLaw{
 public :
  enum clengthlawname{zeroclengthlaw,isotropicclengthlaw,anisotropicclengthlaw,     
                      variableisotropicclengthlaw, variableanisotropicclengthlaw};
 protected :
  int _num; // number of law (must be unique !)
  bool _initialized; // to initialize law
 public:
  // constructor
#ifndef SWIG
  CLengthLaw(const int num, const bool init=true);
  ~CLengthLaw(){}
  CLengthLaw(const CLengthLaw &source);
  CLengthLaw& operator=(const CLengthLaw &source);
  virtual int getNum() const{return _num;}
  virtual clengthlawname getType() const=0;
  virtual void createIPVariable(IPCLength* &ipv) const=0;
  virtual void initLaws(const std::map<int,CLengthLaw*> &maplaw)=0;
  virtual const bool isInitialized() {return _initialized;}
  virtual void computeCL(double p, IPCLength &ipv) const=0;
  virtual CLengthLaw * clone() const=0;
#endif
};

class ZeroCLengthLaw : public CLengthLaw
{
 public :
 protected :
  public:
  // constructor
  ZeroCLengthLaw(const int num, const bool init=true);
  ~ZeroCLengthLaw(){}
#ifndef SWIG
  ZeroCLengthLaw(const ZeroCLengthLaw &source);
  ZeroCLengthLaw& operator=(const CLengthLaw &source);
  virtual clengthlawname getType() const {return CLengthLaw::zeroclengthlaw;};
  virtual void createIPVariable(IPCLength* &ipv) const;
  virtual void initLaws(const std::map<int,CLengthLaw*> &maplaw){}; //nothing now, we will see if we use the mapping;
  virtual void computeCL(double p, IPCLength &ipcl) const;
  virtual CLengthLaw * clone() const;
#endif
};

class IsotropicCLengthLaw : public CLengthLaw
{
 public :
 protected :
  double cl; 
 public:
  // constructor
  IsotropicCLengthLaw(const int num, const double _cl, const bool init=true);
  ~IsotropicCLengthLaw(){}
#ifndef SWIG
  IsotropicCLengthLaw(const IsotropicCLengthLaw &source);
  IsotropicCLengthLaw& operator=(const CLengthLaw &source);
  virtual clengthlawname getType() const {return CLengthLaw::isotropicclengthlaw;};
  virtual void createIPVariable(IPCLength* &ipv) const;
  virtual void initLaws(const std::map<int,CLengthLaw*> &maplaw){}; //nothing now, we will see if we use the mapping;
  virtual void computeCL(double p, IPCLength &ipcl) const;
  virtual CLengthLaw * clone() const;
#endif
};

class AnisotropicCLengthLaw : public CLengthLaw
{
 public :
 protected :
  SVector3 cl;
  SVector3 euler; 
 public:
  // constructor
  AnisotropicCLengthLaw(const int num, const double _cl1, const double _cl2, const double _cl3, 
                                       const double _euler1, const double _euler2, const double _euler3,
                                       const bool init=true);
  ~AnisotropicCLengthLaw(){}
#ifndef SWIG
  AnisotropicCLengthLaw(const AnisotropicCLengthLaw &source);
  AnisotropicCLengthLaw& operator=(const CLengthLaw &source);
  virtual clengthlawname getType() const {return CLengthLaw::anisotropicclengthlaw;};
  virtual void createIPVariable(IPCLength* &ipv) const;
  virtual void initLaws(const std::map<int,CLengthLaw*> &maplaw){}; //nothing now, we will see if we use the mapping;
  virtual void computeCL(double p, IPCLength &ipcl) const;
  virtual CLengthLaw * clone() const;
#endif
};

class VariableIsotropicCLengthLaw : public CLengthLaw
{
 public :
 protected :
  double cl;
  double el, nl; 
 public:
  // constructor
  VariableIsotropicCLengthLaw(const int num, const double _cl, const double _el, const double _nl,
                              const bool init=true);
  ~VariableIsotropicCLengthLaw(){}
#ifndef SWIG
  VariableIsotropicCLengthLaw(const VariableIsotropicCLengthLaw &source);
  VariableIsotropicCLengthLaw& operator=(const CLengthLaw &source);
  virtual clengthlawname getType() const {return CLengthLaw::variableisotropicclengthlaw;};
  virtual void createIPVariable(IPCLength* &ipv) const;
  virtual void initLaws(const std::map<int,CLengthLaw*> &maplaw){}; //nothing now, we will see if we use the mapping;
  virtual void computeCL(double p, IPCLength &ipcl) const;
  virtual CLengthLaw * clone() const;
#endif
};

class VariableAnisotropicCLengthLaw : public CLengthLaw
{
 public :
 protected :
  SVector3 cl;
  SVector3 euler; 
  double el, nl;
 public:
  // constructor
  VariableAnisotropicCLengthLaw(const int num, const double _cl1, const double _cl2, const double _cl3, 
                                       const double _euler1, const double _euler2, const double _euler3,
                                        const double _el, const double _nl, const bool init=true);
  ~VariableAnisotropicCLengthLaw(){}
#ifndef SWIG
  VariableAnisotropicCLengthLaw(const VariableAnisotropicCLengthLaw &source);
  VariableAnisotropicCLengthLaw& operator=(const CLengthLaw &source);
  virtual clengthlawname getType() const {return CLengthLaw::variableanisotropicclengthlaw;};
  virtual void createIPVariable(IPCLength* &ipv) const;
  virtual void initLaws(const std::map<int,CLengthLaw*> &maplaw){}; //nothing now, we will see if we use the mapping;
  virtual void computeCL(double p, IPCLength &ipcl) const;
  virtual CLengthLaw * clone() const;
#endif
};
#endif //CLENGTHLAW_H_
