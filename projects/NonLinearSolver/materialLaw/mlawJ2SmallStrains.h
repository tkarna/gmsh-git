//
// C++ Interface: materialLaw
//
// Description: j2 law for small strains
//
//
// Author:  <Gauthier BECKER>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWJ2SMALLSTRAINS_H_
#define MLAWJ2SMALLSTRAINS_H_

#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"

#ifndef SWIG
// class for the hardening law (virtual pure class)
class hardeningLaw
{
 public:
  virtual void hard(const double plastic_n,const double sy0, double &R, double &dR, double &ddR) const=0;
};
#endif // SWIG

class linearHardeningLaw : public hardeningLaw
{
#ifndef SWIG
 protected:
  const double _hardening;
#endif // SWIG
 public:
  linearHardeningLaw(const double _hard);
#ifndef SWIG
  linearHardeningLaw(const linearHardeningLaw &source);
  ~linearHardeningLaw(){}
  virtual void hard(const double plastic_n,const double sy0, double &R, double &dR, double &ddR) const;
#endif //SWIG
};

class powerHardeningLaw : public hardeningLaw
{
#ifndef SWIG
 protected:
  const double _hardening, _hexp;
#endif // SWIG
 public:
  powerHardeningLaw(const double _hard, const double _exp);
#ifndef SWIG
  powerHardeningLaw(const powerHardeningLaw &source);
  ~powerHardeningLaw(){}
  virtual void hard(const double plastic_n,const double sy0, double &R, double &dR, double &ddR) const;
#endif // SWIG
};

class exponentialHardeningLaw : public hardeningLaw
{
#ifndef SWIG
 protected:
  const double _hardening, _hexp;
#endif //SWIG
 public:
  exponentialHardeningLaw(const double _hard, const double _exp);
#ifndef SWIG
  exponentialHardeningLaw(const exponentialHardeningLaw &source);
  ~exponentialHardeningLaw(){}
  virtual void hard(const double plastic_n,const double sy0, double &R, double &dR, double &ddR) const;
#endif // SWIG
};

class swiftHardeningLaw : public hardeningLaw
{
#ifndef SWIG
 protected:
  const double _hardening, _hexp;
#endif // SWIG
 public:
  swiftHardeningLaw(const double _hard, const double _exp);
#ifndef SWIG
  swiftHardeningLaw(const swiftHardeningLaw &source);
  ~swiftHardeningLaw(){}
  virtual void hard(const double plastic_n,const double sy0, double &R, double &dR, double &ddR) const;
#endif // SWIG
};

class linearExponentialHardeningLaw : public hardeningLaw
{
#ifndef SWIG
 protected:
  const double _hard1, _hard2, _hexp;
#endif // SWIG
 public:
  linearExponentialHardeningLaw(const double hard1, const double hard2, const double exp_);
#ifndef SWIG
  linearExponentialHardeningLaw(const linearExponentialHardeningLaw &source);
  ~linearExponentialHardeningLaw(){}
  virtual void hard(const double plastic_n,const double sy0, double &R, double &dR, double &ddR) const;
#endif // SWIG
};

class mlawJ2SmallStrains : public materialLaw
{
 #ifndef SWIG
 protected:
  const double _rho, _E,_nu,_G,_sy0, _G2, _G3,_Gsquare3, _Gsquare4, _kiso, _lambda, _onethird, _twothird, _mindp;
  const double _tol; // tolerance for return mapping algorithm
  const int _itemax; // max number of iteration for return mapping algorithm
  const hardeningLaw * _hlaw;
  bool _rhlaw; // to remove _hlaw if it is created here
  STensor43 _hooke, _oneandhalfIdev; // for elastic predictor and stiffness computation
 private: // cache data for constitutive
  mutable STensor3 _destrain, _estress, _edeviator; // elastic strain, elastic stress and elastic deviatoric stress
  mutable STensor3 _normal; // to compute normal to yield surface
  mutable STensor43 _dianormal; // dyadic produc of _normal to compute the stiffness
 #endif // SWIG
 public:
  mlawJ2SmallStrains(const int num,const double E, const double nu, const double rho,const double sy0,const hardeningLaw* hlaw=NULL);
  #ifndef SWIG
  mlawJ2SmallStrains(const mlawJ2SmallStrains &source);
  ~mlawJ2SmallStrains();
  /* material law */
  virtual matname getType() const{return materialLaw::j2smallstrain;}
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){};
  virtual double soundSpeed() const;
  virtual double density() const{return _rho;}

  /* specific functions */
//  virtual void constitutive(dstrn, strs_n, strs, pstrn_n, pstrn, p_n, &p, &twomu, dtwomu, Calgo, dCalgo,dpdE); // constbox_ep
  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPVariable *q0,       // array of initial internal variable
                            IPVariable *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            const bool stiff            // if true compute the tangents
                           ) const;
  #endif // SWIG
};

#endif // MLAWJ2SMALLSTRAINS_H_
