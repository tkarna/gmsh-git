//
//
//
// Description: Define gurson damake nucleation
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "GursonDamageNucleation.h"

GursonDamageNucleation::GursonDamageNucleation(const int num, const bool init): _num(num), _initialized(init)
											
{
  
}

GursonDamageNucleation::GursonDamageNucleation(const GursonDamageNucleation &source)
{
  _num = source._num;
  _initialized = source._initialized;
}

GursonDamageNucleation& GursonDamageNucleation::operator=(const GursonDamageNucleation &source)
{
  _num = source._num;
  _initialized = source._initialized;
  return *this;
}

LinearGursonDamageNucleation::LinearGursonDamageNucleation(const int num, double A0, const bool init) :
					GursonDamageNucleation(num,init), _A0(A0)
{

}
LinearGursonDamageNucleation::LinearGursonDamageNucleation(const LinearGursonDamageNucleation &source) :
					GursonDamageNucleation(source)
{
  _A0 = source._A0;
}

LinearGursonDamageNucleation& LinearGursonDamageNucleation::operator=(const GursonDamageNucleation &source)
{
  GursonDamageNucleation::operator=(source);
  const LinearGursonDamageNucleation* src = dynamic_cast<const LinearGursonDamageNucleation*>(&source);
  if(src != NULL)
  {
    _A0 = src->_A0;
  }
  return *this;
}
void LinearGursonDamageNucleation::createIPVariable(IPGursonNucleation* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPLinearGursonNucleationRate();
}

void LinearGursonDamageNucleation::rate(double p,  double p0, IPGursonNucleation &ipv) const
{
  double fdot   = 0.;
  double dFdot  = _A0;
  double ddFdot = 0.;
  if(p>0.)
  {
    fdot   = _A0*p;
  }
  ipv.set(fdot,dFdot,ddFdot);
}
GursonDamageNucleation * LinearGursonDamageNucleation::clone() const
{
  return new LinearGursonDamageNucleation(*this);
}

ExponentialGursonDamageNucleation::ExponentialGursonDamageNucleation(const int num,  double fn, double sn, double epsilonn, 
                                                                     const bool init) :
					GursonDamageNucleation(num,init), _fn(fn), _sn(sn), _epsilonn(epsilonn)
{

}
ExponentialGursonDamageNucleation::ExponentialGursonDamageNucleation(const ExponentialGursonDamageNucleation &source) :
					GursonDamageNucleation(source)
{
  _fn = source._fn;
  _sn = source._sn;
  _epsilonn = source._epsilonn;
}

ExponentialGursonDamageNucleation& ExponentialGursonDamageNucleation::operator=(const GursonDamageNucleation &source)
{
  GursonDamageNucleation::operator=(source);
  const ExponentialGursonDamageNucleation* src = dynamic_cast<const ExponentialGursonDamageNucleation*>(&source);
  if(src != NULL)
  {
    _fn = src->_fn;
    _sn = src->_sn;
    _epsilonn = src->_epsilonn;
  }
  return *this;
}
void ExponentialGursonDamageNucleation::createIPVariable(IPGursonNucleation* &ipv) const
{
  if(ipv != NULL) delete ipv;
  ipv = new IPExponentialGursonNucleationRate();
}

void ExponentialGursonDamageNucleation::rate(double p, double p0, IPGursonNucleation &ipv) const
{
  double fdot   = 0.;
  if(p0<0.) p0=0.;
  double dFdot  = 0.;
  if(_sn!=0. and _fn>0.) dFdot=_fn/sqrt(2.*3.14159*_sn*_sn)*exp(-1./2.*(p0-_epsilonn)*(p0-_epsilonn)/_sn/_sn);
  double ddFdot = 0.;
  if(p>0.)
  {
    fdot   = dFdot*p;
  }
  ipv.set(fdot,dFdot,ddFdot);
}
GursonDamageNucleation * ExponentialGursonDamageNucleation::clone() const
{
  return new ExponentialGursonDamageNucleation(*this);
}

