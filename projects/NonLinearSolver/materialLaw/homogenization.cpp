#ifndef HOMOGENIZATION_C
#define HOMOGENIZATION_C 1

#ifdef NONLOCALGMSH
#include "homogenization.h"
#include "matrix_operations.h"
#include "rotations.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "ID.h"
#include "material.h"
using namespace MFH;
#endif

/*******************************************************************************************************
//Resolution of the localization problem on the equivalent inclusion system
// INPUT:	DE: macro strain increment (given)
// 				DeM, DeI: first guess for the strain increment in each phase
// 				C_n: reference tangent operator (MT->matrix, SC->macro) at tn for the computation of Eshelby's tensor
// 				mtx_mat, icl_mat: Material objects for each phase
//				mtx: 1 if there is a matrix phase, 0 otherwise
//				Nicl: number of inclusion phases
// OUTPUT: solution DeM and DeI
//				 updated state variables in each phase
//					Reference macro tangent operator Cref and derivatives dCref, macro algo tangent operator Calgo 																	
**********************************************************************************************************/
//***************new tensor added by wu ling: double* dpdE, double* strs_dDdp_bar
int impl_localization(int sch, double* DE, Material* mtx_mat, Material** icl_mat, double vfM, double* vfI, double** ar, double** angles,
	double* statev_n, double* statev, int nsdv, int idsdv_m, int* idsdv_i, int mtx, int Nicl, double** C, double*** dC, double** Calgo, double* dpdE, double* strs_dDdp_bar,double *Sp_bar, int kinc, int kstep){


	return 0;

}

/*******************************************************************************************************
// EXPLICIT resolution of the localization problem on the equivalent inclusion system
// INPUT:	DE: macro strain increment (given)
// 				DeM, DeI: strain increment in each phase at tn
// 				C_n: reference tangent operator (MT->matrix, SC->macro) at tn for the computation of Eshelby's tensor
// 				mtx_mat, icl_mat: Material objects for each phase
//				mtx: 1 if there is a matrix phase, 0 otherwise
//				Nicl: number of inclusion phases
// OUTPUT: updated DeM and DeI
//				 updated state variables in each phase
//				 Reference macro tangent operator Cref and derivatives dCref, macro algo tangent operator Calgo 																	
**********************************************************************************************************/
int expl_localization(int sch, double* DE, Material* mtx_mat, Material** icl_mat, double vfM, double* vfI, double** ar, double** angles,
	double* statev_n, double* statev, int nsdv, int idsdv_m, int* idsdv_i, int mtx, int Nicl, double** C, double*** dC, double** Calgo, int kinc, int kstep){

	return 0;

}

/**************************************************************************************************************************************************/
 //*****************New tenor added by wu ling: double* dpdE, double* strs_dDdp_bar ****************
int equationsNR(double* DE, double* DeM, double** DeI, Material* mtx_mat, Material** icl_mat, double vfM, double* vfI, 
	double* statev_n, double* statev, int idsdv_m, int* idsdv_i, double*** S0, int sch, int mtx, int Nicl, int kinc, int kstep, int last, 
	int neq, double* F, double** J, double** C, double*** dC, double** Calgo,double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double alpha1)

{


}
/************************************************
 * UPDATE UNKNOWNS ACCORDING TO NR CORRECTION **
 * *********************************************/
//  INPUT:   DE:  macro strain increment
//  				De1:	phase strain increments at tn
//          DeM1: matrix strain increment at tn
//            vf: volume fraction of inclusion phases
//           vfM: volume fraction of matrix
//  				  C:	macro tangent operator at tn
//  		 corrNR:  vector of N-R corrections
//  OUTPUT: De1:  updated strain increments
//  					C:  updated macro tangent operator (if SC)

void updateNR(double* DE, double** De1, double* DeM1, double* vf, double vfM, double** C, double* corrNR, int sch, int mtx, int Nicl)
{

}


/**********************************************
 * COMPUTE THE NORM OF THE NR RESIDUAL VECTOR**
 * ********************************************/
// INPUT:   vec:	 residual vector 
// 				  	 sch:  homogenization scheme
// 				  	 Nicl: number of inclusions
// 				  	 NORM: reference quantity for normalization
// OUTPUT:  return a scalar residual
double scalar_res(double* vec, int sch, int Nicl, double NORM){



}

/*************************************************
 * COMPUTE ESHELBY's TENSOR - GENERAL CASE	**
 * **********************************************/
//   INPUT:  dSdE:  				reference moduli  (may be anisotropic)
//    	     a1,a2,a3:		  aspect ratio
//   OUTPUT: Esh:           Eshelby's tensor            	       
void eshelby(double** dSdE, double ar1, double ar2, double**Esh)
{
   
	return;
}


/* ***************************************************************************
// ** COMPUTATION OF ESCHELBY'S TENSOR IN THE CASE OF SPHEROIDAL INCLUSIONS ** 
// ***************************************************************************/

/*    April 2003 - O. Pierard, modified october 2008 - L. Brassart
//    Formulae from graduation work C. Friebel
//    INPUT:  CO:     Isotropic stiffness tensor
//            ar:     aspect ratio of the inclusion
//    OUTPUT: Esh:    Eshelby's tensor, revolution axis of inclusions along direction 3 */

void eshelby_iso(double **C0, double ar, double **Esh)

{
}


// WL: for modification 

/* ***************************************************************************
// ** COMPUTATION OF ESHELBY'S TENSOR IN THE CASE OF SPHEROIDAL INCLUSIONS ** 
// ***************************************************************************/

/*    April 2003 - O. Pierard, modified october 2008 - L. Brassart, modified JUNE 2012 - L. Wu
//    Formulae from graduation work C. Friebel
//    INPUT:  CO:     Isotropic stiffness tensor
//            ar:     aspect ratio of the inclusion
//    OUTPUT: S:    Eshelby's tensor, revolution axis of inclusions along direction 3 */
//            dS:   dSdnu, 
//            ddS:  ddSddnu
//            dnu:  dnu[0]= dnudmu, dnu[1]=ddnuddmu, dnu[2]=dnudk , dnu[3]=ddnuddk, dnu[4]=ddnudmudk 

void eshelby_iso(double **C0, double ar, double **S, double **dS, double ** ddS, double *dnu)

{
	
  
}


void Eshelby(double **C0, double* ar, double *euler, double **S, double **dS, double ** ddS, double *dnu){


}




/*******************************************************************************************************
//Resolution of the localization problem on the equivalent inclusion system
// INPUT:	DE: macro strain increment (given)
// 				DeM, DeI: first guess for the strain increment in each phase
// 				C_n: reference tangent operator (MT->matrix, SC->macro) at tn for the computation of Eshelby's tensor
// 				mtx_mat, icl_mat: Material objects for each phase
//				mtx: 1 if there is a matrix phase, 0 otherwise
//				Nicl: number of inclusion phases
// OUTPUT: solution DeM and DeI
//				 updated state variables in each phase
//					Reference macro tangent operator Cref and derivatives dCref, macro algo tangent operator Calgo 																	
**********************************************************************************************************/
//***************new tensor added by wu ling: double* dpdE, double* strs_dDdp_bar
int impl_SecLocal(int sch, double* DE, Material* mtx_mat, Material** icl_mat, double vfM, double* vfI, double** ar, double** angles,
	double* statev_n, double* statev, int nsdv, int idsdv_m, int* idsdv_i, int mtx, int Nicl, double** C, double*** dC, double** Calgo, double* dpdE, double* dstrsdp_bar,double *Sp_bar, int kinc, int kstep){



	return 0;

}
//********************************for secant MT scheme by Ling Wu June 2012********************************************************************
//*****************New tenor added by wu ling: double* dpdE, double* dstrsdp_bar ****************

int equationsNR_Sec(int sch, double* DE, double* DeM, double** DeI, Material* mtx_mat, Material** icl_mat, double vfM, double* vfI, 
	double* statev_n, double* statev, int idsdv_m, int* idsdv_i, int mtx, int Nicl, double** ar, double** angles, int kinc, int kstep, int last, 
	int neq, double* F, double** J, double** C, double*** dC, double** Calgo, double* dpdE, double* dstrsdp_bar, double *Sp_bar, double alpha1){

       return 0;

}


#endif




