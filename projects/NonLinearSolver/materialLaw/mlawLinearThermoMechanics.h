//
// C++ Interface: material law
//              Linear Thermo Mechanical law
// Author:  <L. Homsy>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWLINEARTHERMOMECHANICS_H_
#define MLAWLINEARTHERMOMECHANICS_H_
#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"
#include "ipLinearThermoMechanics.h"

class mlawLinearThermoMechanics : public materialLaw
{
 protected:
  // can't be const due to operator= constructor (HOW TO CHANGE THIS ??)
  double _rho; // density
  STensor43 _ElasticityTensor;
  double _poissonMax;
  double _alpha; // parameter of anisotropy
  double _beta; // parameter of anisotropy
  double _gamma; // parameter of anisotropy
  double _cv;
  double _t0;
  double _Kx;
  double _Ky;
  double _Kz;
  double _alphax;
  double _alphay;
  double _alphaz;
  
  STensor3 _k;
  STensor3 _alphaDilatation;
  

#ifndef SWIG
 private: // cache data for constitutive function
  //mutable STensor3 defo_g,defo_l,sigma_g,sigma_l;
 private: // cache data for update function
  mutable STensor3 S,Str,Siso;
 private: // cache data for tangent function
  mutable STensor43 M,Mtr,Miso;
#endif // SWIG
 public:
  mlawLinearThermoMechanics(const int num,const double rho, const double Ex, const double Ey, const double Ez, 
						  const double Vxy, const double Vxz, const double Vyz,
						  const double MUxy, const double MUxz, const double MUyz,
			 			  const double alpha, const double beta, const double gamma  ,
			                   const double cv,const double t0,const double Kx,const double Ky,
			                  const double Kz,const double alphax,const double alphay,const double alphaz);
 #ifndef SWIG
  mlawLinearThermoMechanics(const mlawLinearThermoMechanics &source);
  mlawLinearThermoMechanics& operator=(const materialLaw &source);
  // function of materialLaw
  virtual matname getType() const{return materialLaw::LinearThermoMechanics;}  // WTF IS THIS ??
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt =0) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
  virtual double poissonRatio() const;
  virtual double shearModulus() const;
  virtual double soundSpeed() const; // default but you can redefine it for your case 
public:
  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPLinearThermoMechanics *q0,       // array of initial internal variable
                            IPLinearThermoMechanics *q1,              // updated array of internal variable (in ipvcur on output),
			    STensor43 &Tangent,         // constitutive tangents (output)               	            
			    const bool stiff
			    ) const;
  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPLinearThermoMechanics *q0,       // array of initial internal variable
                            IPLinearThermoMechanics *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            double T,
  			    const SVector3 &gradT,
                            SVector3 &fluxT,
                            STensor3 &dPdT,
                            STensor3 &dqdgradT,
                            const bool stiff            // if true compute the tangents
                           ) const;

  const STensor43& getElasticityTensor() const { return _ElasticityTensor;};
  const STensor3&  getConductivityTensor() const { return _k;};
 protected:
  virtual double deformationEnergy(STensor3 defo) const ;
  virtual double deformationEnergy(STensor3 defo, const STensor3& sigma) const ;

 #endif // SWIG
};

#endif // MLAWLINEARTHERMOMECHANICS_H_
