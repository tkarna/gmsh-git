#ifndef MATERIAL_H
#define MATERIAL_H 1

#include "ID.h"
#include "damage.h" 
#include "clength.h"
#include "lccfunctions.h"

#ifdef NONLOCALGMSH
namespace MFH {
#endif
//****in all of the constbox**New tenor added by wu ling: double* dpdE, double* strs_dDdp_bar ****************
//Generic material
class Material{

	protected:
		int constmodel;
		int nsdv;
		int pos_strn, pos_strs, pos_dstrn; 		//position of strain, stress and dstrn in the statev vector
		Damage* dam;
		int pos_dam;	//position of damage state variables in statev vector

                int pos_p;

                Clength* clength;

	public:
		
		
		Material(double* props, int idmat){
			constmodel = (int) props[idmat];
		}
		virtual ~Material(){ ; }
		inline int get_constmodel(){  return constmodel; }
		
		//give the number of state variables needed for the constitutive model
		inline int get_nsdv(){  return nsdv; }
		
		//give the position of strn, strs and dstrn in the statev vector
		inline int get_pos_strn() {	return pos_strn; }
		inline int get_pos_strs() {	return pos_strs; }
		inline int get_pos_dstrn()	{	return pos_dstrn; }

                //give the position of damage in the statev vector === p_bar, dp_bar ,D, dD
                inline int get_pos_dam() { if(dam)   return pos_dam; else return -1; }    
                inline int get_pos_p() { if(dam)   return pos_p; else return -1; }  
		
		//print the material properties
		virtual void print()=0;

		//compute the constitutive response of the material
		//INPUT: dstrn, strs_n, statev_n, kinc, kstep
		//OUTPUT: strs, statev, Cref, dCref, Calgo
		virtual void constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev,double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, int kinc, int kstep)=0;

		virtual void constboxSecant(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep);
		virtual void constboxSecantZero(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep);

                virtual void constboxFullStep(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** C0d, double*** dC0d, double** dC0dp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** depdE, double** c_g, int kinc, int kstep);

                virtual void constbox_2order(int mtx, double *DE, double* dstrn, double* strs, double* statev_n, double* statev, double **Calgo, Lcc* LCC, YieldF* YF, double** c_g, int kinc, int kstep);

		//compute a reference tangent operator over the time increment using a generalized mid-point rule
		virtual void get_refOp(double* statev, double** C, int kinc, int kstep)=0;

		//compute an elastic operator 
		virtual void get_elOp(double** Cel)=0;
		virtual void get_elOD(double* statev, double** Cel)=0;
                virtual void unload_step(double* dstrn, double* strs_n, double* statev, int kinc, int kstep)=0;

                //added function for second moment methed
                virtual int get_pos_eqstrs() const;
                virtual bool eqstrs_exist() const;
                 

#ifdef NONLOCALGMSH
		virtual int get_pos_currentplasticstrainfornonlocal() const = 0;
		virtual int get_pos_effectiveplasticstrainfornonlocal() const = 0;
		virtual int get_pos_damagefornonlocal() const = 0;
                virtual int get_pos_mtx_stress () const;
                virtual int get_pos_inc_stress () const;
		virtual double getElasticEnergy (double* statev) =0;
		virtual double getPlasticEnergy (double* statev) =0;
#endif


};

//Linear elastic material
class EL_Material : public Material{

	private:
		double E, nu;
               
	public:
		
		EL_Material(double* props, int idmat);
		~EL_Material();
		
		virtual void print();
		virtual void constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, int kinc, int kstep);
		virtual void constboxSecant(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep);

                virtual void constbox_2order(int mtx, double *DE, double* dstrn, double* strs, double* statev_n, double* statev, double **Calgo, Lcc* LCC, YieldF* YF, double** c_g, int kinc, int kstep);

               	virtual void get_refOp(double* statev, double** C, int kinc, int kstep);
		virtual void get_elOp(double** Cel);
		virtual void get_elOD(double* statev, double** Cel);
                virtual void unload_step(double* dstrn, double* strs_n, double* statev, int kinc, int kstep);
#ifdef NONLOCALGMSH
		virtual int get_pos_currentplasticstrainfornonlocal() const;
		virtual int get_pos_effectiveplasticstrainfornonlocal() const;
		virtual int get_pos_damagefornonlocal() const;
                virtual double getElasticEnergy (double* statev) ;
		virtual double getPlasticEnergy (double* statev) ;
#endif
		
}; 

//Elasto-plastic material
class EP_Material : public Material{

	private:
		double E,nu,sy0;
		int htype;
		double hmod1, hmod2, hexp;
		int pos_pstrn, pos_dp, pos_twomu, pos_eqstrs;

	public:
	

		EP_Material(double* props, int idmat);
		~EP_Material();

		virtual void print();
		virtual void constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, int kinc, int kstep);
//*****************New tenor added by wu ling: double* dpdE, double* strs_dDdp_bar, double* dDdE,****************
		virtual void constboxSecant(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep);
                virtual void constboxSecantZero(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep);
//******************new constbox for Fullstep
                virtual void constboxFullStep(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** C0d, double*** dC0d, double** dC0dp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** depdE, double** c_g, int kinc, int kstep);
//******************new constbox for 2rd order method
                virtual void constbox_2order( int mtx, double *DE, double* dstrn, double* strs, double* statev_n, double* statev, double **Calgo, Lcc* LCC, YieldF* YF, double** c_g, int kinc, int kstep);

		virtual void get_refOp(double* statev, double** C, int kinc, int kstep);
		virtual void get_elOp(double** Cel);
		virtual void get_elOD(double* statev, double** Cel);
                virtual void unload_step(double* dstrn, double* strs_n, double* statev, int kinc, int kstep);

                virtual int get_pos_eqstrs() const;
                virtual bool eqstrs_exist() const;

#ifdef NONLOCALGMSH
		virtual int get_pos_currentplasticstrainfornonlocal() const;
		virtual int get_pos_effectiveplasticstrainfornonlocal() const;
		virtual int get_pos_damagefornonlocal() const;
                virtual double getElasticEnergy (double* statev) ;
		virtual double getPlasticEnergy (double* statev) ;
#endif


};

//Composite material homogenized with Voigt scheme
class VT_Material : public Material{

	private:
		int Nph;					//number of phases in the composite
		Material** mat;		//array of pointers on Materials
		double* vf;				//array of volume fraction of each phase
			

	public:
		
		VT_Material(double* props, int idmat);
		~VT_Material();

		virtual void print();
		virtual void constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, int kinc, int kstep);
		virtual void get_refOp(double* statev, double** C, int kinc, int kstep);
		virtual void get_elOp(double** Cel);
		virtual void get_elOD(double* statev, double** Cel);
                virtual void unload_step(double* dstrn, double* strs_n, double* statev, int kinc, int kstep);
#ifdef NONLOCALGMSH
		virtual int get_pos_currentplasticstrainfornonlocal() const;
		virtual int get_pos_effectiveplasticstrainfornonlocal() const;
		virtual int get_pos_damagefornonlocal() const;
                virtual double getElasticEnergy (double* statev) ;
		virtual double getPlasticEnergy (double* statev) ;
#endif

};  
// Composite material homogenized with Mori-Tanaka scheme
class MT_Material : public Material{

	protected:
		int Nph;
		Material* mtx_mat;
		Material* icl_mat;
		int idsdv_m, idsdv_i;
		double vf_i, vf_m;
		double* ar;
		double* euler;
		int pos_C;

       
	public:
		
		MT_Material(double* props, int idmat);
		~MT_Material();

		virtual void print();
		virtual void constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, int kinc, int kstep);
		inline int get_pos_C() {	return pos_C; } 
	
		virtual void get_refOp(double* statev, double** C, int kinc, int kstep);
		virtual void get_elOp(double** Cel);
		virtual void get_elOD(double* statev, double** Cel);
                virtual void unload_step(double* dstrn, double* strs_n, double* statev, int kinc, int kstep);
#ifdef NONLOCALGMSH
		virtual int get_pos_currentplasticstrainfornonlocal() const;
		virtual int get_pos_effectiveplasticstrainfornonlocal() const;
		virtual int get_pos_damagefornonlocal() const;
                virtual int get_pos_mtx_stress () const;
                virtual int get_pos_inc_stress () const;
                virtual double getElasticEnergy (double* statev) ;
		virtual double getPlasticEnergy (double* statev) ;
#endif
};


//Composite material homogenized with Mori-Tanaka scheme with fist-order Incremental-secant method
class MTSecF_Material : public MT_Material{

	private:

	public:
		
		MTSecF_Material(double* props, int idmat);
		~MTSecF_Material();

		virtual void constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, int kinc, int kstep);
                virtual void unload_step(double* dstrn, double* strs_n, double* statev, int kinc, int kstep);
};

//Composite material homogenized with Mori-Tanaka scheme with fist-order secant method
class MTFullStep_Material : public MT_Material{

	private:

	public:
		
		MTFullStep_Material(double* props, int idmat);
		~MTFullStep_Material();

		virtual void constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, int kinc, int kstep);
               
};

//By Ling Wu AUG 2013
//Composite material homogenized with Mori-Tanaka scheme with Second-order Incremental-secant method
class MTSecSd_Material : public MT_Material{

	private:
                  Lcc LCC;
	public:
		
		MTSecSd_Material(double* props, int idmat);
		~MTSecSd_Material();

		virtual void constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, int kinc, int kstep);
                virtual void unload_step(double* dstrn, double* strs_n, double* statev, int kinc, int kstep);
};



// Composite material homogenized with Self-Consistent scheme
class SC_Material : public Material{

	private:
		int Nph;
		int mtx;
		int Nicl;
		Material* mtx_mat;
		Material** icl_mat;
		int idsdv_m, *idsdv_i;
		double *vf_i, vf_m;
		double** ar;
		double** euler;
		int pos_C;

	public:
		
		SC_Material(double* props, int idmat);
		~SC_Material();

		virtual void print();
		virtual void constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, int kinc, int kstep);
		inline int get_pos_C() {	return pos_C; } 
		
		virtual void get_refOp(double* statev, double** C, int kinc, int kstep);
		virtual void get_elOp(double** Cel);
		virtual void get_elOD(double* statev, double** Cel);
                virtual void unload_step(double* dstrn, double* strs_n, double* statev, int kinc, int kstep);
#ifdef NONLOCALGMSH
		virtual int get_pos_currentplasticstrainfornonlocal() const;
		virtual int get_pos_effectiveplasticstrainfornonlocal() const;
		virtual int get_pos_damagefornonlocal() const;
                virtual int get_pos_mtx_stress () const;
                virtual int get_pos_inc_stress () const;
                virtual double getElasticEnergy (double* statev) ;
		virtual double getPlasticEnergy (double* statev) ;
#endif
};

// Anisotropic Linear elastic material
class ANEL_Material : public Material{

	private:
		double* anpr;
                double* euler;

	public:
		
		ANEL_Material(double* props, int idmat);
		~ANEL_Material();
		
		virtual void print();
		virtual void constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, int kinc, int kstep);

		virtual void constboxSecant(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep);

//******************new constbox for 2rd order method
                virtual void constbox_2order(int mtx, double *DE, double* dstrn, double* strs, double* statev_n, double* statev, double **Calgo, Lcc* LCC, YieldF* YF, double** c_g, int kinc, int kstep);

		virtual void get_refOp(double* statev, double** C, int kinc, int kstep);
		virtual void get_elOp(double** Cel);

		virtual void get_elOD(double* statev, double** Cel);
                virtual void unload_step(double* dstrn, double* strs_n, double* statev, int kinc, int kstep);
#ifdef NONLOCALGMSH
		virtual int get_pos_currentplasticstrainfornonlocal() const;
		virtual int get_pos_effectiveplasticstrainfornonlocal() const;
		virtual int get_pos_damagefornonlocal() const;
                virtual double getElasticEnergy (double* statev) ;
		virtual double getPlasticEnergy (double* statev) ;
#endif
		
}; 

#ifdef NONLOCALGMSH
#else
Damage* init_damage(double* props, int iddam);
Clength* init_clength(double* props, int idclength);
#endif
#ifdef NONLOCALGMSH
}
#endif
#endif
