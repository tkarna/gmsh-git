#ifndef J2PLAST_H
#define J2PLAST_H 1

// Resolution of a constitutive model in elastoplasticity 
int constbox_ep(double E, double nu, double sy0, int htype, double hmod1, double hmod2,
	double hexp, double *dstrn, double *strs_n, double *strs, double* pstrn_n, double* pstrn,
	double p_n, double *p, double *twomu_iso, double* dtwomu_iso, double** Calgo, double*** dCalgo, double* dpdE, bool residual);

// Evaluation of the hardening function and its derivatives  
void j2hard (double p, double sy0, int htype, double hmod1, double hmod2, double hexp, double* R, double* dR, double* ddR);

// Computation of J2 algorithmic tangent operator at tn 
void algoJ2(double E, double nu, double sy0, int htype, double hmod1, double hmod2, double hexp, 
	double* strs, double p, double dp, double** Calgo);

#endif
