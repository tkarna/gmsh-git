//
// C++ Interface: material law
//
// Description: TransverseIsotropic law
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "mlawTransverseIsotropic.h"
#include <math.h>
#include "MInterfaceElement.h"

mlawTransverseIsotropic::mlawTransverseIsotropic(const int num,const double E,const double nu,
                           const double rho, const double EA, const double GA, const double nu_minor,
                           const double Ax, const double Ay, const double Az) :
                                materialLaw(num,true), _E(E), _nu(nu), _nu_minor(nu_minor), _rho(rho), _EA(EA),
                                _GA(GA), _A(Ax,Ay,Az), _n(EA/E)
{
  _A.normalize();
  _m = 1.-nu-2.*nu_minor*nu_minor*_n;
  if(_m<=0.)
    Msg::Error("Wrong parameter m = 1-nu-2*EA/E*nu*nu %f",_m);

  _lambda = E*(nu+_n*nu_minor*nu_minor)/(1.+nu)/_m;
  _mu = 0.5*E/(1.+nu);
  _alpha = _mu-_GA;
  _beta  = _E*(_n*nu_minor*(1.+nu-nu_minor)-_nu)/4./_m/(1+_nu);
  _gamma = EA*(1-nu)/8./_m-(_lambda+2*_mu)/8.+_alpha/2.-_beta;

}
mlawTransverseIsotropic::mlawTransverseIsotropic(const mlawTransverseIsotropic &source) : materialLaw(source),_E(source._E), _nu(source._nu),
                                                       _rho(source._rho),_EA(source._EA), _GA(source._GA),
                                                       _nu_minor(source._nu_minor),
                                                       _A(source._A), _n(source._n), _m(source._m),
                                                       _lambda(source._lambda), _mu(source._mu),
                                                       _alpha(source._alpha),_beta(source._beta),
                                                       _gamma(source._gamma) {}
mlawTransverseIsotropic& mlawTransverseIsotropic::operator=(const materialLaw &source)
{
  materialLaw::operator=(source);
  const mlawTransverseIsotropic* src =static_cast<const mlawTransverseIsotropic*>(&source);
  _E = src->_E;
  _nu = src->_nu;
  _nu_minor = src->_nu_minor;
  _rho = src->_rho;
  _EA = src->_EA;
  _GA = src->_GA;
  _A = src->_A;
  _n = src->_n;
  _m = src->_m;
  _lambda = src->_lambda;
  _mu = src->_mu;
  _alpha = src->_alpha;
  _beta = src->_beta;
  _gamma = src->_gamma;
  return *this;
}

void mlawTransverseIsotropic::createIPState(IPStateBase* &ips,const bool* state_,const MElement *ele, const int nbFF_, const int gpt) const
{
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new IPTransverseIsotropic();
  IPVariable* ipv1 = new IPTransverseIsotropic();
  IPVariable* ipv2 = new IPTransverseIsotropic();
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

double mlawTransverseIsotropic::soundSpeed() const
{
  double factornu = (1.-_nu)/((1.+_nu)*(1.-2.*_nu));
  if(_E>_EA)  return sqrt(_E*factornu/_rho);
  else   return sqrt(_EA*factornu/_rho);
}


void mlawTransverseIsotropic::constitutive(const STensor3& F0,const STensor3& Fn,STensor3 &P,const IPTransverseIsotropic *q0, IPTransverseIsotropic *q1,STensor43 &Tangent,
                                const bool stiff) const
{

  /* compute gradient of deformation */
  double Jac= (Fn(0,0) * (Fn(1,1) * Fn(2,2) - Fn(1,2) * Fn(2,1)) -
          Fn(0,1) * (Fn(1,0) * Fn(2,2) - Fn(1,2) * Fn(2,0)) +
          Fn(0,2) * (Fn(1,0) * Fn(2,1) - Fn(1,1) * Fn(2,0)));
  double lnJ = log(Jac);
  C = 0.;
  for(int I=0; I<3; I++)
    for(int J=0; J<3; J++)
      for(int k=0; k<3; k++)
        C(I,J)+=Fn(k,I)*Fn(k,J);

//  double I1= C.trace();
  Cinv = C.invert();

  Siso =(Cinv * (_lambda*lnJ-_mu));
  for(int I=0; I<3; I++)
    Siso(I,I) +=_mu;

  SVector3 CA(0.);
  for(int I=0; I<3; I++)
    for(int J=0; J<3; J++)
      CA(I) += C(I,J)*_A(J);

  double I4= dot(_A,CA);
  tensprod(_A,_A,AA);
  tensprod(_A,CA,ACA);
  tensprod(CA,_A,CAA);

  Str = Cinv;
  Str *= (2.*_beta*(I4-1.));
  Str += AA*(2.*(_alpha+2.*_beta*lnJ+2.*_gamma*(I4-1)));
  Str -= ((CAA+ACA)*_alpha);

  P=0.;
  for(int i=0; i<3; i++)
    for(int J=0; J<3; J++)
      for(int K=0; K<3; K++)
        P(i,J)+=Fn(i,K)*(Str(K,J)+Siso(K,J));

  q1->_elasticEnergy=deformationEnergy(C);

  /* tangents */
  if(stiff)
  {
    for(int I=0; I<3; I++)
      for(int J=0; J<3; J++)
        for(int K=0; K<3; K++)
          for(int L=0; L<3; L++)
          {
            Miso(I,J,K,L)=(Cinv(I,J)*Cinv(K,L)*(_lambda*Jac))-((_lambda*lnJ-_mu)*(Cinv(I,K)*Cinv(J,L)+Cinv(I,L)*Cinv(J,K)));
            Mtr(I,J,K,L)=4.*_beta*(Cinv(I,J)*_A(K)*_A(L)+Cinv(K,L)*_A(I)*_A(J))+8.*_gamma*_A(I)*_A(J)*_A(K)*_A(L)-2.*_beta*(I4-1.)*(Cinv(I,K)*Cinv(J,L)+Cinv(I,L)*Cinv(J,K));
            if(J==K) Mtr(I,J,K,L) -= _alpha*_A(I)*_A(L);
            if(I==K) Mtr(I,J,K,L) -= _alpha*_A(J)*_A(L);
            if(J==L) Mtr(I,J,K,L) -= _alpha*_A(I)*_A(K);
            if(I==L) Mtr(I,J,K,L) -= _alpha*_A(J)*_A(K);
          }
    for(int i=0; i<3; i++)
      for(int J=0; J<3; J++)
        for(int k=0; k<3; k++)
          for(int L=0; L<3; L++)
          {
            Tangent(i,J,k,L) = 0.;
            for(int M=0; M<3; M++)
              for(int N=0; N<3; N++)
                Tangent(i,J,k,L) += (Miso(M,J,N,L)+Mtr(M,J,N,L))*Fn(i,M)*Fn(k,N);

            if(i==k) Tangent(i,J,k,L) += (Siso(J,L)+Str(J,L));

          }
  }

}

double mlawTransverseIsotropic::deformationEnergy(const STensor3 &C) const
{
  double Jac= sqrt((C(0,0) * (C(1,1) * C(2,2) - C(1,2) * C(2,1)) -
          C(0,1) * (C(1,0) * C(2,2) - C(1,2) * C(2,0)) +
          C(0,2) * (C(1,0) * C(2,1) - C(1,1) * C(2,0))));
  double lnJ = log(Jac);
  double I1= C.trace();

  SVector3 CA(0.);
  for(int I=0; I<3; I++)
    for(int J=0; J<3; J++)
      CA(I) += C(I,J)*_A(J);

  double I4= dot(_A,CA);
  STensor3 CC=C;
  CC*=C;

  SVector3 CCA(0.);
  for(int I=0; I<3; I++)
    for(int J=0; J<3; J++)
      CCA(I) += CC(I,J)*_A(J);
  double I5= dot(_A,CCA);

  double PsyIso = (_alpha+_GA)*((I1-3)*0.5-lnJ)+0.5*_lambda*lnJ*lnJ;
  double PsyTr = (_alpha+2*_beta*lnJ+_gamma*(I4-1.))*(I4-1)-0.5*_alpha*(I5-1.);
  return (PsyIso+PsyTr);

}


