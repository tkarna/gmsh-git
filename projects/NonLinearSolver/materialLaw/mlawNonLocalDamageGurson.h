//
// C++ Interface: material law
//
// Description: Gurson elasto-plastic law with non local damage interface
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWNONLOCALDAMAGEGURSON_H_
#define MLAWNONLOCALDAMAGEGURSON_H_
#include "mlaw.h"
#include "ipNonLocalDamageGurson.h"
#include "j2IsotropicHardening.h"
#include "GursonDamageNucleation.h"
#include "CLengthLaw.h"
#include "STensor43.h"
#include "STensor63.h"

class mlawNonLocalDamageGurson : public materialLaw
{
 protected:
  CLengthLaw *_cLLaw;
  J2IsotropicHardening *_j2IH;
  GursonDamageNucleation *_gdn;

  double _rho; // density
  double _E; // young modulus
  double _nu; // Poisson ratio
  double _lambda; // 1st lame parameter
  double _mu; // 2nd lame parameter (=G)
  double _K; // bulk modulus
  double _K3; // 3*bulk modulus
  double _mu3; // 3*_mu = 3*G
  double _mu2; // 2*_mu = 2*G
  double _tol; // tolerance for iterative process
  double _perturbationfactor; // perturbation factor
  bool _tangentByPerturbation; // flag for tangent by perturbation  STensor43   Cel;

  STensor43   Cel;

  double _q1;
  double _q2;
  double _q3;
  double _fC;
  double _ff;
  double _ffstar;
 
  //initial porosity
  double _fVinitial;

  //tool
   STensor43 _I4;
   STensor3  _I;
   STensor43 _Idev;


 public:
  mlawNonLocalDamageGurson(const int num,const double E,const double nu, const double rho, 
                           const double q1, const double q2, const double q3, const double fC, 
                           const double ff, const double ffstar, const double fVinitial,
				const J2IsotropicHardening &j2IH, const CLengthLaw &cLLaw, const GursonDamageNucleation &gdn,
                                const double tol=1.e-6, const bool matrixbyPerturbation = false, const double pert = 1e-8);
 #ifndef SWIG
  mlawNonLocalDamageGurson(const mlawNonLocalDamageGurson &source);
  mlawNonLocalDamageGurson& operator=(const materialLaw &source);
  ~mlawNonLocalDamageGurson() 
  { 
    if(_j2IH!=NULL) delete _j2IH;
    if(_cLLaw!= NULL) delete _cLLaw;
    if(_gdn!=NULL) delete _gdn;
  }
  // function of materialLaw
  virtual matname getType() const{return materialLaw::nonLocalDamageGurson;}
  virtual void createIPState(IPNonLocalDamageGurson *ivi, IPNonLocalDamageGurson *iv1, IPNonLocalDamageGurson *iv2) const;
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const;
  virtual void createIPVariable(IPNonLocalDamageGurson *&ipv,const MElement *ele,const int nbFF) const;

  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
  virtual double soundSpeed() const; // default but you can redefine it for your case
  virtual const CLengthLaw *getCLengthLaw() const {return _cLLaw; };
  virtual const J2IsotropicHardening * getJ2IsotropicHardening() const {return _j2IH;}
  virtual const GursonDamageNucleation * getGursonDamageNucleation() const {return _gdn;}
  virtual double density()const{return _rho;}
  virtual const double bulkModulus() const{return _K;}
  virtual const double shearModulus() const{return _mu;}
  virtual const double poissonRatio() const{return _nu;}
  virtual const double getInitialPorosity() const {return _fVinitial;}
  // specific function
 public:
  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPNonLocalDamageGurson *q0,       // array of initial internal variable
                            IPNonLocalDamageGurson *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            const bool stiff            // if true compute the tangents
                           ) const;

  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPNonLocalDamageGurson *q0,       // array of initial internal variable
                            IPNonLocalDamageGurson *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            STensor3  &dLocalCorrectedPorosityDStrain,
  			    STensor3  &dStressDNonLocalCorrectedPorosity,
                            double    &dLocalCorrectedPorosityDNonLocalCorrectedPorosity,
                            const bool stiff            // if true compute the tangents
                           ) const;

  protected:
   virtual double deformationEnergy(const STensor3 &C) const ;
   virtual void logSTensor3(const STensor3& a, STensor3 &loga, STensor43 *dloga=NULL, STensor63 *ddloga=NULL) const;
   virtual void expSTensor3(const STensor3 &a,STensor3 &expa,STensor43 *dexpa=NULL,STensor63 *ddexpa=NULL) const;

   virtual void predictorCorrector(double tildefVstar, const STensor3& F1, const STensor3& Fp0_, 
                                         STensor3& Fp1_, const IPNonLocalDamageGurson *q0_,
                                         IPNonLocalDamageGurson *q1,   STensor3&P, double &fVstar, bool stiff, 
                                         STensor3 &dDeltaGammadC, STensor3 &dDeltaHatPdC, STensor3 &dtrNpdC, 
                                         STensor3 &dDeltafVdC, STensor43 &dNpDevdC, STensor43 &dKCordC,
 				         STensor43 &dFpdC,
                                         double &dDeltaGammadtildefVstar, double &dDeltaHatPdtildefVstar, 
                                         double &dtrNpdtildefVstar,
                                         double &dDeltafVdtildefVstar, STensor3 &dNpDevdtildefVstar, 
                                         STensor3 &dKCordtildefVstar,STensor3 &dFpdtildefVstar,
                                         STensor3 & Fe1, STensor3 & kCor, STensor43 &Le, STensor63 &dLe) const;
   virtual void KirchhoffStress(STensor3 &SigDev_, double &p, const STensor3 &E_) const;  
   virtual void yieldNormal(STensor3 &Npdev, double &trNp, const STensor3 &kDev_, double p, double yield, 
                                                       double tildefVstar) const;
   virtual double yieldFunction(const STensor3 &kDev_, double p, double yield, 
                                                       double tildefVstar) const;

   virtual void computeResidual(fullVector<double> &res, 
                                                       double tildefVstar, const STensor3 &kprDev, double ppr,
						       double yield, double yield0, double DeltaHatP, 
                                                       double DeltaGamma, double trNp, 
                                                       double fVn, double DeltafV, double _A) const;
   virtual void computeJacobian(fullMatrix<double> &J, 
                                                       double tildefVstar, const STensor3 &kprDev, double ppr,
						       double yield, double yield0, double h, double DeltaHatP, 
                                                       double DeltaGamma, double trNp, 
                                                       double fVn, double DeltafV, double _A) const;
   virtual void tangent_full_perturbation(STensor43 &T_,
                            STensor3  &dLocalCorrectedPorosityDStrain,
  			    STensor3  &dStressDNonLocalCorrectedPorosity,
                            double    &dLocalCorrectedPorosityDNonLocalCorrectedPorosity,
                            const STensor3 &P_,const STensor3 &F_,const STensor3 &Fp0_,const STensor3 &Fp1_, 
                            double tildefVstar, double fVstar, 
                            const IPNonLocalDamageGurson* q0_,const IPNonLocalDamageGurson* q1_) const;
   virtual void tangent_full_analytic(STensor43 &T_,
                            STensor3  &dLocalCorrectedPorosityDStrain,
  			    STensor3  &dStressDNonLocalCorrectedPorosity,
                            double    &dLocalCorrectedPorosityDNonLocalCorrectedPorosity,
                            const STensor3 &P_,const STensor3 &F_,const STensor3 &Fp0_,const STensor3 &Fp1_, 
                            double tildefVstar, double fVstar, 
                            const IPNonLocalDamageGurson* q0_,const IPNonLocalDamageGurson* q1_,
                            const STensor43 &dKCordC, const STensor43 &dFpdC, const STensor3 &dDeltafVdC,
                            const STensor3 &dKCordtildefVstar, const STensor3 &dFpdtildefVstar, 
                            const double &dDeltafVdtildefVstar,
                            const STensor3 & Fe1, const STensor3 & kCor, const STensor43 &Le, const STensor63 &dLe) const;

   virtual void computeDResidual(STensor3 &dres0dC, STensor3 &dres1dC, STensor3 &dres2dC, STensor3 &dres3dC,
                                                double &dres0dtildefVstar, double &dres1dtildefVstar, double &dres2dtildefVstar,
                                                double &dres3dtildefVstar, 
                                                double tildefVstar, const STensor3 &Fppr, const STensor3 &kprDev, double ppr,
						double yield, double h, double yield0, double DeltaHatP, 
                                                double DeltaGamma, double trNp, 
                                                double fVn, double DeltafV, const STensor43 &L) const;

   virtual void computeDInternalVariables(
                                                STensor3 &dDeltaGammadC, STensor3 &dDeltaHatPdC, STensor3 &dtrNpdC, 
                                                STensor3 &dDeltafVdC,
                                                double &dDeltaGammadtildefVstar, double &dDeltaHatPdtildefVstar, 
                                                double &dtrNpdtildefVstar,
                                                double &dDeltafVdtildefVstar, 
                                                double tildefVstar, const STensor3 &Fppr, const STensor3 &kprDev, double ppr,
						double yield, double h, double yield0, double DeltaHatP, 
                                                double DeltaGamma, double trNp, 
                                                double fVn, double DeltafV, const STensor43 &L, 
                                                const fullMatrix<double> &scaledJinv) const;
   virtual void computeDNpDevAndDKCorAndDFp(   STensor43 &dNpDevdC, STensor43 &dKCordC, STensor43 &dFpdC,
                                                        STensor3 &dNpDevdtildefVstar,
                                                        STensor3 &dKCordtildefVstar, STensor3 &dFpdtildefVstar,
                                                const STensor3 &dDeltaGammadC,const  STensor3 &dDeltaHatPdC,
                                                const STensor3 &dtrNpdC, const STensor3 &dDeltafVdC,
                                                double dDeltaGammadtildefVstar, double dDeltaHatPdtildefVstar, 
                                                double dtrNpdtildefVstar, double dDeltafVdtildefVstar, 
						double tildefVstar, const STensor3 &Fppr, const STensor3 &kprDev, double ppr,
						double yield, double h, double yield0, double DeltaHatP, 
                                                double DeltaGamma, double trNp, 
                                                double fVn, double DeltafV, const STensor43 &Lpr, const STensor43 &ENp) const;
 #endif // SWIG
};

#endif // MLAWNONLOCALDAMAGEGURSON_H_
