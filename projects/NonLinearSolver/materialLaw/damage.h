#ifndef DAMAGE_H
#define DAMAGE_H 1

#ifdef NONLOCALGMSH
namespace MFH {
#endif

//Generic material
class Damage{

	protected:
		int dammodel;
		int ndamprops;
		int nsdv;
		
	public:
		
		Damage(double* props, int iddam){
			dammodel = (int) props[iddam];
		}
		virtual ~Damage(){ ; }
		inline int get_dammodel(){  return dammodel; }
		inline int get_nsdv() {	return nsdv; } 
		
		//print the damage properties
		virtual void print()=0;

		//compute the constitutive response of the material
		//INPUT: dstrn, strs_n, statev_n, kinc, kstep
		//OUTPUT: strs, statev, Cref, dCref, Calgo
		virtual void damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdp_bar, double alpha, int kinc, int kstep)=0;


};

//Linear elastic material
class LC_Damage : public Damage{

	private:
		double S0,n;

	public:
		
		LC_Damage(double* props, int iddam);
		~LC_Damage();
		
		virtual void print();
		virtual void damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdp_bar, double alpha, int kinc, int kstep);

		
		
}; 


////////////////////////////////////////////////
/////////////new defined ling Wu 5/4/2011.
////////////////////////////////////////////////


//LEMAITRE-CHABOCHE DAMAGE MODEL (nonlocal)
//for nonlocal MT type 
//PROPS: ID, S0, n
//STATEV: D,dD,p_bar,dp_bar, plastic strn (6)
//
// output: dD increment of D in statev
//         dDdE, dDdp_bar D w.r.t strain and p_bar respectivly
//****************************************
class LCN_Damage : public Damage{

	private:
		double S0,n, p0;

	public:
		
		LCN_Damage(double* props, int iddam);
		~LCN_Damage();
		
		virtual void print();
		virtual void damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdp_bar, double alpha, int kinc, int kstep);

		
		
}; 



//Linear DAMAGE MODEL (nonlocal)
//for nonlocal MT type 
//PROPS: ID, p0, pc
//STATEV: D,dD,p_bar,dp_bar, plastic strn (6)
//
// output: dD increment of D in statev
//         dDdE, dDdp_bar D w.r.t strain and p_bar respectivly
//****************************************
class LINN_Damage : public Damage{

	private:
		double p0,pc;

	public:
		
		LINN_Damage(double* props, int iddam);
		~LINN_Damage();
		
		virtual void print();
		virtual void damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdp_bar, double alpha, int kinc, int kstep);

		
		
}; 




//Exponential DAMAGE MODEL (nonlocal)
//for nonlocal MT type 
//PROPS: ID, Beta
//STATEV: D,dD,p_bar,dp_bar, plastic strn (6)
//
// output: dD increment of D in statev
//         dDdE, dDdp_bar D w.r.t strain and p_bar respectivly
//****************************************
class EXPN_Damage : public Damage{

	private:
		double Beta;

	public:
		
		EXPN_Damage(double* props, int iddam);
		~EXPN_Damage();
		
		virtual void print();
		virtual void damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdp_bar, double alpha, int kinc, int kstep);

		
		
}; 

///ling Wu 5/4/2011.



//***********************************
//*******Power_law DAMAGE MODEL (nonlocal)
//for nonlocal MT type 
//PROPS: ID, Alpha, Beta, p0,pc, Dc
//STATEV: D,dD,p_bar,dp_bar, plastic strn (6)
// **here, p is no longer the accumulate plastic strian, it is a defined equvialent strain. 
// output: dD increment of D in statev
//         dDdE, dDdp_bar D w.r.t strain and p_bar respectivly
//****************************************
class PLN_Damage : public Damage{

	private:
		double Alpha, Beta;
                double p0, pc;
		double Dc;

	public:
		
		PLN_Damage(double* props, int iddam);
		~PLN_Damage();
		
		virtual void print();
		virtual void damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdp_bar, double alpha, int kinc, int kstep);

		
}; 

////////////////////////////////////////////////
//////////end of new defined ling Wu 30/11/2012.
////////////////////////////////////////////////




#ifdef NONLOCALGMSH
}
#endif


#endif
