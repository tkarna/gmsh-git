//
// C++ Interface: material law
//
// Description: TransverseIsotropic law
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "mlawAnisotropic.h"
#include <math.h>
#include "MInterfaceElement.h"


mlawAnisotropic::mlawAnisotropic(const int num,const double rho,
					const double Ex, const double Ey, const double Ez, 
					const double Vxy, const double Vxz, const double Vyz,
				        const double MUxy, const double MUxz, const double MUyz,
				        const double alpha, const double beta, const double gamma) :
				materialLaw(num,true),_rho(rho),_alpha(alpha),_beta(beta),_gamma(gamma)
{
	const double Vyx= Vxy*Ey/Ex  ; 
	const double Vzx= Vxz*Ez/Ex  ; 
	const double Vzy= Vyz*Ez/Ey  ;
	const double D=( 1-Vxy*Vyx-Vzy*Vyz-Vxz*Vzx-2*Vxy*Vyz*Vzx ) / ( Ex*Ey*Ez );

 	STensor43 ElasticityTensor; 
 	ElasticityTensor(0,0,0,0)=( 1-Vyz*Vzy ) / (Ey*Ez*D );
 	ElasticityTensor(1,1,1,1)=( 1-Vxz*Vzx ) / (Ex*Ez*D );
	ElasticityTensor(2,2,2,2)=( 1-Vyx*Vxy ) / (Ey*Ex*D );

	ElasticityTensor(0,0,1,1)=( Vyx+Vzx*Vyz ) / (Ey*Ez*D );
	ElasticityTensor(0,0,2,2)=( Vzx+Vyx*Vzy ) / (Ey*Ez*D );
	ElasticityTensor(1,1,2,2)=( Vzy+Vxy*Vzx ) / (Ex*Ez*D );

	ElasticityTensor(1,1,0,0)=( Vxy+Vzy*Vxz ) / (Ex*Ez*D );
	ElasticityTensor(2,2,0,0)=( Vxz+Vzy*Vyz ) / (Ey*Ex*D );
	ElasticityTensor(2,2,1,1)=( Vyz+Vxz*Vyx ) / (Ey*Ex*D );

 	ElasticityTensor(1,2,1,2)=MUyz;ElasticityTensor(1,2,2,1)=MUyz;
	ElasticityTensor(2,1,2,1)=MUyz;ElasticityTensor(2,1,1,2)=MUyz;

	ElasticityTensor(0,1,0,1)=MUxy;ElasticityTensor(0,1,1,0)=MUxy;
	ElasticityTensor(1,0,1,0)=MUxy;ElasticityTensor(1,0,0,1)=MUxy;

	ElasticityTensor(0,2,0,2)=MUxz;ElasticityTensor(0,2,2,0)=MUxz;
	ElasticityTensor(2,0,2,0)=MUxz;ElasticityTensor(2,0,0,2)=MUxz;

    	STensor3 R;		//3x3 rotation matrix

	double c1,c2,c3,s1,s2,s3;
	double s1c2, c1c2;
	double pi(3.14159265359);
    	double fpi = pi/180.;

	c1 = cos(_alpha*fpi);
	s1 = sin(_alpha*fpi);

	c2 = cos(_beta*fpi);
	s2 = sin(_beta*fpi);

	c3 = cos(_gamma*fpi);
	s3 = sin(_gamma*fpi);

	s1c2 = s1*c2;
	c1c2 = c1*c2;

	R(0,0) = c3*c1 - s1c2*s3;
	R(0,1) = c3*s1 + c1c2*s3;
	R(0,2) = s2*s3;

	R(1,0) = -s3*c1 - s1c2*c3;
	R(1,1) = -s3*s1 + c1c2*c3;
	R(1,2) = s2*c3;

	R(2,0) = s1*s2;
	R(2,1) = -c1*s2;
	R(2,2) = c2;

  	for(int i=0;i<3;i++)
  	 {
    	  for(int j=0;j<3;j++)
    	   {
      	    for(int k=0;k<3;k++)
      	     {
              for(int l=0;l<3;l++)
               {
          	_ElasticityTensor(i,j,k,l)=0.;
          	 for(int m=0;m<3;m++)
          	  {
            	   for(int n=0;n<3;n++)
            	    {
              	     for(int o=0;o<3;o++)
              	      {
                       for(int p=0;p<3;p++)
                        {
                  	 _ElasticityTensor(i,j,k,l)+=R(m,i)*R(n,j)*R(o,k)*R(p,l)*ElasticityTensor(m,n,o,p);
                	}
              	      }
           	    }
         	  }
       	        }
     	      }
   	    }
	  } 

	//ElasticityTensor=_ElasticityTensor;
	
	//Init du _poissonMax
	if (Vxy >=  Vxz && Vxy >= Vyz)
		{_poissonMax=Vxy;}
	else if (Vxz >=  Vyz && Vxz >= Vxy)
		{_poissonMax=Vxz;}
	else
		{_poissonMax=Vyz;}
        //Msg::Info("K %e %e %e", _ElasticityTensor(0,0,0,0), _ElasticityTensor(1,1,1,1),_ElasticityTensor(2,2,2,2));
} 
mlawAnisotropic::mlawAnisotropic(const mlawAnisotropic &source) : materialLaw(source),_ElasticityTensor(source._ElasticityTensor),
                                                       _rho(source._rho),_poissonMax(source._poissonMax),_alpha(source._alpha),_beta(source._beta),_gamma(source._gamma)
{

}


mlawAnisotropic& mlawAnisotropic::operator=(const materialLaw &source)
{
  materialLaw::operator=(source);
  const mlawAnisotropic* src =static_cast<const mlawAnisotropic*>(&source);
  _ElasticityTensor = src->_ElasticityTensor;
  _rho = src->_rho;
  _alpha = src->_alpha;
  _beta = src->_beta;
  _gamma = src->_gamma;
  return *this;
}

void mlawAnisotropic::createIPState(IPStateBase* &ips,const bool* state_,const MElement *ele, const int nbFF_, const int gpt) const
{
  bool inter=true;
  const MInterfaceElement *iele = dynamic_cast<const MInterfaceElement*>(ele);
  if(iele==NULL) inter=false;
  IPVariable* ipvi = new IPAnisotropic();
  IPVariable* ipv1 = new IPAnisotropic();
  IPVariable* ipv2 = new IPAnisotropic();
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}

double mlawAnisotropic::poissonRatio() const
{
	return _poissonMax;
}

double mlawAnisotropic::shearModulus() const
{
	if (_ElasticityTensor(1,2,1,2) >=  _ElasticityTensor(0,1,0,1) && _ElasticityTensor(1,2,1,2) >= _ElasticityTensor(2,0,2,0))
		{return _ElasticityTensor(1,2,1,2);}
	else if (_ElasticityTensor(0,1,0,1) >=  _ElasticityTensor(2,0,2,0) && _ElasticityTensor(0,1,0,1) >= _ElasticityTensor(1,2,1,2))
		{return _ElasticityTensor(0,1,0,1);}
	else
		{return _ElasticityTensor(2,0,2,0);}

}

double mlawAnisotropic::soundSpeed() const
{
  double nu=poissonRatio();
  double factornu=(1.-nu) / ( (1.+nu) * (1.-2.*nu) ) ;
  double E(0.);

	if (_ElasticityTensor(0,0,0,0) >=  _ElasticityTensor(1,1,1,1) && _ElasticityTensor(0,0,0,0) >= _ElasticityTensor(2,2,2,2))
		{double E=_ElasticityTensor(0,0,0,0);}
	else if (_ElasticityTensor(1,1,1,1) >=  _ElasticityTensor(2,2,2,2) && _ElasticityTensor(1,1,1,1) >= _ElasticityTensor(0,0,0,0))
		{double E=_ElasticityTensor(1,1,1,1);}
	else
		{double E=_ElasticityTensor(2,2,2,2);}	

  return sqrt(E*factornu/_rho);
}


void mlawAnisotropic::constitutive(const STensor3& F0,const STensor3& Fn,STensor3 &P,
                              const IPAnisotropic *q0, IPAnisotropic *q1,STensor43 &Tangent,const bool stiff) const
{
  

    STensor3 FnT=Fn.transpose();

    STensor3 defo(FnT); // static
    defo+=Fn;
    defo*=0.5;
    defo(0,0)-=1.; 
    defo(1,1)-=1.; 
    defo(2,2)-=1.; 

    P*=0.;
    for(int i=0;i<3;i++)
    {
      for(int j=0;j<3;j++)
      {
        for(int k=0;k<3;k++)
        {
          for(int l=0;l<3;l++)
          {
            P(i,j)+=_ElasticityTensor(i,j,k,l)*defo(k,l);
          }
        }
      }
    }

    q1->_elasticEnergy=deformationEnergy(defo,P);
    if(stiff)
      Tangent=_ElasticityTensor;
    
}

double mlawAnisotropic::deformationEnergy(STensor3 defo) const // If i need to compute sigma
{
    STensor3 sigma;
    sigma*=0.;
    //These 'fors' can be avoided with the surcharged function 
    for(int i=0;i<3;i++)
    {
      for(int j=0;j<3;j++)
      {
        for(int k=0;k<3;k++)
        {
          for(int l=0;l<3;l++)
          {
            sigma(i,j)+=_ElasticityTensor(i,j,k,l)*defo(k,l);
          }
        }
      }
    }

  defo.transpose();
  defo*=sigma; 				//eps^T * sigma 
  double En=defo.trace();

  return 0.5*En;  			// W=0.5*trace(epsilon^T*sigma)

}

double mlawAnisotropic::deformationEnergy(STensor3 defo, const STensor3& sigma) const // If i have sigma ...
{

  defo.transpose();
  defo*=sigma; 				//eps^T * sigma 
  double En=defo.trace();

  return 0.5*En;  			// W=0.5*trace(epsilon^T*sigma)

}

