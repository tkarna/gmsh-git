//
// C++ Interface: terms
//
// Description: Define Gurson nucleation
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef _GURSONDAMAGENUCLEATION_H_
#define _GURSONDAMAGENUCLEATION_H_
#ifndef SWIG
#include "ipstate.h"
#include "MElement.h"
#include "ipGursonNucleation.h"
#endif
class GursonDamageNucleation{
 public :
  enum nucleationname{linearDamageNucleationRate,exponentialDamageNucleationRate};
 protected :
  int _num; // number of law (must be unique !)
  bool _initialized; // to initialize law
 public:
  // constructor
#ifndef SWIG
  GursonDamageNucleation(const int num, const bool init=true);
  ~GursonDamageNucleation(){}
  GursonDamageNucleation(const GursonDamageNucleation &source);
  GursonDamageNucleation& operator=(const GursonDamageNucleation &source);
  virtual int getNum() const{return _num;}
  virtual nucleationname getType() const=0;
  virtual void createIPVariable(IPGursonNucleation* &ipv) const=0;
  virtual void initLaws(const std::map<int,GursonDamageNucleation*> &maplaw)=0;
  virtual const bool isInitialized() {return _initialized;}
  virtual void rate(double p, double p0, IPGursonNucleation &ipv) const=0;
  virtual GursonDamageNucleation * clone() const=0;
#endif
};

class LinearGursonDamageNucleation : public GursonDamageNucleation
{
 protected:
  double _A0;
 public :
  // constructor
  LinearGursonDamageNucleation(const int num, double A0, const bool init=true);
  ~LinearGursonDamageNucleation(){}
#ifndef SWIG
  LinearGursonDamageNucleation(const LinearGursonDamageNucleation &source);
  LinearGursonDamageNucleation& operator=(const GursonDamageNucleation &source);
  virtual int getNum() const{return _num;}
  virtual nucleationname getType() const{return GursonDamageNucleation::linearDamageNucleationRate; };
  virtual void createIPVariable(IPGursonNucleation* &ipv) const;
  virtual void initLaws(const std::map<int,GursonDamageNucleation*> &maplaw) {}; //nothing now, we will see if we use the mapping
   virtual void rate(double p, double p0, IPGursonNucleation &ipv) const;
  virtual GursonDamageNucleation * clone() const;
#endif
};

class ExponentialGursonDamageNucleation : public GursonDamageNucleation
{
 protected:
  double _fn;
  double _sn;
  double _epsilonn;
 public :
  // constructor
  ExponentialGursonDamageNucleation(const int num, double fn, double sn, double epsilonn, const bool init=true);
  ~ExponentialGursonDamageNucleation(){}
#ifndef SWIG
  ExponentialGursonDamageNucleation(const ExponentialGursonDamageNucleation &source);
  ExponentialGursonDamageNucleation& operator=(const GursonDamageNucleation &source);
  virtual int getNum() const{return _num;}
  virtual nucleationname getType() const{return GursonDamageNucleation::exponentialDamageNucleationRate; };
  virtual void createIPVariable(IPGursonNucleation* &ipv) const;
  virtual void initLaws(const std::map<int,GursonDamageNucleation*> &maplaw) {}; //nothing now, we will see if we use the mapping
   virtual void rate(double p, double p0,IPGursonNucleation &ipv) const;
  virtual GursonDamageNucleation * clone() const;
#endif
};

#endif //GURSONDAMAGENUCLEATION
