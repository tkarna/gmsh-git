#ifndef HOMOGENIZATION_2RD_C
#define HOMOGENIZATION_2RD_C 1

#ifdef NONLOCALGMSH
#include "homogenization_2rd.h"
#include "matrix_operations.h"
#include "rotations.h"
#include "lccfunctions.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "ID.h"
#include "material.h"
#include "homogenization.h"
using namespace MFH;
#endif


//*******************************************************************************************************

int homogenize_2rd(int sch, double* DE, Material* mtx_mat, Material* icl_mat, double vfM, double vfI, double* ar, double* angles, double* statev_n, double* statev, int nsdv, int idsdv_m, int idsdv_i, Lcc* LCC, double** Calgo, double* dpdE, double* strs_dDdp_bar,double *Sp_bar, int kinc, int kstep){
		
	return 0;

} //end homogenize
//*****************************************************
//NEWTON-RAPHSON: COMPUTE RESIDUAL and JACOBIAN MATRIX  
//*****************************************************

int preNR(double* DE, double* mu0, Material* mtx_mat, Material* icl_mat, double vfI, double* ar, double* angles, double* statev_n, double* statev, int idsdv_m, int idsdv_i, Lcc* LCC, int last, double* F, double** J, int* indx, int* neq, int solver, double** Calgo, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, int kinc, int kstep){

}	//end function


//************************
//COMPUTE SCALAR RESIDUAL 
//************************

double scalar_res(double* F,int neq, double norm){

	int i;
	double res=0.0;
	for(i=0;i<neq;i++){
		res += F[i]*F[i];
	}
	return sqrt(res)/norm;

}

//***************************
//SOLVE NEWTON-RAPHSON SYSTEM 
//***************************

void solveNR(double* F,double** J,double** invJ, double* DX, int* indx, int Nmax, int neq){

	int error;
	int i,j,k;
    
        for(i=0;i<Nmax;i++){
	   DX[i]=0.0;
        }

	inverse(J,invJ,&error,neq);

	//correction vector	
	for (i=0;i<neq;i++){
		k=indx[i];
		for(j=0;j<neq;j++){
			DX[k] -= invJ[i][j]*F[j];
		}
	}
}


//************************
//UPDATE UNKNOWNS VECTOR 
//************************
void updateNR(double* X, double* dX, int N, double factor){

	int i;

	for(i=0;i<N;i++){
		X[i] += dX[i]*factor;	
	}
}


#endif  
  
