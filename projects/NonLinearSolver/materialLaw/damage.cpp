#ifndef DAMAGE_CC
#define DAMAGE_CC 1

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "damage.h"
#include "matrix_operations.h"
#ifdef NONLOCALGMSH
using namespace MFH;
#endif

//****************************************
//LEMAITRE-CHABOCHE DAMAGE MODEL (1985)
//for a pseudo-grain of MT type 
//PROPS: ID, S0, n
//STATEV: D, p, plastic strn (6)
//****************************************
//constructor
LC_Damage::LC_Damage(double* props, int iddam): Damage(props, iddam){

	nsdv = 8;
	S0 = props[iddam+1];
	n = props[iddam+2];
}

//destructor
LC_Damage::~LC_Damage(){
	;
}

//print
void LC_Damage::print(){

	printf("Lemaitre-Chaboche damage law\n");
	printf("S0: %lf, n: %lf\n", S0, n);
	
}

//damagebox
void LC_Damage::damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdp_bar, double alpha, int kinc, int kstep){

//empty
}




///******************************************************************************************************************************************************************
////////////////////////////////////////////////
/////////////new defined ling Wu 6/4/2011.
////////////////////////////////////////////////


//LEMAITRE-CHABOCHE DAMAGE MODEL (nonlocal)
//for nonlocal MT type 
//PROPS: ID, S0, n
//STATEV: D,dD,p_bar,dp_bar, plastic strn (6)
//
// output: dD increment of D in statev
//         dDdE, dDdp_bar D w.r.t strain and p_bar respectivly
//****************************************
//constructor
LCN_Damage::LCN_Damage(double* props, int iddam):Damage(props, iddam){

	nsdv = 5;                                     //p_bar dp_bar D dD, Max_p_bar,         Max_p_bar keep the highest history value of p_bar
	S0 = props[iddam+1];
	n = props[iddam+2];
        p0 = props[iddam+3];
}

//destructor
LCN_Damage::~LCN_Damage(){
	;
}

//print
void LCN_Damage::print(){

	printf("Lemaitre-Chaboche nonlocaldamage law\n");
	printf("S0: %lf, n: %lf\n", S0, n);
	
}

//damagebox
void LCN_Damage::damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdp_bar, double alpha, int kinc, int kstep){

	//  statev for damage  p_bar, dp_bar ,D, dD

        double Y_n, Y, Y_a;        

        double p_bar, dp_bar, p_bar_n;
        double D, dD, D_n;
        double Max_p_bar;

        // allocated once
        static double* strs_n;
        static double* strs;
        static double* Estrn_n;
        static double* Estrn;

        static double* strn;
        static double* pstrn;

        int i, j;
        double mat1;
        
        static bool initialized = false; 

        //allocate memory
	if(!initialized){
          mallocvector(&strs_n,6);
          mallocvector(&strs,6);
          mallocvector(&Estrn_n,6);
          mallocvector(&Estrn,6);

          mallocvector(&strn,6);
          mallocvector(&pstrn,6);
 
          initialized = true;
        }
  
        p_bar_n= statev_n[pos_dam];                       
        dp_bar= statev_n[pos_dam+1];
               
        D_n= statev_n[pos_dam+2];
        Max_p_bar = statev_n[pos_dam+4];


        p_bar= p_bar_n+dp_bar;  

        dp_bar = p_bar - Max_p_bar;  

       if(p_bar >= Max_p_bar){

             Max_p_bar= p_bar;                    
        }
        
        
        if(Max_p_bar > p0){ 

                 copyvect(&(statev_n[pos_strs]),strs_n,6);
                 copyvect(&(statev[pos_strs]),strs,6);
                 
                 copyvect(&(statev_n[pos_strn]),strn,6);
                 copyvect(&(statev_n[pos_strn+18]),pstrn,6);
                 addtens2(1., strn, -1., pstrn, Estrn_n);

                 copyvect(&(statev[pos_strn]),strn,6);
                 copyvect(&(statev[pos_strn+18]),pstrn,6);
                 addtens2(1., strn, -1., pstrn, Estrn);

                 Y_n= 0.5*fabs(contraction22(strs_n, Estrn_n));
                 Y= 0.5*fabs(contraction22(strs, Estrn));

                 Y_a= (1.-alpha)*Y_n + alpha*Y;

                 
         //  compute dDdp_bar 
                 *dDdp_bar=pow(Y_a/S0, n);


                 if(dp_bar>0.0){
                      
                      dD= dp_bar*pow(Y_a/S0, n);

          //  compute dDdE 
                      mat1= alpha*n*dD/Y_a;

                      cleartens2(dDdE);

                      for(i=0;i<6;i++){
                          for (j=0; j<6; j++) {
                               dDdE[i] = dDdE[i] + Estrn[j]*Calgo[j][i];
                          }
                          dDdE[i]= mat1*dDdE[i];  
                      }
                 } 
                 else{
                      dD=0.;
                      cleartens2(dDdE);
                 }

                 D= D_n + dD;
       }
        else {
                   D=D_n;
                   dD=0.0;
  
                   *dDdp_bar= 0.0; 
                   cleartens2(dDdE);

        }

        if( D > 0.99){
                   D=0.99;
                  dD=0.0;
                   *dDdp_bar= 0.0; 
                   cleartens2(dDdE);
        }
          

       // update damage variable in statev
         statev[pos_dam]= p_bar;
	 statev[pos_dam+2]= D;
	 statev[pos_dam+3]= dD;
	 statev[pos_dam+4]= Max_p_bar;   


	//free memory allocated once
//       free(strs_n);
//       free(strs);
//       free(Estrn_n);
//       free(Estrn);

//        free(strn);
//        free(pstrn);         
        
        		
}


///******************************************************************************************************************************************************************
//Linear DAMAGE MODEL (nonlocal)
//for nonlocal MT type 
//PROPS: ID, p0, pc
//STATEV: D,dD,p_bar,dp_bar, plastic strn (6)
//
// output: dD increment of D in statev
//         dDdE, dDdp_bar D w.r.t strain and p_bar respectivly
//****************************************
//constructor
LINN_Damage::LINN_Damage(double* props, int iddam):Damage(props, iddam){

	nsdv = 5;                                     //p_bar dp_bar D dD, Max_p_bar,         Max_p_bar keep the highest history value of p_bar
	p0 = props[iddam+1];
	pc = props[iddam+2];
}

//destructor
LINN_Damage::~LINN_Damage(){
	;
}

//print
void LINN_Damage::print(){

	printf("Linear nonlocaldamage law\n");
	printf("p0: %lf, pc: %lf\n", p0, pc);
	
}
//damagebox

void LINN_Damage::damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdp_bar, double alpha, int kinc, int kstep){

//  statev for damage  p_bar, dp_bar ,D, dD

         double p_bar, dp_bar, p_bar_n;
         double D, dD, D_n;
         double Max_p_bar;

                 p_bar_n= statev_n[pos_dam];                       
                 dp_bar= statev_n[pos_dam+1];
                 D_n= statev_n[pos_dam+2];
                 Max_p_bar = statev_n[pos_dam+4];

                 p_bar= p_bar_n+dp_bar;
         
           dp_bar =  p_bar -Max_p_bar;
           
           if (dp_bar >= 0) {
                Max_p_bar = p_bar;

                if(p_bar > p0){ 

                    D=(p_bar-p0)/(pc-p0);    
                    dD=D-D_n;
                 
                    *dDdp_bar= 1.0/(pc-p0);
             
        	}
            }
	    else {
                   D=D_n;
                   dD=0.0;
  
                   *dDdp_bar= 0.0; 

            }


// update damage variable in statev
         statev[pos_dam]= p_bar;
	 statev[pos_dam+2]= D;
	 statev[pos_dam+3]= dD;
	 statev[pos_dam+4]= Max_p_bar;

         cleartens2(dDdE);
         
}




///******************************************************************************************************************************************************************
//Exponential DAMAGE MODEL (nonlocal)
//for nonlocal MT type 
//PROPS: ID, Beta
//STATEV: D,dD,p_bar,dp_bar, plastic strn (6)
//
// output: dD increment of D in statev
//         dDdE, dDdp_bar D w.r.t strain and p_bar respectivly
//****************************************
//constructor
EXPN_Damage::EXPN_Damage(double* props, int iddam):Damage(props,iddam){

	nsdv = 5;                                     //p_bar dp_bar D dD,  Max_p_bar,         Max_p_bar keep the highest history value of p_bar
	Beta = props[iddam+1];
}

//destructor
EXPN_Damage::~EXPN_Damage(){
	;
}
//print
void EXPN_Damage::print(){

	printf("Exponential nonlocaldamage law\n");
	printf("Beta: %lf\n", Beta);
	
}
//damagebox

void EXPN_Damage::damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdp_bar, double alpha, int kinc, int kstep){

//  statev for damage  p_bar, dp_bar ,D, dD

         double p_bar, dp_bar, p_bar_n;
         double Max_p_bar;
         double D, dD, D_n;
         double para;

                 p_bar_n= statev_n[pos_dam];                       // p_bar=p_bar_n+dp_bar
                 dp_bar= statev_n[pos_dam+1];
                
                 D_n= statev_n[pos_dam+2];
                 Max_p_bar = statev_n[pos_dam+4];


                 p_bar= p_bar_n+dp_bar;
         
            dp_bar =  p_bar -Max_p_bar;

            if(dp_bar >= 0.0){ 
                    
                    Max_p_bar= p_bar;

                    para= -Beta*p_bar;
                    D=1-exp(para);    
                    dD=D-D_n;
             
        	}
	    else{
                    D=D_n;
                    dD=0.0;
                 }  
// update damage variable in statev
         statev[pos_dam]= p_bar;
	 statev[pos_dam+2]= D;
	 statev[pos_dam+3]= dD;
	 statev[pos_dam+4]= Max_p_bar;


         cleartens2(dDdE);
         *dDdp_bar= Beta*(1.0-D_n-alpha*dD);

}


	

////////////////////////////////////////////////
//////////end of new defined ling Wu 11/4/2011.
////////////////////////////////////////////////


//********************************************************************************************************************************
//Power_law DAMAGE MODEL (nonlocal) ling Wu 30/11/2012
// PROPS: ID, Alpha, Beta, p0,pc
//STATEV: D,dD,p_bar,dp_bar, p (equivalent strain)
// **here, p is no longer the accumulate plastic strian, it is a defined equivalent strain. 
//
// output: dD increment of D in statev
//         dDdE, dDdp_bar D w.r.t strain and p_bar respectivly
//****************************************
//Note:
//  statev order for damage: p_bar, dp_bar ,D, dD, p
//  for this damage law, the special equivalent strain p is computered in this box, 
//  and its nonlocal value p_bar is used for damage.
//***Here we use dDdE to give back the derivatives of dpdE, which is only for this special defined damage law
//constructor
PLN_Damage::PLN_Damage(double* props, int iddam):Damage(props,iddam){

	nsdv = 6;                                     //p_bar dp_bar D dD, p, Max_p_bar,         Max_p_bar keep the highest history value of p_bar
	Alpha = props[iddam+1];
        Beta = props[iddam+2];
        p0 = props[iddam+3];
        pc = props[iddam+4];
	Dc = props[iddam+5];
	if(Dc>1.) Dc=0.999;
}

//destructor
PLN_Damage::~PLN_Damage(){
	;
}
//print
void PLN_Damage::print(){

	printf("Power_Law nonlocaldamage law\n");
	printf("Alpha: %lf,Beta: %lf,p0: %lf,pc: %lf\n", Alpha, Beta, p0,pc);
	
}
//damagebox

void PLN_Damage::damagebox(double* statev_n, double* statev, int pos_strn, int pos_strs, int pos_dam, double** Calgo,double* dDdE, double *dDdp_bar, double alpha, int kinc, int kstep){

         double p, p_bar, dp_bar, p_bar_n;
         double Max_p_bar;
         double D, dD, D_n;
         double para1, para2, sq2;
      
         // for euivalent strain  
         int i, j, k;
         static double* strn = NULL; 
         static double** S = NULL;
         static double* e = NULL;
         static double** U = NULL;
         static double*** dedS = NULL;

         static double** dpdS = NULL;

         if(strn == NULL)
         {
           mallocvector(&strn,6);
	   mallocmatrix(&S,3,3);
           mallocvector(&e,3);
	   mallocmatrix(&U,3,3);
	   malloctens3(&dedS,3,3,3);
	   mallocmatrix(&dpdS,3,3);
         }

// compute equivalent strain p 
    //****** before calling function Jacobian_Eigen a sembly of matrix from strain tensor is used
    //******** strain: (e11,e22,e33,sq2*e12,sq2*e13,sq2*e23)
         sq2 = sqrt(2.);
         cleartens2(dDdE);

         copyvect(&(statev[pos_strn]), strn, 6);
         for (i=0; i<3; i++){
            S[i][i] = strn[i];
            e[i] = 0.;
         }
          S[0][1] = S[1][0] = strn[3]/sq2;
          S[0][2] = S[2][0] = strn[4]/sq2;
          S[1][2] = S[2][1] = strn[5]/sq2;

    //**compute principle strain and keep in e ************************
          Jacobian_Eigen( 3, S, e, U, dedS);
  
          para1 = 0.0;
          for (int i=0; i<3; i++){
               if(e[i] > 0.) { 
                    para1 = para1 + e[i]*e[i]; 
                }
          }
          p = sqrt(para1);

//********* compute damage**********************

                 p_bar_n = statev_n[pos_dam];                       // p_bar=p_bar_n+dp_bar
                 dp_bar = statev_n[pos_dam+1];
                 D_n = statev_n[pos_dam+2];
                 Max_p_bar = statev_n[pos_dam+5];
       

#ifdef NONLOCALGMSH
#else
// add by wu ling try nonlocal mehod without finite element cacilation
        
                 dp_bar = p - statev_n[pos_dam+4];
#endif

                 p_bar= p_bar_n+dp_bar;    
                
            if(p_bar >= p0 && p_bar >= Max_p_bar)
            {
               Max_p_bar = p_bar;

               if(pc > Max_p_bar)
                    {
                      para1 = pow((pc - Max_p_bar)/(pc-p0), Alpha);
                      para2 = pow(p0/Max_p_bar, Beta);
                      D = 1.0 - para2 * para1;
                      *dDdp_bar= Beta*para2/ Max_p_bar * para1 + para2 * Alpha * pow((pc- Max_p_bar), (Alpha-1.0))/pow((pc-p0), Alpha);
                      if(D > Dc)
                      {
                        D=Dc;
                        *dDdp_bar = 0;
                      }
                    }
                    else if (pc <= Max_p_bar)
                    { 
                      D = Dc;
                      *dDdp_bar= 0.;
                    }
                    dD=D-D_n;
            }

            else {
                   D=D_n;
                   dD=0.0;
  
                   *dDdp_bar= 0.0; 

            }







// update damage variable in statev
         statev[pos_dam]= p_bar;
	 statev[pos_dam+2]= D;
	 statev[pos_dam+3]= dD;
	 statev[pos_dam+5]= Max_p_bar;


// compute equivalent strain p's derivatives to strain dpdE which is keept in dDdE
    //****** before calling function Jacobian_Eigen a sembly of matrix from strain tensor is used
    //******** strain: (e11,e22,e33,sq2*e12,sq2*e13,sq2*e23)

         cleartens2(dDdE);
   
         for (int j=0; j<3; j++){
                for (int k=0; k<3; k++){
                       dpdS[j][k] = 0.0;
                }
          }

          for (int i=0; i<3; i++){
               if(e[i] > 0.) { 
                      for (int j=0; j<3; j++){
                            for (int k=0; k<3; k++){
                            dpdS[j][k] = dpdS[j][k] + e[i]/p*dedS[i][j][k];
                            }
                      }
                } 
          }
      // here we use dDdE to give back dpdE*****
          
          dDdE[0] = dpdS[0][0];
          dDdE[1] = dpdS[1][1];
          dDdE[2] = dpdS[2][2];

          dDdE[3] = (dpdS[0][1]+dpdS[1][0])/sq2;
          dDdE[4] = (dpdS[0][2]+dpdS[2][0])/sq2;
          dDdE[5] = (dpdS[1][2]+dpdS[2][1])/sq2;

     // update damage variable in statev
	 statev[pos_dam+4]= p;

}



#endif
