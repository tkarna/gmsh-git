//
// C++ Interface: materialLaw
//
// Description: j2 law for small strains
//
//
// Author:  <Gauthier BECKER>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "mlawJ2SmallStrains.h"
#include "ipJ2SmallStrains.h"

linearHardeningLaw::linearHardeningLaw(const double hard) : hardeningLaw(), _hardening(hard){}

linearHardeningLaw::linearHardeningLaw(const linearHardeningLaw &source) : hardeningLaw(source), _hardening(source._hardening){}

void linearHardeningLaw::hard(const double plastic_n, const double sy0, double &R, double &dR, double &ddR) const
{
  if(_hardening==0.) //perfectly plastic
  {
    R = 0.; dR = 0.; ddR = 0.; return;
  }
  R = _hardening*plastic_n;
  dR = _hardening;
  ddR = 0.;
}

powerHardeningLaw::powerHardeningLaw(const double hard, const double exp_) : hardeningLaw(), _hardening(hard), _hexp(exp_){}

powerHardeningLaw::powerHardeningLaw(const powerHardeningLaw &source) : hardeningLaw(source), _hardening(source._hardening), _hexp(source._hexp){}

void powerHardeningLaw::hard(const double plastic_n, const double sy0, double &R, double &dR, double &ddR) const
{
  R = _hardening*pow(plastic_n,_hexp);
  dR = R*_hexp/plastic_n;
  ddR= dR*(_hexp-1.)/plastic_n;
}

exponentialHardeningLaw::exponentialHardeningLaw(const double hard, const double exp_) : hardeningLaw(), _hardening(hard), _hexp(exp_){}

exponentialHardeningLaw::exponentialHardeningLaw(const exponentialHardeningLaw &source) : hardeningLaw(source), _hardening(source._hardening), _hexp(source._hexp){}

void exponentialHardeningLaw::hard(const double plastic_n, const double sy0, double &R, double &dR, double &ddR) const
{
  double tmp = exp(-_hexp*plastic_n);
  R = _hardening*(1-tmp);
  dR = _hardening*_hexp*tmp;
  ddR= -_hexp*dR;
}

swiftHardeningLaw::swiftHardeningLaw(const double hard, const double exp_) : hardeningLaw(), _hardening(hard), _hexp(exp_){}

swiftHardeningLaw::swiftHardeningLaw(const swiftHardeningLaw &source) : hardeningLaw(source), _hardening(source._hardening), _hexp(source._hexp){}

void swiftHardeningLaw::hard(const double plastic_n, const double sy0, double &R, double &dR, double &ddR) const
{
  double tmp = 1.+_hardening*plastic_n;
  R = sy0*pow(tmp,_hexp) - sy0;
  tmp = _hardening / tmp;
  dR = R*_hexp*tmp;
  ddR= dR*(_hexp-1.)*tmp;
}


linearExponentialHardeningLaw::linearExponentialHardeningLaw(const double hard1, const double hard2,
                                                             const double exp_) : hardeningLaw(), _hard1(hard1), _hard2(hard2), _hexp(exp_){}

linearExponentialHardeningLaw::linearExponentialHardeningLaw(const linearExponentialHardeningLaw &source) : hardeningLaw(source),
                                                                                                            _hard1(source._hard1), _hard2(source._hard2),
                                                                                                            _hexp(source._hexp){}

void linearExponentialHardeningLaw::hard(const double plastic_n, const double sy0, double &R, double &dR, double &ddR) const
{
  double tmp = exp(-_hexp*plastic_n);
  R = _hard1*plastic_n + _hard2*(1.-tmp);
  dR = _hard1 + _hard2*_hexp*tmp;
  ddR=  - _hard2*_hexp*_hexp*tmp;
}


mlawJ2SmallStrains::mlawJ2SmallStrains(const int num,const double E, const double nu,
                                       const double rho,const double sy0,
                                       const hardeningLaw* hlaw) : materialLaw(num,true), _E(E), _nu(nu), _rho(rho), _rhlaw(false),
                                                                   _G(E/(2.*(1+nu))), _sy0(sy0), _G2(2.*_G), _G3(3.*_G), _kiso(E/(3.*(1.-2.*nu))),
                                                                   _lambda(E*nu/((1.-2.*nu)*(1.+nu))), _hooke(0.), _onethird(1./3.),
                                                                   _twothird(2./3.), _mindp(1.e-16), _tol(1.e-6), _itemax(20), _Gsquare3(3*_G*_G),
                                                                   _Gsquare4(4.*_G*_G), _oneandhalfIdev(1.,1.) // multiply by 3./2. in the function
{
  _oneandhalfIdev*=1.5; // to have 3/2 Idev
  if(hlaw==NULL)
  {
    _hlaw = new linearHardeningLaw(0.);
    _rhlaw = true;
  }
  else
    _hlaw = hlaw;
  // fill Hooke tensor
  for(int i=0;i<3;i++)
    for(int j=0;j<3;j++)
      for(int k=0;k<3;k++)
        for(int l=0;l<3;l++)
        {
          if(i==j and k==l) _hooke(i,j,k,l) = _lambda;
          if(i==k and j==l) _hooke(i,j,k,l)+= _G;
          if(i==l and j==k) _hooke(i,j,k,l)+= _G;
        }
}

mlawJ2SmallStrains::mlawJ2SmallStrains(const mlawJ2SmallStrains &source) : materialLaw(source), _E(source._E), _nu(source._nu),
                                                                           _rho(source._rho), _rhlaw(source._rhlaw), _G(source._G),
                                                                           _sy0(source._sy0), _G2(source._G2), _G3(source._G3), _kiso(source._kiso),
                                                                           _lambda(source._lambda), _hooke(source._hooke),
                                                                           _onethird(source._onethird), _twothird(source._twothird),
                                                                           _mindp(source._mindp), _oneandhalfIdev(source._oneandhalfIdev),
                                                                           _tol(source._tol), _itemax(source._itemax), _Gsquare4(source._Gsquare4),
                                                                           _Gsquare3(source._Gsquare3)
{
  if(_rhlaw)
    _hlaw = new linearHardeningLaw(0.);
  else
    _hlaw = source._hlaw;
}

mlawJ2SmallStrains::~mlawJ2SmallStrains()
{
  if(_rhlaw) delete _hlaw;
}


double mlawJ2SmallStrains::soundSpeed() const
{
  double factornu = (1.-_nu)/((1.+_nu)*(1.-2.*_nu));
  return sqrt(_E*factornu/_rho);
}

void mlawJ2SmallStrains::createIPState(IPStateBase* &ips,const bool* state_,const MElement *ele, const int nbFF_, const int gpt) const
{
  IPVariable* ipvi = new ipJ2SmallStrains();
  IPVariable* ipv1 = new ipJ2SmallStrains();
  IPVariable* ipv2 = new ipJ2SmallStrains();
  if(ips != NULL) delete ips;
  ips = new IP3State(state_,ipvi,ipv1,ipv2);
}


void mlawJ2SmallStrains::constitutive(const STensor3& F0,const STensor3& Fn,STensor3 &P,const IPVariable *q0,IPVariable *q1,
                                      STensor43 &Tangent,const bool stiff) const
{
  ipJ2SmallStrains* ipvcur = static_cast<ipJ2SmallStrains*>(q1);
  const ipJ2SmallStrains* ipvprev = static_cast<const ipJ2SmallStrains*>(q0);

  // set strain from F why
  ipvcur->_epsilon = Fn;
  for(int i=0;i<3;i++) ipvcur->_epsilon(i,i)-=1.;

  /* compute the elastic predictor */
  // elastic strain
  _destrain = ipvcur->_epsilon;
  _destrain -= ipvprev->_epsilon;
  // elastic stress
  _estress = _hooke*_destrain;
  _estress+= ipvprev->_sigma;
  // elastic deviatoric stress
  _edeviator= _estress;
  double p = _onethird*(_estress(0,0) + _estress(1,1) + _estress(2,2));
  for(int i=0;i<3;i++) _edeviator(i,i) -= p;

  // Von Mises stress
  double svm = 0.;
  for(int i=0;i<3;i++)
    for(int j=0;j<3;j++)
      svm += _edeviator(i,j)*_edeviator(i,j);
  svm *= 1.5;
  svm = sqrt(svm);

  /* elastic increment */
  double pstrain_n = ipvprev->_effectivePlasticStrain; // + _mindp;
  double pstrain=pstrain_n; // initial value of pstrain
  double R,dR,ddR;
  _hlaw->hard(pstrain_n,_sy0,R,dR,ddR);
  double elasticyield = _sy0 + R;
  double h = (_G3) + dR;
  double kp = elasticyield - svm; // + _G3*_mindp

  if(kp>=0.) // elastic
  {
    ipvcur->_sigma = _estress;
    ipvcur->_plasticStrain = ipvprev->_plasticStrain;
    ipvcur->_effectivePlasticStrain = ipvprev->_effectivePlasticStrain;
    ipvcur->_dpdE*=0.;
    if(stiff) // only if stiff ??
      ipvcur->_H = _hooke; // == hooke tensor if elastic !
    // + dCalgo !!!
  }
  else // plastic
  {
    /* normal to the yield surface */
    _normal = _edeviator;
    _normal*= (1.5/svm);

    /* plastic increment */
    double Dp = std::max(_mindp,-kp/h);

    /* return mapping algorithm */
    int iteration = 0;
    double res = 1.;
    double res0 = res;
    double plasticcorrection = _mindp;
    while(res>_tol)
    {
      iteration++;
      if(iteration > _itemax)
      {
        Msg::Error("No convergence after %d iterations in the return mapping algorithm. Plastic strain value: %e",iteration,pstrain);
        return;
      }
      pstrain = pstrain_n + Dp;
      _hlaw->hard(pstrain,_sy0,R,dR,ddR);
      kp = _G3*Dp + R + _sy0 - svm;

      res = fabs(kp)/_G;
      if(iteration==1 or res<res0)
      {
        res0 = res;
        plasticcorrection = std::max(- kp/(_G3+dR),0.99*(_mindp-Dp)); // idem than j2plast ??
        Dp += plasticcorrection;
      }
      else
      {
        plasticcorrection*=0.5;
        Dp = Dp - plasticcorrection;
      }
    }
    /* update stress */
    _estress = _normal;
    _estress*= (_twothird*(_sy0 + R));
    for(int i=0;i<3;i++) _estress(i,i) +=p;
    ipvcur->_sigma = _estress;

    /* update plastic strain */
    ipvcur->_effectivePlasticStrain = pstrain;
    ipvcur->_plasticStrain = _normal;
    ipvcur->_plasticStrain*= Dp;
    ipvcur->_plasticStrain += ipvprev->_plasticStrain;

    /* stiffness tensor */
    if(stiff) // only if stiff ?
    {
      h = _G3 +dR;
      double hcube = h*h*h;
      double fourGsquareOnh = _Gsquare4/h;
      double fourGsquaredepsOnSvm = _Gsquare4*(pstrain-pstrain_n)/svm;
      tensprod(_normal,_normal,_dianormal);
      _dianormal*=(-fourGsquareOnh+fourGsquaredepsOnSvm);
      ipvcur->_H = _oneandhalfIdev;
      ipvcur->_H*= (-1.5*fourGsquaredepsOnSvm);
      ipvcur->_H+=_hooke;
      ipvcur->_H+=_dianormal;
      // + dCalgo
    }
    /* dpdE  accumulate plastice strain p derivative w.r.t. strain increment */
    ipvcur->_dpdE = _normal;
    ipvcur->_dpdE*= (_G2/h);

  }
  // end plastic correction

  if(pstrain>pstrain_n)
  {
    ipvcur->_muiso = _G - _Gsquare3/h;
    ipvcur->_d2muiso = _normal;
    double Gonh = _G/h;
    ipvcur->_d2muiso*=(12.*ddR*Gonh*Gonh*Gonh);
  }
  else
  {
    ipvcur->_muiso = _G;
    ipvcur->_d2muiso*=0.;
  }
}
