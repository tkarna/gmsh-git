#ifndef ID_H
#define ID_H 1

//Identifiers of constitutive models
#define EL 1
#define EP 2
#define VT 3
#define MT 4
#define SC 5
#define AL 6
#define ANEL 7
#define MTSecF 8
#define MTFullStep 9
#define MTSecSd 10

//Identifiers of hardening models
#define H_POW 1
#define H_EXP 2
#define H_SWIFT 3
#define H_LINEXP 4

//Identifiers of damage models
#define D_LC 1
#define D_LCN 2
#define D_LINN 3
#define D_EXPN 4
#define D_PLN 5

//Identifiers of characteristic length models
#define CL_Iso 1
#define CL_Aniso 2
#define CL_Iso_V1 3
#define CL_Aniso_V1 4

//solvers
#define NR 1		//Newton-Raphson



//Identifiers of isotropization methods
#define ALL_ISO 1
#define ESH_ISO 2
#define ALL_ANI 3


//Identifiers of solver
#define NR 1

#endif
