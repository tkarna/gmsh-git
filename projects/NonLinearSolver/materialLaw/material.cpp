#ifndef MATERIAL_CC
#define MATERIAL_CC 1

#include <stdio.h>
#include <stdlib.h>
#include <math.h>


#include "material.h"

#ifdef NONLOCALGMSH
#include "matrix_operations.h"
#include "lccfunctions.h"
#include "j2plast.h"
#include "mlawNonLocalDamage.h"
#include "homogenization.h"
#include "homogenization_2rd.h"
using std::max;
using namespace MFH;
#include "rotations.h"
#endif
//****in all of the constbox**New tenor added by wu ling: double* dpdE, double* strs_dDdp_bar  ****************
//*******c_g the characteristic length tensor, the value is returned by Clength_creat function***********


void Material::constboxSecant(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep)
{
  printf("Function constBoxSecant is not defined for this material\n");
}

void Material::constboxSecantZero(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep)
{
   constboxSecant(dstrn, strs_n,  strs, statev_n, statev, Calgo, Csd, dCsd, dCsdp_bar, dnu, dnudp_bar, alpha, dpdE, dstrsdp_bar,c_g, kinc, kstep);
}

void Material::constboxFullStep(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** C0d, double*** dC0d, double** dC0dp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** depdE, double** c_g, int kinc, int kstep)
{
  printf("Function constBoxFullstep is not defined for this material\n");
}

void Material::constbox_2order(int mtx, double *DE, double* dstrn, double* strs, double* statev_n, double* statev, double **Calgo, Lcc* LCC, YieldF* YF, double** c_g, int kinc, int kstep)
{
  printf("Function constBox_2order is not defined for this material\n");
}

int Material::get_pos_eqstrs() const
{
  printf("Function get_pos_eqstrs() is not defined for this material\n");
  return -1;
}

bool Material::eqstrs_exist() const
{
    return false;
}
          

#ifdef NONLOCALGMSH
int Material::get_pos_mtx_stress () const
{ 
   return -1;
}
int Material::get_pos_inc_stress () const
{
   return -1;
}
#endif




//remarks: For tangent M-T method, the state varibles keep the effective stress value, then this value is corrected by minus (D_stres_eff) in finit element
//         For first order secant M-T method, the state varibles keep the apparent stress value, then D_stres_eff is set zero as feedback for finite element. 

//**************************
//LINEAR ELASTICITY 
//PROPS: MODEL, iddam, E, nu
//STATEV: strn, strs, dstrn
//**************************
//constructor
EL_Material::EL_Material(double* props, int idmat):Material(props, idmat){
	
	int iddam;
        int idclength;

	iddam = (int) props[idmat+1];
        idclength=(int) props[idmat+2];
	E = props[idmat+3];
	nu = props[idmat+4];

	nsdv = 18;
	pos_strn=0;
	pos_strs=6;
	pos_dstrn=12;
	
	dam = init_damage(props,iddam);
	if(dam){
		pos_dam = nsdv;
                pos_p = pos_dam+4;
		nsdv += dam->get_nsdv();
	}

        clength = init_clength(props,idclength);
}

#ifdef NONLOCALGMSH
int EL_Material::get_pos_currentplasticstrainfornonlocal() const 
{
	if(dam)
	{
		return pos_p;
	}
	return -1;
}
int EL_Material::get_pos_effectiveplasticstrainfornonlocal() const
{
	if(dam)
	{
		return pos_dam;
	}
	return -1;
}

int EL_Material::get_pos_damagefornonlocal() const
{
	if(dam) return pos_dam+2;
        return -1;
}
double EL_Material::getElasticEnergy (double* statev) 
{
    double eE = 0.;
    int i;
    static double* strn = NULL;
    static double* strs = NULL;
    //static double**CelD = NULL;
    //double  D;
    if(strn == NULL)
    {
       mallocvector(&strn,6);
       mallocvector(&strs,6);
       //mallocmatrix(&CelD,6,6);
    }
    copyvect(&(statev[this->get_pos_strn()]), strn,6);
    //get_elOD(statev, CelD);
    //contraction42(CelD,strn,strs);
    copyvect(&statev[get_pos_strs()], strs, 6);   
    //neegry due to damage has been removed strn3 =eps12*sqrt2, strs3=sqrt2*strs12
    for (i =0; i< 6; i++) eE+=0.5*(strs[i]*strn[i]);

    return eE;

}

double EL_Material::getPlasticEnergy (double* statev) 
{
    double eP = 0.;
    return eP;
}

#endif



//destructor
EL_Material::~EL_Material(){
	
	if(dam) delete dam;	
        if(clength) delete clength;

}

//print
void EL_Material::print(){

	printf("Linear elasticity\n");
	printf("Young modulus: %lf, Poisson ratio: %lf\n", E, nu);
	if(dam){ dam->print();}
        if(clength){clength->print();}
	
}


//constbox
void EL_Material::constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar,double *Sp_bar, double** c_g, int kinc, int kstep){

        int i,j,k; 
	static double* strn_n = NULL;
	static double* strn = NULL;

        double  dDdp_bar;                 //*********add by wu ling 
        double  D;  

        if(strn_n == NULL)
        {
	  mallocvector(&strn_n,6);
	  mallocvector(&strn,6);
        }

	copyvect(&(statev_n[pos_strn]), strn_n,6);

	//compute reference and algorithmic operators (=Hooke's operator)
	compute_Hooke(E,nu,Calgo);
	copymatr(Calgo,Cref,6,6);

	addtens2(1.,strn_n,1.,dstrn,strn);
	//compute new stress
	contraction42(Calgo,strn,strs);

	//update state variables
	addtens2(1., statev_n, 1., dstrn, statev); 
	copyvect(dstrn, &(statev[pos_dstrn]),6);



#ifdef NONLOCALGMSH
#else
// add by wu ling try nonlocal mehod without finite element cacilation
        if(dam)
        {
          statev[pos_dam]= statev[pos_p];
	  statev[pos_dam+1]= statev[pos_p]-statev_n[pos_p]; 
        }
#endif



        //Note: damage evolution doesn't depend on stress, it depends on local/nonlocal strain.
        //dDdE in damage box is replaced, and sends back dpdE
        if(dam){
                dDdp_bar=0.0;
		dam->damagebox(statev_n, statev, pos_strn, pos_strs, pos_dam, Calgo, dpdE, &dDdp_bar, alpha, kinc, kstep);

               // up date reference operator Cref, Calgo and dCref dCalgo according to damage
                 D = statev[pos_dam+2];

                 for(i=0;i<6;i++){
		         for(j=0;j<6;j++){
                                 Cref[i][j] = (1.-D)*Cref[i][j];
                                 Calgo[i][j] = (1.-D)*Calgo[i][j];			         
		         }
                          //  get stress*dDdp_bar
                         strs_dDdp_bar[i]= -1.*strs[i]*dDdp_bar;
                          //   apparent stress
                         strs[i]= (1.-D)*strs[i];
	         }

        }


#ifdef NONLOCALGMSH
#else
// add by wu ling try nonlocal mehod without finite element caculation
        if(dam)
        {
          for(i=0;i<6;i++){
		         for(j=0;j<6;j++){
                                  for(k=0;k<6;k++){
                                         dCref[i][j][k] = -Cref[i][j]*dDdp_bar*dpdE[k];
                                  }
                         }
         }
        }
#endif



        // update state variable
	copyvect(strs, &(statev[6]),6);

	//TO DO: call damage box, if dam!=NULL
        clength->creat_cg(statev_n, pos_p, c_g);

}

//costboxSecant
void EL_Material::constboxSecant(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep){

        int i,j,k; 
	static double* strn_n = NULL;
	static double* strn = NULL;

        double  dDdp_bar;                 //*********add by wu ling 
        double  D;  

        if(strn_n == NULL)
        {
	  mallocvector(&strn_n,6);
	  mallocvector(&strn,6);
        }

	copyvect(&(statev_n[pos_strn]), strn_n,6);

	//compute reference and algorithmic operators (=Hooke's operator)
	compute_Hooke(E,nu,Calgo);
	copymatr(Calgo,Csd,6,6);

	addtens2(1.,strn_n,1.,dstrn,strn);
	//compute new stress
       
	contraction42(Calgo,strn,strs); 



	//update state variables
	addtens2(1., statev_n, 1., dstrn, statev); 
	copyvect(dstrn, &(statev[pos_dstrn]),6);

#ifdef NONLOCALGMSH
#else
// add by wu ling try nonlocal mehod without finite element cacilation
        if(dam)
        {
          statev[pos_dam]= statev[pos_p];
	  statev[pos_dam+1]= statev[pos_p]-statev_n[pos_p]; 
        }
#endif


        //Note: damage evolution doesn't depend on stress, it depends on local/nonlocal strain.
        //dDdE in damage box is replaced, and sends back dpdE
        if(dam){
                dDdp_bar=0.0;
		dam->damagebox(statev_n, statev, pos_strn, pos_strs, pos_dam, Calgo, dpdE, &dDdp_bar, alpha, kinc, kstep);

               // up date reference operator Cref, Calgo and dCref dCalgo according to damage
                 D = statev[pos_dam+2];

                 for(i=0;i<6;i++){
		         for(j=0;j<6;j++){
                                 Csd[i][j] = (1.-D)*Csd[i][j];
                                 Calgo[i][j] = (1.-D)*Calgo[i][j];			         
		         }
                          //  get stress*dDdp_bar
                         dstrsdp_bar[i]= -1.*strs[i]*dDdp_bar;
                          //   apparent stress
                         strs[i]= (1.-D)*strs[i];
	         }
#ifdef NONLOCALGMSH
#else
// add by wu ling try nonlocal mehod without finite element cacilation
        if(dam)
        {
          for(i=0;i<6;i++){
		         for(j=0;j<6;j++){
                                  for(k=0;k<6;k++){
                                         dCsd[i][j][k] = -Csd[i][j]*dDdp_bar*dpdE[k];
                                  }
                         }
         }
        }
#endif

        }


        // update state variable
	copyvect(strs, &(statev[6]),6);

	//TO DO: call damage box, if dam!=NULL
         clength->creat_cg(statev_n, pos_p, c_g);


}

//constitutive box called by 2rd order method*******************
//for second-order method
//**************************************************************
void EL_Material::constbox_2order( int mtx, double *DE, double* dstrn, double* strs, double* statev_n, double* statev, 
double **Calgo, Lcc* LCC, YieldF* YF, double** c_g, int kinc, int kstep){

        int i,j,k; 
	static double* strn_n = NULL;
	static double* strn = NULL;

        double  D;  

        if(strn_n == NULL)
        {
	  mallocvector(&strn_n,6);
	  mallocvector(&strn,6);
	}

	//compute reference and algorithmic operators (=Hooke's operator)
	compute_Hooke(E,nu,Calgo);

        if(dam){ //need to be implemented****************************
  
#ifdef NONLOCALGMSH
#else
   // add by wu ling try nonlocal mehod without finite element caculation
               if(dam)
               {
                     statev[pos_dam]= statev[pos_p];
	             statev[pos_dam+1]= statev[pos_p]-statev_n[pos_p]; 
               }
#endif


        }   //end of damage,

      
        // compute LCC to get strain concentration tensor A, where dstrn=A:dstrn_c
        
	copyvect(&(statev_n[pos_strn]), strn_n,6);


	addtens2(1.,strn_n,1.,dstrn,strn);
	//compute new stress
       
	contraction42(Calgo,strn,strs); 


	//update state variables
	addtens2(1., statev_n, 1., dstrn, statev); 
	copyvect(dstrn, &(statev[pos_dstrn]),6);


       //clean structure YielF YF before update its values.
       // for elastic material YF->f...=0, If there is damage in the material YF->dp...will be update
          YF->trial=0.0;          // no plasticity
          YF->f=0.0;		  // value of yielding function
	  YF->dfdmu[0] = YF->dfdmu[1] = YF->dfdp_bar= 0.0;	
          for(i=0;i<6;i++){
                     YF->dfdstrn[i]=0.0;            //  derivatives w.r. incremental strain of composite
                     YF->dpdstrn[i]=0.0;     
          }
	  YF->dpdmu[0] = YF->dpdmu[1] = YF->dpdp_bar= 0.0;    //if there is damage	these values will be updated


        // update state variable
	copyvect(strs, &(statev[pos_strs]),6);

	//TO DO: call damage box, if dam!=NULL
         clength->creat_cg(statev_n, pos_p, c_g);

}





//reference operator from statev or material properties 
// = Hooke's operator in any case
void EL_Material::get_refOp(double* statev, double** C, int kinc, int kstep){

	get_elOp(C);
}

void EL_Material::get_elOp(double** Cel){

	compute_Hooke(E,nu,Cel);
}
void EL_Material::get_elOD(double* statev, double** Cel)
{
     get_elOp(Cel);
     if(dam)
     {
       double damage = statev[get_pos_dam()];
       for(int i=0; i < 6; i++)
         for(int j=0; j < 6; j++)
           Cel[i][j]*=1.-damage;
    }
}


void EL_Material::unload_step(double* dstrn, double* strs_n, double* statev_n, int kinc, int kstep){

       printf("unload_step not implemented for EL material\n");
}

//*****************************************************
//RATE-INDEPENDENT ELASTO-PLASTICITY
//PROPS: MODEL, iddam, E, nu, sy0, htype, hmod1, (hmod2), hexp 
//STATEV: strn, strs, dstrn, pstrn, p, dp, 2*mu_iso
//*****************************************************
//constructor
EP_Material::EP_Material(double* props, int idmat):Material(props, idmat){
	int k;
	int iddam, idclength;
	

	iddam = (int)props[idmat+1];
        idclength = (int)props[idmat+2];

	k=idmat+3;          
	E = props[k++];
	nu = props[k++];
	sy0 = props[k++];
	htype = (int) props[k++];
	hmod1 = props[k++];

	//hardening models with two hardening moduli
	if(htype==H_LINEXP){
		hmod2 = props[k++];
	}
	else{
		hmod2 = 0.;
	}
	hexp = props[k];

	nsdv = 28;
	pos_strn=0;
	pos_strs=6;
	pos_dstrn=12;
	pos_pstrn=18;
	pos_p = 24;
	pos_dp = 25;
	pos_twomu = 26;
	pos_eqstrs = 27;
	dam = init_damage(props,iddam);
	if(dam){
		pos_dam = nsdv;
		nsdv += dam->get_nsdv();
	}

        clength = init_clength(props,idclength);

}


int EP_Material::get_pos_eqstrs() const 
{
	return pos_eqstrs;
}

bool EP_Material::eqstrs_exist() const
{
        return true;
}


#ifdef NONLOCALGMSH
int EP_Material::get_pos_currentplasticstrainfornonlocal() const 
{
	return pos_p;
}
int EP_Material::get_pos_effectiveplasticstrainfornonlocal() const
{
	if(dam)
	{
		return pos_dam;
	}
	return -1;
}

int EP_Material::get_pos_damagefornonlocal() const
{
	if(dam) return pos_dam+2;
        return -1;
}
double EP_Material::getElasticEnergy (double* statev)
{
  double eE = 0.;
  int i;
  static double* strn = NULL;
  static double* pstrn = NULL;
  static double* strs = NULL;
  //static double**CelD = NULL;
  if(strn == NULL)
  {
    mallocvector(&strn,6);
    mallocvector(&pstrn,6);
    mallocvector(&strs,6);
   // mallocmatrix(&CelD,6,6);
  }
  copyvect(&(statev[pos_strn]), strn,6);
  copyvect(&(statev[pos_pstrn]),pstrn,6);
  copyvect(&statev[pos_strs], strs, 6);
  //enegry due to damage has been removed
  for (i =0; i< 6; i++) eE+=0.5*(strs[i]*(strn[i]-pstrn[i]));

  return eE;
}

double EP_Material::getPlasticEnergy (double* statev) 
{
  double eP = 0.;
  double p  = statev[get_pos_currentplasticstrainfornonlocal()];
  eP = sy0*p;
  switch(htype) {
    //POWER LAW HARDENING
    case H_POW:
      if(fabs(hexp+1.)!=0) 
         eP+=hmod1*pow(p,hexp+1.)/(hexp+1.);
      break;
    //EXPONENTIAL HARDENING
    case H_EXP:
      eP+=hmod1*p;
      if(fabs(hexp)!=0) eP+=hmod1*(exp(-hexp*p)-exp(0))/(hexp);
      break;
    //SWIFT LAW
    case H_SWIFT:
      eP-=sy0*p;
      if(fabs(hexp+1.)!=0 && fabs(hmod1)!=0.) eP+=sy0*pow(1.+hmod1*p,hexp+1.)/(hexp+1.)/hmod1;
      break;
    //LINEAR-EXPONENTIAL HARDENING
    case H_LINEXP:
      eP+=hmod1*p*p/2.+hmod2*p;
      if(fabs(hexp)!=0) eP+=hmod2*(exp(-hexp*p)-exp(0))/(hexp);
      break;
    default:
      printf("Bad choice of hardening law in j2hard: %d\n",htype);
      break;
  }
  return eP;
}
#endif

//destructor
EP_Material::~EP_Material(){
	if(dam) delete dam;
        if(clength) delete clength;
}

//print
void EP_Material::print(){

	printf("J2 elasto-plasticity\n");
	printf("Young modulus: %lf, Poisson ratio: %lf\n", E, nu);
	printf("Intial yield stress: %lf, hardening law: %d, hardening coefficients: %lf\t%lf\t%lf\n", sy0, htype,hmod1,hmod2,hexp);
	if(dam){ dam->print();}
        if(clength){clength->print();}
}

//constbox      //*****************New tenor added by wu ling: double* dpdE, double* strs_dDdp_bar ****************
void EP_Material::constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar,double *Sp_bar, double** c_g, int kinc, int kstep){

}
//******************************************************
//costboxSecant
//****************************************************
void EP_Material::constboxSecant(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, 
double* dnu, double &dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep){


                
}
//end of costboxSecant

// constboxSecant with Cs computed from Zero stress
void EP_Material::constboxSecantZero(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, 
double* dnu, double &dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep){


     
}
//end of costboxSecant
//**************************************************************
// EP_Material::constboxFullStep
//*************************************************************************************************************
void EP_Material::constboxFullStep(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** C0d, double*** dC0d, double** dC0dp_bar, 
double* dnu, double &dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double **depdE, double** c_g, int kinc, int kstep){

     
}
//end of constboxFullStep


//****************************************************
// consbox called by 2rd order method
//****************************************************

void EP_Material::constbox_2order(int mtx, double *DE, double* dstrn, double* strs, double* statev_n, double* statev,  double **Calgo, Lcc* LCC, YieldF* YF, double** c_g, int kinc, int kstep){


}

//end of consbox_2order

//****************************************************
//****************************************************

void EP_Material::get_refOp(double* statev, double** C, int kinc, int kstep){

	
	double twomu, threekappa, D;

	if(kinc<2 && kstep==1){
	   get_elOp(C);
	}
	//if(kinc<2 ){
                //get_elOD(statev,C);
	//}
	else{
                twomu = statev[pos_twomu];
		threekappa = E/(1.-2.*nu); 


                if(dam){                                           //*********add by wu ling 
                        D = statev[pos_dam+2];                     //***********13.04.2011********
		        twomu = twomu*(1-D);
		        threekappa = threekappa*(1-D);
                 }
		makeiso(threekappa/3.,twomu/2.,C);
	}

}

void EP_Material::get_elOD(double* statev, double** Cel)
{
     get_elOp(Cel);
     if(dam)
     {
       double damage = statev[pos_dam+2];
       for(int i=0; i < 6; i++)
         for(int j=0; j < 6; j++)
           Cel[i][j]*=1.-damage;
    }
}


void EP_Material::get_elOp(double** Cel){

	compute_Hooke(E,nu,Cel);
}

void EP_Material::unload_step(double* dstrn,double* strs_n, double* statev_n, int kinc, int kstep){

       if(kinc == 1 && kstep==1){
		return;
	}

	int i,error;
	double **C0;
        double **invC0;

	double *strs;
        double *udstrn;

	mallocmatrix(&C0,6,6);
	mallocmatrix(&invC0,6,6);
     
	
        mallocvector(&strs,6);
        mallocvector(&udstrn,6);





	get_elOD(statev_n, C0);
	inverse(C0,invC0,&error,6);
	if(error!=0) printf("problem while inversing C0 in unload_step of MTSecF\n");

	copyvect(&statev_n[pos_strs], strs, 6);        //stress in at tn
        contraction42(invC0, strs, udstrn);          //unloading strain of composite at tn
       
 // update statev_n to keep the imformation for residual condition 

        for(i=0;i<6;i++){
	strs_n[i]=0.0;	
        statev_n[pos_strs +i]=0.0;                                   //residual stress is 0
        statev_n[pos_strn +i]= statev_n[pos_strn +i]- udstrn[i];      //residual strain 
        statev_n[pos_dstrn +i]= dstrn[i]+ udstrn[i];    // strain increment in composte

        dstrn[i]= statev_n[pos_dstrn +i];
      	}


	freematrix(C0,6);
	freematrix(invC0,6);
	
        free(strs);
        free(udstrn);

}

//********************************************************************************************
// COMPOSITE - VOIGT  
//PROPS: MODEL, IDDAM, Nph, IDMAT phase 1, IDTOP phase 1, ... , 
//	 	...	IDMAT phase N, IDTOP phase N
//  (material properties of each phase), (volume fraction of each phase)
//STATEV: strn, strs, dstrn, (statev of each phase)   
//********************************************************************************************
//constructor
VT_Material::VT_Material(double* props, int idmat):Material(props, idmat){

	int i,idmat_i,idtop_i,k;
	int iddam, idclength;

	nsdv=18;
	iddam = (int) props[idmat+1];
        idclength = (int)props[idmat+2];
	Nph = (int) props[idmat+3];
	mat = (Material**) malloc(Nph*sizeof(Material*));	
	mallocvector(&vf,Nph);
        

	

	k=idmat+4;

	for(i=0;i<Nph;i++){
		idmat_i = (int)props[k++];
		idtop_i = (int)props[k++];
		
		mat[i] = init_material(props,idmat_i);
		vf[i] = props[idtop_i];
		nsdv += mat[i]->get_nsdv();
	}

	pos_strn=0;
	pos_strs=6;
	pos_dstrn=12;

        pos_p = 0; // defined by Ling Wu for the input of clength->creat_cg, which is not implemented for this material


	dam = init_damage(props,iddam);
	if(dam){
		pos_dam=nsdv;
		nsdv += dam->get_nsdv();
	}

        
        clength = init_clength(props,idclength);

 
}
#ifdef NONLOCALGMSH
int VT_Material::get_pos_currentplasticstrainfornonlocal() const
{
	return -1;
}
int VT_Material::get_pos_effectiveplasticstrainfornonlocal() const
{
	return -1;
}

int VT_Material::get_pos_damagefornonlocal() const
{
	return -1;
}
double VT_Material::getElasticEnergy (double* statev) 
{
    double eE = 0.;
    int i;
    static double* strn = NULL;
    static double* strs = NULL;
    //static double**CelD = NULL;
    //double  D;
    if(strn == NULL)
    {
       mallocvector(&strn,6);
       mallocvector(&strs,6);
       //mallocmatrix(&CelD,6,6);
    }
    copyvect(&(statev[get_pos_strn()]), strn,6);
    //get_elOD(statev, CelD);
    //contraction42(CelD,strn,strs);
    copyvect(&statev[get_pos_strs()], strs, 6);   
    //neegry due to damage has been removed strn3 =eps12*sqrt2, strs3=sqrt2*strs12
    for (i =0; i< 6; i++) eE+=0.5*(strs[i]*strn[i]);

    return eE;

}

double VT_Material::getPlasticEnergy (double* statev)
{
    double eP = 0.;
    return eP;
}

#endif

//destructor
VT_Material::~VT_Material(){

	int i;
	for (i=0;i<Nph;i++){
		if(mat[i]){
			delete mat[i];
		}
	}
	free(mat);
	free(vf);
	if(dam) delete(dam);
        if(clength) delete clength;
	
}

//print
void VT_Material::print(){

	int i;
	printf("Voigt\n");
	if(dam){ dam->print();}

	printf("Number of phases: %d\n", Nph);
	
	for(i=0;i<Nph;i++){
		printf("phase %d, volume fraction: %lf\n",i+1,vf[i]);
		mat[i]->print();
	}
}

//constbox
void VT_Material::constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, int kinc, int kstep){


}

//reference operator from statev
void VT_Material::get_refOp(double* statev, double** C, int kinc, int kstep){

	printf("get_refOp not implemented for Voigt material\n");

}

void VT_Material::get_elOp(double** Cel){

	printf("get_elOp not implemented for Voigt material\n");

}

void VT_Material::unload_step(double* dstrn, double* strs_n, double* statev_n, int kinc, int kstep){

       printf("unload_step not implemented for Voigt material\n");
}

void VT_Material::get_elOD(double* statev, double** Cel)
{
     get_elOp(Cel);
     if(dam)
     {
       double damage = statev[get_pos_dam()];
       for(int i=0; i < 6; i++)
         for(int j=0; j < 6; j++)
           Cel[i][j]*=1.-damage;
    }
}
//**************************************************************************************************************************************************
// COMPOSITE - MORI-TANAKA  
//PROPS: MODEL, IDDAM, Nph=2, IDMAT matrix, IDMAT inclusion, IDTOP inclusion,
//  (material properties of each phase), topology of inclusion: vf, ar1, ar2, 3 euler angles
//STATEV: strn, strs, dstrn, MT macro operator (21 components - symmetric), MT Elastic operator (21 components - symmetric), (statev of each phase)   
//***************************************************************************************************************************************************
//constructor
MT_Material::MT_Material(double* props, int idmat):Material(props,idmat){

	
}
#ifdef NONLOCALGMSH
int MT_Material::get_pos_currentplasticstrainfornonlocal() const
{
        if(mtx_mat->get_pos_currentplasticstrainfornonlocal()>=0)	
	    return mtx_mat->get_pos_currentplasticstrainfornonlocal()+idsdv_m;
	return -1;

}
int MT_Material::get_pos_effectiveplasticstrainfornonlocal() const
{
       if(mtx_mat->get_pos_effectiveplasticstrainfornonlocal()>=0)
	   return mtx_mat->get_pos_effectiveplasticstrainfornonlocal()+idsdv_m;
       return -1;
}

int MT_Material::get_pos_damagefornonlocal() const
{
       if(mtx_mat->get_pos_damagefornonlocal() >=0)
	   return mtx_mat->get_pos_damagefornonlocal()+idsdv_m;
       return -1;
}
int MT_Material::get_pos_mtx_stress () const
{ 
   return idsdv_m + mtx_mat->get_pos_strs();
}
int MT_Material::get_pos_inc_stress () const
{
   return idsdv_i + icl_mat->get_pos_strs();
}
double MT_Material::getElasticEnergy (double* statev) 
{
    double eEi = icl_mat->getElasticEnergy(&statev[idsdv_i]);
    double eE0 = mtx_mat->getElasticEnergy(&statev[idsdv_m]);
    double eE = vf_i*eEi+vf_m*eE0;

    return eE;

}

double MT_Material::getPlasticEnergy (double* statev) 
{
    double ePi = icl_mat->getPlasticEnergy(&statev[idsdv_i]);
    double eP0 = mtx_mat->getPlasticEnergy(&statev[idsdv_m]);
    double eP = vf_i*ePi+vf_m*eP0;

    return eP;
}
#endif

//destructor
MT_Material::~MT_Material(){

	
	if(mtx_mat) delete mtx_mat;
	if(icl_mat) delete icl_mat;

	free(ar);
	free(euler);

	if(dam) delete(dam);
        if(clength) delete clength;

}

//print
void MT_Material::print(){

	
	printf("Mori-Tanaka\n");
	if(dam) dam->print();
	printf("Number of phases: %d\n", Nph);
	
	printf("matrix, volume fraction: %lf\n",vf_m);
	mtx_mat->print();

	printf("inclusion, volume fraction: %lf\n",vf_i);
	printf("aspect ratio: %lf, %lf\n",ar[0],ar[1]);
	printf("euler angles: %lf, %lf, %lf\n",euler[0],euler[1],euler[2]);	
	icl_mat->print();
	
}

//constbox   ************* new tensor added by wu ling: double* dpdE, double* strs_dDdp_bar 13.04.2011
void MT_Material::constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, int kinc, int kstep){
	//alpha: for mid-point integration law to compute reference MT operator: C_MT = (1-alpha)*C_MT(tn) + alpha*C_MT(tn+1)
	

}

void MT_Material::get_refOp(double* statev, double** C, int kinc, int kstep){


	int i,j,k;

	if(kinc < 2 && kstep==1){
		get_elOp(C);
	}
	else{
		k=pos_C;
		for(i=0;i<6;i++){
			for(j=i;j<6;j++){
				C[i][j] = statev[k++];
				C[j][i] = C[i][j];
			}
		}
	}		

}


void MT_Material::get_elOp(double** Cel){

}
// Cel with damaged matrix and inclusion, add by Ling WU June 2012 

void MT_Material::get_elOD(double* statev, double** Cel)
{
}


void MT_Material::unload_step(double* dstrn, double* strs_n, double* statev_n, int kinc, int kstep){
   printf("Function unload_step is not defined for this MT material\n");
}
//**************************************************************************************************************************************************
// COMPOSITE - MORI-TANAKA  
//*******modified by Ling wu for secant mehod of first moment---  JUNE 2012
//PROPS: MODEL, IDDAM, Nph=2, IDMAT matrix, IDMAT inclusion, IDTOP inclusion,
//  (material properties of each phase), topology of inclusion: vf, ar1, ar2, 3 euler angles
//STATEV: strn, strs, dstrn, MT macro operator (21 components - symmetric), MT Elastic operator (21 components - symmetric), (statev of each phase)   
//***************************************************************************************************************************************************
//
//constructor
MTSecF_Material::MTSecF_Material(double* props, int idmat):MT_Material(props,idmat){

}

//destructor
MTSecF_Material::~MTSecF_Material(){

}


//constbox   ************* new tensor added by wu ling: double* dpdE, double* strs_dDdp_bar 13.04.2011
//*********************************************
//unloading is carried out at each iteration 

void MTSecF_Material::constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* dstrsdp_bar, double *Sp_bar, double** c_g, int kinc, int kstep){
	//alpha: for mid-point integration law to compute reference MT operator: C_MT = (1-alpha)*C_MT(tn) + alpha*C_MT(tn+1)
	

}



// unloading step for MTSecF_Material material to reset statev_n in residual condition, by Ling Wu June 2012

void MTSecF_Material::unload_step(double* dstrn, double* strs_n, double* statev_n, int kinc, int kstep){
}

//**************************************************************************************************************************************************
// COMPOSITE - MORI-TANAKA   
//*******modified by Ling wu for full step of first moment---  AUGUST 2012
//PROPS: MODEL, IDDAM, Nph=2, IDMAT matrix, IDMAT inclusion, IDTOP inclusion,
//  (material properties of each phase), topology of inclusion: vf, ar1, ar2, 3 euler angles
//STATEV: strn, strs, dstrn, MT macro operator (21 components - symmetric), MT Elastic operator (21 components - symmetric), (statev of each phase)   
//***************************************************************************************************************************************************
//
//constructor
MTFullStep_Material::MTFullStep_Material(double* props, int idmat):MT_Material(props,idmat){

}

//destructor
MTFullStep_Material::~MTFullStep_Material(){

}


//constbox   ************* new tensor added by wu ling: double* dpdE, double* strs_dDdp_bar 13.04.2011
//*********************************************
//unloading is carried out at each iteration 

void MTFullStep_Material::constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* dstrsdp_bar, double *Sp_bar, double** c_g, int kinc, int kstep){
	//alpha: for mid-point integration law to compute reference MT operator: C_MT = (1-alpha)*C_MT(tn) + alpha*C_MT(tn+1)
	

}





//**************************************************************************************************************************************************
// COMPOSITE - MORI-TANAKA  
//*******modified by Ling wu for secant mehod of Second moment---  AUG 2013
//PROPS: MODEL, IDDAM, Nph=2, IDMAT matrix, IDMAT inclusion, IDTOP inclusion,
//  (material properties of each phase), topology of inclusion: vf, ar1, ar2, 3 euler angles
//STATEV: strn, strs, dstrn, MT macro operator (21 components - symmetric), MT Elastic operator (21 components - symmetric), (statev of each phase)   
//***************************************************************************************************************************************************
//
//constructor
MTSecSd_Material::MTSecSd_Material(double* props, int idmat):MT_Material(props,idmat){

     malloc_lcc(&LCC, Nph, vf_i);

}

//destructor
MTSecSd_Material::~MTSecSd_Material(){

     free_lcc(&LCC);  

}


//constbox   ************* new tensor added by wu ling: double* dpdE, double* strs_dDdp_bar 13.04.2011
//*********************************************
//unloading is carried out at each iteration 

void MTSecSd_Material::constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* dstrsdp_bar, double *Sp_bar, double** c_g, int kinc, int kstep){
	//alpha: for mid-point integration law to compute reference MT operator: C_MT = (1-alpha)*C_MT(tn) + alpha*C_MT(tn+1)
	
        
}



// unloading step for MTSecSd_Material material to reset statev_n in residual condition, and the second monment von mise stress eqstrs in elastic-plastic phases, by Ling Wu AUG 2013
// the value of strain concentration tensor is also initialized with mu0 mu1 kappa0 kappa1

void MTSecSd_Material::unload_step(double* dstrn, double* strs_n, double* statev_n, int kinc, int kstep){

}



//********************************************************************************************
// COMPOSITE - SELF-CONSISTENT
//PROPS: MODEL, IDDAM, mtx (1 or 0), Nicl, IDMAT matrix (if any), IDMAT inclusion, IDTOP inclusion,
//  (material properties of each phase), topology of inclusions: vf, ar1, ar2, 3 euler angles
//STATEV: strn, strs, dstrn, SC macro operator (36 components) , (statev of each phase)   
//********************************************************************************************
//constructor
SC_Material::SC_Material(double* props, int idmat):Material(props, idmat){

}
#ifdef NONLOCALGMSH
int SC_Material::get_pos_currentplasticstrainfornonlocal() const
{
	if(mtx_mat->get_pos_currentplasticstrainfornonlocal()>=0)
	     return mtx_mat->get_pos_currentplasticstrainfornonlocal()+idsdv_m;
        return -1;

}
int SC_Material::get_pos_effectiveplasticstrainfornonlocal() const
{
	if(mtx_mat->get_pos_effectiveplasticstrainfornonlocal()>=0)
             return mtx_mat->get_pos_effectiveplasticstrainfornonlocal()+idsdv_m;
	return -1;
}

int SC_Material::get_pos_damagefornonlocal() const
{
	if(mtx_mat->get_pos_damagefornonlocal() >=0)
	     return mtx_mat->get_pos_damagefornonlocal();
	return -1;
}
int SC_Material::get_pos_mtx_stress () const
{ 
   return idsdv_m + mtx_mat->get_pos_strs();
}
int SC_Material::get_pos_inc_stress () const
{
   return idsdv_i[0] + icl_mat[0]->get_pos_strs();
}
double SC_Material::getElasticEnergy (double* statev) 
{
    int i=0;
    double eEi = 0.;
    double eE0 = mtx_mat->getElasticEnergy(&statev[idsdv_m]);
    for(i=0;i<Nicl;i++)
	eEi+=vf_i[i]*icl_mat[i]->getElasticEnergy(&statev[idsdv_i[i]]);
    double eE = eEi+vf_m*eE0;
    return eE;

}

double SC_Material::getPlasticEnergy (double* statev) 
{
    int i=0;
    double ePi = 0.;
    double eP0 = mtx_mat->getPlasticEnergy(&statev[idsdv_m]);
    for(i=0;i<Nicl;i++) ePi += vf_i[i]*icl_mat[i]->getPlasticEnergy(&statev[idsdv_i[i]]);
    double eP = ePi+vf_m*eP0;

    return eP;
}

#endif

//destructor
SC_Material::~SC_Material(){

	int i;
	if(mtx_mat) delete mtx_mat;
	for(i=0;i<Nicl;i++){
		if(icl_mat[i]) delete icl_mat[i];
	}
	free(icl_mat);

	freematrix(ar,Nicl);
	freematrix(euler,Nicl);
	free(idsdv_i);
	free(vf_i);

	if(dam) delete(dam);
        if(clength) delete clength;

}

//print
void SC_Material::print(){

	int i;
	printf("Self-Consistent\n");
	if(dam) dam->print();
	printf("Number of phases: %d\n", Nph);
	
	if(mtx){
		printf("matrix, volume fraction: %lf\n",vf_m);
		mtx_mat->print();
	}
	for(i=0;i<Nicl;i++){
		printf("inclusion %d, volume fraction: %lf\n",i+1,vf_i[i]);
		printf("aspect ratio: %lf, %lf\n",ar[i][0],ar[i][1]);
		printf("euler angles: %lf, %lf, %lf\n",euler[i][0],euler[i][1],euler[i][2]);	
		(icl_mat[i])->print();
	}
	
}

//constbox
void SC_Material::constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar, double *Sp_bar, double** c_g, int kinc, int kstep){
	

}

void SC_Material::get_refOp(double* statev_n, double** C, int kinc, int kstep){


}


void SC_Material::get_elOp(double** Cel){

	printf("get_elOp not implemented for Self-Consistent material\n");
}

void SC_Material::get_elOD(double* statev, double** Cel)
{
     printf("get_elOD not implemented for Self-Consistent material\n");
}

void SC_Material::unload_step(double* dstrn, double* strs_n, double* statev_n, int kinc, int kstep){

       printf("unload_step not implemented for SC material\n");
}

//********************************************************************
//************  add by Ling wu 14/09/2011   *********************
//ANISOTROPIC LINEAR ELASTICITY 
//PROPS: MODEL, iddam, anpr(E1, E2,E3,nu12,nu13,nu23,G12,G13,G23) 9 material constants
//PROPS: three euler anagles from golbe coordinates to local coordinates 
//( in order to agree with the euler angle of inclusion)
//STATEV: strn, strs, dstrn in global coordinates
//*******************************************************************
//constructor
ANEL_Material::ANEL_Material(double* props, int idmat):Material(props, idmat){
	
	int k;
        int iddam, idclength;

        int i;

//allocate memory for the 9 material constants and 3 euler angles

	mallocvector(&anpr,9);
	mallocvector(&euler,3);


	iddam = (int) props[idmat+1];    // damage
        idclength = (int)props[idmat+2];

        k=idmat+3;

	anpr[0] = props[k++];
        anpr[1] = props[k++];
        anpr[2] = props[k++];
        anpr[3] = props[k++];
        anpr[4] = props[k++];
        anpr[5] = props[k++];
        anpr[6] = props[k++];
        anpr[7] = props[k++];
        anpr[8] = props[k++];

        euler[0] = props[k++];
        euler[1] = props[k++];
        euler[2] = props[k];
        
	nsdv = 18;
	pos_strn=0;
	pos_strs=6;
	pos_dstrn=12;

        pos_p =0; // for the input of clength->creat_cg, as damage is not implemented in this material
	
	dam = init_damage(props,iddam);
	if(dam){
		pos_dam = nsdv;
		nsdv += dam->get_nsdv();
	}
	

        clength = init_clength(props,idclength);

}

//destructor
ANEL_Material::~ANEL_Material(){
	
	if(dam) delete dam;
        if(clength) delete clength;

        free(anpr);
        free(euler);	

}

//print
void ANEL_Material::print(){

	printf("Anisotropic Linear elasticity\n");
        printf("Young modulus E1,E2,E3: %lf,%lf,%lf\n", anpr[0],anpr[1],anpr[2]);
	printf("Poisson ratio nu12,nu13,nu23: %lf, %lf, %lf\n", anpr[3],anpr[4],anpr[5]);
        printf("Shear modulus G12,G13,G23: %lf, %lf, %lf\n", anpr[6],anpr[7],anpr[8]);
        printf("Euler angles: %lf, %lf, %lf\n", euler[0],euler[1],euler[2]);
	if(dam){ dam->print();}
	
}
#ifdef NONLOCALGMSH
int ANEL_Material::get_pos_currentplasticstrainfornonlocal() const {return -1;}
int ANEL_Material::get_pos_effectiveplasticstrainfornonlocal() const {return -1;}
int ANEL_Material::get_pos_damagefornonlocal() const
{
	if(dam) return pos_dam;
	return -1;
}
double ANEL_Material::getElasticEnergy (double* statev) 
{
    double eE = 0.;
    int i;
    static double* strn = NULL;
    static double* strs = NULL;
    if(strn == NULL)
    {
       mallocvector(&strn,6);
       mallocvector(&strs,6);
    }
    copyvect(&(statev[get_pos_strn()]), strn,6);
    copyvect(&statev[get_pos_strs()], strs, 6);   
    //enegry due to damage has been removed
    for (i =0; i< 6; i++) eE+=0.5*(strs[i]*strn[i]);

    return eE;

}

double ANEL_Material::getPlasticEnergy (double* statev) 
{
    double eP = 0.;
    return eP;
}
#endif

//constbox
void ANEL_Material::constbox(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double** Cref, double*** dCref, double* tau, double** dtau, double** Calgo, double alpha, double* dpdE, double* strs_dDdp_bar,double *Sp_bar, double** c_g, int kinc, int kstep){

	static double* dstrs;
	static bool initialized = false;
        if(!initialized){
          mallocvector(&dstrs,6);
          initialized = true;
        }
        for(int i=0; i < 6; i++) dstrs[i]=0.;
	//compute reference and algorithmic operators (in global coordinates)
	compute_AnisoOperator(anpr, euler, Calgo);
	copymatr(Calgo,Cref,6,6);

	//compute new stress
	contraction42(Calgo,dstrn,dstrs);
	addtens2(1.,strs_n,1.,dstrs,strs);

	//update state variables
	addtens2(1., statev_n, 1., dstrn, statev); 
	copyvect(strs, &(statev[6]),6);
	copyvect(dstrn, &(statev[12]),6);

	//TO DO: call damage box, if dam!=NULL

        clength->creat_cg(statev_n, pos_p, c_g);     //*********add by wu ling 

	//free(dstrs);
}

void ANEL_Material::constboxSecant(double* dstrn, double* strs_n,  double* strs, double* statev_n, double* statev, double **Calgo, double** Csd, double*** dCsd, double** dCsdp_bar, double* dnu, double& dnudp_bar, double alpha, double* dpdE, double* dstrsdp_bar, double** c_g, int kinc, int kstep){

        static double* dstrs;
	static bool initialized = false;
        if(!initialized){
          mallocvector(&dstrs,6);
          initialized = true;
        }
	//compute reference and algorithmic operators (in global coordinates)
	compute_AnisoOperator(anpr, euler, Csd);
	copymatr(Csd,Calgo,6,6);

	//compute new stress
	contraction42(Calgo,dstrn,dstrs);
	addtens2(1.,strs_n,1.,dstrs,strs);

	//update state variables
	addtens2(1., statev_n, 1., dstrn, statev); 
	copyvect(strs, &(statev[6]),6);
	copyvect(dstrn, &(statev[12]),6);

	//TO DO: call damage box, if dam!=NULL

        clength->creat_cg(statev_n, pos_p, c_g);       //*********add by wu ling 

}


//****************************************************
// consbox called by 2rd order method
//****************************************************

void ANEL_Material::constbox_2order(int mtx, double *DE, double* dstrn, double* strs, double* statev_n, double* statev,  double **Calgo, Lcc* LCC, YieldF* YF, double** c_g, int kinc, int kstep){

        int i;
        static double* dstrs;
	static bool initialized = false;
        if(!initialized){
          mallocvector(&dstrs,6);
          initialized = true;
        }
	//compute reference and algorithmic operators (in global coordinates)
	compute_AnisoOperator(anpr, euler, Calgo);

	//compute new stress
	contraction42(Calgo,dstrn,dstrs);
	addtens2(1.,&(statev_n[pos_strs]), 1.,dstrs,strs);

	
       //clean structure YielF YF before update its values.
       // for elastic material YF->f...=0, If there is damage in the material YF->dp...will be update
          YF->trial=0.0;          // no plasticity
          YF->f=0.0;		  // value of yielding function
	  YF->dfdmu[0] = YF->dfdmu[1] = YF->dfdp_bar= 0.0;	
          for(i=0;i<6;i++){
                     YF->dfdstrn[i]=0.0;            //  derivatives w.r. incremental strain of composite
                     YF->dpdstrn[i]=0.0;     
          }
	  YF->dpdmu[0] = YF->dpdmu[1] = YF->dpdp_bar= 0.0;    //if there is damage	this value will be updated


        //update state variables
	addtens2(1., statev_n, 1., dstrn, statev); 
	copyvect(strs, &(statev[pos_strs]),6);
	copyvect(dstrn, &(statev[pos_dstrn]),6);

	//TO DO: call damage box, if dam!=NULL

        clength->creat_cg(statev_n, pos_p, c_g);       //*********add by wu ling 

}

//end of consbox_2order


//reference operator from statev or material properties 
// = anisotropic elastic operator in any case
void ANEL_Material::get_refOp(double* statev, double** C, int kinc, int kstep){

	get_elOp(C);
}

void ANEL_Material::get_elOp(double** Cel){

	compute_AnisoOperator(anpr, euler,Cel);
}

void ANEL_Material::get_elOD(double* statev, double** Cel)
{
     get_elOp(Cel);
     if(dam)
     {
       double damage = statev[get_pos_dam()];
       for(int i=0; i < 6; i++)
         for(int j=0; j < 6; j++)
           Cel[i][j]*=1.-damage;
    }
}

void ANEL_Material::unload_step(double* dstrn, double* strs_n, double* statev_n, int kinc, int kstep){

       printf("unload_step not implemented for ANEL material\n");
}
#endif
