//
// C++ Interface: numericalMaterial
//
// Description: Class creates microsolver from setting
//
//
// Author:  <Van Dung NGUYEN>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef NUMERICALMATERIAL_H_
#define NUMERICALMATERIAL_H_



#include "nonLinearMicroBC.h"
#include "mlaw.h"

class nonLinearMechSolver;
class partDomain;
class IPStateBase;

class numericalMaterial{
  #ifndef SWIG
  public:
    inline static int createTypeWithTwoInts(int i1, int i2)
    {
      return i2 + 10000 * i1;
    }
    inline static void getTwoIntsFromType(int t, int &i1, int &i2)
    {
      i2 = t % 10000;
      i1 = t / 10000;
    }
    // function to distribut IP to different process
    static void distributeMacroIP(const std::vector<int>& allIPNum, const std::vector<int>& allRanks,
                                    std::map<int,std::set<int> >& mapIP);
	protected:
    int _tag; // tag for solver
    int _bcTag; // tag for BC
    int _numstep; // number of time step at microsolve
    double _tol; // tolerance used in NR at microsolver
    int _order; // homogenization order 1- first, 2- second
    int _systemType; // 0 - Disp elumination, 1 - mult elimination, 2 - disp-mult
    int _scheme; // 1- linear static, 2- nonlinear static
    int _solver; // 0 - gmm, 1 - tausc, 2 -petsc
    bool _isIterative; // 0 - non-iterative , 1 - iterative
    bool _stiffModification; // 0 - using one stiffness, 1- using multiple stiffness


    std::map<int,int> _meshIdMap; // mesh id map,
    std::map<int,std::string>::iterator _idIterator;// iterator to acces meshes
    std::map<int,std::string> _allPertMesh; // all perturbed meshes

    std::string _microMeshFile;	// micro meshfile
    std::string _microGeoFile; // micro geometry
    int _meshOrder; // mesh ord
    int _meshDim; // mesh dimension
    bool _meshIncomplete; // mesh incomplete

    std::vector<partDomain*> _allDomain; // micro domain to define
    std::vector<materialLaw*> _allMaterialLaw; // micro material to define

    std::set<int> _pertMaterialLawIP; // all perturbed by material law
    std::vector<materialLaw*> _allPertubationLaw; // all perturbed law

    nonLinearMicroBC::whichBCType _bcType; // BC type
    SVector3 _xPeriodicity, _yPeriodicity, _zPeriodicity; // micro-perioidcity
    std::vector<int> _physical;  // entities for applying microscopic boundary condition
    std::string _pbcNodeData; // for PBC from file

    int _method, _degree;  // periodic options
    bool _virtualVertexFlag; // add virtual vertex flag

    bool _stressFlag; //  stress flag 0- no 1- yes
    int _stressIntegFlag; // stress method 0- volume, 1-surface
    bool _tangentFlag; // tangent flag 0- no 1- yes
    int _tangentIntegFlag; // tangent method 0- perturbation 1-condensation
    double _tangentPerturbation; // perturbation value to compute tangent operator


    std::set<int> _allStabilityView; // stability view
    bool _stabilityAnalysis; // true if using eigen analysis
    std::vector<int> _modeview; // view mode
    int _numeigenvalue; // number eigenvalue to computed
    double _perturbationfactor; // factor for adding buckling mode to solution
    double _instabilitycriterion; //  criterion for instability
		bool _perturbation; // perturbaton flag: 0- not perturbate buckling mode 1 - yes
		bool _eigenFollowing; // eigen following

    std::set<int> _allView; // micro-solver to view
    bool _isViewAll; // true if view all
    int _allGP; // view all GP

    // for pathFollowing analysis
    bool _pathFollowing; // true if using path following method
    int _controlType; // following strategies

    double _rveVolume; // volume of RVE
    #endif

	public:
    #ifndef SWIG
		numericalMaterial(int tag);
		numericalMaterial(const numericalMaterial& src);
		numericalMaterial& operator = (const numericalMaterial& src);

		virtual ~numericalMaterial();
    // create micro solver
    nonLinearMechSolver* createMicroSolver(const int ele=0, const int gpt=0) const;

    virtual void createIPState(const bool issolve,  /*create IPstate but create solver if true else false*/
                                IPStateBase* &ips, const bool* state_=NULL,
                                const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const  = 0;

		#endif
		// set micro meshes
		virtual void loadModel(const std::string meshfile);

		virtual void createMicroModel(const std::string geoFileName,const int dim,const int order,
                   const bool incomplete=false);

		// set micro part domains
		virtual void addDomain(partDomain* domain);
		// set micro material law
    virtual void addMaterialLaw(materialLaw* mlaw);

    virtual void setPerturbationMaterialLawIP(const int ele, const int ip);
    virtual void addPerturbationMaterialLaw(materialLaw* mlaw);
		// set micro periodicity
		virtual void setPeriodicity(const double x,const double y,const double z, const std::string direction);
		// set periodic solver options
		virtual void setPeriodicBCOptions(const int method, const int degree = 3, const bool addverex = false) ;
		// set periodic boundary conditon entities
		virtual void addPeriodicBC(const int tag, const int g1=0, const int g2=0, const int g3=0,
                       const int g4=0, const int g5 =0,const int g6= 0);
    // set linear displacement condition
		virtual void addLinearDisplacementBC(const int tag, const int g1, const int g2, const int g3,
                               const int g4, const int g5 = 0, const int g6 = 0);
		// set minimal kinematical condition
		virtual void addMinimalKinematicBC(const int tag, const int g1, const int g2, const int g3,
                             const int g4, const int g5=0, const int g6=0);
		// set tolerence for micro newton-raphson
		virtual void setNumStep(const int i);
		//
		virtual void setTolerance(const double tol);
		// set view to this element
		virtual void addViewMicroSolver(const int e, const int gp = 0);
		// set view all elements
		virtual void setViewAllMicroProblems(const bool flag = true, const int algp = 0);
		// set order
		virtual void setOrder(const int order );

		virtual void tangentAveragingFlag(const bool fl = true);
		virtual void stressAveragingFlag(const bool fl = true);
		virtual void setStressAveragingMethod(const int method);
		virtual void setTangentAveragingMethod(const int method, const double prec = 1.e-8);
		//
		virtual void Scheme(const int s);
		virtual void Solver(const int s);
		virtual void setSystemType(const int s);
		//
		virtual void stiffnessModification(const bool flag = true);
    virtual void iterativeProcedure(const bool flag = true);

    // path following flag
    virtual void pathFollowing(const bool p);
    virtual void setPathFollowingControlType(const int i);

    virtual void readPBCDataFromFile(std::string fn);
    virtual void setRVEVolume(const double val);

    /** for stability analysis */
    virtual void addStabilityMicroSolver(const int e, const int gp);
    virtual void eigenValueSolver(const int num = 10, const bool fl = true); // number of eigenvalue
		virtual void setModeView(const int view); // for view buckling mode
		virtual void setPerturbationFactor(const double val = 1e-2);
		virtual void setInstabilityCriterion(const double val = 1e-6);
		virtual void perturbateBucklingModeByMinimalEigenValueMode(const bool flag = true);
		virtual void setEigenSolverFollowing(const bool flag);

		// this function load all meshes in current folder
		virtual void loadAllRVEMeshes(const std::string prefix);
		virtual void printMeshIdMap() const;
		#ifndef SWIG
		//this function provides an id for a GP
		virtual void assignMeshId(const int e, const int gpt);
		virtual void setMeshId(const int e, const int gpt, const int id);
		// this function get mesh id
		virtual int getMeshId(const int e, const int gpt) const;
		// this function get mesh filename
		virtual std::string getMeshFileName(const int e, const int gpt) const;
		#if defined(HAVE_MPI)
		// these function used for comunicate MPI
		virtual int getNumberValuesMPI() const;
		virtual void fillValuesMPI(int* val) const;
		virtual void getValuesMPI(const int* val, const int size);
		#endif //HAVE_MPI
		#endif //SWIG
};

#ifndef SWIG
class fileNameFilter {
  protected:
    std::string _prefix;
    std::string _fileTag;
  public:
    fileNameFilter(const std::string prefix, const std::string tag);
    ~fileNameFilter(){}
    int getNumber(const std::string& filename);
};

#endif

#endif // NUMERICALMATERIAL_H_
