//
// C++ Interface: terms
//
// Description: Define isotropic hardening laws for j" plasticity
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef _J2ISOTROPICHARDENING_H_
#define _J2ISOTROPICHARDENING_H_
#ifndef SWIG
#include "ipstate.h"
#include "MElement.h"
#include "ipHardening.h"
#endif
class J2IsotropicHardening{
 public :
  enum hardeningname{perfectlyPlasticJ2IsotropicHardening,powerLawJ2IsotropicHardening, exponentialJ2IsotropicHardening,
                     swiftJ2IsotropicHardening, linearExponentialJ2IsotropicHardening};
 protected :
  int _num; // number of law (must be unique !)
  bool _initialized; // to initialize law
  double _yield0;
 public:
  // constructor
#ifndef SWIG
  J2IsotropicHardening(const int num, double yield0, const bool init=true);
  ~J2IsotropicHardening(){}
  J2IsotropicHardening(const J2IsotropicHardening &source);
  J2IsotropicHardening& operator=(const J2IsotropicHardening &source);
  virtual int getNum() const{return _num;}
  virtual hardeningname getType() const=0;
  virtual void createIPVariable(IPJ2IsotropicHardening* &ipv) const=0;
  virtual void initLaws(const std::map<int,J2IsotropicHardening*> &maplaw)=0;
  virtual const bool isInitialized() {return _initialized;}
  virtual void hardening(double p, IPJ2IsotropicHardening &ipv) const=0;
  virtual double getYield0() const {return _yield0;}
  virtual J2IsotropicHardening * clone() const=0;
#endif
};

class PerfectlyPlasticJ2IsotropicHardening : public J2IsotropicHardening
{
 public :
  // constructor
  PerfectlyPlasticJ2IsotropicHardening(const int num, double yield0, const bool init=true);
  ~PerfectlyPlasticJ2IsotropicHardening(){}
#ifndef SWIG
  PerfectlyPlasticJ2IsotropicHardening(const PerfectlyPlasticJ2IsotropicHardening &source);
  PerfectlyPlasticJ2IsotropicHardening& operator=(const J2IsotropicHardening &source);
  virtual int getNum() const{return _num;}
  virtual hardeningname getType() const{return J2IsotropicHardening::perfectlyPlasticJ2IsotropicHardening; };
  virtual void createIPVariable(IPJ2IsotropicHardening* &ipv) const;
  virtual void initLaws(const std::map<int,J2IsotropicHardening*> &maplaw) {}; //nothing now, we will see if we use the mapping
   virtual void hardening(double p, IPJ2IsotropicHardening &ipv) const;
  virtual J2IsotropicHardening * clone() const;
#endif
};

class PowerLawJ2IsotropicHardening : public J2IsotropicHardening
{
 // R = yield0 + h p^hexp
 protected :
  double _h, _hexp;

 public:
  // constructor
  PowerLawJ2IsotropicHardening(const int num,  double yield0, double h, double hexp, bool init=true);
#ifndef SWIG
  ~PowerLawJ2IsotropicHardening(){}
  PowerLawJ2IsotropicHardening(const PowerLawJ2IsotropicHardening &source);
  PowerLawJ2IsotropicHardening& operator=(const J2IsotropicHardening &source);
  virtual int getNum() const{return _num;}
  virtual hardeningname getType() const{return J2IsotropicHardening::powerLawJ2IsotropicHardening; };
  virtual void createIPVariable(IPJ2IsotropicHardening* &ipv) const;
  virtual void initLaws(const std::map<int,J2IsotropicHardening*> &maplaw) {}; //nothing now, we will see if we use the mapping
  virtual void hardening(double p, IPJ2IsotropicHardening &ipv) const;
  virtual J2IsotropicHardening * clone() const;
#endif
};

class ExponentialJ2IsotropicHardening : public J2IsotropicHardening
{
 // R = yield0 +h * (1-exp (-hexp *p))
 protected :
  double _h, _hexp;

 public:
  // constructor
  ExponentialJ2IsotropicHardening(const int num,  double yield0, double h, double hexp, bool init=true);
#ifndef SWIG
  ~ExponentialJ2IsotropicHardening(){}
  ExponentialJ2IsotropicHardening(const ExponentialJ2IsotropicHardening &source);
  ExponentialJ2IsotropicHardening& operator=(const J2IsotropicHardening &source);
  virtual int getNum() const{return _num;}
  virtual hardeningname getType() const{return J2IsotropicHardening::exponentialJ2IsotropicHardening; };
  virtual void createIPVariable(IPJ2IsotropicHardening* &ipv) const;
  virtual void initLaws(const std::map<int,J2IsotropicHardening*> &maplaw) {}; //nothing now, we will see if we use the mapping
  virtual void hardening(double p, IPJ2IsotropicHardening &ipv) const;
  virtual J2IsotropicHardening * clone() const;
#endif
};

class SwiftJ2IsotropicHardening : public J2IsotropicHardening
{
 // R = yield0 * (1+ h*p)^hexp
 protected :
  double _h, _hexp;

 public:
  // constructor
  SwiftJ2IsotropicHardening(const int num,  double yield0, double h, double hexp, bool init=true);
#ifndef SWIG
  ~SwiftJ2IsotropicHardening(){}
  SwiftJ2IsotropicHardening(const SwiftJ2IsotropicHardening &source);
  SwiftJ2IsotropicHardening& operator=(const J2IsotropicHardening &source);
  virtual int getNum() const{return _num;}
  virtual hardeningname getType() const{return J2IsotropicHardening::swiftJ2IsotropicHardening; };
  virtual void createIPVariable(IPJ2IsotropicHardening* &ipv) const;
  virtual void initLaws(const std::map<int,J2IsotropicHardening*> &maplaw) {}; //nothing now, we will see if we use the mapping
  virtual void hardening(double p, IPJ2IsotropicHardening &ipv) const;
  virtual J2IsotropicHardening * clone() const;
#endif
};

class LinearExponentialJ2IsotropicHardening : public J2IsotropicHardening
{
 // R = yield0 +h1 * p + h2* (1- exp(-hexp*p))
 protected :
  double _h1, _h2, _hexp;

 public:
  // constructor
  LinearExponentialJ2IsotropicHardening(const int num,  double yield0, double h1, double h2, double hexp, bool init=true);
#ifndef SWIG
  ~LinearExponentialJ2IsotropicHardening(){}
  LinearExponentialJ2IsotropicHardening(const LinearExponentialJ2IsotropicHardening &source);
  LinearExponentialJ2IsotropicHardening& operator=(const J2IsotropicHardening &source);
  virtual int getNum() const{return _num;}
  virtual hardeningname getType() const{return J2IsotropicHardening::linearExponentialJ2IsotropicHardening; };
  virtual void createIPVariable(IPJ2IsotropicHardening* &ipv) const;
  virtual void initLaws(const std::map<int,J2IsotropicHardening*> &maplaw) {}; //nothing now, we will see if we use the mapping
  virtual void hardening(double p, IPJ2IsotropicHardening &ipv) const;
  virtual J2IsotropicHardening * clone() const;
#endif
};

#endif //J2ISOTROPICHARDENING_H_
