//
// C++ Interface: terms
//
// Description: Define damage law for non local j2 plasticity
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef _DAMAGELAW_H_
#define _DAMAGELAW_H_
#ifndef SWIG
#include "ipstate.h"
#include "MElement.h"
#include "ipDamage.h"
#include "STensor43.h"
#endif
class DamageLaw{
 public :
  enum damagelawname{lemaitrechabochedamagelaw,powerdamagelaw};
 protected :
  int _num; // number of law (must be unique !)
  bool _initialized; // to initialize law
 public:
  // constructor
#ifndef SWIG
  DamageLaw(const int num, const bool init=true);
  ~DamageLaw(){}
  DamageLaw(const DamageLaw &source);
  DamageLaw& operator=(const DamageLaw &source);
  virtual int getNum() const{return _num;}
  virtual damagelawname getType() const=0;
  virtual void createIPVariable(IPDamage* &ipv) const=0;
  virtual void initLaws(const std::map<int,DamageLaw*> &maplaw)=0;
  virtual const bool isInitialized() {return _initialized;}
  virtual void computeDamage(double p1, double p0, double phiel, const STensor3 &Fe, 
                             const STensor3 &Fp, const STensor3 &Peff,
                             const STensor43 &Cel, const IPDamage &ipvprev, IPDamage &ipvcur) const=0;
  virtual DamageLaw * clone() const=0;
#endif
};

class LemaitreChabocheDamageLaw : public DamageLaw
{
 public :
 protected :
  double p0, pc, n, alpha;
  
 public:
  // constructor
  LemaitreChabocheDamageLaw(const int num, const double _p0, const double _pc, const double _n,
                            const double _alpha, const bool init=true);
  ~LemaitreChabocheDamageLaw(){}
#ifndef SWIG
  LemaitreChabocheDamageLaw(const LemaitreChabocheDamageLaw &source);
  LemaitreChabocheDamageLaw& operator=(const DamageLaw &source);
  virtual damagelawname getType() const {return DamageLaw::lemaitrechabochedamagelaw;};
  virtual void createIPVariable(IPDamage* &ipv) const;
  virtual void initLaws(const std::map<int,DamageLaw*> &maplaw){}; //nothing now, we will see if we use the mapping;
  virtual void computeDamage(double p1, double p0, double phiel, const STensor3 &Fe, 
                           const STensor3 &Fp, const STensor3 &Peff,
                             const STensor43 &Cel, const IPDamage &ipvprev, IPDamage &ipvcur) const;
  virtual DamageLaw * clone() const;
#endif
};


class PowerDamageLaw : public DamageLaw
{
 public :
 protected :
  double p0, pc, alpha, beta;
  
 public:
  // constructor
  PowerDamageLaw(const int num, const double _p0, const double _pc, const double _alpha,
                            const double _beta, const bool init=true);
  ~PowerDamageLaw(){}
#ifndef SWIG
  PowerDamageLaw(const PowerDamageLaw &source);
  PowerDamageLaw& operator=(const DamageLaw &source);
  virtual damagelawname getType() const {return DamageLaw::powerdamagelaw;};
  virtual void createIPVariable(IPDamage* &ipv) const;
  virtual void initLaws(const std::map<int,DamageLaw*> &maplaw){}; //nothing now, we will see if we use the mapping;
  virtual void computeDamage(double p1, double p0, double phiel, const STensor3 &Fe, 
                             const STensor3 &Fp, const STensor3 &Peff,
                             const STensor43 &Cel, const IPDamage &ipvprev, IPDamage &ipvcur) const;
  virtual DamageLaw * clone() const;
#endif
};

#endif //DAMAGELAW_H_
