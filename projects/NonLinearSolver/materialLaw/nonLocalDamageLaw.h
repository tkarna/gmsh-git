//
// C++ Interface: materialLaw
//
// Description: Class container for 2 laws (1 constitutive, 1 damage) for non local approach
//
//
// Author:  <Gauthier BECKER>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef NONLOCALDAMAGELAW_H_
#define NONLOCALDAMAGELAW_H_
#include "mlaw.h"

// empty law to unify old (LLN) style and Gmsh style
class materialLawWithDamage : public materialLaw
{
 public:
  materialLawWithDamage(const int num,const bool init) : materialLaw(num,init){}
  materialLawWithDamage(const materialLawWithDamage &source) : materialLaw(source){}
  materialLawWithDamage& operator=(const materialLaw &source){materialLaw::operator=(source);}
  ~materialLawWithDamage(){}

  // material law (has to be defined in the law)
  virtual matname getType() const=0;
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const=0;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw)=0;
  // specific functions
  virtual const double bulkModulus() const=0;
  virtual const double shearModulus() const=0;
  virtual const double poissonRatio() const=0;
  virtual void createIPState(IPNonLocalDamage *ivi, IPNonLocalDamage *iv1, IPNonLocalDamage *iv2) const=0;
  virtual void createIPVariable(IPNonLocalDamage* &ipv,const MElement *ele, const int nbFF) const=0;
  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPNonLocalDamage *q0,       // array of initial internal variable
                            IPNonLocalDamage *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            const bool stiff            // if true compute the tangents
                           ) const=0;

  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPNonLocalDamage *q0,       // array of initial internal variable
                            IPNonLocalDamage *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            STensor3  &dNonLocalPlasticStrainDStrain,
  			    STensor3  &dStressDNonLocalPlasticStrain,
                            double   &dLocalPlasticStrainDNonLocalPlasticStrain,
                            const bool stiff            // if true compute the tangents
                           ) const=0;
  virtual int getNsdv() const=0;
};

// based on fractureBy2Laws which has to be renamed ??
template<class Tbulk, class Tdamage> class nonLocalDamageLaw : public fractureBy2Laws<Tbulk,Tdamage> , public materialLawWithDamage
{
 public:
  nonLocalDamageLaw(const int num, const int nbulk, const int nfrac) : fractureBy2Laws<Tbulk,Tdamage>(num,nbulk,nfrac), materialLaw(num){}
  nonLocalDamageLaw(const nonLocalDamageLaw &source) : fractureBy2Laws<Tbulk,Tdamage>(source), materialLaw(source){}
  ~nonLocalDamageLaw(){}
  // functions that have to be define as fractureBy2Laws CHANGE THIS
  virtual void initialBroken(IPStateBase *ips) const{Msg::Error("Call initialBroken for a nonLocalDamageLaw");}
  virtual bool fullBroken(const IPVariable *ipv)const{Msg::Error("Call fullBroken for a nonLocalDamageLaw"); return false;};

  // material law
  virtual matname getType() const=0;
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const=0;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw)=0;
  virtual int getNsdv() const=0;
  // specific functions
  virtual void createIPState(IPNonLocalDamage *ivi, IPNonLocalDamage *iv1, IPNonLocalDamage *iv2) const=0;
  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPNonLocalDamage *q0,       // array of initial internal variable
                            IPNonLocalDamage *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            const bool stiff            // if true compute the tangents
                           ) const=0;

  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPNonLocalDamage *q0,       // array of initial internal variable
                            IPNonLocalDamage *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            STensor3  &dNonLocalPlasticStrainDStrain,
  			    STensor3  &dStressDNonLocalPlasticStrain,
                            double   &dLocalPlasticStrainDNonLocalPlasticStrain,
                            const bool stiff            // if true compute the tangents
                           ) const=0;

};

/*
// ADDED By G. Becker 26/03/2012 (Compatibility with old definition of mlawNonLocalDamage)
// mlawNonLocalDamage contains both laws
#ifndef SWIG
class oldNonLocalDamageLaw : public nonLocalDamageLaw<mlawNonLocalDamage,materialLaw>
{
  public:
   oldNonLocalDamageLaw(const int num, const int nbulk, const char *propName) : nonLocalDamageLaw(num,nbulk,-10000000){}
   oldNonLocalDamageLaw(const oldNonLocalDamageLaw &source) : nonLocalDamageLaw(source){}

  virtual matname getType() const{return _mbulk->getType();}
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const{_mbulk->createIPState(ips,state_,ele,nbFF_,gpt);}
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}
  virtual double soundSpeed() const{return _mbulk->soundSpeed();}
  virtual double scaleFactor() const {return _mbulk->scaleFactor();}
  virtual double density() const{return _mbulk->density();}
  virtual const double bulkModulus() const{_mbulk->bulkModulus();}
  virtual const double shearModulus() const{_mbulk->shearModulus();}
  virtual const double poissonRatio() const{_mbulk->poissonRatio();}
  virtual int getNsdv() const{return _mbulk->getNsdv();}

  virtual void createIPState(IPNonLocalDamage *ivi, IPNonLocalDamage *iv1, IPNonLocalDamage *iv2) const{_mbulk->createIPState(ivi,iv1,iv2);}

  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPNonLocalDamage *q0,       // array of initial internal variable
                            IPNonLocalDamage *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            const bool stiff            // if true compute the tangents
                           ) const{_mbulk->constitutive(F0,Fn,P,q0,q1,Tangent,stiff);}

  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPNonLocalDamage *q0,       // array of initial internal variable
                            IPNonLocalDamage *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            STensor3  &dNonLocalPlasticStrainDStrain,
  			                STensor3  &dStressDNonLocalPlasticStrain,
                            double   &dLocalPlasticStrainDNonLocalPlasticStrain,
                            const bool stiff            // if true compute the tangents
                           ) const{_mbulk->constitutive(F0,Fn,P,q0,q1,Tangent,dNonLocalPlasticStrainDStrain,dStressDNonLocalPlasticStrain,
                                                        dLocalPlasticStrainDNonLocalPlasticStrain,stiff);};
};
#endif // SWIG
*/


#endif //NONLOCALDAMAGELAW_H_
