#ifndef LCC_FUNCTIONS_C
#define LCC_FUNCTIONS_C 1

#include "lccfunctions.h"
#include "matrix_operations.h"
#include <stdio.h>
#include <stdlib.h>

#ifdef NONLOCALGMSH
using namespace MFH;
#endif


//*************************************************************************
void malloc_lcc(Lcc* LCC, int N, double vf_i){

	LCC->Nph=N;
        LCC->v1=vf_i;
	//shear and bulk moduli of the phases in the LCC
	mallocvector(&(LCC->mu),N);
	mallocvector(&(LCC->kappa),N);

	//strain concentration tensor (for the inclusion) and derivatives w.r.t. mu0 and mu1 
	//should be generalized for more than 2 phases
	mallocmatrix(&(LCC->A),6,6);
	mallocmatrix(&(LCC->dAdmu0),6,6);
	mallocmatrix(&(LCC->dAdmu1),6,6);
	mallocmatrix(&(LCC->ddAddmu0),6,6);
	mallocmatrix(&(LCC->ddAddmu1),6,6);
	mallocmatrix(&(LCC->ddAdmu0dmu1),6,6);
	
	//effective stiffness of the LCC and derivatives w.r.t. mu0 and mu1
	//should be generalized for more than 2 phases
	mallocmatrix(&(LCC->C),6,6);
	mallocmatrix(&(LCC->C0),6,6);
	mallocmatrix(&(LCC->C1),6,6);
	mallocmatrix(&(LCC->dCdmu0),6,6);
	mallocmatrix(&(LCC->dCdmu1),6,6);
	mallocmatrix(&(LCC->ddCddmu0),6,6);
	mallocmatrix(&(LCC->ddCddmu1),6,6);
	mallocmatrix(&(LCC->ddCdmu0dmu1),6,6);

	//square of an equivalent strain measure and derivatives w.r.t to mu and epsilon
        mallocmatrix(&(LCC->dCdp_bar),6,6);
        mallocmatrix(&(LCC->ddCdmu0dp_bar),6,6);
        mallocmatrix(&(LCC->ddCdmu1dp_bar),6,6); 
        malloctens3(&(LCC->dCdstrn),6,6,6); 
        malloctens3(&(LCC->ddCdmu0dstrn),6,6,6); 
        malloctens3(&(LCC->ddCdmu1dstrn),6,6,6);
}

void set_elprops(Lcc* LCC, double* mu, double* kappa){
	
	if(LCC->Nph==1){
		LCC->mu[0]=mu[0];
                LCC->kappa[0]=kappa[0];
		makeiso(LCC->kappa[0],LCC->mu[0],LCC->C0);
	}

	else if(LCC->Nph==2){
		LCC->mu[0]=mu[0];
		LCC->mu[1]=mu[1];
		LCC->kappa[0]=kappa[0];
		LCC->kappa[1]=kappa[1];
		makeiso(LCC->kappa[0],LCC->mu[0],LCC->C0);
		makeiso(LCC->kappa[1],LCC->mu[1],LCC->C1);
	}
	else{
		printf("Case of LCC with more than two phase not allowed!\n");
	}
	
}


void free_lcc(Lcc* LCC){

	int Nph=LCC->Nph;

	free(LCC->mu);
	free(LCC->kappa);
	freematrix(LCC->A,6);
	freematrix(LCC->dAdmu0,6);
	freematrix(LCC->dAdmu1,6);
	freematrix(LCC->ddAddmu0,6);
	freematrix(LCC->ddAddmu1,6);
	freematrix(LCC->ddAdmu0dmu1,6);

	freematrix(LCC->C,6);
	freematrix(LCC->C0,6);
	freematrix(LCC->C1,6);
	freematrix(LCC->dCdmu0,6);
	freematrix(LCC->dCdmu1,6);
	freematrix(LCC->ddCddmu0,6);
	freematrix(LCC->ddCddmu1,6);
	freematrix(LCC->ddCdmu0dmu1,6);

        freematrix(LCC->dCdp_bar,6);
        freematrix(LCC->ddCdmu0dp_bar,6);
        freematrix(LCC->ddCdmu1dp_bar,6); 
        freetens3(LCC->dCdstrn,6,6); 
        freetens3(LCC->ddCdmu0dstrn,6,6); 
        freetens3(LCC->ddCdmu1dstrn,6,6);

}

void malloc_YF(YieldF* YFdf){

	//derivatives w.r. strain
	mallocvector(&(YFdf->dfdstrn),6);
	mallocvector(&(YFdf->dpdstrn),6);

	mallocvector(&(YFdf->dfdmu),2);
	mallocvector(&(YFdf->dpdmu),2);
}

void free_YF(YieldF* YFdf){

	free(YFdf->dfdstrn);
	free(YFdf->dpdstrn);

	free(YFdf->dfdmu);
	free(YFdf->dpdmu);
}



//************************************************************************
// COMPUTE MAROSTIFFNESS AND STRAIN CONCENTRATION TENSORS AND THEIR FIRST DERIVATIVES
//************************************************************************
void CA_dCdA_MT(Lcc* LCC, double v1, double **S, double **dS, double *dnu){

	
}


//************************************************************************
// COMPUTE MAROSTIFFNESS AND STRAIN CONCENTRATION TENSORS AND THEIR FIRST  and second DERIVATIVES
//************************************************************************
void CA_2ndCdA_MT(Lcc* LCC, double v1, double **S, double **dS, double **ddS, double *dnu){

	
}



//****************************************************************
//COMPUTE EQUIVALENT STRAIN AND DERIVATIVES - 2nd order 
//****************************************************************
void eqstrn2_order2(int mtx, double* e, Lcc* LCC,double* es2, double* des2de, double* des2dmu0, double* des2dmu1){

			
}


#endif
