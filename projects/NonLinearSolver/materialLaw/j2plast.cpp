#ifndef J2PLAST_C
#define J2PLAST_C 1

#ifndef max
#define max(a,b) ( ((a) > (b)) ? (a) : (b) )
#endif


#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "j2plast.h"
#include "matrix_operations.h"

#ifdef NONLOCALGMSH
#include "ID.h"
#endif

#define DP_MIN 1e-16

//ELASTO-PLASTIC CONSTITUTIVE BOX
int constbox_ep(double E, double nu, double sy0, int htype, double hmod1, double hmod2,
	double hexp, double *dstrn, double *strs_n, double *strs, double* pstrn_n, double* pstrn,
	double p_n, double *p, double *twomu_iso, double* dtwomu_iso,  double** Calgo, double*** dCalgo, double* dpdE, bool residual)

{
	int i,j,k,it,error;
	double strstr[6];
	double strstrNor0[6];              //used to compute the modified normal
	double strseqtr, strseqtrNor0, trstrstr;       	//Variables for the elastic prediction 
	double R, dR, ddR, strseq, G;    //Variables for the correction phase 
	double Str[6], Ntr[6], Stau[6];
        double Ntr_o[6], invNtr_o[6], Str_o[6];       //add by ling wu to keep the original normal of yieding surface
	double dstrs[6];
	double kp;
	double Dp;  
	double h, tmp1, tmp2;
	double cp=0;
	double tol = 1e-12;    //tolerance on the residual 
	double res, res0;    		 //residual 
	double mu_iso, k_iso;
	double G3, h3;
        double sq2;

        sq2 = sqrt(2.);
	R=0.;
	dR=0.;
	ddR=0.;

	G = E/(2.*(1.+nu));  
	k_iso = E/(3.*(1.-(2.*nu)));
	G3 = G*G*G;

        static bool initialized=false;
        // initialize only once!
	static double **Cel;
	static double **NoN;
	static double **Idev;
	static double **mat1;
	static double **N_o;
	static double **invN_o;
        if(!initialized)
        {
          mallocmatrix(&Cel, 6, 6);
	  mallocmatrix(&NoN,6,6);
	  mallocmatrix(&Idev,6,6);
	  mallocmatrix(&mat1,6,6);
	  mallocmatrix(&N_o,3,3);
	  mallocmatrix(&invN_o,3,3);
          initialized = true;
        }
	//deviatoric operator
	for(i=0;i<3;i++){
	  for(j=0;j<3;j++){
	    Idev[i][j]=-1./3.;
          }
	  Idev[i][i]+=1.;
	  Idev[i+3][i+3]=1.;
	}
	//reset tangent operators to zero
	for(i=0;i<6;i++){
		for(j=0;j<6;j++){
			Calgo[i][j]=0.;
		}
	}

	//elastic predictor
	compute_Hooke(E,nu,Cel);

	//compute trial stress
	contraction42(Cel, dstrn, dstrs);
	if(residual)
	  addtens2(1, dstrs,   0, dstrs, strstrNor0);
	else
	  addtens2(1, strs_n,   1, dstrs, strstrNor0);
	addtens2(1, strs_n, 1, dstrs, strstr);
        strseqtrNor0 = vonmises(strstrNor0);
	strseqtr = vonmises(strstr);
  
	//normal to the yield surface
	dev(strstrNor0, Str);
	dev(strstr, Str_o);
  
	if(strseqtrNor0 > 0){
		addtens2(1.5/strseqtrNor0, Str, 0, Str, Ntr);
		produit_tensoriel(Ntr,Ntr,NoN);
	}

	if(strseqtr > 0 && residual){
		addtens2(1.5/strseqtr, Str_o, 0, Str_o, Ntr_o);
	
                //get the inverse of Ntr_o
                 for (i=0; i<3; i++){
                        N_o[i][i] = Ntr_o[i];
                 }
                 N_o[0][1] = N_o[1][0] = Ntr_o[3]/sq2;
                 N_o[0][2] = N_o[2][0] = Ntr_o[4]/sq2;
                 N_o[1][2] = N_o[2][1] = Ntr_o[5]/sq2;

                 inverse (N_o, invN_o, &error, 3);
                 if(error!=0){
                   for(i=0;i<3;i++)
                      for(j=0;j<1;j++)
                        invN_o[i][j]=N_o[i][j]*2; //to get one in N:N-1/3
                 }

                 for (i=0; i<3; i++){
                        invNtr_o[i] = invN_o[i][i];
                 }
                 invNtr_o[3] = invN_o[0][1]*sq2;
                 invNtr_o[4] = invN_o[2][0]*sq2;
                 invNtr_o[5] = invN_o[2][1]*sq2;
          }
         
	//elastic increment
	Dp=DP_MIN;
	j2hard (p_n+Dp, sy0, htype, hmod1, hmod2, hexp, &R, &dR,&ddR);
	strseq= sy0 + R;
	h = (3.*G) + dR;
	kp = strseq + 3.*G*Dp - strseqtr;

	if(kp>=0.){
		//printf("elastic: !");
		copyvect(strstr, strs, 6);
		copyvect(pstrn_n, pstrn,6);
    		*p = p_n;
		Dp=0.;
		copymatr(Cel,Calgo,6,6);
		for(i=0;i<6;i++){
			for(j=0;j<6;j++){
				for(k=0;k<6;k++){
					dCalgo[i][j][k]=0.;
				}
			}
		}
      //***** By Wu Ling 06/04/2011************************************************************************************
       //compute dpdE  accumulate plastice strain p derivative w.r.t. strain increment

       cleartens2(dpdE);


       //******* By Wu Ling 06/04/2011**********************************************************************************

	}
	//Plastic correction: return mapping algorithm 
	else{
                if(residual){
                    h=0.0;       //compute h according to modified normal
                    for (i=0; i<6; i++){
                         h = h + Ntr[i]*invNtr_o[i];
                    }
                    h =  h*dR/3.0 + (3.*G); 
                }

		Dp = max(DP_MIN,-kp/h);  //correction must be such that Dp >= DP_MIN

		it=0;
		res=1;
		res0=res;

		while(res>tol){
		
			it++;
			if(it>20){
				printf("no convergence in return mapping algorithm, p: %lf\n",*p);
				
				return(1);
			}

			*p=p_n+Dp;
			j2hard (*p, sy0, htype, hmod1, hmod2, hexp, &R, &dR,&ddR);
			kp = 3.*G*Dp + R + sy0 - strseqtr;
      
			res = fabs(kp)/G;

			if(it==1||res<res0){
				res0=res;
				cp =  - kp/(3.*G+dR);
				tmp1 = 0.99*(DP_MIN-Dp);
				if(cp<tmp1) {
					//printf("new Dp below Dpmin : correction!\n");
					cp=tmp1;
				}
				Dp+=cp;
			}
			else{
				//printf("residual is not decreasing: correction!\n");
				cp=cp/2.;
				Dp=Dp-cp;
			}

			//printf("j2plast iteration: %d, res: %.10e, p: %.10e\n",it,res,*p);
		}
		//printf("Number of iterations in radial return algorithm: %d\n",it); 
		strseq = sy0 + R;
		if(residual)
		{	
		     addtens2(1., strstr,-2.*G*(Dp), Ntr, Stau);
		     for (i=0;i<3;i++) {
	            //strstr is not deviatoric
		       strs[i] = Stau[i]; // + (1.0/3.0)*trstrstr;
		       strs[i+3] = Stau[i+3];
		     }

                     h=0.0;       //compute h according to modified normal
                     for (i=0; i<6; i++){
                          h = h + Ntr[i]*invNtr_o[i];
                     }
                     h =  h*dR/3.0 + (3.*G);
		}
		else
		{
		  addtens2(2.0*strseq/3.0, Ntr, 0, Ntr, Stau);
		  trstrstr = trace(strstr);
		  for (i=0;i<3;i++) {
			strs[i] = Stau[i] + (1.0/3.0)*trstrstr;
			strs[i+3] = Stau[i+3];
		  }
                  h=(3.*G) + dR;
		}
		addtens2(1.,pstrn_n, Dp, Ntr, pstrn);

		h3 = h*h*h;
		tmp1 = 4.*G*G/h;
		tmp2 = 4.*G*G*(*p-p_n)/strseqtr;

		addtens4(1.,Cel,(-tmp1+tmp2),NoN, Calgo);
		addtens4(1.,Calgo,-1.5*tmp2,Idev,Calgo);

		addtens4(1.5,Idev,-1.,NoN,mat1);
		for(i=0;i<6;i++){
			for(j=0;j<6;j++){
				for(k=0;k<6;k++){
					dCalgo[i][j][k] = (8.*G3*ddR/h3)*NoN[i][j]*Ntr[k] - (16.*G3/(h*strseqtr))*Ntr[i]*mat1[j][k] - mat1[i][j]*8.*G3*Ntr[k]*((1./(h*strseqtr))-(Dp/(strseqtr*strseqtr))) + (16.*G3*Dp/(strseqtr*strseqtr))*Ntr[i]*mat1[j][k];
				}
			}
		}

       //***** By Wu Ling 06/04/2011************************************************************************************
       //compute dpdE  accumulate plastice strain p derivative w.r.t. strain increment

       addtens2((2.*G/h), Ntr, 0, Ntr, dpdE);


       //******* By Wu Ling 06/04/2011**********************************************************************************


	} //end case plastic correction



       


	//compute isoJ2 tangent operator and its derivative w.r.t. strain increment
	if(Dp<DP_MIN){		//that is, Dp=0 	
		mu_iso=G;
		for(i=0;i<6;i++){
			dtwomu_iso[i]=0.;
		}
	}
		
	else {
		mu_iso = G*(1.-(3.*G/h));
		for(i=0;i<6;i++){
			dtwomu_iso[i] = 12.*pow(G/h,3)*ddR*Ntr[i];
		}
	}
  
	*twomu_iso = 2.*mu_iso;

//      no free as initialized once        
//     	freematrix(Cel, 6);
//	freematrix(Idev,6);
//	freematrix(NoN,6);
//	freematrix(mat1,6);
	return(0);
}

// EVALUATION OF THE HARDENING FUNCTION 
void j2hard (double p, double sy0, int htype, double hmod1, double hmod2, double hexp, double* R, double* dR, double* ddR){

	double tmp;

	//Perfect plasticity
	if(hmod1 == 0){
		
		*R = 0.;
		*dR = 0.;
		*ddR = 0.;
		return;

	}

	switch(htype) {
	
	//POWER LAW HARDENING
	case H_POW:

		if(p<DP_MIN){
			printf("Should not be here: p: %.10e\n",p);
			*R=0.;
			if(hmod1<1){
				*dR = 1e20;
				*ddR = -1e20;
			}
			else{
				*dR = hmod1;
				*ddR = 0.;
			}		
		}	
		else{
			*R=hmod1*pow(p,hexp);
			*dR = (*R)*hexp/p;
			*ddR = (*dR)*(hexp-1.)/p;
		}

		break;	
			
	//EXPONENTIAL HARDENING
	case H_EXP:
	
		if(p<DP_MIN){
			printf("Should not be here: p: %.10e\n",p);
			*R=0.;
			*dR = hmod1*hexp;
			*ddR = -hexp*(*dR);
		} 
		else{
			tmp = exp(-hexp*p);
			*R = hmod1*(1.-tmp);
			*dR = hmod1*hexp*tmp;
			*ddR = -hexp*(*dR);
		}
		break;
	
	//SWIFT LAW
	case H_SWIFT:
		if(p<DP_MIN){
			printf("Should not be here: p: %.10e\n",p);
			*R=0.;
			*dR = sy0*hmod1*hexp;
			*ddR = (*dR)*(hexp-1.)*hmod1;
		}
		else{
			tmp = 1.+hmod1*p;
			*R = sy0*pow(tmp,hexp) - sy0;
			tmp = hmod1/tmp;
			*dR = (*R)*hexp*tmp;
			*ddR = (*dR)*(hexp-1.)*tmp;
		}
		break;

	//LINEAR-EXPONENTIAL HARDENING
	case H_LINEXP:
	
		if(p<DP_MIN){
			printf("Should not be here: p: %.10e\n",p);
			*R = 0;
			*dR = hmod1 + hmod2*hexp;
			*ddR = -hmod2*hexp*hexp;
		}
		else{
			tmp = exp(-hexp*p);
			*R = hmod1*p + hmod2*(1.-tmp);
			*dR = hmod1 + hmod2*hexp*tmp;
			*ddR = - hmod2*hexp*hexp*tmp; 
		}
		break;
	
	default:
		printf("Bad choice of hardening law in j2hard: %d\n",htype);
		break;
		
	}

	//printf("p: %lf\t, R: %.10e, \t dR: %.10e \t, ddR: %.10e \n",p, *R,*dRdp,*ddRdp);
 
}

/***********************************************
 * COMPUTATION OF ALGORITHMIC TANGENT OPERATOR
 * FROM STATE VARIABLES 
 * ********************************************/
void algoJ2(double E, double nu, double sy0, int htype, double hmod1, double hmod2, double hexp, 
	double* strs, double p, double dp, double** Calgo){

	int i,j;
	double seqtr;
	double R,dR,ddR;
	double mu;
	double *N, *devstrs;
	double** Cel, **mat1, **mat2;
	double** NoN;
	double** Idev;
	double h, tmp1, tmp2;

	mallocvector(&devstrs,6);
	mallocvector(&N,6);
	mallocmatrix(&Cel,6,6);
	mallocmatrix(&NoN,6,6);
	mallocmatrix(&mat1,6,6);
	mallocmatrix(&mat2,6,6);
	mallocmatrix(&Idev,6,6);

  //deviatoric operator
  for(i=0;i<3;i++){
		for(j=0;j<3;j++){
		  Idev[i][j]=-1./3.;

		}
		Idev[i][i]+=1.;
		Idev[i+3][i+3]=1.;
	}
  //reset tangent operators to zero
	mu = E/(2.*(1.+nu));

	compute_Hooke(E,nu,Cel);

	dev(strs,devstrs);
	
	j2hard (p, sy0, htype, hmod1, hmod2, hexp, &R, &dR, &ddR);

	addtens2(1.5/(sy0+R),devstrs,0,devstrs,N);
	seqtr = 3*mu*dp + sy0 + R;
	produit_tensoriel(N,N,NoN);
	
	h = 3.*mu + dR;
	tmp1 = 4.*mu*mu/h;
	tmp2 = 4.*mu*mu*dp/seqtr;

	addtens4(tmp1,NoN,0,NoN,mat1);
	addtens4(3.*tmp2/2.,Idev,-tmp2,NoN,mat2);

	addtens4(1.,Cel,-1.,mat1,Calgo);
	addtens4(1.,Calgo,-1.,mat2,Calgo);

	free(devstrs);
	free(N);
	freematrix(Cel,6);
	freematrix(mat1,6);
	freematrix(mat2,6);
	freematrix(Idev,6);
	freematrix(NoN,6);


}


#endif








