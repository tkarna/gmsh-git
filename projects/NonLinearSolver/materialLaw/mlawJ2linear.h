//
// C++ Interface: material law
//
// Description: j2 linear elasto-plastic law it is a "virtual implementation" you have to define a law
//              in your project which derive from this law. you have to do THE SAME for your IPVariable
//              which have to derive from IPJ2linear (all data in this ipvarible have name beggining by _j2...
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef MLAWJ2LINEAR_H_
#define MLAWJ2LINEAR_H_
#include "mlaw.h"
#include "STensor3.h"
#include "STensor43.h"
#include "ipJ2linear.h"
class mlawJ2linear : public materialLaw
{
 protected:
  J2IsotropicHardening *_j2IH;
  // can't be const due to operator= constructor (HOW TO CHANGE THIS ??)
  double _rho; // density
  double _E; // young modulus
  double _nu; // Poisson ratio
  double _lambda; // 1st lame parameter
  double _mu; // 2nd lame parameter (=G)
  double _K; // bulk modulus
  double _K3; // 3*bulk modulus
  double _mu3; // 3*_mu = 3*G
  double _mu2; // 2*_mu = 2*G
  double _tol; // tolerance for iterative process
  double _perturbationfactor; // perturbation factor
  bool _tangentByPerturbation; // flag for tangent by perturbation
#ifndef SWIG
 private: // cache data for constitutive function
  mutable STensor3 F1,Fp1,Fp0;
  mutable STensor43 L;
 private: // cache data for update function
  mutable STensor3 Fpinv,Fe,Ce,Ee,S,Sig,M,dFp;
 private: // cache data for tangent_full function can use the cache data of update function (these function don't call themselves)
  mutable STensor3 Fp0inv,Fe0,Epre,Finv;
  mutable STensor43 tmp,Ll,Lr,tmpdFpdF,dFpdE,KK;
 private: // cache data for tangent function
  mutable STensor3 coefEpre,MM,Aux;
  mutable STensor43 ddeM,iL;
 private: // cache function for constitutive_elastic_stiffness function (be aware it call constitutive)
  mutable STensor3 eFp1;
#endif // SWIG
 public:
  mlawJ2linear(const int num,const double E,const double nu, const double rho,const double sy0,const double h,const double tol=1.e-6,
              const bool matrixbyPerturbation = false, const double pert = 1e-8);
  mlawJ2linear(const int num,const double E,const double nu, const double rho,const J2IsotropicHardening &_j2IH,const double tol=1.e-6,
              const bool matrixbyPerturbation = false, const double pert = 1e-8);
 #ifndef SWIG
  mlawJ2linear(const mlawJ2linear &source);
  mlawJ2linear& operator=(const materialLaw &source);
  ~mlawJ2linear()
  {
    if(_j2IH!=NULL) delete _j2IH;
  }

  // function of materialLaw
  virtual matname getType() const{return materialLaw::j2linear;}
  virtual void createIPState(IPStateBase* &ips,const bool* state_=NULL,const MElement *ele=NULL, const int nbFF_=0, const int gpt = 0) const;
  virtual void initLaws(const std::map<int,materialLaw*> &maplaw){}; // this law is initialized so nothing to do
  virtual double soundSpeed() const; // default but you can redefine it for your case
  virtual double density()const{return _rho;}
  virtual const double bulkModulus() const{return _K;}
  virtual const double shearModulus() const{return _mu;}
  virtual const double poissonRatio() const{return _nu;}
  virtual const J2IsotropicHardening * getJ2IsotropicHardening() const {return _j2IH;}
  // specific function
 public:
  virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPJ2linear *q0,       // array of initial internal variable
                            IPJ2linear *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            const bool stiff            // if true compute the tangents
                           ) const;
  virtual void tangent_full(
                             STensor43 &T_,
                             const STensor3 &P_,
                             const STensor3 &F_,
                             const STensor3 &Fp_,
                             const STensor3 &Fp1_,
                             const IPJ2linear* q0,
                             const IPJ2linear* q1
                           ) const
  {
    tangent_full(T_,P_,F_,Fp_,Fp1_,q0,q1, tmpdFpdF);
  }
  virtual void tangent_full_perturbation(
                             STensor43 &T_,
                             const STensor3 &P_,
                             const STensor3 &F_,
                             const STensor3 &Fp_,
                             const STensor3 &Fp1_,
                             const IPJ2linear* q0,
                             const IPJ2linear* q1
                           ) const
  {
    tangent_full_perturbation(T_,P_,F_,Fp_,Fp1_,q0,q1, tmpdFpdF);

  }

 protected:
   virtual void constitutive(
                            const STensor3& F0,         // initial deformation gradient (input @ time n)
                            const STensor3& Fn,         // updated deformation gradient (input @ time n+1)
                            STensor3 &P,                // updated 1st Piola-Kirchhoff stress tensor (output)
                                                        // contains the initial values on input
                            const IPJ2linear *q0,       // array of initial internal variable
                            IPJ2linear *q1,             // updated array of internal variable (in ipvcur on output),
                            STensor43 &Tangent,         // constitutive tangents (output)
                            const bool stiff,           // if true compute the tangents
                             STensor43 &dFpdF           // used when damage
                           ) const;
  virtual void update(
                       const STensor3& F,
                       const STensor3& Fp0_,
                       STensor3& Fp1_,
                       const IPJ2linear *q0,
                       IPJ2linear *q1,
                       STensor3&P
                      ) const;
  virtual void stress(
                       STensor3 &Sig_,
                       const STensor3 &E_
                     ) const;
  virtual void J2flow(
                       STensor3 &M_,
                       const STensor3 &Ee_
                     ) const;
  //virtual void harden(
  //                     const double epspbarre,
  //                     double &Sy,
  //                     double &H
  //                   ) const;
  virtual void tangent_full(
                             STensor43 &T_,
                             const STensor3 &P_,
                             const STensor3 &F_,
                             const STensor3 &Fp_,
                             const STensor3 &Fp1_,
                             const IPJ2linear* q0,
                             const IPJ2linear* q1,
                             STensor43 &dFpdF
                           ) const;
  virtual void tangent_full_perturbation(
                             STensor43 &T_,
                             const STensor3 &P_,
                             const STensor3 &F_,
                             const STensor3 &Fp_,
                             const STensor3 &Fp1_,
                             const IPJ2linear* q0,
                             const IPJ2linear* q1,
                             STensor43 &dFpdF
                           ) const;
  virtual void tangent(
                        STensor43 &K_,
                        STensor3 &Epre_,
                        const IPJ2linear *q0_,
                        const IPJ2linear *q1_,
                        STensor43 &dFpdE_
                      ) const;
  virtual void constitutive_elastic_stiffness(
                                               const STensor3 &F0,   /* initial deformation gradient (input @ time n) */
                                               const STensor3 &Fn,   /* updated deformation gradient (input @ time n+1) */
                                               STensor3 &P,          /* updated 1st Piola-Kirchhoff stress tensor (output) */
                                               const IPJ2linear *q0, /* initial internal variables */
                                               IPJ2linear *q1,        /* updated internal variables (output) */
                                               STensor43 &Tangent,   /* constitutive tangent (output) */
                                               STensor43 &ElTangent, /* elastic part of the constitutive tangents (output) */
                                               bool stiff            /* if true compute the tangents */
                                             ) const;
  protected:
  virtual double deformationEnergy(const STensor3 &C) const ;

  virtual double determinantSTensor3(const STensor3 &a) const;
  virtual void inverseSTensor3(const STensor3 &a, STensor3 &ainv) const;
  virtual void multSTensor3(const STensor3 &a, const STensor3 &b, STensor3 &c) const;
  virtual void multSTensor3FirstTranspose(const STensor3 &A, const STensor3 &B, STensor3 &C) const;
  virtual void multSTensor3SecondTranspose(const STensor3 &a, const STensor3 &b, STensor3 &c) const;
  virtual void multSTensor43STensor3(const STensor43 &t, const STensor3 &m,STensor3 &val) const;
  virtual void multSTensor43(const STensor43 &a, const STensor43 &b, STensor43 &c) const;
  virtual void multSTensor43Trans(const STensor43 &a, const STensor43 &b, STensor43 &c) const;
  virtual void addtensprod(const STensor3 &a, const STensor3 &b, STensor43 &c) const;
  virtual void logSTensor3(const STensor3& a, STensor3 &loga, STensor43 *dloga=NULL) const;
  virtual void expSTensor3(const STensor3 &a,STensor3 &expa,STensor43 *dexpa=NULL) const;

 #endif // SWIG
};

#endif // MLAWJ2LINEAR_H_
