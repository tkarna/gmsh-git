Be aware to the definition of GaussQuadrature(int order). It is defined a Gauss' integration rule with a number of points (npts) != order. The rules depends on the MElement:

For MLine:
npts = (order+1)/2

For Mtriangle:
if order < 21
order = 0 --> npts = 1
order = 1 --> npts = 1
order = 2 --> npts = 3 
order = 3 --> npts = 4
order = 4 --> npts = 6
order = 5 --> npts = 7
order = 6 --> npts = 12
order = 7 --> npts = 13
order = 8 --> npts = 16
order = 9 --> npts = 19
order = 10 --> npts = 25
order = 11 --> npts = 27
order = 12 --> npts = 33
order = 13 --> npts = 37
order = 14 --> npts = 42
order = 15 --> npts = 48
order = 16 --> npts = 52
order = 17 --> npts = 61
order = 18 --> npts = 70
order = 19 --> npts = 73
order = 20 --> npts = 79
otherwise (order >=21)
npts = (0.5*(order+3))^2

MQuadrangle:
order = 0 --> npts = 1
order = 1 --> npts = 1
order = 2 --> npts = 4
order = 3 --> npts = 4
otherwise (order > 3)
npts = (0.5*(order+3))^2

Please complete this for other MElement
