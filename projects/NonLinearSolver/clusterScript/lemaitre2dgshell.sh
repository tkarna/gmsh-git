#!/bin/bash

#SBATCH --job-name=mpiBeamBenchmark
#
#SBATCH --mail-user=gauthier.becker@ulg.ac.be
#SBATCH --mail-type=ALL
#
#SBATCH --output="dgshell.out"

#SBATCH --ntasks=2
#SBATCH --mem-per-cpu=50
#SBATCH --time=0:5:0

# file .py  to launch computation 
PYFILE=beam.py

# command (normally souldn't edit) 
module load openmpi/1.4.5/gcc-4.6.2 # should correspond to the mpi version you used to install Gmsh and Petsc
mpirun python $PYFILE
