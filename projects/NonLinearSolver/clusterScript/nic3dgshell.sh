#!/bin/sh

# script example for nic3.ulg.ac.be
# the #dollar are pseudo command for qsub so they have to be edited !!


#$ -N beamTest
#$ -m beas
#$ -M gauthier.becker@ulg.ac.be
#$ -l highmem=true
#$ -l h_vmem=400M
#$ -l h_rt=0:06:00
#$ -pe openmpi 4
#$ -cwd
#$ -j yes

source /etc/profile.d/modules.sh 
source /etc/profile.d/segi.sh 

module add openmpi/gcc/64/1.3.0
subdir=$HOME/workspace/dynrel
echo "subdir=" $subdir

workdir=/tmpscratch/${USER}_$JOB_ID
echo "workdir=" $workdir

pyfile=beam.py
mshfile=beam.msh


echo "cat \$TMPDIR/machines | uniq"
cat $TMPDIR/machines | uniq

for ii in `cat $TMPDIR/machines | uniq`
	do
		echo $ii
		ssh $ii mkdir $workdir 
		ssh $ii cp $subdir/$pyfile $workdir/ 
		ssh $ii cp $subdir/$mshfile $workdir/ 
	done

cd $workdir # needed to work in the tmpscrach dir otherwise work on home !!
#ls -artl
export PETSC_DIR=$HOME/petsc31p8
export PETSC_ARCH=linux-gnu-cxx-opt
export PYTHONPATH=$PYTHONPATH:/u/gbecker/gmsh/projects/dgshell/release:/u/gbecker/gmsh/projects/dgshell/release/NonLinearSolver/gmsh/wrappers
export LD_PRELOAD=$LD_PRELOAD:/cvos/shared/apps/openmpi/gcc/64/1.3.0/lib64/libmpi.so


mpiexec -n 4 python  $workdir/$pyfile >& $workdir/output.txt

echo -e "\n"

sleep 5

for ii in `tac $TMPDIR/machines | uniq`
  do
     ssh $ii mv $workdir/output.txt $subdir/output_$ii.txt
     ssh $ii mv $workdir/previousScheme $subdir/previousScheme_$ii # if a previous scheme exist
     ssh $ii mv $workdir/* $subdir/ # copy archiving files
     ssh $ii rm -rf $workdir
  done
echo -e "\n"
