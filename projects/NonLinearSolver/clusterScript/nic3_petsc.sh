#!/bin/sh

# script example for nic3.ulg.ac.be
# the #dollar are pseudo command for qsub so they have to be edited !!


#$ -N beamTest
#$ -m beas
#$ -M l.noels@ulg.ac.be
#$ -l h_vmem=400M
#$ -l h_rt=0:06:00
#$ -cwd

source /etc/profile.d/modules.sh 
source /etc/profile.d/segi.sh 

module add openmpi/gcc/64/1.3.0
SUBDIR=/u/lnoels/local/petsc
echo "SUBDIR=" $SUBDIR

# command (normally souldn't edit)

$SUBDIR/./conftest

echo Finish
~                   
