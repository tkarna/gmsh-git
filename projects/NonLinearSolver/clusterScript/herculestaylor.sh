#!/bin/sh

# script example for dragon1
#SBATCH --job-name taylorTetDGDynPart4
#SBATCH --mail-user=l.noels@ulg.ac.be
#SBATCH --mail-type=ALL
#SBATCH --output="out.txt"
#SBATCH --ntasks=4
#SBATCH --mem-per-cpu=800
#SBATCH --time=0:20:0

SUBDIR=$HOME/gmsh/projects/dG3D/benchmarks/taylor
echo "subdir=" $SUBDIR

SCRATCH=/workdir/$USER/$SLURM_JOB_ID
echo "workdir=" $SCRATCH


pyfile=taylorTetDGDynPart4.py
mshfile=taylorTetPart4.msh
echo "node list of job"  >> $SUBDIR/output.po$SLURM_JOB_ID

srun hostname >> $SUBDIR/output.po$SLURM_JOB_ID

mkdir -p $SCRATCH || exit $?
cp -f $SUBDIR/$pyfile $SCRATCH/ || exit $?
cp -f $SUBDIR/$mshfile $SCRATCH/ || exit $? 

cd $SCRATCH # needed to work in the tmpscrach dir otherwise work on home !!
#ls -artl

export PETSC_DIR=$HOME/local/petsc-3.4.3
export PETSC_ARCH=linux-gnu-c-opt

export PATH=$HOME/local/bin:$PATH
export PATH=$PATH:$HOME/gmsh/projects/dG3D/release
export PATH=$PATH:$HOME/gmsh/projects/dG3D/release/NonLinearSolver/gmsh

export PYTHONPATH=$PYTHONPATH:$HOME/gmsh/projects/dG3D/release:$HOME/gmsh/projects/dG3D/release/NonLinearSolver/gmsh/wrappers

mpirun python  $SCRATCH/$pyfile > $SCRATCH/output.txt 2>&1

echo -e "\n"

sleep 5

cp -f $SCRATCH/* $SUBDIR/  || exit $? 
rm -rf $SCRATCH ||  exit $? 
echo -e "\n"




