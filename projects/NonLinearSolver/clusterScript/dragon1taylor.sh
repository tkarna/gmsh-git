#!/bin/sh

# script example for dragon1
#SBATCH --job-name taylorTetDGDynPart4
#SBATCH --mail-user=l.noels@ulg.ac.be
#SBATCH --mail-type=ALL
#SBATCH --output="out.txt"
#SBATCH --ntasks=4
#SBATCH --mem-per-cpu=800
#SBATCH --time=0:20:0


module load openmpi/1.6.1/gnu64-4.7.3

SUBDIR=$HOME/gmsh/projects/dG3D/benchmarks/taylor
echo "subdir=" $SUBDIR

SCRATCH=/scratch/${USER}_$SLURM_JOB_ID
echo "workdir=" $SCRATCH


pyfile=taylorTetDGDynPart4.py
mshfile=taylorTetPart4.msh
echo "node list of job"  >> $SUBDIR/output.po$SLURM_JOB_ID

srun hostname >> $SUBDIR/output.po$SLURM_JOB_ID

srun mkdir -p $SCRATCH || exit $?
srun cp $SUBDIR/$pyfile $SCRATCH/ || exit $?
srun cp $SUBDIR/$mshfile $SCRATCH/ || exit $? 

cd $SCRATCH # needed to work in the tmpscrach dir otherwise work on home !!
#ls -artl

export PETSC_DIR=$HOME/local/petsc-3.3-p7
export PETSC_ARCH=linux-gnu-c-opt

export PATH=$HOME/local/bin:$PATH
export PATH=$PATH:$HOME/gmsh/projects/dG3D/release
export PATH=$PATH:$HOME/gmsh/projects/dG3D/release/NonLinearSolver/gmsh

export PYTHONPATH=$PYTHONPATH:$HOME/gmsh/projects/dG3D/release:$HOME/gmsh/projects/dG3D/release/NonLinearSolver/gmsh/wrappers

mpirun python  $SCRATCH/$pyfile >& $SCRATCH/output.txt

echo -e "\n"

sleep 5

srun cp -f $SCRATCH/* $SUBDIR/  || exit $? 
srun rm -rf $SCRATCH ||  exit $? 
echo -e "\n"




