//
//
// Description: Class for tracking fragment
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef _PPFRAGMENT_H_
#define _PPFRAGMENT_H_
#include <utility>
#include "MElement.h"
#include "MInterfaceElement.h"
class interfaceFragment
{
 protected:
  std::pair<MElement*,MElement*> pairElem;
  std::pair<double,double> pairDensity;
 public:
  interfaceFragment(const MInterfaceElement *ielem,const double rhominus,const double rhoplus);
  interfaceFragment(const interfaceFragment &source);
  ~interfaceFragment(){};
  // true if ele is included in interfragment
  bool matchElem(const MElement *ele) const;
  double getDensity(const int i) const;
  MElement* getElem(const int i) const;
  // needed for std::set
  bool operator<(const interfaceFragment &ie2) const;
};

class pairMassElem
{
 public:
  MElement *ele;
  double rho;
 public:
  pairMassElem(MElement *ele_,const double rho_);
  pairMassElem(const pairMassElem &source);
  ~pairMassElem(){}
  bool operator<(const pairMassElem &pme2) const;
};

class fragment
{
 protected:
  mutable std::set<pairMassElem> _data; // remove mutable ??
  friend class fragmentMPI;
 public:
  fragment();
  fragment(const fragment &source);
  fragment(const interfaceFragment &source);
  fragment(const pairMassElem &source);
  ~fragment(){}
  bool match(const interfaceFragment &frag) const;
  bool match(const pairMassElem &source) const;
  // as to be const for iterative
  void add(const interfaceFragment &source) const;
  void add(const fragment &frag2) const;
  int numberOfElements() const;
  double mass() const;
  // to known if an element is included in the fragment
  bool isElem(const int elenum) const;
  // needed for std::set
  bool operator<(const fragment &frag2) const;
  // for MPI
 #if defined(HAVE_MPI)
  bool definedOnMoreThanOneRank() const;
  // I don't known how to perform an MPI comm with element
  // so transfert only required data (element num and its mass)
  // if more data are needed in future these functions have to be updated
  // element number is communicate as a double (allow to perform only 1 communication)
  int numberOfValueToCommunicateMPI() const;
 #endif // HAVE_MPI
};

class pairMassElemMPI
{
 public:
  int elenum;
  double mass;
 public:
  pairMassElemMPI(const pairMassElem &source);
  pairMassElemMPI(const int num,const double m);
  ~pairMassElemMPI(){}
  bool operator<(const pairMassElemMPI &pme2) const;
};

class fragmentMPI
{
 protected:
  mutable std::set<pairMassElemMPI> _data; // remove mutable ??
 public:
  fragmentMPI(const fragment &frag);
  fragmentMPI(double* &fraglist,const int curpos);
  int fillMPIvector(double* mpivector) const;
  bool match(const fragmentMPI &frag) const;
  bool match(const int num) const;
  void add(const fragmentMPI &frag) const;
  int numberOfElements() const;
  int memorySize() const;
  double mass() const;
  bool isElem(const int elenum)const;
  bool operator<(const fragmentMPI &frag2) const;
};
#endif // _PPFRAGMENT_H_
