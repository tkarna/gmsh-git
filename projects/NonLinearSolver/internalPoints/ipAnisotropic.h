//
// Description: storing class for transverse Isotropic law it is a "virtual implementation" you have to define an ipvariable
//              in your project which derive from this one. (all data in this ipvarible have name beggining by _j2l...)
//              so don't do the same in your project...
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPANISOTROPIC_H_
#define IPANISOTROPIC_H_
#include "ipvariable.h"
#include "STensor3.h"
class IPAnisotropic : public IPVariableMechanics
{
 public:
  double _elasticEnergy;
 public:
  IPAnisotropic();
  IPAnisotropic(const IPAnisotropic &source);
  IPAnisotropic& operator=(const IPVariable &source);
  virtual double defoEnergy() const;
};

#endif // IPANISOTROPIC_H_
