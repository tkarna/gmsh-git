//
// Description: storing class for Linear Thermo Mechanics
//
//
// Author:  <Lina Homsy>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipLinearThermoMechanics.h"
IPLinearThermoMechanics::IPLinearThermoMechanics() : IPVariableMechanics(), _elasticEnergy(0.) {};
IPLinearThermoMechanics::IPLinearThermoMechanics(const IPLinearThermoMechanics &source) : IPVariableMechanics(source), _elasticEnergy(source._elasticEnergy){}
IPLinearThermoMechanics& IPLinearThermoMechanics::operator=(const IPVariable &source)
{
  IPVariableMechanics::operator=(source);
  const IPLinearThermoMechanics* src = static_cast<const IPLinearThermoMechanics*>(&source);
}

double IPLinearThermoMechanics::defoEnergy() const
{
  return _elasticEnergy;
}

