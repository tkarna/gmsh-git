//
// Description: storing class for gurson nucleation
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipGursonNucleation.h"

IPGursonNucleation::IPGursonNucleation(): IPVariable(), fdot(0), dFdot(0)
{
}

IPGursonNucleation::IPGursonNucleation(const IPGursonNucleation &source) : IPVariable(source)
{
  fdot      = source.fdot;
  dFdot     = source.dFdot;
  ddFdot    = source.ddFdot;
}

IPGursonNucleation &IPGursonNucleation::operator=(const IPVariable &source)
{
  IPVariable::operator=(source);
  const IPGursonNucleation* src = dynamic_cast<const IPGursonNucleation*>(&source);
  if(src != NULL)
  {
    fdot      = src->fdot;
    dFdot     = src->dFdot;
    ddFdot    = src->ddFdot;
  }  
  return *this;
}


void IPGursonNucleation::createRestart(FILE *fp)
{
  IPVariable::createRestart(fp);
  fprintf(fp,"%e %e %e",fdot, dFdot, ddFdot);
}

void IPGursonNucleation::setFromRestart(FILE *fp)
{
  IPVariable::setFromRestart(fp);
  double a0,a1,a2;
  fscanf(fp,"%e %e %e",&a0,&a1,&a2);
  fdot      = a0;
  dFdot     = a1;
  ddFdot    = a2;
}

IPLinearGursonNucleationRate::IPLinearGursonNucleationRate(): IPGursonNucleation()
{

}

IPLinearGursonNucleationRate::IPLinearGursonNucleationRate(const IPLinearGursonNucleationRate &source) : IPGursonNucleation(source)
{
}

IPLinearGursonNucleationRate &IPLinearGursonNucleationRate::operator=(const IPVariable &source)
{
  IPGursonNucleation::operator=(source);
  const IPLinearGursonNucleationRate* src = dynamic_cast<const IPLinearGursonNucleationRate*>(&source);
  if(src != NULL)
  {
  }
  return *this;
}

IPGursonNucleation * IPLinearGursonNucleationRate::clone() const
{
  
  return new IPLinearGursonNucleationRate(*this);
} 

void IPLinearGursonNucleationRate::createRestart(FILE *fp)
{
  IPGursonNucleation::createRestart(fp);
}

void IPLinearGursonNucleationRate::setFromRestart(FILE *fp)
{
  IPGursonNucleation::setFromRestart(fp);
}

IPExponentialGursonNucleationRate::IPExponentialGursonNucleationRate(): IPGursonNucleation()
{

}

IPExponentialGursonNucleationRate::IPExponentialGursonNucleationRate(const IPExponentialGursonNucleationRate &source) : IPGursonNucleation(source)
{
}

IPExponentialGursonNucleationRate &IPExponentialGursonNucleationRate::operator=(const IPVariable &source)
{
  IPGursonNucleation::operator=(source);
  const IPExponentialGursonNucleationRate* src = dynamic_cast<const IPExponentialGursonNucleationRate*>(&source);
  if(src != NULL)
  {
  }
  return *this;
}

IPGursonNucleation * IPExponentialGursonNucleationRate::clone() const
{
  
  return new IPExponentialGursonNucleationRate(*this);
} 

void IPExponentialGursonNucleationRate::createRestart(FILE *fp)
{
  IPGursonNucleation::createRestart(fp);
}

void IPExponentialGursonNucleationRate::setFromRestart(FILE *fp)
{
  IPGursonNucleation::setFromRestart(fp);
}

