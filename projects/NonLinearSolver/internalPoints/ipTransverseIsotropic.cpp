//
// Description: storing class for j2 linear elasto-plastic law
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipTransverseIsotropic.h"
IPTransverseIsotropic::IPTransverseIsotropic() : IPVariableMechanics(), _elasticEnergy(0.) {};
IPTransverseIsotropic::IPTransverseIsotropic(const IPTransverseIsotropic &source) : IPVariableMechanics(source), _elasticEnergy(source._elasticEnergy){}
IPTransverseIsotropic& IPTransverseIsotropic::operator=(const IPVariable &source)
{
  IPVariableMechanics::operator=(source);
  const IPTransverseIsotropic* src = static_cast<const IPTransverseIsotropic*>(&source);
}

double IPTransverseIsotropic::defoEnergy() const
{
  return _elasticEnergy;
}

