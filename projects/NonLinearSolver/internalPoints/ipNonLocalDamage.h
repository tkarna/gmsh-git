//
// Description: storing class for non local damage law it is a "virtual implementation" you have to define an ipvariable
//              in your project which derive from this one. (all data in this ipvarible have name beggining by _nld...)
//              so don't do the same in your project...
// Author:  <L. NOELS>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPNONLOCALDAMAGE_H_
#define IPNONLOCALDAMAGE_H_
#include "ipvariable.h"
#include "STensor3.h"
#include "matrix_operations.h"
class IPNonLocalDamage : public IPVariableMechanics
{
 protected:
  int _nldNsdv;
  int _pos_str_mtx;
  int _pos_str_inc;
 public: // All data public to avoid the creation of function to access

// x y z
  fullMatrix<double> _nldMaterialTensor;
  fullVector<double> _nldCouplingStressEffectiveStrain;
  fullVector<double> _nldCouplingEffectiveStrainStress;
  fullVector<double> _nldStrain; // epsilon
  fullVector<double> _nldStress; //cauchy
  STensor3            _nldCharacteristicLengthMatrix;      // by Wu Ling
// bareps
  double _nldEffectivePlasticStrain;
  double _nldCurrentPlasticStrain;
  double _nldDamage;
  double _nldSpBar;
// state variables
  double* _nldStatev;
  double _elasticEne;
  double _plasticEne;

 public:
  IPNonLocalDamage(int _nsdv) : IPVariableMechanics(), _nldNsdv(_nsdv), _nldStrain(6), _nldStress(6),
                            _nldMaterialTensor(6,6), _nldCouplingStressEffectiveStrain(6),
                            _nldCouplingEffectiveStrainStress(6),
                            _nldEffectivePlasticStrain(0.), _nldCurrentPlasticStrain(0.), _nldDamage(0.),_nldSpBar(0.),
                            _pos_str_mtx(-1), _pos_str_inc(-1), _elasticEne(0.), _plasticEne(0.)
    {
       	mallocvector(&_nldStatev,_nldNsdv);
        for(int i = 0; i<_nldNsdv; i++) _nldStatev[i]=0.;
        for(int i=0; i<3; i++)
        {
            for(int j=0;j<3; j++)
            {
               _nldCharacteristicLengthMatrix(i,j) = 0.;
            }
            _nldCharacteristicLengthMatrix(i,i) = 1.;
        }

    }
  IPNonLocalDamage(const IPNonLocalDamage &source) : IPVariableMechanics(source)
    {
        _nldNsdv = source._nldNsdv;
        _pos_str_mtx = source._pos_str_mtx;
        _pos_str_inc = source._pos_str_inc;
        _nldStrain =source._nldStrain;
        _nldStress =source._nldStress;
        _nldEffectivePlasticStrain=source._nldEffectivePlasticStrain;
        _nldMaterialTensor=source._nldMaterialTensor;
        _nldCouplingStressEffectiveStrain = source._nldCouplingStressEffectiveStrain;
	_nldCouplingEffectiveStrainStress = source._nldCouplingEffectiveStrainStress;
        _nldCharacteristicLengthMatrix = source._nldCharacteristicLengthMatrix;                 //by Wu Ling
        _nldCurrentPlasticStrain=source._nldCurrentPlasticStrain;
        _nldDamage=source._nldDamage;
        _nldSpBar = source._nldSpBar;
	_elasticEne=source._elasticEne; 
	_plasticEne=source._plasticEne; 
       	mallocvector(&_nldStatev,_nldNsdv);
        copyvect(source._nldStatev,_nldStatev,_nldNsdv);
    }
  IPNonLocalDamage &operator = (const IPVariable &_source)
  {
      IPVariableMechanics::operator=(_source);
      const IPNonLocalDamage *source=static_cast<const IPNonLocalDamage *> (&_source);
      _pos_str_mtx = source->_pos_str_mtx;
      _pos_str_inc = source->_pos_str_inc;
      _nldStrain = source->_nldStrain;
      _nldStress = source->_nldStress;
      _nldEffectivePlasticStrain=source->_nldEffectivePlasticStrain;
      _nldMaterialTensor=source->_nldMaterialTensor;
      _nldCouplingStressEffectiveStrain = source->_nldCouplingStressEffectiveStrain;
      _nldCouplingEffectiveStrainStress = source->_nldCouplingEffectiveStrainStress;
      _nldCharacteristicLengthMatrix = source->_nldCharacteristicLengthMatrix;                 //by Wu Ling
      _nldCurrentPlasticStrain=source->_nldCurrentPlasticStrain;
      _nldDamage=source->_nldDamage;
      _nldSpBar = source->_nldSpBar;
      _elasticEne=source->_elasticEne; 
      _plasticEne=source->_plasticEne; 
      if(_nldNsdv !=source->_nldNsdv) Msg::Error("IPNonLocalDamage do not have the same number of internal variables");
      copyvect(source->_nldStatev,_nldStatev,_nldNsdv);
      return *this;
  }
  virtual ~IPNonLocalDamage()
  {
      free(_nldStatev);
  }
  // Archiving data
  virtual double get(const int i) const;
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual double getEffectivePlasticStrain() const { return _nldEffectivePlasticStrain;}
  virtual double getCurrentPlasticStrain() const { return _nldCurrentPlasticStrain;}
  virtual double getRefToEffectivePlasticStrain() { return _nldEffectivePlasticStrain;}
  virtual double getRefToCurrentPlasticStrain() { return _nldCurrentPlasticStrain;}
  virtual double getNsdv() const {return _nldNsdv;}
  virtual void setPosStrMtx(int i) {_pos_str_mtx = i;}
  virtual void setPosStrInc(int i) {_pos_str_inc = i;}

};

#endif // IPNONLOCALDAMAGE_H_
