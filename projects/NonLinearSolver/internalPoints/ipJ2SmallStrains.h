//
// C++ Interface: internal variable
//
// Description: ipvariable associated to mlawJ2SmallStrains
//
//
// Author:  <Gauthier BECKER>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPJ2SMALLSTRAINS_H_
#define IPJ2SMALLSTRAINS_H_
#include "ipvariable.h"
#include "STensor43.h"
#include "STensor3.h"
class ipJ2SmallStrains : public IPVariableMechanics
{
 public: // direct access to variable to avoid function definition
  STensor43 _H; // material tensor
//  STensor3 _couplingStressEffectiveStrain, _couplingEffectiveStrainStress, _caracLmatrix; // used ?
  STensor3 _epsilon,_sigma,_sigmaeff, _plasticStrain, _dpdE, _d2muiso;
  double _effectivePlasticStrain, _muiso;
  // double _nldDamage;
//  double _nldSpBar; // usefull ??
 public:
  ipJ2SmallStrains();
  ipJ2SmallStrains(const ipJ2SmallStrains &source);
  ipJ2SmallStrains& operator=(const IPVariable &source);
  ~ipJ2SmallStrains(){}

  /* IPVariableMechanics */
  virtual double get(const int i) const{Msg::Error("ipJ2SmallStrains::get() implement me");}
  virtual double defoEnergy() const{Msg::Error("ipJ2smallStrains::defoEnergy() implement me");}
};
#endif // IPJ2SMALLSTRAINS_H_
