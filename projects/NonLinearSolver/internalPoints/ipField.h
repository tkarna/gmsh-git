//
// C++ Interface: terms
//
// Description: Class to compute Internal point
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
# ifndef _IPFIELD_H_
# define _IPFIELD_H_
#include<vector>
#include"quadratureRules.h"
#include "unknownField.h"
#include "elementsField.h"
#include "GModel.h"
#include "ipstate.h"
#include "partDomain.h"
struct archiveIPVariable;
class IPField : public elementsField {
  public:
    enum  Output { SVM=0, SIG_XX, SIG_YY, SIG_ZZ, SIG_XY, SIG_YZ, SIG_XZ, DAMAGE, PLASTICSTRAIN, GLSTRAINEQUIVALENT,
                    STRAIN_XX, STRAIN_YY, STRAIN_ZZ, STRAIN_XY, STRAIN_YZ, STRAIN_XZ, HIGHSTRAIN_NORM,
                    HIGHSTRESS_NORM, GL_NORM, STRESS_NORM, P_STRESS_NORM , F_XX, F_XY, F_XZ, F_YX, F_YY,F_YZ,
                    F_ZX, F_ZY, F_ZZ, P_XX, P_XY, P_XZ,P_YX, P_YY, P_YZ, P_ZX, P_ZY, P_ZZ,
                    G_XXX, G_XXY, G_XXZ, G_XYX, G_XYY, G_XYZ,G_XZX, G_XZY, G_XZZ,
                    G_YXX, G_YXY, G_YXZ, G_YYX, G_YYY, G_YYZ,G_YZX, G_YZY, G_YZZ,
                    G_ZXX, G_ZXY, G_ZXZ, G_ZYX, G_ZYY, G_ZYZ,G_ZZX, G_ZZY, G_ZZZ,
                    Q_XXX, Q_XXY, Q_XXZ, Q_XYX, Q_XYY, Q_XYZ,Q_XZX, Q_XZY, Q_XZZ,
                    Q_YXX, Q_YXY, Q_YXZ, Q_YYX, Q_YYY, Q_YYZ,Q_YZX, Q_YZY, Q_YZZ,
                    Q_ZXX, Q_ZXY, Q_ZXZ, Q_ZYX, Q_ZYY, Q_ZYZ,Q_ZZX, Q_ZZY, Q_ZZZ,
                    MTX_SVM,MTX_SIG_XX,MTX_SIG_YY,MTX_SIG_ZZ,MTX_SIG_XY,MTX_SIG_YZ,MTX_SIG_XZ,
		    INC_SVM,INC_SIG_XX,INC_SIG_YY,INC_SIG_ZZ,INC_SIG_XY,INC_SIG_YZ,INC_SIG_XZ,TEMPERATURE,
		    THERMALFLUX_X,THERMALFLUX_Y,THERMALFLUX_Z};
    enum  Operator { MEAN_VALUE=1, MIN_VALUE, MAX_VALUE, CRUDE_VALUE};
#ifndef SWIG
    // Struct for archiving
    struct ip2archive{
      std::vector< MElement * > ele; // element where the point is situated (if node is common to several element the first one met is chosen. Normally OK continuity)
      int numgauss; // local gauss num
      int dim;
      int comp;  // component to archive == ipval
      int numphys;
      int nstep;
      std::vector<const partDomain *> domain; // address of domain needed to access IP
      nlsField::ElemValue evalue;
      FILE *fp;

      void openFile()
      {
        if(fp!=NULL) fclose(fp);
        std::string fname;
        std::ostringstream oss;
        oss << numphys;
        std::string s = oss.str();
        oss.str("");
        oss << comp;
        std::string s2 = oss.str();
        switch(evalue){
         case nlsField::mean:
          oss.str("Mean");
          break;
         case nlsField::min:
          oss.str("Min");
          break;
         case nlsField::max:
          oss.str("Max");
          break;
         default:
          oss.str("");
        }
        std::string s3 = oss.str();
        #if defined(HAVE_MPI)
        if(Msg::GetCommSize() != 1){
          oss.str("");
          oss << Msg::GetCommRank();
          s3 += "_part"+oss.str();
        }
        #endif // HAVE_MPI
        if(dim == 0)
          fname = "IP"+s+"val"+s2+s3+".csv";
        else if(dim == 1)
          fname = "IPLine"+s+"val"+s2+s3+".csv";
        else if(dim == 2)
          fname = "IPFace"+s+"val"+s2+s3+".csv";
        else
          fname = "IPVolume"+s+"val"+s2+s3+".csv";
        fp = fopen(fname.c_str(),"w");
      }

      // constructor for solver
      ip2archive(const int dim_,const int nphys_,const int ipval_,const int eval_,
                 const int nstep_) : dim(dim_), numphys(nphys_), comp(ipval_), nstep(nstep_),fp(NULL),// the other has to be defined later
                                     numgauss(0)
      {
        // empty vectors (they will be filled later) clear to be sure
        ele.clear();
        domain.clear();
        switch(eval_){
         case 1:
          evalue=nlsField::mean;
          break;
         case 2:
          evalue=nlsField::min;
          break;
         case 3:
          evalue=nlsField::max;
          break;
         default:
          evalue=nlsField::crude;
        }
      }

      ip2archive(const ip2archive &source) : ele(source.ele), domain(source.domain), fp(source.fp), numgauss(source.numgauss),
                                             numphys(source.numphys), comp(source.comp), evalue(source.evalue), dim(source.dim),
                                             nstep(source.nstep) {}
      ~ip2archive(){}
      void closeFile()
      {
        if(fp!=NULL) fclose(fp);
        fp=NULL;
      }

    };
  protected :
    std::vector<partDomain*>* _efield;
    dofManager<double> *_dm;
    AllIPState *_AIPS;
    unknownField *_ufield;
    std::vector<ip2archive> viparch;
    bool isMultiscale;
//    normalVariation _nvaripfield; // To avoid multiple allocation
 public :
  IPField(std::vector<partDomain*> *ef,dofManager<double>* pa,
          unknownField* uf, std::vector<ip2archive> &vaip,
          const std::vector<dataBuildView> &dbview_,std::string filename = "stress.msh");
  void archive(const double time,const int step=1);

  AllIPState* getAips() const {return _AIPS;}
  virtual ~IPField(){delete _AIPS;}
  void compute1state(IPStateBase::whichState ws, bool stiff);

  // num allows to select a particular gauss' point. Ins value is used only if ev = crude
  double getIPcomp(const partDomain *ef,MElement *ele, const IPStateBase::whichState ws,
                   const int cmp,const nlsField::ElemValue ev,const int num=0) const;
  // vol return the associated volume
  double getIPcompAndVolume(const partDomain *ef,MElement *ele, const IPStateBase::whichState ws,
                   const int cmp,const nlsField::ElemValue ev, double &vol) const;
  // function to archive
  virtual void get(partDomain *dom, MElement *ele, std::vector<double> &val, const int cc,
                   const nlsField::ElemValue ev)const{
    val[0]=  this->getIPcomp(dom,ele,IPStateBase::current,cc,ev);
  }

  // Interaction with Aips
  void copy(IPStateBase::whichState source, IPStateBase::whichState dest){_AIPS->copy(source,dest);}
  void nextStep(const double time=0.){
    _AIPS->nextStep();
  }

  // initial broken
  void initialBroken(GModel* pModel, std::vector<int> &vnumphys);
  void initialBroken(MElement *iele, materialLaw *mlaw);
  // used in case of switch between scheme
  void resetUnknownField(unknownField *uf);
  void setFactorOnArchivingFiles(const int fact);

  // close all files
  void closeFile();
  // open all files (used after switch between scheme otherwise open in constructor)
  void openArchivingFiles();

  template<class T> void getIPv(const MElement *ele,const T** vipv, const IPStateBase::whichState ws= IPStateBase::current) const
  {
    AllIPState::ipstateElementContainer* vips = _AIPS->getIPstate(ele->getNum());
    for(int i=0; i<vips->size(); i++){
      IPStateBase* ips = (*vips)[i];
      vipv[i] = static_cast<const T*>(ips->getState(ws));
    }
  }
  template<class T> void getIPv(const MElement *ele,const T** vipv, const T** vipvprev) const
  {
    AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(ele->getNum());
    for(int i=0; i<vips->size(); i++){
      IPStateBase* ips = (*vips)[i];
      vipv[i] = static_cast<const T*>(ips->getState(IPStateBase::current));
      vipvprev[i] = static_cast<const T*>(ips->getState(IPStateBase::previous));
    }
  }
  template<class T1,class T2> void getIPv(const MInterfaceElement *iele, const T1** vipv_m, const T2** vipv_p,
                                          const IPStateBase::whichState ws=IPStateBase::current) const
  {
    AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(iele->getNum());
    // SAME NUMBER OF GAUSS POINTS ON BOTH SIDES
    int npts = vips->size()/2;
    for(int i=0;i<npts;i++){
      IPStateBase *ipsm = (*vips)[i];
      IPStateBase *ipsp = (*vips)[i+npts];
      vipv_m[i] = dynamic_cast<const T1*>(ipsm->getState(ws));
      vipv_p[i] = dynamic_cast<const T2*>(ipsp->getState(ws));
    }
  }
  template<class T1,class T2> void getIPv(const MInterfaceElement *iele, const T1** vipv_m, const T1** vipvprev_m,
                                             const T2** vipv_p, const T2** vipvprev_p) const
  {
    AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(iele->getNum());
    // SAME NUMBER OF GAUSS POINTS ON BOTH SIDES
    int npts = vips->size()/2;
    for(int i=0;i<npts;i++){
      IPStateBase *ipsm = (*vips)[i];
      IPStateBase *ipsp = (*vips)[i+npts];
      vipv_m[i] = static_cast<const T1*>(ipsm->getState(IPStateBase::current));
      vipv_p[i] = static_cast<const T2*>(ipsp->getState(IPStateBase::current));
      vipvprev_m[i] = static_cast<const T1*>(ipsm->getState(IPStateBase::previous));
      vipvprev_p[i] = static_cast<const T2*>(ipsp->getState(IPStateBase::previous));
    }
  }
  template<class T1,class T2> void getIPv(const MInterfaceElement *iele, T1** vipv_m, T2** vipv_p,
                                             const IPStateBase::whichState ws=IPStateBase::current) const
  {
    AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(iele->getNum());
    // SAME NUMBER OF GAUSS POINTS ON BOTH SIDES
    int npts = vips->size()/2;
    for(int i=0;i<npts;i++){
      IPStateBase *ipsm = (*vips)[i];
      IPStateBase *ipsp = (*vips)[i+npts];
      vipv_m[i] = static_cast<T1*>(ipsm->getState(ws));
      vipv_p[i] = static_cast<T2*>(ipsp->getState(ws));
    }
  }
  template<class T1,class T2> void getIPv(const MInterfaceElement *iele, T1** vipv_m, T2** vipvprev_m,
                                             T2** vipv_p, T2** vipvprev_p) const
  {
    AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(iele->getNum());
    // SAME NUMBER OF GAUSS POINTS ON BOTH SIDES
    int npts = vips->size()/2;
    for(int i=0;i<npts;i++){
      IPStateBase *ipsm = (*vips)[i];
      IPStateBase *ipsp = (*vips)[i+npts];
      vipv_m[i] = static_cast<T1*>(ipsm->getState(IPStateBase::current));
      vipv_p[i] = static_cast<T2*>(ipsp->getState(IPStateBase::current));
      vipvprev_m[i] = static_cast<T1*>(ipsm->getState(IPStateBase::previous));
      vipvprev_p[i] = static_cast<T2*>(ipsp->getState(IPStateBase::previous));
    }
  }


  template<class T1> void getIPv(const MInterfaceElement *iele, const T1** vipv_m,
                                   const IPStateBase::whichState ws=IPStateBase::current) const
  {
    AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(iele->getNum());
    // SAME NUMBER OF GAUSS POINTS ON BOTH SIDES
    int npts = vips->size();
    for(int i=0;i<npts;i++){
      IPStateBase *ipsm = (*vips)[i];
      vipv_m[i] = static_cast<const T1*>(ipsm->getState(ws));
    }
  }
  template<class T1> void getIPv(const MInterfaceElement *iele, const T1** vipv_m, const T1** vipvprev_m) const
  {
    AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(iele->getNum());
    // SAME NUMBER OF GAUSS POINTS ON BOTH SIDES
    int npts = vips->size();
    for(int i=0;i<npts;i++){
      IPStateBase *ipsm = (*vips)[i];
      vipv_m[i] = static_cast<const T1*>(ipsm->getState(IPStateBase::current));
      vipvprev_m[i] = static_cast<const T1*>(ipsm->getState(IPStateBase::previous));
    }
  }

  template<class T1> void getIPv(const MInterfaceElement *iele, T1** vipv_m,
                                   const IPStateBase::whichState ws=IPStateBase::current) const
  {
    AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(iele->getNum());
    int npts = vips->size();
    for(int i=0;i<npts;i++){
      IPStateBase *ipsm = (*vips)[i];
      vipv_m[i] = static_cast<T1*>(ipsm->getState(ws));
    }
  }
  template<class T1> void getIPv(const MInterfaceElement *iele, T1** vipv_m, T1** vipvprev_m) const
  {
    AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(iele->getNum());
    int npts = vips->size();
    for(int i=0;i<npts;i++){
      IPStateBase *ipsm = (*vips)[i];
      vipv_m[i] = static_cast<T1*>(ipsm->getState(IPStateBase::current));
      vipvprev_m[i] = static_cast<T1*>(ipsm->getState(IPStateBase::previous));
    }
  }
  double computeDeformationEnergy(MElement *ele, const partDomain *dom) const;
  double computeDeformationEnergy() const{
    double ener=0.;
    for(std::vector<partDomain*>::const_iterator itdom=_efield->begin(); itdom!=_efield->end(); ++itdom){
      partDomain *dom = *itdom;
      for(groupOfElements::elementContainer::const_iterator it=dom->g->begin(); it!=dom->g->end(); ++it){
        MElement *ele = *it;
        ener += this->computeDeformationEnergy(ele,dom);
      }
    }
    return ener;
  }
  double computePlasticEnergy(MElement *ele, const partDomain *dom) const;
  double computePlasticEnergy() const{
    double ener=0.;
    for(std::vector<partDomain*>::const_iterator itdom=_efield->begin(); itdom!=_efield->end(); ++itdom){
      partDomain *dom = *itdom;
      for(groupOfElements::elementContainer::const_iterator it=dom->g->begin(); it!=dom->g->end(); ++it){
        MElement *ele = *it;
        ener += this->computePlasticEnergy(ele,dom);
      }
    }
    return ener;
  }
  int computeFractureEnergy(MElement *ele,const dgPartDomain *dom,double* arrayEnergy) const;
  int computeFractureEnergy(double * arrayEnergy) const
  {
    static double tmpArray[10];
    int maxcomp = 0;
    for(int i=0;i<10;i++){
      arrayEnergy[i]=0.;
      tmpArray[i]=0.;
    }
    for(std::vector<partDomain*>::const_iterator itdom=_efield->begin(); itdom!=_efield->end(); ++itdom){
      const partDomain *dom = *itdom;
      if(dom->IsInterfaceTerms()) // otherwise no fracture
      {
        const dgPartDomain* dgdom = static_cast<const dgPartDomain*>(dom);
        if((dgdom->getMaterialLawMinus()->getType() == materialLaw::fracture) and (dgdom->getMaterialLawPlus()->getType() == materialLaw::fracture))
        {
          for(groupOfElements::elementContainer::const_iterator it=dgdom->gi->begin(); it!=dgdom->gi->end(); ++it)
          {
            MElement *ele = *it;
           #if defined(HAVE_MPI)
            if(Msg::GetCommSize()>1)
            {
              MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(ele);
              if(iele->getElem(0)->getPartition()==0 or iele->getElem(0)->getPartition() == Msg::GetCommRank()+1) // if - element is ghost not computed to avoid to count twice the contribution
              {
                int ncomp = this->computeFractureEnergy(ele,dgdom,tmpArray);
                for(int i=0;i<ncomp;i++){
                  arrayEnergy[i]+=tmpArray[i];
                  tmpArray[i]=0.;
                }
                if(ncomp>maxcomp)
                  maxcomp = ncomp;
              }
            }
            else
           #endif // HAVE_MPI
            {
              int ncomp = this->computeFractureEnergy(ele,dgdom,tmpArray);
              for(int i=0;i<ncomp;i++){
                arrayEnergy[i]+=tmpArray[i];
                tmpArray[i]=0.;
              }
              if(ncomp>maxcomp)
                maxcomp = ncomp;
            }
          }
        }
      }
    }
    return maxcomp;
  }
  // store all IPVariable values
  virtual void createRestart() const;
  virtual void setFromRestart();
#endif // swig
};
#endif // IPField

