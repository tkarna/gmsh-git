//
// Description: storing class for non local damage
//
//
// Author:  <L. Noels>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipNonLocalDamage.h"
#include "ipField.h"

double IPNonLocalDamage::defoEnergy() const
{
  return _elasticEne;
}
double IPNonLocalDamage::plasticEnergy() const
{
  return _plasticEne;
}

double IPNonLocalDamage::get(int comp) const
{
  if(comp >= IPField::SIG_XX and comp<=IPField::SIG_XY )
	  return _nldStress(comp-1);
  else if(comp==IPField::SIG_XZ) // order xx yy zz xy xz yz
	  return _nldStress(comp-2);
  else if(comp==IPField::SIG_YZ)
	  return _nldStress(comp);
  else if(comp == IPField::SVM) // von Mises
  {

     double svm= (_nldStress(0)-_nldStress(1))*(_nldStress(0)-_nldStress(1))+(_nldStress(2)-_nldStress(1))*(_nldStress(2)-_nldStress(1))+(_nldStress(2)-_nldStress(0))*(_nldStress(2)-_nldStress(0));
     svm += 6.*(_nldStress(3)*_nldStress(3)+_nldStress(4)*_nldStress(4)+_nldStress(5)*_nldStress(5));
     svm /= 2.;
     svm = sqrt(svm);
     return svm;
  }
  else if(comp == IPField::DAMAGE) // damage
     return _nldDamage;
  else if(comp == IPField::PLASTICSTRAIN) 
     return _nldCurrentPlasticStrain;
  else if(comp >= IPField::STRAIN_XX and comp<IPField::STRAIN_XY )
	  return _nldStrain(comp-IPField::STRAIN_XX);
  else if(comp == IPField::STRAIN_XY)
	  return _nldStrain(comp-IPField::STRAIN_XX)/2.;
  else if(comp==IPField::STRAIN_XZ) // order xx yy zz xy xz yz
	  return _nldStrain(comp-IPField::STRAIN_XX-1)/2.;
  else if(comp==IPField::STRAIN_YZ)
	  return _nldStrain(comp-IPField::STRAIN_XX)/2.;
  else if(comp>=  IPField::MTX_SVM and comp<=IPField::INC_SIG_XZ and _pos_str_mtx >=0 and _pos_str_inc >=0)
  {
     const double *str = NULL;
     double sq2 = sqrt(2.);
     if(comp>=  IPField::MTX_SVM and comp<=IPField::MTX_SIG_XZ) str = &(_nldStatev[_pos_str_mtx]);
     else str = &(_nldStatev[_pos_str_inc]);

     fullVector<double> _cauchy(6);
     for(int i=0;i<3;i++)
     {
       _cauchy(i)=str[i];
       _cauchy(i+3) = str[i+3]/sq2;
     }
     if(comp == IPField::MTX_SVM or comp == IPField::INC_SVM)
     {
        double svm= (_cauchy(0)-_cauchy(1))*(_cauchy(0)-_cauchy(1))+(_cauchy(2)-_cauchy(1))*(_cauchy(2)-_cauchy(1))+(_cauchy(2)-_cauchy(0))*(_cauchy(2)-_cauchy(0));
       svm += 6.*(_cauchy(3)*_cauchy(3)+_cauchy(4)*_cauchy(4)+_cauchy(5)*_cauchy(5));
       svm /= 2.;
       svm = sqrt(svm);
       return svm;
     }
     else if(comp == IPField::MTX_SIG_XX or comp == IPField::INC_SIG_XX)
       return _cauchy(0);
     else if(comp == IPField::MTX_SIG_YY or comp == IPField::INC_SIG_YY)
       return _cauchy(1);
     else if(comp == IPField::MTX_SIG_ZZ or comp == IPField::INC_SIG_ZZ)
       return _cauchy(2);
     else if(comp == IPField::MTX_SIG_XY or comp == IPField::INC_SIG_XY)
       return _cauchy(3);
     else if(comp == IPField::MTX_SIG_YZ or comp == IPField::INC_SIG_YZ)
       return _cauchy(5);
     else if(comp == IPField::MTX_SIG_XZ or comp == IPField::INC_SIG_XZ)
       return _cauchy(4);
     else
       return 0.;
  }
  else
     return 0.; 
     
}

