//
// C++ Interface: terms
//
// Description: Class to compute Internal point
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipField.h"
#include "mlaw.h"
#include "ipstate.h"
#include "nonLinearMechSolver.h"
#include "nlsFunctionSpace.h"
#include "energyField.h"
void IPField::compute1state(IPStateBase::whichState ws, bool stiff){
  for(std::vector<partDomain*>::iterator itdom=_efield->begin(); itdom!=_efield->end(); ++itdom){
    partDomain *dom = *itdom;
    dom->computeIPVariable(_AIPS,_ufield,ws,stiff);
  }
}

void IPField::initialBroken(MElement *iele, materialLaw* mlaw ){
 #ifdef _DEBUG
  Msg::Error("Interface element %d is broken at initialization on rank %d",iele->getNum(),Msg::GetCommRank());
 #else
  Msg::Info("Interface element %d is broken at initialization",iele->getNum());
 #endif // _DEBUG
  materialLaw2LawsInitializer * mflaw = dynamic_cast<materialLaw2LawsInitializer*>(mlaw);
  // get ipstate
  AllIPState::ipstateElementContainer *vips;
  IPStateBase *ips;
  vips = _AIPS->getIPstate(iele->getNum());
  for(int i=0;i<vips->size();i++){
    ips = (*vips)[i];
    mflaw->initialBroken(ips);
  }
}

void IPField::initialBroken(GModel* pModel, std::vector<int> &vnumphys){
  std::vector<MVertex*> vv;
  for(int i=0;i<vnumphys.size();i++){
    // get the vertex associated to the physical entities LINES ONLY !!
    pModel->getMeshVerticesForPhysicalGroup(1,vnumphys[i],vv);
    // find the InterfaceElement associated to these vertex (identify interior node as degree 2 min)
    for(std::vector<partDomain*>::iterator itfield = _efield->begin(); itfield != _efield->end(); ++itfield){
      if((*itfield)->IsInterfaceTerms())
      {
        dgPartDomain *dgdom = static_cast<dgPartDomain*>(*itfield);
        for(groupOfElements::elementContainer::const_iterator it = dgdom->gi->begin(); it!=dgdom->gi->end(); ++it){
          for(int k=0;k<vv.size();k++){
            if(vv[k] == (*it)->getVertex(2) )
            {
              this->initialBroken(*it, dgdom->getMaterialLaw());
            }
          }
        }
      }
    }
    vv.clear();
  }
}

void IPField::archive(const double time, const int step){
  // msh view
  this->buildAllView(*_efield,time,step);
  if(!_forceView)
  {
    // txt archive
    for(std::vector<ip2archive>::const_iterator it=viparch.begin(); it!=viparch.end(); ++it){
      const ip2archive &ipa = *it;
      if(step%ipa.nstep == 0)
      {
        // get ip value
        if( ipa.ele.size() == 1)
        {
          double val = this->getIPcomp(ipa.domain[0],ipa.ele[0],IPStateBase::current,ipa.comp,
                                        ipa.evalue,ipa.numgauss);
          fprintf(ipa.fp,"%e;%e\n",time,val);
	  if(step%(ipa.nstep*10) == 0)
            fflush (ipa.fp);
        }
        else
        {
          double totalVolume = 0.;
          double totalValue = 0;
          if(ipa.evalue == nlsField::min) totalValue = 1.e50;
          if(ipa.evalue == nlsField::max) totalValue = -1.e50;
          std::vector<const partDomain *>::const_iterator itd = ipa.domain.begin();
          for(std::vector<MElement *>::const_iterator ite = ipa.ele.begin();
              ite !=  ipa.ele.end(); ite++, itd++)
          {
            double vol = 0.;
            double val = this->getIPcompAndVolume(*itd,*ite, IPStateBase::current, ipa.comp,
                                      ipa.evalue,vol);
            totalVolume += vol;
            if(ipa.evalue == nlsField::crude)
            {
              Msg::Error("Cannot archive on a non nodal endity crude values");
            }
            else
            {
              switch(ipa.evalue){
                case nlsField::max :
                 if(val>totalValue) totalValue=val;
                 break;
                case nlsField::min :
                 if(val<totalValue) totalValue=val;
                 break;
                case nlsField::mean :
                 totalValue+=val*vol;
                 break;
              }
            }
          }
          if(ipa.evalue==nlsField::mean && totalVolume!=0.) totalValue/=totalVolume;
//        FILE *fp = fopen(ipa.fname.c_str(),"a");
          fprintf(ipa.fp,"%e;%e\n",time,totalValue);
          if(step%(ipa.nstep*10) == 0)
            fflush (ipa.fp);
//        fclose(fp);
        }
      }
    }
  }
  else
  {
    _forceView = false; // the command forceView has to be used at each step you want to force archiving
  }
}

double IPField::computeDeformationEnergy(MElement *ele, const partDomain *dom) const
{
  IntPt *GP;
  double jac[3][3];
  int npts = dom->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
  AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(ele->getNum());
  double ener =0.;
  // Jacobian values;
  std::vector<double> vdetJ(npts);
  nlsFunctionSpaceUVW<double>* nlsspace = dynamic_cast<nlsFunctionSpaceUVW<double>*>(dom->getFunctionSpace());
  if(nlsspace!=NULL)
  {
    std::vector<std::vector<TensorialTraits<double>::GradType> > vgradsuvw;
    nlsspace->getgradfuvw(ele,npts,GP,vgradsuvw);
    for(int i=0;i<npts;i++)
    {
      vdetJ[i] = ele->getJacobian(vgradsuvw[i],jac);
    }
  }
  else
  {
    for(int i=0;i<npts;i++)
    {
      const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
      vdetJ[i] = ele->getJacobian(u, v, w, jac);
    }
  }

  for(int i=0;i<npts;i++){
    IPStateBase* ips = (*vips)[i];
   #ifdef DEBUG_
    IPVariableMechanics *ipv = dynamic_cast<IPVariableMechanics*>(ips->getState(IPStateBase::current));
    if(ipv == NULL){
      Msg::Error("Compute defo energy on an non mechanics gauss' point");
      return 0.;
    }
   #else
    IPVariableMechanics *ipv = static_cast<IPVariableMechanics*>(ips->getState(IPStateBase::current));
   #endif // DEBUG_
    double enerpt = ipv->defoEnergy();
    // gauss point weight
    double weight = GP[i].weight;
    ener += weight*vdetJ[i]*enerpt;
  }
  return ener;
}

double IPField::computePlasticEnergy(MElement *ele, const partDomain *dom) const
{
  IntPt *GP;
  double jac[3][3];
  int npts = dom->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
  AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(ele->getNum());
  double ener =0.;
  // Jacobian values;
  std::vector<double> vdetJ(npts);
  nlsFunctionSpaceUVW<double>* nlsspace = dynamic_cast<nlsFunctionSpaceUVW<double>*>(dom->getFunctionSpace());
  if(nlsspace!=NULL)
  {
    std::vector<std::vector<TensorialTraits<double>::GradType> > vgradsuvw;
    nlsspace->getgradfuvw(ele,npts,GP,vgradsuvw);
    for(int i=0;i<npts;i++)
    {
      vdetJ[i] = ele->getJacobian(vgradsuvw[i],jac);
    }
  }
  else
  {
    for(int i=0;i<npts;i++)
    {
      const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
      vdetJ[i] = ele->getJacobian(u, v, w, jac);
    }
  }
  for(int i=0;i<npts;i++){
    IPStateBase* ips = (*vips)[i];
   #ifdef DEBUG_
    IPVariableMechanics *ipv = dynamic_cast<IPVariableMechanics*>(ips->getState(IPStateBase::current));
    if(ipv == NULL){
      Msg::Error("Compute plastic energy on an non mechanics gauss' point");
      return 0.;
    }
   #else
    IPVariableMechanics *ipv = static_cast<IPVariableMechanics*>(ips->getState(IPStateBase::current));
   #endif // DEBUG_
    double enerpt = ipv->plasticEnergy();
    // gauss point weight
    double weight = GP[i].weight;
    ener += weight*vdetJ[i]*enerpt;
  }
  return ener;
}

int IPField::computeFractureEnergy(MElement *ele, const dgPartDomain *dom,double* arrayEnergy) const
{
  IntPt *GP;
  static double jac[3][3];
  int npts = dom->getInterfaceGaussIntegrationRule()->getIntPoints(ele,&GP);
  AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(ele->getNum());
  // Jacobian values;
  std::vector<double> vdetJ(npts);
  nlsFunctionSpaceUVW<double>* nlsspace = dynamic_cast<nlsFunctionSpaceUVW<double>*>(dom->getFunctionSpace());
  if(nlsspace!=NULL)
  {
    std::vector<std::vector<TensorialTraits<double>::GradType> > vgradsuvw;
    nlsspace->getgradfuvw(ele,npts,GP,vgradsuvw);
    for(int i=0;i<npts;i++)
    {
      vdetJ[i] = ele->getJacobian(vgradsuvw[i],jac);
    }
  }
  else
  {
    for(int i=0;i<npts;i++)
    {
      const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
      vdetJ[i] = ele->getJacobian(u, v, w, jac);
    }
  }
  int ncomp=0;
  static double tmpArray[10];
  for(int i=0;i<npts;i++){ // loop on minus interface only to compute energy (otherwise it would be computed twice)
    IPStateBase* ips = (*vips)[i];
    IPVariableMechanics *ipv = static_cast<IPVariableMechanics*>(ips->getState(IPStateBase::current)); // The IPField:computeFractureEnergy(double* &) ensures that this is OK
    // gauss point weight
    double weight = GP[i].weight;
    const double wJ = weight*vdetJ[i];
    if(ipv->isbroken()){
      ncomp = ipv->fractureEnergy(tmpArray);
      for(int j=0;j<ncomp;j++)
        arrayEnergy[j]+= wJ*tmpArray[j];
    }
  }
  return ncomp;
}


IPField::IPField(std::vector<partDomain*> *ef,dofManager<double>* pa,
          unknownField* uf, std::vector<ip2archive> &vaip,
          const std::vector<dataBuildView> &dbview_, std::string filename): _efield(ef), _dm(pa),_ufield(uf),
                                              elementsField(filename,100000000,1,dbview_){
  // Creation of storage for IP data
  _AIPS = new AllIPState(*_efield);
  // compute the number of element
  long int nelem=0;
  isMultiscale = false;
  for(std::vector<partDomain*>::iterator itdom=_efield->begin(); itdom!=_efield->end(); ++itdom){
    partDomain *dom = *itdom;
    nelem+= dom->g->size();
    if (dom->getMaterialLaw() != NULL)
      if (dom->getMaterialLaw()->getType() == materialLaw::numeric)
        isMultiscale = true;
  }
  this->setTotElem(nelem);
  this->buildAllView(*_efield,0.,0);

  // Build the viparch vector (find the nearest ipvariable to each given vertex)
  for(std::vector<ip2archive>::iterator ita=vaip.begin(); ita!=vaip.end(); ++ita){
    ip2archive &aip = *ita;
    int dim = aip.dim;
    // get the vertex of the node
    groupOfElements g(dim,aip.numphys);
    bool flagfind = false;
    int nummin = 0;
    if(dim == 0)
    {
      groupOfElements::vertexContainer::iterator itv = g.vbegin();
      MVertex *ver = *itv;
      dgPartDomain *dgdom;
      bool samedim = false;
      for(std::vector<partDomain*>::iterator itdom=_efield->begin(); itdom!=_efield->end(); ++itdom)
      {
        partDomain *dom = *itdom;
        if(dom->IsInterfaceTerms())
        { // otherwise the domain doesn't contain interface element
          dgdom = static_cast<dgPartDomain*>(dom);
          for(groupOfElements::elementContainer::iterator it2 = dgdom->gi->begin(); it2 != dgdom->gi->end(); ++it2 )
          {
            MElement *iele = *it2; // two elements on interface
            int numv = iele->getNumVertices();
            for(int j=0;j<numv;j++)
            {
              if(ver == iele->getVertex(j))
              {
                aip.ele.push_back(iele);
                aip.domain.push_back(dom);
                flagfind = true;
                break;
              }
            }
            if(flagfind) break;
          }
          if(!flagfind)
          { // the interface element are on boundary
            for(groupOfElements::elementContainer::iterator iti = dgdom->gib->begin(); iti!=dgdom->gib->end(); ++iti){
              MElement *ie = *iti;
              int numv = ie->getNumVertices();
              for(int j=0;j<numv;j++){
                if(ver == ie->getVertex(j)){
                  aip.ele.push_back(ie);
                  aip.domain.push_back(dom);
                  flagfind = true;
                  break;
                }
              }
              if(flagfind) break;
            }
          }
        }
        if(!flagfind){ // no interface element found (Can happend on boundary)
          for(groupOfElements::elementContainer::iterator it2 = dom->g->begin(); it2 != dom->g->end(); ++it2 ){
            MElement *ele = *it2;
            int numv = ele->getNumVertices();
            for(int j=0;j<numv;j++){
              if(ver == ele->getVertex(j)){
                aip.ele.push_back(ele);
                samedim = true;
                flagfind = true;
                aip.domain.push_back(dom);
                break;
              }
            }
            if(flagfind) break;
          }
        }
        if(flagfind) break;
      }
      if(flagfind){ // If solve on more than one cpu it if possible that the point is not found
        // Now the element where is situated the node is know find the nearest Gauss Point
        IntPt *GP;
        QuadratureBase *gq;
        if(samedim){ //bulk point
          gq = aip.domain[0]->getBulkGaussIntegrationRule();
        }
        else{
          const dgPartDomain *dgfind = static_cast<const dgPartDomain*>(aip.domain[0]);
          gq = dgfind->getInterfaceGaussIntegrationRule();
        }
        int npts = gq->getIntPoints(aip.ele[0],&GP);
        // coordonate in uvw of vertex
        double uvw[3];
        double xyz[3];
        xyz[0] = ver->x(); xyz[1] = ver->y(); xyz[2] = ver->z();
        aip.ele[0]->xyz2uvw(xyz,uvw);
        double distmin = 1.e100; // inf value at itnitialization
        double dist, u,v,w;
        for(int i=0;i<npts;i++){
          u = GP[i].pt[0]; v = GP[i].pt[1]; w = GP[i].pt[2];
          dist = sqrt((u-uvw[0])*(u-uvw[0]) + (v-uvw[1])*(v-uvw[1]) + (w-uvw[2])*(w-uvw[2]));
          if(dist<distmin){
            distmin = dist;
            nummin = i;
          }
        }
      }
    }
    else
    {
      for(groupOfElements::elementContainer::iterator ite = g.begin(); ite != g.end(); ite++)
      {
        dgPartDomain *dgdom;
        bool samedim = false;
        for(std::vector<partDomain*>::iterator itdom=_efield->begin(); itdom!=_efield->end(); ++itdom){
          partDomain *dom = *itdom;
          if(dom->IsInterfaceTerms())
          { // otherwise the domain doesn't contain interface element
            dgdom = static_cast<dgPartDomain*>(dom);
            for(groupOfElements::elementContainer::iterator it2 = dgdom->gi->begin(); it2 != dgdom->gi->end(); ++it2 )
            {
              if(*it2 == *ite)
              {
                aip.ele.push_back(*ite);
                aip.domain.push_back(dom);
                flagfind = true;
              }
            }
            for(groupOfElements::elementContainer::iterator iti = dgdom->gib->begin(); iti!=dgdom->gib->end(); ++iti)
            {
              if(*iti == *ite)
              {
                aip.ele.push_back(*ite);
                aip.domain.push_back(dom);
                flagfind = true;
              }
            }
          }
          for(groupOfElements::elementContainer::iterator it2 = dom->g->begin(); it2 != dom->g->end(); ++it2 )
          {
            if(*it2 == *ite)
            {
              aip.ele.push_back(*ite);
              aip.domain.push_back(dom);
              flagfind = true;
            }
          }
        }
      }
    }
    if(flagfind){ // If solve on more than one cpu it if possible that the point is not found
      // Now the element where is situated the node is know find the nearest Gauss Point
      // Store information
      aip.numgauss = nummin;
      viparch.push_back(ip2archive(aip));
      viparch.back().openFile();
    }
    else
    {
      #if defined(HAVE_MPI)
      if(Msg::GetCommSize() == 1)
      #endif // HAVE_MPI
      {
        Msg::Error("Unable to find an IPVariable where data is supposed to be archived");
      }
    }
  }
  vaip.clear();
}

double IPField::getIPcomp(const partDomain *ef,MElement *ele, const IPStateBase::whichState ws,
                            const int cmp,const nlsField::ElemValue ev,const int num) const{
  if(ev == nlsField::crude){ // value in a particular gauss point
    IPStateBase* ips = (*_AIPS->getIPstate(ele->getNum()))[num];
    IPVariable *ipv = ips->getState(ws);
    return ipv->get(cmp);
  }
  else{ // loop on all IPVariable of an element and return a particular value (max, min, mean)
    double valcmp =0.;
    double valcmpp;
    IntPt *GP;
    int npts = ef->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
    AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(ele->getNum());
    for(int i=0;i<npts;i++){
      IPStateBase* ips = (*vips)[i];
      IPVariable *ipv = ips->getState(ws);

      valcmpp = ipv->get(cmp);
      if(i==0)
        valcmp = valcmpp;
      else{
        switch(ev){
          case nlsField::max :
            if(valcmpp>valcmp) valcmp=valcmpp;
            break;
          case nlsField::min :
            if(valcmpp<valcmp) valcmp=valcmpp;
            break;
          case nlsField::mean :
            valcmp+=valcmpp;
            break;
        }
      }
    }
    if(ev==nlsField::mean) valcmp/=(double)npts;
    return valcmp;
  }
}
double IPField::getIPcompAndVolume(const partDomain *ef,MElement *ele, const IPStateBase::whichState ws,
                           const int cmp, const nlsField::ElemValue ev, double &vol) const
{
  if(ev == nlsField::crude)
  {
     Msg::Error("Cannot archive on a non nodal endity crude values");
  }
  else
  {
    vol = 0.;
    double valcmp =0.;
    double jac[3][3];
    double valcmpp;
    IntPt *GP;
    int npts = ef->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
    AllIPState::ipstateElementContainer *vips = _AIPS->getIPstate(ele->getNum());
    // Jacobian values;
    std::vector<double> vdetJ(npts);
    nlsFunctionSpaceUVW<double>* nlsspace = dynamic_cast<nlsFunctionSpaceUVW<double>*>(ef->getFunctionSpace());
    if(nlsspace!=NULL)
    {
      std::vector<std::vector<TensorialTraits<double>::GradType> > vgradsuvw;
      nlsspace->getgradfuvw(ele,npts,GP,vgradsuvw);
      for(int i=0;i<npts;i++)
      {
        vdetJ[i] = ele->getJacobian(vgradsuvw[i],jac);
      }
    }
    else
    {
      for(int i=0;i<npts;i++)
      {
        const double u = GP[i].pt[0]; const double v = GP[i].pt[1]; const double w = GP[i].pt[2];
        vdetJ[i] = ele->getJacobian(u, v, w, jac);
      }
    }
    for(int i=0;i<npts;i++){
      IPStateBase* ips = (*vips)[i];
      IPVariable *ipv = ips->getState(ws);
      double weight = GP[i].weight;
      const double detJ = vdetJ[i];

      valcmpp = ipv->get(cmp);
      vol += detJ*weight;
      if(i==0)
      {
        switch(ev){
          case nlsField::max :
            valcmp = valcmpp;
            break;
          case nlsField::min :
           valcmp = valcmpp;
            break;
          case nlsField::mean :
            valcmp =valcmpp*detJ*weight;
            break;
        }
      }
      else{
        switch(ev){
          case nlsField::max :
            if(valcmpp>valcmp) valcmp=valcmpp;
            break;
          case nlsField::min :
            if(valcmpp<valcmp) valcmp=valcmpp;
            break;
          case nlsField::mean :
            valcmp+=valcmpp*detJ*weight;
            break;
        }
      }
    }
    if(ev==nlsField::mean) valcmp/=vol;
    return valcmp;
  }
}

void IPField::resetUnknownField(unknownField *uf)
{
  _ufield = uf;
  // reset archiving file if needed
  if(_vBuildView->size() > 0)
  {
//    FILE *fp = fopen(this->fileName.c_str(),"w"); WHY ??
    this->createFile();
  }
}

void IPField::setFactorOnArchivingFiles(const int fact)
{
  for(int i=0;i<viparch.size();i++)
  {
    viparch[i].nstep*=fact;
  }
}

void IPField::closeFile()
{
  for(int i=0;i<viparch.size();i++)
    viparch[i].closeFile();
  nlsField::closeFile();

  /*Close all file in multiscale analysis*/

  if (isMultiscale){
    AllIPState::ipstateContainer* container = _AIPS->getAIPSContainer();
    for (AllIPState::ipstateContainer::iterator it = container->begin(); it != container->end(); it++){
      std::vector<IPStateBase*> *vips = &((*it).second);
        for (int i = 0; i< vips->size(); i++){
          IP3StateMultiscale* multiscaleState =dynamic_cast<IP3StateMultiscale*>((*vips)[i]);
          if (multiscaleState){
            nonLinearMechSolver* solver = multiscaleState->getSolver();
            if (solver!= NULL)
              solver->closeAllFile();
          }
        }
    }
  };
}
void IPField::openArchivingFiles()
{
  for(int i=0;i<viparch.size();i++)
    viparch[i].openFile();
}

void IPField::createRestart() const
{
  // file name
  std::string fname;
 #if defined(HAVE_MPI)
  if(Msg::GetCommSize()>1)
  {
    std::ostringstream oss;
    oss << Msg::GetCommRank();
    std::string s = oss.str();
    fname = "restartIPFieldPart"+s;
  }
  else
 #endif //HAVE_MPI
  {
    fname = "restartIPField";
  }
  FILE *fp = fopen(fname.c_str(),"w"); // only one restart (last restart is removed)

  // loop on all ipstates
  _AIPS->createRestart(fp);

  fclose(fp);
}

void IPField::setFromRestart()
{
  // file name
  std::string fname;
 #if defined(HAVE_MPI)
  if(Msg::GetCommSize()>1)
  {
    std::ostringstream oss;
    oss << Msg::GetCommRank();
    std::string s = oss.str();
    fname = "restartIPFieldPart"+s;
  }
  else
 #endif //HAVE_MPI
  {
    fname = "restartIPField";
  }
  FILE *fp = fopen(fname.c_str(),"r"); // only one restart (last restart is removed)

  // loop on all ipstates
  _AIPS->setFromRestart(fp);

  fclose(fp);
}
