//
// C++ Interface: Quadrature Rule for interface element
//
// Description: Gives the value on minus and plus element from the interface value
//
//
// Author:  <Gauthier BECKER>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "interfaceQuadrature.h"
#include "GaussLobattoQuadrature.h"
/*
int interfaceQuadrature::getIntPoints(MInterfaceElement *iele, IntPt *GPs, IntPt **GPm, IntPt **GPp)
{
  for(int i=0;i<_npts;i++)
  {
    iele->getuvwOnElem(GPs[i].pt[0],GPs[i].pt[1],_GPm[i].pt[0],_GPm[i].pt[1],_GPm[i].pt[2],_GPp[i].pt[0],_GPp[i].pt[1],_GPp[i].pt[2]);
    _GPm[i].weight = GPs[i].weight;
    _GPp[i].weight = GPs[i].weight;
  }
  *GPm = _GPm;
  *GPp = _GPp;
  return _npts;
}
*/
interface1DQuadrature::interface1DQuadrature(const int order, const bool lobatto) : interfaceQuadratureBase(), _npts((order+1)/2) , _GPq(NULL), _GPt(NULL),
                                                                                    _GPqI(NULL), _GPtI(NULL)
{
  // allocation
  _GPq = new IntPt[_npts*4]; // 4 edges
  _GPt = new IntPt[_npts*3]; // 3 edges
  _GPqI= new IntPt[_npts*4];
  _GPtI= new IntPt[_npts*3];
  // Get the intPt on a MLine
  IntPt *GPs;
  lobatto ? GPs = getGaussLobattoQuadrature1DPts(order) : GPs= getGQLPts(order);
  // Value on elements plus and minus (small duplication if MInterfaceLine::getuvwOnElement() as no MInterfaceLine here!)
  for(int i=0;i<_npts;i++)
  {
    double u = GPs[i].pt[0] ; double weight = GPs[i].weight;
    // triangular
    _GPt[i].pt[0] = 0.5*(1.+u) ; _GPt[i].pt[1] = 0. ; _GPt[i].pt[2] = 0.; _GPt[i].weight = weight;
    _GPtI[i].pt[0]= 0.5*(1.-u) ; _GPtI[i].pt[1]= 0. ; _GPtI[i].pt[2]= 0.; _GPtI[i].weight= weight;
    _GPt[i+_npts].pt[0] = 0.5*(1.-u)  ; _GPt[i+_npts].pt[1] = 0.5*(1+u) ; _GPt[i+_npts].pt[2] = 0.; _GPt[i+_npts].weight = weight;
    _GPtI[i+_npts].pt[0]= 0.5*(1.+u)  ; _GPtI[i+_npts].pt[1]= 0.5*(1-u) ; _GPtI[i+_npts].pt[2]= 0.; _GPtI[i+_npts].weight= weight;
    _GPt[i+2*_npts].pt[0] = 0.        ; _GPt[i+2*_npts].pt[1] = 0.5*(1.-u); _GPt[i+2*_npts].pt[2]=0.; _GPt[i+2*_npts].weight=weight;
    _GPtI[i+2*_npts].pt[0]= 0.        ; _GPtI[i+2*_npts].pt[1]= 0.5*(1.+u); _GPtI[i+2*_npts].pt[2]=0.;_GPtI[i+2*_npts].weight=weight;
    // quadrangular
    _GPq[i].pt[0] = u  ; _GPq[i].pt[1] = -1.  ;             _GPq[i].pt[2] = 0. ;  _GPq[i].weight = weight;
    _GPqI[i].pt[0]= -u ; _GPqI[i].pt[1]= -1.  ;             _GPqI[i].pt[2]= 0. ;  _GPqI[i].weight= weight;
    _GPq[i+_npts].pt[0] = 1.; _GPq[i+_npts].pt[1] = u     ; _GPq[i+_npts].pt[2]=0.; _GPq[i+_npts].weight=weight;
    _GPqI[i+_npts].pt[0]= 1.; _GPqI[i+_npts].pt[1]= -u    ; _GPqI[i+_npts].pt[2]=0.;_GPqI[i+_npts].weight=weight;
    _GPq[i+2*_npts].pt[0] = -u; _GPq[i+2*_npts].pt[1] = 1. ; _GPq[i+2*_npts].pt[2] = 0.; _GPq[i+2*_npts].weight=weight;
    _GPqI[i+2*_npts].pt[0]= u;  _GPqI[i+2*_npts].pt[1]= 1. ; _GPqI[i+2*_npts].pt[2] =0.; _GPqI[i+2*_npts].weight=weight;
    _GPq[i+3*_npts].pt[0] = -1 ;_GPq[i+3*_npts].pt[1] = -u; _GPq[i+3*_npts].pt[2] = 0.; _GPq[i+3*_npts].weight=weight;
    _GPqI[i+3*_npts].pt[0]= -1 ;_GPqI[i+3*_npts].pt[1]= u ; _GPqI[i+3*_npts].pt[2]= 0.; _GPqI[i+3*_npts].weight=weight;
  }
}

int interface1DQuadrature::getIntPoints(MInterfaceElement *iele, IntPt *GPs, IntPt **GPm, IntPt **GPp)
{
  // minus element
  if(iele->getElem(0)->getNumPrimaryVertices()==3) // triangular
    iele->isSameDirection(0) ? *GPm = &_GPt[iele->getEdgeOrFaceNumber(0)*_npts] : *GPm = &_GPtI[iele->getEdgeOrFaceNumber(0)*_npts];
  else // quadrangular
    iele->isSameDirection(0) ? *GPm = &_GPq[iele->getEdgeOrFaceNumber(0)*_npts] : *GPm = &_GPqI[iele->getEdgeOrFaceNumber(0)*_npts];
  // plus element
  if(iele->getElem(1)->getNumPrimaryVertices()==3) // triangular
    iele->isSameDirection(1) ? *GPp = &_GPt[iele->getEdgeOrFaceNumber(1)*_npts] : *GPp = &_GPtI[iele->getEdgeOrFaceNumber(1)*_npts];
  else // quadrangular
    iele->isSameDirection(1) ? *GPp = &_GPq[iele->getEdgeOrFaceNumber(1)*_npts] : *GPp = &_GPqI[iele->getEdgeOrFaceNumber(1)*_npts];
  return _npts;
}

interface1DQuadrature::~interface1DQuadrature()
{
  if(_GPq!=NULL) delete[] _GPq; _GPq=NULL;
  if(_GPt!=NULL) delete[] _GPt; _GPt=NULL;
  if(_GPqI!=NULL) delete[] _GPqI; _GPqI=NULL;
  if(_GPtI!=NULL) delete[] _GPtI; _GPtI=NULL;
}

interface2DQuadrature::interface2DQuadrature(const int order) : interfaceQuadratureBase(), _GPt(NULL), _GPtI(NULL), _GPh(NULL), _GPhI(NULL),
                                                                _nptst(getNGQTPts(order)),_nptsh(getNGQQPts(order))
{
  // allocation
  _GPt = new IntPt[_nptst*4*3]; // 4 faces on a Tetra 3 permutations
  _GPh = new IntPt[_nptsh*6*4]; // 6 faces on an hexa 4 permutations
  _GPtI= new IntPt[_nptst*4*3]; //inverse orientation
  _GPhI= new IntPt[_nptsh*6*4];
  // fill after (Check on weight if weight == 0. the array has to be filled)
  for(int i=0;i<4*_nptst*3;i++)
  {
    _GPt[i].weight = 0.;
    _GPtI[i].weight= 0.;
  }
  for(int i=0;i<6*_nptsh*4;i++)
  {
    _GPh[i].weight = 0.;
    _GPhI[i].weight= 0.;
  }

  //fill u v w for quadratic tetrahedron [face][per][gauss][uvw]
  // not sure about the permutation here below
  uvwTetQuad[0][0][0][0]=4.459485e-01; uvwTetQuad[0][0][0][1]=4.459485e-01; uvwTetQuad[0][0][0][2]=0.;
  uvwTetQuad[0][0][1][0]=1.081030e-01; uvwTetQuad[0][0][1][1]=4.459485e-01; uvwTetQuad[0][0][1][2]=0.;
  uvwTetQuad[0][0][2][0]=4.459485e-01; uvwTetQuad[0][0][2][1]=1.081030e-01; uvwTetQuad[0][0][2][2]=0.;
  uvwTetQuad[0][0][3][0]=9.157621e-02; uvwTetQuad[0][0][3][1]=9.157621e-02; uvwTetQuad[0][0][3][2]=0.;
  uvwTetQuad[0][0][4][0]=8.168476e-01; uvwTetQuad[0][0][4][1]=9.157621e-02; uvwTetQuad[0][0][4][2]=0.;
  uvwTetQuad[0][0][5][0]=9.157621e-02; uvwTetQuad[0][0][5][1]=8.168476e-01; uvwTetQuad[0][0][5][2]=0.;
  //
  uvwTetQuad[0][1][2][0]=4.459485e-01; uvwTetQuad[0][1][2][1]=4.459485e-01; uvwTetQuad[0][1][2][2]=0.;
  uvwTetQuad[0][1][0][0]=1.081030e-01; uvwTetQuad[0][1][0][1]=4.459485e-01; uvwTetQuad[0][1][0][2]=0.;
  uvwTetQuad[0][1][1][0]=4.459485e-01; uvwTetQuad[0][1][1][1]=1.081030e-01; uvwTetQuad[0][1][1][2]=0.;
  uvwTetQuad[0][1][5][0]=9.157621e-02; uvwTetQuad[0][1][5][1]=9.157621e-02; uvwTetQuad[0][1][5][2]=0.;
  uvwTetQuad[0][1][3][0]=8.168476e-01; uvwTetQuad[0][1][3][1]=9.157621e-02; uvwTetQuad[0][1][3][2]=0.;
  uvwTetQuad[0][1][4][0]=9.157621e-02; uvwTetQuad[0][1][4][1]=8.168476e-01; uvwTetQuad[0][1][4][2]=0.;
  //
  uvwTetQuad[0][2][1][0]=4.459485e-01; uvwTetQuad[0][2][1][1]=4.459485e-01; uvwTetQuad[0][2][1][2]=0.;
  uvwTetQuad[0][2][2][0]=1.081030e-01; uvwTetQuad[0][2][2][1]=4.459485e-01; uvwTetQuad[0][2][2][2]=0.;
  uvwTetQuad[0][2][0][0]=4.459485e-01; uvwTetQuad[0][2][0][1]=1.081030e-01; uvwTetQuad[0][2][0][2]=0.;
  uvwTetQuad[0][2][4][0]=9.157621e-02; uvwTetQuad[0][2][4][1]=9.157621e-02; uvwTetQuad[0][2][4][2]=0.;
  uvwTetQuad[0][2][5][0]=8.168476e-01; uvwTetQuad[0][2][5][1]=9.157621e-02; uvwTetQuad[0][2][5][2]=0.;
  uvwTetQuad[0][2][3][0]=9.157621e-02; uvwTetQuad[0][2][3][1]=8.168476e-01; uvwTetQuad[0][2][3][2]=0.;


  uvwTetQuad[1][0][0][0]=4.459485e-01; uvwTetQuad[1][0][0][1]=0.; uvwTetQuad[1][0][0][2]=4.459485e-01;
  uvwTetQuad[1][0][1][0]=4.459485e-01; uvwTetQuad[1][0][1][1]=0.; uvwTetQuad[1][0][1][2]=1.081030e-01;
  uvwTetQuad[1][0][2][0]=1.081030e-01; uvwTetQuad[1][0][2][1]=0.; uvwTetQuad[1][0][2][2]=4.459485e-01;
  uvwTetQuad[1][0][3][0]=9.157621e-02; uvwTetQuad[1][0][3][1]=0.; uvwTetQuad[1][0][3][2]=9.157621e-02;
  uvwTetQuad[1][0][4][0]=9.157621e-02; uvwTetQuad[1][0][4][1]=0.; uvwTetQuad[1][0][4][2]=8.168476e-01;
  uvwTetQuad[1][0][5][0]=8.168476e-01; uvwTetQuad[1][0][5][1]=0.; uvwTetQuad[1][0][5][2]=9.157621e-02;
  //
  uvwTetQuad[1][1][2][0]=4.459485e-01; uvwTetQuad[1][1][2][1]=0.; uvwTetQuad[1][1][2][2]=4.459485e-01;
  uvwTetQuad[1][1][0][0]=4.459485e-01; uvwTetQuad[1][1][0][1]=0.; uvwTetQuad[1][1][0][2]=1.081030e-01;
  uvwTetQuad[1][1][1][0]=1.081030e-01; uvwTetQuad[1][1][1][1]=0.; uvwTetQuad[1][1][1][2]=4.459485e-01;
  uvwTetQuad[1][1][5][0]=9.157621e-02; uvwTetQuad[1][1][5][1]=0.; uvwTetQuad[1][1][5][2]=9.157621e-02;
  uvwTetQuad[1][1][3][0]=9.157621e-02; uvwTetQuad[1][1][3][1]=0.; uvwTetQuad[1][1][3][2]=8.168476e-01;
  uvwTetQuad[1][1][4][0]=8.168476e-01; uvwTetQuad[1][1][4][1]=0.; uvwTetQuad[1][1][4][2]=9.157621e-02;
  //
  uvwTetQuad[1][2][1][0]=4.459485e-01; uvwTetQuad[1][2][1][1]=0.; uvwTetQuad[1][2][1][2]=4.459485e-01;
  uvwTetQuad[1][2][2][0]=4.459485e-01; uvwTetQuad[1][2][2][1]=0.; uvwTetQuad[1][2][2][2]=1.081030e-01;
  uvwTetQuad[1][2][0][0]=1.081030e-01; uvwTetQuad[1][2][0][1]=0.; uvwTetQuad[1][2][0][2]=4.459485e-01;
  uvwTetQuad[1][2][4][0]=9.157621e-02; uvwTetQuad[1][2][4][1]=0.; uvwTetQuad[1][2][4][2]=9.157621e-02;
  uvwTetQuad[1][2][5][0]=9.157621e-02; uvwTetQuad[1][2][5][1]=0.; uvwTetQuad[1][2][5][2]=8.168476e-01;
  uvwTetQuad[1][2][3][0]=8.168476e-01; uvwTetQuad[1][2][3][1]=0.; uvwTetQuad[1][2][3][2]=9.157621e-02;


  uvwTetQuad[2][0][0][0]=0.; uvwTetQuad[2][0][0][1]=4.459485e-01; uvwTetQuad[2][0][0][2]=4.459485e-01;
  uvwTetQuad[2][0][1][0]=0.; uvwTetQuad[2][0][1][1]=1.081030e-01; uvwTetQuad[2][0][1][2]=4.459485e-01;
  uvwTetQuad[2][0][2][0]=0.; uvwTetQuad[2][0][2][1]=4.459485e-01; uvwTetQuad[2][0][2][2]=1.081030e-01;
  uvwTetQuad[2][0][3][0]=0.; uvwTetQuad[2][0][3][1]=9.157621e-02; uvwTetQuad[2][0][3][2]=9.157621e-02;
  uvwTetQuad[2][0][4][0]=0.; uvwTetQuad[2][0][4][1]=8.168476e-01; uvwTetQuad[2][0][4][2]=9.157621e-02;
  uvwTetQuad[2][0][5][0]=0.; uvwTetQuad[2][0][5][1]=9.157621e-02; uvwTetQuad[2][0][5][2]=8.168476e-01;
  //
  uvwTetQuad[2][1][2][0]=0.; uvwTetQuad[2][1][2][1]=4.459485e-01; uvwTetQuad[2][1][2][2]=4.459485e-01;
  uvwTetQuad[2][1][0][0]=0.; uvwTetQuad[2][1][0][1]=1.081030e-01; uvwTetQuad[2][1][0][2]=4.459485e-01;
  uvwTetQuad[2][1][1][0]=0.; uvwTetQuad[2][1][1][1]=4.459485e-01; uvwTetQuad[2][1][1][2]=1.081030e-01;
  uvwTetQuad[2][1][5][0]=0.; uvwTetQuad[2][1][5][1]=9.157621e-02; uvwTetQuad[2][1][5][2]=9.157621e-02;
  uvwTetQuad[2][1][3][0]=0.; uvwTetQuad[2][1][3][1]=8.168476e-01; uvwTetQuad[2][1][3][2]=9.157621e-02;
  uvwTetQuad[2][1][4][0]=0.; uvwTetQuad[2][1][4][1]=9.157621e-02; uvwTetQuad[2][1][4][2]=8.168476e-01;
  //
  uvwTetQuad[2][2][1][0]=0.; uvwTetQuad[2][2][1][1]=4.459485e-01; uvwTetQuad[2][2][1][2]=4.459485e-01;
  uvwTetQuad[2][2][2][0]=0.; uvwTetQuad[2][2][2][1]=1.081030e-01; uvwTetQuad[2][2][2][2]=4.459485e-01;
  uvwTetQuad[2][2][0][0]=0.; uvwTetQuad[2][2][0][1]=4.459485e-01; uvwTetQuad[2][2][0][2]=1.081030e-01;
  uvwTetQuad[2][2][4][0]=0.; uvwTetQuad[2][2][4][1]=9.157621e-02; uvwTetQuad[2][2][4][2]=9.157621e-02;
  uvwTetQuad[2][2][5][0]=0.; uvwTetQuad[2][2][5][1]=8.168476e-01; uvwTetQuad[2][2][5][2]=9.157621e-02;
  uvwTetQuad[2][2][3][0]=0.; uvwTetQuad[2][2][3][1]=9.157621e-02; uvwTetQuad[2][2][3][2]=8.168476e-01;


  uvwTetQuad[3][0][0][0]=4.459485e-01; uvwTetQuad[3][0][0][1]=4.459485e-01; uvwTetQuad[3][0][0][2]=1.081030e-01;
  uvwTetQuad[3][0][1][0]=4.459485e-01; uvwTetQuad[3][0][1][1]=1.081030e-01; uvwTetQuad[3][0][1][2]=4.459485e-01;
  uvwTetQuad[3][0][2][0]=1.081030e-01; uvwTetQuad[3][0][2][1]=4.459485e-01; uvwTetQuad[3][0][2][2]=4.459485e-01;
  uvwTetQuad[3][0][3][0]=9.157621e-02; uvwTetQuad[3][0][3][1]=9.157621e-02; uvwTetQuad[3][0][3][2]=8.168476e-01;
  uvwTetQuad[3][0][4][0]=9.157621e-02; uvwTetQuad[3][0][4][1]=8.168476e-01; uvwTetQuad[3][0][4][2]=9.157621e-02;
  uvwTetQuad[3][0][5][0]=8.168476e-01; uvwTetQuad[3][0][5][1]=9.157621e-02; uvwTetQuad[3][0][5][2]=9.157621e-02;
  //
  uvwTetQuad[3][1][2][0]=4.459485e-01; uvwTetQuad[3][1][2][1]=4.459485e-01; uvwTetQuad[3][1][2][2]=1.081030e-01;
  uvwTetQuad[3][1][0][0]=4.459485e-01; uvwTetQuad[3][1][0][1]=1.081030e-01; uvwTetQuad[3][1][0][2]=4.459485e-01;
  uvwTetQuad[3][1][1][0]=1.081030e-01; uvwTetQuad[3][1][1][1]=4.459485e-01; uvwTetQuad[3][1][1][2]=4.459485e-01;
  uvwTetQuad[3][1][5][0]=9.157621e-02; uvwTetQuad[3][1][5][1]=9.157621e-02; uvwTetQuad[3][1][5][2]=8.168476e-01;
  uvwTetQuad[3][1][3][0]=9.157621e-02; uvwTetQuad[3][1][3][1]=8.168476e-01; uvwTetQuad[3][1][3][2]=9.157621e-02;
  uvwTetQuad[3][1][4][0]=8.168476e-01; uvwTetQuad[3][1][4][1]=9.157621e-02; uvwTetQuad[3][1][4][2]=9.157621e-02;
  //
  uvwTetQuad[3][2][1][0]=4.459485e-01; uvwTetQuad[3][2][1][1]=4.459485e-01; uvwTetQuad[3][2][1][2]=1.081030e-01;
  uvwTetQuad[3][2][2][0]=4.459485e-01; uvwTetQuad[3][2][2][1]=1.081030e-01; uvwTetQuad[3][2][2][2]=4.459485e-01;
  uvwTetQuad[3][2][0][0]=1.081030e-01; uvwTetQuad[3][2][0][1]=4.459485e-01; uvwTetQuad[3][2][0][2]=4.459485e-01;
  uvwTetQuad[3][2][4][0]=9.157621e-02; uvwTetQuad[3][2][4][1]=9.157621e-02; uvwTetQuad[3][2][4][2]=8.168476e-01;
  uvwTetQuad[3][2][5][0]=9.157621e-02; uvwTetQuad[3][2][5][1]=8.168476e-01; uvwTetQuad[3][2][5][2]=9.157621e-02;
  uvwTetQuad[3][2][3][0]=8.168476e-01; uvwTetQuad[3][2][3][1]=9.157621e-02; uvwTetQuad[3][2][3][2]=9.157621e-02;

  uvwTetQuadI[0][0][0][0]=4.459485e-01; uvwTetQuadI[0][0][0][1]=4.459485e-01; uvwTetQuadI[0][0][0][2]=0.;
  uvwTetQuadI[0][0][1][0]=4.459485e-01; uvwTetQuadI[0][0][1][1]=1.081030e-01; uvwTetQuadI[0][0][1][2]=0.;
  uvwTetQuadI[0][0][2][0]=1.081030e-01; uvwTetQuadI[0][0][2][1]=4.459485e-01; uvwTetQuadI[0][0][2][2]=0.;
  uvwTetQuadI[0][0][3][0]=9.157621e-02; uvwTetQuadI[0][0][3][1]=9.157621e-02; uvwTetQuadI[0][0][3][2]=0.;
  uvwTetQuadI[0][0][4][0]=9.157621e-02; uvwTetQuadI[0][0][4][1]=8.168476e-01; uvwTetQuadI[0][0][4][2]=0.;
  uvwTetQuadI[0][0][5][0]=8.168476e-01; uvwTetQuadI[0][0][5][1]=9.157621e-02; uvwTetQuadI[0][0][5][2]=0.;
  //
  uvwTetQuadI[0][1][2][0]=4.459485e-01; uvwTetQuadI[0][1][2][1]=4.459485e-01; uvwTetQuadI[0][1][2][2]=0.;
  uvwTetQuadI[0][1][0][0]=4.459485e-01; uvwTetQuadI[0][1][0][1]=1.081030e-01; uvwTetQuadI[0][1][0][2]=0.;
  uvwTetQuadI[0][1][1][0]=1.081030e-01; uvwTetQuadI[0][1][1][1]=4.459485e-01; uvwTetQuadI[0][1][1][2]=0.;
  uvwTetQuadI[0][1][5][0]=9.157621e-02; uvwTetQuadI[0][1][5][1]=9.157621e-02; uvwTetQuadI[0][1][5][2]=0.;
  uvwTetQuadI[0][1][3][0]=9.157621e-02; uvwTetQuadI[0][1][3][1]=8.168476e-01; uvwTetQuadI[0][1][3][2]=0.;
  uvwTetQuadI[0][1][4][0]=8.168476e-01; uvwTetQuadI[0][1][4][1]=9.157621e-02; uvwTetQuadI[0][1][4][2]=0.;
  //
  uvwTetQuadI[0][2][1][0]=4.459485e-01; uvwTetQuadI[0][2][1][1]=4.459485e-01; uvwTetQuadI[0][2][1][2]=0.;
  uvwTetQuadI[0][2][2][0]=4.459485e-01; uvwTetQuadI[0][2][2][1]=1.081030e-01; uvwTetQuadI[0][2][2][2]=0.;
  uvwTetQuadI[0][2][0][0]=1.081030e-01; uvwTetQuadI[0][2][0][1]=4.459485e-01; uvwTetQuadI[0][2][0][2]=0.;
  uvwTetQuadI[0][2][4][0]=9.157621e-02; uvwTetQuadI[0][2][4][1]=9.157621e-02; uvwTetQuadI[0][2][4][2]=0.;
  uvwTetQuadI[0][2][5][0]=9.157621e-02; uvwTetQuadI[0][2][5][1]=8.168476e-01; uvwTetQuadI[0][2][5][2]=0.;
  uvwTetQuadI[0][2][3][0]=8.168476e-01; uvwTetQuadI[0][2][3][1]=9.157621e-02; uvwTetQuadI[0][2][3][2]=0.;


  uvwTetQuadI[1][0][0][0]=4.459485e-01; uvwTetQuadI[1][0][0][1]=0.; uvwTetQuadI[1][0][0][2]=4.459485e-01;
  uvwTetQuadI[1][0][1][0]=1.081030e-01; uvwTetQuadI[1][0][1][1]=0.; uvwTetQuadI[1][0][1][2]=4.459485e-01;
  uvwTetQuadI[1][0][2][0]=4.459485e-01; uvwTetQuadI[1][0][2][1]=0.; uvwTetQuadI[1][0][2][2]=1.081030e-01;
  uvwTetQuadI[1][0][3][0]=9.157621e-02; uvwTetQuadI[1][0][3][1]=0.; uvwTetQuadI[1][0][3][2]=9.157621e-02;
  uvwTetQuadI[1][0][4][0]=8.168476e-01; uvwTetQuadI[1][0][4][1]=0.; uvwTetQuadI[1][0][4][2]=9.157621e-02;
  uvwTetQuadI[1][0][5][0]=9.157621e-02; uvwTetQuadI[1][0][5][1]=0.; uvwTetQuadI[1][0][5][2]=8.168476e-01;
  //
  uvwTetQuadI[1][1][2][0]=4.459485e-01; uvwTetQuadI[1][1][2][1]=0.; uvwTetQuadI[1][1][2][2]=4.459485e-01;
  uvwTetQuadI[1][1][0][0]=1.081030e-01; uvwTetQuadI[1][1][0][1]=0.; uvwTetQuadI[1][1][0][2]=4.459485e-01;
  uvwTetQuadI[1][1][1][0]=4.459485e-01; uvwTetQuadI[1][1][1][1]=0.; uvwTetQuadI[1][1][1][2]=1.081030e-01;
  uvwTetQuadI[1][1][5][0]=9.157621e-02; uvwTetQuadI[1][1][5][1]=0.; uvwTetQuadI[1][1][5][2]=9.157621e-02;
  uvwTetQuadI[1][1][3][0]=8.168476e-01; uvwTetQuadI[1][1][3][1]=0.; uvwTetQuadI[1][1][3][2]=9.157621e-02;
  uvwTetQuadI[1][1][4][0]=9.157621e-02; uvwTetQuadI[1][1][4][1]=0.; uvwTetQuadI[1][1][4][2]=8.168476e-01;
  //
  uvwTetQuadI[1][2][1][0]=4.459485e-01; uvwTetQuadI[1][2][1][1]=0.; uvwTetQuadI[1][2][1][2]=4.459485e-01;
  uvwTetQuadI[1][2][2][0]=1.081030e-01; uvwTetQuadI[1][2][2][1]=0.; uvwTetQuadI[1][2][2][2]=4.459485e-01;
  uvwTetQuadI[1][2][0][0]=4.459485e-01; uvwTetQuadI[1][2][0][1]=0.; uvwTetQuadI[1][2][0][2]=1.081030e-01;
  uvwTetQuadI[1][2][4][0]=9.157621e-02; uvwTetQuadI[1][2][4][1]=0.; uvwTetQuadI[1][2][4][2]=9.157621e-02;
  uvwTetQuadI[1][2][5][0]=8.168476e-01; uvwTetQuadI[1][2][5][1]=0.; uvwTetQuadI[1][2][5][2]=9.157621e-02;
  uvwTetQuadI[1][2][3][0]=9.157621e-02; uvwTetQuadI[1][2][3][1]=0.; uvwTetQuadI[1][2][3][2]=8.168476e-01;


  uvwTetQuadI[2][0][0][0]=0.; uvwTetQuadI[2][0][0][1]=4.459485e-01; uvwTetQuadI[2][0][0][2]=4.459485e-01;
  uvwTetQuadI[2][0][1][0]=0.; uvwTetQuadI[2][0][1][1]=4.459485e-01; uvwTetQuadI[2][0][1][2]=1.081030e-01;
  uvwTetQuadI[2][0][2][0]=0.; uvwTetQuadI[2][0][2][1]=1.081030e-01; uvwTetQuadI[2][0][2][2]=4.459485e-01;
  uvwTetQuadI[2][0][3][0]=0.; uvwTetQuadI[2][0][3][1]=9.157621e-02; uvwTetQuadI[2][0][3][2]=9.157621e-02;
  uvwTetQuadI[2][0][4][0]=0.; uvwTetQuadI[2][0][4][1]=9.157621e-02; uvwTetQuadI[2][0][4][2]=8.168476e-01;
  uvwTetQuadI[2][0][5][0]=0.; uvwTetQuadI[2][0][5][1]=8.168476e-01; uvwTetQuadI[2][0][5][2]=9.157621e-02;
  //
  uvwTetQuadI[2][1][2][0]=0.; uvwTetQuadI[2][1][2][1]=4.459485e-01; uvwTetQuadI[2][1][2][2]=4.459485e-01;
  uvwTetQuadI[2][1][0][0]=0.; uvwTetQuadI[2][1][0][1]=4.459485e-01; uvwTetQuadI[2][1][0][2]=1.081030e-01;
  uvwTetQuadI[2][1][1][0]=0.; uvwTetQuadI[2][1][1][1]=1.081030e-01; uvwTetQuadI[2][1][1][2]=4.459485e-01;
  uvwTetQuadI[2][1][5][0]=0.; uvwTetQuadI[2][1][5][1]=9.157621e-02; uvwTetQuadI[2][1][5][2]=9.157621e-02;
  uvwTetQuadI[2][1][3][0]=0.; uvwTetQuadI[2][1][3][1]=9.157621e-02; uvwTetQuadI[2][1][3][2]=8.168476e-01;
  uvwTetQuadI[2][1][4][0]=0.; uvwTetQuadI[2][1][4][1]=8.168476e-01; uvwTetQuadI[2][1][4][2]=9.157621e-02;
  //
  uvwTetQuadI[2][2][1][0]=0.; uvwTetQuadI[2][2][1][1]=4.459485e-01; uvwTetQuadI[2][2][1][2]=4.459485e-01;
  uvwTetQuadI[2][2][2][0]=0.; uvwTetQuadI[2][2][2][1]=4.459485e-01; uvwTetQuadI[2][2][2][2]=1.081030e-01;
  uvwTetQuadI[2][2][0][0]=0.; uvwTetQuadI[2][2][0][1]=1.081030e-01; uvwTetQuadI[2][2][0][2]=4.459485e-01;
  uvwTetQuadI[2][2][4][0]=0.; uvwTetQuadI[2][2][4][1]=9.157621e-02; uvwTetQuadI[2][2][4][2]=9.157621e-02;
  uvwTetQuadI[2][2][5][0]=0.; uvwTetQuadI[2][2][5][1]=9.157621e-02; uvwTetQuadI[2][2][5][2]=8.168476e-01;
  uvwTetQuadI[2][2][3][0]=0.; uvwTetQuadI[2][2][3][1]=8.168476e-01; uvwTetQuadI[2][2][3][2]=9.157621e-02;


  uvwTetQuadI[3][0][0][0]=4.459485e-01; uvwTetQuadI[3][0][0][1]=4.459485e-01; uvwTetQuadI[3][0][0][2]=1.081030e-01;
  uvwTetQuadI[3][0][1][0]=1.081030e-01; uvwTetQuadI[3][0][1][1]=4.459485e-01; uvwTetQuadI[3][0][1][2]=4.459485e-01;
  uvwTetQuadI[3][0][2][0]=4.459485e-01; uvwTetQuadI[3][0][2][1]=1.081030e-01; uvwTetQuadI[3][0][2][2]=4.459485e-01;
  uvwTetQuadI[3][0][3][0]=9.157621e-02; uvwTetQuadI[3][0][3][1]=9.157621e-02; uvwTetQuadI[3][0][3][2]=8.168476e-01;
  uvwTetQuadI[3][0][4][0]=8.168476e-01; uvwTetQuadI[3][0][4][1]=9.157621e-02; uvwTetQuadI[3][0][4][2]=9.157621e-02;
  uvwTetQuadI[3][0][5][0]=9.157621e-02; uvwTetQuadI[3][0][5][1]=8.168476e-01; uvwTetQuadI[3][0][5][2]=9.157621e-02;
  //
  uvwTetQuadI[3][1][2][0]=4.459485e-01; uvwTetQuadI[3][1][2][1]=4.459485e-01; uvwTetQuadI[3][1][2][2]=1.081030e-01;
  uvwTetQuadI[3][1][0][0]=1.081030e-01; uvwTetQuadI[3][1][0][1]=4.459485e-01; uvwTetQuadI[3][1][0][2]=4.459485e-01;
  uvwTetQuadI[3][1][1][0]=4.459485e-01; uvwTetQuadI[3][1][1][1]=1.081030e-01; uvwTetQuadI[3][1][1][2]=4.459485e-01;
  uvwTetQuadI[3][1][5][0]=9.157621e-02; uvwTetQuadI[3][1][5][1]=9.157621e-02; uvwTetQuadI[3][1][5][2]=8.168476e-01;
  uvwTetQuadI[3][1][3][0]=8.168476e-01; uvwTetQuadI[3][1][3][1]=9.157621e-02; uvwTetQuadI[3][1][3][2]=9.157621e-02;
  uvwTetQuadI[3][1][4][0]=9.157621e-02; uvwTetQuadI[3][1][4][1]=8.168476e-01; uvwTetQuadI[3][1][4][2]=9.157621e-02;
  //
  uvwTetQuadI[3][2][1][0]=4.459485e-01; uvwTetQuadI[3][2][1][1]=4.459485e-01; uvwTetQuadI[3][2][1][2]=1.081030e-01;
  uvwTetQuadI[3][2][2][0]=1.081030e-01; uvwTetQuadI[3][2][2][1]=4.459485e-01; uvwTetQuadI[3][2][2][2]=4.459485e-01;
  uvwTetQuadI[3][2][0][0]=4.459485e-01; uvwTetQuadI[3][2][0][1]=1.081030e-01; uvwTetQuadI[3][2][0][2]=4.459485e-01;
  uvwTetQuadI[3][2][4][0]=9.157621e-02; uvwTetQuadI[3][2][4][1]=9.157621e-02; uvwTetQuadI[3][2][4][2]=8.168476e-01;
  uvwTetQuadI[3][2][5][0]=8.168476e-01; uvwTetQuadI[3][2][5][1]=9.157621e-02; uvwTetQuadI[3][2][5][2]=9.157621e-02;
  uvwTetQuadI[3][2][3][0]=9.157621e-02; uvwTetQuadI[3][2][3][1]=8.168476e-01; uvwTetQuadI[3][2][3][2]=9.157621e-02;




// LINEAR HEXAHEDRAL

  //uvwHexLin : not sure of the permutation here below
  uvwHexLin[0][0][0][0]= 5.773503e-01; uvwHexLin[0][0][0][1]= 5.773503e-01; uvwHexLin[0][0][0][2]=-1.000000e+00;
  uvwHexLin[0][0][1][0]= 5.773503e-01; uvwHexLin[0][0][1][1]=-5.773503e-01; uvwHexLin[0][0][1][2]=-1.000000e+00;
  uvwHexLin[0][0][2][0]=-5.773503e-01; uvwHexLin[0][0][2][1]= 5.773503e-01; uvwHexLin[0][0][2][2]=-1.000000e+00;
  uvwHexLin[0][0][3][0]=-5.773503e-01; uvwHexLin[0][0][3][1]=-5.773503e-01; uvwHexLin[0][0][3][2]=-1.000000e+00;
  //
  uvwHexLin[0][1][1][0]= 5.773503e-01; uvwHexLin[0][1][1][1]= 5.773503e-01; uvwHexLin[0][1][1][2]=-1.000000e+00;
  uvwHexLin[0][1][3][0]= 5.773503e-01; uvwHexLin[0][1][3][1]=-5.773503e-01; uvwHexLin[0][1][3][2]=-1.000000e+00;
  uvwHexLin[0][1][0][0]=-5.773503e-01; uvwHexLin[0][1][0][1]= 5.773503e-01; uvwHexLin[0][1][0][2]=-1.000000e+00;
  uvwHexLin[0][1][2][0]=-5.773503e-01; uvwHexLin[0][1][2][1]=-5.773503e-01; uvwHexLin[0][1][2][2]=-1.000000e+00;
  //
  uvwHexLin[0][2][2][0]= 5.773503e-01; uvwHexLin[0][2][2][1]= 5.773503e-01; uvwHexLin[0][2][2][2]=-1.000000e+00;
  uvwHexLin[0][2][0][0]= 5.773503e-01; uvwHexLin[0][2][0][1]=-5.773503e-01; uvwHexLin[0][2][0][2]=-1.000000e+00;
  uvwHexLin[0][2][3][0]=-5.773503e-01; uvwHexLin[0][2][3][1]= 5.773503e-01; uvwHexLin[0][2][3][2]=-1.000000e+00;
  uvwHexLin[0][2][1][0]=-5.773503e-01; uvwHexLin[0][2][1][1]=-5.773503e-01; uvwHexLin[0][2][1][2]=-1.000000e+00;
  //
  uvwHexLin[0][3][3][0]= 5.773503e-01; uvwHexLin[0][3][3][1]= 5.773503e-01; uvwHexLin[0][3][3][2]=-1.000000e+00;
  uvwHexLin[0][3][2][0]= 5.773503e-01; uvwHexLin[0][3][2][1]=-5.773503e-01; uvwHexLin[0][3][2][2]=-1.000000e+00;
  uvwHexLin[0][3][1][0]=-5.773503e-01; uvwHexLin[0][3][1][1]= 5.773503e-01; uvwHexLin[0][3][1][2]=-1.000000e+00;
  uvwHexLin[0][3][0][0]=-5.773503e-01; uvwHexLin[0][3][0][1]=-5.773503e-01; uvwHexLin[0][3][0][2]=-1.000000e+00;


  uvwHexLin[1][0][0][0]= 5.773503e-01; uvwHexLin[1][0][0][1]=-1.000000e+00; uvwHexLin[1][0][0][2]= 5.773503e-01;
  uvwHexLin[1][0][1][0]=-5.773503e-01; uvwHexLin[1][0][1][1]=-1.000000e+00; uvwHexLin[1][0][1][2]= 5.773503e-01;
  uvwHexLin[1][0][2][0]= 5.773503e-01; uvwHexLin[1][0][2][1]=-1.000000e+00; uvwHexLin[1][0][2][2]=-5.773503e-01;
  uvwHexLin[1][0][3][0]=-5.773503e-01; uvwHexLin[1][0][3][1]=-1.000000e+00; uvwHexLin[1][0][3][2]=-5.773503e-01;
  //
  uvwHexLin[1][1][1][0]= 5.773503e-01; uvwHexLin[1][1][1][1]=-1.000000e+00; uvwHexLin[1][1][1][2]= 5.773503e-01;
  uvwHexLin[1][1][3][0]=-5.773503e-01; uvwHexLin[1][1][3][1]=-1.000000e+00; uvwHexLin[1][1][3][2]= 5.773503e-01;
  uvwHexLin[1][1][0][0]= 5.773503e-01; uvwHexLin[1][1][0][1]=-1.000000e+00; uvwHexLin[1][1][0][2]=-5.773503e-01;
  uvwHexLin[1][1][2][0]=-5.773503e-01; uvwHexLin[1][1][2][1]=-1.000000e+00; uvwHexLin[1][1][2][2]=-5.773503e-01;
  //
  uvwHexLin[1][2][2][0]= 5.773503e-01; uvwHexLin[1][2][2][1]=-1.000000e+00; uvwHexLin[1][2][2][2]= 5.773503e-01;
  uvwHexLin[1][2][0][0]=-5.773503e-01; uvwHexLin[1][2][0][1]=-1.000000e+00; uvwHexLin[1][2][0][2]= 5.773503e-01;
  uvwHexLin[1][2][3][0]= 5.773503e-01; uvwHexLin[1][2][3][1]=-1.000000e+00; uvwHexLin[1][2][3][2]=-5.773503e-01;
  uvwHexLin[1][2][1][0]=-5.773503e-01; uvwHexLin[1][2][1][1]=-1.000000e+00; uvwHexLin[1][2][1][2]=-5.773503e-01;
  //
  uvwHexLin[1][3][3][0]= 5.773503e-01; uvwHexLin[1][3][3][1]=-1.000000e+00; uvwHexLin[1][3][3][2]= 5.773503e-01;
  uvwHexLin[1][3][2][0]=-5.773503e-01; uvwHexLin[1][3][2][1]=-1.000000e+00; uvwHexLin[1][3][2][2]= 5.773503e-01;
  uvwHexLin[1][3][1][0]= 5.773503e-01; uvwHexLin[1][3][1][1]=-1.000000e+00; uvwHexLin[1][3][1][2]=-5.773503e-01;
  uvwHexLin[1][3][0][0]=-5.773503e-01; uvwHexLin[1][3][0][1]=-1.000000e+00; uvwHexLin[1][3][0][2]=-5.773503e-01;


  uvwHexLin[2][0][0][0]=-1.000000e+00; uvwHexLin[2][0][0][1]= 5.773503e-01; uvwHexLin[2][0][0][2]= 5.773503e-01;
  uvwHexLin[2][0][1][0]=-1.000000e+00; uvwHexLin[2][0][1][1]= 5.773503e-01; uvwHexLin[2][0][1][2]=-5.773503e-01;
  uvwHexLin[2][0][2][0]=-1.000000e+00; uvwHexLin[2][0][2][1]=-5.773503e-01; uvwHexLin[2][0][2][2]= 5.773503e-01;
  uvwHexLin[2][0][3][0]=-1.000000e+00; uvwHexLin[2][0][3][1]=-5.773503e-01; uvwHexLin[2][0][3][2]=-5.773503e-01;
  //
  uvwHexLin[2][1][1][0]=-1.000000e+00; uvwHexLin[2][1][1][1]= 5.773503e-01; uvwHexLin[2][1][1][2]= 5.773503e-01;
  uvwHexLin[2][1][3][0]=-1.000000e+00; uvwHexLin[2][1][3][1]= 5.773503e-01; uvwHexLin[2][1][3][2]=-5.773503e-01;
  uvwHexLin[2][1][0][0]=-1.000000e+00; uvwHexLin[2][1][0][1]=-5.773503e-01; uvwHexLin[2][1][0][2]= 5.773503e-01;
  uvwHexLin[2][1][2][0]=-1.000000e+00; uvwHexLin[2][1][2][1]=-5.773503e-01; uvwHexLin[2][1][2][2]=-5.773503e-01;
  //
  uvwHexLin[2][2][2][0]=-1.000000e+00; uvwHexLin[2][2][2][1]= 5.773503e-01; uvwHexLin[2][2][2][2]= 5.773503e-01;
  uvwHexLin[2][2][0][0]=-1.000000e+00; uvwHexLin[2][2][0][1]= 5.773503e-01; uvwHexLin[2][2][0][2]=-5.773503e-01;
  uvwHexLin[2][2][3][0]=-1.000000e+00; uvwHexLin[2][2][3][1]=-5.773503e-01; uvwHexLin[2][2][3][2]= 5.773503e-01;
  uvwHexLin[2][2][1][0]=-1.000000e+00; uvwHexLin[2][2][1][1]=-5.773503e-01; uvwHexLin[2][2][1][2]=-5.773503e-01;
  //
  uvwHexLin[2][3][3][0]=-1.000000e+00; uvwHexLin[2][3][3][1]= 5.773503e-01; uvwHexLin[2][3][3][2]= 5.773503e-01;
  uvwHexLin[2][3][2][0]=-1.000000e+00; uvwHexLin[2][3][2][1]= 5.773503e-01; uvwHexLin[2][3][2][2]=-5.773503e-01;
  uvwHexLin[2][3][1][0]=-1.000000e+00; uvwHexLin[2][3][1][1]=-5.773503e-01; uvwHexLin[2][3][1][2]= 5.773503e-01;
  uvwHexLin[2][3][0][0]=-1.000000e+00; uvwHexLin[2][3][0][1]=-5.773503e-01; uvwHexLin[2][3][0][2]=-5.773503e-01;


  uvwHexLin[3][0][0][0]=1.000000e+00; uvwHexLin[3][0][0][1]= 5.773503e-01; uvwHexLin[3][0][0][2]= 5.773503e-01;
  uvwHexLin[3][0][1][0]=1.000000e+00; uvwHexLin[3][0][1][1]=-5.773503e-01; uvwHexLin[3][0][1][2]= 5.773503e-01;
  uvwHexLin[3][0][2][0]=1.000000e+00; uvwHexLin[3][0][2][1]= 5.773503e-01; uvwHexLin[3][0][2][2]=-5.773503e-01;
  uvwHexLin[3][0][3][0]=1.000000e+00; uvwHexLin[3][0][3][1]=-5.773503e-01; uvwHexLin[3][0][3][2]=-5.773503e-01;
  //
  uvwHexLin[3][1][1][0]=1.000000e+00; uvwHexLin[3][1][1][1]= 5.773503e-01; uvwHexLin[3][1][1][2]= 5.773503e-01;
  uvwHexLin[3][1][3][0]=1.000000e+00; uvwHexLin[3][1][3][1]=-5.773503e-01; uvwHexLin[3][1][3][2]= 5.773503e-01;
  uvwHexLin[3][1][0][0]=1.000000e+00; uvwHexLin[3][1][0][1]= 5.773503e-01; uvwHexLin[3][1][0][2]=-5.773503e-01;
  uvwHexLin[3][1][2][0]=1.000000e+00; uvwHexLin[3][1][2][1]=-5.773503e-01; uvwHexLin[3][1][2][2]=-5.773503e-01;
  //
  uvwHexLin[3][2][2][0]=1.000000e+00; uvwHexLin[3][2][2][1]= 5.773503e-01; uvwHexLin[3][2][2][2]= 5.773503e-01;
  uvwHexLin[3][2][0][0]=1.000000e+00; uvwHexLin[3][2][0][1]=-5.773503e-01; uvwHexLin[3][2][0][2]= 5.773503e-01;
  uvwHexLin[3][2][3][0]=1.000000e+00; uvwHexLin[3][2][3][1]= 5.773503e-01; uvwHexLin[3][2][3][2]=-5.773503e-01;
  uvwHexLin[3][2][1][0]=1.000000e+00; uvwHexLin[3][2][1][1]=-5.773503e-01; uvwHexLin[3][2][1][2]=-5.773503e-01;
  //
  uvwHexLin[3][3][3][0]=1.000000e+00; uvwHexLin[3][3][3][1]= 5.773503e-01; uvwHexLin[3][3][3][2]= 5.773503e-01;
  uvwHexLin[3][3][2][0]=1.000000e+00; uvwHexLin[3][3][2][1]=-5.773503e-01; uvwHexLin[3][3][2][2]= 5.773503e-01;
  uvwHexLin[3][3][1][0]=1.000000e+00; uvwHexLin[3][3][1][1]= 5.773503e-01; uvwHexLin[3][3][1][2]=-5.773503e-01;
  uvwHexLin[3][3][0][0]=1.000000e+00; uvwHexLin[3][3][0][1]=-5.773503e-01; uvwHexLin[3][3][0][2]=-5.773503e-01;


  uvwHexLin[4][0][0][0]=-5.773503e-01; uvwHexLin[4][0][0][1]=1.000000e+00; uvwHexLin[4][0][0][2]= 5.773503e-01;
  uvwHexLin[4][0][1][0]= 5.773503e-01; uvwHexLin[4][0][1][1]=1.000000e+00; uvwHexLin[4][0][1][2]= 5.773503e-01;
  uvwHexLin[4][0][2][0]=-5.773503e-01; uvwHexLin[4][0][2][1]=1.000000e+00; uvwHexLin[4][0][2][2]=-5.773503e-01;
  uvwHexLin[4][0][3][0]= 5.773503e-01; uvwHexLin[4][0][3][1]=1.000000e+00; uvwHexLin[4][0][3][2]=-5.773503e-01;
  //
  uvwHexLin[4][1][1][0]=-5.773503e-01; uvwHexLin[4][1][1][1]=1.000000e+00; uvwHexLin[4][1][1][2]= 5.773503e-01;
  uvwHexLin[4][1][3][0]= 5.773503e-01; uvwHexLin[4][1][3][1]=1.000000e+00; uvwHexLin[4][1][3][2]= 5.773503e-01;
  uvwHexLin[4][1][0][0]=-5.773503e-01; uvwHexLin[4][1][0][1]=1.000000e+00; uvwHexLin[4][1][0][2]=-5.773503e-01;
  uvwHexLin[4][1][2][0]= 5.773503e-01; uvwHexLin[4][1][2][1]=1.000000e+00; uvwHexLin[4][1][2][2]=-5.773503e-01;
  //
  uvwHexLin[4][2][2][0]=-5.773503e-01; uvwHexLin[4][2][2][1]=1.000000e+00; uvwHexLin[4][2][2][2]= 5.773503e-01;
  uvwHexLin[4][2][0][0]= 5.773503e-01; uvwHexLin[4][2][0][1]=1.000000e+00; uvwHexLin[4][2][0][2]= 5.773503e-01;
  uvwHexLin[4][2][3][0]=-5.773503e-01; uvwHexLin[4][2][3][1]=1.000000e+00; uvwHexLin[4][2][3][2]=-5.773503e-01;
  uvwHexLin[4][2][1][0]= 5.773503e-01; uvwHexLin[4][2][1][1]=1.000000e+00; uvwHexLin[4][2][1][2]=-5.773503e-01;
  //
  uvwHexLin[4][3][3][0]=-5.773503e-01; uvwHexLin[4][3][3][1]=1.000000e+00; uvwHexLin[4][3][3][2]= 5.773503e-01;
  uvwHexLin[4][3][2][0]= 5.773503e-01; uvwHexLin[4][3][2][1]=1.000000e+00; uvwHexLin[4][3][2][2]= 5.773503e-01;
  uvwHexLin[4][3][1][0]=-5.773503e-01; uvwHexLin[4][3][1][1]=1.000000e+00; uvwHexLin[4][3][1][2]=-5.773503e-01;
  uvwHexLin[4][3][0][0]= 5.773503e-01; uvwHexLin[4][3][0][1]=1.000000e+00; uvwHexLin[4][3][0][2]=-5.773503e-01;

  uvwHexLin[5][0][0][0]= 5.773503e-01; uvwHexLin[5][0][0][1]= 5.773503e-01; uvwHexLin[5][0][0][2]=1.000000e+00;
  uvwHexLin[5][0][1][0]=-5.773503e-01; uvwHexLin[5][0][1][1]= 5.773503e-01; uvwHexLin[5][0][1][2]=1.000000e+00;
  uvwHexLin[5][0][2][0]= 5.773503e-01; uvwHexLin[5][0][2][1]=-5.773503e-01; uvwHexLin[5][0][2][2]=1.000000e+00;
  uvwHexLin[5][0][3][0]=-5.773503e-01; uvwHexLin[5][0][3][1]=-5.773503e-01; uvwHexLin[5][0][3][2]=1.000000e+00;
  //
  uvwHexLin[5][1][1][0]= 5.773503e-01; uvwHexLin[5][1][1][1]= 5.773503e-01; uvwHexLin[5][1][1][2]=1.000000e+00;
  uvwHexLin[5][1][3][0]=-5.773503e-01; uvwHexLin[5][1][3][1]= 5.773503e-01; uvwHexLin[5][1][3][2]=1.000000e+00;
  uvwHexLin[5][1][0][0]= 5.773503e-01; uvwHexLin[5][1][0][1]=-5.773503e-01; uvwHexLin[5][1][0][2]=1.000000e+00;
  uvwHexLin[5][1][2][0]=-5.773503e-01; uvwHexLin[5][1][2][1]=-5.773503e-01; uvwHexLin[5][1][2][2]=1.000000e+00;
  //
  uvwHexLin[5][2][2][0]= 5.773503e-01; uvwHexLin[5][2][2][1]= 5.773503e-01; uvwHexLin[5][2][2][2]=1.000000e+00;
  uvwHexLin[5][2][0][0]=-5.773503e-01; uvwHexLin[5][2][0][1]= 5.773503e-01; uvwHexLin[5][2][0][2]=1.000000e+00;
  uvwHexLin[5][2][3][0]= 5.773503e-01; uvwHexLin[5][2][3][1]=-5.773503e-01; uvwHexLin[5][2][3][2]=1.000000e+00;
  uvwHexLin[5][2][1][0]=-5.773503e-01; uvwHexLin[5][2][1][1]=-5.773503e-01; uvwHexLin[5][2][1][2]=1.000000e+00;
  //
  uvwHexLin[5][3][3][0]= 5.773503e-01; uvwHexLin[5][3][3][1]= 5.773503e-01; uvwHexLin[5][3][3][2]=1.000000e+00;
  uvwHexLin[5][3][2][0]=-5.773503e-01; uvwHexLin[5][3][2][1]= 5.773503e-01; uvwHexLin[5][3][2][2]=1.000000e+00;
  uvwHexLin[5][3][1][0]= 5.773503e-01; uvwHexLin[5][3][1][1]=-5.773503e-01; uvwHexLin[5][3][1][2]=1.000000e+00;
  uvwHexLin[5][3][0][0]=-5.773503e-01; uvwHexLin[5][3][0][1]=-5.773503e-01; uvwHexLin[5][3][0][2]=1.000000e+00;

  //uvwHexLinI
  uvwHexLinI[0][0][0][0]= 5.773503e-01; uvwHexLinI[0][0][0][1]= 5.773503e-01; uvwHexLinI[0][0][0][2]=-1.000000e+00;
  uvwHexLinI[0][0][1][0]=-5.773503e-01; uvwHexLinI[0][0][1][1]= 5.773503e-01; uvwHexLinI[0][0][1][2]=-1.000000e+00;
  uvwHexLinI[0][0][2][0]= 5.773503e-01; uvwHexLinI[0][0][2][1]=-5.773503e-01; uvwHexLinI[0][0][2][2]=-1.000000e+00;
  uvwHexLinI[0][0][3][0]=-5.773503e-01; uvwHexLinI[0][0][3][1]=-5.773503e-01; uvwHexLinI[0][0][3][2]=-1.000000e+00;
  //
  uvwHexLinI[0][1][1][0]= 5.773503e-01; uvwHexLinI[0][1][0][1]= 5.773503e-01; uvwHexLinI[0][1][0][2]=-1.000000e+00;
  uvwHexLinI[0][1][3][0]=-5.773503e-01; uvwHexLinI[0][1][3][1]= 5.773503e-01; uvwHexLinI[0][1][3][2]=-1.000000e+00;
  uvwHexLinI[0][1][0][0]= 5.773503e-01; uvwHexLinI[0][1][0][1]=-5.773503e-01; uvwHexLinI[0][1][0][2]=-1.000000e+00;
  uvwHexLinI[0][1][2][0]=-5.773503e-01; uvwHexLinI[0][1][2][1]=-5.773503e-01; uvwHexLinI[0][1][2][2]=-1.000000e+00;
  //
  uvwHexLinI[0][2][2][0]= 5.773503e-01; uvwHexLinI[0][2][2][1]= 5.773503e-01; uvwHexLinI[0][2][2][2]=-1.000000e+00;
  uvwHexLinI[0][2][0][0]=-5.773503e-01; uvwHexLinI[0][2][0][1]= 5.773503e-01; uvwHexLinI[0][2][0][2]=-1.000000e+00;
  uvwHexLinI[0][2][3][0]= 5.773503e-01; uvwHexLinI[0][2][3][1]=-5.773503e-01; uvwHexLinI[0][2][3][2]=-1.000000e+00;
  uvwHexLinI[0][2][1][0]=-5.773503e-01; uvwHexLinI[0][2][1][1]=-5.773503e-01; uvwHexLinI[0][2][1][2]=-1.000000e+00;
  //
  uvwHexLinI[0][3][3][0]= 5.773503e-01; uvwHexLinI[0][3][3][1]= 5.773503e-01; uvwHexLinI[0][3][3][2]=-1.000000e+00;
  uvwHexLinI[0][3][2][0]=-5.773503e-01; uvwHexLinI[0][3][2][1]= 5.773503e-01; uvwHexLinI[0][3][2][2]=-1.000000e+00;
  uvwHexLinI[0][3][1][0]= 5.773503e-01; uvwHexLinI[0][3][1][1]=-5.773503e-01; uvwHexLinI[0][3][1][2]=-1.000000e+00;
  uvwHexLinI[0][3][0][0]=-5.773503e-01; uvwHexLinI[0][3][0][1]=-5.773503e-01; uvwHexLinI[0][3][0][2]=-1.000000e+00;


  uvwHexLinI[1][0][0][0]= 5.773503e-01; uvwHexLinI[1][0][0][1]=-1.000000e+00; uvwHexLinI[1][0][0][2]= 5.773503e-01;
  uvwHexLinI[1][0][1][0]= 5.773503e-01; uvwHexLinI[1][0][1][1]=-1.000000e+00; uvwHexLinI[1][0][1][2]=-5.773503e-01;
  uvwHexLinI[1][0][2][0]=-5.773503e-01; uvwHexLinI[1][0][2][1]=-1.000000e+00; uvwHexLinI[1][0][2][2]= 5.773503e-01;
  uvwHexLinI[1][0][3][0]=-5.773503e-01; uvwHexLinI[1][0][3][1]=-1.000000e+00; uvwHexLinI[1][0][3][2]=-5.773503e-01;
  //
  uvwHexLinI[1][1][1][0]= 5.773503e-01; uvwHexLinI[1][1][1][1]=-1.000000e+00; uvwHexLinI[1][1][1][2]= 5.773503e-01;
  uvwHexLinI[1][1][3][0]= 5.773503e-01; uvwHexLinI[1][1][3][1]=-1.000000e+00; uvwHexLinI[1][1][3][2]=-5.773503e-01;
  uvwHexLinI[1][1][0][0]=-5.773503e-01; uvwHexLinI[1][1][0][1]=-1.000000e+00; uvwHexLinI[1][1][0][2]= 5.773503e-01;
  uvwHexLinI[1][1][2][0]=-5.773503e-01; uvwHexLinI[1][1][2][1]=-1.000000e+00; uvwHexLinI[1][1][2][2]=-5.773503e-01;
  //
  uvwHexLinI[1][2][2][0]= 5.773503e-01; uvwHexLinI[1][2][2][1]=-1.000000e+00; uvwHexLinI[1][2][2][2]= 5.773503e-01;
  uvwHexLinI[1][2][0][0]= 5.773503e-01; uvwHexLinI[1][2][0][1]=-1.000000e+00; uvwHexLinI[1][2][0][2]=-5.773503e-01;
  uvwHexLinI[1][2][3][0]=-5.773503e-01; uvwHexLinI[1][2][3][1]=-1.000000e+00; uvwHexLinI[1][2][3][2]= 5.773503e-01;
  uvwHexLinI[1][2][1][0]=-5.773503e-01; uvwHexLinI[1][2][1][1]=-1.000000e+00; uvwHexLinI[1][2][1][2]=-5.773503e-01;
  //
  uvwHexLinI[1][3][3][0]= 5.773503e-01; uvwHexLinI[1][3][3][1]=-1.000000e+00; uvwHexLinI[1][3][3][2]= 5.773503e-01;
  uvwHexLinI[1][3][2][0]= 5.773503e-01; uvwHexLinI[1][3][2][1]=-1.000000e+00; uvwHexLinI[1][3][2][2]=-5.773503e-01;
  uvwHexLinI[1][3][1][0]=-5.773503e-01; uvwHexLinI[1][3][1][1]=-1.000000e+00; uvwHexLinI[1][3][1][2]= 5.773503e-01;
  uvwHexLinI[1][3][0][0]=-5.773503e-01; uvwHexLinI[1][3][0][1]=-1.000000e+00; uvwHexLinI[1][3][0][2]=-5.773503e-01;


  uvwHexLinI[2][0][0][0]=-1.000000e+00; uvwHexLinI[2][0][0][1]= 5.773503e-01; uvwHexLinI[2][0][0][2]= 5.773503e-01;
  uvwHexLinI[2][0][1][0]=-1.000000e+00; uvwHexLinI[2][0][1][1]=-5.773503e-01; uvwHexLinI[2][0][1][2]= 5.773503e-01;
  uvwHexLinI[2][0][2][0]=-1.000000e+00; uvwHexLinI[2][0][2][1]= 5.773503e-01; uvwHexLinI[2][0][2][2]=-5.773503e-01;
  uvwHexLinI[2][0][3][0]=-1.000000e+00; uvwHexLinI[2][0][3][1]=-5.773503e-01; uvwHexLinI[2][0][3][2]=-5.773503e-01;
  //
  uvwHexLinI[2][1][1][0]=-1.000000e+00; uvwHexLinI[2][1][1][1]= 5.773503e-01; uvwHexLinI[2][1][1][2]= 5.773503e-01;
  uvwHexLinI[2][1][3][0]=-1.000000e+00; uvwHexLinI[2][1][3][1]=-5.773503e-01; uvwHexLinI[2][1][3][2]= 5.773503e-01;
  uvwHexLinI[2][1][0][0]=-1.000000e+00; uvwHexLinI[2][1][0][1]= 5.773503e-01; uvwHexLinI[2][1][0][2]=-5.773503e-01;
  uvwHexLinI[2][1][2][0]=-1.000000e+00; uvwHexLinI[2][1][2][1]=-5.773503e-01; uvwHexLinI[2][1][2][2]=-5.773503e-01;
  //
  uvwHexLinI[2][2][2][0]=-1.000000e+00; uvwHexLinI[2][2][2][1]= 5.773503e-01; uvwHexLinI[2][2][2][2]= 5.773503e-01;
  uvwHexLinI[2][2][0][0]=-1.000000e+00; uvwHexLinI[2][2][0][1]=-5.773503e-01; uvwHexLinI[2][2][0][2]= 5.773503e-01;
  uvwHexLinI[2][2][3][0]=-1.000000e+00; uvwHexLinI[2][2][3][1]= 5.773503e-01; uvwHexLinI[2][2][3][2]=-5.773503e-01;
  uvwHexLinI[2][2][1][0]=-1.000000e+00; uvwHexLinI[2][2][1][1]=-5.773503e-01; uvwHexLinI[2][2][1][2]=-5.773503e-01;
  //
  uvwHexLinI[2][3][3][0]=-1.000000e+00; uvwHexLinI[2][3][3][1]= 5.773503e-01; uvwHexLinI[2][3][3][2]= 5.773503e-01;
  uvwHexLinI[2][3][2][0]=-1.000000e+00; uvwHexLinI[2][3][2][1]=-5.773503e-01; uvwHexLinI[2][3][2][2]= 5.773503e-01;
  uvwHexLinI[2][3][1][0]=-1.000000e+00; uvwHexLinI[2][3][1][1]= 5.773503e-01; uvwHexLinI[2][3][1][2]=-5.773503e-01;
  uvwHexLinI[2][3][0][0]=-1.000000e+00; uvwHexLinI[2][3][0][1]=-5.773503e-01; uvwHexLinI[2][3][0][2]=-5.773503e-01;


  uvwHexLinI[3][0][0][0]=1.000000e+00; uvwHexLinI[3][0][0][1]= 5.773503e-01; uvwHexLinI[3][0][0][2]= 5.773503e-01;
  uvwHexLinI[3][0][1][0]=1.000000e+00; uvwHexLinI[3][0][1][1]= 5.773503e-01; uvwHexLinI[3][0][1][2]=-5.773503e-01;
  uvwHexLinI[3][0][2][0]=1.000000e+00; uvwHexLinI[3][0][2][1]=-5.773503e-01; uvwHexLinI[3][0][2][2]= 5.773503e-01;
  uvwHexLinI[3][0][3][0]=1.000000e+00; uvwHexLinI[3][0][3][1]=-5.773503e-01; uvwHexLinI[3][0][3][2]=-5.773503e-01;
  //
  uvwHexLinI[3][1][1][0]=1.000000e+00; uvwHexLinI[3][1][1][1]= 5.773503e-01; uvwHexLinI[3][1][1][2]= 5.773503e-01;
  uvwHexLinI[3][1][3][0]=1.000000e+00; uvwHexLinI[3][1][3][1]= 5.773503e-01; uvwHexLinI[3][1][3][2]=-5.773503e-01;
  uvwHexLinI[3][1][0][0]=1.000000e+00; uvwHexLinI[3][1][0][1]=-5.773503e-01; uvwHexLinI[3][1][0][2]= 5.773503e-01;
  uvwHexLinI[3][1][2][0]=1.000000e+00; uvwHexLinI[3][1][2][1]=-5.773503e-01; uvwHexLinI[3][1][2][2]=-5.773503e-01;
  //
  uvwHexLinI[3][2][2][0]=1.000000e+00; uvwHexLinI[3][2][2][1]= 5.773503e-01; uvwHexLinI[3][2][2][2]= 5.773503e-01;
  uvwHexLinI[3][2][0][0]=1.000000e+00; uvwHexLinI[3][2][0][1]= 5.773503e-01; uvwHexLinI[3][2][0][2]=-5.773503e-01;
  uvwHexLinI[3][2][3][0]=1.000000e+00; uvwHexLinI[3][2][3][1]=-5.773503e-01; uvwHexLinI[3][2][3][2]= 5.773503e-01;
  uvwHexLinI[3][2][1][0]=1.000000e+00; uvwHexLinI[3][2][1][1]=-5.773503e-01; uvwHexLinI[3][2][1][2]=-5.773503e-01;
  //
  uvwHexLinI[3][3][3][0]=1.000000e+00; uvwHexLinI[3][3][3][1]= 5.773503e-01; uvwHexLinI[3][3][3][2]= 5.773503e-01;
  uvwHexLinI[3][3][2][0]=1.000000e+00; uvwHexLinI[3][3][2][1]= 5.773503e-01; uvwHexLinI[3][3][2][2]=-5.773503e-01;
  uvwHexLinI[3][3][1][0]=1.000000e+00; uvwHexLinI[3][3][1][1]=-5.773503e-01; uvwHexLinI[3][3][1][2]= 5.773503e-01;
  uvwHexLinI[3][3][0][0]=1.000000e+00; uvwHexLinI[3][3][0][1]=-5.773503e-01; uvwHexLinI[3][3][0][2]=-5.773503e-01;


  uvwHexLinI[4][0][0][0]=-5.773503e-01; uvwHexLinI[4][0][0][1]=1.000000e+00; uvwHexLinI[4][0][0][2]= 5.773503e-01;
  uvwHexLinI[4][0][1][0]=-5.773503e-01; uvwHexLinI[4][0][1][1]=1.000000e+00; uvwHexLinI[4][0][1][2]=-5.773503e-01;
  uvwHexLinI[4][0][2][0]= 5.773503e-01; uvwHexLinI[4][0][2][1]=1.000000e+00; uvwHexLinI[4][0][2][2]= 5.773503e-01;
  uvwHexLinI[4][0][3][0]= 5.773503e-01; uvwHexLinI[4][0][3][1]=1.000000e+00; uvwHexLinI[4][0][3][2]=-5.773503e-01;
  //
  uvwHexLinI[4][1][1][0]=-5.773503e-01; uvwHexLinI[4][1][1][1]=1.000000e+00; uvwHexLinI[4][1][1][2]= 5.773503e-01;
  uvwHexLinI[4][1][3][0]=-5.773503e-01; uvwHexLinI[4][1][3][1]=1.000000e+00; uvwHexLinI[4][1][3][2]=-5.773503e-01;
  uvwHexLinI[4][1][0][0]= 5.773503e-01; uvwHexLinI[4][1][0][1]=1.000000e+00; uvwHexLinI[4][1][0][2]= 5.773503e-01;
  uvwHexLinI[4][1][2][0]= 5.773503e-01; uvwHexLinI[4][1][2][1]=1.000000e+00; uvwHexLinI[4][1][2][2]=-5.773503e-01;
  //
  uvwHexLinI[4][2][2][0]=-5.773503e-01; uvwHexLinI[4][2][2][1]=1.000000e+00; uvwHexLinI[4][2][2][2]= 5.773503e-01;
  uvwHexLinI[4][2][0][0]=-5.773503e-01; uvwHexLinI[4][2][0][1]=1.000000e+00; uvwHexLinI[4][2][0][2]=-5.773503e-01;
  uvwHexLinI[4][2][3][0]= 5.773503e-01; uvwHexLinI[4][2][3][1]=1.000000e+00; uvwHexLinI[4][2][3][2]= 5.773503e-01;
  uvwHexLinI[4][2][1][0]= 5.773503e-01; uvwHexLinI[4][2][1][1]=1.000000e+00; uvwHexLinI[4][2][1][2]=-5.773503e-01;
  //
  uvwHexLinI[4][3][3][0]=-5.773503e-01; uvwHexLinI[4][3][3][1]=1.000000e+00; uvwHexLinI[4][3][3][2]= 5.773503e-01;
  uvwHexLinI[4][3][2][0]=-5.773503e-01; uvwHexLinI[4][3][2][1]=1.000000e+00; uvwHexLinI[4][3][2][2]=-5.773503e-01;
  uvwHexLinI[4][3][1][0]= 5.773503e-01; uvwHexLinI[4][3][1][1]=1.000000e+00; uvwHexLinI[4][3][1][2]= 5.773503e-01;
  uvwHexLinI[4][3][0][0]= 5.773503e-01; uvwHexLinI[4][3][0][1]=1.000000e+00; uvwHexLinI[4][3][0][2]=-5.773503e-01;


  uvwHexLinI[5][0][0][0]= 5.773503e-01; uvwHexLinI[5][0][0][1]= 5.773503e-01; uvwHexLinI[5][0][0][2]=1.000000e+00;
  uvwHexLinI[5][0][1][0]= 5.773503e-01; uvwHexLinI[5][0][1][1]=-5.773503e-01; uvwHexLinI[5][0][1][2]=1.000000e+00;
  uvwHexLinI[5][0][2][0]=-5.773503e-01; uvwHexLinI[5][0][2][1]= 5.773503e-01; uvwHexLinI[5][0][2][2]=1.000000e+00;
  uvwHexLinI[5][0][3][0]=-5.773503e-01; uvwHexLinI[5][0][3][1]=-5.773503e-01; uvwHexLinI[5][0][3][2]=1.000000e+00;
  //
  uvwHexLinI[5][1][1][0]= 5.773503e-01; uvwHexLinI[5][1][1][1]= 5.773503e-01; uvwHexLinI[5][1][1][2]=1.000000e+00;
  uvwHexLinI[5][1][3][0]= 5.773503e-01; uvwHexLinI[5][1][3][1]=-5.773503e-01; uvwHexLinI[5][1][3][2]=1.000000e+00;
  uvwHexLinI[5][1][0][0]=-5.773503e-01; uvwHexLinI[5][1][0][1]= 5.773503e-01; uvwHexLinI[5][1][0][2]=1.000000e+00;
  uvwHexLinI[5][1][2][0]=-5.773503e-01; uvwHexLinI[5][1][2][1]=-5.773503e-01; uvwHexLinI[5][1][2][2]=1.000000e+00;
  //
  uvwHexLinI[5][2][2][0]= 5.773503e-01; uvwHexLinI[5][2][2][1]= 5.773503e-01; uvwHexLinI[5][2][2][2]=1.000000e+00;
  uvwHexLinI[5][2][0][0]= 5.773503e-01; uvwHexLinI[5][2][0][1]=-5.773503e-01; uvwHexLinI[5][2][0][2]=1.000000e+00;
  uvwHexLinI[5][2][3][0]=-5.773503e-01; uvwHexLinI[5][2][3][1]= 5.773503e-01; uvwHexLinI[5][2][3][2]=1.000000e+00;
  uvwHexLinI[5][2][1][0]=-5.773503e-01; uvwHexLinI[5][2][1][1]=-5.773503e-01; uvwHexLinI[5][2][1][2]=1.000000e+00;
  //
  uvwHexLinI[5][3][3][0]= 5.773503e-01; uvwHexLinI[5][3][3][1]= 5.773503e-01; uvwHexLinI[5][3][3][2]=1.000000e+00;
  uvwHexLinI[5][3][2][0]= 5.773503e-01; uvwHexLinI[5][3][2][1]=-5.773503e-01; uvwHexLinI[5][3][2][2]=1.000000e+00;
  uvwHexLinI[5][3][1][0]=-5.773503e-01; uvwHexLinI[5][3][1][1]= 5.773503e-01; uvwHexLinI[5][3][1][2]=1.000000e+00;
  uvwHexLinI[5][3][0][0]=-5.773503e-01; uvwHexLinI[5][3][0][1]=-5.773503e-01; uvwHexLinI[5][3][0][2]=1.000000e+00;


  // Quadratic hexahedra
  uvwHexQuad[0][0][0][0]=-7.745967e-01; uvwHexQuad[0][0][0][1]=-7.745967e-01; uvwHexQuad[0][0][0][2]=-1.000000e+00;
  uvwHexQuad[0][0][1][0]= 0.;           uvwHexQuad[0][0][1][1]=-7.745967e-01; uvwHexQuad[0][0][1][2]=-1.000000e+00;
  uvwHexQuad[0][0][2][0]= 7.745967e-01; uvwHexQuad[0][0][2][1]=-7.745967e-01; uvwHexQuad[0][0][2][2]=-1.000000e+00;
  uvwHexQuad[0][0][3][0]=-7.745967e-01; uvwHexQuad[0][0][3][1]= 0.;           uvwHexQuad[0][0][3][2]=-1.000000e+00;
  uvwHexQuad[0][0][4][0]= 0.;           uvwHexQuad[0][0][4][1]= 0.;           uvwHexQuad[0][0][4][2]=-1.000000e+00;
  uvwHexQuad[0][0][5][0]= 7.745967e-01; uvwHexQuad[0][0][5][1]= 0.;           uvwHexQuad[0][0][5][2]=-1.000000e+00;
  uvwHexQuad[0][0][6][0]=-7.745967e-01; uvwHexQuad[0][0][6][1]= 7.745967e-01; uvwHexQuad[0][0][6][2]=-1.000000e+00;
  uvwHexQuad[0][0][7][0]= 0.;           uvwHexQuad[0][0][7][1]= 7.745967e-01; uvwHexQuad[0][0][7][2]=-1.000000e+00;
  uvwHexQuad[0][0][8][0]= 7.745967e-01; uvwHexQuad[0][0][8][1]= 7.745967e-01; uvwHexQuad[0][0][8][2]=-1.000000e+00;
  //
  uvwHexQuad[0][1][6][0]=-7.745967e-01; uvwHexQuad[0][1][6][1]=-7.745967e-01; uvwHexQuad[0][1][6][2]=-1.000000e+00;
  uvwHexQuad[0][1][3][0]= 0.;           uvwHexQuad[0][1][3][1]=-7.745967e-01; uvwHexQuad[0][1][3][2]=-1.000000e+00;
  uvwHexQuad[0][1][0][0]= 7.745967e-01; uvwHexQuad[0][1][0][1]=-7.745967e-01; uvwHexQuad[0][1][0][2]=-1.000000e+00;
  uvwHexQuad[0][1][7][0]=-7.745967e-01; uvwHexQuad[0][1][7][1]= 0.;           uvwHexQuad[0][1][7][2]=-1.000000e+00;
  uvwHexQuad[0][1][4][0]= 0.;           uvwHexQuad[0][1][4][1]= 0.;           uvwHexQuad[0][1][4][2]=-1.000000e+00;
  uvwHexQuad[0][1][1][0]= 7.745967e-01; uvwHexQuad[0][1][1][1]= 0.;           uvwHexQuad[0][1][1][2]=-1.000000e+00;
  uvwHexQuad[0][1][8][0]=-7.745967e-01; uvwHexQuad[0][1][8][1]= 7.745967e-01; uvwHexQuad[0][1][8][2]=-1.000000e+00;
  uvwHexQuad[0][1][5][0]= 0.;           uvwHexQuad[0][1][5][1]= 7.745967e-01; uvwHexQuad[0][1][5][2]=-1.000000e+00;
  uvwHexQuad[0][1][2][0]= 7.745967e-01; uvwHexQuad[0][1][2][1]= 7.745967e-01; uvwHexQuad[0][1][2][2]=-1.000000e+00;
  //
  uvwHexQuad[0][2][2][0]=-7.745967e-01; uvwHexQuad[0][2][2][1]=-7.745967e-01; uvwHexQuad[0][2][2][2]=-1.000000e+00;
  uvwHexQuad[0][2][5][0]= 0.;           uvwHexQuad[0][2][5][1]=-7.745967e-01; uvwHexQuad[0][2][5][2]=-1.000000e+00;
  uvwHexQuad[0][2][8][0]= 7.745967e-01; uvwHexQuad[0][2][8][1]=-7.745967e-01; uvwHexQuad[0][2][8][2]=-1.000000e+00;
  uvwHexQuad[0][2][1][0]=-7.745967e-01; uvwHexQuad[0][2][1][1]= 0.;           uvwHexQuad[0][2][1][2]=-1.000000e+00;
  uvwHexQuad[0][2][4][0]= 0.;           uvwHexQuad[0][2][4][1]= 0.;           uvwHexQuad[0][2][4][2]=-1.000000e+00;
  uvwHexQuad[0][2][7][0]= 7.745967e-01; uvwHexQuad[0][2][7][1]= 0.;           uvwHexQuad[0][2][7][2]=-1.000000e+00;
  uvwHexQuad[0][2][0][0]=-7.745967e-01; uvwHexQuad[0][2][0][1]= 7.745967e-01; uvwHexQuad[0][2][0][2]=-1.000000e+00;
  uvwHexQuad[0][2][3][0]= 0.;           uvwHexQuad[0][2][3][1]= 7.745967e-01; uvwHexQuad[0][2][3][2]=-1.000000e+00;
  uvwHexQuad[0][2][6][0]= 7.745967e-01; uvwHexQuad[0][2][6][1]= 7.745967e-01; uvwHexQuad[0][2][6][2]=-1.000000e+00;
  //
  uvwHexQuad[0][3][8][0]=-7.745967e-01; uvwHexQuad[0][3][8][1]=-7.745967e-01; uvwHexQuad[0][3][8][2]=-1.000000e+00;
  uvwHexQuad[0][3][7][0]= 0.;           uvwHexQuad[0][3][7][1]=-7.745967e-01; uvwHexQuad[0][3][7][2]=-1.000000e+00;
  uvwHexQuad[0][3][6][0]= 7.745967e-01; uvwHexQuad[0][3][6][1]=-7.745967e-01; uvwHexQuad[0][3][6][2]=-1.000000e+00;
  uvwHexQuad[0][3][5][0]=-7.745967e-01; uvwHexQuad[0][3][5][1]= 0.;           uvwHexQuad[0][3][5][2]=-1.000000e+00;
  uvwHexQuad[0][3][4][0]= 0.;           uvwHexQuad[0][3][4][1]= 0.;           uvwHexQuad[0][3][4][2]=-1.000000e+00;
  uvwHexQuad[0][3][3][0]= 7.745967e-01; uvwHexQuad[0][3][3][1]= 0.;           uvwHexQuad[0][3][3][2]=-1.000000e+00;
  uvwHexQuad[0][3][2][0]=-7.745967e-01; uvwHexQuad[0][3][2][1]= 7.745967e-01; uvwHexQuad[0][3][2][2]=-1.000000e+00;
  uvwHexQuad[0][3][1][0]= 0.;           uvwHexQuad[0][3][1][1]= 7.745967e-01; uvwHexQuad[0][3][1][2]=-1.000000e+00;
  uvwHexQuad[0][3][0][0]= 7.745967e-01; uvwHexQuad[0][3][0][1]= 7.745967e-01; uvwHexQuad[0][3][0][2]=-1.000000e+00;


  uvwHexQuad[1][0][0][0]=-7.745967e-01; uvwHexQuad[1][0][0][1]=-1.000000e+00; uvwHexQuad[1][0][0][2]=-7.745967e-01;
  uvwHexQuad[1][0][1][0]=-7.745967e-01; uvwHexQuad[1][0][1][1]=-1.000000e+00; uvwHexQuad[1][0][1][2]= 0.;
  uvwHexQuad[1][0][2][0]=-7.745967e-01; uvwHexQuad[1][0][2][1]=-1.000000e+00; uvwHexQuad[1][0][2][2]= 7.745967e-01;
  uvwHexQuad[1][0][3][0]= 0.;           uvwHexQuad[1][0][3][1]=-1.000000e+00; uvwHexQuad[1][0][3][2]=-7.745967e-01;
  uvwHexQuad[1][0][4][0]= 0.;           uvwHexQuad[1][0][4][1]=-1.000000e+00; uvwHexQuad[1][0][4][2]= 0.;
  uvwHexQuad[1][0][5][0]= 0.;           uvwHexQuad[1][0][5][1]=-1.000000e+00; uvwHexQuad[1][0][5][2]= 7.745967e-01;
  uvwHexQuad[1][0][6][0]= 7.745967e-01; uvwHexQuad[1][0][6][1]=-1.000000e+00; uvwHexQuad[1][0][6][2]=-7.745967e-01;
  uvwHexQuad[1][0][7][0]= 7.745967e-01; uvwHexQuad[1][0][7][1]=-1.000000e+00; uvwHexQuad[1][0][7][2]= 0.;
  uvwHexQuad[1][0][8][0]= 7.745967e-01; uvwHexQuad[1][0][8][1]=-1.000000e+00; uvwHexQuad[1][0][8][2]= 7.745967e-01;
  //
  uvwHexQuad[1][1][6][0]=-7.745967e-01; uvwHexQuad[1][1][6][1]=-1.000000e+00; uvwHexQuad[1][1][6][2]=-7.745967e-01;
  uvwHexQuad[1][1][3][0]=-7.745967e-01; uvwHexQuad[1][1][3][1]=-1.000000e+00; uvwHexQuad[1][1][3][2]= 0.;
  uvwHexQuad[1][1][0][0]=-7.745967e-01; uvwHexQuad[1][1][0][1]=-1.000000e+00; uvwHexQuad[1][1][0][2]= 7.745967e-01;
  uvwHexQuad[1][1][7][0]= 0.;           uvwHexQuad[1][1][7][1]=-1.000000e+00; uvwHexQuad[1][1][7][2]=-7.745967e-01;
  uvwHexQuad[1][1][4][0]= 0.;           uvwHexQuad[1][1][4][1]=-1.000000e+00; uvwHexQuad[1][1][4][2]= 0.;
  uvwHexQuad[1][1][1][0]= 0.;           uvwHexQuad[1][1][1][1]=-1.000000e+00; uvwHexQuad[1][1][1][2]= 7.745967e-01;
  uvwHexQuad[1][1][8][0]= 7.745967e-01; uvwHexQuad[1][1][8][1]=-1.000000e+00; uvwHexQuad[1][1][8][2]=-7.745967e-01;
  uvwHexQuad[1][1][5][0]= 7.745967e-01; uvwHexQuad[1][1][5][1]=-1.000000e+00; uvwHexQuad[1][1][5][2]= 0.;
  uvwHexQuad[1][1][2][0]= 7.745967e-01; uvwHexQuad[1][1][2][1]=-1.000000e+00; uvwHexQuad[1][1][2][2]= 7.745967e-01;
  //
  uvwHexQuad[1][2][2][0]=-7.745967e-01; uvwHexQuad[1][2][2][1]=-1.000000e+00; uvwHexQuad[1][2][2][2]=-7.745967e-01;
  uvwHexQuad[1][2][5][0]=-7.745967e-01; uvwHexQuad[1][2][5][1]=-1.000000e+00; uvwHexQuad[1][2][5][2]= 0.;
  uvwHexQuad[1][2][8][0]=-7.745967e-01; uvwHexQuad[1][2][8][1]=-1.000000e+00; uvwHexQuad[1][2][8][2]= 7.745967e-01;
  uvwHexQuad[1][2][1][0]= 0.;           uvwHexQuad[1][2][1][1]=-1.000000e+00; uvwHexQuad[1][2][1][2]=-7.745967e-01;
  uvwHexQuad[1][2][4][0]= 0.;           uvwHexQuad[1][2][4][1]=-1.000000e+00; uvwHexQuad[1][2][4][2]= 0.;
  uvwHexQuad[1][2][7][0]= 0.;           uvwHexQuad[1][2][7][1]=-1.000000e+00; uvwHexQuad[1][2][7][2]= 7.745967e-01;
  uvwHexQuad[1][2][0][0]= 7.745967e-01; uvwHexQuad[1][2][0][1]=-1.000000e+00; uvwHexQuad[1][2][0][2]=-7.745967e-01;
  uvwHexQuad[1][2][3][0]= 7.745967e-01; uvwHexQuad[1][2][3][1]=-1.000000e+00; uvwHexQuad[1][2][3][2]= 0.;
  uvwHexQuad[1][2][6][0]= 7.745967e-01; uvwHexQuad[1][2][6][1]=-1.000000e+00; uvwHexQuad[1][2][6][2]= 7.745967e-01;
  //
  uvwHexQuad[1][3][8][0]=-7.745967e-01; uvwHexQuad[1][3][8][1]=-1.000000e+00; uvwHexQuad[1][3][8][2]=-7.745967e-01;
  uvwHexQuad[1][3][7][0]=-7.745967e-01; uvwHexQuad[1][3][7][1]=-1.000000e+00; uvwHexQuad[1][3][7][2]= 0.;
  uvwHexQuad[1][3][6][0]=-7.745967e-01; uvwHexQuad[1][3][6][1]=-1.000000e+00; uvwHexQuad[1][3][6][2]= 7.745967e-01;
  uvwHexQuad[1][3][5][0]= 0.;           uvwHexQuad[1][3][5][1]=-1.000000e+00; uvwHexQuad[1][3][5][2]=-7.745967e-01;
  uvwHexQuad[1][3][4][0]= 0.;           uvwHexQuad[1][3][4][1]=-1.000000e+00; uvwHexQuad[1][3][4][2]= 0.;
  uvwHexQuad[1][3][3][0]= 0.;           uvwHexQuad[1][3][3][1]=-1.000000e+00; uvwHexQuad[1][3][3][2]= 7.745967e-01;
  uvwHexQuad[1][3][2][0]= 7.745967e-01; uvwHexQuad[1][3][2][1]=-1.000000e+00; uvwHexQuad[1][3][2][2]=-7.745967e-01;
  uvwHexQuad[1][3][1][0]= 7.745967e-01; uvwHexQuad[1][3][1][1]=-1.000000e+00; uvwHexQuad[1][3][1][2]= 0.;
  uvwHexQuad[1][3][0][0]= 7.745967e-01; uvwHexQuad[1][3][0][1]=-1.000000e+00; uvwHexQuad[1][3][0][2]= 7.745967e-01;

  uvwHexQuad[2][0][0][0]=-1.000000e+00; uvwHexQuad[2][0][0][1]=-7.745967e-01; uvwHexQuad[2][0][0][2]=-7.745967e-01;
  uvwHexQuad[2][0][1][0]=-1.000000e+00; uvwHexQuad[2][0][1][1]= 0.;           uvwHexQuad[2][0][1][2]=-7.745967e-01;
  uvwHexQuad[2][0][2][0]=-1.000000e+00; uvwHexQuad[2][0][2][1]= 7.745967e-01; uvwHexQuad[2][0][2][2]=-7.745967e-01;
  uvwHexQuad[2][0][3][0]=-1.000000e+00; uvwHexQuad[2][0][3][1]=-7.745967e-01; uvwHexQuad[2][0][3][2]= 0.;
  uvwHexQuad[2][0][4][0]=-1.000000e+00; uvwHexQuad[2][0][4][1]= 0.;           uvwHexQuad[2][0][4][2]= 0.;
  uvwHexQuad[2][0][5][0]=-1.000000e+00; uvwHexQuad[2][0][5][1]= 7.745967e-01; uvwHexQuad[2][0][5][2]= 0.;
  uvwHexQuad[2][0][6][0]=-1.000000e+00; uvwHexQuad[2][0][6][1]=-7.745967e-01; uvwHexQuad[2][0][6][2]= 7.745967e-01;
  uvwHexQuad[2][0][7][0]=-1.000000e+00; uvwHexQuad[2][0][7][1]= 0.;           uvwHexQuad[2][0][7][2]= 7.745967e-01;
  uvwHexQuad[2][0][8][0]=-1.000000e+00; uvwHexQuad[2][0][8][1]= 7.745967e-01; uvwHexQuad[2][0][8][2]= 7.745967e-01;
  //
  uvwHexQuad[2][1][6][0]=-1.000000e+00; uvwHexQuad[2][1][6][1]=-7.745967e-01; uvwHexQuad[2][1][6][2]=-7.745967e-01;
  uvwHexQuad[2][1][3][0]=-1.000000e+00; uvwHexQuad[2][1][3][1]= 0.;           uvwHexQuad[2][1][3][2]=-7.745967e-01;
  uvwHexQuad[2][1][0][0]=-1.000000e+00; uvwHexQuad[2][1][0][1]= 7.745967e-01; uvwHexQuad[2][1][0][2]=-7.745967e-01;
  uvwHexQuad[2][1][7][0]=-1.000000e+00; uvwHexQuad[2][1][7][1]=-7.745967e-01; uvwHexQuad[2][1][7][2]= 0.;
  uvwHexQuad[2][1][4][0]=-1.000000e+00; uvwHexQuad[2][1][4][1]= 0.;           uvwHexQuad[2][1][4][2]= 0.;
  uvwHexQuad[2][1][1][0]=-1.000000e+00; uvwHexQuad[2][1][1][1]= 7.745967e-01; uvwHexQuad[2][1][1][2]= 0.;
  uvwHexQuad[2][1][8][0]=-1.000000e+00; uvwHexQuad[2][1][8][1]=-7.745967e-01; uvwHexQuad[2][1][8][2]= 7.745967e-01;
  uvwHexQuad[2][1][5][0]=-1.000000e+00; uvwHexQuad[2][1][5][1]= 0.;           uvwHexQuad[2][1][5][2]= 7.745967e-01;
  uvwHexQuad[2][1][2][0]=-1.000000e+00; uvwHexQuad[2][1][2][1]= 7.745967e-01; uvwHexQuad[2][1][2][2]= 7.745967e-01;
  //
  uvwHexQuad[2][2][2][0]=-1.000000e+00; uvwHexQuad[2][2][2][1]=-7.745967e-01; uvwHexQuad[2][2][2][2]=-7.745967e-01;
  uvwHexQuad[2][2][5][0]=-1.000000e+00; uvwHexQuad[2][2][5][1]= 0.;           uvwHexQuad[2][2][5][2]=-7.745967e-01;
  uvwHexQuad[2][2][8][0]=-1.000000e+00; uvwHexQuad[2][2][8][1]= 7.745967e-01; uvwHexQuad[2][2][8][2]=-7.745967e-01;
  uvwHexQuad[2][2][1][0]=-1.000000e+00; uvwHexQuad[2][2][1][1]=-7.745967e-01; uvwHexQuad[2][2][1][2]= 0.;
  uvwHexQuad[2][2][4][0]=-1.000000e+00; uvwHexQuad[2][2][4][1]= 0.;           uvwHexQuad[2][2][4][2]= 0.;
  uvwHexQuad[2][2][7][0]=-1.000000e+00; uvwHexQuad[2][2][7][1]= 7.745967e-01; uvwHexQuad[2][2][7][2]= 0.;
  uvwHexQuad[2][2][0][0]=-1.000000e+00; uvwHexQuad[2][2][0][1]=-7.745967e-01; uvwHexQuad[2][2][0][2]= 7.745967e-01;
  uvwHexQuad[2][2][3][0]=-1.000000e+00; uvwHexQuad[2][2][3][1]= 0.;           uvwHexQuad[2][2][3][2]= 7.745967e-01;
  uvwHexQuad[2][2][6][0]=-1.000000e+00; uvwHexQuad[2][2][6][1]= 7.745967e-01; uvwHexQuad[2][2][6][2]= 7.745967e-01;
  //
  uvwHexQuad[2][3][8][0]=-1.000000e+00; uvwHexQuad[2][3][8][1]=-7.745967e-01; uvwHexQuad[2][3][8][2]=-7.745967e-01;
  uvwHexQuad[2][3][7][0]=-1.000000e+00; uvwHexQuad[2][3][7][1]= 0.;           uvwHexQuad[2][3][7][2]=-7.745967e-01;
  uvwHexQuad[2][3][6][0]=-1.000000e+00; uvwHexQuad[2][3][6][1]= 7.745967e-01; uvwHexQuad[2][3][6][2]=-7.745967e-01;
  uvwHexQuad[2][3][5][0]=-1.000000e+00; uvwHexQuad[2][3][5][1]=-7.745967e-01; uvwHexQuad[2][3][5][2]= 0.;
  uvwHexQuad[2][3][4][0]=-1.000000e+00; uvwHexQuad[2][3][4][1]= 0.;           uvwHexQuad[2][3][4][2]= 0.;
  uvwHexQuad[2][3][3][0]=-1.000000e+00; uvwHexQuad[2][3][3][1]= 7.745967e-01; uvwHexQuad[2][3][3][2]= 0.;
  uvwHexQuad[2][3][2][0]=-1.000000e+00; uvwHexQuad[2][3][2][1]=-7.745967e-01; uvwHexQuad[2][3][2][2]= 7.745967e-01;
  uvwHexQuad[2][3][1][0]=-1.000000e+00; uvwHexQuad[2][3][1][1]= 0.;           uvwHexQuad[2][3][1][2]= 7.745967e-01;
  uvwHexQuad[2][3][0][0]=-1.000000e+00; uvwHexQuad[2][3][0][1]= 7.745967e-01; uvwHexQuad[2][3][0][2]= 7.745967e-01;


  uvwHexQuad[3][0][0][0]= 1.000000e+00; uvwHexQuad[3][0][0][1]=-7.745967e-01; uvwHexQuad[3][0][0][2]=-7.745967e-01;
  uvwHexQuad[3][0][1][0]= 1.000000e+00; uvwHexQuad[3][0][1][1]=-7.745967e-01; uvwHexQuad[3][0][1][2]= 0.;
  uvwHexQuad[3][0][2][0]= 1.000000e+00; uvwHexQuad[3][0][2][1]=-7.745967e-01; uvwHexQuad[3][0][2][2]= 7.745967e-01;
  uvwHexQuad[3][0][3][0]= 1.000000e+00; uvwHexQuad[3][0][3][1]= 0.;           uvwHexQuad[3][0][3][2]=-7.745967e-01;
  uvwHexQuad[3][0][4][0]= 1.000000e+00; uvwHexQuad[3][0][4][1]= 0.;           uvwHexQuad[3][0][4][2]= 0.;
  uvwHexQuad[3][0][5][0]= 1.000000e+00; uvwHexQuad[3][0][5][1]= 0.;           uvwHexQuad[3][0][5][2]= 7.745967e-01;
  uvwHexQuad[3][0][6][0]= 1.000000e+00; uvwHexQuad[3][0][6][1]= 7.745967e-01; uvwHexQuad[3][0][6][2]=-7.745967e-01;
  uvwHexQuad[3][0][7][0]= 1.000000e+00; uvwHexQuad[3][0][7][1]= 7.745967e-01; uvwHexQuad[3][0][7][2]= 0.;
  uvwHexQuad[3][0][8][0]= 1.000000e+00; uvwHexQuad[3][0][8][1]= 7.745967e-01; uvwHexQuad[3][0][8][2]= 7.745967e-01;
  //
  uvwHexQuad[3][1][6][0]= 1.000000e+00; uvwHexQuad[3][1][6][1]=-7.745967e-01; uvwHexQuad[3][1][6][2]=-7.745967e-01;
  uvwHexQuad[3][1][3][0]= 1.000000e+00; uvwHexQuad[3][1][3][1]=-7.745967e-01; uvwHexQuad[3][1][3][2]= 0.;
  uvwHexQuad[3][1][0][0]= 1.000000e+00; uvwHexQuad[3][1][0][1]=-7.745967e-01; uvwHexQuad[3][1][0][2]= 7.745967e-01;
  uvwHexQuad[3][1][7][0]= 1.000000e+00; uvwHexQuad[3][1][7][1]= 0.;           uvwHexQuad[3][1][7][2]=-7.745967e-01;
  uvwHexQuad[3][1][4][0]= 1.000000e+00; uvwHexQuad[3][1][4][1]= 0.;           uvwHexQuad[3][1][4][2]= 0.;
  uvwHexQuad[3][1][1][0]= 1.000000e+00; uvwHexQuad[3][1][1][1]= 0.;           uvwHexQuad[3][1][1][2]= 7.745967e-01;
  uvwHexQuad[3][1][8][0]= 1.000000e+00; uvwHexQuad[3][1][8][1]= 7.745967e-01; uvwHexQuad[3][1][8][2]=-7.745967e-01;
  uvwHexQuad[3][1][5][0]= 1.000000e+00; uvwHexQuad[3][1][5][1]= 7.745967e-01; uvwHexQuad[3][1][5][2]= 0.;
  uvwHexQuad[3][1][2][0]= 1.000000e+00; uvwHexQuad[3][1][2][1]= 7.745967e-01; uvwHexQuad[3][1][2][2]= 7.745967e-01;
  //
  uvwHexQuad[3][2][2][0]= 1.000000e+00; uvwHexQuad[3][2][2][1]=-7.745967e-01; uvwHexQuad[3][2][2][2]=-7.745967e-01;
  uvwHexQuad[3][2][5][0]= 1.000000e+00; uvwHexQuad[3][2][5][1]=-7.745967e-01; uvwHexQuad[3][2][5][2]= 0.;
  uvwHexQuad[3][2][8][0]= 1.000000e+00; uvwHexQuad[3][2][8][1]=-7.745967e-01; uvwHexQuad[3][2][8][2]= 7.745967e-01;
  uvwHexQuad[3][2][1][0]= 1.000000e+00; uvwHexQuad[3][2][1][1]= 0.;           uvwHexQuad[3][2][1][2]=-7.745967e-01;
  uvwHexQuad[3][2][4][0]= 1.000000e+00; uvwHexQuad[3][2][4][1]= 0.;           uvwHexQuad[3][2][4][2]= 0.;
  uvwHexQuad[3][2][7][0]= 1.000000e+00; uvwHexQuad[3][2][7][1]= 0.;           uvwHexQuad[3][2][7][2]= 7.745967e-01;
  uvwHexQuad[3][2][0][0]= 1.000000e+00; uvwHexQuad[3][2][0][1]= 7.745967e-01; uvwHexQuad[3][2][0][2]=-7.745967e-01;
  uvwHexQuad[3][2][3][0]= 1.000000e+00; uvwHexQuad[3][2][3][1]= 7.745967e-01; uvwHexQuad[3][2][3][2]= 0.;
  uvwHexQuad[3][2][6][0]= 1.000000e+00; uvwHexQuad[3][2][6][1]= 7.745967e-01; uvwHexQuad[3][2][6][2]= 7.745967e-01;
  //
  uvwHexQuad[3][3][8][0]= 1.000000e+00; uvwHexQuad[3][3][8][1]=-7.745967e-01; uvwHexQuad[3][3][8][2]=-7.745967e-01;
  uvwHexQuad[3][3][7][0]= 1.000000e+00; uvwHexQuad[3][3][7][1]=-7.745967e-01; uvwHexQuad[3][3][7][2]= 0.;
  uvwHexQuad[3][3][6][0]= 1.000000e+00; uvwHexQuad[3][3][6][1]=-7.745967e-01; uvwHexQuad[3][3][6][2]= 7.745967e-01;
  uvwHexQuad[3][3][5][0]= 1.000000e+00; uvwHexQuad[3][3][5][1]= 0.;           uvwHexQuad[3][3][5][2]=-7.745967e-01;
  uvwHexQuad[3][3][4][0]= 1.000000e+00; uvwHexQuad[3][3][4][1]= 0.;           uvwHexQuad[3][3][4][2]= 0.;
  uvwHexQuad[3][3][3][0]= 1.000000e+00; uvwHexQuad[3][3][3][1]= 0.;           uvwHexQuad[3][3][3][2]= 7.745967e-01;
  uvwHexQuad[3][3][2][0]= 1.000000e+00; uvwHexQuad[3][3][2][1]= 7.745967e-01; uvwHexQuad[3][3][2][2]=-7.745967e-01;
  uvwHexQuad[3][3][1][0]= 1.000000e+00; uvwHexQuad[3][3][1][1]= 7.745967e-01; uvwHexQuad[3][3][1][2]= 0.;
  uvwHexQuad[3][3][0][0]= 1.000000e+00; uvwHexQuad[3][3][0][1]= 7.745967e-01; uvwHexQuad[3][3][0][2]= 7.745967e-01;

  uvwHexQuad[4][0][0][0]= 7.745967e-01; uvwHexQuad[4][0][0][1]= 1.000000e+00; uvwHexQuad[4][0][0][2]=-7.745967e-01;
  uvwHexQuad[4][0][1][0]= 7.745967e-01; uvwHexQuad[4][0][1][1]= 1.000000e+00; uvwHexQuad[4][0][1][2]= 0.;
  uvwHexQuad[4][0][2][0]= 7.745967e-01; uvwHexQuad[4][0][2][1]= 1.000000e+00; uvwHexQuad[4][0][2][2]= 7.745967e-01;
  uvwHexQuad[4][0][3][0]= 0.;           uvwHexQuad[4][0][3][1]= 1.000000e+00; uvwHexQuad[4][0][3][2]=-7.745967e-01;
  uvwHexQuad[4][0][4][0]= 0.;           uvwHexQuad[4][0][4][1]= 1.000000e+00; uvwHexQuad[4][0][4][2]= 0.;
  uvwHexQuad[4][0][5][0]= 0.;           uvwHexQuad[4][0][5][1]= 1.000000e+00; uvwHexQuad[4][0][5][2]= 7.745967e-01;
  uvwHexQuad[4][0][6][0]=-7.745967e-01; uvwHexQuad[4][0][6][1]= 1.000000e+00; uvwHexQuad[4][0][6][2]=-7.745967e-01;
  uvwHexQuad[4][0][7][0]=-7.745967e-01; uvwHexQuad[4][0][7][1]= 1.000000e+00; uvwHexQuad[4][0][7][2]= 0.;
  uvwHexQuad[4][0][8][0]=-7.745967e-01; uvwHexQuad[4][0][8][1]= 1.000000e+00; uvwHexQuad[4][0][8][2]= 7.745967e-01;
  //
  uvwHexQuad[4][1][6][0]= 7.745967e-01; uvwHexQuad[4][1][6][1]= 1.000000e+00; uvwHexQuad[4][1][6][2]=-7.745967e-01;
  uvwHexQuad[4][1][3][0]= 7.745967e-01; uvwHexQuad[4][1][3][1]= 1.000000e+00; uvwHexQuad[4][1][3][2]= 0.;
  uvwHexQuad[4][1][0][0]= 7.745967e-01; uvwHexQuad[4][1][0][1]= 1.000000e+00; uvwHexQuad[4][1][0][2]= 7.745967e-01;
  uvwHexQuad[4][1][7][0]= 0.;           uvwHexQuad[4][1][7][1]= 1.000000e+00; uvwHexQuad[4][1][7][2]=-7.745967e-01;
  uvwHexQuad[4][1][4][0]= 0.;           uvwHexQuad[4][1][4][1]= 1.000000e+00; uvwHexQuad[4][1][4][2]= 0.;
  uvwHexQuad[4][1][1][0]= 0.;           uvwHexQuad[4][1][1][1]= 1.000000e+00; uvwHexQuad[4][1][1][2]= 7.745967e-01;
  uvwHexQuad[4][1][8][0]=-7.745967e-01; uvwHexQuad[4][1][8][1]= 1.000000e+00; uvwHexQuad[4][1][8][2]=-7.745967e-01;
  uvwHexQuad[4][1][5][0]=-7.745967e-01; uvwHexQuad[4][1][5][1]= 1.000000e+00; uvwHexQuad[4][1][5][2]= 0.;
  uvwHexQuad[4][1][2][0]=-7.745967e-01; uvwHexQuad[4][1][2][1]= 1.000000e+00; uvwHexQuad[4][1][2][2]= 7.745967e-01;
  //
  uvwHexQuad[4][2][2][0]= 7.745967e-01; uvwHexQuad[4][2][2][1]= 1.000000e+00; uvwHexQuad[4][2][2][2]=-7.745967e-01;
  uvwHexQuad[4][2][5][0]= 7.745967e-01; uvwHexQuad[4][2][5][1]= 1.000000e+00; uvwHexQuad[4][2][5][2]= 0.;
  uvwHexQuad[4][2][8][0]= 7.745967e-01; uvwHexQuad[4][2][8][1]= 1.000000e+00; uvwHexQuad[4][2][8][2]= 7.745967e-01;
  uvwHexQuad[4][2][1][0]= 0.;           uvwHexQuad[4][2][1][1]= 1.000000e+00; uvwHexQuad[4][2][1][2]=-7.745967e-01;
  uvwHexQuad[4][2][4][0]= 0.;           uvwHexQuad[4][2][4][1]= 1.000000e+00; uvwHexQuad[4][2][4][2]= 0.;
  uvwHexQuad[4][2][7][0]= 0.;           uvwHexQuad[4][2][7][1]= 1.000000e+00; uvwHexQuad[4][2][7][2]= 7.745967e-01;
  uvwHexQuad[4][2][0][0]=-7.745967e-01; uvwHexQuad[4][2][0][1]= 1.000000e+00; uvwHexQuad[4][2][0][2]=-7.745967e-01;
  uvwHexQuad[4][2][3][0]=-7.745967e-01; uvwHexQuad[4][2][3][1]= 1.000000e+00; uvwHexQuad[4][2][3][2]= 0.;
  uvwHexQuad[4][2][6][0]=-7.745967e-01; uvwHexQuad[4][2][6][1]= 1.000000e+00; uvwHexQuad[4][2][6][2]= 7.745967e-01;
  //
  uvwHexQuad[4][3][8][0]= 7.745967e-01; uvwHexQuad[4][3][8][1]= 1.000000e+00; uvwHexQuad[4][3][8][2]=-7.745967e-01;
  uvwHexQuad[4][3][7][0]= 7.745967e-01; uvwHexQuad[4][3][7][1]= 1.000000e+00; uvwHexQuad[4][3][7][2]= 0.;
  uvwHexQuad[4][3][6][0]= 7.745967e-01; uvwHexQuad[4][3][6][1]= 1.000000e+00; uvwHexQuad[4][3][6][2]= 7.745967e-01;
  uvwHexQuad[4][3][5][0]= 0.;           uvwHexQuad[4][3][5][1]= 1.000000e+00; uvwHexQuad[4][3][5][2]=-7.745967e-01;
  uvwHexQuad[4][3][4][0]= 0.;           uvwHexQuad[4][3][4][1]= 1.000000e+00; uvwHexQuad[4][3][4][2]= 0.;
  uvwHexQuad[4][3][3][0]= 0.;           uvwHexQuad[4][3][3][1]= 1.000000e+00; uvwHexQuad[4][3][3][2]= 7.745967e-01;
  uvwHexQuad[4][3][2][0]=-7.745967e-01; uvwHexQuad[4][3][2][1]= 1.000000e+00; uvwHexQuad[4][3][2][2]=-7.745967e-01;
  uvwHexQuad[4][3][1][0]=-7.745967e-01; uvwHexQuad[4][3][1][1]= 1.000000e+00; uvwHexQuad[4][3][1][2]= 0.;
  uvwHexQuad[4][3][0][0]=-7.745967e-01; uvwHexQuad[4][3][0][1]= 1.000000e+00; uvwHexQuad[4][3][0][2]= 7.745967e-01;

  uvwHexQuad[5][0][0][0]=-7.745967e-01; uvwHexQuad[5][0][0][1]=-7.745967e-01; uvwHexQuad[5][0][0][2]= 1.000000e+00;
  uvwHexQuad[5][0][1][0]=-7.745967e-01; uvwHexQuad[5][0][1][1]= 0. ;          uvwHexQuad[5][0][1][2]= 1.000000e+00;
  uvwHexQuad[5][0][2][0]=-7.745967e-01; uvwHexQuad[5][0][2][1]= 7.745967e-01; uvwHexQuad[5][0][2][2]= 1.000000e+00;
  uvwHexQuad[5][0][3][0]= 0.;           uvwHexQuad[5][0][3][1]=-7.745967e-01; uvwHexQuad[5][0][3][2]= 1.000000e+00;
  uvwHexQuad[5][0][4][0]= 0.;           uvwHexQuad[5][0][4][1]= 0.;           uvwHexQuad[5][0][4][2]= 1.000000e+00;
  uvwHexQuad[5][0][5][0]= 0.;           uvwHexQuad[5][0][5][1]= 7.745967e-01; uvwHexQuad[5][0][5][2]= 1.000000e+00;
  uvwHexQuad[5][0][6][0]= 7.745967e-01; uvwHexQuad[5][0][6][1]=-7.745967e-01; uvwHexQuad[5][0][6][2]= 1.000000e+00;
  uvwHexQuad[5][0][7][0]= 7.745967e-01; uvwHexQuad[5][0][7][1]=-0.;           uvwHexQuad[5][0][7][2]= 1.000000e+00;
  uvwHexQuad[5][0][8][0]= 7.745967e-01; uvwHexQuad[5][0][8][1]= 7.745967e-01; uvwHexQuad[5][0][8][2]= 1.000000e+00;
  //
  uvwHexQuad[5][1][6][0]=-7.745967e-01; uvwHexQuad[5][1][6][1]=-7.745967e-01; uvwHexQuad[5][1][6][2]= 1.000000e+00;
  uvwHexQuad[5][1][3][0]=-7.745967e-01; uvwHexQuad[5][1][3][1]= 0. ;          uvwHexQuad[5][1][3][2]= 1.000000e+00;
  uvwHexQuad[5][1][0][0]=-7.745967e-01; uvwHexQuad[5][1][0][1]= 7.745967e-01; uvwHexQuad[5][1][0][2]= 1.000000e+00;
  uvwHexQuad[5][1][7][0]= 0.;           uvwHexQuad[5][1][7][1]=-7.745967e-01; uvwHexQuad[5][1][7][2]= 1.000000e+00;
  uvwHexQuad[5][1][4][0]= 0.;           uvwHexQuad[5][1][4][1]= 0.;           uvwHexQuad[5][1][4][2]= 1.000000e+00;
  uvwHexQuad[5][1][1][0]= 0.;           uvwHexQuad[5][1][1][1]= 7.745967e-01; uvwHexQuad[5][1][1][2]= 1.000000e+00;
  uvwHexQuad[5][1][8][0]= 7.745967e-01; uvwHexQuad[5][1][8][1]=-7.745967e-01; uvwHexQuad[5][1][8][2]= 1.000000e+00;
  uvwHexQuad[5][1][5][0]= 7.745967e-01; uvwHexQuad[5][1][5][1]=-0.;           uvwHexQuad[5][1][5][2]= 1.000000e+00;
  uvwHexQuad[5][1][2][0]= 7.745967e-01; uvwHexQuad[5][1][2][1]= 7.745967e-01; uvwHexQuad[5][1][2][2]= 1.000000e+00;
  //
  uvwHexQuad[5][2][2][0]=-7.745967e-01; uvwHexQuad[5][2][2][1]=-7.745967e-01; uvwHexQuad[5][2][2][2]= 1.000000e+00;
  uvwHexQuad[5][2][5][0]=-7.745967e-01; uvwHexQuad[5][2][5][1]= 0. ;          uvwHexQuad[5][2][5][2]= 1.000000e+00;
  uvwHexQuad[5][2][8][0]=-7.745967e-01; uvwHexQuad[5][2][8][1]= 7.745967e-01; uvwHexQuad[5][2][8][2]= 1.000000e+00;
  uvwHexQuad[5][2][1][0]= 0.;           uvwHexQuad[5][2][1][1]=-7.745967e-01; uvwHexQuad[5][2][1][2]= 1.000000e+00;
  uvwHexQuad[5][2][4][0]= 0.;           uvwHexQuad[5][2][4][1]= 0.;           uvwHexQuad[5][2][4][2]= 1.000000e+00;
  uvwHexQuad[5][2][7][0]= 0.;           uvwHexQuad[5][2][7][1]= 7.745967e-01; uvwHexQuad[5][2][7][2]= 1.000000e+00;
  uvwHexQuad[5][2][0][0]= 7.745967e-01; uvwHexQuad[5][2][0][1]=-7.745967e-01; uvwHexQuad[5][2][0][2]= 1.000000e+00;
  uvwHexQuad[5][2][3][0]= 7.745967e-01; uvwHexQuad[5][2][3][1]=-0.;           uvwHexQuad[5][2][3][2]= 1.000000e+00;
  uvwHexQuad[5][2][6][0]= 7.745967e-01; uvwHexQuad[5][2][6][1]= 7.745967e-01; uvwHexQuad[5][2][6][2]= 1.000000e+00;
  //
  uvwHexQuad[5][3][8][0]=-7.745967e-01; uvwHexQuad[5][3][8][1]=-7.745967e-01; uvwHexQuad[5][3][8][2]= 1.000000e+00;
  uvwHexQuad[5][3][7][0]=-7.745967e-01; uvwHexQuad[5][3][7][1]= 0. ;          uvwHexQuad[5][3][7][2]= 1.000000e+00;
  uvwHexQuad[5][3][6][0]=-7.745967e-01; uvwHexQuad[5][3][6][1]= 7.745967e-01; uvwHexQuad[5][3][6][2]= 1.000000e+00;
  uvwHexQuad[5][3][5][0]= 0.;           uvwHexQuad[5][3][5][1]=-7.745967e-01; uvwHexQuad[5][3][5][2]= 1.000000e+00;
  uvwHexQuad[5][3][4][0]= 0.;           uvwHexQuad[5][3][4][1]= 0.;           uvwHexQuad[5][3][4][2]= 1.000000e+00;
  uvwHexQuad[5][3][3][0]= 0.;           uvwHexQuad[5][3][3][1]= 7.745967e-01; uvwHexQuad[5][3][3][2]= 1.000000e+00;
  uvwHexQuad[5][3][2][0]= 7.745967e-01; uvwHexQuad[5][3][2][1]=-7.745967e-01; uvwHexQuad[5][3][2][2]= 1.000000e+00;
  uvwHexQuad[5][3][1][0]= 7.745967e-01; uvwHexQuad[5][3][1][1]=-0.;           uvwHexQuad[5][3][1][2]= 1.000000e+00;
  uvwHexQuad[5][3][0][0]= 7.745967e-01; uvwHexQuad[5][3][0][1]= 7.745967e-01; uvwHexQuad[5][3][0][2]= 1.000000e+00;


  uvwHexQuadI[0][0][0][0]=-7.745967e-01; uvwHexQuadI[0][0][0][1]=-7.745967e-01; uvwHexQuadI[0][0][0][2]=-1.000000e+00;
  uvwHexQuadI[0][0][1][0]=-7.745967e-01; uvwHexQuadI[0][0][1][1]= 0.;           uvwHexQuadI[0][0][1][2]=-1.000000e+00;
  uvwHexQuadI[0][0][2][0]=-7.745967e-01; uvwHexQuadI[0][0][2][1]= 7.745967e-01; uvwHexQuadI[0][0][2][2]=-1.000000e+00;
  uvwHexQuadI[0][0][3][0]= 0.;           uvwHexQuadI[0][0][3][1]=-7.745967e-01; uvwHexQuadI[0][0][3][2]=-1.000000e+00;
  uvwHexQuadI[0][0][4][0]= 0.;           uvwHexQuadI[0][0][4][1]= 0.;           uvwHexQuadI[0][0][4][2]=-1.000000e+00;
  uvwHexQuadI[0][0][5][0]= 0.;           uvwHexQuadI[0][0][5][1]= 7.745967e-01; uvwHexQuadI[0][0][5][2]=-1.000000e+00;
  uvwHexQuadI[0][0][6][0]= 7.745967e-01; uvwHexQuadI[0][0][6][1]=-7.745967e-01; uvwHexQuadI[0][0][6][2]=-1.000000e+00;
  uvwHexQuadI[0][0][7][0]= 7.745967e-01; uvwHexQuadI[0][0][7][1]= 0.;           uvwHexQuadI[0][0][7][2]=-1.000000e+00;
  uvwHexQuadI[0][0][8][0]= 7.745967e-01; uvwHexQuadI[0][0][8][1]= 7.745967e-01; uvwHexQuadI[0][0][8][2]=-1.000000e+00;
  //
  uvwHexQuadI[0][1][6][0]=-7.745967e-01; uvwHexQuadI[0][1][6][1]=-7.745967e-01; uvwHexQuadI[0][1][6][2]=-1.000000e+00;
  uvwHexQuadI[0][1][3][0]=-7.745967e-01; uvwHexQuadI[0][1][3][1]= 0.;           uvwHexQuadI[0][1][3][2]=-1.000000e+00;
  uvwHexQuadI[0][1][0][0]=-7.745967e-01; uvwHexQuadI[0][1][0][1]= 7.745967e-01; uvwHexQuadI[0][1][0][2]=-1.000000e+00;
  uvwHexQuadI[0][1][7][0]= 0.;           uvwHexQuadI[0][1][7][1]=-7.745967e-01; uvwHexQuadI[0][1][7][2]=-1.000000e+00;
  uvwHexQuadI[0][1][4][0]= 0.;           uvwHexQuadI[0][1][4][1]= 0.;           uvwHexQuadI[0][1][4][2]=-1.000000e+00;
  uvwHexQuadI[0][1][1][0]= 0.;           uvwHexQuadI[0][1][1][1]= 7.745967e-01; uvwHexQuadI[0][1][1][2]=-1.000000e+00;
  uvwHexQuadI[0][1][8][0]= 7.745967e-01; uvwHexQuadI[0][1][8][1]=-7.745967e-01; uvwHexQuadI[0][1][8][2]=-1.000000e+00;
  uvwHexQuadI[0][1][5][0]= 7.745967e-01; uvwHexQuadI[0][1][5][1]= 0.;           uvwHexQuadI[0][1][5][2]=-1.000000e+00;
  uvwHexQuadI[0][1][2][0]= 7.745967e-01; uvwHexQuadI[0][1][2][1]= 7.745967e-01; uvwHexQuadI[0][1][2][2]=-1.000000e+00;
  //
  uvwHexQuadI[0][2][2][0]= 7.745967e-01; uvwHexQuadI[0][2][2][1]=-7.745967e-01; uvwHexQuadI[0][2][2][2]=-1.000000e+00;
  uvwHexQuadI[0][2][5][0]= 0.;           uvwHexQuadI[0][2][5][1]=-7.745967e-01; uvwHexQuadI[0][2][5][2]=-1.000000e+00;
  uvwHexQuadI[0][2][8][0]=-7.745967e-01; uvwHexQuadI[0][2][8][1]=-7.745967e-01; uvwHexQuadI[0][2][8][2]=-1.000000e+00;
  uvwHexQuadI[0][2][1][0]= 7.745967e-01; uvwHexQuadI[0][2][1][1]=-0.;           uvwHexQuadI[0][2][1][2]=-1.000000e+00;
  uvwHexQuadI[0][2][4][0]= 0.;           uvwHexQuadI[0][2][4][1]= 0.;           uvwHexQuadI[0][2][4][2]=-1.000000e+00;
  uvwHexQuadI[0][2][7][0]=-7.745967e-01; uvwHexQuadI[0][2][7][1]= 0.;           uvwHexQuadI[0][2][7][2]=-1.000000e+00;
  uvwHexQuadI[0][2][0][0]= 7.745967e-01; uvwHexQuadI[0][2][0][1]= 7.745967e-01; uvwHexQuadI[0][2][0][2]=-1.000000e+00;
  uvwHexQuadI[0][2][3][0]= 0.;           uvwHexQuadI[0][2][3][1]= 7.745967e-01; uvwHexQuadI[0][2][3][2]=-1.000000e+00;
  uvwHexQuadI[0][2][6][0]= -7.745967e-01; uvwHexQuadI[0][2][6][1]= 7.745967e-01; uvwHexQuadI[0][2][6][2]=-1.000000e+00;
  //
  uvwHexQuadI[0][3][8][0]=-7.745967e-01; uvwHexQuadI[0][3][8][1]=-7.745967e-01; uvwHexQuadI[0][3][8][2]=-1.000000e+00;
  uvwHexQuadI[0][3][7][0]=-7.745967e-01; uvwHexQuadI[0][3][7][1]= 0.;           uvwHexQuadI[0][3][7][2]=-1.000000e+00;
  uvwHexQuadI[0][3][6][0]=-7.745967e-01; uvwHexQuadI[0][3][6][1]= 7.745967e-01; uvwHexQuadI[0][3][6][2]=-1.000000e+00;
  uvwHexQuadI[0][3][5][0]= 0.;           uvwHexQuadI[0][3][5][1]=-7.745967e-01; uvwHexQuadI[0][3][5][2]=-1.000000e+00;
  uvwHexQuadI[0][3][4][0]= 0.;           uvwHexQuadI[0][3][4][1]= 0.;           uvwHexQuadI[0][3][4][2]=-1.000000e+00;
  uvwHexQuadI[0][3][3][0]= 0.;           uvwHexQuadI[0][3][3][1]= 7.745967e-01; uvwHexQuadI[0][3][3][2]=-1.000000e+00;
  uvwHexQuadI[0][3][2][0]= 7.745967e-01; uvwHexQuadI[0][3][2][1]=-7.745967e-01; uvwHexQuadI[0][3][2][2]=-1.000000e+00;
  uvwHexQuadI[0][3][1][0]= 7.745967e-01; uvwHexQuadI[0][3][1][1]= 0.;           uvwHexQuadI[0][3][1][2]=-1.000000e+00;
  uvwHexQuadI[0][3][1][0]= 7.745967e-01; uvwHexQuadI[0][3][0][1]= 7.745967e-01; uvwHexQuadI[0][3][0][2]=-1.000000e+00;


  uvwHexQuadI[1][0][0][0]=-7.745967e-01; uvwHexQuadI[1][0][0][1]=-1.000000e+00; uvwHexQuadI[1][0][0][2]=-7.745967e-01;
  uvwHexQuadI[1][0][1][0]= 0.;           uvwHexQuadI[1][0][1][1]=-1.000000e+00; uvwHexQuadI[1][0][1][2]=-7.745967e-01;
  uvwHexQuadI[1][0][2][0]= 7.745967e-01; uvwHexQuadI[1][0][2][1]=-1.000000e+00; uvwHexQuadI[1][0][2][2]=-7.745967e-01;
  uvwHexQuadI[1][0][3][0]=-7.745967e-01; uvwHexQuadI[1][0][3][1]=-1.000000e+00; uvwHexQuadI[1][0][3][2]= 0.;
  uvwHexQuadI[1][0][4][0]= 0.;           uvwHexQuadI[1][0][4][1]=-1.000000e+00; uvwHexQuadI[1][0][4][2]= 0.;
  uvwHexQuadI[1][0][5][0]= 7.745967e-01; uvwHexQuadI[1][0][5][1]=-1.000000e+00; uvwHexQuadI[1][0][5][2]= 0.;
  uvwHexQuadI[1][0][6][0]=-7.745967e-01; uvwHexQuadI[1][0][6][1]=-1.000000e+00; uvwHexQuadI[1][0][6][2]= 7.745967e-01;
  uvwHexQuadI[1][0][7][0]= 0.;           uvwHexQuadI[1][0][7][1]=-1.000000e+00; uvwHexQuadI[1][0][7][2]= 7.745967e-01;
  uvwHexQuadI[1][0][8][0]= 7.745967e-01; uvwHexQuadI[1][0][8][1]=-1.000000e+00; uvwHexQuadI[1][0][8][2]= 7.745967e-01;
  //
  uvwHexQuadI[1][1][6][0]=-7.745967e-01; uvwHexQuadI[1][1][6][1]=-1.000000e+00; uvwHexQuadI[1][1][6][2]=-7.745967e-01;
  uvwHexQuadI[1][1][3][0]= 0.;           uvwHexQuadI[1][1][3][1]=-1.000000e+00; uvwHexQuadI[1][1][3][2]=-7.745967e-01;
  uvwHexQuadI[1][1][0][0]= 7.745967e-01; uvwHexQuadI[1][1][0][1]=-1.000000e+00; uvwHexQuadI[1][1][0][2]=-7.745967e-01;
  uvwHexQuadI[1][1][7][0]=-7.745967e-01; uvwHexQuadI[1][1][7][1]=-1.000000e+00; uvwHexQuadI[1][1][7][2]= 0.;
  uvwHexQuadI[1][1][4][0]= 0.;           uvwHexQuadI[1][1][4][1]=-1.000000e+00; uvwHexQuadI[1][1][4][2]= 0.;
  uvwHexQuadI[1][1][1][0]= 7.745967e-01; uvwHexQuadI[1][1][1][1]=-1.000000e+00; uvwHexQuadI[1][1][1][2]= 0.;
  uvwHexQuadI[1][1][8][0]=-7.745967e-01; uvwHexQuadI[1][1][8][1]=-1.000000e+00; uvwHexQuadI[1][1][8][2]= 7.745967e-01;
  uvwHexQuadI[1][1][5][0]= 0.;           uvwHexQuadI[1][1][5][1]=-1.000000e+00; uvwHexQuadI[1][1][5][2]= 7.745967e-01;
  uvwHexQuadI[1][1][2][0]= 7.745967e-01; uvwHexQuadI[1][1][2][1]=-1.000000e+00; uvwHexQuadI[1][1][2][2]= 7.745967e-01;
  //
  uvwHexQuadI[1][2][2][0]=-7.745967e-01; uvwHexQuadI[1][2][2][1]=-1.000000e+00; uvwHexQuadI[1][2][2][2]=-7.745967e-01;
  uvwHexQuadI[1][2][5][0]= 0.;           uvwHexQuadI[1][2][5][1]=-1.000000e+00; uvwHexQuadI[1][2][5][2]=-7.745967e-01;
  uvwHexQuadI[1][2][8][0]= 7.745967e-01; uvwHexQuadI[1][2][8][1]=-1.000000e+00; uvwHexQuadI[1][2][8][2]=-7.745967e-01;
  uvwHexQuadI[1][2][1][0]=-7.745967e-01; uvwHexQuadI[1][2][1][1]=-1.000000e+00; uvwHexQuadI[1][2][1][2]= 0.;
  uvwHexQuadI[1][2][4][0]= 0.;           uvwHexQuadI[1][2][4][1]=-1.000000e+00; uvwHexQuadI[1][2][4][2]= 0.;
  uvwHexQuadI[1][2][7][0]= 7.745967e-01; uvwHexQuadI[1][2][7][1]=-1.000000e+00; uvwHexQuadI[1][2][7][2]= 0.;
  uvwHexQuadI[1][2][0][0]=-7.745967e-01; uvwHexQuadI[1][2][0][1]=-1.000000e+00; uvwHexQuadI[1][2][0][2]= 7.745967e-01;
  uvwHexQuadI[1][2][3][0]= 0.;           uvwHexQuadI[1][2][3][1]=-1.000000e+00; uvwHexQuadI[1][2][3][2]= 7.745967e-01;
  uvwHexQuadI[1][2][6][0]= 7.745967e-01; uvwHexQuadI[1][2][6][1]=-1.000000e+00; uvwHexQuadI[1][2][6][2]= 7.745967e-01;
  //
  uvwHexQuadI[1][3][8][0]=-7.745967e-01; uvwHexQuadI[1][3][8][1]=-1.000000e+00; uvwHexQuadI[1][3][8][2]=-7.745967e-01;
  uvwHexQuadI[1][3][7][0]= 0.;           uvwHexQuadI[1][3][7][1]=-1.000000e+00; uvwHexQuadI[1][3][7][2]=-7.745967e-01;
  uvwHexQuadI[1][3][6][0]= 7.745967e-01; uvwHexQuadI[1][3][6][1]=-1.000000e+00; uvwHexQuadI[1][3][6][2]=-7.745967e-01;
  uvwHexQuadI[1][3][5][0]=-7.745967e-01; uvwHexQuadI[1][3][5][1]=-1.000000e+00; uvwHexQuadI[1][3][5][2]= 0.;
  uvwHexQuadI[1][3][4][0]= 0.;           uvwHexQuadI[1][3][4][1]=-1.000000e+00; uvwHexQuadI[1][3][4][2]= 0.;
  uvwHexQuadI[1][3][3][0]= 7.745967e-01; uvwHexQuadI[1][3][3][1]=-1.000000e+00; uvwHexQuadI[1][3][3][2]= 0.;
  uvwHexQuadI[1][3][2][0]=-7.745967e-01; uvwHexQuadI[1][3][2][1]=-1.000000e+00; uvwHexQuadI[1][3][2][2]= 7.745967e-01;
  uvwHexQuadI[1][3][1][0]= 0.;           uvwHexQuadI[1][3][1][1]=-1.000000e+00; uvwHexQuadI[1][3][1][2]= 7.745967e-01;
  uvwHexQuadI[1][3][0][0]= 7.745967e-01; uvwHexQuadI[1][3][0][1]=-1.000000e+00; uvwHexQuadI[1][3][0][2]= 7.745967e-01;


  uvwHexQuadI[2][0][0][0]=-1.000000e+00; uvwHexQuadI[2][0][0][1]=-7.745967e-01; uvwHexQuadI[2][0][0][2]=-7.745967e-01;
  uvwHexQuadI[2][0][1][0]=-1.000000e+00; uvwHexQuadI[2][0][1][1]=-7.745967e-01; uvwHexQuadI[2][0][1][2]= 0.;
  uvwHexQuadI[2][0][2][0]=-1.000000e+00; uvwHexQuadI[2][0][2][1]=-7.745967e-01; uvwHexQuadI[2][0][2][2]= 7.745967e-01;
  uvwHexQuadI[2][0][3][0]=-1.000000e+00; uvwHexQuadI[2][0][3][1]= 0.;           uvwHexQuadI[2][0][3][2]=-7.745967e-01;
  uvwHexQuadI[2][0][4][0]=-1.000000e+00; uvwHexQuadI[2][0][4][1]= 0.;           uvwHexQuadI[2][0][4][2]= 0.;
  uvwHexQuadI[2][0][5][0]=-1.000000e+00; uvwHexQuadI[2][0][5][1]= 0.;           uvwHexQuadI[2][0][5][2]= 7.745967e-01;
  uvwHexQuadI[2][0][6][0]=-1.000000e+00; uvwHexQuadI[2][0][6][1]= 7.745967e-01; uvwHexQuadI[2][0][6][2]=-7.745967e-01;
  uvwHexQuadI[2][0][7][0]=-1.000000e+00; uvwHexQuadI[2][0][7][1]= 7.745967e-01; uvwHexQuadI[2][0][7][2]= 0.;
  uvwHexQuadI[2][0][8][0]=-1.000000e+00; uvwHexQuadI[2][0][8][1]= 7.745967e-01; uvwHexQuadI[2][0][8][2]= 7.745967e-01;
  //
  uvwHexQuadI[2][1][6][0]=-1.000000e+00; uvwHexQuadI[2][1][6][1]=-7.745967e-01; uvwHexQuadI[2][1][6][2]=-7.745967e-01;
  uvwHexQuadI[2][1][3][0]=-1.000000e+00; uvwHexQuadI[2][1][3][1]=-7.745967e-01; uvwHexQuadI[2][1][3][2]= 0.;
  uvwHexQuadI[2][1][0][0]=-1.000000e+00; uvwHexQuadI[2][1][0][1]=-7.745967e-01; uvwHexQuadI[2][1][0][2]= 7.745967e-01;
  uvwHexQuadI[2][1][7][0]=-1.000000e+00; uvwHexQuadI[2][1][7][1]= 0.;           uvwHexQuadI[2][1][7][2]=-7.745967e-01;
  uvwHexQuadI[2][1][4][0]=-1.000000e+00; uvwHexQuadI[2][1][4][1]= 0.;           uvwHexQuadI[2][1][4][2]= 0.;
  uvwHexQuadI[2][1][1][0]=-1.000000e+00; uvwHexQuadI[2][1][1][1]= 0.;           uvwHexQuadI[2][1][1][2]= 7.745967e-01;
  uvwHexQuadI[2][1][8][0]=-1.000000e+00; uvwHexQuadI[2][1][8][1]= 7.745967e-01; uvwHexQuadI[2][1][8][2]=-7.745967e-01;
  uvwHexQuadI[2][1][5][0]=-1.000000e+00; uvwHexQuadI[2][1][5][1]= 7.745967e-01; uvwHexQuadI[2][1][5][2]= 0.;
  uvwHexQuadI[2][1][2][0]=-1.000000e+00; uvwHexQuadI[2][1][2][1]= 7.745967e-01; uvwHexQuadI[2][1][2][2]= 7.745967e-01;
  //
  uvwHexQuadI[2][2][2][0]=-1.000000e+00; uvwHexQuadI[2][2][2][1]=-7.745967e-01; uvwHexQuadI[2][2][2][2]=-7.745967e-01;
  uvwHexQuadI[2][2][5][0]=-1.000000e+00; uvwHexQuadI[2][2][5][1]=-7.745967e-01; uvwHexQuadI[2][2][5][2]= 0.;
  uvwHexQuadI[2][2][8][0]=-1.000000e+00; uvwHexQuadI[2][2][8][1]=-7.745967e-01; uvwHexQuadI[2][2][8][2]= 7.745967e-01;
  uvwHexQuadI[2][2][1][0]=-1.000000e+00; uvwHexQuadI[2][2][1][1]= 0.;           uvwHexQuadI[2][2][1][2]=-7.745967e-01;
  uvwHexQuadI[2][2][4][0]=-1.000000e+00; uvwHexQuadI[2][2][4][1]= 0.;           uvwHexQuadI[2][2][4][2]= 0.;
  uvwHexQuadI[2][2][7][0]=-1.000000e+00; uvwHexQuadI[2][2][7][1]= 0.;           uvwHexQuadI[2][2][7][2]= 7.745967e-01;
  uvwHexQuadI[2][2][0][0]=-1.000000e+00; uvwHexQuadI[2][2][0][1]= 7.745967e-01; uvwHexQuadI[2][2][0][2]=-7.745967e-01;
  uvwHexQuadI[2][2][3][0]=-1.000000e+00; uvwHexQuadI[2][2][3][1]= 7.745967e-01; uvwHexQuadI[2][2][3][2]= 0.;
  uvwHexQuadI[2][2][6][0]=-1.000000e+00; uvwHexQuadI[2][2][6][1]= 7.745967e-01; uvwHexQuadI[2][2][6][2]= 7.745967e-01;
  //
  uvwHexQuadI[2][3][8][0]=-1.000000e+00; uvwHexQuadI[2][3][8][1]=-7.745967e-01; uvwHexQuadI[2][3][8][2]=-7.745967e-01;
  uvwHexQuadI[2][3][7][0]=-1.000000e+00; uvwHexQuadI[2][3][7][1]=-7.745967e-01; uvwHexQuadI[2][3][7][2]= 0.;
  uvwHexQuadI[2][3][6][0]=-1.000000e+00; uvwHexQuadI[2][3][6][1]=-7.745967e-01; uvwHexQuadI[2][3][6][2]= 7.745967e-01;
  uvwHexQuadI[2][3][5][0]=-1.000000e+00; uvwHexQuadI[2][3][5][1]= 0.;           uvwHexQuadI[2][3][5][2]=-7.745967e-01;
  uvwHexQuadI[2][3][4][0]=-1.000000e+00; uvwHexQuadI[2][3][4][1]= 0.;           uvwHexQuadI[2][3][4][2]= 0.;
  uvwHexQuadI[2][3][3][0]=-1.000000e+00; uvwHexQuadI[2][3][3][1]= 0.;           uvwHexQuadI[2][3][3][2]= 7.745967e-01;
  uvwHexQuadI[2][3][2][0]=-1.000000e+00; uvwHexQuadI[2][3][2][1]= 7.745967e-01; uvwHexQuadI[2][3][2][2]=-7.745967e-01;
  uvwHexQuadI[2][3][1][0]=-1.000000e+00; uvwHexQuadI[2][3][1][1]= 7.745967e-01; uvwHexQuadI[2][3][1][2]= 0.;
  uvwHexQuadI[2][3][0][0]=-1.000000e+00; uvwHexQuadI[2][3][0][1]= 7.745967e-01; uvwHexQuadI[2][3][0][2]= 7.745967e-01;

  uvwHexQuadI[3][0][0][0]= 1.000000e+00; uvwHexQuadI[3][0][0][1]=-7.745967e-01; uvwHexQuadI[3][0][0][2]=-7.745967e-01;
  uvwHexQuadI[3][0][1][0]= 1.000000e+00; uvwHexQuadI[3][0][1][1]= 0.;           uvwHexQuadI[3][0][1][2]=-7.745967e-01;
  uvwHexQuadI[3][0][2][0]= 1.000000e+00; uvwHexQuadI[3][0][2][1]= 7.745967e-01; uvwHexQuadI[3][0][2][2]=-7.745967e-01;
  uvwHexQuadI[3][0][3][0]= 1.000000e+00; uvwHexQuadI[3][0][3][1]=-7.745967e-01; uvwHexQuadI[3][0][3][2]= 0.;
  uvwHexQuadI[3][0][4][0]= 1.000000e+00; uvwHexQuadI[3][0][4][1]= 0.;           uvwHexQuadI[3][0][4][2]= 0.;
  uvwHexQuadI[3][0][5][0]= 1.000000e+00; uvwHexQuadI[3][0][5][1]= 7.745967e-01; uvwHexQuadI[3][0][5][2]= 0.;
  uvwHexQuadI[3][0][6][0]= 1.000000e+00; uvwHexQuadI[3][0][6][1]=-7.745967e-01; uvwHexQuadI[3][0][6][2]= 7.745967e-01;
  uvwHexQuadI[3][0][7][0]= 1.000000e+00; uvwHexQuadI[3][0][7][1]= 0.;           uvwHexQuadI[3][0][7][2]= 7.745967e-01;
  uvwHexQuadI[3][0][8][0]= 1.000000e+00; uvwHexQuadI[3][0][8][1]= 7.745967e-01; uvwHexQuadI[3][0][8][2]= 7.745967e-01;
  //
  uvwHexQuadI[3][1][6][0]= 1.000000e+00; uvwHexQuadI[3][1][6][1]=-7.745967e-01; uvwHexQuadI[3][1][6][2]=-7.745967e-01;
  uvwHexQuadI[3][1][3][0]= 1.000000e+00; uvwHexQuadI[3][1][3][1]= 0.;           uvwHexQuadI[3][1][3][2]=-7.745967e-01;
  uvwHexQuadI[3][1][0][0]= 1.000000e+00; uvwHexQuadI[3][1][0][1]= 7.745967e-01; uvwHexQuadI[3][1][0][2]=-7.745967e-01;
  uvwHexQuadI[3][1][7][0]= 1.000000e+00; uvwHexQuadI[3][1][7][1]=-7.745967e-01; uvwHexQuadI[3][1][7][2]= 0.;
  uvwHexQuadI[3][1][4][0]= 1.000000e+00; uvwHexQuadI[3][1][4][1]= 0.;           uvwHexQuadI[3][1][4][2]= 0.;
  uvwHexQuadI[3][1][1][0]= 1.000000e+00; uvwHexQuadI[3][1][1][1]= 7.745967e-01; uvwHexQuadI[3][1][1][2]= 0.;
  uvwHexQuadI[3][1][8][0]= 1.000000e+00; uvwHexQuadI[3][1][8][1]=-7.745967e-01; uvwHexQuadI[3][1][8][2]= 7.745967e-01;
  uvwHexQuadI[3][1][5][0]= 1.000000e+00; uvwHexQuadI[3][1][5][1]= 0.;           uvwHexQuadI[3][1][5][2]= 7.745967e-01;
  uvwHexQuadI[3][1][2][0]= 1.000000e+00; uvwHexQuadI[3][1][2][1]= 7.745967e-01; uvwHexQuadI[3][1][2][2]= 7.745967e-01;
  //
  uvwHexQuadI[3][2][2][0]= 1.000000e+00; uvwHexQuadI[3][2][2][1]=-7.745967e-01; uvwHexQuadI[3][2][2][2]=-7.745967e-01;
  uvwHexQuadI[3][2][5][0]= 1.000000e+00; uvwHexQuadI[3][2][5][1]= 0.;           uvwHexQuadI[3][2][5][2]=-7.745967e-01;
  uvwHexQuadI[3][2][8][0]= 1.000000e+00; uvwHexQuadI[3][2][8][1]= 7.745967e-01; uvwHexQuadI[3][2][8][2]=-7.745967e-01;
  uvwHexQuadI[3][2][1][0]= 1.000000e+00; uvwHexQuadI[3][2][1][1]=-7.745967e-01; uvwHexQuadI[3][2][1][2]= 0.;
  uvwHexQuadI[3][2][4][0]= 1.000000e+00; uvwHexQuadI[3][2][4][1]= 0.;           uvwHexQuadI[3][2][4][2]= 0.;
  uvwHexQuadI[3][2][7][0]= 1.000000e+00; uvwHexQuadI[3][2][7][1]= 7.745967e-01; uvwHexQuadI[3][2][7][2]= 0.;
  uvwHexQuadI[3][2][0][0]= 1.000000e+00; uvwHexQuadI[3][2][0][1]=-7.745967e-01; uvwHexQuadI[3][2][0][2]= 7.745967e-01;
  uvwHexQuadI[3][2][3][0]= 1.000000e+00; uvwHexQuadI[3][2][3][1]= 0.;           uvwHexQuadI[3][2][3][2]= 7.745967e-01;
  uvwHexQuadI[3][2][6][0]= 1.000000e+00; uvwHexQuadI[3][2][6][1]= 7.745967e-01; uvwHexQuadI[3][2][6][2]= 7.745967e-01;
  //
  uvwHexQuadI[3][3][8][0]= 1.000000e+00; uvwHexQuadI[3][3][8][1]=-7.745967e-01; uvwHexQuadI[3][3][8][2]=-7.745967e-01;
  uvwHexQuadI[3][3][7][0]= 1.000000e+00; uvwHexQuadI[3][3][7][1]= 0.;           uvwHexQuadI[3][3][7][2]=-7.745967e-01;
  uvwHexQuadI[3][3][6][0]= 1.000000e+00; uvwHexQuadI[3][3][6][1]= 7.745967e-01; uvwHexQuadI[3][3][6][2]=-7.745967e-01;
  uvwHexQuadI[3][3][5][0]= 1.000000e+00; uvwHexQuadI[3][3][5][1]=-7.745967e-01; uvwHexQuadI[3][3][5][2]= 0.;
  uvwHexQuadI[3][3][4][0]= 1.000000e+00; uvwHexQuadI[3][3][4][1]= 0.;           uvwHexQuadI[3][3][4][2]= 0.;
  uvwHexQuadI[3][3][3][0]= 1.000000e+00; uvwHexQuadI[3][3][3][1]= 7.745967e-01; uvwHexQuadI[3][3][3][2]= 0.;
  uvwHexQuadI[3][3][2][0]= 1.000000e+00; uvwHexQuadI[3][3][2][1]=-7.745967e-01; uvwHexQuadI[3][3][2][2]= 7.745967e-01;
  uvwHexQuadI[3][3][1][0]= 1.000000e+00; uvwHexQuadI[3][3][1][1]= 0.;           uvwHexQuadI[3][3][1][2]= 7.745967e-01;
  uvwHexQuadI[3][3][0][0]= 1.000000e+00; uvwHexQuadI[3][3][0][1]= 7.745967e-01; uvwHexQuadI[3][3][0][2]= 7.745967e-01;



  uvwHexQuadI[4][0][0][0]= 7.745967e-01; uvwHexQuadI[4][0][0][1]= 1.000000e+00; uvwHexQuadI[4][0][0][2]=-7.745967e-01;
  uvwHexQuadI[4][0][1][0]= 0.;           uvwHexQuadI[4][0][1][1]= 1.000000e+00; uvwHexQuadI[4][0][1][2]=-7.745967e-01;
  uvwHexQuadI[4][0][2][0]=-7.745967e-01; uvwHexQuadI[4][0][2][1]= 1.000000e+00; uvwHexQuadI[4][0][2][2]=-7.745967e-01;
  uvwHexQuadI[4][0][3][0]= 7.745967e-01; uvwHexQuadI[4][0][3][1]= 1.000000e+00; uvwHexQuadI[4][0][3][2]= 0.;
  uvwHexQuadI[4][0][4][0]= 0.;           uvwHexQuadI[4][0][4][1]= 1.000000e+00; uvwHexQuadI[4][0][4][2]= 0.;
  uvwHexQuadI[4][0][5][0]=-7.745967e-01; uvwHexQuadI[4][0][5][1]= 1.000000e+00; uvwHexQuadI[4][0][5][2]= 0.;
  uvwHexQuadI[4][0][6][0]= 7.745967e-01; uvwHexQuadI[4][0][6][1]= 1.000000e+00; uvwHexQuadI[4][0][6][2]= 7.745967e-01;
  uvwHexQuadI[4][0][7][0]= 0.;           uvwHexQuadI[4][0][7][1]= 1.000000e+00; uvwHexQuadI[4][0][7][2]= 7.745967e-01;
  uvwHexQuadI[4][0][8][0]=-7.745967e-01; uvwHexQuadI[4][0][8][1]= 1.000000e+00; uvwHexQuadI[4][0][8][2]= 7.745967e-01;
  //
  uvwHexQuadI[4][1][6][0]= 7.745967e-01; uvwHexQuadI[4][1][6][1]= 1.000000e+00; uvwHexQuadI[4][1][6][2]=-7.745967e-01;
  uvwHexQuadI[4][1][3][0]= 0.;           uvwHexQuadI[4][1][3][1]= 1.000000e+00; uvwHexQuadI[4][1][3][2]=-7.745967e-01;
  uvwHexQuadI[4][1][0][0]=-7.745967e-01; uvwHexQuadI[4][1][0][1]= 1.000000e+00; uvwHexQuadI[4][1][0][2]=-7.745967e-01;
  uvwHexQuadI[4][1][7][0]= 7.745967e-01; uvwHexQuadI[4][1][7][1]= 1.000000e+00; uvwHexQuadI[4][1][7][2]= 0.;
  uvwHexQuadI[4][1][4][0]= 0.;           uvwHexQuadI[4][1][4][1]= 1.000000e+00; uvwHexQuadI[4][1][4][2]= 0.;
  uvwHexQuadI[4][1][1][0]=-7.745967e-01; uvwHexQuadI[4][1][1][1]= 1.000000e+00; uvwHexQuadI[4][1][1][2]= 0.;
  uvwHexQuadI[4][1][8][0]= 7.745967e-01; uvwHexQuadI[4][1][8][1]= 1.000000e+00; uvwHexQuadI[4][1][8][2]= 7.745967e-01;
  uvwHexQuadI[4][1][5][0]= 0.;           uvwHexQuadI[4][1][5][1]= 1.000000e+00; uvwHexQuadI[4][1][5][2]= 7.745967e-01;
  uvwHexQuadI[4][1][2][0]=-7.745967e-01; uvwHexQuadI[4][1][2][1]= 1.000000e+00; uvwHexQuadI[4][1][2][2]= 7.745967e-01;
  //
  uvwHexQuadI[4][2][2][0]= 7.745967e-01; uvwHexQuadI[4][2][2][1]= 1.000000e+00; uvwHexQuadI[4][2][2][2]=-7.745967e-01;
  uvwHexQuadI[4][2][5][0]= 0.;           uvwHexQuadI[4][2][5][1]= 1.000000e+00; uvwHexQuadI[4][2][5][2]=-7.745967e-01;
  uvwHexQuadI[4][2][8][0]=-7.745967e-01; uvwHexQuadI[4][2][8][1]= 1.000000e+00; uvwHexQuadI[4][2][8][2]=-7.745967e-01;
  uvwHexQuadI[4][2][1][0]= 7.745967e-01; uvwHexQuadI[4][2][1][1]= 1.000000e+00; uvwHexQuadI[4][2][1][2]= 0.;
  uvwHexQuadI[4][2][4][0]= 0.;           uvwHexQuadI[4][2][4][1]= 1.000000e+00; uvwHexQuadI[4][2][4][2]= 0.;
  uvwHexQuadI[4][2][7][0]=-7.745967e-01; uvwHexQuadI[4][2][7][1]= 1.000000e+00; uvwHexQuadI[4][2][7][2]= 0.;
  uvwHexQuadI[4][2][0][0]= 7.745967e-01; uvwHexQuadI[4][2][0][1]= 1.000000e+00; uvwHexQuadI[4][2][0][2]= 7.745967e-01;
  uvwHexQuadI[4][2][3][0]= 0.;           uvwHexQuadI[4][2][3][1]= 1.000000e+00; uvwHexQuadI[4][2][3][2]= 7.745967e-01;
  uvwHexQuadI[4][2][6][0]=-7.745967e-01; uvwHexQuadI[4][2][6][1]= 1.000000e+00; uvwHexQuadI[4][2][6][2]= 7.745967e-01;
  //
  uvwHexQuadI[4][3][8][0]= 7.745967e-01; uvwHexQuadI[4][3][8][1]= 1.000000e+00; uvwHexQuadI[4][3][8][2]=-7.745967e-01;
  uvwHexQuadI[4][3][7][0]= 0.;           uvwHexQuadI[4][3][7][1]= 1.000000e+00; uvwHexQuadI[4][3][7][2]=-7.745967e-01;
  uvwHexQuadI[4][3][6][0]=-7.745967e-01; uvwHexQuadI[4][3][6][1]= 1.000000e+00; uvwHexQuadI[4][3][6][2]=-7.745967e-01;
  uvwHexQuadI[4][3][5][0]= 7.745967e-01; uvwHexQuadI[4][3][5][1]= 1.000000e+00; uvwHexQuadI[4][3][5][2]= 0.;
  uvwHexQuadI[4][3][4][0]= 0.;           uvwHexQuadI[4][3][4][1]= 1.000000e+00; uvwHexQuadI[4][3][4][2]= 0.;
  uvwHexQuadI[4][3][3][0]=-7.745967e-01; uvwHexQuadI[4][3][3][1]= 1.000000e+00; uvwHexQuadI[4][3][3][2]= 0.;
  uvwHexQuadI[4][3][2][0]= 7.745967e-01; uvwHexQuadI[4][3][2][1]= 1.000000e+00; uvwHexQuadI[4][3][2][2]= 7.745967e-01;
  uvwHexQuadI[4][3][1][0]= 0.;           uvwHexQuadI[4][3][1][1]= 1.000000e+00; uvwHexQuadI[4][3][1][2]= 7.745967e-01;
  uvwHexQuadI[4][3][0][0]=-7.745967e-01; uvwHexQuadI[4][3][0][1]= 1.000000e+00; uvwHexQuadI[4][3][0][2]= 7.745967e-01;


  uvwHexQuadI[5][0][0][0]=-7.745967e-01; uvwHexQuadI[5][0][0][1]=-7.745967e-01; uvwHexQuadI[5][0][0][2]= 1.000000e+00;
  uvwHexQuadI[5][0][1][0]= 0.;           uvwHexQuadI[5][0][1][1]=-7.745967e-01; uvwHexQuadI[5][0][1][2]= 1.000000e+00;
  uvwHexQuadI[5][0][2][0]= 7.745967e-01; uvwHexQuadI[5][0][2][1]=-7.745967e-01; uvwHexQuadI[5][0][2][2]= 1.000000e+00;
  uvwHexQuadI[5][0][3][0]=-7.745967e-01; uvwHexQuadI[5][0][3][1]= 0.;           uvwHexQuadI[5][0][3][2]= 1.000000e+00;
  uvwHexQuadI[5][0][4][0]= 0.;           uvwHexQuadI[5][0][4][1]= 0.;           uvwHexQuadI[5][0][4][2]= 1.000000e+00;
  uvwHexQuadI[5][0][5][0]= 7.745967e-01; uvwHexQuadI[5][0][5][1]= 0.;           uvwHexQuadI[5][0][5][2]= 1.000000e+00;
  uvwHexQuadI[5][0][6][0]=-7.745967e-01; uvwHexQuadI[5][0][6][1]= 7.745967e-01; uvwHexQuadI[5][0][6][2]= 1.000000e+00;
  uvwHexQuadI[5][0][7][0]= 0.;           uvwHexQuadI[5][0][7][1]= 7.745967e-01; uvwHexQuadI[5][0][7][2]= 1.000000e+00;
  uvwHexQuadI[5][0][8][0]= 7.745967e-01; uvwHexQuadI[5][0][8][1]= 7.745967e-01; uvwHexQuadI[5][0][8][2]= 1.000000e+00;
  //
  uvwHexQuadI[5][1][6][0]=-7.745967e-01; uvwHexQuadI[5][1][6][1]=-7.745967e-01; uvwHexQuadI[5][1][6][2]= 1.000000e+00;
  uvwHexQuadI[5][1][3][0]= 0.;           uvwHexQuadI[5][1][3][1]=-7.745967e-01; uvwHexQuadI[5][1][3][2]= 1.000000e+00;
  uvwHexQuadI[5][1][0][0]= 7.745967e-01; uvwHexQuadI[5][1][0][1]=-7.745967e-01; uvwHexQuadI[5][1][0][2]= 1.000000e+00;
  uvwHexQuadI[5][1][7][0]=-7.745967e-01; uvwHexQuadI[5][1][7][1]= 0.;           uvwHexQuadI[5][1][7][2]= 1.000000e+00;
  uvwHexQuadI[5][1][4][0]= 0.;           uvwHexQuadI[5][1][4][1]= 0.;           uvwHexQuadI[5][1][4][2]= 1.000000e+00;
  uvwHexQuadI[5][1][1][0]= 7.745967e-01; uvwHexQuadI[5][1][1][1]= 0.;           uvwHexQuadI[5][1][1][2]= 1.000000e+00;
  uvwHexQuadI[5][1][8][0]=-7.745967e-01; uvwHexQuadI[5][1][8][1]= 7.745967e-01; uvwHexQuadI[5][1][8][2]= 1.000000e+00;
  uvwHexQuadI[5][1][5][0]= 0.;           uvwHexQuadI[5][1][5][1]= 7.745967e-01; uvwHexQuadI[5][1][5][2]= 1.000000e+00;
  uvwHexQuadI[5][1][2][0]= 7.745967e-01; uvwHexQuadI[5][1][2][1]= 7.745967e-01; uvwHexQuadI[5][1][2][2]= 1.000000e+00;
  //
  uvwHexQuadI[5][2][2][0]=-7.745967e-01; uvwHexQuadI[5][2][2][1]=-7.745967e-01; uvwHexQuadI[5][2][2][2]= 1.000000e+00;
  uvwHexQuadI[5][2][5][0]= 0.;           uvwHexQuadI[5][2][5][1]=-7.745967e-01; uvwHexQuadI[5][2][5][2]= 1.000000e+00;
  uvwHexQuadI[5][2][8][0]= 7.745967e-01; uvwHexQuadI[5][2][8][1]=-7.745967e-01; uvwHexQuadI[5][2][8][2]= 1.000000e+00;
  uvwHexQuadI[5][2][1][0]=-7.745967e-01; uvwHexQuadI[5][2][1][1]= 0.;           uvwHexQuadI[5][2][1][2]= 1.000000e+00;
  uvwHexQuadI[5][2][4][0]= 0.;           uvwHexQuadI[5][2][4][1]= 0.;           uvwHexQuadI[5][2][4][2]= 1.000000e+00;
  uvwHexQuadI[5][2][7][0]= 7.745967e-01; uvwHexQuadI[5][2][7][1]= 0.;           uvwHexQuadI[5][2][7][2]= 1.000000e+00;
  uvwHexQuadI[5][2][0][0]=-7.745967e-01; uvwHexQuadI[5][2][0][1]= 7.745967e-01; uvwHexQuadI[5][2][0][2]= 1.000000e+00;
  uvwHexQuadI[5][2][3][0]= 0.;           uvwHexQuadI[5][2][3][1]= 7.745967e-01; uvwHexQuadI[5][2][3][2]= 1.000000e+00;
  uvwHexQuadI[5][2][6][0]= 7.745967e-01; uvwHexQuadI[5][2][6][1]= 7.745967e-01; uvwHexQuadI[5][2][6][2]= 1.000000e+00;
  //
  uvwHexQuadI[5][3][8][0]=-7.745967e-01; uvwHexQuadI[5][3][8][1]=-7.745967e-01; uvwHexQuadI[5][3][8][2]= 1.000000e+00;
  uvwHexQuadI[5][3][7][0]= 0.;           uvwHexQuadI[5][3][7][1]=-7.745967e-01; uvwHexQuadI[5][3][7][2]= 1.000000e+00;
  uvwHexQuadI[5][3][6][0]= 7.745967e-01; uvwHexQuadI[5][3][6][1]=-7.745967e-01; uvwHexQuadI[5][3][6][2]= 1.000000e+00;
  uvwHexQuadI[5][3][5][0]=-7.745967e-01; uvwHexQuadI[5][3][5][1]= 0.;           uvwHexQuadI[5][3][5][2]= 1.000000e+00;
  uvwHexQuadI[5][3][4][0]= 0.;           uvwHexQuadI[5][3][4][1]= 0.;           uvwHexQuadI[5][3][4][2]= 1.000000e+00;
  uvwHexQuadI[5][3][3][0]= 7.745967e-01; uvwHexQuadI[5][3][3][1]= 0.;           uvwHexQuadI[5][3][3][2]= 1.000000e+00;
  uvwHexQuadI[5][3][2][0]=-7.745967e-01; uvwHexQuadI[5][3][2][1]= 7.745967e-01; uvwHexQuadI[5][3][2][2]= 1.000000e+00;
  uvwHexQuadI[5][3][1][0]= 0.;           uvwHexQuadI[5][3][1][1]= 7.745967e-01; uvwHexQuadI[5][3][1][2]= 1.000000e+00;
  uvwHexQuadI[5][3][0][0]= 7.745967e-01; uvwHexQuadI[5][3][0][1]= 7.745967e-01; uvwHexQuadI[5][3][0][2]= 1.000000e+00;


}

int interface2DQuadrature::getIntPoints(MInterfaceElement* iele,IntPt *GPs, IntPt **GPm, IntPt **GPp)
{
  // minus element
  int npts;
  if(iele->getElem(0)->getNumPrimaryVertices()==4){ // tetra
    int index = ((iele->getPermutation(0))*4+iele->getEdgeOrFaceNumber(0))*_nptst;
    iele->isSameDirection(0) ? *GPm = &_GPt[index] : *GPm = &_GPtI[index];
    npts = _nptst;
  }
  else{ // hexa
    int index = ((iele->getPermutation(0))*6+iele->getEdgeOrFaceNumber(0))*_nptsh;
    iele->isSameDirection(0) ? *GPm = &_GPh[index] : *GPm = &_GPhI[index];
    npts = _nptsh;
  }
  // see if the points are already computed or not
  if((*GPm)[0].weight==0.)
  {
    //Msg::Error("Minus element, dir :  %d, permutation : %d, face : %d",  iele->isSameDirection(0), iele->getPermutation(0), iele->getEdgeOrFaceNumber(0));
    for(int i=0;i<npts;i++)
    {
      if(npts == 6 and iele->getElem(0)->getNumPrimaryVertices()==4)
      {
        if(iele->isSameDirection(0) == 0)
        {
          (*GPm)[i].pt[0] =uvwTetQuadI[iele->getEdgeOrFaceNumber(0)][iele->getPermutation(0)][i][0];
          (*GPm)[i].pt[1] =uvwTetQuadI[iele->getEdgeOrFaceNumber(0)][iele->getPermutation(0)][i][1];
          (*GPm)[i].pt[2] =uvwTetQuadI[iele->getEdgeOrFaceNumber(0)][iele->getPermutation(0)][i][2];
        }
        else
        {
          (*GPm)[i].pt[0] =uvwTetQuad[iele->getEdgeOrFaceNumber(0)][iele->getPermutation(0)][i][0];
          (*GPm)[i].pt[1] =uvwTetQuad[iele->getEdgeOrFaceNumber(0)][iele->getPermutation(0)][i][1];
          (*GPm)[i].pt[2] =uvwTetQuad[iele->getEdgeOrFaceNumber(0)][iele->getPermutation(0)][i][2];
        }
        double um,vm,wm,up,vp,wp;
        iele->getuvwOnElem(GPs[i].pt[0],GPs[i].pt[1],um,vm,wm,up,vp,wp);
        if(fabs(um-(*GPm)[i].pt[0])>0.1 or fabs(vm-(*GPm)[i].pt[1])>0.1 or fabs(wm-(*GPm)[i].pt[2])>0.1)
        {
          Msg::Error("interfaceQuadrature: Gauss points not stored properly for dir %d, face %d, permutation %d",
                     iele->isSameDirection(0), iele->getEdgeOrFaceNumber(0), iele->getPermutation(0));
          Msg::Error(" i %d, u % f v % f, w %f, cu %f, cv %f, cw %f",i, (*GPm)[i].pt[0], (*GPm)[i].pt[1], (*GPm)[i].pt[2], um, vm, wm);
        }
      }
      else if(npts == 4 and iele->getElem(0)->getNumPrimaryVertices()==8)
      {
        if(iele->isSameDirection(0) == 0)
        {
          (*GPm)[i].pt[0] =uvwHexLinI[iele->getEdgeOrFaceNumber(0)][iele->getPermutation(0)][i][0];
          (*GPm)[i].pt[1] =uvwHexLinI[iele->getEdgeOrFaceNumber(0)][iele->getPermutation(0)][i][1];
          (*GPm)[i].pt[2] =uvwHexLinI[iele->getEdgeOrFaceNumber(0)][iele->getPermutation(0)][i][2];
        }
        else
        {
          (*GPm)[i].pt[0] =uvwHexLin[iele->getEdgeOrFaceNumber(0)][iele->getPermutation(0)][i][0];
          (*GPm)[i].pt[1] =uvwHexLin[iele->getEdgeOrFaceNumber(0)][iele->getPermutation(0)][i][1];
          (*GPm)[i].pt[2] =uvwHexLin[iele->getEdgeOrFaceNumber(0)][iele->getPermutation(0)][i][2];
        }
        double um,vm,wm,up,vp,wp;
        iele->getuvwOnElem(GPs[i].pt[0],GPs[i].pt[1],um,vm,wm,up,vp,wp);
        if(fabs(um-(*GPm)[i].pt[0])>0.1 or fabs(vm-(*GPm)[i].pt[1])>0.1 or fabs(wm-(*GPm)[i].pt[2])>0.1)
	{
          Msg::Error("interfaceQuadrature: Gauss points not stored properly for dir %d, face %d, permutation %d",
                     iele->isSameDirection(0), iele->getEdgeOrFaceNumber(0), iele->getPermutation(0));
           Msg::Error(" i %d, u % f v % f, w %f, cu %f, cv %f, cw %f",i, (*GPm)[i].pt[0], (*GPm)[i].pt[1], (*GPm)[i].pt[2], um, vm, wm);
	}
      }
      else if(npts == 9 and iele->getElem(0)->getNumPrimaryVertices()==8)
      {
        if(iele->isSameDirection(0) == 0)
        {
          (*GPm)[i].pt[0] =uvwHexQuadI[iele->getEdgeOrFaceNumber(0)][iele->getPermutation(0)][i][0];
          (*GPm)[i].pt[1] =uvwHexQuadI[iele->getEdgeOrFaceNumber(0)][iele->getPermutation(0)][i][1];
          (*GPm)[i].pt[2] =uvwHexQuadI[iele->getEdgeOrFaceNumber(0)][iele->getPermutation(0)][i][2];
        }
        else
        {
          (*GPm)[i].pt[0] =uvwHexQuad[iele->getEdgeOrFaceNumber(0)][iele->getPermutation(0)][i][0];
          (*GPm)[i].pt[1] =uvwHexQuad[iele->getEdgeOrFaceNumber(0)][iele->getPermutation(0)][i][1];
          (*GPm)[i].pt[2] =uvwHexQuad[iele->getEdgeOrFaceNumber(0)][iele->getPermutation(0)][i][2];
        }
        double um,vm,wm,up,vp,wp;
        iele->getuvwOnElem(GPs[i].pt[0],GPs[i].pt[1],um,vm,wm,up,vp,wp);
        if(fabs(um-(*GPm)[i].pt[0])>0.1 or fabs(vm-(*GPm)[i].pt[1])>0.1 or fabs(wm-(*GPm)[i].pt[2])>0.1){
          Msg::Error("interfaceQuadrature: Gauss points not stored properly for dir %d, face %d, permutation %d",
                     iele->isSameDirection(0), iele->getEdgeOrFaceNumber(0), iele->getPermutation(0));
          Msg::Error(" i %d, u % f v % f, w %f, cu %f, cv %f, cw %f",i, (*GPm)[i].pt[0], (*GPm)[i].pt[1], (*GPm)[i].pt[2], um, vm, wm);
        }
      }
      else
      {
            //Msg::Error("interfaceQuadrature: Gauss points not stored for this element, use approximation");

            //Msg::Error("Minus stored values u %e v %e w %e : ",(*GPm)[i].pt[0], (*GPm)[i].pt[1],(*GPm)[i].pt[2]);
	    double up,vp,wp;
	    iele->getuvwOnElem(GPs[i].pt[0],GPs[i].pt[1],(*GPm)[i].pt[0],(*GPm)[i].pt[1],(*GPm)[i].pt[2],up,vp,wp);
	    //if (iele->isSameDirection(0)==0)
	      //Msg::Error("uvwHexQuadI[%d][%d][%d][0]=%e; uvwHexQuadI[%d][%d][%d][1]=%e; uvwHexQuadI[%d][%d][%d][2]=%e;",iele->getEdgeOrFaceNumber(0),iele->getPermutation(0), i, (*GPm)[i].pt[0],iele->getEdgeOrFaceNumber(0),iele->getPermutation(0), i, (*GPm)[i].pt[1],iele->getEdgeOrFaceNumber(0),iele->getPermutation(0), i, (*GPm)[i].pt[2]);
	    //else
	      //Msg::Error("uvwHexQuad[%d][%d][%d][0]=%e; uvwHexQuad[%d][%d][%d][1]=%e; uvwHexQuad[%d][%d][%d][2]=%e;",iele->getEdgeOrFaceNumber(0),iele->getPermutation(0), i, (*GPm)[i].pt[0],iele->getEdgeOrFaceNumber(0),iele->getPermutation(0), i, (*GPm)[i].pt[1],iele->getEdgeOrFaceNumber(0),iele->getPermutation(0), i, (*GPm)[i].pt[2]);
      }

      (*GPm)[i].weight = GPs[i].weight;
    }
  }
//  Msg::Error("dir %d %d\n",iele->isSameDirection(0),iele->isSameDirection(1));
//  Msg::Error("face %d %d\n",iele->getEdgeOrFaceNumber(0),iele->getEdgeOrFaceNumber(1));
  // plus element
  if(iele->getElem(1)->getNumPrimaryVertices()==4){ // tetra
    int index = ((iele->getPermutation(1))*4+iele->getEdgeOrFaceNumber(1))*_nptst;
    iele->isSameDirection(1) ? *GPp = &_GPt[index] : *GPp = &_GPtI[index];
    npts = _nptst;
  }
  else{ // hexa
    int index = ((iele->getPermutation(1))*6+iele->getEdgeOrFaceNumber(1))*_nptsh;
    iele->isSameDirection(1) ? *GPp = &_GPh[index] : *GPp = &_GPhI[index];
    npts = _nptsh;
  }
  // see if the points are already computed or not
  if((*GPp)[0].weight==0.)
  {
    //Msg::Error("Plus element, dir :  %d, permutation : %d, face : %d",  iele->isSameDirection(1), iele->getPermutation(1), iele->getEdgeOrFaceNumber(1));
    for(int i=0;i<npts;i++)
    {
      if(npts == 6 and iele->getElem(1)->getNumPrimaryVertices()==4)
      {
        if(iele->isSameDirection(1) == 0)
        {
          (*GPp)[i].pt[0] =uvwTetQuadI[iele->getEdgeOrFaceNumber(1)][iele->getPermutation(1)][i][0];
          (*GPp)[i].pt[1] =uvwTetQuadI[iele->getEdgeOrFaceNumber(1)][iele->getPermutation(1)][i][1];
          (*GPp)[i].pt[2] =uvwTetQuadI[iele->getEdgeOrFaceNumber(1)][iele->getPermutation(1)][i][2];
        }
        else
        {
          (*GPp)[i].pt[0] =uvwTetQuad[iele->getEdgeOrFaceNumber(1)][iele->getPermutation(1)][i][0];
          (*GPp)[i].pt[1] =uvwTetQuad[iele->getEdgeOrFaceNumber(1)][iele->getPermutation(1)][i][1];
          (*GPp)[i].pt[2] =uvwTetQuad[iele->getEdgeOrFaceNumber(1)][iele->getPermutation(1)][i][2];
        }
        double um,vm,wm,up,vp,wp;
        iele->getuvwOnElem(GPs[i].pt[0],GPs[i].pt[1],um,vm,wm,up,vp,wp);
              if(fabs(up-(*GPp)[i].pt[0])>0.1 or fabs(vp-(*GPp)[i].pt[1])>0.1 or fabs(wp-(*GPp)[i].pt[2])>0.1)
        {
          Msg::Error("interfaceQuadrature: Gauss points not stored properly for dir %d, face %d, permutation %d",
                     iele->isSameDirection(1), iele->getEdgeOrFaceNumber(1), iele->getPermutation(1));
          Msg::Error(" i %d, u % f v % f, w %f, cu %f, cv %f, cw %f",i, (*GPp)[i].pt[0], (*GPp)[i].pt[1], (*GPp)[i].pt[2], up, vp, wp);
        }
      }
      else if(npts == 4 and iele->getElem(1)->getNumPrimaryVertices()==8)
      {
        if(iele->isSameDirection(1) == 0)
        {
          (*GPp)[i].pt[0] =uvwHexLinI[iele->getEdgeOrFaceNumber(1)][iele->getPermutation(1)][i][0];
          (*GPp)[i].pt[1] =uvwHexLinI[iele->getEdgeOrFaceNumber(1)][iele->getPermutation(1)][i][1];
          (*GPp)[i].pt[2] =uvwHexLinI[iele->getEdgeOrFaceNumber(1)][iele->getPermutation(1)][i][2];
        }
        else
        {
          (*GPp)[i].pt[0] =uvwHexLin[iele->getEdgeOrFaceNumber(1)][iele->getPermutation(1)][i][0];
          (*GPp)[i].pt[1] =uvwHexLin[iele->getEdgeOrFaceNumber(1)][iele->getPermutation(1)][i][1];
          (*GPp)[i].pt[2] =uvwHexLin[iele->getEdgeOrFaceNumber(1)][iele->getPermutation(1)][i][2];
        }
        double um,vm,wm,up,vp,wp;
        iele->getuvwOnElem(GPs[i].pt[0],GPs[i].pt[1],um,vm,wm,up,vp,wp);
        if(fabs(up-(*GPp)[i].pt[0])>0.1 or fabs(vp-(*GPp)[i].pt[1])>0.1 or fabs(wp-(*GPp)[i].pt[2])>0.1){
          Msg::Error("interfaceQuadrature: Gauss points not stored properly for dir %d, face %d, permutation %d",
                     iele->isSameDirection(1), iele->getEdgeOrFaceNumber(1), iele->getPermutation(1));
          Msg::Error(" i %d, u % f v % f, w %f, cu %f, cv %f, cw %f",i, (*GPp)[i].pt[0], (*GPp)[i].pt[1], (*GPp)[i].pt[2], up, vp, wp);
        }

      }
      else if(npts == 9 and iele->getElem(1)->getNumPrimaryVertices()==8)
      {
        if(iele->isSameDirection(1) == 0)
        {
          (*GPp)[i].pt[0] =uvwHexQuadI[iele->getEdgeOrFaceNumber(1)][iele->getPermutation(1)][i][0];
          (*GPp)[i].pt[1] =uvwHexQuadI[iele->getEdgeOrFaceNumber(1)][iele->getPermutation(1)][i][1];
          (*GPp)[i].pt[2] =uvwHexQuadI[iele->getEdgeOrFaceNumber(1)][iele->getPermutation(1)][i][2];
        }
        else
        {
          (*GPp)[i].pt[0] =uvwHexQuad[iele->getEdgeOrFaceNumber(1)][iele->getPermutation(1)][i][0];
          (*GPp)[i].pt[1] =uvwHexQuad[iele->getEdgeOrFaceNumber(1)][iele->getPermutation(1)][i][1];
          (*GPp)[i].pt[2] =uvwHexQuad[iele->getEdgeOrFaceNumber(1)][iele->getPermutation(1)][i][2];
        }
        double um,vm,wm,up,vp,wp;
        iele->getuvwOnElem(GPs[i].pt[0],GPs[i].pt[1],um,vm,wm,up,vp,wp);
        if(fabs(up-(*GPp)[i].pt[0])>0.1 or fabs(vp-(*GPp)[i].pt[1])>0.1 or fabs(wp-(*GPp)[i].pt[2])>0.1){
          Msg::Error("interfaceQuadrature: Gauss points not stored properly for dir %d, face %d, permutation %d",
                     iele->isSameDirection(1), iele->getEdgeOrFaceNumber(1), iele->getPermutation(1));
          Msg::Error(" i %d, u % f v % f, w %f, cu %f, cv %f, cw %f",i, (*GPp)[i].pt[0], (*GPp)[i].pt[1], (*GPp)[i].pt[2], up, vp, wp);
        }
      }
      else
      {
	//Msg::Error("Plus stored values u %e v %e w %e : ",(*GPp)[i].pt[0], (*GPp)[i].pt[1],(*GPp)[i].pt[2]);
        //Msg::Error("interfaceQuadrature: Gauss points not stored for this element, use approximation");
        double um,vm,wm;
        iele->getuvwOnElem(GPs[i].pt[0],GPs[i].pt[1],um,vm,wm,(*GPp)[i].pt[0],(*GPp)[i].pt[1],(*GPp)[i].pt[2]);
        //if (iele->isSameDirection(1)==0)
          //Msg::Error("uvwHexQuadI[%d][%d][%d][0]=%e; uvwHexQuadI[%d][%d][%d][1]=%e; uvwHexQuadI[%d][%d][%d][2]=%e;",iele->getEdgeOrFaceNumber(1),iele->getPermutation(1), i, (*GPp)[i].pt[0],iele->getEdgeOrFaceNumber(1),iele->getPermutation(1), i, (*GPp)[i].pt[1],iele->getEdgeOrFaceNumber(1),iele->getPermutation(1), i, (*GPp)[i].pt[2]);
        //else
          //Msg::Error("uvwHexQuad[%d][%d][%d][0]=%e; uvwHexQuad[%d][%d][%d][1]=%e; uvwHexQuad[%d][%d][%d][2]=%e;",iele->getEdgeOrFaceNumber(1),iele->getPermutation(1), i, (*GPp)[i].pt[0],iele->getEdgeOrFaceNumber(1),iele->getPermutation(1), i, (*GPp)[i].pt[1],iele->getEdgeOrFaceNumber(1),iele->getPermutation(1), i, (*GPp)[i].pt[2]);
      }
      (*GPp)[i].weight = GPs[i].weight;
    }
  }
  return npts; // wrong if 1 tetra and 1 hexa fix this but it is never used and it is impossible (interface with 1 tri can't match a quadrangle)
}

interface2DQuadrature::~interface2DQuadrature()
{
  if(_GPt!=NULL) delete[] _GPt; _GPt=NULL;
  if(_GPtI!=NULL) delete[] _GPtI; _GPtI=NULL;
  if(_GPh!=NULL) delete[] _GPh; _GPh=NULL;
  if(_GPhI!=NULL) delete[] _GPhI; _GPhI=NULL;

}
