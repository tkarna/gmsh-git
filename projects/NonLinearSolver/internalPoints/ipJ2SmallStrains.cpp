//
// C++ Interface: internal variable
//
// Description: ipvariable associated to mlawJ2SmallStrains
//
//
// Author:  <Gauthier BECKER>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipJ2SmallStrains.h"
ipJ2SmallStrains::ipJ2SmallStrains() : IPVariableMechanics(), _H(0.), _epsilon(0.), _sigma(0.), _sigmaeff(0.), _effectivePlasticStrain(0.),
                                         //_caracLmatrix(0.), _couplingStressEffectiveStrain(0.), _couplingEffectiveStrainStress(0.),
                                         _dpdE(0.), _plasticStrain(0.), _muiso(0.), _d2muiso(0.){}

ipJ2SmallStrains::ipJ2SmallStrains(const ipJ2SmallStrains &source) : IPVariableMechanics(source), _H(source._H),
                                                                     //_couplingStressEffectiveStrain(source._couplingStressEffectiveStrain),
                                                                     //_couplingEffectiveStrainStress(source._couplingEffectiveStrainStress),
                                                                     //_caracLmatrix(source._caracLmatrix),
                                                                     _epsilon(source._epsilon),
                                                                     _sigma(source._sigma), _sigmaeff(source._sigmaeff),
                                                                     _effectivePlasticStrain(source._effectivePlasticStrain),
                                                                     _plasticStrain(source._plasticStrain), _dpdE(source._dpdE),
                                                                     _muiso(source._muiso), _d2muiso(source._d2muiso){}

ipJ2SmallStrains& ipJ2SmallStrains::operator=(const IPVariable &source)
{
  IPVariableMechanics::operator=(source);
  const ipJ2SmallStrains* src = static_cast<const ipJ2SmallStrains*>(&source);
  _H = src->_H;
//  _couplingStressEffectiveStrain = src->_couplingStressEffectiveStrain;
//  _couplingEffectiveStrainStress = src->_couplingEffectiveStrainStress;
//  _caracLmatrix = src->_caracLmatrix;
  _epsilon = src->_epsilon;
  _sigma = src->_sigma;
  _sigmaeff = src->_sigmaeff;
  _effectivePlasticStrain = src->_effectivePlasticStrain;
  _plasticStrain = src->_plasticStrain;
  _dpdE = src->_dpdE;
  _muiso = src->_muiso;
  _d2muiso = src->_d2muiso;
}
