//
// Description: storing class for j2 linear elasto-plastic law
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipJ2linear.h"
IPJ2linear::IPJ2linear() : IPVariableMechanics(), _j2lepspbarre(0.), _j2lepsp(1.), _j2ldsy(0.),  _elasticEnergy(0.)
{
  ipvJ2IsotropicHardening=NULL;
  Msg::Error("IPJ2Linear::IPJ2Linear is not initialized with a hardening IP Variable");

};
IPJ2linear::IPJ2linear(const J2IsotropicHardening *j2IH) : IPVariableMechanics(), _j2lepspbarre(0.), _j2lepsp(1.), _j2ldsy(0.), _elasticEnergy(0.)
{
  ipvJ2IsotropicHardening=NULL;
  if(j2IH ==NULL) Msg::Error("IPJ2Linear::IPJ2Linear has no j2IH");
  j2IH->createIPVariable(ipvJ2IsotropicHardening);

};
IPJ2linear::IPJ2linear(const IPJ2linear &source) : IPVariableMechanics(source), _j2lepspbarre(source._j2lepspbarre),
                                                   _j2lepsp(source._j2lepsp), _j2ldsy(source._j2ldsy),  					           _elasticEnergy(source._elasticEnergy)
{
  if(source.ipvJ2IsotropicHardening != NULL)
  {
    ipvJ2IsotropicHardening = source.ipvJ2IsotropicHardening->clone();
  }
}
IPJ2linear& IPJ2linear::operator=(const IPVariable &source)
{
  IPVariableMechanics::operator=(source);
  const IPJ2linear* src = static_cast<const IPJ2linear*>(&source);
  if(src!=NULL)
  {
    _j2lepspbarre = src->_j2lepspbarre;
    _j2lepsp = src->_j2lepsp;
    _j2ldsy = src->_j2ldsy;
    _elasticEnergy = src->_elasticEnergy;
    if(ipvJ2IsotropicHardening != NULL) delete ipvJ2IsotropicHardening;
    if(src->ipvJ2IsotropicHardening != NULL)
    {
      ipvJ2IsotropicHardening=src->ipvJ2IsotropicHardening->clone();
    }
  }
  return *this;
}

double IPJ2linear::defoEnergy() const
{
  return _elasticEnergy;

}
double IPJ2linear::plasticEnergy() const
{
  if(ipvJ2IsotropicHardening != NULL)
    return ipvJ2IsotropicHardening->getIntegR();
  else
    return 0.;
}

void IPJ2linear::createRestart(FILE *fp)
{
  IPVariableMechanics::createRestart(fp);
  fprintf(fp,"%e %e %e %e %e %e %e %e %e %e %e %e",_j2lepspbarre,_j2lepsp[0],_j2lepsp[1],_j2lepsp[2],_j2lepsp[3],_j2lepsp[4],
                                                 _j2lepsp[5],_j2lepsp[6],_j2lepsp[7],_j2lepsp[8],_j2ldsy,_elasticEnergy);
  if(ipvJ2IsotropicHardening != NULL)
  {
    fprintf(fp,"%d",1);
    ipvJ2IsotropicHardening->createRestart(fp);
  }
  else
    fprintf(fp,"%d",0);

}
void IPJ2linear::setFromRestart(FILE *fp)
{
  IPVariableMechanics::setFromRestart(fp);
  double j2eba,j2lepsp0,j2lepsp1,j2lepsp2,j2lepsp3,j2lepsp4,j2lepsp5,j2lepsp6,j2lepsp7,j2lepsp8,j2ldsy,elasticEnergy;
  fscanf(fp,"%e %e %e %e %e %e %e %e %e %e %e %e",&j2eba,&j2lepsp0,&j2lepsp1,&j2lepsp2,&j2lepsp3,&j2lepsp4,
                                               &j2lepsp5,&j2lepsp6,&j2lepsp7,&j2lepsp8,&j2ldsy,&elasticEnergy);
  _j2lepspbarre = j2eba;
  _j2lepsp[0] = j2lepsp0;
  _j2lepsp[1] = j2lepsp1;
  _j2lepsp[2] = j2lepsp2;
  _j2lepsp[3] = j2lepsp3;
  _j2lepsp[4] = j2lepsp4;
  _j2lepsp[5] = j2lepsp5;
  _j2lepsp[6] = j2lepsp6;
  _j2lepsp[7] = j2lepsp7;
  _j2lepsp[8] = j2lepsp8;
  _j2ldsy = j2ldsy;
  _elasticEnergy = elasticEnergy;
  int iph=0;
  fscanf(fp,"%d",&iph);
  if(iph == 1)
  {
    if(ipvJ2IsotropicHardening == NULL) Msg::Error("IPJ2linear::setFromRestart: ipvJ2IsotyropicHardening should be initialized");
    ipvJ2IsotropicHardening->setFromRestart(fp);
  }
}
