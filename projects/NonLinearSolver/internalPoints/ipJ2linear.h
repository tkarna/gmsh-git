//
// Description: storing class for j2 linear elasto-plastic law it is a "virtual implementation" you have to define an ipvariable
//              in your project which derive from this one. (all data in this ipvarible have name beggining by _j2l...)
//              so don't do the same in your project...
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef IPJ2LINEAR_H_
#define IPJ2LINEAR_H_
#include "ipvariable.h"
#include "STensor3.h"
#include "ipHardening.h"
#include "j2IsotropicHardening.h"

class IPJ2linear : public IPVariableMechanics
{
 protected:
  IPJ2IsotropicHardening* ipvJ2IsotropicHardening;

 public: // All data public to avoid the creation of function to access
  double _j2lepspbarre; // equivalent plastic strain
  STensor3 _j2lepsp;    // plastic part of the deformation gradient
  double _j2ldsy;       // yield stress increment
  double _elasticEnergy; // elastic energy stored
 public:
  IPJ2linear();
  IPJ2linear(const J2IsotropicHardening *j2IH);
  IPJ2linear(const IPJ2linear &source);
  IPJ2linear& operator=(const IPVariable &source);
  ~IPJ2linear()
  {
    if(ipvJ2IsotropicHardening != NULL) 
    {
      delete ipvJ2IsotropicHardening;
    }
  }
  virtual double defoEnergy() const;
  virtual double plasticEnergy() const;
  virtual void createRestart(FILE *fp); // default don't save values for a restart
  virtual void setFromRestart(FILE *fp);
  virtual const IPJ2IsotropicHardening &getConstRefToIPJ2IsotropicHardening() const 
  {
    if(ipvJ2IsotropicHardening==NULL)
      Msg::Error("IPJ2linear: ipvJ2IsotropicHardening not initialized");
    return *ipvJ2IsotropicHardening;
  }
  virtual IPJ2IsotropicHardening &getRefToIPJ2IsotropicHardening()
  {
    if(ipvJ2IsotropicHardening==NULL)
      Msg::Error("IPJ2linear: ipvJ2IsotropicHardening not initialized");
    return *ipvJ2IsotropicHardening;
  }
  virtual const STensor3 & getConstRefToFp() const
  {
    return _j2lepsp;
  }
};

#endif // IPJ2LINEAR_H_

