//
// Description: storing class for hardening law
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipHardening.h"

IPHardening::IPHardening(): IPVariable()
{
}

IPHardening::IPHardening(const IPHardening &source) : IPVariable(source)
{

}

IPHardening &IPHardening::operator=(const IPVariable &source)
{
  IPVariable::operator=(source);
  return *this;
}

IPJ2IsotropicHardening::IPJ2IsotropicHardening(): IPHardening(), R(0), dR(0), ddR(0), integR(0)
{

}

IPJ2IsotropicHardening::IPJ2IsotropicHardening(const IPJ2IsotropicHardening &source) : IPHardening(source)
{
  R      = source.R;
  dR     = source.dR;
  ddR    = source.ddR;
  integR = source.integR;
}

IPJ2IsotropicHardening &IPJ2IsotropicHardening::operator=(const IPVariable &source)
{
  IPHardening::operator=(source);
  const IPJ2IsotropicHardening* src = dynamic_cast<const IPJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
    R      = src->R;
    dR     = src->dR;
    ddR    = src->ddR;
    integR = src->integR;
  }
  return *this;
}

void IPJ2IsotropicHardening::createRestart(FILE *fp)
{
  IPHardening::createRestart(fp);
  fprintf(fp,"%e %e %e",R, dR, ddR, integR);
}

void IPJ2IsotropicHardening::setFromRestart(FILE *fp)
{
  IPHardening::setFromRestart(fp);
  double a0,a1,a2,a3;
  fscanf(fp,"%e %e %e",&a0,&a1,&a2,&a3);
  R      = a0;
  dR     = a1;
  ddR    = a2;
  integR = a3;
}

IPPerfectlyPlasticJ2IsotropicHardening::IPPerfectlyPlasticJ2IsotropicHardening(): IPJ2IsotropicHardening()
{

}

IPPerfectlyPlasticJ2IsotropicHardening::IPPerfectlyPlasticJ2IsotropicHardening(const IPPerfectlyPlasticJ2IsotropicHardening &source) : IPJ2IsotropicHardening(source)
{
}

IPPerfectlyPlasticJ2IsotropicHardening &IPPerfectlyPlasticJ2IsotropicHardening::operator=(const IPVariable &source)
{
  IPJ2IsotropicHardening::operator=(source);
  const IPPerfectlyPlasticJ2IsotropicHardening* src = dynamic_cast<const IPPerfectlyPlasticJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
  }
  return *this;
}

IPJ2IsotropicHardening * IPPerfectlyPlasticJ2IsotropicHardening::clone() const
{
  
  return new IPPerfectlyPlasticJ2IsotropicHardening(*this);
} 

void IPPerfectlyPlasticJ2IsotropicHardening::createRestart(FILE *fp)
{
  IPJ2IsotropicHardening::createRestart(fp);
}

void IPPerfectlyPlasticJ2IsotropicHardening::setFromRestart(FILE *fp)
{
  IPJ2IsotropicHardening::setFromRestart(fp);
}

IPPowerLawJ2IsotropicHardening::IPPowerLawJ2IsotropicHardening(): IPJ2IsotropicHardening()
{

}

IPPowerLawJ2IsotropicHardening::IPPowerLawJ2IsotropicHardening(const IPPowerLawJ2IsotropicHardening &source) : IPJ2IsotropicHardening(source)
{
}

IPPowerLawJ2IsotropicHardening &IPPowerLawJ2IsotropicHardening::operator=(const IPVariable &source)
{
  IPJ2IsotropicHardening::operator=(source);
  const IPPowerLawJ2IsotropicHardening* src = dynamic_cast<const IPPowerLawJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
  }
  return *this;
}

void IPPowerLawJ2IsotropicHardening::createRestart(FILE *fp)
{
  IPJ2IsotropicHardening::createRestart(fp);
}

void IPPowerLawJ2IsotropicHardening::setFromRestart(FILE *fp)
{
  IPJ2IsotropicHardening::setFromRestart(fp);
}
IPJ2IsotropicHardening * IPPowerLawJ2IsotropicHardening::clone() const
{
  return new IPPowerLawJ2IsotropicHardening(*this);
} 


IPExponentialJ2IsotropicHardening::IPExponentialJ2IsotropicHardening(): IPJ2IsotropicHardening()
{

}

IPExponentialJ2IsotropicHardening::IPExponentialJ2IsotropicHardening(const IPExponentialJ2IsotropicHardening &source) : IPJ2IsotropicHardening(source)
{
}

IPExponentialJ2IsotropicHardening &IPExponentialJ2IsotropicHardening::operator=(const IPVariable &source)
{
  IPJ2IsotropicHardening::operator=(source);
  const IPExponentialJ2IsotropicHardening* src = dynamic_cast<const IPExponentialJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
  }
  return *this;
}

void IPExponentialJ2IsotropicHardening::createRestart(FILE *fp)
{
  IPJ2IsotropicHardening::createRestart(fp);
}

void IPExponentialJ2IsotropicHardening::setFromRestart(FILE *fp)
{
  IPJ2IsotropicHardening::setFromRestart(fp);
}
IPJ2IsotropicHardening * IPExponentialJ2IsotropicHardening::clone() const
{
 return new IPExponentialJ2IsotropicHardening(*this);
} 


IPSwiftJ2IsotropicHardening::IPSwiftJ2IsotropicHardening(): IPJ2IsotropicHardening()
{

}

IPSwiftJ2IsotropicHardening::IPSwiftJ2IsotropicHardening(const IPSwiftJ2IsotropicHardening &source) : IPJ2IsotropicHardening(source)
{
}

IPSwiftJ2IsotropicHardening &IPSwiftJ2IsotropicHardening::operator=(const IPVariable &source)
{
  IPJ2IsotropicHardening::operator=(source);
  const IPSwiftJ2IsotropicHardening* src = dynamic_cast<const IPSwiftJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
  }
  return *this;
}

void IPSwiftJ2IsotropicHardening::createRestart(FILE *fp)
{
  IPJ2IsotropicHardening::createRestart(fp);
}

void IPSwiftJ2IsotropicHardening::setFromRestart(FILE *fp)
{
  IPJ2IsotropicHardening::setFromRestart(fp);
}

IPJ2IsotropicHardening * IPSwiftJ2IsotropicHardening::clone() const
{
  return new IPSwiftJ2IsotropicHardening(*this);
} 

IPLinearExponentialJ2IsotropicHardening::IPLinearExponentialJ2IsotropicHardening(): IPJ2IsotropicHardening()
{

}

IPLinearExponentialJ2IsotropicHardening::IPLinearExponentialJ2IsotropicHardening(const IPLinearExponentialJ2IsotropicHardening &source) : IPJ2IsotropicHardening(source)
{
}

IPLinearExponentialJ2IsotropicHardening &IPLinearExponentialJ2IsotropicHardening::operator=(const IPVariable &source)
{
  IPJ2IsotropicHardening::operator=(source);
  const IPLinearExponentialJ2IsotropicHardening* src = dynamic_cast<const IPLinearExponentialJ2IsotropicHardening*>(&source);
  if(src != NULL)
  {
  }
  return *this;
}

void IPLinearExponentialJ2IsotropicHardening::createRestart(FILE *fp)
{
  IPJ2IsotropicHardening::createRestart(fp);
}

void IPLinearExponentialJ2IsotropicHardening::setFromRestart(FILE *fp)
{
  IPJ2IsotropicHardening::setFromRestart(fp);
}

IPJ2IsotropicHardening * IPLinearExponentialJ2IsotropicHardening::clone() const
{
  return new IPLinearExponentialJ2IsotropicHardening(*this);
} 

