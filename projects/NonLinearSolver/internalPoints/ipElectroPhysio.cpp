//
// Description: storing class for Electro-Physiology
//
//
// Author:  <A. Jerusalem>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipElectroPhysio.h"

IPElectroPhysio::IPElectroPhysio(): IPVariable(), CaC(0), NaC(0), KC(0)
{
}

IPElectroPhysio::IPElectroPhysio(const IPElectroPhysio &source) : IPVariable(source)
{
   CaC      = source.CaC;
   NaC      = source.NaC;
   KC       = source.KC;
}

IPElectroPhysio &IPElectroPhysio::operator=(const IPVariable &source)
{
  IPVariable::operator=(source);
  const IPElectroPhysio* src = dynamic_cast<const IPElectroPhysio*>(&source);  
  if(src!=NULL)
  {
   CaC      = src->CaC;
   NaC      = src->NaC;
   KC       = src->KC;
  }
  return *this;
}

void IPElectroPhysio::createRestart(FILE *fp)
{
  IPVariable::setFromRestart(fp);
  fprintf(fp,"%e %e %e",CaC,NaC,KC);
}


void IPElectroPhysio::setFromRestart(FILE *fp)
{
  IPVariable::setFromRestart(fp);
  double a0,a1,a2;
  fscanf(fp,"%e %e %e",&a0,&a1,&a2);
  CaC      = a0;
  NaC      = a1;
  KC       = a2;
}

IPCableTheoryEP::IPCableTheoryEP() : IPElectroPhysio()
{

}
IPCableTheoryEP::IPCableTheoryEP(const IPCableTheoryEP &source) :IPElectroPhysio(source)
{

}

IPCableTheoryEP &IPCableTheoryEP::operator=(const IPVariable &source)
{
  IPElectroPhysio::operator=(source);
  const IPCableTheoryEP* src = dynamic_cast<const IPCableTheoryEP*>(&source);  
  if(src!=NULL)
  {
  }
  return *this;
}
void IPCableTheoryEP::createRestart(FILE *fp)
{
  IPElectroPhysio::setFromRestart(fp);
}


void IPCableTheoryEP::setFromRestart(FILE *fp)
{
  IPElectroPhysio::setFromRestart(fp);
}

IPElectroPhysio *IPCableTheoryEP::clone() const
{
  return new IPCableTheoryEP(*this);
} 


IPHodgkinHuxleyEP::IPHodgkinHuxleyEP() : IPElectroPhysio()
{

}
IPHodgkinHuxleyEP::IPHodgkinHuxleyEP(const IPHodgkinHuxleyEP &source) :IPElectroPhysio(source)
{

}

IPHodgkinHuxleyEP &IPHodgkinHuxleyEP::operator=(const IPVariable &source)
{
  IPElectroPhysio::operator=(source);
  const IPHodgkinHuxleyEP* src = dynamic_cast<const IPHodgkinHuxleyEP*>(&source);  
  if(src!=NULL)
  {
  }
  return *this;
}
void IPHodgkinHuxleyEP::createRestart(FILE *fp)
{
  IPElectroPhysio::setFromRestart(fp);
}


void IPHodgkinHuxleyEP::setFromRestart(FILE *fp)
{
  IPElectroPhysio::setFromRestart(fp);
}

IPElectroPhysio *IPHodgkinHuxleyEP::clone() const
{
  return new IPHodgkinHuxleyEP(*this);
} 

