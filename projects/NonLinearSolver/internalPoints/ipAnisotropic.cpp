//
// Description: storing class for j2 linear elasto-plastic law
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipAnisotropic.h"
IPAnisotropic::IPAnisotropic() : IPVariableMechanics(), _elasticEnergy(0.) {};
IPAnisotropic::IPAnisotropic(const IPAnisotropic &source) : IPVariableMechanics(source), _elasticEnergy(source._elasticEnergy){}
IPAnisotropic& IPAnisotropic::operator=(const IPVariable &source)
{
  IPVariableMechanics::operator=(source);
  const IPAnisotropic* src = static_cast<const IPAnisotropic*>(&source);
}

double IPAnisotropic::defoEnergy() const
{
  return _elasticEnergy;
}

