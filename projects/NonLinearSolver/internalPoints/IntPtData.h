#ifndef INTPTDATA_H_
#define INTPTDATA_H_

#include "functionSpace.h"


template<class T>
class IntPtData{
  public:
    typedef typename TensorialTraits<T>::ValType ValType;
    typedef typename TensorialTraits<T>::GradType GradType;
    typedef typename TensorialTraits<T>::HessType HessType;
    typedef typename TensorialTraits<T>::ThirdDevType ThirdDevType;

    std::vector<ValType>  val; // all values of shape functions
    std::vector<GradType> grad; // all gradients of shape functions
    std::vector<HessType> hess; // all hessiens of shape functions
    std::vector<ThirdDevType> third; // all third derivatives of shape functions
    double jacobianDet; // jacobien determinent
    bool isHessian, isThird;

  public:
    IntPtData(FunctionSpace<T>* sp, MElement* ele, IntPt& GP, bool hessian, bool thirdd):
              isHessian(hessian),isThird(thirdd){
      double u = GP.pt[0];
      double v = GP.pt[1];
      double w = GP.pt[2];
      jacobianDet = ele->getJacobianDeterminant(u,v,w);
      sp->f(ele,u,v,w,val);
      sp->gradf(ele,u,v,w,grad);
      if (hessian)
        sp->hessf(ele,u,v,w,hess);
      if (thirdd)
        sp->thirdDevf(ele,u,v,w,third);
    };
    IntPtData(const IntPtData& src) :val(src.val),
                                    grad(src.grad),hess(src.hess),third(src.third),jacobianDet(src.jacobianDet),
                                    isHessian(src.isHessian),isThird(src.isThird){}
    IntPtData& operator = (const IntPtData& src){
      val = src.val;
      grad =src.grad;
      hess = src.hess;
      third = src.third;
      jacobianDet = src.jacobianDet;
      isHessian = src.isHessian;
      isThird = src.isThird;
      return *this;
    };
    ~IntPtData(){
      val.clear();
      grad.clear();
      hess.clear();
      third.clear();
      };
};


#endif // INTPTDATA_H_
