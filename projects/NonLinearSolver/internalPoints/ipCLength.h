//
// C++ Interface: ipCLength
//
// Description: Base class for ipCLength
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
// class with the variables of IP (stress, deformation and localBasis)
#ifndef IPCLENGTH_H_
#define IPCLENGTH_H_
#include <stdlib.h>
#include <stdio.h>
#include "GmshConfig.h"
#include "STensor3.h"
#include "ipvariable.h"

class IPCLength : public IPVariable{
  protected:
   STensor3 cL;
 public:
  IPCLength();
  IPCLength(const IPCLength &source);
  virtual IPCLength &operator=(const IPVariable &source);
  virtual ~IPCLength(){}
  virtual void createRestart(FILE *fp);
  virtual void setFromRestart(FILE *fp);
  virtual const STensor3 & getConstRefToCL() const {return cL;}
  virtual STensor3 &getRefToCL() {return cL;}
  virtual IPCLength* clone() const=0;
};

class IPZeroCLength : public IPCLength
{

 protected:
 
 public:
  IPZeroCLength();
  IPZeroCLength(const IPZeroCLength &source);
  virtual IPZeroCLength &operator=(const IPVariable &source);
  virtual ~IPZeroCLength(){}
  virtual void createRestart(FILE *fp);
  virtual void setFromRestart(FILE *fp);
  virtual IPCLength* clone() const; 
  
};
class IPIsotropicCLength : public IPCLength
{

 protected:
 
 public:
  IPIsotropicCLength();
  IPIsotropicCLength(const IPIsotropicCLength &source);
  virtual IPIsotropicCLength &operator=(const IPVariable &source);
  virtual ~IPIsotropicCLength(){}
  virtual void createRestart(FILE *fp);
  virtual void setFromRestart(FILE *fp);
  virtual IPCLength* clone() const; 
  
};

class IPAnisotropicCLength : public IPCLength
{

 protected:
 
 public:
  IPAnisotropicCLength();
  IPAnisotropicCLength(const IPAnisotropicCLength &source);
  virtual IPAnisotropicCLength &operator=(const IPVariable &source);
  virtual ~IPAnisotropicCLength(){}
  virtual void createRestart(FILE *fp);
  virtual void setFromRestart(FILE *fp);
  virtual IPCLength* clone() const; 
};

class IPVariableIsotropicCLength : public IPCLength
{

 protected:
 
 public:
  IPVariableIsotropicCLength();
  IPVariableIsotropicCLength(const IPVariableIsotropicCLength &source);
  virtual IPVariableIsotropicCLength &operator=(const IPVariable &source);
  virtual ~IPVariableIsotropicCLength(){}
  virtual void createRestart(FILE *fp);
  virtual void setFromRestart(FILE *fp);
  virtual IPCLength* clone() const; 
  
};

class IPVariableAnisotropicCLength : public IPCLength
{

 protected:
 
 public:
  IPVariableAnisotropicCLength();
  IPVariableAnisotropicCLength(const IPVariableAnisotropicCLength &source);
  virtual IPVariableAnisotropicCLength &operator=(const IPVariable &source);
  virtual ~IPVariableAnisotropicCLength(){}
  virtual void createRestart(FILE *fp);
  virtual void setFromRestart(FILE *fp);
  virtual IPCLength* clone() const; 
};

#endif //IPCLength_H_

