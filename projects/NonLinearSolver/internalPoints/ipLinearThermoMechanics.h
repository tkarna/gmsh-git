//
// Description: storing class for linear thermomechanical law it is a "virtual implementation" you have to define an ipvariable
//              in your project which derive from this one. 
// Author:  <L. Homsy>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPLINEARTHERMOMECHANICS_H_
#define IPLINEARTHERMOMECHANICS_H_
#include "ipvariable.h"
#include "STensor3.h"
class IPLinearThermoMechanics : public IPVariableMechanics
{
 public:
  double _elasticEnergy;
 public:
  IPLinearThermoMechanics();
  IPLinearThermoMechanics(const IPLinearThermoMechanics &source);
  IPLinearThermoMechanics& operator=(const IPVariable &source);
  virtual double defoEnergy() const;
};

#endif // IPLINEARTHERMOMECHANICS_H_
