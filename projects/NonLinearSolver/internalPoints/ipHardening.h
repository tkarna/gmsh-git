//
// C++ Interface: ipHardening
//
// Description: Base class for ipHardening
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
// class with the variables of IP (stress, deformation and localBasis)
#ifndef IPHARDENING_H_
#define IPHARDENING_H_
#include <stdlib.h>
#include <stdio.h>
#include "GmshConfig.h"
#include "ipvariable.h"

class IPHardening : public IPVariable{
 public:
  IPHardening();
  IPHardening(const IPHardening &source);
  virtual IPHardening &operator=(const IPVariable &source);
  virtual ~IPHardening(){}
  virtual void createRestart(FILE *fp){IPVariable::setFromRestart(fp);}
  virtual void setFromRestart(FILE *fp){IPVariable::setFromRestart(fp);}
};

class IPJ2IsotropicHardening : public IPHardening{

  protected:
 
  double R;
  double dR;
  double ddR;
  double integR;
 public:
  IPJ2IsotropicHardening();
  IPJ2IsotropicHardening(const IPJ2IsotropicHardening &source);
  virtual IPJ2IsotropicHardening &operator=(const IPVariable &source);
  virtual ~IPJ2IsotropicHardening(){}
  virtual void createRestart(FILE *fp);
  virtual void setFromRestart(FILE *fp);
  virtual IPJ2IsotropicHardening * clone() const=0;
  
  virtual double getR() const {return R;}
  virtual double getDR() const {return dR;}
  virtual double getDDR() const {return ddR;}
  virtual double getIntegR() const {return integR;}

  virtual void set(const double &_r, const double &_dr, const double &_ddr, const double &_integR)
  {
    R=_r;
    dR=_dr;
    ddR=_ddr;
    integR=_integR;
  }
};

class IPPerfectlyPlasticJ2IsotropicHardening : public IPJ2IsotropicHardening
{

 protected:
 
 public:
  IPPerfectlyPlasticJ2IsotropicHardening();
  IPPerfectlyPlasticJ2IsotropicHardening(const IPPerfectlyPlasticJ2IsotropicHardening &source);
  virtual IPPerfectlyPlasticJ2IsotropicHardening &operator=(const IPVariable &source);
  virtual ~IPPerfectlyPlasticJ2IsotropicHardening(){}
  virtual void createRestart(FILE *fp);
  virtual void setFromRestart(FILE *fp);
  virtual IPJ2IsotropicHardening * clone() const; 
};

class IPPowerLawJ2IsotropicHardening : public IPJ2IsotropicHardening
{

 protected:
 public:
  IPPowerLawJ2IsotropicHardening();
  IPPowerLawJ2IsotropicHardening(const IPPowerLawJ2IsotropicHardening &source);
  virtual IPPowerLawJ2IsotropicHardening &operator=(const IPVariable &source);
  virtual ~IPPowerLawJ2IsotropicHardening(){}
  virtual void createRestart(FILE *fp);
  virtual void setFromRestart(FILE *fp);
  virtual IPJ2IsotropicHardening *clone() const; 
  
};

class IPExponentialJ2IsotropicHardening : public IPJ2IsotropicHardening
{

 protected:
 
 public:
  IPExponentialJ2IsotropicHardening();
  IPExponentialJ2IsotropicHardening(const IPExponentialJ2IsotropicHardening &source);
  virtual IPExponentialJ2IsotropicHardening &operator=(const IPVariable &source);
  virtual ~IPExponentialJ2IsotropicHardening(){}
  virtual void createRestart(FILE *fp);
  virtual void setFromRestart(FILE *fp);
  virtual IPJ2IsotropicHardening * clone() const; 
  
};

class IPSwiftJ2IsotropicHardening : public IPJ2IsotropicHardening
{

 protected:
 
 public:
  IPSwiftJ2IsotropicHardening();
  IPSwiftJ2IsotropicHardening(const IPSwiftJ2IsotropicHardening &source);
  virtual IPSwiftJ2IsotropicHardening &operator=(const IPVariable &source);
  virtual ~IPSwiftJ2IsotropicHardening(){}
  virtual void createRestart(FILE *fp);
  virtual void setFromRestart(FILE *fp);
  virtual IPJ2IsotropicHardening * clone() const; 
  
};

class IPLinearExponentialJ2IsotropicHardening : public IPJ2IsotropicHardening
{

 protected:
 
 public:
  IPLinearExponentialJ2IsotropicHardening();
  IPLinearExponentialJ2IsotropicHardening(const IPLinearExponentialJ2IsotropicHardening &source);
  virtual IPLinearExponentialJ2IsotropicHardening &operator=(const IPVariable &source);
  virtual ~IPLinearExponentialJ2IsotropicHardening(){}
  virtual void createRestart(FILE *fp);
  virtual void setFromRestart(FILE *fp);
  virtual IPJ2IsotropicHardening * clone() const; 
  
};
#endif //IPHardening_H_

