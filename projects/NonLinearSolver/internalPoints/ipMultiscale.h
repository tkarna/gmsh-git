#ifndef IPMULTISCALE_H_
#define IPMULTISCALE_H_

#include "ipvariable.h"
#include "nonLinearMechSolver.h"

class IPMultiscale{
  protected:
    nonLinearMechSolver* _solver;

  public:
    IPMultiscale(nonLinearMechSolver* sol): _solver(sol){};
    IPMultiscale(const IPMultiscale& src) {
      _solver = src._solver;
    }
    IPMultiscale& operator = (const IPMultiscale& src){
      _solver = src._solver;
      return *this;
    }
    virtual ~IPMultiscale(){
      if (_solver) delete _solver;
    };
    nonLinearMechSolver* getSolver() {return _solver;};
};

#endif // IPMULTISCALE_H_
