//
// Description: storing class for gurson model
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPNONLOCALDAMAGEGURSON_H_
#define IPNONLOCALDAMAGEGURSON_H_
#include "ipvariable.h"
#include "STensor3.h"
#include "ipHardening.h"
#include "j2IsotropicHardening.h"
#include "ipCLength.h"
#include "CLengthLaw.h"
#include "GursonDamageNucleation.h"

class IPNonLocalDamageGurson : public IPVariableMechanics
{

 protected:
  IPCLength* ipvCL;
  IPJ2IsotropicHardening* ipvJ2IsotropicHardening;
  IPGursonNucleation*     ipvgdn;

  double _elasticEnergy; // elastic energy stored

  double _eplmatrix;     // equivalent plastic strain in the matrix (hatp)
  double _fV;            // local porosity
  double _fVstar;        // local GTN corrected porosity
  double _nldfVstar;     // non-local GTN corrected porosity tilde{f}^star_V

  STensor3 _Fp;    // plastic part of the deformation gradient
  
  STensor3	_DFvStarDStrain;     //derivative of the local corrected porosity with the strain
  double        _DFvStarDNldfVstar;  //derivative of the local corrected porosity with the non local corrected porosity
  
 public:
  IPNonLocalDamageGurson();
  IPNonLocalDamageGurson(double fVinitial, const J2IsotropicHardening *j2IH, const CLengthLaw *cll, 
                          const GursonDamageNucleation *gdn);
  ~IPNonLocalDamageGurson()
  {
    if(ipvJ2IsotropicHardening != NULL) 
    {
      delete ipvJ2IsotropicHardening;
    }
    if(ipvCL != NULL) 
    {
      delete ipvCL;
    }
    if(ipvgdn != NULL)
    {
      delete ipvgdn;
    }
  }


  IPNonLocalDamageGurson(const IPNonLocalDamageGurson &source);
  IPNonLocalDamageGurson& operator=(const IPVariable &source);
  virtual void createRestart(FILE *fp); // default don't save values for a restart
  virtual void setFromRestart(FILE *fp);

  virtual double defoEnergy() const;
  virtual double &getRefToElasticEnergy() {return _elasticEnergy;};
  virtual double plasticEnergy() const;

  virtual const IPJ2IsotropicHardening &getConstRefToIPJ2IsotropicHardening() const 
  {
    if(ipvJ2IsotropicHardening==NULL)
      Msg::Error("IPNonLocalDamageGurson: ipvJ2IsotropicHardening not initialized");
    return *ipvJ2IsotropicHardening;
  }
  virtual IPJ2IsotropicHardening &getRefToIPJ2IsotropicHardening()
  {
    if(ipvJ2IsotropicHardening==NULL)
      Msg::Error("IIPNonLocalDamageGurson: ipvJ2IsotropicHardening not initialized");
    return *ipvJ2IsotropicHardening;
  }
  virtual const IPCLength &getConstRefToIPCLength() const 
  {
    if(ipvCL==NULL)
      Msg::Error("IPNonLocalDamageGurson: ipvCL not initialized");
    return *ipvCL;
  }
  virtual IPCLength &getRefToIPCLength() 
  {
    if(ipvCL==NULL)
      Msg::Error("IPNonLocalDamageGurson: ipvCL not initialized");
    return *ipvCL;
  }
  virtual const IPGursonNucleation &getConstRefToIPGursonNucleation() const 
  {
    if(ipvCL==NULL)
      Msg::Error("IPNonLocalDamageGurson: ipvgdn not initialized");
    return *ipvgdn;
  }
  virtual IPGursonNucleation &getRefToIPGursonNucleation() 
  {
    if(ipvgdn==NULL)
      Msg::Error("IPNonLocalDamageGurson: ipvCL not initialized");
    return *ipvgdn;
  }
  virtual const STensor3 &getConstRefToCharacteristicLength() const
  {
    if(ipvCL==NULL)
      Msg::Error("IPNonLocalDamageGurson: ipvCL not initialized");
    return ipvCL->getConstRefToCL();
  }
  virtual STensor3 &getRefToCharacteristicLength() const
  {
    if(ipvCL==NULL)
      Msg::Error("IPNonLocalDamageJ2Hyper: ipvCL not initialized");
    return ipvCL->getRefToCL();
  }
  virtual const STensor3 & getConstRefToFp() const
  {
    return _Fp;
  }
  virtual STensor3 & getRefToFp()
  {
    return _Fp;
  }

  virtual double getMatrixPlasticStrain() const { return _eplmatrix;}
  virtual double &getRefToMatrixPlasticStrain() { return _eplmatrix;}
  virtual double getLocalPorosity() const { return _fV;}
  virtual double &getRefToLocalPorosity() { return _fV;}
  virtual double getLocalCorrectedPorosity() const { return _fVstar;}
  virtual double &getRefToLocalCorrectedPorosity() { return _fVstar;}
  virtual double getNonLocalCorrectedPorosity() const { return _nldfVstar;}
  virtual double &getRefToNonLocalCorrectedPorosity() { return _nldfVstar;}
  
  virtual double &getRefToDLocalCorrectedPorosityDNonLocalCorrectedPorosity()
  { 
    return _DFvStarDNldfVstar;
  }
  virtual double getDLocalCorrectedPorosityDNonLocalCorrectedPorosity() const
  { 
    return _DFvStarDNldfVstar;
  }
  virtual const STensor3 & getConstRefToDLocalCorrectedPorosityDStrain() const
  {
    return _DFvStarDStrain;
  }  
  virtual STensor3 & getRefToDLocalCorrectedPorosityDStrain()
  {
    return _DFvStarDStrain;
  }

  virtual double getDamage() const 
  {
    return getLocalCorrectedPorosity();
  }

};

#endif // IPNONLOCALDAMAGEGURSON_H_
