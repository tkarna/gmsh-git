//
// C++ Interface: ipGursonNucleation
//
// Description: Base class for ipGursonNucleation
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
// class with the variables of IP 
#ifndef IPGURSONNUCLEATION_H_
#define IPGURSONNUCLEATION_H_
#include <stdlib.h>
#include <stdio.h>
#include "GmshConfig.h"
#include "ipvariable.h"

class IPGursonNucleation : public IPVariable{
  protected:
 
  double fdot;
  double dFdot;
  double ddFdot;

 public:
  IPGursonNucleation();
  IPGursonNucleation(const IPGursonNucleation &source);
  virtual IPGursonNucleation &operator=(const IPVariable &source);
  virtual ~IPGursonNucleation(){}
  virtual void createRestart(FILE *fp);
  virtual void setFromRestart(FILE *fp);
  virtual IPGursonNucleation * clone() const=0;
  virtual double getFdot() const {return fdot;}
  virtual double getDFdot() const {return dFdot;}
  virtual double getDDFdot() const {return ddFdot;}
  virtual void set(const double &_fdot, const double &_dFdot, const double &_ddFdot)
  {
    fdot=_fdot;
    dFdot=_dFdot;
    ddFdot=_ddFdot;
  }
};

class IPLinearGursonNucleationRate : public IPGursonNucleation{


 public:
  IPLinearGursonNucleationRate();
  IPLinearGursonNucleationRate(const IPLinearGursonNucleationRate &source);
  virtual IPLinearGursonNucleationRate &operator=(const IPVariable &source);
  virtual ~IPLinearGursonNucleationRate(){}
  virtual IPGursonNucleation * clone() const;
  virtual void createRestart(FILE *fp);
  virtual void setFromRestart(FILE *fp);
};

class IPExponentialGursonNucleationRate : public IPGursonNucleation{


 public:
  IPExponentialGursonNucleationRate();
  IPExponentialGursonNucleationRate(const IPExponentialGursonNucleationRate &source);
  virtual IPExponentialGursonNucleationRate &operator=(const IPVariable &source);
  virtual ~IPExponentialGursonNucleationRate(){}
  virtual IPGursonNucleation * clone() const;
  virtual void createRestart(FILE *fp);
  virtual void setFromRestart(FILE *fp);
};

#endif //IPGursonNucleation_H_
