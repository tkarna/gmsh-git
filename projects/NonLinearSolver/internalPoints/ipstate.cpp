//
// C++ Interface: terms
//
// Description: Class to store internal variables at gauss point
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "ipstate.h"
#include "partDomain.h"
#include "MInterfaceElement.h"
#include "MElement.h"
#include "nonLinearMechSolver.h"
#include "numericalMaterial.h"

#if defined(HAVE_MPI)
#include <mpi.h>
#endif // HAVE_MPI
IP3State::~IP3State(){
  if(_initial) delete _initial;
  if(_step2) delete _step2;
  if(_step1) delete _step1;
}

IP3State::IP3State(const IP3State &source) : IPStateBase()
{
  _initial = source._initial;
  _step1 = source._step1;
  _step2 = source._step2;
  _st = source._st;
}

IP3State & IP3State::operator = (const IPStateBase &source){
  IPStateBase::operator=(source);
  const IP3State *src = static_cast<const IP3State*>(&source);
  *_initial = *src->_initial;
  *_step1 = *src->_step1;
  *_step2 = *src->_step2;
  _st = src->_st;
  return *this;
}

IPVariable* IP3State::getState(const whichState wst) const
{
  switch(wst){
    case temp :
      if (_temp == NULL){
        if (_initial)
          _temp = _initial->clone();
        else if (_step1)
          _temp = _step1->clone();
        else if (_step2)
          _temp = _step2->clone();
        else
          Msg::Error("this IP3State is not initialized");
      }
      return _temp;
      break;
    case initial :
      return _initial;
      break;
    case previous :
      if(*_st) return _step1; else return _step2;
      break;
    case current :
      if(*_st) return _step2; else return _step1;
      break;
    default : Msg::Error("Impossible to select the desired state for internal variable \n");
  }
}

void IP3State::createRestart(FILE *fp)
{
  IPVariable *ipv = this->getState(IPStateBase::initial);
  ipv->createRestart(fp);
  ipv = this->getState(IPStateBase::previous);
  ipv->createRestart(fp);
  ipv = this->getState(IPStateBase::current);
  ipv->createRestart(fp);
}

void IP3State::setFromRestart(FILE *fp)
{
  IPVariable *ipv = this->getState(IPStateBase::initial);
  ipv->setFromRestart(fp);
  ipv = this->getState(IPStateBase::previous);
  ipv->setFromRestart(fp);
  ipv = this->getState(IPStateBase::current);
  ipv->setFromRestart(fp);
}

IP3StateMultiscale::IP3StateMultiscale(const IP3StateMultiscale &source) :IP3State(source){
  _solver = source._solver;
} ;
IP3StateMultiscale & IP3StateMultiscale::operator = (const IPStateBase &source){
  IP3State::operator=(source);
  const IP3StateMultiscale *src = dynamic_cast<const IP3StateMultiscale*>(&source);
  if (src){
    _solver = src->_solver;
  }
  return *this;
};

AllIPState::AllIPState(std::vector<partDomain*> &vdom)
{
  state = true; // at creation of object state is true
  isMultiscale = false;
  std::map<int,materialLaw*> allNumericLaw;
  #if defined(HAVE_MPI)
  int rootRank = 0;
  std::set<int> otherRanks;
  #endif //HAVE_MPI
  for(std::vector<partDomain*>::iterator itdom=vdom.begin(); itdom!=vdom.end(); ++itdom){
    partDomain *dom = *itdom;
    materialLaw* mlaw = dom->getMaterialLaw();
    if(mlaw!=NULL)
    {
      if (mlaw->isNumeric()){
        allNumericLaw[mlaw->getNum()] = mlaw;
      }
    }
    #if defined(HAVE_MPI)
    if (otherRanks.size()==0){
      rootRank = dom->getRootRank();
      dom->getOtherRanks(otherRanks);
    }
    if (rootRank == Msg::GetCommRank())
    #endif
    {
      dom->initMicroMeshId();
    }
  };

  if (allNumericLaw.size()>0){
    isMultiscale = true;
  }

  #if defined(HAVE_MPI)
  for (std::map<int,materialLaw*>::iterator it = allNumericLaw.begin(); it!= allNumericLaw.end(); it++){
    materialLaw* mlaw = it->second;
    numericalMaterial* nummat = dynamic_cast<numericalMaterial*>(mlaw);
    if (rootRank == Msg::GetCommRank()){
      int bufferSize= nummat->getNumberValuesMPI();
      int* buffer=NULL;
      if (bufferSize>0){
        buffer = new int[bufferSize];
        nummat->fillValuesMPI(buffer);
      }
      for (std::set<int>::iterator itR = otherRanks.begin(); itR != otherRanks.end(); itR++){
        int other=*itR;
        int tag1 = numericalMaterial::createTypeWithTwoInts(mlaw->getNum(),other);
        MPI_Send(&bufferSize,1,MPI_INT,other,tag1,MPI_COMM_WORLD);
        if (bufferSize>0){
          int tag2 = numericalMaterial::createTypeWithTwoInts(mlaw->getNum(),other+1);
          MPI_Send(buffer,bufferSize,MPI_INT,other,tag2,MPI_COMM_WORLD);
        }
      }
      if (bufferSize>0){
        delete buffer;
      }
    }
    else if (otherRanks.find(Msg::GetCommRank()) != otherRanks.end()){
      int bufferSize = 0;
      int tag1 = numericalMaterial::createTypeWithTwoInts(mlaw->getNum(),Msg::GetCommRank());
      MPI_Status status;
      MPI_Recv(&bufferSize,1,MPI_INT,rootRank,tag1,MPI_COMM_WORLD,&status);
      if (bufferSize>0){
        int* buffer = new int[bufferSize];
        int tag2 = numericalMaterial::createTypeWithTwoInts(mlaw->getNum(),Msg::GetCommRank()+1);
        MPI_Recv(buffer,bufferSize,MPI_INT,rootRank,tag2,MPI_COMM_WORLD,&status);
        nummat->getValuesMPI(buffer,bufferSize);
        delete buffer;
      }
    }
    nummat->printMeshIdMap();
  }
  #endif//HAVE_MPI

  for(std::vector<partDomain*>::iterator itdom=vdom.begin(); itdom!=vdom.end(); ++itdom){
    partDomain *dom = *itdom;
    dom->createIPState(_mapall,&state);
  }
 #if defined(HAVE_MPI)
  if(Msg::GetCommSize()>1) // maybe that some values have to be exchanged between interDomain!
  {
    // Communication with the domain in 2 times. First to known the number of value to exchanged
    // to size the array and a second to fill the array
    std::map<dgPartDomain*,int*> interSize;
    for(std::vector<partDomain*>::iterator itdom=vdom.begin(); itdom!=vdom.end(); ++itdom)
    {
      partDomain *dom = *itdom;
      if(dom->IsInterfaceTerms())
      {
        dgPartDomain *dgdom = static_cast<dgPartDomain*>(dom);
        if(dgdom->g->size()==0) // otherwise the domain is not an interface domain
        {
           int *sizeeachrank = new int[Msg::GetCommSize()];
           for(int i=0;i<Msg::GetCommSize();i++)
              sizeeachrank[i]=0;
           dgdom->numberValuesToTransfertMPI(this,sizeeachrank);
           bool vex=false;
           for(int i=0;i<Msg::GetCommSize();i++)
           {
             if(sizeeachrank[i]!=0)
             {
               vex = true;
               break;
             }
           }
           if(vex)
           {
             interSize.insert(std::pair<dgPartDomain*,int*>(dgdom,sizeeachrank));
           }
           else
           {
             delete[] sizeeachrank;
           }
        }
      }
    }

    // compute the total size to communicate to each rank
    int *allsize = new int[Msg::GetCommSize()];
    for(int i=0;i<Msg::GetCommSize();i++)
      allsize[i] = 0;
    for(std::map<dgPartDomain*,int*>::iterator it=interSize.begin(); it!=interSize.end();++it)
    {
      for(int i=0;i<Msg::GetCommSize();i++)
        if(it->second[i]>0)
          allsize[i] += (it->second[i]+1); // as we give in first argument the domain num
    }
    double *arrayMPI;
    int arraympisize=0;
    MPI_Status mpistatus;
    for(int i=1;i<Msg::GetCommSize();i++) // no comm with node 0 (we take the data from the lowest rank)
    {
      if(Msg::GetCommRank() < i) // communicate data from this rank to rank i
      {
        if(allsize[i]!=0) // otherwise no data to transfert
        {
          if(arraympisize<allsize[i])
          {
            if(arraympisize!=0) delete[] arrayMPI;
            arraympisize = allsize[i];
            arrayMPI = new double[arraympisize];
          }
          // fill the vector to communicate to rank i
          int curpos=0;
          for(std::map<dgPartDomain*,int*>::iterator it=interSize.begin(); it!=interSize.end();++it)
          {
            if(it->second[i]!=0){
	      arrayMPI[curpos] = (double)it->first->getPhysical();
              it->first->fillArrayIPvariableMPI(this,i,&arrayMPI[curpos+1]);
              curpos+=(it->second[i]+1); // as we communicate the number of domain in argument 0
            }
	  }
          MPI_Send(arrayMPI,allsize[i],MPI_DOUBLE,i,Msg::GetCommRank(),MPI_COMM_WORLD);
	}
      }
      else if(Msg::GetCommRank() == i) // recieved the data from rank i
      {
        for(int j=0;j<i;j++){
          if(allsize[j]!=0){
            // array dimension
            if(arraympisize<allsize[j])
            {
              if(arraympisize!=0) delete[] arrayMPI;
              arraympisize = allsize[j];
              arrayMPI = new double[arraympisize];
            }
            // receieve the data
            MPI_Recv(arrayMPI,allsize[j],MPI_DOUBLE,j,j,MPI_COMM_WORLD,&mpistatus);
            // set data in each interDomain
            int curpos=0;
            while(curpos<allsize[j])
            {
              int domphys = (int)arrayMPI[curpos];
	          // get the domain
              dgPartDomain *idom=NULL;
              int domsize=0;
	          for(std::map<dgPartDomain*,int*>::iterator it=interSize.begin(); it!=interSize.end();++it)
              {
                if(domphys == it->first->getPhysical())
                {
                  idom = it->first;
                  domsize = it->second[j];
                  break;
                }
              }
              if(idom!=NULL){
		        idom->setIPVariableFromArrayMPI(this,j,domsize,&arrayMPI[curpos+1]);
                curpos+= (1+domsize);
	          }
              else
              {
                Msg::Fatal("Domain %d to communicate ipvariable from rank %d seems not existing on rank %d",domphys,j,Msg::GetCommRank());
              }
            }
          }
        }
      }
    }

    // clear previously allocated array
    if(arraympisize!=0) delete[] arrayMPI;
    delete[] allsize;
    for(std::map<dgPartDomain*,int*>::iterator it=interSize.begin(); it!=interSize.end();++it)
    {
      delete[] it->second;
    }
  }

 #endif // HAVE_MPI
}
AllIPState::~AllIPState()
{
  for(ipstateContainer::iterator it=_mapall.begin();it!=_mapall.end();++it){
    ipstateElementContainer vips = (*it).second;
    for(int i=0;i<vips.size();i++)
	  delete vips[i];
  }
};

AllIPState::ipstateElementContainer* AllIPState::getIPstate(const long int num)
{

  ipstateContainer::iterator it = _mapall.find(num);
 #ifdef _DEBUG
  if(it == _mapall.end()){
    Msg::Error("Try to get an inexisting IPState on rank %d",Msg::GetCommRank());
    return NULL;
  }
  else
  #endif // _DEBUG
  {
    return &(it->second);
  }
}

AllIPState::ipstateElementContainer* AllIPState::operator[](const long int num)
{
  return &(_mapall.find(num)->second);
}

void AllIPState::createRestart(FILE *fp)
{
  for(ipstateContainer::iterator its=_mapall.begin(); its!=_mapall.end();++its)
  {
    ipstateElementContainer& vips = its->second;
    for(ipstateElementContainer::iterator it=vips.begin(); it!=vips.end();++it)
    {
       IPStateBase* ips = *it;
       ips->createRestart(fp);
    }
  }
}

void AllIPState::setFromRestart(FILE *fp)
{
  for(ipstateContainer::iterator its=_mapall.begin(); its!=_mapall.end();++its)
  {
    ipstateElementContainer& vips = its->second;
    for(ipstateElementContainer::iterator it=vips.begin(); it!=vips.end();++it)
    {
       IPStateBase* ips = *it;
       ips->setFromRestart(fp);
    }
  }
}

void AllIPState::nextStep() {
    state ? state=false : state=true;
    if (isMultiscale){
      for (ipstateContainer::iterator it = _mapall.begin(); it!= _mapall.end(); it++){
        std::vector<IPStateBase*> *vips = &((*it).second);
        for (int i = 0; i< vips->size(); i++){
          IP3StateMultiscale* multiscaleState =dynamic_cast<IP3StateMultiscale*>((*vips)[i]);
          if (multiscaleState){
            nonLinearMechSolver* solver = multiscaleState->getSolver();
            if (solver!= NULL)
              solver->nextStep();
          }
        }
      }
    }
  } // change boolvalue
