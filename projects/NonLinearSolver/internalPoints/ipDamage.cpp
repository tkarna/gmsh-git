//
// Description: storing class for non Local Damage
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipDamage.h"

IPDamage::IPDamage(): IPVariable(), D(0), maximalp (0), DeltaD (0), dDdp(0), dDdFe(0)
{
}

IPDamage::IPDamage(const IPDamage &source) : IPVariable(source)
{
   D        = source.D;
   maximalp = source.maximalp;
   DeltaD   = source.D;
   dDdp     = source.dDdp;
   dDdFe    = source.dDdFe;
}

IPDamage &IPDamage::operator=(const IPVariable &source)
{
  IPVariable::operator=(source);
  const IPDamage* src = dynamic_cast<const IPDamage*>(&source);  
  if(src!=NULL)
  {
   D        = src->D;
   maximalp = src->maximalp;
   DeltaD   = src->D;
   dDdp     = src->dDdp;
   dDdFe    = src->dDdFe;
  }
  return *this;
}

void IPDamage::createRestart(FILE *fp)
{
  IPVariable::setFromRestart(fp);
  fprintf(fp,"%e %e %e %e",D,maximalp,DeltaD,dDdp);
  fprintf(fp,"%e %e %e %e %e %e %e %e %e",dDdFe(0,0),dDdFe(0,1),dDdFe(0,2),dDdFe(1,0),
                                dDdFe(1,1),dDdFe(1,2),dDdFe(2,0),dDdFe(2,1),dDdFe(2,2));
}


void IPDamage::setFromRestart(FILE *fp)
{
  IPVariable::setFromRestart(fp);
  double a0,a1,a2,a3,a00,a01,a02,a10,a11,a12,a20,a21,a22;
  fscanf(fp,"%e %e %e %e",&a0,&a1,&a2,&a3);
  D        = a0;
  maximalp = a1;
  DeltaD   = a2;
  dDdp     = a3;
  fscanf(fp,"%e %e %e %e %e %e %e %e %e",&a00,&a01,&a02,&a10,&a11,&a12,&a20,&a21,&a22);
  dDdFe(0,0) = a00;
  dDdFe(0,1) = a01;
  dDdFe(0,2) = a02;
  dDdFe(1,0) = a10;
  dDdFe(1,1) = a11;
  dDdFe(1,2) = a12;
  dDdFe(2,0) = a20;
  dDdFe(2,1) = a21;
  dDdFe(2,2) = a22;
}

IPLemaitreChabocheDamage::IPLemaitreChabocheDamage() : IPDamage()
{

}
IPLemaitreChabocheDamage::IPLemaitreChabocheDamage(const IPLemaitreChabocheDamage &source) :IPDamage(source)
{

}

IPLemaitreChabocheDamage &IPLemaitreChabocheDamage::operator=(const IPVariable &source)
{
  IPDamage::operator=(source);
  const IPLemaitreChabocheDamage* src = dynamic_cast<const IPLemaitreChabocheDamage*>(&source);  
  if(src!=NULL)
  {
  }
  return *this;
}
void IPLemaitreChabocheDamage::createRestart(FILE *fp)
{
  IPDamage::setFromRestart(fp);
}


void IPLemaitreChabocheDamage::setFromRestart(FILE *fp)
{
  IPDamage::setFromRestart(fp);
}

IPDamage *IPLemaitreChabocheDamage::clone() const
{
  return new IPLemaitreChabocheDamage(*this);
} 


IPPowerLaw::IPPowerLaw() : IPDamage()
{

}
IPPowerLaw::IPPowerLaw(const IPPowerLaw &source) :IPDamage(source)
{

}

IPPowerLaw &IPPowerLaw::operator=(const IPVariable &source)
{
  IPDamage::operator=(source);
  const IPPowerLaw* src = dynamic_cast<const IPPowerLaw*>(&source);  
  if(src!=NULL)
  {
  }
  return *this;
}
void IPPowerLaw::createRestart(FILE *fp)
{
  IPDamage::setFromRestart(fp);
}


void IPPowerLaw::setFromRestart(FILE *fp)
{
  IPDamage::setFromRestart(fp);
}

IPDamage *IPPowerLaw::clone() const
{
  return new IPPowerLaw(*this);
} 

