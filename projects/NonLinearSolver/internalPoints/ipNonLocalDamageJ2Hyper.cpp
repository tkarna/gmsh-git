//
// Description: storing class for j2 elasto-plastic law with non-local damage
//
//
// Author:  <L. Noels>, (C) 2014
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipNonLocalDamageJ2Hyper.h"
IPNonLocalDamageJ2Hyper::IPNonLocalDamageJ2Hyper() : IPJ2linear(),  _nldJ2HyperDLocalPlasticStrainDStrain(0),
							_nldJ2HyperEffectivePlasticStrain (0), _nldJ2HyperDLocalPlasticStrainDNonLocalPlasticStrain(0)
{ 
  Msg::Error("IPNonLocalDamageJ2Hyper::IPNonLocalDamageJ2Hyper is not initialized with a Characteritsic IP Variable nor a IP CLength");
ipvCL=NULL;

  Msg::Error("IPNonLocalDamageJ2Hyper::IPNonLocalDamageJ2Hyper is not initialized with a damage IP Variable nor a IP CLength");
ipvDam=NULL;

};

IPNonLocalDamageJ2Hyper::IPNonLocalDamageJ2Hyper(const J2IsotropicHardening *j2IH, 
                                                 const CLengthLaw *cll, const DamageLaw *daml) : 
                                                    IPJ2linear(j2IH), _nldJ2HyperDLocalPlasticStrainDStrain(0),
						_nldJ2HyperEffectivePlasticStrain (0),  
						_nldJ2HyperDLocalPlasticStrainDNonLocalPlasticStrain(0)
{
  ipvCL=NULL;
  if(cll ==NULL) Msg::Error("IPNonLocalDamageJ2Hyper::IPNonLocalDamageJ2Hyper has no cll");
  cll->createIPVariable(ipvCL);

  ipvDam=NULL;
  if(daml ==NULL) Msg::Error("IPNonLocalDamageJ2Hyper::IPNonLocalDamageJ2Hyper has no daml");
  daml->createIPVariable(ipvDam);

};

  


IPNonLocalDamageJ2Hyper::IPNonLocalDamageJ2Hyper(const IPNonLocalDamageJ2Hyper &source) : IPJ2linear(source)
{
  _nldJ2HyperDLocalPlasticStrainDStrain=source._nldJ2HyperDLocalPlasticStrainDStrain;
  _nldJ2HyperEffectivePlasticStrain=source._nldJ2HyperEffectivePlasticStrain;
  _nldJ2HyperDLocalPlasticStrainDNonLocalPlasticStrain=source._nldJ2HyperDLocalPlasticStrainDNonLocalPlasticStrain;
  if(source.ipvCL != NULL)
  {
    ipvCL = source.ipvCL->clone();
  }
  if(source.ipvDam != NULL)
  {
    ipvDam = source.ipvDam->clone();
  }
}
IPNonLocalDamageJ2Hyper& IPNonLocalDamageJ2Hyper::operator=(const IPVariable &source)
{
  IPJ2linear::operator=(source);
  const IPNonLocalDamageJ2Hyper* src = dynamic_cast<const IPNonLocalDamageJ2Hyper*>(&source);
  if(src != NULL)
  {
    _nldJ2HyperDLocalPlasticStrainDStrain=src->_nldJ2HyperDLocalPlasticStrainDStrain;

    _nldJ2HyperEffectivePlasticStrain=src->_nldJ2HyperEffectivePlasticStrain;
    _nldJ2HyperDLocalPlasticStrainDNonLocalPlasticStrain=src->_nldJ2HyperDLocalPlasticStrainDNonLocalPlasticStrain;
    if(ipvCL != NULL) delete ipvCL;
    if(src->ipvCL != NULL)
    {
      ipvCL=src->ipvCL->clone();
    }
    if(ipvDam != NULL) delete ipvDam;
    if(src->ipvDam != NULL)
    {
      ipvDam=src->ipvDam->clone();
    }
  }
  return *this;
}

void IPNonLocalDamageJ2Hyper::createRestart(FILE *fp)
{
  IPJ2linear::createRestart(fp);
 fprintf(fp,"%e %e %e %e %e %e %e %e %e",_nldJ2HyperDLocalPlasticStrainDStrain(0,0),_nldJ2HyperDLocalPlasticStrainDStrain(0,1),_nldJ2HyperDLocalPlasticStrainDStrain(0,2),_nldJ2HyperDLocalPlasticStrainDStrain(1,0),_nldJ2HyperDLocalPlasticStrainDStrain(1,1),_nldJ2HyperDLocalPlasticStrainDStrain(1,2),_nldJ2HyperDLocalPlasticStrainDStrain(2,0),_nldJ2HyperDLocalPlasticStrainDStrain(2,1),_nldJ2HyperDLocalPlasticStrainDStrain(2,2));
fprintf(fp,"%e %e",_nldJ2HyperEffectivePlasticStrain, _nldJ2HyperDLocalPlasticStrainDNonLocalPlasticStrain);
  if(ipvCL != NULL)
  {
    fprintf(fp,"%d",1);
    ipvCL->createRestart(fp);
  }
  else
    fprintf(fp,"%d",0);
  if(ipvDam != NULL)
  {
    fprintf(fp,"%d",1);
    ipvDam->createRestart(fp);
  }
  else
    fprintf(fp,"%d",0);
}

void IPNonLocalDamageJ2Hyper::setFromRestart(FILE *fp)
{
  IPJ2linear::setFromRestart(fp);
  double a00,a01,a02,a10,a11,a12,a20,a21,a22;
  fscanf(fp,"%e %e %e %e %e %e %e %e %e",&a00,&a01,&a02,&a10,&a11,&a12,&a20,&a21,&a22);
  _nldJ2HyperDLocalPlasticStrainDStrain(0,0) = a00;
  _nldJ2HyperDLocalPlasticStrainDStrain(0,1) = a01;
  _nldJ2HyperDLocalPlasticStrainDStrain(0,2) = a02;
  _nldJ2HyperDLocalPlasticStrainDStrain(1,0) = a10;
  _nldJ2HyperDLocalPlasticStrainDStrain(1,1) = a11;
  _nldJ2HyperDLocalPlasticStrainDStrain(1,2) = a12;
  _nldJ2HyperDLocalPlasticStrainDStrain(2,0) = a20;
  _nldJ2HyperDLocalPlasticStrainDStrain(2,1) = a21;
  _nldJ2HyperDLocalPlasticStrainDStrain(2,2) = a22;

   fscanf(fp,"%e %e",&a00,&a02);
  _nldJ2HyperEffectivePlasticStrain = a00;
  _nldJ2HyperDLocalPlasticStrainDNonLocalPlasticStrain=a02;
  int ipcl=0;
  fscanf(fp,"%d",&ipcl);
  if(ipcl == 1)
  {
    if(ipvCL == NULL) Msg::Error("IPNonLocalDamageJ2Hyper::setFromRestart: ipvCL should be initialized");
    ipvCL->setFromRestart(fp);
  }
  int ipdam=0;
  fscanf(fp,"%d",&ipdam);
  if(ipdam == 1)
  {
    if(ipvDam == NULL) Msg::Error("IPNonLocalDamageJ2Hyper::setFromRestart: ipvDam should be initialized");
    ipvDam->setFromRestart(fp);
  }
}

