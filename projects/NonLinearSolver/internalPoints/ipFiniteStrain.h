#ifndef FSIPVARIABLEBASE_H_
#define FSIPVARIABLEBASE_H_

#include "ipvariable.h"
#include "IntPtData.h"

class ipFiniteStrain : public IPVariableMechanics{
  protected:
    mutable IntPtData<double>* _GPData;

  public:
    ipFiniteStrain();
    ipFiniteStrain(const ipFiniteStrain &source);
    virtual ipFiniteStrain &operator=(const IPVariable &source);
    virtual ~ipFiniteStrain();

    #if defined(HAVE_MPI)
    // using in multiscale analysis with MPI
    // get number of values to set the kinematics of microscopic problem
    virtual int getMacroNumberElementDataSendToMicroProblem() const;
    // get number of values obtained by microscopic analysis to send to macroscopic analysis
    virtual int getMicroNumberElementDataSendToMacroProblem() const ;
    // get macroscopic kinematic data to send to microscopic problem
    virtual void getMacroDataSendToMicroProblem(double* val) const;
    // get computed data obtaind by microscopic analysis to send to macroscopic analysis
    virtual void getMicroDataToMacroProblem(double* val) const ;
    // set the received data from microscopic analysis to microscopic analysis
    virtual void setReceivedMacroDataToMicroProblem(const double* val);
    // set the received data from microscopic analysis to macroscopic analysis
    virtual void setReceivedMicroDataToMacroProblem(const double* val);
    #endif

      // get shape function information from ipvariable
    virtual std::vector<TensorialTraits<double>::ValType>& f(FunctionSpaceBase* space, MElement* ele, IntPt &GP, const bool hessian, const bool thirdd) const;
    virtual std::vector<TensorialTraits<double>::GradType>& gradf(FunctionSpaceBase* space, MElement* ele, IntPt &GP, const bool hessian, const bool thirdd) const;
    virtual std::vector<TensorialTraits<double>::HessType>& hessf(FunctionSpaceBase* space, MElement* ele, IntPt &GP, const bool hessian, const bool thirdd) const;
    virtual std::vector<TensorialTraits<double>::ThirdDevType>& thirdDevf(FunctionSpaceBase* space, MElement* ele, IntPt &GP, const bool hessian, const bool thirdd) const;
    virtual double& getJacobianDeterminant(FunctionSpaceBase* space, MElement* ele, IntPt &GP, const bool hessian, const bool thirdd) const;

    // Archiving data
    virtual double get(const int i) const=0;
    virtual double defoEnergy() const=0;
    virtual double plasticEnergy() const=0;
    virtual double vonMises() const=0;
    virtual void getCauchyStress(STensor3 &cauchy) const=0;
    virtual double getJ() const=0;

    virtual const STensor3 &getConstRefToDeformationGradient() const=0;
    virtual STensor3 &getRefToDeformationGradient()=0;

    virtual const STensor3 &getConstRefToFirstPiolaKirchhoffStress() const=0;
    virtual STensor3 &getRefToFirstPiolaKirchhoffStress()=0;

    virtual const STensor43 &getConstRefToTangentModuli() const=0;
    virtual STensor43 &getRefToTangentModuli()=0;
};


#endif // FSIPVARIABLEBASE_H_
