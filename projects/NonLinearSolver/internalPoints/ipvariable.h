//
// C++ Interface: ipvariable
//
// Description: Base class for ipvariable
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
// class with the variables of IP (stress, deformation and localBasis)

#ifndef IPVARIABLE_H_
#define IPVARIABLE_H_
#include <stdlib.h>
#include <stdio.h>
#include "GmshConfig.h"
class IPVariable
{
 public :
  IPVariable(){}
  virtual ~IPVariable(){}
  // copie constructor
  IPVariable(const IPVariable &source){};
  virtual IPVariable &operator=(const IPVariable &source){
    return *this;
  };
  virtual double get(const int i) const{return 0.;} // Allow to get any component defined in the ipvariable
 #if defined(HAVE_MPI)
  // by default in MPI there is no value that are communicate between rank.
  // but you can communicate values by filling these functions for your IPVariable
  virtual int numberValuesToCommunicateMPI()const{return 0;}
  virtual void fillValuesToCommunicateMPI(double *arrayMPI)const{};
  virtual void getValuesFromMPI(const double *arrayMPI){};
 #endif // HAVE_MPI
  virtual void createRestart(FILE *fp){} // default don't save values for a restart
  virtual void setFromRestart(FILE *fp){}
  virtual IPVariable* clone() const {return NULL;};
};

class IPVariableMechanics : public IPVariable{
 public:
  IPVariableMechanics(): IPVariable(){}
  IPVariableMechanics(const IPVariableMechanics &source) : IPVariable(source){};
  virtual IPVariableMechanics &operator=(const IPVariable &source){
    IPVariable::operator=(source);
    return *this;
  }
  virtual ~IPVariableMechanics(){}
  virtual double defoEnergy() const{return 0.;}
  virtual double plasticEnergy() const{return 0.;}
  // Default for fracture (a separate class cannot be defined in order to avoid a double derivation IPVariable2ForFracture)
  virtual int fractureEnergy(double* arrayEnergy) const{return 0;}
  virtual bool isbroken() {return false;}
  virtual const bool isbroken() const{return false;}
  virtual void createRestart(FILE *fp){IPVariable::setFromRestart(fp);}
  virtual void setFromRestart(FILE *fp){IPVariable::setFromRestart(fp);}
};
/*
class IPVariableMechanicsWithFracture : virtual public IPVariableMechanics
{
 public:
  IPVariableMechanicsWithFracture(): IPVariableMechanics(){}
  IPVariableMechanicsWithFracture(const IPVariableMechanicsWithFracture &source) : IPVariableMechanics(source){};
  virtual IPVariableMechanicsWithFracture &operator=(const IPVariable &source){
    IPVariableMechanics::operator=(source);
    return *this;
  }
  virtual int fractureEnergy(double* arrayEnergy) const=0;
  virtual const bool broken() const=0;
  virtual void createRestart(FILE *fp){IPVariableMechanics::setFromRestart(fp);}
  virtual void setFromRestart(FILE *fp){IPVariableMechanics::setFromRestart(fp);}
};
*/
template<class Tbulk,class Tfrac> class IPVariable2ForFracture : public Tbulk{
 protected:
  Tbulk *_ipvbulk;
  Tfrac *_ipvfrac;
  bool _broken; // To know which law to use
 public:
  IPVariable2ForFracture() : Tbulk(),_ipvbulk(NULL), _ipvfrac(NULL), _broken(false){}
  IPVariable2ForFracture(const IPVariable2ForFracture &source) : Tbulk(source), _broken(source._broken)
  {
    (*_ipvbulk) = (*(static_cast<const IPVariable*>(source._ipvbulk)));
    if(source._ipvfrac != NULL)
      if(_ipvfrac == NULL) _ipvfrac = new Tfrac(*(source._ipvfrac));
      else  (*_ipvfrac) = (*(static_cast<const IPVariable*>(source._ipvfrac)));
  }
  IPVariable2ForFracture& operator=(const IPVariable &source)
  {
    IPVariableMechanics::operator=(source);
    const IPVariable2ForFracture *ipvf =static_cast<const IPVariable2ForFracture *> (&source);
    _broken = ipvf->_broken;
    (*_ipvbulk) = (*(static_cast<const IPVariableMechanics*>(ipvf->_ipvbulk)));
    if(ipvf->_ipvfrac != NULL){
      if(_ipvfrac == NULL) _ipvfrac = new Tfrac(*(ipvf->_ipvfrac));
      else (*_ipvfrac) = (*(static_cast<IPVariableMechanics*>(ipvf->_ipvfrac)));
    }
    else{
      if(_ipvfrac != NULL) delete _ipvfrac;
      _ipvfrac =NULL;
    }
    return *this;
  }
  ~IPVariable2ForFracture()
  {
    if(_ipvbulk != NULL){
      delete _ipvbulk;
    }
    if(_ipvfrac != NULL){
      delete _ipvfrac;
    }
  }
  virtual const bool isbroken() const{return _broken;}
  virtual bool isbroken() {return _broken;}
  void broken(){_broken=true;}
  void nobroken(){_broken=false;}
  Tbulk* getIPvBulk(){return _ipvbulk;}
  const Tbulk* getIPvBulk() const{return _ipvbulk;}
  Tfrac* getIPvFrac(){return _ipvfrac;}
  const Tfrac* getIPvFrac() const{return _ipvfrac;}
  void setIPvBulk(IPVariable *ipv){ _ipvbulk = static_cast<Tbulk*>(ipv);}
  void setIPvFrac(IPVariable *ipv){ _ipvfrac = static_cast<Tfrac*>(ipv);}
 #if defined(HAVE_MPI)
  virtual int numberValuesToCommunicateMPI()const{return 0;}
  // has to return the number of values exactly as the previous one
  virtual void fillValuesToCommunicateMPI(double *arrayMPI)const{}
  virtual void getValuesFromMPI(const double *arrayMPI){}
 #endif // HAVE_MPI
  virtual void createRestart(FILE *fp)
  {
    IPVariableMechanics::createRestart(fp);
    if(_broken)
    {
      fprintf(fp,"%d ",1);
    }
    else
    {
      fprintf(fp,"%d ",0);
    }
  }
  virtual void setFromRestart(FILE *fp)
  {
    IPVariableMechanics::setFromRestart(fp);
    int br;
    fscanf(fp,"%d",&br);
    if(br==1)
    {
      _broken=true;
    }
    else
    {
      _broken=false;
    }
  }
  virtual int fractureEnergy(double* arrayEnergy) const=0;
};

#endif //IPVARIABLE_H_

