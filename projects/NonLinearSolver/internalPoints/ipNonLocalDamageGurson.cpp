//
// Description: storing class for gurson model
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipNonLocalDamageGurson.h"



IPNonLocalDamageGurson::IPNonLocalDamageGurson() : IPVariableMechanics(),  _elasticEnergy(0.), 
                                                   _eplmatrix(0.), _fV(0),  _fVstar(0), _nldfVstar(0),_DFvStarDNldfVstar(1)
{
  ipvJ2IsotropicHardening=NULL;
  ipvCL=NULL;
  ipvgdn = NULL;
  Msg::Error("IPNonLocalDamageGurson::IPNonLocalDamageGurson is not initialized with a Characteritsic IP Variable nor a IP CLength, nor IP Damage Nucleation");
  _Fp=0.;
  _Fp(0,0)=1.;
  _Fp(1,1)=1.;
  _Fp(2,2)=1.;
  _DFvStarDStrain=0;
};

IPNonLocalDamageGurson::IPNonLocalDamageGurson(double fVinitial, const J2IsotropicHardening *j2IH, 
                                                 const CLengthLaw *cll, const GursonDamageNucleation *gdn) : 
                                                    IPVariableMechanics(),  _elasticEnergy(0.), 
                                                 _eplmatrix(0.), _fV(fVinitial),  _fVstar(fVinitial), 
                                                 _nldfVstar(fVinitial), _DFvStarDNldfVstar(1.)
{
  ipvJ2IsotropicHardening=NULL;
  if(j2IH ==NULL) Msg::Error("IPNonLocalDamageGurson::IPNonLocalDamageGurson has no j2IH");
  j2IH->createIPVariable(ipvJ2IsotropicHardening);

  ipvCL=NULL;
  if(cll ==NULL) Msg::Error("IPNonLocalDamageGurson::IPNonLocalDamageGurson has no cll");
  cll->createIPVariable(ipvCL); 

  ipvgdn=NULL;
  if(gdn ==NULL) Msg::Error("IPNonLocalDamageGurson::IPNonLocalDamageGurson has no gurson nucleation");
  gdn->createIPVariable(ipvgdn); 

  if(fVinitial <0. or fVinitial >1.) Msg::Error("IPNonLocalDamageGurson::IPNonLocalDamageGurson wrong initial porosity");
  _Fp=0.;
  _Fp(0,0)=1.;
  _Fp(1,1)=1.;
  _Fp(2,2)=1.;
  _DFvStarDStrain=0;
};

IPNonLocalDamageGurson::IPNonLocalDamageGurson(const IPNonLocalDamageGurson &source) : IPVariableMechanics(source)
{
  if(source.ipvJ2IsotropicHardening != NULL)
  {
    ipvJ2IsotropicHardening = source.ipvJ2IsotropicHardening->clone();
  }
  if(source.ipvCL != NULL)
  {
    ipvCL = source.ipvCL->clone();
  }
  if(source.ipvgdn != NULL)
  {
    ipvgdn = source.ipvgdn->clone();
  }
  _elasticEnergy=source._elasticEnergy;
  _eplmatrix=source._eplmatrix;
  _fV=source._fV;
  _fVstar=source._fVstar;
  _nldfVstar=source._nldfVstar;
  _Fp=source._Fp;
  _DFvStarDStrain=source._DFvStarDStrain;
  _DFvStarDNldfVstar=source._DFvStarDNldfVstar; 
}

IPNonLocalDamageGurson& IPNonLocalDamageGurson::operator=(const IPVariable &source)
{
  IPVariableMechanics::operator=(source);
  const IPNonLocalDamageGurson* src = dynamic_cast<const IPNonLocalDamageGurson*>(&source);
  if(src != NULL)
  {
    if(ipvJ2IsotropicHardening != NULL) delete ipvJ2IsotropicHardening;
    if(src->ipvJ2IsotropicHardening != NULL)
    {
      ipvJ2IsotropicHardening=src->ipvJ2IsotropicHardening->clone();
    }
    if(ipvCL != NULL) delete ipvCL;
    if(src->ipvCL != NULL)
    {
      ipvCL=src->ipvCL->clone();
    }
    if(ipvgdn != NULL) delete ipvgdn;
    if(src->ipvgdn != NULL)
    {
      ipvgdn=src->ipvgdn->clone();
    }    
    _elasticEnergy=src->_elasticEnergy;
    _eplmatrix=src->_eplmatrix;
    _fV=src->_fV;
    _fVstar=src->_fVstar;
    _nldfVstar=src->_nldfVstar;
    _Fp=src->_Fp;
    _DFvStarDStrain=src->_DFvStarDStrain;
    _DFvStarDNldfVstar=src->_DFvStarDNldfVstar;    
  }
  return *this;
}

double IPNonLocalDamageGurson::defoEnergy() const
{
  return _elasticEnergy;

}
double IPNonLocalDamageGurson::plasticEnergy() const
{
  if(ipvJ2IsotropicHardening != NULL)
    return ipvJ2IsotropicHardening->getIntegR();
  else
    return 0.;
}

void IPNonLocalDamageGurson::createRestart(FILE *fp)
{
  IPVariableMechanics::createRestart(fp);
  if(ipvJ2IsotropicHardening != NULL)
  {
    fprintf(fp,"%d",1);
    ipvJ2IsotropicHardening->createRestart(fp);
  }
  else
    fprintf(fp,"%d",0);
  if(ipvCL != NULL)
  {
    fprintf(fp,"%d",1);
    ipvCL->createRestart(fp);
  }
  else
    fprintf(fp,"%d",0);
  if(ipvgdn != NULL)
  {
    fprintf(fp,"%d",1);
    ipvgdn->createRestart(fp);
  }
  else
    fprintf(fp,"%d",0);
  fprintf(fp,"%e %e %e %e %e %e",_elasticEnergy,_eplmatrix,_fV,_fVstar,_nldfVstar,_DFvStarDNldfVstar);  
  
  fprintf(fp,"%e %e %e %e %e %e %e %e %e",_Fp(0,0),_Fp(0,1),_Fp(0,2),_Fp(1,0),_Fp(1,1),_Fp(1,2),
          _Fp(2,0),_Fp(2,1),_Fp(2,2));

  fprintf(fp,"%e %e %e %e %e %e %e %e %e",_DFvStarDStrain(0,0),_DFvStarDStrain(0,1),_DFvStarDStrain(0,2),
                                          _DFvStarDStrain(1,0),_DFvStarDStrain(1,1),_DFvStarDStrain(1,2),
                                          _DFvStarDStrain(2,0),_DFvStarDStrain(2,1),_DFvStarDStrain(2,2));
}
void IPNonLocalDamageGurson::setFromRestart(FILE *fp)
{
  IPVariableMechanics::setFromRestart(fp);
  int iph=0;
  fscanf(fp,"%d",&iph);
  if(iph == 1)
  {
    if(ipvJ2IsotropicHardening == NULL) Msg::Error("IPNonLocalDamageGurson::setFromRestart: ipvJ2IsotyropicHardening should be initialized");
    ipvJ2IsotropicHardening->setFromRestart(fp);
  }
  int ipcl=0;
  fscanf(fp,"%d",&ipcl);
  if(ipcl == 1)
  {
    if(ipvCL == NULL) Msg::Error("IPNonLocalDamageGurson::setFromRestart: ipvCL should be initialized");
    ipvCL->setFromRestart(fp);
  }
  int ipgdn=0;
  fscanf(fp,"%d",&ipgdn);
  if(ipgdn == 1)
  {
    if(ipvgdn == NULL) Msg::Error("IPNonLocalDamageGurson::setFromRestart: ipvgdn should be initialized");
    ipvgdn->setFromRestart(fp);
  }  
  fscanf(fp,"%e %e %e %e %e %e",&_elasticEnergy,&_eplmatrix,&_fV,&_fVstar,&_nldfVstar,&_DFvStarDNldfVstar);  
  double a00,a01,a02,a10,a11,a12,a20,a21,a22;
  fscanf(fp,"%e %e %e %e %e %e %e %e %e",&a00,&a01,&a02,&a10,&a11,&a12,&a20,&a21,&a22);
  _Fp(0,0) = a00;
  _Fp(0,1) = a01;
  _Fp(0,2) = a02;
  _Fp(1,0) = a10;
  _Fp(1,1) = a11;
  _Fp(1,2) = a12;
  _Fp(2,0) = a20;
  _Fp(2,1) = a21;
  _Fp(2,2) = a22;
  fscanf(fp,"%e %e %e %e %e %e %e %e %e",&a00,&a01,&a02,&a10,&a11,&a12,&a20,&a21,&a22);
  _DFvStarDStrain(0,0) = a00;
  _DFvStarDStrain(0,1) = a01;
  _DFvStarDStrain(0,2) = a02;
  _DFvStarDStrain(1,0) = a10;
  _DFvStarDStrain(1,1) = a11;
  _DFvStarDStrain(1,2) = a12;
  _DFvStarDStrain(2,0) = a20;
  _DFvStarDStrain(2,1) = a21;
  _DFvStarDStrain(2,2) = a22;

}


