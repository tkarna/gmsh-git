//
// Description: storing class for characteristic length
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "ipCLength.h"

IPCLength::IPCLength(): IPVariable(), cL(0)
{
 for(int i=1; i<3; i++)
   cL(i,i)= 1.e-10;

}

IPCLength::IPCLength(const IPCLength &source) : IPVariable(source)
{
  cL = source.cL;
}

IPCLength &IPCLength::operator=(const IPVariable &source)
{
  IPVariable::operator=(source);
  const IPCLength* src = dynamic_cast<const IPCLength*>(&source);  
  if(src!=NULL)
  {
    cL=src->cL;
  }
  return *this;
}

void IPCLength::createRestart(FILE *fp)
{
  IPVariable::setFromRestart(fp);
  fprintf(fp,"%e %e %e %e %e %e %e %e %e",cL(0,0),cL(0,1),cL(0,2),cL(1,0),cL(1,1),cL(1,2),cL(2,0),cL(2,1),cL(2,2));
}


void IPCLength::setFromRestart(FILE *fp)
{
  IPVariable::setFromRestart(fp);
  double a00,a01,a02,a10,a11,a12,a20,a21,a22;
  fscanf(fp,"%e %e %e %e %e %e %e %e %e",&a00,&a01,&a02,&a10,&a11,&a12,&a20,&a21,&a22);
  cL(0,0) = a00;
  cL(0,1) = a01;
  cL(0,2) = a02;
  cL(1,0) = a10;
  cL(1,1) = a11;
  cL(1,2) = a12;
  cL(2,0) = a20;
  cL(2,1) = a21;
  cL(2,2) = a22;
}

IPZeroCLength::IPZeroCLength() : IPCLength()
{

}
IPZeroCLength::IPZeroCLength(const IPZeroCLength &source) :IPCLength(source)
{

}

IPZeroCLength &IPZeroCLength::operator=(const IPVariable &source)
{
  IPCLength::operator=(source);
  const IPZeroCLength* src = dynamic_cast<const IPZeroCLength*>(&source);  
  if(src!=NULL)
  {
  }
  return *this;
}
void IPZeroCLength::createRestart(FILE *fp)
{
  IPCLength::setFromRestart(fp);
}


void IPZeroCLength::setFromRestart(FILE *fp)
{
  IPCLength::setFromRestart(fp);
}

IPCLength *IPZeroCLength::clone() const
{
  return new IPZeroCLength(*this);
} 

IPIsotropicCLength::IPIsotropicCLength() : IPCLength()
{

}
IPIsotropicCLength::IPIsotropicCLength(const IPIsotropicCLength &source) :IPCLength(source)
{

}

IPIsotropicCLength &IPIsotropicCLength::operator=(const IPVariable &source)
{
  IPCLength::operator=(source);
  const IPIsotropicCLength* src = dynamic_cast<const IPIsotropicCLength*>(&source);  
  if(src!=NULL)
  {
  }
  return *this;
}
void IPIsotropicCLength::createRestart(FILE *fp)
{
  IPCLength::setFromRestart(fp);
}


void IPIsotropicCLength::setFromRestart(FILE *fp)
{
  IPCLength::setFromRestart(fp);
}

IPCLength* IPIsotropicCLength::clone() const
{
  return new IPIsotropicCLength(*this);
} 

IPAnisotropicCLength::IPAnisotropicCLength() : IPCLength()
{

}
IPAnisotropicCLength::IPAnisotropicCLength(const IPAnisotropicCLength &source) :IPCLength(source)
{

}

IPAnisotropicCLength &IPAnisotropicCLength::operator=(const IPVariable &source)
{
  IPCLength::operator=(source);
  const IPAnisotropicCLength* src = dynamic_cast<const IPAnisotropicCLength*>(&source);  
  if(src!=NULL)
  {
  }
  return *this;
}
void IPAnisotropicCLength::createRestart(FILE *fp)
{
  IPCLength::setFromRestart(fp);
}


void IPAnisotropicCLength::setFromRestart(FILE *fp)
{
  IPCLength::setFromRestart(fp);
}

IPCLength* IPAnisotropicCLength::clone() const
{
  return new IPAnisotropicCLength(*this);
} 


IPVariableIsotropicCLength::IPVariableIsotropicCLength() : IPCLength()
{

}
IPVariableIsotropicCLength::IPVariableIsotropicCLength(const IPVariableIsotropicCLength &source) :IPCLength(source)
{

}

IPVariableIsotropicCLength &IPVariableIsotropicCLength::operator=(const IPVariable &source)
{
  IPCLength::operator=(source);
  const IPVariableIsotropicCLength* src = dynamic_cast<const IPVariableIsotropicCLength*>(&source);  
  if(src!=NULL)
  {
  }
  return *this;
}
void IPVariableIsotropicCLength::createRestart(FILE *fp)
{
  IPCLength::setFromRestart(fp);
}


void IPVariableIsotropicCLength::setFromRestart(FILE *fp)
{
  IPCLength::setFromRestart(fp);
}

IPCLength* IPVariableIsotropicCLength::clone() const
{
  return new IPVariableIsotropicCLength(*this);
} 

IPVariableAnisotropicCLength::IPVariableAnisotropicCLength() : IPCLength()
{

}
IPVariableAnisotropicCLength::IPVariableAnisotropicCLength(const IPVariableAnisotropicCLength &source) :IPCLength(source)
{

}

IPVariableAnisotropicCLength &IPVariableAnisotropicCLength::operator=(const IPVariable &source)
{
  IPCLength::operator=(source);
  const IPVariableAnisotropicCLength* src = dynamic_cast<const IPVariableAnisotropicCLength*>(&source);  
  if(src!=NULL)
  {
  }
  return *this;
}
void IPVariableAnisotropicCLength::createRestart(FILE *fp)
{
  IPCLength::setFromRestart(fp);
}


void IPVariableAnisotropicCLength::setFromRestart(FILE *fp)
{
  IPCLength::setFromRestart(fp);
}

IPCLength* IPVariableAnisotropicCLength::clone() const
{
  return new IPVariableAnisotropicCLength(*this);
} 

