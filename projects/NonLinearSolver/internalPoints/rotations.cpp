#ifndef ROTATIONS_C 
#define ROTATIONS_C 1


/*
 *
 * VERSION 30 septembre 2008
 * opérations sur les quaternions testées en comparant avec le programme de Maxime (~/Maxime/..) 	-> ok 
 * changement de repère pour un tenseur symétrique 3x3 testé: convention LD vs convention ID vs Matlab ->ok
	======================================================================
	Définition des angles d''Euler (phi1, PHI, phi2)
	1° Au départ, les deux repères d''axes coincident. On effectue trois rotations successives
	   pour amener un des deux repères à son orientation souhaitée.
	2° On effectue une première rotation d''un angle phi1 autour de x3
	3° On effectue une deuxième rotation d''un angle PHI autour du x1 tourné
	4° On effectue une troisième rotation d''un angle phi2 autour du x3 tourné

	======================================================================
	Définition d''un quaternion 'q'
	Le changement de repère est défini sur base d''un axe de rotation 'n'
	et d''un angle de rotation 'alpha'
	Le quaternion vaut: q(1)= cos(alpha/2)  ; q(i+1)= n(i)*sin(alpha/2)

	==========================================================================
	CONVENTION:  T'ij = Aik*Ajl*Tkl				(MMC convention - different from LD!)
	***************************************************************************/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "rotations.h"

// Calcul de la matrice de rotation 6x6 correspondant aux angles d'euler
// euler: euler angles (phi1,phi,phi2)
// R: rotation matrix (6X6), see Doghri appendix C2
void eul_mat(double* euler, double** R){



}


// Calcul du quaternion correspondant à un ensemble d'angles d'euler */
void eul_q4(double* euler, double* q){

  return;
}

// Calcul de la matrice de rotation correspondant au quaternion q
void q4_mat(double* q, double **mat){

}

// Calcul du quaternion correspondant à la matrice de rotation 'mat'
void mat_q4(double** mat, double* q){
}


//Produit de deux quaternions (=composition de rotations)
//Attention: le produit q1*q2 correspond au produit de matrices mat2.mat1
void q4mult(int Res, double* q_1, double* q_2){
}

//Calcul des angles d''euler correspondant à un quaternions 'r'
void q4_eul(double* r, double* ang){

}

// Calcul de arctan
double Karc(double si,double co){
	
}


//Rotation d''un tenseur d''ordre 2 symétrique
//(i6 décrit la convention pour convertir en vecteur à 6 composantes)
void Krots3(double** rot, double* D0, double* D, int** i6){
  return;
}

//** Rotation d''un tenseur d''ordre 2 anti-symétrique
//** convention: w(1)= W(1,2) ; w(2)= W(1,3) ; w(3)= W(2,3)
void Krotw3(double** rot, double* w1, double* w2, int** i3){
	
}

//** Calcule la matrice de rotation 6x6 (convention d'Issam)
//   correspondant à la matrice P (3x3)
void rmat66(double** P, double** R){
  
}

//** Rotation d'un tenseur d'ordre 2 symétrique stocké selon la convention d'Issam
// préserve la convention d'Issam
void rot2(double** q, double* a, double* res){


}
//** Rotation d'un tenseur d'ordre 4 stocké selon la convention d'Issam
void rot4(double** q, double** T, double** res){
// INPUT: q: matrice de rotation 6x6
// 				T: tenseur d'ordre 4, convention d'Issam
// OUTPUT: res=T'=q*T*q')

}



void eul_mat33(double* euler, double** P){
//input euler angles globle to local, calculate rotation matrix P,3x3 rotation matrix 
// By Ling Wu 3-10-2011

	int i,j;
	double c1,c2,c3,s1,s2,s3;
	double s1c2, c1c2;
	double sq2;
  double fpi = pi/180.;

	sq2 = sqrt(2.);

	c1 = cos(euler[0]*fpi);
	s1 = sin(euler[0]*fpi);

	c2 = cos(euler[1]*fpi);
	s2 = sin(euler[1]*fpi);
	
	c3 = cos(euler[2]*fpi);
	s3 = sin(euler[2]*fpi);
	
	s1c2 = s1*c2;
	c1c2 = c1*c2;

	P[0][0] = c3*c1 - s1c2*s3; 
	P[0][1] = c3*s1 + c1c2*s3;
	P[0][2] = s2*s3;

	P[1][0] = -s3*c1 - s1c2*c3;
	P[1][1] = -s3*s1 + c1c2*c3;
	P[1][2] = s2*c3;

	P[2][0] = s1*s2;
	P[2][1] = -c1*s2;
	P[2][2] = c2;

}




void rot33(double** P, double** T, double** res){
// INPUT: P: matrice de rotation 3x3
//  T: 3x3
// OUTPUT: res=T'=P*T*P'       By Ling Wu 3-10-2011

int i,j, k,l;

for(i=0;i<3;i++){
  for(j=0;j<3;j++){
	   res[i][j]=0.;
	}
}

for(i=0;i<3;i++){
   for(j=0;j<3;j++){
	    for(k=0;k<3;k++){
			  for(l=0;l<3;l++){
				   res[i][j] = res[i][j] + P[i][k]*P[j][l]*T[k][l];
				}
			}
	 }
}

}




#endif
