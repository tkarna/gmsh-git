//
// C++ Interface: Quadrature Rule
//
// Description: Define 1D Gauss-Lobatto quadrature rule (number of point 3 to 6)
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "GaussLobattoQuadrature.h"
// As this rule considers point at extremities the minimum number of point is 3

// order is replaced by getting directly the number of point
IntPt *getGaussLobattoQuadrature2DPts(int order)
{
  switch(order)
  {
   default:
     Msg::Error("The Gauss-Lobatto Quadrature rules in 2D is not defined for %d integration points",order);
     return NULL;
  }
}

int getNGaussLobattoQuadrature2DPts(int order)
{
  switch(order)
  {
   default:
     Msg::Error("The Gauss-Lobatto Quadrature rules in 2D is not defined for %d integration points",order);
     return 0;
  }
}

// As this rule considers point at extremities the minimum number of point is 3
const double xglq1D3[3] = {-1.,0., 1.};
const double wglq1D3[3] = {1./3., 4./3., 1./3.};
IntPt GaussLobattoQuadrature1D3[3] = {
  { {xglq1D3[0], 0., 0.} , wglq1D3[0] },
  { {xglq1D3[1], 0., 0.} , wglq1D3[1] },
  { {xglq1D3[2], 0., 0.} , wglq1D3[2] }
};

const double xglq1D4[4] = {-1. , -0.2*sqrt(5), 0.2*sqrt(5), 1.};
const double wglq1D4[4] = { 1./6., 5./6., 5./6. ,1./6.};
IntPt GaussLobattoQuadrature1D4[4] = {
  { {xglq1D4[0], 0. , 0.}, wglq1D4[0]  },
  { {xglq1D4[1], 0. , 0.}, wglq1D4[1]  },
  { {xglq1D4[2], 0. , 0.}, wglq1D4[2]  },
  { {xglq1D4[3], 0. , 0.}, wglq1D4[3]  }
};

const double xglq1D5[5] = { -1. , -1./7.*sqrt(21.), 0 , 1./7.*sqrt(21.), 1.};
const double wglq1D5[5] = { 0.1, 49./90. , 32./45. , 49./90., 0.1};
IntPt GaussLobattoQuadrature1D5[5] = {
  { {xglq1D5[0], 0.,  0.} , wglq1D5[0] },
  { {xglq1D5[1], 0.,  0.} , wglq1D5[1] },
  { {xglq1D5[2], 0.,  0.} , wglq1D5[2] },
  { {xglq1D5[3], 0.,  0.} , wglq1D5[3] },
  { {xglq1D5[4], 0.,  0.} , wglq1D5[4] }
};

const double xglq1D6[6] = { -1. , -sqrt(1./21.*(7.+2.*sqrt(7))), -sqrt(1./21.*(7.-2.*sqrt(7))), sqrt(1./21.*(7.-2.*sqrt(7))), sqrt(1./21.*(7.+2.*sqrt(7))), 1.};
const double wglq1D6[6] = { 1./15., 1./30.*(14.-sqrt(7)), 1./30.*(14.+sqrt(7)), 1./30.*(14.+sqrt(7)), 1./30.*(14.-sqrt(7)), 1./15.};
IntPt GaussLobattoQuadrature1D6[6] = {
  { {xglq1D6[0], 0.,  0.} , wglq1D6[0] },
  { {xglq1D6[1], 0.,  0.} , wglq1D6[1] },
  { {xglq1D6[2], 0.,  0.} , wglq1D6[2] },
  { {xglq1D6[3], 0.,  0.} , wglq1D6[3] },
  { {xglq1D6[4], 0.,  0.} , wglq1D6[4] },
  { {xglq1D6[5], 0.,  0.} , wglq1D6[5] }
};

IntPt * GaussLobattoQuadrature1D[7] = {NULL,NULL,NULL,GaussLobattoQuadrature1D3,GaussLobattoQuadrature1D4,GaussLobattoQuadrature1D5,GaussLobattoQuadrature1D6};
int GaussLobattoQuadrature1DnPt[7] = {0,0,0,3,4,5,6};

// order is replaced by getting directly the number of point
IntPt *getGaussLobattoQuadrature1DPts(int order)
{
  int npts = (order+1)/2;
  switch(npts)
  {
   case 3:
    return GaussLobattoQuadrature1D[3];
   case 4:
    return GaussLobattoQuadrature1D[4];
   case 5:
    return GaussLobattoQuadrature1D[5];
   case 6:
    return GaussLobattoQuadrature1D[6];
   default:
     Msg::Error("The Gauss-Lobatto Quadrature rules in 1D is not defined for %d integration points",npts);
     return NULL;
  }
}

int getNGaussLobattoQuadrature1DPts(int order)
{
  int npts = (order+1)/2;
  switch(npts)
  {
   case 3:
    return GaussLobattoQuadrature1DnPt[3];
   case 4:
    return GaussLobattoQuadrature1DnPt[4];
   case 5:
    return GaussLobattoQuadrature1DnPt[5];
   case 6:
    return GaussLobattoQuadrature1DnPt[6];
   default:
     Msg::Error("The Gauss-Lobatto Quadrature rules in 1D is not defined for %d integration points",npts);
     return 0;
  }
}

// integration on a point
const double xglq0D1 = 0.;
const double wglq0D1 = 1.;
IntPt GaussLobattoQuadrature0D1[1] = {{ {xglq0D1 , 0. , 0.} , wglq0D1 }};

IntPt * GaussLobattoQuadrature0D[1] = {GaussLobattoQuadrature0D1};
int GaussLobattoQuadrature0DnPt[1] = { 1.};

IntPt *getGaussLobattoQuadrature0DPts(int order)
{
  if( order == 0)
    return GaussLobattoQuadrature0D[0];
  Msg::Error("The Gauss-Lobatto integration rules for 0D is not defined for %d",order);
  return NULL;
}

int getNGaussLobattoQuadrature0DPts(int order)
{
  if(order == 0)
    return GaussLobattoQuadrature0DnPt[0];
  Msg::Error("The Gauss-Lobatto integration rules for 0D is not defined for %d",order);
  return 0;
}
