#ifndef pi
#define pi 3.141592654  
#endif

#ifndef ROTATIONS_H 
#define ROTATIONS_H 1

void eul_mat(double* euler, double** R);
void eul_q4(double* euler, double* q);
void q4_mat(double* q, double **mat);
void mat_q4(double** mat, double* q);
void q4mult(int Res, double* q_1, double* q_2);
void q4_eul(double* r, double* ang);
double Karc(double si,double co);
void Krots3(double** rot, double* D0, double* D, int** i6);
void Krotw3(double** rot, double* w1, double* w2, int** i3);
void rmat66(double** P, double** R);
void rot2(double** R, double* a, double* res);
void rot4(double** R, double** T, double** res);
void eul_mat33(double* euler, double** P);
void rot33(double** P, double** T, double** res);


#endif
