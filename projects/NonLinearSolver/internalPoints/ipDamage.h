//
// C++ Interface: terms
//
// Description: Define damage variable for non local j2 plasticity
//
//
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPDAMAGE_H_
#define IPDAMAGE_H_
#include <stdlib.h>
#include <stdio.h>
#include "GmshConfig.h"
#include "STensor3.h"
#include "ipvariable.h"

class IPDamage : public IPVariable{
  protected:
   double D, maximalp, DeltaD, dDdp;
   STensor3 dDdFe;
 public:
  IPDamage();
  IPDamage(const IPDamage &source);
  virtual IPDamage &operator=(const IPVariable &source);
  virtual ~IPDamage(){}
  virtual void createRestart(FILE *fp);
  virtual void setFromRestart(FILE *fp);
  virtual double getDamage() const {return D;}
  virtual double getMaximalP() const {return maximalp;}
  virtual double getDeltaDamage() const {return DeltaD;}
  virtual double getDDamageDp() const {return dDdp;}
  virtual const STensor3 &getConstRefToDDamageDFe() const {return dDdFe;}

  virtual void setValues(const double &_dam, const double &_mp, const double &_DD,
                         const double &_dDdp, const STensor3 &_dDdFe) 
  {
    D        = _dam;
    maximalp = _mp;
    DeltaD   = _DD;
    dDdp     = _dDdp;
    dDdFe    = _dDdFe;
  }

  virtual IPDamage* clone() const=0;
};

class IPLemaitreChabocheDamage : public IPDamage
{

 protected:
 
 public:
  IPLemaitreChabocheDamage();
  IPLemaitreChabocheDamage(const IPLemaitreChabocheDamage &source);
  virtual IPLemaitreChabocheDamage &operator=(const IPVariable &source);
  virtual ~IPLemaitreChabocheDamage(){}
  virtual void createRestart(FILE *fp);
  virtual void setFromRestart(FILE *fp);
  virtual IPDamage* clone() const; 
  
};

class IPPowerLaw : public IPDamage
{

 protected:
 
 public:
  IPPowerLaw();
  IPPowerLaw(const IPPowerLaw &source);
  virtual IPPowerLaw &operator=(const IPVariable &source);
  virtual ~IPPowerLaw(){}
  virtual void createRestart(FILE *fp);
  virtual void setFromRestart(FILE *fp);
  virtual IPDamage* clone() const; 
  
};

#endif //IPDamage_H_

