//
// Description: storing class for j2 elasto-plastic law with non-local damag interface
// Author:  <L. Noels>, (C) 2013
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef IPNONLOCALDAMAGEJ2HYPER_H_
#define IPNONLOCALDAMAGEJ2HYPER_H_
#include "ipJ2linear.h"
#include "ipHardening.h"
#include "ipCLength.h"
#include "ipDamage.h"
#include "CLengthLaw.h"
#include "DamageLaw.h"

class IPNonLocalDamageJ2Hyper : public IPJ2linear
{
 protected:
  IPCLength* ipvCL;
  IPDamage*  ipvDam;

 protected: // All data public to avoid the creation of function to access

  STensor3	_nldJ2HyperDLocalPlasticStrainDStrain;

// bareps
  double _nldJ2HyperEffectivePlasticStrain;
  double _nldJ2HyperDLocalPlasticStrainDNonLocalPlasticStrain;
  

 public:
  IPNonLocalDamageJ2Hyper();
  IPNonLocalDamageJ2Hyper(const J2IsotropicHardening *j2IH, const CLengthLaw *cll, const DamageLaw *dl);
  ~IPNonLocalDamageJ2Hyper()
  {
    if(ipvCL != NULL) 
    {
      delete ipvCL;
    }
    if(ipvDam !=NULL)
    {
      delete ipvDam;
    }
  }


  IPNonLocalDamageJ2Hyper(const IPNonLocalDamageJ2Hyper &source);
  IPNonLocalDamageJ2Hyper& operator=(const IPVariable &source);
  virtual void createRestart(FILE *fp); // default don't save values for a restart
  virtual void setFromRestart(FILE *fp);
  virtual double getCurrentPlasticStrain() const { return _j2lepspbarre;}
  virtual double &getRefToCurrentPlasticStrain() { return _j2lepspbarre;}
  virtual double getEffectivePlasticStrain() const { return _nldJ2HyperEffectivePlasticStrain;}
  virtual double &getRefToEffectivePlasticStrain() { return _nldJ2HyperEffectivePlasticStrain;}
  
  virtual double &getRefToDLocalPlasticStrainDNonLocalPlasticStrain()
  { 
    return _nldJ2HyperDLocalPlasticStrainDNonLocalPlasticStrain;
  }
  virtual double getDLocalPlasticStrainDNonLocalPlasticStrain() const
  { 
    return _nldJ2HyperDLocalPlasticStrainDNonLocalPlasticStrain;
  }

  virtual const STensor3 & getConstRefToDLocalPlasticStrainDStrain() const
  {
    return _nldJ2HyperDLocalPlasticStrainDStrain;
  }  
  virtual STensor3 & getRefToDLocalPlasticStrainDStrain()
  {
    return _nldJ2HyperDLocalPlasticStrainDStrain;
  }

  virtual const IPCLength &getConstRefToIPCLength() const 
  {
    if(ipvCL==NULL)
      Msg::Error("IPNonLocalDamageJ2Hyper: ipvCL not initialized");
    return *ipvCL;
  }
  virtual IPCLength &getRefToIPCLength() 
  {
    if(ipvCL==NULL)
      Msg::Error("IPNonLocalDamageJ2Hyper: ipvCL not initialized");
    return *ipvCL;
  }
  virtual const STensor3 &getConstRefToCharacteristicLength() const
  {
    if(ipvCL==NULL)
      Msg::Error("IPNonLocalDamageJ2Hyper: ipvCL not initialized");
    return ipvCL->getConstRefToCL();
  }
  virtual STensor3 &getRefToCharacteristicLength() const
  {
    if(ipvCL==NULL)
      Msg::Error("IPNonLocalDamageJ2Hyper: ipvCL not initialized");
    return ipvCL->getRefToCL();
  }

  virtual const IPDamage &getConstRefToIPDamage() const 
  {
    if(ipvDam==NULL)
      Msg::Error("IPNonLocalDamageJ2Hyper: ipvDam not initialized");
    return *ipvDam;
  }
  virtual IPDamage &getRefToIPDamage() 
  {
    if(ipvDam==NULL)
      Msg::Error("IPNonLocalDamageJ2Hyper: ipvDam not initialized");
    return *ipvDam;
  }

  virtual double getDamage() const 
  {
    if(ipvDam==NULL)
      Msg::Error("IPNonLocalDamageJ2Hyper: ipvDam not initialized");
    return ipvDam->getDamage();
  }
  virtual double getMaximalP() const 
  {
    if(ipvDam==NULL)
      Msg::Error("IPNonLocalDamageJ2Hyper: ipvDam not initialized");
    return ipvDam->getMaximalP();
  }
  virtual double getDeltaDamage() const 
  {
    if(ipvDam==NULL)
      Msg::Error("IPNonLocalDamageJ2Hyper: ipvDam not initialized");
    return ipvDam->getDeltaDamage();
  }
  virtual double getDDamageDp() const 
  {
    if(ipvDam==NULL)
      Msg::Error("IPNonLocalDamageJ2Hyper: ipvDam not initialized");
    return ipvDam->getDDamageDp();
  }
  virtual const STensor3 &getConstRefToDDamageDFe() const 
  {
    if(ipvDam==NULL)
      Msg::Error("IPNonLocalDamageJ2Hyper: ipvDam not initialized");
    return ipvDam->getConstRefToDDamageDFe();
  }

};

#endif // IPNONLOCALJ2HYPER_H_w
