
#include "ipFiniteStrain.h"
#include "highOrderTensor.h"

ipFiniteStrain::ipFiniteStrain():_GPData(NULL), IPVariableMechanics(){}
ipFiniteStrain::ipFiniteStrain(const ipFiniteStrain &source): IPVariableMechanics(source){
  if(source._GPData != NULL) _GPData = new IntPtData<double>(*source._GPData);
  else{
    _GPData = NULL;
  }
};
ipFiniteStrain &ipFiniteStrain::operator=(const IPVariable &source){
  IPVariableMechanics::operator=(source);
  const ipFiniteStrain* src = dynamic_cast<const ipFiniteStrain*>(&source);
  if(src->_GPData != NULL && _GPData==NULL) _GPData = new IntPtData<double>(*src->_GPData);
  else if(src->_GPData != NULL && _GPData!=NULL) _GPData->operator = (*src->_GPData);
  else if(src->_GPData == NULL){
    if (_GPData) delete _GPData;
    _GPData = NULL;
  }
  return *this;
};
ipFiniteStrain::~ipFiniteStrain(){
  if(_GPData!= NULL){
    delete _GPData;
    _GPData = NULL;
  }
}
  // get shape function information from ipvariable
std::vector<TensorialTraits<double>::ValType>& ipFiniteStrain::f(FunctionSpaceBase* space, MElement* ele, IntPt &GP, const bool hessian, const bool thirdd) const {
  if (_GPData == NULL){
    FunctionSpace<double>* sp = dynamic_cast<FunctionSpace<double>*>(space);
    if (sp) _GPData = new IntPtData<double>(sp,ele,GP,hessian,thirdd);
    else Msg::Error("FunctionSpace<double> must be used");
  }
  return _GPData->val;
};
std::vector<TensorialTraits<double>::GradType>& ipFiniteStrain::gradf(FunctionSpaceBase* space, MElement* ele, IntPt &GP, const bool hessian, const bool thirdd) const {
  if (_GPData == NULL){
    FunctionSpace<double>* sp = dynamic_cast<FunctionSpace<double>*>(space);
    if (sp) _GPData = new IntPtData<double>(sp,ele,GP,hessian,thirdd);
    else Msg::Error("FunctionSpace<double> must be used");
  }
  return _GPData->grad;
};

std::vector<TensorialTraits<double>::HessType>& ipFiniteStrain::hessf(FunctionSpaceBase* space, MElement* ele, IntPt &GP, const bool hessian, const bool thirdd) const {
  //if (!hessian) Msg::Warning("Get hessians of shape functions are not valid");
  if (_GPData == NULL){
    FunctionSpace<double>* sp = dynamic_cast<FunctionSpace<double>*>(space);
    if (sp) _GPData = new IntPtData<double>(sp,ele,GP,hessian,thirdd);
    else Msg::Error("FunctionSpace<double> must be used");
  }
  return _GPData->hess;
};

std::vector<TensorialTraits<double>::ThirdDevType>& ipFiniteStrain::thirdDevf(FunctionSpaceBase* space, MElement* ele, IntPt &GP, const bool hessian, const bool thirdd) const {
  //if (!thirdd) Msg::Warning("Get third derivatives of shape functions are not valid");
  if (_GPData == NULL){
    FunctionSpace<double>* sp = dynamic_cast<FunctionSpace<double>*>(space);
    if (sp) _GPData = new IntPtData<double>(sp,ele,GP,hessian,thirdd);
    else Msg::Error("FunctionSpace<double> must be used");
  }
  return _GPData->third;
};

double& ipFiniteStrain::getJacobianDeterminant(FunctionSpaceBase* space, MElement* ele, IntPt &GP, const bool hessian, const bool thirdd) const{
  if (_GPData == NULL){
    FunctionSpace<double>* sp = dynamic_cast<FunctionSpace<double>*>(space);
    if (sp) _GPData = new IntPtData<double>(sp,ele,GP,hessian,thirdd);
    else Msg::Error("FunctionSpace<double> must be used");
  }
  return _GPData->jacobianDet;
};

#if defined(HAVE_MPI)

// using in multiscale analysis with MPI
int ipFiniteStrain::getMacroNumberElementDataSendToMicroProblem() const {return 9;};
int ipFiniteStrain::getMicroNumberElementDataSendToMacroProblem() const {return 9+81;};
void ipFiniteStrain::getMacroDataSendToMicroProblem(double* val) const {
  const STensor3& F = this->getConstRefToDeformationGradient();
  int bufferSize =  9;
  for (int idex = 0; idex < bufferSize; idex++){
    int i,j;
    Tensor23::getIntsFromIndex(idex,i,j);
    val[idex] = F(i,j);
  }
};
void ipFiniteStrain::getMicroDataToMacroProblem(double* val) const {
  int bufferSize = 9+81;
  const STensor3& P = this->getConstRefToFirstPiolaKirchhoffStress();
  const STensor43& L = this->getConstRefToTangentModuli();
  for (int idex = 0; idex< 9; idex++){
    int i,j;
    Tensor23::getIntsFromIndex(idex,i,j);
    val[idex] = P(i,j);
  }
  for (int idex = 9; idex <bufferSize; idex ++){
    int type = idex-9;
    int i,j,k,l;
    Tensor43::getIntsFromIndex(type,i,j,k,l);
    val[idex] = L(i,j,k,l);
  };

};
void ipFiniteStrain::setReceivedMacroDataToMicroProblem(const double* val){
  int bufferSize = 9;
  STensor3& F = this->getRefToDeformationGradient();
  for (int idex = 0; idex < bufferSize; idex++){
    int i,j;
    Tensor23::getIntsFromIndex(idex,i,j);
     F(i,j) = val[idex];
  }
};
void ipFiniteStrain::setReceivedMicroDataToMacroProblem(const double* val){
  int bufferSize = 9+81;
  STensor3& P = this->getRefToFirstPiolaKirchhoffStress();
  STensor43& L = this->getRefToTangentModuli();
  for (int k=0; k<9; k++){
    int i,j;
    Tensor23::getIntsFromIndex(k,i,j);
    P(i,j) = val[k];
  };
  for (int k=9; k<bufferSize; k++){
    int index = k-9;
    int i,j,p,q;
    Tensor43::getIntsFromIndex(index,i,j,p,q);
    L(i,j,p,q) = val[k];
  };
};
#endif // MPI

