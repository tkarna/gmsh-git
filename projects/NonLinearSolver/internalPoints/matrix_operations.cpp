#ifndef MATRIX_OPERATIONS_C
#define MATRIX_OPERATIONS_C 1

#include "matrix_operations.h"
#include "rotations.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


//******************************************************
//**** caculate eigenvalue e, eigenvector u and the derivatives of eigenvaule to the eoriginal matrix dedS
//**** with jacobian iteration, S is symetric real matrix
//**************************************************************************************

int Jacobian_Eigen( int n, double** S, double* e, double** U, double*** dedS)
{

     int i,j,k,l;
     int col, row;
     double t, c, s, theta;
     double max, norm_S;
     double Tol = 1.0e-8;
     int iterm;
     int itmax = 50;

     double** R, **temp;

     
     mallocmatrix(&R,n,n);
     mallocmatrix(&temp,n,n);

     for (i=0; i<n; i++ ){
           U[i][i] = 1.0;
           for (j=i+1; j<n; j++ ){
                U[i][j] = U[j][i]=0.;
           }
     }

     norm_S = 0.0;
     for (i=0; i<n; i++ ){
          for (j=0; j<n; j++ ){
                norm_S = norm_S + S[i][j]*S[i][j];
          }
     }
     norm_S = sqrt(norm_S);

     iterm = 0;

     max = 1.0;
     while( max > Tol * norm_S && iterm < itmax )
     {
        iterm = iterm + 1;
//*** find the maximum abs(component) which is not on diagonal
        row = col = 0;
        max = 0.0;
        for (i=0; i<n; i++ ){
             for (j=i+1; j<n; j++ ){
                 if(fabs(S[i][j]) > max){
                    row = i;
                    col = j;
                    max = fabs(S[i][j]);
                 }
             }
        }

        if (row != col){

             // computer rotation matrix

              theta=(S[col][col]-S[row][row])/(2.0*S[row][col]);

              t = 1.0/(fabs(theta)+sqrt(theta*theta+1.0));

              if (theta<0.) t = -t;

              c = 1.0/sqrt(t*t + 1.0);
              s = c*t;

              for (i=0; i<n; i++ ){
                   R[i][i]=1.0;
                   for (j=i+1; j<n; j++ ){
                        R[i][j] = R[j][i] = 0.0;
                   }
              }

               R[row][row] = R[col][col] = c;
               R[row][col] = s;
               R[col][row] = -s;

               for (i=0; i<n; i++){
                    for (j=0; j<n; j++){
                         temp[i][j] = 0.0;
                          for (k=0; k<n; k++){
                               for (l=0; l<n; l++){
                                        temp[i][j] = temp[i][j] + R[k][i]*S[k][l]*R[l][j];
                               }
                          }
                    }
               }

               for (i=0; i<n; i++){
                    for (j=0; j<n; j++){
                         S[i][j] = temp[i][j];
                         temp[i][j] = 0.0;
                         for (k=0; k<n; k++){
                              temp[i][j] = temp[i][j] + U[i][k]*R[k][j];
                         }
                    }
               }

               for (i=0; i<n; i++){
                    for (j=0; j<n; j++){
                        U[i][j] = temp[i][j];
                    }
               }

          }
     }

       if (max < Tol * norm_S){
              for (int i=0; i<n; i++) e[i] = S[i][i];
       }
       else{
              printf("iteration dosen't converge after maximum iterations\n");
              return 0;
       }

//compute derivatives dedS

        for (i=0; i<n; i++ ){
            for (j=0; j<n; j++ ){
                for (k=0; k<n; k++ ){
                     dedS[i][j][k]=U[j][i]*U[k][i];
                }
            }
       }

       freematrix(R,n);
       freematrix(temp,n);

      return 0;
  
}


/* ********************************************
// ** SUM OF TWO TENSORS OF THE SECOND ORDER **
// ** EACH ONE CAN BE PREVIOUSLY BY A SCALAR **
// ********************************************/

void addtens2 (double a, double *A, double b, double *B, double *res)
{
 

}

/* ********************************************
// ** SUM OF TWO TENSORS OF THE FOURTH ORDER **
// ** EACH ONE CAN BE PREVIOUSLY BY A SCALAR **
// ********************************************/

void addtens4 (double a, double **A, double b, double **B, double **res)
{
 
  
}

/* ********************************************
// ** SUM OF TWO TENSORS OF THE SIXTH ORDER **
// ** EACH ONE CAN BE PREVIOUSLY BY A SCALAR **
// ********************************************/

void addtens6 (double a, double ***A, double b, double ***B, double ***res)
{
 
  
}

/* *************************************************************      
// ** CONTRACTION D'UN TENSEUR D'ORDRE 2 (mis sous forme 6*1) **
// **      ET D'UN TENSEUR D'ORDRE 2 (sous forme 6*1)         **
// **  POUR DONNER UN SCALAIRE   **
// ************************************************************/

double contraction22(double* a, double* b){



}


/* *************************************************************      
// ** CONTRACTION D'UN TENSEUR D'ORDRE 4 (mis sous forme 6*6) **
// **      ET D'UN TENSEUR D'ORDRE 2 (sous forme 6*1)         **
// **  POUR DONNER UN TENSEUR D'ORDRE 2 (mis sous forme 6*1)  **
// ************************************************************/

void contraction42 (double **T4, double *T2, double *Sol)

{


  
}

/* ************************************************************** 
// ** CALCUL DE LA CONTRACTION DE DEUX TENSEURS D'ORDRE 4(6*6) **
// **  RENVOIE UN TENSEUR D'ORDRE 4 SOUS FORME 6*6             **
// **************************************************************/

void contraction44 (double **TA, double **TB, double **Tres)
{
 

}


/* ***************************************************************
 * **  CALCULE LA CONTRACTION D'UN TENSEUR D'ORDRE 6 (6*6*6)    **
   **  AVEC UN TENSEUR D'ORDRE 2 (6*1)                          **
   ***************************************************************/
/** INPUT: T6: a sixth-order tensor    
 *         T2: a second-order tensor
 *         idx: index on which contraction must be preformed
 *  OUTPUT: Tres: a second-order tensor                        */
 
void contraction62 (double*** T6, double* T2, double**Tres, int idx)
{
 

}


/* ************************************************************ 
// ** CALCUL DU PRODUIT TENSORIEL DE DEUX TENSEURS D'ORDRE 2 **
// **  RENVOIE UN TENSEUR D'ORDRE 4 SOUS FORME MATRICIELLE   **
// ***********************************************************/

void produit_tensoriel (double *TA, double *TB, double **Tres)

{

  
}


/*********************************
 * TRANSPOSE UNE MATRICE MxN       **
 * ******************************/
void transpose(double** Tinit, double** Ttrans, int m, int n){

	

}

/*********************************************/
/** Compute the von Mises equivalent stress **/
/*********************************************/

/* October 2003 */
/* New prototype in May 2004 */

double vonmises(double *tens2)
{
 

}

/******************************************/
/** Compute the equivalent strain scalar **/
/******************************************/

/* November 2005 */

double eqstrn(double *tens2)
{
 
}


/*******************************************************************************
// ** COMPUTE THE ISOTROPIC HOOKE'S ELASTIC OPERATOR FROM E AND NU **
********************************************************************************/

/* June 2003
// INPUT : E : Elastic young's modulus in the longitudinal direction (same in other directions if isotropic)
//         nu : Poisson's ratio in the longitudinal direction (same in other directions if isotropic)
// OUTPUT : Isotropic stiffness tensor stored as in book of ID p.555
*/

void compute_Hooke(double E, double nu, double **C)

{
   
}

/*******************************************************************************
// ** COMPUTE THE ANISOTROPIC ELASTIC OPERATOR FROM EX,NUXX and GXX   **
********************************************************************************/

/* Sept 2011,  by Ling Wu
// INPUT : anpr : E1, E2,E3,nu12,nu13,nu23,G12,G13,G23
//         Euler angles for global to local coordinates
// OUTPUT : Anisotropic stiffness tensor in global coordinates (see book of ID p.555)
*/

void compute_AnisoOperator(double *anpr, double *euler, double **C)

{
  double Gama;
  double E1, E2, E3;
  double nu12, nu13, nu23;
  double nu21, nu31, nu32;
  int i,j;

  double** R66;
  double** C_L;   // elastic operator in local coordinate

  mallocmatrix(&C_L,6,6);
  mallocmatrix(&R66,6,6);

  E1 = anpr[0];
  E2 = anpr[1];
  E3 = anpr[2];

  nu12 = anpr[3];
  nu13 = anpr[4];
  nu23 = anpr[5];

  nu21 = nu12*E2/E1;
  nu31 = nu13*E3/E1;
  nu32 = nu23*E3/E2;

  Gama = 1.0/(1.0-nu12*nu21-nu23*nu32-nu31*nu13-2.0*nu21*nu32*nu13);

  for(i=0;i<6;i++) {
    for(j=0;j<6;j++) {
      C_L[i][j]=0.;
    }
  }

  C_L[0][0] = E1/(1.0-nu23*nu32)*Gama;
  C_L[1][1] = E2/(1.0-nu13*nu31)*Gama;
  C_L[2][2] = E3/(1.0-nu12*nu21)*Gama;

  C_L[0][1] = E1*(nu21+nu31*nu23)*Gama;
  C_L[0][2] = E1*(nu31+nu21*nu32)*Gama;
  C_L[1][2] = E2*(nu32+nu12*nu31)*Gama;

  C_L[1][0] = C[0][1];
  C_L[2][0] = C[0][2];
  C_L[2][1] = C[1][2];

  C_L[3][3] = 2.0*anpr[6];   // the order for shear is '12 13 23'
  C_L[4][4] = 2.0*anpr[7];
  C_L[5][5] = 2.0*anpr[8];

  eul_mat(euler,R66);
  transpose(R66,R66,6,6); 			//transpose rotation matrix for inverse transformation
  rot4(R66,C_L,C);				//express elastic operator in global axes

  freematrix(C_L,6);
  freematrix(R66,6);
  
}

/*******************************************************************
// ** MAKE AN ISOTROPIC TENSOR FROM A BULK and SHEAR MODULI   ******
********************************************************************/
//  INTPUT: kappa: a bulk modulus
//	    mu: a shear modulus 
//    OUtPUT: Ciso: an isotropic fourth-order tensor (6x6) 
 
void makeiso(double kappa, double mu, double** Ciso)
{

  
}

/* ******************************************************************************       
// ** EXTRACTION OF THE ISOTROPIC PART OF A FOURTH ORDER TENSOR: GENERAL METHOD **
// *******************************************************************************/
/*
 * "extractiso": Extract the isotropic part of a fourth order tensor.
 * Original: Olivier Pierard (April 2003)
 * Modified: Laurence Brassart (December 2008)
 * INPUT : A fourth order tensor (6*6)
 * OUTPUT : An isotropic fourth order tensor
 */

void extractiso(double** Cani, double** Ciso ){
		


}
/******************************/
/* Copy a vector into another */
/******************************/

/* November 2003 */

void copyvect(double *A, double *B, int size)
{
  int i;

  for (i=0; i<size; i++) {
    B[i] = A[i];
  }
}

/******************************/
/* Copy a matrix into another */
/******************************/

/* November 2003 */

void copymatr(double **A, double **B, int size1, int size2)
{
  int i,j;

  for (i=0; i<size1; i++) {
    for (j=0; j<size2; j++) {
      B[i][j] = A[i][j];
      }
  }
}

/************************************************/
/** Compute the trace of a second order tensor **/
/************************************************/

/* October 2003 */
/* New prototype in May 2004 */

double trace(double *tens2)
{
  int i;
  double tr=0;

  for (i=0;i<3;i++) {
    tr = tr + tens2[i];
  }
  return (tr);
}

/**********************************************************/
/** Extract the deviatoric part of a second order tensor **/
/**********************************************************/


/* October 2003 */

void dev(double *tens2, double *dev_tens2)
{
  int i;
  double tr_tens2=0;

  for (i=0;i<3;i++) {
    tr_tens2 = tr_tens2 + tens2[i];
  }

  for (i=0;i<3;i++) {
    dev_tens2[i] = tens2[i] - tr_tens2/3;
  }

  for (i=3;i<6;i++) {
    dev_tens2[i] = tens2[i];
  }

}


/* *************************************************************       
// ** DECOMPOSITION LU D'UNE MATRICE CAREE D'ORDRE QUELCONQUE **
// *************************************************************/
/*
 * "ludcmp": computes the LU decomposition of a square matrix.
 * Original: Olivier Pierard (april 2003)
 * Source : Numerical recipies in C pp44-50(available from internet)
 * Some modifications about indices were rapported
 */

#define TINY 1.0e-14

void ludcmp(double **a, int n, int *indx, double *d, int *error)
     /* Given a matrix a(n*n), this routine replaces it by the LU decomposition of a rows permutation 
itself.  a and n are input.  a is output containing L and U; indx[1..n] si an output vector that records 
the row permutation effected y the partial pivoting, d is outpus as +/-1 depending on whether the number
of row interchanges was even or odd, respectively.  This routine is used in combination with lubksb to 
solve linear equations or invert a matrix.  error=1 if matrix is singular(but LU decomposition possible).
If one row is null, the function is automatically stopped*/

{

}

/**************************************
** FORWARD AND BACKWARD SUBSTITUTION **
***************************************/

void lubksb (double **a, int n, int *indx, double b[])

     /* Solves the set of n linear equations A.X=b.  Here a(n*n) is input, not as the matrix A but rather 
as its LU decomposition, determined by the routine ludcmp.  indx[1..n] is input as the permutation vector 
returned by ludcmp.  b[1..n] is input as the right-hand side vector , and returns with the solution 
vector X.  a,n, and indx are not modified by this routine and can be left in place for successive calls 
with different right-hand sides b.  This routine takes into account the possibility that b will begin 
with many zero elements, do it is efficient for use in matrix inversion. */

{
 

}



/************************************************
** CALCUL DE L'INVERSE D'UNE MATRICE D'ORDRE n **
*************************************************/
/* 
 * INPUT: **a : the original matrix to invert
 * 	  **y : the inverse matrix
 *	  *error: error flag
 *	  n: size of the square matrix
*/
void inverse (double **a, double **y, int *error, int n)

{
 
}

/***********************************************/
/* Check if a fourth order tensor is isotropic */
/***********************************************/

/* April 2004 - O. Pierard */
/* Input: A: a fourth order tensor
   Output: res: 1 if A is isotropic; 0 otherwise */

int isisotropic(double**A)
{
 
  return 1;
}

/**************************************************
* Memory allocation for a table of pointers (1D) **
* **************************************************/
void mallocvector (double** V, int m)
{
  int i;

  *V = (double*)malloc(m*sizeof(double));
  
  for(i=0;i<m;i++){
     (*V)[i]=0.;
  }
}  

/* ***************************************************       
// ** Memory allocation for a table of pointers (2D) **
// **************************************************/
/*
 * "mallocmatrix": Memory allocation for a table of pointers.
 * INPUT : m : the number of lines(number of boxes for pointers)
 *         n : the number of rows
 *         **A : A declared pointer to pointers not yet initialized
 * OUTPUT : 
 */

void mallocmatrix (double ***A, int m, int n)

{
  int i,j;

  *A = (double**)malloc(m*sizeof(double*));
  for(i=0;i<m;i++){
    (*A)[i] = (double*)malloc(n*sizeof(double));
  }

  for(i=0;i<m;i++) {
    for(j=0;j<n;j++) {
      (*A)[i][j]=0;
    }
  }
}

/* *********************************************
// ** Memory allocation for a "cube" of data  **
// ********************************************/
void malloctens3( double**** A, int m, int n, int o){

  int i,j,k;

  *A = (double***) malloc(m*sizeof(double**));
  for(i=0;i<m;i++){
     (*A)[i] = (double**) malloc(n*sizeof(double*));
     for(j=0;j<n;j++){
        (*A)[i][j] = (double*) malloc (o*sizeof(double));
     }
  }
 
  for(i=0;i<m;i++){
     for(j=0;j<n;j++){
        for(k=0;k<o;k++){
           (*A)[i][j][k]=0.;
        }
     }
  }
}

/***********************************************
 * Memory allocation for a 4D-matrix					**
 * *********************************************/
void malloctens4(double ***** A, int m, int n, int p, int q){

   int i,j,k,l;
	 *A = (double****) malloc(m*sizeof(double***));
	 for(i=0;i<m;i++){
	   (*A)[i] = (double***) malloc(n*sizeof(double**));
		 for(j=0;j<n;j++){
		   (*A)[i][j] = (double**) malloc(p*sizeof(double*));
			 for(k=0;k<p;k++){
			    (*A)[i][j][k] = (double*) malloc(q*sizeof(double));
			 }
		 }
	 }
  for(i=0;i<m;i++){
     for(j=0;j<n;j++){
        for(k=0;k<p;k++){
				   for(l=0;l<q;l++){
              (*A)[i][j][k][l]=0.;
					 }
        }
     }
  }

}


/***********************************************
 * Freeing memory of a table of pointers **
 **********************************************/
/*
 * "freematrix": free memory of a table of pointers
 * INPUT : Tensor which has to be freed, number of lines
 * OUTPUT : nothing; memory cleaned
*/
void freematrix (double **A, int m)

{
  int i;

  for (i=0;i<m;i++) {
    free (A[i]);
  }
  free (A);

}

/************************************************
 *  Freing memory of a table of pointers (3D)  **
 *  *********************************************/
void freetens3 (double ***A, int m, int n)
{
  int i,j;
 
  for(i=0;i<m;i++){
     for(j=0;j<n;j++){
        free (A[i][j]);
        }
     free (A[i]);
  }
  free(A);
}

/************************************************
 *  Freing memory of a table of pointers (4D)  **
 *  *********************************************/
void freetens4 (double ****A, int m, int n, int p)
{
  int i,j, k;
 
  for(i=0;i<m;i++){
     for(j=0;j<n;j++){
		    for(k=0;k<p;k++){
           free (A[i][j][k]);
        }
     free (A[i][j]);
		 }
	free(A[i]);
  }
free(A);
}

/*************************************************/
/** Print on the screen values of a m*1 vector  **/
/************************************************/
void printvector(double *vect, int m)
{
 int i;
 for(i=0;i<m;i++){
    printf("%.9le \t",vect[i]);
 }
 printf("\n");
}

/************************************************/
/** Print on the screen values of a m*n matrix **/
/************************************************/

void printmatr(double **matr4, int m, int n)
{
  int i, j;

  for (i=0; i<m; i++) {
    for (j=0; j<n; j++) {
      printf("%.10le \t ", matr4[i][j]);
    }
    printf("\n");
  }
  printf("\n");
}   

/*************************************************/
/** check if all components of a vector are zero **
 * ***********************************************/
int iszero(double* vec, int size, double tol){

  int i;
  for(i=0;i<size;i++){
     if(fabs(vec[i])>tol){
        return 0;
     }

  }
  return 1;

}

void cleartens2(double* T){

	int i;
	for(i=0;i<6;i++){
		T[i]=0.;	
	}
}

void cleartens4(double** T){

	int i,j;
	for(i=0;i<6;i++){
		for(j=0;j<6;j++){
			T[i][j]=0.;
		}
	}
}
void cleartens6(double*** T){

	int i,j,k;
	for(i=0;i<6;i++){
		for(j=0;j<6;j++){
			for(k=0;k<6;k++){
				T[i][j][k] = 0.;				
			}
		}
	}
}
//creat cg -- charateristic length tensor in globle axis,
void creat_cg(double *pro, double** cg){

      int i;
      double* euler;
      double** P;
      double** T;

      mallocmatrix(&P,3,3);
      mallocmatrix(&T,3,3);
      mallocvector(&euler,3);
     
      for(i=0;i<3;i++){
               T[i][i]= pro[i];
               euler[i]= pro[i+3];

      } 
     eul_mat33(euler, P);
     transpose(P,P,3,3);  
     rot33(P,T, cg);
     
     freematrix(P,3);
     freematrix(T,3);
     free(euler);
		
}


void creat_cg(double pro, double** cg){

     int i, j;
     for(i=0;i<3;i++){
             for(j=0;j<3;j++){
                    cg[i][j]=0.0;
             }
             cg[i][i]=pro;
     }
}


#endif
