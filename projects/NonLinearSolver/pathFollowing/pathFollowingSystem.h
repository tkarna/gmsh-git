#ifndef PATHFOLLOWINGSYSTEM_H_
#define PATHFOLLOWINGSYSTEM_H_

template<class scalar>
class pathFollowingSystem{
  public:
    /**control type 0 - state control, 1- arc-length control and 2 - load control **/
    virtual void setControlType(const int i) = 0;
    virtual int getControlType() const {};
    /**control parameter **/
    virtual double getControlParameter() const = 0;
    /**arc-length increment or load increment depend on control type **/
    virtual void setPseudoTimeIncrement(const double dt) = 0;
    /**state parameter to draw the equilibrium path **/
    virtual double getStateParameter() const = 0;
    virtual void resetControlParameter() {}
    virtual int solvePerturbedSystem(){return 0.;};
};
#endif // PATHFOLLOWINGSYSTEM_H_
