#ifndef PBCPATHFOLLOWINGSYSTEMPETSC_H_
#define PBCPATHFOLLOWINGSYSTEMPETSC_H_

#include "pbcNonLinearSystemPETSc.h"
#include "pathFollowingSystem.h"

template<class scalar>
class pbcPathFollowingSystemPETSC : public pbcNonLinearSystemPETSc<scalar>,
                                    public pathFollowingSystem<scalar>{
#if defined(HAVE_PETSC)
  protected:
    Vec _q; // load vector-> fix during computation
    Vec _qeff; // effective load vector --> change during computation

    Vec _stateStep;  // state step
    Vec _stateStepPrevious; // previous state step

    double _controlParameter; // current control variable
    double _controlParameterPrev; // previous control variable
    double _controlStep; // control step

		double _pseudoTimeIncrement; // current arc-length step
		double _pseudoTimeIncrement_prev;

		double _uScale; // displacement scale to nondimensional constraints
    bool _setScale; // flag scale

		double _aU, _aL; // parameter for control type

		int _iter; //  specify predictor and corrector
		double uScale2; // equal to _uScale^2

  public:
    pbcPathFollowingSystemPETSC(MPI_Comm com = PETSC_COMM_SELF) : pbcNonLinearSystemPETSc<scalar>(com),pathFollowingSystem<scalar>(),
                  _controlParameter(0.),_controlParameterPrev(0.),_controlStep(0), _pseudoTimeIncrement(0.),
                  _aU(1.),_aL(1),_iter(1),_uScale(1.),uScale2(1.),_setScale(false){}
    virtual ~pbcPathFollowingSystemPETSC(){}

    virtual void setControlType(const int i){
      if (i == 0){
        // state control
        _aU = 1.;
        _aL = 0.;
      }
      else if (i == 1){
        // arc  length control
        _aU = 1.;
        _aL = 1.;
      }
      else if (i== 2){
        // load control
        _aU = 0.;
        _aL = 1;
      }
      else{
        Msg::Fatal(" this method is not valid");
      }
    }

    virtual void setPseudoTimeIncrement(const double dt) {
       _pseudoTimeIncrement = dt;
    };

    virtual void addToLoadVector(int row, const scalar& val){
      PetscInt i = row;
      PetscScalar s = val;
      _try(VecSetValues(_q, 1, &i, &s, ADD_VALUES));
    }

    virtual void zeroLoadVector() {
      if (pbcNonLinearSystemPETSc<scalar>::isAllocated()){
        _try(VecAssemblyBegin(_q));
        _try(VecAssemblyEnd(_q));
        _try(VecZeroEntries(_q));
      }
    }

    virtual void allocate(int nbRows){
      pbcNonLinearSystemPETSc<scalar>::allocate(nbRows);
      _try(VecDuplicate(this->_b,&_qeff));
      _try(VecDuplicate(this->_b,&_stateStep));
      _try(VecDuplicate(this->_b,&_stateStepPrevious));
    };

    virtual void allocateConstraintMatrix(int nbcon, int systemSize) {
      pbcNonLinearSystemPETSc<scalar>::allocateConstraintMatrix(nbcon,systemSize);
      _try(VecDuplicate(this->_rc,&_q));
    };

    virtual void clearConstraintMatrix()  {
      if (pbcNonLinearSystemPETSc<scalar>::isAllocatedConstraintMatrix()){
        pbcNonLinearSystemPETSc<scalar>::clearConstraintMatrix();
        _try(VecDestroy(&_q));
      }
 		};


    virtual void clear(){
      if (pbcNonLinearSystemPETSc<scalar>::isAllocated()){
        pbcNonLinearSystemPETSc<scalar>::clear();
        _try(VecDestroy(&_qeff));
        _try(VecDestroy(&_stateStep));
        _try(VecDestroy(&_stateStepPrevious));
      };
    };

    virtual double getControlParameter() const {
      return _controlParameter;
    };
    virtual double getStateParameter() const {
      PetscReal nor;
      VecAssemblyBegin(this->_xsol);
      VecAssemblyEnd(this->_xsol);
      _try(VecNorm(this->_xsol, NORM_2, &nor));
      return nor;
    };

    virtual void nextStep(){
      pbcNonLinearSystemPETSc<scalar>::nextStep();
      _controlParameterPrev = _controlParameter;
      _try(VecCopy(_stateStep,_stateStepPrevious));
      _try(VecAssemblyBegin(_stateStep));
      _try(VecAssemblyEnd(_stateStep));
      _try(VecZeroEntries(_stateStep));
      _controlStep = 0.;
      _iter = 0;
      _pseudoTimeIncrement_prev = _pseudoTimeIncrement;
    }

    virtual void resetUnknownsToPreviousTimeStep(){
      pbcNonLinearSystemPETSc<scalar>::resetUnknownsToPreviousTimeStep();
      _controlParameter = _controlParameterPrev;
      _try(VecAssemblyBegin(_stateStep));
      _try(VecAssemblyEnd(_stateStep));
      _try(VecZeroEntries(_stateStep));
      _controlStep = 0.;
      _iter = 0;
    }

    virtual  void resetControlParameter() {
      _controlParameter = 0.;
      _controlParameterPrev = 0.;
      _controlStep = 0.;
      _setScale = false;

      _try(VecAssemblyBegin(_stateStep));
      _try(VecAssemblyEnd(_stateStep));
      _try(VecZeroEntries(_stateStep));

      _try(VecAssemblyBegin(_stateStepPrevious));
      _try(VecAssemblyEnd(_stateStepPrevious));
      _try(VecZeroEntries(_stateStepPrevious));
    };

    virtual int systemSolve(){
      /**compute new stiffness matrix **/
      if (this->_matrixModified){
        _try(MatDestroy(&this->_a));
        _try(MatAssemblyBegin(this->_K,MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(this->_K,MAT_FINAL_ASSEMBLY));
        _try(MatPtAP(this->_K,this->_Q,MAT_INITIAL_MATRIX,2.,&this->_a));
        _try(MatAXPY(this->_a,this->_scale,this->_CTC,DIFFERENT_NONZERO_PATTERN));

        _try(MatAssemblyBegin(this->_a,MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(this->_a,MAT_FINAL_ASSEMBLY));

        if (!this->_kspAllocated)
          this->_kspCreate();
        _try(KSPSetOperators(this->_ksp, this->_a, this->_a, DIFFERENT_NONZERO_PATTERN));
        this->_matrixModified = false;
      }

      /** compute _qeff **/
      Vec Rq, KRq;
      _try(VecDuplicate(this->_x,&Rq));
      _try(VecDuplicate(this->_x,&KRq));
      _try(MatMult(this->_R,_q,Rq));
      _try(MatMult(this->_K,Rq,KRq));
      _try(MatMult(this->_Q,KRq,_qeff));
      PetscScalar vv = -1./this->_scale;
      _try(VecScale(_qeff,vv));
      _try(MatMultAdd(this->_CT,_q,_qeff,_qeff));
      _try(VecScale(_qeff,this->_scale));

      _try(VecAssemblyBegin(_qeff));
      _try(VecAssemblyEnd(_qeff));


      _try(VecDestroy(&Rq));
      _try(VecDestroy(&KRq));

      if (_iter == 0){
        // solve linear system
        _try(KSPSolve(this->_ksp, this->_qeff, this->_x));
        PetscScalar vTv;
        _try(VecDot(this->_x,this->_x,&vTv));
        if (_setScale == false){
          _uScale = sqrt(vTv);
          uScale2  = vTv;
          _setScale = true;
        }
        PetscScalar A =  _aU*vTv/uScale2+_aL;

        double a1 = _pseudoTimeIncrement/sqrt(A);
        double a2 = -1.*a1;

        PetscScalar uprevTu;
        _try(VecDot(_stateStepPrevious,this->_x,&uprevTu));
        //Msg::Error("cre = %f",uprevTu);
        if (uprevTu>= 0){
          _controlStep = a1;
            _controlParameter += a1;
            _try(VecAXPY(_stateStep,a1,this->_x));
            _try(VecAXPY(this->_xsol,a1,this->_x));

        }
        else{
          _controlStep = a2;
          _controlParameter += a2;
          _try(VecAXPY(_stateStep,a2,this->_x));
          _try(VecAXPY(this->_xsol,a2,this->_x));
        }

      }
      else{
        /** compute right hand side**/
        if (!this->_rhsflag){
          Vec Rrc; // Rrc = R*rc;
          Vec RHS; // RHS = fext - fint + KRrc
          Vec CTrc; //CTrc = CT*rc;
          _try(VecDuplicate(this->_x,&Rrc));
          _try(VecDuplicate(this->_x,&RHS));
          _try(VecDuplicate(this->_x,&CTrc));

          _try(MatMult(this->_R,this->_rc,Rrc));
          _try(MatMult(this->_K,Rrc,RHS));

          _try(VecAXPY(RHS,1.,this->_Fext));
          _try(VecAXPY(RHS,-1.,this->_Fint));

          _try(MatMult(this->_CT,this->_rc,CTrc));
          _try(MatMult(this->_Q,RHS,this->_b));

          PetscScalar minusscale = -1.*this->_scale;
          _try(VecAXPY(this->_b,minusscale,CTrc));

          _try(VecAssemblyBegin(this->_b));
          _try(VecAssemblyEnd(this->_b));
          _try(VecDestroy(&Rrc));
          _try(VecDestroy(&RHS));
          _try(VecDestroy(&CTrc));
          this->_rhsflag = true;
        }

        // solve system
        Vec dr;
        _try(VecDuplicate(this->_x,&dr));
        _try(KSPSolve(this->_ksp, this->_b, dr));
        _try(VecAssemblyBegin(dr));
        _try(VecAssemblyEnd(dr));

        /** solve to find v now is _x**/
        _try(KSPSolve(this->_ksp, this->_qeff, this->_x));

        /** compute A = 1+vT*v/scale^2**/
        PetscScalar A;
        _try(VecDot(this->_x,this->_x,&A));

        A *= _aU;
        A /= uScale2;
        A += _aL;

        /** compute D, E, F**/

        PetscScalar vTdu,drTdr, vTdr, duTdr, duTdu;
        _try(VecDot(this->_x,_stateStep,&vTdu));
        _try(VecDot(dr,dr,&drTdr));
         _try(VecDot(this->_x,dr,&vTdr));
        _try(VecDot(_stateStep,dr,&duTdr));
        _try(VecDot(_stateStep,_stateStep,&duTdu));


        PetscScalar D = _aU*_aU*(vTdr/uScale2)*(vTdr/uScale2) - A*drTdr/uScale2*_aU;
        PetscScalar E = (_aL*_controlStep+ _aU*vTdu/uScale2)*(vTdr/uScale2)*_aU - A*duTdr/uScale2*_aU;
        PetscScalar F = (_aL*_controlStep+ _aU*vTdu/uScale2)*(_aL*_controlStep+ _aU*vTdu/uScale2)
                        -A*(_aU*duTdu/uScale2+_aL*_controlStep*_controlStep - _pseudoTimeIncrement*_pseudoTimeIncrement);

        double delta = E*E -D*F;
        PetscScalar _beta = 1.;
        //Msg::Error("D = %e, E = %e, F = %e",D,E,F);
        if (fabs(D)>0){
          _beta = 0.99*(-E-sqrt(delta))/D;
          //Msg::Error("b1 = %f, b2 = %f",(-E+sqrt(delta))/D,_beta);
          if (_beta>1) _beta = 1.;
        }



        /** du += dr**/
        _try(VecAXPY(_stateStep,_beta,dr));

        /** compute B = dlambda + vT*(du + dr)/scale^2**/

        PetscScalar B;
        _try(VecDot(this->_x,_stateStep,&B));
        B *= _aU;
        B /= uScale2;
        B += (_controlStep*_aL);


        /** compute C **/
        PetscScalar C;
        _try(VecDot(_stateStep,_stateStep,&C));
        C*= _aU;
        C /= uScale2;
        double dlamda2 = _controlStep*_controlStep*_aL;
        dlamda2 -= _pseudoTimeIncrement*_pseudoTimeIncrement;

        C += dlamda2;

        /** solve equation Ax*x + 2*B*x +C = 0 **/
        delta = B*B - A*C;
        //Msg::Error("beta = %f, delta = %f",_beta,delta);
        if (delta>=0.){
          double rdelta = sqrt(delta);
          double a1 = (-1.*B - rdelta)/A;
          double a2 = (-1.*B + rdelta)/A;
          double sum1 = a1*vTdu;
          double sum2 = a2*vTdu;

          double s = 0.;
          if ( sum1 >= sum2){
            s = a1;
          }
          else{
            s = a2;
          }
          _controlStep += s;
          _controlParameter += s;
          _try(VecAXPY(_stateStep,s,this->_x));
          _try(VecAXPY(this->_xsol,_beta,dr));
          _try(VecAXPY(this->_xsol,s,this->_x));
        }
        else{
          Msg::Error("relaxation error delta = %e",delta);
          return 0;
        }

        _try(VecDestroy(&dr));
      }
      _iter++;
      return 1.;
    };

    virtual double normInfRightHandSide() const{
			if (_iter == 0)
        return 0.;
      else
        return pbcNonLinearSystemPETSc<scalar>::normInfRightHandSide();
    }

    virtual double norm0Inf() const{
      PetscReal norm;
      _try(VecNorm(_qeff,NORM_INFINITY,&norm));
      norm *= _controlStep;
      return fabs(norm);
    };

    virtual int solvePerturbedSystem(){
      pbcNonLinearSystemPETSc<scalar>::systemSolve();
      return 1;
    };

#else
  public:
    pbcPathFollowingSystemPETSC(){Msg::Fatal("petsc is required");};
    virtual void setControlType(const int i) {};
    virtual scalar getControlParameter() const {return 0;};
    virtual void setPseudoTimeIncrement(const double dt) {};
    virtual scalar getStateParameter() const {return 0.;};
#endif
};

#endif // PBCPATHFOLLOWINGSYSTEMPETSC_H_
