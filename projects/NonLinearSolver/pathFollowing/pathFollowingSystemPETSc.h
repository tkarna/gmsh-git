#ifndef PATHFOLLOWINGSYSTEMPETSC_H_
#define PATHFOLLOWINGSYSTEMPETSC_H_

#include "pathFollowingSystem.h"
#include "nonLinearSystems.h"

/**
  // arc length control with constraint equation
  // _aU*_stateStep^T*_stateStep/scale2 + _aL*_controlStep*_controlStep = _pseudoTimeIncrement*_pseudoTimeIncrement

**/


template<class scalar>
class pathFollowingSystemPETSC : public nonLinearSystemPETSc<scalar>,
                                  public pathFollowingSystem<scalar>{
  #if defined(HAVE_PETSC)
  protected:
    Vec _q;  // force vector
    Vec _stateStep;  // state step
    Vec _stateStepPrevious;

    double _controlParameter; // current control variable
    double _controlParameterPrev; // previous control variable

    double _controlStep; // control step
		double _pseudoTimeIncrement;

		double _scale; // scale parameter in constraint
		double _aU, _aL;  // parameter for control type

		bool _setScale; // flag scale
		int _iter;
		double scale2; // _scale*_scale

  public:
    pathFollowingSystemPETSC(MPI_Comm com = PETSC_COMM_WORLD):nonLinearSystemPETSc<scalar>(com),
                       pathFollowingSystem<scalar>(),_controlParameterPrev(0.),_controlParameter(0.),
                       _scale(1.),scale2(1.),_controlStep(0),_setScale(false),_iter(0), _aU(1.),_aL(1.),
                       _pseudoTimeIncrement(0.){};
    virtual ~pathFollowingSystemPETSC(){
      this->clear();
    }

    virtual void setControlType(const int i){
      if (i == 0){
        // state control
        _aU = 1.;
        _aL = 0.;
      }
      else if (i == 1){
        // arc_length control
        _aU = 1.;
        _aL = 1.;
      }
      else if (i== 2){
        // load control
        _aU = 0.;
        _aL = 1;
      }
      else{
        Msg::Fatal(" this method is not valid");
      }
    }

    virtual void setPseudoTimeIncrement(const double dt) {
       _pseudoTimeIncrement = dt;
    };

    virtual void allocate(int nbRows){
      nonLinearSystemPETSc<scalar>::allocate(nbRows);
      _try(VecDuplicate(this->_b,&_q));
      _try(VecDuplicate(this->_b,&_stateStep));
      _try(VecDuplicate(this->_b,&_stateStepPrevious));
    };

    virtual void clear(){
      if (nonLinearSystemPETSc<scalar>::isAllocated()){
        nonLinearSystemPETSc<scalar>::clear();
        _try(VecDestroy(&_q));
        _try(VecDestroy(&_stateStep));
        _try(VecDestroy(&_stateStepPrevious));
      };
    };

    virtual void addToLoadVector(int row, const scalar& val){
      PetscInt i = row;
      PetscScalar s = val;
      _try(VecSetValues(_q, 1, &i, &s, ADD_VALUES));
    }

    virtual double getControlParameter() const {
      return _controlParameter;
    };

    virtual double getStateParameter() const {
      PetscReal nor;
      VecAssemblyBegin(this->_xsol);
      VecAssemblyEnd(this->_xsol);
      _try(VecNorm(this->_xsol, NORM_INFINITY, &nor));
      return nor;
    };

    virtual void nextStep(){
      nonLinearSystemPETSc<scalar>::nextStep();
      _controlParameterPrev = _controlParameter;
      _try(VecCopy(_stateStep,_stateStepPrevious));

      _try(VecAssemblyBegin(_stateStep));
      _try(VecAssemblyEnd(_stateStep));
      _try(VecZeroEntries(_stateStep));

      _controlStep = 0.;

      _iter = 0;
    }

    virtual void resetUnknownsToPreviousTimeStep(){
      nonLinearSystemPETSc<scalar>::resetUnknownsToPreviousTimeStep();
      _controlParameter = _controlParameterPrev;

      _try(VecAssemblyBegin(_stateStep));
      _try(VecAssemblyEnd(_stateStep));
      _try(VecZeroEntries(_stateStep));

      _controlStep = 0.;

      _iter = 0;
    }

    virtual int systemSolve(){
      if (_iter == 0){
        /** solve to find v now is _x**/
        _try(VecAssemblyBegin(_q));
        _try(VecAssemblyEnd(_q));
        _try(VecCopy(_q,this->_b));

        linearSystemPETSc<scalar>::systemSolve();

        PetscScalar vTv;
        _try(VecDot(this->_x,this->_x,&vTv));
        if (_setScale == false){
          _scale = sqrt(vTv);
          scale2  = vTv;
          _setScale = true;
        }
        PetscScalar A =  _aU*vTv/scale2+_aL;

        PetscScalar uprevTu;
        _try(VecDot(_stateStepPrevious,this->_x,&uprevTu));
        if (uprevTu<0.){
          _controlStep = -1.*_pseudoTimeIncrement/sqrt(A);;
        }
        else {
          _controlStep = _pseudoTimeIncrement/sqrt(A);;
        }

        // update
        _controlParameter += _controlStep;
        _try(VecAXPY(_stateStep,_controlStep,this->_x));
        _try(VecAXPY(this->_xsol,_controlStep,this->_x));
      }
      else{
        /** solve to find dr and _xsol += _x**/
        if(!this->_flagb){
          #if defined(HAVE_MPI)
          if(Msg::GetCommSize() > 1)
          {
            _try(VecAssemblyBegin(this->_q));
            _try(VecAssemblyEnd(this->_q));
            _try(VecAssemblyBegin(this->_Fint));
            _try(VecAssemblyEnd(this->_Fint));
          }
          #endif // HAVE_MPI
          _try(VecCopy(this->_Fint,this->_b));
          _try(VecScale(this->_b,-1.));

          _try(VecAXPY(this->_b,this->getControlParameter(),this->_q));
          this->_flagb = true;
        }
        // solve system
        linearSystemPETSc<scalar>::systemSolve();

        // copy _x to dr
        Vec dr;
        _try(VecDuplicate(this->_x,&dr));
        _try(VecCopy(this->_x,dr));
        _try(VecAssemblyBegin(dr));
        _try(VecAssemblyEnd(dr));

        /** solve to find v now is _x**/
        _try(VecAssemblyBegin(_q));
        _try(VecAssemblyEnd(_q));
        _try(VecCopy(_q,this->_b));
        linearSystemPETSc<scalar>::systemSolve();

        /** compute A = 1+vT*v/scale^2**/
        PetscScalar A;
        _try(VecDot(this->_x,this->_x,&A));

        A *= _aU;
        A /= scale2;
        A += _aL;

        /** compute D, E, F**/


        PetscScalar vTdu,drTdr, vTdr, duTdr, duTdu;
        _try(VecDot(this->_x,_stateStep,&vTdu));
        _try(VecDot(dr,dr,&drTdr));
        _try(VecDot(this->_x,dr,&vTdr));
        _try(VecDot(_stateStep,dr,&duTdr));
        _try(VecDot(_stateStep,_stateStep,&duTdu));


        PetscScalar D = _aU*_aU*(vTdr/scale2)*(vTdr/scale2) - A*drTdr/scale2*_aU;
        PetscScalar E = (_aL*_controlStep+ _aU*vTdu/scale2)*(vTdr/scale2)*_aU - A*duTdr/scale2*_aU;
        PetscScalar F = (_aL*_controlStep+ _aU*vTdu/scale2)*(_aL*_controlStep+ _aU*vTdu/scale2)
                        -A*(_aU*duTdu/scale2+_aL*_controlStep*_controlStep - _pseudoTimeIncrement*_pseudoTimeIncrement);

        double delta = E*E -D*F;
        PetscScalar _beta = 1.;

        if (fabs(D)>0){
          _beta = 0.99*(-E-sqrt(delta))/D;
          //Msg::Error("b1 = %f, b2 = %f",(-E-sqrt(delta))/D,_beta);
        }
        else
          _beta = F/(2.*E);

        if (_beta>1) _beta = 1.;



        /** du += beta*dr**/
        /** xsol += beta*dr**/
        _try(VecAXPY(_stateStep,_beta,dr));
        _try(VecAXPY(this->_xsol,_beta,dr));

        /** compute B = dlambda + vT*(du + dr)/scale^2**/

        PetscScalar B;
        _try(VecDot(this->_x,_stateStep,&B));
        B *= _aU;
        B /= scale2;
        B += (_controlStep*_aL);


        /** compute C **/
        PetscScalar C;
        _try(VecDot(_stateStep,_stateStep,&C));
        C*= _aU;
        C /= scale2;
        double dlamda2 = _controlStep*_controlStep*_aL - _pseudoTimeIncrement*_pseudoTimeIncrement;

        C += dlamda2;

        /** solve equation Ax*x + 2*B*x +C = 0 **/
        delta = B*B - A*C;
        if (delta <0)  {
          Msg::Error("relaxation error %e ",delta);
          return 0;
        }

        double rdelta = sqrt(delta);
        double a1 = (-1.*B - rdelta)/A;
        double a2 = (-1.*B + rdelta)/A;
        double theta1 = a1*vTdu;
        double theta2 = a2*vTdu;

        double s = 0.;
        if (theta1>=theta2){
          s = a1;
        }
        else
          s = a2;

        _controlStep += s;
        _controlParameter += s;
        _try(VecAXPY(_stateStep,s,this->_x));
        _try(VecAXPY(this->_xsol,s,this->_x));
        _try(VecDestroy(&dr));
      }
      _iter++;
      return 1;
    }

    virtual double normInfRightHandSide() const{
      if(!this->_flagb){
        #if defined(HAVE_MPI)
        if(Msg::GetCommSize() > 1)
        {
          _try(VecAssemblyBegin(this->_q));
          _try(VecAssemblyEnd(this->_q));
          _try(VecAssemblyBegin(this->_Fint));
          _try(VecAssemblyEnd(this->_Fint));
        }
        #endif // HAVE_MPI
        _try(VecCopy(this->_Fint,this->_b));
        _try(VecScale(this->_b,-1.));

        _try(VecAXPY(this->_b,this->getControlParameter(),this->_q));
        this->_flagb = true;
      }
      return linearSystemPETSc<scalar>::normInfRightHandSide();
    }

    virtual double norm0Inf() const{
        PetscReal norq;
        PetscReal norFint;
        _try(VecNorm(_q, NORM_INFINITY, &norq));
        _try(VecNorm(this->_Fint, NORM_INFINITY, &norFint));
        double val = norFint + fabs(this->getControlParameter())*norq;
      return val;
    }

  #else
  public:
    pathFollowingSystemPETSC():nonLinearSystemPETSc<scalar>(),pathFollowingSystem<scalar>(){};
    virtual void setControlType(const int i) {};
    virtual double getControlParameter() const {return 0;};
    virtual void setPseudoTimeIncrement(const double dt) {};
    virtual double getStateParameter() const {return 0.;};
  #endif //HAVE_PETSC
};

#endif // PATHFOLLOWINGSYSTEMPETSC_H_
