//
//
// Description: Specific function for solve problem with mpi
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef _ELEMENTFILTERMPI_H_
#define _ELEMENTFILTERMPI_H_
#include "groupOfElements.h"
#include "MElement.h"
#include "elementFilterBox.h"

// filter to have only the element of the domain in the groupOfElements
class elementFilterMPITrivial : public elementFilter{
 public:
  elementFilterMPITrivial() : elementFilter(){}
  virtual bool operator() (MElement *ele) const{
    if(ele->getPartition() == 0) return true;
    if(ele->getPartition() != Msg::GetCommRank()+1) return false;
    else return true;
  }
};

// add MPI filter to a filter defined by the user
class elementFilterMPIUntrivial : public elementFilter
{
 protected:
  elementFilter *_filter;
 public:
  elementFilterMPIUntrivial(elementFilter *fil) : elementFilter(), _filter(fil){}
  virtual ~elementFilterMPIUntrivial(){}
  virtual bool operator() (MElement *ele) const{
    if(ele->getPartition() != Msg::GetCommRank()+1) return false;
    return _filter->operator()(ele);
  }
};
// and kept the element of other groups in a vector. Usefull for interface creation.
class elementFilterMPI : public elementFilter{
  std::vector<groupOfElements> &_groupOtherPart;
 public :
  elementFilterMPI(std::vector<groupOfElements> &gop) : elementFilter(), _groupOtherPart(gop){}
  virtual bool operator() (MElement *ele) const{
    if(Msg::GetCommSize() == 1) return true;
    if(ele->getPartition()== 0) return true;// for case in mpi where the mesh is not partitioned
    if(ele->getPartition() != Msg::GetCommRank()+1){
      _groupOtherPart[ele->getPartition()-1].insert(ele);
      return false;
    }
    return true;
  }
};
#endif //_ELEMENTFILTERMPI_H_
