//
// C++ Interface: field
//
// Description: Class for a field on elements (specific functions)
// Author:  <Gauthier BECKER>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "elementsField.h"
#include "partDomain.h"
elementsField::elementsField(const std::string &fnn, const uint32_t fms, const int ncomp,
               const std::vector<dataBuildView> &dbview_) : nlsField(fnn,fms,ncomp,dbview_), _dname("ElementData"){}

void elementsField::buildData(FILE* myview, const std::vector<partDomain*> &vdom,const int cc,const ElemValue ev) const
{
  std::vector<double> fieldData;
  for (unsigned int i = 0; i < vdom.size(); ++i)
    for (groupOfElements::elementContainer::const_iterator it = vdom[i]->g->begin(); it != vdom[i]->g->end(); ++it){
      MElement *ele = *it;
      fieldData.resize(numcomp);
      this->get(vdom[i],ele,fieldData,cc,ev);
      fprintf(myview, "%d",ele->getNum());
      for(int j=0;j<numcomp;j++)
        fprintf(myview, " %.16g",fieldData[j]);
      fprintf(myview,"\n");
    }
}

const std::string& elementsField::dataname() const{return _dname;}
