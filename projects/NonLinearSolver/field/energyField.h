//
// C++ Interface: energetic field
//
// Description: Class derived from element field to manage energetic balance
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef ENERGETICFIELD_H_
#define ENERGETICFIELD_H_

#include "elementsField.h"
#include "nonLinearMechSolver.h"
#include "staticDofManager.h"
#include "solverAlgorithms.h"
class energeticField : public elementsField{
 protected:
//  linearSystem<double> *_lsys;
  IPField *_ipf;
  unknownField *_ufield;
  dofManager<double> *_pAssembler;
  staticDofManager<double>* _sAssembler; // can be == NULL but stored to avoid multiple dynamic_cast
  nonLinearSystem<double> *_nlsys; // can be == NULL but stored to avoid multiple dynamic_cast
  std::string _fname; // file to store total energy in function of time (one by part in mpi the sum column by column has to be done in post processing)
  std::vector<partDomain*> &_domvec;
  std::vector<nonLinearNeumannBC> &_vneu;
  int _energyComputation,_fractureEnergyComputation;
  int _systemSizeWithoutRigidContact;
  FILE *fpenergy;
  FILE *fpfrac;
  mutable FilterDofSet _rigidContactFilter; // + mpi ghost dof as bool filterDof::operator() is not a const function !!!!
  mutable std::map<Dof,double> _fextn;
  mutable std::map<Dof,double> _dispn;
  mutable std::map<Dof,double> _fextnp1;
  mutable std::map<Dof,double> _dispnp1;
  mutable double _wext;

 public:
  energeticField(IPField *ipf,
                 unknownField *ufield, dofManager<double> *pAssembler,
                 std::vector<partDomain*> &domvec, nonLinearMechSolver::contactContainer &vcdom, std::vector<nonLinearNeumannBC> &vNeumann,
                 const std::vector<dataBuildView> &dbview_,const int energyComp,const int fracComp);
  ~energeticField()
  {
    if(fpenergy!=NULL) fclose(fpenergy);
    if(fpfrac!=NULL) fclose(fpfrac);
  }

  // functions to compute the different parts of energy
  double kineticEnergy() const; // More efficient than a loop on element thanks to vector operation via PETSC
  double kineticEnergy(MElement *ele, const partDomain *dom) const;
  double deformationEnergy(MElement *ele, const partDomain *dom) const;
  double deformationEnergy() const;
  double plasticEnergy(MElement *ele, const partDomain *dom) const;
  double plasticEnergy() const;
  void externalWork()const; // it is stored in a internal variable _wext
  int fractureEnergy(double* arrayEnergy) const; // int return the number of components to archive (max 12) the first one is the total energy

  void get(partDomain *dom,MElement *ele,std::vector<double> &ener, const int cc,
           const nlsField::ElemValue=nlsField::crude)const;
  void archive(const double time,const int step=1);
};
#endif // ENERGETICFIELD_H_

