//
// C++ Interface: terms
//
// Description: Class with the displacement field
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef UNKNOWNFIELD_H_
#define UNKNOWNFIELD_H_
#include "dofManager.h"
#include "PView.h"
#include "PViewData.h"
#include <stdint.h>
#include <stdlib.h>
#include "nodesField.h"
#include "nonLinearBC.h"
#include "MInterfaceElement.h"
class contactDomain;
template<class T> class staticDofManager;
class unknownField : public nodesField{
 public:
  struct archiveNode{
    long int nodenum;
    Dof D;
    int _comp;
    int nstep; // number of step between to archive
    nonLinearBoundaryCondition::whichCondition wc;
    FILE *fp;
    bool iscontact;
    // "solver" constructor
    archiveNode(const int nnum,const int comp,const nonLinearBoundaryCondition::whichCondition wv=nonLinearBoundaryCondition::position,
                const int nstep_=1,bool isc=false ) : nodenum(nnum), _comp(comp), wc(wv), nstep(nstep_), fp(NULL),
                                                      iscontact(isc), D(0,0) {} // D is initialized after
    // "internal unknownField" constructor
    archiveNode(Dof R, int n, int comp,nonLinearBoundaryCondition::whichCondition wv=nonLinearBoundaryCondition::position,
                int nstep_=1,bool isc=false) : nodenum(n), D(R),_comp(comp), wc(wv), nstep(nstep_), iscontact(isc),fp(NULL)
    {
       // open File
       std::ostringstream oss;
       oss << nodenum;
       std::string s = oss.str();
       // component of displacement
       oss.str("");
       oss << comp;
       std::string s2 = oss.str();
       #if defined(HAVE_MPI)
       if(Msg::GetCommSize() != 1){
         oss.str("");
         oss << Msg::GetCommRank();
         s2 += "_part"+oss.str();
       }
       #endif // HAVE_MPI
       std::string fname;
       switch(wc){
        case nonLinearBoundaryCondition::position:
         fname = "NodalDisplacement"+s+"comp"+s2+".csv";
         break;
        case nonLinearBoundaryCondition::velocity:
         fname = "NodalVelocity"+s+"comp"+s2+".csv";
         break;
        case nonLinearBoundaryCondition::acceleration:
         fname = "NodalAcceleration"+s+"comp"+s2+".csv";
         break;
       }
       fp = fopen(fname.c_str(),"a");
    }
    archiveNode(const archiveNode &source) : nodenum(source.nodenum), D(source.D), _comp(source._comp), nstep(source.nstep),
                                             wc(source.wc), fp(source.fp), iscontact(source.iscontact){}
    ~archiveNode(){};
    void closeFile(){fclose(fp);}
  };
 protected:
  dofManager<double> *pAssembler; // To access to component of equation system template this
  staticDofManager<double>* _dynassembler; // can be == NULL but avoid multiple dynamic_cast
  std::vector<archiveNode> varch;
  std::vector<partDomain*> *_vdom;
  std::set<contactDomain*> *_allContact;
  virtual void buildData(FILE* myview,const std::vector<partDomain*> &vdom,const int cc,const ElemValue ev) const;
 public:
  // update all displacement value
  unknownField(dofManager<double> *pas, std::vector<partDomain*> &elas, std::vector<partDomain*> &gdom,std::set<contactDomain*> *acontact,
                      const int nc, std::vector<archiveNode> &archiving,
                      const std::vector<nlsField::dataBuildView> &dbview_, const std::string="disp.msh");
  virtual ~unknownField();
  // get Operation
  virtual void get(Dof &D,double &udof) const;
  virtual void get(Dof &D, double &udof, nonLinearBoundaryCondition::whichCondition wv) const ;
  virtual void get(std::vector<Dof> &R, std::vector<double> &disp) const;
  virtual void get(std::vector<Dof> &R, fullVector<double> &disp) const;
  virtual void get(std::vector<Dof> &R, std::vector<double> &disp,nonLinearBoundaryCondition::whichCondition wv) const ;
  virtual void get(std::vector<Dof> &R, fullVector<double> &disp,nonLinearBoundaryCondition::whichCondition wv) const ;

  // For archiving
  virtual void get(partDomain *dom, MElement *ele,std::vector<double> &udofs, const int cc,
                   const ElemValue ev=crude)const;
  virtual void archive(const double time,const int step=1);
  virtual void valuesForOnelab(fullVector<double> &nodalValues) const;
};
#endif // _UNKNOWNFIELD_H_
