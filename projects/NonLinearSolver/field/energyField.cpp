//
// C++ Interface: energetic field
//
// Description: Class derived from element field to manage energetic balance
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "energyField.h"
#include "explicitHulbertChung.h"
#include "ipField.h"
#include "SVector3.h"
#include "unknownField.h"
#include "dofManagerMultiSystems.h"
double dot(const std::vector<double> &a, const fullVector<dofManager<double>::dataMat> &b){
  double c = 0.;
  for(int i =0; i<a.size(); i++){
    c += a[i]*b(i);
  }
  return c;
}

double dot(const std::vector<double> &a, const std::vector<double> &b){
  double c=0.;
  for(int i =0; i<a.size(); i++){
    c += a[i]*b[i];
  }
  return c;
}

energeticField::energeticField(IPField *ipf,unknownField *ufield, dofManager<double> *pAssembler,
                               std::vector<partDomain*> &domvec, nonLinearMechSolver::contactContainer &vcdom,
                               std::vector<nonLinearNeumannBC> &vNeumann,const std::vector<dataBuildView> &dbview_,
                               const int energyComp,const int fracComp) : elementsField("energy.msh",100000000,1,dbview_),
                                                       //_lsys(lsys),
                                                       _ipf(ipf), _ufield(ufield), _pAssembler(pAssembler),
                                                       _energyComputation(energyComp),_fractureEnergyComputation(fracComp),
                                                       _sAssembler(dynamic_cast<staticDofManager<double>*>(pAssembler)),
                                                       _domvec(domvec), _vneu(vNeumann),_wext(0.),
                                                       _rigidContactFilter(),fpenergy(NULL), _systemSizeWithoutRigidContact(0), fpfrac(NULL)
{
    std::string fracname;
    #if defined(HAVE_MPI)
    if(Msg::GetCommSize() > 1){
      std::ostringstream oss;
      oss << Msg::GetCommRank();
      _fname = "energy_part" + oss.str() + ".csv";
      fracname = "fractureEnergy_part" + oss.str() + ".csv";
    }
    else
    #endif // HAVE_MPI
    {
      _fname = "energy.csv";
      fracname = "fractureEnergy.csv";
    }
    if(_energyComputation>0)
    {
      // initialize file to store energy
      fpenergy = fopen(_fname.c_str(),"w");
      fprintf(fpenergy,"Time;Kinetic;Deformation;Plastic;Wext;Total\n");
      fprintf(fpenergy,"0.;0.;0.;0.;0.;0.\n"); // false if initial deformation FIX IT HOW ??
    }
    else
    {
      // remove file
      std::string rfname = "rm -rf "+_fname;
      system(rfname.c_str());
    }
    if(_fractureEnergyComputation>0)
    {
      fpfrac = fopen(fracname.c_str(),"w");
      fprintf(fpfrac,"Time;Total;Array of value\n");
    }
    else
    {
      std::string rfname = "rm -rf "+fracname;
      system(rfname.c_str());
    }
    // set system size without rigid contact
    if(_sAssembler !=NULL)
    {
      _systemSizeWithoutRigidContact = _sAssembler->getFirstRigidContactUnknowns();
    }
    else
    {
      _systemSizeWithoutRigidContact = pAssembler->sizeOfR();
    }
    // avoid this HOW ??
    long int nelem=0;
    for(std::vector<partDomain*>::iterator itdom=_domvec.begin(); itdom!=_domvec.end();++itdom){
      partDomain *dom = *itdom;
      nelem+=dom->g->size();
    }
    this->setTotElem(nelem);

    // init non linear system to get the external work
    std::string Aname("A");
    dofManagerMultiSystems<double>* multiDof = dynamic_cast<dofManagerMultiSystems<double>*>(_pAssembler);
    linearSystem<double> *lsys;
    if(multiDof ==NULL)
    {
      lsys = _pAssembler->getLinearSystem(Aname);
    }
    else
    {
      lsys = multiDof->getManager(0)->getLinearSystem(Aname);
    }
    _nlsys = dynamic_cast<nonLinearSystem<double>*>(lsys);
    if(_nlsys!=NULL)
    {
      // Wext for prescribed displacement (but without rigid contact body)
      std::vector<Dof> R;
      for(nonLinearMechSolver::contactContainer::iterator itC = vcdom.begin(); itC!=vcdom.end();++itC)
      {
        contactDomain *cdom = *itC;
        rigidContactSpaceBase *rspace = static_cast<rigidContactSpaceBase*>(cdom->getSpace());
        rspace->getKeysOfGravityCenter(R);
        _rigidContactFilter.addDof(R);
      }
      R.clear();
      _pAssembler->getFixedDof(R);
      for(int i=0; i<R.size(); i++){
        if(!_rigidContactFilter(R[i])){
         #if defined(HAVE_MPI)
          if(R[i].getType()<0)  // dof another rank
          {
            _rigidContactFilter.addDof(R[i]);
          }
          else
         #endif // HAVE_MPI
          {
            _fextn.insert(std::pair<Dof,double>(R[i],0.));
            _dispn.insert(std::pair<Dof,double>(R[i],0.));
            _fextnp1.insert(std::pair<Dof,double>(R[i],0.));
            _dispnp1.insert(std::pair<Dof,double>(R[i],0.));
          }
        }
      }
    }
    else
    {
      Msg::Error("Wext cannot be computed for a linear system! The value will be set to 0");
    }

  }


double energeticField::kineticEnergy() const {
  if(_sAssembler == NULL)  return 0.; // system is not dynamic --> kinetic energy = 0
  else return _sAssembler->getKineticEnergy(_systemSizeWithoutRigidContact,_rigidContactFilter);
}

double energeticField::kineticEnergy(MElement *ele, const partDomain *dom) const{
  double ener=0.;
  if(_sAssembler != NULL){
    // get Dof
    std::vector<Dof> R;
    std::vector<double> velocities;
    std::vector<double> vmass;
    FunctionSpaceBase *sp = dom->getFunctionSpace();
    sp->getKeys(ele,R);
    int nkeys = sp->getNumKeys(ele);
    _sAssembler->getDofValue(R,velocities,nonLinearBoundaryCondition::velocity);
    _sAssembler->getVertexMass(R,vmass);
    for(int i=0; i!=nkeys; i++)
      ener+=vmass[i]*velocities[i]*velocities[i];
    R.clear(); velocities.clear(); vmass.clear();
  }
  return ener;
}

double energeticField::deformationEnergy(MElement *ele, const partDomain *dom) const{
  return _ipf->computeDeformationEnergy(ele,dom);
}

double energeticField::deformationEnergy() const{
  return _ipf->computeDeformationEnergy();
}

double energeticField::plasticEnergy(MElement *ele, const partDomain *dom) const{
  return _ipf->computePlasticEnergy(ele,dom);
}

double energeticField::plasticEnergy() const{
  return _ipf->computePlasticEnergy();
}

void energeticField::externalWork()const{ // for multiSystem takes only the first one (displacement)
  double Deltawext=0.;
  if(_nlsys!=NULL){
    Deltawext = 2*_nlsys->getExternalWork(_systemSizeWithoutRigidContact); // as we divided the value by 2 later !!!

    // Now compute the work of external forces due to prescribed displacement (-Wint - Winertia)
    // Do the scalar product on vector (more efficient) TO DO THE SCALAR PRODUCT WITH BLAS HOW ??
    std::vector<double> disp;
    std::vector<double> acc;
    std::vector<double> _mass;
    std::vector<double> forceval;
    std::vector<Dof> R2;
    std::vector<double> forceval2;
    if(_sAssembler!=NULL)
      _sAssembler->getFixedRightHandSide(R2,forceval2);
    else
    {
      _pAssembler->getFixedDof(R2);
      _pAssembler->getDofValue(R2,forceval2);
    }
    std::vector<Dof> R;
    for(int i=0;i<R2.size();i++){
      if(!_rigidContactFilter(R2[i])){
        R.push_back(R2[i]);
        forceval.push_back(forceval2[i]);
      }
    }
    R2.clear();
    forceval2.clear();
    _ufield->get(R,disp);
    // Compute inertial forces if needed
    if(_sAssembler != NULL and _sAssembler->getScheme() == nonLinearMechSolver::Explicit){ // otherwise static cases and no inertial forces
      _ufield->get(R,acc,nonLinearBoundaryCondition::acceleration);
      _sAssembler->getVertexMass(R,_mass);
      for(int i=0; i<forceval.size(); i++)
        forceval[i] -= _mass[i]*acc[i];
    }
    for(int i=0;i<R.size(); i++){
      _fextnp1[R[i]] = forceval[i];
      forceval[i] += _fextn[R[i]];
      _dispnp1[R[i]] = disp[i];
      disp[i] -= _dispn[R[i]];
    }
    Deltawext += dot(forceval,disp);
    // swap value (next step)
    _fextn.swap(_fextnp1);
    _dispn.swap(_dispnp1);

    // New value of Wext W = 0.5*F*u
    _wext += 0.5*Deltawext;
  }
}

int energeticField::fractureEnergy(double*  arrayEnergy) const
{
  return _ipf->computeFractureEnergy(arrayEnergy);
}

void energeticField::get(partDomain *dom,MElement *ele,std::vector<double> &ener, const int cc,
                         const nlsField::ElemValue ev)const{
  switch(cc){
   //case 0:
   // ener[0] = this->kineticEnergy(ele,dom) + this->deformationEnergy(ele,dom);
   // break;
   case 1:
    ener[0] = this->kineticEnergy(ele,dom);
    break;
   case 2:
    ener[0] = this->deformationEnergy(ele,dom);
    break;
   case 3:
    ener[0] = _wext; // false fix this (How and Interrest ?)
   case 4:
    ener[0] = this->plasticEnergy(ele,dom);
   default:
    ener[0] = this->kineticEnergy(ele,dom) + this->deformationEnergy(ele,dom);
  }
}

void energeticField::archive(const double time,const int step){
  // The increment has to be computed each step to have a good approximation of the integral !!
  if(_energyComputation !=0)
    this->externalWork();
  // msh file
  this->buildAllView(_domvec,time,step);
  if((!_forceView) and(_energyComputation>0))
  {
    // txt file
    if(step%_energyComputation == 0)
    {
      // without MPI transfert user has to sum the contribution of all processors at the end
/*      double ekin,edefo,etot,eplast;
      ekin = this->kineticEnergy();
      edefo= this->deformationEnergy();
      eplast = this->plasticEnergy();
      etot = ekin + edefo;
      fprintf(fpenergy,"%e;%e;%e;%e;%e;%e;\n",time,ekin,edefo,eplast,_wext,etot);
*/
      static double energy[4];
      energy[0] = this->kineticEnergy();
      energy[1] = this->deformationEnergy();
      energy[2] = this->plasticEnergy();
      energy[3] = _wext;
      #if 0 // No exchange of energy each rank stores its own contribution ( 1 each rank stores all the energy)
        #if defined(HAVE_MPI)
        if(Msg::GetCommSize() > 1){
	      static double localEnergy[4];
	      for(int i=0; i<4; i++) localEnergy[i]=energy[i];
	        MPI_Allreduce(&localEnergy,&energy,4,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
        }
        #endif // HAVE_MPI
      # endif // 0 or 1
      double etot = energy[0]+energy[1];

      fprintf(fpenergy,"%e;%e;%e;%e;%e;%e;\n",time,energy[0], energy[1], energy[2], energy[3],etot);
      if(step%(_energyComputation*10) == 0)
        fflush(fpenergy);

    }
    // same with fracture energy
    if(_fractureEnergyComputation>0)
    {
      if(step%_fractureEnergyComputation==0)
      {
        static double arrayEnergy[10];
        int ncomp = this->fractureEnergy(arrayEnergy);
        if(ncomp>10)
        {
          Msg::Error("You try to archive more than 10 values for the fracture energy. Please update the variable ncomp and arrayEnergy and the IPField::computeFractureEnergy function. Only the 10 first variables will be stored");
          ncomp=10;
        }
        fprintf(fpfrac,"%e;",time);
        if(ncomp==0)
        {
          fprintf(fpfrac,"0.;");
        }
        for(int i=0;i<ncomp;i++)
        {
          fprintf(fpfrac,"%e;",arrayEnergy[i]);
        }
        fprintf(fpfrac,"\n");
        if(step%(_fractureEnergyComputation*10) == 0)
          fflush(fpfrac);
      }
    }
  }
  else
  {
    _forceView = false; // the command forceView has to be used at each step you want to force archiving
  }
}

