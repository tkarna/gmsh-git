//
// C++ Interface: field
//
// Description: Base class to manage a field on nodes or elements
// The base class contains archiving data (which is not dependent of the field)
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef nlsField_H_
#define nlsField_H_
#ifndef SWIG
#include <string>
#include <stdint.h>
#include "MElement.h"
#include <vector>
#include <iostream>
#include <fstream>
class partDomain;
#include "groupOfElements.h"
#endif // SWIG
class nlsField{
 public:
  typedef enum {crude=10000, mean=10001, max=10002, min=10003, periodic=10004} ElemValue; // enum to select a particular value on element
#ifndef SWIG
 protected :
  // data needed to build a view
  bool view;
  mutable std::string fileName; // name of file where the displacement are store
  std::string _originalName; // name at creation of the field. Used by create file (usefull in case of 2 computations with the same field)
  mutable FILE *_fpview; // file where data is written
  long int totelem; // total number of element present in all elasticFields
  uint32_t fmaxsize; // Size max of file in bytes (if size of file is greater a new one is created) TODO Argument for this parameter ?
  mutable int numfile; // numero of file
  int numcomp; // number of component in the view
  mutable bool _forceView; // To force last view archiving at the end of simulation for example
  // function to update file name
  void updateFileName();
  void createFile();
  // Specific to elements or nodes
  virtual void buildData(FILE* myview,const std::vector<partDomain*> &vdom,const int cmp,const ElemValue ev_) const=0;
  virtual const std::string& dataname() const=0;
 public :
  // struct to keep necessary data to build a view
  struct dataBuildView{
    std::string viewname;
    int comp;
    int nbstepArch;
    mutable int mystepnum;
    ElemValue ev;
    bool binary;
    mutable int _lastSaveStep;
    dataBuildView(const std::string vn, const int cmp,const ElemValue ev_,
                  const int nbsa_,const bool bina=false): viewname(vn), comp(cmp), ev(ev_), mystepnum(0),
                                                             nbstepArch(nbsa_),binary(bina), _lastSaveStep(-1){}
    dataBuildView(const dataBuildView &source) : viewname(source.viewname), comp(source.comp), mystepnum(source.mystepnum),
                                                  ev(source.ev),nbstepArch(source.nbstepArch),binary(source.binary),
                                                  _lastSaveStep(source._lastSaveStep){}
    dataBuildView& operator=(const dataBuildView &source)
    {
      viewname = source.viewname;
      comp = source.comp;
      ev = source.ev;
      binary=source.binary;
      nbstepArch=source.nbstepArch;
      _lastSaveStep = source._lastSaveStep;
      mystepnum = source.mystepnum;
      return *this;
    }
  };
 protected:
  // set with view to archive
  const std::vector<dataBuildView> *_vBuildView; // keep a pointer otherwise problem with copy vector WHY ?? FIX THIS
 public:
  nlsField(const std::string &fnn, const uint32_t fms, const int ncomp,const std::vector<dataBuildView> &dbview_);
  virtual ~nlsField();
  // avoid this set HOW?? fix it
  void setTotElem(const int ne){totelem=ne;}
  virtual void get(partDomain* dom, MElement *ele,std::vector<double> &fieldData,
                   const int comp,const ElemValue ev=crude)const=0; // comp allow to use an enum
                                                                                         // in derivate class to choose which component to save
  virtual void buildView(const std::vector<partDomain*> &vdom,const double time,
                  const int nstep, const std::string &valuename, const int cc,
                  const ElemValue ev=crude,const bool binary=false);
  // archiving only 1 component (or more) in a txt file
  virtual void archive(const double time,const int step=1)=0;
  virtual void buildAllView(const std::vector<partDomain*> &vdom,const double time, const int stepnum=1);
  virtual void forceView(){_forceView = true;}
  virtual void closeFile();
  virtual void createRestart() const{}; // each field can create a file for a restart
  // Onelab
  virtual void onelabView(const std::string &meshFileName, const std::string &fname,
                          const std::vector<partDomain*> &vdom,const std::string &valuename,
                          const int comp,const double time, const int nstep) const;
#endif // SWIG
};
#endif // nlsField_H_

