//
// C++ Interface: terms
//
// Description: Class with the displacement field
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "unknownField.h"
#include "nonLinearMechSolver.h"
#include "nlsolAlgorithms.h"
#include "MPoint.h"
// constructor

unknownField::unknownField(dofManager<double> *pas, std::vector<partDomain*> &vdom, std::vector<partDomain*> &vghostdom,
                           nonLinearMechSolver::contactContainer *acontact,const int nc, std::vector<unknownField::archiveNode> &archiving,
                           const std::vector<nlsField::dataBuildView> &dbview_,
                           const std::string filen): pAssembler(pas),nodesField(filen,100000000,nc,dbview_),
                                                     _allContact(acontact), _vdom(&vdom),
                                                     _dynassembler(dynamic_cast<staticDofManager<double>*>(pAssembler))
{
  std::vector<bool> alfind;
  for(int i=0;i<archiving.size(); i++)
    alfind.push_back(false);
  long int totelem=0;
  for(std::vector<partDomain*>::iterator itdom=vdom.begin(); itdom!=vdom.end(); ++itdom){
    partDomain *dom = *itdom;
    bool fullDg = dom->getFormulation();
    FunctionSpaceBase *sp = dom->getFunctionSpace();
    std::vector<Dof> R;
    totelem += dom->g->size();
    if(!fullDg){
      for(groupOfElements::vertexContainer::iterator it=dom->g->vbegin(); it!=dom->g->vend(); ++it){
        MVertex *ver = *it;
        MPoint ele = (Msg::GetCommSize() == 1) ? MPoint(ver,-1) : MPoint(ver,-1,Msg::GetCommRank()+1); // We don't care about element number it is just to have the vertex keys;
        sp->getKeys(&ele,R);
        // make dof for archiving node
        for(int i=0;i<archiving.size();i++){
          if( (alfind[i] == false) and (ver->getNum() == archiving[i].nodenum) and (archiving[i].iscontact==false)){
            FilterDof* filter = dom->createFilterDof(archiving[i]._comp);
            for(int j=0;j<R.size();j++){
              if(filter->operator()(R[j])){
                alfind[i] = true;
                #if defined(HAVE_MPI)
                if(R[j].getType() > 0)
                #endif // HAVE_MPI
                {
                  varch.push_back(archiveNode(R[j],ver->getNum(),archiving[i]._comp,archiving[i].wc,archiving[i].nstep));
                }
              }
            }
            delete filter;
          }
        }
        R.clear();
      }
    }
    else{
      // loop on element (entity of formulation)
      for(groupOfElements::elementContainer::iterator it=dom->g->begin(); it!=dom->g->end(); ++it){
        MElement *ele = *it;
        sp->getKeys(ele,R);
        // loop on vertex element (for archiving node)
        int nbvertex = ele->getNumVertices();
        for(int j=0;j<nbvertex;j++){
          MVertex *ver = ele->getVertex(j);
          for(int i=0; i<archiving.size(); i++){
            if((alfind[i] == false) and(ver->getNum() == archiving[i].nodenum) and(archiving[i].iscontact==false)){
              // get the comp of the archiving node
              alfind[i] = true;
              int comp = archiving[i]._comp;
              #if defined(HAVE_MPI)
              if(R[j+comp+nbvertex].getType() > 0)
              #endif // HAVE_MPI
              {
                varch.push_back(archiveNode(R[j+comp*nbvertex],ver->getNum(),comp,archiving[i].wc,archiving[i].nstep));
              }
            }
          }
        }
        R.clear();
      }
    }
  }
  // increment the total element with rigid contact and add archiving if necessary

  for(nonLinearMechSolver::contactContainer::iterator it = _allContact->begin(); it!=_allContact->end(); ++it){
    contactDomain *cdom = *it;
    if(cdom->isRigid()){
      totelem += cdom->gMaster->size();
      int pMaster = cdom->getPhys();
      for(int i=0;i<archiving.size(); i++){
        partDomain* domf=NULL;
        if((archiving[i].iscontact==true) and ((int)archiving[i].nodenum == pMaster)){
          int physDom = cdom->getPhysSlave();
          for(std::vector<partDomain*>::iterator itdom = vdom.begin(); itdom!=vdom.end(); ++itdom){
            partDomain *dom = *itdom;
            if(dom->getPhysical() == physDom){
              domf = dom;
              break;
            }
          }
         #if defined(HAVE_MPI)
          if(domf == NULL)
          {
            for(std::vector<partDomain*>::iterator itdom = vghostdom.begin(); itdom!=vghostdom.end(); ++itdom)
            {
              partDomain *dom = *itdom;
              if(dom->getPhysical() == physDom)
              {
                domf = dom;
                break;
              }
            }
          }
         #endif // HAVE_MPI
          if(domf!=NULL){
            rigidContactSpaceBase *sp = static_cast<rigidContactSpaceBase*>(cdom->getSpace());
            std::vector<Dof> R;
            sp->getKeysOfGravityCenter(R);
            FilterDof *fdof = domf->createFilterDof(archiving[i]._comp);
            for(int j=0;j<R.size(); j++)
              if(fdof->operator()(R[j]))
                varch.push_back(archiveNode(R[j],pMaster,archiving[i]._comp,archiving[i].wc,archiving[i].nstep,true));
            delete fdof;
          }
        }
      }
    }
  }
//  archiving.clear(); cannot be clear in case of switch
  this->setTotElem(totelem);
  // save initial values
  this->buildAllView(vdom,0.,0);
}

void unknownField::buildData(FILE* myview,const std::vector<partDomain*> &vdom,const int cc,const ElemValue ev) const
{
  nodesField::buildData(myview,vdom,cc,ev);
  // add contact domain
  std::vector<double> fieldData;
  for(nonLinearMechSolver::contactContainer::iterator it=_allContact->begin(); it!=_allContact->end(); ++it){
    contactDomain *cdom = *it;
    if(cdom->isRigid()){
      for (groupOfElements::elementContainer::const_iterator it = cdom->gMaster->begin(); it != cdom->gMaster->end(); ++it){
        MElement *ele = *it;
        int numv = ele->getNumVertices();
        rigidContactSpaceBase *spgc = static_cast<rigidContactSpaceBase*>(cdom->getSpace());
        std::vector<Dof> R;
        spgc->getKeysOfGravityCenter(R);
        this->get(R,fieldData);
        fprintf(myview, "%d %d",ele->getNum(),numv);
        for(int i=0;i<numv;i++)
          for(int j=0;j<numcomp;j++)
            fprintf(myview, " %.16g",fieldData[j]);
        fprintf(myview,"\n");
        fieldData.clear();
      }
    }
  }
}


unknownField::~unknownField()
{
  for(int i=0;i<varch.size();i++)
  {
    varch[i].closeFile();
  }
}

void unknownField::get(Dof &D,double &udof) const{
  pAssembler->getDofValue(D,udof);
}

void unknownField::get(Dof &D, double &udof, nonLinearBoundaryCondition::whichCondition wv) const {
  //staticDofManager<double>* dynassembler = static_cast<staticDofManager<double>*>(pAssembler);
  _dynassembler->getDofValue(D,udof,wv);
}

void unknownField::get(std::vector<Dof> &R, std::vector<double> &disp) const{
  double du;
  for(int i=0;i<R.size(); i++){
    this->get(R[i],du);
    disp.push_back(du);
  }
}

void unknownField::get(std::vector<Dof> &R, fullVector<double> &disp) const{
  double du;
  for(int i=0;i<R.size(); i++){
    this->get(R[i],du);
    disp(i) = du;
  }
}

void unknownField::get(std::vector<Dof> &R, std::vector<double> &udofs, nonLinearBoundaryCondition::whichCondition wv) const {
  double du;
  for(int i=0;i<R.size(); i++){
    this->get(R[i],du,wv);
    udofs.push_back(du);
  }
}
void unknownField::get(std::vector<Dof> &R, fullVector<double> &udofs, nonLinearBoundaryCondition::whichCondition wv) const {
  double du;
  for(int i=0;i<R.size(); i++){
    this->get(R[i],du,wv);
    udofs(i) = du;
  }
}

void unknownField::get(partDomain *dom, MElement *ele,std::vector<double> &udofs, const int cc,const nlsField::ElemValue ev)const{
  std::vector<Dof> R;
  FunctionSpaceBase *sp = dom->getFunctionSpace();
  sp->getKeys(ele,R);
  if(cc==1 or cc == 2)
  {
    nonLinearBoundaryCondition::whichCondition wv;
    if (cc==1)
      wv= nonLinearBoundaryCondition::velocity;
    else if(cc==2)
      wv= nonLinearBoundaryCondition::acceleration;
    this->get(R,udofs,wv);
  }
  else
    this->get(R,udofs);
}

void unknownField::archive(const double time,const int step)
{
  // msh view
  this->buildAllView(*_vdom,time,step);
  if(!_forceView)
  {
    double u;
    for(std::vector<archiveNode>::const_iterator it = varch.begin(); it!=varch.end();++it){
      if( step%it->nstep == 0){
        const Dof D = it->D;
        if(_dynassembler!=NULL)
          _dynassembler->getDofValue(D,u,it->wc);
        else
          pAssembler->getDofValue(D,u);
        fprintf(it->fp,"%e;%e\n",time,u);
        if( step%(it->nstep*10) == 0) fflush(it->fp);
      }
    }
  }
  else
  {
    _forceView = false; // the command forceView has to be used at each step you want to force archiving
  }
}

void unknownField::valuesForOnelab(fullVector<double> &nodalValues) const
{
  nodalValues.resize(varch.size());
  int index = 0;
  double u;
  for(std::vector<archiveNode>::const_iterator it = varch.begin(); it!=varch.end();++it,++index){
    const Dof D = it->D;
    if(_dynassembler!=NULL)
      _dynassembler->getDofValue(D,u,it->wc);
    else
      pAssembler->getDofValue(D,u);

    nodalValues.set(index,u);
  }
}
