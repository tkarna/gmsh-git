//
// C++ Interface: field
//
// Description: Class for a field on nodes (specific functions)
// Author:  <Gauthier BECKER>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "nodesField.h"
#include "partDomain.h"
nodesField::nodesField(const std::string &fnn, const uint32_t fms, const int ncomp,
               const std::vector<dataBuildView> &dbview_) : nlsField(fnn,fms,ncomp,dbview_), _dname("ElementNodeData"){};

void nodesField::buildData(FILE* myview,const std::vector<partDomain*> &vdom,const int cc,const ElemValue ev) const
{
  std::vector<double> fieldData;
  for(std::vector<partDomain*>::const_iterator itdom=vdom.begin(); itdom!=vdom.end(); ++itdom){
    partDomain *dom = *itdom;
    for (groupOfElements::elementContainer::const_iterator it = dom->g->begin(); it != dom->g->end(); ++it){
      MElement *ele = *it;
      int numv = ele->getNumVertices();
      this->get(dom,ele,fieldData,cc,ev);
      fprintf(myview, "%d %d",ele->getNum(),numv);
      for(int i=0;i<numv;i++)
        for(int j=0;j<numcomp;j++){
          int ij = i+j*numv;
          if (ij < fieldData.size())
            fprintf(myview, " %.16g",fieldData[i+j*numv]);
          else
            fprintf(myview, " %.16g",0.0);
        }
      fprintf(myview,"\n");
      fieldData.clear();
    }
  }
}

const std::string& nodesField::dataname() const{return _dname;}
