//
// C++ Interface: field
//
// Description: Base class to manage a field on nodes or elements
// The base class contains archiving data (which is not dependent of the field)
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include "nlsField.h"
#include <sstream>
#include "partDomain.h"
void nlsField::updateFileName()
{
  // add a numero to file name
  std::ostringstream oss;
  oss << ++numfile;
  std::string s = oss.str();
  std::string snum;
  // cut filename and its extension
  size_t ext_pos;
  if(numfile==1){
   #if defined(HAVE_MPI)
    if(Msg::GetCommSize()>1)
      ext_pos = fileName.find_last_of('_');
    else
   #endif // HAVE_MPI
    {
      ext_pos = fileName.find_last_of('.');
    }
    snum = "0";
  }
  else{
    oss.str("");
    oss << numfile-1;
    snum = oss.str();
    ext_pos = fileName.find_first_of(snum);
  }
  std::string newname(fileName,0,ext_pos);
 #if defined(HAVE_MPI)
  if(Msg::GetCommSize()>1)
  {
    ext_pos = fileName.find_last_of('_');
    std::string ext(fileName,ext_pos+1,fileName.size());
    fileName  = newname+s+"_"+ext;
  }
  else
 #endif // HAVE_MPI
  {
    ext_pos = fileName.find_last_of('.');
    std::string ext(fileName,ext_pos+1,fileName.size());
    fileName  = newname+s+"."+ext;
  }
  nlsField::closeFile();
  // create initialization of file
  _fpview = fopen(fileName.c_str(), "w");
  fprintf(_fpview, "$MeshFormat\n2.2 0 8\n$EndMeshFormat\n");
}

void nlsField::createFile(){
  if(view){
    // creation of file to store view (delete the previous one if exist)
    size_t ext_pos;
    std::string rfn;
   #if defined(HAVE_MPI)
    if(Msg::GetCommSize()>1){
      ext_pos = fileName.find_last_of('_');
      std::string newname = fileName;
      newname.resize(ext_pos);
      std::ostringstream oss;
      oss << Msg::GetCommRank();
      std::string rankstr = oss.str();
      rfn = "rm -f "+newname+"*"+"_part"+rankstr+".msh";
    }
    else{
      ext_pos = fileName.find_last_of('.');
      std::string newname = fileName;
      newname.resize(ext_pos);
      rfn = "rm -f "+newname+"*";
    }
   #endif // HAVE_MPI
    system(rfn.c_str());
    nlsField::closeFile();
    _fpview = fopen(_originalName.c_str(), "w");
    fileName = _originalName;
    numfile = 0;
    fprintf(_fpview, "$MeshFormat\n2.2 0 8\n$EndMeshFormat\n"); // find how to retrieve the last mesh format version
  }
}

nlsField::nlsField(const std::string &fnn, const uint32_t fms, const int ncomp,
                   const std::vector<dataBuildView> &dbview_) : numfile(0), fmaxsize(fms),
                                                                totelem(0), numcomp(ncomp), view(false),
                                                                _vBuildView(&dbview_), _forceView(false), _fpview(NULL)
{
  if(dbview_.size() !=0)
    view = true;
#if defined(HAVE_MPI)
  // modification of filename to include partition in mpi
  if(Msg::GetCommSize() !=1){ // mpi version in this case
    std::ostringstream oss;
    oss << Msg::GetCommRank();
    std::string partnum = oss.str();
    // retrieve file extension
    size_t ext_pos;
    ext_pos = fnn.find_last_of('.');
    std::string ext(fnn,ext_pos+1,fnn.size());
    // set file name
    std::string newname(fnn,0,ext_pos);
    fileName = newname + "_part" + partnum +"." + ext;
  }
  else
#endif
  {
    fileName = fnn;
  }
  _originalName = fileName;
  this->createFile();
}

void nlsField::buildView(const std::vector<partDomain*> &vdom,const double time,
                  const int nstep, const std::string &valuename, const int cc,
                  const ElemValue ev,const bool binary)
{
  if(view){
    // test file size (and create a new one if needed)
    uint32_t fileSize;
    if(_fpview==NULL){
      Msg::Error("Unable to open file '%s'", fileName.c_str());
      return;
    }
    fseek (_fpview, 0, SEEK_END);
    fileSize = (uint32_t) (ftell(_fpview));
    if(fileSize > fmaxsize) this->updateFileName();

    // compute the number of element
    if(binary) Msg::Warning("Binary write not implemented yet");
    fprintf(_fpview, "$%s\n1\n\"%s\"\n1\n%.16g\n4\n%d\n%d\n%d\n%d\n",this->dataname().c_str(),valuename.c_str(),time,nstep, numcomp, totelem,Msg::GetCommRank());

    this->buildData(_fpview,vdom,cc,ev);
    fprintf(_fpview, "$End%s\n",this->dataname().c_str());
  }
  else Msg::Warning("No element view created because the variable view is set to false for this field\n");
}

void nlsField::buildAllView(const std::vector<partDomain*> &vdom,const double time, const int numstep){
  for(std::vector<dataBuildView>::const_iterator it=_vBuildView->begin(); it!=_vBuildView->end(); ++it){
    const dataBuildView &dbv = *it;
    if(numstep != dbv._lastSaveStep)
    {
      if( (numstep%dbv.nbstepArch == 0) or (_forceView) ){
        this->buildView(vdom,time,dbv.mystepnum,dbv.viewname,dbv.comp,dbv.ev,dbv.binary);
        dbv._lastSaveStep = numstep;
        dbv.mystepnum++;
        fflush (_fpview);
      }
    }
  }
}

void nlsField::closeFile()
{
  if(_fpview!=NULL){
    fclose(_fpview);
    _fpview=NULL;
  }
}

nlsField::~nlsField(){}

void
nlsField::onelabView(const std::string &meshFileName, const std::string &fname,
                     const std::vector<partDomain*> &vdom,const std::string &valuename,
                     const int comp,const double time,const int nstep) const
{
  // The onelab file has to contains the mesh info
  // Copy first the mesh file
  std::string copyCommand("cp "); // LINUX ONLY
  copyCommand += meshFileName + " " + fname;
  system(copyCommand.c_str());

  // add the data at the end of the file
  FILE* fpview = fopen(fname.c_str(),"a");

  // Put the data
  fprintf(fpview, "$%s\n1\n\"%s\"\n1\n%.16g\n4\n%d\n%d\n%d\n%d\n",this->dataname().c_str(),valuename.c_str(),time,nstep, numcomp, totelem,Msg::GetCommRank());

  this->buildData(fpview,vdom,comp,nlsField::crude);
  fprintf(fpview, "$End%s\n",this->dataname().c_str());

  fclose(fpview);
  return;
}
