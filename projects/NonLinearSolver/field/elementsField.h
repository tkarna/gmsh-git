//
// C++ Interface: field
//
// Description: Class for a field on elements (specific functions)
// Author:  <Gauthier BECKER>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef ELEMENTSFIELD_H_
#define ELEMENTSFIELD_H_
#include "nlsField.h"
class elementsField : public nlsField
{
#ifndef SWIG
 protected:
  const std::string _dname;
  virtual void buildData(FILE* myview,const std::vector<partDomain*> &vdom,const int cc,const ElemValue ev) const;
  virtual const std::string& dataname() const;
 public:
  elementsField(const std::string &fnn, const uint32_t fms, const int ncomp,const std::vector<dataBuildView> &dbview_);
  virtual ~elementsField(){}
#endif // SWIG
};
#endif // ELEMENTSFIELD_H_
