//
// contact Domain
//
// Description: Domain to solve contact problem
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef CONTACTDOMAIN_H_
#define CONTACTDOMAIN_H_
#ifndef SWIG
#include "groupOfElements.h"
#include "partDomain.h"
#include "contactTerms.h"
#include "rigidCylinderContactTerms.h"
#include "rigidConeContactTerms.h"
#include "MVertex.h"
#include "MElement.h"
template<class T2> class contactBilinearTermBase;
template<class T2> class contactLinearTermBase;
#endif
class contactDomain{
 public:
  enum contact{rigidCylinder, rigidSphere};
 protected:
  int _phys;
  int _physSlave;
  int _tag;
  double _penalty;
  BilinearTermBase* _bterm;
  BilinearTermBase* _massterm;
  LinearTermBase<double>* _lterm;
  partDomain *_dom;
  contact _contype;
  bool _rigid;
  FunctionSpaceBase *_space;
  QuadratureBase *_integ;
  elementFilter *_filterSlave; // search contact only on a part of slave domain
 public:
  groupOfElements *gMaster;
  groupOfElements *gSlave;
#ifndef SWIG
  contactDomain() : gMaster(0), gSlave(0), _phys(0), _physSlave(0), _tag(0), _contype(rigidCylinder), _dom(0), _penalty(0.), _rigid(true){}
  contactDomain(const int tag, const int phys, const int physSlave, double pe,
                contact conty,elementFilter *fil=NULL, const bool rigid=false) : _tag(tag), _phys(phys), _physSlave(physSlave),
                                                          _penalty(pe), _contype(conty),
                                                          _rigid(rigid), _space(NULL), _filterSlave(fil){}
  contactDomain(const contactDomain &source);
  virtual ~contactDomain(){}

  virtual void setTag(const int t){ _tag =t;}
  virtual void setPhys(const int p){_phys =p;}
  virtual void setPenalty(const double pe){_penalty=pe;}
  virtual void setContactType(const int ct);
  virtual void setContactType(const contact ct){_contype =ct;}
  virtual int getTag() const{return _tag;}
  virtual int getPhys() const{return _phys;}
  virtual int getPhysSlave() const{return _physSlave;}
  virtual double getPenalty() const{return _penalty;}
  virtual contact getContactType() const{return _contype;}
  virtual partDomain* getDomain() const {return _dom;}
  virtual bool isRigid() const {return _rigid;}
  virtual BilinearTermBase* getMassTerm(){return _massterm;}
  virtual BilinearTermBase* getStiffnessTerm(){return _bterm;}
  virtual LinearTermBase<double>* getForceTerm(){return _lterm;}
  virtual FunctionSpaceBase* getSpace() {return _space;}
  virtual const FunctionSpaceBase* getSpace() const {return _space;}
  virtual QuadratureBase* getGaussIntegration() const{return _integ;}
  virtual FilterDof* createFilterDof(const int comp)const {return _dom->createFilterDof(comp);}
  virtual void setDomainAndFunctionSpace(partDomain *dom)=0;
  virtual void initializeTerms(const unknownField *ufield)=0;
  virtual MVertex* getGC()const=0;
//  static void registerBindings(binding *b);
#endif
};

class rigidCylinderContactDomain : public contactDomain{
 protected:
  double _length; // length of cylinder;
  double _radius; // outer radius of cylinder;
  double _thick;  // thickness of cylinder;
  double _thickContact; // use for shell (contact with neutral axis of external fiber is !=0)
  double _density; // density of cylinder Not a material law for now
  MVertex *_vergc;  // vertex of gravity center
  SVector3 *_axisDirector; // normalized vector director of cylinder's axis
 public:
 #ifndef SWIG
  rigidCylinderContactDomain(const int tag, const int physMaster, const int physSlave, const int physpt1,
                       const int physpt2,const double penalty,const double h,const double rho,
                       elementFilter *filSlave=NULL);
  rigidCylinderContactDomain() : contactDomain(), _length(0.), _radius(0.), _thick(0.){
    _contype = rigidCylinder;
    _rigid = true;
    _integ = new QuadratureVoid();
  }
  rigidCylinderContactDomain(const rigidCylinderContactDomain &source);
  virtual ~rigidCylinderContactDomain()
  {
    delete _axisDirector;
   #if defined(HAVE_MPI)
    if(Msg::GetCommSize()>1) delete _filterSlave;
   #endif // HAVE_MPI
  }
  virtual void initializeTerms(const unknownField *ufield);
  SVector3* getAxisDirector() const{return _axisDirector;}
  virtual void setDomainAndFunctionSpace(partDomain *dom)=0;
  virtual MVertex* getGC() const{return _vergc;}
  double getDensity() const{return _density;}
  double getLength() const{return _length;}
  double getRadius() const{return _radius;}
  double getThickness() const{return _thick;}
 #endif // SWIG
};

class rigidConeContactDomain : public contactDomain{
 protected:
  double _baseRadius; // radius at the cone's bottom
  double _thick; // thickness to compute mass
  double _heightCone; // height of cone
  double _heightCylinder; // height of cylinder at the end of cone can be == 0
  double _thickContact; // use for shell (contact with external fiber if = 0)
  double _density; // pass for a material law ??
  MVertex *_vergcCone; // vertex at gravity center
  MVertex *_vergcTot;
  SVector3* _axisDirector; // normalized vector director of cylinder's axis (base --> top)
  double _alpha; // half angle at cone's top in radian
 public:
 #ifndef SWIG
  rigidConeContactDomain(const int tag, const int physMaster, const int physSlave,const int physptBase,
                         const int physptTop, const int physptBot, const double penalty,const double bradius,
                         const double h,const double rho,elementFilter *filSlave=NULL);
  rigidConeContactDomain(const rigidConeContactDomain &source);
  virtual ~rigidConeContactDomain()
  {
   #if defined(HAVE_MPI)
    if(Msg::GetCommSize()>1) delete _filterSlave;
   #endif // HAVE_MPI
  }
  virtual void initializeTerms(const unknownField *ufield);
  SVector3* getAxisDirector() const{return _axisDirector;}
  virtual void setDomainAndFunctionSpace(partDomain *dom)=0;
  virtual MVertex* getGCcone() const{return _vergcCone;}
  virtual MVertex* getGC() const{return _vergcTot;}
  double getDensity() const{return _density;}
  double getHeightCone() const{return _heightCone;}
  double getHeightCylinder()const{return _heightCylinder;}
  double getRadius() const{return _baseRadius;}
  double getThickness() const{return _thick;}
  double getAlpha() const{return _alpha;}
 #endif // SWIG
};

#endif // CONTACTDOMAIN_H_
