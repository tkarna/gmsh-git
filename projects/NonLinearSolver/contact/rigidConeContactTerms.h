//
// contact base term
//
// Description: contact with a rigid cone
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef RIGIDCONTACTCONETERMS_H_
#define RIGIDCONTACTCONETERMS_H_
#include "contactTerms.h"
#include "contactFunctionSpace.h"
class rigidConeContactDomain;
class massRigidCone : public BilinearTermBase{
 protected:
  rigidContactSpaceBase *_spc;
  double _heightCone, _heightCylinder, _baseRadius, _thick, _rho;
 public:
  massRigidCone(rigidConeContactDomain *cdom, rigidContactSpaceBase *spc);
  massRigidCone(rigidContactSpaceBase *spc, double heightCone, double heightCylinder, double radius,
                double thick, double rho) : _spc(spc), _heightCone(heightCone), _baseRadius(radius), _thick(thick), _rho(rho),
                                            _heightCylinder(heightCylinder){}
  ~massRigidCone(){}
  // Arguments are useless but it's allow to use classical assemble function
  void get(MElement *ele, int npts, IntPt *GP, fullMatrix<double> &m) const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me get by integration point massRigidCone");
  }
  virtual BilinearTermBase* clone () const
  {
    return new massRigidCone(_spc,_heightCone,_heightCylinder,_baseRadius,_thick,_rho);
  }
};


class forceRigidConeContact : public rigidContactLinearTermBase<double>{
 protected:
  const rigidConeContactDomain *_cdom;
  const MVertex *_vergcTot;
  const MVertex *_vergcCone;
  const SVector3* _axisDirector;
  const double _penalty;
  const double _alpha;
  const double _heightCone;
  const double _heightCylinder;
  const double _baseRadius;
  double _facGC;
  const SPoint3 _GCcone, _GCtot;
  SPoint3 _ptTop;
 private:
  mutable SPoint3 B,Bdisp,A,Adisp,C,D;
  mutable SVector3 dirAC;
 public:
  forceRigidConeContact(const rigidConeContactDomain *cdom, rigidContactSpaceBase *sp, const double thickcontact,
                        const unknownField *ufield);
  ~forceRigidConeContact(){}
  void get(const MVertex *ver, const double disp[6], double mvalue[6]) const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullVector<double> > &vv) const
  {
    Msg::Error("define me get by gauss point forceRigidConeContact");
  }
  virtual LinearTermBase<double>* clone () const
  {
    return new forceRigidConeContact(_cdom,this->_spc,0.,_ufield);
  }
};


class stiffnessRigidConeContact : public contactBilinearTermBase<double>{
 private:
  // can only be applied by classical get function perturbation for 1 vertex
  void get(MElement *ele, const int verIndex, fullMatrix<double> &m) const;
 protected:
  const unknownField *_ufield;
  rigidContactSpaceBase *_spc;
  const double _perturbation; // numerical perturbation
  const double _doublepertexpm1;
  mutable double fp[6];
  mutable double fm[6];
  mutable std::vector<Dof> R;
  mutable std::vector<double> disp;
  mutable double pertdisp[6];
 public:
  stiffnessRigidConeContact(rigidContactSpaceBase *space, contactLinearTermBase<double> *lterm,
                               const unknownField *ufield) : contactBilinearTermBase<double>(lterm),
                                                             _ufield(ufield), _perturbation(1.e-10), _doublepertexpm1(1./(2.e-10)),
                                                             _spc(space){}
  ~stiffnessRigidConeContact(){}
  void get(MElement *ele, int npts, IntPt *GP, fullMatrix<double> &m) const;
  virtual void get(MElement *ele, int npts, IntPt *GP, std::vector<fullMatrix<double> > &mv) const
  {
    Msg::Error("Define me get by integration point stiffnessRigidConeContact");
  }
  virtual BilinearTermBase* clone () const
  {
    return new stiffnessRigidConeContact(_spc,_lterm,_ufield);
  }
};

#endif // RIGIDCONTACTCONETERMS_H_
