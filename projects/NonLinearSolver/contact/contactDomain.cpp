//
// contact Domain
//
// Description: Domain to solve contact problem
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "contactDomain.h"
#include "elementFilterMPI.h"
#include "rigidConeContactTerms.h"
contactDomain::contactDomain(const contactDomain &source){
  _tag = source._tag;
  _phys = source._phys;
  _physSlave = source._physSlave;
  _penalty = source._penalty;
  gSlave = source.gSlave;
  gMaster = source.gMaster;
  _dom = source._dom;
  _bterm = source._bterm;
  _massterm = source._massterm;
  _lterm = source._lterm;
  _contype = source._contype;
  _rigid = source._rigid;
  _space = source._space;
  _integ = source._integ;
  _filterSlave = source._filterSlave;
}

void contactDomain::setContactType(const int ct){
  switch(ct){
   case 0:
    _contype = rigidCylinder;
    _rigid = true;
    break;
   case 1:
    _contype = rigidSphere;
    _rigid = true;
    break;
   default:
    Msg::Error("No contact know for int %d",ct);
  }
}

rigidCylinderContactDomain::rigidCylinderContactDomain(const int tag, const int physMaster, const int physSlave, const int physPt1,
                                           const int physPt2, const double penalty,
                                           const double h,const double rho,
                                           elementFilter *filSlave) : contactDomain(tag,physMaster,physSlave,
                                                                           penalty,rigidCylinder,filSlave,true),
                                                               _thick(h), _density(rho){

  // void gauss integration
  _integ = new QuadratureVoid();

  // creation of group of elements
  gMaster = new groupOfElements(2,physMaster); // rigid part (Visualisation only keep on rank 0 for initialisation and clear on other ranks)

  gSlave = new groupOfElements();
 #if defined(HAVE_MPI)
  if(Msg::GetCommSize()>1){
    if(filSlave == NULL)
      _filterSlave = new elementFilterMPITrivial();
    else
      _filterSlave = new elementFilterMPIUntrivial(filSlave);
  }
 #endif // HAVE_MPI
  if(_filterSlave != NULL)
    gSlave->addPhysical(2,physSlave,*_filterSlave);
  else
    gSlave->addPhysical(2,physSlave);

  // use for build --> no save
  groupOfElements gpt1 = groupOfElements(0,physPt1);
  groupOfElements gpt2 = groupOfElements(0,physPt2);

  // find GC and dimension of cylinder
  // get vertex
  groupOfElements::vertexContainer::iterator itpt1 = gpt1.vbegin();
  groupOfElements::vertexContainer::iterator itpt2 = gpt2.vbegin();
  MVertex *ver1 = *itpt1;
  MVertex *ver2 = *itpt2;

  // Vertices coordinates
  double x1 = ver1->x(); double y1 = ver1->y(); double z1 = ver1->z();
  double x2 = ver2->x(); double y2 = ver2->y(); double z2 = ver2->z();
  //creation of GC vertex
  double xgc = 0.5*(x1+x2); double ygc = 0.5*(y1+y2); double zgc = 0.5*(z1+z2);
  _vergc = new MVertex(xgc,ygc,zgc);
  // dimension of cylinder
  _length = ver1->distance(ver2);
  // Radius search for an extreme pnt (ie the point with the most distance of gc)
  double dist=-1.; // initialization
  MVertex *vermax;
  for(groupOfElements::vertexContainer::iterator it=gMaster->vbegin(); it!=gMaster->vend();++it){
    MVertex *ver = *it;
    double d = ver->distance(_vergc);
    if(d > dist){
      vermax = ver;
      dist = d;
    }
  }
  #if defined(HAVE_MPI)
  if(Msg::GetCommRank() != 0)
  {
    gMaster->clearAll();
  }
  #endif // HAVE_MPI

  // radius = smallest distance of extreme point and a center
  double r1 = vermax->distance(ver1);
  double r2 = vermax->distance(ver2);
  (r1>r2) ? _radius=r2 : _radius=r1;

  // vector director of cylinder's axis
  _axisDirector = new SVector3(ver1->point(),ver2->point());
  _axisDirector->normalize();
}

rigidCylinderContactDomain::rigidCylinderContactDomain(const rigidCylinderContactDomain &source) : contactDomain(source),
                                                                                                   _length(source._length),
                                                                                                   _radius(source._radius),
                                                                                                   _thick(source._thick),
                                                                                                   _thickContact(source._thickContact),
                                                                                                   _density(source._density),
                                                                                                   _vergc(source._vergc),
                                                                                                   _axisDirector(source._axisDirector){}


void rigidCylinderContactDomain::initializeTerms(const unknownField *ufield){
  rigidContactSpaceBase *sp = static_cast<rigidContactSpaceBase*>(_space);
  _massterm = new massRigidCylinder(this,sp);
  _lterm = new forceRigidCylinderContact(this,sp,_thickContact,ufield);
  rigidContactLinearTermBase<double> *rlterm = static_cast<rigidContactLinearTermBase<double>*>(_lterm);
  _bterm = new stiffnessRigidCylinderContact(sp,rlterm,ufield);
}

rigidConeContactDomain::rigidConeContactDomain(const int tag, const int physMaster, const int physSlave,const int physptBase,
                                               const int physptTop, const int physptBot, const double penalty, const double bradius,
                                               const double h,const double rho,
                                               elementFilter *filSlave) : contactDomain(tag,physMaster,physSlave,
                                                                                  penalty,rigidCylinder,filSlave,true),
                                                                                  _thick(h), _density(rho), _baseRadius(bradius)
{

  // void gauss integration
  _integ = new QuadratureVoid();

  // creation of group of elements
  gMaster = new groupOfElements(2,physMaster); // rigid part (Visualisation only keep on rank 0 for initialisation and clear on other ranks)

  gSlave = new groupOfElements();
 #if defined(HAVE_MPI)
  if(Msg::GetCommSize()>1){
    if(_filterSlave == NULL)
      _filterSlave = new elementFilterMPITrivial();
    else
      _filterSlave = new elementFilterMPIUntrivial(filSlave);
  }
 #endif // HAVE_MPI
  if(_filterSlave != NULL)
    gSlave->addPhysical(2,physSlave,*_filterSlave);
  else
    gSlave->addPhysical(2,physSlave);  // deformable part


  // Compute Length of cyliner and axis director
  // use for build --> no save
  groupOfElements gptBase = groupOfElements(0,physptBase);
  groupOfElements gptTop = groupOfElements(0,physptTop);
  groupOfElements gptBottom = groupOfElements(0,physptBot);

  // get vertex
  groupOfElements::vertexContainer::iterator itptBase = gptBase.vbegin();
  groupOfElements::vertexContainer::iterator itptTop = gptTop.vbegin();
  groupOfElements::vertexContainer::iterator itptBottom = gptBottom.vbegin();

  MVertex *verBase = *itptBase;
  MVertex *verTop = *itptTop;
  MVertex *verBottom = *itptBottom;

  // Vertices coordinates
  double xBase = verBase->x(); double yBase = verBase->y(); double zBase  = verBase->z();
  double xTop = verTop->x();   double yTop  = verTop->y();  double zTop  =  verTop->z();
  double xBot = verBottom->x(), yBot = verBottom->y(), zBot = verBottom->z();

  // length
  _heightCone = verBase->distance(verTop);
  if(physptBot != physptBase){
    _heightCylinder = verBottom->distance(verBase);
  }
  else{
    _heightCylinder = 0.; // no cylindrical part
  }

  // angle
  _alpha = atan(_baseRadius/_heightCone);

  // axis director (base --> top) Assement ptBottom is alligned with these 2 points
  _axisDirector = new SVector3(verBase->point(),verTop->point());
  _axisDirector->normalize();


  // find GC
  // GC cone ptBase + 0.25*length in the direction of axis director
  SPoint3 ptgcCone = SPoint3();
  ptgcCone.setPosition(verBase->point(),_axisDirector->point(),0.25*_heightCone);
  _vergcCone = new MVertex(ptgcCone.x(),ptgcCone.y(),ptgcCone.z());
  if(_heightCylinder != 0.) // take into account the cylindrical part
  {
    SPoint3 ptgcCylinder = SPoint3();
    ptgcCylinder.setPosition(verBottom->point(),_axisDirector->point(),0.5*_heightCylinder);
    // The gravity center is on the axis between the gravity center of both parts.
    // volume of both parts
    double Volcone = 0.333333333*M_PI*_baseRadius*_baseRadius*_heightCone;
    double VolCyl = M_PI*_baseRadius*_baseRadius*_heightCylinder;
    // distance from cylinder gravity center
    double db2gc = Volcone/(VolCyl+Volcone)*verBottom->distance(verBase);
    SPoint3 ptgc;
    ptgc.setPosition(ptgcCylinder,_axisDirector->point(),db2gc);
    _vergcTot = new MVertex(ptgc.x(),ptgc.y(),ptgc.z());
  }
  else{
    _vergcTot = _vergcCone; //new MVertex(ptgc.x(),ptgc.y(),ptgc.z());
  }


  #if defined(HAVE_MPI)
  if(Msg::GetCommRank() != 0)
  {
    gMaster->clearAll();
  }
  #endif // HAVE_MPI
}


rigidConeContactDomain::rigidConeContactDomain(const rigidConeContactDomain &source) : contactDomain(source),
                                                                                       _baseRadius(source._baseRadius),
                                                                                       _heightCone(source._heightCone),
                                                                                       _heightCylinder(source._heightCylinder),
                                                                                       _thick(source._thick),
                                                                                       _thickContact(source._thickContact),
                                                                                       _density(source._density),
                                                                                       _vergcCone(source._vergcCone),
                                                                                       _vergcTot(source._vergcTot),
                                                                                       _axisDirector(source._axisDirector),
                                                                                       _alpha(source._alpha){}

void rigidConeContactDomain::initializeTerms(const unknownField *ufield)
{
  rigidContactSpaceBase *sp = static_cast<rigidContactSpaceBase*>(_space);
  _massterm = new massRigidCone(this,sp);
  _lterm = new forceRigidConeContact(this,sp,_thickContact,ufield);
  rigidContactLinearTermBase<double> *rlterm = static_cast<rigidContactLinearTermBase<double>*>(_lterm);
  _bterm = new stiffnessRigidConeContact(sp,rlterm,ufield);
}

