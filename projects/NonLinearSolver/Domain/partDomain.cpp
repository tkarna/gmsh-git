//
// C++ Interface: partDomain
//
// Description: Interface class to used solver. Your term ha to be store in a domain
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "partDomain.h"
#include "ipField.h"
#include "numericalMaterial.h"
partDomain::partDomain(const partDomain &source){
  _tag = source._tag;
  _phys = source._phys;
  _fullDg = source._fullDg;
  btermBulk = source.btermBulk;
  massterm = source.massterm;
  ltermBulk = source.ltermBulk;
  integBulk = source.integBulk;
  g = source.g;
  _groupGhostMPI = source._groupGhostMPI;
  allConstraint = source.allConstraint;
  _bmbp = source._bmbp;
  _eps= source._eps;
  _wsp = source._wsp;
  setmaterial = source.setmaterial;
}

partDomain& partDomain::operator=(const partDomain &source)
{
  _tag = source._tag;
  _phys = source._phys;
  _fullDg = source._fullDg;
  btermBulk = source.btermBulk;
  massterm = source.massterm;
  ltermBulk = source.ltermBulk;
  integBulk = source.integBulk;
  g = source.g;
  _groupGhostMPI = source._groupGhostMPI;
  allConstraint = source.allConstraint;
  _wsp = source._wsp;
  _bmbp = source._bmbp;
  _eps = source._eps;
  setmaterial = source.setmaterial;
  return *this;
}

void partDomain::createIPState(AllIPState::ipstateContainer& map, const bool *state) const{
  IntPt* GP;
  const materialLaw *mlaw = this->getMaterialLaw();
  for (groupOfElements::elementContainer::const_iterator it = this->g->begin(); it != this->g->end(); ++it){
    MElement *ele = *it;
    int npts_bulk=this->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
    AllIPState::ipstateElementContainer tp(npts_bulk);
    for(int i=0;i<npts_bulk;i++){
      mlaw->createIPState(tp[i],state,ele,ele->getNumVertices(),i);
    }
    map.insert(AllIPState::ipstatePairType(ele->getNum(),tp));
  }
};

double partDomain::computeMeanValueDomain(const int num, const IPField* ipf) const{
  IntPt* GP;
  double meanVal = 0;
  double solid = 0;
  const IPVariable *vipv[256];
  for (groupOfElements::elementContainer::const_iterator it = this->g->begin(); it != this->g->end(); ++it){
    MElement *ele = *it;
    ipf->getIPv(ele,vipv);
    int npts_bulk=this->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
    for (int i=0; i<npts_bulk; i++){
      double weight= GP[i].weight;
      double u = GP[i].pt[0];
      double v = GP[i].pt[1];
      double w = GP[i].pt[2];
      double detJ = ele->getJacobianDeterminant(u,v,w);
      meanVal += vipv[i]->get(num)*weight*detJ;
      solid += weight*detJ;
    };
  }
  meanVal /= solid;
  return meanVal;
};

#if defined(HAVE_MPI)
void partDomain::setWorkingRanks(const int root, const std::set<int>& others) {
  _rootRank = root;
  _otherRanks = others;
};
int partDomain::getRootRank() const{
  return _rootRank;
};
void partDomain::getOtherRanks(std::set<int>& others) const{
  others = _otherRanks;
};
#endif //HAVE_MPI

void partDomain::initMicroMeshId(){
  materialLaw* mlaw = this->getMaterialLaw();
  if (mlaw->isNumeric() ){
    numericalMaterial* nummat = dynamic_cast<numericalMaterial*>(mlaw);
    IntPt* GP;
    for (groupOfElements::elementContainer::iterator it = g->begin(); it!= g->end(); it++){
      MElement* ele = *it;
      int npts = this->getBulkGaussIntegrationRule()->getIntPoints(ele,&GP);
      for (int i=0; i<npts; i++){
        nummat->assignMeshId(ele->getNum(),i);
      }
    }
  }
};

dgPartDomain::dgPartDomain(const dgPartDomain &source) : partDomain(source){
  btermBound = source.btermBound;
  ltermBound = source.ltermBound;
  btermVirtBound = source.btermVirtBound;
  ltermVirtBound = source.ltermVirtBound;
  integBound = source.integBound;
  gi = source.gi;
  gib = source.gib;
  giv = source.giv;
  _interByPert = source._interByPert;
  _virtByPert = source._virtByPert;
}

dgPartDomain& dgPartDomain::operator=(dgPartDomain &source)
{
  this->partDomain::operator=(source);
  btermBound = source.btermBound;
  ltermBound = source.ltermBound;
  btermVirtBound = source.btermVirtBound;
  ltermVirtBound = source.ltermVirtBound;
  integBound = source.integBound;
  gi = source.gi;
  gib = source.gib;
  giv = source.giv;
  _interByPert = source._interByPert;
  _virtByPert = source._virtByPert;
  return *this;
}

void dgPartDomain::createIPState(AllIPState::ipstateContainer& map, const bool *state) const{
  partDomain::createIPState(map,state);
  IntPt* GP;
  const materialLaw *mlawMinus = this->getMaterialLawMinus();
  const materialLaw *mlawPlus = this->getMaterialLawPlus();
  // loop
  for(groupOfElements::elementContainer::const_iterator it=this->gi->begin(); it!=this->gi->end();++it){
    MElement *ele = *it;
    MInterfaceElement* iele = dynamic_cast<MInterfaceElement*>(ele);
  // 2* because IP is duplicated for DG formulation
    int npts_inter=2*this->getInterfaceGaussIntegrationRule()->getIntPoints(ele,&GP);
    AllIPState::ipstateElementContainer tp(npts_inter);
    for(int i=0;i<npts_inter/2;i++){
      mlawMinus->createIPState(tp[i],state,ele,iele->getElem(0)->getNumVertices(),i);
    }
    for(int i=npts_inter/2;i<npts_inter;i++){
      mlawPlus->createIPState(tp[i],state,ele,iele->getElem(1)->getNumVertices(),i);
    }
    map.insert(AllIPState::ipstatePairType(iele->getNum(),tp));
  }
  // Virtual interface element (no duplication)
  const materialLaw *mlaw = this->getMaterialLaw();
  for(groupOfElements::elementContainer::const_iterator it=this->gib->begin(); it!=this->gib->end();++it){
    MElement *ele = *it;
    MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(ele);
    int npts_inter=this->getInterfaceGaussIntegrationRule()->getIntPoints(ele,&GP);
    AllIPState::ipstateElementContainer tp(npts_inter);
    for(int i=0;i<npts_inter;i++){
      mlaw->createIPState(tp[i],state,ele,iele->getElem(0)->getNumVertices(),i);
    }
    map.insert(AllIPState::ipstatePairType(iele->getNum(),tp));
  }
};

void partDomain::setBulkMatrixByPerturbation(const int i, const double eps)
{
  i == 0 ? _bmbp = false : _bmbp = true;
  _eps = eps;
}

void dgPartDomain::numberValuesToTransfertMPI(AllIPState *aips,int* sizeeachrank)const
{
 #if defined(HAVE_MPI)
  IntPt *GP;
  for(groupOfElements::elementContainer::iterator ite=gi->begin(); ite!=gi->end(); ++ite)
  {
    MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(*ite);
    int rankminus = iele->getElem(0)->getPartition()-1;
    int rankplus = iele->getElem(1)->getPartition()-1;
    int rankcomm=-1;
    if(rankminus != Msg::GetCommRank())
    {
       rankcomm = rankminus;
    }
    else if(rankplus !=Msg::GetCommRank())
    {
      rankcomm = rankplus;
    }
    if(rankcomm!=-1)
    {
      int npts=integBound->getIntPoints(*ite,&GP);
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(iele->getNum());
      int nvc=0;
      for(int j=0;j<npts;j++)
      {
         IPStateBase* ipsm = (*vips)[j];
         // take initial but supposed same value to transfert for all (so transfert only once)
         IPVariable *ipvm = static_cast<IPVariableMechanics*>(ipsm->getState(IPStateBase::initial));
         nvc += ipvm->numberValuesToCommunicateMPI();
      }
      if(nvc!=0) // otherwise no value to communicate
        sizeeachrank[rankcomm] += (2+nvc); // as the numbers of element minus and plus have to be communicate
    }
  }
 #endif // HAVE_MPI
}

void dgPartDomain::fillArrayIPvariableMPI(AllIPState *aips,const int rank,double* arrayMPI) const
{
 #if defined(HAVE_MPI)
  // We communicate also the number of each element (to identify later the interface)
  // so by element the number of value to tranfert is equal to 2 + npts
  // Only the value of minus element is transfert after in init term val + = val -
  IntPt *GP;
  int curpos=0;
  for(groupOfElements::elementContainer::iterator ite=gi->begin(); ite!=gi->end(); ++ite)
  {
    MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(*ite);
    int rankminus = iele->getElem(0)->getPartition()-1;
    int rankplus = iele->getElem(1)->getPartition()-1;
    if((rankminus == rank) or (rankplus == rank))
    {
      // write information about the interface element (the interface element number is local --> different on each rank)
      arrayMPI[curpos] = (double) iele->getElem(0)->getNum();
      arrayMPI[curpos+1] = (double) iele->getElem(1)->getNum();
      // get the value of the cohesive strength on minus element of each integration point
      // same for initial, previous and current.
      int npts=integBound->getIntPoints(*ite,&GP);
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(iele->getNum());
      curpos +=2;
      for(int i=0;i<npts;i++)
      {
         IPStateBase* ipsm = (*vips)[i];
         IPVariable  *ipvm = ipsm->getState(IPStateBase::initial);
         ipvm->fillValuesToCommunicateMPI(&arrayMPI[curpos]);
         curpos+=ipvm->numberValuesToCommunicateMPI();
      }
    }
  }
 #endif // HAVE_MPI
}

void dgPartDomain::setIPVariableFromArrayMPI(AllIPState *aips,const int rank,const int arraysize,const double* arrayMPI)const
{
 #if defined(HAVE_MPI)
  int curpos=0;
  IntPt *GP;
  while(curpos<arraysize)
  {
     // get information to find the interelement (the number of the interface element is a local number which differ on each rank)
    int numminus = (int) arrayMPI[curpos];
    int numplus = (int) arrayMPI[curpos+1];
    MElement* elefound=NULL;
    for(groupOfElements::elementContainer::iterator it=gi->begin(); it!=gi->end();++it)
    {
      MInterfaceElement *ie = dynamic_cast<MInterfaceElement*>(*it);
      if((ie->getElem(0)->getNum()==numminus) and (ie->getElem(1)->getNum()==numplus))
      {
        elefound = *it;
        break;
      }
    }
    if(elefound!=NULL){
      int npts=integBound->getIntPoints(elefound,&GP);
      AllIPState::ipstateElementContainer *vips = aips->getIPstate(elefound->getNum());
      curpos+=2;
      for(int i=0;i<npts;i++)
      {
         IPStateBase* ipsm = (*vips)[i];
         // same values for the 3 states
         IPVariable *ipvm = ipsm->getState(IPStateBase::initial);
         ipvm->getValuesFromMPI(&arrayMPI[curpos]);
         ipvm = ipsm->getState(IPStateBase::previous);
         ipvm->getValuesFromMPI(&arrayMPI[curpos]);
         ipvm = ipsm->getState(IPStateBase::current);
         ipvm->getValuesFromMPI(&arrayMPI[curpos]);
         curpos += ipvm->numberValuesToCommunicateMPI();
      }
    }
    else
    {
      Msg::Fatal("Can't find an interelement where IPVariable have to be set. dom %d rank %d",_phys,Msg::GetCommRank());
    }
  }
 #endif // HAVE_MPI
}

void dgPartDomain::initMicroMeshId(){
  // for bulk element
  materialLaw* mlaw = this->getMaterialLaw();
  if(mlaw!=NULL)
  {
    if (mlaw->isNumeric()){
      partDomain::initMicroMeshId();
    }
  }

  if (IsInterfaceTerms()){
    // for interface elements
    materialLaw* mlawMinus = this->getMaterialLawMinus();
    materialLaw* mlawPlus = this->getMaterialLawPlus();

    if (mlawPlus->isNumeric() or mlawMinus->isNumeric()){
      numericalMaterial* nummatMinus = dynamic_cast<numericalMaterial*>(mlawMinus);
      numericalMaterial* nummatPlus = dynamic_cast<numericalMaterial*>(mlawPlus);
      IntPt* GP;
      for (groupOfElements::elementContainer::iterator it = gi->begin(); it!= gi->end(); it++){
        MElement* ele = *it;
        int npts = this->getInterfaceGaussIntegrationRule()->getIntPoints(ele,&GP);
        for (int i=0; i<npts; i++){
          if (mlawMinus->isNumeric() == true && mlawPlus->isNumeric() == false ){
            nummatMinus->assignMeshId(ele->getNum(),i);
          }
          else if (mlawMinus->isNumeric() == false && mlawPlus->isNumeric() ==true){
            nummatPlus->assignMeshId(ele->getNum(),i+npts);
          }
          else {
            if (mlawMinus->getNum() == mlawPlus->getNum()){
              // same material law
              nummatMinus->assignMeshId(ele->getNum(),i);
              nummatPlus->setMeshId(ele->getNum(),i+npts,nummatMinus->getMeshId(ele->getNum(),i));
            }
            else{
              // different material law
              nummatMinus->assignMeshId(ele->getNum(),i);
              nummatPlus->assignMeshId(ele->getNum(),i+npts);
            }
          }
        }
      }
    };
  }
};
