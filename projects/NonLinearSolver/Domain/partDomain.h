//
// C++ Interface: partDomain
//
// Description: Interface class to used solver. Your term ha to be store in a domain
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef PARTDOMAIN_H_
#define PARTDOMAIN_H_
#ifndef SWIG
#include "mlaw.h"
#include "SVector3.h"
#include "MInterfaceElement.h"
#include "groupOfElements.h"
#include "timeFunction.h"
#include "functionSpaceType.h"
#include "nonLinearBC.h"
#include "nlTerms.h"
#include "unknownField.h"
#include "InterfaceBuilder.h"
// class for a domain (pure virtual class)
#endif
class dgPartDomain;
class partDomain{
 public :
//  typedef std::set<nonLinearPeriodicBC*> periContainer;
  typedef std::set<nonLinearConstraintBC*> ucContainer;

 protected :
  int _tag; // tag for the dofManager
  int _phys; // physical of interface group I don't know how to retrieve it from *g
  bool _fullDg; // To know which formulation Cg/Dg or FullDg is used for this part
  functionSpaceType::whichSpace _wsp;
  BilinearTermBase* btermBulk;
  BilinearTermBase* massterm;
  LinearTermBase<double>* ltermBulk;
  QuadratureBase* integBulk;
  // for matrix by perturbation
  bool _bmbp;
  double _eps;
  virtual void setBulkMatrixByPerturbation(const int i, const double eps=1e-8);
  // To know if law is already set
  bool setmaterial;
  // BC are put in domain to be sure to use the good function space
//  periContainer allPeriodic;
  // constraint BC
  ucContainer allConstraint;

  #if defined(HAVE_MPI)
  int _rootRank;
  std::set<int> _otherRanks;
  #endif //HAVE_MPI


 public :
  // Todo protect this variable
  groupOfElements *g; // support for this field
  // add a group of element for prescribed Dirichlet BC of other partition in MPI
  groupOfElements* _groupGhostMPI;

 public:
#ifndef SWIG
  // Constructors
  partDomain(const int tag, const int phys,
             const int ws = 0,const bool fdg=false) : g(0), _tag(tag), _phys(phys),
                                                         _bmbp(false), _eps(1e-8), _fullDg(fdg), setmaterial(false)
  {
    _groupGhostMPI = new groupOfElements();
    switch(ws){
     case 0:
      _wsp = functionSpaceType::Lagrange;
      break;
     case 10000: // Allow to set space later (used for interface domain)
      _wsp = functionSpaceType::Inter;
      break;
     default:
      Msg::Error("Function space type is unknown for partDomain %d. So Lagrange by default");
      _wsp = functionSpaceType::Lagrange;
    }
  }
  partDomain(const partDomain &source);
  partDomain& operator=(const partDomain &source);
//  ~partDomain(){delete btermBulk; delete ltermBulk; delete integBulk;}
  BilinearTermBase* getBilinearBulkTerm() const{return btermBulk;}
  BilinearTermBase* getBilinearMassTerm() const{return massterm;}
  LinearTermBase<double>* getLinearBulkTerm() const{return ltermBulk;}
  QuadratureBase* getBulkGaussIntegrationRule() const{return integBulk;}
  // Dimension of domain
  virtual int getDim() const=0;
  int getTag()const{return _tag;}
  int getPhysical() const{return _phys;}
  // BE AWARE the terms of Neumann BC related to this domain have to be initialized by this method !!
  virtual void initializeTerms(unknownField *uf,IPField *ip,std::vector<nonLinearNeumannBC> &vNeumann)=0;
  // true is domain has interface terms
  virtual bool IsInterfaceTerms() const=0;
  functionSpaceType::whichSpace getFunctionSpaceType() const{return _wsp;}
  // can be return const FunctionSpaceBase if the function of this class are declarated const
  virtual FunctionSpaceBase* getFunctionSpace() const=0;
// some data of BC have to be set by domain
  virtual FunctionSpaceBase* getSpaceForBC(const nonLinearBoundaryCondition &boundary,
                                           const groupOfElements *groupBC) const=0; // for dirichlet, neumann and initial
                                                                                    // groupBC = element of domain where BC is applied
  virtual QuadratureBase* getQuadratureRulesForNeumannBC(nonLinearNeumannBC &neu) const=0;

  virtual void createIPState(AllIPState::ipstateContainer& map, const bool *state) const;


  virtual void addConstraintBC(nonLinearConstraintBC* uc){
    Msg::Info("Add constraint BC for this partDomain");
  };
  void setTimeBC(const double curtime){
    for (ucContainer::iterator it=ucBegin(); it!=ucEnd(); ++it){
      nonLinearConstraintBC *uc= *it;
    };
  }
  ucContainer::iterator ucBegin(){return allConstraint.begin();}
  ucContainer::iterator ucEnd(){return allConstraint.end();}

  virtual void computeIPVariable(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, bool stiff)=0;
  virtual void computeIpv(AllIPState *aips,MElement *e, IPStateBase::whichState ws,
                           materialLaw *mlaw,fullVector<double> &disp, bool stiff)=0;
  virtual void setGaussIntegrationRule()=0;
  virtual bool getFormulation() const{return _fullDg;}
  virtual void setMaterialLaw(const std::map<int,materialLaw*> maplaw)=0;
  // No materialLaw store here but the function to give the material law exist
  // The law has to be store in the derivated class
  virtual materialLaw* getMaterialLaw()=0;
  virtual const materialLaw* getMaterialLaw() const=0;
  virtual int getLawNum() const=0;
  // For dg formulation the stability parameters decrease the critical time step size
  // in explicit. So this function allows to scale the time step
  virtual double scaleTimeStep() const {return 1.;}
  // creation of interface. At least boundary to create interface domain
  // can be empty be not interdomain creation in this case
  virtual void createInterface(manageInterface &maninter)=0;
  virtual MElement* createVirtualInterface(IElement *ie) const=0;
  virtual MElement* createInterface(IElement* ie1,IElement* ie2) const=0;
  // create an interface domain. Use only in mpi case (an interface domain can be given or a new one has to be
                                                       // created if dgdom==NULL)
  virtual void createInterfaceMPI(const int rankOtherPart, const groupOfElements &groupOtherPart,dgPartDomain* &dgdom){
    Msg::Error("To use parallel implementation you have to define createInterfaceMPI on your domain. Or your version is obsoleted to domain has to be given as last argument");
  }
  // Add for BC
  virtual FilterDof* createFilterDof(const int comp) const=0;
  //compute average stress for periodic BC
  virtual void computeAverageStress(const IPField* ipf, STensor3& stress, double& volume) const{
    #ifdef _DEBUG
    Msg::Warning(" Define computeAverageStress on your domain for multiscale problem");
    #endif
  };
  virtual void computeAverageHighOrderStress(const IPField* ipf, STensor33& stress, double& volume) const{
    #ifdef _DEBUG
    Msg::Warning("Define computeAverageHighOrderStress on your domain for multiscale problem");
    #endif
  }
  virtual double  computeVolumeDomain(const IPField* ipf) const{
    #ifdef _DEBUG
    Msg::Warning("Define computeVolumeDomain on your domain for multiscale problem");
    #endif
    return 0;
  };
  virtual partDomain* clone() const{
    #ifdef _DEBUG
    Msg::Warning("Define copyDomain on your domain for multiscale problem");
    #endif
    return NULL;
  }
  #if defined(HAVE_MPI)
  virtual void setWorkingRanks(const int root, const std::set<int>& others);
  virtual int getRootRank() const;
  virtual void getOtherRanks(std::set<int>& others) const;
  #endif //HAVE_MPI
  virtual bool considerForTimeStep(AllIPState *aips,MElement *e) {return true;};
  virtual double computeMeanValueDomain(const int num, const IPField* ipf) const;
  virtual void initMicroMeshId();

#endif // SWIG
};
// class for Dg part domain (pure virtual)
class dgPartDomain : public partDomain{
 protected :
  BilinearTermBase* btermBound;
  LinearTermBase<double>* ltermBound;
  BilinearTermBase* btermVirtBound;
  LinearTermBase<double>* ltermVirtBound;
  QuadratureBase* integBound;
  // For matrix by perturbation
  bool _interByPert;
  bool _virtByPert;

 public :
  // TODO protect these variables
  groupOfElements *gi; // support for the interfaceElement TODO cast to a groupOfElements
  groupOfElements *gib; // support for the interfaceElement TODO cast to a groupOfElements
  groupOfElements *giv; // support for the virtual interface element (used to set Neumann and Dirichlet BC)

 public:
#ifndef SWIG
  dgPartDomain(const int tag, const int phys, const int ws = 0,
               const bool fdg=false) : partDomain(tag,phys,ws,fdg), _interByPert(false), _virtByPert(false)
  {
    gi = new groupOfElements();
    gib = new groupOfElements();
    giv = new groupOfElements();
  }
  dgPartDomain(const dgPartDomain &source);
  dgPartDomain& operator=(dgPartDomain &source);
//  ~dgPartDomain(){delete btermBound; delete ltermBound; delete btermVirtBound; delete ltermVirtBound;
//                  delete integBound; delete gib; delete giv}
  BilinearTermBase* getBilinearInterfaceTerm(){return btermBound;}
  LinearTermBase<double>* getLinearInterfaceTerm()const{return ltermBound;}
  BilinearTermBase* getBilinearVirtualInterfaceTerm(){return btermVirtBound;}
  LinearTermBase<double>* getLinearVirtualInterfaceTerm(){return ltermVirtBound;}
  QuadratureBase* getInterfaceGaussIntegrationRule() const {return integBound;}
  virtual void computeIPVariable(AllIPState *aips,const unknownField *ufield,const IPStateBase::whichState ws, bool stiff)=0;
  virtual void computeIpv(AllIPState *aips,MInterfaceElement *ie, IntPt *GP,const IPStateBase::whichState ws,
                            partDomain* efMinus, partDomain *efPlus,materialLaw *mlawminus,
                            materialLaw *mlawplus,fullVector<double> &dispm,
                            fullVector<double> &dispp,const bool virt, bool stiff,const bool checkfrac=true)=0;
  virtual void computeIpv(AllIPState *aips,MElement *e, IPStateBase::whichState ws,
                            materialLaw *mlaw,fullVector<double> &disp, bool stiff)=0;
  virtual void setGaussIntegrationRule()=0;
  virtual void createIPState(AllIPState::ipstateContainer& map, const bool *state) const;
  virtual int getDim() const=0;
  virtual bool IsInterfaceTerms() const{return true;}
  virtual FunctionSpaceBase* getFunctionSpace() const=0;
  virtual FunctionSpaceBase* getFunctionSpaceMinus() const=0;
  virtual FunctionSpaceBase* getFunctionSpacePlus() const=0;
  virtual void matrixByPerturbation(const int ibulk, const int iinter, const int ivirt,const double eps=1e-8)=0;
  virtual void setMaterialLaw(const std::map<int,materialLaw*> maplaw)=0;
  virtual materialLaw* getMaterialLaw(){Msg::Error("The law to retrieve is not given on a dgdom"); return NULL;}
  virtual const materialLaw* getMaterialLaw() const{Msg::Error("The law to retrieve is not given on a dgdom"); return NULL;}
  virtual materialLaw* getMaterialLawMinus()=0;
  virtual const materialLaw* getMaterialLawMinus() const=0;
  virtual materialLaw* getMaterialLawPlus()=0;
  virtual const materialLaw* getMaterialLawPlus() const=0;
  virtual int getLawNum() const=0;
  virtual int getMinusLawNum() const=0;
  virtual int getPlusLawNum() const=0;
  virtual const partDomain* getMinusDomain() const=0;
  virtual const partDomain* getPlusDomain() const=0;
  virtual partDomain* getMinusDomain()=0;
  virtual partDomain* getPlusDomain()=0;
  virtual void createInterface(manageInterface &maninter)=0;
  virtual MElement* createVirtualInterface(IElement *ie) const=0;
  virtual MElement* createInterface(IElement *ie1, IElement *ie2) const=0;
  // Add for BC
  virtual FilterDof* createFilterDof(const int comp) const=0;

  // as to fill the array (of size Msg::GetCommSize()) with at index i the number of values to communicate with rank i
  // Normally it is an array of zero in input
  virtual void numberValuesToTransfertMPI(AllIPState *aips,int* sizeeachrank)const;
  // fill the vector that will be communicate to rank rank
  virtual void fillArrayIPvariableMPI(AllIPState *aips,const int rank,double* arrayMPI) const;
  virtual void setIPVariableFromArrayMPI(AllIPState *aips,const int rank,const int arraysize,const double* arrayMPI)const;
  virtual void initMicroMeshId();
#endif
};
#endif
