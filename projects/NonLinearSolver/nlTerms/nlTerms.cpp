//
// C++ Interface: terms
//
// Description: Basic term for non linear solver
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "nlTerms.h"
#include "MInterfaceElement.h"
#include "unknownField.h"
#include "ipField.h"
#include "nlsFunctionSpace.h"
template<> void BilinearTermPerturbation<double>::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const
{
  MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(ele);
  std::vector<Dof> R;
  if(_nlterm->isData())
    _nlterm->set(&disp);
  if(iele == NULL){ // compute on element
    int nbdof = space1.getNumKeys(ele);
    m.resize(nbdof,nbdof,false); // set operation after
    disp.resize(nbdof);
    space1.getKeys(ele,R);
    _ufield->get(R,disp);

    // Size of force vector
    fp.resize(nbdof);
    fm.resize(nbdof);

    for(int i=0;i<nbdof;i++){
      // pertubation +
      disp(i) += _eps;
      _dom->computeIpv(_ipf->getAips(),ele,IPStateBase::current,_dom->getMaterialLaw(),disp,false);

      _nlterm->get(ele,npts,GP,fp);
      // perturbation -
      disp(i) -=_twoeps;
      _dom->computeIpv(_ipf->getAips(),ele,IPStateBase::current,_dom->getMaterialLaw(),disp,false);
      _nlterm->get(ele,npts,GP,fm);
      // restore dof value
      disp(i) += _eps;
      fp.axpy(fm,-1);
      m.copyOneColumn(fp,i); // divide by 1/(2eps) at the end
    }
    _dom->computeIpv(_ipf->getAips(),ele,IPStateBase::current,_dom->getMaterialLaw(),disp,true);
  }
  else{
    // get displacement of element
    space1.getKeys(iele->getElem(0),R);
    if(iele->getElem(0) != iele->getElem(1))
      space2.getKeys(iele->getElem(1),R);
    disp.resize(R.size());
    _ufield->get(R,disp);
    dgPartDomain *dgdom = static_cast<dgPartDomain*>(_dom);
    bool virt=false;
    if(iele->getElem(0) == iele->getElem(1)) virt = true;
    // store in dispm and dispp
    int nbdof_m = space1.getNumKeys(iele->getElem(0));
    int nbdof_p = 0;
    dispm.resize(nbdof_m);
    for(int i=0;i<nbdof_m;i++)
      dispm(i) = disp(i);
    if(!virt){
      nbdof_p = space2.getNumKeys(iele->getElem(1));
      dispp.resize(nbdof_p);
      for(int i=0;i<nbdof_p;i++)
        dispp(i) = disp(i+nbdof_m);
      m.resize(nbdof_m+nbdof_p,nbdof_m+nbdof_p);
    }
    else
      m.resize(nbdof_m,nbdof_m,false);
    fp.resize(disp.size()); // use disp size to know the number of dof (here unknow if interafce or virtual interface)
    fm.resize(disp.size());

    // Perturbation on minus element
    for(int i=0;i<nbdof_m;i++){
      // dof perturbation +
      disp(i)+=_eps;
      dispm(i)+=_eps;
      dgdom->computeIpv(_ipf->getAips(),iele,GP,IPStateBase::current,dgdom->getMinusDomain(),dgdom->getPlusDomain(),
                        dgdom->getMaterialLawMinus(),dgdom->getMaterialLawPlus(),dispm,dispp,virt,false,false); // 0 for - elem and npts for + elem
      _nlterm->get(ele,npts,GP,fp);
      // dof perturbation -
      disp(i)-=_twoeps;
      dispm(i)-=_twoeps;
      dgdom->computeIpv(_ipf->getAips(),iele,GP,IPStateBase::current,dgdom->getMinusDomain(),dgdom->getPlusDomain(),
                        dgdom->getMaterialLawMinus(),dgdom->getMaterialLawPlus(),dispm,dispp,virt,false,false); // 0 for - elem and npts for + elem
      _nlterm->get(ele,npts,GP,fm);
      disp(i)+=_eps;
      dispm(i)+=_eps;
      fp.axpy(fm,-1);
      m.copyOneColumn(fp,i); // divide by 1/(2eps) at the end
    }
    // restore ipv value
    dgdom->computeIpv(_ipf->getAips(),iele,GP,IPStateBase::current,dgdom->getMinusDomain(), dgdom->getPlusDomain(),
                      dgdom->getMaterialLawMinus(),dgdom->getMaterialLawPlus(),dispm,dispp,virt,true,false); // 0 for - elem and npts for + elem
    if(!virt){ // Otherwise virtual interface element
      // Perturbation on plus element
      for(int i=0;i<nbdof_p;i++){
        // dof perturbation +
        disp(i+nbdof_m)+=_eps;
        dispp(i)+=_eps;
        dgdom->computeIpv(_ipf->getAips(),iele,GP,IPStateBase::current,dgdom->getMinusDomain(),dgdom->getPlusDomain(),
                          dgdom->getMaterialLawMinus(),dgdom->getMaterialLawPlus(),dispm,dispp,virt,false,false); // 0 for - elem and npts for + elem
        _nlterm->get(ele,npts,GP,fp);
        // dof perturbation +
        disp(i+nbdof_m)-=_twoeps;
        dispp(i)-=_twoeps;
        dgdom->computeIpv(_ipf->getAips(),iele,GP,IPStateBase::current,dgdom->getMinusDomain(),dgdom->getPlusDomain(),
                          dgdom->getMaterialLawMinus(), dgdom->getMaterialLawPlus(),dispm,dispp,virt,false,false); // 0 for - elem and npts for + elem
        _nlterm->get(ele,npts,GP,fm);
        disp(i+nbdof_m)+=_eps;
        dispp(i)+=_eps;
        fp.axpy(fm,-1);
        m.copyOneColumn(fp,i+nbdof_m); // divide by 1/(2eps) at the end
      }
      // restore ipv values
      dgdom->computeIpv(_ipf->getAips(),iele,GP,IPStateBase::current,dgdom->getMinusDomain(),dgdom->getPlusDomain(),
                        dgdom->getMaterialLawMinus(),dgdom->getMaterialLawPlus(),dispm,dispp,virt,true, false);
    }
  }
  // divide all components by 1/2eps
  m.scale(_onedivtwoeps);
}

void mass3D::get(MElement *ele,int npts,IntPt *GP,fullMatrix<double> &m) const{
  // the weight of 6 nodes has to be prescribed 1/32 at corner and 7/48 for middle nodes
  nlsFunctionSpaceXYZ<double> *sp1 = static_cast<nlsFunctionSpaceXYZ<double>*>(&(this->space1));
  std::vector<std::vector<TensorialTraits<double>::GradType> >vgradsuvw;
  // Get the value at all Gauss points
  sp1->getgradfuvw(ele,npts,GP,vgradsuvw);
  double jac[3][3];

  if(ele->getTypeForMSH() == MSH_TRI_6){
    // integration on Gauss' points to know area
    double area = 0.;
    double onediv12 = 1./12.;
    int nbdof = BilinearTerm<double,double>::space1.getNumKeys(ele);
    int nbFF = ele->getNumShapeFunctions();
    int dim = nbdof/nbFF;
    m.resize(nbdof, nbdof,true); // true --> setAll 0.

    // get the shape function (and derivative values)
    nlsFunctionSpaceUVW<double>* sp1 = static_cast<nlsFunctionSpaceUVW<double>*>(&(this->space1));
    std::vector<GaussPointSpaceValues<double>*> vgps;
    sp1->get(ele,npts,GP,vgps);
    for (int i = 0; i < npts; i++)
    {
      double weight = GP[i].weight;
      double detJ = ele->getJacobian(vgradsuvw[i],jac);
      area+=weight*detJ;
    }
    double massele = area*_rho; // rho is the density by unit thickness
    // mass in three directions
    for(int kk=0;kk<dim;kk++){
       m(kk*nbFF,kk*nbFF) += onediv12*massele;
       m(1+kk*nbFF,1+kk*nbFF) += onediv12*massele;
       m(2+kk*nbFF,2+kk*nbFF) += onediv12*massele;
       m(3+kk*nbFF,3+kk*nbFF) += 0.25*massele;
       m(4+kk*nbFF,4+kk*nbFF) += 0.25*massele;
       m(5+kk*nbFF,5+kk*nbFF) += 0.25*massele;
    }
  }
  else if(ele->getTypeForMSH() == MSH_TET_10){
    // integration on Gauss' points to know area
    double vol = 0.;
    double onediv32 = 1./32.;
    int nbdof = BilinearTerm<double,double>::space1.getNumKeys(ele);
    int nbFF = ele->getNumShapeFunctions();
    m.resize(nbdof, nbdof,true); // true --> setAll 0.
    //double jac[3][3];
    for (int i = 0; i < npts; i++)
    {
      // Coordonate of Gauss' point i
      //double u = GP[i].pt[0]; double v = GP[i].pt[1]; double w = GP[i].pt[2];
      // Weight of Gauss' point i
      double weight = GP[i].weight;
      //double detJ = ele->getJacobian(u, v, w, jac);
      double detJ = ele->getJacobian(vgradsuvw[i],jac);
      vol+=weight*detJ;
    }
    double massele = vol*_rho;
    // mass in three directions
    for(int kk=0;kk<3;kk++){
       m(kk*nbFF,kk*nbFF) += onediv32*massele;
       m(1+kk*nbFF,1+kk*nbFF) += onediv32*massele;
       m(2+kk*nbFF,2+kk*nbFF) += onediv32*massele;
       m(3+kk*nbFF,3+kk*nbFF) += onediv32*massele;
       m(4+kk*nbFF,4+kk*nbFF) += 7.*massele/48.;
       m(5+kk*nbFF,5+kk*nbFF) += 7.*massele/48.;
       m(6+kk*nbFF,6+kk*nbFF) += 7.*massele/48.;
       m(7+kk*nbFF,7+kk*nbFF) += 7.*massele/48.;
       m(8+kk*nbFF,8+kk*nbFF) += 7.*massele/48.;
       m(9+kk*nbFF,9+kk*nbFF) += 7.*massele/48.;
    }
  }
  else if(ele->getTypeForMSH() == MSH_HEX_8){
    // integration on Gauss' points to know area
    double vol = 0.;
    double onediv8 = 1./8.;
    int nbdof = BilinearTerm<double,double>::space1.getNumKeys(ele);
    int nbFF = ele->getNumShapeFunctions();
    m.resize(nbdof, nbdof,true); // true --> setAll 0.
    for (int i = 0; i < npts; i++)
    {
      // Coordonate of Gauss' point i
//      double u = GP[i].pt[0]; double v = GP[i].pt[1]; double w = GP[i].pt[2];
      // Weight of Gauss' point i
      double weight = GP[i].weight;
//      double detJ = ele->getJacobian(u, v, w, jac);
      double detJ = ele->getJacobian(vgradsuvw[i], jac);

      vol+=weight*detJ;
    }
    double massele = vol*_rho;
    // mass in three directions
    for(int kk=0;kk<3;kk++)
    {
       m(kk*nbFF,kk*nbFF) += onediv8*massele;
       m(1+kk*nbFF,1+kk*nbFF) += onediv8*massele;
       m(2+kk*nbFF,2+kk*nbFF) += onediv8*massele;
       m(3+kk*nbFF,3+kk*nbFF) += onediv8*massele;
       m(4+kk*nbFF,4+kk*nbFF) += onediv8*massele;
       m(5+kk*nbFF,5+kk*nbFF) += onediv8*massele;
       m(6+kk*nbFF,6+kk*nbFF) += onediv8*massele;
       m(7+kk*nbFF,7+kk*nbFF) += onediv8*massele;
    }
  }
  else{
    if(sym){
      // Initialization of some data
      int nbdof = BilinearTerm<double,double>::space1.getNumKeys(ele);
      int nbFF = ele->getNumShapeFunctions();
      int dim = nbdof/nbFF;
      m.resize(nbdof, nbdof,true); // true --> setAll 0.
      // sum on Gauss' points
      std::vector<std::vector<TensorialTraits<double>::ValType> > vvals;
      sp1->get(ele,npts,GP,vvals);
      for (int i = 0; i < npts; i++)
      {
        // Coordonate of Gauss' point i
//        double u = GP[i].pt[0]; double v = GP[i].pt[1]; double w = GP[i].pt[2];
        // Weight of Gauss' point i
        double weight = GP[i].weight;
//        double detJ = ele->getJacobian(u, v, w, jac);
        double detJ = ele->getJacobian(vgradsuvw[i], jac);
//        Vals.clear();
//        BilinearTerm<double,double>::space1.fuvw(ele,u, v, w, Vals);
        std::vector<TensorialTraits<double>::ValType> &Vals = vvals[i];
        for(int j=0;j<nbFF; j++)
        {
          for(int k=0;k<nbFF;k++){
            // same mass in the 3 directions
            double mij = detJ*weight*_rho*Vals[j]*Vals[k];
            for(int kk=0;kk<dim;kk++)
              m(j+kk*nbFF,k+kk*nbFF) += mij;
          }
        }
      }
 //     m.print("mass");
   }
   else
     Msg::Error("Mass matrix is not implemented for dg3D with two different function spaces");
  }
}
