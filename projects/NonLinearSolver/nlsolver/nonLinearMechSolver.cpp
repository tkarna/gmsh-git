
//
//
// Description: Non linear solver for mechanic problems
//              quasi-static implicit scheme & dynamic explicit Hulbert-Chung scheme
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#include <string.h>
#include <cstdlib>
#include <ctime>
#include <time.h>
#include <cstdio>
#include "GmshConfig.h"
#include "nonLinearMechSolver.h"
#include "linearSystemCSR.h"
#include "linearSystemPETSc.h"
#include "linearSystemGMM.h"
#include "Numeric.h"
#include "nlTerms.h"
#include "solverAlgorithms.h"
#include "quadratureRules.h"
#include "MPoint.h"
#include "ipstate.h"
#include "ipField.h"
#include "timeFunction.h"
#include "GModel.h"
#include "explicitHulbertChungPETSc.h"
#include "explicitHulbertChungBLAS.h"
#include "nlsolAlgorithms.h"
#include "nonLinearMechSolver.h"
#include "MTriangle.h"
#include "MQuadrangle.h"
#include "energyField.h"
#include "nlsolAlgorithms.h"
#include "solverAlgorithms.h"
#include "quadratureRules.h"
#include "MPoint.h"
#include "timeFunction.h"
#include "pbcDofManager.h"
#include "staticDofManager.h"
#include "dofManagerMultiSystems.h"
#include "contactTerms.h"
#include "NonLinearSolverConfig.h"
#include "currentConfig.h"
#include "MInterfaceLine.h"
#include "fragment.h"
#include "OS.h"
#include "pathFollowingSystem.h"
#include "StringUtils.h"
#include "Context.h"
#include "HighOrder.h"
#include "implicitHulbertChungPETSc.h"
#include "pathFollowingSystemPETSc.h"
#include "pbcPathFollowingSystemPETSc.h"
//#include "linearSystemGMRESk.h"

#if defined(HAVE_POST)
#include "PView.h"
#include "PViewData.h"
#endif

#if defined(HAVE_MPI)
#include "elementFilterMPI.h"
#include "nlmpiAlgorithms.h"
#endif // HAVE_MPI

 nonLinearMechSolver::nonLinearMechSolver(int tag, bool isParallel) : _tag(tag), _dim(0), pAssembler(NULL), _numstepExpl(1), _numstepImpl(1),
                                                                      endtime(1.), _tol(1.e-6), nsba(1),whatSolver(nonLinearMechSolver::Gmm),
                                                                      whatScheme(nonLinearMechSolver::StaticLinear),
                                                                      _beta(0.), _gamma(0.5), _alpham(0.), _alphaf(1.),_rhoinfty(1.), _gammas(0.6666),
                                                                      _mpiUserDom(0), _maxNRite(15),_pathFollowing(false),_macroControlType(1),
                                                                      _timeStepFactorReduction(2.),_maxAttemptStepReduction(6), _niteTimeStepIncrease(5),
                                                                      _crackTrackingFile(NULL), _mpiDofManager(false), _previousInit(false),
                                                                      _unresetedBC(false),_unresetedContact(false), _ipf(NULL), _meshFileName(""),
                                                                      _dynamicRelaxation(false),_previousScheme(StaticNonLinear),_criterionFile(NULL),
                                                                      _microFlag(false),_ufield(NULL),_pAl(NULL), _multiscaleFlag(false), _pbcNodeData(NULL),
                                                                      _periodicBC(NULL), _extractPerturbationToFile(false), _sameStateCriterion(1e-50),
                                                                       _controlStateFile(NULL), _currentState(NULL),_initialState(NULL),_whichState(true),
                                                                      _condensation(NULL),_eigenSolver(NULL),_eigflag(false),_energField(NULL),
                                                                      _stressflag(true),_tangentflag(false),_enum(0),_gnum(0),_rveVolume(0.),
                                                                      _systemType(nonLinearMechSolver::MULT_ELIM),_controlType(nonLinearMechSolver::LOAD_CONTROL),
                                                                      _highOrderFlag(false), _eigenValueFile(NULL), _perturbationfactor(0.1),
                                                                      _archive(true),_archivecounter(0),_stiffEstimation(true),_stiffnessModification(true),
                                                                      _iterativeNR(true),_messageView(false), _outputFile(NULL), _eigenFollowing(false),
                                                                      _homogenizeStressMethod(nonLinearMechSolver::VOLUME),
                                                                      _homogenizeTangentMethod(nonLinearMechSolver::PERTURB), _isHommStrainSaveToFile(true),
                                                                      _isHommProSaveToFile(true), _tangentPerturbation(1.e-8), _perturbate(false),
                                                                      _fragmentationFile(NULL), _energyComputation(1), _fractureEnergyComputation(0),
                                                                      _isaRestart(false), _secondToRestart(0.), _checkRestart(false), _interRestart(0.),
                                                                      _timeStepByBenson(false),_lineSearch(false),_strainMap(NULL),_strainMappingFollwing(true),
                                                                      _GmshOneLabViewNum(0), _isWriteDeformedMeshToFile(false)
{
  // check parallelization of dofManager
 #if defined(HAVE_MPI)
 if(isParallel and Msg::GetCommSize() > 1)
   _mpiDofManager = true;
 #endif // HAVE_MPI

  // default view (displacement)
  unknownView.push_back(nlsField::dataBuildView("displacement",-1,nlsField::crude,1,false));

  // initialize c++ function rand (already made in gmsh ?? put elsewhere ??)
  _beginTime = time(NULL);
  srand(_beginTime);
}

nonLinearMechSolver::~nonLinearMechSolver()
  {
    if (pAssembler) delete pAssembler;
    if (_crackTrackingFile != NULL) fclose(_crackTrackingFile);
    if (_fragmentationFile != NULL) fclose(_fragmentationFile);
    this->resetBoundaryConditions();
    this->resetContactInteraction();

    if (_strainMap) delete _strainMap;

    if (_periodicBC) delete _periodicBC;
    if (_pAl) delete _pAl;
    if (_condensation) delete _condensation;
    if (_eigenSolver) delete _eigenSolver;


    if (_multiscaleFlag){
      if (_ufield) delete _ufield;
      if (_energField) delete _energField;
    }





//    if(_ipf != NULL) delete _ipf;
//    for(std::map<int,materialLaw*>::iterator it = maplaw.begin(); it!=maplaw.end();++it){ delete it->second;}
    // I can't delete otherwise problem at the end since swig.
//    for(std::vector<partDomain*>::iterator it = domainVector.begin(); it!=domainVector.end(); ++it){delete *it;}
  // proper finalize?
  if(!_microFlag) // only on the master solver?
    Msg::Exit(0);

  }

void nonLinearMechSolver::createRestart(const int day,const int hour,const int minute,const int second)
{
  _checkRestart = true;
  _secondToRestart = (double)(second+60.*minute+3600.*(hour+day*24.));
  _interRestart = _secondToRestart;
}

bool nonLinearMechSolver::createRestart()
{
  if(!_checkRestart) return false;
  size_t ttime = time(NULL);
  double nsec = difftime(ttime,_beginTime);
  if(nsec > _secondToRestart)
  {
    _secondToRestart += _interRestart;
    return true;
  }
  return false;
}

void nonLinearMechSolver::stepBetweenArchiving(const int n)
{
  nsba = n;
  unknownView[0].nbstepArch=n;
  if(_previousInit) // same values for all change this ?
  {
    for(int i=0;i<ipView.size();i++)
    {
      ipView[i].nbstepArch = nsba;
    }
    for(int i=1;i<unknownView.size();i++)
    {
      unknownView[i].nbstepArch = nsba;
    }
  }
}

void nonLinearMechSolver::setFactorOnArchivingFiles(const int fact)
{
  // read all the csv (disp, IP forces) files and put a factor on it
  //disp
  for(int i=0;i<anoded.size();i++)
  {
    anoded[i].nstep*=fact;
  }

  // IP
  if(_ipf!=NULL) _ipf->setFactorOnArchivingFiles(fact);

  // force
  for(int i=0;i<vaf.size();i++)
  {
    vaf[i].nstep*=fact;
  }
}

// nonLinearMechSolver
void nonLinearMechSolver::loadModel(const std::string &meshFileName)
{
  pModel = new GModel();
  pModel->setAsCurrent();
  pModel->readMSH(meshFileName.c_str());
  _meshFileName = meshFileName;
  this->commonModel();
}

void nonLinearMechSolver::commonModel()
{
  _dim = pModel->getNumRegions() ? 3 : 2;
 #if defined(HAVE_MPI)
  std::set<int>& mshPart = pModel->getMeshPartitions();
  // int map of ranks
  if (!_microFlag)
    initMapRanks(mshPart);

   // find the maximal number
  int partMax = 0;
  for(std::set<int>::const_iterator it = mshPart.begin(); it!=mshPart.end(); ++it)
  {
    if((*it) > partMax) partMax = (*it);
  }
  Msg::Info("Number of mesh partition %d for %d cpu(s)",partMax,Msg::GetCommSize());
  if(partMax > Msg::GetCommSize())
  {
    Msg::Fatal("Your mesh is wrong partitioned");
  }
  else if(partMax !=0 and Msg::GetCommSize() == 1)
  {
    Msg::Fatal("Cannot perform a serial computation with a partitionned mesh");
  }
  else if(partMax > 0 and partMax < Msg::GetCommSize())
  {
    Msg::Warning("You have more cpu than mesh partition so some cpus have no work");
  }
 #endif // HAVE_MPI
}


void nonLinearMechSolver::createModel(const std::string &geoFileName, const std::string &outFileName,
                                      const int dim,const int order,const bool incomplete)
{
  // Overwrite options
  CTX::instance()->mesh.order = order;
  CTX::instance()->mesh.secondOrderIncomplete = incomplete;
  // Use version 3 of mesh to avoid a renmbering of the elements
  CTX::instance()->mesh.mshFileVersion = 3.0;
  if(Msg::GetCommSize()>1 and _microFlag == false)
  {
    Msg::Fatal("The option of creating mesh on the fly from a geo file is not yet allowed for parallel simulation. Please use loadModel instead");
  }
  pModel = new GModel();
  pModel->setAsCurrent();
  pModel->setFileName(geoFileName);
  pModel->setName(SplitFileName(geoFileName)[1]);
  pModel->readGEO(geoFileName);
  pModel->mesh(dim);

  if(!outFileName.empty()) // otherwise the mesh file is not written on disc
  {
      pModel->writeMSH(outFileName, CTX::instance()->mesh.mshFileVersion,CTX::instance()->mesh.binary, CTX::instance()->mesh.saveAll,
                       CTX::instance()->mesh.saveParametric, CTX::instance()->mesh.scalingFactor);
  }
  _meshFileName = outFileName;
  this->commonModel();
}

void nonLinearMechSolver::createMicroModel(const std::string geoFile, const int dim, const int order,
                        const bool incomplte){
  std::string mshFile = "E_"+int2str(_enum)+"_GP_"+int2str(_gnum)+".msh";
  CTX::instance()->mesh.algo2d = ALGO_2D_FRONTAL_QUAD;
  this->createModel(geoFile,mshFile,dim,order,incomplte);
};

void nonLinearMechSolver::initMapRanks(const std::set<int>& parts){
  _mapRanks.clear();
  std::vector<int> macroProcs;
  int partSize = parts.size();
  for (int i=0; i< partSize; i++)
    macroProcs.push_back(i);
  if (macroProcs.size() == 0)
    macroProcs.push_back(0);
  int macroSize = macroProcs.size();
  int allSize = Msg::GetCommSize();
  int distSize = allSize - macroSize;
  int div = distSize/macroSize;
  for (int i=0; i<macroSize; i++){
    int root = macroProcs[i];
    std::set<int> others;
    for (int j=0; j<div; j++){
      int num = macroSize+i*div+j;
      others.insert(num);
    }
    _mapRanks[root] = others;
  }
  int j = macroSize+ div*macroSize;
  while (j< allSize){
    for (int i=0; i< macroSize; i++){
      int root = macroProcs[i];
      std::set<int>& others =_mapRanks[root];
      others.insert(j);
      j++;
      if (j>=allSize) break;
    }
  }
};

bool nonLinearMechSolver::isMultiscale() const{
  bool isMult = false;
  for (std::map<int,materialLaw*>::const_iterator it = maplaw.begin(); it!= maplaw.end(); it++){
    materialLaw* mlaw = it->second;
    if (mlaw != NULL){
      if (mlaw->isNumeric()){
        isMult = true;
        break;
      }
    }
  }
  return isMult;
};

bool nonLinearMechSolver::isDgDomain() const{
  bool fullDG = false;
  for (int i=0; i<domainVector.size(); i++){
    partDomain* dom = domainVector[i];
    if (dynamic_cast<dgPartDomain*>(dom)){
      fullDG = true;
      break;
    }
  }
  return fullDG;
};

void nonLinearMechSolver::init(){

  if(!_previousInit)
  {
    // Set the material law for each domain and the gauss integration rule
    for(std::vector<partDomain*>::iterator it = domainVector.begin(); it!=domainVector.end(); ++it){
      partDomain *dom = *it;
      dom->setMaterialLaw(maplaw);
    }

    // create interfaceElement
    this->createInterfaceElement();

    for(std::vector<partDomain*>::iterator it = domainVector.begin(); it!=domainVector.end(); ++it){
      partDomain *dom = *it;
      dom->setGaussIntegrationRule();
    }
  }
  else  // move all archiving data in a folder to start a new analysis
        // keep only the previous one ( --> not valid if 2 switches)
        // Linux only ?? (implementation MAC and Windows)
  {
   #if defined(HAVE_MPI)
    if(Msg::GetCommSize() > 1)
    {
       // get the host name of each rank
       const char* myhostname = GetEnvironmentVar("HOSTNAME");
       int* vsizename = new int[Msg::GetCommSize()];
       int* vallsizename = new int[Msg::GetCommSize()];
       for(int i=0;i<Msg::GetCommSize();i++)
       {
          vallsizename[i] = 0;
          vsizename[i] = 0;
       }
       vsizename[Msg::GetCommRank()] = strlen(myhostname);
       MPI_Allreduce(vsizename,vallsizename,Msg::GetCommSize(),MPI_INT,MPI_SUM,MPI_COMM_WORLD);
       delete[] vsizename;

       char** vhostname = new char*[Msg::GetCommSize()];
       for(int i=0;i<Msg::GetCommSize();i++)
       {
           vhostname[i] = new char[vallsizename[i]];
           if(Msg::GetCommRank()==i)
           {
             vhostname[i] = (char*)myhostname;
           }
           MPI_Bcast(vhostname[i],vallsizename[i],MPI_CHAR,i,MPI_COMM_WORLD);
       }

       // At this point all processes have an array with the host name of all processes
       // if the host name of the rank doesn't match lower rank files are deleted
       bool firsthostname=true;
       for(int i=0;i<Msg::GetCommRank();i++){
         if(!strcmp(myhostname,vhostname[i]))
         {
            firsthostname = false;
            break;
         }
       }
       if(firsthostname)
       {
         Msg::Error("File are moved on rank %d, hostname %s",Msg::GetCommRank(),vhostname[Msg::GetCommRank()]);
         system("rm -rf previousScheme"); // reserved name folder ?
         system("mkdir previousScheme");
         system("mv *.csv previousScheme"); // no support for other format for now
         system("mv *.msh previousScheme");
         // get back the mesh file
         if(!_meshFileName.empty())
         {
           std::string backMesh = "cp previousScheme/" + _meshFileName + " .";
           system(backMesh.c_str());
         }
       }
       else
       {
         Msg::Error("Nothing moved on rank %d, hostname %s",Msg::GetCommRank(),vhostname[Msg::GetCommRank()]);
       }
       Msg::Barrier();
       // delete array
       delete[] vhostname;
       delete[] vallsizename;
    }
    else
   #endif // HAVE_MPI
    {
      system("rm -rf previousScheme"); // reserved name folder ?
      system("mkdir previousScheme");
      system("mv *.csv previousScheme"); // no support for other format for now
      system("mv *.msh previousScheme");
      // get back the mesh file
      if(!_meshFileName.empty())
      {
        std::string backMesh = "cp previousScheme/" + _meshFileName + " .";
        system(backMesh.c_str());
      }
    }
  }
  if(whatScheme==Explicit or whatScheme==Multi)
    this->initMapMElementToComputeTimeStep();
  // find the domain associated to a physical slave number of a rigid contact interaction
  // WARNING a contact interaction cannot be defined on two domains in the same time
  // a physical group has to be define by domain
  if(!_unresetedContact){
    this->initContactInteraction();
    _unresetedContact =true;
  }

  if(!_unresetedBC){
    this->setPairSpaceElementForBoundaryConditions();
    _unresetedBC = true;
  }

  // initialisation of archiving force
  this->initArchiveForce();
  this->initArchivePathFollowing();
}

void nonLinearMechSolver::init2(unknownField* &ufield, energeticField* &efield)
{
  // initialize the system and dofManager
  this->createSystem();

  // Boundary Conditions and Dof creation
  this->fillConstraintDof();
  this->applyConstraintBC(pAssembler);
  this->applyPBCBetweenTwoGroups(pAssembler);
  this->applySameDispBC(pAssembler);
  this->applyFixOnFace(pAssembler);
  if(whatScheme!=StaticLinear)
   this->setTimeForBC(0.);
  else
   this->setTimeForBC(1.);
  //for periodic BC
  if (_microFlag){
    this->initializeLocalImplementationMethod();
  };
  Msg::Info("Fix and number Dofs");
  this->fixNodalDofs();
  if(!_previousInit)
    this->numberDofs();
  Msg::Info("Dofs are fixed and numbered");


  /* MPI init */
 #if defined(HAVE_MPI)
  if(whatScheme!=StaticLinear and Msg::GetCommSize()>1)
  {
    staticDofManager<double>* sAssembler = static_cast<staticDofManager<double>*>(pAssembler);
    Msg::Info("MPI initialization begins");
    sAssembler->systemMPIComm(); // To initiate the MPI Communication
    Msg::Info("MPI initialization OK");
  }
 #endif // HAVE_MPI

 /* initialization of Dof value */
  // so now initial condition can be given to system
  this->setInitialCondition();


  /* fields creation */
  ufield = new unknownField(pAssembler,domainVector,_ghostDomainMPI,&_allContact,3,anoded,unknownView);
  //  this->restart(ufield); // CHANGE THIS

  bool stiffcomputation=true;
  if(whatScheme==Explicit) stiffcomputation=false;
  if(!_previousInit){
    Msg::Info("Creation of IPVariable");
    _ipf = new IPField(&domainVector,pAssembler,ufield,vaip,ipView); // Field for GaussPoint
    Msg::Info("IPVariable are created");
    _ipf->compute1state(IPStateBase::initial,stiffcomputation);
    _ipf->initialBroken(pModel, initbrokeninter);
    if(whatScheme==StaticNonLinear)
      _ipf->copy(IPStateBase::initial,IPStateBase::previous);
    else
      _ipf->copy(IPStateBase::initial,IPStateBase::current);
  }
  else{ // as nextStep is performed at the end of previous scheme the last step is in previous (and not in current) !!
    _ipf->resetUnknownField(ufield);
    _ipf->copy(IPStateBase::previous,IPStateBase::initial);
    _ipf->copy(IPStateBase::previous,IPStateBase::current); // same value to avois initialization problem ??
    _ipf->openArchivingFiles();
  }
  efield = new energeticField(_ipf,ufield,pAssembler,domainVector,_allContact,allNeumann,energyView,_energyComputation,_fractureEnergyComputation);

  /* terms initialization */
  this->initTerms(ufield,_ipf);
  _previousInit = true;
  Msg::Info("End of initialization");
 #if defined(HAVE_MPI) && defined(_DEBUG)
  if(Msg::GetCommSize() > 1)
  {
    Msg::Error("End init OK from rank %d wait all in debug",Msg::GetCommRank());
    Msg::Barrier();
  }
 #endif // HAVE_MPI
}

void nonLinearMechSolver::endOfScheme(unknownField *ufield,energeticField *efield,const double curtime, const int step)
{
  this->postproFragment(_ipf);

  if (_microFlag){
    _homogenizedFiles.closeFiles();
  }

  if (_strainMap && !_strainMappingFollwing){
    Msg::Info("Create displacement and strain mapping");
    _strainMap->solve(pAssembler);
    std::string filename = "dispMap"+int2str(step+1)+".msh";
    _strainMap->buildDisplacementView(filename);
    filename = "strainMap"+int2str(step+1)+".msh";
    _strainMap->buildStrainView(filename);
  }

  if(whatScheme!=StaticLinear)
    ufield->forceView();
  ufield->archive(curtime,step);
  ufield->closeFile();
   _ipf->forceView();
  _ipf->archive(curtime,step);
  _ipf->closeFile();
  this->endArchiveForce();
  this->endArchivePathFollowing();
  if(efield !=NULL)
  {
    efield->forceView();
    efield->archive(curtime,step);
    efield->closeFile();
    delete efield;
  }
  delete ufield;
 #if defined(HAVE_MPI)
  Msg::Barrier(); // Wait all before new computation (MPI problem ??)
 #endif // HAVE_MPI
}

void nonLinearMechSolver::oneStepPreSolve(const double curtime, const double timestep)
{
  this->setTimeForBC(curtime);
  this->setTimeForLaw(curtime,timestep);
  //for periodic BC
  if (_microFlag) this->applyMicroBCByLocalImplementationMethod();
  this->fixNodalDofs();
}

void nonLinearMechSolver::oneStepPostSolve(unknownField* ufield, energeticField* efield,const double curtime, const int numstep)
{
  staticDofManager<double> *sAssembler = static_cast<staticDofManager<double>*>(pAssembler);

  if (_microFlag && !_multiscaleFlag){
    this->extractAverageProperties(_tangentflag);
    this->homogenizedDataToFile(curtime);
  }
  /* Archiving */
  ufield->archive(curtime,numstep);
  _ipf->archive(curtime,numstep);
  efield->archive(curtime,numstep);
  this->crackTracking(curtime,ufield,_ipf);
  this->forceArchiving(curtime,numstep,sAssembler);   // Edge force value;
  this->pathFollowingArchiving(curtime,numstep,pAssembler);
  this->computeIPCompOnDomain(curtime);

  /* restart */
  if(this->createRestart())
  {
    sAssembler->createRestart();
  }

  /* field next step */
  _ipf->nextStep(curtime); //ipvariable
  sAssembler->nextStep();
}

void nonLinearMechSolver::initContactInteraction(){
  // find a common element between slave group of element and the group of element of partdomain
  bool flagfind;
  for(contactContainer::iterator it = _allContact.begin(); it!=_allContact.end(); ++it){
    contactDomain *cdom = *it;
    // get physical of domain
    int physSlave = cdom->getPhysSlave();
    flagfind=false;
    for(int kk=0;kk<_mpiUserDom; kk++){
      partDomain *dom = domainVector[kk];
      if(dom->getPhysical() == physSlave){
        flagfind =true;
        cdom->setDomainAndFunctionSpace(dom);
        break;
      }
    }
    if(!flagfind){
     #if defined(HAVE_MPI)
      // find the domain in the Ghosted domain (the search on an empty group OK)
      for(int kk=0; kk<_ghostDomainMPI.size(); kk++)
      {
        partDomain *dom = _ghostDomainMPI[kk];
        if(dom->getPhysical() == physSlave){
          flagfind =true;
          cdom->setDomainAndFunctionSpace(dom);
          break;
        }
      }
      if(!flagfind)
     #endif // HAVE_MPI
      {
        Msg::Error("The contact interaction %d can't be initiated because the domain is not find on rank %d",cdom->getPhys(),Msg::GetCommRank());
      }
    }
  }

  // now init the contact boundary conditions
  for(std::vector<rigidContactBC>::iterator it=allContactBC.begin(); it!=allContactBC.end(); ++it){
    rigidContactBC &diri = *it;
    if(diri.onWhat == nonLinearBoundaryCondition::RIGIDCONTACT){
      for(contactContainer::iterator itcdom = _allContact.begin(); itcdom!=_allContact.end(); ++itcdom){
        contactDomain *cdom = *itcdom;
        if(cdom->getPhys() == diri._tag){ // we can have some slave domain with the same master --> don't put a break in the loop !
          rigidContactSpaceBase *sp = static_cast<rigidContactSpaceBase*>(cdom->getSpace());
          diri.setSpace(sp);
          diri._filter = cdom->getDomain()->createFilterDof(diri._comp);
        }
      }
    }
  }
}

void nonLinearMechSolver::setPairSpaceElementForBoundaryConditions()
{
  // The current GModel has to be selected to create the BC
  GModel* save_current_model = GModel::current();
  GModel::setCurrent(pModel);
  // loop on BC
  // BC which are prescribed weakly (the interfaceElement has to be finded in vinter)
  // to prescribed the BC
  // Theta (normally on Boundary only --> impossible that the condition be on 2 partitions)
  for(std::vector<nonLinearNeumannBC>::iterator it=allTheta.begin(); it!=allTheta.end(); ++it){
     nonLinearNeumannBC &neu = *it;
     for(groupOfElements::elementContainer::iterator ite=neu.g->begin(); ite!=neu.g->end(); ++ite){
       MVertex *vv = (*ite)->getVertex(2);
       for(int k=0;k<domainVector.size(); k++){
         partDomain *dom = domainVector[k];
         if(dom->IsInterfaceTerms()){ // change this (virtual interface has to be defined on domain)
           // Ok because 2nd degree min BoundaryInterfaceElement is created only for this vertex
           dgPartDomain *dgdom = static_cast<dgPartDomain*>(dom);
           groupOfElements *gvi = dgdom->giv;
           for(groupOfElements::elementContainer::const_iterator it_inter = gvi->begin(); it_inter!=gvi->end();++it_inter){
             MElement *minter = *it_inter;
             //loop on component of map_edge
             int numvertexminus1 = minter->getNumVertices()-1;
             if((vv == minter->getVertex(2)) or (vv == minter->getVertex(numvertexminus1))){
               dgdom->gib->insert(minter);
               break;
             }
           }
         }
       }
     }
   }


  //Dirichlet (can be applied on each domain look on ghost element to prescribed BC on them avoid a search in unknown map at each iteration)
  for(int i=0;i<allDirichlet.size();i++){
    // first look if the BC is not applied on whole domain
    bool domfind=false;
    for(std::vector<partDomain*>::iterator itdom=domainVector.begin(); itdom!=domainVector.end(); ++itdom){
      partDomain *dom = *itdom;
      if(allDirichlet[i]._tag == dom->getPhysical()){
        domfind = true;
        if(dom->_groupGhostMPI->size() > 0) // save BC to applied an other groupOfElement
        {
          FunctionSpaceBase *spdbc = dom->getSpaceForBC(allDirichlet[i],dom->_groupGhostMPI);
          FilterDof *fdof = dom->createFilterDof(allDirichlet[i]._comp);
          allDirichlet[i]._vspace.push_back(spdbc);
          allDirichlet[i]._vgroup.push_back(dom->_groupGhostMPI);
          allDirichlet[i]._vfilter.push_back(fdof);
          allDirichlet[i]._vdg.push_back(dom->getFormulation());
        }
        FunctionSpaceBase *spdbc = dom->getSpaceForBC(allDirichlet[i],dom->g);
        FilterDof *fdof = dom->createFilterDof(allDirichlet[i]._comp);
        allDirichlet[i]._vspace.push_back(spdbc);
        allDirichlet[i]._vgroup.push_back(dom->g);
        allDirichlet[i]._vfilter.push_back(fdof);
        allDirichlet[i]._vdg.push_back(dom->getFormulation());
        break;
      }
    }
    #if defined(HAVE_MPI)
    // same on ghost domain (be aware copy past of loop on domain !!)
    bool ghostdomfind=false;
    for(std::vector<partDomain*>::iterator itdom=_ghostDomainMPI.begin(); itdom!=_ghostDomainMPI.end(); ++itdom){
      partDomain *dom = *itdom;
      if(allDirichlet[i]._tag == dom->getPhysical() and dom->_groupGhostMPI->size()>0){
        ghostdomfind = true;
	    FunctionSpaceBase *spdbc = dom->getSpaceForBC(allDirichlet[i],dom->_groupGhostMPI);
	    FilterDof* fdof = dom->createFilterDof(allDirichlet[i]._comp);
        allDirichlet[i]._vspace.push_back(spdbc);
        allDirichlet[i]._vgroup.push_back(dom->_groupGhostMPI);
        allDirichlet[i]._vfilter.push_back(fdof);
        allDirichlet[i]._vdg.push_back(dom->getFormulation());
        break;
      }
    }
    #endif // HAVE_MPI
    if(!domfind){
      // loop on element
      // map
      std::map<partDomain*,std::vector<MElement*> > mapfind;
      std::map<partDomain*,std::vector<MElement*> > mapfindGhost;
      for(groupOfElements::elementContainer::iterator it=allDirichlet[i].g->begin(); it!=allDirichlet[i].g->end();++it){
        MElement *e = *it;
        // Loop on all Domain to find on which domain the BC is applied
        for(std::vector<partDomain*>::iterator itdom=domainVector.begin(); itdom!=domainVector.end(); ++itdom){
          // if Dimensions are the same the element can be find with a comparison to element
          partDomain *dom = *itdom;
          // otherwise the vertex must be identified separately
          MElement *etest = *(dom->g->begin());
          if( (etest !=NULL) and (etest->getDim() == e->getDim())){
            for(groupOfElements::elementContainer::iterator it2=dom->g->begin(); it2!=dom->g->end(); ++it2){
              if(*it2 == e){ // The Element is contained in the domain
                mapfind[dom].push_back(e);
                break;
              }
            }
            #if defined(HAVE_MPI)
            for(groupOfElements::elementContainer::iterator it2=dom->_groupGhostMPI->begin(); it2!=dom->_groupGhostMPI->end(); ++it2){
              if(*it2 == e){ // The Element is contained in the domain
                mapfind[dom].push_back(e);
                break;
              }
            }
            #endif // HAVE_MPI
          }
          else{
                 // an element is included in one domain --> one vertex of Element is OK
                 // On interface the BC is put on the first met domain
            fillMapOfInterfaceElementsInOneDomain(e, mapfind[(*itdom)], dom->g);
            #if defined(HAVE_MPI)
            // change map in mapFindGhost???
            fillMapOfInterfaceElementsInOneDomain(e, mapfind[(*itdom)], dom->_groupGhostMPI);
            // check on element which are not included in _groupGhostMPI as there is no interface element with the element
            for(int j=0;j<Msg::GetCommSize();j++)
            {
              if(j!=Msg::GetCommRank() and !(*itdom)->getFormulation() ) // no ghost with itself
                fillMapOfInterfaceElementsInOneDomain(e, mapfindGhost[(*itdom)], &(_mapOtherDomain[(*itdom)->getPhysical()][j]));
            }

            #endif // HAVE_MPI
          }
        }
      }
      // now the space element map of the BC is filled
      for(std::map<partDomain*,std::vector<MElement*> >::iterator it=mapfind.begin(); it!=mapfind.end(); ++it){
        if(it->second.size() !=0){
          groupOfElements* dirig = new groupOfElements(it->second);
          FunctionSpaceBase *spdbc = it->first->getSpaceForBC(allDirichlet[i],dirig);
          FilterDof *fdof = it->first->createFilterDof(allDirichlet[i]._comp);
          allDirichlet[i]._vspace.push_back(spdbc);
          allDirichlet[i]._vgroup.push_back(dirig);
          allDirichlet[i]._vfilter.push_back(fdof);
          allDirichlet[i]._vdg.push_back(it->first->getFormulation());
        }
      }
      // same with mapfindGhost
      #if defined(HAVE_MPI)
      for(std::map<partDomain*,std::vector<MElement*> >::iterator it=mapfindGhost.begin(); it!=mapfindGhost.end(); ++it){
        if(it->second.size() !=0){
          groupOfElements* dirig = new groupOfElements(it->second);
          FunctionSpaceBase *spdbc = it->first->getSpaceForBC(allDirichlet[i],dirig);
          FilterDof *fdof = it->first->createFilterDof(allDirichlet[i]._comp);
          allDirichlet[i]._vspace.push_back(spdbc);
          allDirichlet[i]._vgroup.push_back(dirig);
          allDirichlet[i]._vfilter.push_back(fdof);
          allDirichlet[i]._vdg.push_back(it->first->getFormulation());
        }
      }
      #endif // HAVE_MPI
    }

    #if defined(HAVE_MPI)
    // same on ghostDomain (be aware copy past of loop on domain !!)
    if(!ghostdomfind){
      // loop on element
      // map
      std::map<partDomain*,std::vector<MElement*> > ghostmapfind;
      for(groupOfElements::elementContainer::iterator it=allDirichlet[i].g->begin(); it!=allDirichlet[i].g->end();++it){
        MElement *e = *it;
        // Loop on all Domain to find on which domain the BC is applied
        for(std::vector<partDomain*>::iterator itdom=_ghostDomainMPI.begin(); itdom!=_ghostDomainMPI.end(); ++itdom){
          // if Dimensions are the same the element can be find with a comparison to element
          partDomain *dom = *itdom;
          // otherwise the vertex must be identified separately
          MElement *etest = *(dom->g->begin());
          if( (etest !=NULL) and (etest->getDim() == e->getDim())){
            for(groupOfElements::elementContainer::iterator it2=dom->_groupGhostMPI->begin(); it2!=dom->_groupGhostMPI->end(); ++it2){
              if(*it2 == e){ // The Element is contained in the domain
                ghostmapfind[dom].push_back(e);
                break;
              }
            }
          }
          else
          {
                 // an element is included in one domain --> one vertex of Element is OK
                 // On interface the BC is put on the first met domain
            fillMapOfInterfaceElementsInOneDomain(e, ghostmapfind[(*itdom)], dom->_groupGhostMPI);
          }
        }
      }
      // now the space element of the BC is filled
      for(std::map<partDomain*,std::vector<MElement*> >::iterator it=ghostmapfind.begin(); it!=ghostmapfind.end(); ++it){
        if(it->second.size() !=0){
          groupOfElements *dirig = new groupOfElements(it->second);
          FunctionSpaceBase *spdbc = it->first->getSpaceForBC(allDirichlet[i],dirig);
          FilterDof* fdof = it->first->createFilterDof(allDirichlet[i]._comp);
          allDirichlet[i]._vspace.push_back(spdbc);
          allDirichlet[i]._vgroup.push_back(dirig);
          allDirichlet[i]._vfilter.push_back(fdof);
          allDirichlet[i]._vdg.push_back(it->first->getFormulation());
        }
      }
    }
    #endif // HAVE_MPI
  }

  // Idem for Neumann BC APPLY ON FIRST FOUNDED DOM. MPI problem the condition has be applied once (on the partition with the lowest number)
  for(int i=0;i<allNeumann.size();i++){
    // first look if the BC is not applied on whole domain
    bool domfind=false;
    for(std::vector<partDomain*>::iterator itdom=domainVector.begin(); itdom!=domainVector.end(); ++itdom){
      partDomain *dom = *itdom;
      if(allNeumann[i]._tag == dom->getPhysical()){
        domfind = true;
        FunctionSpaceBase *spnbc = dom->getSpaceForBC(allNeumann[i],dom->g);
        QuadratureBase *integ = dom->getQuadratureRulesForNeumannBC(allNeumann[i]);
        allNeumann[i]._vspace.push_back(spnbc);
        allNeumann[i]._vgroup.push_back(dom->g);
        allNeumann[i]._vquadrature.push_back(integ);
        allNeumann[i]._vdom.push_back(dom);
        break;
      }
    }
    // map
    std::map<partDomain*,std::vector<MElement*> > mapfind;
    #if defined(HAVE_MPI)
    // look if the BC is applied on a vertex
    if(!domfind){
      if(allNeumann[i].g->vsize() == 1){
        // loop on dof of other partition (if the dof is included only on this partition it will be find after)
        groupOfElements::vertexContainer::iterator itvBC = allNeumann[i].g->vbegin();
        MVertex *verBC = *itvBC;
        for(std::map<int,std::vector<groupOfElements> >::iterator it=_mapOtherDomain.begin(); it!=_mapOtherDomain.end(); ++it){
          std::vector<groupOfElements> & groupOtherPart = it->second;
          for(int j=0;j<Msg::GetCommRank(); j++){ // look on domain with a lower partition number
            for(groupOfElements::vertexContainer::const_iterator itv = groupOtherPart[j].vbegin(); itv!=groupOtherPart[j].vend(); ++itv){
              MVertex *ver = *itv;
              if(verBC == ver){
                domfind=true; // the BC will be applied on an other partition
                break;
              }
            }
          }
        }
      }
    }
    #endif //HAVE_MPI
    if(!domfind){
      // loop on element
      std::map<partDomain*,MElement*> mapelemtype; // need for gauss quadrature rule TODO group with mapfind in a structure but no time benefit
      for(groupOfElements::elementContainer::iterator it=allNeumann[i].g->begin(); it!=allNeumann[i].g->end();++it){
        MElement *e = *it;
        bool flagfind = false;
        // Loop on all Domain to find on which domain the BC is applied
        for(int j=0; j<_mpiUserDom; j++){
          // if Dimensions are the same the element can be find with a comparison to element
          partDomain *dom = domainVector[j];
          // otherwise the vertex must be identified separately
          MElement *etest = *(dom->g->begin());
          mapelemtype[dom] = etest;
          if( (etest != NULL) and (etest->getDim() == e->getDim())){
            for(groupOfElements::elementContainer::iterator it2=dom->g->begin(); it2!=dom->g->end(); ++it2){
              if(*it2 == e){ // The Element is contained in the domain
                mapfind[dom].push_back(e);
                flagfind = true;
                break;
              }
            }
          }
          else{ // loop on Vertex identify the BC with first vertex of e
                 // an element is included in one domain --> one vertex of Element is OK
            // in mpi the BC can be included in an other partition
            #if defined(HAVE_MPI)
            for(int j=_mpiUserDom; j<domainVector.size(); j++){ // loop only on interface domain "added by mpi"
              dgPartDomain *dgdom = static_cast<dgPartDomain*>(domainVector[j]);
              // get the two partition number
              groupOfElements::elementContainer::const_iterator iteleDom = dgdom->gi->begin();
              MInterfaceElement *ie = dynamic_cast<MInterfaceElement*>(*iteleDom);
              int numPlus = ie->getElem(1)->getPartition();
              // if this partition is the one of numPlus it is the partition with the highest number and
              // it has been look  if the BC appear in the other partition
              if(Msg::GetCommRank()+1 == numPlus){
                // loop on element of the domain
                for(groupOfElements::vertexContainer::const_iterator itv=dgdom->gi->vbegin(); itv!=dgdom->gi->vend(); ++itv){
                  MVertex *verDom = *itv;
                  for(int jj=0;jj<e->getNumVertices();jj++){
                    MVertex *verBC = e->getVertex(jj);
                    if(verBC == verDom){
                      flagfind = true; // the BC will be applied on an other partition
                      break;
                    }
                  }
                  if(flagfind) break;
                }
              }
            }
            #endif // HAVE_MPI
            if(!flagfind){
              MVertex *ver = e->getVertex(0);
              for(groupOfElements::vertexContainer::iterator itv=dom->g->vbegin(); itv!=dom->g->vend(); ++itv){
                if((*itv) == ver){
                  mapfind[dom].push_back(e);
                  flagfind = true;
                  break;
                }
              }
            }
          }
        if(flagfind)
          break;
        }
      }
      // now the map Space Element is filled for the BC
      for(std::map<partDomain*,std::vector<MElement*> >::iterator it=mapfind.begin(); it!=mapfind.end(); ++it){
        //nonLinearNeumannBC *neu = new nonLinearNeumannBC(allNeumann[i]);
        groupOfElements *neug = new groupOfElements(it->second);
        FunctionSpaceBase *spnbc = it->first->getSpaceForBC(allNeumann[i],neug);
        QuadratureBase *integ = it->first->getQuadratureRulesForNeumannBC(allNeumann[i]);
        allNeumann[i]._vspace.push_back(spnbc);
        allNeumann[i]._vgroup.push_back(neug);
        allNeumann[i]._vquadrature.push_back(integ);
        allNeumann[i]._vdom.push_back(it->first);
      }
    }
  }

  // Initial (NOT MPI ??)
  for(int i=0;i<allinitial.size();i++){
    // first look if the BC is not applied on whole domain
    bool domfind=false;
    for(std::vector<partDomain*>::iterator itdom=domainVector.begin(); itdom!=domainVector.end(); ++itdom){
      partDomain *dom = *itdom;
      if(allinitial[i]._tag == dom->getPhysical()){
        domfind = true;
        FunctionSpaceBase *spic = dom->getSpaceForBC(allinitial[i],dom->g);
        // if comp == -1 normal initial condition --> trivial filter dof
        FilterDof *fdof;
        if(allinitial[i]._comp != -1)
          fdof = dom->createFilterDof(allinitial[i]._comp);
        else
          fdof = new FilterDofTrivial();
        allinitial[i]._vspace.push_back(spic);
        allinitial[i]._vgroup.push_back(allinitial[i].g); // gr can be different from dom->g (filter when BC is added to domain)
        allinitial[i]._vDomGroup.push_back(allinitial[i].g); // idem
        allinitial[i]._vfilter.push_back(fdof);
        allinitial[i]._vdg.push_back(dom->getFormulation());
        break;
      }
    }
    // look also if initial BC is not applied on a rigid contact
    if(!domfind)
    {
      for(contactContainer::iterator it = _allContact.begin(); it!=_allContact.end(); ++it){
        contactDomain *cdom = *it;
        if(allinitial[i]._tag == cdom->getPhys())
        {
          domfind = true;
          MPoint *mgc = new MPoint(cdom->getGC());
          // initialize groupOfElement where the BC is applied
          allinitial[i].g->insert(mgc);
          // fill conditions
          FilterDof *fdof = cdom->createFilterDof(allinitial[i]._comp);
          allinitial[i]._vspace.push_back(cdom->getSpace());
          allinitial[i]._vgroup.push_back(allinitial[i].g);
          allinitial[i]._vDomGroup.push_back(allinitial[i].g);
          allinitial[i]._vfilter.push_back(fdof);
          allinitial[i]._vdg.push_back(false); // rigid contact cannot be discontinuous
          break;
        }
      }
    }

    if(!domfind){
      // loop on element
      // map
      std::map<partDomain*,std::vector<MElement*> > mapfind;
      for(groupOfElements::elementContainer::iterator it=allinitial[i].g->begin(); it!=allinitial[i].g->end();++it){
        MElement *e = *it;
        // Loop on all Domain to find on which domain the BC is applied
        for(std::vector<partDomain*>::iterator itdom=domainVector.begin(); itdom!=domainVector.end(); ++itdom){
          // if Dimensions are the same the element can be find with a comparison to element
          partDomain *dom = *itdom;
          // otherwise the vertex must be identified separately
          MElement *etest = *(dom->g->begin());
          if((etest != NULL) and etest->getDim() == e->getDim()){
            for(groupOfElements::elementContainer::iterator it2=dom->g->begin(); it2!=dom->g->end(); ++it2){
              if(*it2 == e){ // The Element is contained in the domain
                mapfind[dom].push_back(e);
                break;
              }
            }
          }
          else{ // loop on Vertex identify the BC with first vertex of e
                 // an element is included in one domain --> one vertex of Element is OK
                 // On interface the BC is put on the first met domain
            fillMapOfInterfaceElementsInOneDomain(e, mapfind[(*itdom)], dom->g);
          }
        }
      }
      // now One Initial Condition is created on domain
      for(std::map<partDomain*,std::vector<MElement*> >::iterator it=mapfind.begin(); it!=mapfind.end(); ++it){
        groupOfElements *initCg = new groupOfElements(it->second);
        FunctionSpaceBase *spic = it->first->getSpaceForBC(allinitial[i],initCg);
        // if comp == -1 normal initial condition --> trivial filter dof
        FilterDof *fdof;
        if(allinitial[i]._comp != -1)
          fdof = it->first->createFilterDof(allinitial[i]._comp);
        else
          fdof = new FilterDofTrivial();

        allinitial[i]._vspace.push_back(spic);
        allinitial[i]._vDomGroup.push_back(it->first->g);
        allinitial[i]._vgroup.push_back(initCg);
        allinitial[i]._vfilter.push_back(fdof);
        allinitial[i]._vdg.push_back(it->first->getFormulation());
      }
    }
  }

  // Constraint BC
  for (int i=0; i<allConstraint.size(); i++){
    for(std::vector<partDomain*>::iterator itdom=domainVector.begin(); itdom!=domainVector.end(); ++itdom){
        partDomain* dom= *itdom;
        nonLinearConstraintBC *uc = new nonLinearConstraintBC(allConstraint[i]);
        uc->g = new groupOfElements(*(allConstraint[i].g));
        dom->addConstraintBC(uc);

    };
  };

  // put back the curent GModel
  GModel::setCurrent(save_current_model);
};


void nonLinearMechSolver::setTimeForBC(double time){
  for(std::vector<nonLinearDirichletBC>::iterator it = allDirichlet.begin(); it != allDirichlet.end();++it)
  {
    it->_f->setTime(time);
  }

  for(std::vector<nonLinearNeumannBC>::iterator it = allNeumann.begin(); it!= allNeumann.end();++it)
  {
    it->_f->setTime(time);
  }

  // set time for rigid contact BC
  for(std::vector<rigidContactBC>::iterator it = allContactBC.begin(); it!=allContactBC.end(); ++it){
    it->_f->setTime(time);
  }

	// for periodic BC
  this->setTimeForMicroBC(time);

}

void nonLinearMechSolver::setTimeForLaw(const double t,const double dt)
{
  for(std::map<int,materialLaw*>::iterator it=maplaw.begin(); it!=maplaw.end();++it)
  {
    it->second->setTime(t,dt);
  }
}

void nonLinearMechSolver::fixNodalDofs(){
  // Fixation (prescribed displacement)
  dofManager<double>* currentManager = NULL;
  for(std::vector<nonLinearDirichletBC>::iterator it = allDirichlet.begin(); it!=allDirichlet.end();++it ){
    nonLinearDirichletBC &diri = *it;
    // select the system if required
    if(_vcompBySys.size()==0)
    {
      currentManager = pAssembler;
    }
    else
    {
      dofManagerMultiSystems<double>* multiManager = static_cast<dofManagerMultiSystems<double>*>(pAssembler);
      currentManager = multiManager->getDofManagerByComp(diri._comp);
    }
    if(diri._mycondition==nonLinearBoundaryCondition::position)
    {
      for(int j=0; j<diri._vspace.size();j++)
      {
        FunctionSpaceBase *spdiri = diri._vspace[j];
        groupOfElements *gdiri = diri._vgroup[j];
        FilterDof* fdofdiri = diri._vfilter[j];
        bool fdg = diri._vdg[j];
        FixNodalDofs(spdiri,gdiri->begin(),gdiri->end(),*currentManager,*diri._f,*(fdofdiri),fdg);
      }
    }
    else
    {
      for(int j=0;j<diri._vspace.size();j++){
        // allow to prescribed a velocity or acceleration over time using a dirichlet BC
        SetInitialDofs(diri._vspace[j],diri._vgroup[j]->begin(),diri._vgroup[j]->end(),diri._mycondition,*currentManager,diri._f,*(diri._vfilter[j]),diri._vdg[j]);
      }
    }
  }

  // prescribed displacement of rigid bodies (CG)
  for(std::vector<rigidContactBC>::iterator it=allContactBC.begin(); it!=allContactBC.end(); ++it){
    rigidContactBC &rcbc = *it;
    // select the system if required
    if(_vcompBySys.size()==0)
    {
      currentManager = pAssembler;
    }
    else
    {
      dofManagerMultiSystems<double>* multiManager = static_cast<dofManagerMultiSystems<double>*>(pAssembler);
      currentManager = multiManager->getDofManagerByComp(rcbc._comp);
    }
    FixNodalDofs(rcbc.space,*rcbc._f,*(rcbc._filter),*pAssembler);
  }
}

void nonLinearMechSolver::numberDofs(){
  // we number the dofs : when a dof is numbered, it cannot be numbered
  // again with another number.

  for(std::vector<partDomain*>::iterator itdom = domainVector.begin(); itdom!=domainVector.end(); ++itdom)
  {
    partDomain *dom = *itdom;
    if(dom->g->size() > 0){
      NumberDofsByVector(*(dom->getFunctionSpace()), dom->g->begin(), dom->g->end(),*pAssembler);
    }
   #if defined(HAVE_MPI)
    // create the mpi communication on interface domain
    // no element in g --> interfaceDomain
    else if(Msg::GetCommSize()>1){
      dgPartDomain *dgdom = dynamic_cast<dgPartDomain*>(dom);
      if (dgdom){
        staticDofManager<double>* sAssembler = static_cast<staticDofManager<double>*>(pAssembler);
        NumberInterfaceDofsMPI(dom->getFunctionSpace(), dgdom->gi->begin(), dgdom->gi->end(),sAssembler);
      }
    }
   #endif // HAVE_MPI
  }
  if(whatScheme!=StaticLinear)
  {
    staticDofManager<double>* sAssembler = static_cast<staticDofManager<double>*>(pAssembler);
    sAssembler->setFirstRigidContactUnknowns();
  }

  // NumberDofs of Rigid Contact Entities (Dofs for GC)
  // select the system if required
  dofManager<double>* currentManager = NULL;
  if(_vcompBySys.size()==0)
  {
    currentManager = pAssembler;
  }
  else
  {
    dofManagerMultiSystems<double>* multiManager = static_cast<dofManagerMultiSystems<double>*>(pAssembler);
    currentManager = multiManager->getManager(0);
    if(_allContact.size()>0)
      Msg::Warning("Contact with a multiSystemDofManager --> Do the hypothesis that displacement is the first system");
  }
  for(contactContainer::iterator it = _allContact.begin(); it!=_allContact.end(); ++it){
    contactDomain* cdom = *it;
    if(cdom->isRigid()){
      rigidContactSpaceBase *rcspace = static_cast<rigidContactSpaceBase*>(cdom->getSpace());
      NumberDofs(*rcspace,*currentManager);
    }
  }
  if(_vcompBySys.size()==0)
  {
    // total number of unkowns to allocate the system
    int nunk = pAssembler->sizeOfR();
    // allocate system
    static std::string A("A");
    pAssembler->getLinearSystem(A)->allocate(nunk);
  }
  else
  {
    dofManagerMultiSystems<double>* multiManager = static_cast<dofManagerMultiSystems<double>*>(pAssembler);
    multiManager->allocateSystem();
  }
}

void nonLinearMechSolver::addDomain(partDomain* dom){
  // initialization of GroupOfElements
  int dim = dom->getDim();
  int phys = dom->getPhysical();
#if defined(HAVE_MPI)
  std::vector<groupOfElements> groupOtherPart(Msg::GetCommSize());
  elementFilterMPI elemfil(groupOtherPart) ;
  dom->g = new groupOfElements();
  dom->g->addPhysical(dim,phys,elemfil);
  // add to map (to construct later interface domain)
  _mapOtherDomain.insert(std::pair<int,std::vector<groupOfElements> >(phys,groupOtherPart));
  if (_microFlag == false){
    // at root ranks
    if (_mapRanks.find(Msg::GetCommRank()) != _mapRanks.end()){
      int addFlag = 0;
      if(dom->g->size() != 0){ // otherwise the domain is include totally on an other partition
        dom->setWorkingRanks(Msg::GetCommRank(),_mapRanks[Msg::GetCommRank()]);
        domainVector.push_back(dom);
        addFlag= 1;
      }
      else if(dom->IsInterfaceTerms()){ // take into account for pure interface domain which have dom->g.size()=0
        dgPartDomain *dgdom = static_cast<dgPartDomain*>(dom);
        if(dgdom->getMinusDomain()->getPhysical() != dgdom->getPlusDomain()->getPhysical()){ // pure interface --> add
          domainVector.push_back(dom);
          addFlag = 2;
        }
        else{ // add the domain in ghostDomain vector to prescribed Dirichlet BC (applied on all cpu)
          _ghostDomainMPI.push_back(dom);
          addFlag = 3;
        }
      }

      std::set<int>& otherRanks = _mapRanks[Msg::GetCommRank()];
      for (std::set<int>::iterator its = otherRanks.begin(); its != otherRanks.end(); its++)
        MPI_Send(&addFlag,1,MPI_INT,*its,*its,MPI_COMM_WORLD);

    }
    // in other ranks
    else{
      for (std::map<int,std::set<int> >::iterator it = _mapRanks.begin(); it!= _mapRanks.end(); it++){
        std::set<int>& other = it->second;
        if (other.find(Msg::GetCommRank()) != other.end()){
          int rank = it->first;
          int addFlag = 0;
          MPI_Status status;
          MPI_Recv(&addFlag,1,MPI_INT,rank,Msg::GetCommRank(),MPI_COMM_WORLD,&status);
          if (addFlag == 1){
            domainVector.push_back(dom);
            dom->setWorkingRanks(rank,other);
            dgPartDomain *dgdom = dynamic_cast<dgPartDomain*>(dom);
            dom->g->clearAll();
            if (dgdom){
              dgdom->gi->clearAll();
            }
          }
        }
      }
    }
  }
  else {
    if(dom->g->size() != 0){ // otherwise the domain is include totally on an other partition
      domainVector.push_back(dom);
    }
    else if(dom->IsInterfaceTerms()){ // take into account for pure interface domain which have dom->g.size()=0
      dgPartDomain *dgdom = static_cast<dgPartDomain*>(dom);
      if(dgdom->getMinusDomain()->getPhysical() != dgdom->getPlusDomain()->getPhysical()){ // pure interface --> add
        domainVector.push_back(dom);
      }
      else{ // add the domain in ghostDomain vector to prescribed Dirichlet BC (applied on all cpu)
        _ghostDomainMPI.push_back(dom);
      }
    }
  }


#else
  dom->g = new groupOfElements(dim,phys);
  domainVector.push_back(dom);
#endif // HAVE_MPI
}

void nonLinearMechSolver::addMaterialLaw(materialLaw* mlaw){
  maplaw.insert(std::pair<int,materialLaw*>(mlaw->getNum(),mlaw));
}

materialLaw* nonLinearMechSolver::getMaterialLaw(const int num){
  return (maplaw.find(num)->second);
}

void nonLinearMechSolver::thetaBC(const int numphys){
  groupOfElements *goe = new groupOfElements(1,numphys);
  this->insertTheta(numphys,goe);
}
void nonLinearMechSolver::stiffnessModification(const bool flag){
  _stiffnessModification = flag;
};
void nonLinearMechSolver::iterativeProcedure(const bool flag){
  _iterativeNR = flag;
};
void nonLinearMechSolver::lineSearch(const bool lin){
  _lineSearch = lin;
}
void nonLinearMechSolver::pathFollowing(const bool p){
  _pathFollowing = p;
}
void nonLinearMechSolver::setPathFollowingControlType(const int i){
  _macroControlType = i;
}

/*
void nonLinearMechSolver::setInitOrRestartFileName(const std::string fname){
  initOrResartfname =fname;
  _restart = true;
}

void nonLinearMechSolver::restart(unknownField *ufield){
  if(!_restart) // no file to restart
    return;
  // create a map between elem number and adress
  std::map<long int,std::pair<partDomain*,MElement*> > allelem;
  for(std::vector<partDomain*>::iterator itdom = domainVector.begin(); itdom!=domainVector.end(); ++itdom){
    partDomain *dom = *itdom;
    for(groupOfElements::elementContainer::iterator ite = dom->g->begin(); ite!=dom->g->end(); ++ite){
      MElement *ele = *ite;
      std::pair<partDomain*,MElement*> pa(dom,ele);
      allelem.insert(std::pair<long int,std::pair<partDomain*,MElement*> >(ele->getNum(),pa));
    }
  }

  // read data in file
  FILE *f = fopen(initOrResartfname.c_str(), "r");
  char what[256];
  while(!feof(f)){
    fscanf(f, "%s", what);
    if(!strcmp(what,"$ElementNodeData")){
      int nstring;
      fscanf(f,"%d",&nstring);
      int i=0;
      char what2[256];
      while(i<nstring){
        fscanf(f,"%s",&what2);
        i++;
      }
      int nint, n;
      fscanf(f,"%d",&nint);
      i=0;
      while(i<nint){
        fscanf(f,"%d",&n);
        i++;
      }
      fscanf(f,"%d",&nint);
      if(nint != 4)
      {
        Msg::Error("Your restart file hasn't the good format since the number of int (!=4) is not good. The number of element is taken from the third one!");
      }
      int nelem;
      i=0;
      while(i<nint){
        fscanf(f,"%d",&n);
        if(i == 2)
          nelem = n;
        i++;
      }
      int elemnum,elemtype,ndofs,nvertex,ncomp;
      i = 0;
      int n2 = nelem; // because n is modified by fscanf(f,"%ld %d",&elemnum,&elemtype); WHY ??
      while(i<n2){
        fscanf(f,"%d %d",&elemnum,&elemtype);
        //MElement *ele = allelem[elemnum].second;
        //partDomain *dom = allelem[elem]
        std::map<long int,std::pair<partDomain*,MElement*> >::iterator it = allelem.find(elemnum);
        MElement *ele = (it->second).second;
        partDomain *dom = (it->second).first;
        FunctionSpaceBase *space = dom->getFunctionSpace();
        std::vector<Dof> R;
        space->getKeys(ele,R);
        ndofs = space->getNumKeys(ele);
        nvertex = ele->getNumVertices();
        ncomp = ndofs/nvertex;
        int j=0;
        std::vector<double> disp;
        disp.resize(ndofs);
        //double temp;
        while(j<nvertex){
          for(int k=0;k<ncomp;k++){
            fscanf(f,"%lf",&disp[j+k*nvertex]);
          }
          j++;
        }
        // set displacement in displacement field
        ufield->setInitial(R,disp);
        i++;
      }
    }
  }
  fclose(f);
  ufield->buildAllView(domainVector,0.,0);
}
*/
void nonLinearMechSolver::insertTheta(const int numphys, groupOfElements *goe){
  nonLinearNeumannBC neu;
  neu.g = goe;
  neu.onWhat = nonLinearBoundaryCondition::UNDEF;
  neu._tag = numphys;
  neu._f = new simpleFunctionTimeWithConstant<double>(0.);
  allTheta.push_back(neu);
}

void nonLinearMechSolver::physInitBroken(const int numphys){
  initbrokeninter.push_back(numphys);
}

void nonLinearMechSolver::createInterfaceElement(){
  // Compute and add interface element to the model
  // Loop on mesh element and store each edge (which will be distributed between InterfaceElement, Boundary InterfaceElement and VirtualInterfaceElement)
  // A tag including the vertex number is used to identified the edge common to two elements. In this case a interface element is created
  // manage creation of interface between 2 domains
  manageInterface maninter(&domainVector);
  // loop on element
  for(std::vector<partDomain*>::iterator itdom=domainVector.begin(); itdom !=domainVector.end(); ++itdom)
  {
    partDomain *dom = *itdom;
    dom->createInterface(maninter);
  }

  // virtual interface
  for(std::vector<partDomain*>::iterator itdom = domainVector.begin(); itdom!=domainVector.end(); ++itdom){
    partDomain *dom = *itdom;
    int phys = dom->getPhysical();
    for(manageInterface::IelementContainer::iterator it=maninter.begin(); it!=maninter.end(); ++it){
      IElement *ie = it->second;
      if(ie->getPhys() == phys){
        dgPartDomain *dgdom = static_cast<dgPartDomain*>(dom);
        MElement* interel = dom->createVirtualInterface(ie);
        dgdom->giv->insert(interel);
      }
    }
  }
  _mpiUserDom = domainVector.size();

#if defined(HAVE_MPI)
  if(Msg::GetCommSize() > 1){
    // first add interface to the interdomain defined by the user
    for(std::vector<partDomain*>::iterator itdom = domainVector.begin(); itdom != domainVector.end(); ++itdom){
      // find interdomain (g->size==0)
      partDomain *dom = *itdom;
      if(dom->g->size()==0){
        int phys1,phys2;
        dgPartDomain *dgdom = static_cast<dgPartDomain*>(dom);
        manageInterface::getPhysDom(dom->getPhysical(),phys1,phys2);
        // find both domains
        partDomain *dom1=NULL;
        partDomain *dom2=NULL;
        for(std::vector<partDomain*>::iterator itdom = domainVector.begin(); itdom != domainVector.end(); ++itdom){
          partDomain* domtmp = *itdom;
          if(domtmp->getPhysical() == phys1) dom1=domtmp;
          else if(domtmp->getPhysical() == phys2) dom2=domtmp;
          if((dom1!=NULL) and(dom2!=NULL)) break;
        }
        if(dom1!=NULL){ // otherwise no element of dom1 on the partition
          // find the group of ghosted element of dom2
          std::map<int,std::vector<groupOfElements> >::iterator itg= _mapOtherDomain.find(phys2);
          if(itg!=_mapOtherDomain.end()){
            for(int i=0; i< itg->second.size(); i++){
              groupOfElements &group = itg->second[i];
              if(group.size()!=0)
                dom1->createInterfaceMPI(i,group,dgdom);
            }
          }
        }
        if(dom2!=NULL){ // otherwise no element of dom1 on partition
          // find the group of ghosted element of dom1
          std::map<int,std::vector<groupOfElements> >::iterator itg= _mapOtherDomain.find(phys1);
          if(itg!=_mapOtherDomain.end()){
            for(int i=0; i< itg->second.size(); i++){
              groupOfElements &group = itg->second[i];
              if(group.size()!=0)
                dom2->createInterfaceMPI(i,group,dgdom);
            }
          }
        }
      }
    }
    // create pure interface domain for MPI
    for(std::map<int,std::vector<groupOfElements> >::iterator it=_mapOtherDomain.begin(); it!=_mapOtherDomain.end(); ++it){
      int physdom = it->first;
      // get the domain
      partDomain *dom = NULL;
      for(std::vector<partDomain*>::iterator itdom = domainVector.begin(); itdom != domainVector.end(); ++itdom){
        if(physdom == (*itdom)->getPhysical()){
          dom = *itdom;
          break;
        }
      }
      if(dom != NULL){
        std::vector<groupOfElements> & groupOtherPart = it->second;
        dgPartDomain* newInterDom = NULL;
        int gsize = groupOtherPart.size();
        for(int i=0; i< gsize; i++){
          if(i!=Msg::GetCommRank()){
            groupOfElements &group = groupOtherPart[i];
            if(group.size() != 0){ // otherwise no domain to create
              dom->createInterfaceMPI(i,group,newInterDom);
            }
          }
        }
        if((newInterDom!=NULL) and (newInterDom->gi->size() > 0)){
          // law are set before (change this ??)
          newInterDom->setMaterialLaw(maplaw);
          domainVector.push_back(newInterDom); // otherwise no interface element
        }
        else{
         if(newInterDom!=NULL) delete newInterDom;
        }
      }
    }
  }
#endif // HAVE_MPI
}

void nonLinearMechSolver::createInterfaceElement_2()
{
  // The contructor of dgGroupCollection create automatically the GroupsOfElements
/*  _groups = dgGroupCollection(this->pModel,this->_dim,1); // TODO ?? Add parameter to model to store order
  _groups.buildGroupsOfInterfaces();
  // Affichage temporaire pour vérification
  int nn = _groups.getNbFaceGroups();
  printf("Number of group of faces : %d\n",nn);
  for(int i=0;i<nn;i++){
    printf("Group of face number %d\n",i);
    dgGroupOfFaces *inter = _groups.getFaceGroup(i);
    int nnn = inter->getNbGroupOfConnections();
    printf("Number of connection group %d \n",nnn);
    for(int j=0;j<nnn;j++){
      const dgGroupOfConnections connec = inter->getGroupOfConnections(j);
      printf("Connection's group number %d\n",j);
      int nnnn = connec.getNbClosures();
      printf("Number of closures %d\n",nnnn);
      for(int k=0;k<nnnn;k++){
        printf("Closure number %d\n",k);
        std::vector<int> vec = connec.getClosure(k);
        for(int kk=0;kk<vec.size();kk++){
          printf(" %d ",vec[kk]);
        }
        printf("\n");
      }
    }
  }*/
}

void nonLinearMechSolver::initTerms(unknownField *ufield, IPField *ipf){
  // resize _vterm in NeumannBC
  if(_previousInit){ // if previous computation delete first the terms
    // BC
    for(int i=0;i<allNeumann.size();i++)
    {
      nonLinearNeumannBC &neu = allNeumann[i];
      for(int j=0;j<neu._vterm.size();j++)
      {
        delete neu._vterm[j];
      }
    }
    // domain
    for(std::vector<partDomain*>::iterator itdom = domainVector.begin(); itdom!=domainVector.end(); ++itdom){
      partDomain *dom = *itdom;
      if(dom->getBilinearBulkTerm() != NULL) delete dom->getBilinearBulkTerm();
      if(dom->getBilinearMassTerm() != NULL) delete dom->getBilinearMassTerm();
      if(dom->getLinearBulkTerm() != NULL) delete dom->getLinearBulkTerm();
      if(dom->IsInterfaceTerms()){
        dgPartDomain *dgdom = static_cast<dgPartDomain*>(dom);
        if(dgdom->getBilinearInterfaceTerm() != NULL) delete dgdom->getBilinearInterfaceTerm();
        if(dgdom->getBilinearVirtualInterfaceTerm() !=NULL) delete dgdom->getBilinearVirtualInterfaceTerm();
        if(dgdom->getLinearInterfaceTerm() != NULL) delete dgdom->getLinearInterfaceTerm();
        if(dgdom->getLinearVirtualInterfaceTerm() != NULL) delete dgdom->getLinearVirtualInterfaceTerm();
      }
    }
  }
  for(int i=0;i<allNeumann.size();i++)
  {
    nonLinearNeumannBC &neu = allNeumann[i];
    int nsize = neu._vspace.size();
    neu._vterm.resize(nsize);
  }

  for(std::vector<partDomain*>::iterator itdom = domainVector.begin(); itdom!=domainVector.end(); ++itdom){
    partDomain *dom = *itdom;
    dom->initializeTerms(ufield,ipf,allNeumann);
  }

  // contact domain
  for(contactContainer::iterator it = _allContact.begin(); it!=_allContact.end(); ++it){
    contactDomain *cdom = *it;
      cdom->initializeTerms(ufield);
  }
}

double nonLinearMechSolver::solveStaticLinear()
{
  /* init data */
  this->init();
  unknownField *ufield=NULL;
  energeticField *efield=NULL;
  this->init2(ufield,efield);
  delete efield; efield=NULL; // energy cannot be computed with this scheme as the system does not computed the forces of fixed ddl!!


  /* time initialization */
  double curtime = 1.;
  double timestep = 1.;
  int step = 1;

  /* solving */
  this->setTimeForLaw(curtime,timestep);
  std::cout <<  "Neumann BC"<< std::endl;
  this->computeExternalForces(ufield);
  this->computeStiffMatrix(pAssembler);
  printf("-- done assembling!\n");
  pAssembler->systemSolve();
  printf("-- done solving!\n");

  /* compute stress after solve */
  _ipf->compute1state(IPStateBase::current,true);
  if(_microFlag){
    _homogenizedFiles.openFiles(_enum,_gnum,allMicroBC[0]->getOrder(),
                      _isHommStrainSaveToFile, _isHommProSaveToFile);
  }

  /* end of scheme */
  this->endOfScheme(ufield,efield,curtime,step);
  Msg::Info("StaticLinear OK");
  return curtime;
}

void nonLinearMechSolver::computeLoadVector(const unknownField *ufield){
  this->setTimeForBC(1.);
  staticDofManager<double> *sAssembler;
  for(int i=0;i<allNeumann.size();i++){
    nonLinearNeumannBC &neu = allNeumann[i];
    // select the system if required
    if(_vcompBySys.size()==0)
    {
      sAssembler = static_cast<staticDofManager<double>*>(pAssembler);
    }
    else
    {
      dofManagerMultiSystems<double>* multiManager = static_cast<dofManagerMultiSystems<double>*>(pAssembler);
      sAssembler = static_cast<staticDofManager<double>*>(multiManager->getDofManagerByComp(neu._comp));
    }
    for(int j=0;j<neu._vspace.size();j++)
    {
      FunctionSpaceBase *spn = neu._vspace[j];
      groupOfElements *gneu = neu._vgroup[j];
      QuadratureBase *qneu = neu._vquadrature[j];
      Assemble(neu._vterm[j],*spn,gneu->begin(),
             gneu->end(),*(qneu),ufield,*sAssembler,nonLinearSystem<double>::LoadVector);
    }
  }
  this->setTimeForBC(0.);
}

void nonLinearMechSolver::computeExternalForces(const unknownField *ufield){
  if(this->getScheme() != StaticLinear){
    staticDofManager<double> *sAssembler;
    for(int i=0;i<allNeumann.size();i++){
      nonLinearNeumannBC &neu = allNeumann[i];
      // select the system if required
      if(_vcompBySys.size()==0)
      {
        sAssembler = static_cast<staticDofManager<double>*>(pAssembler);
      }
      else
      {
        dofManagerMultiSystems<double>* multiManager = static_cast<dofManagerMultiSystems<double>*>(pAssembler);
        sAssembler = static_cast<staticDofManager<double>*>(multiManager->getDofManagerByComp(neu._comp));
      }
      for(int j=0;j<neu._vspace.size();j++)
      {
        FunctionSpaceBase *spn = neu._vspace[j];
        groupOfElements *gneu = neu._vgroup[j];
        QuadratureBase *qneu = neu._vquadrature[j];
        Assemble(neu._vterm[j],*spn,gneu->begin(),
               gneu->end(),*(qneu),ufield,*sAssembler,nonLinearSystem<double>::Fext);
      }
    }
  }
  else{
    for(int i=0;i<allNeumann.size();i++){
      nonLinearNeumannBC &neu = allNeumann[i];
      for(int j=0;j<neu._vspace.size();j++)
      {
        FunctionSpaceBase *spn = neu._vspace[j];
        groupOfElements *gneu = neu._vgroup[j];
        QuadratureBase *qneu = neu._vquadrature[j];
        Assemble(*(neu._vterm[j]),*spn,gneu->begin(),gneu->end(),*(qneu),*pAssembler);
      }
    }
  }
}

void nonLinearMechSolver::snlData(const int ns, const double et, const double reltol){
  if(whatScheme==StaticNonLinear or whatScheme==Multi or whatScheme== Implicit){
    _numstepImpl=ns;endtime=et;_tol=reltol;
  }
  else{
    _numstepImpl = 1; // for static linear
    Msg::Error("Impossible to set data for Static Non Linear scheme because another is chosen to solve the problem");
  }
}

void nonLinearMechSolver::addSystem(const int comp,const int s)
{
  _vcompBySys.push_back(comp);
  _vschemeBySys.push_back((scheme)s);
}

void nonLinearMechSolver::switchSystem(const int sysindex,const int s)
{
  _vschemeBySys[sysindex] = (scheme)s;
}

void nonLinearMechSolver::snlManageTimeStep(const int miteNR,const int iteIncreaseTS, const double redfactor, const int maxAttemptRedFactor)
{
  _maxNRite = miteNR;
  _niteTimeStepIncrease = iteIncreaseTS;
  _timeStepFactorReduction = redfactor;
  _maxAttemptStepReduction = maxAttemptRedFactor;
}

void nonLinearMechSolver::explicitData(const double ftime, const double gams, const double beta, const double gamma, const double alpham,const bool benson){
  _timeStepByBenson = benson;
  endtime = ftime;
  _beta=beta;
  _gamma=gamma;
  _alpham = alpham;
  // depends on numerical damping
  double omegas;
  _rhoinfty = (alpham+1.)/(2.-alpham);
  omegas = sqrt((12.*(1.+_rhoinfty)*(1.+_rhoinfty)*(1.+_rhoinfty)*(2.-_rhoinfty))/(10.+15.*_rhoinfty-_rhoinfty*_rhoinfty+_rhoinfty*_rhoinfty*_rhoinfty-_rhoinfty*_rhoinfty*_rhoinfty*_rhoinfty));
    _gammas = gams*omegas;
}

void nonLinearMechSolver::implicitData(const double beta, const double gamma, const double alpham , const double alphaf){
  _beta = beta;
  _gamma = gamma;
  _alphaf = alphaf;
  _alpham = alpham;
};

void nonLinearMechSolver::explicitSpectralRadius(const double ftime,const double gams, const double rho,const bool benson){
  _timeStepByBenson = benson;
  endtime = ftime;
  _rhoinfty=rho;
  _beta = (5.-3.*_rhoinfty)/((1.+_rhoinfty)*(1.+_rhoinfty)*(2.-_rhoinfty));
  _alpham = (2.*_rhoinfty-1.)/(1.+_rhoinfty);
  _gamma = 1.5-_alpham;
  double omegas = sqrt((12.*(1.+_rhoinfty)*(1.+_rhoinfty)*(1.+_rhoinfty)*(2.-_rhoinfty))/(10.+15.*_rhoinfty-_rhoinfty*_rhoinfty+_rhoinfty*_rhoinfty*_rhoinfty-_rhoinfty*_rhoinfty*_rhoinfty*_rhoinfty));
    _gammas = gams*omegas;
}

void nonLinearMechSolver::implicitSpectralRadius(const double rho){
  _rhoinfty = rho;
  _alpham = (2.*_rhoinfty-1.)/(1.+_rhoinfty);
  _alphaf = _rhoinfty/(_rhoinfty +1.);
  _beta = 1./((_rhoinfty +1.)*(_rhoinfty +1.));
  _gamma = (3.-_rhoinfty)/(2.+2.*_rhoinfty);

};

void nonLinearMechSolver::explicitTimeStepEvaluation(const int nst){
  _numstepExpl = nst;
}

double nonLinearMechSolver::solve(){
  double reachtime=0.;
  if (_microFlag == false){
    switch(whatScheme){
      case StaticLinear :
        if(Msg::GetCommRank() == 0)
          reachtime = this->solveStaticLinear();
        else
          Msg::Warning("Static linear scheme is not defined with mpi --> use only 1 cpu");
        break;
      case StaticNonLinear :
          reachtime = this->solveSNL();
        break;
      case Implicit:
          reachtime = this->solveSNL();
        break;
      case Explicit:
        reachtime = this->solveExplicit();
        break;
      case Multi:
        reachtime = this->solveMulti();
    }
  }
  else{
    switch(whatScheme){
      case StaticLinear:
        reachtime = this->microSolveStaticLinear();
        break;
      case StaticNonLinear :
        reachtime = this->microSolveSNL();
    }
  };
  Msg::Barrier(); // wait all before end (for MPI)
  _previousScheme = whatScheme;
  _unresetedBC = true;
  _unresetedContact = true;
  _dynamicRelaxation = false;
  return reachtime;
}

void nonLinearMechSolver::resetBoundaryConditions()
{
  for(int i=0;i<allDirichlet.size();i++)
  {
    allDirichlet[i].clear();
  }
  allDirichlet.clear();
  for(int i=0;i<allNeumann.size();i++)
  {
    allNeumann[i].clear();
  }
  allNeumann.clear();
  for(int i=0;i<allinitial.size();i++)
  {
    allinitial[i].clear();
  }
  allinitial.clear();
  _unresetedBC = false;
}

void nonLinearMechSolver::resetContactInteraction()
{
 // for(contactContainer::iterator it = _allContact.begin(); it!=_allContact.end(); ++it){
 //   contactDomain *cdom = *it;
 //   delete cdom;
 // }
  _allContact.clear();
  _unresetedContact = false;
}

void nonLinearMechSolver::dynamicRelaxation(const double gams,const double ftime,const double tol,const int wsolver,const bool benson)
{
  _timeStepByBenson = true;
  _dynamicRelaxation = true;
  whatScheme = Explicit;
  if(wsolver == 2)
    whatSolver = Petsc;
  _tol = tol;
  this->explicitSpectralRadius(ftime,gams,0.99); //rho_infty == 0 for dynamic relaxtion (no influence ??)
}

void nonLinearMechSolver::displacementBC(std::string onwhat, const int numphys, const int comp, const double value)
{
  simpleFunction<double> *fct = new simpleFunctionTime<double>(value);
  elementFilterTrivial filter = elementFilterTrivial();
  this->displacementBC(onwhat,numphys,comp,&filter,fct);
}

void nonLinearMechSolver::velocityBC(std::string onwhat, const int numphys, const int comp, const double value)
{
  this->displacementBC(onwhat,numphys,comp,value);
  allDirichlet.back()._mycondition = nonLinearBoundaryCondition::velocity;
}

void nonLinearMechSolver::displacementBC(std::string onwhat, const int numphys, const int comp, const double value, elementFilter *filter)
{
  simpleFunction<double> *fct = new simpleFunctionTime<double>(value);
  this->displacementBC(onwhat,numphys,comp,filter,fct);
}

void nonLinearMechSolver::velocityBC(std::string onwhat, const int numphys, const int comp, const double value, elementFilter *filter)
{
  this->displacementBC(onwhat,numphys,comp,value,filter);
  allDirichlet.back()._mycondition = nonLinearBoundaryCondition::velocity;
}

void nonLinearMechSolver::displacementBC(std::string onwhat, const int numphys, const int comp,simpleFunction<double> *fct)
{
  elementFilterTrivial filter = elementFilterTrivial();
  this->displacementBC(onwhat,numphys,comp,&filter,fct);
}

void nonLinearMechSolver::velocityBC(std::string onwhat, const int numphys, const int comp,simpleFunction<double> *fct)
{
  this->displacementBC(onwhat,numphys,comp,fct);
  allDirichlet.back()._mycondition = nonLinearBoundaryCondition::velocity;
}

void nonLinearMechSolver::displacementBC(std::string onwhat, const int numphys, const int comp, elementFilter *filter,
                                         simpleFunction<double> *fct)
{
  nonLinearDirichletBC diri;
  const std::string node("Node");
  const std::string edge("Edge");
  const std::string face("Face");
  const std::string volume("Volume");
  diri.g = new groupOfElements();
  if(onwhat==node){
    diri.g->addPhysical(0, numphys,*filter);
    diri.onWhat=nonLinearBoundaryCondition::ON_VERTEX;
  }
  else if(onwhat==edge){
    diri.g->addPhysical(1, numphys,*filter);
    diri.onWhat=nonLinearBoundaryCondition::ON_EDGE;
  }
  else if(onwhat==face){
    diri.g->addPhysical(2, numphys,*filter);
    diri.onWhat=nonLinearBoundaryCondition::ON_FACE;
  }
  else if(onwhat==volume){
    diri.g->addPhysical(3, numphys,*filter);
    diri.onWhat=nonLinearBoundaryCondition::ON_VOLUME;
  }
  else{
    Msg::Error("Impossible to prescribe a displacement on a %s\n",onwhat.c_str());
    delete diri.g;
    return;
  }
  diri._f= static_cast<simpleFunctionTime<double>*>(fct);
  diri._comp=comp;
  diri._tag=numphys;
  allDirichlet.push_back(diri);
}

void nonLinearMechSolver::velocityBC(std::string onwhat, const int numphys, const int comp, elementFilter *filter,
                                         simpleFunction<double> *fct)
{
  this->displacementBC(onwhat,numphys,comp,filter,fct);
  allDirichlet.back()._mycondition = nonLinearBoundaryCondition::velocity;
}

void nonLinearMechSolver::displacementRigidContactBC(const int numphys, const int comp_, const double value)
{
  simpleFunction<double> *fct = new simpleFunctionTime<double>(value);
  this->displacementRigidContactBC(numphys,comp_,fct);
}
void nonLinearMechSolver::displacementRigidContactBC(const int numphys, const int comp_, simpleFunction<double> *fct)
{
  rigidContactBC diri(numphys);
  diri.onWhat = nonLinearBoundaryCondition::RIGIDCONTACT;
  diri._comp = comp_;
  diri._f = static_cast<simpleFunctionTime<double>*>(fct);
  allContactBC.push_back(diri);
}

void nonLinearMechSolver::initialBC(std::string onwhat, std::string whichC, const int numphys,
                                          const int comp, const double value)
{
  elementFilterTrivial filter = elementFilterTrivial();
  this->initialBC(onwhat,whichC,numphys,comp,value,&filter);
}

void nonLinearMechSolver::initialBC(std::string onwhat, std::string whichC, const int numphys,
                                          const int comp, const double value,
                                          elementFilter *filter)
{
  simpleFunctionTime<double>* sfunction = new simpleFunctionTime<double>(value,false,0.);
  this->initialBC(onwhat,whichC,numphys,comp,sfunction,filter);
}

void nonLinearMechSolver::initialBC(std::string onwhat, std::string whichC, const int numphys, const int comp,
                                    simpleFunctionTime<double> *fct,elementFilter* filter)
{
  bool nullfilter=false;
  if(filter==NULL)
  {
    filter = new elementFilterTrivial();
    nullfilter = true;
  }

  const std::string node("Node");
  const std::string edge("Edge");
  const std::string face("Face");
  const std::string volume("Volume");
  const std::string rigid("Rigid");
  const std::string position("Position");
  const std::string velocity("Velocity");
  const std::string acceleration("Acceleration");

  nonLinearBoundaryCondition::location ow;
  groupOfElements *gr = new groupOfElements();
  if(onwhat == node){
    ow = nonLinearBoundaryCondition::ON_VERTEX;
    gr->addPhysical(0,numphys,*filter);
  }
  else if(onwhat == edge){
    ow = nonLinearBoundaryCondition::ON_EDGE;
    gr->addPhysical(1,numphys,*filter);
  }
  else if(onwhat == face){
    ow = nonLinearBoundaryCondition::ON_FACE;
    gr->addPhysical(2,numphys,*filter);
  }
  else if(onwhat == volume){
    ow = nonLinearBoundaryCondition::ON_VOLUME;
    gr->addPhysical(3,numphys,*filter);
  }
  else if(onwhat == rigid)
  {
    ow = nonLinearBoundaryCondition::RIGIDCONTACT;
    // empty group insert gravity center later
  }
  else{
    ow = nonLinearBoundaryCondition::UNDEF;
    gr->addPhysical(2,numphys,*filter);
  }

  nonLinearBoundaryCondition::whichCondition wc;
  if(whichC == position)
    wc = nonLinearBoundaryCondition::position;
  else if(whichC == velocity)
    wc = nonLinearBoundaryCondition::velocity;
  else if(whichC == acceleration)
    wc = nonLinearBoundaryCondition::acceleration;
  else{
    Msg::Warning("Impossible to prescribed an initial condition %d",numphys);
    //return;
  }
  initialCondition initc(comp,wc);
  initc.onWhat = ow;
  initc.g = gr;
  initc._tag = numphys;
  initc._f = fct;
  allinitial.push_back(initc);


  if(nullfilter)
  {
    delete filter;
  }
}

void nonLinearMechSolver::initialDCBVeloBC(std::string onwhat,const int numphys,const int axiscomp, const int deflcomp,const double length, const double value)
{
  const std::string edge("Edge");
  const std::string face("Face");
  const std::string volume("Volume");
  initialCondition initc = initialCondition(deflcomp,nonLinearBoundaryCondition::velocity);
  initc._f = new functionVelocityDCB(value,axiscomp,length);
  initc.g = new groupOfElements();
  initc._tag = numphys;
  if(onwhat == edge){
    initc.onWhat = nonLinearBoundaryCondition::ON_EDGE;
    initc.g->addPhysical(1,numphys);
    allinitial.push_back(initc);
  }
  else if(onwhat == face){
    initc.onWhat = nonLinearBoundaryCondition::ON_FACE;
    initc.g->addPhysical(2,numphys);
    allinitial.push_back(initc);
  }
  else if(onwhat == volume){
    initc.onWhat  = nonLinearBoundaryCondition::ON_VOLUME;
    initc.g->addPhysical(3,numphys);
    allinitial.push_back(initc);
  }
  else{
    Msg::Error("The initial velocity BC can be applied only on an edge or on a face");
  }
}

void nonLinearMechSolver::independentDisplacementBC(std::string onwhat, const int numphys, const int comp, const double value){
  nonLinearDirichletBC diri;
  const std::string node("Node");
  const std::string edge("Edge");
  const std::string face("Face");
  const std::string volume("Volume");
  if(onwhat==node){
    diri.g = new groupOfElements (0, numphys);
    diri.onWhat=nonLinearBoundaryCondition::ON_VERTEX;
  }
  else if(onwhat==edge){
    diri.g = new groupOfElements (1, numphys);
    diri.onWhat=nonLinearBoundaryCondition::ON_EDGE;
  }
  else if(onwhat==face){
    diri.g = new groupOfElements (2, numphys);
    diri.onWhat=nonLinearBoundaryCondition::ON_FACE;
  }
  else if(onwhat==volume){
    diri.g = new groupOfElements (3, numphys);
    diri.onWhat=nonLinearBoundaryCondition::ON_VOLUME;
  }
  else Msg::Error("Impossible to prescribe a displacement on a %s\n",onwhat.c_str());
  diri._f= new simpleFunctionTime<double>(value,false);
  diri._comp=comp;
  diri._tag=numphys;
  allDirichlet.push_back(diri);
}

void nonLinearMechSolver::forceBC(std::string onwhat, const int numphys, const int comp, const double val)
{
  simpleFunctionTime<double> *fct = new simpleFunctionTime<double>(val,true,0.);
  this->forceBC(onwhat,numphys,comp,fct);
}
void nonLinearMechSolver::forceBC(std::string onwhat, const int numphys, const int comp, simpleFunctionTime<double> *fct)
{
  nonLinearNeumannBC neu;
  const std::string node("Node");
  const std::string edge("Edge");
  const std::string face("Face");
  const std::string volume("Volume");
  const std::string pressure("Pressure");
  const std::string facevertex("FaceVertex");
  if(onwhat==node){
    neu.g = new groupOfElements (0, numphys);
    neu.onWhat=nonLinearBoundaryCondition::ON_VERTEX;
  }
  else if(onwhat==edge){
    neu.g = new groupOfElements (1, numphys);
    neu.onWhat=nonLinearBoundaryCondition::ON_EDGE;
  }
  else if(onwhat==face){
    neu.g = new groupOfElements (2, numphys);
    neu.onWhat=nonLinearBoundaryCondition::ON_FACE;
  }
  else if(onwhat==pressure){
    neu.g = new groupOfElements (2, numphys);
    neu.onWhat=nonLinearBoundaryCondition::PRESSURE;
  }
  else if(onwhat==facevertex)
  {
    neu.g = new groupOfElements(2,numphys);
    neu.onWhat=nonLinearBoundaryCondition::ON_FACE_VERTEX;
  }
  else if(onwhat==volume){
    neu.g = new groupOfElements (3, numphys);
    neu.onWhat=nonLinearBoundaryCondition::ON_VOLUME;
  }
  else  Msg::Error("Impossible to prescribe a force on a %s\n",onwhat.c_str());
  neu._f= fct;
  neu._tag=numphys;
  neu._comp=comp;
  allNeumann.push_back(neu);
}

void nonLinearMechSolver::independentForceBC(std::string onwhat, const int numphys, const int comp, const double val){
  nonLinearNeumannBC neu;
  const std::string node("Node");
  const std::string edge("Edge");
  const std::string face("Face");
  const std::string volume("Volume");
  if(onwhat==node){
    neu.g = new groupOfElements (0, numphys);
    neu.onWhat=nonLinearBoundaryCondition::ON_VERTEX;
  }
  else if(onwhat==edge){
    neu.g = new groupOfElements (1, numphys);
    neu.onWhat=nonLinearBoundaryCondition::ON_EDGE;
  }
  else if(onwhat==face){
    neu.g = new groupOfElements (2, numphys);
    neu.onWhat=nonLinearBoundaryCondition::ON_FACE;
  }
  else if(onwhat==volume){
    neu.g = new groupOfElements (3, numphys);
    neu.onWhat=nonLinearBoundaryCondition::ON_VOLUME;
  }
  else  Msg::Error("Impossible to prescribe a force on a %s\n",onwhat.c_str());
  neu._f= new simpleFunctionTimeWithConstant<double>(val,false);
  neu._tag=numphys;
  neu._comp = comp;
  allNeumann.push_back(neu);
}

void nonLinearMechSolver::pressureOnPhysicalGroupBC(const int numphys, const double press, const double p0){
  nonLinearNeumannBC neu;
  neu.g = new groupOfElements(2,numphys); // Always group of face ??
  neu.onWhat = nonLinearBoundaryCondition::PRESSURE;
  neu._f = new simpleFunctionTimeWithConstant<double>(press,true,1.,p0); // Use a SVector3 because it was defined like this in
  neu._tag=numphys;
  allNeumann.push_back(neu);
}

void nonLinearMechSolver::blastPressureBC(const int numphys,const double p0,const double p1, const double plexp,
                                       const double t0, const double t1){
  powerDecreasingFunctionTime *pdl = new powerDecreasingFunctionTime(p0,p1,plexp,t0,t1,this->endtime);
  nonLinearNeumannBC neu(numphys,5,pdl);
  allNeumann.push_back(neu);
}

void nonLinearMechSolver::archivingForceOnPhysicalGroup(const std::string onwhat, const int numphys, const int comp,const int nstep){
  // get the node of the edge
  static std::string node("Node");
  static std::string edge("Edge");
  static std::string face("Face");
  static std::string volu("Volume");
  int dim=0;
  if(onwhat == node )
    dim = 0;
  else if(onwhat == edge)
    dim = 1;
  else if(onwhat == face)
    dim = 2;
  else if(onwhat == volu)
    dim = 3;

  vaf.push_back(archiveForce(numphys,dim,comp,nstep));
}

void nonLinearMechSolver::initArchiveForce(){
  for(std::vector<archiveForce>::iterator itf=vaf.begin(); itf!=vaf.end(); ++itf){
    archiveForce &af = *itf;
    if((af.vdof.size()==0) or (!_previousInit)){// otherwise it has already be initialized before in a previous computation
      std::vector<MVertex*> vv;
      pModel->getMeshVerticesForPhysicalGroup(af.dim,af.numphys,vv);
      // loop on domain (to find the domain where the force is applied)
      for(int idom = 0; idom < _mpiUserDom;++idom){ // Do not consider Ghost domain when archiving forces
        partDomain *dom = domainVector[idom];
        FunctionSpaceBase *sp = dom->getFunctionSpace();
        std::vector<Dof> R;
        for(groupOfElements::elementContainer::iterator ite = dom->g->begin(); ite!=dom->g->end(); ++ite){
          MElement *ele = *ite;
          int nbvertex = ele->getNumVertices();
          sp->getKeys(ele,R);
          for(std::vector<MVertex*>::iterator it=vv.begin(); it!=vv.end(); ++it){
            for(int j=0;j<nbvertex; j++){
              if(ele->getVertex(j) == *it){
                af.vdof.insert(R[j+nbvertex*af.comp]);
                break;
              }
            }
          }
          R.clear();
        }
      }
    }
    af.openFile();
  }
}

void nonLinearMechSolver::initArchivePathFollowing(){
  if (_pathFollowing ){
    std::ostringstream oss;
    std::string s2="";
   #if defined(HAVE_MPI)
    if(Msg::GetCommSize() != 1){
      oss.str("");
      oss << Msg::GetCommRank();
      s2 += "_part"+oss.str();
    }
   #endif // HAVE_MPI
    std::string fname = "pathResult"+s2+".csv";

    _controlStateFile = fopen(fname.c_str(),"w");
    fprintf(_controlStateFile,"Time; Control Parameter; State parameter;\n");
    fflush(_controlStateFile);
  }
};
void nonLinearMechSolver::endArchivePathFollowing(){
  if (_controlStateFile)
    fclose(_controlStateFile);
};
void nonLinearMechSolver::pathFollowingArchiving(const double curtime, const int numstep, dofManager<double>* assembler){
  if (_controlStateFile){
    std::string name = "A";
    linearSystem<double>* lsys = assembler->getLinearSystem(name);
    nonLinearSystem<double>* nsys = dynamic_cast<nonLinearSystem<double>*>(lsys);
    pathFollowingSystem<double>* psys = dynamic_cast<pathFollowingSystem<double>*>(lsys);

    double control = psys->getControlParameter();
    double state = psys->getStateParameter();

    fprintf(_controlStateFile,"%e; %e; %e; \n",curtime,control,state);

    if (numstep%10==0) fflush(_controlStateFile);
  }
};

void nonLinearMechSolver::archivingNodeDisplacement(const int numphys, const int comp,const int nstep){
  this->archivingNode(numphys,comp,nonLinearBoundaryCondition::position,nstep);
}

void nonLinearMechSolver::archivingNodeVelocity(const int numphys, const int comp,const int nstep){
  this->archivingNode(numphys,comp,nonLinearBoundaryCondition::velocity,nstep);
}

void nonLinearMechSolver::archivingNodeAcceleration(const int numphys, const int comp, const int nstep){
  this->archivingNode(numphys,comp,nonLinearBoundaryCondition::acceleration,nstep);
}
void nonLinearMechSolver::constraintBC(std::string onwhat, const int numphys, const int comp)
{
  nonLinearConstraintBC uc;
  const std::string node("Node");
  const std::string edge("Edge");
  const std::string face("Face");
  const std::string volume("Volume");
  if(onwhat==node){
    uc.g = new groupOfElements (0, numphys);
    uc.onWhat=nonLinearBoundaryCondition::ON_VERTEX;
  }
  else if(onwhat==edge){
    uc.g = new groupOfElements (1, numphys);
    uc.onWhat=nonLinearBoundaryCondition::ON_EDGE;
  }
  else if(onwhat==face){
    uc.g = new groupOfElements (2, numphys);
    uc.onWhat=nonLinearBoundaryCondition::ON_FACE;
  }
  else if(onwhat==volume){
    uc.g = new groupOfElements(3, numphys);
    uc.onWhat=nonLinearBoundaryCondition::ON_VOLUME;
  }
  else Msg::Error("Impossible to prescribe a constraint on a %s\n",onwhat.c_str());
  uc._comp=comp;
  uc._tag=numphys;
  allConstraint.push_back(uc);
}

void nonLinearMechSolver::setmeanValueOnDomain(const std::string vname,const int comp){
  _archiveMean.push_back(archiveMeanValue(comp,vname));
};

void nonLinearMechSolver::computeIPCompOnDomain(const double time){
  for (int i=0; i<_archiveMean.size(); i++){
    archiveMeanValue& am = _archiveMean[i];
    am.openFile(domainVector);
    fprintf(am.file,"%e;",time);
    for (int j=0; j<domainVector.size(); j++){
      partDomain* dom = domainVector[j];
      double val = dom->computeMeanValueDomain(am.comp,_ipf);
      fprintf(am.file,"%e;",val);
    }
    fprintf(am.file,"\n");
    am.closeFile();
  }
};

void nonLinearMechSolver::periodicBC(std::string onwhat, const int phys1, const int phys2, const int comp,
                                     const double xx, const double yy, const double zz){
  nonLinearPeriodicBCBetweenTwoGroups pbc(xx,yy,zz);
  const std::string node("Node");
  const std::string edge("Edge");
  const std::string face("Face");
  const std::string volume("Volume");

  if(onwhat==node){
    pbc.g1 = new groupOfElements (0, phys1);
    pbc.g2 = new groupOfElements (0, phys2);
    pbc.onWhat=nonLinearBoundaryCondition::ON_VERTEX;
  }
  else if(onwhat==edge){
    pbc.g1 = new groupOfElements (1, phys1);
    pbc.g2 = new groupOfElements (1, phys2);
    pbc.onWhat=nonLinearBoundaryCondition::ON_EDGE;
  }
  else if(onwhat==face){
    pbc.g1 = new groupOfElements (2, phys1);
    pbc.g2 = new groupOfElements (2, phys2);
    pbc.onWhat=nonLinearBoundaryCondition::ON_FACE;
  }
  else if(onwhat==volume){
    pbc.g1 = new groupOfElements(3, phys1);
    pbc.g2 = new groupOfElements(3, phys2);
    pbc.onWhat=nonLinearBoundaryCondition::ON_VOLUME;
  }
  else Msg::Error("Impossible to prescribe a constraint on a %s\n",onwhat.c_str());

  pbc.comp = comp;
  pbc.tag1 = phys1;
  pbc.tag2 = phys2;
  allPeriodic.push_back(pbc);
}

void nonLinearMechSolver::sameDisplacementBC(std::string onwhat, const int phy, const int rootphy, const int comp){
  Msg::Info("Displacement comp %d on physical %d is the same as on physical %d",comp,phy,rootphy);
  nonLinearSameDisplacementBC bc;

  bc.comp = comp;
  bc.tagRoot = rootphy;
  bc.gRoot = new groupOfElements(0,rootphy);
  bc._tag = phy;

  const std::string node("Node");
  const std::string edge("Edge");
  const std::string face("Face");
  const std::string volume("Volume");

  if(onwhat==node){
    bc.onWhat=nonLinearBoundaryCondition::ON_VERTEX;
    bc.g = new groupOfElements(0,phy);
  }
  else if (onwhat == edge){
    bc.onWhat=nonLinearBoundaryCondition::ON_EDGE;
    bc.g = new groupOfElements(1,phy);
  }
  else if (onwhat == face){
    bc.onWhat=nonLinearBoundaryCondition::ON_FACE;
    bc.g = new groupOfElements(2,phy);
  }
  else if (onwhat == volume){
    bc.onWhat=nonLinearBoundaryCondition::ON_VOLUME;
    bc.g = new groupOfElements(3,phy);
  }
  else{
    Msg::Error("Impossible to prescribe a constraint on a %s\n",onwhat.c_str());
  }

  allSameDisp.push_back(bc);
}

void nonLinearMechSolver::fixOnFace(std::string onwhat, const int phy, const double A, const double B, const double C, const double D){
  nonLinearFixOnFaceBC bc(A,B,C,D);
  const std::string node("Node");
  const std::string edge("Edge");
  const std::string face("Face");
  const std::string volume("Volume");

  bc._tag = phy;

  if(onwhat==node){
    bc.onWhat=nonLinearBoundaryCondition::ON_VERTEX;
    bc.g = new groupOfElements(0,phy);
  }
  else if (onwhat == edge){
    bc.onWhat=nonLinearBoundaryCondition::ON_EDGE;
    bc.g = new groupOfElements(1,phy);
  }
  else if (onwhat == face){
    bc.onWhat=nonLinearBoundaryCondition::ON_FACE;
    bc.g = new groupOfElements(2,phy);
  }
  else if (onwhat == volume){
    bc.onWhat=nonLinearBoundaryCondition::ON_VOLUME;
    bc.g = new groupOfElements(3,phy);
  }
  else{
    Msg::Error("Impossible to prescribe a constraint on a %s\n",onwhat.c_str());
  }

  allFixAllFace.push_back(bc);
};

void nonLinearMechSolver::applySameDispBC(dofManager<double>* p){
  if (isDgDomain()) return;
  for (int i=0; i<allSameDisp.size(); i++){

    // because of CG
    FunctionSpaceBase* spb = domainVector[0]->getFunctionSpace();
    FilterDof* fiterDof = domainVector[0]->createFilterDof(allSameDisp[i].comp);

    // get Root Dof
    MElement* e = *(allSameDisp[i].gRoot->begin());
    MVertex* vroot = e->getVertex(0);
    std::vector<Dof> R, dofRoot;
    spb->getKeys(e,R);
    for (int idof=0; idof<R.size(); idof++){
      if (fiterDof->operator()(R[idof])){
        dofRoot.push_back(R[idof]);
        break;
      }
    }

    std::vector<Dof> others;
    // get constraint Dof
    for (groupOfElements::vertexContainer::iterator it = allSameDisp[i].g->vbegin(); it!=allSameDisp[i].g->vend(); it++){
      MVertex* vv = *it;
      if (vv->getNum() != vroot->getNum()){
        MPoint point(*it);
        R.clear();
        spb->getKeys(&point,R);
        for (int idof=0; idof<R.size(); idof++){
          if (fiterDof->operator()(R[idof])){
            others.push_back(R[idof]);
            break;
          }
        }
      }
    }

    DofAffineConstraint<double> dof;
    dof.linear.push_back(std::pair<Dof,double>(dofRoot[0],1.));
    dof.shift = 0.;

    for (int j =0; j<others.size(); j++){
      p->setLinearConstraint(others[j],dof);
    };
  };

};

void nonLinearMechSolver::applyFixOnFace(dofManager<double>* p){
  if (isDgDomain()) return;
  for (int i=0; i<allFixAllFace.size(); i++){
    // because of CG
    FunctionSpaceBase* spb = domainVector[0]->getFunctionSpace();
    for (groupOfElements::vertexContainer::iterator it = allFixAllFace[i].g->vbegin();
              it!= allFixAllFace[i].g->vend(); it++){
      MVertex* v = *it;
      MPoint pt(v);


      double A = allFixAllFace[i].A;
      double B = allFixAllFace[i].B;
      double C = allFixAllFace[i].C;
      double D = allFixAllFace[i].D;

      std::vector<Dof> R;
      spb->getKeys(&pt,R);

      DofAffineConstraint<double> dof;
      double val = -1.*(A*v->x()+B*v->y()+C*v->z()+D);

      if (fabs(A)>= fabs(B) and fabs(A)>= fabs(C)){
        dof.shift = val/A;
        dof.linear.push_back(std::pair<Dof,double>(R[1],-1.*B/A));

        if (_dim == 3)
          dof.linear.push_back(std::pair<Dof,double>(R[2],-1.*C/A));

        p->setLinearConstraint(R[0],dof);
      }
      else if (fabs(B)>= fabs(A) and fabs(B)>= fabs(C)){
        dof.shift = val/B;
        dof.linear.push_back(std::pair<Dof,double>(R[0],-1.*A/B));

        if (_dim==3)
          dof.linear.push_back(std::pair<Dof,double>(R[2],-1.*C/B));

        p->setLinearConstraint(R[1],dof);
      }
      else if (fabs(C)>= fabs(A) and fabs(C)>= fabs(B) and _dim == 3){
        dof.shift = val/C;
        dof.linear.push_back(std::pair<Dof,double>(R[0],-1.*A/C));
        dof.linear.push_back(std::pair<Dof,double>(R[1],-1.*B/C));

        p->setLinearConstraint(R[2],dof);
      }
      else
        Msg::Error("error in nonLinearMechSolver::applyFixOnFace");

    }

  };
};

void nonLinearMechSolver::archivingNode(const int numphys, const int comp, nonLinearBoundaryCondition::whichCondition wc,const int nstep){
  // no distinction between cG/dG and full Dg formulation. class Displacement Field manage it
  std::vector<MVertex*> vv;
  pModel->getMeshVerticesForPhysicalGroup(0,numphys,vv);

  if(vv.size() == 1){
//    std::pair< Dof,initialCondition::whichCondition > pai(Dof(vv[0]->getNum(),Dof3IntType::createTypeWithThreeInts(comp,_tag)),wc);
    anoded.push_back(unknownField::archiveNode(vv[0]->getNum(),comp,wc,nstep));
    // remove old file (linux only ??)
    std::ostringstream oss;
    oss << vv[0]->getNum(); // Change this
    std::string s = oss.str();
    oss.str("");
    oss << comp;
    std::string s2 = oss.str();
    std::string rfname;
    #if defined(HAVE_MPI)
    if(Msg::GetCommSize() > 1)
    {
      oss.str("");
      oss << Msg::GetCommRank();
      s2 += "_part"+oss.str();
    }
    #endif // HAVE_MPI
    switch(wc){
     case nonLinearBoundaryCondition::position:
      rfname= "rm -f NodalDisplacement"+s+"comp"+s2+".csv";
      break;
     case nonLinearBoundaryCondition::velocity:
      rfname= "rm -f NodalVelocity"+s+"comp"+s2+".csv";
      break;
     case nonLinearBoundaryCondition::acceleration:
      rfname= "rm -f NodalAcceleration"+s+"comp"+s2+".csv";
      break;
    }
    system(rfname.c_str());
  }
  else{
    if(vv.size()==0)
      Msg::Warning("No physical group %d So it is impossible to archive nodal displacement",numphys);
    else
      Msg::Warning("More than one node in physical group impssible to archive for now (no loop)",numphys);
  }
}

void nonLinearMechSolver::archivingNode(const int numpphys, const int dim, const int comp, nonLinearBoundaryCondition::whichCondition wc,const int nstep){
  std::vector<MVertex*> vv;
  pModel->getMeshVerticesForPhysicalGroup(dim,numpphys,vv);
  std::ostringstream oss;
  oss << numpphys;
  std::string ss = oss.str();
  std::string fnam = "numver" + ss+ ".csv";
  FILE* file = fopen(fnam.c_str(),"w");
  for (int i=0; i<vv.size(); i++){
    std::ostringstream oss;
    oss << vv[i]->getNum(); // Change this
    std::string s = oss.str();
    oss.str("");
    oss << comp;
    std::string s2 = oss.str();
    std::string rfname;
    #if defined(HAVE_MPI)
    if(Msg::GetCommSize() > 1)
    {
      oss.str("");
      oss << Msg::GetCommRank();
      s2 += "_part"+oss.str();
    }
    #endif // HAVE_MPI
    switch(wc){
     case nonLinearBoundaryCondition::position:
      rfname= "rm -f NodalDisplacement*";
      break;
     case nonLinearBoundaryCondition::velocity:
      rfname= "rm -f NodalVelocity*";
      break;
     case nonLinearBoundaryCondition::acceleration:
      rfname= "rm -f NodalAcceleration*";
      break;
    }
    system(rfname.c_str());

    anoded.push_back(unknownField::archiveNode(vv[i]->getNum(),comp,wc,nstep));
    fprintf(file,"%d ;%f;%f;%f\n",vv[i]->getNum(),vv[i]->x(),vv[i]->y(),vv[i]->z());
  }
  fclose(file);
};


void nonLinearMechSolver::archivingRigidContact(const int numphys, const int comp, const int wc,const int nstep){
  // For rigid contact the value are disponible on all rank --> archiving only on last rank
  #if defined(HAVE_MPI)
  if(Msg::GetCommRank() == Msg::GetCommSize() -1)
  #endif// HAVE_MPI
  {
    nonLinearBoundaryCondition::whichCondition whc;
    switch(wc){
     case 0:
      whc = nonLinearBoundaryCondition::position;
      break;
     case 1:
      whc = nonLinearBoundaryCondition::velocity;
      break;
     case 2:
      whc = nonLinearBoundaryCondition::acceleration;
      break;
     default:
      whc = nonLinearBoundaryCondition::position;
    }
    anoded.push_back(unknownField::archiveNode(numphys,comp,whc,nstep,true));

    // remove of csv file Linux only
    std::ostringstream oss;
    oss << numphys; // Change this
    std::string s = oss.str();
    oss.str("");
    oss << comp;
    std::string s2 = oss.str();
    std::string rfname;
    std::string rfnameMPI;
    switch(whc){
     case nonLinearBoundaryCondition::position:
      rfname= "rm -f NodalDisplacement"+s+"comp"+s2+".csv";
      rfnameMPI = "rm -f NodalDisplacement"+s+"comp"+s2+"*.csv";
      break;
     case nonLinearBoundaryCondition::velocity:
      rfname= "rm -f NodalVelocity"+s+"comp"+s2+".csv";
      rfnameMPI= "rm -f NodalVelocity"+s+"comp"+s2+"*.csv";
      break;
     case nonLinearBoundaryCondition::acceleration:
      rfname= "rm -f NodalAcceleration"+s+"comp"+s2+".csv";
      rfnameMPI= "rm -f NodalAcceleration"+s+"comp"+s2+".*csv";
      break;
    }
    system(rfname.c_str());
    system(rfnameMPI.c_str());
  }
}

void nonLinearMechSolver::archivingNodeIP(const int numphys, const int ipval,const int elemval,const int nstep){
  vaip.push_back(IPField::ip2archive(0, numphys,ipval,elemval,nstep));
}

void nonLinearMechSolver::archivingIPOnPhysicalGroup(std::string onwhat, const int numphys, const int ipval,const int elemval, const int nstep){

  const std::string node("Node");
  const std::string edge("Edge");
  const std::string face("Face");
  const std::string volume("Volume");
  int dim= 0;
  if(onwhat==node){
     dim =0;
  }
  else if(onwhat==edge){
     dim =1;
  }
  else if(onwhat==face){
     dim =2;
  }
  else if(onwhat==volume){
     dim =3;
  }
  else Msg::Error("Impossible to prescribe a constraint on a %s\n",onwhat.c_str());
  vaip.push_back(IPField::ip2archive(dim, numphys,ipval,elemval,nstep));
}

void nonLinearMechSolver::contactInteraction(contactDomain *cdom){
  _allContact.insert(cdom);

}


//
// C++ Interface: terms explicit
//
// Description: Files with definition of function : nonLinearMechSolver::solveExplicit()
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
/*
double distanceMin(const double x1, const double x2, const double x3,
                    const double y1, const double y2, const double y3,
                    const double z1, const double z2, const double z3){
  double dist1 = sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)+(z1-z2)*(z1-z2));
  double dist2 = sqrt((x2-x3)*(x2-x3)+(y2-y3)*(y2-y3)+(z2-z3)*(z2-z3));
  if(dist1 > dist2)
    dist1 = sqrt((x3-x1)*(x3-x1)+(y3-y1)*(y3-y1)+(z3-z1)*(z3-z1));
  else
    dist2 = sqrt((x3-x1)*(x3-x1)+(y3-y1)*(y3-y1)+(z3-z1)*(z3-z1));
  if(dist1 > dist2)
    return dist2;
  else
    return dist1;
}
*/
// Define inner radius for high order element ...
double triangular2orderCharacteristicSize(MElement *ele, std::map<int,MElement*> mefo){
  MElement *elefo = mefo[ele->getType()];
  static std::vector<double> dist(4);
  elefo->setVertex(0,ele->getVertex(0)); elefo->setVertex(1,ele->getVertex(3)); elefo->setVertex(2,ele->getVertex(5));
  dist[0] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(3)); elefo->setVertex(1,ele->getVertex(4)); elefo->setVertex(2,ele->getVertex(5));
  dist[1] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(3)); elefo->setVertex(1,ele->getVertex(1)); elefo->setVertex(2,ele->getVertex(4));
  dist[2] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(5)); elefo->setVertex(1,ele->getVertex(4)); elefo->setVertex(2,ele->getVertex(2));
  dist[3] = elefo->getInnerRadius();
  return (*std::min_element(dist.begin(),dist.end()));
}
double triangular3orderCharacteristicSize(MElement *ele, std::map<int,MElement*> mefo){
  MElement *elefo = mefo[ele->getType()];
  static std::vector<double> dist(9);
  elefo->setVertex(0,ele->getVertex(0)); elefo->setVertex(1,ele->getVertex(3)); elefo->setVertex(2,ele->getVertex(8));
  dist[0] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(8)); elefo->setVertex(1,ele->getVertex(3)); elefo->setVertex(2,ele->getVertex(9));
  dist[1] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(3)); elefo->setVertex(1,ele->getVertex(4)); elefo->setVertex(2,ele->getVertex(9));
  dist[2] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(4)); elefo->setVertex(1,ele->getVertex(5)); elefo->setVertex(2,ele->getVertex(9));
  dist[3] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(4)); elefo->setVertex(1,ele->getVertex(5)); elefo->setVertex(2,ele->getVertex(1));
  dist[4] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(9)); elefo->setVertex(1,ele->getVertex(5)); elefo->setVertex(2,ele->getVertex(6));
  dist[5] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(7)); elefo->setVertex(1,ele->getVertex(9)); elefo->setVertex(2,ele->getVertex(6));
  dist[6] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(7)); elefo->setVertex(1,ele->getVertex(8)); elefo->setVertex(2,ele->getVertex(9));
  dist[7] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(7)); elefo->setVertex(1,ele->getVertex(6)); elefo->setVertex(2,ele->getVertex(2));
  dist[8] = elefo->getInnerRadius();
  return (*std::min_element(dist.begin(),dist.end()));
}

double quadrangle2orderCharacteristicSize(MElement *ele, std::map<int,MElement*> mefo){
  MElement *elefo = mefo[ele->getType()];
  static std::vector<double> dist(4);
  elefo->setVertex(0,ele->getVertex(0)); elefo->setVertex(1,ele->getVertex(4)); elefo->setVertex(2,ele->getVertex(8)); elefo->setVertex(3,ele->getVertex(7));
  dist[0] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(4)); elefo->setVertex(1,ele->getVertex(1)); elefo->setVertex(2,ele->getVertex(5)); elefo->setVertex(3,ele->getVertex(8));
  dist[1] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(8)); elefo->setVertex(1,ele->getVertex(5)); elefo->setVertex(2,ele->getVertex(2)); elefo->setVertex(3,ele->getVertex(6));
  dist[2] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(7)); elefo->setVertex(1,ele->getVertex(8)); elefo->setVertex(2,ele->getVertex(6)); elefo->setVertex(3,ele->getVertex(3));
  dist[3] = elefo->getInnerRadius();
  return (*std::min_element(dist.begin(),dist.end()));
}

double quadrangle3orderCharacteristicSize(MElement *ele, std::map<int,MElement*> mefo){
  MElement *elefo = mefo[ele->getType()];
  static std::vector<double> dist(9);
  elefo->setVertex(0,ele->getVertex(0)); elefo->setVertex(1,ele->getVertex(4)); elefo->setVertex(2,ele->getVertex(12)); elefo->setVertex(3,ele->getVertex(11));
  dist[0] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(4)); elefo->setVertex(1,ele->getVertex(5)); elefo->setVertex(2,ele->getVertex(13)); elefo->setVertex(3,ele->getVertex(12));
  dist[1] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(5)); elefo->setVertex(1,ele->getVertex(1)); elefo->setVertex(2,ele->getVertex(6)); elefo->setVertex(3,ele->getVertex(13));
  dist[2] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(13)); elefo->setVertex(1,ele->getVertex(6)); elefo->setVertex(2,ele->getVertex(7)); elefo->setVertex(3,ele->getVertex(14));
  dist[3] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(14)); elefo->setVertex(1,ele->getVertex(7)); elefo->setVertex(2,ele->getVertex(2)); elefo->setVertex(3,ele->getVertex(8));
  dist[4] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(15)); elefo->setVertex(1,ele->getVertex(14)); elefo->setVertex(2,ele->getVertex(8)); elefo->setVertex(3,ele->getVertex(9));
  dist[5] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(10)); elefo->setVertex(1,ele->getVertex(15)); elefo->setVertex(2,ele->getVertex(9)); elefo->setVertex(3,ele->getVertex(3));
  dist[6] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(11)); elefo->setVertex(1,ele->getVertex(12)); elefo->setVertex(2,ele->getVertex(15)); elefo->setVertex(3,ele->getVertex(10));
  dist[7] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(12)); elefo->setVertex(1,ele->getVertex(13)); elefo->setVertex(2,ele->getVertex(14)); elefo->setVertex(3,ele->getVertex(15));
  dist[8] = elefo->getInnerRadius();
  return (*std::min_element(dist.begin(),dist.end()));
}


double det3(const SVector3 &p1, const SVector3 &p2, const SVector3 &p3)
{
    double det;
    det = p1[0]*(p2[1]*p3[2]-p3[1]*p2[2]) -
          p1[1]*(p2[0]*p3[2]-p3[0]*p2[2]) +
          p1[2]*(p2[0]*p3[1]-p3[0]*p2[1]);
    return det;
}
double inradiusTetrahedron(const SVector3 &p1, const SVector3 &p2, const SVector3 &p3, const SVector3 &p4)
{
  double inradius;
  double l12,l13,l14,l23,l24,l34,S1,S2,S3,S4,volume;
  //  double r,s,t;
  l12 = (p1-p2).norm();
  l13 = (p1-p3).norm();
  l14 = (p1-p4).norm();
  l23 = (p3-p2).norm();
  l24 = (p4-p2).norm();
  l34 = (p4-p3).norm();

  S1 = (l12+l23+l13)/2;
  S1 = sqrt(S1*(S1-l12)*(S1-l23)*(S1-l13));

  S2 = (l12+l24+l14)/2;
  S2 = sqrt(S2*(S2-l12)*(S2-l24)*(S2-l14));

  S3 = (l23+l34+l24)/2;
  S3 = sqrt(S3*(S3-l23)*(S3-l34)*(S3-l24));

  S4 = (l13+l34+l14)/2;
  S4 = sqrt(S4*(S4-l13)*(S4-l34)*(S4-l14));

  volume = det3(p2,p3,p4);
  volume -= det3(p1,p3,p4);
  volume += det3(p1,p2,p4);
  volume -= det3(p1,p2,p3);
  volume /= 6;

  inradius = 3 * volume / (S1+S2+S3+S4);

  return inradius;
}

double tetrahedronCharacteristicSize(MElement *ele, unknownField *ufield, std::vector<Dof> &R){
  std::vector<double> disp;
  ufield->get(R,disp);
  SVector3 p1,p2,p3,p4;
  p1(0) = ele->getVertex(0)->x() + disp[0];
  p2(0) = ele->getVertex(1)->x() + disp[1];
  p3(0) = ele->getVertex(2)->x() + disp[2];
  p4(0) = ele->getVertex(3)->x() + disp[3];
  p1(1) = ele->getVertex(0)->y() + disp[4];
  p2(1) = ele->getVertex(1)->y() + disp[5];
  p3(1) = ele->getVertex(2)->y() + disp[6];
  p4(1) = ele->getVertex(3)->y() + disp[7];
  p1(2) = ele->getVertex(0)->z() + disp[8];
  p2(2) = ele->getVertex(1)->z() + disp[9];
  p3(2) = ele->getVertex(2)->z() + disp[10];
  p4(2) = ele->getVertex(3)->z() + disp[11];
  return inradiusTetrahedron(p1,p2,p3,p4);
}
double tetrahedron2orderCharacteristicSize(MElement *ele, unknownField *ufield, std::vector<Dof> &R){
  std::vector<double> disp;
  ufield->get(R,disp);
  SVector3 p1,p2,p3,p4,p5,p6,p7,p8,p9,p10;
  p1(0) = ele->getVertex(0)->x() + disp[0];
  p2(0) = ele->getVertex(1)->x() + disp[1];
  p3(0) = ele->getVertex(2)->x() + disp[2];
  p4(0) = ele->getVertex(3)->x() + disp[3];
  p5(0) = ele->getVertex(4)->x() + disp[4];
  p6(0) = ele->getVertex(5)->x() + disp[5];
  p7(0) = ele->getVertex(6)->x() + disp[6];
  p8(0) = ele->getVertex(7)->x() + disp[7];
  p9(0) = ele->getVertex(8)->x() + disp[8];
  p10(0) = ele->getVertex(9)->x() + disp[9];
  p1(1) = ele->getVertex(0)->y() + disp[10];
  p2(1) = ele->getVertex(1)->y() + disp[11];
  p3(1) = ele->getVertex(2)->y() + disp[12];
  p4(1) = ele->getVertex(3)->y() + disp[13];
  p5(1) = ele->getVertex(4)->y() + disp[14];
  p6(1) = ele->getVertex(5)->y() + disp[15];
  p7(1) = ele->getVertex(6)->y() + disp[16];
  p8(1) = ele->getVertex(7)->y() + disp[17];
  p9(1) = ele->getVertex(8)->y() + disp[18];
  p10(1) = ele->getVertex(9)->y() + disp[19];
  p1(2) = ele->getVertex(0)->z() + disp[20];
  p2(2) = ele->getVertex(1)->z() + disp[21];
  p3(2) = ele->getVertex(2)->z() + disp[22];
  p4(2) = ele->getVertex(3)->z() + disp[23];
  p5(2) = ele->getVertex(4)->z() + disp[24];
  p6(2) = ele->getVertex(5)->z() + disp[25];
  p7(2) = ele->getVertex(6)->z() + disp[26];
  p8(2) = ele->getVertex(7)->z() + disp[27];
  p9(2) = ele->getVertex(8)->z() + disp[28];
  p10(2) = ele->getVertex(9)->z() + disp[29];


  double r1=inradiusTetrahedron(p1, p5, p7, p8);
  double r2=inradiusTetrahedron(p2, p6, p5, p10);
  double r3=inradiusTetrahedron(p3, p7, p6, p9);
  double r4=inradiusTetrahedron(p8, p9, p4, p10);

  if (r1 < r2 and r1 < r3 and r1 < r4)
    return r1;
  else if (r2 < r1 and r2 < r3 and r2 < r4)
    return r2;
  else if (r3 < r1 and r3 < r2 and r3 < r4)
    return r3;
  else
    return r4;


}
/*
double tetrahedron2orderCharacteristicSize(MElement *ele, std::map<int,MElement*> mefo)
{
  MElement *elefo = mefo[ele->getType()];
  static std::vector<double> dist(4);
  elefo->setVertex(0,ele->getVertex(0)); elefo->setVertex(1,ele->getVertex(4)); elefo->setVertex(2,ele->getVertex(6)); elefo->setVertex(3,ele->getVertex(7));
  dist[0] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(1)); elefo->setVertex(1,ele->getVertex(5)); elefo->setVertex(2,ele->getVertex(4)); elefo->setVertex(3,ele->getVertex(9));
  dist[1] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(2)); elefo->setVertex(1,ele->getVertex(6)); elefo->setVertex(2,ele->getVertex(5)); elefo->setVertex(3,ele->getVertex(8));
  dist[2] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(7)); elefo->setVertex(1,ele->getVertex(8)); elefo->setVertex(2,ele->getVertex(3)); elefo->setVertex(3,ele->getVertex(9));
  dist[3] = elefo->getInnerRadius();
  return (*std::min_element(dist.begin(),dist.end()));
}
*/
double hexahedronCharacteristicSize(MElement *ele, unknownField *ufield, std::vector<Dof> &R){
  std::vector<double> disp;
  ufield->get(R,disp);
  SVector3 p1,p2,p3,p4,p5,p6,p7,p8;
  p1(0) = ele->getVertex(0)->x() + disp[0];
  p2(0) = ele->getVertex(1)->x() + disp[1];
  p3(0) = ele->getVertex(2)->x() + disp[2];
  p4(0) = ele->getVertex(3)->x() + disp[3];
  p5(0) = ele->getVertex(4)->x() + disp[4];
  p6(0) = ele->getVertex(5)->x() + disp[5];
  p7(0) = ele->getVertex(6)->x() + disp[6];
  p8(0) = ele->getVertex(7)->x() + disp[7];
  p1(1) = ele->getVertex(0)->y() + disp[8];
  p2(1) = ele->getVertex(1)->y() + disp[9];
  p3(1) = ele->getVertex(2)->y() + disp[10];
  p4(1) = ele->getVertex(3)->y() + disp[11];
  p5(1) = ele->getVertex(4)->y() + disp[12];
  p6(1) = ele->getVertex(5)->y() + disp[13];
  p7(1) = ele->getVertex(6)->y() + disp[14];
  p8(1) = ele->getVertex(7)->y() + disp[15];
  p1(2) = ele->getVertex(0)->z() + disp[16];
  p2(2) = ele->getVertex(1)->z() + disp[17];
  p3(2) = ele->getVertex(2)->z() + disp[18];
  p4(2) = ele->getVertex(3)->z() + disp[19];
  p5(2) = ele->getVertex(4)->z() + disp[20];
  p6(2) = ele->getVertex(5)->z() + disp[21];
  p7(2) = ele->getVertex(6)->z() + disp[22];
  p8(2) = ele->getVertex(7)->z() + disp[23];

  double r1=inradiusTetrahedron(p1,p2,p4,p5);
  double r2=inradiusTetrahedron(p2,p4,p5,p6);
  double r3=inradiusTetrahedron(p5,p6,p4,p8);
  double r4=inradiusTetrahedron(p2,p4,p6,p3);
  double r5=inradiusTetrahedron(p4,p8,p6,p3);
  double r6=inradiusTetrahedron(p6,p8,p7,p3);

  if (r1 < r2 and r1 < r3 and r1 < r4 and r1 < r5 and r1 < r6)
    return r1;
  else if (r2 < r1 and r2 < r3 and r2 < r4 and r2 < r5 and r2 < r6)
    return r2;
  else if (r3 < r1 and r3 < r2 and r3 < r4 and r3 < r5 and r3 < r6)
    return r3;
  else if (r4 < r1 and r4 < r2 and r4 < r3 and r4 < r5 and r4 < r6)
    return r4;
  else if (r5 < r1 and r5 < r2 and r5 < r3 and r5 < r4 and r5 < r6)
    return r5;
  else
    return r6;


}
double hexahedron2orderCharacteristicSize(MElement *ele, unknownField *ufield, std::vector<Dof> &R){
  std::vector<double> disp;
  ufield->get(R,disp);
  SVector3 p1,p2,p3,p4,p5,p6,p7,p8;
  p1(0) = ele->getVertex(0)->x() + disp[0];
  p2(0) = ele->getVertex(1)->x() + disp[1];
  p3(0) = ele->getVertex(2)->x() + disp[2];
  p4(0) = ele->getVertex(3)->x() + disp[3];
  p5(0) = ele->getVertex(4)->x() + disp[4];
  p6(0) = ele->getVertex(5)->x() + disp[5];
  p7(0) = ele->getVertex(6)->x() + disp[6];
  p8(0) = ele->getVertex(7)->x() + disp[7];
  p1(1) = ele->getVertex(0)->y() + disp[8];
  p2(1) = ele->getVertex(1)->y() + disp[27];
  p3(1) = ele->getVertex(2)->y() + disp[28];
  p4(1) = ele->getVertex(3)->y() + disp[29];
  p5(1) = ele->getVertex(4)->y() + disp[30];
  p6(1) = ele->getVertex(5)->y() + disp[31];
  p7(1) = ele->getVertex(6)->y() + disp[32];
  p8(1) = ele->getVertex(7)->y() + disp[33];
  p1(2) = ele->getVertex(0)->z() + disp[54];
  p2(2) = ele->getVertex(1)->z() + disp[55];
  p3(2) = ele->getVertex(2)->z() + disp[56];
  p4(2) = ele->getVertex(3)->z() + disp[57];
  p5(2) = ele->getVertex(4)->z() + disp[58];
  p6(2) = ele->getVertex(5)->z() + disp[59];
  p7(2) = ele->getVertex(6)->z() + disp[60];
  p8(2) = ele->getVertex(7)->z() + disp[61];

  double r1=inradiusTetrahedron(p1,p2,p4,p5)/2.;
  double r2=inradiusTetrahedron(p2,p4,p5,p6)/2.;
  double r3=inradiusTetrahedron(p5,p6,p4,p8)/2.;
  double r4=inradiusTetrahedron(p2,p4,p6,p3)/2.;
  double r5=inradiusTetrahedron(p4,p8,p6,p3)/2.;
  double r6=inradiusTetrahedron(p6,p8,p7,p3)/2.;

  if (r1 < r2 and r1 < r3 and r1 < r4 and r1 < r5 and r1 < r6)
    return r1;
  else if (r2 < r1 and r2 < r3 and r2 < r4 and r2 < r5 and r2 < r6)
    return r2;
  else if (r3 < r1 and r3 < r2 and r3 < r4 and r3 < r5 and r3 < r6)
    return r3;
  else if (r4 < r1 and r4 < r2 and r4 < r3 and r4 < r5 and r4 < r6)
    return r4;
  else if (r5 < r1 and r5 < r2 and r5 < r3 and r5 < r4 and r5 < r6)
    return r5;
  else
    return r6;


}
/*
double hexahedron2orderCharacteristicSize(MElement *ele, std::map<int,MElement*> mefo)
{
  MElement *elefo = mefo[TYPE_TET]; // as compute by dividing in Tetrahedra
  static std::vector<double> dist(6);
  elefo->setVertex(0,ele->getVertex(0)); elefo->setVertex(1,ele->getVertex(1)); elefo->setVertex(2,ele->getVertex(3)); elefo->setVertex(3,ele->getVertex(4));
  dist[0] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(1)); elefo->setVertex(1,ele->getVertex(3)); elefo->setVertex(2,ele->getVertex(4)); elefo->setVertex(3,ele->getVertex(5));
  dist[1] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(4)); elefo->setVertex(1,ele->getVertex(5)); elefo->setVertex(2,ele->getVertex(3)); elefo->setVertex(3,ele->getVertex(7));
  dist[2] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(1)); elefo->setVertex(1,ele->getVertex(3)); elefo->setVertex(2,ele->getVertex(5)); elefo->setVertex(3,ele->getVertex(2));
  dist[3] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(3)); elefo->setVertex(1,ele->getVertex(7)); elefo->setVertex(2,ele->getVertex(5)); elefo->setVertex(3,ele->getVertex(2));
  dist[4] = elefo->getInnerRadius();
  elefo->setVertex(0,ele->getVertex(5)); elefo->setVertex(1,ele->getVertex(7)); elefo->setVertex(2,ele->getVertex(6)); elefo->setVertex(3,ele->getVertex(2));
  dist[5] = elefo->getInnerRadius();
  return 0.5*(*std::min_element(dist.begin(),dist.end())); // 0.5 as it is defined before like that !
}
*/

// create an element for each element type
// which will be used to compute the time step
void nonLinearMechSolver::initMapMElementToComputeTimeStep()
{
  MElementFactory factory;
  std::vector<MVertex*> vver;
  std::map<int,MElement*>::iterator ite;
  for(int i=0;i< _mpiUserDom;i++)
  {
    partDomain * dom = domainVector[i];
    for(groupOfElements::elementContainer::iterator it=dom->g->begin(); it!=dom->g->end(); ++it){
      MElement *ele = *it;
      ite = _mapMElementTmp.find(ele->getTypeForMSH());
      if(ite==_mapMElementTmp.end()) // create one new element all vertex are in (0,0,0) They will be filled later
      {
        vver.clear();
        for(int j=0;j<ele->getNumVertices();j++)
        {
          vver.push_back(new MVertex(0.,0.,0.));
        }
        _mapMElementTmp.insert(std::pair<int,MElement*>(ele->getTypeForMSH(),factory.create(ele->getTypeForMSH(),vver)));
      }
      ite = _mapMElementFirstOrder.find(ele->getType());
      if(ite == _mapMElementFirstOrder.end())
      {
        vver.clear();
        for(int j=0;j<ele->getNumPrimaryVertices();j++)
        {
          vver.push_back(new MVertex(0.,0.,0.));
        }
        MElement *eletmp;
        switch(ele->getType())
        {
         case TYPE_PNT:
          eletmp = new MPoint(vver[0]); break;
         case TYPE_LIN:
          eletmp = factory.create(MSH_LIN_2,vver); break;
         case TYPE_TRI:
          eletmp = factory.create(MSH_TRI_3,vver); break;
         case TYPE_QUA:
          eletmp = factory.create(MSH_QUA_4,vver); break;
         case TYPE_TET:
          eletmp = factory.create(MSH_TET_4,vver); break;
         case TYPE_PYR:
          eletmp = factory.create(MSH_PYR_5,vver); break;
         case TYPE_PRI:
          eletmp = factory.create(MSH_PRI_6,vver); break;
         case TYPE_HEX:
          eletmp = factory.create(MSH_HEX_8,vver); break;
         case TYPE_POLYG:
          eletmp = factory.create(MSH_POLYG_,vver); break;
         case TYPE_POLYH:
          eletmp = factory.create(MSH_POLYH_,vver); break;
         default:
          Msg::Fatal("unknown element type %d",ele->getType());
        }
        _mapMElementFirstOrder.insert(std::pair<int,MElement*>(ele->getType(),eletmp));
      }
    }
  }
}

double nonLinearMechSolver::criticalExplicitTimeStep(unknownField *ufield){
  // evaluate the critical time step carachteristic length = hs
  double hs;
  double dtmin=1.e100; // Initialization to infinity
  for(int i=0; i< _mpiUserDom; i++){
    partDomain *dom = domainVector[i];
    if(dom->g->size() > 0){ // otherwise no element (interface domain and no need to compute time step)
      double hsmin= 1.e100; // Initialization to infinity
      FunctionSpaceBase *spdom = dom->getFunctionSpace();
      std::vector<Dof> R;
      std::vector<double> disp;
      materialLaw *mlt = dom->getMaterialLaw();
      double celerity = mlt->soundSpeed();
      double scaleTimeStep = dom->scaleTimeStep();
      for(groupOfElements::elementContainer::iterator it=dom->g->begin(); it!=dom->g->end(); ++it){
       MElement *e = *it;
       // nodal displacement 3 per nodes FIX THIS ASSUMPTION
       if(dom->considerForTimeStep(_ipf->getAips(),e))
       {
	spdom->getKeys(e,R);
        int nkeys = spdom->getNumKeys(e);
        int nbvertex = e->getNumVertices();
//        int nkeyspernodes = nkeys / nbvertex;
//        if(nkeyspernodes > 3) nkeyspernodes=3; // if more than 3 unknowns per node the 3 first one have to be the displacement
        ufield->get(R,disp);
        MElement *etmp;
        if((e->getTypeForMSH()!=MSH_TET_10) and (e->getTypeForMSH()!=MSH_HEX_27)) // some functions are undefined in 3D so cannot use new method!
        {
          etmp = _mapMElementTmp[e->getTypeForMSH()];
          for(int j=0;j<nbvertex;j++)
          {
            //for(int jj=0;jj<nkeyspernodes;jj++){}
            MVertex *ver = e->getVertex(j);
            MVertex *vertmp = etmp->getVertex(j);
            vertmp->x() = ver->x() + disp[j];
            vertmp->y() = ver->y() + disp[j+nbvertex];
            vertmp->z() = ver->z() + disp[j+nbvertex+nbvertex];
          }
        }
        // Unfortunally, computation of characteristic length depends on the element type
        // ADD THIS FUNCTIONS directly in gmsh (method of MELEMENT)
        // Problem as inner radius is defined only for 1st order element
        switch(e->getTypeForMSH()){
         case MSH_QUA_9 :
          hs = quadrangle2orderCharacteristicSize(etmp,_mapMElementFirstOrder);
          break;
         case MSH_QUA_16:
          hs = quadrangle3orderCharacteristicSize(etmp,_mapMElementFirstOrder);
          break;
         case MSH_TRI_6 :
          hs = triangular2orderCharacteristicSize(etmp,_mapMElementFirstOrder);
          break;
         case MSH_TRI_10:
          hs = triangular3orderCharacteristicSize(etmp,_mapMElementFirstOrder);
          break;
         case MSH_TET_10:
          hs = tetrahedron2orderCharacteristicSize(e,ufield,R);
          break;
         case MSH_HEX_27:
          hs = hexahedron2orderCharacteristicSize(e,ufield,R);
          break;
         default :
          //default criteria innerRadius()/polynomialOrder() as innerRAdius is defined for first order only !!
          // keep divide by polynomialOrder to be general for undefined case
          hs = etmp->getInnerRadius()/e->getPolynomialOrder();
        }
        if(hs<0)
        {
          Msg::Fatal("Negative characteristic size for element %d on rank %d",e->getNum(),Msg::GetCommRank());
        }
        if(hs<hsmin) hsmin = hs;
        R.clear(); disp.clear();
       }
      }
      double dt = 0.5*scaleTimeStep*hsmin/celerity;  // WHY 0.5 ??
      if(dt < dtmin ) dtmin = dt;
    }
  }

  // Delta t of contact
/*  explicitHCDofManager<double>* dynass = static_cast<explicitHCDofManager<double>*>(pAssembler);
  // filter because one component is needed (same mass in all direction)
  DgC0ShellFilterDofComponent filDof(0);
  for(nonLinearMechSolver::contactContainer::iterator itc = _allContact.begin(); itc!=_allContact.end(); ++itc){
    contactDomain *cdom = *itc;
    DgC0rigidContactSpace *csp = static_cast<DgC0rigidContactSpace*>(spaceManager->get(cdom));
    double penalty = cdom->getPenalty();
    for(groupOfElements::elementContainer::iterator it = cdom->gSlave->begin(); it!=cdom->gSlave->end(); ++it){
      MElement *ele = *it;
      // get mass
//      int nbdofs = csp->getNumKeys(ele);
      int nbvertex = ele->getNumVertices();
//      int nbdofsGC = csp->getNumKeysOfGravityCenter();
//      int nbcomp = (nbdofs-nbdofsGC)/nbvertex;
      std::vector<Dof> R;
      std::vector<double> vmass;
      csp->getKeys(ele,R);
      std::vector<Dof> Rfilter;
      filDof.filter(R,Rfilter);
      dynass->getVertexMass(Rfilter,vmass);
      double Mmaster = vmass[nbvertex];
      for(int i=0; i<nbvertex; i++){
        double dt = 10000.*sqrt(0.5*Mmaster*vmass[i]/penalty/(Mmaster+vmass[i]));
        if(dt < dtmin ){
          dtmin = dt;
          Msg::Info("Dtc is fixed by contact");
        }
      }
    }
  }*/
  return dtmin;
}

void nonLinearMechSolver::setInitialCondition(){
  dofManager<double>* currentManager = NULL;
  for(int i=0; i<allinitial.size();i++){
    initialCondition &initC = allinitial[i];
    if(allinitial[i]._comp != -1){
      // select the system if required
      if(_vcompBySys.size()==0)
      {
        currentManager = pAssembler;
      }
      else
      {
        dofManagerMultiSystems<double>* multiManager = static_cast<dofManagerMultiSystems<double>*>(pAssembler);
        currentManager = multiManager->getDofManagerByComp(initC._comp);
      }
      for(int j=0;j<initC._vspace.size();j++){
        SetInitialDofs(initC._vspace[j],initC._vgroup[j]->begin(),initC._vgroup[j]->end(),initC._mycondition,*currentManager,initC._f,*(initC._vfilter[j]),initC._vdg[j]);
      }
    }
    else{
      // select the system if required
      if(_vcompBySys.size()==0)
      {
        currentManager = pAssembler;
      }
      else
      {
        Msg::Warning("setNormalInitialDofs with a multisystem --> Do the hypothesis that displacement is the first system !");
        dofManagerMultiSystems<double>* multiManager = static_cast<dofManagerMultiSystems<double>*>(pAssembler);
        currentManager = multiManager->getManager(0);
      }
      for(int j=0;j<initC._vspace.size();j++){
        groupOfElements *groupDom = NULL;
        if(initC._vgroup[j] != initC._vDomGroup[j])
        {
          groupDom = initC._vDomGroup[j];
        }
        SetNormalInitialDofs(initC._vspace[j],initC._vgroup[j]->begin(),initC._vgroup[j]->end(),initC._mycondition,*currentManager,initC._f,*(initC._vfilter[j]),initC._vdg[j],groupDom);
      }
    }
  }
  // Eventually read file with initial position
/*  if(initOrResartfname != ""){
    // loop on all elements on all domain
    FunctionSpaceBase *sp; // to create the dofs
    std::vector<Dof> R;
    std::vector<double> val;
    for(std::vector<partDomain*>::iterator itdom = domainVector.begin(); itdom != domainVector.end(); ++itdom){
      partDomain *dom = *itdom;
      sp = spaceManager->get(dom);
      for(groupOfElements::elementContainer::iterator it = dom->g->begin(); it != dom->g->end(); ++it){
        MElement *ele = *it;
        // create dof of element
        sp->getKeys(ele,R);

        // clear
        R.clear();
      }

    }

  }*/
}

double nonLinearMechSolver::solveExplicit(){

  this->initializeExplicitScheme();
  double curtime = 0.;
  /* time loop */
  while(curtime<endtime){ // end via break for dynamic relaxation
    curtime = this->oneExplicitStep(curtime);
  }

  /* end of scheme */
  return this->finalizeExplicitScheme(curtime);
}

void nonLinearMechSolver::initializeExplicitScheme()
{
  Msg::Info("Explicit Data: endtime = %e beta=%f gamma=%f alpha_m=%f",this->getEndTime(), this->_beta, this->_gamma, this->_alpham);

  /* init data */
  this->init();
  this->init2(_ufield,_energField);
  staticDofManager<double> *sAssembler = static_cast<staticDofManager<double>*>(pAssembler);

  /* mass matrix computation outside the loop */
  for(std::vector<partDomain*>::iterator it = domainVector.begin(); it!=domainVector.end(); ++it){
    partDomain *dom = *it;
    AssembleMass(dom->getBilinearMassTerm(),*(dom->getFunctionSpace()),dom->g->begin(),dom->g->end(),
               *(dom->getBulkGaussIntegrationRule()),*pAssembler);
  }
  if(_unresetedContact){
    for(nonLinearMechSolver::contactContainer::iterator itc = _allContact.begin(); itc!= _allContact.end(); ++itc){
      contactDomain *cdom = *itc;
      if(cdom->isRigid()){
        rigidContactSpaceBase *rcsp = static_cast<rigidContactSpaceBase*>(cdom->getSpace());
        AssembleMass(cdom->getMassTerm(),rcsp,pAssembler);
      }
    }
  }

  /* time initialization */
  sAssembler->nextStep();
  _currentStep = 0;
}

double nonLinearMechSolver::oneExplicitStep(const double curtime)
{
  // Make these to variables static as they have to be kept
  // betwwen 2 calls to this function
  static double timeStep=0.;
  static staticDofManager<double> *sAssembler = static_cast<staticDofManager<double>*>(pAssembler);

  // compute time step and current time
  if(_currentStep%_numstepExpl ==0 ){
    timeStep = _gammas * this->criticalExplicitTimeStep(_ufield);
   #if defined(HAVE_MPI)
    if(Msg::GetCommSize()>1){
      // in case of mpi the time step is choosen as the minimal value compute on each partition
      double mpiTimeStep;
      MPI_Allreduce(&timeStep,&mpiTimeStep,1,MPI_DOUBLE,MPI_MIN,MPI_COMM_WORLD);
      timeStep = mpiTimeStep;
    }
   #endif // HAVE_MPI
    Msg::Info("time step value: %e at time %e",timeStep,curtime);
    sAssembler->setTimeStep(timeStep);
  }
  double mytime= curtime + timeStep;

  if(mytime>endtime) mytime = endtime;

  this->oneStepPreSolve(mytime,timeStep);

  // solve via assembler to perform mpi operation (communication before and after systemSolve
  pAssembler->systemSolve();

  // compute the new forces
  _ipf->compute1state(IPStateBase::current,false);
  double normRHS = this->computeRightHandSide(pAssembler,_ufield);
  if(isnan(normRHS))
  {
    Msg::Fatal("Nan force value --> end");
    return endtime; // To finish the loop
  }
  if(_dynamicRelaxation){
    double norm0 = sAssembler->norm0Inf();
    if(_currentStep%_numstepExpl ==0 ) Msg::Info("Rel norm for dynamic relaxation %e tol = %e",normRHS/norm0,_tol);
    if(normRHS/norm0 < _tol){
      Msg::Info("Dynamic relaxation procedure is converged. Return endtime to finish the loop");
      return endtime;
    }
    if(mytime == endtime)
      Msg::Info("Dynamic relaxation ended as the final time is reached. But convergence is not achieved!");
  }

  this->oneStepPostSolve(_ufield,_energField,mytime,_currentStep+1);
  _currentStep++;

  return mytime;
}

double nonLinearMechSolver::finalizeExplicitScheme(const double curtime)
{
  this->endOfScheme(_ufield,_energField,curtime,_currentStep);
  Msg::Info("Explicit OK");
  return curtime;
}

void nonLinearMechSolver::createOnelabViewOption(const std::string &fname,const int displayType) const
{
  std::string optfname(fname);
  optfname += ".opt";
  FILE * fopt = fopen(optfname.c_str(),"w");

  fprintf(fopt,"n = %d;\n",_GmshOneLabViewNum);
  fprintf(fopt,"View[n].VectorType = %d;\n",displayType);

  fclose(fopt);

  _GmshOneLabViewNum++;
  return;
}

void nonLinearMechSolver::createOnelabDisplacementView(const std::string &fname, const double time) const
{
  static bool initialized = false;
  // ask the unknown field to create the view
  _ufield->onelabView(_meshFileName,fname,domainVector,std::string("displacement"),0,time,_currentStep);

  if(!initialized)
  {
    this->createOnelabViewOption(fname,5); // 5 for displacement view type
    initialized = true;
  }
}

void nonLinearMechSolver::createOnelabVelocityView(const std::string &fname, const double time) const
{
  static bool initialized = false;
  // ask the unknown field to create the view
  _ufield->onelabView(_meshFileName,fname,domainVector,std::string("velocity"),1,time,_currentStep);

  if(!initialized)
  {
    this->createOnelabViewOption(fname,4); // 4 for 3D arrow view type
    initialized = true;
  }
}

void nonLinearMechSolver::getOnelabArchiveNodalUnknowns(fullVector<double> &nodalValues) const
{
  _ufield->valuesForOnelab(nodalValues);
  return;
}

void nonLinearMechSolver::forceArchiving(const double curtime,const int numstep,const dofManager<double>* pAssembler)
{
    const staticDofManager<double> *sAssembler = static_cast<const staticDofManager<double>*>(pAssembler);
    sAssembler->getForces(vaf); // get force value from system
    for(int i=0;i<vaf.size();i++){
      if((numstep%(vaf[i].nstep)==0) and (vaf[i].vdof.size()!=0))
      {
         fprintf(vaf[i].FP,"%e;%e\n",curtime,vaf[i].fval);
         if( numstep%(vaf[i].nstep*10) == 0) fflush(vaf[i].FP);
      }
    }
}

void nonLinearMechSolver::endArchiveForce()
{
  for(int i=0;i<vaf.size();i++)
    vaf[i].closeFile();
}

//
// C++ Interface: terms Quasi Static
//
// Description: Files with definition of function : nonLinearMechSolver::solveSNL()
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//

// Function to compute the initial norm
double nonLinearMechSolver::computeNorm0(linearSystem<double> *lsys, dofManager<double> *pAssembler,
                                     unknownField *ufield, IPField *ipf){
  double normFext,normFint;
  // Set all component of RightHandSide to zero (only RightHandSide is used to compute norm
  lsys->zeroRightHandSide();

  // fext norm
  // compute ext forces
  this->computeExternalForces(ufield);

  // Contact forces
  this->computeContactForces();

  // norm
  normFext = lsys->normInfRightHandSide();

  // Internal forces
  lsys->zeroRightHandSide();
  // compute internal forces
    this->computeInternalForces(ufield);
    //norm
    normFint = lsys->normInfRightHandSide();

    // archive
    return normFext+normFint;
}
// compute Fext-Fint
double nonLinearMechSolver::computeRightHandSide(dofManager<double> *pAssembler,
                                    unknownField *ufield){
  //lsys->zeroRightHandSide();
  staticDofManager<double> *pA2 = static_cast<staticDofManager<double>*>(pAssembler);
  pA2->clearRHSfixed();

  this->computeInternalForces(ufield);

  // compute ext forces
  if (_pathFollowing == false)
    this->computeExternalForces(ufield);

  // compute contact forces
  this->computeContactForces();

  // for periodic BC
  if (_microFlag){
    if (_systemType == nonLinearMechSolver::MULT_ELIM){
      _pAl->assembleConstraintResidualToSystem();
      this->computeStiffMatrix(pAssembler);
    }
    else if (_systemType == nonLinearMechSolver::DISP_MULT) {
      _pAl->computeLinearConstraints();
    }
  }
  // save Fint component to archive ( write in file when Fint = Fext)
//  return lsys->normInfRightHandSide();
  pA2->normInfRightHandSide();
}

void nonLinearMechSolver::computeInternalForces(unknownField *ufield){
  staticDofManager<double>* nlAssembler = static_cast<staticDofManager<double>*>(pAssembler);
  for(std::vector<partDomain*>::iterator itdom=domainVector.begin(); itdom!=domainVector.end();++itdom)
  {
    partDomain *dom = *itdom;
    Assemble(dom->getLinearBulkTerm(),*(dom->getFunctionSpace()),dom->g->begin(),dom->g->end(),
             *(dom->getBulkGaussIntegrationRule()),ufield,*nlAssembler,
             nonLinearSystem<double>::Fint);

    if(dom->IsInterfaceTerms()){
      dgPartDomain *dgdom = static_cast<dgPartDomain*>(dom);
      if(whatScheme!=Multi)
      {
        // Assembling loop on elementary interface terms
        Assemble(dgdom->getLinearInterfaceTerm(),*(dgdom->getFunctionSpace()),dgdom->gi->begin(),dgdom->gi->end(),*(dgdom->getInterfaceGaussIntegrationRule()),
                 ufield,*nlAssembler,nonLinearSystem<double>::Fint); // Use the same GaussQuadrature rule than on the boundary
      }
      else
      {
        dofManagerMultiSystems<double>* multiManager = static_cast<dofManagerMultiSystems<double>*>(pAssembler);
        // Assembling loop on elementary interface terms
        AssembleInterVectorMulti(dgdom->getLinearInterfaceTerm(),*(dgdom->getFunctionSpace()),dgdom->gi->begin(),dgdom->gi->end(),*(dgdom->getInterfaceGaussIntegrationRule()),
                        ufield,*multiManager); // Use the same GaussQuadrature rule than on the boundary
      }
      // Assembling loop on elementary boundary interface terms
      Assemble(dgdom->getLinearVirtualInterfaceTerm(),*(dgdom->getFunctionSpace()),dgdom->gib->begin(),dgdom->gib->end(),
               *(dgdom->getInterfaceGaussIntegrationRule()),ufield,*nlAssembler,nonLinearSystem<double>::Fint); // Use the same GaussQuadrature rule than on the boundary
    }
  }
}

void nonLinearMechSolver::computeContactForces(){
  staticDofManager<double>* nlAssembler = static_cast<staticDofManager<double>*>(pAssembler);
  for(contactContainer::iterator it = _allContact.begin(); it!=_allContact.end(); ++it){
    contactDomain *cdom = *it;
    contactLinearTermBase<double> *cterm = static_cast<contactLinearTermBase<double>*>(cdom->getForceTerm());
    cterm->clearContactNodes();
    Assemble(*(cterm),*(cdom->getSpace()),cdom->gSlave->begin(),
            cdom->gSlave->end(),*(cdom->getGaussIntegration()),*nlAssembler,nonLinearSystem<double>::Fext);
  }
}

void nonLinearMechSolver::computeStiffMatrix(dofManager<double> *pmanager){
  if (_stiffEstimation)
  {
    if (_stiffnessModification ==false){
      _stiffEstimation = false;
    }
    else{
      _stiffEstimation = true;
    };

    if(_vcompBySys.size()==0) // otherwise problem with the staticLinear scheme that uses a classical dofManager !
    {
      static std::string Aname("A");
      pmanager->getLinearSystem(Aname)->zeroMatrix();
    }
    else
    {
      staticDofManager<double>* smanager = static_cast<staticDofManager<double>*>(pmanager);
      smanager->zeroMatrix();
    }
    for(std::vector<partDomain*>::iterator itdom=domainVector.begin(); itdom!=domainVector.end(); ++itdom)
    {
      partDomain* dom = *itdom;
      // Assembling loop on Elementary terms
      Assemble(*(dom->getBilinearBulkTerm()),*(dom->getFunctionSpace()),dom->g->begin(),dom->g->end(),
               *(dom->getBulkGaussIntegrationRule()),*pAssembler);

      if(dom->IsInterfaceTerms()){
        dgPartDomain *dgdom = static_cast<dgPartDomain*>(dom);
        if(whatScheme!=Multi)
        {
          // Assembling loop on elementary interface terms
          Assemble(*(dgdom->getBilinearInterfaceTerm()),*(dom->getFunctionSpace()),dgdom->gi->begin(),dgdom->gi->end(),
                   *(dgdom->getInterfaceGaussIntegrationRule()),*pAssembler);
        }
        else
        {
          dofManagerMultiSystems<double>* multiAssembler = static_cast<dofManagerMultiSystems<double>*>(pAssembler);
          // Assembling loop on elementary interface terms
          AssembleInterMatrixMulti(*(dgdom->getBilinearInterfaceTerm()),*(dom->getFunctionSpace()),dgdom->gi->begin(),dgdom->gi->end(),
                                   *(dgdom->getInterfaceGaussIntegrationRule()),*multiAssembler);
        }
        // Assembling loop on elementary boundary interface terms
        Assemble(*(dgdom->getBilinearVirtualInterfaceTerm()),*(dom->getFunctionSpace()),dgdom->gib->begin(),dgdom->gib->end(),
                 *(dgdom->getInterfaceGaussIntegrationRule()),*pAssembler);
      }
    }

    // Contact
    // little particular because it the term itself which know (via its linked linear term) the element
    // where there is contact and so a bilinearterm != 0 as to be computed
    for(contactContainer::iterator it = _allContact.begin(); it!=_allContact.end(); ++it){
      contactDomain *cdom = *it;
      contactBilinearTermBase<double> *sterm = static_cast<contactBilinearTermBase<double>*>(cdom->getStiffnessTerm());
      Assemble(*sterm,*(cdom->getSpace()),sterm->getContactNodes()->elemBegin(),
                      sterm->getContactNodes()->elemEnd(),*(cdom->getGaussIntegration()),*pAssembler);
  //    sterm->clearContactNodes();
    }

    // for periodic BC
    if (_microFlag)
      if (_systemType == nonLinearMechSolver::DISP_MULT)
        _pAl->assembleConstraintMatrix();
  }
}

// Newton Raphson scheme to solve one step
int nonLinearMechSolver::NewtonRaphson(dofManager<double> *pmanager,unknownField *ufield){
  staticDofManager<double>* staticAssembler = static_cast<staticDofManager<double>*>(pmanager);
  if (whatScheme == Implicit){
    staticAssembler->systemSolveIntReturn();
  }

  // compute ipvariable
  _ipf->compute1state(IPStateBase::current, true);

  // Compute Right Hand Side (Fext-Fint)
  double normFinf = this->computeRightHandSide(pmanager,ufield);


  // norm0 = norm(Fext) + norm(Fint) to have a relative convergence
  double norm0 = staticAssembler->norm0Inf();

  // loop until convergence
  int iter=0;
  if(norm0 == 0){ // no force (can append if contact )
    Msg::Info("no force go next step");
    if (_microFlag) this->computeStiffMatrix(pmanager);
    return 0;
  }
  double relnorm = normFinf/norm0;
  Msg::Info("iteration n %d : residu : %e",iter,relnorm);
  while (relnorm>_tol or iter == 0){ // ensure 1 iteration min

    iter++;
    // Compute Stiffness Matrix
    this->computeStiffMatrix(pmanager);

    // Solve KDu = Fext-Fint
    int succeed = staticAssembler->systemSolveIntReturn();
    if(!succeed)
    {
      staticAssembler->resetUnknownsToPreviousTimeStep();
      iter = _maxNRite;
      break;
    }
    // update ipvariable
    _ipf->compute1state(IPStateBase::current,true);

    if (!_iterativeNR) break;
    // new forces
    normFinf = this->computeRightHandSide(pmanager,ufield);

    // Check of convergence
    relnorm = normFinf/norm0;
    Msg::Info("iteration n %d : residu : %e",iter,relnorm);

    // perform line search if activate (default)
    while((relnorm > _tol) and (iter > 1))    // avoid line search for linear case
    {
      int ls = staticAssembler->lineSearch();
      if(ls == 1) break; // processus is converged or failed
      // update ipvariable
      _ipf->compute1state(IPStateBase::current,true);
      // new RightHandSide
      this->computeRightHandSide(pmanager,ufield);
    }
    if((iter == _maxNRite) or isnan(relnorm))
    {
      // reset system value
      staticAssembler->resetUnknownsToPreviousTimeStep();
      iter = _maxNRite; // for isnan case
      break;
    }
  }
  return iter;
}


linearSystem<double>* nonLinearMechSolver::createSNLSystem()
{
  // Select the solver for linear system Ax=b (KDeltau = Fext-Fint)
  linearSystem<double> *lsys; // = new linearSystemGMRESk<double>();
  if(nonLinearMechSolver::whatSolver == Taucs){
   #if defined(HAVE_TAUCS)
    lsys = new nonLinearSystemCSRTaucs<double>(_lineSearch);
    Msg::Info("Taucs is chosen to solve");
   #else
    lsys = new nonLinearSystemGmm<double>(_lineSearch);
    dynamic_cast<nonLinearSystemGmm<double>*>(lsys)->setNoisy(2);
    Msg::Error("Taucs is not installed\n Gmm is chosen to solve");
   #endif
  }
  else if(nonLinearMechSolver::whatSolver == Petsc){
   #if defined(HAVE_PETSC)
   if (_pathFollowing){
    lsys = new pathFollowingSystemPETSC<double>();
    dynamic_cast<pathFollowingSystem<double>*>(lsys)->setControlType(_macroControlType);
   }
   else
    lsys = new nonLinearSystemPETSc<double>(PETSC_COMM_WORLD,_lineSearch);
    Msg::Info("PETSc is chosen to solve");
   #else
    lsys = new nonLinearSystemGmm<double>(_lineSearch);
    dynamic_cast<nonLinearSystemGmm<double>*>(lsys)->setNoisy(2);
    Msg::Error("PETSc is not installed\n Gmm is chosen to solve");
   #endif
  }
  else if(nonLinearMechSolver::whatSolver == GMRESk){
    lsys = new nonLinearSystemGMRESk<double>(_lineSearch);
    Msg::Info("GMRESk is chosen to solve");
  }
  else{
    lsys = new nonLinearSystemGmm<double>(_lineSearch);
    static_cast<nonLinearSystemGmm<double>*>(lsys)->setNoisy(2);
    Msg::Info("Gmm is chosen to solve");
  }
  return lsys;
}

linearSystem<double>* nonLinearMechSolver::createExplicitSystem()
{
  linearSystem<double> *esys;
  if(_previousScheme != Explicit){
    if(nonLinearMechSolver::whatSolver == Petsc){
     #if defined(HAVE_PETSC)
       Msg::Info("Explicit scheme based on PETSc");
       esys = new explicitHulbertChungPetsc<double>(_alpham,_beta,_gamma,_timeStepByBenson,_dynamicRelaxation);
     #else
       Msg::Warning("petsc is not install on this computer. Use blas explicit system");
      #if !defined(HAVE_BLAS)
       Msg::Warning("blas is not installed. Use not optimized default implementation");
      #endif // HAVE_BLAS
       esys = new explicitHulbertChungBlas<double>(_alpham,_beta,_gamma,_timeStepByBenson,_dynamicRelaxation);
     #endif // HAVE_PETSC
    }
    else{
       #if !defined(HAVE_BLAS)
        Msg::Warning("blas is not installed. Use not optimized default implementation. Maybe you can use PETSc ?");
       #else
        Msg::Info("Explicit scheme based on BLAS");
       #endif // HAVE_BLAS
        esys = new explicitHulbertChungBlas<double>(_alpham,_beta,_gamma,_timeStepByBenson,_dynamicRelaxation);
    }
  }
   else{
     std::string sysname("A");
     esys = pAssembler->getLinearSystem(sysname);
     explicitHulbertChung<double>* expsys = static_cast<explicitHulbertChung<double>*>(esys);
     expsys->setDynamicRelaxation(_dynamicRelaxation);
   }
   return esys;
}

linearSystem<double>* nonLinearMechSolver::createImplicitSystem(){
  linearSystem<double> *esys;
  if(nonLinearMechSolver::whatSolver == Petsc){
   #if defined(HAVE_PETSC)
     Msg::Info("Implicit scheme based on PETSc");
     esys = new implicitHulbertChungPetsc<double>(_alpham,_alphaf,_beta,_gamma);
   #else
     Msg::Warning("petsc is not install on this computer. Use blas explicit system");
   #endif // HAVE_PETSC
  }
  else{
    Msg::Fatal("Petsc is not install");
  }

   return esys;

}

void nonLinearMechSolver::createSystem()
{
  std::string Aname("A");
  if(_vcompBySys.size()==0)
  {
    /* System creation */
    linearSystem<double> *lsys;
    if((!_previousInit) or (_previousScheme != whatScheme))
    {
      if(whatScheme==Explicit)
      {
        lsys = this->createExplicitSystem();
        _mpiDofManager = false; // in explicit we need to useGlobalSystemPosition  = false to not use a global system position
      }
      else if (whatScheme == Implicit){
        lsys = this->createImplicitSystem();
      }
      else
        lsys = this->createSNLSystem();
    }
    else
    {
      lsys = pAssembler->getLinearSystem(Aname);
      if(whatScheme==Explicit)
      {
        explicitHulbertChung<double>* esys = static_cast<explicitHulbertChung<double>*>(lsys);
        esys->setDynamicRelaxation(_dynamicRelaxation);
      }
    }

    /* Create a dofManager for the system */
    if(!_previousInit){
      if(pAssembler) delete pAssembler;
        if(whatScheme!=StaticLinear)
          pAssembler = new staticDofManager<double>(lsys,whatScheme,_mpiDofManager);
        else
          pAssembler = new dofManager<double>(lsys);
    }
    else{
      if(_previousScheme != whatScheme)
      {
        staticDofManager<double>* oldass = static_cast<staticDofManager<double>*>(pAssembler);
        pAssembler = new staticDofManager<double>(lsys,oldass,whatScheme);
        delete oldass->getLinearSystem(Aname);
        delete oldass;
      }
    }
  }
  else // Switch allowed only from a dofManagerMultiSystem
  {
    if(!_previousInit){
      pAssembler = new dofManagerMultiSystems<double>(StaticNonLinear);
      dofManagerMultiSystems<double>* allManager = static_cast<dofManagerMultiSystems<double>*>(pAssembler);
      for(int i=0;i<_vcompBySys.size();i++){
        linearSystem<double> *lsys;
        bool globalPos = true;
        if(_vschemeBySys[i] == Explicit)
        {
          lsys = this->createExplicitSystem();
          globalPos = false;
        }
        else
          lsys = this->createSNLSystem();
        dofManager<double>* currentAssembler = new staticDofManager<double>(lsys,_vschemeBySys[i],globalPos);
        allManager->add(currentAssembler,_vcompBySys[i]);
       }
    }
    else{
      dofManagerMultiSystems<double> *oldass = dynamic_cast<dofManagerMultiSystems<double>*>(pAssembler);
      if(oldass!=NULL) // init from another multi system
      {
        for(int i=0;i<_vschemeBySys.size();i++)
        {
          // get the previous scheme
          staticDofManager<double> *oldmsys = dynamic_cast<staticDofManager<double>*>(oldass->getManager(i));
          scheme oldscheme = StaticLinear;
          if(oldmsys!=NULL)
          {
            oldscheme = oldmsys->getScheme();
          }

          // change the dofManger only if the scheme is different from the previous one
          if(_vschemeBySys[i]!=oldscheme)
          {
            // create a new system
            linearSystem<double> *lsys;
            if(_vschemeBySys[i]==Explicit)
            {
              lsys = this->createExplicitSystem();
            }
            else
            {
              lsys = this->createSNLSystem();
            }
            // create new manager
            dofManager<double> *newmanager = new staticDofManager<double>(lsys,oldmsys,_vschemeBySys[i]);
            oldass->reset(newmanager,i);
            // remove the old manager
            delete oldmsys->getLinearSystem(Aname);
            delete oldmsys;
          }
        }
      }
      else // init from a simple system
      {
        Msg::Info("A multi-system is initialized from a previous single system");
        // create the new systems
        std::vector<linearSystem<double>*> vsys(_vschemeBySys.size(),NULL);
        for(int i=0;i<_vschemeBySys.size();i++)
        {
          _vschemeBySys[i]==Explicit ? vsys[i] = this->createExplicitSystem() : vsys[i] = this->createSNLSystem();
        }
        // new dofManager
        staticDofManager<double> *oldstat = dynamic_cast<staticDofManager<double>*>(pAssembler);
        pAssembler = new dofManagerMultiSystems<double>(StaticNonLinear,oldstat,vsys,_vcompBySys,_vschemeBySys);
        // remove the old system and dofManager
        delete oldstat->getLinearSystem(Aname);
        delete oldstat;
        Msg::Info("Multi-system is ready");
      }
    }
  }
}

int nonLinearMechSolver::NewtonRaphsonPathFollowing(dofManager<double> *pmanager, unknownField *ufield){
   std::string name = "A";
  linearSystem<double>* lsys =pmanager->getLinearSystem(name);
  nonLinearSystem<double>* nonsys = dynamic_cast<nonLinearSystem<double>*>(lsys);
  staticDofManager<double>* staticAssembler = static_cast<staticDofManager<double>*>(pmanager);

  // copy state from previous
  _ipf->compute1state(IPStateBase::current,true);

  double norm0 = 0.;

  // loop until convergence
  int iter=0;
  double relnorm = 1;
  Msg::Info("iteration n %d : residu : %e",iter,relnorm);
  while (relnorm>_tol){ // ensure 1 iteration min

    iter++;
    // Compute Stiffness Matrix
    this->computeStiffMatrix(pmanager);

    // Solve KDu = Fext-Fint
    int succeed = staticAssembler->systemSolveIntReturn();
    if(!succeed)
    {
      staticAssembler->resetUnknownsToPreviousTimeStep();
      iter = _maxNRite;
      break;
    }
    // update ipvariable
    _ipf->compute1state(IPStateBase::current,true);

    if (!_iterativeNR) break;
    // new forces
    double normFinf = this->computeRightHandSide(pmanager,ufield);

    if (norm0 == 0.) norm0 = staticAssembler->norm0Inf();
    // Check of convergence
    relnorm = normFinf/norm0;
    Msg::Info("iteration n %d : residu : %e",iter,relnorm);

    if((iter == _maxNRite) or isnan(relnorm))
    {
      // reset system value
      staticAssembler->resetUnknownsToPreviousTimeStep();
      iter = _maxNRite; // for isnan case
      break;
    }
  }
  return iter;
};

void nonLinearMechSolver::perturbationProcedure(const double pre, double* mode){
   /*
    Perturbation is case of simple buckling mode
   */
  if (pre>this->_instabilitycriterion) return;
  Msg::Warning("buckling occurs, criterion = %f < %f",pre,_instabilitycriterion);
  int sizeR = pAssembler->sizeOfR();
  /* Get infinity norm of mode to give the unity vector*/
  double norm = 0;
  for (int i=0; i<sizeR; i++){
   if (fabs(mode[i])> norm) norm = fabs(mode[i]);
  }

  std::string name = "A";
  linearSystem<double>* lsys = pAssembler->getLinearSystem(name);
  double normSol = lsys->normInfSolution();

  Msg::Warning("Perturbating to system: ");
  double factor = (_perturbationfactor*normSol)/norm;
  /* Perturbation to solution*/
  for (int i=0; i<sizeR; i++){
   double val = mode[i]*factor;
   lsys->addToSolution(i,val);
  }
}

double nonLinearMechSolver::solveSNL()
{
  Msg::Info("SNL Data : nstep =%d endtime=%f",_numstepImpl,this->getEndTime());
  /* init data */
  this->init();
  unknownField *ufield=NULL;
  energeticField* efield=NULL;
  this->init2(ufield,efield);
  // iterative scheme (loop on timestep)
  if(_microFlag){
    _ipf->copy(IPStateBase::initial,IPStateBase::current);
    _homogenizedFiles.openFiles(_enum,_gnum,allMicroBC[0]->getOrder(),
                      _isHommStrainSaveToFile, _isHommProSaveToFile);
    _currentState = & _state1;
    _initialState = &_state2;
  }
  staticDofManager<double> *sAssembler = static_cast<staticDofManager<double>*>(pAssembler);

  if (whatScheme == Implicit){
    /* mass matrix computation outside the loop */
    for(std::vector<partDomain*>::iterator it = domainVector.begin(); it!=domainVector.end(); ++it){
      partDomain *dom = *it;
      AssembleMass(dom->getBilinearMassTerm(),*(dom->getFunctionSpace()),dom->g->begin(),dom->g->end(),
                 *(dom->getBulkGaussIntegrationRule()),*pAssembler);
    }
    if(_unresetedContact){
      for(nonLinearMechSolver::contactContainer::iterator itc = _allContact.begin(); itc!= _allContact.end(); ++itc){
        contactDomain *cdom = *itc;
        if(cdom->isRigid()){
          rigidContactSpaceBase *rcsp = static_cast<rigidContactSpaceBase*>(cdom->getSpace());
          AssembleMass(cdom->getMassTerm(),rcsp,pAssembler);
        }
      }
    }
  }


  std::string name = "A";
  linearSystem<double>* lsys =pAssembler->getLinearSystem(name);
  nonLinearSystem<double>* nonsys = dynamic_cast<nonLinearSystem<double>*>(lsys);
  pathFollowingSystem<double>* pathSys = dynamic_cast<pathFollowingSystem<double>*>(lsys);

  if (_pathFollowing){
    this->computeLoadVector(ufield);
  }

  /* time initialization */
  // save solution of previous (initially last step == initial step BEAWARE FOR RESTART)
  sAssembler->nextStep();
  // change to introduce a dynamic modification of time step in case of convergence problem
  double curtime = 0.;
  double lasttime = 0.; // to avoid precision problem curtime = lasttime in place of curtime -= dt if no convergence
  double dendtime = double(endtime);
  double dtmax = dendtime/double(_numstepImpl); // time step value prescribed by user (time step can not be greater than this value)
  double dt = dtmax; // initial time step value
  int ii = 0; // iteration counter
  double tolend = 0.000001*dendtime; // OTHERWISE LAST STEP CAN BE DONE TWICE ??
  double toldtmax = 0.000001*dtmax; // IDEM (PRECISION PROBLEM)
  int numberTimeStepReduction=0;
  /* time loop */
  sAssembler->setTimeStep(dt);
  while(fabs(curtime-dendtime) > tolend ){
    curtime+=dt;
    if(curtime > dendtime)
    {
      dt = dt -(curtime - dendtime);
      curtime = dendtime;
    }
    Msg::Info("t= %e on %e",curtime,dendtime);

    int niteNR = 0;
    if (_pathFollowing){
      pathSys->setPseudoTimeIncrement(dt);
      niteNR = NewtonRaphsonPathFollowing(pAssembler,ufield);
    }
    else{
      this->oneStepPreSolve(curtime,dt);
      // Solve one step by NR scheme
      niteNR = NewtonRaphson(pAssembler,ufield);
    }

    if(niteNR == _maxNRite) // time step reduction
    {
       numberTimeStepReduction ++;
       if(numberTimeStepReduction > _maxAttemptStepReduction) // end of simulation
       {
         Msg::Error("Simulation end due to convergence problem! Values are set to the last converged time step in case you want to switch to another scheme");
         break;
       }
       Msg::Warning("Convergence of Newton-Raphson failed --> reduced time step");
       curtime = lasttime;
       dt /= _timeStepFactorReduction;
       sAssembler->setTimeStep(dt);
       if (_eigflag and _perturbate){
        double* mode = NULL;
        double pre = critialPointTestFunction(curtime,mode);
        if (pre< _instabilitycriterion){
          this->perturbationProcedure(pre, mode);
        }
        if (mode) delete mode;
       }

    }
    else
    {
      if(fabs(dtmax-dt) > toldtmax ) // increase time step if convergence in a few iteration
      {
        if(niteNR <= _niteTimeStepIncrease)
        {
           Msg::Warning("Time step is increased due to convergence of Newton-Raphson in less than %d",_niteTimeStepIncrease);
           numberTimeStepReduction --;
           dt *= _timeStepFactorReduction;
           sAssembler->setTimeStep(dt);
        }
      }
      lasttime = curtime;
      this->oneStepPostSolve(ufield,efield,curtime,ii+1);
      if (_strainMap && _strainMappingFollwing){
        Msg::Info("Create displacement and strain mapping");
        _strainMap->solve(pAssembler);
        std::string filename = "dispMap"+int2str(ii+1)+".msh";
        _strainMap->buildDisplacementView(filename);
        filename = "strainMap"+int2str(ii+1)+".msh";
        _strainMap->buildStrainView(filename);
      }

      if (_eigenFollowing){
        double* mode = NULL;
        double f = this->critialPointTestFunction(curtime,mode);
        if (mode) delete mode;
        Msg::Error("minimal eigen value = %e",f);
      }
    }
    ii ++; // increase counter of iteration even if no convergence --> more archivage if no convergence (can be usefull)
  }

  /* end of scheme */
  this->endOfScheme(ufield,efield,curtime,ii);
  Msg::Info("NonLinearStatic OK");
  return curtime;
}

double nonLinearMechSolver::solveMulti()
{
  Msg::Info("Mixed static and dynamic solving: endtime=%e",this->getEndTime());
  Msg::Info("Static Data: tolerance: %lf",_tol);
  Msg::Info("Dynamic Data: beta=%f gamma=%f alpha_m=%f",this->_beta, this->_gamma, this->_alpham);
  /* init data */
  this->init();
  unknownField *ufield=NULL;
  energeticField* efield=NULL;
  // set the first time step to 1 if previous computation to avoid initialization problem
  int step = 0; // iteration counter
  if(_previousInit) step = 1;
  this->init2(ufield,efield);
  // iterative scheme (loop on timestep)
  if(_microFlag){
    _ipf->copy(IPStateBase::initial,IPStateBase::current);
    _homogenizedFiles.openFiles(_enum,_gnum,allMicroBC[0]->getOrder(),
                      _isHommStrainSaveToFile, _isHommProSaveToFile);
  }
  staticDofManager<double> *sAssembler = static_cast<staticDofManager<double>*>(pAssembler);
  dofManagerMultiSystems<double> *multiAssembler = static_cast<dofManagerMultiSystems<double>*>(pAssembler);

  /* mass matrix computation outside the loop */
  for(std::vector<partDomain*>::iterator it = domainVector.begin(); it!=domainVector.end(); ++it){
    partDomain *dom = *it;
    AssembleMass(dom->getBilinearMassTerm(),*(dom->getFunctionSpace()),dom->g->begin(),dom->g->end(),
               *(dom->getBulkGaussIntegrationRule()),*pAssembler);
  }
  if(_unresetedContact){
    for(nonLinearMechSolver::contactContainer::iterator itc = _allContact.begin(); itc!= _allContact.end(); ++itc){
      contactDomain *cdom = *itc;
      if(cdom->isRigid()){
        rigidContactSpaceBase *rcsp = static_cast<rigidContactSpaceBase*>(cdom->getSpace());
        AssembleMass(cdom->getMassTerm(),rcsp,pAssembler);
      }
    }
  }


  /* time initialization */
  sAssembler->nextStep();   // save solution of previous (initially last step == initial step BEAWARE FOR RESTART)
  double curtime = 0.;
  double lasttime = 0.; // to avoid precision problem curtime = lasttime in place of curtime -= dt if no convergence
  double dendtime = double(endtime);
  double dtmax; // time step value prescribed by user (time step can not be greater than this value)
  double dt; // initial time step value
  double tolend = 0.000001*dendtime; // OTHERWISE LAST STEP CAN BE DONE TWICE ??
  double toldtmax; // IDEM (PRECISION PROBLEM)
  int numberTimeStepReduction=0;
  // if one system is explicit --> avoid the reduction of time step
  bool oneExplicitScheme=false;
  for(int i=0;i<_vschemeBySys.size();i++)
  {
    if(_vschemeBySys[i]==Explicit)
    {
      _maxAttemptStepReduction = 0;
      oneExplicitScheme = true;
    }
  }
  if(!oneExplicitScheme)
  {
    dt=dendtime/_numstepImpl;
    dtmax = dt;
  }
  else if (step==1) // compute the explicit time step in case of a previousScheme
  {
    dt = _gammas * this->criticalExplicitTimeStep(ufield);
    #if defined(HAVE_MPI)
     if(Msg::GetCommSize()>1){
       // in case of mpi the time step is choosen as the minimal value compute on each partition
       double mpiTimeStep;
       MPI_Allreduce(&dt,&mpiTimeStep,1,MPI_DOUBLE,MPI_MIN,MPI_COMM_WORLD);
       dt = mpiTimeStep;
      }
    #endif // HAVE_MPI
    Msg::Info("time step value: %e at time %e",dt,curtime);
    sAssembler->setTimeStep(dt);
    dtmax = dt;
    toldtmax = 1e-6*dtmax;
  }

  /* time loop */
  while(fabs(curtime-dendtime) > tolend ){
    // compute the time step (explicit limitation)
    if(step%_numstepExpl ==0 and oneExplicitScheme){
      dt = _gammas * this->criticalExplicitTimeStep(ufield);
      #if defined(HAVE_MPI)
       if(Msg::GetCommSize()>1){
          // in case of mpi the time step is choosen as the minimal value compute on each partition
          double mpiTimeStep;
          MPI_Allreduce(&dt,&mpiTimeStep,1,MPI_DOUBLE,MPI_MIN,MPI_COMM_WORLD);
          dt = mpiTimeStep;
       }
      #endif // HAVE_MPI
      Msg::Info("time step value: %e at time %e",dt,curtime);
      sAssembler->setTimeStep(dt);
      dtmax = dt;
      toldtmax = 1e-6*dtmax;
    }
    else if(!oneExplicitScheme)
      Msg::Info("time step value: %e at time %e",dt,curtime);
    curtime+=dt;
    if(curtime > dendtime){curtime = dendtime;}

    this->oneStepPreSolve(curtime,dt);
    // Solve one step by NR scheme
    int niteNR=0;
    if(step%_numstepImpl==0 or !oneExplicitScheme)
    {
      Msg::Info("Solve implicit system(s)");
      niteNR = NewtonRaphson(pAssembler,ufield);
      if(niteNR == _maxNRite) // time step reduction
      {
        numberTimeStepReduction ++;
        if(numberTimeStepReduction > _maxAttemptStepReduction) // end of simulation
        {
          Msg::Error("Simulation end due to convergence problem! Values are set to the last converged time step in case you want to switch to another scheme");
          // reset value to the previous converged time step and break (to switch from implicit to explicit in the pyfile for exemple)
          break;
        }
        Msg::Warning("Convergence of Newton-Raphson failed --> reduced time step");
        curtime = lasttime;
        dt /= _timeStepFactorReduction;
      }
      else if(niteNR!=0)
      {
        if(fabs(dtmax-dt) > toldtmax ) // increase time step if convergence in a few iteration
        {
          if(niteNR <= _niteTimeStepIncrease)
          {
            Msg::Warning("Time step is increased due to convergence of Newton-Raphson in less than %d",_niteTimeStepIncrease);
            numberTimeStepReduction --;
            dt *= _timeStepFactorReduction;
          }
        }
        lasttime = curtime;
        this->oneStepPostSolve(ufield,efield,curtime,step+1);
      }
    }
    if(niteNR==0) // no Implicit computation or no force on the implicit systems
    {
      // solve only the Explicit scheme
      int globalsuc = 0;
      for(int i=0;i<_vschemeBySys.size();i++)
      {
        staticDofManager<double> *nlass = static_cast<staticDofManager<double>*>(multiAssembler->getManager(i));
        if(nlass->getScheme()==Explicit)
          globalsuc = nlass->systemSolveIntReturn();
	  }
      if(globalsuc){ // otherwise no resolution (QS scheme only with no forces) !!
        // compute the new forces
        _ipf->compute1state(IPStateBase::current,false);
        double normRHS = this->computeRightHandSide(pAssembler,ufield);
        if(isnan(normRHS))
        {
          Msg::Fatal("Nan force value --> end");
          break;
        }
        if(_dynamicRelaxation){
          double norm0 = sAssembler->norm0Inf();
          if(step%_numstepExpl ==0 ) Msg::Info("Rel norm for dynamic relaxation %e tol = %e",normRHS/norm0,_tol);
          if(normRHS/norm0 < _tol){
            Msg::Info("Dynamic relaxation procedure is achieved");
            break;
          }
          if(curtime == endtime)
            Msg::Info("Dynamic relaxation ended as the final time is reached. But convergence is not achieved!");
        }
      }
      lasttime = curtime;
      this->oneStepPostSolve(ufield,efield,curtime,step+1);
    }
    step ++; // increase counter of iteration even if no convergence --> more archivage if no convergence (can be usefull)
  }
  /* end of scheme */
  this->endOfScheme(ufield,efield,curtime,step);
  Msg::Info("Multi OK");
  return curtime;

}
void nonLinearMechSolver::unknownBuildView(const int comp,const int nbstep)
{
  int nbstep_;
  nbstep==1 ? nbstep_ = nsba : nbstep_=nbstep;
  switch(comp){
   case 1:
    unknownView.push_back(nlsField::dataBuildView("velocity",1,nlsField::crude,nbstep_,false));
    break;
   case 2:
    unknownView.push_back(nlsField::dataBuildView("acceleration",2,nlsField::crude,nbstep_,false));
    break;
   case 3:
    unknownView.push_back(nlsField::dataBuildView("displacement",0,nlsField::periodic,nbstep_,false));
    break;
   default:
    unknownView.push_back(nlsField::dataBuildView("displacement",0,nlsField::crude,nbstep_,false));
  }
}
void nonLinearMechSolver::internalPointBuildView(const std::string vname,const int comp,
                                                 const int nbstep,const int ev)
{
  nlsField::ElemValue ev_;
  switch(ev){
   case 0:
    ev_ = nlsField::crude;
    break;
   case 1:
    ev_ = nlsField::mean;
    break;
   case 2 :
    ev_ = nlsField::min;
    break;
   case 3 :
    ev_ = nlsField::max;
    break;
   default:
    ev_ = nlsField::mean;
  }
  int nbstep_;
  nbstep==1 ? nbstep_ = nsba : nbstep_=nbstep;
  ipView.push_back(nlsField::dataBuildView(vname,comp,ev_,nbstep_,false));
}
void nonLinearMechSolver::energyBuildView(const std::string vname,const int comp,const int nbstep)
{
  int nbstep_;
  nbstep==1 ? nbstep_ = nsba : nbstep_=nbstep;
  switch(comp){
   case 1:
    energyView.push_back(nlsField::dataBuildView("kinetic",1,nlsField::crude,nbstep_,false));
    break;
   case 2:
    energyView.push_back(nlsField::dataBuildView("deformation",2,nlsField::crude,nbstep_,false));
    break;
   case 3:
    energyView.push_back(nlsField::dataBuildView("external work",3,nlsField::crude,nbstep_,false));
    break;
   default:
    energyView.push_back(nlsField::dataBuildView("total",4,nlsField::crude,nbstep_,false));
  }
}

void nonLinearMechSolver::energyComputation(const int val)
{
  _energyComputation=val;
}

void nonLinearMechSolver::fractureEnergy(const int val)
{
  _fractureEnergyComputation=val;
}

void nonLinearMechSolver::fillConstraintDof()
{
  allConstraintDof.clear();
  for(int i=0;i<allConstraint.size();i++)
  {
    std::vector<MVertex*> vv;
    int dim=0;
    switch(allConstraint[i].onWhat){
     case nonLinearBoundaryCondition::ON_VERTEX:
      dim = 0;
      break;
     case nonLinearBoundaryCondition::ON_EDGE:
      dim = 1;
      break;
     case nonLinearBoundaryCondition::ON_FACE:
      dim = 2;
      break;
     case nonLinearBoundaryCondition::ON_VOLUME:
      dim = 3;
      break;
    }
    pModel->getMeshVerticesForPhysicalGroup(dim,allConstraint[i]._tag,vv);
    std::list< Dof > vdof;// all dof include in the edge

    // loop on domain (to find the domain where the force is applied)
    for(std::vector<partDomain*>::iterator itdom=domainVector.begin(); itdom!=domainVector.end(); ++itdom){
      partDomain *dom = *itdom;
      FunctionSpaceBase *sp = dom->getFunctionSpace();
      std::vector<Dof> R;
      if(!(dom->getFormulation())){  // Cg/Dg
        FilterDof* filter = dom->createFilterDof(allConstraint[i]._comp);
        for(groupOfElements::vertexContainer::iterator itv=dom->g->vbegin(); itv!=dom->g->vend(); ++itv){
          MVertex *ver = *itv;
          for(std::vector<MVertex*>::iterator it=vv.begin(); it!=vv.end(); ++it){
            MVertex *vver = *it;
            if(ver == vver){
              MPoint ele = MPoint(ver,-1); // We don't care about element number it's just to have the vertex keys
              // build the dof
              sp->getKeys(&ele,R);
              for(int j=0;j<R.size(); j++)
                if(filter->operator()(R[j]))
                  vdof.push_back(R[j]);
              R.clear();
              break;
            }
          }
        }
        delete filter;
      }
      else{ // full Dg
        for(groupOfElements::elementContainer::iterator ite = dom->g->begin(); ite!=dom->g->end(); ++ite){
          MElement *ele = *ite;
          int nbvertex = ele->getNumVertices();
          sp->getKeys(ele,R);
          for(std::vector<MVertex*>::iterator it=vv.begin(); it!=vv.end(); ++it){
            for(int j=0;j<nbvertex; j++){
              if(ele->getVertex(j) == *it){
                vdof.push_back(R[j+nbvertex*allConstraint[i]._comp]);
                break;
              }
            }
          }
          R.clear();
        }
      }
    }
    vdof.unique();
    allConstraintDof.push_back(vdof);
  }
};

void nonLinearMechSolver::applyConstraintBC(dofManager<double>* p)
{
  for(int i=0;i<allConstraint.size();i++)
  {
    int nvdof = allConstraintDof[i].size();
    for (std::list<Dof>::iterator it =allConstraintDof[i].begin(); it != allConstraintDof[i].end(); it++)
    {
      if (it!=allConstraintDof[i].begin())
      {
        DofAffineConstraint<double> cons;
        cons.linear.push_back(std::pair<Dof,double>(*(allConstraintDof[i].begin()),1));
        cons.shift=0;
        p->setLinearConstraint(*it,cons);
        cons.linear.clear();
      }
    };
  }
};


void nonLinearMechSolver::applyPBCBetweenTwoGroups(dofManager<double>* p){
  std::vector<groupTwoElements> allTwoElementGroup;
  for (int i=0; i<allPeriodic.size(); i++){
    int comp = allPeriodic[i].comp;
    SVector3& direction = allPeriodic[i].direction;
    std::set<MElement*> rightGroup;
    for (groupOfElements::elementContainer::iterator it = allPeriodic[i].g2->begin();
                                                      it != allPeriodic[i].g2->end(); it++){
      rightGroup.insert(*it);
    }
    for (std::set<MElement*>::iterator itMaster = allPeriodic[i].g1->begin();
                                          itMaster!= allPeriodic[i].g1->end(); itMaster++){
      MElement* master = *itMaster;
      std::set<MElement*>::iterator slaveBegin = rightGroup.begin();
      MElement* slave = *(slaveBegin);
      slaveBegin++;
      SPoint3 masterBaryCenter = master->barycenter();
      for (int ii=0; ii<3; ii++)
        masterBaryCenter[ii] += direction[ii];
      double length = masterBaryCenter.distance(slave->barycenter());
      for (std::set<MElement*>::iterator itSlave = slaveBegin; itSlave != rightGroup.end(); itSlave++){
        MElement* ele = *itSlave;
        double lengthtemp = masterBaryCenter.distance(ele->barycenter());
        if (lengthtemp < length){
          slave = ele;
          length = lengthtemp;
        };
      }
      groupTwoElements grp;
      grp.eLeft = master;
      grp.eRight = slave;
      grp.comp = comp;
      //Msg::Error("Procs %d, Periodic master %d, slave %d",Msg::GetCommRank(),master->getNum(),slave->getNum());
      allTwoElementGroup.push_back(grp);
      rightGroup.erase(slave);
    }
  }

  for (int i=0; i< allTwoElementGroup.size(); i++){
    MElement* master = allTwoElementGroup[i].eLeft;
    MElement* slave = allTwoElementGroup[i].eRight;
    int comp = allTwoElementGroup[i].comp;
    // found elements on domain related with master element
    MElement* eMaster = NULL;
    partDomain* masterDom = NULL;
    bool foundMaster = false;

    int numPriVert = master->getNumPrimaryVertices();
    std::vector<MVertex*> vvMaster;
    for (int ii=0; ii< numPriVert; ii++)
      vvMaster.push_back(master->getVertex(ii));

    for (int idom=0; idom<domainVector.size(); idom ++){
      if (foundMaster) break;
      partDomain* dom = domainVector[idom];
      groupOfElements* g = dom->g;
      for (groupOfElements::elementContainer::iterator ite = g->begin(); ite != g->end(); ite++){
        MElement* ele = *ite;
        if (ele->getDim() == master->getDim()){ // periodic between to domain
          if (ele->getNum() == master->getNum()){
            eMaster = ele;
            masterDom = dom;
            foundMaster  = true;
            break;
          }
        }
        else if (master->getDim() == 2 and ele->getDim() == 3){
          if (foundMaster) break;
          int numFace = ele->getNumFaces();
          for (int kk=0; kk<numFace; kk++){
            std::vector<MVertex*> faceVer;
            ele->getFaceVertices(kk,faceVer);
            if (master->getType() == TYPE_TRI){
              if ((vvMaster[0] == faceVer[0] and vvMaster[1] == faceVer[1] and vvMaster[2] == faceVer[2]) or
                  (vvMaster[0] == faceVer[0] and vvMaster[1] == faceVer[2] and vvMaster[2] == faceVer[1]) or
                  (vvMaster[0] == faceVer[1] and vvMaster[1] == faceVer[0] and vvMaster[2] == faceVer[2]) or
                  (vvMaster[0] == faceVer[1] and vvMaster[1] == faceVer[1] and vvMaster[2] == faceVer[0]) or
                  (vvMaster[0] == faceVer[2] and vvMaster[1] == faceVer[1] and vvMaster[2] == faceVer[0]) or
                  (vvMaster[0] == faceVer[2] and vvMaster[1] == faceVer[0] and vvMaster[2] == faceVer[1])){
                eMaster = ele;
                masterDom = dom;
                foundMaster  = true;
                break;
              }
            }
            else if (master->getType() == TYPE_QUA){
              if ((vvMaster[0] == faceVer[0] and vvMaster[1] == faceVer[1] and vvMaster[2] == faceVer[2]) or
                  (vvMaster[0] == faceVer[1] and vvMaster[1] == faceVer[2] and vvMaster[2] == faceVer[3]) or
                  (vvMaster[0] == faceVer[2] and vvMaster[1] == faceVer[3] and vvMaster[2] == faceVer[0]) or
                  (vvMaster[0] == faceVer[3] and vvMaster[1] == faceVer[0] and vvMaster[2] == faceVer[1]) or
                  (vvMaster[0] == faceVer[0] and vvMaster[1] == faceVer[3] and vvMaster[2] == faceVer[2]) or
                  (vvMaster[0] == faceVer[3] and vvMaster[1] == faceVer[2] and vvMaster[2] == faceVer[1]) or
                  (vvMaster[0] == faceVer[2] and vvMaster[1] == faceVer[1] and vvMaster[2] == faceVer[0]) or
                  (vvMaster[0] == faceVer[1] and vvMaster[1] == faceVer[0] and vvMaster[2] == faceVer[3])){
                eMaster = ele;
                masterDom = dom;
                foundMaster  = true;
                break;
              }
            }
          }
        }
        else if (master->getDim() == 1 and ele->getDim() == 2){
          if (foundMaster) break;
          int numEdge = ele->getNumEdges();
          for (int kk=0; kk< numEdge; kk++){
            std::vector<MVertex*> edgeVer;
            ele->getEdgeVertices(kk,edgeVer);
            if ((vvMaster[0] == edgeVer[0] and vvMaster[1] == edgeVer[1]) or
                 (vvMaster[0] == edgeVer[1] and vvMaster[1] == edgeVer[0])){
               eMaster = ele;
               masterDom = dom;
               foundMaster  = true;
               break;
            }
          }
        }
      }
    }

    // found elements on domain related with slave element
    MElement* eSlave = NULL;
    partDomain* slaveDom = NULL;
    bool foundSlave = false;

    numPriVert = slave->getNumPrimaryVertices();
    std::vector<MVertex*> vvSlave;
    for (int ii=0; ii< numPriVert; ii++)
      vvSlave.push_back(slave->getVertex(ii));

    for (int idom=0; idom<domainVector.size(); idom ++){
      if (foundSlave) break;
      partDomain* dom = domainVector[idom];
      groupOfElements* g = dom->g;
      for (groupOfElements::elementContainer::iterator ite = g->begin(); ite != g->end(); ite++){
        MElement* ele = *ite;
        if (ele->getDim() == slave->getDim()){ // periodic between to domain
          if (ele->getNum() == slave->getNum()){
            eSlave = ele;
            slaveDom = dom;
            foundSlave = true;
            break;
          }
        }
        else if (slave->getDim() == 2 and ele->getDim() == 3){
          if (foundSlave) break;
          int numFace = ele->getNumFaces();
          for (int kk=0; kk<numFace; kk++){
            std::vector<MVertex*> faceVer;
            ele->getFaceVertices(kk,faceVer);
            if (slave->getType() == TYPE_TRI){
              if ((vvSlave[0] == faceVer[0] and vvSlave[1] == faceVer[1] and vvSlave[2] == faceVer[2]) or
                  (vvSlave[0] == faceVer[0] and vvSlave[1] == faceVer[2] and vvSlave[2] == faceVer[1]) or
                  (vvSlave[0] == faceVer[1] and vvSlave[1] == faceVer[0] and vvSlave[2] == faceVer[2]) or
                  (vvSlave[0] == faceVer[1] and vvSlave[1] == faceVer[1] and vvSlave[2] == faceVer[0]) or
                  (vvSlave[0] == faceVer[2] and vvSlave[1] == faceVer[1] and vvSlave[2] == faceVer[0]) or
                  (vvSlave[0] == faceVer[2] and vvSlave[1] == faceVer[0] and vvSlave[2] == faceVer[1])){
                eSlave = ele;
                slaveDom = dom;
                foundSlave = true;
                break;
              }
            }
            else if (slave->getType() == TYPE_QUA){
              if ((vvSlave[0] == faceVer[0] and vvSlave[1] == faceVer[1] and vvSlave[2] == faceVer[2]) or
                  (vvSlave[0] == faceVer[1] and vvSlave[1] == faceVer[2] and vvSlave[2] == faceVer[3]) or
                  (vvSlave[0] == faceVer[2] and vvSlave[1] == faceVer[3] and vvSlave[2] == faceVer[0]) or
                  (vvSlave[0] == faceVer[3] and vvSlave[1] == faceVer[0] and vvSlave[2] == faceVer[1]) or
                  (vvSlave[0] == faceVer[0] and vvSlave[1] == faceVer[3] and vvSlave[2] == faceVer[2]) or
                  (vvSlave[0] == faceVer[3] and vvSlave[1] == faceVer[2] and vvSlave[2] == faceVer[1]) or
                  (vvSlave[0] == faceVer[2] and vvSlave[1] == faceVer[1] and vvSlave[2] == faceVer[0]) or
                  (vvSlave[0] == faceVer[1] and vvSlave[1] == faceVer[0] and vvSlave[2] == faceVer[3])){
                eSlave = ele;
                slaveDom = dom;
                foundSlave  = true;
                break;
              }
            }
          }
        }
        else if (slave->getDim() == 1 and ele->getDim() == 2){
          if (foundSlave) break;
          int numEdge = ele->getNumEdges();
          for (int kk=0; kk< numEdge; kk++){
            std::vector<MVertex*> edgeVer;
            ele->getEdgeVertices(kk,edgeVer);
            if ((vvSlave[0] == edgeVer[0] and vvSlave[1] == edgeVer[1]) or
                 (vvSlave[0] == edgeVer[1] and vvSlave[1] == edgeVer[0])){
               eSlave = ele;
               slaveDom = dom;
               foundSlave = true;
               break;
            }
          }
        }
      }
    }

    if(foundMaster and foundSlave){
      SPoint3 baryMaster = master->barycenter();
      SPoint3 barySlave = slave->barycenter();
      SVector3 direction(baryMaster,barySlave);

      std::map<MVertex*,MVertex*> verMap;

      int numverMaster = master->getNumVertices();
      int numverSlave = slave->getNumVertices();

      for (int iv = 0; iv< numverMaster; iv++){
        MVertex* vp = master->getVertex(iv);
        SPoint3 pp = vp->point();
        for (int ipp =0; ipp<3; ipp++)
          pp[ipp] += direction[ipp];
        MVertex* vn = slave->getVertex(0);
        double length = pp.distance(vn->point());
        for (int in = 1; in< numverSlave; in++){
          MVertex* v = slave->getVertex(in);
          double templeng = pp.distance(v->point());
          if (templeng< length){
            vn = v;
            length = templeng;
          }
        }
        verMap.insert(std::pair<MVertex*,MVertex*>(vp,vn));
      }

      std::vector<Dof> masterDof, slaveDof;
      masterDom->getFunctionSpace()->getKeys(eMaster,masterDof);
      slaveDom->getFunctionSpace()->getKeys(eSlave,slaveDof);


      std::vector<Dof> Rleft, Rright;
      int numDomMaster = eMaster->getNumVertices();
      int numDomSlave = eSlave->getNumVertices();
      for (std::map<MVertex*,MVertex*>::iterator it =verMap.begin(); it!=verMap.end(); it++){
        MVertex* vleft = it->first;
        MVertex* vright = it->second;
        for (int ileft =0; ileft < numDomMaster; ileft++){
          if (eMaster->getVertex(ileft) == vleft){
            if (comp<0 or comp >2){
              Rleft.push_back(masterDof[ileft+numDomMaster*0]);
              Rleft.push_back(masterDof[ileft+numDomMaster*1]);
              Rleft.push_back(masterDof[ileft+numDomMaster*2]);
            }
            else
              Rleft.push_back(masterDof[ileft+numDomMaster*comp]);
            break;
          }
        }

        for (int ileft =0; ileft < numDomSlave; ileft++){
          if (eSlave->getVertex(ileft) == vright){
            if (comp<0 or comp >2){
              Rright.push_back(slaveDof[ileft+numDomSlave*0]);
              Rright.push_back(slaveDof[ileft+numDomSlave*1]);
              Rright.push_back(slaveDof[ileft+numDomSlave*2]);
            }
            else
              Rright.push_back(slaveDof[ileft+numDomSlave*comp]);
            break;
          }
        }
         // apply constraint
        //Msg::Info("Process =%d, Periodic BC beetween vertex %d and %d comp %d ",Msg::GetCommRank(),vleft->getNum(),vright->getNum(), comp);
        for(int k=0; k<Rleft.size(); k++){
          DofAffineConstraint<double> cons;
          cons.linear.push_back(std::pair<Dof,double>(Rright[k],1.));
          cons.shift=0;
          p->setLinearConstraint(Rleft[k],cons);
          cons.linear.clear();
        }
        Rleft.clear();
        Rright.clear();
      }
    }
  }
};

void nonLinearMechSolver::crackTracking(std::string fname)
{
 #if defined(HAVE_MPI)
  if(Msg::GetCommSize() > 1){
    std::ostringstream oss;
    oss << Msg::GetCommRank();
    fname += oss.str();
  }
 #endif // HAVE_MPI
  _crackTrackingFile = fopen(fname.c_str(),"w");
  fprintf(_crackTrackingFile,"time0,time1;inter num;num minus;num plus;x0;y0;z0;x1;y1;z1\n");
}

// In MPI only 1 final file on rank 0
void nonLinearMechSolver::postproFragment(std::string fname)
{
 #if defined(HAVE_MPI)
  if(Msg::GetCommSize()>1)
  {
     // change file name (include the part number in it
    std::ostringstream oss;
    oss << Msg::GetCommRank();
    std::string partnum = oss.str();
    // retrieve file extension
    size_t ext_pos;
    ext_pos = fname.find_last_of('.');
    std::string ext(fname,ext_pos+1,fname.size());
    // set file name
    std::string newname(fname,0,ext_pos);
    std::string fileName = newname + "_part" + partnum +"." + ext;
    // open file
    _fragmentationFile = fopen(fileName.c_str(),"w");
  }
  else
 #endif //HAVE_MPI
  {
    _fragmentationFile = fopen(fname.c_str(),"w");
  }
}

void nonLinearMechSolver::crackTracking(const double curtime,unknownField *ufield,IPField *ipf)
{
  if(_crackTrackingFile != NULL) // do nothing otherwise
  {
     // loop on domain
     for(std::vector<partDomain*>::const_iterator itdom = domainVector.begin(); itdom != domainVector.end(); ++itdom)
     {
        partDomain *dom = *itdom;
        if(dom->IsInterfaceTerms())
        {
          dgPartDomain *dgdom = static_cast<dgPartDomain*>(dom);
          if((dgdom->getMaterialLawMinus()->getType() == materialLaw::fracture)
             and (dgdom->getMaterialLawPlus()->getType() == materialLaw::fracture)
             and (dgdom->getDim() == 2))
          {
            // loop on interface element
            QuadratureBase *integBound = dgdom->getInterfaceGaussIntegrationRule();
            IntPt *GP;
            const materialLaw2LawsInitializer *mfminus = dynamic_cast<const materialLaw2LawsInitializer*>(dgdom->getMaterialLawMinus());
            const materialLaw2LawsInitializer *mfplus = dynamic_cast<const materialLaw2LawsInitializer*>(dgdom->getMaterialLawPlus());
            for(groupOfElements::elementContainer::const_iterator itele = dgdom->gi->begin(); itele != dgdom->gi->end(); ++itele)
            {
              MElement *ele = *itele;
              AllIPState::ipstateElementContainer *vips = ipf->getAips()->getIPstate(ele->getNum());
              int npts_inter = integBound->getIntPoints(ele,&GP);
              int jj;
              /* Assume same value on minus and plus element !!*/
              bool prevOther = false; // other previous Gauss point already broken
              for(jj=0;jj<npts_inter;jj++)
              {
                IPStateBase *ipsm = (*vips)[jj];
                IPVariable *ipvmcur = ipsm->getState(IPStateBase::current);
                IPVariable *ipvmprev= ipsm->getState(IPStateBase::previous);
                bool fbmp = mfminus->fullBroken(ipvmprev);
                bool fbmc = mfminus->fullBroken(ipvmcur);
                if((fbmc==false))
                  break;
                else if(fbmp==false){
                  if(prevOther==false)
                    prevOther = true;
                  else
                    break;
                }
              }
              if( (jj==npts_inter) and (prevOther==true))// element is broken when all Gauss points are broken
              {
                /* get the time of crack initialization at extremities !! */
                IPStateBase *ipsm = (*vips)[0];
                IPVariable *ipvmcur = ipsm->getState(IPStateBase::current);
                double time00 = mfminus->timeInitBroken(ipvmcur);
                ipsm = (*vips)[npts_inter-1];
                ipvmcur = ipsm->getState(IPStateBase::current);
                double time01 = mfminus->timeInitBroken(ipvmcur);
                double time0;
                int index1,index0; // 0 or 1
                if(time00 < time01){
                  time0 = time00;
                  index0 = 0;
                  index1 = 1;
                }
                else{
                  time0 = time01;
                  index0 = 1;
                  index1 = 0;
                }
                /* find current position of element */
                FunctionSpaceBase *sp = dgdom->getFunctionSpace();
                std::vector<Dof> R;
                MInterfaceLine *iele = dynamic_cast<MInterfaceLine*>(ele);  // track a 2D crack--> interface == line
                MElement *minusele = iele->getElem(0);
                MElement *plusele = iele->getElem(1);
                int totalnodes = minusele->getNumVertices() + plusele->getNumVertices();
                // number of dimension
                sp->getKeys(iele,R); // can't only get keys of elem minus on interDomain !!
                int dimMinus = R.size()/totalnodes;
                int numdofsMinus = dimMinus*minusele->getNumVertices();
                int numdofsPlus = dimMinus*plusele->getNumVertices();
                // limit R to the dofs of minus elem (can't resize as Dof() is not defined)
                for(int kk=0;kk<numdofsPlus;kk++)
                {
                  R.pop_back();
                }
                fullVector<double> disp(R.size());
                ufield->get(R,disp);
                std::vector<double> elempos;
                currentConfig::elementPositionXYZ(minusele,disp,elempos);
                std::vector<int> vn(2);
                iele->getLocalVertexNum(0,vn);
                int numvertex = minusele->getNumVertices();
                fprintf(_crackTrackingFile,"%e;%e;%d;%d;%d;%e;%e;%e;%e;%e;%e\n",time0,curtime,ele->getNum(),iele->getElem(index0)->getNum(),iele->getElem(index1)->getNum(),elempos[vn[index0]],elempos[vn[index0]+numvertex],elempos[vn[index0]+2*numvertex],elempos[vn[index1]],elempos[vn[index1]+numvertex],elempos[vn[index1]+2*numvertex]);
            }
          }
        }
      }
    }
    fflush(_crackTrackingFile);
  }
}

void nonLinearMechSolver::postproFragment(IPField *ipf)
{
  if(_fragmentationFile!=NULL)
  {
    std::set<interfaceFragment> sfrag; // contains interface element not broken
    std::set<pairMassElem> isolatedfrag; // contains elements of broken interface
    // loop on domain
    for(std::vector<partDomain*>::const_iterator itdom = domainVector.begin(); itdom != domainVector.end(); ++itdom)
    {
      partDomain *dom = *itdom;
      if(dom->IsInterfaceTerms())
      {
        dgPartDomain *dgdom = static_cast<dgPartDomain*>(dom);
        if((dgdom->getMaterialLawMinus()->getType() == materialLaw::fracture)
           and (dgdom->getMaterialLawPlus()->getType() == materialLaw::fracture)) // same for dim 3 normally. But not tested
        {
          // loop on interface element
          QuadratureBase *integBound = dgdom->getInterfaceGaussIntegrationRule();
          IntPt *GP;
          const materialLaw2LawsInitializer *mfminus = dynamic_cast<const materialLaw2LawsInitializer*>(dgdom->getMaterialLawMinus());
          const materialLaw2LawsInitializer *mfplus = dynamic_cast<const materialLaw2LawsInitializer*>(dgdom->getMaterialLawPlus());
          for(groupOfElements::elementContainer::const_iterator itele = dgdom->gi->begin(); itele != dgdom->gi->end(); ++itele)
          {
            MElement *ele = *itele;
            AllIPState::ipstateElementContainer *vips = ipf->getAips()->getIPstate(ele->getNum());
            int npts_inter = integBound->getIntPoints(ele,&GP);
            int jj;
            /* Assume same value on minus and plus element !!*/
            for(jj=0;jj<npts_inter;jj++)
            {
              IPStateBase *ipsm = (*vips)[jj];
              IPVariable *ipvmcur = ipsm->getState(IPStateBase::current);
              if(!(mfminus->fullBroken(ipvmcur)))
                break;
            }
            MInterfaceElement *miel = dynamic_cast<MInterfaceElement*>(ele);
            if(!(jj==npts_inter))// element is not broken
            {
              sfrag.insert(interfaceFragment(miel,dgdom->getMaterialLawMinus()->density(),dgdom->getMaterialLawPlus()->density()));
            }
            else
            {
             #if defined(HAVE_MPI)
              if((miel->getElem(0)->getPartition() == Msg::GetCommRank()+1) or (Msg::GetCommSize()==1)) // insert only the element that is on the partition
             #endif // HAVE_MPI
              {
                isolatedfrag.insert(pairMassElem(miel->getElem(0),dgdom->getMaterialLawMinus()->density()));
              }
             #if defined(HAVE_MPI)
              if((miel->getElem(1)->getPartition() == Msg::GetCommRank()+1) or (Msg::GetCommSize()==1)) // insert only the element that is on the partition
             #endif // HAVE_MPI
              {
                isolatedfrag.insert(pairMassElem(miel->getElem(1),dgdom->getMaterialLawPlus()->density()));
              }
            }
          }
        }
      }
    }
    // loop on interelement
    std::set<fragment> fragmentlist;
    std::set<fragment>::iterator it1=fragmentlist.end();
    std::set<fragment>::iterator it2=fragmentlist.end();
    for(std::set<interfaceFragment>::iterator it=sfrag.begin(); it!=sfrag.end();++it)
    {
       it1=fragmentlist.end();
       it2=fragmentlist.end();
       for(std::set<fragment>::iterator itf=fragmentlist.begin(); itf!=fragmentlist.end(); ++itf)
       {
          if((*itf).match(*it))
          {
            if(it1==fragmentlist.end())
            {
              it1 = itf;
            }
            else
            {
              it2 = itf;
              break; // can only found 2 elements;
            }
          }
       }
       // insert fragment
       if((it1!=fragmentlist.end()) and(it2==fragmentlist.end())) // match with one --> insert interfacefragment to the fragment
       {
          (*it1).add(*it);
       }
       else if(it2!=fragmentlist.end()) // match twice --> regroup both fragment
       {
          (*it1).add(*it2);
          fragmentlist.erase(it2);
       }
       else // no match --> create a new one
       {
          fragmentlist.insert(fragment(*it));
       }
    }
    // Loop on isolated fragment. If not founded the fragment contains only 1 element which has to be inserted in fragmentlist
    for(std::set<pairMassElem>::iterator it=isolatedfrag.begin(); it!=isolatedfrag.end();++it)
    {
      std::set<fragment>::iterator itfound=fragmentlist.end();
      for(std::set<fragment>::iterator itf=fragmentlist.begin(); itf!=fragmentlist.end();++itf)
      {
        if((*itf).match(*it))
        {
          itfound = itf;
          break;
        }
      }
      if(itfound==fragmentlist.end())
      {
        fragmentlist.insert(fragment(*it));
      }
    }

    // at this point fragmentlist contains all fragment of the partition
    // the file can be written except for fragment defined on more than 1
    // partition which have to be regrouped.
    double massallfrag=0.;
    int fragnum=1;
    fprintf(_fragmentationFile,"number;number of element;mass\n");
    #if defined(HAVE_MPI)
    std::set<fragmentMPI> fragmentlistMPI;
    int mysizeMPI = 0;  // compute the size of data to transfert
    #endif // HAVE_MPI
    std::vector<std::set<fragment>::iterator> fragmypart;
    for(std::set<fragment>::iterator it=fragmentlist.begin(); it!=fragmentlist.end();++it)
    {
      #if defined(HAVE_MPI)
      if((Msg::GetCommSize()>1) and ((*it).definedOnMoreThanOneRank())){
        fragmentlistMPI.insert(fragmentMPI(*it));
        mysizeMPI +=(*it).numberOfValueToCommunicateMPI();
      }
      else // fragment is totally included on this rank so can be written into the files
      #endif // HAVE_MPI
      {
        fprintf(_fragmentationFile,"%d;%d;%e\n",fragnum,(*it).numberOfElements(),(*it).mass());
        fragnum++;
        massallfrag+= (*it).mass();
        fragmypart.push_back(it);
      }
    }
    #if defined(HAVE_MPI)
    // regroup inter rank fragment (the data is send to rank 0 which do the job (change this ??))
    // size to transfert
    int *alldatasize;
    double* allfragMPI;
    double** allfragMPIrank0;
    MPI_Status mpistatus;
    if(Msg::GetCommRank()==0){
      // get fragment of the other ranks
      alldatasize = new int[Msg::GetCommSize()];
      allfragMPIrank0 = new double*[Msg::GetCommSize()];
      alldatasize[0] = mysizeMPI;
      for(int i=1;i<Msg::GetCommSize();i++)
      {
        MPI_Recv(&alldatasize[i],1,MPI_INT,i,i,MPI_COMM_WORLD,&mpistatus);
        allfragMPIrank0[i] = new double[alldatasize[i]];
        MPI_Recv(allfragMPIrank0[i],alldatasize[i],MPI_DOUBLE,i,i,MPI_COMM_WORLD,&mpistatus);
      }
      // double loop to avoid wait of other rank for MPI_comm ??
      // insert fragment of each rank
      std::set<fragmentMPI>::iterator it1;
      std::set<fragmentMPI>::iterator it2;
      for(int i=1;i<Msg::GetCommSize();i++)
      {
        int curpos=0;
        while(curpos<alldatasize[i])
        {
          fragmentMPI newfrag = fragmentMPI(allfragMPIrank0[i],curpos);
          curpos += newfrag.memorySize();
          // add fragment in the list
          std::vector<std::set<fragmentMPI>::iterator> vit;
          for(std::set<fragmentMPI>::iterator itf=fragmentlistMPI.begin(); itf!=fragmentlistMPI.end(); ++itf)
          {
            if((*itf).match(newfrag))
            {
              newfrag.add(*itf);
              vit.push_back(itf);
            }
          }
          // delete matched fragment
          for(int i=vit.size()-1; i>=0;i--)
          {
            fragmentlistMPI.erase(vit[i]);
          }
          // insert the new one
          fragmentlistMPI.insert(newfrag);
        }
      }
      // write to files
      for(std::set<fragmentMPI>::iterator it=fragmentlistMPI.begin(); it!=fragmentlistMPI.end();++it)
      {
        fprintf(_fragmentationFile,"%d;%d;%e\n",fragnum,(*it).numberOfElements(),(*it).mass());
        fragnum++;
        massallfrag+= (*it).mass();
      }


      // free memory
      for(int i=1;i<Msg::GetCommSize();i++)
        delete[] allfragMPIrank0[i];
      delete[] allfragMPIrank0;
    }
    else
    {
      // build a double vector to transfert all data to node 0
      allfragMPI = new double[mysizeMPI];
      int vecpos = 0;
      for(std::set<fragmentMPI>::iterator it=fragmentlistMPI.begin(); it!=fragmentlistMPI.end(); ++it)
      {
        vecpos +=(*it).fillMPIvector(&allfragMPI[vecpos]);
      }
      MPI_Send(&mysizeMPI,1,MPI_INT,0,Msg::GetCommRank(),MPI_COMM_WORLD);
      // send data
      MPI_Send(allfragMPI,mysizeMPI,MPI_DOUBLE,0,Msg::GetCommRank(),MPI_COMM_WORLD);

      delete[] allfragMPI;
    }
    Msg::Barrier(); // To wait rank0 before end
    #endif // HAVE_MPI
    // total mass at the end
    fprintf(_fragmentationFile,"total mass; %e\n number of fragment %d\n",massallfrag,fragnum-1);
    fflush(_fragmentationFile);
    /* create a view of fragment ( ie a mesh with a different physical number for each fragment) */
    //filename
    std::string fragmshname;
   #if defined(HAVE_MPI)
    if(Msg::GetCommSize()>1)
    {
      std::ostringstream oss;
      oss << Msg::GetCommRank();
      std::string partnum = oss.str();
      fragmshname = "fragview_part"+partnum+".msh";
    }
    else
   #endif //HAVE_MPI
    {
      fragmshname = "fragview.msh";
    }
    FILE *fpfragmsh = fopen(fragmshname.c_str(),"w");
    FILE *fpmsh = fopen(_meshFileName.c_str(),"r");
    fprintf(fpfragmsh,"$MeshFormat\n2.2 0 8\n$EndMeshFormat\n");
    // The node are copy only on rank 0
    char what[256];
//    if(Msg::GetCommRank()==0)
    {
      while(1)
      {
        fscanf(fpmsh,"%s",what);
        if(!strcmp(what,"$Nodes"))
        {
          fprintf(fpfragmsh,"$Nodes\n");
          fscanf(fpmsh,"%s",what);
          fprintf(fpfragmsh,"%s\n",what);
          break;
        }
      }
      int j=1;
      while(!feof(fpmsh))
      {
        fscanf(fpmsh,"%s",what);
        fprintf(fpfragmsh,"%s",what);
        if(!strcmp(what,"$EndNodes"))
        {
          fprintf(fpfragmsh,"\n");
          break;
        }
        if(j%4==0)
        {
          fprintf(fpfragmsh,"\n");
        }
        else
        {
          fprintf(fpfragmsh," ");
        }
        j++;
      }
    }
/*    else{
      while(!feof(fpmsh))
      {
        fscanf(fpmsh,"%s",what);
        if(!strcmp(what,"$EndNodes"))
        {
          break;
        }
      }
    }
*/
    fscanf(fpmsh,"%s",what);
    fprintf(fpfragmsh,"%s\n",what);
    int allelem; // total number of element in the mesh file
    fscanf(fpmsh,"%d",&allelem);
    // compute the total number of element included on this partition
    int totalelem=0;
    for(int i=0;i<fragmypart.size();i++)
    {
      totalelem +=fragmypart[i]->numberOfElements();
    }
   #if defined(HAVE_MPI)
    if(Msg::GetCommRank()==0)
    {
      for(std::set<fragmentMPI>::iterator it = fragmentlistMPI.begin(); it!=fragmentlistMPI.end();++it)
      {
        totalelem +=it->numberOfElements();
      }
    }
   #endif // HAVE_MPI
    fprintf(fpfragmsh,"%d\n",totalelem);
    int num1,num2;
    int physfrag;
    int typemshint;
    for(int k=0;k<allelem;k++)
    {
      int elenum;
      fscanf(fpmsh,"%d",&elenum);
      // check if the element is in the fragment of this partition
      bool writeelem=false;
      physfrag=0;
      for(int i=0;i<fragmypart.size();i++)
      {
        physfrag++;
        if(fragmypart[i]->isElem(elenum))
        {
          writeelem=true;
          break;
        }
      }
     #if defined(HAVE_MPI)
      if(Msg::GetCommRank()==0 and !writeelem)
      {
        for(std::set<fragmentMPI>::iterator it = fragmentlistMPI.begin(); it!=fragmentlistMPI.end();++it)
        {
          physfrag++;
          if(it->isElem(elenum))
          {
            writeelem = true;
            break;
          }
        }
      }
     #endif // HAVE_MPI
      fscanf(fpmsh,"%d %d",&typemshint,&num2);
      if(writeelem) fprintf(fpfragmsh,"%d %d %d ",elenum,typemshint,num2);
      fscanf(fpmsh,"%d",&num1); // read the physical number which has to be replaced
      if(writeelem) fprintf(fpfragmsh,"%d ",physfrag+1000000*Msg::GetCommRank());
      int nver = MElement::getInfoMSH(typemshint);       // get the number of vertices of the element
      // the gentity has to be different for each fragment ie same as physical
      fscanf(fpmsh,"%d",&num1);
      if(writeelem) fprintf(fpfragmsh,"%d ",physfrag+1000000*Msg::GetCommRank());
      for(int i=2;i<num2;i++){
        fscanf(fpmsh,"%d",&num1);
        if(writeelem) fprintf(fpfragmsh,"%d ",num1);
      }
      for(int i=0;i<nver;i++)
      {
        fscanf(fpmsh,"%d",&num1);
        if(writeelem){
          if(i!=nver-1)
            fprintf(fpfragmsh,"%d ",num1);
          else
            fprintf(fpfragmsh,"%d\n",num1);
        }
      }
      //if(writeelem) fprintf(fpfragmsh,"\n");
    }
    fprintf(fpfragmsh,"$EndElements");
    fclose(fpmsh);
    fclose(fpfragmsh);
    Msg::Barrier(); // wait all otherwise archiving problem ??
  }
}

void nonLinearMechSolver::fillMapOfInterfaceElementsInOneDomain(MElement *e, std::vector<MElement*> &eleFound,
                                const groupOfElements *g) const
{
    // take only the primary vertex (avoid unnecessery check for element with order > 1
    int nbVertex = e->getNumPrimaryVertices();
    std::vector<MVertex*> vv;
    for(int i=0;i<nbVertex;i++)
      vv.push_back(e->getVertex(i));

    // In 3D, in //, it is possible that all the nodes are in group g, but not from the same element
    // We need to identify by face.
    for(groupOfElements::elementContainer::const_iterator ite=g->begin(); ite !=g->end(); ite++)
    {
      MElement *gele = *ite;
      int nbVertex2 = gele->getNumPrimaryVertices();
      bool oneNotFound = false;
      for(std::vector<MVertex*>::const_iterator itvv = vv.begin(); itvv!= vv.end(); itvv++)
      {
        oneNotFound = true;
        for(int j=0;j<nbVertex2;j++)
        {
          if((gele->getVertex(j)) == (*itvv))
          {
            oneNotFound = false;
            break;
          }
        }
        if(oneNotFound) break;
      }
      if(!oneNotFound)
      {
        eleFound.push_back(e);
        break;
      }
    }
}

void nonLinearMechSolver::createStrainMapping(const std::string  filename){
  if (_strainMap) delete _strainMap;
  _strainMap = new strainMapping(domainVector);
  _strainMap->readMesh(filename);

  GModel::setCurrent(pModel);
};

void nonLinearMechSolver::setStrainMappingFollowing(const bool flag){
  _strainMappingFollwing = flag;
};

void nonLinearMechSolver::setTimeForMicroBC(const double time){
	for (int i=0; i<allMicroBC.size(); i++){
    allMicroBC[i]->setTime(time);
	};
};

void nonLinearMechSolver::microNumberDof(){
  if (_pathFollowing){
    _systemType = MULT_ELIM;
    _pAl->numberDof(MULT_ELIM);
  }
  else{
    if (_controlType == LOAD_CONTROL){
    // number normally dof
    _pAl->numberDof(_systemType);
    // allocate micro system
    }
    else if (_controlType == ARC_CONTROL_EULER){
      _systemType = DISP_MULT;
      _pAl->numberDof(DISP_MULT);
    }
    else{
      Msg::Fatal("control Type must be correct defined");
    }
  }
  // allocate system
  int systemSize = pAssembler->sizeOfR();
  std::string A("A");
  pAssembler->getLinearSystem(A)->allocate(systemSize);

  // assemble all linear constraint matrix to system in case of using mult -elim method
  if (_systemType == MULT_ELIM){
    _pAl->allocateConstraintMatrixToSystem();
    _pAl->assembleLinearConstraintMatrixToSystem();
  }
};

void nonLinearMechSolver::setPeriodicity(const double x, const double y, const double z, const std::string direction){
	if ((direction == "x") or (direction == "X")) {
		_Lx(0) = x; _Lx(1) = y; _Lx(2) = z;
	}
	else if ((direction == "y") or (direction == "Y")){
		_Ly(0) = x; _Ly(1) = y; _Ly(2) = z;
	}
	else if ((direction == "z") or (direction == "Z")){
		_Lz(0) = x; _Lz(1) = y; _Lz(2) = z;
	}
	else {
		Msg::Error("Incorrect direction is used in setting the periodicity!");
	};
};

void nonLinearMechSolver::setPeriodicity(const SVector3& xd, const SVector3& yd, const SVector3& zd){
  _Lx = xd; _Ly = yd; _Lz = zd;
};


void nonLinearMechSolver::addLinearDisplacementBC(const int tag, const int g1, const int g2, const int g3,
                                                  const int g4, const int g5, const int g6){
	_microFlag = true;
  printf("Using Linear Displacement Boundary Condition \n");
	nonLinearMicroBC* ndis = new nonLinearDisplacementBC(tag,g1,g2,g3, g4, g5, g6);
	allMicroBC.push_back(ndis);
};

void nonLinearMechSolver::addLinearDisplacementBC(const int tag, const std::vector<int>& physical){
  _microFlag = true;
  printf("Using Linear Displacement Boundary Condition \n");
  nonLinearMicroBC* ldbc = new nonLinearDisplacementBC(tag,physical[0],physical[1],physical[2],
                                                   physical[3],physical[4],physical[5]);
  allMicroBC.push_back(ldbc);
};

void nonLinearMechSolver::addPeriodicBC(const int tag, const int g1, const int g2, const int g3,
                                        const int g4, const int g5, const int g6){
	_microFlag= true;
  printf("Using Periodic Boundary Condition \n");

	nonLinearMicroBC* peri = new nonLinearPeriodicBC(tag,g1,g2,g3,g4,g5,g6);
	allMicroBC.push_back(peri);
};

void nonLinearMechSolver::addPeriodicBC(const int tag, const std::vector<int>& physical){
  _microFlag = true;
  printf("Using Periodic Boundary Condition \n");

  nonLinearMicroBC* peri = new nonLinearPeriodicBC(tag,physical[0],physical[1],physical[2],
                                                   physical[3],physical[4],physical[5]);
  allMicroBC.push_back(peri);
};

void nonLinearMechSolver::addMinimalKinematicBC(const int tag, const int g1, const int g2, const int g3,
                                                const int g4, const int g5, const int g6){
  _microFlag = true;
  printf("Using Minimal Kinematic Boundary Condition \n");
  nonLinearMicroBC* mini = new nonLinearMinimalKinematicBC(tag,g1,g2,g3,g4,g5,g6);
  allMicroBC.push_back(mini);
};

void nonLinearMechSolver::addMixedBC(const int tag, const int g1, const int g2, const int g3, const int g4, const int g5, const int g6){
  _microFlag = true;
  printf("Using Mixed BC\n");
  nonLinearMicroBC* mini = new nonLinearMixedBC(tag,g1,g2,g3,g4,g5,g6);
  allMicroBC.push_back(mini);
};

void nonLinearMechSolver::addKinematicPhysical(const int i, const int comp){
  if (allMicroBC.size() == 0) Msg::Fatal("no BC on boundary");
  nonLinearMixedBC* mixbc = dynamic_cast<nonLinearMixedBC*>(allMicroBC[0]);
  if (mixbc == NULL){
    Msg::Fatal("BC used in this boundary is not mixed BC");
  }
  else{
    mixbc->setKinematicPhysical(i,comp);
  }
};
void nonLinearMechSolver::addStaticPhysical(const int i, const int comp){
  if (allMicroBC.size() == 0) Msg::Fatal("no BC on boundary");
  nonLinearMixedBC* mixbc = dynamic_cast<nonLinearMixedBC*>(allMicroBC[0]);
  if (mixbc == NULL){
    Msg::Fatal("BC used in this boundary is not mixed BC");
  }
  else{
    mixbc->setStaticPhysical(i,comp);
  }
};

void nonLinearMechSolver::addMinimalKinematicBC(const int tag, const std::vector<int>& physical){
  _microFlag = true;
  printf("Using Linear Displacement Boundary Condition \n");
  nonLinearMicroBC* mkbc = new nonLinearMinimalKinematicBC(tag,physical[0],physical[1],physical[2],
                                                   physical[3],physical[4],physical[5]);
  allMicroBC.push_back(mkbc);
};

void nonLinearMechSolver::setPeriodicBCOptions(const int method, const int seg, const bool add){
	for (int i=0; i< allMicroBC.size(); i++){
	  nonLinearPeriodicBC* peri = dynamic_cast<nonLinearPeriodicBC*>(allMicroBC[i]);
	  if (peri){
	    peri->setPBCMethod(method);
	    peri->setPBCPolynomialDegree(0,seg);
	    peri->setPBCPolynomialDegree(1,seg);
	    peri->setPBCPolynomialDegree(2,seg);
	    peri->setAddNewVertices(0,add);
	    peri->setAddNewVertices(1,add);
	    peri->setAddNewVertices(2,add);

      if (peri->getPBCMethod() == nonLinearPeriodicBC::CEM){
        printf("Periodic mesh formulation is used for building the PBC constraint \n");
        return;
      }
      else if (peri->getPBCMethod() == nonLinearPeriodicBC::CSIM){
        printf("Cubic Spline Interpolation Method is used for building the PBC constraint\n");
        printf("Segment number X: %d  \n",peri->getPBCPolynomialDegree(0));
        printf("Segment number Y: %d  \n",peri->getPBCPolynomialDegree(1));
        if (_dim == 3)
          printf("Segment number Z: %d  \n",peri->getPBCPolynomialDegree(2));
      }
      else if (peri->getPBCMethod() == nonLinearPeriodicBC::LIM){
        printf("Lagrange Interpolation Method is used for building the PBC constraint\n");
        printf("Polynomial order X: %d \n",peri->getPBCPolynomialDegree(0));
        printf("Polynomial order Y: %d \n",peri->getPBCPolynomialDegree(1));
        if (_dim == 3)
          printf("Polynomial order Z: %d \n",peri->getPBCPolynomialDegree(2));
      }
      else if (peri->getPBCMethod() == nonLinearPeriodicBC::FE_LIN){
        printf("Linear segment is used for building the PBC constraint\n");
      }
      else if (peri->getPBCMethod() == nonLinearPeriodicBC::FE_QUA){
        printf("Quadratic segment is used for building the PBC constraint\n");
      }
      else{
        Msg::Fatal("This method is not implemented yet");
      }

      if (peri->addNewVertices(0)) {
        printf("Add new vertices X for periodic boudary condition \n");
      }
      if (peri->addNewVertices(1)) {
        printf("Add new vertices Y for periodic boudary condition \n");
      }
      if (peri->addNewVertices(2) && _dim == 3) {
        printf("Add new vertices Z for periodic boudary condition \n");
      }
	  }
	  else{
      Msg::Error("Impossible to set data for periodicBC because another BC type is chosen");
	  };
	};
};

void nonLinearMechSolver::setPeriodicBCDegree(const int direction, const int d, const bool addvertex){
  for (int i=0; i< allMicroBC.size(); i++){
	  nonLinearPeriodicBC* peri = dynamic_cast<nonLinearPeriodicBC*>(allMicroBC[i]);
	  if (peri){
	    peri->setPBCPolynomialDegree(direction,d);
	    peri->setAddNewVertices(direction,addvertex);
      if (peri->getPBCMethod() == nonLinearPeriodicBC::CEM){
        printf("Periodic mesh formulation is used for building the PBC constraint \n");
      }
      else if (peri->getPBCMethod() == nonLinearPeriodicBC::CSIM){
        printf("Cubic Spline Interpolation Method is used for building the PBC constraint\n");
        printf("Segment number %d: %d  \n",direction,peri->getPBCPolynomialDegree(direction));
      }
      else if (peri->getPBCMethod() == nonLinearPeriodicBC::LIM){
        printf("Lagrange Interpolation Method is used for building the PBC constraint\n");
        printf("Polynomial order %d: %d \n",direction,peri->getPBCPolynomialDegree(direction));
      }
      else if (peri->getPBCMethod() == nonLinearPeriodicBC::FE_LIN){
        printf("Linear segment is used for building the PBC constraint\n");
      }
      else if (peri->getPBCMethod() == nonLinearPeriodicBC::FE_QUA){
        printf("Quadratic segment is used for building the PBC constraint\n");
      }
      else{
        Msg::Fatal("This method is not implemented yet");
      }
      if (peri->addNewVertices(direction)) {
        printf("Add new vertices %d for periodic boudary condition \n",direction);
      }
	  }
	  else{
      Msg::Error("Impossible to set data for periodicBC because another BC type is chosen");
	  };
	};
};

void nonLinearMechSolver::setDeformationGradient(const int i,const  int j, const double val){
  for (int iter=0; iter< allMicroBC.size(); iter++){
		allMicroBC[iter]->setDeformationGradient(i,j,val);
	};
};

void nonLinearMechSolver::setDeformationGradient(const double F00, const double F01, const double F02,
                                                 const double F10, const double F11, const double F12,
                                                 const double F20, const double F21, const double F22){

	STensor3 eps(0.);
	eps(0,0) = F00; eps(0,1) = F01; eps(0,2) = F02;
	eps(1,0) = F10; eps(1,1) = F11; eps(1,2) = F12;
	eps(2,0) = F20; eps(2,1) = F21; eps(2,2) = F22;
	for (int i=0; i< allMicroBC.size(); i++){
	  for (int j=0; j<3; j++)
      for (int k=0; k<3; k++)
        allMicroBC[i]->setDeformationGradient(j,k,eps(j,k));
	};
};

void nonLinearMechSolver::setDeformationGradient(const double F00,const double F01,
                                                 const double F10, const double F11){
	STensor3 eps(0.);
	eps(0,0) = F00; eps(0,1) = F01;
	eps(1,0) = F10; eps(1,1) = F11;
	eps(2,2) = 1.;
	for (int i=0; i< allMicroBC.size(); i++){
	  for (int j=0; j<3; j++)
      for (int k=0; k<3; k++)
        allMicroBC[i]->setDeformationGradient(j,k,eps(j,k));
	};
};

void nonLinearMechSolver::setDeformationGradient(const STensor3& F){
	for (int i=0; i< allMicroBC.size(); i++){
		allMicroBC[i]->setDeformationGradient(F);
	};
};

void nonLinearMechSolver::setGradientOfDeformationGradient(int i, int j, int k, const double val){
  for (int iter=0; iter<allMicroBC.size(); iter++){
    allMicroBC[iter]->setGradientOfDeformationGradient(i,j,k,val);
  };
};

void nonLinearMechSolver::setGradientOfDeformationGradient(const STensor33& G){
  for (int i=0; i<allMicroBC.size(); i++){
    allMicroBC[i]->setGradientOfDeformationGradient(G);
  };
};

void nonLinearMechSolver::initializeLocalImplementationMethod(){
	for (int i=0; i< allMicroBC.size(); i++){
    int tag = allMicroBC[i]->getTag();
	  if (_periodicBC) delete _periodicBC;
	  nonLinearDisplacementBC* ldbc = dynamic_cast<nonLinearDisplacementBC*>(allMicroBC[i]);
    if (ldbc){
      _periodicBC = new linearDisplacementBC(tag,_dim,pAssembler);
      _periodicBC->setPeriodicGroup(ldbc->getBoundaryGroupOfElements(),_dim);
    };
    nonLinearPeriodicBC* peri = dynamic_cast<nonLinearPeriodicBC*>(allMicroBC[i]);
    if (peri){
      if (peri->getPBCMethod() == nonLinearPeriodicBC::CEM){
        _periodicBC= new constraintElimination(tag,_dim,pAssembler);
      }
      else if (peri->getPBCMethod() == nonLinearPeriodicBC::LIM){
        if (_dim ==2){
          _periodicBC = new polynomialInterpolationByLagrangeFuction(tag,_dim,pAssembler,peri->getPBCPolynomialDegree(0));
        }
        else if (_dim ==3){
          _periodicBC = new polynomialInterpolationByLagrangeFuction3D(tag,_dim,pAssembler,peri->getPBCPolynomialDegree(0));
        };
      }
      else if (peri->getPBCMethod() == nonLinearPeriodicBC::CSIM){
        if (_dim ==2){
          _periodicBC = new polynomialInterpolationByCubicSpline(tag,_dim,pAssembler,peri->getPBCPolynomialDegree(0));
        }
        else if (_dim ==3){
          _periodicBC = new polynomialInterpolationByCubicSpline3D(tag,_dim,pAssembler,peri->getPBCPolynomialDegree(0));
        };
      };
      _periodicBC->setPeriodicGroup(peri->getBoundaryGroupOfElements(),_dim);
    };
	};
	if (whatScheme == StaticLinear)
    this->setTimeForMicroBC(1.0);
  else
    this->setTimeForMicroBC(0.0);
	this->applyMicroBCByLocalImplementationMethod();
	this->createEigenvalueSolver();
};

void nonLinearMechSolver::applyMicroBCByLocalImplementationMethod(){
	if (_periodicBC){
		for (int i=0; i<allMicroBC.size(); i++){
      _periodicBC->setDeformation(allMicroBC[i]->getFirstOrderKinematicalVariable());
      _periodicBC->applyPeriodicCondition();
		};
	};
};

double nonLinearMechSolver::getRVEVolume() const{
	return _rveVolume;
};

void nonLinearMechSolver::setRVEVolume(const double val){
  _rveVolume = val;
}

double nonLinearMechSolver::getHomogenizedDensity() const{
  return _rho;
};

void nonLinearMechSolver::extractAverageStressByVolumeIntegral(STensor3& P, STensor33& Q){
  P*= (0.);
  if (_highOrderFlag)
    Q*= (0.);
  double vsolide = 0;
  for (int i=0; i<domainVector.size(); i++){
    double vpart, vpart2;
    STensor3 sigpart(0.);
    partDomain* dom = domainVector[i];
    dom->computeAverageStress(_ipf,sigpart,vpart);
    sigpart *= vpart;
    P+=(sigpart);
    vsolide += vpart;
    if (_highOrderFlag){
      STensor33 highsigpart;
      dom->computeAverageHighOrderStress(_ipf,highsigpart,vpart2);
      highsigpart *= vpart2;
      Q+= (highsigpart);
    }
  };
  double vtotal = this->getRVEVolume();
  double vin = 1./vtotal;

  P*=(vin);

  if (_highOrderFlag){
    Q*= (vin);
  }
};

void nonLinearMechSolver::homogenizedDataToFile(const double time){
  homogenizedData* data = this->getHomogenizationState(IPStateBase::current);
  if (_homogenizedFiles._firstStrainFile){
    Tensor23::writeData(_homogenizedFiles._firstStrainFile,allMicroBC[0]->getFirstOrderKinematicalVariable(),time);
  }
  if (_homogenizedFiles._secondStrainFile){
    Tensor33::writeData(_homogenizedFiles._secondStrainFile,allMicroBC[0]->getSecondOrderKinematicalVariable(),time);
  }
  if (_homogenizedFiles._firstStressFile){
    Tensor23::writeData(_homogenizedFiles._firstStressFile,data->_stress,time);
  }
  if (_homogenizedFiles._secondStressFile){
    Tensor33::writeData(_homogenizedFiles._secondStressFile,data->_highStress,time);
  }
  if (_homogenizedFiles._firstFirstTangentFile){
    Tensor43::writeData(_homogenizedFiles._firstFirstTangentFile,data->_tangent,time);
  }
  if (_homogenizedFiles._firstSecondTangentFile){
    Tensor53::writeData(_homogenizedFiles._firstSecondTangentFile,data->_tangentFG,time);
  }
  if (_homogenizedFiles._secondFirstTangentFile){
    Tensor53::writeData(_homogenizedFiles._secondFirstTangentFile,data->_tangentGF,time);
  }
  if (_homogenizedFiles._secondSecondTangentFile){
    Tensor63::writeData(_homogenizedFiles._secondSecondTangentFile,data->_tangentGG,time);
  }
};

void nonLinearMechSolver::extractAverageStressByCondensation(STensor3& P, STensor33& Q){
  /**create object in case of NULL **/
  if (_condensation == NULL)
    _condensation = this->createStiffnessCondensation(true,false);

  /** solve**/
  _condensation->stressSolve();

  /** get stress from results **/
  P = _condensation->getConstRefToMacroStress();
  if (_highOrderFlag){
    stiffnessCondensationSecondOrder* secondCon = dynamic_cast<stiffnessCondensationSecondOrder*>(_condensation);
    if (secondCon){
      Q = secondCon->getConstRefToMacroStressSecondOrder();
    }
  }
};

void nonLinearMechSolver::extractAveragePropertiesByCondensation(STensor3& P, STensor33& Q,
                              STensor43& FF, STensor53& FG, STensor53& GF, STensor63& GG){
  /**create object in case of NULL **/
  if (_condensation == NULL){
    if (_homogenizeStressMethod == VOLUME)
      _condensation = this->createStiffnessCondensation(false, true);
    else
      _condensation = this->createStiffnessCondensation(true, true);
  }

  if (_homogenizeStressMethod == VOLUME){
    this->extractAverageStressByVolumeIntegral(P,Q);
  }
  else{

    _condensation->stressSolve();

    /** get stress from results **/
    P = _condensation->getConstRefToMacroStress();
    if (_highOrderFlag){
      stiffnessCondensationSecondOrder* secondCon = dynamic_cast<stiffnessCondensationSecondOrder*>(_condensation);
      if (secondCon){
        Q = secondCon->getConstRefToMacroStressSecondOrder();
      }
    }
  }

  /** compute splitted stiffness matrix**/
  this->computeSplittedStiffnessMatrix();

  /** solve**/
  _condensation->tangentCondensationSolve();

  /**get tangents from results **/
  FF = _condensation->getConstRefToMacroTangent();

  if (_highOrderFlag){
    stiffnessCondensationSecondOrder* conhighorder = dynamic_cast<stiffnessCondensationSecondOrder*>(_condensation);
    if (conhighorder){
      FG = conhighorder->getConstRefToMacroTangentFirstSecond();
      GF = conhighorder->getConstRefToMacroTangentSecondFirst();
      GG = conhighorder->getConstRefToMacroTangentSecondSecond();
    }
  }
};

void nonLinearMechSolver::extractAverageStress(STensor3& P, STensor33& Q){
  if (_homogenizeStressMethod == VOLUME)
    this->extractAverageStressByVolumeIntegral(P,Q);
  else if (_homogenizeStressMethod == SURFACE)
    this->extractAverageStressByCondensation(P,Q);
  else {
    Msg::Fatal("This method is not implemented");
  }
};

void nonLinearMechSolver::extractAverageStressAndTangent(STensor3& P, STensor33& Q, STensor43& FF, STensor53& FG, STensor53& GF, STensor63& GG){
  if (_homogenizeTangentMethod == CONDEN){
    this->extractAveragePropertiesByCondensation(P,Q,FF,FG,GF,GG);
  }
  else if (_homogenizeTangentMethod == PERTURB){
    this->extractAveragePropertiesPerturbation(P,Q,FF,FG,GF,GG);
  }
  else{
    Msg::Fatal("Homogenization tangent method must be correctly defined");
  }
};

void nonLinearMechSolver::extractAveragePropertiesPerturbation(STensor3& P, STensor33& Q,
                                            STensor43& FF, STensor53& FG, STensor53& GF, STensor63& GG){
  this->extractAverageStress(P,Q);
  /** function for compute tangent by perturbation on current state**/
    /*
    save all computed current data to tmp step
  */

  _ipf->copy(IPStateBase::current,IPStateBase::temp);
  std::string Aname ="A";
  linearSystem<double>* lsys = pAssembler->getLinearSystem(Aname);
  nonLinearSystem<double>* nonlsys = dynamic_cast<nonLinearSystem<double>*>(lsys);
  nonlsys->copy(IPStateBase::current,IPStateBase::temp);

  const STensor3 F = allMicroBC[0]->getFirstOrderKinematicalVariable();
  FF *= (0.);
  if (_highOrderFlag){
    FG*=(0.);
    GF*=(0.);
    GG*=(0.);
  }
  for (int i=0; i<_dim; i++){
    for (int j=0; j<_dim; j++){
      nonlsys->copy(IPStateBase::previous,IPStateBase::current);
      /** perturbation in deformation gradient **/
      STensor3 Fplus(F);
      Fplus(i,j) += _tangentPerturbation;
      allMicroBC[0]->setFirstOrderKinematicalVariable(Fplus);

      /** solve perturbated system**/
      this->OneStep();

      /** get homogenized stress in perturbed system **/
      STensor3 Pplus;
      STensor33 Qplus;
      this->extractAverageStress(Pplus,Qplus);

      for (int k=0; k<3; k++){
        for (int l=0; l<3; l++){
          FF(k,l,i,j) = (Pplus(k,l) - P(k,l))/(_tangentPerturbation);
        }
      }

      if (_highOrderFlag){
        for (int k=0; k<3; k++)
          for (int l=0; l<3; l++)
            for (int m=0; m<3; m++)
              GF(k,l,m,i,j) = (Qplus(k,l,m) - Q(k,l,m))/(_tangentPerturbation);
      }
    }
  }
  /** back micro BC as before perturbation**/
  allMicroBC[0]->setFirstOrderKinematicalVariable(F);

  if (_highOrderFlag){
    /**perturbation in gradient of deformation gradient **/
    const STensor33 G = allMicroBC[0]->getSecondOrderKinematicalVariable();
    MElement* ele = *(domainVector[0]->g->begin());
    double volume = ele->getVolume();
    double secondpert;
    if (_dim == 2){
      secondpert = _tangentPerturbation/sqrt(volume);
    }
    else if (_dim ==3){
      secondpert = _tangentPerturbation/pow(volume,1.0/3.0);
    }

    if (secondpert <= 0.)
      Msg::Fatal("No element on this problem");

    for (int i=0; i<_dim; i++){
      for (int j=0; j<_dim; j++){
        for (int s=0; s<=j; s++){
          nonlsys->copy(IPStateBase::previous,IPStateBase::current);
          STensor33 Gplus(G);
          Gplus(i,j,s) += secondpert;
          Gplus(i,s,j) += secondpert;

          allMicroBC[0]->setSecondOrderKinematicalVariable(Gplus);
          /** solve perturbed system**/
          this->OneStep();

          /** get homogenized stress in perturbed system **/
          STensor3 Pplus;
          STensor33 Qplus;

          this->extractAverageStress(Pplus,Qplus);

          for (int k=0; k<3; k++){
            for (int l=0; l<3; l++){
              FG(k,l,i,j,s) = (Pplus(k,l) - P(k,l))/(2*secondpert);
              FG(k,l,i,s,j) = FG(k,l,i,j,s);
            }
          }

          if (_highOrderFlag){
            for (int k=0; k<3; k++)
              for (int l=0; l<3; l++)
                for (int m=0; m<3; m++){
                  GG(k,l,m,i,j,s) = (Qplus(k,l,m) - Q(k,l,m))/(2*secondpert);
                  GG(k,l,m,i,s,j)  = GG(k,l,m,i,j,s) ;
                }

          }
        }
      }
    }
    allMicroBC[0]->setSecondOrderKinematicalVariable(G);
  }

  /**back to current step from temp state **/
  _ipf->copy(IPStateBase::temp,IPStateBase::current);
  nonlsys->copy(IPStateBase::temp,IPStateBase::current);
}

void nonLinearMechSolver::computeDensity(){
  _rho = 0;
  for (int i=0; i<domainVector.size(); i++){
    partDomain* dom = domainVector[i];
    double rhodom = dom->getMaterialLaw()->density();
    double voldom =dom->computeVolumeDomain(_ipf);
    _rho += (voldom*rhodom);
  };
  double vtotal = this->getRVEVolume();
  _rho /= vtotal;
};

stiffnessCondensation* nonLinearMechSolver::createStiffnessCondensation(const bool sflag, const bool tflag){
  stiffnessCondensation* stiffCon = NULL;
  nonLinearPeriodicBC* pbc = dynamic_cast<nonLinearPeriodicBC*>(allMicroBC[0]);

  /**condensation require **/
  if (_pAl == NULL){
    _pAl = new pbcAlgorithm(allMicroBC[0],domainVector,pAssembler);
  };
  if (!_pAl->isSplittedDof()) _pAl->splitDofs();

  if (tflag) {
      _pAl->getSplittedDof()->allocateSplittedMatrix();
  };

  if (pbc->getPBCMethod() == nonLinearPeriodicBC::LIM && pbc->getMaxDegree() == 1){
    #if defined(HAVE_PETSC)
    if (allMicroBC[0]->getOrder()==1){
      stiffCon = new stiffnessCondensationLDBCPETSc(pAssembler,_pAl,sflag,tflag);
    }
    else if (allMicroBC[0]->getOrder()==2){
      stiffCon = new stiffnessCondensationSecondOrderLDBCPETSc(pAssembler,_pAl,sflag,tflag);
    }
    #else
    Msg::Fatal("Petsc must be used");
    #endif
  }
  else{
    bool addnewvertexflag = false;
    if (pbc)
      addnewvertexflag = pbc->isNewVertices();

    if (addnewvertexflag){
      /** polynomial interplation is used**/
      if (allMicroBC[0]->getOrder()==1){
        #if defined(HAVE_PETSC)
        stiffCon = new interpolationStiffnessCondensationPETSC(pAssembler,_pAl,sflag,tflag);
        #else
        Msg::Fatal("Petsc must be used");
        #endif
      }
      else if (allMicroBC[0]->getOrder()==2){
        #if defined(HAVE_PETSC)
        stiffCon = new stiffnessCondensationSecondOrderPETSc(pAssembler,_pAl,sflag,tflag);
        #else
        Msg::Fatal("Petsc must be used");
        #endif
      }
    }
    else{
      /**classical pbc is used **/
      if (allMicroBC[0]->getOrder()==1){
        printf("first order condensation \n");
        #if defined(HAVE_PETSC)
        if (dynamic_cast<nonLinearMinimalKinematicBC*>(allMicroBC[0])){
          printf("MKBC condensation is used \n");
          stiffCon = new stiffnessCondensationMKBCPETSc(pAssembler,_pAl,sflag,tflag);
        }
        else if (dynamic_cast<nonLinearDisplacementBC*>(allMicroBC[0])){
          printf("LDBC condensation is used \n");
          stiffCon = new stiffnessCondensationLDBCPETSc(pAssembler,_pAl,sflag,tflag);
        }
        else if (dynamic_cast<nonLinearPeriodicBC*>(allMicroBC[0])){
          printf("Periodic condensation is used\n");
          stiffCon = new stiffnessCondensationPETSc(pAssembler,_pAl,sflag,tflag);
        }
        else{
          Msg::Fatal("Micro BC type must be defined");
        }
        #else
        if (dynamic_cast<nonLinearPeriodicBC*>(allMicroBC[0])){
          printf("Periodic condensation is used\n");
          stiffCon = new stiffnessCondensationFullMatrix(pAssembler,_pAl,sflag,tflag);
        }
        else
          Msg::Fatal("Condensation for other BC types is not defined");

        #endif
      }
      else if (allMicroBC[0]->getOrder()==2){
        #if defined(HAVE_PETSC)
        printf("second order condensation \n");
        if (dynamic_cast<nonLinearMinimalKinematicBC*>(allMicroBC[0])){
          printf("MKBC condensation is used \n");
          stiffCon = new stiffnessCondensationSecondOrderPETSc(pAssembler,_pAl,sflag,tflag);
        }
        else if (dynamic_cast<nonLinearDisplacementBC*>(allMicroBC[0])){
          printf("LDBC condensation is used\n");
          stiffCon = new stiffnessCondensationSecondOrderLDBCPETSc(pAssembler,_pAl,sflag,tflag);
        }
        else if (dynamic_cast<nonLinearPeriodicBC*>(allMicroBC[0])){
          printf("Periodic condensation is used\n");
          stiffCon = new stiffnessCondensationSecondOrderPETSc(pAssembler,_pAl,sflag,tflag);
        }
        else {
          Msg::Fatal("Micro BC type must be defined");
        }
        #else
        Msg::Fatal("Petsc must be used");
        #endif
      }
    };
  }

  stiffCon->setRVEVolume(this->getRVEVolume());
  return stiffCon;
};

void nonLinearMechSolver::setMessageView(const bool view){
  _messageView = view;
}

void nonLinearMechSolver::extractAverageProperties(bool stiff){
  homogenizedData* homoData = this->getHomogenizationState(IPStateBase::current);
  /* get homogenization value */
  if (stiff){
    this->extractAverageStressAndTangent(homoData->_stress,homoData->_highStress,
                              homoData->_tangent,homoData->_tangentFG,
                              homoData->_tangentGF,homoData->_tangentGG);
  }
  else{
    this->extractAverageStress(homoData->_stress,homoData->_highStress);
  }
};

void nonLinearMechSolver::createEigenvalueSolver(){
	if (_eigflag){
		if (_eigenSolver) delete _eigenSolver;
		std::string str = "A";
		linearSystem<double>* lsys = pAssembler->getLinearSystem(str);
		linearSystemPETSc<double>* lpet = dynamic_cast<linearSystemPETSc<double>*>(lsys);
		if (lpet){
      _eigenSolver = new eigenSolver(lpet);
      // for eigen mode view
      _eigfield.clear();
      for (int i=0; i<_modeview.size(); i++){
        std::string modefilename = "E_"+int2str(_enum)+"_GP_"+int2str(_gnum)+"_mode";
        unknownField* field = new eigenVectorField(pAssembler,domainVector,_ghostDomainMPI,
                                                   &_allContact,3,anoded,_eigview[i],_eigenSolver,_modeview[i],modefilename);
        _eigfield.push_back(field);
      };
      std::string eigfilename = "E_"+int2str(_enum)+"_GP_"+int2str(_gnum)+"_eigenValue.csv";
      _eigenValueFile = fopen(eigfilename.c_str(),"w");
		}
		else{
      Msg::Fatal("eigensolver requires PETSc");
		}
	};
};

void nonLinearMechSolver::computeSplittedStiffnessMatrix(){
	_pAl->getSplitStiffness()->zeroSubMatrix();
	for(std::vector<partDomain*>::iterator itdom=domainVector.begin(); itdom!=domainVector.end(); ++itdom){
    partDomain* dom = *itdom;
    AssembleSplittedStiffness(*(dom->getBilinearBulkTerm()),*(dom->getFunctionSpace()),dom->g->begin(),dom->g->end(),
             *(dom->getBulkGaussIntegrationRule()),*_pAl->getSplittedDof());

    if(dom->IsInterfaceTerms()){
      dgPartDomain *dgdom = static_cast<dgPartDomain*>(dom);
      // Assembling loop on elementary interface terms
      AssembleSplittedStiffness(*(dgdom->getBilinearInterfaceTerm()),*(dom->getFunctionSpace()),dgdom->gi->begin(),dgdom->gi->end(),
               *(dgdom->getInterfaceGaussIntegrationRule()),*_pAl->getSplittedDof());
      // Assembling loop on elementary boundary interface terms
      AssembleSplittedStiffness(*(dgdom->getBilinearVirtualInterfaceTerm()),*(dom->getFunctionSpace()),dgdom->gib->begin(),dgdom->gib->end(),
               *(dgdom->getInterfaceGaussIntegrationRule()),*_pAl->getSplittedDof());
    }
  }

  for(contactContainer::iterator it = _allContact.begin(); it!=_allContact.end(); ++it){
    contactDomain *cdom = *it;
    contactBilinearTermBase<double> *sterm = static_cast<contactBilinearTermBase<double>*>(cdom->getStiffnessTerm());
    AssembleSplittedStiffness(*sterm,*(cdom->getSpace()),sterm->getContactNodes()->elemBegin(),
                    sterm->getContactNodes()->elemEnd(),*(cdom->getGaussIntegration()),*_pAl->getSplittedDof());
  }
};

homogenizedData* nonLinearMechSolver::getHomogenizationState(const IPStateBase::whichState state){
  if (state == IPStateBase::initial or state == IPStateBase::previous){
    return _initialState;
  }
  else if (state == IPStateBase::current){
    return _currentState;
  }
  else{
    Msg::Error("This state does not exist");
    return NULL;
  }
}

void nonLinearMechSolver::resetSolverToInitialStep(){
  // system
  std::string name = "A";
  linearSystem<double>* lsys = pAssembler->getLinearSystem(name);
  nonLinearSystem<double>* nonlsys = dynamic_cast<nonLinearSystem<double>*>(lsys);
  nonlsys->copy(IPStateBase::initial,IPStateBase::current);

  pathFollowingSystem<double>* pathSys = dynamic_cast<pathFollowingSystem<double>*>(nonlsys);
  if (pathSys){
    pathSys->resetControlParameter();
  }

  // ipf field
  _ipf->copy(IPStateBase::initial,IPStateBase::current);

  // homogenized data
  (*_currentState) = (*_initialState);
};

void nonLinearMechSolver::closeAllFile(){
  if (_ipf) _ipf->closeFile();
  if (_ufield) _ufield->closeFile();
  if (_energField) _energField->closeFile();

  for (int i=0; i<_eigfield.size(); i++){
    _eigfield[i]->closeFile();
  }

  if (_outputFile != NULL) fclose(_outputFile);

  if (_eigenValueFile != NULL) fclose(_eigenValueFile);

  _homogenizedFiles.closeFiles();

  if (_criterionFile != NULL) fclose(_criterionFile);
};

void nonLinearMechSolver::nextStep(){
  // in microBC
  for (int i=0; i<allMicroBC.size(); i++){
    allMicroBC[i]->nextStep();
  }
  // archive data
  this->archiveData(0.,0.);

  // next step for solver
  std::string name = "A";
  linearSystem<double>* lsys = pAssembler->getLinearSystem(name);
  nonLinearSystem<double>* nonlsys = dynamic_cast<nonLinearSystem<double>*>(lsys);
  pathFollowingSystem<double>* pathSys = dynamic_cast<pathFollowingSystem<double>*>(nonlsys);
  nonlsys->copy(IPStateBase::current,IPStateBase::initial);

  if (pathSys){
    pathSys->resetControlParameter();
  }

  // for ipfield
  _ipf->copy(IPStateBase::current,IPStateBase::initial);

  // for homogenized data
  if (_whichState){
    _currentState = &_state2;
    _initialState = &_state1;
    _whichState = false;
  }
  else{
    _currentState = &_state1;
    _initialState = &_state2;
    _whichState = true;
  }
};

void nonLinearMechSolver::getStress(STensor3& stress) const{
  stress = _currentState->_stress;
};
void nonLinearMechSolver::getTangent(STensor43& tangent) const{
  tangent = _currentState->_tangent;
};
void nonLinearMechSolver::getHighOrderStress(STensor33& stress) const{
  stress = _currentState->_highStress;
};
void nonLinearMechSolver::getHighOrderTangent(STensor63& J) const{
  J = _currentState->_tangentGG;
};
void nonLinearMechSolver::getHighOrderCrossFirstSecondTangent(STensor53& J) const{
  J = _currentState->_tangentFG;
};
void nonLinearMechSolver::getHighOrderCrossSecondFirstTangent(STensor53& J) const{
 J = _currentState->_tangentGF;
};

double nonLinearMechSolver::critialPointTestFunction(double time, double* &mode){
  if (_eigflag){
    if (_eigenSolver == NULL) createEigenvalueSolver();
    _eigenSolver->clear();
    int nsize = pAssembler->sizeOfR();
    if (_numeigenvalue >= nsize) {
      _numeigenvalue = nsize;
      if (_outputFile)
      fprintf(_outputFile,"Modify number of eigenvalues computed: %d \n",nsize);
    }
    // solver eigen solver
    _eigenSolver->solve(_numeigenvalue,"smallestReal");

    std::complex<double> ls = _eigenSolver->getEigenValue(0);
    std::complex<double> lm = _eigenSolver->getEigenValue(_numeigenvalue-1);

    fprintf(_eigenValueFile,"%.16g\t",time);
    for (int i=0; i<_numeigenvalue; i++){
      std::complex<double> val = _eigenSolver->getEigenValue(i);
        fprintf(_eigenValueFile,"%.16g  \t %.16g \t",val.real(), val.imag());
    }
    fprintf(_eigenValueFile,"\n");
    fflush(_eigenValueFile);

    mode = new double[nsize];
    for (int i=0; i<nsize; i++){
      std::complex<double>  s = _eigenSolver->getEigenVectorComp(0,i);
        mode[i] = s.real();
    }

    return ls.real();
  }
  else
  {
    return 1.e10;
  }
};

linearSystem<double>* nonLinearMechSolver::createMicroSNLSystem(){
  linearSystem<double>* lsys = NULL;
  if (_pathFollowing){
    _controlType = nonLinearMechSolver::LOAD_CONTROL; // this means compute until time = 1.
    _systemType == nonLinearMechSolver::MULT_ELIM; // always multiplier elimination
    #if defined(HAVE_PETSC)
    Msg::Info("Path following is used");
    lsys = new pbcPathFollowingSystemPETSC<double>();
    dynamic_cast<pathFollowingSystem<double>*>(lsys)->setControlType(_macroControlType);
    #else
    Msg::Fatal("This option used with petsc only");
    #endif
  }
  else{
    if (_controlType == nonLinearMechSolver::LOAD_CONTROL){
      if (_systemType == nonLinearMechSolver::MULT_ELIM){
        if(nonLinearMechSolver::whatSolver == Petsc){
          #if defined(HAVE_PETSC)
          lsys = new pbcNonLinearSystemPETSc<double>();
          if (_outputFile)
            fprintf(_outputFile,"PETSc is chosen to solve \n");
          #else
          lsys = new pbcNonLinearSystemGmm<double>();
          dynamic_cast<pbcNonLinearSystemGmm<double>*>(lsys)->setNoisy(2);
          if (_outputFile)
            fprintf(_outputFile,"PETSc is not installed\n Gmm is chosen to solve \n");
          #endif
        }
        else{
          Msg::Fatal("Petsc is required");
        }
      }
      else{
        if(nonLinearMechSolver::whatSolver == Petsc){
          #if defined(HAVE_PETSC)
          lsys = new nonLinearSystemPETSc<double>(PETSC_COMM_SELF,_lineSearch);
          if (_outputFile)
            fprintf(_outputFile,"PETSc is chosen to solve \n");
          #else
          lsys = new nonLinearSystemGmm<double>(_lineSearch);
          dynamic_cast<nonLinearSystemGmm<double>*>(lsys)->setNoisy(2);
          if (_outputFile)
            fprintf(_outputFile,"PETSc is not installed\n Gmm is chosen to solve \n");
          #endif
        }
        else
          lsys= nonLinearMechSolver::createSNLSystem();
      }

    }
    else if (_controlType == nonLinearMechSolver::ARC_CONTROL_EULER){
      if (nonLinearMechSolver::whatSolver == Petsc){
        #if defined(HAVE_PETSC)
        lsys = new forwardEulerLinearSystemPETSc<double>();
        if (_outputFile)
          fprintf(_outputFile,"PETSc is chosen to solve \n");
        #else
        Msg::Fatal("Petsc is not available");
        #endif
      }
      else{
        Msg::Fatal("This is not implemented without PETSc");
      };
    };
  }

  if (_outputFile)
    fflush(_outputFile);

  return lsys;
};

void nonLinearMechSolver::createMicroSystem(){
  if (!_previousInit){
    linearSystem<double>* lsys = createMicroSNLSystem();
    if (_controlType == ARC_CONTROL_EULER or whatScheme == StaticLinear)
      pAssembler = new dofManager<double>(lsys);
    else
      pAssembler = new staticDofManager<double>(lsys,whatScheme,false,false);
  };
};

void nonLinearMechSolver::initMicroSolver(){
  if (_messageView){
    std::string filename = "E_"+int2str(_enum)+"_GP_"+int2str(_gnum)+"_out.txt";
    _outputFile = fopen(filename.c_str(),"w");

    fprintf(_outputFile,"Initializing on processor %d of %d \nMicro-problem: element %d, gauss point %d \nMicro SNL Data : nstep =%d  endtime = %f\n",
        Msg::GetCommRank(),Msg::GetCommSize(),this->getElementSolverNumber(),this->getGaussPointSolverNumber(),
        _numstepImpl,endtime);
  }

  // read mesh file
  this->init();

  // create system, dofManager
  this->createMicroSystem();

  // create periodic algorithm
  if (_pAl) delete _pAl;
	_pAl = new pbcAlgorithm(allMicroBC[0],domainVector,pAssembler);

	//pbc data from file
	const std::vector<groupOfElements*>& bgroup = allMicroBC[0]->getBoundaryGroupOfElements();
	if (bgroup.size() == 0){
    _pAl->getPBCConstraintGroup()->createPBCFromFile(_pbcNodeData,pModel);
	}

	// split dofs
  if (!_pAl->isSplittedDof()) _pAl->splitDofs();

  std::string ff = "E_"+int2str(_enum)+"_GP_"+int2str(_gnum)+"_virtualSize.csv";
  FILE* fffile = fopen(ff.c_str(),"w");
	fprintf(fffile,"%d",_pAl->getSplittedDof()->sizeOfVirtualDof());
	fclose(fffile);

  // set initial time for micro BC
  this->setTimeForBC(0.);

  // number Dof and allocate the system
  this->microNumberDof();

  // create ufield, ipfield
  if (_ufield) delete _ufield;
  if (_ipf) delete _ipf;
  if (_energField) delete _energField;
  // clear all view if non archive
  if (!_archive){
    unknownView.clear();
    ipView.clear();
    energyView.clear();
  }

  std::string dispfile ="E_"+int2str(_enum)+"_GP_"+int2str(_gnum)+"_disp.msh";
  _ufield = new unknownField(pAssembler,domainVector,_ghostDomainMPI,&_allContact,3,anoded,unknownView,dispfile); // 3 components by nodes User choice ??

  std::string ipfile = "E_"+int2str(_enum)+"_GP_"+int2str(_gnum)+"_stress.msh";
  _ipf = new IPField(&domainVector,pAssembler,_ufield,vaip,ipView,ipfile); // Field for GaussPoint

  _energyComputation = 0;
  _fractureEnergyComputation = 0;
  _energField = new energeticField(_ipf,_ufield,pAssembler,domainVector,_allContact,allNeumann,energyView,_energyComputation,_fractureEnergyComputation);


  if (_eigflag){
    if (_outputFile)
      fprintf(_outputFile,"Create EigenSolver \n");
    createEigenvalueSolver();
    if (_outputFile)
      fprintf(_outputFile,"EigenSolver created \n");
  };

  _ipf->compute1state(IPStateBase::initial,true);

  _ipf->initialBroken(pModel, initbrokeninter);

  _ipf->copy(IPStateBase::initial,IPStateBase::previous);
  _ipf->copy(IPStateBase::initial,IPStateBase::current);

  // init terms
  this->initTerms(_ufield,_ipf);


  /** data for stabitity analysis **/

  std::string filename = "E_"+int2str(_enum)+"_GP_"+int2str(_gnum)+"_stabilityCriterion.csv";
  _criterionFile = fopen(filename.c_str(),"w");

  // rve_volume from periodicity
  if (_rveVolume == 0.){
    SVector3 vec = crossprod(_Lx,_Ly);
    _rveVolume = dot(vec,_Lz);
	}

  // compute density
  this->computeDensity();
  Msg::Error("homogenized density: %e",_rho);
  std::string denfile = "E_"+int2str(_enum)+"_GP_"+int2str(_gnum)+"_density.csv";
  FILE* fileden = fopen(denfile.c_str(),"w");
  fprintf(fileden,"%e",this->_rho);
  fclose(fileden);


  // initialize all homogenized
  _whichState = true;
  _currentState = &_state1;
  _initialState = &_state2;

  if (allMicroBC[0]->getOrder() >1)
    _highOrderFlag = true;
  else
    _highOrderFlag = false;

  /** initial tangent**/
  //this->extractAverageProperties(_tangentflag);

  if (_tangentflag){
    this->extractAverageStressAndTangent(_currentState->_stress,_currentState->_highStress,
                _currentState->_tangent,_currentState->_tangentFG, _currentState->_tangentGF,
                _currentState->_tangentGG);
  }
  else{
    this->extractAverageStress(_currentState->_stress,_currentState->_highStress);
  }

  *_initialState = *_currentState;

  if (_multiscaleFlag == false){
    _currentState->_stress.print("stress");
    if (_tangentflag){
      _currentState->_tangent.print("tangent");
    }
    if (_highOrderFlag)
      _currentState->_highStress.print("ho stress");

  }

  _homogenizedFiles.openFiles(_enum,_gnum,allMicroBC[0]->getOrder(),
                              _isHommStrainSaveToFile,_isHommProSaveToFile);
  this->homogenizedDataToFile(0.);
  _previousInit = true;

  if (_outputFile) {
    fprintf(_outputFile,"---------End of initialization-------- \n");
    fflush(_outputFile);
  }
};

double nonLinearMechSolver::microSolve(){
  double nor = allMicroBC[0]->kinematicalResidual();
  double time = 0;
  this->resetSolverToInitialStep();
  if (nor> _sameStateCriterion){
    if (_outputFile)
      fprintf(_outputFile,"Solving in procs %d \n",Msg::GetCommRank());
    // solve system
    if (whatScheme ==StaticLinear){
      time = solveMicroSolverStaticLinear();
    }
    else if (whatScheme == StaticNonLinear){
      time = solveMicroSolverSNL();
    }
    else
      Msg::Fatal("This scheme is not implemented");

    this->extractAverageProperties(_tangentflag);

    if (_outputFile)
      fflush(_outputFile);
  }
  return time;
};

void nonLinearMechSolver::OneStep(){
  int niteNR = 0;
  if (_pathFollowing){
    niteNR = pathFollowingPerturbation(pAssembler,_ufield);
  }
  else{
    if (_systemType == MULT_ELIM)
      niteNR = microNewtonRaphson(pAssembler,_ufield);
    else {
      if (_systemType == DISP_ELIM)
        _pAl->applyPBCByConstraintElimination();
      niteNR = NewtonRaphson(pAssembler,_ufield);
    }
    if(niteNR == _maxNRite) // time step reduction
    {
      Msg::Fatal("convergent fails in tangent perturbation ele %d GP %d",_enum,_gnum);
    }
  }

};

double nonLinearMechSolver::solveMicroSolverSNL(){
  std::string name = "A";
  linearSystem<double>* lsys =pAssembler->getLinearSystem(name);
  nonLinearSystem<double>* nonsys = dynamic_cast<nonLinearSystem<double>*>(lsys);
  pathFollowingSystem<double>* pathSys = dynamic_cast<pathFollowingSystem<double>*>(lsys);
  staticDofManager<double> *sAssembler = dynamic_cast<staticDofManager<double>*>(pAssembler);

  // change to introduce a dynamic modification of time step in case of convergence problem
  double curtime = 0.;
  double lasttime = 0.; // to avoid precision problem curtime = lasttime in place of curtime -= dt if no convergence
  double dendtime = double(endtime);
  double dtmax = dendtime/double(_numstepImpl); // time step value prescribed by user (time step can not be greater than this value)
  double dt = dtmax; // initial time step value
  int ii = 0; // iteration counter
  double tolend = 0.000001/dendtime; // OTHERWISE LAST STEP CAN BE DONE TWICE ??
  double toldtmax = 0.000001/dtmax; // IDEM (PRECISION PROBLEM)
  int numberTimeStepReduction=0;
  bool isSuccess = true;

  if (_pathFollowing){
    _pAl->computeLoadVector();
  }

  bool willFinish = false;

  while(fabs(curtime-dendtime) > tolend ){
    curtime+=dt;
    if(curtime > dendtime)
    {
      dt -= (curtime -dendtime);
      curtime = dendtime;
    }
    if (_outputFile)
      fprintf(_outputFile,"MICRO t= %e on %e \n",curtime,dendtime);
    // switch next step
    if (isSuccess){
      if (sAssembler)
        sAssembler->nextStep();
      _ipf->nextStep();
    }

    if (sAssembler)
      sAssembler->setTimeStep(dt);
    // Solve one step by NR scheme
    int niteNR = 0;
    if (_pathFollowing){
      pathSys->setPseudoTimeIncrement(dt);
      niteNR = microNewtonRaphsonPathFollowing(pAssembler,_ufield);

      double control = pathSys->getControlParameter();
      if (_outputFile)
        fprintf(_outputFile,"control variable %e \n",control);
      if (control>1. and fabs(control-1) >tolend){
        if (_outputFile)
          fprintf(_outputFile,"Load correction step\n");
        willFinish = true;
        nonsys->resetUnknownsToPreviousTimeStep();
        pathSys->setControlType(2);
        curtime = lasttime;
        dt = 1-pathSys->getControlParameter();
        dtmax = dt;
        isSuccess = false;
        continue;
      }
      else if (fabs(control-1) <tolend) {
        willFinish = true;
      }
      else if (control<0){
        Msg::Fatal("path following is not converge e= %d, gp = %d!",_enum,_gnum);
      }
    }
    else{
      this->setTimeForBC(curtime);
      this->setTimeForLaw(curtime,dt);
      if (_systemType == MULT_ELIM)
        niteNR = microNewtonRaphson(pAssembler,_ufield);
      else {
        if (_systemType == DISP_ELIM)
          _pAl->applyPBCByConstraintElimination();
        niteNR = NewtonRaphson(pAssembler,_ufield);
      }
    }

    if(niteNR == _maxNRite) // time step reduction
    {
       isSuccess = false;
       numberTimeStepReduction ++;
       if(numberTimeStepReduction > _maxAttemptStepReduction) // end of simulation
       {
         if (_outputFile)
          fprintf(_outputFile,"Simulation end due to convergence problem  element %d, IP %d !!!\n",
                  getElementSolverNumber(),getGaussPointSolverNumber());
         Msg::Fatal("Simulation end due to convergence problem  element %d, IP %d !!!",
                  getElementSolverNumber(),getGaussPointSolverNumber());
         break;
       }
       if (_outputFile)
        fprintf(_outputFile,"Convergence of Newton-Raphson failed at element %d, IP %d --> reduced time step",
                    getElementSolverNumber(),getGaussPointSolverNumber());
       Msg::Warning("Convergence of Newton-Raphson failed at element %d, IP %d --> reduced time step",
                    getElementSolverNumber(),getGaussPointSolverNumber());
       curtime = lasttime;
       dt /= _timeStepFactorReduction;
    }
    else
    {
      isSuccess = true;
      // save solution of previous as sucessfull convergence of NR process
      if(fabs(dtmax-dt) > toldtmax ) // increase time step if convergence in a few iteration
      {
        if(niteNR <= _niteTimeStepIncrease)
        {
           if (_outputFile)
            fprintf(_outputFile,"Time step is increased due to convergence of Newton-Raphson in less than %d at element %d, IP %d ",_niteTimeStepIncrease,
                        getElementSolverNumber(),getGaussPointSolverNumber());
           Msg::Warning("Time step is increased due to convergence of Newton-Raphson in less than %d at element %d, IP %d ",_niteTimeStepIncrease,
                        getElementSolverNumber(),getGaussPointSolverNumber());
           numberTimeStepReduction --;
           dt *= _timeStepFactorReduction;
        }
      }
      // next step

      if (_eigenFollowing){
        double* mode = NULL;
        double pre = this->critialPointTestFunction(curtime,mode);
        printf("Buckling criterion = %e\n",pre);
        if (_outputFile)
          fprintf(_outputFile,"Critial Point test function = %f \n",pre);
        this->modeViewToFile(_modeview,pre,ii+1);
        if ((pre < _instabilitycriterion) and _perturbate){
          this->perturbationProcedure(pre,mode);
        }
        if (mode) delete mode;
      }

      lasttime = curtime;
      if (!_multiscaleFlag){
        this->microPostSolveStep(curtime,ii+1);
      }
    }
    ii ++; // increase counter of iteration even if no convergence --> more archivage if no convergence (can be usefull)
    if(_pathFollowing){
      if (fabs(curtime-dendtime) < tolend){
        double control = pathSys->getControlParameter();
        if (control<1.){
          dendtime *=2.;
        }
      }
      if (willFinish) {
        dendtime = 1.;
        pathSys->setControlType(_macroControlType);
        break;
      }
    }
  }
  _iterMax = ii;
  return curtime;
};

void nonLinearMechSolver::microPostSolveStep(const double curtime, const int numstep){
    Msg::Info("Extract macro properties");
    this->extractAverageProperties(_tangentflag);

    this->pathFollowingArchiving(curtime,numstep,pAssembler);

    if (_stressflag){
      _currentState->_stress.print("Stress ");
      if (_highOrderFlag)
        _currentState->_highStress.print("hostress ");
    }

    if (_tangentflag){
       _currentState->_tangent.print("tangent");
    }

    if (_extractPerturbationToFile){
      _pAl->getPBCConstraintGroup()->writePertBoundaryToFile(pAssembler,_enum,_gnum,numstep);
    }

    if (_isWriteDeformedMeshToFile){
			this->writeDeformedMesh(numstep);
    }
    this->archiveData(curtime,numstep);
};


void nonLinearMechSolver::archiveData(const double curtime, const int numstep){
  // Archiving
  if (_outputFile)
    fprintf(_outputFile,"Archiving on element %d, IP point %d \n",getElementSolverNumber(),getGaussPointSolverNumber());
	double time = curtime;
	double step = numstep;

	if (_multiscaleFlag){
    _archivecounter++;
    time += (double)_archivecounter;
    step +=  _archivecounter;
	}

	if (_criterionFile != NULL){
    std::string name = "A";
    linearSystem<double>* lsys = pAssembler->getLinearSystem(name);
    nonLinearSystem<double>* nonsys = dynamic_cast<nonLinearSystem<double>*>(lsys);
    double val = nonsys->getInstabilityCriterion();
    fprintf(_criterionFile,"%e; %e; \n",time,val);
  }

  if (_archive){
    _energField->archive(time,step);
    _ufield->archive(time,step);
    _ipf->archive(time,step);
  }

  this->homogenizedDataToFile(time);

  staticDofManager<double>* sAssembler = dynamic_cast<staticDofManager<double>*>(pAssembler);
  if (sAssembler)
    this->forceArchiving(time,step,sAssembler);
};


double nonLinearMechSolver::solveMicroSolverStaticLinear(){
  /* time initialization */
  double curtime = 1.;
  double timestep = 1.;
  int step = 1;
  /* solving */
  this->setTimeForBC(curtime);
  this->setTimeForLaw(curtime,timestep);

  if (_systemType == DISP_ELIM)
    _pAl->applyPBCByConstraintElimination();
  else if (_systemType == DISP_MULT)
    _pAl->assembleRightHandSide();
  else if (_systemType == MULT_ELIM)
    _pAl->assembleConstraintResidualToSystem();

  this->computeExternalForces(_ufield);
  this->computeStiffMatrix(pAssembler);
  if (_outputFile)
    fprintf(_outputFile,"-- done assembling!\n");
  pAssembler->systemSolve();
  if (_outputFile)
    fprintf(_outputFile,"-- done solving!\n");
  _ipf->compute1state(IPStateBase::current,true);

   /* end of scheme */
  _iterMax = step;
  return curtime;
};

double nonLinearMechSolver::solveMicroSolverForwardEuler(){
  // external force vector // not change in function of time
	this->setTimeForBC(1.);
	_pAl->assembleRightHandSide();

  double dt = endtime/_numstepImpl;
  std::string Aname = "A";
  linearSystem<double>* lsys = pAssembler->getLinearSystem(Aname);

	double time = 0;
	for (int i=0; i<_numstepImpl; i++){
    _ipf->nextStep();
		double arclength = dt;
		time += dt;
		if (_outputFile)
      fprintf(_outputFile," iter = %d, t = %f\n",i+1,time);

		this->computeStiffMatrix(pAssembler);
    _pAl->assembleConstraintMatrix();
    forwardEulerArcLengthBase<double>* elsys = dynamic_cast<forwardEulerArcLengthBase<double>*>(lsys);
    elsys->setArcLengthControl(arclength);
    pAssembler->systemSolve();
    double control;
    elsys->getControlParameter(control);
    this->setTimeForBC(control);
    // update ipvariable
    _ipf->compute1state(IPStateBase::current,true);
    this->microPostSolveStep(time,i+1);
		lsys->zeroMatrix();
	};
};

double nonLinearMechSolver::microSolveStaticLinear(){
  initMicroSolver();
  double time = 0;
  if (_controlType == LOAD_CONTROL){
    time= solveMicroSolverStaticLinear();

    extractAverageProperties(_tangentflag);

    if (_stressflag)
    _currentState->_stress.print("stress");
    if (_tangentflag)
      _currentState->_tangent.print("tangent");
    this->endOfScheme(_ufield,_energField,time,_iterMax);
  }
  else if (_controlType == ARC_CONTROL_EULER)
    Msg::Fatal("This is not exist");


  Msg::Info("StaticLinear OK");
  return time;
};

double nonLinearMechSolver::microSolveSNL(){
  initMicroSolver();
  double time = 0;
  if (_controlType == LOAD_CONTROL or _pathFollowing)
    time = solveMicroSolverSNL();
  else if (_controlType == ARC_CONTROL_EULER)
    time = solveMicroSolverForwardEuler();

  this->endOfScheme(_ufield,_energField,time,_iterMax);
  Msg::Info("StaticNonLinear OK");
  return time;
};

int nonLinearMechSolver::pathFollowingPerturbation(dofManager<double> *pmanager,unknownField *ufield){
  std::string name = "A";
  linearSystem<double>* lsys = pmanager->getLinearSystem(name);
  pbcSystem<double> * pbcSys = dynamic_cast<pbcSystem<double>*>(lsys);
  nonLinearSystem<double>* nlsys = dynamic_cast<nonLinearSystem<double>*>(lsys);
	staticDofManager<double>* staticAssembler = static_cast<staticDofManager<double>*>(pmanager);
	pathFollowingSystem<double>* pathsys = dynamic_cast<pathFollowingSystem<double>*>(lsys);

  // compute ipvariable
  _ipf->compute1state(IPStateBase::current, true);
  double normFinfInit = this->computeRightHandSide(pmanager,ufield);

  // loop until convergence
  int iter=0;
  if(normFinfInit <_tol){ // no force (can append if contact )
    if (_outputFile)
      fprintf(_outputFile,"NO FORCE GO TO NEXT STEP\n");
      Msg::Error("perturbation is too small!");
    return 0;
  }
  double relnorm = 1;

  if (_outputFile)
    fprintf(_outputFile,"MICROITERATION n %d : RESIDU : %e \n",iter,relnorm);

  while (relnorm>_tol){
    iter++;
    // Solve KDu = Fext-Fint
    int succeed = pathsys->solvePerturbedSystem();
    if(!succeed)
    {
      Msg::Fatal("solution is not converged in perturbed system");
      iter = _maxNRite;
      break;
    }

    // update ipvariable
    _ipf->compute1state(IPStateBase::current,true);

    // break in case of non-iterative procedure
    if (!_iterativeNR) break;

    // check convergence criterion
    double normFinf = this->computeRightHandSide(pAssembler,ufield);
    relnorm = normFinf/normFinfInit;
    if (_outputFile)
      fprintf(_outputFile,"MICROITERATION n %d : RESIDU : %e \n",iter,relnorm);
    if((iter == _maxNRite) or isnan(relnorm))
    {
      // reset system value
      Msg::Fatal("solution is not converged in perturbed system");
      iter = _maxNRite; // for isnan case
      break;
    }
  }
  return iter;
};

int nonLinearMechSolver::microNewtonRaphson(dofManager<double> *pmanager,unknownField *ufield){
  std::string name = "A";
  linearSystem<double>* lsys = pmanager->getLinearSystem(name);
  pbcSystem<double> * pbcSys = dynamic_cast<pbcSystem<double>*>(lsys);
  nonLinearSystem<double>* nlsys = dynamic_cast<nonLinearSystem<double>*>(lsys);
	staticDofManager<double>* staticAssembler = static_cast<staticDofManager<double>*>(pmanager);

  // compute ipvariable
  _ipf->compute1state(IPStateBase::current, true);
  double normFinfInit = this->computeRightHandSide(pmanager,ufield);

  // loop until convergence
  int iter=0;
  if(normFinfInit <_tol){ // no force (can append if contact )
    if (_outputFile)
      fprintf(_outputFile,"NO FORCE GO TO NEXT STEP\n");
    return 0;
  }
  double relnorm = 1;

  if (_outputFile)
    fprintf(_outputFile,"MICROITERATION n %d : RESIDU : %e \n",iter,relnorm);

  while (relnorm>_tol){
    iter++;
    // Solve KDu = Fext-Fint
    int succeed = staticAssembler->systemSolveIntReturn();
    if(!succeed)
    {
      staticAssembler->resetUnknownsToPreviousTimeStep();
      iter = _maxNRite;
      break;
    }

    // update ipvariable
    _ipf->compute1state(IPStateBase::current,true);

    // break in case of non-iterative procedure
    if (!_iterativeNR) break;

    // check convergence criterion
    double normFinf = this->computeRightHandSide(pAssembler,ufield);
    relnorm = normFinf/normFinfInit;
    if (_outputFile)
      fprintf(_outputFile,"MICROITERATION n %d : RESIDU : %e \n",iter,relnorm);
    if((iter == _maxNRite) or isnan(relnorm))
    {
      // reset system value
      staticAssembler->resetUnknownsToPreviousTimeStep();
      iter = _maxNRite; // for isnan case
      break;
    }
  }
  return iter;
}

int nonLinearMechSolver::microNewtonRaphsonPathFollowing(dofManager<double> *pmanager,unknownField *ufield){
  std::string name = "A";
  linearSystem<double>* lsys =pmanager->getLinearSystem(name);
  nonLinearSystem<double>* nonsys = dynamic_cast<nonLinearSystem<double>*>(lsys);
  pathFollowingSystem<double>* pathSys = dynamic_cast<pathFollowingSystem<double>*>(lsys);
  staticDofManager<double>* staticAssembler = static_cast<staticDofManager<double>*>(pmanager);

  // copy state from previous
  _ipf->compute1state(IPStateBase::current,true);
  double normFint = this->computeRightHandSide(pmanager,ufield);
  double norm0 = 0.;

  // loop until convergence
  int iter=0;
  double relnorm = 1;
  if (_outputFile)
    fprintf(_outputFile,"iteration n %d : residu : %e \n",iter,relnorm);
  while (relnorm>_tol){

    iter++;
    int succeed = staticAssembler->systemSolveIntReturn();
    if(!succeed)
    {
      staticAssembler->resetUnknownsToPreviousTimeStep();
      iter = _maxNRite;
      break;
    }
    // update ipvariable
    this->setTimeForBC(pathSys->getControlParameter());
    _ipf->compute1state(IPStateBase::current,true);

    if (!_iterativeNR) break;
    normFint = this->computeRightHandSide(pmanager,ufield);
    if (norm0 == 0.) norm0 = nonsys->norm0Inf();
    relnorm = normFint/norm0;

    if (_outputFile)
    fprintf(_outputFile,"iteration n %d : residu : %e \n",iter,relnorm);

    if((iter == _maxNRite) or isnan(relnorm))
    {
      // reset system value
      staticAssembler->resetUnknownsToPreviousTimeStep();
      iter = _maxNRite; // for isnan case
      break;
    }
  }
  return iter;
};


void nonLinearMechSolver::setPerturbationFactor(const double fact){
  _perturbationfactor = fact;
};

void nonLinearMechSolver::setInstabilityCriterion(const double val){
  _instabilitycriterion = val;
}
void nonLinearMechSolver::perturbateBucklingModeByMinimalEigenValueMode(const bool flag){
  _perturbate = flag;
};

void nonLinearMechSolver::setModeView(const int view){
	_modeview.push_back(view);
	std::string str = "Mode shape" + int2str(view);
	std::vector<nlsField::dataBuildView> tempview;
	tempview.push_back(nlsField::dataBuildView(str,-1,nlsField::crude,1,false));
	_eigview.push_back(tempview);
};

void nonLinearMechSolver::modeViewToFile(std::vector<int>& num, double time, int step){
	if (_eigflag){
		for (int i=0; i<num.size(); i++){
			printf("printting mode %d to file \n", num[i]);
			_eigfield[i]->archive(time,step);
		};
	}
	else{
		Msg::Info("Eigen solver is not activated");
	};
};

void nonLinearMechSolver::tangentAveragingFlag(const bool fl){
	_tangentflag = fl;
	#ifdef _DEBUG
	if (_tangentflag) Msg::Info("Tangent averaging is activated");
	else Msg::Info("Tangent averaging is desactivated");
	#endif
};

void nonLinearMechSolver::stressAveragingFlag(const bool fl){
	_stressflag = fl;
	#ifdef _DEBUG
	if (_stressflag) Msg::Info("Stress averaging is activated");
	else Msg::Info("Stress averaging is desactivated");
	#endif
};

void nonLinearMechSolver::setStressAveragingMethod(const int method){
  _homogenizeStressMethod = STRESS_HOMO_TYPE(method);
};
void nonLinearMechSolver::setTangentAveragingMethod(const int method, const double prec){
  _homogenizeTangentMethod = (TANGENT_HOMO_TYPE)method;
  _tangentPerturbation = prec;
};

void nonLinearMechSolver::eigenValueSolver(const int num , const bool fl){
	_numeigenvalue = num;
  _eigflag = fl;
  #ifdef _DEBUG
  if (_eigflag) Msg::Info("Eigen solver is activated");
  else Msg::Info("Eigen solver is desactivated");
  #endif
};


void nonLinearMechSolver::setMicroProblemIndentification(int ele, int gpt){
	_enum = ele;
	_gnum = gpt;
};
int nonLinearMechSolver::getElementSolverNumber() const{
	return _enum;
};
int nonLinearMechSolver::getGaussPointSolverNumber() const{
	return _gnum;
};

void nonLinearMechSolver::setHomogenizationPropertyArchiveFlag(const bool flg){
  _isHommProSaveToFile = flg;
};
void nonLinearMechSolver::setDisplacementAndIPArchiveFlag(const bool flg){
  _archive = flg;
}
void nonLinearMechSolver::setStrainArchiveFlag(const bool flg){
  _isHommStrainSaveToFile = flg;
};

void nonLinearMechSolver::setOrder(const int ord){
  if (ord >1) _highOrderFlag = true;
  else if (ord ==1) _highOrderFlag = false;
  printf("Order of homogenization %d \n",ord);
  for (int i=0; i<allMicroBC.size(); i++){
    allMicroBC[i]->setOrder(ord);
  };
};

void nonLinearMechSolver::setExtractPerturbationToFileFlag(const bool flag){
  _extractPerturbationToFile = flag;
};

void nonLinearMechSolver::setSystemType(const int i){
  _systemType = (SYSTEM_TYPE)i;
};
void nonLinearMechSolver::setControlType(const int i){
  _controlType = (CONTROL_TYPE)i;
};
void nonLinearMechSolver::setMicroSolverFlag(const bool flag){
  _microFlag = flag;
};

void nonLinearMechSolver::setEigenSolverFollowing(const bool f){
  _eigenFollowing = f;
}

void nonLinearMechSolver::setMultiscaleFlag( const bool flag){
  _multiscaleFlag = true;
};

void nonLinearMechSolver::readPBCDataFromFile(const std::string fn){
  if (_pbcNodeData != NULL) fclose(_pbcNodeData);
  _pbcNodeData = NULL;

  _pbcNodeData = fopen(fn.c_str(),"r");
  if (_pbcNodeData == NULL){
    Msg::Error("Unable to open file %s",fn.c_str());
  }
};


void nonLinearMechSolver::saveStiffnessMatrixToFile(const int iter){
linearSystem<double>* lsys=NULL;
std::string name="A";
lsys=pAssembler->getLinearSystem(name);
#if defined(HAVE_PETSC)
linearSystemPETSc<double>* lpet=dynamic_cast<linearSystemPETSc<double>*>(lsys);
if(lpet==NULL)return;
else{
	Mat& a = lpet->getMatrix();
	MatToFile(a,"data/Mat_Stiffness.txt");
}
implicitHulbertChungPetsc<double>* hcsys=dynamic_cast<implicitHulbertChungPetsc<double>*>(lsys);
if(hcsys==NULL)return;
else{
	Mat& b = hcsys->getMassMatrix();
	MatToFile(b,"data/Mat_Mass.txt");
}
#endif
};


void nonLinearMechSolver::setWriteDeformedMeshToFile(bool flag){
  _isWriteDeformedMeshToFile = flag;
};

void nonLinearMechSolver::writeDeformedMesh(int step){
  Msg::Info("Begin writing deformed mesh");
  GModel* dispgmodel = new GModel();
  dispgmodel->readMSH(_meshFileName.c_str());
  std::set<MVertex*> computedVertex;

  for (int i=0; i<domainVector.size(); i++){
  	partDomain* dom = domainVector[i];
    FunctionSpaceBase* sp = dom->getFunctionSpace();
    groupOfElements* g = dom->g;
    for (groupOfElements::elementContainer::iterator it = g->begin(); it!= g->end(); it++){
      MElement* e = *it;
      std::vector<Dof> keys;
      sp->getKeys(e,keys);
      // get displacement results
      std::vector<double> vals;
      pAssembler->getDofValue(keys,vals);
      for (int iver =0; iver< e->getNumVertices(); iver++){
        MVertex* v = e->getVertex(iver);
				MVertex* vdisp = dispgmodel->getMeshVertexByTag(v->getNum());
				// modify directly vertex coordinates
        if (computedVertex.find(vdisp) == computedVertex.end()){
          computedVertex.insert(vdisp);
          double x = vals[iver];
          double y = vals[iver+1*e->getNumVertices()];
          double z = 0.;
          if (_dim == 3)
            z = vals[iver+2*e->getNumVertices()];
          vdisp->x() += x;
          vdisp->y() += y;
          vdisp->z() += z;
    		}
    	}
 	 	}
  }
  // write deformed mesh to file
  std::string filename = "deformed_mesh_"+int2str(step)+ ".msh";
  dispgmodel->writeMSH(filename, CTX::instance()->mesh.mshFileVersion,CTX::instance()->mesh.binary, CTX::instance()->mesh.saveAll,CTX::instance()->mesh.saveParametric, CTX::instance()->mesh.scalingFactor);
  computedVertex.clear();
  delete dispgmodel;
	Msg::Info("End writing deformed mesh");
};
