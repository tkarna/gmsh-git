//
//
// Description: Non linear solver for mechanic problems
//              quasi-static implicit scheme & dynamic explicit Hulbert-Chung scheme
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef _NONLINEARMECHSOLVER_H_
#define _NONLINEARMECHSOLVER_H_

#ifndef SWIG
#include <map>
#include <string>
#include "SVector3.h"
#include "dofManager.h"
#include "simpleFunction.h"
#include "simpleFunctionTime.h"
#include "functionSpace.h"
#include "MInterfaceElement.h"
#include "groupOfElements.h"
#include "partDomain.h"
#include "explicitHulbertChung.h"
#include "contactDomain.h"
#include "NonLinearSolverConfig.h"
#include "eigenSolver.h"
#include "periodicBoundaryCondition.h"
#include "nonLinearMicroBC.h"
#include "ipField.h"
#include "unknownField.h"
#include "strainMapping.h"
#include "homogenizedData.h"

class PView;
class groupOfElements;
class binding;
class unknownDynamicField;
class energeticField;
template<class T1,class T2> class DgC0BilinearTerm;
template<class T1> class DgC0LinearTerm;

class archiveForce{
 public:
  int numphys;
  int dim;
  int comp;
  int nstep;
  FILE *FP;
  std::set<Dof> vdof; // With a set we are sur that a Dof will not be archived twice
  //std::string fname;
  double fval; // tempory to get value from system
  archiveForce() : numphys(0), dim(0), comp(0), nstep(1), fval(0.), FP(NULL){}
  archiveForce(const archiveForce &source) : numphys(source.numphys), dim(source.dim),
                                             comp(source.comp), nstep(source.nstep),
                                             fval(source.fval),
                                             FP(source.FP)
  {
    for(std::set<Dof>::const_iterator itD = source.vdof.begin(); itD!= source.vdof.end(); ++itD)
    {
      vdof.insert(*itD);
    }
  }
  void openFile()
  {
    if(FP!=NULL) fclose(FP);
    // create file only if there is dof to archive
    if(vdof.size()!=0){
      std::ostringstream oss;
      oss << numphys;
      std::string s = oss.str();
      oss.str("");
      oss << comp;
      std::string s2 = oss.str();
     #if defined(HAVE_MPI)
      if(Msg::GetCommSize() != 1){
        oss.str("");
        oss << Msg::GetCommRank();
        s2 += "_part"+oss.str();
      }
     #endif // HAVE_MPI
      std::string fname = "force"+s+"comp"+s2+".csv";
      FP = fopen(fname.c_str(),"w");
    }
    #if defined(HAVE_MPI)
    else if(Msg::GetCommSize() == 1)
    {
      Msg::Warning("Can't archive forces on physical %d because it contains no dofs!",numphys);
    }
    #endif
  }

  archiveForce(int np, int d, int c,int ns) : numphys(np), dim(d), comp(c), nstep(ns), fval(0.), FP(NULL){}
  ~archiveForce(){}
  void closeFile(){
    if(FP!=NULL) fclose(FP);
    FP=NULL;
  }
};

struct groupTwoElements{
  MElement* eLeft, *eRight;
  int comp;
};

class archiveMeanValue{
  public:
    int comp;
    std::string name;
    FILE* file;
    bool isInit;


    archiveMeanValue(const int cp, const std::string n ):comp(cp),name(n),file(NULL),isInit(false){};
    ~archiveMeanValue(){};
    void openFile(std::vector<partDomain*>& allDomain){
      std::string s2 = "";
      #if defined(HAVE_MPI)
      if(Msg::GetCommSize() > 1){
        std::ostringstream oss;
        oss.str("");
        oss << Msg::GetCommRank();
        s2 += "_part"+oss.str();
      }
      #endif // HAVE_MPI
      std::string fname = "meanValue"+name+s2+".csv";
      if (isInit == false){
        isInit = true;
        if (file != NULL) fclose(file);

        file = fopen(fname.c_str(),"w");
        fprintf(file,"Time;");
        for (int i=0; i<allDomain.size(); i++){
          fprintf(file,"Physical%d ;",allDomain[i]->getPhysical());
        }
        fprintf(file,"\n");
      }
      else{
        file = fopen(fname.c_str(),"a");
      }
    }

    void closeFile(){
      if (file != NULL) fclose(file);
      file = NULL;
    };
};
#endif // SWIG


class nonLinearMechSolver
{
 public:
  typedef std::set<contactDomain*> contactContainer;
 #ifndef SWIG
  enum solver{ Gmm=0,Taucs=1,Petsc=2, GMRESk=3};
  enum scheme{StaticLinear=0, StaticNonLinear=1, Explicit=2, Multi=3, Implicit=4};
 #endif //SWIG
 protected:
  GModel *pModel;
  int _dim, _tag;
  int _mpirank,_mpitotalrank, _mpiUserDom;
  bool _mpiDofManager; // true -> parallelization of dofManager. By default = true if Msg::GetCommSize() > 1
  dofManager<double> *pAssembler;
  // IPField declared here to allow to pass it from one scheme to an other scheme
  IPField* _ipf;

//  dgGroupCollection _groups;
  // specific data
  int _numstepImpl, _numstepExpl; // Number of step not used for StaticLinearScheme and number of step between time step evaluation for Explicit Scheme
  unsigned long int _currentStep; // As it is used in different functions now (Onelab control)
  double endtime; // final time not used for StaticLinearScheme but it is not necesary to derive class because (small useless data)
  double _tol; // relative tolerance for iteration not used for StaticLinearScheme but it is not necesary to derive class because (small useless data)
  int nsba; // number of step between 2 archiving (=1 by default)
  // adaptative time step if no convergence for Static Non Linear Scheme
  int _maxNRite; // if this number of iteration is reached the time step is reduce
  double _timeStepFactorReduction; // the time step is divided by this value if no convergence (default 2)
  int _maxAttemptStepReduction; // number of attemp to reduce time step (default 6 which leads to a decrease by 64 (2^6) of time step)
  int _niteTimeStepIncrease; // if convergence with nite < _nite the time step is increase (default 5)(but can be greater than its max value given by numstep/endtime)

  /* data for restart */
  size_t _beginTime;
  double _secondToRestart,_interRestart;
  bool _isaRestart,_checkRestart;

  /* specific data for explicit scheme */
  double _beta, _gamma, _alpham, _alphaf, _gammas, _rhoinfty;
  bool _timeStepByBenson;
  std::map<int,MElement*> _mapMElementTmp; // used to compute characteristique element size in the deformed configuration CLEAR THE MAP ???
  std::map<int,MElement*> _mapMElementFirstOrder; // idem

  /* crack tracking */
  FILE *_crackTrackingFile; // name of files for crack tracking no name == NULL and no track

  /* fragmentation */
  FILE *_fragmentationFile;

  std::vector<partDomain*> domainVector;
  // contact
  contactContainer _allContact;
  // neumann BC
  std::vector<nonLinearNeumannBC> allNeumann;
  // dirichlet BC
  std::vector<nonLinearDirichletBC> allDirichlet;

  // all periodic groups
  std::vector<nonLinearPeriodicBCBetweenTwoGroups> allPeriodic;
  // all samedisp bc
  std::vector<nonLinearSameDisplacementBC> allSameDisp;
  // fix on face
  std::vector<nonLinearFixOnFaceBC> allFixAllFace;

  // constraint BC
  std::vector<nonLinearConstraintBC> allConstraint;
  std::vector< std::list<Dof> > allConstraintDof;// all dof to be constraint for all constraintBC

  // initial BC
  std::vector<initialCondition> allinitial;

  // neumann BC theta (weak enforcement of rotation) group this with allNeumann ?
  std::vector<nonLinearNeumannBC> allTheta;

  // dirichlet BC on rigid surface (prescribed the motion of gravity center)
  std::vector<rigidContactBC> allContactBC;

  // set with virtual interfaceElements used to prescribed BC put this in an other place ??
  std::map<int,groupOfElements> mapvinter;

  // vector with material law
  std::map<int,materialLaw*> maplaw;
  // physical entities that are initialy broken
  std::vector<int> initbrokeninter;
  // std vector to archive a node displacement
  std::vector<unknownField::archiveNode> anoded;
  // std::vector to archive a force (info musy be store before creation because functionSpace are not initialize)
  std::vector<archiveForce> vaf;
  // std vector to archive ipvariable
  std::vector<IPField::ip2archive> vaip;
  // msh view One archiving vector is needed by field Change THIS ??
  std::vector<nlsField::dataBuildView> unknownView;
  std::vector<nlsField::dataBuildView> ipView;
  std::vector<nlsField::dataBuildView> energyView;
  int _energyComputation, _fractureEnergyComputation; // step interval to compute total energy (never if 0)

  /* SWITCH scheme data */
  bool _previousInit; // To known if the initialization as already be made by an other scheme
  bool _unresetedBC; // To known if the BC are modified.
  bool _unresetedContact; // To known if the contact interaction are modified
  std::string _meshFileName; // To transfert the mesh file from one folder to an other one

  /* Dynamic relaxation */
  bool _dynamicRelaxation; // performed explicit dynamic relaxtion

  /*Path following option with arc-length control*/
  bool _pathFollowing;
  int _macroControlType;

  /*pour save the mean value list on domain */
  std::vector<archiveMeanValue> _archiveMean;

  #if defined(HAVE_MPI)
  std::map<int,std::vector<groupOfElements> >_mapOtherDomain; // map build by the creation of groupOfElement. Retrieve the element of other partition
  #endif // HAVE_MPI
  std::vector<partDomain*> _ghostDomainMPI; // to prescribed Dirichlet BC of other partition in MPI (avoid search in map in each iteration)

  /* MultiSystems solve */
  std::vector<int> _vcompBySys;
  std::vector<scheme> _vschemeBySys;

  bool _stiffEstimation, _stiffnessModification; // true if recalculate stiffness matrix
  bool _iterativeNR; // true if using iterative procedure
  bool _lineSearch;
  // for multiscale analysis
  std::map<int,std::set<int> > _mapRanks;
  strainMapping* _strainMap;
  bool _strainMappingFollwing;
  // path following archive
  FILE* _controlStateFile;
  // For Onelab display
  mutable int _GmshOneLabViewNum;

#ifndef SWIG
  void commonModel(); // common operation for loadModel and createModel
  void initMapRanks(const std::set<int>& parts);
  void init();
  void init2(unknownField* &ufield, energeticField* &efield); // Has to be regrouped with init() Van Dung ??
  void initTerms(unknownField *ufield, IPField *ipf);
  void endOfScheme(unknownField *ufield,energeticField *efield,const double endtime, const int endstep); // common operation at the end of a scheme
  void oneStepPreSolve(const double curtime, const double timestep);
  void computeIPCompOnDomain(const double time);
  void oneStepPostSolve(unknownField* ufield, energeticField* efield,const double curtime, const int numstep);
  void fillMapOfInterfaceElementsInOneDomain(MElement *e, std::vector<MElement*> &eleFound,
                                const groupOfElements *g) const;
  void setPairSpaceElementForBoundaryConditions(); // new implementation

  // Function used by non linear solver (return the number of iteration to converge)
  int NewtonRaphson(dofManager<double> *pmanager, unknownField *ufield);

  /*
  NewtonRaphson function for arc-length path following
  */
  int NewtonRaphsonPathFollowing(dofManager<double> *pmanager, unknownField *ufield);

  double computeNorm0(linearSystem<double> *lsys, dofManager<double> *pAssembler, unknownField *ufield,
                      IPField *ipf);


  double computeRightHandSide(dofManager<double> *pAssembler, unknownField *ufield);
  void computeLoadVector(const unknownField *ufield);
  void computeStiffMatrix(dofManager<double>* pmanager);
  void computeExternalForces(const unknownField *ufield);
  void computeContactForces();
  void computeInternalForces(unknownField *ufield);
  void fixNodalDofs();
  void numberDofs();
  void insertTheta(const int numphys, groupOfElements *goe);
  void initMapMElementToComputeTimeStep();
  double criticalExplicitTimeStep(unknownField *ufield);
  void setInitialCondition();
  void setTimeForBC(double time);
  void setTimeForLaw(const double t,const double dt);
  void initArchiveForce();
  void endArchiveForce();
  void forceArchiving(const double curtime,const int numstep,const dofManager<double>* pAssembler);

  void initArchivePathFollowing();
  void endArchivePathFollowing();
  void pathFollowingArchiving(const double curtime, const int numstep, dofManager<double>* assembler);

  void initContactInteraction();
  void crackTracking(const double curtime,unknownField *ufield,IPField *ipf); // 2D only for now !!!
  void postproFragment(IPField *ipf);

  void applyPBCBetweenTwoGroups(dofManager<double>* p);
  void applySameDispBC(dofManager<double>* p);
  void applyFixOnFace(dofManager<double>* p);
  bool isMultiscale() const;
  bool isDgDomain() const;
  // For Onelab
  void createOnelabViewOption(const std::string &fname,const int displayType) const;

#endif // SWIG
 protected:
  solver whatSolver; //  Solver used to solve
  scheme whatScheme; // scheme used to solve equation
  scheme _previousScheme; // to known the previous used scheme for switch
 public:
  nonLinearMechSolver(int tag,bool isParallel=true);
  virtual ~nonLinearMechSolver();
#ifndef SWIG
  int getDim() const {return _dim;};
  int getStepBetweenArchiving() const{return nsba;}
  double getEndTime() const{return endtime;}
  scheme getScheme() const{return whatScheme;}
  virtual linearSystem<double>* createSNLSystem();
  virtual linearSystem<double>* createExplicitSystem();
  virtual linearSystem<double>* createImplicitSystem();
  virtual void createSystem();

  virtual double solveExplicit();
  virtual double solveMulti();
  virtual void createInterfaceElement();
  virtual materialLaw* getMaterialLaw(const int num);
  // create interfaceelement with dgGoupOfElement from dg project doesn't work (segmentation fault)
  virtual void createInterfaceElement_2();
//  virtual void restart(unknownField *ufield);
  virtual bool createRestart();
#endif
  virtual double solveStaticLinear();
  virtual double solveSNL();
  virtual void perturbationProcedure(const double pre, double* mode);
  // functions for python (swig) interaction
  void loadModel(const std::string &meshFileName);
  // experimental could change for // which is not supported now
  void createModel(const std::string &geoFileName, const std::string &outFileName="",const int dim=3,const int order=1,
                   const bool incomplete=false);
  virtual double solve(); // return the time reaches at the end of the computation can be !=endtime if solving problem
  virtual void addDomain(partDomain *dom);
  virtual void addMaterialLaw(materialLaw *mlaw);
  void Solver(const int s){whatSolver= (solver)s;}
  void Scheme(const int s){whatScheme=(scheme)s;}
  void lineSearch(const bool line);
  void pathFollowing(const bool p);
  void setPathFollowingControlType(const int i);
  void stiffnessModification(const bool flag = true);
  void iterativeProcedure(const bool flag = true);
  void snlData(const int ns, const double et, const double reltol);
  void snlManageTimeStep(const int miteNR,const int iteIncreaseTS, const double redfactor, const int maxAttemptRedFactor);
  void addSystem(const int ncomp,const int s);
  void switchSystem(const int sysindex,const int s); // first sysindex => sysnum =0
  void explicitData(const double ftime, const double gams=0.666667, const double beta=0, const double gamma=0.5, const double alpham=0.,const bool benson=false);
  void implicitData(const double beta, const double gamma, const double alpham , const double alphaf);
  void explicitSpectralRadius(const double ftime,const double gams=0.666667,const double rho=1.,const bool benson=false);
  void implicitSpectralRadius(const double rho);
  void explicitTimeStepEvaluation(const int nst);
  void stepBetweenArchiving(const int n);
  virtual void thetaBC(const int numphys);
  virtual void displacementBC(std::string onwhat, const int numphys, const int comp, const double value);
  virtual void displacementBC(std::string onwhat, const int numphys, const int comp, const double value,elementFilter *filter);
  virtual void displacementBC(std::string onwhat, const int numphys, const int comp, simpleFunction<double> *fct);
  virtual void displacementBC(std::string onwhat, const int numphys, const int comp, elementFilter *filter, simpleFunction<double> *fct);
  virtual void velocityBC(std::string onwhat, const int numphys, const int comp, const double value);
  virtual void velocityBC(std::string onwhat, const int numphys, const int comp, const double value,elementFilter *filter);
  virtual void velocityBC(std::string onwhat, const int numphys, const int comp, simpleFunction<double> *fct);
  virtual void velocityBC(std::string onwhat, const int numphys, const int comp, elementFilter *filter, simpleFunction<double> *fct);
  virtual void constraintBC(std::string onwhat, const int numphys, const int comp);

  virtual void setmeanValueOnDomain(const std::string vname,const int comp);
  virtual void periodicBC(std::string onwhat, const int phys1, const int phys2, const int comp = -1,
                            const double x=0, const double y=0, const double z = 0);
  virtual void sameDisplacementBC(std::string onwhat, const int phy, const int rootphy,
                                  const int comp);
  virtual void fixOnFace(std::string onwhat, const int phy, const double A, const double B, const double C, const double D);
  virtual void displacementRigidContactBC(const int numphys, const int comp,const double value);
  virtual void displacementRigidContactBC(const int numphys, const int comp_, simpleFunction<double> *fct);
  virtual void initialBC(std::string onwhat, std::string whichC, const int numphys, const int comp, const double value);
  virtual void initialBC(std::string onwhat, std::string whichC, const int numphys, const int comp, const double value, elementFilter *filter);
  virtual void initialBC(std::string onwhat, std::string whichC, const int numphys, const int comp, simpleFunctionTime<double> *fct,elementFilter* filter=NULL);
  virtual void initialDCBVeloBC(std::string onwhat,const int numphys,const int axiscomp,const int deflcomp,const double length, const double value);
  virtual void forceBC(std::string onwhat, const int numphys, const int comp, const double val);
  virtual void forceBC(std::string onwhat, const int numphys, const int comp, simpleFunctionTime<double> *fct);
  virtual void independentDisplacementBC(std::string onwhat, const int numphys, const int comp, const double value);
  virtual void independentForceBC(std::string onwhat, const int numphys, const int comp, const double val);
  virtual void blastPressureBC(const int numphys,const double p0,const double p1, const double plexp, const double t0, const double t1);
  virtual void pressureOnPhysicalGroupBC(const int numphys, const double press, const double p0);
  virtual void fillConstraintDof();
  virtual void applyConstraintBC(dofManager<double>* p);
  virtual void archivingForceOnPhysicalGroup(const std::string onwhat, const int numphys, const int comp,const int nstep=1);
  virtual void archivingNodeDisplacement(const int num, const int comp,const int nstep=1);
  virtual void archivingNodeVelocity(const int num, const int comp,const int nstep=1);
  virtual void archivingNodeAcceleration(const int num, const int comp,const int nstep=1);
  virtual void archivingNode(const int num, const int comp, nonLinearBoundaryCondition::whichCondition,const int nstep=1);
  virtual void archivingNode(const int numpphys, const int dim, const int comp, nonLinearBoundaryCondition::whichCondition,const int nstep);
  virtual void archivingNodeIP(const int numphys, const int ipval,const int elemval,const int nstep=1);
  virtual void archivingIPOnPhysicalGroup(std::string onwhat, const int numphys, const int ipval, const int elemval,
                      const int nstep=1);
  virtual void physInitBroken(const int phys);
//  virtual void setInitOrRestartFileName(const std::string fname);
  virtual void contactInteraction(contactDomain *cdom);
  virtual void archivingRigidContact(const int numphys, const int comp, const int wc=0,const int nstep=1);
  virtual void unknownBuildView(const int comp=0,const int nbstep=1);
  virtual void internalPointBuildView(const std::string vname,const int comp,const int nbstep=1,const int ev=-1);
  virtual void energyBuildView(const std::string vname,const int comp,const int nbstep=1);
  virtual void energyComputation(const int val=1);
  virtual void fractureEnergy(const int val=0);
  virtual void crackTracking(std::string fname);
  virtual void postproFragment(std::string fname);
  virtual void resetBoundaryConditions();
  virtual void resetContactInteraction();
  virtual void setFactorOnArchivingFiles(const int fact); // only for csv files (for view use nstepBetweenArchiving)
  virtual void dynamicRelaxation(const double gams,const double ftime,const double tol=1.e-6,const int wsolver=0,const bool benson=false);
  virtual void createRestart(const int day,const int hour=0,const int minute=0,const int second=0);

  virtual void createStrainMapping(const std::string  filename);
  virtual void setStrainMappingFollowing(const bool flag);

  /** To control via Onelab **/
  // Explicit scheme control
  virtual void initializeExplicitScheme();
  virtual double oneExplicitStep(const double curtime);
  virtual double finalizeExplicitScheme(const double curtime);
  // Display control
  virtual void createOnelabDisplacementView(const std::string &fname,const double time) const;
  virtual void createOnelabVelocityView(const std::string &fname,const double time) const;
  // Onelab can only access to dof that are archived in txt files
  // They are given in the order defined by the user in his/her python files
  virtual void getOnelabArchiveNodalUnknowns(fullVector<double> &nodalValues) const;

// for periodic BC
 public:
  /**
   * 3 method for constraint treatment
   * DISP_ELIM = displacement elimination
   * MULT_ELIM = multiplier elimination
   * DISP_MULT = displacement + multiplier system, no elimination
   */
  enum SYSTEM_TYPE{DISP_ELIM=0, MULT_ELIM = 1, DISP_MULT=2};
   /**
     * homogenized stress method by volume or surface integral
     */
  enum STRESS_HOMO_TYPE{VOLUME=0, SURFACE=1};
  /**
    * homogenized tangent by perturbation of by condensation
    */
  enum TANGENT_HOMO_TYPE{PERTURB=0, CONDEN=1};
  /**
    * if  LOAD_CONTROL , solve until time = 1.
    * if ARC_CONTROL_EULER, solve by explicit Euler and only at microlevel
    */
  enum CONTROL_TYPE{LOAD_CONTROL=0, ARC_CONTROL_EULER=1};
 protected:
 #ifndef SWIG
  //for micro flag
  SYSTEM_TYPE _systemType;
  CONTROL_TYPE _controlType;
  // micro flag--> true if microsolver is used
  bool _microFlag, _multiscaleFlag;
  // Element number and Integration point number
  int _enum, _gnum;
  // for periodicity == RVE dimension
  SVector3  _Lx, _Ly, _Lz;
  //micro BC
  std::vector<nonLinearMicroBC*> allMicroBC;
  //stress flag and tangent flag
  bool _stressflag, _tangentflag;
  // homogenized density
  double _rho;
  // homogenized stresses

  double _sameStateCriterion; // this parameter to check if same sate-> economise

  homogenizedData _state1, _state2;
  homogenizedData* _currentState, *_initialState;
  bool _whichState;

  // highorder flag : true if using second order multiscale analysis
  bool _highOrderFlag;
  // homogenized  method
  STRESS_HOMO_TYPE _homogenizeStressMethod;
  TANGENT_HOMO_TYPE _homogenizeTangentMethod;
  // class for PBC by constraint elimination
  periodicConstraintsBase* _periodicBC;
  // for managing periodic boudary condition
  pbcAlgorithm* _pAl;
  // for tangent estimation
  stiffnessCondensation* _condensation;
  // unknown field
  unknownField* _ufield;
  // energetic field
  energeticField* _energField;
  // eigensolver using for stability analysis
  bool _eigflag;
  eigenSolver* _eigenSolver;
  std::vector<std::vector<nlsField::dataBuildView> > _eigview;
  std::vector<unknownField*> _eigfield;
  std::vector<int> _modeview;
  int _numeigenvalue;
  double _perturbationfactor, _instabilitycriterion;
  bool _eigenFollowing, _perturbate;

  // for archiving
  bool _archive, _messageView;
  int _iterMax;
  int _archivecounter;
  // all homogenized filename

  bool _isHommProSaveToFile, _isHommStrainSaveToFile, _extractPerturbationToFile;

  homogenizedDataFiles _homogenizedFiles;

  FILE* _outputFile;
  FILE* _eigenValueFile;
  FILE* _criterionFile;

  FILE* _pbcNodeData;

  double _tangentPerturbation;

  int _numberIterConverged;

  double _rveVolume;

  bool _isWriteDeformedMeshToFile;

 #endif //SWIG
 protected:
 #ifndef SWIG
  void setTimeForMicroBC(const double time);
  //rve volume
  double getRVEVolume() const;
  void microNumberDof();
  // for local implementation method
  void initializeLocalImplementationMethod();
  void applyMicroBCByLocalImplementationMethod();

  void computeSplittedStiffnessMatrix();
  stiffnessCondensation* createStiffnessCondensation(const bool sflag, const bool tflag);

  /** get homogzenized stress in case of no tangent computed **/
  void extractAverageStressByCondensation(STensor3& P, STensor33& Q);
  void extractAverageStressByVolumeIntegral(STensor3& P, STensor33& Q);

  void extractAverageStress(STensor3& P, STensor33& Q);

  /**get homogenized stress and tangent if stress and tangent are computed at the same time **/
  void extractAveragePropertiesByCondensation(STensor3& P, STensor33& Q,STensor43& FF, STensor53& FG, STensor53& GF, STensor63& GG);
  void extractAveragePropertiesPerturbation(STensor3& P, STensor33& Q,STensor43& FF, STensor53& FG, STensor53& GF, STensor63& GG);


  void extractAverageStressAndTangent(STensor3& P, STensor33& Q, STensor43& FF, STensor53& FG, STensor53& GF, STensor63& GG);

  void homogenizedDataToFile(const double time);
  void computeDensity();
  // for eigen value problem
  void createEigenvalueSolver();
  double critialPointTestFunction(double time, double* &mode);
  void modeViewToFile(std::vector<int>& num, double time, int step);
  // newton raphson
  int microNewtonRaphson(dofManager<double> *pAssembler,unknownField *ufield);
  int microNewtonRaphsonPathFollowing(dofManager<double> *pAssembler,unknownField *ufield);

  int pathFollowingPerturbation(dofManager<double> *pAssembler,unknownField *ufield);

  linearSystem<double>* createMicroSNLSystem();
  void microPostSolveStep(const double curtime, const int iter);
  void createMicroSystem();

  double solveMicroSolverSNL();
  double solveMicroSolverStaticLinear();
  double solveMicroSolverForwardEuler();

  void OneStep();

  void writeDeformedMesh(int step);
 #endif // SWIG
 public:
 #ifndef SWIG
  // RVE analysis newton raphson
  double microSolveStaticLinear();
  double microSolveSNL();

  void setPeriodicity(const SVector3& xd, const SVector3& yd, const SVector3& zd);
  void addLinearDisplacementBC(const int tag, const std::vector<int>& physical);
  void addPeriodicBC(const int tag, const std::vector<int>& physical);
  void addMinimalKinematicBC(const int tag, const std::vector<int>& physical);
  void setDeformationGradient(const STensor3& F);
  void setGradientOfDeformationGradient(const STensor33& G);
  int getElementSolverNumber() const;
  int getGaussPointSolverNumber() const;

  void extractAverageProperties(bool stiff = true);

  homogenizedData* getHomogenizationState(const IPStateBase::whichState state);
  void resetSolverToInitialStep();
  void closeAllFile();

 #endif // SWIG
  void createMicroModel(const std::string geoFile, const int dim, const int order, const bool incomplte  = false);
  void setMicroProblemIndentification(int ele, int gpt);
  void initMicroSolver();
  double microSolve();

  void nextStep();
  void archiveData(const double curtime, const int iter);

  void setMessageView(const bool view);
  void setPeriodicity(const double x, const double y, const double z, const std::string direction);
  void addLinearDisplacementBC(const int tag, const int g1, const int g2, const int g3,const int g4,const int g5 = 0, const int g6 = 0);
  void addPeriodicBC(const int tag, const int g1=0, const int g2=0, const int g3=0,const int g4=0,const int g5 = 0, const int g6 = 0);
  void addMinimalKinematicBC(const int tag, const int g1, const int g2, const int g3,const int g4,const int g5 = 0, const int g6 = 0);
  void addMixedBC(const int tag, const int g1, const int g2, const int g3, const int g4, const int g5 =0, const int g6=0);
  void addKinematicPhysical(const int i, const int comp);
  void addStaticPhysical(const int i, const int comp);
  //periodic options
  void setPeriodicBCOptions(const int method, const int seg = 5, const bool addvertex = 0);
  void setPeriodicBCDegree(const int direction, const int d, const bool addvertex);

  // for order
  void setOrder(const int ord);

  // for deformation gradient
  void setDeformationGradient(const int i, const int j, const double val);
  void setDeformationGradient(const double F00, const double F01, const double F02,
                              const double F10, const double F11, const double F12,
                              const double F20, const double F21, const double F22);
  void setDeformationGradient(const double F00, const double F01,
                              const double F10, const double F11);
  // for gradient of deformation gradient
  void setGradientOfDeformationGradient(const int i, const int j, const int k, const double val);
  // for instability
  void eigenValueSolver(const int num = 10, const bool fl = true); // number of eigenvalue
  void setModeView(const int view); // for view buckling mode
  void setPerturbationFactor(const double val = 1e-2);
  void setInstabilityCriterion(const double val = 1e-6);
  void perturbateBucklingModeByMinimalEigenValueMode(const bool flag = true);
  void setEigenSolverFollowing(const bool flag);


  // get macro properties
  void tangentAveragingFlag(const bool fl = true);
  void stressAveragingFlag(const bool fl = true);

  void setStressAveragingMethod(const int method);
  void setTangentAveragingMethod(const int method, const double prec = 1.e-8);

  void setDisplacementAndIPArchiveFlag(const bool flag = true);
  void setHomogenizationPropertyArchiveFlag(const bool flg = true);
  void setStrainArchiveFlag(const bool flg = true);

  double getHomogenizedDensity() const;
  void getStress(STensor3& stress) const;
  void getTangent(STensor43& tangent) const;
  void getHighOrderStress(STensor33& stress) const;
  void getHighOrderTangent(STensor63& J) const;
  void getHighOrderCrossFirstSecondTangent(STensor53& J) const;
  void getHighOrderCrossSecondFirstTangent(STensor53& J) const;
  void setExtractPerturbationToFileFlag(const bool flag);
  void setSystemType(const int i);
  void setControlType(const int i);
  void setMicroSolverFlag(const bool flag);
  void setMultiscaleFlag( const bool flag);

  void saveStiffnessMatrixToFile(const int iter);

  void readPBCDataFromFile(const std::string fn);
  void setRVEVolume(const double val);
  void setWriteDeformedMeshToFile(bool flag);
};

#endif //_NONLINEARMECHSOLVER_H_

