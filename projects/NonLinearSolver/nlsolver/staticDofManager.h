//
// C++ Interface: terms
//
// Description: dofManager for non linear system (rewritte of assemble(fullMatrix) no contribution for fixed Dof
//              The contribution is taken into account in the assemble(fullvector) of RightHandSide
//              More add functions to archiving force can be placed in dofManager but I put it here to not pollute gmsh code
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef _STATICDOFMANAGER_H_
#define _STATICDOFMANAGER_H_

#include "dofManager.h"
#include "nonLinearMechSolver.h"
#include "nonLinearSystems.h"

#if defined(HAVE_BLAS)

// extern blas function declaration
#if !defined(F77NAME)
#define F77NAME(x) (x##_)
#endif // F77NAME
extern "C" {
  void F77NAME(dcopy)(int *n, double *x,int *incx, double *y, int *incy);
}
#endif // HAVE_BLAS

class FilterCompByMod100 : public FilterDof
{
 protected:
  std::set<int> _mycomp;
 public:
  FilterCompByMod100(std::vector<int> &vcomp)
  {
    for(int i=0;i<vcomp.size();i++)
      _mycomp.insert(vcomp[i]);
  }
  virtual ~FilterCompByMod100(){}
  virtual bool operator() (Dof key)
  {
    // get the comp
    int comp = abs(key.getType()%100);
    // compare
    std::set<int>::iterator it = _mycomp.find(comp);
    if(it!=_mycomp.end()) return true;
    return false;
  }
};


// This struct is used to avoid multiple map search
template<class T>
struct FixedNodeDynamicData
{
 public:
  typename dofManager<T>::dataVec position;
  typename dofManager<T>::dataVec velocity;
  typename dofManager<T>::dataVec acceleration;
  typename dofManager<T>::dataVec mass;
  FixedNodeDynamicData() : position(0.), velocity(0.), acceleration(0.), mass(0.){}
  FixedNodeDynamicData(const FixedNodeDynamicData & src) : position(src.position), velocity(src.velocity), acceleration(src.acceleration),
                                                             mass(src.mass) {}
  FixedNodeDynamicData& operator=(const FixedNodeDynamicData & src)
  {
    position = src.position;
    velocity = src.velocity;
    acceleration = src.acceleration;
    mass = src.mass;
  }
  ~FixedNodeDynamicData(){}
};

template<class T>
class staticDofManager : public dofManager<T>{
 protected :
  nonLinearMechSolver::scheme _scheme;
  // map to retains the force of fixed dof (needed for archiving) the other RHS value are accessible in RHS
  std::map<Dof, typename dofManager<T>::dataVec> RHSfixed;
//  std::map<Dof, typename dofManager<T>::dataVec> _fixedpositions; // needed to compute velocities (position of last time step)
//  std::map<Dof, typename dofManager<T>::dataVec> _fixedvelocities;
//  std::map<Dof, typename dofManager<T>::dataVec> _fixedaccelerations;
//  std::map<Dof, typename dofManager<T>::dataVec> _fixedmass; // needed to compute kinetic energy
  std::map<Dof,FixedNodeDynamicData<T> > _fixedDynamic;
  bool _useGlobalSystemPosition ; // bool to known if the dofManager is parallelized. WARNING: _useGlobalSystemPosition  != _isParallel = false always for staticDofManager
  bool _isPartitioned; // bool to know if the dofmanager use for micro or macro problem
  int _firstRigidContactDof;
  /* pointer to the current nonLinearSystem */ // normally only 1 system is stored by dofManager and we avoid multiple dynamic_cast
  nonLinearSystem<T> *_NLScurrent;

 #if defined(HAVE_MPI)
  bool _mpiInit;
  std::map<Dof,int> _otherPartUnknowns; // local position of the unknowns
  std::map<Dof,int> _globalOtherPartSystemPosition; // global position of unknowns in the system
  bool _mpiSendGlobalPosition;
  T* _otherPartPositions;   // only works with T == double
  T* _previousOtherPartPositions;
  T* _mpiBuffer;
  int _sizeMPIBuffer;
  std::vector< std::set<Dof> > _mapMPIRecvComm;  //
  std::vector< std::vector<int>* > _mapMPIDof; // We are sure by construction that the Dof to transfert is not fixed so we can save directly the position of the Dof in the system
  int* _localNumberDofMPI; // array with the number of Dofs on each rank;

  T* _mpiRigidContactTMP; // array to communicate the unknown of rigid contact
  int _mpiRigidContactSize;
  /* static analysis involve only displacement here consider velocity and acceleration*/
  T* _otherPartVelocities;  // velocity
  T* _otherPartAccelerations; // acceleration

  // initialization of MPI communication (send the unknowns that this processor need and recieve from other rank the unknown to send
  virtual void initMPI()
  {
    if(Msg::GetCommSize() > 1){
      this->_mpiRigidContactSize = this->unknown.size() - this->_firstRigidContactDof;
      if(this->_mpiRigidContactSize > 0)
        this->_mpiRigidContactTMP = new T[this->_mpiRigidContactSize];

      /* first give the number of unknowns to transfert */
      int* MPImySendComm = new int[Msg::GetCommSize()];
      int* MPImySendTmp = new int[Msg::GetCommSize()];
      for(int i=0;i<Msg::GetCommSize();i++)
      {
          MPImySendTmp[i] = int(_mapMPIRecvComm[i].size());
      }
      MPI_Alltoall(MPImySendTmp,1,MPI_INT,MPImySendComm,1,MPI_INT,MPI_COMM_WORLD);

      delete[] MPImySendTmp;

      if(_mpiSendGlobalPosition){
       /* modify the unknnown map (static only !!). The access on matrix and vector is done with a global number
         So the sum of number of unknowns of previous ranks have to be added */
        // communicate the number of unknowns on each rank
        for(int i=0;i<Msg::GetCommSize();i++)
          _localNumberDofMPI[i] = 0;
        int* localbufferMPI = new int[Msg::GetCommSize()];
        for (int ii = 0; ii<Msg::GetCommSize(); ii++ ){
          localbufferMPI[ii] = 0;
        }
        localbufferMPI[Msg::GetCommRank()] = this->unknown.size();
        MPI_Allreduce(localbufferMPI,_localNumberDofMPI,Msg::GetCommSize(),MPI_INT,MPI_SUM,MPI_COMM_WORLD);
        delete[] localbufferMPI;
        // compute the number of unknowns
        int numberOfUnknownsPreviouRank=0;
        for(int i=0;i<Msg::GetCommRank();i++)
          numberOfUnknownsPreviouRank+= _localNumberDofMPI[i];
        // update unknown map
        if(numberOfUnknownsPreviouRank > 0)
        {
          for(std::map<Dof,int>::iterator it=this->unknown.begin(); it!=this->unknown.end();++it)
          {
            it->second += numberOfUnknownsPreviouRank;
          }
        }
      }
      // initiation of storage
      int nbdofOtherPart = _otherPartUnknowns.size();
      _otherPartPositions = new T[nbdofOtherPart];
      _previousOtherPartPositions = new T[nbdofOtherPart];

      // initiation of Communication (transfert the Dof to recieve from otherPart)
      long int * arrayBuffer;
      int currentBufferSize=0;
      // array to transfert the global system's position of the Dof to the other part
      int* arraySysPosition;
      int arraySysPositionSize=0;
      MPI_Status status;

      for(int i=0; i<Msg::GetCommSize(); i++)
      {
        // recieve first from lowest Rank
        if(Msg::GetCommRank() < i)
        {
          // transform Dof on the form of long int tabular (in sequence entity, type)
          std::set<Dof> &RecvSetDof = _mapMPIRecvComm[i];
          int commSize = 2*RecvSetDof.size(); // 2 components to transfert
          if(commSize > currentBufferSize){
            if(currentBufferSize != 0) delete[] arrayBuffer;
            currentBufferSize = commSize;
            arrayBuffer = new long int[commSize];
          }
          int j=0;
          for(std::set<Dof>::const_iterator it = RecvSetDof.begin(); it!=RecvSetDof.end(); ++it)
          {
            Dof R = *it;
            arrayBuffer[j] = R.getEntity();
            arrayBuffer[j+1] = (long int)R.getType();
            j+=2;
          }
          MPI_Send(arrayBuffer,commSize,MPI_LONG,i,Msg::GetCommRank(),MPI_COMM_WORLD);

          /* recieve the data from other rank */
          if(_mpiSendGlobalPosition){
            // recieve the dof position from other rank
            if(RecvSetDof.size() > arraySysPositionSize)
            {
              if(arraySysPositionSize !=0) delete[] arraySysPosition;
              arraySysPositionSize = RecvSetDof.size();
              arraySysPosition = new int[arraySysPositionSize];
            }
            MPI_Recv(arraySysPosition,RecvSetDof.size(),MPI_INT,i,i,MPI_COMM_WORLD,&status);
            j=0;
            for(std::set<Dof>::const_iterator it = RecvSetDof.begin(); it!=RecvSetDof.end(); ++it)
            {
              Dof R = *it;
              _globalOtherPartSystemPosition.insert(std::pair<Dof,int>(R,arraySysPosition[j]));
              j++;
            }
          }

          int commSizeSend = 2*MPImySendComm[i];
          if(commSizeSend > currentBufferSize){
            if(currentBufferSize != 0) delete[] arrayBuffer;
            currentBufferSize = commSizeSend;
            arrayBuffer = new long int[commSizeSend];
          }
          MPI_Recv(arrayBuffer,commSizeSend,MPI_LONG,i,i,MPI_COMM_WORLD,&status);
          // Vector to send global system position
          if(_mpiSendGlobalPosition){
            if(MPImySendComm[i] > arraySysPositionSize)
            {
              if(arraySysPositionSize !=0 ) delete[] arraySysPosition;
              arraySysPositionSize = MPImySendComm[i];
              arraySysPosition = new int [arraySysPositionSize];
            }
          }
          // build the map with dof to transfert during the iteration
          std::vector<int>* &mapDof = _mapMPIDof[i];
          if(mapDof !=NULL) delete mapDof;
          mapDof = new std::vector<int>;
          int jj=0; // arraySysPosition index
          for(int j=0;j<commSizeSend; j+=2)
          {
            Dof R = Dof(arrayBuffer[j],-(int)arrayBuffer[j+1]);
            // search the Dof in map
            std::map<Dof,int>::const_iterator itR = this->unknown.find(R);
             if(itR == this->unknown.end())
             {
               Msg::Error(" It seems that a Dof is not existing for MPI transfert from rank %d to rank %d Dof ent = %d Dof type = %d ",Msg::GetCommRank(),i,R.getEntity(),R.getType());
             }
             else
            {
              mapDof->push_back(itR->second);
              if(_mpiSendGlobalPosition) arraySysPosition[jj] = itR->second;
            }
            jj++;
          }
          if(_mpiSendGlobalPosition)
            MPI_Send(arraySysPosition,MPImySendComm[i],MPI_INT,i,Msg::GetCommRank(),MPI_COMM_WORLD);

        }
        // invert the order of operation (Recv first)
        else if(Msg::GetCommRank() > i) // nothing to do if i == GetCommRank (self communication)
        {
          // recieve the data from other rank
          int commSizeSend = 2*MPImySendComm[i];
          if(commSizeSend > currentBufferSize){
            if(currentBufferSize != 0) delete[] arrayBuffer;
            currentBufferSize = commSizeSend;
            arrayBuffer = new long int[commSizeSend];
          }
          MPI_Recv(arrayBuffer,commSizeSend,MPI_LONG,i,i,MPI_COMM_WORLD,&status);

          // build the map with dof to transfert during the iteration
          std::vector<int>* &mapDof = _mapMPIDof[i];
          if(mapDof !=NULL) delete mapDof;
          mapDof = new std::vector<int>;
          if(_mpiSendGlobalPosition){
            // array to send the global system's position
            if(MPImySendComm[i] > arraySysPositionSize)
            {
              if(arraySysPositionSize !=0) delete[] arraySysPosition;
              arraySysPositionSize = MPImySendComm[i];
              arraySysPosition = new int[arraySysPositionSize];
            }
          }
          int jj=0; // arraySysPosition index
          for(int j=0;j<commSizeSend; j+=2)
          {
            Dof R = Dof(arrayBuffer[j],-(int)arrayBuffer[j+1]);
            // search the Dof in map
            std::map<Dof,int>::const_iterator itR = this->unknown.find(R);
             if(itR == this->unknown.end())
             {
               Msg::Error("It seems that a Dof is not existing for MPI transfert from rank %d to rank %d Dof ent = %d Dof type = %d ",Msg::GetCommRank(),i,R.getEntity(),R.getType());

             }
             else
            {
              mapDof->push_back(itR->second);
              if(_mpiSendGlobalPosition) arraySysPosition[jj] = itR->second;
            }
            jj++;
          }
          if(_mpiSendGlobalPosition)
            MPI_Send(arraySysPosition,MPImySendComm[i],MPI_INT,i,Msg::GetCommRank(),MPI_COMM_WORLD);

          // send now
          std::set<Dof> &RecvSetDof = _mapMPIRecvComm[i];
          int commSize = 2*RecvSetDof.size(); // 2 components to transfert
          if(commSize > currentBufferSize){
            if(currentBufferSize != 0) delete[] arrayBuffer;
            currentBufferSize = commSize;
            arrayBuffer = new long int[commSize];
          }
          int j=0;
          for(std::set<Dof>::const_iterator it = RecvSetDof.begin(); it!=RecvSetDof.end(); ++it)
          {
            Dof R = *it;
            arrayBuffer[j] = R.getEntity();
            arrayBuffer[j+1] = (long int)R.getType();
            j+=2;
          }
          MPI_Send(arrayBuffer,commSize,MPI_LONG,i,Msg::GetCommRank(),MPI_COMM_WORLD);

          if(_mpiSendGlobalPosition){
            // recieve the global system position
            if(RecvSetDof.size() > arraySysPositionSize)
            {
              if(arraySysPositionSize != 0) delete[] arraySysPosition;
              arraySysPositionSize = RecvSetDof.size();
              arraySysPosition = new int[arraySysPositionSize];
            }
            MPI_Recv(arraySysPosition,RecvSetDof.size(),MPI_INT,i,i,MPI_COMM_WORLD,&status);
            j=0;
            for(std::set<Dof>::const_iterator it = RecvSetDof.begin(); it!=RecvSetDof.end(); ++it)
            {
              Dof R = *it;
              _globalOtherPartSystemPosition.insert(std::pair<Dof,int>(R,arraySysPosition[j]));
              j++;
            }
          }
        }
      }

      if(currentBufferSize !=0) delete[] arrayBuffer;
      if(arraySysPositionSize!=0) delete[] arraySysPosition;
      delete[] MPImySendComm;
      // initiation of additional storage for Explicit Only (Implicit no need of velocity and acceleration
      if(_scheme==nonLinearMechSolver::Explicit)
      {
        this->_otherPartVelocities = new T[nbdofOtherPart];
        this->_otherPartAccelerations = new T[nbdofOtherPart];
      }
    }
    _mpiInit = true;
  }

 #endif // HAVE_MPI

 public :
  /* BE AWARE ALWAYS init a sequential dofManager ALL the parallelization is managed here by
    the staticDofManager */
 #if defined(HAVE_MPI)
  staticDofManager(linearSystem< typename dofManager<T>::dataMat > *l, nonLinearMechSolver::scheme sh,
                   bool useGlobalSystemPosition =false, bool part = true) : dofManager<T>(l,false), _scheme(sh), _useGlobalSystemPosition (useGlobalSystemPosition ),
                                       _NLScurrent(dynamic_cast<nonLinearSystem<T>*>(l)),
                                       _mpiInit(false), _otherPartPositions(NULL), _previousOtherPartPositions(NULL),
                                       _mapMPIRecvComm(Msg::GetCommSize()),
                                       _localNumberDofMPI(NULL),
                                       _mapMPIDof(Msg::GetCommSize()),_mpiBuffer(NULL), _sizeMPIBuffer(0),
                                       _mpiSendGlobalPosition(true), _firstRigidContactDof(0),
                                       _mpiRigidContactSize(0),_otherPartVelocities(NULL),_otherPartAccelerations(NULL),
                                       _mpiRigidContactTMP(NULL), _isPartitioned(part)
  {
    if(Msg::GetCommSize() > 1){
      _localNumberDofMPI = new int[Msg::GetCommSize()];
      for(int i=0;i<Msg::GetCommSize();i++){
        _localNumberDofMPI[i] =0;
      }
    }
    this->_mpiSendGlobalPosition = useGlobalSystemPosition ;
  }
  virtual ~staticDofManager()
  {
    if(_sizeMPIBuffer != 0) delete[] _mpiBuffer;
    if(_localNumberDofMPI != NULL) delete[] _localNumberDofMPI;
  }
 #else
  staticDofManager(linearSystem< typename dofManager<T>::dataMat > *l, nonLinearMechSolver::scheme sh, bool useGlobalSystemPosition =false, bool part = false) : dofManager<T>(l,false), _isPartitioned(false),
                                                                     _scheme(sh), _useGlobalSystemPosition (false), _NLScurrent(dynamic_cast<nonLinearSystem<T>*>(l)){} // _useGlobalSystemPosition  can't be true if no MPI
 #endif // HAVE_MPI
  staticDofManager(linearSystem< typename dofManager<T>::dataMat > *l1,
                   linearSystem<typename dofManager<T>::dataMat> *l2,
                   nonLinearMechSolver::scheme sh) : dofManager<T>(l1,l2), _useGlobalSystemPosition (false), _scheme(sh),
                                                     _NLScurrent(static_cast<nonLinearSystem<T>*>(l1)),_isPartitioned(true){} // _current == l1

  staticDofManager(linearSystem< typename dofManager<T>::dataMat> *l,staticDofManager<T> *oldmanager,
                   nonLinearMechSolver::scheme currentScheme) : dofManager<T>(l,false), _useGlobalSystemPosition (oldmanager->_useGlobalSystemPosition ),
                                                                 _isPartitioned(oldmanager->_isPartitioned), _scheme(currentScheme),
                                                                _NLScurrent(dynamic_cast<nonLinearSystem<T>*>(l))
                                                               #if defined(HAVE_MPI)
                                                                , _localNumberDofMPI(NULL)
                                                               #endif // HAVE_MPI
  {
    _firstRigidContactDof = 0; // no implicit contact in mpi for now !!
   #if defined(HAVE_MPI)
    _sizeMPIBuffer = 0;
    _mpiBuffer = NULL;
    _mpiInit = oldmanager->_mpiInit;
    _mapMPIRecvComm = oldmanager->_mapMPIRecvComm;
    _mapMPIDof = oldmanager->_mapMPIDof;
    if(Msg::GetCommSize() > 1)
    {
      _localNumberDofMPI = new int[Msg::GetCommSize()];
      for(int i=0;i<Msg::GetCommSize();i++)
      {
        _localNumberDofMPI[i] = oldmanager->_localNumberDofMPI[i];
        _otherPartUnknowns = oldmanager->_otherPartUnknowns;
        _otherPartPositions = new T[_otherPartUnknowns.size()];
        _previousOtherPartPositions = new T[_otherPartUnknowns.size()];
        for(int i=0;i<_otherPartUnknowns.size();i++)
        {
          _otherPartPositions[i] = oldmanager->_otherPartPositions[i];
          _previousOtherPartPositions[i] = oldmanager->_previousOtherPartPositions[i];
        }
        _mpiSendGlobalPosition = oldmanager->_mpiSendGlobalPosition;
        if(_mpiSendGlobalPosition){
          _globalOtherPartSystemPosition = oldmanager->_globalOtherPartSystemPosition;
        }
      }
    }
   #endif // HAVE_MPI
    this->constraints = oldmanager->constraints;
    this->fixed = oldmanager->fixed;
    this->_fixedDynamic = oldmanager->_fixedDynamic;
    this->initial = oldmanager->initial;
    this->unknown = oldmanager->unknown;
    this->associatedWith = oldmanager->associatedWith;

    std::string sysname("A");
    linearSystem<typename dofManager<T>::dataMat>* oldsys = oldmanager->getLinearSystem(sysname);
     if(_scheme == nonLinearMechSolver::Explicit && oldmanager->_scheme ==  nonLinearMechSolver::StaticNonLinear )
     {
       _useGlobalSystemPosition =false;
       int numberOfUnknownsPreviouRank=0;
       #if defined(HAVE_MPI)
       this->_mpiRigidContactSize = 0;
       this->_otherPartVelocities = new double[this->_otherPartUnknowns.size()];
       this->_otherPartAccelerations = new double[this->_otherPartUnknowns.size()];
       for(int i=0;i<this->_otherPartUnknowns.size();i++)
       {
         this->_otherPartVelocities[i] = 0.;
         this->_otherPartAccelerations[i]=0.;
       }
       this->_mpiSendGlobalPosition = false;

       for(int i=0;i<Msg::GetCommRank();i++)
         numberOfUnknownsPreviouRank+= this->_localNumberDofMPI[i];
       // update unknown map
       if(numberOfUnknownsPreviouRank > 0) // last one static we retrieve the number of unknowns of previous rank as explicit scheme use a local number of unknowns
       {
         for(std::map<Dof,int>::iterator it=this->unknown.begin(); it!=this->unknown.end();++it)
         {
           it->second -= numberOfUnknownsPreviouRank;
         }
         for(int j=0;j<this->_mapMPIDof.size();j++)
         {
           if(j != Msg::GetCommRank()){
             std::vector<int> &mapDof = *(this->_mapMPIDof[j]);
             for(int k=0;k<mapDof.size();k++)
               mapDof[k] -= numberOfUnknownsPreviouRank;
           }
         }
       }
      #endif // HAVE_MPI
      // initialization for previous sys
      double val;
      l->allocate(this->unknown.size());
      double dt = 0.;
      nonLinearSystem<typename dofManager<T>::dataMat>* oldnlsys = dynamic_cast<nonLinearSystem<typename dofManager<T>::dataMat>*>(oldsys);
      if(oldnlsys!=NULL)
      {
        dt = oldnlsys->getTimeStep();
      }
      double oldval;
      for(int i=0;i<this->unknown.size();i++){
        oldsys->getFromSolution(i+numberOfUnknownsPreviouRank,val);
        _NLScurrent->setInitialCondition(i,val,nonLinearBoundaryCondition::position);
        if(dt!=0.)
        {
          oldnlsys->getFromPreviousSolution(i+numberOfUnknownsPreviouRank,oldval);
          _NLScurrent->setInitialCondition(i,(val-oldval)/dt,nonLinearBoundaryCondition::velocity);
        }
      }
    }
    else if(l != oldsys) // For transfering mesh (some modifications are needed to account for the growth static only...
    {
      double val;
      int numberOfUnknownsPreviouRank=0;
    #if defined(HAVE_MPI)
      for(int i=0;i<Msg::GetCommRank();i++)
        numberOfUnknownsPreviouRank+= this->_localNumberDofMPI[i];
    #endif // HAVE_MPI
      l->allocate(this->unknown.size());
      for(int i=0;i<this->unknown.size();i++){
        oldsys->getFromSolution(i+numberOfUnknownsPreviouRank,val);
        _NLScurrent->setInitialCondition(i+numberOfUnknownsPreviouRank,val,nonLinearBoundaryCondition::position);
      }
    }
    return;
  }

  // oldmanager contains all comp and this contains only several of them
  // Fix by regrouping with the previous constructor (filter true for all Dofs)
  // Here comp are compared but comp is obtained by abs(type%100)
staticDofManager(linearSystem< typename dofManager<T>::dataMat> *l,staticDofManager<T> *oldmanager,
                   nonLinearMechSolver::scheme currentScheme, const int tcomps,
                   std::vector<int> &mycomp) : dofManager<T>(l,false), _useGlobalSystemPosition (oldmanager->_useGlobalSystemPosition ), _isPartitioned(oldmanager->_isPartitioned),_scheme(currentScheme),
                                                                _NLScurrent(dynamic_cast<nonLinearSystem<T>*>(l))
                                                               #if defined(HAVE_MPI)
                                                                ,_localNumberDofMPI(NULL),
                                                                _mpiInit(false), _otherPartPositions(NULL), _previousOtherPartPositions(NULL),
                                                                _mapMPIRecvComm(Msg::GetCommSize()),
                                                                _mapMPIDof(Msg::GetCommSize()),_mpiBuffer(NULL),
                                                                _sizeMPIBuffer(0),
                                                                _mpiSendGlobalPosition(true),
                                                                _firstRigidContactDof(0),
                                                                _mpiRigidContactSize(0),_otherPartVelocities(NULL),
                                                                _otherPartAccelerations(NULL),_mpiRigidContactTMP(NULL)
                                                               #endif // HAVE_MPI
  {
    FilterCompByMod100 myfilter = FilterCompByMod100(mycomp);

    for(typename std::map<Dof,DofAffineConstraint<typename dofManager<T>::dataVec> >::iterator it = oldmanager->constraints.begin(); it!=oldmanager->constraints.end();++it)
    {
      if(myfilter((*it).first))
      {
        this->constraints.insert(std::pair<Dof,DofAffineConstraint<typename dofManager<T>::dataVec> >((*it).first,(*it).second));
      }
    }
    for(typename std::map<Dof,typename dofManager<T>::dataVec>::iterator it=oldmanager->fixed.begin(); it!=oldmanager->fixed.end();++it)
    {
      if(myfilter((*it).first))
      {
        this->fixed.insert(std::pair<Dof,typename dofManager<T>::dataVec>((*it).first,(*it).second));
      }
    }
    for(typename std::map<Dof,FixedNodeDynamicData<T> >::iterator it=oldmanager->_fixedDynamic.begin(); it!=oldmanager->_fixedDynamic.end();++it)
    {
      if(myfilter((*it).first))
      {
        this->_fixedDynamic.insert(std::pair<Dof,FixedNodeDynamicData<T> >((*it).first,(*it).second));
      }
    }
    for(typename std::map<Dof,std::vector<typename dofManager<T>::dataVec> >::iterator it=oldmanager->initial.begin(); it!=oldmanager->initial.end();++it)
    {
      if(myfilter((*it).first))
      {
        this->initial.insert(std::pair<Dof,std::vector<typename dofManager<T>::dataVec> >((*it).first,(*it).second));
      }
    }
    int syspos=0;
    for(std::map<Dof,int>::iterator it=oldmanager->unknown.begin(); it!=oldmanager->unknown.end();++it)
    {
      if(myfilter((*it).first))
      {
        this->unknown.insert(std::pair<Dof,int>((*it).first,syspos));
        syspos++;
      }
    }
    this->associatedWith = oldmanager->associatedWith;
    for(std::map<Dof,Dof>::iterator it = oldmanager->associatedWith.begin(); it!=oldmanager->associatedWith.end();++it)
    {
      if(myfilter((*it).first))
      {
        this->associatedWith.insert(std::pair<Dof,Dof>((*it).first,(*it).second));
      }
    }
   #if defined(HAVE_MPI)
    if(_scheme == nonLinearMechSolver::Explicit){
      _mpiSendGlobalPosition = false;
      _useGlobalSystemPosition  = false;
    }
    if(Msg::GetCommSize() > 1){
      _localNumberDofMPI = new int[Msg::GetCommSize()];
      for(int i=0;i<Msg::GetCommSize();i++){
        _localNumberDofMPI[i] =0;
      }
    }
    for(int i=0;i<Msg::GetCommSize();i++)
    {
      std::set<Dof> &rankset = _mapMPIRecvComm[i];
      std::set<Dof> &oldset = oldmanager->_mapMPIRecvComm[i];
      for(std::set<Dof>::iterator itR=oldset.begin();itR!=oldset.end();++itR)
      {
        if(myfilter(*itR))
        {
          rankset.insert((*itR));
        }
      }
    }
    int othersyspos=0;
    for(std::map<Dof,int>::iterator it = oldmanager->_otherPartUnknowns.begin(); it!=oldmanager->_otherPartUnknowns.end();++it)
    {
      if(myfilter((*it).first))
      {
        _otherPartUnknowns.insert(std::pair<Dof,int>((*it).first,othersyspos));
        othersyspos++;
      }
    }
   #endif // HAVE_MPI

    // initialization for previous sys
    double val;
    l->allocate(this->unknown.size());
    std::string sysname("A");
    linearSystem<typename dofManager<T>::dataMat>* oldsys = oldmanager->getLinearSystem(sysname);
    double dt = 0.;
    nonLinearSystem<typename dofManager<T>::dataMat>* oldnlsys = dynamic_cast<nonLinearSystem<typename dofManager<T>::dataMat>*>(oldsys);
    if(oldnlsys!=NULL)
    {
      dt = oldnlsys->getTimeStep();
    }
    double oldval;
    for(std::map<Dof,int>::iterator itR = this->unknown.begin(); itR!=this->unknown.end();++itR){
      oldmanager->getDofValue((*itR).first,val);
      _NLScurrent->setInitialCondition((*itR).second,val,nonLinearBoundaryCondition::position);
      if(dt!=0. && this->_scheme==nonLinearMechSolver::Explicit)
      {
        oldmanager->getDofValuePrevious((*itR).first,oldval);
        _NLScurrent->setInitialCondition((*itR).second,(val-oldval)/dt,nonLinearBoundaryCondition::velocity);
      }
    }

    #if defined(HAVE_MPI)
     this->initMPI();
     // value of other part
     for(std::map<Dof,int>::iterator itR = _otherPartUnknowns.begin(); itR!=_otherPartUnknowns.end();++itR)
     {
       std::map<Dof,int>::iterator oldit = oldmanager->_otherPartUnknowns.find((*itR).first);
       if(oldit==oldmanager->_otherPartUnknowns.end()){
         Msg::Error("Cannot initialize the position and the velocity of a Dof of another part!");
         _otherPartPositions[itR->second] = 0.;
         _previousOtherPartPositions[itR->second] = 0.;
         if(_scheme == nonLinearMechSolver::Explicit)
           _otherPartVelocities[itR->second] = 0.;
       }
       else
       {
         double val = oldmanager->_otherPartPositions[oldit->second];
         _otherPartPositions[itR->second] = val;
         double oldval = oldmanager->_previousOtherPartPositions[oldit->second];
         _previousOtherPartPositions[itR->second] = oldval;
         if(dt!=0. && _scheme==nonLinearMechSolver::Explicit){
           _otherPartVelocities[itR->second] = (val-oldval)/dt;
         }
       }
     }
    #endif // HAVE_MPI
  }

  virtual nonLinearMechSolver::scheme getScheme()const {return _scheme;}
  virtual void setFirstRigidContactUnknowns()
  {
    _firstRigidContactDof = this->unknown.size();
  }
  virtual int getFirstRigidContactUnknowns(){return _firstRigidContactDof;}

  virtual inline void assemble(std::vector<Dof> &R, const fullMatrix<typename dofManager<T>::dataMat> &m){
    this->assemble(R,m,nonLinearSystem<T>::stiff);
  }

  virtual inline void assemble(std::vector<Dof> &R, const fullMatrix<typename dofManager<T>::dataMat> &m,const typename explicitHulbertChung<T>::whichMatrix wm)
  {
    if (!this->_current->isAllocated()) this->_current->allocate(this->unknown.size());
    std::vector<int> NR(R.size()); // in MPI NR != NC
    std::vector<int> NC(R.size());

    for (unsigned int i = 0; i < R.size(); i++)
    {
     #if defined(HAVE_MPI)
      if(R[i].getType() < 0){
        NR[i] = -1;
        std::map<Dof,int>::iterator itR = _globalOtherPartSystemPosition.find(R[i]);
        if(itR == _globalOtherPartSystemPosition.end())
        {
          NC[i] = -1;
         #ifdef _DEBUG
          typename std::map<Dof,typename dofManager<T>::dataVec>::iterator itR = this->fixed.find(R[i]);
          if(itR == this->fixed.end())
            Msg::Error("Can't find the global column of a dof in mpi on rank  %d",Msg::GetCommRank());
         #endif // _DEBUG

        }
        else
        {
          NC[i] = itR->second;
        }
      }
      else
     #endif // HAVE_MPI
      {
       // std::map<Dof, int>::iterator itLocal = this->unknown.end();
        //for(itLocal = this->unknown.begin();  itLocal!= this->unknown.end(); itLocal ++)
         //      if(itLocal->first == R[i]) break;
        std::map<Dof, int>::iterator itLocal = this->unknown.find(R[i]);
        if (itLocal != this->unknown.end())
        {
          NR[i] = itLocal->second;
          NC[i] = itLocal->second;
        }
        else
        {
          NR[i] = -1;
          NC[i] = -1;
        }
      }
    }
    if(wm == explicitHulbertChung<T>::stiff)
    {
      for (unsigned int i = 0; i < R.size(); i++){
        if (NR[i] != -1)
        {
          for (unsigned int j = 0; j < R.size(); j++)
          {
            if (NC[j] != -1)
            {
              this->_current->addToMatrix(NR[i], NC[j], m(i, j));
            }
            else
            {
              typename std::map<Dof,  typename dofManager<T>::dataVec>::iterator itFixed = this->fixed.find(R[j]);
              if (itFixed == this->fixed.end())
                this->assembleLinConst(R[i],R[j],m(i,j));
            }
          }
        }
        else
        {
          for (unsigned int j = 0; j < R.size(); j++){
            this->assembleLinConst(R[i],R[j],m(i,j));
          }
        }
      }
    }
    else{ // mass matrix
      for (unsigned int i = 0; i < R.size(); i++)
        if (NR[i] != -1)
          for (unsigned int j = 0; j < R.size(); j++)
            _NLScurrent->addToMatrix(NR[i], NC[j], m(i, j),explicitHulbertChung<T>::mass);
        else{ // store mass in _fixedmass
          double linemass=0.;
          // if already exist in _fixed mass retrieve value to add sum because diagonal matrix
          typename std::map<Dof,FixedNodeDynamicData<T> >::iterator itm = this->_fixedDynamic.find(R[i]);
          if(itm != this->_fixedDynamic.end())
            linemass+=itm->second.mass;
          for(unsigned int j=0; j<R.size(); j++)
            linemass+=m(i,j);
          (this->_fixedDynamic[R[i]]).mass = linemass;
        }
    }
  }

  virtual inline void setCurrentMatrix(std::string name)
  {
    dofManager<T>::setCurrentMatrix(name);
    _NLScurrent = dynamic_cast<nonLinearSystem<T>*>(this->_current);
  }

  // Function to fix dof (Create the key in RHS fixed too)
  inline void fixDof(Dof key, const typename dofManager<T>::dataVec &value){
   #ifdef _DEBUG
   // verify that the dof is fixed
    if(this->unknown.find(key) != this->unknown.end())
    {
      Msg::Error("It seems that you try to fix a dof that is declared as an unknown... fix this");
      return;
    }
   #endif // _DEBUG
    this->RHSfixed[key]=0.;
    typename std::map<Dof, typename dofManager<T>::dataVec>::iterator itD = this->fixed.find(key);
    typename std::map<Dof,FixedNodeDynamicData<T> >::iterator itDyn;
    // Perform a new insertion in the system
    if(itD == this->fixed.end())
    {
      itD = this->fixed.insert(this->fixed.begin(), std::pair<Dof,typename dofManager<T>::dataVec>(key,value));
      itDyn = this->_fixedDynamic.insert(this->_fixedDynamic.begin(), std::pair<Dof,FixedNodeDynamicData<T> >(key,FixedNodeDynamicData<T>()));
      return;
    }
    else
    {
      itDyn = this->_fixedDynamic.find(key); // By construction we are sure that the key exists in the map
    }

    // The value already exist --> update
    double& myvalue = itD->second; //= value;
    double& lastpos = itDyn->second.position;
    if(_scheme==nonLinearMechSolver::Explicit)
    {
      double timestep = _NLScurrent->getTimeStep();
      double& lastvelo= itDyn->second.velocity;

      double newvelo = (value-lastpos) / timestep;
      itDyn->second.acceleration = (newvelo-lastvelo) / timestep;
      lastvelo = newvelo;
    }
    lastpos = myvalue;
    myvalue = value;

    return;
  }

  // function assemble(vect) is rewritten to archiving rhs if needed
  // THIS ONE IS THE OLD ONE kept for retrocompatibility (please use the next one)
  virtual inline void assemble(std::vector<Dof> &R, const fullVector<typename dofManager<T>::dataMat> &m) // for linear forms
  {
    #ifdef _DEBUG
      Msg::Warning("assemble(R,fullVector) is deprecated for non linear system. Please add argument nonLinearSystem::rhs");
    #endif
    if (!this->_current->isAllocated()) this->_current->allocate(this->unknown.size());
    std::vector<int> NR(R.size());
    // Archiving
    for (unsigned int i = 0; i < R.size(); i++)
    {
      typename std::map<Dof,  typename dofManager<T>::dataVec>::iterator itR = RHSfixed.find(R[i]);
      if (itR != RHSfixed.end()) itR->second += m(i); // add of contribution set to zero when the value is by a function FIX it
      else NR[i] = -1;
    }

    for (unsigned int i = 0; i < R.size(); i++)
    {
     #if defined(HAVE_MPI)
      if(R[i].getType() < 0)
      {
        NR[i]  = -1;
      }
      else
     #endif // HAVE_MPI
      {
        std::map<Dof, int>::iterator itR = this->unknown.find(R[i]);
        if (itR != this->unknown.end()) NR[i] = itR->second;
        else NR[i] = -1;
      }
    }
    for (unsigned int i = 0; i < R.size(); i++)
    {
      if (NR[i] != -1)
      {
        _NLScurrent->addToRightHandSidePlus(NR[i], m(i)); // plus because it's the old implementation that is kept for compatibility
      }
      else
      {
        typename std::map<Dof,DofAffineConstraint<typename dofManager<T>::dataVec> >::iterator itConstraint;
        itConstraint = this->constraints.find(R[i]);
        if (itConstraint != this->constraints.end())
        {
          for (unsigned j = 0; j < (itConstraint->second).linear.size(); j++){
            typename dofManager<T>::dataMat tmp;
            dofTraits<T>::gemm(tmp, (itConstraint->second).linear[j].second, m(i), 1, 0);
            dofManager<T>::assemble((itConstraint->second).linear[j].first, tmp);
          }
        }
      }
    }
  }
  virtual inline void assemble(std::vector<Dof> &R, const fullVector<typename dofManager<T>::dataMat> &m,const typename nonLinearSystem<T>::rhs wrhs) // for linear forms
  {
    if (!this->_current->isAllocated()) this->_current->allocate(this->unknown.size());
    std::vector<int> NR(R.size());
    // Archiving
    for (unsigned int i = 0; i < R.size(); i++)
    {
     #if defined(HAVE_MPI)
      if(R[i].getType() < 0)
      {
        NR[i] = -1;
      }
      else
     #endif // HAVE_MPI
      {
        typename std::map<Dof,  typename dofManager<T>::dataVec>::iterator itR = RHSfixed.find(R[i]);
        if (itR != RHSfixed.end()) itR->second += m(i); // add of contribution set to zero when the value is by a function FIX it
        else NR[i] = -1;
      }
    }

    for (unsigned int i = 0; i < R.size(); i++)
    {
      std::map<Dof, int>::iterator itR = this->unknown.find(R[i]);
      if (itR != this->unknown.end()) NR[i] = itR->second;
      else NR[i] = -1;
    }
    if(wrhs == nonLinearSystem<T>::Fext){ // Warning small duplication to avoid if for each component of R
      for (unsigned int i = 0; i < m.size(); i++) // use m in place of R (size m < size R for full DG formulation and external force applied on an internal edge)
      {
        if (NR[i] != -1)
        {
          _NLScurrent->addToRightHandSidePlus(NR[i], m(i));
        }
        else
        {
          typename std::map<Dof,DofAffineConstraint<typename dofManager<T>::dataVec> >::iterator itConstraint;
          itConstraint = this->constraints.find(R[i]);
          if (itConstraint != this->constraints.end())
          {
            for (unsigned j = 0; j < (itConstraint->second).linear.size(); j++){
              typename dofManager<T>::dataMat tmp;
              dofTraits<T>::gemm(tmp, (itConstraint->second).linear[j].second, m(i), 1, 0);
              this->assemble((itConstraint->second).linear[j].first,tmp,wrhs);
            }
          }
        }
      }
    }
    else if (wrhs == nonLinearSystem<T>::LoadVector){
      for (unsigned int i = 0; i < m.size(); i++) // use m in place of R (size m < size R for full DG formulation and external force applied on an internal edge)
      {
        if (NR[i] != -1)
        {
          _NLScurrent->addToLoadVector(NR[i], m(i));
        }
        else
        {
          typename std::map<Dof,DofAffineConstraint<typename dofManager<T>::dataVec> >::iterator itConstraint;
          itConstraint = this->constraints.find(R[i]);
          if (itConstraint != this->constraints.end())
          {
            for (unsigned j = 0; j < (itConstraint->second).linear.size(); j++){
              typename dofManager<T>::dataMat tmp;
              dofTraits<T>::gemm(tmp, (itConstraint->second).linear[j].second, m(i), 1, 0);
              this->assemble((itConstraint->second).linear[j].first,tmp,wrhs);
            }
          }
        }
      }
    }
    else{ // Waring small duplication to avoid if for each component of R
      for (unsigned int i = 0; i < R.size(); i++)
      {
        if (NR[i] != -1)
        {
          _NLScurrent->addToRightHandSideMinus(NR[i], m(i));
        }
        else
        {
          typename std::map<Dof,DofAffineConstraint<typename dofManager<T>::dataVec> >::iterator itConstraint;
          itConstraint = this->constraints.find(R[i]);
          if (itConstraint != this->constraints.end())
          {
            for (unsigned j = 0; j < (itConstraint->second).linear.size(); j++){
              typename dofManager<T>::dataMat tmp;
              dofTraits<T>::gemm(tmp, (itConstraint->second).linear[j].second, m(i), 1, 0);
              this->assemble((itConstraint->second).linear[j].first,tmp,wrhs);
            }
          }
        }
      }
    }
  }

  virtual inline void assemble(const Dof &R, const typename dofManager<T>::dataMat &value, const typename nonLinearSystem<T>::rhs wrhs){
    if (!this->_current->isAllocated()) this->_current->allocate(this->unknown.size());
    #if defined(HAVE_MPI)
    if(R.getType() >= 0)
   #endif // HAVE_MPI
    {
      std::map<Dof, int>::iterator itR = this->unknown.find(R);
      if(itR != this->unknown.end()){
        if(wrhs == nonLinearSystem<T>::Fext){
          _NLScurrent->addToRightHandSidePlus(itR->second,value);
        }
        else if (wrhs == nonLinearSystem<T>::LoadVector){
          _NLScurrent->addToLoadVector(itR->second,value);
        }
        else{
          _NLScurrent->addToRightHandSideMinus(itR->second,value);
        }
      }
      else{
        typename std::map<Dof, DofAffineConstraint<typename dofManager<T>::dataVec> >::iterator itConstraint;
        itConstraint = this->constraints.find(R);
        if (itConstraint != this->constraints.end()){
          for (unsigned j = 0; j < (itConstraint->second).linear.size(); j++){
            typename dofManager<T>::dataVec tmp;
            dofTraits<T>::gemm(tmp, (itConstraint->second).linear[j].second, value, 1, 0);
            this->assemble((itConstraint->second).linear[j].first, tmp,wrhs);
          }
        }
      }
    }
  }

  // zero for RHSfixed
  virtual void clearRHSfixed(){
    this->_current->zeroRightHandSide();
    for(typename std::map<Dof,typename dofManager<T>::dataVec>::iterator itR = RHSfixed.begin();itR != RHSfixed.end(); ++itR)
      itR->second = 0.;
  }
  virtual void getForces(std::vector<archiveForce> &vaf) const{
    for(int j=0;j<vaf.size();j++){
      double Fext =0.;
      for(std::set<Dof>::const_iterator itD = vaf[j].vdof.begin(); itD!=vaf[j].vdof.end();++itD){
        // look in RHSfixed
        typename std::map<Dof, typename dofManager<T>::dataVec>::const_iterator itRHS = RHSfixed.find(*itD);
        if(itRHS != RHSfixed.end())
          Fext += itRHS->second; // Fext
 /*       else{
          typename std::map<Dof,int>::const_iterator itunk = this->unknown.find(vaf[j].vdof[i]);
          if(itunk != this->unknown.end()){
            double ff;
            _NLScurrent->getFromRightHandSideMinus(itunk->second,ff);
            Fext -= ff; // Fext = -Fint
          }
        }*/
      }
      vaf[j].fval= Fext;
    }
  }

  virtual inline void setInitialCondition(const Dof &R, const typename dofManager<T>::dataVec &value,
                                          const typename nonLinearBoundaryCondition::whichCondition wc){

    if (!this->_current->isAllocated()) this->_current->allocate(this->unknown.size());
    // find the dof in map if not find -1
    int NR=-1;
    std::map<Dof, int>::iterator itR = this->unknown.find(R);
    if (itR != this->unknown.end()){
      NR = itR->second;
      _NLScurrent->setInitialCondition(NR, value,wc);
    }
  }

  virtual void getVertexMass(std::vector<Dof> &R, std::vector<typename dofManager<T>::dataVec> &vmass){
    if(this->_current->isAllocated()){
      std::map<Dof, int>::const_iterator it;
      double mass;
      for(int i=0;i<R.size(); i++){
        it = this->unknown.find(R[i]);
        if(it != this->unknown.end()){
          _NLScurrent->getFromMatrix(it->second,it->second,mass,nonLinearSystem<T>::mass);
          vmass.push_back(mass);
        }
        else{  // find mass in _fixedmass
          typename std::map<Dof,FixedNodeDynamicData<T> >::iterator itm = this->_fixedDynamic.find(R[i]);
          if(itm != this->_fixedDynamic.end()) vmass.push_back(itm->second.mass);
          else vmass.push_back(0.);
        }
      }
    }
    else{
      for(int i=0;i<R.size(); i++) vmass.push_back(0.);
    }
  }

    // Function to get global kinetic energy. Used vector PETSC function so it's more quick
  virtual double getKineticEnergy(const int systemSizeWithoutRigidContact,FilterDofSet &fildofcontact) const{
    double ener = _NLScurrent->getKineticEnergy(systemSizeWithoutRigidContact); // energy of free dofs
    // add energy of fixed dof
    for(typename std::map<Dof,FixedNodeDynamicData<T> >::const_iterator it = this->_fixedDynamic.begin(); it!=this->_fixedDynamic.end(); ++it){
      if(!fildofcontact(it->first)){ // remove energy of the rigid body (contact)
        double mv = it->second.mass;
        double v =  it->second.velocity;
        ener += 0.5*mv*v*v;
      }
    }
    return ener;
  }

  virtual void printRightHandSide() const{
    Msg::Info("Right Hand Side");
    for(std::map<Dof,int>::const_iterator it=this->unknown.begin(); it!=this->unknown.end(); ++it){
      double force;
      _NLScurrent->getFromRightHandSide(it->second,force);
      Msg::Info("Dof Ent: %d Dof Type %d value %e",it->first.getEntity(),it->first.getType(),force);
    }
  }


  virtual void getFixedRightHandSide(std::vector<Dof> &R, std::vector<T> &val){
    val.clear(); R.clear();
    for(typename std::map<Dof, typename dofManager<T>::dataVec>::iterator it = RHSfixed.begin(); it != RHSfixed.end(); ++it){
      R.push_back(it->first);
      val.push_back(it->second);
    }
  }

  virtual void getRightHandSidePlus(Dof R, typename dofManager<T>::dataVec &val){
    // plus is the Fext vector so normally the dof related are not fixed (neumann BC)
    std::map<Dof,int>::const_iterator it = this->unknown.find(R);
    if(it != this->unknown.end()){
      _NLScurrent->getFromRightHandSidePlus(it->second,val);
    }
    else{
    #ifdef _DEBUG
      Msg::Warning("Try to get external forces of a fixed Dof");
    #endif
      val = 0.;
    }
  }
  virtual void getRightHandSidePlus(const std::vector<Dof> &R, std::vector<typename dofManager<T>::dataVec> &val){
    // plus is the Fext vector so normally the dof related are not fixed (neumann BC)
    val.resize(R.size());
    for(int i=0;i<R.size();i++)
      this->getRightHandSidePlus(R[i],val[i]);
  }

  virtual void setInitial(const std::vector<Dof> &R, const std::vector<double> &disp){
    for(int i=0;i<R.size();i++){
      typename std::map<Dof,int>::const_iterator it = this->unknown.find(R[i]);
      if(it != this->unknown.end()){
        _NLScurrent->setInitialValues(it->second,disp[i]);
      }
    }
  }
  virtual int systemSolveIntReturn(){
   #if defined(HAVE_MPI)
    if (_isPartitioned)
      if(!this->_mpiInit) this->initMPI();
   #endif //HAVE_MPI
    int suc = this->_current->systemSolve();
   #if defined(HAVE_MPI)
    if (_isPartitioned)
      this->systemMPIComm();
   #endif // HAVE_MPI
    return suc;
  }

  virtual void systemSolve(){
  // do mpi communication before system resolution (contact)
 #if defined(HAVE_MPI)
  if(!this->_mpiInit) this->initMPI();
  if(this->_mpiRigidContactSize != 0){
    // copie the value before transfert
    T* firstContactDof = _NLScurrent->getArrayIndexPreviousRightHandSidePlusMPI(this->_firstRigidContactDof);
   #if defined(HAVE_BLAS)
    int INC = 1;
    F77NAME(dcopy)(&(this->_mpiRigidContactSize),firstContactDof,&INC,this->_mpiRigidContactTMP,&INC);
   #else
    for(int i=0;i<_mpiRigidContactSize;i++)
      this->_mpiRigidContactTMP[i] = firstContactDof[i];
   #endif // HAVE_BLAS
    // sum contribution of all cpu for the rigid contact unknown
    MPI_Allreduce(this->_mpiRigidContactTMP,firstContactDof,this->_mpiRigidContactSize,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  }
 #endif // HAVE_MPI
  dofManager<T>::systemSolve();
  if(_scheme!=nonLinearMechSolver::Explicit) // no convergence in Explicit
    Msg::Warning("Void System Solve return. No succeed garantee Use systemSolveIntReturn");
 #if defined(HAVE_MPI)
  this->systemMPIComm();
 #endif // HAVE_MPI
 }

  virtual inline void getDofValuePrevious(const Dof &key,typename dofManager<T>::dataVec &val) const
  {
    if(_NLScurrent == NULL)
    {
      Msg::Error("Cannot get previous dof in a linear system");
      val = 0.;
      return;
    }
    typename std::map<Dof, FixedNodeDynamicData<T> >::const_iterator it = this->_fixedDynamic.find(key);
    if (it != this->_fixedDynamic.end()) {
     #ifdef _DEBUG
      if(this->_scheme == nonLinearMechSolver::Explicit)
      {
        Msg::Error("The system is explicit so the fixed value is the current and not the previous one! Fix this");
      }
     #endif // _DEBUG
      val =  it->second.position;
      return ;
    }
   #if defined(HAVE_MPI)
    if(key.getType() < 0)
    {
       std::map<Dof,int>::const_iterator itR = this->_otherPartUnknowns.find(key);
       if(itR == _otherPartUnknowns.end())
         Msg::Error("Can't get a value of ghost unknown entity: %d type %d from rank %d systype %d",key.getEntity(),key.getType(),Msg::GetCommRank(),this->_scheme);
       val = this->_previousOtherPartPositions[itR->second];
       return;
    }
   #endif // HAVE_MPI
    std::map<Dof, int>::const_iterator itK = this->unknown.find(key);
    if (itK != this->unknown.end()) {
      _NLScurrent->getFromPreviousSolution(itK->second, val);
      return;
    }
  }

  virtual inline void getDofValue(Dof key,  typename dofManager<T>::dataVec &val) const
  {
    this->getDofValue(key,val,nonLinearBoundaryCondition::position);
  }

  virtual inline void getDofValue(Dof key,  typename dofManager<T>::dataVec &val,
                                  nonLinearBoundaryCondition::whichCondition wv) const
  {
   switch(wv){
    case nonLinearBoundaryCondition::position:
     {
       // look first for fixed dof (the type can be negative)
       typename std::map<Dof, typename dofManager<T>::dataVec>::const_iterator it = this->fixed.find(key);
       if (it != this->fixed.end()) {
         val =  it->second;
         return ;
       }
       #if defined(HAVE_MPI)
       if(key.getType() < 0)
       {
          std::map<Dof,int>::const_iterator itR = this->_otherPartUnknowns.find(key);
//          #ifdef _DEBUG
          if(itR == _otherPartUnknowns.end())
            Msg::Error("Can't get a value of ghost unknown entity: %d type %d from rank %d systype %d",key.getEntity(),key.getType(),Msg::GetCommRank(),this->_scheme);
//          #endif // _DEBUG
          val = this->_otherPartPositions[itR->second];
          return;
       }
       #endif // HAVE_MPI
       std::map<Dof, int>::const_iterator itK = this->unknown.find(key);
       if (itK != this->unknown.end()) {
         _NLScurrent->getFromSolution(itK->second, val,wv);
         return;
       }
       typename std::map<Dof, DofAffineConstraint< typename dofManager<T>::dataVec > >::const_iterator itC =
                  this->constraints.find(key);
       if (itC != this->constraints.end())
       {
          typename dofManager<T>::dataVec tmp(val);
          val = itC->second.shift;
          for (unsigned i=0;i<(itC->second).linear.size();i++)
          {
             std::map<Dof, int>::const_iterator itu = this->unknown.find(((itC->second).linear[i]).first);
             getDofValue(((itC->second).linear[i]).first, tmp);
             dofTraits<T>::gemm(val,((itC->second).linear[i]).second, tmp, 1, 1);
          }
          return ;
       }
     }
     break;
     case nonLinearBoundaryCondition::velocity:
     {
       // look first for fixed Dof
       typename std::map<Dof, FixedNodeDynamicData<T> >::const_iterator it = this->_fixedDynamic.find(key);
       if (it != this->_fixedDynamic.end()) {
         val =  it->second.velocity;
         return ;
       }
      #if defined(HAVE_MPI)
       if(key.getType() < 0)
       {
          std::map<Dof,int>::const_iterator itR = this->_otherPartUnknowns.find(key);
          val = this->_otherPartVelocities[itR->second];
          return;
       }
      #endif // HAVE_MPI
       std::map<Dof, int>::const_iterator itK = this->unknown.find(key);
       if (itK != this->unknown.end()) {
         _NLScurrent->getFromSolution(itK->second, val,wv);
         return;
       }
     }
     break;
     case nonLinearBoundaryCondition::acceleration:
     {
       // look first for fixed Dof
       typename std::map<Dof, FixedNodeDynamicData<T> >::const_iterator it = this->_fixedDynamic.find(key);
       if (it != this->_fixedDynamic.end()) {
         val =  it->second.acceleration;
         return ;
       }
      #if defined(HAVE_MPI)
      if(key.getType() < 0)
       {
          std::map<Dof,int>::const_iterator itR = this->_otherPartUnknowns.find(key);
          val = this->_otherPartAccelerations[itR->second];
          return;
       }
      #endif // HAVE_MPI
       std::map<Dof, int>::const_iterator itK = this->unknown.find(key);
       if (itK != this->unknown.end()) {
         _NLScurrent->getFromSolution(itK->second, val,wv);
         return;
       }
     }
     break;
    }
  }

  virtual inline void getDofValue(std::vector<Dof> &keys,std::vector<typename dofManager<T>::dataVec> &Vals) const{
    this->getDofValue(keys,Vals,nonLinearBoundaryCondition::position);
  }

  virtual inline void getDofValue(std::vector<Dof> &keys,std::vector<typename dofManager<T>::dataVec> &Vals,
                                    const typename nonLinearBoundaryCondition::whichCondition wv) const
  {
    int ndofs=keys.size();
    size_t originalSize = Vals.size();
    Vals.resize(originalSize+ndofs);
    for (int i=0;i<ndofs;++i) getDofValue(keys[i], Vals[originalSize+i],wv);
  }

  virtual inline double normInfRightHandSide()
  {
    this->_current->normInfRightHandSide();
  }
  virtual inline double norm0Inf()
  {
    return _NLScurrent->norm0Inf();
  }
  virtual inline void zeroMatrix()
  {
    this->_current->zeroMatrix();
  }
  virtual inline int lineSearch()
  {
    return _NLScurrent->lineSearch();
  }
  virtual inline void resetUnknownsToPreviousTimeStep()
  {
    _NLScurrent->resetUnknownsToPreviousTimeStep();
  }
  virtual inline void nextStep()
  {
    _NLScurrent->nextStep();
    #if defined(HAVE_MPI)
     #if defined(HAVE_BLAS)
      int one = 1.;
      int mysize = _otherPartUnknowns.size();
      F77NAME(dcopy)(&mysize,_otherPartPositions,&one,_previousOtherPartPositions,&one);
     #else
      for(int i=0;i<_otherPartUnknowns.size();i++)
        _previousOtherPartPositions[i] = _otherPartPositions[i];
     #endif // HAVE_BLAS

    #endif // HAVE_MPI
  }
  virtual inline void setTimeStep(const double dt)
  {
    _NLScurrent->setTimeStep(dt);
  }
  virtual inline void createRestart()
  {
    _NLScurrent->createRestart();
  }
 #if defined(HAVE_MPI)
  virtual void manageMPIComm(const int otherPartNum,const std::vector<Dof> &otherPartR)
  {
    // create the map of unknown to recevieve from cpu otherPartNum
    std::set<Dof> &RecvSetDof = _mapMPIRecvComm[otherPartNum];
    for(int i=0; i<otherPartR.size(); i++)
    {
      std::map<Dof,int>::const_iterator itR = _otherPartUnknowns.find(otherPartR[i]);
      std::map<Dof,double>::const_iterator itRfixed = this->fixed.find(otherPartR[i]);
      if((itR == _otherPartUnknowns.end()) and (itRfixed == this->fixed.end())) // add a communication Dof (otherwise it is already the case)
      {
        unsigned int size = _otherPartUnknowns.size();
        _otherPartUnknowns[otherPartR[i]] = size;
        RecvSetDof.insert(otherPartR[i]);
      }
    }
  }

  virtual void systemMPIComm()
  {
    if(!_mpiInit) this->initMPI();
    // mpi communication post resolution (exchange dof values)
    for(int i=0;i<Msg::GetCommSize();i++)
    {
      // first recieve from lowest rank
      T valpos; // to get value from system
      T valvelo;
      T valacc;
      int nbdofPerNode =1;
      if(_scheme == nonLinearMechSolver::Explicit)
      {
        nbdofPerNode+=2;
      }
      MPI_Status status;
      if(i < Msg::GetCommRank())
      {
         // dim of buffer;
         std::set<Dof> &RecvSetDof = _mapMPIRecvComm[i];
         int commSize = nbdofPerNode*RecvSetDof.size();
         if(commSize > 0) // otherwise nothing to do
         {
           if(_sizeMPIBuffer < commSize)
           {
              if(_sizeMPIBuffer != 0) delete []_mpiBuffer;
              _sizeMPIBuffer = commSize;
              _mpiBuffer = new T[commSize];
           }
           MPI_Recv(_mpiBuffer,commSize,MPI_DOUBLE,i,i,MPI_COMM_WORLD,&status);
           // put val in _otherPartUnknowns
           int j=0;
           for(std::set<Dof>::const_iterator itR = RecvSetDof.begin(); itR!=RecvSetDof.end(); ++itR)
           {
             std::map<Dof,int>::const_iterator it = _otherPartUnknowns.find(*itR);
             _otherPartPositions[it->second] = this->_mpiBuffer[j];
             if(_scheme == nonLinearMechSolver::Explicit)
             {
               _otherPartVelocities[it->second]= this->_mpiBuffer[j+1];
               _otherPartAccelerations[it->second] = this->_mpiBuffer[j+2];
             }
             j+=nbdofPerNode;
           }
         }
        // send
        std::vector<int> &mapDof = *(_mapMPIDof[i]);
        commSize = nbdofPerNode*mapDof.size();
        if(commSize > 0)
        {
          if(_sizeMPIBuffer < commSize)
          {
            if(_sizeMPIBuffer != 0) delete []_mpiBuffer;
            _sizeMPIBuffer = commSize;
            _mpiBuffer = new T[commSize];
          }
          int j=0;
          for(int k=0; k< mapDof.size(); k++)
          {
            if(_scheme == nonLinearMechSolver::Explicit)
            {
              _NLScurrent->getFromSolution(mapDof[k], valpos,nonLinearBoundaryCondition::position);
              _NLScurrent->getFromSolution(mapDof[k], valvelo,nonLinearBoundaryCondition::velocity);
              _NLScurrent->getFromSolution(mapDof[k], valacc,nonLinearBoundaryCondition::acceleration);
              _mpiBuffer[j] = valpos;
              _mpiBuffer[j+1] = valvelo;
              _mpiBuffer[j+2] = valacc;
            }
            else
            {
              (this->_current)->getFromSolution(mapDof[k], valpos);
              _mpiBuffer[j] = valpos;
            }
            j+=nbdofPerNode;
          }
          MPI_Send(_mpiBuffer,commSize,MPI_DOUBLE,i,Msg::GetCommRank(),MPI_COMM_WORLD);
        }
      }
      else if(i > Msg::GetCommRank()) // if i == Rank nothing to do (no self communication)
      {
        // send
        const std::vector<int> &mapDof = *(_mapMPIDof[i]);
        int commSize = nbdofPerNode*mapDof.size();
        if(commSize > 0)
        {
          if(_sizeMPIBuffer < commSize)
          {
            if(_sizeMPIBuffer != 0) delete []_mpiBuffer;
            _sizeMPIBuffer = commSize;
            _mpiBuffer = new T[commSize];
          }
          int j=0;
          for(int k=0; k<mapDof.size(); k++)
          {
            if(_scheme == nonLinearMechSolver::Explicit)
            {
              _NLScurrent->getFromSolution(mapDof[k], valpos,nonLinearBoundaryCondition::position);
              _NLScurrent->getFromSolution(mapDof[k], valvelo,nonLinearBoundaryCondition::velocity);
              _NLScurrent->getFromSolution(mapDof[k], valacc,nonLinearBoundaryCondition::acceleration);
              _mpiBuffer[j] = valpos;
              _mpiBuffer[j+1] = valvelo;
              _mpiBuffer[j+2] = valacc;
            }
            else
            {
              this->_current->getFromSolution(mapDof[k], valpos);
              _mpiBuffer[j] = valpos;
            }
            j+=nbdofPerNode;
          }
          MPI_Send(_mpiBuffer,commSize,MPI_DOUBLE,i,Msg::GetCommRank(),MPI_COMM_WORLD);
        }
        // recieve
        std::set<Dof> &RecvSetDof = _mapMPIRecvComm[i];
        commSize = nbdofPerNode*RecvSetDof.size();
        if(commSize > 0) // otherwise nothing to do
        {
          if(_sizeMPIBuffer < commSize)
          {
             if(_sizeMPIBuffer != 0) delete []_mpiBuffer;
             _sizeMPIBuffer = commSize;
             _mpiBuffer = new T[commSize];
          }
          MPI_Recv(_mpiBuffer,commSize,MPI_DOUBLE,i,i,MPI_COMM_WORLD,&status);
          // put val in _otherPartUnknowns
          int j=0;
          for(std::set<Dof>::const_iterator itR = RecvSetDof.begin(); itR!=RecvSetDof.end(); ++itR)
          {
            std::map<Dof,int>::const_iterator it = _otherPartUnknowns.find(*itR);
            if(_scheme == nonLinearMechSolver::Explicit)
            {
              _otherPartPositions[it->second] = this->_mpiBuffer[j];
              _otherPartVelocities[it->second]= this->_mpiBuffer[j+1];
              _otherPartAccelerations[it->second] = this->_mpiBuffer[j+2];
            }
            else
            {
              _otherPartPositions[it->second] = _mpiBuffer[j];
            }
            j+=nbdofPerNode;
          }
        }
      }
    }
  }
 #endif // HAVE_MPI
};
#endif // STATICDOFMANAGERSYSTEM_H_

