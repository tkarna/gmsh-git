//
//
// Description: Filter element in a box defined by 2 points (x0,y0,z0) and (x1,y1,z1)
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#include "elementFilterBox.h"

elementFilterBox::elementFilterBox(const double x0,const double y0,const double z0,
                                   const double x1,const double y1,const double z1) : elementFilter(), _pt0(x0,y0,z0), _pt1(x1,y1,z1)
{
  if(x0 < x1)
  {
    xmin = x0;
    xmax = x1;
  }
  else
  {
    xmin = x1;
    xmax = x0;
  }

  if(y0 < y1)
  {
    ymin = y0;
    ymax = y1;
  }
  else
  {
    ymin = y1;
    ymax = y0;
  }

  if(z0 < z1)
  {
    zmin = z0;
    zmax = z1;
  }
  else
  {
      zmin = z1;
      zmax = z0;
  }
}

bool elementFilterBox::operator() (MElement *ele) const
{
  SPoint3 bary = ele->barycenter();
  if(( (xmin < bary.x() ) and (bary.x() < xmax) ) and
     ( (ymin < bary.y() ) and (bary.y() < ymax)) and
     ( (zmin < bary.z() ) and (bary.z() < zmax))) return true;
  else
    return false;
}
