#ifndef HOMOGENIZEDDATA_H_
#define HOMOGENIZEDDATA_H_

#include "STensor3.h"
#include "STensor33.h"
#include "STensor43.h"
#include "STensor53.h"
#include "STensor63.h"

#include "highOrderTensor.h"

class homogenizedData {
  public:
    STensor3   _stress;
		STensor33  _highStress;
    // homogenized tangents
    STensor43 _tangent;
		STensor53 _tangentFG, _tangentGF;
		STensor63 _tangentGG;

  public:
    homogenizedData(): _stress(0.),_highStress(0.), _tangent(0.),
                      _tangentFG(0.),_tangentGF(0.),_tangentGG(0.){};
    homogenizedData(const homogenizedData& src): _stress(src._stress),
                      _highStress(src._highStress), _tangent(src._tangent),
                      _tangentFG(src._tangentFG),_tangentGF(src._tangentGF),
                      _tangentGG(src._tangentGG){};
    homogenizedData& operator = (const homogenizedData& src){
      _stress = src._stress;
      _highStress = src._highStress;
      _tangent = src._tangent;
      _tangentFG = src._tangentFG;
      _tangentGF = src._tangentGF;
      _tangentGG = src._tangentGG;
      return *this;
    };
};

class homogenizedDataFiles{
  public:
    FILE* _firstStrainFile, *_secondStrainFile,
        *_firstStressFile, *_secondStressFile, *_firstFirstTangentFile,
        *_firstSecondTangentFile, *_secondFirstTangentFile, *_secondSecondTangentFile;

  public:
    homogenizedDataFiles() : _firstStrainFile(NULL),_secondStrainFile(NULL),
                            _firstStressFile(NULL),_secondStressFile(NULL),
                            _firstFirstTangentFile(NULL),_firstSecondTangentFile(NULL),
                            _secondFirstTangentFile(NULL),_secondSecondTangentFile(NULL){};
    ~homogenizedDataFiles(){}
    void openFiles(const int _enum, const int _gnum, const int order,
                  const bool strain, const bool stress){
      if (strain == true){
        std::string filename  = "E_"+int2str(_enum)+"_GP_"+int2str(_gnum)+"_strain.csv";
        _firstStrainFile = Tensor23::createFile(filename);

        if (order>1){
          filename = "E_"+int2str(_enum)+"_GP_"+int2str(_gnum)+"_second_strain.csv";
          _secondStrainFile = Tensor33::createFile(filename);
        }

      }

      if (stress == true){
        std::string filename =  "E_"+int2str(_enum)+"_GP_"+int2str(_gnum)+"_stress.csv";
        _firstStressFile = Tensor23::createFile(filename);

        filename = "E_"+int2str(_enum)+"_GP_"+int2str(_gnum)+"_tangent.csv";
        _firstFirstTangentFile = Tensor43::createFile(filename);

        if (order>1){
          filename = "E_"+int2str(_enum)+"_GP_"+int2str(_gnum)+"_second_stress.csv";
          _secondStressFile = Tensor33::createFile(filename);

          filename = "E_"+int2str(_enum)+"_GP_"+int2str(_gnum)+"_firstsecond_tangent.csv";
          _firstSecondTangentFile = Tensor53::createFile(filename);

          filename = "E_"+int2str(_enum)+"_GP_"+int2str(_gnum)+"_secondfirst_tangent.csv";
          _secondFirstTangentFile = Tensor53::createFile(filename);

          filename = "E_"+int2str(_enum)+"_GP_"+int2str(_gnum)+"_second_tangent.csv";
          _secondSecondTangentFile = Tensor63::createFile(filename);
        }
      };

    };
    void closeFiles(){
      if (_firstStrainFile!= NULL) fclose(_firstStrainFile);
      if (_firstStressFile!= NULL) fclose(_firstStressFile);

      if (_secondStrainFile!= NULL) fclose(_secondStrainFile);
      if (_secondStressFile!= NULL) fclose(_secondStressFile);

      if (_firstFirstTangentFile!= NULL) fclose(_firstFirstTangentFile);
      if (_firstSecondTangentFile!= NULL) fclose(_firstSecondTangentFile);

      if (_secondFirstTangentFile!= NULL) fclose(_secondFirstTangentFile);
      if (_secondSecondTangentFile!= NULL) fclose(_secondSecondTangentFile);
    };

};

#endif // HOMOGENIZEDDATA_H_
