//
// Description: System for non linear static scheme
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
// The system solve K Delta x = F so add a vector xsol which contains the actual value of unknowns
// Only thing that change is the solve function (add operation _xsol += _x)
// The allocation (add the allocation of _xsol)
// The getFromSolution (return _xsol[i] in place of _x[i])
// clear (destroy also _xsol)
// Add a function to set initial values (has to derived from two types HOW TO AVOID THIS)
// Define two members of RightHandSide (optimization for archving and computation of energy)
// to have direct access to Fext and Fint. At begining of system _b = Fext - Fint

#include "nonLinearBC.h"
#include "ipstate.h"

#ifndef _NONLINEARSYSTEMS_H_
#define _NONLINEARSYSTEMS_H_
//#if defined(HAVE_MPI)
//#include "mpi.h"
//#endif // HAVE_MPI
template<class scalar>
class nonLinearSystem{
 public:
  enum rhs{Fext,Fint,LoadVector} ;
  enum whichMatrix{stiff, mass};
  nonLinearSystem(){}
  virtual ~nonLinearSystem(){}
  virtual double getInstabilityCriterion() const {return 1.;};
  virtual void setInitialValues(const int i, scalar val)=0;
  virtual void getFromPreviousSolution(int row, scalar &val) const=0; // needed to initialize velocities in case of switch
  virtual void getFromRightHandSidePlus(int row, scalar &val) const=0;
  virtual void getFromRightHandSideMinus(int row, scalar &val) const=0;
  virtual void getFromRightHandSide(int row, scalar &val) const=0;
  virtual void addToRightHandSidePlus(int row, const scalar &val)=0;
  virtual void addToLoadVector(int row, const scalar& val){} // this used in path following context
  virtual void zeroLoadVector() {}
  virtual void addToRightHandSideMinus(int row, const scalar &val)=0;
  virtual double norm0Inf() const=0;
  virtual int lineSearch(){return 1;} // if 1 the process in converged explicit no lineSearch so OK
  virtual void resetUnknownsToPreviousTimeStep()=0;
  virtual void copy(const IPStateBase::whichState ws1, const IPStateBase::whichState ws2){}
  virtual void nextStep()=0;
  virtual void setTimeStep(const double dt)=0;
  virtual double getTimeStep() const=0;
  virtual void createRestart(){Msg::Error("A restart file cannot be created as the function is not implemented for this scheme");}
  virtual double getExternalWork(const int syssize) const=0;
  virtual double getKineticEnergy(const int syssize) const=0;
  virtual void addToMatrix(int row, int col, const scalar &val, const typename nonLinearSystem<scalar>::whichMatrix wm)=0;
  virtual void setInitialCondition(int row, scalar val, const typename nonLinearBoundaryCondition::whichCondition wc=nonLinearBoundaryCondition::position)=0;
  virtual void setDynamicRelaxation(const bool dynrel)=0;
  virtual void getFromSolution(int row, scalar &val, const nonLinearBoundaryCondition::whichCondition wv) const=0;
  virtual void getFromMatrix(int row, int col, scalar &val,const typename nonLinearSystem<scalar>::whichMatrix wm) const=0;
  #if defined(HAVE_MPI)
  virtual double* getArrayIndexPreviousRightHandSidePlusMPI(const int index)=0;
//  virtual void setPreviousRightHandSidePlusMPI(const int row, const scalar &val)=0;
  #endif // HAVE_MPI

};
// petsc
#include "linearSystemPETSc.h"
#include "linearSystemPETSc.hpp"

// to debug
#if defined(HAVE_PETSC)
static void nlprintPETScVector(Vec &a,int vsize,const char *vname){
  double val;
  for(int i=0; i<vsize;i++){
    VecGetValues(a, 1, &i, &val);
    printf("%s comp %d %e\n",vname,i,val);
  }
  printf("---\n");
}
static void nlprintPETScMatrix(Mat &a,int rsize, int csize,const char *mname)
{
  double val;
  for(int i=0;i<rsize;++i){
    for(int j=0;j<csize;++j){
      MatGetValues(a, 1, &i, 1, &j, &val);
      printf("%s(%d,%d) = %e\n",mname,i,j,val);
    }
  }
  printf("----\n");
}

#endif // HAVE_PETSC
template<class scalar>
class nonLinearSystemPETSc : public linearSystemPETSc<scalar>, public nonLinearSystem<scalar>{
  #if defined(HAVE_PETSC)
 protected:
  bool _allocatednldata;
  mutable bool _flagb;
 protected:
  Vec _xsol;  // _xsol = _xitels + alpha * _x; (alpha == line search parameter)
  Vec _xitels; // for line search the value of solution as to be kept
  Vec _xprev; // solution of previous time step
  Vec _xinit;// solution of initial time step
  Vec _xtemp; // temp step for cache data
  Vec _Fext;
  Vec _Fint;
  double _timeStep;
  // To compute external work
  mutable Vec _Fextprev;
  mutable Vec _Deltax; // u_{n+1} - u_n
  mutable Vec _FextpFextprev;
  // data for line search
  const double _Tolls; // tolerance for line search
  bool _lineSearch;
  double _alphaRneg, _alphaRpos, _alphaLsIte, _alphaLsPrev;
  PetscScalar _resNeg, _resPos, _resLsIte, _res0;
  int _lsite,_lsitemax;
  int _nbRows; // can be usefull to know the system's size for debug operation
 public:
  nonLinearSystemPETSc(MPI_Comm com = PETSC_COMM_WORLD, const bool ls=false, const double tolls=1e-6,
                       const int lsitemax=10) : linearSystemPETSc<scalar>(com), nonLinearSystem<scalar>(),
                                                                 _allocatednldata(false), _flagb(false),
                                                                 _lineSearch(ls), _Tolls(tolls),
                                                                 _alphaRneg(0.), _alphaRpos(1.), _resNeg(0.), _resPos(0), _lsite(0),
                                                                 _alphaLsIte(0.), _resLsIte(0.), _res0(1.),
                                                                 _alphaLsPrev(1.), _lsitemax(lsitemax), _timeStep(0.),
                                                                 _nbRows(0)
  {
    this->_parameters["matrix_reuse"] = "same_sparsity";
  }
  ~nonLinearSystemPETSc(){} // _xsol is destroied by clear() called by ~linearSystemPETSc()

  Vec& getState(const IPStateBase::whichState ws) {
    if (ws == IPStateBase::initial) return _xinit;
    else if (ws == IPStateBase::previous) return _xprev;
    else if (ws == IPStateBase::current) return _xsol;
    else if (ws == IPStateBase::temp) return _xtemp;
    else{
      Msg::Error("This state is not defined");
    }
  }

  Vec& getRHS(const typename nonLinearSystem<scalar>::rhs wrhs){
    if (wrhs == nonLinearSystem<scalar>::Fint) return _Fint;
    else if (wrhs == nonLinearSystem<scalar>::Fext) return _Fext;
    else Msg::Error("This RHS is not defined");
  }

  virtual void copy(const IPStateBase::whichState source,
                      const IPStateBase::whichState destination){
    Vec& src = this->getState(source);
    Vec& dst = this->getState(destination);
    _try(VecCopy(src,dst));
  }
  virtual void allocate(int nbRows){
    linearSystemPETSc<scalar>::allocate(nbRows);
    // To avoid a segmentation fault with version petsc3.3 in // computation Better fix ??
   #if (((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)))
    if(Msg::GetCommSize()>1){
      _try(MatSetUp(this->_a));
    }
   #endif // (PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)
    _try(VecDuplicate(this->_x, &_xsol));
    _try(VecDuplicate(this->_x, &_xprev));
    _try(VecDuplicate(this->_x, &_xinit));
    _try(VecDuplicate(this->_x, &_xtemp));
    _try(VecDuplicate(this->_x, &_xitels));
    _try(VecDuplicate(this->_x, &_Fext));
    _try(VecDuplicate(this->_x, &_Fint));
    _try(VecDuplicate(this->_x, &_Fextprev));
    _try(VecDuplicate(this->_x, &_Deltax));
    _try(VecDuplicate(this->_x, &_FextpFextprev));
    _nbRows = nbRows;
    _allocatednldata=true;
    _try(VecAssemblyBegin(_Fextprev));
    _try(VecAssemblyEnd(_Fextprev));
    _try(VecZeroEntries(_Fextprev));
    _try(VecAssemblyBegin(_xprev));
    _try(VecAssemblyEnd(_xprev));
    _try(VecZeroEntries(_xprev));

    _try(VecAssemblyBegin(_xinit));
    _try(VecAssemblyEnd(_xinit));
    _try(VecZeroEntries(_xinit));
  }
  virtual void clear(){
    linearSystemPETSc<scalar>::clear();
    if(_allocatednldata){
      _try(VecDestroy(&_xsol));
      _try(VecDestroy(&_xprev));
      _try(VecDestroy(&_xinit));
      _try(VecDestroy(&_xtemp));
      _try(VecDestroy(&_xitels));
      _try(VecDestroy(&_Fext));
      _try(VecDestroy(&_Fextprev));
      _try(VecDestroy(&_Fint));
      _try(VecDestroy(&_FextpFextprev));
      _try(VecDestroy(&_Deltax));
    }
    _nbRows = 0;
    _allocatednldata = false;
    _flagb = false;
  }
  virtual void getFromSolution(int row, scalar &val) const
  {
    #if defined(PETSC_USE_COMPLEX)
    PetscScalar *tmp;
    _try(VecGetArray(_xsol, &tmp));
    PetscScalar s = tmp[row];
    _try(VecRestoreArray(_xsol, &tmp));
    val = s.real();
    #else
      _try(VecGetValues(_xsol, 1, &row, &val));
    #endif //PETSC_USE_COMPLEX
  }
  virtual void getFromPreviousSolution(int row, scalar &val)  const
  {
   #if defined(PETSC_USE_COMPLEX)
    PetscScalar *tmp;
    _try(VecGetArray(_xprev, &tmp));
    PetscScalar s = tmp[row];
    _try(VecRestoreArray(_xprev, &tmp));
    val = s.real();
    #else
      _try(VecGetValues(_xprev, 1, &row, &val));
    #endif //PETSC_USE_COMPLEX
  }
  virtual int systemSolve(){
    // _b = _Fext - _Fint Normally done via the called of normInfRightHandSide
    if(!_flagb){
     #if defined(HAVE_MPI)
      if(Msg::GetCommSize() > 1)
      {
        _try(VecAssemblyBegin(_Fext));
        _try(VecAssemblyEnd(_Fext));
        _try(VecAssemblyBegin(_Fint));
        _try(VecAssemblyEnd(_Fint));
      }
     #endif // HAVE_MPI
      _try(VecCopy(_Fext,this->_b));
      _try(VecAXPY(this->_b,-1,_Fint));
      _flagb = true;
    }

    // solve system
    linearSystemPETSc<scalar>::systemSolve();

    // print the matrix (debug purpose)
    // nlprintPETScMatrix(this->_a,_nbRows,_nbRows,"K");

    // initialisation of line search
    if(_lineSearch)
    {
      _try(VecCopy(_xsol,_xitels)); // copy last solution vector
      _try(VecDot(this->_b,this->_x,&_resNeg));
      _resNeg =-_resNeg; // because b == - residu;
      // other initial values
      _alphaRneg = 0.;
      _alphaRpos = 1.;
      _alphaLsPrev = 1.;
      _alphaLsIte = 1.;
      _resPos = 0.;
      _res0 = _resNeg;
      _lsite = 0;
    }

    // update xsol (for line search alpha0 == 1)
    _try(VecAXPY(_xsol,1.,this->_x));
//    nlprintPETScVector(_xsol,_nbRows,"sol");
    return 1;
  }
  virtual void setInitialValues(const int i, scalar val){
    if(this->isAllocated())
      _try(VecSetValues(_xsol, 1, &i, &val, INSERT_VALUES));
    else{
      Msg::Error("Can't give an initial value in a non allocated system!");
    }
  }
  virtual void getFromRightHandSide(int row, scalar &val) const{
  #if defined(PETSC_USE_COMPLEX)
  PetscScalar *tmp;
  _try(VecGetArray(_Fext, &tmp));
  PetscScalar s = tmp[row];
  _try(VecRestoreArray(_Fext, &tmp));
  // FIXME specialize this routine
  val = s.real();
  _try(VecGetArray(_Fint, &tmp));
  PetscScalar s = tmp[row];
  _try(VecRestoreArray(_Fint, &tmp));
  val -= s.real();
#else
  scalar tmp;
  VecGetValues(_Fext, 1, &row, &tmp);
  val = tmp;
  VecGetValues(_Fint, 1, &row, &tmp);
  val -= tmp;
#endif
  }
  virtual void getFromRightHandSidePlus(int row, scalar &val) const{
#if defined(PETSC_USE_COMPLEX)
  PetscScalar *tmp;
  _try(VecGetArray(_Fext, &tmp));
  PetscScalar s = tmp[row];
  _try(VecRestoreArray(_Fext, &tmp));
  // FIXME specialize this routine
  val = s.real();
#else
  VecGetValues(_Fext, 1, &row, &val);
#endif
  }
  // retrieve the value _Fint and not -_Fint
  virtual void getFromRightHandSideMinus(int row, scalar &val) const{
#if defined(PETSC_USE_COMPLEX)
  PetscScalar *tmp;
  _try(VecGetArray(_Fint, &tmp));
  PetscScalar s = tmp[row];
  _try(VecRestoreArray(_Fint, &tmp));
  // FIXME specialize this routine
  val = s.real();
#else
  VecGetValues(_Fint, 1, &row, &val);
#endif
  }

  virtual void addToRightHandSide(int row, const scalar &val){ // kept for linear constraint
    PetscInt i = row;
    PetscScalar s = val;
//    #ifdef _DEBUG
//      Msg::Warning("addToRightHandSide is deprecated for non linear system please use addToRightHandSidePlus or addToRightHandSideMinus");
//    #endif
    _try(VecSetValues(_Fext, 1, &i, &s, ADD_VALUES));
  }
  virtual void addToSolution(int row, const scalar& val){
    PetscInt i = row;
    PetscScalar s = val;
    _try(VecSetValues(_xsol, 1, &i, &s, ADD_VALUES));
  }
  virtual void addToRightHandSidePlus(int row, const scalar &val){
    PetscInt i = row;
    PetscScalar s = val;
    _try(VecSetValues(_Fext, 1, &i, &s, ADD_VALUES));
  }
  virtual void addToRightHandSideMinus(int row, const scalar &val){
    PetscInt i = row;
    PetscScalar s = val;
    _try(VecSetValues(_Fint, 1, &i, &s, ADD_VALUES));
  }


  virtual void zeroRightHandSide(){
    linearSystemPETSc<scalar>::zeroRightHandSide();
    _flagb = false;
    if (_allocatednldata) {
      _try(VecAssemblyBegin(_Fext));
      _try(VecAssemblyEnd(_Fext));
      _try(VecZeroEntries(_Fext));
      _try(VecAssemblyBegin(_Fint));
      _try(VecAssemblyEnd(_Fint));
      _try(VecZeroEntries(_Fint));
    }
  }
  virtual double normInfSolution() const{
    PetscReal norm;
    _try(VecAssemblyBegin(_xsol));
    _try(VecAssemblyEnd(_xsol));
    _try(VecNorm(_xsol,NORM_INFINITY,&norm));
    return norm;
  }
  virtual double normInfRightHandSide() const{
    PetscReal nor;
    // compute b
    if(!_flagb){
      /* For MPI Assemblebegin and end have to be called */
     #if defined(HAVE_MPI)
      if(Msg::GetCommSize() > 1)
      {
        _try(VecAssemblyBegin(_Fext));
        _try(VecAssemblyEnd(_Fext));
        _try(VecAssemblyBegin(_Fint));
        _try(VecAssemblyEnd(_Fint));
      }
     #endif // HAVE_MPI

      _try(VecCopy(_Fext,this->_b));
      _try(VecAXPY(this->_b,-1,_Fint));
      _flagb = true;
    }
    return linearSystemPETSc<scalar>::normInfRightHandSide();
  }
  virtual double norm0Inf() const{
    PetscReal norFext;
    PetscReal norFint;
    _try(VecNorm(_Fext, NORM_INFINITY, &norFext));
    _try(VecNorm(_Fint, NORM_INFINITY, &norFint));
    return norFext + norFint;
  }
  // function for linear search iteration return 1 when line search is converged
  virtual int lineSearch()
  {
    if(!_lineSearch) return 1;
    // compute (Fext-Fint)
    Msg::Info("Line search iteration %d",_lsite);
    if(!_flagb){
      _try(VecCopy(_Fext,this->_b));
      _try(VecAXPY(this->_b,-1,_Fint));
    }
    _try(VecDot(this->_b,this->_x,&_resLsIte));
    Msg::Info("Residu of line search %e relatif res %e alpha %e",_resLsIte,_resLsIte/_res0,_alphaLsIte);
    _resLsIte = - _resLsIte; // due to definition of _b that is -residu
    // new alpha value
    if(_lsite == 0)
    {
      if(_resLsIte < 0)
      {
        _alphaRpos+=1.; // for first itration we want _resPos > 0 increase alpha from 1 if it is not the case
        _alphaLsIte = _alphaRpos;
        if(_alphaRpos > 3)
        {
          _alphaLsIte = 1.;
          _try(VecWAXPY(_xsol,_alphaLsIte,this->_x,_xitels));
          return 1;
        }
      }
      else
      {
        _lsite++; // increase iteration counter;
        _resPos = _resLsIte;
        // new alpha
        _alphaLsPrev = _alphaLsIte;
        _alphaRpos = _alphaLsIte;
        _alphaLsIte = _alphaRneg - _resNeg*(_alphaRpos-_alphaRneg)/(_resPos - _resNeg);
      }
    }
    else if(_lsite > _lsitemax) // divergence of process take 1
    {
      // new positions
      _alphaLsIte = 1.;
      _try(VecWAXPY(_xsol,_alphaLsIte,this->_x,_xitels));
      return 1;
    }
    else
    {
      _lsite++; // increase iteration counter;
      // check convergence on residu
      double relres = _resLsIte / _res0;
      if(relres < 0.) relres = -relres;
      if(relres < _Tolls) return 1; // achieve convergence
      // check convergence on alpha
      relres = (_alphaLsIte - _alphaLsPrev) / _alphaLsPrev;
      if(relres < 0.) relres = -relres;
      if(relres < _Tolls) return 1; // achieve convergence

      // update value of residu
      _alphaLsPrev = _alphaLsIte;
      if(_resLsIte <= 0.)
      {
        _resNeg = _resLsIte;
        _alphaRneg = _alphaLsIte;
      }
      else
      {
        _resPos = _resLsIte;
        _alphaRpos = _alphaLsIte;
      }
      // new alpha
      _alphaLsIte = _alphaRneg - _resNeg*(_alphaRpos-_alphaRneg)/(_resPos - _resNeg);
    }

    // new positions
    _try(VecWAXPY(_xsol,_alphaLsIte,this->_x,_xitels));
//    _try(VecWAXPY(_xsol,1.,this->_x,_xitels));
    return 0;
  }
  virtual void resetUnknownsToPreviousTimeStep()
  {
    _try(VecCopy(_xprev,_xsol));
  }
  virtual void nextStep()
  {
    /* For MPI Assemblebegin and end have to be called */
   #if defined(HAVE_MPI)
    if(Msg::GetCommSize() > 1)
    {
      _try(VecAssemblyBegin(_xsol));
      _try(VecAssemblyEnd(_xsol));
      _try(VecAssemblyBegin(_xprev));
      _try(VecAssemblyEnd(_xprev));
    }
   #endif // HAVE_MPI
    _try(VecCopy(_xsol,_xprev));
  }
  virtual double getExternalWork(const int syssize) const
  {
    PetscScalar wext;
    _try(VecCopy(_Fextprev,_FextpFextprev));
    _try(VecAXPY(_FextpFextprev,1,_Fext));
    _try(VecCopy(_xsol,_Deltax));
    _try(VecAXPY(_Deltax,-1,_xprev));
    // put zero for displacement of rigid contact node (avoid to take into account this contribution)
    PetscScalar s = 0.;
    for(int i=syssize; i<this->_nbRows;i++)
    {
      _try(VecSetValues(_Deltax, 1, &i, &s, INSERT_VALUES));
    }
     #if defined(HAVE_MPI)
      if(Msg::GetCommSize() > 1)
      {
        _try(VecAssemblyBegin(_Deltax));
        _try(VecAssemblyEnd(_Deltax));
      }
     #endif // HAVE_MPI
    _try(VecDot(_FextpFextprev,_Deltax,&wext));
    _try(VecCopy(_Fext,_Fextprev));
    return 0.5*wext;
  }
  virtual void setTimeStep(const double dt){_timeStep = dt;}
  virtual double getTimeStep() const{return _timeStep;}
 #else // define function of nonlinearSystem
  virtual void setInitialValues(const int i, scalar val){}
  virtual void getFromPreviousSolution(int row, scalar &val)  const{return 0.;}
  virtual void getFromRightHandSidePlus(int row, scalar &val) const{return 0.;}
  virtual void getFromRightHandSideMinus(int row, scalar &val) const{return 0.;}
  virtual void addToRightHandSidePlus(int row, const scalar &val){}
  virtual void addToRightHandSideMinus(int row, const scalar &val){}
  virtual double norm0Inf() const{return 0.;}
  virtual int lineSearch(){return 1;}
  virtual void resetUnknownsToPreviousTimeStep(){}
  virtual double getExternalWork(const int syssize) const{return 0.;}
  virtual void setTimeStep(const double dt){}
  virtual double getTimeStep() const{return 0.;}
 #endif // HAVEPETSC
  virtual double getKineticEnergy(const int syssize) const{return 0.;} // no kinetic energy in a QS scheme
  void addToMatrix(int row, int col, const scalar &val, const typename nonLinearSystem<scalar>::whichMatrix wm)
  {
   #ifdef _DEBUG
    if(wm!=nonLinearSystem<scalar>::stiff)
      Msg::Fatal("Try to assemble an inexisting matrix in a QS PETSc scheme");
    else
   #endif // _DEBUG
    this->linearSystemPETSc<scalar>::addToMatrix(row,col,val);
  }
  virtual void setInitialCondition(int row, scalar val, const typename nonLinearBoundaryCondition::whichCondition wc=nonLinearBoundaryCondition::position)
  {
    if(wc!=nonLinearBoundaryCondition::position)
    {
      Msg::Error("Can set a velocity or a aceleration in a QS scheme");
      return;
    }
    this->setInitialValues(row,val);
  }
  virtual void setDynamicRelaxation(const bool dynrel)
  {
    Msg::Error("Cannot use dynamic relaxation in an QS system");
  }
  virtual void getFromSolution(int row, scalar &val, const nonLinearBoundaryCondition::whichCondition wv) const
  {
    if(wv!=nonLinearBoundaryCondition::position)
    {
      Msg::Error("Cannot take velocity or acceleration in a QS scheme");
      val=0.;
      return;
    }
    this->getFromSolution(row,val);
  }

  virtual void getFromMatrix(int row, int col, scalar &val,const typename nonLinearSystem<scalar>::whichMatrix wm) const
  {
    if(wm!=nonLinearSystem<scalar>::stiff)
    {
      Msg::Error("A QS system has only a stiffness matrix. So cannot get a value in another matrix");
      val=0;
      return;
    }
    this->linearSystemPETSc<scalar>::getFromMatrix(row,col,val);
  }

  #if defined(HAVE_MPI)
  virtual double* getArrayIndexPreviousRightHandSidePlusMPI(const int index)
  {
    Msg::Fatal("This function cannot be used in a QS PETSc scheme"); // has to return a reference on an array Impossible with a _Vec PETSc
  }
  #endif // HAVE_MPI
};

// gmm
#include "linearSystemGMM.h"
template <class scalar>
class nonLinearSystemGmm : public linearSystemGmm<scalar>, public nonLinearSystem<scalar>{
 protected:
  mutable bool _flagb;
  std::vector<scalar> *_xsol;
  std::vector<scalar> *_xprev;
  std::vector<scalar> *_xitels;
  std::vector<scalar> *_Fext;
  std::vector<scalar> *_Fint;
  double _timeStep;
  // For external work
  mutable std::vector<scalar> *_Fextprev;
  const double _Tolls;
  const bool _lineSearch;
  double _alphaRneg, _alphaRpos, _alphaLsIte, _alphaLsPrev, _resNeg, _resPos, _resLsIte, _res0;
  int _lsite,_lsitemax;
 public:
  nonLinearSystemGmm(const bool ls=false, const bool tolls=1.e-2,
                     const int lsitemax = 10) : linearSystemGmm<scalar>(), nonLinearSystem<scalar>(),
                                                             _xsol(NULL), _xprev(NULL), _xitels(NULL), _Fext(NULL), _Fint(NULL),
                                                             _Fextprev(NULL), _flagb(false),
                                                             _lineSearch(ls), _Tolls(tolls), _alphaRneg(0.), _alphaRpos(1.),
                                                             _resNeg(0.), _resPos(0.), _lsite(0), _alphaLsIte(0.), _resLsIte(0.),
                                                             _res0(1.), _alphaLsPrev(1.),_lsitemax(lsitemax), _timeStep(0.) {}
  ~nonLinearSystemGmm(){} // // _xsol is destroied by clear() called by ~linearSystemGMM()
#if defined(HAVE_GMM)
  virtual void allocate(int nbRows)
  {
    linearSystemGmm<scalar>::allocate(nbRows);
    if(_xsol !=NULL) delete _xsol; // I don't known which clear is used
    if(_xprev !=NULL) delete _xprev; // I don't known which clear is used
    if(_xitels !=NULL) delete _xitels; // I don't known which clear is used
    if(_Fext !=NULL) delete _Fext; // I don't known which clear is used
    if(_Fint !=NULL) delete _Fint; // I don't known which clear is used
    if(_Fextprev!=NULL) delete _Fextprev; // I don't known which clear is used
    _xsol = new std::vector<scalar>(nbRows);
    _xprev = new std::vector<scalar>(nbRows);
    _xitels = new std::vector<scalar>(nbRows);
    _Fext = new std::vector<scalar>(nbRows);
    _Fint = new std::vector<scalar>(nbRows);
    _Fextprev = new std::vector<scalar>(nbRows);
  }
  virtual void clear(){
    linearSystemGmm<scalar>::clear();
    if(_xsol !=NULL) delete _xsol;
    if(_xprev !=NULL) delete _xprev;
    if(_xitels !=NULL) delete _xitels;
    if(_Fext !=NULL) delete _Fext;
    if(_Fint !=NULL) delete _Fint;
    if(_Fextprev!=NULL) delete _Fextprev;
    _xsol=NULL;
    _xprev=NULL;
    _xitels=NULL;
    _Fext=NULL;
    _Fint=NULL;
    _Fextprev=NULL;
    _flagb = false;
  }
  virtual void getFromSolution(int row, scalar &val) const
  {
    val = (*_xsol)[row];
  }
  virtual void getFromPreviousSolution(int row, scalar &val)  const
  {
    val = (*_xprev)[row];
  }
  virtual int systemSolve(){
    // _b = _Fext - _Fint
    if(!_flagb){
      for(unsigned int i = 0; i < this->_b->size(); i++) (*this->_b)[i] = (*_Fext)[i]-(*_Fint)[i];
      _flagb = true;
    }
    linearSystemGmm<scalar>::systemSolve();

    // initialisation of line search
    if(_lineSearch)
    {
      _resNeg = 0.;
      for(unsigned int i = 0; i < _xsol->size(); i++)
      {
        (*_xitels)[i] = (*_xsol)[i]; // copy last solution vector
        _resNeg+= (*this->_b)[i] * (*this->_x)[i];
      }
      _resNeg =-_resNeg; // because b == - residu;
      // other initial values
      _alphaRneg = 0.;
      _alphaRpos = 1.;
      _alphaLsPrev = 1.;
      _alphaLsIte = 1.;
      _resPos = 0.;
      _res0 = _resNeg;
      _flagb = false;
      _lsite = 0;
    }

    // add to _xsol
    for(int i=0;i<_xsol->size(); i++)
    {
      (*_xsol)[i]+=(*this->_x)[i];
    }

    return 1;
  }
  virtual void setInitialValues(const int i, scalar val){
    if(this->isAllocated())
      (*_xsol)[i] = val;
    else{
      Msg::Error("Can't set an initial value in a non allocated system!");
    }
  }
  virtual void getFromRightHandSide(int row, scalar &val) const{
    val = (*_Fext)[row]-(*_Fint)[row];
  }
  virtual void getFromRightHandSidePlus(int row, scalar &val) const{
    val = (*_Fext)[row];
  }
  virtual void getFromRightHandSideMinus(int row, scalar &val) const{
    val = (*_Fint)[row];
  }
  virtual void addToRightHandSide(int row, const scalar &val) // kept for linear constraint
  {
    if(val != 0.0) (*_Fext)[row] += val;
//    #ifdef _DEBUG
//      Msg::Warning("addToRightHandSide is deprecated for non linear system please use addToRightHandSidePlus or addToRightHandSideMinus");
//    #endif
  }
  virtual void addToRightHandSidePlus(int row, const scalar &val)
  {
    if(val != 0.0) (*_Fext)[row] += val;
  }
    virtual void addToRightHandSideMinus(int row, const scalar &val)
  {
    if(val != 0.0) (*_Fint)[row] += val;
  }

  virtual void zeroRightHandSide(){
    linearSystemGmm<scalar>::zeroRightHandSide();
    for(unsigned int i = 0; i < _Fext->size(); i++) (*_Fext)[i] = 0.;
    for(unsigned int i = 0; i < _Fint->size(); i++) (*_Fint)[i] = 0.;
    _flagb = false;
  }
  virtual double normInfRightHandSide() const {
    // compute _b
    if(!_flagb){
      for(unsigned int i = 0; i < this->_b->size(); i++) (*this->_b)[i] = (*_Fext)[i]-(*_Fint)[i];
      _flagb = true;
    }
    return linearSystemGmm<scalar>::normInfRightHandSide();
  }
  virtual double norm0Inf() const{
    double nor=0;
    double temp;
    for(unsigned int i=0;i<_Fext->size();i++){
      temp = (*_Fext)[i];
      if(temp<0) temp = -temp;
      if(nor<temp) nor=temp;
    }
    double tnor = nor;
    nor = 0;
    for(unsigned int i=0;i<_Fint->size();i++){
      temp = (*_Fint)[i];
      if(temp<0) temp = -temp;
      if(nor<temp) nor=temp;
    }
    return tnor+nor;
  }

    // function for linear search iteration return 1 when line search is converged
  virtual int lineSearch()
  {
    if(!_lineSearch) return 1;
    // compute (Fext-Fint)
    if(!_flagb){
      for(unsigned int i=0; i< this->_b->size(); i++)
      {
        (*this->_b)[i] = (*_Fext)[i] - (*_Fint)[i];
      }
    }
    _flagb = false;
    _resLsIte = 0.;
    for(unsigned int i=0; i < this->_b->size(); i++)
    {
      _resLsIte+=(*this->_b)[i]*(*this->_x)[i];
    }
    _resLsIte = - _resLsIte; // due to definition of _b that is -residu
    // new alpha value
    if(_lsite == 0)
    {
      if(_resLsIte < 0)
      {
        _alphaRpos+=1.; // for first itration we want _resPos > 0 increase alpha from 1 if it is not the case
        _alphaLsIte = _alphaRpos;
        if(_alphaRpos > 3)
        {
          for(unsigned int i=0; i<_xsol->size(); i++)
          {
            (*_xsol)[i] = (*this->_x)[i] + (*_xitels)[i]; //alpha = 1
          }
          return 1;
        }
      }
      else
      {
        _lsite++; // increase iteration counter;
        _resPos = _resLsIte;
        // new alpha
        _alphaLsPrev = _alphaLsIte;
        _alphaRpos = _alphaLsIte;
        _alphaLsIte = _alphaRneg - _resNeg*(_alphaRpos-_alphaRneg)/(_resPos - _resNeg);
      }
    }
    else if(_lsite > _lsitemax) // divergence of process
    {
      for(unsigned int i=0; i<_xsol->size(); i++)
      {
        (*_xsol)[i] = (*this->_x)[i] + (*_xitels)[i]; //alpha = 1
      }
      return 1;
    }
    else
    {
      _lsite++; // increase iteration counter;
      // check convergence on residu
      double relres = _resLsIte / _res0;
      if(relres < 0.) relres = -relres;
      if(relres < _Tolls) return 1; // achieve convergence
      // check convergence on alpha
      relres = (_alphaLsIte - _alphaLsPrev) / _alphaLsPrev;
      if(relres < 0.) relres = -relres;
      if(relres < _Tolls) return 1; // achieve convergence

      // update value of residu
      _alphaLsPrev = _alphaLsIte;
      if(_resLsIte <= 0.)
      {
        _resNeg = _resLsIte;
        _alphaRneg = _alphaLsIte;
      }
      else
      {
        _resPos = _resLsIte;
        _alphaRpos = _alphaLsIte;
      }
      // new alpha
      _alphaLsIte = _alphaRneg - _resNeg*(_alphaRpos-_alphaRneg)/(_resPos - _resNeg);
    }
    // new positions
    for(unsigned int i=0; i<_xsol->size(); i++)
    {
      (*_xsol)[i] = _alphaLsIte*(*this->_x)[i] + (*_xitels)[i];
    }
    return 0;
  }
  virtual void resetUnknownsToPreviousTimeStep()
  {
    for(unsigned int i=0; i<_xsol->size(); i++)
    {
      (*_xsol)[i] = (*_xprev)[i];
    }
  }
  virtual void nextStep()
  {
    for(unsigned int i=0; i<_xsol->size(); i++)
    {
      (*_xprev)[i] = (*_xsol)[i];
    }
  }
  virtual double getExternalWork(const int syssize) const
  {
    double wext=0;
    for(int i=0;i<syssize;i++)
    {
      wext += ((*_Fext)[i] + (*_Fextprev)[i])*((*_xsol)[i] - (*_xprev)[i]);
      (*_Fextprev)[i] = (*_Fext)[i];
    }
    for(int i=syssize;i<_xsol->size();i++)
    {
      (*_Fextprev)[i] = (*_Fext)[i];
    }
    return 0.5*wext;
  }
  virtual double getKineticEnergy(const int syssize) const{return 0.;} // no kinetic energy in a QS scheme
  virtual void addToMatrix(int row, int col, const scalar &val, const typename nonLinearSystem<scalar>::whichMatrix wm)
  {
   #ifdef _DEBUG
    if(wm!=nonLinearSystem<scalar>::stiff)
      Msg::Fatal("Try to assemble a matrix different from stiffness in a QS Gmm system");
    else
   #endif //_DEBUG
     this->linearSystemGmm<scalar>::addToMatrix(row,col,val);
  }
  virtual void setInitialCondition(int row, scalar val, const typename nonLinearBoundaryCondition::whichCondition wc=nonLinearBoundaryCondition::position)
  {
    if(wc!=nonLinearBoundaryCondition::position)
    {
      Msg::Error("Can set a velocity or a aceleration in a QS scheme");
      return;
    }
    this->setInitialValues(row,val);
  }
  virtual void setDynamicRelaxation(const bool dynrel)
  {
    Msg::Error("Cannot use dynamic relaxation in an QS system");
  }
  virtual void getFromSolution(int row, scalar &val, const nonLinearBoundaryCondition::whichCondition wv) const
  {
    if(wv!=nonLinearBoundaryCondition::position)
    {
      Msg::Error("Cannot take velocity or acceleration in a QS scheme");
      val=0.;
      return;
    }
    this->getFromSolution(row,val);
  }
  virtual void getFromMatrix(int row, int col, scalar &val,const typename nonLinearSystem<scalar>::whichMatrix wm) const
  {
    if(wm!=nonLinearSystem<scalar>::stiff)
    {
      Msg::Error("A QS system has only a stiffness matrix. So cannot get a value in another matrix");
      val=0;
      return;
    }
    this->linearSystemGmm<scalar>::getFromMatrix(row,col,val);
  }
  #if defined(HAVE_MPI)
  virtual double* getArrayIndexPreviousRightHandSidePlusMPI(const int index)
  {
    Msg::Fatal("This function cannot be used in a QS Gmm scheme"); // has to return a reference on an array Impossible with a std::vector
  }
  #endif // HAVE_MPI
  virtual void setTimeStep(const double dt){_timeStep = dt;}
  virtual double getTimeStep() const{return _timeStep;}
#endif // HAVE_GMM
};

#include "linearSystemCSR.h"
// Taucs
template<class scalar>
class nonLinearSystemCSRTaucs : public linearSystemCSRTaucs<scalar>, public nonLinearSystem<scalar>{
 protected:
  mutable bool _flagb;
  std::vector<scalar> *_xsol;
  std::vector<scalar> *_xprev;
  std::vector<scalar> *_xitels;
  std::vector<scalar> *_Fext;
  std::vector<scalar> *_Fint;
  double _timeStep;
  mutable std::vector<scalar> *_Fextprev;
  const double _Tolls;
  const bool _lineSearch;
  double _alphaRneg, _alphaRpos, _alphaLsIte, _alphaLsPrev, _resNeg, _resPos, _resLsIte, _res0;
  int _lsite,_lsitemax;
 public:
  nonLinearSystemCSRTaucs(const bool ls=false, const double tolls = 1.e-2,
                          const int lsitemax=10) : linearSystemCSRTaucs<scalar>(), nonLinearSystem<scalar>(),
                                                                            _xsol(NULL), _xprev(NULL), _xitels(NULL),_Fext(NULL),
                                                                            _Fextprev(NULL),_Fint(NULL),
                                                                            _flagb(false), _Tolls(tolls), _lineSearch(ls),
                                                                            _alphaRneg(0.), _alphaRpos(1.), _resNeg(0.), _resPos(0.),
                                                                            _alphaLsIte(0.), _resLsIte(0.), _res0(1.), _timeStep(0.),
                                                                            _alphaLsPrev(1.), _lsitemax(lsitemax){}
  ~nonLinearSystemCSRTaucs(){} // _xsol is destroied by clear() called by ~linearSystemCSR()
#if defined (HAVE_TAUCS)
  virtual void allocate(int nbRows){
    linearSystemCSR<scalar>::allocate(nbRows);
    if(_xsol !=NULL) delete _xsol;
    if(_xprev !=NULL) delete _xprev;
    if(_xitels !=NULL) delete _xitels;
    if(_Fext !=NULL) delete _Fext;
    if(_Fint !=NULL) delete _Fint;
    if(_Fextprev!=NULL) delete _Fextprev;
    if(nbRows == 0){
      _xsol = NULL;
      _xprev = NULL;
      _xitels = NULL;
      _Fext = NULL;
      _Fextprev=NULL;
      _Fint = NULL;
      return;
    }
    _xsol = new std::vector<scalar>(nbRows);
    _xprev = new std::vector<scalar>(nbRows);
    _xitels = new std::vector<scalar>(nbRows);
    _Fext = new std::vector<scalar>(nbRows);
    _Fint = new std::vector<scalar>(nbRows);
    _Fextprev = new std::vector<scalar>(nbRows);
    for(int i=0;i<nbRows; i++)
      (*_Fextprev)[i]=0.;
  }
  virtual void clear(){
    linearSystemCSRTaucs<scalar>::clear();
    if(_xsol !=NULL) delete _xsol;
    if(_xprev !=NULL) delete _xprev;
    if(_xitels !=NULL) delete _xitels;
    if(_Fext !=NULL) delete _Fext;
    if(_Fint !=NULL) delete _Fint;
    if(_Fextprev!=NULL) delete _Fextprev;
    _xsol = NULL;
    _xprev = NULL;
    _xitels = NULL;
    _Fext = NULL;
    _Fint = NULL;
    _Fextprev = NULL;
    _flagb = false;
  }
  virtual void getFromSolution(int row, scalar &val) const
  {
    val = (*_xsol)[row];
  }
  virtual void getFromPreviousSolution(int row, scalar &val)  const
  {
    val = (*_xprev)[row];
  }
  virtual int systemSolve(){
    // _b = _Fext - _Fint
    if(!_flagb){
      for(unsigned int i = 0; i < this->_b->size(); i++) (*(this->_b))[i] = (*_Fext)[i]-(*_Fint)[i];
      _flagb = true;
    }

    linearSystemCSRTaucs<scalar>::systemSolve();

     // initialisation of line search
    if(_lineSearch)
    {
      _resNeg = 0.;
      for(unsigned int i = 0; i < _xsol->size(); i++)
      {
        (*_xitels)[i] = (*_xsol)[i]; // copy last solution vector
        _resNeg+= (*this->_b)[i] * (*this->_x)[i];
      }
      _resNeg =-_resNeg; // because b == - residu;
      // other initial values
      _alphaRneg = 0.;
      _alphaRpos = 1.;
      _alphaLsPrev = 1.;
      _alphaLsIte = 1.;
      _resPos = 0.;
      _res0 = _resNeg;
      _lsite = 0;
    }

    for(int i=0;i<_xsol->size(); i++){
      (*_xsol)[i]+=(*this->_x)[i];
    }
    return 1;
  }
  virtual void setInitialValues(const int i, scalar val){
    if(this->isAllocated())
     (*_xsol)[i] = val;
    else{
      Msg::Error("Can't give an initial value in a non allocated system!");
    }
  }
  virtual void getFromRightHandSide(int row, scalar &val) const{
    val = (*_Fext)[row]-(*_Fint)[row];
  }
  virtual void getFromRightHandSidePlus(int row, scalar &val) const{
    val = (*_Fext)[row];
  }
  virtual void getFromRightHandSideMinus(int row, scalar &val) const{
    val = (*_Fint)[row];
  }
  virtual void addToRightHandSide(int row, const scalar &val) // kept for linear constraint
  {
    if(val != 0.0) (*_Fext)[row] += val;
//    #ifdef _DEBUG
//      Msg::Warning("addToRightHandSide is deprecated for non linear system. Please use addToRightHandSidePlus or addToRightHandSideMinus");
//    #endif
  }
  virtual void addToRightHandSidePlus(int row, const scalar &val)
  {
    if(val != 0.0) (*_Fext)[row] += val;
  }
  virtual void addToRightHandSideMinus(int row, const scalar &val)
  {
    if(val != 0.0) (*_Fint)[row] += val;
  }

  virtual void zeroRightHandSide(){
    linearSystemCSRTaucs<scalar>::zeroRightHandSide();
    if (!_Fext) return;
    for(unsigned int i = 0; i < _Fext->size(); i++) (*_Fext)[i] = 0.;
    if (!_Fint) return;
    for(unsigned int i = 0; i < _Fint->size(); i++) (*_Fint)[i] = 0.;
    _flagb = false;
  }
  virtual double normInfRightHandSide() const{
    if (!_Fext) return 0.;
    // compute _b
    if(!_flagb){
      for(unsigned int i = 0; i < this->_b->size(); i++) (*(this->_b))[i] = (*_Fext)[i]-(*_Fint)[i];
      _flagb = true;
    }
    return linearSystemCSRTaucs<scalar>::normInfRightHandSide();
  }
  virtual double norm0Inf() const{
    double tnor;
    double nor = 0.;
    double temp;
    for(unsigned int i = 0; i < _Fext->size(); i++){
      temp = (*_Fext)[i];
      if(temp < 0) temp = -temp;
      if(nor < temp) nor = temp;
    }
    tnor = nor;
    nor = 0.;
    for(unsigned int i = 0; i < _Fint->size(); i++){
      temp = (*_Fint)[i];
      if(temp < 0) temp = -temp;
      if(nor < temp) nor = temp;
    }
    return tnor + nor;
  }

      // function for linear search iteration return 1 when line search is converged
  virtual int lineSearch()
  {
    if(!_lineSearch) return 1;
    // compute (Fext-Fint)
    Msg::Info("Line search iteration %d",_lsite);
    if(!_flagb){
      for(unsigned int i=0; i< this->_b->size(); i++)
      {
        (*this->_b)[i] = (*_Fext)[i] - (*_Fint)[i];
      }
    }
    _flagb = false;
    _resLsIte = 0.;
    for(unsigned int i=0; i < this->_b->size(); i++)
    {
      _resLsIte+=(*this->_b)[i]*(*this->_x)[i];
    }
    _resLsIte = - _resLsIte; // due to definition of _b that is -residu
    // new alpha value
    if(_lsite == 0)
    {
      if(_resLsIte < 0)
      {
        _alphaRpos+=1.; // for first itration we want _resPos > 0 increase alpha from 1 if it is not the case
        _alphaLsIte = _alphaRpos;
        if(_alphaRpos > 3)
        {
          for(unsigned int i=0; i<_xsol->size(); i++)
          {
            (*_xsol)[i] = (*this->_x)[i] + (*_xitels)[i]; //alpha = 1
          }
          return 1;
        }
      }
      else
      {
        _lsite++; // increase iteration counter;
        _resPos = _resLsIte;
        // new alpha
        _alphaLsPrev = _alphaLsIte;
        _alphaRpos = _alphaLsIte;
        _alphaLsIte = _alphaRneg - _resNeg*(_alphaRpos-_alphaRneg)/(_resPos - _resNeg);
      }
    }
    else if(_lsite > _lsitemax) // divergence of process
    {
      for(unsigned int i=0; i<_xsol->size(); i++)
      {
        (*_xsol)[i] = (*this->_x)[i] + (*_xitels)[i]; //alpha = 1
      }
      return 1;
    }
    else
    {
      _lsite++; // increase iteration counter;
      // check convergence on residu
      double relres = _resLsIte / _res0;
      if(relres < 0.) relres = -relres;
      if(relres < _Tolls) return 1; // achieve convergence
      // check convergence on alpha
      relres = (_alphaLsIte - _alphaLsPrev) / _alphaLsPrev;
      if(relres < 0.) relres = -relres;
      if(relres < _Tolls) return 1; // achieve convergence

      // update value of residu
      _alphaLsPrev = _alphaLsIte;
      if(_resLsIte <= 0.)
      {
        _resNeg = _resLsIte;
        _alphaRneg = _alphaLsIte;
      }
      else
      {
        _resPos = _resLsIte;
        _alphaRpos = _alphaLsIte;
      }
      // new alpha
      _alphaLsIte = _alphaRneg - _resNeg*(_alphaRpos-_alphaRneg)/(_resPos - _resNeg);
    }
    // new positions
    for(unsigned int i=0; i<_xsol->size(); i++)
    {
      (*_xsol)[i] = _alphaLsIte*(*this->_x)[i] + (*_xitels)[i];
    }
    return 0;
  }
  virtual void resetUnknownsToPreviousTimeStep()
  {
    for(unsigned int i=0; i<_xsol->size(); i++)
    {
      (*_xsol)[i] = (*_xprev)[i];
    }
  }
  virtual void nextStep()
  {
    for(unsigned int i=0; i<_xsol->size(); i++)
    {
      (*_xprev)[i] = (*_xsol)[i];
    }
  }
  virtual double getExternalWork(const int syssize) const
  {
    double wext=0;
    for(int i=0;i<syssize;i++)
    {
      wext += ((*_Fext)[i] + (*_Fextprev)[i])*((*_xsol)[i] - (*_xprev)[i]);
      (*_Fextprev)[i] = (*_Fext)[i];
    }
    for(int i=syssize;i<_xsol->size();i++)
    {
      (*_Fextprev)[i] = (*_Fext)[i];
    }
    return 0.5*wext;
  }
  virtual double getKineticEnergy(const int syssize) const{return 0.;} // no kinetic energy in a QS scheme
  virtual void addToMatrix(int row, int col, const scalar &val, const typename nonLinearSystem<scalar>::whichMatrix wm)
  {
   #ifdef _DEBUG
    if(wm!=nonLinearSystem<scalar>::stiff)
      Msg:Fatal("Try to assemble a matrix different from stiffness for a QS taucs system");
    else
   #endif // _DEBUG
      this->addToMatrix(row,col,val);
  }

  virtual void setInitialCondition(int row, scalar val, const typename nonLinearBoundaryCondition::whichCondition wc=nonLinearBoundaryCondition::position)
  {
    if(wc!=nonLinearBoundaryCondition::position)
    {
      Msg::Error("Can set a velocity or a aceleration in a QS scheme");
      return;
    }
    this->setInitialValues(row,val);
  }
  virtual void setDynamicRelaxation(const bool dynrel)
  {
    Msg::Error("Cannot use dynamic relaxation in an QS system");
  }
  virtual void getFromSolution(int row, scalar &val, const nonLinearBoundaryCondition::whichCondition wv) const
  {
    if(wv!=nonLinearBoundaryCondition::position)
    {
      Msg::Error("Cannot take velocity or acceleration in a QS scheme");
      scalar=0.;
      return;
    }
    this->getFromSolution(row,val);
  }
  virtual void getFromMatrix(int row, int col, scalar &val,const typename nonLinearSystem<scalar>::whichMatrix wm) const
  {
    if(wm!=nonLinearSystem<scalar>::stiff)
    {
      Msg::Error("A QS system has only a stiffness matrix. So cannot get a value in another matrix");
      val=0;
      return;
    }
    this->linearSystemCSRTaucs<scalar>::getFromMatrix(row,col,val);
  }
  #if defined(HAVE_MPI)
  virtual double* getArrayIndexPreviousRightHandSidePlusMPI(const int index)
  {
    Msg::Fatal("This function cannot be used in a QS Taucs scheme"); // has to return a reference on an array Impossible with a std::vector
  }
  #endif // HAVE_MPI
  virtual void setTimeStep(const double dt){_timeStep = dt;}
  virtual double getTimeStep() const=0{return _timeStep;}
#endif // HAVE_TAUCS
};

// GMREs
#include "linearSystemGMRESk.h"
#if defined(HAVE_BLAS)

// extern blas function declaration
#if !defined(F77NAME)
#define F77NAME(x) (x##_)
#endif // F77NAME
extern "C" {
  void F77NAME(daxpy)(int *n, double *alpha, double *x, int *incx, double *y, int *incy);
  void F77NAME(dscal)(int *n, double *alpha,double *x,  int *incx);
  void F77NAME(dcopy)(int *n, double *x,int *incx, double *y, int *incy);
  double F77NAME(ddot)(int *n,double *x,int *incx, double *y, int *incy);
  int F77NAME(idamax)(int *n,double *x,int *incx);
}
#endif // HAVE_BLAS
template<class scalar>
class nonLinearSystemGMRESk : public linearSystemGMRESk<scalar>, public nonLinearSystem<scalar>{
 private:
  bool _allocatednldata;
  mutable bool _flagb;
 protected:
  double* _xsol;  // _xsol = _xitels + alpha * _x; (alpha == line search parameter)
  double* _xitels; // for line search the value of solution as to be kept
  double* _xprev; // solution of previous time step
  double* _Fext;
  double* _Fint;
  double _timeStep;
  mutable double* _Fextprev;
  mutable double* _FextpFextprev;
  mutable double* _Deltax;
  // data for line search
  const double _Tolls; // tolerance for line search
  bool _lineSearch;
  double _alphaRneg, _alphaRpos, _alphaLsIte, _alphaLsPrev;
  double _resNeg, _resPos, _resLsIte, _res0;
  int _lsite,_lsitemax;
 public:
  nonLinearSystemGMRESk(const bool ls=false, const double tolls=1e-6,
                       const int lsitemax=10) : linearSystemGMRESk<scalar>(), nonLinearSystem<scalar>(),
                                                                 _allocatednldata(false), _flagb(false),
                                                                 _lineSearch(ls), _Tolls(tolls),
                                                                 _alphaRneg(0.), _alphaRpos(1.), _resNeg(0.), _resPos(0), _lsite(0),
                                                                 _alphaLsIte(0.), _resLsIte(0.), _res0(1.), _timeStep(0.),
                                                                 _alphaLsPrev(1.), _lsitemax(lsitemax){}
  ~nonLinearSystemGMRESk(){} // _xsol is destroied by clear() called by ~linearSystemPETSc()

  virtual void allocate(int nbRows){
    linearSystemGMRESk<scalar>::allocate(nbRows);
    _xsol = new double[this->_nbColumns];
    _xprev = new double[this->_nbColumns];
    _xitels = new double[this->_nbColumns];
    _Fext = new double[this->_nbColumns];
    _Fint = new double[this->_nbColumns];
    _FextpFextprev = new double[this->_nbColumns];
    _Fextprev = new double[this->_nbColumns];
    _Deltax = new double[this->_nbColumns];
    _allocatednldata=true;
  }
  virtual void clear(){
    linearSystemGMRESk<scalar>::clear();
    if(_allocatednldata){
      delete[] _xsol;
      delete[] _xprev;
      delete[] _xitels;
      delete[] _Fext;
      delete[] _Fint;
      delete[] _Fextprev;
      delete[] _FextpFextprev;
      delete[] _Deltax;
    }
    _allocatednldata = false;
    _flagb = false;
  }
  virtual void getFromSolution(int row, scalar &val) const
  {
    val = _xsol[row];
  }
  virtual void getFromPreviousSolution(int row, scalar &val)  const
  {
    val = _xprev[row];
  }
  virtual int systemSolve(){
    // _b = _Fext - _Fint Normally done via the called of normInfRightHandSide
    if(!_flagb){
     #if defined(HAVE_BLAS)
      int INCX = 1;
      double alpha = -1.;
      F77NAME(dcopy)(&(this->_nbColumns),_Fext,&INCX,this->_b,&INCX);
      F77NAME(daxpy)(&(this->_nbColumns),&alpha,_Fint,&INCX,_Fext,&INCX);
     #else
      for(int i=0;i<this->_nbColumns;i++) this->_b[i] = _Fext[i] - _Fint[i];
     #endif // HAVE_BLAS
      _flagb = true;
    }
    // solve system
    linearSystemGMRESk<scalar>::systemSolve();

    // initialisation of line search
    if(_lineSearch)
    {
     #if defined(HAVE_BLAS)
      int INCX = 1;
      F77NAME(dcopy)(&(this->_nbColumns),_xsol,&INCX,_xitels,&INCX);
      _resNeg = F77NAME(ddot)(&(this->_nbColumns),this->_b,&INCX,this->_x,&INCX);
     #else
      _resNeg = 0.;
      for(int i=0;i<this->_nbColumns;i++)
      {
        _xitels[i] = _xsol[i];
        _resNeg+= this->_b[i]*this->_x[i];
      }
     #endif
      _resNeg =-_resNeg; // because b == - residu;
      // other initial values
      _alphaRneg = 0.;
      _alphaRpos = 1.;
      _alphaLsPrev = 1.;
      _alphaLsIte = 1.;
      _resPos = 0.;
      _res0 = _resNeg;
      _lsite = 0;
    }

    // update xsol (for line search alpha0 == 1)
   #if defined(HAVE_BLAS)
    int INCX = 1;
    double alpha = 1.;
    F77NAME(daxpy)(&(this->_nbColumns),&alpha,this->_x,&INCX,_xsol,&INCX);
   #else
    for(int i=0;i<this->_nbColumns;i++) _xsol[i] += this->_x[i];
   #endif // HAVE_BLAS
    return 1;
  }
  virtual void setInitialValues(const int i, scalar val){
    if(this->isAllocated())
      _xsol[i] += val;
    else{
      Msg::Error("Can't give an initial value in a non allocated system!");
    }
  }
  virtual void getFromRightHandSide(int row, scalar &val) const{
    val = _Fext[row] - _Fint[row];
  }
  virtual void getFromRightHandSidePlus(int row, scalar &val) const{
    val = _Fext[row];
  }
  // retrieve the value _Fint and not -_Fint
  virtual void getFromRightHandSideMinus(int row, scalar &val) const{
    val = _Fint[row];
  }

  virtual void addToRightHandSide(int row, const scalar &val){ // kept for linear constraint
//    #ifdef _DEBUG
//      Msg::Warning("addToRightHandSide is deprecated for non linear system please use addToRightHandSidePlus or addToRightHandSideMinus");
//    #endif
    _Fext[row] += val;
  }
  virtual void addToRightHandSidePlus(int row, const scalar &val){
    _Fext[row] += val;
  }
  virtual void addToRightHandSideMinus(int row, const scalar &val){
    _Fint[row] += val;
  }


  virtual void zeroRightHandSide(){
    linearSystemGMRESk<scalar>::zeroRightHandSide();
    _flagb = false;
    if (_allocatednldata) {
       // dscal multiply by 0. and not set
//     #if defined(HAVE_BLAS)
//      int INCX = 1;
//      double alpha = 0.;
//      F77NAME(dscal)(&(this->_nbColumns),&alpha,_Fext,&INCX);
//      F77NAME(dscal)(&(this->_nbColumns),&alpha,_Fint,&INCX);
//     #else
      for(int i=0; i<this->_nbColumns;i++)
      {
        _Fext[i] = 0.;
        _Fint[i] = 0.;
      }
//     #endif // HAVE_BLAS
    }
  }
  virtual double normInfRightHandSide() const{
    // compute b
    if(!_flagb){
     #if defined(HAVE_BLAS)
      int INCX = 1;
      double alpha = -1.;
      int nc = this->_nbColumns;
      F77NAME(dcopy)(&nc,_Fext,&INCX,this->_b,&INCX);
      F77NAME(daxpy)(&nc,&alpha,_Fint,&INCX,_Fext,&INCX);
     #else
      for(int i=0;i<this->_nbColumns;i++) this->_b[i] = _Fext[i] - _Fint[i];
     #endif // HAVE_BLAS
      _flagb = true;
    }
    return linearSystemGMRESk<scalar>::normInfRightHandSide();
  }
  virtual double norm0Inf() const{
    double norFext;
    double norFint;
   #if defined(HAVE_BLAS)
    int INCX = 1;
    int nc = this->_nbColumns;
    int indFext = F77NAME(idamax)(&nc,_Fext,&INCX)-1; // idamax first index = 1 !!
    int indFint = F77NAME(idamax)(&nc,_Fint,&INCX)-1; // idamax first index = 1 !!
    norFext = fabs(_Fext[indFext]);
    norFint = fabs(_Fint[indFint]);
   #else
    norFext = _Fext[0];
    norFint = _Fint[0];
    if(norFext < 0) norFext = - norFext;
    if(norFint < 0) norFint = - norFint;
    for(int i=1;i<this->_nbColumns;i++)
    {
      if(_Fext[i] > norFext) norFext = _Fext[i];
      else if (-_Fext[i] > norFext) norFext = - _Fext[i];
      if(_Fint[i] > norFint) norFint = _Fint[i];
      else if(-_Fint[i] > norFint) norFint = -_Fint[i];
    }
   #endif // HAVE_BLAS
   #if defined(HAVE_MPI)
    if(Msg::GetCommSize() > 1)
    {
      double nn[2] = {norFext,norFint};
      MPI_Allreduce(nn,MPI_IN_PLACE,2,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
      norFext = nn[0];
      norFint = nn[1];
    }
   #endif // HAVE_MPI
    return norFext + norFint;
  }
  // function for linear search iteration return 1 when line search is converged
  virtual int lineSearch()
  {
    if(!_lineSearch) return 1;
    // compute (Fext-Fint)
    Msg::Info("Line search iteration %d",_lsite);
    if(!_flagb){
     #if defined(HAVE_BLAS)
       int INCX = 1;
       double alpha = -1;
       int nc = this->_nbColumns;
       F77NAME(dcopy)(&nc,_Fext,&INCX,this->_b,&INCX);
       F77NAME(daxpy)(&nc,&alpha,_Fint,&INCX,this->_b,&INCX);
     #else
       for(int i=0;i<this->_nbColumns;i++) this->_b[i] = _Fext[i] - _Fint[i];
     #endif // HAVE_BLAS
     _flagb = true;
    }
   #if defined(HAVE_BLAS)
    int INCX = 1;
    _resLsIte = F77NAME(ddot)(&(this->_nbColumns),this->_b,&INCX,this->_x,&INCX);
   #else
     _resLsIte = 0.;
     for(int i=0;i<this->_nbColumns;i++) _resLsIte += this->_b[i]*this->_x[i];
   #endif // HAVE_BLAS

    Msg::Info("Residu of line search %e relatif res %e alpha %e",_resLsIte,_resLsIte/_res0,_alphaLsIte);
    _resLsIte = - _resLsIte; // due to definition of _b that is -residu
    // new alpha value
    if(_lsite == 0)
    {
      if(_resLsIte < 0)
      {
        _alphaRpos+=1.; // for first itration we want _resPos > 0 increase alpha from 1 if it is not the case
        _alphaLsIte = _alphaRpos;
        if(_alphaRpos > 3)
        {
          _alphaLsIte = 1.;
         #if defined(HAVE_BLAS)
          int INCX = 1;
          F77NAME(dcopy)(&(this->_nbColumns),_xitels,&INCX,_xsol,&INCX);
          F77NAME(daxpy)(&(this->_nbColumns),&_alphaLsIte,this->_x,&INCX,_xsol,&INCX);
         #else
          for(int i=0;i<this->_nbColumns;i++) _xsol[i] = _xitels[i] + _alphaLsIte*this->_x[i];
         #endif // HAVE_BLAS
          return 1;
        }
      }
      else
      {
        _lsite++; // increase iteration counter;
        _resPos = _resLsIte;
        // new alpha
        _alphaLsPrev = _alphaLsIte;
        _alphaRpos = _alphaLsIte;
        _alphaLsIte = _alphaRneg - _resNeg*(_alphaRpos-_alphaRneg)/(_resPos - _resNeg);
      }
    }
    else if(_lsite > _lsitemax) // divergence of process take 1
    {
      // new positions
      _alphaLsIte = 1.;
     #if defined(HAVE_BLAS)
      int INCX = 1;
      F77NAME(dcopy)(&(this->_nbColumns),_xitels,&INCX,_xsol,&INCX);
      F77NAME(daxpy)(&(this->_nbColumns),&_alphaLsIte,this->_x,&INCX,_xsol,&INCX);
     #else
      for(int i=0;i<this->_nbColumns;i++) _xsol[i] = _xitels[i] + _alphaLsIte*this->_x[i];
     #endif // HAVE_BLAS
      return 1;
    }
    else
    {
      _lsite++; // increase iteration counter;
      // check convergence on residu
      double relres = _resLsIte / _res0;
      if(relres < 0.) relres = -relres;
      if(relres < _Tolls) return 1; // achieve convergence
      // check convergence on alpha
      relres = (_alphaLsIte - _alphaLsPrev) / _alphaLsPrev;
      if(relres < 0.) relres = -relres;
      if(relres < _Tolls) return 1; // achieve convergence

      // update value of residu
      _alphaLsPrev = _alphaLsIte;
      if(_resLsIte <= 0.)
      {
        _resNeg = _resLsIte;
        _alphaRneg = _alphaLsIte;
      }
      else
      {
        _resPos = _resLsIte;
        _alphaRpos = _alphaLsIte;
      }
      // new alpha
      _alphaLsIte = _alphaRneg - _resNeg*(_alphaRpos-_alphaRneg)/(_resPos - _resNeg);
    }

    // new positions
    #if defined(HAVE_BLAS)
     F77NAME(dcopy)(&(this->_nbColumns),_xitels,&INCX,_xsol,&INCX);
     F77NAME(daxpy)(&(this->_nbColumns),&_alphaLsIte,this->_x,&INCX,_xsol,&INCX);
    #else
     for(int i=0;i<this->_nbColumns;i++) _xsol[i] = _xitels[i] + _alphaLsIte*this->_x[i];
    #endif // HAVE_BLAS
    return 0;
  }
  virtual void resetUnknownsToPreviousTimeStep()
  {
   #if defined(HAVE_BLAS)
    int INCX = 1;
    F77NAME(dcopy)(&(this->_nbColumns),_xprev,&INCX,_xsol,&INCX);
   #else
    for(int i=0;i<this->_nbColumns;i++) _xsol[i] = _xprev[i];
   #endif // HAVE_BLAS
  }
  virtual void nextStep()
  {
   #if defined(HAVE_BLAS)
    int INCX = 1;
    F77NAME(dcopy)(&(this->_nbColumns),_xsol,&INCX,_xprev,&INCX);
   #else
    for(int i=0;i<this->_nbColumns; i++) _xprev[i] = _xsol[i];
   #endif // HAVE_BLAS
  }
  virtual double getExternalWork(const int syssize) const
  {
   #if defined(HAVE_BLAS)
    int INCX = 1;
    double one = 1;
    double mone = -1;
    int nbc = (this->_nbColumns);
    int ssize = syssize;
    F77NAME(dcopy)(&nbc,_Fextprev,&INCX,_FextpFextprev,&INCX);
    F77NAME(daxpy)(&nbc,&one,_Fext,&INCX,_FextpFextprev,&INCX);
    F77NAME(dcopy)(&nbc,_xsol,&INCX,_Deltax,&INCX);
    F77NAME(daxpy)(&nbc,&mone,_xprev,&INCX,_Deltax,&INCX);
    double wext = F77NAME(ddot)(&ssize,_FextpFextprev,&INCX,_Deltax,&INCX);
    F77NAME(dcopy)(&nbc,_Fext,&INCX,_Fextprev,&INCX);
    return 0.5*wext;
   #else
    double wext=0.;
    for(int i=0;i<syssize;i++)
    {
      wext += (_Fext[i]+_Fextprev[i])*(_xsol[i]-_xprev[i]);
      _Fextprev[i] = _Fext[i];
    }
    for(int i=syssize;i<this->_nbColumns;i++)
    {
      _Fextprev[i] = _Fext[i];
    }
   #endif // HAVE_BLAS
  }
  virtual double getKineticEnergy(const int syssize) const{return 0.;} // no kinetic energy in a QS scheme
  virtual void addToMatrix(int row, int col, const scalar &val, const typename nonLinearSystem<scalar>::whichMatrix wm)
  {
   #ifdef _DEBUG
    if(wm!=nonLinearSystem<scalar>::stiff)
      Msg::Fatal("Try to assemble a matrix different from stiffness in a QS Gmresk system");
    else
   #endif // _DEBUG
    this->linearSystemGMRESk<scalar>::addToMatrix(row,col,val);
  }


  virtual void setInitialCondition(int row, scalar val, const typename nonLinearBoundaryCondition::whichCondition wc=nonLinearBoundaryCondition::position)
  {
    if(wc!=nonLinearBoundaryCondition::position)
    {
      Msg::Error("Can set a velocity or a aceleration in a QS scheme");
      return;
    }
    this->setInitialValues(row,val);
  }
  virtual void setDynamicRelaxation(const bool dynrel)
  {
    Msg::Error("Cannot use dynamic relaxation in an QS system");
  }
  virtual void getFromSolution(int row, scalar &val, const nonLinearBoundaryCondition::whichCondition wv) const
  {
    if(wv!=nonLinearBoundaryCondition::position)
    {
      Msg::Error("Cannot take velocity or acceleration in a QS scheme");
      val=0.;
      return;
    }
    this->getFromSolution(row,val);
  }
  virtual void getFromMatrix(int row, int col, scalar &val,const typename nonLinearSystem<scalar>::whichMatrix wm) const
  {
    if(wm!=nonLinearSystem<scalar>::stiff)
    {
      Msg::Error("A QS system has only a stiffness matrix. So cannot get a value in another matrix");
      val=0;
      return;
    }
    this->linearSystemGMRESk<scalar>::getFromMatrix(row,col,val);
  }
  #if defined(HAVE_MPI)
  virtual double* getArrayIndexPreviousRightHandSidePlusMPI(const int index)
  {
    return (&(_Fextprev[index]));
  }
  #endif // HAVE_MPI
  virtual void setTimeStep(const double dt){_timeStep = dt;}
  virtual double getTimeStep() const{return _timeStep;}
};

#endif // _NONLINEARSYSTEMS_H_
