//
// C++ Interface: terms
//
// Description: Function to prescribed BC
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef TIMEFUNCTION_H_
#define TIMEFUNCTION_H_
#include "simpleFunctionTime.h"
#include "SVector3.h"
#include <math.h>
template<class scalar>
class simpleFunctionTimeWithConstant : public simpleFunctionTime<scalar>{
 protected:
  scalar _b;
 public:
  simpleFunctionTimeWithConstant(scalar val=0, bool td=true, double t=1.,scalar b=0.) : simpleFunctionTime<scalar>(val,td,t), _b(b){};
 #ifndef SWIG
  virtual ~simpleFunctionTimeWithConstant(){};
  virtual scalar operator () (double x, double y, double z) const { if(this->timeDependency) return this->time*this->_val+_b; else return this->_val;}
 #endif // SWIG
};

class powerDecreasingFunctionTime : public simpleFunctionTime<double>{
 protected:
// _val = p0  const double p0; // minimal pressure
  const double _p1; // maximal pressure
  const double _a;  // power exponent
  const double _t0; // if t < t0 p = p0
  const double _t1; // if t0 < t < t1 p = p0 + (p1-p0)*(t-t0)/(t1-t0)
  const double _tf; // if t1 < t < tf p = (p1-p0)*(tf-t1)^-a*(tf-t)^a + p0
 public:
  powerDecreasingFunctionTime(double p0, const double p1, const double a, const double t0,
                              const double t1, const double tf) : simpleFunctionTime<double>(p0,true,1.),
                                                                     _p1(p1), _a(a), _t0(t0), _t1(t1), _tf(tf){}
#ifndef SWIG
  virtual ~powerDecreasingFunctionTime(){}
  virtual double operator () (double x, double y, double z) const{
    double p0 = _val;
    if((_t0<time) and(time<_t1))
      return p0 + (_p1-p0)*(time-_t0)/(_t1-_t0);
    else if ((_t1<time) and (time<_tf))
      return (_p1-p0)*pow(_tf-_t1,-_a)*pow(_tf-time,_a)+p0;
    else
      return p0;
  }
#endif //SWIG
};
class cycleFunctionTime : public simpleFunctionTime<double>{
 protected:
// _val = p0  const double p0; // minimal pressure
  const double _d1; // maximal displacement
  const double _d2; // minimal displacement
  const double _d3; // second maximal displacement
  const double _d4;
  const double _d5;
  const double _d6;
  const double _t1; // time for maximal displacement
  const double _t2; // time for minimal displacement
  const double _t3; // time for second maximal displacement
  const double _t4;
  const double _t5;
  const double _t6;
  double lastT, penT, lastD, penD;
  int nbInput;
 public:
  cycleFunctionTime(double t1, double d1, double t2, double d2) : simpleFunctionTime<double>(d1,true,1.),_d1(d1), _t1(t1),_d2(d2), _t2(t2),_d3(d2), _t3(t2), _t4(t2), _d4(d2),_t5(t2),_d5(d2),_t6(t2),_d6(d2)
  {
    nbInput = 2;
    lastT = _t2; penT = _t1;
    lastD = _d2; penD = _d1;
  }
  cycleFunctionTime(double t1, double d1, double t2, double d2,double t3, double d3) : simpleFunctionTime<double>(d1,true,1.),_d1(d1), _t1(t1),_d2(d2), _t2(t2),_d3(d3), _t3(t3), _t4(t3), _d4(d3),_t5(t3),_d5(d3),_t6(t3),_d6(d3)
  {
    nbInput = 3;
    lastT = _t3; penT = _t2;
    lastD = _d3; penD = _d2;
  }
  cycleFunctionTime(double t1, double d1, double t2, double d2,double t3, double d3, double t4, double d4) : simpleFunctionTime<double>(d1,true,1.),_d1(d1), _t1(t1),_d2(d2), _t2(t2),_d3(d3), _t3(t3), _t4(t4), _d4(d4),_t5(t4),_d5(d4),_t6(t4),_d6(d4)
  {
    nbInput = 4;
    lastT = _t4; penT = _t3;
    lastD = _d4; penD = _d3;
  }
  cycleFunctionTime(double t1, double d1, double t2, double d2,double t3, double d3, double t4, double d4, double t5, double d5) : simpleFunctionTime<double>(d1,true,1.),_d1(d1), _t1(t1),_d2(d2), _t2(t2),_d3(d3), _t3(t3), _t4(t4), _d4(d4),_t5(t5),_d5(d5),_t6(t5),_d6(d5)
  {
    nbInput = 5;
    lastT = _t5; penT = _t4;
    lastD = _d5; penD = _d4;
  }
  cycleFunctionTime(double t1, double d1, double t2, double d2,double t3, double d3, double t4, double d4, double t5, double d5, double t6, double d6) : simpleFunctionTime<double>(d1,true,1.),_d1(d1), _t1(t1),_d2(d2), _t2(t2),_d3(d3), _t3(t3), _t4(t4), _d4(d4),_t5(t5),_d5(d5),_t6(t6),_d6(d6)
  {
    nbInput = 6;
    lastT = _t6; penT = _t5;
    lastD = _d6; penD = _d5;
  }

  virtual ~cycleFunctionTime(){}
  virtual double operator () (double x, double y, double z) const{
    double val =0.;
    if(time<=_t1)
      val = _d1*time/_t1;
    else if ((_t1<time) and (time<=_t2) and (nbInput >2))
      val = _d1+((_d2-_d1)/(_t2-_t1)*(time-_t1));
    else if ((_t2<time) and (time<=_t3) and (nbInput >3))
      val =  _d2+((_d3-_d2)/(_t3-_t2)*(time-_t2));
    else if ((_t3<time) and (time<=_t4) and (nbInput >4))
      val =  _d3+((_d4-_d3)/(_t4-_t3)*(time-_t3));
    else if ((_t4<time) and (time<=_t5) and (nbInput >5))
      val =  _d4+((_d5-_d4)/(_t5-_t4)*(time-_t4));
    else
      val =  penD+(lastD-penD)/(lastT-penT)*(time-penT);
    return val;
  }
};

class stepFunctionTime : public simpleFunctionTime<double>{
 protected:
  const int _nstep;
  const double _stepvalue;
  const double _tswitch;
  const double _tend;
  double _currentstep;
  double _currenttime;
 public:
  // BE AWARE tend > t_final simulation!!
  stepFunctionTime(const double fvalue,const int nstep,const double tend) : simpleFunctionTime<double>(1.,true,1.), _nstep(nstep),
                                                                             _stepvalue(fvalue/nstep),_tswitch(tend/nstep), _tend(tend),
                                                                            _currentstep(_stepvalue),_currenttime(_tswitch){}
  virtual ~stepFunctionTime(){}
  virtual void setTime(const double t)
  {
    time=t;
    if(time >_currenttime)
    {
      _currenttime+=_tswitch;
      _currentstep+=_stepvalue;
    }
  }
  virtual double operator () (double x,double y, double z) const{
    return _currentstep;
  }
};

class beltmanPressure : public simpleFunctionTime<double>
{
 protected:
  const double _vcj;
  const double _pcj;
  const SPoint3 _detonationSource;
  const SVector3 _detonationDirection;
  mutable double T0;
 public:
  beltmanPressure(const double x0, const double y0,const double z0,
                  const double u, const double v, const double w,
                  const double vcj,const double pcj) : simpleFunctionTime<double>(0,true,1.),_detonationSource(x0,y0,z0),
                                                       _detonationDirection(u,v,w), _vcj(vcj), _pcj(pcj){}
 #ifndef SWIG
  virtual ~beltmanPressure(){}
  virtual double operator() (double x,double y,double z) const
  {
    // distance from the source
    SPoint3 baryelem(x,y,z);
    SVector3 tmp(baryelem,_detonationSource);
    double distOnvcj = fabs(dot(tmp,_detonationDirection)) / _vcj;

    if(time < distOnvcj) return 0.;
    if(distOnvcj==0.)
      T0 = 1.; // avoid dividing by 0.
    else
      T0 = 3.*distOnvcj;
    return (_pcj * exp(-(time-distOnvcj)/T0));
  }
 #endif // SWIG
};

class bulletPlatePressure : public simpleFunctionTime<double>
{
 protected:
  const double _bulletDiameter;
  const double _holePlateDiameter;
  const double _p0;
  const SPoint3 _plateCenter;
  const double _bulletVelocity;
  const double _tanbulletAngle; // angle in rad !!
  double _distContact; // change with time
 public:
  bulletPlatePressure(const double x0, const double y0, const double z0,
                      const double db, const double dh, const double p0,
                      const double vi, const double alpha) : simpleFunctionTime<double>(0,true,1.),
                                                             _bulletDiameter(db), _holePlateDiameter(dh),
                                                             _p0(p0), _plateCenter(x0,y0,z0), _distContact(0.),
                                                             _tanbulletAngle(tan(alpha)), _bulletVelocity(vi){}
 #ifndef SWIG
  virtual ~bulletPlatePressure(){}
  virtual void setTime(const double t)
  {
    this->time = t;
    // compute contact distance
    _distContact = 0.5*_holePlateDiameter + _bulletVelocity * t / _tanbulletAngle;
    if(_distContact > 0.5*_bulletDiameter)
    {
      _distContact = 0.5*_bulletDiameter;
    }
  }
  virtual double operator() (double x, double y,double z) const
  {
    SPoint3 bary(x,y,z);
    double d = bary.distance(_plateCenter);
    if( d > _distContact)
      return 0.;
    else{
      return _p0;
    }
  }
 #endif // SWIG
};

class functionVelocityDCB : public simpleFunctionTime<double>{
 protected:
  const int _beamAxis; // x=0, y=1, z=2
  const double _beamLength; // length of beam supposed to have origin in 0
  const double _3rdPowerLength; // _beamLength^3
  const double _halfLength; // 0.5*_beamLength
 public:
  functionVelocityDCB(const double val_, const int baxis, const double blength) : simpleFunctionTime<double>(val_,false,1.),
                                                                                  _beamAxis(baxis), _beamLength(blength),
                                                                                  _3rdPowerLength(blength*blength*blength),
                                                                                  _halfLength(0.5*blength){}
 #ifndef SWIG
  functionVelocityDCB(const functionVelocityDCB &source) : simpleFunctionTime<double>(source), _beamAxis(source._beamAxis),
                                                           _beamLength(source._beamLength), _3rdPowerLength(source._3rdPowerLength),
                                                           _halfLength(source._halfLength){}
  virtual ~functionVelocityDCB(){}
  virtual double operator() (double x, double y,double z) const
  {
    double axiscoord;
    switch(_beamAxis){
     case 0:
      axiscoord = x;
      break;
     case 1:
      axiscoord = y;
      break;
     case 2:
      axiscoord = z;
      break;
    }
    if(axiscoord > _halfLength)
      axiscoord = _beamLength - axiscoord; // Symmetric profile
    return ( (4*axiscoord*axiscoord)/_3rdPowerLength*(3*_beamLength - 4*axiscoord )*this->_val);
  }
 #endif // SWIG
};

class functionDistanceFromPointByAxis : public simpleFunctionTime<double>
{
 protected:
  const int _dir; // x=0, y=1, z=2
  const double _pt; // value from which the distance is computed
 public:
  functionDistanceFromPointByAxis(const int dir, const double pt,const double val) : simpleFunctionTime<double>(val,false,0.),
                                                                                     _dir(dir), _pt(pt){}
 #ifndef SWIG
  functionDistanceFromPointByAxis(const functionDistanceFromPointByAxis &source) : simpleFunctionTime<double>(source),
                                                                                   _dir(source._dir), _pt(source._pt){}
  virtual ~functionDistanceFromPointByAxis(){}
  virtual double operator() (double x, double y,double z) const
  {
    double axiscoord;
    switch(_dir){
     case 0:
      axiscoord = x;
      break;
     case 1:
      axiscoord = y;
      break;
     case 2:
      axiscoord = z;
      break;
    }
    return (axiscoord-_pt)*this->_val; // negative if the coordinate is less than the axis coordinate !!
  }
 #endif // SWIG
};


class radialVelocityRing : public simpleFunctionTime<double>
{
 protected:
  // _w0 = val;
  const double _t0;
  const SPoint3 _origin;
  const int _comp; // to known if vx(=0) or vy(=1) has to be returned
 public:
  radialVelocityRing(const double w,const double t,const int comp) : simpleFunctionTime<double>(w,true,0.), _t0(t),
                                                                     _origin(0.,0.,0.), _comp(comp){}
 #ifndef SWIG
  radialVelocityRing(const radialVelocityRing &source) : simpleFunctionTime<double>(source), _t0(source._t0),
                                                         _origin(source._origin), _comp(source._comp){}
  virtual ~radialVelocityRing(){}
  virtual double operator() (double x, double y,double z) const
  {
    double w = _val/(2.*3.141592654);
    if(time<=_t0)
    {
      w*=(time/_t0);
    }

    SPoint3 P(x,y,z);
    double radius = _origin.distance(P);
    double alpha = asin(fabs(x)/radius);
    double wr = w*radius;
    if(_comp==0)
    {
      double cosa = cos(alpha);
      if(y>0)
        return (-wr*cosa);
      else
        return (wr*cosa);
    }
    else if(_comp==1)
    {
      double sina = sin(alpha);
      if(x<0)
        return (-wr*sina);
      else
        return (wr*sina);
    }
    else
    {
      Msg::Fatal("_comp has to be equal to 0 or 1!");
    }
  }
 #endif // SWIG
};



class centrifugalForceRing : public simpleFunctionTime<double>
{
 protected:
  // _w0 = val;
  const double _t0;
  const SPoint3 _origin;
  const int _comp; // to known if vx(=0) or vy(=1) has to be returned
  const double _rho; // density
  mutable double _w,_rw2;
 public:
  centrifugalForceRing(const double rho,const double w,const double t,const int comp) : simpleFunctionTime<double>(w/(2.*3.141592654),true,0.), _t0(t),
                                                                     _origin(0.,0.,0.), _comp(comp), _w(0.), _rho(rho), _rw2(0.){}
 #ifndef SWIG
  centrifugalForceRing(const centrifugalForceRing &source) : simpleFunctionTime<double>(source), _t0(source._t0),
                                                         _origin(source._origin), _comp(source._comp), _w(0.), _rho(source._rho), _rw2(0){}
  virtual ~centrifugalForceRing(){}
  virtual void setTime(const double t)
  {
    time = t;
    if(time<=_t0)
    {
      _w=_val*(time/_t0);
    }
    else
    {
      _w = _val;
    }
    _rw2 = _rho*_w*_w;
  }

  virtual double operator() (double x, double y,double z) const
  {
    if(_comp==0)
      return _rw2*x;
    else if(_comp==1)
      return _rw2*y;
    else
      Msg::Fatal("_comp has to be equal to 0 or 1!");
  }
 #endif // SWIG
};

class linearPositionFunctionTime : public simpleFunctionTime<double>{
  protected:
    double _a, _b, _c, _d; // ax+by+cz+d

  public:
    linearPositionFunctionTime(const double a, const double b, const double c, const double d,
                               bool td=true, double t=1.): simpleFunctionTime<double>(0,td,t),
                               _a(a),_b(b),_c(c),_d(d){};
    virtual ~linearPositionFunctionTime(){};
  #ifndef SWIG
    virtual double operator () (double x, double y, double z) const {
      if (this->timeDependency){
        return time*(_a*x+_b*y+_c*z+_d);
      }
      else return  _a*x+_b*y+_c*z+_d;
    }
  #endif //SWIG
};

class BCfunctionBendWings : public simpleFunctionTime<double>
{
  protected:
    const double _theta;
    const double _Lx;
    const double _nu;
  public:
    BCfunctionBendWings(const double magnitude,const double nu,const double Lx,
                        const double overhang) : simpleFunctionTime<double>(magnitude,false,1.),
                                               _theta(magnitude/overhang), _nu(nu), _Lx(Lx){}
    BCfunctionBendWings(const BCfunctionBendWings &source) : simpleFunctionTime<double>(source),
                                                             _theta(source._theta), _nu(source._nu), _Lx(source._Lx){}
    virtual ~BCfunctionBendWings(){}
  #ifndef SWIG
    virtual double operator () (double x, double y, double z) const {
      return -_theta*x*(1.0-x/_Lx)-_nu*_theta/_Lx*y*y;
    }
  #endif //SWIG
};

/**
 * Class for python user defined function
 * The user has to define in it driver a
 * def my_function(x,y,z,t,extra=None)
 *     ...
 *     return <a float>
 * where extra is a python tuple that contains
 * extra python argument for the function
 */
 #include<Python.h>
class PythonBCfunctionDouble : public simpleFunctionTime<double>
{
 protected:
  PyObject* _pyFunction;
  PyObject* _pyArgs; // Assume that is a tuple
 public:
  PythonBCfunctionDouble(PyObject* pyfunc, PyObject* args) : simpleFunctionTime<double>(),
                                                       _pyFunction(pyfunc), _pyArgs(args){}
 #ifndef SWIG
  PythonBCfunctionDouble(const PythonBCfunctionDouble &src) : simpleFunctionTime<double>(src),
                                                  _pyFunction(src._pyFunction),
                                                  _pyArgs(src._pyArgs){}
  virtual ~PythonBCfunctionDouble(){}
  virtual double operator() (double x, double y, double z) const {
    PyObject* tuple = Py_BuildValue("ddddO",x, y, z, this->time,_pyArgs);
    PyObject* returnVal = PyObject_Call(_pyFunction,tuple,NULL);
    return PyFloat_AsDouble(returnVal);
  }
 #endif // SWIG
};

#endif // TIMEFUNCTION

