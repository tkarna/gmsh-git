//
// Description: explicit hulbert chung system based on PETSc implementation
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef EXPLICITHULBERTCHUNGPETSC_H_
#define EXPLICITHULBERTCHUNGPETSC_H_
#include "explicitHulbertChung.h"

#if defined(HAVE_PETSC)
#include <petsc.h>
#include <petscksp.h>
#include <math.h>
// print for debug

static double frand(const double a,const double b){
  return (rand()/(double)RAND_MAX) * (b-a) + a;
}

void printPETScVector(Vec &a,int vsize,const char *vname){
  double val;
  for(int i=0; i<vsize;i++){
    VecGetValues(a, 1, &i, &val);
    printf("%s comp %d %e\n",vname,i,val);
  }
  printf("---\n");
}

// 4 vectors are necessary per step and two step are kept
// current and next. Creation of a class regrouping the four vector
template<class scalar>
class explicitStepPetsc{
 private:
  void _try(int ierr) const { CHKERRABORT(PETSC_COMM_WORLD, ierr); }
 protected:
  bool _benson;
 public : // Direct access by the system
  Vec _Fext, _Fint, _x, _xdot, _xddot, _Psi;
  explicitStepPetsc(bool benson) : _benson(benson){}
  ~explicitStepPetsc(){}
  void clear(){
   _try(VecDestroy(&_Fext));
   _try(VecDestroy(&_Fint));
   _try(VecDestroy(&_x));
   _try(VecDestroy(&_xdot));
   _try(VecDestroy(&_xddot));
   if(_benson) _try(VecDestroy(&_Psi));
  }
  explicitStepPetsc& operator=(const explicitStepPetsc &other)
  {
    _try(VecCopy(other._Fext,_Fext));
    _try(VecCopy(other._Fint,_Fint));
    _try(VecCopy(other._x,_x));
    _try(VecCopy(other._xdot,_xdot));
    _try(VecCopy(other._xddot,_xddot));
    if(_benson) _try(VecCopy(other._Psi,_Psi));
    return *this;
  }

  void allocate(int nbRows){
   // Create sequential vector even in mpi (the cut over all cpu is made a top level)
//    _try(VecCreate(PETSC_COMM_WORLD, &_x));
//    _try(VecSetSizes(_x, nbRows, PETSC_DETERMINE));
//    _try(VecSetFromOptions(_x)); // Usefull ??
    _try(VecCreateSeq(PETSC_COMM_SELF,nbRows, &_x));
    _try(VecDuplicate(_x, &_Fext));
    _try(VecDuplicate(_x, &_Fint));
    _try(VecDuplicate(_x, &_xdot));
    _try(VecDuplicate(_x, &_xddot));
    if(_benson)
    {
      _try(VecDuplicate(_x, &_Psi));
      for(int row=0; row<nbRows;row++)
      {
        PetscInt i = row;
        PetscScalar s = frand(0.,1.);
        _try(VecSetValues(_Psi, 1, &i, &s, INSERT_VALUES));
      }
    }
  }
};

template<class scalar>
class explicitHulbertChungPetsc : public explicitHulbertChung<scalar>{
 public :
//  enum state{current, next};
//  enum whatValue{position, velocity, acceleration};

 protected:
  int _blockSize; // for block Matrix (not used for now)
  bool _isAllocated, _whichStep, _imassAllocated, _dynamicRelaxation, _timeStepByBenson;
  double _alpham, _beta,_gamma; // parameters of scheme
  double _timeStep; // value of time step
  int _nbRows; // To know the system size.
  // values used by solve function
  double _oneDivbyOneMinusAlpham,_minusAlphamDivbyOneMinusAlpham;
  double _oneMinusgammaDeltat,_gammaDeltat,_Deltat2halfMinusbeta,_Deltat2beta;
  explicitStepPetsc<scalar> *_currentStep, *_previousStep, *_step1, *_step2;
  Vec _M; // mass matrix
  Vec _invM; // inverse of mass matrix
  mutable Vec _v2; // square of velocitie (kinetic energy) // and Fext n+1 -Fext n for external work
  Vec _b; // tempory value _Fext - _Fin
  mutable Vec _S; // for dynamic relaxation only (diagonal approximation of stiffness matrix) and Delta u for external work
  // For rigid contact in MPI ( a double* contains the MPIAllReduce performs in the dofManager which ensure MPI communication)
  scalar* _reducedRigidContactForceMPI;
  int _indexFirstRigidContactMPI;

  // function to invert mass matrix
  void invertMassMatrix(){
    _try(VecCopy(_M,_invM));
    _try(VecReciprocal(_invM));
    _imassAllocated=true;
  }
  virtual int systemSize()const{return _nbRows;}
 private:
  void _try(int ierr) const { CHKERRABORT(PETSC_COMM_WORLD, ierr); }

 public:
  explicitHulbertChungPetsc(double alpham=0., double beta=0.,
                        double gamma=0.5,bool benson=false, bool dynrel=false) : _isAllocated(false), _whichStep(true), _blockSize(0),
                                                              _alpham(alpham), _beta(beta), _gamma(gamma),
                                                              _timeStep(0.), _nbRows(0), _imassAllocated(false),
                                                              _reducedRigidContactForceMPI(NULL), _indexFirstRigidContactMPI(0),
                                                              _dynamicRelaxation(dynrel), _timeStepByBenson(benson){
    _oneDivbyOneMinusAlpham = 1./(1.-_alpham);
    _minusAlphamDivbyOneMinusAlpham = - _alpham * _oneDivbyOneMinusAlpham;
    _oneMinusgammaDeltat = (1.-_gamma)*_timeStep;
    _gammaDeltat = _gamma*_timeStep;
    _Deltat2halfMinusbeta = _timeStep*_timeStep *(0.5-_beta);
    _Deltat2beta = _timeStep * _timeStep * _beta;
    _step1 = new explicitStepPetsc<scalar>(_timeStepByBenson);
    _step2 = new explicitStepPetsc<scalar>(_timeStepByBenson);
    _currentStep = _step1;
    _previousStep = _step2;
  }
  ~explicitHulbertChungPetsc(){delete _step1; delete _step2;}

  void nextStep(){
    if(_whichStep){
      _currentStep = _step2;
      _previousStep = _step1;
      _whichStep =false;
    }
    else{
     _currentStep = _step1;
     _previousStep = _step2;
     _whichStep = true;
    }
  }

  virtual void clear(){
    if(_isAllocated){
      _try(VecDestroy(&_M));
      _try(VecDestroy(&_invM));
      _try(VecDestroy(&_v2));
      _try(VecDestroy(&_b));
      _step1->clear();
      _step2->clear();
      _nbRows=0;
      if(_dynamicRelaxation)
        _try(VecDestroy(&_S));
    }
    if(_reducedRigidContactForceMPI != NULL)
    {
      delete[] _reducedRigidContactForceMPI;
      _reducedRigidContactForceMPI = NULL;
    }
    _isAllocated=false;
    _imassAllocated=false;
  }
  // Or compute directly the time step here ??
  virtual void setTimeStep(const double dt){
    _timeStep = dt;
    // update variables which depends on _timeStep
    _oneMinusgammaDeltat = (1.-_gamma)*_timeStep;
    _gammaDeltat = _gamma*_timeStep;
    _Deltat2halfMinusbeta = _timeStep*_timeStep *(0.5-_beta);
    _Deltat2beta = _timeStep * _timeStep * _beta;

  }
  virtual double getTimeStep()const{return _timeStep;}
  virtual bool isAllocated() const { return _isAllocated; }
  virtual void allocate(int nbRows){
    clear();
//    _try(VecCreate(PETSC_COMM_WORLD, &_M));
//    _try(VecSetSizes(_M, nbRows, PETSC_DETERMINE));
//    _try(VecSetFromOptions(_M)); // Usefull ??
    // sequential creation (manage mpi at top level)
    _try(VecCreateSeq(PETSC_COMM_SELF,nbRows, &_M));
    _try(VecDuplicate(_M, &_invM));
    _try(VecDuplicate(_M, &_v2));
    _try(VecDuplicate(_M, &_b));
    _try(VecDuplicate(_M,&_S)); //  llocated as used in external work computation
    _step1->allocate(nbRows);
    _step2->allocate(nbRows);
    _isAllocated = true;
    _nbRows=nbRows;
  }
  // get the value of diagonalized mass matrix col is not used
  virtual void getFromMatrix(int row, int col, scalar &val) const{
#if defined(PETSC_USE_COMPLEX)
    PetscScalar *tmp;
    _try(VecGetArray(_M, &tmp));
    PetscScalar s = tmp[row];
    _try(VecRestoreArray(_M, &tmp));
    // FIXME specialize this routine
    val = s.real();
#else
    VecGetValues(_M, 1, &row, &val);
#endif
  }
  virtual void addToRightHandSide(int row, const scalar &val)
  {
    PetscInt i = row;
    PetscScalar s = val;
    _try(VecSetValues(_currentStep->_Fext, 1, &i, &s, ADD_VALUES));
    #ifdef _DEBUG
      Msg::Warning("addToRightHandSide is deprecated for explicitHulbertChung system. Please use addToRightHandSidePlus or addToRightHandSideMinus");
    #endif
  }
  virtual void addToRightHandSidePlus(int row, const scalar &val)
  {
    PetscInt i = row;
    PetscScalar s = val;
    _try(VecSetValues(_currentStep->_Fext, 1, &i, &s, ADD_VALUES));
  }
  virtual void addToRightHandSideMinus(int row, const scalar &val)
  {
    PetscInt i = row;
    PetscScalar s = val;
    _try(VecSetValues(_currentStep->_Fint, 1, &i, &s, ADD_VALUES));
  }

  virtual void getFromRightHandSide(int row, scalar &val) const
  {
#if defined(PETSC_USE_COMPLEX)
    PetscScalar *tmp;
    _try(VecGetArray(_currentStep->_Fext, &tmp));
    PetscScalar s = tmp[row];
    _try(VecRestoreArray(_currentStep->_Fext, &tmp));
    // FIXME specialize this routine
    val = s.real();
    _try(VecGetArray(_currentStep->_Fint, &tmp));
    PetscScalar s = tmp[row];
    _try(VecRestoreArray(_currentStep->_Fint, &tmp));
    val -= s.real();
#else
    scalar tmp;
    VecGetValues(_currentStep->_Fext, 1, &row, &tmp);
    val = tmp;
    VecGetValues(_currentStep->_Fint, 1, &row, &tmp);
    val -=tmp;
#endif
  }

  virtual void getFromRightHandSidePlus(int row, scalar &val) const
  {
#if defined(PETSC_USE_COMPLEX)
    PetscScalar *tmp;
    _try(VecGetArray(_currentStep->_Fext, &tmp));
    PetscScalar s = tmp[row];
    _try(VecRestoreArray(_currentStep->_Fext, &tmp));
    // FIXME specialize this routine
    val = s.real();
#else
    VecGetValues(_currentStep->_Fext, 1, &row, &val);
#endif
  }

  virtual void getFromRightHandSideMinus(int row, scalar &val) const
  {
#if defined(PETSC_USE_COMPLEX)
    PetscScalar *tmp;
    _try(VecGetArray(_currentStep->_Fint, &tmp));
    PetscScalar s = tmp[row];
    _try(VecRestoreArray(_currentStep->_Fint, &tmp));
    // FIXME specialize this routine
    val = s.real();
#else
    VecGetValues(_currentStep->_Fint, 1, &row, &val);
#endif
  }
  virtual double normInfRightHandSide() const
  {
    PetscReal nor;
    _try(VecCopy(_currentStep->_Fext,_b));
    _try(VecAXPY(_b,-1,_currentStep->_Fint));
    _try(VecNorm(_b, NORM_INFINITY, &nor));
   #if defined(HAVE_MPI)
    if(Msg::GetCommSize() != 1)
    {
      double norDouble = nor;
      double norMPI;
      MPI_Allreduce(&norDouble,&norMPI,1,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
      nor = norMPI;
    }
   #endif //HAVE_MPI
    return nor;
  }
  virtual double norm0Inf() const{
    PetscReal norFext;
    PetscReal norFint;
    _try(VecNorm(_currentStep->_Fext, NORM_INFINITY, &norFext));
    _try(VecNorm(_currentStep->_Fint, NORM_INFINITY, &norFint));
   #if defined(HAVE_MPI)
    if(Msg::GetCommSize() > 1)
    {
      double nn[2] = {norFext,norFint};
      double nntmp[2];
      MPI_Allreduce(nn,nntmp,2,MPI_DOUBLE,MPI_MAX,MPI_COMM_WORLD);
      norFext = nntmp[0];
      norFint = nntmp[1];
    }
   #endif // HAVE_MPI
    return norFext + norFint;
  }

  // Add to mass matrix (which is diagonalized)
  virtual void addToMatrix(int row, int col, const scalar &val){
    Msg::Error("No stiffness matrix for an explicit newmark scheme");
  }
  virtual void addToMatrix(int row, int col, const scalar &val, const typename explicitHulbertChung<scalar>::whichMatrix wm)
  {
    if(wm == explicitHulbertChung<scalar>::stiff){
      this->addToMatrix(row,col,val);
    }
    else if( wm == explicitHulbertChung<scalar>::mass){
      PetscInt i = row;
      PetscScalar s = val;
      _try(VecSetValues(_M, 1, &i, &s, ADD_VALUES));
    }
    else{
     Msg::Error("stiff and mass are the only possible matrix choice");
    }

  }

  virtual void getFromSolution(int row, scalar &val) const
  {
   #if defined(PETSC_USE_COMPLEX)
    PetscScalar *tmp;
    _try(VecGetArray(_currentStep->_x, &tmp));
    PetscScalar s = tmp[row];
    _try(VecRestoreArray(_currentStep->_x, &tmp));
    val = s.real();
   #else
    VecGetValues(_currentStep->_x, 1, &row, &val);
   #endif // PETSC_USE_COMPLEX
  }

  virtual void getFromPreviousSolution(int row, scalar &val)  const
  {
   #if defined(PETSC_USE_COMPLEX)
    PetscScalar *tmp;
    _try(VecGetArray(_previousStep->_x, &tmp));
    PetscScalar s = tmp[row];
    _try(VecRestoreArray(_previousStep->_x, &tmp));
    val = s.real();
   #else
    VecGetValues(_previousStep->_x, 1, &row, &val);
   #endif // PETSC_USE_COMPLEX
  }

  virtual void getFromSolution(int row, scalar &val, const nonLinearBoundaryCondition::whichCondition wv) const
  {
    switch(wv){
     case nonLinearBoundaryCondition::position:
       this->getFromSolution(row,val);
     break;
     case nonLinearBoundaryCondition::velocity:
#if defined(PETSC_USE_COMPLEX)
      PetscScalar *tmp;
      _try(VecGetArray(_currentStep->_xdot, &tmp));
      PetscScalar s = tmp[row];
      _try(VecRestoreArray(_currentStep->_xdot, &tmp));
      val = s.real();
#else
      VecGetValues(_currentStep->_xdot, 1, &row, &val);
#endif
     break;
     case nonLinearBoundaryCondition::acceleration:
#if defined(PETSC_USE_COMPLEX)
      PetscScalar *tmp;
      _try(VecGetArray(_currentStep->_xddot, &tmp));
      PetscScalar s = tmp[row];
      _try(VecRestoreArray(_currentStep->_xddot, &tmp));
      val = s.real();
#else
      VecGetValues(_currentStep->_xddot, 1, &row, &val);
#endif
     break;
     default:
       Msg::Error("Impossible to get value from solution. Only possible choices position, velocity, acceleration ");
    }

  }

/*  virtual void getFromSolution(int row, scalar &val) const
  {
#if defined(PETSC_USE_COMPLEX)
      PetscScalar *tmp;
      _try(VecGetArray(_currentStep->_x, &tmp));
      PetscScalar s = tmp[row];
      _try(VecRestoreArray(_currentStep->_x, &tmp));
      val = s.real();
#else
      VecGetValues(_currentStep->_x, 1, &row, &val);
#endif
  }*/


  virtual void zeroMatrix(){
    if (_isAllocated) {
      _try(VecAssemblyBegin(_M));
      _try(VecAssemblyEnd(_M));
      _try(VecZeroEntries(_M));
    }
  }

  virtual void zeroRightHandSide()
  {
    if (_isAllocated) {
//      _try(VecAssemblyBegin(_b));
//      _try(VecAssemblyEnd(_b)); // NO NEED _b is a tempory internal vector
//      _try(VecZeroEntries(_b));
      _try(VecAssemblyBegin(_currentStep->_Fext));
      _try(VecAssemblyEnd(_currentStep->_Fext));
      _try(VecZeroEntries(_currentStep->_Fext));
      _try(VecAssemblyBegin(_currentStep->_Fint));
      _try(VecAssemblyEnd(_currentStep->_Fint));
      _try(VecZeroEntries(_currentStep->_Fint));
    }
  }

  int systemSolve(){
#if defined(PETSC_USE_COMPLEX)
  Msg::Error("explicit Newmark resolution is not implemented for complex number\n");
#else
    // check if the mass matrix is computed or not
    if(!_imassAllocated) this->invertMassMatrix();

    // set the value of contact nodes in MPI (after reduction of force)
    #if defined(HAVE_MPI)
    if((Msg::GetCommSize()) > 1 and (_indexFirstRigidContactMPI > 0))
    {
      int k=0;
      for(int j=_indexFirstRigidContactMPI; j<_nbRows; j++)
      {
        // the value of forces are on the previous step !!
        PetscInt i = j;
        PetscScalar s = _reducedRigidContactForceMPI[k];
        _try(VecSetValues(_previousStep->_Fext, 1, &i, &s, INSERT_VALUES));
        k++;
      }
    }
    #endif // HAVE_MPI

   // compute _b first = _Fext - _Fint
    _try(VecCopy(_previousStep->_Fext,_b));
    _try(VecAXPY(_b,-1,_previousStep->_Fint));

// The following comment are kept to explain what is done component by component
//    double x,xdot,xddot,fextMinusfint, fext, fint imasse, xddotn, xdotn, xn;
//    for(PetscInt i=0;i<_nbRows;i++){

      // accelerations n+1
//      VecGetValues(_previousStep->_Fext, 1, &i, &fext);
//      VecGetValues(_previousStep->_Fint, 1, &i, &fint);
//      fextMinusfint = fext-fint;
//      VecGetValues(_invM, 1, &i, &imasse);
//      VecGetValues(_previousStep->_xddot, 1 , &i, &xddotn);
//      xddot = _oneDivbyOneMinusAlpham*imasse*fextMinusfint + _minusAlphamDivbyOneMinusAlpham * xddotn;
//      _try(VecSetValues(_currentStep->_xddot, 1, &i, &xddot, INSERT_VALUES));
      _try(VecPointwiseMult(_currentStep->_xddot,_invM,_b));
      _try(VecScale(_currentStep->_xddot,_oneDivbyOneMinusAlpham));
      _try(VecAXPY(_currentStep->_xddot,_minusAlphamDivbyOneMinusAlpham,_previousStep->_xddot));
      // velocities n+1
//      VecGetValues(_previousStep->_xdot, 1 , &i, &xdotn);
//      xdot = xdotn + _oneMinusgammaDeltat*xddotn + _gammaDeltat*xddot;
//      _try(VecSetValues(_currentStep->_xdot, 1 , &i, &xdot, INSERT_VALUES));
      _try(VecCopy(_previousStep->_xdot,_currentStep->_xdot));
      // Regroups the two in one operation with VecMAXPY ??
      _try(VecAXPY(_currentStep->_xdot,_oneMinusgammaDeltat,_previousStep->_xddot));
      _try(VecAXPY(_currentStep->_xdot,_gammaDeltat,_currentStep->_xddot));
      /* dynamic relaxation */
      if(_dynamicRelaxation)
      {
          double dampingFactor = this->dynamicRelaxationFactor();
          _try(VecScale(_currentStep->_xdot,dampingFactor));
      }

      // positions n+1
//      VecGetValues(_previousStep->_x, 1 , &i, &xn);
//      x = xn + _timeStep * xdotn + _Deltat2halfMinusbeta * xddotn + _Deltat2beta * xddot;
//      _try(VecSetValues(_currentStep->_x, 1 , &i, &x, INSERT_VALUES));
      _try(VecCopy(_previousStep->_x,_currentStep->_x));
      // Regroups the three in one operation with VecMAXPY ??
      _try(VecAXPY(_currentStep->_x,_timeStep,_previousStep->_xdot));
      _try(VecAXPY(_currentStep->_x,_Deltat2halfMinusbeta,_previousStep->_xddot));
      _try(VecAXPY(_currentStep->_x,_Deltat2beta,_currentStep->_xddot));
 //   }
#endif
    return 1;
  }

    // Specific functions (To put initial conditions)
    // set on current step a next step operation is necessary after the prescribtion of initial value step0->step1
    void setInitialCondition(int row, scalar val, const typename nonLinearBoundaryCondition::whichCondition wc=nonLinearBoundaryCondition::position){ //CANNOT PASS VAL BY REF WHY ??
      PetscInt i = row;
      switch(wc){
       case nonLinearBoundaryCondition::position:
        _try(VecSetValues(_currentStep->_x, 1, &i, &val, INSERT_VALUES));
        break;
       case nonLinearBoundaryCondition::velocity:
        _try(VecSetValues(_currentStep->_xdot, 1, &i, &val, INSERT_VALUES));
        break;
       case nonLinearBoundaryCondition::acceleration:
        _try(VecSetValues(_currentStep->_xddot, 1 , &i, &val, INSERT_VALUES));
        break;
       default:
        Msg::Warning("Invalid initial conditions");
      }
    }
    virtual void setInitialValues(const int row, scalar val){
      this->setInitialCondition(row,val,nonLinearBoundaryCondition::position);
    }

    // Get mass of system
    virtual double getSystemMass(){
      double m,mele;
      m=0.;
      for(PetscInt i=0;i<_nbRows;i++){
        _try(VecGetValues(_M, 1, &i, &mele));
        m+=mele;
      }
      return m;
    }
  // function to get the kinetic energy
  virtual double getKineticEnergy(const int syssize) const{
    if(isAllocated()){
      PetscScalar ener;
      _try(VecPointwiseMult(_v2,_currentStep->_xdot,_currentStep->_xdot));
      // put zero velocity for rigid contact node (avoid to take into account this contribution)
      PetscScalar s = 0.;
      for(int i=syssize; i<this->_nbRows;i++)
      {
        _try(VecSetValues(_v2, 1, &i, &s, INSERT_VALUES));
      }
     #if defined(HAVE_MPI)
      if(Msg::GetCommSize() > 1)
      {
        _try(VecAssemblyBegin(_v2));
        _try(VecAssemblyEnd(_v2));
      }
     #endif // HAVE_MPI
      _try(VecDot(_M,_v2,&ener));
      return 0.5*ener;
    }
    else return 0.;
  }
  virtual double getExternalWork(const int syssize) const
  {
    PetscScalar wext;
    _try(VecCopy(_previousStep->_Fext,_v2));
    _try(VecAXPY(_v2,1,_currentStep->_Fext));
    _try(VecCopy(_currentStep->_x,_S));
    _try(VecAXPY(_S,-1,_previousStep->_x));
    // put zero for displacement of rigid contact node (avoid to take into account this contribution)
    PetscScalar s = 0.;
    for(int i=syssize; i<this->_nbRows;i++)
    {
      _try(VecSetValues(_S, 1, &i, &s, INSERT_VALUES));
    }
     #if defined(HAVE_MPI)
      if(Msg::GetCommSize() > 1)
      {
        _try(VecAssemblyBegin(_S));
        _try(VecAssemblyEnd(_S));
      }
     #endif // HAVE_MPI
    _try(VecDot(_v2,_S,&wext));
    return 0.5*wext;
  }

  virtual void resetUnknownsToPreviousTimeStep()
  {
    (*_currentStep) = (*_previousStep);
  }
  #if defined(HAVE_MPI)
  virtual scalar* getArrayIndexPreviousRightHandSidePlusMPI(const int index)
  {
    // create vector comm if needed
    if(index != _indexFirstRigidContactMPI)
    {
      _indexFirstRigidContactMPI = index;
      if(_reducedRigidContactForceMPI != NULL) delete[] _reducedRigidContactForceMPI;
      _reducedRigidContactForceMPI = new scalar[_nbRows - index];
    }
    return _reducedRigidContactForceMPI;
  }
//  virtual void setPreviousRightHandSidePlusMPI(const int row, const scalar &val)
//  {
//    #ifdef _DEBUG
//      Msg::Warning("MPI version is not implemented for explicitHulbertChungPETSc so normally the function setPreviousRightHandSidePlusMPI should not be called. However this function is defined properly");
//    #endif // _DEBUG
//    PetscInt i = row;
//    PetscScalar s = val;
//    _try(VecSetValues(_previousStep->_Fext, 1, &i, &s, INSERT_VALUES));
//  }
  #endif // HAVE_MPI
  virtual void setDynamicRelaxation(const bool dynrel)
  {
    _dynamicRelaxation = dynrel;
  }

  virtual double dynamicRelaxationFactor()
  {
    /* factor = (1+2*c*_timeStep)^{-1} */
    // compute c = \sqrt{\frac{x^T_n F^{int}_N}{x^T_n M x_n}}
    // for MPI compute local value and then communicate the result
    PetscScalar xFint, xMx;
    _try(VecDot(_previousStep->_x,_previousStep->_Fint,&xFint));

    // MPI comm
   #if defined(HAVE_MPI)
    if(Msg::GetCommSize() > 1)
    {
      double mpitmp =0.;
      double mpitmp2 = xFint;
      MPI_Allreduce(&mpitmp2,&mpitmp,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
      xFint = mpitmp;
    }
   #endif // HAVE_MPI
    if(xFint == 0.)
      return 1.; // no deplacement --> no damping factor --> c = 0 and factor = 1

    _try(VecPointwiseMult(_S,_previousStep->_x,_M));
    _try(VecDot(_S,_previousStep->_x,&xMx));

    // MPI comm
   #if defined(HAVE_MPI)
    if(Msg::GetCommSize() > 1)
    {
      double mpitmp =0.;
      double mpitmp2 = xMx;
      MPI_Allreduce(&mpitmp2,&mpitmp,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
      xMx = mpitmp;
    }
   #endif // HAVE_MPI
     return (1./(1. + 2.*sqrt(fabs(xFint)/xMx)*_timeStep));
  }
};

#else
template <class scalar>
class explicitHulbertChungPetsc : public linearSystem<scalar> {
 protected:
   virtual double dynamicRelaxationFactor(){return 1.;}
   virtual int systemSize()const{return _nbRows;}
 public :
  explicitHulbertChungPetsc()
  {
    Msg::Error("PETSc is not available in this version of Gmsh and so it is impossible to use explicit alpha generalized scheme");
  }
  virtual bool isAllocated() const { return false; }
  virtual void allocate(int nbRows) {}
  virtual void clear(){}
  virtual void addToMatrix(int row, int col, const scalar &val) {}
  virtual void getFromMatrix(int row, int col, scalar &val) const {}
  virtual void addToRightHandSide(int row, const scalar &val) {}
  virtual void addToRightHandSidePlus(int row, const scalar &val) {}
  virtual void addToRightHandSideMinus(int row, const scalar &val) {}
  virtual void getFromRightHandSide(int row, scalar &val) const { return 0.; }
  virtual void getFromRightHandSidePlus(int row, scalar &val) const { return 0.; }
  virtual void getFromRightHandSideMinus(int row, scalar &val) const { return 0.; }
  virtual void getFromPreviousSolution(int row, scalar &val)  const {return 0.;}
  virtual void getFromSolution(int row, scalar &val) const { return 0.; }
  virtual void zeroMatrix() {}
  virtual void zeroRightHandSide() {}
  virtual int systemSolve() { return 0; }
  virtual double normInfRightHandSide() const{return 0;}
  virtual double getKineticEnergy(){return 0;}
  virtual double getExternalWork(const int syssize) const{return 0;}
  virtual void getFromSolution(int row, scalar &val, const nonLinearBoundaryCondition::whichCondition wv){};
  virtual void nextStep(){}
  virtual void setTimeStep(const double dt){}
  virtual void addToMatrix(int row, int col, const scalar &val, const typename explicitHulbertChung<scalar>::whichMatrix wm){}
  virtual void setInitialCondition(int row, scalar val, const typename nonLinearBoundaryCondition::whichCondition wc=nonLinearBoundaryCondition::position){}
  virtual void resetUnknownsToPreviousTimeStep(){}; // to restart with a smaller time step if convergence problem
  virtual void setUnknownsPreviousStep(){};
  virtual void setDynamicRelaxation(const bool dynrel){}
  #if defined(HAVE_MPI)
  virtual scalar* getArrayIndexPreviousRightHandSidePlusMPI(const int index){return NULL;}
//  virtual void setPreviousRightHandSidePlusMPI(const int row, const scalar &val){};
  #endif // HAVE_MPI
};
#endif // PETSC

#endif //EXPLICITHULBERTCHUNGPETSC_H_
