#ifndef IMPLICITEHULBERTCHUNGPETSC_H_
#define IMPLICITEHULBERTCHUNGPETSC_H_

#include "nonLinearSystems.h"
#if defined(HAVE_PETSC)

template<class scalar>
class implicitStepPetsc{
 private:
  void _try(int ierr) const { CHKERRABORT(PETSC_COMM_WORLD, ierr); }

 public : // Direct access by the system
  Vec _Fext, _Fint, _x, _xdot, _xddot;
  implicitStepPetsc(){}
  ~implicitStepPetsc(){}
  void clear(){
   _try(VecDestroy(&_Fext));
   _try(VecDestroy(&_Fint));
   _try(VecDestroy(&_x));
   _try(VecDestroy(&_xdot));
   _try(VecDestroy(&_xddot));
  }
  implicitStepPetsc& operator=(const implicitStepPetsc &other)
  {
    #if defined(HAVE_MPI)
    if(Msg::GetCommSize() > 1)
    {
      _try(VecAssemblyBegin(other._Fext));
      _try(VecAssemblyEnd(other._Fext));
      _try(VecAssemblyBegin(other._Fint));
      _try(VecAssemblyEnd(other._Fint));

      _try(VecAssemblyBegin(other._x));
      _try(VecAssemblyEnd(other._x));
      _try(VecAssemblyBegin(other._xdot));
      _try(VecAssemblyEnd(other._xdot));
      _try(VecAssemblyBegin(other._xddot));
      _try(VecAssemblyEnd(other._xddot));
    }
    #endif // HAVE_MPI

    _try(VecCopy(other._Fext,_Fext));
    _try(VecCopy(other._Fint,_Fint));
    _try(VecCopy(other._x,_x));
    _try(VecCopy(other._xdot,_xdot));
    _try(VecCopy(other._xddot,_xddot));
    return *this;
  }


  void allocate(int nbRows, Vec* vec = NULL){
    if (vec == NULL){
      _try(VecCreate(PETSC_COMM_WORLD, &_x));
      _try(VecSetSizes(_x, nbRows, PETSC_DETERMINE));
      _try(VecSetFromOptions(_x)); // Usefull ??
    }
    else{
      _try(VecDuplicate(*vec,&_x));
    }

    _try(VecDuplicate(_x, &_Fext));
    _try(VecDuplicate(_x, &_Fint));
    _try(VecDuplicate(_x, &_xdot));
    _try(VecDuplicate(_x, &_xddot));
  }
};

/**
Class for solving the dynamic problem
(1-alpham)*M*xddot_{n+1} + alpham*xddot_n = (1-alphaf)*(fext_{n+1} - fint_{n+1})+ alphaf*(fext_n -fint_n)
**/


template<class scalar>
class implicitHulbertChungPetsc : public nonLinearSystem<scalar>,
                                  public linearSystemPETSc<scalar>{

  protected:
    bool _whichStep;
    double _alpham,_alphaf, _beta,_gamma; // parameters of scheme
    double _timeStep; // value of time step
    int _nbRows; // To know the system size.

    implicitStepPetsc<scalar> *_currentStep, *_previousStep, *_initialStep, *_step1, *_step2;
    Mat _M; // mass matrix

    int _numIteration;
    double OneMinusAlphaf; // 1 - alphaf
    double AlphafMinusOne; //
    double MinusAlphaf;
    double OneMinusAlpham; // 1-alpham
    double MinusAlpham; //
    double AlphamMinusOne; //
    double Deltat2halfMinusbeta;// dt^2(0.5-beta)
    double DeltatOneMinusGamma; // dt(1-gamma)
    double GammaDivByBetaDeltat; // gamma/(beta*dt)
    double OneMinusAlphamDivByBetaDeltat2; // (1-alpham)/(beta*dt^2)
    double OneDivByBetaDeltat2; // 1/(beta*dt^2)

    mutable bool _flagb; // flag for right hand side computation

  public:

    implicitHulbertChungPetsc(double alpham=0., double alphaf = 1., double beta=0., double gamma=0.5):
          _alphaf(alphaf), _alpham(alpham),_beta(beta),_gamma(gamma),
          linearSystemPETSc<scalar>(),_whichStep(true),_timeStep(0.), _nbRows(0),
          _numIteration(0),_flagb(false),_initialStep(NULL){
      OneMinusAlphaf = 1. - _alphaf;
      AlphafMinusOne = -1.* OneMinusAlphaf;
      OneMinusAlpham = 1. - _alpham;
      MinusAlphaf = -1.*_alphaf;
      AlphamMinusOne = -1.* OneMinusAlpham;
      MinusAlpham = -1.*_alpham;

      _step1 = new implicitStepPetsc<scalar>();
      _step2 = new implicitStepPetsc<scalar>();
      _currentStep = _step1;
      _previousStep = _step2;

    }
    virtual ~implicitHulbertChungPetsc(){
      delete _step1;
      delete _step2;
      if (_initialStep) delete _initialStep;
    }

    implicitStepPetsc<scalar>& getState(const IPStateBase::whichState ws) {
      if (ws == IPStateBase::initial){
        if (_initialStep == NULL){
          _initialStep = new implicitStepPetsc<scalar>();
          _initialStep->allocate(_nbRows,&_currentStep->_x);
        }
        return *_initialStep;
      }
      else if (ws == IPStateBase::previous) return *_previousStep;
      else if (ws == IPStateBase::current) return *_currentStep;
      else
        Msg::Fatal("this state does not exist");
    }

    virtual void copy(const IPStateBase::whichState source,
                        const IPStateBase::whichState destination){
      implicitStepPetsc<scalar>& src = this->getState(source);
      implicitStepPetsc<scalar>& dst = this->getState(destination);
      dst = src;
    }

    void nextStep(){
      if(_whichStep){
        _currentStep = _step2;
        _previousStep = _step1;
        _whichStep =false;
      }
      else{
       _currentStep = _step1;
       _previousStep = _step2;
       _whichStep = true;
      }
      _numIteration = 0;
    }

    virtual void clear(){
      if (this->isAllocated()){
        linearSystemPETSc<scalar>::clear();
        _step1->clear();
        _step2->clear();
        _nbRows=0;

				if (_initialStep) _initialStep->clear();

        _try(MatDestroy(&_M));
      }
    }

    // Or compute directly the time step here ??
    virtual void setTimeStep(const double dt){
      _timeStep = dt;
      // update variables which depends on _timeStep
      Deltat2halfMinusbeta = _timeStep*_timeStep*(0.5- _beta);
      DeltatOneMinusGamma = _timeStep*(1.-_gamma);
      GammaDivByBetaDeltat = _gamma/(_beta*_timeStep);
      OneMinusAlphamDivByBetaDeltat2 = (1. - _alpham)/(_beta*_timeStep*_timeStep);
      OneDivByBetaDeltat2 = 1./(_beta*_timeStep*_timeStep);
    }

    virtual double getTimeStep()const{return _timeStep;}
    virtual void allocate(int nbRows){
      clear();
      linearSystemPETSc<scalar>::allocate(nbRows);
      _step1->allocate(nbRows,&this->_x);
      _step2->allocate(nbRows,&this->_x);
      _nbRows=nbRows;

      _try(MatCreate(PETSC_COMM_WORLD, &_M));
      _try(MatSetSizes(_M, nbRows, nbRows, PETSC_DETERMINE, PETSC_DETERMINE));
      _try(MatSetFromOptions(_M));

      #if (((PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)))
      _try(MatSetUp(this->_M));
      if(Msg::GetCommSize()>1){
        _try(MatSetUp(this->_a));
      }
      #endif // (PETSC_VERSION_MAJOR == 3) && (PETSC_VERSION_MINOR >= 3)
    }

    virtual void addToRightHandSide(int row, const scalar &val)
    {
      PetscInt i = row;
      PetscScalar s = val;
      _try(VecSetValues(_currentStep->_Fext, 1, &i, &s, ADD_VALUES));
      #ifdef _DEBUG
      Msg::Warning("addToRightHandSide is deprecated for explicitHulbertChung system. Please use addToRightHandSidePlus or addToRightHandSideMinus");
      #endif
    }
    virtual void addToRightHandSidePlus(int row, const scalar &val)
    {
      PetscInt i = row;
      PetscScalar s = val;
      _try(VecSetValues(_currentStep->_Fext, 1, &i, &s, ADD_VALUES));
    }
    virtual void addToRightHandSideMinus(int row, const scalar &val)
    {
      PetscInt i = row;
      PetscScalar s = val;
      _try(VecSetValues(_currentStep->_Fint, 1, &i, &s, ADD_VALUES));
    }

    virtual void getFromRightHandSide(int row, scalar &val) const
    {
      #if defined(PETSC_USE_COMPLEX)
      PetscScalar *tmp;
      _try(VecGetArray(_currentStep->_Fext, &tmp));
      PetscScalar s = tmp[row];
      _try(VecRestoreArray(_currentStep->_Fext, &tmp));
      // FIXME specialize this routine
      val = s.real();
      _try(VecGetArray(_currentStep->_Fint, &tmp));
      PetscScalar s = tmp[row];
      _try(VecRestoreArray(_currentStep->_Fint, &tmp));
      val -= s.real();
      #else
      scalar tmp;
      VecGetValues(_currentStep->_Fext, 1, &row, &tmp);
      val = tmp;
      VecGetValues(_currentStep->_Fint, 1, &row, &tmp);
      val -=tmp;
      #endif
    }

    virtual void getFromRightHandSidePlus(int row, scalar &val) const
    {
      #if defined(PETSC_USE_COMPLEX)
      PetscScalar *tmp;
      _try(VecGetArray(_currentStep->_Fext, &tmp));
      PetscScalar s = tmp[row];
      _try(VecRestoreArray(_currentStep->_Fext, &tmp));
      // FIXME specialize this routine
      val = s.real();
      #else
      VecGetValues(_currentStep->_Fext, 1, &row, &val);
      #endif
    }

    virtual void getFromRightHandSideMinus(int row, scalar &val) const
    {
      #if defined(PETSC_USE_COMPLEX)
      PetscScalar *tmp;
      _try(VecGetArray(_currentStep->_Fint, &tmp));
      PetscScalar s = tmp[row];
      _try(VecRestoreArray(_currentStep->_Fint, &tmp));
      // FIXME specialize this routine
      val = s.real();
      #else
      VecGetValues(_currentStep->_Fint, 1, &row, &val);
      #endif
    }

    virtual double normInfRightHandSide() const {
      // compute b
      if(!_flagb){
       Vec tempxdd;
       _try(VecDuplicate(this->_currentStep->_xddot,&tempxdd));
       _try(VecAXPY(tempxdd,AlphamMinusOne,this->_currentStep->_xddot));
       _try(VecAXPY(tempxdd,MinusAlpham,this->_previousStep->_xddot));

       _try(VecAXPY(this->_b,OneMinusAlphaf,this->_currentStep->_Fext));
       _try(VecAXPY(this->_b,AlphafMinusOne,this->_currentStep->_Fint));
       _try(VecAXPY(this->_b,this->_alphaf,this->_previousStep->_Fext));
       _try(VecAXPY(this->_b,MinusAlphaf,this->_previousStep->_Fint));

       _try(MatAssemblyBegin(_M,MAT_FINAL_ASSEMBLY));
       _try(MatAssemblyEnd(_M,MAT_FINAL_ASSEMBLY));

       _try(MatMultAdd(_M,tempxdd,this->_b,this->_b));
        _flagb = true;

        _try(VecDestroy(&tempxdd));

      }
      double norm = linearSystemPETSc<scalar>::normInfRightHandSide();
      return norm;
    };

    virtual void addToMatrix(int row, int col, const scalar &val, const typename explicitHulbertChung<scalar>::whichMatrix wm)
    {
      if(wm == explicitHulbertChung<scalar>::stiff){
        linearSystemPETSc<scalar>::addToMatrix(row,col,val);
      }
      else if( wm == explicitHulbertChung<scalar>::mass){
        PetscInt i = row;
        PetscInt j = col;
        PetscScalar s = val;
        _try(MatSetValues(_M, 1, &i, 1, &j, &s, ADD_VALUES));
      }
      else{
       Msg::Error("stiff and mass are the only possible matrix choice");
      }

    };

     virtual void getFromSolution(int row, scalar &val) const
    {
     #if defined(PETSC_USE_COMPLEX)
      PetscScalar *tmp;
      _try(VecGetArray(_currentStep->_x, &tmp));
      PetscScalar s = tmp[row];
      _try(VecRestoreArray(_currentStep->_x, &tmp));
      val = s.real();
     #else
      VecGetValues(_currentStep->_x, 1, &row, &val);
     #endif // PETSC_USE_COMPLEX
    }

    virtual void getFromPreviousSolution(int row, scalar &val)  const
    {
      #if defined(PETSC_USE_COMPLEX)
      PetscScalar *tmp;
      _try(VecGetArray(_previousStep->_x, &tmp));
      PetscScalar s = tmp[row];
      _try(VecRestoreArray(_previousStep->_x, &tmp));
      val = s.real();
      #else
      VecGetValues(_previousStep->_x, 1, &row, &val);
      #endif // PETSC_USE_COMPLEX
    }

    virtual void getFromSolution(int row, scalar &val, const nonLinearBoundaryCondition::whichCondition wv) const
    {
      switch(wv){
       case nonLinearBoundaryCondition::position:
         this->getFromSolution(row,val);
       break;
       case nonLinearBoundaryCondition::velocity:
        #if defined(PETSC_USE_COMPLEX)
        PetscScalar *tmp;
        _try(VecGetArray(_currentStep->_xdot, &tmp));
        PetscScalar s = tmp[row];
        _try(VecRestoreArray(_currentStep->_xdot, &tmp));
        val = s.real();
        #else
        VecGetValues(_currentStep->_xdot, 1, &row, &val);
        #endif
       break;
       case nonLinearBoundaryCondition::acceleration:
        #if defined(PETSC_USE_COMPLEX)
        PetscScalar *tmp;
        _try(VecGetArray(_currentStep->_xddot, &tmp));
        PetscScalar s = tmp[row];
        _try(VecRestoreArray(_currentStep->_xddot, &tmp));
        val = s.real();
        #else
        VecGetValues(_currentStep->_xddot, 1, &row, &val);
        #endif
       break;
       default:
         Msg::Error("Impossible to get value from solution. Only possible choices position, velocity, acceleration ");
      }

    }

    virtual double norm0Inf() const{
      PetscReal norFext;
      PetscReal norFint;
      _try(VecNorm(this->_currentStep->_Fext, NORM_INFINITY, &norFext));
      _try(VecNorm(this->_currentStep->_Fint, NORM_INFINITY, &norFint));

      PetscReal norFextPrev, norFintPrev;
      _try(VecNorm(this->_previousStep->_Fext, NORM_INFINITY, &norFextPrev));
      _try(VecNorm(this->_previousStep->_Fint, NORM_INFINITY, &norFintPrev));


      Vec tmp1, tmp2;
      _try(VecDuplicate(this->_x,&tmp1));
      _try(VecDuplicate(this->_x,&tmp2));

      #if defined(HAVE_MPI)
      if (Msg::GetCommSize()>1){
        _try(VecAssemblyBegin(_previousStep->_xddot));
        _try(VecAssemblyEnd(_previousStep->_xddot));
        _try(VecAssemblyBegin(_currentStep->_xddot));
        _try(VecAssemblyEnd(_currentStep->_xddot));
      }
      #endif //HAVE_MPI

      _try(VecAXPY(tmp1,_alpham,_previousStep->_xddot));
      _try(VecAXPY(tmp1,OneMinusAlpham,_currentStep->_xddot));

      _try(MatMult(_M,tmp1,tmp2));
      PetscReal norAcc;
      _try(VecNorm(tmp2,NORM_INFINITY,&norAcc));

      _try(VecDestroy(&tmp1));
      _try(VecDestroy(&tmp2));

      double norm = norAcc+fabs(OneMinusAlphaf)*(norFext+norFint)+ fabs(_alphaf)*(norFextPrev+norFintPrev);

      return norm;
    }

    virtual void zeroRightHandSide()
    {
      linearSystemPETSc<scalar>::zeroRightHandSide();
      if (this->isAllocated()) {
        _try(VecAssemblyBegin(_currentStep->_Fext));
        _try(VecAssemblyEnd(_currentStep->_Fext));
        _try(VecZeroEntries(_currentStep->_Fext));
        _try(VecAssemblyBegin(_currentStep->_Fint));
        _try(VecAssemblyEnd(_currentStep->_Fint));
        _try(VecZeroEntries(_currentStep->_Fint));
      }
      _flagb = false;
    }


    virtual int systemSolve(){
      if (_numIteration == 0){
        #if defined(HAVE_MPI)
        if(Msg::GetCommSize() > 1)
        {
          _try(VecAssemblyBegin(this->_previousStep->_x));
          _try(VecAssemblyEnd(this->_previousStep->_x));
          _try(VecAssemblyBegin(this->_previousStep->_xdot));
          _try(VecAssemblyEnd(this->_previousStep->_xdot));
        }
        #endif // HAVE_MPI

        _try(VecCopy(this->_previousStep->_x,this->_currentStep->_x));
        _try(VecAXPY(this->_currentStep->_x,_timeStep,this->_previousStep->_xdot));
        _try(VecAXPY(this->_currentStep->_x,Deltat2halfMinusbeta,this->_previousStep->_xddot));

        _try(VecCopy(this->_previousStep->_xdot,this->_currentStep->_xdot));
        _try(VecAXPY(this->_currentStep->_xdot,DeltatOneMinusGamma,this->_previousStep->_xddot));

        _try(VecAssemblyBegin(_currentStep->_xddot));
        _try(VecAssemblyEnd(_currentStep->_xddot));
        _try(VecZeroEntries(this->_currentStep->_xddot));
      }
      else{
        _try(MatAssemblyBegin(this->_a,MAT_FINAL_ASSEMBLY));
        _try(MatAssemblyEnd(this->_a,MAT_FINAL_ASSEMBLY));
        _try(MatScale(this->_a,OneMinusAlphaf));
        _try(MatAXPY(this->_a,OneMinusAlphamDivByBetaDeltat2,_M,DIFFERENT_NONZERO_PATTERN));
        if(!_flagb){
         Vec tempxdd;
         _try(VecDuplicate(this->_currentStep->_xddot,&tempxdd));
         _try(VecAXPY(tempxdd,AlphamMinusOne,this->_currentStep->_xddot));
         _try(VecAXPY(tempxdd,MinusAlpham,this->_previousStep->_xddot));

         _try(VecAXPY(this->_b,OneMinusAlphaf,this->_currentStep->_Fext));
         _try(VecAXPY(this->_b,AlphafMinusOne,this->_currentStep->_Fint));

         if (_alphaf != 0.){
          _try(VecAXPY(this->_b,this->_alphaf,this->_previousStep->_Fext));
          _try(VecAXPY(this->_b,MinusAlphaf,this->_previousStep->_Fint));
         }


         _try(MatAssemblyBegin(_M,MAT_FINAL_ASSEMBLY));
         _try(MatAssemblyEnd(_M,MAT_FINAL_ASSEMBLY));

         _try(MatMultAdd(_M,tempxdd,this->_b,this->_b));
          _flagb = true;

          _try(VecDestroy(&tempxdd));
        }

        linearSystemPETSc<scalar>::systemSolve();
        _try(VecAXPY(this->_currentStep->_x,1.,this->_x));
        _try(VecAXPY(this->_currentStep->_xddot,OneDivByBetaDeltat2,this->_x));
        _try(VecAXPY(this->_currentStep->_xdot,GammaDivByBetaDeltat,this->_x));
      }
      _numIteration++;
      return 1;
    };
    // Specific functions (To put initial conditions)
    // set on current step a next step operation is necessary after the prescribtion of initial value step0->step1
    virtual void setInitialCondition(int row, scalar val, const typename nonLinearBoundaryCondition::whichCondition wc=nonLinearBoundaryCondition::position){ //CANNOT PASS VAL BY REF WHY ??
      PetscInt i = row;
      switch(wc){
       case nonLinearBoundaryCondition::position:
        _try(VecSetValues(_currentStep->_x, 1, &i, &val, INSERT_VALUES));
        break;
       case nonLinearBoundaryCondition::velocity:
        _try(VecSetValues(_currentStep->_xdot, 1, &i, &val, INSERT_VALUES));
        break;
       case nonLinearBoundaryCondition::acceleration:
        _try(VecSetValues(_currentStep->_xddot, 1 , &i, &val, INSERT_VALUES));
        break;
       default:
        Msg::Warning("Invalid initial conditions");
      }
    }
    virtual void setInitialValues(const int row, scalar val){
      this->setInitialCondition(row,val,nonLinearBoundaryCondition::position);
    }

    virtual void resetUnknownsToPreviousTimeStep()
    {
      (*_currentStep) = (*_previousStep);
      _numIteration = 0;
    }

    virtual void getFromMatrix(int row, int col, scalar &val,const typename nonLinearSystem<scalar>::whichMatrix wm) const
    {
      if(wm!=nonLinearSystem<scalar>::stiff)
      {
        Msg::Error("A QS system has only a stiffness matrix. So cannot get a value in another matrix");
        val=0;
        return;
      }
      this->linearSystemPETSc<scalar>::getFromMatrix(row,col,val);
    }

    virtual double getKineticEnergy(const int syssize) const {
      Vec xpt, Mxpt;
      _try(VecDuplicate(_currentStep->_xdot,&xpt));
      _try(VecDuplicate(_currentStep->_xdot,&Mxpt));

      #if defined(HAVE_MPI)
      if (Msg::GetCommSize()>1){
        _try(VecAssemblyBegin(_currentStep->_xdot));
        _try(VecAssemblyEnd(_currentStep->_xdot));
      }
      #endif //HAVE_MPI

      _try(VecCopy(_currentStep->_xdot,xpt));
      PetscScalar s = 0.;
      for (int i= 0; i< this->_globalSize; i++){
        if (i< this->_localRowStart or i >this->_localRowEnd + syssize - _nbRows){
          _try(VecSetValues(xpt,1,&i,&s,INSERT_VALUES));
        }
      }
      _try(MatMult(_M,xpt,Mxpt));
      #if defined(HAVE_MPI)
      if (Msg::GetCommSize()>1){
        _try(VecAssemblyBegin(Mxpt));
        _try(VecAssemblyEnd(Mxpt));
      }
      #endif //HAVE_MPI
      PetscScalar ener;
      _try(VecDot(Mxpt,xpt,&ener));

      _try(VecDestroy(&xpt));
      _try(VecDestroy(&Mxpt));
      return 0.5*ener;


    };
    virtual double getExternalWork(const int syssize) const {
      double energ = 0;
      for (int i=this->_localRowStart; i<this->_localRowEnd + syssize - _nbRows; i++){
        PetscScalar val, valprev, fext, fextprev;
        _try(VecGetValues(_currentStep->_x,1,&i,&val));
        _try(VecGetValues(_previousStep->_x,1,&i,&valprev));

        _try(VecGetValues(_currentStep->_Fext,1,&i,&fext));
        _try(VecGetValues(_previousStep->_Fext,1,&i,&fextprev));

        double ext = (fext+ fextprev)*(val- valprev);
        energ += ext;
      }
      energ *= 0.5;
      return energ;
    };

    virtual Mat& getMassMatrix(){return _M;};

    #if defined(HAVE_MPI)
    virtual scalar* getArrayIndexPreviousRightHandSidePlusMPI(const int index){return NULL;};
    #endif // HAVE_MPI
    virtual void setDynamicRelaxation(const bool dynrel) {};

};

#else
class implicitHulbertChungPetsc : public nonLinearSystem<scalar>,
                                  public linearSystemPETSc<scalar>{
  public:
    implicitHulbertChungPetsc(){Msg::Fatal("Petsc is not available");};
    virtual ~implicitHulbertChungPetsc(){}
    virtual void setInitialValues(const int i, scalar val){};
    virtual void getFromPreviousSolution(int row, scalar &val) const{};
    virtual void getFromRightHandSidePlus(int row, scalar &val) const {};
    virtual void getFromRightHandSideMinus(int row, scalar &val) const {};
    virtual void getFromRightHandSide(int row, scalar &val) const{};
    virtual void addToRightHandSidePlus(int row, const scalar &val) {};
    virtual void addToRightHandSideMinus(int row, const scalar &val) {};
    virtual double norm0Inf() const {return 0.;};
    virtual void resetUnknownsToPreviousTimeStep() {};
    virtual void nextStep() {};
    virtual void setTimeStep(const double dt) {};
    virtual double getTimeStep() const {return 0.;};
    virtual double getExternalWork(const int syssize) const {return 0.;};
    virtual double getKineticEnergy(const int syssize) const {return 0.;};
    virtual void addToMatrix(int row, int col, const scalar &val, const typename nonLinearSystem<scalar>::whichMatrix wm){};
    virtual void setInitialCondition(int row, scalar val, const typename nonLinearBoundaryCondition::whichCondition wc=nonLinearBoundaryCondition::position) {};
    virtual void setDynamicRelaxation(const bool dynrel) {};
    virtual void getFromSolution(int row, scalar &val, const nonLinearBoundaryCondition::whichCondition wv) const {};
    virtual void getFromMatrix(int row, int col, scalar &val,const typename nonLinearSystem<scalar>::whichMatrix wm) const {};
    virtual Mat& getMassMatrix(){return _M};
    #if defined(HAVE_MPI)
    virtual double* getArrayIndexPreviousRightHandSidePlusMPI(const int index) {return NULL;};
    #endif // HAVE_MPI
};
#endif //HAVE_PETSC

#endif // IMPLICITEHULBERTCHUNGPETSC_H_
