
#include "getTotalVolume.h"
#include "GModel.h"
#include "quadratureRules.h"


getTotalVolume::getTotalVolume(): _g(NULL){}
getTotalVolume::~getTotalVolume(){
  if (_g != NULL) delete _g;
  _g = NULL;
};

void getTotalVolume::loadModel(const std::string filename){
  GModel* pModel = new GModel();
  pModel->readMSH(filename.c_str());
  int dim = pModel->getNumRegions() ? 3 : 2;
  std::map<int, std::vector<GEntity*> > groups[4];
  pModel->getPhysicalGroups(groups);
  std::map<int, std::vector<GEntity*> > &entmap = groups[dim];

  _g = new groupOfElements();
  for (std::map<int, std::vector<GEntity*> >::iterator it = entmap.begin(); it!= entmap.end(); it++){
    std::vector<GEntity*> &ent = it->second;
    for (unsigned int i = 0; i < ent.size(); i++){
      for (unsigned int j = 0; j < ent[i]->getNumMeshElements(); j++){
        MElement *e = ent[i]->getMeshElement(j);
        _g->insert(e);
      }
    }
  }
  Msg::Error("num element = %d",_g->size());
};

double getTotalVolume::totalVolume(){
  double volume = 0.;
  IntPt* GP = NULL;
  double jac[3][3];
  GaussQuadrature Integ_Bulk(GaussQuadrature::GradGrad);
  for (groupOfElements::elementContainer::iterator it = _g->begin(); it!= _g->end(); it++){
    MElement* e = *it;
    int npts = Integ_Bulk.getIntPoints(e,&GP);
    for (int i=0; i<npts; i++){
      const double u = GP[i].pt[0];
      const double v = GP[i].pt[1];
      const double w = GP[i].pt[2];
      const double weight = GP[i].weight;
      const double detJ = e->getJacobian(u, v, w, jac);
      double ratio = detJ*weight;
      volume+=ratio;
    }
  }
  Msg::Info("total volume = %e",volume);
  return volume;
};
