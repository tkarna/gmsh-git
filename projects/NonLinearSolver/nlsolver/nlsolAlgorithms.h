//
// C++ Interface: terms
//
// Description: non linear assembly fonctions
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef NONLINEARSOLVERALGORITHMS_H_
#define NONLINEARSOLVERALGORITHMS_H_
#include "dofManager.h"
#include "quadratureRules.h"
#include "MVertex.h"
#include "MInterfaceElement.h"
#include "functionSpace.h"
#include "explicitHulbertChung.h"
#include "solverAlgorithms.h"
#include "staticDofManager.h"
#include "dofManagerMultiSystems.h"

// stiffess matrix for multi-system
template<class Iterator, class T> void AssembleInterMatrixMulti(BilinearTermBase &term, FunctionSpaceBase &space,
                                                                        Iterator itbegin, Iterator itend,
                                                                        QuadratureBase &integrator, dofManagerMultiSystems<T> &assembler)
  // symmetric
{
  fullMatrix<typename dofManagerMultiSystems<T>::dataMat> localMatrix;
  std::vector<Dof> R;
  for (Iterator it = itbegin; it != itend; ++it){
    MElement *e = *it;
    MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(e);
    R.clear();
    IntPt *GP;
    int npts = integrator.getIntPoints(e, &GP);
    term.get(e, npts, GP, localMatrix); //localMatrix.print();
    space.getKeys(e, R);
    assembler.assemble(iele,R,localMatrix);
  }
}

// Assemble nonlinearterm. The field of term can be pass (compute matrix by perturbation and avoid a additional getKeys)
// Additional argument is pass to give if rhsPlus (Fext) or rhsMinus (Fint) is used in system

template<class Iterator, class Assembler> void Assemble(LinearTermBase<double> *term, FunctionSpaceBase &space,
                                                        Iterator itbegin, Iterator itend,
                                                        QuadratureBase &integrator,
                                                        const unknownField *ufield,Assembler &assembler,
                                                        const nonLinearSystem<double>::rhs wrhs)
{
  fullVector<typename Assembler::dataMat> localVector(0);
  std::vector<Dof> R;
  nonLinearTermBase<double> *nlterm = dynamic_cast<nonLinearTermBase<double>*>(term);
  if( (nlterm !=NULL) and (nlterm->isData())){
    fullVector<double> disp;
    nlterm->set(&disp);
    for (Iterator it = itbegin; it != itend; ++it){
      MElement *e = *it;
      R.clear();
      IntPt *GP;
      int npts = integrator.getIntPoints(e, &GP);
      space.getKeys(e, R);
      for(int i=0;i<R.size();i++)
      disp.resize(R.size());
      ufield->get(R,disp);
      term->get(e, npts, GP, localVector); //localVector.print();
      assembler.assemble(R, localVector,wrhs);
    }
  }
  else{
    for (Iterator it = itbegin; it != itend; ++it){
      MElement *e = *it;
      R.clear();
      IntPt *GP;
      int npts = integrator.getIntPoints(e, &GP);
      space.getKeys(e, R);
      term->get(e, npts, GP, localVector); //localVector.print();
      assembler.assemble(R, localVector,wrhs);
    }

  }
}

// Interface of multi systems
template<class Iterator, class T> void AssembleInterVectorMulti(LinearTermBase<double> *term, FunctionSpaceBase &space,
                                                        Iterator itbegin, Iterator itend,
                                                        QuadratureBase &integrator,
                                                        const unknownField *ufield,dofManagerMultiSystems<T> &assembler)
{
  fullVector<typename dofManagerMultiSystems<T>::dataMat> localVector(0);
  std::vector<Dof> R;
  nonLinearTermBase<double> *nlterm = dynamic_cast<nonLinearTermBase<double>*>(term);
  if( (nlterm !=NULL) and (nlterm->isData())){
    fullVector<double> disp;
    nlterm->set(&disp);
    for (Iterator it = itbegin; it != itend; ++it){
      MElement *e = *it;
      MInterfaceElement* iele = dynamic_cast<MInterfaceElement*>(e);
      R.clear();
      IntPt *GP;
      int npts = integrator.getIntPoints(e, &GP);
      space.getKeys(e, R);
      for(int i=0;i<R.size();i++)
      disp.resize(R.size());
      ufield->get(R,disp);
      term->get(e, npts, GP, localVector); //localVector.print();
      assembler.assemble(iele,R, localVector);
    }
  }
  else{
    for (Iterator it = itbegin; it != itend; ++it){
      MElement *e = *it;
      MInterfaceElement *iele = dynamic_cast<MInterfaceElement*>(e);
      R.clear();
      IntPt *GP;
      int npts = integrator.getIntPoints(e, &GP);
      space.getKeys(e, R);
      term->get(e, npts, GP, localVector); //localVector.print();
      assembler.assemble(iele,R, localVector);
    }

  }
}


// for rigid contact
template<class Iterator, class Assembler> void Assemble(LinearTermBase<double> &term, FunctionSpaceBase &space,
                                                        Iterator itbegin, Iterator itend,
                                                        QuadratureBase &integrator, Assembler &assembler,
                                                        const nonLinearSystem<double>::rhs wrhs)
{
  fullVector<typename Assembler::dataMat> localVector;
  std::vector<Dof> R;
  for (Iterator it = itbegin; it != itend; ++it){
    MElement *e = *it;
    R.clear();
    IntPt *GP;
    int npts = integrator.getIntPoints(e, &GP);
    term.get(e, npts, GP, localVector); //localVector.print();
    space.getKeys(e, R);
    assembler.assemble(R, localVector,wrhs);
  }
}

// Function Assemble for mass matrix. An other function is needed because Two matrix in the system
template<class Iterator,class Assembler> void AssembleMass(BilinearTermBase *term,FunctionSpaceBase &space,Iterator itbegin,
                                                           Iterator itend,QuadratureBase &integrator,Assembler &assembler) // symmetric
{
  fullMatrix<typename Assembler::dataMat> localMatrix;
  // remove the Dynamic Cast How
  staticDofManager<typename Assembler::dataVec>* expAss = static_cast<staticDofManager<typename Assembler::dataVec>*>(&assembler);
  std::vector<Dof> R;
  for (Iterator it = itbegin;it!=itend; ++it)
  {
    MElement *e = *it;
    R.clear();
    IntPt *GP;
    int npts=integrator.getIntPoints(e,&GP);
    term->get(e,npts,GP,localMatrix);
    space.getKeys(e,R);
    expAss->assemble(R, localMatrix,explicitHulbertChung<double>::mass);
  }
}

// Assemble mass for a rigid contact space. Only ddl of GC
template<class Assembler>
void AssembleMass(BilinearTermBase *term, rigidContactSpaceBase *space,Assembler *assembler){
  fullMatrix<typename Assembler::dataMat> localMatrix;
  staticDofManager<typename Assembler::dataVec>* expAss = static_cast<staticDofManager<typename Assembler::dataVec>*>(assembler);
  std::vector<Dof> R;
  space->getKeysOfGravityCenter(R);
  MElement *ele;
  IntPt *GP;
  term->get(ele,0,GP,localMatrix);
  expAss->assemble(R, localMatrix,explicitHulbertChung<double>::mass);
}

template<class Assembler> void FixNodalDofs(FunctionSpaceBase *space,MElement *e,Assembler &assembler,simpleFunction<typename Assembler::dataVec> &fct,FilterDof &filter,bool fullDg)
{
  std::vector<MVertex*> tabV;
  int nv=e->getNumVertices();
  std::vector<Dof> R;
  space->getKeys(e,R);
  tabV.reserve(nv);
  for (int i=0;i<nv;++i) tabV.push_back(e->getVertex(i));

  if(!fullDg){
    for (std::vector<Dof>::iterator itd=R.begin();itd!=R.end();++itd)
    {
      Dof key=*itd;
      if (filter(key))
      {
        for (int i=0;i<nv;++i)
        {
          if (tabV[i]->getNum()==key.getEntity())
          {
            //Msg::Info("Fix dof number %d comp %d",key.getEntity(),key.getType());
            assembler.fixDof(key, fct(tabV[i]->x(),tabV[i]->y(),tabV[i]->z()));
            break;
          }
        }
      }
    }
  }
  else{
    for (std::vector<Dof>::iterator itd=R.begin();itd!=R.end();++itd)
    {
      Dof key=*itd;
      if (filter(key))
      {
        for (int i=0;i<nv;++i)
        {
          //Msg::Info("Fix dof number %d comp %d on rank %d",key.getEntity(),key.getType(),Msg::GetCommRank());
          assembler.fixDof(key, fct(tabV[i]->x(),tabV[i]->y(),tabV[i]->z()));
          break;
        }
      }
    }
  }
}

template<class Iterator,class Assembler> void FixNodalDofs(FunctionSpaceBase *space,Iterator itbegin,Iterator itend,Assembler &assembler,
                                                           simpleFunction<typename Assembler::dataVec> &fct,FilterDof &filter,bool fullDg)
{
  for (Iterator it=itbegin;it!=itend;++it)
  {
    FixNodalDofs(space,*it,assembler,fct,filter,fullDg);
  }
}

template<class Assembler>
void FixNodalDofs(rigidContactSpaceBase *space,simpleFunction<typename Assembler::dataVec> &fct,FilterDof &filter, Assembler &assembler){
  std::vector<Dof> R;
  space->getKeysOfGravityCenter(R);
  for(int i=0;i<R.size();i++){
    if(filter(R[i]))
      assembler.fixDof(R[i], fct(0.,0.,0.));
  }

}

template<class Assembler> void SetInitialDofs(FunctionSpaceBase *space,MElement *e,const nonLinearBoundaryCondition::whichCondition whichC,
                                              Assembler &assembler,const simpleFunctionTime<double> *fct,
                                              FilterDof &filter,bool fullDg)
{
  int nv=e->getNumVertices();
  std::vector<Dof> R;
  space->getKeys(e,R);
  int ncomp = space->getNumKeys(e)/nv;
  staticDofManager<double> *dynass= static_cast<staticDofManager<double>*>(&assembler);
  for(int i=0; i<nv;i++)
  {
    // get the value of velocity
    double myvalue = fct->operator()(e->getVertex(i)->x(),e->getVertex(i)->y(),e->getVertex(i)->z());
    // set the value
    for(int j=0;j<ncomp;j++)
    {
      if(filter(R[i+j*nv]))
      {
        dynass->setInitialCondition(R[i+j*nv],myvalue,whichC);
        break;
      }
    }
  }
}

template<class Iterator,class Assembler> void SetInitialDofs(FunctionSpaceBase *space,Iterator itbegin,Iterator itend,
                                                           const nonLinearBoundaryCondition::whichCondition whichC,
                                                            Assembler &assembler, const simpleFunctionTime<double> *fct,
                                                            FilterDof &filter,bool fullDg)
{
  for (Iterator it=itbegin;it!=itend;++it)
  {
    SetInitialDofs(space,*it,whichC,assembler,fct,filter,fullDg);
  }
}


/* Consider the undeformed configuration for now !!!!*/
/* problem no comp function ??? --> can't filter the dof */
template<class Assembler> void SetNormalInitialDofs(FunctionSpaceBase *space,MElement *e,Assembler &assembler,
                                            const simpleFunction<double> *fct,const nonLinearBoundaryCondition::whichCondition whichC,
                                            FilterDof &filter,bool fullDg,groupOfElements *groupDom)
{
  int nv=e->getNumVertices();
  std::vector<Dof> R;
  space->getKeys(e,R);
  staticDofManager<double> *dynass= static_cast<staticDofManager<double>*>(&assembler);
  // compute the value of the condition for each node in each direction
  double u,v,w; // local node position
  std::vector<TensorialTraits<double>::GradType> grads;
  FunctionSpace<double>* spd = static_cast<FunctionSpace<double>*>(space);
//  std::vector<double> vertexvalue(R.size);
  std::vector<int> nodeIndex;
  MElement *ele;
  if(groupDom == NULL){
    for(int i=0;i<nv;i++)
      nodeIndex.push_back(i);
    ele = e;
  }
  else{
    // find the element which e is the boundary
    MVertex *ver1 = e->getVertex(0);
    MVertex *ver2 = e->getVertex(1);
    for(groupOfElements::elementContainer::iterator it = groupDom->begin(); it!=groupDom->end();++it)
    {
      MElement* eletmp = *it;
      int numpver = eletmp->getNumPrimaryVertices();
      bool find1 = false, find2 = false;
      for(int i=0;i<numpver; i++)
      {
         if(eletmp->getVertex(i)->getNum() == ver1->getNum()) find1 = true;
         else if (eletmp->getVertex(i)->getNum() == ver2->getNum()) find2 = true;
      }
      if(find1 and find2)
      {
        ele = eletmp;
        int elenum = ele->getNum();
        for(int i=0;i<e->getNumVertices();i++)
        {
          for(int j=0;j<eletmp->getNumVertices();j++)
          {
            if(e->getVertex(i)->getNum() == eletmp->getVertex(j)->getNum()){
              nodeIndex.push_back(j);
              break;
            }
          }
        }
        break;
      }
    }
  }
  for(int i=0;i<nv;i++)
  {
     /* Value of normal */
     //get u,v of element
     ele->getNode(nodeIndex[i],u,v,w);
     // compute first derivative at this point
     grads.clear();
     spd->gradfuvw(ele,u,v,w,grads); // How to avoid this ??
     // tangent vector
     SVector3 t1(0.,0.,0.);
     SVector3 t2(0.,0.,0.);
     for(int j=0;j<ele->getNumVertices();j++){
       t1 += SVector3(grads[j](0)*ele->getVertex(j)->x(),grads[j](0)*ele->getVertex(j)->y(),grads[j](0)*ele->getVertex(j)->z());
       t2 += SVector3(grads[j](1)*ele->getVertex(j)->x(),grads[j](1)*ele->getVertex(j)->y(),grads[j](1)*ele->getVertex(j)->z());
     }
     SVector3 nor = crossprod(t1,t2);
     nor.normalize();
     double value = fct->operator()(ele->getVertex(nodeIndex[i])->x(),ele->getVertex(nodeIndex[i])->y(),ele->getVertex(nodeIndex[i])->z());
     /* Value of condition for this point */
     for(int j=0;j<3;j++) // impossible to make a filter on component for now
     {
       //vertexvalue[i+j*nv] = nor(j)*value;
       dynass->setInitialCondition(R[i+j*nv],value*nor(j),whichC);
     }
  }
/* // uncomment when get comp will be activated
  if(!fullDg){
    for (std::vector<Dof>::iterator itd=R.begin();itd!=R.end();++itd)
    {
      Dof key=*itd;
      if (filter(key))
      {
        for (int i=0;i<nv;++i)
        {
          if (e->getVertex(i)->getNum()==key.getEntity())
          {
            //printf("Fix dof number %d comp %d\n",key.getEntity(),key.getType());
            dynass->setInitialCondition(key,vertexvalue[i],whichC);
            break;
          }
        }
      }
    }
  }
  else{
    for (std::vector<Dof>::iterator itd=R.begin();itd!=R.end();++itd)
    {
      Dof key=*itd;
      if (filter(key))
      {
        for (int i=0;i<nv;++i)
        {
          dynass->setInitialCondition(key, value,whichC);
          break;
        }
      }
    }
  }
  */
}


template<class Iterator,class Assembler> void SetNormalInitialDofs(FunctionSpaceBase *space,Iterator itbegin,Iterator itend,
                                                            const nonLinearBoundaryCondition::whichCondition whichC,Assembler &assembler,
                                                            const simpleFunctionTime<double> *fct, FilterDof &filter,bool fullDg,
                                                            groupOfElements *groupDom=NULL)
{
  for (Iterator it=itbegin;it!=itend;++it)
  {
    SetNormalInitialDofs(space,*it,assembler,fct,whichC,filter,fullDg,groupDom);
  }
}

// Number the Dof by a vector (allow to hash it by a dofManagerMultiSystem)
template<class Iterator, class Assembler> void NumberDofsByVector(FunctionSpaceBase &space, Iterator itbegin,
                                                          Iterator itend, Assembler &assembler)
{
 for (Iterator it = itbegin; it != itend; ++it){
    MElement *e = *it;
    std::vector<Dof> R;
    space.getKeys(e, R);
    assembler.numberDof(R);
  }
}

// Function Numbering Dof for rigid contact (Create Three Dofs for GC of rigid bodies)
template<class Assembler>
void NumberDofs(rigidContactSpaceBase &space, Assembler &assembler){
  // get Dofs of GC
  std::vector<Dof> R;
  space.getKeysOfGravityCenter(R);
  // Put them into DofManager
  int nbdofs=R.size();
  for (int i=0;i<nbdofs;++i)
    assembler.numberDof(R[i]);
}
#endif //NONLINEARSOLVERALGORITHMS_H_

