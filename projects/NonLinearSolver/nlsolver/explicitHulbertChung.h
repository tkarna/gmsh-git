//
// C++ explicit Newmark scheme
//
// Description: implementation of the explicit algorithms of alpha-generalized method
//              This system is derived from linearSystem from compatibility reason (use of Assemble function and DofManager
//              even if the system is not a no linear system FIX THIS ??
//              for now works only this PETsc
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef EXPLICITHULBERTCHUNGSYSTEM_H_
#define EXPLICITHULBERTCHUNGSYSTEM_H_

#include "GmshConfig.h"
#include "GmshMessage.h"
#include "linearSystem.h"
#include "partDomain.h"
#include "nonLinearSystems.h"

// virtual class (several implementation of system BLAS and petsc)
template <class scalar>
class explicitHulbertChung : public linearSystem<scalar>, public nonLinearSystem<scalar>{
 protected:
  virtual double dynamicRelaxationFactor()=0; // internal function normally called by systemSolve()
  virtual int systemSize()const=0; // has to return the number of the system (use internally by to create a restart
 public :
  enum state{current, next};
  virtual bool isAllocated() const=0;
  virtual void allocate(int nbRows)=0;
  virtual void clear()=0;
  virtual void addToMatrix(int row, int col, const scalar &val)=0;
  virtual void addToRightHandSide(int row, const scalar &val)=0;
  virtual void addToRightHandSidePlus(int row, const scalar &val)=0;
  virtual void addToRightHandSideMinus(int row, const scalar &val)=0;
  virtual void getFromPreviousSolution(int row, scalar &val) const=0;
  virtual void getFromRightHandSide(int row, scalar &val) const=0;
  virtual void getFromRightHandSidePlus(int row, scalar &val) const=0;
  virtual void getFromRightHandSideMinus(int row, scalar &val) const=0;
  virtual void zeroMatrix()=0;
  virtual void zeroRightHandSide()=0;
  virtual int systemSolve()=0;
  virtual double normInfRightHandSide() const=0;
  virtual void getFromMatrix(int row, int col, scalar &val) const=0;
  virtual double getExternalWork(const int syssize)const=0;
  virtual void getFromSolution(int row, scalar &val) const=0;
  virtual void nextStep()=0;
  virtual void setTimeStep(const double dt)=0;
  virtual double getTimeStep()const=0;
  virtual void getFromSolution(int row, scalar &val, const nonLinearBoundaryCondition::whichCondition wv) const=0;
  virtual void getFromMatrix(int row, int col, scalar &val,const typename nonLinearSystem<scalar>::whichMatrix wm) const
  {
   #ifdef _DEBUG
    if(wm!=nonLinearSystem<scalar>::mass)
    {
      Msg::Error("A QS system has only a mass matrix. So cannot get a value in another matrix");
      val=0;
      return;
    }
   #endif // _DEBUG
    this->getFromMatrix(row,col,val);
  }
  virtual void addToSolution(int _row, const scalar &val) {};
  virtual void zeroSolution() {};

  // Contact communication in mpi
  #if defined(HAVE_MPI)
  virtual double* getArrayIndexPreviousRightHandSidePlusMPI(const int index)=0;
//  virtual void setPreviousRightHandSidePlusMPI(const int row, const scalar &val)=0;
  #endif // HAVE_MPI
  // restart
  virtual void createRestart()
  {
    // file name
    std::string fname;
   #if defined(HAVE_MPI)
    if(Msg::GetCommSize()>1)
    {
      std::ostringstream oss;
      oss << Msg::GetCommRank();
      std::string s = oss.str();
      fname = "restartExplicitSystemPart"+s;
    }
    else
   #endif //HAVE_MPI
    {
      fname = "restartExplicitSystem";
    }
    FILE *fp = fopen(fname.c_str(),"w"); // only one restart (last restart is removed)

    // get system size
    int nbrows = this->systemSize();

    // put first the values of previous step
    this->nextStep();
    double m,fi,fe,b,x,xd,xdd;
    for(int i=0;i<nbrows;i++)
    {
      // get values from system
      this->getFromMatrix(i,i,m);
      this->getFromRightHandSide(i,b);
      this->getFromRightHandSidePlus(i,fe);
      this->getFromRightHandSideMinus(i,fi);
      this->getFromSolution(i,x,nonLinearBoundaryCondition::position);
      this->getFromSolution(i,xd,nonLinearBoundaryCondition::velocity);
      this->getFromSolution(i,xdd,nonLinearBoundaryCondition::acceleration);
      // put value in files
      fprintf(fp,"%e %e %e %e %e %e %e ",m,fi,fe,b,x,xd,xdd);
    }

    // put the values of current step
    this->nextStep();
    for(int i=0;i<nbrows;i++)
    {
      // get values from system
      this->getFromMatrix(i,i,m);
      this->getFromRightHandSide(i,b);
      this->getFromRightHandSidePlus(i,fe);
      this->getFromRightHandSideMinus(i,fi);
      this->getFromSolution(i,x,nonLinearBoundaryCondition::position);
      this->getFromSolution(i,xd,nonLinearBoundaryCondition::velocity);
      this->getFromSolution(i,xdd,nonLinearBoundaryCondition::acceleration);
      // put value in files
      fprintf(fp,"%e %e %e %e %e %e %e ",m,fi,fe,b,x,xd,xdd);
    }
    fclose(fp);
  }
  virtual void setFromRestart(){ // system has to be allocated before this function be called
    // file name
    std::string fname;
   #if defined(HAVE_MPI)
    if(Msg::GetCommSize()>1)
    {
      std::ostringstream oss;
      oss << Msg::GetCommRank();
      std::string s = oss.str();
      fname = "restartExplicitSystemPart"+s;
    }
    else
   #endif //HAVE_MPI
    {
      fname = "restartExplicitSystem";
    }
    FILE *fp = fopen(fname.c_str(),"r");

    // get system size
    int nbrows = this->systemSize();

    // set first the values of previous step
    this->nextStep();
    this->zeroMatrix();
    this->zeroRightHandSide();
    double m,fi,fe,b,x,xd,xdd;
    for(int i=0;i<nbrows;i++)
    {
      // get values in from file
      fscanf(fp,"%e %e %e %e %e %e %e",&m,&fi,&fe,&b,&x,&xd,&xdd);
      // set values from system
      this->addToMatrix(i,i,m);
      this->addToRightHandSide(i,b);
      this->addToRightHandSidePlus(i,fe);
      this->addToRightHandSideMinus(i,fi);
      this->setInitialCondition(i,x,nonLinearBoundaryCondition::position);
      this->setInitialCondition(i,xd,nonLinearBoundaryCondition::velocity);
      this->setInitialCondition(i,xdd,nonLinearBoundaryCondition::acceleration);
    }

    // set the values of current step
    this->nextStep();
    this->zeroMatrix();
    this->zeroRightHandSide();
    for(int i=0;i<nbrows;i++)
    {
      // get values from files
      fscanf(fp,"%e %e %e %e %e %e %e",&m,&fi,&fe,&b,&x,&xd,&xdd);
      // set values from system
      this->addToMatrix(i,i,m);
      this->addToRightHandSide(i,b);
      this->addToRightHandSidePlus(i,fe);
      this->addToRightHandSideMinus(i,fi);
      this->setInitialCondition(i,x,nonLinearBoundaryCondition::position);
      this->setInitialCondition(i,xd,nonLinearBoundaryCondition::velocity);
      this->setInitialCondition(i,xdd,nonLinearBoundaryCondition::acceleration);
    }
    fclose(fp);
  }
};

#endif // EXPLICITHULBERTCHUNGSYSTEM_H_

