//
// Description: Derivate class of SimpleFunction to include a time dependency
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
// As this function is templated, it has to be in a separated files of others timeFunction (SWIG generation problem)
#ifndef _SIMPLEFUNCTIONTIME_H_
#define _SIMPLEFUNCTIONTIME_H_
#include "simpleFunction.h"
template<class scalar>
class simpleFunctionTime : public simpleFunction<scalar>{
 protected :
  double time;
  bool timeDependency;
 public :
  simpleFunctionTime(scalar val=0, bool td=true, double t=1.) : simpleFunction<scalar>(val), time(t), timeDependency(td){}; // time=1 by default to avoid set time for Static linear Scheme
  virtual ~simpleFunctionTime(){};
 #ifndef SWIG
  virtual scalar operator () (double x, double y, double z) const { if(timeDependency) return time*this->_val; else return this->_val;}
  virtual void setTime(const double t){time=t;}
  void scaleVal(const double fac){this->_val*=fac;}
 #endif
};
#endif // _SIMPLEFUNCTIONTIME_H_

