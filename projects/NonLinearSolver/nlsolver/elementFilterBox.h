//
//
// Description: Filter element in a box defined by 2 points (x0,y0,z0) and (x1,y1,z1)
//
//
// Author:  <Gauthier BECKER>, (C) 2011
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef _ELEMENTFILTERBOX_H_
#define _ELEMENTFILTERBOX_H_
#include "groupOfElements.h"
#include "MElement.h"
class elementFilterBox : public elementFilter{
 protected:
  const SPoint3 _pt0;
  const SPoint3 _pt1;
  double xmin,xmax,ymin,ymax,zmin,zmax;
 public :
  elementFilterBox(const double x0,const double y0,const double z0,const double x1,const double y1,const double z1);
  ~elementFilterBox(){}
  // true if barycenter is in the box
  virtual bool operator() (MElement *ele) const;
};

#endif // _ELEMENTFILTERBOX_H_
