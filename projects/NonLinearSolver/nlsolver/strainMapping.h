#ifndef STRAINMAPPING_H_
#define STRAINMAPPING_H_

#include "partDomain.h"
#include "unknownField.h"
#include "GModel.h"
#include <map>
#include <vector>
#include <set>

class strainMapping{
  protected:
    std::vector<partDomain*>& _allDomain;
    GModel *_pModel;
    groupOfElements* _g;
    std::map<MVertex*, std::set<MElement*> > _nodeElementMap;

    std::map<MElement*, SVector3> _elementValue;
    std::map<MVertex*, SVector3> _nodeValue;

    std::map<MElement*,std::map<partDomain*,groupOfElements*> > _meanMap;


  public:
    strainMapping(std::vector<partDomain*>& dom);
    ~strainMapping();

    void readMesh(const std::string meshFileName);
    void getMeanValueOnElement(dofManager<double>* p, MElement* e, SVector3& val);
    void solve(dofManager<double>* p);
    void buildDisplacementView(const std::string postFileName);
    void buildStrainView(const std::string postFileName);
};

#endif // STRAINMAPPING_H_
