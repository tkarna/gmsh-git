
#include "strainMapping.h"
#include "MPoint.h"

strainMapping::strainMapping(std::vector<partDomain*>& dom):_allDomain(dom), _pModel(NULL){}
strainMapping::~strainMapping(){
  _nodeElementMap.clear();
  _elementValue.clear();
  _nodeValue.clear();
  delete _g;
  delete _pModel;
}

void strainMapping::readMesh(const std::string meshFileName){
  _pModel = new GModel();
  _pModel->readMSH(meshFileName.c_str());
  int dim = _pModel->getNumRegions() ? 3 : 2;
  _g = new groupOfElements();

  std::map<int, std::vector<GEntity*> > groups[4];
  _pModel->getPhysicalGroups(groups);
  std::map<int, std::vector<GEntity*> > &entmap = groups[dim];
  for (std::map<int, std::vector<GEntity*> >::iterator it = entmap.begin(); it!= entmap.end(); it++){
    std::vector<GEntity*> &ent = it->second;
    for (unsigned int i = 0; i < ent.size(); i++){
      for (unsigned int j = 0; j < ent[i]->getNumMeshElements(); j++){
        MElement *e = ent[i]->getMeshElement(j);
        _g->insert(e);
      }
    }
  }


  FILE* file = fopen("elementCenter.txt","w");

  for (groupOfElements::elementContainer::iterator it = _g->begin(); it!= _g->end(); it++){
    MElement* e = *it;
    SPoint3 pt = e->barycenter();
    fprintf(file,"%d ;%e;%e;%e\n",e->getNum(),pt.x(),pt.y(),pt.z());
    std::vector<MVertex*> vv;
    e->getVertices(vv);
    for (int i=0; i<vv.size(); i++){
      MVertex* ver = vv[i];
      if (_nodeElementMap.find(ver) == _nodeElementMap.end()){
        std::set<MElement*> eleset;
        eleset.insert(e);
        _nodeElementMap[ver] = eleset;
      }
      else{
         std::set<MElement*>& eleset = _nodeElementMap[ver];
         eleset.insert(e);
      }
    }
  }
  fclose(file);

  for (groupOfElements::elementContainer::iterator itg = _g->begin(); itg!= _g->end(); itg++){
    MElement* e = *itg;
    std::map<partDomain*,groupOfElements*> insideEle;
    for (int i=0; i<_allDomain.size(); i++){
      partDomain* dom = _allDomain[i];
      groupOfElements* gr = new groupOfElements();
      for (groupOfElements::elementContainer::iterator it = dom->g->begin(); it!= dom->g->end(); it++){
        MElement* ele = *it;
        SPoint3 pt = ele->barycenter();
        double uvw[3];
        double xyz[3]; xyz[0]=pt.x(); xyz[1]=pt.y(); xyz[2]=pt.z();
        e->xyz2uvw(xyz,uvw);
        if (e->isInside(uvw[0],uvw[1],uvw[2])){
          gr->insert(ele);
        }
      }
      insideEle[dom] = gr;
    }
    _meanMap[e] = insideEle;
  }
};
void strainMapping::getMeanValueOnElement(dofManager<double>* p, MElement* e, SVector3& val){
  val *= 0.;
  int counter = 0;
  std::map<partDomain*,groupOfElements*>& domainele =  _meanMap[e];
  for (std::map<partDomain*,groupOfElements*>::iterator it = domainele.begin(); it!= domainele.end(); it++){
    partDomain* dom = it->first;
    groupOfElements* g = it->second;
    FunctionSpaceBase* space = dom->getFunctionSpace();
    if (dom->getFormulation() == false){
      for (groupOfElements::vertexContainer::iterator itv = g->vbegin(); itv!= g->vend(); itv++){
        MVertex* v = *itv;
        counter++;
        MPoint mpt(v);
        std::vector<Dof> keys;
        space->getKeys(&mpt,keys);
        std::vector<double> verval;
        p->getDofValue(keys,verval);
        for (int j=0; j<keys.size(); j++){
          val[j]+= verval[j];
        }
      }
    }
    else{
      Msg::Fatal("this is not implemented for DG domain");
    }
  }

  if (counter>0){
    double inv = 1./((double)counter);
    val*= inv;
  }
}
void strainMapping::solve(dofManager<double>* p){
  printf("Begin creating mapping \n");
  // create displacement mean by element
  for (groupOfElements::elementContainer::iterator it = _g->begin(); it!= _g->end(); it++){
    MElement* e = *it;
    SVector3 val(0.);
    this->getMeanValueOnElement(p,e,val);
    _elementValue[e] = val;
  }


  // change to mean by node
  for (std::map<MVertex*,std::set<MElement*> >::iterator it = _nodeElementMap.begin(); it!= _nodeElementMap.end(); it++){
    MVertex* v = it->first;
    std::set<MElement*>& ele = it->second;
    SVector3 val(0.);
    for (std::set<MElement*>::iterator itele = ele.begin(); itele!= ele.end(); itele++){
      val += _elementValue[*itele];
    }
    double inv = 1./ (double) ele.size();
    val *= inv;
    _nodeValue[v] = val;
  }
  printf("End creating mapping \n");
}
void strainMapping::buildDisplacementView(const std::string postFileName){
  std::cout <<  "build Displacement View"<< std::endl;
  std::map<int, std::vector<double> > data;
  for (std::map<MVertex*,SVector3>::iterator it = _nodeValue.begin(); it != _nodeValue.end(); ++it){
    SVector3& val = it->second;
    std::vector<double> vec(3);vec[0]=val(0);vec[1]=val(1);vec[2]=val(2);
    data[it->first->getNum()]=vec;
  }
  PView *pv = new PView (postFileName, "NodeData", _pModel, data, 0.0);
  pv->write(postFileName,5,false);
  delete pv;
};
void strainMapping::buildStrainView(const std::string postFileName){
  std::cout <<  "build strain view"<< std::endl;
  std::map<int, std::vector<double> > data;
  std::map<int, std::vector<double> > dataEq;
  IntPt* GP;
  GaussQuadrature Integ_Bulk(GaussQuadrature::GradGrad);
  for (groupOfElements::elementContainer::const_iterator it = _g->begin(); it != _g->end(); ++it){
    MElement *e=*it;
    int npts = Integ_Bulk.getIntPoints(e, &GP);
    int nbVertex = e->getNumVertices();
    double valx[256];
    double valy[256];
    double valz[256];
    for (int k = 0; k < nbVertex; k++){
      MVertex *v = e->getVertex(k);
      SVector3 val = _nodeValue[v];
      valx[k] =val[0];
      valy[k] =val[1];
      valz[k] =val[2];
    }
    double gradux[3];
    double graduy[3];
    double graduz[3];

    double sxx(0.) , syy(0.) , szz(0.) , sxy(0.) , sxz(0.), syz(0.);

    double volume = 0;
    for (int i=0; i<npts; i++){
      double u=GP[i].pt[0];
      double v=GP[i].pt[1];
      double w=GP[i].pt[2];
      double weight = GP[i].weight;

      double detJ = e->getJacobianDeterminant(u,v,w);
      volume += detJ*weight;

      e->interpolateGrad(valx, u, v, w, gradux);
      e->interpolateGrad(valy, u, v, w, graduy);
      e->interpolateGrad(valz, u, v, w, graduz);

      sxx += gradux[0]*detJ*weight;
      syy += gradux[1]*detJ*weight;
      szz += gradux[2]*detJ*weight;
      sxy += 0.5 * (gradux[1] + graduy[0])*detJ*weight;
      sxz += 0.5 * (gradux[2] + graduz[0])*detJ*weight;
      syz += 0.5 * (graduy[2] + graduz[1])*detJ*weight;
    }

    sxx /= volume;
    syy /= volume;
    szz /= volume;
    sxy /= volume;
    sxz /= volume;
    syz /= volume;

    std::vector<double> vec(9);
    vec[0]=sxx; vec[1]=sxy; vec[2]=sxz; vec[3]=sxy; vec[4]=syy; vec[5]=syz; vec[6]=sxz; vec[7]=syz; vec[8]=szz;

    STensor3 E(0.);
    E(0,0) = sxx; E(0,1) = sxy; E(0,2) = sxz;
    E(1,0) = sxy; E(1,1) = syy; E(0,2) = syz;
    E(2,0) = sxz; E(2,1) = syz; E(0,2) = szz;

    double tr = E.trace()/3.;
    double temp = 0;
    for (int i=0; i<3; i++){
      for (int j=0; j<3; j++){
        if (i==j){
          temp+= (E(i,j) - tr)*(E(i,j)-tr);
        }
        else{
          temp+= E(i,j)*E(i,j);
        };
      };
    };
    temp*=(2.0/3.0);
    std::vector<double> vecEq(1);
    vecEq[0] = sqrt(temp);
    dataEq[e->getNum()] = vecEq;
    data[e->getNum()]=vec;
  }
  PView *pv = new PView (postFileName, "ElementData", _pModel, data, 0.0);
  pv->write(postFileName,5,false);
  delete pv;

  size_t ext_pos = postFileName.find_last_of('.');
  std::string ext(postFileName,ext_pos+1,postFileName.size());
  std::string newname(postFileName,0,ext_pos);
  std::string fileName  = newname+"_strainEquivalent"+"."+ext;

  pv = new PView (fileName, "ElementData", _pModel, dataEq, 0.0);
  pv->write(fileName,5,false);
  delete pv;

};
