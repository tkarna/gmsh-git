#ifndef GETTOTALVOLUME_H_
#define GETTOTALVOLUME_H_

#include "groupOfElements.h"

class getTotalVolume{
  protected:
    groupOfElements* _g;

  public:
    getTotalVolume();
    ~getTotalVolume();

    void loadModel(const std::string filename);
    double totalVolume();
};


#endif // GETTOTALVOLUME_H_
