//
// C++ Interface: dofManager
//
// Description: manage multi system with only 1 dofManager
//
// Author:  <Gauthier BECKER>, (C) 2012
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef DOFMANAGERMULTISYSTEMS_H_
#define DOFMANAGERMULTISYSTEMS_H_
#include "staticDofManager.h"
template<class T>
class dofManagerMultiSystems : public staticDofManager<T>
{
 public:
  typedef typename dofManager<T>::dataVec dataVec;
  typedef typename dofManager<T>::dataMat dataMat;
 protected:
  std::vector<dofManager<T>*> _vmanager;
  std::vector<staticDofManager<T>*> _vstatic; // some components can be == NULL but avoid multiple dynamic_cast
  std::vector<int> _vncomps; // ième component = number of components of dofManager i
  int _tcomps; // number of components of all dofManagers
  // cache data
  mutable fullMatrix<T> _tmpMatrix;
  mutable fullVector<T> _tmpVector;
  // functions to hash the vectors
  void getStartStopIndices(const int i, const int allsize, int &startdof, int &stopdof) const
  {
    int mult = allsize/_tcomps;
    startdof = 0;
    for(int j=0;j<=i-1;j++) startdof += _vncomps[j];
    stopdof = startdof + _vncomps[i];
    startdof*=mult;
    stopdof*=mult;
  }
  void getStartStopIndices(const int i, const MInterfaceElement *iele, int &startdofm, int &stopdofm, int &startdofp, int &stopdofp) const
  {
    int nbFFm = iele->getElem(0)->getNumVertices();
    int nbFFp = iele->getElem(1)->getNumVertices();
    startdofm = 0;
    for(int j=0;j<=i-1;j++) startdofm += _vncomps[j];
    stopdofm = startdofm + _vncomps[i];
    startdofp = _tcomps*nbFFm+startdofm*nbFFp;
    stopdofp =  _tcomps*nbFFm+stopdofm*nbFFp;
    startdofm*= nbFFm;
    stopdofm*= nbFFm;
  }
  void fillDofVectorSystem(const int i, const std::vector<Dof> &alldofs, std::vector<Dof> &vdofs) const
  {
    vdofs.clear();
    int allsize = alldofs.size();
    int startdof,stopdof;
    this->getStartStopIndices(i,allsize,startdof,stopdof);
    for(int j=startdof;j<stopdof;j++) vdofs.push_back(alldofs[j]);
  }
  void fillDofVectorAndMatrix(const int i, std::vector<Dof> &allR, std::vector<Dof> &allC, const fullMatrix<dataMat> &allm,
                                      std::vector<Dof> &R, std::vector<Dof> &C, fullMatrix<dataMat> &m) const
  {
    R.clear(); C.clear();
    int Rallsize = allR.size();
    int Rstartdof, Rstopdof;
    this->getStartStopIndices(i,Rallsize,Rstartdof,Rstopdof);
    int Callsize = allC.size();
    int Cstartdof, Cstopdof;
    this->getStartStopIndices(i,Callsize,Cstartdof,Cstopdof);
    int Rsizem = Rstopdof-Rstartdof;
    int Csizem = Cstopdof-Cstartdof;
    m.resize(Rsizem,Csizem);
    for(int j=0;j<Rsizem;j++)
    {
      for(int k=0;k<Csizem;k++)
      {
          m(j,k) = allm(j+Rstartdof,k+Cstartdof);
      }
      R.push_back(allR[j+Rstartdof]);
    }
    for(int k=0;k<Csizem;k++) C.push_back(allC[k+Cstartdof]);
  }
  void fillDofVectorAndMatrixForInterface(const int i, const MInterfaceElement *iele, std::vector<Dof> &allR, std::vector<Dof> &allC,
                                          const fullMatrix<dataMat> &allm,std::vector<Dof> &R, std::vector<Dof> &C, fullMatrix<dataMat> &m) const
  {
    R.clear(); C.clear();
    int Rstartdofm, Rstartdofp, Rstopdofm, Rstopdofp;
    this->getStartStopIndices(i,iele,Rstartdofm,Rstopdofm,Rstartdofp,Rstopdofp);
    int Cstartdofm, Cstartdofp,Cstopdofm,Cstopdofp;
    this->getStartStopIndices(i,iele,Cstartdofm,Cstopdofm,Cstartdofp,Cstopdofp);
    int Rsizemm = Rstopdofm-Rstartdofm;
    int Rsizemp = Rstopdofp-Rstartdofp;
    int Csizemm = Cstopdofm-Cstartdofm;
    int Csizemp = Cstopdofp-Cstartdofp;
    int Rsizem = Rsizemm+Rsizemp;
    int Csizem = Csizemm+Csizemp;
    m.resize(Rsizem,Csizem);
    for(int j=0;j<Rsizemm;j++)
    {
      for(int k=0;k<Csizemm;k++)
      {
          m(j,k) = allm(j+Rstartdofm,k+Cstartdofm);
      }
      for(int k=0;k<Csizemp;k++)
      {
          m(j,k+Csizemm) = allm(j+Rstartdofm,k+Cstartdofp);
      }
      R.push_back(allR[j+Rstartdofm]);
    }
    for(int k=0;k<Csizemm;k++) C.push_back(allC[k+Cstartdofm]);
    for(int j=0;j<Rsizemp;j++)
    {
      for(int k=0;k<Csizemm;k++)
      {
          m(j+Rsizemm,k) = allm(j+Rstartdofp,k+Cstartdofm);
      }
      for(int k=0;k<Csizemp;k++)
      {
          m(j+Rsizemm,k+Csizemm) = allm(j+Rstartdofp,k+Cstartdofp);
      }
      R.push_back(allR[j+Rstartdofp]);
    }
    for(int k=0;k<Csizemp;k++) C.push_back(allC[k+Cstartdofp]);
  }

  void fillDofVectorAndMatrix(const int i, std::vector<Dof> &allR, const fullMatrix<dataMat> &allm,
                                      std::vector<Dof> &R, fullMatrix<dataMat> &m) const
  {
    R.clear();
    int allsize = allR.size();
    int startdof, stopdof;
    this->getStartStopIndices(i,allsize,startdof,stopdof);
    int sizem = stopdof-startdof;
    m.resize(sizem,sizem);
    for(int j=0;j<sizem;j++)
    {
      for(int k=0;k<sizem;k++)
      {
          m(j,k) = allm(j+startdof,k+startdof);
      }
      R.push_back(allR[j+startdof]);
    }
  }
  void fillDofVectorAndMatrixForInterface(const int i, const MInterfaceElement *iele, const std::vector<Dof> &allR,
                                          const fullMatrix<dataMat> &allm,std::vector<Dof> &R, fullMatrix<dataMat> &m) const
  {
    R.clear();
    int startdofm, startdofp, stopdofm,stopdofp;
    this->getStartStopIndices(i,iele,startdofm,stopdofm,startdofp,stopdofp);
    int sizemm = stopdofm-startdofm;
    int sizemp = stopdofp-startdofp;
    int sizem = sizemm + sizemp;
    m.resize(sizem,sizem);
    for(int j=0;j<sizemm;j++)
    {
      for(int k=0;k<sizemm;k++)
      {
          m(j,k) = allm(j+startdofm,k+startdofm);
      }
      for(int k=0;k<sizemp;k++)
      {
          m(j,k+sizemm) = allm(j+startdofm,k+startdofp);
      }
      R.push_back(allR[j+startdofm]);
    }
    for(int j=0;j<sizemp;j++)
    {
      for(int k=0;k<sizemm;k++)
      {
          m(j+sizemm,k) = allm(j+startdofp,k+startdofm);
      }
      for(int k=0;k<sizemp;k++)
      {
          m(j+sizemm,k+sizemm) = allm(j+startdofp,k+startdofp);
      }
      R.push_back(allR[j+startdofp]);
    }
  }
  void fillDofVectorAndVector(const int i, const std::vector<Dof> &allR, const fullVector<dataMat> &allv,
                              std::vector<Dof> &R, fullVector<dataMat> &v) const
  {
    R.clear();
    int allsize = allR.size();
    int startdof, stopdof;
    this->getStartStopIndices(i,allsize,startdof,stopdof);
    int sizem = stopdof-startdof;
    v.resize(sizem);
    for(int j=0;j<sizem;j++)
    {
      v(j) = allv(j+startdof);
      R.push_back(allR[j+startdof]);
    }
  }
  void fillDofVectorAndVectorForInterface(const int i, const MInterfaceElement *iele,const std::vector<Dof> &allR, const fullVector<dataMat> &allv,
                                          std::vector<Dof> &R, fullVector<dataMat> &v) const
  {
    R.clear();
    int startdofm, stopdofm, startdofp, stopdofp;
    this->getStartStopIndices(i,iele,startdofm,stopdofm,startdofp,stopdofp);
    int sizemm = stopdofm-startdofm;
    int sizemp = stopdofp-startdofp;
    v.resize(sizemm+sizemp);
    for(int j=0;j<sizemm;j++)
    {
      v(j) = allv(j+startdofm);
      R.push_back(allR[j+startdofm]);
    }
    for(int j=0;j<sizemp;j++)
    {
      v(j+sizemm) = allv(j+startdofp);
      R.push_back(allR[j+startdofp]);
    }
  }
  void fillDofVectorAndVector(const int i, const std::vector<Dof> &allR, const std::vector<double> &allv,
                              std::vector<Dof> &R, std::vector<double> &v) const
  {
    R.clear();
    int allsize = allR.size();
    int startdof, stopdof;
    this->getStartStopIndices(i,allsize,startdof,stopdof);
    int sizem = stopdof-startdof;
    v.clear();
    for(int j=0;j<sizem;j++)
    {
      v.push_back(allv[j+startdof]);
      R.push_back(allR[j+startdof]);
    }
  }
  void fillDofVectorAndVectorForInterface(const int i, const MInterfaceElement *iele, const std::vector<Dof> &allR, const std::vector<double> &allv,
                              std::vector<Dof> &R, std::vector<double> &v) const
  {
    R.clear();
    int startdofm, stopdofm, startdofp, stopdofp;
    this->getStartStopIndices(i,iele,startdofm,stopdofm,startdofp,stopdofp);
    int sizemm = stopdofm-startdofm;
    int sizemp = stopdofp-startdofp;
    v.clear();
    for(int j=0;j<sizemm;j++)
    {
      v.push_back(allv[j+startdofm]);
      R.push_back(allR[j+startdofm]);
    }
    for(int j=0;j<sizemp;j++)
    {
      v.push_back(allv[j+startdofp]);
      R.push_back(allR[j+startdofp]);
    }
  }

 public:
  // This dofManager is empty. The resolution is made on the dofManager contained in the _vmanager
  dofManagerMultiSystems(nonLinearMechSolver::scheme mys) : staticDofManager<T>(NULL,mys,false), _tcomps(0), _tmpMatrix(),
                                                            _tmpVector(){}
  dofManagerMultiSystems(linearSystem< typename dofManager<T>::dataMat> *l,staticDofManager<T> *oldmanager,
                         nonLinearMechSolver::scheme currentScheme) : staticDofManager<T>(NULL,currentScheme,false)
  {
    dofManagerMultiSystems<T>* oldmulti = static_cast<dofManagerMultiSystems<T>*>(oldmanager);
    _vmanager.clear();
    _vstatic.clear();
    for(int i=0;i<oldmulti->_vmanager.size();i++)
    {
      dofManager<T>* newman = new staticDofManager<T>(oldmulti->getManager(i)->getLinearSystem(),oldmulti->getManager(i),currentScheme);
      _vmanager.push_back(newman);
      _vstatic.push_back(static_cast<staticDofManager<T>*>(newman));
    }
  }
  dofManagerMultiSystems(nonLinearMechSolver::scheme mys, staticDofManager<T>* oldSingleManager,
                         std::vector<linearSystem<T>*> &vsys,
                         std::vector<int> &vcompbysys, std::vector<nonLinearMechSolver::scheme> &vschemebysys) : staticDofManager<T>(NULL,mys,false), _tcomps(0), _tmpMatrix(),
                                                            _tmpVector(), _vncomps(vcompbysys)
  {
    // compute the total number of comp
    for(int i=0;i<vcompbysys.size();i++)
      _tcomps += vcompbysys[i];
    // create a new manager for each system and fill the system
    std::vector<int> vmycomp;
    int firstcomp=0;
    for(int i=0;i<_vncomps.size();i++)
    {
      vmycomp.clear();
      for(int j=0;j<_vncomps[i];j++)
      {
        vmycomp.push_back(firstcomp);
        firstcomp++;
      }
      staticDofManager<T>* newmanager = new staticDofManager<T>(vsys[i],oldSingleManager,vschemebysys[i],_tcomps,vmycomp);
      _vmanager.push_back(newmanager);
      _vstatic.push_back(dynamic_cast<staticDofManager<T>*>(newmanager));
    }
  }

  ~dofManagerMultiSystems()
  {
    for(int i=0;i<_vmanager.size();i++)
    {
      delete _vmanager[i];
    }
  }
  virtual void add(dofManager<T>* manager, const int ncomp)
  {
    _vmanager.push_back(manager);
    _vstatic.push_back(dynamic_cast<staticDofManager<T>*>(manager));
    _vncomps.push_back(ncomp);
    _tcomps+=ncomp;
  }
  virtual void reset(dofManager<T>* manager,const int manindex)
  {
    _vmanager[manindex] = manager;
    _vstatic[manindex] = dynamic_cast<staticDofManager<T>*>(manager);
  }
  virtual dofManager<T>* getManager(const int i){return _vmanager[i];}
  virtual dofManager<T>* getDofManagerByComp(int comp)
  {
   #if defined(HAVE_MPI)
    if(comp<0) comp = -comp; // positive one (can be negative for ghost Dofs)
   #endif // HAVE_MPI
   #ifdef _DEBUG
    if(_tcomps < comp+1){
      Msg::Error("You try to select a system with a comp that is larger than the total number of comp");
      return NULL;
    }
   #endif // _DEBUG
    int addcomp = 0;
    for(int i=0;i<_vmanager.size();i++)
    {
      addcomp+= _vncomps[i];
      if(addcomp>comp) return _vmanager[i];
    }
    Msg::Error("Fail to find the manager associated to your comp!");
    return NULL;
  }
  virtual const dofManager<T>* getDofManagerByComp(int comp) const
  {
   #if defined(HAVE_MPI)
    if(comp<0) comp = -comp; // positive one (can be negative for ghost Dofs)
   #endif // HAVE_MP
   #ifdef _DEBUG
    if(_tcomps < comp+1){
      Msg::Error("You try to select a system with a comp that is larger than the total number of comp");
      return NULL;
    }
   #endif // _DEBUG
    int addcomp = 0;
    for(int i=0;i<_vmanager.size();i++)
    {
      addcomp+= _vncomps[i];
      if(addcomp>comp) return _vmanager[i];
    }
    Msg::Error("Fail to find the manager associated to your comp!");
    return NULL;
  }
  virtual inline void fixDof(Dof key, const dataVec &value){Msg::Error("A dof cannot be fixed using the dofManagerMultiSystem! Select first your system");}
  virtual inline bool isFixed(Dof key) const{Msg::Error("dofManagerMultiSystems cannot check if a Dof is fixed select first your system");}
  virtual inline bool isAnUnknown(Dof key) const{Msg::Error("dofManagerMultiSystems cannot check if a Dof is an unknown. Select first your system");}
  virtual inline void numberDof(Dof key){Msg::Error("dofManagerMultiSystems cannot number a Dof. Select first your system");}
  virtual inline void numberDof(const std::vector<Dof> &keys)
  {
    std::vector<Dof> syskeys;
    for(int i=0;i<_vmanager.size();i++)
    {
      this->fillDofVectorSystem(i,keys,syskeys);
      _vmanager[i]->numberDof(syskeys);
    }
  }

  virtual inline void numberGhostDof (Dof key, int procId){Msg::Error("dofManagerMultiSystems cannot number a ghost dof. Select first your system");}
  virtual inline void getDofValue(std::vector<Dof> &keys, std::vector<dataVec> &Vals)
  {
    std::vector<Dof> syskeys;
    for(int i=0;i<_vmanager.size();i++)
    {
      this->fillDofVectorSystem(i,keys,syskeys);
      _vmanager[i]->getDofValue(syskeys,Vals);
    }
  }
  virtual inline bool getAnUnknown(Dof key,  dataVec &val) const
  {
    for(int i=0;i<_vmanager.size();i++)
    {
      bool finddof = _vmanager[i]->getAnUnknown(key,val);
      if(finddof) return true;
    }
    return false;
  }
  virtual inline void getDofValue(Dof key,  dataVec &val) const // WARNING NOT EFFICIENT
  {
    this->getDofValue(key,val,nonLinearBoundaryCondition::position);
  }
/*
  {
    for(int i=0;i<_vmanager.size();i++)
    {
      _vmanager[i]->getDofValue(key,val);
    }
  }
*/
  virtual inline void insertInSparsityPatternLinConst(const Dof &R, const Dof &C)
  {
    Msg::Error("dofManagerMultiSystems cannot use insertInSparsityPatternLinConst");
  }
  virtual inline void insertInSparsityPattern(const Dof &R, const Dof &C)
  {
    Msg::Error("dofManagerMultiSystems cannot use insertInSparsityPattern");
  }
  virtual inline void assemble(const Dof &R, const Dof &C, const dataMat &value)
  {
    Msg::Error("dofManagerMultiSystems cannot use assemble Dof &R Dof & C datamat");
  }
  virtual inline void assemble(std::vector<Dof> &R, std::vector<Dof> &C,
                       const fullMatrix<dataMat> &m)
  {
    std::vector<Dof> Rkeys;
    std::vector<Dof> Ckeys;
    for(int i=0;i<_vmanager.size();i++)
    {
      this->fillDofVectorAndMatrix(i,R,C,m,Rkeys,Ckeys,_tmpMatrix);
      _vmanager[i]->assemble(Rkeys,Ckeys,_tmpMatrix);
    }
  }
  virtual inline void assemble(const MInterfaceElement *iele,std::vector<Dof> &R, std::vector<Dof> &C,
                               const fullMatrix<dataMat> &m)
  {
    std::vector<Dof> Rkeys;
    std::vector<Dof> Ckeys;
    for(int i=0;i<_vmanager.size();i++)
    {
      this->fillDofVectorAndMatrixForInterface(i,iele,R,C,m,Rkeys,Ckeys,_tmpMatrix);
      _vmanager[i]->assemble(Rkeys,Ckeys,_tmpMatrix);
    }
  }
  virtual inline void assemble(std::vector<Dof> &R, const fullVector<dataMat> &m)
  {
    std::vector<Dof> Rkeys;
    for(int i=0;i<_vmanager.size();i++)
    {
      this->fillDofVectorAndVector(i,R,m,Rkeys,_tmpVector);
      _vmanager[i]->assemble(Rkeys,_tmpVector);
    }
  }
  virtual inline void assemble(const MInterfaceElement *iele,std::vector<Dof> &R, const fullVector<dataMat> &m)
  {
    std::vector<Dof> Rkeys;
    for(int i=0;i<_vmanager.size();i++)
    {
      this->fillDofVectorAndVectorForInterface(i,iele,R,m,Rkeys,_tmpVector);
      (static_cast<staticDofManager<T>*>(_vmanager[i]))->assemble(Rkeys,_tmpVector,nonLinearSystem<T>::Fint);
    }
  }
  virtual inline void assemble(std::vector<Dof> &R, const fullMatrix<dataMat> &m)
  {
    std::vector<Dof> Rkeys;
    for(int i=0;i<_vmanager.size();i++)
    {
      // This function is only call for stiffness matrix otherwise extra argument --> Do not assemble for explicit system
      if(static_cast<staticDofManager<double>*>(_vmanager[i])->getScheme()!=nonLinearMechSolver::Explicit)
      {
        this->fillDofVectorAndMatrix(i,R,m,Rkeys,_tmpMatrix);
        _vmanager[i]->assemble(Rkeys,_tmpMatrix);
      }
    }
  }
  virtual inline void assemble(const MInterfaceElement *iele,std::vector<Dof> &R, const fullMatrix<dataMat> &m)
  {
    std::vector<Dof> Rkeys;
    for(int i=0;i<_vmanager.size();i++)
    {
      // This function is only call for stiffness matrix otherwise extra argument --> Do not assemble for explicit system
      if(static_cast<staticDofManager<double>*>(_vmanager[i])->getScheme()!=nonLinearMechSolver::Explicit)
      {
        this->fillDofVectorAndMatrixForInterface(i,iele,R,m,Rkeys,_tmpMatrix);
        _vmanager[i]->assemble(Rkeys,_tmpMatrix);
      }
    }
  }
  virtual inline void assemble(const Dof &R, const dataMat &value)
  {
    Msg::Error("dofManagerMultiSystems cannot use assemble Dof &R datamat");
  }
  virtual int sizeOfR() const
  {
    int syssize=0;
    for(int i=0;i<_vmanager.size();i++)
      syssize+=_vmanager[i]->sizeOfR();
    return syssize;
  }
  virtual int sizeOfF() const
  {
    int fixsize=0;
    for(int i=0;i<_vmanager.size();i++)
      fixsize+=_vmanager[i]->sizeOfF();
    return fixsize;
  }
  virtual void systemSolve()
  {
    for(int i=0;i<_vmanager.size();i++)
      _vmanager[i]->systemSolve();
  }
  virtual void systemClear()
  {
    for(int i=0;i<_vmanager.size();i++)
    {
      _vmanager[i]->systemClear();
    }
  }
  virtual inline void setCurrentMatrix(std::string name)
  {
    for(int i=0;i<_vmanager.size();i++)
      _vmanager[i]->setCurrentMatrix(name);
  }
  virtual linearSystem<dataMat> *getLinearSystem(std::string &name)
  {
    Msg::Error("dofManagerMultiSystems cannot get a linearSystem");
  }
  virtual inline void setLinearConstraint (Dof key, DofAffineConstraint<dataVec> &affineconstraint)
  {
    Msg::Error("dofManagerMultiSystems cannot setLinearConstraint");
  }
  virtual inline void assembleLinConst(const Dof &R, const Dof &C, const dataMat &value)
  {
    Msg::Error("dofManagerMultiSystems cannot assembleLinConst");
  }
  virtual void getFixedDof(std::vector<Dof> &R)
  {
    R.clear();
    std::vector<Dof> Rtmp;
    for(int i=0;i<_vmanager.size();i++)
    {
      _vmanager[i]->getFixedDof(Rtmp);
      for(int j=0;j<Rtmp.size();j++)
        R.push_back(Rtmp[j]);
    }
  }
  virtual int getDofNumber(const Dof& key)
  {
    for(int i=0;i<_vmanager.size();i++)
    {
      int dnum = _vmanager[i]->getDofNumber(key);
      if(dnum !=-1) return dnum;
    }
    return -1;
  }
  virtual void setFirstRigidContactUnknowns() // contact in the first system !!
  {
    this->_firstRigidContactDof = _vmanager[0]->sizeOfR();
  }
  virtual inline void assemble(std::vector<Dof> &R, const fullMatrix<typename dofManager<T>::dataMat> &m,
                               const typename explicitHulbertChung<T>::whichMatrix wm)
  {
    std::vector<Dof> Rkeys;
    for(int i=0;i<_vmanager.size();i++)
    {
      this->fillDofVectorAndMatrix(i,R,m,Rkeys,_tmpMatrix);
      if(((wm==explicitHulbertChung<T>::mass) and _vstatic[i]->getScheme()!=nonLinearMechSolver::StaticNonLinear and _vstatic[i]->getScheme()!=nonLinearMechSolver::StaticLinear) or
         ((wm==explicitHulbertChung<T>::stiff) and _vstatic[i]->getScheme()!=nonLinearMechSolver::Explicit))
      {
        _vstatic[i]->assemble(Rkeys,_tmpMatrix,wm);
      }
    }
  }
  virtual inline void assemble(std::vector<Dof> &R, const fullVector<typename dofManager<T>::dataMat> &m,const typename nonLinearSystem<T>::rhs wrhs)
  {
    std::vector<Dof> Rkeys;
    for(int i=0;i<_vmanager.size();i++)
    {
      this->fillDofVectorAndVector(i,R,m,Rkeys,_tmpVector);
      _vstatic[i]->assemble(Rkeys,_tmpVector,wrhs);
    }
  }
  virtual inline void assemble(const Dof &R, const typename dofManager<T>::dataMat &value, const typename nonLinearSystem<T>::rhs wrhs)
  {
    Msg::Error("dofManagerMultiSystems cannot assemble a single Dof");
  }
  virtual void clearRHSfixed()
  {
    for(int i=0;i<_vmanager.size();i++){
      _vstatic[i]->clearRHSfixed();
    }
  }
  virtual void getForces(std::vector<archiveForce> &vaf) const
  {
    std::vector<double> forceallsys(vaf.size(),0.);
    for(int i=0;i<_vmanager.size();i++)
    {
      _vstatic[i]->getForces(vaf);
      for(int j=0;j<vaf.size();j++)
        forceallsys[j]+= vaf[j].fval;
    }
    for(int j=0;j<forceallsys.size();j++)
      vaf[j].fval = forceallsys[j];
  }
  virtual inline void setInitialCondition(const Dof &R, const typename dofManager<T>::dataVec &value,
                                          const typename nonLinearBoundaryCondition::whichCondition wc)
  {
    Msg::Error("dofManagerMultiSystems cannot setInitialConditions a single Dof");
  }

  virtual void getVertexMass(std::vector<Dof> &R, std::vector<typename dofManager<T>::dataVec> &vmass)
  {
    std::vector<Dof> syskeys;
    for(int i=0;i<_vmanager.size();i++)
    {
      this->fillDofVectorSystem(i,R,syskeys);
      _vstatic[i]->getVertexMass(syskeys,vmass);
    }
  }
  virtual double getKineticEnergy(const int systemSizeWithoutRigidContact,FilterDofSet &fildofcontact) const
  {
    double allsyskin=0.;
    for(int i=0;i<_vmanager.size();i++) allsyskin += _vstatic[i]->getKineticEnergy(systemSizeWithoutRigidContact,fildofcontact);
  }
  virtual void printRightHandSide() const
  {
    for(int i=0;i<_vmanager.size();i++)
    {
      Msg::Info("dofManager %d",i);
      _vstatic[i]->printRightHandSide();
    }
  }
  virtual void getFixedRightHandSide(std::vector<Dof> &R, std::vector<T> &val)
  {
    R.clear(); val.clear();
    std::vector<Dof> syskeys;
    std::vector<T> sysval;
    for(int i=0;i<_vmanager.size();i++)
    {
      static_cast<staticDofManager<T>*>(_vmanager[i])->getFixedRightHandSide(syskeys,sysval);
      for(int j=0;j<syskeys.size();j++)
      {
        R.push_back(syskeys[j]);
        val.push_back(sysval[j]);
      }
    }
  }
  virtual void getRightHandSidePlus(Dof R, typename dofManager<T>::dataVec &val)
  {
    Msg::Error("dofManagerMultiSystems cannot getRightHandSidePlus of a single Dof");
  }
  virtual void getRightHandSidePlus(const std::vector<Dof> &R, std::vector<typename dofManager<T>::dataVec> &val)
  {
    val.clear();
    std::vector<Dof> syskeys;
    std::vector<T> sysval;
    for(int i=0;i<_vmanager.size();i++)
    {
      _vstatic[i]->getRightHandSidePlus(syskeys,sysval);
      for(int j=0;j<syskeys.size();j++)
      {
        val.push_back(sysval[j]);
      }
    }
  }
  virtual void setInitial(const std::vector<Dof> &R, const std::vector<double> &disp)
  {
    std::vector<Dof> Rkeys;
    std::vector<double> disptmp;
    for(int i=0;i<_vmanager.size();i++)
    {
      this->fillDofVectorAndVector(i,R,disp,Rkeys,disptmp);
      _vstatic[i]->setInitial(Rkeys,disptmp);
    }
  }
  virtual int systemSolveIntReturn()
  {
    int globalSuc = 1;
    for(int i=0;i<_vmanager.size();i++)
    {
      int suc = _vstatic[i]->systemSolveIntReturn();
      if(suc==0)
      {
        Msg::Error("The system %d is not successfully solved!",i);
        globalSuc = 0;
      }
    }
    return globalSuc;
  }
  virtual inline void getDofValue(Dof key,  typename dofManager<T>::dataVec &val,
                                  nonLinearBoundaryCondition::whichCondition wv) const
  {
    if(_tcomps<100)
    {
      int comp = key.getType()%100; // FIX THIS HOW ??
      const staticDofManager<T>* nlManager = static_cast<const staticDofManager<T>*>(this->getDofManagerByComp(comp));
      if(wv==nonLinearBoundaryCondition::position or nlManager->getScheme()==nonLinearMechSolver::Explicit)
        nlManager->getDofValue(key,val,wv);
      else
        val=0.;
    }
    else
    {
      Msg::Error("Cannot extract the dof comp when the total number of comp is > 99! Fix this");
      val = 0.;
    }
  }

  virtual inline void getDofValue(std::vector<Dof> &keys,std::vector<typename dofManager<T>::dataVec> &Vals,
                                    const typename nonLinearBoundaryCondition::whichCondition wv) const
  {
    std::vector<Dof> Rkeys;
    std::vector<dataVec> valstmp;
    for(int i=0;i<_vmanager.size();i++)
    {
      this->fillDofVectorSystem(i,keys,Rkeys);
      _vstatic[i]->getDofValue(Rkeys,valstmp,wv);
      for(int j=0;j<Rkeys.size();j++)
        Vals.push_back(valstmp[j]);
    }
  }
  virtual inline void allocateSystem()
  {
    static std::string A("A");
    for(int i=0;i<_vmanager.size();i++)
    {
      int nsize = _vmanager[i]->sizeOfR();
      _vmanager[i]->getLinearSystem(A)->allocate(nsize);
    }
  }
  // computed on Static System Only !! Fix this normally the check has to be performed on each system
  virtual inline double normInfRightHandSide()
  {
    double sysnorm=0;
    for(int i=0;i<_vmanager.size();i++){
      staticDofManager<T> *nlassembler = _vstatic[i];
      if(nlassembler->getScheme()!=nonLinearMechSolver::Explicit)
      {
        double mynorm = nlassembler->normInfRightHandSide();
        if(mynorm>sysnorm)
          sysnorm = mynorm;
      }
    }
    return sysnorm;
  }
  // Computed on Static System Only. Fix this normally the check has to be performed on each system
  virtual inline double norm0Inf()
  {
    double sysnorm=0;
    for(int i=0;i<_vmanager.size();i++){
      staticDofManager<T>* nlassembler = _vstatic[i];
      if(nlassembler->getScheme()!=nonLinearMechSolver::Explicit)
      {
        double mynorm = nlassembler->norm0Inf();
        if(mynorm>sysnorm)
          sysnorm = mynorm;
      }
    }
    return sysnorm;
  }
  virtual inline void zeroMatrix()
  {
    for(int i=0;i<_vmanager.size();i++)
      if(_vstatic[i]->getScheme()!=nonLinearMechSolver::Explicit) // the matrices are never recomputed in explicit !!
        _vstatic[i]->zeroMatrix();
  }
  virtual inline int lineSearch()
  {
    int globalConverged = 1;
    for(int i=0;i<_vmanager.size();i++)
    {
      int ls = _vstatic[i]->lineSearch();
      if(ls == 0)
      {
        Msg::Warning("No line Search convergence for system %d !",i);
        globalConverged = 0;
      }
    }
    return globalConverged;
  }
  virtual inline void resetUnknownsToPreviousTimeStep()
  {
    for(int i=0;i<_vmanager.size();i++)
      _vstatic[i]->resetUnknownsToPreviousTimeStep();
  }
  virtual inline void nextStep()
  {
    for(int i=0;i<_vmanager.size();i++)
      _vstatic[i]->nextStep();
  }
  virtual inline void setTimeStep(const double dt)
  {
    for(int i=0;i<_vmanager.size();i++)
      _vstatic[i]->setTimeStep(dt);
  }
  virtual inline void createRestart()
  {
    for(int i=0;i<_vmanager.size();i++)
      _vstatic[i]->createRestart();
  }
 #if defined(HAVE_MPI)
  virtual void manageMPIComm(const int otherPartNum,const std::vector<Dof> &otherPartR)
  {
    std::vector<Dof> Rkeys;
    for(int i=0;i<_vmanager.size();i++)
    {
      this->fillDofVectorSystem(i,otherPartR,Rkeys);
      _vstatic[i]->manageMPIComm(otherPartNum,Rkeys);
    }
  }
  virtual void systemMPIComm()
  {
    for(int i=0;i<_vmanager.size();i++)
      _vstatic[i]->systemMPIComm();
  }
 #endif // HAVE_MPI
};
#endif //DOFMANAGERMULTISYSTEMS_H_
