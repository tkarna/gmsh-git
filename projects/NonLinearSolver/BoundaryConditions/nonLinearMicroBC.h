#ifndef NONLINEARMICROBC_H_
#define NONLINEARMICROBC_H_

#include "nonLinearBC.h"

// periodic BC
#ifndef SWIG

class nonLinearMicroBC{
	public:
		enum whichBCType{LDBC =0, PBC=1, MKBC=2, MIXBC=3}; // Linear displacement BC, periodic BC, minimal kinematical BC, mixed BC

	private:
	  // kinematical variables: deformation gradient F, gradient of deformation gradient
	  STensor3 _currentDeformationGradient, _previousDeformationGradient;
	  STensor33 _currentGradientOfDeformationGradient, _previousGradientOfDeformationGradient;
	  STensor3 _firstKinematicVariable;
	  STensor33 _secondKinematicVariable;

	  double _time;
		std::vector<groupOfElements*> _g;	// boundary element groups
		std::vector<int> _physical;				// physical ofr apply  micro BC
		int _order;  // order of homogenization
		bool _timeDependency;
		int _dim, _tag;

	public:
		nonLinearMicroBC(const int tag,const int g1,const int g2,const int g3,
                     const int g4, const int g5 , const int g6)
                    : _tag(tag),_currentDeformationGradient(1.0),_previousDeformationGradient(1.),
                    _previousGradientOfDeformationGradient(0.0),_currentGradientOfDeformationGradient(0.),
                    _time(1.0),_order(1),_timeDependency(true),_firstKinematicVariable(0),
                    _secondKinematicVariable(0){
      _physical.clear();
      if (g1>0) _physical.push_back(g1);
      if (g2>0) _physical.push_back(g2);
      if (g3>0) _physical.push_back(g3);
      if (g4>0) _physical.push_back(g4);
      if (g5>0) _physical.push_back(g5);
      if (g6>0) _physical.push_back(g6);
      int size =_physical.size();
      if (size ==0 or size ==2 or size == 4 or size == 6) {
        _dim = size/2;
        for (int i=0; i<2*_dim; i++){
          groupOfElements* gr = new groupOfElements(_dim-1,_physical[i]);
          _g.push_back(gr);
        };
      }
      else
        Msg::Fatal("Micro boundary condition is not correctly defined");
    };
		nonLinearMicroBC(const nonLinearMicroBC& src){
		  _tag = src._tag;
			_currentDeformationGradient = src._currentDeformationGradient;
			_time = src._time;
			_currentGradientOfDeformationGradient = src._currentGradientOfDeformationGradient;
			_order = src._order;
			_g = src._g;
			_timeDependency = src._timeDependency;
			_physical = src._physical;
			_previousDeformationGradient = src._previousDeformationGradient;
			_previousGradientOfDeformationGradient = src._previousGradientOfDeformationGradient;
			_firstKinematicVariable = src._firstKinematicVariable;
			_secondKinematicVariable = src._secondKinematicVariable;
		};
		virtual ~nonLinearMicroBC(){};
		virtual nonLinearMicroBC::whichBCType getType() const  = 0;

    int getTag() const {return _tag;};
		int getOrder() const {return _order;};
		void setOrder(const int order){ _order =order;};
		int getDim() const{return _dim;};

		int size() const {return _physical.size();};

		const std::vector<groupOfElements*>& getBoundaryGroupOfElements() const {return _g;};
		const std::vector<int>& getBoundaryPhysicals() const{return _physical;};

		const groupOfElements* getBCGroup(const int physical) const{
      for (int i=0; i<_physical.size(); i++){
        if (_physical[i] == physical){
          return _g[i];
        }
      }
      Msg::Fatal("physical is not exist in this boundary group");
		};

		groupOfElements* getBCGroup(const int physical){
      for (int i=0; i<_physical.size(); i++){
        if (_physical[i] == physical){
          return _g[i];
        }
      }
      Msg::Fatal("physical is not exist in this boundary group");
		};


    void setTime(double t){
      _time = t;
      _firstKinematicVariable = this->getDeformationGradient();
      _firstKinematicVariable(0,0) -= 1.;
      _firstKinematicVariable(1,1) -= 1.;
      _firstKinematicVariable(2,2) -= 1.;
      if (_order==2){
        _secondKinematicVariable = this->getGradientOfDeformationGradient();
      }
    };
		double getTime() const {return _time;}
    bool isTimeDependency() const{ return _timeDependency;};
    void switchTimeDependency() {
      if (_timeDependency) _timeDependency = false;
      else _timeDependency = true;
    };
    // for kinematic variable
    void setDeformationGradient(const int i,const int j, const double val){
      _currentDeformationGradient(i,j) = val;
		};
		void setDeformationGradient(const STensor3& ten){
      _currentDeformationGradient = ten;
		};
		void setGradientOfDeformationGradient(const int i, const int j,const int k, const double val){
      _currentGradientOfDeformationGradient(i,j,k) = val;
		};
		void setGradientOfDeformationGradient(const STensor33& ho){
      _currentGradientOfDeformationGradient = ho;
		};

		STensor3 getDeformationGradient() const{
			STensor3 F(_currentDeformationGradient);
			F -= _previousDeformationGradient;
			if (_timeDependency) F*= _time;
			F += _previousDeformationGradient;
			return F;
		};

		STensor33 getGradientOfDeformationGradient() const{
      STensor33 s(_currentGradientOfDeformationGradient);
      s -= _previousGradientOfDeformationGradient;
      if (_timeDependency) s*=_time;
      s += _previousGradientOfDeformationGradient;
      return s;
		};

		void setFirstOrderKinematicalVariable(const STensor3& F){
      _firstKinematicVariable = F;
		}
		void setSecondOrderKinematicalVariable(const STensor33& G){
      _secondKinematicVariable = G;
		}

    const STensor3& getFirstOrderKinematicalVariable() const{
      return _firstKinematicVariable;
    };

    const STensor33& getSecondOrderKinematicalVariable() const{
      return _secondKinematicVariable;
    };

    void nextStepFirstOrder(){
      _previousDeformationGradient = _currentDeformationGradient;
      _time = 0.;
    };
    void nextStepSecondOrder(){
      _previousGradientOfDeformationGradient = _currentGradientOfDeformationGradient;
      _time=0.;
    };
    void nextStep(){
      this->nextStepFirstOrder();
      this->nextStepSecondOrder();
    }
    void resetCurrentDeformationGradient(){
      _currentDeformationGradient *= 0.;
      for (int i=0; i<3; i++)
        _currentDeformationGradient(i,i) = 1.;
    }
    void resetCurrentGradientOfDeformationGradient(){
      _currentGradientOfDeformationGradient *= 0;
    }
    void resetPreviousDeformationGradient(){
      _previousDeformationGradient *= 0;
      for (int i=0; i<3; i++)
        _previousDeformationGradient(i,i) = 1.;
    }
    void resetPreviousGradientOfDeformationGradient(){
      _previousGradientOfDeformationGradient *= 0;
    }
    void resetCurrentVariable(){
    	resetCurrentDeformationGradient();
    	resetCurrentGradientOfDeformationGradient();
    }
    void resetPreviousVariable(){
    	resetPreviousDeformationGradient();
    	resetPreviousGradientOfDeformationGradient();
    }
    double kinematicalResidual() const {
      STensor3 diff(_currentDeformationGradient);
      diff-= _previousDeformationGradient;
      double valref = dot(_currentDeformationGradient, _currentDeformationGradient)+
                      dot(_previousDeformationGradient,_previousDeformationGradient);
      double val = 0.;
      if (valref>1.e-16)
      	val = sqrt(dot(diff,diff)/valref);

      double f = 0.;
      if (_g.size()>0){
        const groupOfElements* g0 = _g[0];

        if (g0->size()>0){
          MElement* ele = *(g0->begin());
          int dim = ele->getDim();
          double volume = ele->getVolume();
          if (volume <= 0)
            Msg::Fatal("Error in nonLinearMicroBC::kinematicalResidual");

          if (dim ==1) f = 1./volume;
          else f = 1./sqrt(volume);
        }
      }



      if (_order==2){
		    STensor33 diff33(_currentGradientOfDeformationGradient);
		    diff33 -= _previousGradientOfDeformationGradient;
		    valref = f+dot(_currentGradientOfDeformationGradient,_currentGradientOfDeformationGradient)+
								 dot(_previousGradientOfDeformationGradient,_previousGradientOfDeformationGradient);
		    if (valref >1.e-16) val += sqrt(dot(diff33,diff33)/valref);
      }
      return val;
    }
    const STensor3& getCurrentDeformationGradient() const{ return _currentDeformationGradient;};
    const STensor3& getPreviousDeformationGradient() const{return _previousDeformationGradient;};
    const STensor33& getCurrentGradientOfDeformationGradient() const{return _currentGradientOfDeformationGradient;};
    const STensor33& getPreviousGradientOfDeformationGradient()const{return _previousGradientOfDeformationGradient;};

    STensor3 getDifferenceOfDeformationGradient()const{
      STensor3 dF = this->getCurrentDeformationGradient();
      dF -= this->getPreviousDeformationGradient();
      return dF;
    }
    STensor33 getDifferenceOfGradientOfDeformationGradient() const{
      STensor33 dG = this->getCurrentGradientOfDeformationGradient();
      dG -= getPreviousGradientOfDeformationGradient();
      return dG;
    };
};

class nonLinearPeriodicBC: public nonLinearMicroBC{
	public:
		enum whichMethod{CEM=0, LIM=1, CSIM=2, FE_LIN=3, FE_QUA=4}; // constraint elimination, lagrange interpolation, cubic spline interpolation --> for constructing linear constraint

	private:
		whichMethod _wM;
		int _Xdegree;		// for lagrange and spline interpolation
		bool _XaddNewVertices; // add new nodes for formultation


    int _Ydegree;		// for lagrange and spline interpolation
		bool _YaddNewVertices; // add new nodes for formultation

		int _Zdegree;		// for lagrange and spline interpolation
		bool _ZaddNewVertices; // add new nodes for formultation


	public:
		nonLinearPeriodicBC(const int tag, const int g1,const int g2,const int g3,
                        const int g4, const int g5 , const int g6=0)
                        : nonLinearMicroBC(tag,g1,g2,g3,g4,g5,g6),
                        _XaddNewVertices(false),_Xdegree(3),
                        _YaddNewVertices(false),_Ydegree(3),
                        _ZaddNewVertices(false),_Zdegree(3),
                        _wM(nonLinearPeriodicBC::CEM){};

		nonLinearPeriodicBC(const nonLinearPeriodicBC& src) : nonLinearMicroBC(src){
			_wM = src._wM;
			_Xdegree = src._Xdegree;
			_XaddNewVertices = src._XaddNewVertices;
			_Ydegree = src._Ydegree;
			_YaddNewVertices = src._YaddNewVertices;
			_Zdegree = src._Zdegree;
			_ZaddNewVertices = src._ZaddNewVertices;
		};
		virtual ~nonLinearPeriodicBC(){};

		int getMaxDegree() const{
      int xy = std::max(_Xdegree,_Ydegree);
      if (this->getDim() == 2) return xy;
      else return std::max(_Zdegree,xy);
		};

		bool isNewVertices() const{
      return (_XaddNewVertices&& _YaddNewVertices)&&_ZaddNewVertices;
		}

		bool addNewVertices(const int i) const { // add new Dof flag
      if (_wM == CSIM) return true;
      else {
        if (i ==0) return _XaddNewVertices;
        else if (i==1) return _YaddNewVertices;
        else if (i==2) return _ZaddNewVertices;
        else Msg::Fatal("direction must be correctly defined addNewVertices(i)");
      };
    };
		void setAddNewVertices(const int i, const bool add = true) {
      if (i==0) _XaddNewVertices= add;
      else if (i==1) _YaddNewVertices = add;
      else if (i==2) _ZaddNewVertices = add;
      else Msg::Fatal("direction must be correctly defined setAddNewVertices(i)");
    };

    int getPBCPolynomialDegree(const int i) const{
      if (i==0) return _Xdegree;
      else if (i==1) return _Ydegree;
      else if (i==2) return _Zdegree;
      else Msg::Fatal("direction must be correctly defined getPBCPolynomialDegree(i)");
    }
    void setPBCPolynomialDegree(const int i, const int d){
      if (i==0) _Xdegree = d;
      else if (i==1) _Ydegree = d;
      else if (i==2) _Zdegree = d;
      else Msg::Fatal("direction must be correctly defined setPBCPolynomialDegree(i)");
    }

		whichMethod getPBCMethod() const{
			return _wM;
		};
		void setPBCMethod(const int i){
      _wM = whichMethod(i);
		}
    virtual nonLinearMicroBC::whichBCType getType() const  {return nonLinearMicroBC::PBC;};
};

class nonLinearDisplacementBC : public nonLinearMicroBC{

	public:
		nonLinearDisplacementBC( const int tag, const int g1, const int g2, const int g3,
                             const int g4, const int g5, const int g6=0): nonLinearMicroBC(tag,g1,g2,g3,g4,g5,g6){

		};
		virtual nonLinearMicroBC::whichBCType getType() const{
			return nonLinearMicroBC::LDBC;
		};
};

class nonLinearMinimalKinematicBC : public nonLinearMicroBC{
  public:
    nonLinearMinimalKinematicBC(const int tag, const int g1 , const int g2,  const int g3,
                                const int g4, const int g5, const int g6) : nonLinearMicroBC(tag,g1,g2,g3,g4,g5,g6){
		};

		virtual nonLinearMicroBC::whichBCType getType() const{
			return nonLinearMicroBC::MKBC;
		};
};

class nonLinearMixedBC : public nonLinearMicroBC{
  protected:
    std::vector<std::pair<int,int> > _kPhysical; // null fluctuation physical + component
    std::vector<std::pair<int,int> > _sPhysical; // minimal BC physical + component

  public:
    nonLinearMixedBC(const int tag, const int g1 , const int g2,  const int g3,
                      const int g4, const int g5, const int g6) : nonLinearMicroBC(tag,g1,g2,g3,g4,g5,g6){}
    virtual ~nonLinearMixedBC(){};

    virtual nonLinearMicroBC::whichBCType getType() const{
			return nonLinearMicroBC::MIXBC;
		};

		void setKinematicPhysical(const int i, const int comp){
      _kPhysical.push_back(std::pair<int,int>(i,comp));
		}
		void setStaticPhysical(const int i, const int comp){
      _sPhysical.push_back(std::pair<int,int>(i,comp));
		}

		const std::vector<std::pair<int,int> >& getKinematicPhysical() const {return _kPhysical;}
		const std::vector<std::pair<int,int> >& getStaticPhysical() const {return _sPhysical;}
};

#endif //SWIG
#endif // NONLINEARMICROBC_H_
