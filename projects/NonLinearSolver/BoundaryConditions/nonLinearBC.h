//
//
// Description: Class to set Boundary Conditions
//
//
// Author:  <Gauthier BECKER>, (C) 2010
//
// Copyright: See COPYING file that comes with this distribution
//
//
// Has to be grouped with BoundaryCondition defined in elasticitySolver.h These BC have to be defined in a separated file
// I add nonLinear above to avoid ambiguities
#ifndef NONLINEARBC_H_
#define NONLINEARBC_H_
#ifndef SWIG
#include "groupOfElements.h"
#include "simpleFunction.h"
#include "SVector3.h"
#include "quadratureRules.h"
#include "timeFunction.h"
#include "contactFunctionSpace.h"
#include "solverAlgorithms.h" // change this
#endif
class partDomain;
class nonLinearBoundaryCondition
{
 public:
  enum whichCondition{position=0, velocity=1, acceleration=2};
  enum location{UNDEF,ON_VERTEX,ON_EDGE,ON_FACE,ON_VOLUME,PRESSURE,RIGIDCONTACT,ON_FACE_VERTEX};
  enum type{DIRICHLET, NEUMANN,INITIAL,PERIODIC,CONSTRAINT,RCONTACT,SAMEDISP,FIXONFACE};
  #ifndef SWIG
  location onWhat; // on vertices or elements
  int _tag; // tag for the dofManager or Physical number of BC
  groupOfElements *g; // support for this BC
  nonLinearBoundaryCondition() : g(0),_tag(0),onWhat(UNDEF) {};
  nonLinearBoundaryCondition(const nonLinearBoundaryCondition &source){
    this->onWhat = source.onWhat;
    this->_tag = source._tag;
    this->g = source.g;
  }
  virtual type getType()const=0;
  const int dimension()
  {
    switch(onWhat){
     case UNDEF:
      Msg::Warning("can't give the dimension of an UNDEF boundary conditions");
      return 0;
     case ON_VERTEX:
      return 0;
     case ON_EDGE:
      return 1;
     case ON_FACE:
      return 2;
     case ON_VOLUME:
      return 3;
     case PRESSURE:
      return 2;
     case RIGIDCONTACT:
      return 2;
    }
  }
#endif
};
#ifndef SWIG

class nonLinearDirichletBC : public nonLinearBoundaryCondition
{
 public:
  int _comp; // component
  simpleFunctionTime<double> *_f;
  /* BEAWARE the four following vectors have to be filled in the same time */
  std::vector<FunctionSpaceBase*> _vspace;
  std::vector<groupOfElements*> _vgroup;
  std::vector<FilterDof*> _vfilter;
  std::vector<bool> _vdg; // true if discontinuous galerkin formulation (ie Dof entity = element number)
  nonLinearBoundaryCondition::whichCondition _mycondition;
  virtual nonLinearBoundaryCondition::type getType()const{return nonLinearBoundaryCondition::DIRICHLET;}
  nonLinearDirichletBC() : nonLinearBoundaryCondition(),_comp(0),_f(0), _mycondition(nonLinearBoundaryCondition::position){}
  nonLinearDirichletBC(const nonLinearDirichletBC &source) : nonLinearBoundaryCondition(source){
    this->_comp = source._comp;
    this->_f = source._f;
    this->_vspace = source._vspace;
    this->_vgroup = source._vgroup;
    this->_vfilter = source._vfilter;
    this->_vdg = source._vdg;
    this->_mycondition = source._mycondition;
  }
  void clear()
  {
    for(int i=0;i<_vspace.size();i++)
    {
      delete _vspace[i];
//      _vgroup[i]->clearAll(); // end problem
      delete _vfilter[i];
    }
    _vspace.clear();
    _vgroup.clear();
    _vfilter.clear();
    _vdg.clear();
  }
};
class nonLinearConstraintBC : public nonLinearBoundaryCondition
{
 public:
  int _comp; // component
  FilterDof *_filter;
  FunctionSpaceBase *_space;
  virtual nonLinearBoundaryCondition::type getType() const{return nonLinearBoundaryCondition::CONSTRAINT;}
  nonLinearConstraintBC() : nonLinearBoundaryCondition(),_comp(0),_space(NULL), _filter(NULL){}
  nonLinearConstraintBC(const nonLinearConstraintBC &source) : nonLinearBoundaryCondition(source){
    this->_comp = source._comp;
    this->_space = source._space;
    this->_filter = source._filter;
  }
};

class nonLinearFixOnFaceBC: public nonLinearBoundaryCondition{
  public:
    double A, B, C, D;

  public:
    virtual nonLinearBoundaryCondition::type getType() const {return nonLinearBoundaryCondition::FIXONFACE;}
    nonLinearFixOnFaceBC(const double aa, const double bb, const double cc, const double dd):
            A(aa),B(bb),C(cc),D(dd),nonLinearBoundaryCondition(){}
    nonLinearFixOnFaceBC(const nonLinearFixOnFaceBC& src):nonLinearBoundaryCondition(src){
      this->A = src.A;
      this->B = src.B;
      this->C = src.C;
      this->D = src.D;
    }
    virtual ~nonLinearFixOnFaceBC(){};
};

class nonLinearSameDisplacementBC: public nonLinearBoundaryCondition{
  public:
    int comp; // component
    groupOfElements* gRoot;
    int tagRoot;

  public:
    virtual nonLinearBoundaryCondition::type getType() const {return nonLinearBoundaryCondition::SAMEDISP;}
    nonLinearSameDisplacementBC(): comp(-1),tagRoot(-1),gRoot(NULL),nonLinearBoundaryCondition(){}
    nonLinearSameDisplacementBC(const nonLinearSameDisplacementBC& src): nonLinearBoundaryCondition(src){
      this->comp = src.comp;
      this->gRoot = src.gRoot;
      this->tagRoot = src.tagRoot;
    }
    virtual ~nonLinearSameDisplacementBC(){}
};

class nonLinearPeriodicBCBetweenTwoGroups : public nonLinearBoundaryCondition{
 public:
  int comp;
  FilterDof* filter;
  groupOfElements* g1, *g2;
  int tag1, tag2;
  SVector3 direction;

  virtual nonLinearBoundaryCondition::type getType() const {return nonLinearBoundaryCondition::PERIODIC;}
  nonLinearPeriodicBCBetweenTwoGroups(const double x=0, const double y=0,const double z=0)
    :nonLinearBoundaryCondition(),comp(0),filter(NULL),g1(NULL),g2(NULL),direction(x,y,z){};
  nonLinearPeriodicBCBetweenTwoGroups(const nonLinearPeriodicBCBetweenTwoGroups& src) : nonLinearBoundaryCondition(src){
   this->comp = src.comp;
   this->g1 = src.g1;
   this->g2 = src.g2;
   this->tag1 = src.tag1;
   this->tag2 = src.tag2;
   this->filter = src.filter;
   this->direction = src.direction;
  }
  virtual ~nonLinearPeriodicBCBetweenTwoGroups(){}
};

// group will be not used but allow to store in same vector
// tag == Master physical number
class rigidContactBC : public nonLinearBoundaryCondition
{
 public:
  rigidContactSpaceBase *space;
  int _comp; // component
  simpleFunctionTime<double> *_f;
  FilterDof *_filter;
  virtual nonLinearBoundaryCondition::type getType() const{return nonLinearBoundaryCondition::RCONTACT;}
  rigidContactBC(const int physMaster) : nonLinearBoundaryCondition() , _filter(NULL){
    _tag = physMaster;
    onWhat = RIGIDCONTACT;
  }
  rigidContactBC(const rigidContactBC &source) : nonLinearBoundaryCondition(source)
  {
    _comp = source._comp;
    space = source.space;
    _f = source._f;
    _filter = source._filter;
  }
  ~rigidContactBC(){}
  void setSpace(rigidContactSpaceBase *sp){space = sp;}
};
#endif // SWIG
class nonLinearNeumannBC  : public nonLinearBoundaryCondition
{
 public:
  simpleFunctionTime<double> *_f;
  /* BE AWARE the four following vectors have to be filles in the same time */
  std::vector<FunctionSpaceBase*> _vspace;
  std::vector<groupOfElements*> _vgroup;
  std::vector<QuadratureBase*> _vquadrature;
  std::vector<partDomain*> _vdom; // Used to create the term after
  // is fill after
  std::vector<LinearTermBase<double>*> _vterm; // given by partDomain
  int _comp; // component x, y or z to create function space
  nonLinearNeumannBC () : nonLinearBoundaryCondition(),_f(NULL) {}
  nonLinearNeumannBC (const int tag, const int ow, simpleFunctionTime<double>* f): nonLinearBoundaryCondition(), _f(f),_comp(0)
  {
    _tag = tag;
    switch(ow){
     case 0:
      this->onWhat = nonLinearBoundaryCondition::UNDEF;
      break;
     case 1:
      this->onWhat = nonLinearBoundaryCondition::ON_VERTEX;
      g = new groupOfElements(0,tag);
      break;
     case 2:
      onWhat = nonLinearBoundaryCondition::ON_EDGE;
      g = new groupOfElements(1,tag);
      break;
     case 3:
      onWhat = nonLinearBoundaryCondition::ON_FACE;
      g = new groupOfElements(2,tag);
      break;
     case 4:
      onWhat = nonLinearBoundaryCondition::ON_VOLUME;
      g = new groupOfElements(3,tag);
      break;
     case 5:
      onWhat = nonLinearBoundaryCondition::PRESSURE;
      g = new groupOfElements(2,tag);
      break;
    }
  }
  nonLinearNeumannBC(const nonLinearNeumannBC &source) : _f(source._f), nonLinearBoundaryCondition(source),
                                                          _vspace(source._vspace), _vterm(source._vterm),
                                                          _comp(source._comp), _vquadrature(source._vquadrature),
                                                          _vgroup(source._vgroup), _vdom(source._vdom){}
  virtual nonLinearBoundaryCondition::type getType() const {return nonLinearBoundaryCondition::NEUMANN;}
  void clear()
  {
    for(int i=0;i<_vspace.size();i++)
    {
      delete _vspace[i];
//      _vgroup[i]->clearAll(); // end problem ??
      delete _vquadrature[i];
      delete _vterm[i];
    }
    _vspace.clear();
    _vgroup.clear();
    _vquadrature.clear();
    _vterm.clear();
    _vdom.clear();
  }
};
#ifndef SWIG
class initialCondition : public nonLinearDirichletBC {
 public:
  // for veloDCB the axis of beam is given by a simpleFunctionTime and is applied in the comp direction !!
  std::vector<groupOfElements*> _vDomGroup; // needed to prescribed normal on the boundary of domain
  /* COMP == -1 --> prescribed an initial value normal to the element */
  initialCondition(int comp, nonLinearBoundaryCondition::whichCondition wc) : nonLinearDirichletBC()
  {
    _comp=comp;
    _mycondition=wc;
  }
  ~initialCondition(){}
  initialCondition(const initialCondition &source) : nonLinearDirichletBC(source){
    _vDomGroup = source._vDomGroup;
  }
  virtual nonLinearBoundaryCondition::type getType() const {return nonLinearBoundaryCondition::INITIAL;}
  void clear()
  {
    nonLinearDirichletBC::clear();
  }
};
#endif

#endif // non linear Boundary Conditions
